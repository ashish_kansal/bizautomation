/******************************************************************
Project: Release 14.1 Date: 14.SEP.2020
Comments: STORE PROCEDURES
*******************************************************************/

/****** Object:  UserDefinedFunction [dbo].[Split]    Script Date: 07/26/2008 18:13:14 ******/
SET ANSI_NULLS OFF
GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetProjectCapacityLoad')
DROP FUNCTION GetProjectCapacityLoad
GO
CREATE FUNCTION [dbo].[GetProjectCapacityLoad]
(
	@numDomainID NUMERIC(18,0)
	,@numUsercntID NUMERIC(18,0)
	,@numTeamID NUMERIC(18,0)
	,@numProId NUMERIC(18,0)
	,@numTaskID NUMERIC(18,0)
	,@tintDateRange TINYINT -- 1:Today, 2:Week, 3:Month
	,@dtFromDate DATETIME
	,@ClientTimeZoneOffset INT
)    
RETURNS INT   
AS
BEGIN
	SET @dtFromDate = DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,@dtFromDate)

	DECLARE @numDivisionID NUMERIC(18,0)
	SELECT @numDivisionID=numDivisionID FROM Domain WHERE numDomainId=@numDomainID
	
	DECLARE @numQtyToBuild FLOAT
	DECLARE @dtPlannedStartDate DATETIME
	DECLARE @dtActualStartDate DATETIME = NULL

	SELECT 
		@numQtyToBuild=1
		,@dtPlannedStartDate=ISNULL(dtmStartDate,bintCreatedDate)
	FROM 
		ProjectsMaster WHERE numProId=@numProId


	IF EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID IN (SELECT numTaskId FROM StagePercentageDetailsTask WHERE numProjectId=@numProId) AND tintAction=1)
	BEGIN
		SELECT @dtActualStartDate = MIN(dtActionTime) FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID IN (SELECT numTaskId FROM StagePercentageDetailsTask WHERE numProjectId=@numProId) AND tintAction=1
	END

	DECLARE @TEMPTaskTimeSequence TABLE
	(
		numAssignedTo NUMERIC(18,0)
		,dtDate DATE
		,numTaskHours NUMERIC(18,0)
	)

	DECLARE @TempTaskAssignee TABLE
	(
		numAssignedTo NUMERIC(18,0)
		,dtLastTaskCompletionTime DATETIME
	)

	DECLARE @TempTasks TABLE
	(
		ID INT IDENTITY(1,1)
		,numTaskID NUMERIC(18,0)
		,numTaskTimeInMinutes NUMERIC(18,0)
		,numTaskAssignee NUMERIC(18,0)
		,intTaskType INT --1:Parallel, 2:Sequential
		,dtPlannedStartDate DATETIME
	)

	INSERT INTO @TempTasks
	(
		numTaskID
		,numTaskTimeInMinutes
		,intTaskType
		,numTaskAssignee
	)
	SELECT 
		SPDT.numTaskID
		,((ISNULL(SPDT.numHours,0) * 60) + ISNULL(SPDT.numMinutes,0)) * (@numQtyToBuild - ISNULL((SELECT SUM(numProcessedQty) FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID = SPDT.numTaskId),0))
		,ISNULL(SPDT.intTaskType,1)
		,SPDT.numAssignTo
	FROM 
		StagePercentageDetailsTask SPDT
	INNER JOIN
		ProjectsMaster pro
	ON
		SPDT.numProjectId = pro.numProId
	WHERE 
		SPDT.numDomainId=@numDomainID
		AND SPDT.numProjectId = @numProId
		AND NOT EXISTS (SELECT * FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID = SPDT.numTaskId AND tintAction=4) 

	UPDATE @TempTasks SET dtPlannedStartDate=@dtPlannedStartDate

	INSERT INTO @TempTaskAssignee
	(
		numAssignedTo
	)
	SELECT DISTINCT
		numTaskAssignee
	FROM
		@TempTasks

	DECLARE @i INT = 1
	DECLARE @iCount INT 	
	SELECT @iCount = COUNT(*) FROM @TempTasks

	DECLARE @numTaskAssignee NUMERIC(18,0)
	DECLARE @intTaskType INT
	DECLARE @numWorkScheduleID NUMERIC(18,0)
	DECLARE @numTempUserCntID NUMERIC(18,0)
	DECLARE @dtStartDate DATETIME
	DECLARE @numTotalTaskInMinutes NUMERIC(18,0)
	DECLARE @numProductiveTimeInMinutes NUMERIC(18,0)
	DECLARE @tmStartOfDay TIME(7)
	DECLARE @numTimeLeftForDay NUMERIC(18,0)
	DECLARE @vcWorkDays VARCHAR(20)
	DECLARE @bitParallelStartSet BIT = 0

	WHILE @i <= @iCount
	BEGIN
		SELECT
			@numTaskAssignee=numTaskAssignee
			,@numTotalTaskInMinutes=numTaskTimeInMinutes
			,@intTaskType = intTaskType
		FROM
			@TempTasks 
		WHERE
			ID=@i

		-- GET TASK COMPLETION TIME WHICH WILL BE START DATETIME FOR NEXT TASK
		SELECT
			@numWorkScheduleID = WS.ID
			,@numProductiveTimeInMinutes=(ISNULL(WS.numProductiveHours,0) * 60) + ISNULL(WS.numProductiveMinutes,0)
			,@tmStartOfDay = tmStartOfDay 
			,@vcWorkDays=vcWorkDays
			,@dtStartDate = (CASE WHEN @dtActualStartDate IS NULL THEN DATEADD(MINUTE,@ClientTimeZoneOffset,CONVERT(DATETIME,CONVERT(VARCHAR(8),DATEADD(MINUTE,-@ClientTimeZoneOffset,@dtPlannedStartDate),112)) + tmStartOfDay) ELSE @dtActualStartDate END)
		FROM
			WorkSchedule WS
		INNER JOIN
			UserMaster
		ON
			WS.numUserCntID = UserMaster.numUserDetailId
		WHERE 
			WS.numUserCntID = @numTaskAssignee

		IF @intTaskType=1 AND EXISTS (SELECT numAssignedTo FROM @TempTaskAssignee WHERE numAssignedTo = @numTaskAssignee AND dtLastTaskCompletionTime IS NOT NULL)
		BEGIN
			SELECT @dtStartDate = dtLastTaskCompletionTime FROM @TempTaskAssignee WHERE numAssignedTo = @numTaskAssignee
		END
		ELSE IF EXISTS (SELECT numAssignedTo FROM @TempTaskAssignee WHERE numAssignedTo = @numTaskAssignee AND dtLastTaskCompletionTime IS NOT NULL)
		BEGIN
			SELECT @dtStartDate = dtLastTaskCompletionTime FROM @TempTaskAssignee WHERE numAssignedTo = @numTaskAssignee
		END

		UPDATE @TempTasks SET dtPlannedStartDate=@dtStartDate WHERE ID=@i 

		IF @numProductiveTimeInMinutes > 0 AND @numTotalTaskInMinutes > 0 AND LEN(ISNULL(@vcWorkDays,'')) > 0
		BEGIN
			WHILE @numTotalTaskInMinutes > 0
			BEGIN
				-- IF ITS WORKING DAY AND ALSO NOT HOLIDAY THEN PROCEED OR INCRESE DATE TO NEXT DAY
				IF DATEPART(WEEKDAY,@dtStartDate) IN (SELECT Id FROM dbo.SplitIDs(@vcWorkDays,',')) AND NOT EXISTS (SELECT ID FROM WorkScheduleDaysOff WHERE numWorkScheduleID=@numWorkScheduleID AND CAST(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,@dtStartDate) AS DATE) BETWEEN dtDayOffFrom AND dtDayOffTo)
				BEGIN
					IF CAST(@dtStartDate AS DATE) = CAST(GETUTCDATE() AS DATE)
					BEGIN
						SET @numTimeLeftForDay = DATEDIFF(MINUTE,@dtStartDate,DATEADD(MINUTE,@numProductiveTimeInMinutes,DATEADD(MINUTE,@ClientTimeZoneOffset,CONVERT(DATETIME,CONVERT(VARCHAR(8),DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()),112)) + @tmStartOfDay)))

						-- CHECK TIME LEFT FOR DAY BASED
						IF @numTimeLeftForDay >= @numProductiveTimeInMinutes
						BEGIN
							SET @numTimeLeftForDay = @numProductiveTimeInMinutes;
						END
					END
					ELSE
					BEGIN
						SET @numTimeLeftForDay = @numProductiveTimeInMinutes
					END

					IF @numTimeLeftForDay > 0
					BEGIN
						INSERT INTO @TEMPTaskTimeSequence
						(
							numAssignedTo
							,dtDate
							,numTaskHours
						)
						VALUES
						(
							@numTaskAssignee
							,@dtStartDate
							,(CASE WHEN @numTimeLeftForDay > @numTotalTaskInMinutes THEN @numTotalTaskInMinutes ELSE @numTimeLeftForDay END)
						)

						IF @numTimeLeftForDay > @numTotalTaskInMinutes
						BEGIN
							SET @dtStartDate = DATEADD(MINUTE,@numTotalTaskInMinutes,@dtStartDate)
						END
						ELSE
						BEGIN
							SET @dtStartDate = DATEADD(DAY,1,@dtStartDate)
						END

						SET @numTotalTaskInMinutes = @numTotalTaskInMinutes - @numTimeLeftForDay
					END
					ELSE
					BEGIN
						SET @dtStartDate = DATEADD(DAY,1,@dtStartDate)
					END
				END
				ELSE
				BEGIN
					SET @dtStartDate = DATEADD(DAY,1,@dtStartDate)					
				END				
			END
		END	

		UPDATE @TempTaskAssignee SET dtLastTaskCompletionTime=@dtStartDate WHERE numAssignedTo=@numTaskAssignee

		SET @i = @i + 1
	END


	DECLARE @TEMPEmployee TABLE
	(
		ID INT IDENTITY(1,1)
		,numContactID NUMERIC(18,0)
		,numProductiveMinutes NUMERIC(18,0)
		,tmStartOfDay TIME(7)
		,vcWorkDays VARCHAR(20)
		,numCapacityLoad NUMERIC(18,0)
	)

	INSERT INTO @TEMPEmployee
	(
		numContactID
		,numProductiveMinutes
		,tmStartOfDay
		,vcWorkDays
	)
	SELECT 
		AdditionalContactsInformation.numContactID
		,(ISNULL(WorkSchedule.numProductiveHours,0) * 60) + ISNULL(WorkSchedule.numProductiveMinutes,0)
		,WorkSchedule.tmStartOfDay
		,WorkSchedule.vcWorkDays
	FROM
		AdditionalContactsInformation
	LEFT JOIN
		WorkSchedule
	ON
		AdditionalContactsInformation.numContactId = WorkSchedule.numUserCntID
	WHERE
		AdditionalContactsInformation.numDomainID=@numDomainID
		AND AdditionalContactsInformation.numDivisionId = @numDivisionID
		AND ((@numUsercntID > 0 AND AdditionalContactsInformation.numContactId=@numUsercntID) OR (@numTeamID > 0 AND AdditionalContactsInformation.numTeam=@numTeamID))

	DECLARE @j INT 
	DECLARE @jCount INT
	DECLARE @numTimeAndExpenseMinues NUMERIC(18,0)
	DECLARE @numAssemblyBuildMinues NUMERIC(18,0)

	SET @j = 1
	SELECT @jCount = COUNT(*) FROM @TEMPEmployee
	DECLARE @numContactID NUMERIC(18,0)

	WHILE @j <= @jCount
	BEGIN
		SELECT 
			@numContactID=numContactID
			,@numProductiveTimeInMinutes = numProductiveMinutes
		FROM 
			@TEMPEmployee 
		WHERE 
			ID=@j

		SET @numTimeAndExpenseMinues = ISNULL((SELECT 
														SUM(DATEDIFF(MINUTE,TimeAndExpense.dtFromDate,TimeAndExpense.dtToDate))
													FROM 
														TimeAndExpense 
													WHERE 
														numDomainID=@numDomainID 
														AND numUserCntID = @numContactID
														AND numCategory=1 
														AND numType IN (1,2)
														AND 1 = (CASE 
																	WHEN @tintDateRange = 1 -- Day
																	THEN (CASE WHEN CAST(dtFromDate AS DATE) = CAST(@dtFromDate AS DATE) THEN 1 ELSE 0 END)
																	WHEN @tintDateRange = 2 -- Week
																	THEN (CASE WHEN CAST(dtFromDate AS DATE) BETWEEN CAST(@dtFromDate AS DATE) AND CAST(DATEADD(DAY,7,@dtFromDate) AS DATE) THEN 1 ELSE 0 END) --DATEADD(DAY,2 - DATEPART(WEEKDAY, GETDATE()), CAST(GETDATE() AS DATE))
																	WHEN @tintDateRange = 3 -- Month
																	THEN (CASE WHEN CAST(dtFromDate AS DATE) BETWEEN CAST(@dtFromDate AS DATE) AND CAST(DATEADD(MILLISECOND,-3,DATEADD(MONTH,1,@dtFromDate)) AS DATE) THEN 1 ELSE 0 END) --CAST(DATEADD(DAY, 1-DAY(GETUTCDATE()), GETUTCDATE()) AS DATE)
																	ELSE 0
																END)),0)

		SET @numAssemblyBuildMinues = ISNULL((SELECT 
												SUM(numTaskHours)
											FROM
												@TEMPTaskTimeSequence 
											WHERE
												numAssignedTo = @numContactID
												AND 1 = (CASE 
															WHEN @tintDateRange = 1 -- Day
															THEN (CASE WHEN CAST(dtDate AS DATE) = CAST(@dtFromDate AS DATE) THEN 1 ELSE 0 END)
															WHEN @tintDateRange = 2 -- Week
															THEN (CASE WHEN CAST(dtDate AS DATE) BETWEEN CAST(@dtFromDate AS DATE) AND CAST(DATEADD(DAY,7,@dtFromDate) AS DATE) THEN 1 ELSE 0 END)
															WHEN @tintDateRange = 3 -- Month
															THEN (CASE WHEN CAST(dtDate AS DATE) BETWEEN CAST(@dtFromDate AS DATE) AND CAST(DATEADD(MILLISECOND,-3,DATEADD(MONTH,1,@dtFromDate)) AS DATE) THEN 1 ELSE 0 END)
															ELSE 0
														END)),0)


		SET @numProductiveTimeInMinutes = @numProductiveTimeInMinutes * (CASE WHEN @tintDateRange=1 THEN 1 WHEN @tintDateRange=2 THEN 7 WHEN @tintDateRange=3 THEN 30 END) 
		
		UPDATE @TEMPEmployee SET numCapacityLoad = (CASE WHEN @numProductiveTimeInMinutes > 0 THEN (((@numAssemblyBuildMinues + @numTimeAndExpenseMinues) * 100) / @numProductiveTimeInMinutes) ELSE 0 END) WHERE ID = @j


		SET @j = @j + 1
	END

	RETURN ISNULL((SELECT AVG(numCapacityLoad) FROM @TEMPEmployee),0)
END
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetTimeSpendOnTask')
DROP FUNCTION GetTimeSpendOnTask
GO
CREATE FUNCTION [dbo].[GetTimeSpendOnTask]
(
	@numDomainID NUMERIC(18,0)
	,@numTaskID NUMERIC(18,0)
	,@tintReturnType TINYINT
)    
RETURNS VARCHAR(100)    
AS
BEGIN
	DECLARE @vcTotalTimeSpendOnTask VARCHAR(50) = ''
	DECLARE @numTotalMinutes INT = 0

	DECLARE @TEMTIMELong TABLE
	(
		ID INT IDENTITY(1,1)
		,tintAction TINYINT
		,dtActionTime DATETIME
	)

	INSERT INTO @TEMTIMELong
	(
		tintAction
		,dtActionTime
	)
	SELECT
		tintAction
		,dtActionTime
	FROM
		StagePercentageDetailsTaskTimeLog SPDTTL
	WHERE
		SPDTTL.numDomainID=@numDomainID
		AND SPDTTL.numTaskID=@numTaskID
	ORDER BY
		dtActionTime
		,tintAction

	DECLARE @i INT = 1
	DECLARE @iCount INT
	DECLARE @tintLastAction TINYINT
	DECLARE @dtLastActionTime DATETIME

	DECLARE @tintAction TINYINT
	DECLARE @dtActionTime DATETIME
	SELECT @iCount = COUNT(*) FROM @TEMTIMELong

	WHILE @i <= @iCount
	BEGIN
		SELECT
			@tintAction=tintAction
			,@dtActionTime=dtActionTime
		FROM
			@TEMTIMELong
		WHERE
			ID = @i

		IF @i = 1 
		BEGIN
			IF @tintAction <> 1
			BEGIN
				SET @vcTotalTimeSpendOnTask = 'Invalid time sequence'
				BREAK;
			END
			ELSE
			BEGIN
				SET @tintLastAction = @tintAction
				SET @dtLastActionTime = @dtActionTime
			END
		END
		ELSE
		BEGIN
			IF @tintLastAction = 1 AND @tintAction NOT IN (2,4)
			BEGIN
				SET @vcTotalTimeSpendOnTask = 'Invalid time sequence'
				BREAK;
			END
			--ELSE IF @tintLastAction = 2 AND @tintAction <> 3
			ELSE IF @tintLastAction = 2 AND @tintAction NOT IN(3,4)
			BEGIN
				SET @vcTotalTimeSpendOnTask = 'Invalid time sequence'
				BREAK;
			END
			ELSE IF @tintLastAction = 3 AND @tintAction NOT IN (2,4)
			BEGIN
				SET @vcTotalTimeSpendOnTask = 'Invalid time sequence'
				BREAK;
			END
			ELSE IF @tintLastAction = 4
			BEGIN
				SET @vcTotalTimeSpendOnTask = 'Invalid time sequence'
				BREAK;
			END
			ELSE
			BEGIN
				IF @tintAction IN (2,4)
				BEGIN
					SET @numTotalMinutes = @numTotalMinutes + DATEDIFF(MINUTE,@dtLastActionTime,@dtActionTime)
				END

				SET @tintLastAction = @tintAction
				SET @dtLastActionTime = @dtActionTime
			END
		END

		SET @i = @i + 1
	END

	IF @vcTotalTimeSpendOnTask <> 'Invalid time sequence'
	BEGIN
		IF ISNULL(@tintReturnType,0) = 1
		BEGIN
			RETURN CONCAT('<ul class="list-inline"><li style="padding-right:0px">',(CASE WHEN (@numTotalMinutes / 60) > 0 THEN CONCAT('<label style="color:#3366ff">',(@numTotalMinutes / 60),(CASE WHEN (@numTotalMinutes / 60) > 1 THEN ' Hours' ELSE ' Hour' END),'</label>') ELSE '' END),'</li><li>',(CASE WHEN (@numTotalMinutes % 60) > 0 THEN CONCAT('<label>',@numTotalMinutes % 60,(CASE WHEN (@numTotalMinutes % 60) > 1 THEN ' Minutes' ELSE ' Minute' END),'</label>') ELSE '' END),'</li></ul>')
		END
		ELSE IF ISNULL(@tintReturnType,0) = 2
		BEGIN
			RETURN @numTotalMinutes
		END
		ELSE
		BEGIN
			RETURN FORMAT(@numTotalMinutes / 60,'00') + ':' + FORMAT(@numTotalMinutes % 60,'00')
		END
	END
	ELSE IF ISNULL(@tintReturnType,0) = 2
	BEGIN
		SET @vcTotalTimeSpendOnTask = '0'
	END
	
	RETURN @vcTotalTimeSpendOnTask
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetTimeSpendOnTaskByProject')
DROP FUNCTION GetTimeSpendOnTaskByProject
GO
CREATE FUNCTION [dbo].[GetTimeSpendOnTaskByProject]
(
	@numDomainID NUMERIC(18,0)
	,@numTaskID NUMERIC(18,0)
	,@tintReturnType TINYINT
)    
RETURNS VARCHAR(100)    
AS
BEGIN
	DECLARE @vcTotalTimeSpendOnTask VARCHAR(50) = ''
	DECLARE @numTotalMinutes INT = 0

	DECLARE @TEMTIMELong TABLE
	(
		ID INT IDENTITY(1,1)
		,tintAction TINYINT
		,dtActionTime DATETIME
	)

	INSERT INTO @TEMTIMELong
	(
		tintAction
		,dtActionTime
	)
	SELECT
		tintAction
		,dtActionTime
	FROM
		StagePercentageDetailsTaskTimeLog SPDTTL
	WHERE
		SPDTTL.numDomainID=@numDomainID
		AND SPDTTL.numTaskID=@numTaskID
	ORDER BY
		dtActionTime
		,ID 

	DECLARE @i INT = 1
	DECLARE @iCount INT
	DECLARE @tintLastAction TINYINT
	DECLARE @dtLastActionTime DATETIME

	DECLARE @tintAction TINYINT
	DECLARE @dtActionTime DATETIME
	SELECT @iCount = COUNT(*) FROM @TEMTIMELong

	WHILE @i <= @iCount
	BEGIN
		SELECT
			@tintAction=tintAction
			,@dtActionTime=dtActionTime
		FROM
			@TEMTIMELong
		WHERE
			ID = @i

		IF @i = 1 
		BEGIN
		
			IF @tintAction <> 1
			BEGIN
				SET @vcTotalTimeSpendOnTask = 'Invalid time sequence'
				BREAK;
			END
			ELSE
			BEGIN
				SET @tintLastAction = @tintAction
				SET @dtLastActionTime = @dtActionTime
			END
		END
		ELSE
		BEGIN
			
			IF @tintLastAction = 1 AND @tintAction NOT IN (2,4)
			BEGIN
				SET @vcTotalTimeSpendOnTask = 'Invalid time sequence'
				BREAK;
			END
			ELSE IF @tintLastAction = 2 AND @tintAction NOT IN(3,4)
			BEGIN
				SET @vcTotalTimeSpendOnTask = 'Invalid time sequence'
				BREAK;
			END
			ELSE IF @tintLastAction = 3 AND @tintAction NOT IN (2,4)
			BEGIN
				SET @vcTotalTimeSpendOnTask = 'Invalid time sequence'
				BREAK;
			END
			ELSE IF @tintLastAction = 4
			BEGIN
				SET @vcTotalTimeSpendOnTask = 'Invalid time sequence'
				BREAK;
			END
			ELSE
			BEGIN
				IF @tintAction IN (2,4)
				BEGIN
					SET @numTotalMinutes = @numTotalMinutes + DATEDIFF(MINUTE,@dtLastActionTime,@dtActionTime)
				END

				SET @tintLastAction = @tintAction
				SET @dtLastActionTime = @dtActionTime
			END
		END

		SET @i = @i + 1
	END

	IF @vcTotalTimeSpendOnTask <> 'Invalid time sequence'
	BEGIN
		IF ISNULL(@tintReturnType,0) = 1
		BEGIN
			RETURN CONCAT('<ul class="list-inline"><li style="padding-right:0px">',(CASE WHEN (@numTotalMinutes / 60) > 0 THEN CONCAT('<label style="color:#3366ff">',(@numTotalMinutes / 60),(CASE WHEN (@numTotalMinutes / 60) > 1 THEN ' Hours' ELSE ' Hour' END),'</label>') ELSE '' END),'</li><li>',(CASE WHEN (@numTotalMinutes % 60) > 0 THEN CONCAT('<label>',@numTotalMinutes % 60,(CASE WHEN (@numTotalMinutes % 60) > 1 THEN ' Minutes' ELSE ' Minute' END),'</label>') ELSE '' END),'</li></ul>')
		END
		ELSE IF ISNULL(@tintReturnType,0) = 2
		BEGIN
			RETURN @numTotalMinutes
		END
		ELSE
		BEGIN
			RETURN FORMAT(@numTotalMinutes / 60,'00') + ':' + FORMAT(@numTotalMinutes % 60,'00')
		END
	END
	ELSE IF ISNULL(@tintReturnType,0) = 2
	BEGIN
		SET @vcTotalTimeSpendOnTask = '0'
	END
	
	RETURN @vcTotalTimeSpendOnTask
END

GO
/****** Object:  StoredProcedure [dbo].[usp_GenDocList]    Script Date: 02/16/2010 00:39:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Anoop Jayaraj                  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_gendoclist')
DROP PROCEDURE usp_gendoclist
GO
CREATE PROCEDURE [dbo].[usp_GenDocList]                    
(
	@numDomainID NUMERIC(18,0)=0,           
	@numUserCntID NUMERIC(18,0)=0,	                   
	@tintUserRightType TINYINT=0,
	@CurrentPage INT,
	@PageSize INT,
	@columnName VARCHAR(50),
	@columnSortOrder VARCHAR(10),
	@DocCategory NUMERIC(18,0)=0,
	@numDocStatus NUMERIC(18,0)=0,
	@ClientTimeZoneOffset INT,
	@numGenericDocId NUMERIC(18,0) = 0,
	@SortChar char(1)='0',
	@tintDocumentType TINYINT
)                   
AS
BEGIN  
	IF @columnSortOrder <> 'Desc' AND @columnSortOrder <> 'Asc'
	BEGIN
		SET @columnSortOrder = 'Asc'
	END

	SELECT 
		COUNT(*) OVER() AS numTotalRecords,
		numGenericDocId,                    
		VcFileName ,                    
		vcDocName ,                  
		numDocCategory,                    
		(CASE numDocCategory 
			WHEN 369 THEN CONCAT('Email Template',' <font color=green>(',ISNULL(EMM.vcModuleName,'-'),')</font> ')
			ELSE dbo.fn_GetListItemName(numDocCategory) 
		END) Category,
		vcfiletype,                    
		cUrlType,                    
		dbo.fn_GetListItemName(numDocStatus) as BusClass,                    
		dbo.fn_GetContactName(numModifiedBy)+','+ convert(varchar(20),dateadd(minute,-@ClientTimeZoneOffset,bintModifiedDate)) as [Mod]
	FROM 
		GenericDocuments 
	LEFT JOIN 
		EmailMergeModule EMM 
	ON 
		EMM.numModuleID = GenericDocuments.numModuleID 
		AND EMM.tintModuleType=0 
	WHERE 
		numDomainID = @numDomainID
		AND (ISNULL(@numGenericDocId,0) = 0 OR numGenericDocID=@numGenericDocId)
		AND tintDocumentType=1
		AND (ISNULL(@numDocStatus,0) = 0 OR numDocStatus=@numDocStatus)
		AND (ISNULL(@DocCategory,0) = 0 OR numDocCategory=@DocCategory)
		AND 1 =(CASE WHEN @tintUserRightType=1 THEN (CASE WHEN numCreatedby = @numUserCntID THEN 1 ELSE 0 END) ELSE 1 END)
		AND 1 = (CASE WHEN @SortChar <> '0' THEN (CASE WHEN vcDocName like CONCAT(@SortChar,'%') THEN 1 ELSE 0 END) ELSE 1 END)
		AND 1 = (CASE @tintDocumentType 
					WHEN 2 THEN (CASE WHEN ISNULL(VcFileName,'') LIKE '#SYS#%' THEN 1 ELSE 0 END)
					WHEN 1 THEN (CASE WHEN ISNULL(VcFileName,'') NOT LIKE '#SYS#%' THEN 1 ELSE 0 END) 
					ELSE 1 
				END)
	ORDER BY
		CASE 
			WHEN @columnSortOrder = 'Desc' AND @ColumnName <> 'Mod' THEN
			CASE @ColumnName 
			   WHEN 'vcDocName' THEN vcDocName
			   WHEN 'Category' THEN (CASE numDocCategory 
										WHEN 369 THEN CONCAT('Email Template',' <font color=green>(',ISNULL(EMM.vcModuleName,'-'),')</font> ')
										ELSE dbo.fn_GetListItemName(numDocCategory) 
									END)
			   WHEN 'vcfiletype' THEN vcfiletype
			   WHEN 'BusClass' THEN dbo.fn_GetListItemName(numDocStatus)
			END
		END DESC,
		CASE @columnSortOrder WHEN 'Desc' THEN
			CASE @ColumnName 
				WHEN 'Mod' THEN bintModifiedDate
			   ELSE bintcreateddate
			END
		END DESC,
		CASE WHEN @columnSortOrder = 'Asc' AND  @ColumnName <> 'Mod' THEN
			CASE @ColumnName
			   WHEN 'vcDocName' THEN vcDocName
			   WHEN 'Category' THEN (CASE numDocCategory 
										WHEN 369 THEN CONCAT('Email Template',' <font color=green>(',ISNULL(EMM.vcModuleName,'-'),')</font> ')
										ELSE dbo.fn_GetListItemName(numDocCategory) 
									END)
			   WHEN 'vcfiletype' THEN vcfiletype
			   WHEN 'BusClass' THEN dbo.fn_GetListItemName(numDocStatus)
			END
		END ASC,
		CASE @columnSortOrder WHEN 'Asc' THEN
			CASE @ColumnName 
				WHEN 'Mod' THEN bintModifiedDate
			   ELSE bintcreateddate
			END
		END ASC
	OFFSET (@CurrentPage-1) * @PageSize ROWS FETCH NEXT @PageSize ROWS ONLY		
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GenericDocuments_Search')
DROP PROCEDURE USP_GenericDocuments_Search
GO
CREATE PROCEDURE [dbo].[USP_GenericDocuments_Search]                          
(                          
	@numDomainID NUMERIC(18,0)
	,@numDocCategory NUMERIC(18,0)
	,@tintDocType TINYINT
	,@vcSearchText VARCHAR(100)
	,@numPageIndex INT
	,@numPageSize INT
)                          
as 
BEGIN
	SELECT
		COUNT(*) OVER() AS numTotalRecords
		,numGenericDocID AS id
		,vcDocName AS [text]
		,ISNULL(EMM.vcModuleName,'-') vcModuleName
	FROM
		GenericDocuments
	LEFT JOIN 
		EmailMergeModule EMM 
	ON 
		EMM.numModuleID = GenericDocuments.numModuleID 
		AND EMM.tintModuleType=0
	WHERE 
		numDomainID = @numDomainID
		AND tintDocumentType=1
		AND (ISNULL(@numDocCategory,0) = 0 OR numDocCategory=@numDocCategory)
		AND 1 = (CASE @tintDocType 
					WHEN 2 THEN (CASE WHEN ISNULL(VcFileName,'') LIKE '#SYS#%' THEN 1 ELSE 0 END)
					WHEN 1 THEN (CASE WHEN ISNULL(VcFileName,'') NOT LIKE '#SYS#%' THEN 1 ELSE 0 END) 
					ELSE 1 
				END)
		AND vcDocName LIKE CONCAT('%',@vcSearchText,'%')
	ORDER BY
		vcDocName
	OFFSET 
		((@numPageIndex -1) * @numPageSize) ROWS 
	FETCH NEXT @numPageSize ROWS ONLY
END
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetBillDetails' ) 
    DROP PROCEDURE USP_GetBillDetails
GO
-- exec USP_GetBillPaymentDetails 3,1,0
CREATE PROCEDURE [dbo].[USP_GetBillDetails]
    @numBillID NUMERIC(18, 0),
    @numDomainID NUMERIC
AS 
    SET NOCOUNT ON
    SET TRANSACTION ISOLATION LEVEL READ COMMITTED
    
	DECLARE @numCurrencyID NUMERIC(18,0)
	SELECT @numCurrencyID=numCurrencyID FROM Domain WHERE numDomainID = @numDomainID

    SELECT  BH.[numBillID],
            [numDivisionID],
            dbo.fn_GetComapnyName(numDivisionID) AS [vcCompanyName],
            BH.[numAccountID],
            BH.[dtBillDate],
            BH.[numTermsID],
            BH.[monAmountDue],
            BH.[dtDueDate],
            BH.[vcMemo],
            BH.[vcReference],
            BH.[monAmtPaid],
            BH.[bitIsPaid],
            BH.[numDomainID],
            BH.[numCreatedBy],
            BH.[dtCreatedDate],
            BH.[numModifiedBy],
            BH.[dtModifiedDate],ISNULL(GJD.numTransactionId,0) AS numTransactionId,
            ISNULL(PO.numProId,0) AS numProjectID,ISNULL(PO.numStageID,0) AS numStageID,
            (SELECT vcTerms FROM BillingTerms WHERE numTermsID = BH.[numTermsID] AND numDomainId = BH.numDomainID)  AS [vcTermName],
			ISNULL(BH.numCurrencyID,@numCurrencyID) numCurrencyID,
			ISNULL(BH.fltExchangeRate,1) fltExchangeRate
    FROM    [dbo].[BillHeader] BH
            LEFT OUTER JOIN General_Journal_Details GJD ON GJD.tintReferenceType=3 
						AND GJD.numReferenceID=BH.numBillID
			LEFT OUTER JOIN ProjectsOpportunities PO ON PO.numBillId=BH.numBillId			
    WHERE   BH.[numBillID] = @numBillID
            AND BH.numDomainID = @numDomainID
    
    
    SELECT  BD.[numBillDetailID],
            BD.[numExpenseAccountID],
            BD.[monAmount],
            BD.[vcDescription],
            BD.[numProjectID],
            BD.[numClassID],
            BD.[numCampaignID],
            BD.numBillID
			,ISNULL(BD.numOppItemID,0) numOppItemID
			,ISNULL(BD.numOppID,0) numOppID
			,ISNULL(GJD.numTransactionId,0) AS numTransactionId
    FROM    dbo.BillDetails BD
            INNER JOIN BillHeader BH ON BD.numBillID = BH.numBillID
           	LEFT OUTER JOIN General_Journal_Details GJD ON GJD.tintReferenceType=4 AND GJD.numReferenceID=BD.numBillDetailID
    WHERE   BH.[numBillID] = @numBillID
            AND BH.numDomainID = @numDomainID
            
GO


GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetDropDownValue')
DROP PROCEDURE USP_GetDropDownValue
GO
Create PROCEDURE [dbo].[USP_GetDropDownValue]     
    @numDomainID numeric(18, 0),
    @numListID numeric(18, 0),
    @vcListItemType AS VARCHAR(5),
    @vcDbColumnName VARCHAR(100)
as                 

CREATE TABLE #temp(numID VARCHAR(500),vcData VARCHAR(max))

INSERT INTO #temp  SELECT 0,'-- Select One --'

			 IF @vcListItemType='LI' OR @vcListItemType='T'                                                    
				BEGIN  
					INSERT INTO #temp                                                   
					SELECT Ld.numListItemID,vcData from ListDetails LD          
					left join listorder LO on Ld.numListItemID= LO.numListItemID  and lo.numDomainId = @numDomainID
					where (Ld.numDomainID=@numDomainID or Ld.constFlag=1) and Ld.numListID=@numListID order by intSortOrder
				end  
 			 ELSE IF @vcListItemType='C'                                                     
				begin    
		   		    INSERT INTO #temp                                                   
	  			    SELECT  numCampaignID,vcCampaignName + CASE bitIsOnline WHEN 1 THEN ' (Online)' ELSE ' (Offline)' END AS vcCampaignName
					FROM CampaignMaster WHERE numDomainID = @numDomainID
				end  
			 ELSE IF @vcListItemType='AG'                                                     
				begin      
		   		    INSERT INTO #temp                                                   
					SELECT numGrpID, vcGrpName FROM groups       
					ORDER BY vcGrpName                                               
				end   
			else if @vcListItemType='DC'                                                     
				begin    
		   		    INSERT INTO #temp                                                   
					select numECampaignID,vcECampName from ECampaign where numDomainID= @numDomainID                                                       
				end 
			else if @vcListItemType='U' OR @vcDbColumnName='numManagerID'
			    BEGIN
		   		     INSERT INTO #temp                                                   
			    	 SELECT A.numContactID,ISNULL(A.vcFirstName,'') +' '+ ISNULL(A.vcLastName,'') as vcUserName          
					 from UserMaster UM join AdditionalContactsInformation A        
					 on UM.numUserDetailId=A.numContactID          
					 where UM.numDomainID=@numDomainID and UM.numDomainID=A.numDomainID  ORDER BY A.vcFirstName,A.vcLastName 
				END
			else if @vcListItemType='S'
			    BEGIN
		   		     INSERT INTO #temp                                                   
			    	 SELECT S.numStateID,S.vcState          
					 from State S      
					 where S.numDomainID=@numDomainID ORDER BY S.vcState
				END
			ELSE IF @vcListItemType = 'OC' 
				BEGIN
					INSERT INTO #temp   
					SELECT DISTINCT C.numCurrencyID,C.vcCurrencyDesc FROM dbo.Currency C WHERE C.numDomainID=@numDomainID
				END
			ELSE IF @vcListItemType = 'UOM' 
				BEGIN
					INSERT INTO #temp   
					SELECT  numUOMId AS numItemID,vcUnitName AS vcItemName FROM UOM WHERE numDomainID = @numDomainID
				END	
			ELSE IF @vcListItemType = 'IG' 
				BEGIN
					INSERT INTO #temp   
					SELECT numItemGroupID,vcItemGroup FROM dbo.ItemGroups WHERE numDomainID=@numDomainID   	
				END
			ELSE IF @vcListItemType = 'SYS'
				BEGIN
					INSERT INTO #temp
					SELECT 0 AS numItemID,'Lead' AS vcItemName
					UNION ALL
					SELECT 1 AS numItemID,'Prospect' AS vcItemName
					UNION ALL
					SELECT 2 AS numItemID,'Account' AS vcItemName
				END
			ELSE IF @vcListItemType = 'PP' 
				BEGIN
					INSERT INTO #temp 
					SELECT 'P','Inventory Item'
					UNION 
					SELECT 'N','Non-Inventory Item'
					UNION 
					SELECT 'S','Service'
				END

				--<asp:ListItem Value="0" Selected="True">0</asp:ListItem>
    --                                <asp:ListItem Value="25" Selected="True">25%</asp:ListItem>
    --                                <asp:ListItem Value="50">50%</asp:ListItem>
    --                                <asp:ListItem Value="75">75%</asp:ListItem>
    --                                <asp:ListItem Value="100">100%</asp:ListItem>
				ELSE IF @vcListItemType = 'SP' --Progress Percentage
				BEGIN
					INSERT INTO #temp 
					SELECT '0','0%'
					UNION 
					SELECT '25','25%'
					UNION 
					SELECT '50','50%'
					UNION 
					SELECT '75','75%'
					UNION 
					SELECT '100','100%'
				END
				ELSE IF @vcListItemType = 'ST' --Stage Details
				BEGIN
					INSERT INTO #temp 
					SELECT vcStageName ,vcStageName  from StagePercentageDetails where numDomainId=@numDomainID and slp_id=@numListID  AND ISNULL(numProjectID,0)= 0 AND ISNULL(numOppID,0)= 0
				END
				ELSE IF @vcListItemType = 'SG' --Stage Details
				BEGIN
					INSERT INTO #temp 
					SELECT vcStageName ,vcStageName  from StagePercentageDetails where numDomainId=@numDomainID   AND ISNULL(numProjectID,0)= 0 AND ISNULL(numOppID,0)= 0
				END
				ELSE IF @vcListItemType = 'SM' --Reminer,snooze time
				BEGIN
					INSERT INTO #temp 
					SELECT '5','5 minutes'
					UNION 
					SELECT '15','15 minutes'
					UNION 
					SELECT '30','30 Minutes'
					UNION 
					SELECT '60','1 Hour'
					UNION 
					SELECT '240','4 Hour'
					UNION 
					SELECT '480','8 Hour'
					UNION 
					SELECT '1440','24 Hour'

				END
			 ELSE IF @vcListItemType = 'PT' --Pin To
				BEGIN
					INSERT INTO #temp 
					SELECT '1','Sales Opportunity'
					UNION 
					SELECT '2','Purchase Opportunity'
					UNION 
					SELECT '3','Sales Order'
					UNION 
					SELECT '4','Purchase Order'
					UNION 
					SELECT '5','Project'
					UNION 
					SELECT '6','Cases'
					UNION 
					SELECT '7','Item'

				END
				ELSE IF @vcListItemType = 'DV' --Customer
				BEGIN
					--INSERT INTO #temp 
					--SELECT vcStageName ,vcStageName  from StagePercentageDetails where numDomainId=@numDomainID and slp_id=@numListID  AND ISNULL(numProjectID,0)= 0 AND ISNULL(numOppID,0)= 0
				INSERT INTO #temp 
					SELECT d.numDivisionID,a.vcCompanyname + Case when isnull(d.numCompanyDiff,0)>0 then  ' ' + dbo.fn_getlistitemname(d.numCompanyDiff) + ':' + isnull(d.vcCompanyDiff,'') else '' end as vcCompanyname from   companyinfo AS a                      
					join divisionmaster d                      
					on  a.numCompanyid=d.numCompanyid
					WHERE D.numDomainID=@numDomainID
			
				END
				ELSE IF @vcListItemType = 'BD' --Net Days
				BEGIN
					--INSERT INTO #temp 
					--SELECT vcStageName ,vcStageName  from StagePercentageDetails where numDomainId=@numDomainID and slp_id=@numListID  AND ISNULL(numProjectID,0)= 0 AND ISNULL(numOppID,0)= 0
				INSERT INTO #temp 
						SELECT numTermsID,vcTerms	
						FROM dbo.BillingTerms 
						WHERE 
						numDomainID = @numDomainID
						AND ISNULL(bitActive,0) = 1
			
				END
				ELSE IF @vcListItemType = 'BT' --Net Days
				BEGIN
					--INSERT INTO #temp 
					--SELECT vcStageName ,vcStageName  from StagePercentageDetails where numDomainId=@numDomainID and slp_id=@numListID  AND ISNULL(numProjectID,0)= 0 AND ISNULL(numOppID,0)= 0
				INSERT INTO #temp 
							SELECT numBizDocTempID,    
							vcTemplateName
							FROM BizDocTemplate  
							WHERE [numDomainID] = @numDomainID and tintTemplateType=0
			
				END
				ELSE IF @vcListItemType = 'PSS' --Parcel Shipping Service
				BEGIN
					INSERT INTO #temp (numID,vcData) 
					SELECT
						numShippingServiceID
						,vcShipmentService
					FROM 
						ShippingService
					WHERE
						numDomainID=@numDomainID OR ISNULL(numDomainID,0)=0
					UNION
					SELECT
						-1
						,'Set Manually'
				END
				ELSE IF @vcListItemType = 'IC'  -- Item Category
				BEGIN
					INSERT INTO #temp (numID,vcData) 
					SELECT 							
							C1.numCategoryID,
							C1.vcCategoryName							
						FROM 
							Category C1 
						WHERE 
							numDomainID =@numDomainID				
					ORDER BY
						vcCategoryName
					
				END
				ELSE IF @vcDbColumnName = 'vcInventoryStatus'
				BEGIN
					INSERT INTO #temp
							SELECT 1,'Not Applicable' UNION
							SELECT 2,'Shipped' UNION
							SELECT 3,'Back Order' UNION
							SELECT 4,'Shippable' 
				END
				ELSE IF @vcDbColumnName='charSex'
					BEGIN
							INSERT INTO #temp
							SELECT 'M','Male' UNION
							SELECT 'F','Female'
						END
				ELSE IF @vcDbColumnName='tintOppStatus'
					BEGIN
							INSERT INTO #temp
							SELECT 1,'Deal Won' UNION
							SELECT 2,'Deal Lost'
					END
				ELSE IF @vcDbColumnName='tintOppType'
					BEGIN
							INSERT INTO #temp
							SELECT 1,'Sales Order' UNION
							SELECT 2,'Purchase Order' UNION
							SELECT 3,'Sales Opportunity' UNION
							SELECT 4,'Purchase Opportunity' 
					END
					ELSE IF @vcDbColumnName = 'vcWarehouse'
					BEGIN
						INSERT INTO #temp
						SELECT
							numWareHouseID,
							ISNULL(vcWareHouse,'')
						FROM
							Warehouses
						WHERE
							numDomainID=@numDomainID
					END
					ELSE IF @vcDbColumnName = 'vcLocation'
					BEGIN
						INSERT INTO #temp
						SELECT -1,'Global' UNION
						SELECT
							numWLocationID,
							ISNULL(vcLocation,'')
						FROM
							WarehouseLocation
						WHERE
							numDomainID=@numDomainID
				
					END
					ELSE IF @vcDbColumnName = 'tintPriceLevel'
					BEGIN
						DECLARE @Temp TABLE
						(
							Id INT
						)
						INSERT INTO @Temp
						(
							Id
						)
						SELECT DISTINCT 
							ROW_NUMBER() OVER(PARTITION BY pt.numItemCode ORDER BY numPricingID) Id
						FROM 
							[PricingTable] pt
						INNER JOIN 
							Item
						ON 
							Item.numItemCode = pt.numItemCode 
							AND Item.numDomainID = @numDomainID
						WHERE 
							tintRuleType = 3
							AND ISNULL(pt.numCurrencyID,0) = 0
						ORDER BY [Id] 

						INSERT INTO 
							#temp
						SELECT 
							pt.Id
							,ISNULL(NULLIF(pnt.vcPriceLevelName, ''), CONCAT('Price Level ',pt.Id)) Value
						FROM 
							@Temp pt
						LEFT JOIN
							PricingNamesTable pnt
						ON 
							pt.Id = pnt.tintPriceLevel
						AND 
							pnt.numDomainID = @numDomainID				
					END
					ELSE IF @vcDbColumnName = 'numExpenseServiceItems'
					BEGIN
						INSERT INTO #temp SELECT numItemCode,vcItemName FROM Item WHERE numDomainID=@numDomainID AND charItemType='S' AND ISNULL(bitExpenseItem,0)=1 ORDER BY vcItemName
					END

--			Else                                                     
--				BEGIN                                                     
--					select @listName=vcData from ListDetails WHERE numListItemID=@numlistID
--				end 	

 				
        
		SELECT * FROM #temp 
		DROP TABLE #temp	
GO

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetOpenOrdersAddBill')
DROP PROCEDURE USP_GetOpenOrdersAddBill
GO
CREATE PROCEDURE [dbo].[USP_GetOpenOrdersAddBill]
(
	@numDomainID NUMERIC(18,0)
	,@numDivisionID NUMERIC(18,0)
	,@numOppID NUMERIC(18,0)
)
AS
BEGIN
	IF ISNULL(@numOppID,0) > 0
	BEGIN
		SELECT
			numOppId
			,vcPOppName
		FROM
			OpportunityMaster
		WHERE
			numDomainId = @numDomainID
			AND numOppId = @numOppID
	END
	ELSE
	BEGIN
		SELECT
			numOppId
			,vcPOppName
		FROM
			OpportunityMaster
		WHERE
			numDomainId = @numDomainID
			AND numDivisionId = @numDivisionID
			AND tintOppType = 1
			AND tintOppStatus = 1
			AND ISNULL(tintshipped,0) = 0
			AND EXISTS (SELECT OI.numoppitemtCode FROM OpportunityItems OI INNER JOIN Item I ON OI.numItemCode=I.numItemCode WHERE OI.numOppId = OpportunityMaster.numOppId AND ISNULL(I.bitExpenseItem,0) = 1)
		ORDER BY
			numOppId
	END
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetOrderExpenseItemsAddBill')
DROP PROCEDURE USP_GetOrderExpenseItemsAddBill
GO
CREATE PROCEDURE [dbo].[USP_GetOrderExpenseItemsAddBill]
(
	@numDomainID NUMERIC(18,0)
	,@numOppID NUMERIC(18,0)
	,@numOppItemID NUMERIC(18,0)
)
AS
BEGIN
	IF ISNULL(@numOppItemID,0) > 0
	BEGIN
		SELECT 
			OI.numoppitemtCode
			,ISNULL(OI.vcItemName,I.vcItemName) vcItemName
		FROM 
			OpportunityItems OI
		INNER JOIN
			Item I
		ON
			OI.numItemCode = I.numItemCode
		WHERE
			OI.numoppitemtCode = @numOppItemID
	END
	ELSE
	BEGIN

		SELECT 
			OI.numoppitemtCode
			,ISNULL(OI.vcItemName,I.vcItemName) vcItemName
		FROM 
			OpportunityItems OI
		INNER JOIN
			Item I
		ON
			OI.numItemCode = I.numItemCode
		WHERE
			OI.numOppId = @numOppID
			AND ISNULL(I.charItemType,0) = 'S'
			AND ISNULL(bitExpenseItem,0) = 1
			AND OI.numoppitemtCode NOT IN (SELECT BD.numOppItemID FROM BillDetails BD WHERE numOppID=@numOppID)
		ORDER BY
			ISNULL(OI.numSortOrder,0)
	END
END
GO
GO
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetProjectExpenseReports')
DROP PROCEDURE USP_GetProjectExpenseReports
GO
CREATE PROCEDURE [dbo].[USP_GetProjectExpenseReports]
(
    @numDomainID NUMERIC(18,0),
    @numProId NUMERIC(18,0)
)
AS 
BEGIN
	DECLARE @numCost TINYINT
	DECLARE @numCurrencyID NUMERIC(18,0)
	DECLARE @varCurrSymbol VARCHAR(10)
	SELECT @numCost = ISNULL(numCost,1),@numCurrencyID=ISNULL(numCurrencyID,0) FROM Domain WHERE numDomainId=@numDomainID

	SELECT @varCurrSymbol = ISNULL(varCurrSymbol,'$') FROM Currency WHERE numCurrencyID = @numCurrencyID

	DECLARE @TEMP TABLE
	(
		vcItem TEXT
		,vcUnits TEXT
		,vcIncomeSource TEXT
		,vcExpenseSource TEXT
		,vcIncomeExpense TEXT
		,vcNetIncome TEXT
		,monIncome DECIMAL(20,5)
		,monExpense DECIMAL(20,5)
		,monNetIncome DECIMAL(20,5)
		,bitExpensePending BIT
	)

	-- Sales Orders
	INSERT INTO @TEMP
	(
		vcItem
		,vcUnits
		,vcIncomeSource
		,vcExpenseSource
		,monIncome
		,monExpense
		,bitExpensePending
	)
	SELECT 
		CONCAT(ISNULL(OI.vcItemName,I.vcItemName),' <i style="color:#afabab">(',(CASE 
																						WHEN I.charItemType='P' 
																						THEN CONCAT('Inventory',(CASE WHEN ISNULL(OI.bitDropship,0) = 1 THEN ' Drop ship' ELSE '' END))
																						WHEN I.charItemType='N' THEN 'Non-Inventory' 
																						WHEN I.charItemType='S' THEN 'Service' 
																						WHEN I.charItemType='A' THEN 'Accessory' 
																					END),')</i> ',OI.vcItemDesc)
		,CONCAT(dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0),I.numItemCode, @numDomainID,ISNULL(OI.numUOMId, 0)) * OI.numUnitHour,' (',ISNULL(UOM.vcUnitName,'-'),')')
		,CONCAT('<a href="../opportunity/frmOpportunities.aspx?OpID=',OM.numOppID,'" target="_blank">',vcPOppName,'</a>')
		,(CASE 
			WHEN I.charItemType='S' AND ISNULL(I.bitExpenseItem,0) = 1 
			THEN (CASE 
					WHEN BH.numBillID IS NULL 
					THEN '<span class="text-orange">Pending</span>' 
					ELSE CONCAT('<a href="javascript:void(0)" onclick="OpenBizInvoice(0,0,',BH.numBillID,',2);">',((CASE WHEN ISNULL(BH.bitLandedCost,0)=1 THEN 'Laned Cost-' ELSE 'Bill-' END) + CONVERT(VARCHAR(10),BH.numBillID)),'</a>') 
				END)
			ELSE (CASE @numCost 
					WHEN 3 THEN 'Products & Services Cost' 
					WHEN 2 THEN 'Primary Vendor Cost'
					ELSE (CASE WHEN I.charItemType='P' THEN 'Average Cost' ELSE 'Primary Vendor Cost' END)
				END)
		END)
		,OI.monTotAmount
		,(CASE 
			WHEN I.charItemType='S' AND ISNULL(I.bitExpenseItem,0) = 1 
			THEN (CASE 
					WHEN BH.numBillID IS NULL 
					THEN 0
					ELSE ISNULL(BD.monAmount,0)
				END)
			ELSE (CASE @numCost 
					WHEN 3 THEN ISNULL(OI.numUnitHour,0) * ISNULL(OI.numCost,0)
					WHEN 2 THEN ISNULL(OI.numUnitHour,0) * ISNULL(OI.monVendorCost,0)
					ELSE (CASE 
							WHEN I.charItemType='P' 
							THEN ((ISNULL(OI.numUnitHour,0) - ISNULL(TEMPFulfilledItems.numUnitHour,0)) * ISNULL(OI.monAvgCost,0)) + ISNULL(TEMPFulfilledItems.monCost,0)
							ELSE ISNULL(OI.numUnitHour,0) * ISNULL(OI.monVendorCost,0) 
						END)
				END)
		END)
		,(CASE 
			WHEN I.charItemType='S' AND ISNULL(I.bitExpenseItem,0) = 1 
			THEN (CASE 
					WHEN BH.numBillID IS NULL 
					THEN 1
					ELSE 0
				END)
			ELSE 0
		END)
	FROM 
		OpportunityMaster OM
	INNER JOIN
		OpportunityItems OI
	ON
		OM.numOppId = OI.numOppId
	INNER JOIN
		Item I
	ON
		OI.numItemCode = I.numItemCode
	LEFT JOIN
		UOM
	ON
		OI.numUOMId = UOM.numUOMId
	LEFT JOIN
		BillDetails BD
	ON
		OI.numoppitemtCode = BD.numOppItemID
	LEFT JOIN
		BillHeader BH
	ON
		BD.numBillID = BH.numBillID
	OUTER APPLY
	(
		SELECT 
			SUM(numUnitHour) numUnitHour
			,SUM(ISNULL(OBDI.numUnitHour,0) * ISNULL(OBDI.monAverageCost,0)) monCost
		FROM 
			OpportunityBizDocs OBD
		INNER JOIN
			OpportunityBizDocItems OBDI
		ON
			OBD.numOppBizDocsId = OBDI.numOppBizDocID
		WHERE
			OBD.numOppId = OM.numOppID
			AND OBD.numBizDocId = 296
			AND OBDI.numOppItemID = OI.numoppitemtCode
	) TEMPFulfilledItems
	WHERE
		OM.numDomainId=@numDomainID
		AND OM.tintOppType = 1
		AND OM.tintOppStatus = 1
		AND ISNULL(OI.numProjectID,OM.numProjectID) = @numProId
		

	-- Bills
	INSERT INTO @TEMP
	(
		vcItem
		,vcUnits
		,vcIncomeSource
		,vcExpenseSource
		,monIncome
		,monExpense
		,bitExpensePending
	)
	SELECT
		ISNULL(BD.vcDescription,'')
		,''
		,''
		,(CASE WHEN(BD.numBillID>0) THEN  Case When isnull(BH.bitLandedCost,0)=1 THEN 'Laned Cost-' ELSE 'Bill-' END + CONVERT(VARCHAR(10),BH.numBillID) END)
		,0
		,ISNULL(monAmount,0)
		,0
	FROM	
		BillHeader BH
	INNER JOIN
		BillDetails BD
	ON
		BH.numBillID = BD.numBillID
	WHERE
		BH.numDomainID = @numDomainID
		AND BD.numProjectID = @numProId
		AND ISNULL(BD.numOppItemID,0) = 0

	UPDATE
		@TEMP
	SET 
		vcNetIncome = (CASE 
						WHEN ISNULL(bitExpensePending,0) = 1 THEN CONCAT('<span class="text-orange">',@varCurrSymbol,' ',FORMAT(ISNULL(monIncome,0) - ISNULL(monExpense,0), '#,##0.00###'),'</span>')
						WHEN ISNULL(monIncome,0) - ISNULL(monExpense,0) >= 0 
						THEN CONCAT('<span class="text-green">',@varCurrSymbol,' ',FORMAT(ISNULL(monIncome,0) - ISNULL(monExpense,0), '#,##0.00###'),'</span>') 
						ELSE CONCAT('<span class="text-red">',@varCurrSymbol,' ',FORMAT(ISNULL(monIncome,0) - ISNULL(monExpense,0), '#,##0.00###'),'</span>') 
					END)
		,monNetIncome = ISNULL(monIncome,0) - ISNULL(monExpense,0)
		,vcIncomeExpense = CONCAT(FORMAT(ISNULL(monIncome,0), '#,##0.00###'),(CASE WHEN ISNULL(bitExpensePending,0) = 1 THEN '' ELSE CONCAT(' (',FORMAT(ISNULL(monExpense,0), '#,##0.00###'),')') END))
			
	SELECT * FROM @TEMP

	;WITH CTE AS 
	(SELECT
		ISNULL((SELECT SUM(monIncome) FROM @TEMP),0) AS monIncomeAR
		,ISNULL((SELECT SUM(monExpense) FROM @TEMP),0) AS monNonLaborExpense
		,ISNULL((SELECT SUM(monNetIncome) FROM @TEMP),0) AS monTotalIncome
		,0 AS monExpenseAP
		,ISNULL((SELECT 
					SUM(dbo.GetTimeSpendOnTaskInMinutes(@numDomainID,SPDT.numTaskId) * (ISNULL(SPDT.monHourlyRate,UM.monHourlyRate)/60))
				FROM
					Sales_process_List_Master SPLM
				INNER JOIN
					StagePercentageDetails SPD
				ON
					SPLM.Slp_Id = SPD.slp_id
				INNER JOIN
					StagePercentageDetailsTask SPDT
				ON
					SPD.numStageDetailsId = SPDT.numStageDetailsId
				INNER JOIN
					UserMaster UM
				ON
					SPDT.numAssignTo = UM.numUserDetailId
				WHERE
					SPLM.numdomainid=@numDomainID
					AND SPLM.numProjectId = @numProID
					AND EXISTS (SELECT SPDTTL.ID FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID = SPDT.numTaskId AND tintAction=4)),0) AS monLaborExpense
		,ISNULL((SELECT SUM(monIncome) FROM @TEMP WHERE ISNULL(bitExpensePending,0) = 1),0) AS monIncomePendingExpense)

	SELECT *,ISNULL(monTotalIncome,0) - ISNULL(monLaborExpense,0) AS monGrossProfit FROM CTE
		


--IF @tintMode=0 --Use Order Amounts
--BEGIN
--            SELECT 1 as iTEType,'Sales Order' AS vcTimeExpenseType,I.[vcItemName],
--                    CASE numType
--                      WHEN 1 THEN ISNULL(OPP.numUnitHour, 0)
--                      ELSE ISNULL(CONVERT(VARCHAR(100), CONVERT(DECIMAL(10, 1), ( CONVERT(DECIMAL(10, 1), DATEDIFF(minute, TE.dtFromDate, TE.dtToDate)) / 60 ))),
--                                  0)
--                    END AS [numUnitHour],
--                    ISNULL(OPP.[monPrice], 0) monPrice,
--					ISNULL(OPP.[monPrice], 0) * CASE numType
--                      WHEN 1 THEN ISNULL(OPP.numUnitHour, 0)
--                      ELSE ISNULL(CONVERT(VARCHAR(100), CONVERT(DECIMAL(10, 1), ( CONVERT(DECIMAL(10, 1), DATEDIFF(minute, TE.dtFromDate, TE.dtToDate)) / 60 ))),
--                                  0)
--                    END - (CASE WHEN I.charItemType='P' THEN CASE WHEN ISNULL(Opp.monAvgCost,0)=0 THEN ISNULL(Opp.monVendorCost,0) ELSE ISNULL(Opp.monAvgCost,0) END ELSE 0 END)  AS ExpenseAmount,
--                    dbo.[FormatedDateFromDate](TE.[dtTCreatedOn], @numDomainID) AS dtDateEntered,
--                    dbo.fn_GetContactName(TE.numTCreatedBy) AS vcCreatedBy,TE.numOppId,1 as numOppType,
--					TE.[numCategoryHDRID],isnull(OPP.numProjectStageID,0) as numProjectStageID,0 as numBillId,
--					Case When OPP.numProjectStageID>0 then dbo.fn_GetProjectStageName(OPP.numProjectId,OPP.numProjectStageID) when OPP.numProjectStageID=-1 then 'T&E' else 'S.O.' end as vcFrom,
--					OPP.numProjectId as numProId,TE.[dtTCreatedOn] as dtCreatedOn,isnull((SELECT SUBSTRING((SELECT ',' + Cast(OB.numOppBizDocsId as varchar(18)) from  [OpportunityBizDocs] OB 
--					INNER JOIN [OpportunityBizDocItems] OBI ON OBI.numOppBizDocID = OB.numOppBizDocsId AND OPP.numoppitemtCode = OBI.numOppItemID 
--					WHERE OB.bitAuthoritativeBizDocs=1 and OB.numOppId = OM.numOppId FOR XML PATH('')),2,200000)),'') AS vcBizDoc
--					,0 AS numDepositId,0 AS numCheckHeaderID,0 AS numJournalId,ISNULL(Opp.monAvgCost,0) AS monAvgCost,ISNULL(Opp.monVendorCost,0) AS monVendorCost
--            FROM    [TimeAndExpense] TE
--                    INNER JOIN OpportunityMaster OM ON TE.numOppId = OM.numOppId
--                    INNER JOIN OpportunityItems OPP ON OPP.numOppId = TE.numOppId
--							AND OPP.numoppitemtCode = TE.numOppItemID AND OPP.numProjectId=TE.numProId
--					INNER JOIN Item I ON OPP.[numItemCode] = I.[numItemCode]
--            WHERE   [numProId] = @numProId
--                    AND TE.[numDomainID] = @numDomainID
--                    AND numType =1 --Billable 
--					AND OM.[tintOppType] = 1

--			UNION ALL
	
--			SELECT 1 as iTEType,'Sales Order' AS vcTimeExpenseType,I.[vcItemName],
--                    ISNULL(OPP.numUnitHour, 0) AS [numUnitHour],
--                    ISNULL(OPP.[monPrice], 0) monPrice,
--					ISNULL(OPP.[monPrice], 0) * ISNULL(OPP.numUnitHour, 0) - (CASE WHEN I.charItemType='P' THEN CASE WHEN ISNULL(Opp.monAvgCost,0)=0 THEN ISNULL(Opp.monVendorCost,0) ELSE ISNULL(Opp.monAvgCost,0) END ELSE 0 END)  AS ExpenseAmount,
--                    dbo.[FormatedDateFromDate](OM.bintCreatedDate, @numDomainID) AS dtDateEntered,
--                    dbo.fn_GetContactName(OM.numCreatedBy) AS vcCreatedBy,OM.numOppId,1 as numOppType,
--					0 as numCategoryHDRID,isnull(OPP.numProjectStageID,0) as numProjectStageID,0 as numBillId,
--					Case When OPP.numProjectStageID>0 then dbo.fn_GetProjectStageName(OPP.numProjectId,OPP.numProjectStageID) when OPP.numProjectStageID=-1 then 'T&E' else 'S.O.' end as vcFrom,
--					OPP.numProjectId as numProId,OM.bintCreatedDate as dtCreatedOn,isnull((SELECT SUBSTRING((SELECT ',' + Cast(OB.numOppBizDocsId as varchar(18)) from  [OpportunityBizDocs] OB 
--					INNER JOIN [OpportunityBizDocItems] OBI ON OBI.numOppBizDocID = OB.numOppBizDocsId AND OPP.numoppitemtCode = OBI.numOppItemID 
--					WHERE OB.bitAuthoritativeBizDocs=1 and OB.numOppId = OM.numOppId FOR XML PATH('')),2,200000)),'') AS vcBizDoc,0 AS numDepositId,0 AS numCheckHeaderID,0 AS numJournalId
--					,ISNULL(Opp.monAvgCost,0) AS monAvgCost,ISNULL(Opp.monVendorCost,0) AS monVendorCost
--            FROM    OpportunityMaster OM INNER JOIN OpportunityItems OPP ON OPP.numOppId = OM.numOppId
--					INNER JOIN Item I ON OPP.[numItemCode] = I.[numItemCode]
--            WHERE  OM.[numDomainID] = @numDomainID AND OPP.numProjectID = @numProId
--                    AND OPP.numProjectStageID=0 AND OM.[tintOppType] = 1

--			UNION ALL
			
--			SELECT  2 as iTEType,'Non-Billable Time' AS vcTimeExpenseType,
--                    ISNULL(CONVERT(VARCHAR(25),txtDesc) + '..' ,'') AS [vcItemName],
--                    ISNULL(CONVERT(VARCHAR(100), CONVERT(DECIMAL(10, 1), ( CONVERT(DECIMAL(10, 1), DATEDIFF(minute, TE.dtFromDate, TE.dtToDate)) / 60 ))),0)
--                    AS [numUnitHour],
--                    0 monPrice,
--					0 AS ExpenseAmount,
--                    dbo.[FormatedDateFromDate](TE.[dtTCreatedOn], @numDomainID) AS dtDateEntered,
--                    dbo.fn_GetContactName(TE.numTCreatedBy) AS vcCreatedBy,0 as numOppId,0 as numOppType,
--					TE.[numCategoryHDRID],isnull(TE.numStageId,0) as numProjectStageID,0 as numBillId,
--					Case When TE.numStageId>0 then dbo.fn_GetProjectStageName(TE.numProId,TE.numStageId) else 'Time' end as vcFrom,
--					TE.numProId,TE.[dtTCreatedOn] as dtCreatedOn,'' as vcBizDoc,0 AS numDepositId,0 AS numCheckHeaderID,0 AS numJournalId
--					,0 AS monAvgCost,0 AS monVendorCost
--            FROM    [TimeAndExpense] TE
--            WHERE   [numProId] = @numProId
--                    AND TE.[numDomainID] = @numDomainID
--                    AND numType =2 --Non Billable

--		    UNION ALL

--            select 3 as iTEType,'Purchase Order' AS vcTimeExpenseType,I.[vcItemName],
--                    ISNULL(OPP.numUnitHour, 0) as numUnitHour,
--                    ISNULL(OPP.[monPrice], 0) monPrice,
--	   			    (OPP.[monPrice] * OPP.[numUnitHour]) AS ExpenseAmount,
--					dbo.[FormatedDateFromDate](OM.bintCreatedDate, @numDomainID) AS dtDateEntered,
--                    dbo.fn_GetContactName(OM.numCreatedBy) AS vcCreatedBy,OM.numOppId,2 as numOppType,
--					0 as numCategoryHDRID,isnull(OPP.numProjectStageID,0) as numProjectStageID,0 as numBillId,
--					Case When OPP.numProjectStageID>0 then dbo.fn_GetProjectStageName(OPP.numProjectId,OPP.numProjectStageID) else 'P.O.' end as vcFrom,
--					OPP.numProjectID as numProId,OM.bintCreatedDate as dtCreatedOn,isnull((SELECT SUBSTRING((SELECT ',' + Cast(OB.numOppBizDocsId as varchar(18)) from  [OpportunityBizDocs] OB 
--					INNER JOIN [OpportunityBizDocItems] OBI ON OBI.numOppBizDocID = OB.numOppBizDocsId AND OPP.numoppitemtCode = OBI.numOppItemID 
--					WHERE OB.bitAuthoritativeBizDocs=1 and OB.numOppId = OM.numOppId FOR XML PATH('')),2,200000)),'') AS vcBizDoc,0 AS numDepositId,0 AS numCheckHeaderID,0 AS numJournalId
--					,ISNULL(Opp.monAvgCost,0) AS monAvgCost,ISNULL(Opp.monVendorCost,0) AS monVendorCost
--			 from  [OpportunityMaster] OM 
--						JOIN [OpportunityItems] OPP ON OM.[numOppId] = OPP.[numOppId]
--                        LEFT OUTER JOIN [Item] I ON OPP.[numItemCode] = I.[numItemCode]
--						LEFT OUTER JOIN [Chart_Of_Accounts] COA ON I.[numCOGsChartAcntId] = COA.[numAccountId]
--			 where  OM.numDomainId=@numDomainID  
--					AND OPP.numProjectID = @numProId
--					AND OM.[tintOppType] = 2	

--			 Union ALL

--			select 4 as iTEType,'Bill' as vcTimeExpenseType,BD.vcDescription as vcItemName,0 as numUnitHour,0 as monPrice,BD.monAmount AS ExpenseAmount,
--				dbo.[FormatedDateFromDate](BH.dtCreatedDate, @numDomainID) AS dtDateEntered,
--                    dbo.fn_GetContactName(BH.numCreatedBy) AS vcCreatedBy,0 as numOppId,0 as numOppType,
--					0 as numCategoryHDRID,isnull(PO.numStageID,0) as numProjectStageID,PO.numBillId,
--					Case When PO.numStageID>0 then dbo.fn_GetProjectStageName(PO.numProID,PO.numStageID) else 'Bill' end as vcFrom,
--					PO.numProID,BH.dtCreatedDate as dtCreatedOn,'' as vcBizDoc,0 AS numDepositId,0 AS numCheckHeaderID,0 AS numJournalId
--					,0 AS monAvgCost,0 AS monVendorCost
--			 from  ProjectsOpportunities PO 
--				   JOIN BillHeader BH ON BH.numBillID = PO.numBillID
--				   JOIN BillDetails BD ON BH.numBillID=BD.numBillID AND PO.numProID=BD.numProjectID
--			 where  PO.numDomainId=@numDomainID  
--					AND PO.numProID = @numProId
--					and isnull(PO.numBillId,0)>0
					
--			Union ALL

--			select 4 as iTEType,'Bill' as vcTimeExpenseType,BD.vcDescription as vcItemName,0 as numUnitHour,0 as monPrice,BD.monAmount AS ExpenseAmount,
--				dbo.[FormatedDateFromDate](BH.dtCreatedDate, @numDomainID) AS dtDateEntered,
--                    dbo.fn_GetContactName(BH.numCreatedBy) AS vcCreatedBy,0 as numOppId,0 as numOppType,
--					0 as numCategoryHDRID,0 as numProjectStageID,BD.numBillId,
--					'Bill' as vcFrom,
--					BD.numProjectID,BH.dtCreatedDate as dtCreatedOn,'' as vcBizDoc,0 AS numDepositId,0 AS numCheckHeaderID,0 AS numJournalId
--					,0 AS monAvgCost,0 AS monVendorCost
--			 from  BillHeader BH 
--				   JOIN BillDetails BD ON BH.numBillID=BD.numBillID 
--			 where  BH.numDomainId=@numDomainID  
--					AND BD.numProjectID = @numProId
							
			
--			UNION ALL

--			SELECT  5 as iTEType,'Employee Expense' AS vcTimeExpenseType,
--                    ISNULL(CONVERT(VARCHAR(25),txtDesc) + '..' ,'') AS [vcItemName],
--                    ISNULL(CONVERT(VARCHAR(100), CONVERT(DECIMAL(10, 1), ( CONVERT(DECIMAL(10, 1), DATEDIFF(minute, TE.dtFromDate, TE.dtToDate)) / 60 ))),0)
--                    AS [numUnitHour],
--                    isnull(UM.monHourlyRate,0) monPrice,
--					isnull(Cast(datediff(minute,TE.dtfromdate,dttodate) as float)/Cast(60 as float),0) * isnull(UM.monHourlyRate,0) AS ExpenseAmount,
--                    dbo.[FormatedDateFromDate](TE.[dtTCreatedOn], @numDomainID) AS dtDateEntered,
--                    dbo.fn_GetContactName(TE.numTCreatedBy) AS vcCreatedBy,0 as numOppId,0 as numOppType,
--					TE.[numCategoryHDRID],isnull(TE.numStageId,0) as numProjectStageID,0 as numBillId,
--					Case When TE.numStageId>0 then dbo.fn_GetProjectStageName(TE.numProId,TE.numStageId) else 'Expense' end as vcFrom,
--					TE.numProId,TE.[dtTCreatedOn] as dtCreatedOn,'' as vcBizDoc,0 AS numDepositId,0 AS numCheckHeaderID,0 AS numJournalId
--					,0 AS monAvgCost,0 AS monVendorCost
--            FROM    [TimeAndExpense] TE join UserMaster UM on UM.numUserDetailId=TE.numUserCntID
--            WHERE   [numProId] = @numProId
--                    AND TE.[numDomainID] = @numDomainID
--                    AND numType in (1,2) --Non Billable & Billable
--					AND TE.numUserCntID>0
----					AND bitReimburse=1 and  numCategory=2  

--			UNION ALL
			
--			select 6 as iTEType,'Deposit' as vcTimeExpenseType,DED.vcMemo as vcItemName,0 as numUnitHour,0 as monPrice,DED.monAmountPaid AS ExpenseAmount,
--				dbo.[FormatedDateFromDate](DEM.dtCreationDate, @numDomainID) AS dtDateEntered,
--                    dbo.fn_GetContactName(DEM.numCreatedBy) AS vcCreatedBy,0 as numOppId,0 as numOppType,
--					0 as numCategoryHDRID,0 as numProjectStageID,0 AS numBillId,
--					'' as vcFrom,
--					DED.numProjectID AS numProID,DEM.dtCreationDate as dtCreatedOn,'' as vcBizDoc,DEM.numDepositId,0 AS numCheckHeaderID,0 AS numJournalId
--					,0 AS monAvgCost,0 AS monVendorCost
--			 from  DepositMaster DEM JOIN DepositeDetails DED ON DEM.numDepositID=DED.numDepositID
--			 WHERE DEM.tintDepositePage=1 AND DEM.numDomainId=@numDomainID  
--					AND DED.numProjectID = @numProId
			
--			UNION ALL
					
--			select 7 as iTEType,'Check' as vcTimeExpenseType,CD.vcDescription as vcItemName,0 as numUnitHour,0 as monPrice,CD.monAmount AS ExpenseAmount,
--				dbo.[FormatedDateFromDate](CH.dtCreatedDate, @numDomainID) AS dtDateEntered,
--                    dbo.fn_GetContactName(CH.numCreatedBy) AS vcCreatedBy,0 as numOppId,0 as numOppType,
--					0 as numCategoryHDRID,0 as numProjectStageID,0 AS numBillId,
--					'' as vcFrom,
--					CD.numProjectID AS numProID,CH.dtCreatedDate as dtCreatedOn,'' as vcBizDoc,0 AS numDepositId,CH.numCheckHeaderID,0 AS numJournalId
--					,0 AS monAvgCost,0 AS monVendorCost
--			 from  CheckHeader CH JOIN CheckDetails CD ON CH.numCheckHeaderID=CD.numCheckHeaderID
--			 WHERE CH.tintReferenceType=1 AND CH.numDomainId=@numDomainID  
--					AND CD.numProjectID = @numProId		
			
--			UNION ALL
			
--			select 9 as iTEType,'Journal' as vcTimeExpenseType,GD.varDescription as vcItemName,0 as numUnitHour,0 as monPrice,GD.numDebitAmt AS ExpenseAmount,
--				dbo.[FormatedDateFromDate](GH.datCreatedDate, @numDomainID) AS dtDateEntered,
--                    dbo.fn_GetContactName(GH.numCreatedBy) AS vcCreatedBy,0 as numOppId,0 as numOppType,
--					0 as numCategoryHDRID,0 as numProjectStageID,0 AS numBillId,
--					'' as vcFrom,
--					GD.numProjectID AS numProID,GH.datCreatedDate as dtCreatedOn,'' as vcBizDoc,0 AS numDepositId,0 AS numCheckHeaderID,GD.numJournalId
--					,0 AS monAvgCost,0 AS monVendorCost
--			 from  dbo.General_Journal_Header GH JOIN dbo.General_Journal_Details GD ON GH.numJournal_Id = GD.numJournalId
--					JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = GD.numChartAcntId
--			 WHERE ISNULL(GH.numCheckHeaderID,0)=0 AND ISNULL(GH.numBillPaymentID,0)=0 AND ISNULL(GH.numBillID,0)=0
--					AND ISNULL(GH.numDepositId,0)=0 AND ISNULL(GH.numOppId,0)=0 AND ISNULL(GH.numOppBizDocsId,0)=0
--					AND ISNULL(GH.numReturnId,0)=0
--					AND GH .numDomainId=@numDomainID  AND GD.numProjectID = @numProId
--					AND ISNULL(GD.numDebitAmt,0)>0	AND (COA.vcAccountCode LIKE '0103%' OR  COA.vcAccountCode LIKE '0104%' OR  COA.vcAccountCode LIKE '0106%')	
				
--			UNION ALL
					
--			 select 8 as iTEType,'Journal' as vcTimeExpenseType,GD.varDescription as vcItemName,0 as numUnitHour,0 as monPrice,GD.numCreditAmt AS ExpenseAmount,
--				dbo.[FormatedDateFromDate](GH.datCreatedDate, @numDomainID) AS dtDateEntered,
--                    dbo.fn_GetContactName(GH.numCreatedBy) AS vcCreatedBy,0 as numOppId,0 as numOppType,
--					0 as numCategoryHDRID,0 as numProjectStageID,0 AS numBillId,
--					'' as vcFrom,
--					GD.numProjectID AS numProID,GH.datCreatedDate as dtCreatedOn,'' as vcBizDoc,0 AS numDepositId,0 AS numCheckHeaderID,GD.numJournalId
--					,0 AS monAvgCost,0 AS monVendorCost
--			 from  dbo.General_Journal_Header GH JOIN dbo.General_Journal_Details GD ON GH.numJournal_Id = GD.numJournalId
--					JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = GD.numChartAcntId
--			 WHERE ISNULL(GH.numCheckHeaderID,0)=0 AND ISNULL(GH.numBillPaymentID,0)=0 AND ISNULL(GH.numBillID,0)=0
--					AND ISNULL(GH.numDepositId,0)=0 AND ISNULL(GH.numOppId,0)=0 AND ISNULL(GH.numOppBizDocsId,0)=0
--					AND ISNULL(GH.numReturnId,0)=0
--					AND GH .numDomainId=@numDomainID  AND GD.numProjectID = @numProId
--					AND ISNULL(GD.numCreditAmt,0)>0  AND (COA.vcAccountCode LIKE '0103%' OR  COA.vcAccountCode LIKE '0104%' OR  COA.vcAccountCode LIKE '0106%')
					
								
--			order by dtCreatedOn
-- END 

--ELSE IF @tintMode=1 --Use Auth BizDoc Amounts
--BEGIN
--            SELECT 1 as iTEType,'Sales Order' AS vcTimeExpenseType,I.[vcItemName],
--                    ISNULL(X.[numUnitHour], 0) AS [numUnitHour],
--                    ISNULL(OPP.[monPrice], 0) monPrice,
--					ISNULL(OPP.[monPrice], 0) * ISNULL(X.numUnitHour, 0) - (CASE WHEN I.charItemType='P' THEN CASE WHEN ISNULL(Opp.monAvgCost,0)=0 THEN ISNULL(Opp.monVendorCost,0) ELSE ISNULL(Opp.monAvgCost,0) END ELSE 0 END) AS ExpenseAmount,
--                    dbo.[FormatedDateFromDate](TE.[dtTCreatedOn], @numDomainID) AS dtDateEntered,
--                    dbo.fn_GetContactName(TE.numTCreatedBy) AS vcCreatedBy,TE.numOppId,1 as numOppType,
--					TE.[numCategoryHDRID],isnull(OPP.numProjectStageID,0) as numProjectStageID,0 as numBillId,
--					Case When OPP.numProjectStageID>0 then dbo.fn_GetProjectStageName(OPP.numProjectId,OPP.numProjectStageID) when OPP.numProjectStageID=-1 then 'T&E' else 'S.O.' end as vcFrom,
--					OPP.numProjectId as numProId,TE.[dtTCreatedOn] as dtCreatedOn,isnull((SELECT SUBSTRING((SELECT ',' + Cast(OB.numOppBizDocsId as varchar(18)) from  [OpportunityBizDocs] OB 
--					INNER JOIN [OpportunityBizDocItems] OBI ON OBI.numOppBizDocID = OB.numOppBizDocsId AND OPP.numoppitemtCode = OBI.numOppItemID 
--					WHERE OB.bitAuthoritativeBizDocs=1 and OB.numOppId = OM.numOppId FOR XML PATH('')),2,200000)),'') AS vcBizDoc,0 AS numDepositId,0 AS numCheckHeaderID,0 AS numJournalId
--					,ISNULL(Opp.monAvgCost,0) AS monAvgCost,ISNULL(Opp.monVendorCost,0) AS monVendorCost
--            FROM    [TimeAndExpense] TE
--                    INNER JOIN OpportunityMaster OM ON TE.numOppId = OM.numOppId
--                    INNER JOIN OpportunityItems OPP ON OPP.numOppId = TE.numOppId
--							AND OPP.numoppitemtCode = TE.numOppItemID AND OPP.numProjectId=TE.numProId
--					INNER JOIN Item I ON OPP.[numItemCode] = I.[numItemCode]
--					Inner Join (select sum(ISNULL(OBI.[numUnitHour], 0)) as numUnitHour,OM.numOppId,OPP.numoppitemtCode from  [TimeAndExpense] TE
--                    INNER JOIN OpportunityMaster OM ON TE.numOppId = OM.numOppId
--                    INNER JOIN OpportunityItems OPP ON OPP.numOppId = TE.numOppId
--							AND OPP.numoppitemtCode = TE.numOppItemID AND OPP.numProjectId=TE.numProId
--					INNER JOIN [OpportunityBizDocs] OB ON OB.numOppId = OM.numOppId
--					INNER JOIN [OpportunityBizDocItems] OBI ON OBI.numOppBizDocID = OB.numOppBizDocsId 
--						AND OPP.numoppitemtCode = OBI.numOppItemID 
--					WHERE [numProId] = @numProId AND TE.[numDomainID] = @numDomainID AND numType =1 and OB.bitAuthoritativeBizDocs=1
--						group by OM.numOppId,OPP.numoppitemtCode) X on
--					X.numOppId = OM.numOppId and OPP.numoppitemtCode = X.numoppitemtCode
--            WHERE   [numProId] = @numProId
--                    AND TE.[numDomainID] = @numDomainID
--                    AND numType =1 --Billable 
--					AND OM.[tintOppType] = 1

--			UNION ALL
			
--			SELECT 1 as iTEType,'Sales Order' AS vcTimeExpenseType,I.[vcItemName],
--                    ISNULL(X.numUnitHour, 0) AS [numUnitHour],
--                    ISNULL(OPP.[monPrice], 0) monPrice,
--					ISNULL(OPP.[monPrice], 0) * ISNULL(X.numUnitHour, 0) - (CASE WHEN I.charItemType='P' THEN CASE WHEN ISNULL(Opp.monAvgCost,0)=0 THEN ISNULL(Opp.monVendorCost,0) ELSE ISNULL(Opp.monAvgCost,0) END ELSE 0 END)  AS ExpenseAmount,
--                    dbo.[FormatedDateFromDate](OM.bintCreatedDate, @numDomainID) AS dtDateEntered,
--                    dbo.fn_GetContactName(OM.numCreatedBy) AS vcCreatedBy,OM.numOppId,1 as numOppType,
--					0 as numCategoryHDRID,isnull(OPP.numProjectStageID,0) as numProjectStageID,0 as numBillId,
--					Case When OPP.numProjectStageID>0 then dbo.fn_GetProjectStageName(OPP.numProjectId,OPP.numProjectStageID) when OPP.numProjectStageID=-1 then 'T&E' else 'S.O.' end as vcFrom,
--					OPP.numProjectId as numProId,OM.bintCreatedDate as dtCreatedOn,isnull((SELECT SUBSTRING((SELECT ',' + Cast(OB.numOppBizDocsId as varchar(18)) from  [OpportunityBizDocs] OB 
--					INNER JOIN [OpportunityBizDocItems] OBI ON OBI.numOppBizDocID = OB.numOppBizDocsId AND OPP.numoppitemtCode = OBI.numOppItemID 
--					WHERE OB.bitAuthoritativeBizDocs=1 and OB.numOppId = OM.numOppId FOR XML PATH('')),2,200000)),'') AS vcBizDoc,0 AS numDepositId,0 AS numCheckHeaderID,0 AS numJournalId
--					,ISNULL(Opp.monAvgCost,0) AS monAvgCost,ISNULL(Opp.monVendorCost,0) AS monVendorCost
--            FROM    OpportunityMaster OM INNER JOIN OpportunityItems OPP ON OPP.numOppId = OM.numOppId
--					INNER JOIN Item I ON OPP.[numItemCode] = I.[numItemCode]
--					Inner Join (select sum(ISNULL(OBI.[numUnitHour], 0)) as numUnitHour,OM.numOppId,OPP.numoppitemtCode from  
--                    OpportunityMaster OM INNER JOIN OpportunityItems OPP ON OPP.numOppId = OM.numOppId
--					INNER JOIN [OpportunityBizDocs] OB ON OB.numOppId = OM.numOppId
--					INNER JOIN [OpportunityBizDocItems] OBI ON OBI.numOppBizDocID = OB.numOppBizDocsId 
--						AND OPP.numoppitemtCode = OBI.numOppItemID 
--					WHERE OM.[numDomainID] = @numDomainID AND OPP.numProjectID = @numProId AND OPP.numProjectStageID=0 and OB.bitAuthoritativeBizDocs=1
--						group by OM.numOppId,OPP.numoppitemtCode) X on
--					X.numOppId = OM.numOppId and OPP.numoppitemtCode = X.numoppitemtCode
--            WHERE  OM.[numDomainID] = @numDomainID AND OPP.numProjectID = @numProId
--                    AND OPP.numProjectStageID=0 AND OM.[tintOppType] = 1

--			Union ALL

--			SELECT  2 as iTEType,'Non-Billable Time' AS vcTimeExpenseType,
--                    ISNULL(CONVERT(VARCHAR(25),txtDesc) + '..' ,'') AS [vcItemName],
--                    ISNULL(CONVERT(VARCHAR(100), CONVERT(DECIMAL(10, 1), ( CONVERT(DECIMAL(10, 1), DATEDIFF(minute, TE.dtFromDate, TE.dtToDate)) / 60 ))),0)
--                    AS [numUnitHour],
--                    0 monPrice,
--					0 AS ExpenseAmount,
--                    dbo.[FormatedDateFromDate](TE.[dtTCreatedOn], @numDomainID) AS dtDateEntered,
--                    dbo.fn_GetContactName(TE.numTCreatedBy) AS vcCreatedBy,0 as numOppId,0 as numOppType,
--					TE.[numCategoryHDRID],isnull(TE.numStageId,0) as numProjectStageID,0 as numBillId,
--					Case When TE.numStageId>0 then dbo.fn_GetProjectStageName(TE.numProId,TE.numStageId) else 'Time' end as vcFrom,
--					TE.numProId,TE.[dtTCreatedOn] as dtCreatedOn,'' as vcBizDoc,0 AS numDepositId,0 AS numCheckHeaderID,0 AS numJournalId
--					,0 AS monAvgCost,0 AS monVendorCost
--            FROM    [TimeAndExpense] TE
--            WHERE   [numProId] = @numProId
--                    AND TE.[numDomainID] = @numDomainID
--                    AND numType =2 --Non Billable

--		    UNION ALL

--            select 3 as iTEType,'Purchase Order' AS vcTimeExpenseType,I.[vcItemName],
--                    ISNULL(X.[numUnitHour], 0) as numUnitHour,
--                    ISNULL(OPP.[monPrice], 0) monPrice,
--	   			    (OPP.[monPrice] * X.[numUnitHour]) AS ExpenseAmount,
--					dbo.[FormatedDateFromDate](OM.bintCreatedDate, @numDomainID) AS dtDateEntered,
--                    dbo.fn_GetContactName(OM.numCreatedBy) AS vcCreatedBy,OM.numOppId,2 as numOppType,
--					0 as numCategoryHDRID,isnull(OPP.numProjectStageID,0) as numProjectStageID,0 as numBillId,
--					Case When OPP.numProjectStageID>0 then dbo.fn_GetProjectStageName(OPP.numProjectId,OPP.numProjectStageID) else 'P.O.' end as vcFrom,
--					OPP.numProjectID as numProId,OM.bintCreatedDate as dtCreatedOn,isnull((SELECT SUBSTRING((SELECT ',' + Cast(OB.numOppBizDocsId as varchar(18)) from  [OpportunityBizDocs] OB 
--					INNER JOIN [OpportunityBizDocItems] OBI ON OBI.numOppBizDocID = OB.numOppBizDocsId AND OPP.numoppitemtCode = OBI.numOppItemID 
--					WHERE OB.bitAuthoritativeBizDocs=1 and OB.numOppId = OM.numOppId FOR XML PATH('')),2,200000)),'') AS vcBizDoc,0 AS numDepositId,0 AS numCheckHeaderID,0 AS numJournalId
--					,ISNULL(Opp.monAvgCost,0) AS monAvgCost,ISNULL(Opp.monVendorCost,0) AS monVendorCost
--			 from  [OpportunityMaster] OM 
--						JOIN [OpportunityItems] OPP ON OM.[numOppId] = OPP.[numOppId]
--                        LEFT OUTER JOIN [Item] I ON OPP.[numItemCode] = I.[numItemCode]
--						LEFT OUTER JOIN [Chart_Of_Accounts] COA ON I.[numCOGsChartAcntId] = COA.[numAccountId]
--						Inner Join (select sum(ISNULL(OBI.[numUnitHour], 0)) as numUnitHour,OM.numOppId,OPP.numoppitemtCode from  
--                    OpportunityMaster OM 
--                    JOIN [OpportunityItems] OPP ON OM.[numOppId] = OPP.[numOppId]
--					INNER JOIN [OpportunityBizDocs] OB ON OB.numOppId = OM.numOppId
--					INNER JOIN [OpportunityBizDocItems] OBI ON OBI.numOppBizDocID = OB.numOppBizDocsId 
--						AND OPP.numoppitemtCode = OBI.numOppItemID 
--					WHERE OM.numDomainId=@numDomainID AND OPP.numProjectID = @numProId AND OM.[tintOppType] = 2 and OB.bitAuthoritativeBizDocs=1
--						group by OM.numOppId,OPP.numoppitemtCode) X on
--					X.numOppId = OM.numOppId and OPP.numoppitemtCode = X.numoppitemtCode
--			 where  OM.numDomainId=@numDomainID  
--					AND OPP.numProjectID = @numProId
--					AND OM.[tintOppType] = 2

--			 Union ALL
			
--			select 4 as iTEType,'Bill' as vcTimeExpenseType,BD.vcDescription as vcItemName,0 as numUnitHour,0 as monPrice,BD.monAmount AS ExpenseAmount,
--				dbo.[FormatedDateFromDate](BH.dtCreatedDate, @numDomainID) AS dtDateEntered,
--                    dbo.fn_GetContactName(BH.numCreatedBy) AS vcCreatedBy,0 as numOppId,0 as numOppType,
--					0 as numCategoryHDRID,isnull(PO.numStageID,0) as numProjectStageID,PO.numBillId,
--					Case When PO.numStageID>0 then dbo.fn_GetProjectStageName(PO.numProID,PO.numStageID) else 'Bill' end as vcFrom,
--					PO.numProID,BH.dtCreatedDate as dtCreatedOn,'' as vcBizDoc,0 AS numDepositId,0 AS numCheckHeaderID,0 AS numJournalId
--					,0 AS monAvgCost,0 AS monVendorCost
--			 from  ProjectsOpportunities PO 
--				   JOIN BillHeader BH ON BH.numBillID = PO.numBillID
--				   JOIN BillDetails BD ON BH.numBillID=BD.numBillID AND PO.numProID=BD.numProjectID
--			 where  PO.numDomainId=@numDomainID  
--					AND PO.numProID = @numProId
--					and isnull(PO.numBillId,0)>0
			
--			UNION ALL

--			SELECT  5 as iTEType,'Employee Expense' AS vcTimeExpenseType,
--                    ISNULL(CONVERT(VARCHAR(25),txtDesc) + '..' ,'') AS [vcItemName],
--                    ISNULL(CONVERT(VARCHAR(100), CONVERT(DECIMAL(10, 1), ( CONVERT(DECIMAL(10, 1), DATEDIFF(minute, TE.dtFromDate, TE.dtToDate)) / 60 ))),0)
--                    AS [numUnitHour],
--                    isnull(UM.monHourlyRate,0) monPrice,
--					isnull(Cast(datediff(minute,TE.dtfromdate,dttodate) as float)/Cast(60 as float),0) * isnull(UM.monHourlyRate,0) AS ExpenseAmount,
--                    dbo.[FormatedDateFromDate](TE.[dtTCreatedOn], @numDomainID) AS dtDateEntered,
--                    dbo.fn_GetContactName(TE.numTCreatedBy) AS vcCreatedBy,0 as numOppId,0 as numOppType,
--					TE.[numCategoryHDRID],isnull(TE.numStageId,0) as numProjectStageID,0 as numBillId,
--					Case When TE.numStageId>0 then dbo.fn_GetProjectStageName(TE.numProId,TE.numStageId) else 'Expense' end as vcFrom,
--					TE.numProId,TE.[dtTCreatedOn] as dtCreatedOn,'' as vcBizDoc,0 AS numDepositId,0 AS numCheckHeaderID,0 AS numJournalId
--					,0 AS monAvgCost,0 AS monVendorCost
--            FROM    [TimeAndExpense] TE join UserMaster UM on UM.numUserDetailId=TE.numUserCntID
--            WHERE   [numProId] = @numProId
--                    AND TE.[numDomainID] = @numDomainID
--                    AND numType in (1,2) --Non Billable & Billable
--					AND TE.numUserCntID>0
----					AND bitReimburse=1 and  numCategory=2  

--			select 6 as iTEType,'Deposit' as vcTimeExpenseType,DED.vcMemo as vcItemName,0 as numUnitHour,0 as monPrice,DED.monAmountPaid AS ExpenseAmount,
--				dbo.[FormatedDateFromDate](DEM.dtCreationDate, @numDomainID) AS dtDateEntered,
--                    dbo.fn_GetContactName(DEM.numCreatedBy) AS vcCreatedBy,0 as numOppId,0 as numOppType,
--					0 as numCategoryHDRID,0 as numProjectStageID,0 AS numBillId,
--					'' as vcFrom,
--					DED.numProjectID AS numProID,DEM.dtCreationDate as dtCreatedOn,'' as vcBizDoc,DEM.numDepositId,0 AS numCheckHeaderID,0 AS numJournalId
--					,0 AS monAvgCost,0 AS monVendorCost
--			 from  DepositMaster DEM JOIN DepositeDetails DED ON DEM.numDepositID=DED.numDepositID
--			 WHERE DEM.tintDepositePage=1 AND DEM.numDomainId=@numDomainID  
--					AND DED.numProjectID = @numProId
				
--			UNION ALL
					
--			select 7 as iTEType,'Check' as vcTimeExpenseType,CD.vcDescription as vcItemName,0 as numUnitHour,0 as monPrice,CD.monAmount AS ExpenseAmount,
--				dbo.[FormatedDateFromDate](CH.dtCreatedDate, @numDomainID) AS dtDateEntered,
--                    dbo.fn_GetContactName(CH.numCreatedBy) AS vcCreatedBy,0 as numOppId,0 as numOppType,
--					0 as numCategoryHDRID,0 as numProjectStageID,0 AS numBillId,
--					'' as vcFrom,
--					CD.numProjectID AS numProID,CH.dtCreatedDate as dtCreatedOn,'' as vcBizDoc,0 AS numDepositId,CH.numCheckHeaderID,0 AS numJournalId
--					,0 AS monAvgCost,0 AS monVendorCost
--			 from  CheckHeader CH JOIN CheckDetails CD ON CH.numCheckHeaderID=CD.numCheckHeaderID
--			 WHERE CH.tintReferenceType=1 AND CH.numDomainId=@numDomainID  
--					AND CD.numProjectID = @numProId		
			
--			UNION ALL
			
--			select 9 as iTEType,'Journal' as vcTimeExpenseType,GD.varDescription as vcItemName,0 as numUnitHour,0 as monPrice,GD.numDebitAmt AS ExpenseAmount,
--				dbo.[FormatedDateFromDate](GH.datCreatedDate, @numDomainID) AS dtDateEntered,
--                    dbo.fn_GetContactName(GH.numCreatedBy) AS vcCreatedBy,0 as numOppId,0 as numOppType,
--					0 as numCategoryHDRID,0 as numProjectStageID,0 AS numBillId,
--					'' as vcFrom,
--					GD.numProjectID AS numProID,GH.datCreatedDate as dtCreatedOn,'' as vcBizDoc,0 AS numDepositId,0 AS numCheckHeaderID,GD.numJournalId
--					,0 AS monAvgCost,0 AS monVendorCost
--			 from  dbo.General_Journal_Header GH JOIN dbo.General_Journal_Details GD ON GH.numJournal_Id = GD.numJournalId
--					JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = GD.numChartAcntId
--			 WHERE ISNULL(GH.numCheckHeaderID,0)=0 AND ISNULL(GH.numBillPaymentID,0)=0 AND ISNULL(GH.numBillID,0)=0
--					AND ISNULL(GH.numDepositId,0)=0 AND ISNULL(GH.numOppId,0)=0 AND ISNULL(GH.numOppBizDocsId,0)=0
--					AND ISNULL(GH.numReturnId,0)=0
--					AND GH .numDomainId=@numDomainID  AND GD.numProjectID = @numProId
--					AND ISNULL(GD.numDebitAmt,0)>0	AND (COA.vcAccountCode LIKE '0103%' OR  COA.vcAccountCode LIKE '0104%' OR  COA.vcAccountCode LIKE '0106%')	
				
--			UNION ALL
					
--			 select 8 as iTEType,'Journal' as vcTimeExpenseType,GD.varDescription as vcItemName,0 as numUnitHour,0 as monPrice,GD.numCreditAmt AS ExpenseAmount,
--				dbo.[FormatedDateFromDate](GH.datCreatedDate, @numDomainID) AS dtDateEntered,
--                    dbo.fn_GetContactName(GH.numCreatedBy) AS vcCreatedBy,0 as numOppId,0 as numOppType,
--					0 as numCategoryHDRID,0 as numProjectStageID,0 AS numBillId,
--					'' as vcFrom,
--					GD.numProjectID AS numProID,GH.datCreatedDate as dtCreatedOn,'' as vcBizDoc,0 AS numDepositId,0 AS numCheckHeaderID,GD.numJournalId
--					,0 AS monAvgCost,0 AS monVendorCost
--			 from  dbo.General_Journal_Header GH JOIN dbo.General_Journal_Details GD ON GH.numJournal_Id = GD.numJournalId
--					JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = GD.numChartAcntId
--			 WHERE ISNULL(GH.numCheckHeaderID,0)=0 AND ISNULL(GH.numBillPaymentID,0)=0 AND ISNULL(GH.numBillID,0)=0
--					AND ISNULL(GH.numDepositId,0)=0 AND ISNULL(GH.numOppId,0)=0 AND ISNULL(GH.numOppBizDocsId,0)=0
--					AND ISNULL(GH.numReturnId,0)=0
--					AND GH .numDomainId=@numDomainID  AND GD.numProjectID = @numProId
--					AND ISNULL(GD.numCreditAmt,0)>0  AND (COA.vcAccountCode LIKE '0103%' OR  COA.vcAccountCode LIKE '0104%' OR  COA.vcAccountCode LIKE '0106%')
									
--			order by dtCreatedOn
-- END 
	   
END        
        

/****** Object:  StoredProcedure [dbo].[usp_GetRecentItems]    Script Date: 07/26/2008 16:18:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj  
--EXEC usp_GetRecentItems 1 
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getrecentitems')
DROP PROCEDURE usp_getrecentitems
GO
CREATE PROCEDURE [dbo].[usp_GetRecentItems]
(   
	@numUserCntID as numeric(9)      
)
AS
BEGIN
	IF @numUserCntID = 0
	BEGIN
		--get blank table to avoid error
		select 0 numRecId 
		from communication    
		where  1=0
		RETURN
	END
	

	declare @v1 int
	if exists(select  ISNULL([intLastViewedRecord],10) FROM [Domain] WHERE [numDomainId] IN (SELECT numDomainID FROM [AdditionalContactsInformation] WHERE [numContactId]=@numUserCntID))
		begin
			select @v1 = ISNULL([intLastViewedRecord],10) FROM [Domain] WHERE [numDomainId] IN (SELECT numDomainID FROM [AdditionalContactsInformation] WHERE [numContactId]=@numUserCntID)
		end
	else
		begin
			set @v1=10
		end

	DECLARE @TEMPTABLE TABLE
	(
		numRecordId numeric(18,0),
		bintvisiteddate DATETIME,
		numRecentItemsID numeric(18,0),
		chrRecordType char(10)
	)

	INSERT INTO 
		@TEMPTABLE
	SELECT DISTINCT TOP (@v1)
		numrecordId,
		bintvisiteddate,
		numRecentItemsID,
		chrRecordType	
	FROM    
		RECENTITEMS
	WHERE   
		numUserCntId = @numUserCntID
		AND bitdeleted = 0
	ORDER BY 
		bintvisiteddate DESC

	SELECT  DM.numDivisionID AS numRecID ,isnull(CMP.vcCompanyName,'' ) as RecName,                           
		 DM.tintCRMType,                             
		 CMP.numCompanyID AS numCompanyID,                                           
		 ADC.numContactID AS numContactID,          
		 DM.numDivisionID,'C'   as Type,bintvisiteddate  ,0 as caseId,0 as caseTimeId,0 as caseExpId,
		 0 as numOppID,
		'~/images/Icon/organization.png' AS vcImageURL
		FROM  CompanyInfo CMP                            
		join DivisionMaster DM on DM.numCompanyID=CMP.numCompanyID                  
		LEFT join AdditionalContactsInformation ADC on ADC.numDivisionID=DM.numDivisionID AND ISNULL(ADC.bitPrimaryContact,0)=1         
		join @TEMPTABLE R on R.numRecordID=DM.numDivisionID                                             
		WHERE chrRecordType='C'    
	union     
         
	SELECT  DM.numDivisionID AS numRecID,    
	isnull(ISNULL(vcFirstName,'-') + ' '+ISNULL(vcLastName,'-'),'') as RecName,                     
		 DM.tintCRMType,                             
		 CMP.numCompanyID AS numCompanyID,                                           
		 ADC.numContactID AS numContactID,          
		 DM.numDivisionID,'U',bintvisiteddate     ,0 as caseId,0 as caseTimeId,0 as caseExpId,
		 0 as numOppID,
		 '~/images/Icon/contact.png' AS vcImageURL                
		FROM  CompanyInfo CMP                            
		join DivisionMaster DM on DM.numCompanyID=CMP.numCompanyID                  
		join AdditionalContactsInformation ADC on ADC.numDivisionID=DM.numDivisionID          
		join @TEMPTABLE R on numRecordID=ADC.numContactID   where chrRecordType='U'                                         
    
    
	union           
    
	select  numOppId as numRecID,isnull(vcPOppName,'') as RecName,          
	0,0,0,numDivisionID,'O',bintvisiteddate  ,0 as caseId,0 as caseTimeId,0 as caseExpId,
	0 as numOppID,
	CASE 
		WHEN tintOppType=1 AND tintOppStatus=0 THEN '~/images/Icon/Sales opportunity.png'
		WHEN tintOppType=1 AND tintOppStatus=2 THEN '~/images/Icon/Sales opportunity.png'
		WHEN tintOppType=1 AND tintOppStatus=1 THEN '~/images/Icon/Sales order.png'
		WHEN tintOppType=2 AND tintOppStatus=0 THEN '~/images/Icon/Purchase opportunity.png'
		WHEN tintOppType=2 AND tintOppStatus=1 THEN '~/images/Icon/Purchase order.png'
	ELSE '~/images/icons/cart.png' 
	END AS vcImageURL from OpportunityMaster          
	join @TEMPTABLE  on numRecordID=numOppId    where  chrRecordType='O'      
    
          
	union       
        
	select  numProId as numRecID,isnull(vcProjectName,'') as RecName,          
	0,0,0,numDivisionID,'P',bintvisiteddate  ,0 as caseId,0 as caseTimeId,0 as caseExpId,0 as numOppID,'~/images/Icon/project.png' AS vcImageURL from ProjectsMaster          
	join @TEMPTABLE  on numRecordID=numProId  where  chrRecordType='P'    
       
    
	union    
           
	select  numCaseId as numRecID,    
	isnull(vcCaseNumber,'')  as RecName,          
	0,0,0,numDivisionID,'S',bintvisiteddate ,0 as caseId,0 as caseTimeId,0 as caseExpId,0 as numOppID,'~/images/Icon/case.png' AS vcImageURL  from Cases          
	join @TEMPTABLE  on numRecordID=numCaseId   where  chrRecordType='S'      
    
	union    
    
	select numCommId as numRecId,    
	isnull(    
	case when bitTask =971 then 'C - '    
	  when bitTask =972 then 'T - '    
	  when bitTask =973 then 'N - '    
	  when bitTask =974 then 'F - '   
	 else convert(varchar(2),dbo.fn_GetListItemName(bitTask))  
	end + CONVERT(VARCHAR(100),textDetails),'')    
	as RecName,0,0,0,numDivisionID,'A',bintvisiteddate     
	 , caseId, caseTimeId, caseExpId,0 as numOppID,'~/images/Icon/action.png' AS vcImageURL  
	from communication    
	join @TEMPTABLE  on numRecordID=numCommId   where  chrRecordType='A'    
    
    
		union    
        

	select  numItemCode as numRecID,    
	isnull(vcItemName,'') as RecName, 
	0,0,0,0,'I',bintvisiteddate ,0 as caseId,0 as caseTimeId,0 as caseExpId,0 as numOppID,'~/images/Icon/item.png' AS vcImageURL  from Item          
	join @TEMPTABLE  on numRecordID=numItemCode   where  chrRecordType='I'  

	  union    
           
	select  numItemCode as numRecID,    
	isnull(vcItemName,'') as RecName, 
	0,0,0,0,'AI',bintvisiteddate ,0 as caseId,0 as caseTimeId,0 as caseExpId,0 as numOppID,'~/images/Icon/item.png' AS vcImageURL  from Item          
	join @TEMPTABLE  on numRecordID=numItemCode   where  chrRecordType='AI'  
                                    
	union    
           
	select  numGenericDocId as numRecID,    
	isnull(vcDocName,'')  as RecName,          
	0,0,0,0,'D',bintvisiteddate ,0 as caseId,0 as caseTimeId,0 as caseExpId,0 as numOppID,'~/images/Icon/Doc.png' AS vcImageURL  from GenericDocuments          
	join @TEMPTABLE  on numRecordID=numGenericDocId   where  chrRecordType='D'      

	union 

	select  numCampaignId as numRecID,isnull(vcCampaignName,'') as RecName,          
	0,0,0,0 AS numDivisionID,'M',bintvisiteddate  ,0 as caseId,0 as caseTimeId,0 as caseExpId,0 as numOppID,'~/images/Icon/campaign.png' AS vcImageURL from dbo.CampaignMaster        
	join @TEMPTABLE  on numRecordID=numCampaignId    where  chrRecordType='M'      
    
	union 

	select  numReturnHeaderID as numRecID,isnull(vcRMA,'') as RecName,          
	0,0,0,numDivisionID,'R',bintvisiteddate  ,0 as caseId,0 as caseTimeId,0 as caseExpId,0 as numOppID, CASE 
																											WHEN tintReturnType=1 THEN '~/images/Icon/Sales Return.png'
																											WHEN tintReturnType=2 THEN '~/images/Icon/Purchase Return.png'
																											WHEN tintReturnType=3 THEN '~/images/Icon/CreditMemo.png'
																											WHEN tintReturnType=4 THEN '~/images/Icon/Refund.png'
																										ELSE 
																											'~/images/Icon/ReturnLastView.png' 
																										END AS vcImageURL from ReturnHeader          
	join @TEMPTABLE  on numRecordID=numReturnHeaderID  where  chrRecordType='R'   

	UNION

	SELECT
		numOppBizDocsId as numRecID,
		isnull(vcBizDocID,'') as RecName,
		0,0,0,0 AS numDivisionID,'B',bintvisiteddate,0 as caseId,0 as caseTimeId,0 as caseExpId,OpportunityMaster.numOppId as numOppID,
		(CASE 
			WHEN OpportunityMaster.tintOppType = 1 THEN '~/images/Icon/Sales BizDoc.png' 
			WHEN OpportunityMaster.tintOppType = 2 THEN '~/images/Icon/Purchase BizDoc.png' 
			ELSE 
				'~/images/Icon/Doc.png' 
		END) AS vcImageURL
	FROM
		OpportunityBizDocs
	JOIN
		OpportunityMaster
	ON
		OpportunityBizDocs.numOppId = OpportunityMaster.numOppId
	JOIN
		@TEMPTABLE
	ON
		numOppBizDocsId = numRecordId
	WHERE
		chrRecordType = 'B'
	union 

	SELECT 
		numWOId AS numRecID
		,ISNULL(vcWorkOrderName,'') as RecName
		,0
		,0
		,0
		,0 AS numDivisionID
		,'W'
		,bintvisiteddate
		,0 as caseId
		,0 as caseTimeId
		,0 as caseExpId
		,0 as numOppID
		,'~/images/Icon/WorkOrder.png' AS vcImageURL 
	FROM 
		dbo.WorkOrder        
	JOIN
		@TEMPTABLE 
	ON 
		numRecordID=numWOId
	WHERE 
		chrRecordType='W'  

	order by bintVisitedDate desc     
END
GO
-- =============================================
-- Author:		<Author,,Sachin Sadhu>
-- Create date: <Create Date,,07-Dec-13>
-- Description:	<Description,,Fetch New MailBox>
-- =============================================

GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_InboxTreeSort_NewSortOrder' ) 
    DROP PROCEDURE USP_InboxTreeSort_NewSortOrder
GO
CREATE PROCEDURE USP_InboxTreeSort_NewSortOrder
   	-- Add the parameters for the stored procedure here
	@numDomainID int,
	@numUserCntID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- Insert statements for procedure here
	IF Not Exists (Select 'col1' from  InboxTreeSort where numdomainid=@numDomainID AND numUserCntId=@numUserCntID)
	BEGIN

		INSERT INTO InboxTreeSort
		(numParentID, vcNodeName, numDomainID, numUserCntID, numSortOrder, bitSystem)
		VALUES     (NULL,NULL,@numDomainID,@numUserCntID,1,1)
				
		DECLARE @lastParentid INT
		SELECT @lastParentid=@@identity FROM InboxTreeSort

		INSERT INTO InboxTreeSort
		(numParentID, vcNodeName, numDomainID, numUserCntID, numSortOrder, bitSystem, numFixID)
		VALUES     (@lastParentid,'Inbox',@numDomainID,@numUserCntID,2,1,1)
		INSERT INTO InboxTreeSort
		(numParentID, vcNodeName, numDomainID, numUserCntID, numSortOrder, bitSystem, numFixID)
		VALUES     (@lastParentid,'Email Archive',@numDomainID,@numUserCntID,3,1,2)
		INSERT INTO InboxTreeSort
		(numParentID, vcNodeName, numDomainID, numUserCntID, numSortOrder, bitSystem, numFixID)
		VALUES     (@lastParentid,'Sent Messages',@numDomainID,@numUserCntID,4,1,4)
		INSERT INTO InboxTreeSort
		(numParentID, vcNodeName, numDomainID, numUserCntID, numSortOrder, bitSystem, numFixID)
		VALUES     (@lastParentid,'Calendar',@numDomainID,@numUserCntID,5,1,5)
		INSERT INTO InboxTreeSort
		(numParentID, vcNodeName, numDomainID, numUserCntID, numSortOrder, bitSystem, numFixID)
		VALUES     (@lastParentid,'Custom Folders',@numDomainID,@numUserCntID,6,1,6)

		--ALL RECORDS ARE ALREADY REPLACED WITH MASTER SCRIPT TO APPROPROATE ID FOR USERCNTID BUT
		--THERE ARE SOME RECORDS IN EMAIL HISTORY TABLE FOR SOME USERS WHOSE CORRESPONDING TREE STRUCTURE IS NOT AVILABLE IN INBOXTREESORT TABLE
		--FOLLOWING SCRIPTS UPDATED NODEID IN EMAIL HISTORY TABLE FOR INBOX=0, SENT EMAILS=4 AND DELETED MAILS=2 INCASE EXISTS BEFORE TREE STRUCTURE CREATED
		UPDATE EmailHistory SET numNodeId = ISNULL((SELECT numNodeId FROM InboxTreeSort WHERE numDomainID = @numDomainID AND numUserCntID = @numUserCntID AND ISNULL(numFixID,0)=4),4) WHERE numDomainID = @numDomainID AND numUserCntId = @numUserCntID AND numNodeId = 4 AND (numNodeId <> (SELECT numNodeId FROM InboxTreeSort WHERE numDomainID = @numDomainID AND numUserCntID = @numUserCntID AND ISNULL(numFixID,0)=4))
		UPDATE EmailHistory SET numNodeId = ISNULL((SELECT numNodeId FROM InboxTreeSort WHERE numDomainID = @numDomainID AND numUserCntID = @numUserCntID AND ISNULL(numFixID,0)=2),2) WHERE numDomainID = @numDomainID AND numUserCntId = @numUserCntID AND numNodeId = 2 AND (numNodeId <> (SELECT numNodeId FROM InboxTreeSort WHERE numDomainID = @numDomainID AND numUserCntID = @numUserCntID AND ISNULL(numFixID,0)=2))
		UPDATE EmailHistory SET numNodeId = ISNULL((SELECT numNodeId FROM InboxTreeSort WHERE numDomainID = @numDomainID AND numUserCntID = @numUserCntID AND ISNULL(numFixID,0)=1),0) WHERE numDomainID = @numDomainID AND numUserCntId = @numUserCntID AND numNodeId = 0 AND (numNodeId <> (SELECT numNodeId FROM InboxTreeSort WHERE numDomainID = @numDomainID AND numUserCntID = @numUserCntID AND ISNULL(numFixID,0)=1))
	END

	DECLARE @TMEP TABLE
	(
		numNodeID int,
		numParentId int,
		vcNodeName varchar(100),
		numSortOrder int,
		bitSystem bit,
		numFixID numeric(18,0),
		bitHidden BIT
	)
	INSERT INTO 
		@TMEP
	SELECT 
		numNodeID,
		CASE WHEN numParentId=0 THEN NULL ELSE numParentID END AS numParentID,
		vcNodeName as vcNodeName,
		numSortOrder, 
		bitSystem,
		numFixID,
		ISNULL(bitHidden,0) AS bitHidden
	FROM 
		InboxTreeSort 
	WHERE 
		(numdomainid=@numDomainID) AND (numUserCntId=@numUserCntID )
	ORDER BY 
		numSortOrder asc

	DECLARE @numParentNodeID AS NUMERIC(18,0)
	SELECT @numParentNodeID = numNodeID FROM InboxTreeSort WHERE numDomainID=@numDomainID AND numUserCntID=@numUserCntID AND ISNULL(numParentID,0) = 0 AND ISNULL(vcNodeName,'')=''

	UPDATE @TMEP SET numParentId = NULL WHERE numParentID =@numParentNodeID
	UPDATE @TMEP SET numParentId = @numParentNodeID WHERE numParentID NOT IN (SELECT numNodeID FROM @TMEP)
	UPDATE @TMEP SET numParentId = @numParentNodeID WHERE numParentID IS NULL AND numNodeID<>@numParentNodeID


	--DELETE FROM @TMEP WHERE numNodeID = @numParentNodeID

	SELECT * FROM @TMEP ORDER BY numSortOrder ASC
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_InLineEditDataSave')
DROP PROCEDURE USP_InLineEditDataSave
GO
CREATE PROCEDURE [dbo].[USP_InLineEditDataSave] 
( 
 @numDomainID NUMERIC(18,0),
 @numUserCntID NUMERIC(18,0),
 @numFormFieldId NUMERIC(18,0),
 @bitCustom AS BIT = 0,
 @InlineEditValue VARCHAR(8000),
 @numContactID NUMERIC(18,0),
 @numDivisionID NUMERIC(18,0),
 @numWOId NUMERIC(18,0),
 @numProId NUMERIC(18,0),
 @numOppId NUMERIC(18,0),
 @numOppItemId NUMERIC(18,0),
 @numRecId NUMERIC(18,0),
 @vcPageName NVARCHAR(100),
 @numCaseId NUMERIC(18,0),
 @numWODetailId NUMERIC(18,0),
 @numCommID NUMERIC(18,0)
 )
AS 

declare @strSql as nvarchar(4000)
DECLARE @vcDbColumnName AS VARCHAR(100),@vcOrigDbColumnName AS VARCHAR(100),@vcLookBackTableName AS varchar(100)
DECLARE @vcListItemType as VARCHAR(3),@numListID AS numeric(9),@vcAssociatedControlType varchar(20)
declare @tempAssignedTo as numeric(9);set @tempAssignedTo=null           
DECLARE @Value1 AS VARCHAR(18),@Value2 AS VARCHAR(18)

IF  @bitCustom =0
BEGIN
	SELECT @vcDbColumnName=vcDbColumnName,@vcLookBackTableName=vcLookBackTableName,
		   @vcListItemType=vcListItemType,@numListID=numListID,@vcAssociatedControlType=vcAssociatedControlType,
		   @vcOrigDbColumnName=vcOrigDbColumnName
	 FROM DycFieldMaster WHERE numFieldId=@numFormFieldId
	 
	IF @vcAssociatedControlType = 'DateField' AND  @InlineEditValue=''
	BEGIN
		SET @InlineEditValue = null
	END

	DECLARE @intExecuteDiv INT=0
	if @vcLookBackTableName = 'DivisionMaster' 
	 BEGIN 
		
	   IF @vcDbColumnName='numFollowUpStatus' --Add to FollowUpHistory
	    Begin
			IF(@numCommID>0 AND @numDivisionID=0)
			BEGIN
				SET @numDivisionID=(SELECT TOP 1 numDivisionId FROM Communication WHERE numCommId=@numCommID)
			END
			declare @numFollow as varchar(10),@binAdded as varchar(20),@PreFollow as varchar(10),@RowCount as varchar(2)                            
			                      
			set @PreFollow=(select count(*)  numFollowUpStatus from FollowUpHistory where numDivisionID= @numDivisionID and numDomainId= @numDomainID)                          
			set @RowCount=@@rowcount  
			                        
			select   @numFollow=numFollowUpStatus, @binAdded=bintModifiedDate from divisionmaster where numDivisionID=@numDivisionID and numDomainId= @numDomainID                    
			
			if @numFollow <>'0' and @numFollow <> @InlineEditValue                          
			begin                          
				if @PreFollow<>0                          
					begin                          
			                   
						if @numFollow <> (select top 1  numFollowUpStatus from FollowUpHistory where numDivisionID= @numDivisionID and numDomainId= @numDomainID order by numFollowUpStatusID desc)                          
							begin                          
			                      	insert into FollowUpHistory(numFollowUpstatus,numDivisionID,bintAddedDate,numDomainId)                          
									values(@numFollow,@numDivisionID,@binAdded,@numDomainID)                          
							end                          
					end                          
				else                          
					begin                          
						insert into FollowUpHistory(numFollowUpstatus,numDivisionID,bintAddedDate,numDomainId)                          
						 values(@numFollow,@numDivisionID,@binAdded,@numDomainID)                          
					end                          
			   end 
			   
			
			UPDATE  DivisionMaster SET  dtLastFollowUp=getutcdate() where numDivisionID=@numDivisionID and numDomainID=@numDomainID
		END
		 IF @vcDbColumnName='numAssignedTo' ---Updating if organization is assigned to someone
			BEGIN
				select @tempAssignedTo=isnull(numAssignedTo,0) from DivisionMaster where numDivisionID = @numDivisionID and numDomainId= @numDomainID           
			
				if (@tempAssignedTo<>@InlineEditValue and  @InlineEditValue<>'0')          
					begin            
						update DivisionMaster set numAssignedTo=@InlineEditValue ,numAssignedBy=@numUserCntID where numDivisionID = @numDivisionID  and numDomainId= @numDomainID  
						UPDATE CompanyInfo SET bintModifiedDate=GETUTCDATE() WHERE numCompanyId IN (SELECT numCompanyId 
																									FROM 
																										DivisionMaster
																									WHERE 
																										numDivisionID = @numDivisionID  and numDomainId= @numDomainID)         
					end					
				else if  (@InlineEditValue ='0')          
					begin          
						update DivisionMaster set numAssignedTo=0 ,numAssignedBy=0 where numDivisionID = @numDivisionID  and numDomainId= @numDomainID
						UPDATE CompanyInfo SET bintModifiedDate=GETUTCDATE() WHERE numCompanyId IN (SELECT numCompanyId 
																									FROM 
																										DivisionMaster
																									WHERE 
																										numDivisionID = @numDivisionID  and numDomainId= @numDomainID)          
					end    
			END
	    UPDATE CompanyInfo SET numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() WHERE numCompanyId=(SELECT TOP 1 numCompanyId FROM DivisionMaster where numDivisionID=@numDivisionID and numDomainID=@numDomainID) and numDomainID=@numDomainID
		
		IF @vcDbColumnName='vcPartnerCode'
		BEGIN
			IF(@InlineEditValue<>'')
			BEGIN
				IF EXISTS(SELECT vcPartnerCode FROM DivisionMaster where numDivisionID<>@numDivisionID and numDomainID=@numDomainID AND vcPartnerCode=@InlineEditValue)
				BEGIN
					SET @intExecuteDiv=1
					SET @InlineEditValue='-#12'
				END
			END
		END
		IF @intExecuteDiv=0
		BEGIN
			SET @strSql='update DivisionMaster set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numDivisionID=@numDivisionID and numDomainID=@numDomainID'
			
		END
	 END 
	 ELSE if @vcLookBackTableName = 'AdditionalContactsInformation' 
	 BEGIN 
			IF @vcDbColumnName='bitPrimaryContact' AND @InlineEditValue='True'
			BEGIN
				DECLARE @bitPrimaryContactCheck AS BIT             
				DECLARE @numID AS NUMERIC(9) 
				
				SELECT TOP 1 @numID = numContactID,@bitPrimaryContactCheck = ISNULL(bitPrimaryContact,0)
					FROM AdditionalContactsInformation WHERE numDivisionID = @numDivisionID 
                   
					IF @@rowcount = 0 SET @numID = 0             
				
					WHILE @numID > 0            
					BEGIN            
						IF @bitPrimaryContactCheck = 1 
							UPDATE  AdditionalContactsInformation SET bitPrimaryContact = 0 WHERE numContactID = @numID  
                              
						SELECT TOP 1 @numID = numContactID,@bitPrimaryContactCheck = ISNULL(bitPrimaryContact,0)
						FROM AdditionalContactsInformation WHERE numDivisionID = @numDivisionID AND numContactID > @numID    
                                
						IF @@rowcount = 0 SET @numID = 0             
					END 
				END
				ELSE IF @vcDbColumnName='bitPrimaryContact' AND @InlineEditValue='False'
				BEGIN
					IF (SELECT COUNT(*) FROM AdditionalContactsInformation WHERE numDivisionID = @numDivisionID AND numContactId!=@numContactId AND ISNULL(bitPrimaryContact,0)=1) = 0
						SET @InlineEditValue='True'
				END 
				-- Added by Priya(to check no Campaign got engaged wothout contacts email
				ELSE IF @vcDbColumnName = 'numECampaignID' 
				BEGIN
					
					IF EXISTS (select 1 from AdditionalContactsInformation WHERE numContactId = @numContactID AND (vcEmail IS NULL OR vcEmail = ''))
					BEGIN
						RAISERROR('EMAIL_ADDRESS_REQUIRED',16,1)
								RETURN
					END

				END
				
		SET @strSql='update AdditionalContactsInformation set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numDivisionID=@numDivisionID and numDomainID=@numDomainID and numContactId=@numContactId'
	 END 
	 ELSE if @vcLookBackTableName = 'CompanyInfo' 
	 BEGIN 
		SET @strSql='update CompanyInfo set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numCompanyId=(select numCompanyId from divisionmaster where numDivisionID=@numDivisionID and numDomainID=@numDomainID) and numDomainID=@numDomainID'
	 END 
	 ELSE if @vcLookBackTableName = 'WorkOrder' 
	 BEGIN 
		DECLARE @numItemCode AS NUMERIC(9)
	 
		IF @numWODetailId>0 AND @vcDbColumnName <> 'numQtyItemsReq'
		BEGIN
			SET @strSql='update WorkOrderDetails set ' + @vcDbColumnName + '=@InlineEditValue where numWOId=@numWOId and numWODetailId=@numWODetailId' 
		END 	
		ELSE
		BEGIN
			SET @strSql='update WorkOrder set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numWOId=@numWOId and numDomainID=@numDomainID'
			
			IF @vcDbColumnName='numQtyItemsReq'
			BEGIN 
				DECLARE @numPickedQty FLOAT = 0
				
				SET @numPickedQty = ISNULL((SELECT
												MAX(numPickedQty)
											FROM
											(
												SELECT 
													WorkOrderDetails.numWODetailId
													,CEILING(SUM(WorkOrderPickedItems.numPickedQty)/numQtyItemsReq_Orig) numPickedQty
												FROM
													WorkOrderDetails
												INNER JOIN
													WorkOrderPickedItems
												ON
													WorkOrderDetails.numWODetailId=WorkOrderPickedItems.numWODetailId
												WHERE
													WorkOrderDetails.numWOId=@numWOID
													AND ISNULL(WorkOrderDetails.numWareHouseItemId,0) > 0
												GROUP BY
													WorkOrderDetails.numWODetailId
													,WorkOrderDetails.numQtyItemsReq_Orig
											) TEMP),0)

				IF @InlineEditValue < ISNULL((@numPickedQty),0)
				BEGIN
					RAISERROR('WORKORDR_QUANITY_CAN_NOT_BE_LESS_THEN_PICKED_QTY',16,1)
					RETURN
				END

				IF EXISTS (SELECT 
								SPDTTL.ID 
							FROM 
								StagePercentageDetailsTask SPDT
							INNER JOIN
								StagePercentageDetailsTaskTimeLog SPDTTL
							ON
								SPDT.numTaskId = SPDTTL.numTaskID 
							WHERE 
								SPDT.numDomainID=@numDomainID 
								AND SPDT.numWorkOrderId = @numWOId)
				BEGIN
					RAISERROR('TASKS_ARE_ALREADY_STARTED',16,1)
					RETURN
				END

				IF EXISTS (SELECT numWOId FROM WorkOrder WHERE numDomainID=@numDomainID AND numWOId=@numWOId AND numWOStatus=23184)
				BEGIN
					RAISERROR('WORKORDR_COMPLETED',16,1)
					RETURN
				END

				SELECT @numItemCode=numItemCode FROM WorkOrder WHERE numWOId=@numWOId and numDomainID=@numDomainID
			
				EXEC USP_UpdateWorkOrderDetailQuantity @numDomainID,@numUserCntID,@numItemCode,@InlineEditValue,@numWOId
			END 
		END	
	 END
	 ELSE if @vcLookBackTableName = 'ProjectsMaster' 
	 BEGIN 
		IF @vcDbColumnName='numAssignedTo' ---Updating if Project is assigned to someone 
			BEGIN
				select @tempAssignedTo=isnull(numAssignedTo,0) from ProjectsMaster where numProId=@numProId and numDomainId= @numDomainID           
			
				if (@tempAssignedTo<>@InlineEditValue and  @InlineEditValue<>'0')          
					begin            
						update ProjectsMaster set numAssignedTo=@InlineEditValue ,numAssignedBy=@numUserCntID,bintModifiedDate=GETUTCDATE() where numProId = @numProId  and numDomainId= @numDomainID           
					end           
				else if  (@InlineEditValue ='0')          
					begin          
						update ProjectsMaster set numAssignedTo=0 ,numAssignedBy=0,bintModifiedDate=GETUTCDATE() where numProId = @numProId  and numDomainId= @numDomainID        
					end    
			END
		else IF @vcDbColumnName='numProjectStatus'  ---Updating All Statge to 100% if Completed
			BEGIN
				IF @InlineEditValue='27492'
				BEGIN
					UPDATE PM SET dtCompletionDate=GETUTCDATE(),monTotalExpense=PIE.monExpense,
					monTotalIncome=PIE.monIncome,monTotalGrossProfit=PIE.monBalance,numProjectStatus=27492
					FROM ProjectsMaster PM,dbo.GetProjectIncomeExpense(@numDomainId,@numProId) PIE
					WHERE PM.numProId=@numProId
					
					EXEC USP_ManageProjectCommission @numDomainId,@numProId
						
					Update StagePercentageDetails set tinProgressPercentage=100,bitclose=1 where numProjectID=@numProId

					EXEC dbo.USP_GetProjectTotalProgress
							@numDomainID = @numDomainID, --  numeric(9, 0)
							@numProId = @numProId, --  numeric(9, 0)
							@tintMode = 1 --  tinyint
				END			
				ELSE
				BEGIN
					DELETE from BizDocComission WHERE numDomainID = @numDomainID AND numProId=@numProId AND numComissionID 
						NOT IN (SELECT BC.numComissionID FROM dbo.BizDocComission BC JOIN dbo.PayrollTracking PT 
							ON BC.numComissionID = PT.numComissionID AND BC.numDomainId = PT.numDomainId WHERE BC.numDomainID = @numDomainID AND ISNULL(BC.numProId,0) = @numProId)
				END
			END
			

		SET @strSql='update ProjectsMaster set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numProId=@numProId and numDomainID=@numDomainID'
	 END 
	 ELSE if @vcLookBackTableName = 'OpportunityMaster' 
	 BEGIN 
		IF @vcDbColumnName='numAssignedTo' ---Updating if organization is assigned to someone  
			BEGIN
				select @tempAssignedTo=isnull(numAssignedTo,0) from OpportunityMaster where numOppId=@numOppId and numDomainId= @numDomainID           
			
				if (@tempAssignedTo<>@InlineEditValue and  @InlineEditValue<>'0')          
					begin            
						update OpportunityMaster set numAssignedTo=@InlineEditValue ,numAssignedBy=@numUserCntID,bintModifiedDate=GETUTCDATE() where numOppId = @numOppId  and numDomainId= @numDomainID           
					end           
				else if  (@InlineEditValue ='0')          
					begin          
						update OpportunityMaster set numAssignedTo=0 ,numAssignedBy=0,bintModifiedDate=GETUTCDATE() where numOppId = @numOppId  and numDomainId= @numDomainID        
					end    
			END
		ELSE IF @vcDbColumnName='numPartner' OR @vcDbColumnName='numPartenerSource'
		 BEGIN
			update OpportunityMaster set numPartner=@InlineEditValue ,bintModifiedDate=GETUTCDATE() where numOppId = @numOppId  and numDomainId= @numDomainID           
		 END
		
		ELSE IF @vcDbColumnName='tintActive'
		 BEGIN
		 	SET @InlineEditValue= CASE @InlineEditValue WHEN 'True' THEN 1 ELSE 0 END 
		 END
		 ELSE IF @vcDbColumnName='tintSource'
		 BEGIN
			IF CHARINDEX('~', @InlineEditValue)>0
			BEGIN
				SET @Value1=SUBSTRING(@InlineEditValue, 1,CHARINDEX('~', @InlineEditValue)-1) 
				SET @Value2=SUBSTRING(@InlineEditValue, CHARINDEX('~', @InlineEditValue)+1,LEN(@InlineEditValue)) 
			END	
			ELSE
			BEGIN
				SET @Value1=@InlineEditValue
				SET @Value2=0
			END
			
		 	update OpportunityMaster set tintSourceType=@Value2 where numOppId = @numOppId  and numDomainId= @numDomainID        
		 	
		 	SET @InlineEditValue= @Value1
		 END
		ELSE IF @vcDbColumnName='tintOppStatus' --Update Inventory  0=Open,1=Won,2=Lost
		 BEGIN			
			DECLARE @bitViolatePrice BIT
			DECLARE @intPendingApprovePending INT
			DECLARE @tintCommitAllocation1 TINYINT
			SELECT @tintCommitAllocation1=ISNULL(tintCommitAllocation,1) FROM Domain WHERE numDomainId=@numDomainId

			IF EXISTS (SELECT numApprovalRuleID FROM ApprovalProcessItemsClassification WHERE numDomainId=@numDomainID AND ISNULL(bitMarginPriceViolated,0) = 1)
			BEGIN
				SET @bitViolatePrice = 1
			END
			SET @intPendingApprovePending=(SELECT COUNT(*) FROM OpportunityItems WHERE numOppId=@numOppId AND ISNULL(bitItemPriceApprovalRequired,0)=1)

			IF(@intPendingApprovePending > 0 AND @bitViolatePrice=1 AND @InlineEditValue='1')
			BEGIN
				SET @intExecuteDiv=1
				SET @InlineEditValue='-#123'
			END

			IF ISNULL(@intExecuteDiv,0)=0
			BEGIN
				DECLARE @tintOppStatus AS TINYINT
				SELECT @tintOppStatus=tintOppStatus FROM OpportunityMaster WHERE numOppID=@numOppID              

				IF (@tintOppStatus=0 OR @tintOppStatus=2) AND @InlineEditValue='1' --Open to Won
				BEGIN
					SELECT @numDivisionID=numDivisionID FROM OpportunityMaster WHERE numOppID=@numOppID   
					
					DECLARE @tintCRMType AS TINYINT      
					SELECT @tintCRMType=tintCRMType FROM divisionmaster WHERE numDivisionID =@numDivisionID 

					-- Promote Lead to Account when Sales/Purchase Order is created against it
					IF @tintCRMType=0 --Lead & Order
					BEGIN        
						UPDATE 
							divisionmaster 
						SET 
							tintCRMType=2
							,bintLeadProm=GETUTCDATE()
							,bintLeadPromBy=@numUserCntID
							,bintProsProm=GETUTCDATE()
							,bintProsPromBy=@numUserCntID        
						WHERE 
							numDivisionID=@numDivisionID        
					END
					--Promote Prospect to Account
					ELSE IF @tintCRMType=1 
					BEGIN        
						UPDATE 
							divisionmaster 
						SET 
							tintCRMType=2
							,bintProsProm=GETUTCDATE()
							,bintProsPromBy=@numUserCntID        
						WHERE 
							numDivisionID=@numDivisionID        
					END    

					IF @tintCommitAllocation1=1
					BEGIN
		 				EXEC USP_UpdatingInventoryonCloseDeal @numOppID,0,@numUserCntID
					END

					UPDATE OpportunityMaster SET bintOppToOrder=GETUTCDATE() WHERE numOppId=@numOppId and numDomainID=@numDomainID
					UPDATE OpportunityItems SET bitRequiredWarehouseCorrection=NULL WHERE numOppId=@numOppId
					EXEC USP_OpportunityItems_CheckWarehouseMapping @numDomainID,@numOppId
				END
				ELSE IF (@InlineEditValue='2') -- Win to Lost
				BEGIN
					UPDATE OpportunityMaster SET dtDealLost=GETUTCDATE() WHERE numOppId=@numOppId and numDomainID=@numDomainID
				END
				
				IF ((@tintOppStatus=1 and @InlineEditValue='2') or (@tintOppStatus=1 and @InlineEditValue='0')) AND @tintCommitAllocation1=1 --Won to Lost or Won to Open
						EXEC USP_RevertDetailsOpp @numOppID,0,0,@numUserCntID 

				UPDATE OpportunityMaster SET tintOppStatus = @InlineEditValue WHERE numOppId=@numOppID
				
			END
		 END
		 ELSE IF @vcDbColumnName='vcOppRefOrderNo' --Update vcRefOrderNo OpportunityBizDocs
		 BEGIN
			 IF LEN(ISNULL(@InlineEditValue,''))>0
			 BEGIN
	   			UPDATE dbo.OpportunityBizDocs SET vcRefOrderNo=@InlineEditValue WHERE numOppId=@numOppID
			 END
		 END
		 ELSE IF @vcDbColumnName='numStatus' 
		 BEGIN
			DECLARE @tintOppType AS TINYINT
			select @tintOppStatus=tintOppStatus,@tintOppType=tintOppType from OpportunityMaster where numOppID=@numOppID

			IF @tintOppType=1 AND @tintOppStatus=1
			BEGIN
				DECLARE @numOppStatus AS NUMERIC(18,0) = CAST(@InlineEditValue AS NUMERIC(18,0))
				EXEC USP_SalesFulfillmentQueue_Save @numDomainID,@numUserCntID,@numOppID,@numOppStatus
			END

		 	 IF EXISTS (SELECT 1 FROM dbo.OpportunityMaster WHERE numOppId = @numOppID AND tintOppType=1 AND isnull(tintOppStatus,0)=1 AND ISNULL(numStatus,0)!= @InlineEditValue)
			 BEGIN
	   				EXEC USP_ManageOpportunityAutomationQueue
							@numOppQueueID = 0, -- numeric(18, 0)
							@numDomainID = @numDomainID, -- numeric(18, 0)
							@numOppId = @numOppID, -- numeric(18, 0)
							@numOppBizDocsId = 0, -- numeric(18, 0)
							@numOrderStatus = @InlineEditValue, -- numeric(18, 0)
							@numUserCntID = @numUserCntID, -- numeric(18, 0)
							@tintProcessStatus = 1, -- tinyint
							@tintMode = 1 -- TINYINT
			END 
		 END	
		 ELSE IF @vcDbColumnName = 'dtReleaseDate'
		 BEGIN
			IF EXISTS (SELECT 1 FROM OpportunityMaster WHERE numDomainId=@numDomainID AND numOppId=@numOppId AND tintOppType=1 AND tintOppStatus=0)
			BEGIN
				IF (SELECT dbo.GetListIemName(ISNULL(numPercentageComplete,0)) FROM OpportunityMaster WHERE numDomainId=@numDomainID AND numOppId=@numOppId) = '100'
				BEGIN
					SET @strSql='update OpportunityMaster set ' + @vcDbColumnName + '=DATEADD(hh, DATEDIFF(hh, GETDATE(), GETUTCDATE()), @InlineEditValue),numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numOppId=@numOppId and numDomainID=@numDomainID'
				END
				ELSE
				BEGIN
					RAISERROR('PERCENT_COMPLETE_MUST_BE_100',16,1)
					RETURN
				END
			END
			ELSE
			BEGIN
				SET @strSql='update OpportunityMaster set ' + @vcDbColumnName + '=DATEADD(hh, DATEDIFF(hh, GETDATE(), GETUTCDATE()), @InlineEditValue),numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numOppId=@numOppId and numDomainID=@numDomainID'
			END
		END
		ELSE IF @vcDbColumnName = 'numPercentageComplete'
		BEGIN
			IF EXISTS (SELECT 1 FROM OpportunityMaster WHERE numDomainId=@numDomainID AND numOppId=@numOppId AND tintOppType=1 AND tintOppStatus=0 AND dbo.GetListIemName(ISNULL(numPercentageComplete,0))='100')
			BEGIN
				IF EXISTS(SELECT dtReleaseDate FROM OpportunityMaster WHERE numDomainId=@numDomainID AND numOppId=@numOppId AND dtReleaseDate IS NULL)
				BEGIN
					SET @strSql='update OpportunityMaster set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numOppId=@numOppId and numDomainID=@numDomainID'
				END
				ELSE
				BEGIN
					RAISERROR('REMOVE_RELEASE_DATE',16,1)
					RETURN
				END
			END
			ELSE
			BEGIN
				SET @strSql='update OpportunityMaster set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numOppId=@numOppId and numDomainID=@numDomainID'
			END
		END
		ELSE IF @vcDbColumnName = 'numProjectID'
		BEGIN
			UPDATE OpportunityItems SET numProjectID=@InlineEditValue WHERE numOppId=@numOppId
		END
		ELSE IF @vcDbColumnName = 'intUsedShippingCompany'
		BEGIN
			UPDATE OpportunityMaster SET numShippingService=0 WHERE numOppId=@numOppId
		END

		IF(@vcDbColumnName!='tintOppStatus')
		BEGIN
			SET @strSql='update OpportunityMaster set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numOppId=@numOppId and numDomainID=@numDomainID'
		END
	 END
	 ELSE if @vcLookBackTableName = 'Cases' 
	 BEGIN 
	 		IF @vcDbColumnName='numAssignedTo' ---Updating if Case is assigned to someone  
			BEGIN
				select @tempAssignedTo=isnull(numAssignedTo,0) from Cases where numCaseId=@numCaseId and numDomainId= @numDomainID           
			
				if (@tempAssignedTo<>@InlineEditValue and  @InlineEditValue<>'0')          
					begin            
						update Cases set numAssignedTo=@InlineEditValue ,numAssignedBy=@numUserCntID,bintModifiedDate=GETUTCDATE() where numCaseId = @numCaseId  and numDomainId= @numDomainID           
					end           
				else if  (@InlineEditValue ='0')          
					begin          
						update Cases set numAssignedTo=0 ,numAssignedBy=0,bintModifiedDate=GETUTCDATE() where numCaseId = @numCaseId  and numDomainId= @numDomainID        
					end    
			END
		UPDATE Cases SET bintModifiedDate=GETUTCDATE() WHERE numCaseId = @numCaseId  and numDomainId= @numDomainID    
		SET @strSql='update Cases set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numCaseId=@numCaseId and numDomainID=@numDomainID'
	 END 
	 ELSE if @vcLookBackTableName = 'Tickler' 
	 BEGIN 	
		IF(@vcOrigDbColumnName = 'textDetails' AND EXISTS(SELECT * FROM Communication WHERE numCommId=@numCommID and numDomainID=@numDomainID))
		BEGIN
			SET @strSql='update Communication set ' + @vcOrigDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,dtModifiedDate=getutcdate() where numCommId=@numCommId and numDomainID=@numDomainID'
		END
		ELSE IF(@vcOrigDbColumnName = 'textDetails' AND EXISTS(SELECT * FROM Activity WHERE ActivityID=@numCommID))
		BEGIN
			SET @strSql='update Activity set Comments=@InlineEditValue where ActivityID=@numCommId'
		END
		ELSE
		BEGIN
			SET @strSql='update Communication set ' + @vcOrigDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,dtModifiedDate=getutcdate() where numCommId=@numCommId and numDomainID=@numDomainID'
		END
	 END
	 ELSE IF @vcLookBackTableName = 'ExtarnetAccounts'
	 BEGIN
		IF @vcDbColumnName='bitEcommerceAccess'
		BEGIN
			IF LOWER(@InlineEditValue) = 'yes' OR LOWER(@InlineEditValue) = 'true'
			BEGIN
				IF NOT EXISTS (SELECT * FROM ExtarnetAccounts where numDivisionID=@numDivisionID) 
				BEGIN
					DECLARE @numGroupID NUMERIC
					DECLARE @numCompanyID NUMERIC(18,0)
					SELECT @numCompanyID=numCompanyID FROM DivisionMaster WHERE numDivisionID=@numDivisionID

					SELECT TOP 1 @numGroupID=numGroupID FROM dbo.AuthenticationGroupMaster WHERE numDomainID=@numDomainID AND tintGroupType=2
				
					IF @numGroupID IS NULL SET @numGroupID = 0 
					INSERT INTO ExtarnetAccounts (numCompanyID,numDivisionID,numGroupID,numDomainID)                                                
					VALUES(@numCompanyID,@numDivisionID,@numGroupID,@numDomainID)
				END
			END
			ELSE
			BEGIN
				DELETE FROM ExtranetAccountsDtl WHERE numExtranetID IN (SELECT ISNULL(numExtranetID,0) FROM ExtarnetAccounts where numDivisionID=@numDivisionID)
				DELETE FROM ExtarnetAccounts where numDivisionID=@numDivisionID
			END

			SELECT (CASE WHEN LOWER(@InlineEditValue) = 'yes' OR LOWER(@InlineEditValue) = 'true' THEN 'Yes' ELSE 'No' END) AS vcData

			RETURN
		END
	 END
	ELSE IF @vcLookBackTableName = 'ExtranetAccountsDtl'
	BEGIN
		IF @vcDbColumnName='vcPassword'
		BEGIN
			IF LEN(@InlineEditValue) = 0
			BEGIN
				DELETE FROM ExtranetAccountsDtl WHERE numContactID=@numContactID
			END
			ELSE
			BEGIN
				IF EXISTS (SELECT numExtranetID FROM ExtarnetAccounts WHERE numDivisionID = @numDivisionID)
				BEGIN
					IF EXISTS (SELECT numExtranetDtlID FROM ExtranetAccountsDtl WHERE numContactID=@numContactID)
					BEGIN
						UPDATE
							ExtranetAccountsDtl
						SET 
							vcPassword=@InlineEditValue
							,tintAccessAllowed=1
						WHERE
							numContactID=@numContactID
					END
					ELSE
					BEGIN
						DECLARE @numExtranetID NUMERIC(18,0)
						SELECT @numExtranetID=numExtranetID FROM ExtarnetAccounts WHERE numDivisionID = @numDivisionID

						INSERT INTO ExtranetAccountsDtl (numExtranetID,numContactID,vcPassword,tintAccessAllowed,numDomainID)
						VALUES (@numExtranetID,@numContactID,@InlineEditValue,1,@numDomainID)
					END
				END
				ELSE
				BEGIN
					RAISERROR('Organization e-commerce access is not enabled',16,1)
				END
			END

			SELECT @InlineEditValue AS vcData

			RETURN
		END
	END
	ELSE IF @vcLookBackTableName = 'CustomerPartNumber'
	BEGIN
		IF @vcDbColumnName='CustomerPartNo'
		BEGIN
			SELECT @numCompanyID=numCompanyID FROM DivisionMaster WHERE numDivisionID=@numDivisionID
			
			IF EXISTS(SELECT CustomerPartNoID FROM CustomerPartNumber WHERE numItemCode = @numRecId AND numCompanyID = @numCompanyID)
			BEGIN
				UPDATE CustomerPartNumber SET CustomerPartNo = @InlineEditValue WHERE numItemCode = @numRecId AND numCompanyId = @numCompanyID AND numDomainID = @numDomainId 
			END
			ELSE
			BEGIN
				INSERT INTO CustomerPartNumber
				(numItemCode, numCompanyId, numDomainID, CustomerPartNo)
				VALUES
				(@numRecId, @numCompanyID, @numDomainID, @InlineEditValue)
			END

			SELECT @InlineEditValue AS vcData

			RETURN
		END
		
	END
	ELSE IF @vcLookBackTableName = 'CheckHeader'
	BEGIN
		IF EXISTS (SELECT 1 FROM CheckHeader WHERE numDomainID=@numDomainID AND numCheckNo=@InlineEditValue AND numCheckHeaderID <> @numRecId)
		BEGIN
			RAISERROR('DUPLICATE_CHECK_NUMBER',16,1)
			RETURN
		END

		SET @strSql='UPDATE CheckHeader SET ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,dtModifiedDate=getutcdate() WHERE numCheckHeaderID=@numRecId and numDomainID=@numDomainID'
	END
	ELSE IF @vcLookBackTableName = 'Item'
	BEGIN
		IF @vcDbColumnName = 'monListPrice'
		BEGIN
			SET @InlineEditValue = REPLACE(@InlineEditValue,',','')

			IF EXISTS (SELECT numItemCode FROM Item WHERE numDomainID=@numDomainID AND numItemCode=@numRecId AND 1 = (CASE WHEN ISNULL(numItemGroup,0) > 0 THEN (CASE WHEN ISNULL(bitMatrix,0)=1 THEN 1 ELSE 0 END) ELSE 1 END))
			BEGIN
				UPDATE WareHouseItems SET monWListPrice=@InlineEditValue WHERE numDomainID=@numDomainID AND numItemID=@numRecId
				SELECT @InlineEditValue AS vcData
			END
			ELSE
			BEGIN
				RAISERROR('You are not allowed to edit list price for warehouse level matrix item.',16,1)
			END

			RETURN
		END

		SET @strSql='UPDATE Item set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numItemCode=@numRecId AND numDomainID=@numDomainID'
	END
	ELSE IF @vcLookBackTableName = 'Warehouses'
	BEGIN
		IF EXISTS (SELECT numOppId FROM OpportunityMaster WHERE numDomainId=@numDomainID AND numOppID=@numOppId AND ISNULL(tintOppStatus,0)=1 AND ISNULL(tintshipped,0)=0)
		BEGIN
			IF EXISTS (SELECT numWareHouseID FROM Warehouses WHERE numDomainID=@numDomainID AND numWareHouseID=@InlineEditValue)
			BEGIN
				DECLARE @numTempOppItemID NUMERIC(18,0)
				DECLARE @numTempItemCode NUMERIC(18,0)
				DECLARE @numOldWarehouseItemID NUMERIC(18,0)
				DECLARE @numNewWarehouseItemID NUMERIC(18,0)
				DECLARE @tintCommitAllocation TINYINT 

				SELECT @tintCommitAllocation=ISNULL(tintCommitAllocation,1) FROM Domain WHERE numDomainId=@numDomainID


				DECLARE @TEMPItems TABLE
				(
					ID INT IDENTITY(1,1)
					,numOppItemID NUMERIC(18,0)
					,numItemCode NUMERIC(18,0)
					,numWarehouseItemID NUMERIC(18,0)
				)
				INSERT INTO @TEMPItems
				(
					numOppItemID
					,numItemCode
					,numWarehouseItemID
				)
				SELECT
					numoppitemtCode
					,numItemCode
					,numWarehouseItmsID
				FROM 
					OpportunityItems
				WHERE
					numOppID=@numOppID
					AND (numOppItemtcode=@numOppItemId OR ISNULL(@numOppItemId,0) = 0)

				IF EXISTS (SELECT TI.numItemCode FROM @TEMPItems TI LEFT JOIN WareHouseItems WI ON TI.numItemCode=WI.numItemID AND WI.numWareHouseID=@InlineEditValue WHERE WI.numWareHouseItemID IS NULL)
				BEGIN
					RAISERROR('WAREHOUSE_DOES_NOT_EXISTS_IN_ITEM',16,1)
				END
				ELSE
				BEGIN
					DECLARE @j INT = 1
					DECLARE @jCount INT 

					SELECT @jCount=COUNT(*) FROM @TEMPItems

					WHILE @j <= @jCount
					BEGIN
						SELECT 
							@numTempOppItemID = numOppItemID
							,@numTempItemCode = numItemCode
							,@numOldWarehouseItemID = numWarehouseItemID
						FROM 
							@TEMPItems TI 
						WHERE 
							ID=@j

						SELECT TOP 1
							@numNewWarehouseItemID = numWareHouseItemID
						FROM
							WareHouseItems 
						WHERE 
							numDomainID=@numDomainID 
							AND numItemID=@numTempItemCode 
							AND numWareHouseID=@InlineEditValue
						ORDER BY
							ISNULL(numWLocationID,0) ASC

						IF @numOldWarehouseItemID <> @numNewWarehouseItemID
						BEGIN
							BEGIN TRY
							BEGIN TRANSACTION 
								--REVERTING BACK THE WAREHOUSE ITEMS                  
								IF @tintCommitAllocation=1
								BEGIN               
									EXEC USP_RevertDetailsOpp @numOppID,@numTempOppItemID,0,@numUserCntID                  
								END  

								UPDATE OpportunityItems SET numWarehouseItmsID=@numNewWarehouseItemID WHERE numoppitemtCode=@numTempOppItemID

								IF @tintCommitAllocation=1
								BEGIN               
									EXEC USP_UpdatingInventoryonCloseDeal @numOppID,@numTempOppItemID,@numUserCntID
								END  

								UPDATE OpportunityItems SET bitRequiredWarehouseCorrection=0 WHERE numoppitemtCode=@numTempOppItemID
							COMMIT
							END TRY
							BEGIN CATCH
								IF @@TRANCOUNT > 0
									ROLLBACK

								-- Raise an error with the details of the exception
								DECLARE @ErrMsg nvarchar(4000), @ErrSeverity int
								SELECT @ErrMsg = ERROR_MESSAGE(), @ErrSeverity = ERROR_SEVERITY()

								RAISERROR(@ErrMsg, @ErrSeverity, 1)
							END CATCH
						END

						SET @j = @j + 1
					END
				END
			END
			ELSE
			BEGIN
				RAISERROR('WAREHOUSE_DOES_NOT_EXISTS',16,1)
			END
		END
	END

	EXECUTE sp_executeSQL @strSql, N'@numDomainID NUMERIC(18,0),@numUserCntID NUMERIC(18,0),@numDivisionID NUMERIC(18,0),@numContactID NUMERIC(18,0),@InlineEditValue VARCHAR(8000),@numWOId NUMERIC(18,0),@numProId NUMERIC(18,0),@numOppId NUMERIC(18,0),@numOppItemId NUMERIC(18,0),@numCaseId NUMERIC(18,0),@numWODetailId NUMERIC(18,0),@numCommID Numeric(18,0),@numRecId NUMERIC(18,0)',
		     @numDomainID,@numUserCntID,@numDivisionID,@numContactID,@InlineEditValue,@numWOId,@numProId,@numOppId,@numOppItemId,@numCaseId,@numWODetailId,@numCommID,@numRecId;
		     
	IF @vcAssociatedControlType='SelectBox'                                                    
       BEGIN       
			IF @vcDbColumnName='tintSource' AND @vcLookBackTableName = 'OpportunityMaster'
			BEGIN
				SELECT dbo.fn_GetOpportunitySourceValue(@Value1,@Value2,@numDomainID) AS vcData                                             
			END
			ELSE IF @vcDbColumnName='numContactID'
			BEGIN
				SELECT dbo.fn_GetContactName(@InlineEditValue)
			END
			ELSE IF @vcDbColumnName='numPartner' OR @vcDbColumnName='numPartenerSource'
			BEGIN
				SET @intExecuteDiv=1
				SELECT D.vcPartnerCode+'-'+C.vcCompanyName  AS vcData FROM DivisionMaster AS D 
				LEFT JOIN CompanyInfo AS C
				ON D.numCompanyID=C.numCompanyID WHERE D.numDivisionID=@InlineEditValue
			END
			ELSE IF @vcDbColumnName='tintOppStatus' AND @intExecuteDiv=1
			BEGIN
				SELECT @InlineEditValue AS vcData
			END
			ELSE IF @vcDbColumnName='numReleaseStatus'
			BEGIN
				If @InlineEditValue = '1'
				BEGIN
					SELECT 'Open'
				END
				ELSE IF @InlineEditValue = '2'
				BEGIN
					SELECT 'Purchased'
				END
				ELSE
				BEGIN
					SELECT ''
				END
			END
			ELSE IF @vcDbColumnName='numPartenerContact'
			BEGIN
				SET @InlineEditValue=(SELECT TOP 1 A.vcGivenName FROM AdditionalContactsInformation AS A 
										LEFT JOIN DivisionMaster AS D 
										ON D.numDivisionID=A.numDivisionId
										WHERE D.numDomainID=@numDomainID AND A.numCOntactId=@InlineEditValue )
				SELECT @InlineEditValue AS vcData
			END
			ELSE IF @vcDbColumnName='numPartenerSource'
			BEGIN
				
				SET @InlineEditValue=(SELECT D.vcPartnerCode+'-'+C.vcCompanyName  AS vcData FROM DivisionMaster AS D 
				LEFT JOIN CompanyInfo AS C
				ON D.numCompanyID=C.numCompanyID WHERE D.numDivisionID=@InlineEditValue)

				SELECT @InlineEditValue AS vcData
			END
			ELSE IF @vcDbColumnName='numShippingService'
			BEGIN
				SELECT ISNULL((SELECT vcShipmentService FROM ShippingService WHERE (numDomainID=@numDomainID OR ISNULL(numDomainID,0)=0) AND numShippingServiceID = @InlineEditValue),'') AS vcData
			END
			ELSE IF @vcDbColumnName='intDropShip'
			BEGIN
				 SELECT CASE WHEN ISNULL(@InlineEditValue,0)=1 THEN 'Not Available'
						 WHEN ISNULL(@InlineEditValue,0)=2 THEN 'Blank Available'
						 WHEN ISNULL(@InlineEditValue,0)=3 THEN 'Vendor Label'
						 WHEN ISNULL(@InlineEditValue,0)=4 THEN 'Private Label'
						 ELSE 'Select One'
					END AS vcData
			END
			ELSE IF @vcDbColumnName = 'numProjectID'
			BEGIN
				SELECT ISNULL((SELECT vcProjectName FROM ProjectsMaster WHERE numDomainID=@numDomainID AND numProID=@InlineEditValue),'-') AS vcData
			END
			else
			BEGIN
				SELECT dbo.fn_getSelectBoxValue(@vcListItemType,@InlineEditValue,@vcDbColumnName) AS vcData                                             
			END
		end 
		ELSE
		BEGIN
			IF @vcDbColumnName='tintActive'
		  	BEGIN
				SET @InlineEditValue= CASE @InlineEditValue WHEN 1 THEN 'True' ELSE 'False' END 
			END
			ELSE IF @vcDbColumnName='vcWareHouse' AND ISNUMERIC(@InlineEditValue) = 1
			BEGIN
				SELECT @InlineEditValue=vcWareHouse FROM Warehouses WHERE numDomainID=@numDomainID AND numWareHouseID=@InlineEditValue
			END

			IF @vcAssociatedControlType = 'Check box' or @vcAssociatedControlType = 'Checkbox'
			BEGIN
	 			SET @InlineEditValue= Case @InlineEditValue WHEN 'True' THEN 'Yes' ELSE 'No' END
			END
			
		    SELECT @InlineEditValue AS vcData
		END  
		
	IF @vcLookBackTableName = 'AdditionalContactsInformation' AND @vcDbColumnName = 'numECampaignID'
	BEGIN
		--Manage ECampiagn When Campaign is changed from inline edit
		DECLARE @Date AS DATETIME 
		SELECT @Date = dbo.[GetUTCDateWithoutTime]()
		DECLARE @numECampaignID AS NUMERIC(18,0)
		SET @numECampaignID = @InlineEditValue

		IF @numECampaignID = -1 
		SET @numECampaignID=0
	 
 		EXEC [USP_ManageConEmailCampaign]
		@numConEmailCampID = 0, --  numeric(9, 0)
		@numContactID = @numContactID, --  numeric(9, 0)
		@numECampaignID = @numECampaignID, --  numeric(9, 0)
		@intStartDate = @Date, --  datetime
		@bitEngaged = 1, --  bit
		@numUserCntID = @numUserCntID --  numeric(9, 0)
	END  
END
ELSE
BEGIN	 

	SET @InlineEditValue=Cast(@InlineEditValue as varchar(1000))
	SELECT @vcAssociatedControlType=Fld_type
	 FROM CFW_Fld_Master WHERE Fld_Id=@numFormFieldId 

	IF @vcAssociatedControlType = 'CheckBox'
	BEGIN
	    SET @InlineEditValue=(case when @InlineEditValue='False' then 0 ELSE 1 END)
	END

	IF @vcPageName='organization'
	BEGIN 
		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
			BEGIN
				UPDATE CFW_FLD_Values SET Fld_Value=@InlineEditValue WHERE FldDTLID=(SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
			END	
		ELSE
			BEGIN
				insert into CFW_FLD_Values (Fld_ID,Fld_Value,RecId) VALUES (@numFormFieldId,@InlineEditValue,@numRecId)         
			END

			--Added By :Sachin Sadhu ||Date:13thOct2014
			--Purpose  :To manage CustomFields in Queue
			EXEC USP_CFW_FLD_Values_CT
					@numDomainID =@numDomainID,
					@numUserCntID =@numUserCntID,
					@numRecordID =@numRecId ,
					@numFormFieldId  =@numFormFieldId
  
	  END
	ELSE IF @vcPageName='contact'
	BEGIN 
		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Cont CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
			BEGIN
				UPDATE CFW_FLD_Values_Cont SET Fld_Value=@InlineEditValue WHERE FldDTLID=(SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Cont CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
			END	
		ELSE
			BEGIN
				insert into CFW_FLD_Values_Cont (Fld_ID,Fld_Value,RecId) VALUES (@numFormFieldId,@InlineEditValue,@numRecId)         
			END
			
			--Added By :Sachin Sadhu ||Date:13thOct2014
			--Purpose  :To manage CustomFields in Queue
			EXEC USP_CFW_FLD_Values_Cont_CT
					@numDomainID =@numDomainID,
					@numUserCntID =@numUserCntID,
					@numRecordID =@numRecId ,
					@numFormFieldId  =@numFormFieldId

	END
	ELSE IF @vcPageName='project'
	BEGIN 
		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Pro CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
			BEGIN
				UPDATE CFW_FLD_Values_Pro SET Fld_Value=@InlineEditValue WHERE FldDTLID=(SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Pro CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
			END	
		ELSE
			BEGIN
				insert into CFW_FLD_Values_Pro (Fld_ID,Fld_Value,RecId) VALUES (@numFormFieldId,@InlineEditValue,@numRecId)         
			END

			
			--Added By :Sachin Sadhu ||Date:14thOct2014
			--Purpose  :To manage CustomFields in Queue
			EXEC USP_CFW_FLD_Values_Pro_CT
					@numDomainID =@numDomainID,
					@numUserCntID =@numUserCntID,
					@numRecordID =@numRecId ,
					@numFormFieldId  =@numFormFieldId
	END
	ELSE IF @vcPageName='opp'
	BEGIN 
		DECLARE @dtDate AS DATETIME = GETUTCDATE()

		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_Fld_Values_Opp CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
		BEGIN
			UPDATE 
				CFW_Fld_Values_Opp 
			SET 
				Fld_Value=@InlineEditValue ,
				numModifiedBy = @numUserCntID,
				bintModifiedDate = @dtDate
			WHERE 
				FldDTLID= (SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_Fld_Values_Opp CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
		END	
		ELSE
		BEGIN
			INSERT INTO CFW_Fld_Values_Opp (Fld_ID,Fld_Value,RecId,numModifiedBy,bintModifiedDate) VALUES (@numFormFieldId,@InlineEditValue,@numRecId,@numUserCntID,@dtDate)         
		END

		UPDATE OpportunityMaster SET numModifiedBy=@numUserCntID,bintModifiedDate=@dtDate WHERE numOppID = @numRecId 

		--Added By :Sachin Sadhu ||Date:14thOct2014
		--Purpose  :To manage CustomFields in Queue
		EXEC USP_CFW_Fld_Values_Opp_CT
				@numDomainID =@numDomainID,
				@numUserCntID =@numUserCntID,
				@numRecordID =@numRecId ,
				@numFormFieldId  =@numFormFieldId

	END
	ELSE IF @vcPageName='oppitems'
	BEGIN 
		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_Fld_Values_OppItems CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
		BEGIN
			UPDATE 
				CFW_Fld_Values_OppItems 
			SET 
				Fld_Value=@InlineEditValue
			WHERE 
				FldDTLID= (SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_Fld_Values_OppItems CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
		END	
		ELSE
		BEGIN
			INSERT INTO CFW_Fld_Values_OppItems(Fld_ID,Fld_Value,RecId) VALUES (@numFormFieldId,@InlineEditValue,@numRecId)         
		END
	END
	ELSE IF @vcPageName='case'
	BEGIN 
		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Case CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
			BEGIN
				UPDATE CFW_FLD_Values_Case SET Fld_Value=@InlineEditValue WHERE FldDTLID=(SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Case CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
			END	
		ELSE
			BEGIN
				insert into CFW_FLD_Values_Case (Fld_ID,Fld_Value,RecId) VALUES (@numFormFieldId,@InlineEditValue,@numRecId)         
			END

			
			--Added By :Sachin Sadhu ||Date:14thOct2014
			--Purpose  :To manage CustomFields in Queue
			EXEC USP_CFW_FLD_Values_Case_CT
					@numDomainID =@numDomainID,
					@numUserCntID =@numUserCntID,
					@numRecordID =@numRecId ,
					@numFormFieldId  =@numFormFieldId
	END
	ELSE IF @vcPageName='item'
	BEGIN
		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Item CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
		BEGIN
			UPDATE CFW_FLD_Values_Item SET Fld_Value=@InlineEditValue WHERE FldDTLID=(SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Item CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
		END	
		ELSE
		BEGIN
			INSERT INTO CFW_FLD_Values_Item (Fld_ID,Fld_Value,RecId) VALUES (@numFormFieldId,@InlineEditValue,@numRecId)         
		END
	END


	IF @vcAssociatedControlType = 'SelectBox'            
	BEGIN            
		    
		 SELECT vcData FROM ListDetails WHERE numListItemID=@InlineEditValue            
	END
	ELSE IF @vcAssociatedControlType = 'CheckBoxList'
	BEGIN
		DECLARE @str AS VARCHAR(MAX)
		SET @str = 'SELECT STUFF((SELECT CONCAT('','', vcData) FROM ListDetails WHERE numlistitemid IN (' + (CASE WHEN LEN(@InlineEditValue) > 0 THEN @InlineEditValue ELSE '0' END) + ') FOR XML PATH('''')), 1, 1, '''')'
		EXEC(@str)
	END
	ELSE IF @vcAssociatedControlType = 'CheckBox'
	BEGIN
	 	select CASE @InlineEditValue WHEN 1 THEN 'Yes' ELSE 'No' END AS vcData
	END
	ELSE
	BEGIN
		SELECT @InlineEditValue AS vcData
	END 
END

 
/****** Object:  StoredProcedure [dbo].[usp_InsertCommunication]    Script Date: 07/26/2008 16:19:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Modified By Anoop jayaraj                 
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_insertcommunication')
DROP PROCEDURE usp_insertcommunication
GO
CREATE PROCEDURE [dbo].[usp_InsertCommunication]                                                
@numCommId numeric=0,                            
@bitTask numeric,                                        
@numContactId numeric,                            
@numDivisionId numeric,                            
@txtDetails text,                            
@numOppId numeric=0,                            
@numAssigned numeric=0,                                                
@numUserCntID numeric,                                                                                                                                               
@numDomainId numeric,                                                
@bitClosed tinyint,                                                
@vcCalendarName varchar(100)='',                                                                                                     
@dtStartTime datetime,                 
@dtEndtime datetime,                                                                             
@numActivity as numeric(9),                                              
@numStatus as numeric(9),                                              
@intSnoozeMins as int,                                              
@tintSnoozeStatus as tinyint,                                            
@intRemainderMins as int,                                            
@tintRemStaus as tinyint,                                  
@ClientTimeZoneOffset Int,                  
@bitOutLook tinyint,            
@bitSendEmailTemplate bit,            
@bitAlert bit,            
@numEmailTemplate numeric(9)=0,            
@tintHours tinyint=0,          
@CaseID  numeric=0 ,        
@CaseTimeId numeric =0,    
@CaseExpId numeric = 0  ,                     
@ActivityId numeric = 0 ,
@bitFollowUpAnyTime BIT,
@strAttendee TEXT,
@numLinkedOrganization NUMERIC(18,0) = 0,
@numLinkedContact NUMERIC(18,0) = 0,
@strContactIds VARCHAR(500)=''                                             
AS
BEGIN               
	DECLARE @CheckRecord AS BIT   
	SET @CheckRecord = 1                

	IF NOT EXISTS (SELECT * FROM communication WHERE numcommid=@numCommId)                                                
		SET @CheckRecord = 0                                                

	IF @numCommId = 0 OR @CheckRecord = 0                                            
	BEGIN
		IF(@strContactIds='')
		BEGIN
			IF ISNULL(@numDivisionId,0) = 0                         
			BEGIN
				IF ISNULL(@numContactId,0) > 0
				BEGIN
					SELECT @numDivisionId = numDivisionId FROM AdditionalContactsInformation WHERE numContactId = @numContactId
					
					IF ISNULL(@numDivisionId,0) = 0 
					BEGIN
						RAISERROR('COMPANY_NOT_FOUND_FOR_CONTACT' ,16,1)
						RETURN
					END                   
				END
				ELSE
				BEGIN
					RAISERROR('COMPANY_NOT_FOUND',16,1)
					RETURN	
				END
			END
  
			IF ISNULL(@numContactId,0) = 0 
			BEGIN
				RAISERROR('CONTACT_NOT_FOUND',16,1)
				RETURN
			END   
		END         
   
		IF @numAssigned=0  
			SET @numAssigned=@numUserCntID
			                                            
		IF(@strContactIds='')
		BEGIN
			INSERT INTO communication                  
			(                
				bitTask,                
				numContactId,                
				numDivisionId,                
				textDetails,                
				intSnoozeMins,                
				intRemainderMins,                
				numStatus,                
				numActivity,                
				numAssign,                
				tintSnoozeStatus,                
				tintRemStatus,                
				numOppId,                
				numCreatedby,                
				dtCreatedDate,                
				numModifiedBy,                
				dtModifiedDate,                
				numDomainID,                
				bitClosedFlag,                
				vcCalendarName,                
				bitOutlook,                
				dtStartTime,                
				dtEndTime,              
				numAssignedBy,            
				bitSendEmailTemp,            
				numEmailTemplate,            
				tintHours,            
				bitAlert,          
				CaseId,        
				CaseTimeId,                                                                                 
				CaseExpId,  
				numActivityId,
				bitFollowUpAnyTime            
			)                 
			VALUES                
			(                                              
				@bitTask,                
				@numcontactId,                                              
				@numDivisionId,                
				ISNULL(@txtdetails,''),                
				@intSnoozeMins,                
				@intRemainderMins,                
				@numStatus,                
				@numActivity,                                            
				@numAssigned,                
				@tintSnoozeStatus,                
				@tintRemStaus,                                             
				@numoppid,                
				@numUserCntID,                
				getutcdate(),                
				@numUserCntID,                                              
				getutcdate(),                
				@numDomainId,                
				@bitClosed,                
				@vcCalendarName,                
				@bitOutLook,                                              
				DateAdd(minute, @ClientTimeZoneOffset, @dtStartTime),                
				DateAdd(minute, @ClientTimeZoneOffset,@dtEndtime),              
				@numUserCntID,            
				@bitSendEmailTemplate,            
				@numEmailTemplate,            
				@tintHours,            
				@bitAlert,          
				@CaseID,        
				@CaseTimeId,                                                                                 
				@CaseExpId,  
				@ActivityId,
				@bitFollowUpAnyTime 
			)  

			SET @numCommId= SCOPE_IDENTITY() 

			IF NOT EXISTS(SELECT * FROM CommunicationAttendees WHERE numCommId=@numCommId AND numContactId=@numcontactId)
			BEGIN
				INSERT INTO CommunicationAttendees(numCommId,numContactId) VALUES(@numCommId,@numcontactId)
			END
			IF NOT EXISTS(SELECT * FROM CommunicationAttendees WHERE numCommId=@numCommId AND numContactId=@numAssigned)
			BEGIN
				INSERT INTO CommunicationAttendees(numCommId,numContactId) VALUES(@numCommId,@numAssigned)
			END
			IF NOT EXISTS(SELECT * FROM CommunicationAttendees WHERE numCommId=@numCommId AND numContactId=@numUserCntID)
			BEGIN
				INSERT INTO CommunicationAttendees(numCommId,numContactId) VALUES(@numCommId,@numUserCntID)
			END
		END
		ELSE
		BEGIN
			INSERT INTO COMMUNICATION                  
			(                
				bitTask,                
				numContactId,                
				numDivisionId,                
				textDetails,                
				intSnoozeMins,                
				intRemainderMins,                
				numStatus,                
				numActivity,                
				numAssign,                
				tintSnoozeStatus,                
				tintRemStatus,                
				numOppId,                
				numCreatedby,                
				dtCreatedDate,                
				numModifiedBy,                
				dtModifiedDate,                
				numDomainID,                
				bitClosedFlag,                
				vcCalendarName,                
				bitOutlook,                
				dtStartTime,                
				dtEndTime,              
				numAssignedBy,            
				bitSendEmailTemp,            
				numEmailTemplate,            
				tintHours,            
				bitAlert,          
				CaseId,        
				CaseTimeId,                                                                                 
				CaseExpId,  
				numActivityId,
				bitFollowUpAnyTime            
			)                 
			SELECT                                          
				@bitTask,                
				CAST(Items AS NUMERIC(9)),                                              
				(SELECT TOP 1 numDivisionId FROM AdditionalContactsInformation WHERE numContactId=CAST(Items AS NUMERIC(9))),                
				ISNULL(@txtdetails,''),                
				@intSnoozeMins,                
				@intRemainderMins,                
				@numStatus,                
				@numActivity,                                            
				@numAssigned,                
				@tintSnoozeStatus,                
				@tintRemStaus,                                             
				@numoppid,                
				@numUserCntID,                
				getutcdate(),                
				@numUserCntID,                                              
				getutcdate(),                
				@numDomainId,                
				@bitClosed,                
				@vcCalendarName,                
				@bitOutLook,                                              
				DateAdd(minute, @ClientTimeZoneOffset, @dtStartTime),                
				DateAdd(minute, @ClientTimeZoneOffset,@dtEndtime),              
				@numUserCntID,            
				@bitSendEmailTemplate,            
				@numEmailTemplate,            
				@tintHours,            
				@bitAlert,          
				@CaseID,        
				@CaseTimeId,                                                                                 
				@CaseExpId,  
				@ActivityId ,@bitFollowUpAnyTime 
			FROM 
				dbo.Split(@strContactIds,',') 
			WHERE 
				Items<>'' AND Items<>'0'

			SET @numCommId= SCOPE_IDENTITY() 
		END                                                           
   
		IF ISNULL(@numLinkedOrganization,0) > 0
		BEGIN
			INSERT INTO CommunicationLinkedOrganization VALUES (@numCommId,@numLinkedOrganization,@numLinkedContact)
		END                                          
	END                                                
	ELSE                    
	BEGIN
    ---Updating if Action Item is assigned to someone                      
		DECLARE @tempAssignedTo AS NUMERIC(18,0)                    
		SET @tempAssignedTo=null                     
		SELECT @tempAssignedTo=isnull(numAssign,0) FROM communication WHERE numcommid=@numCommId                     
	                 
		IF (@tempAssignedTo <> @numAssigned AND @numAssigned<>'0')                    
		BEGIN                      
			UPDATE communication SET numAssignedBy=@numUserCntID WHERE numcommid=@numCommId              
		END                     
		ELSE IF (@numAssigned =0)                    
		BEGIN                    
			UPDATE communication SET numAssignedBy=0 WHERE numcommid=@numCommId                  
		END                
                                              
		IF @vcCalendarName=''                                                 
		BEGIN
			UPDATE 
				communication 
			SET                 
				bittask=@bittask,                                                                              
				textdetails=@txtdetails,                                               
				intSnoozeMins=@intSnoozeMins,                                               
				numStatus=@numStatus,                                              
				numActivity=@numActivity,                                             
				tintSnoozeStatus=@tintSnoozeStatus,                                            
				intRemainderMins= @intRemainderMins,                                            
				tintRemStatus= @tintRemStaus,                                              
				numassign=@numAssigned,                                                
				nummodifiedby=@numUserCntID,                                                
				dtModifiedDate=getutcdate(),                                                
				bitClosedFlag=@bitClosed,                                                        
				bitOutLook=@bitOutLook,                
				dtStartTime=DateAdd(minute, @ClientTimeZoneOffset, @dtStartTime),                
				dtEndTime=DateAdd(minute, @ClientTimeZoneOffset,@dtEndtime),            
				bitSendEmailTemp=@bitSendEmailTemplate,            
				numEmailTemplate=@numEmailTemplate,            
				tintHours=@tintHours,            
				bitAlert=@bitAlert,       
				CaseID=@CaseID,        
				CaseTimeId=@CaseTimeId,                                                                                 
				CaseExpId=@CaseExpId  ,  
				numActivityId=@ActivityId ,bitFollowUpAnyTime=@bitFollowUpAnyTime,
				dtEventClosedDate=Case When isnull(@bitClosed,0)=1 then getutcdate() else null end                                 
			WHERE 
				numcommid=@numCommId                                                
		END                                                
		ELSE                                                
		BEGIN
			UPDATE 
				communication 
			SET                 
				bittask=@bittask,                                                                              
				textdetails=@txtdetails,                                               
				intSnoozeMins=@intSnoozeMins,                                
				numStatus=@numStatus,                                              
				numActivity=@numActivity,                                             
				tintSnoozeStatus=@tintSnoozeStatus,                                            
				intRemainderMins= @intRemainderMins,                                            
				tintRemStatus= @tintRemStaus,        
				numassign=@numAssigned,                                                
				nummodifiedby=@numUserCntID,                                                
				dtModifiedDate=getutcdate(),                                                
				bitClosedFlag=@bitClosed,                                                        
				bitOutLook=@bitOutLook,                
				dtStartTime=DateAdd(minute, @ClientTimeZoneOffset, @dtStartTime),                
				dtEndTime=DateAdd(minute, @ClientTimeZoneOffset,@dtEndtime),                                                 
				vcCalendarName=@vcCalendarName,            
				bitSendEmailTemp=@bitSendEmailTemplate,            
				numEmailTemplate=@numEmailTemplate,            
				tintHours=@tintHours,            
				bitAlert=@bitAlert ,      
				CaseID=@CaseID,        
				CaseTimeId=@CaseTimeId,                                                                                 
				CaseExpId=@CaseExpId  ,  
				numActivityId=@ActivityId  ,bitFollowUpAnyTime=@bitFollowUpAnyTime,
				dtEventClosedDate=Case When isnull(@bitClosed,0)=1 then getutcdate() else null end 
			WHERE 
				numcommid=@numCommId                                                
		END  
   
		IF ISNULL(@numLinkedOrganization,0) > 0
		BEGIN
			IF EXISTS (SELECT numCLOID FROM CommunicationLinkedOrganization WHERE numCommID=@numCommId)
			BEGIN
				UPDATE CommunicationLinkedOrganization SET numDivisionID=@numLinkedOrganization,numContactID=@numLinkedContact WHERE numCommID=@numCommId
			END
			ELSE
			BEGIN
				INSERT INTO CommunicationLinkedOrganization VALUES (@numCommId,@numLinkedOrganization,@numLinkedContact)
			END
		END
		ELSE
		BEGIN
			DELETE FROM CommunicationLinkedOrganization WHERE numCommID=@numCommId
		END                                         
	END 
   
	SELECT @numCommId
END
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ManageBillHeader' ) 
    DROP PROCEDURE USP_ManageBillHeader
GO

CREATE PROCEDURE USP_ManageBillHeader
    @numBillID [numeric](18, 0) OUTPUT,
    @numDivisionID [numeric](18, 0),
    @numAccountID [numeric](18, 0),
    @dtBillDate [datetime],
    @numTermsID [numeric](18, 0),
    @numDueAmount [numeric](18, 2),
    @dtDueDate [datetime],
    @vcMemo [varchar](1000),
    @vcReference [varchar](500),
    @numBillTotalAmount [numeric](18, 2),
    @bitIsPaid [bit],
    @numDomainID [numeric](18, 0),
    @strItems VARCHAR(MAX),
    @numUserCntID NUMERIC(18, 0),
	@numOppId NUMERIC(18,0)=0,
	@bitLandedCost BIT=0,
	@numCurrencyID NUMERIC(18,0) = 0
AS 
    SET NOCOUNT ON  
    BEGIN TRY 
        BEGIN TRAN  
        --Validation of closed financial year
		EXEC dbo.USP_ValidateFinancialYearClosingDate @numDomainID,@dtBillDate

		DECLARE @fltExchangeRate AS FLOAT

		IF ISNULL(@numCurrencyID,0) = 0
		BEGIN
			SET @fltExchangeRate=1
			SELECT @numCurrencyID=numCurrencyID FROM Domain WHERE numDomainId = @numDomainID
		END
		ELSE
		BEGIN 
			SET @fltExchangeRate = dbo.GetExchangeRate(@numDomainID,@numCurrencyID)
        END  
        
        IF EXISTS ( SELECT  [numBillID]
                    FROM    [dbo].[BillHeader]
                    WHERE   [numBillID] = @numBillID ) 
            BEGIN
                UPDATE  [dbo].[BillHeader]
                SET     numDivisionID = @numDivisionID,
                        numAccountID = @numAccountID,
                        dtBillDate = @dtBillDate,
                        numTermsID = @numTermsID,
                        monAmountDue = @numDueAmount,
                        dtDueDate = @dtDueDate,
                        vcMemo = @vcMemo,
                        vcReference = @vcReference,
                        --monAmtPaid = @numBillTotalAmount,
                        bitIsPaid = (CASE WHEN ISNULL(monAmtPaid,0)=@numDueAmount THEN 1
									ELSE 0 END),
                        numDomainID = @numDomainID,
                        dtModifiedDate = GETUTCDATE(),
                        numModifiedBy = @numUserCntID,
						numCurrencyID=@numCurrencyID,
						fltExchangeRate = @fltExchangeRate
                WHERE   [numBillID] = @numBillID
                        AND numDomainID = @numDomainID
            END
        ELSE 
            BEGIN

				-- GET ACCOUNT CLASS IF ENABLED
				DECLARE @numAccountClass AS NUMERIC(18) = 0

				DECLARE @tintDefaultClassType AS INT = 0
				SELECT @tintDefaultClassType = ISNULL(tintDefaultClassType,0) FROM Domain WHERE numDomainId = @numDomainID 

				IF @tintDefaultClassType = 1 --USER
				BEGIN
					SELECT 
						@numAccountClass=ISNULL(numDefaultClass,0) 
					FROM 
						dbo.UserMaster UM 
					JOIN 
						dbo.Domain D 
					ON 
						UM.numDomainID=D.numDomainId
					WHERE 
						D.numDomainId=@numDomainId 
						AND UM.numUserDetailId=@numUserCntID
				END
				ELSE IF @tintDefaultClassType = 2 --COMPANY
				BEGIN
					SELECT 
						@numAccountClass=ISNULL(numAccountClassID,0) 
					FROM 
						dbo.DivisionMaster DM 
					WHERE 
						DM.numDomainId=@numDomainId 
						AND DM.numDivisionID=@numDivisionID
				END
				ELSE
				BEGIN
					SET @numAccountClass = 0
				END
            
                INSERT  INTO [dbo].[BillHeader]
                        (
                          [numDivisionID],
                          [numAccountID],
                          [dtBillDate],
                          [numTermsID],
                          [monAmountDue],
                          [dtDueDate],
                          [vcMemo],
                          [vcReference],
                          --[monAmtPaid],
                          [bitIsPaid],
                          [numDomainID],
                          [numCreatedBy],
                          [dtCreatedDate],numAccountClass,[numOppId],[bitLandedCost],[numCurrencyID],[fltExchangeRate]
                        )
                VALUES  (
                          @numDivisionID,
                          @numAccountID,
                          @dtBillDate,
                          @numTermsID,
                          @numDueAmount,
                          @dtDueDate,
                          @vcMemo,
                          @vcReference,
                          --@numBillTotalAmount,
                          @bitIsPaid,
                          @numDomainID,
                          @numUserCntID,
                          GETUTCDATE(),@numAccountClass,@numOppId,@bitLandedCost,@numCurrencyID,@fltExchangeRate
                        )
                SET @numBillID = SCOPE_IDENTITY()
            END
				
		PRINT @numBillID		
--        DELETE  FROM dbo.BillDetails
--        WHERE   numBillID = @numBillID 
				
        DECLARE @hDocItem INT
        IF CONVERT(VARCHAR(MAX), @strItems) <> '' 
        BEGIN
            EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems
                
            DELETE FROM BillDetails WHERE numBillID = @numBillID AND numBillDetailID NOT IN (SELECT X.numBillDetailID FROM OPENXML(@hDocItem,'/NewDataSet/Table1 [numBillDetailID>0]',2) WITH(numBillDetailID NUMERIC(18,0))X)
			                                                           
			--Update transactions
			Update 
				BillDetails 
			SET 
				numExpenseAccountID=X.numExpenseAccountID
				,monAmount=X.monAmount
				,vcDescription=X.vcDescription
				,numProjectID=X.numProjectID
				,numClassID=X.numClassID
				,numCampaignID=X.numCampaignID
				,numOppItemID=X.numOppItemID
				,numOppID=X.numOppID
			FROM 
			(
				SELECT 
					* 
				FROM 
					OPENXML(@hDocItem,'/NewDataSet/Table1 [numBillDetailID>0]',2)                                                                                                              
				WITH
				(                                                                                       
					numBillDetailID NUMERIC(18,0)
					,[numExpenseAccountID] NUMERIC(18,0)
					,[monAmount] DECIMAL(20,5)
					,[vcDescription] VARCHAR(1000)
					,[numProjectID] NUMERIC(18,0)
					,[numClassID] NUMERIC(18,0)
					,[numCampaignID] NUMERIC(18,0)
					,numOppItemID NUMERIC(18,0)
					,numOppID NUMERIC(18,0)                                                                                             
				)
			)X                                                                                                  
			WHERE 
				BillDetails.numBillDetailID=X.numBillDetailID 
				AND numBillID = @numBillID                                                                                             
			               
			
            INSERT INTO [dbo].[BillDetails]
            (
                [numBillID],
                [numExpenseAccountID],
                [monAmount],
                [vcDescription],
                [numProjectID],
                [numClassID],
                [numCampaignID]
				,numOppItemID
				,numOppID
            )
            SELECT  
				@numBillID,
                X.[numExpenseAccountID],
                X.[monAmount],
                X.[vcDescription],
                X.[numProjectID],
                X.[numClassID],
                X.[numCampaignID]
				,X.numOppItemID
				,X.numOppID
            FROM
			(
				SELECT 
					*
                FROM 
					OPENXML(@hDocItem,'/NewDataSet/Table1 [numBillDetailID=0]',2)
                WITH 
				(
					numBillDetailID NUMERIC(18,0)
					,[numExpenseAccountID] NUMERIC(18,0)
					,[monAmount] DECIMAL(20,5)
					,[vcDescription] VARCHAR(1000)
					,[numProjectID] NUMERIC(18,0)
					,[numClassID] NUMERIC(18,0)
					,[numCampaignID] NUMERIC(18,0)
					,numOppItemID NUMERIC(18,0)
					,numOppID NUMERIC(18,0)
				)
            ) X
            
			EXEC sp_xml_removedocument @hDocItem
        END

		IF EXISTS (SELECT numBillDetailID FROM BillDetails WHERE numBillID=@numBillID AND numExpenseAccountID NOT IN (SELECT numAccountId FROM Chart_Of_Accounts WHERE numDomainId=@numDomainID))
		BEGIN
			RAISERROR('INVALID_EXPENSE_ACCOUNT',16,1)
		END
		-- Update leads,prospects to Accounts
		EXEC dbo.USP_UpdateCRMTypeToAccounts @numDivisionID ,@numDomainID ,@numUserCntID
		
        COMMIT TRAN 
    END TRY 
    BEGIN CATCH		
        DECLARE @strMsg VARCHAR(200)
        SET @strMsg = ERROR_MESSAGE()
        IF ( @@TRANCOUNT > 0 ) 
            BEGIN
                RAISERROR ( @strMsg, 16, 1 ) ;
                ROLLBACK TRAN
                RETURN 1
            END
    END CATCH	
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityMaster_GetPOForMerge')
DROP PROCEDURE dbo.USP_OpportunityMaster_GetPOForMerge
GO
CREATE PROCEDURE [dbo].[USP_OpportunityMaster_GetPOForMerge]
(
	@numDomainID NUMERIC(18,0)
	,@numWarehouseID NUMERIC(18,0)
	,@bitOnlyMergable BIT
	,@bitExcludePOSendToVendor BIT
	,@bitDisplayOnlyCostSaving BIT
	,@tintCostType TINYINT
	,@ClientTimeZoneOffset INT
)
AS 
BEGIN
	DECLARE @TEMP TABLE
	(
		numDivisionID NUMERIC(18,0)
		,numOppId NUMERIC(18,0)
		,numItemCode NUMERIC(18,0)
		,vcCompanyName VARCHAR(300)
		,vcNavigationURL VARCHAR(300)
		,vcPOppName VARCHAR(300)
		,vcCreatedBy VARCHAR(300)
		,bintCreatedDate VARCHAR(50)
		,monDealAmount DECIMAL(20,5)
		,vcItemName VARCHAR(300)
		,vcSKU VARCHAR(100)
		,fltWeight FLOAT
		,numoppitemtCode NUMERIC(18,0)
		,numUnitHour FLOAT
		,monPrice DECIMAL(20,5)
		,vcItemCostSaving VARCHAR(MAX)
	)

	INSERT INTO @TEMP
	(
		numDivisionID
		,numOppId
		,numItemCode
		,vcCompanyName
		,vcNavigationURL
		,vcPOppName
		,vcCreatedBy
		,bintCreatedDate
		,monDealAmount
		,vcItemName
		,vcSKU
		,fltWeight
		,numoppitemtCode
		,numUnitHour
		,monPrice
		,vcItemCostSaving
	)
	SELECT
		DivisionMaster.numDivisionID
		,OpportunityMaster.numOppId
		,Item.numItemCode
		,CompanyInfo.vcCompanyName
		,(CASE DivisionMaster.tintCRMType 
			WHEN 2 THEN CONCAT('../account/frmAccounts.aspx?DivID=',DivisionMaster.numDivisionID) 
			WHEN 1 THEN CONCAT('../prospects/frmProspects.aspx?DivID=',DivisionMaster.numDivisionID) 
			ELSE CONCAT('../Leads/frmLeads.aspx?DivID=',DivisionMaster.numDivisionID) 
		END)
		,OpportunityMaster.vcPOppName
		,dbo.fn_GetContactName(OpportunityMaster.numCreatedBy) vcCreatedBy
		,dbo.FormatedDateFromDate(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,OpportunityMaster.bintCreatedDate),@numDomainID) bintCreatedDate
		,OpportunityMaster.monDealAmount
		,Item.vcItemName
		,Item.vcSKU
		,ISNULL(Item.fltWeight,0)
		,OpportunityItems.numoppitemtCode
		,ISNULL(OpportunityItems.numUnitHour,0)
		,ISNULL(OpportunityItems.monPrice,0)
		,(CASE 
			WHEN @tintCostType = 2 
			THEN ISNULL(STUFF((SELECT 
					CONCAT(', <input type="button" class="btn btn-xs btn-success" value="$" onClick="return RecordSelected(this,',OM.numDivisionId,',',ISNULL(OI.monPrice,0),',',ISNULL(V.intMinQty,0),')" /> <b>$'
					,FORMAT((ISNULL(OpportunityItems.numUnitHour,0) * ISNULL(OI.monPrice,0)) - (ISNULL(OpportunityItems.numUnitHour,0) * ISNULL(OpportunityItems.monPrice,0)), '#,##0.#####')
					,'</b> (',ISNULL(OI.numUnitHour,0),' U) '
					,CONCAT('<span style="color:',(CASE
								WHEN (DATEDIFF(DAY,OM.bintCreatedDate,GETUTCDATE()) = 0 AND DATEDIFF(DAY,OM.bintCreatedDate,GETUTCDATE()) <= 90)
								THEN '#00b050'
								WHEN (DATEDIFF(DAY,OM.bintCreatedDate,GETUTCDATE()) = 91 AND DATEDIFF(DAY,OM.bintCreatedDate,GETUTCDATE()) <= 180)
								THEN '#ff9933'
								WHEN (DATEDIFF(DAY,OM.bintCreatedDate,GETUTCDATE()) = 181 AND DATEDIFF(DAY,OM.bintCreatedDate,GETUTCDATE()) <= 270)
								THEN '#9148c8'
								ELSE '#ff0000'
							END),'">',DATEDIFF(DAY,OM.bintCreatedDate,GETUTCDATE()),'d</span> ')
						,'<b><a target="_blank" href="../opportunity/frmOpportunities.aspx?OpID=',OM.numOppId,'">',OM.vcPOppName,'</a></b>'
						,' (',CI.vcCompanyName,')') 
				FROM 
					OpportunityMaster OM 
				INNER JOIN
					DivisionMaster DM
				ON
					OM.numDivisionId = DM.numDivisionID
				INNER JOIN
					CompanyInfo CI
				ON
					DM.numCompanyID = CI.numCompanyId
				INNER JOIN 
					OpportunityItems OI 
				ON 
					OM.numOppID=OI.numOppID 
				LEFT JOIN
					Vendor V
				ON
					V.numVendorID = DM.numDivisionID
					AND V.numItemCode = OI.numItemCode
				WHERE 
					OM.numDomainId=@numDomainID 
					AND OM.numOppId <> OpportunityMaster.numOppId
					AND OM.tintOppType = 2 
					AND OM.tintOppStatus = 1 
					AND ISNULL(OM.bitStockTransfer,0) = 0
					AND OI.numItemCode = Item.numItemCode
					AND 1 = (CASE 
								WHEN ISNULL(@bitDisplayOnlyCostSaving,0) = 1 
								THEN (CASE WHEN ISNULL(OI.monPrice,0) <> 0 AND ISNULL(OI.monPrice,0) < ISNULL(OpportunityItems.monPrice,0) THEN 1 ELSE 0 END)
								ELSE 1 
							END)
					AND DATEDIFF(DAY,OM.bintCreatedDate,GETUTCDATE()) <= 600
				ORDER BY
					(ISNULL(OpportunityItems.numUnitHour,0) * ISNULL(OI.monPrice,0)) - (ISNULL(OpportunityItems.numUnitHour,0) * ISNULL(OpportunityItems.monPrice,0)) ASC OFFSET 0 ROWS FETCH NEXT 5 ROWS ONLY
				FOR XML PATH(''), TYPE).value('.', 'varchar(max)'),1,1,''),'') 
			ELSE ''
		END) vcItemCostSaving
	FROM
		OpportunityMaster
	INNER JOIN
		DivisionMaster
	ON
		OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
	INNER JOIN
		CompanyInfo
	ON
		DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
	INNER JOIN
		OpportunityItems
	ON
		OpportunityMaster.numOppId = OpportunityItems.numOppId
	INNER JOIN
		Item
	ON
		OpportunityItems.numItemCode = Item.numItemCode
	INNER JOIN
		WareHouseItems
	ON
		OpportunityItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
	WHERE
		OpportunityMaster.numDomainId=@numDomainID
		AND OpportunityMaster.tintOppType = 2
		AND OpportunityMaster.tintOppStatus = 1
		AND ISNULL(OpportunityMaster.bitStockTransfer,0) = 0
		AND ISNULL(OpportunityMaster.tintshipped,0) = 0
		AND (SELECT COUNT(*) FROM OpportunityBizDocs WHERE OpportunityBizDocs.numOppId=OpportunityMaster.numOppId) = 0
		AND WareHouseItems.numWareHouseID=@numWarehouseID
		AND ISNULL(OpportunityItems.numUnitHourReceived,0) = 0
		AND 1 = (CASE WHEN ISNULL(@bitExcludePOSendToVendor,0)=1 THEN (CASE WHEN EXISTS (SELECT numCorrespondenceID FROM Correspondence WHERE numOpenRecordID=OpportunityMaster.numOppId AND tintCorrType IN (2,4)) THEN 0 ELSE 1 END) ELSE 1 END)
		AND 1 = (CASE 
					WHEN @tintCostType = 2 
					THEN (CASE WHEN EXISTS (SELECT 
												OI.numoppitemtCode
											FROM 
												OpportunityMaster OM 
											INNER JOIN
												DivisionMaster DM
											ON
												OM.numDivisionId = DM.numDivisionID
											INNER JOIN
												CompanyInfo CI
											ON
												DM.numCompanyID = CI.numCompanyId
											INNER JOIN 
												OpportunityItems OI 
											ON 
												OM.numOppID=OI.numOppID 
											WHERE 
												OM.numDomainId=@numDomainID 
												AND OM.numOppId <> OpportunityMaster.numOppId
												AND OM.tintOppType = 2 
												AND OM.tintOppStatus = 1 
												AND ISNULL(OM.bitStockTransfer,0) = 0
												AND OI.numItemCode = Item.numItemCode
												AND 1 = (CASE 
															WHEN ISNULL(@bitDisplayOnlyCostSaving,0) = 1 
															THEN (CASE WHEN ISNULL(OI.monPrice,0) <> 0 AND ISNULL(OI.monPrice,0) < ISNULL(OpportunityItems.monPrice,0) THEN 1 ELSE 0 END)
															ELSE 1 
														END)
												AND DATEDIFF(DAY,OM.bintCreatedDate,GETUTCDATE()) <= 600) THEN 1 ELSE 0 END)
					ELSE 1 
				END)
	
	IF ISNULL(@bitOnlyMergable,0) = 1
	BEGIN
		DELETE FROM @TEMP WHERE numDivisionID IN (SELECT numDivisionID FROM (SELECT numDivisionID,numOppId FROM @TEMP GROUP BY numDivisionID,numOppId) TEMP GROUP BY numDivisionID HAVING COUNT(*) < 2)
	END

	SELECT 
		numDivisionID
		,vcCompanyName
		,vcNavigationURL
		,CONCAT('[',STUFF((SELECT 
					CONCAT(', {"intType":"',PurchaseIncentives.intType ,'", "vcIncentives":"',PurchaseIncentives.vcIncentives,'", "vcbuyingqty":"',PurchaseIncentives.vcbuyingqty,'", "tintDiscountType":"',ISNULL(PurchaseIncentives.tintDiscountType,0),'"}') 
				FROM 
					PurchaseIncentives 
				WHERE
					PurchaseIncentives.numDivisionID = T.numDivisionID
				FOR XML PATH('')),1,1,''),']') AS vcPurchaseIncentive 
	FROM 
		@TEMP T 
	GROUP BY 
		numDivisionID,vcCompanyName,vcNavigationURL
	ORDER BY
		T.vcCompanyName

	SELECT
		numDivisionID
		,numOppId
		,vcPOppName
		,vcCreatedBy
		,bintCreatedDate
		,monDealAmount
		,(CASE WHEN EXISTS (SELECT numCorrespondenceID FROM Correspondence WHERE numOpenRecordID=T.numOppId AND tintCorrType IN (2,4)) THEN 1 ELSE 0 END) AS bitPOSendToVendor
		,ISNULL((SELECT TOP 1
					(CASE 
						WHEN EmailHistory.numEmailHstrID IS NOT NULL 
						THEN CONCAT('<i></i>',dbo.FormatedDateFromDate(DateAdd(minute,-@ClientTimeZoneOffset,EmailHistory.dtReceivedOn),@numDomainID))
						ELSE dbo.FormatedDateFromDate(DateAdd(minute,-@ClientTimeZoneOffset, dtCreatedDate),@numDomainID)
					END) 
				FROM 
					Correspondence 
				LEFT JOIN 
					EmailHistory 
				ON 
					Correspondence.numEmailHistoryID=EmailHistory.numEmailHstrID 
				LEFT JOIN 
					Communication
				ON
					Correspondence.numCommID = Communication.numCommId
				WHERE 
					numOpenRecordID=T.numOppId
					AND tintCorrType IN (2,4)),'') vcPOSend
	FROM
		@TEMP T
	GROUP BY
		numDivisionID,numOppId,vcPOppName,vcCreatedBy,bintCreatedDate,monDealAmount
	ORDER BY
		numOppId

	SELECT
		numOppId
		,numoppitemtCode
		,numItemCode
		,vcItemName
		,vcSKU
		,fltWeight
		,numUnitHour
		,monPrice
		,vcItemCostSaving
	FROM 
		@TEMP T
END
GO
/****** Object:  StoredProcedure [dbo].[USP_ProjectsDTLPL]    Script Date: 07/26/2008 16:20:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_projectsdtlpl')
DROP PROCEDURE usp_projectsdtlpl
GO
CREATE PROCEDURE [dbo].[USP_ProjectsDTLPL]                                   
(                                    
@numProId numeric(9)=null  ,                  
@numDomainID numeric(9),    
@ClientTimeZoneOffset as int                                     
)                                    
as                                    
                                    
begin                                    
                                                    
                                    
 select  pro.vcProjectName,
		 pro.numintPrjMgr,
		 dbo.fn_GetContactName(pro.numintPrjMgr) as numintPrjMgrName,                              
		 pro.intDueDate,                                    
		 dbo.fn_GetContactName(pro.numCustPrjMgr) as numCustPrjMgrName,
		 pro.numCustPrjMgr,                                    
         pro.txtComments,
		 [dbo].[fn_GetContactName](pro.numAssignedTo ) as numAssignedToName, 
		 pro.numAssignedTo,                  
        (select  count(*) from GenericDocuments   where numRecID=@numProId and  vcDocumentSection='P') as DocumentCount,
		CASE 
           WHEN tintProStatus = 1 THEN 100
           ELSE isnull((SELECT SUM(tintPercentage)
                 FROM   ProjectsStageDetails
                 WHERE  numProId = @numProId
                        AND bitStageCompleted = 1),0)
         END AS TProgress,
         (select  count(*) from [ProjectsOpportunities] PO Left JOIN [OpportunityMaster] OM ON OM.[numOppId] = PO.[numOppId] WHERE [numProId]=@numProId) as LinkedOrderCount,
         vcProjectID,
         '' TimeAndMaterial,
         numContractId,
         ISNULL((SELECT vcContractName FROM dbo.ContractManagement WHERE numContractId=pro.numContractId),'') numContractIdName,
         isnull(numProjectType,0) numProjectType,
         dbo.fn_GetListItemName(numProjectType) vcProjectTypeName,isnull(Pro.numProjectStatus,0) as numProjectStatus,
dbo.fn_GetListItemName(numProjectStatus) vcProjectStatusName,
(SELECT SUBSTRING(
	(SELECT ','+  A.vcFirstName+' ' +A.vcLastName + CASE WHEN ISNULL(SR.numContactType,0)>0 THEN '(' + dbo.fn_GetListItemName(SR.numContactType) + ')' ELSE '' END
	FROM ShareRecord SR JOIN UserMaster UM ON UM.numUserDetailId=SR.numAssignedTo 
							 JOIN AdditionalContactsInformation A ON UM.numUserDetailId=A.numContactID 
		 WHERE SR.numDomainID=pro.numDomainID AND SR.numModuleID=5 AND SR.numRecordID=pro.numProId
				AND UM.numDomainID=pro.numDomainID and UM.numDomainID=A.numDomainID FOR XML PATH('')),2,200000)) AS numShareWith ,
	isnull(AD.vcStreet,'')+ 
	' <br/>'+ isnull(AD.vcCity,'')+ ' ,'
	         + isnull(dbo.fn_GetState(AD.numState),'')+ ' '
	         + isnull(AD.vcPostalCode,'')+ ' <br>' 
	         + isnull(dbo.fn_GetListItemName(AD.numCountry),'') as ShippingAddress,
pro.dtCompletionDate AS vcProjectedFinish,
NULL AS dtActualStartDate,
pro.dtmEndDate,
pro.dtmStartDate,
ISNULL(pro.numClientBudget,0) AS numClientBudget,
0 AS numClientBudgetBalance
from ProjectsMaster pro    
LEFT JOIN dbo.AddressDetails AD ON AD.numAddressID = pro.numAddressID                                 
where numProId=@numProId     and pro.numdomainID=  @numDomainID                             
                                    
end
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ProjectsMaster_GetAssociatedContacts')
DROP PROCEDURE USP_ProjectsMaster_GetAssociatedContacts
GO
CREATE PROCEDURE [dbo].[USP_ProjectsMaster_GetAssociatedContacts]
(
	@numDomainID NUMERIC(18,0)
	,@numProId NUMERIC(18,0)
)
AS
BEGIN
	DECLARE @TEMPContacts TABLE
	(
		numContactID NUMERIC(18,0)
		,vcContact VARCHAR(200)
		,vcCompanyname VARCHAR(300)
		,tinUserType TINYINT
		,ActivityID NUMERIC(18,0)
		,Email VARCHAR(150)
		,vcStatus VARCHAR(200)
		,vcPosition VARCHAR(200)
		,vcEmail VARCHAR(200)
		,numPhone VARCHAR(200)
		,numPhoneExtension VARCHAR(200)
		,bitDefault BIT
	)

	INSERT INTO 
		@TEMPContacts
	SELECT 
		AC.numContactId
		,CONCAT(ISNULL(vcFirstName,''),' ',ISNULL(vcLastName,'')) AS vcContact
		,CI.vcCompanyName AS vcCompanyname
		,CASE WHEN UM.numUserDetailId IS NULL THEN 2 ELSE 1 END AS tinUserType
		,0 AS ActivityID
		,AC.vcEmail as Email
		,'' vcStatus
		,dbo.GetListIemName(AC.vcPosition) vcPosition
		,AC.vcEmail
		,AC.numPhone
		,AC.numPhoneExtension
		,1
	FROM
		ProjectsMaster PM
	INNER JOIN 
		AdditionalContactsInformation AC 
	ON 
		PM.numIntPrjMgr=AC.numContactID  
	LEFT JOIN
		DivisionMaster DM 
	ON 
		DM.numDivisionID=AC.numDivisionID 
	LEFT JOIN 
		CompanyInfo CI 
	ON 
		DM.numCompanyId=CI.numCompanyId 
	LEFT JOIN 
		UserMaster UM  
	ON 
		UM.numUserDetailID=AC.numContactID
	WHERE 
		PM.numDomainID=@numDomainId
		AND PM.numProId=@numProId

	INSERT INTO 
		@TEMPContacts
	SELECT 
		AC.numContactId
		,CONCAT(ISNULL(vcFirstName,''),' ',ISNULL(vcLastName,'')) AS vcContact
		,CI.vcCompanyName AS vcCompanyname
		,CASE WHEN UM.numUserDetailId IS NULL THEN 2 ELSE 1 END AS tinUserType
		,0 AS ActivityID
		,AC.vcEmail as Email
		,'' vcStatus
		,dbo.GetListIemName(AC.vcPosition) vcPosition
		,AC.vcEmail
		,AC.numPhone
		,AC.numPhoneExtension
		,1
	FROM
		ProjectsMaster PM
	INNER JOIN 
		AdditionalContactsInformation AC 
	ON 
		PM.numCustPrjMgr=AC.numContactID  
	LEFT JOIN
		DivisionMaster DM 
	ON 
		DM.numDivisionID=AC.numDivisionID 
	LEFT JOIN 
		CompanyInfo CI 
	ON 
		DM.numCompanyId=CI.numCompanyId 
	LEFT JOIN 
		UserMaster UM  
	ON 
		UM.numUserDetailID=AC.numContactID
	WHERE 
		PM.numDomainID=@numDomainId
		AND PM.numProId=@numProId
		AND AC.numContactId NOT IN (SELECT TC.numContactID FROM @TEMPContacts TC)

	INSERT INTO 
		@TEMPContacts
	SELECT
		AC.numContactId
		,CONCAT(ISNULL(vcFirstName,''),' ',ISNULL(vcLastName,'')) AS vcContact
		,CI.vcCompanyName AS vcCompanyname
		,CASE WHEN UM.numUserDetailId IS NULL THEN 2 ELSE 1 END AS tinUserType
		,0 AS ActivityID
		,AC.vcEmail as Email
		,'' vcStatus
		,dbo.GetListIemName(AC.vcPosition) vcPosition
		,AC.vcEmail
		,AC.numPhone
		,AC.numPhoneExtension
		,1
	FROM
		ProjectsMaster PM
	INNER JOIN
		StagePercentageDetailsTask SPDT
	ON
		PM.numProId = SPDT.numProjectId
	INNER JOIN 
		AdditionalContactsInformation AC 
	ON 
		SPDT.numAssignTo=AC.numContactID  
	LEFT JOIN
		DivisionMaster DM 
	ON 
		DM.numDivisionID=AC.numDivisionID 
	LEFT JOIN 
		CompanyInfo CI 
	ON 
		DM.numCompanyId=CI.numCompanyId 
	LEFT JOIN 
		UserMaster UM  
	ON 
		UM.numUserDetailID=AC.numContactID
	WHERE 
		PM.numDomainID=@numDomainId
		AND PM.numProId=@numProId
		AND AC.numContactId NOT IN (SELECT TC.numContactID FROM @TEMPContacts TC)

	INSERT INTO 
		@TEMPContacts
	SELECT
		PC.numContactID
		,CONCAT(ISNULL(vcFirstName,''),' ',ISNULL(vcLastName,'')) AS vcContact
		,CI.vcCompanyName AS vcCompanyname
		,CASE WHEN UM.numUserDetailId IS NULL THEN 2 ELSE 1 END AS tinUserType
		,0 AS ActivityID
		,AC.vcEmail as Email
		,'' vcStatus
		,dbo.GetListIemName(AC.vcPosition) vcPosition
		,AC.vcEmail
		,AC.numPhone
		,AC.numPhoneExtension
		,0
	FROM 
		ProjectsMaster PM
	INNER JOIN
		ProjectsContacts PC 
	ON
		PM.numProId = PC.numProId
	INNER JOIN 
		AdditionalContactsInformation AC 
	ON 
		PC.numContactID=AC.numContactID  
	LEFT JOIN
		DivisionMaster DM 
	ON 
		DM.numDivisionID=AC.numDivisionID 
	LEFT JOIN 
		CompanyInfo CI 
	ON 
		DM.numCompanyId=CI.numCompanyId 
	LEFT JOIN 
		UserMaster UM  
	ON 
		UM.numUserDetailID=AC.numContactID
	WHERE 
		PM.numDomainID=@numDomainId
		AND PM.numProId=@numProId
		AND AC.numContactId NOT IN (SELECT TC.numContactID FROM @TEMPContacts TC)

	SELECT * FROM @TEMPContacts ORDER BY vcContact
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_SelectCommunicationAttendees')
DROP PROCEDURE USP_SelectCommunicationAttendees
GO
CREATE PROCEDURE [dbo].[USP_SelectCommunicationAttendees]
@numCommId AS numeric(9)=0
AS
BEGIN
 SELECT ISNULL(vcFirstName,'')+' '+ISNULL(vcLastName,'') as vcContact,CA.numContactID,CI.vcCompanyName AS vcCompanyname,
  CASE WHEN UM.numUserDetailId IS NULL THEN 2 ELSE 1 END AS tinUserType,CA.ActivityID,AC.vcEmail as Email,
  dbo.GetListIemName(CA.numStatus) vcStatus,
  dbo.GetListIemName(AC.vcPosition) vcPosition,
  AC.vcEmail,
  AC.numPhone,
  AC.numPhoneExtension,
  0 AS bitDefault
 FROM CommunicationAttendees CA LEFT JOIN AdditionalContactsInformation AC ON CA.numContactID=AC.numContactID  
 LEFT JOIN divisionmaster DM on DM.numDivisionID=AC.numDivisionID 
 LEFT JOIN CompanyInfo CI on DM.numCompanyId=CI.numCompanyId 
 LEFT JOIN UserMaster UM  ON UM.numUserDetailId=CA.numContactID
 WHERE CA.numCommId=@numCommId
END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_StagePercentageDetailsTask_ChangeTime')
DROP PROCEDURE dbo.USP_StagePercentageDetailsTask_ChangeTime
GO
CREATE PROCEDURE [dbo].[USP_StagePercentageDetailsTask_ChangeTime]
(
	@numDomainID NUMERIC(18,0)
	,@numTaskID NUMERIC(18,0)
	,@numHours NUMERIC(18,0)
	,@numMinutes NUMERIC(18,0)
	,@bitMasterUpdateAlso BIT
)
AS 
BEGIN
	IF EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numDomainID=@numDomainID AND numTaskID = @numTaskID AND tintAction=4)
	BEGIN
		RAISERROR('TASK_IS_MARKED_AS_FINISHED',16,1)
		RETURN
	END
	ELSE IF ISNULL((SELECT TOP 1 tintAction FROM StagePercentageDetailsTaskTimeLog WHERE numDomainID=@numDomainID AND numTaskID = @numTaskID ORDER BY tintAction),0) IN (1,3)
	BEGIN
		RAISERROR('TASK_IS_ALREADY_STARTED',16,1)
		RETURN
	END
	ELSE
	BEGIN
		DECLARE @numToTalTimeSpendOnTask NUMERIC(18,0)
		DECLARE @vcTimeSpend VARCHAR(20) = ''
		SET @vcTimeSpend = dbo.GetTimeSpendOnTask(@numDomainID,@numTaskID,0)

		IF @vcTimeSpend = 'Invalid time sequence'
		BEGIN
			RAISERROR('INVALID_TIME_ENTRY_SEQUENCE',16,1)
			RETURN
		END

		SELECT @numToTalTimeSpendOnTask = (CAST(SUBSTRING(@vcTimeSpend,0,CHARINDEX(':',@vcTimeSpend)) AS INT) * 60) + CAST(SUBSTRING(@vcTimeSpend,CHARINDEX(':',@vcTimeSpend) + 1,LEN(@vcTimeSpend)) AS INT)

		If @numToTalTimeSpendOnTask > ((ISNULL(@numHours,0) * 60) + ISNULL(@numMinutes,0))
		BEGIN
			RAISERROR('MORE_TIME_SPEND_ON_TASK',16,1)
			RETURN
		END

		UPDATE StagePercentageDetailsTask SET numHours=@numHours,numMinutes=@numMinutes WHERE numDomainID=@numDomainID AND numTaskId=@numTaskID

		IF @bitMasterUpdateAlso = 1
		BEGIN
			DECLARE @numReferenceTaskId NUMERIC(18,0)
			SELECT @numReferenceTaskId=ISNULL(numReferenceTaskId,0) FROM StagePercentageDetailsTask WHERE numDomainID=@numDomainID AND numTaskId=@numTaskID

			UPDATE StagePercentageDetailsTask SET numHours=@numHours,numMinutes=@numMinutes WHERE numDomainID=@numDomainID AND numTaskId=@numReferenceTaskId 
		END
	END	
END
GO
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_StagePercentageDetailsTaskNotes_GetByTaskID')
DROP PROCEDURE dbo.USP_StagePercentageDetailsTaskNotes_GetByTaskID
GO
CREATE PROCEDURE [dbo].[USP_StagePercentageDetailsTaskNotes_GetByTaskID]
(
	@numTaskID NUMERIC(18,0)
)
AS 
BEGIN
	SELECT * FROM StagePercentageDetailsTaskNotes WHERE numTaskID = @numTaskID
END
GO
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_StagePercentageDetailsTaskNotes_Save')
DROP PROCEDURE dbo.USP_StagePercentageDetailsTaskNotes_Save
GO
CREATE PROCEDURE [dbo].[USP_StagePercentageDetailsTaskNotes_Save]
(
	@numTaskID NUMERIC(18,0)
	,@vcNotes NVARCHAR(MAX)
)
AS 
BEGIN
	IF EXISTS (SELECT ID FROM StagePercentageDetailsTaskNotes WHERE numTaskID = @numTaskID)
	BEGIN
		UPDATE StagePercentageDetailsTaskNotes SET vcNotes=@vcNotes WHERE numTaskID=@numTaskID
	END
	ELSE IF EXISTS (SELECT numTaskId FROM StagePercentageDetailsTask WHERE numTaskId = @numTaskID)
	BEGIN
		INSERT INTO StagePercentageDetailsTaskNotes
		(
			numTaskID
			,vcNotes
		)
		VALUES
		(
			@numTaskID
			,@vcNotes
		)
	END	
END
GO
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_StagePercentageDetailsTaskTimeLog_GetByTaskID')
DROP PROCEDURE dbo.USP_StagePercentageDetailsTaskTimeLog_GetByTaskID
GO
CREATE PROCEDURE [dbo].[USP_StagePercentageDetailsTaskTimeLog_GetByTaskID]
(
	@numDomainID NUMERIC(18,0)
	,@numTaskID NUMERIC(18,0)
	,@ClientTimeZoneOffset INT
	,@bitForProject BIT=0
	,@vcTotalTimeSpendOnTask VARCHAR(50) OUTPUT
)
AS 
BEGIN
	IF(@bitForProject=1)
	BEGIN
		SET @vcTotalTimeSpendOnTask = dbo.GetTimeSpendOnTaskByProject(@numDomainID,@numTaskID,0)
	END
	ELSE
	BEGIN
		SET @vcTotalTimeSpendOnTask = dbo.GetTimeSpendOnTask(@numDomainID,@numTaskID,0)
	END

	SELECT
		SPDTTL.ID
		,SPDTTL.numTaskID
		,SPDTTL.tintAction
		,ISNULL(SPDTTL.numProcessedQty,0) numProcessedQty
		,ISNULL(SPDTTL.numReasonForPause,0) numReasonForPause
		,dbo.fn_GetContactName(SPDTTL.numUserCntID) vcEmployee
		,CASE SPDTTL.tintAction
			WHEN 1 THEN 'Started'
			WHEN 2 THEN 'Paused'
			WHEN 3 THEN 'Resumed'
			WHEN 4 THEN 'Finished'
			ELSE ''
		END vcAction
		,CONCAT(dbo.FormatedDateFromDate(DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,SPDTTL.dtActionTime),@numDomainID),' ',CONVERT(VARCHAR(5),DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,SPDTTL.dtActionTime), 108),' ',RIGHT(CONVERT(VARCHAR(30),DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,SPDTTL.dtActionTime),9),2)) vcActionTime
		,DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,SPDTTL.dtActionTime) dtActionTime
		,CASE WHEN tintAction = 2 THEN CAST(numProcessedQty AS VARCHAR) ELSE '' END vcProcessedQty
		,ISNULL(LD.vcData,'') vcReasonForPause
		,ISNULL(SPDTTL.vcNotes,'') vcNotes
		,(CASE WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog S WHERE S.numTaskID = @numTaskID AND tintAction=4) THEN 1 ELSE 0 END) bitTaskFinished
	FROM
		StagePercentageDetailsTaskTimeLog SPDTTL
	LEFT JOIN
		ListDetails LD
	ON
		LD.numListID = 52
		AND SPDTTL.numReasonForPause = LD.numListItemID
	WHERE
		SPDTTL.numDomainID=@numDomainID
		AND SPDTTL.numTaskID=@numTaskID
	ORDER BY
		dtActionTime
END
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_UpdateSingleFieldValue' ) 
    DROP PROCEDURE USP_UpdateSingleFieldValue
GO

CREATE PROCEDURE USP_UpdateSingleFieldValue
	@tintMode AS TINYINT,
	@numUpdateRecordID NUMERIC,
	@numUpdateValueID NUMERIC=0,
	@vcText AS TEXT,
	@numDomainID AS NUMERIC= 0 
AS 
BEGIN
	 -- For 26 and 37
     DECLARE @Status AS NUMERIC(9,0)
     DECLARE @SiteID       AS NUMERIC(9,0)
     DECLARE @OrderStatus  AS NUMERIC(9,0)
	 DECLARE @BizdocStatus AS NUMERIC(9,0)  
	 DECLARE @numDomain AS NUMERIC(18,0)
	 SET @numDomain = @numDomainID 

	IF @tintMode=1
	BEGIN
		UPDATE  dbo.OpportunityBizDocsDetails
		SET     bitAmountCaptured = @numUpdateValueID
		WHERE   numBizDocsPaymentDetId = @numUpdateRecordID
	END
--	IF @tintMode = 2 
--		BEGIN
--			UPDATE  dbo.OpportunityMaster
--			SET     numStatus = @numUpdateValueID
--			WHERE   numOppId = @numUpdateRecordID
--		END
	IF @tintMode = 3
		BEGIN
			 UPDATE AdditionalContactsInformation SET txtNotes=ISNULL(CAST(txtNotes AS VARCHAR(8000)),'') + ' ' +  ISNULL(CAST(@vcText AS VARCHAR(8000)),'') WHERE numContactId=@numUpdateRecordID
		END
	IF @tintMode = 4
		BEGIN
			 UPDATE dbo.OpportunityBizDocsDetails SET numBillPaymentJournalID=@numUpdateValueID WHERE numBizDocsPaymentDetId=@numUpdateRecordID
		END
		
	IF @tintMode = 5
		BEGIN
			 UPDATE dbo.BizDocComission SET bitCommisionPaid=@numUpdateValueID WHERE numOppBizDocID=@numUpdateRecordID
		END
		
	IF @tintMode = 6
		BEGIN
			 UPDATE dbo.OpportunityRecurring SET numOppBizDocID=@numUpdateValueID WHERE numOppRecID=@numUpdateRecordID
		END
		
	IF @tintMode = 7
		BEGIN
			 UPDATE dbo.OpportunityRecurring SET dtRecurringDate=CAST( CAST( @vcText AS VARCHAR) AS DATETIME) WHERE numOppRecID=@numUpdateRecordID
		END
		
	IF @tintMode = 8
		BEGIN
			 UPDATE dbo.OpportunityRecurring SET fltBreakupPercentage=CAST( CAST( @vcText AS VARCHAR) AS float) WHERE numOppRecID=@numUpdateRecordID
		END
		/*Commented by chintan.. this is moved to USP_ShoppingCart SP*/
--	IF @tintMode = 9
--		BEGIN
--			UPDATE  dbo.OpportunityMaster
--			SET     txtComments = CAST(@vcText AS VARCHAR(1000))
--			WHERE   numOppId = @numUpdateRecordID
--		END    
--	IF @tintMode = 10
--		BEGIN
--			UPDATE  dbo.OpportunityMaster
--			SET     vcRefOrderNo = CAST(@vcText AS VARCHAR(100))
--			WHERE   numOppId = @numUpdateRecordID
--		END    
	IF @tintMode = 11
		BEGIN
			DELETE FROM dbo.LuceneItemsIndex WHERE numItemCode=@numUpdateRecordID
		END    
	IF @tintMode = 12
		BEGIN
			UPDATE dbo.Domain SET vcPortalName = CAST(@vcText AS VARCHAR(50))
			WHERE numDomainId=@numUpdateRecordID
		END 
	IF @tintMode = 13
		BEGIN
			UPDATE dbo.WareHouseItems SET numOnHand =numOnHand + CAST(CAST(@vcText AS VARCHAR(50)) AS numeric(18,0)),dtModified = GETDATE() 
			WHERE numWareHouseItemID = @numUpdateRecordID
			
			DECLARE @numItemCode NUMERIC(18)

			SELECT @numItemCode=numItemID from WareHouseItems where numWareHouseItemID = @numUpdateRecordID

			EXEC dbo.USP_ManageWareHouseItems_Tracking
				@numWareHouseItemID = @numUpdateRecordID, --  numeric(9, 0)
				@numReferenceID = @numItemCode, --  numeric(9, 0)
				@tintRefType = 1, --  tinyint
				@vcDescription = 'Inventory Adjustment', --  varchar(100)
				@numModifiedBy = @numUpdateValueID,
				@numDomainID = @numDomain
			 
		END
	IF @tintMode = 14
		BEGIN
			UPDATE dbo.Item SET monAverageCost=CAST(CAST(@vcText AS VARCHAR(50)) AS numeric(20,5))
			WHERE numItemCode = @numUpdateRecordID
		END
		IF @tintMode = 15
		BEGIN
			UPDATE dbo.EmailHistory  SET numListItemId = @numUpdateValueID  WHERE numEmailHstrID=@numUpdateRecordID
		END
		IF @tintMode = 16
		BEGIN
			UPDATE dbo.EmailHistory SET bitIsRead=@numUpdateValueID WHERE numEmailHstrID IN ( SELECT Id FROM dbo.SplitIDs(CAST(@vcText AS VARCHAR(200)) ,','))
		END
		IF @tintMode = 17
		BEGIN
			UPDATE CartItems SET numUserCntId = @numUpdateValueID WHERE vcCookieId =CAST(@vcText AS VARCHAR(MAX)) AND numUserCntId = 0
		END
		IF @tintMode = 18
		BEGIN
			UPDATE OpportunityBizDocs SET tintDeferred = 0 WHERE numOppBizDocsId=@numUpdateRecordID
		END
		IF @tintMode = 19
		BEGIN
			UPDATE dbo.OpportunityMaster SET vcPOppName=CAST(@vcText AS VARCHAR(100)) WHERE numOppId=@numUpdateRecordID
		END
	    IF @tintMode = 20
		BEGIN
			UPDATE UserMaster SET tintTabEvent=@numUpdateValueID WHERE numUserId=@numUpdateRecordID
		END
		IF @tintMode = 21
		BEGIN
			UPDATE OpportunityBizDocs SET numBizDocTempID = @numUpdateValueID WHERE numOppBizDocsId=@numUpdateRecordID
		END
		IF @tintMode = 22
		BEGIN
			UPDATE OpportunityBizDocItems SET tintTrackingStatus = 1 WHERE numOppBizDocItemID =@numUpdateRecordID
		END
		IF	 @tintMode = 23
		BEGIN
			UPDATE item SET bintModifiedDate = GETUTCDATE() WHERE numItemCode=@numUpdateRecordID			
		END
		IF @tintMode = 24
		BEGIN
			UPDATE dbo.OpportunityMaster SET numStatus=@numUpdateValueID WHERE numOppId in (select Items from dbo.split(@vcText,','))
		END
		
		IF @tintMode = 25
		BEGIN
			UPDATE dbo.WebAPIDetail SET vcEighthFldValue = @vcText WHERE WebApiId = @numUpdateRecordID AND numDomainId = @numDomainID
		END
		IF @tintMode = 26
		BEGIN
			
		     SELECT  @Status = ISNULL(numStatus ,0)  FROM dbo.OpportunityMaster WHERE numOppId = @numUpdateRecordID
		     
		     IF @Status = 0
		      BEGIN
				SELECT @SiteID  = CONVERT(NUMERIC(9,0),tintSource)   FROM	dbo.OpportunityMaster WHERE numOppId = @numUpdateRecordID
			    SELECT @OrderStatus = ISNULL(numOrderStatus,0) ,@BizdocStatus = ISNULL(numBizDocStatus,0)  FROM dbo.eCommercePaymentConfig WHERE numSiteId = @SiteID AND numPaymentMethodId = 31488
		       
			    UPDATE dbo.OpportunityMaster SET numStatus = ISNULL(@OrderStatus,0) WHERE numOppId = @numUpdateRecordID AND numDomainId = @numDomainID 
			    Update OpportunityBizDocs set monAmountPaid = ISNULL(monDealAmount,0),numBizDocStatus = @BizdocStatus  WHERE  numOppId = @numUpdateRecordID and bitAuthoritativeBizDocs =1
			  END   
		END
		IF @tintMode = 27
		BEGIN
			UPDATE OpportunityBizDocs SET numBizDocStatusOLD=numBizDocStatus,numBizDocStatus = @numUpdateValueID WHERE numOppBizDocsId=@numUpdateRecordID 
		END
	    IF @tintMode = 28
		BEGIN
			UPDATE General_Journal_Details SET bitReconcile=(CASE WHEN CAST(@vcText AS VARCHAR(5))='R' THEN 1 ELSE 0 END),
			bitCleared=(CASE WHEN CAST(@vcText AS VARCHAR(5))='C' THEN 1 ELSE 0 END),numReconcileID=0
			WHERE numDomainId=@numDomainID AND numTransactionId=@numUpdateRecordID 
		END
		
		IF @tintMode = 29
		BEGIN
			UPDATE dbo.ReturnHeader SET numRMATempID = @numUpdateValueID WHERE numReturnHeaderID = @numUpdateRecordID 
		END
		
		IF @tintMode = 30
		BEGIN
			UPDATE dbo.ReturnHeader SET numBizdocTempID = @numUpdateValueID WHERE numReturnHeaderID = @numUpdateRecordID 
		END
		
		IF @tintMode = 31
		BEGIN
			UPDATE dbo.OpportunityMaster SET numOppBizDocTempID = @numUpdateValueID WHERE numOppId = @numUpdateRecordID 
		END
		/*Commented by chintan.. this is moved to USP_ShoppingCart SP*/
--		IF @tintMode = 32
--		BEGIN
--			DECLARE @numBizDocId AS NUMERIC(9,0)
--			SELECT @numBizDocId = numBizDocId FROM eCommercePaymentConfig WHERE  numPaymentMethodId = @numUpdateValueId AND numDomainID = @numDomainID   
--			UPDATE dbo.OpportunityMaster SET numOppBizDocTempID = @numBizDocId WHERE numOppId = @numUpdateRecordID 
--		END
		
		IF @tintMode = 33
		BEGIN
			UPDATE dbo.ReturnHeader SET numBizdocTempID = @numUpdateValueID,numRMATempID = @numUpdateValueID WHERE numReturnHeaderID = @numUpdateRecordID 
		END
		
			
		IF @tintMode = 34
		BEGIN
			UPDATE dbo.WebAPIDetail SET vcFourteenthFldValue = @numUpdateValueID WHERE WebApiId = @numUpdateRecordID AND numDomainId = @numDomainID
		END
		
		IF @tintMode = 35
		BEGIN
			UPDATE OpportunityMasterTaxItems SET fltPercentage = CAST(CAST(@vcText AS VARCHAR(10)) AS FLOAT)
			WHERE numOppId = @numUpdateRecordID AND numTaxItemID = 0
		END
		
		IF @tintMode = 36
		BEGIN
			UPDATE dbo.Import_File_Master SET numProcessedCSVRowNumber = @numUpdateValueID
			WHERE intImportFileID = @numUpdateRecordID AND numDomainID = @numDomainID
		END
		IF @tintMode = 37
		BEGIN
		     SELECT  @Status = ISNULL(numStatus ,0)  FROM dbo.OpportunityMaster WHERE numOppId = @numUpdateRecordID
		     
		     IF @Status = 0
		      BEGIN
			    SELECT @SiteID  = CONVERT(NUMERIC(9,0),tintSource)   FROM	dbo.OpportunityMaster WHERE numOppId = @numUpdateRecordID
			    SELECT @OrderStatus = ISNULL(numFailedOrderStatus,0)    FROM dbo.eCommercePaymentConfig WHERE numSiteId = @SiteID AND numPaymentMethodId = 31488
		        
			    UPDATE dbo.OpportunityMaster SET numStatus = ISNULL(@OrderStatus,0) WHERE numOppId = @numUpdateRecordID AND numDomainId = @numDomainID 
			    
			  END   
		END
		IF @tintMode = 38
		BEGIN
			UPDATE [WebAPIDetail] SET    vcNinthFldValue = CAST(CAST(@vcText AS VARCHAR(30)) AS DATETIME)
			WHERE  [WebApiId] =  @numUpdateRecordID AND [numDomainId] = @numDomainId
		END
		
		IF @tintMode = 39
		BEGIN
			UPDATE [WebAPIDetail] SET    vcSixteenthFldValue = CAST(CAST(@vcText AS VARCHAR(30)) AS DATETIME)
			WHERE  [WebApiId] =  @numUpdateRecordID AND [numDomainId] = @numDomainId
		END

		IF @tintMode = 40
		BEGIN
			UPDATE dbo.WebAPIDetail SET bitEnableAPI = @numUpdateValueID WHERE WebApiId = @numUpdateRecordID AND numDomainId = @numDomainID
		END

		IF @tintMode = 41
		BEGIN
			UPDATE dbo.domain SET numShippingServiceItemID = @numUpdateValueID WHERE numDomainId = @numDomainID
		END
		
		IF @tintMode = 42
		BEGIN
			UPDATE dbo.domain SET numDiscountServiceItemID = @numUpdateValueID WHERE numDomainId = @numDomainID
		END
		
		IF @tintMode = 43
		BEGIN
			UPDATE dbo.Cases SET numStatus = @numUpdateValueID WHERE numCaseId = @numUpdateRecordID AND numDomainId = @numDomainID
		END
		
		IF @tintMode = 44
		BEGIN
			UPDATE dbo.UserMaster SET numDefaultClass = @numUpdateValueID WHERE numUserId = @numUpdateRecordID AND numDomainId = @numDomainID
		END

		IF @tintMode = 46
		BEGIN
			UPDATE dbo.Communication SET bitClosedFlag = @numUpdateValueID WHERE numCommId = @numUpdateRecordID AND numDomainId = @numDomainID
		END

		IF @tintMode = 47
		BEGIN
			UPDATE dbo.Domain SET bitCloneLocationWithItem = @numUpdateValueID WHERE numDomainId = @numDomainID
		END

		IF @tintMode = 48
		BEGIN
			UPDATE dbo.Domain SET vcMarketplaces = @vcText WHERE numDomainId = @numDomainID
		END

		IF @tintMode = 49
		BEGIN
			UPDATE dbo.OpportunityMaster SET intUsedShippingCompany=@numUpdateValueID,numShippingService=-1 WHERE numDomainId = @numDomainID AND numOppId=@numUpdateRecordID
		END

		IF @tintMode = 50
		BEGIN
			UPDATE dbo.StagePercentageDetailsTask SET vcTaskName=@vcText WHERE numDomainId = @numDomainID AND numTaskId=@numUpdateRecordID
		END
		
    SELECT @@ROWCOUNT
END
--Exec  USP_UpdateSingleFieldValue 26,52205,0,'',156
--UPDATE dbo.OpportunityMaster SET numStatus = 0 where numOppId = 52185
--SELECT numStatus FROM dbo.OpportunityMaster where numOppId = 52203
--SELECT * FROM dbo.OpportunityMaster 
--SELECT * FROM dbo.OpportunityBizDocs where numOppId = 52203
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_WorkOrder_GetTasks')
DROP PROCEDURE dbo.USP_WorkOrder_GetTasks
GO
CREATE PROCEDURE [dbo].[USP_WorkOrder_GetTasks]
(
	@numDomainID NUMERIC(18,0)
	,@numWOID NUMERIC(18,0)
	,@numStageDetailsId NUMERIC(18,0)
	,@ClientTimeZoneOffset INT
	,@tintMode TINYINT
)
AS 
BEGIN
	IF @tintMode = 1
	BEGIN
		DECLARE @numQtyToBuild FLOAT
		DECLARE @dtPlannedStartDate DATETIME
		SELECT 
			@numQtyToBuild=numQtyItemsReq
			,@dtPlannedStartDate=ISNULL(WorkOrder.dtmStartDate,WorkOrder.bintCreatedDate) 
		FROM 
			WorkOrder 
		WHERE
			WorkOrder.numDomainID=@numDomainID 
			AND WorkOrder.numWOId=@numWOID

		DECLARE @TempTaskAssignee TABLE
		(
			numAssignedTo NUMERIC(18,0)
			,dtLastTaskCompletionTime DATETIME
		)

		DECLARE @TempTasks TABLE
		(
			ID INT IDENTITY(1,1)
			,numTaskID NUMERIC(18,0)
			,numTaskTimeInMinutes NUMERIC(18,0)
			,numTaskAssignee NUMERIC(18,0)
			,intTaskType INT --1:Parallel, 2:Sequential
			,dtPlannedStartDate DATETIME
		)

		INSERT INTO @TempTasks
		(
			numTaskID
			,numTaskTimeInMinutes
			,intTaskType
			,numTaskAssignee
		)
		SELECT 
			SPDT.numTaskID
			,((ISNULL(SPDT.numHours,0) * 60) + ISNULL(SPDT.numMinutes,0)) * @numQtyToBuild
			,ISNULL(SPDT.intTaskType,1)
			,SPDT.numAssignTo
		FROM 
			StagePercentageDetailsTask SPDT
		INNER JOIN
			WorkOrder WO
		ON
			SPDT.numWorkOrderId = WO.numWOId
		WHERE 
			SPDT.numDomainId=@numDomainID
			AND SPDT.numWorkOrderId = @numWOID

		UPDATE @TempTasks SET dtPlannedStartDate=@dtPlannedStartDate

		INSERT INTO @TempTaskAssignee
		(
			numAssignedTo
		)
		SELECT DISTINCT
			numTaskAssignee
		FROM
			@TempTasks

		DECLARE @i INT = 1
		DECLARE @iCount INT 	
		SELECT @iCount = COUNT(*) FROM @TempTasks

		DECLARE @numTaskAssignee NUMERIC(18,0)
		DECLARE @intTaskType INT
		DECLARE @numWorkScheduleID NUMERIC(18,0)
		DECLARE @numTempUserCntID NUMERIC(18,0)
		DECLARE @dtStartDate DATETIME
		DECLARE @numTotalTaskInMinutes NUMERIC(18,0)
		DECLARE @numProductiveTimeInMinutes NUMERIC(18,0)
		DECLARE @tmStartOfDay TIME(7)
		DECLARE @numTimeLeftForDay NUMERIC(18,0)
		DECLARE @vcWorkDays VARCHAR(20)
		DECLARE @bitParallelStartSet BIT = 0

		WHILE @i <= @iCount
		BEGIN
			SELECT
				@numTaskAssignee=numTaskAssignee
				,@numTotalTaskInMinutes=numTaskTimeInMinutes
				,@intTaskType = intTaskType
			FROM
				@TempTasks 
			WHERE
				ID=@i

			-- GET TASK COMPLETION TIME WHICH WILL BE START DATETIME FOR NEXT TASK
			SELECT
				@numWorkScheduleID = WS.ID
				,@numProductiveTimeInMinutes=(ISNULL(WS.numProductiveHours,0) * 60) + ISNULL(WS.numProductiveMinutes,0)
				,@tmStartOfDay = tmStartOfDay
				,@vcWorkDays=CONCAT(',',vcWorkDays,',')
				,@dtStartDate = DATEADD(MINUTE,@ClientTimeZoneOffset,CONVERT(DATETIME,CONVERT(VARCHAR(8),DATEADD(MINUTE,-@ClientTimeZoneOffset,@dtPlannedStartDate),112)) + tmStartOfDay)
			FROM
				WorkSchedule WS
			INNER JOIN
				UserMaster
			ON
				WS.numUserCntID = UserMaster.numUserDetailId
			WHERE 
				WS.numUserCntID = @numTaskAssignee

			IF @intTaskType=1 AND EXISTS (SELECT numAssignedTo FROM @TempTaskAssignee WHERE numAssignedTo = @numTaskAssignee AND dtLastTaskCompletionTime IS NOT NULL)
			BEGIN
				SELECT @dtStartDate = dtLastTaskCompletionTime FROM @TempTaskAssignee WHERE numAssignedTo = @numTaskAssignee
			END
			ELSE IF EXISTS (SELECT numAssignedTo FROM @TempTaskAssignee WHERE numAssignedTo = @numTaskAssignee AND dtLastTaskCompletionTime IS NOT NULL)
			BEGIN
				SELECT @dtStartDate = dtLastTaskCompletionTime FROM @TempTaskAssignee WHERE numAssignedTo = @numTaskAssignee
			END

			UPDATE @TempTasks SET dtPlannedStartDate=@dtStartDate WHERE ID=@i 

			IF @numProductiveTimeInMinutes > 0 AND @numTotalTaskInMinutes > 0
			BEGIN
				WHILE @numTotalTaskInMinutes > 0
				BEGIN
					-- IF ITS WORKING DAY AND ALSO NOT HOLIDAY THEN PROCEED OR INCRESE DATE TO NEXT DAY
					IF CHARINDEX(CONCAT(',',DATEPART(WEEKDAY,@dtStartDate),','),@vcWorkDays) > 0 AND NOT EXISTS (SELECT ID FROM WorkScheduleDaysOff WHERE numWorkScheduleID=@numWorkScheduleID AND CAST(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,@dtStartDate) AS DATE) BETWEEN dtDayOffFrom AND dtDayOffTo)
					BEGIN
						IF CAST(@dtStartDate AS DATE) = CAST(GETUTCDATE() AS DATE)
						BEGIN
							-- CHECK TIME LEFT FOR DAY BASED
							SET @numTimeLeftForDay = DATEDIFF(MINUTE,@dtStartDate,DATEADD(MINUTE,@numProductiveTimeInMinutes,DATEADD(MINUTE,@ClientTimeZoneOffset,CONVERT(DATETIME,CONVERT(VARCHAR(8),DATEADD(MINUTE,-@ClientTimeZoneOffset,@dtStartDate),112)) + @tmStartOfDay)))
						END
						ELSE
						BEGIN
							SET @numTimeLeftForDay = @numProductiveTimeInMinutes
						END

						IF @numTimeLeftForDay > 0
						BEGIN
							IF @numTimeLeftForDay > @numTotalTaskInMinutes
							BEGIN
								SET @dtStartDate = DATEADD(MINUTE,@numTotalTaskInMinutes,@dtStartDate)
							END
							ELSE
							BEGIN
								SET @dtStartDate = DATEADD(MINUTE,@ClientTimeZoneOffset,CONVERT(DATETIME,CONVERT(VARCHAR(8),DATEADD(MINUTE,-@ClientTimeZoneOffset,DATEADD(DAY,1,@dtStartDate)),112)) + @tmStartOfDay)
							END

							SET @numTotalTaskInMinutes = @numTotalTaskInMinutes - @numTimeLeftForDay
						END
						ELSE
						BEGIN
							SET @dtStartDate = DATEADD(MINUTE,@ClientTimeZoneOffset,CONVERT(DATETIME,CONVERT(VARCHAR(8),DATEADD(MINUTE,-@ClientTimeZoneOffset,DATEADD(DAY,1,@dtStartDate)),112)) + @tmStartOfDay)
						END
					END
					ELSE
					BEGIN
						SET @dtStartDate = DATEADD(MINUTE,@ClientTimeZoneOffset,CONVERT(DATETIME,CONVERT(VARCHAR(8),DATEADD(MINUTE,-@ClientTimeZoneOffset,DATEADD(DAY,1,@dtStartDate)),112)) + @tmStartOfDay)					
					END				
				END
			END	

			UPDATE @TempTaskAssignee SET dtLastTaskCompletionTime=@dtStartDate WHERE numAssignedTo=@numTaskAssignee

			SET @i = @i + 1
		END

		SELECT	
			SPDT.numTaskId
			,SPDT.numAssignTo
			,SPDT.numReferenceTaskId
			,SPDT.vcTaskName
			,dbo.fn_GetContactName(SPDT.numAssignTo) vcAssignedTo
			,'Work Station 1' AS vcWorkStation
			,CASE
				WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId AND tintAction=4) THEN ISNULL(WO.numQtyItemsReq,0)
				ELSE ISNULL((SELECT SUM(numProcessedQty) FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId),0)
			END AS numProcessedQty
			,(CASE
				WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId AND tintAction=4) THEN 0
				ELSE ISNULL(WO.numQtyItemsReq,0) - ISNULL((SELECT SUM(numProcessedQty) FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId),0)
			END) AS numRemainingQty
			,CASE ISNULL((SELECT TOP 1 tintAction FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId ORDER BY dtActionTime DESC),0) 
				WHEN 4 THEN '<img src="../images/comflag.png" />'
				WHEN 3 THEN CONCAT('<ul class="list-inline" style="margin-bottom:0px;"><li style="vertical-align:middle;padding-left:0px;padding-right:0px;"><button class="btn btn-xs btn-info" onclick="return ShowTaskTimeLogWindow(',SPDT.numTaskId,');"><i class="fa fa-clock-o"></i></button></li><li style="vertical-align:middle;padding-right:0px;"><button class="btn btn-xs btn-flat btn-success btn-task-start" onclick="return TaskStarted(this,',SPDT.numTaskId,',0);">Start</button></li><li style="vertical-align:middle;padding-right:0px;"><label class="taskTimerInitial"></label></li></ul>')
				WHEN 2 THEN CONCAT('<ul class="list-inline" style="margin-bottom:0px;"><li style="vertical-align:middle;padding-left:0px;padding-right:0px;"><button class="btn btn-xs btn-info" onclick="return ShowTaskTimeLogWindow(',SPDT.numTaskId,');"><i class="fa fa-clock-o"></i></button></li><li style="vertical-align:middle;padding-right:0px;"><button class="btn btn-xs btn-flat bg-purple" onclick="return TaskStarted(this,',SPDT.numTaskId,',1);">Resume</button></li></ul>')
				WHEN 1 THEN CONCAT('<ul class="list-inline" style="margin-bottom:0px;"><li style="vertical-align:middle;padding-left:0px;padding-right:0px;"><button class="btn btn-xs btn-info" onclick="return ShowTaskTimeLogWindow(',SPDT.numTaskId,');"><i class="fa fa-clock-o"></i></button></li><li style="vertical-align:middle;padding-right:0px;"><button class="btn btn-xs btn-flat btn-warning" onclick="return ShowTaskPausedWindow(this,',SPDT.numTaskId,');">Pause</button></li><li style="vertical-align:middle;padding-right:0px;"><input type="text" onkeydown="return ProcessedUnitsChanged(this,',SPDT.numTaskId,',event);" class="form-control txtUnitsProcessed" style="width:50px;padding:1px 2px 1px 2px;height:23px;" /></li><li style="vertical-align:middle;padding-right:0px;"><label class="taskTimerInitial"></label></li></ul>')
				ELSE CONCAT('<ul class="list-inline" style="margin-bottom:0px;"><li style="vertical-align:middle;padding-left:0px;padding-right:0px;"><button class="btn btn-xs btn-info" onclick="return ShowTaskTimeLogWindow(',SPDT.numTaskId,');"><i class="fa fa-clock-o"></i></button></li><li style="vertical-align:middle;padding-right:0px;"><button class="btn btn-xs btn-flat btn-success btn-task-start" onclick="return TaskStarted(this,',SPDT.numTaskId,',0);">Start</button></li></ul>')
			END  AS vcTaskControls
			,FORMAT(FLOOR((((ISNULL(SPDT.numHours,0) * 60) + ISNULL(SPDT.numMinutes,0)) * @numQtyToBuild) / 60),'00') + ':' + FORMAT((((ISNULL(SPDT.numHours,0) * 60) + ISNULL(SPDT.numMinutes,0)) * CAST(@numQtyToBuild AS DECIMAL)) % 60,'00')  vcEstimatedTaskTime
			,(((ISNULL(SPDT.numHours,0) * 60) + ISNULL(SPDT.numMinutes,0)) * @numQtyToBuild) numTaskEstimationInMinutes
			,(CASE ISNULL((SELECT TOP 1 tintAction FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=SPDT.numTaskId ORDER BY SPDTTL.dtActionTime DESC,SPDTTL.ID DESC),0) 
				WHEN 4 THEN 0
				WHEN 3 THEN dbo.GetTimeSpendOnTask(SPDT.numDomainID,SPDT.numTaskId,2)
				WHEN 2 THEN dbo.GetTimeSpendOnTask(SPDT.numDomainID,SPDT.numTaskId,2)
				WHEN 1 THEN 0
			END) numTimeSpentInMinutes
			,(CASE ISNULL((SELECT TOP 1 tintAction FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId ORDER BY dtActionTime DESC,ID DESC),0) 
				WHEN 4 THEN NULL
				WHEN 3 THEN DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,(SELECT TOP 1 dtActionTime FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=SPDT.numTaskId ORDER BY SPDTTL.dtActionTime DESC,SPDTTL.ID DESC))
				WHEN 2 THEN NULL
				WHEN 1 THEN DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,(SELECT TOP 1 dtActionTime FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=SPDT.numTaskId ORDER BY SPDTTL.dtActionTime DESC,SPDTTL.ID DESC))
			END) dtLastStartDate
			,CASE WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId AND tintAction=4)
				THEN dbo.GetTimeSpendOnTaskByProject(SPDT.numDomainID,SPDT.numTaskId,1)
				ELSE ''
			END vcActualTaskTimeHtml
			,(CASE WHEN dbo.GetTimeSpendOnTask(SPDT.numDomainID,SPDT.numTaskId,0) = 'Invalid time sequence' THEN '00:00' ELSE dbo.GetTimeSpendOnTask(SPDT.numDomainID,SPDT.numTaskId,0) END) vcActualTaskTime
			,CASE 
				WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId AND tintAction=1)
				THEN CONCAT('<span>',dbo.FormatedDateTimeFromDate(DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,(SELECT dtActionTime FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId AND tintAction=1)),@numDomainID),'</span>')
				ELSE CONCAT('<span style="color:#a6a6a6">',dbo.FormatedDateTimeFromDate((CASE
										WHEN TT.dtPlannedStartDate IS NULL
										THEN DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,ISNULL(WO.dtmStartDate,WO.bintCreatedDate))
										ELSE DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,TT.dtPlannedStartDate)
									END),@numDomainID),'<i> (planned)</i></span>')
			END dtPlannedStartDate
			,CASE WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId AND tintAction=1)
				THEN DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,(SELECT dtActionTime FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId AND tintAction=1))
				ELSE ''
			END AS dtStartDate
			,CASE WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId AND tintAction=4)
				THEN DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,(SELECT dtActionTime FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId AND tintAction=4))
				ELSE ''
			END AS dtFinishDate
			,CASE WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId AND tintAction=4)
				THEN dbo.FormatedDateTimeFromDate(DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,(SELECT dtActionTime FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId AND tintAction=4)),@numDomainID)
				ELSE ''
			END AS vcFinishDate
			,CASE WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId AND tintAction=4)
				THEN 1
				ELSE 0
			END AS bitTaskCompleted
		FROM 
			StagePercentageDetailsTask SPDT
		INNER JOIN
			WorkOrder WO
		ON
			SPDT.numWorkOrderId = WO.numWOId
		LEFT JOIN
			@TempTasks TT
		ON
			SPDT.numTaskId = TT.numTaskID
		WHERE
			SPDT.numStageDetailsId = @numStageDetailsId
			AND SPDT.numWorkOrderId = @numWOID
	END
	ELSE IF @tintMode = 2
	BEGIN
		SELECT	
			SPDT.numTaskId
			,SPDT.numReferenceTaskId
			,SPDT.numStageDetailsId
			,SPDT.vcTaskName
			,ISNULL(SPDT.numHours,0) numHours
			,ISNULL(SPDT.numMinutes,0) numMinutes
			,SPDT.numAssignTo
			,ADC.numTeam
			,CONCAT(FORMAT(ISNULL(SPDT.numHours,0),'00'),':',FORMAT(ISNULL(SPDT.numMinutes,0),'00')) vcEstimatedTaskTime
			,(CASE WHEN dbo.GetTimeSpendOnTask(SPDT.numDomainID,SPDT.numTaskId,0) = 'Invalid time sequence' THEN '00:00' ELSE dbo.GetTimeSpendOnTask(SPDT.numDomainID,SPDT.numTaskId,0) END) vcActualTaskTime
			,CASE WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId AND tintAction=1)
				THEN DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,(SELECT dtActionTime FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId AND tintAction=1))
				ELSE ''
			END AS dtStartDate
			,CASE WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId AND tintAction=4)
				THEN DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,(SELECT dtActionTime FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId AND tintAction=4))
				ELSE ''
			END AS dtFinishDate
			,CASE WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId)
				THEN 1
				ELSE 0
			END bitTaskStarted
		FROM 
			StagePercentageDetailsTask SPDT
		INNER JOIN
			WorkOrder WO
		ON
			SPDT.numWorkOrderId = WO.numWOId
		INNER JOIN
			AdditionalContactsInformation ADC
		ON
			SPDT.numAssignTo = ADC.numContactId
		LEFT JOIN
			@TempTasks TT
		ON
			SPDT.numTaskId = TT.numTaskID
		WHERE
			SPDT.numStageDetailsId = @numStageDetailsId
			AND SPDT.numWorkOrderId = @numWOID
	END
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetWorkOrder2')
DROP PROCEDURE USP_GetWorkOrder2
GO
CREATE PROCEDURE [dbo].[USP_GetWorkOrder2]
(
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@tintUserRightType TINYINT
	,@CurrentPage INT                                                
	,@PageSize INT
	,@ClientTimeZoneOffset INT
	,@numWOStatus NUMERIC(18,0)
	,@vcRegularSearchCriteria VARCHAR(MAX)
	,@vcCustomSearchCriteria VARCHAR(MAX)
	,@SearchText VARCHAR(300) = ''
)
AS                            
BEGIN
	DECLARE @vcCurrencySymbol VARCHAR(20) = ''
	DECLARE @tintDecimalPoints TINYINT

	SELECT 
		@vcCurrencySymbol = ISNULL(Currency.varCurrSymbol ,'')
		,@tintDecimalPoints = ISNULL(Domain.tintDecimalPoints,0)
	FROM 
		Domain 
	LEFT JOIN 
		Currency 
	ON 
		Currency.numDomainID=@numDomainID
		AND Domain.numCurrencyID=Currency.numCurrencyID 
	WHERE 
		Domain.numDomainID=@numDomainID 

	CREATE TABLE #TEMPWorkOrderStatus
	(
		ID INT IDENTITY(1,1)
		,numWOID NUMERIC(18,0)
		,numWorkOrderStatus TINYINT
	)


	DECLARE @vcDynamicQuery AS NVARCHAR(MAX)=''
	DECLARE @vcStatusQuery VARCHAR(500) = ''
	IF(@vcRegularSearchCriteria<>'')
	BEGIN
		IF CHARINDEX('WO.TotalProgress',@vcRegularSearchCriteria) > 0
		BEGIN
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'WO.TotalProgress','dbo.GetTotalProgress(WO.numDomainID,WO.numWOID,1,1,'''',0)')
		END

		IF CHARINDEX('WO.dtmStartDate',@vcRegularSearchCriteria) > 0
		BEGIN
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'WO.dtmStartDate',CONCAT('dbo.FormatedDateFromDate(DateAdd(MINUTE,-1 * ',@ClientTimeZoneOffset,', WO.dtmStartDate)',',',@numDomainId,')'))
		END

		IF CHARINDEX('WO.dtmActualStartDate',@vcRegularSearchCriteria) > 0
		BEGIN
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'WO.dtmActualStartDate',CONCAT('dbo.FormatedDateFromDate(DateAdd(MINUTE,-1 * ',@ClientTimeZoneOffset,',(SELECT TOP 1 SPDTTL.dtActionTime FROM StagePercentageDetailsTask SPDT INNER JOIN StagePercentageDetailsTaskTimeLog SPDTTL ON SPDT.numTaskId = SPDTTL.numTaskID WHERE SPDT.numWorkOrderId=WO.numWOID ORDER BY SPDTTL.dtActionTime))',',',@numDomainId,')'))
		END

		IF CHARINDEX('WO.dtmEndDate',@vcRegularSearchCriteria) > 0
		BEGIN
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'WO.dtmEndDate',CONCAT('dbo.FormatedDateFromDate(DateAdd(MINUTE,-1 * ',@ClientTimeZoneOffset,', WO.dtmEndDate)',',',@numDomainId,')'))
		END

		IF CHARINDEX('WO.dtmProjectFinishDate',@vcRegularSearchCriteria) > 0
		BEGIN
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'WO.dtmProjectFinishDate',(CASE WHEN @numWOStatus=1 THEN CONCAT('dbo.FormatedDateFromDate(DateAdd(MINUTE,-1 * ',@ClientTimeZoneOffset,', WO.bintCompletedDate)',',',@numDomainId,')') ELSE 'CAST(TableProjectedFinish.dtProjectedFinish AS DATE)' END))
		END

		IF CHARINDEX('WO.ForecastDetails=1',@vcRegularSearchCriteria) > 0
		BEGIN
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'WO.ForecastDetails=1','DATEDIFF(DAY,WO.dtmEndDate,TableProjectedFinish.dtProjectedFinish) > 0')
		END

		IF CHARINDEX('WO.ForecastDetails=2',@vcRegularSearchCriteria) > 0
		BEGIN
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'WO.ForecastDetails=2','DATEDIFF(DAY,WO.dtmEndDate,TableProjectedFinish.dtProjectedFinish) < 0')
		END

		IF CHARINDEX('WO.SchedulePerformance=1',@vcRegularSearchCriteria) > 0
		BEGIN
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'WO.ForecastDetails=1','DATEDIFF(DAY,WO.dtmEndDate,WO.bintCompletedDate) > 0')
		END

		IF CHARINDEX('WO.SchedulePerformance=2',@vcRegularSearchCriteria) > 0
		BEGIN
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'WO.ForecastDetails=2','DATEDIFF(DAY,WO.dtmEndDate,WO.bintCompletedDate) < 0')
		END

		IF CHARINDEX('WO.ActualProjectDifference=1',@vcRegularSearchCriteria) > 0
		BEGIN
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'WO.ActualProjectDifference=1','ISNULL(TableHourCost.numEstimatedTimeInMinutes,0) > 0 AND ISNULL(TableHourCost.numActualTimeInMinutes,0) > ISNULL(TableHourCost.numEstimatedTimeInMinutes,0)')
		END

		IF CHARINDEX('WO.ActualProjectDifference=2',@vcRegularSearchCriteria) > 0
		BEGIN
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'WO.ActualProjectDifference=2','ISNULL(TableHourCost.numEstimatedTimeInMinutes,0) > 0 AND ISNULL(TableHourCost.numActualTimeInMinutes,0) < ISNULL(TableHourCost.numEstimatedTimeInMinutes,0)')
		END

		IF CHARINDEX('WO.CapacityLoad',@vcRegularSearchCriteria) > 0 
		BEGIN
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'WO.CapacityLoad','TableCapacity.numCapacityLoad')
		END

		IF CHARINDEX('WO.vcWorkOrderStatus',@vcRegularSearchCriteria) > 0
		BEGIN
			INSERT INTO #TEMPWorkOrderStatus
			(
				numWOID
				,numWorkOrderStatus
			)
			SELECT
				numWOID
				,numWorkOrderStatus
			FROM
				dbo.GetWOStatus(@numDomainID,'')
			WHERE
				numWorkOrderStatus = (CASE 
										WHEN CHARINDEX('WO.vcWorkOrderStatus like ''%1%''',@vcRegularSearchCriteria) > 0 THEN 1
										WHEN CHARINDEX('WO.vcWorkOrderStatus like ''%2%''',@vcRegularSearchCriteria) > 0 THEN 2
										WHEN CHARINDEX('WO.vcWorkOrderStatus like ''%3%''',@vcRegularSearchCriteria) > 0 THEN 3
										WHEN CHARINDEX('WO.vcWorkOrderStatus like ''%4%''',@vcRegularSearchCriteria) > 0 THEN 4
										ELSE 1
									END)

			SET @vcStatusQuery = 'INNER JOIN 
										#TEMPWorkOrderStatus TWOS 
									ON 
										WO.numWOID = TWOS.numWOID'
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'WO.vcWorkOrderStatus like ''%1%''','1=1')
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'WO.vcWorkOrderStatus like ''%2%''','1=1')
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'WO.vcWorkOrderStatus like ''%3%''','1=1')
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'WO.vcWorkOrderStatus like ''%4%''','1=1')
		END

		SET @vcRegularSearchCriteria = ' AND '+@vcRegularSearchCriteria
	END

	SET @vcDynamicQuery = ' SELECT 
								COUNT(*) OVER() AS TotalRecords,
								WO.numWOID,
								WO.vcWorkOrderName,
								WO.numQtyItemsReq,
								WHI.numWareHouseID,
								WO.numWarehouseItemID,
								WHI.numWareHouseID,
								(select dbo.fn_CalculateMinBuildQtyByItem(WO.numItemCode,WHI.numWareHouseID)) AS MaxBuildQty,
								I.vcItemName,
								(SELECT dbo.FormatedDateFromDate( DateAdd(minute,+'+convert(varchar(15),-@ClientTimeZoneOffset)+', WO.dtmStartDate)'+ ','+ CONVERT(VARCHAR(10),@numDomainId) +')) AS dtmStartDate,
								(SELECT TOP 1 SPDTTL.dtActionTime FROM StagePercentageDetailsTask SPDT INNER JOIN StagePercentageDetailsTaskTimeLog SPDTTL ON SPDT.numTaskId = SPDTTL.numTaskID WHERE SPDT.numWorkOrderId=WO.numWOID ORDER BY SPDTTL.dtActionTime) AS dtmActualStartDate,
								DateAdd(MINUTE,-1 * ' +convert(varchar(15),@ClientTimeZoneOffset)+', WO.dtmEndDate) dtmEndDate,
								WO.bintCompletedDate AS dtmProjectFinishDate,
								OI.ItemReleaseDate,
								'''' AS ForecastDetails,
								SP.Slp_Name,
								dbo.fn_GetContactName(isnull(WO.numAssignedTo,0)) AS vcAssignedTo,
								0 AS CapacityLoad,
								Opp.vcPOppName,
								WO.numWOStatus,
								WO.numWOID AS [numWOID~0~0],
								CONCAT(''<a target="_blank" href="../items/frmWorkOrder.aspx?WOID='',WO.numWOID,''">'',ISNULL(WO.vcWorkOrderName,''-''),'' ('',ISNULL(WO.numQtyItemsReq,0),'')'',''</a>'') AS [vcWorkOrderName~16~0],
								CONCAT(''<a target="_blank" href="../items/frmWorkOrder.aspx?WOID='',WO.numWOID,''">'',ISNULL(WO.vcWorkOrderName,''-''),'' ('',ISNULL(WO.numQtyItemsReq,0),'')'',''</a>'') AS [vcWorkOrderNameWithQty~1~0],
								CONCAT(I.vcItemName,(CASE WHEN ISNULL(I.vcSKU,'''') = '''' THEN '''' ELSE CONCAT('' ('',I.vcSKU,'')'') END)) AS ' + (CASE WHEN @numWOStatus=1 THEN '[AssemblyItemSKU~17~0]' ELSE '[AssemblyItemSKU~4~0]' END) + ',
								(CASE 
									WHEN ISNULL(TableHourCost.numEstimatedTimeInMinutes,0) > 0 
									THEN CONCAT((CASE 
													WHEN ISNULL(TableHourCost.numActualTimeInMinutes,0) > ISNULL(TableHourCost.numEstimatedTimeInMinutes,0) 
													THEN ''<lable style="color: #dd4b39;border: 2px solid #dd4b39;padding: 4px;font-weight: 600;">''
													ELSE ''<lable style="color: #00a65a;border: 2px solid #00a65a;padding: 4px;font-weight: 600;">''
												END),ISNULL(TableHourCost.vcActualTaskTime,''-''),'' & '',''' + @vcCurrencySymbol + ''',CAST(TableHourCost.monActualLabourCost AS DECIMAL(20,' + CAST(@tintDecimalPoints AS VARCHAR) + ')),(CASE WHEN ISNULL(TableHourCost.numActualTimeInMinutes,0) > ISNULL(TableHourCost.numEstimatedTimeInMinutes,0) THEN '' > Projected'' ELSE '' < Projected'' END),''</lable>'') 
									ELSE '''' 
								END) AS [ActualProjectDifference~23~0],
								WO.numQtyItemsReq AS [numQtyItemsReq~18~0],
								WHI.numWareHouseID AS [numWareHouseID~0~0],
								WO.numWarehouseItemID AS [numWarehouseItemID~0~0],
								WHI.numWareHouseID AS [numWareHouseID~0~0],
								(select dbo.fn_CalculateMinBuildQtyByItem(WO.numItemCode,WHI.numWareHouseID)) AS [MaxBuildQty~2~0],
								dbo.GetTotalProgress(WO.numDomainID,WO.numWOID,1,1,'''',0) AS [TotalProgress~3~0],
								I.vcItemName AS [vcItemName~0~0],
								(SELECT dbo.FormatedDateFromDate( DateAdd(MINUTE,-1 * ' +convert(varchar(15),@ClientTimeZoneOffset)+', WO.dtmStartDate)'+ ','+ CONVERT(VARCHAR(10),@numDomainId) +')) AS [dtmStartDate~5~0],
								(SELECT dbo.FormatedDateFromDate( DateAdd(MINUTE,-1 * ' +convert(varchar(15),@ClientTimeZoneOffset)+', (SELECT TOP 1 SPDTTL.dtActionTime FROM StagePercentageDetailsTask SPDT INNER JOIN StagePercentageDetailsTaskTimeLog SPDTTL ON SPDT.numTaskId = SPDTTL.numTaskID WHERE SPDT.numWorkOrderId=WO.numWOID ORDER BY SPDTTL.dtActionTime))'+ ','+ CONVERT(VARCHAR(10),@numDomainId) +')) AS [dtmActualStartDate~6~0],
								(SELECT dbo.FormatedDateFromDate( DateAdd(MINUTE,-1 * ' +convert(varchar(15),@ClientTimeZoneOffset)+', WO.dtmEndDate)'+ ','+ CONVERT(VARCHAR(10),@numDomainId) +')) AS ' + (CASE WHEN @numWOStatus=1 THEN '[dtmEndDate~19~0]' ELSE '[dtmEndDate~7~0]' END) + ',
								' + (CASE WHEN @numWOStatus=1 THEN CONCAT('dbo.FormatedDateFromDate(DateAdd(MINUTE,-1 * ',@ClientTimeZoneOffset,', WO.bintCompletedDate)',',',@numDomainId,') AS [dtmProjectFinishDate~20~0]') ELSE '''<label class="lblProjectedFinish"><i class="fa fa-refresh fa-spin" style="font-size:24px"></i></lable>'' AS [dtmProjectFinishDate~8~0]' END) + ',
								(SELECT dbo.FormatedDateFromDate( DateAdd(MINUTE,-1 * ' + convert(varchar(15),@ClientTimeZoneOffset)+', CAST(OI.ItemReleaseDate AS DATETIME))'+ ','+ CONVERT(VARCHAR(10),@numDomainId) +')) AS ' + (CASE WHEN @numWOStatus=1 THEN '[ItemReleaseDate~21~0]' ELSE '[ItemReleaseDate~9~0]' END) + ',
								''<label class="lblForecastDetail"><i class="fa fa-refresh fa-spin" style="font-size:24px"></i></label>'' AS [ForecastDetails~10~0],
								(CASE 
									WHEN DATEDIFF(DAY,WO.dtmEndDate,WO.bintCompletedDate) > 0 
									THEN CONCAT(''<lable style="color: #dd4b39;border: 2px solid #dd4b39;padding: 4px;font-weight: 600;">'',DATEDIFF(DAY,WO.dtmEndDate,WO.bintCompletedDate),(CASE WHEN DATEDIFF(DAY,WO.dtmEndDate,WO.bintCompletedDate) > 1 THEN '' Days'' ELSE '' Day'' END),'' Late</lable>'')
									WHEN DATEDIFF(DAY,WO.dtmEndDate,WO.bintCompletedDate) <= 0
									THEN CONCAT(''<lable style="color: #00a65a;border: 2px solid #00a65a;padding: 4px;font-weight: 600;">'',DATEDIFF(DAY,WO.dtmEndDate,WO.bintCompletedDate) * -1,(CASE WHEN DATEDIFF(DAY,WO.dtmEndDate,WO.bintCompletedDate) * -1 > 1 THEN '' Days'' ELSE '' Day'' END),'' Early</lable>'')
									ELSE ''''
								END) AS [SchedulePerformance~22~0],
								SP.Slp_Name AS ' + (CASE WHEN @numWOStatus=1 THEN '[Slp_Name~24~0]' ELSE '[Slp_Name~11~0]' END) + ',
								dbo.fn_GetContactName(isnull(WO.numAssignedTo,0)) AS ' + (CASE WHEN @numWOStatus=1 THEN '[vcAssignedTo~25~0]' ELSE '[vcAssignedTo~12~0]' END) + ',
								''<label class="lblCapacityLoad"><i class="fa fa-refresh fa-spin" style="font-size:24px"></i></label>''	AS [CapacityLoad~13~0],
								Opp.vcPOppName AS [vcPOppName~0~0],
								WO.numWOStatus AS [numWOStatus~0~0],
								''<label class="lblWorkOrderStatus"><i class="fa fa-refresh fa-spin" style="font-size:24px"></i></label>''AS [vcWorkOrderStatus~14~0],
								dbo.GetWorkOrderPickedRemainingQty(WO.numWOId,WO.numQtyItemsReq) [vcPickedRemaining~15~0]
							FROM 
								WorkOrder AS WO 
							' + @vcStatusQuery + '
							LEFT JOIN  
								Item I 
							ON 
								WO.numItemCode=I.numItemCode 
							LEFT JOIN
								WareHouseItems AS WHI
							ON
								WO.numWarehouseItemID = WHI.numWareHouseItemID
							LEFT JOIN
								Warehouses AS WH
							ON
								WHI.numWareHouseID = WH.numWareHouseID
							LEFT JOIN
								OpportunityMaster AS Opp
							ON
								WO.numOppId = Opp.numOppId 
							LEFT JOIN
								OpportunityItems AS OI
							ON
								OPP.numOppId=OI.numOppId
								AND OI.numOppItemtcode = WO.numOppItemID
							LEFT JOIN
								Sales_process_List_Master AS SP
							ON
								WO.numBuildProcessId = SP.Slp_id
							OUTER APPLY
							(
								SELECT
									dbo.GetProjectedFinish('+ CAST(@numDomainID AS VARCHAR) +',2,WO.numWOID,0,0,NULL,'+ CAST(ISNULL(@ClientTimeZoneOffset,0) AS VARCHAR) +',0) AS dtProjectedFinish
							) TableProjectedFinish
							OUTER APPLY
							(
								SELECT
									dbo.GetCapacityLoad(' + CAST(@numDomainID AS VARCHAR) + ',0,0,WO.numWOID,0,4,ISNULL(WO.dtmStartDate,WO.bintCreatedDate),' + CAST(ISNULL(@ClientTimeZoneOffset,0) AS VARCHAR) + ') AS numCapacityLoad
							) TableCapacity
							OUTER APPLY
							(
								SELECT
									FORMAT(FLOOR(SUM(numEstimatedTimeInMinutes) / 60),''00'') + '':'' + FORMAT(SUM(numEstimatedTimeInMinutes) % 60,''00'') vcEstimatedTaskTime
									,FORMAT(FLOOR(SUM(numActualTimeInMinutes) / 60),''00'') + '':'' + FORMAT(SUM(numActualTimeInMinutes) % 60,''00'') vcActualTaskTime
									,SUM(numEstimatedTimeInMinutes) numEstimatedTimeInMinutes
									,SUM(numActualTimeInMinutes) numActualTimeInMinutes
									,SUM(monEstimatedLabourCost) monEstimatedLabourCost
									,SUM(monActualLabourCost) monActualLabourCost
								FROM
								(
									SELECT 
										((ISNULL(SPDT.numHours,0) * 60) + ISNULL(SPDT.numMinutes,0)) * ISNUll(WO.numQtyItemsReq,0)  numEstimatedTimeInMinutes
										,dbo.GetTimeSpendOnTaskInMinutes(' + CAST(@numDomainID AS VARCHAR) + ',SPDT.numTaskId) numActualTimeInMinutes
										,((ISNULL(SPDT.numHours,0) * 60) + ISNULL(SPDT.numMinutes,0)) * ISNUll(WO.numQtyItemsReq,0) * (ISNULL(SPDT.monHourlyRate,0)/60) monEstimatedLabourCost
										,dbo.GetTimeSpendOnTaskInMinutes(' + CAST(@numDomainID AS VARCHAR) + ',SPDT.numTaskId) * (ISNULL(SPDT.monHourlyRate,0)/60) monActualLabourCost
									FROM 
										StagePercentageDetailsTask SPDT
									WHERE 
										numWorkOrderId=WO.numWOId
								) TEMP
							) TableHourCost
							WHERE
								WO.numDomainID='+ CAST(@numDomainID AS VARCHAR(MAX)) +' 
								AND 1 = (CASE ' + CAST(@tintUserRightType AS VARCHAR) + '
											WHEN 3 THEN 1 
											WHEN 1 THEN (CASE 
															WHEN WO.numAssignedTo=' + CAST(@numUserCntID AS VARCHAR) + ' 
																	OR EXISTS (SELECT numTaskId FROM StagePercentageDetailsTask SPDT WHERE SPDT.numWorkOrderId=WO.numWOID AND numAssignTo=' + CAST(@numUserCntID AS VARCHAR) + ') 
																	OR (NOT EXISTS (SELECT numUserDetailID FROM UserMaster WHERE numDomainID='+ CAST(@numDomainID AS VARCHAR(MAX)) +' AND numUserDetailID=' + CAST(@numUserCntID AS VARCHAR) + ') AND EXISTS (SELECT EAD.numExtranetDtlID FROM ExtranetAccountsDtl EAD INNER JOIN ExtarnetAccounts EA ON EAD.numExtranetID=EA.numExtranetID WHERE EAD.numDomainID=' + CAST(@numDomainID AS VARCHAR) + ' AND EAD.numContactID=' + CAST(@numUserCntID AS VARCHAR) + ' AND EA.numDivisionID=Opp.numDivisionID))
															THEN 1 
															ELSE 0 
														END) 
											ELSE 0
										END)
								AND 1 = (CASE WHEN ISNULL('+CAST(@numWOStatus AS VARCHAR(MAX))+',0) = 1 AND WO.numWOStatus=23184 THEN 1  WHEN ISNULL('''+CAST(@numWOStatus AS VARCHAR(500))+''',0)=0 AND WO.numWOStatus<>23184 THEN 1 ELSE 0 END) '+@vcRegularSearchCriteria+' ORDER BY WO.bintCreatedDate desc OFFSET '+CAST(((@CurrentPage - 1) * @PageSize) AS VARCHAR(500))+' ROWS FETCH NEXT '+CAST(@PageSize AS VARCHAR(500))+' ROWS ONLY '

PRINT CAST(@vcDynamicQuery AS NTEXT)
EXEC(@vcDynamicQuery)

	DECLARE @TempColumns TABLE
	(
        vcFieldName VARCHAR(200)
        ,vcDbColumnName VARCHAR(200)
        ,vcOrigDbColumnName VARCHAR(200)
        ,bitAllowSorting BIT
        ,numFieldId INT
        ,bitAllowEdit BIT
        ,bitCustomField BIT
        ,vcAssociatedControlType VARCHAR(50)
        ,vcListItemType  VARCHAR(200)
        ,numListID INT
        ,ListRelID INT
        ,vcLookBackTableName VARCHAR(200)
        ,bitAllowFiltering BIT
        ,vcFieldDataType  VARCHAR(20)
		,intColumnWidth INT
		,bitClosedColumn BIT
	)

	IF @numWOStatus = 1
	BEGIN
		INSERT INTO @TempColumns
		(
			vcFieldName
			,vcDbColumnName
			,vcOrigDbColumnName
			,bitAllowSorting
			,numFieldId
			,bitAllowEdit
			,bitCustomField
			,vcAssociatedControlType
			,vcListItemType
			,numListID
			,ListRelID
			,vcLookBackTableName
			,bitAllowFiltering
			,vcFieldDataType
			,bitClosedColumn
		)
		VALUES
			('Work Order','vcWorkOrderName','vcWorkOrderName',0,16,0,0,'TextBox','',0,0,'WorkOrder',1,'V',1)
			,('Assembly Item (SKU)','AssemblyItemSKU','AssemblyItemSKU',0,17,0,0,'TextBox','',0,0,'Item',1,'V',1)
			,('Quantity','numQtyItemsReq','numQtyItemsReq',0,18,0,0,'TextBox','',0,0,'WorkOrder',1,'V',1)
			,('Requested Finish','dtmEndDate','dtmEndDate',0,19,0,0,'DateField','',0,0,'WorkOrder',1,'V',1)
			,('Actual Finish','dtmProjectFinishDate','dtmProjectFinishDate',0,20,0,0,'DateField','',0,0,'WorkOrder',1,'V',1)
			,('Item Release','ItemReleaseDate','ItemReleaseDate',0,21,0,0,'DateField','',0,0,'OpportunityItems',1,'V',1)
			,('Schedule Performance','SchedulePerformance','SchedulePerformance',0,22,0,0,'SelectBox','',0,0,'WorkOrder',1,'V',1)
			,('Actual Time & $ vs Projected','ActualProjectDifference','ActualProjectDifference',0,23,0,0,'SelectBox','',0,0,'WorkOrder',1,'V',1)
			,('Build Process','Slp_Name','Slp_Name',0,24,0,0,'SelectBox','',0,0,'Sales_process_List_Master',1,'V',1)
			,('Build Manager','vcAssignedTo','vcAssignedTo',0,25,0,0,'SelectBox','',0,0,'WorkOrder',1,'V',1)
	END
	ELSE
	BEGIN
		INSERT INTO @TempColumns
		(
			vcFieldName
			,vcDbColumnName
			,vcOrigDbColumnName
			,bitAllowSorting
			,numFieldId
			,bitAllowEdit
			,bitCustomField
			,vcAssociatedControlType
			,vcListItemType
			,numListID
			,ListRelID
			,vcLookBackTableName
			,bitAllowFiltering
			,vcFieldDataType
			,bitClosedColumn
		)
		VALUES
		('Work Order(Qty)','vcWorkOrderNameWithQty','vcWorkOrderNameWithQty',0,1,0,0,'TextBox','',0,0,'WorkOrder',1,'V',0)
		,('Max Build Qty','MaxBuildQty','MaxBuildQty',0,2,0,0,'SelectBox','',0,0,'WorkOrder',1,'V',0)
		,('Total Progress','TotalProgress','TotalProgress',0,3,0,0,'TextBoxRangePercentage','',0,0,'WorkOrder',1,'V',0)
		,('Item','AssemblyItemSKU','AssemblyItemSKU',0,4,0,0,'TextBox','',0,0,'Item',1,'V',0)
		,('Planned Start','dtmStartDate','dtmStartDate',0,5,0,0,'DateField','',0,0,'WorkOrder',1,'V',0)
		,('Actual Start','dtmActualStartDate','dtmActualStartDate',0,6,0,0,'DateField','',0,0,'WorkOrder',1,'V',0)
		,('Requested Finish','dtmEndDate','dtmEndDate',0,7,0,0,'DateField','',0,0,'WorkOrder',1,'V',0)
		,('Projected Finish','dtmProjectFinishDate','dtmProjectFinishDate',0,8,0,0,'DateField','',0,0,'WorkOrder',1,'V',0)
		,('Item Release','ItemReleaseDate','ItemReleaseDate',0,9,0,0,'DateField','',0,0,'OpportunityItems',1,'V',0)
		,('Forecast','ForecastDetails','ForecastDetails',0,10,0,0,'SelectBox','',0,0,'WorkOrder',1,'V',0)
		,('Build Process','Slp_Name','Slp_Name',0,11,0,0,'SelectBox','',0,0,'Sales_process_List_Master',1,'V',0)
		,('Build Manager','vcAssignedTo','vcAssignedTo',0,12,0,0,'SelectBox','',0,0,'WorkOrder',1,'V',0)
		,('Capacity Load','CapacityLoad','CapacityLoad',0,13,0,0,'TextBoxRangePercentage','',0,0,'WorkOrder',1,'V',0)
		,('Work Order Status','vcWorkOrderStatus','WorkOrderStatus',0,14,0,0,'Label','',0,0,'WorkOrder',1,'V',0)
		,('Picked | Remaining','vcPickedRemaining','PickedRemaining',0,15,0,0,'Label','',0,0,'WorkOrder',1,'V',0)
	END

	UPDATE
		TC
	SET
		TC.intColumnWidth = ISNULL(DFCD.intColumnWidth,0)
	FROM
		@TempColumns TC
	INNER JOIN
		DycFormConfigurationDetails DFCD
	ON
		TC.numFieldId = DFCD.numFieldId
	WHERE
		DFCD.numDomainId = @numDomainID
		AND DFCD.numUserCntID = @numUserCntID
		AND DFCD.numFormId = 145
		AND DFCD.tintPageType = 1


	IF @numWOStatus <> 1 AND EXISTS (SELECT numFieldId FROM DycFormConfigurationDetails WHERE numDomainId = @numDomainID AND numUserCntID = @numUserCntID AND numFormId = 145 AND tintPageType = 1)
	BEGIN
		SELECT 
			TC.*
		FROM 
			DycFormConfigurationDetails DFCD 
		INNER JOIN 
			@TempColumns TC 
		ON 
			DFCD.numFieldId=TC.numFieldId
		WHERE 
			numDomainId = @numDomainID 
			AND numUserCntID = @numUserCntID 
			AND numFormId = 145 
			AND tintPageType = 1
		ORDER BY
			DFCD.intRowNum
	END
	ELSE
	BEGIN
		SELECT * FROM @TempColumns
	END
END
GO

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Project_GetTasks')
DROP PROCEDURE USP_Project_GetTasks
GO
CREATE PROCEDURE [dbo].[USP_Project_GetTasks]
(
	@numDomainID NUMERIC(18,0)
	,@numProId NUMERIC(18,0)
	,@numStageDetailsId NUMERIC(18,0)
	,@ClientTimeZoneOffset INT
	,@tintMode TINYINT
)
AS 
BEGIN
	IF @tintMode = 1
	BEGIN
		DECLARE @numQtyToBuild FLOAT
		DECLARE @dtPlannedStartDate DATETIME
		SELECT 
			@numQtyToBuild=1
			,@dtPlannedStartDate=ISNULL(ProjectsMaster.dtmStartDate,ProjectsMaster.bintCreatedDate) 
		FROM 
			ProjectsMaster 
		WHERE
			ProjectsMaster.numDomainID=@numDomainID 
			AND ProjectsMaster.numProId=@numProId

		DECLARE @TempTaskAssignee TABLE
		(
			numAssignedTo NUMERIC(18,0)
			,dtLastTaskCompletionTime DATETIME
		)

		DECLARE @TempTasks TABLE
		(
			ID INT IDENTITY(1,1)
			,numTaskID NUMERIC(18,0)
			,numTaskTimeInMinutes NUMERIC(18,0)
			,numTaskAssignee NUMERIC(18,0)
			,intTaskType INT --1:Parallel, 2:Sequential
			,dtPlannedStartDate DATETIME
		)

		INSERT INTO @TempTasks
		(
			numTaskID
			,numTaskTimeInMinutes
			,intTaskType
			,numTaskAssignee
		)
		SELECT 
			SPDT.numTaskID
			,((ISNULL(SPDT.numHours,0) * 60) + ISNULL(SPDT.numMinutes,0)) * @numQtyToBuild
			,ISNULL(SPDT.intTaskType,1)
			,SPDT.numAssignTo
		FROM 
			StagePercentageDetailsTask SPDT
		INNER JOIN
			ProjectsMaster PO
		ON
			SPDT.numProjectId = PO.numProId
		WHERE 
			SPDT.numDomainId=@numDomainID
			AND SPDT.numProjectId = @numProId

		UPDATE @TempTasks SET dtPlannedStartDate=@dtPlannedStartDate

		INSERT INTO @TempTaskAssignee
		(
			numAssignedTo
		)
		SELECT DISTINCT
			numTaskAssignee
		FROM
			@TempTasks

		DECLARE @i INT = 1
		DECLARE @iCount INT 	
		SELECT @iCount = COUNT(*) FROM @TempTasks

		DECLARE @numTaskAssignee NUMERIC(18,0)
		DECLARE @intTaskType INT
		DECLARE @numWorkScheduleID NUMERIC(18,0)
		DECLARE @numTempUserCntID NUMERIC(18,0)
		DECLARE @dtStartDate DATETIME
		DECLARE @numTotalTaskInMinutes NUMERIC(18,0)
		DECLARE @numProductiveTimeInMinutes NUMERIC(18,0)
		DECLARE @tmStartOfDay TIME(7)
		DECLARE @numTimeLeftForDay NUMERIC(18,0)
		DECLARE @vcWorkDays VARCHAR(20)
		DECLARE @bitParallelStartSet BIT = 0

		WHILE @i <= @iCount
		BEGIN
			SELECT
				@numTaskAssignee=numTaskAssignee
				,@numTotalTaskInMinutes=numTaskTimeInMinutes
				,@intTaskType = intTaskType
			FROM
				@TempTasks 
			WHERE
				ID=@i

			-- GET TASK COMPLETION TIME WHICH WILL BE START DATETIME FOR NEXT TASK
			SELECT
				@numWorkScheduleID = WS.ID
				,@numProductiveTimeInMinutes=(ISNULL(WS.numProductiveHours,0) * 60) + ISNULL(WS.numProductiveMinutes,0)
				,@tmStartOfDay = tmStartOfDay
				,@vcWorkDays=vcWorkDays
				,@dtStartDate = DATEADD(MINUTE,@ClientTimeZoneOffset,CONVERT(DATETIME,CONVERT(VARCHAR(8),DATEADD(MINUTE,-@ClientTimeZoneOffset,@dtPlannedStartDate),112)) + tmStartOfDay)
			FROM
				WorkSchedule WS
			INNER JOIN
				UserMaster
			ON
				WS.numUserCntID = UserMaster.numUserDetailId
			WHERE 
				WS.numUserCntID = @numTaskAssignee

			IF @intTaskType=1 AND EXISTS (SELECT numAssignedTo FROM @TempTaskAssignee WHERE numAssignedTo = @numTaskAssignee AND dtLastTaskCompletionTime IS NOT NULL)
			BEGIN
				SELECT @dtStartDate = dtLastTaskCompletionTime FROM @TempTaskAssignee WHERE numAssignedTo = @numTaskAssignee
			END
			ELSE IF EXISTS (SELECT numAssignedTo FROM @TempTaskAssignee WHERE numAssignedTo = @numTaskAssignee AND dtLastTaskCompletionTime IS NOT NULL)
			BEGIN
				SELECT @dtStartDate = dtLastTaskCompletionTime FROM @TempTaskAssignee WHERE numAssignedTo = @numTaskAssignee
			END

			UPDATE @TempTasks SET dtPlannedStartDate=@dtStartDate WHERE ID=@i 

			IF @numProductiveTimeInMinutes > 0 AND @numTotalTaskInMinutes > 0
			BEGIN
				WHILE @numTotalTaskInMinutes > 0
				BEGIN
					-- IF ITS WORKING DAY AND ALSO NOT HOLIDAY THEN PROCEED OR INCRESE DATE TO NEXT DAY
					IF DATEPART(WEEKDAY,@dtStartDate) IN (SELECT Id FROM dbo.SplitIDs(@vcWorkDays,',')) AND NOT EXISTS (SELECT ID FROM WorkScheduleDaysOff WHERE numWorkScheduleID=@numWorkScheduleID AND CAST(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,@dtStartDate) AS DATE) BETWEEN dtDayOffFrom AND dtDayOffTo)
					BEGIN
						IF CAST(@dtStartDate AS DATE) = CAST(GETUTCDATE() AS DATE)
						BEGIN
							-- CHECK TIME LEFT FOR DAY BASED
							SET @numTimeLeftForDay = DATEDIFF(MINUTE,@dtStartDate,DATEADD(MINUTE,@numProductiveTimeInMinutes,DATEADD(MINUTE,@ClientTimeZoneOffset,CONVERT(DATETIME,CONVERT(VARCHAR(8),DATEADD(MINUTE,-@ClientTimeZoneOffset,@dtStartDate),112)) + @tmStartOfDay)))
						END
						ELSE
						BEGIN
							SET @numTimeLeftForDay = @numProductiveTimeInMinutes
						END

						IF @numTimeLeftForDay > 0
						BEGIN
							IF @numTimeLeftForDay > @numTotalTaskInMinutes
							BEGIN
								SET @dtStartDate = DATEADD(MINUTE,@numTotalTaskInMinutes,@dtStartDate)
							END
							ELSE
							BEGIN
								SET @dtStartDate = DATEADD(MINUTE,@ClientTimeZoneOffset,CONVERT(DATETIME,CONVERT(VARCHAR(8),DATEADD(MINUTE,-@ClientTimeZoneOffset,DATEADD(DAY,1,@dtStartDate)),112)) + @tmStartOfDay)
							END

							SET @numTotalTaskInMinutes = @numTotalTaskInMinutes - @numTimeLeftForDay
						END
						ELSE
						BEGIN
							SET @dtStartDate = DATEADD(MINUTE,@ClientTimeZoneOffset,CONVERT(DATETIME,CONVERT(VARCHAR(8),DATEADD(MINUTE,-@ClientTimeZoneOffset,DATEADD(DAY,1,@dtStartDate)),112)) + @tmStartOfDay)
						END
					END
					ELSE
					BEGIN
						SET @dtStartDate = DATEADD(MINUTE,@ClientTimeZoneOffset,CONVERT(DATETIME,CONVERT(VARCHAR(8),DATEADD(MINUTE,-@ClientTimeZoneOffset,DATEADD(DAY,1,@dtStartDate)),112)) + @tmStartOfDay)					
					END				
				END
			END	

			UPDATE @TempTaskAssignee SET dtLastTaskCompletionTime=@dtStartDate WHERE numAssignedTo=@numTaskAssignee

			SET @i = @i + 1
		END

		SELECT	
			SPDT.numTaskId
			,SPDT.numReferenceTaskId
			,SPDT.vcTaskName
			,A.vcEmail
			,SPDT.numAssignTo AS numAssignedTo 
			,ISNULL(SPDT.bitTimeAddedToContract,0) AS bitTimeAddedToContract
			,dbo.fn_GetContactName(SPDT.numAssignTo) vcAssignedTo
			,ISNULL(L.vcData,'-') AS vcWorkStation
			,ISNULL((SELECT SUM(numProcessedQty) FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId),0) AS numProcessedQty
			,1 - ISNULL((SELECT SUM(numProcessedQty) FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId),0) AS numRemainingQty
			,CASE ISNULL((SELECT TOP 1 tintAction FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId ORDER BY dtActionTime DESC,ID DESC),0) 
				WHEN 4 THEN '<img src="../images/comflag.png" />'
				WHEN 3 THEN CONCAT('<ul class="list-inline" style="margin-bottom:0px;"><li style="vertical-align:middle;padding-left:0px;padding-right:0px;"><button class="btn btn-xs btn-info" onclick="return ShowTaskTimeLogWindow(',SPDT.numTaskId,');"><i class="fa fa-clock-o"></i></button></li><li style="vertical-align:middle;padding-right:0px;"><button class="btn btn-xs btn-flat btn-warning" onclick="return ShowTaskPausedWindow(this,',SPDT.numTaskId,');">Pause</button></li><li style="vertical-align:middle;padding-right:0px;"><button class="btn btn-xs btn-flat btn-danger btn-task-finish" onclick="return TaskFinished(this,',SPDT.numTaskId,',4);">Finish</button></li><li style="vertical-align:middle;padding-right:0px;"><label class="taskTimerInitial"></label></li></ul>')
				WHEN 2 THEN CONCAT('<ul class="list-inline" style="margin-bottom:0px;"><li style="vertical-align:middle;padding-left:0px;padding-right:0px;"><button class="btn btn-xs btn-info" onclick="return ShowTaskTimeLogWindow(',SPDT.numTaskId,');"><i class="fa fa-clock-o"></i></button></li><li style="vertical-align:middle;padding-right:0px;"><button class="btn btn-xs btn-flat bg-purple" onclick="return TaskStarted(this,',SPDT.numTaskId,',1);">Resume</button></li><li style="vertical-align:middle;padding-right:0px;"><button class="btn btn-xs btn-flat btn-danger btn-task-finish" onclick="return TaskFinished(this,',SPDT.numTaskId,',4);">Finish</button></li></ul>')
				WHEN 1 THEN CONCAT('<ul class="list-inline" style="margin-bottom:0px;"><li style="vertical-align:middle;padding-left:0px;padding-right:0px;"><button class="btn btn-xs btn-info" onclick="return ShowTaskTimeLogWindow(',SPDT.numTaskId,');"><i class="fa fa-clock-o"></i></button></li><li style="vertical-align:middle;padding-right:0px;"><button class="btn btn-xs btn-flat btn-warning" onclick="return ShowTaskPausedWindow(this,',SPDT.numTaskId,');">Pause</button></li><li style="vertical-align:middle;padding-right:0px;"><button class="btn btn-xs btn-flat btn-danger btn-task-finish" onclick="return TaskFinished(this,',SPDT.numTaskId,',4);">Finish</button></li><li style="vertical-align:middle;padding-right:0px;"><label class="taskTimerInitial"></label></li></ul>')
				ELSE CONCAT('<ul class="list-inline" style="margin-bottom:0px;"><li style="vertical-align:middle;padding-left:0px;padding-right:0px;"><button class="btn btn-xs btn-info" onclick="return ShowTaskTimeLogWindow(',SPDT.numTaskId,');"><i class="fa fa-clock-o"></i></button></li><li style="vertical-align:middle;padding-right:0px;"><button class="btn btn-xs btn-flat btn-success btn-task-start" onclick="return TaskStarted(this,',SPDT.numTaskId,',0);">Start</button></li><li style="vertical-align:middle;padding-right:0px;"><button class="btn btn-xs btn-flat btn-danger btn-task-finish" onclick="return TaskFinished(this,',SPDT.numTaskId,',4);">Finish</button></li></ul>')
			END  AS vcTaskControls
			,FORMAT((((ISNULL(SPDT.numHours,0) * 60) + ISNULL(SPDT.numMinutes,0)) * @numQtyToBuild) / 60,'00') + ':' + FORMAT((((ISNULL(SPDT.numHours,0) * 60) + ISNULL(SPDT.numMinutes,0)) * CAST(@numQtyToBuild AS DECIMAL)) % 60.0,'00')  vcEstimatedTaskTime
			,(((ISNULL(SPDT.numHours,0) * 60) + ISNULL(SPDT.numMinutes,0)) * @numQtyToBuild) numTaskEstimationInMinutes
			,(CASE ISNULL((SELECT TOP 1 tintAction FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=SPDT.numTaskId ORDER BY SPDTTL.dtActionTime DESC,SPDTTL.ID DESC),0) 
				WHEN 4 THEN 0
				WHEN 3 THEN dbo.GetTimeSpendOnTaskByProject(SPDT.numDomainID,SPDT.numTaskId,2)
				WHEN 2 THEN dbo.GetTimeSpendOnTaskByProject(SPDT.numDomainID,SPDT.numTaskId,2)
				WHEN 1 THEN 0
			END) numTimeSpentInMinutes
			,(CASE ISNULL((SELECT TOP 1 tintAction FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId ORDER BY dtActionTime DESC,ID DESC),0) 
				WHEN 4 THEN NULL
				WHEN 3 THEN DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,(SELECT TOP 1 dtActionTime FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=SPDT.numTaskId ORDER BY SPDTTL.dtActionTime DESC,SPDTTL.ID DESC))
				WHEN 2 THEN NULL
				WHEN 1 THEN DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,(SELECT TOP 1 dtActionTime FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=SPDT.numTaskId ORDER BY SPDTTL.dtActionTime DESC,SPDTTL.ID DESC))
			END) dtLastStartDate
			,(CASE WHEN dbo.GetTimeSpendOnTaskByProject(SPDT.numDomainID,SPDT.numTaskId,0) = 'Invalid time sequence' THEN '00:00' ELSE dbo.GetTimeSpendOnTaskByProject(SPDT.numDomainID,SPDT.numTaskId,0) END) vcActualTaskTime
			,CASE WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId AND tintAction=4)
				THEN dbo.GetTimeSpendOnTaskByProject(SPDT.numDomainID,SPDT.numTaskId,1)
				ELSE ''
			END vcActualTaskTimeHtml
			,CASE 
				WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId AND tintAction=1)
				THEN CONCAT('<span>',dbo.FormatedDateTimeFromDate(DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,(SELECT dtActionTime FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId AND tintAction=1)),@numDomainID),'</span>')
				ELSE CONCAT('<span style="color:#a6a6a6">',dbo.FormatedDateTimeFromDate((CASE
										WHEN TT.dtPlannedStartDate IS NULL
										THEN DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,ISNULL(PO.dtmStartDate,PO.bintCreatedDate))
										ELSE DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,TT.dtPlannedStartDate)
									END),@numDomainID),'<i> (planned)</i></span>')
			END dtPlannedStartDate
			,CASE WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId AND tintAction=1)
				THEN DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,(SELECT dtActionTime FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId AND tintAction=1))
				ELSE ''
			END AS dtStartDate
			,CASE WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId AND tintAction=4)
				THEN DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,(SELECT dtActionTime FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId AND tintAction=4))
				ELSE ''
			END AS dtFinishDate
			,CASE WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId AND tintAction=4)
				THEN dbo.FormatedDateTimeFromDate(DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,(SELECT dtActionTime FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId AND tintAction=4)),@numDomainID)
				ELSE ''
			END AS vcFinishDate
			,CASE WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId AND tintAction=4)
				THEN 1
				ELSE 0
			END AS bitTaskCompleted
			,(SELECT COUNT(*) FROM TopicMaster WHERE intRecordType=4 AND numRecordId=SPDT.numTaskId) AS TopicCount
		FROM 
			StagePercentageDetailsTask SPDT
		INNER JOIN
			ProjectsMaster PO
		ON
			SPDT.numProjectId = PO.numProId
		LEFT JOIN
			@TempTasks TT
		ON
			SPDT.numTaskId = TT.numTaskID
		LEFT JOIN
			AdditionalContactsInformation AS A
		ON
			SPDT.numAssignTo=A.numContactId
		LEFT JOIN
			ListDetails AS L
		ON
			A.numTeam=L.numListItemID
		WHERE
			SPDT.numStageDetailsId = @numStageDetailsId
			AND SPDT.numProjectId = @numProId
	END
	ELSE IF @tintMode = 2
	BEGIN
		SELECT	
			SPDT.numTaskId
			,SPDT.numReferenceTaskId
			,SPDT.numStageDetailsId
			,SPDT.vcTaskName
			,ISNULL(SPDT.numHours,0) numHours
			,ISNULL(SPDT.numMinutes,0) numMinutes
			,SPDT.numAssignTo
			,SPDT.numAssignTo AS numAssignedTo 
			,ISNULL(SPDT.bitTimeAddedToContract,0) AS bitTimeAddedToContract
			,ADC.numTeam
			,CONCAT(FORMAT(ISNULL(SPDT.numHours,0),'00'),':',FORMAT(ISNULL(SPDT.numMinutes,0),'00')) vcEstimatedTaskTime
			,(CASE WHEN dbo.GetTimeSpendOnTaskByProject(SPDT.numDomainID,SPDT.numTaskId,0) = 'Invalid time sequence' THEN '00:00' ELSE dbo.GetTimeSpendOnTaskByProject(SPDT.numDomainID,SPDT.numTaskId,0) END) vcActualTaskTime
			,CASE WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId AND tintAction=1)
				THEN DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,(SELECT dtActionTime FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId AND tintAction=1))
				ELSE ''
			END AS dtStartDate
			,CASE WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId AND tintAction=4)
				THEN DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,(SELECT dtActionTime FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId AND tintAction=4))
				ELSE ''
			END AS dtFinishDate
			,CASE WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId)
				THEN 1
				ELSE 0
			END bitTaskStarted,
			(SELECT COUNT(*) FROM TopicMaster WHERE intRecordType=4 AND numRecordId=SPDT.numTaskId) AS TopicCount
		FROM 
			StagePercentageDetailsTask SPDT
		INNER JOIN
			ProjectsMaster PO
		ON
			SPDT.numProjectId = PO.numProId
		LEFT JOIN
			AdditionalContactsInformation ADC
		ON
			SPDT.numAssignTo = ADC.numContactId
		LEFT JOIN
			@TempTasks TT
		ON
			SPDT.numTaskId = TT.numTaskID
		WHERE
			SPDT.numStageDetailsId = @numStageDetailsId
			AND SPDT.numProjectId = @numProId
	END
END
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_TicklerActItemsV2')
DROP PROCEDURE USP_TicklerActItemsV2
GO
CREATE Proc [dbo].[USP_TicklerActItemsV2]       
@numUserCntID as numeric(9)=null,                                                                        
@numDomainID as numeric(9)=null,                                                                        
@startDate as datetime,                                                                        
@endDate as datetime,                                                                    
@ClientTimeZoneOffset Int,  --Added by Debasish to enable calculation of date according to client machine             
@bitFilterRecord bit=0,
@columnName varchar(50), 
@columnSortOrder varchar(50),
@vcBProcessValue varchar(500),
@RegularSearchCriteria VARCHAR(MAX)='',
@CustomSearchCriteria VARCHAR(MAX)='', 
@OppStatus int,
@CurrentPage int,                                                              
@PageSize int
AS
BEGIN
	SET @endDate = DATEADD(MILLISECOND,-2,DATEADD(DAY,1,@endDate))

--SET @PageSize=10
DECLARE @tintActionItemsViewRights TINYINT  = 3
DECLARE @bitREQPOApproval AS BIT
DECLARE @bitARInvoiceDue AS BIT
DECLARE @bitAPBillsDue AS BIT
DECLARE @vchARInvoiceDue AS VARCHAR(500)
DECLARE @vchAPBillsDue AS VARCHAR(500)
DECLARE @decReqPOMinValue AS DECIMAL(18,2)
DECLARE @vchREQPOApprovalEmp AS VARCHAR(500)=''
DECLARE @numDomainDivisionID NUMERIC(18,0)
DECLARE @dynamicPartQuery AS NVARCHAR(MAX)=''
DECLARE @ActivityRegularSearch AS VARCHAR(MAX)=''
DECLARE @OtherRegularSearch AS VARCHAR(MAX)=''
DECLARE @IndivRecordPaging AS NVARCHAR(MAX)=''
--SET @IndivRecordPaging=' '
DECLARE @TotalRecordCount INT =0
SET @IndivRecordPaging=  ' ORDER BY dtDueDate DESC OFFSET '+CAST(((@CurrentPage - 1 ) * 10) AS VARCHAR) +' ROWS FETCH NEXT '+CAST(10 AS VARCHAR)+' ROWS ONLY'

SELECT TOP 1 
	@bitREQPOApproval=bitREQPOApproval,
	@vchREQPOApprovalEmp=vchREQPOApprovalEmp,
	@decReqPOMinValue=ISNULL(decReqPOMinValue,0),
	@bitARInvoiceDue=bitARInvoiceDue,
	@bitAPBillsDue = bitAPBillsDue,
	@vchARInvoiceDue = vchARInvoiceDue,
	@vchAPBillsDue = vchAPBillsDue,
	@numDomainDivisionID=numDivisionID
FROM 
	Domain 
WHERE 
	numDomainId=@numDomainID

IF EXISTS (SELECT * FROM GroupAuthorization WHERE numDomainID=@numDomainID AND numModuleID=1 AND numPageID=2 AND numGroupID IN (SELECT ISNULL(numGroupID,0) FROM UserMaster WHERE numDomainID=@numDomainID AND numUserDetailId=@numUserCntID))
BEGIN
	SELECT @tintActionItemsViewRights=ISNULL(intViewAllowed,0) FROM GroupAuthorization WHERE numDomainID=@numDomainID AND numModuleID=1 AND numPageID=2 AND numGroupID IN (SELECT ISNULL(numGroupID,0) FROM UserMaster WHERE numDomainID=@numDomainID AND numUserDetailId=@numUserCntID)
END
--12 - Communication task
--1 - Case Task
--2 - Project Task
--3 - Opportunity Task
--4 - Sales Order Task
--5 - Work Order Management
--6 - Work Center Task
--7 - Requisition Approval
--8 - PO Approval
--9 - Price Margin Approval
--10 - A/R Invoice Due
--11 - A/P Bill Due

DECLARE @dynamicQuery AS NVARCHAR(MAX)=''
DECLARE @dynamicQueryCount AS NVARCHAR(MAX)=''

Create table #tempRecords (RecordCount NUMERIC(9),RecordId NUMERIC(9),numTaskId numeric(9),IsTaskClosed bit,TaskTypeName VARCHAR(100),TaskType NUMERIC(9),
OrigDescription NVARCHAR(MAX),Description NVARCHAR(MAX),TotalProgress INT,numAssignToId NUMERIC(9),
Priority NVARCHAR(MAX),PriorityId  NUMERIC(9),ActivityId NUMERIC(9),OrgName NVARCHAR(MAX),CompanyRating NVARCHAR(MAX),
numContactID numeric(9),numDivisionID numeric(9),tintCRMType tinyint,
Id numeric(9),bitTask varchar(100),Startdate datetime,EndTime datetime,
itemDesc text,[Name] varchar(200),vcFirstName varchar(100),vcLastName varchar(100),numPhone varchar(15),numPhoneExtension varchar(7),
[Phone] varchar(30),vcCompanyName varchar(100),vcProfile VARCHAR(100), vcEmail varchar(50),Task varchar(100),Activity varchar(MAX),
Status varchar(100),numCreatedBy numeric(9),numRecOwner VARCHAR(500), numModifiedBy VARCHAR(500), numOrgTerId NUMERIC(18,0), numTerId VARCHAR(200),numAssignedTo varchar(1000),numAssignedBy varchar(50),
 caseid numeric(9),vcCasenumber varchar(50),casetimeId numeric(9),caseExpId numeric(9),type int,itemid varchar(50),
changekey varchar(50),dtDueDate DATETIME, DueDate varchar(50),bitFollowUpAnyTime bit,numNoOfEmployeesId varchar(50),vcComPhone varchar(50),numFollowUpStatus varchar(200),dtLastFollowUp varchar(500),vcLastSalesOrderDate VARCHAR(100),monDealAmount DECIMAL(20,5), vcPerformance VARCHAR(100),vcColorScheme VARCHAR(50),
Slp_Name varchar(100),intTotalProgress INT,vcPoppName varchar(100), numOppId NUMERIC(18,0),numBusinessProcessID numeric(18,0),
numCompanyRating varchar(500),numStatusID varchar(500),numCampaignID varchar(500),[Action-Item Participants] NVARCHAR(MAX),HtmlLink varchar(500),
Location nvarchar(150),OrignalDescription text)                                                         
 

declare @strSql as varchar(MAX)                                                            
declare @strSql1 as varchar(MAX)                         
declare @strSql2 as varchar(MAX)                                                            
declare @vcCustomColumnName AS VARCHAR(8000)
declare @vcnullCustomColumnName AS VARCHAR(8000)
   -------Change Row Color-------
Declare @vcCSOrigDbCOlumnName as varchar(50),@vcCSLookBackTableName as varchar(50),@vcCSAssociatedControlType as varchar(50)
SET @vcCSOrigDbCOlumnName=''
SET @vcCSLookBackTableName=''

declare @tintOrder as tinyint                                                
declare @vcFieldName as varchar(50)                                                
declare @vcListItemType as varchar(3)                                           
declare @vcAssociatedControlType varchar(20)                                                
declare @numListID AS numeric(9)                                                
declare @vcDbColumnName varchar(20)                    
declare @WhereCondition varchar(2000)                     
declare @vcLookBackTableName varchar(2000)              
Declare @bitCustomField as BIT
DECLARE @bitAllowEdit AS CHAR(1)                 
Declare @numFieldId as numeric  
DECLARE @bitAllowSorting AS CHAR(1)   
declare @vcColumnName AS VARCHAR(500)       
declare @vcCustomColumnNameOnly AS VARCHAR(500)       


IF(@RegularSearchCriteria='')
BEGIN
	SET @RegularSearchCriteria =@RegularSearchCriteria + ' 1=1'
END
if(@OppStatus=0)
BEGIN
SET @RegularSearchCriteria =@RegularSearchCriteria + ' AND IsTaskClosed = 0'
END
if(@OppStatus=1)
BEGIN
SET @RegularSearchCriteria = @RegularSearchCriteria + ' AND IsTaskClosed = 1'
END
 SET @RegularSearchCriteria  = REPLACE(@RegularSearchCriteria,'TaskTypeName','TaskType')

 SET @ActivityRegularSearch = ''
 SET @OtherRegularSearch = ''

 SET @ActivityRegularSearch = ISNULL((SELECT STUFF( 
(
	SELECT  'AND ' + [Items] FROM 
	(
	  SELECT [Items] FROM SplitByString(@RegularSearchCriteria,'AND') WHERE Items<>'' AND 1=
	  (CASE WHEN Items LIKE '%IsTaskClosed%' THEN 0
	 WHEN Items LIKE '%TaskType%'  THEN 0
	 WHEN  Items LIKE '%numAssignToId%'  THEN 0
	 WHEN  Items LIKE '%PriorityId%'  THEN 0
	 WHEN  Items LIKE '%ActivityId%'  THEN 0
	 WHEN  Items LIKE '%Description%'   THEN 0
	 WHEN  Items LIKE '%OrgName%'   THEN 0 ELSE 1 END)
	) AS T FOR XML PATH('')
)
,1,3,'') AS [Items]),'')
SET @ActivityRegularSearch = REPLACE(@ActivityRegularSearch,'&gt;','>')
SET @ActivityRegularSearch = REPLACE(@ActivityRegularSearch,'&lt;','<')
PRINT @ActivityRegularSearch
 SET @OtherRegularSearch = ISNULL((SELECT STUFF( 
(
	SELECT  'AND ' + [Items] FROM 
	(
	  SELECT [Items] FROM SplitByString(@RegularSearchCriteria,'AND') WHERE Items<>'' AND 1=
	  (CASE WHEN Items LIKE '%IsTaskClosed%' THEN 1
	 WHEN Items LIKE '%TaskType%'  THEN 1
	 WHEN  Items LIKE '%numAssignToId%'  THEN 1
	 WHEN  Items LIKE '%PriorityId%'  THEN 1
	 WHEN  Items LIKE '%ActivityId%'  THEN 1
	 WHEN  Items LIKE '%Description%'   THEN 1
	 WHEN  Items LIKE '%OrgName%'   THEN 1 ELSE 0 END)
	) AS T FOR XML PATH('')
)
,1,3,'') AS [Items]),'')
SET @OtherRegularSearch = ISNULL(REPLACE(@OtherRegularSearch,'T.',''),'')
SET @OtherRegularSearch = REPLACE(@OtherRegularSearch,'&gt;','>')
SET @OtherRegularSearch = REPLACE(@OtherRegularSearch,'&lt;','<')
IF(LEN(@OtherRegularSearch)=0)
BEGIN
	SET @OtherRegularSearch = ' 1=1 '
END
PRINT @OtherRegularSearch
Declare @ListRelID as numeric(9)             
set @tintOrder=0   

CREATE TABLE #tempForm 
	(
		tintOrder INT,vcDbColumnName nvarchar(50),vcFieldDataType nvarchar(50),vcOrigDbColumnName nvarchar(50),bitAllowFiltering BIT,vcFieldName nvarchar(50)
		,vcAssociatedControlType nvarchar(50),vcListItemType char(3),numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,numFieldId NUMERIC
		,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,bitIsAlphaNumeric BIT,bitIsNumeric BIT,bitIsLengthValidation BIT,intMaxLength int
		,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),ListRelID numeric(9),intColumnWidth NUMERIC(18,2)
	)
                
	--Custom Fields related to Organization
	INSERT INTO 
		#tempForm
	SELECT  
		tintRow + 1 AS tintOrder,vcDbColumnName,'',vcDbColumnName,1,vcFieldName,vcAssociatedControlType,'' as vcListItemType,numListID,'' as vcLookBackTableName                                                 
		,bitCustom,numFieldId,bitAllowSorting,0,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength
		,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth
	FROM 
		View_DynamicCustomColumns
	WHERE 
		numFormId=43 
		AND numUserCntID=@numUserCntID 
		AND numDomainID=@numDomainID 
		AND tintPageType=1
		AND ISNULL(bitCustom,0)=1
 
	SELECT TOP 1 
		@tintOrder = tintOrder + 1
		,@vcDbColumnName=vcDbColumnName
		,@vcFieldName=vcFieldName
		,@vcAssociatedControlType=vcAssociatedControlType
		,@vcListItemType=vcListItemType
		,@numListID=numListID
		,@vcLookBackTableName=vcLookBackTableName                                               
		,@bitCustomField=bitCustomField
		,@numFieldId=numFieldId
		,@bitAllowSorting=bitAllowSorting
		,@bitAllowEdit=0
		,@ListRelID=ListRelID
	FROM 
		#tempForm 
	ORDER BY 
		tintOrder ASC     
  
	SET @vcCustomColumnName=''
	SET @vcnullCustomColumnName=''
	SET @vcCustomColumnNameOnly = ''
	WHILE @tintOrder>0      
	BEGIN  
		SET @vcColumnName= @vcDbColumnName
		SET @strSql = 'ALTER TABLE #tempRecords ADD [' + @vcColumnName + '] NVARCHAR(MAX)'
		SET @vcCustomColumnNameOnly = @vcCustomColumnNameOnly+','+@vcColumnName
		EXEC(@strSql)
		
		IF @bitCustomField = 1
		BEGIN 
			IF @vcAssociatedControlType = 'TextBox' OR @vcAssociatedControlType = 'TextArea'            
			BEGIN            
				SET @vcCustomColumnName= @vcCustomColumnName+', ISNULL((SELECT TOP 1 CFW.Fld_Value FROM CFW_FLD_Values AS CFW WHERE CFW.Fld_ID='+CAST(@numFieldId AS VARCHAR(50))+' AND CFW.RecId=Div.numDivisionID),'''') ['+ @vcColumnName +']'
				SET @vcnullCustomColumnName= @vcnullCustomColumnName+','''' AS  ['+ @vcColumnName +']'
			END  
			ELSE IF @vcAssociatedControlType = 'CheckBox'         
			BEGIN            
				SET @vcCustomColumnName= @vcCustomColumnName+',ISNULL((SELECT TOP 1 CFW.Fld_Value FROM CFW_FLD_Values AS CFW WHERE CFW.Fld_ID='+CAST(@numFieldId AS VARCHAR(50))+' AND CFW.RecId=Div.numDivisionID),0)  ['+ @vcColumnName + ']'
				SET @vcnullCustomColumnName= @vcnullCustomColumnName+',0 AS ['+ @vcColumnName + ']'
			END            
			ELSE IF @vcAssociatedControlType = 'DateField'        
			BEGIN            
				SET @vcCustomColumnName= @vcCustomColumnName+',dbo.FormatedDateFromDate((SELECT TOP 1 CFW.Fld_Value FROM CFW_FLD_Values AS CFW WHERE CFW.Fld_ID='+CAST(@numFieldId AS VARCHAR(50))+' AND CFW.RecId=Div.numDivisionID),'+convert(varchar(10),@numDomainId)+')  ['+ @vcColumnName +']'              
				SET @vcnullCustomColumnName= @vcnullCustomColumnName+','''' AS  ['+ @vcColumnName +']'
			END            
			ELSE IF @vcAssociatedControlType = 'SelectBox'           
			BEGIN            
				SET @vcCustomColumnName=@vcCustomColumnName+',(SELECT TOP 1 ISNULL(L.vcData,'''') FROM CFW_FLD_Values CFW left Join ListDetails L on L.numListItemID=CFW.Fld_Value WHERE  CFW.Fld_ID='+CAST(@numFieldId AS VARCHAR(50))+' AND CFW.RecId=Div.numDivisionID)'+' ['+ @vcColumnName +']'                                                         
				SET @vcnullCustomColumnName=@vcnullCustomColumnName+','''''+'AS  ['+ @vcColumnName +']'                                                         
			END
			ELSE IF @vcAssociatedControlType = 'CheckBoxList'
			BEGIN
				SET @vcCustomColumnName=@vcCustomColumnName+',(SELECT STUFF((SELECT CONCAT('', '', vcData) FROM ListDetails WHERE numListID= ' + CAST(@numListID AS varchar) + ' AND numlistitemid IN (SELECT ISNULL(Id,0) FROM dbo.SplitIDs(ISNULL((SELECT CFWInner.Fld_Value FROM CFW_FLD_Values CFWInner WHERE CFWInner.Fld_ID='+CAST(@numFieldId AS VARCHAR(50))+' AND CFWInner.RecId=Div.numDivisionID),''''),'','')) FOR XML PATH('''')), 1, 1, ''''))'+' ['+ @vcColumnName +']'
				SET @vcnullCustomColumnName=@vcnullCustomColumnName+','''''+'AS  ['+ @vcColumnName +']'
			END
			ELSE
			BEGIN
				SET @vcCustomColumnName= CONCAT(@vcCustomColumnName,',(SELECT dbo.fn_GetCustFldStringValue(',@numFieldId,',CFW.RecId,CFW.Fld_Value',') FROM CFW_FLD_Values CFW left Join ListDetails L on L.numListItemID=CFW.Fld_Value WHERE  CFW.Fld_ID=',@numFieldId,' AND CFW.RecId=Div.numDivisionID)',' [', @vcColumnName ,']')
				SET @vcnullCustomColumnName=@vcnullCustomColumnName+','''''+'AS  ['+ @vcColumnName +']' 
			END
		END
		  
		SELECT TOP 1 
			@tintOrder=tintOrder + 1
			,@vcDbColumnName=vcDbColumnName
			,@vcFieldName=vcFieldName
			,@vcAssociatedControlType=vcAssociatedControlType
			,@vcListItemType=vcListItemType
			,@numListID=numListID
			,@vcLookBackTableName=vcLookBackTableName                                               
			,@bitCustomField=bitCustomField
			,@numFieldId=numFieldId
			,@bitAllowSorting=bitAllowSorting
			,@bitAllowEdit=0
			,@ListRelID=ListRelID
		FROM 
			#tempForm 
		WHERE 
			tintOrder > @tintOrder-1 
		ORDER BY 
			tintOrder asc            
 
		IF @@rowcount=0 set @tintOrder=0 
	END
SET @strSql=''

Create table #tempColorScheme(vcOrigDbCOlumnName varchar(50),vcLookBackTableName varchar(50),vcFieldValue varchar(50),vcFieldValue1 varchar(50),vcColorScheme varchar(50),vcAssociatedControlType varchar(50))

insert into #tempColorScheme select DFM.vcOrigDbCOlumnName,DFM.vcLookBackTableName,DFCS.vcFieldValue,DFCS.vcFieldValue1,DFCS.vcColorScheme,DFFM.vcAssociatedControlType 
from DycFieldColorScheme DFCS join DycFormField_Mapping DFFM on DFCS.numFieldID=DFFM.numFieldID
join DycFieldMaster DFM on DFM.numModuleID=DFFM.numModuleID and DFM.numFieldID=DFFM.numFieldID
where DFCS.numDomainID=@numDomainID and DFFM.numFormID=43 AND DFCS.numFormID=43 and isnull(DFFM.bitAllowGridColor,0)=1

IF(SELECT COUNT(*) FROM #tempColorScheme)>0
BEGIN
   select top 1 @vcCSOrigDbCOlumnName=vcOrigDbCOlumnName,@vcCSLookBackTableName=vcLookBackTableName,@vcCSAssociatedControlType=vcAssociatedControlType from #tempColorScheme
END   
-------------------- 0 - TASK----------------------------

IF(LEN(@CustomSearchCriteria)>0)
BEGIN
SET @CustomSearchCriteria = ' AND '+@CustomSearchCriteria
END
IF(LEN(@ActivityRegularSearch)=0)
BEGIN
	SET @ActivityRegularSearch = '1=1'
END
DECLARE @StaticColumns AS NVARCHAR(MAX)=''

SET @StaticColumns = ' RecordCount,RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
dtDueDate,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating,
numContactID,numDivisionID,tintCRMType,Id,bitTask,Startdate,EndTime,itemDesc,
[Name],vcFirstName,vcLastName,numPhone,numPhoneExtension,[Phone],vcCompanyName,vcProfile,vcEmail,Task,Status,numRecOwner,numModifiedBy,numTerId,numAssignedBy,caseid,vcCasenumber,
casetimeId,caseExpId,type,itemid,bitFollowUpAnyTime,numNoOfEmployeesId,vcComPhone,numFollowUpStatus,dtLastFollowUp,vcLastSalesOrderDate,monDealAmount,vcPerformance'
SET @dynamicQuery = ' INSERT INTO #tempRecords(
 '+@StaticColumns +@vcCustomColumnNameOnly+'
) SELECT COUNT(*) OVER() RecordCount,* FROM (SELECT 
Comm.numCommId AS RecordId,
Comm.numCreatedBy AS numCreatedBy, 
ISNULL(Div.numTerId,0) AS numOrgTerId,
-2 AS numTaskId,
bitClosedFlag AS IsTaskClosed,
''<a onclick="openCommTask(''+CAST(Comm.numCommId AS VARCHAR)+'',0)" href="javascript:void(0)">''+dbo.GetListIemName(Comm.bitTask)+''</a>'' As TaskTypeName,
Comm.bitTask AS TaskType,
'''' AS OrigDescription,
comm.textDetails AS Description,
Comm.dtStartTime AS dtDueDate,
cast(Comm.dtStartTime as datetime) as DueDate,
''-'' As TotalProgress,
dbo.fn_GetContactName(numAssign) As numAssignedTo,
numAssign As numAssignToId,
listdetailsStatus.VcData As Priority, -- Priority column   
 listdetailsActivity.VcData As Activity ,
listdetailsStatus.NumlistItemID As PriorityId, -- Priority column   
 listdetailsActivity.NumlistItemID As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(Div.numDivisionID AS VARCHAR)+''">''+Comp.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating,
 Addc.numContactID,Addc.numDivisionID,Div.tintCRMType,Comm.numCommId AS Id,convert(varchar(15),bitTask) as bitTask,
  DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dtStartTime) as Startdate,                        
 DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dtEndTime) as EndTime ,                                                                        
 Comm.textDetails AS itemDesc,  
 AddC.vcFirstName + '' '' + AddC.vcLastName  as [Name], 
 AddC.vcFirstName,AddC.vcLastName,Addc.numPhone,AddC.numPhoneExtension,                               
 case when Addc.numPhone<>'''' then + AddC.numPhone +case when AddC.numPhoneExtension<>'''' then '' - '' + AddC.numPhoneExtension else '''' end  else '''' end as [Phone], 
  Comp.vcCompanyName As vcCompanyName , ISNULL((SELECT vcData FROM ListDetails WHERE numListID =21 AND numDomainID='+ CAST(@numDomainID AS VARCHAR) +' AND  numListItemID=Comp.vcProfile ) ,'''') vcProfile ,                                                        
 AddC.vcEmail As vcEmail ,    
 dbo.GetListIemName(Comm.bitTask)AS Task,   
 listdetailsStatus.VcData As Status,dbo.fn_GetContactName(Comm.numCreatedBy) AS numRecOwner,dbo.fn_GetContactName(Comm.numModifiedBy) AS numModifiedBy,  
  (select TOP 1 vcData from ListDetails where numListItemID=Div.numTerId) AS numTerId,dbo.fn_GetContactName(Comm.numAssignedBy) AS numAssignedBy,
  convert(varchar(50),comm.caseid) as caseid,(select top 1 vcCaseNumber from cases where cases.numcaseid= comm.caseid )as vcCasenumber,
  ISNULL(comm.casetimeId,0) casetimeId,ISNULL(comm.caseExpId,0) caseExpId,0 as type,'''' as itemid,ISNULL(bitFollowUpAnyTime,0) bitFollowUpAnyTime,  
  dbo.GetListIemName(Comp.numNoOfEmployeesId) AS numNoOfEmployeesId,Div.vcComPhone,
dbo.GetListIemName(Div.numFollowUpStatus) AS numFollowUpStatus,' + 
CONCAT('ISNULL(dbo.FormatedDateFromDate(Div.dtLastFollowUp,',@numDomainID,'),'''')') + ' AS dtLastFollowUp,
' + CONCAT('ISNULL(dbo.FormatedDateFromDate(TempLastOrder.bintCreatedDate,',@numDomainID,'),'''')') + ' AS vcLastSalesOrderDate,ISNULL(TempPerformance.monDealAmount,0) monDealAmount,
(CASE WHEN ISNULL(TempPerformance.monDealAmount,0) > 0 THEN CONCAT(FORMAT(TempPerformance.monDealAmount,''##,####.00''),'' ('',CAST(TempPerformance.fltProfitPer AS DECIMAL(18,2)),''%)'') ELSE '''' END) AS vcPerformance
  '+@vcCustomColumnName+'
 FROM  Communication Comm                              
 JOIN AdditionalContactsInformation AddC                                                  
 ON Comm.numContactId = AddC.numContactId                                  
 JOIN DivisionMaster Div                                                                         
 ON AddC.numDivisionId = Div.numDivisionID                                   
 JOIN CompanyInfo  Comp                                                                         
 ON Div.numCompanyID = Comp.numCompanyId  
 OUTER APPLY(' + CONCAT('SELECT MAX(bintCreatedDate) AS bintCreatedDate FROM OpportunityMaster WHERE numDomainID = ',@numDomainID,' AND numDivisionID=Div.numDivisionID AND tintOppType = 1 AND tintOppStatus = 1') + ' ) AS TempLastOrder
 OUTER APPLY(' + CONCAT('SELECT
																	SUM(monDealAmount) AS monDealAmount
																	,AVG(ProfitPer) AS fltProfitPer
																FROM
																(
																	SELECT
																		OM.numDivisionId
																		,monDealAmount
																		,AVG(((ISNULL(OI.monPrice,0) - (CASE D.numCost WHEN 3 THEN ISNULL(V.monCost,0) * dbo.fn_UOMConversion(I.numBaseUnit,I.numItemCode,I.numDomainID,I.numPurchaseUnit) WHEN 2 THEN ISNULL(OI.numCost,0) ELSE ISNULL(I.monAverageCost,0) END)) / ISNULL(NULLIF(OI.monPrice,0),1)) * 100) ProfitPer
																	FROM
																		OpportunityMaster OM
																	INNER JOIN
																		Domain D
																	ON
																		OM.numDomainID = D.numDomainID
																	INNER JOIN
																		OpportunityItems OI
																	ON
																		OM.numOppId = OI.numOppId
																	INNER JOIN
																		Item I
																	ON
																		OI.numItemCode = I.numItemCode
																	LEFT JOIN
																		Vendor V
																	ON
																		V.numVendorID = I.numVendorID
																		AND V.numItemCode = I.numItemCode
																	WHERE
																		OM.numDomainId = ',@numDomainID,'
																		AND OM.numDivisionId = Div.numDivisionID
																		AND ISNULL(I.bitContainer,0) = 0
																		AND tintOppType = 1
																		AND tintOppStatus=1
																	GROUP BY
																		OM.numDivisionId
																		,OM.numOppId
																		,monDealAmount
																) T1
																GROUP BY
																	T1.numDivisionId )') + ' AS TempPerformance
 LEFT JOIN OpportunityMaster Opp ON Opp.numDivisionId = Div.numDivisionID AND Opp.numContactId = AddC.numContactId  AND Opp.numOppID = Comm.numOppID 
 LEFT JOIN Sales_process_List_Master splm ON splm.Slp_Id = Opp.numBusinessProcessID 
 LEFT JOIN ProjectProgress PP On PP.numOppId = Opp.numOppId
  Left Join listdetails listdetailsStatus                                                 
 On Comm.numStatus = listdetailsStatus.NumlistItemID
  Left Join listdetails listdetailsActivity                                                  
 On Comm.numActivity = listdetailsActivity.NumlistItemID
 LEFT JOIN ListDetails AS listCompanyRating
 ON Comp.numCompanyRating=listCompanyRating.NumlistItemID
WHERE '+@ActivityRegularSearch+' AND
	Comm.numDomainID = ' + Cast(@numDomainID as varchar(10))  + ' AND 1 = (CASE ' + CAST( @tintActionItemsViewRights AS VARCHAR) + 
' WHEN 3 THEN 1 
WHEN 2 THEN (CASE WHEN Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+ CONVERT(VARCHAR(15),@numUserCntID) + ' ) THEN 1 ELSE 0 END)
WHEN 1 THEN (CASE WHEN (Comm.numCreatedBy= ' + CONVERT(VARCHAR(15),@numUserCntID) + ' or Comm.numAssign= '+ CONVERT(VARCHAR(15),@numUserCntID) + ') THEN 1 ELSE 0 END) 
ELSE 0 END)  AND (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',dtStartTime) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',dtStartTime) <= '''+Cast(@endDate as varchar(30))+''')
'+@CustomSearchCriteria+'
 )   Q WHERE '+@OtherRegularSearch+'
  '+@IndivRecordPaging+'
'


EXEC (@dynamicQuery)
PRINT 'HI'
PRINT CAST(@dynamicQuery AS TEXT)
PRINT LEN(@ActivityRegularSearch)
-------------------- 0 - Email Communication----------------------------
--''<a onclick="openCommTask(''+CAST(ac.activityid AS VARCHAR)+'',1)" href="javascript:void(0)">Calendar</a>'' As TaskTypeName,
IF(LEN(@ActivityRegularSearch)=0 OR RTRIM(LTRIM(@ActivityRegularSearch))='1=1')
BEGIN
DECLARE @FormattedItems As VARCHAR(MAX)
SET @FormattedItems ='
RecordCount,RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
dtDueDate,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating'
SET @dynamicQuery =  ' INSERT INTO #tempRecords(
'+@FormattedItems+'
) SELECT  COUNT(*) OVER() RecordCount,* FROM (  SELECT 
ac.activityid AS RecordId,
0 AS numCreatedBy,
ISNULL(Div.numTerId,0) AS numOrgTerId,
-2 AS numTaskId,
case when alldayevent = 1 then ''1'' else ''0'' end AS IsTaskClosed,
''<a href="javascript:void(0)">Calendar</a>'' As TaskTypeName,
12 AS TaskType,
CASE WHEN ISNULL(ac.Comments,'''') ='''' THEN ac.ActivityDescription ELSE ISNULL(ac.Comments,'''') END AS OrigDescription,
(SELECT DISTINCT  
replace(replace((SELECT 
CASE(ISNULL(bitpartycalendarTitle,1)) WHEN 1 THEN ''<b><font color=red>Title</font></b>'' + CASE(ISNULL(ac.Subject,'''')) WHEN '''' THEN '''' else ac.Subject END + ''<br>'' else ''''  END  
+ CASE(ISNULL(bitpartycalendarLocation,1)) WHEN 1 THEN ''<b><font color=green>Location</font></b>'' + CASE(ISNULL(ac.location,'''')) WHEN '''' THEN '''' else ac.location END + ''<br>'' else ''''  END  
+ CASE ISNULL(bitpartycalendarDescription,1) 
	WHEN 1 
	THEN CONCAT(''<b><font color=#3c8dbc>Description</font></b>'',
				(CASE 
					WHEN LEN(CASE WHEN ISNULL(ac.Comments,'''') ='''' THEN ac.ActivityDescription ELSE ISNULL(ac.Comments,'''') END) > 100 
					THEN CONCAT(CAST((CASE WHEN ISNULL(ac.Comments,'''') ='''' THEN ac.ActivityDescription ELSE ISNULL(ac.Comments,'''') END) AS VARCHAR(100)),CONCAT(''...'',''<a href="#" role="button" title="Description" data-toggle="popover" data-content="'',(CASE WHEN ISNULL(ac.Comments,'''') ='''' THEN ac.ActivityDescription ELSE ISNULL(ac.Comments,'''') END) ,''"> more</a>'')) 
					ELSE (CASE WHEN ISNULL(ac.Comments,'''') ='''' THEN ac.ActivityDescription ELSE ISNULL(ac.Comments,'''') END) 
				END)) 
	ELSE ''''  
	END  
FROM Domain where numDomainId='+ CAST(@numDomainID AS VARCHAR) +' 
FOR XML PATH('''')
),''&lt;'', ''<''), ''&gt;'', ''>''))   AS Description,
DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dateadd(second,duration,startdatetimeutc)) AS dtDueDate,
DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dateadd(second,duration,startdatetimeutc)) as DueDate,
''0'' As TotalProgress,
''-'' As numAssignedTo,
''0'' As numAssignToId,
'''' As Priority, -- Priority column   
'''' As Activity ,
0 As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(Div.numDivisionID AS VARCHAR)+''">''+Comp.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating
 From activity ac join             
activityresource ar on  ac.activityid=ar.activityid             
join resource res on ar.resourceid=res.resourceid                         
--join usermaster UM on um.numUserId = res.numUserCntId                
join AdditionalContactsInformation ACI on ACI.numContactId = res.numUserCntId                
 JOIN DivisionMaster Div                                                          
 ON ACI.numDivisionId = Div.numDivisionID                                   
 JOIN CompanyInfo  Comp                             
 ON Div.numCompanyID = Comp.numCompanyId
  LEFT JOIN ListDetails AS listCompanyRating
 ON Comp.numCompanyRating=listCompanyRating.NumlistItemID
 OUTER APPLY(' + CONCAT('SELECT MAX(bintCreatedDate) AS bintCreatedDate FROM OpportunityMaster WHERE numDomainID = ',@numDomainID,' AND numDivisionID=Div.numDivisionID AND tintOppType = 1 AND tintOppStatus = 1') + ' ) AS TempLastOrder                 
 OUTER APPLY(' + CONCAT('SELECT
																	SUM(monDealAmount) AS monDealAmount
																	,AVG(ProfitPer) AS fltProfitPer
																FROM
																(
																	SELECT
																		OM.numDivisionId
																		,monDealAmount
																		,AVG(((ISNULL(OI.monPrice,0) - (CASE D.numCost WHEN 3 THEN ISNULL(V.monCost,0) * dbo.fn_UOMConversion(I.numBaseUnit,I.numItemCode,I.numDomainID,I.numPurchaseUnit) WHEN 2 THEN ISNULL(OI.numCost,0) ELSE ISNULL(I.monAverageCost,0) END)) / ISNULL(NULLIF(OI.monPrice,0),1)) * 100) ProfitPer
																	FROM
																		OpportunityMaster OM
																	INNER JOIN
																		Domain D
																	ON
																		OM.numDomainID = D.numDomainID
																	INNER JOIN
																		OpportunityItems OI
																	ON
																		OM.numOppId = OI.numOppId
																	INNER JOIN
																		Item I
																	ON
																		OI.numItemCode = I.numItemCode
																	LEFT JOIN
																		Vendor V
																	ON
																		V.numVendorID = I.numVendorID
																		AND V.numItemCode = I.numItemCode
																	WHERE
																		OM.numDomainId = ',@numDomainID,'
																		AND OM.numDivisionId = Div.numDivisionID
																		AND ISNULL(I.bitContainer,0) = 0
																		AND tintOppType = 1
																		AND tintOppStatus=1
																		
																	GROUP BY
																		OM.numDivisionId
																		,OM.numOppId
																		,monDealAmount
																) T1
																GROUP BY
																	T1.numDivisionId )') + ' AS TempPerformance

 where  res.numUserCntId = ' + Cast(@numUserCntID as varchar(10)) +'             
and ac.OriginalStartDateTimeUtc IS NULL            
and res.numDomainID = ' + Cast(@numDomainID as varchar(10)) +'  and 
(DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', startdatetimeutc) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', startdatetimeutc) <= '''+Cast(@endDate as varchar(30))+''')
--(startdatetimeutc between dateadd(month,-1,getutcdate()) and dateadd(month,1,getutcdate())) 
and [status] <> -1                                                     
and ac.activityid not in 
(select distinct(numActivityId) from communication Comm where Comm.numDomainID = ' + Cast(@numDomainID as varchar(10)) +'  
AND 1=(Case ' + Cast(@bitFilterRecord as varchar(10)) +' when 1 then Case when Comm.numAssign =' + Cast(@numUserCntID as varchar(10)) +'  then 1 else 0 end 
		else  Case when (Comm.numAssign =' + Cast(@numUserCntID as varchar(10)) +'  or Comm.numCreatedBy=' + Cast(@numUserCntID as varchar(10)) +' ) then 1 else 0 end end)   
		AND 1 = (CASE ' + CAST( @tintActionItemsViewRights AS VARCHAR) + 
' WHEN 3 THEN 1 
WHEN 2 THEN (CASE WHEN Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+ CONVERT(VARCHAR(15),@numUserCntID) + ' ) THEN 1 ELSE 0 END)
WHEN 1 THEN (CASE WHEN (Comm.numCreatedBy= ' + CONVERT(VARCHAR(15),@numUserCntID) + ' or Comm.numAssign= '+ CONVERT(VARCHAR(15),@numUserCntID) + ') THEN 1 ELSE 0 END) 
ELSE 0 END)                                                                   

)) Q WHERE '+@OtherRegularSearch+'  '+@IndivRecordPaging+'
'

EXEC (@dynamicQuery)
-------------------- 1 - CASE----------------------------

SET @dynamicQuery =  ' INSERT INTO #tempRecords(
RecordCount,
RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
dtDueDate,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating
) SELECT  COUNT(*) OVER() RecordCount,* FROM (  SELECT 
C.numCaseId AS RecordId,
C.numCreatedBy AS numCreatedBy,
ISNULL(Div.numTerId,0) AS numOrgTerId,
0 AS numTaskId,
CASE WHEN C.numStatus=1008 THEN 1 ELSE 0 END AS IsTaskClosed,
''<a onclick="openCaseDetails(''+CAST(C.numCaseId AS VARCHAR)+'')" href="javascript:void(0)">Case</a>'' As TaskTypeName,
1 AS TaskType,
'''' AS OrigDescription,
C.textSubject AS Description,
cast(C.intTargetResolveDate as datetime) AS dtDueDate,
cast(C.intTargetResolveDate as datetime) as DueDate,
''0'' As TotalProgress,
dbo.fn_GetContactName(C.numAssignedTo) As numAssignedTo,
C.numAssignedTo As numAssignToId,
listdetailsStatus.VcData As Priority, -- Priority column   
'''' As Activity ,
listdetailsStatus.NumlistItemID As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(Div.numDivisionID AS VARCHAR)+''">''+Com.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating

 FROM 
	Cases  C                          
INNER JOIN  
	AdditionalContactsInformation  AddC                          
ON 
	C.numContactId = AddC.numContactId                           
INNER JOIN 
	DivisionMaster Div                           
ON 
	AddC.numDivisionId = Div.numDivisionID AND  
	AddC.numDivisionId = Div.numDivisionID                           
INNER JOIN  
	CompanyInfo  Com                          
ON 
	Div.numCompanyID = Com.numCompanyId    
 LEFT JOIN ListDetails AS listCompanyRating
 ON Com.numCompanyRating=listCompanyRating.NumlistItemID
  Left Join listdetails listdetailsStatus                                                 
 On C.numPriority = listdetailsStatus.NumlistItemID
WHERE
	C.numDomainID = ' + Cast(@numDomainID as varchar(10))  + ' AND C.numStatus <> 136  AND 1 = (CASE ' + CAST( @tintActionItemsViewRights AS VARCHAR) + 
' WHEN 3 THEN 1 
WHEN 2 THEN (CASE WHEN Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+ CONVERT(VARCHAR(15),@numUserCntID) + ' ) THEN 1 ELSE 0 END)
WHEN 1 THEN (CASE WHEN (C.numCreatedBy= ' + CONVERT(VARCHAR(15),@numUserCntID) + ' or C.numAssignedTo= '+ CONVERT(VARCHAR(15),@numUserCntID) + ') THEN 1 ELSE 0 END) 
ELSE 0 END)  AND (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',intTargetResolveDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',intTargetResolveDate) <= '''+Cast(@endDate as varchar(30))+''') 
) Q WHERE '+@OtherRegularSearch+' '+@IndivRecordPaging+'
'
EXEC (@dynamicQuery)

-------------------- 2 - Project Task ----------------------------

SET @dynamicQuery =  ' INSERT INTO #tempRecords(
RecordCount,
RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
dtDueDate,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating
)  SELECT  COUNT(*) OVER() RecordCount,* FROM (SELECT
T.numProjectId AS RecordId,
T.numCreatedBy AS numCreatedBy,
ISNULL(Div.numTerId,0) AS numOrgTerId,
T.numTaskId AS numTaskId,
T.bitTaskClosed AS IsTaskClosed,
''<a onclick="openProjectTask(''+CAST(T.numProjectId AS VARCHAR)+'')" href="javascript:void(0)">Project Task</a>'' As TaskTypeName,
2 AS TaskType,
'''' AS OrigDescription,
T.vcTaskName AS Description,
CASE WHEN S.bitIsDueDaysUsed=1 THEN CONVERT(VARCHAR(10),DATEADD(DAY,S.intDueDays,CAST(S.dtStartDate AS DATE)), 101)
		ELSE CONVERT(VARCHAR(10),DATEADD(DAY,(SELECT TOP 1 CAST(Items AS DECIMAL(18,2)) FROM Split(dbo.fn_calculateTotalHoursMinutesByStageDetails(T.numStageDetailsId,T.numOppId,T.numProjectId),''_'')),CAST(S.dtStartDate AS DATE)), 101) END AS dtDueDate,
CASE WHEN S.bitIsDueDaysUsed=1 THEN CONVERT(VARCHAR(10),DATEADD(DAY,S.intDueDays,CAST(S.dtStartDate AS DATE)), 101)
		ELSE CONVERT(VARCHAR(10),DATEADD(DAY,(SELECT TOP 1 CAST(Items AS DECIMAL(18,2)) FROM Split(dbo.fn_calculateTotalHoursMinutesByStageDetails(T.numStageDetailsId,T.numOppId,T.numProjectId),''_'')),CAST(S.dtStartDate AS DATE)), 101) END as DueDate,
ISNULL((SELECT TOP 1 intTotalProgress FROM ProjectProgress WHERE numProId = T.numProjectId),0) As TotalProgress,
dbo.fn_GetContactName(T.numAssignTo) As numAssignedTo,
T.numAssignTo As numAssignToId,
'''' As Priority, -- Priority column   
'''' As Activity ,
0 As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(Div.numDivisionID AS VARCHAR)+''">''+Com.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating

 FROM 
	StagePercentageDetailsTask AS T
	LEFT JOIN StagePercentageDetails AS S
	ON S.numStageDetailsId=T.numStageDetailsId AND S.numProjectID=T.numProjectID
	LEFT JOIN ProjectsMaster AS OP 
	ON T.numProjectID=OP.numProId
	LEFT JOIN StagePercentageMaster AS SM
	ON S.numStagePercentageId=SM.numStagePercentageId                        
INNER JOIN 
	DivisionMaster Div                           
ON 
	OP.numDivisionId = Div.numDivisionID                       
INNER JOIN  
	CompanyInfo  Com                          
ON 
	Div.numCompanyID = Com.numCompanyId    
LEFT JOIN ListDetails AS listCompanyRating
 ON Com.numCompanyRating=listCompanyRating.NumlistItemID
WHERE
	T.numDomainID = ' + Cast(@numDomainID as varchar(10))  + ' AND T.numProjectId>0 AND s.numProjectId>0 AND T.bitSavedTask=1 AND T.bitTaskClosed=0 AND 1 = (CASE ' + CAST( @tintActionItemsViewRights AS VARCHAR) + 
' WHEN 3 THEN 1 
WHEN 2 THEN (CASE WHEN Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+ CONVERT(VARCHAR(15),@numUserCntID) + ' ) THEN 1 ELSE 0 END)
WHEN 1 THEN (CASE WHEN (T.numCreatedBy= ' + CONVERT(VARCHAR(15),@numUserCntID) + ' or T.numAssignTo= '+ CONVERT(VARCHAR(15),@numUserCntID) + ') THEN 1 ELSE 0 END) 
ELSE 0 END) ) Q WHERE '+@OtherRegularSearch+' AND (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) <= '''+Cast(@endDate as varchar(30))+''') 
'+@IndivRecordPaging+'
'

EXEC (@dynamicQuery)

------------------ 3 - Opportunity  Task ----------------------------

SET @dynamicQuery =  ' INSERT INTO #tempRecords(
RecordCount,
RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
dtDueDate,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating
) SELECT  COUNT(*) OVER() RecordCount,* FROM ( SELECT 
T.numOppId AS RecordId,
T.numCreatedBy AS numCreatedBy,
ISNULL(Div.numTerId,0) AS numOrgTerId,
T.numTaskId AS numTaskId,
T.bitTaskClosed AS IsTaskClosed,
''<a onclick="openTask(''+CAST(T.numOppId AS VARCHAR)+'',''+CAST(S.numStagePercentageId AS VARCHAR)+'',''+CAST(S.tinProgressPercentage AS VARCHAR)+'',''+CAST(S.tintConfiguration AS VARCHAR)+'')" href="javascript:void(0)">Opportunity Task</a>'' As TaskTypeName,
3 AS TaskType,
'''' AS OrigDescription,
T.vcTaskName AS Description,
CASE WHEN S.bitIsDueDaysUsed=1 THEN CONVERT(VARCHAR(10),DATEADD(DAY,S.intDueDays,CAST(S.dtStartDate AS DATE)), 101)
		ELSE CONVERT(VARCHAR(10),DATEADD(DAY,(SELECT TOP 1 CAST(Items AS DECIMAL(18,2)) FROM Split(dbo.fn_calculateTotalHoursMinutesByStageDetails(T.numStageDetailsId,T.numOppId,T.numProjectId),''_'')),CAST(S.dtStartDate AS DATE)), 101) END AS dtDueDate,
CASE WHEN S.bitIsDueDaysUsed=1 THEN CONVERT(VARCHAR(10),DATEADD(DAY,S.intDueDays,CAST(S.dtStartDate AS DATE)), 101)
		ELSE CONVERT(VARCHAR(10),DATEADD(DAY,(SELECT TOP 1 CAST(Items AS DECIMAL(18,2)) FROM Split(dbo.fn_calculateTotalHoursMinutesByStageDetails(T.numStageDetailsId,T.numOppId,T.numProjectId),''_'')),CAST(S.dtStartDate AS DATE)), 101) END as DueDate,
ISNULL((SELECT TOP 1 intTotalProgress FROM ProjectProgress WHERE numOppId = T.numOppId),0) As TotalProgress,
dbo.fn_GetContactName(T.numAssignTo) As numAssignedTo,
T.numAssignTo As numAssignToId,
'''' As Priority, -- Priority column   
'''' As Activity ,
0 As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(Div.numDivisionID AS VARCHAR)+''">''+Com.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating

 FROM 
	StagePercentageDetailsTask AS T
	LEFT JOIN StagePercentageDetails AS S
	ON S.numStageDetailsId=T.numStageDetailsId AND S.numOppId=T.numOppId
	LEFT JOIN OpportunityMaster AS OP 
	ON T.numOppId=OP.numOppId
	LEFT JOIN StagePercentageMaster AS SM
	ON S.numStagePercentageId=SM.numStagePercentageId                        
INNER JOIN 
	DivisionMaster Div                           
ON 
	OP.numDivisionId = Div.numDivisionID                       
INNER JOIN  
	CompanyInfo  Com                          
ON 
	Div.numCompanyID = Com.numCompanyId    
LEFT JOIN ListDetails AS listCompanyRating
 ON Com.numCompanyRating=listCompanyRating.NumlistItemID
WHERE
	T.numDomainID = ' + Cast(@numDomainID as varchar(10))  + ' AND OP.tintOppType=1 AND OP.tintOppStatus=0 AND T.numOppId>0 AND s.numOppId>0 AND T.bitSavedTask=1 AND T.bitTaskClosed=0 AND 1 = (CASE ' + CAST( @tintActionItemsViewRights AS VARCHAR) + 
' WHEN 3 THEN 1 
WHEN 2 THEN (CASE WHEN Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+ CONVERT(VARCHAR(15),@numUserCntID) + ' ) THEN 1 ELSE 0 END)
WHEN 1 THEN (CASE WHEN (T.numCreatedBy= ' + CONVERT(VARCHAR(15),@numUserCntID) + ' or T.numAssignTo= '+ CONVERT(VARCHAR(15),@numUserCntID) + ') THEN 1 ELSE 0 END) 
ELSE 0 END) ) Q WHERE '+@OtherRegularSearch+' AND (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) <= '''+Cast(@endDate as varchar(30))+''')
'+@IndivRecordPaging+' 
 '
 
 EXEC (@dynamicQuery)

------------------ 4 -  Sales Order Task ----------------------------

SET @dynamicQuery =  ' INSERT INTO #tempRecords(
RecordCount,
RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
dtDueDate,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating
)SELECT  COUNT(*) OVER() RecordCount,* FROM (  SELECT 
T.numOppId AS RecordId,
T.numCreatedBy AS numCreatedBy,
ISNULL(Div.numTerId,0) AS numOrgTerId,
T.numTaskId AS numTaskId,
T.bitTaskClosed AS IsTaskClosed,
''<a onclick="openTask(''+CAST(T.numOppId AS VARCHAR)+'',''+CAST(S.numStagePercentageId AS VARCHAR)+'',''+CAST(S.tinProgressPercentage AS VARCHAR)+'',''+CAST(S.tintConfiguration AS VARCHAR)+'')" href="javascript:void(0)">Sales Order Task</a>'' As TaskTypeName,
4 AS TaskType,
'''' AS OrigDescription,
T.vcTaskName AS Description,
CASE WHEN S.bitIsDueDaysUsed=1 THEN CONVERT(VARCHAR(10),DATEADD(DAY,S.intDueDays,CAST(S.dtStartDate AS DATE)), 101)
		ELSE CONVERT(VARCHAR(10),DATEADD(DAY,(SELECT TOP 1 CAST(Items AS DECIMAL(18,2)) FROM Split(dbo.fn_calculateTotalHoursMinutesByStageDetails(T.numStageDetailsId,T.numOppId,T.numProjectId),''_'')),CAST(S.dtStartDate AS DATE)), 101) END AS dtDueDate,
CASE WHEN S.bitIsDueDaysUsed=1 THEN CONVERT(VARCHAR(10),DATEADD(DAY,S.intDueDays,CAST(S.dtStartDate AS DATE)), 101)
		ELSE CONVERT(VARCHAR(10),DATEADD(DAY,(SELECT TOP 1 CAST(Items AS DECIMAL(18,2)) FROM Split(dbo.fn_calculateTotalHoursMinutesByStageDetails(T.numStageDetailsId,T.numOppId,T.numProjectId),''_'')),CAST(S.dtStartDate AS DATE)), 101) END as DueDate,
ISNULL((SELECT TOP 1 intTotalProgress FROM ProjectProgress WHERE numOppId = T.numOppId),0) As TotalProgress,
dbo.fn_GetContactName(T.numAssignTo) As numAssignedTo,
T.numAssignTo As numAssignToId,
'''' As Priority, -- Priority column   
'''' As Activity ,
0 As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(Div.numDivisionID AS VARCHAR)+''">''+Com.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating

 FROM 
	StagePercentageDetailsTask AS T
	LEFT JOIN StagePercentageDetails AS S
	ON S.numStageDetailsId=T.numStageDetailsId AND S.numOppId=T.numOppId
	LEFT JOIN OpportunityMaster AS OP 
	ON T.numOppId=OP.numOppId
	LEFT JOIN StagePercentageMaster AS SM
	ON S.numStagePercentageId=SM.numStagePercentageId                        
INNER JOIN 
	DivisionMaster Div                           
ON 
	OP.numDivisionId = Div.numDivisionID                       
INNER JOIN  
	CompanyInfo  Com                          
ON 
	Div.numCompanyID = Com.numCompanyId    
LEFT JOIN ListDetails AS listCompanyRating
 ON Com.numCompanyRating=listCompanyRating.NumlistItemID
WHERE
	T.numDomainID = ' + Cast(@numDomainID as varchar(10))  + ' AND OP.tintOppType=1 AND OP.tintOppStatus=1 AND T.numOppId>0 AND s.numOppId>0 AND T.bitSavedTask=1 AND T.bitTaskClosed=0 AND 1 = (CASE ' + CAST( @tintActionItemsViewRights AS VARCHAR) + 
' WHEN 3 THEN 1 
WHEN 2 THEN (CASE WHEN Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+ CONVERT(VARCHAR(15),@numUserCntID) + ' ) THEN 1 ELSE 0 END)
WHEN 1 THEN (CASE WHEN (T.numCreatedBy= ' + CONVERT(VARCHAR(15),@numUserCntID) + ' or T.numAssignTo= '+ CONVERT(VARCHAR(15),@numUserCntID) + ') THEN 1 ELSE 0 END) 
ELSE 0 END) ) Q WHERE '+@OtherRegularSearch+' AND (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) <= '''+Cast(@endDate as varchar(30))+''')
'+@IndivRecordPaging+'
 '
 EXEC (@dynamicQuery)
  
------------------ 5 -  Work Order Task ----------------------------

SET @dynamicQuery =  ' INSERT INTO #tempRecords(
RecordCount,
RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
dtDueDate,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating
)SELECT  COUNT(*) OVER() RecordCount,* FROM ( SELECT 
WorkOrder.numWOID AS RecordId,
WorkOrder.numCreatedBy AS numCreatedBy,
ISNULL(Div.numTerId,0) AS numOrgTerId,
WorkOrder.numWOID AS numTaskId,
(CASE WHEN WorkOrder.numWOStatus = 23184 THEN 1 ELSE 0 END) AS IsTaskClosed,
CONCAT(''<a href="../items/frmWorkOrder.aspx?WOID='',WorkOrder.numWOID,''" target="_blank">Work Order Management</a>'') As TaskTypeName,
5 AS TaskType,
'''' AS OrigDescription,
CONCAT(''<b>'',Item.vcItemName,''</b>'','' ('',WorkOrder.numQtyItemsReq,'')'','' <b>Work Order Status :'', ''</b>'',''<label class="lblWorkOrderStatus" id="'',WorkOrder.numWOID,''"><i class="fa fa-refresh fa-spin"></i></lable>'') AS Description,
DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',WorkOrder.dtmEndDate) AS dtDueDate,
DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',WorkOrder.dtmEndDate) as DueDate,
dbo.GetTotalProgress(WorkOrder.numDomainID,WorkOrder.numWOID,1,1,'''',0) As TotalProgress,
dbo.fn_GetContactName(WorkOrder.numAssignedTo) As numAssignedTo,
WorkOrder.numAssignedTo As numAssignToId,
'''' As Priority, -- Priority column   
'''' As Activity ,
0 As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(Div.numDivisionID AS VARCHAR)+''">''+Com.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating

 FROM 
	WorkOrder
INNER JOIN 
	Item
ON
	WorkOrder.numItemCode = Item.numItemCode
LEFT JOIN 
	OpportunityMaster
ON 
	WorkOrder.numOppId=OpportunityMaster.numOppId
INNER JOIN 
	DivisionMaster Div                           
ON 
	Div.numDivisionId = (CASE WHEN OpportunityMaster.numOppID IS NOT NULL THEN OpportunityMaster.numDivisionID ELSE ' + CAST(@numDomainDivisionID AS VARCHAR) + ' END)                        
INNER JOIN  
	CompanyInfo  Com                          
ON 
	Div.numCompanyID = Com.numCompanyId    
LEFT JOIN 
	ListDetails AS listCompanyRating
ON 
	Com.numCompanyRating=listCompanyRating.NumlistItemID
WHERE
	WorkOrder.numDomainID = ' + Cast(@numDomainID as varchar(10))  + ' 
	AND WorkOrder.numWOStatus <> 23184
	AND WorkOrder.numAssignedTo= '+ CONVERT(VARCHAR(15),@numUserCntID) + '
	AND ((DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',WorkOrder.dtmStartDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',WorkOrder.dtmStartDate) <= '''+Cast(@endDate as varchar(30))+''') OR (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',WorkOrder.dtmEndDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',WorkOrder.dtmEndDate) <= '''+Cast(@endDate as varchar(30))+'''))
) Q WHERE '+@OtherRegularSearch+' '+@IndivRecordPaging+'
 '
 EXEC (@dynamicQuery)
------------------ 6 -  Work Center Task ----------------------------

SET @dynamicQuery =  ' INSERT INTO #tempRecords(
RecordCount,
RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
dtDueDate,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating
) SELECT  COUNT(*) OVER() RecordCount,* FROM ( SELECT 
T.numTaskId AS RecordId,
T.numCreatedBy AS numCreatedBy,
ISNULL(Div.numTerId,0) AS numOrgTerId,
T.numTaskId AS numTaskId,
(CASE WHEN EXISTS (SELECT * FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=T.numTaskID AND SPDTTL.tintAction=4) THEN 1 ELSE 0 END) AS IsTaskClosed,
CONCAT(''<a href="../items/frmWorkOrder.aspx?WOID='',W.numWOID,''" target="_blank">Work Center Task</a>'') As TaskTypeName,
6 AS TaskType,
'''' AS OrigDescription,
T.vcTaskName AS Description,
DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',W.dtmEndDate) AS dtDueDate,
DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',W.dtmEndDate) as DueDate,
(CASE 
	WHEN EXISTS (SELECT * FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=T.numTaskID AND SPDTTL.tintAction=4) THEN CAST(100 AS INT)
	ELSE CAST(((ISNULL((SELECT SUM(numProcessedQty) FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=T.numTaskID),0) / W.numQtyItemsReq) * 100) AS INT)
END) As TotalProgress,
dbo.fn_GetContactName(T.numAssignTo) As numAssignedTo,
T.numAssignTo As numAssignToId,
'''' As Priority, -- Priority column   
'''' As Activity ,
0 As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(Div.numDivisionID AS VARCHAR)+''">''+Com.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating

 FROM 
	StagePercentageDetailsTask AS T
	INNER JOIN WorkOrder AS W 
	ON T.numWorkOrderId=W.numWOId     
LEFT JOIN OpportunityMaster AS OP 
	ON W.numOppId=OP.numOppId
INNER JOIN 
	DivisionMaster Div                           
ON 
	Div.numDivisionId = (CASE WHEN OP.numOppID IS NOT NULL THEN OP.numDivisionID ELSE ' + CAST(@numDomainDivisionID AS VARCHAR) + ' END)                         
INNER JOIN  
	CompanyInfo  Com                          
ON 
	Div.numCompanyID = Com.numCompanyId    
LEFT JOIN ListDetails AS listCompanyRating
 ON Com.numCompanyRating=listCompanyRating.NumlistItemID
WHERE
	W.numDomainID = ' + Cast(@numDomainID as varchar(10))  + '
	AND W.numWOStatus <> 23184 
	AND NOT EXISTS (SELECT * FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=T.numTaskID AND SPDTTL.tintAction=4) 
	AND T.numAssignTo = ' + CAST(@numUserCntID AS VARCHAR) + '
	AND ((DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',W.dtmStartDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',W.dtmStartDate) <= '''+Cast(@endDate as varchar(30))+''') OR (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',W.dtmEndDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',W.dtmEndDate) <= '''+Cast(@endDate as varchar(30))+''')) 
) Q WHERE '+@OtherRegularSearch+' '+@IndivRecordPaging+'
'
EXEC (@dynamicQuery)

------------------ 7 -  Requisition Approval ----------------------------

IF(@bitREQPOApproval=1 AND @numUserCntID IN(SELECT * FROM Split(@vchREQPOApprovalEmp,',') WHERE Items<>''))
BEGIN
SET @dynamicQuery =  ' INSERT INTO #tempRecords(
RecordCount,
RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
dtDueDate,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating
) SELECT  COUNT(*) OVER() RecordCount,* FROM( SELECT 
OP.numOppId AS RecordId,
OP.numCreatedBy AS numCreatedBy,
ISNULL(Div.numTerId,0) AS numOrgTerId,
0 AS numTaskId,
CASE WHEN ISNULL(OP.intReqPOApproved,0)=0 THEN 0 ELSE 1 END AS IsTaskClosed,
''<a  onclick="openSalesOrder(''+CAST(OP.numOppId AS VARCHAR)+'')" href="javascript:void(0)">Requisition Approval</a>'' As TaskTypeName,
7 AS TaskType,
'''' AS OrigDescription,
''<b>For :</b> <a class="underLineHyperLink" onclick="openSalesOrder(''+CAST(OP.numOppId AS VARCHAR)+'')" href="javascript:void(0)">''+OP.vcPOppName+''</a> &nbsp;<i onclick="ApproveRejectPORequistion(''+CAST(OP.numOppId AS VARCHAR)+'',1)"  class="fa fa-thumbs-up cursor"></i>  &nbsp;<i onclick="ApproveRejectPORequistion(''+CAST(OP.numOppId AS VARCHAR)+'',2)"  class="fa fa-thumbs-down cursor"></i>'' AS Description,
OP.intPEstimatedCloseDate AS dtDueDate,
OP.intPEstimatedCloseDate as DueDate,
0 As TotalProgress,
dbo.fn_GetContactName('+CAST(@numUserCntID AS VARCHAR(200))+') As numAssignedTo,
'+CAST(@numUserCntID AS VARCHAR(200))+' As numAssignToId,
'''' As Priority, -- Priority column   
'''' As Activity ,
0 As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(Div.numDivisionID AS VARCHAR)+''">''+Com.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating

 FROM  
 OpportunityMaster AS OP 
INNER JOIN 
	DivisionMaster Div                           
ON 
	OP.numDivisionId = Div.numDivisionID                       
INNER JOIN  
	CompanyInfo  Com                          
ON 
	Div.numCompanyID = Com.numCompanyId    
LEFT JOIN ListDetails AS listCompanyRating
 ON Com.numCompanyRating=listCompanyRating.NumlistItemID
WHERE
	OP.numDomainID = ' + Cast(@numDomainID as varchar(10))  + ' AND OP.tintOppType = 2 AND OP.tintOppStatus=0 
	AND 1 = (CASE WHEN '+CAST(@decReqPOMinValue AS VARCHAR(100))+'>0 AND Op.monPAmount>'+CAST(@decReqPOMinValue AS VARCHAR(100))+' THEN 1  WHEN '+CAST(@decReqPOMinValue AS VARCHAR(100))+'=0 THEN 1  ELSE 0 END ) ) AS Q
WHERE '+@OtherRegularSearch+' AND (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) <= '''+Cast(@endDate as varchar(30))+''') 
'+@IndivRecordPaging+'
'
EXEC (@dynamicQuery)

END
------------------ 8 -  PO Approval ----------------------------
IF(@bitREQPOApproval=1 AND @numUserCntID IN(SELECT * FROM Split(@vchREQPOApprovalEmp,',') WHERE Items<>''))
BEGIN
SET @dynamicQuery =  ' INSERT INTO #tempRecords(
RecordCount,
RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
dtDueDate,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating
) SELECT  COUNT(*) OVER() RecordCount,* FROM ( SELECT
OP.numOppId AS RecordId,
OP.numCreatedBy AS numCreatedBy,
ISNULL(Div.numTerId,0) AS numOrgTerId,
0 AS numTaskId,
CASE WHEN ISNULL(OP.intReqPOApproved,0)=0 THEN 0 ELSE 1 END AS IsTaskClosed,
''<a onclick="openSalesOrder(''+CAST(OP.numOppId AS VARCHAR)+'')" href="javascript:void(0)">PO Approval</a>'' As TaskTypeName,
8 AS TaskType,
'''' AS OrigDescription,
''<b>For:</b> <a href="javascript:void(0)"  class="underLineHyperLink" onclick="openSalesOrder(''+CAST(OP.numOppId AS VARCHAR)+'')">''+OP.vcPOppName+''</a> &nbsp;<i onclick="ApproveRejectPORequistion(''+CAST(OP.numOppId AS VARCHAR)+'',1)"  class="fa fa-thumbs-up cursor"></i>  &nbsp;<i onclick="ApproveRejectPORequistion(''+CAST(OP.numOppId AS VARCHAR)+'',2)"  class="fa fa-thumbs-down cursor"></i> '' AS Description,
OP.intPEstimatedCloseDate AS dtDueDate,
OP.intPEstimatedCloseDate as DueDate,
0 As TotalProgress,
dbo.fn_GetContactName('+CAST(@numUserCntID AS VARCHAR(200))+') As numAssignedTo,
'+CAST(@numUserCntID AS VARCHAR(200))+' As numAssignToId,
'''' As Priority, -- Priority column   
'''' As Activity ,
0 As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(Div.numDivisionID AS VARCHAR)+''">''+Com.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating

 FROM  
 OpportunityMaster AS OP 
INNER JOIN 
	DivisionMaster Div                           
ON 
	OP.numDivisionId = Div.numDivisionID                       
INNER JOIN  
	CompanyInfo  Com                          
ON 
	Div.numCompanyID = Com.numCompanyId    
LEFT JOIN ListDetails AS listCompanyRating
 ON Com.numCompanyRating=listCompanyRating.NumlistItemID
WHERE
	OP.numDomainID = ' + Cast(@numDomainID as varchar(10))  + ' AND OP.tintOppType = 2 AND OP.tintOppStatus=1 
	AND 1 = (CASE WHEN '+CAST(@decReqPOMinValue AS VARCHAR(100))+'>0 AND Op.monPAmount>'+CAST(@decReqPOMinValue AS VARCHAR(100))+' THEN 1  WHEN '+CAST(@decReqPOMinValue AS VARCHAR(100))+'=0 THEN 1  ELSE 0 END )) AS Q
	WHERE  '+@OtherRegularSearch+' AND  (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) <= '''+Cast(@endDate as varchar(30))+''') 
'+@IndivRecordPaging+'
'
EXEC (@dynamicQuery)

END

------------------ 9 -  Price Margin Approval ----------------------------

SET @dynamicQuery =  ' INSERT INTO #tempRecords(
RecordCount,
RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
dtDueDate,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating
) SELECT COUNT(*) OVER() RecordCount,* FROM (SELECT
OP.numOppId AS RecordId,
OP.numCreatedBy AS numCreatedBy,
ISNULL(Div.numTerId,0) AS numOrgTerId,
0 AS numTaskId,
ISNULL(OP.bitReqPOApproved,0) AS IsTaskClosed,
''<a onclick="OpenMarginApproval(''+CAST(OP.numOppId AS VARCHAR)+'')" href="javascript:void(0)">Price Margin Approval</a>'' As TaskTypeName,
9 AS TaskType,
'''' AS OrigDescription,
''<b>For : </b> <a href="javascript:void(0)"  onclick="openSalesOrder(''+CAST(OP.numOppId AS VARCHAR)+'')">''+OP.vcPOppName+''</a> : '' +dbo.ItemUnitPriceApproval_CheckAndGetItemDetails(OP.numOppId,OP.numDomainId)+''  &nbsp;<i onclick="ApproveRejectPriceMarginApproval(''+CAST(OP.numOppId AS VARCHAR)+'',1)" class="fa fa-thumbs-up cursor"></i>  &nbsp;<i onclick="ApproveRejectPriceMarginApproval(''+CAST(OP.numOppId AS VARCHAR)+'',2)" class="fa fa-thumbs-down cursor"></i>''  AS Description,
OP.bintCreatedDate AS dtDueDate,
OP.bintCreatedDate as DueDate,
0 As TotalProgress,
dbo.fn_GetContactName('+CAST(@numUserCntID AS VARCHAR(200))+') As numAssignedTo,
'+CAST(@numUserCntID AS VARCHAR(200))+' As numAssignToId,
'''' As Priority, -- Priority column   
'''' As Activity ,
0 As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(Div.numDivisionID AS VARCHAR)+''">''+Com.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating

 FROM  
 OpportunityMaster AS OP 
INNER JOIN 
	DivisionMaster Div                           
ON 
	OP.numDivisionId = Div.numDivisionID                       
INNER JOIN  
	CompanyInfo  Com                          
ON 
	Div.numCompanyID = Com.numCompanyId    
LEFT JOIN ListDetails AS listCompanyRating
 ON Com.numCompanyRating=listCompanyRating.NumlistItemID
WHERE
	OP.numDomainID = ' + Cast(@numDomainID as varchar(10))  + ' AND OP.tintOppType = 1 AND (SELECT COUNT(OpportunityItems.numoppitemtCode) FROM OpportunityItems 
						WHERE OpportunityItems.bitItemPriceApprovalRequired=1 AND OpportunityItems.numOppId=OP.numOppId) >0
	AND '+CAST(@numUserCntID AS VARCHAR(100))+' IN (SELECT numUserID FROM UnitPriceApprover WHERE numDomainId= ' + Cast(@numDomainID as varchar(10))  + ' )) AS Q
	WHERE  '+@OtherRegularSearch+' AND  (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) <= '''+Cast(@endDate as varchar(30))+''') 
'+@IndivRecordPaging+'

'
EXEC (@dynamicQuery)

------------------------10 - A/R Invoice Due------------------------------------
IF(@bitARInvoiceDue=1 AND @numUserCntID IN(SELECT * FROM Split(@vchARInvoiceDue,',') WHERE Items<>''))
BEGIN
 DECLARE  @AuthoritativeSalesBizDocId  AS INTEGER
    SELECT @AuthoritativeSalesBizDocId = isnull(numAuthoritativeSales,0)
    FROM   AuthoritativeBizDocs
    WHERE  numDomainId = @numDomainId;
SET @dynamicQuery=  ' INSERT INTO #tempRecords(
RecordCount,
RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
dtDueDate,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating
) SELECT  COUNT(*) OVER() RecordCount,* FROM ( SELECT
OM.numOppId AS RecordId,
OM.numCreatedBy AS numCreatedBy,
ISNULL(DM.numTerId,0) AS numOrgTerId,
OB.numOppBizDocsId AS numTaskId,
0 AS IsTaskClosed,
''<a href="javascript:void(0)"  onclick="openBizDoc(''+CAST(OM.numOppId AS VARCHAR)+'',''+CAST(OB.numOppBizDocsId AS VARCHAR)+'')">A/R Invoice Due</a>'' As TaskTypeName,
10 AS TaskType,
'''' AS OrigDescription,
''<b>For : </b> <a href="javascript:void(0)" class="underLineHyperLink" onclick="openBizDoc(''+CAST(OM.numOppId AS VARCHAR)+'',''+CAST(OB.numOppBizDocsId AS VARCHAR)+'')">''+CAST(OB.[vcBizDocID] AS VARCHAR)+''</a> of <a href="javascript:void(0)" class="underLineHyperLink" onclick="openSalesOrder(''+CAST(OM.numOppId AS VARCHAR)+'')">''+CAST(OM.[vcPOppName] AS VARCHAR)+''</a> <b>Balance Due</b> <a href="javascript:void(0)" onclick="OpenAmtPaid(''+CAST(OM.numOppId AS VARCHAR)+'',''+CAST(OB.numOppBizDocsId AS VARCHAR)+'',''+CAST(OM.[numDivisionId] AS VARCHAR)+'')"   class="underLineHyperLink">'' + CAST(ISNULL(C.[varCurrSymbol],'''') AS VARCHAR) + '''' + CAST(CAST(isnull(OB.monDealAmount  - ISNULL(TablePayments.monPaidAmount,0),0) AS DECIMAL(18,2)) AS VARCHAR)+''</a>''  AS Description,
 CASE ISNULL(bitBillingTerms,0) 
                          WHEN 1 THEN DATEADD(dd,convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0)),OB.dtFromDate)
                          WHEN 0 THEN ob.[dtFromDate]
                        END AS dtDueDate,
 CASE ISNULL(bitBillingTerms,0) 
                          WHEN 1 THEN [dbo].[FormatedDateFromDate](DATEADD(dd,convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0)),OB.dtFromDate),
                                                                   1)
                          WHEN 0 THEN [dbo].[FormatedDateFromDate](ob.[dtFromDate],1)
                        END as DueDate,
0 As TotalProgress,
'''' As numAssignedTo,
0 As numAssignToId,
'''' As Priority, -- Priority column   
'''' As Activity ,
0 As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(DM.numDivisionID AS VARCHAR)+''">''+Com.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating

 FROM   [OpportunityMaster] OM INNER JOIN [DivisionMaster] DM ON OM.[numDivisionId] = DM.[numDivisionID]
				LEFT JOIN CompanyInfo  Com ON DM.numCompanyID = Com.numCompanyId    
				LEFT JOIN ListDetails AS listCompanyRating
 ON Com.numCompanyRating=listCompanyRating.NumlistItemID
               LEFT OUTER JOIN OpportunityBizDocs OB ON OB.numOppId = OM.numOppID
               LEFT OUTER JOIN [Currency] C ON OM.[numCurrencyID] = C.[numCurrencyID]
			   OUTER APPLY
			   (
					SELECT 
						SUM(monAmountPaid) AS monPaidAmount
					FROM
						DepositeDetails DD
					INNER JOIN
						DepositMaster DM
					ON
						DD.numDepositID=DM.numDepositId
					WHERE 
						numOppID = OM.numOppID
						AND numOppBizDocsID=OB.numOppBizDocsID
						AND CONVERT(DATE,DM.dtDepositDate) <= DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +','''+Cast(@endDate as varchar(100))+''')
				) TablePayments
        WHERE  OM.[tintOppType] = 1
               AND OM.[tintOppStatus] = 1
               AND om.[numDomainId] = ' + Cast(@numDomainId as varchar(10))  + '
               AND OB.[numBizDocId] = '+CAST(@AuthoritativeSalesBizDocId As VARCHAR)+'
               AND OB.bitAuthoritativeBizDocs=1
               AND ISNULL(OB.tintDeferred,0) <> 1
			   AND (OM.numAccountClass= 0 OR 0=0)
			   AND CONVERT(DATE,OB.[dtCreatedDate]) <= DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +','''+Cast(@endDate as varchar(100))+''')
               AND isnull(OB.monDealAmount
                            ,0) > 0  AND isnull(OB.monDealAmount * OM.fltExchangeRate
                        - ISNULL(TablePayments.monPaidAmount,0) * OM.fltExchangeRate,0) != 0
	) AS Q
	WHERE  '+@OtherRegularSearch+' AND  (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) <= '''+Cast(@endDate as varchar(30))+''') 
'+@IndivRecordPaging+'
'
EXEC (@dynamicQuery)
END
------------------------11 - A/P Bill Due------------------------------------
IF(@bitAPBillsDue=1 AND @numUserCntID IN(SELECT * FROM Split(@vchAPBillsDue,',') WHERE Items<>''))
BEGIN   
   DECLARE  @AuthoritativePurchaseBizDocId  AS INTEGER
    SELECT  @AuthoritativePurchaseBizDocId= isnull(numAuthoritativePurchase,0)
    FROM   AuthoritativeBizDocs
    WHERE  numDomainId =  @numDomainId
	

SET @dynamicQuery = '  INSERT INTO #tempRecords(
RecordCount,
RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
dtDueDate,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating
) SELECT  COUNT(*) OVER() RecordCount,* FROM (SELECT
OM.numOppId AS RecordId,
OM.numCreatedBy AS numCreatedBy,
ISNULL(DM.numTerId,0) AS numOrgTerId,
OB.numOppBizDocsId AS numTaskId,
0 AS IsTaskClosed,
''<a href="javascript:void(0)" onclick="openPurchaseBill(''+CAST(DM.[numDivisionID] As VARCHAR)+'')">A/P Bill Due<a/>'' As TaskTypeName,
11 AS TaskType,
'''' AS OrigDescription,
''<b>For : </b> <a href="javascript:void(0)" class="underLineHyperLink" onclick="openBizDoc(''+CAST(OM.numOppId AS VARCHAR)+'',''+CAST(OB.numOppBizDocsId AS VARCHAR)+'')">''+CAST(OB.[vcBizDocID] AS VARCHAR)+''</a> of <a href="javascript:void(0)" class="underLineHyperLink" onclick="openSalesOrder(''+CAST(OM.numOppId AS VARCHAR)+'')">''+CAST(OM.[vcPOppName] AS VARCHAR)+''</a> <b>Balance Due : </b> <a href="javascript:void(0)" onclick="openBizDoc(''+CAST(OM.numOppId AS VARCHAR)+'',''+CAST(OB.numOppBizDocsId AS VARCHAR)+'')"   class="underLineHyperLink">'' + CAST(ISNULL(C.[varCurrSymbol],'''') AS VARCHAR) + '''' + CAST(CAST(isnull((OB.monDealAmount * OM.fltExchangeRate)  - (ISNULL(TablePayment.monPaidAmount,0)* OM.fltExchangeRate),0) AS DECIMAL(18,2))  AS VARCHAR)+''</a>''  AS Description,
 CASE ISNULL(bitBillingTerms,0) 
                          WHEN 1 THEN DATEADD(dd,convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0)),OB.dtFromDate)
                          WHEN 0 THEN ob.[dtFromDate]
                        END AS dtDueDate,
 CASE ISNULL(bitBillingTerms,0) 
                          WHEN 1 THEN [dbo].[FormatedDateFromDate](DATEADD(dd,convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0)),OB.dtFromDate),
                                                                   1)
                          WHEN 0 THEN [dbo].[FormatedDateFromDate](ob.[dtFromDate],1)
                        END as DueDate,
0 As TotalProgress,
'''' As numAssignedTo,
0 As numAssignToId,
'''' As Priority, -- Priority column   
'''' As Activity ,
0 As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(DM.numDivisionID AS VARCHAR)+''">''+Com.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating

   FROM   [OpportunityMaster] OM
               INNER JOIN [DivisionMaster] DM
                 ON OM.[numDivisionId] = DM.[numDivisionID]
				 LEFT JOIN CompanyInfo  Com ON DM.numCompanyID = Com.numCompanyId    
				LEFT JOIN ListDetails AS listCompanyRating
 ON Com.numCompanyRating=listCompanyRating.NumlistItemID
               LEFT OUTER JOIN OpportunityBizDocs OB
                 ON OB.numOppId = OM.numOppID
               LEFT OUTER JOIN [Currency] C
                 ON OM.[numCurrencyID] = C.[numCurrencyID]
				 OUTER APPLY
				(
					SELECT
						SUm(BPD.monAmount) monPaidAmount
					FROM	
						BillPaymentDetails BPD
					INNER JOIN
						BillPaymentHeader BPH
					ON
						BPD.numBillPaymentID=BPH.numBillPaymentID
					WHERE
						BPD.numOppBizDocsID = OB.numOppBizDocsId
						AND CAST(BPH.dtPaymentDate AS DATE) <= DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +','''+Cast(@endDate as varchar(100))+''')
				) TablePayment
        WHERE  OM.[tintOppType] = 2
               AND OM.[tintOppStatus] = 1
               AND om.[numDomainId] = 1
               AND OB.[numBizDocId] = '+CAST(@AuthoritativePurchaseBizDocId AS VARCHAR)+' 
               AND OB.bitAuthoritativeBizDocs=1
               AND ISNULL(OB.tintDeferred,0) <> 1
			   AND (OM.numAccountClass= 0 OR 0=0)
			   AND CONVERT(DATE,OB.[dtCreatedDate]) <= DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +','''+Cast(@endDate as varchar(100))+''')
               AND  isnull(OB.monDealAmount * OM.fltExchangeRate,0) > 0 
			   AND isnull(OB.monDealAmount * OM.fltExchangeRate
                        - ISNULL(TablePayment.monPaidAmount,0) * OM.fltExchangeRate,0) > 0
	) AS Q
	WHERE  '+@OtherRegularSearch+' AND  (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) <= '''+Cast(@endDate as varchar(30))+''') 
'+@IndivRecordPaging+'
'

EXEC (@dynamicQuery)
END

END

SET @FormattedItems =''
SET @FormattedItems=' UPDATE #tempRecords SET DueDate = CASE WHEN convert(varchar(11),DueDate)=convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getdate())) then ''<b><font color=red>Today</font></b>''
WHEN convert(varchar(11),DueDate)=convert(varchar(11),DateAdd(day,-1,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getdate()))) then ''<b><font color=purple>Yesterday</font></b>''
WHEN convert(varchar(11),DueDate)=convert(varchar(11),DateAdd(day,1,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getdate()))) then ''<b><font color=orange>Tommorow</font></b>''
ELSE  dbo.FormatedDateFromDate(DueDate,'+ CONVERT(VARCHAR(10),@numDomainId)+') END '

 EXEC (@FormattedItems)
 DECLARE @strSql3 VARCHAR(MAX)=''
 IF LEN(@columnName) > 0
BEGIN
	SET @strSql3= CONCAT(@strSql3,' order by ',CASE WHEN @columnName = 'Action-Item Participants' THEN CONCAT('[','Action-Item Participants',']') ELSE @columnName END,' ',@columnSortOrder)
END
ELSE
BEGIN
	SET @strSql3= CONCAT(@strSql3,' ORDER BY dtDueDate DESC  ')
END
PRINT @strSql3
SET @TotalRecordCount=(SELECT ISNULL(MAX(RecordCount),0) FROM #tempRecords)

IF(@TotalRecordCount>0)
BEGIN
	DECLARE @totalPageRequired AS float = CEILING((@TotalRecordCount/CAST(10 AS float)))
	DECLARE @totalPageSize AS INT=0
	SET @totalPageSize = (SELECT TOP 1 tintCustomPagingRows FROM Domain WHERE numDomainId=@numDomainID)
	SET @TotalRecordCount = @totalPageRequired*@totalPageSize
END
SET @dynamicQuery = 'SELECT '+CAST(@TotalRecordCount AS varchar(400))+' AS TotalRecords,T.*
 FROM #tempRecords T WHERE 1=1 '+@strSql3


EXEC(@dynamicQuery)
DECLARE @TempColumns TABLE
	(
        vcFieldName VARCHAR(200)
        ,vcDbColumnName VARCHAR(200)
        ,vcOrigDbColumnName VARCHAR(200)
        ,bitAllowSorting BIT
        ,numFieldId INT
        ,bitAllowEdit BIT
        ,bitCustomField BIT
        ,vcAssociatedControlType VARCHAR(50)
        ,vcListItemType  VARCHAR(200)
        ,numListID INT
        ,ListRelID INT
        ,vcLookBackTableName VARCHAR(200)
        ,bitAllowFiltering BIT
        ,vcFieldDataType  VARCHAR(20)
		,intColumnWidth INT
		,bitClosedColumn BIT
		,bitFieldMessage BIT DEFAULT 0
		,vcFieldMessage VARCHAR(500)
		,bitIsRequired BIT DEFAULT 0
		,bitIsNumeric BIT DEFAULT 0
		,bitIsAlphaNumeric BIT DEFAULT 0
		,bitIsEmail BIT DEFAULT 0
		,bitIsLengthValidation  BIT DEFAULT 0
		,intMaxLength INT DEFAULT 0
		,intMinLength INT DEFAULT 0
	)



		declare @Nocolumns as tinyint         
set @Nocolumns=0       
         
Select @Nocolumns=isnull(count(*),0) from View_DynamicColumns where numFormId=43 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 
  
  
if @Nocolumns > 0            
begin      
INSERT INTO #tempForm
select tintRow+1 as tintOrder,vcDbColumnName,vcFieldDataType,vcOrigDbColumnName,bitAllowFiltering,ISNULL(vcCultureFieldName,vcFieldName),                  
 vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName                                                 
,bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth
 FROM View_DynamicColumns 
 where numFormId=43 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 
  AND ISNULL(bitSettingField,0)=1 AND ISNULL(bitCustom,0)=0
  order by tintOrder asc  
  
end            
else            
begin 

	--Check if Master Form Configuration is created by administrator if NoColumns=0
	DECLARE @IsMasterConfAvailable BIT = 0
	DECLARE @numUserGroup NUMERIC(18,0)

	SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

	IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = 43 AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
	BEGIN
		SET @IsMasterConfAvailable = 1
	END

	--If MasterConfiguration is available then load it otherwise load default columns
	IF @IsMasterConfAvailable = 1
	BEGIN
		INSERT INTO DycFormConfigurationDetails 
		(
			numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
		)
		SELECT 
			43,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,NULL,1,0,intColumnWidth
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=43 AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0

		INSERT INTO #tempForm
		SELECT 
			(intRowNum + 1) as tintOrder, vcDbColumnName,vcFieldDataType,vcOrigDbColumnName,bitAllowFiltering, ISNULL(vcCultureFieldName,vcFieldName), vcAssociatedControlType,
			vcListItemType,numListID,vcLookBackTableName, bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=43 AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
	ORDER BY 
		tintOrder ASC  
	END
	ELSE                                        
	BEGIN
		 INSERT INTO #tempForm
		select tintOrder,vcDbColumnName,vcFieldDataType
		,vcOrigDbColumnName,bitAllowFiltering,ISNULL(vcCultureFieldName,vcFieldName),                  
		 vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName                                                 
		,bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
		bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
		intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth
		 FROM View_DynamicDefaultColumns
		 where numFormId=43 and bitDefault=1 AND ISNULL(bitSettingField,0)=1 AND numDomainID=@numDomainID
		order by tintOrder asc  
	END 
enD

DELETE FROM #tempForm WHERE vcOrigDbColumnName IN('numActivity','numStatus','intTotalProgress','textDetails','vcCompanyName','bitTask','numAssignedTo','FormattedDate')
--UPDATE #tempForm SET bitCustomField=1
INSERT INTO #tempForm
		(
			tintOrder
			,vcFieldName
			,vcDbColumnName
			,vcOrigDbColumnName
			,bitAllowSorting
			,numFieldId
			,bitAllowEdit
			,bitCustomField
			,vcAssociatedControlType
			,vcListItemType
			,numListID
			,ListRelID
			,vcLookBackTableName
			,bitAllowFiltering
			,vcFieldDataType
			
		)
		VALUES
		(-9,'Type','TaskTypeName','TaskTypeName',0,1,0,0,'SelectBox','',0,0,'T',1,'V')
		,(-8,'Descriptions & Comments','Description','Description',0,184,1,0,'TextArea','',0,0,'T',1,'V')
		,(-7,'Due Date','DueDate','DueDate',1,3,0,0,'DateField','',0,0,'T',1,'V')
		,(-6,'Total Progress','TotalProgress','TotalProgress',1,4,0,0,'TextBox','',0,0,'T',0,'V')
		,(-5,'Assigned to','numAssignedTo','numAssignedTo',0,5,0,0,'SelectBox','',0,0,'T',1,'V')
		,(-4,'Priority','Priority','Priority',0,183,1,0,'SelectBox','',0,0,'T',1,'V')
		,(-3,'Activity','Activity','Activity',0,182,1,0,'SelectBox','',0,0,'T',1,'V')
		,(-2,'Organization (Rating)','OrgName','OrgName',0,8,0,0,'TextBox','',0,0,'T',1,'V')
UPDATE
		TC
	SET
		TC.intColumnWidth = ISNULL(DFCD.intColumnWidth,0)
	FROM
		#tempForm TC
	INNER JOIN
		DycFormConfigurationDetails DFCD
	ON
		TC.numFieldId = DFCD.numFieldId
	WHERE
		DFCD.numDomainId = @numDomainID
		AND DFCD.numUserCntID = @numUserCntID
		AND DFCD.numFormId = 43
		AND DFCD.tintPageType = 1 


	SELECT * FROM #tempForm order by tintOrder asc  
END
GO
