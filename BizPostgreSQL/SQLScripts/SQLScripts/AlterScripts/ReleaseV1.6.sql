/******************************************************************
Project: Release 1.6 Date: 27.05.2013
Comments: 
*******************************************************************/


/*******************Kamal Script******************/

--List Price (NI+I) to List Price
--SELECT * FROM dbo.DycFormField_Mapping WHERE numFormID=29
UPDATE DycFormField_Mapping SET vcFieldName='List Price' WHERE numFieldID=190 AND numFormID=29


--SELECT * FROM BillHeader WHERE monAmountDue=monAmtPaid AND bitIsPaid=0
UPDATE BillHeader SET bitIsPaid=1 WHERE monAmountDue=monAmtPaid AND bitIsPaid=0


/*******************Manish Script******************/


/******************************************************************
Project: BACRMUI   Date: 23.May.2013
Comments: To modify Campaign Name
*******************************************************************/
BEGIN TRANSACTION
--SELECT * FROM dbo.ShortCutBar WHERE vcLinkName = 'Online & Offline Campaigns'
UPDATE dbo.ShortCutBar SET vcLinkName = 'Campaigns' WHERE vcLinkName = 'Online & Offline Campaigns'
ROLLBACK


/******************************************************************
Project: BACRMUI   Date: 15.May.2013
Comments: To Add Campaign into Sales Order
*******************************************************************/
BEGIN TRANSACTION
UPDATE dbo.DycFieldMaster SET vcPropertyName = 'CampaignID', numListID = 0, vcListItemType = 'C' WHERE vcDbColumnName = 'numCampainID' AND vcLookBackTableName = 'OpportunityMaster'

INSERT INTO dbo.DycFormField_Mapping 
(numModuleID,numFieldID,numDomainID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,[order],tintRow,
 tintColumn,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,bitAllowFiltering,
 bitRequired,numFormFieldID,intSectionID,bitAllowGridColor )
 SELECT 3,116,NULL,39,1,1,'Campaign','SelectBox',NULL,NULL,1,1,1,1,0,1,1,1,1,1,NULL,NULL,NULL,1,NULL,0,NULL,NULL
 
 exec usp_GetTableInfoDefault @numUserCntID=1,@numRecordID=51916,@numDomainId=1,@charCoType='O',@pageId=2,@numRelCntType=0,@numFormID=39,@tintPageType=3

 --SELECT * FROM dbo.DycFormField_Mapping	WHERE vcFieldName = 'Campaign' AND numFormID = 39
 --DELETE FROM dbo.DycFormField_Mapping	WHERE vcFieldName = 'Campaign' AND numFormID = 39
ROLLBACK


/******************************************************************
Project: BACRMUI   Date: 13.May.2013
Comments: To Add Average Cost into Import
*******************************************************************/
BEGIN TRANSACTION
UPDATE dbo.DycFieldMaster SET vcPropertyName = 'AverageCost' WHERE vcFieldName = 'Average Cost' AND vcDbColumnName ='monAverageCost'

INSERT INTO dbo.DycFormField_Mapping 
(numModuleID,numFieldID,numDomainID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,[order],tintRow,
 tintColumn,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,bitAllowFiltering,
 bitRequired,numFormFieldID,intSectionID,bitAllowGridColor )
 SELECT 4,209,	NULL,	20,	0,	0,	'Average Cost',	'TextBox','AverageCost',NULL,NULL,NULL,NULL,1,0,0,NULL,	NULL,	NULL,	NULL,	NULL,	1,	NULL,	1,	NULL,	0,	1,	NULL
 
 --SELECT * FROM dbo.DycFormField_Mapping	WHERE vcFieldName LIKE '%avera%' AND numFormID = 20
 --DELETE FROM dbo.DycFormField_Mapping	WHERE vcFieldName LIKE '%avera%' AND numFormID = 20
ROLLBACK


/*******************Joseph Script******************/



/***********************************************************************************************
Project: Redirect Configurations  Date: 24/May/2013
Comments: 
************************************************************************************************/
INSERT INTO [dbo].[PageNavigationDTL]
           ([numPageNavID]
           ,[numModuleID]
           ,[numParentID]
           ,[vcPageNavName]
           ,[vcNavURL]
           ,[vcImageURL]
           ,[bitVisible]
           ,[numTabID])
     VALUES
           (221,13,157,'Configure Redirects','../ECommerce/frmRedirectConfigurations.aspx',NULL,1,-1)

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RedirectConfig](
	[numRedirectConfigID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numDomainId] [numeric](18, 0) NOT NULL,
	[numUserCntId] [numeric](18, 0) NOT NULL,
	[numSiteId] [numeric](18, 0) NULL,
	[vcOldUrl] [varchar](500) NULL,
	[vcNewUrl] [varchar](500) NULL,
	[numRedirectType] [numeric](18, 0) NULL,
	[numReferenceType] [numeric](18, 0) NULL,
	[numReferenceNo] [numeric](18, 0) NULL,
	[dtCreated] [datetime] NULL CONSTRAINT [DF_RedirectConfig_dtCreated]  DEFAULT (getdate()),
	[dtModified] [datetime] NULL,
	[bitIsActive] [bit] NULL CONSTRAINT [DF_RedirectConfig_bitIsActive]  DEFAULT ((1)),
 CONSTRAINT [PK_RedirectConfig] PRIMARY KEY CLUSTERED 
(
	[numRedirectConfigID] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING ON

/***********************************************************************************************
Project: Google Analytics Work  Date: 11/May/2013
Comments: 
************************************************************************************************/

BEGIN TRANSACTION
GO
ALTER TABLE Domain ADD
vcGAUserEMail VARCHAR(50) NULL

ALTER TABLE Domain ADD
vcGAUserPassword VARCHAR(50) NULL

ALTER TABLE Domain ADD
vcGAUserProfileID VARCHAR(50) NULL


GO
ROLLBACK TRANSACTION




