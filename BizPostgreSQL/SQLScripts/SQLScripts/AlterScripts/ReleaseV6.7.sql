/******************************************************************
Project: Release 6.7 Date: 17.JAN.2017
Comments: ALTER SCRIPTS
*******************************************************************/

/************************** SANDEEP ************************/

INSERT INTO DynamicFormMaster
(
	numFormId,vcFormName,cCustomFieldsAssociated,cAOIAssociated,bitDeleted,tintFlag,vcLocationID,numBizFormModuleID
)
VALUES
(
	130,'Item Grid Columns','Y','N',0,0,5,5
)



---------------------------------------

BEGIN TRY
BEGIN TRANSACTION

	DECLARE @numFieldID INT

	INSERT INTO DycFieldMaster
	(
		numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,[order],bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitDetailField,bitAllowSorting,bitAllowFiltering
	)
	VALUES
	(
		8,'Record Owner','numRecOwner','numRecOwner','UserCntID','Communication','N','R','SelectBox','U',0,0,1,0,0,0,1,0,1,1
	) 


	SELECT @numFieldID = SCOPE_IDENTITY()


	INSERT INTO DycFormField_Mapping
	(
		numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,bitAllowGridColor
	)
	VALUES
	(
		8,@numFieldID,43,0,0,'Record Owner','SelectBox',16,1,0,1,1,0,0,1,1,1
	)

COMMIT
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		ERROR_MESSAGE(),
		ERROR_NUMBER(),
		ERROR_SEVERITY(),
		ERROR_STATE(),
		ERROR_LINE(),
		ISNULL(ERROR_PROCEDURE(), '-');
END CATCH


/************************** PRASANT ************************/

ALTER TABLE ActivityFormAuthentication ADD bitAttendee BIT DEFAULT 0

-----------------------------------------------------

CREATE TABLE [dbo].[ActivityFormAuthentication](
	[numActivityAuthenticationId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numDomainId] [numeric](18, 0) NULL,
	[numGroupId] [numeric](18, 0) NULL,
	[bitFollowupStatus] [bit] NULL,
	[bitPriority] [bit] NULL,
	[bitActivity] [bit] NULL,
	[bitCustomField] [bit] NULL,
	[bitFollowupAnytime] [bit] NULL,
 CONSTRAINT [PK_ActivityFormAuthentication] PRIMARY KEY CLUSTERED 
(
	[numActivityAuthenticationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[ActivityFormAuthentication] ADD  CONSTRAINT [DF_ActivityFormAuthentication_bitFollowupStatus]  DEFAULT ((0)) FOR [bitFollowupStatus]
GO

ALTER TABLE [dbo].[ActivityFormAuthentication] ADD  CONSTRAINT [DF_ActivityFormAuthentication_bitPriority]  DEFAULT ((0)) FOR [bitPriority]
GO

ALTER TABLE [dbo].[ActivityFormAuthentication] ADD  CONSTRAINT [DF_ActivityFormAuthentication_bitActivity]  DEFAULT ((0)) FOR [bitActivity]
GO

ALTER TABLE [dbo].[ActivityFormAuthentication] ADD  CONSTRAINT [DF_ActivityFormAuthentication_bitCustomField]  DEFAULT ((0)) FOR [bitCustomField]
GO

ALTER TABLE [dbo].[ActivityFormAuthentication] ADD  CONSTRAINT [DF_ActivityFormAuthentication_bitFollowupAnytime]  DEFAULT ((0)) FOR [bitFollowupAnytime]
GO


-----------------------------------------------

INSERT INTO BizFormWizardModule(vcModule) values('Activity')

---------------------------------

INSERT INTO DynamicFormMaster
			(numFormId, vcFormName, cCustomFieldsAssociated, cAOIAssociated, bitDeleted
			, tintFlag, bitWorkFlow, vcLocationID, bitAllowGridColor, numBizFormModuleID)
		VALUES
			(131,'Activity Form','Y','Y',0,0,NULL,NULL,NULL,11)