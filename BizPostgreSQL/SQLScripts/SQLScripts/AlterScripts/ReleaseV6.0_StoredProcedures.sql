/******************************************************************
Project: Release 6.0 Date: 10.SEP.2016
Comments: STORED PROCEDURES
*******************************************************************/


GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetOppItemReleaseDates')
DROP FUNCTION GetOppItemReleaseDates
GO
CREATE FUNCTION [dbo].[GetOppItemReleaseDates] 
(
	@numDomainID NUMERIC(18,0),
    @numOppID AS NUMERIC(18,0),
	@numOppItemID AS NUMERIC(18,0)
)
RETURNS VARCHAR(MAX)
AS 
BEGIN
	DECLARE @vcValue AS VARCHAR(MAX) = ''

	IF EXISTS (SELECT * FROM OpportunityItemsReleaseDates WHERE	numOppID=@numOppID AND numOppItemID=@numOppItemID)
	BEGIN
		SET @vcValue = CONCAT('<a href="#" onclick="OpenItemReleaseDates(',@numOppID,',',@numOppItemID,')">',STUFF((SELECT 
							', ',CONCAT(dbo.FormatedDateFromDate(dtReleaseDate,@numDomainID),'(',numQty,')(',CASE tintStatus WHEN 1 THEN 'Open)' ELSE 'Purchased)' END)
						FROM 
							OpportunityItemsReleaseDates
						WHERE
							numOppID=@numOppID
							AND numOppItemID=@numOppItemID
						For XML PATH ('') ), 1, 2, ''),'</a>');

	END
	ELSE
	BEGIN
		SET @vcValue = CONCAT('<a href="#" onclick="OpenItemReleaseDates(',@numOppID,',',@numOppItemID,')">Not Available</a>')
	END

	RETURN @vcValue
END

GO
/****** Object:  StoredProcedure [dbo].[USP_AddUpdateWareHouseForItems]    Script Date: 07/26/2008 16:14:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_AddUpdateWareHouseForItems')
DROP PROCEDURE USP_AddUpdateWareHouseForItems
GO
CREATE PROCEDURE [dbo].[USP_AddUpdateWareHouseForItems]  
@numItemCode as numeric(9)=0,  
@numWareHouseID as numeric(9)=0,
@numWareHouseItemID as numeric(9)=0 OUTPUT,
@vcLocation as varchar(250)='',
@monWListPrice as money =0,
@numOnHand as FLOAT=0,
@numReorder as FLOAT=0,
@vcWHSKU as varchar(100)='',
@vcBarCode as varchar(50)='',
@numDomainID AS NUMERIC(9),
@strFieldList as TEXT='',
@vcSerialNo as varchar(100)='',
@vcComments as varchar(1000)='',
@numQty as numeric(18)=0,
@byteMode as tinyint=0,
@numWareHouseItmsDTLID as numeric(18)=0,
@numUserCntID AS NUMERIC(9)=0,
@dtAdjustmentDate AS DATETIME=NULL,
@numWLocationID NUMERIC(9)=0
AS  
BEGIN
	DECLARE @numDomain AS NUMERIC(18,0)
	SET @numDomain = @numDomainID

	DECLARE @bitLotNo AS BIT;
	SET @bitLotNo=0  
	
	DECLARE @bitSerialized AS BIT;
	SET @bitSerialized=0  

	SELECT 
		@bitLotNo=ISNULL(Item.bitLotNo,0),
		@bitSerialized=ISNULL(Item.bitSerialized,0)
	FROM 
		Item 
	WHERE 
		numItemCode=@numItemCode 
		AND numDomainID=@numDomainID

	DECLARE @vcDescription AS VARCHAR(100)

	IF @byteMode=0 or @byteMode=1 or @byteMode=2 or @byteMode=5
	BEGIN
		--Insert/Update WareHouseItems
		IF @byteMode=0 or @byteMode=1
		BEGIN
			IF @numWareHouseItemID>0
			BEGIN
				UPDATE 
					WareHouseItems
				SET 
					numWareHouseID=@numWareHouseID,				
					numReorder=@numReorder,
					monWListPrice=@monWListPrice,
					vcLocation=@vcLocation,
					numWLocationID = @numWLocationID,
					vcWHSKU=@vcWHSKU,
					vcBarCode=@vcBarCode,
					dtModified=GETDATE()   
				WHERE 
					numItemID=@numItemCode 
					AND numDomainID=@numDomainID 
					AND numWareHouseItemID=@numWareHouseItemID
		
				IF NOT EXISTS(SELECT numItemCode FROM dbo.OpportunityItems WHERE numItemCode = @numItemCode)
				BEGIN
					IF ((SELECT ISNULL(numOnHand,0) FROM WareHouseItems where numItemID = @numItemCode AND numDomainId = @numDomainID and numWareHouseItemID = @numWareHouseItemID) = 0)
					BEGIN
						UPDATE 
							WareHouseItems 
						SET 
							numOnHand = @numOnHand 
						WHERE 
							numItemID = @numItemCode 
							AND numDomainID = @numDomainID 
							AND numWareHouseItemID = @numWareHouseItemID		
					END
				END
		
				SET @vcDescription='UPDATE WareHouse'
			END
			ELSE
			BEGIN
				INSERT INTO WareHouseItems 
				(
					numItemID, 
					numWareHouseID,
					numOnHand,
					numReorder,
					monWListPrice,
					vcLocation,
					vcWHSKU,
					vcBarCode,
					numDomainID,
					dtModified,
					numWLocationID
				)  
				VALUES
				(
					@numItemCode,
					@numWareHouseID,
					(Case When @bitLotNo=1 or @bitSerialized=1 then 0 else @numOnHand end),
					@numReorder,
					@monWListPrice,
					@vcLocation,
					@vcWHSKU,
					@vcBarCode,
					@numDomainID,
					GETDATE(),
					@numWLocationID
				)  

				SET @numWareHouseItemID = @@identity
				SET @vcDescription='INSERT WareHouse'
			END
		END

		--Insert/Update WareHouseItmsDTL

		DECLARE @OldQty FLOAT = 0

		IF @byteMode=0 or @byteMode=2 or @byteMode=5
		BEGIN
			IF @bitLotNo=1 or @bitSerialized=1
			BEGIN
				IF @bitSerialized=1
					SET @numQty=1
	
				IF @byteMode=0
				BEGIN
					SELECT TOP 1 
						@numWareHouseItmsDTLID=numWareHouseItmsDTLID,
						@OldQty=numQty 
					FROM 
						WareHouseItmsDTL 
					WHERE 
						numWareHouseItemID=@numWareHouseItemID 
						AND vcSerialNo=@vcSerialNo and numQty > 0
				END
				ELSE IF @numWareHouseItmsDTLID>0
				BEGIN
					SELECT TOP 1 
						@OldQty=numQty 
					FROM 
						WareHouseItmsDTL 
					WHERE 
						numWareHouseItmsDTLID=@numWareHouseItmsDTLID
				END

				IF @numWareHouseItmsDTLID>0
				BEGIN
					UPDATE 
						WareHouseItmsDTL 
					SET 
						vcComments = @vcComments,
						numQty=@numQty,
						vcSerialNo=@vcSerialNo
					WHERE 
						numWareHouseItmsDTLID=@numWareHouseItmsDTLID 
						AND numWareHouseItemID=@numWareHouseItemID
                
					SET @vcDescription=CASE WHEN @byteMode=5 THEN 'Inventory Adjustment (Update Lot/Serial#) : ' ELSE 'UPDATE Lot/Serial# : ' END + @vcSerialNo 
				END
				ELSE
				BEGIN
				   INSERT INTO WareHouseItmsDTL
				   (numWareHouseItemID,vcSerialNo,vcComments,numQty,bitAddedFromPO)  
				   VALUES 
				   (@numWareHouseItemID,@vcSerialNo,@vcComments,@numQty,0)
				
				   SET @numWareHouseItmsDTLID=@@identity

				   SET @vcDescription=CASE WHEN @byteMode=5 THEN 'Inventory Adjustment (Insert Lot/Serial#) : ' ELSE 'INSERT Lot/Serial# : ' END + @vcSerialNo 
				END

 				UPDATE 
					WareHouseItems 
				SET 
					numOnHand=numOnHand + (@numQty - @OldQty),
					dtModified=GETDATE() 
				WHERE 
					numWareHouseItemID = @numWareHouseItemID 
					AND [numDomainID] = @numDomainID
					AND (numOnHand + (@numQty - @OldQty))>=0
			END 
		END

		IF DATALENGTH(@strFieldList)>2
		BEGIN
			--Insert Custom Fields base on WareHouseItems/WareHouseItmsDTL 
			declare @hDoc as int     
			EXEC sp_xml_preparedocument @hDoc OUTPUT, @strFieldList                                                                        

			declare  @rows as integer                                        
	                                         
			SELECT @rows=count(*) FROM OPENXML (@hDoc,'/NewDataSet/CusFlds',2)                                                  
							WITH (Fld_ID numeric(9),Fld_Value varchar(100))                                        
	                                                                           

			if @rows>0                                        
			begin  
			  Create table #tempTable (ID INT IDENTITY PRIMARY KEY,Fld_ID numeric(9),Fld_Value varchar(100))   
	                                      
			  insert into #tempTable (Fld_ID,Fld_Value)                                        
			  SELECT * FROM OPENXML (@hDoc,'/NewDataSet/CusFlds',2)                                                  
			  WITH (Fld_ID numeric(9),Fld_Value varchar(100))                                           
	                                         
			  delete CFW_Fld_Values_Serialized_Items from CFW_Fld_Values_Serialized_Items C                                        
			  inner join #tempTable T on C.Fld_ID=T.Fld_ID 
				where C.RecId=(Case when @bitLotNo=1 or @bitSerialized=1 then @numWareHouseItmsDTLID else @numWareHouseItemID end) and bitSerialized=(Case when @bitLotNo=1 or @bitSerialized=1 then 1 else 0 end)                                       
	      
			  insert into CFW_Fld_Values_Serialized_Items(Fld_ID,Fld_Value,RecId,bitSerialized)                                                  
			  select Fld_ID,isnull(Fld_Value,'') as Fld_Value,(Case when @bitLotNo=1 or @bitSerialized=1 then @numWareHouseItmsDTLID else @numWareHouseItemID end),(Case when @bitLotNo=1 or @bitSerialized=1 then 1 else 0 end) from #tempTable 

			  drop table #tempTable                                        
			 end  

			 EXEC sp_xml_removedocument @hDoc 
		END	  
 
		EXEC dbo.USP_ManageWareHouseItems_Tracking
		@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
		@numReferenceID = @numItemCode, --  numeric(9, 0)
		@tintRefType = 1, --  tinyint
		@vcDescription = @vcDescription, --  varchar(100)
		@numModifiedBy = @numUserCntID,
		@ClientTimeZoneOffset = 0,
		@dtRecordDate = @dtAdjustmentDate,
		@numDomainID = @numDomain 
	END
	ELSE IF @byteMode=3
	BEGIN
		IF EXISTS (SELECT * FROM [OpportunityMaster] OM INNER JOIN [OpportunityItems] OI ON OM.[numOppId] = OI.[numOppId] WHERE OM.[numDomainId]=@numDomainID AND [numWarehouseItmsID]=@numWareHouseItemID)
		BEGIN
			RAISERROR('OpportunityItems_Depend',16,1);
			RETURN
		END

		IF EXISTS (SELECT * FROM OpportunityItemsReceievedLocation WHERE [numDomainId]=@numDomainID AND numWarehouseItemID=@numWareHouseItemID)
		BEGIN
			RAISERROR('OpportunityItems_Depend',16,1);
			RETURN
		END

		IF (SELECT COUNT(*) FROM OpportunityKitItems WHERE numWareHouseItemId=@numWareHouseItemID) > 0
 		BEGIN
	  		RAISERROR('OpportunityKitItems_Depend',16,1);
			RETURN
		END
	
		IF (SELECT COUNT(*) FROM ItemDetails WHERE numWareHouseItemId=@numWareHouseItemID) > 0
 		BEGIN
	  		RAISERROR ('KitItems_Depend',16,1);
			RETURN
		END	
	    
		IF @bitLotNo=1 OR @bitSerialized=1
		BEGIN
			DELETE from CFW_Fld_Values_Serialized_Items where RecId in (select numWareHouseItmsDTLID from WareHouseItmsDTL where numWareHouseItemID=@numWareHouseItemID)
					and bitSerialized=(Case when @bitLotNo=1 or @bitSerialized=1 then 1 else 0 end) 
		END
		ELSE
		BEGIN
			DELETE from CFW_Fld_Values_Serialized_Items where RecId=@numWareHouseItemID
					and bitSerialized=(Case when @bitLotNo=1 or @bitSerialized=1 then 1 else 0 end) 
		END

		DELETE from WareHouseItmsDTL where numWareHouseItemID=@numWareHouseItemID 
		DELETE FROM WareHouseItems_Tracking WHERE numWareHouseItemID=@numWareHouseItemID and numDomainID=@numDomainID
		DELETE from WareHouseItems where numWareHouseItemID=@numWareHouseItemID and numDomainID=@numDomainID
	END
	ELSE IF @byteMode=4
	BEGIN
		IF @bitLotNo=1 or @bitSerialized=1
		BEGIN
			DELETE from CFW_Fld_Values_Serialized_Items where RecId=@numWareHouseItmsDTLID
			and bitSerialized=(Case when @bitLotNo=1 or @bitSerialized=1 then 1 else 0 end) 
		END

		update WHI SET numOnHand=WHI.numOnHand - WHID.numQty,dtModified=GETDATE()
		from WareHouseItems WHI join WareHouseItmsDTL WHID on WHI.numWareHouseItemID=WHID.numWareHouseItemID 
		where WHID.numWareHouseItmsDTLID=@numWareHouseItmsDTLID 
		AND WHI.numDomainID = @numDomainID
		AND (WHI.numOnHand - WHID.numQty)>=0

		SELECT @numWareHouseItemID=WHI.numWareHouseItemID,@numItemCode=numItemID 
		from WareHouseItems WHI join WareHouseItmsDTL WHID on WHI.numWareHouseItemID=WHID.numWareHouseItemID 
		where WHID.numWareHouseItmsDTLID=@numWareHouseItmsDTLID 
		AND WHI.numDomainID = @numDomainID


		EXEC dbo.USP_ManageWareHouseItems_Tracking
			@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
			@numReferenceID = @numItemCode, --  numeric(9, 0)
			@tintRefType = 1, --  tinyint
			@vcDescription = 'DELETE Lot/Serial#', --  varchar(100)
			@numModifiedBy = @numUserCntID,
			@ClientTimeZoneOffset = 0,
			@dtRecordDate = NULL,
			@numDomainID = @numDomain

		DELETE from WareHouseItmsDTL where numWareHouseItmsDTLID=@numWareHouseItmsDTLID
	END
END
/****** Object:  StoredProcedure [dbo].[USP_CheckCanbeDeleteOppertunity]    Script Date: 07/26/2008 16:20:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CheckCanbeDeleteOppertunity')
DROP PROCEDURE USP_CheckCanbeDeleteOppertunity
GO
CREATE PROCEDURE [dbo].[USP_CheckCanbeDeleteOppertunity]              
	@numOppId AS NUMERIC(18,0),
	@tintError AS TINYINT=0 output
AS
BEGIN
	DECLARE @numDomainID NUMERIC(18,0)
	DECLARE @OppType AS VARCHAR(2)   
	DECLARE @itemcode AS NUMERIC     
	DECLARE @numWareHouseItemID AS NUMERIC(18,0)
	DECLARE @numWLocationID AS NUMERIC(18,0)                           
	DECLARE @numUnits AS FLOAT                                            
	DECLARE @numQtyShipped AS FLOAT
	DECLARE @onHand AS FLOAT
	DECLARE @onOrder AS FLOAT                                                                                                                           
	DECLARE @numoppitemtCode AS NUMERIC(9)
	DECLARE @Kit AS BIT                   
	DECLARE @LotSerial AS BIT                   
	DECLARE @numQtyReceived AS FLOAT
	DECLARE @tintShipped AS TINYINT 

	SET @tintError=0          
	SELECT 
		@OppType=tintOppType,
		@tintShipped=tintShipped,
		@numDomainID=numDomainId 
	FROM 
		OpportunityMaster 
	WHERE 
		numOppId=@numOppId
 
	IF @OppType=2
	BEGIN
		SELECT TOP 1 
			@numoppitemtCode=numoppitemtCode,
			@itemcode=OI.numItemCode,
			@numUnits=ISNULL(numUnitHour,0),
			@numQtyShipped=ISNULL(numQtyShipped,0),
			@numQtyReceived=ISNULL(numUnitHourReceived,0),
			@numWareHouseItemID=ISNULL(numWarehouseItmsID,0),
			@numWLocationID=ISNULL(numWLocationID,0),
			@Kit= (CASE WHEN bitKitParent=1 AND bitAssembly=1 THEN 0 WHEN bitKitParent=1 THEN 1 ELSE 0 END),
			@LotSerial=(CASE WHEN I.bitLotNo=1 OR I.bitSerialized=1 THEN 1 ELSE 0 END) 
		FROM 
			OpportunityItems OI
		LEFT JOIN
			WareHouseItems WI
		ON
			OI.numWarehouseItmsID=WI.numWareHouseItemID                                            
		JOIN 
			Item I                                            
		ON 
			OI.numItemCode=I.numItemCode 
			AND numOppId=@numOppId 
			AND (bitDropShip=0 OR bitDropShip is NULL)                                        
		WHERE 
			charitemtype='P' 
		ORDER BY 
			OI.numoppitemtCode

		WHILE @numoppitemtCode>0                               
		BEGIN  
			IF @numWLocationID = -1
			BEGIN
				IF (SELECT 
						COUNT(*)
					FROM
						OpportunityItemsReceievedLocation OIRL
					INNER JOIN
						WareHouseItems WI
					ON
						OIRL.numWarehouseItemID=WI.numWareHouseItemID
					WHERE
						OIRL.numDomainID=@numDomainID
						AND numOppID=@numOppId
						AND numOppItemID=@numoppitemtCode
					GROUP BY
						OIRL.numWarehouseItemID,
						WI.numOnHand
					HAVING 
						WI.numOnHand < SUM(OIRL.numUnitReceieved)) > 0
				BEGIN
					IF @tintError=0 SET @tintError=1
				END


				IF (SELECT 
						COUNT(*)
					FROM
						WareHouseItems
					WHERE 
						numWareHouseItemID=@numWareHouseItemID
						AND ISNULL(numOnOrder,0) < (@numUnits - ISNULL((SELECT SUM(numUnitReceieved) FROM OpportunityItemsReceievedLocation WHERE numDomainID=@numDomainID AND numOppID=@numOppId AND numOppItemID=@numoppitemtCode),0))
					) > 0
				BEGIN  
					if @tintError=0 set @tintError=1
				END 
			END
			ELSE
			BEGIN
				SELECT 
					@onHand = ISNULL(numOnHand, 0),
					@onOrder=ISNULL(numonOrder,0) 
				FROM 
					WareHouseItems 
				WHERE 
					numWareHouseItemID=@numWareHouseItemID 
  
				IF @onHand < @numQtyReceived
				BEGIN  
					IF @tintError=0 SET @tintError=1
				END  
				 
				IF @onOrder < (@numUnits-@numQtyReceived)
				BEGIN  
					if @tintError=0 set @tintError=1
				END 
			END
	
			SELECT TOP 1 
				@numoppitemtCode=numoppitemtCode,
				@itemcode=OI.numItemCode,
				@numUnits=ISNULL(numUnitHour,0),
				@numQtyShipped=ISNULL(numQtyShipped,0),
				@numQtyReceived=ISNULL(numUnitHourReceived,0),
				@numWareHouseItemID=numWarehouseItmsID,
				@numWLocationID=ISNULL(numWLocationID,0),
				@Kit= (CASE WHEN bitKitParent=1 AND bitAssembly=1 THEN 0 WHEN bitKitParent=1 THEN 1 ELSE 0 END),
				@LotSerial=(CASE WHEN I.bitLotNo=1 OR I.bitSerialized=1 THEN 1 ELSE 0 END) 
			FROM 
				OpportunityItems OI                                            
			LEFT JOIN
				WareHouseItems WI
			ON
				OI.numWarehouseItmsID=WI.numWareHouseItemID
			JOIN 
				Item I                                            
			ON 
				OI.numItemCode=I.numItemCode 
				AND numOppId=@numOppId                                          
			WHERE 
				charitemtype='P' 
				AND OI.numoppitemtCode>@numoppitemtCode 
				AND (bitDropShip=0 OR bitDropShip IS NULL) 
			ORDER BY 
				OI.numoppitemtCode                                            
    
			IF @@rowcount=0 
				SET @numoppitemtCode=0    
		END  
	END
END
GO
/****** Object:  StoredProcedure [dbo].[USP_CheckCanbeReOpenOppertunity]    Script Date: 07/26/2008 16:20:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CheckCanbeReOpenOppertunity')
DROP PROCEDURE USP_CheckCanbeReOpenOppertunity
GO
CREATE PROCEDURE [dbo].[USP_CheckCanbeReOpenOppertunity]              
@intOpportunityId AS NUMERIC(18,0),
@bitCheck AS BIT=0
AS              
BEGIN         
	DECLARE @numDomainID NUMERIC(18,0)
	DECLARE @OppType AS VARCHAR(2)   
	DECLARE @Sel AS int   
	DECLARE @itemcode AS NUMERIC     
	DECLARE @numWareHouseItemID AS NUMERIC(18,0)
	DECLARE @numWLocationID AS NUMERIC(18,0)                                
	DECLARE @numUnits AS FLOAT                                            
	DECLARE @numQtyShipped AS FLOAT
	DECLARE @onHand AS FLOAT                                                                                 
	DECLARE @numoppitemtCode AS NUMERIC(9)
	DECLARE @Kit AS BIT                   
	DECLARE @LotSerial AS BIT                   
	DECLARE @numQtyReceived AS FLOAT

	SET @Sel=0          
	SELECT 
		@OppType=tintOppType,
		@numDomainID=numDomainId
	FROM 
		OpportunityMaster 
	WHERE 
		numOppId=@intOpportunityId
 
	IF @OppType=2
	BEGIN
		SELECT TOP 1 
			@numoppitemtCode=numoppitemtCode,
			@itemcode=OI.numItemCode,
			@numUnits=ISNULL(numUnitHour,0),
			@numQtyShipped=ISNULL(numQtyShipped,0),
			@numQtyReceived=ISNULL(numUnitHourReceived,0),
			@numWareHouseItemID=numWarehouseItmsID,
			@numWLocationID=ISNULL(numWLocationID,0),
			@Kit= (CASE WHEN bitKitParent=1 and bitAssembly=1 THEN 0 WHEN bitKitParent=1 THEN 1 ELSE 0 END),
			@LotSerial=(CASE WHEN I.bitLotNo=1 OR I.bitSerialized=1 THEN 1 ELSE 0 END) 
		FROM 
			OpportunityItems OI
		LEFT JOIN
			WareHouseItems WI
		ON
			OI.numWarehouseItmsID=WI.numWareHouseItemID                                            
		JOIN 
			Item I                                            
		ON 
			OI.numItemCode=I.numItemCode 
			AND numOppId=@intOpportunityId 
			AND (bitDropShip=0 OR bitDropShip IS NULL)                                        
		WHERE 
			charitemtype='P' 
		ORDER BY 
			OI.numoppitemtCode

		WHILE @numoppitemtCode>0                               
		BEGIN  
			-- Selected Global Warehouse Location 
			IF @numWLocationID = -1
			BEGIN
				IF (SELECT 
						COUNT(*)
					FROM
						OpportunityItemsReceievedLocation OIRL
					INNER JOIN
						WareHouseItems WI
					ON
						OIRL.numWarehouseItemID=WI.numWareHouseItemID
					WHERE
						OIRL.numDomainID=@numDomainID
						AND numOppID=@intOpportunityId
						AND numOppItemID=@numoppitemtCode
					GROUP BY
						OIRL.numWarehouseItemID,
						WI.numOnHand
					HAVING 
						WI.numOnHand < SUM(OIRL.numUnitReceieved)) > 0
				BEGIN
					IF @Sel=0 SET @Sel=1
				END
			END
			ELSE
			BEGIN
				SELECT 
					@onHand = ISNULL(numOnHand, 0) 
				FROM
					WareHouseItems 
				WHERE 
					numWareHouseItemID=@numWareHouseItemID

				IF @onHand < @numUnits
				BEGIN  
					IF @Sel=0 SET @Sel=1
				END
			END

			SELECT TOP 1 
				@numoppitemtCode=numoppitemtCode,
				@itemcode=OI.numItemCode,
				@numUnits=ISNULL(numUnitHour,0),
				@numQtyShipped=ISNULL(numQtyShipped,0),
				@numQtyReceived=ISNULL(numUnitHourReceived,0),
				@numWareHouseItemID=numWarehouseItmsID,
				@numWLocationID=ISNULL(numWLocationID,0),
				@Kit= (CASE WHEN bitKitParent=1 AND bitAssembly=1 THEN 0 WHEN bitKitParent=1 THEN 1 ELSE 0 END),
				@LotSerial=(CASE WHEN I.bitLotNo=1 OR I.bitSerialized=1 THEN 1 ELSE 0 END) 
			FROM 
				OpportunityItems OI                                            
			LEFT JOIN
				WareHouseItems WI
			ON
				OI.numWarehouseItmsID=WI.numWareHouseItemID
			JOIN 
				Item I                                            
			ON 
				OI.numItemCode=I.numItemCode and numOppId=@intOpportunityId                                          
			WHERE 
				charitemtype='P' 
				AND OI.numoppitemtCode>@numoppitemtCode 
				AND (bitDropShip=0 OR bitDropShip IS NULL) 
			ORDER BY 
				OI.numoppitemtCode                                            
    
			IF @@rowcount=0 
				SET @numoppitemtCode=0    
		END  
	END      


	IF EXISTS (SELECT * FROM dbo.ReturnHeader WHERE numOppId = @intOpportunityId)         
	BEGIN
		SET @Sel = -1 * @OppType;
	END	
                                    
  
	SELECT @Sel
END
GO

/****** Object:  StoredProcedure [dbo].[USP_DeleteCartItem]    Script Date: 11/08/2011 17:28:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_DeleteCartItem' ) 
                    DROP PROCEDURE USP_DeleteCartItem
                    Go
CREATE PROCEDURE [dbo].[USP_DeleteCartItem]
    (
      @numUserCntId NUMERIC(18, 0),
      @numDomainId NUMERIC(18, 0),
      @vcCookieId VARCHAR(100),
      @numItemCode NUMERIC(9, 0) = 0,
      @bitDeleteAll BIT=0,
	  @numSiteID NUMERIC(18,0) = 0
    )
AS 
IF @bitDeleteAll = 0
BEGIN
    IF @numItemCode <> 0 
    BEGIN
		-- REMOVE DISCOUNT APPLIED TO ALL ITEMS FROM PROMOTION TRIGGERED OF ITEM TO BE DELETED
		UPDATE 
			CartItems
		SET 
			PromotionDesc='',PromotionID=0,fltDiscount=0,monTotAmount=(monPrice*numUnitHour),monTotAmtBefDiscount=(monPrice*numUnitHour),vcCoupon=NULL,bitParentPromotion=0
		WHERE
			PromotionID=(SELECT TOP 1 PromotionID FROM CartItems WHERE numDomainId = @numDomainId AND numUserCntId = @numUserCntId AND numItemCode = @numItemCode AND vcCookieId=@vcCookieId AND ISNULL(bitParentPromotion,0)=1)
			AND numDomainId = @numDomainId
            AND numUserCntId = @numUserCntId
			AND vcCookieId=@vcCookieId
			AND ISNULL(bitParentPromotion,0)=0

        DELETE FROM CartItems WHERE numDomainId = @numDomainId AND numUserCntId = @numUserCntId AND numItemCode = @numItemCode		

		EXEC USP_ManageECommercePromotion @numUserCntId,@numDomainId,@vcCookieId,0,@numSiteID
    END
    ELSE
    BEGIN
		UPDATE 
			CartItems
		SET 
			PromotionDesc='',PromotionID=0,fltDiscount=0,monTotAmount=(monPrice*numUnitHour),monTotAmtBefDiscount=(monPrice*numUnitHour),vcCoupon=NULL,bitParentPromotion=0
		WHERE
			numDomainId = @numDomainId
            AND numUserCntId = @numUserCntId
			AND vcCookieId=@vcCookieId

		DELETE FROM CartItems WHERE numDomainId = @numDomainId AND numUserCntId = 0 AND vcCookieId = @vcCookieId		
    END
END
ELSE
BEGIN
	DELETE FROM CartItems WHERE numDomainId = @numDomainId AND numUserCntId = @numUserCntId	
END


 



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DemandForecast_Execute')
DROP PROCEDURE dbo.USP_DemandForecast_Execute
GO
CREATE PROCEDURE [dbo].[USP_DemandForecast_Execute]
	@numDFID NUMERIC (18,0),
	@numUserCntID NUMERIC(18,0),
	@numPageIndex INT,
	@numPageSize INT,
	@ClientTimeZoneOffset INT,
	@FromDate DATETIME,
	@ToDate DATETIME
AS 
BEGIN
	DECLARE @numDomainID NUMERIC(18,0)
	DECLARE @numDFAPID NUMERIC(18,0)
	DECLARE @numDFDaysID NUMERIC(18,0)
	DECLARE @bitLastYear BIT
	DECLARE @bitIncludeSalesOpp BIT
	DECLARE @dtFromDate DATETIME
	DECLARE @dtToDate DATETIME
	DECLARE @dtLastExecution DATETIME

	
	IF (SELECT COUNT(*) FROM DemandForecast WHERE numDFID = @numDFID) > 0
	BEGIN
		
		DECLARE @TotalRowCount AS INTEGER = 0
		DECLARE @numAnalysisDays INT
		DECLARE @numForecastDays INT

		-- Get Selected Historical Analysis Pattern, Forecast Days and is Last Year
		SELECT 
			@numDomainID=numDomainID, 
			@numDFAPID=numDFAPID, 
			@numDFDaysID=numDFDaysID, 
			@bitLastYear=bitLastYear, 
			@bitIncludeSalesOpp= bitIncludeSalesOpp,
			@dtLastExecution = dtExecutionDate
		FROM 
			DemandForecast 
		WHERE 
			numDFID = @numDFID

		-- Get Days of Historical Analysis
		SELECT @numAnalysisDays=numDays FROM DemandForecastAnalysisPattern WHERE numDFAPID = @numDFAPID

		-- Get Days for forecast
		SELECT  @numForecastDays = numDays FROM DemandForecastDays WHERE numDFDaysID = @numDFDaysID

		--User has selected its own range
		IF NOT @FromDate IS NULL AND NOT @ToDate IS NULL
		BEGIN
			SET @dtFromDate = @FromDate
			SET @dtToDate = @ToDate
		END
		ELSE
		BEGIN
			-- Check if get last week from last year
			IF ISNULL(@bitLastYear,0) = 1 --last week from last year
			BEGIN
				SET @dtToDate = DATEADD(yy,-1,DATEADD(d,-1,GETDATE()))
				SET @dtFromDate = DATEADD(yy,-1,DATEADD(d,-(@numAnalysisDays),GETDATE()))
			END
			ELSE --last week current year
			BEGIN
				SET @dtToDate = DATEADD(d,-1,GETDATE())
				SET @dtFromDate = DATEADD(d,-(@numAnalysisDays),GETDATE())
			END
		END
		
		DECLARE @TEMPOppMaster TABLE
		(
			numItemCode NUMERIC(18,0),
			numWarehouseItemID NUMERIC(18,0),
			numQuantitySold INT
		)

		DECLARE @TEMPSALESOPP TABLE
		(
			numItemCode INT,
			numWarehouseItemID NUMERIC(18,0),
			numQuantitySold INT
		)

		-- Get total quantity of each item sold in sales order between from and to date and insert into temp table
		INSERT INTO @TEMPOppMaster
			(
				numItemCode,
				numWarehouseItemID,
				numQuantitySold
			)
		SELECT
			numItemCode,
			numWarehouseItmsID,
			ISNULL(numQtySold,0)
		FROM
			dbo.GetDFHistoricalSalesOrderData(@numDFID,@numDomainID,@dtFromDate,@dtToDate)

		--If user has selected to include Sales Opportunity(tintOppType = 1 AND tintOppStatus = 0) items in demand forecast then include it in total quantity sold by numPercentComplete
		--For example if 10 units within a Sales Opportunity that shows 50% completion, then an addition 5 units would be added
		IF ISNULL(@bitIncludeSalesOpp,0) = 1
		BEGIN
				
			INSERT INTO @TEMPSALESOPP
			(
				numItemCode,
				numWarehouseItemID,
				numQuantitySold
			)
			SELECT
				numItemCode,
				numWarehouseItmsID,
				ISNULL(numQtySold,0)
			FROM
				dbo.GetDFHistoricalSalesOppData(@numDFID,@numDomainID,@dtFromDate,@dtToDate)


			-- Increase quantity of item is it already exist in table @TEMPOppMaster
			UPDATE
				@TEMPOppMaster
			SET
				numQuantitySold = ISNULL(t1.numQuantitySold,0) + ISNULL(t2.numQuantitySold,0)
			FROM
				@TEMPOppMaster AS t1
			LEFT JOIN
				@TEMPSALESOPP AS t2
			ON
				t1.numItemCode = t2.numItemCode AND
				t1.numWarehouseItemID = t2.numWarehouseItemID

			-- If item is not exist in @TEMPOppMaster then insert them in @TEMPOppMaster
			INSERT INTO 
				@TEMPOppMaster
			SELECT
				TEMPSALESOPP.numItemCode,
				TEMPSALESOPP.numWarehouseItemID,
				TEMPSALESOPP.numQuantitySold
			FROM
				@TEMPSALESOPP AS TEMPSALESOPP
			WHERE NOT EXISTS
				(
					SELECT 
						1
					FROM
						@TEMPOppMaster t2
					WHERE
						TEMPSALESOPP.numItemCode = t2.numItemCode and 
						TEMPSALESOPP.numWarehouseItemID = t2.numWarehouseItemID
				)
		END

				
		SELECT 
			ROW_NUMBER() OVER (ORDER BY TEMP.dtOrderDate) AS NUMBER,
			TEMP.*
		INTO 
			#TEMP
		FROM
		(
			SELECT
				Item.vcModelID,
				Item.vcItemName,
				Item.bitAssembly,
				'Inventory Item' AS vcItemType,
				CONCAT(T2.numOnHand,' / ', T2.numAllocation,' / ', T2.numOnOrder,' / ',T2.numBackOrder,' / ', T2.numReOrder) AS vcInventory,
				T2.numOnHand, 
				T2.numOnOrder, 
				T2.numAllocation, 
				T2.numBackOrder,
				T2.numReOrder,
				Item.numItemCode,
				(SELECT ISNULL(vcData,'') FROM ListDetails WHERE numListID = 36 and numListItemID = Item.numItemClassification) AS numItemClassification,
				(SELECT ISNULL(vcItemGroup,'') FROM ItemGroups WHERE numItemGroupID = Item.numItemGroup) AS numItemGroup,
				(CASE WHEN Item.bitAssembly = 1 THEN Item.vcSKU ELSE WareHouseItems.vcWHSKU END) AS vcSKU,
				WareHouses.numWarehouseID,
				WareHouses.vcWareHouse,
				WareHouseItems.numWarehouseItemID,
				T2.intMinQty,
				Item.numVendorID AS numVendorID,
				0 AS intLeadTimeDays, -- Not fetched from this store procedure. It is fetched from ui when vendor dropdown loads
				0 AS monListPrice, -- Not fetched from this store procedure. It is fetched from ui when vendor dropdown loads
				ISNULL(t1.numQuantitySold,0) AS numQuantitySold,
				(CASE WHEN T2.intMinQty > T2.numQtyToOrder THEN T2.intMinQty ELSE T2.numQtyToOrder END) AS numQtyToOrder,
				CONVERT(DATE,T2.dtOrderDate) dtOrderDate,
				(CASE WHEN CONVERT(DATE,T2.dtOrderDate) = CONVERT(DATE,GETDATE()) THEN
				'<b><font color=red>Today</font></b>'
				WHEN CONVERT(DATE,T2.dtOrderDate) = CONVERT(DATE,DATEADD(d,1,GETDATE())) THEN
				'<b><font color=purple>Tomorrow</font></b>'
				ELSE
					dbo.FormatedDateFromDate(dtOrderDate,@numDomainID)
				END) AS vcOrderDate,
				@numAnalysisDays AS numAnalysisDays,
				@numForecastDays AS numForecastDays
			FROM
				Item
			INNER JOIN
				WareHouseItems
			ON
				Item.numItemCode = WareHouseItems.numItemID
			INNER JOIN
				WareHouses
			ON
				WareHouseItems.numWarehouseID = WareHouses.numWarehouseID
			INNER JOIN
				@TEMPOppMaster t1
			ON
				Item.numItemCode = t1.numItemCode AND
				WareHouseItems.numWarehouseItemID = t1.numWarehouseItemID
			CROSS APPLY
				dbo.GetItemQuantityToOrder
				(
					t1.numItemCode,
					WareHouseItems.numWarehouseItemID,
					@numDomainID,
					@numAnalysisDays,
					@numForecastDays,
					ISNULL(t1.numQuantitySold,0),
					NULL
				) T2
			WHERE
				Item.numDomainID = @numDomainID AND WareHouseItems.numWLocationID <> -1 AND 
				(
					(SELECT COUNT(*) FROM DemandForecastItemClassification WHERE numDFID = @numDFID) = 0 OR
					numItemClassification IN (SELECT numItemClassificationID FROM DemandForecastItemClassification WHERE numDFID = @numDFID)
				) AND
				(
					(SELECT COUNT(*) FROM DemandForecastItemGroup WHERE numDFID = @numDFID) = 0 OR
					numItemGroup IN (SELECT numItemGroupID FROM DemandForecastItemGroup WHERE numDFID = @numDFID)
				) AND
				(
					(SELECT COUNT(*) FROM DemandForecastWarehouse WHERE numDFID = @numDFID) = 0 OR
					WareHouseItems.numWareHouseID IN (SELECT numWarehouseID FROM DemandForecastWarehouse WHERE numDFID = @numDFID)
				) AND
				ISNULL(T2.numQtyToOrder,0) > 0 
		) AS TEMP
			
		
		SELECT @TotalRowCount = COUNT(*) FROM #TEMP
		
		SELECT *,ISNULL(@TotalRowCount,0) AS TotalRowCount FROM #TEMP WHERE NUMBER BETWEEN ((@numPageIndex - 1) * @numPageSize + 1) AND (@numPageIndex * @numPageSize) ORDER BY dtOrderDate

		-- Grid Column Configuration Detail
		DECLARE  @Nocolumns  AS TINYINT;
		SET @Nocolumns = 0

		SELECT @Nocolumns=isnull(sum(TotalRow),0) from(            
		SELECT count(*) TotalRow from View_DynamicColumns where numFormId=125 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 
		UNION 
		SELECT count(*) TotalRow from View_DynamicCustomColumns where numFormId=125 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1) TotalRows

		IF @Nocolumns = 0
		BEGIN
			INSERT INTO DycFormConfigurationDetails 
			(
				numFormID,numFieldID,intColumnNum,intRowNum,
				numDomainID,numUserCntID,numRelCntType,
				tintPageType,bitCustom,intColumnWidth,numViewID
			)
			SELECT 
				125,numFieldId,0,Row_number() over(order by numFormID),
				@numDomainID,@numUserCntID,0,
				1,0,0,0
			FROM 
				DycFormField_Mapping
			WHERE 
				numFormId=125 and bitDefault=1 AND ISNULL(bitSettingField,0)=1 
			ORDER BY 
				[order] ASC  
		END

		SELECT 
			tintRow+1 AS tintOrder,
			vcDbColumnName,
			vcOrigDbColumnName,
			ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName
		FROM 
			View_DynamicColumns 
		WHERE 
		 numFormId=125 AND 
		 numUserCntID=@numUserCntID AND 
		 numDomainID=@numDomainID AND 
		 tintPageType=1 AND 
		 ISNULL(bitSettingField,0)=1 AND 
		 ISNULL(bitCustom,0)=0
		ORDER BY
			tintOrder
 
		--Update Last Execution Date
		UPDATE DemandForecast SET dtExecutionDate = dateadd(MINUTE,-@ClientTimeZoneOffset,GETDATE()) WHERE numDFID = @numDFID

		SELECT CONVERT(VARCHAR(20),dateadd(MINUTE,-@ClientTimeZoneOffset,@dtLastExecution)) AS dtLastExecution
	END
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS (SELECT * FROM sysobjects WHERE xtype = 'p' AND NAME = 'USP_ECommerceTiggerPromotion') 
DROP PROCEDURE USP_ECommerceTiggerPromotion
GO
CREATE  PROCEDURE [dbo].[USP_ECommerceTiggerPromotion]
(
	@numDomainId numeric(9,0),
	@numUserCntId numeric(9,0),
	@vcCookieId varchar(100),
	@numCartID NUMERIC(18,0),
	@numItemCode NUMERIC(18, 0),
	@numItemClassification NUMERIC(18,0),
    @numUnitHour NUMERIC(18, 0),
	@monPrice MONEY,
	@numSiteID NUMERIC(18,0)
)
AS
BEGIN
	DECLARE @monTotalAmount MONEY = @numUnitHour * @monPrice
	DECLARE @numAppliedPromotion NUMERIC(18,0) = 0
	DECLARE @numPromotionID NUMERIC(18,0)
	DECLARE @fltOfferTriggerValue FLOAT
	DECLARE @tintOfferTriggerValueType TINYINT
	DECLARE @tintOfferBasedOn TINYINT
	DECLARE @fltDiscountValue FLOAT
	DECLARE @tintDiscountType TINYINT
	DECLARE @tintDiscoutBaseOn TINYINT
	DECLARE @vcPromotionDescription VARCHAR(MAX)

	DECLARE @TEMPPromotion TABLE
	(
		ID INT IDENTITY(1,1),
		numPromotionID NUMERIC(18,0),
		fltOfferTriggerValue FLOAT,
		tintOfferTriggerValueType TINYINT,
		tintOfferBasedOn TINYINT,
		fltDiscountValue FLOAT,
		tintDiscountType TINYINT,
		tintDiscoutBaseOn TINYINT,
		vcPromotionDescription VARCHAR(MAX)
	)

	INSERT INTO 
		@TEMPPromotion
	SELECT
		PO.numProId,
		PO.fltOfferTriggerValue,
		PO.tintOfferTriggerValueType,
		PO.tintOfferBasedOn,
		PO.fltDiscountValue,
		PO.tintDiscountType,
		PO.tintDiscoutBaseOn,
		(SELECT CONCAT('Buy '
						,CASE WHEN tintOfferTriggerValueType=1 THEN CAST(fltOfferTriggerValue AS VARCHAR) ELSE CONCAT('$',fltOfferTriggerValue) END
						,CASE tintOfferBasedOn
								WHEN 1 THEN CONCAT(' of "',(SELECT Stuff((SELECT CONCAT(', ',Item.vcItemName) FROM PromotionOfferItems INNER JOIN Item ON PromotionOfferItems.numValue = Item.numItemCode WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 FOR XML PATH('')), 1, 2, '')), '"')
								WHEN 2 THEN CONCAT(' of items belonging to classification(s) "',(SELECT Stuff((SELECT CONCAT(', ',ListDetails.vcData) FROM PromotionOfferItems INNER JOIN ListDetails ON PromotionOfferItems.numValue = ListDetails.numListItemID WHERE numListID=36 AND numProId=PO.numProId AND tintRecordType=5 AND tintType=2 FOR XML PATH('')), 1, 2, '')), '"')
							END
						,' & get '
						, CASE tintDiscountType WHEN 1 THEN CONCAT(fltDiscountValue,'% off on') WHEN 2 THEN CONCAT('$',fltDiscountValue, ' off on') WHEN 3 THEN CONCAT(fltDiscountValue,' of') END
						, CASE 
							WHEN tintDiscoutBaseOn = 1 THEN 
								CONCAT
								(
									CONCAT(' "',(SELECT Stuff((SELECT CONCAT(',',Item.vcItemName) FROM PromotionOfferItems INNER JOIN Item ON PromotionOfferItems.numValue = Item.numItemCode WHERE numProId=PO.numProId AND tintRecordType=6 AND tintType=1 FOR XML PATH('')), 1, 1, '')), '"')
									,(CASE WHEN PO.tintDiscountType = 3 THEN ' free' ELSE '' END)
									,'.'
								)
							WHEN tintDiscoutBaseOn = 2 THEN 
								CONCAT
								(
									CONCAT(' items belonging to classification(s) "',(SELECT Stuff((SELECT CONCAT(',',ListDetails.vcData) FROM PromotionOfferItems INNER JOIN ListDetails ON PromotionOfferItems.numValue = ListDetails.numListItemID WHERE numListID=36 AND numProId=PO.numProId AND tintRecordType=6 AND tintType=2 FOR XML PATH('')), 1, 1, '')), '"')
									,(CASE WHEN PO.tintDiscountType = 3 THEN ' free' ELSE '' END)
									,'.'
								)
							WHEN tintDiscoutBaseOn = 3 THEN ' related items.'
							ELSE '' 
						END
		))
	FROM 
		PromotionOffer PO
	INNER JOIN
		PromotionOfferSites POS
	ON
		PO.numProId=POS.numPromotionID
		AND POS.numSiteID=@numSiteID
	WHERE
		PO.numDomainId=@numDomainId
		AND ISNULL(bitEnabled,0)=1 
		AND ISNULL(bitRequireCouponCode,0) = 0
		AND 1 = (CASE WHEN ISNULL(bitNeverExpires,0) = 1 THEN 1 ELSE (CASE WHEN GETDATE() BETWEEN dtValidFrom AND dtValidTo THEN 1 ELSE 0 END) END)
		AND 1=(CASE PO.tintOfferBasedOn
				WHEN 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems POI WHERE POI.numProId=PO.numProId AND POI.numValue=@numItemCode AND tintRecordType=5 AND tintType=1) > 0 THEN 1 ELSE 0 END)
				WHEN 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems POI WHERE POI.numProId=PO.numProId AND POI.numValue=@numItemClassification AND tintRecordType=5 AND tintType=2) > 0 THEN 1 ELSE 0 END)
				ELSE 0
			END)
	ORDER BY
		CASE 
			WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 1 THEN 1
			WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 1 THEN 2
			WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 2 THEN 3
			WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 2 THEN 4
			WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 3 THEN 5
			WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 3 THEN 6
		END

	DECLARE @i AS INT = 1
	DECLARE @Count AS INT
		
	SELECT @Count=COUNT(*) FROM @TEMPPromotion

	IF @Count > 0
	BEGIN
		WHILE @i <= @Count
		BEGIN
			SELECT 
				@numPromotionID=numPromotionID,
				@fltOfferTriggerValue = fltOfferTriggerValue,
				@tintOfferTriggerValueType=tintOfferTriggerValueType,
				@tintOfferBasedOn=tintOfferBasedOn,
				@fltDiscountValue=fltDiscountValue,
				@tintDiscountType=tintDiscountType,
				@tintDiscoutBaseOn=tintDiscoutBaseOn,
				@vcPromotionDescription=vcPromotionDescription
			FROM 
				@TEMPPromotion 
			WHERE 
				ID=@i

			IF @tintOfferBasedOn = 1 -- individual items
				AND (SELECT COUNT(*) FROm CartItems WHERE numDomainId=@numDomainId AND numUserCntId=@numUserCntId AND vcCookieId=@vcCookieId AND PromotionID=@numPromotionID AND bitParentPromotion=1) = 0
				AND 1 = (CASE 
							WHEN @tintOfferTriggerValueType=1 -- Quantity
							THEN CASE WHEN @numUnitHour >= @fltOfferTriggerValue THEN 1 ELSE 0 END
							WHEN @tintOfferTriggerValueType=2 -- Amount
							THEN CASE WHEN @monTotalAmount >= @fltOfferTriggerValue THEN 1 ELSE 0 END
							ELSE 0
						END)
			BEGIN
				UPDATE 
					CartItems 
				SET 
					PromotionID=@numPromotionID ,
					PromotionDesc=@vcPromotionDescription,
					bitParentPromotion=1
				WHERE 
					numCartId=@numCartId

				IF 1 =(CASE @tintDiscoutBaseOn
						WHEN 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems POI WHERE POI.numProId=@numPromotionID AND POI.numValue=@numItemCode AND tintRecordType=6 AND tintType=1) > 0 THEN 1 ELSE 0 END)
						WHEN 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems POI WHERE POI.numProId=@numPromotionID AND POI.numValue=@numItemClassification AND tintRecordType=6 AND tintType=2) > 0 THEN 1 ELSE 0 END)
						WHEN 3 THEN (CASE @tintOfferBasedOn
										WHEN 1 THEN (CASE WHEN (SELECT COUNT(*) FROM SimilarItems WHERE numItemCode=@numItemCode AND numParentItemCode IN (SELECT numValue FROM PromotionOfferItems POI WHERE POI.numProId=@numPromotionID AND tintRecordType=5 AND tintType=1)) > 0 THEN 1 ELSE 0 END)
										WHEN 2 THEN (CASE WHEN (SELECT COUNT(*) FROM SimilarItems WHERE numItemCode=@numItemCode AND numParentItemCode IN (SELECT numItemCode FROM Item WHERE numDomainID=@numDomainId AND numItemClassification IN (SELECT numValue FROM PromotionOfferItems POI WHERE POI.numProId=@numPromotionID AND tintRecordType=5 AND tintType=2))) > 0 THEN 1 ELSE 0 END)
										ELSE 0
									END) 
						ELSE 0
					END)
				BEGIN
					IF @tintDiscountType=1  AND @fltDiscountValue > 0 --Percentage
					BEGIN
						UPDATE 
							CartItems
						SET
							fltDiscount=@fltDiscountValue,
							bitDiscountType=0,
							monTotAmount=(@monTotalAmount-((@monTotalAmount*@fltDiscountValue)/100))
						WHERE
							numCartId=@numCartId
					END
					ELSE IF @tintDiscountType=2 AND @fltDiscountValue > 0 --Flat Amount
					BEGIN
						-- FIRST CHECK WHETHER DISCOUNT AMOUNT IS NOT ALREADY USED BY OTHER ITEMS
						DECLARE @usedDiscountAmount AS FLOAT
				
						SELECT
							@usedDiscountAmount = SUM(fltDiscount)
						FROM
							CartItems
						WHERE
							PromotionID=@numPromotionID

						IF @usedDiscountAmount < @fltDiscountValue
						BEGIN
							UPDATE 
								CartItems 
							SET 
								monTotAmount=(CASE WHEN (@fltDiscountValue - @usedDiscountAmount) >= @monTotalAmount THEN 0 ELSE (@monTotalAmount-(@fltDiscountValue - @usedDiscountAmount)) END),
								bitDiscountType=1,
								fltDiscount=(CASE WHEN (@fltDiscountValue - @usedDiscountAmount) >= @monTotalAmount THEN @monTotalAmount ELSE (@fltDiscountValue - @usedDiscountAmount) END)
							WHERE 
								numCartId=@numCartId
						END
					END
					ELSE IF @tintDiscountType=3  AND @fltDiscountValue > 0 --Quantity
					BEGIN
						-- FIRST CHECK WHETHER DISCOUNT QTY IS NOT ALREADY USED BY OTHER ITEMS
						DECLARE @usedDiscountQty AS FLOAT

						SELECT
							@usedDiscountQty = SUM(fltDiscount/monPrice)
						FROM
							CartItems
						WHERE
							PromotionID=@numPromotionID

						DECLARE @remainingDiscountValue FLOAT
						SET @remainingDiscountValue = (@fltDiscountValue - @usedDiscountQty)

						IF @usedDiscountQty < @fltDiscountValue
						BEGIN
							IF (SELECT COUNT(*) FROM CartItems WHERE numDomainId=@numDomainId AND numCartId=@numCartId AND bitParentPromotion=1 AND @tintOfferBasedOn=1) > 0
							BEGIN	
								IF @tintOfferTriggerValueType=1
								BEGIN
									IF @numUnitHour > @fltOfferTriggerValue
									BEGIN
										DECLARE @numTempUnitHour FLOAT
										SET @numTempUnitHour = @numUnitHour - @fltOfferTriggerValue

										UPDATE 
											CartItems 
										SET 
											monTotAmount=(CASE WHEN @remainingDiscountValue >= @numTempUnitHour THEN @fltOfferTriggerValue * monPrice ELSE (@monTotalAmount-(@remainingDiscountValue * @monPrice)) END),
											bitDiscountType=1,
											fltDiscount=(CASE WHEN @remainingDiscountValue >= @numTempUnitHour THEN (@numTempUnitHour * @monPrice) ELSE (@remainingDiscountValue * @monPrice) END) 
										WHERE 
											numCartId=@numCartId

										SET @remainingDiscountValue = @remainingDiscountValue - (CASE WHEN @remainingDiscountValue >= @numTempUnitHour THEN @numTempUnitHour ELSE @remainingDiscountValue END)
									END
									ELSE
									BEGIN
										UPDATE
											CartItems
										SET
											fltDiscount=0,
											monTotAmount=numUnitHour*monPrice,
											monTotAmtBefDiscount=numUnitHour*monPrice
										WHERE	
											numCartId=@numCartId
									END
								END
								ELSE IF @tintOfferTriggerValueType = 2
								BEGIN
									IF (@numUnitHour * @monPrice) > @fltOfferTriggerValue
									BEGIN
										DECLARE @discountQty AS FLOAT
										SET @discountQty = FLOOR(((@numUnitHour * @monPrice) - @fltOfferTriggerValue) / (CASE WHEN ISNULL(@monPrice,0) = 0 THEN 1 ELSE @monPrice END))

										IF @discountQty > 0
										BEGIN
											UPDATE 
												CartItems 
											SET 
												monTotAmount=(CASE WHEN @remainingDiscountValue >= @discountQty THEN (@numTempUnitHour - @discountQty) * monPrice ELSE ((numUnitHour * monPrice)-(@remainingDiscountValue * monPrice)) END),
												bitDiscountType=1,
												fltDiscount=(CASE WHEN @remainingDiscountValue >= @discountQty THEN (@discountQty * monPrice) ELSE (@remainingDiscountValue * monPrice) END) 
											WHERE 
												numCartId=@numCartId

											SET @remainingDiscountValue = @remainingDiscountValue - @discountQty
										END
										ELSE
										BEGIN
											UPDATE
												CartItems
											SET
												fltDiscount=0,
												monTotAmount=@numUnitHour*@monPrice,
												monTotAmtBefDiscount=@numUnitHour*@monPrice
											WHERE	
												numCartId=@numCartId
										END
									END
									ELSE
									BEGIN
										UPDATE
											CartItems
										SET
											fltDiscount=0,
											monTotAmount=numUnitHour*monPrice,
											monTotAmtBefDiscount=numUnitHour*monPrice
										WHERE	
											numCartId=@numCartId
									END
								END
							END
							ELSE 
							BEGIN
								UPDATE 
									CartItems 
								SET 
									monTotAmount=(CASE WHEN (@fltDiscountValue - @usedDiscountQty) >= numUnitHour THEN 0 ELSE (@monTotalAmount-((@fltDiscountValue - @usedDiscountQty) * @monPrice)) END),
									bitDiscountType=1,
									fltDiscount=(CASE WHEN (@fltDiscountValue - @usedDiscountQty) >= numUnitHour THEN (@numUnitHour*@monPrice) ELSE ((@fltDiscountValue - @usedDiscountQty) * @monPrice) END) 
								WHERE 
									numCartId=@numCartId
							END
						END
					END
				END

				SET @numAppliedPromotion = @numPromotionID
				BREAK
			END
			ELSE IF @tintOfferBasedOn = 2 -- item classifications
			BEGIN
				DECLARE @TEMPItems TABLE
				(
					ID INT,
					numCartID NUMERIC(18,0),
					numItemCode NUMERIC(18,0),
					numItemClassification NUMERIC(18,0),
					numUnitHour NUMERIC(18,0),
					monPrice NUMERIC(18,0),
					monTotalAmount NUMERIC(18,0)
				)

				INSERT INTO 
					@TEMPItems
				SELECT
					1,
					numCartId,
					CI.numItemCode,
					I.numItemClassification,
					CI.numUnitHour,
					CI.monPrice,
					(CI.numUnitHour * CI.monPrice)
				FROM
					CartItems CI
				INNER JOIN
					Item I
				ON
					CI.numItemCode = I.numItemCode
				WHERE
					numCartId=@numCartID
				UNION
				SELECT
					(ROW_NUMBER() OVER(ORDER BY numCartID ASC) + 1),
					numCartId,
					CI.numItemCode,
					I.numItemClassification,
					CI.numUnitHour,
					CI.monPrice,
					(CI.numUnitHour * CI.monPrice)
				FROM
					CartItems CI
				INNER JOIN
					Item I
				ON
					CI.numItemCode = I.numItemCode
				WHERE
					numCartId <> @numCartID
					AND CI.numDomainId=@numDomainId
					AND CI.numUserCntId=@numUserCntId
					AND CI.vcCookieId=@vcCookieId
					AND ISNULL(CI.PromotionID,0) = 0
					AND I.numItemClassification IN (SELECT numValue FROM PromotionOfferItems WHERE numProId=@numPromotionID AND tintRecordType=5 AND tintType=2)

				IF(SELECT 
						(CASE @tintOfferTriggerValueType WHEN  1 THEN SUM(numUnitHour) WHEN 2 THEN SUM(monTotalAmount) ELSE 0 END)
					FROM
						@TEMPItems) >= @fltOfferTriggerValue
				BEGIN
					DECLARE @remainingOfferValue FLOAT = @fltOfferTriggerValue
					DECLARE @numTempCartID NUMERIC(18,0)
					DECLARE @numTempItemCode NUMERIC(18,0)
					DECLARE @numTempItemClassification NUMERIC(18,0)
					DECLARE @monTempPrice NUMERIC(18,0)
					DECLARE @monTempTotalAmount NUMERIC(18,0)
					DECLARE @j AS INT = 1 
					DECLARE @jCount AS INT
					SELECT @jCount=COUNT(*) FROM @TEMPItems

					WHILE @j <= @jCount
					BEGIN
						SELECT 
							@numTempCartID=numCartID,
							@numTempItemCode=numItemCode,
							@numTempItemClassification=ISNULL(numItemClassification,0),
							@numTempUnitHour=numUnitHour,
							@monTempPrice=monPrice,
							@monTempTotalAmount = (numUnitHour * monPrice)
						FROM 
							@TEMPItems 
						WHERE 
							ID=@j

						UPDATE
							CartItems
						SET
							PromotionID=@numPromotionID,
							PromotionDesc=@vcPromotionDescription,
							fltDiscount=0,
							bitDiscountType=0,
							bitParentPromotion=1
						WHERE
							numCartId=@numCartID


						-- CHECK IF DISCOUNT FROM PROMOTION CAN ALSO BE APPLIED TO ITEM
						IF 1=(CASE @tintDiscoutBaseOn
								WHEN 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems POI WHERE POI.numProId=@numPromotionID AND POI.numValue=@numTempItemCode AND tintRecordType=6 AND tintType=1) > 0 THEN 1 ELSE 0 END)
								WHEN 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems POI WHERE POI.numProId=@numPromotionID AND POI.numValue=@numTempItemClassification AND tintRecordType=6 AND tintType=2) > 0 THEN 1 ELSE 0 END)
								WHEN 3 THEN (CASE @tintOfferBasedOn
												WHEN 1 THEN (CASE WHEN (SELECT COUNT(*) FROM SimilarItems WHERE numItemCode=@numTempItemCode AND numParentItemCode IN (SELECT numValue FROM PromotionOfferItems POI WHERE POI.numProId=@numPromotionID AND tintRecordType=5 AND tintType=1)) > 0 THEN 1 ELSE 0 END)
												WHEN 2 THEN (CASE WHEN (SELECT COUNT(*) FROM SimilarItems WHERE numItemCode=@numTempItemCode AND numParentItemCode IN (SELECT numItemCode FROM Item WHERE numDomainID=@numDomainId AND numItemClassification IN (SELECT numValue FROM PromotionOfferItems POI WHERE POI.numProId=@numPromotionID AND tintRecordType=5 AND tintType=2))) > 0 THEN 1 ELSE 0 END)
												ELSE 0
											END) 
								ELSE 0
							END)
						BEGIN
							IF @tintDiscountType=1 --Percentage
							BEGIN
								UPDATE 
									CartItems
								SET
									fltDiscount=@fltDiscountValue,
									bitDiscountType=0,
									monTotAmount=(@monTempTotalAmount-((@monTempTotalAmount*@fltDiscountValue)/100))
								WHERE
									numCartId=@numCartId
							END
							ELSE IF @tintDiscountType=2 --Flat Amount
							BEGIN
								SELECT
									@usedDiscountAmount = SUM(ISNULL(fltDiscount,0))
								FROM
									CartItems
								WHERE
									PromotionID=@numPromotionID

								IF @usedDiscountAmount < @fltDiscountValue
								BEGIN
									UPDATE 
										CartItems 
									SET 
										monTotAmount=(CASE WHEN (@fltDiscountValue - @usedDiscountAmount) >= @monTempTotalAmount THEN 0 ELSE (@monTempTotalAmount-(@fltDiscountValue - @usedDiscountAmount)) END),
										bitDiscountType=1,
										fltDiscount=(CASE WHEN (@fltDiscountValue - @usedDiscountAmount) >= @monTempTotalAmount THEN @monTempTotalAmount ELSE (@fltDiscountValue - @usedDiscountAmount) END)
									WHERE 
										numCartId=@numCartId
								END
							END
							ELSE IF @tintDiscountType=3 --Quantity
							BEGIN
								-- FIRST CHECK WHETHER DISCOUNT QTY IS NOT ALREADY USED BY OTHER ITEMS
								SELECT
									@usedDiscountQty = SUM(fltDiscount/monPrice)
								FROM
									CartItems
								WHERE
									PromotionID=@numPromotionID

								IF @usedDiscountQty < @fltDiscountValue
								BEGIN
									UPDATE 
										CartItems 
									SET 
										monTotAmount=(CASE WHEN (@fltDiscountValue - @usedDiscountQty) >= @numTempUnitHour THEN 0 ELSE (@monTempTotalAmount-((@fltDiscountValue - @usedDiscountQty) * @monTempPrice)) END),
										bitDiscountType=1,
										fltDiscount=(CASE WHEN (@fltDiscountValue - @usedDiscountQty) >= @numTempUnitHour THEN (@numTempUnitHour*@monTempPrice) ELSE ((@fltDiscountValue - @usedDiscountQty) * @monTempPrice) END) 
									WHERE 
										numCartId=@numCartId
								END
							END
						END

						SET @remainingOfferValue = @remainingOfferValue - (CASE @tintOfferTriggerValueType WHEN  1 THEN @numTempUnitHour WHEN 2 THEN @monTempTotalAmount ELSE 0 END)

						IF @remainingOfferValue <= 0
						BEGIN
							BREAK
						END

						SET @j = @j + 1
					END

					SET @numAppliedPromotion = @numPromotionID
					BREAK
				END
			END

			SET @i = @i + 1
		END
	END

	-- CHECK IF Promotion discount can be applied to other items already added to cart.
	IF ISNULL(@numAppliedPromotion,0) > 0
	BEGIN
		DELETE FROM @TEMPItems
				
		INSERT INTO 
			@TEMPItems
		SELECT
			ROW_NUMBER() OVER(ORDER BY numCartID ASC),
			numCartId,
			CI.numItemCode,
			I.numItemClassification,
			CI.numUnitHour,
			CI.monPrice,
			(CI.numUnitHour * CI.monPrice)
		FROM
			CartItems CI
		INNER JOIN
			Item I
		ON
			CI.numItemCode = I.numItemCode
		WHERE
			CI.numDomainId=@numDomainId
			AND CI.numUserCntId=@numUserCntId
			AND CI.vcCookieId=@vcCookieId
			AND ISNULL(CI.PromotionID,0) = 0
			AND 1 = (CASE @tintDiscoutBaseOn
						WHEN 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numValue=I.numItemCode AND numProId=@numAppliedPromotion AND tintRecordType=6 AND tintType=1) > 0 THEN 1 ELSE 0 END)
						WHEN 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numValue=I.numItemClassification AND numProId=@numAppliedPromotion AND tintRecordType=6 AND tintType=2) > 0 THEN 1 ELSE 0 END)
						WHEN 3 THEN (CASE @tintOfferBasedOn 
								WHEN 1 THEN (CASE WHEN (SELECT COUNT(*) FROM SimilarItems WHERE numItemCode=I.numItemCode AND numParentItemCode IN (SELECT numValue FROM PromotionOfferItems POI WHERE POI.numProId=@numAppliedPromotion AND tintRecordType=5 AND tintType=1)) > 0 THEN 1 ELSE 0 END)
								WHEN 2 THEN (CASE WHEN (SELECT COUNT(*) FROM SimilarItems WHERE numItemCode=I.numItemCode AND numParentItemCode IN (SELECT numItemCode FROM Item WHERE numDomainID=@numDomainId AND numItemClassification IN (SELECT numValue FROM PromotionOfferItems POI WHERE POI.numProId=@numAppliedPromotion AND tintRecordType=5 AND tintType=2))) > 0 THEN 1 ELSE 0 END)
								ELSE 0
							END) 
						ELSE 0
					END)

		DECLARE @k AS INT = 1
		DECLARE @kCount AS INT 

		SELECT @kCount = COUNT(*) FROM @TEMPItems
		
		IF @kCount > 0
		BEGIN
			WHILE @k <= @kCount
			BEGIN
				SELECT 
					@numTempCartID=numCartID,
					@numTempItemCode=numItemCode,
					@numTempItemClassification=ISNULL(numItemClassification,0),
					@numTempUnitHour=numUnitHour,
					@monTempPrice=monPrice,
					@monTempTotalAmount = (numUnitHour * monPrice)
				FROM 
					@TEMPItems 
				WHERE 
					ID=@k


				IF @tintDiscountType=1 --Percentage
				BEGIN
					UPDATE 
						CartItems
					SET
						PromotionID=@numPromotionID,
						PromotionDesc=@vcPromotionDescription,
						bitParentPromotion=0,
						fltDiscount=@fltDiscountValue,
						bitDiscountType=0,
						monTotAmount=(@monTempTotalAmount-((@monTempTotalAmount*@fltDiscountValue)/100))
					WHERE
						numCartId=@numTempCartID
				END
				ELSE IF @tintDiscountType=2 --Flat Amount
				BEGIN
					-- FIRST CHECK WHETHER DISCOUNT AMOUNT IS NOT ALREADY USED BY OTHER ITEMS
					SELECT
						@usedDiscountAmount = SUM(ISNULL(fltDiscount,0))
					FROM
						CartItems
					WHERE
						PromotionID=@numAppliedPromotion

					IF @usedDiscountAmount < @fltDiscountValue
					BEGIN
						UPDATE 
							CartItems 
						SET 
							monTotAmount=(CASE WHEN (@fltDiscountValue - @usedDiscountAmount)  >= @monTempTotalAmount THEN 0 ELSE (@monTempTotalAmount-(@fltDiscountValue - @usedDiscountAmount)) END),
							PromotionID=@numPromotionID ,
							PromotionDesc=@vcPromotionDescription,
							bitDiscountType=1,
							fltDiscount=(CASE WHEN (@fltDiscountValue - @usedDiscountAmount) >= @monTempTotalAmount THEN @monTempTotalAmount ELSE (@fltDiscountValue - @usedDiscountAmount) END)
						WHERE 
							numCartId=@numTempCartID
					END
					ELSE
					BEGIN
						BREAK
					END
				END
				ELSE IF @tintDiscountType=3 --Quantity
				BEGIN
					-- FIRST CHECK WHETHER DISCOUNT QTY IS NOT ALREADY USED BY OTHER ITEMS
					SELECT
						@usedDiscountQty = SUM(ISNULL(fltDiscount,0)/ISNULL(monPrice,1))
					FROM
						CartItems
					WHERE
						PromotionID=@numAppliedPromotion

					IF @usedDiscountQty < @fltDiscountValue
					BEGIN
						UPDATE 
							CartItems 
						SET 
							monTotAmount=(CASE WHEN (@fltDiscountValue - @usedDiscountQty)  >= numUnitHour THEN 0 ELSE (@monTempTotalAmount-((@fltDiscountValue - @usedDiscountQty) * @monTempPrice)) END),
							PromotionID=@numPromotionID ,
							PromotionDesc=@vcPromotionDescription,
							bitDiscountType=1,
							fltDiscount=(CASE WHEN (@fltDiscountValue - @usedDiscountQty) >= numUnitHour THEN @monTempTotalAmount ELSE ((@fltDiscountValue - @usedDiscountQty) * @monTempPrice) END) 
						WHERE 
							numCartId=@numTempCartID
					END
					ELSE
					BEGIN
						BREAK
					END
				END

				SET @k = @k + 1
			END
		END
	END
END

/****** Object:  StoredProcedure [dbo].[USP_GetCartItem]    Script Date: 11/08/2011 18:00:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetCartItem' ) 
                    DROP PROCEDURE USP_GetCartItem
                    Go
CREATE PROCEDURE [dbo].[USP_GetCartItem]
(
		 @numUserCntId numeric(18,0),
		 @numDomainId numeric(18,0),
		 @vcCookieId varchar(100),
		 @bitUserType BIT =0--if 0 then anonomyous user 1 for logi user
)
AS
	IF @numUserCntId <> 0
	BEGIN
		SELECT DISTINCT numCartId,
			   numUserCntId,
			   [dbo].[CartItems].numDomainId,
			   vcCookieId,
			   numOppItemCode,
			   [dbo].[CartItems].numItemCode,
			   (SELECT TOP 1 numCategoryId  FROM dbo.ItemCategory WHERE numItemID =  CartItems.numItemCode AND [dbo].[CartItems].numDomainId = @numDomainId ) AS numCategoryId,
			   (SELECT vcCategoryName FROM [dbo].[Category] WHERE [Category].[numCategoryID]  = (SELECT TOP 1 numCategoryId  FROM dbo.ItemCategory WHERE numItemID =  CartItems.numItemCode AND [dbo].[CartItems].numDomainId = @numDomainId )) AS vcCategoryName,
			   numUnitHour,
			   monPrice,
			   monPrice AS [monListPrice],
			   numSourceId,
			   vcItemDesc,
			   vcItemDesc AS txtItemDesc,
			   [dbo].[CartItems].numWarehouseId,
			   [dbo].[CartItems].vcItemName,
			   vcWarehouse,
			   numWarehouseItmsID,
			   vcItemType,
			   vcAttributes,
			   vcAttrValues,
			   I.bitFreeShipping,
			   fltWeight as numWeight,
			   ISNULL((SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numDomainId=@numDomainId AND bitDefault=1 AND numItemCode=I.numItemCode),'') AS vcPathForTImage,
			   tintOpFlag,
			   bitDiscountType,
			   fltDiscount,
			   monTotAmtBefDiscount,
			   ItemURL,
			   numUOM,
			   vcUOMName,
			   decUOMConversionFactor,
			   numHeight,
			   numLength,
			   numWidth,
			   vcShippingMethod,
			   numServiceTypeId,
			   decShippingCharge,
			   numShippingCompany,
			   vcCoupon,
			   tintServicetype,CAST( dtDeliveryDate AS VARCHAR(30)) dtDeliveryDate,monTotAmount,
			   ISNULL(monTotAmtBefDiscount,0) - ISNULL(monTotAmount,0) AS monTotalDiscountAmount,
			   (SELECT vcModelID FROM dbo.Item WHERE Item.numItemCode =  CartItems.numItemCode) AS [vcModelID]
			   ,(SELECT TOP 1 vcPathForTImage FROM dbo.ItemImages II WHERE II.numItemCode = CartItems.numItemCode AND II.numDomainId= CartItems.numDomainId AND II.bitDefault=1) As vcPathForTImage
			   , ISNULL(vcUpSellDesc,'') AS [txtUpSellItemDesc],ISNULL(bitPreUpSell,0) [bitPreUpSell], ISNULL(bitPostUpSell,0) [bitPostUpSell], ISNULL(vcUpSellDesc,'') [vcUpSellDesc]
			   ,ISNULL([I].[vcSKU],'') AS [vcSKU]
			   ,I.vcManufacturer
			   ,ISNULL(PromotionID,0) AS PromotionID
			   ,CAST(ISNULL(PromotionDesc,'') AS varchar) AS PromotionDesc
			   FROM CartItems 
			   LEFT JOIN [dbo].[SimilarItems] AS SI ON SI.[numItemCode] = [dbo].[CartItems].[numItemCode]
			   LEFT JOIN [dbo].[Item] AS I ON I.[numItemCode] = [dbo].[CartItems].[numItemCode]
			   --LEFT JOIN [dbo].[WareHouseItems] AS WHI ON WHI.[numItemID] = I.[numItemCode]
			   WHERE [dbo].[CartItems].numDomainId = @numDomainId AND numUserCntId = @numUserCntId 	
	END
	ELSE
	BEGIN
		SELECT DISTINCT numCartId,
			   numUserCntId,
			   [dbo].[CartItems].numDomainId,
			   vcCookieId,
			   numOppItemCode,
			   [dbo].[CartItems].numItemCode,
			   (SELECT TOP 1 numCategoryId  FROM dbo.ItemCategory WHERE numItemID =  CartItems.numItemCode AND CartItems.numDomainId =@numDomainId ) AS numCategoryId,
			   (SELECT vcCategoryName FROM [dbo].[Category] WHERE [Category].[numCategoryID]  = (SELECT TOP 1 numCategoryId  FROM dbo.ItemCategory WHERE numItemID =  CartItems.numItemCode AND [dbo].[CartItems].numDomainId = @numDomainId )) AS vcCategoryName,
			   numUnitHour,
			   monPrice,
			   monPrice AS [monListPrice],
			   numSourceId,
			   vcItemDesc,
			   vcItemDesc AS txtItemDesc,
			   [dbo].[CartItems].numWarehouseId,
			   [dbo].[CartItems].vcItemName,
			   vcWarehouse,
			   numWarehouseItmsID,
			   vcItemType,
			   vcAttributes,
			   vcAttrValues,
			   I.bitFreeShipping,
			   fltWeight as numWeight,
			   ISNULL((SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numDomainId=@numDomainId AND bitDefault=1 AND numItemCode=I.numItemCode),'') AS vcPathForTImage,
			   tintOpFlag,
			   bitDiscountType,
			   fltDiscount,
			   monTotAmtBefDiscount,
			   ItemURL,
			   numUOM,
			   vcUOMName,
			   decUOMConversionFactor,
			   numHeight,
			   numLength,
			   numWidth,
			   vcShippingMethod,
			   numServiceTypeId,
			   decShippingCharge,
			   numShippingCompany,
			   tintServicetype,
			   vcCoupon,
			   CAST( dtDeliveryDate AS VARCHAR(30)) dtDeliveryDate,
			   monTotAmount,
			   ISNULL(monTotAmtBefDiscount,0) - ISNULL(monTotAmount,0) AS monTotalDiscountAmount,
			   (SELECT vcModelID FROM dbo.Item WHERE Item.numItemCode =  CartItems.numItemCode) AS [vcModelID]
			   ,(SELECT TOP 1 vcPathForTImage FROM dbo.ItemImages II WHERE II.numItemCode = CartItems.numItemCode AND II.numDomainId= CartItems.numDomainId AND II.bitDefault=1) As vcPathForTImage
			   , ISNULL(vcUpSellDesc,'') AS [txtUpSellItemDesc],ISNULL(bitPreUpSell,0) [bitPreUpSell], ISNULL(bitPostUpSell,0) [bitPostUpSell], ISNULL(vcUpSellDesc,'') [vcUpSellDesc]
			   , ISNULL(vcUpSellDesc,'') AS [txtUpSellItemDesc],ISNULL(bitPreUpSell,0) [bitPreUpSell], ISNULL(bitPostUpSell,0) [bitPostUpSell], ISNULL(vcUpSellDesc,'') [vcUpSellDesc]
			   ,ISNULL([I].[vcSKU],'') AS [vcSKU]
			   ,I.vcManufacturer
			   ,ISNULL(PromotionID,0) AS PromotionID
			   ,CAST(ISNULL(PromotionDesc,'') AS VARCHAR) AS PromotionDesc
			   FROM CartItems
			   LEFT JOIN [dbo].[SimilarItems] AS SI ON SI.[numItemCode] = [dbo].[CartItems].[numItemCode]
			   LEFT JOIN [dbo].[Item] AS I ON I.[numItemCode] = [dbo].[CartItems].[numItemCode]
			   --LEFT JOIN [dbo].[WareHouseItems] AS WHI ON WHI.[numItemID] = I.[numItemCode]
			   WHERE [dbo].[CartItems].numDomainId = @numDomainId AND numUserCntId = @numUserCntId AND vcCookieId =@vcCookieId	
	END
	
	

--exec USP_GetCartItem @numUserCntId=1,@numDomainId=1,@vcCookieId='fc3d604b-25fa-4c25-960c-1e317768113e',@bitUserType=NULL


/****** Object:  StoredProcedure [dbo].[USP_GetDealsList1]    Script Date: 05/07/2009 22:12:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getdealslist1')
DROP PROCEDURE usp_getdealslist1
GO
CREATE PROCEDURE [dbo].[USP_GetDealsList1]
               @numUserCntID              NUMERIC(9)  = 0,
               @numDomainID               NUMERIC(9)  = 0,
               @tintSalesUserRightType    TINYINT  = 0,
               @tintPurchaseUserRightType TINYINT  = 0,
               @tintSortOrder             TINYINT  = 4,
               @CurrentPage               INT,
               @PageSize                  INT,
               @TotRecs                   INT  OUTPUT,
               @columnName                AS VARCHAR(50),
               @columnSortOrder           AS VARCHAR(10),
               @numCompanyID              AS NUMERIC(9)  = 0,
               @bitPartner                AS BIT  = 0,
               @ClientTimeZoneOffset      AS INT,
               @tintShipped               AS TINYINT,
               @intOrder                  AS TINYINT,
               @intType                   AS TINYINT,
               @tintFilterBy              AS TINYINT  = 0,
               @numOrderStatus			  AS NUMERIC,
			   @vcRegularSearchCriteria varchar(1000)='',
			   @vcCustomSearchCriteria varchar(1000)='',
			   @SearchText VARCHAR(300) = '',
			   @SortChar char(1)='0'  
AS
	DECLARE  @PageId  AS TINYINT
	DECLARE  @numFormId  AS INT 
  
	SET @PageId = 0

	IF @inttype = 1
	BEGIN
		SET @PageId = 2
		SET @inttype = 3
		SET @numFormId=39
	END
	ELSE IF @inttype = 2
	BEGIN
		SET @PageId = 6
		SET @inttype = 4
		SET @numFormId=41
	END

	CREATE TABLE #tempForm 
	(
		tintOrder TINYINT,vcDbColumnName nvarchar(50),vcOrigDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),vcListItemType varchar(3),
		numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,
		bitIsAlphaNumeric BIT,bitIsNumeric BIT,bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),
		ListRelID numeric(9),intColumnWidth int,bitAllowFiltering BIT,vcFieldDataType CHAR(1)
	)

	-- FIRST CHECK IF GRID COLUMNS ARE CONFIGURED
	DECLARE  @Nocolumns  AS TINYINT = 0

	SELECT 
		@Nocolumns=ISNULL(SUM(TotalRow),0) 
	FROM
	(            
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND numRelCntType = @numFormId
		UNION 
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicCustomColumns 
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND numRelCntType = @numFormId
	) TotalRows

	-- IF GRID COLUMNS ARE NOT CONFIGURES THEN CHECK IF MASTER FORM CONFIGURATION IS CREATED BY ADMINISTRATOR
	DECLARE @IsMasterConfAvailable BIT = 0
	DECLARE @numUserGroup NUMERIC(18,0)

	IF @Nocolumns=0
	BEGIN
		SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

		IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = @numFormId AND numRelCntType=@numFormId AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
		BEGIN
			SET @IsMasterConfAvailable = 1
		END
	END

	--IF MASTERCONFIGURATION IS AVAILABLE THEN LOAD IT OTHERWISE LOAD DEFAULT COLUMNS
	IF @IsMasterConfAvailable = 1
	BEGIN
		INSERT INTO 
			#tempForm
		SELECT 
			(intRowNum + 1) as tintOrder, vcDbColumnName,	vcOrigDbColumnName,	ISNULL(vcCultureFieldName,vcFieldName), vcAssociatedControlType,
			vcListItemType, numListID, vcLookBackTableName,	bitCustom, numFieldId, bitAllowSorting,	bitInlineEdit, bitIsRequired,
			bitIsEmail,	bitIsAlphaNumeric, bitIsNumeric, bitIsLengthValidation,	intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,
			ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType 
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.numRelCntType=@numFormId AND 
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName, vcAssociatedControlType,'' as vcListItemType,numListID,'',
			bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,0,''
		FROM 
			View_DynamicCustomColumnsMasterConfig
		WHERE 
			View_DynamicCustomColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.numRelCntType=@numFormId AND 
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1
		ORDER BY 
			tintOrder ASC  
	END
	ELSE                                        
	BEGIN
		IF @Nocolumns=0
		BEGIN
			INSERT INTO DycFormConfigurationDetails 
			(
				numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
			)
			SELECT 
				@numFormId,numFieldId,0,Row_number() over(order by tintRow desc),@numDomainID,@numUserCntID,@numFormId,1,0,intColumnWidth
			FROM 
				View_DynamicDefaultColumns
			WHERE 
				numFormId=@numFormId 
				AND bitDefault=1 
				AND ISNULL(bitSettingField,0)=1 
				AND numDomainID=@numDomainID
			ORDER BY 
				tintOrder asc   
		END

		INSERT INTO 
			#tempForm
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
			vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName,bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,bitFieldMessage,
			vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType  
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND ISNULL(bitSettingField,0)=1 
			AND ISNULL(bitCustom,0)=0  
			AND numRelCntType = @numFormId
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName,vcAssociatedControlType,'' as vcListItemType,numListID,'',bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,0,'' 
		FROM 
			View_DynamicCustomColumns
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND ISNULL(bitCustom,0)=1 
			AND numRelCntType = @numFormId
		ORDER BY 
			tintOrder asc  
	END

	DECLARE  @strColumns AS VARCHAR(MAX)
	SET @strColumns = ' ADC.numContactId, Div.numDivisionID,ISNULL(Div.numTerID,0) numTerID, opp.numRecOwner as numRecOwner, Div.tintCRMType, opp.numOppId, opp.monDealAmount, ISNULL(opp.intUsedShippingCompany,0) AS intUsedShippingCompany'


	DECLARE  @tintOrder  AS TINYINT;SET @tintOrder = 0
	DECLARE  @vcFieldName  AS VARCHAR(50)
	DECLARE  @vcListItemType  AS VARCHAR(3)
	DECLARE  @vcListItemType1  AS VARCHAR(1)
	DECLARE  @vcAssociatedControlType VARCHAR(30)
	DECLARE  @numListID  AS NUMERIC(9)
	DECLARE  @vcDbColumnName VARCHAR(40)
	DECLARE  @WhereCondition VARCHAR(2000);SET @WhereCondition = ''
	DECLARE  @vcLookBackTableName VARCHAR(2000)
	DECLARE  @bitCustom  AS BIT
	Declare @numFieldId as numeric  
	DECLARE @bitAllowSorting AS CHAR(1)   
	DECLARE @bitAllowEdit AS CHAR(1)                 
	DECLARE @vcColumnName AS VARCHAR(500)          
	
	Declare @ListRelID as numeric(9) 
	DECLARE @SearchQuery AS VARCHAR(MAX) = ''        

	SELECT TOP 1 
		@tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,@vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,
		@numListID=numListID,@vcLookBackTableName=vcLookBackTableName ,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,
		@ListRelID=ListRelID
	FROM 
		#tempForm 
	ORDER BY 
		tintOrder ASC            
--	SET @strColumns = @strColumns + ' , (SELECT SUBSTRING((SELECT '', '' + cast(I.vcItemName as varchar)
--                      FROM OpportunityItems AS t
--					  LEFT JOIN OpportunityBizDocs as BC
--					  ON t.numOppId=BC.numOppId
--LEFT JOIN Item as I
--ON t.numItemCode=I.numItemCode
--WHERE t.numOppId=Opp.numOppId AND t.numoppItemtCode NOT IN (SELECT numOppItemId FROM OpportunityBizDocItems WHERE numOppBizDocID=BC.numOppBizDocsId)
--                     FOR XML PATH('''')), 2, 200000)  )[List_Item_Approval_UNIT] '
--SET @strColumns = @strColumns + ' , (SELECT 
-- SUBSTRING((SELECT '', '' + cast(I.vcItemName as varchar)
--FROM 
-- OpportunityItems AS t
--LEFT JOIN 
-- Item as I
--ON 
-- t.numItemCode=I.numItemCode
--WHERE 
-- t.numOppId=Opp.numOppId AND t.numoppItemtCode NOT IN (SELECT numOppItemId FROM OpportunityBizDocs JOIN OpportunityBizDocItems ON OpportunityBizDocs.numOppBizDocsId=OpportunityBizDocItems.numOppBizDocID  WHERE OpportunityBizDocs.numOppId=Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1)
--                     FOR XML PATH('''')), 2, 200000)  )[List_Item_Approval_UNIT] '
	WHILE @tintOrder > 0
    BEGIN
		IF @bitCustom = 0
		BEGIN
			DECLARE  @Prefix  AS VARCHAR(5)

			IF @vcLookBackTableName = 'AdditionalContactsInformation'
				SET @Prefix = 'ADC.'
			IF @vcLookBackTableName = 'DivisionMaster'
				SET @Prefix = 'Div.'
			IF @vcLookBackTableName = 'OpportunityMaster'
				SET @PreFix = 'Opp.'
			IF @vcLookBackTableName = 'OpportunityRecurring'
				SET @PreFix = 'OPR.'
			IF @vcLookBackTableName = 'CompanyInfo'
				SET @PreFix = 'cmp.'
			IF @vcLookBackTableName = 'ProjectProgress'
				SET @PreFix = 'PP'
			SET @vcColumnName= @vcDbColumnName+'~'+ convert(varchar(10),@numFieldId) +'~0'

			IF @vcAssociatedControlType = 'SelectBox'
			BEGIN
				IF @vcDbColumnName = 'numPartner'
				BEGIN
					SET @strColumns=@strColumns+ ' ,(Opp.numPartner) '+' ['+ @vcColumnName+']'    
					SET @strColumns=@strColumns+ ' ,(SELECT D.vcPartnerCode+''-''+C.vcCompanyName FROM DivisionMaster AS D LEFT JOIN CompanyInfo AS C ON D.numCompanyID=C.numCompanyID WHERE D.numDivisionID=Opp.numPartner) AS vcPartner' 
				END
				IF @vcDbColumnName = 'numStatus'
				BEGIN
					SET @strColumns=@strColumns+ ' ,(SELECT COUNT(OpportunityItems.numoppitemtCode) FROM OpportunityItems 
WHERE OpportunityItems.bitItemPriceApprovalRequired=1 AND OpportunityItems.numOppId=Opp.numOppId) ApprovalMarginCount'
				END
				IF @vcDbColumnName = 'tintSource'
				BEGIN
					SET @strColumns = @strColumns + ',ISNULL(dbo.fn_GetOpportunitySourceValue(ISNULL(Opp.tintSource,0),ISNULL(Opp.tintSourceType,0),Opp.numDomainID),''Internal Order'')' + ' [' + @vcColumnName + ']'

					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'ISNULL(dbo.fn_GetOpportunitySourceValue(ISNULL(Opp.tintSource,0),ISNULL(Opp.tintSourceType,0),Opp.numDomainID),''Internal Order'') LIKE ''%' + @SearchText + '%'''
					END
				END
				ELSE IF @vcDbColumnName = 'numCampainID'
				BEGIN
					SET @strColumns = @strColumns + ', (SELECT vcCampaignName FROM CampaignMaster WHERE numCampaignID= Opp.numCampainID) ' + ' [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'ISNULL((SELECT vcCampaignName FROM CampaignMaster WHERE numCampaignID= Opp.numCampainID),'''') LIKE ''%' + @SearchText + '%'''
					END
				END
				ELSE IF @vcListItemType = 'LI'
				BEGIN
					SET @strColumns = @strColumns + ',L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData' + ' [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData LIKE ''%' + @SearchText + '%'''
					END

					SET @WhereCondition = @WhereCondition + ' left Join ListDetails L' + CONVERT(VARCHAR(3),@tintOrder) + ' on L' + CONVERT(VARCHAR(3),@tintOrder) + '.numListItemID=' + @vcDbColumnName
				END
				ELSE IF @vcListItemType = 'S'
				BEGIN
					SET @strColumns = @strColumns + ',S' + CONVERT(VARCHAR(3),@tintOrder) + '.vcState' + ' [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'S' + CONVERT(VARCHAR(3),@tintOrder) + '.vcState LIKE ''%' + @SearchText + '%'''
					END

					SET @WhereCondition = @WhereCondition + ' left join State S' + CONVERT(VARCHAR(3),@tintOrder) + ' on S' + CONVERT(VARCHAR(3),@tintOrder) + '.numStateID=' + @vcDbColumnName
				END
				ELSE IF @vcListItemType = 'BP'
				BEGIN
					SET @strColumns = @strColumns + ',SPLM.Slp_Name' + ' [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'SPLM.Slp_Name LIKE ''%' + @SearchText + '%'''
					END

					SET @WhereCondition = @WhereCondition + ' LEFT JOIN Sales_process_List_Master SPLM on SPLM.Slp_Id = OPP.numBusinessProcessID '
				END
				ELSE IF @vcListItemType = 'PP'
				BEGIN
					SET @strColumns = @strColumns + ',ISNULL(PP.intTotalProgress,0)' + ' [' + @vcColumnName + ']'

					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'ISNULL(PP.intTotalProgress,0) LIKE ''%' + @SearchText + '%'''
					END
				END
				ELSE IF @vcListItemType = 'T'
				BEGIN
					SET @strColumns = @strColumns + ',L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData' + ' [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData LIKE ''%' + @SearchText + '%'''
					END

					SET @WhereCondition = @WhereCondition + ' left Join ListDetails L' + CONVERT(VARCHAR(3),@tintOrder) + ' on L' + CONVERT(VARCHAR(3),@tintOrder) + '.numListItemID=' + @vcDbColumnName
				END
				ELSE IF @vcListItemType = 'U'
				BEGIN
					SET @strColumns = @strColumns + ',dbo.fn_GetContactName(' + @Prefix + @vcDbColumnName + ') [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'dbo.fn_GetContactName(' + @Prefix + @vcDbColumnName + ') LIKE ''%' + @SearchText + '%'''
					END
				END
				ELSE IF @vcListItemType = 'WI'
				BEGIN
					SET @strColumns = @strColumns + ', Case when isnull(OL.numSiteID,0)>0 then Sites.vcSiteName else WebAPI.vcProviderName end [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + '(Case when isnull(OL.numSiteID,0)>0 then Sites.vcSiteName else WebAPI.vcProviderName end) LIKE ''%' + @SearchText + '%'''
					END

					set @WhereCondition= @WhereCondition +' left JOIN OpportunityLinking OL on OL.numChildOppID=Opp.numOppId left join WebAPI on WebAPI.WebApiId=isnull(OL.numWebApiId,0)
													left join Sites on isnull(OL.numSiteID,0)=Sites.numSIteID and Sites.numDomainID=' + convert(varchar(15),@numDomainID)                                                       
				END 
            END
			ELSE IF @vcAssociatedControlType = 'DateField'
			BEGIN
				IF @Prefix ='OPR.'
				BEGIN
					SET @strColumns = @strColumns + ', (SELECT TOP 1 dbo.FormatedDateFromDate( DateAdd(minute,+'+convert(varchar(15),-@ClientTimeZoneOffset)+', dtRecurringDate)'+ ','+ CONVERT(VARCHAR(10),@numDomainId) +') FROM  dbo.OpportunityRecurring WHERE numOppID=OPP.numOppID AND ISNULL(numOppBizDocID,0)=0 and dtRecurringDate > dbo.GetUTCDateWithoutTime()) AS  ['+ @vcColumnName + ']'

					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + '(SELECT TOP 1 dbo.FormatedDateFromDate( DateAdd(minute,+'+convert(varchar(15),-@ClientTimeZoneOffset)+', dtRecurringDate)'+ ','+ CONVERT(VARCHAR(10),@numDomainId) +') FROM  dbo.OpportunityRecurring WHERE numOppID=OPP.numOppID AND ISNULL(numOppBizDocID,0)=0 and dtRecurringDate > dbo.GetUTCDateWithoutTime()) LIKE ''%' + @SearchText + '%'''
					END
				END
				ELSE IF @vcDbColumnName = 'dtReleaseDate'
				BEGIN
					SET @strColumns = @strColumns
                                + ',case when convert(varchar(11),'
                                + @Prefix
                                + @vcDbColumnName
                                + ')= convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getdate())) then ''<b><font color=red>Today</font></b>'''
					SET @strColumns = @strColumns
									+ 'when convert(varchar(11),'
									+ @Prefix
									+ @vcDbColumnName
									+ ')= convert(varchar(11),dateadd(day,-1,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getdate()))) then ''<b><font color=purple>Yesterday</font></b>'''
					SET @strColumns = @strColumns
									+ 'when convert(varchar(11),'
									+ @Prefix
									+ @vcDbColumnName
									+ ')= convert(varchar(11),dateadd(day,1,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getdate()))) then''<b><font color=orange>Tommorow</font></b>'' '
					SET @strColumns = @strColumns
									+ 'else dbo.FormatedDateFromDate('
									+ @Prefix
									+ @vcDbColumnName
									+ ','
									+ CONVERT(VARCHAR(10),@numDomainId)
									+ ') end  ['
									+ @vcColumnName + ']' 	

					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + @Prefix + @vcDbColumnName + ' LIKE ''%' + @SearchText + '%'''
					END

				END
				ELSE
				BEGIN
					SET @strColumns = @strColumns
                                + ',case when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','
                                + @Prefix
                                + @vcDbColumnName
                                + ') )= convert(varchar(11),getdate()) then ''<b><font color=red>Today</font></b>'''
					SET @strColumns = @strColumns
									+ 'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','
									+ @Prefix
									+ @vcDbColumnName
									+ ') )= convert(varchar(11),dateadd(day,-1,getdate())) then ''<b><font color=purple>Yesterday</font></b>'''
					SET @strColumns = @strColumns
									+ 'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','
									+ @Prefix
									+ @vcDbColumnName
									+ ') )= convert(varchar(11),dateadd(day,1,getdate())) then''<b><font color=orange>Tommorow</font></b>'' '
					SET @strColumns = @strColumns
									+ 'else dbo.FormatedDateFromDate('
									+ @Prefix
									+ @vcDbColumnName
									+ ','
									+ CONVERT(VARCHAR(10),@numDomainId)
									+ ') end  ['
									+ @vcColumnName + ']' 	

					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + @Prefix + @vcDbColumnName + ' LIKE ''%' + @SearchText + '%'''
					END
				 END	
            END
            ELSE
            IF @vcAssociatedControlType = 'TextBox'
            BEGIN
				
                SET @strColumns = @strColumns
                                  + ','
                                  + CASE 
                                      WHEN @vcDbColumnName = 'numAge' THEN 'year(getutcdate())-year(bintDOB)'
                                      WHEN @vcDbColumnName = 'monPAmount' THEN 'isnull((SELECT varCurrSymbol FROM [Currency] WHERE numCurrencyID = Opp.[numCurrencyID]),'''') + '' ''+ CONVERT(VARCHAR,CONVERT(MONEY,monPAmount),1)'
                                      WHEN @vcDbColumnName = 'numDivisionID' THEN 'Div.numDivisionID'
                                      WHEN @vcDbColumnName = 'tintOppStatus' THEN 'CASE tintOppStatus WHEN 0 then ''Open'' WHEN 1 THEN ''Closed-Won'' WHEN 2 THEN ''Closed-Lost'' END '
                                      WHEN @vcDbColumnName = 'tintOppType' THEN 'CASE tintOppType WHEN 1 then ''Sales'' WHEN 2 THEN ''Purchase'' Else ''NA'' END '
                                      WHEN @vcDbColumnName = 'vcPOppName' THEN 'Opp.vcPOppName'
									  WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'vcBizDocsList' THEN 'ISNULL((SELECT SUBSTRING((SELECT ''$^$'' + CAST(numOppBizDocsId AS VARCHAR(18)) +''#^#''+ vcBizDocID
										FROM OpportunityBizDocs WHERE numOppId=Opp.numOppId FOR XML PATH('''')),4,200000)),'''')'
                                      ELSE @Prefix + @vcDbColumnName
                                    END
                                  + ' ['
                                  + @vcColumnName + ']'
				IF(@vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'vcBizDocsList')
				BEGIN
					SET @strColumns=@strColumns+','+' (SELECT COUNT(I.vcItemName) FROM 
													 OpportunityItems AS t LEFT JOIN  Item as I ON 
													 t.numItemCode=I.numItemCode WHERE 
													 t.numOppId=Opp.numOppId AND t.numoppItemtCode NOT IN 
													 (SELECT numOppItemId FROM OpportunityBizDocs JOIN 
													 OpportunityBizDocItems ON OpportunityBizDocs.numOppBizDocsId=OpportunityBizDocItems.numOppBizDocID  
													 WHERE OpportunityBizDocs.numOppId=Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1)) AS [List_Item_Approval_UNIT] '
				END
				
				IF LEN(@SearchText) > 0
				BEGIN
					SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + (CASE 
                                      WHEN @vcDbColumnName = 'numAge' THEN 'year(getutcdate())-year(bintDOB)'
                                      WHEN @vcDbColumnName = 'monPAmount' THEN 'isnull((SELECT varCurrSymbol FROM [Currency] WHERE numCurrencyID = Opp.[numCurrencyID]),'''') + '' ''+ CONVERT(VARCHAR,CONVERT(MONEY,monPAmount),1)'
                                      WHEN @vcDbColumnName = 'numDivisionID' THEN 'Div.numDivisionID'
                                      WHEN @vcDbColumnName = 'tintOppStatus' THEN 'CASE tintOppStatus WHEN 0 then ''Open'' WHEN 1 THEN ''Closed-Won'' WHEN 2 THEN ''Closed-Lost'' END '
                                      WHEN @vcDbColumnName = 'tintOppType' THEN 'CASE tintOppType WHEN 1 then ''Sales'' WHEN 2 THEN ''Purchase'' Else ''NA'' END '
                                      WHEN @vcDbColumnName = 'vcPOppName' THEN 'Opp.vcPOppName'
									  WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'vcBizDocsList' THEN 'ISNULL((SELECT SUBSTRING((SELECT ''$^$'' + CAST(numOppBizDocsId AS VARCHAR(18)) +''#^#''+ vcBizDocID
										FROM OpportunityBizDocs WHERE numOppId=Opp.numOppId FOR XML PATH('''')),4,200000)),'''')'
                                      ELSE @Prefix + @vcDbColumnName
                                    END) + ' LIKE ''%' + @SearchText + '%'''
				END
            END
            ELSE
            IF @vcAssociatedControlType = 'TextArea'
            BEGIN
				SET @strColumns=@strColumns+','+  @Prefix + @vcDbColumnName +' ['+ @vcColumnName+']' 

				IF LEN(@SearchText) > 0
				BEGIN
					SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + @Prefix + @vcDbColumnName + ' LIKE ''%' + @SearchText + '%'''
				END
            END
            ELSE IF  @vcAssociatedControlType='Label'                                              
			BEGIN
				SET @strColumns=@strColumns + ',' + CASE                    
				WHEN @vcDbColumnName = 'CalAmount' THEN 'Opp.monDealAmount'--'[dbo].[getdealamount](Opp.numOppId,Getutcdate(),0)'
				WHEN @vcDbColumnName = 'vcInventoryStatus' THEN 'ISNULL(dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID),'''')'
				WHEN @vcDbColumnName = 'vcOrderedShipped' THEN 'dbo.GetOrderShipReceiveStatus(Opp.numOppId,1,Opp.tintshipped)'
				WHEN @vcDbColumnName = 'vcOrderedReceived' THEN 'dbo.GetOrderShipReceiveStatus(Opp.numOppId,2,Opp.tintshipped)'
				WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monDealAmount' THEN 'ISNULL((SELECT SUM(ISNULL(monDealAmount,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
				WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monAmountPaid' THEN 'ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
				else @Prefix + @vcDbColumnName END +' ['+ @vcColumnName+']'        
				
				IF @vcDbColumnName='vcOrderedShipped'
				BEGIN
				DECLARE @temp VARCHAR(MAX)
				SET @temp = '(SELECT 
				STUFF((SELECT '', '' + CAST((CAST(numOppBizDocsId AS varchar)+''~''+CAST(ISNULL(numShippingReportId,0) AS varchar) ) AS VARCHAR(MAX)) [text()]
				FROM OpportunityBizDocs
				LEFT JOIN ShippingReport
				ON ShippingReport.numOppBizDocId=OpportunityBizDocs.numOppBizDocsId
				WHERE OpportunityBizDocs.numOppId=Opp.numOppId  AND OpportunityBizDocs.bitShippingGenerated=1  FOR XML PATH(''''), TYPE)
						.value(''.'',''NVARCHAR(MAX)''),1,2,'' ''))'
					SET @strColumns = @strColumns + ' , (SELECT SUBSTRING('+ @temp +', 2, 200000))AS ShippingIcons '
					SET @strColumns = @strColumns + ' , CASE WHEN (SELECT COUNT(*) FROM dbo.ShippingBox 
			 WHERE numShippingReportId IN (SELECT numShippingReportId FROM dbo.ShippingReport WHERE numOppBizDocId = (SELECT numOppBizDocsId FROM OpportunityBizDocs WHERE numOppId=Opp.numOppId AND bitShippingGenerated=1) AND numDomainID ='+CAST(@numDomainID AS VARCHAR)+')
			 AND LEN(ISNULL(vcTrackingNumber,'''')) > 0) > 0 THEN 1 ELSE 0 END [ISTrackingNumGenerated]'
				END

				IF LEN(@SearchText) > 0
				BEGIN
					SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + (CASE                    
					WHEN @vcDbColumnName = 'CalAmount' THEN 'Opp.monDealAmount'
					WHEN @vcDbColumnName = 'vcInventoryStatus' THEN 'ISNULL(dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID),'''')'
					WHEN @vcDbColumnName = 'vcOrderedShipped' THEN 'dbo.GetOrderShipReceiveStatus(Opp.numOppId,1,Opp.tintshipped)'
					WHEN @vcDbColumnName = 'vcOrderedReceived' THEN 'dbo.GetOrderShipReceiveStatus(Opp.numOppId,2,Opp.tintshipped)'
					WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monDealAmount' THEN 'ISNULL((SELECT SUM(ISNULL(monDealAmount,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
					WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monAmountPaid' THEN 'ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
					else @Prefix + @vcDbColumnName END) + ' LIKE ''%' + @SearchText + '%'''
				END    
			END
            ELSE IF @vcAssociatedControlType='Popup' and @vcDbColumnName='numShareWith'                                                   
			BEGIN
				SET @strColumns = @strColumns + ',(SELECT SUBSTRING(
								(SELECT '','' + A.vcFirstName+'' ''+A.vcLastName + CASE WHEN ISNULL(SR.numContactType,0)>0 THEN ''('' + dbo.fn_GetListItemName(SR.numContactType) + '')'' ELSE '''' END
								FROM ShareRecord SR JOIN UserMaster UM ON UM.numUserDetailId=SR.numAssignedTo 
								JOIN AdditionalContactsInformation A ON UM.numUserDetailId=A.numContactID 
								WHERE SR.numDomainID=Opp.numDomainID AND SR.numModuleID=3 AND SR.numRecordID=Opp.numOppId
								AND UM.numDomainID=Opp.numDomainID and UM.numDomainID=A.numDomainID FOR XML PATH('''')),2,200000)) ['+ @vcColumnName+']'                
			END 
        END
		ELSE
        BEGIN
			SET @vcColumnName= @vcDbColumnName +'~'+ convert(varchar(10),@numFieldId)+'~1'

			IF @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'
            BEGIN
              SET @strColumns = @strColumns
                              + ',CFW'
                              + CONVERT(VARCHAR(3),@tintOrder)
                              + '.Fld_Value  ['
                              + @vcColumnName + ']'
              SET @WhereCondition = @WhereCondition
                                      + ' left Join CFW_FLD_Values_Opp CFW'
                                      + CONVERT(VARCHAR(3),@tintOrder)
                                      + ' on CFW'
                                      + CONVERT(VARCHAR(3),@tintOrder)
                                      + '.Fld_Id='
                                      + CONVERT(VARCHAR(10),@numFieldId)
                                      + 'and CFW'
                                      + CONVERT(VARCHAR(3),@tintOrder)
                                      + '.RecId=Opp.numOppid   '
			END
			ELSE
            IF @vcAssociatedControlType = 'CheckBox' 
            BEGIN
                SET @strColumns = @strColumns
                                + ',case when isnull(CFW'
                                + CONVERT(VARCHAR(3),@tintOrder)
                                + '.Fld_Value,0)=0 then ''No'' when isnull(CFW'
                                + CONVERT(VARCHAR(3),@tintOrder)
                                + '.Fld_Value,0)=1 then ''Yes'' end   ['
                                + @vcColumnName + ']'
                SET @WhereCondition = @WhereCondition
                                        + ' left Join CFW_FLD_Values_Opp CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + ' on CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.Fld_Id='
                                        + CONVERT(VARCHAR(10),@numFieldId)
                                        + 'and CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.RecId=Opp.numOppid   '
            END
            ELSE
            IF @vcAssociatedControlType = 'DateField'
            BEGIN
                  SET @strColumns = @strColumns
                                  + ',dbo.FormatedDateFromDate(CFW'
                                  + CONVERT(VARCHAR(3),@tintOrder)
                                  + '.Fld_Value,'
                                  + CONVERT(VARCHAR(10),@numDomainId)
                                  + ')  ['
                                  + @vcColumnName + ']'
                  SET @WhereCondition = @WhereCondition
                                          + ' left Join CFW_FLD_Values_Opp CFW'
                                          + CONVERT(VARCHAR(3),@tintOrder)
                                          + ' on CFW'
                                          + CONVERT(VARCHAR(3),@tintOrder)
                                          + '.Fld_Id='
                                          + CONVERT(VARCHAR(10),@numFieldId)
                                          + 'and CFW'
                                          + CONVERT(VARCHAR(3),@tintOrder)
                                          + '.RecId=Opp.numOppid    '
            END
            ELSE
            IF @vcAssociatedControlType = 'SelectBox'
            BEGIN
                SET @vcDbColumnName = 'DCust'
                                            + CONVERT(VARCHAR(10),@numFieldId)
                SET @strColumns = @strColumns
                                + ',L'
                                + CONVERT(VARCHAR(3),@tintOrder)
                                + '.vcData'
                                + ' ['
                                + @vcColumnName + ']'

                SET @WhereCondition = @WhereCondition
                                        + ' left Join CFW_FLD_Values_Opp CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + ' on CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.Fld_Id='
                                        + CONVERT(VARCHAR(10),@numFieldId)
                                        + 'and CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.RecId=Opp.numOppid '

                SET @WhereCondition = @WhereCondition
                                        + ' left Join ListDetails L'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + ' on L'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.numListItemID=CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.Fld_Value'
            END
			ELSE
			BEGIN
				SET @strColumns = @strColumns + CONCAT(',dbo.GetCustFldValueOpp(',@numFieldId,',opp.numOppID)') + ' [' + @vcColumnName + ']'
			END

			IF LEN(@SearchText) > 0
			BEGIN
				SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + CONCAT('dbo.GetCustFldValueOpp(',@numFieldId,',opp.numOppID) LIKE ''%',@SearchText,'%''')
			END
		END
      
     
		SELECT TOP 1 
			@tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,@vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,
			@numListID=numListID,@vcLookBackTableName=vcLookBackTableName,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,
			@ListRelID=ListRelID
		FROM 
			#tempForm 
		WHERE 
			tintOrder > @tintOrder-1 
		ORDER BY 
			tintOrder asc            
 
		IF @@rowcount=0 SET @tintOrder=0 
	END 

	

	DECLARE @strShareRedordWith AS VARCHAR(300);
	SET @strShareRedordWith=' Opp.numOppId in (select SR.numRecordID FROM ShareRecord SR WHERE SR.numDomainID='+ convert(varchar(15),@numDomainID )+' AND SR.numModuleID=3 AND SR.numAssignedTo='+convert(varchar(15),@numUserCntId)+') '


	DECLARE @StrSql AS VARCHAR(MAX) = ''

	SET @StrSql = @StrSql + ' FROM OpportunityMaster Opp                                                               
                                 INNER JOIN AdditionalContactsInformation ADC ON Opp.numContactId = ADC.numContactId                                                               
                                 INNER JOIN DivisionMaster Div ON Opp.numDivisionId = Div.numDivisionID AND ADC.numDivisionId = Div.numDivisionID
								 INNER JOIN CompanyInfo cmp ON Div.numCompanyID = cmp.numCompanyId 
								 LEFT JOIN ProjectProgress PP On PP.numOppId = Opp.numOppId ' + @WhereCondition

	-------Change Row Color-------
	
	DECLARE @vcCSOrigDbCOlumnName AS VARCHAR(50) = ''
	DECLARE @vcCSLookBackTableName AS VARCHAR(50) = ''
	DECLARE @vcCSAssociatedControlType AS VARCHAR(50)

	CREATE TABLE #tempColorScheme
	(
		vcOrigDbCOlumnName VARCHAR(50),
		vcLookBackTableName VARCHAR(50),
		vcFieldValue VARCHAR(50),
		vcFieldValue1 VARCHAR(50),
		vcColorScheme VARCHAR(50),
		vcAssociatedControlType varchar(50)
	)

	INSERT INTO 
		#tempColorScheme 
	SELECT 
		DFM.vcOrigDbCOlumnName,
		DFM.vcLookBackTableName,
		DFCS.vcFieldValue,
		DFCS.vcFieldValue1,
		DFCS.vcColorScheme,
		DFFM.vcAssociatedControlType 
	FROM 
		DycFieldColorScheme DFCS 
	JOIN 
		DycFormField_Mapping DFFM 
	ON 
		DFCS.numFieldID=DFFM.numFieldID
	JOIN 
		DycFieldMaster DFM 
	ON 
		DFM.numModuleID=DFFM.numModuleID and DFM.numFieldID=DFFM.numFieldID
	WHERE
		DFCS.numDomainID=@numDomainID 
		AND DFFM.numFormID=@numFormId 
		AND DFCS.numFormID=@numFormId 
		AND isnull(DFFM.bitAllowGridColor,0)=1

	IF(SELECT COUNT(*) FROM #tempColorScheme)>0
	BEGIN
		SELECT TOP 1 @vcCSOrigDbCOlumnName=vcOrigDbCOlumnName,@vcCSLookBackTableName=vcLookBackTableName,@vcCSAssociatedControlType=vcAssociatedControlType from #tempColorScheme
	END                        

	IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
	BEGIN
		SET @strColumns=@strColumns + ',tCS.vcColorScheme'
	END

	IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
	BEGIN
		IF @vcCSLookBackTableName = 'AdditionalContactsInformation'                  
			SET @Prefix = 'ADC.'                  
		IF @vcCSLookBackTableName = 'DivisionMaster'                  
			SET @Prefix = 'Div.'
		if @vcCSLookBackTableName = 'CompanyInfo'                  
			SET @Prefix = 'CMP.'   
		if @vcCSLookBackTableName = 'OpportunityMaster'                  
			SET @Prefix = 'Opp.'   
 
		IF @vcCSAssociatedControlType='DateField'
		BEGIN
			SET @strSql=@strSql+' LEFT JOIN #tempColorScheme tCS ON CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) >= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue as int),GETUTCDATE()),111)
				 and CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) <= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue1 as int),GETUTCDATE()),111)'
		END
		ELSE IF @vcCSAssociatedControlType='SelectBox' OR @vcCSAssociatedControlType='ListBox' OR @vcCSAssociatedControlType='CheckBox'
		BEGIN
			SET @strSql=@strSql+' LEFT JOIN #tempColorScheme tCS on tCS.vcFieldValue=' + @Prefix + @vcCSOrigDbCOlumnName
		END
	END
                          
	IF @columnName like 'CFW.Cust%'             
	BEGIN            
		SET @strSql = @strSql + ' LEFT JOIN CFW_Fld_Values_Opp CFW on CFW.RecId=Opp.numOppid and CFW.fld_id= '+ REPLACE(@columnName,'CFW.Cust','') +' '                                                     
		SET @columnName='CFW.Fld_Value'            
	END 

	IF @bitPartner = 1
		SET @strSql = @strSql + ' LEFT JOIN OpportunityContact OppCont ON OppCont.numOppId=Opp.numOppId AND OppCont.bitPartner=1 AND OppCont.numContactId=' + CONVERT(VARCHAR(15),@numUserCntID)
                    
                
	IF @tintFilterBy = 1 --Partially Fulfilled Orders 
		SET @strSql = @strSql + ' LEFT JOIN AdditionalContactsInformation ADC1 ON ADC1.numContactId=Opp.numRecOwner                                                               
                             LEFT OUTER JOIN [OpportunityBizDocs] OB ON OB.[numOppId] = Opp.[numOppId] 
                             WHERE Opp.numDomainID=' + CONVERT(VARCHAR(15),@numDomainID) + ' and Opp.tintOppstatus=1 and (Opp.tintShipped=' + CONVERT(VARCHAR(15),@tintShipped) + ' OR ' + CONVERT(VARCHAR(15),@tintShipped) + '=2)'
	ELSE
		SET @strSql = @strSql + ' LEFT JOIN AdditionalContactsInformation ADC1 on ADC1.numContactId=Opp.numRecOwner                                                               
                             WHERE Opp.numDomainID=' + CONVERT(VARCHAR(15),@numDomainID) + ' and Opp.tintOppstatus=1 and (Opp.tintShipped='+ CONVERT(VARCHAR(15),@tintShipped)  + ' OR ' + CONVERT(VARCHAR(15),@tintShipped) + '=2)'
  
  
	IF @intOrder <> 0
    BEGIN
      IF @intOrder = 1
        SET @strSql = @strSql + ' and Opp.bitOrder = 0 '
      IF @intOrder = 2
        SET @strSql = @strSql + ' and Opp.bitOrder = 1'
    END
    
	IF @numCompanyID <> 0
		SET @strSql = @strSql + ' And Div.numCompanyID ='+ CONVERT(VARCHAR(15),@numCompanyID)

	IF @SortChar <> '0' 
		SET @strSql = @strSql + ' And Opp.vcPOppName like ''' + @SortChar + '%'''
  
	IF @tintSalesUserRightType = 0 OR @tintPurchaseUserRightType = 0
		SET @strSql = @strSql + ' AND opp.tintOppType=0'
	ELSE IF @tintSalesUserRightType = 1 OR @tintPurchaseUserRightType = 1
		SET @strSql = @strSql + ' AND (Opp.numRecOwner= ' + CONVERT(VARCHAR(15),@numUserCntID)
                    + ' or Opp.numAssignedTo= '+ CONVERT(VARCHAR(15),@numUserCntID)
                    + CASE WHEN @bitPartner = 1 THEN ' or ( OppCont.bitPartner=1 and OppCont.numContactId='+ CONVERT(VARCHAR(15),@numUserCntID) + ')' ELSE '' END
                    + ' or ' + @strShareRedordWith +')'
	ELSE IF @tintSalesUserRightType = 2 OR @tintPurchaseUserRightType = 2
		SET @strSql = @strSql + ' AND (Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+ CONVERT(VARCHAR(15),@numUserCntID) + ' ) 
								or div.numTerID=0 or Opp.numAssignedTo= '+ CONVERT(VARCHAR(15),@numUserCntID)
								+ ' or ' + @strShareRedordWith +')'
					
	IF @tintSortOrder <> '0'
		SET @strSql = @strSql + '  AND Opp.tintOppType= '+ CONVERT(VARCHAR(1),@tintSortOrder)
    
    
	IF @tintFilterBy = 1 --Partially Fulfilled Orders
		SET @strSql = @strSql + ' AND OB.bitPartialFulfilment = 1 AND OB.bitAuthoritativeBizDocs = 1 '
	ELSE IF @tintFilterBy = 2 --Fulfilled Orders
		SET @strSql = @strSql + ' AND Opp.[numOppId] IN (SELECT OM.numOppId FROM [OpportunityMaster] OM
                      			INNER JOIN [OpportunityBizDocs] OB ON OB.[numOppId] = OM.[numOppId]
                      			INNER JOIN [OpportunityItems] OI ON OM.[numOppId] = OI.[numOppId]
                      			INNER JOIN [OpportunityBizDocItems] BI ON BI.[numOppBizDocID] = OB.[numOppBizDocsId]
                      			Where OM.numDomainId=' + CONVERT(VARCHAR(15),@numDomainId) + 'AND OB.[bitAuthoritativeBizDocs] = 1 GROUP BY OM.[numOppId]
                      			HAVING   COUNT(OI.[numUnitHour]) = COUNT(BI.[numUnitHour])) '
	ELSE IF @tintFilterBy = 3 --Orders without Authoritative-BizDocs
		SET @strSql = @strSql + ' AND Opp.[numOppId] not IN (SELECT DISTINCT OM.[numOppId]
                      		  FROM [OpportunityMaster] OM JOIN [OpportunityBizDocs] OB ON OM.[numOppId] = OB.[numOppId]
                      		  Where OM.numDomainId=' + CONVERT(VARCHAR(15),@numDomainId) + ' AND OM.tintOppstatus=1 AND (OM.tintShipped=' + CONVERT(VARCHAR(15),@tintShipped) + ' OR ' + CONVERT(VARCHAR(15),@tintShipped) + '=2)' + ' AND OM.tintOppType= '+ CONVERT(VARCHAR(1),@tintSortOrder) + ' AND isnull(OB.[bitAuthoritativeBizDocs],0)=1) '
	ELSE IF @tintFilterBy = 4 --Orders with items yet to be added to Invoices
		SET @strSql = @strSql + ' AND Opp.[numOppId] IN (SELECT distinct OPPM.numOppId FROM [OpportunityMaster] OPPM
                                  INNER JOIN [OpportunityItems] OPPI ON OPPM.[numOppId] = OPPI.[numOppId]
                                  Where OPPM.numDomainId=' + CONVERT(VARCHAR(15),@numDomainId) + ' 
								  and OPPI.numoppitemtCode not in (select numOppItemID from [OpportunityBizDocs] OB INNER JOIN [OpportunityBizDocItems] BI
                      			  ON BI.[numOppBizDocID] = OB.[numOppBizDocsId] where OB.[numOppId] = OPPM.[numOppId]))'      
	ELSE IF @tintFilterBy = 6 --Orders with deferred Income/Invoices
		SET @strSql = @strSql + ' AND Opp.[numOppId] IN (SELECT DISTINCT OM.[numOppId]
                      		 FROM [OpportunityMaster] OM LEFT OUTER JOIN [OpportunityBizDocs] OB ON OM.[numOppId] = OB.[numOppId]
                      		 Where OM.numDomainId=' + CONVERT(VARCHAR(15),@numDomainId) + ' AND OB.[bitAuthoritativeBizDocs] =1 and OB.tintDeferred=1)'

	IF @numOrderStatus <>0 
		SET @strSql = @strSql + ' AND Opp.numStatus = ' + CONVERT(VARCHAR(15),@numOrderStatus);

	IF CHARINDEX('vcInventoryStatus',@vcRegularSearchCriteria) > 0
	BEGIN
		IF CHARINDEX('vcInventoryStatus=1',@vcRegularSearchCriteria) > 0
			SET @strSql = @strSql + ' AND ' + ' CHARINDEX(''Not Applicable'', dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID)) > 0 '
		IF CHARINDEX('vcInventoryStatus=2',@vcRegularSearchCriteria) > 0
			SET @strSql = @strSql + ' AND ' + ' CHARINDEX(''Allocation Cleared'', dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID)) > 0 '
		IF CHARINDEX('vcInventoryStatus=3',@vcRegularSearchCriteria) > 0
			SET @strSql = @strSql + ' AND ' + ' CHARINDEX(''Back Order'', dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID)) > 0 '
		IF CHARINDEX('vcInventoryStatus=4',@vcRegularSearchCriteria) > 0
			SET @strSql = @strSql + ' AND ' + ' CHARINDEX(''Ready to Ship'', dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID)) > 0 '
	END
	ELSE IF @vcRegularSearchCriteria<>'' 
	BEGIN
		SET @strSql=@strSql+' AND ' + @vcRegularSearchCriteria 
	END
	
	IF @vcCustomSearchCriteria<>'' 
	BEGIN
		SET @strSql = @strSql + ' AND Opp.numOppid in (select distinct CFW.RecId from CFW_Fld_Values_Opp CFW where ' + @vcCustomSearchCriteria + ')'
	END

	IF LEN(@SearchQuery) > 0
	BEGIN
		SET @strSql = @strSql + ' AND (' + @SearchQuery + ') '
	END

	

	DECLARE  @firstRec  AS INTEGER
	DECLARE  @lastRec  AS INTEGER
  
	SET @firstRec = (@CurrentPage - 1) * @PageSize
	SET @lastRec = (@CurrentPage * @PageSize + 1)


	-- GETS ROWS COUNT
	DECLARE @strTotal NVARCHAR(MAX) = 'SELECT @TotalRecords = COUNT(*) ' + @strSql
	exec sp_executesql @strTotal, N'@TotalRecords INT OUT', @TotRecs OUT


	-- EXECUTE FINAL QUERY
	DECLARE @strFinal AS VARCHAR(MAX)
	SET @strFinal = CONCAT('SELECT ROW_NUMBER() OVER(ORDER BY ',@columnName,' ',@columnSortOrder,') ID, ',@strColumns,' INTO #tempTable',@strSql,'; SELECT * FROM #tempTable WHERE ID > ',@firstRec,' and ID <',@lastRec,' ORDER BY ID; DROP TABLE #tempTable;')
	PRINT @strFinal
	EXEC (@strFinal) 

	SELECT * FROM #tempForm

	DROP TABLE #tempForm

	DROP TABLE #tempColorScheme
--Created by anoop jayaraj

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetPOItemsForFulfillment')
DROP PROCEDURE USP_GetPOItemsForFulfillment
GO
CREATE PROCEDURE [dbo].[USP_GetPOItemsForFulfillment]
@numDomainID as numeric(9),
@numUserCntID numeric(9)=0,                                                                                                               
@SortChar char(1)='0',                                                            
 @CurrentPage int,                                                              
 @PageSize int,                                                              
 @TotRecs int output,                                                              
 @columnName as Varchar(50),                                                              
 @columnSortOrder as Varchar(10),
 @Filter AS VARCHAR(30),
 @FilterBy AS int
as


--Create a Temporary table to hold data                                                              
Create table #tempTable ( ID INT IDENTITY PRIMARY KEY, 
    numoppitemtCode numeric(9),
    numOppID  numeric(9),
     vcPOppName varchar(200),
    vcItemName varchar(500),
    vcModelID varchar(200),
    numUnitHour FLOAT,
    numUnitHourReceived FLOAT,
    numOnHand FLOAT,
	numOnOrder FLOAT,
	numAllocation FLOAT,
	numBackOrder FLOAT,
	vcWarehouse varchar(200),
	dtDueDate VARCHAR(25),bitSerialized BIT,numWarehouseItmsID NUMERIC(9),bitLotNo BIT,SerialLotNo VARCHAR(1000),
	fltExchangeRate float,numCurrencyID numeric(9),numDivisionID numeric(9),
	itemIncomeAccount numeric(9),itemInventoryAsset numeric(9),itemCoGs numeric(9),
	DropShip BIT,charItemType CHAR(1),ItemType VARCHAR(50),
	numProjectID numeric(9),numClassID numeric(9),numItemCode numeric(9),monPrice MONEY,bitPPVariance bit,Vendor varchar(max),numWLocationID NUMERIC(18,0))
	
declare @strSql as varchar(8000)                                                              
set @strSql='Select numoppitemtCode,Opp.numOppID,vcPOppName,OI.vcItemName + '' - ''+ isnull(dbo.fn_GetAttributes(OI.numWarehouseItmsID,0),'''') as vcItemName ,OI.vcModelID,numUnitHour,isnull(numUnitHourReceived,0) as numUnitHourReceived,
isnull(numOnHand,0) as numOnHand,isnull(numOnOrder,0) as numOnOrder,isnull(numAllocation,0) as numAllocation,isnull(numBackOrder,0) as numBackOrder,vcWarehouse + '': '' + isnull(WL.vcLocation,'''') as  vcWarehouse,
[dbo].[FormatedDateFromDate](intPEstimatedCloseDate,'+convert(varchar(20),@numDomainID)+') AS dtDueDate,isnull(I.bitSerialized,0) as bitSerialized,OI.numWarehouseItmsID,isnull(I.bitLotNo,0) as bitLotNo
,SUBSTRING(
(SELECT '','' + vcSerialNo + CASE WHEN isnull(I.bitLotNo,0)=1 THEN '' ('' + CONVERT(VARCHAR(15),whi.numQty) + '')'' ELSE '''' end 
FROM OppWarehouseSerializedItem oppI JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID=whi.numWareHouseItmsDTLID where oppI.numOppID=OI.numOppId and oppI.numOppItemID=OI.numoppitemtCode 
ORDER BY vcSerialNo FOR XML PATH('''')),2,200000) AS SerialLotNo,
ISNULL(Opp.fltExchangeRate,0) AS fltExchangeRate,ISNULL(Opp.numCurrencyID,0) AS numCurrencyID,ISNULL(Opp.numDivisionID,0) AS numDivisionID,
isnull(I.numIncomeChartAcntId,0) as itemIncomeAccount,isnull(I.numAssetChartAcntId,0) as itemInventoryAsset,isnull(I.numCOGsChartAcntId,0) as itemCoGs,
isnull(OI.bitDropShip,0) as DropShip,I.charItemType,OI.vcType ItemType,ISNULL(OI.numProjectID, 0) numProjectID,ISNULL(OI.numClassID, 0) numClassID,
OI.numItemCode,OI.monPrice,isnull(Opp.bitPPVariance,0) as bitPPVariance,
ISNULL(com.vcCompanyName,''-'') as Vendor,
WI.numWLocationID
from OpportunityMaster Opp
Join OpportunityItems OI On OI.numOppId=Opp.numOppId
Join Item I on I.numItemCode=OI.numItemCode
Left Join WareHouseItems WI on OI.numWarehouseItmsID=WI.numWareHouseItemID
Left Join Warehouses W on W.numWarehouseID=WI.numWarehouseID
LEFT join divisionmaster div on Opp.numDivisionId=div.numDivisionId 
LEFT join companyInfo com  on com.numCompanyid=div.numcompanyID
left join WarehouseLocation WL on WL.numWLocationID =WI.numWLocationID
where tintOppType=2 and tintOppstatus=1 and ISNULL(bitStockTransfer,0)=0 and tintShipped=0 and numUnitHour-isnull(numUnitHourReceived,0)>0  
AND I.[charItemType] IN (''P'',''N'',''S'') and (OI.bitDropShip=0 or OI.bitDropShip is null) and Opp.numDomainID='+convert(varchar(20),@numDomainID) 

--OI.[numWarehouseItmsID] IS NOT NULL
if @SortChar<>'0' set @strSql=@strSql + ' And Opp.vcPOppName like '''+@SortChar+'%'''                                                      

IF @FilterBy=1 --Item
	set	@strSql=@strSql + ' and OI.vcItemName like ''%' + @Filter + '%'''
ELSE IF @FilterBy=2 --Warehouse
	set	@strSql=@strSql + ' and W.vcWareHouse like ''%' + @Filter + '%'''
ELSE IF @FilterBy=3 --Purchase Order
	set	@strSql=@strSql + ' and Opp.vcPOppName like ''%' + @Filter + '%'''
ELSE IF @FilterBy=4 --Vendor
	set	@strSql=@strSql + ' and com.vcCompanyName LIKE ''%' + @Filter + '%'''		
ELSE IF @FilterBy=5 --BizDoc
	set	@strSql=@strSql + ' and Opp.numOppID in (SELECT numOppId FROM OpportunityBizDocs WHERE vcBizDocID like ''%' + @Filter + '%'')'

If @columnName = 'vcModelID' 
	set @columnName='OI.vcModelID'
								
set @strSql=@strSql + ' ORDER BY  ' + @columnName +' '+ @columnSortOrder

PRINT @strSql

insert into #tempTable(numoppitemtCode,
    numOppID ,
    vcPOppName,
    vcItemName,
    vcModelID,
    numUnitHour,
    numUnitHourReceived,
    numOnHand,
	numOnOrder,
	numAllocation,
	numBackOrder,
	vcWarehouse,
	dtDueDate,bitSerialized,numWarehouseItmsID,bitLotNo,SerialLotNo,fltExchangeRate,numCurrencyID,numDivisionID,
	itemIncomeAccount,itemInventoryAsset,itemCoGs,DropShip,charItemType,ItemType,numProjectID,numClassID,numItemCode,monPrice,bitPPVariance,Vendor,numWLocationID)                                                              
exec (@strSql) 


declare @firstRec as integer                                                
declare @lastRec as integer                                                              
set @firstRec= (@CurrentPage-1) * @PageSize                                                              
set @lastRec= (@CurrentPage*@PageSize+1)                                           
set @TotRecs=(select count(*) from #tempTable) 


select *  from #tempTable where ID>@firstRec and ID < @lastRec

drop table #tempTable
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetPriceLevelItemPrice')
DROP PROCEDURE USP_GetPriceLevelItemPrice
GO
CREATE PROCEDURE [dbo].[USP_GetPriceLevelItemPrice]                                        
@numItemCode as numeric(9),
@numDomainID as numeric(9)=0,            
--@numPricingID as numeric(9)=0,
@numWareHouseItemID as numeric(9),
@numDivisionID as numeric(9)=0                  
as                 
BEGIN
	DECLARE @monListPrice AS MONEY;	SET @monListPrice=0
	DECLARE @monVendorCost AS MONEY;	SET @monVendorCost=0

	IF((@numWareHouseItemID>0) and exists(select * from item where numItemCode=@numItemCode and charItemType='P'))      
	BEGIN      
		SELECT 
			@monListPrice=ISNULL(monWListPrice,0) 
		FROM 
			WareHouseItems 
		WHERE 
			numWareHouseItemID=@numWareHouseItemID      
		
		IF @monListPrice=0 
		BEGIN
			SELECT @monListPrice=monListPrice FROM Item WHERE numItemCode=@numItemCode       
		END
	END      
	ELSE      
	BEGIN      
		 select @monListPrice=monListPrice from Item where numItemCode=@numItemCode      
	END 

	SELECT @monVendorCost = dbo.[fn_GetVendorCost](@numItemCode)

	SELECT  
		[numPricingID],
		[numPriceRuleID],
		[intFromQty],
		[intToQty],
		[tintRuleType],
		[tintDiscountType],
		ISNULL(vcName,'') vcName,
        CASE 
			WHEN tintRuleType=1 AND tintDiscountType=1 --Deduct from List price & Percentage
				THEN @monListPrice - (@monListPrice * ( decDiscount /100))
            WHEN tintRuleType=1 AND tintDiscountType=2 --Deduct from List price & Flat discount
				THEN @monListPrice - decDiscount
            WHEN tintRuleType=2 AND tintDiscountType=1 --Add to Primary Vendor Cost & Percentage
				THEN @monVendorCost + (@monVendorCost * ( decDiscount /100))
            WHEN tintRuleType=2 AND tintDiscountType=2 --Add to Primary Vendor Cost & Flat discount
				THEN @monVendorCost + decDiscount
            WHEN tintDiscountType=3 --Named Price
				THEN decDiscount
        END AS decDiscount
    FROM
		[PricingTable]
    WHERE
		ISNULL(numItemCode,0)=@numItemCode
    ORDER BY 
		[numPricingID] 
  
	DECLARE @numRelationship AS NUMERIC(9)
	DECLARE @numProfile AS NUMERIC(9)

	SELECT 
		@numRelationship=numCompanyType,
		@numProfile=vcProfile 
	FROM 
		DivisionMaster D                  
	JOIN 
		CompanyInfo C 
	ON 
		C.numCompanyId=D.numCompanyID
	WHERE 
		numDivisionID =@numDivisionID       

  
	SELECT 
		PT.numPricingID,
		PT.numPriceRuleID,
		PT.intFromQty,
		PT.intToQty,
		PT.tintRuleType,
		PT.tintDiscountType,
		ISNULL(PT.vcName,'') vcName,
		CASE 
			WHEN PT.tintRuleType=1 AND PT.tintDiscountType=1 --Deduct from List price & Percentage
				THEN @monListPrice - (@monListPrice * ( PT.decDiscount /100))
            WHEN PT.tintRuleType=1 AND PT.tintDiscountType=2 --Deduct from List price & Flat discount
				THEN @monListPrice - PT.decDiscount
            WHEN PT.tintRuleType=2 AND PT.tintDiscountType=1 --Add to Primary Vendor Cost & Percentage
				THEN @monVendorCost + (@monVendorCost * ( PT.decDiscount /100))
            WHEN PT.tintRuleType=2 AND PT.tintDiscountType=2 --Add to Primary Vendor Cost & Flat discount
				THEN @monVendorCost + PT.decDiscount 
		END AS decDiscount
	FROM 
		Item I
    JOIN 
		PriceBookRules P 
	ON 
		I.numDomainID = P.numDomainID
    LEFT JOIN 
		PriceBookRuleDTL PDTL 
	ON 
		P.numPricRuleID = PDTL.numRuleID
    LEFT JOIN 
		PriceBookRuleItems PBI 
	ON 
		P.numPricRuleID = PBI.numRuleID
    LEFT JOIN 
		[PriceBookPriorities] PP 
	ON 
		PP.[Step2Value] = P.[tintStep2] 
		AND PP.[Step3Value] = P.[tintStep3]
    JOIN 
		[PricingTable] PT 
	ON 
		P.numPricRuleID = PT.numPriceRuleID
	WHERE
		I.numItemCode = @numItemCode 
		AND P.tintRuleFor=1 
		AND P.tintPricingMethod = 1
		AND 
		(
		((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 1
		OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND (tintStep3= 1 AND PDTL.numValue = @numDivisionID)) -- Priority 2
		OR ((tintStep2 = 3) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 3

		OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 4
		OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 5
		OR ((tintStep2 = 3) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 6

		OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 3)) -- Priority 7
		OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND (tintStep3 = 3)) -- Priority 8
		OR ((tintStep2 = 3) and (tintStep3 = 3)) -- Priority 9
		)
	ORDER BY PP.Priority ASC
END
      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetShippingBox')
DROP PROCEDURE USP_GetShippingBox
GO
CREATE PROCEDURE USP_GetShippingBox
    @ShippingReportId NUMERIC(9)
AS 
    BEGIN
        SELECT DISTINCT
                [numBoxID],
                [vcBoxName],
                [numShippingReportId],
                CASE WHEN [fltTotalWeight] = 0 THEN SUM([fltWeight]) ELSE [fltTotalWeight]+[fltDimensionalWeight] END AS fltTotalWeight,
                [fltHeight],
                [fltWidth],
                [fltLength],
				dbo.FormatedDateFromDate(dtDeliveryDate,numDomainID) dtDeliveryDate,
				ISNULL(monShippingRate,0) [monShippingRate],
				vcShippingLabelImage,
				ISNULL(vcTrackingNumber,'') AS vcTrackingNumber,
				tintServiceType,--Your Packaging
				tintPayorType,
				vcPayorAccountNo,
				vcPayorZip,
				numPayorCountry,
				numPackageTypeID,
				ISNULL(vcPackageName,'') AS [vcPackageName],
				numServiceTypeID,
				CAST(ROUND(
				(CASE WHEN SUM(fltDimensionalWeight) > fltTotalRegularWeight
				      THEN SUM(fltDimensionalWeight) 
				      ELSE fltTotalRegularWeight+fltDimensionalWeight
				END), 2) AS DECIMAL(9,2)) AS [fltTotalWeightForShipping],
				SUM(fltDimensionalWeight) AS [fltTotalDimensionalWeight],
				ISNULL(fltDimensionalWeight,0) AS [fltDimensionalWeight],
				ISNULL(numShipCompany,0) AS [numShipCompany],
				numOppID
--				,intBoxQty
				--ShippingReportItemId
        FROM    View_ShippingBox
        WHERE   [numShippingReportId] = @ShippingReportId
        GROUP BY [numBoxID],
                [vcBoxName],
                [numShippingReportId],
                [fltHeight],
                [fltWidth],
                [fltLength],
                fltTotalWeight,dtDeliveryDate,numOppID,View_ShippingBox.numOppBizDocID,
				monShippingRate,vcShippingLabelImage,
				vcTrackingNumber,numDomainID,
				tintServiceType,tintPayorType,
				vcPayorAccountNo,
				vcPayorZip,
				numPayorCountry,numPackageTypeID,vcPackageName,numServiceTypeID,fltDimensionalWeight,fltTotalRegularWeight,numShipCompany--,intBoxQty
				                
        SELECT  [numBoxID],
                [vcBoxName],
                [ShippingReportItemId],
                [numShippingReportId],
                fltTotalWeightItem [fltTotalWeight],
                fltHeightItem [fltHeight],
                fltWidthItem [fltWidth],
                fltLengthItem [fltLength],
                [numOppBizDocItemID],
                [numItemCode],
                [numoppitemtCode],
                ISNULL([vcItemName],'') vcItemName,
                [vcModelID],
                CASE WHEN LEN (ISNULL(vcItemDesc,''))> 100 THEN  CONVERT(VARCHAR(100),[vcItemDesc]) + '..'
                ELSE ISNULL(vcItemDesc,'') END vcItemDesc,
                intBoxQty,
                numServiceTypeID,
                vcUnitName,monUnitPrice,
				numOppID
        FROM    [View_ShippingBox]
        WHERE   [numShippingReportId] = @ShippingReportId       
                
                
    END
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetSimilarItem')
DROP PROCEDURE USP_GetSimilarItem
GO
CREATE PROCEDURE USP_GetSimilarItem
@numDomainID NUMERIC(9),
@numParentItemCode NUMERIC(9),
@byteMode TINYINT,
@vcCookieId VARCHAR(MAX)='',
@numUserCntID NUMERIC(18)=0,
@numSiteID NUMERIC(18,0)=0,
@numOppID NUMERIC(18,0) = 0
AS 
BEGIN

If @byteMode=1
BEGIN
 select count(*) as Total from SimilarItems SI INNER JOIN Item I ON I.numItemCode = SI.numItemCode 
 WHERE SI.numDomainId = @numDomainID AND SI.numParentItemCode = @numParentItemCode
END
ELSE IF @byteMode=3
BEGIN
SELECT DISTINCT I.vcItemName AS vcItemName,(SELECT TOP 1 vcPathForTImage FROM dbo.ItemImages II WHERE II.numItemCode = I.numItemCode AND II.numDomainId= I.numDomainId AND II.bitDefault=1) As vcPathForTImage ,I.txtItemDesc AS txtItemDesc,  SI.* ,Category.vcCategoryName,
ISNULL(CASE WHEN I.[charItemType]='P' THEN CASE WHEN I.bitSerialized = 1 THEN 1.00000 * monListPrice ELSE 1.00000 * W.[monWListPrice] END ELSE 1.00000 * monListPrice END,0) monListPrice,
dbo.fn_UOMConversion(ISNULL(I.numSaleUnit,0),I.numItemCode,I.numDomainId,ISNULL(I.numBaseUnit,0)) as UOMConversionFactor,I.vcSKU,I.vcManufacturer
, ISNULL(vcUpSellDesc,'') AS [txtUpSellItemDesc],ISNULL(bitPreUpSell,0) [bitPreUpSell], ISNULL(bitPostUpSell,0) [bitPostUpSell], ISNULL(vcUpSellDesc,'') [vcUpSellDesc]
,(SELECT TOP 1 CASE WHEN P.tintDiscountType=1  
				THEN (I.monListPrice*fltDiscountValue)/100
				WHEN P.tintDiscountType=2
				THEN fltDiscountValue
				WHEN P.tintDiscountType=3
				THEN fltDiscountValue*I.monListPrice
				ELSE 0 END
FROM PromotionOffer AS P 
LEFT JOIN CartItems AS C ON P.numProId=C.PromotionID
WHERE C.vcCookieId=@vcCookieId AND C.numUserCntId=@numUserCntID AND 
(I.numItemCode IN (SELECT PIS.numValue FROM PromotionOfferItems AS PIS WHERE PIS.numProId=P.numProId AND PIS.tintType=1 AND PIS.tintRecordType=6) OR
I.numItemClassification IN (SELECT PIS.numValue FROM PromotionOfferItems AS PIS WHERE PIS.numProId=P.numProId AND PIS.tintType=2 AND PIS.tintRecordType=6))
AND P.numDomainId=@numDomainID 
		AND ISNULL(bitEnabled,0)=1 
		AND ISNULL(bitAppliesToSite,0)=1 
		AND 1= (CASE WHEN P.tintOfferTriggerValueType=1 AND C.numUnitHour>=fltOfferTriggerValue THEN 1 WHEN P.tintOfferTriggerValueType=2 AND monTotAmount>=fltOfferTriggerValue THEN 1 ELSE 0 END)
		AND 1 = (CASE WHEN bitRequireCouponCode = 1 THEN CASE WHEN ISNULL(intCouponCodeUsed,0) < ISNULL(tintUsageLimit,0) OR ISNULL(tintUsageLimit,0) = 0  THEN 1 ELSE 0 END ELSE 1 END)
		AND 1 = (CASE WHEN bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN CAST(GETDATE() AS DATE)>=dtValidFrom AND CAST(GETDATE() AS DATE)<=dtValidTo THEN 1 ELSE 0 END) END)
) AS PromotionOffers
FROM 
Category INNER JOIN ItemCategory ON Category.numCategoryID = ItemCategory.numCategoryID INNER JOIN
                      Item I ON ItemCategory.numItemID = I.numItemCode INNER JOIN
                      SimilarItems SI ON I.numItemCode = SI.numItemCode LEFT  JOIN
                       WareHouseItems W ON I.numItemCode = W.numItemID
 WHERE SI.numDomainId = @numDomainID AND SI.numParentItemCode = @numParentItemCode
END
ELSE IF @byteMode=2
BEGIN
	SELECT 
		I.numItemCode,
		I.vcItemName AS vcItemName,
		(SELECT TOP 1 vcPathForTImage FROM dbo.ItemImages II WHERE II.numItemCode = I.numItemCode AND II.numDomainId= I.numDomainId AND II.bitDefault=1) As vcPathForTImage,
		I.txtItemDesc AS txtItemDesc, 
		dbo.fn_GetUOMName(I.numSaleUnit) AS vcUOMName,
		WarehouseItems.numWareHouseItemID,
		(CASE WHEN I.charItemType='P' THEN ISNULL(monWListPrice,0) ELSE I.monListPrice END) monListPrice
		,ISNULL(vcRelationship,'') vcRelationship
		,ISNULL(bitPreUpSell,0) [bitPreUpSell]
		,ISNULL(bitPostUpSell,0) [bitPostUpSell]
		,ISNULL(vcUpSellDesc,'') [vcUpSellDesc]
	FROM 
		SimilarItems SI 
	INNER JOIN 
		Item I 
	ON 
		I.numItemCode = SI.numItemCode
	OUTER APPLY
	(
		SELECT TOP 1
			numWareHouseItemID,
			monWListPrice
		FROM
			WarehouseItems
		WHERE
			WarehouseItems.numItemID = I.numItemCode
	) AS WarehouseItems
	WHERE 
		SI.numDomainId = @numDomainID AND SI.numParentItemCode = @numParentItemCode
END 

ELSE IF @byteMode=4 -- Pre up sell
BEGIN
DECLARE @numWarehouseID AS NUMERIC(18,0)
SELECT @numWarehouseID=ISNULL(numDefaultWareHouseID,0) FROM eCommerceDTL WHERE numSiteId=@numSiteID

SELECT DISTINCT I.vcItemName AS vcItemName,(SELECT TOP 1 vcPathForTImage FROM dbo.ItemImages II WHERE II.numItemCode = I.numItemCode AND II.numDomainId= I.numDomainId AND II.bitDefault=1) As vcPathForTImage ,I.txtItemDesc AS txtItemDesc,  SI.* ,Category.vcCategoryName,
ISNULL(CASE WHEN I.[charItemType]='P' THEN CASE WHEN I.bitSerialized = 1 THEN 1.00000 * monListPrice ELSE 1.00000 * W.[monWListPrice] END ELSE 1.00000 * monListPrice END,0) monListPrice,
dbo.fn_UOMConversion(ISNULL(I.numSaleUnit,0),I.numItemCode,I.numDomainId,ISNULL(I.numBaseUnit,0)) as UOMConversionFactor,I.vcSKU,I.vcManufacturer
, ISNULL(vcUpSellDesc,'') AS [txtUpSellItemDesc],ISNULL(bitPreUpSell,0) [bitPreUpSell], ISNULL(bitPostUpSell,0) [bitPostUpSell], ISNULL(vcUpSellDesc,'') [vcUpSellDesc],
(SELECT TOP 1 CASE WHEN P.tintDiscountType=1  
				THEN (I.monListPrice*fltDiscountValue)/100
				WHEN P.tintDiscountType=2
				THEN fltDiscountValue
				WHEN P.tintDiscountType=3
				THEN fltDiscountValue*I.monListPrice
				ELSE 0 END
FROM PromotionOffer AS P 
LEFT JOIN CartItems AS C ON P.numProId=C.PromotionID
WHERE C.vcCookieId=@vcCookieId AND C.numUserCntId=@numUserCntID AND 
(I.numItemCode IN (SELECT PIS.numValue FROM PromotionOfferItems AS PIS WHERE PIS.numProId=P.numProId AND PIS.tintType=1 AND PIS.tintRecordType=6) OR
I.numItemClassification IN (SELECT PIS.numValue FROM PromotionOfferItems AS PIS WHERE PIS.numProId=P.numProId AND PIS.tintType=2 AND PIS.tintRecordType=6))
AND P.numDomainId=@numDomainID 
		AND ISNULL(bitEnabled,0)=1 
		AND ISNULL(bitAppliesToSite,0)=1 
		AND 1= (CASE WHEN P.tintOfferTriggerValueType=1 AND C.numUnitHour>=fltOfferTriggerValue THEN 1 WHEN P.tintOfferTriggerValueType=2 AND monTotAmount>=fltOfferTriggerValue THEN 1 ELSE 0 END)
		AND 1 = (CASE WHEN bitRequireCouponCode = 1 THEN CASE WHEN ISNULL(intCouponCodeUsed,0) < ISNULL(tintUsageLimit,0) OR ISNULL(tintUsageLimit,0) = 0  THEN 1 ELSE 0 END ELSE 1 END)
		AND 1 = (CASE WHEN bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN CAST(GETDATE() AS DATE)>=dtValidFrom AND CAST(GETDATE() AS DATE)<=dtValidTo THEN 1 ELSE 0 END) END)
) AS PromotionOffers
FROM 
Category INNER JOIN ItemCategory ON Category.numCategoryID = ItemCategory.numCategoryID INNER JOIN
                      Item I ON ItemCategory.numItemID = I.numItemCode INNER JOIN
                      SimilarItems SI ON I.numItemCode = SI.numItemCode 
                       OUTER APPLY
	(
		SELECT
			TOP 1 *
		FROM
			WareHouseItems
		WHERE
			 numItemID = I.numItemCode
			 AND (numWareHouseID = @numWarehouseID OR @numWarehouseID=0)
	) AS W
 --FROM SimilarItems SI INNER JOIN Item I ON I.numItemCode = SI.numItemCode  INNER JOIN dbo.Category cat ON cat.numCategoryID = I.num
 WHERE SI.numDomainId = @numDomainID AND SI.numParentItemCode = @numParentItemCode AND ISNULL(bitPreUpSell,0)=1
END 

ELSE IF @byteMode=5 -- Post up sell
BEGIN
	SELECT @numWarehouseID=ISNULL(numDefaultWareHouseID,0) FROM eCommerceDTL WHERE numSiteId=@numSiteID

	DECLARE @numPromotionID NUMERIC(18,0) = 0
	DECLARE @vcPromotionDescription VARCHAR(MAX) = ''
	DECLARE @intPostSellDiscount INT=0
	
	SELECT TOP 1 
		@intPostSellDiscount=ISNULL(P.intPostSellDiscount,0)
		,@numPromotionID=C.numPromotionID
		,@vcPromotionDescription= CONCAT('Post sell Discount: ', OM.vcPOppName)
	FROM 
		OpportunityItems AS C 
	INNER JOIN
		OpportunityMaster OM
	ON
		C.numOppId=OM.numOppId
	LEFT JOIN 
		PromotionOffer AS P 
	ON 
		P.numProId=C.numPromotionID 
	WHERE 
		OM.numDomainId=@numDomainID
		AND OM.numOppID = @numOppID
		AND ISNULL(OM.bitPostSellDiscountUsed,0) = 0
		AND ISNULL(C.numPromotionID,0) > 0
		AND ISNULL(P.bitDisplayPostUpSell,0)=1
		AND ISNULL(P.intPostSellDiscount,0) BETWEEN 1 AND 100

	SELECT DISTINCT 
		I.numItemCode
		,I.vcItemName AS vcItemName
		,(SELECT TOP 1 vcPathForTImage FROM dbo.ItemImages II WHERE II.numItemCode = I.numItemCode AND II.numDomainId= I.numDomainId AND II.bitDefault=1) As vcPathForTImage 
		,I.txtItemDesc AS txtItemDesc
		,I.vcModelID
		,Category.vcCategoryName
		,1 AS numUnitHour
		,ISNULL(CASE WHEN I.[charItemType]='P' THEN [monWListPrice] ELSE monListPrice END,0) monListPrice
		,ISNULL(CASE WHEN I.[charItemType]='P' THEN (monWListPrice-((monWListPrice*(CASE WHEN OI.numPromotionID = @numPromotionID THEN @intPostSellDiscount ELSE 0 END))/100)) ELSE (monListPrice-((monListPrice*(CASE WHEN OI.numPromotionID = @numPromotionID THEN @intPostSellDiscount ELSE 0 END))/100)) END,0) monOfferListPrice
		,W.numWarehouseItemID
		,(CASE WHEN OI.numPromotionID = @numPromotionID THEN @intPostSellDiscount ELSE 0 END) AS fltDiscount
		,0 AS bitDiscountType
		,(CASE WHEN OI.numPromotionID = @numPromotionID THEN @numPromotionID ELSE 0 END) AS numPromotionID
		,(CASE WHEN OI.numPromotionID = @numPromotionID THEN @vcPromotionDescription ELSE '' END) AS vcPromotionDescription
		,dbo.fn_UOMConversion(ISNULL(I.numSaleUnit,0)
		,I.numItemCode
		,I.numDomainId
		,ISNULL(I.numBaseUnit,0)) as UOMConversionFactor
		,I.vcSKU
		,I.vcManufacturer
		,ISNULL(vcUpSellDesc,'') AS [txtUpSellItemDesc]
		,ISNULL(bitPreUpSell,0) [bitPreUpSell]
		,ISNULL(bitPostUpSell,0) [bitPostUpSell]
		,ISNULL(vcUpSellDesc,'') [vcUpSellDesc]
	FROM 
		Category 
	INNER JOIN 
		ItemCategory 
	ON 
		Category.numCategoryID = ItemCategory.numCategoryID 
	INNER JOIN
        Item I 
	ON 
		ItemCategory.numItemID = I.numItemCode
	INNER JOIN
		OpportunityItems OI
	ON
		OI.numItemCode = I.numItemCode
	INNER JOIN 
		OpportunityMaster OM 
	ON 
		OI.numOppId=OM.numOppId	
	INNER JOIN
        SimilarItems SI 
	ON 
		SI.numParentItemCode=OI.numItemCode
		AND SI.bitPostUpSell = 1
	OUTER APPLY
	(
		SELECT
			TOP 1 *
		FROM
			WareHouseItems
		WHERE
			 numItemID = I.numItemCode
			 AND numWareHouseID = @numWarehouseID
	) AS W
	WHERE 
		SI.numDomainId = @numDomainID 
		AND OM.numDomainID=@numDomainID 
		AND OM.numOppId=@numOppID
		AND 1 = (CASE WHEN charItemType='P' THEN CASE WHEN ISNULL(W.numWareHouseItemID,0) > 0 THEN 1 ELSE 0 END ELSE 1 END)
		
		

	UPDATE OpportunityMaster SET bitPostSellDiscountUsed=1 WHERE numDomainId=@numDomainID AND numOppID = @numOppID
END 

END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetTaxOfCartItems')
DROP PROCEDURE USP_GetTaxOfCartItems
GO
CREATE PROCEDURE [dbo].[USP_GetTaxOfCartItems]
(
    @numDomainId AS NUMERIC(9) = 0 ,
    @numDivisionId AS NUMERIC(9)=0,
    @BaseTaxOn AS Int ,
    @CookieId AS VARCHAR(100),
    @numCountryID AS NUMERIC(9,0),
    @numStateID AS NUMERIC(9,0),
    @vcCity AS VARCHAR(100),
    @vcZipPostalCode AS VARCHAR(20),
    @fltTotalTaxAmount AS FLOAT OUTPUT ,
    @numUserCntId AS NUMERIC(9,0)
)
AS
BEGIN
	Declare  @sqlQuery AS NVARCHAR(MAX) 
	SET @sqlQuery = ''
 
	CREATE TABLE #CartItems
	(
		numItemCode   NUMERIC(9,0) null,
		numTaxItemID NUMERIC(9,0) NULL ,
		numTaxID NUMERIC(18,0) NULL ,
		numUnitHour NUMERIC(18,0) NULL,
		numUOM NUMERIC(18,0) NULL,
		monTotAmount MONEY NULL ,
		numCartId NUMERIC(18,0),
		bitApplicable BIT 
	)
  
	CREATE TABLE #TaxDetail  
	(
		numTaxID   numeric(9,0) ,
		numTaxItemID NUMERIC (9,0) ,
		numCountryID NUMERIC(9,0) ,
		numStateID NUMERIC(9,0),
		decTaxPercentage FLOAT,
		tintTaxType TINYINT,
		numDomainId NUMERIC(9,0),
		vcCity VARCHAR(100) ,
		vcZipPostal VARCHAR(20)
	)

	CREATE TABLE #ApplicableTaxesForDivision
	( 
		vcTaxName   VARCHAR(100) null,
		decTaxPercentage FLOAT NULL,
		tintTaxType TINYINT,
		numTaxItemID NUMERIC(9,0) ,
		numTaxID NUMERIC(18,0),
		numCountryID NUMERIC(9,0) ,
		numStateID NUMERIC(9,0) ,
		numDivisionID NUMERIC(9,0),
		bitApplicable BIT
	)
  
	CREATE TABLE #TaxAmount  
	(
		numItemCode   numeric(9,0) ,
		numTaxItemID NUMERIC (9,0) ,
		bitApplicable BIT ,
		decTaxPercentage FLOAT,
		vcTaxName VARCHAR(100),
		monTotAmount MONEY ,
		bitTaxable Bit ,
		fltTaxAmount FLOAT 
	)

	-- here union  is used to remove dublicate rows 
	INSERT INTO 
		#CartItems 
    SELECT 
		IT.numItemCode , 
		IT.numTaxItemID , 
		ISNULL(IT.numTaxID,0),
		CI.numUnitHour,
		CI.numUOM,
		CI.monTotAmount, 
		CI.numCartId, 
		IT.bitApplicable 
    FROM 
		dbo.ItemTax IT 
	INNER JOIN 
		dbo.CartItems CI 
	ON 
		IT.numItemCode =  CI.numItemCode  
    WHERE  
		CI.vcCookieId = @CookieId 
		AND ((CI.numUserCntId = @numUserCntId) OR (CI.numUserCntId = 0))
        AND CI.numItemCode NOT IN (SELECT numShippingServiceItemID FROM Domain WHERE Domain.numDomainID = CI.numDomainID)
    UNION 
    SELECT 
		CI.numItemCode,
		0,
		0,
		CI.numUnitHour,
		CI.numUOM,
		CI.monTotAmount, 
		CI.numCartId, 
		1 
    FROM 
		dbo.CartItems CI 
	INNER JOIN 
		Item I 
	ON 
		CI.numItemCode = I.numItemCode  
    WHERE  
		CI.vcCookieId = @CookieId 
		AND ((CI.numUserCntId = @numUserCntId) OR (CI.numUserCntId = 0)) 
		AND  I.bitTaxable = 1          
        AND CI.numItemCode NOT IN (SELECT numShippingServiceItemID FROM Domain WHERE Domain.numDomainID = CI.numDomainID)
 
	SELECT * FROM #CartItems
 
	DECLARE @TaxPercentage AS NUMERIC(9,0)
 

	DECLARE @Temp1 TABLE
	(
		row_id int NOT NULL PRIMARY KEY IDENTITY(1,1),
		TaxItemID NUMERIC
	)

	DECLARE @RowNo as INT
	
	INSERT INTO @Temp1 
    (   
        TaxItemID
    )
	SELECT DISTINCT 
		numTaxItemID 
	FROM 
		TaxDetails 
	WHERE 
		numDomainID= @numDomainID

	SELECT * FROM @Temp1

	DECLARE @RowsToProcess int
	DECLARE @CurrentRow int

	SELECT @RowsToProcess = COUNT(*) FROM @Temp1
	SET @CurrentRow = 1
 
	WHILE @CurrentRow <= @RowsToProcess
	BEGIN   
		IF @numCountryID >0 and @numStateID >0
		BEGIN
			IF @numStateID>0        
			BEGIN 
				
			    IF EXISTS(SELECT * FROM TaxDetails TD inner JOIN @Temp1 T ON T.TaxItemID = TD.numTaxItemID WHERE numCountryID=@numCountryID and ((numStateID =@numStateID  AND 
							(1=(Case 
								when @BaseTaxOn=0 then Case When numStateID = @numStateID then 1 else 0 end --State
								when @BaseTaxOn=1 then Case When vcCity like '%' + @vcCity +'%' then 1 else 0 end --City
								when @BaseTaxOn=2 then Case When vcZipPostal=@vcZipPostalCode then 1 else 0 end --Zip/Postal
								else 0 end)) ))
								and numDomainID= @numDomainID and T.row_id = @CurrentRow)
				BEGIN
					INSERT INTO 
						#TaxDetail 
					SELECT TOP 1
						numTaxID, 
						numTaxItemID,  
						numCountryID, 
						numStateID,
						decTaxPercentage,
						tintTaxType,
						numDomainId,
						vcCity,
						vcZipPostal 
					FROM 
						TaxDetails 
					WHERE 
						numCountryID=@numCountryID 
						AND ((numStateID =@numStateID  AND 
							(1=(Case 
								when @BaseTaxOn=0 then Case When numStateID = @numStateID then 1 else 0 end --State
								when @BaseTaxOn=1 then Case When vcCity like '%' + @vcCity +'%' then 1 else 0 end --City
								when @BaseTaxOn=2 then Case When vcZipPostal=@vcZipPostalCode then 1 else 0 end --Zip/Postal
								else 0 end)) ))
								
								and numDomainID= @numDomainID  
								and numTaxItemID  = (Select T.TaxItemID from @Temp1 T where T.row_id = @CurrentRow)   
				END
				ELSE IF exists(select * from TaxDetails TD inner JOIN @Temp1 T on T.TaxItemID = TD.numTaxItemID 
							where numCountryID=@numCountryID and (numStateID=@numStateID )
							and numDomainID= @numDomainID  and isnull(vcCity,'')='' and isnull(vcZipPostal,'')='' and T.row_id = @CurrentRow)            
				BEGIN
					INSERT INTO
						#TaxDetail 
					SELECT  TOP 1
						numTaxID, 
						numTaxItemID,
						numCountryID,
						numStateID,
						decTaxPercentage,
						tintTaxType,
						numDomainId, 
						vcCity,
						vcZipPostal 
					FROM 
						TaxDetails TD
					WHERE 
						numCountryID=@numCountryID 
						AND (numStateID = @numStateID)
						AND numDomainID = @numDomainID 
						AND ISNULL(vcCity,'') = '' AND ISNULL(vcZipPostal,'') = '' AND numTaxItemID = (Select T.TaxItemID from @Temp1 T where T.row_id = @CurrentRow)   
				END
				ELSE    
				BEGIN         
					INSERT INTO 
						#TaxDetail 
					SELECT TOP 1
						numTaxID, 
						numTaxItemID,
						numCountryID,
						numStateID,
						decTaxPercentage,
						tintTaxType,
						numDomainId,
						vcCity,
						vcZipPostal 
					FROM 
						TaxDetails TD
					WHERE 
						numCountryID=@numCountryID 
						AND numStateID=0 
						AND isnull(vcCity,'')='' 
						AND isnull(vcZipPostal,'')='' 
						AND numDomainID= @numDomainID 
						AND numTaxItemID  = (Select T.TaxItemID from @Temp1 T where T.row_id = @CurrentRow)   
				END  
			END                   
		END 
		ELSE IF @numCountryID >0              
		BEGIN 
			INSERT INTO 
				#TaxDetail 
			SELECT TOP 1
				numTaxID, 
				numTaxItemID,
				numCountryID,
				numStateID,
				decTaxPercentage,
				tintTaxType,
				numDomainId,
				vcCity,
				vcZipPostal 
			FROM 
				TaxDetails
			WHERE 
				numCountryID=@numCountryID 
				AND numStateID=0 
				AND ISNULL(vcCity,'')='' 
				AND ISNULL(vcZipPostal,'')='' 
				AND numDomainID= @numDomainID 
				AND numTaxItemID  = (Select T.TaxItemID from @Temp1 T where T.row_id = @CurrentRow)   
		END 

		SET @CurrentRow = @CurrentRow + 1  
	END

	SELECT * FROM #TaxDetail

	INSERT INTO 
		#ApplicableTaxesForDivision
	SELECT 
		CASE WHEN DTD.numTaxItemID = 0 THEN 'Sales Tax' ELSE TI.vcTaxName END AS vcTaxName
		,TD.decTaxPercentage
		,TD.tintTaxType
		,DTD.numTaxItemID 
		,TD.numTaxID
		,TD.numCountryID
		,TD.numStateID
		,DTD.numDivisionID
		,DTD.bitApplicable
    FROM
		dbo.DivisionTaxTypes DTD 
	LEFT JOIN 
		#TaxDetail TD 
	ON 
		DTD.numTaxItemID = TD.numTaxItemID
    LEFT JOIN 
		dbo.TaxItems TI 
	ON 
		TD.numDomainId = TI.numDomainID 
		AND TD.numTaxItemID = TI.numTaxItemID
    WHERE 
		ISNULL(DTD.bitApplicable,0)=1 
		AND numDivisionID=@numDivisionID 
		AND TD.numDomainId= @numDomainID
	UNION
	SELECT 'CRV',TD.decTaxPercentage,TD.tintTaxType,1,numTaxID,TD.numCountryID,TD.numStateID,@numDivisionID,1 FROM #TaxDetail TD WHERE numTaxItemID = 1

	SELECT * FROM  #ApplicableTaxesForDivision
  
	INSERT INTO 
		#TaxAmount
    SELECT 
		I.numItemCode, 
		TT.numTaxItemID,
		TT.bitApplicable, 
		TT1.decTaxPercentage,
		TT1.vcTaxName,
		TT.monTotAmount,
		I.bitTaxable, 
		CASE 
			WHEN TT1.tintTaxType = 2 --FLAT AMOUNT
			THEN (CONVERT(FLOAT, TT1.decTaxPercentage) * (CONVERT(FLOAT,numUnitHour) * dbo.fn_UOMConversion(ISNULL(I.numBaseUnit,0),I.numItemCode,@numDomainID,ISNULL(TT.numUOM, 0))))
			ELSE --PERCENT
				((CONVERT(FLOAT, TT.monTotAmount)   * CONVERT(FLOAT, TT1.decTaxPercentage))/100) 
		END AS fltTaxAmount  
	FROM 
		#CartItems TT
	INNER JOIN 
		#ApplicableTaxesForDivision TT1 
	ON 
		TT.numTaxItemID = TT1.numTaxItemID 
		AND 1 = (CASE WHEN TT.numTaxItemID = 1 THEN (CASE WHEN TT.numTaxID = TT1.numTaxID THEN 1 ELSE 0 END) ELSE 1 END)
    INNER JOIN 
		Item I 
	ON 
		I.numItemCode = TT.numItemCode  
	WHERE 
		I.bitTaxable = 1 ORDER BY numItemCode
  
	SELECT numItemCode,numTaxItemID ,bitApplicable  ,fltTaxAmount AS fltTaxAmount  FROM  #TaxAmount
	SELECT numItemCode ,SUM(fltTaxAmount) AS fltTaxAmount  FROM  #TaxAmount GROUP BY numItemCode 
  
  
	IF EXISTS(SELECT * FROM #TaxAmount )
	BEGIN
		SELECT @fltTotalTaxAmount =  SUM(fltTaxAmount)  FROM  #TaxAmount  WHERE bitTaxable = 1 
	END

	DROP TABLE #CartItems
	DROP TABLE #ApplicableTaxesForDivision
	DROP TABLE #TaxDetail
	DROP TABLE #TaxAmount
 
 END
GO

--exec USP_GetTaxOfCartItems @numDomainId=1,@numDivisionId=1,@BaseTaxOn=2,@CookieId='bbe5f0f4-2791-461f-a5fa-d7d33cf2f7f0',@numCountryID=600,@numStateID=112,@vcCity='Ahmedabad',@vcZipPostalCode='361305',@fltTotalTaxAmount=0,@numUserCntId=1
/****** Object:  StoredProcedure [dbo].[USP_GetWareHouseItems]    Script Date: 07/26/2008 16:18:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec USP_GetWareHouseItems 6                            
--created by anoop jayaraj                              
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getwarehouseitems')
DROP PROCEDURE usp_getwarehouseitems
GO
CREATE PROCEDURE [dbo].[USP_GetWareHouseItems]                              
@numItemCode as numeric(9)=0,
@byteMode as tinyint=0,
@numWarehouseItemID AS NUMERIC(18,0) = NULL                             
as                              
                              
                          
                              
declare @bitSerialize as bit                      
declare @str as nvarchar(max)                 
declare @str1 as nvarchar(max)               
declare @ColName as varchar(500)                      
set @str=''                       

DECLARE @numDomainID AS INT 
DECLARE @numBaseUnit AS NUMERIC(18,0)
DECLARE @vcUnitName as VARCHAR(100) 
DECLARE @numSaleUnit AS NUMERIC(18,0)
DECLARE @vcSaleUnitName as VARCHAR(100) 
DECLARE @numPurchaseUnit AS NUMERIC(18,0)
DECLARE @vcPurchaseUnitName as VARCHAR(100)
DECLARE @bitLot AS BIT                     
DECLARE @bitKitParent BIT
declare @numItemGroupID as numeric(9)                        
                        
select 
	@numDomainID=numDomainID,
	@numBaseUnit = ISNULL(numBaseUnit,0),
	@numSaleUnit = ISNULL(numSaleUnit,0),
	@numPurchaseUnit = ISNULL(numPurchaseUnit,0),
	@numItemGroupID=numItemGroup,
	@bitLot = bitLotNo,
	@bitSerialize=CASE WHEN bitSerialized=0 THEN bitLotNo ELSE bitSerialized END,
	@bitKitParent = ( CASE 
						WHEN ISNULL(bitKitParent,0) = 1 AND ISNULL(bitAssembly,0) = 1 THEN 0
                        WHEN ISNULL(bitKitParent,0) = 1 THEN 1
                        ELSE 0
                     END ) 
FROM 
	Item 
WHERE 
	numItemCode=@numItemCode     
	
SELECT @vcUnitName=ISNULL(vcUnitName,'-') FROM UOM u WHERE u.numUOMId=@numBaseUnit
SELECT @vcSaleUnitName=ISNULL(vcUnitName,'-') FROM UOM u WHERE u.numUOMId=@numSaleUnit
SELECT @vcPurchaseUnitName=ISNULL(vcUnitName,'-') FROM UOM u WHERE u.numUOMId=@numPurchaseUnit
	
DECLARE @numSaleUOMFactor AS DECIMAL(28,14)
DECLARE @numPurchaseUOMFactor AS DECIMAL(28,14)

SELECT @numSaleUOMFactor = dbo.fn_UOMConversion(@numBaseUnit,@numItemCode,@numDOmainID,@numSaleUnit)
SELECT @numPurchaseUOMFactor = dbo.fn_UOMConversion(@numBaseUnit,@numItemCode,@numDOmainID,@numPurchaseUnit)


SET @ColName='WareHouseItems.numWareHouseItemID,0'              
              
--Create a Temporary table to hold data                                                            
create table #tempTable ( ID INT IDENTITY PRIMARY KEY,                                                                      
 numCusFlDItemID numeric(9)                                                         
 )                         
                        
insert into #tempTable                         
(numCusFlDItemID)                                                            
select distinct(numOppAccAttrID) from ItemGroupsDTL where numItemGroupID=@numItemGroupID and tintType=2                       
                          
                 
 declare @ID as numeric(9)                        
 declare @numCusFlDItemID as varchar(20)                        
 declare @fld_label as varchar(100),@fld_type as varchar(100)                        
 set @ID=0                        
 select top 1 @ID=ID,@numCusFlDItemID=numCusFlDItemID,@fld_label=fld_label,@fld_type=fld_type from #tempTable                         
 join CFW_Fld_Master on numCusFlDItemID=Fld_ID                        
                         
 while @ID>0                        
 begin                        
                          
   set @str=@str+',  dbo.GetCustFldItems('+@numCusFlDItemID+',9,'+@ColName+') as ['+ @fld_label+']'
	
	IF @byteMode=1                                        
		set @str=@str+',  dbo.GetCustFldItemsValue('+@numCusFlDItemID+',9,'+@ColName+','''+@fld_type+''') as ['+ @fld_label+'Value]'                                        
                          
   select top 1 @ID=ID,@numCusFlDItemID=numCusFlDItemID,@fld_label=fld_label from #tempTable                         
   join CFW_Fld_Master on numCusFlDItemID=Fld_ID and ID >@ID                        
   if @@rowcount=0 set @ID=0                        
                          
 end                        
                       
--      

DECLARE @KitOnHand AS NUMERIC(9);SET @KitOnHand=0

--IF @bitKitParent=1
	--SELECT @KitOnHand=ISNULL(dbo.fn_GetKitInventory(@numItemCode),0)          
  
set @str1='select '

IF @byteMode=1                                        
		set @str1 =@str1 +'I.numItemCode,'
	
set @str1 =@str1 + 'numWareHouseItemID,WareHouseItems.numWareHouseID,
ISNULL(vcWarehouse,'''') + (CASE WHEN ISNULL(WL.vcLocation,'''') = '''' THEN '''' ELSE '', '' + isnull(WL.vcLocation,'''') END) vcWarehouse,
ISNULL(vcWarehouse,'''') AS vcExternalLocation,
(CASE WHEN WareHouseItems.numWLocationID = -1 THEN ''Global'' ELSE ISNULL(WL.vcLocation,'''') END) AS vcInternalLocation,
W.numWareHouseID,
Case when @bitKitParent=1 then CAST(ISNULL(dbo.fn_GetKitInventoryByWarehouse(I.numItemCode, W.numWareHouseID),0) AS DECIMAL(18,2)) ELSE CAST(ISNULL(numOnHand,0) AS FLOAT) END AS [OnHand],
CAST((@numPurchaseUOMFactor * Case when @bitKitParent=1 THEN ISNULL(dbo.fn_GetKitInventoryByWarehouse(I.numItemCode, W.numWareHouseID),0) ELSE isnull(numOnHand,0) end) as FLOAT) as PurchaseOnHand,
CAST((@numSaleUOMFactor * Case when @bitKitParent=1 THEN ISNULL(dbo.fn_GetKitInventoryByWarehouse(I.numItemCode, W.numWareHouseID),0) ELSE isnull(numOnHand,0) end) as FLOAT) as SalesOnHand,
Case when @bitKitParent=1 THEN 0 ELSE CAST(ISNULL(numReorder,0) AS FLOAT) END as [Reorder] 
,Case when @bitKitParent=1 THEN 0 ELSE CAST(ISNULL(numOnOrder,0) AS FLOAT) END as [OnOrder]
,Case when @bitKitParent=1 THEN 0 ELSE CAST(ISNULL(numAllocation,0) AS FLOAT) END as [Allocation] 
,Case when @bitKitParent=1 THEN 0 ELSE CAST(ISNULL(numBackOrder,0) AS FLOAT) END as [BackOrder],
CAST((@numSaleUOMFactor * Case when @bitKitParent=1 THEN ISNULL(dbo.fn_GetKitInventoryByWarehouse(I.numItemCode, W.numWareHouseID),0) ELSE isnull(numOnHand,0) end) as numeric(18,2)) as OnHandUOM,
CAST((@numPurchaseUOMFactor * Case when @bitKitParent=1 THEN 0 ELSE isnull(numOnOrder,0) END) AS DECIMAL(18,2)) as OnOrderUOM,
CAST((@numPurchaseUOMFactor * Case when @bitKitParent=1 THEN 0 ELSE isnull(numReorder,0) END) AS DECIMAL(18,2)) as ReorderUOM,
CAST((@numSaleUOMFactor * Case when @bitKitParent=1 THEN 0 ELSE isnull(numAllocation,0) END) AS DECIMAL(18,2)) as AllocationUOM,
CAST((@numSaleUOMFactor * Case when @bitKitParent=1 THEN 0 ELSE isnull(numBackOrder,0) END) AS DECIMAL(18,2)) as BackOrderUOM,
@vcUnitName As vcBaseUnit,
@vcSaleUnitName As vcSaleUnit,
@vcPurchaseUnitName As vcPurchaseUnit
,0 as Op_Flag , isnull(WL.vcLocation,'''') Location,isnull(WL.numWLocationID,0) as numWLocationID,
round(isnull(monWListPrice,0),2) Price,isnull(vcWHSKU,'''') as SKU,isnull(vcBarCode,'''') as BarCode '+ @str +'                   
,(SELECT CASE WHEN ISNULL(subI.bitKitParent,0) = 1 AND ISNULL((SELECT COUNT(*) FROM OpportunityItems 
															   WHERE OpportunityItems.numItemCode = WareHouseItems.numItemID
															   AND WareHouseItems.numWareHouseItemID = OpportunityItems.numWarehouseItmsID),0) = 0 THEN 1
			ELSE 0 
	    END 
FROM Item subI WHERE subI.numItemCode = WareHouseItems.numItemID) [IsDeleteKitWarehouse],@bitKitParent [bitKitParent],
(CASE WHEN (SELECT COUNT(*) FROM ItemDetails WHERE numChildItemID=WareHouseItems.numItemID AND numWareHouseItemId = WareHouseItems.numWareHouseItemID) > 0 THEN 1 ELSE 0 END) AS bitChildItemWarehouse
,' + (CASE WHEN @bitSerialize=1 THEN CONCAT('dbo.GetWarehouseSerialLot(',@numDomainID,',WareHouseItems.numWareHouseItemID,',@bitLot,')') ELSE '''''' END) + ' AS vcSerialLot,
CASE 
	WHEN ISNULL(I.numItemGroup,0) > 0 
	THEN dbo.fn_GetAttributes(WareHouseItems.numWareHouseItemID,0)
	ELSE ''''
END AS vcAttribute 
from WareHouseItems                           
join Warehouses W                             
on W.numWareHouseID=WareHouseItems.numWareHouseID 
left join WarehouseLocation WL on WL.numWLocationID =WareHouseItems.numWLocationID
join Item I on I.numItemCode=WareHouseItems.numItemID and WareHouseItems.numDomainId=I.numDomainId
where numItemID='+convert(varchar(15),@numItemCode) + ' AND (WareHouseItems.numWarehouseItemID = ' + CAST(@numWarehouseItemID AS VARCHAR(20)) + ' OR ' + CAST(ISNULL(@numWarehouseItemID,0) AS VARCHAR(20)) + ' = 0 )'              
   
print(@str1)           

EXECUTE sp_executeSQL @str1, N'@bitKitParent bit,@numSaleUOMFactor DECIMAL(28,14), @numPurchaseUOMFactor DECIMAL(28,14), @vcUnitName VARCHAR(100), @vcSaleUnitName VARCHAR(100), @vcPurchaseUnitName VARCHAR(100)', @bitKitParent, @numSaleUOMFactor, @numPurchaseUOMFactor, @vcUnitName, @vcSaleUnitName, @vcPurchaseUnitName
                       
                        
set @str1='select numWareHouseItmsDTLID,WDTL.numWareHouseItemID, vcSerialNo,WDTL.vcComments as Comments,WDTL.numQty, WDTL.numQty as OldQty,ISNULL(W.vcWarehouse,'''') + (CASE WHEN ISNULL(WL.vcLocation,'''') = '''' THEN '''' ELSE '', '' + isnull(WL.vcLocation,'''') END) vcWarehouse '+ case when @bitSerialize=1 then @str else '' end +'
,WDTL.dExpirationDate, WDTL.bitAddedFromPO
from WareHouseItmsDTL WDTL                             
join WareHouseItems                             
on WDTL.numWareHouseItemID=WareHouseItems.numWareHouseItemID                              
join Warehouses W                             
on W.numWareHouseID=WareHouseItems.numWareHouseID  
left join WarehouseLocation WL on WL.numWLocationID =WareHouseItems.numWLocationID
where ISNULL(numQty,0) > 0 and numItemID='+ convert(varchar(15),@numItemCode) + ' AND (WareHouseItems.numWarehouseItemID = ' + CAST(@numWarehouseItemID AS VARCHAR(20)) + ' OR ' + CAST(ISNULL(@numWarehouseItemID,0) AS VARCHAR(20)) + ' = 0 )'                          
print @str1                       
exec (@str1)                       
                      
                      
select Fld_label,fld_id,fld_type,numlistid,vcURL from #tempTable                         
 join CFW_Fld_Master on numCusFlDItemID=Fld_ID                      
drop table #tempTable


-----------------For Kendo UI testting purpose
--create table #tempColumnConfig ( field varchar(100),                                                                      
-- title varchar(100) , format varchar(100)                                                      
-- )  
--
--insert into #tempColumnConfig
--	select 'vcWarehouse','WareHouse','' union all
--	select 'OnHand','On Hand','' union all
--	select 'Reorder','Re Order','' union all
--	select 'Allocation','Allocation','' union all
--	select 'BackOrder','Back Order','' union all
--	select 'Price','Price','{0:c}'
--
--select * from #tempColumnConfig
--
--drop table #tempColumnConfig

-----------------

--set @str1='select case when COUNT(*)=1 then MIN(numWareHouseItmsDTLID) else 0 end as numWareHouseItmsDTLID,MIN(WDTL.numWareHouseItemID) as numWareHouseItemID, vcSerialNo,0 as Op_Flag,WDTL.vcComments as Comments'+ case when @bitSerialize=1 then @str else '' end +',W.vcWarehouse,COUNT(*) Qty
--from WareHouseItmsDTL WDTL                             
--join WareHouseItems WI                             
--on WDTL.numWareHouseItemID=WI.numWareHouseItemID                              
--join Warehouses W                             
--on W.numWareHouseID=WI.numWareHouseID  
--where (tintStatus is null or tintStatus=0)  and  numItemID='+ convert(varchar(15),@numItemCode) +'
--GROUP BY vcSerialNo,WDTL.vcComments,W.vcWarehouse'
--                          
--print @str1                       
--exec (@str1)                       
              
GO

/****** Object:  StoredProcedure [dbo].[USP_InsertCartItem]    Script Date: 11/08/2011 17:53:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_InsertCartItem' ) 
                    DROP PROCEDURE USP_InsertCartItem
                    Go
CREATE  PROCEDURE [dbo].[USP_InsertCartItem]
(
    @numCartId AS NUMERIC OUTPUT,
    @numUserCntId NUMERIC(18, 0),
    @numDomainId NUMERIC(18, 0),
    @vcCookieId VARCHAR(100),
    @numOppItemCode NUMERIC(18, 0),
    @numItemCode NUMERIC(18, 0),
    @numUnitHour NUMERIC(18, 0),
    @monPrice MONEY,
    @numSourceId NUMERIC(18, 0),
    @vcItemDesc VARCHAR(2000),
    @numWarehouseId NUMERIC(18, 0),
    @vcItemName VARCHAR(200),
    @vcWarehouse VARCHAR(200),
    @numWarehouseItmsID NUMERIC(18, 0),
    @vcItemType VARCHAR(200),
    @vcAttributes VARCHAR(100),
    @vcAttrValues VARCHAR(100),
    @bitFreeShipping BIT,
    @numWeight NUMERIC(18, 2),
    @tintOpFlag TINYINT,
    @bitDiscountType BIT,
    @fltDiscount DECIMAL(18,2),
    @monTotAmtBefDiscount MONEY,
    @ItemURL VARCHAR(200),
    @numUOM NUMERIC(18, 0),
    @vcUOMName VARCHAR(200),
    @decUOMConversionFactor DECIMAL(18, 0),
    @numHeight NUMERIC(18, 0),
    @numLength NUMERIC(18, 0),
    @numWidth NUMERIC(18, 0),
    @vcShippingMethod VARCHAR(200),
    @numServiceTypeId NUMERIC(18, 0),
    @decShippingCharge DECIMAL(18, 2),
    @numShippingCompany NUMERIC(18, 0),
    @tintServicetype TINYINT,
    @dtDeliveryDate DATETIME,
    @monTotAmount money,
	@postselldiscount INT=0,
	@numSiteID NUMERIC(18,0) = 0,
	@numPromotionID NUMERIC(18,0) = 0,
	@vcPromotionDescription VARCHAR(MAX) = ''
)
AS 
BEGIN TRY
BEGIN TRANSACTION;
	
	INSERT INTO [dbo].[CartItems]
    (
        [numUserCntId],
        [numDomainId],
        [vcCookieId],
        [numOppItemCode],
        [numItemCode],
        [numUnitHour],
        [monPrice],
        [numSourceId],
        [vcItemDesc],
        [numWarehouseId],
        [vcItemName],
        [vcWarehouse],
        [numWarehouseItmsID],
        [vcItemType],
        [vcAttributes],
        [vcAttrValues],
        [bitFreeShipping],
        [numWeight],
        [tintOpFlag],
        [bitDiscountType],
        [fltDiscount],
        [monTotAmtBefDiscount],
        [ItemURL],
        [numUOM],
        [vcUOMName],
        [decUOMConversionFactor],
        [numHeight],
        [numLength],
        [numWidth],
        [vcShippingMethod],
        [numServiceTypeId],
        [decShippingCharge],
        [numShippingCompany],
        [tintServicetype],
        [monTotAmount],
		[PromotionID],
		[PromotionDesc]
    )
	VALUES  
	(
        @numUserCntId,
        @numDomainId,
        @vcCookieId,
        @numOppItemCode,
        @numItemCode,
        @numUnitHour,
        @monPrice,
        @numSourceId,
        @vcItemDesc,
        @numWarehouseId,
        @vcItemName,
        @vcWarehouse,
        @numWarehouseItmsID,
        @vcItemType,
        @vcAttributes,
        @vcAttrValues,
        @bitFreeShipping,
        @numWeight,
        @tintOpFlag,
        @bitDiscountType,
        @fltDiscount,
        @monTotAmtBefDiscount,
        @ItemURL,
        @numUOM,
        @vcUOMName,
        @decUOMConversionFactor,
        @numHeight,
        @numLength,
        @numWidth,
        @vcShippingMethod,
        @numServiceTypeId,
        @decShippingCharge,
        @numShippingCompany,
        @tintServicetype,
        @monTotAmount,
		@numPromotionID,
		@vcPromotionDescription
    )

	SET @numCartId = SCOPE_IDENTITY()

	DECLARE @numItemClassification As NUMERIC(18,0)

	SELECT 
		@numItemClassification=numItemClassification
	FROM
		Item
	WHERE
		numItemCode=@numItemCode


	IF(@postselldiscount=0)
	BEGIN

		/************************** First check if any promotion can be applied *********************************/
		DECLARE @TEMPPromotion TABLE
		(
			ID INT IDENTITY(1,1),
			numPromotionID NUMERIC(18,0),
			vcPromotionDescription VARCHAR(MAX)
		)

		INSERT INTO 
			@TEMPPromotion
		SELECT
			CI.PromotionID,
			CI.PromotionDesc
		FROM 
			CartItems CI
		INNER JOIN
			PromotionOffer PO
		ON
			CI.PromotionID = PO.numProId
		WHERE
			CI.numDomainId=@numDomainId
			AND CI.numUserCntId=@numUserCntId
			AND CI.vcCookieId=@vcCookieId
			AND ISNULL(CI.bitParentPromotion,0)=1
			AND ISNULL(CI.PromotionID,0) > 0
			AND 1=(CASE PO.tintDiscoutBaseOn
					 WHEN 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems POI WHERE POI.numProId=PO.numProId AND POI.numValue=@numItemCode AND tintRecordType=6 AND tintType=1) > 0 THEN 1 ELSE 0 END)
					 WHEN 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems POI WHERE POI.numProId=PO.numProId AND POI.numValue=@numItemClassification AND tintRecordType=6 AND tintType=2) > 0 THEN 1 ELSE 0 END)
					 WHEN 3 THEN (CASE PO.tintOfferBasedOn 
									WHEN 1 THEN (CASE WHEN (SELECT COUNT(*) FROM SimilarItems WHERE numItemCode=@numItemCode AND numParentItemCode IN (SELECT numValue FROM PromotionOfferItems POI WHERE POI.numProId=PO.numProId AND tintRecordType=5 AND tintType=1)) > 0 THEN 1 ELSE 0 END)
									WHEN 2 THEN (CASE WHEN (SELECT COUNT(*) FROM SimilarItems WHERE numItemCode=@numItemCode AND numParentItemCode IN (SELECT numItemCode FROM Item WHERE numDomainID=@numDomainId AND numItemClassification IN (SELECT numValue FROM PromotionOfferItems POI WHERE POI.numProId=PO.numProId AND tintRecordType=5 AND tintType=2))) > 0 THEN 1 ELSE 0 END)
									ELSE 0
								END) 
					ELSE 0
				END)
		ORDER BY
			numCartId ASC

		DECLARE @fltDiscountValue FLOAT
		DECLARE @tintDiscountType INT
		DECLARE @tintDiscoutBaseOn INT
		DECLARE @IsPromotionApplied BIT = 0

		DECLARE @Count AS INT
		SELECT @Count=COUNT(*) FROM @TEMPPromotion

		IF @Count > 0
		BEGIN
			DECLARE @i AS INT = 1
		
			WHILE @i <= @Count
			BEGIN
				SELECT @numPromotionID=numPromotionID,@vcPromotionDescription=vcPromotionDescription FROM @TEMPPromotion WHERE ID=@i

				SELECT
					@tintDiscountType=tintDiscountType,
					@fltDiscountValue=fltDiscountValue
				FROM
					PromotionOffer 
				WHERE
					numProId=@numPromotionID
			
				IF @tintDiscountType=1 --Percentage
				BEGIN
					UPDATE 
						CartItems
					SET
						PromotionID=@numPromotionID,
						PromotionDesc=@vcPromotionDescription,
						bitParentPromotion=0,
						fltDiscount=@fltDiscountValue,
						bitDiscountType=0,
						monTotAmount=(monTotAmount-((monTotAmount*@fltDiscountValue)/100))
					WHERE
						numCartId=@numCartId

					SET @IsPromotionApplied = 1
					BREAK
				END
				ELSE IF @tintDiscountType=2 --Flat Amount
				BEGIN
					-- FIRST CHECK WHETHER DISCOUNT AMOUNT IS NOT ALREADY USED BY OTHER ITEMS
					DECLARE @usedDiscountAmount AS FLOAT
				
					SELECT
						@usedDiscountAmount = SUM(fltDiscount)
					FROM
						CartItems
					WHERE
						PromotionID=@numPromotionID

					IF @usedDiscountAmount < @fltDiscountValue
					BEGIN
						UPDATE 
							CartItems 
						SET 
							monTotAmount=(CASE WHEN (@fltDiscountValue - @usedDiscountAmount)  >= monTotAmount THEN 0 ELSE (monTotAmount-(@fltDiscountValue - @usedDiscountAmount)) END),
							PromotionID=@numPromotionID ,
							PromotionDesc=@vcPromotionDescription,
							bitDiscountType=1,
							fltDiscount=(CASE WHEN (@fltDiscountValue - @usedDiscountAmount) >= monTotAmount THEN monTotAmount ELSE (@fltDiscountValue - @usedDiscountAmount) END)
						WHERE 
							numCartId=@numCartId

						SET @IsPromotionApplied = 1
						BREAK
					END
				END
				ELSE IF @tintDiscountType=3 --Quantity
				BEGIN
					-- FIRST CHECK WHETHER DISCOUNT QTY IS NOT ALREADY USED BY OTHER ITEMS
					DECLARE @usedDiscountQty AS FLOAT

					SELECT
						@usedDiscountQty = SUM(fltDiscount/monPrice)
					FROM
						CartItems
					WHERE
						PromotionID=@numPromotionID

					IF @usedDiscountQty < @fltDiscountValue
					BEGIN
						UPDATE 
							CartItems 
						SET 
							monTotAmount=(CASE WHEN (@fltDiscountValue - @usedDiscountQty)  >= numUnitHour THEN 0 ELSE (monTotAmount-((@fltDiscountValue - @usedDiscountQty)*monPrice)) END),
							PromotionID=@numPromotionID ,
							PromotionDesc=@vcPromotionDescription,
							bitDiscountType=1,
							fltDiscount=(CASE WHEN (@fltDiscountValue - @usedDiscountQty) >= numUnitHour THEN (numUnitHour*monPrice) ELSE ((@fltDiscountValue - @usedDiscountQty) *monPrice) END) 
						WHERE 
							numCartId=@numCartId

						SET @IsPromotionApplied = 1
						BREAK
					END

				END

				SET @i = @i + 1
			END
		END


		IF @IsPromotionApplied = 0
		BEGIN
			/************************** Check if any promotion can be trigerred *********************************/
			EXEC USP_ECommerceTiggerPromotion @numDomainID,@numUserCntId,@vcCookieId,@numCartID,@numItemCode,@numItemClassification,@numUnitHour,@monPrice,@numSiteID
		
		END

	END


	SELECT @numCartId

 COMMIT TRANSACTION;
  END TRY
  BEGIN CATCH
    IF @@TRANCOUNT > 0
    ROLLBACK TRANSACTION;

    DECLARE @ErrorNumber INT = ERROR_NUMBER();
    DECLARE @ErrorLine INT = ERROR_LINE();
    DECLARE @ErrorMessage NVARCHAR(4000) = ERROR_MESSAGE();
    DECLARE @ErrorSeverity INT = ERROR_SEVERITY();
    DECLARE @ErrorState INT = ERROR_STATE();

    PRINT 'Actual error number: ' + CAST(@ErrorNumber AS VARCHAR(10));
    PRINT 'Actual line number: ' + CAST(@ErrorLine AS VARCHAR(10));

    RAISERROR(@ErrorMessage, @ErrorSeverity, @ErrorState);
  END CATCH
            
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_Item_SearchForSalesOrder' )
    DROP PROCEDURE USP_Item_SearchForSalesOrder
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Sandeep Patel
-- Create date: 3 March 2014
-- Description:	Search items for sales order
-- =============================================
CREATE PROCEDURE USP_Item_SearchForSalesOrder
    @tintOppType AS TINYINT ,
    @numDomainID AS NUMERIC(9) ,
    @numDivisionID AS NUMERIC(9) = 0 ,
    @str AS VARCHAR(1000) ,
    @numUserCntID AS NUMERIC(9) ,
    @tintSearchOrderCustomerHistory AS TINYINT = 0 ,
    @numPageIndex AS INT ,
    @numPageSize AS INT ,
    @WarehouseID AS NUMERIC(18, 0) = NULL ,
    @TotalCount AS INT OUTPUT
AS
    BEGIN
  SET NOCOUNT ON;

DECLARE @strNewQuery NVARCHAR(MAX)
DECLARE @strSQL AS NVARCHAR(MAX)
SELECT  @str = REPLACE(@str, '''', '''''')

DECLARE @TableRowCount TABLE ( Value INT );
	
SELECT  *
INTO    #Temp1
FROM    ( SELECT    numFieldId ,
                    vcDbColumnName ,
                    ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                    tintRow AS tintOrder ,
                    0 AS Custom
          FROM      View_DynamicColumns
          WHERE     numFormId = 22
                    AND numUserCntID = @numUserCntID
                    AND numDomainID = @numDomainID
                    AND tintPageType = 1
                    AND bitCustom = 0
                    AND ISNULL(bitSettingField, 0) = 1
                    AND numRelCntType = 0
          UNION
          SELECT    numFieldId ,
                    vcFieldName ,
                    vcFieldName ,
                    tintRow AS tintOrder ,
                    1 AS Custom
          FROM      View_DynamicCustomColumns
          WHERE     Grp_id = 5
                    AND numFormId = 22
                    AND numUserCntID = @numUserCntID
                    AND numDomainID = @numDomainID
                    AND tintPageType = 1
                    AND bitCustom = 1
                    AND numRelCntType = 0
        ) X 
  
IF NOT EXISTS ( SELECT  *
                FROM    #Temp1 )
    BEGIN
        INSERT  INTO #Temp1
                SELECT  numFieldId ,
                        vcDbColumnName ,
                        ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                        tintorder ,
                        0
                FROM    View_DynamicDefaultColumns
                WHERE   numFormId = 22
                        AND bitDefault = 1
                        AND ISNULL(bitSettingField, 0) = 1
                        AND numDomainID = @numDomainID 
    END

CREATE TABLE #tempItemCode ( numItemCode NUMERIC(9) )
DECLARE @bitRemoveVendorPOValidation AS BIT
SELECT  
	@bitRemoveVendorPOValidation = ISNULL(bitRemoveVendorPOValidation,0)
FROM    
	domain
WHERE  
	numDomainID = @numDomainID

IF @tintOppType > 0
    BEGIN 
        IF CHARINDEX(',', @str) > 0
        BEGIN
			DECLARE @itemSearchText VARCHAR(100)
            DECLARE @vcAttribureSearch VARCHAR(MAX)
			DECLARE @vcAttribureSearchCondition VARCHAR(MAX)

			SET @itemSearchText = LTRIM(RTRIM(SUBSTRING(@str,0,CHARINDEX(',',@str))))

			--GENERATES ATTRIBUTE SEARCH CONDITION
			SET @vcAttribureSearch = SUBSTRING(@str,CHARINDEX(',',@str) + 1,LEN(@str))
	
			DECLARE @attributeSearchText VARCHAR(MAX)
			DECLARE @TempTable TABLE (vcValue VARCHAR(MAX))

			WHILE LEN(@vcAttribureSearch) > 0
			BEGIN
				SET @attributeSearchText = LEFT(@vcAttribureSearch, 
										ISNULL(NULLIF(CHARINDEX(',', @vcAttribureSearch) - 1, -1),
										LEN(@vcAttribureSearch)))
				SET @vcAttribureSearch = SUBSTRING(@vcAttribureSearch,
												ISNULL(NULLIF(CHARINDEX(',', @vcAttribureSearch), 0),
												LEN(@vcAttribureSearch)) + 1, LEN(@vcAttribureSearch))

				INSERT INTO @TempTable (vcValue) VALUES ( @attributeSearchText )
			END
		
			--REMOVES WHITE SPACES
			UPDATE
				@TempTable
			SET
				vcValue = LTRIM(RTRIM(vcValue))
	
			--CONVERTING TABLE TO COMMA SEPERATED STRING AFTER REMOVEING WHITE SPACES
			SELECT @vcAttribureSearch = COALESCE(@vcAttribureSearch,'') +  CONVERT(VARCHAR(MAX),vcValue) + ',' FROM @TempTable
	
			--REMOVE LAST COMMA FROM FINAL SEARCH STRING
			SET @vcAttribureSearch = LTRIM(RTRIM(@vcAttribureSearch))
			IF DATALENGTH(@vcAttribureSearch) > 0
				SET @vcAttribureSearch = LEFT(@vcAttribureSearch, LEN(@vcAttribureSearch) - 1)
	
			--START: GENERATES ATTRIBUTE SEARCH CONDITION
			SELECT 	@vcAttribureSearchCondition = '(TEMPMATRIX.Attributes LIKE ''%' + REPLACE(@vcAttribureSearch, ',', '%'' AND TEMPMATRIX.Attributes LIKE ''%') + '%'')' 

			--LOGIC FOR GENERATING FINAL SQL STRING
			IF @tintOppType = 1 OR ( @bitRemoveVendorPOValidation = 1 AND @tintOppType = 2)
                BEGIN      

				SET @strSQL = 'SELECT
									I.numItemCode,
									I.numVendorID,
									MIN(WHT.numWareHouseItemID) as numWareHouseItemID,
									dbo.fn_GetAttributes(WHT.numWareHouseItemId,I.bitSerialized) AS [Attributes]
								FROM
									dbo.Item I WITH (NOLOCK)
								LEFT JOIN
									dbo.WareHouseItems WHT
								ON
									I.numItemCode = WHT.numItemID
								WHERE 
									((ISNULL(I.numItemGroup,0) > 0 AND I.vcItemName LIKE ''%' + @itemSearchText + '%'') OR (ISNULL(I.numItemGroup,0) = 0 AND I.vcItemName LIKE ''%' + @str + '%''))
									AND ISNULL(I.Isarchieve, 0) <> 1
									AND I.charItemType NOT IN ( ''A'' )
									AND I.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + '
									AND (WHT.numWareHouseID = ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0)'

				--- added Asset validation  by sojan
                IF @bitRemoveVendorPOValidation = 1
                    AND @tintOppType = 2
                    SET @strSQL = @strSQL
                        + ' and ISNULL(I.bitKitParent,0) = 0 AND ISNULL(I.bitAssembly,0) = 0 '

                IF @tintSearchOrderCustomerHistory = 1 AND @numDivisionID > 0
                BEGIN
                    SET @strSQL = @strSQL
                        + ' and I.numItemCode in (select distinct oppI.numItemCode from 
						OpportunityMaster oppM join OpportunityItems oppI on oppM.numOppId=oppI.numOppId where oppM.numDomainID='
                        + CONVERT(VARCHAR(20), @numDomainID)
                        + ' and oppM.numDivisionId='
                        + CONVERT(VARCHAR(20), @numDivisionId)
                        + ')'
                END

				SET @strSQL = @strSQL + ' GROUP BY
										I.numItemCode,
										I.numVendorID,
										dbo.fn_GetAttributes(numWareHouseItemId,I.bitSerialized)'

				SET @strSQL = 
				'
				SELECT    
					SRNO=ROW_NUMBER() OVER( ORDER BY  I.vcItemName),
					I.numItemCode,
					(SELECT TOP 1 vcPathForImage FROM ItemImages WHERE numItemCode = I.numItemCode) AS vcPathForImage ,
					(SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = I.numItemCode) AS vcPathForTImage ,
					ISNULL(vcItemName, '''') vcItemName ,
					CASE WHEN I.[charItemType] = ''P''
					THEN CONVERT(VARCHAR(100), ISNULL((SELECT TOP 1 ROUND(monWListPrice,2) FROM [WareHouseItems] WHERE [numItemID] = I.numItemCode), 0))
					ELSE CONVERT(VARCHAR(100), ROUND(monListPrice, 2))
					END AS monListPrice,
					ISNULL(vcSKU, '''') vcSKU ,
					ISNULL(numBarCodeId, 0) numBarCodeId ,
					ISNULL(vcModelID, '''') vcModelID ,
					ISNULL(txtItemDesc, '''') AS txtItemDesc ,
					ISNULL(C.vcCompanyName, '''') AS vcCompanyName,
					TEMPMATRIX.numWareHouseItemID,
					TEMPMATRIX.Attributes AS [vcAttributes],
					(CASE 
						WHEN ISNULL(I.bitKitParent,0) = 1 
						THEN 
							(
								CASE
									WHEN (SELECT COUNT(*) FROM ItemDetails INNER JOIN Item ON ItemDetails.numChildItemID = Item.numItemCode WHERE numItemKitID=I.numItemCode AND ISNULL(Item.bitKitParent,0) = 1) > 0
									THEN 1
									ELSE 0
								END
							)
						ELSE 0 
					END) bitHasKitAsChild
				FROM  
					(' + @strSQL +') AS TEMPMATRIX
				INNER JOIN    
					Item I
				ON
					TEMPMATRIX.numItemCode = I.numItemCode
				LEFT JOIN 
					DivisionMaster D 
				ON 
					I.numVendorID = D.numDivisionID
				LEFT JOIN 
					CompanyInfo C 
				ON 
					C.numCompanyID = D.numCompanyID
				WHERE  
				  ((ISNULL(I.numItemGroup,0) = 0 AND I.vcItemName LIKE ''%' + @str + '%'') OR ' + @vcAttribureSearchCondition + ')'

				

                INSERT  INTO @TableRowCount
                        EXEC
                            ( 'SELECT COUNT(*) FROM ( ' + @strSQL
                                + ') as t2'
                                    );

                SELECT  @TotalCount = Value
                FROM    @TableRowCount;

                SET @strNewQuery = 'select * from (' + @strSQL
                    + ') as t where SRNO> '
                    + CAST(( ( @numPageIndex - 1 ) * @numPageSize ) AS VARCHAR(10))
                    + ' and SRNO < '
                    + CAST(( ( @numPageIndex * @numPageSize ) + 1 ) AS VARCHAR(10))
                    + ' order by  vcItemName'

				--PRINT @strNewQuery
                EXEC(@strNewQuery)
				END
			ELSE
				BEGIN

				SET @strSQL = 'SELECT
									I.numItemCode,
									I.numVendorID,
									MIN(WHT.numWareHouseItemID) as numWareHouseItemID,
									dbo.fn_GetAttributes(WHT.numWareHouseItemId,I.bitSerialized) AS [Attributes]
								FROM
									dbo.Item I WITH (NOLOCK)
								INNER JOIN
									dbo.Vendor V
								ON
									V.numItemCode = I.numItemCode
								LEFT JOIN
									dbo.WareHouseItems WHT
								ON
									I.numItemCode = WHT.numItemID
								WHERE 
									((ISNULL(I.numItemGroup,0) > 0 AND I.vcItemName LIKE ''%' + @itemSearchText + '%'') OR (ISNULL(I.numItemGroup,0) = 0 AND I.vcItemName LIKE ''%' + @str + '%''))
									AND ISNULL(I.Isarchieve, 0) <> 1
									AND I.charItemType NOT IN ( ''A'' )
									AND I.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + '
									AND (WHT.numWareHouseID = ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0)'

				--- added Asset validation  by sojan
                IF @tintSearchOrderCustomerHistory = 1 AND @numDivisionID > 0
                BEGIN
                    SET @strSQL = @strSQL
                        + ' and I.numItemCode in (select distinct oppI.numItemCode from 
						OpportunityMaster oppM join OpportunityItems oppI on oppM.numOppId=oppI.numOppId where oppM.numDomainID='
                        + CONVERT(VARCHAR(20), @numDomainID)
                        + ' and oppM.numDivisionId='
                        + CONVERT(VARCHAR(20), @numDivisionId)
                        + ')'
                END

				SET @strSQL = @strSQL + ' GROUP BY
										I.numItemCode,
										I.numVendorID,
										dbo.fn_GetAttributes(numWareHouseItemId,I.bitSerialized)'

				SET @strSQL = 
				'
				SELECT    
					SRNO=ROW_NUMBER() OVER( ORDER BY  I.vcItemName),
					I.numItemCode,
					(SELECT TOP 1 vcPathForImage FROM ItemImages WHERE numItemCode = I.numItemCode) AS vcPathForImage ,
					(SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = I.numItemCode) AS vcPathForTImage ,
					ISNULL(vcItemName, '''') vcItemName ,
					CASE WHEN I.[charItemType] = ''P''
					THEN CONVERT(VARCHAR(100), ISNULL((SELECT TOP 1 ROUND(monWListPrice,2) FROM [WareHouseItems] WHERE [numItemID] = I.numItemCode), 0))
					ELSE CONVERT(VARCHAR(100), ROUND(monListPrice, 2))
					END AS monListPrice,
					ISNULL(vcSKU, '''') vcSKU ,
					ISNULL(numBarCodeId, 0) numBarCodeId ,
					ISNULL(vcModelID, '''') vcModelID ,
					ISNULL(txtItemDesc, '''') AS txtItemDesc ,
					ISNULL(C.vcCompanyName, '''') AS vcCompanyName,
					TEMPMATRIX.numWareHouseItemID,
					TEMPMATRIX.Attributes AS [vcAttributes],
					(CASE 
						WHEN ISNULL(I.bitKitParent,0) = 1 
						THEN 
							(
								CASE
									WHEN (SELECT COUNT(*) FROM ItemDetails INNER JOIN Item ON ItemDetails.numChildItemID = Item.numItemCode WHERE numItemKitID=I.numItemCode AND ISNULL(Item.bitKitParent,0) = 1) > 0
									THEN 1
									ELSE 0
								END
							)
						ELSE 0 
					END) bitHasKitAsChild
				FROM  
					(' + @strSQL +') AS TEMPMATRIX
				INNER JOIN    
					Item I
				ON
					TEMPMATRIX.numItemCode = I.numItemCode
				LEFT JOIN 
					DivisionMaster D 
				ON 
					I.numVendorID = D.numDivisionID
				LEFT JOIN 
					CompanyInfo C 
				ON 
					C.numCompanyID = D.numCompanyID
				WHERE  
				 ((ISNULL(I.numItemGroup,0) = 0 AND I.vcItemName LIKE ''%' + @str + '%'') OR ' + @vcAttribureSearchCondition + ')'

                INSERT  INTO @TableRowCount
                        EXEC
                            ( 'SELECT COUNT(*) FROM ( ' + @strSQL
                                + ') as t2'
                                    );

                SELECT  @TotalCount = Value
                FROM    @TableRowCount;

                SET @strNewQuery = 'select * from (' + @strSQL
                    + ') as t where SRNO> '
                    + CAST(( ( @numPageIndex - 1 ) * @numPageSize ) AS VARCHAR(10))
                    + ' and SRNO < '
                    + CAST(( ( @numPageIndex * @numPageSize ) + 1 ) AS VARCHAR(10))
                    + ' order by  vcItemName'

				--PRINT @strNewQuery
                EXEC(@strNewQuery)
			END	
        END
        ELSE
        BEGIN
				DECLARE @vcWareHouseSearch VARCHAR(1000)
				DECLARE @vcVendorSearch VARCHAR(1000)
               

				SET @vcWareHouseSearch = ''
				SET @vcVendorSearch = ''

                DECLARE @tintOrder AS INT
                DECLARE @Fld_id AS NVARCHAR(20)
                DECLARE @Fld_Name AS VARCHAR(20)
                SET @strSQL = ''
                SELECT TOP 1
                        @tintOrder = tintOrder + 1 ,
                        @Fld_id = numFieldId ,
                        @Fld_Name = vcFieldName
                FROM    #Temp1
                WHERE   Custom = 1
                ORDER BY tintOrder
                WHILE @tintOrder > 0
                    BEGIN
                        SET @strSQL = @strSQL + ', dbo.GetCustFldValueItem('
                            + @Fld_id + ', I.numItemCode) as [' + @Fld_Name
                            + ']'

                        SELECT TOP 1
                                @tintOrder = tintOrder + 1 ,
                                @Fld_id = numFieldId ,
                                @Fld_Name = vcFieldName
                        FROM    #Temp1
                        WHERE   Custom = 1
                                AND tintOrder >= @tintOrder
                        ORDER BY tintOrder
                        IF @@rowcount = 0
                            SET @tintOrder = 0
                    END


				--Temp table for Item Search Configuration

                SELECT  *
                INTO    #tempSearch
                FROM    ( SELECT    numFieldId ,
                                    vcDbColumnName ,
                                    ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                                    tintRow AS tintOrder ,
                                    0 AS Custom
                          FROM      View_DynamicColumns
                          WHERE     numFormId = 22
                                    AND numUserCntID = @numUserCntID
                                    AND numDomainID = @numDomainID
                                    AND tintPageType = 1
                                    AND bitCustom = 0
                                    AND ISNULL(bitSettingField, 0) = 1
                                    AND numRelCntType = 1
                          UNION
                          SELECT    numFieldId ,
                                    vcFieldName ,
                                    vcFieldName ,
                                    tintRow AS tintOrder ,
                                    1 AS Custom
                          FROM      View_DynamicCustomColumns
                          WHERE     Grp_id = 5
                                    AND numFormId = 22
                                    AND numUserCntID = @numUserCntID
                                    AND numDomainID = @numDomainID
                                    AND tintPageType = 1
                                    AND bitCustom = 1
                                    AND numRelCntType = 1
                        ) X 
  
                IF NOT EXISTS ( SELECT  *
                                FROM    #tempSearch )
                    BEGIN
                        INSERT  INTO #tempSearch
                                SELECT  numFieldId ,
                                        vcDbColumnName ,
                                        ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                                        tintorder ,
                                        0
                                FROM    View_DynamicDefaultColumns
                                WHERE   numFormId = 22
                                        AND bitDefault = 1
                                        AND ISNULL(bitSettingField, 0) = 1
                                        AND numDomainID = @numDomainID 
                    END

				--Regular Search
                DECLARE @strSearch AS VARCHAR(8000) ,
                    @CustomSearch AS VARCHAR(4000) ,
                    @numFieldId AS NUMERIC(9)
                SET @strSearch = ''
                SELECT TOP 1
                        @tintOrder = tintOrder + 1 ,
                        @Fld_Name = vcDbColumnName ,
                        @numFieldId = numFieldId
                FROM    #tempSearch
                WHERE   Custom = 0
                ORDER BY tintOrder
                WHILE @tintOrder > -1
                    BEGIN
                        IF @Fld_Name = 'vcPartNo'
						BEGIN
                            INSERT  INTO #tempItemCode
                                    SELECT DISTINCT
                                            Item.numItemCode
                                    FROM    Vendor
                                            JOIN Item ON Item.numItemCode = Vendor.numItemCode
                                    WHERE   Item.numDomainID = @numDomainID
                                            AND Vendor.numDomainID = @numDomainID
                                            AND Vendor.vcPartNo IS NOT NULL
                                            AND LEN(Vendor.vcPartNo) > 0
                                            AND vcPartNo LIKE '%' + @str + '%'
							
							SET @vcVendorSearch = 'dbo.Vendor.vcPartNo LIKE ''%' + @str + '%'''
							
						END
                        ELSE IF @Fld_Name = 'vcBarCode'
						BEGIN
                            INSERT  INTO #tempItemCode
                                    SELECT DISTINCT
                                            Item.numItemCode
                                    FROM    Item
                                            JOIN WareHouseItems WI ON WI.numDomainID = @numDomainID
                                                            AND Item.numItemCode = WI.numItemID
                                            JOIN Warehouses W ON W.numDomainID = @numDomainID
                                                            AND W.numWareHouseID = WI.numWareHouseID
                                    WHERE   Item.numDomainID = @numDomainID
                                            AND ISNULL(Item.numItemGroup,
                                                        0) > 0
                                            AND WI.vcBarCode IS NOT NULL
                                            AND LEN(WI.vcBarCode) > 0
                                            AND WI.vcBarCode LIKE '%' + @str + '%'

							SET @vcWareHouseSearch = 'dbo.WareHouseItems.vcBarCode LIKE ''%' + @str + '%'''
						END
                        ELSE IF @Fld_Name = 'vcWHSKU'
						BEGIN
                            INSERT  INTO #tempItemCode
                                    SELECT DISTINCT
                                            Item.numItemCode
                                    FROM    Item
                                            JOIN WareHouseItems WI ON WI.numDomainID = @numDomainID
                                                        AND Item.numItemCode = WI.numItemID
                                            JOIN Warehouses W ON W.numDomainID = @numDomainID
                                                        AND W.numWareHouseID = WI.numWareHouseID
                                    WHERE   Item.numDomainID = @numDomainID
                                            AND ISNULL(Item.numItemGroup,
                                                        0) > 0
                                            AND WI.vcWHSKU IS NOT NULL
                                            AND LEN(WI.vcWHSKU) > 0
                                            AND WI.vcWHSKU LIKE '%'
                                            + @str + '%'

							IF LEN(@vcWareHouseSearch) > 0
								SET @vcWareHouseSearch = @vcWareHouseSearch + ' OR dbo.WareHouseItems.vcWHSKU LIKE ''%' + @str + '%'''
							ELSE
								SET @vcWareHouseSearch = 'dbo.WareHouseItems.vcWHSKU LIKE ''%' + @str + '%'''
						END
                        ELSE
                            SET @strSearch = @strSearch
                                + (CASE @Fld_Name WHEN 'numItemCode' THEN ' I.[' + @Fld_Name ELSE ' [' + @Fld_Name END)  + '] LIKE ''%' + @str
                                + '%'''

                        SELECT TOP 1
                                @tintOrder = tintOrder + 1 ,
                                @Fld_Name = vcDbColumnName ,
                                @numFieldId = numFieldId
                        FROM    #tempSearch
                        WHERE   tintOrder >= @tintOrder
                                AND Custom = 0
                        ORDER BY tintOrder
                        IF @@rowcount = 0
                            BEGIN
                                SET @tintOrder = -1
                            END
                        ELSE
                            IF @Fld_Name != 'vcPartNo'
                                AND @Fld_Name != 'vcBarCode'
                                AND @Fld_Name != 'vcWHSKU'
                                BEGIN
                                    SET @strSearch = @strSearch + ' or '
                                END
                    END

                IF ( SELECT COUNT(*)
                     FROM   #tempItemCode
                   ) > 0
                    SET @strSearch = @strSearch
                        + ' or  I.numItemCode in (select numItemCode from #tempItemCode)' 

				--Custom Search
                SET @CustomSearch = ''
                SELECT TOP 1
                        @tintOrder = tintOrder + 1 ,
                        @Fld_Name = vcDbColumnName ,
                        @numFieldId = numFieldId
                FROM    #tempSearch
                WHERE   Custom = 1
                ORDER BY tintOrder
                WHILE @tintOrder > -1
                    BEGIN
    
                        SET @CustomSearch = @CustomSearch
                            + CAST(@numFieldId AS VARCHAR(10)) 

                        SELECT TOP 1
                                @tintOrder = tintOrder + 1 ,
                                @Fld_Name = vcDbColumnName ,
                                @numFieldId = numFieldId
                        FROM    #tempSearch
                        WHERE   tintOrder >= @tintOrder
                                AND Custom = 1
                        ORDER BY tintOrder
                        IF @@rowcount = 0
                            BEGIN
                                SET @tintOrder = -1
                            END
                        ELSE
                            BEGIN
                                SET @CustomSearch = @CustomSearch + ' , '
                            END
                    END
	
                IF LEN(@CustomSearch) > 0
                    BEGIN
                        SET @CustomSearch = ' I.numItemCode in (SELECT RecId FROM dbo.CFW_FLD_Values_Item WHERE Fld_ID IN ('
                            + @CustomSearch + ') and dbo.GetCustFldValueItem(Fld_ID,RecId) like ''%'
                            + @str + '%'')'

                        IF LEN(@strSearch) > 0
                            SET @strSearch = @strSearch + ' OR '
                                + @CustomSearch
                        ELSE
                            SET @strSearch = @CustomSearch  
                    END


                IF @tintOppType = 1
                    OR ( @bitRemoveVendorPOValidation = 1
                         AND @tintOppType = 2
                       )
                    BEGIN      
                        SET @strSQL = 'select SRNO=ROW_NUMBER() OVER( ORDER BY  I.vcItemName), I.numItemCode,
									  (SELECT TOP 1 vcPathForImage from ItemImages WHERE numItemCode = I.numItemCode) as vcPathForImage,
									  (SELECT TOP 1 vcPathForTImage from ItemImages WHERE numItemCode = I.numItemCode) as vcPathForTImage,
									  isnull(vcItemName,'''') vcItemName,
									  CASE 
									  WHEN I.[charItemType]=''P''
									  THEN convert(varchar(100),ISNULL((SELECT TOP 1 ROUND(monWListPrice,2) FROM [WareHouseItems] WHERE [numItemID]=I.numItemCode),0)) 
									  ELSE 
										CONVERT(VARCHAR(100), ROUND(monListPrice, 2)) 
									  END AS monListPrice,
									  isnull(vcSKU,'''') vcSKU,
									  isnull(numBarCodeId,0) numBarCodeId,
									  isnull(vcModelID,'''') vcModelID,
									  isnull(txtItemDesc,'''') as txtItemDesc,
									  isnull(C.vcCompanyName,'''') as vcCompanyName,
									  WHT.numWareHouseItemID,
									  dbo.fn_GetAttributes(WHT.numWareHouseItemId,I.bitSerialized) AS [vcAttributes],
									  (CASE 
											WHEN ISNULL(I.bitKitParent,0) = 1 
											THEN 
												(
													CASE
														WHEN (SELECT COUNT(*) FROM ItemDetails INNER JOIN Item ON ItemDetails.numChildItemID = Item.numItemCode WHERE numItemKitID=I.numItemCode AND ISNULL(Item.bitKitParent,0) = 1) > 0
														THEN 1
														ELSE 0
													END
												)
											ELSE 0 
										END) bitHasKitAsChild'
									  + @strSQL
									  + ' from Item  I WITH (NOLOCK)
									  Left join DivisionMaster D              
									  on I.numVendorID=D.numDivisionID  
									  left join CompanyInfo C  
									  on C.numCompanyID=D.numCompanyID 
									  LEFT JOIN
											dbo.WareHouseItems WHT
									  ON
											WHT.numItemID = I.numItemCode AND
											WHT.numWareHouseItemID = (' +
																			CASE LEN(@vcWareHouseSearch)
																			WHEN 0 
																			THEN 
																				'SELECT 
																					TOP 1 numWareHouseItemID 
																				FROM 
																					dbo.WareHouseItems 
																				WHERE 
																					dbo.WareHouseItems.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' 
																					AND dbo.WareHouseItems.numItemID = I.numItemCode 
																					AND (dbo.WareHouseItems.numWareHouseID = ' +  CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20))  + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0)'
																			ELSE
																				'CASE 
																					WHEN (SELECT 
																								COUNT(*) 
																							FROM 
																								dbo.WareHouseItems 
																							WHERE 
																								dbo.WareHouseItems.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' 
																								AND dbo.WareHouseItems.numItemID = I.numItemCode 
																								AND (dbo.WareHouseItems.numWareHouseID = ' +  CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20))  + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0)' +
																								' AND (' + @vcWareHouseSearch + ')) > 0 
																					THEN
																						(SELECT 
																							TOP 1 numWareHouseItemID 
																						FROM 
																							dbo.WareHouseItems 
																						WHERE 
																							dbo.WareHouseItems.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' 
																							AND dbo.WareHouseItems.numItemID = I.numItemCode 
																							AND (dbo.WareHouseItems.numWareHouseID = ' +  CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20))  + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0) AND (' + @vcWareHouseSearch + '))
																					ELSE
																						(SELECT 
																							TOP 1 numWareHouseItemID 
																						FROM 
																							dbo.WareHouseItems 
																						WHERE 
																							dbo.WareHouseItems.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' 
																							AND dbo.WareHouseItems.numItemID = I.numItemCode 
																							AND (dbo.WareHouseItems.numWareHouseID = ' +  CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20))  + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0))
																					END' 
																			END
																		+
																	') 
										where ((I.charItemType <> ''P'') OR ((SELECT COUNT(*) FROM dbo.WareHouseItems WHERE numItemID = I.numItemCode AND numDomainID = '
									  + CONVERT(VARCHAR(20), @numDomainID)
									  + ' AND numWareHouseID =  ' +  CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20))  + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0) > 0)) AND ISNULL(I.Isarchieve,0) <> 1 And I.charItemType NOT IN(''A'') AND  I.numDomainID='
									  + CONVERT(VARCHAR(20), @numDomainID) + ' and ('
									  + @strSearch + ') '   
							      
						--- added Asset validation  by sojan
                        IF @bitRemoveVendorPOValidation = 1
                            AND @tintOppType = 2
                            SET @strSQL = @strSQL
                                + ' and ISNULL(I.bitKitParent,0) = 0 AND ISNULL(I.bitAssembly,0) = 0 '

                        IF @tintSearchOrderCustomerHistory = 1
                            AND @numDivisionID > 0
                            BEGIN
                                SET @strSQL = @strSQL
                                    + ' and I.numItemCode in (select distinct oppI.numItemCode from 
									OpportunityMaster oppM join OpportunityItems oppI on oppM.numOppId=oppI.numOppId where oppM.numDomainID='
                                    + CONVERT(VARCHAR(20), @numDomainID)
                                    + ' and oppM.numDivisionId='
                                    + CONVERT(VARCHAR(20), @numDivisionId)
                                    + ')'
                            END

                        INSERT  INTO @TableRowCount
                                EXEC
                                    ( 'SELECT COUNT(*) FROM ( ' + @strSQL
                                      + ') as t2'
                                    );
                        SELECT  @TotalCount = Value
                        FROM    @TableRowCount;

                        SET @strNewQuery = 'select * from (' + @strSQL
                            + ') as t where SRNO> '
                            + CAST(( ( @numPageIndex - 1 ) * @numPageSize ) AS VARCHAR(10))
                            + ' and SRNO < '
                            + CAST(( ( @numPageIndex * @numPageSize ) + 1 ) AS VARCHAR(10))
                            + ' order by  vcItemName'
                        
						--PRINT @strNewQuery
						EXEC(@strNewQuery)
                    END      
                ELSE
                    IF @tintOppType = 2
                        BEGIN      
                            SET @strSQL = 'SELECT SRNO=ROW_NUMBER() OVER( ORDER BY  I.vcItemName), I.numItemCode,
										  (SELECT TOP 1 vcPathForImage from ItemImages WHERE numItemCode = I.numItemCode) as vcPathForImage,
										  (SELECT TOP 1 vcPathForTImage from ItemImages WHERE numItemCode = I.numItemCode) as vcPathForTImage,
										  ISNULL(vcItemName,'''') vcItemName,
										  convert(varchar(200),round(monListPrice,2)) as monListPrice,
										  isnull(vcSKU,'''') vcSKU,
										  isnull(numBarCodeId,0) numBarCodeId,
										  isnull(vcModelID,'''') vcModelID,
										  isnull(txtItemDesc,'''') as txtItemDesc,
										  isnull(C.vcCompanyName,'''') as vcCompanyName,
										  WHT.numWareHouseItemID,
										  dbo.fn_GetAttributes(WHT.numWareHouseItemId,I.bitSerialized) AS [vcAttributes],
										  (CASE 
												WHEN ISNULL(I.bitKitParent,0) = 1 
												THEN 
													(
														CASE
															WHEN (SELECT COUNT(*) FROM ItemDetails INNER JOIN Item ON ItemDetails.numChildItemID = Item.numItemCode WHERE numItemKitID=I.numItemCode AND ISNULL(Item.bitKitParent,0) = 1) > 0
															THEN 1
															ELSE 0
														END
													)
												ELSE 0 
											END) bitHasKitAsChild'
										  + @strSQL
										  + '  from item I  WITH (NOLOCK)
										  INNER JOIN
											dbo.Vendor V
										  ON
											V.numItemCode = I.numItemCode
										  Left join 
											DivisionMaster D              
										  ON 
											V.numVendorID=D.numDivisionID  
										  LEFT join 
											CompanyInfo C  
										  ON 
											C.numCompanyID=D.numCompanyID    
										  LEFT JOIN
											dbo.WareHouseItems WHT
										  ON
											WHT.numItemID = I.numItemCode AND
											WHT.numWareHouseItemID = (' +
																			CASE LEN(@vcWareHouseSearch)
																			WHEN 0 
																			THEN 
																				'SELECT 
																					TOP 1 numWareHouseItemID 
																				FROM 
																					dbo.WareHouseItems 
																				WHERE 
																					dbo.WareHouseItems.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' 
																					AND dbo.WareHouseItems.numItemID = I.numItemCode 
																					AND (dbo.WareHouseItems.numWareHouseID = ' +  CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20))  + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0)'
																			ELSE
																				'CASE 
																					WHEN (SELECT 
																								COUNT(*) 
																							FROM 
																								dbo.WareHouseItems 
																							WHERE 
																								dbo.WareHouseItems.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' 
																								AND dbo.WareHouseItems.numItemID = I.numItemCode 
																								AND (dbo.WareHouseItems.numWareHouseID = ' +  CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20))  + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0)' +
																								' AND (' + @vcWareHouseSearch + ')) > 0 
																					THEN
																						(SELECT 
																							TOP 1 numWareHouseItemID 
																						FROM 
																							dbo.WareHouseItems 
																						WHERE 
																							dbo.WareHouseItems.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' 
																							AND dbo.WareHouseItems.numItemID = I.numItemCode 
																							AND (dbo.WareHouseItems.numWareHouseID = ' +  CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20))  + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0) AND (' + @vcWareHouseSearch + '))
																					ELSE
																						(SELECT 
																							TOP 1 numWareHouseItemID 
																						FROM 
																							dbo.WareHouseItems 
																						WHERE 
																							dbo.WareHouseItems.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' 
																							AND dbo.WareHouseItems.numItemID = I.numItemCode 
																							AND (dbo.WareHouseItems.numWareHouseID = ' +  CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20))  + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0))
																					END' 
																			END
																		+
																	')
										       
										  WHERE (
													(I.charItemType <> ''P'') OR 
													((SELECT COUNT(*) FROM dbo.WareHouseItems WHERE numItemID = I.numItemCode AND numDomainID = '
										 + CONVERT(VARCHAR(20), @numDomainID)
										 + ' AND numWareHouseID =  ' +  CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20))  + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0) > 0)) AND ISNULL(I.Isarchieve,0) <> 1 and ISNULL(I.bitKitParent,0) = 0  AND ISNULL(I.bitAssembly,0) = 0 And I.charItemType NOT IN(''A'') AND V.numVendorID='
										 + CONVERT(VARCHAR(20), @numDivisionID)
										 + ' and ('+ CASE LEN(@vcVendorSearch)
																			WHEN 0 THEN ''
																			ELSE REPLACE(@vcVendorSearch,'dbo.Vendor','V') + ' OR '
																			END + @strSearch + ') ' 

							--- added Asset validation  by sojan
                            IF @tintSearchOrderCustomerHistory = 1
                                AND @numDivisionID > 0
                                BEGIN
                                    SET @strSQL = @strSQL
                                        + ' and I.numItemCode in (select distinct oppI.numItemCode from 
									OpportunityMaster oppM join OpportunityItems oppI on oppM.numOppId=oppI.numOppId where oppM.numDomainID='
                                        + CONVERT(VARCHAR(20), @numDomainID)
                                        + ' and oppM.numDivisionId='
                                        + CONVERT(VARCHAR(20), @numDivisionId)
                                        + ')'
                                END


                            INSERT  INTO @TableRowCount
                                    EXEC
                                        ( 'SELECT COUNT(*) FROM ( ' + @strSQL
                                          + ') as t2'
                                        );
                            SELECT  @TotalCount = Value
                            FROM    @TableRowCount;

                            SET @strNewQuery = 'select * from (' + @strSQL
                                + ') as t where SRNO> '
                                + CAST(( ( @numPageIndex - 1 ) * @numPageSize ) AS VARCHAR(10))
                                + ' and SRNO < '
                                + CAST(( ( @numPageIndex * @numPageSize ) + 1 ) AS VARCHAR(10))
                                + ' order by  vcItemName'
                            
							--PRINT @strNewQuery
							EXEC(@strNewQuery)
                        END
                    ELSE
                        SELECT  0  
            END
    END
ELSE
    SELECT  0  

SELECT  *
FROM    #Temp1
WHERE   vcDbColumnName NOT IN ( 'vcPartNo', 'vcBarCode', 'vcWHSKU' )ORDER BY tintOrder 
   


IF OBJECT_ID('dbo.Scores', 'U') IS NOT NULL
  DROP TABLE dbo.Scores

IF OBJECT_ID('tempdb..#Temp1') IS NOT NULL
BEGIN
    DROP TABLE #Temp1
END

IF OBJECT_ID('tempdb..#tempSearch') IS NOT NULL
BEGIN
    DROP TABLE #tempSearch
END

IF OBJECT_ID('tempdb..#tempItemCode') IS NOT NULL
BEGIN
    DROP TABLE #tempItemCode
END


    END

GO
/****** Object:  StoredProcedure [dbo].[USP_LoadWarehouseBasedOnItem]    Script Date: 09/01/2009 19:12:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec USP_LoadWarehouseBasedOnItem 14    
--created by anoop jayaraj    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_loadwarehousebasedonitem')
DROP PROCEDURE usp_loadwarehousebasedonitem
GO
CREATE PROCEDURE [dbo].[USP_LoadWarehouseBasedOnItem]    
@numItemCode as varchar(20)=''  
as    
DECLARE @numDomainID AS NUMERIC(18,0)
DECLARE @numBaseUnit AS NUMERIC(18,0)
DECLARE @vcUnitName as VARCHAR(100) 
DECLARE @numSaleUnit AS NUMERIC(18,0)
DECLARE @vcSaleUnitName as VARCHAR(100) 
DECLARE @numPurchaseUnit AS NUMERIC(18,0)
DECLARE @vcPurchaseUnitName as VARCHAR(100) 
declare @bitSerialize as bit     
DECLARE @bitKitParent BIT
 
SELECT 
	@numDomainID = numDomainID,
	@numBaseUnit = ISNULL(numBaseUnit,0),
	@numSaleUnit = ISNULL(numSaleUnit,0),
	@numPurchaseUnit = ISNULL(numPurchaseUnit,0),
	@bitSerialize=bitSerialized,
	@bitKitParent = ( CASE WHEN ISNULL(bitKitParent,0) = 1
                               AND ISNULL(bitAssembly,0) = 1 THEN 0
                          WHEN ISNULL(bitKitParent,0) = 1 THEN 1
                          ELSE 0
                     END ) 
FROM 
	Item 
WHERE 
	numItemCode=@numItemCode    


SELECT @vcUnitName=ISNULL(vcUnitName,'-') FROM UOM u WHERE u.numUOMId=@numBaseUnit
SELECT @vcSaleUnitName=ISNULL(vcUnitName,'-') FROM UOM u WHERE u.numUOMId=@numSaleUnit
SELECT @vcPurchaseUnitName=ISNULL(vcUnitName,'-') FROM UOM u WHERE u.numUOMId=@numPurchaseUnit

DECLARE @numSaleUOMFactor AS DECIMAL(28,14)
DECLARE @numPurchaseUOMFactor AS DECIMAL(28,14)

SELECT @numSaleUOMFactor = dbo.fn_UOMConversion(@numSaleUnit,@numItemCode,@numDOmainID,@numBaseUnit)
SELECT @numPurchaseUOMFactor = dbo.fn_UOMConversion(@numPurchaseUnit,@numItemCode,@numDOmainID,@numBaseUnit)


DECLARE @KitOnHand AS NUMERIC(9) = 0

IF @bitKitParent=1
	SELECT @KitOnHand=dbo.fn_GetKitInventory(@numItemCode)          

SELECT 
	DISTINCT(WareHouseItems.numWareHouseItemId),
	ISNULL(WareHouseItems.numWLocationID,0) numWLocationID,
	CONCAT(vcWareHouse, (CASE WHEN WareHouseItems.numWLocationID = -1 THEN ' (Global)' ELSE (CASE WHEN ISNULL(WarehouseLocation.numWLocationID,0) > 0 THEN CONCAT(' (',WarehouseLocation.vcLocation,')') ELSE '' END) END)) AS vcWareHouse, 
	dbo.fn_GetAttributes(WareHouseItems.numWareHouseItemId,@bitSerialize) as Attr, 
	WareHouseItems.monWListPrice,
	CASE WHEN @bitKitParent=1 THEN @KitOnHand else ISNULL(numOnHand,0) END numOnHand,
	CASE WHEN @bitKitParent=1 THEN 0 ELSE ISNULL(numOnOrder,0) END numOnOrder,
	CASE WHEN @bitKitParent=1 THEN 0 ELSE ISNULL(numReorder,0) END numReorder,
	CASE WHEN @bitKitParent=1 THEN 0 ELSE ISNULL(numAllocation,0) END numAllocation,
	CASE WHEN @bitKitParent=1 THEN 0 ELSE ISNULL(numBackOrder,0) END numBackOrder,
	Warehouses.numWareHouseID,
	@vcUnitName AS vcUnitName,
	@vcSaleUnitName AS vcSaleUnitName,
	@vcPurchaseUnitName AS vcPurchaseUnitName,
	CASE WHEN @numBaseUnit > 0 AND @numSaleUnit > 0
	THEN
		CASE WHEN @bitKitParent=1 THEN CAST(CAST(@KitOnHand/@numSaleUOMFactor AS INT) AS VARCHAR) else CAST(CAST(ISNULL(numOnHand,0)/@numSaleUOMFactor AS INT) AS VARCHAR) END
	ELSE
		'-'
	END AS numOnHandUOM,
	CASE WHEN @numBaseUnit > 0 AND @numSaleUnit > 0
	THEN
		CASE WHEN @bitKitParent=1 THEN '0' else CAST(CAST(ISNULL(numOnOrder,0)/@numPurchaseUOMFactor AS INT) AS VARCHAR) END
	ELSE
		'-'
	END AS numOnOrderUOM,
	CASE WHEN @numBaseUnit > 0 AND @numSaleUnit > 0
	THEN
		CASE WHEN @bitKitParent=1 THEN '0' else CAST(CAST(ISNULL(numReorder,0)/@numSaleUOMFactor AS INT) AS VARCHAR) END
	ELSE
		'-'
	END AS numReorderUOM,
	CASE WHEN @numBaseUnit > 0 AND @numSaleUnit > 0
	THEN
		CASE WHEN @bitKitParent=1 THEN '0' else CAST(CAST(ISNULL(numAllocation,0)/@numSaleUOMFactor AS INT) AS VARCHAR) END
	ELSE
		'-'
	END AS numAllocationUOM,
	CASE WHEN @numBaseUnit > 0 AND @numSaleUnit > 0
	THEN
		CASE WHEN @bitKitParent=1 THEN '0' else CAST(CAST(ISNULL(numBackOrder,0)/@numSaleUOMFactor AS INT) AS VARCHAR) END
	ELSE
		'-'
	END AS numBackOrderUOM
FROM 
	WareHouseItems    
JOIN 
	Warehouses 
ON 
	Warehouses.numWareHouseID=WareHouseItems.numWareHouseID  
LEFT JOIN
	WarehouseLocation
ON
	WareHouseItems.numWLocationID = WarehouseLocation.numWLocationID
WHERE 
	numItemID=@numItemCode
ORDER BY
	numWareHouseItemId ASC
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS (SELECT * FROM sysobjects WHERE xtype = 'p' AND NAME = 'USP_ManageECommercePromotion') 
DROP PROCEDURE USP_ManageECommercePromotion
GO
CREATE  PROCEDURE [dbo].[USP_ManageECommercePromotion]
(
	@numUserCntId numeric(9,0),
	@numDomainId numeric(9,0),
	@vcCookieId varchar(100),
	@postselldiscount INT=0,
	@numSiteID NUMERIC(18,0)
)
AS
BEGIN
	IF(@postselldiscount=0)
	BEGIN
		
		DECLARE @TEMPItems TABLE
		(
			ID INT,
			numCartID NUMERIC(18,0),
			numItemCode NUMERIC(18,0),
			numUnitHour NUMERIC(18,0),
			monPrice MONEY,
			monTotalAmount MONEY
		)

		-- FIRST CHECK EXISTING PROMOTION TRIGERRED ARE STILL VALID BECAUSE USER CAN DESCREASE QTY
		DECLARE @TEMPUsedPromotion TABLE
		(
			ID INT,
			numPromotionID NUMERIC(18,0),
			vcPromotionDescription VARCHAR(MAX)
		)

		INSERT INTO 
			@TEMPUsedPromotion
		SELECT
			ROW_NUMBER() OVER(ORDER BY numCartID),
			PromotionID,
			CI.PromotionDesc
		FROM
			CartItems CI
		WHERE
			CI.numDomainId=@numDomainId
			AND vcCookieId=@vcCookieId  
			AND numUserCntId=@numUserCntId 
			AND ISNULL(bitParentPromotion,0)=1

		DECLARE @i AS INT = 1
		DECLARE @COUNT AS INT
		DECLARE @numPromotionID AS NUMERIC(18,0)=0
		DECLARE @vcPromorionDescription VARCHAR(MAX) 
		DECLARE @tintOfferTriggerValueType INT
		DECLARE @fltOfferTriggerValue FLOAT
		DECLARE @tintOfferBasedOn INT
		DECLARE @fltDiscountValue FLOAT
		DECLARE @tintDiscountType INT
		DECLARE @tintDiscoutBaseOn INT
		
		SELECT @COUNT=COUNT(*) FROM @TEMPUsedPromotion

		IF @COUNT > 0
		BEGIN
			WHILE @i <= @COUNT
			BEGIN
				SELECT
					@numPromotionID=T1.numPromotionID
					,@vcPromorionDescription=T1.vcPromotionDescription
					,@tintOfferTriggerValueType=tintOfferTriggerValueType
					,@fltOfferTriggerValue=fltOfferTriggerValue
					,@tintOfferBasedOn=tintOfferBasedOn
					,@fltDiscountValue=fltDiscountValue
					,@tintDiscountType=tintDiscountType
					,@tintDiscoutBaseOn=tintDiscoutBaseOn
				FROM
					@TEMPUsedPromotion T1
				INNER JOIN
					PromotionOffer PO
				ON
					T1.numPromotionID=PO.numProId
				WHERE
					ID=@i

				IF (SELECT 
						(CASE WHEN @tintOfferTriggerValueType = 1 THEN SUM(numUnitHour) ELSE SUM(numUnitHour * monPrice) END)
					FROM
						CartItems
					WHERE
						numDomainId=@numDomainId
						AND vcCookieId=@vcCookieId  
						AND numUserCntId=@numUserCntId 
						AND ISNULL(bitParentPromotion,0)=1
						AND PromotionID=@numPromotionID) < @fltOfferTriggerValue
				BEGIN
					-- CLEAR APPLIED PROMOTION BECAUSE NOW REQUIRED QTY OR AMOUNT FOR PROMOTION IS NOT AVAILABLE
					UPDATE
						CartItems
					SET
						fltDiscount=0,
						PromotionID=0,
						PromotionDesc='',
						vcCoupon=NULL,
						bitDiscountType=0,
						bitParentPromotion=0,
						monTotAmount=(numUnitHour * monPrice),
						monTotAmtBefDiscount=(numUnitHour * monPrice)
					WHERE
						numDomainId=@numDomainId
						AND vcCookieId=@vcCookieId  
						AND numUserCntId=@numUserCntId 
						AND PromotionID=@numPromotionID
				END
				ELSE
				BEGIN
					--UPDATE DISCOUNT VALUE BECAUSE USER MAY HAVE CHANGE QTY OF ITEM
					DELETE FROM @TEMPItems

					INSERT INTO
						@TEMPItems
					SELECT
						ROW_NUMBER() OVER(ORDER BY numCartID),
						numCartId,
						CI.numItemCode,
						numUnitHour,
						monPrice,
						(numUnitHour * monPrice)
					FROM
						CartItems CI
					INNER JOIN
						Item I
					ON
						CI.numItemCode = I.numItemCode
					WHERE
						CI.numDomainId=@numDomainId
						AND vcCookieId=@vcCookieId  
						AND numUserCntId=@numUserCntId 
						AND PromotionID=@numPromotionID
						AND 1 = (CASE @tintDiscoutBaseOn
									WHEN 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numValue=I.numItemCode AND numProId=@numPromotionID AND tintRecordType=6 AND tintType=1) > 0 THEN 1 ELSE 0 END)
									WHEN 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numValue=I.numItemClassification AND numProId=@numPromotionID AND tintRecordType=6 AND tintType=2) > 0 THEN 1 ELSE 0 END)
									WHEN 3 THEN (CASE @tintOfferBasedOn 
											WHEN 1 THEN (CASE WHEN (SELECT COUNT(*) FROM SimilarItems WHERE numItemCode=I.numItemCode AND numParentItemCode IN (SELECT numValue FROM PromotionOfferItems POI WHERE POI.numProId=@numPromotionID AND tintRecordType=5 AND tintType=1)) > 0 THEN 1 ELSE 0 END)
											WHEN 2 THEN (CASE WHEN (SELECT COUNT(*) FROM SimilarItems WHERE numItemCode=I.numItemCode AND numParentItemCode IN (SELECT numItemCode FROM Item WHERE numDomainID=@numDomainId AND numItemClassification IN (SELECT numValue FROM PromotionOfferItems POI WHERE POI.numProId=@numPromotionID AND tintRecordType=5 AND tintType=2))) > 0 THEN 1 ELSE 0 END)
											ELSE 0
										END) 
									ELSE 0
								END)

					DECLARE @j INT = 1
					DECLARE @jCOUNT INT 
					DECLARE @numTempCartID NUMERIC(18,0)
					DECLARE @numTempItemCode NUMERIC(18,0)
					DECLARE @numTempItemClassification NUMERIC(18,0)
					DECLARE @numTempUnitHour NUMERIC(18,0)
					DECLARE @monTempPrice MONEY
					DECLARE @monTempTotalAmount MONEY
					DECLARE @remainingDiscountValue FLOAT = @fltDiscountValue

					SELECT @jCOUNT=COUNT(*) FROM @TEMPItems

					WHILE @j <= @jCOUNT
					BEGIN
						SELECT 
							@numTempCartID=numCartID,
							@numTempUnitHour=numUnitHour,
							@monTempPrice=monPrice,
							@monTempTotalAmount=monTotalAmount
						FROM
							@TEMPItems
						WHERE
							ID=@j
						
						
						IF @remainingDiscountValue <= 0
						BEGIN
							-- CLEAR DISCOUNT OF REMAINING ITEM ONCE DISCOUNT IS USED BY OTHER ITEMS
							UPDATE
								CartItems
							SET 
								fltDiscount=0,
								monTotAmount=(numUnitHour * monPrice),
								monTotAmtBefDiscount=(numUnitHour * monPrice)
							WHERE
								numCartId=@numTempCartID
								AND ISNULL(bitParentPromotion,0)=1

							UPDATE
								CartItems
							SET 
								fltDiscount=0,
								PromotionID=0,
								PromotionDesc='',
								vcCoupon=NULL
							WHERE
								numCartId=@numTempCartID
								AND ISNULL(bitParentPromotion,0)=0
						END
						ELSE
						BEGIN
							IF @tintDiscountType=1 --Percentage
							BEGIN
								UPDATE 
									CartItems
								SET
									fltDiscount=@fltDiscountValue,
									bitDiscountType=0,
									monTotAmount=(@monTempTotalAmount-((@monTempTotalAmount*@fltDiscountValue)/100))
								WHERE
									numCartId=@numTempCartID
							END
							ELSE IF @tintDiscountType=2 --Flat Amount
							BEGIN
								UPDATE 
									CartItems 
								SET 
									monTotAmount=(CASE WHEN @remainingDiscountValue >= @monTempTotalAmount THEN 0 ELSE (@monTempTotalAmount-@remainingDiscountValue) END),
									bitDiscountType=1,
									fltDiscount=(CASE WHEN @remainingDiscountValue >= @monTempTotalAmount THEN @monTempTotalAmount ELSE @remainingDiscountValue END)
								WHERE 
									numCartId=@numTempCartID

								SET @remainingDiscountValue = @remainingDiscountValue - (CASE WHEN @remainingDiscountValue >= @monTempTotalAmount THEN @monTempTotalAmount ELSE @remainingDiscountValue END)
							END
							ELSE IF @tintDiscountType=3 --Quantity
							BEGIN
								IF (SELECT COUNT(*) FROM CartItems WHERE numDomainId=@numDomainId AND numCartId=@numTempCartID AND ISNULL(bitParentPromotion,0)=1 AND @tintOfferBasedOn=1) > 0
								BEGIN	
									IF @tintOfferTriggerValueType = 1
									BEGIN
										IF @numTempUnitHour > @fltOfferTriggerValue
										BEGIN
											SET @numTempUnitHour = @numTempUnitHour - @fltOfferTriggerValue

											UPDATE 
												CartItems 
											SET 
												monTotAmount=(CASE WHEN @remainingDiscountValue >= @numTempUnitHour THEN @fltOfferTriggerValue * monPrice ELSE (@monTempTotalAmount-(@remainingDiscountValue * @monTempPrice)) END),
												bitDiscountType=1,
												fltDiscount=(CASE WHEN @remainingDiscountValue >= @numTempUnitHour THEN (@numTempUnitHour * @monTempPrice) ELSE (@remainingDiscountValue * @monTempPrice) END) 
											WHERE 
												numCartId=@numTempCartID

											SET @remainingDiscountValue = @remainingDiscountValue - (CASE WHEN @remainingDiscountValue >= @numTempUnitHour THEN @numTempUnitHour ELSE @remainingDiscountValue END)
										END
										ELSE
										BEGIN
											UPDATE
												CartItems
											SET
												fltDiscount=0,
												monTotAmount=@numTempUnitHour*@monTempPrice,
												monTotAmtBefDiscount=@numTempUnitHour*@monTempPrice
											WHERE	
												numCartId=@numTempCartID
										END
									END
									ELSE IF @tintOfferTriggerValueType = 2
									BEGIN
										IF (@numTempUnitHour * @monTempPrice) > @fltOfferTriggerValue
										BEGIN
											DECLARE @discountQty AS FLOAT
											SET @discountQty = FLOOR(((@numTempUnitHour * @monTempPrice) - @fltOfferTriggerValue) / (CASE WHEN ISNULL(@monTempPrice,0) = 0 THEN 1 ELSE @monTempPrice END))

											IF @discountQty > 0
											BEGIN
												UPDATE 
													CartItems 
												SET 
													monTotAmount=(CASE WHEN @remainingDiscountValue >= @discountQty THEN (@numTempUnitHour - @discountQty) * monPrice ELSE (@monTempTotalAmount-(@remainingDiscountValue * @monTempPrice)) END),
													bitDiscountType=1,
													fltDiscount=(CASE WHEN @remainingDiscountValue >= @discountQty THEN (@discountQty * @monTempPrice) ELSE (@remainingDiscountValue * @monTempPrice) END) 
												WHERE 
													numCartId=@numTempCartID

												SET @remainingDiscountValue = @remainingDiscountValue - @discountQty
											END
											ELSE
											BEGIN
												UPDATE
													CartItems
												SET
													fltDiscount=0,
													monTotAmount=@numTempUnitHour*@monTempPrice,
													monTotAmtBefDiscount=@numTempUnitHour*@monTempPrice
												WHERE	
													numCartId=@numTempCartID
											END
										END
										ELSE
										BEGIN
											UPDATE
												CartItems
											SET
												fltDiscount=0,
												monTotAmount=@numTempUnitHour*@monTempPrice,
												monTotAmtBefDiscount=@numTempUnitHour*@monTempPrice
											WHERE	
												numCartId=@numTempCartID
										END
									END
								END
								ELSE 
								BEGIN
									UPDATE 
										CartItems 
									SET 
										monTotAmount=(CASE WHEN @remainingDiscountValue >= @numTempUnitHour THEN 0 ELSE (@monTempTotalAmount-(@remainingDiscountValue * @monTempPrice)) END),
										bitDiscountType=1,
										fltDiscount=(CASE WHEN @remainingDiscountValue >= @numTempUnitHour THEN (@numTempUnitHour*@monTempPrice) ELSE (@remainingDiscountValue * @monTempPrice) END) 
									WHERE 
										numCartId=@numTempCartID

									SET @remainingDiscountValue = @remainingDiscountValue - (CASE WHEN @remainingDiscountValue >= @numTempUnitHour THEN @numTempUnitHour ELSE @remainingDiscountValue END)
								END
							END
						END

						SET @j = @j + 1
					END
				END

				-- CHECK IF DISCOUNT CAN BE APPLIED TO ANY REMINING ITEM
				DELETE FROM @TEMPItems

				INSERT INTO
					@TEMPItems
				SELECT
					ROW_NUMBER() OVER(ORDER BY numCartID),
					numCartId,
					CI.numItemCode,
					numUnitHour,
					monPrice,
					(numUnitHour * monPrice)
				FROM
					CartItems CI
				INNER JOIN
					Item I
				ON
					CI.numItemCode=I.numItemCode
				WHERE
					CI.numDomainId=@numDomainId
					AND vcCookieId=@vcCookieId  
					AND numUserCntId=@numUserCntId 
					AND ISNULL(PromotionID,0)=0
					AND 1 = (CASE @tintDiscoutBaseOn
								WHEN 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numValue=I.numItemCode AND numProId=@numPromotionID AND tintRecordType=6 AND tintType=1) > 0 THEN 1 ELSE 0 END)
								WHEN 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numValue=I.numItemClassification AND numProId=@numPromotionID AND tintRecordType=6 AND tintType=2) > 0 THEN 1 ELSE 0 END)
								WHEN 3 THEN (CASE @tintOfferBasedOn 
										WHEN 1 THEN (CASE WHEN (SELECT COUNT(*) FROM SimilarItems WHERE numItemCode=I.numItemCode AND numParentItemCode IN (SELECT numValue FROM PromotionOfferItems POI WHERE POI.numProId=@numPromotionID AND tintRecordType=5 AND tintType=1)) > 0 THEN 1 ELSE 0 END)
										WHEN 2 THEN (CASE WHEN (SELECT COUNT(*) FROM SimilarItems WHERE numItemCode=I.numItemCode AND numParentItemCode IN (SELECT numItemCode FROM Item WHERE numDomainID=@numDomainId AND numItemClassification IN (SELECT numValue FROM PromotionOfferItems POI WHERE POI.numProId=@numPromotionID AND tintRecordType=5 AND tintType=2))) > 0 THEN 1 ELSE 0 END)
										ELSE 0
									END) 
								ELSE 0
							END)
					
				DECLARE @l INT = 1
				DECLARE @lCount INT

				SELECT @lCount = COUNT(*) FROM @TEMPItems

				WHILE @l <= @lCount
				BEGIN
					SELECT 
						@numTempCartID=numCartID,
						@numTempItemCode=numItemCode,
						@numTempUnitHour=numUnitHour,
						@monTempPrice=monPrice,
						@monTempTotalAmount = (numUnitHour * monPrice)
					FROM 
						@TEMPItems 
					WHERE 
						ID=@l


					IF @tintDiscountType=1 --Percentage
					BEGIN
						UPDATE 
							CartItems
						SET
							PromotionID=@numPromotionID,
							PromotionDesc=@vcPromorionDescription,
							bitParentPromotion=0,
							fltDiscount=@fltDiscountValue,
							bitDiscountType=0,
							monTotAmount=(@monTempTotalAmount-((@monTempTotalAmount*@fltDiscountValue)/100))
						WHERE
							numCartId=@numTempCartID
					END
					ELSE IF @tintDiscountType=2 --Flat Amount
					BEGIN
						-- FIRST CHECK WHETHER DISCOUNT AMOUNT IS NOT ALREADY USED BY OTHER ITEMS
						DECLARE @usedDiscountAmount FLOAT
						SELECT
							@usedDiscountAmount = SUM(ISNULL(fltDiscount,0))
						FROM
							CartItems
						WHERE
							PromotionID=@numPromotionID

						IF @usedDiscountAmount < @fltDiscountValue
						BEGIN
							UPDATE 
								CartItems 
							SET 
								monTotAmount=(CASE WHEN (@fltDiscountValue - @usedDiscountAmount)  >= @monTempTotalAmount THEN 0 ELSE (@monTempTotalAmount-(@fltDiscountValue - @usedDiscountAmount)) END),
								PromotionID=@numPromotionID ,
								PromotionDesc=@vcPromorionDescription,
								bitDiscountType=1,
								fltDiscount=(CASE WHEN (@fltDiscountValue - @usedDiscountAmount) >= @monTempTotalAmount THEN @monTempTotalAmount ELSE (@fltDiscountValue - @usedDiscountAmount) END)
							WHERE 
								numCartId=@numTempCartID
						END
						ELSE
						BEGIN
							BREAK
						END
					END
					ELSE IF @tintDiscountType=3 --Quantity
					BEGIN
						-- FIRST CHECK WHETHER DISCOUNT QTY IS NOT ALREADY USED BY OTHER ITEMS
						DECLARE @usedDiscountQty FLOAT
						SELECT
							@usedDiscountQty = SUM(ISNULL(fltDiscount,0)/ISNULL(monPrice,1))
						FROM
							CartItems
						WHERE
							PromotionID=@numPromotionID

						IF @usedDiscountQty < @fltDiscountValue
						BEGIN
							UPDATE 
								CartItems 
							SET 
								monTotAmount=(CASE WHEN (@fltDiscountValue - @usedDiscountQty)  >= numUnitHour THEN 0 ELSE (@monTempTotalAmount-((@fltDiscountValue - @usedDiscountQty) * @monTempPrice)) END),
								PromotionID=@numPromotionID ,
								PromotionDesc=@vcPromorionDescription,
								bitDiscountType=1,
								fltDiscount=(CASE WHEN (@fltDiscountValue - @usedDiscountQty) >= numUnitHour THEN @monTempTotalAmount ELSE ((@fltDiscountValue - @usedDiscountQty) * @monTempPrice) END) 
							WHERE 
								numCartId=@numTempCartID
						END
						ELSE
						BEGIN
							BREAK
						END
					END

					SET @l = @l + 1
				END

				SET @i = @i + 1
			END
		END

		-- NOW CHECK PROMOTION CAN BE TRIGERRED FROM REMANING ITEMS FROM WHICH PROMOTION IS NOT TRIGERRED OR PROMOTION IS NOT APPLIED
		DELETE FROM @TEMPItems

		INSERT INTO 
			@TEMPItems
		SELECT
			ROW_NUMBER() OVER(ORDER BY numCartID),
			numCartId,
			numItemCode,
			numUnitHour,
			monPrice,
			(numUnitHour * monPrice)
		FROM
			CartItems
		WHERE
			numDomainId=@numDomainId
			AND vcCookieId=@vcCookieId  
			AND numUserCntId=@numUserCntId
			AND ISNULL(PromotionID,0)=0


		DECLARE @k AS INT = 1
		DECLARE @kCount AS INT

		SELECT @kCount=COUNT(*) FROM @TEMPItems

		IF @kCount > 0
		BEGIN
			WHILE @k <= @kCount
			BEGIN
				SELECT 
					@numTempCartID=numCartID,
					@numTempUnitHour=numUnitHour,
					@numTempItemCode=T1.numItemCode,
					@numTempItemClassification=I.numItemClassification,
					@monTempPrice=monPrice,
					@monTempTotalAmount=monTotalAmount
				FROM 
					@TEMPItems T1
				INNER JOIN
					Item I
				ON
					T1.numItemCode = I.numItemCode
				WHERE 
					ID=@k

	
				-- IF PROMOTION IS NOT TRIGERRED OR APPLIED
				IF (SELECT COUNT(*) FROM CartItems WHERE numDomainId=@numDomainId AND vcCookieId=@vcCookieId AND numUserCntId=@numUserCntId	AND numCartId=@numTempCartID AND ISNULL(PromotionID,0)=0) > 0
				BEGIN
					/************************** Check if any promotion can be trigerred *********************************/
					EXEC USP_ECommerceTiggerPromotion @numDomainID,@numUserCntId,@vcCookieId,@numTempCartID,@numTempItemCode,@numTempItemClassification,@numTempUnitHour,@monTempPrice,@numSiteID
				END


				SET @k = @k + 1
			END
		END
	END	
END
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_ManageInventory' ) 
    DROP PROCEDURE usp_ManageInventory
GO
CREATE PROCEDURE [dbo].[usp_ManageInventory]
    @itemcode AS NUMERIC(9) = 0,
    @numWareHouseItemID AS NUMERIC(9) = 0,
    @monAmount AS MONEY,
    @tintOpptype AS TINYINT,
    @numUnits AS FLOAT,
    @QtyShipped AS FLOAT,
    @QtyReceived AS FLOAT,
    @monPrice AS MONEY,
    @tintFlag AS TINYINT,  --1: SO/PO insert/edit 2:SO/PO Close 3:SO/PO Re-Open 4:Sales Fulfillment Release 5:Sales Fulfillment Revert
    @numOppID as numeric(9)=0,
    @numoppitemtCode AS NUMERIC(9)=0,
    @numUserCntID AS NUMERIC(9)=0,
	@bitWorkOrder AS BIT = 0   
AS 
    DECLARE @onHand AS FLOAT                                    
    DECLARE @onOrder AS FLOAT 
    DECLARE @onBackOrder AS FLOAT
    DECLARE @onAllocation AS FLOAT
	DECLARE @onReOrder AS FLOAT
    DECLARE @monAvgCost AS MONEY 
    DECLARE @bitKitParent BIT
	DECLARE @numOrigUnits AS NUMERIC		
	DECLARE @description AS VARCHAR(100)
	DECLARE @bitAsset as BIT
	DECLARE @numDomainID AS NUMERIC
	DECLARE @bitReOrderPoint AS BIT = 0
	DECLARE @numReOrderPointOrderStatus AS NUMERIC(18,0) = 0   
      
	SELECT @bitAsset=ISNULL(bitAsset,0),@numDomainID=numDomainID from Item where numItemCode=@itemcode--Added By Sachin
	SELECT @bitReOrderPoint=ISNULL(bitReOrderPoint,0),@numReOrderPointOrderStatus=ISNULL(numReOrderPointOrderStatus,0) FROM Domain WHERE numDomainID = @numDomainID
    
	SELECT  @monAvgCost = (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(monAverageCost, 0) END) ,
            @bitKitParent = ( CASE WHEN ISNULL(bitKitParent,0) = 1
                               AND ISNULL(bitAssembly,0) = 1 THEN 0
                          WHEN ISNULL(bitKitParent,0) = 1 THEN 1
                          ELSE 0
                     END )
    FROM    Item
    WHERE   numitemcode = @itemcode                                   
    
    IF @numWareHouseItemID>0
    BEGIN                     
		SELECT  @onHand = ISNULL(numOnHand, 0),
				@onAllocation = ISNULL(numAllocation, 0),
				@onOrder = ISNULL(numOnOrder, 0),
				@onBackOrder = ISNULL(numBackOrder, 0),
				@onReOrder = ISNULL(numReorder,0)
		FROM    WareHouseItems
		WHERE   numWareHouseItemID = @numWareHouseItemID 
    END
   
   DECLARE @dtItemReceivedDate AS DATETIME;SET @dtItemReceivedDate=NULL
   
   DECLARE @TotalOnHand AS NUMERIC;SET @TotalOnHand=0  
   SELECT @TotalOnHand=(SUM(ISNULL(numOnHand,0)) + SUM(ISNULL(numAllocation,0))) FROM dbo.WareHouseItems WHERE numItemID=@itemcode
					
   SET @numOrigUnits=@numUnits
            
    IF @tintFlag = 1  --1: SO/PO insert/edit 
    BEGIN
        IF @tintOpptype = 1 
        BEGIN  
		--When change below code also change to USP_ManageWorkOrderStatus  

			/* When QtyShipped has been updated form sales fulfillment then it need to be deducted */
			SET @description='SO insert/edit (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'
				
			IF @bitKitParent = 1 
            BEGIN
                EXEC USP_UpdatingInventoryForEmbeddedKits @numoppitemtCode,@numOrigUnits, 0,@numOppID,0,@numUserCntID
            END 
			ELSE IF @bitWorkOrder = 1
			BEGIN
				SET @description='SO-WO insert/edit (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'
				-- MANAGE INVENTOY OF ASSEMBLY ITEMS
				EXEC USP_ManageInventoryAssemblyWO @numUserCntID,@numOppID,@itemcode,@numWareHouseItemID,@QtyShipped,1
			END
            ELSE
			BEGIN       
				SET @numUnits = @numUnits - @QtyShipped
                                                        
                IF @onHand >= @numUnits 
                BEGIN                                    
                    SET @onHand = @onHand - @numUnits                            
                    SET @onAllocation = @onAllocation + @numUnits                                    
                END                                    
                ELSE IF @onHand < @numUnits 
                BEGIN                                    
                    SET @onAllocation = @onAllocation + @onHand                                    
                    SET @onBackOrder = @onBackOrder + @numUnits
                        - @onHand                                    
                    SET @onHand = 0                                    
                END    
			                                 

				IF @bitAsset=0--Not Asset
				BEGIN 
					UPDATE  WareHouseItems
					SET     numOnHand = @onHand,
							numAllocation = @onAllocation,
							numBackOrder = @onBackOrder,dtModified = GETDATE() 
					WHERE   numWareHouseItemID = @numWareHouseItemID  


					-- IF REORDER POINT IS ENABLED FROM GLOABAL SETTING THEN CREATE PURCHASE OPPORTUNITY
					If (@onHand + @onOrder <= @onReOrder) AND @bitReOrderPoint = 1 AND @onReOrder > 0
					BEGIN
						BEGIN TRY
							DECLARE @numNewOppID AS NUMERIC(18,0) = 0
							EXEC USP_OpportunityMaster_CreatePO @numNewOppID OUTPUT,@numDomainID,@numUserCntID,@itemcode,@onBackOrder,@numWareHouseItemID,0,0,@numReOrderPointOrderStatus
						END TRY
						BEGIN CATCH
							--WE ARE NOT THROWING ERROR
						END CATCH
					END         
				END
			END  
        END                       
        ELSE IF @tintOpptype = 2 
        BEGIN  
			SET @description='PO insert/edit (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Received:' +  CAST(@QtyReceived AS VARCHAR(10)) + ')'
		            
			/* When QtyReceived has been updated form purchase fulfillment then it need to be deducted */
            SET @numUnits = @numUnits - @QtyReceived   
                                             
            SET @onOrder = @onOrder + @numUnits 
		    
            UPDATE  
				WareHouseItems
            SET     
				numOnHand = @onHand,
                numAllocation = @onAllocation,
                numBackOrder = @onBackOrder,
                numOnOrder = @onOrder,dtModified = GETDATE() 
            WHERE   
				numWareHouseItemID = @numWareHouseItemID   
        END                                                                                                                                         
    END
    ELSE IF @tintFlag = 2 --2:SO/PO Close
    BEGIN
        PRINT '@OppType=' + CONVERT(VARCHAR(5), @tintOpptype)
		    
        IF @tintOpptype = 1 
        BEGIN
			SET @description='SO Close (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'
                    
            IF @bitKitParent = 1 
            BEGIN
                EXEC USP_UpdatingInventoryForEmbeddedKits @numoppitemtCode,
                    @numOrigUnits, 2,@numOppID,0,@numUserCntID
            END  
			ELSE IF @bitWorkOrder = 1
			BEGIN
				SET @description='SO-WO Close (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'
				EXEC USP_ManageInventoryAssemblyWO @numUserCntID,@numOppID,@itemcode,@numWareHouseItemID,@QtyShipped,2
			END
            ELSE
            BEGIN       
				/* When QtyShipped has been updated form sales fulfillment then it need to be deducted */
				SET @numUnits = @numUnits - @QtyShipped
				IF @bitAsset=0--Not Asset
				BEGIN
					SET @onAllocation = @onAllocation - @numUnits 
				END
				ELSE--Asset
				BEGIN
					SET @onAllocation = @onAllocation  
				END
											           
				UPDATE 
					WareHouseItems
				SET 
					numOnHand = @onHand,
					numAllocation = @onAllocation,
					numBackOrder = @onBackOrder,
					dtModified = GETDATE() 
				WHERE 
					numWareHouseItemID = @numWareHouseItemID   
			END
        END                
        ELSE IF @tintOpptype = 2 
        BEGIN
			SET @description='PO Close (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Received:' +  CAST(@QtyReceived AS VARCHAR(10)) + ')'
                        
			/* When QtyReceived has been updated form purchase fulfillment then it need to be deducted */
            SET @numUnits = @numUnits - @QtyReceived
            
			If @numUnits > 0
			BEGIN
				IF EXISTS(SELECT 1 FROM dbo.OpportunityMaster WHERE numOppID=@numOppID AND ISNULL(bitStockTransfer,0)=0)
				BEGIN
					--Updating the Average Cost
					IF @TotalOnHand + @numUnits <= 0
					BEGIN
						SET @monAvgCost = @monAvgCost
					END
					ELSE
					BEGIN
						SET @monAvgCost = (( @TotalOnHand * @monAvgCost) + (@numUnits * @monPrice)) / ( @TotalOnHand + @numUnits )
					END    
		                            
					UPDATE 
						Item
					SET 
						monAverageCost = (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE @monAvgCost END)
					WHERE 
						numItemCode = @itemcode
				END
		    
				IF @onOrder >= @numUnits 
				BEGIN
					SET @onOrder = @onOrder - @numUnits            
                
					IF @onBackOrder >= @numUnits 
					BEGIN            
						SET @onBackOrder = @onBackOrder - @numUnits            
						SET @onAllocation = @onAllocation + @numUnits            
					END            
					ELSE 
					BEGIN            
						SET @onAllocation = @onAllocation + @onBackOrder            
						SET @numUnits = @numUnits - @onBackOrder            
						SET @onBackOrder = 0            
						SET @onHand = @onHand + @numUnits            
					END         
				END            
				ELSE IF @onOrder < @numUnits 
				BEGIN
					PRINT 'in @onOrder < @numUnits '
					SET @onHand = @onHand + @onOrder
					SET @onOrder = @numUnits - @onOrder
				END         

				UPDATE
					WareHouseItems
				SET 
					numOnHand = @onHand,
					numAllocation = @onAllocation,
					numBackOrder = @onBackOrder,
					numOnOrder = @onOrder,dtModified = GETDATE() 
				WHERE 
					numWareHouseItemID = @numWareHouseItemID  
			                
				SELECT @dtItemReceivedDate=dtItemReceivedDate FROM OpportunityMaster WHERE numOppId=@numOppId
			END
		END
    END
	ELSE IF @tintFlag = 3 --3:SO/PO Re-Open
    BEGIN
		PRINT '@OppType=' + CONVERT(VARCHAR(5), @tintOpptype)           
                
		IF @tintOpptype = 1 
        BEGIN
			SET @description='SO Re-Open (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'
                    
            IF @bitKitParent = 1 
            BEGIN
				EXEC USP_UpdatingInventoryForEmbeddedKits @numoppitemtCode,@numOrigUnits, 3,@numOppID,0,@numUserCntID
            END 
			ELSE IF @bitWorkOrder = 1
			BEGIN
				SET @description='SO-WO Re-Open (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'
				EXEC USP_ManageInventoryAssemblyWO @numUserCntID,@numOppID,@itemcode,@numWareHouseItemID,@QtyShipped,3
			END
            ELSE
            BEGIN
				/* When QtyShipped has been updated form sales fulfillment then it need to be deducted */
                SET @numUnits = @numUnits - @QtyShipped
  
				IF @bitAsset=0--Not Asset
				BEGIN
					SET @onAllocation = @onAllocation + @numUnits
				END
				ELSE--Asset
				BEGIN
					SET @onAllocation = @onAllocation 
				END
                                    
                UPDATE  
					WareHouseItems
                SET 
					numOnHand = @onHand,
                    numAllocation = @onAllocation,
                    numBackOrder = @onBackOrder,dtModified = GETDATE() 
                WHERE 
					numWareHouseItemID = @numWareHouseItemID
			END	               
        END                
        ELSE IF @tintOpptype = 2 
        BEGIN
			/* When QtyReceived has been updated form purchase fulfillment then it need to be deducted */
            SET @numUnits = @numUnits - @QtyReceived
            
			--Updating the Average Cost
			IF EXISTS(SELECT 1 FROM dbo.OpportunityMaster WHERE numOppID=@numOppID AND ISNULL(bitStockTransfer,0)=0)
			BEGIN
				IF @TotalOnHand - @numUnits <= 0
				BEGIN
					SET @monAvgCost = 0
				END
				ELSE
				BEGIN
					SET @monAvgCost = ((@TotalOnHand * @monAvgCost) - (@numUnits * @monPrice)) / ( @TotalOnHand - @numUnits )
				END        
				
				UPDATE  
					item
				SET 
					monAverageCost = (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE @monAvgCost END)
				WHERE 
					numItemCode = @itemcode
			END


			DECLARE @TEMPReceievedItems TABLE
			(
				ID INT,
				numWarehouseItemID NUMERIC(18,0),
				numUnitReceieved FLOAT
			)

			INSERT INTO
				@TEMPReceievedItems
			SELECT
				ROW_NUMBER() OVER(ORDER BY ID),
				numWarehouseItemID,
				numUnitReceieved
			FROM
				OpportunityItemsReceievedLocation 
			WHERE
				numDomainID=@numDomainID
				AND numOppID=@numOppID
				AND numOppItemID=@numoppitemtCode

			DECLARE @i AS INT = 1
			DECLARE @COUNT AS INT
			DECLARE @numTempWarehouseItemID NUMERIC(18,0)
			DECLARE @numTempUnitReceieved FLOAT

			SELECT @COUNT=COUNT(*) FROM @TEMPReceievedItems

			WHILE @i <= @COUNT
			BEGIN
				SELECT 
					@numTempWarehouseItemID=ISNULL(numWarehouseItemID,0),
					@numTempUnitReceieved=ISNULL(numUnitReceieved,0)
				FROM 
					@TEMPReceievedItems 
				WHERE 
					ID=@i

				SET @description='PO Re-Open (Qty:' + CAST(@numTempUnitReceieved AS VARCHAR(10)) + ' Received:' +  CAST(@QtyReceived AS VARCHAR(10)) + ')'

				UPDATE
					WareHouseItems
				SET
					numOnHand = ISNULL(numOnHand,0) - ISNULL(@numTempUnitReceieved,0),
					dtModified = GETDATE()
				WHERE
					numWareHouseItemID=@numTempWarehouseItemID

				EXEC dbo.USP_ManageWareHouseItems_Tracking
					@numWareHouseItemID = @numTempWarehouseItemID, --  numeric(9, 0)
					@numReferenceID = @numOppID, --  numeric(9, 0)
					@tintRefType = 4, --  tinyint
					@vcDescription = @description, --  varchar(100)
					@numModifiedBy = @numUserCntID,
					@dtRecordDate =  @dtItemReceivedDate,
					@numDomainID = @numDomainID

				SET @i = @i + 1
			END

			DELETE FROM OpportunityItemsReceievedLocation WHERE numOppID=@numOppID AND numOppItemID=@numoppitemtCode

			SET @description='PO Re-Open (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Received:' +  CAST(@QtyReceived AS VARCHAR(10)) + ')'

            IF @onHand >= @numUnits 
            BEGIN     
                SET @onHand = @onHand - @numUnits            
                SET @onOrder = @onOrder + @numUnits
            END         
                          
			UPDATE  
				WareHouseItems
            SET 
				numOnHand = @onHand
                ,numOnOrder = @onOrder
				,dtModified = GETDATE() 
            WHERE 
				numWareHouseItemID = @numWareHouseItemID
				AND numWLocationID <> -1                   

			UPDATE  
				WareHouseItems
            SET 
                numOnOrder = @onOrder
				,dtModified = GETDATE() 
            WHERE 
				numWareHouseItemID = @numWareHouseItemID
				AND numWLocationID = -1
        END
    END
            
            
            
    IF @numWareHouseItemID>0
	BEGIN
		DECLARE @tintRefType AS TINYINT
					
		IF @tintOpptype = 1 --SO
			SET @tintRefType=3
		ELSE IF @tintOpptype = 2 --PO
			SET @tintRefType=4
					  
		DECLARE @numDomain AS NUMERIC(18,0)
		SET @numDomain = (SELECT TOP 1 [numDomainID] FROM WareHouseItems WHERE [WareHouseItems].[numWareHouseItemID] = @numWareHouseItemID)

		EXEC dbo.USP_ManageWareHouseItems_Tracking
			@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
			@numReferenceID = @numOppID, --  numeric(9, 0)
			@tintRefType = @tintRefType, --  tinyint
			@vcDescription = @description, --  varchar(100)
			@numModifiedBy = @numUserCntID,
			@dtRecordDate =  @dtItemReceivedDate,
			@numDomainID = @numDomain
    END
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_ManageRMAInventory' ) 
    DROP PROCEDURE usp_ManageRMAInventory
GO
CREATE PROCEDURE [dbo].[usp_ManageRMAInventory]
    @numReturnHeaderID AS NUMERIC(9) = 0,
    @numDomainId NUMERIC(9),
    @numUserCntID AS NUMERIC(9)=0,
    @tintFlag AS TINYINT  --1: Receive 2: Revert
AS 
  BEGIN
  	
	DECLARE @numOppItemID NUMERIC(18,0)
  	DECLARE @tintReturnType AS TINYINT
	DECLARE @tintReceiveType AS TINYINT
  	DECLARE @numReturnItemID AS NUMERIC
	DECLARE @itemcode AS NUMERIC
	DECLARE @numUnits as FLOAT
	DECLARE @numWareHouseItemID as numeric
	DECLARE @numWLocationID AS NUMERIC(18,0)
	DECLARE @monAmount as money 
	DECLARE @onHand AS FLOAT
	DECLARE @onOrder AS FLOAT
	DECLARE @onBackOrder AS FLOAT
	DECLARE @onAllocation AS FLOAT
	DECLARE @description AS VARCHAR(100)
    
  	SELECT 
		@tintReturnType=tintReturnType,
		@tintReceiveType=tintReceiveType
	FROM 
		dbo.ReturnHeader 
	WHERE 
		numReturnHeaderID=@numReturnHeaderID
  	
  	SELECT TOP 1 
		@numReturnItemID=numReturnItemID,
		@itemcode=RI.numItemCode,
		@numUnits=RI.numUnitHourReceived,
  		@numWareHouseItemID=ISNULL(numWareHouseItemID,0),
		@monAmount=monTotAmount,
		@numOppItemID=numOppItemID
	FROM 
		ReturnItems RI 
	JOIN 
		Item I 
	ON 
		RI.numItemCode=I.numItemCode                                          
	WHERE 
		numReturnHeaderID=@numReturnHeaderID 
		AND (charitemtype='P' OR 1=(CASE 
									WHEN @tintReturnType=1 THEN 
										CASE 
											WHEN ISNULL(I.bitKitParent,0) = 1 AND ISNULL(I.bitAssembly,0) = 1 THEN 0
											WHEN ISNULL(I.bitKitParent,0) = 1 THEN 1
											ELSE 0 
										END 
									ELSE 
										0 
									END)) 
	ORDER BY 
		RI.numReturnItemID
				                                   
	WHILE @numReturnItemID>0                                        
	BEGIN   
		IF @numWareHouseItemID>0
		BEGIN                     
			SELECT  
				@onHand = ISNULL(numOnHand, 0),
				@onAllocation = ISNULL(numAllocation, 0),
				@onOrder = ISNULL(numOnOrder, 0),
				@onBackOrder = ISNULL(numBackOrder, 0),
				@numWLocationID=ISNULL(numWLocationID,0)
			FROM 
				WareHouseItems
			WHERE 
				numWareHouseItemID = @numWareHouseItemID 
			
			--Receive : SalesReturn 
			IF (@tintFlag=1 AND @tintReturnType=1)
			BEGIN
				SET @description='Sales Return Receive (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ')'
										
                IF @onBackOrder >= @numUnits 
                BEGIN            
                        SET @onBackOrder = @onBackOrder - @numUnits            
                        SET @onAllocation = @onAllocation + @numUnits            
                END            
                ELSE 
                BEGIN            
                        SET @onAllocation = @onAllocation + @onBackOrder            
                        SET @numUnits = @numUnits - @onBackOrder            
                        SET @onBackOrder = 0            
                        SET @onHand = @onHand + @numUnits            
                END         
			END
			--Revert : SalesReturn
			ELSE IF (@tintFlag=2 AND @tintReturnType=1)
			BEGIN
				SET @description='Sales Return Delete (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ')'
					
				IF @onHand  - @numUnits >= 0
					BEGIN						
						SET @onHand = @onHand - @numUnits
					END
				ELSE IF (@onHand + @onAllocation)  - @numUnits >= 0
					BEGIN						
						SET @numUnits = @numUnits - @onHand
                        SET @onHand = 0 
                            
                        SET @onBackOrder = @onBackOrder + @numUnits
                        SET @onAllocation = @onAllocation - @numUnits  
					END	
				ELSE IF  (@onHand + @onBackOrder + @onAllocation) - @numUnits >= 0
					BEGIN
						SET @numUnits = @numUnits - @onHand
                        SET @onHand = 0 
                            
                        SET @numUnits = @numUnits - @onAllocation  
                        SET @onAllocation = 0 
                            
						SET @onBackOrder = @onBackOrder + @numUnits
					END
			END
			--Revert : PurchaseReturn
			ELSE IF (@tintFlag=2 AND @tintReturnType=2) 
			BEGIN
				SET @description='Purchase Return Delete (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ')'
				
				IF @numWLocationID = -1 AND ISNULL(@numOppItemID,0) > 0
				BEGIN
					-- DELETE RETURN QTY
					DECLARE @TEMPReturn TABLE
					(
						ID INT,
						numWarehouseItemID NUMERIC(18,0),
						numReturnedQty FLOAT
					)

					INSERT INTO @TEMPReturn
					(
						ID,
						numWarehouseItemID,
						numReturnedQty
					)
					SELECT
						ROW_NUMBER() OVER(ORDER BY numWarehouseItemID)
						,numWarehouseItemID
						,SUM(numReturnedQty)
					FROM
						OpportunityItemsReceievedLocationReturn OITLR
					INNER JOIN
						OpportunityItemsReceievedLocation OIRL
					ON
						OITLR.numOIRLID = OIRL.ID
					WHERE
						numReturnID=@numReturnHeaderID
						AND numReturnItemID = @numReturnItemID
					GROUP BY
						numWarehouseItemID

					DECLARE @i AS INT = 1
					DECLARE @iCOUNT AS INT
					DECLARE @numQtyReturned FLOAT
					DECLARE @numTempReturnWarehouseItemID NUMERIC(18,0)

					SELECT @iCOUNT = COUNT(*) FROM @TEMPReturn
							
					WHILE @i <= @iCOUNT
					BEGIN
						SELECT 
							@numTempReturnWarehouseItemID=numWarehouseItemID,
							@numQtyReturned=numReturnedQty
						FROM 
							@TEMPReturn
						WHERE
							ID = @i
						
						-- INCREASE THE OnHand Of Destination Location
						UPDATE
							WareHouseItems
						SET
							numBackOrder = (CASE WHEN numBackOrder >= @numQtyReturned THEN ISNULL(numBackOrder,0) - @numQtyReturned ELSE 0 END),         
							numAllocation = (CASE WHEN numBackOrder >= @numQtyReturned THEN ISNULL(numAllocation,0) + @numQtyReturned ELSE ISNULL(numAllocation,0) + ISNULL(numBackOrder,0) END),
							numOnHand = (CASE WHEN numBackOrder >= @numQtyReturned THEN numOnHand ELSE ISNULL(numOnHand,0) + @numQtyReturned - ISNULL(numBackOrder,0) END),
							dtModified = GETDATE()
						WHERE
							numWareHouseItemID=@numTempReturnWarehouseItemID

						DECLARE @descriptionInner VARCHAR(MAX)='Purchase Return Delete (Qty:' + CAST(@numQtyReturned AS VARCHAR(10)) + ')'

						EXEC dbo.USP_ManageWareHouseItems_Tracking
							@numWareHouseItemID = @numTempReturnWarehouseItemID, --  numeric(9, 0)
							@numReferenceID = @numReturnHeaderID, --  numeric(9, 0)
							@tintRefType = 5, --  tinyint
							@vcDescription = @descriptionInner, --  varchar(100)
							@numModifiedBy = @numUserCntID,
							@numDomainId = @numDomainId

						SET @i = @i + 1
					END

					DELETE FROM OpportunityItemsReceievedLocationReturn WHERE numReturnID=@numReturnHeaderID AND numReturnItemID = @numReturnItemID
				END
				ELSE
				BEGIN
					IF @onBackOrder >= @numUnits 
					BEGIN            
						SET @onBackOrder = @onBackOrder - @numUnits            
						SET @onAllocation = @onAllocation + @numUnits            
					END            
					ELSE 
					BEGIN            
						SET @onAllocation = @onAllocation + @onBackOrder            
						SET @numUnits = @numUnits - @onBackOrder            
						SET @onBackOrder = 0            
						SET @onHand = @onHand + @numUnits            
					END
				END
			END
			--Receive : PurchaseReturn 
			ELSE IF (@tintFlag=1 AND @tintReturnType=2) 
			BEGIN
				SET @description='Purchase Return Receive (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ')'

				IF @numWLocationID = -1 AND ISNULL(@numOppItemID,0) > 0
				BEGIN
					-- FIRST CHECK IF SINGLE LOCATION HAS RECEIEVED QTY MORE THAN OR EQUAL TO QTY NEED TO BE RETURN
					IF EXISTS (SELECT 
									TOP 1 WI.numWareHouseItemID
								FROM 
									OpportunityItemsReceievedLocation OIRL 
								INNER JOIN 
									WareHouseItems WI 
								ON 
									OIRL.numWarehouseItemID=WI.numWareHouseItemID  
								WHERE 
									numOppItemID=@numOppItemID 
									AND OIRL.numUnitReceieved - ISNULL((SELECT SUM(numReturnedQty) FROM OpportunityItemsReceievedLocationReturn WHERE numOIRLID = OIRL.ID),0) >= @numUnits
									AND WI.numOnHand - @numUnits >= 0 ORDER BY WI.numWarehouseItemID)
					BEGIN
						DECLARE @ID NUMERIC(18,0)
						DECLARE @numSelectedWarehouseItemID AS NUMERIC(18,0)

						SELECT TOP 1
							@ID=OIRL.ID,
							@numSelectedWarehouseItemID = WI.numWareHouseItemID
						FROM 
							OpportunityItemsReceievedLocation OIRL 
						INNER JOIN 
							WareHouseItems WI 
						ON 
							OIRL.numWarehouseItemID=WI.numWareHouseItemID  
						WHERE 
							numOppItemID=@numOppItemID 
							AND OIRL.numUnitReceieved - ISNULL((SELECT SUM(numReturnedQty) FROM OpportunityItemsReceievedLocationReturn WHERE numOIRLID = OIRL.ID),0) >= @numUnits 
							AND WI.numOnHand - @numUnits >= 0

						UPDATE  
							WareHouseItems
						SET 
							numOnHand = ISNULL(numOnHand,0) - @numUnits,
							dtModified = GETDATE() 
						WHERE 
							numWareHouseItemID = @numSelectedWarehouseItemID 

						INSERT INTO OpportunityItemsReceievedLocationReturn
						(
							numOIRLID
							,[numReturnID]
							,[numReturnItemID]
							,[numReturnedQty]
						)
						VALUES
						(
							@ID
							,@numReturnHeaderID
							,@numReturnItemID
							,@numUnits
						)

						EXEC dbo.USP_ManageWareHouseItems_Tracking
							@numWareHouseItemID = @numSelectedWarehouseItemID, --  numeric(9, 0)
							@numReferenceID = @numReturnHeaderID, --  numeric(9, 0)
							@tintRefType = 5, --  tinyint
							@vcDescription = @description, --  varchar(100)
							@numModifiedBy = @numUserCntID,
							@numDomainId = @numDomainId
					END
					ELSE
					BEGIN
						-- NOW CHECK SUM OF QTY RECEIVED IN DIFFERENT LCOATION MORE THAN OR EQUAL TO QTY NEED TO BE RETURN
						DECLARE @TEMP TABLE
						(
							ID INT,
							numOIRLID NUMERIC(18,0),
							numWarehouseItemID NUMERIC(18,0),
							numUnitReceieved FLOAT,
							numReturnedQty FLOAT
						)

						INSERT INTO 
							@TEMP
						SELECT 
							ROW_NUMBER() OVER(ORDER BY OIRL.ID),
							OIRL.ID,
							OIRL.numWarehouseItemID,
							OIRL.numUnitReceieved,
							ISNULL((SELECT SUM(numReturnedQty) FROM OpportunityItemsReceievedLocationReturn WHERE numOIRLID = OIRL.ID),0)
						FROM
							OpportunityItemsReceievedLocation OIRL
						WHERE 
							numOppItemID=@numOppItemID
							AND (OIRL.numUnitReceieved - ISNULL((SELECT SUM(numReturnedQty) FROM OpportunityItemsReceievedLocationReturn WHERE numOIRLID = OIRL.ID),0)) > 0


						IF (SELECT 
								COUNT(*)
							FROM 
								@TEMP T1
							INNER JOIN 
								WareHouseItems WI 
							ON 
								T1.numWarehouseItemID=WI.numWareHouseItemID
							WHERE
								T1.numUnitReceieved - T1.numReturnedQty > 0
							HAVING 
								SUM(T1.numUnitReceieved - T1.numReturnedQty) >= @numUnits) > 0
						BEGIN
							DECLARE @numTempOnHand AS FLOAT
							DECLARE @numRemainingQtyToReturn AS FLOAT = @numUnits
							DECLARE @numTempOIRLID NUMERIC(18,0)
							DECLARE @numTempWarehouseItemID	NUMERIC(18,0)
							DECLARE @numRemaingUnits FLOAT

							DECLARE @j AS INT = 1
							DECLARE @jCOUNT AS INT

							SELECT @jCOUNT = COUNT(*) FROM @TEMP
							
							WHILE @j <= @jCOUNT AND @numRemainingQtyToReturn > 0
							BEGIN
								SELECT 
									@numTempOIRLID=numOIRLID,
									@numTempWarehouseItemID=T1.numWarehouseItemID,
									@numTempOnHand = ISNULL(WI.numOnHand,0),
									@numRemaingUnits=ISNULL(numUnitReceieved,0)-ISNULL(numReturnedQty,0)
								FROM
									@TEMP T1
								INNER JOIN
									WareHouseItems WI
								ON
									T1.numWarehouseItemID=WI.numWareHouseItemID
								WHERE
									ID = @j

								IF @numTempOnHand >= @numRemaingUnits
								BEGIN
									UPDATE  
										WareHouseItems
									SET 
										numOnHand = ISNULL(numOnHand,0) - (CASE WHEN @numRemaingUnits >= @numRemainingQtyToReturn THEN @numRemainingQtyToReturn ELSE @numRemaingUnits END),
										dtModified = GETDATE() 
									WHERE 
										numWareHouseItemID = @numTempWarehouseItemID 

									INSERT INTO OpportunityItemsReceievedLocationReturn
									(
										numOIRLID
										,[numReturnID]
										,[numReturnItemID]
										,[numReturnedQty]
									)
									VALUES
									(
										@numTempOIRLID
										,@numReturnHeaderID
										,@numReturnItemID
										,(CASE WHEN @numRemaingUnits >= @numRemainingQtyToReturn THEN @numRemainingQtyToReturn ELSE @numRemaingUnits END)
									)

									SET @descriptionInner = 'Purchase Return Receive (Qty:' + CAST((CASE WHEN @numRemaingUnits >= @numRemainingQtyToReturn THEN @numRemainingQtyToReturn ELSE @numRemaingUnits END) AS VARCHAR(10)) + ')'

									EXEC dbo.USP_ManageWareHouseItems_Tracking
										@numWareHouseItemID = @numTempWarehouseItemID, --  numeric(9, 0)
										@numReferenceID = @numReturnHeaderID, --  numeric(9, 0)
										@tintRefType = 5, --  tinyint
										@vcDescription = @descriptionInner, --  varchar(100)
										@numModifiedBy = @numUserCntID,
										@numDomainId = @numDomainId

									SET @numRemainingQtyToReturn = @numRemainingQtyToReturn - (CASE WHEN @numRemaingUnits >= @numRemainingQtyToReturn THEN @numRemainingQtyToReturn ELSE @numRemaingUnits END)
								END
								ELSE IF @numTempOnHand > 0
								BEGIN
									UPDATE  
										WareHouseItems
									SET 
										numOnHand = numOnHand - (CASE WHEN @numTempOnHand >= @numRemainingQtyToReturn THEN @numRemainingQtyToReturn ELSE @numTempOnHand END),
										dtModified = GETDATE() 
									WHERE 
										numWareHouseItemID = @numTempWarehouseItemID 

									INSERT INTO OpportunityItemsReceievedLocationReturn
									(
										numOIRLID
										,[numReturnID]
										,[numReturnItemID]
										,[numReturnedQty]
									)
									VALUES
									(
										@numTempOIRLID
										,@numReturnHeaderID
										,@numReturnItemID
										,(CASE WHEN @numTempOnHand >= @numRemainingQtyToReturn THEN @numRemainingQtyToReturn ELSE @numTempOnHand END)
									)

									SET @descriptionInner ='Purchase Return Receive (Qty:' + CAST((CASE WHEN @numTempOnHand >= @numRemainingQtyToReturn THEN @numRemainingQtyToReturn ELSE @numTempOnHand END) AS VARCHAR(10)) + ')'

									EXEC dbo.USP_ManageWareHouseItems_Tracking
										@numWareHouseItemID = @numTempWarehouseItemID, --  numeric(9, 0)
										@numReferenceID = @numReturnHeaderID, --  numeric(9, 0)
										@tintRefType = 5, --  tinyint
										@vcDescription = @descriptionInner, --  varchar(100)
										@numModifiedBy = @numUserCntID,
										@numDomainId = @numDomainId

									SET @numRemainingQtyToReturn = @numRemainingQtyToReturn - (CASE WHEN @numTempOnHand >= @numRemainingQtyToReturn THEN @numRemainingQtyToReturn ELSE @numTempOnHand END)
								END

								SET @j = @j + 1
							END


							IF @numRemainingQtyToReturn > 0
							BEGIN
								RAISERROR ( 'PurchaseReturn_Qty', 16, 1 ) ;
								RETURN ;
							END
						END
						ELSE
						BEGIN
							RAISERROR ( 'PurchaseReturn_Qty', 16, 1 ) ;
							RETURN ;
						END
					END
				END
				ELSE
				BEGIN
					IF @onHand  - @numUnits >= 0
					BEGIN						
						SET @onHand = @onHand - @numUnits
					END
					ELSE
					BEGIN
						RAISERROR ( 'PurchaseReturn_Qty', 16, 1 ) ;
						RETURN ;
					END
				END
			END 
				
			UPDATE  
				WareHouseItems
            SET 
				numOnHand = @onHand,
                numAllocation = @onAllocation,
                numBackOrder = @onBackOrder,
                numOnOrder = @onOrder,
				dtModified = GETDATE() 
            WHERE 
				numWareHouseItemID = @numWareHouseItemID 
                            
            IF @numWareHouseItemID>0
			BEGIN
				DECLARE @tintRefType AS TINYINT;SET @tintRefType=5
					  
				EXEC dbo.USP_ManageWareHouseItems_Tracking
				@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
				@numReferenceID = @numReturnHeaderID, --  numeric(9, 0)
				@tintRefType = @tintRefType, --  tinyint
				@vcDescription = @description, --  varchar(100)
				@numModifiedBy = @numUserCntID,
				@numDomainId = @numDomainId
            END           
		END
		  
		SELECT TOP 1 
			@numReturnItemID=numReturnItemID,
			@itemcode=RI.numItemCode,
			@numUnits=RI.numUnitHourReceived,
  			@numWareHouseItemID=ISNULL(numWareHouseItemID,0),
			@monAmount=monTotAmount
		FROM 
			ReturnItems RI 
		JOIN 
			Item I 
		ON 
			RI.numItemCode=I.numItemCode                                          
		WHERE 
			numReturnHeaderID=@numReturnHeaderID 
			AND (charitemtype='P' OR 1=(CASE 
										WHEN @tintReturnType=1 THEN 
											CASE 
												WHEN ISNULL(I.bitKitParent,0) = 1 AND ISNULL(I.bitAssembly,0) = 1 THEN 0
												WHEN ISNULL(I.bitKitParent,0) = 1 THEN 1
												ELSE 0 
											END 
										ELSE 0 END)) 
			AND RI.numReturnItemID>@numReturnItemID 
		ORDER BY 
			RI.numReturnItemID
							
		IF @@ROWCOUNT=0 SET @numReturnItemID=0         
	END
END
GO
/****** Object:  StoredProcedure [dbo].[USP_OppBizDocs]    Script Date: 03/25/2009 15:23:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_oppbizdocs')
DROP PROCEDURE usp_oppbizdocs
GO
CREATE PROCEDURE [dbo].[USP_OppBizDocs]
(                        
 @byteMode as tinyint=null,                        
 @numOppId as numeric(9)=null,                        
 @numOppBizDocsId as numeric(9)=null,
 @ClientTimeZoneOffset AS INT=0,
 @numDomainID AS NUMERIC(18,0) = NULL,
 @numUserCntID AS NUMERIC(18,0) = NULL
)                        
as                        
          
SELECT 
	ISNULL(CompanyInfo.numCompanyType,0) AS numCompanyType,
	ISNULL(CompanyInfo.vcProfile,0) AS vcProfile,
	ISNULL(OpportunityMaster.numAccountClass,0) AS numAccountClass
FROM 
	OpportunityMaster 
JOIN 
	DivisionMaster 
ON 
	DivisionMaster.numDivisionId = OpportunityMaster.numDivisionId
JOIN 
	CompanyInfo
ON 
	CompanyInfo.numCompanyId = DivisionMaster.numCompanyID
WHERE 
	numOppId = @numOppId
		                                  
if @byteMode= 1                        
begin 
BEGIN TRY
BEGIN TRANSACTION
DECLARE @numBizDocID AS INT
DECLARE @bitFulfilled AS BIT
Declare @tintOppType as tinyint

SELECT @tintOppType=tintOppType FROM OpportunityMaster WHERE  numOppId=@numOppId  and numDomainID= @numDomainID 
SELECT @numBizDocID = numBizDocId, @bitFulfilled=bitFulfilled FROM OpportunityBizDocs WHERE numOppBizDocsId = @numOppBizDocsId

--IF it's as sales order and BizDoc is of type of fulfillment and already fulfilled revert items to allocations
IF @tintOppType = 1 AND @numBizDocID = 296 AND @bitFulfilled = 1
BEGIN
	EXEC USP_OpportunityBizDocs_RevertFulFillment @numDomainID,@numUserCntID,@numOppId,@numOppBizDocsId
END


DELETE FROM [ProjectsOpportunities] WHERE numOppBizDocID=@numOppBizDocsId

DELETE FROM [ShippingReportItems] WHERE [numShippingReportId] IN  (SELECT numShippingReportId from [ShippingReport] WHERE [numOppBizDocId] = @numOppBizDocsId)
DELETE FROM [ShippingBox] WHERE [numShippingReportId] IN  (SELECT numShippingReportId from [ShippingReport] WHERE [numOppBizDocId] = @numOppBizDocsId)
DELETE FROM [ShippingReport] WHERE [numOppBizDocId] = @numOppBizDocsId

 --Delete All entries for current bizdoc
EXEC [USP_DeleteEmbeddedCost] @numOppBizDocsId, 0, @numDomainID 
--DELETE FROM [OpportunityBizDocsPaymentDetails] WHERE [numBizDocsPaymentDetId] IN (SELECT  [numBizDocsPaymentDetId] FROM [OpportunityBizDocsDetails] WHERE [numBizDocsPaymentDetId] IN (SELECT [numBizDocsPaymentDetId] FROM [EmbeddedCost] WHERE [numOppBizDocID]=@numOppBizDocsId))
--DELETE FROM [EmbeddedCostItems] WHERE [numEmbeddedCostID] IN (SELECT [numEmbeddedCostID] FROM [EmbeddedCost] WHERE [numOppBizDocID] = @numOppBizDocsId)
--DELETE FROM [EmbeddedCost] WHERE [numOppBizDocID] = @numOppBizDocsId
--DELETE FROM [OpportunityBizDocsDetails] WHERE [numBizDocsPaymentDetId] IN (SELECT [numBizDocsPaymentDetId] FROM [EmbeddedCost] WHERE [numOppBizDocID]=@numOppBizDocsId)


delete  from OpportunityBizDocItems where   numOppBizDocID=  @numOppBizDocsId              

--Delete Deferred BizDoc Recurring Entries
delete from OpportunityRecurring where numOppBizDocID=@numOppBizDocsId and tintRecurringType=4

UPDATE dbo.OpportunityRecurring SET numOppBizDocID=NULL WHERE numOppBizDocID=@numOppBizDocsId

DELETE FROM [RecurringTransactionReport] WHERE [numRecTranBizDocID] = @numOppBizDocsId

--Credit Balance
Declare @monCreditAmount as money;Set @monCreditAmount=0
Declare @numDivisionID as numeric(9);Set @numDivisionID=0

Select @monCreditAmount=isnull(oppBD.monCreditAmount,0),@numDivisionID=Om.numDivisionID,
@numOppId=OM.numOppId 
from OpportunityBizDocs oppBD join OpportunityMaster Om on OM.numOppId=oppBD.numOppId WHERE  numOppBizDocsId = @numOppBizDocsId

delete from [CreditBalanceHistory] where numOppBizDocsId=@numOppBizDocsId

 
IF @tintOppType=1 --Sales
	update DivisionMaster set monSCreditBalance=isnull(monSCreditBalance,0) + isnull(@monCreditAmount,0) where numDivisionID=@numDivisionID and numDomainID=@numDomainID
else IF @tintOppType=2 --Purchase
	update DivisionMaster set monPCreditBalance=isnull(monPCreditBalance,0) + isnull(@monCreditAmount,0) where numDivisionID=@numDivisionID and numDomainID=@numDomainID


delete from DocumentWorkflow where numDocID=@numOppBizDocsId and cDocType='B'      

delete from BizDocAction where numBizActionId in(select numBizActionId from BizActionDetails where numOppBizDocsId =@numOppBizDocsId and btDocType=1) 
delete from BizActionDetails where numOppBizDocsId =@numOppBizDocsId and btDocType=1

delete from OpportunityBizDocs where numOppBizDocsId=@numOppBizDocsId                        

COMMIT TRANSACTION
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH             
end                        
                        
if @byteMode= 2                        
begin  

DECLARE @BaseCurrencySymbol nVARCHAR(3);SET @BaseCurrencySymbol='$'
SELECT @BaseCurrencySymbol = ISNULL(varCurrSymbol,'$') FROM dbo.Currency 
														   WHERE numCurrencyID IN ( SELECT numCurrencyID FROM dbo.Domain 
																					WHERE numDomainId=@numDomainId)
--PRINT @BaseCurrencySymbol

																					                      
select *, isnull(X.monPAmount,0)-X.monAmountPaid as BalDue from (select  opp.numOppBizDocsId,                        
 opp.numOppId,                        
 opp.numBizDocId,                        
 ISNULL((SELECT TOP 1 numOrientation FROM BizDocTemplate WHERE numBizDocTempID = Opp.numBizDocTempID),0) AS numOrientation, 
 ISNULL((SELECT TOP 1 bitKeepFooterBottom FROM BizDocTemplate WHERE numBizDocTempID = Opp.numBizDocTempID),0) AS bitKeepFooterBottom,                  
 oppmst.vcPOppName,                        
 mst.vcData as BizDoc,                        
 opp.numCreatedBy,                        
DATEADD(minute,-@ClientTimeZoneOffset,opp.dtCreatedDate) CreatedDate,
 opp.vcRefOrderNo,                       
 vcBizDocID,
 CASE WHEN LEN(ISNULL(vcBizDocName,''))=0 THEN vcBizDocID ELSE vcBizDocName END vcBizDocName,      
  [dbo].[GetDealAmount](@numOppId ,getutcdate(),opp.numOppBizDocsId ) as monPAmount,
  CASE 
	WHEN tintOppType=1  
	THEN (CASE 
			WHEN (ISNULL(numAuthoritativeSales,0)=numBizDocId OR numBizDocId=296) 
			THEN 'Authoritative' + CASE WHEN ISNULL(Opp.tintDeferred,0)=1 THEN ' (Deferred)' ELSE '' END 
			WHEN opp.numBizDocId =  304 THEN 'Deferred-Authoritative'
			ELSE 'Non-Authoritative' END ) 
  when tintOppType=2  then (case when isnull(numAuthoritativePurchase,0)=numBizDocId then 'Authoritative' + Case when isnull(Opp.tintDeferred,0)=1 then ' (Deferred)' else '' end else 'Non-Authoritative' end )  end as BizDocType,
  isnull(monAmountPaid,0) monAmountPaid, 
  ISNULL(C.varCurrSymbol,'') varCurrSymbol ,
  ISNULL(Opp.[bitAuthoritativeBizDocs],0) bitAuthoritativeBizDocs,
  ISNULL(oppmst.[tintshipped] ,0) tintshipped,isnull(opp.numShipVia,0) as numShipVia,oppmst.numDivisionId,isnull(Opp.tintDeferred,0) as tintDeferred,
  ISNULL(Opp.numBizDocTempID,0) AS numBizDocTempID,con.numContactid,isnull(con.vcEmail,'') AS vcEmail,ISNULL(Opp.numBizDocStatus,0) AS numBizDocStatus,
--  case when isnumeric(ISNULL(dbo.fn_GetListItemName(isnull(oppmst.intBillingDays,0)),0))=1 
--	   then ISNULL(dbo.fn_GetListItemName(isnull(oppmst.intBillingDays,0)),0) 
--	   else 0 
--  end AS numBillingDaysName,
  CASE WHEN ISNUMERIC((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(oppmst.intBillingDays,0))) = 1
 	   THEN (SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(oppmst.intBillingDays,0))
	   ELSE 0
  END AS numBillingDaysName,
  Opp.dtFromDate,
  opp.dtFromDate AS BillingDate,
  ISNULL(C.fltExchangeRate,1) fltExchangeRateCurrent,
  ISNULL(oppmst.fltExchangeRate,1) fltExchangeRateOfOrder,
  @BaseCurrencySymbol BaseCurrencySymbol,0 AS numReturnHeaderID,0 AS tintReturnType,ISNULL(Opp.fltExchangeRateBizDoc,ISNULL(oppmst.fltExchangeRate,1)) AS fltExchangeRateBizDoc,
  CASE WHEN (SELECT COUNT(*) FROM dbo.ShippingReport WHERE numOppBizDocId = opp.numOppBizDocsId AND numDomainID = @numDomainID) > 0 THEN 1 ELSE 0 END AS [IsShippingReportGenerated],
  --CASE WHEN (SELECT ISNULL(vcTrackingNo,'') FROM dbo.OpportunityBizDocs WHERE numOppBizDocsId = opp.numOppBizDocsId) <> '' THEN 1 ELSE 0 END AS [ISTrackingNumGenerated]
  CASE WHEN (SELECT COUNT(*) FROM dbo.ShippingBox 
			 WHERE numShippingReportId IN (SELECT numShippingReportId FROM dbo.ShippingReport WHERE numOppBizDocId = opp.numOppBizDocsId AND numDomainID = @numDomainID)
			 AND LEN(ISNULL(vcTrackingNumber,'')) > 0) > 0 THEN 1 ELSE 0 END AS [ISTrackingNumGenerated],
  ISNULL((SELECT MAX(numShippingReportId) from ShippingReport SR 
		  WHERE SR.numOppBizDocId = opp.numoppbizdocsid 
		  AND SR.numDomainID = oppmst.numDomainID),0) AS numShippingReportId,
  ISNULL((SELECT MAX(SB.numShippingReportId) FROM [ShippingBox] SB 
		  JOIN dbo.ShippingReport SR ON SB.numShippingReportId = SR.numShippingReportId 
		  where SR.numOppBizDocId = opp.numoppbizdocsid AND SR.numDomainID = oppmst.numDomainID 
		  AND LEN(ISNULL(SB.vcTrackingNumber,''))>0),0) AS numShippingLabelReportId			 
 ,ISNULL(STUFF(( SELECT  ',' +  CASE WHEN LEN(ISNULL(W.[vcWareHouse],'')  + ':' + ISNULL([WL].[vcLocation],'')) > 0 THEN ISNULL(W.[vcWareHouse],'')  + ':' + ISNULL([WL].[vcLocation],'') ELSE '' END
                FROM    [dbo].[WarehouseLocation] AS WL
				JOIN [dbo].[Warehouses] W ON [WL].[numWarehouseID] = [W].[numWareHouseID]
                WHERE  [WL].[numWLocationID] IN (
                        SELECT  WHI.[numWLocationID]
                        FROM    [dbo].[WareHouseItems] AS WHI
                                JOIN [dbo].[OpportunityItems] AS OI ON [WHI].[numWareHouseItemID] = [OI].[numWarehouseItmsID]
                        WHERE   [OI].[numOppId] = oppmst.[numOppId])
              FOR
                XML PATH('')
              ), 1, 1, ''),'') AS [vcLocation],
		(CASE WHEN ISNULL(RecurrenceConfiguration.numRecConfigID,0) = 0 THEN 0 ELSE 1 END) AS bitRecur,
		ISNULL(Opp.bitRecurred,0) AS bitRecurred,
		ISNULL((SELECT COUNT(*) FROM OpportunityBizDocs WHERE numOppID=@numOppID AND ISNULL(numDeferredBizDocID,0) = ISNULL(opp.numOppBizDocsId,0)),0) AS intInvoiceForDiferredBizDoc,
		ISNULL(Opp.bitShippingGenerated,0) AS bitShippingGenerated,
		ISNULL(oppmst.intUsedShippingCompany,0) as intUsedShippingCompany
 FROM                         
 OpportunityBizDocs  opp                        
 left join ListDetails mst                        
 on mst.numListItemID=opp.numBizDocId                        
 join OpportunityMaster oppmst                        
 on oppmst.numOppId= opp.numOppId                                
 left join  AuthoritativeBizDocs  AB
 on AB.numDomainId=  oppmst.numDomainID             
 LEFT OUTER JOIN [Currency] C
  ON C.numCurrencyID = oppmst.numCurrencyID
  LEFT JOIN AdditionalContactsInformation con ON con.numContactid = oppmst.numContactId
  LEFT JOIN
	RecurrenceConfiguration
  ON
	RecurrenceConfiguration.numOppBizDocID = opp.numOppBizDocsId
where opp.numOppId=@numOppId

UNION ALL

select  0 AS numOppBizDocsId,                        
 RH.numOppId,                        
 0 AS numBizDocId,                        
 1 AS numOrientation,  
 0 AS bitKeepFooterBottom,                   
 '' AS vcPOppName,                        
 'RMA' as BizDoc,                        
 RH.numCreatedBy,                        
DATEADD(minute,-@ClientTimeZoneOffset,RH.dtCreatedDate) CreatedDate,
 '' vcRefOrderNo,                       
 RH.vcRMA AS vcBizDocID,
 RH.vcRMA AS vcBizDocName,      
  ISNULL(monAmount,0) as monPAmount,
  'RMA' as BizDocType,
  0 AS monAmountPaid, 
  '' varCurrSymbol ,
  0 bitAuthoritativeBizDocs,
  0 tintshipped,0 as numShipVia,RH.numDivisionId,0 as tintDeferred,
  0 AS numBizDocTempID,con.numContactid,isnull(con.vcEmail,'') AS vcEmail,0 AS numBizDocStatus,
  0 AS numBillingDaysName,RH.dtCreatedDate AS dtFromDate,
  RH.dtCreatedDate AS BillingDate,
  1 fltExchangeRateCurrent,
  1 fltExchangeRateOfOrder,
  @BaseCurrencySymbol BaseCurrencySymbol,RH.numReturnHeaderID,RH.tintReturnType,1 AS fltExchangeRateBizDoc
  ,0 [IsShippingReportGenerated],0 [ISTrackingNumGenerated],
  0 AS numShippingReportId,
  0 AS numShippingLabelReportId			 
  ,ISNULL(STUFF(( SELECT  ',' +  CASE WHEN LEN(ISNULL(W.[vcWareHouse],'')  + ':' + ISNULL([WL].[vcLocation],'')) > 0 THEN ISNULL(W.[vcWareHouse],'')  + ':' + ISNULL([WL].[vcLocation],'') ELSE '' END
                FROM    [dbo].[WarehouseLocation] AS WL
				JOIN [dbo].[Warehouses] W ON [WL].[numWarehouseID] = [W].[numWareHouseID]
                WHERE  [WL].[numWLocationID] IN (
                        SELECT  WHI.[numWLocationID]
                        FROM    [dbo].[WareHouseItems] AS WHI
                                JOIN [dbo].[OpportunityItems] AS OI ON [WHI].[numWareHouseItemID] = [OI].[numWarehouseItmsID]
                        WHERE   [OI].[numOppId] = RH.[numOppId])
              FOR
                XML PATH('')
              ), 1, 1, ''),'') AS [vcLocation],
			  0 AS bitRecur,
			  0 AS bitRecurred,
		0 AS intInvoiceForDiferredBizDoc ,
		0 AS bitShippingGenerated,
		0 as intUsedShippingCompany
from                         
 ReturnHeader RH                        
 LEFT JOIN AdditionalContactsInformation con ON con.numContactid = RH.numContactId
  
where RH.numOppId=@numOppId
)X         
                        
END




GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OPPGetINItems')
DROP PROCEDURE USP_OPPGetINItems
GO
-- =============================================  
-- Modified by: <Author,,Sachin Sadhu>  
-- Create date: <Create Date,,24thDec2013>  
-- Description: <Description,,add 3 fields:vcBizDocImagePath,vcBizDocFooter,vcPurBizDocFooter>  
-- =============================================  
CREATE PROCEDURE [dbo].[USP_OPPGetINItems]
(
          @byteMode        AS TINYINT  = NULL,
          @numOppId        AS NUMERIC(9)  = NULL,
          @numOppBizDocsId AS NUMERIC(9)  = NULL,
          @numDomainID     AS NUMERIC(9)  = 0,
          @numUserCntID    AS NUMERIC(9)  = 0,
          @ClientTimeZoneOffset INT=0)
AS
  IF @byteMode = 1
    BEGIN
      DECLARE  @BizDcocName  AS VARCHAR(100)
      DECLARE  @OppName  AS VARCHAR(100)
      DECLARE  @Contactid  AS VARCHAR(100)
      DECLARE  @shipAmount  AS MONEY
      DECLARE  @tintOppType  AS TINYINT
      DECLARE  @numlistitemid  AS VARCHAR(15)
      DECLARE  @RecOwner  AS VARCHAR(15)
      DECLARE  @MonAmount  AS VARCHAR(15)
      DECLARE  @OppBizDocID  AS VARCHAR(100)
      DECLARE  @bizdocOwner  AS VARCHAR(15)
      DECLARE  @tintBillType  AS TINYINT
      DECLARE  @tintShipType  AS TINYINT
      DECLARE  @numCustomerDivID AS NUMERIC 
      
      SELECT @RecOwner = numRecOwner,@MonAmount = CONVERT(VARCHAR(20),monPAmount),
             @Contactid = numContactId,@OppName = vcPOppName,
             @tintOppType = tintOppType,@tintBillType = tintBillToType,@tintShipType = tintShipToType
      FROM   OpportunityMaster WHERE  numOppId = @numOppId
      
      -- When Creating PO from SO and Bill type is Customer selected 
      SELECT @numCustomerDivID = ISNULL(numDivisionID,0) FROM dbo.OpportunityMaster WHERE 
			numOppID IN (SELECT  ISNULL(numParentOppID,0) FROM dbo.OpportunityLinking WHERE numChildOppID = @numOppId)
			
      --select @BizDcocName=vcBizDocID from OpportunityBizDocs where numOppBizDocsId=@numOppBizDocsId
      SELECT @BizDcocName = vcdata,
             @shipAmount = monShipCost,
             @numlistitemid = numlistitemid,
             @OppBizDocID = vcBizDocID,
             @bizdocOwner = OpportunityBizDocs.numCreatedBy
      FROM   listdetails JOIN OpportunityBizDocs ON numBizDocId = numlistitemid
      WHERE  numOppBizDocsId = @numOppBizDocsId
      
      SELECT @bizdocOwner AS BizDocOwner,
             @OppBizDocID AS BizDocID,
             @MonAmount AS Amount,
             isnull(@RecOwner,1) AS Owner,
             @BizDcocName AS BizDcocName,
             @numlistitemid AS BizDoc,
             @OppName AS OppName,
             VcCompanyName AS CompName,
             @shipAmount AS ShipAmount,
             dbo.fn_getOPPAddress(@numOppId,@numDomainID,1) AS BillAdd,
             dbo.fn_getOPPAddress(@numOppId,@numDomainID,2) AS ShipAdd,
             AD.VcStreet,
             AD.VcCity,
             isnull(dbo.fn_GetState(AD.numState),'') AS vcBilState,
             AD.vcPostalCode,
             isnull(dbo.fn_GetListItemName(AD.numCountry),
                    '') AS vcBillCountry,
             con.vcFirstName AS ConName,
             ISNULL(con.numPhone,'') numPhone,
             con.vcFax,
             isnull(con.vcEmail,'') AS vcEmail,
             div.numDivisionID,
             numContactid,
             isnull(vcFirstFldValue,'') vcFirstFldValue,
             isnull(vcSecndFldValue,'') vcSecndFldValue,
             isnull(bitTest,0) AS bitTest,
			dbo.fn_getOPPAddress(@numOppId,@numDomainID,1) AS BillToAddressName,
			dbo.fn_getOPPAddress(@numOppId,@numDomainID,1) AS ShipToAddressName,
             CASE 
               WHEN @tintBillType IS NULL THEN Com.vcCompanyName
             	-- When Sales order and bill to is set to customer 
               WHEN @tintBillType = 1 AND @tintOppType = 1 THEN Com.vcCompanyName
             -- When Create PO from SO and Bill to is set to Customer 
			   WHEN @tintBillType = 1 AND @tintOppType = 2 THEN dbo.fn_GetComapnyName(@numCustomerDivID)
               WHEN @tintBillType = 0 THEN (SELECT TOP 1 Com1.vcCompanyName
                                            FROM   companyinfo [Com1]
                                                   JOIN divisionmaster div1
                                                     ON com1.numCompanyID = div1.numCompanyID
                                                   JOIN Domain D1
                                                     ON D1.numDivisionID = div1.numDivisionID
                                                    JOIN dbo.AddressDetails AD1
														ON AD1.numDomainID = div1.numDomainID AND AD1.numRecordID = div1.numDivisionID AND tintAddressOf = 2 AND tintAddressType = 1 AND bitIsPrimary=1
                                            WHERE  D1.numDomainID = @numDomainID)
               WHEN @tintBillType = 2 THEN (SELECT vcBillCompanyName
                                            FROM   OpportunityAddress
                                            WHERE  numOppID = @numOppId)
				WHEN @tintBillType = 3 THEN  Com.vcCompanyName
              END AS BillToCompanyName,
             CASE 
               WHEN @tintShipType IS NULL THEN Com.vcCompanyName
               WHEN @tintShipType = 1 AND @tintOppType = 1 THEN Com.vcCompanyName
				-- When Create PO from SO and Ship to is set to Customer 
   			   WHEN @tintShipType = 1 AND @tintOppType = 2 THEN dbo.fn_GetComapnyName(@numCustomerDivID)
               WHEN @tintShipType = 0 THEN (SELECT TOP 1 Com1.vcCompanyName
                                            FROM   companyinfo [Com1]
                                                   JOIN divisionmaster div1
                                                     ON com1.numCompanyID = div1.numCompanyID
                                                   JOIN Domain D1
                                                     ON D1.numDivisionID = div1.numDivisionID
                                                    JOIN dbo.AddressDetails AD1
													 ON AD1.numDomainID = div1.numDomainID AND AD1.numRecordID = div1.numDivisionID AND tintAddressOf = 2 AND tintAddressType = 2 AND bitIsPrimary=1
                                            WHERE  D1.numDomainID = @numDomainID)
               WHEN @tintShipType = 2 THEN (SELECT vcShipCompanyName
                                            FROM   OpportunityAddress
                                            WHERE  numOppID = @numOppId)
				WHEN @tintShipType = 3 THEN (SELECT vcShipCompanyName
														FROM   OpportunityAddress
														WHERE  numOppID = @numOppId)
             END AS ShipToCompanyName
      FROM   companyinfo [Com] JOIN divisionmaster div ON com.numCompanyID = div.numCompanyID
             JOIN AdditionalContactsInformation con ON con.numdivisionId = div.numdivisionId
             JOIN Domain D ON D.numDomainID = div.numDomainID
             LEFT JOIN PaymentGatewayDTLID PGDTL ON PGDTL.intPaymentGateWay = D.intPaymentGateWay AND D.numDomainID = PGDTL.numDomainID
             LEFT JOIN AddressDetails AD --Billing add
				ON AD.numDomainID=DIV.numDomainID AND AD.numRecordID=div.numDivisionID AND AD.tintAddressOf = 2 AND AD.tintAddressType = 1 AND AD.bitIsPrimary=1
			 LEFT JOIN AddressDetails AD2--Shipping address
				ON AD2.numDomainID=DIV.numDomainID AND AD2.numRecordID=div.numDivisionID AND AD2.tintAddressOf = 2 AND AD2.tintAddressType = 2 AND AD2.bitIsPrimary=1	
      WHERE  numContactid = @Contactid AND Div.numDomainID = @numDomainID
    END
  IF @byteMode = 2
    BEGIN
      SELECT opp.vcBizDocID,
             Mst.fltDiscount AS decDiscount,
             isnull(opp.monAmountPaid,0) AS monAmountPaid,
             isnull(opp.vcComments,'') AS vcComments,
             CONVERT(VARCHAR(20),DATEADD(minute,-@ClientTimeZoneOffset,opp.dtCreatedDate)) dtCreatedDate,
             dbo.fn_GetContactName(opp.numCreatedby) AS numCreatedby,
             dbo.fn_GetContactName(opp.numModifiedBy) AS numModifiedBy,
             CONVERT(VARCHAR(20),DATEADD(minute,-@ClientTimeZoneOffset,opp.dtModifiedDate)) dtModifiedDate,
             CASE 
               WHEN isnull(opp.numViewedBy,0) = 0 THEN ''
               ELSE dbo.fn_GetContactName(opp.numViewedBy)
             END AS numViewedBy,
             CASE 
               WHEN opp.numViewedBy = 0 THEN NULL
               ELSE opp.dtViewedDate
             END AS dtViewedDate,
             isnull(Mst.bitBillingTerms,0) AS tintBillingTerms,
             isnull(Mst.intBillingDays,0) AS numBillingDays,
--			 case when isnumeric(ISNULL(dbo.fn_GetListItemName(isnull(Mst.intBillingDays,0)),0))=1 
--				  then ISNULL(dbo.fn_GetListItemName(isnull(Mst.intBillingDays,0)),0) 
--				  else 0 
--			 end AS numBillingDaysName,
--			 CASE WHEN ISNUMERIC((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(Mst.intBillingDays,0))) = 1
-- 				   THEN (SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(Mst.intBillingDays,0))
--				   ELSE 0
--			  END AS numBillingDaysName,
			 ISNULL(BTR.numNetDueInDays,0) AS numBillingDaysName,
			 BTR.vcTerms AS vcBillingTermsName,
             isnull(Mst.bitInterestType,0) AS tintInterestType,
             isnull(Mst.fltInterest,0) AS fltInterest,
             tintOPPType,
             Opp.numBizDocId,
             dbo.fn_GetContactName(numApprovedBy) AS ApprovedBy,
             dtApprovedDate,
             Mst.bintAccountClosingDate,
             tintShipToType,
             tintBillToType,
             tintshipped,
             dtShippedDate bintShippedDate,
             isnull(opp.monShipCost,0) AS monShipCost,
             (SELECT COUNT(* )
              FROM   DocumentWorkflow
              WHERE  numDocID = @numOppBizDocsId
                     AND numContactID = @numUserCntID
                     AND cDocType = 'B'
                     AND tintApprove = 0) AS AppReq,
             (SELECT COUNT(* )
              FROM   DocumentWorkflow
              WHERE  numDocID = @numOppBizDocsId
                     AND cDocType = 'B'
                     AND tintApprove = 1) AS Approved,
             (SELECT COUNT(* )
              FROM   DocumentWorkflow
              WHERE  numDocID = @numOppBizDocsId
                     AND cDocType = 'B'
                     AND tintApprove = 2) AS Declined,
             (SELECT COUNT(* )
              FROM   DocumentWorkflow
              WHERE  numDocID = @numOppBizDocsId
                     AND cDocType = 'B'
                     AND tintApprove = 0) AS Pending,
             ISNULL(BT.vcBizDocFooter,D.vcBizDocFooter) AS BizdocFooter,
             ISNULL(BT.vcBizDocImagePath,D.vcBizDocImagePath) As vcBizDocImagePath, 
             Mst.numDivisionID,
             ISNULL(BT.vcPurBizDocFooter,D.vcPurBizDocFooter) AS PurBizdocFooter, 
             isnull(Mst.fltDiscount,0) AS fltDiscount,
             isnull(Mst.bitDiscountType,0) AS bitDiscountType,
             isnull(Opp.numShipVia,0) AS numShipVia,
--             isnull(vcTrackingURL,'') AS vcTrackingURL,
			ISNULL( (SELECT vcShipFieldValue FROM dbo.ShippingFieldValues WHERE numDomainID=MSt.numDomainID AND intShipFieldID IN ( SELECT intShipFieldID FROM dbo.ShippingFields WHERE numListItemID=Opp.numShipVia  AND vcFieldName='Tracking URL')),'')		AS vcTrackingURL,
             isnull(Opp.numBizDocStatus,0) AS numBizDocStatus,
             CASE 
               WHEN Opp.numBizDocStatus IS NULL THEN '-'
               ELSE dbo.fn_GetListItemName(Opp.numBizDocStatus)
             END AS BizDocStatus,
             CASE 
               WHEN Opp.numShipVia IS NULL THEN '-'
               ELSE dbo.fn_GetListItemName(Opp.numShipVia)
             END AS ShipVia,
             ISNULL(C.varCurrSymbol,'') varCurrSymbol,
             (SELECT COUNT(*) FROM [ShippingReport] WHERE numOppBizDocId = opp.numOppBizDocsId )AS ShippingReportCount,
			 isnull(bitPartialFulfilment,0) bitPartialFulfilment,
			 ISNULL(Opp.vcBizDocName,'') vcBizDocName,
			 dbo.[fn_GetContactName](opp.[numModifiedBy])  + ', '
				+ CONVERT(VARCHAR(20),dateadd(MINUTE,-@ClientTimeZoneOffset,Opp.dtModifiedDate)) AS ModifiedBy,
			 dbo.[fn_GetContactName](Mst.[numRecOwner]) + ', ' +  dbo.[fn_GetContactName](Mst.[numAssignedTo]) AS OrderRecOwner,
			 dbo.[fn_GetContactName](DM.[numRecOwner]) + ', ' +  dbo.[fn_GetContactName](DM.[numAssignedTo]) AS AccountRecOwner,
			 Opp.dtFromDate,
			 Mst.tintTaxOperator,
			 dbo.GetAccountedEmbeddedCost(opp.numOppBizDocsID,1) AS monTotalEmbeddedCost,
			 dbo.GetAccountedEmbeddedCost(opp.numOppBizDocsID,2) AS monTotalAccountedCost,
			 ISNULL(BT.bitEnabled,0) bitEnabled,
			 ISNULL(BT.txtBizDocTemplate,'') txtBizDocTemplate, 
			 ISNULL(BT.txtCSS,'') txtCSS,
			 ISNULL(BT.numOrientation,0) numOrientation,
			 ISNULL(BT.bitKeepFooterBottom,0) bitKeepFooterBottom,
			 dbo.fn_GetContactName(Mst.numAssignedTo) AS AssigneeName,
			 ISNULL((SELECT vcEmail FROM dbo.AdditionalContactsInformation WHERE numContactID = Mst.numAssignedTo),'') AS AssigneeEmail,
			 ISNULL((SELECT numPhone FROM dbo.AdditionalContactsInformation WHERE numContactID = Mst.numAssignedTo),'') AS AssigneePhone,
			 dbo.fn_GetComapnyName(Mst.numDivisionId) OrganizationName,
			 dbo.fn_GetContactName(Mst.numContactID)  OrgContactName,

--CASE WHEN Mst.tintOppType = 1 THEN 
--dbo.getCompanyAddress((select ISNULL(DM.numDivisionID,0)  as numDivisionID from AdditionalContactsInformation AC INNER JOIN DivisionMaster DM 
--on AC.numDivisionID = DM.numDivisionID where numContactID = Mst.numCreatedBy),1,Mst.numDomainId)
--               ELSE dbo.getCompanyAddress(Mst.numDivisionId,1,Mst.numDomainId)
--             END AS CompanyBillingAddress1,

CASE WHEN Mst.tintOppType = 1 THEN 
dbo.getCompanyAddress((select ISNULL(AC.numDivisionID,0)  as numDivisionID from AdditionalContactsInformation AC
INNER JOIN Domain D ON D.numDomainID = AC.numDomainID where AC.numContactID = D.numAdminID AND D.numDomainID = Mst.numDomainId),1,Mst.numDomainId)
               ELSE dbo.getCompanyAddress(Mst.numDivisionId,1,Mst.numDomainId)
             END AS CompanyBillingAddress,

CASE WHEN Mst.tintOppType = 1 THEN 
dbo.getCompanyAddress((select ISNULL(AC.numDivisionID,0)  as numDivisionID from AdditionalContactsInformation AC
INNER JOIN Domain D ON D.numDomainID = AC.numDomainID where AC.numContactID = D.numAdminID AND D.numDomainID = Mst.numDomainId),1,Mst.numDomainId)
               ELSE dbo.getCompanyAddress(Mst.numDivisionId,2,Mst.numDomainId)
             END AS CompanyShippingAddress,
--
--			 dbo.getCompanyAddress(Mst.numDivisionId,1,Mst.numDomainId) CompanyBillingAddress,
--			 dbo.getCompanyAddress(Mst.numDivisionId,2,Mst.numDomainId) CompanyShippingAddress,
			 case when ACI.numPhone<>'' then ACI.numPhone +case when ACI.numPhoneExtension<>'' then ' - ' + ACI.numPhoneExtension else '' end  else '' END OrgContactPhone,
			 DM.vcComPhone as OrganizationPhone,
             ISNULL(ACI.vcEmail,'') AS OrgContactEmail,
 			 dbo.[fn_GetContactName](Mst.[numRecOwner]) AS OnlyOrderRecOwner,
 			 ISNULL(Opp.bitAuthoritativeBizDocs,0) bitAuthoritativeBizDocs,
			 ISNULL(Opp.tintDeferred,0) tintDeferred,isnull(Opp.monCreditAmount,0) AS monCreditAmount,
			 isnull(Opp.bitRentalBizDoc,0) as bitRentalBizDoc,isnull(BT.numBizDocTempID,0) as numBizDocTempID,isnull(BT.vcTemplateName,'') as vcTemplateName,
		     [dbo].[GetDealAmount](opp.numOppId ,getutcdate(),opp.numOppBizDocsId ) as monPAmount,
			 --(select top 1 SD.vcSignatureFile from SignatureDetail SD join OpportunityBizDocsDetails OBD on SD.numSignID=OBD.numSignID where SD.numDomainID = @numDomainID and OBD.numDomainID = @numDomainID and OBD.numSignID IS not null and OBD.numBizDocsId=opp.numOppBizDocsId order by OBD.numBizDocsPaymentDetId desc) as vcSignatureFile,
			 isnull(CMP.txtComments,'') as vcOrganizationComments,
			 isnull(Opp.vcTrackingNo,'') AS  vcTrackingNo,isnull(Opp.vcShippingMethod,'') AS  vcShippingMethod,
			 Opp.dtDeliveryDate,
			 CASE WHEN Opp.vcRefOrderNo IS NULL OR LEN(Opp.vcRefOrderNo)=0 THEN ISNULL(Mst.vcOppRefOrderNo,'') ELSE 
			  isnull(Opp.vcRefOrderNo,'') END AS vcRefOrderNo,ISNULL(Mst.numDiscountAcntType,0) AS numDiscountAcntType,
			 ISNULL(opp.numSequenceId,'') AS numSequenceId,
			 CASE WHEN ISNULL(Opp.numBizDocId,0)=296 THEN ISNULL((SELECT SUBSTRING((SELECT '$^$' + dbo.fn_GetListItemName(numBizDocStatus) +'#^#'+ dbo.fn_GetContactName(numUserCntID) +'#^#'+ dbo.FormatedDateTimeFromDate(DateAdd(minute, -@ClientTimeZoneOffset , dtCreatedDate),@numDomainId) + '#^#'+ isnull(DFCS.vcColorScheme,'NoForeColor')
					FROM OppFulfillmentBizDocsStatusHistory OFBS left join DycFieldColorScheme DFCS on OFBS.numBizDocStatus=DFCS.vcFieldValue AND DFCS.numDomainID=@numDomainID
					WHERE numOppId=opp.numOppId and numOppBizDocsId=opp.numOppBizDocsId FOR XML PATH('')),4,200000)),'') ELSE '' END AS vcBizDocsStatusList,ISNULL(Mst.numCurrencyID,0) AS numCurrencyID,
			ISNULL(Opp.fltExchangeRateBizDoc,Mst.fltExchangeRate) AS fltExchangeRateBizDoc,
			com1.vcCompanyName AS EmployerOrganizationName,div1.vcComPhone as EmployerOrganizationPhone,ISNULL(opp.bitAutoCreated,0) AS bitAutoCreated,
			ISNULL(RecurrenceConfiguration.numRecConfigID,0) AS numRecConfigID,
			(CASE WHEN RecurrenceConfiguration.numRecConfigID IS NULL THEN 0 ELSE 1 END) AS bitRecur,
			(CASE WHEN RecurrenceTransaction.numRecTranID IS NULL THEN 0 ELSE 1 END) AS bitRecurred,
			ISNULL(Mst.bitRecurred,0) AS bitOrderRecurred,
			dbo.FormatedDateFromDate(RecurrenceConfiguration.dtStartDate,@numDomainID) AS dtStartDate,
			RecurrenceConfiguration.vcFrequency AS vcFrequency,
			ISNULL(RecurrenceConfiguration.bitDisabled,0) AS bitDisabled,
			ISNULL(opp.bitFulfilled,0) AS bitFulfilled,
			ISNULL(opp.numDeferredBizDocID,0) AS numDeferredBizDocID,
			dbo.GetTotalQtyByUOM(opp.numOppBizDocsId) AS vcTotalQtybyUOM,
			ISNULL(Mst.intUsedShippingCompany,0) AS intUsedShippingCompany
		FROM   OpportunityBizDocs opp JOIN OpportunityMaster Mst ON Mst.numOppId = opp.numOppId
             JOIN Domain D ON D.numDomainID = Mst.numDomainID
             LEFT OUTER JOIN [Currency] C ON C.numCurrencyID = Mst.numCurrencyID
             LEFT JOIN [DivisionMaster] DM ON DM.numDivisionID = Mst.numDivisionID   
			 LEFT JOIN CompanyInfo CMP ON DM.numCompanyId = CMP.numCompanyId and CMP.numDomainID = @numDomainID  
             LEFT JOIN dbo.AdditionalContactsInformation ACI ON ACI.numContactID = Mst.numContactID
             LEFT JOIN [BizDocTemplate] BT ON BT.numDomainID = @numDomainID AND BT.numBizDocID = Opp.numBizDocID AND BT.numOppType = Mst.tintOppType AND BT.tintTemplateType=0 and BT.numBizDocTempID=opp.numBizDocTempID
             LEFT JOIN BillingTerms BTR ON BTR.numTermsID = ISNULL(Mst.intBillingDays,0)
             JOIN divisionmaster div1 ON D.numDivisionID = div1.numDivisionID
			 JOIN companyinfo [Com1] ON com1.numCompanyID = div1.numCompanyID
			 LEFT JOIN RecurrenceConfiguration ON Opp.numOppBizDocsId = RecurrenceConfiguration.numOppBizDocID
			 LEFT JOIN RecurrenceTransaction ON Opp.numOppBizDocsId = RecurrenceTransaction.numRecurrOppBizDocID
      WHERE  opp.numOppBizDocsId = @numOppBizDocsId
             AND Mst.numDomainID = @numDomainID
    END
/****** Object:  StoredProcedure [dbo].[USP_OppShippingorReceiving]    Script Date: 07/26/2008 16:20:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- created by anoop jayaraj            
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityMaster_GlobalLocationFulfilled')
DROP PROCEDURE USP_OpportunityMaster_GlobalLocationFulfilled
GO
CREATE PROCEDURE [dbo].[USP_OpportunityMaster_GlobalLocationFulfilled]
@numDomainID NUMERIC(18,0),
@numOppID AS NUMERIC(18,0)
AS
BEGIN
	IF (SELECT 
			COUNT(*) 
		FROM 
			OpportunityMaster OM 
		INNER JOIN 
			OpportunityItems OI 
		ON 
			OM.numOppId=OI.numOppId 
		INNER JOIN 
			WareHouseItems WI 
		ON 
			OI.numWarehouseItmsID=WI.numWareHouseItemID 
		WHERE 
			OM.numDomainId=@numDomainID 
			AND OM.numOppId=@numOppID
			AND WI.numWLocationID = -1
			AND OI.numUnitHour <> ISNULL(OI.numUnitHourReceived,0)) > 0
	BEGIN
		SELECT 0
	END
	ELSE
	BEGIN
		SELECT 1
	END
END
/****** Object:  StoredProcedure [dbo].[USP_OppShippingorReceiving]    Script Date: 07/26/2008 16:20:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- created by anoop jayaraj            
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_oppshippingorreceiving')
DROP PROCEDURE usp_oppshippingorreceiving
GO
CREATE PROCEDURE [dbo].[USP_OppShippingorReceiving]            
@OppID as numeric(9),
@numUserCntID AS NUMERIC(9)          
as
BEGIN TRY
	declare @status as TINYINT            
	declare @OppType as TINYINT   
	declare @bitStockTransfer as BIT
	DECLARE @fltExchangeRate AS FLOAT 
	DECLARE @numDomain AS NUMERIC(18,0)
	SELECT @numDomain = OM.numDomainID,@status=tintOppStatus,@OppType=tintOppType,@bitStockTransfer=ISNULL(bitStockTransfer,0),@fltExchangeRate=(CASE WHEN ISNULL(fltExchangeRate,0)=0 THEN 1 ELSE fltExchangeRate END) FROM [dbo].[OpportunityMaster] AS OM WHERE [OM].[numOppId] = @OppID
	
	-- Added by Manish Anjara : 17th Sep,2014
	-- Check whether order is already closed or not. If closed then show a warning message
	IF (SELECT ISNULL(tintshipped,0) FROM [dbo].[OpportunityMaster] WHERE [OpportunityMaster].[numOppId] = @OppID AND [OpportunityMaster].[numDomainId] = @numDomain) = 1
	BEGIN
		RAISERROR('ORDER_CLOSED', 16, 1)
	END		

	IF @OppType = 2 AND @status=1 AND ISNULL(@bitStockTransfer,0)=0 AND (SELECT 
			COUNT(*) 
		FROM 
			OpportunityMaster OM 
		INNER JOIN 
			OpportunityItems OI 
		ON 
			OM.numOppId=OI.numOppId 
		LEFT JOIN 
			WareHouseItems WI 
		ON 
			OI.numWarehouseItmsID=WI.numWareHouseItemID 
		WHERE 
			OM.numDomainId=@numDomain 
			AND OM.numOppId=@OppID
			AND WI.numWLocationID = -1
			AND OI.numUnitHour <> OI.numUnitHourReceived) > 0
	BEGIN
		RAISERROR('GLOCAL_LOCATION_PENDING_FULFILLMENT', 16, 1)
	END

   BEGIN TRANSACTION 
			
			update OpportunityMaster set tintshipped=1,bintAccountClosingDate=GETUTCDATE(), bintClosedDate=GETUTCDATE() where  numOppId=@OppID 
			UPDATE [OpportunityBizDocs] SET [dtShippedDate] = GETUTCDATE() WHERE [numOppId]=@OppID AND numBizDocID <> 296

			UPDATE [OpportunityBizDocs] SET [dtShippedDate] = GETUTCDATE() WHERE [numOppId]=@OppID AND numBizDocID = 296 AND dtShippedDate IS NULL
			
						
			
       
			
			-- Archive item based on Item's individual setting
			IF @OppType = 1
			BEGIN
				UPDATE Item SET IsArchieve = CASE WHEN EXISTS(SELECT OpportunityItems.numItemCode FROM dbo.OpportunityItems WHERE OpportunityItems.numItemCode = I.numItemCode) 
												  THEN 1
												  ELSE 0 
											 END 
				FROM Item I JOIN dbo.OpportunityItems OI ON I.numItemCode = OI.numItemCode  
				WHERE numOppId = @OppID 
				AND I.[numDomainID] = @numDomain

				AND ISNULL(I.bitArchiveItem,0) = 1 
			END
			----------------------------------------------------------------------------            
			--Updating Item Details            
			-----------------------------------------------------------------------            
            
			---Updating the inventory Items once the deal is won
			declare @numoppitemtCode as numeric
			declare @numUnits as FLOAT
			declare @numWarehouseItemID as numeric       
			declare @numToWarehouseItemID as numeric(9) --to be used with stock transfer
			declare @itemcode as numeric        
			declare @QtyShipped as FLOAT
			declare @QtyReceived as FLOAT
			Declare @monPrice as money
			declare @Kit as bit
			declare @bitWorkOrder AS BIT
			declare @bitSerialized AS BIT
			declare @bitLotNo AS BIT
    
			 select top 1 @numoppitemtCode=numoppitemtCode,@itemcode=OI.numItemCode,@numUnits=ISNULL(numUnitHour,0),@qtyShipped=ISNULL([numQtyShipped],0),@qtyReceived=ISNULL([numUnitHourReceived],0),
			 @numWareHouseItemID=ISNULL(numWarehouseItmsID,0),@numToWarehouseItemID=numToWarehouseItemID,@monPrice=isnull(monPrice,0) * @fltExchangeRate,@Kit= (case when bitKitParent=1 and bitAssembly=1 then 0 when bitKitParent=1 then 1 else 0 end),
			 @bitWorkOrder = ISNULL(bitWorkOrder,0),@bitSerialized=ISNULL(bitSerialized,0),@bitLotNo=ISNULL(bitLotNo,0)
			 from OpportunityItems OI join Item I                                                
			 on OI.numItemCode=I.numItemCode and numOppId=@OppID and (bitDropShip=0 or bitDropShip is null)                                            
			 where (charitemtype='P' OR 1=(CASE WHEN @OppType=1 THEN 
							CASE WHEN ISNULL(I.bitKitParent,0) = 1 AND ISNULL(I.bitAssembly,0) = 1 THEN 0
								 WHEN ISNULL(I.bitKitParent,0) = 1 THEN 1
								 ELSE 0 END 
							ELSE 0 END))  
			 AND I.[numDomainID] = @numDomain	
							order by OI.numoppitemtCode      

			 while @numoppitemtCode>0                
			 begin   
 
--			  if @Kit = 1
--			  begin
--					exec USP_UpdateKitItems @numoppitemtCode,@numUnits,@OppType,2,@OppID
--			  end
             
             
				IF @bitStockTransfer = 1
				BEGIN
					--Make inventory changes as if sales order was closed , using OpportunityItems.numWarehouseItmsID
					EXEC usp_ManageInventory @itemcode,@numWareHouseItemID,0,1,@numUnits,@qtyShipped,@qtyReceived,@monPrice,2,@OppID,@numoppitemtCode,@numUserCntID,@bitWorkOrder
					--Make inventory changes as if purchase order was closed, using OpportunityItems.numToWarehouseItemID
					EXEC usp_ManageInventory @itemcode,@numToWarehouseItemID,0,2,@numUnits,@qtyShipped,@qtyReceived,@monPrice,2,@OppID,@numoppitemtCode,@numUserCntID,@bitWorkOrder
					If @bitSerialized = 1 OR @bitLotNo = 1
					BEGIN
						--Transfer Serial/Lot Numbers
						EXEC USP_WareHouseItmsDTL_TransferSerialLotNo @OppID,@numoppitemtCode,@numWareHouseItemID,@numToWarehouseItemID,@bitSerialized,@bitLotNo
					END
				END
				ELSE
				BEGIN
					EXEC usp_ManageInventory @itemcode,@numWareHouseItemID,0,@OppType,@numUnits,@qtyShipped,@qtyReceived,@monPrice,2,@OppID,@numoppitemtCode,@numUserCntID,@bitWorkOrder
				END
        
			  select top 1 @numoppitemtCode=numoppitemtCode,@itemcode=OI.numItemCode,@numUnits=ISNULL(numUnitHour,0),@qtyShipped=ISNULL([numQtyShipped],0),@qtyReceived=ISNULL([numUnitHourReceived],0),
			 @numWareHouseItemID=ISNULL(numWarehouseItmsID,0),@numToWarehouseItemID=numToWarehouseItemID,@monPrice=isnull(monPrice,0) * @fltExchangeRate,@Kit= (case when bitKitParent=1 and bitAssembly=1 then 0 when bitKitParent=1 then 1 else 0 end),
			 @bitWorkOrder = ISNULL(bitWorkOrder,0),@bitSerialized=ISNULL(bitSerialized,0),@bitLotNo=ISNULL(bitLotNo,0)
			  from OpportunityItems OI
			  join Item I
			  on OI.numItemCode=I.numItemCode and numOppId=@OppID                                              
			  where (charitemtype='P' OR 1=(CASE WHEN @OppType=1 THEN 
							CASE WHEN ISNULL(I.bitKitParent,0) = 1 AND ISNULL(I.bitAssembly,0) = 1 THEN 0
								 WHEN ISNULL(I.bitKitParent,0) = 1 THEN 1
								 ELSE 0 END 
							ELSE 0 END)) and OI.numoppitemtCode>@numoppitemtCode and (bitDropShip=0 or bitDropShip is null) 
			  AND I.numDomainID = @numDomain
			  ORDER by OI.numoppitemtCode                                                
			  if @@rowcount=0 set @numoppitemtCode=0     
  
			 END
 COMMIT
END TRY
BEGIN CATCH
  -- Whoops, there was an error
  IF @@TRANCOUNT > 0
     ROLLBACK

  -- Raise an error with the details of the exception
  DECLARE @ErrMsg nvarchar(4000), @ErrSeverity int
  SELECT @ErrMsg = ERROR_MESSAGE(),
         @ErrSeverity = ERROR_SEVERITY()

  RAISERROR(@ErrMsg, @ErrSeverity, 1)
END CATCH

GO
/****** Object:  StoredProcedure [dbo].[USP_RevertDetailsOpp]    Script Date: 07/26/2008 16:20:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj      
/* deducts Qty from allocation and Adds back to onhand */
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_RevertDetailsOpp')
DROP PROCEDURE USP_RevertDetailsOpp
GO
CREATE PROCEDURE [dbo].[USP_RevertDetailsOpp] 
@numOppId NUMERIC,
@tintMode AS TINYINT=0, -- 0:Add/Edit 1:Delete
@numUserCntID AS NUMERIC(9)
AS ---reverting back to previous state if deal is being edited 
	DECLARE @numDomain AS NUMERIC(18,0)
	SELECT @numDomain = numDomainID FROM [dbo].[OpportunityMaster] AS OM WHERE [OM].[numOppId] = @numOppID
    
	DECLARE @OppType AS VARCHAR(2)              
    DECLARE @itemcode AS NUMERIC       
    DECLARE @numWareHouseItemID AS NUMERIC
	DECLARE @numWLocationID NUMERIC(18,0)                     
    DECLARE @numToWarehouseItemID AS NUMERIC 
    DECLARE @numUnits AS FLOAT                                              
    DECLARE @onHand AS FLOAT                                            
    DECLARE @onOrder AS FLOAT                                            
    DECLARE @onBackOrder AS FLOAT                                              
    DECLARE @onAllocation AS FLOAT
    DECLARE @numQtyShipped AS FLOAT
    DECLARE @numUnitHourReceived AS FLOAT
    DECLARE @numoppitemtCode AS NUMERIC(9) 
    DECLARE @monAmount AS MONEY 
    DECLARE @monAvgCost AS MONEY   
    DECLARE @Kit AS BIT                                        
    DECLARE @bitKitParent BIT
    DECLARE @bitStockTransfer BIT 
    DECLARE @numOrigUnits AS FLOAT			
    DECLARE @description AS VARCHAR(100)
	DECLARE @bitWorkOrder AS BIT
	--Added by :Sachin Sadhu||Date:18thSept2014
	--For Rental/Asset Project
	Declare @numRentalIN as FLOAT
	Declare @numRentalOut as FLOAT
	Declare @numRentalLost as FLOAT
	DECLARE @bitAsset as BIT
	--end sachin

    						
    SELECT TOP 1
            @numoppitemtCode = numoppitemtCode,
            @itemcode = OI.numItemCode,
            @numUnits = ISNULL(numUnitHour,0),
            @numWareHouseItemID = ISNULL(numWarehouseItmsID,0),
			@numWLocationID = ISNULL(numWLocationID,0),
            @Kit = ( CASE WHEN bitKitParent = 1
                               AND bitAssembly = 1 THEN 0
                          WHEN bitKitParent = 1 THEN 1
                          ELSE 0
                     END ),
            @monAmount = ISNULL(monTotAmount,0) * (CASE WHEN ISNULL(fltExchangeRate,0)=0 THEN 1 ELSE fltExchangeRate END),
            @monAvgCost = ISNULL(monAverageCost,0),
            @numQtyShipped = ISNULL(numQtyShipped,0),
            @numUnitHourReceived = ISNULL(numUnitHourReceived,0),
            @bitKitParent=ISNULL(bitKitParent,0),
            @numToWarehouseItemID =OI.numToWarehouseItemID,
            @bitStockTransfer = ISNULL(OM.bitStockTransfer,0),
            @OppType = tintOppType,
		    @numRentalIN=ISNULL(oi.numRentalIN,0),
			@numRentalOut=Isnull(oi.numRentalOut,0),
			@numRentalLost=Isnull(oi.numRentalLost,0),
			@bitAsset =ISNULL(I.bitAsset,0),
			@bitWorkOrder = ISNULL(OI.bitWorkOrder,0)
    FROM    OpportunityItems OI
			LEFT JOIN WareHouseItems WI ON OI.numWarehouseItmsID=WI.numWareHouseItemID
			JOIN dbo.OpportunityMaster OM ON OI.numOppId = OM.numOppId
            JOIN Item I ON OI.numItemCode = I.numItemCode
    WHERE   (charitemtype='P' OR 1=(CASE WHEN tintOppType=1 THEN 
							CASE WHEN ISNULL(I.bitKitParent,0) = 1 AND ISNULL(I.bitAssembly,0) = 1 THEN 0
								 WHEN ISNULL(I.bitKitParent,0) = 1 THEN 1
								 ELSE 0 END 
							ELSE 0 END)) AND OI.numOppId = @numOppId
                           AND ( bitDropShip = 0
                                 OR bitDropShip IS NULL
                               ) 
    ORDER BY OI.numoppitemtCode

	PRINT '@numoppitemtCode:' + CAST(@numoppitemtCode AS VARCHAR(10))
	PRINT '@numUnits:' + CAST(@numUnits AS VARCHAR(10))
	PRINT '@numWareHouseItemID:' + CAST(@numWareHouseItemID AS VARCHAR(10))
	PRINT '@numQtyShipped:' + CAST(@numQtyShipped AS VARCHAR(10))
	PRINT '@numUnitHourReceived:' + CAST(@numUnitHourReceived AS VARCHAR(10))
	PRINT '@numToWarehouseItemID:' + CAST(@numToWarehouseItemID AS VARCHAR(10))
	PRINT '@bitStockTransfer:' + CAST(@bitStockTransfer AS VARCHAR(10))
	
    WHILE @numoppitemtCode > 0                                  
    BEGIN    
        SET @numOrigUnits=@numUnits
            
        IF @bitStockTransfer=1
        BEGIN
			SET @OppType = 1
		END
            
        PRINT '@OppType:' + CAST(@OppType AS VARCHAR(10))      
        IF @numWareHouseItemID>0
        BEGIN     
			PRINT 'FROM REVERT : CONDITION : @numWareHouseItemID>0'                             
			SELECT  @onHand = ISNULL(numOnHand, 0),
					@onAllocation = ISNULL(numAllocation, 0),
					@onOrder = ISNULL(numOnOrder, 0),
					@onBackOrder = ISNULL(numBackOrder, 0)
			FROM    WareHouseItems
			WHERE   numWareHouseItemID = @numWareHouseItemID       
				
			PRINT '@onHand:' + CAST(@onHand AS VARCHAR(10))
			PRINT '@onAllocation:' + CAST(@onAllocation AS VARCHAR(10))
			PRINT '@onOrder:' + CAST(@onOrder AS VARCHAR(10))
			PRINT '@onBackOrder:' + CAST(@onBackOrder AS VARCHAR(10))                                         
        END
            
        IF @OppType = 1 
        BEGIN
			SET @description='SO Deleted (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@numQtyShipped AS VARCHAR(10)) + ')'
                
            IF @Kit = 1
			BEGIN
				exec USP_UpdatingInventoryForEmbeddedKits @numoppitemtCode,@numOrigUnits,1,@numOppID,@tintMode,@numUserCntID
			END
			ELSE IF @bitWorkOrder = 1
			BEGIN
					
				IF @tintMode=0 -- EDIT
				BEGIN
					SET @description='SO-WO Edited (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@numQtyShipped AS VARCHAR(10)) + ')'
					-- UPDATE WORK ORDER - REVERT INVENTOY OF ASSEMBLY ITEMS
					EXEC USP_ManageInventoryAssemblyWO @numUserCntID,@numOppID,@itemcode,@numWareHouseItemID,@numQtyShipped,5
				END
				ELSE IF @tintMode=1 -- DELETE
				BEGIN
					DECLARE @numWOID AS NUMERIC(18,0)
					DECLARE @numWOStatus AS NUMERIC(18,0)
					SELECT @numWOID=numWOId,@numWOStatus=numWOStatus FROM WorkOrder WHERE numOppId=@numOppId AND numItemCode=@itemcode AND numWareHouseItemId=@numWareHouseItemID

					SET @description='SO-WO Deleted (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@numQtyShipped AS VARCHAR(10)) + ')'

					--IF WORK ORDER IS NOT COMPLETED THAN REMOVE ON ORDER QUANTITY
					IF @numWOStatus <> 23184
					BEGIN
						IF @onOrder >= @numUnits
							SET @onOrder = @onOrder - @numUnits
						ELSE
							SET @onOrder = 0
					END

					-- IF BACKORDER QTY IS GREATER THEN QTY TO REVERT THEN
					-- DECREASE BACKORDER QTY BY QTY TO REVERT
					IF @numUnits < @onBackOrder 
					BEGIN                  
						SET @onBackOrder = @onBackOrder - @numUnits
					END 
					-- IF BACKORDER QTY IS LESS THEN OR EQUAL TO QTY TO REVERT THEN
					-- DECREASE QTY TO REVERT BY QTY OF BACK ORDER AND
					-- SET BACKORDER QTY TO 0
					ELSE IF @numUnits >= @onBackOrder 
					BEGIN
						SET @numUnits = @numUnits - @onBackOrder
						SET @onBackOrder = 0
                        
						--REMOVE ITEM FROM ALLOCATION 
						IF (@onAllocation - @numUnits) >= 0
							SET @onAllocation = @onAllocation - @numUnits
						
						--ADD QTY TO ONHAND
						SET @onHand = @onHand + @numUnits
					END

					UPDATE  
						WareHouseItems
					SET 
						numOnHand = @onHand ,
						numAllocation = @onAllocation,
						numBackOrder = @onBackOrder,
						numOnOrder = @onOrder,
						dtModified = GETDATE()
					WHERE 
						numWareHouseItemID = @numWareHouseItemID   

					--IF WORK ORDER IS NOT COMPLETED THAN DELETE WORK ORDER AND ITS CHILDS WORK ORDER
					IF @numWOStatus <> 23184
					BEGIN
						EXEC USP_DeleteAssemblyWOAndInventory  @numDomain,@numUserCntID,@numWOID,@numOppID
					END
				END
			END	
			ELSE
			BEGIN	
				IF @numQtyShipped>0
				BEGIN
					IF @tintMode=0
						SET @numUnits = @numUnits - @numQtyShipped
					ELSE IF @tintmode=1
						SET @onAllocation = @onAllocation + @numQtyShipped 
				END 
								                    
                IF @numUnits >= @onBackOrder 
                BEGIN
                    SET @numUnits = @numUnits - @onBackOrder
                    SET @onBackOrder = 0
                            
                    IF (@onAllocation - @numUnits >= 0)
						SET @onAllocation = @onAllocation - @numUnits

					IF @bitAsset=1--Not Asset
					BEGIN
						SET @onHand = @onHand +@numRentalIN+@numRentalLost+@numRentalOut     
					END
					ELSE
					BEGIN
						SET @onHand = @onHand + @numUnits     
					END                                         
                END                                            
                ELSE IF @numUnits < @onBackOrder 
                BEGIN                  
					IF (@onBackOrder - @numUnits >0)
						SET @onBackOrder = @onBackOrder - @numUnits
                END 
                 	
				UPDATE  
					WareHouseItems
				SET 
					numOnHand = @onHand ,
					numAllocation = @onAllocation,
					numBackOrder = @onBackOrder,
					dtModified = GETDATE()
				WHERE 
					numWareHouseItemID = @numWareHouseItemID              
			END
				
			IF @numWareHouseItemID>0
			BEGIN 
					EXEC dbo.USP_ManageWareHouseItems_Tracking
					@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
					@numReferenceID = @numOppId, --  numeric(9, 0)
					@tintRefType = 3, --  tinyint
					@vcDescription = @description, --  varchar(100)
					@numModifiedBy = @numUserCntID,
					@numDomainID = @numDomain 
			END		
		END      
          
        IF @bitStockTransfer=1
        BEGIN
			SET @numWareHouseItemID = @numToWarehouseItemID;
			SET @OppType = 2
			SET @numUnits = @numOrigUnits

			SELECT
				@onHand = ISNULL(numOnHand, 0),
                @onAllocation = ISNULL(numAllocation, 0),
                @onOrder = ISNULL(numOnOrder, 0),
                @onBackOrder = ISNULL(numBackOrder, 0)
			FROM 
				WareHouseItems
			WHERE
				numWareHouseItemID = @numWareHouseItemID   
		END
          
        IF @OppType = 2 
        BEGIN 
			

			IF @numWLocationID = -1 AND ISNULL(@bitStockTransfer,0)=0
			BEGIN
				IF @tintmode = 1
				BEGIN
					DECLARE @TEMPReceievedItems TABLE
					(
						ID INT,
						numOIRLID NUMERIC(18,0),
						numWarehouseItemID NUMERIC(18,0),
						numUnitReceieved FLOAT
					)

					INSERT INTO
						@TEMPReceievedItems
					SELECT
						ROW_NUMBER() OVER(ORDER BY ID),
						ID,
						numWarehouseItemID,
						numUnitReceieved
					FROM
						OpportunityItemsReceievedLocation 
					WHERE
						numDomainID=@numDomain
						AND numOppID=@numOppId
						AND numOppItemID=@numoppitemtCode

					DECLARE @i AS INT = 1
					DECLARE @COUNT AS INT
					DECLARE @numTempOIRLID NUMERIC(18,0)
					DECLARE @numTempOnHand FLOAT
					DECLARE @numTempWarehouseItemID NUMERIC(18,0)
					DECLARE @numTempUnitReceieved FLOAT

					SELECT @COUNT=COUNT(*) FROM @TEMPReceievedItems

					WHILE @i <= @COUNT
					BEGIN
						SELECT 
							@numTempOIRLID=TRI.numOIRLID,
							@numTempOnHand= ISNULL(numOnHand,0),
							@numTempWarehouseItemID=ISNULL(TRI.numWarehouseItemID,0),
							@numTempUnitReceieved=ISNULL(numUnitReceieved,0)
						FROM 
							@TEMPReceievedItems TRI
						INNER JOIN
							WareHouseItems WI
						ON
							TRI.numWarehouseItemID=WI.numWareHouseItemID
						WHERE 
							ID=@i

						IF @numTempOnHand >= @numTempUnitReceieved
						BEGIN
							UPDATE
								WareHouseItems
							SET
								numOnHand = ISNULL(numOnHand,0) - ISNULL(@numTempUnitReceieved,0),
								dtModified = GETDATE()
							WHERE
								numWareHouseItemID=@numTempWarehouseItemID
						END
						ELSE
						BEGIN
							RAISERROR('INSUFFICIENT_ONHAND_QTY',16,1)
							RETURN
						END

						SET @description='PO Deleted (Total Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Deleted Qty: ' + CAST(@numTempUnitReceieved AS VARCHAR(10)) + ' Received:' +  CAST(@numUnitHourReceived AS VARCHAR(10)) + ')'

						EXEC dbo.USP_ManageWareHouseItems_Tracking
							@numWareHouseItemID = @numTempWarehouseItemID,
							@numReferenceID = @numOppId,
							@tintRefType = 4,
							@vcDescription = @description,
							@numModifiedBy = @numUserCntID,
							@numDomainID = @numDomain

						DELETE FROM OpportunityItemsReceievedLocation WHERE ID=@numTempOIRLID

						SET @i = @i + 1
					END
				END

				SET @description='PO Deleted (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Received:' +  CAST(@numUnitHourReceived AS VARCHAR(10)) + ')'

				SET @numUnits = @numUnits - @numUnitHourReceived

				IF (@onOrder - @numUnits)>=0
				BEGIN
					UPDATE
						WareHouseItems
					SET 
						numOnOrder = ISNULL(numOnOrder,0) - @numUnits,
						dtModified = GETDATE()
					WHERE 
						numWareHouseItemID = @numWareHouseItemID

					EXEC dbo.USP_ManageWareHouseItems_Tracking
						@numWareHouseItemID = @numWareHouseItemID,
						@numReferenceID = @numOppId,
						@tintRefType = 4,
						@vcDescription = @description,
						@numModifiedBy = @numUserCntID,
						@numDomainID = @numDomain
				END
				ELSE
				BEGIN
					RAISERROR('INSUFFICIENT_ONORDER_QTY',16,1)
					RETURN
				END
			END
			ELSE
			BEGIN
				--Partial Fulfillment
				IF @tintmode=1 and  @onHand >= @numUnitHourReceived
				BEGIN
					SET @onHand= @onHand - @numUnitHourReceived
				END
						
				SET @numUnits = @numUnits - @numUnitHourReceived 

				IF (@onOrder - @numUnits)>=0
				BEGIN
					--Causing Negative Inventory Bug ID:494
					SET @onOrder = @onOrder - @numUnits	
				END
				ELSE IF (@onHand + @onOrder) - @numUnits >= 0
				BEGIN						
					SET @onHand = @onHand - (@numUnits-@onOrder)
					SET @onOrder = 0
				END
				ELSE IF  (@onHand + @onOrder + @onAllocation) - @numUnits >= 0
				BEGIN
					Declare @numDiff numeric
	
					SET @numDiff = @numUnits - @onOrder
					SET @onOrder = 0

					SET @numDiff = @numDiff - @onHand
					SET @onHand = 0

					SET @onAllocation = @onAllocation - @numDiff
					SET @onBackOrder = @onBackOrder + @numDiff
				END
					    
				UPDATE
					WareHouseItems
				SET 
					numOnHand = @onHand,
					numAllocation = @onAllocation,
					numBackOrder = @onBackOrder,
					numOnOrder = @onOrder,
					dtModified = GETDATE()
				WHERE 
					numWareHouseItemID = @numWareHouseItemID   
                        
				EXEC dbo.USP_ManageWareHouseItems_Tracking
				@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
				@numReferenceID = @numOppId, --  numeric(9, 0)
				@tintRefType = 4, --  tinyint
				@vcDescription = @description, --  varchar(100)
				@numModifiedBy = @numUserCntID,
				@numDomainID = @numDomain
			END                                        
        END   
                                                                
        SELECT TOP 1
                @numoppitemtCode = numoppitemtCode,
                @itemcode = OI.numItemCode,
                @numUnits = numUnitHour,
                @numWareHouseItemID = ISNULL(numWarehouseItmsID,0),
				@numWLocationID = ISNULL(numWLocationID,0),
                @Kit = ( CASE WHEN bitKitParent = 1
                                    AND bitAssembly = 1 THEN 0
                                WHEN bitKitParent = 1 THEN 1
                                ELSE 0
                            END ),
                @monAmount = ISNULL(monTotAmount,0)  * (CASE WHEN ISNULL(fltExchangeRate,0)=0 THEN 1 ELSE fltExchangeRate END),
                @monAvgCost = monAverageCost,
                @numQtyShipped = ISNULL(numQtyShipped,0),
				@numUnitHourReceived = ISNULL(numUnitHourReceived,0),
				@bitKitParent=ISNULL(bitKitParent,0),
				@numToWarehouseItemID =OI.numToWarehouseItemID,
				@bitStockTransfer = ISNULL(OM.bitStockTransfer,0),
				@OppType = tintOppType
        FROM    OpportunityItems OI
				LEFT JOIN WareHouseItems WI ON OI.numWarehouseItmsID=WI.numWareHouseItemID
				JOIN dbo.OpportunityMaster OM ON OI.numOppId = OM.numOppId
                JOIN Item I ON OI.numItemCode = I.numItemCode
                                   
        WHERE   (charitemtype='P' OR 1=(CASE WHEN tintOppType=1 THEN 
						CASE WHEN ISNULL(I.bitKitParent,0) = 1 AND ISNULL(I.bitAssembly,0) = 1 THEN 0
								WHEN ISNULL(I.bitKitParent,0) = 1 THEN 1
								ELSE 0 END 
						ELSE 0 END))  
						AND OI.numOppId = @numOppId 
                AND OI.numoppitemtCode > @numoppitemtCode
                AND ( bitDropShip = 0
                        OR bitDropShip IS NULL
                    )
        ORDER BY OI.numoppitemtCode                                              
        IF @@rowcount = 0 
            SET @numoppitemtCode = 0      
    END

/****** Object:  StoredProcedure [dbo].[USP_ShoppingCart]    Script Date: 05/07/2009 22:09:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_shoppingcart')
DROP PROCEDURE usp_shoppingcart
GO
CREATE PROCEDURE [dbo].[USP_ShoppingCart]                            
@numDivID as numeric(18),                            
@numOppID as numeric(18)=null output ,                                              
@vcPOppName as varchar(200)='' output,  
@numContactId as numeric(18),                            
@numDomainId as numeric(18),
@vcSource VARCHAR(50)=NULL,
@numCampainID NUMERIC(9)=0,
@numCurrencyID as numeric(9),
@numBillAddressId numeric(9),
@numShipAddressId numeric(9),
@bitDiscountType BIT,
@fltDiscount FLOAT,
@numProId NUMERIC(9),
@numSiteID NUMERIC(9),
@tintOppStatus INT,
@monShipCost MONEY,
@tintSource AS BIGINT ,
@tintSourceType AS TINYINT ,                          
@numPaymentMethodId NUMERIC(9),
@txtComments varchar(1000),
@numPartner NUMERIC(18,0),
@txtFuturePassword VARCHAR(50) = NULL,
@intUsedShippingCompany NUMERIC(18,0)=0
AS                            
	DECLARE @CRMType AS INTEGER               
	DECLARE @numRecOwner AS INTEGER                            
	
	IF @numOppID=  0                             
	BEGIN                            
		SELECT @CRMType=tintCRMType,@numRecOwner=numRecOwner from DivisionMaster where numDivisionID=@numDivID                            
	
		IF @CRMType= 0                             
		BEGIN                            
			UPDATE DivisionMaster SET tintCRMType=1 WHERE numDivisionID=@numDivID                                        
		END                            
		
		DECLARE @TotalAmount as FLOAT

     

		IF LEN(ISNULL(@txtFuturePassword,'')) > 0
		BEGIN
			DECLARE @numExtranetID NUMERIC(18,0)

			SELECT @numExtranetID=numExtranetID FROM ExtarnetAccounts WHERE numDivisionID=@numDivID

			UPDATE ExtranetAccountsDtl SET vcPassword=@txtFuturePassword WHERE numExtranetID=@numExtranetID
		END                      

		DECLARE @numTemplateID NUMERIC
		SELECT  @numTemplateID= numMirrorBizDocTemplateId FROM eCommercePaymentConfig WHERE  numSiteId=@numSiteID AND numPaymentMethodId = @numPaymentMethodId AND numDomainID = @numDomainID   

		Declare @fltExchangeRate float                                 
    
		IF @numCurrencyID=0 
			SELECT @numCurrencyID=ISNULL(numCurrencyID,0) FROM Domain WHERE numDomainID=@numDomainId
			                       
		IF @numCurrencyID=0 
			SET @fltExchangeRate=1
		ELSE 
			SET @fltExchangeRate=dbo.GetExchangeRate(@numDomainID,@numCurrencyID)                          
                            
  
		-- GET ACCOUNT CLASS IF ENABLED
		DECLARE @numAccountClass AS NUMERIC(18) = 0

		DECLARE @tintDefaultClassType AS INT = 0
		SELECT @tintDefaultClassType = ISNULL(tintDefaultClassType,0) FROM Domain WHERE numDomainId = @numDomainID 

		IF @tintDefaultClassType = 1 --USER
		BEGIN
			SELECT 
				@numAccountClass=ISNULL(numDefaultClass,0) 
			FROM 
				dbo.UserMaster UM 
			JOIN 
				dbo.Domain D 
			ON 
				UM.numDomainID=D.numDomainId
			WHERE 
				D.numDomainId=@numDomainId 
				AND UM.numUserDetailId=@numContactId
		END
		ELSE IF @tintDefaultClassType = 2 --COMPANY
		BEGIN
			SELECT 
				@numAccountClass=ISNULL(numAccountClassID,0) 
			FROM 
				dbo.DivisionMaster DM 
			WHERE 
				DM.numDomainId=@numDomainId 
				AND DM.numDivisionID=@numDivID
		END
		ELSE
		BEGIN
			SET @numAccountClass = 0
		END

		SET @numAccountClass=(SELECT 
								  TOP 1 
								  numDefaultClass from dbo.eCommerceDTL 
								  where 
								  numSiteId=@numSiteID 
								  and numDomainId=@numDomainId)                  

		insert into OpportunityMaster                              
		  (                              
		  numContactId,                              
		  numDivisionId,                              
		  txtComments,                              
		  numCampainID,                              
		  bitPublicFlag,                              
		  tintSource, 
		  tintSourceType ,                               
		  vcPOppName,                              
		  monPAmount,                              
		  numCreatedBy,                              
		  bintCreatedDate,                               
		  numModifiedBy,                              
		  bintModifiedDate,                              
		  numDomainId,                               
		  tintOppType, 
		  tintOppStatus,                   
		  intPEstimatedCloseDate,              
		  numRecOwner,
		  bitOrder,
		  numCurrencyID,
		  fltExchangeRate,fltDiscountTotal,bitDiscountTypeTotal,
		  monShipCost,
		  numOppBizDocTempID,numAccountClass,numPartner,intUsedShippingCompany
		  )                              
		 Values                              
		  (                              
		  @numContactId,                              
		  @numDivID,                              
		  @txtComments,                              
		  @numCampainID,--  0,
		  0,                              
		  @tintSource,   
		  @tintSourceType,                           
		  ISNULL(@vcPOppName,'SO'),                             
		  0,                                
		  @numContactId,                              
		  getutcdate(),                              
		  @numContactId,                              
		  getutcdate(),        
		  @numDomainId,                              
		  1,             
		  @tintOppStatus,       
		  getutcdate(),                                 
		  @numRecOwner ,
		  1,
		  @numCurrencyID,
		  @fltExchangeRate,@fltDiscount,@bitDiscountType
		  ,@monShipCost,
		  @numTemplateID,@numAccountClass,@numPartner,@intUsedShippingCompany
  
		  )        
		                         
		SET @numOppID=scope_identity()                             

		EXEC dbo.USP_UpdateNameTemplateValue 1,@numDomainId,@numOppID
		SELECT @vcPOppName = vcPOppName FROM OpportunityMaster WHERE numOppID= @numOppID
	END

	DELETE FROM OpportunityItems WHERE numOppId= @numOppID                               

	UPDATE 
		P
	SET
		intCouponCodeUsed=ISNULL(intCouponCodeUsed,0)+1
	FROM 
		PromotionOffer AS P
	LEFT JOIN
		CartItems AS C
	ON
		P.numProId=C.PromotionID
	WHERE
		C.vcCoupon=P.txtCouponCode 
		AND P.numProId = C.PromotionID
		AND ISNULL(C.bitParentPromotion,0)=1
		AND numUserCntId =@numContactId


	INSERT INTO OpportunityItems                                                        
	(
		numOppId
		,numItemCode
		,numUnitHour
		,monPrice
		,monTotAmount
		,vcItemDesc
		,numWarehouseItmsID
		,vcType
		,vcAttributes
		,vcAttrValues
		,[vcItemName]
		,[vcModelID]
		,[vcManufacturer]
		,vcPathForTImage
		,monVendorCost
		,numUOMId
		,bitDiscountType
		,fltDiscount
		,monTotAmtBefDiscount
		,numPromotionID
		,bitPromotionTriggered
		,vcPromotionDetail
	)                                                        
	SELECT 
		@numOppID
		,X.numItemCode
		,x.numUnitHour * x.decUOMConversionFactor
		,x.monPrice/x.decUOMConversionFactor
		,x.monTotAmount
		,X.vcItemDesc
		,NULLIF(X.numWarehouseItmsID,0)
		,X.vcItemType
		,X.vcAttributes
		,X.vcAttrValues
		,(SELECT vcItemName FROM item WHERE numItemCode = X.numItemCode)
		,(SELECT vcModelID FROM item WHERE numItemCode = X.numItemCode)
		,(SELECT vcManufacturer FROM item WHERE numItemCode = X.numItemCode)
		,(SELECT TOP 1  vcPathForTImage FROM dbo.ItemImages WHERE numItemCode = X.numItemCode AND bitDefault = 1 AND numDomainId = @numDomainId)
		,(SELECT ISNULL(VN.monCost,0) FROM Vendor VN, Item IT WHERE IT.numItemCode=VN.numItemCode AND VN.numItemCode=X.numItemCode AND VN.numVendorID=IT.numVendorID)
		,numUOM
		,bitDiscountType
		,fltDiscount
		,monTotAmtBefDiscount
		,X.PromotionID
		,X.bitParentPromotion
		,x.PromotionDesc
	FROM 
		dbo.CartItems X 
	WHERE 
		numUserCntId =@numContactId

	declare @tintShipped as tinyint      
	DECLARE @tintOppType AS TINYINT
      
	select @tintOppStatus=tintOppStatus,@tintOppType=tintOppType,@tintShipped=tintShipped from OpportunityMaster where numOppID=@numOppID              
	if @tintOppStatus=1               
	begin         
	PRINT 'inside USP_UpdatingInventoryonCloseDeal'
		exec USP_UpdatingInventoryonCloseDeal @numOppID,@numContactId
	end              

                  
	select @TotalAmount=sum(monTotAmount)from OpportunityItems where numOppId=@numOppID                            
	update OpportunityMaster set  monPamount=@TotalAmount where numOppId=@numOppID                           
                          
	IF(@vcSource IS NOT NULL)
	BEGIN
		insert into OpportunityLinking ([numParentOppID],[numChildOppID],[vcSource],[numParentOppBizDocID],numPromotionId,numSiteID)  values(null,@numOppID,@vcSource,NULL,@numProId,@numSiteID);
	END

	--Add/Update Address Details
	DECLARE @vcStreet  varchar(100),@vcCity  varchar(50),@vcPostalCode  varchar(15),@vcCompanyName  varchar(100),@vcAddressName VARCHAR(50)      
	DECLARE @numState  numeric(9),@numCountry  numeric(9), @numCompanyId  numeric(9) 
	DECLARE @bitIsPrimary BIT;

	--Bill Address
	IF @numBillAddressId>0
	BEGIN
	 SELECT  @vcStreet=ISNULL(vcStreet,''),@vcCity=ISNULL(vcCity,''),@vcPostalCode=ISNULL(vcPostalCode,''),
			@numState=ISNULL(numState,0),@numCountry=ISNULL(numCountry,0),@bitIsPrimary=bitIsPrimary,
			@vcAddressName=vcAddressName
				FROM  dbo.AddressDetails WHERE   numDomainID = @numDomainID AND numAddressID = @numBillAddressId 
            
  
  		EXEC dbo.USP_UpdateOppAddress
		@numOppID = @numOppID, --  numeric(9, 0)
		@byteMode = 0, --  tinyint
		@vcStreet = @vcStreet, --  varchar(100)
		@vcCity = @vcCity, --  varchar(50)
		@vcPostalCode = @vcPostalCode, --  varchar(15)
		@numState = @numState, --  numeric(9, 0)
		@numCountry = @numCountry, --  numeric(9, 0)
		@vcCompanyName = '', --  varchar(100)
		@numCompanyId = 0, --  numeric(9, 0)
		@vcAddressName =@vcAddressName
	
        
	END
  
  
	--Ship Address
	IF @numShipAddressId>0
	BEGIN
   		SELECT  @vcStreet=ISNULL(vcStreet,''),@vcCity=ISNULL(vcCity,''),@vcPostalCode=ISNULL(vcPostalCode,''),
			@numState=ISNULL(numState,0),@numCountry=ISNULL(numCountry,0),@bitIsPrimary=bitIsPrimary,
			@vcAddressName=vcAddressName
				FROM  dbo.AddressDetails WHERE   numDomainID = @numDomainID AND numAddressID = @numShipAddressId 
  
  
		select  @vcCompanyName=vcCompanyName,@numCompanyId=div.numCompanyID from CompanyInfo Com                            
		join divisionMaster Div                            
		on div.numCompanyID=com.numCompanyID                            
		where div.numdivisionID=@numDivID

  		EXEC dbo.USP_UpdateOppAddress
		@numOppID = @numOppID, --  numeric(9, 0)
		@byteMode = 1, --  tinyint
		@vcStreet = @vcStreet, --  varchar(100)
		@vcCity = @vcCity, --  varchar(50)
		@vcPostalCode = @vcPostalCode, --  varchar(15)
		@numState = @numState, --  numeric(9, 0)
		@numCountry = @numCountry, --  numeric(9, 0)
		@vcCompanyName = @vcCompanyName, --  varchar(100)
		@numCompanyId =@numCompanyId, --  numeric(9, 0)
		@vcAddressName =@vcAddressName
	
   	
	END

	--Insert Tax for Division                       
	INSERT dbo.OpportunityMasterTaxItems 
	(
		numOppId,
		numTaxItemID,
		fltPercentage,
		tintTaxType,
		numTaxID
	) 
	SELECT 
		@numOppID,
		TI.numTaxItemID,
		TEMPTax.decTaxValue,
		TEMPTax.tintTaxType,
		numTaxID
	FROM 
		TaxItems TI 
	JOIN 
		DivisionTaxTypes DTT 
	ON 
		TI.numTaxItemID = DTT.numTaxItemID
	OUTER APPLY
	(
		SELECT
			decTaxValue,
			tintTaxType,
			numTaxID
		FROM
			dbo.fn_CalItemTaxAmt(@numDomainID,@numDivID,0,TI.numTaxItemID,@numOppId,0,NULL)
	) AS TEMPTax
	WHERE 
		DTT.numDivisionID=@numDivID AND DTT.bitApplicable=1
	UNION 
	SELECT 
		@numOppID,
		0,
		TEMPTax.decTaxValue,
		TEMPTax.tintTaxType,
		numTaxID
	FROM 
		dbo.DivisionMaster 
	OUTER APPLY
	(
		SELECT
			decTaxValue,
			tintTaxType,
			numTaxID
		FROM
			dbo.fn_CalItemTaxAmt(@numDomainID,@numDivID,0,0,@numOppId,1,NULL)
	) AS TEMPTax
	WHERE 
		bitNoTax=0 
		AND numDivisionID=@numDivID

	-- DELETE ITEM LEVEL CRV TAX TYPES
	DELETE FROM OpportunityMasterTaxItems WHERE numOppId=@numOppId AND numTaxItemID = 1

	-- INSERT ITEM LEVEL CRV TAX TYPES
	INSERT INTO OpportunityMasterTaxItems
	(
		numOppId,
		numTaxItemID,
		fltPercentage,
		tintTaxType,
		numTaxID
	) 
	SELECT 
		OI.numOppId,
		1,
		TEMPTax.decTaxValue,
		TEMPTax.tintTaxType,
		TEMPTax.numTaxID
	FROM 
		OpportunityItems OI
	INNER JOIN
		ItemTax IT
	ON
		IT.numItemCode = OI.numItemCode
	OUTER APPLY
	(
		SELECT
			decTaxValue,
			tintTaxType,
			numTaxID
		FROM
			dbo.fn_CalItemTaxAmt(@numDomainID,@numDivID,OI.numItemCode,IT.numTaxItemID,@numOppId,1,NULL)
	) AS TEMPTax
	WHERE
		OI.numOppId = @numOppID
		AND IT.numTaxItemID = 1 -- CRV TAX
	GROUP BY
		OI.numOppId,
		TEMPTax.decTaxValue,
		TEMPTax.tintTaxType,
		TEMPTax.numTaxID
  
	--Delete Tax for Opportunity Items if item deleted 
	DELETE FROM OpportunityItemsTaxItems WHERE numOppId=@numOppId AND numOppItemID NOT IN (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID)

	--Insert Tax for Opportunity Items
	INSERT INTO dbo.OpportunityItemsTaxItems 
	(
		numOppId,
		numOppItemID,
		numTaxItemID,
		numTaxID
	) 
	SELECT 
		@numOppId,
		OI.numoppitemtCode,
		TI.numTaxItemID,
		0 
	FROM 
		dbo.OpportunityItems OI 
	JOIN 
		dbo.ItemTax IT 
	ON 
		OI.numItemCode=IT.numItemCode 
	JOIN
		TaxItems TI 
	ON 
		TI.numTaxItemID=IT.numTaxItemID 
	WHERE 
		OI.numOppId=@numOppID 
		AND IT.bitApplicable=1 
		AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
	UNION
	SELECT 
		@numOppId,
		OI.numoppitemtCode,
		0,
		0 
	FROM 
		dbo.OpportunityItems OI 
	JOIN 
		dbo.Item I 
	ON 
		OI.numItemCode=I.numItemCode
	WHERE 
		OI.numOppId=@numOppID 
		AND I.bitTaxable=1 
		AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
	UNION
	SELECT
		@numOppId,
		OI.numoppitemtCode,
		1,
		TD.numTaxID
	FROM
		dbo.OpportunityItems OI 
	INNER JOIN
		ItemTax IT
	ON
		IT.numItemCode = OI.numItemCode
	INNER JOIN
		TaxDetails TD
	ON
		TD.numTaxID = IT.numTaxID
		AND TD.numDomainId = @numDomainId
	WHERE
		OI.numOppId = @numOppID
		AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
  
 
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Sites_GetCategoties')
DROP PROCEDURE USP_Sites_GetCategoties
GO
CREATE PROCEDURE [dbo].[USP_Sites_GetCategoties] 
	@numDomainID NUMERIC(18,0),
	@numSiteID NUMERIC(18,0)
AS  
BEGIN
	DECLARE @numCategoryProfileID NUMERIC(18,0)

	SELECT  
		@numCategoryProfileID=CPS.numCategoryProfileID
	FROM 
		Sites S
	JOIN
		CategoryProfileSites CPS
	ON
		S.numSiteID=CPS.numSiteID
	JOIN
		CategoryProfile CP
	ON
		CPS.numCategoryProfileID = CP.ID
	WHERE 
		S.numSiteID=@numSiteID

	;WITH CategoryList (numCategoryID,numParentCategoryID,vcCategoryName,tintLevel,intDisplayOrder,vcDescription)
	As 
	( 
		SELECT 
			C.numCategoryID,
			ISNULL(C.numDepCategory,0),
			ISNULL(C.vcCategoryName,''),
			0,
			ISNULL(C.intDisplayOrder,0),
			C.vcDescription
		FROM 
			[SiteCategories] SC
		INNER JOIN 
			[Category] C 
		ON 
			SC.[numCategoryID] = C.[numCategoryID]
		WHERE
			SC.numSiteID=@numSiteID
			AND C.numCategoryProfileID=@numCategoryProfileID
			AND C.numDomainID=@numDomainID
			AND ISNULL(numDepCategory,0) = 0
	 UNION ALL
	 SELECT 
			C.numCategoryID,
			ISNULL(C.numDepCategory,0),
			ISNULL(C.vcCategoryName,''),
			ISNULL(CL.tintLevel + 1,0),
			ISNULL(C.intDisplayOrder,0),
			C.vcDescription
		FROM 
			[SiteCategories] SC
		INNER JOIN 
			[Category] C 
		ON 
			SC.[numCategoryID] = C.[numCategoryID]
		INNER JOIN
			CategoryList CL
		ON
			C.numDepCategory = CL.numCategoryID
		WHERE
			SC.numSiteID=@numSiteID
			AND C.numCategoryProfileID=@numCategoryProfileID
			AND C.numDomainID=@numDomainID
	)

	SELECT * FROM CategoryList ORDER BY tintLevel,intDisplayOrder
END

/****** Object:  StoredProcedure [dbo].[USP_UpdateCartItem]    Script Date: 11/08/2011 17:49:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_UpdateCartItem' ) 
                    DROP PROCEDURE USP_UpdateCartItem
                    Go
CREATE  PROCEDURE [dbo].[USP_UpdateCartItem]
(
	@numUserCntId numeric(9,0),
	@numDomainId numeric(9,0),
	@vcCookieId varchar(100),
	@strXML text,
	@postselldiscount INT=0,
	@numSiteID NUMERIC(18,0)
)
AS
BEGIN
	IF convert(varchar(10),@strXML) <>''
	BEGIN
		DECLARE @i INT
		EXEC sp_xml_preparedocument @i OUTPUT, @strXML 
		
		IF @numUserCntId <> 0
		BEGIN
			UPDATE 
				CartItems
			SET 
				CartItems.monPrice = ox.monPrice
				,CartItems.numUnitHour = ox.numUnitHour
				,CartItems.monTotAmtBefDiscount = ox.monTotAmtBefDiscount
				,CartItems.monTotAmount = ox.monTotAmount
				,CartItems.bitDiscountType=ISNULL(ox.bitDiscountType,0)
				,CartItems.vcShippingMethod=ox.vcShippingMethod
				,CartItems.numServiceTypeID=ox.numServiceTypeID
				,CartItems.decShippingCharge=ox.decShippingCharge
				,CartItems.tintServiceType=ox.tintServiceType
				,CartItems.numShippingCompany =ox.numShippingCompany
				,CartItems.dtDeliveryDate =ox.dtDeliveryDate
				,CartItems.fltDiscount =ox.fltDiscount
				,CartItems.vcCookieId = ox.vcCookieId
				,CartItems.vcCoupon=(CASE WHEN CartItems.vcCoupon<>'' THEN CartItems.vcCoupon ELSE ox.vcCoupon END)
			FROM 
				OPENXML(@i, '/NewDataSet/Table1',2)
			WITH 
			(
				numDomainId NUMERIC(9,0),numUserCntId NUMERIC(9,0),vcCookieId VARCHAR(200),numItemCode NUMERIC(9,0), monPrice money, numUnitHour FLOAT,
				monTotAmtBefDiscount MONEY,monTotAmount MONEY ,bitDiscountType BIT,vcShippingMethod VARCHAR(200),numServiceTypeID NUMERIC(9,0),decShippingCharge money,
				tintServiceType TINYINT,numShippingCompany NUMERIC(9,0),dtDeliveryDate DATETIME,fltDiscount DECIMAL(18,2),vcCoupon VARCHAR(200) 
			) ox
			WHERE 
				CartItems.numDomainId = ox.numDomainId
				AND CartItems.numUserCntId = ox.numUserCntId
				AND CartItems.numItemCode = ox.numItemCode
		END 
		ELSE
		BEGIN
			UPDATE 
				CartItems
			SET 
				CartItems.monPrice = ox.monPrice
				,CartItems.numUnitHour = ox.numUnitHour
				,CartItems.monTotAmtBefDiscount = ox.monTotAmtBefDiscount
				,CartItems.monTotAmount = ox.monTotAmount
				,CartItems.bitDiscountType =ISNULL(ox.bitDiscountType,0)
				,CartItems.vcShippingMethod =ox.vcShippingMethod
				,CartItems.numServiceTypeID =ox.numServiceTypeID
				,CartItems.decShippingCharge =ox.decShippingCharge
				,CartItems.tintServiceType =ox.tintServiceType
				,CartItems.numShippingCompany =ox.numShippingCompany
				,CartItems.dtDeliveryDate =ox.dtDeliveryDate
				,CartItems.fltDiscount =ox.fltDiscount
				,CartItems.vcCoupon=(CASE WHEN CartItems.vcCoupon<>'' THEN CartItems.vcCoupon ELSE ox.vcCoupon END)
			FROM 
				OpenXml(@i, '/NewDataSet/Table1',2)
			WITH 
			(
				numDomainId NUMERIC(9,0),numUserCntId NUMERIC(9,0),vcCookieId VARCHAR(200),numItemCode NUMERIC(9,0), monPrice money, numUnitHour FLOAT,
				monTotAmtBefDiscount MONEY,monTotAmount MONEY ,bitDiscountType BIT,vcShippingMethod VARCHAR(200),numServiceTypeID NUMERIC(9,0),decShippingCharge MONEY,
				tintServiceType TINYINT,numShippingCompany NUMERIC(9,0),dtDeliveryDate DATETIME,fltDiscount DECIMAL(18,2),vcCoupon VARCHAR(200) 
			) ox
			WHERE 
				CartItems.numDomainId = ox.numDomainId 
				AND CartItems.numUserCntId = 0 
				AND CartItems.numItemCode = ox.numItemCode
				AND CartItems.vcCookieId = ox.vcCookieId
		END

		exec sp_xml_removedocument @i

		EXEC USP_ManageECommercePromotion @numUserCntId,@numDomainId,@vcCookieId,@postselldiscount,@numSiteID


		UPDATE
			CI
		SET
			CI.fltDiscount=0,
			CI.bitDiscountType=0,
			CI.monTotAmount=CI.numUnitHour*CI.monPrice,
			CI.PromotionID=0,
			CI.PromotionDesc='',
			CI.vcCoupon=NULL
		FROM
			CartItems CI
		WHERE
			CI.numDomainId = @numDomainId
			AND CI.numUserCntId = @numUserCntId
			AND CI.vcCookieId=@vcCookieId
			AND ISNULL(PromotionID,0) > 0
			AND PromotionDesc NOT LIKE '%Post sell Discount%'
			AND (SELECT COUNT(*) FROM CartItems WHERE numDomainId=@numDomainId AND numUserCntId=@numUserCntId AND vcCookieId=@vcCookieId AND PromotionID=CI.PromotionID AND bitParentPromotion=1) = 0
	END
END
--Created by Anoop Jayaraj

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_UpdatePOItemsForFulfillment')
DROP PROCEDURE USP_UpdatePOItemsForFulfillment
GO
CREATE PROCEDURE  USP_UpdatePOItemsForFulfillment
    @numQtyReceived FLOAT,
    @numOppItemID NUMERIC(9),
    @vcError VARCHAR(200) = '' OUTPUT,
    @numUserCntID AS NUMERIC(9),
    @dtItemReceivedDate AS DATETIME,
	@numSelectedWarehouseItemID AS NUMERIC(18,0) = 0
AS 
BEGIN
--Transaction is invoked from ADO.net (Purchase Fulfillment) Do not add transaction code here.
-- BEGIN TRY
-- BEGIN TRANSACTION
	DECLARE @numDomain AS NUMERIC(18,0)
	          
	DECLARE @numWarehouseItemID AS NUMERIC
	DECLARE @numWLocationID AS NUMERIC(18,0)     
	DECLARE @numOldQtyReceived AS FLOAT       
	DECLARE @numNewQtyReceived AS FLOAT
	              
	DECLARE @bitStockTransfer BIT
	DECLARE @numToWarehouseItemID NUMERIC
	DECLARE @numOppId NUMERIC
	DECLARE @numUnits FLOAT
	DECLARE @monPrice AS MONEY 
	DECLARE @numItemCode NUMERIC 
		
	SELECT  
		@numWarehouseItemID = ISNULL([numWarehouseItmsID], 0),
		@numWLocationID = ISNULL(WI.numWLocationID,0),
		@numOldQtyReceived = ISNULL([numUnitHourReceived], 0),
		@bitStockTransfer = ISNULL(OM.bitStockTransfer,0),
		@numToWarehouseItemID= ISNULL(numToWarehouseItemID,0),
		@numOppId=OM.numOppId,
		@numUnits=OI.numUnitHour,
		@monPrice=isnull(monPrice,0) * (CASE WHEN ISNULL(OM.fltExchangeRate,0)=0 THEN 1 ELSE OM.fltExchangeRate END),
		@numItemCode=OI.numItemCode,
		@numDomain = OM.[numDomainId]
	FROM 
		[OpportunityItems] OI 
	INNER JOIN 
		dbo.OpportunityMaster OM
	ON 
		OI.numOppId = OM.numOppId
	LEFT JOIN
		WareHouseItems WI
	ON
		OI.numWarehouseItmsID = WI.numWareHouseItemID
	WHERE 
		[numoppitemtCode] = @numOppItemID
    

	DECLARE @numTotalQuantityReceived FLOAT
	SET @numTotalQuantityReceived = @numQtyReceived + @numOldQtyReceived ;
	SET @numNewQtyReceived = @numQtyReceived;
	DECLARE @description AS VARCHAR(100)
	SET @description='PO Qty Received (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Received:' +  CAST(@numTotalQuantityReceived AS VARCHAR(10)) + ')'
			

	IF @numNewQtyReceived <= 0 
		RETURN 
  
	
			
	DECLARE @TotalOnHand AS FLOAT;SET @TotalOnHand=0  
	SELECT @TotalOnHand=(SUM(ISNULL(numOnHand,0)) + SUM(ISNULL(numAllocation,0))) FROM dbo.WareHouseItems WHERE numItemID=@numItemCode 
						
	--Updating the Average Cost
	DECLARE @monAvgCost AS MONEY 
	SELECT  @monAvgCost = (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(monAverageCost, 0) END) FROM Item WHERE   numitemcode = @numItemCode  
    
  	SET @monAvgCost = ((@TotalOnHand * @monAvgCost) + (@numNewQtyReceived * @monPrice)) / ( @TotalOnHand + @numNewQtyReceived )
                            
	UPDATE  
		item
	SET 
		monAverageCost = (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE @monAvgCost END)
	WHERE 
		numItemCode = @numItemCode
    

	

	IF @numWLocationID = -1 AND ISNULL(@numSelectedWarehouseItemID,0) > 0		
	BEGIN
		-- FIRST DECRESE OnOrder OF GLOBAL LOCATION
		UPDATE
			WareHouseItems
		SET 
			numOnOrder = ISNULL(numOnOrder,0) - @numNewQtyReceived,
			dtModified = GETDATE()
		WHERE
			numWareHouseItemID=@numWareHouseItemID
		
		SET @description = CONCAT('PO Qty Received To Internal Location From Global Location (Qty:',@numUnits,' Received:',@numNewQtyReceived,' Total Received:',@numTotalQuantityReceived,')')

		EXEC dbo.USP_ManageWareHouseItems_Tracking
			@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
			@numReferenceID = @numOppId, --  numeric(9, 0)
			@tintRefType = 4, --  tinyint
			@vcDescription = @description,
			@numModifiedBy = @numUserCntID,
			@dtRecordDate =  @dtItemReceivedDate,
			@numDomainID = @numDomain

		-- INCREASE THE OnHand Of Destination Location
		UPDATE
			WareHouseItems
		SET
			numBackOrder = (CASE WHEN numBackOrder >= @numNewQtyReceived THEN ISNULL(numBackOrder,0) - @numNewQtyReceived ELSE 0 END),         
			numAllocation = (CASE WHEN numBackOrder >= @numNewQtyReceived THEN ISNULL(numAllocation,0) + @numNewQtyReceived ELSE ISNULL(numAllocation,0) + ISNULL(numBackOrder,0) END),
			numOnHand = (CASE WHEN numBackOrder >= @numNewQtyReceived THEN numOnHand ELSE ISNULL(numOnHand,0) + @numNewQtyReceived - ISNULL(numBackOrder,0) END),
			dtModified = GETDATE()
		WHERE
			numWareHouseItemID=@numSelectedWarehouseItemID

		SET @description = CONCAT('PO Qty Received From Global Location (Qty:',@numUnits,' Received:',@numNewQtyReceived,' Total Received:',@numTotalQuantityReceived,')')
		EXEC dbo.USP_ManageWareHouseItems_Tracking
			@numWareHouseItemID = @numSelectedWarehouseItemID, --  numeric(9, 0)
			@numReferenceID = @numOppId, --  numeric(9, 0)
			@tintRefType = 4, --  tinyint
			@vcDescription = @description,
			@numModifiedBy = @numUserCntID,
			@dtRecordDate =  @dtItemReceivedDate,
			@numDomainID = @numDomain

		INSERT INTO OpportunityItemsReceievedLocation
		(
			numDomainID,
			numOppID,
			numOppItemID,
			numWarehouseItemID,
			numUnitReceieved
		)
		VALUES
		(
			@numDomain,
			@numOppId,
			@numOppItemID,
			@numSelectedWarehouseItemID,
			@numNewQtyReceived
		)
	END
	ELSE 
	BEGIN
		DECLARE @onHand AS FLOAT
		DECLARE @onAllocation AS FLOAT    
		DECLARE @onOrder AS FLOAT            
		DECLARE @onBackOrder AS FLOAT

		SELECT  
			@onHand = ISNULL(numOnHand, 0),
			@onAllocation = ISNULL(numAllocation, 0),
			@onOrder = ISNULL(numOnOrder, 0),
			@onBackOrder = ISNULL(numBackOrder, 0)
		FROM 
			WareHouseItems
		WHERE 
			numWareHouseItemID = @numWareHouseItemID

		IF @onOrder >= @numNewQtyReceived 
		BEGIN
			PRINT '1 case'

			SET @onOrder = @onOrder - @numNewQtyReceived             
			
			IF @onBackOrder >= @numNewQtyReceived 
			BEGIN            
				SET @onBackOrder = @onBackOrder - @numNewQtyReceived             
				SET @onAllocation = @onAllocation + @numNewQtyReceived             
			END            
			ELSE 
			BEGIN            
				SET @onAllocation = @onAllocation + @onBackOrder            
				SET @numNewQtyReceived = @numNewQtyReceived - @onBackOrder            
				SET @onBackOrder = 0            
				SET @onHand = @onHand + @numNewQtyReceived             
			END         
		END            
		ELSE IF @onOrder < @numNewQtyReceived 
		BEGIN
			PRINT '2 case'        
			SET @onHand = @onHand + @onOrder
			SET @onOrder = @numNewQtyReceived - @onOrder
		END   

		SELECT 
			@onHand onHand,
			@onAllocation onAllocation,
			@onBackOrder onBackOrder,
			@onOrder onOrder
                
		UPDATE 
			WareHouseItems
		SET     
			numOnHand = @onHand,
			numAllocation = @onAllocation,
			numBackOrder = @onBackOrder,
			numOnOrder = @onOrder,
			dtModified = GETDATE() 
		WHERE 
			numWareHouseItemID = @numWareHouseItemID
    
		EXEC dbo.USP_ManageWareHouseItems_Tracking
			@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
			@numReferenceID = @numOppId, --  numeric(9, 0)
			@tintRefType = 4, --  tinyint
			@vcDescription = @description, --  varchar(100)
			@numModifiedBy = @numUserCntID,
			@dtRecordDate =  @dtItemReceivedDate,
			@numDomainID = @numDomain
	END
 
	UPDATE  
		[OpportunityItems]
	SET 
		numUnitHourReceived = @numTotalQuantityReceived
	WHERE
		[numoppitemtCode] = @numOppItemID

	UPDATE dbo.OpportunityMaster SET dtItemReceivedDate=@dtItemReceivedDate WHERE numOppId=@numOppId
END	
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_ValidateSerializLot_Name')
DROP PROCEDURE usp_ValidateSerializLot_Name
GO
CREATE PROCEDURE [dbo].[usp_ValidateSerializLot_Name]
(
    @numItemCode as numeric(9)=0,
    @numWarehouseItmsID as numeric(9)=0,
    @strItems as VARCHAR(3000)='',
    @bitSerialized as bit,
    @bitLotNo as bit
)
AS 
    BEGIN
 
Create table #tempTable 
(                                                                    
	vcSerialNo VARCHAR(100),
	TotalQty [numeric](18,0),
	numWarehouseItmsDTLID [numeric](18, 0)                                             
)                       

INSERT INTO #tempTable 
(
	vcSerialNo,TotalQty,numWarehouseItmsDTLID
)  
SELECT 
	vcSerialNo,numWareHouseItmsDTLID,ISNULL(numQty,0) AS TotalQty
FROM   
	WareHouseItmsDTL   
WHERE 
	numWareHouseItemID=@numWarehouseItmsID  
ORDER BY 
	vcSerialNo,TotalQty desc

Create table #tempError 
(                                                                    
	vcSerialNo VARCHAR(100),
	UsedQty [numeric](18, 0),
	AvailableQty [numeric](18, 0),
	vcError VARCHAR(100)                                             
)                       

  
DECLARE @posComma int, @strKeyVal varchar(20)

SET @strItems=RTRIM(@strItems)
IF RIGHT(@strItems, 1)!=',' SET @strItems=@strItems+','

SET @posComma=PatIndex('%,%', @strItems)
WHILE @posComma>1
BEGIN
	SET @strKeyVal=ltrim(rtrim(substring(@strItems, 1, @posComma-1)))
	
	DECLARE @posBStart INT,@posBEnd int, @strQty varchar(20),@strName varchar(20)

    IF @bitLotNo=1 
    BEGIN
		SET @posBStart=PatIndex('%(%', @strKeyVal)
		SET @posBEnd=PatIndex('%)%', @strKeyVal)
		IF( @posBStart>1 AND @posBEnd>1)
		BEGIN
			SET @strName=LTRIM(RTRIM(SUBSTRING(@strKeyVal, 1, @posBStart-1)))
			SET	@strQty=LTRIM(RTRIM(SUBSTRING(@strKeyVal, @posBStart+1,len(@strKeyVal)-@posBStart-1)))
		END
		ELSE
		BEGIN
			SET @strName=@strKeyVal
			SET	@strQty=1
		END
	END
	ELSE
	BEGIN
		SET @strName=@strKeyVal
		SET	@strQty=1
	END  
	  
	DECLARE @AvailableQty NUMERIC(9)
	  
	 IF NOT EXISTS(SELECT 1 FROM #tempTable WHERE vcSerialNo=@strName)
	 BEGIN
		 INSERT INTO #tempError (vcSerialNo,UsedQty,AvailableQty,vcError) VALUES (@strName,@strQty,0,'Serial / Lot # not available')
	END
	 ELSE
	 BEGIN 
	    SELECT TOP 1 @AvailableQty=TotalQty FROM #tempTable WHERE vcSerialNo=@strName 
	    
	    IF  (@strQty>@AvailableQty)	 
		 INSERT INTO #tempError (vcSerialNo,UsedQty,AvailableQty,vcError) VALUES (@strName,@strQty,@AvailableQty,'Used Serial / Lot # more than Available Serial / Lot #')
	  END
	  
	SET @strItems=substring(@strItems, @posComma +1, len(@strItems)-@posComma)
	SET @posComma=PatIndex('%,%',@strItems)
	
END
--SELECT * FROM #tempTable

SELECT * FROM #tempError
DROP TABLE #tempError
DROP TABLE #tempTable

END
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_WarehouseItems_InventoryTransfer' ) 
    DROP PROCEDURE USP_WarehouseItems_InventoryTransfer
GO
CREATE PROCEDURE [dbo].[USP_WarehouseItems_InventoryTransfer]
@numDomainID NUMERIC(18,0),
@numUserCntID NUMERIC(18,0),
@numFromWarehouseItemID NUMERIC(18,0),
@numToWarehouseItemID	NUMERIC(18,0),
@numQty NUMERIC(18,0),
@strItems as VARCHAR(3000)=''
AS
BEGIN
BEGIN TRY
BEGIN TRANSACTION
	DECLARE @numOnHand FLOAT
	DECLARE @bitSerial BIT
	DECLARE @bitLot BIT
	DECLARE @numItemCode AS NUMERIC(18,0)

	SELECT @numOnHand=numOnHand,@numItemCode=numItemID FROM WarehouseItems WHERE numDomainID=@numDomainID AND numWareHouseItemID=@numFromWarehouseItemID 

	IF ISNULL(@numOnHand,0) < @numQty
	BEGIN
		RAISERROR('INSUFFICIENT_ONHAND_QUANTITY',16,1)
	END
	ELSE 
	BEGIN
		-- IF SERIAL/LOT ITEM THAN CHECK SERIAL/LOT NUMBER ARE VALID AND ITS QTY ARE VALID
		SELECT @bitSerial=bitSerialized,@bitLot=bitLotNo FROM Item WHERE numDomainID=@numDomainID AND numItemCode=@numItemCode
		
		IF @bitSerial = 1 OR @bitLot = 1
		BEGIN
			DECLARE @posComma int, @strKeyVal varchar(20)

			SET @strItems=RTRIM(@strItems)
			IF RIGHT(@strItems, 1)!=',' SET @strItems=@strItems+','

			SET @posComma=PatIndex('%,%', @strItems)
			WHILE @posComma>1
			BEGIN
				SET @strKeyVal=ltrim(rtrim(substring(@strItems, 1, @posComma-1)))
	
				DECLARE @posBStart INT,@posBEnd int, @strQty INT,@strName varchar(20)

				IF @bitLot=1 
				BEGIN
					SET @posBStart=PatIndex('%(%', @strKeyVal)
					SET @posBEnd=PatIndex('%)%', @strKeyVal)
					IF( @posBStart>1 AND @posBEnd>1)
					BEGIN
						SET @strName=LTRIM(RTRIM(SUBSTRING(@strKeyVal, 1, @posBStart-1)))
						SET	@strQty=LTRIM(RTRIM(SUBSTRING(@strKeyVal, @posBStart+1,len(@strKeyVal)-@posBStart-1)))
					END
					ELSE
					BEGIN
						SET @strName=@strKeyVal
						SET	@strQty=1
					END

					IF EXISTS (SELECT * FROM WareHouseItmsDTL WHERE numWareHouseItemID=@numFromWarehouseItemID AND vcSerialNo=@strName AND numQty >= @strQty)
					BEGIN
						UPDATE WareHouseItmsDTL SET numQty = numQty - @strQty WHERE numWareHouseItemID=@numFromWarehouseItemID AND vcSerialNo=@strName

						IF EXISTS (SELECT * FROM WareHouseItmsDTL WHERE numWareHouseItemID=@numToWarehouseItemID AND vcSerialNo=@strName)
						BEGIN
							UPDATE WareHouseItmsDTL SET numQty = ISNULL(numQty,0) + @strQty WHERE numWareHouseItemID=@numToWarehouseItemID AND vcSerialNo=@strName
						END
						ELSE
						BEGIN
							INSERT INTO WareHouseItmsDTL
							(
								numWareHouseItemID,
								vcSerialNo,
								numQty,
								dExpirationDate,
								bitAddedFromPO
							)
							SELECT
								@numToWarehouseItemID,
								vcSerialNo,
								@strQty,
								dExpirationDate,
								bitAddedFromPO
							FROM 
								WareHouseItmsDTL
							WHERE
								numWareHouseItemID=@numFromWarehouseItemID AND vcSerialNo=@strName
						END
					END
					ELSE
					BEGIN
						RAISERROR('INVALID SERIAL/LOT OR INSUFFICIENT SERIAL/LOT QUANTITY',16,1)
					END
				END
				ELSE
				BEGIN
					SET @strName=@strKeyVal
					SET	@strQty=1

					IF EXISTS (SELECT * FROM WareHouseItmsDTL WHERE numWareHouseItemID=@numFromWarehouseItemID AND vcSerialNo=@strName AND numQty = 1)
					BEGIN
						UPDATE WareHouseItmsDTL SET numWareHouseItemID=@numToWarehouseItemID WHERE numWareHouseItemID=@numFromWarehouseItemID AND vcSerialNo=@strName
					END
					ELSE
					BEGIN
						RAISERROR('INVALID SERIAL/LOT OR INSUFFICIENT SERIAL/LOT QUANTITY',16,1)
					END
				END  
	  
				SET @strItems=substring(@strItems, @posComma +1, len(@strItems)-@posComma)
				SET @posComma=PatIndex('%,%',@strItems)
			END
		END


		DECLARE @description VARCHAR(MAX) = 'Inventory Transfer - Transferred (Qty:' + CAST(@numQty AS VARCHAR(10)) + ')'

		UPDATE
			WarehouseItems
		SET
			numOnHand = ISNULL(numOnHand,0) - @numQty
			,dtModified = GETDATE() 
		WHERE
			numWareHouseItemID=@numFromWarehouseItemID

		EXEC dbo.USP_ManageWareHouseItems_Tracking
			@numWareHouseItemID = @numFromWarehouseItemID,
			@numReferenceID = @numItemCode,
			@tintRefType = 1,
			@vcDescription = @description,
			@numModifiedBy = @numUserCntID,
			@numDomainID = @numDomainID

		SET @description = 'Inventory Transfer - Receieved (Qty:' + CAST(@numQty AS VARCHAR(10)) + ')'

		UPDATE
			WarehouseItems
		SET
			numOnHand = (CASE WHEN numBackOrder >= @numQty THEN numOnHand ELSE ISNULL(numOnHand,0) + (@numQty - ISNULL(numBackOrder,0)) END) 
			,numAllocation = (CASE WHEN numBackOrder >= @numQty THEN ISNULL(numAllocation,0) + @numQty ELSE ISNULL(numAllocation,0) + ISNULL(numBackOrder,0) END) 
			,numBackOrder = (CASE WHEN numBackOrder >= @numQty THEN ISNULL(numBackOrder,0) - @numQty ELSE 0 END) 
			,dtModified = GETDATE() 
		WHERE
			numWareHouseItemID=@numToWarehouseItemID

		EXEC dbo.USP_ManageWareHouseItems_Tracking
			@numWareHouseItemID = @numToWarehouseItemID,
			@numReferenceID = @numItemCode,
			@tintRefType = 1,
			@vcDescription = @description,
			@numModifiedBy = @numUserCntID,
			@numDomainID = @numDomainID

	END
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ApplyCouponCodetoCart')
DROP PROCEDURE USP_ApplyCouponCodetoCart
GO
CREATE PROCEDURE [dbo].[USP_ApplyCouponCodetoCart]
	@numDomainID NUMERIC(18,0),
	@numItemCode NUMERIC(18,0) OUTPUT,
	@cookieId VARCHAR(MAX)=NULL,
	@numUserCntId numeric=0,
	@vcSendCoupon VARCHAR(200)=0,
	@numSiteID NUMERIC(18,0)=0
AS
BEGIN
	



	IF (SELECT COUNT(*) FROM PromotionOffer PO INNER JOIN PromotionOfferSites POS ON PO.numProId=POS.numPromotionID WHERE numDomainId=@numDomainID AND numSiteID=@numSiteID AND txtCouponCode = @vcSendCoupon)=0
	BEGIN
		RAISERROR('INVALID_COUPON_CODE',16,1)
		RETURN
	END 
	ELSE IF (SELECT 
			COUNT(*) 
		FROM 
			PromotionOffer PO 
		INNER JOIN 
			PromotionOfferSites POS 
		ON 
			PO.numProId=POS.numPromotionID
		WHERE 
			numDomainId=@numDomainID 
			AND numSiteID=@numSiteID
			AND txtCouponCode = @vcSendCoupon 
			AND bitEnabled = 1 
			AND 1= (CASE WHEN ISNULL(tintUsageLimit,0) > 0 THEN (CASE WHEN ISNULL(intCouponCodeUsed,0) < ISNULL(tintUsageLimit,0) THEN 1 ELSE 0 END) ELSE 1 END)
			AND 1 = (CASE WHEN ISNULL(bitNeverExpires,0) = 1 THEN 1 ELSE (CASE WHEN GETDATE() BETWEEN dtValidFrom AND dtValidTo THEN 1 ELSE 0 END) END)) = 0
	BEGIN
		RAISERROR('COUPON_EXPIRED',16,1)
		RETURN
	END

	IF((SELECT COUNT(numCartId) FROM CartItems WHERE numDomainId=@numDomainID AND vcCookieId=@cookieId AND numUserCntId=@numUserCntId AND vcCoupon=@vcSendCoupon)=0)
	BEGIN
		SET @numItemCode=1
		
		DECLARE @fltOfferTriggerValue FLOAT
		DECLARE @tintOfferTriggerValueType TINYINT
		DECLARE @tintOfferBasedOn TINYINT
		DECLARE @fltDiscountValue FLOAT
		DECLARE @tintDiscountType TINYINT
		DECLARE @tintDiscoutBaseOn TINYINT
		DECLARE @PromotionID NUMERIC(18,0)
		DECLARE @PromotionDesc VARCHAR(500)
		


		 SElECT TOP 1 
			@PromotionID=PO.numProId,
			@fltOfferTriggerValue=PO.fltOfferTriggerValue,
			@tintOfferTriggerValueType=PO.tintOfferTriggerValueType,
			@tintOfferBasedOn=PO.tintOfferBasedOn,
			@fltDiscountValue=fltDiscountValue,
			@tintDiscountType=tintDiscountType,
			@tintDiscoutBaseOn=tintDiscoutBaseOn,
			@PromotionDesc=(CONCAT('Buy '
					,CASE WHEN tintOfferTriggerValueType=1 THEN CAST(fltOfferTriggerValue AS VARCHAR) ELSE CONCAT('$',fltOfferTriggerValue) END
					,CASE tintOfferBasedOn
							WHEN 1 THEN CONCAT(' of "',(SELECT Stuff((SELECT CONCAT(', ',Item.vcItemName) FROM PromotionOfferItems INNER JOIN Item ON PromotionOfferItems.numValue = Item.numItemCode WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 FOR XML PATH('')), 1, 2, '')), '"')
							WHEN 2 THEN CONCAT(' of items belonging to classification(s) "',(SELECT Stuff((SELECT CONCAT(', ',ListDetails.vcData) FROM PromotionOfferItems INNER JOIN ListDetails ON PromotionOfferItems.numValue = ListDetails.numListItemID WHERE numListID=36 AND numProId=PO.numProId AND tintRecordType=5 AND tintType=2 FOR XML PATH('')), 1, 2, '')), '"')
						END
					,' & get '
					, CASE tintDiscountType WHEN 1 THEN CONCAT(fltDiscountValue,'% off on') WHEN 2 THEN CONCAT('$',fltDiscountValue, ' off on') WHEN 3 THEN CONCAT(fltDiscountValue,' of') END
					, CASE 
						WHEN tintDiscoutBaseOn = 1 THEN 
							CONCAT
							(
								CONCAT(' "',(SELECT Stuff((SELECT CONCAT(',',Item.vcItemName) FROM PromotionOfferItems INNER JOIN Item ON PromotionOfferItems.numValue = Item.numItemCode WHERE numProId=PO.numProId AND tintRecordType=6 AND tintType=1 FOR XML PATH('')), 1, 1, '')), '"')
								,(CASE WHEN PO.tintDiscountType = 3 THEN ' free' ELSE '' END)
								,'.'
							)
						WHEN tintDiscoutBaseOn = 2 THEN 
							CONCAT
							(
								CONCAT(' items belonging to classification(s) "',(SELECT Stuff((SELECT CONCAT(',',ListDetails.vcData) FROM PromotionOfferItems INNER JOIN ListDetails ON PromotionOfferItems.numValue = ListDetails.numListItemID WHERE numListID=36 AND numProId=PO.numProId AND tintRecordType=6 AND tintType=2 FOR XML PATH('')), 1, 1, '')), '"')
								,(CASE WHEN PO.tintDiscountType = 3 THEN ' free' ELSE '' END)
								,'.'
							)
						WHEN tintDiscoutBaseOn = 3 THEN ' related items.'
						ELSE '' 
					END
				))
		FROM
			PromotionOffer  AS PO
		WHERE
			PO.numDomainId=@numDomainID AND
			PO.txtCouponCode=@vcSendCoupon
		

		DECLARE @TEMPItems TABLE
		(
			ID INT,
			numCartID NUMERIC(18,0),
			numItemCode NUMERIC(18,0),
			numItemClassification NUMERIC(18,0),
			numUnitHour NUMERIC(18,0),
			monPrice NUMERIC(18,0),
			monTotalAmount NUMERIC(18,0)
		)

		IF @tintOfferBasedOn = 1 --individual items
		BEGIN
			INSERT INTO 
				@TEMPItems
			SELECT
				ROW_NUMBER() OVER(ORDER BY numCartID ASC),
				numCartId,
				CI.numItemCode,
				I.numItemClassification,
				CI.numUnitHour,
				CI.monPrice,
				(CI.numUnitHour * CI.monPrice)
			FROM
				CartItems CI
			INNER JOIN
				Item I
			ON
				CI.numItemCode = I.numItemCode
			WHERE
				CI.numDomainId=@numDomainId
				AND CI.numUserCntId=@numUserCntId
				AND CI.vcCookieId=@cookieId
				AND ISNULL(CI.PromotionID,0) = 0
				AND CI.numItemCode IN (SELECT numValue FROM PromotionOfferItems WHERE numProId=@PromotionID AND tintRecordType=5 AND tintType=1)
				AND 1 = (CASE 
						WHEN @tintOfferTriggerValueType=1 -- Quantity
						THEN (CASE WHEN numUnitHour >= @fltOfferTriggerValue THEN 1 ELSE 0 END)
						WHEN @tintOfferTriggerValueType=2 -- Amount
						THEN (CASE WHEN (CI.numUnitHour * CI.monPrice) >= @fltOfferTriggerValue THEN 1 ELSE 0 END)
						ELSE 0
						END)
		END
		ELSE IF @tintOfferBasedOn = 2 --item classifications
		BEGIN
			;WITH CTE (numCartId,numItemCode,numItemClassification,numUnitHour,monPrice,monTotalAmount) AS
			(
				SELECT
					numCartId,
					CI.numItemCode,
					I.numItemClassification,
					CI.numUnitHour,
					CI.monPrice,
					(CI.numUnitHour * CI.monPrice)
				FROM
					CartItems CI
				INNER JOIN
					Item I
				ON
					CI.numItemCode = I.numItemCode
				WHERE
					CI.numDomainId=@numDomainId
					AND CI.numUserCntId=@numUserCntId
					AND CI.vcCookieId=@cookieId
					AND ISNULL(CI.PromotionID,0) = 0
					AND I.numItemClassification IN (SELECT numValue FROM PromotionOfferItems WHERE numProId=@PromotionID AND tintRecordType=5 AND tintType=2)
			)

			INSERT INTO 
				@TEMPItems
			SELECT
				ROW_NUMBER() OVER(ORDER BY numCartID ASC),
				numCartId,
				numItemCode,
				numItemClassification,
				numUnitHour,
				monPrice,
				monTotalAmount
			FROM
				CTE
			WHERE
				1 = (CASE 
					WHEN @tintOfferTriggerValueType=1 -- Quantity
					THEN (CASE WHEN (SELECT SUM(numUnitHour) FROM CTE) >= @fltOfferTriggerValue THEN 1 ELSE 0 END)
					WHEN @tintOfferTriggerValueType=2 -- Amount
					THEN (CASE WHEN (SELECT SUM(monTotalAmount) FROM CTE) >= @fltOfferTriggerValue THEN 1 ELSE 0 END)
					ELSE 0
					END)
		END

		DECLARE @isPromotionTrigerred AS BIT = 0
		DECLARE @i AS INT = 1
		DECLARE @COUNT AS INT
		DECLARE @numTempCartID NUMERIC(18,0)
		DECLARE @numTempItemCode NUMERIC(18,0)
		DECLARE @numTempItemClassification NUMERIC(18,0)
		DECLARE @numTempUnitHour NUMERIC(18,0)
		DECLARE @monTempPrice NUMERIC(18,0)
		DECLARE @monTempTotalAmount NUMERIC(18,0)
		DECLARE @remainingOfferValue FLOAT = @fltOfferTriggerValue
		DECLARE @remainingDiscountValue FLOAT = @fltDiscountValue

		SELECT @COUNT=COUNT(*) FROM @TEMPItems

		IF @COUNT > 0
		BEGIN
			SET @isPromotionTrigerred = 1

			WHILE @i <= @COUNT
			BEGIN
				SELECT 
					@numTempCartID=numCartID,
					@numTempItemCode=numItemCode,
					@numTempItemClassification=ISNULL(numItemClassification,0),
					@numTempUnitHour=numUnitHour,
					@monTempPrice=monPrice,
					@monTempTotalAmount = (numUnitHour * monPrice)
				FROM 
					@TEMPItems 
				WHERE 
					ID=@i

				IF @tintOfferBasedOn = 1 -- Individual Items
					AND 1 = (CASE 
							WHEN @tintOfferTriggerValueType=1 -- Quantity
							THEN CASE WHEN @numTempUnitHour >= @fltOfferTriggerValue THEN 1 ELSE 0 END
							WHEN @tintOfferTriggerValueType=2 -- Amount
							THEN CASE WHEN @monTempTotalAmount >= @fltOfferTriggerValue THEN 1 ELSE 0 END
							ELSE 0
						END) 
				BEGIN
					UPDATE 
						CartItems 
					SET 
						PromotionID=@PromotionID ,
						PromotionDesc=@PromotionDesc,
						bitParentPromotion=1,
						vcCoupon=@vcSendCoupon
					WHERE 
						numCartId=@numTempCartID

					IF 1 =(CASE @tintDiscoutBaseOn
						WHEN 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems POI WHERE POI.numProId=@PromotionID AND POI.numValue=@numTempItemCode AND tintRecordType=6 AND tintType=1) > 0 THEN 1 ELSE 0 END)
						WHEN 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems POI WHERE POI.numProId=@PromotionID AND POI.numValue=@numTempItemClassification AND tintRecordType=6 AND tintType=2) > 0 THEN 1 ELSE 0 END)
						WHEN 3 THEN (CASE @tintOfferBasedOn
										WHEN 1 THEN (CASE WHEN (SELECT COUNT(*) FROM SimilarItems WHERE numItemCode=@numTempItemCode AND numParentItemCode IN (SELECT numValue FROM PromotionOfferItems POI WHERE POI.numProId=@PromotionID AND tintRecordType=5 AND tintType=1)) > 0 THEN 1 ELSE 0 END)
										WHEN 2 THEN (CASE WHEN (SELECT COUNT(*) FROM SimilarItems WHERE numItemCode=@numTempItemCode AND numParentItemCode IN (SELECT numItemCode FROM Item WHERE numDomainID=@numDomainId AND numItemClassification IN (SELECT numValue FROM PromotionOfferItems POI WHERE POI.numProId=@PromotionID AND tintRecordType=5 AND tintType=2))) > 0 THEN 1 ELSE 0 END)
										ELSE 0
									END) 
						ELSE 0
					END)
					BEGIN
						IF @tintDiscountType=1  AND @fltDiscountValue > 0 --Percentage
						BEGIN
							UPDATE 
								CartItems
							SET
								fltDiscount=@fltDiscountValue,
								bitDiscountType=0,
								monTotAmount=(@monTempTotalAmount-((@monTempTotalAmount*@fltDiscountValue)/100))
							WHERE
								numCartId=@numTempCartID
						END
						ELSE IF @tintDiscountType=2 AND @fltDiscountValue > 0 --Flat Amount
						BEGIN
							-- FIRST CHECK WHETHER DISCOUNT AMOUNT IS NOT ALREADY USED BY OTHER ITEMS
							DECLARE @usedDiscountAmount AS FLOAT
				
							SELECT
								@usedDiscountAmount = SUM(ISNULL(fltDiscount,0))
							FROM
								CartItems
							WHERE
								PromotionID=@PromotionID

							IF @usedDiscountAmount < @fltDiscountValue
							BEGIN
								UPDATE 
									CartItems 
								SET 
									monTotAmount=(CASE WHEN (@fltDiscountValue - @usedDiscountAmount) >= @monTempTotalAmount THEN 0 ELSE (@monTempTotalAmount-(@fltDiscountValue - @usedDiscountAmount)) END),
									bitDiscountType=1,
									fltDiscount=(CASE WHEN (@fltDiscountValue - @usedDiscountAmount) >= @monTempTotalAmount THEN @monTempTotalAmount ELSE (@fltDiscountValue - @usedDiscountAmount) END)
								WHERE 
									numCartId=@numTempCartID
							END
						END
						ELSE IF @tintDiscountType=3  AND @fltDiscountValue > 0 --Quantity
						BEGIN
							-- FIRST CHECK WHETHER DISCOUNT QTY IS NOT ALREADY USED BY OTHER ITEMS
							DECLARE @usedDiscountQty AS FLOAT

							SELECT
								@usedDiscountQty = SUM(ISNULL(fltDiscount,0)/(CASE WHEN ISNULL(monPrice,1) = 0 THEN 1 ELSE ISNULL(monPrice,1) END))
							FROM
								CartItems
							WHERE
								PromotionID=@PromotionID

							IF @usedDiscountQty < @fltDiscountValue
							BEGIN
								UPDATE 
									CartItems 
								SET 
									monTotAmount=(CASE WHEN (@fltDiscountValue - @usedDiscountQty) >= @numTempUnitHour THEN 0 ELSE (@monTempTotalAmount-((@fltDiscountValue - @usedDiscountQty) * @monTempPrice)) END),
									bitDiscountType=1,
									fltDiscount=(CASE WHEN (@fltDiscountValue - @usedDiscountQty) >= @numTempUnitHour THEN (@numTempUnitHour*@monTempPrice) ELSE ((@fltDiscountValue - @usedDiscountQty) * @monTempPrice) END) 
								WHERE 
									numCartId=@numTempCartID
							END
						END
					END

					
					BREAK
				END
				ELSE IF @tintOfferBasedOn = 2 -- Item Classification
				BEGIN
					UPDATE 
						CartItems 
					SET 
						PromotionID=@PromotionID ,
						PromotionDesc=@PromotionDesc,
						bitParentPromotion=1,
						vcCoupon=@vcSendCoupon
					WHERE 
						numCartId=@numTempCartID

					-- CHECK IF DISCOUNT FROM PROMOTION CAN ALSO BE APPLIED TO ITEM
					IF 1=(CASE @tintDiscoutBaseOn
							WHEN 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems POI WHERE POI.numProId=@PromotionID AND POI.numValue=@numTempItemCode AND tintRecordType=6 AND tintType=1) > 0 THEN 1 ELSE 0 END)
							WHEN 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems POI WHERE POI.numProId=@PromotionID AND POI.numValue=@numTempItemClassification AND tintRecordType=6 AND tintType=2) > 0 THEN 1 ELSE 0 END)
							WHEN 3 THEN (CASE @tintOfferBasedOn
											WHEN 1 THEN (CASE WHEN (SELECT COUNT(*) FROM SimilarItems WHERE numItemCode=@numTempItemCode AND numParentItemCode IN (SELECT numValue FROM PromotionOfferItems POI WHERE POI.numProId=@PromotionID AND tintRecordType=5 AND tintType=1)) > 0 THEN 1 ELSE 0 END)
											WHEN 2 THEN (CASE WHEN (SELECT COUNT(*) FROM SimilarItems WHERE numItemCode=@numTempItemCode AND numParentItemCode IN (SELECT numItemCode FROM Item WHERE numDomainID=@numDomainId AND numItemClassification IN (SELECT numValue FROM PromotionOfferItems POI WHERE POI.numProId=@PromotionID AND tintRecordType=5 AND tintType=2))) > 0 THEN 1 ELSE 0 END)
											ELSE 0
										END) 
							ELSE 0
						END)
					BEGIN
						IF @tintDiscountType=1 --Percentage
						BEGIN
							UPDATE 
								CartItems
							SET
								fltDiscount=@fltDiscountValue,
								bitDiscountType=0,
								monTotAmount=(@monTempTotalAmount-((@monTempTotalAmount*@fltDiscountValue)/100))
							WHERE
								numCartId=@numTempCartID
						END
						ELSE IF @tintDiscountType=2 --Flat Amount
						BEGIN
							-- FIRST CHECK WHETHER DISCOUNT AMOUNT IS NOT ALREADY USED BY OTHER ITEMS
							SELECT
								@usedDiscountAmount = SUM(ISNULL(fltDiscount,0))
							FROM
								CartItems
							WHERE
								PromotionID=@PromotionID

							IF @usedDiscountAmount < @fltDiscountValue
							BEGIN
								UPDATE 
									CartItems 
								SET 
									monTotAmount=(CASE WHEN (@fltDiscountValue - @usedDiscountAmount) >= @monTempTotalAmount THEN 0 ELSE (@monTempTotalAmount-(@fltDiscountValue - @usedDiscountAmount)) END),
									bitDiscountType=1,
									fltDiscount=(CASE WHEN (@fltDiscountValue - @usedDiscountAmount) >= @monTempTotalAmount THEN @monTempTotalAmount ELSE (@fltDiscountValue - @usedDiscountAmount) END)
								WHERE 
									numCartId=@numTempCartID
							END
						END
						ELSE IF @tintDiscountType=3 --Quantity
						BEGIN
							-- FIRST CHECK WHETHER DISCOUNT QTY IS NOT ALREADY USED BY OTHER ITEMS
							SELECT
								@usedDiscountQty = SUM(ISNULL(fltDiscount,0)/(CASE WHEN ISNULL(monPrice,1) = 0 THEN 1 ELSE ISNULL(monPrice,1) END))
							FROM
								CartItems
							WHERE
								PromotionID=@PromotionID

							IF @usedDiscountQty < @fltDiscountValue
							BEGIN
								UPDATE 
									CartItems 
								SET 
									monTotAmount=(CASE WHEN (@fltDiscountValue - @usedDiscountQty) >= @numTempUnitHour THEN 0 ELSE (@monTempTotalAmount-((@fltDiscountValue - @usedDiscountQty) * @monTempPrice)) END),
									bitDiscountType=1,
									fltDiscount=(CASE WHEN (@fltDiscountValue - @usedDiscountQty) >= @numTempUnitHour THEN (@numTempUnitHour*@monTempPrice) ELSE ((@fltDiscountValue - @usedDiscountQty) * @monTempPrice) END) 
								WHERE 
									numCartId=@numTempCartID
							END
						END
					END

					SET @remainingOfferValue = @remainingOfferValue - (CASE WHEN @tintOfferTriggerValueType=1 THEN @numTempUnitHour ELSE @monTempTotalAmount END)

					IF @remainingOfferValue <= 0
					BEGIN
						BREAK
					END
				END

				SET @i = @i + 1
			END
		END

		

		IF ISNULL(@isPromotionTrigerred,0) = 1
		BEGIN
			PRINT CONCAT(@isPromotionTrigerred,@tintDiscoutBaseOn)
			DELETE FROM @TEMPItems
				
			INSERT INTO 
				@TEMPItems
			SELECT
				ROW_NUMBER() OVER(ORDER BY numCartID ASC),
				numCartId,
				CI.numItemCode,
				I.numItemClassification,
				CI.numUnitHour,
				CI.monPrice,
				(CI.numUnitHour * CI.monPrice)
			FROM
				CartItems CI
			INNER JOIN
				Item I
			ON
				CI.numItemCode = I.numItemCode
			WHERE
				CI.numDomainId=@numDomainId
				AND CI.numUserCntId=@numUserCntId
				AND CI.vcCookieId=@CookieId
				AND ISNULL(CI.PromotionID,0) = 0
				AND 1 = (CASE @tintDiscoutBaseOn
							WHEN 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numValue=I.numItemCode AND numProId=@PromotionID AND tintRecordType=6 AND tintType=1) > 0 THEN 1 ELSE 0 END)
							WHEN 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numValue=I.numItemClassification AND numProId=@PromotionID AND tintRecordType=6 AND tintType=2) > 0 THEN 1 ELSE 0 END)
							WHEN 3 THEN (CASE @tintOfferBasedOn 
									WHEN 1 THEN (CASE WHEN (SELECT COUNT(*) FROM SimilarItems WHERE numItemCode=I.numItemCode AND numParentItemCode IN (SELECT numValue FROM PromotionOfferItems POI WHERE POI.numProId=@PromotionID AND tintRecordType=5 AND tintType=1)) > 0 THEN 1 ELSE 0 END)
									WHEN 2 THEN (CASE WHEN (SELECT COUNT(*) FROM SimilarItems WHERE numItemCode=I.numItemCode AND numParentItemCode IN (SELECT numItemCode FROM Item WHERE numDomainID=@numDomainId AND numItemClassification IN (SELECT numValue FROM PromotionOfferItems POI WHERE POI.numProId=@PromotionID AND tintRecordType=5 AND tintType=2))) > 0 THEN 1 ELSE 0 END)
									ELSE 0
								END) 
							ELSE 0
						END)

			DECLARE @k AS INT = 1
			DECLARE @kCount AS INT 

			SELECT @kCount = COUNT(*) FROM @TEMPItems
			SELECT * FROM @TEMPItems
			IF @kCount > 0
			BEGIN
				WHILE @k <= @kCount
				BEGIN
					SELECT 
						@numTempCartID=numCartID,
						@numTempItemCode=numItemCode,
						@numTempItemClassification=ISNULL(numItemClassification,0),
						@numTempUnitHour=numUnitHour,
						@monTempPrice=monPrice,
						@monTempTotalAmount = (numUnitHour * monPrice)
					FROM 
						@TEMPItems 
					WHERE 
						ID=@k

					IF @tintDiscountType=1 --Percentage
					BEGIN
						UPDATE 
							CartItems
						SET
							PromotionID=@PromotionID,
							PromotionDesc=@PromotionDesc,
							bitParentPromotion=0,
							fltDiscount=@fltDiscountValue,
							bitDiscountType=0,
							monTotAmount=(@monTempTotalAmount-((@monTempTotalAmount*@fltDiscountValue)/100))
						WHERE
							numCartId=@numTempCartID
					END
					ELSE IF @tintDiscountType=2 --Flat Amount
					BEGIN
						-- FIRST CHECK WHETHER DISCOUNT AMOUNT IS NOT ALREADY USED BY OTHER ITEMS
						SELECT
							@usedDiscountAmount = SUM(fltDiscount)
						FROM
							CartItems
						WHERE
							PromotionID=@PromotionID

						IF @usedDiscountAmount < @fltDiscountValue
						BEGIN
							UPDATE 
								CartItems 
							SET 
								monTotAmount=(CASE WHEN (@fltDiscountValue - @usedDiscountAmount)  >= @monTempTotalAmount THEN 0 ELSE (@monTempTotalAmount-(@fltDiscountValue - @usedDiscountAmount)) END),
								PromotionID=@PromotionID ,
								PromotionDesc=@PromotionDesc,
								bitDiscountType=1,
								fltDiscount=(CASE WHEN (@fltDiscountValue - @usedDiscountAmount) >= @monTempTotalAmount THEN @monTempTotalAmount ELSE (@fltDiscountValue - @usedDiscountAmount) END)
							WHERE 
								numCartId=@numTempCartID
						END
						ELSE
						BEGIN
							BREAK
						END
					END
					ELSE IF @tintDiscountType=3 --Quantity
					BEGIN
						-- FIRST CHECK WHETHER DISCOUNT QTY IS NOT ALREADY USED BY OTHER ITEMS
						SELECT
							@usedDiscountQty = SUM(fltDiscount/monPrice)
						FROM
							CartItems
						WHERE
							PromotionID=@PromotionID

						IF @usedDiscountQty < @fltDiscountValue
						BEGIN
							UPDATE 
								CartItems 
							SET 
								monTotAmount=(CASE WHEN (@fltDiscountValue - @usedDiscountQty)  >= numUnitHour THEN 0 ELSE (@monTempTotalAmount-((@fltDiscountValue - @usedDiscountQty) * @monTempPrice)) END),
								PromotionID=@PromotionID ,
								PromotionDesc=@PromotionDesc,
								bitDiscountType=1,
								fltDiscount=(CASE WHEN (@fltDiscountValue - @usedDiscountQty) >= numUnitHour THEN @monTempTotalAmount ELSE ((@fltDiscountValue - @usedDiscountQty) * @monTempPrice) END) 
							WHERE 
								numCartId=@numTempCartID
						END
						ELSE
						BEGIN
							BREAK
						END
					END

					SET @k = @k + 1
				END
			END
		END
	END
	ELSE
	BEGIN
		SET @numItemCode=2
	END

	SELECT @numItemCode AS ResOutput
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetShippingCharge')
DROP PROCEDURE USP_GetShippingCharge
GO
CREATE PROCEDURE [dbo].[USP_GetShippingCharge]
	@numDomainID NUMERIC(18,0),
	@numShippingCountry AS NUMERIC(18,0),
	@cookieId VARCHAR(MAX)=NULL,
	@numUserCntId numeric=0,
	@monShippingAmount money OUTPUT
AS
BEGIN
	SET @monShippingAmount=-1
	DECLARE @numPromotionID AS NUMERIC(18,0)=0

	DECLARE @decmPrice DECIMAL(18,2)
	SET @decmPrice = (
						SELECT 
							ISNULL(SUM(monTotAmount),0) 
						FROM 
							CartItems 
						WHERE 
							numDomainId=@numDomainID 
							AND vcCookieId=@cookieId 
							AND numUserCntId=@numUserCntId
					)


	SET @numPromotionID= (
							SELECT TOP 1 
								PromotionID 
							FROM 
								CartItems AS C 
							LEFT JOIN 
								PromotionOffer AS P 
							ON 
								C.PromotionID=P.numProId
							WHERE
								C.numDomainId=@numDomainID 
								AND C.vcCookieId=@cookieId 
								AND C.numUserCntId=@numUserCntId
								AND (ISNULL(bitFixShipping1,0)=1 OR ISNULL(bitFixShipping2,0)=1 OR ISNULL(bitFreeShiping,0)=1) 
							ORDER BY 
								numCartId 
							)

	IF(@numPromotionID>0)
	BEGIN
		DECLARE @bitFreeShiping BIT
		DECLARE @monFreeShippingOrderAmount money
		DECLARE @numFreeShippingCountry money
		DECLARE @bitFixShipping1 BIT
		DECLARE @monFixShipping1OrderAmount money
		DECLARE @monFixShipping1Charge money
		DECLARE @bitFixShipping2 BIT
		DECLARE @monFixShipping2OrderAmount money
		DECLARE @monFixShipping2Charge money
		SELECT 
			@bitFreeShiping=bitFreeShiping
			,@monFreeShippingOrderAmount=monFreeShippingOrderAmount
			,@numFreeShippingCountry=numFreeShippingCountry
			,@bitFixShipping1=bitFixShipping1
			,@monFixShipping1OrderAmount=monFixShipping1OrderAmount
			,@monFixShipping1Charge=monFixShipping1Charge
			,@bitFixShipping2=bitFixShipping2
			,@monFixShipping2OrderAmount=monFixShipping2OrderAmount
			,@monFixShipping2Charge=monFixShipping2Charge
		FROM
			PromotionOffer PO
		WHERE
			numDomainId=@numDomainID 
			AND numProId = @numPromotionID

		IF(ISNULL(@bitFixShipping1,0)=1 AND @decmPrice > @monFixShipping1OrderAmount)
		BEGIN
			SET @monShippingAmount=@monFixShipping1Charge
		END

		IF(ISNULL(@bitFixShipping2,0)=1 AND @decmPrice > @monFixShipping2OrderAmount)
		BEGIN
			SET @monShippingAmount=@monFixShipping2Charge
		END

		IF((ISNULL(@bitFreeShiping,0)=1 AND @numFreeShippingCountry=@numShippingCountry AND @decmPrice>@monFreeShippingOrderAmount))
		BEGIN
			SET @monShippingAmount=0
		END
	END
	
	SELECT ISNULL(@monShippingAmount,0)
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_RemoveCouponCode')
DROP PROCEDURE USP_RemoveCouponCode
GO
CREATE PROCEDURE [dbo].[USP_RemoveCouponCode]
	@numDomainID NUMERIC(18,0),
	@numItemCode NUMERIC(18,0),
	@cookieId VARCHAR(MAX)=NULL,
	@numUserCntId numeric=0,
	@vcSendCoupon VARCHAR(200)=0,
	@numSiteID NUMERIC(18,0) = 0
AS
BEGIN
	UPDATE
		CartItems
	SET
		PromotionID=0,
		PromotionDesc='',
		monTotAmount=monTotAmtBefDiscount,
		fltDiscount=0,
		vcCoupon='',
		bitParentPromotion=0
	WHERE
		numDomainId=@numDomainID AND
		vcCookieId=@cookieId AND
		numUserCntId=@numUserCntId AND 
		PromotionID IN (ISNULL((SELECT TOP 1 PromotionID FROM CartItems WHERE numDomainId=@numDomainID AND vcCookieId=@cookieId AND vcCoupon=@vcSendCoupon AND numUserCntId=@numUserCntId),0))

	EXEC USP_ManageECommercePromotion @numUserCntId,@numDomainId,@cookieId,0,@numSiteID
		
END
