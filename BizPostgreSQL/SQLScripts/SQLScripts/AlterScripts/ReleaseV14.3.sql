/******************************************************************
Project: Release 14.3 Date: 13.OCT.2020
Comments: ALTER SCRIPTS
*******************************************************************/

/******************************************** SANDEEP *********************************************/

UPDATE OpportunityBizDocs SET dtFromDate=dtCreatedDate WHERE dtCreatedDate <> dtFromDate AND CAST(dtCreatedDate AS DATE) = CAST(dtFromDate AS DATE)

-----------------------------

UPDATE PageNavigationDTL SET vcPageNavName='My Reports & Saved Searches' WHERE numParentID=22 AND vcPageNavName ='My Reports'
UPDATE PageNavigationDTL SET bitVisible=0 WHERE numParentID=22 AND vcPageNavName = 'Recurring Transactions Report'
UPDATE PageNavigationDTL SET bitVisible=0 WHERE numParentID=22 AND vcPageNavName = 'Sales Forecasts'
UPDATE PageNavigationDTL SET vcPageNavName='Legacy Reports' WHERE numParentID=22 AND vcPageNavName = 'E-Commerce Reports'

-----------------------

UPDATE PageNavigationDTL SET intSortOrder=1 WHERE numParentID=22 AND numPageNavID=24
UPDATE PageNavigationDTL SET intSortOrder=2 WHERE numParentID=22 AND numPageNavID=92
UPDATE PageNavigationDTL SET intSortOrder=3 WHERE numParentID=22 AND numPageNavID=245
UPDATE PageNavigationDTL SET intSortOrder=4 WHERE numParentID=22 AND numPageNavID=246

-----------------------

BEGIN TRY
BEGIN TRANSACTION

	DECLARE @numMAXPageNavID INT
	SELECT @numMAXPageNavID = MAX(numPageNavID) FROM PageNavigationDTL


	INSERT INTO PageNavigationDTL
	(
		numPageNavID,numModuleID,numParentID,vcPageNavName,vcNavURL,bitVisible,numTabID,intSortOrder
	)
	VALUES
	(
		(@numMAXPageNavID + 1),8,22,'Scheduled Reports & Saved Searches','../reports/frmScheduledReports.aspx',1,6,5
	)

	INSERT INTO dbo.TreeNavigationAuthorization
	(
		numGroupID,
		numTabID,
		numPageNavID,
		bitVisible,
		numDomainID,
		tintType
	)
	SELECT  
		numGroupID,
		6,
		(@numMAXPageNavID + 1),
		1,
		numDomainID,
		1
	FROM    
		AuthenticationGroupMaster
COMMIT
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		ERROR_MESSAGE(),
		ERROR_NUMBER(),
		ERROR_SEVERITY(),
		ERROR_STATE(),
		ERROR_LINE(),
		ISNULL(ERROR_PROCEDURE(), '-');
END CATCH

---------------------------------------------------------------

INSERT INTO CFw_Grp_Master
(
	Grp_Name,Loc_Id,numDomainID,tintType,vcURLF
)
SELECT
	'Sales Forecasts',2,numDomainId,2,''
FROM
	Domain

------------------------

ALTER TABLE ReportDashboard ADD dtLastCacheDate DATE

--------------------------

USE [Production.2014]
GO

/****** Object:  Table [dbo].[ScheduledReportsGroup]    Script Date: 13-Oct-20 12:11:33 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ScheduledReportsGroup](
	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numDomainID] [numeric](18, 0) NOT NULL,
	[numCreatedBy] [numeric](18, 0) NOT NULL,
	[vcName] [nvarchar](200) NOT NULL,
	[tintFrequency] [tinyint] NOT NULL,
	[dtStartDate] [datetime] NOT NULL,
	[numEmailTemplate] [numeric](18, 0) NOT NULL,
	[vcSelectedTokens] [nvarchar](max) NOT NULL,
	[vcRecipientsEmail] [nvarchar](max) NOT NULL,
	[vcReceipientsContactID] [nvarchar](max) NOT NULL,
	[tintDataCacheStatus] [tinyint] NOT NULL CONSTRAINT [DF_ScheduledReportsGroup_tintDataCacheStatus]  DEFAULT ((0)),
	[dtNextDate] [datetime] NULL,
	[vcExceptionMessage] [nvarchar](max) NULL,
	[intTimeZoneOffset] [int] NULL,
	[intNotOfTimeTried] [int] NULL,
 CONSTRAINT [PK_ScheduledReportsGroup] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

-------------------------------------------

USE [Production.2014]
GO

/****** Object:  Table [dbo].[ScheduledReportsGroupLog]    Script Date: 13-Oct-20 12:11:50 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ScheduledReportsGroupLog](
	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numSRGID] [numeric](18, 0) NOT NULL,
	[dtExecutionDate] [datetime] NOT NULL,
	[vcException] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_ScheduledReportsGroupLog] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

------------------------------------------------

USE [Production.2014]
GO

/****** Object:  Table [dbo].[ScheduledReportsGroupReports]    Script Date: 13-Oct-20 12:12:11 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ScheduledReportsGroupReports](
	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numSRGID] [numeric](18, 0) NOT NULL,
	[numReportID] [numeric](18, 0) NOT NULL,
	[tintReportType] [tinyint] NOT NULL,
	[intSortOrder] [int] NOT NULL,
 CONSTRAINT [PK_ScheduledReportsGroupReports] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

-----------------------------

ALTER TABLE ContractsLog ADD tintRecordType TINYINT