/******************************************************************
Project: Release 11.3 Date: 11.MAR.2019
Comments: ALTER SCRIPTS
*******************************************************************/

/******************************************** SANDEEP *********************************************/

ALTER TABLE OpportunityBizDocs ADD numARAccountID NUMERIC(18,0)
ALTER TABLE ReturnHeader ADD numOppBizDocID NUMERIC(18,0)


USE [Production.2014]
GO

/****** Object:  Table [dbo].[ECommerceCreditCardTransactionLog]    Script Date: 11-Mar-19 2:35:58 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ECommerceCreditCardTransactionLog](
	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numDomainID] [numeric](18, 0) NOT NULL,
	[numOppID] [numeric](18, 0) NOT NULL,
	[monAmount] [decimal](20, 5) NOT NULL,
	[bitNsoftwareCallStarted] [bit] NOT NULL,
	[bitNSoftwareCallCompleted] [bit] NOT NULL,
	[bitSuccess] [bit] NULL,
	[numTransHistoryID] [numeric](18, 0) NULL,
	[vcMessage] [varchar](max) NULL,
	[vcExceptionMessage] [varchar](max) NULL,
	[vcStackStrace] [varchar](max) NULL,
 CONSTRAINT [PK_ECommerceCreditCardTransactionLog] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING ON
GO





/******************************************** PRASANT *********************************************/

ALTER TABLE StagePercentageDetailsTask ADD numReferenceTaskId NUMERIC DEFAULT 0
ALTER TABLE ListDetails ADD numListItemGroupId NUMERIC DEFAULT 0
SET IDENTITY_INSERT ListMaster ON;

INSERT INTO ListMaster 
(
	numListID,vcListName,bitDeleted,bitFixed,numDomainID,bitFlag,vcDataType,numModuleID,numCreatedBy
)
VALUES
(
	1047,'Follow up Status Group',0,1,1,1,'string',1,1
)

SET IDENTITY_INSERT ListMaster OFF;