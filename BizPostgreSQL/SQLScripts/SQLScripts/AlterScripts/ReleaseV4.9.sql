/******************************************************************
Project: Release 4.9 Date: 05.September.2015
Comments: ALTER SCRIPTS
*******************************************************************/

-------------- SANDEEP --------------

ALTER TABLE WarehouseItmsDTL ADD 
bitAddedFromPO BIT

-------------- KAMAL ----------------

--SELECT * FROM [dbo].[DycFieldMaster] AS DFM WHERE [DFM].[vcFieldName] LIKE '%Compliation%'
UPDATE [DycFieldMaster] SET [vcFieldName]='Completion Date' WHERE [vcFieldName] LIKE '%Compliation%'


--SELECT * FROM [dbo].DycFormField_Mapping AS DFM WHERE [DFM].[vcFieldName] LIKE '%Compliation%'
UPDATE DycFormField_Mapping SET [vcFieldName]='Completion Date' WHERE [vcFieldName] LIKE '%Compliation%'



ALTER TABLE dbo.Domain ADD
	numPODropShipBizDoc numeric(18, 0) NULL,
	numPODropShipBizDocTemplate numeric(18, 0) NULL