/******************************************************************
Project: Release 2.5 Date: 18.12.2013
Comments: 
*******************************************************************/

/*******************Kamal Script******************/
--/************************************************************************************************/
--/************************************************************************************************/

UPDATE dbo.DycFieldMaster SET vcFieldDataType='M' WHERE numFieldId=99

ALTER TABLE dbo.ReturnHeader ADD
	numDepositIDRef numeric(18, 0) NULL,
	numBillPaymentIDRef numeric(18, 0) NULL
	

ALTER TABLE dbo.BillPaymentHeader ADD
	monRefundAmount money NULL	
	
	
ALTER TABLE dbo.DepositMaster ADD
	monRefundAmount money NULL	
	
	
	

DECLARE @v sql_variant 
SET @v = N'0: BizForm Wizards
1: Regular Form (Grid Settings)
2: Other Form
3: WorkFlow Form'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'DynamicFormMaster', N'COLUMN', N'tintFlag'



INSERT INTO dbo.DynamicFormMaster (numFormId,vcFormName,cCustomFieldsAssociated,
	cAOIAssociated,bitDeleted,tintFlag,bitWorkFlow,vcLocationID,bitAllowGridColor)
SELECT 68,'Organization','Y','N',0,3,0,'1,12,13,14',0 UNION
SELECT 69,'Contacts','Y','N',0,3,0,'4',0 UNION
SELECT 70,'Opportunities & Orders','Y','N',0,3,0,'2,6',0 UNION
SELECT 71,'BizDocs','Y','N',0,3,0,'',0 UNION
SELECT 72,'Cases','Y','N',0,3,0,'3',0 UNION
SELECT 73,'Projects','Y','N',0,3,0,'11',0 UNION
SELECT 74,'Items','Y','N',0,3,0,'5',0




CREATE TABLE [dbo].[WorkFlowMaster](
	[numWFID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numDomainID] [numeric](18, 0) NULL,
	[vcWFName] [varchar](200) NULL,
	[vcWFDescription] [varchar](500) NULL,
	[numCreatedBy] [numeric](18, 0) NULL,
	[numModifiedBy] [numeric](18, 0) NULL,
	[dtCreatedDate] [datetime] NULL,
	[dtModifiedDate] [datetime] NULL,
	[bitActive] [bit] NULL,
	[numFormID] [numeric](18, 0) NULL,
	[tintWFTriggerOn] [tinyint] NULL,
 CONSTRAINT [PK_WorkFlowMaster] PRIMARY KEY CLUSTERED 
(
	[numWFID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING ON
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1:Create
2:Edit
3:Create/Edit
4:Field Update
5:Delete
6:Date Field Condition' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WorkFlowMaster', @level2type=N'COLUMN',@level2name=N'tintWFTriggerOn'



CREATE TABLE [dbo].[WorkFlowTriggerFieldList](
	[numWFTriggerFieldID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numWFID] [numeric](18, 0) NULL,
	[numFieldID] [numeric](18, 0) NULL,
	[bitCustom] [bit] NOT NULL,
 CONSTRAINT [PK_WorkFlowTriggerFieldList] PRIMARY KEY CLUSTERED 
(
	[numWFTriggerFieldID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[WorkFlowTriggerFieldList]  WITH CHECK ADD  CONSTRAINT [FK_WorkFlowTriggerFieldList_WorkFlowMaster] FOREIGN KEY([numWFID])
REFERENCES [dbo].[WorkFlowMaster] ([numWFID])
GO
ALTER TABLE [dbo].[WorkFlowTriggerFieldList] CHECK CONSTRAINT [FK_WorkFlowTriggerFieldList_WorkFlowMaster]



CREATE TABLE [dbo].[WorkFlowConditionList](
	[numWFConditionID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numWFID] [numeric](18, 0) NULL,
	[numFieldID] [numeric](18, 0) NULL,
	[bitCustom] [bit] NULL,
	[vcFilterValue] [varchar](500) NULL,
	[vcFilterOperator] [varchar](10) NULL,
	[vcFilterANDOR] [varchar](10) NULL,
 CONSTRAINT [PK_WorkFlowConditionList] PRIMARY KEY CLUSTERED 
(
	[numWFConditionID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[WorkFlowConditionList]  WITH CHECK ADD  CONSTRAINT [FK_WorkFlowConditionList_WorkFlowMaster] FOREIGN KEY([numWFID])
REFERENCES [dbo].[WorkFlowMaster] ([numWFID])
GO
ALTER TABLE [dbo].[WorkFlowConditionList] CHECK CONSTRAINT [FK_WorkFlowConditionList_WorkFlowMaster]



CREATE TABLE [dbo].[WorkFlowActionList](
	[numWFActionID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numWFID] [numeric](18, 0) NULL,
	[tintActionType] [tinyint] NULL,
	[numTemplateID] [numeric](18, 0) NULL,
	[vcEmailToType] [varchar](50) NULL,
	[tintTicklerActionAssignedTo] [tinyint] NULL,
	[vcAlertMessage] [ntext] NULL,
	[tintActionOrder] [tinyint] NULL,
 CONSTRAINT [PK_WorkFlowActionList] PRIMARY KEY CLUSTERED 
(
	[numWFActionID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1 : Email Alerts
2 : Action Items
3 : Update Fields
4:  Alert Message' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WorkFlowActionList', @level2type=N'COLUMN',@level2name=N'tintActionType'
GO
ALTER TABLE [dbo].[WorkFlowActionList]  WITH CHECK ADD  CONSTRAINT [FK_WorkFlowActionList_WorkFlowMaster] FOREIGN KEY([numWFID])
REFERENCES [dbo].[WorkFlowMaster] ([numWFID])
GO
ALTER TABLE [dbo].[WorkFlowActionList] CHECK CONSTRAINT [FK_WorkFlowActionList_WorkFlowMaster]



CREATE TABLE [dbo].[WorkFlowActionUpdateFields](
	[numWFActionUFID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numWFActionID] [numeric](18, 0) NULL,
	[tintModuleType] [numeric](18, 0) NULL,
	[numFieldID] [numeric](18, 0) NULL,
	[bitCustom] [bit] NULL,
	[vcValue] [varchar](500) NULL,
 CONSTRAINT [PK_WorkFlowConditionUpdateFields] PRIMARY KEY CLUSTERED 
(
	[numWFActionUFID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[WorkFlowActionUpdateFields]  WITH CHECK ADD  CONSTRAINT [FK_WorkFlowActionUpdateFields_WorkFlowActionList] FOREIGN KEY([numWFActionID])
REFERENCES [dbo].[WorkFlowActionList] ([numWFActionID])
GO
ALTER TABLE [dbo].[WorkFlowActionUpdateFields] CHECK CONSTRAINT [FK_WorkFlowActionUpdateFields_WorkFlowActionList]



CREATE TABLE [dbo].[WorkFlowQueue](
	[numWFQueueID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numDomainID] [numeric](18, 0) NOT NULL,
	[numCreatedBy] [numeric](18, 0) NOT NULL,
	[dtCreatedDate] [datetime] NOT NULL,
	[numRecordID] [numeric](18, 0) NOT NULL,
	[numFormID] [numeric](18, 0) NOT NULL,
	[tintProcessStatus] [tinyint] NOT NULL,
	[tintWFTriggerOn] [tinyint] NULL,
	[numWFID] [numeric](18, 0) NULL,
 CONSTRAINT [PK_WorkFlowQueue] PRIMARY KEY CLUSTERED 
(
	[numWFQueueID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1:Pending Execution 2:In Progress 3:Success 4:Failed 5:No WF Found' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WorkFlowQueue', @level2type=N'COLUMN',@level2name=N'tintProcessStatus'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1:Create
2:Edit
3:Create/Edit
4:Field Update
5:Delete
6:Date Field Condition' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WorkFlowQueue', @level2type=N'COLUMN',@level2name=N'tintWFTriggerOn'



CREATE TABLE [dbo].[WorkFlowQueueExecution](
	[numWFQueueExeID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numWFQueueID] [numeric](18, 0) NOT NULL,
	[numWFID] [numeric](18, 0) NOT NULL,
	[dtExecutionDate] [datetime] NOT NULL,
	[bitSuccess] [bit] NOT NULL,
	[vcDescription] [varchar](1000) NULL,
 CONSTRAINT [PK_WorkFlowQueueExecution] PRIMARY KEY CLUSTERED 
(
	[numWFQueueExeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]



CREATE TABLE [dbo].[Audit](
	[AuditID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Type] [char](1) NULL,
	[TableName] [varchar](128) NULL,
	[PK] [varchar](1000) NULL,
	[FieldName] [varchar](128) NULL,
	[OldValue] [varchar](1000) NULL,
	[NewValue] [varchar](1000) NULL,
	[UpdateDate] [datetime] NULL,
	[UserName] [varchar](128) NULL,
	[numRecordID] [numeric](18, 0) NULL,
	[numDomainID] [numeric](18, 0) NULL,
	[AuditGUID] [uniqueidentifier] NULL,
 CONSTRAINT [PK_Audit] PRIMARY KEY CLUSTERED 
(
	[AuditID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]


SELECT * FROM dbo.DycFormField_Mapping WHERE numFormID=70


INSERT INTO [dbo].[DycFormField_Mapping]([numModuleID], [numFieldID], [numDomainID], [numFormID], [bitAllowEdit], [bitInlineEdit], [vcFieldName], [vcAssociatedControlType], [vcPropertyName], [PopupFunctionName], [order], [tintRow], [tintColumn], [bitInResults], [bitDeleted], [bitDefault], [bitSettingField], [bitAddField], [bitDetailField], [bitAllowSorting], [bitWorkFlowField], [bitImport], [bitExport], [bitAllowFiltering], [bitRequired], [numFormFieldID], [intSectionID], [bitAllowGridColor])
SELECT 3, 96, NULL, 70, 1, 0, N'Sales Order Name', N'TextBox', N'OpportunityName', NULL, 1, 1, 1, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, NULL, 1876, NULL, 0 UNION ALL
SELECT 3, 97, NULL, 70, 1, 0, N'Source', N'SelectBox', N'Source', NULL, 2, 5, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, NULL, 1877, NULL, 0 UNION ALL
SELECT 3, 99, NULL, 70, 0, 0, N'Amount', N'TextBox', N'Amount', NULL, 4, 10, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, NULL, 1879, NULL, 0 UNION ALL
SELECT 3, 100, NULL, 70, 1, 0, N'Assigned To', N'SelectBox', N'AssignedTo', NULL, 5, 2, 2, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, NULL, 1880, NULL, 0 UNION ALL
SELECT 3, 101, NULL, 70, 1, 0, N'Order Status', N'SelectBox', N'OrderStatus', NULL, 6, 13, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, NULL, 1881, NULL, 0 UNION ALL
SELECT 3, 103, NULL, 70, 1, 0, N'Conclusion Reason', N'SelectBox', N'ConAnalysis', NULL, 8, 7, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, NULL, 1883, NULL, 0 UNION ALL
SELECT 3, 104, NULL, 70, 0, 0, N'Order Sub-total', N'TextBox', N'CalcAmount', NULL, 9, 9, 2, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, NULL, 1884, NULL, 0 UNION ALL
SELECT 3, 105, NULL, 70, 1, 0, N'Active', N'CheckBox', N'Active', NULL, 10, 8, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, NULL, 1885, NULL, 0 UNION ALL
SELECT 3, 108, NULL, 70, 1, 0, N'Estimated close date', N'DateField', N'EstimatedCloseDate', NULL, 13, 4, 1, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, NULL, 1888, NULL, 0 UNION ALL
SELECT 3, 109, NULL, 70, 0, 0, N'Deal Status', N'SelectBox', N'DealStatus', NULL, 14, 15, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, NULL, 1889, NULL, 0 UNION ALL
SELECT 3, 111, NULL, 70, 1, 0, N'Record Owner', N'SelectBox', NULL, NULL, 16, NULL, NULL, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, NULL, 1891, NULL, 0 UNION ALL
SELECT 3, 112, NULL, 70, 0, 0, N'Assigned By', N'SelectBox', NULL, NULL, 17, NULL, NULL, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, NULL, 1892, NULL, 0 UNION ALL
SELECT 3, 116, NULL, 70, 1, 0, N'Campaign', N'SelectBox', NULL, NULL, 1, 1, 1, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, NULL, 0, NULL, 0 UNION ALL
SELECT 3, 117, NULL, 70, 1, 0, N'Closing Date', N'DateField', NULL, NULL, 22, NULL, NULL, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, NULL, 1897, NULL, 0 UNION ALL
SELECT 3, 118, NULL, 70, 0, 0, N'Opp Type', N'SelectBox', NULL, NULL, 23, NULL, NULL, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, NULL, 1898, NULL, 0 UNION ALL
SELECT 3, 119, NULL, 70, 0, 0, N'Created Date', N'DateField', NULL, NULL, 24, NULL, NULL, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, NULL, 1899, NULL, 0 UNION ALL
SELECT 3, 122, NULL, 70, 1, 0, N'Comments', N'TextArea', N'OppComments', NULL, 28, 27, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, NULL, 1903, NULL, 0 UNION ALL
SELECT 3, 230, NULL, 70, 0, 0, N'Sales Type', N'SelectBox', N'SalesorPurType', NULL, 3, 6, 2, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, NULL, 1878, NULL, 0 UNION ALL
SELECT 3, 268, NULL, 70, 0, 0, N'Invoice Grand-total', N'TextBox', N'InvoiceGrandTotal', NULL, 31, 31, 1, NULL, NULL, NULL, 0, 0, 0, 0, 1, 0, 0, 0, NULL, NULL, NULL, 0 UNION ALL
SELECT 3, 269, NULL, 70, 0, 0, N'Total Amount Paid', N'TextBox', N'TotalAmountPaid', NULL, 32, 31, 2, NULL, NULL, NULL, 0, 0, 0, 0, 1, 0, 0, 0, NULL, NULL, NULL, 0 UNION ALL
SELECT 3, 448, NULL, 70, 1, 0, N'Customer PO#', N'TextBox', N'OppRefOrderNo', NULL, 15, 13, 2, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, NULL, 1890, NULL, 0 UNION ALL
SELECT 3, 464, NULL, 70, 0, 0, N'Marketplace Order ID', N'TextBox', N'MarketplaceOrderID', N'', 1, 1, 2, NULL, NULL, NULL, 0, 0, 0, 0, 1, 0, 0, 1, NULL, NULL, NULL, 0




/*******************Manish Script******************/
/******************************************************************/
/******************************************************************/
