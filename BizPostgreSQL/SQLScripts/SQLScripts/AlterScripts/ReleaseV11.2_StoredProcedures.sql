/******************************************************************
Project: Release 11.2 Date: 02.MAR.2019
Comments: STORE PROCEDURES
*******************************************************************/

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='tf'AND NAME ='fn_GetItemPriceBasedOnMethod')
DROP FUNCTION fn_GetItemPriceBasedOnMethod
GO
CREATE FUNCTION [dbo].[fn_GetItemPriceBasedOnMethod] 
(
    @numDomainID NUMERIC(18,0)
	,@numDivisionID NUMERIC(18,0)
	,@numItemCode NUMERIC(18,0)
	,@numQty FLOAT
)
RETURNS @ItemPrice TABLE
(
	monPrice DECIMAL(20,5)
	,tintDisountType TINYINT
	,decDiscount FLOAT
	,monFinalPrice DECIMAL(20,5) 
)
AS 
BEGIN	
	DECLARE @monListPrice DECIMAL(20,5)
	DECLARE @monVendorCost DECIMAL(20,5)
	DECLARE @tintPriceLevel AS TINYINT
	DECLARE @tintRuleType AS TINYINT
	DECLARE @tintDisountType AS TINYINT
	DECLARE @decDiscount AS FLOAT
	DECLARE @monPrice AS DECIMAL(20,5) = NULL
	DECLARE @monFinalPrice AS DECIMAL(20,5) = 0
	DECLARE @numRelationship NUMERIC(18,0)
	DECLARE @numProfile NUMERIC(18,0)
	DECLARE @numItemClassification AS NUMERIC(18,0)
	DECLARE @tintPriceBookDiscount AS TINYINT
	DECLARE @tintPriceMethod TINYINT
	DECLARE @fltUOMConversionFactor INT = 1

	SELECT 
		@monListPrice = ISNULL(monListPrice,0)
		,@numItemClassification=numItemClassification
		,@monVendorCost=ISNULL(@monVendorCost,0)
	FROM 
		Item 
	LEFT JOIN
		Vendor
	ON
		Item.numVendorID = Vendor.numVendorID
		AND Item.numItemCode = Vendor.numItemCode
	WHERE
		Item.numItemCode=@numItemCode

	SELECT 
		@tintPriceMethod=ISNULL(numDefaultSalesPricing,0)
		,@tintPriceBookDiscount=tintPriceBookDiscount 
	FROM 
		Domain 
	WHERE 
		numDomainId=@numDomainID

	-- GET ORGANIZATION DETAL
	SELECT 
		@numRelationship=numCompanyType,
		@numProfile=vcProfile,
		@tintPriceLevel = ISNULL(D.tintPriceLevel,0)
	FROM 
		DivisionMaster D                  
	JOIN 
		CompanyInfo C 
	ON 
		C.numCompanyId=D.numCompanyID                  
	WHERE 
		numDivisionID =@numDivisionID       

	IF @tintPriceMethod= 1 -- PRICE LEVEL
	BEGIN
		IF (SELECT COUNT(*) FROM PricingTable WHERE numItemCode=@numItemCode AND ISNULL(numPriceRuleID,0) = 0) > 0
		BEGIN
			SELECT
				@tintDisountType = 1,
				@decDiscount = 0,
				@tintRuleType = tintRuleType,
				@monPrice = (CASE tintRuleType
										WHEN 1 -- Deduct From List Price
										THEN
											CASE tintDiscountType 
												WHEN 1 -- PERCENT
												THEN (CASE WHEN @monListPrice > 0 THEN @monListPrice - (@monListPrice * (ISNULL(decDiscount,0)/100)) ELSE 0 END)
												WHEN 2 -- FLAT AMOUNT
												THEN (CASE WHEN @monListPrice > 0 THEN (CASE WHEN (@monListPrice * @numQty * @fltUOMConversionFactor) - ISNULL(decDiscount,0) > 0 THEN ((@monListPrice * @numQty * @fltUOMConversionFactor) - ISNULL(decDiscount,0)) / (@numQty * @fltUOMConversionFactor) ELSE 0 END) ELSE 0 END)
												WHEN 3 -- NAMED PRICE
												THEN ISNULL(decDiscount,0)
											END
										WHEN 2 -- Add to primary vendor cost
										THEN
											CASE tintDiscountType 
												WHEN 1  -- PERCENT
												THEN @monVendorCost + (@monVendorCost * (ISNULL(decDiscount,0)/100)) * @fltUOMConversionFactor
												WHEN 2 -- FLAT AMOUNT
												THEN (CASE WHEN (@monVendorCost * @numQty * @fltUOMConversionFactor) + ISNULL(decDiscount,0) > 0 THEN ((@monVendorCost * @numQty * @fltUOMConversionFactor) + ISNULL(decDiscount,0)) / (@numQty * @fltUOMConversionFactor)  ELSE 0 END)
												WHEN 3 -- NAMED PRICE
												THEN ISNULL(decDiscount,0)
											END
										WHEN 3 -- Named price
										THEN
											CASE 
											WHEN ISNULL(@tintPriceLevel,0) > 0
											THEN
												(SELECT 
													 ISNULL(decDiscount,0)
												FROM
												(
													SELECT 
														ROW_NUMBER() OVER(PARTITION BY PricingTable.numItemCode ORDER BY numPricingID) Id,
														decDiscount
													FROM 
														PricingTable 
													WHERE 
														PricingTable.numItemCode = @numItemCode 
														AND tintRuleType = 3 
												) TEMP
												WHERE
													Id = @tintPriceLevel)
											ELSE
												ISNULL(decDiscount,0)
											END
										END
									) 
			FROM
				PricingTable
			WHERE
				numItemCode=@numItemCode AND (@numQty * @fltUOMConversionFactor) >= intFromQty AND (@numQty * @fltUOMConversionFactor) <= intToQty
		END

		INSERT INTO @ItemPrice
		(
			monPrice
			,tintDisountType
			,decDiscount
			,monFinalPrice
		)
		SELECT 
			ISNULL(@monPrice,@monListPrice) AS monPrice,
			1 AS tintDisountType,
			0 AS decDiscount,
			ISNULL(@monPrice,@monListPrice)
	END
	ELSE IF @tintPriceMethod= 2 AND (SELECT COUNT(*) FROM PriceBookRules WHERE numDomainID=@numDomainID AND tintRuleFor=1) > 0 -- PRICE RULE
	BEGIN
		DECLARE @numPriceRuleID NUMERIC(18,0)
		DECLARE @tintPriceRuleType AS TINYINT
		DECLARE @tintPricingMethod TINYINT
		DECLARE @tintPriceBookDiscountType TINYINT
		DECLARE @decPriceBookDiscount FLOAT
		DECLARE @intQntyItems INT
		DECLARE @decMaxDedPerAmt FLOAT

		SELECT 
			TOP 1
			@numPriceRuleID = numPricRuleID,
			@tintPricingMethod = tintPricingMethod,
			@tintPriceRuleType = p.tintRuleType,
			@tintPriceBookDiscountType = P.[tintDiscountType],
			@decPriceBookDiscount = P.[decDiscount],
			@intQntyItems = P.[intQntyItems],
			@decMaxDedPerAmt = P.[decMaxDedPerAmt]
		FROM   
			PriceBookRules P 
		LEFT JOIN 
			PriceBookRuleDTL PDTL 
		ON 
			P.numPricRuleID = PDTL.numRuleID
		LEFT JOIN 
			PriceBookRuleItems PBI 
		ON 
			P.numPricRuleID = PBI.numRuleID
		LEFT JOIN 
			[PriceBookPriorities] PP 
		ON 
			PP.[Step2Value] = P.[tintStep2] 
			AND PP.[Step3Value] = P.[tintStep3]
		WHERE  
			P.numDomainID=@numDomainID 
			AND tintRuleFor=1
			AND (
					((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 1
					OR ((tintStep2 = 2 AND PBI.numValue = @numItemClassification) AND (tintStep3= 1 AND PDTL.numValue = @numDivisionID)) -- Priority 2
					OR ((tintStep2 = 3) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 3

					OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 4
					OR ((tintStep2 = 2 AND PBI.numValue = @numItemClassification) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 5
					OR ((tintStep2 = 3) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 6

					OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 3)) -- Priority 7
					OR ((tintStep2 = 2 AND PBI.numValue = @numItemClassification) AND (tintStep3 = 3)) -- Priority 8
					OR ((tintStep2 = 3) and (tintStep3 = 3)) -- Priority 9
				)
		ORDER BY 
			PP.Priority ASC


		IF ISNULL(@numPriceRuleID,0) > 0
		BEGIN
			IF @tintPricingMethod = 1 -- BASED ON PRICE TABLE
			BEGIN
				SELECT
					@tintDisountType = tintDiscountType,
					@decDiscount = ISNULL(decDiscount,0),
					@tintRuleType = tintRuleType,
					@monPrice = (CASE tintRuleType WHEN 1 THEN @monListPrice WHEN 2 THEN @monVendorCost END) ,
					@monFinalPrice = (CASE tintRuleType
											WHEN 1 -- Deduct From List Price
											THEN
												CASE tintDiscountType 
													WHEN 1 -- PERCENT
													THEN (CASE WHEN @monListPrice > 0 THEN @monListPrice - (@monListPrice * (ISNULL(decDiscount,0)/100)) ELSE 0 END)
													WHEN 2 -- FLAT AMOUNT
													THEN (CASE WHEN @monListPrice > 0 THEN (CASE WHEN (@monListPrice * @numQty * @fltUOMConversionFactor) - ISNULL(decDiscount,0) > 0 THEN ((@monListPrice * @numQty * @fltUOMConversionFactor) - ISNULL(decDiscount,0)) / (@numQty * @fltUOMConversionFactor) ELSE 0 END) ELSE 0 END)
													WHEN 3 -- NAMED PRICE
													THEN ISNULL(decDiscount,0)
												END
											WHEN 2 -- Add to primary vendor cost
											THEN
												CASE tintDiscountType 
													WHEN 1  -- PERCENT
													THEN @monVendorCost + (@monVendorCost * (ISNULL(decDiscount,0)/100))
													WHEN 2 -- FLAT AMOUNT
													THEN (CASE WHEN (@monVendorCost * @numQty * @fltUOMConversionFactor) + ISNULL(decDiscount,0) > 0 THEN ((@monVendorCost * @numQty * @fltUOMConversionFactor) + ISNULL(decDiscount,0)) / (@numQty * @fltUOMConversionFactor)  ELSE 0 END)
													WHEN 3 -- NAMED PRICE
													THEN ISNULL(decDiscount,0)
												END
											WHEN 3 -- Named price
											THEN
												CASE 
												WHEN ISNULL(@tintPriceLevel,0) > 0
												THEN
													(SELECT 
														 ISNULL(decDiscount,0)
													FROM
													(
														SELECT 
															ROW_NUMBER() OVER(PARTITION BY PricingTable.numItemCode ORDER BY numPricingID) Id,
															decDiscount
														FROM 
															PricingTable 
														WHERE 
															PricingTable.numItemCode = @numItemCode 
															AND tintRuleType = 3 
													) TEMP
													WHERE
														Id = @tintPriceLevel)
												ELSE
													ISNULL(decDiscount,0)
												END
											END
										) 
				FROM
					PricingTable
				WHERE
					numPriceRuleID=@numPriceRuleID AND @numQty * @fltUOMConversionFactor >= intFromQty AND @numQty * @fltUOMConversionFactor <= intToQty
			END
			ELSE IF @tintPricingMethod = 2 -- BASED ON RULE
			BEGIN
				SET @monPrice = (CASE WHEN @tintPriceRuleType = 2 THEN @monVendorCost ELSE @monListPrice END)

				IF (@tintPriceBookDiscountType = 1 ) -- Percentage 
				BEGIN
					SET @tintDisountType = @tintPriceBookDiscountType
					SET @decDiscount = @decPriceBookDiscount * (@numQty/@intQntyItems);
					SET @decDiscount = @decPriceBookDiscount * (CASE WHEN @tintPriceRuleType = 2 THEN @monVendorCost ELSE @monListPrice END) /100;
					SET @decMaxDedPerAmt = @decMaxDedPerAmt * (CASE WHEN @tintPriceRuleType = 2 THEN @monVendorCost ELSE @monListPrice END) /100;
					
					IF (@decDiscount > @decMaxDedPerAmt)
						SET @decDiscount = @decMaxDedPerAmt;
				
					IF ( @tintPriceRuleType = 1 )
							SET @monFinalPrice = (@monListPrice - @decDiscount);
					IF ( @tintPriceRuleType = 2 )
							SET @monFinalPrice = (@monVendorCost + @decDiscount);
				END
				IF ( @tintPriceBookDiscountType = 2 ) -- Flat discount 
				BEGIN
					SET @decDiscount = @decPriceBookDiscount * ((@numQty * @fltUOMConversionFactor)/@intQntyItems);

					IF (@decDiscount > @decMaxDedPerAmt)
						SET @decDiscount = @decMaxDedPerAmt;
					
					IF ( @tintPriceRuleType = 1 )
						SET @monFinalPrice =  ((@monListPrice * @numQty * @fltUOMConversionFactor) - @decDiscount) / (@numQty * @fltUOMConversionFactor);
						
					IF ( @tintPriceRuleType = 2 )
						SET @monFinalPrice = ((@monVendorCost * @numQty * @fltUOMConversionFactor) + @decDiscount) / (@numQty * @fltUOMConversionFactor);
				END
			END
		END
		
		INSERT INTO @ItemPrice
		(
			monPrice
			,tintDisountType
			,decDiscount
			,monFinalPrice
		)
		SELECT 
			(CASE WHEN @monFinalPrice IS NULL THEN @monListPrice ELSE (CASE WHEN @tintPriceBookDiscount = 1 THEN ISNULL(@monPrice,0) ELSE ISNULL(@monFinalPrice,0) END) END),
			(CASE WHEN @monFinalPrice IS NULL THEN 1 ELSE (CASE WHEN @tintPriceBookDiscount = 1 THEN ISNULL(@tintPriceBookDiscountType,1) ELSE 1 END) END),
			(CASE WHEN @monFinalPrice IS NULL THEN 0 ELSE (CASE WHEN @tintPriceBookDiscount = 1 THEN ISNULL(@decPriceBookDiscount,0) ELSE 0 END) END),
			ISNULL(@monFinalPrice,0)
	END
	ELSE IF @tintPriceMethod = 3 -- Last Price
	BEGIN
		SELECT 
			TOP 1 
			@monPrice = monPrice
			,@tintDisountType = ISNULL(OpportunityItems.bitDiscountType,1)
			,@decDiscount = ISNULL(OpportunityItems.fltDiscount,0)
			,@monFinalPrice = (CASE 
								WHEN ISNULL(OpportunityItems.fltDiscount,0) > 0 AND ISNULL(numUnitHour,0) > 0 
								THEN (CASE 
										WHEN ISNULL(OpportunityItems.bitDiscountType,1)=1 
										THEN CAST(((ISNULL(monTotAmtBefDiscount,0) - ISNULL(monTotAmount,0))/numUnitHour) AS DECIMAL(20,5))
										ELSE CAST(monPrice - (monPrice * (ISNULL(OpportunityItems.fltDiscount,0)/100)) AS DECIMAL(20,5)) END) 
								ELSE monPrice 
								END)
			,@tintRuleType = 3
		FROM 
			OpportunityItems 
		JOIN 
			OpportunityMaster 
		ON 
			OpportunityItems.numOppId = OpportunityMaster.numOppId 
		WHERE 
			OpportunityMaster.numDomainId= @numDomainID
			AND tintOppType = 1
			AND tintOppStatus = 1
			AND numItemCode=@numItemCode
		ORDER BY
			OpportunityMaster.numOppId DESC

		INSERT INTO @ItemPrice
		(
			monPrice
			,tintDisountType
			,decDiscount
			,monFinalPrice
		)
		VALUES
		( 
			ISNULL(@monPrice,0),
			@tintDisountType,
			@decDiscount,
			@monFinalPrice
		)
	END
	ELSE
	BEGIN
		INSERT INTO @ItemPrice
		(
			monPrice
			,tintDisountType
			,decDiscount
			,monFinalPrice
		)
		VALUES
		( 
			ISNULL(@monListPrice,0),
			1,
			0,
			ISNULL(@monListPrice,0)
		)
	END

	RETURN
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='tf'AND NAME ='fn_GetItemPromotions')
DROP FUNCTION fn_GetItemPromotions
GO
CREATE FUNCTION [dbo].[fn_GetItemPromotions] 
(
    @numDomainID NUMERIC(18,0)
	,@numDivisionID NUMERIC(18,0)
	,@numItemCode NUMERIC(18,0)
	,@numItemClassification NUMERIC(18,0)
	,@numRelationship NUMERIC(18,0)
	,@numProfile NUMERIC(18,0)
	,@tintMode TINYINT
)
RETURNS @ItemPromotion TABLE
(
	numTotalPromotions INT
	,vcPromoDesc VARCHAR(MAX)
	,bitRequireCouponCode BIT
)
AS 
BEGIN
	INSERT INTO @ItemPromotion
	(
		numTotalPromotions
		,vcPromoDesc
		,bitRequireCouponCode
	)
	SELECT TOP 1
		COUNT(PO.numProId) OVER() numTotalPromotions
		,CONCAT((CASE WHEN COUNT(PO.numProId) OVER() > 1 THEN '' ELSE ISNULL((CASE WHEN @tintMode=2 THEN PO.vcLongDesc ELSE PO.vcShortDesc END),'-') END),(CASE WHEN ISNULL(PO.numOrderPromotionID,0) > 0 THEN ' <br/><i>(coupon code required)</i>' ELSE '' END)) vcShortDesc
		,(CASE WHEN ISNULL(PO.numOrderPromotionID,0) > 0 THEN 1 ELSE 0 END) bitRequireCouponCode
	FROM 
		PromotionOffer PO
	LEFT JOIN
		PromotionOffer POOrder
	ON
		PO.numOrderPromotionID = POOrder.numProId
	WHERE 
		PO.numDomainId=@numDomainID 
		AND ISNULL(PO.bitEnabled,0)=1 
		AND ISNULL(PO.IsOrderBasedPromotion,0) = 0
		AND 1 = (CASE 
					WHEN ISNULL(PO.numOrderPromotionID,0) > 0 
					THEN (CASE WHEN POOrder.bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN GETUTCDATE()>=POOrder.dtValidFrom AND GETUTCDATE()<=POOrder.dtValidTo THEN 1 ELSE 0 END) END)
					ELSE (CASE WHEN PO.bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN GETUTCDATE()>=PO.dtValidFrom AND GETUTCDATE()<=PO.dtValidTo THEN 1 ELSE 0 END) END)
				END)
		AND 1 = (CASE 
					WHEN ISNULL(PO.numOrderPromotionID,0) > 0 
					THEN
						(CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numOrderPromotionID AND tintType=2 AND numRelationship=@numRelationship AND numProfile=@numProfile) > 0 THEN 1 ELSE 0 END)
					ELSE
						(CASE PO.tintCustomersBasedOn 
							WHEN 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numProId AND tintType=1 AND numDivisionID=@numDivisionID) > 0 THEN 1 ELSE 0 END)
							WHEN 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numProId AND tintType=2 AND numRelationship=@numRelationship AND numProfile=@numProfile) > 0 THEN 1 ELSE 0 END)
							WHEN 3 THEN 1
							ELSE 0
						END)
				END)
		AND 1 = (CASE 
					WHEN PO.tintOfferBasedOn = 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 AND numValue=@numItemCode) > 0 THEN 1 ELSE 0 END)
					WHEN PO.tintOfferBasedOn = 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=2 AND numValue=@numItemClassification) > 0 THEN 1 ELSE 0 END)
					WHEN PO.tintOfferBasedOn = 4 THEN 1 
					ELSE 0
				END)
	ORDER BY
		CASE 
			WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 1 THEN 1
			WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 1 THEN 2
			WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 2 THEN 3
			WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 2 THEN 4
			WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 3 THEN 5
			WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 3 THEN 6
		END

	RETURN
END
GO
/****** Object:  StoredProcedure [dbo].[usp_DelBusinessProcess]    Script Date: 07/26/2008 16:15:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--modified by anoop  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_delbusinessprocess')
DROP PROCEDURE usp_delbusinessprocess
GO
CREATE PROCEDURE [dbo].[usp_DelBusinessProcess]  
@slpid numeric     
AS  

DELETE from StagePercentageDetailsTask where numStageDetailsId IN(SELECT numStageDetailsId from stagepercentagedetails where slp_id=@slpid)
DELETE from stagepercentagedetails where slp_id=@slpid
delete from Sales_process_List_Master where slp_id=@slpid
	
SELECT '1'


GO

/****** Object:  StoredProcedure [dbo].[USP_DeleteCartItem]    Script Date: 11/08/2011 17:28:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_DeleteCartItem' ) 
                    DROP PROCEDURE USP_DeleteCartItem
                    Go
CREATE PROCEDURE [dbo].[USP_DeleteCartItem]
(
    @numUserCntId NUMERIC(18, 0),
    @numDomainId NUMERIC(18, 0),
    @vcCookieId VARCHAR(100),
    @numCartId NUMERIC(18,0) = 0,
    @bitDeleteAll BIT=0,
	@numSiteID NUMERIC(18,0) = 0,
	@numItemCode NUMERIC(18,0) = 0
)
AS 
BEGIN
	IF @bitDeleteAll = 0
	BEGIN
		IF @numCartId <> 0 
		BEGIN
			-- REMOVE DISCOUNT APPLIED TO ALL ITEMS FROM PROMOTION TRIGGERED OF ITEM TO BE DELETED
			UPDATE 
				CartItems
			SET 
				PromotionDesc='',PromotionID=0,fltDiscount=0,monTotAmount=(monPrice*numUnitHour),monTotAmtBefDiscount=(monPrice*numUnitHour),vcCoupon=NULL,bitParentPromotion=0
			WHERE
				PromotionID=(SELECT TOP 1 PromotionID FROM CartItems WHERE numDomainId = @numDomainId AND numUserCntId = @numUserCntId AND numCartId = @numCartId AND vcCookieId=@vcCookieId AND ISNULL(bitParentPromotion,0)=1)
				AND numDomainId = @numDomainId
				AND numUserCntId = @numUserCntId
				AND vcCookieId=@vcCookieId
				AND ISNULL(bitParentPromotion,0)=0

			--DELETE FROM CartItems WHERE numDomainId = @numDomainId AND numUserCntId = @numUserCntId AND numItemCode = @numItemCode	

			DECLARE @numTempItemCode NUMERIC(18,0)
			SELECT @numTempItemCode=numItemCode FROM CartItems WHERE numCartId=@numCartId

			DELETE FROM CartItems 
			WHERE numDomainId = @numDomainId AND numUserCntId = @numUserCntId
				AND (numCartId = @numCartId 
					OR numItemCode IN (SELECT numItemCode FROM SimilarItems WHERE numParentItemCode = @numTempItemCode AND numDomainId = @numDomainId ))	--AND bitrequired = 1
		END
		ELSE IF ISNULL(@numItemCode,0) > 0
		BEGIN
			-- REMOVE DISCOUNT APPLIED TO ALL ITEMS FROM PROMOTION TRIGGERED OF ITEM TO BE DELETED
			UPDATE 
				CartItems
			SET 
				PromotionDesc='',PromotionID=0,fltDiscount=0,monTotAmount=(monPrice*numUnitHour),monTotAmtBefDiscount=(monPrice*numUnitHour),vcCoupon=NULL,bitParentPromotion=0
			WHERE
				PromotionID=(SELECT TOP 1 PromotionID FROM CartItems WHERE numDomainId = @numDomainId AND numUserCntId = @numUserCntId AND numItemCode = @numItemCode AND vcCookieId=@vcCookieId AND ISNULL(bitParentPromotion,0)=1)
				AND numDomainId = @numDomainId
				AND numUserCntId = @numUserCntId
				AND vcCookieId=@vcCookieId
				AND ISNULL(bitParentPromotion,0)=0

			DELETE FROM CartItems WHERE numDomainId = @numDomainId AND numUserCntId = @numUserCntId AND vcCookieId = @vcCookieId AND numItemCode=@numItemCode
		END
		ELSE
		BEGIN
			UPDATE 
				CartItems
			SET 
				PromotionDesc='',PromotionID=0,fltDiscount=0,monTotAmount=(monPrice*numUnitHour),monTotAmtBefDiscount=(monPrice*numUnitHour),vcCoupon=NULL,bitParentPromotion=0
			WHERE
				numDomainId = @numDomainId
				AND numUserCntId = @numUserCntId
				AND vcCookieId=@vcCookieId

			DELETE FROM CartItems 
			WHERE numDomainId = @numDomainId AND numUserCntId = 0 AND vcCookieId = @vcCookieId		
		END
	END
	ELSE
	BEGIN
		DELETE FROM CartItems WHERE numDomainId = @numDomainId AND numUserCntId = @numUserCntId	AND vcCookieId <> ''
	END

	EXEC USP_PromotionOffer_ApplyItemPromotionToECommerce @numDomainID,@numUserCntId,@numSiteID,@vcCookieId,0,''
END
GO
 



--modified by anoop jayaraj              
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_deleteoppurtunity')
DROP PROCEDURE usp_deleteoppurtunity
GO
CREATE PROCEDURE [dbo].[USP_DeleteOppurtunity]
 @numOppId numeric(9) ,        
 @numDomainID as numeric(9),
 @numUserCntID AS NUMERIC(9)                       
AS                          
             
       
if exists(select * from OpportunityMaster where numOppID=@numOppId and numDomainID=@numDomainID)         
BEGIN 
	DECLARE @tintCommitAllocation AS TINYINT
	DECLARE @bitAllocateInventoryOnPickList AS BIT

	SELECT @tintCommitAllocation=ISNULL(tintCommitAllocation,1) FROM Domain WHERE numDomainId=@numDomainID
	SELECT @bitAllocateInventoryOnPickList=ISNULL(bitAllocateInventoryOnPickList,0) FROM OpportunityMaster INNER JOIN DivisionMaster ON OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID WHERE numOppID=@numOppID

SET XACT_ABORT ON;

BEGIN TRY 
BEGIN TRAN

	IF (SELECT COUNT(*) FROM MassSalesOrderQueue WHERE numOppID = @numOppId AND ISNULL(bitExecuted,0) = 0) > 0
	BEGIN
		RAISERROR ( 'MASS SALES ORDER', 16, 1 )
		RETURN
	END
	IF (SELECT COUNT(*) FROM RecurrenceConfiguration WHERE numOppID = @numOppId AND numDomainID=@numDomainID AND ISNULL(bitMarkForDelete,0) = 0) > 0
	BEGIN
		RAISERROR ( 'RECURRING ORDER OR BIZDOC', 16, 1 )
		RETURN
	END
	ELSE IF (SELECT COUNT(*) FROM RecurrenceTransaction INNER JOIN RecurrenceConfiguration ON RecurrenceTransaction.numRecConfigID = RecurrenceConfiguration.numRecConfigID WHERE RecurrenceTransaction.numRecurrOppID = @numOppId AND ISNULL(RecurrenceConfiguration.bitDisabled,0) = 0) > 0
	BEGIN	
		RAISERROR ( 'RECURRED ORDER', 16, 1 ) ;
		RETURN ;
	END
	ELSE IF (SELECT COUNT(*) FROM OpportunityBizdocs WHERE numOppId=@numOppId AND numBizDocId=296 AND ISNULL(bitFulfilled,0) = 1) > 0
	BEGIN
		RAISERROR ( 'FULFILLED_ITEMS', 16, 1 ) ;
		RETURN ;
	END

      IF EXISTS ( SELECT  *
                FROM    dbo.CaseOpportunities
                WHERE   numOppID = @numOppId
                        AND numDomainID = @numDomainID )         
        BEGIN
            RAISERROR ( 'CASE DEPENDANT', 16, 1 ) ;
            RETURN ;
        END	
	 IF EXISTS ( SELECT  *
			FROM    dbo.ReturnHeader
			WHERE   numOppId = @numOppId
					AND numDomainId= @numDomainID )         
	BEGIN
		RAISERROR ( 'RETURN_EXIST', 16, 1 ) ;
		RETURN ;
	END	

      
      
      

DECLARE @tintError TINYINT;SET @tintError=0

EXECUTE USP_CheckCanbeDeleteOppertunity @numOppId,@tintError OUTPUT
  
  IF @tintError=1
  BEGIN
  	 RAISERROR ( 'OppItems DEPENDANT', 16, 1 ) ;
     RETURN ;
  END
  
  EXEC dbo.USP_ValidateFinancialYearClosingDate
	@numDomainID = @numDomainID, --  numeric(18, 0)
	@tintRecordType = 2, --  tinyint
	@numRecordID = @numOppId --  numeric(18, 0)
	
  --Credit Balance
   Declare @monCreditAmount as DECIMAL(20,5);Set @monCreditAmount=0
   Declare @monCreditBalance as DECIMAL(20,5);Set @monCreditBalance=0
   Declare @numDivisionID as numeric(9);Set @numDivisionID=0
   Declare @tintOppType as tinyint

   Select @numDivisionID=numDivisionID,@tintOppType=tintOppType from OpportunityMaster WHERE  numOppId=@numOppId  and numDomainID= @numDomainID 
   
    IF @tintOppType=1 --Sales
      Select @monCreditBalance=isnull(monSCreditBalance,0) from DivisionMaster WHERE  numDivisionID=@numDivisionID  and numDomainID= @numDomainID 
    else IF @tintOppType=2 --Purchase
		UPDATE OpportunityMaster SET numReleaseStatus = 1 WHERE numDomainId=@numDomainID AND numOppId IN (SELECT DISTINCT numOppId FROM OpportunityItemsReleaseDates WHERE  numPurchasedOrderID = @numOppId)
		UPDATE OpportunityItemsReleaseDates SET tintStatus = 1, numPurchasedOrderID=0 WHERE numPurchasedOrderID = @numOppId


      Select @monCreditBalance=isnull(monPCreditBalance,0) from DivisionMaster WHERE  numDivisionID=@numDivisionID  and numDomainID= @numDomainID 
   
   Select @monCreditAmount=sum(monAmount) from CreditBalanceHistory where numReturnID in (select numReturnID from [Returns] WHERE [numOppId] = @numOppId)
	
	IF ( @monCreditAmount > @monCreditBalance )         
        BEGIN
            RAISERROR ( 'CreditBalance DEPENDANT', 16, 1 ) ;
            RETURN ;
        END	
        
  declare @tintOppStatus as tinyint    
 declare @tintShipped as tinyint                
  select @tintOppStatus=tintOppStatus,@tintShipped=tintShipped from OpportunityMaster where numOppID=@numOppId                
  
  DECLARE @isOrderItemsAvailable AS INT = 0

	SELECT 
		@isOrderItemsAvailable = COUNT(*) 
	FROM    
		OpportunityItems OI
	JOIN 
		dbo.OpportunityMaster OM ON OI.numOppId = OM.numOppId
	JOIN 
		Item I ON OI.numItemCode = I.numItemCode
	WHERE   
		(charitemtype='P' OR 1=(CASE WHEN tintOppType=1 THEN 
								CASE WHEN ISNULL(I.bitKitParent,0) = 1 AND ISNULL(I.bitAssembly,0) = 1 THEN 0
									 WHEN ISNULL(I.bitKitParent,0) = 1 THEN 1
									 ELSE 0 END 
								ELSE 0 END)) AND OI.numOppId = @numOppId
							   AND ( bitDropShip = 0
									 OR bitDropShip IS NULL
								   ) 

  if ((@tintOppStatus=1 and @tintShipped=0) OR  @tintShipped=1) AND @tintCommitAllocation <> 2            
  begin    
    exec USP_RevertDetailsOpp @numOppId,1,@numUserCntID                
  end                                 
                        
  DELETE FROM [OpportunitySalesTemplate] WHERE [numOppId] =@numOppId 
  delete OpportunityLinking where   numChildOppID=  @numOppId or   numParentOppID=    @numOppId          
  delete RECENTITEMS where numRecordID =  @numOppId and chrRecordType='O'             
                

	IF @tintOppType = 1 AND @tintOppStatus = 1 -- SALES ORDER
	BEGIN
		-- MAKE SERIAL/LOT# NUUMBER AVAILABLE FOR USE AS SALES ORDER IS DELETED
		UPDATE WHIDL
			SET WHIDL.numQty = (CASE WHEN Item.bitLotNo = 1 THEN ISNULL(WHIDL.numQty,0) + ISNULL(OWSI.numQty,0) ELSE 1 END)
		FROM 
			WareHouseItmsDTL WHIDL
		INNER JOIN
			OppWarehouseSerializedItem OWSI
		ON
			WHIDL.numWareHouseItmsDTLID = OWSI.numWarehouseItmsDTLID
			AND WHIDL.numWareHouseItemID = OWSI.numWarehouseItmsID
		INNER JOIN
			WareHouseItems
		ON
			WHIDL.numWareHouseItemID = WareHouseItems.numWareHouseItemID
		INNER JOIN
			Item
		ON
			WareHouseItems.numItemID = Item.numItemCode
		WHERE
			OWSI.numOppID = @numOppId
	END
	ELSE IF @tintOppType = 2 AND @tintOppStatus = 1 -- PURCHASE ORDER
	BEGIN
		IF (
			SELECT 
				COUNT(*)
			FROM 
				OppWarehouseSerializedItem OWSI 
			INNER JOIN 
				OpportunityMaster OM 
			ON 
				OWSI.numOppID=OM.numOppId 
				AND tintOppType=1 
			WHERE 
				OWSI.numWarehouseItmsDTLID IN (SELECT numWarehouseItmsDTLID FROM OppWarehouseSerializedItem OWSIInner WHERE OWSIInner.numOppID = @numOppID)
			) > 0
		BEGIN
			RAISERROR ('SERIAL/LOT#_USED', 16, 1 ) ;
		END
		ELSE
		BEGIN
			-- REMOVE SERIAL/LOT# NUMBER FROM INVENTORY BECAUSE PURCHASE ORDER IS DELETED
			UPDATE WHIDL
				SET WHIDL.numQty = (CASE WHEN Item.bitLotNo = 1 THEN ISNULL(WHIDL.numQty,0) - ISNULL(OWSI.numQty,0) ELSE 0 END)
			FROM 
				WareHouseItmsDTL WHIDL
			INNER JOIN
				OppWarehouseSerializedItem OWSI
			ON
				WHIDL.numWareHouseItmsDTLID = OWSI.numWarehouseItmsDTLID
				AND WHIDL.numWareHouseItemID = OWSI.numWarehouseItmsID
			INNER JOIN
				WareHouseItems
			ON
				WHIDL.numWareHouseItemID = WareHouseItems.numWareHouseItemID
			INNER JOIN
				Item
			ON
				WareHouseItems.numItemID = Item.numItemCode
			WHERE
				OWSI.numOppID = @numOppId
		END
	END
									        
  delete from OppWarehouseSerializedItem where numOppID=  @numOppId                  
                  
                        
  delete from OpportunityItemLinking where numNewOppID=@numOppId                      
                         
  delete from OpportunityAddress  where numOppID=@numOppId


  DELETE FROM OpportunityBizDocKitItems WHERE [numOppBizDocItemID] IN 
  (SELECT [numOppBizDocItemID] FROM [OpportunityBizDocItems] WHERE [numOppBizDocID] IN 
	(select numOppBizDocsId from OpportunityBizDocs where numOppId=@numOppId))
	
  delete from OpportunityBizDocItems where numOppBizDocID in                        
  (select numOppBizDocsId from OpportunityBizDocs where numOppId=@numOppId)

  delete from OpportunityItemsTaxItems WHERE numOppID=@numOppID
  delete from OpportunityMasterTaxItems WHERE numOppID=@numOppID
  
  delete from OpportunityBizDocDtl where numBizDocID in                        
  (select numOppBizDocsId from OpportunityBizDocs where numOppId=@numOppId)                        
           
   DELETE FROM OpportunityBizDocsPaymentDetails WHERE [numBizDocsPaymentDetId] IN 
	 (SELECT [numBizDocsPaymentDetId] FROM [OpportunityBizDocsDetails] where numBizDocsId in                        
		(select numOppBizDocsId from OpportunityBizDocs where numOppId=@numOppId)) 
               
  delete from OpportunityBizDocsDetails where numBizDocsId in                        
  (select numOppBizDocsId from OpportunityBizDocs where numOppId=@numOppId)  

DELETE FROM [ShippingReportItems] WHERE [numShippingReportId] IN  (SELECT numShippingReportId from [ShippingReport] WHERE [numOppBizDocId] IN (select numOppBizDocsId from OpportunityBizDocs where numOppId=@numOppId))
DELETE FROM [ShippingBox] WHERE [numShippingReportId] IN  (SELECT numShippingReportId from [ShippingReport] WHERE [numOppBizDocId] IN (select numOppBizDocsId from OpportunityBizDocs where numOppId=@numOppId))
DELETE FROM [ShippingReport] WHERE [numOppBizDocId] IN (select numOppBizDocsId from OpportunityBizDocs where numOppId=@numOppId)

delete from DocumentWorkflow where numDocID in 
(select numOppBizDocsId from OpportunityBizDocs where numOppId=@numOppId) and cDocType='B'      

delete from BizDocAction where numBizActionId in(select numBizActionId from BizActionDetails where numOppBizDocsId in
(select numOppBizDocsId from OpportunityBizDocs where numOppId=@numOppId) and btDocType=1)

delete from BizActionDetails where numOppBizDocsId in
(select numOppBizDocsId from OpportunityBizDocs where numOppId=@numOppId) and btDocType=1

  delete from OpportunityBizDocs where numOppId=@numOppId                        
                          
  delete from OpportunityContact where numOppId=@numOppId                        
                          
  delete from OpportunityDependency where numOpportunityId=@numOppId                        
                          
  delete from TimeAndExpense where numOppId=@numOppId   and numDomainID= @numDomainID                     
  
  --Credit Balance
  delete from CreditBalanceHistory where numReturnID in (select numReturnID from [Returns] WHERE [numOppId] = @numOppId)

    IF @tintOppType=1 --Sales
		update DivisionMaster set monSCreditBalance=isnull(monSCreditBalance,0) - isnull(@monCreditAmount,0) where numDivisionID=@numDivisionID and numDomainID=@numDomainID
    else IF @tintOppType=2 --Purchase
		update DivisionMaster set monPCreditBalance=isnull(monPCreditBalance,0) - isnull(@monCreditAmount,0) where numDivisionID=@numDivisionID and numDomainID=@numDomainID
  
	UPDATE
		DCU
	SET 
		intCodeUsed = ISNULL(intCodeUsed,0) - 1
	FROM
		DiscountCodeUsage DCU
	INNER JOIN
		(
			SELECT
				numDivisionId
				,numDiscountID
			FROM
				OpportunityMaster
			WHERE
				OpportunityMaster.numOppId=@numOppId
		) AS T1
	ON
		DCU.numDiscountId = T1.numDiscountID
		AND DCU.numDivisionId = T1.numDivisionId

  DELETE FROM [Returns] WHERE [numOppId] = @numOppId
                         
  delete from OpportunityKitItems where numOppId=@numOppId                        
                          
  delete from OpportunityItems where numOppId=@numOppId                        
                          
  DELETE FROM dbo.ProjectProgress WHERE numOppId =@numOppId AND numDomainId=@numDomainID
                          
  delete from OpportunityStageDetails where numOppId=@numOppId
                          
  delete from OpportunitySubStageDetails where numOppId=@numOppId
                                               
  DELETE FROM [OpportunityRecurring] WHERE [numOppId] = @numOppId
  
  DELETE FROM [OpportunityMasterAPI] WHERE [numOppId] = @numOppId
  
  DELETE FROM [WebApiOrderDetails] WHERE [numOppId] = @numOppId
  
  DELETE FROM [WebApiOppItemDetails] WHERE [numOppId] = @numOppId
  
  /*
  added by chintan: reason: when bizdoc is deleted and journal entries are not deleted in exceptional case, 
  it throws Foreign key reference error when deleting order.
  */
  DELETE FROM dbo.General_Journal_Details WHERE numJournalId IN (SELECT numJournal_Id FROM dbo.General_Journal_Header WHERE numOppId=@numOppId AND numDomainId=@numDomainID)
  DELETE FROM dbo.General_Journal_Header WHERE numOppId=@numOppId AND numDomainId= @numDomainID
 
	IF @tintCommitAllocation = 2
	BEGIN
		DELETE FROM OpportunityMaster WHERE numOppId=@numOppId AND numDomainID= @numDomainID
	END
	ELSE
	BEGIN
		IF ISNULL(@tintOppStatus,0)=0 OR ISNULL(@isOrderItemsAvailable,0) = 0 OR (SELECT COUNT(*) FROM WareHouseItems_Tracking WHERE numDomainID=@numDomainID AND numReferenceID=@numOppId AND tintRefType IN (3,4) AND vcDescription LIKE '%Deleted%' AND CAST(dtCreatedDate AS DATE) = CAST(GETUTCDATE() AS DATE)) > 0
			DELETE FROM OpportunityMaster WHERE numOppId=@numOppId and numDomainID= @numDomainID             
		ELSE
			RAISERROR ('INVENTORY IM-BALANCE', 16, 1);
	END
COMMIT TRAN 

    END TRY 
    BEGIN CATCH		
        DECLARE @strMsg VARCHAR(200)
        SET @strMsg = ERROR_MESSAGE()
        IF ( @@TRANCOUNT > 0 ) 
            BEGIN
             RAISERROR ( @strMsg, 16, 1 ) ;
                ROLLBACK TRAN
                RETURN 1
            END
    END CATCH	

SET XACT_ABORT OFF;
END 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DiscountCodes_Validate')
DROP PROCEDURE USP_DiscountCodes_Validate
GO
CREATE PROCEDURE [dbo].[USP_DiscountCodes_Validate]
	@numDomainID NUMERIC(18,0)
	,@numSiteID NUMERIC(18,0)
	,@numDivisionId NUMERIC(18,0)
	,@txtCouponCode VARCHAR(100)
AS
BEGIN
	DECLARE @numRelationship NUMERIC(18,0) = 0
	DECLARE @numProfile NUMERIC(18,0) = 0

	IF ISNULL(@numDivisionID,0) > 0
	BEGIN
		SELECT 
			@numRelationship=ISNULL(numCompanyType,0)
			,@numProfile=ISNULL(vcProfile,0) 
		FROM 
			DivisionMaster 
		INNER JOIN 
			CompanyInfo 
		ON 
			DivisionMaster.numCompanyID=CompanyInfo.numCompanyId 
		WHERE 
			numDivisionID=@numDivisionID
	END
	ELSE IF ISNULL(@numSiteID,0) > 0
	BEGIN
		SELECT
			@numRelationship=ISNULL(numRelationshipId,0)
			,@numProfile=ISNULL(numProfileId,0)
		FROM
			eCommerceDTL
		WHERE
			numSiteID=@numSiteID
	END

	DECLARE @numPromotionID NUMERIC(18,0)
	DECLARE @bitOrderBasedPromotion AS BIT = 0
	DECLARE @bitPromotionNeverExpires BIT
	DECLARE @dtPromotionStartDate DATETIME
	DECLARE @dtPromotionEndDate DATETIME
	DECLARE @intUsageLimit INT
	DECLARE @numDisocuntID NUMERIC(18,0)
	DECLARE @bitUseForCouponManagement BIT

	-- FIRST CHECK WHETHER COUNPON CODE IS AVAILABLE
	SELECT 
		@numPromotionID=PO.numProId
		,@intUsageLimit=ISNULL(DC.CodeUsageLimit,0)
		,@bitPromotionNeverExpires=ISNULL(bitNeverExpires,0)
		,@dtPromotionStartDate=PO.dtValidFrom
		,@dtPromotionEndDate=PO.dtValidTo
		,@numDisocuntID=numDiscountId
		,@bitOrderBasedPromotion=ISNULL(PO.IsOrderBasedPromotion,0)
		,@bitUseForCouponManagement=ISNULL(bitUseForCouponManagement,0)
	FROM 
		PromotionOffer PO
	INNER JOIN 
		PromotionOfferOrganizations PORG
	ON 
		PO.numProId = PORG.numProId
	INNER JOIN 
		DiscountCodes DC
	ON 
		PO.numProId = DC.numPromotionID
	WHERE
		PO.numDomainId = @numDomainID
		AND 1 =(CASE WHEN numRelationship=@numRelationship AND numProfile=@numProfile THEN 1 ELSE 0 END)
		AND DC.vcDiscountCode = @txtCouponCode

	IF ISNULL(@numPromotionID,0) > 0
	BEGIN
		-- NOW CHECK IF PROMOTION EXPIRES OR NOT
		IF ISNULL(@bitPromotionNeverExpires,0) = 0
		BEGIN
			IF (@dtPromotionStartDate <= GETUTCDATE() AND @dtPromotionEndDate >= GETUTCDATE())
			BEGIN
				IF ISNULL(@intUsageLimit,0) <> 0
				BEGIN
					IF ISNULL((SELECT intCodeUsed FROM DiscountCodeUsage WHERE numDiscountId=@numDisocuntID AND numDivisionId=@numDivisionId),0) >= @intUsageLimit
					BEGIN
						RAISERROR('COUPON_USAGE_LIMIT_EXCEEDED',16,1)
						RETURN
					END
				END
			END
			ELSE
			BEGIN
				RAISERROR('COUPON_CODE_EXPIRED',16,1)
				RETURN
			END
		END
	END
	ELSE
	BEGIN
		RAISERROR('INVALID_COUPON_CODE',16,1)
		RETURN
	END

	SELECT 
		@numPromotionID AS numPromotionID
		,@numDisocuntID AS numDisocuntID
		,@txtCouponCode AS vcCouponCode
		,@bitOrderBasedPromotion AS bitOrderBasedPromotion
		,@bitUseForCouponManagement AS bitUseForCouponManagement
END
GO
/****** Object:  StoredProcedure [dbo].[USP_EcommerceSettings]    Script Date: 03/25/2009 16:46:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj              
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_ecommercesettings')
DROP PROCEDURE usp_ecommercesettings
GO
CREATE PROCEDURE [dbo].[USP_EcommerceSettings]              
@numDomainID as numeric(9),              
@vcPaymentGateWay as int ,                
@vcPGWUserId as varchar(100)='' ,                
@vcPGWPassword as varchar(100),              
@bitShowInStock as bit,              
@bitShowQOnHand as bit,
@bitCheckCreditStatus as BIT,
@numDefaultWareHouseID as numeric(9),
@numRelationshipId as numeric(9),
@numProfileId as numeric(9),
@bitHidePriceBeforeLogin	BIT,
@bitAuthOnlyCreditCard BIT=0,
@bitSendMail BIT = 1,
@vcGoogleMerchantID VARCHAR(1000)  = '',
@vcGoogleMerchantKey VARCHAR(1000) = '',
@IsSandbox BIT ,
@numAuthrizedOrderStatus NUMERIC,
@numSiteID numeric(18, 0),
@vcPaypalUserName VARCHAR(50)  = '',
@vcPaypalPassword VARCHAR(50)  = '',
@vcPaypalSignature VARCHAR(500)  = '',
@IsPaypalSandbox BIT,
@bitSkipStep2 BIT = 0,
@bitDisplayCategory BIT = 0,
@bitShowPriceUsingPriceLevel BIT = 0,
@bitEnableSecSorting BIT = 0,
@bitSortPriceMode   BIT=0,
@numPageSize    INT=15,
@numPageVariant  INT=6,
@bitAutoSelectWarehouse BIT =0,
@bitPreSellUp BIT = 0, 
@bitPostSellUp BIT = 0, 
@numDefaultClass NUMERIC(18,0) = 0,
@vcPreSellUp VARCHAR(200)=NULL,
@vcPostSellUp VARCHAR(200)=NULL,
@vcRedirectThankYouUrl VARCHAR(300)=NULL,
@tintPreLoginProceLevel TINYINT = 0,
@bitHideAddtoCart BIT=1,
@bitShowPromoDetailsLink BIT = 1
as              
BEGIN	        
	IF NOT EXISTS(select 'col1' from eCommerceDTL where numDomainID=@numDomainID AND [numSiteId] = @numSiteID)
	BEGIN
		INSERT INTO eCommerceDTL
        (
			numDomainId,
            vcPaymentGateWay,
            vcPGWManagerUId,
            vcPGWManagerPassword,
            bitShowInStock,
            bitShowQOnHand,
            numDefaultWareHouseID,
            bitCheckCreditStatus,
            [numRelationshipId],
            [numProfileId],
            [bitHidePriceBeforeLogin],
            bitAuthOnlyCreditCard,
            bitSendEmail,
            vcGoogleMerchantID,
            vcGoogleMerchantKey,
            IsSandbox ,
            numAuthrizedOrderStatus,
			numSiteId,
			vcPaypalUserName,
			vcPaypalPassword,
			vcPaypalSignature,
			IsPaypalSandbox,
			bitSkipStep2,
			bitDisplayCategory,
			bitShowPriceUsingPriceLevel,
			bitEnableSecSorting,
			bitSortPriceMode,
			numPageSize,
			numPageVariant,
			bitAutoSelectWarehouse,
			[bitPreSellUp],
			[bitPostSellUp],
			numDefaultClass,
			vcPreSellUp,
			vcPostSellUp,
			vcRedirectThankYouUrl,
			tintPreLoginProceLevel,
			bitHideAddtoCart,
			bitShowPromoDetailsLink
		)
		VALUES
		(
			@numDomainID,
            @vcPaymentGateWay,
            @vcPGWUserId,
            @vcPGWPassword,
            @bitShowInStock,
            @bitShowQOnHand,
            @numDefaultWareHouseID,
            @bitCheckCreditStatus,
            @numRelationshipId,
            @numProfileId,
            @bitHidePriceBeforeLogin,
            @bitAuthOnlyCreditCard,
            @bitSendMail,
            @vcGoogleMerchantID,
            @vcGoogleMerchantKey ,
            @IsSandbox,
            @numAuthrizedOrderStatus,
			@numSiteID,
			@vcPaypalUserName,
			@vcPaypalPassword,
			@vcPaypalSignature,
			@IsPaypalSandbox,
			@bitSkipStep2,
			@bitDisplayCategory,
			@bitShowPriceUsingPriceLevel,
			@bitEnableSecSorting,
			@bitSortPriceMode,
			@numPageSize,
			@numPageVariant,
			@bitAutoSelectWarehouse,
			@bitPreSellUp,
			@bitPostSellUp,
			@numDefaultClass,
			@vcPreSellUp,
			@vcPostSellUp,
			@vcRedirectThankYouUrl,
			@tintPreLoginProceLevel,
			@bitHideAddtoCart,
			@bitShowPromoDetailsLink
        )

		IF ISNULL((SELECT numDefaultSiteID FROM DOmain WHERE numDomainId=@numDomainID ),0) = 0
		BEGIN
			UPDATE Domain SET numDefaultSiteID=SCOPE_IDENTITY() WHERE numDomainId=@numDomainID
		END
	END
	ELSE
	BEGIN
		UPDATE 
			eCommerceDTL                                   
		SET              
			vcPaymentGateWay=@vcPaymentGateWay,                
			vcPGWManagerUId=@vcPGWUserId ,                
			vcPGWManagerPassword= @vcPGWPassword,              
			bitShowInStock=@bitShowInStock ,               
			bitShowQOnHand=@bitShowQOnHand,        
			numDefaultWareHouseID =@numDefaultWareHouseID,
			bitCheckCreditStatus=@bitCheckCreditStatus ,
			numRelationshipId = @numRelationshipId,
			numProfileId = @numProfileId,
			[bitHidePriceBeforeLogin]=@bitHidePriceBeforeLogin,
			bitAuthOnlyCreditCard=@bitAuthOnlyCreditCard,
			bitSendEmail = @bitSendMail,
			vcGoogleMerchantID = @vcGoogleMerchantID ,
			vcGoogleMerchantKey =  @vcGoogleMerchantKey ,
			IsSandbox = @IsSandbox,
			numAuthrizedOrderStatus = @numAuthrizedOrderStatus,
			numSiteId = @numSiteID,
			vcPaypalUserName = @vcPaypalUserName ,
			vcPaypalPassword = @vcPaypalPassword,
			vcPaypalSignature = @vcPaypalSignature,
			IsPaypalSandbox = @IsPaypalSandbox,
			bitSkipStep2 = @bitSkipStep2,
			bitDisplayCategory = @bitDisplayCategory,
			bitShowPriceUsingPriceLevel = @bitShowPriceUsingPriceLevel,
			bitEnableSecSorting = @bitEnableSecSorting,
			bitSortPriceMode=@bitSortPriceMode,
			numPageSize=@numPageSize,
			numPageVariant=@numPageVariant,
			bitAutoSelectWarehouse=@bitAutoSelectWarehouse,
			[bitPreSellUp] = @bitPreSellUp,
			[bitPostSellUp] = @bitPostSellUp,
			numDefaultClass=@numDefaultClass,
			vcPreSellUp=@vcPreSellUp,
			vcPostSellUp=@vcPostSellUp,
			vcRedirectThankYouUrl=@vcRedirectThankYouUrl,
			tintPreLoginProceLevel=@tintPreLoginProceLevel,
			bitHideAddtoCart=@bitHideAddtoCart,
			bitShowPromoDetailsLink=@bitShowPromoDetailsLink
		WHERE 
			numDomainId=@numDomainID 
			AND [numSiteId] = @numSiteID
	END
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetECommerceDetails')
DROP PROCEDURE USP_GetECommerceDetails
GO
CREATE PROCEDURE [dbo].[USP_GetECommerceDetails]      
@numDomainID as numeric(9)=0,
@numSiteID AS INTEGER = 0  
as
BEGIN
		IF ISNULL(@numSiteID,0) = 0
	BEGIN
		SELECT @numSiteID=numDefaultSiteID FROM Domain WHERE numDomainId=@numDomainID
	END                  
select                               
isnull(vcPaymentGateWay,'0') as vcPaymentGateWay,                
isnull(vcPGWManagerUId,'')as vcPGWManagerUId ,                
isnull(vcPGWManagerPassword,'') as vcPGWManagerPassword,              
isnull(bitShowInStock,0) as bitShowInStock,              
isnull(bitShowQOnHand,0) as bitShowQOnHand, 
isnull([numDefaultWareHouseID],0) numDefaultWareHouseID,
isnull(bitCheckCreditStatus,0) as bitCheckCreditStatus,
isnull([numRelationshipId],0) numRelationshipId,
isnull([numProfileId],0) numProfileId,
ISNULL([bitHidePriceBeforeLogin],0) AS bitHidePriceBeforeLogin,
ISNULL([numBizDocForCreditCard],0) AS numBizDocForCreditCard,
ISNULL([numBizDocForCreditTerm],0) AS numBizDocForCreditTerm,
ISNULL(bitAuthOnlyCreditCard,0) AS bitAuthOnlyCreditCard,
ISNULL(numAuthrizedOrderStatus,0) AS numAuthrizedOrderStatus,
ISNULL(bitSendEmail,1) AS bitSendEmail,
ISNULL(vcGoogleMerchantID , '') AS vcGoogleMerchantID,
ISNULL(vcGoogleMerchantKey,'') AS vcGoogleMerchantKey,
ISNULL(IsSandbox,0) AS IsSandbox,
ISNULL(numSiteID,0) AS numSiteID,
ISNULL(vcPaypalUserName,'') AS vcPaypalUserName,
ISNULL(vcPaypalPassword,'') AS vcPaypalPassword,
ISNULL(vcPaypalSignature,'') AS vcPaypalSignature,
ISNULL(IsPaypalSandbox,0) AS IsPaypalSandbox,
ISNULL(bitSkipStep2,0) AS [bitSkipStep2],
ISNULL(bitDisplayCategory,0) AS [bitDisplayCategory],
ISNULL(bitShowPriceUsingPriceLevel,0) AS [bitShowPriceUsingPriceLevel],
ISNULL(bitEnableSecSorting,0) AS [bitEnableSecSorting],
ISNULL(bitSortPriceMode,0) AS [bitSortPriceMode],
ISNULL(numPageSize,0) AS [numPageSize],
ISNULL(numPageVariant,0) AS [numPageVariant],
ISNULL(bitAutoSelectWarehouse,0) AS bitAutoSelectWarehouse,
ISNULL([bitPreSellUp],0) AS [bitPreSellUp],
ISNULL([bitPostSellUp],0) AS [bitPostSellUp],
ISNULL([dcPostSellDiscount],0) AS [dcPostSellDiscount],
ISNULL(numDefaultClass,0) numDefaultClass,
vcPreSellUp
,vcPostSellUp
,ISNULL(vcRedirectThankYouUrl,'') AS vcRedirectThankYouUrl
,ISNULL(tintPreLoginProceLevel,0) tintPreLoginProceLevel
,ISNULL(bitHideAddtoCart,0) bitHideAddtoCart
,ISNULL(bitShowPromoDetailsLink,0) bitShowPromoDetailsLink
FROM eCommerceDTL 
WHERE numDomainID=@numDomainID
AND ([numSiteId] = @numSiteID OR ISNULL(@numSiteID,0) = 0)
END
--Created by anoop jayaraj

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetPOItemsForFulfillment')
DROP PROCEDURE USP_GetPOItemsForFulfillment
GO
CREATE PROCEDURE [dbo].[USP_GetPOItemsForFulfillment]
	@numDomainID as numeric(9),
	@numUserCntID numeric(9)=0,                                                                                                               
	@SortChar char(1)='0',                                                            
	@CurrentPage int,                                                              
	@PageSize int,                                                              
	@TotRecs int output,                                                              
	@columnName as Varchar(50),                                                              
	@columnSortOrder as Varchar(10),
	@numRecordID NUMERIC(18,0),
	@FilterBy TINYINT,
	@Filter VARCHAR(300),
	@ClientTimeZoneOffset INT,
	@numVendorInvoiceBizDocID NUMERIC(18,0)
AS
BEGIN
	DECLARE @firstRec AS INTEGER                                                              
	DECLARE @lastRec AS INTEGER                                                     
	SET @firstRec = (@CurrentPage - 1) * @PageSize                             
	SET @lastRec = (@CurrentPage * @PageSize + 1 ) 

	IF LEN(ISNULL(@columnName,''))=0
		SET @columnName = 'Opp.numOppID'
		
	IF LEN(ISNULL(@columnSortOrder,''))=0	
		SET @columnSortOrder = 'DESC'

	CREATE TABLE #tempForm 
	(
		ID INT IDENTITY(1,1), tintOrder TINYINT,vcDbColumnName nvarchar(50),vcOrigDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),vcListItemType varchar(3),
		numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,
		bitIsAlphaNumeric BIT,bitIsNumeric BIT,bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),
		ListRelID numeric(9),intColumnWidth int,bitAllowFiltering BIT,vcFieldDataType CHAR(1)
	)

	-- FIRST CHECK IF GRID COLUMNS ARE CONFIGURED
	DECLARE  @Nocolumns  AS TINYINT = 0
	DECLARE @numFormId INT = 135

	SELECT 
		@Nocolumns=ISNULL(SUM(TotalRow),0) 
	FROM
	(            
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1
		UNION 
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicCustomColumns 
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
	) TotalRows

	-- IF GRID COLUMNS ARE NOT CONFIGURES THEN CHECK IF MASTER FORM CONFIGURATION IS CREATED BY ADMINISTRATOR
	DECLARE @IsMasterConfAvailable BIT = 0
	DECLARE @numUserGroup NUMERIC(18,0)

	IF @Nocolumns=0
	BEGIN
		SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

		IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = @numFormId AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
		BEGIN
			SET @IsMasterConfAvailable = 1
		END
	END

	--IF MASTERCONFIGURATION IS AVAILABLE THEN LOAD IT OTHERWISE LOAD DEFAULT COLUMNS
	IF @IsMasterConfAvailable = 1
	BEGIN
		INSERT INTO DycFormConfigurationDetails 
		(
			numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
		)
		SELECT 
			@numFormId,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,0,1,0,intColumnWidth
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
		UNION
		SELECT 
			@numFormId,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,0,1,1,intColumnWidth
		FROM 
			View_DynamicCustomColumnsMasterConfig
		WHERE 
			View_DynamicCustomColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1


		INSERT INTO 
			#tempForm
		SELECT 
			(intRowNum + 1) as tintOrder, vcDbColumnName,	vcOrigDbColumnName,	ISNULL(vcCultureFieldName,vcFieldName), vcAssociatedControlType,
			vcListItemType, numListID, vcLookBackTableName,	bitCustom, numFieldId, bitAllowSorting,	bitInlineEdit, bitIsRequired,
			bitIsEmail,	bitIsAlphaNumeric, bitIsNumeric, bitIsLengthValidation,	intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,
			ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType 
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName, vcAssociatedControlType,'' as vcListItemType,numListID,'',
			bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,''
		FROM 
			View_DynamicCustomColumnsMasterConfig
		WHERE 
			View_DynamicCustomColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1
		ORDER BY 
			tintOrder ASC  
	END
	ELSE                                        
	BEGIN
		IF @Nocolumns=0
		BEGIN
			INSERT INTO DycFormConfigurationDetails 
			(
				numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
			)
			SELECT 
				@numFormId,numFieldId,0,Row_number() over(order by tintRow desc),@numDomainID,@numUserCntID,0,1,0,intColumnWidth
			FROM 
				View_DynamicDefaultColumns
			WHERE 
				numFormId=@numFormId 
				AND bitDefault=1 
				AND ISNULL(bitSettingField,0)=1 
				AND numDomainID=@numDomainID
			ORDER BY 
				tintOrder asc   
		END

		INSERT INTO 
			#tempForm
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
			vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName,bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,bitFieldMessage,
			vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType  
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND ISNULL(bitSettingField,0)=1 
			AND ISNULL(bitCustom,0)=0  
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName,vcAssociatedControlType,'' as vcListItemType,numListID,'',bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,'' 
		FROM 
			View_DynamicCustomColumns
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND ISNULL(bitCustom,0)=1
		ORDER BY 
			tintOrder asc  
	END

	DECLARE @strColumns VARCHAR(MAX) = ''
    DECLARE @FROM NVARCHAR(MAX) = ''
    DECLARE @WHERE NVARCHAR(MAX) = ''

	SET @FROM =' FROM 
					OpportunityMaster Opp
				INNER JOIN DivisionMaster DM ON Opp.numDivisionId = DM.numDivisionID
				INNER JOIN CompanyInfo cmp ON cmp.numCompanyID=DM.numCompanyID
				INNER JOIN OpportunityItems OI On OI.numOppId=Opp.numOppId
				INNER JOIN Item I on I.numItemCode=OI.numItemCode
				LEFT JOIN WareHouseItems WI on (CASE WHEN ISNULL(Opp.bitStockTransfer,0) = 1 THEN numToWarehouseItemID ELSE OI.numWarehouseItmsID END)=WI.numWareHouseItemID
				LEFT JOIN Warehouses W on W.numWarehouseID=WI.numWarehouseID
				LEFT JOIN WarehouseLocation WL on WL.numWLocationID =WI.numWLocationID'

	SET @WHERE = CONCAT(' WHERE 
							tintOppType=2 
							AND tintOppstatus=1
							AND tintShipped=0 
							AND ',(CASE WHEN ISNULL(@numVendorInvoiceBizDocID,0) > 0 THEN 'OBDI.numUnitHour - ISNULL(OBDI.numVendorInvoiceUnitReceived,0) > 0' ELSE 'OI.numUnitHour - ISNULL(OI.numUnitHourReceived,0) > 0' END),'
							AND I.[charItemType] IN (''P'',''N'',''S'') 
							AND (OI.bitDropShip=0 or OI.bitDropShip IS NULL) 
							AND Opp.numDomainID=',@numDomainID)
	
	IF ISNULL(@numRecordID,0) > 0
	BEGIN
		IF @FilterBy=1 --Item
			set	@WHERE=@WHERE + CONCAT(' AND I.numItemCode = ',@numRecordID)
		ELSE IF @FilterBy=2 --Warehouse
			set	@WHERE=@WHERE + CONCAT(' AND WI.numWarehouseID = ',@numRecordID)
		ELSE IF @FilterBy=3 --Purchase Order
			set	@WHERE=@WHERE + CONCAT(' AND Opp.numOppID = ',@numRecordID)
		ELSE IF @FilterBy=4 --Vendor
			set	@WHERE=@WHERE + CONCAT(' AND DM.numDivisionID = ',@numRecordID)	
		ELSE IF @FilterBy=5 --BizDoc
			set	@WHERE=@WHERE + CONCAT(' AND Opp.numOppID in (SELECT numOppId FROM OpportunityBizDocs WHERE numOppBizDocsId = ',@numRecordID,')')
		ELSE IF @FilterBy=6 --SKU
			set	@WHERE=@WHERE + CONCAT(' AND I.numItemCode = ',@numRecordID)
	END
	ELSE IF LEN(ISNULL(@Filter,'')) > 0
	BEGIN
		IF @FilterBy=1 --Item
			set	@WHERE=@WHERE + ' and OI.vcItemName like ''%' + @Filter + '%'''
		ELSE IF @FilterBy=2 --Warehouse
			set	@WHERE=@WHERE + ' and W.vcWareHouse like ''%' + @Filter + '%'''
		ELSE IF @FilterBy=3 --Purchase Order
			set	@WHERE=@WHERE + ' and Opp.vcPOppName like ''%' + @Filter + '%'''
		ELSE IF @FilterBy=4 --Vendor
			set	@WHERE=@WHERE + ' and cmp.vcCompanyName LIKE ''%' + @Filter + '%'''		
		ELSE IF @FilterBy=5 --BizDoc
			set	@WHERE=@WHERE + ' and Opp.numOppID in (SELECT numOppId FROM OpportunityBizDocs WHERE vcBizDocID like ''%' + @Filter + '%'')'
		ELSE IF @FilterBy=6 --SKU
			set	@WHERE=@WHERE + ' and  I.vcSKU LIKE ''%' + @Filter + '%'' OR WI.vcWHSKU LIKE ''%' + @Filter + '%'')'
	END

	IF ISNULL(@numVendorInvoiceBizDocID,0) > 0
	BEGIN
		SET @FROM = @FROM + ' INNER JOIN OpportunityBizDocs OBD ON OBD.numOppID=Opp.numOppID INNER JOIN OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId = OBDI.numOppBizDocID AND OBDI.numOppItemID=OI.numoppitemtCode'
		SET	@WHERE=@WHERE + CONCAT(' AND OBD.numOppBizDocsId = ',@numVendorInvoiceBizDocID)
	END


	DECLARE @strSql NVARCHAR(MAX)
    DECLARE @SELECT NVARCHAR(MAX) = CONCAT('WITH POItems AS 
									(SELECT
										Row_number() OVER(ORDER BY ',@columnName,' ',@columnSortOrder,') AS row 
										,Opp.numDomainID
										,Opp.numOppID
										,ISNULL(Opp.bitStockTransfer,0) bitStockTransfer
										,OI.numoppitemtCode
										,CASE WHEN ISNULL(Opp.bitStockTransfer,0) = 1 THEN numToWarehouseItemID ELSE OI.numWarehouseItmsID END numWarehouseItmsID
										,WI.numWarehouseID
										,WI.numWLocationID
										,Opp.numRecOwner
										,DM.tintCRMType
										,ISNULL(DM.numTerID,0) numTerID
										,DM.numDivisionID
										,',(CASE WHEN ISNULL(@numVendorInvoiceBizDocID,0) > 0 THEN 'ISNULL(OBDI.numUnitHour,0)' ELSE 'ISNULL(OI.numUnitHour,0)' END), ' numUnitHour
										,',(CASE WHEN ISNULL(@numVendorInvoiceBizDocID,0) > 0 THEN 'ISNULL(OBDI.numVendorInvoiceUnitReceived,0)' ELSE 'ISNULL(OI.numUnitHourReceived,0)' END), ' numUnitHourReceived
										,ISNULL(Opp.fltExchangeRate,0) AS fltExchangeRate
										,ISNULL(Opp.numCurrencyID,0) AS numCurrencyID
										,ISNULL(I.numIncomeChartAcntId,0) AS itemIncomeAccount
										,ISNULL(I.numAssetChartAcntId,0) AS itemInventoryAsset
										,ISNULL(I.numCOGsChartAcntId,0) AS itemCoGs
										,ISNULL(OI.bitDropShip,0) AS DropShip
										,I.charItemType
										,OI.vcType ItemType
										,ISNULL(OI.numProjectID, 0) numProjectID
										,ISNULL(OI.numClassID, 0) numClassID
										,OI.numItemCode
										,OI.monPrice
										,CONCAT(I.vcItemName,CASE WHEN OI.vcItemName <> I.vcItemName THEN CONCAT('' ('',''Changed in order to: '',OI.vcItemName,'')'') ELSE '''' END) AS vcItemName
										,vcPOppName
										,ISNULL(Opp.bitPPVariance,0) AS bitPPVariance
										,ISNULL(I.bitLotNo,0) as bitLotNo
										,ISNULL(I.bitSerialized,0) AS bitSerialized
										,cmp.vcCompanyName')

	DECLARE @tintOrder  AS TINYINT;SET @tintOrder = 0
	DECLARE @vcFieldName  AS VARCHAR(50)
	DECLARE @vcListItemType  AS VARCHAR(3)
	DECLARE @vcListItemType1  AS VARCHAR(1)
	DECLARE @vcAssociatedControlType VARCHAR(30)
	DECLARE @numListID  AS NUMERIC(9)
	DECLARE @vcDbColumnName VARCHAR(40)
	DECLARE @vcLookBackTableName VARCHAR(2000)
	DECLARE @bitCustom  AS BIT
	Declare @numFieldId as numeric  
	DECLARE @bitAllowSorting AS CHAR(1)   
	DECLARE @bitAllowEdit AS CHAR(1)                 
	DECLARE @vcColumnName AS VARCHAR(500)
	DECLARE @ListRelID AS NUMERIC(9) 

	DECLARE @i AS INT = 1
	DECLARE @iCount AS INT 
	SELECT @iCount = COUNT(*) FROM #tempForm

	WHILE @i <= @iCount
	BEGIN
		SELECT 
			@tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,@vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,
			@numListID=numListID,@vcLookBackTableName=vcLookBackTableName ,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,
			@ListRelID=ListRelID
		FROM 
			#tempForm 
		WHERE
			ID=@i

		IF @bitCustom = 0
		BEGIN
			DECLARE  @Prefix  AS VARCHAR(5)

			IF @vcLookBackTableName = 'AdditionalContactsInformation'
				SET @Prefix = 'ADC.'
			IF @vcLookBackTableName = 'DivisionMaster'
				SET @Prefix = 'DM.'
			IF @vcLookBackTableName = 'OpportunityMaster'
				SET @PreFix = 'Opp.'
			IF @vcLookBackTableName = 'OpportunityItems'
				SET @PreFix = 'OI.'
			IF @vcLookBackTableName = 'Item'
				SET @PreFix = 'I.'
			IF @vcLookBackTableName = 'WareHouseItems'
				SET @PreFix = 'WI.'
			IF @vcLookBackTableName = 'Warehouses'
				SET @PreFix = 'W.'
			IF @vcLookBackTableName = 'WarehouseLocation'
				SET @PreFix = 'WL.'
			IF @vcLookBackTableName = 'CompanyInfo'
				SET @PreFix = 'cmp.'

			SET @vcColumnName= @vcDbColumnName+'~'+ convert(varchar(10),@numFieldId) +'~0'

			IF @vcAssociatedControlType = 'SelectBox'
			BEGIN
				IF @vcDbColumnName = 'numPartner'
				BEGIN
					SET @strColumns=@strColumns+ ' ,(SELECT D.vcPartnerCode+''-''+C.vcCompanyName FROM DivisionMaster AS D LEFT JOIN CompanyInfo AS C ON D.numCompanyID=C.numCompanyID WHERE D.numDivisionID=Opp.numPartner) '+' ['+ @vcColumnName+']'    
					SET @strColumns=@strColumns+ ' ,(SELECT D.vcPartnerCode+''-''+C.vcCompanyName FROM DivisionMaster AS D LEFT JOIN CompanyInfo AS C ON D.numCompanyID=C.numCompanyID WHERE D.numDivisionID=Opp.numPartner) AS vcPartner' 
				END
				ELSE IF @vcDbColumnName = 'tintSource'
				BEGIN
					SET @strColumns = @strColumns + ',ISNULL(dbo.fn_GetOpportunitySourceValue(ISNULL(Opp.tintSource,0),ISNULL(Opp.tintSourceType,0),Opp.numDomainID),''Internal Order'')' + ' [' + @vcColumnName + ']'
				END
				ELSE IF @vcDbColumnName = 'numContactID'
				BEGIN
					SET @strColumns = @strColumns + ',dbo.fn_GetContactName(' + @Prefix + @vcDbColumnName + ') [' + @vcColumnName + ']'
				END
				ELSE IF @vcDbColumnName = 'vcLocation'
				BEGIN
					SET @strColumns = @strColumns + ',CONCAT(W.vcWarehouse,CASE WHEN LEN(ISNULL(WL.vcLocation,'''')) > 0 THEN CONCAT('' ('',WL.vcLocation,'')'') ELSE '''' END) [' + @vcColumnName + ']'
				END
				ELSE IF @vcListItemType = 'LI'
				BEGIN
					SET @strColumns = @strColumns + ',L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData' + ' [' + @vcColumnName + ']'
					SET @FROM = @FROM + ' LEFT JOIN ListDetails L' + CONVERT(VARCHAR(3),@tintOrder) + ' on L' + CONVERT(VARCHAR(3),@tintOrder) + '.numListItemID=' + @vcDbColumnName
				END
				ELSE IF @vcListItemType = 'U'
				BEGIN
					SET @strColumns = @strColumns + ',dbo.fn_GetContactName(' + @Prefix + @vcDbColumnName + ') [' + @vcColumnName + ']'
				END
				ELSE IF @vcListItemType = 'SGT'
				BEGIN
					SET @strColumns = @strColumns + ',(CASE ' + @Prefix + @vcDbColumnName + ' 
														WHEN 0 THEN ''Service Default''
														WHEN 1 THEN ''Adult Signature Required''
														WHEN 2 THEN ''Direct Signature''
														WHEN 3 THEN ''InDirect Signature''
														WHEN 4 THEN ''No Signature Required''
														ELSE ''''
														END) [' + @vcColumnName + ']'
				END
				ELSE IF @vcListItemType = 'PSS'
				BEGIN
					SET @strColumns = @strColumns + ',ISNULL((SELECT vcShipmentService FROM ShippingService WHERE (numDomainID=Opp.numDomainID OR ISNULL(numDomainID,0)=0) AND numShippingServiceID = ' + @Prefix + @vcDbColumnName + '),'''') [' + @vcColumnName + ']'
				END
            END
			ELSE IF @vcAssociatedControlType = 'DateField'
			BEGIN
				IF @vcDbColumnName = 'dtReleaseDate'
				BEGIN
					SET @strColumns = @strColumns
                                + ',case when convert(varchar(11),'
                                + @Prefix
                                + @vcDbColumnName
                                + ')= convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getdate())) then ''<b><font color=red>Today</font></b>'''
					SET @strColumns = @strColumns
									+ 'when convert(varchar(11),'
									+ @Prefix
									+ @vcDbColumnName
									+ ')= convert(varchar(11),dateadd(day,-1,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getdate()))) then ''<b><font color=purple>Yesterday</font></b>'''
					SET @strColumns = @strColumns
									+ 'when convert(varchar(11),'
									+ @Prefix
									+ @vcDbColumnName
									+ ')= convert(varchar(11),dateadd(day,1,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getdate()))) then''<b><font color=orange>Tommorow</font></b>'' '
					SET @strColumns = @strColumns
									+ 'else dbo.FormatedDateFromDate('
									+ @Prefix
									+ @vcDbColumnName
									+ ','
									+ CONVERT(VARCHAR(10),@numDomainId)
									+ ') end  ['
									+ @vcColumnName + ']'
				END
				ELSE
				BEGIN
					SET @strColumns = @strColumns
                                + ',case when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','
                                + @Prefix
                                + @vcDbColumnName
                                + ') )= convert(varchar(11),getdate()) then ''<b><font color=red>Today</font></b>'''
					SET @strColumns = @strColumns
									+ 'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','
									+ @Prefix
									+ @vcDbColumnName
									+ ') )= convert(varchar(11),dateadd(day,-1,getdate())) then ''<b><font color=purple>Yesterday</font></b>'''
					SET @strColumns = @strColumns
									+ 'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','
									+ @Prefix
									+ @vcDbColumnName
									+ ') )= convert(varchar(11),dateadd(day,1,getdate())) then''<b><font color=orange>Tommorow</font></b>'' '
					SET @strColumns = @strColumns
									+ 'else dbo.FormatedDateFromDate('
									+ @Prefix
									+ @vcDbColumnName
									+ ','
									+ CONVERT(VARCHAR(10),@numDomainId)
									+ ') end  ['
									+ @vcColumnName + ']'
				 END	
            END
            ELSE IF @vcAssociatedControlType = 'TextBox' OR @vcAssociatedControlType = 'TextArea'
            BEGIN
				IF @vcDbColumnName = 'numShipState'
				BEGIN
					SET @strColumns = @strColumns + ',dbo.fn_getOPPAddressState(Opp.numOppId,Opp.numDomainID,2)' + ' [' + @vcColumnName + ']'
				END
				ELSE IF @vcDbColumnName = 'numQtyToShipReceive'
				BEGIN
					IF ISNULL(@numVendorInvoiceBizDocID,0) > 0
					BEGIN
						SET @strColumns = @strColumns + ',ISNULL(OBDI.numUnitHour,0) - ISNULL(OBDI.numVendorInvoiceUnitReceived,0)' + ' [' + @vcColumnName + ']'
					END
					ELSE
					BEGIN
						SET @strColumns = @strColumns + ',ISNULL(OI.numUnitHour,0) - ISNULL(OI.numUnitHourReceived,0)' + ' [' + @vcColumnName + ']'
					END
				END
				ELSE IF @vcDbColumnName = 'vcPathForTImage'
		   		BEGIN
					SET @strColumns = @strColumns + ',ISNULL(II.vcPathForTImage,'''') AS ' + ' ['+ @vcColumnName+']'                  
					SET @FROM = @FROM + ' LEFT JOIN ItemImages II ON II.numItemCode = I.numItemCode AND II.bitDefault = 1'
				END
				ELSE IF @vcDbColumnName = 'SerialLotNo'
				BEGIN
					SET @strColumns = @strColumns + ',SUBSTRING((SELECT '','' + vcSerialNo + CASE WHEN isnull(I.bitLotNo,0)=1 THEN '' ('' + CONVERT(VARCHAR(15),whi.numQty) + '')'' ELSE '''' end 
							FROM OppWarehouseSerializedItem oppI JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID=whi.numWareHouseItmsDTLID where oppI.numOppID=OI.numOppId and oppI.numOppItemID=OI.numoppitemtCode 
							ORDER BY vcSerialNo FOR XML PATH('''')),2,200000)  AS ' + ' ['+ @vcColumnName+']' 
				END
				ELSE IF @vcDbColumnName = 'numUnitHour'
				BEGIN
					IF ISNULL(@numVendorInvoiceBizDocID,0) > 0
					BEGIN
						SET @strColumns = @strColumns + ',ISNULL(OBDI.numUnitHour,0) [' + @vcColumnName + ']'
					END
					ELSE
					BEGIN
						SET @strColumns = @strColumns + ',ISNULL(' + @Prefix + @vcDbColumnName + ',0) [' + @vcColumnName + ']'
					END
				END
				ELSE IF @vcDbColumnName = 'numUnitHourReceived'
				BEGIN
					IF ISNULL(@numVendorInvoiceBizDocID,0) > 0
					BEGIN
						SET @strColumns = @strColumns + ',ISNULL(OBDI.numVendorInvoiceUnitReceived,0) [' + @vcColumnName + ']'
					END
					ELSE
					BEGIN
						SET @strColumns = @strColumns + ',ISNULL(' + @Prefix + @vcDbColumnName + ',0) [' + @vcColumnName + ']'
					END
				END
				ELSE IF @vcDbColumnName = 'vcSKU'
				BEGIN
					SET @strColumns = @strColumns + ',ISNULL(OI.vcSKU,ISNULL(I.vcSKU,'''')) [' + @vcColumnName + ']'
				END
				ELSE IF @vcDbColumnName = 'vcItemName'
				BEGIN
					SET @strColumns = @strColumns + ',CONCAT(I.vcItemName,CASE WHEN OI.vcItemName <> I.vcItemName THEN CONCAT('' ('',''Changed in order to: '',OI.vcItemName,'')'') ELSE '''' END)  [' + @vcColumnName + ']'
				END
				ELSE
				BEGIN
					SET @strColumns = @strColumns + ',' + @Prefix + @vcDbColumnName + ' [' + @vcColumnName + ']'
				END
            END
            ELSE IF  @vcAssociatedControlType='Label'                                              
			BEGIN
				IF @vcDbColumnName = 'numRemainingQty'
				BEGIN
					IF ISNULL(@numVendorInvoiceBizDocID,0) > 0
					BEGIN
						SET @strColumns = @strColumns + ',ISNULL(OBDI.numUnitHour,0) - ISNULL(OBDI.numVendorInvoiceUnitReceived,0)' + ' [' + @vcColumnName + ']'
					END
					ELSE
					BEGIN
						SET @strColumns = @strColumns + ',ISNULL(OI.numUnitHour,0) - ISNULL(OI.numUnitHourReceived,0)' + ' [' + @vcColumnName + ']'
					END
				END
				ELSE
				BEGIN
					SET @strColumns=@strColumns + ',' + @Prefix + @vcDbColumnName +' ['+ @vcColumnName+']'        
				END
			END
        END
		ELSE
        BEGIN
			SET @vcColumnName= @vcDbColumnName +'~'+ convert(varchar(10),@numFieldId)+'~1'

			IF @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'
            BEGIN
              SET @strColumns = @strColumns
                              + ',CFW'
                              + CONVERT(VARCHAR(3),@tintOrder)
                              + '.Fld_Value  ['
                              + @vcColumnName + ']'
              SET @FROM = @FROM
                                      + ' left Join CFW_FLD_Values_Opp CFW'
                                      + CONVERT(VARCHAR(3),@tintOrder)
                                      + ' on CFW'
                                      + CONVERT(VARCHAR(3),@tintOrder)
                                      + '.Fld_Id='
                                      + CONVERT(VARCHAR(10),@numFieldId)
                                      + 'and CFW'
                                      + CONVERT(VARCHAR(3),@tintOrder)
                                      + '.RecId=Opp.numOppid   '
			END
			ELSE
            IF @vcAssociatedControlType = 'CheckBox' 
            BEGIN
                SET @strColumns = @strColumns
                                + ',case when isnull(CFW'
                                + CONVERT(VARCHAR(3),@tintOrder)
                                + '.Fld_Value,0)=0 then 0 when isnull(CFW'
                                + CONVERT(VARCHAR(3),@tintOrder)
                                + '.Fld_Value,0)=1 then 1 end   ['
                                + @vcColumnName + ']'
                SET @FROM = @FROM
                                        + ' left Join CFW_FLD_Values_Opp CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + ' on CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.Fld_Id='
                                        + CONVERT(VARCHAR(10),@numFieldId)
                                        + 'and CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.RecId=Opp.numOppid   '
            END
            ELSE
            IF @vcAssociatedControlType = 'DateField'
            BEGIN
                  SET @strColumns = @strColumns
                                  + ',dbo.FormatedDateFromDate(CFW'
                                  + CONVERT(VARCHAR(3),@tintOrder)
                                  + '.Fld_Value,'
                                  + CONVERT(VARCHAR(10),@numDomainId)
                                  + ')  ['
                                  + @vcColumnName + ']'
                  SET @FROM = @FROM
                                          + ' left Join CFW_FLD_Values_Opp CFW'
                                          + CONVERT(VARCHAR(3),@tintOrder)
                                          + ' on CFW'
                                          + CONVERT(VARCHAR(3),@tintOrder)
                                          + '.Fld_Id='
                                          + CONVERT(VARCHAR(10),@numFieldId)
                                          + 'and CFW'
                                          + CONVERT(VARCHAR(3),@tintOrder)
                                          + '.RecId=Opp.numOppid    '
            END
            ELSE
            IF @vcAssociatedControlType = 'SelectBox'
            BEGIN
                SET @vcDbColumnName = 'DCust'
                                            + CONVERT(VARCHAR(10),@numFieldId)
                SET @strColumns = @strColumns
                                + ',L'
                                + CONVERT(VARCHAR(3),@tintOrder)
                                + '.vcData'
                                + ' ['
                                + @vcColumnName + ']'

                SET @FROM = @FROM
                                        + ' left Join CFW_FLD_Values_Opp CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + ' on CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.Fld_Id='
                                        + CONVERT(VARCHAR(10),@numFieldId)
                                        + 'and CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.RecId=Opp.numOppid '

                SET @FROM = @FROM
                                        + ' left Join ListDetails L'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + ' on L'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.numListItemID=CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.Fld_Value'
            END
			ELSE
			BEGIN
				SET @strColumns = @strColumns + CONCAT(',dbo.GetCustFldValueOpp(',@numFieldId,',opp.numOppID)') + ' [' + @vcColumnName + ']'
			END
		END

		SET @i = @i + 1
	END

	SET @strSql = @SELECT + @strColumns + @FROM + @WHERE + ') SELECT * INTO #tempTable FROM [POItems] WHERE row > ' + CONVERT(VARCHAR(10), @firstRec) + ' and row <' + CONVERT(VARCHAR(10), @lastRec)

	PRINT CAST(@strSql AS NTEXT)
	SET @strSql = @strSql + '; (SELECT @TotRecs =COUNT(*) ' + @FROM + @WHERE + ')' + '; SELECT * FROM #tempTable ORDER BY [row]; DROP TABLE #tempTable;'

	EXEC sp_executesql 
        @query = @strSql, 
        @params = N'@TotRecs INT OUTPUT', 
        @TotRecs = @TotRecs OUTPUT 

	SELECT * FROM #tempForm
	DROP TABLE #tempForm
END
GO



GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetSitesDetail')
DROP PROCEDURE USP_GetSitesDetail
GO
CREATE PROCEDURE [dbo].[USP_GetSitesDetail]
(
	@numSiteID NUMERIC(9)
)
AS 
BEGIN
    SELECT  
		S.[numDomainID],
        D.[numDivisionID],
        ISNULL(S.[bitIsActive], 1),
        ISNULL(E.[numDefaultWareHouseID],0) numDefaultWareHouseID,
        ISNULL(E.[numRelationshipId],0) numRelationshipId,
        ISNULL(E.[numProfileId],0) numProfileId,
        ISNULL(E.[bitHidePriceBeforeLogin],0) AS bitHidePriceBeforeLogin,
		dbo.fn_GetContactEmail(1,0,numAdminID) AS DomainAdminEmail,
		S.vcLiveURL,
		ISNULL(E.bitAuthOnlyCreditCard,0) bitAuthOnlyCreditCard,
		ISNULL(D.numDefCountry,0) numDefCountry,
		ISNULL(E.bitSendEMail,0) bitSendMail,
		ISNULL((SELECT numAuthoritativeSales FROM dbo.AuthoritativeBizDocs WHERE numDomainId=S.numDomainID),0) AuthSalesBizDoc,
		ISNULL((SELECT TOP 1 numBizDocId FROM dbo.eCommercePaymentConfig WHERE numSiteID=@numSiteID AND numPaymentMethodId = 27261 AND bitEnable =1 ),0) numCreditTermBizDocID,
		ISNULL((SELECT TOP 1 numBizDocId FROM dbo.eCommercePaymentConfig WHERE numSiteID=@numSiteID AND numPaymentMethodId =1 AND bitEnable =1 ),0) numCreditCardBizDocID,
		ISNULL((SELECT TOP 1 numBizDocId FROM dbo.eCommercePaymentConfig WHERE numSiteID=@numSiteID AND numPaymentMethodId = 31488 AND bitEnable =1 ),0) numGoogleCheckoutBizDocID,
		ISNULL((SELECT TOP 1 numBizDocId FROM dbo.eCommercePaymentConfig WHERE numSiteID=@numSiteID AND numPaymentMethodId = 35141 AND bitEnable =1 ),0) numPaypalCheckoutBizDocID,
		ISNULL((SELECT TOP 1 numBizDocId FROM dbo.eCommercePaymentConfig WHERE numSiteID=@numSiteID AND numPaymentMethodId = 84 AND bitEnable =1 ),0) numSalesInquiryBizDocID,
		ISNULL(D.bitSaveCreditCardInfo,0) bitSaveCreditCardInfo,
		ISNULL(bitOnePageCheckout,0) bitOnePageCheckout,
		ISNULL(D.tintBaseTaxOnArea,0) tintBaseTaxOnArea,
		ISNULL(D.numShippingServiceItemID,0) numShippingServiceItemID,
		ISNULL(D.bitAutolinkUnappliedPayment,0) bitAutolinkUnappliedPayment,
		ISNULL(D.numDiscountServiceItemID,0) numDiscountServiceItemID,
		ISNULL(D.tintBaseTax,0) AS [tintBaseTax],
		ISNULL(E.bitSkipStep2,0) AS [bitSkipStep2],
		ISNULL(E.bitDisplayCategory,0) AS [bitDisplayCategory],
		ISNULL(E.bitShowPriceUsingPriceLevel,0) AS [bitShowPriceUsingPriceLevel],
		ISNULL(E.[bitPreSellUp],0) [bitPreSellUp],
		ISNULL(E.[bitPostSellUp],0) [bitPostSellUp],
		ISNULL(E.[dcPostSellDiscount],0) [dcPostSellDiscount],
		ISNULL(E.numDefaultClass,0) numDefaultClass,
		E.vcPreSellUp,E.vcPostSellUp,ISNULL(E.vcRedirectThankYouUrl,'') AS vcRedirectThankYouUrl,
		ISNULL(D.vcSalesOrderTabs,'') AS vcSalesOrderTabs,
		ISNULL(D.vcSalesQuotesTabs,'') AS vcSalesQuotesTabs,
		ISNULL(D.vcItemPurchaseHistoryTabs,'') AS vcItemPurchaseHistoryTabs,
		ISNULL(D.vcItemsFrequentlyPurchasedTabs,'') AS vcItemsFrequentlyPurchasedTabs,
		ISNULL(D.vcOpenCasesTabs,'') AS vcOpenCasesTabs,
		ISNULL(D.vcOpenRMATabs,'') AS vcOpenRMATabs,
		ISNULL(D.bitSalesOrderTabs,0) AS bitSalesOrderTabs,
		ISNULL(D.bitSalesQuotesTabs,0) AS bitSalesQuotesTabs,
		ISNULL(D.bitItemPurchaseHistoryTabs,0) AS bitItemPurchaseHistoryTabs,
		ISNULL(D.bitItemsFrequentlyPurchasedTabs,0) AS bitItemsFrequentlyPurchasedTabs,
		ISNULL(D.bitOpenCasesTabs,0) AS bitOpenCasesTabs,
		ISNULL(D.bitOpenRMATabs,0) AS bitOpenRMATabs,
		ISNULL(D.bitSupportTabs,0) AS bitSupportTabs,
		ISNULL(D.vcSupportTabs,'') AS vcSupportTabs,
		ISNULL(bitHideAddtoCart,0) AS bitHideAddtoCart,
		ISNULL(bitShowPromoDetailsLink,0) bitShowPromoDetailsLink
    FROM
		Sites S
    INNER JOIN 
		[Domain] D 
	ON 
		D.[numDomainId] = S.[numDomainID]
    LEFT OUTER JOIN 
		[eCommerceDTL] E 
	ON 
		D.[numDomainId] = E.[numDomainId] 
		AND S.numSiteID = E.numSiteId
    WHERE
		S.[numSiteID] = @numSiteID
END
GO

GO
/****** Object:  StoredProcedure [dbo].[usp_GetStageItemDetails]    Script Date: 09/23/2010 15:24:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--usp_GetStageItemDetails              
              
--This procedure will return the values from the stagepercentagedetails table. If the stage number is passed then              
--the details pertaining to that stge will be passed else all the records will be passed              
--EXEC usp_GetStageItemDetails 1,133,1
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getstageitemdetails')
DROP PROCEDURE usp_getstageitemdetails
GO
CREATE PROCEDURE [dbo].[usp_GetStageItemDetails]
--    @numStageNo NUMERIC = 0,
    @numDomainid NUMERIC = 0,
    @slpid NUMERIC = 0 ,
    @tintMode TINYINT=0,
	@numOppId NUMERIC=0
--              
AS 
IF @tintMode = 0 
BEGIN              
        SELECT  numStageDetailsId,
                numStagePercentageId,
                vcStageName,
                sp.numDomainId,
                sp.numCreatedBy,
                sp.bintCreatedDate,
                sp.numModifiedBy,
                sp.bintModifiedDate,
                slp_id,
                sp.numAssignTo,
                vcMilestoneName,
				ISNULL((select
				   DISTINCT
					stuff((
						select ',' + ST.vcTaskName+'('+ISNULL((SELECT TOP 1 (AU.vcFirstName+' '+AU.vcLastName) FROM AdditionalContactsInformation AS AU WHERE AU.numContactID=ST.numAssignTo),'-')+')'
						from StagePercentageDetailsTask ST
						where ST.numStageDetailsId = STU.numStageDetailsId AND ISNULL(ST.numOppId,0)=0 AND  ISNULL(ST.numProjectId,0)=0
						order by ST.numTaskId
						for xml path('')
					),1,1,'') as TaskName
				from StagePercentageDetailsTask AS STU  WHERE STU.numStageDetailsID=sp.numStageDetailsId AND ISNULL(STU.numOppId,0)=0 AND  ISNULL(STU.numProjectId,0)=0
				group by numStageDetailsId),'-') As TaskName,
				(SELECT ISNULL(SUM(numHours),0) FROM StagePercentageDetailsTask WHERE numStageDetailsId=sp.numStageDetailsId) As numHours,
				(SELECT ISNULL(SUM(numMinutes),0) FROM StagePercentageDetailsTask WHERE numStageDetailsId=sp.numStageDetailsId)  As numMinutes,
                CASE WHEN sp.numAssignTo = 0 THEN '--Select One--'
                     ELSE A.vcFirstName + ' ' + A.vcLastName
                END AS vcAssignTo,
                ISNULL(tintPercentage, '') tintPercentage,
                ISNULL(bitClose, 0) bitClose,
                tintConfiguration,
                ISNULL(vcDescription,'') vcDescription,
                ISNULL(numParentStageID,0) numParentStageID,
				ISNULL(bitIsDueDaysUsed,0) AS bitIsDueDaysUsed,
				ISNULL(numTeamId,0) AS numTeamId,
				ISNULL(bitRunningDynamicMode,0) AS bitRunningDynamicMode,
                ISNULL(intDueDays,0) AS intDueDays,(select count(*) from StageDependency where numStageDetailID=sp.numStageDetailsId) numDependencyCount,
				(convert(varchar(10),sp.dtStartDate, 101) + right(convert(varchar(32),sp.dtStartDate,100),8)) AS StageStartDate,
				(select dbo.fn_calculateTotalHoursMinutesByStageDetails(sp.numStageDetailsId,@numOppId)) as dayshoursTaskDuration,
				ISNULL(SP.numStageOrder,0) As numStageOrder,
				ISNULL((SELECT ISNULL(intTotalProgress,0) FROM dbo.ProjectProgress WHERE numDomainId=@numDomainID AND numOppId=@numOppId),0) AS intTotalProgress
        FROM    stagepercentagedetails sp
                LEFT JOIN AdditionalContactsInformation A ON sp.numAssignTo = A.numContactID
        WHERE  
                sp.numdomainid = @numDomainid
                AND slp_id = @slpid
                AND ISNULL(sp.numProjectID,0)= 0
                AND ISNULL(sp.numOppID,0)= @numOppId
        ORDER BY numstagepercentageid,
                numStageOrder ASC              
    END              
ELSE IF @tintMode = 1 -- Fill dropdown
SELECT numStageDetailsId,vcStageName FROM dbo.StagePercentageDetails WHERE slp_id = @slpid AND numDomainId=@numDomainid
	AND LEN(vcStageName) > 1

ELSE IF  @tintMode = 2
SELECT [numStageDetailsId],[numStagePercentageId],tintConfiguration
      ,[vcStageName],[numDomainId],[numCreatedBy],
	(Case bitFromTemplate when 0 then isnull(dbo.fn_GetContactName([numCreatedBy]),'-') else 'Template' end) as vcCreatedBy,dbo.[FormatedDateFromDate](ISNULL(bintCreatedDate,GETDATE()),@numDomainId) bintCreatedDate
      ,[numModifiedBy],isnull(dbo.fn_GetContactName([numModifiedBy]),'-') as vcModifiedBy
      ,dbo.[FormatedDateTimeFromDate](ISNULL(bintModifiedDate,GETDATE()),@numDomainId) bintModifiedDate,[slp_id]
      ,[numAssignTo],isnull(dbo.fn_GetContactName(numAssignTo),'-') as vcAssignTo
      ,[vcMileStoneName],[tintPercentage],isnull([tinProgressPercentage],0) tinProgressPercentage,[bitClose]
      ,dbo.[FormatedDateFromDate](ISNULL(dtStartDate,GETDATE()),@numDomainId) dtStartDate,dbo.[FormatedDateFromDate](ISNULL(dtEndDate,GETDATE()),@numDomainId) dtEndDate,[numParentStageID],[intDueDays]
      ,[numProjectID],[numOppID],[vcDescription], 
  isnull(dbo.fn_GetExpenseDtlsbyProStgID(numProjectID,numStageDetailsId,0),0) as numExpense,                        
  isnull(dbo.fn_GeTimeDtlsbyProStgID_BT_NBT(numProjectID,numStageDetailsId,1),'Billable Time (0)  Non Billable Time (0)') as numTime,
(select count(*) from dbo.GenericDocuments where numRecID=numStageDetailsId and vcDocumentSection='PS') numDocuments,
(select count(*) from [StagePercentageDetails] where numParentStageId=p.numStageDetailsId and numProjectID=p.numProjectID) as numChildCount,
isnull(bitTimeBudget,0) as bitTimeBudget,isnull(bitExpenseBudget,0) as bitExpenseBudget,isnull(monTimeBudget,0) as monTimeBudget,isnull(monExpenseBudget,0) as monExpenseBudget
  FROM [StagePercentageDetails] p where [numStageDetailsId]=@slpid

ELSE IF  @tintMode = 3
SELECT count(*) Total
  FROM [StagePercentageDetails] p where numProjectID=@slpid and numDomainid=@numDomainid

ELSE IF  @tintMode =4
SELECT count(*) Total
  FROM [StagePercentageDetails] p where numOppID=@slpid and numDomainid=@numDomainid

ELSE
BEGIN
	SELECT numStageDetailsId,vcStageName FROM dbo.StagePercentageDetails WHERE slp_id = @slpid AND numDomainId=@numDomainid
	AND LEN(vcStageName) > 1
END
--    BEGIN              
--        SELECT  numStageDetailsId,
--                numStagePercentageId,
--                numStageNumber,
--                vcStageDetail,
--                sp.numDomainId,
--                sp.numCreatedBy,
--                sp.bintCreatedDate,
--                sp.numModifiedBy,
--                sp.bintModifiedDate,
--                slp_id,
--                sp.numAssignTo,
--                vcStagePercentageDtl,
--                CASE WHEN sp.numAssignTo = 0 THEN '--Select One--'
--                     ELSE A.vcFirstName + ' ' + A.vcLastName
--                END AS vcAssignTo,
--                numTemplateId,
--                ISNULL(tintPercentage, '') tintPercentage,
--                ISNULL(bitClose, 0) bitClose
--        FROM    stagepercentagedetails sp
--                LEFT JOIN AdditionalContactsInformation A ON sp.numAssignTo = A.numContactID
--        WHERE   sp.numdomainid = @numDomainid
--                AND slp_id = @slpid
--        ORDER BY numstagepercentageid,
--                numStageDetailsId ASC              
            
            
   --SELECT * FROM stagepercentagedetails             
            
--    END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER  ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetWFRecordData')
DROP PROCEDURE USP_GetWFRecordData
GO
Create PROCEDURE [dbo].[USP_GetWFRecordData]     
    @numDomainID numeric(18, 0),
    @numRecordID numeric(18, 0),
    @numFormID numeric(18, 0)
AS                 
BEGIN
	DECLARE @numContactId AS NUMERIC(18),
			@numRecOwner AS NUMERIC(18),
			@numAssignedTo AS NUMERIC(18),
			@numDivisionId AS NUMERIC(18),
			@txtSignature VARCHAR(8000),
			@vcDateFormat VARCHAR(30),
			@numInternalPM numeric(18,0),
			@numExternalPM numeric(18,0),
			@numNextAssignedTo numeric(18,0),
			@numPhone varchar(15),
			@numOppIDPC numeric(18,0),
			@numStageIDPC numeric(18,0),
			@numOppBizDOCIDPC numeric(18,0),
			@numProjectID NUMERIC(18,0),
			@numCaseId NUMERIC(18,0),
			@numCommId NUMERIC(18,0)

	IF @numFormID=70 --Opportunities & Orders
	BEGIN
		SELECT 
			@numContactId=ISNULL(numContactId,0)
			,@numRecOwner=ISNULL(numRecOwner,0)
			,@numAssignedTo=ISNULL(numAssignedTo,0)
			,@numDivisionId=ISNULL(numDivisionId,0)
		FROM 
			dbo.OpportunityMaster 
		WHERE 
			numDomainId=@numDomainID AND numOppId=@numRecordID

		SELECT 
			@numStageIDPC=ISNULL(s.numStageDetailsId,0)
		FROM 
			OpportunityMaster o 
		LEFT JOIN 
			StagePercentageDetails s 
		on 
			s.numOppID=o.numOppId
		WHERE 
			o.numDomainId=@numDomainID 
			AND o.numOppId=@numRecordID

		SELECT 
			@numOppBizDOCIDPC=ISNULL(s.numOppBizDocsId,0)
			,@numOppIDPC=ISNULL(o.numOppId,0)
		FROM 
			OpportunityMaster o 
		LEFT JOIN 
			OpportunityBizDocs s 
		ON 
			s.numOppID=o.numOppId
		WHERE 
			o.numDomainId=@numDomainID 
			AND o.numOppId=@numRecordID
	END

	IF @numFormID=49 --BizDocs
	BEGIN
		SELECT 
			@numOppBizDOCIDPC=ISNULL(obd.numOppBizDocsId,0)
			,@numContactId=ISNULL(om.numContactId,0)
			,@numRecOwner=ISNULL(om.numRecOwner,0)
			,@numOppIDPC=ISNULL(om.numOppId,0)
			,@numAssignedTo=ISNULL(om.numAssignedTo,0)
			,@numDivisionId=ISNULL(om.numDivisionId,0)
			,@vcDateFormat=D.vcDateFormat
		FROM 
			dbo.OpportunityBizDocs AS obd 
		INNER JOIN 
			dbo.OpportunityMaster AS OM 
		ON 
			obd.numOppId=om.numOppId 
		INNER JOIN 
			dbo.Domain AS d 
		ON 
			d.numDomainId=om.numDomainId
		WHERE 
			om.numDomainId=@numDomainID 
			AND obd.numOppBizDocsId=@numRecordID
	END

	IF @numFormID=94 --Sales/Purchase/Project
	BEGIN
		DECLARE @numOppId NUMERIC(18,0)
		SELECT 
			@numProjectID=ISNULL(numProjectID,0)
			,@numOppId=Isnull(numOppId,0) 
		FROM 
			StagePercentageDetails 
		WHERE 
			numStageDetailsId=@numRecordID

		--@numInternalPM numeric(18,0),@numExternalPM numeric(18,0),@numNextAssignedTo numeric(18,0)
		IF @numProjectID=0--Order
		BEGIN
			SELECT 
				@numContactId=ISNULL(om.numContactId,0)
				,@numRecOwner=ISNULL(om.numRecOwner,0)
				,@numAssignedTo=ISNULL(obd.numAssignTo,0)
				,@numDivisionId=ISNULL(om.numDivisionId,0)
				,@vcDateFormat=D.vcDateFormat
			FROM 
				dbo.StagePercentageDetails AS obd 
			INNER JOIN 
				dbo.OpportunityMaster AS OM 
			on 
				obd.numOppId=om.numOppId 
			INNER JOIN 
				dbo.Domain AS d 
			ON 
				d.numDomainId=om.numDomainId
			WHERE 
				om.numDomainId=@numDomainID 
				AND obd.numStageDetailsId=@numRecordID
		END
		ELSE
		BEGIN
			SELECT 
				@numNextAssignedTo=numAssignTo 
			FROM 
				StagePercentageDetails 
			WHERE 
				numStageDetailsId=@numRecordID+1

			SELECT 
				@numContactId=ISNULL(om.numIntPrjMgr,0)
				,@numRecOwner=ISNULL(om.numRecOwner,0)
				,@numAssignedTo=ISNULL(obd.numAssignTo,0)
				,@numDivisionId=ISNULL(om.numDivisionId,0)
				,@numInternalPM=ISNULL(om.numIntPrjMgr,0)
				,@numExternalPM=ISNULL(om.numCustPrjMgr,0)
			FROM 
				dbo.StagePercentageDetails AS  obd 
			INNER JOIN 
				dbo.ProjectsMaster AS OM 
			on 
				obd.numProjectID=om.numProId 	
			WHERE 
				om.numDomainId=@numDomainID 
				AND obd.numStageDetailsId=@numRecordID
		END
	END
	
	IF (@numFormID=68 OR @numFormID=138) --Organization , AR Aging
	BEGIN
		SELECT 
			@numContactId=ISNULL(adc.numContactId,0)
			,@numRecOwner=ISNULL(obd.numRecOwner,0)
			,@numAssignedTo=ISNULL(obd.numAssignedTo,0)
			,@numDivisionId=ISNULL(obd.numDivisionId,0)
			,@vcDateFormat=d.vcDateFormat
		FROM 
			dbo.DivisionMaster AS  obd 	
		INNER join 
			dbo.AdditionalContactsInformation as adc 
		ON 
			adc.numDivisionId=obd.numDivisionID
		INNER JOIN 
			dbo.Domain AS d 
		ON 
			d.numDomainId=obd.numDomainId
		WHERE 
			obd.numDomainId=@numDomainID 
			AND obd.numDivisionID=@numRecordID
	END

	IF @numFormID=69 --Contacts
	BEGIN
		SELECT 
			@numContactId=ISNULL(adc.numContactId,0)
			,@numRecOwner=ISNULL(obd.numRecOwner,0)
			,@numAssignedTo=ISNULL(obd.numAssignedTo,0)
			,@numDivisionId=ISNULL(obd.numDivisionId,0)
			,@vcDateFormat=d.vcDateFormat
		FROM 
			dbo.DivisionMaster AS  obd 	
		INNER JOIN 
			dbo.AdditionalContactsInformation as adc 
		ON 
			adc.numDivisionId=obd.numDivisionID
		INNER JOIN 
			dbo.Domain AS d 
		ON 
			d.numDomainId=obd.numDomainId
		WHERE 
			obd.numDomainId=@numDomainID 
			AND adc.numContactId=@numRecordID
	END

	IF @numFormID=73 --Projects
	BEGIN
		SELECT 
			@numProjectID=ISNULL(OM.numProId,0)
			,@numContactId=ISNULL(om.numIntPrjMgr,0)
			,@numRecOwner=ISNULL(om.numRecOwner,0)
			,@numAssignedTo=ISNULL(om.numAssignedTo,0)
			,@numDivisionId=ISNULL(om.numDivisionId,0)
			,@numInternalPM=ISNULL(om.numIntPrjMgr,0)
			,@numExternalPM=ISNULL(om.numCustPrjMgr,0)
		FROM 
			dbo.ProjectsMaster AS OM	
		WHERE 
			om.numDomainId=@numDomainID 
			AND om.numProId=@numRecordID

		SELECT 
			@numStageIDPC=ISNULL(s.numStageDetailsId,0)
		FROM 
			ProjectsMaster o 
		left join 
			StagePercentageDetails s 
		on 
			s.numProjectID=o.numProId
		WHERE 
			o.numDomainId=@numDomainID 
			AND o.numProId=@numRecordID
	END

	IF @numFormID=72 --Cases
	BEGIN
		SELECT 
			@numCaseId=ISNULL(OM.numCaseId,0)
			,@numContactId=ISNULL(om.numContactId,0)
			,@numRecOwner=ISNULL(om.numRecOwner,0)
			,@numAssignedTo=ISNULL(om.numAssignedTo,0)
			,@numDivisionId=ISNULL(om.numDivisionId,0)
			,@vcDateFormat=d.vcDateFormat
		FROM 
			dbo.Cases AS OM	
		INNER JOIN 
			dbo.Domain AS d 
		ON
			d.numDomainId=om.numDomainId
		WHERE 
			om.numDomainId=@numDomainID 
			AND om.numCaseId=@numRecordID
	
		SELECT 
			@numOppIDPC=ISNULL(o.numOppId,0)
		FROM 
			Cases om 
		left JOIN 
			CaseOpportunities co 
		on 
			om.numCaseId=co.numCaseId  
		left join 
			OpportunityMaster o 
		on
			o.numOppId=co.numOppId
		WHERE 
			om.numDomainId=@numDomainID 
			AND om.numCaseId=@numRecordID
	END

	IF @numFormID=124 --Action items(Ticklers)
	BEGIN
		SELECT 
			@numCommId=ISNULL(OM.numCommId,0)
			,@numContactId=ISNULL(om.numContactId,0)
			,@numRecOwner=ISNULL(om.numAssignedBy,0)
			,@numAssignedTo=ISNULL(om.numAssign,0)
			,@numDivisionId=ISNULL(om.numDivisionId,0)
			,@vcDateFormat=d.vcDateFormat
		FROM 
			dbo.Communication AS OM	
		INNER JOIN 
			dbo.Domain AS d 
		ON 
			d.numDomainId=om.numDomainId
		WHERE 
			om.numDomainId=@numDomainID 
			AND om.numCommId=@numRecordID
	END

	DECLARE @vcEmailID AS VARCHAR(100),
			@ContactName AS VARCHAR(150),
			@bitSMTPServer AS bit,
			@vcSMTPServer AS VARCHAR(100),
			@numSMTPPort AS NUMERIC,
			@bitSMTPAuth AS BIT,
			@vcSmtpPassword AS VARCHAR(100),
			@bitSMTPSSL BIT 

	IF @numRecOwner>0
	BEGIN
		SELECT 
			@vcEmailID=vcEmailID
			,@ContactName=isnull(vcFirstname,'')+' '+isnull(vcLastName,'')
			,@bitSMTPServer=isnull(bitSMTPServer,0)
			,@vcSMTPServer=case when bitSMTPServer= 1 then vcSMTPServer else '' end
			,@numSMTPPort=case when bitSMTPServer= 1 then  isnull(numSMTPPort,0) else 0 end
			,@bitSMTPAuth=isnull(bitSMTPAuth,0)
			,@vcSmtpPassword=isnull([vcSmtpPassword],'')
			,@bitSMTPSSL=isnull(bitSMTPSSL,0)
			,@txtSignature=u.txtSignature
			,@numPhone=numPhone
		FROM 
			UserMaster U 
		JOIN 
			AdditionalContactsInformation A 
		ON 
			A.numContactID=U.numUserDetailId 
		WHERE 
			U.numDomainID=@numDomainID 
			AND numUserDetailId=@numRecOwner
	END

	IF @numAssignedTo=0 --As per Auto Routing Rule
	BEGIN
		DECLARE @numRuleID NUMERIC	 
				
		SELECT @numRuleID=numRoutID FROM RoutingLeads  WHERE numDomainID=@numDomainID and bitDefault=1  

		SELECT 
			@numAssignedTo=rd.numEmpId           
		FROM 
			RoutingLeads r 
		INNER JOIN 
			Routingleaddetails rd 
		ON 
			r.numRoutID=rd.numRoutID
		WHERE 
			r.numRoutID=@numRuleID
	END

	SELECT 
		@numContactId AS numContactId
		,@numRecOwner AS numRecOwner
		,@numAssignedTo AS numAssignedTo
		,@numDivisionId AS numDivisionId
		,@vcEmailID AS vcEmailID
		,ISNULL((SELECT vcPSMTPDisplayName FROM Domain WHERE numDomainId=@numDomainID),@ContactName) AS ContactName
		,@bitSMTPServer AS bitSMTPServer
		,@vcSMTPServer AS vcSMTPServer
		,@numSMTPPort AS numSMTPPort
		,@bitSMTPAuth AS bitSMTPAuth
		,@vcSmtpPassword AS vcSmtpPassword
		,@bitSMTPSSL AS bitSMTPSSL
		,@txtSignature AS Signature
		,@numInternalPM as numInternalPM
		,@numExternalPM as numExternalPM
		,@numNextAssignedTo as numNextAssignedTo
		,@numPhone as numPhone
		,@numOppIDPC as numOppIDPC
		,@numStageIDPC as numStageIDPC
		,@numOppBizDOCIDPC as numOppBizDOCIDPC
		,@numProjectID AS numProjectID
		,@numCaseId AS numCaseId
		,@numCommId AS numCommId
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Import_SaveContactDetails')
DROP PROCEDURE USP_Import_SaveContactDetails
GO
CREATE PROCEDURE [dbo].[USP_Import_SaveContactDetails]  
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@numDivisionID NUMERIC(18,0)
	,@numContactID NUMERIC(18,0)
	,@vcFirstName VARCHAR(50)
	,@vcLastName VARCHAR(50)
	,@vcEmail VARCHAR(80)
	,@numContactType NUMERIC(18,0)
	,@numPhone VARCHAR(15)
	,@numPhoneExtension VARCHAR(7)
	,@numCell VARCHAR(15)
	,@numHomePhone VARCHAR(15)
	,@vcFax VARCHAR(15)
	,@vcCategory NUMERIC(18,0)
	,@numTeam NUMERIC(18,0)
	,@vcPosition NUMERIC(18,0)
	,@vcTitle VARCHAR(100)
	,@vcDepartment NUMERIC(18,0)
	,@txtNotes NVARCHAR(MAX)
	,@bitIsPrimaryContact BIT
	,@vcStreet VARCHAR(100)
	,@vcCity VARCHAR(50)
	,@numState NUMERIC(18,0)
	,@numCountry NUMERIC(18,0)
	,@vcPostalCode VARCHAR(15)
AS
BEGIN
	-- IMPORTANT: PLEASE MAKE SURE NEW FIELD WHICH YOUR ARE ADDING IN STORE PROCEDURE FOR UPDATE ARE ADDED IN SELECT IN PROCEDURE USP_Import_GetContactDetails

	IF LEN(ISNULL(@vcFirstName,'')) = 0
	BEGIN
		SET @vcFirstName = '-'
	END

	IF LEN(ISNULL(@vcLastName,'')) = 0
	BEGIN
		SET @vcLastName = '-'
	END

	--IF LEN(ISNULL(@vcEmail,'')) > 0
	--BEGIN
	--	IF EXISTS (SELECT numContactId FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numContactId <> ISNULL(@numcontactId,0) AND vcEmail=@vcEmail)
	--	BEGIN
	--		RAISERROR('DUPLICATE_EMAIL',16,1)
	--		RETURN
	--	END
	--END

	IF ISNULL(@numDivisionID,0) > 0
	BEGIN
		IF ISNULL(@numContactID,0) = 0
		BEGIN
			IF(SELECT COUNT(numContactId) FROM AdditionalContactsInformation WHERE numDivisionId=@numDivisionID)=0
			BEGIN
				SET @bitIsPrimaryContact = 1
			END

			IF ISNULL(@numContactType,0) = 0
			BEGIN
				SET @numContactType = 70
			END

			INSERT into AdditionalContactsInformation                                    
			(                  
				numContactType,                  
				vcDepartment,                  
				vcCategory,                  
				vcGivenName,                  
				vcFirstName,                  
				vcLastName,                   
				numDivisionId,                  
				numPhone,                  
				numPhoneExtension,                  
				numCell,                  
				numHomePhone,                          
				vcFax,                  
				vcEmail,                
				vcPosition,                  
				txtNotes,                  
				numCreatedBy,                                    
				bintCreatedDate,             
				numModifiedBy,            
				bintModifiedDate,                   
				numDomainID,               
				numRecOwner,                  
				numTeam,                  
				numEmpStatus,
				vcTitle
				,bitPrimaryContact     
			)                                    
			VALUES                                    
			(                  
				@numContactType,                  
				@vcDepartment,                  
				@vcCategory,                  
				CONCAT(@vcLastName,', ',@vcFirstName,'.eml'),                  
				@vcFirstName ,                  
				@vcLastName,                   
				@numDivisionId ,                  
				@numPhone ,                                    
				@numPhoneExtension,                  
				@numCell ,                  
				@NumHomePhone ,                  
				@vcFax ,                  
				@vcEmail ,                  
				@vcPosition,                                   
				@txtNotes,                  
				@numUserCntID,                  
				GETUTCDATE(),             
				@numUserCntID,                  
				GETUTCDATE(),                   
				@numDomainID,                  
				@numUserCntID,                   
				@numTeam,                  
				658,
				@vcTitle
				,@bitIsPrimaryContact                
			)                                    
                     
			SET @numContactID= SCOPE_IDENTITY()


			DECLARE @bitAutoPopulateAddress BIT 
			DECLARE @tintPoulateAddressTo TINYINT 

			SELECT 
				@bitAutoPopulateAddress = ISNULL(bitAutoPopulateAddress,'0')
				,@tintPoulateAddressTo = ISNULL(tintPoulateAddressTo,'0') 
			FROM 
				Domain 
			WHERE 
				numDomainId = @numDomainId
			
			IF (@bitAutoPopulateAddress = 1)
			BEGIN
				IF(@tintPoulateAddressTo = 1) --if primary address is not specified then getting billing address    
				BEGIN
					SELECT                   
						@vcStreet = AD1.vcStreet,
						@vcCity= AD1.vcCity,
						@numState= AD1.numState,
						@vcPostalCode= AD1.vcPostalCode,
						@numCountry= AD1.numCountry              
					FROM 
						DivisionMaster  DM              
					LEFT JOIN 
						dbo.AddressDetails AD1 
					ON 
						AD1.numDomainID=DM.numDomainID 
						AND AD1.numRecordID=DM.numDivisionID 
						AND AD1.tintAddressOf=2 
						AND AD1.tintAddressType=1 
						AND AD1.bitIsPrimary=1
					WHERE 
						numDivisionID=@numDivisionId    
				END  
				ELSE IF (@tintPoulateAddressTo = 2)-- Primary Address is Shipping Address
				BEGIN
					SELECT                   
						@vcStreet= AD2.vcStreet,
						@vcCity=AD2.vcCity,
						@numState=AD2.numState,
						@vcPostalCode=AD2.vcPostalCode,
						@numCountry=AD2.numCountry                   
					FROM 
						DivisionMaster DM
					LEFT JOIN 
						dbo.AddressDetails AD2 
					ON 
						AD2.numDomainID=DM.numDomainID 
						AND AD2.numRecordID= DM.numDivisionID 
						AND AD2.tintAddressOf=2 
						AND AD2.tintAddressType=2 
						AND AD2.bitIsPrimary=1
					WHERE 
						numDivisionID=@numDivisionId
				END  
			END

			INSERT INTO dbo.AddressDetails 
			(
	 			vcAddressName,
	 			vcStreet,
	 			vcCity,
	 			vcPostalCode,
	 			numState,
	 			numCountry,
	 			bitIsPrimary,
	 			tintAddressOf,
	 			tintAddressType,
	 			numRecordID,
	 			numDomainID
			) 
			VALUES
			(
				'Primary'
				,@vcStreet
				,@vcCity
				,@vcPostalCode
				,@numState
				,@numCountry
				,1
				,1
				,0
				,@numContactID
				,@numDomainID
			)                  
		END
		ELSE
		BEGIN
			UPDATE 
				AdditionalContactsInformation 
			SET                                    
				numContactType=@numContactType,                                    
				vcGivenName=CONCAT(@vcLastName,', ',@vcFirstName,'.eml'),
				vcFirstName=@vcFirstName,
				vcLastName =@vcLastName,
				numDivisionId =@numDivisionId,
				numPhone=@numPhone,
				numPhoneExtension=@numPhoneExtension,
				numCell =@numCell,
				NumHomePhone =@NumHomePhone,
				vcFax=@vcFax,
				vcEmail=@vcEmail,
				vcPosition=@vcPosition,
				txtNotes=@txtNotes,
				numModifiedBy=@numUserCntID,
				bintModifiedDate=GETUTCDATE(),
				bitPrimaryContact=@bitIsPrimaryContact
			WHERE 
				numContactId=@numContactId
		END

		DECLARE @numCount AS NUMERIC(18,0) = 0

		IF ISNULL(@vcEmail,'') <> ''
		BEGIN 
			SELECT @numCount = COUNT(*) FROM dbo.EmailMaster WHERE vcEmailID =@vcEmail AND numDomainId=@numDomainID

			IF @numCount = 0 
			BEGIN
				INSERT INTO EmailMaster(vcEmailId,vcName,numDomainId,numContactId)
				VALUES(@vcEmail,@vcFirstName,@numDomainID,@numcontactId)
			END
		END 
		ELSE
		BEGIN
			UPDATE EmailMaster set numContactId=@numcontactId WHERE vcEmailID=@vcEmail and numDomainId=@numDomainID
		END
	END

	SELECT ISNULL(@numContactID,0) AS numContactID             
END

/****** Object:  StoredProcedure [dbo].[USP_InsertCartItem]    Script Date: 11/08/2011 17:53:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_InsertCartItem' ) 
                    DROP PROCEDURE USP_InsertCartItem
                    Go
CREATE  PROCEDURE [dbo].[USP_InsertCartItem]
(
    @numUserCntId NUMERIC(18, 0),
    @numDomainId NUMERIC(18, 0),
    @vcCookieId VARCHAR(100),
    @numOppItemCode NUMERIC(18, 0),
    @numItemCode NUMERIC(18, 0),
    @numUnitHour NUMERIC(18, 0),
    @monPrice DECIMAL(20,5),
    @numSourceId NUMERIC(18, 0),
    @vcItemDesc VARCHAR(2000),
    @numWarehouseId NUMERIC(18, 0),
    @vcItemName VARCHAR(200),
    @vcWarehouse VARCHAR(200),
    @numWarehouseItmsID NUMERIC(18, 0),
    @vcItemType VARCHAR(200),
    @vcAttributes VARCHAR(100),
    @vcAttrValues VARCHAR(100),
    @bitFreeShipping BIT,
    @numWeight NUMERIC(18, 2),
    @tintOpFlag TINYINT,
    @bitDiscountType BIT,
    @fltDiscount DECIMAL(18,2),
    @monTotAmtBefDiscount DECIMAL(20,5),
    @ItemURL VARCHAR(200),
    @numUOM NUMERIC(18, 0),
    @vcUOMName VARCHAR(200),
    @decUOMConversionFactor DECIMAL(18, 5),
    @numHeight NUMERIC(18, 0),
    @numLength NUMERIC(18, 0),
    @numWidth NUMERIC(18, 0),
    @vcShippingMethod VARCHAR(200),
    @numServiceTypeId NUMERIC(18, 0),
    @decShippingCharge DECIMAL(18, 2),
    @numShippingCompany NUMERIC(18, 0),
    @tintServicetype TINYINT,
    @dtDeliveryDate DATETIME,
    @monTotAmount DECIMAL(20,5),
	@numSiteID NUMERIC(18,0) = 0,
	@numParentItemCode NUMERIC(18,0) = 0,
	@vcChildKitItemSelection VARCHAR(MAX) = '',
	@vcPreferredPromotions VARCHAR(MAX)=''
)
AS 
BEGIN TRY
BEGIN TRANSACTION;
	DECLARE @numPreferredPromotionID NUMERIC(18,0)
	DECLARE @hDocItem INT
	DECLARE @TablePreferredPromotion TABLE
	(
		numItemCode NUMERIC(18,0)
		,numPromotionID NUMERIC(18,0)
	)

	IF ISNULL(@vcPreferredPromotions,'') <> ''
	BEGIN
		EXEC sp_xml_preparedocument @hDocItem OUTPUT, @vcPreferredPromotions

		INSERT INTO @TablePreferredPromotion
		(
			numItemCode
			,numPromotionID
		)
		SELECT
			numItemCode
			,numPromotionID
		FROM 
			OPENXML (@hDocItem,'/NewDataSet/Item',2)                                                                          
		WITH                       
		(                                                                          
			numItemCode NUMERIC(18,0)
			,numPromotionID NUMERIC(18,0)
		)

		EXEC sp_xml_removedocument @hDocItem

		SET @numPreferredPromotionID = ISNULL((SELECT TOP 1 numPromotionID  FROM @TablePreferredPromotion WHERE numItemCode=@numItemCode),0)
	END
	
	INSERT INTO [dbo].[CartItems]
    (
        numUserCntId,
        numDomainId,
        vcCookieId,
        numOppItemCode,
        numItemCode,
        numUnitHour,
        monPrice,
        numSourceId,
        vcItemDesc,
        numWarehouseId,
        vcItemName,
        vcWarehouse,
        numWarehouseItmsID,
        vcItemType,
        vcAttributes,
        vcAttrValues,
        bitFreeShipping,
        numWeight,
        tintOpFlag,
        bitDiscountType,
        fltDiscount,
        monTotAmtBefDiscount,
        ItemURL,
        numUOM,
        vcUOMName,
        decUOMConversionFactor,
        numHeight,
        numLength,
        numWidth,
        vcShippingMethod,
        numServiceTypeId,
        decShippingCharge,
        numShippingCompany,
        tintServicetype,
        monTotAmount,
		numParentItem
		,vcChildKitItemSelection
		,monInsertPrice
		,fltInsertDiscount
		,bitInsertDiscountType
		,numPreferredPromotionID
    )
	VALUES  
	(
        @numUserCntId,
        @numDomainId,
        @vcCookieId,
        @numOppItemCode,
        @numItemCode,
        @numUnitHour,
        @monPrice,
        @numSourceId,
        @vcItemDesc,
        @numWarehouseId,
        @vcItemName,
        @vcWarehouse,
        @numWarehouseItmsID,
        @vcItemType,
        @vcAttributes,
        @vcAttrValues,
        @bitFreeShipping,
        @numWeight,
        @tintOpFlag,
        @bitDiscountType,
        @fltDiscount,
        @monTotAmtBefDiscount,
        @ItemURL,
        @numUOM,
        @vcUOMName,
        @decUOMConversionFactor,
        @numHeight,
        @numLength,
        @numWidth,
        @vcShippingMethod,
        @numServiceTypeId,
        @decShippingCharge,
        @numShippingCompany,
        @tintServicetype,
        @monTotAmount,
		@numParentItemCode,
		@vcChildKitItemSelection,
		@monPrice,
		@fltDiscount,
		@bitDiscountType,
		@numPreferredPromotionID
    )

	DECLARE @numCartId NUMERIC(18,0)
	SET @numCartId = SCOPE_IDENTITY()

	IF (SELECT COUNT(*) FROM PromotionOffer WHERE numDomainId=@numDomainId AND ISNULL(bitEnabled,0)=1) > 0
	BEGIN
		
		DECLARE @numPromotionID NUMERIC(18,0)
		DECLARE @vcPromotionDescription VARCHAR(300)
		DECLARE @numDivisionID NUMERIC(18,0)
		DECLARE @numRelationship NUMERIC(18,0)
		DECLARE @numProfile NUMERIC(18,0)

		SET @numDivisionID = ISNULL((SELECT numDivisionId FROM AdditionalContactsInformation WHERE numContactId=@numUserCntId),0)

		SELECT 
			@numRelationship=numCompanyType,
			@numProfile=vcProfile
		FROM 
			DivisionMaster D                  
		JOIN 
			CompanyInfo C 
		ON 
			C.numCompanyId=D.numCompanyID                  
		WHERE 
			numDivisionID =@numDivisionID 

		DECLARE @numItemClassification AS NUMERIC(18,0)
		SELECT @numItemClassification = numItemClassification FROM Item WHERE Item.numItemCode = @numItemCode

		SELECT TOP 1
			@numPromotionID = numProId 
			,@vcPromotionDescription = ISNULL(vcShortDesc,'-')
		FROM 
			PromotionOffer PO
		WHERE 
			numDomainId=@numDomainID 
			AND (numProId=@numPreferredPromotionID OR ISNULL(@numPreferredPromotionID,0)=0)
			AND ISNULL(bitEnabled,0)=1 
			AND ISNULL(IsOrderBasedPromotion,0) = 0
			AND 1 = (CASE WHEN bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN GETUTCDATE()>=dtValidFrom AND GETUTCDATE()<=dtValidTo THEN 1 ELSE 0 END) END)
			AND 1 = (CASE 
						WHEN ISNULL(numOrderPromotionID,0) > 0
						THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=numOrderPromotionID AND tintType=2 AND numRelationship=@numRelationship AND numProfile=@numProfile) > 0 THEN 1 ELSE 0 END)
						ELSE
							(CASE tintCustomersBasedOn 
								WHEN 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numProId AND tintType=1 AND numDivisionID=@numDivisionID) > 0 THEN 1 ELSE 0 END)
								WHEN 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numProId AND tintType=2 AND numRelationship=@numRelationship AND numProfile=@numProfile) > 0 THEN 1 ELSE 0 END)
								WHEN 3 THEN 1
								ELSE 0
							END)
					END)
			AND 1 = (CASE 
						WHEN tintOfferBasedOn = 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 AND numValue=@numItemCode) > 0 THEN 1 ELSE 0 END)
						WHEN tintOfferBasedOn = 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=2 AND numValue=@numItemClassification) > 0 THEN 1 ELSE 0 END)
						WHEN tintOfferBasedOn = 4 THEN 1 
						ELSE 0
					END)
			AND 1 = (CASE 
						WHEN ISNULL(tintOfferTriggerValueType,1) = 1 --Based on quantity
						THEN
							CASE
								WHEN ISNULL(tintOfferTriggerValueTypeRange,1) = 2
								THEN
									(CASE WHEN ISNULL(@numUnitHour,0) BETWEEN ISNULL(fltOfferTriggerValue,0) AND ISNULL(fltOfferTriggerValueRange,0) THEN 1 ELSE 0 END)
								ELSE
									(CASE WHEN ISNULL(@numUnitHour,0) >= fltOfferTriggerValue THEN 1 ELSE 0 END)
							END
						WHEN ISNULL(tintOfferTriggerValueType,1) = 2 --Based on amount
						THEN
							CASE
								WHEN ISNULL(tintOfferTriggerValueTypeRange,1) = 2
								THEN
									(CASE WHEN ISNULL(@monTotAmount,0) BETWEEN ISNULL(fltOfferTriggerValue,0) AND ISNULL(fltOfferTriggerValueRange,0) THEN 1 ELSE 0 END)
								ELSE
									(CASE WHEN ISNULL(@monTotAmount,0) >= fltOfferTriggerValue THEN 1 ELSE 0 END)
							END
					END)
		ORDER BY
			(CASE 
				WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 1 THEN 1
				WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 1 THEN 2
				WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 2 THEN 3
				WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 2 THEN 4
				WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 3 THEN 5
				WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 3 THEN 6
			END) ASC,ISNULL(PO.fltOfferTriggerValue,0) DESC

		IF @numPromotionID > 0 AND (SELECT COUNT(*) FROM CartItems WHERE numDomainId=@numDomainId AND numUserCntId=@numUserCntId AND vcCookieId=@vcCookieId AND ISNULL(PromotionID,0) = @numPromotionID) = 0
		BEGIN
			UPDATE
				CartItems
			SET
				PromotionID=@numPromotionID
				,PromotionDesc=@vcPromotionDescription
				,bitPromotionTrigerred=1
			WHERE
				numCartId=@numCartId
				AND ISNULL(PromotionID,0) = 0
		END

		EXEC USP_PromotionOffer_ApplyItemPromotionToECommerce @numDomainID,@numUserCntId,@numSiteID,@vcCookieId,0,''
	END

	SELECT @numCartId AS numCartID

 COMMIT TRANSACTION;
  END TRY
  BEGIN CATCH
    IF @@TRANCOUNT > 0
    ROLLBACK TRANSACTION;

    DECLARE @ErrorNumber INT = ERROR_NUMBER();
    DECLARE @ErrorLine INT = ERROR_LINE();
    DECLARE @ErrorMessage NVARCHAR(4000) = ERROR_MESSAGE();
    DECLARE @ErrorSeverity INT = ERROR_SEVERITY();
    DECLARE @ErrorState INT = ERROR_STATE();

    PRINT 'Actual error number: ' + CAST(@ErrorNumber AS VARCHAR(10));
    PRINT 'Actual line number: ' + CAST(@ErrorLine AS VARCHAR(10));

    RAISERROR(@ErrorMessage, @ErrorSeverity, @ErrorState);
  END CATCH
            
--Created By Anoop Jayaraj          
--exec USP_ItemDetailsForEcomm @numItemCode=859064,@numWareHouseID=1074,@numDomainID=172,@numSiteId=90
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_itemdetailsforecomm')
DROP PROCEDURE USP_ItemDetailsForEcomm
GO
CREATE PROCEDURE [dbo].[USP_ItemDetailsForEcomm]
@numItemCode as numeric(9),          
@numWareHouseID as numeric(9),    
@numDomainID as numeric(9),
@numSiteId as numeric(9),
@vcCookieId as VARCHAR(MAX)=null,
@numUserCntID AS NUMERIC(9)=0,
@numDivisionID AS NUMERIC(18,0) = 0                                        
AS     
BEGIN
/*Removed warehouse parameter, and taking Default warehouse by default from DB*/
    
	DECLARE @bitShowInStock AS BIT        
	DECLARE @bitShowQuantity AS BIT
	DECLARE @bitAutoSelectWarehouse AS BIT              
	DECLARE @numDefaultWareHouseID AS NUMERIC(9)
	DECLARE @numDefaultRelationship AS NUMERIC(18,0)
	DECLARE @numDefaultProfile AS NUMERIC(18,0)
	DECLARE @tintPreLoginPriceLevel AS TINYINT
	DECLARE @tintPriceLevel AS TINYINT
	DECLARE @bitMatrix BIT
	DECLARE @numItemGroup NUMERIC(18,0)
	SET @bitShowInStock=0        
	SET @bitShowQuantity=0        
       
        
	IF ISNULL(@numDivisionID,0) > 0
	BEGIN
		SELECT 
			@numDefaultRelationship=ISNULL(numCompanyType,0)
			,@numDefaultProfile=ISNULL(vcProfile,0)
			,@tintPriceLevel=ISNULL(tintPriceLevel,0) 
		FROM 
			DivisionMaster 
		INNER JOIN 
			CompanyInfo 
		ON 
			DivisionMaster.numCompanyID=CompanyInfo.numCompanyId 
		WHERE 
			numDivisionID=@numDivisionID 
			AND DivisionMaster.numDomainID=@numDomainID
	END     
      
	--KEEP THIS BELOW ABOVE IF CONDITION 
	SELECT 
		@bitShowInStock=ISNULL(bitShowInStock,0)
		,@bitShowQuantity=ISNULL(bitShowQOnHand,0)
		,@bitAutoSelectWarehouse=ISNULL(bitAutoSelectWarehouse,0)
		,@numDefaultRelationship=(CASE WHEN ISNULL(@numDivisionID,0) > 0 THEN @numDefaultRelationship ELSE numRelationshipId END)
		,@numDefaultProfile=(CASE WHEN ISNULL(@numDivisionID,0) > 0 THEN @numDefaultProfile ELSE numProfileId END)
		,@tintPreLoginPriceLevel=(CASE WHEN ISNULL(bitShowPriceUsingPriceLevel,0)=1 THEN (CASE WHEN @numDivisionID > 0 THEN (CASE WHEN ISNULL(bitShowPriceUsingPriceLevel,0)=1 THEN ISNULL(@tintPriceLevel,0) ELSE 0 END) ELSE ISNULL(tintPreLoginProceLevel,0) END) ELSE 0 END)
	FROM 
		eCommerceDTL 
	WHERE 
		numDomainID=@numDomainID

	IF @numWareHouseID=0
	BEGIN
		SELECT @numDefaultWareHouseID=ISNULL(numDefaultWareHouseID,0) FROM [eCommerceDTL] WHERE [numDomainID]=@numDomainID	
		SET @numWareHouseID =@numDefaultWareHouseID;
	END

	DECLARE @vcWarehouseIDs AS VARCHAR(1000)
	IF @bitAutoSelectWarehouse = 1
	BEGIN		
		SELECT @vcWarehouseIDs = ISNULL( STUFF((SELECT DISTINCT ',' + CAST(numWarehouseID AS VARCHAR(10)) FROM [dbo].[Warehouses] WHERE [Warehouses].[numDomainID] = @numDomainID FOR XML PATH('')),1, 1, '') , '')
	END
	ELSE
	BEGIN
		SET @vcWarehouseIDs = @numWareHouseID
	END

	/*If qty is not found in default warehouse then choose next warehouse with sufficient qty */
	IF @bitAutoSelectWarehouse = 1 AND ( SELECT COUNT(*) FROM dbo.WareHouseItems WHERE  numItemID = @numItemCode AND numWarehouseID= @numWareHouseID ) = 0 
	BEGIN
		SELECT TOP 1 
			@numDefaultWareHouseID = numWareHouseID 
		FROM 
			dbo.WareHouseItems 
		WHERE 
			numItemID = @numItemCode 
			AND numOnHand > 0 
		ORDER BY 
			numOnHand DESC 
		
		IF (ISNULL(@numDefaultWareHouseID,0)>0)
			SET @numWareHouseID =@numDefaultWareHouseID;
	END

    DECLARE @UOMConversionFactor AS DECIMAL(18,5)
      
    SELECT 
		@UOMConversionFactor= ISNULL(dbo.fn_UOMConversion(ISNULL(I.numSaleUnit,0),I.numItemCode,I.numDomainId,ISNULL(I.numBaseUnit,0)),0)
		,@bitMatrix=ISNULL(bitMatrix,0)
		,@numItemGroup=ISNULL(numItemGroup,0)
    FROM 
		Item I 
	WHERE 
		numItemCode=@numItemCode   	
 
	DECLARE @strSql AS VARCHAR(MAX)
	
	SET @strSql = CONCAT('WITH tblItem AS 
							(SELECT 
								I.numItemCode
								,vcItemName
								,txtItemDesc
								,charItemType
								,ISNULL(I.bitKitParent,0) bitKitParent
								,ISNULL(bitCalAmtBasedonDepItems,0) bitCalAmtBasedonDepItems
								,ISNULL(I.bitMatrix,0) bitMatrix,(CASE WHEN (ISNULL(I.bitKitParent,0) = 1 OR ISNULL(I.bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1 THEN dbo.GetCalculatedPriceForKitAssembly(I.numDomainID,I.numItemCode,0,ISNULL(I.tintKitAssemblyPriceBasedOn,1),0,0,'''') ELSE 
								',(CASE 
									WHEN @tintPreLoginPriceLevel > 0 
									THEN CONCAT('ISNULL(PricingOption.monPrice,ISNULL(CASE WHEN I.[charItemType]=''P'' THEN ',@UOMConversionFactor,' * W.[monWListPrice] ELSE ',@UOMConversionFactor,' * monListPrice END,0))') 
									ELSE CONCAT('ISNULL(CASE WHEN I.[charItemType]=''P'' THEN ',@UOMConversionFactor,' * W.[monWListPrice] ELSE ',@UOMConversionFactor,' * monListPrice END,0)')
								END),'END) AS monListPrice
								,(CASE WHEN (ISNULL(I.bitKitParent,0) = 1 OR ISNULL(I.bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1 THEN dbo.GetCalculatedPriceForKitAssembly(I.numDomainID,I.numItemCode,0,ISNULL(I.tintKitAssemblyPriceBasedOn,1),0,0,'''') ELSE ISNULL(CASE WHEN I.[charItemType]=''P'' 
											THEN ',@UOMConversionFactor,' * W.[monWListPrice]  
											ELSE ',@UOMConversionFactor,' * monListPrice 
										END,0) END) AS monMSRP
										,numItemClassification
										,bitTaxable
										,vcSKU
										,I.numModifiedBy
										,(SELECT numItemImageId,vcPathForImage,vcPathForTImage,bitDefault,case when bitdefault=1  then -1 else isnull(intDisplayOrder,0) end as intDisplayOrder FROM ItemImages  WHERE numItemCode=I.numItemCode and bitIsImage = 1 order by case when bitdefault=1 then -1 else isnull(intDisplayOrder,0) end  asc FOR XML AUTO,ROOT(''Images''))  vcImages
										,(SELECT vcPathForImage FROM ItemImages  WHERE numItemCode=I.numItemCode and bitIsImage = 0 order by intDisplayOrder  asc FOR XML AUTO,ROOT(''Documents''))  vcDocuments
										,ISNULL(bitSerialized,0) as bitSerialized
										,vcModelID
										,numItemGroup
										,(ISNULL(sum(numOnHand),0) / ',@UOMConversionFactor,') as numOnHand
										,sum(numOnOrder) as numOnOrder,                    
										sum(numReorder)  as numReorder,                    
										sum(numAllocation)  as numAllocation,                    
										sum(numBackOrder)  as numBackOrder,              
										isnull(fltWeight,0) as fltWeight,              
										isnull(fltHeight,0) as fltHeight,              
										isnull(fltWidth,0) as fltWidth,              
										isnull(fltLength,0) as fltLength,              
										case when charItemType=''P''then isnull(bitFreeShipping,0) else 1 end as bitFreeShipping,
										Case When (case when charItemType=''P''then isnull(bitFreeShipping,0) else 1 end )=1 then ''Free shipping'' else ''Calculated at checkout'' end as Shipping,              
										isnull(bitAllowBackOrder,0) as bitAllowBackOrder,bitShowInStock,bitShowQOnHand,isnull(numWareHouseItemID,0)  as numWareHouseItemID,
										(CASE WHEN charItemType<>''S'' AND charItemType<>''N'' AND ISNULL(bitKitParent,0)=0 THEN         
										 (CASE 
											WHEN I.bitAllowBackOrder = 1 THEN 1
											WHEN (ISNULL(WAll.numTotalOnHand,0)<=0) THEN 0
											ELSE 1
										END) ELSE 1 END ) as bitInStock,
										(
											CASE WHEN ISNULL(bitKitParent,0) = 1
											THEN
												(CASE 
													WHEN ((SELECT COUNT(*) FROM ItemDetails ID INNER JOIN Item IInner ON ID.numChildItemID = IInner.numItemCode WHERE ID.numItemKitID = I.numItemCode AND ISNULL(IInner.bitKitParent,0) = 1)) > 0 
													THEN 1 
													ELSE 0 
												END)
											ELSE
												0
											END
										) bitHasChildKits,
										(case when charItemType<>''S'' AND charItemType<>''N'' AND ISNULL(bitKitParent,0)=0 then         
										 (Case when ',@bitShowInStock,'=1  then 
										 (CASE 
											WHEN I.bitAllowBackOrder = 1 AND I.numDomainID <> 172 AND ISNULL(WAll.numTotalOnHand,0)<=0 AND ISNULL(I.numVendorID,0) > 0 AND dbo.GetVendorPreferredMethodLeadTime(I.numVendorID) > 0 THEN CONCAT(''<font class="vendorLeadTime" style="color:green">Usually ships in '',dbo.GetVendorPreferredMethodLeadTime(I.numVendorID),'' days</font>'')
											WHEN I.bitAllowBackOrder = 1 AND ISNULL(WAll.numTotalOnHand,0)<=0 THEN (CASE WHEN I.numDomainID = 172 THEN ''<font style="color:green">AVAILABLE</font>'' ELSE ''<font style="color:green">In Stock</font>'' END)
											WHEN (ISNULL(WAll.numTotalOnHand,0)<=0) THEN (CASE WHEN I.numDomainID = 172 THEN ''<font style="color:red">ON HOLD</font>'' ELSE ''<font style="color:red">Out Of Stock</font>'' END)
											ELSE (CASE WHEN I.numDomainID = 172 THEN ''<font style="color:green">AVAILABLE</font>'' ELSE ''<font style="color:green">In Stock</font>'' END)  
										END) else '''' end) ELSE '''' END ) as InStock,
										ISNULL(numSaleUnit,0) AS numUOM,
										ISNULL(vcUnitName,'''') AS vcUOMName,
										 ',@UOMConversionFactor,' AS UOMConversionFactor,
										I.numCreatedBy,
										I.numBarCodeId,
										I.[vcManufacturer],
										(SELECT  COUNT(*) FROM  Review where vcReferenceId = CONVERT(VARCHAR(50) ,I.numItemCode) and numDomainId = I.numDomainId and numSiteId = ', @numSiteId,' AND bitHide = 0 ) as ReviewCount ,
										(SELECT  COUNT(*) FROM  Ratings where vcReferenceId = CONVERT(VARCHAR(50) ,I.numItemCode) and numDomainId = I.numDomainId and numSiteId = ', @numSiteId,'  ) as RatingCount ,
										(select top 1 vcMetaKeywords  from MetaTags where numReferenceID = I.numItemCode and tintMetaTagFor =2) as vcMetaKeywords,
										(select top 1 vcMetaDescription  from MetaTags where numReferenceID = I.numItemCode and tintMetaTagFor =2) as vcMetaDescription ,
										(SELECT vcCategoryName  FROM  dbo.Category WHERE numCategoryID =(SELECT TOP 1 numCategoryID FROM dbo.ItemCategory WHERE numItemID = I.numItemCode)) as vcCategoryName
										,ISNULL(TablePromotion.numTotalPromotions,0) numTotalPromotions
										,ISNULL(TablePromotion.vcPromoDesc,'''') vcPromoDesc
										,ISNULL(TablePromotion.bitRequireCouponCode,0) bitRequireCouponCode
										from Item I 
										OUTER APPLY (SELECT * FROM dbo.fn_GetItemPromotions(',@numDomainID,',',@numDivisionID,',I.numItemCode,I.numItemClassification',',',@numDefaultRelationship,',',@numDefaultProfile,',2)) TablePromotion ' + 
										(CASE WHEN @tintPreLoginPriceLevel > 0 THEN CONCAT(' OUTER APPLY(SELECT decDiscount AS monPrice FROM PricingTable WHERE tintRuleType=3 AND numItemCode=I.numItemCode ORDER BY numPricingID OFFSET ',@tintPreLoginPriceLevel-1,' ROWS FETCH NEXT 1 ROWS ONLY) As PricingOption ') ELSE '' END)
										,'                 
										left join  WareHouseItems W                
										on W.numItemID=I.numItemCode and numWareHouseID= ',@numWareHouseID,'
										 OUTER APPLY (SELECT SUM(numOnHand) numTotalOnHand FROM WareHouseItems W 
																							  WHERE  W.numItemID = I.numItemCode
																							  AND numWareHouseID IN (SELECT [OutParam] FROM dbo.[SplitString](''',@vcWarehouseIDs,''','',''))
																							  AND W.numDomainID = ',@numDomainID,') AS WAll
										left join  eCommerceDTL E          
										on E.numDomainID=I.numDomainID     
										LEFT JOIN UOM u ON u.numUOMId=I.numSaleUnit    
										where ISNULL(I.IsArchieve,0)=0 AND I.numDomainID=',@numDomainID,' AND I.numItemCode=',@numItemCode,
										CASE WHEN (SELECT COUNT(*) FROM [dbo].[EcommerceRelationshipProfile] WHERE numSiteID=@numSiteID AND numRelationship = @numDefaultRelationship AND numProfile = @numDefaultProfile) > 0
										THEN
											CONCAT(' AND I.numItemClassification NOT IN (SELECT numItemClassification FROM EcommerceRelationshipProfile ERP INNER JOIN ECommerceItemClassification EIC ON EIC.numECommRelatiionshipProfileID=ERP.ID WHERE ERP.numSiteID=',@numSiteID,' AND ERP.numRelationship=',@numDefaultRelationship,' AND ERP.numProfile=', @numDefaultProfile,')')
										ELSE
											''
										END
										,'           
										group by I.numItemCode, vcItemName, txtItemDesc, charItemType, monListPrice,numItemClassification, bitTaxable, vcSKU, bitKitParent, bitCalAmtBasedonDepItems, bitAssembly, tintKitAssemblyPriceBasedOn, WAll.numTotalOnHand,         
										I.numDomainID,I.numModifiedBy,  bitSerialized, vcModelID,numItemGroup, fltWeight,fltHeight,fltWidth,          
										fltLength,bitFreeShipping,bitAllowBackOrder,bitShowInStock,bitShowQOnHand ,numWareHouseItemID,vcUnitName,I.numCreatedBy,I.numBarCodeId,W.[monWListPrice],I.[vcManufacturer],
										numSaleUnit, I.numVendorID, I.bitMatrix,numTotalPromotions,vcPromoDesc,bitRequireCouponCode ' + (CASE WHEN @tintPreLoginPriceLevel > 0 THEN ',PricingOption.monPrice' ELSE '' END) + ')')

										set @strSql=@strSql+' select numItemCode,vcItemName,txtItemDesc,charItemType, bitMatrix, monListPrice,monMSRP,numItemClassification, bitTaxable, vcSKU, bitKitParent, bitCalAmtBasedonDepItems,                  
										tblItem.numModifiedBy, vcImages,vcDocuments, bitSerialized, vcModelID, numItemGroup,numOnHand,numOnOrder,numReorder,numAllocation, 
										numBackOrder, fltWeight, fltHeight, fltWidth, fltLength, bitFreeShipping,bitAllowBackOrder,bitShowInStock,
										bitShowQOnHand,numWareHouseItemID,bitInStock,bitHasChildKits,InStock,numUOM,vcUOMName,UOMConversionFactor,tblItem.numCreatedBy,numBarCodeId,vcManufacturer,ReviewCount,RatingCount,Shipping,isNull(vcMetaKeywords,'''') as vcMetaKeywords ,isNull(vcMetaDescription,'''') as vcMetaDescription,isNull(vcCategoryName,'''') as vcCategoryName'

	set @strSql=@strSql+ ' from tblItem ORDER BY numOnHand DESC'
	PRINT @strSql
	exec (@strSql)


	declare @tintOrder as tinyint                                                  
	declare @vcFormFieldName as varchar(50)                                                  
	declare @vcListItemType as varchar(1)                                             
	declare @vcAssociatedControlType varchar(10)                                                  
	declare @numListID AS numeric(9)                                                  
	declare @WhereCondition varchar(2000)                       
	Declare @numFormFieldId as numeric  
	DECLARE @vcFieldType CHAR(1)
	DECLARE @GRP_ID INT
                  
	set @tintOrder=0                                                  
	set @WhereCondition =''                 
                   
              
	CREATE TABLE #tempAvailableFields
	(
		numFormFieldId NUMERIC(9)
		,vcFormFieldName NVARCHAR(50)
		,vcFieldType CHAR(1)
		,vcAssociatedControlType NVARCHAR(50)
		,numListID NUMERIC(18)
		,vcListItemType CHAR(1)
		,intRowNum INT
		,vcItemValue VARCHAR(3000)
		,GRP_ID INT
	)
   
	INSERT INTO #tempAvailableFields 
	(
		numFormFieldId
		,vcFormFieldName
		,vcFieldType
		,vcAssociatedControlType
        ,numListID
		,vcListItemType
		,intRowNum
		,GRP_ID
	)                         
    SELECT 
		Fld_id
		,REPLACE(Fld_label,' ','') AS vcFormFieldName
		,CASE GRP_ID WHEN 4 then 'C' WHEN 1 THEN 'D' ELSE 'C' END AS vcFieldType
		,fld_type as vcAssociatedControlType
		,ISNULL(C.numListID, 0) numListID
		,CASE WHEN C.numListID > 0 THEN 'L' ELSE '' END vcListItemType
		,ROW_NUMBER() OVER (ORDER BY Fld_id asc) AS intRowNum
		,GRP_ID
    FROM 
		CFW_Fld_Master C 
	LEFT JOIN 
		CFW_Validation V 
	ON 
		V.numFieldID = C.Fld_id
    WHERE 
		C.numDomainID = @numDomainId
        AND GRP_ID  = 5

    SELECT TOP 1 
		@tintOrder=intRowNum
		,@numFormFieldId=numFormFieldId
		,@vcFormFieldName=vcFormFieldName
		,@vcFieldType=vcFieldType
		,@vcAssociatedControlType=vcAssociatedControlType
		,@numListID=numListID
		,@vcListItemType=vcListItemType
		,@GRP_ID=GRP_ID
	FROM 
		#tempAvailableFields 
	ORDER BY 
		intRowNum ASC
   
	while @tintOrder>0                                                  
	begin                   
		IF @GRP_ID=5
		BEGIN
			IF @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'
			BEGIN  
				UPDATE #tempAvailableFields SET vcItemValue=(SELECT Fld_Value FROM CFW_FLD_Values_Item WHERE Fld_Id=@numFormFieldId AND RecId=@numItemCode) WHERE numFormFieldId=@numFormFieldId              
			END   
			ELSE IF @vcAssociatedControlType = 'CheckBox'
			BEGIN      
				UPDATE #tempAvailableFields SET vcItemValue=(SELECT case when isnull(Fld_Value,0)=0 then 'No' when isnull(Fld_Value,0)=1 then 'Yes' END FROM CFW_FLD_Values_Item WHERE Fld_Id=@numFormFieldId AND RecId=@numItemCode) WHERE numFormFieldId=@numFormFieldId              
			END                
			ELSE IF @vcAssociatedControlType = 'DateField'           
			BEGIN   
				UPDATE #tempAvailableFields SET vcItemValue=(SELECT dbo.FormatedDateFromDate(Fld_Value,@numDomainId) FROM CFW_FLD_Values_Item WHERE Fld_Id=@numFormFieldId AND RecId=@numItemCode) WHERE numFormFieldId=@numFormFieldId              
			END                
			ELSE IF @vcAssociatedControlType = 'SelectBox'           
			BEGIN 
				UPDATE #tempAvailableFields SET vcItemValue=(SELECT L.vcData FROM CFW_FLD_Values_Item CFW left Join ListDetails L
					on L.numListItemID=CFW.Fld_Value                
					WHERE CFW.Fld_Id=@numFormFieldId AND CFW.RecId=@numItemCode) WHERE numFormFieldId=@numFormFieldId              
			END   
		END    
 
		SELECT TOP 1 
			@tintOrder=intRowNum
			,@numFormFieldId=numFormFieldId
			,@vcFormFieldName=vcFormFieldName
			,@vcFieldType=vcFieldType
			,@vcAssociatedControlType=vcAssociatedControlType
			,@numListID=numListID
			,@vcListItemType=vcListItemType
			,@GRP_ID=GRP_ID
		FROM 
			#tempAvailableFields 
		WHERE 
			intRowNum > @tintOrder 
		ORDER BY 
			intRowNum ASC
 
	   IF @@rowcount=0 set @tintOrder=0                                                  
	END   


	INSERT INTO #tempAvailableFields 
	(
		numFormFieldId
		,vcFormFieldName
		,vcFieldType
		,vcAssociatedControlType
		,numListID
		,vcListItemType
		,intRowNum
		,vcItemValue
		,GRP_ID
	)                         
	SELECT 
		Fld_id
		,REPLACE(Fld_label,' ','') AS vcFormFieldName
		,CASE GRP_ID WHEN 4 then 'C' WHEN 1 THEN 'D' ELSE 'C' END AS vcFieldType
		,fld_type as vcAssociatedControlType
		,ISNULL(C.numListID, 0) numListID
		,CASE WHEN C.numListID > 0 THEN 'L' ELSE '' END vcListItemType
		,ROW_NUMBER() OVER (ORDER BY Fld_id asc) AS intRowNum
		,''
		,GRP_ID
	FROM 
		CFW_Fld_Master C 
	LEFT JOIN 
		CFW_Validation V 
	ON 
		V.numFieldID = C.Fld_id
	WHERE 
		C.numDomainID = @numDomainId
		AND GRP_ID = 9

	IF ISNULL(@bitMatrix,0) = 1 AND ISNULL(@numItemGroup,0) > 0
	BEGIN
		UPDATE
			TEMP
		SET
			TEMP.vcItemValue = FLD_ValueName
		FROM
			#tempAvailableFields TEMP
		INNER JOIN
		(
			SELECT 
				CFW_Fld_Master.fld_id
				,CASE 
					WHEN CFW_Fld_Master.fld_type='SelectBox' THEN dbo.GetListIemName(Fld_Value)
					WHEN CFW_Fld_Master.fld_type='CheckBox' THEN CASE WHEN isnull(Fld_Value,0)=1 THEN 'Yes' ELSE 'No' END
					ELSE CAST(Fld_Value AS VARCHAR)
				END AS FLD_ValueName
			FROM 
				CFW_Fld_Master 
			INNER JOIN
				ItemGroupsDTL 
			ON 
				CFW_Fld_Master.Fld_id = ItemGroupsDTL.numOppAccAttrID
				AND ItemGroupsDTL.tintType = 2
			LEFT JOIN
				ItemAttributes
			ON
				CFW_Fld_Master.Fld_id = ItemAttributes.FLD_ID
				AND ItemAttributes.numItemCode = @numItemCode
			LEFT JOIN
				ListDetails LD
			ON
				CFW_Fld_Master.numlistid = LD.numListID
			WHERE
				CFW_Fld_Master.numDomainID = @numDomainId
				AND ItemGroupsDTL.numItemGroupID = @numItemGroup
			GROUP BY
				CFW_Fld_Master.Fld_label
				,CFW_Fld_Master.fld_id
				,CFW_Fld_Master.fld_type
				,CFW_Fld_Master.bitAutocomplete
				,CFW_Fld_Master.numlistid
				,CFW_Fld_Master.vcURL
				,ItemAttributes.FLD_Value 
		) T1
		ON
			TEMP.numFormFieldId = T1.fld_id
		WHERE
			TEMP.GRP_ID=9
	END
  
	SELECT * FROM #tempAvailableFields

	DROP TABLE #tempAvailableFields
END
GO
--SELECT * FROM Category
--- Alterd By Chintan Prajapati
-- EXEC dbo.USP_ItemsForECommerce 9,60,1,1,15,0,''
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ItemsForECommerce' ) 
    DROP PROCEDURE USP_ItemsForECommerce
GO
CREATE PROCEDURE [dbo].[USP_ItemsForECommerce]
    @numSiteID AS NUMERIC(9) = 0,
    @numCategoryID AS NUMERIC(18) = 0,
    @numWareHouseID AS NUMERIC(9) = 0,
    @SortBy NVARCHAR(MAX) = 0,
    @CurrentPage INT,
    @PageSize INT,
    @TotRecs INT = 0 OUTPUT,
    @SearchText AS NVARCHAR(MAX) = '',--Comma seperated item ids
    @FilterQuery AS NVARCHAR(MAX) = '',----Comma seperated Attributes Ids
    @FilterRegularCondition AS NVARCHAR(MAX),
    @FilterCustomFields AS NVARCHAR(MAX),
    @FilterCustomWhere AS NVARCHAR(MAX),
	@numDivisionID AS NUMERIC(18,0) = 0,
	@numManufacturerID AS NUMERIC(18,0)=0,
	@FilterItemAttributes AS NVARCHAR(MAX)
AS 
BEGIN
	DECLARE @numDomainID AS NUMERIC(9)
	DECLARE @numDefaultRelationship AS NUMERIC(18,0)
	DECLARE @numDefaultProfile AS NUMERIC(18,0)
	DECLARE @tintPreLoginPriceLevel AS TINYINT
	DECLARE @tintPriceLevel AS TINYINT
	DECLARE @tintDisplayCategory AS TINYINT
	DECLARE @bitAutoSelectWarehouse AS BIT

	SELECT  
		@numDomainID = numDomainID
    FROM 
		[Sites]
    WHERE 
		numSiteID = @numSiteID
        
	IF ISNULL(@numDivisionID,0) > 0
	BEGIN
		SELECT 
			@numDefaultRelationship=ISNULL(numCompanyType,0)
			,@numDefaultProfile=ISNULL(vcProfile,0)
			,@tintPriceLevel=ISNULL(tintPriceLevel,0) 
		FROM 
			DivisionMaster 
		INNER JOIN 
			CompanyInfo 
		ON 
			DivisionMaster.numCompanyID=CompanyInfo.numCompanyId 
		WHERE 
			numDivisionID=@numDivisionID 
			AND DivisionMaster.numDomainID=@numDomainID
	END

	--KEEP THIS BELOW ABOVE IF CONDITION
	SELECT 
		@tintDisplayCategory = ISNULL(bitDisplayCategory,0)
		,@numDefaultRelationship=(CASE WHEN ISNULL(@numDivisionID,0) > 0 THEN @numDefaultRelationship ELSE numRelationshipId END)
		,@numDefaultProfile=(CASE WHEN ISNULL(@numDivisionID,0) > 0 THEN @numDefaultProfile ELSE numProfileId END)
		,@tintPreLoginPriceLevel=(CASE WHEN ISNULL(bitShowPriceUsingPriceLevel,0)=1 THEN (CASE WHEN @numDivisionID > 0 THEN (CASE WHEN ISNULL(bitShowPriceUsingPriceLevel,0)=1 THEN ISNULL(@tintPriceLevel,0) ELSE 0 END) ELSE ISNULL(tintPreLoginProceLevel,0) END) ELSE 0 END)
		,@bitAutoSelectWarehouse = ISNULL(bitAutoSelectWarehouse,0)
	FROM 
		dbo.eCommerceDTL 
	WHERE 
		numSiteID = @numSiteId
		
	CREATE TABLE #tmpItemCat(numCategoryID NUMERIC(18,0))
	CREATE TABLE #tmpItemCode(numItemCode NUMERIC(18,0))
		
	IF LEN(@FilterCustomFields) > 0 AND LEN(@FilterCustomWhere) > 0
	BEGIN			
		DECLARE @FilterCustomFieldIds AS NVARCHAR(MAX)
		SET @FilterCustomFieldIds = Replace(@FilterCustomFields,'[','')
		SET @FilterCustomFieldIds = Replace(@FilterCustomFieldIds,']','')
			
		DECLARE @strCustomSql AS NVARCHAR(MAX)
		SET @strCustomSql = 'INSERT INTO #tmpItemCode(numItemCode)
								SELECT DISTINCT T.RecId FROM 
														(
															SELECT * FROM 
																(
																	SELECT  t2.RecId, t1.fld_id, t2.Fld_Value FROM CFW_Fld_Master t1 
																	JOIN CFW_FLD_Values_Item AS t2 ON t1.Fld_ID = t2.Fld_ID 
																	WHERE t1.Grp_id  In (5,9)
																	AND t1.numDomainID = ' + CONVERT(VARCHAR(10),@numDomainID) + '
																	AND t1.fld_type <> ''Link'' 
																	AND t1.fld_id IN (' + @FilterCustomFieldIds + ')
																) p  PIVOT (MAX([Fld_Value]) FOR fld_id IN (' + @FilterCustomFields + ')) AS pvt 
														) T  WHERE 1=1 AND ' + @FilterCustomWhere
								 
		EXEC SP_EXECUTESQL @strCustomSql								 					 
	END
		
    DECLARE @strSQL NVARCHAR(MAX)
    DECLARE @firstRec AS INTEGER
    DECLARE @lastRec AS INTEGER
    DECLARE @Where NVARCHAR(MAX)
    DECLARE @SortString NVARCHAR(MAX)
    DECLARE @searchPositionColumn NVARCHAR(MAX)
	DECLARE @searchPositionColumnGroupBy NVARCHAR(MAX)
    DECLARE @row AS INTEGER
    DECLARE @data AS VARCHAR(100)
    DECLARE @dynamicFilterQuery NVARCHAR(MAX)
    DECLARE @checkFldType VARCHAR(100)
    DECLARE @count NUMERIC(9,0)
    DECLARE @whereNumItemCode NVARCHAR(MAX)
    DECLARE @filterByNumItemCode VARCHAR(100)
        
	SET @SortString = ''
    set @searchPositionColumn = ''
	SET @searchPositionColumnGroupBy = ''
    SET @strSQL = ''
	SET @whereNumItemCode = ''
	SET @dynamicFilterQuery = ''
		 
    SET @firstRec = ( @CurrentPage - 1 ) * @PageSize
    SET @lastRec = ( @CurrentPage * @PageSize + 1 )
                
	DECLARE @vcWarehouseIDs AS VARCHAR(1000)
    IF (@numWareHouseID = 0 OR @bitAutoSelectWarehouse = 1)
    BEGIN
		IF @bitAutoSelectWarehouse = 1
		BEGIN
			SELECT @vcWarehouseIDs = ISNULL( STUFF((SELECT DISTINCT ',' + CAST(numWarehouseID AS VARCHAR(10)) FROM [dbo].[Warehouses] WHERE [Warehouses].[numDomainID] = @numDomainID FOR XML PATH('')),1, 1, '') , '') 
		END
		ELSE
		BEGIN
			SELECT 
				@numWareHouseID = ISNULL(numDefaultWareHouseID,0)
			FROM 
				[eCommerceDTL]
			WHERE 
				[numDomainID] = @numDomainID	

			SET @vcWarehouseIDs = @numWareHouseID
		END
    END
	ELSE
	BEGIN
		SET @vcWarehouseIDs = @numWareHouseID
	END

	--PRIAMRY SORTING USING CART DROPDOWN
	DECLARE @Join AS NVARCHAR(MAX)
	DECLARE @SortFields AS NVARCHAR(MAX)
	DECLARE @vcSort AS VARCHAR(MAX)

	SET @Join = ''
	SET @SortFields = ''
	SET @vcSort = ''

	IF @SortBy = ''
	BEGIN
		SET @SortString = ' vcItemName Asc '
	END
	ELSE IF CHARINDEX('~0',@SortBy) > 0 OR CHARINDEX('~1',@SortBy) > 0
	BEGIN
		DECLARE @fldID AS NUMERIC(18,0)
		DECLARE @RowNumber AS VARCHAR(MAX)
				
		SELECT * INTO #fldValues FROM dbo.SplitIDsWithPosition(@SortBy,'~')
		SELECT @RowNumber = RowNumber, @fldID = id FROM #fldValues WHERE RowNumber = 1
		SELECT @vcSort = CASE WHEN Id = 0 THEN 'ASC' ELSE 'DESC' END FROM #fldValues WHERE RowNumber = 2
				
		SET @Join = @Join + ' LEFT JOIN CFW_FLD_Values_Item CFVI' + @RowNumber + ' ON I.numItemCode = CFVI' + @RowNumber + '.RecID AND CFVI' + @RowNumber + '.fld_Id = ' + CONVERT(VARCHAR(10),@fldID)
		---SET @SortString = 'CFVI' + @RowNumber + '.fld_Value' 
		SET @SortString = '[fld_Value' + @RowNumber + '] ' --+ @vcSort
		SET @SortFields = ', CFVI' + @RowNumber + '.fld_Value [fld_Value' + @RowNumber + ']'
	END
	ELSE
	BEGIN	
		IF CHARINDEX('monListPrice',@SortBy) > 0
		BEGIN
			DECLARE @bitSortPriceMode AS BIT
			SELECT @bitSortPriceMode = ISNULL(bitSortPriceMode,0) FROM dbo.eCommerceDTL WHERE numSiteId = @numSiteID
			
			IF @bitSortPriceMode = 1
			BEGIN
				SET @SortString = ' '
			END
			ELSE
			BEGIN
				SET @SortString = @SortBy	
			END
		END
		ELSE
		BEGIN
			SET @SortString = @SortBy	
		END	
	END	

	--SECONDARY SORTING USING DEFAULT SETTINGS FROM ECOMMERCE
	SELECT 
		ROW_NUMBER() OVER (ORDER BY Custom,vcFieldName) AS [RowID]
		,* 
	INTO 
		#tempSort 
	FROM 
	(
		SELECT 
			CONVERT(VARCHAR(9),numFieldID)+'~0' AS numFieldID,
			ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName,
			0 AS Custom,
			vcDbColumnName 
		FROM 
			View_DynamicColumns
		WHERE 
			numFormID = 84 
			AND ISNULL(bitSettingField,0) = 1 
			AND ISNULL(bitDeleted,0)=0 
			AND ISNULL(bitCustom,0) = 0
			AND numDomainID = @numDomainID	       
		UNION     
		SELECT 
			CONVERT(VARCHAR(9),numFieldID)+'~1' AS numFieldID,
			vcFieldName ,
			1 AS Custom,
			'' AS vcDbColumnName
		FROM 
			View_DynamicCustomColumns          
		WHERE 
			numFormID = 84
			AND numDomainID = @numDomainID
			AND grp_id IN (5,9)
			AND vcAssociatedControlType = 'TextBox' 
			AND ISNULL(bitCustom,0) = 1 
			AND tintPageType=1  
	) TABLE1 
	ORDER BY 
		Custom,vcFieldName
		
	--SELECT * FROM #tempSort
	DECLARE @DefaultSort AS NVARCHAR(MAX)
	DECLARE @DefaultSortFields AS NVARCHAR(MAX)
	DECLARE @DefaultSortJoin AS NVARCHAR(MAX)
	DECLARE @intCnt AS INT
	DECLARE @intTotalCnt AS INT	
	DECLARE @strColumnName AS VARCHAR(100)
	DECLARE @bitCustom AS BIT
	DECLARE @FieldID AS VARCHAR(100)				

	SET @DefaultSort = ''
	SET @DefaultSortFields = ''
	SET @DefaultSortJoin = ''		
	SET @intCnt = 0
	SET @intTotalCnt = ( SELECT COUNT(*) FROM #tempSort )
	SET @strColumnName = ''
		
	IF (SELECT ISNULL(bitEnableSecSorting,0) FROM EcommerceDtl WHERE numSiteID = @numSiteID ) = 1
	BEGIN
		CREATE TABLE #fldDefaultValues(RowNumber INT,Id VARCHAR(1000))

		WHILE(@intCnt < @intTotalCnt)
		BEGIN
			SET @intCnt = @intCnt + 1
			SELECT @FieldID = numFieldID, @strColumnName = vcDbColumnName, @bitCustom = Custom FROM #tempSort WHERE RowID = @intCnt

			IF @bitCustom = 0
			BEGIN
				SET @DefaultSort =  @strColumnName + ',' + @DefaultSort 
			END
			ELSE
			BEGIN
				DECLARE @fldID1 AS NUMERIC(18,0)
				DECLARE @RowNumber1 AS VARCHAR(MAX)
				DECLARE @vcSort1 AS VARCHAR(10)
				DECLARE @vcSortBy AS VARCHAR(1000)
				SET @vcSortBy = CONVERT(VARCHAR(100),@FieldID) + '~' + '0'

				INSERT INTO #fldDefaultValues(RowNumber,Id) SELECT RowNumber, id FROM dbo.SplitIDsWithPosition(@vcSortBy,'~')
				SELECT @RowNumber1 = RowNumber + 100 + @intCnt, @fldID1 = id FROM #fldDefaultValues WHERE RowNumber = 1
				SELECT @vcSort1 = 'ASC' 
						
				SET @DefaultSortJoin = @DefaultSortJoin + ' LEFT JOIN CFW_FLD_Values_Item CFVI' + @RowNumber1 + ' ON I.numItemCode = CFVI' + @RowNumber1 + '.RecID AND CFVI' + @RowNumber1 + '.fld_Id = ' + CONVERT(VARCHAR(10),@fldID1)
				SET @DefaultSort = '[fld_Value' + @RowNumber1 + '] ' + ',' + @DefaultSort 
				SET @DefaultSortFields = @DefaultSortFields + ', CFVI' + @RowNumber1 + '.fld_Value [fld_Value' + @RowNumber1 + ']'
			END
		END

		IF LEN(@DefaultSort) > 0
		BEGIN
			SET  @DefaultSort = LEFT(@DefaultSort,LEN(@DefaultSort) - 1)
			SET @SortString = @SortString + ',' 
		END
	END
                                		
    IF LEN(@SearchText) > 0
	BEGIN
		IF CHARINDEX('SearchOrder',@SortString) > 0
		BEGIN
			SET @searchPositionColumn = ' ,SearchItems.RowNumber AS SearchOrder ' 
			SET @searchPositionColumnGroupBy = ' ,SearchItems.RowNumber ' 
		END

		SET @Where = ' INNER JOIN dbo.SplitIDsWithPosition('''+ @SearchText +''','','') AS SearchItems 
                        ON SearchItems.Id = I.numItemCode  and SC.numSiteID = ' + CONVERT(VARCHAR(15), @numSiteID) + ' WHERE ISNULL(IsArchieve,0) = 0'
	END
	ELSE IF @SearchText = ''
	BEGIN
		SET @Where = ' WHERE 1=2 '
		+ ' and SC.numSiteID = ' + CONVERT(VARCHAR(15), @numSiteID)
		+ ' AND ISNULL(IsArchieve,0) = 0'
	END
	ELSE
	BEGIN
		SET @Where = ' WHERE 1=1 '
					+ ' and SC.numSiteID = ' + CONVERT(VARCHAR(15), @numSiteID)
					+ ' AND ISNULL(IsArchieve,0) = 0'

		IF @tintDisplayCategory = 1
		BEGIN
					
			;WITH CTE AS(
			SELECT C.numCategoryID,C.tintLevel,C.numDepCategory,CASE WHEN tintLevel=1 THEN 1 ELSE 0 END AS bitChild 
				FROM SiteCategories SC INNER JOIN Category C ON SC.numCategoryID=C.numCategoryID WHERE SC.numSiteID=@numSiteID AND (C.numCategoryID = @numCategoryID OR ISNULL(@numCategoryID,0) = 0) AND numDomainID = @numDomainID
			UNION ALL
			SELECT  C.numCategoryID,C.tintLevel,C.numDepCategory,CTE.bitChild FROM Category C JOIN CTE ON 
			C.numDepCategory=CTE.numCategoryID AND CTE.bitChild=1 AND numDomainID = @numDomainID)
					
			INSERT INTO #tmpItemCat(numCategoryID)												 
			SELECT DISTINCT C.numCategoryID FROM CTE C JOIN dbo.ItemCategory IC ON C.numCategoryID = IC.numCategoryID 
												WHERE numItemID IN ( 
																						
													 				SELECT numItemID FROM WareHouseItems WI
																	INNER JOIN dbo.Item I ON I.numItemCode = WI.numItemID AND I.numDomainID = @numDomainID
																	GROUP BY numItemID 
																	HAVING SUM(WI.numOnHand) > 0 
																	UNION ALL
													 				SELECT numItemID FROM WareHouseItems WI
																	INNER JOIN dbo.Item I ON I.numItemCode = WI.numItemID AND I.numDomainID = @numDomainID
																	GROUP BY numItemID 
																	HAVING SUM(WI.numAllocation) > 0
																	)
		END

	END

	SET @strSQL = @strSQL + CONCAT(' WITH Items AS 
									( 
											SELECT
											I.numDomainID
											,I.numItemCode
											,ROW_NUMBER() OVER ( Partition by I.numItemCode Order by I.numItemCode) RowID
											,I.bitMatrix
											,I.bitKitParent
											,I.numItemGroup
											,vcItemName
											,I.bintCreatedDate
											,vcManufacturer
											,ISNULL(txtItemDesc,'''') txtItemDesc
											,vcSKU
											,fltHeight
											,fltWidth
											,fltLength
											,vcModelID
											,fltWeight
											,numBaseUnit
											,numSaleUnit
											,numPurchaseUnit
											,bitFreeShipping
											,C.vcCategoryName
											,C.vcDescription as CategoryDesc
											,charItemType
											,monListPrice
											,bitAllowBackOrder
											,bitShowInStock
											,numVendorID
											,numItemClassification',@searchPositionColumn,@SortFields,@DefaultSortFields,'
											,SUM(numOnHand) numOnHand
											,SUM(numAllocation) numAllocation
											FROM      
											Item AS I
											INNER JOIN eCommerceDTL E ON E.numDomainID = I.numDomainID AND E.numSiteId=',@numSiteID,'
											LEFT JOIN WareHouseItems WI ON WI.numDomainID=' + CONVERT(VARCHAR(10),@numDomainID) + ' AND WI.numItemID=I.numItemCode AND numWareHouseID IN (SELECT [OutParam] FROM dbo.[SplitString](''' + @vcWarehouseIDs + ''','',''))
											INNER JOIN ItemCategory IC ON I.numItemCode = IC.numItemID
											INNER JOIN Category C ON IC.numCategoryID = C.numCategoryID
											INNER JOIN SiteCategories SC ON IC.numCategoryID = SC.numCategoryID 
											',@Join,' ',@DefaultSortJoin)

	IF LEN(ISNULL(@FilterItemAttributes,'')) > 0
	BEGIN
		SET @strSQL = @strSQL + @FilterItemAttributes
	END

	IF LEN(@FilterCustomFields) > 0 AND LEN(@FilterCustomWhere) > 0
	BEGIN
		SET @strSQL = @strSQL + ' INNER JOIN #tmpItemCode TP ON TP.numItemCode = I.numItemCode ' 
	END
			
	IF (SELECT COUNT(*) FROM #tmpItemCat) > 0
	BEGIN
		SET @strSQL = @strSQL + ' INNER JOIN #tmpItemCat TC ON TC.numCategoryID = IC.numCategoryID '
	END
	ELSE IF @numManufacturerID>0
	BEGIN
		SET @Where = REPLACE(@Where,'WHERE',' WHERE I.numManufacturer = ' + CONVERT(VARCHAR(10),@numManufacturerID) + ' AND ')
	END 
	ELSE IF @numCategoryID>0
	BEGIN
		SET @Where = REPLACE(@Where,'WHERE',' WHERE IC.numCategoryID = ' + CONVERT(VARCHAR(10),@numCategoryID) + ' AND ')
	END 
			
	IF (SELECT COUNT(*) FROM [dbo].[EcommerceRelationshipProfile] WHERE numSiteID=@numSiteID AND numRelationship = @numDefaultRelationship AND numProfile = @numDefaultProfile) > 0
	BEGIN
		SET @Where = @Where + CONCAT(' AND I.numItemClassification NOT IN (SELECT numItemClassification FROM EcommerceRelationshipProfile ERP INNER JOIN ECommerceItemClassification EIC ON EIC.numECommRelatiionshipProfileID=ERP.ID WHERE ERP.numSiteID=',@numSiteID,' AND ERP.numRelationship=',@numDefaultRelationship,' AND ERP.numProfile=', @numDefaultProfile,')')
	END

	SET @strSQL = @strSQL + @Where
	
	IF LEN(@FilterRegularCondition) > 0
	BEGIN
		SET @strSQL = @strSQL + @FilterRegularCondition
	END                                         

    IF LEN(@whereNumItemCode) > 0
    BEGIN
			SET @strSQL = @strSQL + @filterByNumItemCode	
	END

    SET @strSQL = CONCAT(@strSQL,' GROUP BY
										I.numDomainID
										,I.numItemCode
										,I.bitMatrix
										,I.bitKitParent
										,I.numItemGroup
										,vcItemName
										,I.bintCreatedDate
										,vcManufacturer
										,txtItemDesc
										,vcSKU
										,fltHeight
										,fltWidth
										,fltLength
										,vcModelID
										,fltWeight
										,numBaseUnit
										,numSaleUnit
										,numPurchaseUnit
										,bitFreeShipping
										,C.vcCategoryName
										,C.vcDescription
										,charItemType
										,monListPrice
										,bitAllowBackOrder
										,bitShowInStock
										,numVendorID
										,numItemClassification',@searchPositionColumnGroupBy,@SortFields,@DefaultSortFields,(CASE WHEN ISNULL(@tintDisplayCategory,0) = 1 THEN ' HAVING 1 = (CASE WHEN (I.charItemType=''P'' AND ISNULL(I.bitKitParent,0) = 0) THEN CASE WHEN (ISNULL(SUM(numOnHand), 0) > 0 OR ISNULL(SUM(numAllocation), 0) > 0) THEN 1 ELSE 0 END ELSE 1 END) ' ELSE '' END), ') SELECT * INTO #TEMPItems FROM Items;')
                                        
	IF @numDomainID IN (204,214)
	BEGIN
		SET @strSQL = @strSQL + 'DELETE FROM 
									#TEMPItems
								WHERE 
									numItemCode IN (
														SELECT 
															F.numItemCode
														FROM 
															#TEMPItems AS F
														WHERE 
															ISNULL(F.bitMatrix,0) = 1 
															AND EXISTS (
																		SELECT 
																			t1.vcItemName, t1.bitMatrix, t1.numItemGroup, Count(t1.numItemCode)
																		FROM 
																			#TEMPItems t1
																		WHERE 
																			t1.vcItemName = F.vcItemName
																			AND t1.bitMatrix = 1
																			AND t1.numItemGroup = F.numItemGroup
																		GROUP BY 
																			t1.vcItemName, t1.bitMatrix, t1.numItemGroup
																		HAVING 
																			Count(t1.numItemCode) > 1
																	)
													)
									AND numItemCode NOT IN (
																			SELECT 
																				Min(numItemCode)
																			FROM 
																				#TEMPItems AS F
																			WHERE
																				ISNULL(F.bitMatrix,0) = 1 
																				AND Exists (
																							SELECT 
																								t2.vcItemName, t2.bitMatrix, t2.numItemGroup, Count(t2.numItemCode)
																							FROM 
																								#TEMPItems t2
																							WHERE 
																								t2.vcItemName = F.vcItemName
																								AND t2.bitMatrix = 1
																								AND t2.numItemGroup = F.numItemGroup
																							GROUP BY 
																								t2.vcItemName, t2.bitMatrix, t2.numItemGroup
																							HAVING 
																								Count(t2.numItemCode) > 1
																						)
																			GROUP BY 
																				vcItemName, bitMatrix, numItemGroup
																		);'

	END
									  
									  
	SET @strSQL = @strSQL + ' WITH ItemSorted AS (SELECT  ROW_NUMBER() OVER ( ORDER BY  ' + @SortString + @DefaultSort + ' ' + @vcSort + ' ) AS Rownumber,* FROM  #TEMPItems WHERE RowID = 1)'

    DECLARE @fldList AS NVARCHAR(MAX)
	DECLARE @fldList1 AS NVARCHAR(MAX)
	SELECT @fldList=COALESCE(@fldList + ',','') + QUOTENAME(REPLACE(Fld_label,' ','') + '_C') FROM dbo.CFW_Fld_Master WHERE numDomainID=@numDomainID AND Grp_id=5
	SELECT @fldList1=COALESCE(@fldList1 + ',','') + QUOTENAME(REPLACE(Fld_label,' ','') + '_' + CAST(Fld_Id AS VARCHAR)) FROM dbo.CFW_Fld_Master WHERE numDomainID=@numDomainID AND Grp_id=9
            
    SET @strSQL = @strSQL + 'SELECT * INTO #tmpPagedItems FROM ItemSorted WHERE Rownumber > ' + CONVERT(VARCHAR(10), @firstRec) + '
                                AND   Rownumber < ' + CONVERT(VARCHAR(10), @lastRec) + '
                                order by Rownumber '
                                                                  

	SET @strSQL = CONCAT(@strSQL,'SELECT  
								Rownumber
								,I.numItemCode
								,vcCategoryName
								,CategoryDesc
								,vcItemName
								,txtItemDesc
								,vcManufacturer
								,vcSKU
								,fltHeight
								,fltWidth
								,fltLength
								,vcModelID
								,fltWeight
								,bitFreeShipping
								,', 
									(CASE 
									WHEN @tintPreLoginPriceLevel > 0 
									THEN 'ISNULL(PricingOption.monPrice,ISNULL(CASE WHEN I.[charItemType] = ''P'' THEN ( UOM * ISNULL(W1.[monWListPrice],0)) ELSE (UOM * monListPrice) END, 0))' 
									ELSE 'ISNULL(CASE WHEN I.[charItemType] = ''P'' THEN ( UOM * ISNULL(W1.[monWListPrice],0)) ELSE (UOM * monListPrice) END, 0)' 
									END)
									,' AS monListPrice,
									ISNULL(CASE WHEN I.[charItemType] = ''P'' THEN ( UOM * ISNULL(W1.[monWListPrice],0)) ELSE (UOM * monListPrice) END, 0) monMSRP					   
									',(CASE WHEN @numDomainID = 172 THEN ',ISNULL(CASE 
											WHEN tintRuleType = 1 AND tintDiscountType = 1
											THEN ISNULL((CASE WHEN I.[charItemType] = ''P'' THEN ( UOM * ISNULL(W1.[monWListPrice],0)) ELSE (UOM * monListPrice) END),0) - (ISNULL((CASE WHEN I.[charItemType] = ''P'' THEN ( UOM * ISNULL(W1.[monWListPrice],0)) ELSE (UOM * monListPrice) END),0) * ( decDiscount / 100 ) )
											WHEN tintRuleType = 1 AND tintDiscountType = 2
											THEN ISNULL((CASE WHEN I.[charItemType] = ''P'' THEN ( UOM * ISNULL(W1.[monWListPrice],0)) ELSE (UOM * monListPrice) END),0) - decDiscount
											WHEN tintRuleType = 2 AND tintDiscountType = 1
											THEN ISNULL(Vendor.monCost,0) + (ISNULL(Vendor.monCost,0) * ( decDiscount / 100 ) )
											WHEN tintRuleType = 2 AND tintDiscountType = 2
											THEN ISNULL(Vendor.monCost,0) + decDiscount
											WHEN tintRuleType = 3
											THEN decDiscount
										END, 0) AS monFirstPriceLevelPrice,ISNULL(CASE 
										WHEN tintRuleType = 1 AND tintDiscountType = 1 
										THEN decDiscount 
										WHEN tintRuleType = 1 AND tintDiscountType = 2
										THEN (decDiscount * 100) / (ISNULL((CASE WHEN I.[charItemType] = ''P'' THEN ( UOM * ISNULL(W1.[monWListPrice],0)) ELSE (UOM * monListPrice) END),0) - decDiscount)
										WHEN tintRuleType = 2 AND tintDiscountType = 1
										THEN decDiscount
										WHEN tintRuleType = 2 AND tintDiscountType = 2
										THEN (decDiscount * 100) / (ISNULL(Vendor.monCost,0) + decDiscount)
										WHEN tintRuleType = 3
										THEN 0
									END,'''') AS fltFirstPriceLevelDiscount' ELSE ',0 AS monFirstPriceLevelPrice, 0 AS fltFirstPriceLevelDiscount' END), '  
								,vcPathForImage
								,vcpathForTImage
								,UOM AS UOMConversionFactor
								,UOMPurchase AS UOMPurchaseConversionFactor
								,(CASE WHEN charItemType <> ''S'' AND charItemType <> ''N'' AND ISNULL(bitKitParent,0)=0
											THEN (CASE 
													WHEN bitAllowBackOrder = 1 THEN 1
													WHEN (ISNULL(I.numOnHand,0)<=0) THEN 0
													ELSE 1
												END)
											ELSE 1
								END) AS bitInStock,
								(
									CASE WHEN ISNULL(bitKitParent,0) = 1
									THEN
										(CASE 
											WHEN ((SELECT COUNT(*) FROM ItemDetails ID INNER JOIN Item IInner ON ID.numChildItemID = IInner.numItemCode WHERE ID.numItemKitID = I.numItemCode AND ISNULL(IInner.bitKitParent,0) = 1)) > 0 
											THEN 1 
											ELSE 0 
										END)
									ELSE
										0
									END
								) bitHasChildKits
								,(CASE WHEN charItemType <> ''S'' AND charItemType <> ''N''
											THEN ( CASE WHEN bitShowInStock = 1
														THEN (CASE 
																WHEN bitAllowBackOrder = 1 AND I.numDomainID <> 172 AND ISNULL(I.numOnHand,0)<=0 AND ISNULL(I.numVendorID,0) > 0 AND dbo.GetVendorPreferredMethodLeadTime(I.numVendorID) > 0 THEN CONCAT(''<font class="vendorLeadTime" style="color:green">Usually ships in '',dbo.GetVendorPreferredMethodLeadTime(I.numVendorID),'' days</font>'')
																WHEN bitAllowBackOrder = 1 AND ISNULL(I.numOnHand,0)<=0 THEN (CASE WHEN I.numDomainID = 172 THEN ''<font style="color:green">AVAILABLE</font>'' ELSE ''<font style="color:green">In Stock</font>'' END)
																WHEN (ISNULL(I.numOnHand,0)<=0) THEN (CASE WHEN I.numDomainID = 172 THEN ''<font style="color:red">ON HOLD</font>'' ELSE ''<font style="color:red">Out Of Stock</font>'' END)
																ELSE (CASE WHEN I.numDomainID = 172 THEN ''<font style="color:green">AVAILABLE</font>'' ELSE ''<font style="color:green">In Stock</font>'' END)  
															END)
														ELSE ''''
													END )
											ELSE ''''
										END ) AS InStock
								, ISNULL(W1.numWareHouseItemID,0) [numWareHouseItemID]
								,ISNULL(TablePromotion.numTotalPromotions,0) numTotalPromotions
								,ISNULL(TablePromotion.vcPromoDesc,'''') vcPromoDesc
								,ISNULL(TablePromotion.bitRequireCouponCode,0) bitRequireCouponCode
								',(CASE WHEN LEN(ISNULL(@fldList,'')) > 0 THEN CONCAT(',',@fldList) ELSE '' END),(CASE WHEN LEN(ISNULL(@fldList1,'')) > 0 THEN CONCAT(',',@fldList1) ELSE '' END),'
                            FROM 
								#tmpPagedItems I
							LEFT JOIN ItemImages IM ON I.numItemCode = IM.numItemCode AND IM.bitdefault=1 AND IM.bitIsImage = 1
							OUTER APPLY (SELECT * FROM dbo.fn_GetItemPromotions(',@numDomainID,',',@numDivisionID,',I.numItemCode,I.numItemClassification',',',@numDefaultRelationship,',',@numDefaultProfile,',1)) TablePromotion ',
							(CASE 
								WHEN @tintPreLoginPriceLevel > 0 
								THEN CONCAT(' OUTER APPLY(SELECT decDiscount AS monPrice FROM PricingTable WHERE tintRuleType=3 AND numItemCode=I.numItemCode ORDER BY numPricingID OFFSET ',CAST(@tintPreLoginPriceLevel-1 AS VARCHAR),' ROWS FETCH NEXT 1 ROWS ONLY) As PricingOption ' )
								ELSE '' 
							END),(CASE 
								WHEN @numDomainID = 172
								THEN ' OUTER APPLY (SELECT TOP 1 * FROM PricingTable WHERE numItemCode=I.numItemCode) AS PT LEFT JOIN Vendor ON Vendor.numVendorID =I.numVendorID AND Vendor.numItemCode=I.numItemCode'
								ELSE '' 
							END),' 
							OUTER APPLY (SELECT TOP 1 numWareHouseItemID,monWListPrice FROM WareHouseItems W 
											WHERE  W.numItemID = I.numItemCode 
											AND W.numDomainID = ',CONVERT(VARCHAR(10),@numDomainID),'
											AND numWareHouseID IN (SELECT [OutParam] FROM dbo.[SplitString](''' + @vcWarehouseIDs + ''','',''))) AS W1 			  
								CROSS APPLY (SELECT dbo.fn_UOMConversion(ISNULL(I.numSaleUnit, 0),I.numItemCode, I.numDomainId,ISNULL(I.numBaseUnit, 0)) AS UOM) AS UOM
								CROSS APPLY (SELECT dbo.fn_UOMConversion(ISNULL(I.numPurchaseUnit, 0),I.numItemCode, I.numDomainId,ISNULL(I.numBaseUnit, 0)) AS UOMPurchase) AS UOMPurchase')
			
	IF LEN(ISNULL(@fldList,'')) > 0
	BEGIN
		SET @strSQL = @strSQL +' left join (
											SELECT  t2.RecId, REPLACE(t1.Fld_label,'' '','''') + ''_C'' as Fld_label, 
												CASE t1.fld_type WHEN ''SelectBox'' THEN isnull(dbo.fn_GetListItemName(t2.Fld_Value),'''')
																	WHEN ''CheckBox'' THEN isnull(dbo.fn_getCustomDataValue(t2.Fld_Value,9),'''')
																	WHEN ''DateField'' THEN dbo.FormatedDateFromDate(t2.Fld_Value,' + CAST(@numDomainId AS VARCHAR(18)) +')
																	ELSE Replace(ISNULL(t2.Fld_Value,''0''),''$'',''$$'') end AS Fld_Value FROM CFW_Fld_Master t1 
											JOIN CFW_FLD_Values_Item AS t2 ON t1.Fld_ID = t2.Fld_ID where t1.Grp_id = 5 and t1.numDomainID=' + CAST(@numDomainId AS VARCHAR(18)) +' AND t1.fld_type <> ''Link''
											AND t2.RecId IN (SELECT numItemCode FROM #tmpPagedItems )
											) p PIVOT (MAX([Fld_Value]) FOR Fld_label IN (' + @fldList + ')) AS pvt on I.numItemCode=pvt.RecId' 
	END

	IF LEN(ISNULL(@fldList1,'')) > 0
	BEGIN
					
		SET @strSQL = @strSQL +' left join (
											SELECT  t2.numItemCode AS RecId, REPLACE(t1.Fld_label,'' '','''') + ''_'' + CAST(t1.Fld_Id AS VARCHAR) as Fld_label, 
												CASE t1.fld_type WHEN ''SelectBox'' THEN isnull(dbo.fn_GetListItemName(t2.Fld_Value),'''')
																	WHEN ''CheckBox'' THEN isnull(dbo.fn_getCustomDataValue(t2.Fld_Value,9),'''')
																	WHEN ''DateField'' THEN dbo.FormatedDateFromDate(t2.Fld_Value,' + CAST(@numDomainId AS VARCHAR(18)) +')
																	ELSE Replace(ISNULL(t2.Fld_Value,''0''),''$'',''$$'') end AS Fld_Value FROM CFW_Fld_Master t1 
											JOIN ItemAttributes AS t2 ON t1.Fld_ID = t2.Fld_ID where t1.Grp_id = 9 and t1.numDomainID=' + CAST(@numDomainId AS VARCHAR(18)) +' AND t1.fld_type <> ''Link''
											AND t2.numItemCode IN (SELECT numItemCode FROM #tmpPagedItems )
											) p PIVOT (MAX([Fld_Value]) FOR Fld_label IN (' + @fldList1 + ')) AS pvt1 on I.numItemCode=pvt1.RecId'
	END
	ELSE
	BEGIN
		SET @strSQL = REPLACE(@strSQL, ',##FLDLIST1##', '');
	END

	SET  @strSQL = @strSQL +' order by Rownumber';                
    SET @strSQL = @strSQL + ' SELECT @TotRecs = COUNT(*) FROM #TEMPItems WHERE RowID=1 '                            


	DECLARE @tmpSQL AS NVARCHAR(MAX)
	SET @tmpSQL = @strSQL
	
	PRINT CAST(@tmpSQL AS NTEXT)
	EXEC sp_executeSQL @strSQL, N'@TotRecs INT OUTPUT', @TotRecs OUTPUT
             
             
    DECLARE @tintOrder AS TINYINT                                                  
	DECLARE @vcFormFieldName AS VARCHAR(50)                                                  
	DECLARE @vcListItemType AS VARCHAR(1)                                             
	DECLARE @vcAssociatedControlType VARCHAR(10)                                                  
	DECLARE @numListID AS NUMERIC(9)                                                  
	DECLARE @WhereCondition VARCHAR(MAX)                       
	DECLARE @numFormFieldId AS NUMERIC  
	--DECLARE @vcFieldType CHAR(1)
	DECLARE @vcFieldType VARCHAR(15)
		                  
	SET @tintOrder=0                                                  
	SET @WhereCondition =''                 
                   
              
	CREATE TABLE #tempAvailableFields
	(
		numFormFieldId  NUMERIC(9)
		,vcFormFieldName NVARCHAR(50)
		,vcFieldType VARCHAR(15)
		,vcAssociatedControlType NVARCHAR(50)
		,numListID NUMERIC(18)
		,vcListItemType CHAR(1)
		,intRowNum INT
		,vcItemValue VARCHAR(3000)
	)
		   
	INSERT INTO #tempAvailableFields
	(
		numFormFieldId
		,vcFormFieldName
		,vcFieldType
		,vcAssociatedControlType
		,numListID
		,vcListItemType
		,intRowNum
	)                         
	SELECT 
		Fld_id
		,REPLACE(Fld_label,' ','') AS vcFormFieldName
		,CASE GRP_ID 
			WHEN 4 then 'C' 
			WHEN 1 THEN 'D'
			WHEN 9 THEN CONVERT(VARCHAR(15), Fld_id)
			ELSE 'C' 
		END AS vcFieldType
		,fld_type as vcAssociatedControlType
		,ISNULL(C.numListID, 0) numListID
		,CASE 
			WHEN C.numListID > 0 THEN 'L'
			ELSE ''
		END vcListItemType
		,ROW_NUMBER() OVER (ORDER BY Fld_id ASC) AS intRowNum
	FROM  
		CFW_Fld_Master C 
	LEFT JOIN 
		CFW_Validation V 
	ON 
		V.numFieldID = C.Fld_id
	WHERE 
		C.numDomainID = @numDomainId
		AND GRP_ID IN (5,9)

		  
	SELECT * FROM #tempAvailableFields
	
	IF OBJECT_ID('tempdb..#tempAvailableFields') IS NOT NULL
	DROP TABLE #tempAvailableFields
		
	IF OBJECT_ID('tempdb..#tmpItemCode') IS NOT NULL
	DROP TABLE #tmpItemCode
		
	IF OBJECT_ID('tempdb..#tmpItemCat') IS NOT NULL
	DROP TABLE #tmpItemCat
		
	IF OBJECT_ID('tempdb..#fldValues') IS NOT NULL
	DROP TABLE #fldValues
		
	IF OBJECT_ID('tempdb..#tempSort') IS NOT NULL
	DROP TABLE #tempSort
		
	IF OBJECT_ID('tempdb..#fldDefaultValues') IS NOT NULL
	DROP TABLE #fldDefaultValues

	IF OBJECT_ID('tempdb..#tmpPagedItems') IS NOT NULL
	DROP TABLE #tmpPagedItems


	IF OBJECT_ID('tempdb..#TEMPItems') IS NOT NULL
	DROP TABLE #TEMPItems
END
GO



GO
/****** Object:  StoredProcedure [dbo].[usp_ManageAddlContInfo]    Script Date: 04/02/2009 00:17:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Modified By Anoop jayaraj                  
 GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_manageaddlcontinfo')
DROP PROCEDURE usp_manageaddlcontinfo
GO
CREATE PROCEDURE [dbo].[usp_ManageAddlContInfo]                                    
 @numcontactId numeric=0,                                    
 @numContactType numeric=0,                                    
 @vcDepartment numeric(9)=0,                                    
 @vcCategory numeric(9)=0,                                    
 @vcGivenName varchar(100)='',                                    
 @vcFirstName varchar(50)='',                                    
 @vcLastName varchar(50)='',                                    
 @numDivisionId numeric,                                    
 @numPhone varchar(15)='',                                    
 @numPhoneExtension varchar(7)='',                                    
 @numCell varchar(15)='',                                    
 @NumHomePhone varchar(15)='',                                    
 @vcFax varchar(15)='',                                    
 @vcEmail varchar(50)='',                                    
 @VcAsstFirstName varchar(50)='',                                    
 @vcAsstLastName varchar(50)='',                                    
 @numAsstPhone varchar(15)='',                                    
 @numAsstExtn varchar(6)='',                                    
 @vcAsstEmail varchar(50)='',                                                      
 @charSex char(1)='',                                    
 @bintDOB datetime,                                    
 @vcPosition numeric(9)=0,                                                     
 @txtNotes text='',                                                     
 @numUserCntID numeric,                                                                                              
 @numDomainID numeric=1,                                                   
 @vcPStreet varchar(100)='',                                    
 @vcPCity varchar(50)='',                                    
 @vcPPostalCode varchar(15)='',                                    
 @vcPState numeric(9)=0,                                    
 @vcPCountry numeric(9)=0,                  
 @numManagerID numeric=0,        
 @numTeam as numeric(9)=0,        
 @numEmpStatus as numeric(9)=0,
 @vcTitle AS VARCHAR(100)='',
 @bitPrimaryContact AS BIT=0,
 @bitOptOut AS BIT=0,
 @vcLinkedinId VARCHAR(30)=NULL,
 @vcLinkedinUrl VARCHAR(300)=NULL,
 @numECampaignID NUMERIC(18)=0,
 @vcPassword VARCHAR(50) = '',
 @vcTaxID VARCHAR(100) = ''
AS   
	If LEN(ISNULL(@vcFirstName,'')) = 0
	BEGIN
		SET @vcFirstName = '-'
	END

	If LEN(ISNULL(@vcLastName,'')) = 0
	BEGIN
		SET @vcLastName = '-'
	END

	--IF LEN(ISNULL(@vcEmail,'')) > 0
	--BEGIN
	--	IF EXISTS (SELECT numContactId FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numContactId <> ISNULL(@numcontactId,0) AND vcEmail=@vcEmail)
	--	BEGIN
	--		RAISERROR('DUPLICATE_EMAIL',16,1)
	--		RETURN
	--	END
	--END

 SELECT @vcGivenName = @vcLastName + ', ' + @vcFirstName  + '.eml'                                 
   if @bintDOB ='Jan  1 1753 12:00AM' set  @bintDOB=null                      
IF @numContactId=0
     BEGIN   

IF @bitPrimaryContact = 1 
    BEGIN            
        DECLARE @numID AS NUMERIC(9)            
        DECLARE @bitPrimaryContactCheck AS BIT             
             
        SELECT TOP 1
                @numID = numContactID,
                @bitPrimaryContactCheck = ISNULL(bitPrimaryContact,0)
        FROM    AdditionalContactsInformation
        WHERE   numDivisionID = @numDivisionID 
                   
        IF @@rowcount = 0 
            SET @numID = 0             
        WHILE @numID > 0            
            BEGIN            
                IF @bitPrimaryContactCheck = 1 
                    UPDATE  AdditionalContactsInformation
                    SET     bitPrimaryContact = 0
                    WHERE   numContactID = @numID  
                              
                SELECT TOP 1
                        @numID = numContactID,
                        @bitPrimaryContactCheck = ISNULL(bitPrimaryContact,0)
                FROM    AdditionalContactsInformation
                WHERE   numDivisionID = @numDivisionID
                        AND numContactID > @numID    
                                
                IF @@rowcount = 0 
                    SET @numID = 0             
            END            
    END  
    ELSE IF @bitPrimaryContact = 0
    BEGIN
		IF (SELECT COUNT(*) FROM AdditionalContactsInformation WHERE numDivisionID = @numDivisionID AND numContactId!=@numContactId AND ISNULL(bitPrimaryContact,0)=1)=0
			SET @bitPrimaryContact = 1
    END                 
    
    
declare @bitAutoPopulateAddress bit 
declare @tintPoulateAddressTo tinyint 

select @bitAutoPopulateAddress= isnull(bitAutoPopulateAddress,'0'),@tintPoulateAddressTo=isnull(tintPoulateAddressTo,'0') from Domain where numDomainId = @numDomainId ;
if (@bitAutoPopulateAddress = 1)
	if(@tintPoulateAddressTo = 1) --if primary address is not specified then getting billing address    
	begin
		 
		  select                   
			@vcPStreet = AD1.vcStreet,
			@vcPCity= AD1.vcCity,
			@vcPState= AD1.numState,
			@vcPPostalCode= AD1.vcPostalCode,
			@vcPCountry= AD1.numCountry              
		  from divisionMaster  DM              
		   LEFT JOIN dbo.AddressDetails AD1 ON AD1.numDomainID=DM.numDomainID 
			AND AD1.numRecordID=DM.numDivisionID AND AD1.tintAddressOf=2 AND AD1.tintAddressType=1 AND AD1.bitIsPrimary=1
		  where numDivisionID=@numDivisionId    
	end  
	else if (@tintPoulateAddressTo = 2)-- Primary Address is Shipping Address
	begin
		  select                   
			@vcPStreet= AD2.vcStreet,
			@vcPCity=AD2.vcCity,
			@vcPState=AD2.numState,
			@vcPPostalCode=AD2.vcPostalCode,
			@vcPCountry=AD2.numCountry                   
		  from divisionMaster DM
		  LEFT JOIN dbo.AddressDetails AD2 ON AD2.numDomainID=DM.numDomainID 
		  AND AD2.numRecordID= DM.numDivisionID AND AD2.tintAddressOf=2 AND AD2.tintAddressType=2 AND AD2.bitIsPrimary=1
		  where numDivisionID=@numDivisionId    
	
	end                       
                       
 if (@numEmpStatus is null or @numEmpStatus=0) set @numEmpStatus=658        
 
    INSERT into AdditionalContactsInformation                                    
    (                  
   numContactType,                  
   vcDepartment,                  
   vcCategory,                  
   vcGivenName,                  
   vcFirstName,                  
   vcLastName,                   
   numDivisionId ,                  
   numPhone ,                  
   numPhoneExtension,                  
   numCell ,                  
   numHomePhone ,                          
   vcFax ,                  
   vcEmail ,                  
   VcAsstFirstName ,                  
   vcAsstLastName ,                  
   numAsstPhone ,                  
   numAsstExtn ,                  
   vcAsstEmail  ,                  
   charSex ,                  
   bintDOB ,                  
   vcPosition ,                  
   txtNotes ,                  
   numCreatedBy ,                                    
   bintCreatedDate,             
   numModifiedBy,            
   bintModifiedDate,                   
   numDomainID,                  
   numManagerID,                   
   numRecOwner,                  
   numTeam,                  
   numEmpStatus,
   vcTitle,bitPrimaryContact,bitOptOut,
   vcLinkedinId,vcLinkedinUrl,numECampaignID,vcTaxID           
   )                                    
    VALUES                                    
      (                  
   @numContactType,                  
   @vcDepartment,                  
   @vcCategory,                  
   @vcGivenName ,                  
   @vcFirstName ,                  
   @vcLastName,                   
   @numDivisionId ,                  
   @numPhone ,                                    
   @numPhoneExtension,                  
   @numCell ,                  
   @NumHomePhone ,                  
   @vcFax ,                  
   @vcEmail ,                  
   @VcAsstFirstName,                         
   @vcAsstLastName,                  
   @numAsstPhone,                  
   @numAsstExtn,                  
   @vcAsstEmail,                  
   @charSex,                  
   @bintDOB ,                  
   @vcPosition,                                   
   @txtNotes,                  
   @numUserCntID,                  
   getutcdate(),             
   @numUserCntID,                  
   getutcdate(),                   
   @numDomainID,                   
   @numManagerID,                   
   @numUserCntID,                   
   @numTeam,                  
   @numEmpStatus,
   @vcTitle,@bitPrimaryContact,@bitOptOut,
   @vcLinkedinId,@vcLinkedinUrl,@numECampaignID,@vcTaxID           
   )                                    
                     
 set @numcontactId= SCOPE_IDENTITY()
 
 SELECT @numcontactId             

     INSERT INTO dbo.AddressDetails (
	 	vcAddressName,
	 	vcStreet,
	 	vcCity,
	 	vcPostalCode,
	 	numState,
	 	numCountry,
	 	bitIsPrimary,
	 	tintAddressOf,
	 	tintAddressType,
	 	numRecordID,
	 	numDomainID
	 ) VALUES ('Primary', @vcPStreet,@vcPCity,@vcPPostalCode,@vcPState,@vcPCountry,1,1,0,@numcontactId,@numDomainID)
     
	 IF ISNULL(@numECampaignID,0) > 0 
	 BEGIN
		  --Manage ECampiagn When Campaign is changed from inline edit
		DECLARE @Date AS DATETIME 
		SELECT @Date = dbo.[GetUTCDateWithoutTime]()

 		EXEC [USP_ManageConEmailCampaign]
		@numConEmailCampID = 0, --  numeric(9, 0)
		@numContactID = @numcontactId, --  numeric(9, 0)
		@numECampaignID = @numECampaignID, --  numeric(9, 0)
		@intStartDate = @Date, --  datetime
		@bitEngaged = 1, --  bit
		@numUserCntID = @numUserCntID --  numeric(9, 0)     
	END                                         
                                                 
  END                                    
 ELSE if @numContactId>0                                                        
    BEGIN                                    
    UPDATE AdditionalContactsInformation SET                                    
      numContactType=@numContactType,                                    
      vcGivenName=@vcGivenName,                                    
      vcFirstName=@vcFirstName ,                                    
      vcLastName =@vcLastName ,                   
      numDivisionId =@numDivisionId ,                                    
      numPhone=@numPhone ,                                    
      numPhoneExtension=@numPhoneExtension,                                    
      numCell =@numCell ,                                    
      NumHomePhone =@NumHomePhone ,                                    
      vcFax=@vcFax ,                                    
      vcEmail=@vcEmail ,                                    
      VcAsstFirstName=@VcAsstFirstName,                                    
      vcAsstLastName=@vcAsstLastName,                                    
      numAsstPhone=@numAsstPhone,                                    
      numAsstExtn=@numAsstExtn,                                    
      vcAsstEmail=@vcAsstEmail,                                                      
      charSex=@charSex,                            
      bintDOB=@bintDOB ,                                    
      vcPosition=@vcPosition,                                                    
      txtNotes=@txtNotes,                                                     
      numModifiedBy=@numUserCntID,                                    
      bintModifiedDate=getutcdate(),                                    
      numManagerID=@numManagerID,
      bitPrimaryContact=@bitPrimaryContact                              
     WHERE numContactId=@numContactId   
                    
     Update dbo.AddressDetails set                   
       vcStreet=@vcPStreet,                                    
       vcCity =@vcPCity,                                    
       vcPostalCode=@vcPPostalCode,                                     
       numState=@vcPState,                                    
       numCountry=@vcPCountry
	  where numRecordID=@numContactId AND bitIsPrimary=1 AND tintAddressOf=1 AND tintAddressType=0
 
     SELECT @numcontactId   
                                                
  END    
  ------Check the Email id Exist into the Email Master Table

	IF @vcPassword IS NOT NULL AND  LEN(@vcPassword) > 0
	BEGIN
		IF EXISTS (SELECT numExtranetID FROM ExtarnetAccounts WHERE numDivisionID = @numDivisionID)
		BEGIN
			IF EXISTS (SELECT numExtranetDtlID FROM ExtranetAccountsDtl WHERE numContactID=@numContactID)
			BEGIN
				UPDATE
					ExtranetAccountsDtl
				SET 
					vcPassword=@vcPassword
					,tintAccessAllowed=1
				WHERE
					numContactID=@numContactID
			END
			ELSE
			BEGIN
				DECLARE @numExtranetID NUMERIC(18,0)
				SELECT @numExtranetID=numExtranetID FROM ExtarnetAccounts WHERE numDivisionID = @numDivisionID

				INSERT INTO ExtranetAccountsDtl (numExtranetID,numContactID,vcPassword,tintAccessAllowed,numDomainID)
				VALUES (@numExtranetID,@numContactID,@vcPassword,1,@numDomainID)
			END
		END
		ELSE
		BEGIN
			RAISERROR('Organization e-commerce access is not enabled',16,1)
		END
	END



DECLARE @numCount AS NUMERIC(18,0)
SET @numCount = 0
IF @vcEmail <> ''
BEGIN 
SELECT @numCount = COUNT(*) FROM dbo.EmailMaster WHERE vcEmailID =@vcEmail and numDomainId=@numDomainID
IF @numCount = 0 
BEGIN
	INSERT INTO EmailMaster(vcEmailId,vcName,numDomainId,numContactId)
	VALUES(@vcEmail,@vcFirstName,@numDomainID,@numcontactId)
END
else
begin
	update EmailMaster set numContactId=@numcontactId where vcEmailID =@vcEmail and numDomainId=@numDomainID
end
END 
--IF @vcAsstEmail <> ''
--BEGIN
--SELECT @numCount = COUNT(*) FROM dbo.EmailMaster WHERE vcEmailID =@vcAsstEmail
--IF @numCount = 0 
--BEGIN
--	INSERT INTO EmailMaster(vcEmailId,vcName,numDomainId,numContactId)
--	VALUES(@vcAsstEmail,@vcFirstName,@numDomainID,@numcontactId)
--END
--END
---------------------------------------------------------

/****** Object:  StoredProcedure [dbo].[usp_ManageContInfo]    Script Date: 07/26/2008 16:19:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_managecontinfo')
DROP PROCEDURE usp_managecontinfo
GO
CREATE PROCEDURE [dbo].[usp_ManageContInfo]                  
 @numContactId numeric=0,                  
 @vcDepartment numeric(9)=0,                  
 @vcFirstName varchar(50)='',                  
 @vcLastName varchar(50)='',                  
 @vcEmail varchar(50)='',                  
 @vcSex char(1)='',                  
 @vcPosition  numeric(9)=0,                  
 @numPhone varchar(15)='',                  
 @numPhoneExtension varchar(7)='',                  
 @bintDOB DATETIME=null,                  
 @txtNotes text='',                  
 @numUserCntID numeric=0,                      
 @numCategory numeric=0,                   
 @numCell varchar(15)='',                  
 @NumHomePhone varchar(15)='',                  
 @vcFax varchar(15)='',                  
 @vcAsstFirstName varchar(50)='',                  
 @vcAsstLastName varchar(50)='',                  
 @numAsstPhone varchar(15)='',                  
 @numAsstExtn varchar(6)='',                  
 @vcAsstEmail varchar(50)='',                      
 @numContactType numeric=0 ,               
 @bitOptOut bit=0,                
 @numManagerId numeric=0,                
 @numTeam as numeric(9)=0,                
 @numEmpStatus as numeric(9)=0,        
 @vcAltEmail as varchar(100),        
 @vcTitle as varchar(100),
 @numECampaignID AS NUMERIC(9),
 @bitSelectiveUpdate AS BIT = 0, --Update fields whose value are supplied
 @bitPrimaryContact AS BIT=0,
 @vcTaxID VARCHAR(100) = ''        
AS                  
IF @bitSelectiveUpdate = 0
BEGIN

if @bintDOB ='Jan  1 1753 12:00AM' set  @bintDOB=null                  
       
        DECLARE @numDivisionID AS NUMERIC(9)            
        SELECT  @numDivisionID = numDivisionID FROM AdditionalContactsInformation WHERE numContactID = @numContactId   
           
 IF @bitPrimaryContact = 1 
    BEGIN            
        DECLARE @numID AS NUMERIC(9)            
        DECLARE @bitPrimaryContactCheck AS BIT             
             
        SELECT TOP 1
                @numID = numContactID,
                @bitPrimaryContactCheck = ISNULL(bitPrimaryContact,0)
        FROM    AdditionalContactsInformation
        WHERE   numDivisionID = @numDivisionID 
                   
        IF @@rowcount = 0 
            SET @numID = 0             
        WHILE @numID > 0            
            BEGIN            
                IF @bitPrimaryContactCheck = 1 
                    UPDATE  AdditionalContactsInformation
                    SET     bitPrimaryContact = 0
                    WHERE   numContactID = @numID  
                              
                SELECT TOP 1
                        @numID = numContactID,
                        @bitPrimaryContactCheck = ISNULL(bitPrimaryContact,0)
                FROM    AdditionalContactsInformation
                WHERE   numDivisionID = @numDivisionID
                        AND numContactID > @numID    
                                
                IF @@rowcount = 0 
                    SET @numID = 0             
            END            
    END 
    ELSE IF @bitPrimaryContact = 0
    BEGIN
		IF (SELECT COUNT(*) FROM AdditionalContactsInformation WHERE numDivisionID = @numDivisionID AND numContactId!=@numContactId AND ISNULL(bitPrimaryContact,0)=1)=0
			SET @bitPrimaryContact = 1
    END                 
    
    UPDATE AdditionalContactsInformation SET                  
     vcDepartment=@vcDepartment,                  
     vcFirstName=@vcFirstName ,                  
     vcLastName =@vcLastName ,                  
     numPhone=@numPhone ,                  
     numPhoneExtension=@numPhoneExtension,                  
     vcEmail=@vcEmail ,                  
     charSex=@vcSex ,                  
     vcPosition = @vcPosition,                  
     bintDOB=@bintDOB ,                  
     txtNotes=@txtNotes,                  
     numModifiedBy=@numUserCntID,                  
     bintModifiedDate=getutcdate(),                
     vcCategory = @numCategory,                  
     numCell=@numCell,                  
     NumHomePhone=@NumHomePhone,                  
     vcFax=@vcFax,                  
     vcAsstFirstName=@vcAsstFirstName,                  
     vcAsstLastName=@vcAsstLastName,                  
     numAsstPhone=@numAsstPhone,                  
     numAsstExtn=@numAsstExtn,                  
     vcAsstEmail=@vcAsstEmail,                     
     numContactType=@numContactType,                      
     bitOptOut=@bitOptOut,                
     numManagerId=@numManagerId,                
     numTeam=@numTeam,           
     numEmpStatus = @numEmpStatus,        
     vcTitle=@vcTitle,        
     vcAltEmail= @vcAltEmail,
     numECampaignID = @numECampaignID,
     bitPrimaryContact=@bitPrimaryContact,
	 vcTaxID=@vcTaxID
 WHERE numContactId=@numContactId
 
 /*Added by chintan BugID-262*/
	 DECLARE @Date AS DATETIME 
	 SELECT @Date = dbo.[GetUTCDateWithoutTime]()
	 IF @numECampaignID = -1 
		SET @numECampaignID=0
	 
 	EXEC [USP_ManageConEmailCampaign]
	@numConEmailCampID = 0, --  numeric(9, 0)
	@numContactID = @numContactID, --  numeric(9, 0)
	@numECampaignID = @numECampaignID, --  numeric(9, 0)
	@intStartDate = @Date, --  datetime
	@bitEngaged = 1, --  bit
	@numUserCntID = @numUserCntID --  numeric(9, 0)
END


IF @bitSelectiveUpdate = 1
BEGIN
		DECLARE @sql VARCHAR(1000)
		SET @sql = 'update AdditionalContactsInformation Set '

		IF(LEN(@vcFirstName)>0)
		BEGIN
			SET @sql =@sql + ' vcFirstName=''' + @vcFirstName + ''', '
		END
		IF(LEN(@vcLastName)>0)
		BEGIN
			SET @sql =@sql + ' vcLastName=''' + @vcLastName + ''', '
		END
		
		IF(LEN(@vcEmail)>0)
		BEGIN
			SET @sql =@sql + ' vcEmail=''' + @vcEmail + ''', '
		END
		
		IF(LEN(@numPhone)>0)
		BEGIN
			SET @sql =@sql + ' numPhone=''' + @numPhone + ''', '
		END
		
		IF(LEN(@numPhoneExtension)>0)
		BEGIN
			SET @sql =@sql + ' numPhoneExtension=''' + @numPhoneExtension + ''', '
		END
		
		IF(LEN(@numCell)>0)
		BEGIN
			SET @sql =@sql + ' numCell=''' + @numCell + ''', '
		END
		
		IF(LEN(@vcFax)>0)
		BEGIN
			SET @sql =@sql + ' vcFax=''' + @vcFax + ''', '
		END
		
		SET @sql =@sql + ' bitOptOut=''' + CONVERT(VARCHAR(10),@bitOptOut) + ''', '
		
		IF(@numUserCntID>0)
		BEGIN
			SET @sql =@sql + ' numModifiedBy='+ CONVERT(VARCHAR(10),@numUserCntID) +', '
		END
		
	
	
		SET @sql=SUBSTRING(@sql,0,LEN(@sql)) + ' '
		SET @sql =@sql + 'where numContactId= '+CONVERT(VARCHAR(10),@numContactId)

		PRINT @sql
		EXEC(@sql) 
END 
/****** Object:  StoredProcedure [dbo].[USP_ManageDivisions]    Script Date: 07/26/2008 16:19:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Modified by anoop jayaraj                                  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_managedivisions')
DROP PROCEDURE usp_managedivisions
GO
CREATE PROCEDURE [dbo].[USP_ManageDivisions]                                                        
 @numDivisionID  numeric=0,                                                         
 @numCompanyID  numeric=0,                                                         
 @vcDivisionName  varchar (100)='',                                                        
 @vcBillStreet  varchar (50)='',                                                        
 @vcBillCity   varchar (50)='',                                                        
 @vcBillState  NUMERIC=0,                                                        
 @vcBillPostCode  varchar (15)='',                                                        
 @vcBillCountry  numeric=0,                                                        
 @bitSameAddr  bit=0,                                                        
 @vcShipStreet  varchar (50)='',                                                        
 @vcShipCity   varchar (50)='',                                                        
 @vcShipState  varchar (50)='',                                                        
 @vcShipPostCode  varchar (15)='',                                                        
 @vcShipCountry  varchar (50)='',                                                        
 @numGrpId   numeric=0,                                                                                          
 @numTerID   numeric=0,                                                        
 @bitPublicFlag  bit=0,                                                        
 @tintCRMType  tinyint=0,                                                        
 @numUserCntID  numeric=0,                                                                                                                                                              
 @numDomainID  numeric=0,                                                        
 @bitLeadBoxFlg  bit=0,  --Added this because when called form leadbox this value will be 1 else 0                                                        
 @numStatusID  numeric=0,                                                      
 @numCampaignID numeric=0,                                          
 @numFollowUpStatus numeric(9)=0,                                                                                  
 @tintBillingTerms as tinyint,                                                        
 @numBillingDays as numeric(18,0),                                                       
 @tintInterestType as tinyint,                                                        
 @fltInterest as float,                                        
 @vcComFax as varchar(50)=0,                          
 @vcComPhone as varchar(50)=0,                
 @numAssignedTo as numeric(9)=0,    
 @bitNoTax as bit,
 @UpdateDefaultTax as bit=1,
@numCompanyDiff as numeric(9)=0,
@vcCompanyDiff as varchar(100)='',
@numCurrencyID	AS NUMERIC(9,0),
@numAccountClass AS NUMERIC(18,0) = 0,
@numBillAddressID AS NUMERIC(18,0) = 0,
@numShipAddressID AS NUMERIC(18,0) = 0,
@vcPartenerSource AS VARCHAR(300)=0,              
@vcPartenerContact AS VARCHAR(300)=0,
@numPartner AS NUMERIC(18,0) = 0
AS   
BEGIN
	DECLARE @numPartenerSource NUMERIC(18,0)      
	DECLARE @numPartenerContact NUMERIC(18,0)     

	IF ISNULL(@numPartner,0) = 0
	BEGIN
		SET @numPartenerSource=(SELECT TOP 1 numDivisionID FROM DivisionMaster WHERE numDomainId=@numDomainID AND vcPartnerCode=@vcPartenerSource)
	END
	ELSE 
	BEGIN
		SET @numPartenerSource = @numPartner
	END

	SET @numPartenerContact=(SELECT TOP 1 numContactId FROM AdditionalContactsInformation WHERE numDomainId=@numDomainID AND numDivisionId=@numPartenerSource AND vcEmail=@vcPartenerContact)
                                 
	IF ISNULL(@numGrpId,0) = 0 AND ISNULL(@tintCRMType,0) = 0
	BEGIN
 		SET @numGrpId = 0
 		SET @tintCRMType = 1
	END
                                                                                                               
	IF @numDivisionId is null OR @numDivisionId=0                                                 
	BEGIN
		IF @UpdateDefaultTax=1 
			SET @bitNoTax=0
                                                                                  
		INSERT INTO DivisionMaster                                    
		(                      
			numCompanyID,vcDivisionName,numGrpId,numFollowUpStatus,bitPublicFlag,numCreatedBy,bintCreatedDate,numModifiedBy,bintModifiedDate,tintCRMType,numDomainID,                                    
			bitLeadBoxFlg,numTerID,numStatusID,numRecOwner,tintBillingTerms,numBillingDays,tintInterestType,fltInterest,numCampaignID,vcComPhone,vcComFax,bitNoTax,
			numCompanyDiff,vcCompanyDiff,bitActiveInActive,numCurrencyID,numAccountClassID,numPartenerSource,numPartenerContact        
		)                                    
		VALUES                      
		(                      
			@numCompanyID,@vcDivisionName,@numGrpId,@numFollowUpStatus,@bitPublicFlag,@numUserCntID,GETUTCDATE(),@numUserCntID,GETUTCDATE(),@tintCRMType,@numDomainID,                       
			@bitLeadBoxFlg,@numTerID,@numStatusID,@numUserCntID,(CASE WHEN ISNULL(@numBillingDays,0) > 0 THEN 1 ELSE 0 END),@numBillingDays,0,0,@numCampaignID,@vcComPhone,            
			@vcComFax,@bitNoTax,@numCompanyDiff,@vcCompanyDiff,1,@numCurrencyID,@numAccountClass,@numPartenerSource,@numPartenerContact                         
		)                                  
	
		SELECT @numDivisionID = SCOPE_IDENTITY()

		IF @numBillAddressID > 0 OR @numShipAddressID > 0
		BEGIN
			IF @numBillAddressID > 0
			BEGIN
				UPDATE AddressDetails SET numRecordID=@numDivisionID WHERE numAddressID=@numBillAddressID
			END

			IF @numShipAddressID > 0
			BEGIN
				UPDATE AddressDetails SET numRecordID=@numDivisionID WHERE numAddressID=@numShipAddressID
			END
		END
		ELSE
		BEGIN
		  --added By Sachin Sadhu||Issued Founded on:1stFeb14||By:JJ-AudioJenex
			IF EXISTS(SELECT 'col1' FROM dbo.AddressDetails WHERE  bitIsPrimary=1 AND numDomainID=@numDomainID AND numRecordID=@numDivisionID )
			BEGIN
				INSERT INTO dbo.AddressDetails 
				(
					vcAddressName,
					vcStreet,
					vcCity,
					vcPostalCode,
					numState,
					numCountry,
					bitIsPrimary,
					tintAddressOf,
					tintAddressType,
					numRecordID,
					numDomainID
				) 
				SELECT 'Address1',@vcBillStreet,@vcBillCity,@vcBillPostCode,@vcBillState,@vcBillCountry,0,2,1,@numDivisionID,@numDomainID
				union
				SELECT 'Address1',@vcShipStreet,@vcShipCity,@vcShipPostCode,@vcShipState,@vcShipCountry,0,2,2,@numDivisionID,@numDomainID
			END
			ELSE
			BEGIN
				INSERT INTO dbo.AddressDetails 
				(
					vcAddressName,
					vcStreet,
					vcCity,
					vcPostalCode,
					numState,
					numCountry,
					bitIsPrimary,
					tintAddressOf,
					tintAddressType,
					numRecordID,
					numDomainID
				) 
				SELECT 'Address1',@vcBillStreet,@vcBillCity,@vcBillPostCode,@vcBillState,@vcBillCountry,1,2,1,@numDivisionID,@numDomainID
				union
				SELECT 'Address1',@vcShipStreet,@vcShipCity,@vcShipPostCode,@vcShipState,@vcShipCountry,1,2,2,@numDivisionID,@numDomainID
			END
		END
       --End of Sachin Script                   

		IF @UpdateDefaultTax=1 
		BEGIN
			INSERT INTO DivisionTaxTypes
			SELECT 
				@numDivisionID,numTaxItemID,1 
			FROM 
				TaxItems
			WHERE 
				numDomainID=@numDomainID
			UNION
			SELECT 
				@numDivisionID,0,1 
		END                                                
		                  
		DECLARE @numGroupID NUMERIC
		SELECT TOP 1 
			@numGroupID=numGroupID 
		FROM 
			dbo.AuthenticationGroupMaster 
		WHERE 
			numDomainID=@numDomainID 
			AND tintGroupType=2
		
		IF @numGroupID IS NULL 
			SET @numGroupID = 0 
		
		INSERT INTO ExtarnetAccounts 
		(numCompanyID,numDivisionID,numGroupID,numDomainID)                                                
		VALUES
		(@numCompanyID,@numDivisionID,@numGroupID,@numDomainID)                                                                                                                 
	END                                                        
	ELSE if @numDivisionId>0                                                      
	BEGIN
		DECLARE @numFollow AS VARCHAR(10)                                        
		DECLARE @binAdded AS VARCHAR(20)                                        
		DECLARE @PreFollow AS VARCHAR(10)                                        
		DECLARE @RowCount AS VARCHAR(2)                                        
		SET @PreFollow=(SELECT COUNT(*) numFollowUpStatus FROM FollowUpHistory WHERE numDivisionID= @numDivisionID)                                        
    
		SET @RowCount=@@rowcount                                        
    
		SELECT 
			@numFollow=numFollowUpStatus,
			@binAdded=bintModifiedDate 
		FROM 
			divisionmaster
		WHERE 
			numDivisionID=@numDivisionID                                         
    
		IF @numFollow <>'0' AND @numFollow <> @numFollowUpStatus                                        
		BEGIN
			SELECT TOP 1 
				numFollowUpStatus 
			FROM 
				FollowUpHistory 
			WHERE 
				numDivisionID= @numDivisionID 
			ORDER BY 
				numFollowUpStatusID DESC                                        
                                         
			IF @PreFollow<>0                                        
			BEGIN
				IF @numFollow <> (select top 1  numFollowUpStatus from FollowUpHistory where numDivisionID= @numDivisionID order by numFollowUpStatusID desc)                                        
				BEGIN
					INSERT INTO FollowUpHistory
					(numFollowUpstatus,numDivisionID,bintAddedDate)                                        
					VALUES
					(@numFollow,@numDivisionID,@binAdded)                                        
				END                                        
			END                              
			ELSE
			BEGIN
				INSERT INTO FollowUpHistory
				(numFollowUpstatus,numDivisionID,bintAddedDate)                                        
				VALUES
				(@numFollow,@numDivisionID,@binAdded)                                   
			END                                                                 
		END                                                                                            
                                                        
		UPDATE 
			DivisionMaster                       
		SET                        
			numCompanyID = @numCompanyID ,                     
			vcDivisionName = @vcDivisionName,                                                        
			numGrpId = @numGrpId,                                                                       
			numFollowUpStatus =@numFollowUpStatus,                                                        
			numTerID = @numTerID,                                                   
			bitPublicFlag =@bitPublicFlag,                                                        
			numModifiedBy = @numUserCntID,                                                  
			bintModifiedDate = getutcdate(),                                                     
			tintCRMType = @tintCRMType,                                                      
			numStatusID = @numStatusID,                                                 
			tintBillingTerms = (CASE WHEN ISNULL(@numBillingDays,0) > 0 THEN 1 ELSE 0 END),                                                      
			numBillingDays = @numBillingDays,                                                     
			tintInterestType = @tintInterestType,                                                       
			fltInterest =@fltInterest,                          
			vcComPhone=@vcComPhone,                          
			vcComFax= @vcComFax,                      
			numCampaignID=@numCampaignID,    
			bitNoTax=@bitNoTax
			,numCompanyDiff=@numCompanyDiff
			,vcCompanyDiff=@vcCompanyDiff
			,numCurrencyID = @numCurrencyID
			,numPartenerSource=@numPartenerSource
			,numPartenerContact=@numPartenerContact                                                         
		WHERE 
			numDivisionID = @numDivisionID       

		UPDATE 
			dbo.AddressDetails 
		SET
			vcStreet=@vcBillStreet,                                    
			vcCity =@vcBillCity,                                    
			vcPostalCode=@vcBillPostCode,                                     
			numState=@vcBillState,                                    
			numCountry=@vcBillCountry
		WHERE 
			numDomainID=@numDomainID 
			AND numRecordID=@numDivisionID 
			AND bitIsPrimary=1 
			AND tintAddressOf=2 
			AND tintAddressType=1         
	   
		UPDATE 
			dbo.AddressDetails 
		SET
			vcStreet=@vcShipStreet,                                    
			vcCity =@vcShipCity,                                    
			vcPostalCode=@vcShipPostCode,                                     
			numState=@vcShipState,                                    
			numCountry=@vcShipCountry
		WHERE 
			numDomainID=@numDomainID 
			AND numRecordID=@numDivisionID 
			AND bitIsPrimary=1 
			AND tintAddressOf=2 
			AND tintAddressType=2
	END

    ---Updating if organization is assigned to someone                
	DECLARE @tempAssignedTo AS NUMERIC(9)              
              
	SELECT 
		@tempAssignedTo=isnull(numAssignedTo,0) 
	FROM 
		DivisionMaster 
	WHERE 
		numDivisionID = @numDivisionID                
        
	IF (@tempAssignedTo<>@numAssignedTo and  @numAssignedTo<>'0')              
	BEGIN                
		UPDATE 
			DivisionMaster 
		SET 
			numAssignedTo=@numAssignedTo
			,numAssignedBy=@numUserCntID 
		WHERE 
			numDivisionID = @numDivisionID                
	END               
  
	SELECT @numDivisionID
 END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageWorkFlowMaster')
DROP PROCEDURE USP_ManageWorkFlowMaster
GO
Create PROCEDURE [dbo].[USP_ManageWorkFlowMaster]                                        
	@numWFID numeric(18, 0) OUTPUT,
    @vcWFName varchar(200),
    @vcWFDescription varchar(500),   
    @numDomainID numeric(18, 0),
    @numUserCntID numeric(18, 0),
    @bitActive bit,
    @numFormID NUMERIC(18,0),
    @tintWFTriggerOn NUMERIC(18),
    @vcWFAction varchar(Max),
    @vcDateField VARCHAR(MAX),
    @intDays INT,
    @intActionOn INT,
	@bitCustom bit,
	@numFieldID numeric(18),
    @strText TEXT = ''
as                 
DECLARE @ErrorMessage NVARCHAR(4000);SET @ErrorMessage=''
DECLARE @ErrorSeverity INT;
DECLARE @ErrorState INT;
		
BEGIN TRANSACTION;

BEGIN TRY
	DECLARE @hDocItems INT                                                                                                                                                                
	EXEC sp_xml_preparedocument @hDocItems OUTPUT, @strText


	DECLARE @TEMP TABLE
	(
		tintActionType TINYINT,
		numBizDocTypeID NUMERIC(18,0)
	)
	INSERT INTO 
		@TEMP
	SELECT 
		tintActionType,
		numBizDocTypeID 
	FROM 
		OPENXML (@hDocItems, '/NewDataSet/WorkFlowActionList',2) 
	WITH 
		(
			tintActionType TINYINT, 
			numBizDocTypeID numeric(18,0)
		)

	IF (SELECT COUNT(*) FROM @TEMP WHERE tintActionType = 6 AND (numBizDocTypeID=287 OR numBizDocTypeID=29397)) > 0
		BEGIN
			--CHECK IF CREATE INVOICE OR CREATE PACKING SLIP RULE EXISTS IN MASS SALES FULLFILLMENT
			IF (
				SELECT 
					COUNT(*) 
				FROM 
					OpportunityAutomationRules 
				WHERE 
					numDomainID = @numDomainID AND 
					(numRuleID = 1 OR numRuleID = 2)
				) > 0
			BEGIN
				RAISERROR ('MASS SALES FULLFILLMENT RULE EXISTS',16,1);
				RETURN -1
			END
		END


	DECLARE @bitDiplicate BIT = 0

	-- FIRST CHECK IF WORKFLOW WITH SAME MODULE AND TRIGGER TYPE EXISTS
	IF EXISTS (SELECT numWFID FROM WorkFlowMaster WHERE numDomainID=@numDomainID AND numWFID <> @numWFID AND numFormID=@numFormID AND tintWFTriggerOn=@tintWFTriggerOn AND (SELECT COUNT(*) FROM WorkFlowConditionList WHERE WorkFlowConditionList.numWFID = WorkFlowMaster.numWFID) = (SELECT COUNT(*) FROM OPENXML (@hDocItems, '/NewDataSet/WorkFlowConditionList',2) WITH (numFieldID NUMERIC, bitCustom BIT, vcFilterValue VARCHAR(500), vcFilterOperator VARCHAR(10), vcFilterData VARCHAR(MAX))))
	BEGIN
		-- NOT CHECK IF CONDITIONS ARE ALSO SAME
		DECLARE @TEMPWF TABLE
		(
			ID INT IDENTITY(1,1)
			,numWFID NUMERIC(18,0)
		)
		INSERT INTO 
			@TEMPWF 
		SELECT 
			numWFID 
		FROM 
			WorkFlowMaster 
		WHERE 
			numDomainID=@numDomainID
			AND numWFID <> @numWFID
			AND numFormID=@numFormID 
			AND tintWFTriggerOn=@tintWFTriggerOn

		DECLARE @i AS INT = 1
		DECLARE @iCount AS INT
		DECLARE @numTempWFID NUMERIC(18,0)

		SELECT @iCount=COUNT(*) FROM @TEMPWF

		WHILE @i <= @iCount
		BEGIN
			SELECT @numTempWFID=numWFID FROM @TEMPWF WHERE ID=@i

			IF NOT EXISTS (
				SELECT 
					numFieldID,bitCustom,vcFilterValue,vcFilterOperator,vcFilterData 
				FROM 
				(
					SELECT 
						numFieldID
						,bitCustom
						,vcFilterValue
						,vcFilterOperator
						,Split.a.value('.', 'VARCHAR(100)') AS vcFilterData  
					FROM  
					(
						SELECT 
							numFieldID
							,bitCustom
							,vcFilterValue
							,vcFilterOperator
							,CAST ('<M>' + REPLACE(vcFilterData, ',', '</M><M>') + '</M>' AS XML) AS vcFilterData
						FROM 
							OPENXML (@hDocItems, '/NewDataSet/WorkFlowConditionList',2) 
						WITH 
							(numFieldID NUMERIC, bitCustom BIT, vcFilterValue VARCHAR(500), vcFilterOperator VARCHAR(10), vcFilterData VARCHAR(MAX))
					) AS A CROSS APPLY vcFilterData.nodes ('/M') AS Split(a)
				) T1
				EXCEPT
				SELECT 
					numFieldID,bitCustom,vcFilterValue,vcFilterOperator,vcFilterData 
				FROM 
				(
					SELECT 
						numFieldID
						,bitCustom
						,vcFilterValue
						,vcFilterOperator
						,Split.a.value('.', 'VARCHAR(100)') AS vcFilterData  
					FROM  
					(
						SELECT 
							numFieldID
							,bitCustom
							,vcFilterValue
							,vcFilterOperator
							,CAST ('<M>' + REPLACE(vcFilterData, ',', '</M><M>') + '</M>' AS XML) AS vcFilterData  
						FROM
							WorkFlowConditionList
						WHERE
							numWFID=@numTempWFID
					) AS A CROSS APPLY vcFilterData.nodes ('/M') AS Split(a)
				) T2
			)
			SELECT @bitDiplicate = 1 ELSE SELECT @bitDiplicate = 0


			IF NOT EXISTS (
				SELECT 
					numFieldID,bitCustom,vcFilterValue,vcFilterOperator,vcFilterData 
				FROM 
				(
					SELECT 
						numFieldID
						,bitCustom
						,vcFilterValue
						,vcFilterOperator
						,Split.a.value('.', 'VARCHAR(100)') AS vcFilterData  
					FROM  
					(
						SELECT 
							numFieldID
							,bitCustom
							,vcFilterValue
							,vcFilterOperator
							,CAST ('<M>' + REPLACE(vcFilterData, ',', '</M><M>') + '</M>' AS XML) AS vcFilterData
						FROM 
							OPENXML (@hDocItems, '/NewDataSet/WorkFlowConditionList',2) 
						WITH 
							(numFieldID NUMERIC, bitCustom BIT, vcFilterValue VARCHAR(500), vcFilterOperator VARCHAR(10), vcFilterData VARCHAR(MAX))
					) AS A CROSS APPLY vcFilterData.nodes ('/M') AS Split(a)
				) T1	
				
				EXCEPT

				SELECT 
					numFieldID,bitCustom,vcFilterValue,vcFilterOperator,vcFilterData 
				FROM 
				(
					SELECT 
						numFieldID
						,bitCustom
						,vcFilterValue
						,vcFilterOperator
						,Split.a.value('.', 'VARCHAR(100)') AS vcFilterData  
					FROM  
					(
						SELECT 
							numFieldID
							,bitCustom
							,vcFilterValue
							,vcFilterOperator
							,CAST ('<M>' + REPLACE(vcFilterData, ',', '</M><M>') + '</M>' AS XML) AS vcFilterData  
						FROM
							WorkFlowConditionList
						WHERE
							numWFID=@numTempWFID
					) AS A CROSS APPLY vcFilterData.nodes ('/M') AS Split(a)
				) T2				
			)

			SELECT @bitDiplicate = 1 ELSE SELECT @bitDiplicate = 0

			IF @bitDiplicate = 1
			BEGIN
				BREAK
			END

			SET @i = @i + 1
		END
	END


	IF @bitDiplicate = 1
	BEGIN
		RAISERROR ('DUPLICATE_WORKFLOW',16,1);
		RETURN -1
	END

	IF @numWFID=0
	BEGIN
		INSERT INTO [dbo].[WorkFlowMaster] ([numDomainID], [vcWFName], [vcWFDescription], [numCreatedBy], [numModifiedBy], [dtCreatedDate], [dtModifiedDate], [bitActive], [numFormID], [tintWFTriggerOn],[vcWFAction],[vcDateField],[intDays],[intActionOn],[bitCustom],[numFieldID])
		SELECT @numDomainID, @vcWFName, @vcWFDescription, @numUserCntID, @numUserCntID, GETUTCDATE(), GETUTCDATE(), @bitActive, @numFormID, @tintWFTriggerOn,@vcWFAction,@vcDateField,@intDays,@intActionOn,@bitCustom,@numFieldID

		SET @numWFID=@@IDENTITY
		--start of my
		IF DATALENGTH(@strText)>2
		BEGIN                                                                                                         
			--Delete records into WorkFlowTriggerFieldList and Insert
			DELETE FROM WorkFlowTriggerFieldList WHERE  [numWFID] = @numWFID

			INSERT INTO dbo.WorkFlowTriggerFieldList (numWFID,numFieldID,bitCustom) 
				SELECT @numWFID,numFieldID,bitCustom
				  FROM OPENXML (@hDocItems, '/NewDataSet/WorkFlowTriggerFieldList',2) WITH ( numFieldID NUMERIC, bitCustom BIT )
	
	
			--Delete records into WorkFlowConditionList and Insert
			DELETE FROM WorkFlowConditionList WHERE  [numWFID] = @numWFID

			INSERT INTO dbo.WorkFlowConditionList (numWFID,numFieldID,bitCustom,vcFilterValue,vcFilterOperator,vcFilterANDOR,vcFilterData,intCompare) 
				SELECT @numWFID,numFieldID,bitCustom,vcFilterValue,vcFilterOperator,vcFilterANDOR,vcFilterData,intCompare
				  FROM OPENXML (@hDocItems, '/NewDataSet/WorkFlowConditionList',2) WITH ( numFieldID NUMERIC, bitCustom BIT, vcFilterValue VARCHAR(500), vcFilterOperator VARCHAR(10),vcFilterANDOR VARCHAR(10),vcFilterData VARCHAR(MAX),intCompare int)


			--Delete records into WorkFlowActionUpdateFields and Insert
			DELETE FROM WorkFlowActionUpdateFields WHERE numWFActionID IN(SELECT numWFActionID FROM WorkFlowActionList WHERE numWFID=@numWFID)

			--Delete records into WorkFlowActionList and Insert
			DELETE FROM WorkFlowActionList WHERE  [numWFID] = @numWFID

			INSERT INTO dbo.WorkFlowActionList (numWFID,tintActionType,numTemplateID,vcEmailToType,tintTicklerActionAssignedTo,vcAlertMessage,tintActionOrder,tintIsAttachment,vcApproval,tintSendMail,numBizDocTypeID,numBizDocTemplateID,vcSMSText,vcEmailSendTo,vcMailBody,vcMailSubject) 
				SELECT @numWFID,tintActionType,numTemplateID,vcEmailToType,tintTicklerActionAssignedTo,vcAlertMessage,tintActionOrder,tintIsAttachment,vcApproval ,tintSendMail,numBizDocTypeID,numBizDocTemplateID,vcSMSText,vcEmailSendTo,vcMailBody ,vcMailSubject
				  FROM OPENXML (@hDocItems, '/NewDataSet/WorkFlowActionList',2) WITH ( tintActionType TINYINT, numTemplateID NUMERIC,vcEmailToType VARCHAR(50),tintTicklerActionAssignedTo TINYINT,vcAlertMessage NTEXT,tintActionOrder TINYINT,tintIsAttachment TINYINT,vcApproval VARCHAR(100),tintSendMail TINYINT,numBizDocTypeID numeric(18,0),numBizDocTemplateID numeric(18,0),vcSMSText NTEXT,vcEmailSendTo varchar(500),vcMailBody ntext,vcMailSubject varchar(500))

			INSERT INTO dbo.WorkFlowActionUpdateFields (numWFActionID,tintModuleType,numFieldID,bitCustom,vcValue,vcData) 
				SELECT WFA.numWFActionID,tintModuleType,numFieldID,bitCustom,vcValue,vcData
				  FROM OPENXML (@hDocItems, '/NewDataSet/WorkFlowActionUpdateFields',2) WITH ( numWFActionID NUMERIC, tintModuleType NUMERIC,numFieldID NUMERIC,bitCustom BIT,vcValue VARCHAR(500),vcData VARCHAR(500) ) WFAUF
				  JOIN WorkFlowActionList WFA ON WFA.tintActionOrder=WFAUF.numWFActionID WHERE WFA.numWFID=@numWFID

			EXEC sp_xml_removedocument @hDocItems
		END	
		--end of my
	END
	ELSE
	BEGIN
	
		UPDATE [dbo].[WorkFlowMaster]
		SET    [vcWFName] = @vcWFName, [vcWFDescription] = @vcWFDescription, [numModifiedBy] = @numUserCntID, [dtModifiedDate] = GETUTCDATE(), [bitActive] = @bitActive, [numFormID] = @numFormID, [tintWFTriggerOn] = @tintWFTriggerOn,[vcWFAction]=@vcWFAction,[vcDateField]=@vcDateField,[intDays]=@intDays,[intActionOn]=@intActionOn,[bitCustom]=@bitCustom,[numFieldID]=@numFieldID
		WHERE  [numDomainID] = @numDomainID AND [numWFID] = @numWFID
	
		IF DATALENGTH(@strText)>2
		BEGIN
			DECLARE @hDocItem INT                                                                                                                                                                
	
			EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strText                                                                                                             
        
			--Delete records into WorkFlowTriggerFieldList and Insert
			DELETE FROM WorkFlowTriggerFieldList WHERE  [numWFID] = @numWFID

			INSERT INTO dbo.WorkFlowTriggerFieldList (numWFID,numFieldID,bitCustom) 
				SELECT @numWFID,numFieldID,bitCustom
				  FROM OPENXML (@hDocItem, '/NewDataSet/WorkFlowTriggerFieldList',2) WITH ( numFieldID NUMERIC, bitCustom BIT )
	
	
			--Delete records into WorkFlowConditionList and Insert
			DELETE FROM WorkFlowConditionList WHERE  [numWFID] = @numWFID

			INSERT INTO dbo.WorkFlowConditionList (numWFID,numFieldID,bitCustom,vcFilterValue,vcFilterOperator,vcFilterANDOR,vcFilterData,intCompare) 
				SELECT @numWFID,numFieldID,bitCustom,vcFilterValue,vcFilterOperator,vcFilterANDOR,vcFilterData,intCompare
				  FROM OPENXML (@hDocItem, '/NewDataSet/WorkFlowConditionList',2) WITH ( numFieldID NUMERIC, bitCustom BIT, vcFilterValue VARCHAR(500), vcFilterOperator VARCHAR(10),vcFilterANDOR VARCHAR(10),vcFilterData varchar(MAX),intCompare int)


			--Delete records into WorkFlowActionUpdateFields and Insert
			DELETE FROM WorkFlowActionUpdateFields WHERE numWFActionID IN(SELECT numWFActionID FROM WorkFlowActionList WHERE numWFID=@numWFID)

			--Delete records into WorkFlowActionList and Insert
			DELETE FROM WorkFlowActionList WHERE  [numWFID] = @numWFID

			INSERT INTO dbo.WorkFlowActionList (numWFID,tintActionType,numTemplateID,vcEmailToType,tintTicklerActionAssignedTo,vcAlertMessage,tintActionOrder,tintIsAttachment,vcApproval ,tintSendMail,numBizDocTypeID ,numBizDocTemplateID,vcSMSText,vcEmailSendTo,vcMailBody,vcMailSubject ) 
				SELECT @numWFID,tintActionType,numTemplateID,vcEmailToType,tintTicklerActionAssignedTo,vcAlertMessage,tintActionOrder,tintIsAttachment,vcApproval ,tintSendMail,numBizDocTypeID ,numBizDocTemplateID,vcSMSText ,vcEmailSendTo,vcMailBody,vcMailSubject
				  FROM OPENXML (@hDocItem, '/NewDataSet/WorkFlowActionList',2) WITH ( tintActionType TINYINT, numTemplateID NUMERIC,vcEmailToType VARCHAR(50),tintTicklerActionAssignedTo TINYINT,vcAlertMessage NTEXT,tintActionOrder TINYINT,tintIsAttachment TINYINT,vcApproval VARCHAR(100),tintSendMail TINYINT,numBizDocTypeID numeric(18,0),numBizDocTemplateID numeric(18,0),vcSMSText NTEXT ,vcEmailSendTo varchar(500),vcMailBody ntext,vcMailSubject varchar(500))

			INSERT INTO dbo.WorkFlowActionUpdateFields (numWFActionID,tintModuleType,numFieldID,bitCustom,vcValue,vcData) 
				SELECT WFA.numWFActionID,tintModuleType,numFieldID,bitCustom,vcValue,vcData
				  FROM OPENXML (@hDocItem, '/NewDataSet/WorkFlowActionUpdateFields',2) WITH ( numWFActionID NUMERIC, tintModuleType NUMERIC,numFieldID NUMERIC,bitCustom BIT,vcValue VARCHAR(500),vcData VARCHAR(500)) WFAUF
				  JOIN WorkFlowActionList WFA ON WFA.tintActionOrder=WFAUF.numWFActionID WHERE WFA.numWFID=@numWFID

			EXEC sp_xml_removedocument @hDocItem
		END	
	END

	COMMIT
END TRY
BEGIN CATCH

	SELECT @ErrorMessage = ERROR_MESSAGE(), @ErrorSeverity = ERROR_SEVERITY(), @ErrorState = ERROR_STATE();

	IF @@TRANCOUNT > 0
	ROLLBACK TRANSACTION;

	-- Use RAISERROR inside the CATCH block to return error
	-- information about the original error that caused
	-- execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState);
END CATCH;
/****** Object:  StoredProcedure [dbo].[USP_OppManage]    Script Date: 07/26/2008 16:20:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OppManage')
DROP PROCEDURE usp_oppmanage
GO
CREATE PROCEDURE [dbo].[USP_OppManage] 
(                                                                          
  @numOppID as numeric=0 OUTPUT,                                                                          
  @numContactId numeric(9)=null,                                                                          
  @numDivisionId numeric(9)=null,                                                                          
  @tintSource numeric(9)=null,                                                                          
  @vcPOppName Varchar(100)='' OUTPUT,                                                                          
  @Comments varchar(1000)='',                                                                          
  @bitPublicFlag bit=0,                                                                          
  @numUserCntID numeric(9)=0,                                                                                 
  @monPAmount DECIMAL(20,5) =0,                                                 
  @numAssignedTo as numeric(9)=0,                                                                                                        
  @numDomainId numeric(9)=0,                                                                                                                                                                                   
  @strItems as text=null,                                                                          
  @strMilestone as text=null,                                                                          
  @dtEstimatedCloseDate datetime,                                                                          
  @CampaignID as numeric(9)=null,                                                                          
  @lngPConclAnalysis as numeric(9)=null,                                                                         
  @tintOppType as tinyint,                                                                                                                                             
  @tintActive as tinyint,                                                              
  @numSalesOrPurType as numeric(9),              
  @numRecurringId as numeric(9),
  @numCurrencyID as numeric(9)=0,
  @DealStatus as tinyint =0,-- 0=Open,1=Won,2=Lost
  @numStatus AS NUMERIC(18,0),
  @vcOppRefOrderNo VARCHAR(100),
  @numBillAddressId numeric(9)=0,
  @numShipAddressId numeric(9)=0,
  @bitStockTransfer BIT=0,
  @WebApiId INT = 0,
  @tintSourceType TINYINT=0,
  @bitDiscountType as bit,
  @fltDiscount  as float,
  @bitBillingTerms as bit,
  @intBillingDays as integer,
  @bitInterestType as bit,
  @fltInterest as float,
  @tintTaxOperator AS TINYINT,
  @numDiscountAcntType AS NUMERIC(9),
  @vcWebApiOrderNo VARCHAR(100)=NULL,
  @vcCouponCode		VARCHAR(100) = '',
  @vcMarketplaceOrderID VARCHAR(100)=NULL,
  @vcMarketplaceOrderDate  datetime=NULL,
  @vcMarketplaceOrderReportId VARCHAR(100)=NULL,
  @numPercentageComplete NUMERIC(9),
  @bitUseShippersAccountNo BIT = 0,
  @bitUseMarkupShippingRate BIT = 0,
  @numMarkupShippingRate NUMERIC(19,2) = 0,
  @intUsedShippingCompany INT = 0,
  @numShipmentMethod NUMERIC(18,0)=0,
  @dtReleaseDate DATE = NULL,
  @numPartner NUMERIC(18,0)=0,
  @tintClickBtn INT=0,
  @numPartenerContactId NUMERIC(18,0)=0,
  @numAccountClass NUMERIC(18,0) = 0,
  @numWillCallWarehouseID NUMERIC(18,0) = 0,
  @numVendorAddressID NUMERIC(18,0) = 0,
  @numShipFromWarehouse NUMERIC(18,0) = 0,
  @numShippingService NUMERIC(18,0) = 0,
  @ClientTimeZoneOffset INT = 0,
  @vcCustomerPO VARCHAR(100)=NULL,
  @bitDropShipAddress BIT = 0,
  @PromCouponCode AS VARCHAR(100) = NULL,
  @numPromotionId AS NUMERIC(18,0) = 0
  -- USE PARAMETER WITH OPTIONAL VALUE BECAUSE PROCEDURE IS CALLED FROM OTHER PROCEDURE WHICH WILL NOT PASS NEW PARAMETER VALUE
)                                                                          
                                                                          
as  
BEGIN TRY
BEGIN TRANSACTION
	DECLARE @fltExchangeRate float                                 
	DECLARE @hDocItem int                                                                                                                            
	DECLARE @TotalAmount as DECIMAL(20,5)                                                                          
	DECLARE @tintOppStatus as tinyint
	DECLARE @bitNewOrder AS BIT = (CASE WHEN ISNULL(@numOppID,0) = 0 THEN 1 ELSE 0 END)
	
	IF ISNULL(@numContactId,0) = 0
	BEGIN
		RAISERROR('CONCAT_REQUIRED',16,1)
		RETURN
	END  
	
	IF ISNULL(@tintOppType,0) = 0
	BEGIN
		RAISERROR('OPP_TYPE_CAN_NOT_BE_0',16,1)
		RETURN
	END             

	IF ISNULL(@numOppID,0) > 0 AND EXISTS (SELECT numOppId FROM OpportunityMaster WHERE numDomainId=@numDomainId AND numOppId=@numOppID AND ISNULL(tintshipped,0) = 1) 
	BEGIN	 
		RAISERROR('OPP/ORDER_IS_CLOSED',16,1)
		RETURN
	END

	DECLARE @dtItemRelease AS DATE
	SET @dtItemRelease = DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())

	DECLARE @numCompany AS NUMERIC(18)
	DECLARE @numRelationship NUMERIC(18,0)
	DECLARE @numProfile NUMERIC(18,0)
	DECLARE @tintCommitAllocation AS TINYINT
	DECLARE @bitAllocateInventoryOnPickList AS BIT

	SELECT @tintCommitAllocation=ISNULL(tintCommitAllocation,1) FROM Domain WHERE numDomainId=@numDomainId

	SELECT 
		@numCompany = D.numCompanyID
		,@bitAllocateInventoryOnPickList=ISNULL(bitAllocateInventoryOnPickList,0)
		,@numRelationship=numCompanyType
		,@numProfile=vcProfile
	FROM 
		DivisionMaster D
	JOIN 
		CompanyInfo C 
	ON 
		C.numCompanyId=D.numCompanyID
	WHERE 
		D.numDivisionID = @numDivisionId

	IF @numOppID = 0 AND CONVERT(VARCHAR(10),@strItems) <> ''
	BEGIN
		EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems

		--VALIDATE PROMOTION OF COUPON CODE
		DECLARE @TEMP TABLE
		(
			ID INT IDENTITY(1,1),
			numOppItemID NUMERIC(18,0),
			numPromotionID NUMERIC(18,0)
		)

		INSERT INTO @TEMP
		(
			numOppItemID,
			numPromotionID
		)
		SELECT
			numoppitemtCode,
			numPromotionID
		FROM
			OPENXML (@hDocItem,'/NewDataSet/Item',2)
		WITH
		(
			numoppitemtCode NUMERIC(18,0),
			numPromotionID NUMERIC(18,0)
		)
		WHERE
			numPromotionID > 0

		EXEC sp_xml_removedocument @hDocItem 

		IF (SELECT COUNT(*) FROM @TEMP) > 0
		BEGIN
			-- IF ITEM PROMOTION USED IN ORDER EXIPIRES THAN RAISE ERROR
			IF  (SELECT 
						COUNT(PO.numProId)
					FROM 
						PromotionOffer PO
					LEFT JOIN
						PromotionOffer POOrder
					ON
						PO.numOrderPromotionID = POOrder.numProId
					INNER JOIN
						@TEMP T1
					ON
						PO.numProId = T1.numPromotionID
					WHERE 
						PO.numDomainId=@numDomainID 
						AND ISNULL(PO.IsOrderBasedPromotion,0) = 0
						AND ISNULL(PO.bitEnabled,0) = 1
						AND 1 = (CASE 
									WHEN ISNULL(PO.numOrderPromotionID,0) > 0 
									THEN (CASE WHEN POOrder.bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN GETUTCDATE()>=POOrder.dtValidFrom AND GETUTCDATE()<=POOrder.dtValidTo THEN 1 ELSE 0 END) END)
									ELSE (CASE WHEN PO.bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN GETUTCDATE()>=PO.dtValidFrom AND GETUTCDATE()<=PO.dtValidTo THEN 1 ELSE 0 END) END)
								END)
						AND 1 = (CASE 
									WHEN ISNULL(PO.numOrderPromotionID,0) > 0 
									THEN
										(CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numOrderPromotionID AND tintType=2 AND numRelationship=@numRelationship AND numProfile=@numProfile) > 0 THEN 1 ELSE 0 END)
									ELSE
										(CASE PO.tintCustomersBasedOn 
											WHEN 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numProId AND tintType=1 AND numDivisionID=@numDivisionID) > 0 THEN 1 ELSE 0 END)
											WHEN 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numProId AND tintType=2 AND numRelationship=@numRelationship AND numProfile=@numProfile) > 0 THEN 1 ELSE 0 END)
											WHEN 3 THEN 1
											ELSE 0
										END)
								END)
				) <> (SELECT COUNT(*) FROM @TEMP)
			BEGIN
				RAISERROR('ITEM_PROMOTION_EXPIRED',16,1)
				RETURN
			END

			-- NOW IF COUPON BASED ITEM PROMOTION IS USED THEN VALIDATE COUPON AND ITS USAGE
			IF 
			(
				SELECT 
					COUNT(*)
				FROM
					PromotionOffer PO
				INNER JOIN
					@TEMP T1
				ON
					PO.numProId = T1.numPromotionID
				WHERE
					PO.numDomainId = @numDomainId
					AND ISNULL(IsOrderBasedPromotion,0) = 0
					AND ISNULL(numOrderPromotionID,0) > 0 
			) = 1
			BEGIN
				IF ISNULL(@PromCouponCode,0) <> ''
				BEGIN
					IF (SELECT 
							COUNT(*)
						FROM
							PromotionOffer PO
						INNER JOIN 
							PromotionOffer POOrder
						ON
							PO.numOrderPromotionID = POOrder.numProId
						INNER JOIN
							@TEMP T1
						ON
							PO.numProId = T1.numPromotionID
						INNER JOIN
							DiscountCodes DC
						ON
							POOrder.numProId = DC.numPromotionID
						WHERE
							PO.numDomainId = @numDomainId
							AND ISNULL(PO.IsOrderBasedPromotion,0) = 0
							AND ISNULL(DC.vcDiscountCode,0) = @PromCouponCode) = 0
					BEGIN
						RAISERROR('INVALID_COUPON_CODE_ITEM_PROMOTION',16,1)
						RETURN
					END
					ELSE
					BEGIN
						-- IF THERE IS COUPON CODE USAGE LIMIT THAN VALIDATE USAGE LIMIT
						IF (SELECT 
								COUNT(*)
							FROM
								PromotionOffer PO
							INNER JOIN 
								PromotionOffer POOrder
							ON
								PO.numOrderPromotionID = POOrder.numProId
							INNER JOIN
								@TEMP T1
							ON
								PO.numProId = T1.numPromotionID
							INNER JOIN
								DiscountCodes DC
							ON
								POOrder.numProId = DC.numPromotionID
							WHERE
								PO.numDomainId = @numDomainId
								AND ISNULL(PO.IsOrderBasedPromotion,0) = 0
								AND ISNULL(DC.vcDiscountCode,0) = @PromCouponCode
								AND 1 = (CASE WHEN ISNULL(DC.CodeUsageLimit,0) <> 0 THEN 1 ELSE 0 END)
								AND ISNULL((SELECT intCodeUsed FROM DiscountCodeUsage WHERE DiscountCodeUsage.numDiscountId=DC.numDiscountId AND numDivisionId=@numDivisionId),0) >= DC.CodeUsageLimit) > 0
						BEGIN
							RAISERROR('ITEM_PROMOTION_COUPON_CODE_USAGE_LIMIT_EXCEEDED',16,1)
							RETURN
						END
					END
				END
				ELSE
				BEGIN
					RAISERROR('COUPON_CODE_REQUIRED_ITEM_PROMOTION',16,1)
					RETURN
				END
			END
		END
	END

	--IF NEW OPPERTUNITY
	IF @numOppID = 0                                                                          
	BEGIN
		DECLARE @bitIsInitalSalesOrder BIT
		IF(@DealStatus=1 AND @tintOppType=1)
		BEGIN
			SET @bitIsInitalSalesOrder=1
		END

		DECLARE @intOppTcode AS NUMERIC(9)  
  
		SELECT @intOppTcode = MAX(numOppId) FROM OpportunityMaster                

		SET @intOppTcode = ISNULL(@intOppTcode,0) + 1                                                
		
		SET @vcPOppName = @vcPOppName + '-'+ CONVERT(varchar(4),YEAR(GETUTCDATE()))  +'-A'+ CONVERT(varchar(10),@intOppTcode)                                  
  
		IF ISNULL(@numCurrencyID,0) = 0 
		BEGIN
			SET @fltExchangeRate=1
			SELECT @numCurrencyID= ISNULL(numCurrencyID,0) FROM dbo.Domain WHERE numDomainId=@numDomainId	
		END
		ELSE
		BEGIN 
			SET @fltExchangeRate = dbo.GetExchangeRate(@numDomainID,@numCurrencyID)
        END  
		                                                        
		IF ISNULL(@numAccountClass,0) = 0
		BEGIN                                                  
		DECLARE @tintDefaultClassType AS INT = 0
		SELECT @tintDefaultClassType = ISNULL(tintDefaultClassType,0) FROM Domain WHERE numDomainId = @numDomainID 

		IF @tintDefaultClassType = 1 --USER
		BEGIN
			SELECT 
				@numAccountClass=ISNULL(numDefaultClass,0) 
			FROM 
				dbo.UserMaster UM 
			JOIN 
				dbo.Domain D 
			ON 
				UM.numDomainID=D.numDomainId
			WHERE 
				D.numDomainId=@numDomainId 
				AND UM.numUserDetailId=@numUserCntID
		END
		ELSE IF @tintDefaultClassType = 2 --COMPANY
		BEGIN
			SELECT 
				@numAccountClass=ISNULL(numAccountClassID,0) 
			FROM 
				dbo.DivisionMaster DM 
			WHERE 
				DM.numDomainId=@numDomainId 
				AND DM.numDivisionID=@numDivisionID
		END
		ELSE
		BEGIN
			SET @numAccountClass = 0
		END
		END

        IF(@DealStatus=1 AND @tintOppType=1 AND @tintClickBtn=1)
		BEGIN
			SET @DealStatus=0
		END         
		--PRINT ('Deal Staus ' +@DealStatus)  

		-------- VALIDATE ORDER BASED PROMOTION -------------

		DECLARE @numDiscountID NUMERIC(18,0) = NULL
		IF @numPromotionId > 0 
		BEGIN
			-- VALIDTE IF ITS ORDER BASED PROMOTION (BOTH ITEM AND ORDER BASED PROMOTION CAN HAVE COUPON CODE)
			IF EXISTS (SELECT numProId FROM PromotionOffer WHERE numDomainId=@numDomainId AND numProId=@numPromotionId AND ISNULL(IsOrderBasedPromotion,0) = 1)
			BEGIN
				-- CHECK IF ORDER PROMOTION IS STILL VALID
				IF NOT EXISTS (SELECT 
									PO.numProId
								FROM 
									PromotionOffer PO
								INNER JOIN 
									PromotionOfferOrganizations PORG
								ON 
									PO.numProId = PORG.numProId
								WHERE 
									numDomainId=@numDomainID 
									AND PO.numProId=@numPromotionId
									AND ISNULL(IsOrderBasedPromotion,0) = 1
									AND ISNULL(bitEnabled,0) = 1
									AND 1 = (CASE WHEN bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN GETUTCDATE()>=dtValidFrom AND GETUTCDATE()<=dtValidTo THEN 1 ELSE 0 END) END)
									AND numRelationship=@numRelationship 
									AND numProfile=@numProfile
				)
				BEGIN
					RAISERROR('ORDER_PROMOTION_EXPIRED',16,1)
					RETURN
				END
				ELSE IF ISNULL((SELECT ISNULL(bitRequireCouponCode,0) FROM PromotionOffer WHERE numDomainId=@numDomainId AND numProId=@numPromotionId),0) = 1
				BEGIN
					IF ISNULL(@PromCouponCode,0) <> ''
					BEGIN
						IF (SELECT 
								COUNT(*)
							FROM
								PromotionOffer PO
							INNER JOIN
								DiscountCodes DC
							ON
								PO.numProId = DC.numPromotionID
							WHERE
								PO.numDomainId = @numDomainId
								AND PO.numProId = @numPromotionId
								AND ISNULL(IsOrderBasedPromotion,0) = 1
								AND ISNULL(bitRequireCouponCode,0) = 1
								AND ISNULL(DC.vcDiscountCode,0) = @PromCouponCode) = 0
						BEGIN
							RAISERROR('INVALID_COUPON_CODE_ORDER_PROMOTION',16,1)
							RETURN
						END
						ELSE
						BEGIN
							-- IF THERE IS COUPON CODE USAGE LIMIT THAN VALIDATE USAGE LIMIT
							IF (SELECT 
									COUNT(*)
								FROM
									PromotionOffer PO
								INNER JOIN
									DiscountCodes DC
								ON
									PO.numProId = DC.numPromotionID
								WHERE
									PO.numDomainId = @numDomainId
									AND PO.numProId = @numPromotionId
									AND ISNULL(IsOrderBasedPromotion,0) = 1
									AND ISNULL(bitRequireCouponCode,0) = 1
									AND ISNULL(DC.vcDiscountCode,0) = @PromCouponCode
									AND 1 = (CASE WHEN ISNULL(DC.CodeUsageLimit,0) <> 0 THEN 1 ELSE 0 END)
									AND ISNULL((SELECT intCodeUsed FROM DiscountCodeUsage WHERE DiscountCodeUsage.numDiscountId=DC.numDiscountId AND numDivisionId=@numDivisionId),0) >= DC.CodeUsageLimit) > 0
							BEGIN
								RAISERROR('ORDER_PROMOTION_COUPON_CODE_USAGE_LIMIT_EXCEEDED',16,1)
								RETURN
							END
							ELSE
							BEGIN			
								SELECT
									@numDiscountID=numDiscountId
								FROM
									DiscountCodes DC
								WHERE
									DC.numPromotionID=@numPromotionId
									AND ISNULL(DC.vcDiscountCode,'')=@PromCouponCode
									
								IF EXISTS (SELECT DC.numDiscountID FROM DiscountCodes DC INNER JOIN DiscountCodeUsage DCU ON DC.numDiscountID=DCU.numDIscountID WHERE DC.numPromotionID=@numPromotionId AND DCU.numDivisionID=@numDivisionID AND ISNULL(DC.vcDiscountCode,'')=@PromCouponCode)
								BEGIN
									UPDATE
										DCU
									SET
										DCU.intCodeUsed = ISNULL(DCU.intCodeUsed,0) + 1
									FROM 
										DiscountCodes DC 
									INNER JOIN 
										DiscountCodeUsage DCU 
									ON 
										DC.numDiscountID=DCU.numDIscountID 
									WHERE 
										DC.numPromotionID=@numPromotionId 
										AND DCU.numDivisionID=@numDivisionID 
										AND ISNULL(DC.vcDiscountCode,'')=@PromCouponCode
								END
								ELSE
								BEGIN
									INSERT INTO DiscountCodeUsage
									(
										numDiscountId
										,numDivisionId
										,intCodeUsed
									)
									SELECT 
										DC.numDiscountId
										,@numDivisionId
										,1
									FROM
										DiscountCodes DC
									WHERE
										DC.numPromotionID=@numPromotionId
										AND ISNULL(DC.vcDiscountCode,'')=@PromCouponCode
								END
							END
						END
					END
					ELSE
					BEGIN
						RAISERROR('COUPON_CODE_REQUIRED_ORDER_PROMOTION',16,1)
						RETURN
					END
				END
			END
		END

		-------- ORDER BASED Promotion Logic-------------
		                                                    
		INSERT INTO OpportunityMaster
		(                                                                             
			numContactId,numDivisionId,txtComments,numCampainID,bitPublicFlag,tintSource,tintSourceType,vcPOppName,                                                                          
			intPEstimatedCloseDate,monPAmount,numCreatedBy,bintCreatedDate,numModifiedBy,bintModifiedDate,numDomainId,                                                                                                             
			numRecOwner,lngPConclAnalysis,tintOppType,numSalesOrPurType,numCurrencyID,fltExchangeRate,numAssignedTo,
			numAssignedBy,[tintOppStatus],numStatus,vcOppRefOrderNo,bitStockTransfer,bitBillingTerms,intBillingDays,
			bitInterestType,fltInterest,vcCouponCode,bitDiscountType,fltDiscount,bitPPVariance,vcMarketplaceOrderID,
			vcMarketplaceOrderReportId,numPercentageComplete,numAccountClass,tintTaxOperator,bitUseShippersAccountNo,
			bitUseMarkupShippingRate,numMarkupShippingRate,intUsedShippingCompany,numShipmentMethod,numShippingService,numPartner,numPartenerContact,dtReleaseDate,bitIsInitalSalesOrder,numVendorAddressID,numShipFromWarehouse
			,vcCustomerPO#,bitDropShipAddress,numDiscountID
		)                                                                          
		Values                                                                          
		(                                                                          
			@numContactId,@numDivisionId,ISNULL(@Comments,''),@CampaignID,@bitPublicFlag,@tintSource,@tintSourceType,@vcPOppName,                                                                          
			@dtEstimatedCloseDate,@monPAmount,@numUserCntID,(CASE @tintSourceType WHEN 3 THEN @vcMarketplaceOrderDate ELSE GETUTCDATE() END),                                                                          
			@numUserCntID,GETUTCDATE(),@numDomainId,@numUserCntID,@lngPConclAnalysis,@tintOppType,@numSalesOrPurType,@numCurrencyID, 
			@fltExchangeRate,(CASE WHEN @numAssignedTo > 0 THEN @numAssignedTo ELSE NULL END),(CASE WHEN @numAssignedTo>0 THEN @numUserCntID ELSE NULL END),
			@DealStatus,@numStatus,@vcOppRefOrderNo,@bitStockTransfer,@bitBillingTerms,@intBillingDays,@bitInterestType,@fltInterest,
			@PromCouponCode,@bitDiscountType,@fltDiscount,1,@vcMarketplaceOrderID,@vcMarketplaceOrderReportId,@numPercentageComplete,
			@numAccountClass,@tintTaxOperator,@bitUseShippersAccountNo,@bitUseMarkupShippingRate,@numMarkupShippingRate,@intUsedShippingCompany,@numShipmentMethod,@numShippingService,@numPartner,@numPartenerContactId,@dtReleaseDate,@bitIsInitalSalesOrder,@numVendorAddressID,@numShipFromWarehouse
			,@vcCustomerPO,@bitDropShipAddress,@numDiscountID
		)                                                                                                                      
		
		SET @numOppID=(SELECT SCOPE_IDENTITY())                
                            
		--UPDATE OPPNAME AS PER NAME TEMPLATE
		EXEC dbo.USP_UpdateNameTemplateValue @tintOppType,@numDomainID,@numOppID

		SELECT @vcPOppName=ISNULL(vcPOppName,'') FROM OpportunityMaster WHERE numOppId=@numOppID

		--MAP CUSTOM FIELD	
		DECLARE @tintPageID AS TINYINT;
		SET @tintPageID = CASE WHEN @tintOppType=1 THEN 2 ELSE 6 END 
  	
		EXEC dbo.USP_AddParentChildCustomFieldMap
				@numDomainID = @numDomainID, --  numeric(18, 0)
				@numRecordID = @numOppID, --  numeric(18, 0)
				@numParentRecId = @numDivisionId, --  numeric(18, 0)
				@tintPageID = @tintPageID --  tinyint
 	
		IF ISNULL(@numStatus,0) > 0 AND @tintOppType=1 AND isnull(@DealStatus,0)=1
		BEGIN
	   		EXEC USP_ManageOpportunityAutomationQueue
					@numOppQueueID = 0, -- numeric(18, 0)
					@numDomainID = @numDomainID, -- numeric(18, 0)
					@numOppId = @numOppID, -- numeric(18, 0)
					@numOppBizDocsId = 0, -- numeric(18, 0)
					@numOrderStatus = @numStatus, -- numeric(18, 0)
					@numUserCntID = @numUserCntID, -- numeric(18, 0)
					@tintProcessStatus = 1, -- tinyint
					@tintMode = 1 -- TINYINT
		END
  
		-- INSERTING ITEMS                                                                                                                    
		IF CONVERT(VARCHAR(10),@strItems) <> '' AND @numOppID > 0
		BEGIN
			EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems       
	
			INSERT INTO OpportunityItems
			(
				numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,bitDropShip,bitDiscountType,fltDiscount,
				monTotAmtBefDiscount,vcItemName,vcModelID,vcManufacturer,vcPathForTImage,monVendorCost,numUOMId,bitWorkOrder,numVendorWareHouse,
				numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,numClassID,numToWarehouseItemID,vcAttributes,vcAttrValues,monAvgCost,bitItemPriceApprovalRequired,
				numPromotionID,bitPromotionTriggered,vcPromotionDetail,numSortOrder,numCost,vcNotes,vcInstruction,bintCompliationDate,numWOAssignedTo,numShipToAddressID,ItemReleaseDate
				,bitMarkupDiscount,vcChildKitSelectedItems
			)
			SELECT 
				@numOppID,X.numItemCode,dbo.fn_UOMConversion(X.numUOM,numItemCode,@numDomainId,null) * x.numUnitHour,x.monPrice,x.monTotAmount,X.vcItemDesc,
				(CASE X.numWarehouseItmsID 
				WHEN -1 
				THEN 
					CASE 
						WHEN ((SELECT COUNT(*) FROM Item WHERE numDomainID=@numDomainId AND vcSKU= X.vcSKU) > 0)
							THEN
								(SELECT TOP 1 
									numWareHouseItemID 
								FROM 
									WareHouseItems 
								WHERE 
									[numItemID] = X.numItemCode 
									AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId))
						ELSE
							(SELECT 
								[numWareHouseItemID] 
							FROM 
								[WareHouseItems] 
							WHERE 
								[numItemID] = X.numItemCode 
								AND ISNULL(WareHouseItems.vcWHSKU,'') = ISNULL(X.vcSKU,'')
								AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId)) 
					END
				WHEN 0 
				THEN 
					CASE 
					WHEN ((SELECT COUNT(*) FROM Item WHERE numDomainID=@numDomainId AND vcSKU=X.vcSKU) > 0)
						THEN
							(SELECT TOP 1 
								numWareHouseItemID 
							FROM 
								WareHouseItems 
							WHERE 
								[numItemID] = X.numItemCode 
								AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId))
					ELSE
						(SELECT 
							[numWareHouseItemID] 
						FROM 
							[WareHouseItems] 
						WHERE 
							[numItemID] = X.numItemCode 
							AND ISNULL(WareHouseItems.vcWHSKU,'') = ISNULL(X.vcSKU,'')
							AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId)) 
					END
				ELSE  
					X.numWarehouseItmsID 
				END) AS numWarehouseItmsID,
				X.ItemType,X.DropShip,X.bitDiscountType,X.fltDiscount,X.monTotAmtBefDiscount,X.vcItemName,(SELECT vcModelID FROM item WHERE numItemCode = X.numItemCode),
				(SELECT vcManufacturer FROM item WHERE numItemCode = X.numItemCode),(SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = X.numItemCode AND BitDefault = 1 AND numdomainId = @numdomainId),
				(SELECT isnull(VN.monCost,0) FROM Vendor VN INNER JOIN Item IT ON IT.numItemCode=VN.numItemCode AND VN.numVendorID=IT.numVendorID WHERE  VN.numItemCode=X.numItemCode),
				X.numUOM,X.bitWorkOrder,X.numVendorWareHouse,X.numShipmentMethod,X.numSOVendorId,X.numProjectID,X.numProjectStageID,
				0,
				X.numToWarehouseItemID,X.Attributes,X.AttributeIDs,(SELECT (CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(I.monAverageCost,0) END) FROM Item I WHERE I.numDomainId=@numDomainId AND I.numItemCode=X.numItemCode),
				X.bitItemPriceApprovalRequired,X.numPromotionID,X.bitPromotionTriggered,X.vcPromotionDetail,X.numSortOrder,ISNULL(X.numCost,0),ISNULL(X.vcVendorNotes,'')
				,X.vcInstruction,X.bintCompliationDate,X.numWOAssignedTo,X.numShipToAddressID,ISNULL(ItemReleaseDate,@dtItemRelease),X.bitMarkupDiscount,ISNULL(X.KitChildItems,'')
			FROM
				(
					SELECT 
						*
					FROM 
						OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
					WITH                       
					(                                                                          
						numoppitemtCode NUMERIC(9),numItemCode NUMERIC(9), numUnitHour FLOAT, monPrice DECIMAL(30,16), monTotAmount DECIMAL(20,5),vcItemDesc VARCHAR(1000),                                    
						numWarehouseItmsID NUMERIC(9), ItemType VARCHAR(30), DropShip BIT, bitDiscountType BIT,	fltDiscount DECIMAL(30,16), monTotAmtBefDiscount DECIMAL(20,5),
						vcItemName VARCHAR(300),numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId NUMERIC(9),
						numProjectID numeric(9),numProjectStageID numeric(9),numToWarehouseItemID NUMERIC(9),Attributes varchar(500),AttributeIDs VARCHAR(500),vcSKU VARCHAR(100)
						,bitItemPriceApprovalRequired BIT,numPromotionID NUMERIC(18,0),bitPromotionTriggered BIT,vcPromotionDetail VARCHAR(2000),numSortOrder NUMERIC(18,0)
						,numCost DECIMAL(20,5),vcVendorNotes VARCHAR(1000),vcInstruction VARCHAR(1000),bintCompliationDate DATETIME,numWOAssignedTo NUMERIC(18,0)
						,numShipToAddressID  NUMERIC(18,0),ItemReleaseDate DATETIME,bitMarkupDiscount BIT,KitChildItems VARCHAR(MAX)
					)
				)X    
			ORDER BY 
				X.numoppitemtCode
    
			-- UPDATE EXISTING CUSTOMER PART NO
			UPDATE
				CPN
			SET
				CPN.CustomerPartNo = ISNULL(TEMPCPN.CustomerPartNo,'')
			FROM
				CustomerPartNumber CPN
			INNER JOIN
			(
				SELECT 
					numItemCode
					,MIN(CustomerPartNo) AS CustomerPartNo
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
				WITH                       
				(                                                                          
					CustomerPartNo VARCHAR(300), numItemCode NUMERIC(18,0)
				)
				GROUP BY
					numItemCode
			) AS TEMPCPN
			ON
				CPN.numCompanyId = @numCompany
				AND CPN.numItemCode = TEMPCPN.numItemCode
			WHERE
				CPN.numDomainID=@numDomainID
				AND CPN.numCompanyId = @numCompany
				AND LEN(ISNULL(TEMPCPN.CustomerPartNo,'')) > 0
				AND ISNULL(TEMPCPN.CustomerPartNo,'') <> '0'
			
			-- INSERT NEW CUSTOMER PART NO
			INSERT INTO CustomerPartNumber
			(
				numCompanyId, numDomainID, CustomerPartNo, numItemCode 
			)
			SELECT 
				@numCompany, @numDomainID, TEMPCPN.CustomerPartNo,TEMPCPN.numItemCode
			FROM
			(
				SELECT 
					numItemCode
					,MIN(CustomerPartNo) AS CustomerPartNo
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
				WITH                       
				(                                                                          
					CustomerPartNo VARCHAR(300), numItemCode NUMERIC(18,0)
				)
				GROUP BY
					numItemCode
			) TEMPCPN 
			LEFT JOIN
				CustomerPartNumber CPN
			ON
				CPN.numCompanyId = @numCompany
				AND CPN.numItemCode = TEMPCPN.numItemCode
			WHERE
				CPN.CustomerPartNoID IS NULL
				AND LEN(ISNULL(TEMPCPN.CustomerPartNo,'')) > 0
				AND ISNULL(TEMPCPN.CustomerPartNo,'') <> '0'

			UPDATE 
				OpportunityItems                       
			SET 
				numItemCode=X.numItemCode,numOppId=@numOppID,numUnitHour=dbo.fn_UOMConversion(X.numUOM,X.numItemCode,@numDomainId,null) * X.numUnitHour,                      
				monPrice=x.monPrice,monTotAmount=x.monTotAmount,vcItemDesc=X.vcItemDesc,numWarehouseItmsID=X.numWarehouseItmsID,bitDropShip=X.DropShip,
				bitDiscountType=X.bitDiscountType,fltDiscount=X.fltDiscount,monTotAmtBefDiscount=X.monTotAmtBefDiscount,vcItemName=X.vcItemName,numUOMId=numUOM,
				bitWorkOrder=X.bitWorkOrder,numVendorWareHouse=X.numVendorWareHouse,numShipmentMethod=X.numShipmentMethod,numSOVendorId=X.numSOVendorId,
				numProjectId=X.numProjectID,numProjectStageId=X.numProjectStageID,numToWarehouseItemID = X.numToWarehouseItemID,vcAttributes=X.Attributes,
				numClassID=0,vcModelID=X.vcModelID,
				bitItemPriceApprovalRequired = X.bitItemPriceApprovalRequired,vcAttrValues=X.AttributeIDs,
				numPromotionID=X.numPromotionID,bitPromotionTriggered=X.bitPromotionTriggered,vcPromotionDetail=X.vcPromotionDetail,numSortOrder=X.numSortOrder,numCost=X.numCost
				,vcNotes=X.vcVendorNotes,vcInstruction=X.vcInstruction,bintCompliationDate=X.bintCompliationDate,numWOAssignedTo=X.numWOAssignedTo,ItemRequiredDate=X.ItemRequiredDate
				,bitMarkupDiscount=X.bitMarkupDiscount
			FROM 
			(
				SELECT 
					numoppitemtCode as numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,vcModelID,numWarehouseItmsID,DropShip,bitDiscountType,fltDiscount,
					monTotAmtBefDiscount,vcItemName,numUOM,bitWorkOrder,numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,numToWarehouseItemID,
					Attributes,bitItemPriceApprovalRequired,AttributeIDs,numPromotionID,bitPromotionTriggered,vcPromotionDetail,numSortOrder,ISNULL(numCost,0) numCost
					,ISNULL(vcVendorNotes,'') vcVendorNotes,vcInstruction,bintCompliationDate,numWOAssignedTo,ItemRequiredDate,bitMarkupDiscount
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=2]',2)
				WITH  
					(                      
						numoppitemtCode NUMERIC(9),numItemCode NUMERIC(9),numUnitHour FLOAT,monPrice DECIMAL(30,16),monTotAmount DECIMAL(20,5),vcItemDesc VARCHAR(1000), vcModelID VARCHAR(300),
						numWarehouseItmsID NUMERIC(9),DropShip BIT,bitDiscountType BIT,fltDiscount DECIMAL(30,16),monTotAmtBefDiscount DECIMAL(20,5),vcItemName VARCHAR(300),
						numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId NUMERIC(9),numProjectID NUMERIC(9),
						numProjectStageID numeric(9),numToWarehouseItemID NUMERIC(9),Attributes varchar(500),bitItemPriceApprovalRequired BIT,AttributeIDs VARCHAR(500),
						numPromotionID NUMERIC(18,0),bitPromotionTriggered BIT,vcPromotionDetail VARCHAR(2000),numSortOrder NUMERIC(18,0),numCost DECIMAL(20,5),vcVendorNotes VARCHAR(1000)
						,vcInstruction VARCHAR(1000),bintCompliationDate DATETIME,numWOAssignedTo NUMERIC(18,0),ItemRequiredDate DATETIME,bitMarkupDiscount BIT
					)
			)X 
			WHERE 
				numoppitemtCode=X.numOppItemID AND numOppID=@numOppID
				
			--INSERT KIT ITEM HAS ANOTHER KIT AS CHILD THEN SAVE CONFIGURATION OF KIT
			DECLARE @TempKitConfiguration TABLE
			(
				numOppItemID NUMERIC(18,0),
				numItemCode NUMERIC(18,0),
				KitChildItems VARCHAR(MAX)
			)

			INSERT INTO 
				@TempKitConfiguration
			SELECT
				numoppitemtCode,
				numItemCode,
				vcChildKitSelectedItems
			FROM
				OpportunityItems
			WHERE
				numOppId = @numOppID
				AND LEN(ISNULL(vcChildKitSelectedItems,'')) > 0

			--INSERT KIT ITEMS 
			IF (SELECT 
					COUNT(*) 
				FROM 
					OpportunityItems OI 
				LEFT JOIN
					WarehouseItems WI
				ON
					OI.numWarehouseItmsID = WI.numWarehouseItemID
				LEFT JOIN
					Warehouses W
				ON
					WI.numWarehouseID = W.numWarehouseID
				JOIN 
					ItemDetails ID 
				ON 
					OI.numItemCode=ID.numItemKitID 
				JOIN
					Item I
				ON
					ID.numChildItemID = I.numItemCode
				WHERE 
					OI.numOppId=@numOppId  
					AND ISNULL(OI.bitDropShip,0) = 0
					AND ISNULL(OI.numWarehouseItmsID,0) > 0
					AND I.charItemType = 'P'
					AND 1 = (CASE 
								WHEN (SELECT 
											COUNT(*) 
										FROM 
											@TempKitConfiguration t1
										CROSS APPLY
										(
											SELECT
												*
											FROM 
												dbo.Split((SELECT KitChildItems FROM @TempKitConfiguration t3 WHERE t3.numOppItemID=OI.numoppitemtCode AND t3.numItemCode=OI.numItemCode),',') 
										) AS t2 
										WHERE 
											SUBSTRING(items,0,CHARINDEX('-',items)) = '0') > 0 
								THEN 
									(CASE 
										WHEN I.numItemCode IN (SELECT 
																	SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items))
																FROM 
																	@TempKitConfiguration t1
																CROSS APPLY
																(
																	SELECT
																		*
																	FROM 
																		dbo.Split((SELECT KitChildItems FROM @TempKitConfiguration t3 WHERE t3.numOppItemID=OI.numoppitemtCode AND t3.numItemCode=OI.numItemCode),',') 
																) AS t2 
																WHERE 
																	SUBSTRING(items,0,CHARINDEX('-',items)) = '0') 
										THEN 1 
										ELSE 0 
									END)
								ELSE 1 END)
					AND ISNULL((SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ID.numChildItemID AND numWareHouseID = W.numWarehouseID),0) = 0) > 0
			BEGIN
				RAISERROR('SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS',16,1)
			END

			           		        
			INSERT INTO OpportunityKitItems
			(
				numOppId,
				numOppItemID,
				numChildItemID,
				numWareHouseItemId,
				numQtyItemsReq,
				numQtyItemsReq_Orig,
				numUOMId,
				numQtyShipped,
				monAvgCost
			)
			SELECT
				@numOppId,
				OI.numoppitemtCode,
				ID.numChildItemID,
				(SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ID.numChildItemID AND WareHouseItems.numWareHouseID = W.numWarehouseID ORDER BY numWareHouseItemID),
				(ID.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(ID.numUOMId,ID.numChildItemID,@numDomainID,I.numBaseUnit),1)) * OI.numUnitHour,
				ID.numQtyItemsReq,
				ID.numUOMId,
				0,
				ISNULL(I.monAverageCost,0)
			FROM 
				OpportunityItems OI 
			LEFT JOIN
				WarehouseItems WI
			ON
				OI.numWarehouseItmsID = WI.numWarehouseItemID
			LEFT JOIN
				Warehouses W
			ON
				WI.numWarehouseID = W.numWarehouseID
			JOIN 
				ItemDetails ID 
			ON 
				OI.numItemCode=ID.numItemKitID 
			JOIN
				Item I
			ON
				ID.numChildItemID = I.numItemCode
			WHERE 
				OI.numOppId=@numOppId
				AND 1 = (CASE 
								WHEN (SELECT 
											COUNT(*) 
										FROM 
											@TempKitConfiguration t1
										CROSS APPLY
										(
											SELECT
												*
											FROM 
												dbo.Split((SELECT KitChildItems FROM @TempKitConfiguration t3 WHERE t3.numOppItemID=OI.numoppitemtCode AND t3.numItemCode=OI.numItemCode),',') 
										) AS t2 
										WHERE 
											SUBSTRING(items,0,CHARINDEX('-',items)) = '0') > 0 
								THEN 
									(CASE 
										WHEN I.numItemCode IN (SELECT 
																	SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items))
																FROM 
																	@TempKitConfiguration t1
																CROSS APPLY
																(
																	SELECT
																		*
																	FROM 
																		dbo.Split((SELECT KitChildItems FROM @TempKitConfiguration t3 WHERE t3.numOppItemID=OI.numoppitemtCode AND t3.numItemCode=OI.numItemCode),',') 
																) AS t2 
																WHERE 
																	SUBSTRING(items,0,CHARINDEX('-',items)) = '0') 
										THEN 1 
										ELSE 0 
									END)
								ELSE 1 END)

			IF (SELECT COUNT(*) FROM OPENXML (@hDocItem,'/NewDataSet/Item[bitHasKitAsChild=''true'']',2)) > 0
			BEGIN
				IF (SELECT 
						COUNT(*) 
					FROM 
					(
						SELECT 
							t1.numOppItemID
							,t1.numItemCode
							,SUBSTRING(items,0,CHARINDEX('-',items)) AS numKitItemID
							,SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)) As numKitChildItemID
						FROM 
							@TempKitConfiguration t1
						CROSS APPLY
						(
							SELECT
								*
							FROM 
								dbo.Split((SELECT KitChildItems FROM @TempKitConfiguration t3 WHERE t3.numOppItemID=t1.numOppItemID AND t3.numItemCode=t1.numItemCode),',') 
						) as t2
					) TempChildItems
					INNER JOIN
						OpportunityKitItems OKI
					ON
						OKI.numOppId=@numOppId
						AND OKI.numOppItemID=TempChildItems.numOppItemID
						AND OKI.numChildItemID=TempChildItems.numKitItemID
					INNER JOIN
						OpportunityItems OI
					ON
						OI.numItemCode = TempChildItems.numItemCode
						AND OI.numoppitemtcode = OKI.numOppItemID
					LEFT JOIN
						WarehouseItems WI
					ON
						OI.numWarehouseItmsID = WI.numWarehouseItemID
					LEFT JOIN
						Warehouses W
					ON
						WI.numWarehouseID = W.numWarehouseID
					INNER JOIN
						ItemDetails
					ON
						TempChildItems.numKitItemID = ItemDetails.numItemKitID
						AND TempChildItems.numKitChildItemID = ItemDetails.numChildItemID
					INNER JOIN
						Item 
					ON
						ItemDetails.numChildItemID = Item.numItemCode
					WHERE
						Item.charItemType = 'P'
						AND ISNULL((SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ItemDetails.numChildItemID AND numWareHouseID = W.numWarehouseID),0) = 0) > 0
				BEGIN
					RAISERROR('SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS',16,1)
				END

				INSERT INTO OpportunityKitChildItems
				(
					numOppID,
					numOppItemID,
					numOppChildItemID,
					numItemID,
					numWareHouseItemId,
					numQtyItemsReq,
					numQtyItemsReq_Orig,
					numUOMId,
					numQtyShipped,
					monAvgCost
				)
				SELECT 
					@numOppID,
					OKI.numOppItemID,
					OKI.numOppChildItemID,
					ItemDetails.numChildItemID,
					(SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ItemDetails.numChildItemID AND WareHouseItems.numWareHouseID = W.numWarehouseID ORDER BY numWareHouseItemID),
					(ItemDetails.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(ItemDetails.numUOMId,ItemDetails.numChildItemID,@numDomainID,Item.numBaseUnit),1)) * OKI.numQtyItemsReq,
					ItemDetails.numQtyItemsReq,
					ItemDetails.numUOMId,
					0,
					ISNULL(Item.monAverageCost,0)
				FROM
				(
					SELECT 
						t1.numOppItemID
						,t1.numItemCode
						,SUBSTRING(items,0,CHARINDEX('-',items)) AS numKitItemID
						,SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)) As numKitChildItemID
					FROM 
						@TempKitConfiguration t1
					CROSS APPLY
					(
						SELECT
							*
						FROM 
							dbo.Split((SELECT KitChildItems FROM @TempKitConfiguration t3 WHERE t3.numOppItemID=t1.numOppItemID AND t3.numItemCode=t1.numItemCode),',') 
					) as t2
				) TempChildItems
				INNER JOIN
					OpportunityKitItems OKI
				ON
					OKI.numOppId=@numOppId
					AND OKI.numOppItemID=TempChildItems.numOppItemID
					AND OKI.numChildItemID=TempChildItems.numKitItemID
				INNER JOIN
					OpportunityItems OI
				ON
					OI.numItemCode = TempChildItems.numItemCode
					AND OI.numoppitemtcode = OKI.numOppItemID
				LEFT JOIN
					WarehouseItems WI
				ON
					OI.numWarehouseItmsID = WI.numWarehouseItemID
				LEFT JOIN
					Warehouses W
				ON
					WI.numWarehouseID = W.numWarehouseID
				INNER JOIN
					ItemDetails
				ON
					TempChildItems.numKitItemID = ItemDetails.numItemKitID
					AND TempChildItems.numKitChildItemID = ItemDetails.numChildItemID
				INNER JOIN
					Item 
				ON
					ItemDetails.numChildItemID = Item.numItemCode
			END

			-- UPDATE UOM OF OPPORTUNITY ITEMS HAVE NOT UOM SET WHILE EXPORTED FROM MARKETPLACE (MEANS TINTSOURCETYPE = 3)
			IF ISNULL(@tintSourceType,0) = 3
			BEGIN
				UPDATE 
					OI 
				SET 
					numUOMID = ISNULL(I.numSaleUnit,0), 
					numUnitHour = dbo.fn_UOMConversion(ISNULL(I.numSaleUnit,0),OI.numItemCode,@numDomainId,null) * OI.numUnitHour
				FROM 
					OpportunityItems OI
				JOIN 
					Item I 
				ON 
					OI.numItemCode = I.numItemCode
				WHERE 
					numOppId = @numOppID
					AND I.numDomainID = @numDomainId
			END	
                                     
			INSERT INTO OppWarehouseSerializedItem
			(
				numOppID,
				numWarehouseItmsDTLID,
				numOppItemID,
				numWarehouseItmsID
			)                      
			SELECT 
				@numOppID,
				X.numWarehouseItmsDTLID,
				X.numoppitemtCode,
				X.numWItmsID 
			FROM
			(                                                                          
				SELECT
					* 
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/SerialNo',2)                                                                          
				WITH                        
				(                                               
				numWarehouseItmsDTLID numeric(9),                      
				numoppitemtCode numeric(9),                      
				numWItmsID numeric(9)                                                     
				)
			)X                                    
                                   
			UPDATE 
				OppWarehouseSerializedItem                       
			SET 
				numOppItemID=X.numoppitemtCode                                
			FROM                       
			(
				SELECT 
					numWarehouseItmsDTLID AS DTLID,
					O.numoppitemtCode 
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/SerialNo',2)                                                                         
				WITH
				(
					numWarehouseItmsDTLID NUMERIC(9),numoppitemtCode NUMERIC(9),numWItmsID NUMERIC(9)
				)                                
				JOIN 
					OpportunityItems O 
				ON 
					O.numWarehouseItmsID=numWItmsID and O.numOppID=@numOppID
			)X                 
			WHERE 
				numOppID=@numOppID 
				AND numWarehouseItmsDTLID=X.DTLID      
    
			IF NOT (@tintCommitAllocation=2 AND @bitAllocateInventoryOnPickList=1)
			BEGIN
				EXEC USP_ManageSOWorkOrder @numDomainId,@numOppID,@strItems,@numUserCntID
			END
                     
			EXEC sp_xml_removedocument @hDocItem                                     
		END          
	END
	ELSE                                                                                                                          
	BEGIN

		DECLARE @tempAssignedTo AS NUMERIC(9) = NULL                                                    
		SELECT 
			@tintOppStatus = tintOppStatus,
			@tempAssignedTo=ISNULL(numAssignedTo,0) 
		FROM   
			OpportunityMaster 
		WHERE  
			numOppID = @numOppID

		IF @tempAssignedTo <> @numAssignedTo
		BEGIN
			IF (SELECT COUNT(*) FROM BizDocComission WHERE ISNULL(numOppID,0) = @numOppID AND ISNULL(bitCommisionPaid,0) = 1) > 0
			BEGIN
				RAISERROR('COMMISSION_PAID_AGAINST_BIZDOC',16,1)
			END
		END
 
		--REVERTING BACK THE WAREHOUSE ITEMS                  
		IF @tintOppStatus = 1 AND @tintCommitAllocation=1
		BEGIN               
			EXEC USP_RevertDetailsOpp @numOppID,0,@numUserCntID                  
		END                  

		IF LEN(ISNULL(@vcOppRefOrderNo,''))>0
		BEGIN
			UPDATE dbo.OpportunityBizDocs SET vcRefOrderNo=@vcOppRefOrderNo WHERE numOppId=@numOppID
		END
		
		IF EXISTS (SELECT 1 FROM dbo.OpportunityMaster WHERE numOppId = @numOppID AND tintOppType=1 AND isnull(tintOppStatus,0)=1 AND ISNULL(numStatus,0)!= @numStatus)
		BEGIN
			EXEC USP_ManageOpportunityAutomationQueue
					@numOppQueueID = 0, -- numeric(18, 0)
					@numDomainID = @numDomainID, -- numeric(18, 0)
					@numOppId = @numOppID, -- numeric(18, 0)
					@numOppBizDocsId = 0, -- numeric(18, 0)
					@numOrderStatus = @numStatus, -- numeric(18, 0)
					@numUserCntID = @numUserCntID, -- numeric(18, 0)
					@tintProcessStatus = 1, -- tinyint
					@tintMode = 1 -- TINYINT
		END 
						
		UPDATE   
			OpportunityMaster
		SET      
			vcPOppName = @vcPOppName,txtComments = ISNULL(@Comments,''),bitPublicFlag = @bitPublicFlag,numCampainID = @CampaignID,tintSource = @tintSource,tintSourceType=@tintSourceType,
			intPEstimatedCloseDate = @dtEstimatedCloseDate,numModifiedBy = @numUserCntID,bintModifiedDate = GETUTCDATE(),lngPConclAnalysis = @lngPConclAnalysis,monPAmount = @monPAmount,
			tintActive = @tintActive,numSalesOrPurType = @numSalesOrPurType,numStatus = @numStatus,vcOppRefOrderNo=@vcOppRefOrderNo,
			bintOppToOrder=(CASE WHEN @tintOppStatus=0 AND @DealStatus=1 THEN GETUTCDATE() ELSE bintOppToOrder END),bitDiscountType=@bitDiscountType,fltDiscount=@fltDiscount,
			bitBillingTerms=@bitBillingTerms,intBillingDays=@intBillingDays,bitInterestType=@bitInterestType,fltInterest=@fltInterest,tintTaxOperator=@tintTaxOperator,
			numDiscountAcntType=@numDiscountAcntType,numPercentageComplete =@numPercentageComplete,bitUseShippersAccountNo = @bitUseShippersAccountNo,
			intUsedShippingCompany = @intUsedShippingCompany, numContactID=@numContactId,numShipmentMethod=@numShipmentMethod,numShippingService=@numShippingService,numPartner=@numPartner,numPartenerContact=@numPartenerContactId
		WHERE
			numOppId = @numOppID   
	   
		-- UPDATING IF ORGANIZATION IS ASSIGNED TO SOMEONE                                                      
		IF ( @tempAssignedTo <> @numAssignedTo AND @numAssignedTo <> '0' ) 
		BEGIN
			UPDATE
				OpportunityMaster 
			SET	
				numAssignedTo = @numAssignedTo, 
				numAssignedBy = @numUserCntID 
			WHERE 
				numOppId = @numOppID                                                    
		END                                                     
		ELSE IF(@numAssignedTo = 0) 
		BEGIN
			UPDATE 
				OpportunityMaster 
			SET 
				numAssignedTo = 0, 
				numAssignedBy = 0 
			WHERE
				numOppId = @numOppID
		END  
			                                                                    
		-- UPDATING OPP ITEMS
		IF CONVERT(VARCHAR(10),@strItems) <> '' AND @numOppID > 0                                                                         
		BEGIN
			EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems                                      
			   
			IF EXISTS 
			(
				SELECT 
					OI.numoppitemtCode
				FROM 
					OpportunityItems OI
				WHERE 
					numOppID=@numOppID 
					AND OI.numoppitemtCode NOT IN (SELECT numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) WITH (numoppitemtCode NUMERIC(9)))
					AND ISNULL((SELECT COUNT(*) FROM OppWarehouseSerializedItem WHERE numOppId=@numOppID AND numOppItemID=OI.numoppitemtCode),0) > 0
			)
			BEGIN
				RAISERROR('SERIAL/LOT#_ASSIGNED_TO_ITEM_DELETED',16,1)
			END

			--DELETE CHILD ITEMS OF KIT WITHIN KIT
			DELETE FROM 
				OpportunityKitChildItems 
			WHERE 
				numOppID=@numOppID AND 
				numOppItemID NOT IN (SELECT ISNULL(numoppitemtCode,0) FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) WITH (numoppitemtCode NUMERIC(9)))   

			--DELETE ITEMS
			DELETE FROM 
				OpportunityKitItems 
			WHERE 
				numOppID=@numOppID AND 
				numOppItemID NOT IN (SELECT ISNULL(numoppitemtCode,0) FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) WITH (numoppitemtCode NUMERIC(9)))                      
			                            
			DELETE FROM 
				OpportunityItems 
			WHERE 
				numOppID=@numOppID 
				AND numoppitemtCode NOT IN (SELECT numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) WITH (numoppitemtCode NUMERIC(9)))   
	          
			-- DELETE CORRESPONSING TIME AND EXPENSE ENTRIES OF DELETED ITEMS
			DELETE FROM
				TimeAndExpense
			WHERE
				numDomainID=@numDomainId
				AND numOppId=@numOppID
				AND numOppItemID NOT IN (SELECT numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) WITH (numoppitemtCode NUMERIC(9))) 

			--INSERT NEW ADDED ITEMS                        
			INSERT INTO OpportunityItems
			(
				numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,
				[vcItemName],[vcModelID],[vcManufacturer],vcPathForTImage,monVendorCost,numUOMId,bitWorkOrder,numVendorWareHouse,numToWarehouseItemID,numShipmentMethod,
				numSOVendorId,numProjectID,numProjectStageID,vcAttributes,numClassID,monAvgCost,bitItemPriceApprovalRequired,vcAttrValues,numPromotionID,bitPromotionTriggered
				,vcPromotionDetail,numSortOrder,numCost,vcNotes,vcInstruction,bintCompliationDate,numWOAssignedTo,bitMarkupDiscount,ItemReleaseDate,vcChildKitSelectedItems
			)
			SELECT 
				@numOppID,X.numItemCode,dbo.fn_UOMConversion(X.numUOM,numItemCode,@numDomainId,null) * x.numUnitHour,x.monPrice,x.monTotAmount,X.vcItemDesc,X.numWarehouseItmsID,
				X.ItemType,X.DropShip,X.bitDiscountType,X.fltDiscount,X.monTotAmtBefDiscount,X.vcItemName,(SELECT vcModelID FROM item WHERE numItemCode = X.numItemCode),
				(SELECT vcManufacturer FROM item WHERE numItemCode = X.numItemCode),
				(SELECT TOP 1 vcPathForTImage FROM dbo.ItemImages WHERE numItemCode = X.numItemCode AND BitDefault = 1 AND numDomainId = @numDomainId),
				(select isnull(VN.monCost,0) from Vendor VN INNER JOIN Item IT ON IT.numItemCode=VN.numItemCode and VN.numVendorID=IT.numVendorID where VN.numItemCode=X.numItemCode),
				X.numUOM,X.bitWorkOrder,X.numVendorWareHouse,X.numToWarehouseItemID,X.numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,Attributes,
				0,
				(SELECT (CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(I.monAverageCost,0) END) FROM Item I WHERE I.numDomainId=@numDomainId AND I.numItemCode=X.numItemCode), X.bitItemPriceApprovalRequired, X.AttributeIDs,
				X.numPromotionID,X.bitPromotionTriggered,X.vcPromotionDetail,X.numSortOrder,ISNULL(X.numCost,0),ISNULL(vcVendorNotes,'')
				,X.vcInstruction,X.bintCompliationDate,X.numWOAssignedTo,X.bitMarkupDiscount,@dtItemRelease,ISNULL(KitChildItems,'')
			FROM
			(
				SELECT 
					*
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
				WITH  
				(                      
					numoppitemtCode numeric(9),numItemCode numeric(9),numUnitHour FLOAT,monPrice DECIMAL(30,16),monTotAmount DECIMAL(20,5),vcItemDesc varchar(1000),                                    
					numWarehouseItmsID numeric(9),numToWarehouseItemID numeric(9),Op_Flag tinyint,ItemType varchar(30),DropShip bit,bitDiscountType bit,
					fltDiscount DECIMAL(30,16),monTotAmtBefDiscount DECIMAL(20,5),vcItemName VARCHAR(300),numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),
					numShipmentMethod NUMERIC(9),numSOVendorId numeric(9),numProjectID numeric(9),numProjectStageID numeric(9),Attributes varchar(500), 
					bitItemPriceApprovalRequired BIT,AttributeIDs VARCHAR(500),numPromotionID NUMERIC(18,0),bitPromotionTriggered BIT,vcPromotionDetail VARCHAR(2000)
					,numSortOrder NUMERIC(18,0),numCost DECIMAL(20,5),vcVendorNotes VARCHAR(1000),vcInstruction VARCHAR(1000),bintCompliationDate DATETIME,numWOAssignedTo NUMERIC(18,0)
					,bitMarkupDiscount BIT,KitChildItems VARCHAR(MAX)
				)
			)X 
			
			-- UPDATE EXISTING CUSTOMER PART NO
			UPDATE
				CPN
			SET
				CPN.CustomerPartNo = ISNULL(TEMPCPN.CustomerPartNo,'')
			FROM
				CustomerPartNumber CPN
			INNER JOIN
			(
				SELECT 
					numItemCode
					,MIN(CustomerPartNo) AS CustomerPartNo
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item',2)                                                                          
				WITH                       
				(                                                                          
					CustomerPartNo VARCHAR(300), numItemCode NUMERIC(18,0)
				)
				GROUP BY
					numItemCode
			) AS TEMPCPN
			ON
				CPN.numCompanyId = @numCompany
				AND CPN.numItemCode = TEMPCPN.numItemCode
			WHERE
				CPN.numDomainID=@numDomainID
				AND CPN.numCompanyId = @numCompany
				AND LEN(ISNULL(TEMPCPN.CustomerPartNo,'')) > 0
				AND ISNULL(TEMPCPN.CustomerPartNo,'') <> '0'
			
			-- INSERT NEW CUSTOMER PART NO
			INSERT INTO CustomerPartNumber
			(
				numCompanyId, numDomainID, CustomerPartNo, numItemCode 
			)
			SELECT 
				@numCompany, @numDomainID, TEMPCPN.CustomerPartNo,TEMPCPN.numItemCode
			FROM
			(
				SELECT 
					numItemCode
					,MIN(CustomerPartNo) AS CustomerPartNo
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item',2)                                                                          
				WITH                       
				(                                                                          
					CustomerPartNo VARCHAR(300), numItemCode NUMERIC(18,0)
				)
				GROUP BY
					numItemCode
			) TEMPCPN 
			LEFT JOIN
				CustomerPartNumber CPN
			ON
				CPN.numCompanyId = @numCompany
				AND CPN.numItemCode = TEMPCPN.numItemCode
			WHERE
				CPN.CustomerPartNoID IS NULL
				AND LEN(ISNULL(TEMPCPN.CustomerPartNo,'')) > 0
				AND ISNULL(TEMPCPN.CustomerPartNo,'') <> '0'
			
			-- UPDATE ITEMS
			UPDATE 
				OpportunityItems                       
			SET 
				numItemCode=X.numItemCode,numOppId=@numOppID,numUnitHour=dbo.fn_UOMConversion(X.numUOM,X.numItemCode,@numDomainId,null) * x.numUnitHour,                      
				monPrice=x.monPrice,monTotAmount=x.monTotAmount,vcItemDesc=X.vcItemDesc,vcModelID=X.vcModelID,numWarehouseItmsID=X.numWarehouseItmsID,numToWarehouseItemID = X.numToWarehouseItemID,             
				bitDropShip=X.DropShip,bitDiscountType=X.bitDiscountType,fltDiscount=X.fltDiscount,monTotAmtBefDiscount=X.monTotAmtBefDiscount,vcItemName=X.vcItemName,
				numUOMId=numUOM,numVendorWareHouse=X.numVendorWareHouse,numShipmentMethod=X.numShipmentMethod,numSOVendorId=X.numSOVendorId,numProjectId=X.numProjectID,
				numProjectStageId=X.numProjectStageID,vcAttributes=X.Attributes,bitItemPriceApprovalRequired=X.bitItemPriceApprovalRequired,bitWorkOrder=X.bitWorkOrder,vcAttrValues=AttributeIDs,
				numPromotionID=X.numPromotionID,bitPromotionTriggered=X.bitPromotionTriggered,vcPromotionDetail=X.vcPromotionDetail,numSortOrder=X.numSortOrder,numCost=X.numCost,vcNotes=vcVendorNotes
				,bitMarkupDiscount=X.bitMarkupDiscount,ItemRequiredDate=X.ItemRequiredDate
			FROM 
			(
				SELECT 
					numoppitemtCode as numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,vcModelID,numWarehouseItmsID,numToWarehouseItemID,DropShip,bitDiscountType,
					fltDiscount,monTotAmtBefDiscount,vcItemName,numUOM,numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,Attributes,
					bitItemPriceApprovalRequired,bitWorkOrder,AttributeIDs,numPromotionID,bitPromotionTriggered,vcPromotionDetail,numSortOrder,ISNULL(numCost,0) AS numCost,ISNULL(vcVendorNotes,'') vcVendorNotes,
					ItemRequiredDate,bitMarkupDiscount
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=2]',2)                                                                
				WITH  
				(                      
					numoppitemtCode NUMERIC(9),numItemCode NUMERIC(9),numUnitHour FLOAT,monPrice DECIMAL(30,16),monTotAmount DECIMAL(20,5),vcItemDesc VARCHAR(1000),vcModelID VARCHAR(300), numWarehouseItmsID NUMERIC(9), 
					numToWarehouseItemID NUMERIC(18,0),DropShip BIT,bitDiscountType BIT,fltDiscount DECIMAL(30,16),monTotAmtBefDiscount DECIMAL(20,5),vcItemName VARCHAR(300),numUOM NUMERIC(9),
					numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId NUMERIC(9),numProjectID NUMERIC(9),numProjectStageID NUMERIC(9),Attributes VARCHAR(500),
					bitItemPriceApprovalRequired BIT, bitWorkOrder BIT,AttributeIDs VARCHAR(500),numPromotionID NUMERIC(18,0),bitPromotionTriggered BIT,vcPromotionDetail VARCHAR(2000),numSortOrder NUMERIC(18,0),numCost DECIMAL(20,5),vcVendorNotes VARCHAR(1000),
					ItemRequiredDate DATETIME, bitMarkupDiscount BIT
				)
			)X 
			WHERE 
				numoppitemtCode=X.numOppItemID AND numOppId=@numOppID

			--INSERT KIT ITEM HAS ANOTHER KIT AS CHILD THEN SAVE CONFIGURATION OF KIT
			DELETE FROM @TempKitConfiguration

			INSERT INTO @TempKitConfiguration
			SELECT
				numoppitemtCode,
				numItemCode,
				vcChildKitSelectedItems
			FROM
				OpportunityItems OI
			WHERE
				numOppId = @numOppID
				AND LEN(ISNULL(vcChildKitSelectedItems,'')) > 0
				AND OI.numoppitemtCode NOT IN (SELECT ISNULL(numOppItemID,0) FROM OpportunityKitItems OKI WHERE OKI.numOppId=@numOppID)

			--INSERT NEW ADDED KIT ITEMS
			IF (SELECT 
					COUNT(*) 
				FROM 
					OpportunityItems OI 
				LEFT JOIN
					WarehouseItems WI
				ON
					OI.numWarehouseItmsID = WI.numWarehouseItemID
				LEFT JOIN
					Warehouses W
				ON
					WI.numWarehouseID = W.numWarehouseID
				JOIN 
					ItemDetails ID 
				ON 
					OI.numItemCode=ID.numItemKitID 
				JOIN
					Item I
				ON
					ID.numChildItemID = I.numItemCode
				WHERE 
					OI.numOppId=@numOppId  
					AND ISNULL(OI.bitDropShip,0) = 0
					AND ISNULL(OI.numWarehouseItmsID,0) > 0
					AND I.charItemType = 'P'
					AND OI.numoppitemtCode NOT IN (SELECT ISNULL(numOppItemID,0) FROM OpportunityKitItems OKI WHERE OKI.numOppId=@numOppID)
					AND 1 = (CASE 
								WHEN (SELECT 
											COUNT(*) 
										FROM 
											@TempKitConfiguration t1
										CROSS APPLY
										(
											SELECT
												*
											FROM 
												dbo.Split((SELECT KitChildItems FROM @TempKitConfiguration t3 WHERE t3.numOppItemID=OI.numoppitemtCode AND t3.numItemCode=OI.numItemCode),',') 
										) AS t2 
										WHERE 
											SUBSTRING(items,0,CHARINDEX('-',items)) = '0') > 0 
								THEN 
									(CASE 
										WHEN I.numItemCode IN (SELECT 
																	SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items))
																FROM 
																	@TempKitConfiguration t1
																CROSS APPLY
																(
																	SELECT
																		*
																	FROM 
																		dbo.Split((SELECT KitChildItems FROM @TempKitConfiguration t3 WHERE t3.numOppItemID=OI.numoppitemtCode AND t3.numItemCode=OI.numItemCode),',') 
																) AS t2 
																WHERE 
																	SUBSTRING(items,0,CHARINDEX('-',items)) = '0') 
										THEN 1 
										ELSE 0 
									END)
								ELSE 1 END)
					AND ISNULL((SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ID.numChildItemID AND numWareHouseID = W.numWarehouseID),0) = 0) > 0
			BEGIN
				RAISERROR('SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS',16,1)
			END
			             
			INSERT INTO OpportunityKitItems
			(
				numOppId,
				numOppItemID,
				numChildItemID,
				numWareHouseItemId,
				numQtyItemsReq,
				numQtyItemsReq_Orig,
				numUOMId,
				numQtyShipped,
				monAvgCost
			)
			SELECT
				@numOppId,
				OI.numoppitemtCode,
				ID.numChildItemID,
				(SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ID.numChildItemID AND WareHouseItems.numWareHouseID = W.numWarehouseID ORDER BY numWareHouseItemID),
				(ID.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(ID.numUOMId,ID.numChildItemID,@numDomainID,I.numBaseUnit),1)) * OI.numUnitHour,
				ID.numQtyItemsReq,
				ID.numUOMId,
				0,
				ISNULL(I.monAverageCost,0)
			FROM 
				OpportunityItems OI 
			LEFT JOIN
				WarehouseItems WI
			ON
				OI.numWarehouseItmsID = WI.numWarehouseItemID
			LEFT JOIN
				Warehouses W
			ON
				WI.numWarehouseID = W.numWarehouseID
			JOIN 
				ItemDetails ID 
			ON 
				OI.numItemCode=ID.numItemKitID 
			JOIN
				Item I
			ON
				ID.numChildItemID = I.numItemCode
			WHERE 
				OI.numOppId=@numOppId
				AND 1 = (CASE 
								WHEN (SELECT 
											COUNT(*) 
										FROM 
											@TempKitConfiguration t1
										CROSS APPLY
										(
											SELECT
												*
											FROM 
												dbo.Split((SELECT KitChildItems FROM @TempKitConfiguration t3 WHERE t3.numOppItemID=OI.numoppitemtCode AND t3.numItemCode=OI.numItemCode),',') 
										) AS t2 
										WHERE 
											SUBSTRING(items,0,CHARINDEX('-',items)) = '0') > 0 
								THEN 
									(CASE 
										WHEN I.numItemCode IN (SELECT 
																	SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items))
																FROM 
																	@TempKitConfiguration t1
																CROSS APPLY
																(
																	SELECT
																		*
																	FROM 
																		dbo.Split((SELECT KitChildItems FROM @TempKitConfiguration t3 WHERE t3.numOppItemID=OI.numoppitemtCode AND t3.numItemCode=OI.numItemCode),',') 
																) AS t2 
																WHERE 
																	SUBSTRING(items,0,CHARINDEX('-',items)) = '0') 
										THEN 1 
										ELSE 0 
									END)
								ELSE 1 END)
				AND OI.numoppitemtCode NOT IN (SELECT ISNULL(numOppItemID,0) FROM OpportunityKitItems OKI WHERE OKI.numOppId=@numOppID) 
		
			IF (SELECT COUNT(*) FROM OPENXML (@hDocItem,'/NewDataSet/Item[bitHasKitAsChild=''true'']',2)) > 0
			BEGIN
				IF (SELECT 
						COUNT(*) 
					FROM 
					(
						SELECT 
							t1.numOppItemID,
							t1.numItemCode,
							SUBSTRING(items,0,CHARINDEX('-',items)) AS numKitItemID,
							SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)) As numKitChildItemID
						FROM 
							@TempKitConfiguration t1
						CROSS APPLY
						(
							SELECT
								*
							FROM 
								dbo.Split((SELECT KitChildItems FROM @TempKitConfiguration t3 WHERE t3.numOppItemID=t1.numOppItemID AND t3.numItemCode=t1.numItemCode),',') 
						) as t2
					) TempChildItems
					INNER JOIN
						OpportunityKitItems OKI
					ON
						OKI.numOppId=@numOppId
						AND OKI.numOppItemID = TempChildItems.numOppItemID
						AND OKI.numChildItemID=TempChildItems.numKitItemID
					INNER JOIN
						OpportunityItems OI
					ON
						OI.numItemCode = TempChildItems.numItemCode
						AND OI.numoppitemtcode = OKI.numOppItemID
					LEFT JOIN
						WarehouseItems WI
					ON
						OI.numWarehouseItmsID = WI.numWarehouseItemID
					LEFT JOIN
						Warehouses W
					ON
						WI.numWarehouseID = W.numWarehouseID
					INNER JOIN
						ItemDetails
					ON
						TempChildItems.numKitItemID = ItemDetails.numItemKitID
						AND TempChildItems.numKitChildItemID = ItemDetails.numChildItemID
					INNER JOIN
						Item 
					ON
						ItemDetails.numChildItemID = Item.numItemCode
					WHERE
						Item.charItemType = 'P'
						AND ISNULL((SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ItemDetails.numChildItemID AND numWareHouseID = W.numWarehouseID),0) = 0) > 0
				BEGIN
					RAISERROR('SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS',16,1)
				END

				INSERT INTO OpportunityKitChildItems
				(
					numOppID,
					numOppItemID,
					numOppChildItemID,
					numItemID,
					numWareHouseItemId,
					numQtyItemsReq,
					numQtyItemsReq_Orig,
					numUOMId,
					numQtyShipped,
					monAvgCost
				)
				SELECT 
					@numOppID,
					OKI.numOppItemID,
					OKI.numOppChildItemID,
					ItemDetails.numChildItemID,
					(SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ItemDetails.numChildItemID AND WareHouseItems.numWareHouseID = W.numWarehouseID ORDER BY numWarehouseItemID),
					(ItemDetails.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(ItemDetails.numUOMId,ItemDetails.numChildItemID,@numDomainID,Item.numBaseUnit),1)) * OKI.numQtyItemsReq,
					ItemDetails.numQtyItemsReq,
					ItemDetails.numUOMId,
					0,
					ISNULL(Item.monAverageCost,0)
				FROM 
				(
					SELECT 
						t1.numOppItemID,
						t1.numItemCode,
						SUBSTRING(items,0,CHARINDEX('-',items)) AS numKitItemID,
						SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)) As numKitChildItemID
					FROM 
						@TempKitConfiguration t1
					CROSS APPLY
					(
						SELECT
							*
						FROM 
							dbo.Split((SELECT KitChildItems FROM @TempKitConfiguration t3 WHERE t3.numOppItemID=t1.numOppItemID AND t3.numItemCode=t1.numItemCode),',') 
					) as t2
				) TempChildItems
				INNER JOIN
					OpportunityKitItems OKI
				ON
					OKI.numOppId=@numOppId
					AND OKI.numOppItemID = TempChildItems.numOppItemID
					AND OKI.numChildItemID=TempChildItems.numKitItemID
				INNER JOIN
					OpportunityItems OI
				ON
					OI.numItemCode = TempChildItems.numItemCode
					AND OI.numoppitemtcode = OKI.numOppItemID
				LEFT JOIN
					WarehouseItems WI
				ON
					OI.numWarehouseItmsID = WI.numWarehouseItemID
				LEFT JOIN
					Warehouses W
				ON
					WI.numWarehouseID = W.numWarehouseID
				INNER JOIN
					ItemDetails
				ON
					TempChildItems.numKitItemID = ItemDetails.numItemKitID
					AND TempChildItems.numKitChildItemID = ItemDetails.numChildItemID
				INNER JOIN
					Item 
				ON
					ItemDetails.numChildItemID = Item.numItemCode
			END                                  
			
			--UPDATE KIT ITEMS                 
			UPDATE 
				OKI 
			SET 
				numQtyItemsReq = numQtyItemsReq_Orig * OI.numUnitHour
			FROM 
				OpportunityKitItems OKI 
			JOIN 
				OpportunityItems OI 
			ON 
				OKI.numOppItemID=OI.numoppitemtCode 
				AND OKI.numOppId=OI.numOppId  
			WHERE 
				OI.numOppId=@numOppId 

			-- UPDATE CHILD ITEMS OF KIT WITHIN KIT
			UPDATE
				OKCI
			SET
				OKCI.numQtyItemsReq = (OKI.numQtyItemsReq * OKCI.numQtyItemsReq_Orig)
			FROM
				OpportunityKitChildItems OKCI
			INNER JOIN
				OpportunityKitItems OKI
			ON
				OKCI.numOppID = @numOppId
				AND OKCI.numOppItemID = OKI.numOppItemID
				AND OKCI.numOppChildItemID = OKI.numOppChildItemID                     
			                                    
			IF NOT (@tintCommitAllocation=2 AND @bitAllocateInventoryOnPickList=1)
			BEGIN
				IF @tintCommitAllocation = 2
				BEGIN
					DELETE FROM WorkOrderDetails WHERE numWOId IN (SELECT numWOId FROM WorkOrder WHERE numOppId=@numOppID AND numWOStatus <> 23184)
					DELETE FROM WorkOrder WHERE numOppId=@numOppID AND numWOStatus <> 23184
				END

				EXEC USP_ManageSOWorkOrder @numDomainId,@numOppID,@strItems,@numUserCntID
			END
			   		                                                                      
			EXEC sp_xml_removedocument @hDocItem                                               
		END     

		IF @tintOppType=1 AND @tintOppStatus=1 --Sales Order
		BEGIN
			IF EXISTS (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID AND ISNULL(numWarehouseItmsID,0) > 0 AND ISNULL(bitDropShip,0) = 0 AND (ISNULL(numUnitHour,0) - ISNULL(numQtyShipped,0)) < 0)
			BEGIN
				RAISERROR('EDITED_ITEMS_QTY_IS_LESS_THEN_SHIPPED_QTY',16,1)
				RETURN
			END

			IF EXISTS (SELECT numoppitemtCode FROM OpportunityItems INNER JOIN WorkOrder ON OpportunityItems.numOppID=WorkOrder.numOppID AND OpportunityItems.numoppitemtCode=WorkOrder.numOppItemID AND ISNULL(WorkOrder.numParentWOID,0)=0 AND WorkOrder.numWOStatus=23184 AND ISNULL(OpportunityItems.numUnitHour,0) > ISNULL(WorkOrder.numQtyItemsReq,0) WHERE OpportunityItems.numOppID=@numOppID)
			BEGIN
				RAISERROR('EDITED_ITEMS_QTY_CAN_NOT_BE_GREATER_THEN_COMPLETED_WORK_ORDER',16,1)
				RETURN
			END
		END
		ELSE IF @tintOppType=2 AND @tintOppStatus=1 --Purchase Order
		BEGIN
			IF EXISTS (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID AND ISNULL(numWarehouseItmsID,0) > 0 AND ISNULL(bitDropShip,0) = 0 AND (ISNULL(numUnitHour,0) - ISNULL(numUnitHourReceived,0)) < 0)
			BEGIN
				RAISERROR('EDITED_ITEMS_QTY_IS_LESS_THEN_RECEIVED_QTY',16,1)
				RETURN
			END
		END

		--FOLLOWING DELETE STATEMENT IS COMMENTED BECUASE NOW WE ARE NOT ALLOWING USER TO EIDT SERIAL/LOT ITEM UNTILL USER REMOVED ASSIGNED SERIAL/LOT NUMBERS
	    --Delete previously added serial lot number added for opportunity
		--DELETE FROM OppWarehouseSerializedItem WHERE numOppID = @numOppID                                                         
	END

	IF @tintOppStatus = 1
	BEGIN
		IF (SELECT COUNT(*) FROM OpportunityItems WHERE numOppId=@numOppID AND ISNULL(numWarehouseItmsID,0) > 0 AND ISNULL(bitDropShip,0)=0) <>
			(SELECT COUNT(*) FROM OpportunityItems INNER JOIN WareHouseItems ON OpportunityItems.numWarehouseItmsID=WareHouseItems.numWareHouseItemID AND OpportunityItems.numItemCode=WareHouseItems.numItemID WHERE numOppId=@numOppID AND OpportunityItems.numWarehouseItmsID > 0 AND ISNULL(bitDropShip,0)=0)
		BEGIN
			RAISERROR('INVALID_ITEM_WAREHOUSES',16,1)
			RETURN
		END
	END

	IF(SELECT 
			COUNT(*) 
		FROM 
			OpportunityBizDocItems OBDI 
		INNER JOIN 
			OpportunityBizDocs OBD 
		ON 
			OBDI.numOppBizDocID=OBD.numOppBizDocsID 
		WHERE 
			OBD.numOppId=@numOppID AND OBDI.numOppItemID NOT IN (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID)) > 0
	BEGIN
		RAISERROR('ITEM_USED_IN_BIZDOC',16,1)
		RETURN
	END

	IF @tintOppType=1 AND @tintCommitAllocation=2 AND @bitAllocateInventoryOnPickList=1
	BEGIN
		IF(SELECT 
				COUNT(*)
			FROM
			(
				SELECT
					numOppItemID
					,SUM(numUnitHour) PackingSlipUnits
				FROM 
					OpportunityBizDocItems OBDI 
				INNER JOIN 
					OpportunityBizDocs OBD 
				ON 
					OBDI.numOppBizDocID=OBD.numOppBizDocsID 
				WHERE 
					OBD.numOppId=@numOppID
					AND OBD.numBizDocId=29397 --Picking Slip
				GROUP BY
					numOppItemID
			) AS TEMP
			INNER JOIN
				OpportunityItems OI
			ON
				TEMP.numOppItemID = OI.numoppitemtCode
			WHERE
				OI.numUnitHour < PackingSlipUnits
			) > 0
		BEGIN
			RAISERROR('ORDERED_QUANTITY_CAN_NOT_BE_LESS_THEN_PACKING_SLIP_QTY',16,1)
			RETURN
		END
	END

	SET @TotalAmount=0                                                           
	SELECT @TotalAmount = SUM(monTotAmount) FROM OpportunityItems WHERE numOppId=@numOppID                                                                          
  
	UPDATE OpportunityMaster SET monPamount=@TotalAmount WHERE numOppId=@numOppID

/*Added by chintan - While creating SO from PO it wasn't puting Item Qty to Allocation. this segment was being called from USP_OppSaveMilestone which No longer in use after changes anoop did. */     
--Updating the warehouse items              
            
			
			DECLARE @bitViolatePrice BIT
			DECLARE @intPendingApprovePending INT

			SET @bitIsInitalSalesOrder=(SELECT bitIsInitalSalesOrder FROM OpportunityMaster WHERE numOppId=@numOppID AND numDomainId=@numDomainID)
			--SET @bitViolatePrice=(SELECT bitMarginPriceViolated FROM Domain WHERE numDomainId=@numDomainID)
			IF EXISTS (SELECT numApprovalRuleID FROM ApprovalProcessItemsClassification WHERE numDomainId=@numDomainID AND ISNULL(bitMarginPriceViolated,0) = 1)
			BEGIN
				SET @bitViolatePrice = 1
			END
			
			IF(@bitViolatePrice=1 AND @bitIsInitalSalesOrder=1 AND @tintClickBtn = 1)
			BEGIN
				UPDATE OpportunityMaster SET tintOppStatus=@DealStatus WHERE numOppId=@numOppID
			END
			ELSE
			BEGIN
				UPDATE OpportunityMaster SET tintOppStatus = @DealStatus WHERE numOppId=@numOppID
				DECLARE @tintShipped AS TINYINT               
				SELECT @tintOppStatus=tintOppStatus,@tintOppType=tintOppType,@tintShipped=tintShipped from OpportunityMaster where numOppID=@numOppID               
				
				IF @tintOppStatus=1 AND @tintCommitAllocation=1             
				BEGIN         
					EXEC USP_UpdatingInventoryonCloseDeal @numOppID,@numUserCntID
				END  
				declare @tintCRMType as numeric(9)        
				declare @AccountClosingDate as datetime        

				select @numDivisionID=numDivisionID,@AccountClosingDate=bintAccountClosingDate from OpportunityMaster where numOppID=@numOppID         
				select @tintCRMType=tintCRMType from divisionmaster where numDivisionID =@numDivisionID 

				--When deal is won                                         
				IF @DealStatus = 1 
				BEGIN
					IF @AccountClosingDate IS NULL                   
					BEGIN
						UPDATE OpportunityMaster SET bintAccountClosingDate=GETUTCDATE() WHERE numOppID=@numOppID              
					END

					-- Promote Lead to Account when Sales/Purchase Order is created against it
					IF @tintCRMType=0 --Lead & Order
					BEGIN
						UPDATE 
							divisionmaster 
						SET 
							tintCRMType=2
							,bintLeadProm=getutcdate()
							,bintLeadPromBy=@numUserCntID
							,bintProsProm=getutcdate()
							,bintProsPromBy=@numUserCntID        
						WHERE 
							numDivisionID=@numDivisionID        
					END
					--Promote Prospect to Account
					ELSE IF @tintCRMType=1 
					BEGIN
						UPDATE 
							divisionmaster 
						SET 
							tintCRMType=2
							,bintProsProm=getutcdate()
							,bintProsPromBy=@numUserCntID        
						WHERE 
							numDivisionID=@numDivisionID        
					END
				END         
				--When Deal is Lost
				ELSE IF @DealStatus = 2
				BEGIN
					SELECT @AccountClosingDate=bintAccountClosingDate FROM OpportunityMaster WHERE numOppID=@numOppID         
					
					IF @AccountClosingDate IS NULL                   
					BEGIN
						UPDATE OpportunityMaster SET bintAccountClosingDate=GETUTCDATE() WHERE numOppID=@numOppID
					END
				END 
			END

			/*Added by chintan -  Biz usually automatically promotes the record from Lead/Prospect to Account When a Sales Opp/Order is made against it*/  
			-- Promote Lead to Prospect when Sales/Purchase Opp is created against it
			if @tintCRMType=0 AND @DealStatus = 0 --Lead & Open Opp
			begin        
				update divisionmaster set tintCRMType=1,bintLeadProm=getutcdate(),bintLeadPromBy=@numUserCntID,bintProsProm=getutcdate(),bintProsPromBy=@numUserCntID        
				where numDivisionID=@numDivisionID        
			end        

--Add/Update Address Details
DECLARE @vcStreet  varchar(100),@vcCity  varchar(50),@vcPostalCode  varchar(15),@vcCompanyName  varchar(100),@vcAddressName VARCHAR(50), @vcAltContact VARCHAR(200)      
DECLARE @numState  numeric(9),@numCountry  numeric(9), @numCompanyId  numeric(9), @numContact NUMERIC(18,0)
DECLARE @bitIsPrimary BIT, @bitAltContact BIT;
DECLARE @tintAddressOf TINYINT

select  @vcCompanyName=vcCompanyName,@numCompanyId=div.numCompanyID from CompanyInfo Com                            
join divisionMaster Div                            
on div.numCompanyID=com.numCompanyID                            
where div.numdivisionID=@numDivisionId

--Bill Address
IF @numBillAddressId>0
BEGIN
	SELECT 
		@vcStreet=ISNULL(vcStreet,'')
		,@vcCity=ISNULL(vcCity,'')
		,@vcPostalCode=ISNULL(vcPostalCode,'')
		,@numState=ISNULL(numState,0)
		,@numCountry=ISNULL(numCountry,0)
		,@bitIsPrimary=bitIsPrimary
		,@vcAddressName=vcAddressName
		,@tintAddressOf=ISNULL(tintAddressOf,0)
		,@numContact=numContact
		,@bitAltContact=bitAltContact
		,@vcAltContact=vcAltContact
    FROM 
		dbo.AddressDetails 
	WHERE 
		numDomainID = @numDomainID 
		AND numAddressID = @numBillAddressId

	IF @tintAddressOf = 2 AND @tintOppType = 2
	BEGIN
		SELECT
			@vcCompanyName = ISNULL(vcCompanyName,'')
			,@numCompanyId = ISNULL(CompanyInfo.numCompanyId,0)
		FROM
			AddressDetails
		INNER JOIN
			DivisionMaster 
		ON
			AddressDetails.numRecordID = DivisionMaster.numDivisionID
		INNER JOIN
			CompanyInfo
		ON
			DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
		WHERE 
			AddressDetails.numDomainID = @numDomainID 
			AND numAddressID = @numBillAddressId
	END

  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 0, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = @vcCompanyName, --  varchar(100)
	@numCompanyId = 0, --  numeric(9, 0)
	@vcAddressName = @vcAddressName,
	@bitCalledFromProcedure = 1,
	@numContact = @numContact,
	@bitAltContact = @bitAltContact,
	@vcAltContact = @vcAltContact
END
  
  
--Ship Address
IF @intUsedShippingCompany = 92 -- WILL CALL (PICK UP FROM Warehouse)
BEGIN
	IF EXISTS (SELECT numWareHouseID FROM Warehouses WHERE Warehouses.numDomainID = @numDomainID AND numWareHouseID = @numWillCallWarehouseID AND ISNULL(numAddressID,0) > 0)
	BEGIN
		SELECT  
			@vcStreet=ISNULL(vcStreet,'')
			,@vcCity=ISNULL(vcCity,'')
			,@vcPostalCode=ISNULL(AddressDetails.vcPostalCode,'')
			,@numState=ISNULL(numState,0)
			,@numCountry=ISNULL(numCountry,0)
			,@vcAddressName=vcAddressName
		FROM 
			dbo.Warehouses 
		INNER JOIN
			AddressDetails
		ON
			Warehouses.numAddressID = AddressDetails.numAddressID
		WHERE 
			Warehouses.numDomainID = @numDomainID 
			AND numWareHouseID = @numWillCallWarehouseID
	END
	ELSE
	BEGIN
	SELECT  
		@vcStreet=ISNULL(vcWStreet,'')
		,@vcCity=ISNULL(vcWCity,'')
		,@vcPostalCode=ISNULL(vcWPinCode,'')
		,@numState=ISNULL(numWState,0)
		,@numCountry=ISNULL(numWCountry,0)
		,@vcAddressName=''
	FROM 
		dbo.Warehouses 
	WHERE 
		numDomainID = @numDomainID 
		AND numWareHouseID = @numWillCallWarehouseID
	END

	UPDATE OpportunityMaster SET tintShipToType=2 WHERE numOppId=@numOppID
 
  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 1, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = '', --  varchar(100)
	@numCompanyId =0, --  numeric(9, 0)
	@vcAddressName = @vcAddressName,
	@bitCalledFromProcedure = 1,
	@numContact = 0,
	@bitAltContact = 0,
	@vcAltContact = ''
END
ELSE IF @numShipAddressId>0
BEGIN
	SELECT
		@vcStreet=ISNULL(vcStreet,'')
		,@vcCity=ISNULL(vcCity,'')
		,@vcPostalCode=ISNULL(vcPostalCode,'')
		,@numState=ISNULL(numState,0)
		,@numCountry=ISNULL(numCountry,0)
		,@bitIsPrimary=bitIsPrimary
		,@vcAddressName=vcAddressName
		,@tintAddressOf=ISNULL(tintAddressOf,0)
		,@numContact=numContact
		,@bitAltContact=bitAltContact
		,@vcAltContact=vcAltContact
    FROM  
		dbo.AddressDetails 
	WHERE 
		numDomainID = @numDomainID 
		AND numAddressID = @numShipAddressId
 

	IF @tintAddressOf = 2 AND @tintOppType = 2
	BEGIN
		SELECT
			@vcCompanyName = ISNULL(vcCompanyName,'')
			,@numCompanyId = ISNULL(CompanyInfo.numCompanyId,0)
		FROM
			AddressDetails
		INNER JOIN
			DivisionMaster 
		ON
			AddressDetails.numRecordID = DivisionMaster.numDivisionID
		INNER JOIN
			CompanyInfo
		ON
			DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
		WHERE 
			AddressDetails.numDomainID = @numDomainID 
			AND numAddressID = @numShipAddressId
	END


  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 1, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = @vcCompanyName, --  varchar(100)
	@numCompanyId =@numCompanyId, --  numeric(9, 0)
	@vcAddressName = @vcAddressName,
	@bitCalledFromProcedure = 1,
	@numContact = @numContact,
	@bitAltContact = @bitAltContact,
	@vcAltContact = @vcAltContact
END

-- IMPORTANT:KEEP THIS BELOW ADDRESS UPDATE LOGIC
-- GET TAX APPLICABLE TO DIVISION WHEN ORDER IS CREATED AFTER THAT NOT NEED TO CHANGE IT
IF @bitNewOrder = 1
BEGIN
	--INSERT TAX FOR DIVISION   
	IF @tintOppType=1 OR (@tintOppType=2 and (select isnull(bitPurchaseTaxCredit,0) from Domain where numDomainId=@numDomainId)=1)
	BEGIN
		INSERT INTO dbo.OpportunityMasterTaxItems 
		(
			numOppId,
			numTaxItemID,
			fltPercentage,
			tintTaxType,
			numTaxID
		) 
		SELECT 
			@numOppID,
			TI.numTaxItemID,
			TEMPTax.decTaxValue,
			TEMPTax.tintTaxType,
			0
		FROM 
			TaxItems TI 
		JOIN 
			DivisionTaxTypes DTT 
		ON 
			TI.numTaxItemID = DTT.numTaxItemID
		CROSS APPLY
		(
			SELECT
				decTaxValue,
				tintTaxType
			FROM
				dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,TI.numTaxItemID,@numOppId,0,NULL)
		) AS TEMPTax
		WHERE 
			DTT.numDivisionID=@numDivisionId 
			AND DTT.bitApplicable=1
		UNION 
		SELECT 
			@numOppID,
			0,
			TEMPTax.decTaxValue,
			TEMPTax.tintTaxType,
			0
		FROM 
			dbo.DivisionMaster 
		CROSS APPLY
		(
			SELECT
				decTaxValue,
				tintTaxType
			FROM
				dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,0,@numOppId,1,NULL)
		) AS TEMPTax
		WHERE 
			bitNoTax=0 
			AND numDivisionID=@numDivisionID	
		UNION 
		SELECT
			@numOppId
			,1
			,decTaxValue
			,tintTaxType
			,numTaxID
		FROM
			dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,1,@numOppId,1,NULL)		
	END	
END

--INSERT TAX FOR DIVISION   
IF @tintOppType=1 OR (@tintOppType=2 and (select isnull(bitPurchaseTaxCredit,0) from Domain where numDomainId=@numDomainId)=1)
BEGIN
--Delete Tax for Opportunity Items if item deleted 
DELETE FROM OpportunityItemsTaxItems WHERE numOppId=@numOppId AND numOppItemID NOT IN (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID)

--Insert Tax for Opportunity Items
INSERT INTO dbo.OpportunityItemsTaxItems 
(
	numOppId,
	numOppItemID,
	numTaxItemID,
	numTaxID
) 
SELECT 
	@numOppId,
	OI.numoppitemtCode,
	TI.numTaxItemID,
	0
FROM 
	dbo.OpportunityItems OI 
JOIN 
	dbo.ItemTax IT 
ON 
	OI.numItemCode=IT.numItemCode 
JOIN
	TaxItems TI 
ON 
	TI.numTaxItemID = IT.numTaxItemID 
WHERE 
	OI.numOppId=@numOppID 
	AND IT.bitApplicable=1 
	AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
UNION
SELECT 
	@numOppId,
	OI.numoppitemtCode,
	0,
	0
FROM 
	dbo.OpportunityItems OI 
JOIN 
	dbo.Item I 
ON 
	OI.numItemCode=I.numItemCode
WHERE 
	OI.numOppId=@numOppID 
	AND I.bitTaxable=1 
	AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
UNION
SELECT
	@numOppId,
	OI.numoppitemtCode,
	1,
	TD.numTaxID
FROM
	dbo.OpportunityItems OI 
INNER JOIN
	ItemTax IT
ON
	IT.numItemCode = OI.numItemCode
INNER JOIN
	TaxDetails TD
ON
	TD.numTaxID = IT.numTaxID
	AND TD.numDomainId = @numDomainId
WHERE
	OI.numOppId = @numOppID
	AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)

END
  
 UPDATE OpportunityMaster SET monDealAmount=dbo.GetDealAmount(@numOppID,getdate(),0) WHERE numOppId=@numOppID

	IF @tintOppType=1 AND @tintOppStatus=1 
	BEGIN
		EXEC USP_SalesFulfillmentQueue_Save @numDomainID,@numUserCntID,@numOppID,@numStatus
		EXEC USP_OpportunityMaster_CreateBackOrderPO @numDomainID,@numUserCntID,@numOppID
	END
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SET @numOppID = 0

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS (SELECT * FROM sysobjects WHERE xtype = 'p' AND NAME = 'USP_PromotionOffer_ApplyItemPromotionToECommerce') 
DROP PROCEDURE USP_PromotionOffer_ApplyItemPromotionToECommerce
GO
CREATE  PROCEDURE [dbo].[USP_PromotionOffer_ApplyItemPromotionToECommerce]
(
	@numDomainID NUMERIC(18,0),
	@numUserCntId NUMERIC(18,0),
	@numSiteID NUMERIC(18,0),
	@vcCookieId VARCHAR(100),
	@bitBasedOnDiscountCode BIT,
	@vcDiscountCode VARCHAR(100)	
)
AS
BEGIN
	DECLARE @numDivisionID NUMERIC(18,0)
	DECLARE @numRelationship NUMERIC(18,0)
	DECLARE @numProfile NUMERIC(18,0)

	SET @numDivisionID = ISNULL((SELECT numDivisionId FROM AdditionalContactsInformation WHERE numContactId=@numUserCntId),0)

	IF @numDivisionID > 0
	BEGIN
		SELECT 
			@numRelationship=numCompanyType,
			@numProfile=vcProfile
		FROM 
			DivisionMaster D                  
		JOIN 
			CompanyInfo C 
		ON 
			C.numCompanyId=D.numCompanyID                  
		WHERE 
			numDivisionID =@numDivisionID
	END
	ELSE IF ISNULL(@numSiteID,0) > 0
	BEGIN
		SELECT
			@numRelationship=ISNULL(numRelationshipId,0),
			@numProfile=ISNULL(numProfileId,0)
		FROM
			eCommerceDTL
		WHERE
			numSiteId=@numSiteID
	END

	DECLARE @TEMP TABLE
	(
		ID INT IDENTITY(1,1)
		,numCartID NUMERIC(18,0)
        ,numItemCode NUMERIC(18,0)
		,numItemClassification NUMERIC(18,0)
        ,numUnits FLOAT
		,numWarehouseItemID NUMERIC(18,0)
        ,monUnitPrice DECIMAL(20,5)
        ,fltDiscount DECIMAL(20,5)
        ,bitDiscountType BIT
		,monInsertUnitPrice DECIMAL(20,5)
        ,fltInsertDiscount DECIMAL(20,5)
        ,bitInsertDiscountType BIT
        ,monTotalAmount DECIMAL(20,5)
        ,numPromotionID NUMERIC(18,0)
        ,bitPromotionTriggered BIT
        ,vcPromotionDescription VARCHAR(MAX)
		,vcKitChildItems VARCHAR(MAX)
		,numSelectedPromotionID NUMERIC(18,0)
		,bitChanged BIT		
	)

	INSERT INTO @TEMP
	(
		numCartID
        ,numItemCode
		,numItemClassification
        ,numUnits
		,numWarehouseItemID
        ,monUnitPrice
        ,fltDiscount
        ,bitDiscountType
		,monInsertUnitPrice
        ,fltInsertDiscount
        ,bitInsertDiscountType
        ,monTotalAmount
        ,numPromotionID
        ,bitPromotionTriggered
		,vcKitChildItems
		,numSelectedPromotionID
		,bitChanged
	)
	SELECT
		numCartID
        ,CI.numItemCode
		,ISNULL(I.numItemClassification,0)
        ,numUnitHour * ISNULL(decUOMConversionFactor,1)
		,numWarehouseItmsID
        ,monPrice
        ,fltDiscount
        ,bitDiscountType
		,monInsertPrice
		,fltInsertDiscount
		,bitInsertDiscountType
        ,monTotAmount
        ,PromotionID
        ,bitPromotionTrigerred
		,vcChildKitItemSelection
		,numPreferredPromotionID
		,0
	FROM
		CartItems CI
	INNER JOIN
		Item I
	ON
		CI.numItemCode = I.numItemCode
	WHERE
		CI.numDomainId=@numDomainId
		AND vcCookieId=@vcCookieId  
		AND numUserCntId=@numUserCntId 

	DECLARE @i INT = 1
	DECLARE @iCount INT 
	SET @iCount = (SELECT COUNT(*) FROM @TEMP)

	DECLARE @j INT = 1
	DECLARE @jCount INT

	DECLARE @numTempPromotionID NUMERIC(18,0)
	DECLARE @vcShortDesc VARCHAR(500)
	DECLARE @tintItemCalDiscount TINYINT
	DECLARE @numItemCode NUMERIC(18,0)
	DECLARE @numTempWarehouseItemID NUMERIC(18,0)
	DECLARE @numUnits FLOAT
	DECLARE @monTotalAmount DECIMAL(20,5)
	DECLARE @numItemClassification AS NUMERIC(18,0)

	DECLARE @bitRemainingCheckRquired AS BIT
	DECLARE @numRemainingPromotion FLOAT
	DECLARE @fltDiscountValue FLOAT
	DECLARE @tintDiscountType TINYINT
	DECLARE @tintDiscoutBaseOn TINYINT

	DECLARE @TEMPPromotion TABLE
	(
		ID INT IDENTITY(1,1)
		,numPromotionID NUMERIC(18,0)
		,vcShortDesc VARCHAR(500)
		,numTriggerItemCode NUMERIC(18,0)
		,numTriggerItemClassification NUMERIC(18,0)
		,numWarehouseItemID NUMERIC(18,0)
		,numUnits FLOAT
		,monTotalAmount DECIMAL(20,5)
	)

	IF ISNULL(@bitBasedOnDiscountCode,0) = 1
	BEGIN
		WHILE @i <= @iCount
		BEGIN
			-- IF USER SELECTED PROMOTION HE/SHE WANT TO USE THEN FIRST WE HAVE TO VALIDAE IT AND MARK IT AS TRIGGER
			UPDATE
				T1
			SET
				bitChanged = 1
				,numPromotionID=PO.numProId
				,bitPromotionTriggered=1
				,vcPromotionDescription=ISNULL(PO.vcShortDesc,'')
			FROM
				@TEMP T1
			INNER JOIN
				Item I
			ON
				T1.numItemCode = I.numItemCode
			INNER JOIN
				PromotionOffer PO 
			ON
				T1.numSelectedPromotionID = PO.numProId
			INNER JOIN
				PromotionOffer POOrder
			ON
				PO.numOrderPromotionID = POOrder.numProId
			INNER JOIN 
				DiscountCodes DC
			ON 
				POOrder.numProId = DC.numPromotionID
			WHERE
				T1.numSelectedPromotionID > 0
				AND ISNULL(T1.numPromotionID,0)  = 0
				AND T1.ID = @i
				AND PO.numDomainId=@numDomainID 
				AND ISNULL(PO.bitEnabled,0)=1 
				AND ISNULL(PO.IsOrderBasedPromotion,0) = 0
				AND ISNULL(POOrder.IsOrderBasedPromotion,0) = 1
				AND 1 = (CASE WHEN POOrder.bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN GETUTCDATE()>=POOrder.dtValidFrom AND GETUTCDATE()<=POOrder.dtValidTo THEN 1 ELSE 0 END) END)
				AND 1 = (CASE WHEN ISNULL(DC.CodeUsageLimit,0) <> 0 THEN (CASE WHEN ISNULL((SELECT intCodeUsed FROM DiscountCodeUsage WHERE numDiscountId=DC.numDiscountId AND numDivisionId=@numDivisionId),0) >= DC.CodeUsageLimit THEN 0 ELSE 1 END) ELSE 1 END)
				AND 1 = (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=POOrder.numProId AND tintType=2 AND numRelationship=@numRelationship AND numProfile=@numProfile) > 0 THEN 1 ELSE 0 END)
				AND DC.vcDiscountCode = @vcDiscountCode
				AND 1 = (CASE 
							WHEN ISNULL(PO.tintOfferTriggerValueType,1) = 1 --Based on quantity
							THEN
								CASE
									WHEN ISNULL(PO.tintOfferTriggerValueTypeRange,1) = 2
									THEN
										(CASE WHEN ISNULL(numUnits,0) BETWEEN ISNULL(PO.fltOfferTriggerValue,0) AND ISNULL(PO.fltOfferTriggerValueRange,0) THEN 1 ELSE 0 END)
									ELSE
										(CASE WHEN ISNULL(numUnits,0) >= ISNULL(PO.fltOfferTriggerValue,0) THEN 1 ELSE 0 END)
								END
							WHEN ISNULL(PO.tintOfferTriggerValueType,1) = 2 --Based on amount
							THEN
								CASE
									WHEN ISNULL(PO.tintOfferTriggerValueTypeRange,1) = 2
									THEN
										(CASE WHEN ISNULL(monTotalAmount,0) BETWEEN ISNULL(PO.fltOfferTriggerValue,0) AND ISNULL(PO.fltOfferTriggerValueRange,0) THEN 1 ELSE 0 END)
									ELSE
										(CASE WHEN ISNULL(monTotalAmount,0) >= ISNULL(PO.fltOfferTriggerValue,0) THEN 1 ELSE 0 END)
								END
						END)
				AND 1 = (CASE 
						WHEN PO.tintOfferBasedOn = 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 AND numValue=T1.numItemCode) > 0 THEN 1 ELSE 0 END)
						WHEN PO.tintOfferBasedOn = 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=2 AND numValue=ISNULL(I.numItemClassification,0)) > 0 THEN 1 ELSE 0 END)
						WHEN PO.tintOfferBasedOn = 4 THEN 1 
						ELSE 0
					END)
				AND ISNULL((SELECT COUNT(*) FROM @TEMP T3 WHERE T3.numPromotionID=PO.numProId AND T3.bitPromotionTriggered=1),0) = 0

			UPDATE
				T1
			SET
				bitChanged = 1
				,numPromotionID=T2.numProId
				,bitPromotionTriggered=1
				,vcPromotionDescription=vcShortDesc
			FROM
				@TEMP T1
			INNER JOIN
				Item I
			ON
				T1.numItemCode=I.numItemCode
			CROSS APPLY
			(
				SELECT TOP 1
					PO.numProId
					,PO.tintOfferBasedOn
					,PO.tintOfferTriggerValueType
					,PO.tintOfferTriggerValueTypeRange
					,PO.fltOfferTriggerValue
					,PO.fltOfferTriggerValueRange
					,ISNULL(PO.vcShortDesc,'') vcShortDesc
				FROM 
					PromotionOffer PO
				INNER JOIN
					PromotionOffer POOrder
				ON
					PO.numOrderPromotionID = POOrder.numProId
				INNER JOIN 
					DiscountCodes DC
				ON 
					POOrder.numProId = DC.numPromotionID
				WHERE
					PO.numDomainId=@numDomainID
					AND ISNULL(PO.bitEnabled,0)=1 
					AND ISNULL(PO.IsOrderBasedPromotion,0) = 0
					AND ISNULL(POOrder.IsOrderBasedPromotion,0) = 1
					AND 1 = (CASE WHEN POOrder.bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN GETUTCDATE()>=POOrder.dtValidFrom AND GETUTCDATE()<=POOrder.dtValidTo THEN 1 ELSE 0 END) END)
					AND 1 = (CASE WHEN ISNULL(DC.CodeUsageLimit,0) <> 0 THEN (CASE WHEN ISNULL((SELECT intCodeUsed FROM DiscountCodeUsage WHERE numDiscountId=DC.numDiscountId AND numDivisionId=@numDivisionId),0) >= DC.CodeUsageLimit THEN 0 ELSE 1 END) ELSE 1 END)
					AND 1 = (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=POOrder.numProId AND tintType=2 AND numRelationship=@numRelationship AND numProfile=@numProfile) > 0 THEN 1 ELSE 0 END)
					AND DC.vcDiscountCode = @vcDiscountCode
					AND 1 = (CASE 
							WHEN ISNULL(PO.tintOfferTriggerValueType,1) = 1 --Based on quantity
							THEN
								CASE
									WHEN ISNULL(PO.tintOfferTriggerValueTypeRange,1) = 2
									THEN
										(CASE WHEN ISNULL(T1.numUnits,0) BETWEEN ISNULL(PO.fltOfferTriggerValue,0) AND ISNULL(PO.fltOfferTriggerValueRange,0) THEN 1 ELSE 0 END)
									ELSE
										(CASE WHEN ISNULL(T1.numUnits,0) >= ISNULL(PO.fltOfferTriggerValue,0) THEN 1 ELSE 0 END)
								END
							WHEN ISNULL(PO.tintOfferTriggerValueType,1) = 2 --Based on amount
							THEN
								CASE
									WHEN ISNULL(PO.tintOfferTriggerValueTypeRange,1) = 2
									THEN
										(CASE WHEN ISNULL(T1.monTotalAmount,0) BETWEEN ISNULL(PO.fltOfferTriggerValue,0) AND ISNULL(PO.fltOfferTriggerValueRange,0) THEN 1 ELSE 0 END)
									ELSE
										(CASE WHEN ISNULL(T1.monTotalAmount,0) >= ISNULL(PO.fltOfferTriggerValue,0) THEN 1 ELSE 0 END)
								END
						END)
				ORDER BY
					(CASE 
						WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 1 THEN 1
						WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 1 THEN 2
						WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 2 THEN 3
						WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 2 THEN 4
						WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 3 THEN 5
						WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 3 THEN 6
					END) ASC,ISNULL(PO.fltOfferTriggerValue,0) DESC

			) T2
			WHERE
				ISNULL(numPromotionID,0)  = 0
				AND T1.ID = @i
				AND ISNULL((SELECT COUNT(*) FROM @TEMP T3 WHERE T3.numPromotionID=T2.numProId AND T3.bitPromotionTriggered=1),0) = 0
				AND 1 = (CASE 
							WHEN T2.tintOfferBasedOn = 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=T2.numProId AND tintRecordType=5 AND tintType=1 AND numValue=T1.numItemCode) > 0 THEN 1 ELSE 0 END)
							WHEN T2.tintOfferBasedOn = 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=T2.numProId AND tintRecordType=5 AND tintType=2 AND numValue=ISNULL(I.numItemClassification,0)) > 0 THEN 1 ELSE 0 END)
							WHEN T2.tintOfferBasedOn = 4 THEN 1 
							ELSE 0
						END)

			SET @i = @i + 1
		END
	END

	INSERT INTO @TEMPPromotion
	(
		numPromotionID
		,numTriggerItemCode
		,numTriggerItemClassification
		,numWarehouseItemID
		,numUnits
		,monTotalAmount
	)
	SELECT
		numPromotionID
		,numItemCode
		,numItemClassification
		,numWarehouseItemID
		,numUnits
		,monTotalAmount
	FROM
		@TEMP T1
	INNER JOIN
		PromotionOffer PO
	ON
		T1.numPromotionID = PO.numProId
	WHERE
		ISNULL(numPromotionID,0) > 0
		AND ISNULL(bitPromotionTriggered,0) = 1
		AND 1 = (CASE WHEN @bitBasedOnDiscountCode=0 THEN (CASE WHEN ISNULL(PO.numOrderPromotionID,0) = 0 THEN 1 ELSE 0 END) ELSE 1 END)

	SET @jCount = ISNULL((SELECT COUNT(*) FROM @TEMPPromotion),0)

	-- FIRST REMOVE ALL PROMOTIONS WHERE ITEM WHICH TRIGERRED PROMOTION IS DELETED
	UPDATE 
		T1
	SET 
		numPromotionID=0
		,bitPromotionTriggered=0
		,vcPromotionDescription=''
		,bitChanged=1
		,monUnitPrice=ISNULL(T1.monInsertUnitPrice,0)
		,fltDiscount=ISNULL(T1.fltInsertDiscount,0)
		,bitDiscountType=ISNULL(T1.bitInsertDiscountType,0)
	FROM
		@TEMP T1
	WHERE 
		numPromotionID > 0
		AND ISNULL(bitPromotionTriggered,0) = 0
		AND (SELECT COUNT(*) FROM @TEMP T2 WHERE T2.numPromotionID=T1.numPromotionID AND ISNULL(T2.bitPromotionTriggered,0) = 1) = 0

	-- FIRST REMOVE APPLIED PROMOTION IF THEY ARE NOT VALIES
	WHILE @j <= @jCount
	BEGIN
		SELECT 
			@numTempPromotionID=numPromotionID
			,@numItemCode=numTriggerItemCode
			,@numItemClassification=numTriggerItemClassification
			,@numTempWarehouseItemID=numWarehouseItemID
			,@numUnits = numUnits
			,@monTotalAmount=monTotalAmount
		FROM 
			@TEMPPromotion 
		WHERE 
			ID = @j

		IF NOT EXISTS (SELECT 
						PO.numProId 
					FROM 
						PromotionOffer PO
					LEFT JOIN	
						PromotionOffer POOrder
					ON
						PO.numOrderPromotionID = POOrder.numProId
					LEFT JOIN	
						DiscountCodes DC
					ON
						PO.numOrderPromotionID = DC.numPromotionID
					WHERE 
						PO.numDomainId=@numDomainID 
						AND PO.numProId=@numTempPromotionID
						AND ISNULL(PO.bitEnabled,0)=1 
						AND ISNULL(PO.IsOrderBasedPromotion,0) = 0
						AND 1 = (CASE 
									WHEN ISNULL(PO.numOrderPromotionID,0) > 0 
									THEN (CASE WHEN POOrder.bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN GETUTCDATE()>=POOrder.dtValidFrom AND GETUTCDATE()<=POOrder.dtValidTo THEN 1 ELSE 0 END) END)
									ELSE (CASE WHEN PO.bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN GETUTCDATE()>=PO.dtValidFrom AND GETUTCDATE()<=PO.dtValidTo THEN 1 ELSE 0 END) END)
								END)
						AND 1 = (CASE 
									WHEN ISNULL(PO.numOrderPromotionID,0) > 0 
									THEN
										(CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numOrderPromotionID AND tintType=2 AND numRelationship=@numRelationship AND numProfile=@numProfile) > 0 THEN 1 ELSE 0 END)
									ELSE
										(CASE PO.tintCustomersBasedOn 
											WHEN 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numProId AND tintType=1 AND numDivisionID=@numDivisionID) > 0 THEN 1 ELSE 0 END)
											WHEN 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numProId AND tintType=2 AND numRelationship=@numRelationship AND numProfile=@numProfile) > 0 THEN 1 ELSE 0 END)
											WHEN 3 THEN 1
											ELSE 0
										END)
								END)
						AND 1 = (CASE 
									WHEN PO.tintOfferBasedOn = 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 AND numValue=@numItemCode) > 0 THEN 1 ELSE 0 END)
									WHEN PO.tintOfferBasedOn = 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=2 AND numValue=@numItemClassification) > 0 THEN 1 ELSE 0 END)
									WHEN PO.tintOfferBasedOn = 4 THEN 1 
									ELSE 0
								END)
						AND 1 = (CASE 
									WHEN ISNULL(PO.tintOfferTriggerValueType,1) = 1 --Based on quantity
									THEN
										CASE
											WHEN ISNULL(PO.tintOfferTriggerValueTypeRange,1) = 2
											THEN
												(CASE WHEN ISNULL(@numUnits,0) BETWEEN ISNULL(PO.fltOfferTriggerValue,0) AND ISNULL(PO.fltOfferTriggerValueRange,0) THEN 1 ELSE 0 END)
											ELSE
												(CASE WHEN ISNULL(@numUnits,0) >= PO.fltOfferTriggerValue THEN 1 ELSE 0 END)
										END
									WHEN ISNULL(PO.tintOfferTriggerValueType,1) = 2 --Based on amount
									THEN
										CASE
											WHEN ISNULL(PO.tintOfferTriggerValueTypeRange,1) = 2
											THEN
												(CASE WHEN ISNULL(@monTotalAmount,0) BETWEEN ISNULL(PO.fltOfferTriggerValue,0) AND ISNULL(PO.fltOfferTriggerValueRange,0) THEN 1 ELSE 0 END)
											ELSE
												(CASE WHEN ISNULL(@monTotalAmount,0) >= PO.fltOfferTriggerValue THEN 1 ELSE 0 END)
										END
								END)
						AND 1 = (CASE 
									WHEN ISNULL(PO.numOrderPromotionID,0) > 0 
									THEN 
										(CASE WHEN ISNULL(DC.CodeUsageLimit,0) <> 0 THEN (CASE WHEN ISNULL((SELECT intCodeUsed FROM DiscountCodeUsage WHERE numDiscountId=DC.numDiscountId AND numDivisionId=@numDivisionId),0) >= DC.CodeUsageLimit THEN 0 ELSE 1 END) ELSE 1 END)
									ELSE 1 
								END)
				)
		BEGIN
			-- IF Promotion is not valid than revert line item price to based on default pricing
			UPDATE 
				T1
			SET 
				numPromotionID=0
				,bitPromotionTriggered=0
				,vcPromotionDescription=''
				,bitChanged=1
				,monUnitPrice=ISNULL(T1.monInsertUnitPrice,0)
				,fltDiscount=ISNULL(T1.fltInsertDiscount,0)
				,bitDiscountType=ISNULL(T1.bitInsertDiscountType,0)
			FROM
				@TEMP T1
			WHERE 
				numPromotionID=@numTempPromotionID
		END

		SET @j = @j + 1
	END

	SET @j = 1
	WHILE @j <= @jCount
	BEGIN
		SELECT 
			@numTempPromotionID=numPromotionID
			,@numItemCode=numTriggerItemCode
			,@numItemClassification=numTriggerItemClassification
			,@numTempWarehouseItemID=numWarehouseItemID
			,@numUnits = numUnits
			,@monTotalAmount=monTotalAmount
		FROM 
			@TEMPPromotion 
		WHERE 
			ID = @j

			
		SELECT
			@fltDiscountValue=ISNULL(fltDiscountValue,0)
			,@tintDiscountType=ISNULL(tintDiscountType,0)
			,@tintDiscoutBaseOn=tintDiscoutBaseOn
			,@vcShortDesc=ISNULL(vcShortDesc,'')
			,@tintItemCalDiscount=ISNULL(tintItemCalDiscount,1)
		FROM
			PromotionOffer
		WHERE
			numProId=@numTempPromotionID

		-- If discount is based on amount or quantity than we have to checked remaining amount or qty left to give as discount 
		IF @tintDiscountType = 2 OR @tintDiscountType = 3
		BEGIN
			SET @bitRemainingCheckRquired = 1
		END
		ELSE
		BEGIN
			SET @bitRemainingCheckRquired = 0
			SET @numRemainingPromotion = 0
		END

			-- If promotion is valid than check whether any item left to apply promotion
		SET @i = 1

		WHILE @i <= @iCount
		BEGIN
			IF @bitRemainingCheckRquired=1
			BEGIN
				IF @tintDiscountType = 2 -- Discount by amount
				BEGIN
					SET @numRemainingPromotion = @fltDiscountValue - ISNULL((SELECT SUM(fltDiscount) FROM @TEMP WHERE numPromotionID = @numTempPromotionID	AND ISNULL(bitDiscountType,0)=1 AND bitPromotionTriggered = 0),0)
				END
				ELSE IF @tintDiscountType = 3 -- Discount by quantity
				BEGIN
					SET @numRemainingPromotion = @fltDiscountValue - ISNULL((SELECT SUM(fltDiscount/monUnitPrice) FROM @TEMP WHERE numPromotionID = @numTempPromotionID	AND ISNULL(bitDiscountType,0)=1 AND bitPromotionTriggered = 0),0)
				END
			END

			IF @bitRemainingCheckRquired=0 OR (@bitRemainingCheckRquired=1 AND @numRemainingPromotion > 0)
			BEGIN
				UPDATE
					T1
				SET
					monUnitPrice= (CASE WHEN @tintItemCalDiscount = 1 THEN ISNULL(T1.monInsertUnitPrice,0) ELSE ISNULL(I.monListPrice,0) END)
					,fltDiscount = (CASE 
										WHEN @bitRemainingCheckRquired=0 
										THEN 
											@fltDiscountValue
										ELSE 
											(CASE 
												WHEN @tintDiscountType = 2 -- Discount by amount
												THEN
													(CASE 
														WHEN (T1.numUnits * (CASE 
																							WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
																							THEN dbo.GetCalculatedPriceForKitAssembly(@numDomainID,I.numItemCode,@numTempWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,ISNULL(T1.vcKitChildItems,''))
																							ELSE (CASE WHEN @tintItemCalDiscount = 1 THEN ISNULL(T1.monInsertUnitPrice,0) ELSE ISNULL(I.monListPrice,0) END)
																						END)) >= @numRemainingPromotion 
														THEN @numRemainingPromotion 
														ELSE (T1.numUnits * (CASE 
																							WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
																							THEN dbo.GetCalculatedPriceForKitAssembly(@numDomainID,I.numItemCode,@numTempWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,ISNULL(T1.vcKitChildItems,''))
																							ELSE (CASE WHEN @tintItemCalDiscount = 1 THEN ISNULL(T1.monInsertUnitPrice,0) ELSE ISNULL(I.monListPrice,0) END)
																						END)) 
													END)
												WHEN @tintDiscountType = 3 -- Discount by quantity
												THEN
													(CASE 
														WHEN T1.numUnits >= @numRemainingPromotion 
														THEN (@numRemainingPromotion * (CASE 
																							WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
																							THEN dbo.GetCalculatedPriceForKitAssembly(@numDomainID,I.numItemCode,@numTempWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,ISNULL(T1.vcKitChildItems,''))
																							ELSE (CASE WHEN @tintItemCalDiscount = 1 THEN ISNULL(T1.monInsertUnitPrice,0) ELSE ISNULL(I.monListPrice,0) END)
																						END)) 
														ELSE (T1.numUnits * (CASE 
																				WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
																				THEN dbo.GetCalculatedPriceForKitAssembly(@numDomainID,I.numItemCode,@numTempWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,ISNULL(T1.vcKitChildItems,''))
																				ELSE (CASE WHEN @tintItemCalDiscount = 1 THEN ISNULL(T1.monInsertUnitPrice,0) ELSE ISNULL(I.monListPrice,0) END)
																			END)) 
													END)
												ELSE 0
											END)
										END
									)
					,bitDiscountType = (CASE WHEN @bitRemainingCheckRquired=0 THEN 0 ELSE 1 END)
					,numPromotionID=@numTempPromotionID
					,bitPromotionTriggered=0
					,vcPromotionDescription=@vcShortDesc
					,bitChanged=1
				FROM
					@TEMP T1
				INNER JOIN
					Item I
				ON
					T1.numItemCode = I.numItemCode
				WHERE
					ID=@i
					AND ISNULL(T1.numPromotionID,0) = 0
					AND 1 = (CASE 
								WHEN @tintDiscoutBaseOn = 1 -- Selected Items
								THEN
									(CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=6 AND tintType=1 AND numValue=I.numItemCode) > 0 THEN 1 ELSE 0 END)
								WHEN @tintDiscoutBaseOn = 2 -- Selected Item Classification
								THEN 
									(CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=6 AND tintType=2 AND numValue=ISNULL(I.numItemClassification,0)) > 0 THEN 1 ELSE 0 END)
								WHEN @tintDiscoutBaseOn = 3 -- Related Items
								THEN
									(CASE WHEN (SELECT COUNT(*) FROM SimilarItems WHERE numDomainId=@numDomainID AND numParentItemCode=@numItemCode AND numItemCode=I.numItemCode) > 0 THEN 1 ELSE 0 END)
								WHEN @tintDiscoutBaseOn = 4 -- Item with list price lesser or equal
								THEN
									(CASE WHEN ISNULL(I.monListPrice,0) <= ISNULL((SELECT monListPrice FROM Item WHERE numItemCode=@numItemCode),0) THEN 1 ELSE 0 END)
								ELSE 0
							END)
			END

			SET @i = @i + 1
		END

		SET @j = @j + 1
	END

	UPDATE
		CI
	SET
		monPrice=T1.monUnitPrice
		,fltDiscount=T1.fltDiscount
		,bitDiscountType=T1.bitDiscountType
		,monTotAmtBefDiscount = ISNULL(CI.numUnitHour,0) * ISNULL(T1.monUnitPrice,0)
		,monTotAmount = (CASE 
							WHEN ISNULL(T1.fltDiscount,0) > 0
							THEN (CASE 
									WHEN ISNULL(T1.bitDiscountType,0) = 1 
									THEN (CASE 
											WHEN (ISNULL(CI.numUnitHour,0) * ISNULL(T1.monUnitPrice,0)) - T1.fltDiscount >= 0
											THEN (ISNULL(CI.numUnitHour,0) * ISNULL(T1.monUnitPrice,0)) - T1.fltDiscount
											ELSE 0
										END)
									ELSE ISNULL(CI.numUnitHour,0) * (ISNULL(T1.monUnitPrice,0) - (ISNULL(T1.monUnitPrice,0) * (T1.fltDiscount/100)))
								END)
							ELSE ISNULL(CI.numUnitHour,0) * ISNULL(T1.monUnitPrice,0)
						END)
		,PromotionID=T1.numPromotionID
		,bitPromotionTrigerred=T1.bitPromotionTriggered
		,PromotionDesc=T1.vcPromotionDescription
	FROM
		CartItems CI
	INNER JOIN
		@Temp T1
	ON
		CI.numCartID = T1.numCartID
	WHERE
		T1.bitChanged=1
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_PromotionOffer_ApplyItemPromotionToOrder')
DROP PROCEDURE USP_PromotionOffer_ApplyItemPromotionToOrder
GO
CREATE PROCEDURE [dbo].[USP_PromotionOffer_ApplyItemPromotionToOrder]
	@numDomainID NUMERIC(18,0),
	@numDivisionID NUMERIC(18,0),
	@vcItems VARCHAR(MAX),
	@bitBasedOnDiscountCode BIT,
	@vcDiscountCode VARCHAR(100)
AS
BEGIN
	DECLARE @hDocItem INT
	DECLARE @numRelationship NUMERIC(18,0)
	DECLARE @numProfile NUMERIC(18,0)

	SELECT 
		@numRelationship=numCompanyType,
		@numProfile=vcProfile
	FROM 
		DivisionMaster D                  
	JOIN 
		CompanyInfo C 
	ON 
		C.numCompanyId=D.numCompanyID                  
	WHERE 
		numDivisionID =@numDivisionID 		

	DECLARE @TEMP TABLE
	(
		ID INT IDENTITY(1,1)
		,numOppItemID NUMERIC(18,0)
        ,numItemCode NUMERIC(18,0)
        ,numUnits FLOAT
		,numWarehouseItemID NUMERIC(18,0)
        ,monUnitPrice DECIMAL(20,5)
        ,fltDiscount DECIMAL(20,5)
        ,bitDiscountType BIT
        ,monTotalAmount DECIMAL(20,5)
        ,numPromotionID NUMERIC(18,0)
        ,bitPromotionTriggered BIT
        ,vcPromotionDescription VARCHAR(MAX)
		,vcKitChildItems VARCHAR(MAX)
		,vcInclusionDetail VARCHAR(MAX)
		,numSelectedPromotionID NUMERIC(18,0)
		,bitChanged BIT		
	)

	IF ISNULL(@vcItems,'') <> ''
	BEGIN
		EXEC sp_xml_preparedocument @hDocItem OUTPUT, @vcItems

		INSERT INTO @TEMP
		(
			numOppItemID
			,numItemCode
			,numUnits
			,numWarehouseItemID
			,monUnitPrice
			,fltDiscount
			,bitDiscountType
			,monTotalAmount
			,numPromotionID
			,bitPromotionTriggered
			,vcPromotionDescription
			,vcKitChildItems
			,vcInclusionDetail
			,numSelectedPromotionID
			,bitChanged
		)

		SELECT 
			numOppItemID
			,numItemCode
			,numUnits
			,numWarehouseItemID
			,monUnitPrice
			,fltDiscount
			,bitDiscountType
			,monTotalAmount
			,numPromotionID
			,bitPromotionTriggered
			,vcPromotionDescription
			,vcKitChildItems
			,vcInclusionDetail
			,numSelectedPromotionID
			,0
		FROM 
			OPENXML (@hDocItem,'/NewDataSet/Item',2)                                                                          
		WITH                       
		(                                                                          
			numOppItemID NUMERIC(18,0)
			,numItemCode NUMERIC(18,0)
			,numUnits FLOAT
			,numWarehouseItemID NUMERIC(18,0)
			,monUnitPrice DECIMAL(20,5)
			,fltDiscount DECIMAL(20,5)
			,bitDiscountType BIT
			,monTotalAmount DECIMAL(20,5)
			,numPromotionID NUMERIC(18,0)
			,bitPromotionTriggered BIT
			,vcKitChildItems VARCHAR(MAX)
			,vcInclusionDetail VARCHAR(MAX)
			,vcPromotionDescription VARCHAR(MAX)
			,numSelectedPromotionID NUMERIC(18,0)
		)

		EXEC sp_xml_removedocument @hDocItem

		DECLARE @i INT = 1
		DECLARE @iCount INT 
		SET @iCount = (SELECT COUNT(*) FROM @TEMP)

		DECLARE @j INT = 1
		DECLARE @jCount INT

		DECLARE @numTempPromotionID NUMERIC(18,0)
		DECLARE @vcShortDesc VARCHAR(500)
		DECLARE @tintItemCalDiscount TINYINT
		DECLARE @numItemCode NUMERIC(18,0)
		DECLARE @numTempWarehouseItemID NUMERIC(18,0)
		DECLARE @numUnits FLOAT
		DECLARE @monTotalAmount DECIMAL(20,5)
		DECLARE @numItemClassification AS NUMERIC(18,0)

		DECLARE @bitRemainingCheckRquired AS BIT
		DECLARE @numRemainingPromotion FLOAT
		DECLARE @fltDiscountValue FLOAT
		DECLARE @tintDiscountType TINYINT
		DECLARE @tintDiscoutBaseOn TINYINT

		DECLARE @TEMPPromotion TABLE
		(
			ID INT IDENTITY(1,1)
			,numPromotionID NUMERIC(18,0)
			,vcShortDesc VARCHAR(500)
			,numTriggerItemCode NUMERIC(18,0)
			,numWarehouseItemID NUMERIC(18,0)
			,numUnits FLOAT
			,monTotalAmount DECIMAL(20,5)
		)

		IF ISNULL(@bitBasedOnDiscountCode,0) = 1
		BEGIN
			WHILE @i <= @iCount
			BEGIN
				-- IF USER SELECTED PROMOTION HE/SHE WANT TO USE THEN FIRST WE HAVE TO VALIDAE IT AND MARK IT AS TRIGGER
				UPDATE
					T1
				SET
					bitChanged = 1
					,numPromotionID=PO.numProId
					,bitPromotionTriggered=1
					,vcPromotionDescription=ISNULL(PO.vcShortDesc,'')
				FROM
					@TEMP T1
				INNER JOIN
					Item I
				ON
					T1.numItemCode = I.numItemCode
				INNER JOIN
					PromotionOffer PO 
				ON
					T1.numSelectedPromotionID = PO.numProId
				INNER JOIN
					PromotionOffer POOrder
				ON
					PO.numOrderPromotionID = POOrder.numProId
				INNER JOIN 
					DiscountCodes DC
				ON 
					POOrder.numProId = DC.numPromotionID
				WHERE
					T1.numSelectedPromotionID > 0
					AND ISNULL(T1.numPromotionID,0)  = 0
					AND T1.ID = @i
					AND PO.numDomainId=@numDomainID 
					AND ISNULL(PO.bitEnabled,0)=1 
					AND ISNULL(PO.IsOrderBasedPromotion,0) = 0
					AND ISNULL(POOrder.IsOrderBasedPromotion,0) = 1
					AND 1 = (CASE WHEN POOrder.bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN GETUTCDATE()>=POOrder.dtValidFrom AND GETUTCDATE()<=POOrder.dtValidTo THEN 1 ELSE 0 END) END)
					AND 1 = (CASE WHEN ISNULL(DC.CodeUsageLimit,0) <> 0 THEN (CASE WHEN ISNULL((SELECT intCodeUsed FROM DiscountCodeUsage WHERE numDiscountId=DC.numDiscountId AND numDivisionId=@numDivisionId),0) >= DC.CodeUsageLimit THEN 0 ELSE 1 END) ELSE 1 END)
					AND 1 = (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=POOrder.numProId AND tintType=2 AND numRelationship=@numRelationship AND numProfile=@numProfile) > 0 THEN 1 ELSE 0 END)
					AND DC.vcDiscountCode = @vcDiscountCode
					AND 1 = (CASE 
								WHEN ISNULL(PO.tintOfferTriggerValueType,1) = 1 --Based on quantity
								THEN
									CASE
										WHEN ISNULL(PO.tintOfferTriggerValueTypeRange,1) = 2
										THEN
											(CASE WHEN ISNULL(numUnits,0) BETWEEN ISNULL(PO.fltOfferTriggerValue,0) AND ISNULL(PO.fltOfferTriggerValueRange,0) THEN 1 ELSE 0 END)
										ELSE
											(CASE WHEN ISNULL(numUnits,0) >= ISNULL(PO.fltOfferTriggerValue,0) THEN 1 ELSE 0 END)
									END
								WHEN ISNULL(PO.tintOfferTriggerValueType,1) = 2 --Based on amount
								THEN
									CASE
										WHEN ISNULL(PO.tintOfferTriggerValueTypeRange,1) = 2
										THEN
											(CASE WHEN ISNULL(monTotalAmount,0) BETWEEN ISNULL(PO.fltOfferTriggerValue,0) AND ISNULL(PO.fltOfferTriggerValueRange,0) THEN 1 ELSE 0 END)
										ELSE
											(CASE WHEN ISNULL(monTotalAmount,0) >= ISNULL(PO.fltOfferTriggerValue,0) THEN 1 ELSE 0 END)
									END
							END)
					AND 1 = (CASE 
							WHEN PO.tintOfferBasedOn = 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 AND numValue=T1.numItemCode) > 0 THEN 1 ELSE 0 END)
							WHEN PO.tintOfferBasedOn = 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=2 AND numValue=ISNULL(I.numItemClassification,0)) > 0 THEN 1 ELSE 0 END)
							WHEN PO.tintOfferBasedOn = 4 THEN 1 
							ELSE 0
						END)
					AND ISNULL((SELECT COUNT(*) FROM @TEMP T3 WHERE T3.numPromotionID=PO.numProId AND T3.bitPromotionTriggered=1),0) = 0

				UPDATE
					T1
				SET
					bitChanged = 1
					,numPromotionID=T2.numProId
					,bitPromotionTriggered=1
					,vcPromotionDescription=vcShortDesc
				FROM
					@TEMP T1
				INNER JOIN
					Item I
				ON
					T1.numItemCode=I.numItemCode
				CROSS APPLY
				(
					SELECT TOP 1
						PO.numProId
						,PO.tintOfferBasedOn
						,PO.tintOfferTriggerValueType
						,PO.tintOfferTriggerValueTypeRange
						,PO.fltOfferTriggerValue
						,PO.fltOfferTriggerValueRange
						,ISNULL(PO.vcShortDesc,'') vcShortDesc
					FROM 
						PromotionOffer PO
					INNER JOIN
						PromotionOffer POOrder
					ON
						PO.numOrderPromotionID = POOrder.numProId
					INNER JOIN 
						DiscountCodes DC
					ON 
						POOrder.numProId = DC.numPromotionID
					WHERE
						PO.numDomainId=@numDomainID
						AND ISNULL(PO.bitEnabled,0)=1 
						AND ISNULL(PO.IsOrderBasedPromotion,0) = 0
						AND ISNULL(POOrder.IsOrderBasedPromotion,0) = 1
						AND 1 = (CASE WHEN POOrder.bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN GETUTCDATE()>=POOrder.dtValidFrom AND GETUTCDATE()<=POOrder.dtValidTo THEN 1 ELSE 0 END) END)
						AND 1 = (CASE WHEN ISNULL(DC.CodeUsageLimit,0) <> 0 THEN (CASE WHEN ISNULL((SELECT intCodeUsed FROM DiscountCodeUsage WHERE numDiscountId=DC.numDiscountId AND numDivisionId=@numDivisionId),0) >= DC.CodeUsageLimit THEN 0 ELSE 1 END) ELSE 1 END)
						AND 1 = (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=POOrder.numProId AND tintType=2 AND numRelationship=@numRelationship AND numProfile=@numProfile) > 0 THEN 1 ELSE 0 END)
						AND DC.vcDiscountCode = @vcDiscountCode
						AND 1 = (CASE 
								WHEN ISNULL(PO.tintOfferTriggerValueType,1) = 1 --Based on quantity
								THEN
									CASE
										WHEN ISNULL(PO.tintOfferTriggerValueTypeRange,1) = 2
										THEN
											(CASE WHEN ISNULL(T1.numUnits,0) BETWEEN ISNULL(PO.fltOfferTriggerValue,0) AND ISNULL(PO.fltOfferTriggerValueRange,0) THEN 1 ELSE 0 END)
										ELSE
											(CASE WHEN ISNULL(T1.numUnits,0) >= ISNULL(PO.fltOfferTriggerValue,0) THEN 1 ELSE 0 END)
									END
								WHEN ISNULL(PO.tintOfferTriggerValueType,1) = 2 --Based on amount
								THEN
									CASE
										WHEN ISNULL(PO.tintOfferTriggerValueTypeRange,1) = 2
										THEN
											(CASE WHEN ISNULL(T1.monTotalAmount,0) BETWEEN ISNULL(PO.fltOfferTriggerValue,0) AND ISNULL(PO.fltOfferTriggerValueRange,0) THEN 1 ELSE 0 END)
										ELSE
											(CASE WHEN ISNULL(T1.monTotalAmount,0) >= ISNULL(PO.fltOfferTriggerValue,0) THEN 1 ELSE 0 END)
									END
							END)
					ORDER BY
						(CASE 
							WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 1 THEN 1
							WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 1 THEN 2
							WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 2 THEN 3
							WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 2 THEN 4
							WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 3 THEN 5
							WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 3 THEN 6
						END) ASC,ISNULL(PO.fltOfferTriggerValue,0) DESC

				) T2
				WHERE
					ISNULL(numPromotionID,0)  = 0
					AND T1.ID = @i
					AND ISNULL((SELECT COUNT(*) FROM @TEMP T3 WHERE T3.numPromotionID=T2.numProId AND T3.bitPromotionTriggered=1),0) = 0
					AND 1 = (CASE 
								WHEN T2.tintOfferBasedOn = 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=T2.numProId AND tintRecordType=5 AND tintType=1 AND numValue=T1.numItemCode) > 0 THEN 1 ELSE 0 END)
								WHEN T2.tintOfferBasedOn = 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=T2.numProId AND tintRecordType=5 AND tintType=2 AND numValue=ISNULL(I.numItemClassification,0)) > 0 THEN 1 ELSE 0 END)
								WHEN T2.tintOfferBasedOn = 4 THEN 1 
								ELSE 0
							END)

				SET @i = @i + 1
			END
		END

		INSERT INTO @TEMPPromotion
		(
			numPromotionID
			,numTriggerItemCode
			,numWarehouseItemID
			,numUnits
			,monTotalAmount
		)
		SELECT
			numPromotionID
			,numItemCode
			,numWarehouseItemID
			,numUnits
			,monTotalAmount
		FROM
			@TEMP T1
		INNER JOIN
			PromotionOffer PO
		ON
			T1.numPromotionID = PO.numProId
		WHERE
			ISNULL(numPromotionID,0) > 0
			AND ISNULL(bitPromotionTriggered,0) = 1
			AND 1 = (CASE WHEN @bitBasedOnDiscountCode=0 THEN (CASE WHEN ISNULL(PO.numOrderPromotionID,0) = 0 THEN 1 ELSE 0 END) ELSE 1 END)

		SET @jCount = ISNULL((SELECT COUNT(*) FROM @TEMPPromotion),0)

		-- FIRST REMOVE ALL PROMOTIONS WHERE ITEM WHICH TRIGERRED PROMOTION IS DELETED
		UPDATE 
			T1
		SET 
			numPromotionID=0
			,bitPromotionTriggered=0
			,vcPromotionDescription=''
			,bitChanged=1
			,monUnitPrice=ISNULL(T2.monPrice,0)
			,fltDiscount=ISNULL(T2.decDiscount,0)
			,bitDiscountType=ISNULL(T2.tintDisountType,0)
		FROM
			@TEMP T1
		CROSS APPLY
			dbo.fn_GetItemPriceBasedOnMethod(@numDomainID,@numDivisionID,T1.numItemCode,T1.numUnits) T2
		WHERE 
			numPromotionID > 0
			AND ISNULL(bitPromotionTriggered,0) = 0
			AND (SELECT COUNT(*) FROM @TEMP T3 WHERE T3.numPromotionID=T1.numPromotionID AND ISNULL(T3.bitPromotionTriggered,0) = 1) = 0

		-- FIRST REMOVE APPLIED PROMOTION IF THEY ARE NOT VALIES
		WHILE @j <= @jCount
		BEGIN
			SELECT 
				@numTempPromotionID=numPromotionID
				,@numItemCode=numTriggerItemCode
				,@numTempWarehouseItemID=numWarehouseItemID
				,@numUnits = numUnits
				,@monTotalAmount=monTotalAmount
			FROM 
				@TEMPPromotion 
			WHERE 
				ID = @j

			SET @numItemClassification = ISNULL((SELECT numItemClassification FROM Item WHERE Item.numItemCode = @numItemCode),0)

			IF NOT EXISTS (SELECT 
							PO.numProId 
						FROM 
							PromotionOffer PO
						LEFT JOIN	
							PromotionOffer POOrder
						ON
							PO.numOrderPromotionID = POOrder.numProId
						LEFT JOIN	
							DiscountCodes DC
						ON
							PO.numOrderPromotionID = DC.numPromotionID
						WHERE 
							PO.numDomainId=@numDomainID 
							AND PO.numProId=@numTempPromotionID
							AND ISNULL(PO.bitEnabled,0)=1 
							AND ISNULL(PO.IsOrderBasedPromotion,0) = 0
							AND 1 = (CASE 
										WHEN ISNULL(PO.numOrderPromotionID,0) > 0 
										THEN (CASE WHEN POOrder.bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN GETUTCDATE()>=POOrder.dtValidFrom AND GETUTCDATE()<=POOrder.dtValidTo THEN 1 ELSE 0 END) END)
										ELSE (CASE WHEN PO.bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN GETUTCDATE()>=PO.dtValidFrom AND GETUTCDATE()<=PO.dtValidTo THEN 1 ELSE 0 END) END)
									END)
							AND 1 = (CASE 
										WHEN ISNULL(PO.numOrderPromotionID,0) > 0 
										THEN
											(CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numOrderPromotionID AND tintType=2 AND numRelationship=@numRelationship AND numProfile=@numProfile) > 0 THEN 1 ELSE 0 END)
										ELSE
											(CASE PO.tintCustomersBasedOn 
												WHEN 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numProId AND tintType=1 AND numDivisionID=@numDivisionID) > 0 THEN 1 ELSE 0 END)
												WHEN 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numProId AND tintType=2 AND numRelationship=@numRelationship AND numProfile=@numProfile) > 0 THEN 1 ELSE 0 END)
												WHEN 3 THEN 1
												ELSE 0
											END)
									END)
							AND 1 = (CASE 
										WHEN PO.tintOfferBasedOn = 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 AND numValue=@numItemCode) > 0 THEN 1 ELSE 0 END)
										WHEN PO.tintOfferBasedOn = 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=2 AND numValue=@numItemClassification) > 0 THEN 1 ELSE 0 END)
										WHEN PO.tintOfferBasedOn = 4 THEN 1 
										ELSE 0
									END)
							AND 1 = (CASE 
										WHEN ISNULL(PO.tintOfferTriggerValueType,1) = 1 --Based on quantity
										THEN
											CASE
												WHEN ISNULL(PO.tintOfferTriggerValueTypeRange,1) = 2
												THEN
													(CASE WHEN ISNULL(@numUnits,0) BETWEEN ISNULL(PO.fltOfferTriggerValue,0) AND ISNULL(PO.fltOfferTriggerValueRange,0) THEN 1 ELSE 0 END)
												ELSE
													(CASE WHEN ISNULL(@numUnits,0) >= PO.fltOfferTriggerValue THEN 1 ELSE 0 END)
											END
										WHEN ISNULL(PO.tintOfferTriggerValueType,1) = 2 --Based on amount
										THEN
											CASE
												WHEN ISNULL(PO.tintOfferTriggerValueTypeRange,1) = 2
												THEN
													(CASE WHEN ISNULL(@monTotalAmount,0) BETWEEN ISNULL(PO.fltOfferTriggerValue,0) AND ISNULL(PO.fltOfferTriggerValueRange,0) THEN 1 ELSE 0 END)
												ELSE
													(CASE WHEN ISNULL(@monTotalAmount,0) >= PO.fltOfferTriggerValue THEN 1 ELSE 0 END)
											END
									END)
							AND 1 = (CASE 
										WHEN ISNULL(PO.numOrderPromotionID,0) > 0 
										THEN 
											(CASE WHEN ISNULL(DC.CodeUsageLimit,0) <> 0 THEN (CASE WHEN ISNULL((SELECT intCodeUsed FROM DiscountCodeUsage WHERE numDiscountId=DC.numDiscountId AND numDivisionId=@numDivisionId),0) >= DC.CodeUsageLimit THEN 0 ELSE 1 END) ELSE 1 END)
										ELSE 1 
									END)
					)
			BEGIN
				-- IF Promotion is not valid than revert line item price to based on default pricing
				UPDATE 
					T1
				SET 
					numPromotionID=0
					,bitPromotionTriggered=0
					,vcPromotionDescription=''
					,bitChanged=1
					,monUnitPrice=ISNULL(T2.monPrice,0)
					,fltDiscount=ISNULL(T2.decDiscount,0)
					,bitDiscountType=ISNULL(T2.tintDisountType,0)
				FROM
					@TEMP T1
				CROSS APPLY
					dbo.fn_GetItemPriceBasedOnMethod(@numDomainID,@numDivisionID,T1.numItemCode,T1.numUnits) T2
				WHERE 
					numPromotionID=@numTempPromotionID
			END

			SET @j = @j + 1
		END

		SET @j = 1
		WHILE @j <= @jCount
		BEGIN
			SELECT 
				@numTempPromotionID=numPromotionID
				,@numItemCode=numTriggerItemCode
				,@numTempWarehouseItemID=numWarehouseItemID
				,@numUnits = numUnits
				,@monTotalAmount=monTotalAmount
			FROM 
				@TEMPPromotion 
			WHERE 
				ID = @j

			SELECT
				@fltDiscountValue=ISNULL(fltDiscountValue,0)
				,@tintDiscountType=ISNULL(tintDiscountType,0)
				,@tintDiscoutBaseOn=tintDiscoutBaseOn
				,@vcShortDesc=ISNULL(vcShortDesc,'')
				,@tintItemCalDiscount=ISNULL(tintItemCalDiscount,1)
			FROM
				PromotionOffer
			WHERE
				numProId=@numTempPromotionID

			-- If discount is based on amount or quantity than we have to checked remaining amount or qty left to give as discount 
			IF @tintDiscountType = 2 OR @tintDiscountType = 3
			BEGIN
				SET @bitRemainingCheckRquired = 1
			END
			ELSE
			BEGIN
				SET @bitRemainingCheckRquired = 0
				SET @numRemainingPromotion = 0
			END

			-- If promotion is valid than check whether any item left to apply promotion
			SET @i = 1

			WHILE @i <= @iCount
			BEGIN
				IF @bitRemainingCheckRquired=1
				BEGIN
					IF @tintDiscountType = 2 -- Discount by amount
					BEGIN
						SET @numRemainingPromotion = @fltDiscountValue - ISNULL((SELECT SUM(fltDiscount) FROM @TEMP WHERE numPromotionID = @numTempPromotionID	AND ISNULL(bitDiscountType,0)=1 AND bitPromotionTriggered = 0),0)
					END
					ELSE IF @tintDiscountType = 3 -- Discount by quantity
					BEGIN
						SET @numRemainingPromotion = @fltDiscountValue - ISNULL((SELECT SUM(fltDiscount/monUnitPrice) FROM @TEMP WHERE numPromotionID = @numTempPromotionID	AND ISNULL(bitDiscountType,0)=1 AND bitPromotionTriggered = 0),0)
					END
				END

				IF @bitRemainingCheckRquired=0 OR (@bitRemainingCheckRquired=1 AND @numRemainingPromotion > 0)
				BEGIN
					UPDATE
						T1
					SET
						monUnitPrice= CASE 
											WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
											THEN dbo.GetCalculatedPriceForKitAssembly(@numDomainID,I.numItemCode,@numTempWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,ISNULL(T1.vcKitChildItems,''))
											ELSE (CASE WHEN @tintItemCalDiscount = 1 THEN ISNULL(T2.monFinalPrice,0) ELSE ISNULL(I.monListPrice,0) END)
										END
						,fltDiscount = (CASE 
											WHEN @bitRemainingCheckRquired=0 
											THEN 
												@fltDiscountValue
											ELSE 
												(CASE 
													WHEN @tintDiscountType = 2 -- Discount by amount
													THEN
														(CASE 
															WHEN (T1.numUnits * (CASE 
																								WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
																								THEN dbo.GetCalculatedPriceForKitAssembly(@numDomainID,I.numItemCode,@numTempWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,ISNULL(T1.vcKitChildItems,''))
																								ELSE (CASE WHEN @tintItemCalDiscount = 1 THEN ISNULL(T2.monFinalPrice,0) ELSE ISNULL(I.monListPrice,0) END)
																							END)) >= @numRemainingPromotion 
															THEN @numRemainingPromotion 
															ELSE (T1.numUnits * (CASE 
																								WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
																								THEN dbo.GetCalculatedPriceForKitAssembly(@numDomainID,I.numItemCode,@numTempWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,ISNULL(T1.vcKitChildItems,''))
																								ELSE (CASE WHEN @tintItemCalDiscount = 1 THEN ISNULL(T2.monFinalPrice,0) ELSE ISNULL(I.monListPrice,0) END)
																							END)) 
														END)
													WHEN @tintDiscountType = 3 -- Discount by quantity
													THEN
														(CASE 
															WHEN T1.numUnits >= @numRemainingPromotion 
															THEN (@numRemainingPromotion * (CASE 
																								WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
																								THEN dbo.GetCalculatedPriceForKitAssembly(@numDomainID,I.numItemCode,@numTempWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,ISNULL(T1.vcKitChildItems,''))
																								ELSE (CASE WHEN @tintItemCalDiscount = 1 THEN ISNULL(T2.monFinalPrice,0) ELSE ISNULL(I.monListPrice,0) END)
																							END)) 
															ELSE (T1.numUnits * (CASE 
																					WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
																					THEN dbo.GetCalculatedPriceForKitAssembly(@numDomainID,I.numItemCode,@numTempWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,ISNULL(T1.vcKitChildItems,''))
																					ELSE (CASE WHEN @tintItemCalDiscount = 1 THEN ISNULL(T2.monFinalPrice,0) ELSE ISNULL(I.monListPrice,0) END)
																				END)) 
														END)
													ELSE 0
												END)
											END
										)
						,bitDiscountType = (CASE WHEN @bitRemainingCheckRquired=0 THEN 0 ELSE 1 END)
						,numPromotionID=@numTempPromotionID
						,bitPromotionTriggered=0
						,vcPromotionDescription=@vcShortDesc
						,bitChanged=1
					FROM
						@TEMP T1
					INNER JOIN
						Item I
					ON
						T1.numItemCode = I.numItemCode
					CROSS APPLY
						dbo.fn_GetItemPriceBasedOnMethod(@numDomainID,@numDivisionID,T1.numItemCode,T1.numUnits) T2
					WHERE
						ID=@i
						AND ISNULL(T1.numPromotionID,0) = 0
						AND 1 = (CASE 
									WHEN @tintDiscoutBaseOn = 1 -- Selected Items
									THEN
										(CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=6 AND tintType=1 AND numValue=I.numItemCode) > 0 THEN 1 ELSE 0 END)
									WHEN @tintDiscoutBaseOn = 2 -- Selected Item Classification
									THEN 
										(CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=6 AND tintType=2 AND numValue=ISNULL(I.numItemClassification,0)) > 0 THEN 1 ELSE 0 END)
									WHEN @tintDiscoutBaseOn = 3 -- Related Items
									THEN
										(CASE WHEN (SELECT COUNT(*) FROM SimilarItems WHERE numDomainId=@numDomainID AND numParentItemCode=@numItemCode AND numItemCode=I.numItemCode) > 0 THEN 1 ELSE 0 END)
									WHEN @tintDiscoutBaseOn = 4 -- Item with list price lesser or equal
									THEN
										(CASE WHEN ISNULL(I.monListPrice,0) <= ISNULL((SELECT monListPrice FROM Item WHERE numItemCode=@numItemCode),0) THEN 1 ELSE 0 END)
									ELSE 0
								END)
				END

				SET @i = @i + 1
			END

			SET @j = @j + 1
		END
			
	END

	SELECT * FROM @TEMP WHERE bitChanged=1
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_PromotionOffer_ClearCouponCodeECommerce')
DROP PROCEDURE USP_PromotionOffer_ClearCouponCodeECommerce
GO
CREATE PROCEDURE [dbo].[USP_PromotionOffer_ClearCouponCodeECommerce]
	@numDomainID NUMERIC(18,0),
	@numUserCntId NUMERIC(18,0),
	@numSiteID NUMERIC(18,0),
	@vcCookieId VARCHAR(100),
	@numPromotionID NUMERIC(18,0)
AS
BEGIN
	UPDATE 
		CartItems
	SET 
		PromotionID=0
		,bitPromotionTrigerred=0
		,PromotionDesc=''
		,monPrice=ISNULL(monInsertPrice,0)
		,fltDiscount=ISNULL(fltInsertDiscount,0)
		,bitDiscountType=ISNULL(bitInsertDiscountType,0)
		,monTotAmtBefDiscount = ISNULL(numUnitHour,0) * ISNULL(monInsertPrice,0)
		,monTotAmount = (CASE 
							WHEN ISNULL(fltInsertDiscount,0) > 0
							THEN (CASE 
									WHEN ISNULL(bitInsertDiscountType,0) = 1 
									THEN (CASE 
											WHEN (ISNULL(numUnitHour,0) * ISNULL(monInsertPrice,0)) - fltInsertDiscount >= 0
											THEN (ISNULL(numUnitHour,0) * ISNULL(monInsertPrice,0)) - fltInsertDiscount
											ELSE 0
										END)
									ELSE ISNULL(numUnitHour,0) * (ISNULL(monInsertPrice,0) - (ISNULL(monInsertPrice,0) * (fltInsertDiscount/100)))
								END)
							ELSE ISNULL(numUnitHour,0) * ISNULL(monInsertPrice,0)
						END)
	WHERE 
		numDomainId=@numDomainID
		AND vcCookieId=@vcCookieId  
		AND numUserCntId=@numUserCntId 
		AND ISNULL(PromotionID,0)=@numPromotionID
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_PromotionOffer_GetPromotionApplicableToItem')
DROP PROCEDURE USP_PromotionOffer_GetPromotionApplicableToItem
GO
CREATE PROCEDURE [dbo].[USP_PromotionOffer_GetPromotionApplicableToItem]
(
	@numDomainID NUMERIC(18,0)
	,@numDivisionID NUMERIC(18,0)
	,@numSiteID NUMERIC(18,0)
	,@numItemCode NUMERIC(18,0)
)
AS
BEGIN
	DECLARE @numRelationship NUMERIC(18,0)
	DECLARE @numProfile NUMERIC(18,0)
	DECLARE @numItemClassification NUMERIC(18,0)

	SELECT 
		@numItemClassification=ISNULL(numItemClassification,0) 
	FROM 
		Item 
	WHERE 
		numDomainID=@numDomainID 
		AND numItemCode=@numItemCode

	IF ISNULL(@numDivisionID,0) > 0
	BEGIN
		SELECT 
			@numRelationship=numCompanyType,
			@numProfile=vcProfile
		FROM 
			DivisionMaster D                  
		JOIN 
			CompanyInfo C 
		ON 
			C.numCompanyId=D.numCompanyID                  
		WHERE 
			numDivisionID =@numDivisionID
	END
	ELSE IF ISNULL(@numSiteID,0) > 0
	BEGIN
		SELECT
			@numRelationship=ISNULL(numRelationshipId,0),
			@numProfile=ISNULL(numProfileId,0)
		FROM
			eCommerceDTL
		WHERE
			numSiteId=@numSiteID
	END

	SELECT
		PO.numProId
		,PO.vcProName
		,CONCAT(ISNULL(PO.vcShortDesc,'-'),(CASE WHEN ISNULL(PO.numOrderPromotionID,0) > 0 THEN ' (Coupon code required)' ELSE '' END)) AS vcShortDesc
		,CONCAT(ISNULL(PO.vcLongDesc,'-'),(CASE WHEN ISNULL(PO.numOrderPromotionID,0) > 0 THEN ' (Coupon code required)' ELSE '' END)) AS vcLongDesc
	FROM 
		PromotionOffer PO
	LEFT JOIN
		PromotionOffer POOrder
	ON
		PO.numOrderPromotionID = POOrder.numProId
	WHERE 
		PO.numDomainId=@numDomainID 
		AND ISNULL(PO.bitEnabled,0)=1 
		AND ISNULL(PO.IsOrderBasedPromotion,0) = 0
		AND 1 = (CASE 
					WHEN ISNULL(PO.numOrderPromotionID,0) > 0 
					THEN (CASE WHEN POOrder.bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN GETUTCDATE()>=POOrder.dtValidFrom AND GETUTCDATE()<=POOrder.dtValidTo THEN 1 ELSE 0 END) END)
					ELSE (CASE WHEN PO.bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN GETUTCDATE()>=PO.dtValidFrom AND GETUTCDATE()<=PO.dtValidTo THEN 1 ELSE 0 END) END)
				END)
		AND 1 = (CASE 
					WHEN ISNULL(PO.numOrderPromotionID,0) > 0 
					THEN
						(CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numOrderPromotionID AND tintType=2 AND numRelationship=@numRelationship AND numProfile=@numProfile) > 0 THEN 1 ELSE 0 END)
					ELSE
						(CASE PO.tintCustomersBasedOn 
							WHEN 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numProId AND tintType=1 AND numDivisionID=@numDivisionID) > 0 THEN 1 ELSE 0 END)
							WHEN 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numProId AND tintType=2 AND numRelationship=@numRelationship AND numProfile=@numProfile) > 0 THEN 1 ELSE 0 END)
							WHEN 3 THEN 1
							ELSE 0
						END)
				END)
		AND 1 = (CASE 
					WHEN PO.tintOfferBasedOn = 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 AND numValue=@numItemCode) > 0 THEN 1 ELSE 0 END)
					WHEN PO.tintOfferBasedOn = 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=2 AND numValue=@numItemClassification) > 0 THEN 1 ELSE 0 END)
					WHEN PO.tintOfferBasedOn = 4 THEN 1 
					ELSE 0
				END)
	ORDER BY
		CASE 
			WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 1 THEN 1
			WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 1 THEN 2
			WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 2 THEN 3
			WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 2 THEN 4
			WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 3 THEN 5
			WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 3 THEN 6
		END
END
GO
/****** Object:  StoredProcedure [dbo].[USP_ShoppingCart]    Script Date: 05/07/2009 22:09:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_shoppingcart')
DROP PROCEDURE usp_shoppingcart
GO
CREATE PROCEDURE [dbo].[USP_ShoppingCart]                            
@numDivID as numeric(18),                            
@numOppID as numeric(18)=null output ,                                              
@vcPOppName as varchar(200)='' output,  
@numContactId as numeric(18),                            
@numDomainId as numeric(18),
@vcSource VARCHAR(50)=NULL,
@numCampainID NUMERIC(9)=0,
@numCurrencyID as numeric(9),
@numBillAddressId numeric(9),
@numShipAddressId numeric(9),
@bitDiscountType BIT,
@fltDiscount FLOAT,
@numProId NUMERIC(9),
@numSiteID NUMERIC(9),
@tintOppStatus INT,
@monShipCost DECIMAL(20,5),
@tintSource AS BIGINT ,
@tintSourceType AS TINYINT ,                          
@numPaymentMethodId NUMERIC(9),
@txtComments varchar(1000),
@numPartner NUMERIC(18,0),
@txtFuturePassword VARCHAR(50) = NULL,
@intUsedShippingCompany NUMERIC(18,0)=0,
@vcPartenerContact VARCHAR(200)='',
@numShipmentMethod NUMERIC(18,0)=0,
@vcOppRefOrderNo VARCHAR(200) = '',
@bitBillingTerms as bit,
@intBillingDays as integer,
@bitInterestType as bit,
@fltInterest as float,
@numOrderPromotionID NUMERIC(18,0),
@numOrderPromotionDiscountID NUMERIC(18,0)
as                            
BEGIN TRY
BEGIN TRANSACTION          
	DECLARE @CRMType AS INTEGER               
	DECLARE @numRecOwner AS INTEGER       
	DECLARE @numPartenerContact NUMERIC(18,0)=0
	DECLARE @bitAllocateInventoryOnPickList AS BIT
	DECLARE @numDiscountItemID NUMERIC(18,0)
	DECLARE @numRelationship NUMERIC(18,0) = 0
	DECLARE @numProfile NUMERIC(18,0) = 0
	
	SELECT 
		@numDiscountItemID=ISNULL(numDiscountServiceItemID,0) 
	FROM 
		Domain 
	WHERE 
		numDomainId=@numDomainId

	SET @numPartenerContact=(SELECT TOP 1 numContactId FROM AdditionalContactsInformation WHERE numDomainId=@numDomainID AND numDivisionId=@numPartner AND vcEmail=@vcPartenerContact)
	
	SELECT 
		@CRMType=tintCRMType
		,@numRecOwner=numRecOwner 
		,@bitAllocateInventoryOnPickList=ISNULL(bitAllocateInventoryOnPickList,0)
		,@numRelationship=ISNULL(numCompanyType,0)
		,@numProfile=ISNULL(vcProfile,0) 
	FROM 
		DivisionMaster
	INNER JOIN 
		CompanyInfo 
	ON 
		DivisionMaster.numCompanyID=CompanyInfo.numCompanyId
	WHERE 
		numDivisionID=@numDivID


	--VALIDATE PROMOTION OF COUPON CODE
	DECLARE @TEMP TABLE
	(
		ID INT IDENTITY(1,1),
		numCartID NUMERIC(18,0),
		numPromotionID NUMERIC(18,0)
	)

	INSERT INTO @TEMP
	(
		numCartID,
		numPromotionID
	)
	SELECT
		numCartId,
		PromotionID
	FROM
		CartItems 
	WHERE 
		numUserCntId=@numContactId
		AND ISNULL(PromotionID,0) > 0

	IF (SELECT COUNT(*) FROM @TEMP) > 0
	BEGIN
		-- IF ITEM PROMOTION USED IN ORDER EXIPIRES THAN RAISE ERROR
		IF  (SELECT 
					COUNT(PO.numProId)
				FROM 
					PromotionOffer PO
				LEFT JOIN
					PromotionOffer POOrder
				ON
					PO.numOrderPromotionID = POOrder.numProId
				INNER JOIN
					@TEMP T1
				ON
					PO.numProId = T1.numPromotionID
				WHERE 
					PO.numDomainId=@numDomainID 
					AND ISNULL(PO.IsOrderBasedPromotion,0) = 0
					AND ISNULL(PO.bitEnabled,0) = 1
					AND 1 = (CASE 
								WHEN ISNULL(PO.numOrderPromotionID,0) > 0 
								THEN (CASE WHEN POOrder.bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN GETUTCDATE()>=POOrder.dtValidFrom AND GETUTCDATE()<=POOrder.dtValidTo THEN 1 ELSE 0 END) END)
								ELSE (CASE WHEN PO.bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN GETUTCDATE()>=PO.dtValidFrom AND GETUTCDATE()<=PO.dtValidTo THEN 1 ELSE 0 END) END)
							END)
					AND 1 = (CASE 
								WHEN ISNULL(PO.numOrderPromotionID,0) > 0 
								THEN
									(CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numOrderPromotionID AND tintType=2 AND numRelationship=@numRelationship AND numProfile=@numProfile) > 0 THEN 1 ELSE 0 END)
								ELSE
									(CASE PO.tintCustomersBasedOn 
										WHEN 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numProId AND tintType=1 AND numDivisionID=@numDivID) > 0 THEN 1 ELSE 0 END)
										WHEN 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numProId AND tintType=2 AND numRelationship=@numRelationship AND numProfile=@numProfile) > 0 THEN 1 ELSE 0 END)
										WHEN 3 THEN 1
										ELSE 0
									END)
							END)
			) <> (SELECT COUNT(*) FROM @TEMP)
		BEGIN
			RAISERROR('ITEM_PROMOTION_EXPIRED',16,1)
			RETURN
		END

		-- NOW IF COUPON BASED ITEM PROMOTION IS USED THEN VALIDATE COUPON AND ITS USAGE
		IF 
		(
			SELECT 
				COUNT(*)
			FROM
				PromotionOffer PO
			INNER JOIN
				@TEMP T1
			ON
				PO.numProId = T1.numPromotionID
			WHERE
				PO.numDomainId = @numDomainId
				AND ISNULL(IsOrderBasedPromotion,0) = 0
				AND ISNULL(numOrderPromotionID,0) > 0 
		) = 1
		BEGIN
			IF ISNULL(@numOrderPromotionDiscountID,0) > 0
			BEGIN
				IF (SELECT 
						COUNT(*)
					FROM
						DiscountCodes DC
					WHERE
						Dc.numDiscountId = @numOrderPromotionDiscountID
					) = 0
				BEGIN
					RAISERROR('INVALID_COUPON_CODE_ITEM_PROMOTION',16,1)
					RETURN
				END
				ELSE
				BEGIN
					-- IF THERE IS COUPON CODE USAGE LIMIT THAN VALIDATE USAGE LIMIT
					IF (SELECT 
							COUNT(*)
						FROM
							PromotionOffer PO
						INNER JOIN 
							PromotionOffer POOrder
						ON
							PO.numOrderPromotionID = POOrder.numProId
						INNER JOIN
							@TEMP T1
						ON
							PO.numProId = T1.numPromotionID
						INNER JOIN
							DiscountCodes DC
						ON
							POOrder.numProId = DC.numPromotionID
						WHERE
							PO.numDomainId = @numDomainId
							AND ISNULL(PO.IsOrderBasedPromotion,0) = 0
							AND DC.numDiscountId = @numOrderPromotionDiscountID
							AND 1 = (CASE WHEN ISNULL(DC.CodeUsageLimit,0) <> 0 THEN 1 ELSE 0 END)
							AND ISNULL((SELECT intCodeUsed FROM DiscountCodeUsage WHERE DiscountCodeUsage.numDiscountId=DC.numDiscountId AND numDivisionId=@numDivID),0) >= DC.CodeUsageLimit) > 0
					BEGIN
						RAISERROR('ITEM_PROMOTION_COUPON_CODE_USAGE_LIMIT_EXCEEDED',16,1)
						RETURN
					END
				END
			END
			ELSE
			BEGIN
				RAISERROR('COUPON_CODE_REQUIRED_ITEM_PROMOTION',16,1)
				RETURN
			END
		END
	END

	IF ISNULL(@numOrderPromotionID,0) > 0
	BEGIN
		-- VALIDTE IF ITS ORDER BASED PROMOTION (BOTH ITEM AND ORDER BASED PROMOTION CAN HAVE COUPON CODE)
		IF EXISTS (SELECT numProId FROM PromotionOffer WHERE numDomainId=@numDomainId AND numProId=@numOrderPromotionID AND ISNULL(IsOrderBasedPromotion,0) = 1)
		BEGIN
			-- CHECK IF ORDER PROMOTION IS STILL VALID
			IF NOT EXISTS (SELECT 
								PO.numProId
							FROM 
								PromotionOffer PO
							INNER JOIN 
								PromotionOfferOrganizations PORG
							ON 
								PO.numProId = PORG.numProId
							WHERE 
								numDomainId=@numDomainID 
								AND PO.numProId=@numOrderPromotionID
								AND ISNULL(IsOrderBasedPromotion,0) = 1
								AND ISNULL(bitEnabled,0) = 1
								AND 1 = (CASE WHEN bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN GETUTCDATE()>=dtValidFrom AND GETUTCDATE()<=dtValidTo THEN 1 ELSE 0 END) END)
								AND numRelationship=@numRelationship 
								AND numProfile=@numProfile
			)
			BEGIN
				RAISERROR('ORDER_PROMOTION_EXPIRED',16,1)
				RETURN
			END
			ELSE IF ISNULL((SELECT ISNULL(bitRequireCouponCode,0) FROM PromotionOffer WHERE numDomainId=@numDomainId AND numProId=@numOrderPromotionID),0) = 1
			BEGIN
				IF ISNULL(@numOrderPromotionDiscountID,0) > 0
				BEGIN
					IF (SELECT 
							COUNT(*)
						FROM
							PromotionOffer PO
						INNER JOIN
							DiscountCodes DC
						ON
							PO.numProId = DC.numPromotionID
						WHERE
							PO.numDomainId = @numDomainId
							AND PO.numProId = @numOrderPromotionID
							AND ISNULL(IsOrderBasedPromotion,0) = 1
							AND ISNULL(bitRequireCouponCode,0) = 1
							AND ISNULL(DC.numDiscountId,0) = @numOrderPromotionDiscountID) = 0
					BEGIN
						RAISERROR('INVALID_COUPON_CODE_ORDER_PROMOTION',16,1)
						RETURN
					END
					ELSE
					BEGIN
						-- IF THERE IS COUPON CODE USAGE LIMIT THAN VALIDATE USAGE LIMIT
						IF (SELECT 
								COUNT(*)
							FROM
								PromotionOffer PO
							INNER JOIN
								DiscountCodes DC
							ON
								PO.numProId = DC.numPromotionID
							WHERE
								PO.numDomainId = @numDomainId
								AND PO.numProId = @numOrderPromotionID
								AND ISNULL(IsOrderBasedPromotion,0) = 1
								AND ISNULL(bitRequireCouponCode,0) = 1
								AND ISNULL(DC.numDiscountId,0) = @numOrderPromotionDiscountID
								AND 1 = (CASE WHEN ISNULL(DC.CodeUsageLimit,0) <> 0 THEN 1 ELSE 0 END)
								AND ISNULL((SELECT intCodeUsed FROM DiscountCodeUsage WHERE DiscountCodeUsage.numDiscountId=DC.numDiscountId AND numDivisionId=@numDivID),0) >= DC.CodeUsageLimit) > 0
						BEGIN
							RAISERROR('ORDER_PROMOTION_COUPON_CODE_USAGE_LIMIT_EXCEEDED',16,1)
							RETURN
						END
						ELSE
						BEGIN	
							IF EXISTS (SELECT DC.numDiscountID FROM DiscountCodes DC INNER JOIN DiscountCodeUsage DCU ON DC.numDiscountID=DCU.numDIscountID WHERE DC.numPromotionID=@numOrderPromotionID AND DCU.numDivisionID=@numDivID AND DC.numDiscountId=@numOrderPromotionDiscountID)
							BEGIN
								UPDATE
									DCU
								SET
									DCU.intCodeUsed = ISNULL(DCU.intCodeUsed,0) + 1
								FROM 
									DiscountCodes DC 
								INNER JOIN 
									DiscountCodeUsage DCU 
								ON 
									DC.numDiscountID=DCU.numDIscountID 
								WHERE 
									DC.numPromotionID=@numOrderPromotionID 
									AND DCU.numDivisionID=@numDivID 
									AND DC.numDiscountId=@numOrderPromotionDiscountID
							END
							ELSE
							BEGIN
								INSERT INTO DiscountCodeUsage
								(
									numDiscountId
									,numDivisionId
									,intCodeUsed
								)
								SELECT 
									DC.numDiscountId
									,@numDivID
									,1
								FROM
									DiscountCodes DC
								WHERE
									DC.numPromotionID=@numOrderPromotionID
									AND DC.numDiscountId=@numOrderPromotionDiscountID
							END
						END
					END
				END
				ELSE
				BEGIN
					RAISERROR('COUPON_CODE_REQUIRED_ORDER_PROMOTION',16,1)
					RETURN
				END
			END
		END
	END
	ELSE
	BEGIN
		-- IF PROMOTION ID NOT AVAILABLE THAN REMOVE ORDER PROMOTION DISCOUNT ITEM
		DELETE FROM CartItems WHERE numUserCntId =@numContactId AND numItemCode=@numDiscountItemID
	END                        
		
	IF @CRMType= 0                             
	BEGIN
		UPDATE 
			DivisionMaster                               
		SET 
			tintCRMType=1                              
		WHERE 
			numDivisionID=@numDivID
	END                            

	DECLARE @TotalAmount AS FLOAT
       

	IF LEN(ISNULL(@txtFuturePassword,'')) > 0
	BEGIN
		DECLARE @numExtranetID NUMERIC(18,0)

		SELECT @numExtranetID=numExtranetID FROM ExtarnetAccounts WHERE numDivisionID=@numDivID

		UPDATE ExtranetAccountsDtl SET vcPassword=@txtFuturePassword WHERE numExtranetID=@numExtranetID
	END                      

	DECLARE @numTemplateID NUMERIC
	SELECT 
		@numTemplateID= numMirrorBizDocTemplateId 
	FROM 
		eCommercePaymentConfig 
	WHERE 
		numSiteId=@numSiteID 
		AND numPaymentMethodId = @numPaymentMethodId 
		AND numDomainID = @numDomainID   

	DECLARE @fltExchangeRate FLOAT                                 
    
	IF @numCurrencyID=0 
	BEGIN
		SELECT 
			@numCurrencyID=ISNULL(numCurrencyID,0) 
		FROM 
			Domain 
		WHERE 
			numDomainID=@numDomainId                            
	END
		
	IF @numCurrencyID=0 
	BEGIN
		SET @fltExchangeRate=1
	END
	ELSE 
	BEGIN
		SET @fltExchangeRate=dbo.GetExchangeRate(@numDomainID,@numCurrencyID)                          
	END                      
                           
	DECLARE @numAccountClass AS NUMERIC(18) = 0
	DECLARE @tintDefaultClassType AS INT = 0
	DECLARE @tintCommitAllocation TINYINT

	SELECT 
		@tintDefaultClassType = ISNULL(tintDefaultClassType,0) 
		,@tintCommitAllocation=ISNULL(tintCommitAllocation,1)
	FROM 
		Domain 
	WHERE 
		numDomainId = @numDomainID 

	IF @tintDefaultClassType <> 0
	BEGIN
		SELECT 
			@numAccountClass=ISNULL(numDefaultClass,0) 
		FROM 
			dbo.UserMaster UM 
		JOIN 
			dbo.Domain D 
		ON 
			UM.numDomainID=D.numDomainId
		WHERE 
			D.numDomainId=@numDomainId 
			AND UM.numUserDetailId=@numContactId
	END
	ELSE IF @tintDefaultClassType = 2 --COMPANY
	BEGIN
		SELECT 
			@numAccountClass=ISNULL(numAccountClassID,0) 
		FROM 
			dbo.DivisionMaster DM 
		WHERE 
			DM.numDomainId=@numDomainId 
			AND DM.numDivisionID=@numDivID
	END
	ELSE
	BEGIN
		SET @numAccountClass = 0
	END

	SET @numAccountClass=(SELECT TOP 1 numDefaultClass FROM dbo.eCommerceDTL WHERE numSiteId=@numSiteID AND numDomainId=@numDomainId)                  

	INSERT INTO OpportunityMaster
	(                              
		numContactId,numDivisionId,txtComments,numCampainID,bitPublicFlag,tintSource,tintSourceType,vcPOppName,monPAmount,                              
		numCreatedBy,bintCreatedDate,numModifiedBy,bintModifiedDate,numDomainId,tintOppType,tintOppStatus,intPEstimatedCloseDate,              
		numRecOwner,bitOrder,numCurrencyID,fltExchangeRate,fltDiscountTotal,bitDiscountTypeTotal,monShipCost,numOppBizDocTempID
		,numAccountClass,numPartner,intUsedShippingCompany,numPartenerContact,numShippingService,dtReleaseDate,vcOppRefOrderNo,
		bitBillingTerms,intBillingDays,bitInterestType,fltInterest,numDiscountID
	)                              
	VALUES                              
	(                              
		@numContactId,@numDivID,@txtComments,@numCampainID,0,@tintSource,@tintSourceType,ISNULL(@vcPOppName,'SO'),0,                                
		@numContactId,GETUTCDATE(),@numContactId,GETUTCDATE(),@numDomainId,1,@tintOppStatus,GETUTCDATE(),@numRecOwner,
		1,@numCurrencyID,@fltExchangeRate,@fltDiscount,@bitDiscountType,@monShipCost,@numTemplateID,@numAccountClass,@numPartner
		,@intUsedShippingCompany,@numPartenerContact,@numShipmentMethod,GETDATE(),@vcOppRefOrderNo,@bitBillingTerms,@intBillingDays,
		@bitInterestType,@fltInterest,@numOrderPromotionDiscountID
	)
		
	SET @numOppID=scope_identity()                             

	EXEC dbo.USP_UpdateNameTemplateValue 1,@numDomainId,@numOppID
	SELECT @vcPOppName = vcPOppName FROM OpportunityMaster WHERE numOppID= @numOppID

	INSERT INTO OpportunityItems
	(
		numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,vcAttributes,vcAttrValues,[vcItemName],
		[vcModelID],[vcManufacturer],vcPathForTImage, monVendorCost,numUOMId,bitDiscountType,fltDiscount,monTotAmtBefDiscount,numPromotionID,
		bitPromotionTriggered,bitDropShip,vcPromotionDetail,vcChildKitSelectedItems
	)                                                                                                              
	SELECT 
		@numOppID,X.numItemCode,x.numUnitHour * x.decUOMConversionFactor,x.monPrice/x.decUOMConversionFactor,x.monTotAmount,X.vcItemDesc,
		NULLIF(X.numWarehouseItmsID,0),X.vcItemType,X.vcAttributes,X.vcAttrValues,(SELECT vcItemName FROM item WHERE numItemCode = X.numItemCode),
		(SELECT vcModelID FROM item WHERE numItemCode = X.numItemCode),(SELECT vcManufacturer FROM item WHERE numItemCode = X.numItemCode),
		(SELECT TOP 1  vcPathForTImage FROM dbo.ItemImages WHERE numItemCode = X.numItemCode AND bitDefault = 1 AND numDomainId = @numDomainId),
		(SELECT ISNULL(VN.monCost,0) FROM Vendor VN INNER JOIN Item IT ON IT.numItemCode=VN.numItemCode and VN.numVendorID=IT.numVendorID WHERE VN.numItemCode=X.numItemCode), 
		numUOM,bitDiscountType,fltDiscount,monTotAmtBefDiscount,X.PromotionID,X.bitPromotionTrigerred
		,ISNULL((SELECT bitAllowDropShip FROM Item WHERE numItemCode=X.numItemCode),0),x.PromotionDesc,ISNULL(vcChildKitItemSelection,'')
	FROM 
		dbo.CartItems X 
	WHERE 
		numUserCntId =@numContactId


	--INSERT KIT ITEMS 
	IF (SELECT 
			COUNT(*) 
		FROM 
			OpportunityItems OI 
		LEFT JOIN
			WarehouseItems WI
		ON
			OI.numWarehouseItmsID = WI.numWarehouseItemID
		LEFT JOIN
			Warehouses W
		ON
			WI.numWarehouseID = W.numWarehouseID
		JOIN 
			ItemDetails ID 
		ON 
			OI.numItemCode=ID.numItemKitID 
		JOIN
			Item I
		ON
			ID.numChildItemID = I.numItemCode
		WHERE 
			OI.numOppId=@numOppId  
			AND ISNULL(OI.bitDropShip,0) = 0
			AND ISNULL(OI.numWarehouseItmsID,0) > 0
			AND I.charItemType = 'P'
			AND ISNULL((SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ID.numChildItemID AND numWareHouseID = W.numWarehouseID),0) = 0) > 0
	BEGIN
		RAISERROR('SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS',16,1)
	END

			                
	INSERT INTO OpportunityKitItems
	(
		numOppId,
		numOppItemID,
		numChildItemID,
		numWareHouseItemId,
		numQtyItemsReq,
		numQtyItemsReq_Orig,
		numUOMId,
		numQtyShipped,
		monAvgCost
	)
	SELECT
		@numOppId,
		OI.numoppitemtCode,
		ID.numChildItemID,
		(SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ID.numChildItemID AND WareHouseItems.numWareHouseID = W.numWarehouseID ORDER BY numWareHouseItemID),
		(ID.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(ID.numUOMId,ID.numChildItemID,@numDomainID,I.numBaseUnit),1)) * OI.numUnitHour,
		ID.numQtyItemsReq,
		ID.numUOMId,
		0,
		ISNULL(I.monAverageCost,0)
	FROM 
		OpportunityItems OI 
	LEFT JOIN
		WarehouseItems WI
	ON
		OI.numWarehouseItmsID = WI.numWarehouseItemID
	LEFT JOIN
		Warehouses W
	ON
		WI.numWarehouseID = W.numWarehouseID
	JOIN 
		ItemDetails ID 
	ON 
		OI.numItemCode=ID.numItemKitID 
	JOIN
		Item I
	ON
		ID.numChildItemID = I.numItemCode
	WHERE 
		OI.numOppId=@numOppId 

	--INSERT KIT ITEM HAS ANOTHER KIT AS CHILD THEN SAVE CONFIGURATION OF KIT
	DECLARE @TempKitConfiguration TABLE
	(
		numOppItemID NUMERIC(18,0),
		numItemCode NUMERIC(18,0),
		KitChildItems VARCHAR(MAX)
	)

	INSERT INTO 
		@TempKitConfiguration
	SELECT
		numoppitemtCode,
		numItemCode,
		vcChildKitSelectedItems
	FROM
		OpportunityItems
	WHERE
		numOppId = @numOppID
		AND LEN(ISNULL(vcChildKitSelectedItems,'')) > 0
					

	IF (SELECT 
			COUNT(*) 
		FROM 
		(
			SELECT 
				t1.numOppItemID
				,t1.numItemCode
				,SUBSTRING(items,0,CHARINDEX('-',items)) AS numKitItemID
				,SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)) As numKitChildItemID
			FROM 
				@TempKitConfiguration t1
			CROSS APPLY
			(
				SELECT
					*
				FROM 
					dbo.Split((SELECT KitChildItems FROM @TempKitConfiguration t3 WHERE t3.numOppItemID=t1.numOppItemID AND t3.numItemCode=t1.numItemCode),',') 
			) as t2
		) TempChildItems
		INNER JOIN
			OpportunityKitItems OKI
		ON
			OKI.numOppId=@numOppId
			AND OKI.numOppItemID=TempChildItems.numOppItemID
			AND OKI.numChildItemID=TempChildItems.numKitItemID
		INNER JOIN
			OpportunityItems OI
		ON
			OI.numItemCode = TempChildItems.numItemCode
			AND OI.numoppitemtcode = OKI.numOppItemID
		LEFT JOIN
			WarehouseItems WI
		ON
			OI.numWarehouseItmsID = WI.numWarehouseItemID
		LEFT JOIN
			Warehouses W
		ON
			WI.numWarehouseID = W.numWarehouseID
		INNER JOIN
			ItemDetails
		ON
			TempChildItems.numKitItemID = ItemDetails.numItemKitID
			AND TempChildItems.numKitChildItemID = ItemDetails.numChildItemID
		INNER JOIN
			Item 
		ON
			ItemDetails.numChildItemID = Item.numItemCode
		WHERE
			Item.charItemType = 'P'
			AND ISNULL((SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ItemDetails.numChildItemID AND WareHouseItems.numWareHouseID = W.numWarehouseID),0) = 0) > 0
	BEGIN
		RAISERROR('SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS',16,1)
	END

	INSERT INTO OpportunityKitChildItems
	(
		numOppID,
		numOppItemID,
		numOppChildItemID,
		numItemID,
		numWareHouseItemId,
		numQtyItemsReq,
		numQtyItemsReq_Orig,
		numUOMId,
		numQtyShipped,
		monAvgCost
	)
	SELECT 
		@numOppID,
		OKI.numOppItemID,
		OKI.numOppChildItemID,
		ItemDetails.numChildItemID,
		(SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ItemDetails.numChildItemID AND WareHouseItems.numWareHouseID = W.numWarehouseID ORDER BY numWareHouseItemID),
		(ItemDetails.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(ItemDetails.numUOMId,ItemDetails.numChildItemID,@numDomainID,Item.numBaseUnit),1)) * OKI.numQtyItemsReq,
		ItemDetails.numQtyItemsReq,
		ItemDetails.numUOMId,
		0,
		ISNULL(Item.monAverageCost,0)
	FROM
	(
		SELECT 
			t1.numOppItemID
			,t1.numItemCode
			,SUBSTRING(items,0,CHARINDEX('-',items)) AS numKitItemID
			,SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)) As numKitChildItemID
		FROM 
			@TempKitConfiguration t1
		CROSS APPLY
		(
			SELECT
				*
			FROM 
				dbo.Split((SELECT KitChildItems FROM @TempKitConfiguration t3 WHERE t3.numOppItemID=t1.numOppItemID AND t3.numItemCode=t1.numItemCode),',') 
		) as t2
	) TempChildItems
	INNER JOIN
		OpportunityKitItems OKI
	ON
		OKI.numOppId=@numOppId
		AND OKI.numOppItemID=TempChildItems.numOppItemID
		AND OKI.numChildItemID=TempChildItems.numKitItemID
	INNER JOIN
		OpportunityItems OI
	ON
		OI.numItemCode = TempChildItems.numItemCode
		AND OI.numoppitemtcode = OKI.numOppItemID
	LEFT JOIN
		WarehouseItems WI
	ON
		OI.numWarehouseItmsID = WI.numWarehouseItemID
	LEFT JOIN
		Warehouses W
	ON
		WI.numWarehouseID = W.numWarehouseID
	INNER JOIN
		ItemDetails
	ON
		TempChildItems.numKitItemID = ItemDetails.numItemKitID
		AND TempChildItems.numKitChildItemID = ItemDetails.numChildItemID
	INNER JOIN
		Item 
	ON
		ItemDetails.numChildItemID = Item.numItemCode

	IF NOT (@tintCommitAllocation=2 AND @bitAllocateInventoryOnPickList=1)
	BEGIN
		EXEC USP_ManageSOWorkOrder @numDomainId,@numOppID,'',@numContactId
	END


	BEGIN TRY
		DECLARE @TEMPContainer TABLE
		(
			numConainer NUMERIC(18,0)
			,numNoItemIntoContainer NUMERIC(18,0)
			,numWarehouseID NUMERIC(18,0)
			,monPrice DECIMAL(20,5)
			,monAverageCost DECIMAL(20,5)
			,numQtyToFit FLOAT
			,vcItemName VARCHAR(300)
			,vcItemDesc VARCHAR(1000)
			,vcModelID VARCHAR(300)
		)

		INSERT INTO @TEMPContainer
		(
			numConainer
			,numNoItemIntoContainer
			,numWarehouseID
			,monPrice
			,monAverageCost
			,numQtyToFit
			,vcItemName
			,vcItemDesc
			,vcModelID
		)
		SELECT
			I.numContainer
			,I.numNoItemIntoContainer
			,WI.numWarehouseID
			,ISNULL(IContainer.monListPrice,0)
			,ISNULL(IContainer.monAverageCost,0)
			,SUM(numUnitHour)
			,ISNULL(IContainer.vcItemName,'')
			,ISNULL(IContainer.txtItemDesc,'')
			,ISNULL(IContainer.vcModelID,'')
		FROM
			OpportunityItems OI
		INNER JOIN
			Item I
		ON
			OI.numItemCode = I.numItemCode
		INNER JOIN
			Item IContainer
		ON
			I.numContainer = IContainer.numItemCode
		LEFT JOIN
			WareHouseItems WI
		ON
			OI.numWarehouseItmsID = WI.numWareHouseItemID
		WHERE
			numOppID = @numOppID
			AND ISNULL(OI.bitDropShip,0) = 0
			AND ISNULL(I.bitContainer,0) = 0
			AND ISNULL(IContainer.bitContainer,0) = 1
			AND ISNULL(I.numContainer,0) > 0
			AND ISNULL(I.numNoItemIntoContainer,0) > 0
		GROUP BY
			I.numContainer
			,I.numNoItemIntoContainer
			,IContainer.monListPrice
			,IContainer.monAverageCost
			,WI.numWarehouseID
			,IContainer.vcItemName
			,IContainer.txtItemDesc
			,IContainer.vcModelID

		INSERT INTO OpportunityItems
		(
			numOppId
			,numItemCode
			,numWarehouseItmsID
			,numUnitHour
			,monPrice
			,fltDiscount
			,bitDiscountType
			,monTotAmount
			,monTotAmtBefDiscount
			,monAvgCost
			,vcItemName
			,vcItemDesc
			,vcModelID
		)
		SELECT
			@numOppID
			,numConainer
			,(SELECT TOP 1 numWarehouseItemID FROM WarehouseItems WHERE numItemID=numConainer AND WarehouseItems.numWarehouseID=T1.numWarehouseID ORDER BY numWareHouseItemID)
			,(CASE WHEN numQtyToFit > numNoItemIntoContainer THEN CEILING(numQtyToFit/numNoItemIntoContainer) ELSE 1 END)
			,monPrice
			,0
			,1
			,(CASE WHEN numQtyToFit > numNoItemIntoContainer THEN CEILING(numQtyToFit/numNoItemIntoContainer) ELSE 1 END) * monPrice
			,(CASE WHEN numQtyToFit > numNoItemIntoContainer THEN CEILING(numQtyToFit/numNoItemIntoContainer) ELSE 1 END) * monPrice
			,monAverageCost
			,vcItemName
			,vcItemDesc
			,vcModelID
		FROM
			@TEMPContainer T1

		DECLARE @tintShipped AS TINYINT      
		DECLARE @tintOppType AS TINYINT
	
		SELECT 
			@tintOppStatus=tintOppStatus
			,@tintOppType=tintOppType
			,@tintShipped=tintShipped 
		FROM 
			OpportunityMaster 
		WHERE 
			numOppID=@numOppID
	END TRY
	BEGIN CATCH
		-- DO NOT RAISE ERROR
	END CATCH           
	
	IF @tintOppStatus=1               
	BEGIN         
		EXEC USP_UpdatingInventoryonCloseDeal @numOppID,@numContactId
	END                                   
                  
	SELECT
		@TotalAmount=SUM(monTotAmount)
	FROM 
		OpportunityItems 
	WHERE 
		numOppId=@numOppID                            
 
	UPDATE 
		OpportunityMaster 
	SET 
		monPamount = @TotalAmount - @fltDiscount                           
	WHERE 
		numOppId=@numOppID                           
                                               
	IF(@vcSource IS NOT NULL)
	BEGIN
		insert into OpportunityLinking ([numParentOppID],[numChildOppID],[vcSource],[numParentOppBizDocID],numPromotionId,numSiteID)  values(null,@numOppID,@vcSource,NULL,@numProId,@numSiteID);
	END

	--Add/Update Address Details
	DECLARE @vcStreet  varchar(100),@vcCity  varchar(50),@vcPostalCode  varchar(15),@vcCompanyName  varchar(100),@vcAddressName VARCHAR(50), @vcAltContact VARCHAR(200)     
	DECLARE @numState  numeric(9),@numCountry  numeric(9), @numCompanyId  numeric(9), @numContact NUMERIC(18,0)
	DECLARE @bitIsPrimary BIT, @bitAltContact BIT;

	--Bill Address
	IF @numBillAddressId>0
	BEGIN
		SELECT
			@vcStreet=ISNULL(vcStreet,'')
			,@vcCity=ISNULL(vcCity,'')
			,@vcPostalCode=ISNULL(vcPostalCode,'')
			,@numState=ISNULL(numState,0)
			,@numCountry=ISNULL(numCountry,0)
			,@bitIsPrimary=bitIsPrimary
			,@vcAddressName=vcAddressName
			,@numContact=numContact
			,@bitAltContact=bitAltContact
			,@vcAltContact=vcAltContact
		FROM 
			dbo.AddressDetails 
		WHERE 
			numDomainID = @numDomainID 
			AND numAddressID = @numBillAddressId 
            
  
  		EXEC dbo.USP_UpdateOppAddress
		@numOppID = @numOppID, --  numeric(9, 0)
		@byteMode = 0, --  tinyint
		@vcStreet = @vcStreet, --  varchar(100)
		@vcCity = @vcCity, --  varchar(50)
		@vcPostalCode = @vcPostalCode, --  varchar(15)
		@numState = @numState, --  numeric(9, 0)
		@numCountry = @numCountry, --  numeric(9, 0)
		@vcCompanyName = '', --  varchar(100)
		@numCompanyId = 0, --  numeric(9, 0)
		@vcAddressName =@vcAddressName,
		@bitCalledFromProcedure=1,
		@numContact = @numContact,
		@bitAltContact = @bitAltContact,
		@vcAltContact = @vcAltContact
        
	END
  
  
	--Ship Address
	IF @numShipAddressId>0
	BEGIN
   		SELECT 
			@vcStreet=ISNULL(vcStreet,'')
			,@vcCity=ISNULL(vcCity,'')
			,@vcPostalCode=ISNULL(vcPostalCode,'')
			,@numState=ISNULL(numState,0)
			,@numCountry=ISNULL(numCountry,0)
			,@bitIsPrimary=bitIsPrimary
			,@vcAddressName=vcAddressName
			,@numContact=numContact
			,@bitAltContact=bitAltContact
			,@vcAltContact=vcAltContact
		FROM 
			dbo.AddressDetails 
		WHERE 
			numDomainID = @numDomainID 
			AND numAddressID = @numShipAddressId 
  
		select  @vcCompanyName=vcCompanyName,@numCompanyId=div.numCompanyID from CompanyInfo Com                            
		join divisionMaster Div                            
		on div.numCompanyID=com.numCompanyID                            
		where div.numdivisionID=@numDivID

  		EXEC dbo.USP_UpdateOppAddress
		@numOppID = @numOppID, --  numeric(9, 0)
		@byteMode = 1, --  tinyint
		@vcStreet = @vcStreet, --  varchar(100)
		@vcCity = @vcCity, --  varchar(50)
		@vcPostalCode = @vcPostalCode, --  varchar(15)
		@numState = @numState, --  numeric(9, 0)
		@numCountry = @numCountry, --  numeric(9, 0)
		@vcCompanyName = @vcCompanyName, --  varchar(100)
		@numCompanyId =@numCompanyId, --  numeric(9, 0)
		@vcAddressName =@vcAddressName,
		@bitCalledFromProcedure=1,
		@numContact = @numContact,
		@bitAltContact = @bitAltContact,
		@vcAltContact = @vcAltContact
   	
	END

	-- IMPORTANT: KEEP THIS BELOW ADDRESS UPDATE LOGIC
	--Insert Tax for Division                       
	INSERT dbo.OpportunityMasterTaxItems 
	(
		numOppId,
		numTaxItemID,
		fltPercentage,
		tintTaxType,
		numTaxID
	) 
	SELECT 
		@numOppID,
		TI.numTaxItemID,
		TEMPTax.decTaxValue,
		TEMPTax.tintTaxType,
		0
	FROM 
		TaxItems TI 
	JOIN 
		DivisionTaxTypes DTT 
	ON 
		TI.numTaxItemID = DTT.numTaxItemID
	CROSS APPLY
	(
		SELECT
			decTaxValue,
			tintTaxType,
			numTaxID
		FROM
			dbo.fn_CalItemTaxAmt(@numDomainID,@numDivID,0,TI.numTaxItemID,@numOppId,0,NULL)
	) AS TEMPTax
	WHERE 
		DTT.numDivisionID=@numDivID AND DTT.bitApplicable=1
	UNION 
	SELECT 
		@numOppID,
		0,
		TEMPTax.decTaxValue,
		TEMPTax.tintTaxType,
		0
	FROM 
		dbo.DivisionMaster 
	CROSS APPLY
	(
		SELECT
			decTaxValue,
			tintTaxType,
			numTaxID
		FROM
			dbo.fn_CalItemTaxAmt(@numDomainID,@numDivID,0,0,@numOppId,1,NULL)
	) AS TEMPTax
	WHERE 
		bitNoTax=0 
		AND numDivisionID=@numDivID
	UNION 
	SELECT
		@numOppId
		,1
		,decTaxValue
		,tintTaxType
		,numTaxID
	FROM
		dbo.fn_CalItemTaxAmt(@numDomainID,@numDivID,0,1,@numOppId,1,NULL)
  
	--Delete Tax for Opportunity Items if item deleted 
	DELETE FROM OpportunityItemsTaxItems WHERE numOppId=@numOppId AND numOppItemID NOT IN (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID)

	--Insert Tax for Opportunity Items
	INSERT INTO dbo.OpportunityItemsTaxItems 
	(
		numOppId,
		numOppItemID,
		numTaxItemID,
		numTaxID
	) 
	SELECT 
		@numOppId,
		OI.numoppitemtCode,
		TI.numTaxItemID,
		0 
	FROM 
		dbo.OpportunityItems OI 
	JOIN 
		dbo.ItemTax IT 
	ON 
		OI.numItemCode=IT.numItemCode 
	JOIN
		TaxItems TI 
	ON 
		TI.numTaxItemID=IT.numTaxItemID 
	WHERE 
		OI.numOppId=@numOppID 
		AND IT.bitApplicable=1 
		AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
	UNION
	SELECT 
		@numOppId,
		OI.numoppitemtCode,
		0,
		0 
	FROM 
		dbo.OpportunityItems OI 
	JOIN 
		dbo.Item I 
	ON 
		OI.numItemCode=I.numItemCode
	WHERE 
		OI.numOppId=@numOppID 
		AND I.bitTaxable=1 
		AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
	UNION
	SELECT
		@numOppId,
		OI.numoppitemtCode,
		1,
		TD.numTaxID
	FROM
		dbo.OpportunityItems OI 
	INNER JOIN
		ItemTax IT
	ON
		IT.numItemCode = OI.numItemCode
	INNER JOIN
		TaxDetails TD
	ON
		TD.numTaxID = IT.numTaxID
		AND TD.numDomainId = @numDomainId
	WHERE
		OI.numOppId = @numOppID
		AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)

COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

  -- Whoops, there was an error
  IF @@TRANCOUNT > 0
     ROLLBACK

  SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH

/****** Object:  StoredProcedure [dbo].[USP_UpdateCartItem]    Script Date: 11/08/2011 17:49:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_UpdateCartItem' ) 
                    DROP PROCEDURE USP_UpdateCartItem
                    Go
CREATE  PROCEDURE [dbo].[USP_UpdateCartItem]
(
	@numUserCntId numeric(9,0),
	@numDomainId numeric(9,0),
	@vcCookieId varchar(100),
	@strXML text,
	@postselldiscount INT=0,
	@numSiteID NUMERIC(18,0),
	@vcPreferredPromotions VARCHAR(MAX)=''
)
AS
BEGIN
	IF convert(varchar(10),@strXML) <>''
	BEGIN
		DECLARE @i INT
		EXEC sp_xml_preparedocument @i OUTPUT, @strXML 
		
		IF @numUserCntId <> 0
		BEGIN
			UPDATE 
				CartItems
			SET 
				CartItems.monPrice = ox.monPrice
				,CartItems.numUnitHour = ox.numUnitHour
				,CartItems.monTotAmtBefDiscount = ox.monTotAmtBefDiscount
				,CartItems.monTotAmount = ox.monTotAmount
				,CartItems.bitDiscountType=ISNULL(ox.bitDiscountType,0)
				,CartItems.vcShippingMethod=ox.vcShippingMethod
				,CartItems.numServiceTypeID=ox.numServiceTypeID
				,CartItems.decShippingCharge=ox.decShippingCharge
				,CartItems.tintServiceType=ox.tintServiceType
				,CartItems.numShippingCompany =ox.numShippingCompany
				,CartItems.dtDeliveryDate =ox.dtDeliveryDate
				,CartItems.fltDiscount =ox.fltDiscount
				,CartItems.vcCookieId = ox.vcCookieId
				,CartItems.vcCoupon=(CASE WHEN CartItems.vcCoupon<>'' THEN CartItems.vcCoupon ELSE ox.vcCoupon END)
			FROM 
				OPENXML(@i, '/NewDataSet/Table1',2)
			WITH 
			(
				numDomainId NUMERIC(9,0),numUserCntId NUMERIC(9,0),vcCookieId VARCHAR(200), numCartID NUMERIC(18,0),numItemCode NUMERIC(9,0), monPrice DECIMAL(20,5), numUnitHour FLOAT,
				monTotAmtBefDiscount DECIMAL(20,5),monTotAmount DECIMAL(20,5) ,bitDiscountType BIT,vcShippingMethod VARCHAR(200),numServiceTypeID NUMERIC(9,0),decShippingCharge DECIMAL(20,5),
				tintServiceType TINYINT,numShippingCompany NUMERIC(9,0),dtDeliveryDate DATETIME,fltDiscount DECIMAL(18,2),vcCoupon VARCHAR(200) 
			) ox
			WHERE 
				CartItems.numDomainId = ox.numDomainId
				AND CartItems.numUserCntId = ox.numUserCntId
				AND CartItems.numItemCode = ox.numItemCode
				AND CartItems.numCartId = ox.numCartID
		END 
		ELSE
		BEGIN
			UPDATE 
				CartItems
			SET 
				CartItems.monPrice = ox.monPrice
				,CartItems.numUnitHour = ox.numUnitHour
				,CartItems.monTotAmtBefDiscount = ISNULL(ox.numUnitHour,0) * ISNULL(decUOMConversionFactor,1) * ISNULL(ox.monPrice,0)
				,CartItems.monTotAmount = (CASE 
												WHEN ISNULL(ox.fltDiscount,0) > 0
												THEN (CASE 
														WHEN ISNULL(ox.bitDiscountType,0) = 1 
														THEN (CASE 
																WHEN (ISNULL(ox.numUnitHour,0) * ISNULL(decUOMConversionFactor,1) * ISNULL(ox.monPrice,0)) - ox.fltDiscount >= 0
																THEN (ISNULL(ox.numUnitHour,0) * ISNULL(decUOMConversionFactor,1) * ISNULL(ox.monPrice,0)) - ox.fltDiscount
																ELSE 0
															END)
														ELSE (ISNULL(ox.numUnitHour,0) * ISNULL(decUOMConversionFactor,1)) * (ISNULL(ox.monPrice,0) - (ISNULL(ox.monPrice,0) * (ox.fltDiscount/100)))
													END)
												ELSE ISNULL(ox.numUnitHour,0) * ISNULL(decUOMConversionFactor,1) * ISNULL(ox.monPrice,0)
											END)
				,CartItems.bitDiscountType =ISNULL(ox.bitDiscountType,0)
				,CartItems.vcShippingMethod =ox.vcShippingMethod
				,CartItems.numServiceTypeID =ox.numServiceTypeID
				,CartItems.decShippingCharge =ox.decShippingCharge
				,CartItems.tintServiceType =ox.tintServiceType
				,CartItems.numShippingCompany =ox.numShippingCompany
				,CartItems.dtDeliveryDate =ox.dtDeliveryDate
				,CartItems.fltDiscount =ox.fltDiscount
				,CartItems.vcCoupon=(CASE WHEN CartItems.vcCoupon<>'' THEN CartItems.vcCoupon ELSE ox.vcCoupon END)
			FROM 
				OpenXml(@i, '/NewDataSet/Table1',2)
			WITH 
			(
				numDomainId NUMERIC(9,0),numUserCntId NUMERIC(9,0),vcCookieId VARCHAR(200), numCartID NUMERIC(18,0),numItemCode NUMERIC(9,0), monPrice DECIMAL(20,5), numUnitHour FLOAT,
				monTotAmtBefDiscount DECIMAL(20,5),monTotAmount DECIMAL(20,5) ,bitDiscountType BIT,vcShippingMethod VARCHAR(200),numServiceTypeID NUMERIC(9,0),decShippingCharge DECIMAL(20,5),
				tintServiceType TINYINT,numShippingCompany NUMERIC(9,0),dtDeliveryDate DATETIME,fltDiscount DECIMAL(18,2),vcCoupon VARCHAR(200) 
			) ox
			WHERE 
				CartItems.numDomainId = ox.numDomainId 
				AND CartItems.numUserCntId = 0 
				AND CartItems.numItemCode = ox.numItemCode
				AND CartItems.vcCookieId = ox.vcCookieId
				AND CartItems.numCartId = ox.numCartID
		END

		exec sp_xml_removedocument @i


		IF (SELECT COUNT(*) FROM PromotionOffer WHERE numDomainId=@numDomainId AND ISNULL(bitEnabled,0)=1) > 0
		BEGIN
			DECLARE @numPreferredPromotionID NUMERIC(18,0)
			DECLARE @numPromotionID NUMERIC(18,0)
			DECLARE @vcPromotionDescription VARCHAR(300)
			DECLARE @numItemClassification AS NUMERIC(18,0)
			DECLARE @numDivisionID NUMERIC(18,0)
			DECLARE @numRelationship NUMERIC(18,0)
			DECLARE @numProfile NUMERIC(18,0)
			SET @numDivisionID = ISNULL((SELECT numDivisionId FROM AdditionalContactsInformation WHERE numContactId=@numUserCntId),0)

			SELECT 
				@numRelationship=numCompanyType,
				@numProfile=vcProfile
			FROM 
				DivisionMaster D                  
			JOIN 
				CompanyInfo C 
			ON 
				C.numCompanyId=D.numCompanyID                  
			WHERE 
				numDivisionID =@numDivisionID 

			DECLARE @hDocItem INT
			DECLARE @TablePreferredPromotion TABLE
			(
				numItemCode NUMERIC(18,0)
				,numPromotionID NUMERIC(18,0)
			)

			IF ISNULL(@vcPreferredPromotions,'') <> ''
			BEGIN
				EXEC sp_xml_preparedocument @hDocItem OUTPUT, @vcPreferredPromotions

				INSERT INTO @TablePreferredPromotion
				(
					numItemCode
					,numPromotionID
				)
				SELECT
					numItemCode
					,numPromotionID
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item',2)                                                                          
				WITH                       
				(                                                                          
					numItemCode NUMERIC(18,0)
					,numPromotionID NUMERIC(18,0)
				)

				EXEC sp_xml_removedocument @hDocItem
			END

		
			DECLARE @TEMP TABLE
			(
				ID INT IDENTITY(1,1)
				,numCartID NUMERIC(18,0)
				,numItemCode NUMERIC(18,0)
				,numUnitHour NUMERIC(18,0)
				,monTotAmount DECIMAL(20,5)
			)
			INSERT INTO @TEMP
			(
				numCartID
				,numItemCode
				,numUnitHour
				,monTotAmount
			)
			SELECT
				numCartID
				,numItemCode
				,numUnitHour
				,monTotAmount
			FROM
				CartItems
			WHERE
				numDomainId=@numDomainId
				AND numUserCntId=@numUserCntId
				AND vcCookieId=@vcCookieId
				AND ISNULL(PromotionID,0) = 0 

			DECLARE @j INT = 1
			DECLARE @jCount AS INT
			DECLARE @numCartID NUMERIC(18,0)
			DECLARE @numItemCode NUMERIC(18,0)
			DECLARE @numUnitHour FLOAT
			DECLARE @monTotAmount DECIMAL(20,5)
			SELECT @jCount = COUNT(*) FROM @TEMP

			WHILE @j <= @jCount
			BEGIN
				SELECT 
					@numCartID=numCartID
					,@numItemCode=numItemCode
					,@numUnitHour=numUnitHour
					,@monTotAmount=monTotAmount
				FROM 
					@TEMP
				WHERE
					ID=@j

				SET @numPromotionID= 0
				SET @vcPromotionDescription = ''

				SELECT @numItemClassification = numItemClassification FROM Item WHERE Item.numItemCode = @numItemCode

				SET @numPreferredPromotionID = ISNULL((SELECT TOP 1 numPromotionID  FROM @TablePreferredPromotion WHERE numItemCode=@numItemCode),0)

				SELECT TOP 1
					@numPromotionID = numProId 
					,@vcPromotionDescription = ISNULL(vcShortDesc,'-')
				FROM 
					PromotionOffer PO
				WHERE 
					numDomainId=@numDomainID 
					AND (numProId=@numPreferredPromotionID OR ISNULL(@numPreferredPromotionID,0)=0)
					AND ISNULL(bitEnabled,0)=1 
					AND ISNULL(IsOrderBasedPromotion,0) = 0
					AND 1 = (CASE WHEN bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN GETUTCDATE()>=dtValidFrom AND GETUTCDATE()<=dtValidTo THEN 1 ELSE 0 END) END)
					AND 1 = (CASE 
								WHEN ISNULL(numOrderPromotionID,0) > 0
								THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=numOrderPromotionID AND tintType=2 AND numRelationship=@numRelationship AND numProfile=@numProfile) > 0 THEN 1 ELSE 0 END)
								ELSE
									(CASE tintCustomersBasedOn 
										WHEN 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numProId AND tintType=1 AND numDivisionID=@numDivisionID) > 0 THEN 1 ELSE 0 END)
										WHEN 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numProId AND tintType=2 AND numRelationship=@numRelationship AND numProfile=@numProfile) > 0 THEN 1 ELSE 0 END)
										WHEN 3 THEN 1
										ELSE 0
									END)
							END)
					AND 1 = (CASE 
								WHEN tintOfferBasedOn = 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 AND numValue=@numItemCode) > 0 THEN 1 ELSE 0 END)
								WHEN tintOfferBasedOn = 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=2 AND numValue=@numItemClassification) > 0 THEN 1 ELSE 0 END)
								WHEN tintOfferBasedOn = 4 THEN 1 
								ELSE 0
							END)
					AND 1 = (CASE 
								WHEN ISNULL(tintOfferTriggerValueType,1) = 1 --Based on quantity
								THEN
									CASE
										WHEN ISNULL(tintOfferTriggerValueTypeRange,1) = 2
										THEN
											(CASE WHEN ISNULL(@numUnitHour,0) BETWEEN ISNULL(fltOfferTriggerValue,0) AND ISNULL(fltOfferTriggerValueRange,0) THEN 1 ELSE 0 END)
										ELSE
											(CASE WHEN ISNULL(@numUnitHour,0) >= fltOfferTriggerValue THEN 1 ELSE 0 END)
									END
								WHEN ISNULL(tintOfferTriggerValueType,1) = 2 --Based on amount
								THEN
									CASE
										WHEN ISNULL(tintOfferTriggerValueTypeRange,1) = 2
										THEN
											(CASE WHEN ISNULL(@monTotAmount,0) BETWEEN ISNULL(fltOfferTriggerValue,0) AND ISNULL(fltOfferTriggerValueRange,0) THEN 1 ELSE 0 END)
										ELSE
											(CASE WHEN ISNULL(@monTotAmount,0) >= fltOfferTriggerValue THEN 1 ELSE 0 END)
									END
							END)
				ORDER BY
					(CASE 
						WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 1 THEN 1
						WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 1 THEN 2
						WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 2 THEN 3
						WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 2 THEN 4
						WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 3 THEN 5
						WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 3 THEN 6
					END) ASC,ISNULL(PO.fltOfferTriggerValue,0) DESC

				IF @numPromotionID > 0 AND (SELECT COUNT(*) FROM CartItems WHERE numDomainId=@numDomainId AND numUserCntId=@numUserCntId AND vcCookieId=@vcCookieId AND ISNULL(PromotionID,0) = @numPromotionID) = 0
				BEGIN
					UPDATE
						CartItems
					SET
						PromotionID=@numPromotionID
						,PromotionDesc=@vcPromotionDescription
						,bitPromotionTrigerred=1
					WHERE
						numCartId=@numCartId
						AND ISNULL(PromotionID,0) = 0
				END

				SET @j = @j + 1
			END

			EXEC USP_PromotionOffer_ApplyItemPromotionToECommerce @numDomainID,@numUserCntId,@numSiteID,@vcCookieId,0,''
		END
	END
END

GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateStagePercentageDetails]    Script Date: 09/21/2010 12:00:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----Created By Prasanta    
    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DeleteProcessFromOpportunity')
DROP PROCEDURE USP_DeleteProcessFromOpportunity
GO
CREATE PROCEDURE [dbo].[USP_DeleteProcessFromOpportunity] 
@numOppId AS NUMERIC(18,0)=0
as    
BEGIN   
	IF(@numOppId>0)
	BEGIN
		UPDATE dbo.OpportunityMaster SET numBusinessProcessID=NULL WHERE numOppID=@numOppId
	END
	DELETE FROM StagePercentageDetails WHERE numOppId=@numOppId
	DELETE FROM ProjectProcessStageDetails WHERE numOppId=@numOppId
	DELETE FROM ProjectProgress WHERE numOppId=@numOppId
	
	DELETE FROM StagePercentageDetailsTask WHERE numOppId=@numOppId
	DELETE FROM Sales_process_List_Master WHERE numOppId=@numOppId
	
END 

GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateStagePercentageDetails]    Script Date: 09/21/2010 12:00:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----Created By Gangadhar    
    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetTaskByStageDetailsId')
DROP PROCEDURE USP_GetTaskByStageDetailsId
GO
CREATE PROCEDURE [dbo].[USP_GetTaskByStageDetailsId]
@numDomainID as numeric(9)=0,    
@numStageDetailsId as numeric(18)=0,
@numOppId AS NUMERIC(18,0)=0,   
@numProjectId AS NUMERIC(18,0)=0,
@vcSearchText AS VARCHAR(MAX)='',
@bitAlphabetical AS BIT=0
as    
BEGIN   
	DECLARE @dynamicQuery AS NVARCHAR(MAX)=NULL
	SET @dynamicQuery = 'SELECT 
		ST.numTaskId,
		ST.vcTaskName,
		ST.numHours,
		ST.numMinutes,
		ST.numAssignTo,
		ISNULL(AC.vcFirstName+'' ''+AC.vcLastName,''-'') As vcContactName,
		ISNULL(ST.bitDefaultTask,0) AS bitDefaultTask,
		ISNULL(ST.bitTaskClosed,0) AS bitTaskClosed,
		ISNULL(ST.bitSavedTask,0) AS bitSavedTask,
		ISNULL(ST.numParentTaskId,0) AS numParentTaskId,
		ISNULL(AU.vcFirstName+'' ''+AU.vcLastName,''-'') As vcUpdatedByContactName,
		convert(varchar(10),ST.dtmUpdatedOn, 105) + right(convert(varchar(32),ST.dtmUpdatedOn,100),8) As dtmUpdatedOn,
		 convert(varchar(10),PPD.dtmStartDate, 101) + right(convert(varchar(32),PPD.dtmStartDate,100),8) AS StageStartDate,
		ST.numStageDetailsId,
		AC.numTeam,
		ISNULL(ST.numOrder,0) AS numOrder, 
		ISNULL(S.numStageOrder,0) AS numStageOrder,
		M.numStagePercentage
	FROM
		StagePercentageDetailsTask AS ST
	LEFT JOIN 
		StagePercentageDetails As S
	ON
		ST.numStageDetailsId=S.numStageDetailsId
	LEFT JOIN
		StagePercentageMaster AS M
	ON
		M.numStagePercentageId=S.numStagePercentageId
	LEFT JOIN
		AdditionalContactsInformation AS AC
	ON
		ST.numAssignTo=AC.numContactId
	LEFT JOIN 
		AdditionalContactsInformation AS AU
	ON
		ST.numCreatedBy=AU.numContactId
	LEFT JOIN
		ProjectProcessStageDetails AS PPD
	ON
		ST.numStageDetailsId=PPD.numStageDetailsId AND ST.numOppId=PPD.numOppId
	WHERE
		ST.numDomainID='+CAST(@numDomainID AS VARCHAR(100))+' '
	IF(@numStageDetailsId>0)
	BEGIN
		SET @dynamicQuery=@dynamicQuery+' AND ST.numStageDetailsId='+CAST(@numStageDetailsId AS VARCHAR(100))+' '
	END
	SET @dynamicQuery=@dynamicQuery+' AND ST.numOppId='+CAST(@numOppId AS VARCHAR(100))+' '
	SET @dynamicQuery=@dynamicQuery+' AND ST.numProjectId='+CAST(@numProjectId AS VARCHAR(100))+' '
	
	IF(LEN(@vcSearchText)>0)
	BEGIN
		SET @dynamicQuery=@dynamicQuery+' AND ST.vcTaskName LIKE ''%'+CAST(@vcSearchText AS VARCHAR(100))+'%'' '
	END
	IF(@bitAlphabetical=1)
	BEGIN
		SET @dynamicQuery=@dynamicQuery+' ORDER BY ST.vcTaskName ASC '
	END
	ELSE
	BEGIN
		SET @dynamicQuery=@dynamicQuery+' ORDER BY ST.numOrder ASC '
	END
	EXEC(@dynamicQuery)
END 

GO
/****** Object:  StoredProcedure [dbo].[USP_GetTaskForUserActivity]    Script Date: 09/21/2010 12:00:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----Created By Prasanta    
    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetTaskForUserActivity')
DROP PROCEDURE USP_GetTaskForUserActivity
GO
CREATE PROCEDURE [dbo].[USP_GetTaskForUserActivity]
@numUserCntId as numeric(9)=0,    
@numDomainID as numeric(18)=0
as    
BEGIN   
	SELECT T.dtmDueDate,T.vcMileStoneName,T.OrderType,T.vcStageName,T.vcPOppName,T.vcTaskName,T.numOppID,
	T.numStageDetailsId,T.numOppID,T.numStagePercentageId,T.numStagePercentage AS tintPercentage,T.tintConfiguration FROM (
	SELECT 
		T.vcTaskName,
		CASE WHEN S.bitIsDueDaysUsed=1 THEN CONVERT(VARCHAR(10),DATEADD(DAY,S.intDueDays,CAST(S.dtStartDate AS DATE)), 101)
		ELSE CONVERT(VARCHAR(10),DATEADD(DAY,(SELECT TOP 1 CAST(Items AS DECIMAL(18,2)) FROM Split(dbo.fn_calculateTotalHoursMinutesByStageDetails(T.numStageDetailsId,T.numOppId),'_')),CAST(S.dtStartDate AS DATE)), 101) END AS dtmDueDate,
		CASE WHEN OP.tintOppType=0 THEN 'Sales Opportunity' 
			 WHEN OP.tintOppType=1 THEN 'Sales Order' WHEN OP.tintOppType=2 THEN 'Purchase Order' ELSE '' END AS OrderType,
			 OP.vcPOppName,
			 S.vcMileStoneName,
			 S.vcStageName,
			 T.numStageDetailsId,
			 S.numOppID,
			 S.numStagePercentageId,
			 SM.numStagePercentage,
			 S.tintConfiguration
	FROM StagePercentageDetailsTask AS T
	LEFT JOIN StagePercentageDetails AS S
	ON S.numStageDetailsId=T.numStageDetailsId AND S.numOppID=T.numOppId
	LEFT JOIN OpportunityMaster AS OP 
	ON T.numOppId=OP.numOppId
	LEFT JOIN StagePercentageMaster AS SM
	ON S.numStagePercentageId=SM.numStagePercentageId
	WHERE T.numOppId>0 AND s.numOppID>0 AND T.bitSavedTask=1 AND T.bitTaskClosed=0 AND T.numDomainID=@numDomainID AND
	1=(CASE WHEN T.numAssignTo=@numUserCntId OR (S.numTeamId>0 
	AND @numUserCntId IN(SELECT numContactId FROM AdditionalContactsInformation WHERE numTeam=S.numTeamId)) THEN 1 ELSE 0 END)) AS T 
END

GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateStagePercentageDetails]    Script Date: 09/21/2010 12:00:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----Created By Gangadhar    
    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageStageTask')
DROP PROCEDURE USP_ManageStageTask
GO
CREATE PROCEDURE [dbo].[USP_ManageStageTask]
@numDomainID as numeric(9)=0,    
@numStageDetailsId as numeric(18)=0,           
@vcTaskName as VARCHAR(500)='',           
@numHours as numeric(9)=0,           
@numMinutes as numeric(9)=0,           
@numAssignTo as numeric(18)=0,           
@numCreatedBy as numeric(18)=0,
@numOppId AS NUMERIC(18,0)=0,   
@numProjectId AS NUMERIC(18,0)=0,   
@numParentTaskId AS NUMERIC(18,0)=0,
@bitTaskClosed AS BIT=0,
@numTaskId AS NUMERIC=0,
@bitSavedTask AS BIT=0,
@bitAutoClosedTaskConfirmed As BIT=0
as    
BEGIN   
	DECLARE @bitDefaultTask AS BIT=0
	IF(@numTaskId=0)
	BEGIN
		IF(@numOppId>0)
		BEGIN
			SET @bitDefaultTask=0
			SET @bitSavedTask=1
		END
		SET @bitTaskClosed =0
		INSERT INTO StagePercentageDetailsTask(
			numStageDetailsId, 
			vcTaskName, 
			numHours, 
			numMinutes, 
			numAssignTo, 
			numDomainID, 
			numCreatedBy, 
			dtmCreatedOn,
			numOppId,
			numProjectId,
			numParentTaskId,
			bitDefaultTask,
			bitTaskClosed,
			bitSavedTask
		)
		VALUES(
			@numStageDetailsId,
			@vcTaskName,
			@numHours,
			@numMinutes,
			@numAssignTo,
			@numDomainId,
			@numCreatedBy,
			GETDATE(),
			@numOppId,
			@numProjectId,
			@numParentTaskId,
			@bitDefaultTask,
			@bitTaskClosed,
			@bitSavedTask
		)
	END
	ELSE
	BEGIN
		DECLARE @vcRunningTaskName AS VARCHAR(500)=0
		DECLARE @numTaskValidatorId AS NUMERIC(18,0)=0
		DECLARE @numTaskSlNo AS NUMERIC(18,0)=0
		DECLARE @numStageOrder AS NUMERIC(18,0)=0
		DECLARE @numStagePercentageId AS NUMERIC(18,0)=0
		SELECT TOP 1 @numStageDetailsId=numStageDetailsId,@vcRunningTaskName=vcTaskName,@bitDefaultTask=bitDefaultTask,@numTaskSlNo=numOrder
		 FROM StagePercentageDetailsTask WHERE numTaskId=@numTaskId
		SELECT @numTaskValidatorId=numTaskValidatorId FROM Sales_process_List_Master WHERE Slp_Id=(SELECT TOP 1 slp_id FROM StagePercentageDetails WHERE numStageDetailsId=@numStageDetailsId)

		DECLARE @bitRunningDynamicMode AS BIT=0
		SELECT TOP 1 
			@bitRunningDynamicMode=bitRunningDynamicMode,
			@numStageOrder=numStageOrder ,
			@numStagePercentageId=numStagePercentageId
		FROM 
			StagePercentageDetails 
		WHERE 
			numStageDetailsId=@numStageDetailsId
		PRINT @bitRunningDynamicMode

		IF(@bitRunningDynamicMode=1 AND @numOppId>0)
		BEGIN

			DECLARE @recordCount AS INT=1
			IF(@bitDefaultTask=1)
			BEGIN
				SET @recordCount=(SELECT COUNT(*) FROM StagePercentageDetailsTask WHERE numParentTaskId=@numTaskId AND numStageDetailsId=@numStageDetailsId)
				UPDATE 
					StagePercentageDetailsTask
				SET 
					bitSavedTask=0
				WHERE
					bitDefaultTask=1 AND numStageDetailsId=@numStageDetailsId
			
			END
			IF(@recordCount=0)
			BEGIN
				DELETE FROM 
					StagePercentageDetailsTask
				WHERE
					numParentTaskId IN(SELECT numTaskId FROM StagePercentageDetailsTask WHERE 
					bitDefaultTask=1 AND numStageDetailsId=@numStageDetailsId)
				INSERT INTO StagePercentageDetailsTask(
				numStageDetailsId, 
				vcTaskName, 
				numHours, 
				numMinutes, 
				numAssignTo, 
				numDomainID, 
				numCreatedBy, 
				dtmCreatedOn,
				numOppId,
				numProjectId,
				numParentTaskId,
				bitDefaultTask,
				bitTaskClosed,
				bitSavedTask
			)
			SELECT 
				numStageDetailsId, 
				vcTaskName, 
				numHours, 
				numMinutes, 
				numAssignTo, 
				@numDomainID, 
				@numCreatedBy, 
				GETDATE(),
				@numOppId,
				@numProjectId,
				@numTaskId,
				0,
				0,
				1 
			FROM
				StagePercentageDetailsTask AS ST
			WHERE
				ST.numTaskId IN(
				SELECT numSecondaryListItemID FROM FieldRelationshipDTL WHERE 
				numPrimaryListItemID=(SELECT TOP 1 numTaskId FROM StagePercentageDetailsTask WHERE numOppId=0 AND numStageDetailsId=@numStageDetailsId AND vcTaskName=@vcRunningTaskName) AND
				numFieldRelID=(SELECT TOP 1 numFieldRelID FROM FieldRelationship WHERE numPrimaryListID=@numStageDetailsId AND ISNULL(bitTaskRelation,0)=1)
				) 					
			END

	
					
		END
		IF((@bitAutoClosedTaskConfirmed=1 AND @numOppId>0 AND @numTaskValidatorId=2 AND @bitTaskClosed=1) OR (@numOppId>0 AND @numTaskValidatorId=1 AND @bitTaskClosed=1))
		BEGIN
			IF(ISNULL(@numTaskSlNo,0)>0)
			BEGIN
				UPDATE
					StagePercentageDetailsTask
				SET
					bitTaskClosed=1,
					numCreatedBy=@numCreatedBy,
					dtmUpdatedOn=GETDATE()
				WHERE
					numStageDetailsId=@numStageDetailsId AND numOrder<@numTaskSlNo
			END
			ELSE
			BEGIN
				UPDATE
					StagePercentageDetailsTask
				SET
					bitTaskClosed=1,
					numCreatedBy=@numCreatedBy,
					dtmUpdatedOn=GETDATE()
				WHERE
					numStageDetailsId=@numStageDetailsId AND numTaskId<@numTaskId
			END
		END
		IF((@numOppId>0 AND @numTaskValidatorId=3 AND @bitTaskClosed=1) OR (@bitAutoClosedTaskConfirmed=1 AND @numOppId>0 AND @numTaskValidatorId=4 AND @bitTaskClosed=1))
		BEGIN
			IF(ISNULL(@numTaskSlNo,0)>0)
			BEGIN
				UPDATE
					StagePercentageDetailsTask
				SET
					bitTaskClosed=1,
					numCreatedBy=@numCreatedBy,
					dtmUpdatedOn=GETDATE()
				WHERE
					numStageDetailsId=@numStageDetailsId AND numOrder<@numTaskSlNo
			END
			ELSE
			BEGIN
				UPDATE
					StagePercentageDetailsTask
				SET
					bitTaskClosed=1,
					numCreatedBy=@numCreatedBy,
					dtmUpdatedOn=GETDATE()
				WHERE
					numStageDetailsId=@numStageDetailsId AND numTaskId<@numTaskId
			END
			IF(ISNULL(@numStageOrder,0)>0)
			BEGIN
				UPDATE
					StagePercentageDetailsTask
				SET
					bitTaskClosed=1,
					numCreatedBy=@numCreatedBy,
					dtmUpdatedOn=GETDATE()
				WHERE	
					numOppId=@numOppId AND
					numStageDetailsId IN(SELECT numStageDetailsId FROM StagePercentageDetails WHERE numStageOrder<@numStageOrder AND numStagePercentageId=@numStagePercentageId AND numOppId=@numOppId)
			END
			ELSE
			BEGIN
				UPDATE
					StagePercentageDetailsTask
				SET
					bitTaskClosed=1,
					numCreatedBy=@numCreatedBy,
					dtmUpdatedOn=GETDATE()
				WHERE	
					numOppId=@numOppId AND
					numStageDetailsId IN(SELECT numStageDetailsId FROM StagePercentageDetails WHERE numStageDetailsId<@numStageDetailsId AND numStagePercentageId=@numStagePercentageId AND numOppId=@numOppId)
			END
			UPDATE
					StagePercentageDetailsTask
				SET
					bitTaskClosed=1,
					numCreatedBy=@numCreatedBy,
					dtmUpdatedOn=GETDATE()
				WHERE	
					numOppId=@numOppId AND
					numStageDetailsId IN(SELECT numStageDetailsId FROM StagePercentageDetails WHERE numStagePercentageId<@numStagePercentageId AND numOppId=@numOppId)
		END
	
		UPDATE 
				StagePercentageDetailsTask
			SET 
				numAssignTo=@numAssignTo
			WHERE
				numTaskId=@numTaskId
		IF(@numHours>0)
		BEGIN
			UPDATE 
				StagePercentageDetailsTask
			SET 
				numHours=@numHours
			WHERE
				numTaskId=@numTaskId
		END
		IF(@numMinutes>0)
		BEGIN
			UPDATE 
				StagePercentageDetailsTask
			SET 
				numMinutes=@numMinutes
			WHERE
				numTaskId=@numTaskId
		END
		IF(LEN(@vcTaskName)>0)
		BEGIN
			UPDATE 
				StagePercentageDetailsTask
			SET 
				vcTaskName=@vcTaskName
			WHERE
				numTaskId=@numTaskId
		END
		UPDATE 
			StagePercentageDetailsTask
		SET
			bitSavedTask=@bitSavedTask,
			bitTaskClosed=@bitTaskClosed,
			numCreatedBy=@numCreatedBy,
			dtmUpdatedOn=GETDATE()
		WHERE
			numTaskId=@numTaskId
	END
	IF(@numOppId>0)
	BEGIN
		DECLARE @intTotalProgress AS INT =0
		DECLARE @intTotalTaskCount AS INT=0
		DECLARE @intTotalTaskClosed AS INT =0
		SET @intTotalTaskCount=(SELECT COUNT(*) FROM StagePercentageDetailsTask WHERE numOppId=@numOppId AND bitSavedTask=1)
		SET @intTotalTaskClosed=(SELECT COUNT(*) FROM StagePercentageDetailsTask WHERE numOppId=@numOppId AND bitSavedTask=1 AND bitTaskClosed=1)
		IF(@intTotalTaskCount>0 AND @intTotalTaskClosed>0)
		BEGIN
			SET @intTotalProgress=ROUND(((@intTotalTaskClosed*100)/@intTotalTaskCount),0)
		END
		IF((SELECT COUNT(*) FROM ProjectProgress WHERE numOppId=@numOppId AND numDomainId=@numDomainID)>0)
		BEGIN
			UPDATE ProjectProgress SET intTotalProgress=@intTotalProgress WHERE numOppId=@numOppId AND numDomainId=@numDomainID
		END
		ELSE
		BEGIN
			INSERT INTO ProjectProgress
			(
				numOppId,
				numDomainId,
				intTotalProgress
			)VALUES(
				@numOppId,
				@numDomainID,
				@intTotalProgress
			)
		END
	END
END 
