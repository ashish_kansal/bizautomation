/******************************************************************
Project: Release 3.7 Date: 17.OCT.2014
Comments: 
*******************************************************************/

------------------- SACHIN ------------------

ALTER TABLE dbo.WorkFlowMaster ADD
	bitCustom bit NULL

ALTER TABLE dbo.WorkFlowMaster ADD
	numFieldID numeric(18,0) NULL

ALTER TABLE dbo.CFW_FLD_Values
ENABLE CHANGE_TRACKING 
WITH (TRACK_COLUMNS_UPDATED = ON);

ALTER TABLE dbo.CFW_Fld_Values_Opp
ENABLE CHANGE_TRACKING 
WITH (TRACK_COLUMNS_UPDATED = ON);


ALTER TABLE dbo.CFW_FLD_Values_Pro
ENABLE CHANGE_TRACKING 
WITH (TRACK_COLUMNS_UPDATED = ON);

ALTER TABLE dbo.CFW_FLD_Values_Case
ENABLE CHANGE_TRACKING 
WITH (TRACK_COLUMNS_UPDATED = ON);

ALTER TABLE dbo.CFW_FLD_Values_Cont
ENABLE CHANGE_TRACKING 
WITH (TRACK_COLUMNS_UPDATED = ON);


---------------------------------------------

------------------- SANDEEP ------------------

---------------------------
---BizService App.Config---
---------------------------
<add key="BizRecurrence" value="1440"/>


UPDATE
	ShortCutBar
SET
	NewLink = '../opportunity/frmNewOrder.aspx?OppStatus=0&OppType=1'
WHERE
	Id=113

UPDATE
	ShortCutBar
SET
	NewLink = '../opportunity/frmNewOrder.aspx?OppStatus=1&OppType=1'
WHERE
	Id=115

-- OLD Links Do Not Run on Server
--UPDATE
--	ShortCutBar
--SET
--	NewLink = '../opportunity/frmAddOpportunity.aspx?OppType=1'
--WHERE
--	Id=113

--UPDATE
--	ShortCutBar
--SET
--	NewLink = '../opportunity/frmNewOrder.aspx'
--WHERE
--	Id=115


ALTER TABLE OpportunityMaster ADD
vcRecurrenceType varchar(20),
bitRecurred BIT

ALTER TABLE OpportunityItems ADD
numRecurParentOppItemID	 NUMERIC(18,0)

ALTER TABLE OpportunityBizDocs ADD
bitRecurred BIT

BEGIN TRANSACTION

DECLARE @numFieldID NUMERIC(18,0)

INSERT INTO DycFieldMaster
(
	numModuleID,
	vcFieldName,
	vcDbColumnName,
	vcOrigDbColumnName,
	vcPropertyName,
	vcLookBackTableName,
	vcFieldDataType,
	vcFieldType,
	vcAssociatedControlType,
	numListID,
	bitInResults,
	bitDeleted,
	bitAllowEdit,
	bitDefault,
	bitSettingField,
	bitDetailField,
	bitWorkFlowField,
	vcGroup
)
VALUES
(
	3,
	'Recurrence Type',
	'vcRecurrenceType',
	'vcRecurrenceType',
	'RecurrenceType',
	'OpportunityMaster',
	'V',
	'R',
	'TextBox',
	0,
	1,
	0,
	0,
	0,
	1,
	1,
	1,
	'Order Fields'
)

SET @numFieldID = SCOPE_IDENTITY()
INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (3,@numFieldID,Null,70,0,0,'Recurrence Type','TextBox','RecurrenceType',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)
INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)

VALUES        (3,@numFieldID,Null,39,0,0,'Recurrence Type','Label','RecurrenceType',NULL,NULL,NULL,NULL,1,0,0,1,1,0,1,0,0,0,0,0,NULL,NULL,0)


DECLARE @numFieldRecurredOpp NUMERIC(18,0)
INSERT INTO DycFieldMaster
(
	numModuleID,
	vcFieldName,
	vcDbColumnName,
	vcOrigDbColumnName,
	vcPropertyName,
	vcLookBackTableName,
	vcFieldDataType,
	vcFieldType,
	vcAssociatedControlType,
	numListID,
	bitInResults,
	bitDeleted,
	bitAllowEdit,
	bitDefault,
	bitSettingField,
	bitDetailField,
	bitWorkFlowField,
	vcGroup
)
VALUES
(
	3,
	'Recurred Order',
	'bitRecurred',
	'bitRecurred',
	'bitRecurred',
	'OpportunityMaster',
	'Y',
	'R',
	'CheckBox',
	0,
	1,
	0,
	0,
	0,
	1,
	1,
	1,
	'Order Fields'
)
SET @numFieldRecurredOpp = SCOPE_IDENTITY()

INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (3,@numFieldRecurredOpp,Null,70,0,0,'Recurred Order','CheckBox','bitRecurred',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)

DECLARE @numFieldRecurredBizDoc NUMERIC(18,0)
INSERT INTO DycFieldMaster
(
	numModuleID,
	vcFieldName,
	vcDbColumnName,
	vcOrigDbColumnName,
	vcPropertyName,
	vcLookBackTableName,
	vcFieldDataType,
	vcFieldType,
	vcAssociatedControlType,
	numListID,
	bitInResults,
	bitDeleted,
	bitAllowEdit,
	bitDefault,
	bitSettingField,
	bitDetailField,
	bitWorkFlowField,
	vcGroup
)
VALUES
(
	3,
	'Recurred BizDoc',
	'bitRecurred',
	'bitRecurred',
	'bitRecurred',
	'OpportunityBizDocs',
	'Y',
	'R',
	'CheckBox',
	0,
	1,
	0,
	0,
	0,
	1,
	1,
	1,
	'BizDoc Fields'
)
SET @numFieldRecurredBizDoc = SCOPE_IDENTITY()

INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (3,@numFieldRecurredBizDoc,Null,49,0,0,'Recurred BizDoc','CheckBox','bitRecurred',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)


ROLLBACK


GO

/****** Object:  Table [dbo].[RecurrenceConfiguration]    Script Date: 16-Oct-14 5:16:54 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[RecurrenceConfiguration](
	[numRecConfigID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numDomainID] [numeric](18, 0) NOT NULL,
	[numOppID] [numeric](18, 0) NULL,
	[numOppBizDocID] [numeric](18, 0) NULL,
	[dtStartDate] [date] NOT NULL,
	[dtEndDate] [date] NULL,
	[numType] [smallint] NOT NULL,
	[vcType] [varchar](50) NOT NULL,
	[numFrequency] [smallint] NOT NULL,
	[vcFrequency] [varchar](50) NOT NULL,
	[dtNextRecurrenceDate] [date] NULL,
	[bitCompleted] [bit] NULL,
	[numCreatedBy] [numeric](18, 0) NOT NULL,
	[dtCreatedDate] [datetime] NOT NULL,
	[bitDisabled] [bit] NULL,
	[dtDisabledDate] [date] NULL,
	[numDisabledBy] [numeric](18, 0) NULL,
	[numTransaction] [int] NULL,
	[bitMarkForDelete] [bit] NULL,
 CONSTRAINT [PK_RecurrenceConfiguration] PRIMARY KEY CLUSTERED 
(
	[numRecConfigID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING ON
GO

GO

/****** Object:  Table [dbo].[RecurrenceErrorLog]    Script Date: 16-Oct-14 5:17:20 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[RecurrenceErrorLog](
	[numErrorID] [int] IDENTITY(1,1) NOT NULL,
	[numDomainID] [numeric](18, 0) NOT NULL,
	[numRecConfigID] [numeric](18, 0) NOT NULL,
	[vcType] [nvarchar](100) NULL,
	[vcSource] [nvarchar](100) NULL,
	[vcMessage] [nvarchar](max) NULL,
	[vcStackStrace] [nvarchar](max) NULL,
	[CreatedDate] [datetime] NULL,
 CONSTRAINT [PK_RecurrenceErrorLog] PRIMARY KEY CLUSTERED 
(
	[numErrorID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

/****** Object:  Table [dbo].[RecurrenceTransaction]    Script Date: 16-Oct-14 5:17:55 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[RecurrenceTransaction](
	[numRecTranID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numRecConfigID] [numeric](18, 0) NOT NULL,
	[numRecurrOppID] [numeric](18, 0) NULL,
	[numRecurrOppBizDocID] [numeric](18, 0) NULL,
	[dtCreatedDate] [datetime] NOT NULL,
	[numErrorID] [int] NULL,
 CONSTRAINT [PK_RecurrenceTransaction] PRIMARY KEY CLUSTERED 
(
	[numRecTranID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[RecurrenceTransaction]  WITH CHECK ADD  CONSTRAINT [FK_RecurrenceTransaction_OpportunityBizDocs] FOREIGN KEY([numRecurrOppBizDocID])
REFERENCES [dbo].[OpportunityBizDocs] ([numOppBizDocsId])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[RecurrenceTransaction] CHECK CONSTRAINT [FK_RecurrenceTransaction_OpportunityBizDocs]
GO

ALTER TABLE [dbo].[RecurrenceTransaction]  WITH CHECK ADD  CONSTRAINT [FK_RecurrenceTransaction_OpportunityMaster] FOREIGN KEY([numRecurrOppID])
REFERENCES [dbo].[OpportunityMaster] ([numOppId])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[RecurrenceTransaction] CHECK CONSTRAINT [FK_RecurrenceTransaction_OpportunityMaster]
GO

ALTER TABLE [dbo].[RecurrenceTransaction]  WITH CHECK ADD  CONSTRAINT [FK_RecurrenceTransaction_RecurrenceConfiguration] FOREIGN KEY([numRecConfigID])
REFERENCES [dbo].[RecurrenceConfiguration] ([numRecConfigID])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[RecurrenceTransaction] CHECK CONSTRAINT [FK_RecurrenceTransaction_RecurrenceConfiguration]
GO

GO

/****** Object:  Table [dbo].[RecurrenceTransactionHistory]    Script Date: 16-Oct-14 5:18:22 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[RecurrenceTransactionHistory](
	[numRecTranHistoryID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numRecConfigID] [numeric](18, 0) NOT NULL,
	[numRecurrOppID] [numeric](18, 0) NULL,
	[numRecurrOppBizDocID] [numeric](18, 0) NULL,
	[dtCreatedDate] [datetime] NOT NULL,
 CONSTRAINT [PK_RecurrenceTransactionHistory] PRIMARY KEY CLUSTERED 
(
	[numRecTranHistoryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


---------------------------------------------

------------------- MANISH ------------------

------/******************************************************************
------Project: BACRMUI   Date: 16.Oct.2014
------Comments: Add new column to ReturnHeader
------*******************************************************************/

ALTER TABLE [dbo].[ReturnHeader] ADD numParentID NUMERIC(18,0)

UPDATE [dbo].[ReturnHeader] SET numParentID = 0 
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.ReturnHeader ADD CONSTRAINT
	DF_ReturnHeader_numParentID DEFAULT 0 FOR numParentID
GO
ALTER TABLE dbo.ReturnHeader SET (LOCK_ESCALATION = TABLE)
GO
COMMIT


------/******************************************************************
------Project: BizCart   Date: 06.Oct.2014
------Comments: Added a new setting for displaying treeview as per need (TreeView-UL/LI Based Tree)
------*******************************************************************/

BEGIN TRANSACTION

INSERT INTO [dbo].[PageElementAttributes]
( [numElementID] ,[vcAttributeName] ,[vcControlType] ,[vcControlValues] ,[bitEditor])
SELECT 1, 'Populate UL LI Based Tree','CheckBox',NULL,NULL

/*
UPDATE [PageElementAttributes] SET [vcAttributeName] = 'Populate UL LI Based Tree' 
WHERE [PageElementAttributes].[vcAttributeName] = 'Populate ul li Based Tree' 
AND [numElementID] = 1 
AND [numAttributeID] = 10116
*/
ROLLBACK

------/******************************************************************
------Project: BACRMUI   Date: 30.Sep.2014
------Comments: Create a new report Credit Card Register & Journal Entries Report
------*******************************************************************/

BEGIN TRANSACTION
	DECLARE @numPageNavID numeric 
	SELECT @numPageNavID = MAX(numPageNavID) + 1  FROM dbo.PageNavigationDTL 
	INSERT INTO dbo.PageNavigationDTL
			( numPageNavID ,
			  numModuleID ,
			  numParentID ,
			  vcPageNavName ,
			  vcNavURL ,
			  vcImageURL ,
			  bitVisible ,
			  numTabID
			)
	SELECT @numPageNavID ,35,222,'Credit Card Register','../Accounting/frmAccountsReports.aspx?RType=9',NULL,1,45 
	EXEC USP_CreateTreeNavigationForDomain 0,@numPageNavID --Give permission to tree node.

	exec USP_GetPageNavigation @numModuleID=35,@numDomainID=169,@numGroupID=838
ROLLBACK


BEGIN TRANSACTION

	DECLARE @numPageNavID numeric 
	SELECT @numPageNavID = MAX(numPageNavID) + 1  FROM dbo.PageNavigationDTL 
	INSERT INTO dbo.PageNavigationDTL
			( numPageNavID ,
			  numModuleID ,
			  numParentID ,
			  vcPageNavName ,
			  vcNavURL ,
			  vcImageURL ,
			  bitVisible ,
			  numTabID
			)
	SELECT @numPageNavID ,35,93,'Journal Entry','../Accounting/frmJournalEntryReport.aspx',NULL,1,45 
	EXEC USP_CreateTreeNavigationForDomain 0,@numPageNavID --Give permission to tree node.

	exec USP_GetPageNavigation @numModuleID=35,@numDomainID=169,@numGroupID=838

ROLLBACK


---------------------------------------------

