BEGIN TRANSACTION
GO
--Install E-Payment integrator V5 license on server
DELETE FROM dbo.DycFormConfigurationDetails WHERE numFormId=39 AND numFieldId=250
DELETE FROM dbo.DycFormField_Mapping WHERE vcFieldName='Shipping Amount' AND numFormID=39
UPDATE dbo.DycFormField_Mapping SET bitImport =1 WHERE numFormID=36 AND vcFieldName LIKE 'Web Link%'
--monShipCost Field has been obsolete from OpportunityMaster table
ROLLBACK TRANSACTION



/******************************************************************
Project: Sales Fulfillment and BizDoc changes   Date:12/01/2012
Comments: 
*******************************************************************/
BEGIN TRANSACTION

--SELECT * FROM dbo.ShortCutBar WHERE Link='../opportunity/frmAmtPaid.aspx'
UPDATE ShortCutBar SET bitNew=1 WHERE Link='../opportunity/frmAmtPaid.aspx' 

ALTER TABLE dbo.OpportunityMaster ADD
	vcOppRefOrderNo VARCHAR(100) NULL
	


INSERT INTO [dbo].[DycFieldMaster]([numModuleID], [numDomainID], [vcFieldName], [vcDbColumnName], [vcOrigDbColumnName], [vcPropertyName], [vcLookBackTableName], [vcFieldDataType], [vcFieldType], [vcAssociatedControlType], 
[vcToolTip], [vcListItemType], [numListID], [PopupFunctionName], [order], [tintRow], [tintColumn], 
[bitInResults], [bitDeleted], [bitAllowEdit], [bitDefault], [bitSettingField], [bitAddField], 
[bitDetailField], [bitAllowSorting],bitWorkFlowField,bitAllowFiltering )
SELECT 3, NULL, N'Customer PO# / Vendor Invoice#', N'vcOppRefOrderNo', N'vcOppRefOrderNo', 'OppRefOrderNo', N'OpportunityMaster', N'V', N'R',
 N'TextBox', NULL, N'', 0, N'', 15, 13, 2, 1, 0, 1, 0,1, 0, 1, 1,1,1
	
	

--SELECT * FROM dbo.DycFieldMaster WHERE vcDbColumnName='vcRefOrderNo'
--SELECT numFieldID FROM dbo.DycFieldMaster WHERE vcDbColumnName='vcOppRefOrderNo'

--SELECT * FROM dbo.DycFormField_Mapping WHERE numFieldID=110

UPDATE DycFormField_Mapping SET numFieldID=(SELECT numFieldID FROM dbo.DycFieldMaster 
WHERE vcDbColumnName='vcOppRefOrderNo'),vcPropertyName='OppRefOrderNo' WHERE numFormID IN (38,39,40,41) AND numFieldID=110

--select * FROM dbo.DycFormConfigurationDetails

UPDATE DycFormConfigurationDetails SET numFieldID=(SELECT numFieldID FROM dbo.DycFieldMaster 
WHERE vcDbColumnName='vcOppRefOrderNo') WHERE numFormID IN (38,39,40,41) AND numFieldID=110
	

UPDATE OM SET vcOppRefOrderNo=OBD.vcRefOrderNo FROM 
OpportunityBizDocs OBD JOIN dbo.OpportunityMaster OM ON OBD.numOppId = OM.numOppId
WHERE OBD.vcRefOrderNo IS NOT NULL 
	

--------------------------------------------------------------------------------------------------------

--SELECT * FROM CustRptDefaultFields WHERE vcScrFieldName='Terms' AND numFieldGroupID=25
UPDATE CustRptDefaultFields SET vcDbFieldName='case when OppMas.bitBillingTerms=1 then  ''Net '' + convert(varchar(10),ISNULL(dbo.fn_GetListItemName(isnull(OppMas.intBillingDays,0)),0)) + '' , ''  + case when OppMas.bitInterestType=0  then ''-'' else ''+'' end + convert(varchar(10),OppMas.fltInterest) + ''%''  else '''' end'
WHERE vcScrFieldName='Terms' AND numFieldGroupID=25

--SELECT * FROM CustRptDefaultFields WHERE vcScrFieldName='Due Date' AND numFieldGroupID=25
UPDATE CustRptDefaultFields SET vcDbFieldName='dateadd(day,case when OppMas.bitBillingTerms=1 then convert(int,ISNULL(dbo.fn_GetListItemName(isnull(OppMas.intBillingdays,0)),0)) else 0 end ,convert(varchar(11),OppMas.dtFromDate))'
WHERE vcScrFieldName='Due Date' AND numFieldGroupID=25
	

--SELECT * FROM CustReportFields WHERE vcFieldName LIKE '%intBillingdays%'
UPDATE CustReportFields SET vcFieldName=REPLACE(CAST(vcFieldName AS VARCHAR(8000)),'OppBDoc.bitBillingTerms','OppMas.bitBillingTerms') WHERE vcFieldName LIKE '%OppBDoc.bitBillingTerms%'

--SELECT * FROM CustReportFields WHERE vcFieldName LIKE '%bitBillingTerms%'
UPDATE CustReportFields SET vcFieldName=REPLACE(CAST(vcFieldName AS VARCHAR(8000)),'OppBDoc.bitBillingTerms','OppMas.bitBillingTerms') WHERE vcFieldName LIKE '%OppBDoc.bitBillingTerms%'


--SELECT * FROM CustomReport WHERE textQuery LIKE '%OppBDoc.intBillingdays%'
UPDATE CustomReport SET textQuery=REPLACE(CAST(textQuery AS VARCHAR(8000)),'OppBDoc.intBillingdays','OppMas.intBillingdays') WHERE textQuery LIKE '%OppBDoc.intBillingdays%'

--SELECT * FROM CustomReport WHERE textQuery LIKE '%OppBDoc.bitBillingTerms%'
UPDATE CustomReport SET textQuery=REPLACE(CAST(textQuery AS VARCHAR(8000)),'OppBDoc.bitBillingTerms','OppMas.bitBillingTerms') WHERE textQuery LIKE '%OppBDoc.bitBillingTerms%'

	
--------------------------------------------------------------------------------------------------------
--SELECT OBDD.numDivisionId,tintPaymentType,OPD.numCheckNo,BPH.*,OPD.monAmount AS monCheckAmount
--FROM OpportunityBizDocsDetails OBDD 
--INNER JOIN dbo.OpportunityBizDocsPaymentDetails OPD  ON OBDD.numBizDocsPaymentDetId = OPD.numBizDocsPaymentDetId
--JOIN dbo.BillPaymentHeader BPH ON BPH.numBizDocsPaymentDetId_Ref=OBDD.numBizDocsPaymentDetId 
--WHERE tintPaymentType IN(2,4) AND
--ISNULL(numCheckNo,0)>0


UPDATE BPH SET numDivisionId=OBDD.numDivisionId FROM OpportunityBizDocsDetails OBDD 
INNER JOIN dbo.OpportunityBizDocsPaymentDetails OPD  ON OBDD.numBizDocsPaymentDetId = OPD.numBizDocsPaymentDetId
JOIN dbo.BillPaymentHeader BPH ON BPH.numBizDocsPaymentDetId_Ref=OBDD.numBizDocsPaymentDetId 
WHERE tintPaymentType IN(2,4) AND
ISNULL(numCheckNo,0)>0
	
--SELECT BPH.numDivisionID,CH.numDivisionID,CH.tintReferenceType,* FROM dbo.CheckHeader CH JOIN dbo.BillPaymentHeader BPH ON CH.numReferenceID=BPH.numBillPaymentID
--AND CH.tintReferenceType=8 WHERE BPH.numDivisionID IS NOT NULL  AND CH.numDivisionID=0

UPDATE CH SET numDivisionID=BPH.numDivisionID FROM dbo.CheckHeader CH JOIN dbo.BillPaymentHeader BPH ON CH.numReferenceID=BPH.numBillPaymentID
AND CH.tintReferenceType=8 WHERE BPH.numDivisionID IS NOT NULL  AND ISNULL(CH.numDivisionID,0)=0



--------------------------------------------------------------------------------------------------------
ALTER TABLE dbo.OpportunityBizDocs ADD
	numBizDocStatusOLD numeric(18, 0) NULL
	
UPDATE OpportunityBizDocs SET numBizDocStatusOLD=numBizDocStatus


--------------------------------------------------------------------------------------------------------
SELECT GJD.numCustomerId,CH.numDivisionID FROM dbo.CheckHeader CH LEFT JOIN dbo.General_Journal_Header GJH ON CH.numCheckHeaderID=GJH.numCheckHeaderID
JOIN dbo.General_Journal_Details GJD ON GJD.numJournalId=GJH.numJournal_Id WHERE CH.tintReferenceType=1

UPDATE GJD SET numCustomerId=CH.numDivisionID FROM dbo.CheckHeader CH LEFT JOIN dbo.General_Journal_Header GJH ON CH.numCheckHeaderID=GJH.numCheckHeaderID
JOIN dbo.General_Journal_Details GJD ON GJD.numJournalId=GJH.numJournal_Id WHERE CH.tintReferenceType=1




--------------------------------------------------------------------------------------------------------
DECLARE @numFieldID AS NUMERIC;
SELECT @numFieldID=numFieldID FROM DycFieldMaster WHERE vcDbColumnName='monShipCost' AND vcLookBackTableName='OpportunityBizDocItems'
delete FROM dbo.DycFormField_Mapping WHERE numFieldID=@numFieldID
delete FROM dbo.DycFormConfigurationDetails WHERE numFieldID=@numFieldID

---------------------------------
DECLARE @numFieldID AS NUMERIC;
SELECT @numFieldID=numFieldID FROM DycFieldMaster WHERE vcDbColumnName='vcTrackingNo' AND vcLookBackTableName='OpportunityBizDocItems'
delete FROM dbo.DycFormField_Mapping WHERE numFieldID=@numFieldID
delete FROM dbo.DycFormConfigurationDetails WHERE numFieldID=@numFieldID

---------------------------------
--DECLARE @numFieldID AS NUMERIC;
--SELECT @numFieldID=numFieldID FROM DycFieldMaster WHERE vcDbColumnName='ShippingAmount' AND vcLookBackTableName='TaxItems'
--delete FROM dbo.DycFormField_Mapping WHERE numFieldID=@numFieldID
--delete FROM dbo.DycFormConfigurationDetails WHERE numFieldID=@numFieldID

---------------------------------
DECLARE @numFieldID AS NUMERIC;
SELECT @numFieldID=numFieldID FROM DycFieldMaster WHERE vcDbColumnName='vcShippingMethod' AND vcLookBackTableName='OpportunityBizDocItems'
delete FROM dbo.DycFormField_Mapping WHERE numFieldID=@numFieldID
delete FROM dbo.DycFormConfigurationDetails WHERE numFieldID=@numFieldID

---------------------------------
DECLARE @numFieldID AS NUMERIC;
SELECT @numFieldID=numFieldID FROM DycFieldMaster WHERE vcDbColumnName='dtDeliveryDate' AND vcLookBackTableName='OpportunityBizDocItems'
delete FROM dbo.DycFormField_Mapping WHERE numFieldID=@numFieldID
delete FROM dbo.DycFormConfigurationDetails WHERE numFieldID=@numFieldID

--------------------------------------------------------------------------------------------------------


GO
/****** Object:  Table [dbo].[OpportunityFulfillmentBizDocs]    Script Date: 01/11/2013 15:19:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OpportunityFulfillmentBizDocs](
	[numBizDocTypeID] [numeric](18, 0) NOT NULL,
	[numDomainID] [numeric](18, 0) NOT NULL,
	[ItemType] [char](2) NOT NULL,
 CONSTRAINT [PK_FulfillmentOrderBizDocs] PRIMARY KEY CLUSTERED 
(
	[numBizDocTypeID] ASC,
	[numDomainID] ASC,
	[ItemType] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING ON



GO
/****** Object:  Table [dbo].[OpportunityFulfillmentRules]    Script Date: 01/11/2013 15:19:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OpportunityFulfillmentRules](
	[numRuleID] [numeric](18, 0) NOT NULL,
	[numBizDocTypeID] [numeric](18, 0) NOT NULL,
	[numBizDocStatus] [numeric](18, 0) NOT NULL,
	[numDomainID] [numeric](18, 0) NOT NULL,
 CONSTRAINT [PK_OpportunityFulfillmentRules] PRIMARY KEY CLUSTERED 
(
	[numRuleID] ASC,
	[numBizDocTypeID] ASC,
	[numBizDocStatus] ASC,
	[numDomainID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]


--------------------------------------------------------------------------------------------------------

UPDATE ShortCutBar SET link='../opportunity/frmSalesFulfillmentOrder.aspx' 
WHERE link='../Opportunity/frmSalesFulfullment.aspx'

UPDATE PageNavigationDTL SET vcNavURL='../opportunity/frmSalesFulfillmentOrder.aspx' 
WHERE vcNavURL='../Opportunity/frmSalesFulfullment.aspx'
GO
ROLLBACK TRANSACTION



--------------------------------------------------------------------------------------------------------

UPDATE ShortCutBar SET link='../opportunity/frmSalesFulfillmentOrder.aspx' 
WHERE link='../Opportunity/frmSalesFulfullment.aspx'

UPDATE PageNavigationDTL SET vcNavURL='../opportunity/frmSalesFulfillmentOrder.aspx' 
WHERE vcNavURL='../Opportunity/frmSalesFulfullment.aspx'
GO
ROLLBACK TRANSACTION


BEGIN TRANSACTION
INSERT INTO dbo.ErrorMaster (		
	vcErrorCode,	
	vcErrorDesc,	
	tintApplication	
) VALUES (  		
	/* vcErrorCode - varchar(6) */ 'ERR067',	
	/* vcErrorDesc - nvarchar(max) */ N'Service item for shipping charge is not selected,Please contact your merchant.',	
	/* tintApplication - tinyint */ 3 ) 	

GO
ROLLBACK TRANSACTION

BEGIN TRANSACTION
GO
ALTER TABLE Domain ADD
numShippingServiceItemID NUMERIC(18,0) NULL
GO
ROLLBACK TRANSACTION



/******************************************************************
Project: Time & Expense   Date: 09.01.2013
Comments: Task 24 : Slide # 5 
*******************************************************************/
BEGIN TRANSACTION

GO
DECLARE @v sql_variant 
SET @v = N'Contact->1,Organization->2,Opportunity->3, Project ->4'
EXECUTE sp_updateextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'AddressDetails', N'COLUMN', N'tintAddressOf'
GO

GO
ALTER TABLE ProjectsMaster ADD [numAddressID] [numeric] (18, 0) NULL
GO

GO
INSERT INTO dbo.DycFormField_Mapping 
(numFieldID, numModuleID,numDomainID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,[order],tintRow,
  tintColumn,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,bitAllowFiltering,
  bitRequired,numFormFieldID,intSectionID,bitAllowGridColor ) 
SELECT 428,3,NULL,13,0,NULL,'Shipping Address','Popup','ShippingAddress','openAddress',46,28,2,1,0,0,0,0,1,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL
--SELECT * FROM dbo.DycFormField_Mapping WHERE numFormID = 13
GO
ROLLBACK TRANSACTION

/******************************************************************
Project: Time & Expense   Date: 10.01.2013
Comments: Task 24 : Slide # 5 : Setup/Update Default AddressId For ProjectsMaster
*******************************************************************/
BEGIN TRANSACTION
	BEGIN
		CREATE TABLE #tmpDiv
		(
			ID [numeric] (18, 0) NOT NULL IDENTITY(1, 1),
			numDivisionId [numeric] (18, 0) NOT NULL,
			numDomainID [numeric] (18, 0) NOT NULL 
		)
		
		INSERT INTO #tmpDiv ( numDivisionId,numDomainID ) 
		SELECT DISTINCT numDivisionId,numDomainId FROM dbo.ProjectsMaster  WHERE ISNULL(numAddressID,0) = 0
		SELECT * FROM #tmpDiv
		
		DECLARE @numDomainID  AS NUMERIC(18,0)
		DECLARE @numDivisionId  AS NUMERIC(18,0)
		DECLARE @intCnt AS INT
		SELECT @intCnt = COUNT(*) FROM #tmpDiv
		PRINT '#tmpDiv Table Row Count : ' + CONVERT(VARCHAR(10),@intCnt)
		
		IF @intCnt > 0
			BEGIN
				DECLARE @numPrimaryAddressID AS NUMERIC(18,0)
				DECLARE @numAddressID AS NUMERIC(18,0)
				DECLARE @intCount AS INT 
				SET @intCount = 0
				
				WHILE(@intCount < @intCnt)
				BEGIN
					SET @intCount = @intCount + 1
					PRINT '@intCount : ' + CONVERT(VARCHAR(10),@intCount)
					
					SELECT @numDivisionId = numDivisionID,
					       @numDomainID = numDomainID
					FROM #tmpDiv WHERE ID = @intCount
					
					PRINT '@numDivisionId : ' + CONVERT(VARCHAR(10),@numDivisionId)
					PRINT '@numDomainID : ' + CONVERT(VARCHAR(10),@numDomainID)
					
					SELECT @numPrimaryAddressID = ISNULL(numAddressID,0) FROM dbo.AddressDetails 
					WHERE numRecordID = @numDivisionId  
					AND numDomainID = @numDomainID
					AND ISNULL(bitIsPrimary,0) = 1
					PRINT '@numPrimaryAddressID : ' + CONVERT(VARCHAR(10),@numPrimaryAddressID)
					
					INSERT INTO dbo.AddressDetails 
					(vcAddressName,vcStreet,vcCity,vcPostalCode,numState,numCountry,bitIsPrimary,tintAddressOf,tintAddressType,numRecordID,numDomainID )
					SELECT vcAddressName,vcStreet,vcCity,vcPostalCode,numState,numCountry,bitIsPrimary,tintAddressOf,tintAddressType,numRecordID ,numDomainID
					FROM dbo.AddressDetails
					WHERE numAddressID = @numPrimaryAddressID
					AND numDomainID = @numDomainID
					
					--SELECT @numAddressID = numAddressID FROM dbo.AddressDetails WHERE numRecordID = @numProId AND ISNULL(bitIsPrimary,0) = 1  AND ISNULL(tintAddressOf,0) = 4
					SET @numAddressID = @@Identity  
					PRINT '@numAddressID : ' + CONVERT(VARCHAR(10),@numAddressID)
					
					UPDATE ProjectsMaster SET numAddressID = @numAddressID 	
					WHERE numDivisionId = @numDivisionId  
					AND numDomainID = @numDomainID 	
					
				END
						
			END
		
		SELECT * FROM dbo.AddressDetails WHERE numAddressID IN (SELECT numAddressID FROM ProjectsMaster)
--		SELECT * FROM ProjectsMaster WHERE ISNULL(numAddressID,0) > 0
--		SELECT * FROM ProjectsMaster WHERE numAddressID = 0
--		SELECT * FROM ProjectsMaster WHERE ISNULL(numAddressID,0) = 0

--		UPDATE ProjectsMaster SET numAddressID = NULL
		DROP TABLE #tmpDiv
	END 
ROLLBACK

/******************************************************************
Project: Time & Expense   Date: 08.01.2013
Comments: Task 24 : Slide # 5 
*******************************************************************/
BEGIN TRANSACTION
GO
INSERT INTO dbo.DycFormField_Mapping 
(numFieldID, numModuleID,numDomainID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,[order],tintRow,
  tintColumn,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,bitAllowFiltering,
  bitRequired,numFormFieldID,intSectionID,bitAllowGridColor ) 
SELECT  120,3,NULL,43,0,0,'Business Process','SelectBox',NULL,NULL,25,NULL,NULL,1,0,0,1,0,0,1,NULL,NULL,NULL,1,NULL,1845,NULL,1
UNION ALL
SELECT  121,3,NULL,43,0,0,'Total Progress','SelectBox',NULL,NULL,0,NULL,NULL,1,0,0,1,NULL,NULL,1,NULL,NULL,NULL,1,NULL,1511,NULL,NULL
UNION ALL
SELECT  96,3,NULL,43,0,0,'Sales Order Name','TextBox',NULL,NULL,6,NULL,NULL,1,0,0,1,NULL,NULL,1,NULL,NULL,NULL,1,NULL,1569,NULL,NULL

--SELECT * FROM dbo.DycFormField_Mapping WHERE numFormID =43
GO
ROLLBACK TRANSACTION


/******************************************************************
Project: Time & Expense   Date: 03.01.2013
Comments: Task 24 : Slide # 1
*******************************************************************/
BEGIN TRANSACTION
GO
--- ADD IsEnableResourceScheduling To Domain Details
ALTER TABLE Domain ADD IsEnableResourceScheduling BIT NULL
GO
ROLLBACK TRANSACTION