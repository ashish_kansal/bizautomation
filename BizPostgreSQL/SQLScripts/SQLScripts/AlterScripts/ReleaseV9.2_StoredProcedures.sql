/******************************************************************
Project: Release 9.2 Date: 21.FEBRUARY.2018
Comments: STORE PROCEDURES
*******************************************************************/



GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_GetFollowUpDetails')
DROP FUNCTION fn_GetFollowUpDetails
GO
-- =============================================
-- Author:		<Priya>
-- Create date: <15 Jan 2018>
-- Description:	<Fetch Last and Next Follow ups>
-- =============================================
CREATE FUNCTION [dbo].[fn_GetFollowUpDetails](@numContactID NUMERIC, @numFollowup INT, @numDomainID NUMERIC) 

RETURNS VARCHAR(MAX)
AS
BEGIN

DECLARE @RetFollowup Varchar(MAX)
SET @RetFollowup=''

 IF(@numFollowup = 1)
  BEGIN
	SELECT @RetFollowup =
	CASE 
		WHEN (SELECT COUNT(*) FROM ConECampaignDTL CEDInner WHERE CEDInner.numConECampID=ConECampaign.numConEmailCampID ANd ISNULL(bitSend,0)=1) > 0 THEN
			(SELECT (CASE 
					 WHEN GenericDocuments.numGenericDocID IS NOT NULL
					 THEN CONCAT(GenericDocuments.vcDocName + ' ',
								 ISNULL(dbo.FormatedDateFromDate(ConECampaignDTL.bintSentON,@numDomainID),''),
								 ' <img alt="Emails Read" height="16px" width="16px" title="EmailsRead" src="../images/email_read.png"> (',
								 (SELECT COUNT(*) FROM ConECampaignDTL WHERE ISNULL(tintDeliveryStatus,0) = 1 AND ISNULL(bitEmailRead,0) = 1) ,')' )
					 WHEN tblActionItemData.RowID IS NOT NULL 
					 THEN CONCAT(tblActionItemData.Activity + ' ',
								ISNULL(dbo.FormatedDateFromDate(ConECampaignDTL.bintSentON,@numDomainID),''),
								' <img alt="Action Items" height="16px" width="16px" title="Action Items" src="../images/MasterList-16.gif">')
					ELSE ''
					END)
			FROM
				ConECampaignDTL
			LEFT JOIN
				ECampaignDTLs
			ON
				ConECampaignDTL.numECampDTLID = ECampaignDTLs.numECampDTLId
			LEFT JOIN
				GenericDocuments 
			ON
				ECampaignDTLs.numEmailTemplate = GenericDocuments.numGenericDocID
			LEFT JOIN
				tblActionItemData
			ON
				ECampaignDTLs.numActionItemTemplate = tblActionItemData.RowID
			WHERE
				ConECampaignDTL.numConECampDTLID = (SELECT MAX(ConECampaignDTL.numConECampDTLID) FROM ConECampaignDTL WHERE ConECampaignDTL.numConECampID=ConECampaign.numConEmailCampID AND ISNULL(bitSend,0)=1)
				)

		ELSE ''
	END 
	FROM
		ConECampaign
	WHERE
		ConECampaign.numContactId=@numContactID
		AND ISNULL(bitEngaged,0)=1
	--SET @RetFollowup= vcLastFollowup
END

ELSE IF (@numFollowup = 2)
BEGIN
	SELECT @RetFollowup = 
		CASE 
			WHEN (SELECT COUNT(*) FROM ConECampaignDTL CEDInner WHERE CEDInner.numConECampID=ConECampaign.numConEmailCampID ANd ISNULL(bitSend,0)=0) > 0 THEN
				(SELECT (CASE 
						WHEN GenericDocuments.numGenericDocID IS NOT NULL THEN CONCAT(GenericDocuments.vcDocName, +' '+ ISNULL(dbo.FormatedDateFromDate(ConECampaignDTL.dtExecutionDate,@numDomainID),''))
						WHEN tblActionItemData.RowID IS NOT NULL THEN CONCAT(tblActionItemData.Activity,  +' '+ ISNULL(dbo.FormatedDateFromDate(ConECampaignDTL.dtExecutionDate,@numDomainID),''))
						ELSE ''
						END)
				FROM
					ConECampaignDTL
				LEFT JOIN
					ECampaignDTLs
				ON
					ConECampaignDTL.numECampDTLID = ECampaignDTLs.numECampDTLId
				LEFT JOIN
					GenericDocuments 
				ON
					ECampaignDTLs.numEmailTemplate = GenericDocuments.numGenericDocID
				LEFT JOIN
					tblActionItemData
				ON
					ECampaignDTLs.numActionItemTemplate = tblActionItemData.RowID
				WHERE
					ConECampaignDTL.numConECampDTLID = (SELECT MIN(ConECampaignDTL.numConECampDTLID) FROM ConECampaignDTL WHERE ConECampaignDTL.numConECampID=ConECampaign.numConEmailCampID AND ISNULL(bitSend,0)=0)
					)

			ELSE ''
		END 
	FROM
		ConECampaign
	WHERE
		ConECampaign.numContactId=@numContactID
		AND ISNULL(bitEngaged,0)=1
--SET @RetFollowup = vcNextFollowup
END

ELSE IF(@numFollowup = 3)
  BEGIN
	SELECT @RetFollowup =
	CASE 
		WHEN (SELECT COUNT(*) FROM ConECampaignDTL CEDInner WHERE CEDInner.numConECampID=ConECampaign.numConEmailCampID ANd ISNULL(bitSend,0)=1) > 0 THEN
			(SELECT (CASE 
					 WHEN GenericDocuments.numGenericDocID IS NOT NULL THEN
											(SELECT vcEmailLog FROM ConECampaignDTL WHERE ISNULL(tintDeliveryStatus,0) = 1 AND ISNULL(bitEmailRead,0) = 1)
				ELSE ''
				END)
					
			FROM
				ConECampaignDTL
			LEFT JOIN
				ECampaignDTLs
			ON
				ConECampaignDTL.numECampDTLID = ECampaignDTLs.numECampDTLId
			LEFT JOIN
				GenericDocuments 
			ON
				ECampaignDTLs.numEmailTemplate = GenericDocuments.numGenericDocID
			LEFT JOIN
				tblActionItemData
			ON
				ECampaignDTLs.numActionItemTemplate = tblActionItemData.RowID
			WHERE
				ConECampaignDTL.numConECampDTLID = (SELECT MAX(ConECampaignDTL.numConECampDTLID) FROM ConECampaignDTL WHERE ConECampaignDTL.numConECampID=ConECampaign.numConEmailCampID AND ISNULL(bitSend,0)=1)
				
			)
		ELSE ''
	END 
	FROM
		ConECampaign
	WHERE
		ConECampaign.numContactId=@numContactID
		AND ISNULL(bitEngaged,0)=1   --- LastEmailStatus
END

RETURN @RetFollowup
END

GO


GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetAlertDetail')
DROP FUNCTION GetAlertDetail
GO
CREATE FUNCTION [dbo].[GetAlertDetail]
(
	@numEmailHstrID NUMERIC(18,0),
	@numDivisionID NUMERIC(18,0),
	@numECampaignID NUMERIC(18,0),
	@numContactID NUMERIC(18,0),
	@bitOpenActionItem BIT,
	@bitOpencases BIT,
	@bitOpenProject BIT,
	@bitOpenSalesOpp BIT,
	@bitBalancedue BIT,
	@bitUnreadEmail BIT,
	@bitCampaign BIT,
	@TotalBalanceDue BIGINT,
	@OpenSalesOppCount BIGINT,
	@OpenCaseCount BIGINT,
	@OpenProjectCount BIGINT,
	@UnreadEmailCount BIGINT,
	@OpenActionItemCount BIGINT,
	@CampaignDTLCount BIGINT
)
Returns VARCHAR(MAX) 
As
BEGIN
	DECLARE @vcAlert AS VARCHAR(MAX) = ''
	
	--CHECK AR BALANCE
	IF @bitBalancedue = 1 AND  @TotalBalanceDue > 0 
    BEGIN 
		SET @vcAlert = '<a href="#" onclick="OpenAlertDetail(1,'+ CAST(@numDivisionID AS VARCHAR(18)) +',0)"><img alt="" src="../images/dollar.png" style="height: 16px; vertical-align: middle; padding-bottom: 2px;" /></a>&nbsp;'
    END 

	--CHECK OPEN SALES ORDER COUNT
	IF @bitOpenSalesOpp = 1 AND @OpenSalesOppCount > 0 
    BEGIN 
		SET @vcAlert = @vcAlert + '<a href="#" onclick="OpenAlertDetail(2,'+ CAST(@numDivisionID AS VARCHAR(18)) +',0)"><img alt="" src="../images/icons/cart.png" style="height: 16px; vertical-align: middle; padding-bottom: 2px;" /></a>&nbsp;'
    END 

	--CHECK OPEN CASES COUNT
	IF @bitOpencases = 1 AND @OpenCaseCount > 0 
    BEGIN 
		SET @vcAlert = @vcAlert + '<a href="#" onclick="OpenAlertDetail(3,'+ CAST(@numDivisionID AS VARCHAR(18)) +',0)"><img alt="" src="../images/icons/headphone_mic.png" style="height: 16px; vertical-align: middle; padding-bottom: 2px;" /></a>&nbsp;'
    END 

	--CHECK OPEN PROJECT COUNT
	IF @bitOpenProject = 1 AND @OpenProjectCount > 0 
    BEGIN 
		SET @vcAlert = @vcAlert + '<a href="#" onclick="OpenAlertDetail(4,'+ CAST(@numDivisionID AS VARCHAR(18)) +',0)"><img alt="" src="../images/Compass-16.gif" style="height: 16px; vertical-align: middle; padding-bottom: 2px;" /></a>&nbsp;'  
    END 

	--CHECK UNREAD MAIL COUNT
	--IF @bitUnreadEmail = 1 AND @UnreadEmailCount > 0 
 --   BEGIN 
 --      SET @vcAlert = @vcAlert + '<a title="Recent Correspondance" class="hyperlink" onclick="OpenCorresPondance('+ CAST(@numContactID AS VARCHAR(10)) + ');">(' + CAST(@UnreadEmailCount AS VARCHAR(10)) + ')</a>&nbsp;'
 --   END
	
	--CHECK ACTIVE CAMPAIGN COUNT
	IF @bitCampaign = 1 AND ISNULL(@numECampaignID,0) > 0
	BEGIN
		
		IF @CampaignDTLCount = 0
		BEGIN
			SET @vcAlert = @vcAlert + '<img alt="" src="../images/comflag.png" style="height: 16px; vertical-align: middle; padding-bottom: 2px;" />&nbsp;'  
		END
		ELSE
		BEGIN
			SET @vcAlert = @vcAlert + '<img alt="" src="../images/GreenFlag.png" style="height: 16px; vertical-align: middle; padding-bottom: 2px;" />&nbsp;'  
		END
	END 

	--CHECK COUNT OF ACTION ITEM
	IF @bitOpenActionItem = 1 AND @OpenActionItemCount > 0 
	BEGIN
		SET @vcAlert = @vcAlert + '<a href="#" onclick="OpenAlertDetail(5,'+ CAST(@numDivisionID AS VARCHAR(18)) +',' + CAST(@numContactID AS VARCHAR(18)) + ')"><img alt="" src="../images/MasterList-16.gif" style="height: 16px; vertical-align: middle; padding-bottom: 2px;" /></a>&nbsp;'
	END

	RETURN @vcAlert
END
GO
/****** Object:  StoredProcedure [dbo].[Usp_cflDeleteField]    Script Date: 07/26/2008 16:15:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec Usp_cflDeleteField 2  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_cfldeletefield')
DROP PROCEDURE usp_cfldeletefield
GO
CREATE PROCEDURE [dbo].[Usp_cflDeleteField]      
@fld_id as numeric(9)=NULL,
@numDomainID AS NUMERIC
as      
      
IF @numDomainID >0
BEGIN
	DELETE FROM DycFormConfigurationDetails WHERE bitCustom =1 AND numFieldID = @fld_id AND numDomainId=@numDomainID -- Bug id 278 permenant fix 

	delete from listdetails where numlistID=(select numlistID from  CFW_Fld_Master where fld_id=@fld_id)    
	delete from listmaster where numListID=(select numlistID from  CFW_Fld_Master where fld_id=@fld_id)
	delete from CFW_Fld_Values_Product where Fld_ID=@fld_id   
	delete from CFW_FLD_Values where Fld_ID=@fld_id    
	delete from CFW_FLD_Values_Case where fld_id=@fld_id    
	delete from CFW_FLD_Values_Cont where fld_id=@fld_id    
	delete from CFW_FLD_Values_Item where fld_id=@fld_id      
	delete from CFW_Fld_Values_Opp where fld_id=@fld_id 
	DELETE FROM [dbo].[CFW_Fld_Values_OppItems] where fld_id=@fld_id
	DELETE FROM ItemAttributes WHERE FLD_ID=@fld_id
	
	DELETE FROM dbo.CFW_Validation WHERE numFieldID = @fld_id 
	delete from CFW_Fld_Dtl where numFieldId=@fld_id     
	delete from CFW_Fld_Master where fld_id=@fld_id
	
END      
GO

/****** Object:  StoredProcedure [dbo].[USP_DeleteCartItem]    Script Date: 11/08/2011 17:28:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_DeleteCartItem' ) 
                    DROP PROCEDURE USP_DeleteCartItem
                    Go
CREATE PROCEDURE [dbo].[USP_DeleteCartItem]
    (
      @numUserCntId NUMERIC(18, 0),
      @numDomainId NUMERIC(18, 0),
      @vcCookieId VARCHAR(100),
      @numItemCode NUMERIC(9, 0) = 0,
      @bitDeleteAll BIT=0,
	  @numSiteID NUMERIC(18,0) = 0
    )
AS 
BEGIN
	IF @bitDeleteAll = 0
	BEGIN
		IF @numItemCode <> 0 
		BEGIN
			-- REMOVE DISCOUNT APPLIED TO ALL ITEMS FROM PROMOTION TRIGGERED OF ITEM TO BE DELETED
			UPDATE 
				CartItems
			SET 
				PromotionDesc='',PromotionID=0,fltDiscount=0,monTotAmount=(monPrice*numUnitHour),monTotAmtBefDiscount=(monPrice*numUnitHour),vcCoupon=NULL,bitParentPromotion=0
			WHERE
				PromotionID=(SELECT TOP 1 PromotionID FROM CartItems WHERE numDomainId = @numDomainId AND numUserCntId = @numUserCntId AND numItemCode = @numItemCode AND vcCookieId=@vcCookieId AND ISNULL(bitParentPromotion,0)=1)
				AND numDomainId = @numDomainId
				AND numUserCntId = @numUserCntId
				AND vcCookieId=@vcCookieId
				AND ISNULL(bitParentPromotion,0)=0

			--DELETE FROM CartItems WHERE numDomainId = @numDomainId AND numUserCntId = @numUserCntId AND numItemCode = @numItemCode	
			
			DELETE FROM CartItems 
			WHERE numDomainId = @numDomainId AND numUserCntId = @numUserCntId
				AND (numItemCode = @numItemCode 
					OR numItemCode IN (SELECT numItemCode FROM SimilarItems WHERE numParentItemCode = @numItemCode AND numDomainId = @numDomainId ))	--AND bitrequired = 1

			EXEC USP_ManageECommercePromotion @numUserCntId,@numDomainId,@vcCookieId,0,@numSiteID
		END
		ELSE
		BEGIN
			UPDATE 
				CartItems
			SET 
				PromotionDesc='',PromotionID=0,fltDiscount=0,monTotAmount=(monPrice*numUnitHour),monTotAmtBefDiscount=(monPrice*numUnitHour),vcCoupon=NULL,bitParentPromotion=0
			WHERE
				numDomainId = @numDomainId
				AND numUserCntId = @numUserCntId
				AND vcCookieId=@vcCookieId

			DELETE FROM CartItems 
			WHERE numDomainId = @numDomainId AND numUserCntId = 0 AND vcCookieId = @vcCookieId		
		END
	END
	ELSE
	BEGIN
		DELETE FROM CartItems WHERE numDomainId = @numDomainId AND numUserCntId = @numUserCntId	AND vcCookieId <> ''
	END
END

 



/****** Object:  StoredProcedure [dbo].[usp_DeleteProspect]    Script Date: 07/26/2008 16:15:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_deleteprospect')
DROP PROCEDURE usp_deleteprospect
GO
CREATE PROCEDURE [dbo].[usp_DeleteProspect]
	@numCompanyID numeric,
	@numDivisionID numeric,
	@numUserID numeric,
	@CRMType tinyint   
--
AS
BEGIN
	DECLARE @RowCount numeric
	SELECT @RowCount = 0
	BEGIN TRANSACTION
		DELETE FROM Communication WHERE numDivisionID=@numDivisionID AND numContactId IN (SELECT numContactId FROM AdditionalContactsInformation WHERE numDivisionID = @numDivisionID)
		DELETE FROM TimeAndExpense WHERE numOppID IN (SELECT numOppId FROM OpportunityMaster WHERE numDivisionID = @numDivisionID)
		DELETE FROM OpportunityItems WHERE numOppID IN (SELECT numOppId FROM OpportunityMaster WHERE numDivisionID = @numDivisionID)
		DELETE FROM OpportunityMaster WHERE numDivisionID = @numDivisionID
		DELETE FROM AdditionalContactsInformation WHERE numDivisionID=@numDivisionID
		DELETE FROM DivisionMaster WHERE numDivisionID=@numDivisionID
	
		SELECT @RowCount = Count(*) FROM DivisionMaster WHERE numCompanyID=@numCompanyID
		IF @RowCount=0
		BEGIN
			DELETE FROM CompanyInfo WHERE numCompanyID=@numCompanyID AND numCreatedBy=@numUserID
		END
	COMMIT
END
GO
/****** Object:  StoredProcedure [dbo].[usp_DeleteSelectedSearchRecords]    Script Date: 07/26/2008 16:15:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By: Debasish Tapan Nag                                
--Purpose: Deletes the selected Records of Advance Search              
--Created Date: 08/22/2005                           
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_deleteselectedsearchrecords')
DROP PROCEDURE usp_deleteselectedsearchrecords
GO
CREATE PROCEDURE [dbo].[usp_DeleteSelectedSearchRecords]              
  @vcEntityIdList NVarchar(4000)              
AS                                              
BEGIN                               
 SET @vcEntityIdList = @vcEntityIdList + ','              
 DECLARE @intFirstComma integer                
 DECLARE @intFirstUnderScore integer                
              
 DECLARE @numCompanyId NUMERIC(9)              
 DECLARE @numContactId NUMERIC(9)              
 DECLARE @numDivisionId NUMERIC(9)              
              
 DECLARE @vcCompContDivValue NVarchar(30)              
             
 DECLARE @intDivCount numeric                     
 DECLARE @intContCount numeric         
          
 DECLARE @DeletionFlag Int          
 SET @DeletionFlag = 1        
           
 SET @intFirstComma = CHARINDEX(',', @vcEntityIdList, 0)                
              
 WHILE @intFirstComma > 0              
 BEGIN              
 SET @vcCompContDivValue = SUBSTRING(@vcEntityIdList,0,@intFirstComma)              
 SET @intFirstUnderScore = CHARINDEX('_', @vcCompContDivValue, 0)              
 SET @numCompanyId = SUBSTRING(@vcCompContDivValue, 0, @intFirstUnderScore)              
 SET @vcCompContDivValue = SUBSTRING(@vcCompContDivValue, @intFirstUnderScore+1, Len(@vcCompContDivValue))              
 SET @intFirstUnderScore = CHARINDEX('_', @vcCompContDivValue, 0)              
 SET @numContactId = SUBSTRING(@vcCompContDivValue, 0, @intFirstUnderScore)              
 SET @numDivisionId = SUBSTRING(@vcCompContDivValue, @intFirstUnderScore+1, Len(@vcCompContDivValue))              
 PRINT  @numCompanyId              
 PRINT  @numContactId              
 PRINT  @numDivisionId          
     /*----------------DELETION STARTS HERE----------------------*/        
 DELETE FROM AOIContactLink WHERE numContactId=@numContactID            
 PRINT 'AOIContactLink Deleted'        
 DELETE FROM Communication WHERE numContactId=@numContactID and numDivisionID=@numDivisionID            
 PRINT 'Communication Deleted'       
    
  --Deleting ExtraNetAccounts        
 DECLARE @numExtranetID as Numeric        
 SELECT @numExtranetID = numExtranetID FROM ExtranetAccountsDtl WHERE numContactId = @numContactID           
 DELETE FROM ExtranetAccountsDtl WHERE numContactId=@numContactID            
 PRINT 'ExtranetAccountsDtl Deleted'        
 DECLARE @numExtranetIDCount Numeric        
 SELECT @numExtranetIDCount = COUNT(numExtranetID) FROM ExtranetAccountsDtl WHERE numExtranetID IN (SELECT numExtranetID FROM ExtarnetAccounts WHERE numDivisionID=@numDivisionID AND numCompanyId=@numCompanyID)        
        IF(@numExtranetIDCount = 0)        
 BEGIN        
  DELETE FROM ExtarnetAccounts WHERE  numDivisionID=@numDivisionID AND  numCompanyId=@numCompanyId         
  PRINT 'ExtarnetAccounts Deleted'        
 END        
        
 --Deleting Campaigns        
 DECLARE @numCampaignId Numeric        
 SELECT @numCampaignId = numCampaignId FROM CampaignDetails WHERE numContactId=@numContactID AND  numCompanyId=@numCompanyID        
 DELETE FROM CampaignDetails WHERE numContactId=@numContactID AND  numCompanyId=@numCompanyID          
 PRINT 'CampaignDetails Deleted'        
 /*
 DECLARE @CampaignDivisionCount as Numeric        
 SELECT @CampaignDivisionCount = COUNT(numContactId) FROM CampaignDetails WHERE numContactId IN (SELECT DISTINCT numContactId FROM DivisionMaster WHERE numDivisionId = @numDivisionID)        
 IF(@CampaignDivisionCount=0)        
 BEGIN        
  DELETE FROM CampaignDivision WHERE numDivisionID=@numDivisionID AND numCampaignID = @numCampaignId        
  PRINT 'CampaignDivision Deleted'        
 END        
 */     
 --Deleting Cases        
 DECLARE @numCaseId as Numeric        

 SELECT @numCaseId = numCaseId FROM Cases  WHERE numDivisionID=@numDivisionID AND  numContactId=@numContactID        
 DELETE FROM CaseSolutions WHERE numCaseId = @numCaseId        
 PRINT 'CaseSolutions Deleted'        
 DELETE FROM Cases WHERE numDivisionID=@numDivisionID AND  numContactId=@numContactID AND numCaseId = @numCaseId        
 PRINT 'Cases Deleted'        
        
        
 DELETE FROM FollowUpHistory WHERE numDivisionID=@numDivisionID                
 PRINT 'FollowUpHistory Deleted'        
 DELETE FROM Leads WHERE  numCompanyId=@numCompanyID        
 PRINT 'Leads Deleted'        
 DELETE FROM UniversalSupportKeyMaster WHERE numDivisionID=@numDivisionID        
 PRINT 'UniversalSupportKeyMaster Deleted'        
        
        
 DECLARE @numProId as Numeric        
 SELECT @numProId = numProId FROM ProjectsContacts WHERE numContactId = @numContactID           
        
 DECLARE @numOppId as Numeric        
 SELECT @numOppId = numOppId FROM OpportunityMaster  WHERE numDivisionID=@numDivisionID AND  numContactId=@numContactId         
 DECLARE @numProjectForOppId as Numeric        
 SELECT @numProjectForOppId = numProId FROM ProjectsMaster  WHERE numOppId=@numOppId         
        
 DELETE FROM ProjectsContacts WHERE numProId = @numProId and numContactId = @numContactID          
 DELETE FROM ProjectsContacts WHERE numProId = @numProjectForOppId        
 PRINT 'ProjectsContacts Deleted'        
 DELETE FROM ProjectsSubStageDetails WHERE numProId = @numProId           
 DELETE FROM ProjectsSubStageDetails WHERE numProId = @numProjectForOppId           
 PRINT 'ProjectsSubStageDetails Deleted'        
 DELETE FROM ProjectsStageDetails WHERE numProId = @numProId           
 DELETE FROM ProjectsStageDetails WHERE numProId = @numProjectForOppId           
 PRINT 'ProjectsStageDetails Deleted'        
 DELETE FROM ProjectsDependency WHERE numProId = @numProId           
 DELETE FROM ProjectsDependency WHERE numProId = @numProjectForOppId           
 PRINT 'ProjectsDependency Deleted'        
 DELETE FROM ProjectsTime WHERE numProId = @numProId           
 DELETE FROM ProjectsTime WHERE numProId = @numProjectForOppId           
 PRINT 'ProjectsTime Deleted'        
 DELETE FROM ProjectsExpense WHERE numProId = @numProId           
 DELETE FROM ProjectsExpense WHERE numProId = @numProjectForOppId           
 PRINT 'ProjectsExpense Deleted'        
 DELETE FROM ProjectsMaster WHERE numProId = @numProId            
 DELETE FROM ProjectsMaster WHERE numProId = @numProjectForOppId            
 PRINT 'ProjectsMaster Deleted'        
        
          
 DELETE FROM OpportunitySubStageDetails WHERE numOppId = @numOppId        
 PRINT 'OpportunitySubStageDetails Deleted'        
 DELETE FROM OpportunityStageDetails WHERE numOppId = @numOppId        
 PRINT 'OpportunityStageDetails Deleted'        
 DELETE FROM OpportunityDependency WHERE numOpportunityId = @numOppId        
 PRINT 'OpportunityDependency Deleted'        
 DELETE FROM OpportunityExpense WHERE numOppId = @numOppId        
 PRINT 'OpportunityExpense Deleted'        
 DELETE FROM OpportunityItems WHERE numOppId = @numOppId        
 PRINT 'OpportunityTime Deleted'      
 DELETE FROM TimeAndExpense WHERE numOppId = @numOppId        
 PRINT 'Opportunity Time And Expense Deleted'   
        
 DECLARE @numOppBizDocId as Numeric        
 SELECT @numOppBizDocId = numBizDocId FROM OpportunityBizDocs WHERE numOppId = @numOppId        
 DELETE FROM OpportunityTime WHERE numOppId = @numOppId        
 PRINT 'OpportunityTime Deleted'        
 DELETE FROM OpportunityBizDocDtl WHERE numBizDocId = @numOppBizDocId        
 PRINT 'OpportunityBizDocDtl Deleted'        
 DELETE FROM OpportunityBizDocs WHERE numOppId = @numOppId        
 PRINT 'OpportunityBizDocs Deleted'        
        
 DELETE FROM OpportunityContact WHERE numOppId = @numOppId --Delete all opportunities for the contact (whatever the associated contacts in OpportunityContact is)    
 PRINT 'Opportunity Contact Deleted'        
 DELETE FROM ProjectsMaster WHERE numOppId = @numOppId            
 PRINT 'ProjectsMaster Deleted'     
 DELETE FROM OpportunityMaster WHERE numOppId = @numOppId AND numContactId=@numContactId         
  
 PRINT 'SurveyRespondentsMaster Deleted'     
 UPDATE SurveyRespondentsMaster SET numRegisteredRespondentContactId = NULL WHERE numRegisteredRespondentContactId = @numContactId      
  
        
 DELETE FROM AdditionalContactsInformation WHERE numContactId=@numContactID                
 PRINT 'Contact Deleted'        
 SELECT @intContCount=COUNT(*) FROM AdditionalContactsInformation WHERE numDivisionID=@numDivisionID                
      IF @intContCount=0 --If There are no more Contacts for the selected division, then delete division.                
      BEGIN                
          DELETE FROM ExtarnetAccounts WHERE  numDivisionID=@numDivisionID        
          DELETE FROM CompanyAssociations WHERE numDivisionID=@numDivisionID OR numAssociateFromDivisionID =@numDivisionID        
          PRINT 'CompanyAssociations Deleted'        
          DELETE FROM DivisionMaster WHERE numDivisionID=@numDivisionID                
          PRINT 'Division Deleted'        
          SELECT @intDivCount=COUNT(*) FROM  DivisionMaster WHERE numCompanyId=@numCompanyID                      
        
          IF @intDivCount=0      -- If there are no more Divisions for the selected Company then Delete Company.                
          BEGIN                
              DELETE FROM CompanyInfo WHERE numCompanyId=@numCompanyID                   
              PRINT 'Company Deleted'        
          END        
      END           
 IF @@ERROR <> 0        
  SET @DeletionFlag = 0        
     /*----------------DELETION ENDS HERE----------------------*/        
              
 SET @vcEntityIdList = SUBSTRING(@vcEntityIdList,@intFirstComma+1,Len(@vcEntityIdList))                
 SET @intFirstComma = CHARINDEX(',', @vcEntityIdList, 0)                
 END              
            
            
            
 SELECT @DeletionFlag            
 RETURN              
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DelPromotionOffer')
DROP PROCEDURE USP_DelPromotionOffer
GO
CREATE PROCEDURE [dbo].[USP_DelPromotionOffer]
	@numDomainID AS NUMERIC(18,0),
	@numProId AS NUMERIC(18,0)
AS
BEGIN TRY
BEGIN TRANSACTION
	
	DELETE FROM PromotionOfferOrganizations WHERE numProId = @numProId 
	DELETE FROM PromotionOfferItems WHERE numProId = @numProId AND tintRecordType IN (5,6)
	DELETE FROM PromotionOfferSites WHERE numPromotionID=@numProId		
	DELETE FROM PromotionOfferOrderBased WHERE numProId=@numProId	
	DELETE FROM PromotionOffer WHERE numDomainId=@numDomainID AND numProId=@numProId
		
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
/****** Object:  StoredProcedure [dbo].[USP_ECampaignHstr]    Script Date: 07/26/2008 16:15:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_ecampaignhstr')
DROP PROCEDURE usp_ecampaignhstr
GO
CREATE PROCEDURE [dbo].[USP_ECampaignHstr] 
    @numConatctID AS NUMERIC(9),
    @numConEmailCampID NUMERIC(9),
	@ClientTimeZoneOffset Int 
AS 
		SELECT
			ECampaign.vcECampName,
			(CASE 
				WHEN ISNULL(ECampaign.bitDeleted,0) = 0 
				THEN CASE WHEN ISNULL(ConECampaign.bitEngaged,0) = 0 THEN 'Disengaged' ELSE 'Engaged' END
				ELSE 'Campaign Deleted'
			END) AS [Status],
			(CASE 
				WHEN ISNULL(ECampaignDTLs.numEmailTemplate,0) > 0 THEN GenericDocuments.vcDocName
				WHEN ISNULL(ECampaignDTLs.numActionItemTemplate,0) > 0 THEN tblActionItemData.TemplateName
				ELSE ''
			END) AS Template,
			ISNULL(dbo.FormatedDateTimeFromDate(DATEADD(MINUTE,ISNULL(ECampaign.fltTimeZone,0) * 60,ConECampaignDTL.dtExecutionDate),ACI.[numDomainID]),'') vcExecutionDate,
			(CASE 
				WHEN ISNULL(ConECampaignDTL.bitSend,0)=1 AND ISNULL(ConECampaignDTL.tintDeliveryStatus,0)=0 THEN 'Failed'
				WHEN ISNULL(ConECampaignDTL.bitSend,0)=1 AND ISNULL(ConECampaignDTL.tintDeliveryStatus,0)=1 THEN 'Success'
				ELSE 'Pending'
			END) AS SendStatus,
			--ISNULL(dbo.FormatedDateTimeFromDate(DATEADD(MINUTE,ISNULL(ECampaign.fltTimeZone,0)*60, ConECampaignDTL.[bintSentON]),ACI.[numDomainID]),'') vcSendDate,
			convert(varchar(20),dateAdd(minute, -@ClientTimeZoneOffset,ConECampaignDTL.[bintSentON] )) as vcSendDate, 

			ISNULL(ConECampaignDTL.bitEmailRead,0) bitEmailRead,
			(SELECT COUNT(*) FROM ConECampaignDTLLinks WHERE numConECampaignDTLID = ConECampaignDTL.numConECampDTLID) AS NoOfLinks,
			(SELECT COUNT(*) FROM ConECampaignDTLLinks WHERE ISNULL(bitClicked,0) = 1 AND numConECampaignDTLID = ConECampaignDTL.numConECampDTLID) AS NoOfLinkClicked
		FROM ConECampaignDTL
		INNER JOIN ECampaignDTLs ON ConECampaignDTL.numECampDTLID = ECampaignDTLs.numECampDTLId
		INNER JOIN ConECampaign	ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID
		INNER JOIN ECampaign ON	ConECampaign.numECampaignID = ECampaign.numECampaignID
		INNER JOIN [AdditionalContactsInformation] ACI ON ACI.[numContactId] = ConECampaign.[numContactID]
		LEFT JOIN GenericDocuments ON GenericDocuments.numGenericDocID = ECampaignDTLs.numEmailTemplate
		LEFT JOIN tblActionItemData ON tblActionItemData.RowID = ECampaignDTLs.numActionItemTemplate
		WHERE 
			(ConECampaign.numConEmailCampID = @numConEmailCampID OR ISNULL(@numConEmailCampID,0)=0) AND
			(ACI.numContactID = @numConatctID OR ISNULL(@numConatctID,0)=0)
		ORDER BY [numConECampDTLID] ASC 
GO
/****** Object:  StoredProcedure [dbo].[USP_ECampaignList]    Script Date: 07/26/2008 16:15:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj        
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_ecampaignlist')
DROP PROCEDURE usp_ecampaignlist
GO
CREATE PROCEDURE [dbo].[USP_ECampaignList]            
 @numUserCntID numeric(9)=0,            
 @numDomainID numeric(9)=0,              
 @SortChar char(1)='0',           
 @CurrentPage int,            
 @PageSize int,            
 @TotRecs int output,            
 @columnName as Varchar(50),            
 @columnSortOrder as Varchar(10)              
as            
            
            
               
             
            
create table #tempTable ( ID INT IDENTITY PRIMARY KEY,             
 numECampaignID numeric(9),            
 vcECampName varchar(100),            
 txtDesc text,
 NoOfUsersAssigned INT,
 NoOfEmailSent INT,
 NoOfEmailOpened INT,
 NoOfLinks INT,
 NoOfLinkClicked INT        
 )            
            
            
declare @strSql as varchar(8000)            
          
          
            
set @strSql='select 
numECampaignID,          
vcECampName,          
txtDesc, 
ISNULL((SELECT COUNT(*) FROM ConECampaign WHERE numECampaignID = ECampaign.numECampaignID AND ISNULL(ConECampaign.bitEngaged,0) = 1 ),0) AS NoOfUsersAssigned,
ISNULL((SELECT COUNT(*) FROM ConECampaignDTL INNER JOIN ECampaignDTLs ON ConECampaignDTL.numECampDTLID = ECampaignDTLs.numECampDTLId WHERE ISNULL(ECampaignDTLs.numEmailTemplate,0) > 0 AND numECampID = ECampaign.numECampaignID AND ISNULL(bitSend,0) = 1 AND ISNULL(tintDeliveryStatus,0) = 1),0) AS NoOfEmailSent,
ISNULL((SELECT COUNT(*) FROM ConECampaignDTL INNER JOIN ECampaignDTLs ON ConECampaignDTL.numECampDTLID = ECampaignDTLs.numECampDTLId WHERE ISNULL(ECampaignDTLs.numEmailTemplate,0) > 0 AND numECampID = ECampaign.numECampaignID AND ISNULL(bitSend,0) = 1 AND ISNULL(tintDeliveryStatus,0) = 1 AND ISNULL(bitEmailRead,0) = 1),0) AS NoOfEmailOpened,
(SELECT COUNT(*) FROM ConECampaignDTLLinks WHERE numConECampaignDTLID IN (SELECT ISNULL(CECDTL.numConECampDTLID,0) FROM ConECampaignDTL CECDTL WHERE numConECampID IN (SELECT ISNULL(CEC.numConEmailCampID,0) FROM ConECampaign CEC WHERE numECampaignID = ECampaign.numECampaignID))) AS NoOfLinks,
(SELECT COUNT(*) FROM ConECampaignDTLLinks WHERE ISNULL(bitClicked,0) = 1 AND numConECampaignDTLID IN (SELECT ISNULL(CECDTL.numConECampDTLID,0) FROM ConECampaignDTL CECDTL WHERE numConECampID IN (SELECT ISNULL(CEC.numConEmailCampID,0) FROM ConECampaign CEC WHERE numECampaignID = ECampaign.numECampaignID))) AS NoOfLinkClicked
from ECampaign          
where ISNULL(bitDeleted,0) = 0 AND numDomainID= '+ convert(varchar(15),@numDomainID)           
            
if @SortChar <>'0' set @strSql=@strSql + ' And vcECampName like '''+@SortChar+'%'''          
if @columnName<>''  set @strSql=@strSql + ' ORDER BY ' + @columnName +' '+ @columnSortOrder   
  
print @strSql         
insert into #tempTable
(            
	numECampaignID,            
	vcECampName,            
	txtDesc,
	NoOfUsersAssigned,
	NoOfEmailSent,
	NoOfEmailOpened,
	NoOfLinks,
	NoOfLinkClicked 
)            
exec 
(
	@strSql
)             
            
  declare @firstRec as integer             
  declare @lastRec as integer            
 set @firstRec= (@CurrentPage-1) * @PageSize            
     set @lastRec= (@CurrentPage*@PageSize+1)            
--Don't change column sequence because its being used for Drip Campaign DropDown in Contact Details
select numECampaignID,vcECampName,txtDesc,NoOfUsersAssigned,NoOfEmailSent,NoOfEmailOpened,NoOfLinks,NoOfLinkClicked from #tempTable where ID > @firstRec and ID < @lastRec            
set @TotRecs=(select count(*) from #tempTable)            
drop table #tempTable
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_End(Disengage)SelectedCampaign')
DROP PROCEDURE [USP_End(Disengage)SelectedCampaign]
GO
-- =============================================
-- Author:		<Priya Sharma>
-- Create date: <06/01/2018>
-- Description:	<Disengage selected Camapigns>
-- =============================================
CREATE PROCEDURE [dbo].[USP_End(Disengage)SelectedCampaign]
	@numContactID NUMERIC(9),
	@numECampaignID NUMERIC(9)
AS
  BEGIN
    DECLARE  @numConEmailCampID NUMERIC(9)
    
    SELECT @numConEmailCampID = [numConEmailCampID]
    FROM   [ConECampaign]
    WHERE  [numContactID] = @numContactID
           AND [numECampaignID] = @numECampaignID
        
	UPDATE [ConECampaign] SET [bitEngaged] = 0 WHERE [numConEmailCampID]=@numConEmailCampID
	UPDATE AdditionalContactsInformation SET numECampaignID = NULL WHERE numContactId = @numContactID

END
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetAutomatedFollowups')
DROP PROCEDURE USP_GetAutomatedFollowups
GO

--exec [USP_GetAutomatedFollowups] 279573,72

----Created By Priya (15 Jan 2018)

CREATE PROCEDURE [dbo].[USP_GetAutomatedFollowups]
	@numContactId NUMERIC,
	@numDomainId AS NUMERIC(9)
AS
BEGIN
SET NOCOUNT ON; 
--DECLARE FollowUpCampaign as  VARCHAR(MAX)
--DECLARE	LastFollowUp VARCHAR(MAX)
--DECLARE	NextFollowUp VARCHAR(MAX)

	SELECT

	(CASE WHEN 
			ISNULL((SELECT COUNT(*) FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID
										WHERE ConECampaign.numContactID = A.numContactID AND  ConECampaign.numECampaignID = A.numECampaignID AND bitEngaged =1 AND ISNULL(bitSend,0) = 1),0) 
			= ISNULL((SELECT COUNT(*) FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID
									     WHERE ConECampaign.numContactID = A.numContactID AND ConECampaign.numECampaignID = A.numECampaignID AND bitEngaged =1 ),0)
	THEN

	ISNULL((SELECT CONCAT('<img alt="Campaign completed" height="16px" width="16px" title="Campaign Completed" src="../images/comflag.png"> ',
							vcECampName, ' (' + 
									 CAST(ISNULL((SELECT COUNT(*) FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID
										WHERE ConECampaign.numContactID = A.numContactID AND  ECampaign.numECampaignID = ConECampaign.numECampaignID AND bitEngaged =1 AND ISNULL(bitSend,0) = 1 ),0) AS varchar) + ' Of ' +
									 CAST(ISNULL((SELECT COUNT(*) FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID
									     WHERE ConECampaign.numContactID = A.numContactID AND ECampaign.numECampaignID = ConECampaign.numECampaignID AND bitEngaged =1),0) AS VARCHAR) + ') ' 
		 ) FROM [ECampaign] WHERE numECampaignID = A.numECampaignID),'')
	ELSE

	ISNULL((SELECT CONCAT('<img alt="Active Campaign" height="16px" width="16px" title="Active Campaign" src="../images/Flag_Green.png"> ',
							vcECampName, ' (' + 
									 CAST(ISNULL((SELECT COUNT(*) FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID
										WHERE ConECampaign.numContactID = A.numContactID AND  ECampaign.numECampaignID = ConECampaign.numECampaignID AND bitEngaged =1 AND ISNULL(bitSend,0) = 1 ),0) AS varchar) + ' Of ' +
									 CAST(ISNULL((SELECT COUNT(*) FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID
									     WHERE ConECampaign.numContactID = A.numContactID AND ECampaign.numECampaignID = ConECampaign.numECampaignID AND bitEngaged =1),0) AS VARCHAR) + ') ' 
		 ) FROM [ECampaign] WHERE numECampaignID = A.numECampaignID),'')

	END
	)  FollowUpCampaign,

	dbo.fn_GetFollowUpDetails(@numContactId,1,@numDomainId)LastFollowUp,
	dbo.fn_GetFollowUpDetails(@numContactId,2,@numDomainId)NextFollowUp
	--dbo.fn_GetFollowUpDetails(@numContactId,3,@numDomainId)LastEmailStatus


FROM AdditionalContactsInformation A
WHERE A.numContactId = @numContactId AND A.numDomainID = @numDomainId 


END


GO


GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetBillPaymentDetails' ) 
    DROP PROCEDURE USP_GetBillPaymentDetails
GO
-- exec USP_GetBillPaymentDetails 0,1,0,0
CREATE PROCEDURE [dbo].[USP_GetBillPaymentDetails]
    @numBillPaymentID NUMERIC(18, 0) ,
    @numDomainID NUMERIC ,
    @ClientTimeZoneOffset INT,
    @numDivisionID NUMERIC=0,
    @numCurrencyID NUMERIC=0,
    @CurrentPage INT,
    @PageSize INT,
    @TotRecs INT OUTPUT,
    @numAccountClass NUMERIC(18,0)=0
AS 
    SET NOCOUNT ON
    SET TRANSACTION ISOLATION LEVEL READ COMMITTED
		DECLARE @numBaseCurrencyID NUMERIC
	    DECLARE @BaseCurrencySymbol nVARCHAR(3)
	    
        SELECT @numBaseCurrencyID = ISNULL(numCurrencyID,0) FROM dbo.Domain WHERE numDomainId=@numDomainId
        SELECT @BaseCurrencySymbol = ISNULL(varCurrSymbol,'') FROM dbo.Currency WHERE numCurrencyID = @numBaseCurrencyID
    
		DECLARE @firstRec AS INTEGER
		DECLARE @lastRec AS INTEGER
		
		SET @firstRec = ( @CurrentPage - 1 ) * @PageSize
		SET @lastRec = ( @CurrentPage * @PageSize + 1 )
		
				  
		IF @numBillPaymentID>0
		BEGIN
			SELECT @numDivisionID=numDivisionID FROM [dbo].[BillPaymentHeader] BPH WHERE BPH.numBillPaymentID=@numBillPaymentID
		END
		
	 SELECT BPH.numBillPaymentID,BPH.dtPaymentDate,BPH.numAccountID,BPH.numDivisionID,BPH.numReturnHeaderID,BPH.numPaymentMethod
	 ,ISNULL(BPH.monPaymentAmount,0) AS monPaymentAmount,ISNULL(BPH.monAppliedAmount,0) AS monAppliedAmount,
	 ISNULL(CH.numCheckHeaderID, 0) numCheckHeaderID,
	 ISNULL(CH.numCheckNo,0) AS numCheckNo,ISNULL(BPH.numReturnHeaderID,0) AS numReturnHeaderID,
	 BPH.numCurrencyID,ISNULL(NULLIF(BPH.fltExchangeRate,0),1) fltExchangeRateBillPayment
	,ISNULL(C.varCurrSymbol,'') varCurrSymbol
	,@BaseCurrencySymbol BaseCurrencySymbol,ISNULL(monRefundAmount,0) AS monRefundAmount,ISNULL(BPH.numAccountClass,0) AS numAccountClass
	  FROM  [dbo].[BillPaymentHeader] BPH
			LEFT JOIN dbo.CheckHeader CH ON CH.numReferenceID = BPH.numBillPaymentID AND CH.tintReferenceType = 8
			LEFT JOIN dbo.Currency C ON C.numCurrencyID = BPH.numCurrencyID
			WHERE BPH.numBillPaymentID=@numBillPaymentID
		
		--For Edit Bill Payment Get Details
		SELECT Row_number() OVER ( ORDER BY dtDueDate ASC ) AS row,* INTO #tempBillPayment FROM 
		(
			SELECT  Case When isnull(BH.bitLandedCost,0)=1 then  isnull(BH.numOppId,0) else OBD.numOppId end AS numoppid,
					  BPH.numDivisionID,BPD.[numOppBizDocsID],BPD.[numBillID],
					  CASE WHEN(BPD.numBillID>0) THEN  Case When isnull(BH.bitLandedCost,0)=1 THEN 'Laned Cost-' ELSE 'Bill-' END + CONVERT(VARCHAR(10),BH.numBillID)
					  else OBD.vcbizdocid END AS [Name],
					  CASE BPD.[tintBillType] WHEN 1 THEN OBD.monDealAmount WHEN 2 THEN BH.monAmountDue END monOriginalAmount,
					  CASE BPD.[tintBillType] WHEN 1 THEN ( OBD.monDealAmount - OBD.monAmountPaid + BPD.[monAmount]) WHEN 2 THEN BH.monAmountDue - BH.monAmtPaid + BPD.[monAmount] END monAmountDue,
					  CASE BPD.[tintBillType] WHEN 1 THEN '' WHEN 2 THEN BH.vcReference END Reference,
					  CASE BPD.[tintBillType] WHEN 1 THEN dbo.fn_GetComapnyName(OM.numDivisionId) WHEN 2 THEN dbo.fn_GetComapnyName(BH.numDivisionID) END vcCompanyName,
					  BPD.[tintBillType],
					  CASE BPD.[tintBillType]
						  WHEN 1
						  THEN CASE ISNULL(OM.bitBillingTerms, 0)
								 WHEN 1
								 --THEN [dbo].[FormatedDateFromDate](DATEADD(dd,CONVERT(INT, ISNULL(dbo.fn_GetListItemName(ISNULL(OM.intBillingDays,0)), 0)),OBD.dtFromDate),@numDomainId)
								 THEN [dbo].[FormatedDateFromDate](DATEADD(dd,CONVERT(INT, ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(OM.intBillingDays,0)), 0)),OBD.dtFromDate),@numDomainId)
								 WHEN 0
								 THEN [dbo].[FormatedDateFromDate](OBD.[dtFromDate],
																  @numDomainId)
							   END
						  WHEN 2
						  THEN [dbo].[FormatedDateFromDate](BH.dtDueDate,@numDomainID) END AS DueDate,
						  BPD.[monAmount] AS monAmountPaid,1 bitIsPaid
						  ,ISNULL(NULLIF(OM.fltExchangeRate,0),1) fltExchangeRateOfOrder
						  ,BH.dtDueDate 
				FROM    [dbo].[BillPaymentHeader] BPH
						INNER JOIN dbo.BillPaymentDetails BPD ON BPH.numBillPaymentID = BPD.numBillPaymentID
						LEFT JOIN dbo.OpportunityBizDocs OBD ON BPD.numOppBizDocsID = OBD.numOppBizDocsId
						LEFT JOIN dbo.OpportunityMaster OM ON OBD.numOppId = OM.numOppId
						LEFT JOIN dbo.BillHeader BH ON BH.numBillID = BPD.numBillID
						LEFT JOIN dbo.CheckHeader CH ON CH.numReferenceID = BPH.numBillPaymentID
														AND CH.tintReferenceType = 8
						                                                 
				WHERE   BPH.[numBillPaymentID] = @numBillPaymentID
				AND BPH.numDomainID = @numDomainID
				AND (OM.numCurrencyID = @numCurrencyID OR @numCurrencyID = 0 OR ISNULL(BPD.numOppBizDocsID,0) =0)
	            AND (ISNULL(BPH.numAccountClass,0)=@numAccountClass OR @numAccountClass=0)
	            
				UNION
		--PO Bills For Add Bill Payment
				SELECT  opp.numoppid AS numoppid ,
						opp.numdivisionid AS numDivisionID ,
						OBD.numOppBizDocsId AS numOppBizDocsId ,
						0 AS numBillID ,
						OBD.vcbizdocid AS [name] ,
						OBD.monDealAmount AS monOriginalAmount ,
						( OBD.monDealAmount - OBD.monAmountPaid ) AS monAmountDue ,
	--                    '' AS memo ,
						OBD.vcRefOrderNo AS reference ,
	--                    c.numcompanyid AS numcompanyid ,
						c.vccompanyname AS vccompanyname ,
						1 AS [tintBillType] , -- 1 =PO Bill,2=Regular bill , 3= Bill against liability
						CASE ISNULL(Opp.bitBillingTerms, 0)
						  WHEN 1
						  --THEN [dbo].[FormatedDateFromDate](DATEADD(dd,CONVERT(INT, ISNULL(dbo.fn_GetListItemName(ISNULL(Opp.intBillingDays,0)), 0)),OBD.dtFromDate),@numDomainId)
						  THEN [dbo].[FormatedDateFromDate](DATEADD(dd,CONVERT(INT, ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0)),OBD.dtFromDate),@numDomainId)
						  WHEN 0
						  THEN [dbo].[FormatedDateFromDate](OBD.[dtFromDate],
															@numDomainId)
						END AS DueDate ,
						0 AS monAmountPaid,
						0 bitIsPaid
						,ISNULL(NULLIF(Opp.fltExchangeRate,0),1) fltExchangeRateOfOrder
						,OBD.[dtFromDate] AS dtDueDate
				FROM    OpportunityBizDocs OBD
						INNER JOIN OpportunityMaster Opp ON OBD.numOppId = Opp.numOppId
						INNER JOIN AdditionalContactsInformation ADC ON Opp.numContactId = ADC.numContactId
						INNER JOIN DivisionMaster Div ON Opp.numDivisionId = Div.numDivisionID
														 AND ADC.numDivisionId = Div.numDivisionID
						INNER JOIN CompanyInfo C ON Div.numCompanyID = C.numCompanyId
				WHERE   Opp.numDomainId = @numDomainId
						AND OBD.bitAuthoritativeBizDocs = 1
						AND Opp.tintOppType = 2
						AND ( OBD.monDealAmount - OBD.monAmountPaid ) > 0
						AND (Opp.numDivisionId = @numDivisionID OR ISNULL(@numDivisionID,0)=0)
						AND OBD.numOppBizDocsID NOT IN (SELECT numOppBizDocsID FROM BillPaymentDetails WHERE numBillPaymentID = @numBillPaymentID)
						AND (Opp.numCurrencyID = @numCurrencyID OR @numCurrencyID = 0)
						AND (ISNULL(Opp.numAccountClass,0)=@numAccountClass OR @numAccountClass=0)
				 UNION       
				 --Regular Bills For Add Bill Payment
				SELECT  Case When isnull(BH.bitLandedCost,0)=1 then  isnull(BH.numOppId,0) else 0 end AS numoppid ,
						numDivisionID AS numDivisionID ,
						0 AS numOppBizDocsId ,
						BH.numBillID ,
						Case When isnull(BH.bitLandedCost,0)=1 THEN 'Laned Cost-' ELSE 'Bill-' END + CONVERT(VARCHAR(10),BH.numBillID) AS [name] ,
						BH.monAmountDue AS monOriginalAmount ,
						(BH.monAmountDue - BH.monAmtPaid) AS monAmountDue ,
	--                    '' AS memo ,
						BH.vcReference AS reference ,
	--                    c.numcompanyid AS numcompanyid ,
						dbo.fn_GetComapnyName(BH.numDivisionID) AS vccompanyname ,
						2 AS [tintBillType] , -- 1 =PO Bill,2=Regular bill , 3= Bill against liability
						[dbo].[FormatedDateFromDate](BH.dtDueDate,@numDomainId) AS DueDate ,
						0 AS monAmountPaid,
						0 bitIsPaid
						,ISNULL(BH.fltExchangeRate,1) fltExchangeRateOfOrder
						,BH.dtDueDate
				FROM    dbo.BillHeader BH
				WHERE   BH.numDomainId = @numDomainId
						AND BH.bitIsPaid = 0
						AND (BH.numDivisionId = @numDivisionID OR ISNULL(@numDivisionID,0)=0) 
						AND BH.numBillID NOT IN (SELECT numBillID FROM BillPaymentDetails WHERE numBillPaymentID = @numBillPaymentID)      
						AND (ISNULL(BH.numCurrencyID,@numBaseCurrencyID) = @numCurrencyID OR @numCurrencyID = 0)
						AND (ISNULL(BH.numAccountClass,0)=@numAccountClass OR @numAccountClass=0)
		) [BillPayment]

		SELECT  @TotRecs = COUNT(*)  FROM #tempBillPayment	
			                                                 	
		SELECT * FROM #tempBillPayment	
		WHERE  row > @firstRec 
		   AND row < @lastRec ;				

        DROP TABLE #tempBillPayment	    


GO



/****** Object:  StoredProcedure [dbo].[USP_GetCartItem]    Script Date: 11/08/2011 18:00:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetCartItem' ) 
                    DROP PROCEDURE USP_GetCartItem
                    Go
CREATE PROCEDURE [dbo].[USP_GetCartItem]
(
	@numUserCntId numeric(18,0),
	@numDomainId numeric(18,0),
	@vcCookieId varchar(100),
	@bitUserType BIT =0--if 0 then anonomyous user 1 for logi user
)
AS
	IF @numUserCntId <> 0
	BEGIN
		SELECT DISTINCT numCartId,
			   numParentItemCode,
			   numUserCntId,
			   [dbo].[CartItems].numDomainId,
			   vcCookieId,
			   numOppItemCode,
			   [dbo].[CartItems].numItemCode,
			   (SELECT TOP 1 numCategoryId  FROM dbo.ItemCategory WHERE numItemID =  CartItems.numItemCode AND [dbo].[CartItems].numDomainId = @numDomainId ) AS numCategoryId,
			   (SELECT vcCategoryName FROM [dbo].[Category] WHERE [Category].[numCategoryID]  = (SELECT TOP 1 numCategoryId  FROM dbo.ItemCategory WHERE numItemID =  CartItems.numItemCode AND [dbo].[CartItems].numDomainId = @numDomainId )) AS vcCategoryName,
			   numUnitHour,
			   monPrice,
			   monPrice AS [monListPrice],
			   numSourceId,
			   vcItemDesc,
			   vcItemDesc AS txtItemDesc,
			   [dbo].[CartItems].numWarehouseId,
			   [dbo].[CartItems].vcItemName,
			   vcWarehouse,
			   numWarehouseItmsID,
			   vcItemType,
			   vcAttributes,
			   vcAttrValues,
			   I.bitFreeShipping,
			   fltWeight as numWeight,
			   ISNULL((SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numDomainId=@numDomainId AND bitDefault=1 AND numItemCode=I.numItemCode),'') AS vcPathForTImage,
			   tintOpFlag,
			   bitDiscountType,
			   fltDiscount,
			   monTotAmtBefDiscount,
			   ItemURL,
			   numUOM,
			   vcUOMName,
			   decUOMConversionFactor,
			   numHeight,
			   numLength,
			   numWidth,
			   vcShippingMethod,
			   numServiceTypeId,
			   decShippingCharge,
			   numShippingCompany,
			   vcCoupon,
			   tintServicetype,CAST( dtDeliveryDate AS VARCHAR(30)) dtDeliveryDate,monTotAmount,
			   ISNULL(monTotAmtBefDiscount,0) - ISNULL(monTotAmount,0) AS monTotalDiscountAmount,
			   (SELECT vcModelID FROM dbo.Item WHERE Item.numItemCode =  CartItems.numItemCode) AS [vcModelID]
			   ,(SELECT TOP 1 vcPathForTImage FROM dbo.ItemImages II WHERE II.numItemCode = CartItems.numItemCode AND II.numDomainId= CartItems.numDomainId AND II.bitDefault=1) As vcPathForTImage
			   ,ISNULL(vcUpSellDesc,'') AS [txtUpSellItemDesc],ISNULL(bitPreUpSell,0) [bitPreUpSell], ISNULL(bitPostUpSell,0) [bitPostUpSell], ISNULL(vcUpSellDesc,'') [vcUpSellDesc]
			   ,ISNULL([I].[vcSKU],'') AS [vcSKU]
			   ,I.vcManufacturer
			   ,ISNULL(PromotionID,0) AS PromotionID
			   ,CAST(ISNULL(PromotionDesc,'') AS varchar) AS PromotionDesc
			   ,ISNULL((SELECT IsOrderBasedPromotion FROM PromotionOffer WHERE numProId = [CartItems].PromotionID),0) AS IsOrderBasedPromotion
			   ,I.txtItemDesc AS txtItemDesc 
			   FROM CartItems 
			   LEFT JOIN [dbo].[SimilarItems] AS SI ON SI.[numItemCode] = [dbo].[CartItems].[numItemCode] AND SI.numParentItemCode = CartItems.numParentItem
			   LEFT JOIN [dbo].[Item] AS I ON I.[numItemCode] = [dbo].[CartItems].[numItemCode]
			   --LEFT JOIN [dbo].[WareHouseItems] AS WHI ON WHI.[numItemID] = I.[numItemCode]
			   WHERE [dbo].[CartItems].numDomainId = @numDomainId AND numUserCntId = @numUserCntId 	
				
	END
	ELSE
	BEGIN
		SELECT DISTINCT numCartId,
			   numParentItemCode,
			   numUserCntId,
			   [dbo].[CartItems].numDomainId,
			   vcCookieId,
			   numOppItemCode,
			   [dbo].[CartItems].numItemCode,
			   (SELECT TOP 1 numCategoryId  FROM dbo.ItemCategory WHERE numItemID =  CartItems.numItemCode AND CartItems.numDomainId =@numDomainId ) AS numCategoryId,
			   (SELECT vcCategoryName FROM [dbo].[Category] WHERE [Category].[numCategoryID]  = (SELECT TOP 1 numCategoryId  FROM dbo.ItemCategory WHERE numItemID =  CartItems.numItemCode AND [dbo].[CartItems].numDomainId = @numDomainId )) AS vcCategoryName,
			   numUnitHour,
			   monPrice,
			   monPrice AS [monListPrice],
			   numSourceId,
			   vcItemDesc,
			   vcItemDesc AS txtItemDesc,
			   [dbo].[CartItems].numWarehouseId,
			   [dbo].[CartItems].vcItemName,
			   vcWarehouse,
			   numWarehouseItmsID,
			   vcItemType,
			   vcAttributes,
			   vcAttrValues,
			   I.bitFreeShipping,
			   fltWeight as numWeight,
			   ISNULL((SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numDomainId=@numDomainId AND bitDefault=1 AND numItemCode=I.numItemCode),'') AS vcPathForTImage,
			   tintOpFlag,
			   bitDiscountType,
			   fltDiscount,
			   monTotAmtBefDiscount,
			   ItemURL,
			   numUOM,
			   vcUOMName,
			   decUOMConversionFactor,
			   numHeight,
			   numLength,
			   numWidth,
			   vcShippingMethod,
			   numServiceTypeId,
			   decShippingCharge,
			   numShippingCompany,
			   tintServicetype,
			   vcCoupon,
			   CAST( dtDeliveryDate AS VARCHAR(30)) dtDeliveryDate,
			   monTotAmount,
			   ISNULL(monTotAmtBefDiscount,0) - ISNULL(monTotAmount,0) AS monTotalDiscountAmount,
			   (SELECT vcModelID FROM dbo.Item WHERE Item.numItemCode =  CartItems.numItemCode) AS [vcModelID]
			   ,(SELECT TOP 1 vcPathForTImage FROM dbo.ItemImages II WHERE II.numItemCode = CartItems.numItemCode AND II.numDomainId= CartItems.numDomainId AND II.bitDefault=1) As vcPathForTImage
			   , ISNULL(vcUpSellDesc,'') AS [txtUpSellItemDesc],ISNULL(bitPreUpSell,0) [bitPreUpSell], ISNULL(bitPostUpSell,0) [bitPostUpSell], ISNULL(vcUpSellDesc,'') [vcUpSellDesc]
			   ,ISNULL([I].[vcSKU],'') AS [vcSKU]
			   ,I.vcManufacturer
			   ,ISNULL(PromotionID,0) AS PromotionID
			   ,CAST(ISNULL(PromotionDesc,'') AS VARCHAR) AS PromotionDesc
			   ,ISNULL((SELECT IsOrderBasedPromotion FROM PromotionOffer WHERE numProId = [CartItems].PromotionID),0) AS IsOrderBasedPromotion
			   FROM CartItems
			   LEFT JOIN [dbo].[SimilarItems] AS SI ON SI.[numItemCode] = [dbo].[CartItems].[numItemCode] AND SI.numParentItemCode = CartItems.numParentItem
			   LEFT JOIN [dbo].[Item] AS I ON I.[numItemCode] = [dbo].[CartItems].[numItemCode]
			   --LEFT JOIN [dbo].[WareHouseItems] AS WHI ON WHI.[numItemID] = I.[numItemCode]
			   WHERE [dbo].[CartItems].numDomainId = @numDomainId AND numUserCntId = @numUserCntId AND vcCookieId = @vcCookieId	
				
	END
	
	

--exec USP_GetCartItem @numUserCntId=1,@numDomainId=1,@vcCookieId='fc3d604b-25fa-4c25-960c-1e317768113e',@bitUserType=NULL


/****** Object:  StoredProcedure [dbo].[usp_GetCompanyInfoDtlPl]    Script Date: 07/26/2008 16:16:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--usp_GetCompanyInfoDtlPl 91099,72,-333
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcompanyinfodtlpl')
DROP PROCEDURE usp_getcompanyinfodtlpl
GO
CREATE PROCEDURE [dbo].[usp_GetCompanyInfoDtlPl]                                                                                                                   
@numDivisionID numeric(9) ,                      
@numDomainID numeric(9) ,        
@ClientTimeZoneOffset as int                              
    --                                                                           
AS                                                                            
BEGIN                                                                            
 SELECT CMP.numCompanyID,               
 CMP.vcCompanyName,                
 (select vcdata from listdetails where numListItemID = DM.numStatusID) as numCompanyStatusName,
 DM.numStatusID as numCompanyStatus,                                                                           
 (select vcdata from listdetails where numListItemID = CMP.numCompanyRating) as numCompanyRatingName, 
 CMP.numCompanyRating,             
 (select vcdata from listdetails where numListItemID = CMP.numCompanyType) as numCompanyTypeName,
 CMP.numCompanyType,              
 CMP.numCompanyType as numCmptype,               
 DM.bitPublicFlag,                 
 DM.numDivisionID,                                   
 DM. vcDivisionName,        
dbo.getCompanyAddress(dm.numdivisionId,1,dm.numDomainID) as Address,
dbo.getCompanyAddress(dm.numdivisionId,2,dm.numDomainID) as ShippingAddress,          
  isnull(AD2.vcStreet,'') as vcShipStreet,
  isnull(AD2.vcCity,'') as vcShipCity,
  isnull(dbo.fn_GetState(AD2.numState),'')  as vcShipState,
  ISNULL((SELECT numShippingZone FROM [State] WHERE (numDomainID=@numDomainID OR ConstFlag=1) AND numStateID=AD2.numState),0) AS numShippingZone,
 (select vcdata from listdetails where numListItemID = DM.numFollowUpStatus) as numFollowUpStatusName,
 DM.numFollowUpStatus,                
 CMP.vcWebLabel1,               
 CMP.vcWebLink1,               
 CMP.vcWebLabel2,               
 CMP.vcWebLink2,                                                                             
 CMP.vcWebLabel3,               
 CMP.vcWebLink3,               
 CMP.vcWeblabel4,               
 CMP.vcWebLink4,                                                                       
 DM.tintBillingTerms,              
 DM.numBillingDays,              
 DM.tintInterestType,              
 DM.fltInterest,                                                  
 isnull(AD2.vcPostalCode,'') as vcShipPostCode, 
 AD2.numCountry vcShipCountry,                          
 DM.bitPublicFlag,              
 (select vcdata from listdetails where numListItemID = DM.numTerID)  as numTerIDName,
 DM.numTerID,                                                                         
 (select vcdata from listdetails where numListItemID = CMP.numCompanyIndustry) as numCompanyIndustryName, 
  CMP.numCompanyIndustry,              
 (select vcdata from listdetails where numListItemID = CMP.numCompanyCredit) as  numCompanyCreditName,
 CMP.numCompanyCredit,                                                                             
 isnull(CMP.txtComments,'') as txtComments,               
 isnull(CMP.vcWebSite,'') vcWebSite,              
 (select vcdata from listdetails where numListItemID = CMP.numAnnualRevID) as  numAnnualRevIDName,
 CMP.numAnnualRevID,                
 (select vcdata from listdetails where numListItemID = CMP.numNoOfEmployeesID) as numNoOfEmployeesIDName,               
CMP.numNoOfEmployeesID,                                                                           
    (select vcdata from listdetails where numListItemID = CMP.vcProfile) as vcProfileName, 
 CMP.vcProfile,              
 DM.numCreatedBy,               
 dbo.fn_GetContactName(DM.numRecOwner) as RecOwner,               
 dbo.fn_GetContactName(DM.numCreatedBy)+' ' + convert(varchar(20),dateadd(minute,-@ClientTimeZoneOffset,DM.bintCreatedDate)) as vcCreatedBy,               
 DM.numRecOwner,                
 dbo.fn_GetContactName(DM.numModifiedBy)+' ' +convert(varchar(20),dateadd(minute,-@ClientTimeZoneOffset,DM.bintModifiedDate)) as vcModifiedBy,                    
 isnull(DM.vcComPhone,'') as vcComPhone, 
 DM.vcComFax,                   
 DM.numGrpID,  
 (select vcGrpName from Groups where numGrpId= DM.numGrpID) as numGrpIDName,              
 (select vcdata from listdetails where numListItemID = CMP.vcHow) as vcInfoSourceName,
 CMP.vcHow as vcInfoSource,                                                                            
 DM.tintCRMType,                                                                         
 (SELECT vcCampaignName FROM CampaignMaster WHERE numCampaignID= DM.numCampaignID) as  numCampaignIDName,
 DM.numCampaignID,              
 DM.numAssignedBy, isnull(DM.bitNoTax,0) bitNoTax,                     
 (select A.vcFirstName+' '+A.vcLastName                       
 from AdditionalContactsInformation A                  
 join DivisionMaster D                        
 on D.numDivisionID=A.numDivisionID
 where A.numContactID=DM.numAssignedTo) as numAssignedToName,DM.numAssignedTo,
 (select  count(*) from dbo.GenericDocuments where numRecID= @numDivisionID and vcDocumentSection='A' AND tintDocumentType=2) as DocumentCount ,
 (SELECT count(*)from CompanyAssociations where numDivisionID=@numDivisionID and bitDeleted=0 ) as AssociateCountFrom,                            
 (SELECT count(*)from CompanyAssociations where numAssociateFromDivisionID=@numDivisionID and bitDeleted=0 ) as AssociateCountTo,
 (select vcdata from listdetails where numListItemID = DM.numCompanyDiff) as numCompanyDiffName,               
 DM.numCompanyDiff,DM.vcCompanyDiff,isnull(DM.bitActiveInActive,1) as bitActiveInActive,
(SELECT SUBSTRING(
	(SELECT ','+  A.vcFirstName+' ' +A.vcLastName + CASE WHEN ISNULL(SR.numContactType,0)>0 THEN '(' + dbo.fn_GetListItemName(SR.numContactType) + ')' ELSE '' END
	FROM ShareRecord SR JOIN UserMaster UM ON UM.numUserDetailId=SR.numAssignedTo 
							 JOIN AdditionalContactsInformation A ON UM.numUserDetailId=A.numContactID 
		 WHERE SR.numDomainID=DM.numDomainID AND SR.numModuleID=1 AND SR.numRecordID=DM.numDivisionID
				AND UM.numDomainID=DM.numDomainID and UM.numDomainID=A.numDomainID FOR XML PATH('')),2,200000)) AS numShareWith,
 DM.bitOnCreditHold,
 DM.numDefaultPaymentMethod,
 DM.numAccountClassID,
 DM.tintPriceLevel,DM.vcPartnerCode as vcPartnerCode  ,
  ISNULL(DM.numPartenerSource,0) AS numPartenerSourceId,
 D3.vcPartnerCode+'-'+C3.vcCompanyName as numPartenerSource,
 ISNULL(DM.numPartenerContact,0) AS numPartenerContactId,
A1.vcFirstName+' '+A1.vcLastName AS numPartenerContact,
ISNULL(DM.bitEmailToCase,0) bitEmailToCase,
'' AS vcCreditCards,
----Fetch Last Followup(Template) and EmailSentDate(if any) Value
		  
	(CASE WHEN A2.numECampaignID > 0 THEN dbo.fn_GetFollowUpDetails(A2.numContactID,1,@numDomainID) ELSE '' END) vcLastFollowup,

		----Fetch Last Followup(Template) Value

		----Fetch Next Followup(Template) and ExecutionDate Value
	(CASE WHEN A2.numECampaignID > 0 THEN dbo.fn_GetFollowUpDetails(A2.numContactID,2,@numDomainID) ELSE '' END) vcNextFollowup,										 

		----Fetch Next Followup(Template) Value
(CASE WHEN (SELECt COUNT(*) FROM ExtarnetAccounts WHERE numDivisionID=DM.numDivisionID) >0 THEN 1 ELSE 0 END) AS bitEcommerceAccess                                        
 FROM  CompanyInfo CMP    
  join DivisionMaster DM    
  on DM.numCompanyID=CMP.numCompanyID                                                                                                                                        
   LEFT JOIN dbo.AddressDetails AD1 ON AD1.numDomainID=DM.numDomainID 
   AND AD1.numRecordID=DM.numDivisionID AND AD1.tintAddressOf=2 AND AD1.tintAddressType=1 AND AD1.bitIsPrimary=1
   LEFT JOIN dbo.AddressDetails AD2 ON AD2.numDomainID=DM.numDomainID 
   AND AD2.numRecordID= DM.numDivisionID AND AD2.tintAddressOf=2 AND AD2.tintAddressType=2 AND AD2.bitIsPrimary=1
    LEFT JOIN divisionMaster D3 ON DM.numPartenerSource = D3.numDivisionID
  LEFT JOIN CompanyInfo C3 ON C3.numCompanyID = D3.numCompanyID    
  LEFT JOIN AdditionalContactsInformation A1 ON DM.numPartenerContact = A1.numContactId 
  LEft join   AdditionalContactsInformation A2 on   DM.numDivisionID = A2.numDivisionId and A2.bitPrimaryContact = 1
 where DM.numDivisionID=@numDivisionID   and DM.numDomainID=@numDomainID                                                                                          
END

GO




/****** Object:  StoredProcedure [dbo].[usp_GetContactDTlPL]    Script Date: 07/26/2008 16:16:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By                                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcontactdtlpl')
DROP PROCEDURE usp_getcontactdtlpl
GO
CREATE PROCEDURE [dbo].[usp_GetContactDTlPL]                                                              
@numContactID as numeric(9)=0  ,    
@numDomainID as numeric(9)=0   ,  
@ClientTimeZoneOffset as int                                    
--                                                            
AS                                                              
BEGIN   


  SELECT                                      
 A.numContactId,            
A.vcFirstName,           
A.vcLastName,                                                                   
 D.numDivisionID,             
 C.numCompanyId,            
 C.vcCompanyName,             
 D.vcDivisionName,             
 D.numDomainID,        
 D.tintCRMType,            
 A.numPhone,
 A.numPhoneExtension,
 A.vcEmail,  
 A.numTeam, 
 A.numTeam as vcTeam,         
[dbo].[GetListIemName](A.numTeam) as vcTeamName,                                                             
 A.vcFax,   
 A.numContactType as vcContactType,
 A.numContactType,         
[dbo].[GetListIemName](A.numContactType) as vcContactTypeName,                         
 A.charSex,             
 A.bintDOB,            
 dbo.GetAge(A.bintDOB, getutcdate()) as Age,                                                               
 [dbo].[GetListIemName]( A.vcPosition) as vcPositionName, 
 A.vcPosition,            
 A.txtNotes,             
 A.numCreatedBy,                                                              
 A.numCell,            
 A.NumHomePhone,            
 A.vcAsstFirstName,
 A.vcAsstLastName,            
 A.numAsstPhone,
 A.numAsstExtn,                                                              
 A.vcAsstEmail,            
 A.charSex, 
 A.vcLinkedinId,
  A.vcLinkedinUrl,
 case when A.charSex='M' then 'Male' when A.charSex='F' then 'Female' else  '-' end as charSexName,            
[dbo].[GetListIemName]( A.vcDepartment) as vcDepartmentName,  
A.vcDepartment,                                                             
--    case when AddC.vcPStreet is null then '' when AddC.vcPStreet='' then '' else AddC.vcPStreet + ',' end +             
-- case when AddC.vcPCity is null then '' when AddC.vcPCity='' then ''  else AddC.vcPCity end +             
-- case when  AddC.vcPPostalCode is null then '' when  AddC.vcPPostalCode='' then '' else ','+ AddC.vcPPostalCode end +              
-- case when dbo.fn_GetState(AddC.vcPState) is null then '' else  ','+ dbo.fn_GetState(AddC.vcPState) end +              
-- case when dbo.fn_GetListName(AddC.vcPCountry,0) ='' then '' else ',' + dbo.fn_GetListName(AddC.vcPCountry,0) end      
 dbo.getContactAddress(A.numContactId) as [Address],            
-- AddC.vcPState,            
-- AddC.vcPCountry,            
-- AddC.vcContactLocation,                                    
--  AddC.vcStreet,                                                               
--    AddC.vcCity,             
-- AddC.vcState,             
-- AddC.intPostalCode,                                                               
--  AddC.vcCountry,                                                              
    A.bitOptOut,                                                                         
 dbo.fn_GetContactName(a.numManagerId) as ManagerName,
 a.numManagerId   as   Manager,
[dbo].[GetListIemName](A.vcCategory) as vcCategoryName  ,     
 A.vcCategory,        
 dbo.fn_GetContactName(A.numCreatedBy)+' '+ convert(varchar(20),dateadd(minute,-@ClientTimeZoneOffset,A.bintCreatedDate)) CreatedBy,                                      
 dbo.fn_GetContactName(A.numModifiedBy)+' '+ convert(varchar(20),dateadd(minute,-@ClientTimeZoneOffset,A.bintModifiedDate)) ModifiedBy,            
 A.vcTitle,            
 A.vcAltEmail,                                                          
 dbo.fn_GetContactName(A.numRecOwner) as RecordOwner,
 A.numEmpStatus,                  
[dbo].[GetListIemName]( A.numEmpStatus) as numEmpStatusName,                                                          
(select  count(*) from GenericDocuments   where numRecID=A.numContactId and  vcDocumentSection='C') as DocumentCount,                          
(SELECT count(*)from CompanyAssociations where numDivisionID=A.numContactId and bitDeleted=0 ) as AssociateCountFrom,                            
(SELECT count(*)from CompanyAssociations where numAssociateFromDivisionID=A.numContactId and bitDeleted=0 ) as AssociateCountTo,


 ---- Edited by Priya for Follow-up Changes (10 Jan 2018)
 --ISNULL((SELECT CONCAT(vcECampName, ' (' + 
	--								 CAST(ISNULL((SELECT COUNT(*) FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID
	--									WHERE ConECampaign.numContactID = A.numContactID AND  ECampaign.numECampaignID = ConECampaign.numECampaignID AND ISNULL(bitSend,0) = 1 ),0) AS varchar) + ' Of ' +
	--								 CAST(ISNULL((SELECT COUNT(*) FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID
	--								     WHERE ConECampaign.numContactID = A.numContactID AND ECampaign.numECampaignID = ConECampaign.numECampaignID),0) AS VARCHAR) + ') ', 
	--	 '<a onclick="openDrip(' + (cast(ISNULL((SELECT MAX([numConEmailCampID]) FROM [ConECampaign] WHERE [numContactID] = A.numContactID AND [numECampaignID]= A.numECampaignID AND ISNULL(bitEngaged,0)=1),0)AS varchar)) +');">
	--	 <img alt="Follow-up Campaign history" height="16px" width="16px" title="Follow-up campaign history" src="../images/GLReport.png"
	--	 ></a>') FROM [ECampaign] WHERE numECampaignID = A.numECampaignID),'') vcECampName,

	(CASE WHEN 
			ISNULL((SELECT COUNT(*) FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID
										WHERE ConECampaign.numContactID = A.numContactID AND  ConECampaign.numECampaignID = A.numECampaignID AND bitEngaged =1 AND ISNULL(bitSend,0) = 1),0) 
			= ISNULL((SELECT COUNT(*) FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID
									     WHERE ConECampaign.numContactID = A.numContactID AND ConECampaign.numECampaignID = A.numECampaignID AND bitEngaged =1 ),0)
	THEN

	ISNULL((SELECT CONCAT('<img alt="Campaign completed" height="16px" width="16px" title="Campaign Completed" src="../images/comflag.png"> ',
							vcECampName, ' (' + 
									 CAST(ISNULL((SELECT COUNT(*) FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID
										WHERE ConECampaign.numContactID = A.numContactID AND  ECampaign.numECampaignID = ConECampaign.numECampaignID AND bitEngaged =1 AND ISNULL(bitSend,0) = 1 ),0) AS varchar) + ' Of ' +
									 CAST(ISNULL((SELECT COUNT(*) FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID
									     WHERE ConECampaign.numContactID = A.numContactID AND ECampaign.numECampaignID = ConECampaign.numECampaignID AND bitEngaged =1),0) AS VARCHAR) + ') ', 
		 '<a onclick="openDrip(' + (cast(ISNULL((SELECT MAX([numConEmailCampID]) FROM [ConECampaign] WHERE [numContactID] = A.numContactID AND [numECampaignID]= A.numECampaignID AND ISNULL(bitEngaged,0)=1),0)AS varchar)) +');">
		 <img alt="Follow-up Campaign history" height="16px" width="16px" title="Follow-up campaign history" src="../images/GLReport.png"
		 ></a>') FROM [ECampaign] WHERE numECampaignID = A.numECampaignID),'')
	ELSE

	ISNULL((SELECT CONCAT('<img alt="Active Campaign" height="16px" width="16px" title="Active Campaign" src="../images/GreenFlag.png"> ',
							vcECampName, ' (' + 
									 CAST(ISNULL((SELECT COUNT(*) FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID
										WHERE ConECampaign.numContactID = A.numContactID AND  ECampaign.numECampaignID = ConECampaign.numECampaignID AND bitEngaged =1 AND ISNULL(bitSend,0) = 1 ),0) AS varchar) + ' Of ' +
									 CAST(ISNULL((SELECT COUNT(*) FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID
									     WHERE ConECampaign.numContactID = A.numContactID AND ECampaign.numECampaignID = ConECampaign.numECampaignID AND bitEngaged =1),0) AS VARCHAR) + ') ', 
		 '<a onclick="openDrip(' + (cast(ISNULL((SELECT MAX([numConEmailCampID]) FROM [ConECampaign] WHERE [numContactID] = A.numContactID AND [numECampaignID]= A.numECampaignID AND ISNULL(bitEngaged,0)=1),0)AS varchar)) +');">
		 <img alt="Follow-up Campaign history" height="16px" width="16px" title="Follow-up campaign history" src="../images/GLReport.png"
		 ></a>') FROM [ECampaign] WHERE numECampaignID = A.numECampaignID),'')

	END
	) vcECampName,

		----Fetch Last Followup(Template) and EmailSentDate(if any) Value
		  
----Fetch Last Followup(Template) and EmailSentDate(if any) Value
		  
	(CASE WHEN A.numECampaignID > 0 THEN dbo.fn_GetFollowUpDetails(A.numContactID,1,@numDomainID) ELSE '' END) vcLastFollowup,

		----Fetch Last Followup(Template) Value

		----Fetch Next Followup(Template) and ExecutionDate Value
	(CASE WHEN A.numECampaignID > 0 THEN dbo.fn_GetFollowUpDetails(A.numContactID,2,@numDomainID) ELSE '' END) vcNextFollowup,										 

		----Fetch Next Followup(Template) Value
---- Edited by Priya for Follow-up Changes (10 Jan 2018)


 ISNULL(A.numECampaignID,0) numECampaignID,
 ISNULL( (SELECT MAX([numConEmailCampID]) FROM [ConECampaign] WHERE [numContactID] = A.numContactID AND [numECampaignID]= A.numECampaignID AND ISNULL(bitEngaged,0)=1),0) numConEmailCampID,
 ISNULL(A.bitPrimaryContact,0) AS bitPrimaryContact,D.vcPartnerCode AS vcPartnerCode,
 ISNULL(D.numPartenerSource,0) AS numPartenerSourceId,
 (D3.vcPartnerCode+'-'+C3.vcCompanyName) as numPartenerSource,
 ISNULL(D.numPartenerContact,0) AS numPartenerContactId,
 A1.vcFirstName+' '+A1.vcLastName AS numPartenerContact,
 ISNULL((SELECT vcPassword FROM ExtranetAccountsDtl WHERE numContactId=A.numContactId),'') AS vcPassword,ISNULL(A.vcTaxID,'') vcTaxID
 FROM AdditionalContactsInformation A INNER JOIN                                                              
 DivisionMaster D ON A.numDivisionId = D.numDivisionID INNER JOIN                                                              
 CompanyInfo C ON D.numCompanyID = C.numCompanyId  
  LEFT JOIN divisionMaster D3 ON D.numPartenerSource = D3.numDivisionID
  LEFT JOIN CompanyInfo C3 ON C3.numCompanyID = D3.numCompanyID    
  LEFT JOIN AdditionalContactsInformation A1 ON D.numPartenerContact = A1.numContactId                                                               
 left join UserMaster U on U.numUserDetailId=A.numContactId                                                             
 WHERE A.numContactId = @numContactID and A.numDomainID=@numDomainID                                                     
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcontactlist1')
DROP PROCEDURE usp_getcontactlist1
GO
CREATE PROCEDURE [dbo].[USP_GetContactList1]                                                                                   
@numUserCntID numeric(9)=0,                                                                                    
@numDomainID numeric(9)=0,                                                                                    
@tintUserRightType tinyint=0,                                                                                    
@tintSortOrder tinyint=4,                                                                                    
@SortChar char(1)='0',                                                                                   
@FirstName varChar(100)= '',                                                                                  
@LastName varchar(100)='',                                                                                  
@CustName varchar(100)='',                                                                                  
@CurrentPage int,                                                                                  
@PageSize int,                                                                                  
@columnName as Varchar(50),                                                                                  
@columnSortOrder as Varchar(10),                                                                                  
@numDivisionID as numeric(9)=0,                                              
@bitPartner as bit ,                                                   
@inttype as numeric(9)=0,
@ClientTimeZoneOffset as int,
 @vcRegularSearchCriteria varchar(MAX)='',
 @vcCustomSearchCriteria varchar(MAX)=''                                                                             
--                                                                                    
as                  
                       
declare @join as varchar(400)                  
set @join = ''                 
                        
 if @columnName like 'CFW.Cust%'                 
begin                
                
                 
 set @join= ' left Join CFW_FLD_Values_Cont CFW on CFW.RecId=ADC.numcontactId  and CFW.fld_id= '+replace(@columnName,'CFW.Cust','') +' '                                                         
 set @columnName='CFW.Fld_Value'                
                  
end                                         
if @columnName like 'DCust%'                
begin                
                                                       
 set @join= ' left Join CFW_FLD_Values_Cont CFW on CFW.RecId=ADC.numcontactId  and CFW.fld_id= '+replace(@columnName,'DCust','')                                                                                                   
 set @join= @join +' left Join ListDetails LstCF on LstCF.numListItemID=CFW.Fld_Value  '                
 set @columnName='LstCF.vcData'                  
                
end                                                                              
                                                                                  
declare @firstRec as integer                                                                                  
declare @lastRec as integer                                                                                  
set @firstRec= (@CurrentPage-1) * @PageSize                              
set @lastRec= (@CurrentPage*@PageSize+1)                                                                                   
                                                                                  
                                                                                  
declare @strSql as varchar(8000)                                                                            
 set  @strSql='with tblcontacts as (Select '                                                                          
      if (@tintSortOrder=5 or @tintSortOrder=6) set  @strSql= @strSql+' top 20 '                                                
                                     
   set  @strSql= @strSql+ ' ROW_NUMBER() OVER (ORDER BY '+ CASE when @columnName='ADC.numAge' then 'ADC.bintDOB' ELSE @columnName end +' '+ @columnSortOrder+') AS RowNumber,
   COUNT(*) OVER(PARTITION BY NULL) AS TotalRowCount,
   ADC.numContactId                                   
  FROM AdditionalContactsInformation ADC                                
  JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID                                                
  JOIN CompanyInfo cmp ON DM.numCompanyID = cmp.numCompanyId
  LEFT JOIN VIEW_OPEN_ACTION_ITEMS VOA WITH (NOEXPAND) ON VOA.numDomainID = ADC.numDomainID AND  VOA.numDivisionId = DM.numDivisionID
  LEFT JOIN View_InboxEmail VIE WITH (NOEXPAND) ON VIE.numDomainID = ADC.numDomainID AND  VIE.numContactId = ADC.numContactId '  +@join+  '                                                                  
  left join ListDetails LD on LD.numListItemID=cmp.numCompanyRating                                                                           
  left join ListDetails LD1 on LD1.numListItemID= ADC.numEmpStatus 
  LEFT JOIN AddressDetails AD ON AD.numRecordID = ADC.numContactID AND AD.tintAddressOf=1 AND AD.tintAddressType=0 AND AD.bitIsPrimary=1 '                                           
  if @bitPartner=1 set @strSql=@strSql + ' left join CompanyAssociations CA on CA.numAssociateFromDivisionID=DM.numDivisionID and bitShareportal=1 and CA.bitDeleted=0                                                 
and CA.numDivisionID=(select numDivisionID from AdditionalContactsInformation where numContactID='+convert(varchar(15),@numUserCntID)+ ')'                                                                   
                                                                    
 if @tintSortOrder= 7 set @strSql=@strSql+' join Favorites F on F.numContactid=ADC.numContactID '                                                                     
                                                       
  set  @strSql= @strSql +'                                                                
        where 
 --DM.tintCRMType<>0     AND 
 cmp.numDomainID=DM.numDomainID   
 and DM.numDomainID=ADC.numDomainID                                             
    and DM.numDomainID  = '+convert(varchar(15),@numDomainID)+''                             
 if @inttype<>0 and @inttype<>101  set @strSql=@strSql+' and ADC.numContactType  = '+convert(varchar(10),@inttype)                            
 if @inttype=101  set @strSql=@strSql+' and ADC.bitPrimaryContact  =1 '                            
    if @FirstName<>'' set @strSql=@strSql+' and ADC.vcFirstName  like '''+@FirstName+'%'''                                                             
    if @LastName<>'' set @strSql=@strSql+' and ADC.vcLastName like '''+@LastName+'%'''                                              
    if @CustName<>'' set @strSql=@strSql+' and cmp.vcCompanyName like '''+@CustName+'%'''                                                                                  
if @SortChar<>'0' set @strSql=@strSql + ' And ADC.vcFirstName like '''+@SortChar+'%'''                                                                                           
if @tintUserRightType=1 set @strSql=@strSql + ' AND (ADC.numRecOwner = '+convert(varchar(15),@numUserCntID)+ ')' + case when @bitPartner=1 then ' or (CA.bitShareportal=1 and  CA.bitDeleted=0)' else '' end                                                  
   
else if @tintUserRightType=2 set @strSql=@strSql + ' AND DM.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+convert(varchar(15),@numUserCntID)+' ) '                                                           
                
if @numDivisionID <> 0  set @strSql=@strSql + ' And DM.numDivisionID ='+ convert(varchar(10),@numDivisionID)                      
                                                                                         
if @tintSortOrder=2  set @strSql=@strSql + ' AND (ADC.numRecOwner = '+convert(varchar(15),@numUserCntID)+ ')'                                                                                      
else if @tintSortOrder=3  set @strSql=@strSql + '  AND ADC.numContactType=92  '                                                                          
else if @tintSortOrder=4  set @strSql=@strSql + ' AND ADC.bintCreatedDate > '''+convert(varchar(20),dateadd(day,-7,getutcdate()))+''''                                    
else if @tintSortOrder=5  set @strSql=@strSql + ' and ADC.numCreatedby='+convert(varchar(15),@numUserCntID)                      
                              
else if @tintSortOrder=6  set @strSql=@strSql + ' and ADC.numModifiedby='+convert(varchar(15),@numUserCntID)                      
                                                     
else if @tintSortOrder=7  set @strSql=@strSql + ' and F.numUserCntID='+convert(varchar(15),@numUserCntID)                                                                             
                    
 if (@tintSortOrder=5 and @columnName!='ADC.bintcreateddate')  set @strSql='select * from ('+@strSql+')X order by '+ @columnName +' '+ @columnSortOrder                        
else if (@tintSortOrder=6 and @columnName!='ADC.bintcreateddate')  set @strSql='select * from ('+@strSql+')X order by '+ @columnName +' '+ @columnSortOrder                                                                
else if @tintSortOrder=7  set @strSql=@strSql + ' and cType=''C'' '                      
    
IF CHARINDEX('ADC.vcLastFollowup',@vcRegularSearchCriteria) > 0
	BEGIN
		SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'ADC.vcLastFollowup', '(SELECT dtExecutionDate FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID  WHERE ConECampaign.numContactID=ADC.numContactId AND ISNULL(ADC.numECampaignID,0) > 0 AND numConECampDTLID IN (SELECT MAX(numConECampDTLID) FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID  WHERE ConECampaign.numContactID=ADC.numContactId AND ISNULL(ADC.numECampaignID,0) > 0 AND ISNULL(bitSend,0)=1))')
	END
	IF CHARINDEX('ADC.vcNextFollowup',@vcRegularSearchCriteria) > 0
	BEGIN
		SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'ADC.vcNextFollowup','(SELECT dtExecutionDate FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID  WHERE ConECampaign.numContactID=ADC.numContactId AND ISNULL(ADC.numECampaignID,0) > 0 AND numConECampDTLID IN (SELECT MIN(numConECampDTLID) FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID  WHERE ConECampaign.numContactID=ADC.numContactId AND ISNULL(ADC.numECampaignID,0) > 0 AND ISNULL(bitSend,0)=0))')
	END  

IF @vcRegularSearchCriteria<>'' set @strSql=@strSql+' and ' + @vcRegularSearchCriteria 


IF @vcCustomSearchCriteria<>'' set @strSql=@strSql+' and ' + @vcCustomSearchCriteria

set @strSql=@strSql + ')'                      
                                                                                 
declare @tintOrder as tinyint                                                        
declare @vcFieldName as varchar(50)                                                        
declare @vcListItemType as varchar(3)                                                   
declare @vcListItemType1 as varchar(1)                                                       
declare @vcAssociatedControlType varchar(20)                                                        
declare @numListID AS numeric(9)                                                        
declare @vcDbColumnName varchar(20)                            
declare @WhereCondition varchar(2000)                             
declare @vcLookBackTableName varchar(2000)                      
Declare @bitCustom as bit  
DECLARE @bitAllowEdit AS CHAR(1)                 
Declare @numFieldId as numeric  
DECLARE @bitAllowSorting AS CHAR(1)                   
declare @vcColumnName AS VARCHAR(500)                            
set @tintOrder=0                                                        
set @WhereCondition =''                       
                         
declare @Nocolumns as tinyint                      
set @Nocolumns=0                      
  
select @Nocolumns=isnull(sum(TotalRow),0)  from(            
Select count(*) TotalRow from View_DynamicColumns where numFormId=10 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=@inttype 
Union 
Select count(*) TotalRow from View_DynamicCustomColumns where numFormId=10 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=@inttype) TotalRows

                   
--declare @strColumns as varchar(2000)                      
--set @strColumns=''                      
--while @DefaultNocolumns>0                      
--begin                      
--                  
-- set @strColumns=@strColumns+',null'                      
-- set @DefaultNocolumns=@DefaultNocolumns-1                      
--end                   
                     
   CREATE TABLE #tempForm (tintOrder TINYINT,vcDbColumnName nvarchar(50),vcOrigDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),
  vcListItemType varchar(3),numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,
  numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,bitIsAlphaNumeric BIT,bitIsNumeric BIT,
  bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),ListRelID numeric(9),intColumnWidth int,bitAllowFiltering BIT,vcFieldDataType CHAR(1))

--Check if Master Form Configuration is created by administrator if NoColumns=0
DECLARE @IsMasterConfAvailable BIT = 0
DECLARE @numUserGroup NUMERIC(18,0)

IF @Nocolumns=0
BEGIN
	SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

	IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = 10 AND numRelCntType=@inttype AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
	BEGIN
		SET @IsMasterConfAvailable = 1
	END
END


--If MasterConfiguration is available then load it otherwise load default columns
IF @IsMasterConfAvailable = 1
BEGIN
	INSERT INTO DycFormConfigurationDetails 
	(
		numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
	)
	SELECT 
		10,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,@inttype,1,0,intColumnWidth
	FROM 
		View_DynamicColumnsMasterConfig
	WHERE
		View_DynamicColumnsMasterConfig.numFormId=10 AND 
		View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
		View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
		View_DynamicColumnsMasterConfig.numRelCntType=@inttype AND 
		View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
		ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
		ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
	UNION
    SELECT 
		10,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,@inttype,1,1,intColumnWidth
	FROM 
		View_DynamicCustomColumnsMasterConfig
	WHERE 
		View_DynamicCustomColumnsMasterConfig.numFormId=10 AND 
		View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
		View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
		View_DynamicCustomColumnsMasterConfig.numRelCntType=@inttype AND 
		View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
		ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1

	INSERT INTO #tempForm
	SELECT 
		(intRowNum + 1) as tintOrder, vcDbColumnName,	vcOrigDbColumnName,	ISNULL(vcCultureFieldName,vcFieldName), vcAssociatedControlType,
		vcListItemType, numListID, vcLookBackTableName,	bitCustom, numFieldId, bitAllowSorting,	bitInlineEdit, bitIsRequired,
		bitIsEmail,	bitIsAlphaNumeric, bitIsNumeric, bitIsLengthValidation,	intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,
		ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType 
	FROM 
		View_DynamicColumnsMasterConfig
	WHERE
		View_DynamicColumnsMasterConfig.numFormId=10 AND 
		View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
		View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
		View_DynamicColumnsMasterConfig.numRelCntType=@inttype AND 
		View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
		ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
		ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
	UNION
    SELECT 
		tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName, vcAssociatedControlType,'' as vcListItemType,numListID,'',
		bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
		intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,''
	FROM 
		View_DynamicCustomColumnsMasterConfig
	WHERE 
		View_DynamicCustomColumnsMasterConfig.numFormId=10 AND 
		View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
		View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
		View_DynamicCustomColumnsMasterConfig.numRelCntType=@inttype AND 
		View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
		ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1
	ORDER BY 
		tintOrder ASC  
END
ELSE                                        
BEGIN                 
     
	if @Nocolumns=0
	BEGIN
	INSERT INTO DycFormConfigurationDetails (numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth)
	select 10,numFieldId,0,Row_number() over(order by tintRow desc),@numDomainID,@numUserCntID,@inttype,1,0,intColumnWidth
	 FROM View_DynamicDefaultColumns
	 where numFormId=10 and bitDefault=1 AND ISNULL(bitSettingField,0)=1 and numDomainID=@numDomainID
	order by tintOrder asc 
	END
                                   
   INSERT INTO #tempForm
select tintRow+1 as tintOrder,vcDbColumnName,vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
 vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName                                                 
,bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID ,intColumnWidth,bitAllowFiltering,vcFieldDataType
 FROM View_DynamicColumns 
 where numFormId=10 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=@inttype
  AND ISNULL(bitSettingField,0)=1 AND ISNULL(bitCustom,0)=0
       
       UNION
    
   select tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName,                  
 vcAssociatedControlType,'' as vcListItemType,numListID,''                                                 
,bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,''
 from View_DynamicCustomColumns
 where numFormId=10 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=@inttype
 AND ISNULL(bitCustom,0)=1
 
  order by tintOrder asc  

END
   
set @strSql=@strSql+' select convert(varchar(10),isnull(ADC.numContactId,0)) as numContactId            
       ,ISNULL(ADC.numECampaignID,0) as numECampaignID
	   ,ISNULL( (SELECT MAX([numConEmailCampID]) FROM [ConECampaign] WHERE [numContactID] = ADC.numContactId AND [numECampaignID]= ADC.numECampaignID AND ISNULL(bitEngaged,0)=1),0) numConEmailCampID
	   ,ISNULL(VIE.Total,0) as TotalEmail
	   ,ISNULL(VOA.OpenActionItemCount,0) as TotalActionItem
       ,ISNULL(DM.numDivisionID,0) numDivisionID, isnull(DM.numTerID,0)numTerID,isnull( ADC.numRecOwner,0) numRecOwner,isnull( DM.tintCRMType,0) tintCRMType ,RowNumber'  
	   
---- Added by Priya 16 Jan 2018(Flag with Organisation Name)--------
SET @strSql=@strSql+' ,

	((CASE 
	WHEN ADC.numECampaignID > 0 
	THEN 
		 ISNULL( (CASE WHEN 
				ISNULL((SELECT COUNT(*) FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID
											WHERE ConECampaign.numContactID = ADC.numContactID AND  ConECampaign.numECampaignID = ADC.numECampaignID AND bitEngaged =1 AND ISNULL(bitSend,0) = 1 AND ADC.bitPrimaryContact = 1),0) 
				= ISNULL((SELECT COUNT(*) FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID
											 WHERE ConECampaign.numContactID = ADC.numContactID AND ConECampaign.numECampaignID = ADC.numECampaignID AND bitEngaged =1 AND ADC.bitPrimaryContact = 1 ),0)
		THEN
		  ''CheckeredFlag''
		  Else
		  ''GreenFlag''
		  End),0)



	ELSE '''' 
	END)) as FollowupFlag '
	---- Added by Priya 16 Jan 2018(Flag with Organisation Name)--------                    
                                                    
Declare @ListRelID as numeric(9) 
  
    select top 1 @tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,                
  @vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,@numListID=numListID,@vcLookBackTableName=vcLookBackTableName                                               
,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,@ListRelID=ListRelID
  from  #tempForm order by tintOrder asc            

                   
while @tintOrder>0                                                        
begin                                                        
                                            
   if @bitCustom = 0                
 begin           
        
    declare @Prefix as varchar(5)                      
      if @vcLookBackTableName = 'AdditionalContactsInformation'                      
    set @Prefix = 'ADC.'                      
      if @vcLookBackTableName = 'DivisionMaster'                      
    set @Prefix = 'DM.'         
        
                  SET @vcColumnName= @vcDbColumnName+'~'+ convert(varchar(10),@numFieldId) +'~0'

			----- Added by Priya For Followups(14Jan2018)---- 
if @vcDbColumnName = 'vcLastFollowup'
BEGIN
	set @strSql=@strSql+',  ISNULL((CASE WHEN ADC.numECampaignID > 0 THEN dbo.fn_GetFollowUpDetails(ADC.numContactID,1,ADC.numDomainID) ELSE '''' END),'''') ['+ @vcColumnName+']' 
END
ELSE IF @vcDbColumnName = 'vcNextFollowup'
BEGIN
	set @strSql=@strSql+',  ISNULL((CASE WHEN ADC.numECampaignID > 0 THEN dbo.fn_GetFollowUpDetails(ADC.numContactID,2,ADC.numDomainID) ELSE '''' END),'''') ['+ @vcColumnName+']' 
END
                         
			----- Added by Priya For Followups(14Jan2018)----  
 ELSE if @vcDbColumnName = 'vcPassword'
 BEGIN
	set @strSql=@strSql+',ISNULL((SELECT vcPassword FROM ExtranetAccountsDtl WHERE numContactId=ADC.numContactID),'''') ['+ @vcColumnName+']'  
 END
 ELSE if @vcAssociatedControlType='SelectBox'                                                        
begin                                                                                                      
     if @vcListItemType='LI'                                                         
     begin                                                        
      set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName+']'                                                       
      set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName                                                        
     end                                                        
     else if @vcListItemType='S'                                                         
     begin                                                        
      set @strSql=@strSql+',S'+ convert(varchar(3),@tintOrder)+'.vcState'+' ['+ @vcColumnName+']'                                                        
      set @WhereCondition= @WhereCondition +' left join State S'+ convert(varchar(3),@tintOrder)+ ' on S'+convert(varchar(3),@tintOrder)+ '.numStateID='+@vcDbColumnName                                                        
     end                                                        
     else if @vcListItemType='T'                                                         
     begin                                                        
      set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName+']'                                                        
      set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName                                                        
     end                       
     else if   @vcListItemType='U'                                                     
    begin                       
    set @strSql=@strSql+',dbo.fn_GetContactName('+@Prefix+@vcDbColumnName+') ['+ @vcColumnName+']'                                                        
    end                      
end         
 else if @vcAssociatedControlType='DateField'                                                      
 begin                
           
     set @strSql=@strSql+',case when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','+@Prefix+@vcDbColumnName+') )= convert(varchar(11),getdate()) then ''<b><font color=red>Today</font></b>'''          
     set @strSql=@strSql+'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','+@Prefix+@vcDbColumnName+') )= convert(varchar(11),dateadd(day,-1,getdate())) then ''<b><font color=purple>YesterDay</font></b>'''          
     set @strSql=@strSql+'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','+@Prefix+@vcDbColumnName+') )= convert(varchar(11),dateadd(day,1,getdate())) then''<b><font color=orange>Tommorow</font></b>'' '          
     set @strSql=@strSql+'else dbo.FormatedDateFromDate(DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','+@Prefix+@vcDbColumnName+'),'+convert(varchar(10),@numDomainId)+') end  ['+ @vcColumnName+']'                    
   end        
              
else if @vcAssociatedControlType='TextBox' OR @vcAssociatedControlType='Label'                                                     
begin        
         
     set @strSql=@strSql+','+ case  when @vcDbColumnName='numAge' then 'year(getutcdate())-year(bintDOB)'         
 when @vcDbColumnName='numDivisionID' then 'DM.numDivisionID' else @vcDbColumnName end+' ['+ @vcColumnName+']'                   
 end      
else                                                
begin    
	if @vcDbColumnName='vcCampaignAudit'
		set @strSql=@strSql+', dbo.GetContactCampignAuditDetail(ADC.numECampaignID,ADC.numContactId) ['+ @vcColumnName+']'  
	else
		set @strSql=@strSql+', '+ @vcDbColumnName +' ['+ @vcColumnName+']'                 
 end                                               
            
              
                    
end                
else if @bitCustom = 1                
begin                
            
	SET @vcColumnName= @vcDbColumnName +'~'+ convert(varchar(10),@numFieldId)+'~1'
       
   select @vcFieldName=FLd_label,@vcAssociatedControlType=fld_type,@vcDbColumnName='Cust'+Convert(varchar(10),Fld_Id)                               
    from CFW_Fld_Master                                                
   where    CFW_Fld_Master.Fld_Id = @numFieldId                 
                
                 
    if @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'             
   begin                
                   
    set @strSql= @strSql+',CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value  ['+ @vcColumnName +']'                   
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Cont CFW'+ convert(varchar(3),@tintOrder)+ '                 
    on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=ADC.numContactId   '                                                         
   end      
    else if @vcAssociatedControlType = 'CheckBox'             
   begin              
                 
    set @strSql= @strSql+',case when isnull(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,0)=0 then 0 when isnull(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,0)=1 then 1 end   ['+ @vcColumnName +']'              
   
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Cont CFW'+ convert(varchar(3),@tintOrder)+ '               
    on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=ADC.numContactId   '                                                       
   end              
  else if @vcAssociatedControlType = 'DateField'            
   begin                
                   
    set @strSql= @strSql+',dbo.FormatedDateFromDate(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,'+convert(varchar(10),@numDomainId)+')  ['+ @vcColumnName +']'                   
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Cont CFW'+ convert(varchar(3),@tintOrder)+ '                 
    on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=ADC.numContactId   '                                                         
   end                
   else if @vcAssociatedControlType = 'SelectBox'            
   begin                
    set @vcDbColumnName = 'DCust'+Convert(varchar(10),@numFieldId)                
    set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName +']'                                                          
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Cont CFW'+ convert(varchar(3),@tintOrder)+ '                 
     on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=ADC.numContactId   '                                                         
    set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value'                
   end
   ELSE
	BEGIN
		SET @strSql = @strSql + CONCAT(',dbo.GetCustFldValueContact(',@numFieldId,',ADC.numContactID)') + ' [' + @vcColumnName + ']'
	END                
end                    
                    
   select top 1 @tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,                
  @vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,@numListID=numListID,@vcLookBackTableName=vcLookBackTableName                                               
,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,@ListRelID=ListRelID
  from #tempForm WHERE tintOrder > @tintOrder-1 order by tintOrder asc            
 
  if @@rowcount=0 set @tintOrder=0 

                                  
end                             
                          
       
-------Change Row Color-------
Declare @vcCSOrigDbCOlumnName as varchar(50),@vcCSLookBackTableName as varchar(50),@vcCSAssociatedControlType as varchar(50)
SET @vcCSOrigDbCOlumnName=''
SET @vcCSLookBackTableName=''

Create table #tempColorScheme(vcOrigDbCOlumnName varchar(50),vcLookBackTableName varchar(50),vcFieldValue varchar(50),vcFieldValue1 varchar(50),vcColorScheme varchar(50),vcAssociatedControlType varchar(50))

insert into #tempColorScheme select DFM.vcOrigDbCOlumnName,DFM.vcLookBackTableName,DFCS.vcFieldValue,DFCS.vcFieldValue1,DFCS.vcColorScheme,DFFM.vcAssociatedControlType 
from DycFieldColorScheme DFCS join DycFormField_Mapping DFFM on DFCS.numFieldID=DFFM.numFieldID AND DFCS.numFormID=DFFM.numFormID
join DycFieldMaster DFM on DFM.numModuleID=DFFM.numModuleID and DFM.numFieldID=DFFM.numFieldID
where DFCS.numDomainID=@numDomainID and DFFM.numFormID=10 AND DFCS.numFormID=10 and isnull(DFFM.bitAllowGridColor,0)=1

IF(SELECT COUNT(*) FROM #tempColorScheme)>0
BEGIN
   select top 1 @vcCSOrigDbCOlumnName=vcOrigDbCOlumnName,@vcCSLookBackTableName=vcLookBackTableName,@vcCSAssociatedControlType=vcAssociatedControlType from #tempColorScheme
END   
---------------------------- 
 
IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
BEGIN
set @strSql=@strSql+',tCS.vcColorScheme'
END                         
                                  
set @strSql=@strSql+' ,TotalRowCount FROM AdditionalContactsInformation ADC                                                                             
  JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID                      
  JOIN CompanyInfo CI ON DM.numCompanyID = CI.numCompanyId
  LEFT JOIN VIEW_OPEN_ACTION_ITEMS VOA WITH (NOEXPAND) ON VOA.numDomainID = ADC.numDomainID AND  VOA.numDivisionId = DM.numDivisionID
  LEFT JOIN View_InboxEmail VIE WITH (NOEXPAND) ON VIE.numDomainID = ADC.numDomainID AND  VIE.numContactId = ADC.numContactId
  LEFT JOIN AddressDetails AD ON AD.numRecordID = ADC.numContactID AND AD.tintAddressOf=1 AND AD.tintAddressType=0 AND AD.bitIsPrimary=1
   '+@WhereCondition+                      
' join tblcontacts T on T.numContactId=ADC.numContactId'                     
  
--' union select convert(varchar(10),count(*)),null,null,null,null,null '+@strColumns+' from tblcontacts order by RowNumber'                                                 

IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
BEGIN
	 if @vcCSLookBackTableName = 'AdditionalContactsInformation'                  
		set @Prefix = 'ADC.'                  
     if @vcCSLookBackTableName = 'DivisionMaster'                  
		set @Prefix = 'DM.'
	 if @vcCSLookBackTableName = 'CompanyInfo'                  
		set @Prefix = 'CI.' 
	 if @vcCSLookBackTableName = 'AddressDetails'                  
		set @Prefix = 'AD.'   
 
	IF @vcCSAssociatedControlType='DateField'
	BEGIN
--		set @strSql=@strSql+' left join #tempColorScheme tCS on ' + @Prefix + @vcCSOrigDbCOlumnName + '> DateAdd(day,CAST(SUBSTRING(tCS.vcFieldValue,1,CHARINDEX(''~'',tCS.vcFieldValue)-1) AS INT),GETDATE())
--			 and ' + @Prefix + @vcCSOrigDbCOlumnName + '< DateAdd(day,CAST(SUBSTRING(tCS.vcFieldValue,CHARINDEX(''~'',tCS.vcFieldValue)+1,len(tCS.vcFieldValue) - PATINDEX(''~'',tCS.vcFieldValue)) AS INT),GETDATE())'
		
		
			set @strSql=@strSql+' left join #tempColorScheme tCS on CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) >= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue as int),GETUTCDATE()),111)
			 and CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) <= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue1 as int),GETUTCDATE()),111)'

	END
	ELSE IF @vcCSAssociatedControlType='SelectBox' OR @vcCSAssociatedControlType='ListBox' OR @vcCSAssociatedControlType='CheckBox'
	BEGIN
		set @strSql=@strSql+' left join #tempColorScheme tCS on tCS.vcFieldValue=' + @Prefix + @vcCSOrigDbCOlumnName
	END
END
    

set @strSql=@strSql+' WHERE CI.numDomainID=DM.numDomainID   
  and DM.numDomainID=ADC.numDomainID   
  and RowNumber > '+convert(varchar(10),@firstRec)+ ' and RowNumber <'+ convert(varchar(10),@lastRec)+ ' order by RowNumber'

                                                     
print @strSql                              
                      
exec (@strSql)


SELECT * FROM #tempForm

DROP TABLE #tempForm  

drop table #tempColorScheme
/****** Object:  StoredProcedure [dbo].[USP_GetCreditStatusOfCompany]    Script Date: 07/26/2008 16:17:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcreditstatusofcompany')
DROP PROCEDURE usp_getcreditstatusofcompany
GO
CREATE PROCEDURE [dbo].[USP_GetCreditStatusOfCompany]    
@numDivisionID as numeric(9)    
as    
    
declare @numDomainID as numeric(9)
declare @bitCheckCreditStatus as bit
select @numDomainID=numDomainID from divisionMaster where numDivisionID=@numDivisionID
select @bitCheckCreditStatus=isnull(bitCheckCreditStatus,0) from eCommerceDTL where numDomainID=@numDomainID

if @bitCheckCreditStatus=1
BEGIN
	SELECT 
		ISNULL(vcData,0) 
	FROM 
		companyinfo Com    
	JOIN 
		divisionmaster Div    
	ON 
		Com.numCompanyID=Div.numCompanyID  
	LEFT JOIN 
		listdetails   
	ON
		numListID=3 AND numListItemID=numCompanyCredit  
	WHERE 
		div.numDivisionID=@numDivisionID  
END
else
BEGIN
	SELECT 10000000
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetCustProjectManager')
DROP PROCEDURE USP_GetCustProjectManager
GO
-- =============================================
-- Author:		PRIYA
-- Create date: <2 FEB 2018>
-- Description:	<Get custProjectMGR email id on click of email from Project details screen>
-- =============================================
CREATE PROCEDURE [dbo].[USP_GetCustProjectManager]
@ProID numeric ,
@numDomainId numeric

  AS

  Select ACI.vcEmail from ProjectsMaster PM
  INNER JOIN AdditionalContactsInformation ACI
  ON PM.numCustPrjMgr = ACI.numContactId
  WHERE PM.numProId = @ProID  AND PM.numDomainId = @numDomainId

 

GO


/****** Object:  StoredProcedure [dbo].[USP_GetDealsList1]    Script Date: 05/07/2009 22:12:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getdealslist1')
DROP PROCEDURE usp_getdealslist1
GO
CREATE PROCEDURE [dbo].[USP_GetDealsList1]
               @numUserCntID              NUMERIC(9)  = 0,
               @numDomainID               NUMERIC(9)  = 0,
               @tintSalesUserRightType    TINYINT  = 0,
               @tintPurchaseUserRightType TINYINT  = 0,
               @tintSortOrder             TINYINT  = 4,
               @CurrentPage               INT,
               @PageSize                  INT,
               @TotRecs                   INT  OUTPUT,
               @columnName                AS VARCHAR(50),
               @columnSortOrder           AS VARCHAR(10),
               @numCompanyID              AS NUMERIC(9)  = 0,
               @bitPartner                AS BIT  = 0,
               @ClientTimeZoneOffset      AS INT,
               @tintShipped               AS TINYINT,
               @intOrder                  AS TINYINT,
               @intType                   AS TINYINT,
               @tintFilterBy              AS TINYINT  = 0,
               @numOrderStatus			  AS NUMERIC,
			   @vcRegularSearchCriteria varchar(MAX)='',
			   @vcCustomSearchCriteria varchar(MAX)='',
			   @SearchText VARCHAR(300) = '',
			   @SortChar char(1)='0',
			   @tintDashboardReminderType TINYINT = 0
AS
	DECLARE  @PageId  AS TINYINT
	DECLARE  @numFormId  AS INT 
  
	SET @PageId = 0

	IF @inttype = 1
	BEGIN
		SET @PageId = 2
		SET @inttype = 3
		SET @numFormId=39
	END
	ELSE IF @inttype = 2
	BEGIN
		SET @PageId = 6
		SET @inttype = 4
		SET @numFormId=41
	END

	CREATE TABLE #tempForm 
	(
		tintOrder TINYINT,vcDbColumnName nvarchar(50),vcOrigDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),vcListItemType varchar(3),
		numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,
		bitIsAlphaNumeric BIT,bitIsNumeric BIT,bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),
		ListRelID numeric(9),intColumnWidth int,bitAllowFiltering BIT,vcFieldDataType CHAR(1)
	)

	-- FIRST CHECK IF GRID COLUMNS ARE CONFIGURED
	DECLARE  @Nocolumns  AS TINYINT = 0

	SELECT 
		@Nocolumns=ISNULL(SUM(TotalRow),0) 
	FROM
	(            
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND numRelCntType = @numFormId
		UNION 
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicCustomColumns 
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND numRelCntType = @numFormId
	) TotalRows

	-- IF GRID COLUMNS ARE NOT CONFIGURES THEN CHECK IF MASTER FORM CONFIGURATION IS CREATED BY ADMINISTRATOR
	DECLARE @IsMasterConfAvailable BIT = 0
	DECLARE @numUserGroup NUMERIC(18,0)

	IF @Nocolumns=0
	BEGIN
		SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

		IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = @numFormId AND numRelCntType=@numFormId AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
		BEGIN
			SET @IsMasterConfAvailable = 1
		END
	END

	--IF MASTERCONFIGURATION IS AVAILABLE THEN LOAD IT OTHERWISE LOAD DEFAULT COLUMNS
	IF @IsMasterConfAvailable = 1
	BEGIN
		INSERT INTO DycFormConfigurationDetails 
		(
			numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
		)
		SELECT 
			@numFormId,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,@numFormId,1,0,intColumnWidth
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.numRelCntType=@numFormId AND 
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
		UNION
		SELECT 
			@numFormId,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,@numFormId,1,1,intColumnWidth
		FROM 
			View_DynamicCustomColumnsMasterConfig
		WHERE 
			View_DynamicCustomColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.numRelCntType=@numFormId AND 
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1


		INSERT INTO 
			#tempForm
		SELECT 
			(intRowNum + 1) as tintOrder, vcDbColumnName,	vcOrigDbColumnName,	ISNULL(vcCultureFieldName,vcFieldName), vcAssociatedControlType,
			vcListItemType, numListID, vcLookBackTableName,	bitCustom, numFieldId, bitAllowSorting,	bitInlineEdit, bitIsRequired,
			bitIsEmail,	bitIsAlphaNumeric, bitIsNumeric, bitIsLengthValidation,	intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,
			ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType 
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.numRelCntType=@numFormId AND 
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName, vcAssociatedControlType,'' as vcListItemType,numListID,'',
			bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,''
		FROM 
			View_DynamicCustomColumnsMasterConfig
		WHERE 
			View_DynamicCustomColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.numRelCntType=@numFormId AND 
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1
		ORDER BY 
			tintOrder ASC  
	END
	ELSE                                        
	BEGIN
		IF @Nocolumns=0
		BEGIN
			INSERT INTO DycFormConfigurationDetails 
			(
				numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
			)
			SELECT 
				@numFormId,numFieldId,0,Row_number() over(order by tintRow desc),@numDomainID,@numUserCntID,@numFormId,1,0,intColumnWidth
			FROM 
				View_DynamicDefaultColumns
			WHERE 
				numFormId=@numFormId 
				AND bitDefault=1 
				AND ISNULL(bitSettingField,0)=1 
				AND numDomainID=@numDomainID
			ORDER BY 
				tintOrder asc   
		END

		INSERT INTO 
			#tempForm
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
			vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName,bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,bitFieldMessage,
			vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType  
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND ISNULL(bitSettingField,0)=1 
			AND ISNULL(bitCustom,0)=0  
			AND numRelCntType = @numFormId
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName,vcAssociatedControlType,'' as vcListItemType,numListID,'',bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,'' 
		FROM 
			View_DynamicCustomColumns
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND ISNULL(bitCustom,0)=1 
			AND numRelCntType = @numFormId
		ORDER BY 
			tintOrder asc  
	END

	DECLARE  @strColumns AS VARCHAR(MAX)
	SET @strColumns = ' ADC.numContactId, ADC.vcEmail, Div.numDivisionID,ISNULL(Div.numTerID,0) numTerID, opp.numRecOwner as numRecOwner, Div.tintCRMType, opp.numOppId, opp.monDealAmount, ISNULL(opp.intUsedShippingCompany,0) AS intUsedShippingCompany'
	--Corr.bitIsEmailSent,

	DECLARE  @tintOrder  AS TINYINT;SET @tintOrder = 0
	DECLARE  @vcFieldName  AS VARCHAR(50)
	DECLARE  @vcListItemType  AS VARCHAR(3)
	DECLARE  @vcListItemType1  AS VARCHAR(1)
	DECLARE  @vcAssociatedControlType VARCHAR(30)
	DECLARE  @numListID  AS NUMERIC(9)
	DECLARE  @vcDbColumnName VARCHAR(40)
	DECLARE  @WhereCondition VARCHAR(2000);SET @WhereCondition = ''
	DECLARE  @vcLookBackTableName VARCHAR(2000)
	DECLARE  @bitCustom  AS BIT
	Declare @numFieldId as numeric  
	DECLARE @bitAllowSorting AS CHAR(1)   
	DECLARE @bitAllowEdit AS CHAR(1)                 
	DECLARE @vcColumnName AS VARCHAR(500)          
	
	Declare @ListRelID as numeric(9) 
	DECLARE @SearchQuery AS VARCHAR(MAX) = ''        

	SELECT TOP 1 
		@tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,@vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,
		@numListID=numListID,@vcLookBackTableName=vcLookBackTableName ,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,
		@ListRelID=ListRelID
	FROM 
		#tempForm 
	ORDER BY 
		tintOrder ASC            
--	SET @strColumns = @strColumns + ' , (SELECT SUBSTRING((SELECT '', '' + cast(I.vcItemName as varchar)
--                      FROM OpportunityItems AS t
--					  LEFT JOIN OpportunityBizDocs as BC
--					  ON t.numOppId=BC.numOppId
--LEFT JOIN Item as I
--ON t.numItemCode=I.numItemCode
--WHERE t.numOppId=Opp.numOppId AND t.numoppItemtCode NOT IN (SELECT numOppItemId FROM OpportunityBizDocItems WHERE numOppBizDocID=BC.numOppBizDocsId)
--                     FOR XML PATH('''')), 2, 200000)  )[List_Item_Approval_UNIT] '
--SET @strColumns = @strColumns + ' , (SELECT 
-- SUBSTRING((SELECT '', '' + cast(I.vcItemName as varchar)
--FROM 
-- OpportunityItems AS t
--LEFT JOIN 
-- Item as I
--ON 
-- t.numItemCode=I.numItemCode
--WHERE 
-- t.numOppId=Opp.numOppId AND t.numoppItemtCode NOT IN (SELECT numOppItemId FROM OpportunityBizDocs JOIN OpportunityBizDocItems ON OpportunityBizDocs.numOppBizDocsId=OpportunityBizDocItems.numOppBizDocID  WHERE OpportunityBizDocs.numOppId=Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1)
--                     FOR XML PATH('''')), 2, 200000)  )[List_Item_Approval_UNIT] '
	WHILE @tintOrder > 0
    BEGIN
		IF @bitCustom = 0
		BEGIN
			DECLARE  @Prefix  AS VARCHAR(5)

			IF @vcLookBackTableName = 'AdditionalContactsInformation'
				SET @Prefix = 'ADC.'
			IF @vcLookBackTableName = 'DivisionMaster'
				SET @Prefix = 'Div.'
			IF @vcLookBackTableName = 'OpportunityMaster'
				SET @PreFix = 'Opp.'
			IF @vcLookBackTableName = 'OpportunityRecurring'
				SET @PreFix = 'OPR.'
			IF @vcLookBackTableName = 'CompanyInfo'
				SET @PreFix = 'cmp.'
			IF @vcLookBackTableName = 'ProjectProgress'
				SET @PreFix = 'PP'
			SET @vcColumnName= @vcDbColumnName+'~'+ convert(varchar(10),@numFieldId) +'~0'

			IF @vcAssociatedControlType = 'SelectBox'
			BEGIN
				IF @vcDbColumnName = 'numPartner'
				BEGIN
					SET @strColumns=@strColumns+ ' ,(Opp.numPartner) '+' ['+ @vcColumnName+']'    
					SET @strColumns=@strColumns+ ' ,(SELECT D.vcPartnerCode+''-''+C.vcCompanyName FROM DivisionMaster AS D LEFT JOIN CompanyInfo AS C ON D.numCompanyID=C.numCompanyID WHERE D.numDivisionID=Opp.numPartner) AS vcPartner' 
				END

				IF @vcDbColumnName = 'tintEDIStatus'
				BEGIN
					SET @strColumns = @strColumns + ',(CASE Opp.tintEDIStatus WHEN 1 THEN ''850 Received'' WHEN 2 THEN ''850 Acknowledged'' WHEN 3 THEN ''940 Sent'' WHEN 4 THEN ''940 Acknowledged'' WHEN 5 THEN ''856 Sent'' WHEN 6 THEN ''945 Acknowledged'' ELSE '''' END)' + ' [' + @vcColumnName + ']'
				END
				
				IF @vcDbColumnName = 'numPartenerContact'
				BEGIN
					SET @strColumns=@strColumns+ ' ,(Opp.numPartenerContact) '+' ['+ @vcColumnName+']'    
					SET @strColumns=@strColumns+ ' ,(SELECT A.vcFirstName+'' ''+ A.vcLastName FROM AdditionalContactsInformation AS A WHERE A.numContactId=Opp.numPartenerContact) AS vcPartenerContact' 
				END
				ELSE IF @vcDbColumnName = 'tintInvoicing'
				BEGIN
					SET @strColumns=CONCAT(@strColumns,', CONCAT(ISNULL((SELECT SUBSTRING((SELECT ''$^$'' + CONCAT(numOppBizDocsId,''#^#'',vcBizDocID,''#^#'',ISNULL(bitisemailsent,0),''#^#'')
										FROM OpportunityBizDocs WHERE numOppId=Opp.numOppId AND numBizDocId = ISNULL((SELECT TOP 1 numAuthoritativeSales FROM AuthoritativeBizDocs WHERE numDomainId=' + CAST (@numDomainID AS VARCHAR) + '),287) FOR XML PATH('''')),4,200000)),'''')',
										CASE 
										WHEN CHARINDEX('tintInvoicing=3',@vcRegularSearchCriteria) > 0
											THEN ', (SELECT 
													CONCAT(''('',SUM(X.InvoicedQty),'' Out of '',SUM(X.OrderedQty),'')'')
												FROM 
												(
													SELECT
														OI.numoppitemtCode,
														ISNULL(OI.numUnitHour,0) AS OrderedQty,
														ISNULL(TempInvoice.InvoicedQty,0) AS InvoicedQty
													FROM
														OpportunityItems OI
													INNER JOIN
														Item I
													ON
														OI.numItemCode = I.numItemCode
													OUTER APPLY
													(
														SELECT
															SUM(OpportunityBizDocItems.numUnitHour) AS InvoicedQty
														FROM
															OpportunityBizDocs
														INNER JOIN
															OpportunityBizDocItems 
														ON
															OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
														WHERE
															OpportunityBizDocs.numOppId = Opp.numOppId
															AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
															AND ISNULL(OpportunityBizDocs.bitAuthoritativeBizDocs,0) = 1
													) AS TempInvoice
													WHERE
														OI.numOppID = Opp.numOppId
												) X)'

											WHEN CHARINDEX('tintInvoicing=4',@vcRegularSearchCriteria) > 0
											THEN ', (SELECT 
													CONCAT(''('',(X.InvoicedPercentage),'' % '','')'')
												FROM 
												(

													SELECT fltBreakupPercentage  AS InvoicedPercentage
													FROM dbo.OpportunityRecurring OPR 
														INNER JOIN dbo.OpportunityMaster OM ON OPR.numOppID = OM.numOppID
														LEFT JOIN dbo.OpportunityBizDocs OBD ON OBD.numOppBizDocsID = OPR.numOppBizDocID
													WHERE OPR.numOppId = Opp.numOppId 
														and tintRecurringType <> 4
												) X)'

											ELSE 
													','''''
											END,') [', @vcColumnName,']')


				END
				IF @vcDbColumnName = 'numStatus'
				BEGIN
					SET @strColumns=@strColumns+ ' ,(SELECT COUNT(OpportunityItems.numoppitemtCode) FROM OpportunityItems WHERE OpportunityItems.bitItemPriceApprovalRequired=1 AND OpportunityItems.numOppId=Opp.numOppId) ApprovalMarginCount'
				END
				
				IF @vcDbColumnName = 'tintSource'
				BEGIN
					SET @strColumns = @strColumns + ',ISNULL(dbo.fn_GetOpportunitySourceValue(ISNULL(Opp.tintSource,0),ISNULL(Opp.tintSourceType,0),Opp.numDomainID),''Internal Order'')' + ' [' + @vcColumnName + ']'

					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'ISNULL(dbo.fn_GetOpportunitySourceValue(ISNULL(Opp.tintSource,0),ISNULL(Opp.tintSourceType,0),Opp.numDomainID),''Internal Order'') LIKE ''%' + @SearchText + '%'''
					END
				END

				ELSE IF @vcDbColumnName = 'numCampainID'
				BEGIN
					SET @strColumns = @strColumns + ', (SELECT vcCampaignName FROM CampaignMaster WHERE numCampaignID= Opp.numCampainID) ' + ' [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'ISNULL((SELECT vcCampaignName FROM CampaignMaster WHERE numCampaignID= Opp.numCampainID),'''') LIKE ''%' + @SearchText + '%'''
					END
				END

				ELSE IF @vcDbColumnName = 'numContactID'
				BEGIN
					SET @strColumns = @strColumns + ',dbo.fn_GetContactName(' + @Prefix + @vcDbColumnName + ') [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'dbo.fn_GetContactName(' + @Prefix + @vcDbColumnName + ') LIKE ''%' + @SearchText + '%'''
					END
				END
				ELSE IF @vcListItemType = 'LI'
				BEGIN
					SET @strColumns = @strColumns + ',L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData' + ' [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData LIKE ''%' + @SearchText + '%'''
					END

					SET @WhereCondition = @WhereCondition + ' left Join ListDetails L' + CONVERT(VARCHAR(3),@tintOrder) + ' on L' + CONVERT(VARCHAR(3),@tintOrder) + '.numListItemID=' + @vcDbColumnName
				END
				ELSE IF @vcListItemType = 'S'
				BEGIN
					SET @strColumns = @strColumns + ',S' + CONVERT(VARCHAR(3),@tintOrder) + '.vcState' + ' [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'S' + CONVERT(VARCHAR(3),@tintOrder) + '.vcState LIKE ''%' + @SearchText + '%'''
					END

					SET @WhereCondition = @WhereCondition + ' left join State S' + CONVERT(VARCHAR(3),@tintOrder) + ' on S' + CONVERT(VARCHAR(3),@tintOrder) + '.numStateID=' + @vcDbColumnName
				END
				ELSE IF @vcListItemType = 'BP'
				BEGIN
					SET @strColumns = @strColumns + ',SPLM.Slp_Name' + ' [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'SPLM.Slp_Name LIKE ''%' + @SearchText + '%'''
					END

					SET @WhereCondition = @WhereCondition + ' LEFT JOIN Sales_process_List_Master SPLM on SPLM.Slp_Id = OPP.numBusinessProcessID '
				END
				ELSE IF @vcListItemType = 'PP'
				BEGIN
					SET @strColumns = @strColumns + ',ISNULL(PP.intTotalProgress,0)' + ' [' + @vcColumnName + ']'

					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'ISNULL(PP.intTotalProgress,0) LIKE ''%' + @SearchText + '%'''
					END
				END
				ELSE IF @vcListItemType = 'T'
				BEGIN
					SET @strColumns = @strColumns + ',L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData' + ' [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData LIKE ''%' + @SearchText + '%'''
					END

					SET @WhereCondition = @WhereCondition + ' left Join ListDetails L' + CONVERT(VARCHAR(3),@tintOrder) + ' on L' + CONVERT(VARCHAR(3),@tintOrder) + '.numListItemID=' + @vcDbColumnName
				END
				ELSE IF @vcListItemType = 'U'
				BEGIN
					SET @strColumns = @strColumns + ',dbo.fn_GetContactName(' + @Prefix + @vcDbColumnName + ') [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'dbo.fn_GetContactName(' + @Prefix + @vcDbColumnName + ') LIKE ''%' + @SearchText + '%'''
					END
				END
				ELSE IF @vcListItemType = 'WI'
				BEGIN
					SET @strColumns = @strColumns + ', Case when isnull(OL.numSiteID,0)>0 then Sites.vcSiteName else WebAPI.vcProviderName end [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + '(Case when isnull(OL.numSiteID,0)>0 then Sites.vcSiteName else WebAPI.vcProviderName end) LIKE ''%' + @SearchText + '%'''
					END

					set @WhereCondition= @WhereCondition +' left JOIN OpportunityLinking OL on OL.numChildOppID=Opp.numOppId left join WebAPI on WebAPI.WebApiId=isnull(OL.numWebApiId,0)
													left join Sites on isnull(OL.numSiteID,0)=Sites.numSIteID and Sites.numDomainID=' + convert(varchar(15),@numDomainID)                                                       
				END 
            END
			ELSE IF @vcAssociatedControlType = 'DateField'
			BEGIN
				IF @Prefix ='OPR.'
				BEGIN
					SET @strColumns = @strColumns + ', (SELECT TOP 1 dbo.FormatedDateFromDate( DateAdd(minute,+'+convert(varchar(15),-@ClientTimeZoneOffset)+', dtRecurringDate)'+ ','+ CONVERT(VARCHAR(10),@numDomainId) +') FROM  dbo.OpportunityRecurring WHERE numOppID=OPP.numOppID AND ISNULL(numOppBizDocID,0)=0 and dtRecurringDate > dbo.GetUTCDateWithoutTime()) AS  ['+ @vcColumnName + ']'

					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + '(SELECT TOP 1 dbo.FormatedDateFromDate( DateAdd(minute,+'+convert(varchar(15),-@ClientTimeZoneOffset)+', dtRecurringDate)'+ ','+ CONVERT(VARCHAR(10),@numDomainId) +') FROM  dbo.OpportunityRecurring WHERE numOppID=OPP.numOppID AND ISNULL(numOppBizDocID,0)=0 and dtRecurringDate > dbo.GetUTCDateWithoutTime()) LIKE ''%' + @SearchText + '%'''
					END
				END
				ELSE IF @vcDbColumnName = 'dtReleaseDate'
				BEGIN
					SET @strColumns = @strColumns
                                + ',case when convert(varchar(11),'
                                + @Prefix
                                + @vcDbColumnName
                                + ')= convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getdate())) then ''<b><font color=red>Today</font></b>'''
					SET @strColumns = @strColumns
									+ 'when convert(varchar(11),'
									+ @Prefix
									+ @vcDbColumnName
									+ ')= convert(varchar(11),dateadd(day,-1,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getdate()))) then ''<b><font color=purple>Yesterday</font></b>'''
					SET @strColumns = @strColumns
									+ 'when convert(varchar(11),'
									+ @Prefix
									+ @vcDbColumnName
									+ ')= convert(varchar(11),dateadd(day,1,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getdate()))) then''<b><font color=orange>Tommorow</font></b>'' '
					SET @strColumns = @strColumns
									+ 'else dbo.FormatedDateFromDate('
									+ @Prefix
									+ @vcDbColumnName
									+ ','
									+ CONVERT(VARCHAR(10),@numDomainId)
									+ ') end  ['
									+ @vcColumnName + ']' 	

					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + @Prefix + @vcDbColumnName + ' LIKE ''%' + @SearchText + '%'''
					END

				END
				ELSE
				BEGIN
					SET @strColumns = @strColumns
                                + ',case when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','
                                + @Prefix
                                + @vcDbColumnName
                                + ') )= convert(varchar(11),getdate()) then ''<b><font color=red>Today</font></b>'''
					SET @strColumns = @strColumns
									+ 'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','
									+ @Prefix
									+ @vcDbColumnName
									+ ') )= convert(varchar(11),dateadd(day,-1,getdate())) then ''<b><font color=purple>Yesterday</font></b>'''
					SET @strColumns = @strColumns
									+ 'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','
									+ @Prefix
									+ @vcDbColumnName
									+ ') )= convert(varchar(11),dateadd(day,1,getdate())) then''<b><font color=orange>Tommorow</font></b>'' '
					SET @strColumns = @strColumns
									+ 'else dbo.FormatedDateFromDate('
									+ @Prefix
									+ @vcDbColumnName
									+ ','
									+ CONVERT(VARCHAR(10),@numDomainId)
									+ ') end  ['
									+ @vcColumnName + ']' 	

					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + @Prefix + @vcDbColumnName + ' LIKE ''%' + @SearchText + '%'''
					END
				 END	
            END
            ELSE
            IF @vcAssociatedControlType = 'TextBox'
            BEGIN
				
                SET @strColumns = @strColumns
                                  + ','
                                  + CASE 
                                      WHEN @vcDbColumnName = 'numAge' THEN 'year(getutcdate())-year(bintDOB)'
                                      WHEN @vcDbColumnName = 'monPAmount' THEN 'isnull((SELECT varCurrSymbol FROM [Currency] WHERE numCurrencyID = Opp.[numCurrencyID]),'''') + '' ''+ CONVERT(VARCHAR,CONVERT(DECIMAL(20,5),monPAmount),1)'
                                      WHEN @vcDbColumnName = 'numDivisionID' THEN 'Div.numDivisionID'
                                      WHEN @vcDbColumnName = 'tintOppStatus' THEN 'CASE tintOppStatus WHEN 0 then ''Open'' WHEN 1 THEN ''Closed-Won'' WHEN 2 THEN ''Closed-Lost'' END '
                                      WHEN @vcDbColumnName = 'tintOppType' THEN 'CASE tintOppType WHEN 1 then ''Sales'' WHEN 2 THEN ''Purchase'' Else ''NA'' END '
									  WHEN @vcDbColumnName = 'vcPOppName' THEN 'dbo.CheckChildRecord(Opp.numOppId,Opp.numDomainID)'
									  WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'vcBizDocsList' THEN 'ISNULL((SELECT SUBSTRING((SELECT ''$^$'' + CAST(numOppBizDocsId AS VARCHAR(18)) +''#^#''+ vcBizDocID
										FROM OpportunityBizDocs WHERE numOppId=Opp.numOppId AND 1 = (Case When bitAuthoritativeBizDocs=1 AND 1=' + (CASE WHEN @inttype=1 THEN '1' ELSE '0' END) + '  Then 0 ELSE 1 END) FOR XML PATH('''')),4,200000)),'''')'
                                      ELSE @Prefix + @vcDbColumnName
                                    END
                                  + ' ['
                                  + @vcColumnName + ']'
				IF(@vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'vcBizDocsList')
				BEGIN
					SET @strColumns=@strColumns+','+' (SELECT COUNT(I.vcItemName) FROM 
													 OpportunityItems AS t LEFT JOIN  Item as I ON 
													 t.numItemCode=I.numItemCode WHERE 
													 t.numOppId=Opp.numOppId AND t.numoppItemtCode NOT IN 
													 (SELECT numOppItemId FROM OpportunityBizDocs JOIN 
													 OpportunityBizDocItems ON OpportunityBizDocs.numOppBizDocsId=OpportunityBizDocItems.numOppBizDocID  
													 WHERE OpportunityBizDocs.numOppId=Opp.numOppId 
													 AND 1 = (Case When bitAuthoritativeBizDocs=1 Or numBizDocID=293 Then 0 ELSE 1 END) 
													 AND ISNULL(bitAuthoritativeBizDocs,0)=1)) AS [List_Item_Approval_UNIT] '
				END
				
				IF LEN(@SearchText) > 0
				BEGIN
					SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + (CASE 
                                      WHEN @vcDbColumnName = 'numAge' THEN 'year(getutcdate())-year(bintDOB)'
                                      WHEN @vcDbColumnName = 'monPAmount' THEN 'isnull((SELECT varCurrSymbol FROM [Currency] WHERE numCurrencyID = Opp.[numCurrencyID]),'''') + '' ''+ CONVERT(VARCHAR,CONVERT(DECIMAL(20,5),monPAmount),1)'
                                      WHEN @vcDbColumnName = 'numDivisionID' THEN 'Div.numDivisionID'
                                      WHEN @vcDbColumnName = 'tintOppStatus' THEN 'CASE tintOppStatus WHEN 0 then ''Open'' WHEN 1 THEN ''Closed-Won'' WHEN 2 THEN ''Closed-Lost'' END '
                                      WHEN @vcDbColumnName = 'tintOppType' THEN 'CASE tintOppType WHEN 1 then ''Sales'' WHEN 2 THEN ''Purchase'' Else ''NA'' END '
									  WHEN @vcDbColumnName = 'vcPOppName' THEN 'dbo.CheckChildRecord(Opp.numOppId,Opp.numDomainID)'
									  WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'vcBizDocsList' THEN 'ISNULL((SELECT SUBSTRING((SELECT ''$^$'' + CAST(numOppBizDocsId AS VARCHAR(18)) +''#^#''+ vcBizDocID
										FROM OpportunityBizDocs WHERE numOppId=Opp.numOppId AND 1 = (Case When bitAuthoritativeBizDocs=1 Or numBizDocID=293 Then 0 ELSE 1 END)  FOR XML PATH('''')),4,200000)),'''')'
                                      ELSE @Prefix + @vcDbColumnName
                                    END) + ' LIKE ''%' + @SearchText + '%'''
				END
            END
            ELSE
            IF @vcAssociatedControlType = 'TextArea'
            BEGIN
				SET @strColumns=@strColumns+','+  @Prefix + @vcDbColumnName +' ['+ @vcColumnName+']' 

				IF LEN(@SearchText) > 0
				BEGIN
					SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + @Prefix + @vcDbColumnName + ' LIKE ''%' + @SearchText + '%'''
				END
            END
            ELSE IF  @vcAssociatedControlType='Label'                                              
			BEGIN
				SET @strColumns=@strColumns + ',' + CASE                    
				WHEN @vcDbColumnName = 'CalAmount' THEN 'Opp.monDealAmount'--'[dbo].[getdealamount](Opp.numOppId,Getutcdate(),0)'
				WHEN @vcDbColumnName = 'vcInventoryStatus' THEN 'ISNULL(dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID),'''')'
				WHEN @vcDbColumnName = 'vcOrderedShipped' THEN 'dbo.GetOrderShipReceiveStatus(Opp.numOppId,1,Opp.tintshipped)'
				WHEN @vcDbColumnName = 'vcOrderedReceived' THEN 'dbo.GetOrderShipReceiveStatus(Opp.numOppId,2,Opp.tintshipped)'
				WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monDealAmount' THEN 'ISNULL((SELECT SUM(ISNULL(monDealAmount,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
				WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monAmountPaid' THEN 'ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
				else @Prefix + @vcDbColumnName END +' ['+ @vcColumnName+']'        
				
				IF @vcDbColumnName='vcOrderedShipped'
				BEGIN
				DECLARE @temp VARCHAR(MAX)
				SET @temp = '(SELECT 
				STUFF((SELECT '', '' + CAST((CAST(numOppBizDocsId AS varchar)+''~''+CAST(ISNULL(numShippingReportId,0) AS varchar) ) AS VARCHAR(MAX)) [text()]
				FROM OpportunityBizDocs
				LEFT JOIN ShippingReport
				ON ShippingReport.numOppBizDocId=OpportunityBizDocs.numOppBizDocsId
				WHERE OpportunityBizDocs.numOppId=Opp.numOppId  AND OpportunityBizDocs.bitShippingGenerated=1  FOR XML PATH(''''), TYPE)
						.value(''.'',''NVARCHAR(MAX)''),1,2,'' ''))'
					SET @strColumns = @strColumns + ' , (SELECT SUBSTRING('+ @temp +', 2, 200000))AS ShippingIcons '
					SET @strColumns = @strColumns + ' , CASE WHEN (SELECT COUNT(*) FROM ShippingBox INNER JOIN ShippingReport ON ShippingBox.numShippingReportId = ShippingReport.numShippingReportId WHERE ShippingReport.numOppID = Opp.numOppID AND LEN(vcTrackingNumber) > 0) > 0 THEN 1 ELSE 0 END [ISTrackingNumGenerated]'
				END

				IF LEN(@SearchText) > 0
				BEGIN
					SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + (CASE                    
					WHEN @vcDbColumnName = 'CalAmount' THEN 'Opp.monDealAmount'
					WHEN @vcDbColumnName = 'vcInventoryStatus' THEN 'ISNULL(dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID),'''')'
					WHEN @vcDbColumnName = 'vcOrderedShipped' THEN 'dbo.GetOrderShipReceiveStatus(Opp.numOppId,1,Opp.tintshipped)'
					WHEN @vcDbColumnName = 'vcOrderedReceived' THEN 'dbo.GetOrderShipReceiveStatus(Opp.numOppId,2,Opp.tintshipped)'
					WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monDealAmount' THEN 'ISNULL((SELECT SUM(ISNULL(monDealAmount,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
					WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monAmountPaid' THEN 'ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
					else @Prefix + @vcDbColumnName END) + ' LIKE ''%' + @SearchText + '%'''
				END    
			END
            ELSE IF @vcAssociatedControlType='Popup' and @vcDbColumnName='numShareWith'                                                   
			BEGIN
				SET @strColumns = @strColumns + ',(SELECT SUBSTRING(
								(SELECT '','' + A.vcFirstName+'' ''+A.vcLastName + CASE WHEN ISNULL(SR.numContactType,0)>0 THEN ''('' + dbo.fn_GetListItemName(SR.numContactType) + '')'' ELSE '''' END
								FROM ShareRecord SR JOIN UserMaster UM ON UM.numUserDetailId=SR.numAssignedTo 
								JOIN AdditionalContactsInformation A ON UM.numUserDetailId=A.numContactID 
								WHERE SR.numDomainID=Opp.numDomainID AND SR.numModuleID=3 AND SR.numRecordID=Opp.numOppId
								AND UM.numDomainID=Opp.numDomainID and UM.numDomainID=A.numDomainID FOR XML PATH('''')),2,200000)) ['+ @vcColumnName+']'                
			END 
        END
		ELSE
        BEGIN
			SET @vcColumnName= @vcDbColumnName +'~'+ convert(varchar(10),@numFieldId)+'~1'

			IF @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'
            BEGIN
              SET @strColumns = @strColumns
                              + ',CFW'
                              + CONVERT(VARCHAR(3),@tintOrder)
                              + '.Fld_Value  ['
                              + @vcColumnName + ']'
              SET @WhereCondition = @WhereCondition
                                      + ' left Join CFW_FLD_Values_Opp CFW'
                                      + CONVERT(VARCHAR(3),@tintOrder)
                                      + ' on CFW'
                                      + CONVERT(VARCHAR(3),@tintOrder)
                                      + '.Fld_Id='
                                      + CONVERT(VARCHAR(10),@numFieldId)
                                      + 'and CFW'
                                      + CONVERT(VARCHAR(3),@tintOrder)
                                      + '.RecId=Opp.numOppid   '
			END
			ELSE
            IF @vcAssociatedControlType = 'CheckBox' 
            BEGIN
                SET @strColumns = @strColumns
                                + ',case when isnull(CFW'
                                + CONVERT(VARCHAR(3),@tintOrder)
                                + '.Fld_Value,0)=0 then 0 when isnull(CFW'
                                + CONVERT(VARCHAR(3),@tintOrder)
                                + '.Fld_Value,0)=1 then 1 end   ['
                                + @vcColumnName + ']'
                SET @WhereCondition = @WhereCondition
                                        + ' left Join CFW_FLD_Values_Opp CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + ' on CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.Fld_Id='
                                        + CONVERT(VARCHAR(10),@numFieldId)
                                        + 'and CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.RecId=Opp.numOppid   '
            END
            ELSE
            IF @vcAssociatedControlType = 'DateField'
            BEGIN
                  SET @strColumns = @strColumns
                                  + ',dbo.FormatedDateFromDate(CFW'
                                  + CONVERT(VARCHAR(3),@tintOrder)
                                  + '.Fld_Value,'
                                  + CONVERT(VARCHAR(10),@numDomainId)
                                  + ')  ['
                                  + @vcColumnName + ']'
                  SET @WhereCondition = @WhereCondition
                                          + ' left Join CFW_FLD_Values_Opp CFW'
                                          + CONVERT(VARCHAR(3),@tintOrder)
                                          + ' on CFW'
                                          + CONVERT(VARCHAR(3),@tintOrder)
                                          + '.Fld_Id='
                                          + CONVERT(VARCHAR(10),@numFieldId)
                                          + 'and CFW'
                                          + CONVERT(VARCHAR(3),@tintOrder)
                                          + '.RecId=Opp.numOppid    '
            END
            ELSE
            IF @vcAssociatedControlType = 'SelectBox'
            BEGIN
                SET @vcDbColumnName = 'DCust'
                                            + CONVERT(VARCHAR(10),@numFieldId)
                SET @strColumns = @strColumns
                                + ',L'
                                + CONVERT(VARCHAR(3),@tintOrder)
                                + '.vcData'
                                + ' ['
                                + @vcColumnName + ']'

                SET @WhereCondition = @WhereCondition
                                        + ' left Join CFW_FLD_Values_Opp CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + ' on CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.Fld_Id='
                                        + CONVERT(VARCHAR(10),@numFieldId)
                                        + 'and CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.RecId=Opp.numOppid '

                SET @WhereCondition = @WhereCondition
                                        + ' left Join ListDetails L'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + ' on L'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.numListItemID=CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.Fld_Value'
            END
			ELSE
			BEGIN
				SET @strColumns = @strColumns + CONCAT(',dbo.GetCustFldValueOpp(',@numFieldId,',opp.numOppID)') + ' [' + @vcColumnName + ']'
			END

			IF LEN(@SearchText) > 0
			BEGIN
				SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + CONCAT('dbo.GetCustFldValueOpp(',@numFieldId,',opp.numOppID) LIKE ''%',@SearchText,'%''')
			END
		END
      
     
		SELECT TOP 1 
			@tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,@vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,
			@numListID=numListID,@vcLookBackTableName=vcLookBackTableName,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,
			@ListRelID=ListRelID
		FROM 
			#tempForm 
		WHERE 
			tintOrder > @tintOrder-1 
		ORDER BY 
			tintOrder asc            
 
		IF @@rowcount=0 SET @tintOrder=0 
	END 

	

	DECLARE @strShareRedordWith AS VARCHAR(300);
	SET @strShareRedordWith=' Opp.numOppId in (select SR.numRecordID FROM ShareRecord SR WHERE SR.numDomainID='+ convert(varchar(15),@numDomainID )+' AND SR.numModuleID=3 AND SR.numAssignedTo='+convert(varchar(15),@numUserCntId)+') '


	DECLARE @StrSql AS VARCHAR(MAX) = ''

	SET @StrSql = @StrSql + ' FROM OpportunityMaster Opp            
								 ##PLACEHOLDER##                                                   
                                 LEFT JOIN AdditionalContactsInformation ADC ON Opp.numContactId = ADC.numContactId                                                               
                                 INNER JOIN DivisionMaster Div ON Opp.numDivisionId = Div.numDivisionID 
								 INNER JOIN CompanyInfo cmp ON Div.numCompanyID = cmp.numCompanyId 
								 LEFT JOIN ProjectProgress PP On PP.numOppId = Opp.numOppId ' + @WhereCondition
								 --INNER JOIN Correspondence Corr ON opp.numOppId = Corr.numOpenRecordID 
	-------Change Row Color-------
	
	DECLARE @vcCSOrigDbCOlumnName AS VARCHAR(50) = ''
	DECLARE @vcCSLookBackTableName AS VARCHAR(50) = ''
	DECLARE @vcCSAssociatedControlType AS VARCHAR(50)

	CREATE TABLE #tempColorScheme
	(
		vcOrigDbCOlumnName VARCHAR(50),
		vcLookBackTableName VARCHAR(50),
		vcFieldValue VARCHAR(50),
		vcFieldValue1 VARCHAR(50),
		vcColorScheme VARCHAR(50),
		vcAssociatedControlType varchar(50)
	)

	INSERT INTO 
		#tempColorScheme 
	SELECT 
		DFM.vcOrigDbCOlumnName,
		DFM.vcLookBackTableName,
		DFCS.vcFieldValue,
		DFCS.vcFieldValue1,
		DFCS.vcColorScheme,
		DFFM.vcAssociatedControlType 
	FROM 
		DycFieldColorScheme DFCS 
	JOIN 
		DycFormField_Mapping DFFM 
	ON 
		DFCS.numFieldID=DFFM.numFieldID
	JOIN 
		DycFieldMaster DFM 
	ON 
		DFM.numModuleID=DFFM.numModuleID and DFM.numFieldID=DFFM.numFieldID
	WHERE
		DFCS.numDomainID=@numDomainID 
		AND DFFM.numFormID=@numFormId 
		AND DFCS.numFormID=@numFormId 
		AND isnull(DFFM.bitAllowGridColor,0)=1

	IF(SELECT COUNT(*) FROM #tempColorScheme)>0
	BEGIN
		SELECT TOP 1 @vcCSOrigDbCOlumnName=vcOrigDbCOlumnName,@vcCSLookBackTableName=vcLookBackTableName,@vcCSAssociatedControlType=vcAssociatedControlType from #tempColorScheme
	END                        

	IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
	BEGIN
		SET @strColumns=@strColumns + ',tCS.vcColorScheme'
	END

	IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
	BEGIN
		IF @vcCSLookBackTableName = 'AdditionalContactsInformation'                  
			SET @Prefix = 'ADC.'                  
		IF @vcCSLookBackTableName = 'DivisionMaster'                  
			SET @Prefix = 'Div.'
		if @vcCSLookBackTableName = 'CompanyInfo'                  
			SET @Prefix = 'CMP.'   
		if @vcCSLookBackTableName = 'OpportunityMaster'                  
			SET @Prefix = 'Opp.'   
 
		IF @vcCSAssociatedControlType='DateField'
		BEGIN
			SET @strSql=@strSql+' LEFT JOIN #tempColorScheme tCS ON CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) >= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue as int),GETUTCDATE()),111)
				 and CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) <= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue1 as int),GETUTCDATE()),111)'
		END
		ELSE IF @vcCSAssociatedControlType='SelectBox' OR @vcCSAssociatedControlType='ListBox' OR @vcCSAssociatedControlType='CheckBox'
		BEGIN
			SET @strSql=@strSql+' LEFT JOIN #tempColorScheme tCS on tCS.vcFieldValue=' + @Prefix + @vcCSOrigDbCOlumnName
		END
	END
                          
	IF @columnName like 'CFW.Cust%'             
	BEGIN            
		SET @strSql = @strSql + ' LEFT JOIN CFW_Fld_Values_Opp CFW on CFW.RecId=Opp.numOppid and CFW.fld_id= '+ REPLACE(@columnName,'CFW.Cust','') +' '                                                     
		SET @columnName='CFW.Fld_Value'            
	END 

	IF @bitPartner = 1
		SET @strSql = @strSql + ' LEFT JOIN OpportunityContact OppCont ON OppCont.numOppId=Opp.numOppId AND OppCont.bitPartner=1 AND OppCont.numContactId=' + CONVERT(VARCHAR(15),@numUserCntID)
                    
                
	IF @tintFilterBy = 1 --Partially Fulfilled Orders 
		SET @strSql = @strSql + ' LEFT JOIN AdditionalContactsInformation ADC1 ON ADC1.numContactId=Opp.numRecOwner                                                               
                             LEFT OUTER JOIN [OpportunityBizDocs] OB ON OB.[numOppId] = Opp.[numOppId] 
                             WHERE Opp.numDomainID=' + CONVERT(VARCHAR(15),@numDomainID) + ' and Opp.tintOppstatus=1 and (Opp.tintShipped=' + CONVERT(VARCHAR(15),@tintShipped) + ' OR ' + CONVERT(VARCHAR(15),@tintShipped) + '=2)'
	ELSE
		SET @strSql = @strSql + ' LEFT JOIN AdditionalContactsInformation ADC1 on ADC1.numContactId=Opp.numRecOwner                                                               
                             WHERE Opp.numDomainID=' + CONVERT(VARCHAR(15),@numDomainID) + ' and Opp.tintOppstatus=1 and (Opp.tintShipped='+ CONVERT(VARCHAR(15),@tintShipped)  + ' OR ' + CONVERT(VARCHAR(15),@tintShipped) + '=2)'
  
  
	IF @intOrder <> 0
    BEGIN
      IF @intOrder = 1
        SET @strSql = @strSql + ' and Opp.bitOrder = 0 '
      IF @intOrder = 2
        SET @strSql = @strSql + ' and Opp.bitOrder = 1'
    END
    
	IF @numCompanyID <> 0
		SET @strSql = @strSql + ' And Div.numCompanyID ='+ CONVERT(VARCHAR(15),@numCompanyID)

	IF @SortChar <> '0' 
		SET @strSql = @strSql + ' And Opp.vcPOppName like ''' + @SortChar + '%'''
  

	IF @inttype = 3
	BEGIN
		IF @tintSalesUserRightType = 0
			SET @strSql = @strSql + ' AND opp.tintOppType=0'
		ELSE IF @tintSalesUserRightType = 1
			SET @strSql = @strSql + ' AND (Opp.numRecOwner= ' + CONVERT(VARCHAR(15),@numUserCntID)
						+ ' or Opp.numAssignedTo= '+ CONVERT(VARCHAR(15),@numUserCntID)
						+ CASE WHEN @bitPartner = 1 THEN ' or ( OppCont.bitPartner=1 and OppCont.numContactId='+ CONVERT(VARCHAR(15),@numUserCntID) + ')' ELSE '' END
						+ ' or ' + @strShareRedordWith +')'
		ELSE IF @tintSalesUserRightType = 2
			SET @strSql = @strSql + ' AND (Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+ CONVERT(VARCHAR(15),@numUserCntID) + ' ) 
									 or Opp.numAssignedTo= '+ CONVERT(VARCHAR(15),@numUserCntID)
									+ ' or ' + @strShareRedordWith +')'
	END
	ELSE IF @inttype = 4
	BEGIN
		IF @tintPurchaseUserRightType = 0
			SET @strSql = @strSql + ' AND opp.tintOppType=0'
		ELSE IF @tintPurchaseUserRightType = 1
			SET @strSql = @strSql + ' AND (Opp.numRecOwner= ' + CONVERT(VARCHAR(15),@numUserCntID)
						+ ' or Opp.numAssignedTo= '+ CONVERT(VARCHAR(15),@numUserCntID)
						+ CASE WHEN @bitPartner = 1 THEN ' or ( OppCont.bitPartner=1 and OppCont.numContactId='+ CONVERT(VARCHAR(15),@numUserCntID) + ')' ELSE '' END
						+ ' or ' + @strShareRedordWith +')'
		ELSE IF @tintPurchaseUserRightType = 2
			SET @strSql = @strSql + ' AND (Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+ CONVERT(VARCHAR(15),@numUserCntID) + ' ) 
									 or Opp.numAssignedTo= '+ CONVERT(VARCHAR(15),@numUserCntID)
									+ ' or ' + @strShareRedordWith +')'
	END

	
					
	IF @tintSortOrder <> '0'
		SET @strSql = @strSql + '  AND Opp.tintOppType= '+ CONVERT(VARCHAR(1),@tintSortOrder)
    
    
	IF @tintFilterBy = 1 --Partially Fulfilled Orders
		SET @strSql = @strSql + ' AND OB.bitPartialFulfilment = 1 AND OB.bitAuthoritativeBizDocs = 1 '
	ELSE IF @tintFilterBy = 2 --Fulfilled Orders
		SET @strSql = @strSql + ' AND Opp.[numOppId] IN (SELECT OM.numOppId FROM [OpportunityMaster] OM
                      			INNER JOIN [OpportunityBizDocs] OB ON OB.[numOppId] = OM.[numOppId]
                      			INNER JOIN [OpportunityItems] OI ON OM.[numOppId] = OI.[numOppId]
                      			INNER JOIN [OpportunityBizDocItems] BI ON BI.[numOppBizDocID] = OB.[numOppBizDocsId]
                      			Where OM.numDomainId=' + CONVERT(VARCHAR(15),@numDomainId) + 'AND OB.[bitAuthoritativeBizDocs] = 1 GROUP BY OM.[numOppId]
                      			HAVING   COUNT(OI.[numUnitHour]) = COUNT(BI.[numUnitHour])) '
	ELSE IF @tintFilterBy = 3 --Orders without Authoritative-BizDocs
		SET @strSql = @strSql + ' AND Opp.[numOppId] not IN (SELECT DISTINCT OM.[numOppId]
                      		  FROM [OpportunityMaster] OM JOIN [OpportunityBizDocs] OB ON OM.[numOppId] = OB.[numOppId]
                      		  Where OM.numDomainId=' + CONVERT(VARCHAR(15),@numDomainId) + ' AND OM.tintOppstatus=1 AND (OM.tintShipped=' + CONVERT(VARCHAR(15),@tintShipped) + ' OR ' + CONVERT(VARCHAR(15),@tintShipped) + '=2)' + ' AND OM.tintOppType= '+ CONVERT(VARCHAR(1),@tintSortOrder) + ' AND isnull(OB.[bitAuthoritativeBizDocs],0)=1) '
	ELSE IF @tintFilterBy = 4 --Orders with items yet to be added to Invoices
		SET @strSql = @strSql + ' AND Opp.[numOppId] IN (SELECT distinct OPPM.numOppId FROM [OpportunityMaster] OPPM
                                  INNER JOIN [OpportunityItems] OPPI ON OPPM.[numOppId] = OPPI.[numOppId]
                                  Where OPPM.numDomainId=' + CONVERT(VARCHAR(15),@numDomainId) + ' 
								  and OPPI.numoppitemtCode not in (select numOppItemID from [OpportunityBizDocs] OB INNER JOIN [OpportunityBizDocItems] BI
                      			  ON BI.[numOppBizDocID] = OB.[numOppBizDocsId] where OB.[numOppId] = OPPM.[numOppId]))'      
	ELSE IF @tintFilterBy = 6 --Orders with deferred Income/Invoices
		SET @strSql = @strSql + ' AND Opp.[numOppId] IN (SELECT DISTINCT OM.[numOppId]
                      		 FROM [OpportunityMaster] OM LEFT OUTER JOIN [OpportunityBizDocs] OB ON OM.[numOppId] = OB.[numOppId]
                      		 Where OM.numDomainId=' + CONVERT(VARCHAR(15),@numDomainId) + ' AND OB.[bitAuthoritativeBizDocs] =1 and OB.tintDeferred=1)'

	IF @numOrderStatus <>0 
		SET @strSql = @strSql + ' AND Opp.numStatus = ' + CONVERT(VARCHAR(15),@numOrderStatus);

	IF CHARINDEX('OI.vcInventoryStatus',@vcRegularSearchCriteria) > 0
	BEGIN
		IF CHARINDEX('OI.vcInventoryStatus=1',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'OI.vcInventoryStatus=1',' CHARINDEX(''Not Applicable'', dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID)) > 0 ')
		IF CHARINDEX('OI.vcInventoryStatus=2',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'OI.vcInventoryStatus=2',' CHARINDEX(''Allocation Cleared'', dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID)) > 0 ')
		IF CHARINDEX('OI.vcInventoryStatus=3',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'OI.vcInventoryStatus=3',' CHARINDEX(''Back Order'', dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID)) > 0 ')
		IF CHARINDEX('OI.vcInventoryStatus=4',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'OI.vcInventoryStatus=4',' CHARINDEX(''Ready to Ship'', dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID)) > 0 ')
	END
	
	IF CHARINDEX('Opp.tintInvoicing',@vcRegularSearchCriteria) > 0
	BEGIN
		IF CHARINDEX('Opp.tintInvoicing=0',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintInvoicing=0',' CHARINDEX(''All'', dbo.CheckOrderInvoicingStatus(Opp.numOppID,Opp.numDomainID)) > 0 ')
		IF CHARINDEX('Opp.tintInvoicing=1',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintInvoicing=1',' (SELECT (CASE WHEN SUM(ISNULL(InvoicedQty,0)) = SUM(ISNULL(OrderedQty,0)) THEN 1 ELSE 0 END)
	FROM 
	(
		SELECT OI.numoppitemtCode, ISNULL(OI.numUnitHour,0) AS OrderedQty, ISNULL(TempInvoice.InvoicedQty,0) AS InvoicedQty
		FROM OpportunityItems OI
		INNER JOIN Item I ON OI.numItemCode = I.numItemCode
		OUTER APPLY
		(
			SELECT SUM(OpportunityBizDocItems.numUnitHour) AS InvoicedQty
			FROM OpportunityBizDocs
				INNER JOIN OpportunityBizDocItems ON OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
			WHERE OpportunityBizDocs.numOppId = Opp.numOppID
				AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
				AND numBizDocId IN (SELECT numAuthoritativeSales FROM AuthoritativeBizDocs WHERE numDomainId = Opp.numDomainID)
		) AS TempInvoice
		WHERE
		OI.numOppID = Opp.numOppID
	) X) > 0 ')
		IF CHARINDEX('Opp.tintInvoicing=2',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintInvoicing=2',' (SELECT COUNT(*) 
		FROM OpportunityBizDocs 
		WHERE numOppId = Opp.numOppID 
			AND numBizDocId IN (SELECT numAuthoritativeSales FROM AuthoritativeBizDocs WHERE numDomainId = Opp.numDomainID)) = 0 ')
		IF CHARINDEX('Opp.tintInvoicing=3',@vcRegularSearchCriteria) > 0 
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintInvoicing=3',' (SELECT (CASE WHEN SUM(ISNULL(InvoicedQty,0)) > 0 THEN SUM(ISNULL(OrderedQty,0)) - SUM(ISNULL(InvoicedQty,0)) ELSE 0 END)
	FROM 
	(
		SELECT OI.numoppitemtCode, ISNULL(OI.numUnitHour,0) AS OrderedQty, ISNULL(TempInvoice.InvoicedQty,0) AS InvoicedQty
		FROM OpportunityItems OI
		INNER JOIN Item I ON OI.numItemCode = I.numItemCode
		OUTER APPLY
		(
			SELECT SUM(OpportunityBizDocItems.numUnitHour) AS InvoicedQty
			FROM OpportunityBizDocs
				INNER JOIN OpportunityBizDocItems ON OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
			WHERE OpportunityBizDocs.numOppId = Opp.numOppID
				AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
				AND numBizDocId IN (SELECT numAuthoritativeSales FROM AuthoritativeBizDocs WHERE numDomainId = Opp.numDomainID)
		) AS TempInvoice
		WHERE
		OI.numOppID = Opp.numOppID
	) X) > 0 ')
		
		IF CHARINDEX('Opp.tintInvoicing=4',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintInvoicing=4',' (SELECT (CASE WHEN SUM(ISNULL(InvoicedQty,0)) > 0 THEN SUM(ISNULL(OrderedQty,0)) - SUM(ISNULL(InvoicedQty,0)) ELSE 0 END)
	FROM 
	(
		SELECT OI.numoppitemtCode, ISNULL(OI.numUnitHour,0) AS OrderedQty, ISNULL(TempInvoice.InvoicedQty,0) AS InvoicedQty
		FROM OpportunityItems OI
		INNER JOIN Item I ON OI.numItemCode = I.numItemCode
		OUTER APPLY
		(
			SELECT SUM(OpportunityBizDocItems.numUnitHour) AS InvoicedQty
			FROM OpportunityBizDocs
				INNER JOIN OpportunityBizDocItems ON OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
			WHERE OpportunityBizDocs.numOppId = Opp.numOppID
				AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
				AND numBizDocId IN (SELECT numAuthoritativeSales FROM AuthoritativeBizDocs WHERE numDomainId = Opp.numDomainID)
		) AS TempInvoice
		WHERE
		OI.numOppID = Opp.numOppID
	) X) > 0 ')

		IF CHARINDEX('Opp.tintInvoicing=5',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintInvoicing=5',' 
				Opp.[numOppId] IN (SELECT DISTINCT OM.[numOppId]
                      		 FROM [OpportunityMaster] OM INNER JOIN [OpportunityBizDocs] OB ON OM.[numOppId] = OB.[numOppId]
                      		 Where OM.numDomainId = Opp.numDomainID AND OB.[bitAuthoritativeBizDocs] = 1 and OB.tintDeferred=1) ')
	END
	
	
	IF @vcRegularSearchCriteria<>'' 
	BEGIN
		SET @strSql=@strSql +' AND ' + @vcRegularSearchCriteria 
	END
	
	


	IF @vcCustomSearchCriteria<>'' 
	BEGIN
		SET @strSql = @strSql +' AND ' +  @vcCustomSearchCriteria
	END

	IF LEN(@SearchQuery) > 0
	BEGIN
		SET @strSql = @strSql + ' AND (' + @SearchQuery + ') '
	END

	IF ISNULL(@tintDashboardReminderType,0) = 19
	BEGIN
		SET @strSql = CONCAT(@strSql,' AND Opp.numOppID IN (','SELECT DISTINCT
																	OMInner.numOppID
																FROM 
																	StagePercentageDetails SPDInner
																INNER JOIN 
																	OpportunityMaster OMInner
																ON 
																	SPDInner.numOppID=OMInner.numOppId 
																WHERE 
																	SPDInner.numDomainId=',@numDOmainID,'
																	AND OMInner.numDomainId=',@numDOmainID,' 
																	AND OMInner.tintOppType = 1
																	AND ISNULL(OMInner.tintOppStatus,0) = 1
																	AND tinProgressPercentage <> 100',') ')

	END
	ELSE IF ISNULL(@tintDashboardReminderType,0) = 10
	BEGIN
		SET @strSql = CONCAT(@strSql,' AND Opp.numOppID IN (','SELECT DISTINCT
																	OMInner.numOppId
																FROM
																	OpportunityMaster OMInner
																INNER JOIN
																	OpportunityItems OIInner
																ON
																	OIInner.numOppId = OMInner.numOppID
																INNER JOIN
																	Item IInner
																ON
																	OIInner.numItemCode = IInner.numItemCode
																OUTER APPLY
																(
																	SELECT
																		SUM(OpportunityBizDocItems.numUnitHour) AS BilledQty
																	FROM
																		OpportunityBizDocs
																	INNER JOIN
																		OpportunityBizDocItems 
																	ON
																		OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																	WHERE
																		OpportunityBizDocs.numOppId = OMInner.numOppID
																		AND OpportunityBizDocItems.numOppItemID = OIInner.numoppitemtCode
																		AND OpportunityBizDocs.numBizDocId = (SELECT ISNULL(numAuthoritativePurchase,0) FROM AuthoritativeBizDocs WHERE numDomainId=',@numDomainID,')
																) AS TempBilled
																WHERE
																	OMInner.numDomainId = ',@numDomainID,'
																	AND ISNULL(OMInner.bitStockTransfer,0) = 0
																	AND OMInner.tintOppType = 2
																	AND OMInner.tintOppStatus  = 1
																	AND ISNULL(OIInner.numUnitHour,0) <> ISNULL(TempBilled.BilledQty,0)',') ')
	END
	ELSE IF ISNULL(@tintDashboardReminderType,0) = 11
	BEGIN
		SET @strSql = CONCAT(@strSql,' AND Opp.numOppID IN (','SELECT DISTINCT
																	OMInner.numOppId
																FROM
																	OpportunityMaster OMInner
																INNER JOIN
																	OpportunityItems OIInner
																ON
																	OIInner.numOppId = OMInner.numOppId
																INNER JOIN
																	Item IInner
																ON
																	OIInner.numItemCode = IInner.numItemCode
																OUTER APPLY
																(
																	SELECT
																		SUM(OpportunityBizDocItems.numUnitHour) AS PickPacked
																	FROM
																		OpportunityBizDocs
																	INNER JOIN
																		OpportunityBizDocItems 
																	ON
																		OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																	WHERE
																		OpportunityBizDocs.numOppId = OMInner.numOppId
																		AND ISNULL(OpportunityBizDocs.numBizDocId,0) = ',(SELECT ISNULL(numDefaultSalesShippingDoc,0) FROM Domain WHERE numDomainId=@numDomainID),'
																		AND OpportunityBizDocItems.numOppItemID = OIInner.numoppitemtCode
																) AS TempFulFilled
																WHERE
																	OMInner.numDomainId = ',@numDomainID,'
																	AND OMInner.tintOppType = 1
																	AND OMInner.tintOppStatus = 1
																	AND UPPER(IInner.charItemType) = ''P''
																	AND ISNULL(OMInner.tintshipped,0) = 0
																	AND ISNULL(OIInner.bitDropShip,0) = 0
																	AND ISNULL(OIInner.numUnitHour,0) <> ISNULL(TempFulFilled.PickPacked,0)',') ')
	END
	ELSE IF ISNULL(@tintDashboardReminderType,0) = 12
	BEGIN
		SET @strSql = CONCAT(@strSql,' AND Opp.numOppID IN (','SELECT DISTINCT
																	OMInner.numOppId
																FROM
																	OpportunityMaster OMInner
																INNER JOIN
																	OpportunityItems OIInner
																ON
																	OIInner.numOppId = OMInner.numOppId
																INNER JOIN
																	Item IInner
																ON
																	OIInner.numItemCode = IInner.numItemCode
																OUTER APPLY
																(
																	SELECT
																		SUM(OpportunityBizDocItems.numUnitHour) AS FulFilledQty
																	FROM
																		OpportunityBizDocs
																	INNER JOIN
																		OpportunityBizDocItems 
																	ON
																		OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																	WHERE
																		OpportunityBizDocs.numOppId = OMInner.numOppId
																		AND ISNULL(OpportunityBizDocs.numBizDocId,0) = 296
																		AND ISNULL(OpportunityBizDocs.bitFulFilled,0) = 1
																		AND OpportunityBizDocItems.numOppItemID = OIInner.numoppitemtCode
																) AS TempFulFilled
																WHERE
																	OMInner.numDomainId = ',@numDomainID,'
																	AND OMInner.tintOppType = 1
																	AND OMInner.tintOppStatus = 1
																	AND ISNULL(OMInner.tintshipped,0) = 0
																	AND UPPER(IInner.charItemType) = ''P''
																	AND ISNULL(OIInner.bitDropShip,0) = 0
																	AND ISNULL(OIInner.numUnitHour,0) <> ISNULL(TempFulFilled.FulFilledQty,0)',') ')
	END
	ELSE IF ISNULL(@tintDashboardReminderType,0) = 13
	BEGIN
		SET @strSql = CONCAT(@strSql,' AND Opp.numOppID IN (','SELECT DISTINCT
																	OMInner.numOppId
																FROM
																	OpportunityMaster OMInner
																INNER JOIN
																	OpportunityItems OIInner
																ON
																	OIInner.numOppId = OMInner.numOppID
																INNER JOIN
																	Item IInner
																ON
																	OIInner.numItemCode = IInner.numItemCode
																OUTER APPLY
																(
																	SELECT
																		SUM(OpportunityBizDocItems.numUnitHour) AS InvoicedQty
																	FROM
																		OpportunityBizDocs
																	INNER JOIN
																		OpportunityBizDocItems 
																	ON
																		OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																	WHERE
																		OpportunityBizDocs.numOppId = OMInner.numOppID
																		AND OpportunityBizDocItems.numOppItemID = OIInner.numoppitemtCode
																		AND OpportunityBizDocs.numBizDocId = ISNULL((SELECT TOP 1 numAuthoritativeSales FROM AuthoritativeBizDocs WHERE numDomainId=',@numDomainID,'),287)
																) AS TempInvoice
																WHERE
																	OMInner.numDomainId = ',@numDomainID,'
																	AND OMInner.tintOppType = 1
																	AND OMInner.tintOppStatus  = 1
																	AND ISNULL(OIInner.numUnitHour,0) <> ISNULL(TempInvoice.InvoicedQty,0)',') ')
	END
	ELSE IF ISNULL(@tintDashboardReminderType,0) = 14
	BEGIN
		DECLARE @TEMPOppID TABLE
		(
			numOppID NUMERIC(18,0)
		)

		INSERT INTO @TEMPOppID
		(
			numOppID
		)
		SELECT DISTINCT
			OM.numOppID
		FROM
			OpportunityMaster OM
		INNER JOIN
			OpportunityItems OI
		ON
			OM.numOppId = OI.numOppId
		WHERE   
			OM.numDomainId = @numDomainId
			AND OM.tintOppType = 1
			AND OM.tintOppStatus=1
			AND ISNULL(OI.bitDropShip, 0) = 1
			AND 1 = (CASE WHEN (SELECT COUNT(*) FROM OpportunityLinking OMInner INNER JOIN OpportunityItems OIInner ON OMInner.numChildOppID=OIInner.numOppId WHERE OMInner.numParentOppID = OM.numOppID AND OIInner.numItemCode=OI.numItemCode AND ISNULL(OIInner.numWarehouseItmsID,0)=ISNULL(OI.numWarehouseItmsID,0)) > 0 THEN 0 ELSE 1 END)


		If ISNULL((SELECT bitReOrderPoint FROM Domain WHERE numDomainId=@numDomainID),0) = 1
		BEGIN
			INSERT INTO @TEMPOppID
			(
				numOppID
			)
			SELECT DISTINCT
				OM.numOppID
			FROM
				OpportunityMaster OM
			INNER JOIN
				OpportunityItems OI
			ON
				OI.numOppId = OM.numOppId
			INNER JOIN 
				Item I 
			ON 
				I.numItemCode = OI.numItemCode
			INNER JOIN 
				WarehouseItems WI 
			ON 
				OI.numWarehouseItmsID = WI.numWareHouseItemID
			WHERE   
				OM.numDomainId = @numDomainId
				AND ISNULL((SELECT bitReOrderPoint FROM Domain WHERE numDomainId=@numDomainID),0) = 1
				AND (ISNULL(WI.numOnHand,0) + ISNULL(WI.numOnOrder,0) <= ISNULL(WI.numReOrder,0))
				AND OM.tintOppType = 1
				AND OM.tintOppStatus=1
				AND ISNULL(I.bitAssembly, 0) = 0
				AND ISNULL(I.bitKitParent, 0) = 0
				AND ISNULL(OI.bitDropship,0) = 0
				AND OM.numOppId NOT IN (SELECT numOppID FROM @TEMPOppID)

			IF (SELECT COUNT(*) FROM @TEMPOppID) > 0
			BEGIN
				DECLARE @tmpIds VARCHAR(MAX) = ''
				SELECT @tmpIds = CONCAT(@tmpIds,numOppID,', ') FROM @TEMPOppID
				SET @strSql = CONCAT(@strSql,' AND Opp.numOppID IN (',SUBSTRING(@tmpIds, 0, LEN(@tmpIds)),') ')
			END
			ELSE
			BEGIN
				SET @strSql = CONCAT(@strSql,' AND Opp.numOppID IN (0) ')
			END
		END
	END
	ELSE IF ISNULL(@tintDashboardReminderType,0) = 15
	BEGIN
		SET @strSql = CONCAT(@strSql,' AND Opp.numOppID IN (','SELECT
																	OMInner.numOppID
																FROM
																	OpportunityMaster OMInner
																WHERE
																	numDomainId=',@numDomainID,'
																	AND tintOppType=1
																	AND tintOppStatus=1
																	AND ISNULL(OMInner.tintshipped,0) = 0
																	AND dtReleaseDate IS NOT NULL
																	AND dtReleaseDate < DateAdd(minute,',@ClientTimeZoneOffset * -1,',GETUTCDATE())
																	AND (SELECT 
																			COUNT(*) 
																		FROM 
																		(
																			SELECT
																				OIInner.numoppitemtCode,
																				ISNULL(OIInner.numUnitHour,0) AS OrderedQty,
																				ISNULL(TempFulFilled.FulFilledQty,0) AS FulFilledQty
																			FROM
																				OpportunityItems OIInner
																			INNER JOIN
																				Item IInner
																			ON
																				OIInner.numItemCode = IInner.numItemCode
																			OUTER APPLY
																			(
																				SELECT
																					SUM(OpportunityBizDocItems.numUnitHour) AS FulFilledQty
																				FROM
																					OpportunityBizDocs
																				INNER JOIN
																					OpportunityBizDocItems 
																				ON
																					OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																				WHERE
																					OpportunityBizDocs.numOppId = OMInner.numOppId
																					AND ISNULL(OpportunityBizDocs.numBizDocId,0) = 296
																					AND ISNULL(OpportunityBizDocs.bitFulFilled,0) = 1
																					AND OpportunityBizDocItems.numOppItemID = OIInner.numoppitemtCode
																			) AS TempFulFilled
																			WHERE
																				OIInner.numOppID = OMInner.numOppID
																				AND UPPER(IInner.charItemType) = ''P''
																				AND ISNULL(OIInner.bitDropShip,0) = 0
																		) X
																		WHERE
																			X.OrderedQty <> X.FulFilledQty) > 0',') ')
	END



	DECLARE  @firstRec  AS INTEGER
	DECLARE  @lastRec  AS INTEGER
  
	SET @firstRec = (@CurrentPage - 1) * @PageSize
	SET @lastRec = (@CurrentPage * @PageSize + 1)



	-- EXECUTE FINAL QUERY
	DECLARE @strFinal AS NVARCHAR(MAX)
	SET @strFinal = CONCAT('SELECT ROW_NUMBER() OVER(ORDER BY ',@columnName,' ',@columnSortOrder,') ID, Opp.numOppID INTO #temp2', REPLACE(@strSql,'##PLACEHOLDER##',''),'; SELECT ID,',@strColumns,' INTO #tempTable',REPLACE(@strSql,'##PLACEHOLDER##',' JOIN #temp2 tblAllOrders ON Opp.numOppID = tblAllOrders.numOppID '),' AND tblAllOrders.ID > ',@firstRec,' and tblAllOrders.ID <',@lastRec,' ORDER BY ID; SELECT * FROM  #tempTable; DROP TABLE #tempTable; SELECT @TotalRecords = COUNT(*) FROM #temp2; DROP TABLE #temp2;')
	--SET @strFinal = CONCAT('SELECT ROW_NUMBER() OVER(ORDER BY ',@columnName,' ',@columnSortOrder,') ID, ',@strColumns,' INTO #tempTable',@strSql,'; SELECT * FROM #tempTable WHERE ID > ',@firstRec,' and ID <',@lastRec,' ORDER BY ID; SELECT @TotalRecords = COUNT(*) FROM #tempTable; DROP TABLE #tempTable;')

	PRINT CAST(@strFinal AS NTEXT)
	EXEC sp_executesql @strFinal, N'@TotalRecords INT OUT', @TotRecs OUT

	SELECT * FROM #tempForm

	DROP TABLE #tempForm

	DROP TABLE #tempColorScheme
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p' AND NAME ='USP_GetDomainDetailsForPriceMargin')
DROP PROCEDURE USP_GetDomainDetailsForPriceMargin
GO
CREATE PROCEDURE [dbo].[USP_GetDomainDetailsForPriceMargin]                                          
	@numDomainID AS NUMERIC(9) = 0,
	@numListItemID AS NUMERIC(9) = 0                                           
AS  

DECLARE @listIds VARCHAR(MAX)
SELECT @listIds = COALESCE(@listIds+',' ,'') + CAST(numUserID AS VARCHAR(100)) FROM UnitPriceApprover WHERE numDomainID = @numDomainID

IF @numListItemID > 0
BEGIN
	SELECT     
		ISNULL(AP.numAbovePercent,0) AS numAbovePercent,
		ISNULL(AP.numBelowPercent,0) AS numBelowPercent,
		ISNULL(AP.numBelowPriceField,0) AS numBelowPriceField,
		ISNULL(AP.bitCostApproval,0) bitCostApproval,
		ISNULL(AP.bitListPriceApproval,0) bitListPriceApproval,
		ISNULL(AP.bitMarginPriceViolated,0) bitMarginPriceViolated,
		ISNULL(numListItemID,0) AS numListItemID,
		@listIds AS vcUnitPriceApprover
	FROM Domain D  
		LEFT JOIN ApprovalProcessItemsClassification AP ON AP.numDomainID = D.numDomainID
	WHERE (D.numDomainID=@numDomainID OR @numDomainID=-1)
		AND numListItemID = @numListItemID 
END
ELSE
BEGIN
	SELECT     
		ISNULL(AP.numAbovePercent,0) AS numAbovePercent,
		ISNULL(AP.numBelowPercent,0) AS numBelowPercent,
		ISNULL(AP.numBelowPriceField,0) AS numBelowPriceField,
		ISNULL(AP.bitCostApproval,0) bitCostApproval,
		ISNULL(AP.bitListPriceApproval,0) bitListPriceApproval,
		ISNULL(AP.bitMarginPriceViolated,0) bitMarginPriceViolated,
		ISNULL(numListItemID,0) AS numListItemID,
		@listIds AS vcUnitPriceApprover
	FROM Domain D  
		LEFT JOIN ApprovalProcessItemsClassification AP ON AP.numDomainID = D.numDomainID
	WHERE (D.numDomainID=@numDomainID OR @numDomainID=-1)
END

/****** Object:  StoredProcedure [dbo].[usp_GetDuplicateCompany]    Script Date: 07/26/2008 16:17:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getdueamount')
DROP PROCEDURE usp_getdueamount
GO
CREATE PROCEDURE [dbo].[USP_GetDueAmount]                      
@numDivisionID numeric(9)=0                      
as                      
declare @Amt as DECIMAL(20,5);set @Amt=0                        
declare @Paid as DECIMAL(20,5);set @Paid=0      
                 
declare @AmountDueSO as DECIMAL(20,5);set @AmountDueSO=0                    
declare @AmountPastDueSO as DECIMAL(20,5);set @AmountPastDueSO=0 

declare @AmountDuePO as DECIMAL(20,5);set @AmountDuePO=0                    
declare @AmountPastDuePO as DECIMAL(20,5);set @AmountPastDuePO=0 
                   
declare @CompanyCredit as DECIMAL(20,5);set @CompanyCredit=0   
          
declare @PCreditMemo as DECIMAL(20,5);set @PCreditMemo=0            
declare @SCreditMemo as DECIMAL(20,5);set @SCreditMemo=0            

DECLARE @UTCtime AS datetime	       
SET	@UTCtime = dbo.[GetUTCDateWithoutTime]()

--Sales Order Total Amount Due                
select @Amt=isnull(sum(OppBD.[monDealAmount]),0) from OpportunityMaster  Opp
join  OpportunityBizDocs OppBD
on    OppBD.numOppID=Opp.numOppID               
where bitAuthoritativeBizDocs=1 and numDivisionID=@numDivisionID and tintOppStatus=1   and tintOpptype=1              
               
select @Paid=isnull(sum(monAmountPaid),0) from OpportunityMaster  Opp
join  OpportunityBizDocs OppBD
on    OppBD.numOppID=Opp.numOppID               
where bitAuthoritativeBizDocs=1 and numDivisionID=@numDivisionID and tintOppStatus=1   and tintOpptype=1               
                
set @AmountDueSO=@Amt-@Paid                
         
--Sales Order Total Amount Past Due                
select @Amt=isnull(sum(OppBD.[monDealAmount]),0) from OpportunityMaster   Opp
join  OpportunityBizDocs OppBD
on    OppBD.numOppID=Opp.numOppID               
where  bitAuthoritativeBizDocs=1 and numDivisionID=@numDivisionID and tintOppStatus=1   and tintOpptype=1              
AND DATEADD(day,CASE WHEN Opp.bitBillingTerms = 1 
					 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(Opp.intBillingDays,0)),0))  
					 ELSE 0 
				END, OppBD.[dtFromDate]) < @UTCtime
--AND DATEADD(day,CASE WHEN Opp.bitBillingTerms = 1 
--					 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(Opp.intBillingDays,0)),0))  
--					 ELSE 0 
--				END, OppBD.[dtFromDate]) < @UTCtime				

select @Paid=isnull(SUM(OppBD.monAmountPaid),0) from OpportunityMaster  Opp
join  OpportunityBizDocs OppBD on    OppBD.numOppID=Opp.numOppID               
where  OppBD.bitAuthoritativeBizDocs=1 and Opp.numDivisionID=@numDivisionID and tintOppStatus=1  and tintOpptype=1               
AND DATEADD(day,CASE WHEN Opp.bitBillingTerms = 1 
					 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(Opp.intBillingDays,0)),0)) 
					 ELSE 0 
				END, OppBD.[dtFromDate]) <@UTCtime
--AND DATEADD(day,CASE WHEN Opp.bitBillingTerms = 1 
--					 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(Opp.intBillingDays,0)),0)) 
--					 ELSE 0 
--				END, OppBD.[dtFromDate]) <@UTCtime				

set @AmountPastDueSO=@Amt-@Paid                    
                    
   
--Purchase Order Total Amount Due                
select @Amt=isnull(sum(OppBD.[monDealAmount]),0) from OpportunityMaster  Opp
join  OpportunityBizDocs OppBD
on    OppBD.numOppID=Opp.numOppID               
where bitAuthoritativeBizDocs=1 and numDivisionID=@numDivisionID and tintOppStatus=1   and tintOpptype=2              
               
select @Paid=isnull(sum(monAmountPaid),0) from OpportunityMaster  Opp
join  OpportunityBizDocs OppBD
on    OppBD.numOppID=Opp.numOppID               
where bitAuthoritativeBizDocs=1 and numDivisionID=@numDivisionID and tintOppStatus=1   and tintOpptype=2              
                
set @AmountDuePO=@Amt-@Paid                
         
--Purchase Order Total Amount Past Due                
select @Amt=isnull(sum(OppBD.[monDealAmount]),0) from OpportunityMaster   Opp
join  OpportunityBizDocs OppBD
on    OppBD.numOppID=Opp.numOppID               
where  bitAuthoritativeBizDocs=1 and numDivisionID=@numDivisionID and tintOppStatus=1   and tintOpptype=2              
AND DATEADD(day,CASE WHEN Opp.bitBillingTerms = 1 
					 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(Opp.intBillingDays,0)),0))  
					 ELSE 0 
				END, OppBD.[dtFromDate]) <@UTCtime
--AND DATEADD(day,CASE WHEN Opp.bitBillingTerms = 1 
--					 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(Opp.intBillingDays,0)),0))  
--					 ELSE 0 
--				END, OppBD.[dtFromDate]) <@UTCtime
				

select @Paid=isnull(SUM(OppBD.monAmountPaid),0) from OpportunityMaster  Opp
join  OpportunityBizDocs OppBD on    OppBD.numOppID=Opp.numOppID               
where  OppBD.bitAuthoritativeBizDocs=1 and Opp.numDivisionID=@numDivisionID and tintOppStatus=1  and tintOpptype=2               
AND DATEADD(day,CASE WHEN Opp.bitBillingTerms = 1 
					 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(Opp.intBillingDays,0)),0)) 
					 ELSE 0 
				END, OppBD.[dtFromDate]) <@UTCtime
--AND DATEADD(day,CASE WHEN Opp.bitBillingTerms = 1 
--					 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(Opp.intBillingDays,0)),0)) 
--					 ELSE 0 
--				END, OppBD.[dtFromDate]) <@UTCtime

set @AmountPastDuePO=@Amt-@Paid  


--Company Cresit Limit                    
select @CompanyCredit=convert(DECIMAL(20,5),case when isnumeric(dbo.fn_GetListItemName(numCompanyCredit))=1 then REPLACE(dbo.fn_GetListItemName(numCompanyCredit),',','') else 0 end)
	 from companyinfo C                    
join divisionmaster d                    
on d.numCompanyID=C.numCompanyID                    
where d.numdivisionid=@numDivisionID                    
                    
 
--Purchase Credit Memo
select @PCreditMemo=ISNULL(BPH.monPaymentAmount,0) - ISNULL (BPH.monAppliedAmount,0)
	 from BillPaymentHeader BPH          
where BPH.numdivisionid=@numDivisionID AND ISNULL(numReturnHeaderID,0)>0                  

--Sales Credit Memo
select @SCreditMemo=ISNULL(DM.monDepositAmount,0) - ISNULL (DM.monAppliedAmount,0)
	 from dbo.DepositMaster DM          
where DM.numdivisionid=@numDivisionID AND tintDepositePage=3 AND ISNULL(numReturnHeaderID,0)>0  

DECLARE @vcShippersAccountNo AS VARCHAR(100)
DECLARE @numDefaultShippingServiceID AS NUMERIC(18,0)
DECLARE @intShippingCompany AS NUMERIC(18,0)
DECLARE @numPartnerSource NUMERIC(18,0)
SELECT @vcShippersAccountNo = ISNULL(vcShippersAccountNo,''), @numDefaultShippingServiceID=ISNULL(numDefaultShippingServiceID,0), @intShippingCompany=ISNULL(intShippingCompany,0), @numPartnerSource=ISNULL(numPartenerSource,0) FROM dbo.DivisionMaster WHERE numDivisionID = @numDivisionID

select isnull(@AmountDueSO,0) as AmountDueSO,isnull(@AmountPastDueSO,0) as AmountPastDueSO, 
isnull(@AmountDuePO,0) as AmountDuePO,isnull(@AmountPastDuePO,0) as AmountPastDuePO,                      
ISNULL(@CompanyCredit,0) - (isnull(@AmountDueSO,0) + isnull(@AmountDuePO,0)) as RemainingCredit,
isnull(@PCreditMemo,0) as PCreditMemo,
isnull(@SCreditMemo,0) as SCreditMemo,
ISNULL(@CompanyCredit,0) AS CreditLimit,
@vcShippersAccountNo AS vcShippersAccountNo,
@numDefaultShippingServiceID AS numDefaultShippingServiceID,
@intShippingCompany AS intShippingCompany,
@numPartnerSource AS numPartnerSource

GO
GO
/****** Object:  StoredProcedure [dbo].[USP_GetECampaignDetailsReport]    Script Date: 06/04/2009 16:23:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Pratik  Vasani
-- Create date: 13/DEC/2008
-- Description:	This Stored proc gets the table for display in Ecampaign report
-- =============================================

--declare @p4 int
--set @p4=0
--exec USP_GetECampaignDetailsReport @numDomainId=1,@CurrentPage=1,@PageSize=20,@TotRecs=@p4 output
--select @p4
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getecampaigndetailsreport')
DROP PROCEDURE usp_getecampaigndetailsreport
GO
CREATE PROCEDURE [dbo].[USP_GetECampaignDetailsReport]
               @numDomainId NUMERIC(9,0)  = 0,
               @CurrentPage INT,
               @PageSize    INT,
               @TotRecs     INT  OUTPUT,
			   @numECampaingID NUMERIC(18,0) = 0
AS
  BEGIN
    CREATE TABLE #tempTable (
      ID INT IDENTITY PRIMARY KEY,
	  numECampaignID NUMERIC(9,0),
	  vcECampName VARCHAR(200),
	  numConEmailCampID NUMERIC(9,0),
      numContactID NUMERIC(9,0),
      CompanyName VARCHAR(100),
      ContactName VARCHAR(100),
	  vcEmail VARCHAR(100),
      [Status] VARCHAR(20),
	  Template VARCHAR(500),
	  dtSentDate VARCHAR(80),
	  SendStatus VARCHAR(20),
	  dtNextStage VARCHAR(80),
	  Stage INT,
	  NoOfStagesCompleted INT,
	  NoOfEmailSent INT,
	  NoOfEmailOpened INT
      )
    INSERT INTO #tempTable
    
   SELECT
		ECampaign.numECampaignID,
		ECampaign.vcECampName,
		ConECampaign.numConEmailCampID,
		ConECampaign.numContactID,
		dbo.GetCompanyNameFromContactID(ConECampaign.numContactID,ECampaign.numDomainId) AS CompanyName,
		dbo.fn_GetContactName(ConECampaign.numContactID) AS ContactName,
		ISNULL(AdditionalContactsInformation.vcEmail,'') AS vcEmail,
		(CASE 
			WHEN ISNULL(ECampaign.bitDeleted,0) = 0 
			THEN CASE WHEN ISNULL(ConECampaign.bitEngaged,0) = 0 THEN 'Disengaged' ELSE 'Engaged' END
			ELSE 'Campaign Deleted'
		END) AS Status,
		ISNULL(CampaignExecutedDetail.Template,''),
		ISNULL(dbo.FormatedDateFromDate(CampaignExecutedDetail.bintSentON,@numDomainID),''),
		(CASE 
			WHEN ISNULL(CampaignExecutedDetail.bitSend,0)=1 AND ISNULL(CampaignExecutedDetail.tintDeliveryStatus,0)=0 THEN 'Failed'
			WHEN ISNULL(CampaignExecutedDetail.bitSend,0)=1 AND ISNULL(CampaignExecutedDetail.tintDeliveryStatus,0)=1 THEN 'Success'
			ELSE 'Pending'
		END) AS SendStatus
		,
		 dbo.FormatedDateFromDate(DATEADD(MINUTE,ISNULL(ECampaign.fltTimeZone,0) * 60,CampaignNextExecutationDetail.dtExecutionDate),@numDomainID),
		 ISNULL((SELECT COUNT(*) FROM ConECampaignDTL WHERE numConECampID = ConECampaign.numConEmailCampID),0) AS NoOfStages,
		 ISNULL((SELECT COUNT(*) FROM ConECampaignDTL WHERE numConECampID = ConECampaign.numConEmailCampID AND ISNULL(bitSend,0)=1),0) AS NoOfStagesCompleted,
		 (SELECT COUNT(*) FROM ConECampaignDTL INNER JOIN ECampaignDTLs ON ConECampaignDTL.numECampDTLID = ECampaignDTLs.numECampDTLId WHERE ECampaignDTLs.numEmailTemplate IS NOT NULL AND numConECampID = ConECampaign.numECampaignID AND bitSend = 1 AND ISNULL(tintDeliveryStatus,0) = 1) AS NoOfEmailSent,
		 (SELECT COUNT(*) FROM ConECampaignDTL INNER JOIN ECampaignDTLs ON ConECampaignDTL.numECampDTLID = ECampaignDTLs.numECampDTLId WHERE ECampaignDTLs.numEmailTemplate IS NOT NULL AND numConECampID = ConECampaign.numECampaignID AND bitSend = 1 AND ISNULL(tintDeliveryStatus,0) = 1 AND ISNULL(bitEmailRead,0) = 1) AS NoOfEmailOpened
	FROM
		ConECampaign
	INNER JOIN
		ECampaign
	ON
		ConECampaign.numECampaignID = ECampaign.numECampaignID
	LEFT JOIN
		AdditionalContactsInformation
	ON
		ConECampaign.numContactID = AdditionalContactsInformation.numContactId
	OUTER APPLY
		(
			SELECT 
				TOP 1
				(CASE 
					WHEN ECampaignDTLs.numEmailTemplate IS NOT NULL THEN GenericDocuments.vcDocName
					WHEN ECampaignDTLs.numActionItemTemplate IS NOT NULL THEN tblActionItemData.TemplateName
					ELSE ''
				END) AS Template,
				ConECampaignDTL.bitSend,
				ConECampaignDTL.bintSentON,
				ConECampaignDTL.tintDeliveryStatus
			FROM 
				ConECampaignDTL 
			INNER JOIN 
				ECampaignDTLs 
			ON 
				ConECampaignDTL.numECampDTLID = ECampaignDTLs.numECampDTLId
			LEFT JOIN 
				GenericDocuments 
			ON 
				GenericDocuments.numGenericDocID = ECampaignDTLs.numEmailTemplate
			LEFT JOIN 
				tblActionItemData
			ON
				tblActionItemData.RowID = ECampaignDTLs.numActionItemTemplate
			WHERE
				ECampaignDTLs.numECampID = ECampaign.numECampaignID AND
				ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID AND
				ConECampaignDTL.bitSend IS NOT NULL
			ORDER BY
				numConECampDTLID DESC
		) CampaignExecutedDetail
	OUTER APPLY
		(
			SELECT 
				TOP 1
				ConECampaignDTL.dtExecutionDate
			FROM 
				ConECampaignDTL 
			INNER JOIN 
				ECampaignDTLs 
			ON 
				ConECampaignDTL.numECampDTLID = ECampaignDTLs.numECampDTLId
			LEFT JOIN 
				GenericDocuments 
			ON 
				ECampaignDTLs.numEmailTemplate = GenericDocuments.numGenericDocID
			LEFT JOIN 
				tblActionItemData
			ON
				ECampaignDTLs.numActionItemTemplate = tblActionItemData.RowID
			WHERE
				ECampaignDTLs.numECampID = ECampaign.numECampaignID AND
				ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID AND
				ConECampaignDTL.bitSend IS NULL
			ORDER BY
				numConECampDTLID
		) CampaignNextExecutationDetail
	WHERE
		ECampaign.numDomainID = @numDomainId AND
		(ECampaign.numECampaignID = @numECampaingID OR ISNULL(@numECampaingID,0) = 0)
    
   
            
    DECLARE  @firstRec  AS INTEGER
    DECLARE  @lastRec  AS INTEGER
    SET @firstRec = (@CurrentPage
                       - 1)
                      * @PageSize
    SET @lastRec = (@CurrentPage
                      * @PageSize
                      + 1)
    SET @TotRecs = (SELECT COUNT(* )
                    FROM   #tempTable)
    SELECT   *
    FROM     #tempTable
    WHERE    ID > @firstRec
             AND ID < @lastRec
    ORDER BY id

    DROP TABLE #tempTable
    SELECT @TotRecs AS TotRecs
  END
/****** Object:  StoredProcedure [dbo].[USP_GetEmailCampaignMails]    Script Date: 06/04/2009 15:10:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetEmailCampaignMails')
DROP PROCEDURE USP_GetEmailCampaignMails
GO
CREATE PROCEDURE [dbo].[USP_GetEmailCampaignMails]
AS 
BEGIN
-- Organization Fields Which Needs To Be Replaced
DECLARE  @vcCompanyName  AS VARCHAR(200)
DECLARE  @OrgNoOfEmployee  AS VARCHAR(12)
DECLARE  @OrgShippingAddress  AS VARCHAR(1000)
DECLARE  @Signature  AS VARCHAR(1000)


-- Contact Fields Which Needs To Be Replaced.
DECLARE @ContactAge AS VARCHAR(3)
DECLARE @ContactAssistantEmail AS VARCHAR(100)
DECLARE @AssistantFirstName AS VARCHAR(100)
DECLARE @AssistantLastName AS VARCHAR(100)
DECLARE @ContactAssistantPhone AS VARCHAR(20)
DECLARE @ContactAssistantPhoneExt AS VARCHAR(10)
DECLARE @ContactCategory AS VARCHAR(100)
DECLARE @ContactCell AS VARCHAR(20)
DECLARE @ContactDepartment AS VARCHAR(100)
DECLARE @ContactDripCampaign AS VARCHAR(500)
DECLARE @ContactEmail AS VARCHAR(100)
DECLARE @ContactFax AS VARCHAR(100)
DECLARE @ContactFirstName AS VARCHAR(100)
DECLARE @ContactGender AS VARCHAR(10)
DECLARE @ContactHomePhone AS VARCHAR(20)
DECLARE @ContactLastName AS VARCHAR(100)
DECLARE @ContactManager AS VARCHAR(100)
DECLARE @ContactPhone AS VARCHAR(20)	
DECLARE @ContactPhoneExt AS VARCHAR(10)
DECLARE @ContactPosition AS VARCHAR(100)
DECLARE @ContactPrimaryAddress AS VARCHAR(1000)
DECLARE @ContactStatus AS VARCHAR(100)
DECLARE @ContactTeam AS VARCHAR(100)
DECLARE @ContactTitle AS VARCHAR(100)
DECLARE @ContactType AS VARCHAR(100)
      
DECLARE  @numConEmailCampID  AS NUMERIC(9)
DECLARE  @intStartDate  AS DATETIME


DECLARE  @numConECampDTLID  AS NUMERIC(9)
declare @numContactID as numeric(9)   
declare @numFromContactID as numeric(9)   
declare @numEmailTemplate as varchar(15)
declare @numDomainId as numeric(9)
DECLARE  @tintFromField  AS TINYINT
DECLARE @numCampaignOwner NUMERIC(18,0)

declare @vcSubject as varchar(1000)
declare @vcBody as varchar(8000)              
declare @vcFormattedBody as varchar(8000)

DECLARE @LastDateTime DATETIME
DECLARE @CurrentDateTime DATETIME

SELECT @LastDateTime = [dtLastDateTimeECampaign] FROM [WindowsServiceHistory]
SET @CurrentDateTime = DATEADD(minute, DATEpart(minute,GETUTCDATE()) ,DATEADD(hour,DATEpart(hour,GETUTCDATE()),dbo.[GetUTCDateWithoutTime]()))
PRINT @LastDateTime
PRINT @CurrentDateTime


Create table #tempTableSendApplicationMail
(  vcTo varchar(2000),              
   vcSubject varchar(2000),              
   vcBody varchar(Max),              
   vcCC varchar(2000),      
   numDomainID numeric(9),
   numConECampDTLID NUMERIC(9) NULL,
   tintFromField TINYINT,
   numContactID NUMERIC,
   numFromContactID NUMERIC,
   numCampaignOwner NUMERIC(18,0)
 )                

-- EC.fltTimeZone-2 is used to fetch all email which are not sent from past 2 housrs of last execution date 
-- in case of amazon ses service is down and not able to send email in first try
SELECT   TOP 1 @numContactID = C.numContactID,
               @numConECampDTLID = numConECampDTLID,
               @numConEmailCampID = numConEmailCampID,
               @intStartDate = intStartDate,
               @numEmailTemplate = numEmailTemplate,
               @numDomainId = EC.numDomainID,
               @tintFromField = EC.[tintFromField],
			   @numFromContactID = numFromContactID,
			   @numCampaignOwner = C.numRecOwner
FROM     ConECampaign C
         JOIN ConECampaignDTL DTL
           ON numConEmailCampID = numConECampID
         JOIN ECampaignDTLs E
           ON DTL.numECampDTLId = E.numECampDTLID
         JOIN [ECampaign] EC ON E.[numECampID] = EC.[numECampaignID] 
WHERE    bitSend IS NULL 
         AND bitengaged = 1
         AND numEmailTemplate > 0 
		 AND ISNULL(EC.bitDeleted,0) = 0
		 -- When campaign is first time assigned to contact it may happen that it is assigned after service already executed 
		 -- so first email will be send immediately and after that service will compare execution datetime and current datetime
         AND ((DTL.dtExecutionDate BETWEEN DATEADD(HOUR,-2,@LastDateTime) AND @CurrentDateTime) OR
			(SELECT COUNT(*) FROM ConECampaignDTL WHERE numConECampID = C.numConEmailCampID AND ISNULL(bitSend,0) = 1) = 0)
          
ORDER BY numConEmailCampID
PRINT '@numConEmailCampID='
PRINT @numConEmailCampID



WHILE @numConEmailCampID > 0
  BEGIN
	PRINT 'inside @numConEmailCampID='
	PRINT @numConEmailCampID

    SELECT 
		   @ContactFirstName = ISNULL(A.vcFirstName,''),
           @ContactLastName = ISNULL(A.vcLastName,''),
           @vcCompanyName = ISNULL(C.vcCompanyName,''),
		   @OrgNoOfEmployee = ISNULL(dbo.GetListIemName(C.numNoOfEmployeesId),''),
		   @OrgShippingAddress = vcCompanyName + ' ,<br>' + ISNULL(AD2.vcStreet,'') + ' <br>' + ISNULL(AD2.VcCity,'') + ' ,' 
								+ ISNULL(dbo.fn_GetState(AD2.numState),'') + ' ' + ISNULL(AD2.vcPostalCode,'') + ' <br>' + 
								ISNULL(dbo.fn_GetListItemName(AD2.numCountry),''),								   
		   @Signature = ISNULL((select U.txtSignature from UserMaster U where U.numUserDetailId=A.numRecOwner),''),
           @ContactPhone = ISNULL(A.numPhone,''),
           @ContactEmail = ISNULL(A.vcEmail,''),
		   @ContactAge = ISNULL(dbo.GetAge(A.bintDOB, GETUTCDATE()),''),
		   @ContactAssistantEmail = ISNULL(A.vcAsstEmail,''),
		   @AssistantFirstName = ISNULL(A.VcAsstFirstName,''),
		   @AssistantLastName = ISNULL(A.vcAsstLastName,''),
		   @ContactAssistantPhone = ISNULL(A.numAsstPhone,''),
		   @ContactAssistantPhoneExt= ISNULL(A.numAsstExtn,''),
		   @ContactCategory=ISNULL([dbo].[GetListIemName](A.vcCategory),''),
		   @ContactCell=ISNULL(A.numCell,''),
		   @ContactDepartment=ISNULL([dbo].[GetListIemName](A.vcDepartment),''),
		   @ContactDripCampaign=ISNULL((SELECT vcECampName FROM [ECampaign] WHERE numECampaignID = A.numECampaignID), ''),
		   @ContactFax=ISNULL(A.vcFax,''),
		   @ContactGender=(CASE WHEN A.charSex = 'M' THEN 'Male' WHEN A.charSex = 'F' THEN 'Female' ELSE '-' END),
		   @ContactHomePhone=ISNULL(A.numHomePhone,''),
		   @ContactManager=ISNULL(dbo.fn_GetContactName(A.numManagerId),''),
		   @ContactPhoneExt=ISNULL(A.numPhoneExtension,''),
		   @ContactPosition= ISNULL([dbo].[GetListIemName](A.vcPosition),''),
		   @ContactPrimaryAddress=ISNULL(dbo.getContactAddress(A.numContactID),''),
		   @ContactStatus=ISNULL([dbo].[GetListIemName](A.numEmpStatus),''),
		   @ContactTeam=ISNULL([dbo].[GetListIemName](A.numTeam),''),
		   @ContactTitle=ISNULL(A.vcTitle,''),
		   @ContactType=ISNULL(dbo.GetListIemName(A.numContactType),'')
    FROM   AdditionalContactsInformation A
           JOIN DivisionMaster D
             ON A.numDivisionId = D.numDivisionID
           JOIN CompanyInfo C
             ON D.numCompanyID = C.numCompanyId
		   LEFT JOIN 
				dbo.AddressDetails AD2 
			ON 
				AD2.numDomainID=D.numDomainID 
				AND AD2.numRecordID= D.numDivisionID 
				AND AD2.tintAddressOf=2 
				AND AD2.tintAddressType=2 
				AND AD2.bitIsPrimary=1
    WHERE  A.numContactId = @numContactID
    
    SELECT @vcSubject = vcSubject,
           @vcBody = vcDocDesc
    FROM   genericdocuments
    WHERE  numGenericDocID = @numEmailTemplate
    
	--Replace organization fields from body
    SET @vcFormattedBody = REPLACE(@vcBody,'##OrganizationName##',@vcCompanyName)
	SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##OrgNoOfEmployee##',@OrgNoOfEmployee)
	SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##OrgShippingAddress##',@OrgShippingAddress)
	SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##Signature##',@Signature)

	--Replace contact fields from body
    SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##ContactFirstName##',@ContactFirstName)
    SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##ContactLastName##',@ContactLastName)
    SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##ContactPhone##',@ContactPhone)
    SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##ContactEmail##',@ContactEmail)
	SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##ContactAge##',@ContactAge)
	SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##ContactAssistantEmail##',@ContactAssistantEmail)
	SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##AssistantFirstName##',@AssistantFirstName)
	SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##AssistantLastName##',@AssistantLastName)
	SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##ContactAssistantPhone##',@ContactAssistantPhone)
	SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##ContactAssistantPhoneExt##',@ContactAssistantPhoneExt)
	SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##ContactCategory##',@ContactCategory)
	SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##ContactCell##',@ContactCell)
	SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##ContactDepartment##',@ContactDepartment)
	SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##ContactDripCampaign##',@ContactDripCampaign)
	SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##ContactFax##',@ContactFax)
	SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##ContactGender##',@ContactGender)
	SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##ContactHomePhone##',@ContactHomePhone)
	SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##ContactManager##',@ContactManager)
	SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##ContactPhoneExt##',@ContactPhoneExt)
	SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##ContactPosition##',@ContactPosition)
	SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##ContactPrimaryAddress##',@ContactPrimaryAddress)
	SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##ContactStatus##',@ContactStatus)
	SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##ContactTeam##',@ContactTeam)
	SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##ContactTitle##',@ContactTitle)
	SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##ContactType##',@ContactType)
    SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##ContactOpt-OutLink##','')

    
   INSERT INTO #tempTableSendApplicationMail
   VALUES (     @ContactEmail,
                @vcSubject,
                @vcFormattedBody,
                '',
                @numDomainId,
                @numConECampDTLID,
                @tintFromField,
                @numContactID,
                @numFromContactID,@numCampaignOwner)
                

    
    SELECT   TOP 1 @numContactID = C.numContactID,
                   @numConECampDTLID = numConECampDTLID,
                   @numConEmailCampID = numConEmailCampID,
                   @intStartDate = intStartDate,
                   @numEmailTemplate = numEmailTemplate,
                   @numDomainId = EC.numDomainID,
                   @tintFromField = EC.[tintFromField],
                   @numFromContactID = numFromContactID,
				   @numCampaignOwner = C.numRecOwner
    FROM     ConECampaign C
             JOIN ConECampaignDTL DTL
               ON numConEmailCampID = numConECampID
             JOIN ECampaignDTLs E
               ON DTL.numECampDTLId = E.numECampDTLID
             JOIN [ECampaign] EC ON E.[numECampID] = EC.[numECampaignID] 
--             JOIN AdditionalContactsInformation A
--               ON A.numContactID = C.numRecOwner
    WHERE    bitSend IS NULL 
             AND bitengaged = 1
             AND numConEmailCampID > @numConEmailCampID
			 AND numEmailTemplate > 0 
			 AND ISNULL(EC.bitDeleted,0) = 0
			-- When campaign is first time assigned to contact it may happen that it is assigned after service already executed 
		 -- so first email will be send immediately and after that service will compare execution datetime and current datetime
         AND ((DTL.dtExecutionDate BETWEEN DATEADD(HOUR,-2,@LastDateTime) AND @CurrentDateTime) OR
			(SELECT COUNT(*) FROM ConECampaignDTL WHERE numConECampID = C.numConEmailCampID AND ISNULL(bitSend,0) = 1) = 0)
    ORDER BY numConEmailCampID
    
    IF @@ROWCOUNT = 0
      SET @numConEmailCampID = 0
  END
  
  
  SELECT    X.*,
			ISNULL(EmailBroadcastConfiguration.vcAWSDomain,'') vcAWSDomain,
            ISNULL(EmailBroadcastConfiguration.vcAWSAccessKey,'') vcAWSAccessKey,
            ISNULL(EmailBroadcastConfiguration.vcAWSSecretKey,'') vcAWSSecretKey,
            CASE WHEN LEN(vcCampaignOwnerEmail) > 0 THEN vcCampaignOwnerEmail ELSE ISNULL(EmailBroadcastConfiguration.vcFrom,'') END vcFrom
  FROM      ( SELECT    A.[vcTo],
                        A.[vcSubject],
                        A.[vcBody],
                        A.[vcCC],
                        A.[numDomainID],
                        A.[numConECampDTLID],
                        A.[tintFromField],
                        A.[numContactID],
                        CASE [tintFromField]
                          WHEN 1 THEN --Record Owner of Company
                               DM.[numRecOwner]
                          WHEN 2 THEN --Assignee of Company
                               DM.[numAssignedTo]
                          WHEN 3 THEN numFromContactID
                          ELSE DM.[numRecOwner]
                        END AS numFromContactID,
						ISNULL(UM.vcEmailID,'') AS vcCampaignOwnerEmail
              FROM      #tempTableSendApplicationMail A
                        INNER JOIN [AdditionalContactsInformation] ACI ON ACI.[numContactId] = A.numContactID
						LEFT JOIN UserMaster UM ON A.numCampaignOwner = UM.numUserDetailId
                        INNER JOIN [DivisionMaster] DM ON DM.[numDivisionID] = ACI.[numDivisionId]
            ) X
            LEFT JOIN 
				EmailBroadcastConfiguration
			ON
				X.numDomainID = EmailBroadcastConfiguration.numDomainID
  
  DROP TABLE #tempTableSendApplicationMail

  UPDATE [WindowsServiceHistory] SET [dtLastDateTimeECampaign] = GETUTCDATE()
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetEnforcedMinOrderSubTotal')
DROP PROCEDURE USP_GetEnforcedMinOrderSubTotal
GO
CREATE PROCEDURE [dbo].[USP_GetEnforcedMinOrderSubTotal]  
	@numDomainID AS NUMERIC(9) = 0, 
	@numDivisionID NUMERIC(18,0),
	@numTotalAmount FLOAT          
	      
AS
BEGIN            
	DECLARE @numCompanyId NUMERIC(18,0), @vcProfile NUMERIC(18,0), @fltMinOrderAmount FLOAT = 0
	SELECT @numCompanyId = numCompanyId FROM DivisionMaster WHERE numDivisionID = @numDivisionID AND numDomainID = @numDomainID
	--SELECT @numCompanyId

	IF(SELECT COUNT(*) FROM CompanyInfo WHERE numCompanyId = @numCompanyId AND vcProfile <> 0 AND numDomainID = @numDomainID) > 0
	BEGIN
		SELECT @vcProfile = vcProfile FROM CompanyInfo WHERE numCompanyId = @numCompanyId AND numDomainID = @numDomainID
		--SELECT @vcProfile 
		--SELECT * FROM ListDetails WHERE numlistitemid = @vcProfile AND numDomainID = @numDomainID
		IF(SELECT COUNT(*) FROM ListDetails WHERE numlistitemid = @vcProfile AND numDomainID = @numDomainID) > 0
		BEGIN
			--SELECT * FROM ListDetails WHERE numlistitemid = @vcProfile AND numDomainID = @numDomainID
		
			SELECT @fltMinOrderAmount = fltMinOrderAmount 
			FROM SalesOrderRule 
			WHERE numlistitemid = @vcProfile
				AND bitEnforceMinOrderAmount <> 0
				AND numDomainID = @numDomainID

			IF @numTotalAmount >= @fltMinOrderAmount
			BEGIN
				SELECT 1 AS returnVal
				RETURN
			END
			ELSE
			BEGIN
				SELECT 0 AS returnVal, @fltMinOrderAmount AS MinOrderAmount
				RETURN
			END
		END
		ELSE
		BEGIN
			SELECT 1 AS returnVal
			RETURN
		END
	END
	ELSE
	BEGIN
		SELECT 1 AS returnVal
		RETURN
	END
END
GO
/****** Object:  StoredProcedure [dbo].[USP_GetItemsAndKits]    Script Date: 07/26/2008 16:17:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getitemsandkits')
DROP PROCEDURE usp_getitemsandkits
GO
CREATE PROCEDURE [dbo].[USP_GetItemsAndKits]        
@numDomainID as numeric(9)=0,
@strItem as varchar(10),
@type AS int,
@bitAssembly AS BIT = 0,
@numItemCode AS NUMERIC(18,0) = 0      
as   
IF @type=0 
BEGIN   
	--select numItemCode,vcItemName from Item where numDomainID =@numDomainID   and vcItemName like @strItem+'%'
	select numItemCode
	, vcItemName + '' + CASE WHEN txtItemDesc IS NULL OR txtItemDesc = '' THEN '' ELSE ' - ' + ISNULL(txtItemDesc,'') END AS vcItemName
	from Item where numDomainID =@numDomainID   and vcItemName like @strItem+'%'
END

ELSE IF @type=1 
BEGIN   
	IF (SELECT ISNULL(bitAssembly,0) FROM Item  WHERE numItemCode = @numItemCode) = 1
	BEGIN
		--ONLY NON INVENTORY, SERVICE AND INVENTORY ITEMS WITH PRIMARY VENDOR CAN BE ADDED TO ASSEMBLY
		SELECT 
			numItemCode,CONCAT(vcItemName,CASE WHEN ISNULL(dbo.fn_GetItemAttributes(@numDomainID,numItemCode),'') <> '' THEN CONCAT(' (',dbo.fn_GetItemAttributes(@numDomainID,numItemCode),')') ELSE '' END) vcItemName,bitKitParent,bitAssembly 
		FROM 
			Item 
		WHERE 
			Item.numDomainID =@numDomainID
			AND (vcItemName like @strItem+'%' OR vcSKU like @strItem+'%' OR vcModelID like @strItem+'%' OR numBarcodeID LIKE @strItem+'%')
			AND ISNULL(bitLotNo,0)<>1 
			AND ISNULL(bitSerialized,0)<>1 
			AND ISNULL(bitKitParent,0)<>1 /*and ISNULL(bitAssembly,0)<>1 and  */
			AND ISNULL(bitRental,0) <> 1
			AND ISNULL(bitAsset,0) <> 1
			AND 1=(CASE 
				   WHEN charitemtype='P' THEN
						CASE 
							WHEN ((SELECT COUNT(numWareHouseItemID) FROM WareHouseItems WHERE numDomainID=Item.numDomainID AND numItemID=Item.numItemCode) > 0 AND 
							     ((SELECT COUNT(numDivisionID) FROM DivisionMaster WHERE numDivisionID=Item.numVendorID) > 0 OR ISNULL(bitAssembly,0) = 1)) THEN 1 
							ELSE 
								0 
						END 
					ELSE 
						1 
					END) 
			AND numItemCode NOT IN (@numItemCode)
	END
	ELSE
	BEGIN
		--ONLY NON INVENTORY, SERVICE, INVENTORY ITEMS AND KIT ITEMS WITHOUT HAVING ANOTHER KIT AS SUB ITEM WITH PRIMARY VENDOR CAN BE ADDED TO ASSEMBLY

		SELECT 
			numItemCode,
			vcItemName,
			--vcItemName + '' + CASE WHEN txtItemDesc IS NULL OR txtItemDesc = '' THEN '' ELSE ' - ' + ISNULL(txtItemDesc,'') END AS vcItemName,
			bitKitParent,
			bitAssembly 
		FROM 
			Item 
		WHERE 
			numDomainID =@numDomainID
			AND vcItemName like @strItem+'%' 
			AND ISNULL(bitLotNo,0)<>1 and ISNULL(bitSerialized,0)<>1 
			AND numItemCode <> @numItemCode
			AND 1= (
					CASE 
						WHEN charitemtype='P' THEN
						CASE 
							WHEN (SELECT COUNT(numWareHouseItemID) FROM WareHouseItems WHERE numDomainID=Item.numDomainID AND numItemID=Item.numItemCode)>0 
							THEN 1 
							ELSE 0 
						END 
						ELSE 1 
					END)
			AND 1 = (CASE 
						WHEN Item.bitKitParent = 1 
						THEN 
							(
								CASE 
									WHEN (SELECT COUNT(*) FROM ItemDetails INNER JOIN Item I ON ItemDetails.numChildItemID = I.numItemCode WHERE numItemKitID=Item.numItemCode AND ISNULL(I.bitKitParent,0) = 1) > 0
									THEN 0
									ELSE 1
								END
							)
						ELSE 1 
					END)
	END
END				
GO
GO
/****** Object:  StoredProcedure [dbo].[USP_GetLeadList1]    Script Date: 09/08/2010 14:11:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--- Created By Anoop Jayaraj                                                                
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getleadlist1')
DROP PROCEDURE usp_getleadlist1
GO
CREATE PROCEDURE [dbo].[USP_GetLeadList1]                                                                          
 @numGrpID numeric,                                                                              
 @numUserCntID numeric,
 @tintUserRightType tinyint,
 @CustName varchar(100)='',                                                                              
 @FirstName varChar(100)= '',                                                                              
 @LastName varchar(100)='',                                                                            
 @SortChar char(1)='0' ,                                                                            
 @numFollowUpStatus as numeric(9)=0 ,                                                                            
 @CurrentPage int,                                                                            
 @PageSize int,                                                                            
 @columnName as Varchar(50)='',                                                                            
 @columnSortOrder as Varchar(10)='',                                                    
 @ListType as tinyint=0,                                                    
 @TeamType as tinyint=0,                                                     
 @TeamMemID as numeric(9)=0,                                                
 @numDomainID as numeric(9)=0    ,                  
 @ClientTimeZoneOffset as int  ,              
 @bitPartner as bit,
 @bitActiveInActive as bit,
 @vcRegularSearchCriteria varchar(MAX)='',
 @vcCustomSearchCriteria varchar(MAX)=''                                                                                  
AS       

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

IF CHARINDEX('AD.numBillCountry',@vcRegularSearchCriteria) > 0
BEGIN
	SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'AD.numBillCountry','AD1.numCountry')
END
IF CHARINDEX('AD.numShipCountry',@vcRegularSearchCriteria) > 0
BEGIN
	SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'AD.numShipCountry','AD2.numCountry')
END
IF CHARINDEX('ADC.vcLastFollowup',@vcRegularSearchCriteria) > 0
BEGIN
	SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'ADC.vcLastFollowup', '(SELECT dtExecutionDate FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID  WHERE ConECampaign.numContactID=ADC.numContactId AND ISNULL(ADC.numECampaignID,0) > 0 AND numConECampDTLID IN (SELECT MAX(numConECampDTLID) FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID  WHERE ConECampaign.numContactID=ADC.numContactId AND ISNULL(ADC.numECampaignID,0) > 0 AND ISNULL(bitSend,0)=1))')
END
IF CHARINDEX('ADC.vcNextFollowup',@vcRegularSearchCriteria) > 0
BEGIN
	SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'ADC.vcNextFollowup','(SELECT dtExecutionDate FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID  WHERE ConECampaign.numContactID=ADC.numContactId AND ISNULL(ADC.numECampaignID,0) > 0 AND numConECampDTLID IN (SELECT MIN(numConECampDTLID) FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID  WHERE ConECampaign.numContactID=ADC.numContactId AND ISNULL(ADC.numECampaignID,0) > 0 AND ISNULL(bitSend,0)=0))')
END      
	                                                                         
declare @strSql as varchar(MAX)                                                                             


declare @firstRec as integer                                                                            
  declare @lastRec as integer                                                                            
 set @firstRec= (@CurrentPage-1) * @PageSize                                                                           
     set @lastRec= (@CurrentPage*@PageSize+1)                                                                            
 
  DECLARE @strShareRedordWith AS VARCHAR(300);
 SET @strShareRedordWith=' DM.numDivisionID in (select SR.numRecordID FROM ShareRecord SR WHERE SR.numDomainID='+ convert(varchar(15),@numDomainID )+' AND SR.numModuleID=1 AND SR.numAssignedTo='+convert(varchar(15),@numUserCntId)+') '
                                                                                                                           
                                                              
 set @strSql= '
DECLARE @numDomain NUMERIC
SET @numDomain='+ convert(varchar(15),@numDomainID)+';

 with tblLeads as ( SELECT     '         
 set  @strSql= @strSql+ ' ROW_NUMBER() OVER (ORDER BY '+ CASE WHEN PATINDEX('%txt%',@columnName)>0 THEN ' CONVERT(VARCHAR(Max),' + @columnName + ')'  WHEN @columnName='numRecOwner' THEN 'DM.numRecOwner' ELSE @columnName end +' '+ @columnSortOrder+') AS RowNumber,                                                                                            
	COUNT(*) OVER(PARTITION BY NULL) AS TotalRowCount,
   DM.numDivisionID 
  FROM                                                                                
   CompanyInfo cmp join divisionmaster DM on cmp.numCompanyID=DM.numCompanyID    
   left join AdditionalContactsInformation ADC on ADC.numDivisionID=DM.numDivisionID                                             
 left join ListDetails lst on lst.numListItemID=DM.numFollowUpStatus left join VIEW_OPEN_ACTION_ITEMS VOA WITH (NOEXPAND) ON VOA.numDomainID = @numDomain AND  VOA.numDivisionId = DM.numDivisionID
   left join View_InboxEmail VIE  WITH (NOEXPAND) ON VIE.numContactId = ADC.numContactId ##JOIN##                                           
   
 WHERE                                                                               
   DM.tintCRMType=0  and  (ISNULL(ADC.bitPrimaryContact,0)=1 OR ADC.numContactID IS NULL)
  AND CMP.numDomainID=DM.numDomainID                                                          
 AND DM.numGrpID='+convert(varchar(15),@numGrpID)+ '                                                 
 AND DM.numDomainID=@numDomain'               
      
SET @strSql=@strSql + ' AND DM.bitActiveInActive=' + CONVERT(VARCHAR(15), @bitActiveInActive) 
        
if @bitPartner = 1 set @strSql=@strSql+' and (DM.numAssignedTo='+convert(varchar(15),@numUserCntID)+               
           ' or DM.numCreatedBy='+convert(varchar(15),@numUserCntID)+ ') '              
                                                                                    
 if @FirstName<>'' set @strSql=@strSql+' and ADC.vcFirstName  like '''+@FirstName+'%'''                                            
 if @LastName<>'' set @strSql=@strSql+' and ADC.vcLastName like '''+@LastName+'%'''                                            
 if @CustName<>'' set @strSql=@strSql+' and CMP.vcCompanyName like '''+@CustName+'%'''                                                                           
                                                                                   
if @numFollowUpStatus<>0 set @strSql=@strSql + '  AND DM.numFollowUpStatus='+convert(varchar(15),@numFollowUpStatus)                                                                             
if @SortChar<>'0' set @strSql=@strSql + ' And CMP.vcCompanyName like '''+@SortChar+'%'''                            
if @tintUserRightType=1 AND @numGrpID<>2 set @strSql=@strSql + ' AND (DM.numRecOwner = '+convert(varchar(15),@numUserCntID)+ ' or DM.numAssignedTo='+convert(varchar(15),@numUserCntID)+ ' or ' + @strShareRedordWith +')'                                                                             
else if @tintUserRightType=2 set @strSql=@strSql + ' AND (DM.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+convert(varchar(15),@numUserCntID)+' ) or DM.numAssignedTo='+convert(varchar(15),@numUserCntID)+ ' or ' + @strShareRedordWith +')' 


if @ListType=0 -- when MySelf radiobutton is checked
begin                                                    
IF @tintUserRightType=1 AND @numGrpID=1 set @strSql=@strSql + ' AND (DM.numRecOwner = '+convert(varchar(15),@numUserCntID)+ ' or DM.numAssignedTo='+convert(varchar(15),@numUserCntID)+ ' or ' + @strShareRedordWith +')'                                                                
if @tintUserRightType=1 AND @numGrpID=5 set @strSql=@strSql + ' AND (DM.numRecOwner = '+convert(varchar(15),@numUserCntID)+ ' or DM.numAssignedTo='+convert(varchar(15),@numUserCntID)+ ' or ' + @strShareRedordWith +')'                                                     
end                                          
                                      
else if @ListType=1 -- when Team Selected radiobutton is checked
begin                                 
if @TeamMemID=0 -- When All team members is selected in dropdown
 begin                                                  
 if @numGrpID<>2 set @strSql=@strSql + '  AND (DM.numRecOwner  in(select numContactID from  AdditionalContactsInformation                                      
		where numTeam in(select numTeam from ForReportsByTeam where numUserCntID='+convert(varchar(15),@numUserCntID)+' and tintType='+convert(varchar(15),@TeamType)+') ) or DM.numAssignedTo in(select numContactID from  AdditionalContactsInformation             
		where numTeam in(select numTeam from ForReportsByTeam where numUserCntID='+convert(varchar(15),@numUserCntID)+' and tintType='+convert(varchar(15),@TeamType)+'))' + ' or ' + @strShareRedordWith +')'                                                                       
                                                   
 end                                                  
else-- When All team member name is selected in dropdown
 begin                                                  
 if @numGrpID<>2 set @strSql=@strSql + '  AND ( DM.numRecOwner='+convert(varchar(15),@TeamMemID) +'  or  DM.numAssignedTo='+convert(varchar(15),@TeamMemID) + ' or ' + @strShareRedordWith +')'                                                         
                                                   
 end                                                   
end

IF @vcRegularSearchCriteria<>'' 
BEGIN
	set @strSql=@strSql+' and ' + @vcRegularSearchCriteria 
END
 
IF @vcCustomSearchCriteria<>'' set @strSql=@strSql+' AND ' + @vcCustomSearchCriteria

  set @strSql=@strSql+')'
--set @strSql=@strSql+'), AddUnreadMailCount AS 
--             ( SELECT ISNULL(VIE.Total,0) AS TotalEmail,t.* FROM tblLeads t left JOIN View_InboxEmail VIE  WITH (NOEXPAND) ON VIE.numDomainID = @numDomain AND  VIE.numContactId = t.numContactId
--                            )
--             , AddActionCount AS
--             ( SELECT    ISNULL(VOA.OpenActionItemCount,0) AS TotalActionItem,U.* FROM AddUnreadMailCount U LEFT JOIN  VIEW_OPEN_ACTION_ITEMS VOA WITH (NOEXPAND)  ON VOA.numDomainID = @numDomain AND  VOA.numDivisionId = U.numDivisionID 
--                            '                                  
                                                                           
declare @tintOrder as tinyint                                                
declare @vcFieldName as varchar(50)                                                
declare @vcListItemType as varchar(3)                                           
declare @vcAssociatedControlType varchar(20)                                                
declare @numListID AS numeric(9)                                                
declare @vcDbColumnName varchar(20)                    
declare @WhereCondition varchar(2000)                     
declare @vcLookBackTableName varchar(2000)              
Declare @bitCustomField as BIT
DECLARE @bitAllowEdit AS CHAR(1)                 
Declare @numFieldId as numeric  
DECLARE @bitAllowSorting AS CHAR(1)   
declare @vcColumnName AS VARCHAR(500)                  
set @tintOrder=0                                                
set @WhereCondition =''               
                 
declare @Nocolumns as tinyint              
--declare @DefaultNocolumns as tinyint         
set @Nocolumns=0              
 
select @Nocolumns=isnull(sum(TotalRow),0)  from(            
Select count(*) TotalRow from View_DynamicColumns where numFormId=34 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=1 
Union 
Select count(*) TotalRow from View_DynamicCustomColumns where numFormId=34 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=1) TotalRows
 

--Set @DefaultNocolumns=  @Nocolumns
  

CREATE TABLE #tempForm (tintOrder TINYINT,vcDbColumnName nvarchar(50),vcOrigDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),
  vcListItemType varchar(3),numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,
  numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,bitIsAlphaNumeric BIT,bitIsNumeric BIT,
  bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),ListRelID numeric(9),intColumnWidth int,bitAllowFiltering BIT,vcFieldDataType CHAR(1)
)

set @strSql=@strSql+' select ADC.numContactId,DM.numDivisionID,DM.numTerID,ADC.numRecOwner,DM.tintCRMType,RowNumber'      

---- Added by Priya 16 Jan 2018(Flag with Organisation Name)--------
SET @strSql=@strSql+' ,

	((CASE 
	WHEN ADC.numECampaignID > 0 
	THEN 
		 ISNULL( (CASE WHEN 
				ISNULL((SELECT COUNT(*) FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID
											WHERE ConECampaign.numContactID = ADC.numContactID AND  ConECampaign.numECampaignID = ADC.numECampaignID AND bitEngaged =1 AND ISNULL(bitSend,0) = 1 AND ADC.bitPrimaryContact = 1),0) 
				= ISNULL((SELECT COUNT(*) FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID
											 WHERE ConECampaign.numContactID = ADC.numContactID AND ConECampaign.numECampaignID = ADC.numECampaignID AND bitEngaged =1 AND ADC.bitPrimaryContact = 1 ),0)
		THEN
		  ''CheckeredFlag''
		  Else
		  ''GreenFlag''
		  End),0)



	ELSE '''' 
	END)) as FollowupFlag '

	SET @strSql=@strSql+' ,

	((CASE 
	WHEN ADC.numECampaignID > 0 
	THEN 
		''('' + CAST(ISNULL((SELECT COUNT(*) FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID
											WHERE ConECampaign.numContactID = ADC.numContactID AND  ConECampaign.numECampaignID = ADC.numECampaignID AND bitEngaged =1 AND ISNULL(bitSend,0) = 1 AND ADC.bitPrimaryContact = 1),0) AS varchar)

			+ ''/'' + CAST(ISNULL((SELECT COUNT(*) FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID
											 WHERE ConECampaign.numContactID = ADC.numContactID AND ConECampaign.numECampaignID = ADC.numECampaignID AND bitEngaged =1 AND ADC.bitPrimaryContact = 1 ),0)AS varchar) + '')''
			+ ''<a onclick=openDrip('' + (cast(ISNULL((SELECT MAX([numConEmailCampID]) FROM [ConECampaign] WHERE [numContactID] = ADC.numContactID AND [numECampaignID]= ADC.numECampaignID AND ISNULL(bitEngaged,0)=1 AND ADC.bitPrimaryContact = 1 ),0)AS varchar)) + '');>
		 <img alt=Follow-up Campaign history height=16px width=16px title=Follow-up campaign history src=../images/GLReport.png
		 ></a>'' 
	ELSE '''' 
	END)) as ReadUnreadCntNHstr '
	---- Added by Priya 16 Jan 2018(Flag with Organisation Name)--------
--Check if Master Form Configuration is created by administrator if NoColumns=0
DECLARE @IsMasterConfAvailable BIT = 0
DECLARE @numUserGroup NUMERIC(18,0)

IF @Nocolumns=0
BEGIN
	SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

	IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = 34 AND numRelCntType=1 AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
	BEGIN
		SET @IsMasterConfAvailable = 1
	END
END


--If MasterConfiguration is available then load it otherwise load default columns
IF @IsMasterConfAvailable = 1
BEGIN
	INSERT INTO DycFormConfigurationDetails 
	(
		numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
	)
	SELECT 
		34,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,1,1,0,intColumnWidth
	FROM 
		View_DynamicColumnsMasterConfig
	WHERE
		View_DynamicColumnsMasterConfig.numFormId=34 AND 
		View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
		View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
		View_DynamicColumnsMasterConfig.numRelCntType=1 AND 
		View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
		ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
		ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
	UNION
    SELECT 
		34,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,1,1,1,intColumnWidth
	FROM 
		View_DynamicCustomColumnsMasterConfig
	WHERE 
		View_DynamicCustomColumnsMasterConfig.numFormId=34 AND 
		View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
		View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
		View_DynamicCustomColumnsMasterConfig.numRelCntType=1 AND 
		View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
		ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1

	INSERT INTO #tempForm
	SELECT 
		(intRowNum + 1) as tintOrder, vcDbColumnName,	vcOrigDbColumnName,	ISNULL(vcCultureFieldName,vcFieldName), vcAssociatedControlType,
		vcListItemType, numListID, vcLookBackTableName,	bitCustom, numFieldId, bitAllowSorting,	bitInlineEdit, bitIsRequired,
		bitIsEmail,	bitIsAlphaNumeric, bitIsNumeric, bitIsLengthValidation,	intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,
		ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType 
	FROM 
		View_DynamicColumnsMasterConfig
	WHERE
		View_DynamicColumnsMasterConfig.numFormId=34 AND 
		View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
		View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
		View_DynamicColumnsMasterConfig.numRelCntType=1 AND 
		View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
		ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
		ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
	UNION
    SELECT 
		tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName, vcAssociatedControlType,'' as vcListItemType,numListID,'',
		bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
		intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,''
	FROM 
		View_DynamicCustomColumnsMasterConfig
	WHERE 
		View_DynamicCustomColumnsMasterConfig.numFormId=34 AND 
		View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
		View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
		View_DynamicCustomColumnsMasterConfig.numRelCntType=1 AND 
		View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
		ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1
	ORDER BY 
		tintOrder ASC  
END
ELSE                                        
BEGIN
	if @Nocolumns=0
	BEGIN
					INSERT INTO DycFormConfigurationDetails (numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth)
select 34,numFieldId,0,Row_number() over(order by tintRow desc),@numDomainID,@numUserCntID,1,1,0,intColumnWidth
 FROM View_DynamicDefaultColumns
 where numFormId=34 and bitDefault=1 AND ISNULL(bitSettingField,0)=1 and numDomainID=@numDomainID
order by tintOrder asc   

	END

	INSERT INTO #tempForm
select tintRow+1 as tintOrder,vcDbColumnName,vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
 vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName                                                 
,bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType 
 FROM View_DynamicColumns 
 where numFormId=34 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=1
  AND ISNULL(bitSettingField,0)=1 AND ISNULL(bitCustom,0)=0
       
 UNION
    
    select tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName,                  
 vcAssociatedControlType,'' as vcListItemType,numListID,''                                                 
,bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,''
 from View_DynamicCustomColumns
 where numFormId=34 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=1
 AND ISNULL(bitCustom,0)=1
 
  order by tintOrder asc  
END        
Declare @ListRelID as numeric(9)  
  select top 1 @tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,                
  @vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,@numListID=numListID,@vcLookBackTableName=vcLookBackTableName                                               
,@bitCustomField=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,
@ListRelID=ListRelID
  from  #tempForm order by tintOrder asc            
 


while @tintOrder>0                                                
begin                                                
     
     if @bitCustomField = 0            
 begin             
     declare @Prefix as varchar(5)                  
     if @vcLookBackTableName = 'AdditionalContactsInformation'                  
   set @Prefix = 'ADC.'                  
     if @vcLookBackTableName = 'DivisionMaster'                  
   set @Prefix = 'DM.'       
   
          SET @vcColumnName= @vcDbColumnName+'~'+ convert(varchar(10),@numFieldId) +'~0'
                                         
			IF @vcDbColumnName='bitEcommerceAccess'
			BEGIN
				SET @strSql=@strSql+ ' ,(CASE WHEN (SELECt COUNT(*) FROM ExtarnetAccounts WHERE numDivisionID=DM.numDivisionID) >0 THEN 1 ELSE 0 END) AS ['+ @vcColumnName+']' 
			END
			ELSE IF @vcDbColumnName = 'vcLastFollowup'
			BEGIN
				set @strSql=@strSql+',  ISNULL((CASE WHEN (ADC.numECampaignID > 0) THEN dbo.fn_FollowupDetailsInOrgList(ADC.numContactID,1,ADC.numDomainID) ELSE '''' END),'''') ['+ @vcColumnName+']' 
			END
			ELSE IF @vcDbColumnName = 'vcNextFollowup'
			BEGIN
				set @strSql=@strSql+',  ISNULL((CASE WHEN  (ADC.numECampaignID >0) THEN dbo.fn_FollowupDetailsInOrgList(ADC.numContactID,2,ADC.numDomainID) ELSE '''' END),'''') ['+ @vcColumnName+']' 
			END
			ELSE if @vcAssociatedControlType='SelectBox' or @vcAssociatedControlType='ListBox'                                                    
        begin
     
	IF @vcDbColumnName = 'numPartenerSource'
	BEGIN
		SET @strSql=@strSql+ ' ,(DM.numPartenerSource) '+' ['+ @vcColumnName+']'    
		SET @strSql=@strSql+ ' ,(SELECT D.vcPartnerCode+''-''+C.vcCompanyName FROM DivisionMaster AS D LEFT JOIN CompanyInfo AS C ON D.numCompanyID=C.numCompanyID WHERE D.numDivisionID=DM.numPartenerSource) AS vcPartenerSource' 
	END
	IF @vcDbColumnName = 'numPartenerContact'
	BEGIN
		SET @strSql=@strSql+ ' ,(DM.numPartenerContact) '+' ['+ @vcColumnName+']'    
		SET @strSql=@strSql+ ' ,(SELECT A.vcFirstName+'' ''+ A.vcLastName FROM AdditionalContactsInformation AS A WHERE A.numContactId=DM.numPartenerContact) AS vcPartenerContact' 
	END
      
     if @vcListItemType='LI'                                                     
     begin                                                    
		 IF @numListID=40 
		 BEGIN
			
				SET @strSql = @strSql + ',L' + CONVERT(VARCHAR(3), @tintOrder) + '.vcData' + ' [' + @vcColumnName + ']'                                                   
                                            
				IF @vcDbColumnName='numShipCountry'
				BEGIN
					SET @WhereCondition = @WhereCondition
										+ ' left Join AddressDetails AD2 on AD2.numRecordId= DM.numDivisionID and AD2.tintAddressOf=2 and AD2.tintAddressType=2 AND AD2.bitIsPrimary=1 and AD2.numDomainID= DM.numDomainID '
										+ ' left Join ListDetails L' + CONVERT(VARCHAR(3), @tintOrder) + ' on L' + CONVERT(VARCHAR(3), @tintOrder) + '.numListItemID=AD2.numCountry '
				END
				ELSE
				BEGIN
					SET @WhereCondition = @WhereCondition
									+ ' left Join AddressDetails AD1 on AD1.numRecordId= DM.numDivisionID and AD1.tintAddressOf=2 and AD1.tintAddressType=1 AND AD1.bitIsPrimary=1 and AD1.numDomainID= DM.numDomainID '
									+ ' left Join ListDetails L' + CONVERT(VARCHAR(3), @tintOrder) + ' on L' + CONVERT(VARCHAR(3), @tintOrder) + '.numListItemID=AD1.numCountry '
				END


		 END
		 ELSE
		 BEGIN
			set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName+']'                                                   
			set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName		 	
		 END
     end                                                    
     else if @vcListItemType='S'                                                     
     begin        
			IF @vcDbColumnName='numShipState'
			BEGIN
				SET @strSql = @strSql + ',ShipState.vcState' + ' [' + @vcColumnName + ']' 
				SET @WhereCondition = @WhereCondition
					+ ' left Join AddressDetails AD3 on AD3.numRecordId= DM.numDivisionID and AD3.tintAddressOf=2 and AD3.tintAddressType=2 AND AD3.bitIsPrimary=1 and AD3.numDomainID= DM.numDomainID '
					+ ' left Join State ShipState on ShipState.numStateID=AD3.numState '
			END
			ELSE IF @vcDbColumnName='numBillState'
			BEGIN
				SET @strSql = @strSql + ',BillState.vcState' + ' [' + @vcColumnName + ']' 
				SET @WhereCondition = @WhereCondition
					+ ' left Join AddressDetails AD4 on AD4.numRecordId= DM.numDivisionID and AD4.tintAddressOf=2 and AD4.tintAddressType=1 AND AD4.bitIsPrimary=1 and AD4.numDomainID= DM.numDomainID '
					+ ' left Join State BillState on BillState.numStateID=AD4.numState '
			END
			ELSE  
			BEGIN
				SET @strSql = @strSql + ',S' + CONVERT(VARCHAR(3), @tintOrder) + '.vcState' + ' [' + @vcColumnName + ']' 
				SET @WhereCondition = @WhereCondition + ' left join State S' + CONVERT(VARCHAR(3), @tintOrder) + ' on S' + CONVERT(VARCHAR(3), @tintOrder) + '.numStateID=' + @vcDbColumnName        
			END		                                            
     end                                                    
     else if @vcListItemType='T'                                                     
     begin                                                    
      set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName+']'                                                    
      set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName                                                    
     end   
      else if @vcListItemType='C'                                                     
     begin                                                    
      set @strSql=@strSql+',C'+ convert(varchar(3),@tintOrder)+'.vcCampaignName'+' ['+ @vcColumnName+']'                                                    
      set @WhereCondition= @WhereCondition +' left Join CampaignMaster C'+ convert(varchar(3),@tintOrder)+ ' on C'+convert(varchar(3),@tintOrder)+ '.numCampaignId=DM.'+@vcDbColumnName                                                    
     end  
      else if @vcListItemType='AG'                                                     
     begin                                                    
      set @strSql=@strSql+',AG'+ convert(varchar(3),@tintOrder)+'.vcGrpName'+' ['+ @vcColumnName+']'                                                    
      set @WhereCondition= @WhereCondition +' left Join Groups AG'+ convert(varchar(3),@tintOrder)+ ' on AG'+convert(varchar(3),@tintOrder)+ '.numGrpId=DM.'+@vcDbColumnName                                                    
     end   
      else if @vcListItemType='DC'                                                     
     begin                                                    
      set @strSql=@strSql+',DC'+ convert(varchar(3),@tintOrder)+'.vcECampName'+' ['+ @vcColumnName+']'                                                    
      set @WhereCondition= @WhereCondition +' left Join ECampaign DC'+ convert(varchar(3),@tintOrder)+ ' on DC'+convert(varchar(3),@tintOrder)+ '.numECampaignID=ADC.'+@vcDbColumnName                                                    
     end                   
     else if   @vcListItemType='U'                                                 
    begin                   
    set @strSql=@strSql+',dbo.fn_GetContactName('+@Prefix+@vcDbColumnName+') ['+ @vcColumnName+']'                                                    
    end                  
    end 
	
	----- Added by Priya For Followups(14Jan2018)---- 
			
			----- Added by Priya For Followups(14Jan2018)---- 
                                                   
      else if @vcAssociatedControlType='DateField'                                                  
 begin            
       
     set @strSql=@strSql+',case when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+') )= convert(varchar(11),getdate()) then ''<b><font color=red>Today</font></b>'''      
     set @strSql=@strSql+'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+') )= convert(varchar(11),dateadd(day,-1,getdate())) then ''<b><font color=purple>YesterDay</font></b>'''      
     set @strSql=@strSql+'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+') )= convert(varchar(11),dateadd(day,1,getdate())) then''<b><font color=orange>Tommorow</font></b>'' ' 
     set @strSql=@strSql+'else dbo.FormatedDateFromDate(DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+'),'+convert(varchar(10),@numDomainId)+') end  ['+ @vcColumnName+']'
   end    
          
else if @vcAssociatedControlType='TextBox' OR @vcAssociatedControlType='Label'                                                 
begin    
 set @strSql=@strSql+','+ case  when @vcDbColumnName='numAge' then 'year(getutcdate())-year(bintDOB)' when    
  @vcDbColumnName='numDivisionID' then 'DM.numDivisionID' else @vcDbColumnName end+' ['+ @vcColumnName+']'              
       
 end 
 else if @vcAssociatedControlType='Popup' and @vcDbColumnName='numShareWith'                                                   
		begin      
			 set @strSql=@strSql+',(SELECT SUBSTRING(
	(SELECT '','' + A.vcFirstName+'' ''+A.vcLastName + CASE WHEN ISNULL(SR.numContactType,0)>0 THEN ''('' + dbo.fn_GetListItemName(SR.numContactType) + '')'' ELSE '''' END
	FROM ShareRecord SR JOIN UserMaster UM ON UM.numUserDetailId=SR.numAssignedTo 
							 JOIN AdditionalContactsInformation A ON UM.numUserDetailId=A.numContactID 
		 WHERE SR.numDomainID=DM.numDomainID AND SR.numModuleID=1 AND SR.numRecordID=DM.numDivisionID
				AND UM.numDomainID=DM.numDomainID and UM.numDomainID=A.numDomainID FOR XML PATH('''')),2,200000)) ['+ @vcColumnName+']'                
		 end       
 else                                               
begin    
	if @vcDbColumnName='vcCampaignAudit'
		set @strSql=@strSql+', dbo.GetContactCampignAuditDetail(ADC.numECampaignID,ADC.numContactId) ['+ @vcColumnName+']'  
	else
		set @strSql=@strSql+','+ @vcDbColumnName +' ['+ @vcColumnName+']'
 end          
                
end            
else if @bitCustomField = 1            
begin            
               
     

   SET @vcColumnName= @vcDbColumnName +'~'+ convert(varchar(10),@numFieldId)+'~1'
            
   print @vcAssociatedControlType   
   print @numFieldId           
   if @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'            
   begin            
               
    set @strSql= @strSql+',CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value  ['+ @vcColumnName +']'               
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values CFW'+ convert(varchar(3),@tintOrder)+ '             
    on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=DM.numDivisionId   '                                                     
   end  
   else if @vcAssociatedControlType = 'CheckBox'         
   begin            
               
    --set @strSql= @strSql+',case when isnull(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,0)=0 then ''No'' when isnull(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,0)=1 then ''Yes'' end   ['+ @vcColumnName +']'              
    set @strSql= @strSql+',case when isnull(CFW' + CONVERT(VARCHAR(3), @tintOrder)+ '.Fld_Value,0)='''' then 0 else  isnull(CFW'
                                        + CONVERT(VARCHAR(3), @tintOrder)+ '.Fld_Value,0) end ['+ @vcColumnName + ']'
 
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values CFW'+ convert(varchar(3),@tintOrder)+ '             
    on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=DM.numDivisionId   '                                                     
   end            
   else if @vcAssociatedControlType = 'DateField'        
   begin            
               
    set @strSql= @strSql+',dbo.FormatedDateFromDate(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,'+convert(varchar(10),@numDomainId)+')  ['+ @vcColumnName +']'              
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values CFW'+ convert(varchar(3),@tintOrder)+ '             
    on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=DM.numDivisionId   '                                                     
   end            
   else if @vcAssociatedControlType = 'SelectBox'           
   begin            
    set @vcDbColumnName = 'DCust'+Convert(varchar(10),@numFieldId)            
    set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName +']'                                                     
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values CFW'+ convert(varchar(3),@tintOrder)+ '             
     on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=DM.numDivisionId    '                                                     
    set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value'            
   end   
   ELSE IF @vcAssociatedControlType = 'CheckBoxList' 
	BEGIN
		SET @strSql= @strSql+',(SELECT STUFF((SELECT CONCAT('', '', vcData) FROM ListDetails WHERE numListID= ' + CAST(@numListID AS varchar) + ' AND numlistitemid IN (SELECT ISNULL(Id,0) FROM dbo.SplitIDs(CFW'+ convert(VARCHAR(3),@tintOrder)+'.Fld_Value,'','')) FOR XML PATH('''')), 1, 1, ''''))  ['+ @vcColumnName +']'   

	SET @WhereCondition= @WhereCondition 
						+' left Join CFW_FLD_Values CFW'+ convert(varchar(3),@tintOrder)
						+ ' on CFW' + convert(varchar(3),@tintOrder) 
						+ '.Fld_Id='+convert(varchar(10),@numFieldId) 
						+ 'and CFW' + convert(varchar(3),@tintOrder) 
						+ '.RecId=DM.numDivisionId '
	END        
   BEGIN
		SET @strSql = @strSql + CONCAT(',dbo.GetCustFldValue(',@numFieldId,',1,DM.numDivisionID)') + ' [' + @vcColumnName + ']'
	END                 
end      
            
    
  select top 1 @tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,                
  @vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,@numListID=numListID,@vcLookBackTableName=vcLookBackTableName                                               
,@bitCustomField=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,@ListRelID=ListRelID
  from #tempForm WHERE tintOrder > @tintOrder-1 order by tintOrder asc            
 
  if @@rowcount=0 set @tintOrder=0 
        
    
          
end                     
 
-------Change Row Color-------
Declare @vcCSOrigDbCOlumnName as varchar(50),@vcCSLookBackTableName as varchar(50),@vcCSAssociatedControlType as varchar(50)
SET @vcCSOrigDbCOlumnName=''
SET @vcCSLookBackTableName=''

--select top 1 @vcCSOrigDbCOlumnName=DFM.vcOrigDbCOlumnName,@vcCSLookBackTableName=DFM.vcLookBackTableName from DycFieldColorScheme DFCS join DycFieldMaster DFM on 
--DFCS.numModuleID=DFM.numModuleID and DFCS.numFieldID=DFM.numFieldID
--where DFCS.numDomainID=@numDomainID and DFM.numModuleID=1

Create table #tempColorScheme(vcOrigDbCOlumnName varchar(50),vcLookBackTableName varchar(50),vcFieldValue varchar(50),vcFieldValue1 varchar(50),vcColorScheme varchar(50),vcAssociatedControlType varchar(50))

insert into #tempColorScheme select DFM.vcOrigDbCOlumnName,DFM.vcLookBackTableName,DFCS.vcFieldValue,DFCS.vcFieldValue1,DFCS.vcColorScheme,DFFM.vcAssociatedControlType 
from DycFieldColorScheme DFCS join DycFormField_Mapping DFFM on DFCS.numFieldID=DFFM.numFieldID
join DycFieldMaster DFM on DFM.numModuleID=DFFM.numModuleID and DFM.numFieldID=DFFM.numFieldID
where DFCS.numDomainID=@numDomainID and DFFM.numFormID=34 AND DFCS.numFormID=34 and isnull(DFFM.bitAllowGridColor,0)=1

IF(SELECT COUNT(*) FROM #tempColorScheme)>0
BEGIN
   select top 1 @vcCSOrigDbCOlumnName=vcOrigDbCOlumnName,@vcCSLookBackTableName=vcLookBackTableName,@vcCSAssociatedControlType=vcAssociatedControlType from #tempColorScheme
END   
----------------------------                       

IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
BEGIN
set @strSql=@strSql+',tCS.vcColorScheme'
END

  set @strSql=@strSql+' ,TotalRowCount
 ,ISNULL(ADC.numECampaignID,0) as numECampaignID
 ,ISNULL( (SELECT MAX([numConEmailCampID]) FROM [ConECampaign] WHERE [numContactID] = ADC.numContactId AND [numECampaignID]= ADC.numECampaignID AND ISNULL(bitEngaged,0)=1),0) numConEmailCampID
 ,ISNULL(VIE.Total,0) as TotalEmail,
 ISNULL(VOA.OpenActionItemCount,0) as TotalActionItem
 FROM CompanyInfo cmp join divisionmaster DM on cmp.numCompanyID=DM.numCompanyID                                                        
   left join ListDetails lst on lst.numListItemID=DM.numFollowUpStatus                                                                               
   join AdditionalContactsInformation ADC on ADC.numDivisionID=DM.numDivisionID 
   left join VIEW_OPEN_ACTION_ITEMS VOA WITH (NOEXPAND) ON VOA.numDomainID = @numDomain AND  VOA.numDivisionId = DM.numDivisionID
   left join View_InboxEmail VIE  WITH (NOEXPAND) ON VIE.numContactId = ADC.numContactId                                               
   join tblLeads T on T.numDivisionID=DM.numDivisionID   '+@WhereCondition


SET @strSql = REPLACE(@strSql,'##JOIN##',@WhereCondition)

IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
BEGIN
	 if @vcCSLookBackTableName = 'AdditionalContactsInformation'                  
		set @Prefix = 'ADC.'                  
     if @vcCSLookBackTableName = 'DivisionMaster'                  
		set @Prefix = 'DM.'
	 if @vcCSLookBackTableName = 'CompanyInfo'                  
		set @Prefix = 'CMP.'   
 
	IF @vcCSAssociatedControlType='DateField'
	BEGIN
		--set @strSql=@strSql+' left join #tempColorScheme tCS on convert(varchar(11),DateAdd(day,tCS.vcFieldValue,GETUTCDATE()))=' + @Prefix + 'convert(varchar(11),DateAdd(day,tCS.vcFieldValue,' + @vcCSOrigDbCOlumnName + '))'
					set @strSql=@strSql+' left join #tempColorScheme tCS on CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) >= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue as int),GETUTCDATE()),111) 
			 and CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) <= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue1 as int),GETUTCDATE()),111)'

	END
	ELSE IF @vcCSAssociatedControlType='SelectBox' OR @vcCSAssociatedControlType='ListBox' OR @vcCSAssociatedControlType='CheckBox'
	BEGIN
		set @strSql=@strSql+' left join #tempColorScheme tCS on tCS.vcFieldValue=' + @Prefix + @vcCSOrigDbCOlumnName
	END
END

 set @strSql=@strSql+' WHERE                                               
   DM.tintCRMType=0 and ISNULL(ADC.bitPrimaryContact,0)=1 AND CMP.numDomainID=DM.numDomainID  and DM.numDomainID=ADC.numDomainID                                                  
   and RowNumber > '+convert(varchar(10),@firstRec)+ ' and RowNumber <'+ convert(varchar(10),@lastRec)+ ' order by RowNumber'
                                             
print @strSql                      
              
exec (@strSql)  

SELECT * FROM #tempForm

DROP TABLE #tempForm

drop table #tempColorScheme
/****** Object:  StoredProcedure [dbo].[USP_GetMasterListItemsUsingDomainID]    Script Date: 07/26/2008 16:17:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj            
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getmasterlistitemsusingdomainid')
DROP PROCEDURE usp_getmasterlistitemsusingdomainid
GO
CREATE PROCEDURE [dbo].[USP_GetMasterListItemsUsingDomainID]            
@numDomainID as numeric(9)=0,            
@numListID as numeric(9)=0          
AS
BEGIN            
	DECLARE @bit3PL BIT, @bitEDI BIT
	SELECT @bit3PL=ISNULL(bit3PL,0),@bitEDI=ISNULL(bitEDI,0) FROM Domain WHERE numDomainID=@numDomainID
      
	SELECT 
		Ld.numListItemID
		,ISNULL(LDN.vcName,vcData) AS vcData
		,Ld.constFlag
		,bitDelete
		,ISNULL(intSortOrder,0) intSortOrder
		,ISNULL(numListType,0) as numListType
		,(SELECT vcData FROM ListDetails WHERE numListId=27 and numListItemId=ld.numListType) AS vcListType
		,(CASE WHEN ld.numListType=1 THEN 'Sales' WHEN ld.numListType=2 THEN 'Purchase' ELSE 'All' END) AS vcOrderType
		,(CASE WHEN ld.tintOppOrOrder=1 THEN 'Opportunity' WHEN ld.tintOppOrOrder=2 THEN 'Order' ELSE 'All' END) AS vcOppOrOrder
		,numListType
		,tintOppOrOrder,ISNULL(SOR.bitEnforceMinOrderAmount,'False') AS bitEnforceMinOrderAmount, ISNULL(SOR.fltMinOrderAmount,0) AS fltMinOrderAmount
	FROM 
		ListDetails LD          
	LEFT JOIN 
		listorder LO 
	ON 
		Ld.numListItemID = LO.numListItemID 
		AND lo.numDomainId = @numDomainID
	LEFT JOIN 
		ListDetailsName LDN 
	ON 
		LDN.numDOmainID=@numDomainID 
		AND LDN.numListID=@numListID 
		AND LDN.numListItemID=LD.numListItemID
	LEFT JOIN
		SalesOrderRule SOR ON LD.numListItemID = SOR.numListItemID
	WHERE 
		(Ld.numDomainID=@numDomainID OR Ld.constFlag=1) 
		AND Ld.numListID=@numListID           
		AND 1 = (CASE 
					WHEN LD.numListItemID IN (15445,15446) THEN (CASE WHEN @bit3PL=1 THEN 1 ELSE 0 END)
					WHEN LD.numListItemID IN (15447,15448) THEN (CASE WHEN @bitEDI=1 THEN 1 ELSE 0 END)
					ELSE 1 
				END)
	ORDER BY 
		intSortOrder
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetNavigation')
DROP PROCEDURE dbo.USP_GetNavigation
GO
CREATE PROCEDURE [dbo].[USP_GetNavigation]
(
	@numDomainID NUMERIC(18,0),
	@numGroupID NUMERIC(18,0)
)
AS 
BEGIN

	/********  MAIN MENU  ********/

	SELECT  
		T.numTabID,
		ISNULL((SELECT TOP 1 numTabName FROM TabDefault WHERE numDomainid = @numDomainID AND numTabId = T.numTabId AND tintTabType = T.tintTabType),T.numTabName) vcTabName,
		CASE 
			WHEN (SELECT COUNT(*) FROM ShortCutGrpConf WHERE numDomainID=@numDomainID AND numGroupId=@numGroupID AND numTabId=T.numTabId AND ISNULL(bitInitialPage,0)=1) > 0
			THEN
				REPLACE(ISNULL((SELECT TOP 1 Link FROM ShortCutBar WHERE (numDomainid = @numDomainID OR bitDefault=1) AND numTabId = T.numTabId AND id=(SELECT TOP 1 numLinkId FROM ShortCutGrpConf WHERE numDomainID=@numDomainID AND numGroupId=@numGroupID AND numTabId=T.numTabId AND ISNULL(bitInitialPage,0)=1)),''),'../','')
			ELSE 
				ISNULL(vcURL,'')
		END vcURL,
		ISNULL(vcAddURL,'') vcAddURL,
		ISNULL(bitAddIsPopUp,0) AS bitAddIsPopUp,
		numOrder
	FROM    
		TabMaster T
	JOIN 
		GroupTabDetails G ON G.numTabId = T.numTabId
	WHERE   
		(numDomainID = @numDomainID OR bitFixed = 1)
		AND numGroupID = @numGroupID
		AND ISNULL(G.[tintType], 0) <> 1
		AND tintTabType =1
		AND T.numTabID NOT IN (2,68)
	ORDER BY 
		numOrder   


	/********  SUB MENU  ********/

	DECLARE @numTabID NUMERIC(18,0)
	DECLARE @numModuleID AS NUMERIC(18, 0)

	DECLARE @TEMP TABLE
	(
		ID INT IDENTITY(1,1),
		numOrigParentID NUMERIC(18,0),
		numParentID NUMERIC(18,0),
		numListItemID NUMERIC(18,0),
		numTabID NUMERIC(18,0),
		numPageNavID NUMERIC(18,0),
		vcNodeName VARCHAR(500),
		vcURL VARCHAR(1000),
		vcAddURL VARCHAR(1000),
		bitAddIsPopUp BIT,
		[order] int,
		bitVisible BIT,
		vcImageURL VARCHAR(1000)
	)

	BEGIN /******************** OPPORTUNITY/ORDER ***********************/

		SET @numTabID = 1
		SELECT TOP 1 @numModuleID = numModuleID FROM dbo.PageNavigationDTL WHERE numTabID = @numTabID

		INSERT INTO
			@TEMP
		SELECT
			0,
			0,
			0,
			@numTabID,
			PND.numPageNavID,
			vcPageNavName,
			ISNULL(vcNavURL,'') AS vcNavURL,
			ISNULL(PND.vcAddURL,''),
			ISNULL(PND.bitAddIsPopUp,0),
			0,
			1,
			ISNULL(PND.vcImageURL,'')
		FROM 
			PageNavigationDTL PND
		JOIN 
			dbo.TreeNavigationAuthorization TNA 
		ON 
			PND.numPageNavID = TNA.numPageNavID  
			AND PND.numTabID = TNA.numTabID       
		WHERE  
			ISNULL(TNA.bitVisible, 0) = 1
			AND ISNULL(PND.bitVisible, 0) = 1
			AND numModuleID = @numModuleID
			AND TNA.numDomainID = @numDomainID
			AND numGroupID = @numGroupID
			AND PND.numParentID <> 0
		ORDER BY 
			numPageNavID 

		INSERT INTO
			@TEMP
		SELECT  
			0,
			0,
			0,
			@numTabID,
			0,
			ISNULL(vcData,'') + 's' [vcPageNavName],
			'../Opportunity/frmOpenBizDocs.aspx?BizDocName=' + vcData + '&BizDocID=' + CONVERT(VARCHAR(10),LD.numListItemID) AS [vcNavURL],
			'',
			0,
			0,
			1 ,
			''
		FROM 
			dbo.ListDetails LD
		JOIN 
			dbo.TreeNavigationAuthorization TNA 
		ON 
			LD.numListItemID = TNA.numPageNavID 
			AND ISNULL(bitVisible,0) = 1  
		WHERE 
			numListID = 27 
			AND ISNULL(TNA.[numDomainID], 0) = @numDomainID
			AND ISNULL(TNA.[numGroupID], 0) = @numGroupID
		ORDER BY 
			numPageNavID 

	END

	BEGIN /******************** Email ***************************/
		SET @numTabID = 44
		SELECT TOP 1 @numModuleID = numModuleID FROM dbo.PageNavigationDTL WHERE numTabID = @numTabID 	

		INSERT INTO
			@TEMP
		SELECT 
		    ISNULL(PND.numParentID,0),
			PND.numParentID,
			0,
			@numTabID,
			PND.numPageNavID AS numPageNavID,
			vcPageNavName,
			ISNULL(vcNavURL, '') as vcNavURL,
			ISNULL(PND.vcAddURL,''),
			ISNULL(PND.bitAddIsPopUp,0),
			0,
			1,
			ISNULL(PND.vcImageURL,'')
		FROM   
			PageNavigationDTL PND
		JOIN 
			dbo.TreeNavigationAuthorization TNA 
		ON 
			PND.numPageNavID = TNA.numPageNavID
			AND TNA.numTabID=@numTabID
		WHERE 
			ISNULL(TNA.bitVisible, 0) = 1      
			AND ISNULL(PND.bitVisible, 0) = 1
			AND PND.numParentID <> 0
			AND PND.numModuleID = @numModuleID
			AND numGroupID = @numGroupID
		ORDER BY 
			numParentID,
			intSortOrder,
			numPageNavID

		UPDATE 
			t1
		SET 
			t1.numParentID = ISNULL((SELECT ID FROM @TEMP t2 WHERE t2.numPageNavID=t1.numParentID AND t2.numTabID=@numTabID),0)
		FROM
			@TEMP t1
		WHERE 
			numTabID=@numTabID
	END

	BEGIN /******************** MARKETING ***************************/
		SET @numTabID = 3
		SELECT TOP 1 @numModuleID = numModuleID FROM dbo.PageNavigationDTL WHERE numTabID = @numTabID 	

		INSERT INTO
			@TEMP
		SELECT 
		    ISNULL(PND.numParentID,0),
			PND.numParentID,
			0,
			@numTabID,
			PND.numPageNavID AS numPageNavID,
			vcPageNavName,
			ISNULL(vcNavURL, '') as vcNavURL,
			ISNULL(PND.vcAddURL,''),
			ISNULL(PND.bitAddIsPopUp,0),
			0,
			1,
			ISNULL(PND.vcImageURL,'')
		FROM   
			PageNavigationDTL PND
		JOIN 
			dbo.TreeNavigationAuthorization TNA 
		ON 
			PND.numPageNavID = TNA.numPageNavID
			AND TNA.numTabID=@numTabID
		WHERE 
			ISNULL(TNA.bitVisible, 0) = 1      
			AND ISNULL(PND.bitVisible, 0) = 1
			AND PND.numParentID <> 0
			AND PND.numModuleID = @numModuleID
			AND numGroupID = @numGroupID
		ORDER BY 
			numParentID,
			intSortOrder,
			numPageNavID

		UPDATE 
			t1
		SET 
			t1.numParentID = ISNULL((SELECT ID FROM @TEMP t2 WHERE t2.numPageNavID=t1.numParentID AND t2.numTabID=@numTabID),0)
		FROM
			@TEMP t1
		WHERE 
			numTabID=@numTabID
	END

	BEGIN /******************** SUPPORT/CASE ***************************/

		SET @numTabID = 4
		SELECT TOP 1 @numModuleID = numModuleID FROM dbo.PageNavigationDTL WHERE numTabID = @numTabID 	

		INSERT INTO
			@TEMP
		SELECT 
			ISNULL(PND.numParentID,0),
			PND.numParentID,
			0,
			@numTabID,
			PND.numPageNavID AS numPageNavID,
			vcPageNavName,
			ISNULL(vcNavURL, '') as vcNavURL,
			ISNULL(PND.vcAddURL,''),
			ISNULL(PND.bitAddIsPopUp,0),
			0,
			1,
			ISNULL(PND.vcImageURL,'')
		FROM   
			PageNavigationDTL PND
		LEFT JOIN 
			dbo.TreeNavigationAuthorization TNA 
		ON 
			PND.numPageNavID = TNA.numPageNavID
			AND TNA.numTabID=@numTabID
		WHERE 
			ISNULL(TNA.bitVisible, 1) = 1
			AND ISNULL(PND.bitVisible, 1) = 1
			AND PND.numParentID <> 0
			AND PND.numModuleID = @numModuleID
			AND numGroupID = @numGroupID
		ORDER BY 
			numParentID,
			intSortOrder,
			numPageNavID

		UPDATE 
			t1
		SET 
			t1.numParentID = ISNULL((SELECT ID FROM @TEMP t2 WHERE t2.numPageNavID=t1.numParentID AND t2.numTabID=@numTabID),0)
		FROM
			@TEMP t1
		WHERE 
			numTabID=@numTabID

	END

	BEGIN /******************** REPORTS ***************************/

		SET @numTabID = 6
		SELECT TOP 1 @numModuleID = numModuleID FROM dbo.PageNavigationDTL WHERE numTabID = @numTabID 	

		INSERT INTO
			@TEMP
		SELECT 
		    ISNULL(PND.numParentID,0),
			PND.numParentID,
			0,
			@numTabID,
			PND.numPageNavID AS numPageNavID,
			vcPageNavName,
			ISNULL(vcNavURL, '') as vcNavURL,
			ISNULL(PND.vcAddURL,''),
			ISNULL(PND.bitAddIsPopUp,0),
			0,
			1,
			ISNULL(PND.vcImageURL,'')
		FROM   
			PageNavigationDTL PND
		LEFT JOIN 
			dbo.TreeNavigationAuthorization TNA 
		ON 
			PND.numPageNavID = TNA.numPageNavID
			AND TNA.numTabID=@numTabID
		WHERE 
			ISNULL(TNA.bitVisible, 1) = 1
			AND ISNULL(PND.bitVisible, 1) = 1
			AND PND.numParentID <> 0
			AND PND.numModuleID = @numModuleID
			AND numGroupID = @numGroupID
		ORDER BY 
			numParentID,
			intSortOrder,
			numPageNavID

		UPDATE 
			t1
		SET 
			t1.numParentID = ISNULL((SELECT ID FROM @TEMP t2 WHERE t2.numPageNavID=t1.numParentID AND t2.numTabID=@numTabID),0)
		FROM
			@TEMP t1
		WHERE 
			numTabID=@numTabID

	END

	BEGIN /******************** RELATIONSHIP ***********************/

		SET @numTabID = 7
		SELECT TOP 1 @numModuleID = numModuleID FROM dbo.PageNavigationDTL WHERE numTabID = @numTabID 	

		INSERT INTO 
			@TEMP
		SELECT    
			ISNULL(PND.numParentID,0),
			PND.numParentID AS numParentID,
			0,
			@numTabID,
			PND.numPageNavID AS numPageNavID,
			ISNULL(PND.vcPageNavName, '') AS [vcNodeName],
			ISNULL(vcNavURL, '') AS vcNavURL,
			ISNULL(PND.vcAddURL,''),
			ISNULL(PND.bitAddIsPopUp,0),
			ISNULL(TreeNodeOrder.numOrder,1000) AS numOrder,
			ISNULL(TNA.bitVisible,1) AS bitVisible,
			ISNULL(PND.vcImageURL,'')
		FROM      
			PageNavigationDTL PND
		LEFT JOIN 
			dbo.TreeNavigationAuthorization TNA 
		ON 
			TNA.numPageNavID = PND.numPageNavID AND
			TNA.numDomainID = @numDomainID AND 
			TNA.numGroupID = @numGroupID
			AND TNA.numTabID=@numTabID
		LEFT JOIN
			dbo.TreeNodeOrder 
		ON
			dbo.TreeNodeOrder.numTabID = @numTabID AND
			dbo.TreeNodeOrder.numDomainID = @numDomainID AND
			dbo.TreeNodeOrder.numGroupID = @numGroupID AND
			dbo.TreeNodeOrder.numPageNavID = PND.numPageNavID
		WHERE     
			ISNULL(PND.bitVisible, 0) = 1
			AND PND.numParentID <> 0
			AND numModuleID = @numModuleID

		UPDATE 
			t1
		SET 
			t1.numParentID = ISNULL((SELECT ID FROM @TEMP t2 WHERE t2.numPageNavID=t1.numParentID AND t2.numTabID=@numTabID),0)
		FROM
			@TEMP t1
		WHERE 
			numTabID=@numTabID

		INSERT INTO 
			@TEMP
		SELECT
			0,
			0 AS numParentID,
			ListDetails.numListItemID,
			@numTabID,
			0 AS numPageNavID,
			ListDetails.vcData AS [vcNodeName],
			'../prospects/frmCompanyList.aspx?RelId='+ convert(varchar(10),ListDetails.numListItemID),
			CASE WHEN ListDetails.numListItemID = 93 THEN '' ELSE CONCAT('~/include/frmAddOrganization.aspx?RelID=',ListDetails.numListItemID,'&FormID=36') END,
			CASE WHEN ListDetails.numListItemID = 93 THEN 0 ELSE 1 END,
			ISNULL(TreeNodeOrder.numOrder,2000) AS numOrder,
			ISNULL(dbo.TreeNodeOrder.bitVisible,1) AS bitVisible,
			''
		FROM
			ListDetails
		LEFT JOIN 
			dbo.TreeNodeOrder 
		ON
			dbo.TreeNodeOrder.numListItemID = ListDetails.numListItemID AND
			dbo.TreeNodeOrder.numParentID = 6 AND
			dbo.TreeNodeOrder.numTabID = @numTabID AND
			dbo.TreeNodeOrder.numDomainID = @numDomainID AND
			dbo.TreeNodeOrder.numGroupID = @numGroupID AND
			dbo.TreeNodeOrder.numPageNavID IS NULL
		WHERE
			numListID = 5 AND
			(ListDetails.numDomainID = @numDomainID  OR constFlag = 1) AND
			(ISNULL(bitDelete,0) = 0 OR constFlag = 1) AND
			ListDetails.numListItemID <> 46 

		INSERT INTO 
			@TEMP
		SELECT 
			0,
			(SELECT ID FROM @TEMP WHERE numListItemID = FRDTL.numPrimaryListItemID),
			L2.numListItemID,   
			@numTabID,
			0,
			L2.vcData AS [vcNodeName],
			CONCAT('../prospects/frmCompanyList.aspx?RelId=',FRDTL.numPrimaryListItemID,'&profileid=',numSecondaryListItemID),
			--CONCAT('~/include/frmAddOrganization.aspx?RelID=',FRDTL.numPrimaryListItemID,'&profileid=',numSecondaryListItemID,'&FormID=36'),
			CASE WHEN L2.numListItemID = 37257 THEN '' ELSE CONCAT('~/include/frmAddOrganization.aspx?RelID=',FRDTL.numPrimaryListItemID,'&profileid=',numSecondaryListItemID,'&FormID=36') END,
			--1,
			CASE WHEN L2.numListItemID = 37257 THEN 0 ELSE 1 END,
			ISNULL(TreeNodeOrder.numOrder,3000) AS numOrder,
			ISNULL(dbo.TreeNodeOrder.bitVisible,1) AS bitVisible,
			''
		FROM      
			FieldRelationship FR
		join 
			FieldRelationshipDTL FRDTL on FRDTL.numFieldRelID = FR.numFieldRelID
		join 
			ListDetails L1 on numPrimaryListItemID = L1.numListItemID
		join 
			ListDetails L2 on numSecondaryListItemID = L2.numListItemID
		LEFT JOIN 
			dbo.TreeNodeOrder 
		ON
			dbo.TreeNodeOrder.numListItemID = L2.numListItemID AND
			dbo.TreeNodeOrder.numParentID = FRDTL.numPrimaryListItemID AND
			dbo.TreeNodeOrder.numTabID = @numTabID AND
			dbo.TreeNodeOrder.numDomainID = @numDomainID AND
			dbo.TreeNodeOrder.numGroupID = @numGroupID AND
			dbo.TreeNodeOrder.numPageNavID IS NULL
		WHERE     
			numPrimaryListItemID <> 46
			and FR.numPrimaryListID = 5
			and FR.numSecondaryListID = 21
			and FR.numDomainID = @numDomainID
			and L2.numDomainID = @numDomainID

		INSERT INTO
			@TEMP
		SELECT 
			0,
			(SELECT ID FROM @TEMP WHERE numPageNavID=11),
			L2.numListItemID,
			@numTabID,
			11,
			L2.vcData AS [vcNodeName],
			'../prospects/frmProspectList.aspx?numProfile=' + CONVERT(VARCHAR(10), L2.numListItemID) AS vcNavURL,
			CONCAT('~/include/frmAddOrganization.aspx?RelID=3&profileid=',numSecondaryListItemID,'&FormID=35'),
			1,
			ISNULL(TreeNodeOrder.numOrder,4000) AS numOrder,
			ISNULL(dbo.TreeNodeOrder.bitVisible,1) AS bitVisible,
			''
		FROM    
			FieldRelationship AS FR
		INNER JOIN 
			FieldRelationshipDTL AS FRD ON FRD.numFieldRelID = FR.numFieldRelID
		INNER JOIN 
			ListDetails AS L1 ON L1.numListItemID = FRD.numPrimaryListItemID
		INNER JOIN 
			ListDetails AS L2 ON L2.numListItemID = FRD.numSecondaryListItemID
		LEFT JOIN 
			dbo.TreeNodeOrder 
		ON
			dbo.TreeNodeOrder.numListItemID = L2.numListItemID AND
			dbo.TreeNodeOrder.numParentID = 11 AND
			dbo.TreeNodeOrder.numTabID = @numTabID AND
			dbo.TreeNodeOrder.numDomainID = @numDomainID AND
			dbo.TreeNodeOrder.numGroupID = @numGroupID AND
			dbo.TreeNodeOrder.numPageNavID IS NULL
		WHERE 
			FRD.numPrimaryListItemID = 46
			AND FR.numDomainID = @numDomainID

		INSERT INTO
			@TEMP
		SELECT 
			0,
			(SELECT ID FROM @TEMP WHERE numPageNavID=12),
			L2.numListItemID,
			@numTabID,
			12,
			L2.vcData AS [vcNodeName],
			'../account/frmAccountList.aspx?numProfile=' + CONVERT(VARCHAR(10), L2.numListItemID) AS vcNavURL,
			CONCAT('~/include/frmAddOrganization.aspx?RelID=2&profileid=',numSecondaryListItemID,'&FormID=36'),
			1,
			ISNULL(TreeNodeOrder.numOrder,5000) AS numOrder,
			ISNULL(dbo.TreeNodeOrder.bitVisible,1) AS bitVisible,
			''
		FROM    
			FieldRelationship AS FR
		INNER JOIN 
			FieldRelationshipDTL AS FRD ON FRD.numFieldRelID = FR.numFieldRelID
		INNER JOIN 
			ListDetails AS L1 ON L1.numListItemID = FRD.numPrimaryListItemID
		INNER JOIN 
			ListDetails AS L2 ON L2.numListItemID = FRD.numSecondaryListItemID
		LEFT JOIN 
			dbo.TreeNodeOrder 
		ON
			dbo.TreeNodeOrder.numListItemID = L2.numListItemID AND
			dbo.TreeNodeOrder.numParentID = 12 AND
			dbo.TreeNodeOrder.numTabID = @numTabID AND
			dbo.TreeNodeOrder.numDomainID = @numDomainID AND
			dbo.TreeNodeOrder.numGroupID = @numGroupID AND
			dbo.TreeNodeOrder.numPageNavID IS NULL
		WHERE 
			FRD.numPrimaryListItemID = 46
			AND FR.numDomainID = @numDomainID

		INSERT INTO 
			@TEMP
		SELECT 
			0,
			(SELECT ID FROM @TEMP WHERE numListItemID=FRD.numPrimaryListItemID AND numPageNavID=11),
			L2.numListItemID,
			@numTabID,
			0,
			L2.vcData AS [vcNodeName],
			'../account/frmProspectList.aspx?numProfile=' + CONVERT(VARCHAR(10), FRD.numPrimaryListItemID) + '&numTerritoryID=' + CONVERT(VARCHAR(10), L2.numListItemID) AS vcNavURL,
			CONCAT('~/include/frmAddOrganization.aspx?RelID=3&profileid=',FRD.numPrimaryListItemID,'&FormID=35','&numTerritoryID=',L2.numListItemID),
			1,
			ISNULL(TreeNodeOrder.numOrder,7000) AS numOrder,
			ISNULL(dbo.TreeNodeOrder.bitVisible,1) AS bitVisible,
			''
		FROM    
			FieldRelationship AS FR
		INNER JOIN 
			FieldRelationshipDTL AS FRD ON FRD.numFieldRelID = FR.numFieldRelID
		INNER JOIN 
			ListDetails AS L1 ON L1.numListItemID = FRD.numPrimaryListItemID
		INNER JOIN 
			ListDetails AS L2 ON L2.numListItemID = FRD.numSecondaryListItemID
		LEFT JOIN 
			dbo.TreeNodeOrder 
		ON
			dbo.TreeNodeOrder.numListItemID = L2.numListItemID AND
			dbo.TreeNodeOrder.numParentID = 11 AND
			dbo.TreeNodeOrder.numTabID = @numTabID AND
			dbo.TreeNodeOrder.numDomainID = @numDomainID AND
			dbo.TreeNodeOrder.numGroupID = @numGroupID AND
			dbo.TreeNodeOrder.numPageNavID IS NULL
		WHERE   
			FRD.numPrimaryListItemID IN (SELECT numSecondaryListItemID FROM FieldRelationshipDTL FRDL JOIN FieldRelationship FRP ON FRDL.numFieldRelID = FRP.numFieldRelID WHERE numDomainID=@numDomainID AND numPrimaryListItemID=46)
			AND FR.numDomainID = @numDomainID

		INSERT INTO 
			@TEMP
		SELECT 
			0,
			(SELECT ID FROM @TEMP WHERE numListItemID=FRD.numPrimaryListItemID AND numPageNavID=12),
			L2.numListItemID,
			@numTabID,
			0,
			L2.vcData AS [vcNodeName],
			'../account/frmAccountList.aspx?numProfile=' + CONVERT(VARCHAR(10), FRD.numPrimaryListItemID) + '&numTerritoryID=' + CONVERT(VARCHAR(10), L2.numListItemID) AS vcNavURL,
			CONCAT('~/include/frmAddOrganization.aspx?RelID=2&profileid=',FRD.numPrimaryListItemID,'&FormID=36','&numTerritoryID=',L2.numListItemID),
			1,
			ISNULL(TreeNodeOrder.numOrder,8000) AS numOrder,
			ISNULL(dbo.TreeNodeOrder.bitVisible,1) AS bitVisible,
			''
		FROM    
			FieldRelationship AS FR
		INNER JOIN 
			FieldRelationshipDTL AS FRD ON FRD.numFieldRelID = FR.numFieldRelID
		INNER JOIN 
			ListDetails AS L1 ON L1.numListItemID = FRD.numPrimaryListItemID
		INNER JOIN 
			ListDetails AS L2 ON L2.numListItemID = FRD.numSecondaryListItemID
		LEFT JOIN 
			dbo.TreeNodeOrder 
		ON
			dbo.TreeNodeOrder.numListItemID = L2.numListItemID AND
			dbo.TreeNodeOrder.numParentID = 12 AND
			dbo.TreeNodeOrder.numTabID = @numTabID AND
			dbo.TreeNodeOrder.numDomainID = @numDomainID AND
			dbo.TreeNodeOrder.numGroupID = @numGroupID AND
			dbo.TreeNodeOrder.numPageNavID IS NULL
		WHERE   
			FRD.numPrimaryListItemID IN (SELECT numSecondaryListItemID FROM FieldRelationshipDTL FRDL JOIN FieldRelationship FRP ON FRDL.numFieldRelID = FRP.numFieldRelID WHERE numDomainID=@numDomainID AND numPrimaryListItemID=46)
			AND FR.numDomainID = @numDomainID

		INSERT INTO
			@TEMP
		SELECT 
			0,
			(SELECT ID FROM @TEMP WHERE numPageNavID=13),
			listdetails.numListItemID,
			@numTabID,
			0,
			listdetails.vcData AS [vcNodeName],
			'../contact/frmContactList.aspx?ContactType=' + CONVERT(VARCHAR(10), listdetails.numListItemID) AS vcNavURL,
		   '',
		   0,
			ISNULL(TreeNodeOrder.numOrder,6000) AS numOrder,
			ISNULL(dbo.TreeNodeOrder.bitVisible,1) AS bitVisible,
			''
		FROM    
			listdetails
		LEFT JOIN 
			dbo.TreeNodeOrder 
		ON
			dbo.TreeNodeOrder.numListItemID = listdetails.numListItemID AND
			dbo.TreeNodeOrder.numParentID = 13 AND
			dbo.TreeNodeOrder.numTabID = @numTabID AND
			dbo.TreeNodeOrder.numDomainID = @numDomainID AND
			dbo.TreeNodeOrder.numGroupID = @numGroupID AND
			dbo.TreeNodeOrder.numPageNavID IS NULL
		WHERE   
			numListID = 8
			AND (constFlag = 1 OR listdetails.numDomainID = @numDomainID)

		INSERT INTO
			@TEMP
		SELECT 
			0,
			(SELECT ID FROM @TEMP WHERE numPageNavID=13),
			101 AS numListItemID,
			@numTabID,
			0,
			'Primary Contact' AS [vcNodeName],
			'../Contact/frmcontactList.aspx?ContactType=101',
			'',
			0,
			ISNULL((
						SELECT 
							numOrder 
						FROM 
							dbo.TreeNodeOrder 
						WHERE
							dbo.TreeNodeOrder.numListItemID = 101 AND
							dbo.TreeNodeOrder.numParentID = 13 AND
							dbo.TreeNodeOrder.numTabID = @numTabID AND
							dbo.TreeNodeOrder.numDomainID = @numDomainID AND
							dbo.TreeNodeOrder.numGroupID = @numGroupID AND
							dbo.TreeNodeOrder.numPageNavID IS NULL
					),7000) AS numOrder,
			ISNULL((
						SELECT 
							bitVisible 
						FROM 
							dbo.TreeNodeOrder 
						WHERE
							dbo.TreeNodeOrder.numListItemID = 101 AND
							dbo.TreeNodeOrder.numParentID = 13 AND
							dbo.TreeNodeOrder.numTabID = @numTabID AND
							dbo.TreeNodeOrder.numDomainID = @numDomainID AND
							dbo.TreeNodeOrder.numGroupID = @numGroupID AND
							dbo.TreeNodeOrder.numPageNavID IS NULL
					),1) AS bitVisible,
					''

	END

	BEGIN /******************** DOCUMENTS ***********************/
		SET @numTabID = 8

		INSERT INTO 
			@TEMP
		SELECT 
			0,
			0,
			0,
			@numTabID,
			0,
			'Regular Documents' as vcPageNavName,
			'../Documents/frmRegularDocList.aspx?Category=0',
			'~/Documents/frmGenDocPopUp.aspx',
			1,
			1,
			1,
			''


		INSERT INTO 
			@TEMP
		SELECT  
			0,
			SCOPE_IDENTITY(),
			Ld.numListItemId,
			@numTabID,
			0,
			vcData vcPageNavName,
			'../Documents/frmRegularDocList.aspx?Category=' + cast(Ld.numListItemId as varchar(10)),
			CASE WHEN vcData = 'Email Template' THEN '~/Marketing/frmEmailTemplate.aspx' ELSE '' END,
			CASE WHEN vcData = 'Email Template' THEN 1 ELSE 0 END,
			ISNULL(intSortOrder, LD.sintOrder) SortOrder,
			1,
			''
		FROM    
			ListDetails LD
		LEFT JOIN 
			ListOrder LO 
		ON 
			LD.numListItemID = LO.numListItemID
			AND LO.numDomainId = @numDomainID
		WHERE   
			Ld.numListID = 29
			AND (constFlag = 1 OR Ld.numDomainID = @numDomainID)

	END

	BEGIN /******************** ACCOUNTING **********************/
	
		SET @numTabID = 45
		SELECT TOP 1 @numModuleID = numModuleID FROM dbo.PageNavigationDTL WHERE numTabID = @numTabID

		INSERT INTO 
			@TEMP
		SELECT 
		    ISNULL(PND.numParentID,0),
			PND.numParentID,
			0,
			@numTabID,
			PND.numPageNavID,
			vcPageNavName,
			ISNULL(vcNavURL,'') AS vcNavURL,
			ISNULL(PND.vcAddURL,''),
			ISNULL(PND.bitAddIsPopUp,0),
			0,
			1 ,
			ISNULL(PND.vcImageURL,'')     
		FROM 
			PageNavigationDTL PND
		JOIN 
			dbo.TreeNavigationAuthorization TNA 
		ON 
			PND.numPageNavID = TNA.numPageNavID
			AND TNA.numTabID=@numTabID
		WHERE  
			ISNULL(TNA.bitVisible, 1) = 1
			AND ISNULL(PND.bitVisible, 1) = 1
			AND numModuleID=@numModuleID
			AND TNA.numDomainID = @numDomainID
			AND numGroupID = @numGroupID
			AND numParentID <> 0
		ORDER BY 
			numParentID,PND.numPageNavID 
	
		INSERT INTO 
			@TEMP
		SELECT 
			0,
			(CASE numFinancialViewID 
				WHEN 26770 THEN 96 /*Profit & Loss*/
				WHEN 26769 THEN 176 /*Income & Expense*/
				WHEN 26768 THEN 98 /*Cash Flow Statement*/
				WHEN 26767 THEN 97 /*Balance Sheet*/
				WHEN 26766 THEN 81 /*A/R & A/P Aging*/
				WHEN 26766 THEN 82 /*A/R & A/P Aging*/
				ELSE 0
			END),
			numFRID,
			@numTabID,
			0,
			vcReportName,
			CASE numFinancialViewID 
				WHEN 26770 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =96 AND numModuleID=35)  /*Profit & Loss*/
				WHEN 26769 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =176 AND numModuleID=35) /*Income & Expense*/
				WHEN 26768 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =98 AND numModuleID=35) /*Cash Flow Statement*/
				WHEN 26767 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =97 AND numModuleID=35) /*Balance Sheet*/
				WHEN 26766 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =81 AND numModuleID=35) /*A/R & A/P Aging*/
				WHEN 26766 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =82 AND numModuleID=35) /*A/R & A/P Aging*/
				ELSE ''
			END + 
			CASE CHARINDEX('?',(CASE numFinancialViewID 
				WHEN 26770 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =96 AND numModuleID=35)  /*Profit & Loss*/
				WHEN 26769 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =176 AND numModuleID=35) /*Income & Expense*/
				WHEN 26768 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =98 AND numModuleID=35) /*Cash Flow Statement*/
				WHEN 26767 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =97 AND numModuleID=35) /*Balance Sheet*/
				WHEN 26766 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =81 AND numModuleID=35) /*A/R & A/P Aging*/
				WHEN 26766 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =82 AND numModuleID=35) /*A/R & A/P Aging*/
				ELSE ''
			END)) 
				WHEN 0 THEN '?FRID='
				ELSE '&FRID='
			END + CAST(numFRID AS VARCHAR),
			'',
			0,
			0,
			1,
			''
		FROM 
			dbo.FinancialReport 
		WHERE 
			numDomainID=@numDomainID
	
		UPDATE 
			t1
		SET 
			t1.numParentID = ISNULL((SELECT ID FROM @TEMP t2 WHERE t2.numPageNavID=t1.numParentID AND t2.numTabID=@numTabID),0)
		FROM
			@TEMP t1
		WHERE 
			numTabID=@numTabID
	  
	END

	BEGIN /******************** ITEMS **********************/

		SET @numTabID = 80
		SELECT TOP 1 @numModuleID = numModuleID FROM dbo.PageNavigationDTL WHERE numTabID = @numTabID 	

		INSERT INTO
			@TEMP
		SELECT 
			ISNULL(PND.numParentID,0),
			numParentID,
			0,
			@numTabID,
			PND.numPageNavID,
			vcPageNavName,
			isnull(vcNavURL, '') as vcNavURL,
			ISNULL(PND.vcAddURL,''),
			ISNULL(PND.bitAddIsPopUp,0),
			0,
			1,
			ISNULL(PND.vcImageURL,'')
		FROM 
			PageNavigationDTL PND
		JOIN 
			dbo.TreeNavigationAuthorization TNA 
		ON 
			PND.numPageNavID = TNA.numPageNavID
			AND TNA.numTabID=@numTabID
		WHERE
			ISNULL(TNA.bitVisible, 0) = 1
			AND ISNULL(PND.bitVisible, 0) = 1
			AND PND.numModuleID = @numModuleID
			AND numGroupID = @numGroupID
			AND numParentID <> 0
		ORDER BY 
			numParentID,
			intSortOrder,
			numPageNavID

		UPDATE 
			t1
		SET 
			t1.numParentID = ISNULL((SELECT ID FROM @TEMP t2 WHERE t2.numPageNavID=t1.numParentID AND t2.numTabID=@numTabID),0)
		FROM
			@TEMP t1
		WHERE 
			numTabID=@numTabID
	END

	BEGIN /******************** Activities ***************************/

		SET @numTabID = 36
		SELECT TOP 1 @numModuleID = numModuleID FROM dbo.PageNavigationDTL WHERE numTabID = @numTabID 	

		INSERT INTO
			@TEMP
		SELECT 
			ISNULL(PND.numParentID,0),
			PND.numParentID,
			0,
			@numTabID,
			PND.numPageNavID AS numPageNavID,
			vcPageNavName,
			ISNULL(vcNavURL, '') as vcNavURL,
			ISNULL(PND.vcAddURL,''),
			ISNULL(PND.bitAddIsPopUp,0),
			0,
			1,
			ISNULL(PND.vcImageURL,'')
		FROM   
			PageNavigationDTL PND
		LEFT JOIN 
			dbo.TreeNavigationAuthorization TNA 
		ON 
			PND.numPageNavID = TNA.numPageNavID
			AND TNA.numTabID=@numTabID
		WHERE 
			ISNULL(TNA.bitVisible, 1) = 1
			AND ISNULL(PND.bitVisible, 1) = 1
			AND PND.numParentID <> 0
			AND PND.numModuleID = @numModuleID
			AND numGroupID = @numGroupID
		ORDER BY 
			numParentID,
			intSortOrder,
			numPageNavID

		UPDATE 
			t1
		SET 
			t1.numParentID = ISNULL((SELECT ID FROM @TEMP t2 WHERE t2.numPageNavID=t1.numParentID AND t2.numTabID=@numTabID),0)
		FROM
			@TEMP t1
		WHERE 
			numTabID=@numTabID

	END


	;WITH CTE(ID) AS 
	(
		SELECT
			ID
		FROM
			@TEMP t1
		WHERE 
			ISNULL(numOrigParentID,0) > 0
			AND numOrigParentID NOT IN (SELECT numPageNavID FROM PageNavigationDTL WHERE numParentID=0 AND numTabID=t1.numTabID)
			AND numOrigParentID NOT IN (SELECT numPageNavID FROM @TEMP t2 WHERE t2.numTabID = t1.numTabID)
		UNION ALL
		SELECT 
			t2.ID
		FROM
			@TEMP t2
		JOIN
			CTE c
		ON
			t2.numParentID=c.ID
	)

	DELETE FROM @TEMP WHERE ID IN (SELECT ID FROM CTE)

	
	/** Admin Menu **/
	INSERT INTO 
		@TEMP
	SELECT 
		numParentID,
		numParentID,
		0,
		-1,
		PND.numPageNavID,
		vcPageNavName,
		ISNULL(vcNavURL, '') as vcNavURL,
		'',
		0,
		ISNULL(PND.intSortOrder,0) sintOrder,
		1,
		ISNULL(vcImageURL,'')
	FROM   
		PageNavigationDTL PND
	JOIN 
		dbo.TreeNavigationAuthorization TNA 
	ON 
		PND.numPageNavID = TNA.numPageNavID
		AND PND.numTabID = TNA.numTabID
	WHERE
		ISNULL(TNA.bitVisible, 0) = 1
		AND ISNULL(PND.bitVisible, 0) = 1
		AND PND.numModuleID = 13 --AND bitVisible=1  
		AND TNA.numDomainID = @numDomainID
		AND TNA.numGroupID = @numGroupID
	ORDER BY
		PND.numParentID, 
		PND.intSortOrder,
		(CASE WHEN PND.numPageNavID=79 THEN 2000 ELSE 0 END),	
		numPageNavID   

	-- MAKE FIRST LEVEL CHILD AS PARENT BECUASE WE ARE NOW NOT USING TREE STRUCTURE AND DIPLAYING ITEMS IN MENU
	UPDATE 
		@TEMP
	SET 
		numParentID = 0
	WHERE
		numTabID = -1
		AND numPageNavID = (SELECT numPageNavID FROM @TEMP WHERE numTabID=-1 AND numParentID = 0)

	-- REMOVE ALL CHILD WHOSE PARENT IS HIDDEN
	DELETE FROM @TEMP WHERE numTabID = -1 AND ISNULL(numPageNavID,0) <> 0 AND numParentID NOT IN (SELECT numPageNavID FROM @TEMP WHERE numTabID = -1)

	-- REMOVE ALL CHILD WHOSE PARENT IS HIDDEN
	DELETE FROM @TEMP WHERE numParentID = 0 AND numTabID=-1

	-- UPDATE PARENT DETAIL
	UPDATE 
		t1
	SET 
		t1.numParentID = ISNULL((SELECT ID FROM @TEMP t2 WHERE t2.numTabID=-1 AND t2.numPageNavID=t1.numParentID),0)
	FROM
		@TEMP t1
	WHERE
		t1.numTabID = -1


	/** Advance Search **/
	INSERT INTO 
		@TEMP
	SELECT 
		numParentID,
		numParentID,
		0,
		-3,
		PND.numPageNavID,
		vcPageNavName,
		ISNULL(vcNavURL, '') as vcNavURL,
		'',
		0,
		ISNULL(PND.intSortOrder,0) sintOrder,
		1,
		ISNULL(vcImageURL,'')
	FROM   
		PageNavigationDTL PND
	JOIN 
		dbo.TreeNavigationAuthorization TNA 
	ON 
		PND.numPageNavID = TNA.numPageNavID
		AND PND.numTabID = TNA.numTabID
	WHERE
		ISNULL(TNA.bitVisible, 0) = 1
		AND ISNULL(PND.bitVisible, 0) = 1
		AND PND.numModuleID = 9
		AND TNA.numDomainID = @numDomainID
		AND TNA.numGroupID = @numGroupID
	ORDER BY
		PND.numParentID, 
		sintOrder,
		numPageNavID   

	-- MAKE FIRST LEVEL CHILD AS PARENT BECUASE WE ARE NOW NOT USING TREE STRUCTURE AND DIPLAYING ITEMS IN MENU
	UPDATE 
		@TEMP
	SET 
		numParentID = 0
	WHERE
		numTabID = -3
		AND numPageNavID = (SELECT numPageNavID FROM @TEMP WHERE numTabID=-3 AND numParentID = 0)

	-- REMOVE ALL CHILD WHOSE PARENT IS HIDDEN
	DELETE FROM @TEMP WHERE numTabID = -3 AND ISNULL(numPageNavID,0) <> 0 AND numParentID NOT IN (SELECT numPageNavID FROM @TEMP WHERE numTabID = -3)

	-- REMOVE ALL CHILD WHOSE PARENT IS HIDDEN
	DELETE FROM @TEMP WHERE numParentID = 0 AND numTabID=-3

	-- UPDATE PARENT DETAIL
	UPDATE 
		t1
	SET 
		t1.numParentID = ISNULL((SELECT ID FROM @TEMP t2 WHERE t2.numTabID=-3 AND t2.numPageNavID=t1.numParentID),0)
	FROM
		@TEMP t1
	WHERE
		t1.numTabID = -3

	SELECT * FROM @TEMP WHERE bitVisible=1 ORDER BY numTabID, numParentID, [order]
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetPromotionDTLForCartItems')
	DROP PROCEDURE USP_GetPromotionDTLForCartItems
GO
CREATE PROCEDURE [dbo].[USP_GetPromotionDTLForCartItems]  
	@numDomainID AS NUMERIC(18,0),
	@itemIdSearch AS VARCHAR(100)
AS
BEGIN

	DECLARE @strSQL VARCHAR(1000)

	SET @strSQL = 'IF((SELECT COUNT(*) FROM PromotionOfferOrderBased WHERE cSaleType=''I'' AND numProItemId IN (' + @itemIdSearch + ')) > 0) '
				+ 'BEGIN '
					+ 'SELECT * FROM PromotionOfferOrderBased WHERE cSaleType=''I'' AND numProItemId IN (' + @itemIdSearch + ') '
				+ 'END'

	PRINT @strSQL
	EXEC (@strSQL) 
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetPromotionOffer')
DROP PROCEDURE USP_GetPromotionOffer
GO
CREATE PROCEDURE [dbo].[USP_GetPromotionOffer]
    @numProId AS NUMERIC(9) = 0,
    @byteMode AS TINYINT = 0,
    @numDomainID AS NUMERIC(9) = 0
AS 
BEGIN
    IF @byteMode = 0 
    BEGIN    
		SELECT  
			[numProId]
			,[vcProName]
			,[numDomainId]
			,[dtValidFrom]
			,[dtValidTo]
			,[bitNeverExpires]
			,[bitApplyToInternalOrders]
			,[bitAppliesToSite]
			,[bitRequireCouponCode]
			,[txtCouponCode]
			,[tintUsageLimit]
			--,[bitFreeShiping]
			--,[monFreeShippingOrderAmount]
			--,[numFreeShippingCountry]
			,[bitDisplayPostUpSell]
			,[intPostSellDiscount]
			--,[bitFixShipping1]
			--,[monFixShipping1OrderAmount]
			--,[monFixShipping1Charge]
			--,[bitFixShipping2]
			--,[monFixShipping2OrderAmount]
			--,[monFixShipping2Charge]
			,[tintOfferTriggerValueType]
			,[fltOfferTriggerValue]
			,[tintOfferBasedOn]
			,[fltDiscountValue]
			,[tintDiscountType]
			,[tintDiscoutBaseOn]
			,[numCreatedBy]
			,[dtCreated]
			,[numModifiedBy]
			,[dtModified]
			,[tintCustomersBasedOn]
			,[tintOfferTriggerValueTypeRange]
			,[fltOfferTriggerValueRange]
			,[IsOrderBasedPromotion]
        FROM 
			PromotionOffer
        WHERE 
			numProId = @numProId
    END    
    ELSE IF @byteMode = 1
    BEGIN    
        SELECT  
			numProId,
            vcProName,
			(CASE WHEN ISNULL(bitNeverExpires,0) = 1 THEN 'Promotion Never Expires' ELSE CONCAT(dbo.FormatedDateFromDate(dtValidFrom,@numDomainID),' to ',dbo.FormatedDateFromDate(dtValidTo,@numDomainID)) END) AS vcDateValidation,
			(CASE WHEN ISNULL(bitRequireCouponCode,0) = 1 THEN CONCAT(txtCouponCode,' (', ISNULL(intCouponCodeUsed,0),')') ELSE '' END) vcCouponCode
			--,CONCAT
			--(
			--	(CASE WHEN ISNULL(bitFixShipping1,0)=1 THEN CONCAT('Spend $',monFixShipping1OrderAmount,' and get $',monFixShipping1Charge,' shipping. ') ELSE '' END)
			--	,(CASE WHEN ISNULL(bitFixShipping2,0)=1  THEN CONCAT('Spend $',monFixShipping2OrderAmount,' and get $',monFixShipping2Charge,' shipping. ') ELSE '' END)
				--,(CASE WHEN bitFreeShiping=1 THEN CONCAT('Spend $',monFreeShippingOrderAmount,' and shipping is FREE if shipping country is ',ISNULL((SELECT vcData FROM ListDetails WHERE numListID=40 AND numListItemID=numFreeShippingCountry),''),'.') ELSE '' END) 
			--) AS vcFreeShippingTerms
			,(CASE WHEN ISNULL(bitApplyToInternalOrders,0) = 1 THEN 'Yes' ELSE 'No' END) AS vcInternalOrders
			,(CASE WHEN ISNULL(bitAppliesToSite,0)=1 THEN (SELECT Stuff((SELECT CONCAT(', ',Sites.vcSiteName) FROM PromotionOfferSites POS INNER JOIN Sites ON pos.numSiteID=Sites.numSiteID WHERE POS.numPromotionID=PO.numProId FOR XML PATH('')), 1, 2, '')) ELSE '' END) AS vcSites
			,CONCAT('Buy '
					,CASE WHEN tintOfferTriggerValueType=1 THEN CAST(fltOfferTriggerValue AS VARCHAR) ELSE CONCAT('$',fltOfferTriggerValue) END
					,CASE tintOfferBasedOn
							WHEN 1 THEN CONCAT(' of "',(SELECT Stuff((SELECT CONCAT(', ',Item.vcItemName) FROM PromotionOfferItems INNER JOIN Item ON PromotionOfferItems.numValue = Item.numItemCode WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 FOR XML PATH('')), 1, 2, '')), '"')
							WHEN 2 THEN CONCAT(' of items belonging to classification(s) "',(SELECT Stuff((SELECT CONCAT(', ',ListDetails.vcData) FROM PromotionOfferItems INNER JOIN ListDetails ON PromotionOfferItems.numValue = ListDetails.numListItemID WHERE numListID=36 AND numProId=PO.numProId AND tintRecordType=5 AND tintType=2 FOR XML PATH('')), 1, 2, '')), '"')
						END
					,' & get '
					, CASE tintDiscountType WHEN 1 THEN CONCAT(fltDiscountValue,'% off on') WHEN 2 THEN CONCAT('$',fltDiscountValue, ' off on') WHEN 3 THEN CONCAT(fltDiscountValue,' of') END
					, CASE 
						WHEN tintDiscoutBaseOn = 1 THEN 
							CONCAT
							(
								CONCAT(' "',(SELECT Stuff((SELECT CONCAT(', ',Item.vcItemName) FROM PromotionOfferItems INNER JOIN Item ON PromotionOfferItems.numValue = Item.numItemCode WHERE numProId=PO.numProId AND tintRecordType=6 AND tintType=1 FOR XML PATH('')), 1, 2, '')), '"')
								,(CASE WHEN PO.tintDiscountType = 3 THEN ' free' ELSE '' END)
								,'.'
							)
						WHEN tintDiscoutBaseOn = 2 THEN 
							CONCAT
							(
								CONCAT(' items belonging to classification(s) "',(SELECT Stuff((SELECT CONCAT(', ',ListDetails.vcData) FROM PromotionOfferItems INNER JOIN ListDetails ON PromotionOfferItems.numValue = ListDetails.numListItemID WHERE numListID=36 AND numProId=PO.numProId AND tintRecordType=6 AND tintType=2 FOR XML PATH('')), 1, 2, '')), '"')
								,(CASE WHEN PO.tintDiscountType = 3 THEN ' free' ELSE '' END)
								,'.'
							)
						WHEN tintDiscoutBaseOn = 3 THEN ' related items.'
						ELSE '' 
					END
				) AS vcPromotionRule,
				(CASE WHEN (SELECT COUNT(*) FROM OpportunityItems JOIN OpportunityMaster ON OpportunityItems.numOppId=OpportunityMaster.numOppId WHERE numDomainId=@numDomainID AND numPromotionID=PO.numProId) > 0 THEN 0 ELSE 1 END) AS bitCanEdit
				,bitEnabled, tintCustomersBasedOn, tintOfferTriggerValueTypeRange, fltOfferTriggerValueRange, [IsOrderBasedPromotion]
        FROM  
			PromotionOffer PO
		WHERE 
			numDomainID = @numDomainID
		ORDER BY
			dtCreated DESC
    END
END      
  
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetPromotionOfferForIncentiveSaleItems')
	DROP PROCEDURE USP_GetPromotionOfferForIncentiveSaleItems
GO
CREATE PROCEDURE [dbo].[USP_GetPromotionOfferForIncentiveSaleItems]  
	@numDomainID AS NUMERIC(18,0),
	@monTotAmtBefDiscount FLOAT,
	@itemIdSearch AS VARCHAR(100)
AS
BEGIN
	/*DECLARE @strSQL VARCHAR(1000), @strSQL1 VARCHAR(1000)

	-- CHECKING IF ORDER BASED PROMOTION = FALSE THEN RETURN

	SET @strSQL1 = 'SELECT numProID FROM PromotionOfferItems WHERE tintRecordType = 5 AND numValue IN (' + @itemIdSearch + ') '	

	CREATE TABLE #tempIsOrderBased
	( 
		ID INT IDENTITY (1, 1) NOT NULL, 
		numProID NUMERIC(18,0)
	)

	INSERT INTO #tempIsOrderBased
		EXEC (@strSQL1) 

	IF(SELECT COUNT(*) FROM PromotionOffer WHERE numProId IN (SELECT numProID FROM #tempIsOrderBased) AND numdomainid = @numDomainID AND IsOrderBasedPromotion = 1 ) = 0
	BEGIN
		SELECT 0 AS SubTotal
		DROP TABLE #tempIsOrderBased
		RETURN
	END */

	IF(SELECT COUNT(*) FROM PromotionOffer WHERE numdomainid = @numDomainID AND IsOrderBasedPromotion = 1 ) = 0
	BEGIN
		SELECT 0 AS SubTotal
		RETURN
	END 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	-- FETCHING MAX(SUBTOTAL) TO BE CONSIDERED

	--SET @strSQL = 'IF((SELECT COUNT(*) FROM PromotionOfferOrderBased WHERE cSaleType=''I'' AND numDomainId = ' + CAST(@numDomainID AS VARCHAR(100)) + ' AND numProItemId IN (' + @itemIdSearch + ')) > 0) '
	--			+ 'BEGIN '
	--			+	'SELECT * FROM PromotionOfferOrderBased WHERE cSaleType=''I'' AND numDomainId = ' + CAST(@numDomainID AS VARCHAR(100)) + ' AND numProItemId IN (' + @itemIdSearch + ') '
	--			+ 'END'

	--PRINT @strSQL
	--EXEC (@strSQL) 

	DECLARE @strSQL VARCHAR(1000)

	SET @strSQL = 'IF((SELECT COUNT(*) FROM PromotionOfferOrderBased WHERE cSaleType=''I'' AND numDomainId = ' + CAST(@numDomainID AS VARCHAR(100)) + ' ) > 0) '
				+ 'BEGIN '
				+	'SELECT * FROM PromotionOfferOrderBased WHERE cSaleType=''I'' AND numDomainId = ' + CAST(@numDomainID AS VARCHAR(100)) + '  '
				+ 'END'

	CREATE TABLE #tempPromotionOrderBased 
	( 
		ID INT IDENTITY (1, 1) NOT NULL, 
		numProOrderBasedID NUMERIC(18,0), 
		numProID NUMERIC(18,0), 
		numDomainID NUMERIC(18,0), 
		numProItemID NUMERIC(18,0), 
		fltSubTotal  FLOAT,
		cSaleType CHAR,
		cItems CHAR 
	)

	INSERT INTO #tempPromotionOrderBased
		EXEC (@strSQL) 

	--select * from #temp
	--drop table #temp

	IF(SELECT COUNT(*) from #tempPromotionOrderBased) > 0
	BEGIN
		
		DECLARE @max FLOAT 

		--IF (SELECT COUNT(*) from #tempPromotionOrderBased) > 1
		--BEGIN

			CREATE TABLE #temp 
			(
				ID int IDENTITY (1, 1) NOT NULL,
				fltSubTotal  FLOAT,
				numProID NUMERIC(18,0)
			)

			INSERT INTO #temp
				(fltSubTotal, numProID)
			SELECT fltSubTotal, numProID FROM #tempPromotionOrderBased

			DECLARE @Id INT, @ProID NUMERIC(18,0)

			WHILE (SELECT COUNT(*) FROM #temp) > 0
			BEGIN
				--SELECT @Id = Id, @max = MAX(fltSubTotal) FROM #temp GROUP BY Id

				SELECT @max =  MAX(fltSubTotal) FROM #temp 
				SELECT @Id = Id, @ProID = numProID FROM #temp WHERE fltSubTotal = @max

				IF(@monTotAmtBefDiscount >= @max)
				BEGIN
					SELECT @max AS SubTotal, @ProID AS PromotionID
					RETURN
					DROP TABLE #temp
				END

				DELETE #temp WHERE Id = @Id
			END
			SELECT -1 AS SubTotal
			DROP TABLE #temp
			DROP TABLE #tempPromotionOrderBased
		--END
		--ELSE
		--BEGIN

		--	SELECT @max = fltSubTotal FROM PromotionOfferOrderBased WHERE numProId = @numProId AND numDomainId = @numDomainID AND cSaleType = 'I' AND numProItemId = @numItemId
		--	IF(@monTotAmtBefDiscount >= @max)
		--	BEGIN
		--		SELECT @max
		--		RETURN
		--	END
		--	ELSE
		--	BEGIN
		--		RETURN 0
		--	END
		--END
	END
	ELSE
	BEGIN
		SELECT 0 AS SubTotal
		RETURN
	END





	/*IF(SELECT COUNT(*) from PromotionOfferOrderBased WHERE numProId = @numProId AND numDomainId = @numDomainID AND cSaleType = 'I' AND numProItemId = @numItemId) > 0
	BEGIN
		
		DECLARE @max FLOAT 

		IF (SELECT COUNT(*) from PromotionOfferOrderBased WHERE numProId = @numProId AND numDomainId = @numDomainID AND cSaleType = 'I' AND numProItemId = @numItemId) > 1
		BEGIN

			CREATE TABLE #temp 
			(
				ID int IDENTITY (1, 1) NOT NULL,
				fltSubTotal  FLOAT
			)

			INSERT INTO #temp
			(fltSubTotal)
			SELECT fltSubTotal FROM PromotionOfferOrderBased WHERE numProId = @numProId AND numDomainId = @numDomainID AND cSaleType = 'I' AND numProItemId = @numItemId

			DECLARE @Id INT

			WHILE (SELECT COUNT(*) FROM #temp) > 0
			BEGIN
				SELECT @Id = Id, @max = MAX(fltSubTotal) FROM #temp GROUP BY Id
				
				IF(@monTotAmtBefDiscount >= @max)
				BEGIN
					SELECT @max
					RETURN
					DROP TABLE #temp
				END

				DELETE #temp WHERE Id = @Id
			END
		END
		ELSE
		BEGIN

			SELECT @max = fltSubTotal FROM PromotionOfferOrderBased WHERE numProId = @numProId AND numDomainId = @numDomainID AND cSaleType = 'I' AND numProItemId = @numItemId
			IF(@monTotAmtBefDiscount >= @max)
			BEGIN
				SELECT @max
				RETURN
			END
			ELSE
			BEGIN
				RETURN 0
			END
		END
	END
	ELSE
	BEGIN
		RETURN 0
	END*/
END





	/*DECLARE @vcItemName VARCHAR(200), @numBaseUnit VARCHAR(100), @monListPrice VARCHAR(100)

	SELECT vcProName FROM PromotionOffer WHERE numProId = @numProId AND numDomainId = @numDomainID 

	CREATE TABLE #temp 
	(
		ID int IDENTITY (1, 1) NOT NULL ,
		numProOrderBasedID numeric(18,0),
		numProId numeric(18,0),
		numDomainID NUMERIC(18,0),
		cSaleType  CHAR,
		cItems  CHAR,
		fltSubTotal  FLOAT
	)

	CREATE TABLE #temptbl 
	(
		ID int IDENTITY (1, 1) NOT NULL ,
		numProOrderBasedID numeric(18,0),
		numProId numeric(18,0),
		numDomainID NUMERIC(18,0),
		cSaleType  CHAR,
		cItems  CHAR,
		fltSubTotal  FLOAT,
		Items VARCHAR(200),
		numBaseUnit VARCHAR(100),
		monListPrice  VARCHAR(100)
	)

	Declare @Id int, @tot float

	insert into #temp
		(numProOrderBasedID, numProId, numDomainID, cSaleType, cItems, fltSubTotal)
	(select numProOrderBasedID, numProId, numDomainID, cSaleType, cItems, fltSubTotal FROM PromotionOfferOrderBased WHERE numProId = @numProId AND numDomainId = @numDomainID )

	insert into #temptbl
		(numProOrderBasedID, numProId, numDomainID, cSaleType, cItems, fltSubTotal)
	(select numProOrderBasedID, numProId, numDomainID, cSaleType, cItems, fltSubTotal FROM PromotionOfferOrderBased WHERE numProId = @numProId AND numDomainId = @numDomainID )
	
		While (Select Count(*) FROM #temp WHERE cSaleType = 'I' AND cItems = 'M') > 0
		BEGIN
			
			Select Top 1  @Id = Id, @tot = fltSubTotal From #temp WHERE cSaleType = 'I' AND cItems = 'M'

			SET @vcItemName = CONCAT('',(SELECT Stuff((SELECT CONCAT(', ',Item.vcItemName) FROM PromotionOfferOrderBased INNER JOIN Item ON PromotionOfferOrderBased.numProItemId = Item.numItemCode WHERE numProId = @numProId AND PromotionOfferOrderBased.numDomainID = @numDomainID AND cItems = 'M' AND fltSubTotal = @tot FOR XML PATH('')), 1, 2, '')), '') 
			SET @numBaseUnit = CONCAT('',(SELECT Stuff((SELECT CONCAT(', ',ISNULL(Item.numBaseUnit,0)) FROM PromotionOfferOrderBased INNER JOIN Item ON PromotionOfferOrderBased.numProItemId = Item.numItemCode WHERE numProId = @numProId AND PromotionOfferOrderBased.numDomainID = @numDomainID AND cItems = 'M' AND fltSubTotal = @tot FOR XML PATH('')), 1, 2, '')), '') 
			SET @monListPrice = CONCAT('',(SELECT Stuff((SELECT CONCAT(', ',ISNULL(Item.monListPrice,0)) FROM PromotionOfferOrderBased INNER JOIN Item ON PromotionOfferOrderBased.numProItemId = Item.numItemCode WHERE numProId = @numProId AND PromotionOfferOrderBased.numDomainID = @numDomainID AND cItems = 'M' AND fltSubTotal = @tot FOR XML PATH('')), 1, 2, '')), '') 
	
			UPDATE #temptbl
				SET Items = @vcItemName, numBaseUnit = @numBaseUnit, monListPrice = @monListPrice
			WHERE Id = @Id

			Delete #temp Where Id = @Id
		END
		While (Select Count(*) FROM #temp WHERE cSaleType = 'I' AND cItems = 'S') > 0
		BEGIN
			Select Top 1  @Id = Id,  @tot = fltSubTotal From #temp WHERE cSaleType = 'I' AND cItems = 'S'

			SELECT @vcItemName = Item.vcItemName FROM PromotionOfferOrderBased INNER JOIN Item ON PromotionOfferOrderBased.numProItemId = Item.numItemCode WHERE numProId = @numProId AND PromotionOfferOrderBased.numDomainID = @numDomainID AND cItems = 'S' AND fltSubTotal = @tot
			SELECT @numBaseUnit = ISNULL(Item.numBaseUnit,0) FROM PromotionOfferOrderBased INNER JOIN Item ON PromotionOfferOrderBased.numProItemId = Item.numItemCode WHERE numProId = @numProId AND PromotionOfferOrderBased.numDomainID = @numDomainID AND cItems = 'S' AND fltSubTotal = @tot
			SELECT @monListPrice = ISNULL(Item.monListPrice,0) FROM PromotionOfferOrderBased INNER JOIN Item ON PromotionOfferOrderBased.numProItemId = Item.numItemCode WHERE numProId = @numProId AND PromotionOfferOrderBased.numDomainID = @numDomainID AND cItems = 'S' AND fltSubTotal = @tot
	
			UPDATE #temptbl
				SET Items = @vcItemName, numBaseUnit = @numBaseUnit, monListPrice = @monListPrice
			WHERE Id = @Id

			Delete #temp Where Id = @Id
		END

		SELECT DISTINCT(fltSubTotal), Items, numBaseUnit, monListPrice, cSaleType FROM #temptbl ORDER BY fltSubTotal

END 
GO











	IF ((SELECT COUNT(*) FROM PromotionOfferOrderBased WHERE numProId = @numProId AND numDomainId = @numDomainID ) > 1)
	BEGIN
		SET @vcItemName = CONCAT('',(SELECT Stuff((SELECT CONCAT(', ',Item.vcItemName) FROM PromotionOfferOrderBased INNER JOIN Item ON PromotionOfferOrderBased.numProItemId = Item.numItemCode WHERE numProId = @numProId AND PromotionOfferOrderBased.numDomainID = @numDomainID FOR XML PATH('')), 1, 2, '')), '') 
		SET @numBaseUnit = CONCAT('',(SELECT Stuff((SELECT CONCAT(', ',ISNULL(Item.numBaseUnit,0)) FROM PromotionOfferOrderBased INNER JOIN Item ON PromotionOfferOrderBased.numProItemId = Item.numItemCode WHERE numProId = @numProId AND PromotionOfferOrderBased.numDomainID = @numDomainID FOR XML PATH('')), 1, 2, '')), '') 
		SET @monListPrice = CONCAT('',(SELECT Stuff((SELECT CONCAT(', ',ISNULL(Item.monListPrice,0)) FROM PromotionOfferOrderBased INNER JOIN Item ON PromotionOfferOrderBased.numProItemId = Item.numItemCode WHERE numProId = @numProId AND PromotionOfferOrderBased.numDomainID = @numDomainID FOR XML PATH('')), 1, 2, '')), '') 
	END
	ELSE IF EXISTS(SELECT * FROM PromotionOfferOrderBased WHERE numProId = @numProId AND numDomainId = @numDomainID AND cSaleType = 'I' AND cItems = 'S' ) 
	BEGIN
		SELECT @vcItemName = Item.vcItemName FROM PromotionOfferOrderBased INNER JOIN Item ON PromotionOfferOrderBased.numProItemId = Item.numItemCode WHERE numProId = @numProId AND PromotionOfferOrderBased.numDomainID = @numDomainID
		SELECT @numBaseUnit = ISNULL(Item.numBaseUnit,0) FROM PromotionOfferOrderBased INNER JOIN Item ON PromotionOfferOrderBased.numProItemId = Item.numItemCode WHERE numProId = @numProId AND PromotionOfferOrderBased.numDomainID = @numDomainID
		SELECT @monListPrice = ISNULL(Item.monListPrice,0) FROM PromotionOfferOrderBased INNER JOIN Item ON PromotionOfferOrderBased.numProItemId = Item.numItemCode WHERE numProId = @numProId AND PromotionOfferOrderBased.numDomainID = @numDomainID
	END

	SELECT numProOrderBasedID
		,fltSubTotal
		,cSaleType
		,@vcItemName AS Items
		,@numBaseUnit AS numBaseUnit
		,@monListPrice AS monListPrice
	FROM PromotionOfferOrderBased  
	WHERE numProId = @numProId
		AND numDomainId = @numDomainID 
END 
GO

*/


/*
SET @vcItemName = (SELECT CASE 
							WHEN numProItemId <> NULL AND ((SELECT COUNT(*) FROM PromotionOfferOrderBased WHERE numProId = @numProId AND numDomainId = @numDomainID ) > 1)
								THEN CONCAT('',(SELECT Stuff((SELECT CONCAT(', ',Item.vcItemName) FROM PromotionOfferOrderBased INNER JOIN Item ON PromotionOfferOrderBased.numProItemId = Item.numItemCode WHERE numProId = @numProId AND PromotionOfferOrderBased.numDomainID = @numDomainID FOR XML PATH('')), 1, 2, '')), '') 
							ELSE
								(SELECT Item.vcItemName FROM PromotionOfferOrderBased INNER JOIN Item ON PromotionOfferOrderBased.numProItemId = Item.numItemCode WHERE numProId = @numProId AND PromotionOfferOrderBased.numDomainID = @numDomainID)
							END
								FROM PromotionOfferOrderBased  
								WHERE numProId = @numProId
									AND numDomainId = @numDomainID )

	SET @numBaseUnit = (SELECT CASE 
							WHEN numProItemId <> NULL AND ((SELECT COUNT(*) FROM PromotionOfferOrderBased WHERE numProId = @numProId AND numDomainId = @numDomainID ) > 1)
								THEN CONCAT('',(SELECT Stuff((SELECT CONCAT(', ',Item.numBaseUnit) FROM PromotionOfferOrderBased INNER JOIN Item ON PromotionOfferOrderBased.numProItemId = Item.numItemCode WHERE numProId = @numProId AND PromotionOfferOrderBased.numDomainID = @numDomainID FOR XML PATH('')), 1, 2, '')), '') 
							ELSE
								(SELECT Item.numBaseUnit FROM PromotionOfferOrderBased INNER JOIN Item ON PromotionOfferOrderBased.numProItemId = Item.numItemCode WHERE numProId = @numProId AND PromotionOfferOrderBased.numDomainID = @numDomainID)
							END
								FROM PromotionOfferOrderBased  
								WHERE numProId = @numProId
									AND numDomainId = @numDomainID )

	SET @monListPrice = (SELECT CASE 
							WHEN numProItemId <> NULL AND ((SELECT COUNT(*) FROM PromotionOfferOrderBased WHERE numProId = @numProId AND numDomainId = @numDomainID ) > 1)
								THEN CONCAT('',(SELECT Stuff((SELECT CONCAT(', ',Item.monListPrice) FROM PromotionOfferOrderBased INNER JOIN Item ON PromotionOfferOrderBased.numProItemId = Item.numItemCode WHERE numProId = @numProId AND PromotionOfferOrderBased.numDomainID = @numDomainID FOR XML PATH('')), 1, 2, '')), '') 
							ELSE
								(SELECT Item.monListPrice FROM PromotionOfferOrderBased INNER JOIN Item ON PromotionOfferOrderBased.numProItemId = Item.numItemCode WHERE numProId = @numProId AND PromotionOfferOrderBased.numDomainID = @numDomainID)
			
							END
								FROM PromotionOfferOrderBased  
								WHERE numProId = @numProId
									AND numDomainId = @numDomainID )
*/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetPromotionOfferOrderBasedItems')
	DROP PROCEDURE USP_GetPromotionOfferOrderBasedItems
GO
CREATE PROCEDURE [dbo].[USP_GetPromotionOfferOrderBasedItems]  
	@numProId AS NUMERIC(18,0),
	@numDomainID AS NUMERIC(18,0)
AS
BEGIN
	DECLARE @vcItemName VARCHAR(200), @numBaseUnit VARCHAR(100), @monListPrice VARCHAR(100)

	SELECT vcProName FROM PromotionOffer WHERE numProId = @numProId AND numDomainId = @numDomainID AND ISNULL(bitEnabled,1) = 1

	CREATE TABLE #temp 
	(
		ID int IDENTITY (1, 1) NOT NULL ,
		numProOrderBasedID numeric(18,0),
		numProId numeric(18,0),
		numDomainID NUMERIC(18,0),
		cSaleType  CHAR,
		cItems  CHAR,
		fltSubTotal  FLOAT
	)

	CREATE TABLE #temptbl 
	(
		ID int IDENTITY (1, 1) NOT NULL ,
		numProOrderBasedID numeric(18,0),
		numProId numeric(18,0),
		numDomainID NUMERIC(18,0),
		cSaleType  CHAR,
		cItems  CHAR,
		fltSubTotal  FLOAT,
		Items VARCHAR(200),
		numBaseUnit VARCHAR(100),
		monListPrice  VARCHAR(100)
	)

	Declare @Id int, @tot float

	insert into #temp
		(numProOrderBasedID, numProId, numDomainID, cSaleType, cItems, fltSubTotal)
	(select numProOrderBasedID, numProId, numDomainID, cSaleType, cItems, fltSubTotal FROM PromotionOfferOrderBased WHERE numProId = @numProId AND numDomainId = @numDomainID AND ISNULL(bitEnabled,1) = 1)

	insert into #temptbl
		(numProOrderBasedID, numProId, numDomainID, cSaleType, cItems, fltSubTotal)
	(select numProOrderBasedID, numProId, numDomainID, cSaleType, cItems, fltSubTotal FROM PromotionOfferOrderBased WHERE numProId = @numProId AND numDomainId = @numDomainID AND ISNULL(bitEnabled,1) = 1)
	
		While (Select Count(*) FROM #temp WHERE cSaleType = 'I' AND cItems = 'M') > 0
		BEGIN
			
			Select Top 1  @Id = Id, @tot = fltSubTotal From #temp WHERE cSaleType = 'I' AND cItems = 'M'

			SET @vcItemName = CONCAT('',(SELECT Stuff((SELECT CONCAT(', ',Item.vcItemName) FROM PromotionOfferOrderBased INNER JOIN Item ON PromotionOfferOrderBased.numProItemId = Item.numItemCode WHERE numProId = @numProId AND PromotionOfferOrderBased.numDomainID = @numDomainID AND cItems = 'M' AND fltSubTotal = @tot FOR XML PATH('')), 1, 2, '')), '') 
			SET @numBaseUnit = CONCAT('',(SELECT Stuff((SELECT CONCAT(', ',ISNULL(Item.numBaseUnit,0)) FROM PromotionOfferOrderBased INNER JOIN Item ON PromotionOfferOrderBased.numProItemId = Item.numItemCode WHERE numProId = @numProId AND PromotionOfferOrderBased.numDomainID = @numDomainID AND cItems = 'M' AND fltSubTotal = @tot FOR XML PATH('')), 1, 2, '')), '') 
			SET @monListPrice = CONCAT('',(SELECT Stuff((SELECT CONCAT(', ',ISNULL(Item.monListPrice,0)) FROM PromotionOfferOrderBased INNER JOIN Item ON PromotionOfferOrderBased.numProItemId = Item.numItemCode WHERE numProId = @numProId AND PromotionOfferOrderBased.numDomainID = @numDomainID AND cItems = 'M' AND fltSubTotal = @tot FOR XML PATH('')), 1, 2, '')), '') 
	
			UPDATE #temptbl
				SET Items = @vcItemName, numBaseUnit = @numBaseUnit, monListPrice = @monListPrice
			WHERE Id = @Id

			Delete #temp Where Id = @Id
		END
		While (Select Count(*) FROM #temp WHERE cSaleType = 'I' AND cItems = 'S') > 0
		BEGIN
			Select Top 1  @Id = Id,  @tot = fltSubTotal From #temp WHERE cSaleType = 'I' AND cItems = 'S'

			SELECT @vcItemName = Item.vcItemName FROM PromotionOfferOrderBased INNER JOIN Item ON PromotionOfferOrderBased.numProItemId = Item.numItemCode WHERE numProId = @numProId AND PromotionOfferOrderBased.numDomainID = @numDomainID AND cItems = 'S' AND fltSubTotal = @tot
			SELECT @numBaseUnit = ISNULL(Item.numBaseUnit,0) FROM PromotionOfferOrderBased INNER JOIN Item ON PromotionOfferOrderBased.numProItemId = Item.numItemCode WHERE numProId = @numProId AND PromotionOfferOrderBased.numDomainID = @numDomainID AND cItems = 'S' AND fltSubTotal = @tot
			SELECT @monListPrice = ISNULL(Item.monListPrice,0) FROM PromotionOfferOrderBased INNER JOIN Item ON PromotionOfferOrderBased.numProItemId = Item.numItemCode WHERE numProId = @numProId AND PromotionOfferOrderBased.numDomainID = @numDomainID AND cItems = 'S' AND fltSubTotal = @tot
	
			UPDATE #temptbl
				SET Items = @vcItemName, numBaseUnit = @numBaseUnit, monListPrice = @monListPrice
			WHERE Id = @Id

			Delete #temp Where Id = @Id
		END

		SELECT DISTINCT(fltSubTotal), Items, numBaseUnit, monListPrice, cSaleType FROM #temptbl ORDER BY fltSubTotal

		SELECT Item.vcItemName, numItemCode, monListPrice, fltSubTotal
		FROM PromotionOfferOrderBased 
			INNER JOIN Item ON PromotionOfferOrderBased.numProItemId = Item.numItemCode 
		WHERE numProId = @numProId AND PromotionOfferOrderBased.numDomainID = @numDomainID AND ISNULL(PromotionOfferOrderBased.bitEnabled,1) = 1
END 
GO










/*
	IF ((SELECT COUNT(*) FROM PromotionOfferOrderBased WHERE numProId = @numProId AND numDomainId = @numDomainID ) > 1)
	BEGIN
		SET @vcItemName = CONCAT('',(SELECT Stuff((SELECT CONCAT(', ',Item.vcItemName) FROM PromotionOfferOrderBased INNER JOIN Item ON PromotionOfferOrderBased.numProItemId = Item.numItemCode WHERE numProId = @numProId AND PromotionOfferOrderBased.numDomainID = @numDomainID FOR XML PATH('')), 1, 2, '')), '') 
		SET @numBaseUnit = CONCAT('',(SELECT Stuff((SELECT CONCAT(', ',ISNULL(Item.numBaseUnit,0)) FROM PromotionOfferOrderBased INNER JOIN Item ON PromotionOfferOrderBased.numProItemId = Item.numItemCode WHERE numProId = @numProId AND PromotionOfferOrderBased.numDomainID = @numDomainID FOR XML PATH('')), 1, 2, '')), '') 
		SET @monListPrice = CONCAT('',(SELECT Stuff((SELECT CONCAT(', ',ISNULL(Item.monListPrice,0)) FROM PromotionOfferOrderBased INNER JOIN Item ON PromotionOfferOrderBased.numProItemId = Item.numItemCode WHERE numProId = @numProId AND PromotionOfferOrderBased.numDomainID = @numDomainID FOR XML PATH('')), 1, 2, '')), '') 
	END
	ELSE IF EXISTS(SELECT * FROM PromotionOfferOrderBased WHERE numProId = @numProId AND numDomainId = @numDomainID AND cSaleType = 'I' AND cItems = 'S' ) 
	BEGIN
		SELECT @vcItemName = Item.vcItemName FROM PromotionOfferOrderBased INNER JOIN Item ON PromotionOfferOrderBased.numProItemId = Item.numItemCode WHERE numProId = @numProId AND PromotionOfferOrderBased.numDomainID = @numDomainID
		SELECT @numBaseUnit = ISNULL(Item.numBaseUnit,0) FROM PromotionOfferOrderBased INNER JOIN Item ON PromotionOfferOrderBased.numProItemId = Item.numItemCode WHERE numProId = @numProId AND PromotionOfferOrderBased.numDomainID = @numDomainID
		SELECT @monListPrice = ISNULL(Item.monListPrice,0) FROM PromotionOfferOrderBased INNER JOIN Item ON PromotionOfferOrderBased.numProItemId = Item.numItemCode WHERE numProId = @numProId AND PromotionOfferOrderBased.numDomainID = @numDomainID
	END

	SELECT numProOrderBasedID
		,fltSubTotal
		,cSaleType
		,@vcItemName AS Items
		,@numBaseUnit AS numBaseUnit
		,@monListPrice AS monListPrice
	FROM PromotionOfferOrderBased  
	WHERE numProId = @numProId
		AND numDomainId = @numDomainID 
END 
GO

*/


/*
SET @vcItemName = (SELECT CASE 
							WHEN numProItemId <> NULL AND ((SELECT COUNT(*) FROM PromotionOfferOrderBased WHERE numProId = @numProId AND numDomainId = @numDomainID ) > 1)
								THEN CONCAT('',(SELECT Stuff((SELECT CONCAT(', ',Item.vcItemName) FROM PromotionOfferOrderBased INNER JOIN Item ON PromotionOfferOrderBased.numProItemId = Item.numItemCode WHERE numProId = @numProId AND PromotionOfferOrderBased.numDomainID = @numDomainID FOR XML PATH('')), 1, 2, '')), '') 
							ELSE
								(SELECT Item.vcItemName FROM PromotionOfferOrderBased INNER JOIN Item ON PromotionOfferOrderBased.numProItemId = Item.numItemCode WHERE numProId = @numProId AND PromotionOfferOrderBased.numDomainID = @numDomainID)
							END
								FROM PromotionOfferOrderBased  
								WHERE numProId = @numProId
									AND numDomainId = @numDomainID )

	SET @numBaseUnit = (SELECT CASE 
							WHEN numProItemId <> NULL AND ((SELECT COUNT(*) FROM PromotionOfferOrderBased WHERE numProId = @numProId AND numDomainId = @numDomainID ) > 1)
								THEN CONCAT('',(SELECT Stuff((SELECT CONCAT(', ',Item.numBaseUnit) FROM PromotionOfferOrderBased INNER JOIN Item ON PromotionOfferOrderBased.numProItemId = Item.numItemCode WHERE numProId = @numProId AND PromotionOfferOrderBased.numDomainID = @numDomainID FOR XML PATH('')), 1, 2, '')), '') 
							ELSE
								(SELECT Item.numBaseUnit FROM PromotionOfferOrderBased INNER JOIN Item ON PromotionOfferOrderBased.numProItemId = Item.numItemCode WHERE numProId = @numProId AND PromotionOfferOrderBased.numDomainID = @numDomainID)
							END
								FROM PromotionOfferOrderBased  
								WHERE numProId = @numProId
									AND numDomainId = @numDomainID )

	SET @monListPrice = (SELECT CASE 
							WHEN numProItemId <> NULL AND ((SELECT COUNT(*) FROM PromotionOfferOrderBased WHERE numProId = @numProId AND numDomainId = @numDomainID ) > 1)
								THEN CONCAT('',(SELECT Stuff((SELECT CONCAT(', ',Item.monListPrice) FROM PromotionOfferOrderBased INNER JOIN Item ON PromotionOfferOrderBased.numProItemId = Item.numItemCode WHERE numProId = @numProId AND PromotionOfferOrderBased.numDomainID = @numDomainID FOR XML PATH('')), 1, 2, '')), '') 
							ELSE
								(SELECT Item.monListPrice FROM PromotionOfferOrderBased INNER JOIN Item ON PromotionOfferOrderBased.numProItemId = Item.numItemCode WHERE numProId = @numProId AND PromotionOfferOrderBased.numDomainID = @numDomainID)
			
							END
								FROM PromotionOfferOrderBased  
								WHERE numProId = @numProId
									AND numDomainId = @numDomainID )
*/
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Created By Anoop Jayaraj                                                              
--- Modified By Gangadhar 03/05/2008    
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_getprospectslist1' ) 
    DROP PROCEDURE usp_getprospectslist1
GO
CREATE PROCEDURE [dbo].[USP_GetProspectsList1]
    @CRMType NUMERIC,
    @numUserCntID NUMERIC,
    @tintUserRightType TINYINT,
    @tintSortOrder NUMERIC = 4,
    @numDomainID AS NUMERIC(9) = 0,
    @SortChar CHAR(1) = '0',
    @CurrentPage INT,
    @PageSize INT,
    @columnName AS VARCHAR(50),
    @columnSortOrder AS VARCHAR(10),
    @numProfile AS NUMERIC,
    @bitPartner AS BIT,
    @ClientTimeZoneOffset AS INT,
	@bitActiveInActive as bit,
	@vcRegularSearchCriteria varchar(MAX)='',
	@vcCustomSearchCriteria varchar(MAX)='' ,
	@SearchText VARCHAR(100) = '',
	 @TotRecs int output 
AS 
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	DECLARE @tintPerformanceFilter AS TINYINT

	IF PATINDEX('%cmp.vcPerformance=1%', @vcRegularSearchCriteria)>0 --Last 3 Months
	BEGIN
		SET @tintPerformanceFilter = 1
	END
	ELSE IF PATINDEX('%cmp.vcPerformance=2%', @vcRegularSearchCriteria)>0 --Last 6 Months
	BEGIN
		SET @tintPerformanceFilter = 2
	END
	ELSE IF PATINDEX('%cmp.vcPerformance=3%', @vcRegularSearchCriteria)>0 --Last 1 Year
	BEGIN
		SET @tintPerformanceFilter = 3
	END
	ELSE
	BEGIN
		SET @tintPerformanceFilter = 0
	END

	IF CHARINDEX('AD.numBillCountry',@vcRegularSearchCriteria) > 0
	BEGIN
		SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'AD.numBillCountry','AD1.numCountry')
	END
	ELSE IF CHARINDEX('AD.numShipCountry',@vcRegularSearchCriteria) > 0
	BEGIN
		SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'AD.numShipCountry','AD2.numCountry')
	END
	IF CHARINDEX('ADC.vcLastFollowup',@vcRegularSearchCriteria) > 0
	BEGIN
		SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'ADC.vcLastFollowup', '(SELECT dtExecutionDate FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID  WHERE ConECampaign.numContactID=ADC.numContactId AND ISNULL(ADC.numECampaignID,0) > 0 AND numConECampDTLID IN (SELECT MAX(numConECampDTLID) FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID  WHERE ConECampaign.numContactID=ADC.numContactId AND ISNULL(ADC.numECampaignID,0) > 0 AND ISNULL(bitSend,0)=1))')
	END
	IF CHARINDEX('ADC.vcNextFollowup',@vcRegularSearchCriteria) > 0
	BEGIN
		SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'ADC.vcNextFollowup','(SELECT dtExecutionDate FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID  WHERE ConECampaign.numContactID=ADC.numContactId AND ISNULL(ADC.numECampaignID,0) > 0 AND numConECampDTLID IN (SELECT MIN(numConECampDTLID) FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID  WHERE ConECampaign.numContactID=ADC.numContactId AND ISNULL(ADC.numECampaignID,0) > 0 AND ISNULL(bitSend,0)=0))')
	END  

	CREATE TABLE #tempForm 
	(
		tintOrder TINYINT,vcDbColumnName nvarchar(50),vcOrigDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),vcListItemType varchar(3),
		numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,
		bitIsAlphaNumeric BIT,bitIsNumeric BIT,bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),ListRelID numeric(9),
		intColumnWidth INT,bitAllowFiltering BIT,vcFieldDataType CHAR(1)
	)
    
	DECLARE @Nocolumns AS TINYINT = 0
    
	SELECT 
		@Nocolumns=ISNULL(SUM(TotalRow),0)  
	FROM
	(            
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=35 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND numRelCntType=3 
		UNION 
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicCustomColumns 
		WHERE 
			numFormId=35 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND numRelCntType=3
	) TotalRows


	--Check if Master Form Configuration is created by administrator if NoColumns=0
	DECLARE @IsMasterConfAvailable BIT = 0
	DECLARE @numUserGroup NUMERIC(18,0)

	IF @Nocolumns=0
	BEGIN
		SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

		IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = 35 AND numRelCntType=3 AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
		BEGIN
			SET @IsMasterConfAvailable = 1
		END
	END

	--If MasterConfiguration is available then load it otherwise load default columns
	IF @IsMasterConfAvailable = 1
	BEGIN
		INSERT INTO DycFormConfigurationDetails 
		(
			numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
		)
		SELECT 
			35,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,3,1,0,intColumnWidth
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=35 AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.numRelCntType=3 AND 
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
		UNION
		SELECT 
			35,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,3,1,1,intColumnWidth
		FROM 
			View_DynamicCustomColumnsMasterConfig
		WHERE 
			View_DynamicCustomColumnsMasterConfig.numFormId=35 AND 
			View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.numRelCntType=3 AND 
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1

		INSERT INTO #tempForm
		SELECT 
			(intRowNum + 1) as tintOrder, vcDbColumnName,	vcOrigDbColumnName,	ISNULL(vcCultureFieldName,vcFieldName), vcAssociatedControlType,
			vcListItemType, numListID, vcLookBackTableName,	bitCustom, numFieldId, bitAllowSorting,	bitInlineEdit, bitIsRequired,
			bitIsEmail,	bitIsAlphaNumeric, bitIsNumeric, bitIsLengthValidation,	intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,
			ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType 
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=35 AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.numRelCntType=3 AND 
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName, vcAssociatedControlType,'' as vcListItemType,numListID,'',
			bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,''
		FROM 
			View_DynamicCustomColumnsMasterConfig
		WHERE 
			View_DynamicCustomColumnsMasterConfig.numFormId=35 AND 
			View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.numRelCntType=3 AND 
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1
		ORDER BY 
			tintOrder ASC  
	END
	ELSE                                        
	BEGIN
		IF @Nocolumns=0
		BEGIN
			INSERT INTO DycFormConfigurationDetails (numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth)
			select 35,numFieldId,0,Row_number() over(order by tintRow desc),@numDomainID,@numUserCntID,3,1,0,intColumnWidth
			 FROM View_DynamicDefaultColumns
			 where numFormId=35 and bitDefault=1 AND ISNULL(bitSettingField,0)=1 and numDomainID=@numDomainID
			order by tintOrder asc   
		END    
       
		INSERT INTO 
			#tempForm
        SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName,
			bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,
			bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID ,intColumnWidth,bitAllowFiltering,vcFieldDataType
		FROM 
			View_DynamicColumns 
        WHERE 
			numFormId = 35
			AND numUserCntID = @numUserCntID
			AND numDomainID = @numDomainID
			AND tintPageType = 1
			AND numRelCntType = 3
			AND ISNULL(bitSettingField, 0) = 1
			AND ISNULL(bitCustom, 0) = 0
        UNION
        SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName,vcAssociatedControlType,'' as vcListItemType,numListID,'',
			bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,
			bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,''
		FROM 
			View_DynamicCustomColumns
        WHERE 
			numFormId = 35
            AND numUserCntID = @numUserCntID
            AND numDomainID = @numDomainID
            AND tintPageType = 1
            AND numRelCntType = 3
            AND ISNULL(bitCustom, 0) = 1
        ORDER BY 
			tintOrder ASC  
	END  

		

	DECLARE  @strColumns AS VARCHAR(MAX)
	SET @strColumns=''

	 SET @strColumns =' ISNULL(ADC.numContactId,0) numContactId, ISNULL(DM.numDivisionID,0) numDivisionID, ISNULL(DM.numTerID,0)numTerID, ISNULL(ADC.numRecOwner,0) numRecOwner, ISNULL(DM.tintCRMType,0) AS tintCRMType, CMP.vcCompanyName '              

	DECLARE @tintOrder AS TINYINT                                                      
    DECLARE @vcFieldName AS VARCHAR(50)                                                      
    DECLARE @vcListItemType AS VARCHAR(3)                                                 
    DECLARE @vcAssociatedControlType VARCHAR(20)                                                      
    DECLARE @numListID AS NUMERIC(9)                                                      
    DECLARE @vcDbColumnName VARCHAR(20)                          
    DECLARE @WhereCondition VARCHAR(MAX)                           
    DECLARE @vcLookBackTableName VARCHAR(2000)                    
    DECLARE @bitCustom AS BIT
    DECLARE @bitAllowEdit AS CHAR(1)                 
    DECLARE @numFieldId AS NUMERIC  
    DECLARE @bitAllowSorting AS CHAR(1)              
    DECLARE @vcColumnName AS VARCHAR(500)  
    SET @tintOrder = 0                                                      
    SET @WhereCondition = ''
	Declare @ListRelID as numeric(9) 
	DECLARE @SearchQuery AS VARCHAR(MAX) = '' 

	SELECT TOP 1
            @tintOrder = tintOrder + 1,
            @vcDbColumnName = vcDbColumnName,
            @vcFieldName = vcFieldName,
            @vcAssociatedControlType = vcAssociatedControlType,
            @vcListItemType = vcListItemType,
            @numListID = numListID,
            @vcLookBackTableName = vcLookBackTableName,
            @bitCustom = bitCustomField,
            @numFieldId = numFieldId,
            @bitAllowSorting = bitAllowSorting,
            @bitAllowEdit = bitAllowEdit,@ListRelID=ListRelID
    FROM    #tempForm
    ORDER BY tintOrder ASC            
 
                                               
    WHILE @tintOrder > 0  
    BEGIN
            IF @bitCustom = 0 
                BEGIN            
                    DECLARE @Prefix AS VARCHAR(5)                        
                    IF @vcLookBackTableName = 'AdditionalContactsInformation' 
                        SET @Prefix = 'ADC.'                        
                    IF @vcLookBackTableName = 'DivisionMaster' 
                        SET @Prefix = 'DM.'  

					SET @vcColumnName= @vcDbColumnName+'~'+ convert(varchar(10),@numFieldId) +'~0'

					IF @vcAssociatedControlType='Label' and @vcDbColumnName='vcPerformance'
					BEGIN
						SET @WhereCondition = @WhereCondition
														+ CONCAT(' OUTER APPLY(SELECT
																			SUM(monDealAmount) AS monDealAmount
																			,AVG(ProfitPer) AS fltProfitPer
																		FROM
																		(
																			SELECT
																				OM.numDivisionId
																				,monDealAmount
																				,AVG(((ISNULL(OI.monPrice,0) - (CASE D.numCost WHEN 3 THEN ISNULL(V.monCost,0) * dbo.fn_UOMConversion(I.numBaseUnit,I.numItemCode,I.numDomainID,I.numPurchaseUnit) WHEN 2 THEN ISNULL(OI.numCost,0) ELSE ISNULL(I.monAverageCost,0) END)) / ISNULL(NULLIF(OI.monPrice,0),1)) * 100) ProfitPer
																			FROM
																				OpportunityMaster OM
																			INNER JOIN
																				Domain D
																			ON
																				OM.numDomainID = D.numDomainID
																			INNER JOIN
																				OpportunityItems OI
																			ON
																				OM.numOppId = OI.numOppId
																			INNER JOIN
																				Item I
																			ON
																				OI.numItemCode = I.numItemCode
																			LEFT JOIN
																				Vendor V
																			ON
																				V.numVendorID = I.numVendorID
																				AND V.numItemCode = I.numItemCode
																			WHERE
																				OM.numDomainId = ',@numDomainID,'
																				AND OM.numDivisionId = DM.numDivisionID
																				AND ISNULL(I.bitContainer,0) = 0
																				AND tintOppType = 1
																				AND tintOppStatus=1
																				AND 1 = (CASE ', @tintPerformanceFilter,' 
																						WHEN 1 THEN (CASE WHEN OM.bintCreatedDate BETWEEN DATEADD(MONTH,-3,GETUTCDATE()) AND GETUTCDATE() THEN 1 ELSE 0 END)
																						WHEN 2 THEN (CASE WHEN OM.bintCreatedDate BETWEEN DATEADD(MONTH,-6,GETUTCDATE()) AND GETUTCDATE() THEN 1 ELSE 0 END)
																						WHEN 3 THEN (CASE WHEN OM.bintCreatedDate BETWEEN DATEADD(YEAR,-1,GETUTCDATE()) AND GETUTCDATE() THEN 1 ELSE 0 END)
																						ELSE 1
																						END)
																			GROUP BY
																				OM.numDivisionId
																				,OM.numOppId
																				,monDealAmount
																		) T1
																		GROUP BY
																			T1.numDivisionId ) AS TempPerformance ')

						set @strColumns=@strColumns +', (CASE WHEN ISNULL(TempPerformance.monDealAmount,0) > 0 THEN CONCAT(FORMAT(TempPerformance.monDealAmount,''##,####.00''),'' ('',CAST(TempPerformance.fltProfitPer AS DECIMAL(18,2)),''%)'') ELSE '''' END) ['+ @vcColumnName+']'
					END
					ELSE IF @vcDbColumnName='bitEcommerceAccess'
					BEGIN
						SET @strColumns=@strColumns+ ' ,(CASE WHEN (SELECt COUNT(*) FROM ExtarnetAccounts WHERE numDivisionID=DM.numDivisionID) >0 THEN 1 ELSE 0 END) AS ['+ @vcColumnName+']' 
					END 
					ELSE IF @vcDbColumnName = 'vcLastFollowup'
					BEGIN
						set @strColumns=@strColumns+',  ISNULL((CASE WHEN (ADC.numECampaignID > 0) THEN dbo.fn_FollowupDetailsInOrgList(ADC.numContactID,1,ADC.numDomainID) ELSE '''' END),'''') ['+ @vcColumnName+']' 
					END
					ELSE IF @vcDbColumnName = 'vcNextFollowup'
					BEGIN
						set @strColumns=@strColumns+',  ISNULL((CASE WHEN  (ADC.numECampaignID >0) THEN dbo.fn_FollowupDetailsInOrgList(ADC.numContactID,2,ADC.numDomainID) ELSE '''' END),'''') ['+ @vcColumnName+']' 
					END
                    ELSE IF @vcAssociatedControlType = 'SelectBox' or @vcAssociatedControlType='ListBox'     
                        BEGIN                                                          
                            IF @vcDbColumnName = 'numPartenerSource'
							BEGIN
								SET @strColumns=@strColumns+ ' ,(DM.numPartenerSource) '+' ['+ @vcColumnName+']'    
								SET @strColumns=@strColumns+ ' ,(SELECT D.vcPartnerCode+''-''+C.vcCompanyName FROM DivisionMaster AS D LEFT JOIN CompanyInfo AS C ON D.numCompanyID=C.numCompanyID WHERE D.numDivisionID=DM.numPartenerSource) AS vcPartenerSource' 
							END
							ELSE IF @vcListItemType = 'LI' 
                                BEGIN    
                                    IF @numListID = 40 
                                        BEGIN
                                            SET @strColumns = @strColumns + ',L' + CONVERT(VARCHAR(3), @tintOrder) + '.vcData' + ' [' + @vcColumnName + ']'                                                   
                                            
											IF LEN(@SearchText) > 0
											BEGIN 
											SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData LIKE ''%' + @SearchText + '%'' '     
											END   

											IF @vcDbColumnName='numShipCountry'
											BEGIN
												SET @WhereCondition = @WhereCondition
													+ ' left Join AddressDetails AD2 on AD2.numRecordId= DM.numDivisionID and AD2.tintAddressOf=2 and AD2.tintAddressType=2 AND AD2.bitIsPrimary=1 and AD2.numDomainID= DM.numDomainID '
													+ ' left Join ListDetails L' + CONVERT(VARCHAR(3), @tintOrder) + ' on L' + CONVERT(VARCHAR(3), @tintOrder) + '.numListItemID=AD2.numCountry '
											END
											ELSE
											BEGIN
												SET @WhereCondition = @WhereCondition
													+ ' left Join AddressDetails AD1 on AD1.numRecordId= DM.numDivisionID and AD1.tintAddressOf=2 and AD1.tintAddressType=1 AND AD1.bitIsPrimary=1 and AD1.numDomainID= DM.numDomainID '
													+ ' left Join ListDetails L' + CONVERT(VARCHAR(3), @tintOrder) + ' on L' + CONVERT(VARCHAR(3), @tintOrder) + '.numListItemID=AD1.numCountry '
											END
                                        END
                                    ELSE 
                                        BEGIN                                                      
                                            SET @strColumns = @strColumns + ',L'
                                                + CONVERT(VARCHAR(3), @tintOrder)
                                                + '.vcData' + ' ['
                                                + @vcColumnName + ']'           
											
											IF LEN(@SearchText) > 0
											BEGIN 
											SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData LIKE ''%' + @SearchText + '%'' '     
											END   

											                                              
                                            SET @WhereCondition = @WhereCondition
                                                + ' left Join ListDetails L'
                                                + CONVERT(VARCHAR(3), @tintOrder)
                                                + ' on L'
                                                + CONVERT(VARCHAR(3), @tintOrder)
                                                + '.numListItemID='
                                                + @vcDbColumnName                                                          
                                        END
                                END                                                          
                            ELSE 
                                IF @vcListItemType = 'S' 
                                    BEGIN     
											
											                                                           
											
											IF LEN(@SearchText) > 0
											BEGIN 
											SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'S' + CONVERT(VARCHAR(3),@tintOrder) + '.vcState LIKE ''%' + @SearchText + '%'' '     
											END   

											IF @vcDbColumnName='numShipState'
											BEGIN
												SET @strColumns = @strColumns + ',ShipState.vcState' + ' [' + @vcColumnName + ']' 
												SET @WhereCondition = @WhereCondition
													+ ' left Join AddressDetails AD3 on AD3.numRecordId= DM.numDivisionID and AD3.tintAddressOf=2 and AD3.tintAddressType=2 AND AD3.bitIsPrimary=1 and AD3.numDomainID= DM.numDomainID '
													+ ' left Join State ShipState on ShipState.numStateID=AD3.numState '
											END
											ELSE IF @vcDbColumnName='numBillState'
											BEGIN
												SET @strColumns = @strColumns + ',BillState.vcState' + ' [' + @vcColumnName + ']' 
												SET @WhereCondition = @WhereCondition
													+ ' left Join AddressDetails AD4 on AD4.numRecordId= DM.numDivisionID and AD4.tintAddressOf=2 and AD4.tintAddressType=1 AND AD4.bitIsPrimary=1 and AD4.numDomainID= DM.numDomainID '
													+ ' left Join State BillState on BillState.numStateID=AD4.numState '
											END
											ELSE  
											BEGIN
												SET @strColumns = @strColumns + ',S' + CONVERT(VARCHAR(3), @tintOrder) + '.vcState' + ' [' + @vcColumnName + ']' 
												SET @WhereCondition = @WhereCondition + ' left join State S' + CONVERT(VARCHAR(3), @tintOrder) + ' on S' + CONVERT(VARCHAR(3), @tintOrder) + '.numStateID=' + @vcDbColumnName        
											END
                                                 
                                                                                          
                                    END                                                          
                                ELSE 
                                    IF @vcListItemType = 'T' 
                                        BEGIN                                                          
                                            SET @strColumns = @strColumns + ',L'
                                                + CONVERT(VARCHAR(3), @tintOrder)
                                                + '.vcData' + ' ['
                                                + @vcColumnName + ']'     
												
											IF LEN(@SearchText) > 0
											BEGIN 
											SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData LIKE ''%' + @SearchText + '%'' '     
											END  
											                                                     
                                            SET @WhereCondition = @WhereCondition
                                                + ' left Join ListDetails L'
                                                + CONVERT(VARCHAR(3), @tintOrder)
                                                + ' on L'
                                                + CONVERT(VARCHAR(3), @tintOrder)
                                                + '.numListItemID='
                                                + @vcDbColumnName                                                          
                                        END
                                    ELSE 
                                        IF @vcListItemType = 'C' 
                                            BEGIN                                                    
                                                SET @strColumns = @strColumns + ',C'
                                                    + CONVERT(VARCHAR(3), @tintOrder)
                                                    + '.vcCampaignName' + ' ['
                                                    + @vcColumnName + ']'    
												
												IF LEN(@SearchText) > 0
												BEGIN 
												SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'C' + CONVERT(VARCHAR(3),@tintOrder) + '.vcCampaignName LIKE ''%' + @SearchText + '%'' '     
												END  
												                                                
                                                SET @WhereCondition = @WhereCondition
                                                    + ' left Join CampaignMaster C'
                                                    + CONVERT(VARCHAR(3), @tintOrder)
                                                    + ' on C'
                                                    + CONVERT(VARCHAR(3), @tintOrder)
                                                    + '.numCampaignId=DM.'
                                                    + @vcDbColumnName                                                    
                                            END                       
                                        ELSE 
                                            IF @vcListItemType = 'U' 
                                                BEGIN                         
                            
                              
                                                    SET @strColumns = @strColumns
                                                        + ',dbo.fn_GetContactName('
                                                        + @Prefix
                                                        + @vcDbColumnName
                                                        + ') ['
                                                        + @vcColumnName + ']'  
														
													IF LEN(@SearchText) > 0
													  BEGIN 
														SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'dbo.fn_GetContactName(' + @Prefix + @vcDbColumnName + ') LIKE ''%' + @SearchText + '%'' '    
													  END       
													                                                        
                                                END                        
                        END       
      
                    ELSE 
                        IF @vcAssociatedControlType = 'DateField' 
                            BEGIN              
                                SET @strColumns = @strColumns
                                    + ',case when convert(varchar(11),DateAdd(minute, '
                                    + CONVERT(VARCHAR(15), -@ClientTimeZoneOffset)
                                    + ',' + @Prefix + @vcDbColumnName
                                    + ') )= convert(varchar(11),getdate()) then ''<b><font color=red>Today</font></b>'''        
                                SET @strColumns = @strColumns
                                    + 'when convert(varchar(11),DateAdd(minute, '
                                    + CONVERT(VARCHAR(15), -@ClientTimeZoneOffset)
                                    + ',' + @Prefix + @vcDbColumnName
                                    + ') )= convert(varchar(11),dateadd(day,-1,getdate())) then ''<b><font color=purple>YesterDay</font></b>'''        
                                SET @strColumns = @strColumns
                                    + 'when convert(varchar(11),DateAdd(minute, '
                                    + CONVERT(VARCHAR(15), -@ClientTimeZoneOffset)
                                    + ',' + @Prefix + @vcDbColumnName
                                    + ') )= convert(varchar(11),dateadd(day,1,getdate())) then''<b><font color=orange>Tommorow</font></b>'' '        
                                SET @strColumns = @strColumns
                                    + 'else dbo.FormatedDateFromDate(DateAdd(minute, '
                                    + CONVERT(VARCHAR(15), -@ClientTimeZoneOffset)
                                    + ',' + @Prefix + @vcDbColumnName + '),'
                                    + CONVERT(VARCHAR(10), @numDomainId)
                                    + ') end  [' + @vcColumnName + ']'    
									
								
								IF LEN(@SearchText) > 0
								BEGIN
									SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + @Prefix + @vcDbColumnName + ' LIKE ''%' + @SearchText + '%'' '
								END
								
								              
                            END      
            
                        ELSE 
                            IF @vcAssociatedControlType = 'TextBox'
                                OR @vcAssociatedControlType = 'Label' 
                                BEGIN      
                                    SET @strColumns = @strColumns + ','
                                        + CASE WHEN @vcDbColumnName = 'numAge'
                                               THEN 'year(getutcdate())-year(bintDOB)'
                                               WHEN @vcDbColumnName = 'numDivisionID'
                                               THEN 'DM.numDivisionID'
                                               ELSE @vcDbColumnName
                                          END + ' [' + @vcColumnName + ']'    
										  
										  
									IF LEN(@SearchText) > 0
									BEGIN
										SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 
										CASE WHEN @vcDbColumnName = 'numAge'
                                               THEN 'year(getutcdate())-year(bintDOB)'
                                               WHEN @vcDbColumnName = 'numDivisionID'
                                               THEN 'DM.numDivisionID'
                                               ELSE @vcDbColumnName
                                          END
										+' LIKE ''%' + @SearchText + '%'' '
									END  		  
										              
         
                                END 
                          else if @vcAssociatedControlType='Popup' and @vcDbColumnName='numShareWith'                                                   
		begin      
			 set @strColumns=@strColumns+',(SELECT SUBSTRING(
	(SELECT '','' + A.vcFirstName+'' ''+A.vcLastName + CASE WHEN ISNULL(SR.numContactType,0)>0 THEN ''('' + dbo.fn_GetListItemName(SR.numContactType) + '')'' ELSE '''' END
	FROM ShareRecord SR JOIN UserMaster UM ON UM.numUserDetailId=SR.numAssignedTo 
							 JOIN AdditionalContactsInformation A ON UM.numUserDetailId=A.numContactID 
		 WHERE SR.numDomainID=DM.numDomainID AND SR.numModuleID=1 AND SR.numRecordID=DM.numDivisionID
				AND UM.numDomainID=DM.numDomainID and UM.numDomainID=A.numDomainID FOR XML PATH('''')),2,200000)) ['+ @vcColumnName+']'                
		 end         
                            ELSE 
                                BEGIN    
                                    SET @strColumns = @strColumns + ', '
                                        + @vcDbColumnName + ' ['
                                        + @vcColumnName + ']'              
       
                                END                                    
                   
                END                  
            ELSE 
                IF @bitCustom = 1 
                    BEGIN                  
      
						SET @vcColumnName= @vcDbColumnName +'~'+ convert(varchar(10),@numFieldId)+'~1'
 
                        SELECT  @vcFieldName = FLd_label,
                                @vcAssociatedControlType = fld_type,
                                @vcDbColumnName = 'Cust'
                                + CONVERT(VARCHAR(10), Fld_Id)
                        FROM    CFW_Fld_Master
                        WHERE   CFW_Fld_Master.Fld_Id = @numFieldId                                 
                        IF @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'   
                            BEGIN                              
                                SET @strColumns = @strColumns + ',CFW'
                                    + CONVERT(VARCHAR(3), @tintOrder)
                                    + '.Fld_Value  [' + @vcColumnName + ']'                     
                                SET @WhereCondition = @WhereCondition
                                    + ' left Join CFW_FLD_Values CFW'
                                    + CONVERT(VARCHAR(3), @tintOrder)
                                    + '                   
    on CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
                                    + CONVERT(VARCHAR(10), @numFieldId)
                                    + 'and CFW'
                                    + CONVERT(VARCHAR(3), @tintOrder)
                                    + '.RecId=DM.numDivisionId   '                                                           
                            END    
                        ELSE 
                            IF @vcAssociatedControlType = 'CheckBox'  
                                BEGIN              
set @strColumns= @strColumns+',case when isnull(CFW' + CONVERT(VARCHAR(3), @tintOrder)+ '.Fld_Value,0)='''' then 0 else  isnull(CFW'
                                        + CONVERT(VARCHAR(3), @tintOrder)+ '.Fld_Value,0) end ['+ @vcColumnName + ']'        
 
                                    SET @WhereCondition = @WhereCondition
                                        + ' left Join CFW_FLD_Values CFW'
                                        + CONVERT(VARCHAR(3), @tintOrder)
                                        + '             
    on CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
                                        + CONVERT(VARCHAR(10), @numFieldId)
                                        + 'and CFW'
                                        + CONVERT(VARCHAR(3), @tintOrder)
                                        + '.RecId=DM.numDivisionId   '                                                     
                                END       
                            ELSE 
                                IF @vcAssociatedControlType = 'DateField'  
                                    BEGIN                  
                     
                                        SET @strColumns = @strColumns
                                            + ',dbo.FormatedDateFromDate(CFW'
                                            + CONVERT(VARCHAR(3), @tintOrder)
                                            + '.Fld_Value,'
                                            + CONVERT(VARCHAR(10), @numDomainId)
                                            + ')  [' + @vcColumnName + ']'                     
                                        SET @WhereCondition = @WhereCondition
                                            + ' left Join CFW_FLD_Values CFW'
                                            + CONVERT(VARCHAR(3), @tintOrder)
                                            + '                   
    on CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
                                            + CONVERT(VARCHAR(10), @numFieldId)
                                            + 'and CFW'
                                            + CONVERT(VARCHAR(3), @tintOrder)
                                            + '.RecId=DM.numDivisionId   '                                                           
                                    END                  
                                ELSE 
                                    IF @vcAssociatedControlType = 'SelectBox'
                                        BEGIN                   
                                            SET @vcDbColumnName = 'DCust'
                                                + CONVERT(VARCHAR(10), @numFieldId)                  
                                            SET @strColumns = @strColumns + ',L'
                                                + CONVERT(VARCHAR(3), @tintOrder)
                                                + '.vcData' + ' ['
                                                + @vcColumnName + ']'                                                            
                                            SET @WhereCondition = @WhereCondition
                                                + ' left Join CFW_FLD_Values CFW'
                                                + CONVERT(VARCHAR(3), @tintOrder)
                                                + '                   
     on CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
                                                + CONVERT(VARCHAR(10), @numFieldId)
                                                + 'and CFW'
                                                + CONVERT(VARCHAR(3), @tintOrder)
                                                + '.RecId=DM.numDivisionId    '                                                           
                                            SET @WhereCondition = @WhereCondition
                                                + ' left Join ListDetails L'
                                                + CONVERT(VARCHAR(3), @tintOrder)
                                                + ' on L'
                                                + CONVERT(VARCHAR(3), @tintOrder)
                                                + '.numListItemID=CFW'
                                                + CONVERT(VARCHAR(3), @tintOrder)
                                                + '.Fld_Value'                  
                                        END      
								ELSE IF @vcAssociatedControlType = 'CheckBoxList' 
								BEGIN
									SET @strColumns= @strColumns+',(SELECT STUFF((SELECT CONCAT('', '', vcData) FROM ListDetails WHERE numListID= ' + CAST(@numListID AS varchar) + ' AND numlistitemid IN (SELECT ISNULL(Id,0) FROM dbo.SplitIDs(CFW'+ convert(VARCHAR(3),@tintOrder)+'.Fld_Value,'','')) FOR XML PATH('''')), 1, 1, ''''))  ['+ @vcColumnName +']'   

								SET @WhereCondition= @WhereCondition 
													+' left Join CFW_FLD_Values CFW'+ convert(varchar(3),@tintOrder)
													+ ' on CFW' + convert(varchar(3),@tintOrder) 
													+ '.Fld_Id='+convert(varchar(10),@numFieldId) 
													+ 'and CFW' + convert(varchar(3),@tintOrder) 
													+ '.RecId=DM.numDivisionId '
								END                       
                    END    
                    

		  select top 1 
		   @tintOrder = tintOrder + 1,
            @vcDbColumnName = vcDbColumnName,
            @vcFieldName = vcFieldName,
            @vcAssociatedControlType = vcAssociatedControlType,
            @vcListItemType = vcListItemType,
            @numListID = numListID,
            @vcLookBackTableName = vcLookBackTableName,
            @bitCustom = bitCustomField,
            @numFieldId = numFieldId,
            @bitAllowSorting = bitAllowSorting,
            @bitAllowEdit = bitAllowEdit,@ListRelID=ListRelID
		  from #tempForm WHERE tintOrder > @tintOrder-1 order by tintOrder asc
            IF @@rowcount = 0 
                SET @tintOrder = 0 
    END  

	DECLARE @strShareRedordWith AS VARCHAR(300);
	SET @strShareRedordWith=' DM.numDivisionID in (select SR.numRecordID FROM ShareRecord SR WHERE SR.numDomainID='+ convert(varchar(15),@numDomainID )+' AND SR.numModuleID=1 AND SR.numAssignedTo='+convert(varchar(15),@numUserCntId)+') '

	SET @strColumns=@strColumns+' ,ISNULL(VIE.Total,0) as TotalEmail '
	SET @strColumns=@strColumns+' ,ISNULL(VOA.OpenActionItemCount,0) as TotalActionItem '

	---- Added by Priya 16 Jan 2018(Flag with Organisation Name)--------
	SET @strColumns=@strColumns+' ,

	((CASE 
	WHEN ADC.numECampaignID > 0 
	THEN 
		 ISNULL( (CASE WHEN 
				ISNULL((SELECT COUNT(*) FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID
											WHERE ConECampaign.numContactID = ADC.numContactID AND  ConECampaign.numECampaignID = ADC.numECampaignID AND bitEngaged =1 AND ISNULL(bitSend,0) = 1 AND ADC.bitPrimaryContact = 1),0) 
				= ISNULL((SELECT COUNT(*) FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID
											 WHERE ConECampaign.numContactID = ADC.numContactID AND ConECampaign.numECampaignID = ADC.numECampaignID AND bitEngaged =1 AND ADC.bitPrimaryContact = 1 ),0)
		THEN
		  ''CheckeredFlag''
		  Else
		  ''GreenFlag''
		  End),0)



	ELSE '''' 
	END)) as FollowupFlag '

	SET @strColumns=@strColumns+' ,

	((CASE 
	WHEN ADC.numECampaignID > 0 
	THEN 
		''('' + CAST(ISNULL((SELECT COUNT(*) FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID
											WHERE ConECampaign.numContactID = ADC.numContactID AND  ConECampaign.numECampaignID = ADC.numECampaignID AND bitEngaged =1 AND ISNULL(bitSend,0) = 1 AND ADC.bitPrimaryContact = 1),0) AS varchar)

			+ ''/'' + CAST(ISNULL((SELECT COUNT(*) FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID
											 WHERE ConECampaign.numContactID = ADC.numContactID AND ConECampaign.numECampaignID = ADC.numECampaignID AND bitEngaged =1 AND ADC.bitPrimaryContact = 1 ),0)AS varchar) + '')''
			+ ''<a onclick=openDrip('' + (cast(ISNULL((SELECT MAX([numConEmailCampID]) FROM [ConECampaign] WHERE [numContactID] = ADC.numContactID AND [numECampaignID]= ADC.numECampaignID AND ISNULL(bitEngaged,0)=1 AND ADC.bitPrimaryContact = 1 ),0)AS varchar)) + '');>
		 <img alt=Follow-up Campaign history height=16px width=16px title=Follow-up campaign history src=../images/GLReport.png
		 ></a>'' 
	ELSE '''' 
	END)) as ReadUnreadCntNHstr '
   
---- Added by Priya 16 Jan 2018(Flag with Organisation Name)--------

		DECLARE @StrSql AS VARCHAR(MAX) = ''
		SET @StrSql='	FROM  CompanyInfo CMP                                       
						join DivisionMaster DM on DM.numCompanyID=CMP.numCompanyID
						left join AdditionalContactsInformation ADC on ADC.numDivisionID=DM.numDivisionID 
						left join VIEW_OPEN_ACTION_ITEMS VOA WITH (NOEXPAND) ON VOA.numDomainID =  '+ convert(varchar(15),@numDomainID )+' AND  VOA.numDivisionId = DM.numDivisionID 
						left join View_InboxEmail VIE WITH (NOEXPAND) ON VIE.numDomainID = '+ convert(varchar(15),@numDomainID )+' AND  VIE.numContactId = ADC.numContactId '
		IF @tintSortOrder = 9
		BEGIN
			SET @StrSql=@StrSql+ ' join Favorites F on F.numContactid=DM.numDivisionID '
		END 

		SET @StrSql=@StrSql + ISNULL(@WhereCondition,'')

		 -------Change Row Color-------
		Declare @vcCSOrigDbCOlumnName as varchar(50),@vcCSLookBackTableName as varchar(50),@vcCSAssociatedControlType as varchar(50)
		SET @vcCSOrigDbCOlumnName=''
		SET @vcCSLookBackTableName=''

		Create table #tempColorScheme(vcOrigDbCOlumnName varchar(50),vcLookBackTableName varchar(50),vcFieldValue varchar(50),vcFieldValue1 varchar(50),vcColorScheme varchar(50),vcAssociatedControlType varchar(50))

		insert into #tempColorScheme select DFM.vcOrigDbCOlumnName,DFM.vcLookBackTableName,DFCS.vcFieldValue,DFCS.vcFieldValue1,DFCS.vcColorScheme,DFFM.vcAssociatedControlType 
		from DycFieldColorScheme DFCS join DycFormField_Mapping DFFM on DFCS.numFieldID=DFFM.numFieldID
		join DycFieldMaster DFM on DFM.numModuleID=DFFM.numModuleID and DFM.numFieldID=DFFM.numFieldID
		where DFCS.numDomainID=@numDomainID and DFFM.numFormID=35 AND DFCS.numFormID=35 and isnull(DFFM.bitAllowGridColor,0)=1

		IF(SELECT COUNT(*) FROM #tempColorScheme)>0
		BEGIN
		   select top 1 @vcCSOrigDbCOlumnName=vcOrigDbCOlumnName,@vcCSLookBackTableName=vcLookBackTableName,@vcCSAssociatedControlType=vcAssociatedControlType from #tempColorScheme
		END   
		----------------------------                        
 
		 IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
		BEGIN
		set @strColumns=@strColumns+' ,tCS.vcColorScheme '
		END

		IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
		BEGIN
			 if @vcCSLookBackTableName = 'AdditionalContactsInformation'                  
				set @Prefix = 'ADC.'                  
			 if @vcCSLookBackTableName = 'DivisionMaster'                  
				set @Prefix = 'DM.'
			 if @vcCSLookBackTableName = 'CompanyInfo'                  
				set @Prefix = 'CMP.'   
 
			IF @vcCSAssociatedControlType='DateField'
			BEGIN
					set @strSql=@strSql+' left join #tempColorScheme tCS on CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) >= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue as int),GETUTCDATE()),111)
					 and CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) <= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue1 as int),GETUTCDATE()),111)'
			END
			ELSE IF @vcCSAssociatedControlType='SelectBox' OR @vcCSAssociatedControlType='ListBox' OR @vcCSAssociatedControlType='CheckBox'
			BEGIN
				set @strSql=@strSql+' left join #tempColorScheme tCS on tCS.vcFieldValue=' + @Prefix + @vcCSOrigDbCOlumnName
			END
		END

		IF @columnName LIKE 'CFW.Cust%' 
        BEGIN                   
            SET @strSql = @strSql + ' left Join CFW_FLD_Values CFW on CFW.RecId=DM.numDivisionID and CFW.fld_id= ' + REPLACE(@columnName, 'CFW.Cust', '') + ' '                                                           
            SET @columnName = 'CFW.Fld_Value'                  
        END                                           
		ELSE IF @columnName LIKE 'DCust%' 
        BEGIN                                                      
            SET @strSql = @strSql + ' left Join CFW_FLD_Values CFW on CFW.RecId=DM.numDivisionID  and CFW.fld_id= ' + REPLACE(@columnName, 'DCust', '')                                                                                                     
            SET @strSql = @strSql + ' left Join ListDetails LstCF on LstCF.numListItemID=CFW.Fld_Value  '                  
            SET @columnName = 'LstCF.vcData' 
        END        

		IF @columnName = 'CMP.vcPerformance'   
		BEGIN
			SET @columnName = 'TempPerformance.monDealAmount'
		END

		SET @StrSql = @StrSql + ' WHERE (numCompanyType = 0 OR numCompanyType=46) 
									AND (ISNULL(ADC.bitPrimaryContact,0) = 1 OR ADC.numContactID IS NULL)
									AND CMP.numDomainID=DM.numDomainID   
									AND DM.tintCRMType= ' + CONVERT(VARCHAR(2), @CRMType) + ' AND DM.numDomainID = ' + CONVERT(VARCHAR(20),@numDomainID)


		IF @tintUserRightType = 1 
		BEGIN
			SET @strSql = @strSql + CONCAT(' AND (DM.numRecOwner = ',@numUserCntID,' or DM.numAssignedTo=',@numUserCntID,' or ',@strShareRedordWith,')')                                                                     
		END
		ELSE IF @tintUserRightType = 2 
		BEGIN
			SET @strSql = @strSql + CONCAT(' AND (DM.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= ',@numUserCntID,' ) or DM.numAssignedTo=',@numUserCntID,' or ',@strShareRedordWith,')')
		END

		IF LEN(@SearchQuery) > 0
		BEGIN
			SET @StrSql = @StrSql + ' AND (' + @SearchQuery + ') '
		END	

		IF @SortChar <> '0' 
		BEGIN
			SET @strSql = @strSql + ' And CMP.vcCompanyName like ''' + @SortChar + '%'' '                                                               
		END

		IF LEN(@SearchQuery) > 0
		BEGIN
			IF @tintSortOrder = 1 
				SET @strSql = @strSql + ' AND DM.numStatusID=2 '                                                              
			ELSE IF @tintSortOrder = 2 
				SET @strSql = @strSql + ' AND DM.numStatusID=3 '                          
			ELSE IF @tintSortOrder = 3 
                SET @strSql = @strSql + ' AND (DM.numRecOwner = '
                    + CONVERT(VARCHAR(15), @numUserCntID)
                    + ' or DM.numAssignedTo='
                    + CONVERT(VARCHAR(15), @numUserCntID) + ' or ' + @strShareRedordWith +')'                                                                                                                                                     
            ELSE IF @tintSortOrder = 6 
                    SET @strSql = @strSql + ' AND DM.bintCreatedDate > '''
                            + CONVERT(VARCHAR(20), DATEADD(day, -7,
                                                           GETUTCDATE()))
                            + ''''                                                                    
            ELSE IF @tintSortOrder = 7 
                     SET @strSql = @strSql + ' AND DM.numCreatedby=' + CONVERT(VARCHAR(15), @numUserCntID)                                                                              
            ELSE IF @tintSortOrder = 8 
                     SET @strSql = @strSql + ' AND DM.numModifiedby=' + CONVERT(VARCHAR(15), @numUserCntID) 
			 IF @numProfile <> 0 
					SET @strSQl = @strSql + ' AND cmp.vcProfile = '
            + CONVERT(VARCHAR(15), @numProfile) 
		END
		ELSE
		BEGIN
			IF @tintSortOrder = 1 
				SET @strSql = @strSql + ' AND DM.numStatusID=2 '                                                              
			ELSE IF @tintSortOrder = 2 
				SET @strSql = @strSql + ' AND DM.numStatusID=3 '                          
			ELSE IF @tintSortOrder = 3 
                SET @strSql = @strSql + ' AND (DM.numRecOwner = '
                    + CONVERT(VARCHAR(15), @numUserCntID)
                    + ' or DM.numAssignedTo='
                    + CONVERT(VARCHAR(15), @numUserCntID) + ' or ' + @strShareRedordWith +')'                                                                                                                                                   
            ELSE IF @tintSortOrder = 6 
                    SET @strSql = @strSql + ' AND DM.bintCreatedDate > '''
                            + CONVERT(VARCHAR(20), DATEADD(day, -7,
                                                           GETUTCDATE()))
                            + ''''                                                                    
            ELSE IF @tintSortOrder = 7 
                     SET @strSql = @strSql + ' AND DM.numCreatedby=' + CONVERT(VARCHAR(15), @numUserCntID)                                                                              
            ELSE IF @tintSortOrder = 8 
                     SET @strSql = @strSql + ' AND DM.numModifiedby=' + CONVERT(VARCHAR(15), @numUserCntID) 
			 IF @numProfile <> 0 
        SET @strSQl = @strSql + ' AND cmp.vcProfile = '
            + CONVERT(VARCHAR(15), @numProfile) 
		END

		IF @vcRegularSearchCriteria<>'' 
		BEGIN
			IF PATINDEX('%cmp.vcPerformance%', @vcRegularSearchCriteria)>0
			BEGIN
				-- WE ARE MANAGING CONDITION IN OUTER APPLY
				set @strSql=@strSql
			END
			ELSE
				set @strSql=@strSql+' and ' + @vcRegularSearchCriteria 
		END


		IF @vcCustomSearchCriteria <> '' 
			SET @strSql=@strSql + ' AND ' + @vcCustomSearchCriteria
                                            


		
		DECLARE @firstRec AS INTEGER                                                              
		DECLARE @lastRec AS INTEGER                                                              
		SET @firstRec = ( @CurrentPage - 1 ) * @PageSize                                                              
		SET @lastRec = ( @CurrentPage * @PageSize + 1 )     
		
		DECLARE @strTotal NVARCHAR(MAX) = 'SELECT @TotalRecords = COUNT(*) ' + @StrSql
		exec sp_executesql @strTotal, N'@TotalRecords INT OUT', @TotRecs OUT                                                                   
       
		-- EXECUTE FINAL QUERY
		DECLARE @strFinal AS NVARCHAR(MAX)
		SET @strFinal = CONCAT('SELECT ROW_NUMBER() OVER(ORDER BY ',@columnName,' ',@columnSortOrder,') ID, ',@strColumns,' INTO #tempTable',@StrSql,'; SELECT @TotalRecords = COUNT(*) FROM #tempTable; SELECT * FROM #tempTable WHERE ID > ',@firstRec,' and ID <',@lastRec,' ORDER BY ID; DROP TABLE #tempTable;')
	
		PRINT @strFinal
		exec sp_executesql @strFinal, N'@TotalRecords INT OUT', @TotRecs OUT

		SELECT * FROM #tempForm

		DROP TABLE #tempForm

		DROP TABLE #tempColorScheme 
		 
END	
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetShippingBox')
DROP PROCEDURE USP_GetShippingBox
GO
CREATE PROCEDURE USP_GetShippingBox
    @ShippingReportId NUMERIC(9)
AS 
    BEGIN
        SELECT DISTINCT
                [numBoxID],
                [vcBoxName],
                [numShippingReportId],
                CASE WHEN [fltTotalWeight] = 0 THEN SUM([fltWeight]) ELSE [fltTotalWeight] END AS fltTotalWeight,
                [fltHeight],
                [fltWidth],
                [fltLength],
				dbo.FormatedDateFromDate(dtDeliveryDate,numDomainID) dtDeliveryDate,
				ISNULL(monShippingRate,0) [monShippingRate],
				vcShippingLabelImage,
				ISNULL(vcTrackingNumber,'') AS vcTrackingNumber,
				tintServiceType,--Your Packaging
				tintPayorType,
				vcPayorAccountNo,
				vcPayorZip,
				numPayorCountry,
				numPackageTypeID,
				ISNULL(vcPackageName,'') AS [vcPackageName],
				numServiceTypeID,
				CAST(ROUND(
				(CASE WHEN fltDimensionalWeight > SUM(fltTotalRegularWeight)
				      THEN fltDimensionalWeight 
				      ELSE SUM(fltTotalRegularWeight)
				END), 2) AS DECIMAL(9,2)) AS [fltTotalWeightForShipping],
				fltDimensionalWeight AS [fltTotalDimensionalWeight],
				ISNULL(fltDimensionalWeight,0) AS [fltDimensionalWeight],
				ISNULL(numShipCompany,0) AS [numShipCompany],
				numOppID
--				,intBoxQty
				--ShippingReportItemId
        FROM    View_ShippingBox
        WHERE   [numShippingReportId] = @ShippingReportId
        GROUP BY [numBoxID],
                [vcBoxName],
                [numShippingReportId],
                [fltHeight],
                [fltWidth],
                [fltLength],
                fltTotalWeight,dtDeliveryDate,numOppID,View_ShippingBox.numOppBizDocID,
				monShippingRate,vcShippingLabelImage,
				vcTrackingNumber,numDomainID,
				tintServiceType,tintPayorType,
				vcPayorAccountNo,
				vcPayorZip,
				numPayorCountry,numPackageTypeID,vcPackageName,numServiceTypeID,fltDimensionalWeight,numShipCompany--,intBoxQty
				                
        SELECT  [numBoxID],
                [vcBoxName],
                [ShippingReportItemId],
                [numShippingReportId],
                fltTotalWeightItem [fltTotalWeight],
                fltHeightItem [fltHeight],
                fltWidthItem [fltWidth],
                fltLengthItem [fltLength],
                [numOppBizDocItemID],
                [numItemCode],
                [numoppitemtCode],
                ISNULL([vcItemName],'') vcItemName,
                [vcModelID],
                CASE WHEN LEN (ISNULL(vcItemDesc,''))> 100 THEN  CONVERT(VARCHAR(100),[vcItemDesc]) + '..'
                ELSE ISNULL(vcItemDesc,'') END vcItemDesc,
                intBoxQty,
                numServiceTypeID,
                vcUnitName,monUnitPrice,
				numOppID
        FROM    [View_ShippingBox]
        WHERE   [numShippingReportId] = @ShippingReportId       
                
                
    END
/****** Object:  StoredProcedure [dbo].[USP_GetShippingReport]    Script Date: 05/07/2009 17:34:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--SELECT * FROM ShippingReport
--SELECT * FROM ShippingReportItems
-- USP_GetShippingReport 1,60754,792
-- exec USP_GetShippingReport @numDomainID=72,@numOppBizDocId=5465,@ShippingReportId=4
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetShippingReport' ) 
    DROP PROCEDURE USP_GetShippingReport
GO
CREATE PROCEDURE [dbo].[USP_GetShippingReport]
    @numDomainID NUMERIC(9),
    @numOppBizDocId NUMERIC(9),
    @ShippingReportId NUMERIC(9)
AS 
    BEGIN
  
        SELECT  SRI.numItemCode,
                I.vcItemName,
                I.vcModelID,
                CASE WHEN LEN(BDI.vcItemDesc) > 100
                     THEN CONVERT(VARCHAR(100), BDI.vcItemDesc) + '..'
                     ELSE BDI.vcItemDesc
                END vcItemDesc,
                dbo.FormatedDateFromDate(SRI.dtDeliveryDate, @numDomainID) dtDeliveryDate,
                SRI.tintServiceType,
                ISNULL(I.fltWeight, 0) AS [fltTotalWeight],
                ISNULL(I.[fltHeight], 0) AS [fltHeight],
                ISNULL(I.[fltLength], 0) AS [fltLength],
                ISNULL(I.[fltWidth], 0) AS [fltWidth],
                ISNULL(SRI.monShippingRate,(SELECT monTotAmount FROM dbo.OpportunityItems 
											WHERE SR.numOppID = dbo.OpportunityItems.numOppId
											AND numItemCode = (SELECT numShippingServiceITemID FROM dbo.Domain WHERE numDomainId = @numDomainId))) [monShippingRate],
                SB.vcShippingLabelImage,
                SB.vcTrackingNumber,
                SRI.ShippingReportItemId,
                SRI.intNoOfBox,
                SR.[vcValue3],
                SR.[vcValue4],
                CASE WHEN ISNUMERIC(SR.vcFromState) = 1
                     THEN ( SELECT  vcStateCode
                            FROM    ShippingStateMaster
                            WHERE   vcStateName = ( SELECT TOP 1
                                                            vcState
                                                    FROM    State
                                                    WHERE   numStateID = SR.vcFromState
                                                  )
                                    AND [numShipCompany] = SR.[numShippingCompany]
                          )
                     ELSE ''
                END vcFromState,
                CASE WHEN ISNUMERIC(SR.vcFromCountry) = 1
                     THEN ( SELECT  vcCountryCode
                            FROM    ShippingCountryMaster
                            WHERE   vcCountryName = ( SELECT TOP 1
                                                                vcData
                                                      FROM      ListDetails
                                                      WHERE     numListItemID = SR.vcFromCountry
                                                    )
                                    AND [numShipCompany] = SR.[numShippingCompany]
                          )
                     ELSE ''
                END vcFromCountry,
                CASE WHEN ISNUMERIC(SR.vcToState) = 1
                     THEN ( SELECT  vcStateCode
                            FROM    ShippingStateMaster
                            WHERE   vcStateName = ( SELECT TOP 1
                                                            vcState
                                                    FROM    State
                                                    WHERE   numStateID = SR.vcToState
                                                  )
                                    AND [numShipCompany] = SR.[numShippingCompany]
                          )
                     ELSE ''
                END vcToState,
                CASE WHEN ISNUMERIC(SR.vcToCountry) = 1
                     THEN ( SELECT  vcCountryCode
                            FROM    ShippingCountryMaster
                            WHERE   vcCountryName = ( SELECT TOP 1
                                                                vcData
                                                      FROM      ListDetails
                                                      WHERE     numListItemID = SR.vcToCountry
                                                    )
                                    AND [numShipCompany] = SR.[numShippingCompany]
                          )
                     ELSE ''
                END vcToCountry,
                SR.vcToZip,
                SR.vcFromZip,
                SR.[numShippingCompany],
                SB.[numBoxID],
                SB.[vcBoxName],
                BDI.numOppBizDocItemID,
                SR.tintPayorType,
                ISNULL(SR.vcPayorAccountNo, '') vcPayorAccountNo,
                ISNULL(SR.numPayorCountry, 0) numPayorCountry,
                ( SELECT    vcCountryCode
                  FROM      ShippingCountryMaster
                  WHERE     vcCountryName = ( SELECT TOP 1
                                                        vcData
                                              FROM      ListDetails
                                              WHERE     numListItemID = SR.numPayorCountry
                                            )
                            AND [numShipCompany] = SR.[numShippingCompany]
                ) vcPayorCountryCode,
                SR.vcPayorZip,
                SR.vcFromCity,
                SR.vcFromAddressLine1,
                SR.vcFromAddressLine2,
                ISNULL(SR.vcFromCountry,0) [numFromCountry],
                ISNULL(SR.vcFromState,0) [numFromState],
                SR.vcToCity,
                SR.vcToAddressLine1,
                SR.vcToAddressLine2,
                ISNULL(SR.vcToCountry,0) [numToCountry],
                ISNULL(SR.vcToState,0) [numToState],
                SRI.intBoxQty,
                ISNULL(SB.numServiceTypeID, 0) AS [numServiceTypeID],
                ISNULL(SR.vcFromCompany, '') AS vcFromCompany,
                ISNULL(SR.vcFromName, '') AS vcFromName,
                ISNULL(SR.vcFromPhone, '') AS vcFromPhone,
                ISNULL(SR.vcToCompany, '') AS vcToCompany,
                ISNULL(SR.vcToName, '') AS vcToName,
                ISNULL(SR.vcToPhone, '') AS vcToPhone,
                ISNULL(SB.fltDimensionalWeight, 0) AS [fltDimensionalWeight],
                dbo.fn_GetContactName(SR.numCreatedBy) + ' : '
                + CONVERT(VARCHAR(20), SR.dtCreateDate) AS [CreatedDetails],
                CASE WHEN SR.numShippingCompany = 91 THEN 'Fedex'
                     WHEN SR.numShippingCompany = 88 THEN 'UPS'
                     WHEN SR.numShippingCompany = 90 THEN 'USPS'
                     ELSE 'Other'
                END AS [vcShipCompany],
                CAST(ROUND(( ISNULL(SRI.fltTotalWeight,0) * intBoxQty ), 2) AS DECIMAL(9, 2)) AS [fltTotalRegularWeight],
                CAST(ROUND(ISNULL(fltDimensionalWeight,0), 2) AS DECIMAL(9, 2)) AS [fltTotalDimensionalWeight],
                ISNULL(monTotAmount, 0) AS [monTotAmount],
                ISNULL(bitFromResidential,0) [bitFromResidential],
                ISNULL(bitToResidential,0) [bitToResidential],
                ISNULL(IsCOD,0) [IsCOD],
				ISNULL(IsDryIce,0) [IsDryIce],
				ISNULL(IsHoldSaturday,0) [IsHoldSaturday],
				ISNULL(IsHomeDelivery,0) [IsHomeDelivery],
				ISNULL(IsInsideDelevery,0) [IsInsideDelevery],
				ISNULL(IsInsidePickup,0) [IsInsidePickup],
				ISNULL(IsReturnShipment,0) [IsReturnShipment],
				ISNULL(IsSaturdayDelivery,0) [IsSaturdayDelivery],
				ISNULL(IsSaturdayPickup,0) [IsSaturdayPickup],
				ISNULL(IsAdditionalHandling,0) [IsAdditionalHandling],
				ISNULL(IsLargePackage,0) [IsLargePackage],
				ISNULL(vcCODType,0) [vcCODType],
				SR.numOppID,
				ISNULL(numCODAmount,0) [numCODAmount],
				ISNULL([SR].[numTotalInsuredValue],0) AS [numTotalInsuredValue],
				ISNULL([SR].[numTotalCustomsValue],0) AS [numTotalCustomsValue],
				ISNULL(tintSignatureType,0) AS tintSignatureType,
				OpportunityMaster.numShipFromWarehouse,
				ISNULL(vcDeliveryConfirmation,0) AS vcDeliveryConfirmation,
				ISNULL(SR.vcDescription,'') AS vcDescription
        FROM    ShippingReport AS SR
				INNER JOIN OpportunityMaster ON SR.numOppID = OpportunityMaster.numOppId
                INNER JOIN ShippingBox SB ON SB.numShippingReportId = SR.numShippingReportId
                INNER JOIN ShippingReportItems AS SRI ON SR.numShippingReportId = SRI.numShippingReportId
                                                         AND SRI.numBoxID = SB.numBoxID
                INNER JOIN OpportunityBizDocItems BDI ON SRI.numItemCode = BDI.numItemCode
                                                         AND SR.numOppBizDocId = BDI.numOppBizDocID
                INNER JOIN Item I ON BDI.numItemCode = I.numItemCode
        WHERE   SR.[numOppBizDocId] = @numOppBizDocId
                AND SR.numDomainId = @numDomainID
                AND SR.[numShippingReportId] = @ShippingReportId
--           AND (SB.bitIsMasterTrackingNo = 1 OR SB.bitIsMasterTrackingNo IS NULL)
           
        SELECT  vcBizDocID
        FROM    [OpportunityBizDocs]
        WHERE   [numOppBizDocsId] = @numOppBizDocId

    END

GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_InboxItems' ) 
    DROP PROCEDURE USP_InboxItems
GO
Create PROCEDURE [dbo].[USP_InboxItems]                                                                                              
@PageSize [int] ,                            
@CurrentPage [int],                            
@srch as varchar(100) = '',                            
--@vcSubjectFromTo as varchar(100) ='',
--@srchAttachmenttype as varchar(100) ='',                                   
@ToEmail as varchar (100)  ,        
@columnName as varchar(50) ,        
@columnSortOrder as varchar(4)  ,  
@numDomainId as numeric(9),  
@numUserCntId as numeric(9)  ,    
@numNodeId as numeric(9),  
--@chrSource as char(1),
@ClientTimeZoneOffset AS INT=0,
@tintUserRightType AS INT,
@EmailStatus AS NUMERIC(9)=0,
@srchFrom as varchar(100) = '', 
@srchTo as varchar(100) = '', 
@srchSubject as varchar(100) = '', 
@srchHasWords as varchar(100) = '', 
@srchHasAttachment as bit = 0, 
@srchIsAdvancedsrch as bit = 0,
@srchInNode as numeric(9)=0,
@FromDate AS DATE = NULL,
@ToDate AS DATE = NULL,
@dtSelectedDate AS DATE =NULL,
@bitExcludeEmailFromNonBizContact BIT = 0
as                                                      

--GET ENTERIES FROM PERTICULAR DOMAIN FROM VIEW_Email_Alert_Config
SELECT numDivisionID,numContactId,TotalBalanceDue,OpenSalesOppCount, OpenCaseCount, OpenProjectCount, UnreadEmailCount,OpenActionItemCount INTO #TEMP FROM VIEW_Email_Alert_Config WHERE numDomainId=@numDomainId

/************* START - GET ALERT CONFIGURATION **************/
DECLARE @bitOpenActionItem AS BIT = 1
DECLARE @bitOpencases AS BIT = 1
DECLARE @bitOpenProject AS BIT = 1
DECLARE @bitOpenSalesOpp AS BIT = 1
DECLARE @bitBalancedue AS BIT = 1
DECLARE @bitUnreadEmail AS BIT = 1
DECLARE @bitCampaign AS BIT = 1

SELECT  
	@bitOpenActionItem = ISNULL([bitOpenActionItem],1),
    @bitOpencases = ISNULL([bitOpenCases],1),
    @bitOpenProject = ISNULL([bitOpenProject],1),
    @bitOpenSalesOpp = ISNULL([bitOpenSalesOpp],1),
    @bitBalancedue = ISNULL([bitBalancedue],1),
    @bitUnreadEmail = ISNULL([bitUnreadEmail],1),
	@bitCampaign = ISNULL([bitCampaign],1)
FROM    
	AlertConfig
WHERE   
	AlertConfig.numDomainId = @numDomainId AND 
	AlertConfig.numContactId = @numUserCntId

/************* END - GET ALERT CONFIGURATION **************/
                                                      
 ---DECLARE @CRMType NUMERIC 
DECLARE @tintOrder AS TINYINT                                                      
DECLARE @vcFieldName AS VARCHAR(50)                                                      
DECLARE @vcListItemType AS VARCHAR(1)                                                 
DECLARE @vcAssociatedControlType VARCHAR(20)                                                      
DECLARE @numListID AS NUMERIC(9)                                                      
DECLARE @vcDbColumnName VARCHAR(20)                          
DECLARE @WhereCondition VARCHAR(2000)                           
DECLARE @vcLookBackTableName VARCHAR(2000)                    
DECLARE @bitCustom AS BIT
DECLARE @bitAllowEdit AS CHAR(1)                 
DECLARE @numFieldId AS NUMERIC  
DECLARE @bitAllowSorting AS CHAR(1)              
DECLARE @vcColumnName AS VARCHAR(500)  

DECLARE @strSql AS VARCHAR(5000)
DECLARE @column AS VARCHAR(50)                             
DECLARE @join AS VARCHAR(400) 
DECLARE @Nocolumns AS TINYINT               
DECLARE @lastRec AS INTEGER 
DECLARE @firstRec AS INTEGER                   
SET @join = ''           
SET @strSql = ''
--DECLARE @ClientTimeZoneOffset AS INT 
--DECLARE @numDomainId AS NUMERIC(18, 0)
SET @tintOrder = 0  
SET @WhereCondition = ''

SET @firstRec = ( @CurrentPage - 1 ) * @PageSize                                                              
SET @lastRec = ( @CurrentPage * @PageSize + 1 )   
SET @column = @columnName                
--DECLARE @join AS VARCHAR(400)                    
SET @join = ''             
IF @columnName LIKE 'Cust%' 
    BEGIN                  
        SET @join = ' left Join CFW_FLD_Values_Email CFW on CFW.RecId=DM.numEmailHstrID and CFW.fld_id= '
            + REPLACE(@columnName, 'Cust', '') + ' '                                                           
        SET @column = 'CFW.Fld_Value'
    END                                           
ELSE 
    IF @columnName LIKE 'DCust%' 
        BEGIN                  
            SET @join = ' left Join CFW_FLD_Values_Email CFW on CFW.RecId=DM.numEmailHstrID  and CFW.fld_id= '
                + REPLACE(@columnName, 'DCust', '')                                                                                                     
            SET @join = @join
                + ' left Join ListDetails LstCF on LstCF.numListItemID=CFW.Fld_Value  '                  
            SET @column = 'LstCF.vcData'                    
                  
        END            
    ELSE 
        BEGIN            
            DECLARE @lookbckTable AS VARCHAR(50)                
            SET @lookbckTable = ''                                                         
            SELECT  @lookbckTable = vcLookBackTableName
            FROM    View_DynamicDefaultColumns
            WHERE   numFormId = 44
                    AND vcDbColumnName = @columnName  and numDomainID=@numDOmainID               
            IF @lookbckTable <> '' 
                BEGIN                
                    IF @lookbckTable = 'EmailHistory' 
                        SET @column = 'ADC.' + @column                
                END                                                              
        END              
--DECLARE @strSql AS VARCHAR(5000)   
--SET @numNodeID = 0  
--SET @srchBody = ''
--SET @vcSubjectFromTo =''
--PRINT 'numnodeid' + CONVERT(VARCHAR, @numNodeId)                                 
SET @strSql = '
with FilterRows as (SELECT TOP('+ CONVERT(varchar,@CurrentPage) + ' * '+ CONVERT(varchar,@PageSize) + ') '               
SET @strSql = @strSql + 'ROW_NUMBER() OVER(ORDER BY EH.bintCreatedOn DESC ) AS RowNumber,COUNT(*) OVER(PARTITION BY NULL) AS TotalRowCount,EH.numEmailHstrID,isnull(EM.numContactId,0) as numContactId'
SET @strSql = @strSql + ' FROM [EmailHistory] EH JOIN EmailMaster EM on EM.numEMailId=(CASE numNodeID WHEN 4 THEN SUBSTRING(EH.vcTo, 1, CHARINDEX(''$^$'', EH.vcTo, 1) - 1) ELSE EH.numEmailId END)'
SET @strSql = @strSql + ' 
WHERE EH.numDomainID ='+ CONVERT(VARCHAR,@numDomainId) +' And [numUserCntId] = '+ CONVERT(VARCHAR,@numUserCntId) 

IF LEN(ISNULL(@srch,'')) = 0 AND @dtSelectedDate IS NOT NULL AND LEN(@dtSelectedDate) > 0
BEGIN
	SET @strSql = @strSql + ' AND CAST(DATEADD(MINUTE,'+ CAST(@ClientTimeZoneOffset AS VARCHAR) +' * -1,EH.dtReceivedOn) AS DATE) = '''+ CONVERT(VARCHAR,@dtSelectedDate) + '''' + ' ' 
END

SET @strSql = @strSql + ' AND chrSource IN(''B'',''I'') '

IF ISNULL(@bitExcludeEmailFromNonBizContact,0) = 1
BEGIN
	SET @strSql = @strSql + ' AND ISNULL(EM.numContactId,0) > 0 '
END
 
--Simple Search for All Node
IF @srchIsAdvancedsrch=0 
BEGIN
if len(@srch)>0
	BEGIN
		SET @strSql = @strSql + '
			AND (EH.vcSubject LIKE ''%'' + '''+ @srch +''' + ''%'' or EH.vcBodyText LIKE ''%'' + '''+ @srch + ''' + ''%''
			or EH.vcFrom LIKE ''%'' + '''+ @srch + ''' + ''%'' or EH.vcTo LIKE ''%'' + '''+ @srch + ''' + ''%'')'
	END
ELSE
	BEGIN --Particular Node
		SET @strSql = @strSql + ' AND [numNodeID] = '+ CONVERT(VARCHAR,@numNodeID) +' '
	END
END
--Advanced Search for All Node or selected Node
Else IF @srchIsAdvancedsrch=1
BEGIN
declare @strCondition as varchar(2000);set @strCondition=''

if len(@srchSubject)>0
	SET @strCondition='EH.vcSubject LIKE ''%'' + '''+ @srchSubject +''' + ''%'''

if len(@srchFrom)>0
	begin
		if len(@strCondition)>0
			SET @strCondition=@strCondition + ' and '

		SET @strCondition = @strCondition + ' (EH.vcFrom LIKE ''%' + REPLACE(@srchFrom,',','%'' OR EH.vcFrom LIKE ''%') + '%'')'
	end

if len(@srchTo)>0
	begin
		if len(@strCondition)>0
			SET @strCondition=@strCondition + ' and '

		SET @strCondition = @strCondition + ' (EH.vcTo LIKE ''%' + REPLACE(@srchTo,',','%'' OR EH.vcTo LIKE ''%') + '%'')' 
	end

if len(@srchHasWords)>0
	begin
		if len(@strCondition)>0
			SET @strCondition=@strCondition + ' and '

		SET @strCondition = @strCondition + ' EH.vcBodyText LIKE ''%'' + '''+ @srchHasWords + ''' + ''%'''
	end

IF @srchHasAttachment=1
BEGIN
	if len(@strCondition)>0
			SET @strCondition=@strCondition + ' and '
	
	SET @strCondition = @strCondition + ' EH.bitHasAttachments =  '''+ CONVERT(VARCHAR,@srchHasAttachment) + ''''
END

IF (LEN(@FromDate) > 0 AND  ISDATE(CAST(@FromDate AS VARCHAR(100))) = 1)
BEGIN
	IF LEN(@strCondition)>0
		SET @strCondition=@strCondition + ' AND '

	SET @strCondition = @strCondition + ' EH.dtReceivedOn >= '''+ CONVERT(VARCHAR,@FromDate) + ''''
END

IF (LEN(@ToDate) > 0 AND ISDATE(CAST(@ToDate AS VARCHAR(100))) = 1)
BEGIN
	IF LEN(@strCondition)>0
		SET @strCondition=@strCondition + ' AND '

	SET @strCondition = @strCondition + ' EH.dtReceivedOn <= ''' + CONVERT(VARCHAR,@ToDate) + ''''
END

if len(@strCondition)>0
	BEGIN
		SET @strSql = @strSql +' and (' + @strCondition + ')'
		
		if @numNodeId>-1
			SET @strSql = @strSql + ' AND [numNodeID] = '+ CONVERT(VARCHAR,@numNodeId) +' '
	END
ELSE
	BEGIN --Particular Node
		SET @strSql = @strSql + ' AND [numNodeID] = '+ CONVERT(VARCHAR,@numNodeID) +' '
	END



--SET @strSql = @strSql + '
--AND (EH.vcSubject LIKE ''%'' + '''+ @srchSubject +''' + ''%'' and EH.vcFrom LIKE ''%'' + '''+ @srchFrom + ''' + ''%'' 
--and EH.vcTo LIKE ''%'' + '''+ @srchTo + ''' + ''%'' and EH.vcBodyText LIKE ''%'' + '''+ @srchHasWords + ''' + ''%'''
--
--IF @srchHasAttachment=1
--BEGIN
--SET @strSql = @strSql + ' and EH.bitHasAttachments =  '''+ CONVERT(VARCHAR,@srchHasAttachment) + ''''
--END

END


SET @strSql = @strSql + ' )'
--,FinalResult As
--( SELECT  TOP ('+CONVERT(VARCHAR,@PageSize)+ ') numEmailHstrID'
--SET @strSql = @strSql + ' 
--From FilterRows'
--SET @strSql = @strSql + '
--WHERE RowNumber > (('+ CONVERT(VARCHAR,@CurrentPage) + ' - 1) * '+ CONVERT(VARCHAR,@PageSize) +' ))'
--PRINT @strSql
--EXEC(@strSql)

select @Nocolumns=isnull(sum(TotalRow),0)  from(            
Select count(*) TotalRow from View_DynamicColumns where numFormId=44 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=0 
Union 
Select count(*) TotalRow from View_DynamicCustomColumns where numFormId=44 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=0) TotalRows

  CREATE TABLE #tempForm (tintOrder TINYINT,vcDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),
  vcListItemType char(3),numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,
  numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,bitIsAlphaNumeric BIT,bitIsNumeric BIT,
  bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),ListRelID numeric(9))


---Insert number of rows into temp table.
PRINT 'number OF columns :' + CONVERT(VARCHAR, @Nocolumns)
IF  @Nocolumns > 0 
    BEGIN    

INSERT INTO #tempForm
select tintRow+1 as tintOrder,vcDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
 vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName                                                 
,bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID 
 FROM View_DynamicColumns 
 where numFormId=44 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=0
  AND ISNULL(bitSettingField,0)=1 AND ISNULL(bitCustom,0)=0
                  
    UNION

     select tintRow+1 as tintOrder,vcDbColumnName,vcFieldName,                  
 vcAssociatedControlType,'' as vcListItemType,numListID,''                                                 
,bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID
 from View_DynamicCustomColumns
 where numFormId=44 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=0
 AND ISNULL(bitCustom,0)=1
 
 ORDER BY tintOrder ASC  
   
 END 
ELSE 
    BEGIN

 INSERT INTO #tempForm
select tintOrder,vcDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
 vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName                                                 
,bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID 
 FROM View_DynamicDefaultColumns
 where numFormId=44 and bitDefault=1 AND ISNULL(bitSettingField,0)=1 AND numDomainID=@numDomainID
order by tintOrder asc   

    END
      SET @strSql =@strSql + ' Select TotalRowCount, EH.numEmailHstrID ,EH.numListItemId,bitIsRead,CASE WHEN LEN(Cast(EH.vcBodyText AS VARCHAR(1000)))>150 THEN SUBSTRING(EH.vcBodyText,0,150) + ''...'' ELSE
		 EH.vcBodyText END as vcBodyText,EH.numEmailHstrID As [KeyId~numEmailHstrID~0~0~0~0~HiddenField],FR.numContactId AS [ContactId~numContactID~0~0~0~0~HiddenField] ,EH.bitIsRead As [IsRead~bitIsRead~0~0~0~0~Image],isnull(EH.IsReplied,0) As [~IsReplied~0~0~0~0~Image],
		 isnull(EH.numNoofTimes,0) AS [NoofTimeOpened~numNoofTimes~0~0~0~0~HiddenField] '
 
Declare @ListRelID as numeric(9) 

   select top 1 @tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,                
  @vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,@numListID=numListID,@vcLookBackTableName=vcLookBackTableName                                               
,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,@ListRelID=ListRelID
  from  #tempForm order by tintOrder asc            

WHILE @tintOrder > 0                                                      
    BEGIN                                                      
        IF @bitCustom = 0  
            BEGIN            
                DECLARE @Prefix AS VARCHAR(5)                        
                IF @vcLookBackTableName = 'EmailHistory' 
                    SET @Prefix = 'EH.'                        
                SET @vcColumnName = @vcFieldName + '~' + @vcDbColumnName
                    + '~' + @bitAllowSorting + '~'
                    + CONVERT(VARCHAR(10), @numFieldId) + '~'
                    + @bitAllowEdit + '~' + CONVERT(VARCHAR(1), @bitCustom)
                    + '~' + @vcAssociatedControlType
                PRINT @vcColumnName
                
                IF @vcAssociatedControlType = 'SelectBox' 
                    BEGIN      
                        PRINT @vcListItemType                                                    
                        IF @vcListItemType = 'L' 
                            BEGIN    
                            ---PRINT 'columnName' + @vcColumnName
                                SET @strSql = @strSql + ',L'
                                    + CONVERT(VARCHAR(3), @tintOrder)
                                    + '.vcData' + ' [' + @vcColumnName + ']'                                                         
                                SET @WhereCondition = @WhereCondition
                                    + ' left Join ListDetails L'
                                    + CONVERT(VARCHAR(3), @tintOrder)
                                    + ' on L' + CONVERT(VARCHAR(3), @tintOrder)
                                    + '.numListItemID=EH.numListItemID'                                                           
                            END 
                           
                    END 
                  ELSE IF (@vcAssociatedControlType = 'TextBox'
                        OR @vcAssociatedControlType = 'TextArea'
                        OR @vcAssociatedControlType = 'Image')  AND @vcDbColumnName<>'AlertPanel' AND @vcDbColumnName<>'RecentCorrespondance'
                        BEGIN
							IF @numNodeId=4 and @vcDbColumnName='vcFrom'  
							BEGIN
								  SET @vcColumnName = 'To' + '~' + @vcDbColumnName
                    + '~' + @bitAllowSorting + '~'
                    + CONVERT(VARCHAR(10), @numFieldId) + '~'
                    + @bitAllowEdit + '~' + CONVERT(VARCHAR(1), @bitCustom)
                    + '~' + @vcAssociatedControlType

								 SET @strSql = @strSql + ',' + @Prefix  + 'vcTo' + ' [' + @vcColumnName + ']'   
							END 
							ELSE
							BEGIN
								SET @strSql = @strSql + ',' + @Prefix  + @vcDbColumnName + ' [' + @vcColumnName + ']'   
							END                        
                             PRINT @Prefix   
                        END 
				Else IF  @vcDbColumnName='AlertPanel'   
							BEGIN 
								SET @strSql = @strSql +  ', dbo.GetAlertDetail(EH.numEmailHstrID, ACI.numDivisionID, ACI.numECampaignID, FR.numContactId,' 
													  + CAST(@bitOpenActionItem AS VARCHAR(10)) + ',' 
													  + CAST(@bitOpencases AS VARCHAR(10)) + ',' 
													  + CAST(@bitOpenProject AS VARCHAR(10)) + ',' 
													  + CAST(@bitOpenSalesOpp AS VARCHAR(10)) + ',' 
													  + CAST(@bitBalancedue AS VARCHAR(10)) + ',' 
													  + CAST(@bitUnreadEmail AS VARCHAR(10)) + ',' 
													  + CAST(@bitCampaign AS VARCHAR(10)) + ',' +
													  'V1.TotalBalanceDue,V1.OpenSalesOppCount,V1.OpenCaseCount,V1.OpenProjectCount,V1.UnreadEmailCount,V2.OpenActionItemCount,V3.CampaignDTLCount) AS  [' + '' + @vcColumnName + '' + ']' 
							 END 
                 ELSE IF  @vcDbColumnName='RecentCorrespondance'
							BEGIN 
					            --SET @strSql = @strSql + ','+ '(SELECT COUNT(*) FROM dbo.Communication WHERE numContactId = FR.numContactId)' + ' AS  [' + '' + @vcColumnName + '' + ']'                                                       
					            SET @strSql = @strSql + ',0 AS  [' + '' + @vcColumnName + '' + ']'
                             END 
--				else if @vcAssociatedControlType='DateField'                                                  
--					begin            
--							set @strSql=@strSql+',case when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+') )= convert(varchar(11),getdate()) then ''<b><font color=red>Today</font></b>'''      
--							set @strSql=@strSql+'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+') )= convert(varchar(11),dateadd(day,-1,getdate())) then ''<b><font color=purple>YesterDay</font></b>'''      
--							set @strSql=@strSql+'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+') )= convert(varchar(11),dateadd(day,1,getdate())) then''<b><font color=orange>Tommorow</font></b>'' ' 
--							set @strSql=@strSql+'else dbo.FormatedDateFromDate(DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+'),'+convert(varchar(10),@numDomainId)+') end  ['+ @vcColumnName+']'
--				    end    
				else if @vcAssociatedControlType='DateField' AND @vcDbColumnName <> 'dtReceivedOn'                                     
					begin           
							set @strSql=@strSql+',case when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+') )= convert(varchar(11),getdate()) then ''<b><font color=red>Today</font></b>'''      
							set @strSql=@strSql+'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+') )= convert(varchar(11),dateadd(day,-1,getdate())) then ''<b><font color=purple>YesterDay</font></b>'''      
							set @strSql=@strSql+'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+') )= convert(varchar(11),dateadd(day,1,getdate())) then''<b><font color=orange>Tommorow</font></b>'' ' 
							set @strSql=@strSql+'else dbo.FormatedDateFromDate(DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+'),'+convert(varchar(10),@numDomainId)+') end  ['+ @vcColumnName+']'
				    end    
				else if @vcAssociatedControlType='DateField' AND @vcDbColumnName = 'dtReceivedOn'                                                 
					begin    
							set @strSql=@strSql+',case when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+') )= convert(varchar(11),getdate()) then ''<b><font color=red>Today</font></b>'''      
							set @strSql=@strSql+'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+') )= convert(varchar(11),dateadd(day,-1,getdate())) then ''<b><font color=purple>YesterDay</font></b>'''      
							set @strSql=@strSql+'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+') )= convert(varchar(11),dateadd(day,1,getdate())) then''<b><font color=orange>Tommorow</font></b>'' ' 
							set @strSql=@strSql+'else CONVERT(VARCHAR(20),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+'),100) end  ['+ @vcColumnName+']'
				    end   
			END 
else if @bitCustom = 1                
begin                
            
               SET @vcColumnName= @vcFieldName+'~'+ @vcDbColumnName+'~0~'+ convert(varchar(10),@numFieldId)+'~1~'+ convert(varchar(1),@bitCustom) +'~'+ @vcAssociatedControlType
       
   select @vcFieldName=FLd_label,@vcAssociatedControlType=fld_type,@vcDbColumnName='Cust'+Convert(varchar(10),Fld_Id)                               
    from CFW_Fld_Master                                                
   where    CFW_Fld_Master.Fld_Id = @numFieldId                 
                
                 
    if @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'               
   begin                
                   
    set @strSql= @strSql+',CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value  ['+ @vcColumnName +']'                   
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Cont CFW'+ convert(varchar(3),@tintOrder)+ '                 
    on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=FR.numContactId   '                                                         
   end      
    else if @vcAssociatedControlType = 'CheckBox'                 
   begin              
                 
    set @strSql= @strSql+',case when isnull(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,0)=0 then ''No'' when isnull(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,0)=1 then ''Yes'' end   ['+ @vcColumnName +']'              
   
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Cont CFW'+ convert(varchar(3),@tintOrder)+ '               
    on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=FR.numContactId   '                                                       
   end              
  else if @vcAssociatedControlType = 'DateField'              
   begin                
                   
    set @strSql= @strSql+',dbo.FormatedDateFromDate(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,'+convert(varchar(10),@numDomainId)+')  ['+ @vcColumnName +']'                   
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Cont CFW'+ convert(varchar(3),@tintOrder)+ '                 
    on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=FR.numContactId   '                                                         
   end                
   else if @vcAssociatedControlType = 'SelectBox'                    
   begin                
    set @vcDbColumnName = 'DCust'+Convert(varchar(10),@numFieldId)                
    set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName +']'                                                          
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Cont CFW'+ convert(varchar(3),@tintOrder)+ '                 
     on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=FR.numContactId   '                                                         
    set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value'                
   end                 
end     
                   --    SET @vcColumnName= @vcFieldName+'~'+ @vcDbColumnName+'~0~'+ convert(varchar(10),@numFieldId)+'~1~'+ convert(varchar(1),@bitCustom) +'~'+ @vcAssociatedControlType   
  select top 1 @tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,                
  @vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,@numListID=numListID,@vcLookBackTableName=vcLookBackTableName                                               
,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,@ListRelID=ListRelID
  from #tempForm WHERE tintOrder > @tintOrder-1 order by tintOrder asc            
 
           IF @@rowcount = 0 SET @tintOrder = 0 
		   PRINT @tintOrder 
    END  
                     
SELECT  * FROM    #tempForm

-------Change Row Color-------
Declare @vcCSOrigDbCOlumnName as varchar(50),@vcCSLookBackTableName as varchar(50),@vcCSAssociatedControlType as varchar(50)
SET @vcCSOrigDbCOlumnName=''
SET @vcCSLookBackTableName=''

Create table #tempColorScheme(vcOrigDbCOlumnName varchar(50),vcLookBackTableName varchar(50),vcFieldValue varchar(50),vcFieldValue1 varchar(50),vcColorScheme varchar(50),vcAssociatedControlType varchar(50))

insert into #tempColorScheme select DFM.vcOrigDbCOlumnName,DFM.vcLookBackTableName,DFCS.vcFieldValue,DFCS.vcFieldValue1,DFCS.vcColorScheme,DFFM.vcAssociatedControlType 
from DycFieldColorScheme DFCS join DycFormField_Mapping DFFM on DFCS.numFieldID=DFFM.numFieldID AND DFCS.numFormID=DFFM.numFormID
join DycFieldMaster DFM on DFM.numModuleID=DFFM.numModuleID and DFM.numFieldID=DFFM.numFieldID
where DFCS.numDomainID=@numDomainID and DFFM.numFormID=10 AND DFCS.numFormID=10 and isnull(DFFM.bitAllowGridColor,0)=1

IF(SELECT COUNT(*) FROM #tempColorScheme)>0
BEGIN
   select top 1 @vcCSOrigDbCOlumnName=vcOrigDbCOlumnName,@vcCSLookBackTableName=vcLookBackTableName,@vcCSAssociatedControlType=vcAssociatedControlType from #tempColorScheme
END   
---------------------------- 

IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
BEGIN
set @strSql=@strSql+',tCS.vcColorScheme'
END  

DECLARE @fltTotalSize FLOAT
SELECT @fltTotalSize=isnull(SUM(ISNULL(SpaceOccupied,0)),0) FROM dbo.View_InboxCount where numDomainID=@numDomainID and numUserCntID=@numUserCntID;

SET @strSql = @strSql + ', '+ CONVERT(VARCHAR(18),@fltTotalSize) +' AS TotalSize From EmailHistory EH Inner Join FilterRows FR on  FR.numEmailHstrID = EH.numEmailHstrID '
--SET @strSql = @strSql + ', '+ ISNULL('EH.numNoofTimes',0) +' AS NoOfTimeOpened From EmailHistory EH Inner Join FilterRows FR on  FR.numEmailHstrID = EH.numEmailHstrID '
SET @strSql=@strSql + ' LEFT JOIN
	AdditionalContactsInformation ACI
ON 
	ACI.numContactId = FR.numContactId
CROSS APPLY
(
	SELECT
		SUM(TotalBalanceDue) TotalBalanceDue,
		SUM(OpenSalesOppCount) OpenSalesOppCount,
		SUM(OpenCaseCount) OpenCaseCount,
		SUM(OpenProjectCount) OpenProjectCount,
		SUM(UnreadEmailCount) UnreadEmailCount
	FROM
		#TEMP
	WHERE
		#TEMP.numDivisionId = ACI.numDivisionId
) AS V1
CROSS APPLY
(
	SELECT
		SUM(OpenActionItemCount) OpenActionItemCount
	FROM
		#TEMP
	WHERE
		#TEMP.numDivisionId = ACI.numDivisionId AND
		#TEMP.numContactId = FR.numContactId
) AS V2
CROSS APPLY
(
	SELECT 
		COUNT(*) AS CampaignDTLCount
	FROM 
		ConECampaignDTL 
	WHERE 
		numConECampID = (SELECT TOP 1 ISNULL(numConEmailCampID,0)  FROM ConECampaign WHERE numECampaignID = ACI.numECampaignID AND numContactID = ACI.numContactId ORDER BY numConEmailCampID DESC) AND 
		ISNULL(bitSend,0) = 0
) AS V3 '
SET @strSql = @strSql + @WhereCondition

IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
BEGIN
	 if @vcCSLookBackTableName = 'AdditionalContactsInformation'                  
		set @Prefix = 'ADC.'                  
     if @vcCSLookBackTableName = 'DivisionMaster'                  
		set @Prefix = 'DM.'
	 if @vcCSLookBackTableName = 'CompanyInfo'                  
		set @Prefix = 'CI.' 
	 if @vcCSLookBackTableName = 'AddressDetails'                  
		set @Prefix = 'AD.'   

set @strSql=@strSql+' left JOIN AdditionalContactsInformation ADC ON ADC.numcontactId=FR.numContactID                                                                             
 left JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID                                                                            
 left JOIN CompanyInfo CI ON DM.numCompanyID = CI.numCompanyId '
   
	IF @vcCSAssociatedControlType='DateField'
	BEGIN
			set @strSql=@strSql+' left join #tempColorScheme tCS on CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) >= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue as int),GETUTCDATE()),111)
			 and CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) <= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue1 as int),GETUTCDATE()),111)'
	END
	ELSE IF @vcCSAssociatedControlType='SelectBox' OR @vcCSAssociatedControlType='ListBox' OR @vcCSAssociatedControlType='CheckBox'
	BEGIN
		set @strSql=@strSql+' left join #tempColorScheme tCS on tCS.vcFieldValue=' + @Prefix + @vcCSOrigDbCOlumnName
	END
END

SET @strSql = @strSql + ' Where RowNumber >'  + CONVERT(VARCHAR(10), @firstRec) + ' and RowNumber <' + CONVERT(VARCHAR(10), @lastRec) 
IF @EmailStatus <> 0 
BEGIN
	SET @strSql = @strSql + ' AND EH.numListItemId = ' + CONVERT(VARCHAR(10), @EmailStatus)
END       
SET @strSql = @strSql + ' order by RowNumber'

PRINT @strSql 
EXEC(@strSql)   
DROP TABLE #tempForm          
DROP TABLE #TEMP
 
 /*
 declare @firstRec as integer                                                      
 declare @lastRec as integer                                                      
 set @firstRec= (@CurrentPage-1) * @PageSize                                                      
 set @lastRec= (@CurrentPage*@PageSize+1)   ;    
 
DECLARE @totalRows  INT
DECLARE @fltTotalSize FLOAT
SELECT @totalRows = RecordCount FROM dbo.View_InboxCount where numDomainID=@numDomainID and numUserCntID=@numUserCntID and numNodeId = @numNodeID ;
SELECT @fltTotalSize=SUM(ISNULL(SpaceOccupied,0)) FROM dbo.View_InboxCount where numDomainID=@numDomainID and numUserCntID=@numUserCntID;



-- Step 1: Get Only rows specific to user and domain 

WITH FilterRows
AS 
(
   SELECT 
   TOP (@CurrentPage * @PageSize)
        ROW_NUMBER() OVER(ORDER BY EH.bintCreatedOn DESC ) AS totrows,
        EH.numEmailHstrID
   FROM [EmailHistory] EH
		--LEFT JOIN EmailHstrAttchDtls A ON EH.numEmailHstrID = A.numEmailHstrID
   WHERE 
	   [numDomainID] = @numDomainID
	AND [numUserCntId] = @numUserCntId
	AND numNodeID = @numNodeId
	AND chrSource IN('B','I')
    AND (EH.vcBodyText LIKE '%' + @srchBody + '%' OR @srchBody = '')
	AND (EH.vcSubject LIKE '%' + @vcSubjectFromTo + '%' OR @vcSubjectFromTo ='')
    --AND (A.[vcAttachmentType] LIKE '%'+ @srchAttachmenttype + '%' OR @srchAttachmenttype='')
   
)
 --select seq1 + totrows1 -1 as TotRows2,* FROM [FilterRows]
,FinalResult
AS 
(
	SELECT  TOP (@PageSize) numEmailHstrID --,seq + totrows -1 as TotRows
	FROM FilterRows
	WHERE totrows > ((@CurrentPage - 1) * @PageSize)
)



-- select * FROM [UserEmail]
select  
		@totalRows TotRows,
		@fltTotalSize AS TotalSize,
		B.[numEmailHstrID],
        B.[numDomainID],
--        B.bintCreatedOn,
--        B.dtReceivedOn,
        B.numEmailHstrID,
        ISNULL(B.numListItemId,-1) AS numListItemId,
        ISNULL(vcSubject, '') AS vcSubject,
        CONVERT(VARCHAR(MAX), vcBody) AS vcBody,
        ISNULL(vcItemId, '') AS ItemId,
        ISNULL(vcChangeKey, '') AS ChangeKey,
        ISNULL(bitIsRead, 'False') AS IsRead,
        ISNULL(vcSize, 0) AS vcSize,
        ISNULL(bitHasAttachments, 'False') AS HasAttachments,
        ISNULL(B.tintType, 1) AS type,
        ISNULL(B.chrSource, 'B') AS chrSource,
        ISNULL(vcCategory, 'white') AS vcCategory,
        CASE B.[numNodeId] WHEN 4 THEN REPLACE(REPLACE(ISNULL(B.[vcTo],''),'<','') , '>','') ELSE B.vcFrom END FromName,
--        dbo.GetEmaillName(B.numEmailHstrID, 4) AS FromName,
--        dbo.GetEmaillName(B.numEmailHstrID, 1) AS ToName,
		CASE B.[numNodeId] WHEN 4 THEN 
			dbo.FormatedDateTimeFromDate(DATEADD(minute, -@ClientTimeZoneOffset,B.bintCreatedOn),B.numDomainId) 
		ELSE 
			dbo.FormatedDateTimeFromDate(B.dtReceivedOn,B.numDomainId)
		END dtReceivedOn,
        ISNULL(CONVERT(VARCHAR(5),B.[numNoofTimes]),'') NoOfTimeOpened
FROM [FinalResult] A INNER JOIN emailHistory B ON B.numEmailHstrID = A.numEmailHstrID;



--WITH UserEmail
--AS
--(
--			   SELECT
--						ROW_NUMBER() OVER ( ORDER BY X.bintCreatedOn DESC ) AS RowNumber,
--                        X.[numEmailHstrID]
--               FROM		[EmailHistory] X
--               WHERE    [numDomainID] = @numDomainID
--						AND [numUserCntId] = @numUserCntId
--						AND numNodeID = @numNodeId
--						AND X.chrSource IN('B','I')
--						
--)
--	SELECT 
--		COUNT(*) 
--   FROM UserEmail UM INNER JOIN [EmailHistory] EH  ON UM.numEmailHstrID =EH.numEmailHstrID
--		LEFT JOIN EmailHstrAttchDtls A ON EH.numEmailHstrID = A.numEmailHstrID
--   WHERE 
--		 (EH.vcBodyText LIKE '%' + @srchBody + '%' OR @srchBody = '')
--	AND (EH.vcSubject LIKE '%' + @vcSubjectFromTo + '%' OR @vcSubjectFromTo ='')
--    AND (A.[vcAttachmentType] LIKE '%'+ @srchAttachmenttype + '%' OR @srchAttachmenttype='')



--SELECT COUNT(*)
--FROM      View_Inbox I INNER JOIN [EmailHistory] EH ON EH.numEmailHstrID = I.numEmailHstrID
--			LEFT JOIN EmailHstrAttchDtls A ON EH.numEmailHstrID = A.numEmailHstrID
-- WHERE     I.numDomainId = @numDomainId
--                                AND I.numUserCntId = @numUserCntId
--                                AND I.numNodeId = @numNodeId
--                                AND I.chrSource IN('B','I')
--                                AND (EH.vcBodyText LIKE '%' + @srchBody + '%' OR @srchBody = '')
--								AND (I.vcSubjectFromTo LIKE '%' + @vcSubjectFromTo + '%' OR @vcSubjectFromTo ='')
--                                AND (A.[vcAttachmentType] LIKE '%'+ @srchAttachmenttype + '%' OR @srchAttachmenttype='')

RETURN ;
*/                                           
/* SELECT Y.[ID],
        Y.[numEmailHstrID],
        emailHistory.[numDomainID],
        emailHistory.bintCreatedOn,
        emailHistory.dtReceivedOn,
        emailHistory.numEmailHstrID,
        ISNULL(vcSubject, '') AS vcSubject,
        CONVERT(VARCHAR(MAX), vcBody) AS vcBody,
        ISNULL(vcItemId, '') AS ItemId,
        ISNULL(vcChangeKey, '') AS ChangeKey,
        ISNULL(bitIsRead, 'False') AS IsRead,
        ISNULL(vcSize, 0) AS vcSize,
        ISNULL(bitHasAttachments, 'False') AS HasAttachments,
        ISNULL(emailHistory.tintType, 1) AS type,
        ISNULL(emailHistory.chrSource, 'B') AS chrSource,
        ISNULL(vcCategory, 'white') AS vcCategory,
        dbo.GetEmaillName(Y.numEmailHstrID, 4) AS FromName,
        dbo.GetEmaillName(Y.numEmailHstrID, 1) AS ToName,
        dbo.FormatedDateFromDate(DATEADD(minute, -@ClientTimeZoneOffset,
                                         emailHistory.bintCreatedOn),
                                 emailHistory.numDomainId) AS bintCreatedOn,
        dbo.FormatedDateFromDate(emailHistory.dtReceivedOn,
                                 emailHistory.numDomainId) AS dtReceivedOn,
        ISNULL(CONVERT(VARCHAR(5),EmailHistory.[numNoofTimes]),'') NoOfTimeOpened
 FROM   ( SELECT   
				ROW_NUMBER() OVER ( ORDER BY X.bintCreatedOn DESC ) AS ID,
				X.[numEmailHstrID]
          FROM      ( SELECT    DISTINCT
                                EH.numEmailHstrID,
                                EH.[bintCreatedOn]
                      FROM      --View_Inbox I INNER JOIN 
                      [EmailHistory] EH --ON EH.numEmailHstrID = I.numEmailHstrID
								LEFT JOIN EmailHstrAttchDtls A ON EH.numEmailHstrID = A.numEmailHstrID
                      WHERE     EH.numDomainId = @numDomainId
                                AND EH.numUserCntId = @numUserCntId
                                AND EH.numNodeId = @numNodeId
                                AND EH.chrSource IN('B','I')
                                AND (EH.vcBodyText LIKE '%' + @srchBody + '%' OR @srchBody = '')
								AND (EH.vcSubject LIKE '%' + @vcSubjectFromTo + '%' OR @vcSubjectFromTo ='')
                                AND (A.[vcAttachmentType] LIKE '%'+ @srchAttachmenttype + '%' OR @srchAttachmenttype='')
                    ) X
          
        ) Y
        INNER JOIN emailHistory ON emailHistory.numEmailHstrID = Y.numEmailHstrID
--        LEFT JOIN EmailHstrAttchDtls ON emailHistory.numEmailHstrID = EmailHstrAttchDtls.numEmailHstrID
        AND Y.ID > @firstRec AND Y.ID < @lastRec
--UNION
--   SELECT   0 AS ID,
--			COUNT(*),
--            NULL,
--            NULL,
--            NULL,
--            NULL,
--            NULL,
--            NULL,
--            NULL,
--            NULL,
--            NULL,
--            NULL,
--            NULL,
----            NULL,
----            NULL,
----            NULL,
--            1,
--            NULL,
--            NULL,
--            NULL,
--            NULL,
--            NULL,
--            NULL,
--            ''
--FROM      View_Inbox I INNER JOIN [EmailHistory] EH ON EH.numEmailHstrID = I.numEmailHstrID
--			LEFT JOIN EmailHstrAttchDtls A ON EH.numEmailHstrID = A.numEmailHstrID
-- WHERE     I.numDomainId = @numDomainId
--                                AND I.numUserCntId = @numUserCntId
--                                AND I.numNodeId = @numNodeId
--                                AND I.chrSource IN('B','I')
--                                AND (EH.vcBodyText LIKE '%' + @srchBody + '%' OR @srchBody = '')
--								AND (I.vcSubjectFromTo LIKE '%' + @vcSubjectFromTo + '%' OR @vcSubjectFromTo ='')
--                                AND (A.[vcAttachmentType] LIKE '%'+ @srchAttachmenttype + '%' OR @srchAttachmenttype='')
--   ORDER BY ID

RETURN 
*/
/*
declare @strSql as varchar(8000)                    
   if @columnName = 'FromName'               
 begin        
 set @columnName =  'dbo.GetEmaillName(emailHistory.numEmailHstrID,4)'        
 end        
 if @columnName = 'FromEmail'        
 begin        
 set @columnName = 'dbo.GetEmaillAdd(emailHistory.numEmailHstrID,4)'        
 end        
  if @columnName = 'ToName'               
 begin        
 set @columnName =  'dbo.GetEmaillName(emailHistory.numEmailHstrID,1)'        
 end        
 if @columnName = 'ToEmail'        
 begin        
 set @columnName = 'dbo.GetEmaillAdd(emailHistory.numEmailHstrID,1)'        
 end 
                  
set @strSql='With tblSubscriber AS (SELECT  distinct(emailHistory.numEmailHstrID) ,            
ROW_NUMBER() OVER (ORDER BY '+@columnName+' '+@columnSortOrder+') AS  RowNumber             
from emailHistory                                
--left join EmailHstrAttchDtls on emailHistory.numEmailHstrID=EmailHstrAttchDtls.numEmailHstrID                     
--join EmailHStrToBCCAndCC on emailHistory.numEmailHstrID=EmailHStrToBCCAndCC.numEmailHstrID                     
--join EmailMaster on        EmailMaster.numEmailId=EmailHStrToBCCAndCC.numEmailId                     
where numDomainId='+convert(varchar(15),@numDomainId)+' and numUserCntId='+convert(varchar(15),@numuserCntId)+  
' and numNodeId='+convert(varchar(15),@numNodeId)  
    
 if @chrSource<> '0'   set @strSql=@strSql +' and (chrSource  = ''B'' or chrSource like ''%'+@chrSource+'%'')'   
           
 if @srchBody <> '' set @strSql=@strSql +' and (vcSubject  like ''%'+@srchBody+'%'' or  
  vcBodyText like ''%'+@srchBody+'%''            
  or emailHistory.numEmailHstrID in (select numEmailHstrId from EmailHStrToBCCAndCC where             
 (numemailid in (select numEmailid from emailmaster where vcemailid like ''%'+@srchBody+'%'')) or   
  vcName like ''%'+@srchBody+'%'') )'                
--if @ToEmail <> '' set @strSql=@strSql +'               
--      and (EmailMaster.vcEmailId like '''+@ToEmail+''' and EmailHStrToBCCAndCC.tintType<>4 )  '            
if @srchAttachmenttype <>'' set @strSql=@strSql +'              
  and  emailHistory.numEmailHstrID in  (select numEmailHstrId from EmailHstrAttchDtls                
  where EmailHstrAttchDtls.vcAttachmentType like ''%'+@srchAttachmenttype+'%'')'            */
            
            
/*Please Do not use GetEmaillAdd function as it recursively fetched email address for single mail which create too bad performance*/                 

/*set @strSql=@strSql +')                     
 select RowNumber,            
--dbo.GetEmaillAdd(emailHistory.numEmailHstrID,4) as FromEmail,                  
dbo.GetEmaillName(emailHistory.numEmailHstrID,4) as FromName,                             
--isnull(vcFromEmail,'''') as FromEmail,                                      
--dbo.GetEmaillAdd(emailHistory.numEmailHstrID,1) as ToEmail,                                 
  
--dbo.GetEmaillAdd(emailHistory.numEmailHstrID,2) as CCEmail,                                
--dbo.GetEmaillAdd(emailHistory.numEmailHstrID,3) as BCCEmail,                                     
emailHistory.numEmailHstrID,                            
isnull(vcSubject,'''') as vcSubject,                                      
convert(varchar(max),vcBody) as vcBody,                              
dbo.FormatedDateFromDate( DATEADD(minute,'+ CONVERT(VARCHAR(15),-@ClientTimeZoneOffset)+',bintCreatedOn),numDomainId) as bintCreatedOn,                              
isnull(vcItemId,'''') as ItemId,                              
isnull(vcChangeKey,'''') as ChangeKey,                              
isnull(bitIsRead,''False'') as IsRead,                  
isnull(vcSize,0) as vcSize,                              
isnull(bitHasAttachments,''False'') as HasAttachments,                             
  isnull(EmailHstrAttchDtls.vcFileName,'''') as AttachmentPath,                            
isnull(EmailHstrAttchDtls.vcAttachmentType,'''') as AttachmentType,                      
isnull(EmailHstrAttchDtls.vcAttachmentItemId,'''') as AttachmentItemId,                             
dbo.FormatedDateFromDate(dtReceivedOn,numDomainId) as dtReceivedOn,    
dbo.GetEmaillName(emailHistory.numEmailHstrID,1) as ToName,                          
isnull(emailHistory.tintType,1) as type,                              
isnull(chrSource,''B'') as chrSource,                          
isnull(vcCategory,''white'') as vcCategory                   
 from tblSubscriber T                  
 join emailHistory                   
 on emailHistory.numEmailHstrID=T.numEmailHstrID             
left join EmailHstrAttchDtls on emailHistory.numEmailHstrID=EmailHstrAttchDtls.numEmailHstrID             
where numDomainId='+convert(varchar(15),@numDomainId)+' and numUserCntId='+convert(varchar(15),@numuserCntId)+  
' and numNodeId='+convert(varchar(15),@numNodeId)+' and                
  RowNumber > '+convert(varchar(10),@firstRec)+' and RowNumber < '+convert(varchar(10),@lastRec)+'              
union                 
 select 0 as RowNumber,null,count(*),null,null,            
null,null,null,null,null,null,null,null,null,NULL,           
null,1,null,null from tblSubscriber  order by RowNumber'            
--print    @strSql             
exec (@strSql)
GO
*/               

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_InLineEditDataSave')
DROP PROCEDURE USP_InLineEditDataSave
GO
CREATE PROCEDURE [dbo].[USP_InLineEditDataSave] 
( 
 @numDomainID NUMERIC(9),
 @numUserCntID NUMERIC(9),
 @numFormFieldId NUMERIC(9),
 @bitCustom AS BIT = 0,
 @InlineEditValue VARCHAR(8000),
 @numContactID NUMERIC(9),
 @numDivisionID NUMERIC(9),
 @numWOId NUMERIC(9),
 @numProId NUMERIC(9),
 @numOppId NUMERIC(9),
 @numRecId NUMERIC(9),
 @vcPageName NVARCHAR(100),
 @numCaseId NUMERIC(9),
 @numWODetailId NUMERIC(9),
 @numCommID NUMERIC(9)
 )
AS 

declare @strSql as nvarchar(4000)
DECLARE @vcDbColumnName AS VARCHAR(100),@vcOrigDbColumnName AS VARCHAR(100),@vcLookBackTableName AS varchar(100)
DECLARE @vcListItemType as VARCHAR(3),@numListID AS numeric(9),@vcAssociatedControlType varchar(20)
declare @tempAssignedTo as numeric(9);set @tempAssignedTo=null           
DECLARE @Value1 AS VARCHAR(18),@Value2 AS VARCHAR(18)

IF  @bitCustom =0
BEGIN
	SELECT @vcDbColumnName=vcDbColumnName,@vcLookBackTableName=vcLookBackTableName,
		   @vcListItemType=vcListItemType,@numListID=numListID,@vcAssociatedControlType=vcAssociatedControlType,
		   @vcOrigDbColumnName=vcOrigDbColumnName
	 FROM DycFieldMaster WHERE numFieldId=@numFormFieldId
	 
	IF @vcAssociatedControlType = 'DateField' AND  @InlineEditValue=''
	BEGIN
		SET @InlineEditValue = null
	END

	DECLARE @intExecuteDiv INT=0
	if @vcLookBackTableName = 'DivisionMaster' 
	 BEGIN 
		
	   IF @vcDbColumnName='numFollowUpStatus' --Add to FollowUpHistory
	    Begin
			declare @numFollow as varchar(10),@binAdded as varchar(20),@PreFollow as varchar(10),@RowCount as varchar(2)                            
			                      
			set @PreFollow=(select count(*)  numFollowUpStatus from FollowUpHistory where numDivisionID= @numDivisionID and numDomainId= @numDomainID)                          
			set @RowCount=@@rowcount  
			                        
			select   @numFollow=numFollowUpStatus, @binAdded=bintModifiedDate from divisionmaster where numDivisionID=@numDivisionID and numDomainId= @numDomainID                    
			
			if @numFollow <>'0' and @numFollow <> @InlineEditValue                          
			begin                          
				if @PreFollow<>0                          
					begin                          
			                   
						if @numFollow <> (select top 1  numFollowUpStatus from FollowUpHistory where numDivisionID= @numDivisionID and numDomainId= @numDomainID order by numFollowUpStatusID desc)                          
							begin                          
			                      	insert into FollowUpHistory(numFollowUpstatus,numDivisionID,bintAddedDate,numDomainId)                          
									values(@numFollow,@numDivisionID,@binAdded,@numDomainID)                          
							end                          
					end                          
				else                          
					begin                          
						insert into FollowUpHistory(numFollowUpstatus,numDivisionID,bintAddedDate,numDomainId)                          
						 values(@numFollow,@numDivisionID,@binAdded,@numDomainID)                          
					end                          
			   end 
			END
		 IF @vcDbColumnName='numAssignedTo' ---Updating if organization is assigned to someone
			BEGIN
				select @tempAssignedTo=isnull(numAssignedTo,0) from DivisionMaster where numDivisionID = @numDivisionID and numDomainId= @numDomainID           
			
				if (@tempAssignedTo<>@InlineEditValue and  @InlineEditValue<>'0')          
					begin            
						update DivisionMaster set numAssignedTo=@InlineEditValue ,numAssignedBy=@numUserCntID where numDivisionID = @numDivisionID  and numDomainId= @numDomainID  
						UPDATE CompanyInfo SET bintModifiedDate=GETUTCDATE() WHERE numCompanyId IN (SELECT numCompanyId 
																									FROM 
																										DivisionMaster
																									WHERE 
																										numDivisionID = @numDivisionID  and numDomainId= @numDomainID)         
					end					
				else if  (@InlineEditValue ='0')          
					begin          
						update DivisionMaster set numAssignedTo=0 ,numAssignedBy=0 where numDivisionID = @numDivisionID  and numDomainId= @numDomainID
						UPDATE CompanyInfo SET bintModifiedDate=GETUTCDATE() WHERE numCompanyId IN (SELECT numCompanyId 
																									FROM 
																										DivisionMaster
																									WHERE 
																										numDivisionID = @numDivisionID  and numDomainId= @numDomainID)          
					end    
			END
	    UPDATE CompanyInfo SET numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() WHERE numCompanyId=(SELECT TOP 1 numCompanyId FROM DivisionMaster where numDivisionID=@numDivisionID and numDomainID=@numDomainID) and numDomainID=@numDomainID
		
		IF @vcDbColumnName='vcPartnerCode'
		BEGIN
			IF(@InlineEditValue<>'')
			BEGIN
				IF EXISTS(SELECT vcPartnerCode FROM DivisionMaster where numDivisionID<>@numDivisionID and numDomainID=@numDomainID AND vcPartnerCode=@InlineEditValue)
				BEGIN
					SET @intExecuteDiv=1
					SET @InlineEditValue='-#12'
				END
			END
		END
		IF @intExecuteDiv=0
		BEGIN
			SET @strSql='update DivisionMaster set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numDivisionID=@numDivisionID and numDomainID=@numDomainID'
			
		END
	 END 
	 ELSE if @vcLookBackTableName = 'AdditionalContactsInformation' 
	 BEGIN 
			IF @vcDbColumnName='bitPrimaryContact' AND @InlineEditValue='True'
			BEGIN
				DECLARE @bitPrimaryContactCheck AS BIT             
				DECLARE @numID AS NUMERIC(9) 
				
				SELECT TOP 1 @numID = numContactID,@bitPrimaryContactCheck = ISNULL(bitPrimaryContact,0)
					FROM AdditionalContactsInformation WHERE numDivisionID = @numDivisionID 
                   
					IF @@rowcount = 0 SET @numID = 0             
				
					WHILE @numID > 0            
					BEGIN            
						IF @bitPrimaryContactCheck = 1 
							UPDATE  AdditionalContactsInformation SET bitPrimaryContact = 0 WHERE numContactID = @numID  
                              
						SELECT TOP 1 @numID = numContactID,@bitPrimaryContactCheck = ISNULL(bitPrimaryContact,0)
						FROM AdditionalContactsInformation WHERE numDivisionID = @numDivisionID AND numContactID > @numID    
                                
						IF @@rowcount = 0 SET @numID = 0             
					END 
				END
				ELSE IF @vcDbColumnName='bitPrimaryContact' AND @InlineEditValue='False'
				BEGIN
					IF (SELECT COUNT(*) FROM AdditionalContactsInformation WHERE numDivisionID = @numDivisionID AND numContactId!=@numContactId AND ISNULL(bitPrimaryContact,0)=1) = 0
						SET @InlineEditValue='True'
				END 
				-- Added by Priya(to check no Campaign got engaged wothout contacts email
				ELSE IF @vcDbColumnName = 'numECampaignID' 
				BEGIN
					
					IF EXISTS (select 1 from AdditionalContactsInformation WHERE numContactId = @numContactID AND (vcEmail IS NULL OR vcEmail = ''))
					BEGIN
						RAISERROR('EMAIL_ADDRESS_REQUIRED',16,1)
								RETURN
					END

				END
				
		SET @strSql='update AdditionalContactsInformation set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numDivisionID=@numDivisionID and numDomainID=@numDomainID and numContactId=@numContactId'
	 END 
	 ELSE if @vcLookBackTableName = 'CompanyInfo' 
	 BEGIN 
		SET @strSql='update CompanyInfo set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numCompanyId=(select numCompanyId from divisionmaster where numDivisionID=@numDivisionID and numDomainID=@numDomainID) and numDomainID=@numDomainID'
	 END 
	 ELSE if @vcLookBackTableName = 'WorkOrder' 
	 BEGIN 
		DECLARE @numItemCode AS NUMERIC(9)
	 
		IF @numWODetailId>0
		BEGIN
			SET @strSql='update WorkOrderDetails set ' + @vcDbColumnName + '=@InlineEditValue where numWOId=@numWOId and numWODetailId=@numWODetailId'
			
			IF @vcDbColumnName='numQtyItemsReq'
			BEGIN 
				SELECT @numItemCode=numChildItemID FROM WorkOrderDetails WHERE numWOId=@numWOId and numWODetailId=@numWODetailId
				
				EXEC USP_UpdateWorkOrderDetailQuantity @numItemCode,@InlineEditValue,@numWOId
				--UPDATE WorkOrderDetails SET numQtyItemsReq=CONVERT(NUMERIC(9),numQtyItemsReq_Orig * CONVERT(NUMERIC(9,0),@InlineEditValue)) WHERE numWOId=@numWOId
			END 
		END 	
		ELSE
		BEGIN
			SET @strSql='update WorkOrder set ' + @vcDbColumnName + '=@InlineEditValue where numWOId=@numWOId and numDomainID=@numDomainID'
			
			IF @vcDbColumnName='numQtyItemsReq'
			BEGIN 
				SELECT @numItemCode=numItemCode FROM WorkOrder WHERE numWOId=@numWOId and numDomainID=@numDomainID
			
				EXEC USP_UpdateWorkOrderDetailQuantity @numItemCode,@InlineEditValue,@numWOId
				--UPDATE WorkOrderDetails SET numQtyItemsReq=CONVERT(NUMERIC(9),numQtyItemsReq_Orig * CONVERT(NUMERIC(9,0),@InlineEditValue)) WHERE numWOId=@numWOId
			END 
		END	
	 END
	 ELSE if @vcLookBackTableName = 'ProjectsMaster' 
	 BEGIN 
		IF @vcDbColumnName='numAssignedTo' ---Updating if Project is assigned to someone 
			BEGIN
				select @tempAssignedTo=isnull(numAssignedTo,0) from ProjectsMaster where numProId=@numProId and numDomainId= @numDomainID           
			
				if (@tempAssignedTo<>@InlineEditValue and  @InlineEditValue<>'0')          
					begin            
						update ProjectsMaster set numAssignedTo=@InlineEditValue ,numAssignedBy=@numUserCntID,bintModifiedDate=GETUTCDATE() where numProId = @numProId  and numDomainId= @numDomainID           
					end           
				else if  (@InlineEditValue ='0')          
					begin          
						update ProjectsMaster set numAssignedTo=0 ,numAssignedBy=0,bintModifiedDate=GETUTCDATE() where numProId = @numProId  and numDomainId= @numDomainID        
					end    
			END
		else IF @vcDbColumnName='numProjectStatus'  ---Updating All Statge to 100% if Completed
			BEGIN
				IF @InlineEditValue='27492'
				BEGIN
					UPDATE PM SET dtCompletionDate=GETUTCDATE(),monTotalExpense=PIE.monExpense,
					monTotalIncome=PIE.monIncome,monTotalGrossProfit=PIE.monBalance,numProjectStatus=27492
					FROM ProjectsMaster PM,dbo.GetProjectIncomeExpense(@numDomainId,@numProId) PIE
					WHERE PM.numProId=@numProId
					
					EXEC USP_ManageProjectCommission @numDomainId,@numProId
						
					Update StagePercentageDetails set tinProgressPercentage=100,bitclose=1 where numProjectID=@numProId

					EXEC dbo.USP_GetProjectTotalProgress
							@numDomainID = @numDomainID, --  numeric(9, 0)
							@numProId = @numProId, --  numeric(9, 0)
							@tintMode = 1 --  tinyint
				END			
				ELSE
				BEGIN
					DELETE from BizDocComission WHERE numDomainID = @numDomainID AND numProId=@numProId AND numComissionID 
						NOT IN (SELECT BC.numComissionID FROM dbo.BizDocComission BC JOIN dbo.PayrollTracking PT 
							ON BC.numComissionID = PT.numComissionID AND BC.numDomainId = PT.numDomainId WHERE BC.numDomainID = @numDomainID AND ISNULL(BC.numProId,0) = @numProId)
				END
			END
			

		SET @strSql='update ProjectsMaster set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numProId=@numProId and numDomainID=@numDomainID'
	 END 
	 ELSE if @vcLookBackTableName = 'OpportunityMaster' 
	 BEGIN 
		IF @vcDbColumnName='numAssignedTo' ---Updating if organization is assigned to someone  
			BEGIN
				select @tempAssignedTo=isnull(numAssignedTo,0) from OpportunityMaster where numOppId=@numOppId and numDomainId= @numDomainID           
			
				if (@tempAssignedTo<>@InlineEditValue and  @InlineEditValue<>'0')          
					begin            
						update OpportunityMaster set numAssignedTo=@InlineEditValue ,numAssignedBy=@numUserCntID,bintModifiedDate=GETUTCDATE() where numOppId = @numOppId  and numDomainId= @numDomainID           
					end           
				else if  (@InlineEditValue ='0')          
					begin          
						update OpportunityMaster set numAssignedTo=0 ,numAssignedBy=0,bintModifiedDate=GETUTCDATE() where numOppId = @numOppId  and numDomainId= @numDomainID        
					end    
			END
		ELSE IF @vcDbColumnName='numPartner' OR @vcDbColumnName='numPartenerSource'
		 BEGIN
			update OpportunityMaster set numPartner=@InlineEditValue ,bintModifiedDate=GETUTCDATE() where numOppId = @numOppId  and numDomainId= @numDomainID           
		 END
		
		ELSE IF @vcDbColumnName='tintActive'
		 BEGIN
		 	SET @InlineEditValue= CASE @InlineEditValue WHEN 'True' THEN 1 ELSE 0 END 
		 END
		 ELSE IF @vcDbColumnName='tintSource'
		 BEGIN
			IF CHARINDEX('~', @InlineEditValue)>0
			BEGIN
				SET @Value1=SUBSTRING(@InlineEditValue, 1,CHARINDEX('~', @InlineEditValue)-1) 
				SET @Value2=SUBSTRING(@InlineEditValue, CHARINDEX('~', @InlineEditValue)+1,LEN(@InlineEditValue)) 
			END	
			ELSE
			BEGIN
				SET @Value1=@InlineEditValue
				SET @Value2=0
			END
			
		 	update OpportunityMaster set tintSourceType=@Value2 where numOppId = @numOppId  and numDomainId= @numDomainID        
		 	
		 	SET @InlineEditValue= @Value1
		 END
		ELSE IF @vcDbColumnName='tintOppStatus' --Update Inventory  0=Open,1=Won,2=Lost
		 BEGIN
			declare @bitApprovalforOpportunity bit
			declare @intOpportunityApprovalProcess int
			SELECT 
				@bitApprovalforOpportunity=bitApprovalforOpportunity
				,@intOpportunityApprovalProcess=intOpportunityApprovalProcess 
			FROM 
				Domain 
			WHERE 
				numDomainId=@numDomainID
			
			DECLARE @bitViolatePrice BIT
			DECLARE @intPendingApprovePending INT
		
			IF EXISTS (SELECT numApprovalRuleID FROM ApprovalProcessItemsClassification WHERE numDomainId=@numDomainID AND ISNULL(bitMarginPriceViolated,0) = 1)
			BEGIN
				SET @bitViolatePrice = 1
			END
			SET @intPendingApprovePending=(SELECT COUNT(*) FROM OpportunityItems WHERE numOppId=@numOppId AND ISNULL(bitItemPriceApprovalRequired,0)=1)

			IF(@intPendingApprovePending > 0 AND @bitViolatePrice=1 AND @InlineEditValue='1')
			BEGIN
				SET @intExecuteDiv=1
				SET @InlineEditValue='-#123'
			END

			IF ISNULL(@intExecuteDiv,0)=0
			BEGIN
				IF(@bitApprovalforOpportunity=1)
				BEGIN
					IF(@InlineEditValue='1' OR @InlineEditValue='2')
					BEGIN
						IF(@intOpportunityApprovalProcess=1)
						BEGIN
							UPDATE OpportunityMaster SET intPromotionApprovalStatus=6,intChangePromotionStatus=@InlineEditValue
							WHERE numOppId=@numOppId
						END
						IF(@intOpportunityApprovalProcess=2)
						BEGIN
							UPDATE OpportunityMaster SET intPromotionApprovalStatus=1,intChangePromotionStatus=@InlineEditValue
							WHERE numOppId=@numOppId
						END
					END
				END
				
				DECLARE @tintOppStatus AS TINYINT
				SELECT @tintOppStatus=tintOppStatus FROM OpportunityMaster WHERE numOppID=@numOppID              

				IF @tintOppStatus=0 AND @InlineEditValue='1' --Open to Won
				BEGIN
					SELECT @numDivisionID=numDivisionID FROM OpportunityMaster WHERE numOppID=@numOppID   
					
					DECLARE @tintCRMType AS TINYINT      
					SELECT @tintCRMType=tintCRMType FROM divisionmaster WHERE numDivisionID =@numDivisionID 

					-- Promote Lead to Account when Sales/Purchase Order is created against it
					IF @tintCRMType=0 --Lead & Order
					BEGIN        
						UPDATE 
							divisionmaster 
						SET 
							tintCRMType=2
							,bintLeadProm=GETUTCDATE()
							,bintLeadPromBy=@numUserCntID
							,bintProsProm=GETUTCDATE()
							,bintProsPromBy=@numUserCntID        
						WHERE 
							numDivisionID=@numDivisionID        
					END
					--Promote Prospect to Account
					ELSE IF @tintCRMType=1 
					BEGIN        
						UPDATE 
							divisionmaster 
						SET 
							tintCRMType=2
							,bintProsProm=GETUTCDATE()
							,bintProsPromBy=@numUserCntID        
						WHERE 
							numDivisionID=@numDivisionID        
					END    

		 			EXEC USP_UpdatingInventoryonCloseDeal @numOppID,@numUserCntID

					UPDATE OpportunityMaster SET bintOppToOrder=GETUTCDATE() WHERE numOppId=@numOppId and numDomainID=@numDomainID
				END
				
				IF (@tintOppStatus=1 and @InlineEditValue='2') or (@tintOppStatus=1 and @InlineEditValue='0') --Won to Lost or Won to Open
						EXEC USP_RevertDetailsOpp @numOppID,0,@numUserCntID 

				UPDATE OpportunityMaster SET tintOppStatus = @InlineEditValue WHERE numOppId=@numOppID
				
			END
		 END
		 ELSE IF @vcDbColumnName='vcOppRefOrderNo' --Update vcRefOrderNo OpportunityBizDocs
		 BEGIN
			 IF LEN(ISNULL(@InlineEditValue,''))>0
			 BEGIN
	   			UPDATE dbo.OpportunityBizDocs SET vcRefOrderNo=@InlineEditValue WHERE numOppId=@numOppID
			 END
		 END
		 ELSE IF @vcDbColumnName='numStatus' 
		 BEGIN
			DECLARE @tintOppType AS TINYINT
			select @tintOppStatus=tintOppStatus,@tintOppType=tintOppType from OpportunityMaster where numOppID=@numOppID

			IF @tintOppType=1 AND @tintOppStatus=1
			BEGIN
				DECLARE @numOppStatus AS NUMERIC(18,0) = CAST(@InlineEditValue AS NUMERIC(18,0))
				EXEC USP_SalesFulfillmentQueue_Save @numDomainID,@numUserCntID,@numOppID,@numOppStatus
			END

		 	 IF EXISTS (SELECT 1 FROM dbo.OpportunityMaster WHERE numOppId = @numOppID AND tintOppType=1 AND isnull(tintOppStatus,0)=1 AND ISNULL(numStatus,0)!= @InlineEditValue)
			 BEGIN
	   				EXEC USP_ManageOpportunityAutomationQueue
							@numOppQueueID = 0, -- numeric(18, 0)
							@numDomainID = @numDomainID, -- numeric(18, 0)
							@numOppId = @numOppID, -- numeric(18, 0)
							@numOppBizDocsId = 0, -- numeric(18, 0)
							@numOrderStatus = @InlineEditValue, -- numeric(18, 0)
							@numUserCntID = @numUserCntID, -- numeric(18, 0)
							@tintProcessStatus = 1, -- tinyint
							@tintMode = 1 -- TINYINT
			END 
		 END	
		 ELSE IF @vcDbColumnName = 'dtReleaseDate'
		 BEGIN
			IF EXISTS (SELECT 1 FROM OpportunityMaster WHERE numDomainId=@numDomainID AND numOppId=@numOppId AND tintOppType=1 AND tintOppStatus=0)
			BEGIN
				IF (SELECT dbo.GetListIemName(ISNULL(numPercentageComplete,0)) FROM OpportunityMaster WHERE numDomainId=@numDomainID AND numOppId=@numOppId) = '100'
				BEGIN
					SET @strSql='update OpportunityMaster set ' + @vcDbColumnName + '=DATEADD(hh, DATEDIFF(hh, GETDATE(), GETUTCDATE()), @InlineEditValue),numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numOppId=@numOppId and numDomainID=@numDomainID'
				END
				ELSE
				BEGIN
					RAISERROR('PERCENT_COMPLETE_MUST_BE_100',16,1)
					RETURN
				END
			END
			ELSE
			BEGIN
				SET @strSql='update OpportunityMaster set ' + @vcDbColumnName + '=DATEADD(hh, DATEDIFF(hh, GETDATE(), GETUTCDATE()), @InlineEditValue),numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numOppId=@numOppId and numDomainID=@numDomainID'
			END
		END
		ELSE IF @vcDbColumnName = 'numPercentageComplete'
		BEGIN
			IF EXISTS (SELECT 1 FROM OpportunityMaster WHERE numDomainId=@numDomainID AND numOppId=@numOppId AND tintOppType=1 AND tintOppStatus=0 AND dbo.GetListIemName(ISNULL(numPercentageComplete,0))='100')
			BEGIN
				IF EXISTS(SELECT dtReleaseDate FROM OpportunityMaster WHERE numDomainId=@numDomainID AND numOppId=@numOppId AND dtReleaseDate IS NULL)
				BEGIN
					SET @strSql='update OpportunityMaster set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numOppId=@numOppId and numDomainID=@numDomainID'
				END
				ELSE
				BEGIN
					RAISERROR('REMOVE_RELEASE_DATE',16,1)
					RETURN
				END
			END
			ELSE
			BEGIN
				SET @strSql='update OpportunityMaster set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numOppId=@numOppId and numDomainID=@numDomainID'
			END
		END

		IF(@vcDbColumnName!='tintOppStatus')
		BEGIN
			SET @strSql='update OpportunityMaster set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numOppId=@numOppId and numDomainID=@numDomainID'
		END
	 END
	 ELSE if @vcLookBackTableName = 'Cases' 
	 BEGIN 
	 		IF @vcDbColumnName='numAssignedTo' ---Updating if Case is assigned to someone  
			BEGIN
				select @tempAssignedTo=isnull(numAssignedTo,0) from Cases where numCaseId=@numCaseId and numDomainId= @numDomainID           
			
				if (@tempAssignedTo<>@InlineEditValue and  @InlineEditValue<>'0')          
					begin            
						update Cases set numAssignedTo=@InlineEditValue ,numAssignedBy=@numUserCntID,bintModifiedDate=GETUTCDATE() where numCaseId = @numCaseId  and numDomainId= @numDomainID           
					end           
				else if  (@InlineEditValue ='0')          
					begin          
						update Cases set numAssignedTo=0 ,numAssignedBy=0,bintModifiedDate=GETUTCDATE() where numCaseId = @numCaseId  and numDomainId= @numDomainID        
					end    
			END
		UPDATE Cases SET bintModifiedDate=GETUTCDATE() WHERE numCaseId = @numCaseId  and numDomainId= @numDomainID    
		SET @strSql='update Cases set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numCaseId=@numCaseId and numDomainID=@numDomainID'
	 END 
	 ELSE if @vcLookBackTableName = 'Tickler' 
	 BEGIN 	
		SET @strSql='update Communication set ' + @vcOrigDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,dtModifiedDate=getutcdate() where numCommId=@numCommId and numDomainID=@numDomainID'
	 END
	 ELSE IF @vcLookBackTableName = 'ExtarnetAccounts'
	 BEGIN
		IF @vcDbColumnName='bitEcommerceAccess'
		BEGIN
			IF LOWER(@InlineEditValue) = 'yes' OR LOWER(@InlineEditValue) = 'true'
			BEGIN
				IF NOT EXISTS (SELECT * FROM ExtarnetAccounts where numDivisionID=@numDivisionID) 
				BEGIN
					DECLARE @numGroupID NUMERIC
					DECLARE @numCompanyID NUMERIC(18,0)
					SELECT @numCompanyID=numCompanyID FROM DivisionMaster WHERE numDivisionID=@numDivisionID

					SELECT TOP 1 @numGroupID=numGroupID FROM dbo.AuthenticationGroupMaster WHERE numDomainID=@numDomainID AND tintGroupType=2
				
					IF @numGroupID IS NULL SET @numGroupID = 0 
					INSERT INTO ExtarnetAccounts (numCompanyID,numDivisionID,numGroupID,numDomainID)                                                
					VALUES(@numCompanyID,@numDivisionID,@numGroupID,@numDomainID)
				END
			END
			ELSE
			BEGIN
				DELETE FROM ExtranetAccountsDtl WHERE numExtranetID IN (SELECT ISNULL(numExtranetID,0) FROM ExtarnetAccounts where numDivisionID=@numDivisionID)
				DELETE FROM ExtarnetAccounts where numDivisionID=@numDivisionID
			END

			SELECT (CASE WHEN LOWER(@InlineEditValue) = 'yes' OR LOWER(@InlineEditValue) = 'true' THEN 'Yes' ELSE 'No' END) AS vcData

			RETURN
		END
	 END
	ELSE IF @vcLookBackTableName = 'ExtranetAccountsDtl'
	BEGIN
		IF @vcDbColumnName='vcPassword'
		BEGIN
			IF LEN(@InlineEditValue) = 0
			BEGIN
				DELETE FROM ExtranetAccountsDtl WHERE numContactID=@numContactID
			END
			ELSE
			BEGIN
				IF EXISTS (SELECT numExtranetID FROM ExtarnetAccounts WHERE numDivisionID = @numDivisionID)
				BEGIN
					IF EXISTS (SELECT numExtranetDtlID FROM ExtranetAccountsDtl WHERE numContactID=@numContactID)
					BEGIN
						UPDATE
							ExtranetAccountsDtl
						SET 
							vcPassword=@InlineEditValue
							,tintAccessAllowed=1
						WHERE
							numContactID=@numContactID
					END
					ELSE
					BEGIN
						DECLARE @numExtranetID NUMERIC(18,0)
						SELECT @numExtranetID=numExtranetID FROM ExtarnetAccounts WHERE numDivisionID = @numDivisionID

						INSERT INTO ExtranetAccountsDtl (numExtranetID,numContactID,vcPassword,tintAccessAllowed,numDomainID)
						VALUES (@numExtranetID,@numContactID,@InlineEditValue,1,@numDomainID)
					END
				END
				ELSE
				BEGIN
					RAISERROR('Organization e-commerce access is not enabled',16,1)
				END
			END

			SELECT @InlineEditValue AS vcData

			RETURN
		END
	END

	--ELSE IF @vcLookBackTableName = 'Item'
	--BEGIN
	--	IF @vcDbColumnName = 'monListPrice'
	--	BEGIN
	--		IF EXISTS (SELECT numItemCode FROM Item WHERE numDomainID=@numDomainID AND numItemCode=@numRecId AND 1 = (CASE WHEN ISNULL(numItemGroup,0) > 0 THEN (CASE WHEN ISNULL(bitMatrix,0)=1 THEN 1 ELSE 0 END) ELSE 1 END))
	--		BEGIN
	--			UPDATE WareHouseItems SET monWListPrice=@InlineEditValue WHERE numDomainID=@numDomainID AND numItemID=@numRecId
	--			UPDATE Item SET monListPrice=@InlineEditValue WHERE numDomainID=@numDomainID AND numItemCode=@numRecId

	--			SELECT @InlineEditValue AS vcData
	--		END
	--		ELSE
	--		BEGIN
	--			RAISERROR('You are not allowed to edit list price for warehouse level matrix item.',16,1)
	--		END

	--		RETURN
	--	END

	--	SET @strSql='UPDATE Item set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numItemCode=@numRecId AND numDomainID=@numDomainID'
	--END

	EXECUTE sp_executeSQL @strSql, N'@numDomainID NUMERIC(9),@numUserCntID NUMERIC(9),@numDivisionID NUMERIC(9),@numContactID NUMERIC(9),@InlineEditValue VARCHAR(8000),@numWOId NUMERIC(9),@numProId NUMERIC(9),@numOppId NUMERIC(9),@numCaseId NUMERIC(9),@numWODetailId NUMERIC(9),@numCommID Numeric(9),@numRecId NUMERIC(18,0)',
		     @numDomainID,@numUserCntID,@numDivisionID,@numContactID,@InlineEditValue,@numWOId,@numProId,@numOppId,@numCaseId,@numWODetailId,@numCommID,@numRecId;
		     
	IF @vcAssociatedControlType='SelectBox'                                                    
       BEGIN       
			IF @vcDbColumnName='tintSource' AND @vcLookBackTableName = 'OpportunityMaster'
			BEGIN
				SELECT dbo.fn_GetOpportunitySourceValue(@Value1,@Value2,@numDomainID) AS vcData                                             
			END
			ELSE IF @vcDbColumnName='numContactID'
			BEGIN
				SELECT dbo.fn_GetContactName(@InlineEditValue)
			END
			ELSE IF @vcDbColumnName='numPartner' OR @vcDbColumnName='numPartenerSource'
			BEGIN
				SET @intExecuteDiv=1
				SELECT D.vcPartnerCode+'-'+C.vcCompanyName  AS vcData FROM DivisionMaster AS D 
				LEFT JOIN CompanyInfo AS C
				ON D.numCompanyID=C.numCompanyID WHERE D.numDivisionID=@InlineEditValue
			END
			
			ELSE IF @vcDbColumnName='tintOppStatus' AND @intExecuteDiv=1
			BEGIN
				SELECT @InlineEditValue AS vcData
			END
			ELSE IF @vcDbColumnName='numReleaseStatus'
			BEGIN
				If @InlineEditValue = '1'
				BEGIN
					SELECT 'Open'
				END
				ELSE IF @InlineEditValue = '2'
				BEGIN
					SELECT 'Purchased'
				END
				ELSE
				BEGIN
					SELECT ''
				END
			END
			ELSE IF @vcDbColumnName='numPartenerContact'
			BEGIN
				SET @InlineEditValue=(SELECT TOP 1 A.vcGivenName FROM AdditionalContactsInformation AS A 
										LEFT JOIN DivisionMaster AS D 
										ON D.numDivisionID=A.numDivisionId
										WHERE D.numDomainID=@numDomainID AND A.numCOntactId=@InlineEditValue )
				SELECT @InlineEditValue AS vcData
			END
			ELSE IF @vcDbColumnName='numPartenerSource'
			BEGIN
				
				SET @InlineEditValue=(SELECT D.vcPartnerCode+'-'+C.vcCompanyName  AS vcData FROM DivisionMaster AS D 
				LEFT JOIN CompanyInfo AS C
				ON D.numCompanyID=C.numCompanyID WHERE D.numDivisionID=@InlineEditValue)
				PRINT 'MU'
				SELECT @InlineEditValue AS vcData
			END
			else
			BEGIN
				SELECT dbo.fn_getSelectBoxValue(@vcListItemType,@InlineEditValue,@vcDbColumnName) AS vcData                                             
			END
		end 
		ELSE
		BEGIN
			 IF @vcDbColumnName='tintActive'
		  	 BEGIN
			 	SET @InlineEditValue= CASE @InlineEditValue WHEN 1 THEN 'True' ELSE 'False' END 
			 END

			IF @vcAssociatedControlType = 'Check box' or @vcAssociatedControlType = 'Checkbox'
			BEGIN
	 			SET @InlineEditValue= Case @InlineEditValue WHEN 'True' THEN 'Yes' ELSE 'No' END
			END
			
		    SELECT @InlineEditValue AS vcData
		END  
		
	IF @vcLookBackTableName = 'AdditionalContactsInformation' AND @vcDbColumnName = 'numECampaignID'
	BEGIN
		--Manage ECampiagn When Campaign is changed from inline edit
		DECLARE @Date AS DATETIME 
		SELECT @Date = dbo.[GetUTCDateWithoutTime]()
		DECLARE @numECampaignID AS NUMERIC(18,0)
		SET @numECampaignID = @InlineEditValue

		IF @numECampaignID = -1 
		SET @numECampaignID=0
	 
 		EXEC [USP_ManageConEmailCampaign]
		@numConEmailCampID = 0, --  numeric(9, 0)
		@numContactID = @numContactID, --  numeric(9, 0)
		@numECampaignID = @numECampaignID, --  numeric(9, 0)
		@intStartDate = @Date, --  datetime
		@bitEngaged = 1, --  bit
		@numUserCntID = @numUserCntID --  numeric(9, 0)
	END  
END
ELSE
BEGIN	 

	SET @InlineEditValue=Cast(@InlineEditValue as varchar(1000))
	SELECT @vcAssociatedControlType=Fld_type
	 FROM CFW_Fld_Master WHERE Fld_Id=@numFormFieldId 

	IF @vcAssociatedControlType = 'CheckBox'
	BEGIN
	    SET @InlineEditValue=(case when @InlineEditValue='False' then 0 ELSE 1 END)
	END

	IF @vcPageName='organization'
	BEGIN 
		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
			BEGIN
				UPDATE CFW_FLD_Values SET Fld_Value=@InlineEditValue WHERE FldDTLID=(SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
			END	
		ELSE
			BEGIN
				insert into CFW_FLD_Values (Fld_ID,Fld_Value,RecId) VALUES (@numFormFieldId,@InlineEditValue,@numRecId)         
			END

			--Added By :Sachin Sadhu ||Date:13thOct2014
			--Purpose  :To manage CustomFields in Queue
			EXEC USP_CFW_FLD_Values_CT
					@numDomainID =@numDomainID,
					@numUserCntID =@numUserCntID,
					@numRecordID =@numRecId ,
					@numFormFieldId  =@numFormFieldId
  
	  END
	ELSE IF @vcPageName='contact'
	BEGIN 
		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Cont CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
			BEGIN
				UPDATE CFW_FLD_Values_Cont SET Fld_Value=@InlineEditValue WHERE FldDTLID=(SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Cont CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
			END	
		ELSE
			BEGIN
				insert into CFW_FLD_Values_Cont (Fld_ID,Fld_Value,RecId) VALUES (@numFormFieldId,@InlineEditValue,@numRecId)         
			END
			
			--Added By :Sachin Sadhu ||Date:13thOct2014
			--Purpose  :To manage CustomFields in Queue
			EXEC USP_CFW_FLD_Values_Cont_CT
					@numDomainID =@numDomainID,
					@numUserCntID =@numUserCntID,
					@numRecordID =@numRecId ,
					@numFormFieldId  =@numFormFieldId

	END
	ELSE IF @vcPageName='project'
	BEGIN 
		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Pro CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
			BEGIN
				UPDATE CFW_FLD_Values_Pro SET Fld_Value=@InlineEditValue WHERE FldDTLID=(SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Pro CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
			END	
		ELSE
			BEGIN
				insert into CFW_FLD_Values_Pro (Fld_ID,Fld_Value,RecId) VALUES (@numFormFieldId,@InlineEditValue,@numRecId)         
			END

			
			--Added By :Sachin Sadhu ||Date:14thOct2014
			--Purpose  :To manage CustomFields in Queue
			EXEC USP_CFW_FLD_Values_Pro_CT
					@numDomainID =@numDomainID,
					@numUserCntID =@numUserCntID,
					@numRecordID =@numRecId ,
					@numFormFieldId  =@numFormFieldId
	END
	ELSE IF @vcPageName='opp'
	BEGIN 
		DECLARE @dtDate AS DATETIME = GETUTCDATE()

		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_Fld_Values_Opp CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
		BEGIN
			UPDATE 
				CFW_Fld_Values_Opp 
			SET 
				Fld_Value=@InlineEditValue ,
				numModifiedBy = @numUserCntID,
				bintModifiedDate = @dtDate
			WHERE 
				FldDTLID= (SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_Fld_Values_Opp CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
		END	
		ELSE
		BEGIN
			INSERT INTO CFW_Fld_Values_Opp (Fld_ID,Fld_Value,RecId,numModifiedBy,bintModifiedDate) VALUES (@numFormFieldId,@InlineEditValue,@numRecId,@numUserCntID,@dtDate)         
		END

		UPDATE OpportunityMaster SET numModifiedBy=@numUserCntID,bintModifiedDate=@dtDate WHERE numOppID = @numRecId 

		--Added By :Sachin Sadhu ||Date:14thOct2014
		--Purpose  :To manage CustomFields in Queue
		EXEC USP_CFW_Fld_Values_Opp_CT
				@numDomainID =@numDomainID,
				@numUserCntID =@numUserCntID,
				@numRecordID =@numRecId ,
				@numFormFieldId  =@numFormFieldId

	END
	ELSE IF @vcPageName='case'
	BEGIN 
		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Case CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
			BEGIN
				UPDATE CFW_FLD_Values_Case SET Fld_Value=@InlineEditValue WHERE FldDTLID=(SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Case CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
			END	
		ELSE
			BEGIN
				insert into CFW_FLD_Values_Case (Fld_ID,Fld_Value,RecId) VALUES (@numFormFieldId,@InlineEditValue,@numRecId)         
			END

			
			--Added By :Sachin Sadhu ||Date:14thOct2014
			--Purpose  :To manage CustomFields in Queue
			EXEC USP_CFW_FLD_Values_Case_CT
					@numDomainID =@numDomainID,
					@numUserCntID =@numUserCntID,
					@numRecordID =@numRecId ,
					@numFormFieldId  =@numFormFieldId
	END
	ELSE IF @vcPageName='item'
	BEGIN
		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Item CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
		BEGIN
			UPDATE CFW_FLD_Values_Item SET Fld_Value=@InlineEditValue WHERE FldDTLID=(SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Item CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
		END	
		ELSE
		BEGIN
			INSERT INTO CFW_FLD_Values_Item (Fld_ID,Fld_Value,RecId) VALUES (@numFormFieldId,@InlineEditValue,@numRecId)         
		END
	END


	IF @vcAssociatedControlType = 'SelectBox'            
	BEGIN            
		    
		 SELECT vcData FROM ListDetails WHERE numListItemID=@InlineEditValue            
	END
	ELSE IF @vcAssociatedControlType = 'CheckBoxList'
	BEGIN
		DECLARE @str AS VARCHAR(MAX)
		SET @str = 'SELECT STUFF((SELECT CONCAT('','', vcData) FROM ListDetails WHERE numlistitemid IN (' + (CASE WHEN LEN(@InlineEditValue) > 0 THEN @InlineEditValue ELSE '0' END) + ') FOR XML PATH('''')), 1, 1, '''')'
		EXEC(@str)
	END
	ELSE IF @vcAssociatedControlType = 'CheckBox'
	BEGIN
	 	select CASE @InlineEditValue WHEN 1 THEN 'Yes' ELSE 'No' END AS vcData
	END
	ELSE
	BEGIN
		SELECT @InlineEditValue AS vcData
	END 
END

GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Anoop Jayaraj                                                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Item_ImportPriceLevel')
DROP PROCEDURE USP_Item_ImportPriceLevel
GO
CREATE PROCEDURE [dbo].[USP_Item_ImportPriceLevel]
(
	@numItemCode nUMERIC(18,0)
	,@tintRuleType INT
	,@tintDiscountType INT
	,@monPriceLevel1 DECIMAL(20,5)
	,@monPriceLevel2 DECIMAL(20,5)
	,@monPriceLevel3 DECIMAL(20,5)
	,@monPriceLevel4 DECIMAL(20,5)
	,@monPriceLevel5 DECIMAL(20,5)
	,@monPriceLevel6 DECIMAL(20,5)
	,@monPriceLevel7 DECIMAL(20,5)
	,@monPriceLevel8 DECIMAL(20,5)
	,@monPriceLevel9 DECIMAL(20,5)
	,@monPriceLevel10 DECIMAL(20,5)
	,@monPriceLevel11 DECIMAL(20,5)
	,@monPriceLevel12 DECIMAL(20,5)
	,@monPriceLevel13 DECIMAL(20,5)
	,@monPriceLevel14 DECIMAL(20,5)
	,@monPriceLevel15 DECIMAL(20,5)
	,@monPriceLevel16 DECIMAL(20,5)
	,@monPriceLevel17 DECIMAL(20,5)
	,@monPriceLevel18 DECIMAL(20,5)
	,@monPriceLevel19 DECIMAL(20,5)
	,@monPriceLevel20 DECIMAL(20,5)
	,@intPriceLevel1FromQty INT
	,@intPriceLevel1ToQty INT
	,@intPriceLevel2FromQty INT
	,@intPriceLevel2ToQty INT
	,@intPriceLevel3FromQty INT
	,@intPriceLevel3ToQty INT
	,@intPriceLevel4FromQty INT
	,@intPriceLevel4ToQty INT
	,@intPriceLevel5FromQty INT
	,@intPriceLevel5ToQty INT
	,@intPriceLevel6FromQty INT
	,@intPriceLevel6ToQty INT
	,@intPriceLevel7FromQty INT
	,@intPriceLevel7ToQty INT
	,@intPriceLevel8FromQty INT
	,@intPriceLevel8ToQty INT
	,@intPriceLevel9FromQty INT
	,@intPriceLevel9ToQty INT
	,@intPriceLevel10FromQty INT
	,@intPriceLevel10ToQty INT
	,@intPriceLevel11FromQty INT
	,@intPriceLevel11ToQty INT
	,@intPriceLevel12FromQty INT
	,@intPriceLevel12ToQty INT
	,@intPriceLevel13FromQty INT
	,@intPriceLevel13ToQty INT
	,@intPriceLevel14FromQty INT
	,@intPriceLevel14ToQty INT
	,@intPriceLevel15FromQty INT
	,@intPriceLevel15ToQty INT
	,@intPriceLevel16FromQty INT
	,@intPriceLevel16ToQty INT
	,@intPriceLevel17FromQty INT
	,@intPriceLevel17ToQty INT
	,@intPriceLevel18FromQty INT
	,@intPriceLevel18ToQty INT
	,@intPriceLevel19FromQty INT
	,@intPriceLevel19ToQty INT
	,@intPriceLevel20FromQty INT
	,@intPriceLevel20ToQty INT
	,@vcPriceLevel1Name VARCHAR(300)
	,@vcPriceLevel2Name VARCHAR(300)
	,@vcPriceLevel3Name VARCHAR(300)
	,@vcPriceLevel4Name VARCHAR(300)
	,@vcPriceLevel5Name VARCHAR(300)
	,@vcPriceLevel6Name VARCHAR(300)
	,@vcPriceLevel7Name VARCHAR(300)
	,@vcPriceLevel8Name VARCHAR(300)
	,@vcPriceLevel9Name VARCHAR(300)
	,@vcPriceLevel10Name VARCHAR(300)
	,@vcPriceLevel11Name VARCHAR(300)
	,@vcPriceLevel12Name VARCHAR(300)
	,@vcPriceLevel13Name VARCHAR(300)
	,@vcPriceLevel14Name VARCHAR(300)
	,@vcPriceLevel15Name VARCHAR(300)
	,@vcPriceLevel16Name VARCHAR(300)
	,@vcPriceLevel17Name VARCHAR(300)
	,@vcPriceLevel18Name VARCHAR(300)
	,@vcPriceLevel19Name VARCHAR(300)
	,@vcPriceLevel20Name VARCHAR(300)
) 
AS
BEGIN
	IF @monPriceLevel1 = -1
		AND @monPriceLevel2 = -1
		AND @monPriceLevel3 = -1
		AND @monPriceLevel4 = -1
		AND @monPriceLevel5 = -1
		AND @monPriceLevel6 = -1
		AND @monPriceLevel7 = -1
		AND @monPriceLevel8 = -1
		AND @monPriceLevel9 = -1
		AND @monPriceLevel10 = -1
		AND @monPriceLevel11 = -1
		AND @monPriceLevel12 = -1
		AND @monPriceLevel13 = -1
		AND @monPriceLevel14 = -1
		AND @monPriceLevel15 = -1
		AND @monPriceLevel16 = -1
		AND @monPriceLevel17 = -1
		AND @monPriceLevel18 = -1
		AND @monPriceLevel19 = -1
		AND @monPriceLevel20 = -1
	BEGIN
		RETURN
	END

	IF ISNULL(@intPriceLevel1FromQty,0) = 0 AND ISNULL(@intPriceLevel1ToQty,0) = 0
	BEGIN
		SET @intPriceLevel1FromQty = -1
		SET @intPriceLevel1ToQty = -1
	END
	IF ISNULL(@intPriceLevel2FromQty,0) = 0 AND ISNULL(@intPriceLevel2ToQty,0) = 0
	BEGIN
		SET @intPriceLevel2FromQty = -1
		SET @intPriceLevel2ToQty = -1
	END
	IF ISNULL(@intPriceLevel3FromQty,0) = 0 AND ISNULL(@intPriceLevel3ToQty,0) = 0
	BEGIN
		SET @intPriceLevel3FromQty = -1
		SET @intPriceLevel3ToQty = -1
	END
	IF ISNULL(@intPriceLevel4FromQty,0) = 0 AND ISNULL(@intPriceLevel4ToQty,0) = 0
	BEGIN
		SET @intPriceLevel4FromQty = -1
		SET @intPriceLevel4ToQty = -1
	END
	IF ISNULL(@intPriceLevel5FromQty,0) = 0 AND ISNULL(@intPriceLevel5ToQty,0) = 0
	BEGIN
		SET @intPriceLevel5FromQty = -1
		SET @intPriceLevel5ToQty = -1
	END
	IF ISNULL(@intPriceLevel6FromQty,0) = 0 AND ISNULL(@intPriceLevel6ToQty,0) = 0
	BEGIN
		SET @intPriceLevel6FromQty = -1
		SET @intPriceLevel6ToQty = -1
	END
	IF ISNULL(@intPriceLevel7FromQty,0) = 0 AND ISNULL(@intPriceLevel7ToQty,0) = 0
	BEGIN
		SET @intPriceLevel7FromQty = -1
		SET @intPriceLevel7ToQty = -1
	END
	IF ISNULL(@intPriceLevel8FromQty,0) = 0 AND ISNULL(@intPriceLevel8ToQty,0) = 0
	BEGIN
		SET @intPriceLevel8FromQty = -1
		SET @intPriceLevel8ToQty = -1
	END
	IF ISNULL(@intPriceLevel9FromQty,0) = 0 AND ISNULL(@intPriceLevel9ToQty,0) = 0
	BEGIN
		SET @intPriceLevel9FromQty = -1
		SET @intPriceLevel9ToQty = -1
	END
	IF ISNULL(@intPriceLevel10FromQty,0) = 0 AND ISNULL(@intPriceLevel10ToQty,0) = 0
	BEGIN
		SET @intPriceLevel10FromQty = -1
		SET @intPriceLevel10ToQty = -1
	END
	IF ISNULL(@intPriceLevel11FromQty,0) = 0 AND ISNULL(@intPriceLevel11ToQty,0) = 0
	BEGIN
		SET @intPriceLevel11FromQty = -1
		SET @intPriceLevel11ToQty = -1
	END
	IF ISNULL(@intPriceLevel12FromQty,0) = 0 AND ISNULL(@intPriceLevel12ToQty,0) = 0
	BEGIN
		SET @intPriceLevel12FromQty = -1
		SET @intPriceLevel12ToQty = -1
	END
	IF ISNULL(@intPriceLevel13FromQty,0) = 0 AND ISNULL(@intPriceLevel13ToQty,0) = 0
	BEGIN
		SET @intPriceLevel13FromQty = -1
		SET @intPriceLevel13ToQty = -1
	END
	IF ISNULL(@intPriceLevel14FromQty,0) = 0 AND ISNULL(@intPriceLevel14ToQty,0) = 0
	BEGIN
		SET @intPriceLevel14FromQty = -1
		SET @intPriceLevel14ToQty = -1
	END
	IF ISNULL(@intPriceLevel15FromQty,0) = 0 AND ISNULL(@intPriceLevel15ToQty,0) = 0
	BEGIN
		SET @intPriceLevel15FromQty = -1
		SET @intPriceLevel15ToQty = -1
	END
	IF ISNULL(@intPriceLevel16FromQty,0) = 0 AND ISNULL(@intPriceLevel16ToQty,0) = 0
	BEGIN
		SET @intPriceLevel16FromQty = -1
		SET @intPriceLevel16ToQty = -1
	END
	IF ISNULL(@intPriceLevel17FromQty,0) = 0 AND ISNULL(@intPriceLevel17ToQty,0) = 0
	BEGIN
		SET @intPriceLevel17FromQty = -1
		SET @intPriceLevel17ToQty = -1
	END
	IF ISNULL(@intPriceLevel18FromQty,0) = 0 AND ISNULL(@intPriceLevel18ToQty,0) = 0
	BEGIN
		SET @intPriceLevel18FromQty = -1
		SET @intPriceLevel18ToQty = -1
	END
	IF ISNULL(@intPriceLevel19FromQty,0) = 0 AND ISNULL(@intPriceLevel19ToQty,0) = 0
	BEGIN
		SET @intPriceLevel19FromQty = -1
		SET @intPriceLevel19ToQty = -1
	END
	IF ISNULL(@intPriceLevel20FromQty,0) = 0 AND ISNULL(@intPriceLevel20ToQty,0) = 0
	BEGIN
		SET @intPriceLevel20FromQty = -1
		SET @intPriceLevel20ToQty = -1
	END



	IF ISNULL(@intPriceLevel1FromQty,0) = 0 OR ISNULL(@intPriceLevel1ToQty,0) = 0
	BEGIN
		RAISERROR('QTY_FROM_AND_TO_MUST_BE_GREATER_THEN_0_FOR_PRICE_LEVEL_1',16,1)
		RETURN
	END
	IF ISNULL(@intPriceLevel2FromQty,0) = 0 OR ISNULL(@intPriceLevel2ToQty,0) = 0
	BEGIN
		RAISERROR('QTY_FROM_AND_TO_MUST_BE_GREATER_THEN_0_FOR_PRICE_LEVEL_2',16,1)
		RETURN
	END
	IF ISNULL(@intPriceLevel3FromQty,0) = 0 OR ISNULL(@intPriceLevel3ToQty,0) = 0
	BEGIN
		RAISERROR('QTY_FROM_AND_TO_MUST_BE_GREATER_THEN_0_FOR_PRICE_LEVEL_3',16,1)
		RETURN
	END
	IF ISNULL(@intPriceLevel4FromQty,0) = 0 OR ISNULL(@intPriceLevel4ToQty,0) = 0
	BEGIN
		RAISERROR('QTY_FROM_AND_TO_MUST_BE_GREATER_THEN_0_FOR_PRICE_LEVEL_4',16,1)
		RETURN
	END
	IF ISNULL(@intPriceLevel5FromQty,0) = 0 OR ISNULL(@intPriceLevel5ToQty,0) = 0
	BEGIN
		RAISERROR('QTY_FROM_AND_TO_MUST_BE_GREATER_THEN_0_FOR_PRICE_LEVEL_5',16,1)
		RETURN
	END
	IF ISNULL(@intPriceLevel6FromQty,0) = 0 OR ISNULL(@intPriceLevel6ToQty,0) = 0
	BEGIN
		RAISERROR('QTY_FROM_AND_TO_MUST_BE_GREATER_THEN_0_FOR_PRICE_LEVEL_6',16,1)
		RETURN
	END
	IF ISNULL(@intPriceLevel7FromQty,0) = 0 OR ISNULL(@intPriceLevel7ToQty,0) = 0
	BEGIN
		RAISERROR('QTY_FROM_AND_TO_MUST_BE_GREATER_THEN_0_FOR_PRICE_LEVEL_7',16,1)
		RETURN
	END
	IF ISNULL(@intPriceLevel8FromQty,0) = 0 OR ISNULL(@intPriceLevel8ToQty,0) = 0
	BEGIN
		RAISERROR('QTY_FROM_AND_TO_MUST_BE_GREATER_THEN_0_FOR_PRICE_LEVEL_8',16,1)
		RETURN
	END
	IF ISNULL(@intPriceLevel9FromQty,0) = 0 OR ISNULL(@intPriceLevel9ToQty,0) = 0
	BEGIN
		RAISERROR('QTY_FROM_AND_TO_MUST_BE_GREATER_THEN_0_FOR_PRICE_LEVEL_9',16,1)
		RETURN
	END
	IF ISNULL(@intPriceLevel10FromQty,0) = 0 OR ISNULL(@intPriceLevel10ToQty,0) = 0
	BEGIN
		RAISERROR('QTY_FROM_AND_TO_MUST_BE_GREATER_THEN_0_FOR_PRICE_LEVEL_10',16,1)
		RETURN
	END
	IF ISNULL(@intPriceLevel11FromQty,0) = 0 OR ISNULL(@intPriceLevel11ToQty,0) = 0
	BEGIN
		RAISERROR('QTY_FROM_AND_TO_MUST_BE_GREATER_THEN_0_FOR_PRICE_LEVEL_11',16,1)
		RETURN
	END
	IF ISNULL(@intPriceLevel12FromQty,0) = 0 OR ISNULL(@intPriceLevel12ToQty,0) = 0
	BEGIN
		RAISERROR('QTY_FROM_AND_TO_MUST_BE_GREATER_THEN_0_FOR_PRICE_LEVEL_12',16,1)
		RETURN
	END
	IF ISNULL(@intPriceLevel13FromQty,0) = 0 OR ISNULL(@intPriceLevel13ToQty,0) = 0
	BEGIN
		RAISERROR('QTY_FROM_AND_TO_MUST_BE_GREATER_THEN_0_FOR_PRICE_LEVEL_13',16,1)
		RETURN
	END
	IF ISNULL(@intPriceLevel14FromQty,0) = 0 OR ISNULL(@intPriceLevel14ToQty,0) = 0
	BEGIN
		RAISERROR('QTY_FROM_AND_TO_MUST_BE_GREATER_THEN_0_FOR_PRICE_LEVEL_14',16,1)
		RETURN
	END
	IF ISNULL(@intPriceLevel15FromQty,0) = 0 OR ISNULL(@intPriceLevel15ToQty,0) = 0
	BEGIN
		RAISERROR('QTY_FROM_AND_TO_MUST_BE_GREATER_THEN_0_FOR_PRICE_LEVEL_15',16,1)
		RETURN
	END
	IF ISNULL(@intPriceLevel16FromQty,0) = 0 OR ISNULL(@intPriceLevel16ToQty,0) = 0
	BEGIN
		RAISERROR('QTY_FROM_AND_TO_MUST_BE_GREATER_THEN_0_FOR_PRICE_LEVEL_16',16,1)
		RETURN
	END
	IF ISNULL(@intPriceLevel17FromQty,0) = 0 OR ISNULL(@intPriceLevel17ToQty,0) = 0
	BEGIN
		RAISERROR('QTY_FROM_AND_TO_MUST_BE_GREATER_THEN_0_FOR_PRICE_LEVEL_17',16,1)
		RETURN
	END
	IF ISNULL(@intPriceLevel18FromQty,0) = 0 OR ISNULL(@intPriceLevel18ToQty,0) = 0
	BEGIN
		RAISERROR('QTY_FROM_AND_TO_MUST_BE_GREATER_THEN_0_FOR_PRICE_LEVEL_18',16,1)
		RETURN
	END
	IF ISNULL(@intPriceLevel19FromQty,0) = 0 OR ISNULL(@intPriceLevel19ToQty,0) = 0
	BEGIN
		RAISERROR('QTY_FROM_AND_TO_MUST_BE_GREATER_THEN_0_FOR_PRICE_LEVEL_19',16,1)
		RETURN
	END
	IF ISNULL(@intPriceLevel20FromQty,0) = 0 OR ISNULL(@intPriceLevel20ToQty,0) = 0
	BEGIN
		RAISERROR('QTY_FROM_AND_TO_MUST_BE_GREATER_THEN_0_FOR_PRICE_LEVEL_20',16,1)
		RETURN
	END


	DECLARE @Defualt TABLE
	(
		PriceLevelID INT
		,tintRuleType TINYINT
		,tintDiscountType TINYINT
		,monPrice DECIMAL(20,5)
		,intFromQty INT
		,intToQty INT
		,vcName VARCHAR(300)
	)

	INSERT INTO @Defualt
	(
		PriceLevelID
		,monPrice
		,intFromQty
		,intToQty
		,vcName
	)
	VALUES
	(1,-1,-1,-1,'-1'),
	(2,-1,-1,-1,'-1'),
	(3,-1,-1,-1,'-1'),
	(4,-1,-1,-1,'-1'),
	(5,-1,-1,-1,'-1'),
	(6,-1,-1,-1,'-1'),
	(7,-1,-1,-1,'-1'),
	(8,-1,-1,-1,'-1'),
	(9,-1,-1,-1,'-1'),
	(10,-1,-1,-1,'-1'),
	(11,-1,-1,-1,'-1'),
	(12,-1,-1,-1,'-1'),
	(13,-1,-1,-1,'-1'),
	(14,-1,-1,-1,'-1'),
	(15,-1,-1,-1,'-1'),
	(16,-1,-1,-1,'-1'),
	(17,-1,-1,-1,'-1'),
	(18,-1,-1,-1,'-1'),
	(19,-1,-1,-1,'-1'),
	(20,-1,-1,-1,'-1')

	-- FIRST REPLACE -1 WITH EXISTING ITEM PRICES, REPLEACE -1 IN FROM AND TO QTY ONLY WHEN RuleType is Deduct From List Price or Add to vendor Cost
	UPDATE
		t1
	SET
		t1.monPrice = ISNULL(t2.decDiscount,t1.monPrice)
		,tintRuleType = (CASE WHEN ISNULL(@tintRuleType,0) <> -1 THEN @tintRuleType ELSE ISNULL(t2.tintRuleType,t1.tintRuleType) END)
		,tintDiscountType = (CASE WHEN ISNULL(@tintDiscountType,0) <> -1 THEN @tintDiscountType ELSE ISNULL(t2.tintDiscountType,t1.tintDiscountType) END)
		,t1.intFromQty = (CASE WHEN t2.tintRuleType = 1 OR t2.tintRuleType = 2 THEN t2.intFromQty ELSE t1.intFromQty END)
		,t1.intToQty = (CASE WHEN t2.tintRuleType = 1 OR t2.tintRuleType = 2 THEN t2.intToQty ELSE t1.intToQty END)
		,t1.vcName = ISNULL(t2.vcName,'')
	FROM
		@Defualt t1
	LEFT JOIN
	(
		SELECT
			ROW_NUMBER() OVER(ORDER BY numPricingID) AS RowID 
			,decDiscount
			,intFromQty
			,intToQty
			,tintRuleType
			,tintDiscountType
			,vcName
		FROM 
			PricingTable WHERE numItemCode=@numItemCode AND ISNULL(numPriceRuleID,0)=0
	) t2
	ON
		t1.PriceLevelID=t2.RowID

	DECLARE @localRuleType TINYINT
	DECLARE @localDiscountType TINYINT

	SELECT TOP 1
		@localRuleType = tintRuleType
		,@localDiscountType = tintDiscountType
	FROM
		@Defualt AS t2
		

	IF ISNULL(@localRuleType,0) = 0
	BEGIN
		RAISERROR('INVALID_PRICE_RULE_TYPE',16,1)
		RETURN
	END
	ELSE IF (@localRuleType = 1 OR @localRuleType = 2) AND @localDiscountType NOT IN (1,2,3)
	BEGIN
		RAISERROR('INVALID_PRICE_LEVEL_DISCOUNT_TYPE',16,1)
		RETURN
	END
	ELSE IF @localRuleType = 3
	BEGIN
		SET @tintDiscountType = 0
	END

	-- REPLACE -1 WITH NEW PRICES IF PROVIDED FROM ITEM IMPORT (IF COLUMN IF NOT AVAILABLE IN IMPORT FILE THAN VALUE WILL BE -1)
	-- REPLACE -1 WITH NEW QTY FROM IF PROVIDED FROM ITEM IMPORT (IF COLUMN IF NOT AVAILABLE IN IMPORT FILE THAN VALUE WILL BE -1)
	-- REPLACE -1 WITH NEW QTY TO IF PROVIDED FROM ITEM IMPORT (IF COLUMN IF NOT AVAILABLE IN IMPORT FILE THAN VALUE WILL BE -1)
	UPDATE 
		@Defualt 
	SET 
		monPrice = (CASE WHEN ISNULL(@monPriceLevel1,0) <> -1 THEN ISNULL(@monPriceLevel1,0) ELSE monPrice END)
		,intFromQty = (CASE WHEN ISNULL(@intPriceLevel1FromQty,0) <> -1 THEN ISNULL(@intPriceLevel1FromQty,0) ELSE intFromQty END)
		,intToQty = (CASE WHEN ISNULL(@intPriceLevel1ToQty,0) <> -1 THEN ISNULL(@intPriceLevel1ToQty,0) ELSE intToQty END)
		,vcName = (CASE WHEN ISNULL(@vcPriceLevel1Name,'') <> '-1' THEN ISNULL(@vcPriceLevel1Name,0) ELSE vcName END)
	WHERE 
		PriceLevelID = 1

	UPDATE 
		@Defualt 
	SET 
		monPrice = (CASE WHEN ISNULL(@monPriceLevel2,0) <> -1 THEN ISNULL(@monPriceLevel2,0) ELSE monPrice END) 
		,intFromQty = (CASE WHEN ISNULL(@intPriceLevel2FromQty,0) <> -1 THEN ISNULL(@intPriceLevel2FromQty,0) ELSE intFromQty END)
		,intToQty = (CASE WHEN ISNULL(@intPriceLevel2ToQty,0) <> -1 THEN ISNULL(@intPriceLevel2ToQty,0) ELSE intToQty END)
		,vcName = (CASE WHEN ISNULL(@vcPriceLevel2Name,'') <> '-1' THEN ISNULL(@vcPriceLevel2Name,0) ELSE vcName END)
	WHERE 
		PriceLevelID = 2

	UPDATE 
		@Defualt 
	SET 
		monPrice = (CASE WHEN ISNULL(@monPriceLevel3,0) <> -1 THEN ISNULL(@monPriceLevel3,0) ELSE monPrice END) 
		,intFromQty = (CASE WHEN ISNULL(@intPriceLevel3FromQty,0) <> -1 THEN ISNULL(@intPriceLevel3FromQty,0) ELSE intFromQty END)
		,intToQty = (CASE WHEN ISNULL(@intPriceLevel3ToQty,0) <> -1 THEN ISNULL(@intPriceLevel3ToQty,0) ELSE intToQty END)
		,vcName = (CASE WHEN ISNULL(@vcPriceLevel3Name,'') <> '-1' THEN ISNULL(@vcPriceLevel3Name,0) ELSE vcName END)
	WHERE 
		PriceLevelID = 3

	UPDATE 
		@Defualt 
	SET 
		monPrice = (CASE WHEN ISNULL(@monPriceLevel4,0) <> -1 THEN ISNULL(@monPriceLevel4,0) ELSE monPrice END)
		,intFromQty = (CASE WHEN ISNULL(@intPriceLevel4FromQty,0) <> -1 THEN ISNULL(@intPriceLevel4FromQty,0) ELSE intFromQty END)
		,intToQty = (CASE WHEN ISNULL(@intPriceLevel4ToQty,0) <> -1 THEN ISNULL(@intPriceLevel4ToQty,0) ELSE intToQty END)
		,vcName = (CASE WHEN ISNULL(@vcPriceLevel4Name,'') <> '-1' THEN ISNULL(@vcPriceLevel4Name,0) ELSE vcName END)
	WHERE 
		PriceLevelID = 4

	UPDATE 
		@Defualt
	SET 
		monPrice = (CASE WHEN ISNULL(@monPriceLevel5,0) <> -1 THEN ISNULL(@monPriceLevel5,0) ELSE monPrice END)
		,intFromQty = (CASE WHEN ISNULL(@intPriceLevel5FromQty,0) <> -1 THEN ISNULL(@intPriceLevel5FromQty,0) ELSE intFromQty END)
		,intToQty = (CASE WHEN ISNULL(@intPriceLevel5ToQty,0) <> -1 THEN ISNULL(@intPriceLevel5ToQty,0) ELSE intToQty END)
		,vcName = (CASE WHEN ISNULL(@vcPriceLevel5Name,'') <> '-1' THEN ISNULL(@vcPriceLevel5Name,0) ELSE vcName END)
	WHERE 
		PriceLevelID = 5
	
	UPDATE
		@Defualt 
	SET 
		monPrice = (CASE WHEN ISNULL(@monPriceLevel6,0) <> -1 THEN ISNULL(@monPriceLevel6,0) ELSE monPrice END)
		,intFromQty = (CASE WHEN ISNULL(@intPriceLevel6FromQty,0) <> -1 THEN ISNULL(@intPriceLevel6FromQty,0) ELSE intFromQty END)
		,intToQty = (CASE WHEN ISNULL(@intPriceLevel6ToQty,0) <> -1 THEN ISNULL(@intPriceLevel6ToQty,0) ELSE intToQty END)
		,vcName = (CASE WHEN ISNULL(@vcPriceLevel6Name,'') <> '-1' THEN ISNULL(@vcPriceLevel6Name,0) ELSE vcName END)
	WHERE 
		PriceLevelID = 6

	UPDATE 
		@Defualt 
	SET 
		monPrice = (CASE WHEN ISNULL(@monPriceLevel7,0) <> -1 THEN ISNULL(@monPriceLevel7,0) ELSE monPrice END)
		,intFromQty = (CASE WHEN ISNULL(@intPriceLevel7FromQty,0) <> -1 THEN ISNULL(@intPriceLevel7FromQty,0) ELSE intFromQty END)
		,intToQty = (CASE WHEN ISNULL(@intPriceLevel7ToQty,0) <> -1 THEN ISNULL(@intPriceLevel7ToQty,0) ELSE intToQty END)
		,vcName = (CASE WHEN ISNULL(@vcPriceLevel7Name,'') <> '-1' THEN ISNULL(@vcPriceLevel7Name,0) ELSE vcName END)
	WHERE 
		PriceLevelID = 7

	UPDATE 
		@Defualt 
	SET 
		monPrice = (CASE WHEN ISNULL(@monPriceLevel8,0) <> -1 THEN ISNULL(@monPriceLevel8,0) ELSE monPrice END)
		,intFromQty = (CASE WHEN ISNULL(@intPriceLevel8FromQty,0) <> -1 THEN ISNULL(@intPriceLevel8FromQty,0) ELSE intFromQty END)
		,intToQty = (CASE WHEN ISNULL(@intPriceLevel8ToQty,0) <> -1 THEN ISNULL(@intPriceLevel8ToQty,0) ELSE intToQty END)
		,vcName = (CASE WHEN ISNULL(@vcPriceLevel8Name,'') <> '-1' THEN ISNULL(@vcPriceLevel8Name,0) ELSE vcName END)
	WHERE
		 PriceLevelID = 8
	
	UPDATE 
		@Defualt 
	SET 
		monPrice = (CASE WHEN ISNULL(@monPriceLevel9,0) <> -1 THEN ISNULL(@monPriceLevel9,0) ELSE monPrice END)
		,intFromQty = (CASE WHEN ISNULL(@intPriceLevel9FromQty,0) <> -1 THEN ISNULL(@intPriceLevel9FromQty,0) ELSE intFromQty END)
		,intToQty = (CASE WHEN ISNULL(@intPriceLevel9ToQty,0) <> -1 THEN ISNULL(@intPriceLevel9ToQty,0) ELSE intToQty END)
		,vcName = (CASE WHEN ISNULL(@vcPriceLevel9Name,'') <> '-1' THEN ISNULL(@vcPriceLevel9Name,0) ELSE vcName END)
	WHERE 
		PriceLevelID = 9
	
	UPDATE 
		@Defualt 
	SET 
		monPrice = (CASE WHEN ISNULL(@monPriceLevel10,0) <> -1 THEN ISNULL(@monPriceLevel10,0) ELSE monPrice END)
		,intFromQty = (CASE WHEN ISNULL(@intPriceLevel10FromQty,0) <> -1 THEN ISNULL(@intPriceLevel10FromQty,0) ELSE intFromQty END)
		,intToQty = (CASE WHEN ISNULL(@intPriceLevel10ToQty,0) <> -1 THEN ISNULL(@intPriceLevel10ToQty,0) ELSE intToQty END)
		,vcName = (CASE WHEN ISNULL(@vcPriceLevel10Name,'') <> '-1' THEN ISNULL(@vcPriceLevel10Name,0) ELSE vcName END)
	WHERE 
		PriceLevelID = 10
	
	UPDATE 
		@Defualt 
	SET 
		monPrice = (CASE WHEN ISNULL(@monPriceLevel11,0) <> -1 THEN ISNULL(@monPriceLevel11,0) ELSE monPrice END)
		,intFromQty = (CASE WHEN ISNULL(@intPriceLevel11FromQty,0) <> -1 THEN ISNULL(@intPriceLevel11FromQty,0) ELSE intFromQty END)
		,intToQty = (CASE WHEN ISNULL(@intPriceLevel11ToQty,0) <> -1 THEN ISNULL(@intPriceLevel11ToQty,0) ELSE intToQty END)
		,vcName = (CASE WHEN ISNULL(@vcPriceLevel11Name,'') <> '-1' THEN ISNULL(@vcPriceLevel11Name,0) ELSE vcName END)
	WHERE 
		PriceLevelID = 11
	
	UPDATE
		@Defualt 
	SET 
		monPrice = (CASE WHEN ISNULL(@monPriceLevel12,0) <> -1 THEN ISNULL(@monPriceLevel12,0) ELSE monPrice END)
		,intFromQty = (CASE WHEN ISNULL(@intPriceLevel12FromQty,0) <> -1 THEN ISNULL(@intPriceLevel12FromQty,0) ELSE intFromQty END)
		,intToQty = (CASE WHEN ISNULL(@intPriceLevel12ToQty,0) <> -1 THEN ISNULL(@intPriceLevel12ToQty,0) ELSE intToQty END)
		,vcName = (CASE WHEN ISNULL(@vcPriceLevel12Name,'') <> '-1' THEN ISNULL(@vcPriceLevel12Name,0) ELSE vcName END)
	WHERE
		PriceLevelID = 12
	
	UPDATE 
		@Defualt 
	SET 
		monPrice = (CASE WHEN ISNULL(@monPriceLevel13,0) <> -1 THEN ISNULL(@monPriceLevel13,0) ELSE monPrice END)
		,intFromQty = (CASE WHEN ISNULL(@intPriceLevel13FromQty,0) <> -1 THEN ISNULL(@intPriceLevel13FromQty,0) ELSE intFromQty END)
		,intToQty = (CASE WHEN ISNULL(@intPriceLevel13ToQty,0) <> -1 THEN ISNULL(@intPriceLevel13ToQty,0) ELSE intToQty END)
		,vcName = (CASE WHEN ISNULL(@vcPriceLevel13Name,'') <> '-1' THEN ISNULL(@vcPriceLevel13Name,0) ELSE vcName END)
	WHERE 
		PriceLevelID = 13
	
	UPDATE 
		@Defualt 
	SET 
		monPrice = (CASE WHEN ISNULL(@monPriceLevel14,0) <> -1 THEN ISNULL(@monPriceLevel14,0) ELSE monPrice END) 
		,intFromQty = (CASE WHEN ISNULL(@intPriceLevel14FromQty,0) <> -1 THEN ISNULL(@intPriceLevel14FromQty,0) ELSE intFromQty END)
		,intToQty = (CASE WHEN ISNULL(@intPriceLevel14ToQty,0) <> -1 THEN ISNULL(@intPriceLevel14ToQty,0) ELSE intToQty END)
		,vcName = (CASE WHEN ISNULL(@vcPriceLevel14Name,'') <> '-1' THEN ISNULL(@vcPriceLevel14Name,0) ELSE vcName END)
	WHERE 
		PriceLevelID = 14
	
	UPDATE 
		@Defualt 
	SET 
		monPrice = (CASE WHEN ISNULL(@monPriceLevel15,0) <> -1 THEN ISNULL(@monPriceLevel15,0) ELSE monPrice END)
		,intFromQty = (CASE WHEN ISNULL(@intPriceLevel15FromQty,0) <> -1 THEN ISNULL(@intPriceLevel15FromQty,0) ELSE intFromQty END)
		,intToQty = (CASE WHEN ISNULL(@intPriceLevel15ToQty,0) <> -1 THEN ISNULL(@intPriceLevel15ToQty,0) ELSE intToQty END)
		,vcName = (CASE WHEN ISNULL(@vcPriceLevel15Name,'') <> '-1' THEN ISNULL(@vcPriceLevel15Name,0) ELSE vcName END)
	WHERE 
		PriceLevelID = 15
	
	UPDATE 
		@Defualt 
	SET 
		monPrice = (CASE WHEN ISNULL(@monPriceLevel16,0) <> -1 THEN ISNULL(@monPriceLevel16,0) ELSE monPrice END)
		,intFromQty = (CASE WHEN ISNULL(@intPriceLevel16FromQty,0) <> -1 THEN ISNULL(@intPriceLevel16FromQty,0) ELSE intFromQty END)
		,intToQty = (CASE WHEN ISNULL(@intPriceLevel16ToQty,0) <> -1 THEN ISNULL(@intPriceLevel16ToQty,0) ELSE intToQty END)
		,vcName = (CASE WHEN ISNULL(@vcPriceLevel16Name,'') <> '-1' THEN ISNULL(@vcPriceLevel16Name,0) ELSE vcName END)
	WHERE 
		PriceLevelID = 16
	
	UPDATE 
		@Defualt 
	SET 
		monPrice = (CASE WHEN ISNULL(@monPriceLevel17,0) <> -1 THEN ISNULL(@monPriceLevel17,0) ELSE monPrice END)
		,intFromQty = (CASE WHEN ISNULL(@intPriceLevel17FromQty,0) <> -1 THEN ISNULL(@intPriceLevel17FromQty,0) ELSE intFromQty END)
		,intToQty = (CASE WHEN ISNULL(@intPriceLevel17ToQty,0) <> -1 THEN ISNULL(@intPriceLevel17ToQty,0) ELSE intToQty END)
		,vcName = (CASE WHEN ISNULL(@vcPriceLevel17Name,'') <> '-1' THEN ISNULL(@vcPriceLevel17Name,0) ELSE vcName END)
	WHERE 
		PriceLevelID = 17
	
	UPDATE 
		@Defualt 
	SET 
		monPrice = (CASE WHEN ISNULL(@monPriceLevel18,0) <> -1 THEN ISNULL(@monPriceLevel18,0) ELSE monPrice END)
		,intFromQty = (CASE WHEN ISNULL(@intPriceLevel18FromQty,0) <> -1 THEN ISNULL(@intPriceLevel18FromQty,0) ELSE intFromQty END)
		,intToQty = (CASE WHEN ISNULL(@intPriceLevel18ToQty,0) <> -1 THEN ISNULL(@intPriceLevel18ToQty,0) ELSE intToQty END)
		,vcName = (CASE WHEN ISNULL(@vcPriceLevel18Name,'') <> '-1' THEN ISNULL(@vcPriceLevel18Name,0) ELSE vcName END)
	WHERE 
		PriceLevelID = 18
	
	UPDATE 
		@Defualt 
	SET 
		monPrice = (CASE WHEN ISNULL(@monPriceLevel19,0) <> -1 THEN ISNULL(@monPriceLevel19,0) ELSE monPrice END)
		,intFromQty = (CASE WHEN ISNULL(@intPriceLevel19FromQty,0) <> -1 THEN ISNULL(@intPriceLevel19FromQty,0) ELSE intFromQty END)
		,intToQty = (CASE WHEN ISNULL(@intPriceLevel19ToQty,0) <> -1 THEN ISNULL(@intPriceLevel19ToQty,0) ELSE intToQty END)
		,vcName = (CASE WHEN ISNULL(@vcPriceLevel19Name,'') <> '-1' THEN ISNULL(@vcPriceLevel19Name,0) ELSE vcName END)
	WHERE 
		PriceLevelID = 19
	
	UPDATE 
		@Defualt 
	SET 
		monPrice = (CASE WHEN ISNULL(@monPriceLevel20,0) <> -1 THEN ISNULL(@monPriceLevel20,0) ELSE monPrice END)
		,intFromQty = (CASE WHEN ISNULL(@intPriceLevel20FromQty,0) <> -1 THEN ISNULL(@intPriceLevel20FromQty,0) ELSE intFromQty END)
		,intToQty = (CASE WHEN ISNULL(@intPriceLevel20ToQty,0) <> -1 THEN ISNULL(@intPriceLevel20ToQty,0) ELSE intToQty END)
		,vcName = (CASE WHEN ISNULL(@vcPriceLevel20Name,'') <> '-1' THEN ISNULL(@vcPriceLevel20Name,0) ELSE vcName END)
	WHERE 
		PriceLevelID = 20
	
	DECLARE @MaxPriceLevelIDWithSomeValue AS INT
	
	SELECT @MaxPriceLevelIDWithSomeValue=MAX(PriceLevelID) FROM @Defualt WHERE (monPrice <> -1 OR intFromQty <> -1 OR intToQty <> -1)

	-- NOW REPLACE PRICE -1 TO 0 IN ALL PRICE LEVELS WHICH ARE LESS THAN @MaxPriceLevelIDWithSomeValue 
	-- (THIS REQUIRED BECAUSE IF USER HAS PROVIDED PRICE LEVEL 1 AND PRICE LEVEL 6 COLUMN IN IMPORT FILE THEN WE HAVE TO ADD 0 FROM PRICE LEVEL 2,3,4,5)
	UPDATE
		@Defualt
	SET
		monPrice = (CASE WHEN monPrice = -1 THEN 0 ELSE monPrice END)
		,intFromQty = (CASE WHEN intFromQty = -1 THEN 0 ELSE intFromQty END)
		,intToQty = (CASE WHEN intToQty = -1 THEN 0 ELSE intToQty END)
		,vcName = (CASE WHEN vcName = '-1' THEN '' ELSE vcName END)
	WHERE
		PriceLevelID <= @MaxPriceLevelIDWithSomeValue
		AND (monPrice = -1 OR intFromQty=-1 OR intToQty=-1)

	IF EXISTS(SELECT * FROM @Defualt WHERE PriceLevelID <= @MaxPriceLevelIDWithSomeValue AND tintRuleType IN (1,2) AND (ISNULL(intFromQty,0) = 0 OR ISNULL(intToQty,0) = 0))
	BEGIN
		RAISERROR('QTY_FROM_AND_TO_MUST_BE_GREATER_THEN_0',16,1)
		RETURN
	END

	IF EXISTS(SELECT * FROM @Defualt WHERE PriceLevelID <= @MaxPriceLevelIDWithSomeValue AND tintRuleType IN (1,2) AND ISNULL(tintDiscountType,0) = 0)
	BEGIN
		RAISERROR('INVALID_PRICE_LEVEL_DISCOUNT_TYPE',16,1)
		RETURN
	END

	BEGIN TRY
	BEGIN TRANSACTION
		DELETE FROM PricingTable WHERE numItemCode=@numItemCode

		INSERT INTO PricingTable
		(
			numPriceRuleID,numItemCode,vcName,decDiscount,tintRuleType,tintDiscountType,intFromQty,intToQty
		)
		SELECT
			0,@numItemCode,vcName,monPrice,tintRuleType,(CASE WHEN tintRuleType = 3 THEN 0 ELSE tintDiscountType END),(CASE WHEN tintRuleType = 3 THEN 0 ELSE intFromQty END),(CASE WHEN tintRuleType = 3 THEN 0 ELSE intToQty END)
		FROM
			@Defualt
		WHERE
			PriceLevelID <= @MaxPriceLevelIDWithSomeValue

	COMMIT
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000)
		DECLARE @ErrorNumber INT
		DECLARE @ErrorSeverity INT
		DECLARE @ErrorState INT
		DECLARE @ErrorLine INT
		DECLARE @ErrorProcedure NVARCHAR(200)

		IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorNumber = ERROR_NUMBER(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE(),
			@ErrorLine = ERROR_LINE(),
			@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

		RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
	END CATCH

END
--Created By Anoop Jayaraj          
--exec USP_ItemDetailsForEcomm @numItemCode=859064,@numWareHouseID=1074,@numDomainID=172,@numSiteId=90
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_itemdetailsforecomm')
DROP PROCEDURE USP_ItemDetailsForEcomm
GO
CREATE PROCEDURE [dbo].[USP_ItemDetailsForEcomm]
@numItemCode as numeric(9),          
@numWareHouseID as numeric(9),    
@numDomainID as numeric(9),
@numSiteId as numeric(9),
@vcCookieId as VARCHAR(MAX)=null,
@numUserCntID AS NUMERIC(9)=0,
@numDivisionID AS NUMERIC(18,0) = 0                                        
as     
/*Removed warehouse parameter, and taking Default warehouse by default from DB*/
    
declare @bitShowInStock as bit        
declare @bitShowQuantity as BIT
declare @bitAutoSelectWarehouse as bit        
--declare @tintColumns as tinyint        
DECLARE @numDefaultWareHouseID as numeric(9)
DECLARE @numDefaultRelationship AS NUMERIC(18,0)
DECLARE @numDefaultProfile AS NUMERIC(18,0)
DECLARE @tintPreLoginPriceLevel AS TINYINT
DECLARE @tintPriceLevel AS TINYINT
set @bitShowInStock=0        
set @bitShowQuantity=0        
--set @tintColumns=1        
        
IF ISNULL(@numDivisionID,0) > 0
BEGIN
	SELECT @numDefaultRelationship=ISNULL(tintCRMType,0),@numDefaultProfile=ISNULL(vcProfile,0),@tintPriceLevel=ISNULL(tintPriceLevel,0) FROM DivisionMaster INNER JOIN CompanyInfo ON DivisionMaster.numCompanyID=CompanyInfo.numCompanyId WHERE numDivisionID=@numDivisionID AND DivisionMaster.numDomainID=@numDomainID
END     
        
SELECT 
	@bitShowInStock=ISNULL(bitShowInStock,0)
	,@bitShowQuantity=ISNULL(bitShowQOnHand,0)
	,@bitAutoSelectWarehouse=ISNULL(bitAutoSelectWarehouse,0)
	,@numDefaultRelationship=numRelationshipId
	,@numDefaultProfile=numProfileId
	,@tintPreLoginPriceLevel=(CASE WHEN @numDivisionID > 0 THEN (CASE WHEN ISNULL(bitShowPriceUsingPriceLevel,0)=1 THEN ISNULL(@tintPriceLevel,0) ELSE 0 END) ELSE ISNULL(tintPreLoginProceLevel,0) END)
FROM 
	eCommerceDTL 
WHERE 
	numDomainID=@numDomainID

	
    


IF @numWareHouseID=0
BEGIN
	SELECT @numDefaultWareHouseID=ISNULL(numDefaultWareHouseID,0) FROM [eCommerceDTL] WHERE [numDomainID]=@numDomainID	
	SET @numWareHouseID =@numDefaultWareHouseID;
END

DECLARE @vcWarehouseIDs AS VARCHAR(1000)
IF @bitAutoSelectWarehouse = 1
BEGIN		
	SELECT @vcWarehouseIDs = ISNULL( STUFF((SELECT DISTINCT ',' + CAST(numWarehouseID AS VARCHAR(10)) FROM [dbo].[Warehouses] WHERE [Warehouses].[numDomainID] = @numDomainID FOR XML PATH('')),1, 1, '') , '')
END
ELSE
BEGIN
	SET @vcWarehouseIDs = @numWareHouseID
END

/*If qty is not found in default warehouse then choose next warehouse with sufficient qty */
IF @bitAutoSelectWarehouse = 1 AND ( SELECT COUNT(*) FROM dbo.WareHouseItems WHERE  numItemID = @numItemCode AND numWarehouseID= @numWareHouseID ) = 0 
BEGIN
	SELECT TOP 1 @numDefaultWareHouseID = numWareHouseID FROM dbo.WareHouseItems WHERE  numItemID = @numItemCode AND numOnHand>0 /*TODO: Pass actuall qty from cart*/ ORDER BY numOnHand DESC 
	IF (ISNULL(@numDefaultWareHouseID,0)>0)
		SET @numWareHouseID =@numDefaultWareHouseID;
END

      DECLARE @UOMConversionFactor AS DECIMAL(18,5)
      
      Select @UOMConversionFactor= ISNULL(dbo.fn_UOMConversion(ISNULL(I.numSaleUnit,0),I.numItemCode,I.numDomainId,ISNULL(I.numBaseUnit,0)),0)
      FROM Item I where numItemCode=@numItemCode   
	   
	  DECLARE @PromotionOffers AS DECIMAL(18,2)=0     
	  IF(@vcCookieId!=null OR @vcCookieId!='')   
	  BEGIN                    
SET @PromotionOffers=(SELECT TOP 1 CASE WHEN P.tintDiscountType=1  
				THEN (I.monListPrice*fltDiscountValue)/100
				WHEN P.tintDiscountType=2
				THEN fltDiscountValue
				WHEN P.tintDiscountType=3
				THEN fltDiscountValue*I.monListPrice
				ELSE 0 END
FROM CartItems AS C
LEFT JOIN PromotionOffer AS P ON P.numProId=C.PromotionID
LEFT JOIN Item I ON I.numItemCode=C.numItemCode
WHERE I.numItemCode=@numItemCode AND I.numItemCode=@numDomainID AND C.vcCookieId=@vcCookieId AND C.numUserCntId=@numUserCntID AND C.fltDiscount=0 AND 
(I.numItemCode IN (SELECT PIS.numValue FROM PromotionOfferItems AS PIS WHERE PIS.numProId=P.numProId AND PIS.tintType=1 AND PIS.tintRecordType=6) OR
I.numItemClassification IN (SELECT PIS.numValue FROM PromotionOfferItems AS PIS WHERE PIS.numProId=P.numProId AND PIS.tintType=2 AND PIS.tintRecordType=6))
AND P.numDomainId=@numDomainID 
		AND ISNULL(bitEnabled,0)=1 
		AND ISNULL(bitAppliesToSite,0)=1 
		AND 1 = (CASE WHEN bitRequireCouponCode = 1 THEN CASE WHEN ISNULL(intCouponCodeUsed,0) < ISNULL(tintUsageLimit,0) OR ISNULL(tintUsageLimit,0) = 0  THEN 1 ELSE 0 END ELSE 1 END)
		AND 1 = (CASE WHEN bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN CAST(GETDATE() AS DATE)>=dtValidFrom AND CAST(GETDATE() AS DATE)<=dtValidTo THEN 1 ELSE 0 END) END))
		END
		PRINT @PromotionOffers
 
declare @strSql as varchar(MAX)
set @strSql=' With tblItem AS (                  
select I.numItemCode, vcItemName, txtItemDesc, charItemType, ISNULL(I.bitMatrix,0) bitMatrix,' + 
(CASE WHEN @tintPreLoginPriceLevel > 0 THEN 'ISNULL(PricingOption.monPrice,ISNULL(CASE WHEN I.[charItemType]=''P'' 
			THEN '+ convert(varchar(15),@UOMConversionFactor) + ' * W.[monWListPrice]  
			ELSE '+ convert(varchar(15),@UOMConversionFactor) + ' * monListPrice 
	   END,0))' ELSE 'ISNULL(CASE WHEN I.[charItemType]=''P'' 
			THEN '+ convert(varchar(15),@UOMConversionFactor) + ' * W.[monWListPrice]  
			ELSE '+ convert(varchar(15),@UOMConversionFactor) + ' * monListPrice 
	   END,0)' END)
+'
AS monListPrice,ISNULL(CASE WHEN I.[charItemType]=''P'' 
			THEN '+ convert(varchar(15),@UOMConversionFactor) + ' * W.[monWListPrice]  
			ELSE '+ convert(varchar(15),@UOMConversionFactor) + ' * monListPrice 
	   END,0) AS monMSRP,
numItemClassification, bitTaxable, vcSKU, bitKitParent,              
I.numModifiedBy, (SELECT numItemImageId,vcPathForImage,vcPathForTImage,bitDefault,case when bitdefault=1  then -1 else isnull(intDisplayOrder,0) end as intDisplayOrder FROM ItemImages  WHERE numItemCode=I.numItemCode and bitIsImage = 1 order by case when bitdefault=1 then -1 else isnull(intDisplayOrder,0) end  asc FOR XML AUTO,ROOT(''Images''))  vcImages,
(SELECT vcPathForImage FROM ItemImages  WHERE numItemCode=I.numItemCode and bitIsImage = 0 order by intDisplayOrder  asc FOR XML AUTO,ROOT(''Documents''))  vcDocuments ,
 isnull(bitSerialized,0) as bitSerialized, vcModelID,                 
numItemGroup,                   
(isnull(sum(numOnHand),0) / '+ convert(varchar(15),@UOMConversionFactor) + ') as numOnHand,                    
sum(numOnOrder) as numOnOrder,                    
sum(numReorder)  as numReorder,                    
sum(numAllocation)  as numAllocation,                    
sum(numBackOrder)  as numBackOrder,              
isnull(fltWeight,0) as fltWeight,              
isnull(fltHeight,0) as fltHeight,              
isnull(fltWidth,0) as fltWidth,              
isnull(fltLength,0) as fltLength,              
case when charItemType=''P''then isnull(bitFreeShipping,0) else 1 end as bitFreeShipping,
Case When (case when charItemType=''P''then isnull(bitFreeShipping,0) else 1 end )=1 then ''Free shipping'' else ''Calculated at checkout'' end as Shipping,              
isnull(bitAllowBackOrder,0) as bitAllowBackOrder,bitShowInStock,bitShowQOnHand,isnull(numWareHouseItemID,0)  as numWareHouseItemID,
(case when charItemType<>''S'' and charItemType<>''N'' then         
 (CASE 
	WHEN I.bitAllowBackOrder = 1 THEN 1
	WHEN (ISNULL(WAll.numTotalOnHand,0)<=0) THEN 0
	ELSE 1
END) ELSE 1 END ) as bitInStock,
(case when charItemType<>''S'' and charItemType<>''N'' then         
 (Case when '+ convert(varchar(15),@bitShowInStock) + '=1  then 
 (CASE 
	WHEN I.bitAllowBackOrder = 1 AND I.numDomainID <> 172 AND ISNULL(WAll.numTotalOnHand,0)<=0 AND ISNULL(I.numVendorID,0) > 0 AND dbo.GetVendorPreferredMethodLeadTime(I.numVendorID) > 0 THEN CONCAT(''<font class="vendorLeadTime" style="color:green">Usually ships in '',dbo.GetVendorPreferredMethodLeadTime(I.numVendorID),'' days</font>'')
	WHEN I.bitAllowBackOrder = 1 AND ISNULL(WAll.numTotalOnHand,0)<=0 THEN (CASE WHEN I.numDomainID = 172 THEN ''<font style="color:green">AVAILABLE</font>'' ELSE ''<font style="color:green">In Stock</font>'' END)
	WHEN (ISNULL(WAll.numTotalOnHand,0)<=0) THEN (CASE WHEN I.numDomainID = 172 THEN ''<font style="color:red">ON HOLD</font>'' ELSE ''<font style="color:red">Out Of Stock</font>'' END)
	ELSE (CASE WHEN I.numDomainID = 172 THEN ''<font style="color:green">AVAILABLE</font>'' ELSE ''<font style="color:green">In Stock</font>'' END)  
END) else '''' end) ELSE '''' END ) as InStock,
ISNULL(numSaleUnit,0) AS numUOM,
ISNULL(vcUnitName,'''') AS vcUOMName,
 '+ convert(varchar(15),@UOMConversionFactor) + ' AS UOMConversionFactor,
I.numCreatedBy,
I.numBarCodeId,
I.[vcManufacturer],
(SELECT  COUNT(*) FROM  Review where vcReferenceId = CONVERT(VARCHAR(50) ,I.numItemCode) and numDomainId = I.numDomainId and numSiteId = '+ CONVERT(VARCHAR(20) , @numSiteId)  +' AND bitHide = 0 ) as ReviewCount ,
(SELECT  COUNT(*) FROM  Ratings where vcReferenceId = CONVERT(VARCHAR(50) ,I.numItemCode) and numDomainId = I.numDomainId and numSiteId = '+ CONVERT(VARCHAR(20) , @numSiteId)  +'  ) as RatingCount ,
(select top 1 vcMetaKeywords  from MetaTags where numReferenceID = I.numItemCode and tintMetaTagFor =2) as vcMetaKeywords,
(select top 1 vcMetaDescription  from MetaTags where numReferenceID = I.numItemCode and tintMetaTagFor =2) as vcMetaDescription ,
(SELECT vcCategoryName  FROM  dbo.Category WHERE numCategoryID =(SELECT TOP 1 numCategoryID FROM dbo.ItemCategory WHERE numItemID = I.numItemCode)) as vcCategoryName,('+(SELECT CAST(ISNULL(@PromotionOffers,0) AS varchar(20)))+') as PromotionOffers

from Item I ' + 
(CASE WHEN @tintPreLoginPriceLevel > 0 THEN ' OUTER APPLY(SELECT decDiscount AS monPrice FROM PricingTable WHERE tintRuleType=3 AND numItemCode=I.numItemCode ORDER BY numPricingID OFFSET ' + CAST(@tintPreLoginPriceLevel-1 AS VARCHAR) + ' ROWS FETCH NEXT 1 ROWS ONLY) As PricingOption ' ELSE '' END)
+ '                 
left join  WareHouseItems W                
on W.numItemID=I.numItemCode and numWareHouseID= '+ convert(varchar(15),@numWareHouseID) + '
 OUTER APPLY (SELECT SUM(numOnHand) numTotalOnHand FROM WareHouseItems W 
													  WHERE  W.numItemID = I.numItemCode
                                                      AND numWareHouseID IN (SELECT [OutParam] FROM dbo.[SplitString](''' + @vcWarehouseIDs + ''','',''))
                                                      AND W.numDomainID = ' + CONVERT(VARCHAR(10),@numDomainID) + ') AS WAll
left join  eCommerceDTL E          
on E.numDomainID=I.numDomainID     
LEFT JOIN UOM u ON u.numUOMId=I.numSaleUnit    
where ISNULL(I.IsArchieve,0)=0 AND I.numDomainID='+ convert(varchar(15),@numDomainID) + ' AND I.numItemCode='+ convert(varchar(15),@numItemCode) +
CASE WHEN (SELECT COUNT(*) FROM [dbo].[EcommerceRelationshipProfile] WHERE numSiteID=@numSiteID AND numRelationship = @numDefaultRelationship AND numProfile = @numDefaultProfile) > 0
THEN
	CONCAT(' AND I.numItemClassification NOT IN (SELECT numItemClassification FROM EcommerceRelationshipProfile ERP INNER JOIN ECommerceItemClassification EIC ON EIC.numECommRelatiionshipProfileID=ERP.ID WHERE ERP.numSiteID=',@numSiteID,' AND ERP.numRelationship=',@numDefaultRelationship,' AND ERP.numProfile=', @numDefaultProfile,')')
ELSE
	''
END
+ '           
group by I.numItemCode, vcItemName, txtItemDesc, charItemType, monListPrice,numItemClassification, bitTaxable, vcSKU, bitKitParent, WAll.numTotalOnHand,         
I.numDomainID,I.numModifiedBy,  bitSerialized, vcModelID,numItemGroup, fltWeight,fltHeight,fltWidth,          
fltLength,bitFreeShipping,bitAllowBackOrder,bitShowInStock,bitShowQOnHand ,numWareHouseItemID,vcUnitName,I.numCreatedBy,I.numBarCodeId,W.[monWListPrice],I.[vcManufacturer],
numSaleUnit, I.numVendorID, I.bitMatrix ' + (CASE WHEN @tintPreLoginPriceLevel > 0 THEN ',PricingOption.monPrice' ELSE '' END) + ')'

set @strSql=@strSql+' select numItemCode,vcItemName,txtItemDesc,charItemType, bitMatrix, monListPrice,monMSRP,numItemClassification, bitTaxable, vcSKU, bitKitParent,                  
tblItem.numModifiedBy, vcImages,vcDocuments, bitSerialized, vcModelID, numItemGroup,numOnHand,numOnOrder,numReorder,numAllocation, 
numBackOrder, fltWeight, fltHeight, fltWidth, fltLength, bitFreeShipping,bitAllowBackOrder,bitShowInStock,
bitShowQOnHand,numWareHouseItemID,bitInStock,InStock,numUOM,vcUOMName,UOMConversionFactor,tblItem.numCreatedBy,numBarCodeId,vcManufacturer,ReviewCount,RatingCount,Shipping,isNull(vcMetaKeywords,'''') as vcMetaKeywords ,isNull(vcMetaDescription,'''') as vcMetaDescription,isNull(vcCategoryName,'''') as vcCategoryName,PromotionOffers'     

set @strSql=@strSql+ ' from tblItem ORDER BY numOnHand DESC'
PRINT @strSql
exec (@strSql)


declare @tintOrder as tinyint                                                  
declare @vcFormFieldName as varchar(50)                                                  
declare @vcListItemType as varchar(1)                                             
declare @vcAssociatedControlType varchar(10)                                                  
declare @numListID AS numeric(9)                                                  
declare @WhereCondition varchar(2000)                       
Declare @numFormFieldId as numeric  
DECLARE @vcFieldType CHAR(1)
DECLARE @GRP_ID INT
                  
set @tintOrder=0                                                  
set @WhereCondition =''                 
                   
              
  CREATE TABLE #tempAvailableFields(numFormFieldId  numeric(9),vcFormFieldName NVARCHAR(50),
        vcFieldType CHAR(1),vcAssociatedControlType NVARCHAR(50),numListID NUMERIC(18),
        vcListItemType CHAR(1),intRowNum INT,vcItemValue VARCHAR(3000), GRP_ID INT)
   
  INSERT INTO #tempAvailableFields (numFormFieldId,vcFormFieldName,vcFieldType,vcAssociatedControlType
        ,numListID,vcListItemType,intRowNum, GRP_ID)                         
            SELECT  Fld_id,REPLACE(Fld_label,' ','') AS vcFormFieldName,
                   CASE GRP_ID WHEN 4 then 'C' WHEN 1 THEN 'D' ELSE 'C' END AS vcFieldType,
                    fld_type as vcAssociatedControlType,
                    ISNULL(C.numListID, 0) numListID,
                    CASE WHEN C.numListID > 0 THEN 'L'
                         ELSE ''
                    END vcListItemType,ROW_NUMBER() OVER (ORDER BY Fld_id asc) AS intRowNum, GRP_ID
            FROM    CFW_Fld_Master C LEFT JOIN CFW_Validation V ON V.numFieldID = C.Fld_id
            WHERE   C.numDomainID = @numDomainId
                    AND GRP_ID IN (5,9)

     select top 1 @tintOrder=intRowNum,@numFormFieldId=numFormFieldId,@vcFormFieldName=vcFormFieldName,
				  @vcFieldType=vcFieldType,@vcAssociatedControlType=vcAssociatedControlType,@numListID=numListID,
				  @vcListItemType=vcListItemType,@GRP_ID=GRP_ID
	from #tempAvailableFields order by intRowNum ASC
   
	while @tintOrder>0                                                  
	begin                   
		IF @GRP_ID=9
		BEGIN
			IF @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'
			BEGIN  
				UPDATE 
					#tempAvailableFields 
				SET 
					vcItemValue=(SELECT TOP 1 Fld_Value FROM ItemAttributes WHERE Fld_Id=@numFormFieldId AND numItemCode=@numItemCode) 
				WHERE 
					numFormFieldId=@numFormFieldId              
			END   
			ELSE IF @vcAssociatedControlType = 'CheckBox'
			BEGIN      
				UPDATE 
					#tempAvailableFields 
				SET 
					vcItemValue=(SELECT CASE WHEN isnull(Fld_Value,0)=0 THEN 'No' WHEN ISNULL(Fld_Value,0)=1 then 'Yes' END FROM ItemAttributes WHERE Fld_Id=@numFormFieldId AND numItemCode=@numItemCode) 
				WHERE 
					numFormFieldId=@numFormFieldId              
			END                
			ELSE IF @vcAssociatedControlType = 'DateField'           
			BEGIN   
				UPDATE 
					#tempAvailableFields 
				SET 
					vcItemValue=(SELECT dbo.FormatedDateFromDate(Fld_Value,@numDomainId) FROM ItemAttributes WHERE Fld_Id=@numFormFieldId AND numItemCode=@numItemCode) 
				WHERE 
					numFormFieldId=@numFormFieldId              
			END                
			ELSE IF @vcAssociatedControlType = 'SelectBox'           
			BEGIN 
				UPDATE 
					#tempAvailableFields 
				SET 
					vcItemValue=(SELECT 
									L.vcData 
								FROM 
									ItemAttributes CFW 
								LEFT JOIN 
									ListDetails L
								ON 
									L.numListItemID=CFW.Fld_Value                
								WHERE 
									CFW.Fld_Id=@numFormFieldId AND CFW.numItemCode=@numItemCode) 
				WHERE 
					numFormFieldId=@numFormFieldId              
			END
		END
		ELSE
		BEGIN                           
			IF @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'
			BEGIN  
				UPDATE #tempAvailableFields SET vcItemValue=(SELECT Fld_Value FROM CFW_FLD_Values_Item WHERE Fld_Id=@numFormFieldId AND RecId=@numItemCode) WHERE numFormFieldId=@numFormFieldId              
			END   
			ELSE IF @vcAssociatedControlType = 'CheckBox'
			BEGIN      
				UPDATE #tempAvailableFields SET vcItemValue=(SELECT case when isnull(Fld_Value,0)=0 then 'No' when isnull(Fld_Value,0)=1 then 'Yes' END FROM CFW_FLD_Values_Item WHERE Fld_Id=@numFormFieldId AND RecId=@numItemCode) WHERE numFormFieldId=@numFormFieldId              
			END                
			ELSE IF @vcAssociatedControlType = 'DateField'           
			BEGIN   
				UPDATE #tempAvailableFields SET vcItemValue=(SELECT dbo.FormatedDateFromDate(Fld_Value,@numDomainId) FROM CFW_FLD_Values_Item WHERE Fld_Id=@numFormFieldId AND RecId=@numItemCode) WHERE numFormFieldId=@numFormFieldId              
			END                
			ELSE IF @vcAssociatedControlType = 'SelectBox'           
			BEGIN 
				UPDATE #tempAvailableFields SET vcItemValue=(SELECT L.vcData FROM CFW_FLD_Values_Item CFW left Join ListDetails L
					on L.numListItemID=CFW.Fld_Value                
					WHERE CFW.Fld_Id=@numFormFieldId AND CFW.RecId=@numItemCode) WHERE numFormFieldId=@numFormFieldId              
			END          
        END     
 
    select top 1 @tintOrder=intRowNum,@numFormFieldId=numFormFieldId,@vcFormFieldName=vcFormFieldName,
				  @vcFieldType=vcFieldType,@vcAssociatedControlType=vcAssociatedControlType,@numListID=numListID,
				  @vcListItemType=vcListItemType,@GRP_ID=GRP_ID
	from #tempAvailableFields WHERE intRowNum > @tintOrder order by intRowNum ASC
 
   if @@rowcount=0 set @tintOrder=0                                                  
end   


  
SELECT * FROM #tempAvailableFields

DROP TABLE #tempAvailableFields

--exec USP_ItemDetailsForEcomm @numItemCode=197611,@numWareHouseID=58,@numDomainID=1,@numSiteId=18
--exec USP_ItemDetailsForEcomm @numItemCode=735364,@numWareHouseID=1039,@numDomainID=156,@numSiteId=104
--SELECT * FROM Category
--- Alterd By Chintan Prajapati
-- EXEC dbo.USP_ItemsForECommerce 9,60,1,1,15,0,''
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ItemsForECommerce' ) 
    DROP PROCEDURE USP_ItemsForECommerce
GO
CREATE PROCEDURE [dbo].[USP_ItemsForECommerce]
    @numSiteID AS NUMERIC(9) = 0,
    @numCategoryID AS NUMERIC(18) = 0,
    @numWareHouseID AS NUMERIC(9) = 0,
    @SortBy NVARCHAR(MAX) = 0,
    @CurrentPage INT,
    @PageSize INT,
    @TotRecs INT = 0 OUTPUT,
    @SearchText AS NVARCHAR(MAX) = '',--Comma seperated item ids
    @FilterQuery AS NVARCHAR(MAX) = '',----Comma seperated Attributes Ids
    @FilterRegularCondition AS NVARCHAR(MAX),
    @FilterCustomFields AS NVARCHAR(MAX),
    @FilterCustomWhere AS NVARCHAR(MAX),
	@numDivisionID AS NUMERIC(18,0) = 0,
	@numManufacturerID AS NUMERIC(18,0)=0,
	@FilterItemAttributes AS NVARCHAR(MAX)
   
AS 
    BEGIN

		
		
		DECLARE @numDomainID AS NUMERIC(9)
		DECLARE @numDefaultRelationship AS NUMERIC(18,0)
		DECLARE @numDefaultProfile AS NUMERIC(18,0)
		DECLARE @tintPreLoginPriceLevel AS TINYINT
		DECLARE @tintPriceLevel AS TINYINT

		SELECT  @numDomainID = numDomainID
        FROM    [Sites]
        WHERE   numSiteID = @numSiteID
        
        DECLARE @tintDisplayCategory AS TINYINT
		
		IF ISNULL(@numDivisionID,0) > 0
		BEGIN
			SELECT 
				@numDefaultRelationship=ISNULL(tintCRMType,0)
				,@numDefaultProfile=ISNULL(vcProfile,0)
				,@tintPriceLevel=ISNULL(tintPriceLevel,0) 
			FROM 
				DivisionMaster
			INNER JOIN 
				CompanyInfo 
			ON 
				DivisionMaster.numCompanyID=CompanyInfo.numCompanyId 
			WHERE 
				numDivisionID=@numDivisionID 
				AND DivisionMaster.numDomainID=@numDomainID
		END

		SELECT 
			@tintDisplayCategory = ISNULL(bitDisplayCategory,0)
			,@numDefaultRelationship=(CASE WHEN ISNULL(@numDivisionID,0) > 0 THEN @numDefaultRelationship ELSE numRelationshipId END)
			,@numDefaultProfile=(CASE WHEN ISNULL(@numDivisionID,0) > 0 THEN @numDefaultProfile ELSE numProfileId END)
			,@tintPreLoginPriceLevel=(CASE WHEN @numDivisionID > 0 THEN (CASE WHEN ISNULL(bitShowPriceUsingPriceLevel,0)=1 THEN ISNULL(@tintPriceLevel,0) ELSE 0 END) ELSE ISNULL(tintPreLoginProceLevel,0) END)
		FROM 
			dbo.eCommerceDTL 
		WHERE 
			numSiteID = @numSiteId
		
		CREATE TABLE #tmpItemCat(numCategoryID NUMERIC(18,0))
		CREATE TABLE #tmpItemCode(numItemCode NUMERIC(18,0))
		IF LEN(@FilterCustomFields) > 0 AND LEN(@FilterCustomWhere) > 0
		BEGIN			
			DECLARE @FilterCustomFieldIds AS NVARCHAR(MAX)
			SET @FilterCustomFieldIds = Replace(@FilterCustomFields,'[','')
			SET @FilterCustomFieldIds = Replace(@FilterCustomFieldIds,']','')
			
			DECLARE @strCustomSql AS NVARCHAR(MAX)
			SET @strCustomSql = 'INSERT INTO #tmpItemCode(numItemCode)
								 SELECT DISTINCT T.RecId FROM 
															(
																SELECT * FROM 
																	(
																		SELECT  t2.RecId, t1.fld_id, t2.Fld_Value FROM CFW_Fld_Master t1 
																		JOIN CFW_FLD_Values_Item AS t2 ON t1.Fld_ID = t2.Fld_ID 
																		WHERE t1.Grp_id  In (5,9)
																		AND t1.numDomainID = ' + CONVERT(VARCHAR(10),@numDomainID) + '
																		AND t1.fld_type <> ''Link'' 
																		AND t1.fld_id IN (' + @FilterCustomFieldIds + ')
																	) p  PIVOT (MAX([Fld_Value]) FOR fld_id IN (' + @FilterCustomFields + ')) AS pvt 
															) T  WHERE 1=1 AND ' + @FilterCustomWhere
								 
			PRINT @strCustomSql
			EXEC SP_EXECUTESQL @strCustomSql								 					 
		END
		
        DECLARE @strSQL NVARCHAR(MAX)
        DECLARE @firstRec AS INTEGER
        DECLARE @lastRec AS INTEGER
        DECLARE @Where NVARCHAR(MAX)
        DECLARE @SortString NVARCHAR(MAX)
        DECLARE @searchPositionColumn NVARCHAR(MAX)
        DECLARE @row AS INTEGER
        DECLARE @data AS VARCHAR(100)
        DECLARE @dynamicFilterQuery NVARCHAR(MAX)
        DECLARE @checkFldType VARCHAR(100)
        DECLARE @count NUMERIC(9,0)
        DECLARE @whereNumItemCode NVARCHAR(MAX)
        DECLARE @filterByNumItemCode VARCHAR(100)
        
		SET @SortString = ''
        set @searchPositionColumn = ''
        SET @strSQL = ''
		SET @whereNumItemCode = ''
		SET @dynamicFilterQuery = ''
		 
        SET @firstRec = ( @CurrentPage - 1 ) * @PageSize
        SET @lastRec = ( @CurrentPage * @PageSize + 1 )
                
        DECLARE @bitAutoSelectWarehouse AS BIT
		SELECT @bitAutoSelectWarehouse = ISNULL([ECD].[bitAutoSelectWarehouse],0) FROM [dbo].[eCommerceDTL] AS ECD WHERE [ECD].[numSiteId] = @numSiteID
		AND [ECD].[numDomainId] = @numDomainID

		DECLARE @vcWarehouseIDs AS VARCHAR(1000)
        IF (@numWareHouseID = 0 OR @bitAutoSelectWarehouse = 1)
            BEGIN				
				IF @bitAutoSelectWarehouse = 1
					BEGIN
						SELECT @vcWarehouseIDs = ISNULL( STUFF((SELECT DISTINCT ',' + CAST(numWarehouseID AS VARCHAR(10)) FROM [dbo].[Warehouses] WHERE [Warehouses].[numDomainID] = @numDomainID FOR XML PATH('')),1, 1, '') , '') 
					END
				
				ELSE
					BEGIN
						SELECT  @numWareHouseID = ISNULL(numDefaultWareHouseID,0)
						FROM    [eCommerceDTL]
						WHERE   [numDomainID] = @numDomainID	

						SET @vcWarehouseIDs = @numWareHouseID
					END
            END
		ELSE
		BEGIN
			SET @vcWarehouseIDs = @numWareHouseID
		END

		PRINT @vcWarehouseIDs

		--PRIAMRY SORTING USING CART DROPDOWN
		DECLARE @Join AS NVARCHAR(MAX)
		DECLARE @SortFields AS NVARCHAR(MAX)
		DECLARE @vcSort AS VARCHAR(MAX)

		SET @Join = ''
		SET @SortFields = ''
		SET @vcSort = ''

		IF @SortBy = ''
			BEGIN
				SET @SortString = ' vcItemName Asc '
			END
		ELSE IF CHARINDEX('~0',@SortBy) > 0 OR CHARINDEX('~1',@SortBy) > 0
			BEGIN
				DECLARE @fldID AS NUMERIC(18,0)
				DECLARE @RowNumber AS VARCHAR(MAX)
				
				SELECT * INTO #fldValues FROM dbo.SplitIDsWithPosition(@SortBy,'~')
				SELECT @RowNumber = RowNumber, @fldID = id FROM #fldValues WHERE RowNumber = 1
				SELECT @vcSort = CASE WHEN Id = 0 THEN 'ASC' ELSE 'DESC' END FROM #fldValues WHERE RowNumber = 2
				
				SET @Join = @Join + ' LEFT JOIN CFW_FLD_Values_Item CFVI' + @RowNumber + ' ON I.numItemCode = CFVI' + @RowNumber + '.RecID AND CFVI' + @RowNumber + '.fld_Id = ' + CONVERT(VARCHAR(10),@fldID)
				---SET @SortString = 'CFVI' + @RowNumber + '.fld_Value' 
				SET @SortString = '[fld_Value' + @RowNumber + '] ' --+ @vcSort
				SET @SortFields = ', CFVI' + @RowNumber + '.fld_Value [fld_Value' + @RowNumber + ']'
			END
		ELSE
			BEGIN
				--SET @SortString = @SortBy	
				
				IF CHARINDEX('monListPrice',@SortBy) > 0
				BEGIN
					DECLARE @bitSortPriceMode AS BIT
					SELECT @bitSortPriceMode = ISNULL(bitSortPriceMode,0) FROM dbo.eCommerceDTL WHERE numSiteId = @numSiteID
					PRINT @bitSortPriceMode
					IF @bitSortPriceMode = 1
					BEGIN
						SET @SortString = ' '
					END
					ELSE
					BEGIN
						SET @SortString = @SortBy	
					END
				END
				ELSE
				BEGIN
					SET @SortString = @SortBy	
				END
				
			END	

		--SECONDARY SORTING USING DEFAULT SETTINGS FROM ECOMMERCE
		SELECT ROW_NUMBER() OVER (ORDER BY Custom,vcFieldName) AS [RowID],* INTO #tempSort FROM (
		SELECT  CONVERT(VARCHAR(9),numFieldID)+'~0' AS numFieldID,
				ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName,
				0 AS Custom,
				vcDbColumnName 
		FROM View_DynamicColumns
		WHERE numFormID = 84 
		AND ISNULL(bitSettingField,0) = 1 
		AND ISNULL(bitDeleted,0)=0 
		AND ISNULL(bitCustom,0) = 0
		AND numDomainID = @numDomainID
					       
		UNION     

		SELECT  CONVERT(VARCHAR(9),numFieldID)+'~1' AS numFieldID ,
				vcFieldName ,
				1 AS Custom,
				'' AS vcDbColumnName
		FROM View_DynamicCustomColumns          
		WHERE numFormID = 84
		AND numDomainID = @numDomainID
		AND grp_id IN (5,9)
		AND vcAssociatedControlType = 'TextBox' 
		AND ISNULL(bitCustom,0) = 1 
		AND tintPageType=1  
		) TABLE1 ORDER BY Custom,vcFieldName
		
		--SELECT * FROM #tempSort
		DECLARE @DefaultSort AS NVARCHAR(MAX)
		DECLARE @DefaultSortFields AS NVARCHAR(MAX)
		DECLARE @DefaultSortJoin AS NVARCHAR(MAX)
		DECLARE @intCnt AS INT
		DECLARE @intTotalCnt AS INT	
		DECLARE @strColumnName AS VARCHAR(100)
		DECLARE @bitCustom AS BIT
		DECLARE @FieldID AS VARCHAR(100)				

		SET @DefaultSort = ''
		SET @DefaultSortFields = ''
		SET @DefaultSortJoin = ''		
		SET @intCnt = 0
		SET @intTotalCnt = ( SELECT COUNT(*) FROM #tempSort )
		SET @strColumnName = ''
		
		IF (SELECT ISNULL(bitEnableSecSorting,0) FROM EcommerceDtl WHERE numSiteID = @numSiteID ) = 1
		BEGIN
			CREATE TABLE #fldDefaultValues(RowNumber INT,Id VARCHAR(1000))

			WHILE(@intCnt < @intTotalCnt)
			BEGIN
				SET @intCnt = @intCnt + 1
				SELECT @FieldID = numFieldID, @strColumnName = vcDbColumnName, @bitCustom = Custom FROM #tempSort WHERE RowID = @intCnt
				
				--PRINT @FieldID
				--PRINT @strColumnName
				--PRINT @bitCustom

				IF @bitCustom = 0
					BEGIN
						SET @DefaultSort =  @strColumnName + ',' + @DefaultSort 
					END

				ELSE
					BEGIN
						DECLARE @fldID1 AS NUMERIC(18,0)
						DECLARE @RowNumber1 AS VARCHAR(MAX)
						DECLARE @vcSort1 AS VARCHAR(10)
						DECLARE @vcSortBy AS VARCHAR(1000)
						SET @vcSortBy = CONVERT(VARCHAR(100),@FieldID) + '~' + '0'
						--PRINT @vcSortBy

						INSERT INTO #fldDefaultValues(RowNumber,Id) SELECT RowNumber, id FROM dbo.SplitIDsWithPosition(@vcSortBy,'~')
						SELECT @RowNumber1 = RowNumber + 100 + @intCnt, @fldID1 = id FROM #fldDefaultValues WHERE RowNumber = 1
						SELECT @vcSort1 = 'ASC' 
						
						SET @DefaultSortJoin = @DefaultSortJoin + ' LEFT JOIN CFW_FLD_Values_Item CFVI' + @RowNumber1 + ' ON I.numItemCode = CFVI' + @RowNumber1 + '.RecID AND CFVI' + @RowNumber1 + '.fld_Id = ' + CONVERT(VARCHAR(10),@fldID1)
						SET @DefaultSort = '[fld_Value' + @RowNumber1 + '] ' + ',' + @DefaultSort 
						SET @DefaultSortFields = @DefaultSortFields + ', CFVI' + @RowNumber1 + '.fld_Value [fld_Value' + @RowNumber1 + ']'
					END

			END
			IF LEN(@DefaultSort) > 0
			BEGIN
				SET  @DefaultSort = LEFT(@DefaultSort,LEN(@DefaultSort) - 1)
				SET @SortString = @SortString + ',' 
			END
			--PRINT @SortString
			--PRINT @DefaultSort
			--PRINT @DefaultSortFields
		END
--        IF @SortBy = 1 
--            SET @SortString = ' vcItemName Asc '
--        ELSE IF @SortBy = 2 
--            SET @SortString = ' vcItemName Desc '
--        ELSE IF @SortBy = 3 
--            SET @SortString = ' monListPrice Asc '
--        ELSE IF @SortBy = 4 
--            SET @SortString = ' monListPrice Desc '
--        ELSE IF @SortBy = 5 
--            SET @SortString = ' bintCreatedDate Desc '
--        ELSE IF @SortBy = 6 
--            SET @SortString = ' bintCreatedDate ASC '	
--        ELSE IF @SortBy = 7 --sort by relevance, invokes only when search
--            BEGIN
--                 IF LEN(@SearchText) > 0 
--		            BEGIN
--						SET @SortString = ' SearchOrder ASC ';
--					    set @searchPositionColumn = ' ,SearchItems.RowNumber AS SearchOrder ' 						
--					END
--			END
--        ELSE 
--            SET @SortString = ' vcItemName Asc '
		
                                		
        IF LEN(@SearchText) > 0
			BEGIN
				PRINT 1
				IF CHARINDEX('SearchOrder',@SortString) > 0
				BEGIN
					SET @searchPositionColumn = ' ,SearchItems.RowNumber AS SearchOrder ' 
				END

				SET @Where = ' INNER JOIN dbo.SplitIDsWithPosition('''+ @SearchText +''','','') AS SearchItems 
                        ON SearchItems.Id = I.numItemCode  and SC.numSiteID = ' + CONVERT(VARCHAR(15), @numSiteID) + ' WHERE ISNULL(IsArchieve,0) = 0'
				END

		ELSE IF @SearchText = ''

			SET @Where = ' WHERE 1=2 '
			+ ' and SC.numSiteID = ' + CONVERT(VARCHAR(15), @numSiteID)
			+ ' AND ISNULL(IsArchieve,0) = 0'
		ELSE
			BEGIN
				PRINT 2
				SET @Where = ' WHERE 1=1 '
							+ ' and SC.numSiteID = ' + CONVERT(VARCHAR(15), @numSiteID)
							+ ' AND ISNULL(IsArchieve,0) = 0'


				PRINT @tintDisplayCategory							
				PRINT @Where

				IF @tintDisplayCategory = 1
				BEGIN
					
					;WITH CTE AS(
					SELECT C.numCategoryID,C.tintLevel,C.numDepCategory,CASE WHEN tintLevel=1 THEN 1 ELSE 0 END AS bitChild 
						FROM Category C WHERE (numCategoryID = @numCategoryID OR ISNULL(@numCategoryID,0) = 0) AND numDomainID = @numDomainID
					UNION ALL
					SELECT  C.numCategoryID,C.tintLevel,C.numDepCategory,CTE.bitChild FROM Category C JOIN CTE ON 
					C.numDepCategory=CTE.numCategoryID AND CTE.bitChild=1 AND numDomainID = @numDomainID)
					
					INSERT INTO #tmpItemCat(numCategoryID)												 
					SELECT DISTINCT C.numCategoryID FROM CTE C JOIN dbo.ItemCategory IC ON C.numCategoryID = IC.numCategoryID 
														WHERE numItemID IN ( 
																						
													 						SELECT numItemID FROM WareHouseItems WI
																			INNER JOIN dbo.Item I ON I.numItemCode = WI.numItemID AND I.numDomainID = @numDomainID
																			GROUP BY numItemID 
																			HAVING SUM(WI.numOnHand) > 0 
																			UNION ALL
													 						SELECT numItemID FROM WareHouseItems WI
																			INNER JOIN dbo.Item I ON I.numItemCode = WI.numItemID AND I.numDomainID = @numDomainID
																			GROUP BY numItemID 
																			HAVING SUM(WI.numAllocation) > 0
																		 )		
					SELECT numItemID INTO #tmpOnHandItems FROM 
					(													 					
						SELECT numItemID FROM WareHouseItems WI 
						INNER JOIN dbo.Item I ON I.numItemCode = WI.numItemID AND I.numDomainID = @numDomainID
						GROUP BY numItemID 
						HAVING SUM(WI.numOnHand) > 0
						UNION ALL
						SELECT numItemID FROM WareHouseItems WI 
						INNER JOIN dbo.Item I ON I.numItemCode = WI.numItemID AND I.numDomainID = @numDomainID
						GROUP BY numItemID 
						HAVING SUM(WI.numAllocation) > 0				
					) TABLE1
				END

			END

		       SET @strSQL = @strSQL + ' WITH Items AS 
	                                     ( 
	                                     SELECT   
                                               I.numItemCode,
											   I.bitMatrix,
											   I.numItemGroup,
                                               ROW_NUMBER() OVER ( Partition by I.numItemCode Order by I.numItemCode) RowID,
                                               vcItemName,
                                               I.bintCreatedDate,
                                               vcManufacturer,
                                               C.vcCategoryName,
                                               ISNULL(txtItemDesc,'''') txtItemDesc,' + 
											   (CASE WHEN @tintPreLoginPriceLevel > 0 THEN 'ISNULL(PricingOption.monPrice,ISNULL(CASE WHEN I.[charItemType] = ''P'' THEN ( UOM * ISNULL(W1.[monWListPrice],0)) ELSE (UOM * monListPrice) END, 0))' ELSE 'ISNULL(CASE WHEN I.[charItemType] = ''P'' THEN ( UOM * ISNULL(W1.[monWListPrice],0)) ELSE (UOM * monListPrice) END, 0)' END)
											   +' AS monListPrice,
											   ISNULL(CASE WHEN I.[charItemType] = ''P'' THEN ( UOM * ISNULL(W1.[monWListPrice],0)) ELSE (UOM * monListPrice) END, 0) monMSRP,
                                               vcSKU,
                                               fltHeight,
                                               fltWidth,
                                               fltLength,
                                               IM.vcPathForImage,
                                               IM.vcPathForTImage,
                                               vcModelID,
                                               fltWeight,
                                               bitFreeShipping,
                                               UOM AS UOMConversionFactor,
											   UOMPurchase AS UOMPurchaseConversionFactor,
											   ( CASE WHEN charItemType <> ''S'' AND charItemType <> ''N''
													   THEN (CASE 
																WHEN bitAllowBackOrder = 1 THEN 1
																WHEN (ISNULL(W.numOnHand,0)<=0) THEN 0
																ELSE 1
															END)
													   ELSE 1
												  END ) AS bitInStock,
												( CASE WHEN charItemType <> ''S'' AND charItemType <> ''N''
													   THEN ( CASE WHEN bitShowInStock = 1
																   THEN (CASE 
																			WHEN bitAllowBackOrder = 1 AND I.numDomainID <> 172 AND ISNULL(W.numOnHand,0)<=0 AND ISNULL(I.numVendorID,0) > 0 AND dbo.GetVendorPreferredMethodLeadTime(I.numVendorID) > 0 THEN CONCAT(''<font class="vendorLeadTime" style="color:green">Usually ships in '',dbo.GetVendorPreferredMethodLeadTime(I.numVendorID),'' days</font>'')
																			WHEN bitAllowBackOrder = 1 AND ISNULL(W.numOnHand,0)<=0 THEN (CASE WHEN I.numDomainID = 172 THEN ''<font style="color:green">AVAILABLE</font>'' ELSE ''<font style="color:green">In Stock</font>'' END)
																			WHEN (ISNULL(W.numOnHand,0)<=0) THEN (CASE WHEN I.numDomainID = 172 THEN ''<font style="color:red">ON HOLD</font>'' ELSE ''<font style="color:red">Out Of Stock</font>'' END)
																			ELSE (CASE WHEN I.numDomainID = 172 THEN ''<font style="color:green">AVAILABLE</font>'' ELSE ''<font style="color:green">In Stock</font>'' END)  
																		END)
																   ELSE ''''
															  END )
													   ELSE ''''
												  END ) AS InStock, C.vcDescription as CategoryDesc, ISNULL(W1.numWareHouseItemID,0) [numWareHouseItemID] ' 
								               +	@searchPositionColumn + @SortFields + @DefaultSortFields + ',
											   (SELECT COUNT(numProId) 
													FROM 
														PromotionOffer PO
													WHERE 
														numDomainId='+CAST(@numDomainID AS VARCHAR(20))+' 
														AND ISNULL(bitEnabled,0)=1
														AND ISNULL(bitAppliesToSite,0)=1 
														AND ISNULL(bitRequireCouponCode,0)=0
														AND 1 = (CASE WHEN bitRequireCouponCode = 1 THEN CASE WHEN ISNULL(intCouponCodeUsed,0) < ISNULL(tintUsageLimit,0) OR ISNULL(tintUsageLimit,0) = 0  THEN 1 ELSE 0 END ELSE 1 END)
														AND 1 = (CASE WHEN bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN CAST(GETDATE() AS DATE)>=dtValidFrom AND CAST(GETDATE() AS DATE)<=dtValidTo THEN 1 ELSE 0 END) END)
														AND (1 = (CASE 
																	WHEN tintOfferBasedOn = 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 AND numValue IN (SELECT numOppAccAttrID FROM ItemGroupsDTL WHERE numItemGroupID IN (SELECT numItemGroupID FROM ItemGroups WHERE numProfileItem=I.numItemCode AND numDomainID='+CAST(@numDomainID AS VARCHAR(20))+' ) UNION SELECT I.numItemCode)) > 0 THEN 1 ELSE 0 END)
																	WHEN tintOfferBasedOn = 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=2 AND numValue=I.numItemClassification) > 0 THEN 1 ELSE 0 END)
																	ELSE 0
																END)
															OR
															1 = (CASE 
																	WHEN tintDiscoutBaseOn = 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=6 AND tintType=1 AND numValue IN (SELECT numOppAccAttrID FROM ItemGroupsDTL WHERE numItemGroupID IN (SELECT numItemGroupID FROM ItemGroups WHERE numProfileItem=I.numItemCode AND numDomainID='+CAST(@numDomainID AS VARCHAR(20))+' ) UNION SELECT I.numItemCode)) > 0 THEN 1 ELSE 0 END)
																	WHEN tintDiscoutBaseOn = 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=6 AND tintType=2 AND numValue=I.numItemClassification) > 0 THEN 1 ELSE 0 END)
																	WHEN tintDiscoutBaseOn = 3 AND tintOfferBasedOn = 1 THEN (CASE WHEN (SELECT COUNT(SimilarItems.numItemCode) FROM SimilarItems WHERE numParentItemCode IN (SELECT numValue FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 AND SimilarItems.numItemCode=I.numItemCode))>0 THEN 1 ELSE 0 END)
																	WHEN tintDiscoutBaseOn = 3 AND tintOfferBasedOn = 2 THEN (CASE WHEN (SELECT COUNT(SimilarItems.numItemCode) FROM SimilarItems WHERE SimilarItems.numItemCode IN(I.numItemCode) AND numParentItemCode IN (SELECT numItemCode FROM Item WHERE numItemClassification IN (SELECT numValue FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=2)))>0 THEN 1 ELSE 0 END)
																	ELSE 0
																END)
															)
													)  IsOnSale
								
                                         FROM      
                                                      Item AS I
                                         INNER JOIN   ItemCategory IC   ON   I.numItemCode = IC.numItemID
                                         LEFT JOIN    ItemImages IM     ON   I.numItemCode = IM.numItemCode AND IM.bitdefault=1 AND IM.bitIsImage = 1
                                         INNER JOIN   Category C        ON   IC.numCategoryID = C.numCategoryID
                                         INNER JOIN   SiteCategories SC ON   IC.numCategoryID = SC.numCategoryID ' + 
										 (CASE WHEN @tintPreLoginPriceLevel > 0 THEN ' OUTER APPLY(SELECT decDiscount AS monPrice FROM PricingTable WHERE tintRuleType=3 AND numItemCode=I.numItemCode ORDER BY numPricingID OFFSET ' + CAST(@tintPreLoginPriceLevel-1 AS VARCHAR) + ' ROWS FETCH NEXT 1 ROWS ONLY) As PricingOption ' ELSE '' END)
										 + ' INNER JOIN   eCommerceDTL E    ON   E.numDomainID = I.numDomainID AND E.numSiteId = SC.numSiteID ' + @Join + ' ' + @DefaultSortJoin + ' 
                                         OUTER APPLY (SELECT SUM(numOnHand) numOnHand FROM WareHouseItems W 
													  WHERE  W.numItemID = I.numItemCode
                                                      AND numWareHouseID IN (SELECT [OutParam] FROM dbo.[SplitString](''' + @vcWarehouseIDs + ''','',''))
                                                      AND W.numDomainID = ' + CONVERT(VARCHAR(10),@numDomainID) + ') AS W  
										 OUTER APPLY (SELECT TOP 1 numWareHouseItemID,monWListPrice FROM WareHouseItems W 
													  WHERE  W.numItemID = I.numItemCode 
													  AND W.numDomainID = ' + CONVERT(VARCHAR(10),@numDomainID) + '
													  AND numWareHouseID IN (SELECT [OutParam] FROM dbo.[SplitString](''' + @vcWarehouseIDs + ''','',''))) AS W1
										 OUTER APPLY (SELECT SUM(numAllocation) numAllocation FROM WareHouseItems W2 
													  WHERE  W2.numItemID = I.numItemCode
                                                      AND numWareHouseID IN (SELECT [OutParam] FROM dbo.[SplitString](''' + @vcWarehouseIDs + ''','',''))
                                                      AND W2.numDomainID = ' + CONVERT(VARCHAR(10),@numDomainID) + ') AS W2  			  
										 CROSS APPLY (SELECT dbo.fn_UOMConversion(ISNULL(I.numSaleUnit, 0),I.numItemCode, I.numDomainId,ISNULL(I.numBaseUnit, 0)) AS UOM) AS UOM
										 CROSS APPLY (SELECT dbo.fn_UOMConversion(ISNULL(I.numPurchaseUnit, 0),I.numItemCode, I.numDomainId,ISNULL(I.numBaseUnit, 0)) AS UOMPurchase) AS UOMPurchase '

			IF LEN(@FilterCustomFields) > 0 AND LEN(@FilterCustomWhere) > 0
			BEGIN
				SET @strSQL = @strSQL + ' INNER JOIN #tmpItemCode TP ON TP.numItemCode = I.numItemCode ' 
			END
			
			IF (SELECT COUNT(*) FROM #tmpItemCat) > 0
			BEGIN
				SET @strSQL = @strSQL + ' INNER JOIN #tmpItemCat TC ON TC.numCategoryID = IC.numCategoryID '
				SET @strSQL = @strSQL + ' LEFT JOIN #tmpOnHandItems TOIC ON TOIC.numItemID = I.numItemCode '
			END
			ELSE IF @numManufacturerID>0
			BEGIN
				SET @Where = REPLACE(@Where,'WHERE',' WHERE I.numManufacturer = ' + CONVERT(VARCHAR(10),@numManufacturerID) + ' AND ')
			END 
			ELSE IF @numCategoryID>0
			BEGIN
				SET @Where = REPLACE(@Where,'WHERE',' WHERE IC.numCategoryID = ' + CONVERT(VARCHAR(10),@numCategoryID) + ' AND ')
			END 
			
			IF @tintDisplayCategory = 1
			BEGIN
				SET @Where = @Where + ' AND 1 = (CASE WHEN (I.charItemType=''P'' AND ISNULL(I.bitKitParent,0) = 0) THEN CASE WHEN (ISNULL(W.numOnHand, 0) > 0 OR ISNULL(W2.numAllocation, 0) > 0) THEN 1 ELSE 0 END ELSE 1 END)'
			END
			
			IF (SELECT COUNT(*) FROM [dbo].[EcommerceRelationshipProfile] WHERE numSiteID=@numSiteID AND numRelationship = @numDefaultRelationship AND numProfile = @numDefaultProfile) > 0
			BEGIN
				SET @Where = @Where + CONCAT(' AND I.numItemClassification NOT IN (SELECT numItemClassification FROM EcommerceRelationshipProfile ERP INNER JOIN ECommerceItemClassification EIC ON EIC.numECommRelatiionshipProfileID=ERP.ID WHERE ERP.numSiteID=',@numSiteID,' AND ERP.numRelationship=',@numDefaultRelationship,' AND ERP.numProfile=', @numDefaultProfile,')')
			END

			IF LEN(ISNULL(@FilterItemAttributes,'')) > 0
			BEGIN
				SET @FilterItemAttributes = REPLACE(@FilterItemAttributes,') OR (',')) AND I.numItemCode IN (SELECT numItemCode FROM ItemAttributes WHERE numDomainID = ' + CAST(@numDomainId AS VARCHAR(18)) +' AND (')
				SET @Where = @Where + 'AND I.numItemCode IN (SELECT numItemCode FROM ItemAttributes WHERE numDomainID = ' + CAST(@numDomainId AS VARCHAR(18)) +' AND '+@FilterItemAttributes+')'
			END
			--PRINT @Where
			SET @strSQL = @strSQL + @Where
			PRINT @strSQL
			IF LEN(@FilterRegularCondition) > 0
			BEGIN
				SET @strSQL = @strSQL + @FilterRegularCondition
			END                                         
			
---PRINT @SortString + ',' + @DefaultSort + ' ' + @vcSort 
	
            IF LEN(@whereNumItemCode) > 0
                                        BEGIN
								              SET @strSQL = @strSQL + @filterByNumItemCode	
								    	END
            SET @strSQL = @strSQL +     '   ) SELECT * INTO #TEMPItems FROM Items
                                        
									DELETE FROM 
										#TEMPItems
									WHERE 
										numItemCode IN (
															SELECT 
																F.numItemCode
															FROM 
																#TEMPItems AS F
															WHERE 
																ISNULL(F.bitMatrix,0) = 1 
																AND EXISTS (
																			SELECT 
																				t1.vcItemName, t1.bitMatrix, t1.numItemGroup, Count(t1.numItemCode)
																			FROM 
																				#TEMPItems t1
																			WHERE 
																				t1.vcItemName = F.vcItemName
																				AND t1.bitMatrix = 1
																				AND t1.numItemGroup = F.numItemGroup
																			GROUP BY 
																				t1.vcItemName, t1.bitMatrix, t1.numItemGroup
																			HAVING 
																				Count(t1.numItemCode) > 1
																		)
														)
										AND numItemCode NOT IN (
																				SELECT 
																					Min(numItemCode)
																				FROM 
																					#TEMPItems AS F
																				WHERE
																					ISNULL(F.bitMatrix,0) = 1 
																					AND Exists (
																								SELECT 
																									t2.vcItemName, t2.bitMatrix, t2.numItemGroup, Count(t2.numItemCode)
																								FROM 
																									#TEMPItems t2
																								WHERE 
																									t2.vcItemName = F.vcItemName
																								   AND t2.bitMatrix = 1
																								   AND t2.numItemGroup = F.numItemGroup
																								GROUP BY 
																									t2.vcItemName, t2.bitMatrix, t2.numItemGroup
																								HAVING 
																									Count(t2.numItemCode) > 1
																							)
																				GROUP BY 
																					vcItemName, bitMatrix, numItemGroup
																			);  WITH ItemSorted AS (SELECT  ROW_NUMBER() OVER ( ORDER BY  ' + @SortString + @DefaultSort + ' ' + @vcSort + ' ) AS Rownumber,* FROM  #TEMPItems WHERE RowID = 1)'



            DECLARE @fldList AS NVARCHAR(MAX)
			DECLARE @fldList1 AS NVARCHAR(MAX)
			SELECT @fldList=COALESCE(@fldList + ',','') + QUOTENAME(REPLACE(Fld_label,' ','') + '_C') FROM dbo.CFW_Fld_Master WHERE numDomainID=@numDomainID AND Grp_id=5
			SELECT @fldList1=COALESCE(@fldList1 + ',','') + QUOTENAME(REPLACE(Fld_label,' ','') + '_' + CAST(Fld_Id AS VARCHAR)) FROM dbo.CFW_Fld_Master WHERE numDomainID=@numDomainID AND Grp_id=9
            
            SET @strSQL = @strSQL +' SELECT  Rownumber,numItemCode,bitMatrix,numItemGroup,txtItemDesc,vcManufacturer,monListPrice,monMSRP,vcItemName,vcCategoryName,vcPathForImage,
				vcpathForTImage,vcSKU,fltHeight,fltWidth,fltLength,vcModelID,fltWeight,bitFreeShipping,UOMConversionFactor,UOMPurchaseConversionFactor,bitInStock,InStock,CategoryDesc,
				numWareHouseItemID,IsOnSale
                                     INTO #tempItem FROM ItemSorted ' 
            
            SET @strSQL = @strSQL + 'SELECT * INTO #tmpPagedItems FROM #tempItem WHERE Rownumber > ' + CONVERT(VARCHAR(10), @firstRec) + '
                                      AND   Rownumber < ' + CONVERT(VARCHAR(10), @lastRec) + '
                                      order by Rownumber '
                                                                  
            IF LEN(ISNULL(@fldList,'')) > 0 OR LEN(ISNULL(@fldList1,'')) > 0
            BEGIN
				
				SET @strSQL = @strSQL +'SELECT  Rownumber,numItemCode,txtItemDesc,vcManufacturer,monListPrice,monMSRP,vcItemName,vcCategoryName,vcPathForImage,vcpathForTImage,vcSKU,fltHeight,fltWidth,fltLength,vcModelID,fltWeight,bitFreeShipping,UOMConversionFactor,UOMPurchaseConversionFactor,bitInStock,InStock,CategoryDesc,numWareHouseItemID,IsOnSale,' + @fldList + (CASE WHEN LEN(ISNULL(@fldList1,'')) > 0 THEN CONCAT(',',@fldList1) ELSE '' END) +'
                                        FROM #tmpPagedItems I'

				IF LEN(ISNULL(@fldList,'')) > 0
				BEGIN
					SET @strSQL = @strSQL +' left join (
														SELECT  t2.RecId, REPLACE(t1.Fld_label,'' '','''') + ''_C'' as Fld_label, 
															CASE t1.fld_type WHEN ''SelectBox'' THEN isnull(dbo.fn_GetListItemName(t2.Fld_Value),'''')
																				WHEN ''CheckBox'' THEN isnull(dbo.fn_getCustomDataValue(t2.Fld_Value,9),'''')
																				WHEN ''DateField'' THEN dbo.FormatedDateFromDate(t2.Fld_Value,' + CAST(@numDomainId AS VARCHAR(18)) +')
																				ELSE Replace(ISNULL(t2.Fld_Value,''0''),''$'',''$$'') end AS Fld_Value FROM CFW_Fld_Master t1 
														JOIN CFW_FLD_Values_Item AS t2 ON t1.Fld_ID = t2.Fld_ID where t1.Grp_id = 5 and t1.numDomainID=' + CAST(@numDomainId AS VARCHAR(18)) +' AND t1.fld_type <> ''Link''
														AND t2.RecId IN (SELECT numItemCode FROM #tmpPagedItems )
														) p PIVOT (MAX([Fld_Value]) FOR Fld_label IN (' + @fldList + ')) AS pvt on I.numItemCode=pvt.RecId' 
				END

				IF LEN(ISNULL(@fldList1,'')) > 0
				BEGIN
					
					SET @strSQL = @strSQL +' left join (
														SELECT  t2.numItemCode AS RecId, REPLACE(t1.Fld_label,'' '','''') + ''_'' + CAST(t1.Fld_Id AS VARCHAR) as Fld_label, 
															CASE t1.fld_type WHEN ''SelectBox'' THEN isnull(dbo.fn_GetListItemName(t2.Fld_Value),'''')
																				WHEN ''CheckBox'' THEN isnull(dbo.fn_getCustomDataValue(t2.Fld_Value,9),'''')
																				WHEN ''DateField'' THEN dbo.FormatedDateFromDate(t2.Fld_Value,' + CAST(@numDomainId AS VARCHAR(18)) +')
																				ELSE Replace(ISNULL(t2.Fld_Value,''0''),''$'',''$$'') end AS Fld_Value FROM CFW_Fld_Master t1 
														JOIN ItemAttributes AS t2 ON t1.Fld_ID = t2.Fld_ID where t1.Grp_id = 9 and t1.numDomainID=' + CAST(@numDomainId AS VARCHAR(18)) +' AND t1.fld_type <> ''Link''
														AND t2.numItemCode IN (SELECT numItemCode FROM #tmpPagedItems )
														) p PIVOT (MAX([Fld_Value]) FOR Fld_label IN (' + @fldList1 + ')) AS pvt1 on I.numItemCode=pvt1.RecId'
				END
				ELSE
				BEGIN
					SET @strSQL = REPLACE(@strSQL, ',##FLDLIST1##', '');
				END

				SET  @strSQL = @strSQL +' order by Rownumber';
			END 
			ELSE
			BEGIN
				PRINT 'flds2'
				SET @strSQL = @strSQL +' SELECT  Rownumber,numItemCode,txtItemDesc,vcManufacturer,monListPrice,monMSRP,vcItemName,vcCategoryName,vcPathForImage,vcpathForTImage,vcSKU,fltHeight,fltWidth,fltLength,vcModelID,fltWeight,bitFreeShipping,UOMConversionFactor,UOMPurchaseConversionFactor,InStock,CategoryDesc,numWareHouseItemID,IsOnSale
                                         FROM  #tmpPagedItems order by Rownumber;'
			END               
                                        
            --print('@strSQL=')
            --PRINT(@strSQL)        
--            EXEC ( @strSQL) ; 
			                           
            SET @strSQL = @strSQL + ' SELECT @TotRecs = COUNT(*) FROM #tempItem '                            

			 PRINT  LEN(@strSQL)
			 DECLARE @tmpSQL AS NVARCHAR(MAX)
			 SET @tmpSQL = @strSQL
			 --PRINT  @tmpSQL
			 PRINT CAST(@tmpSQL AS NTEXT)
             EXEC sp_executeSQL @strSQL, N'@TotRecs INT OUTPUT', @TotRecs OUTPUT
             
             
        DECLARE @tintOrder AS TINYINT                                                  
		DECLARE @vcFormFieldName AS VARCHAR(50)                                                  
		DECLARE @vcListItemType AS VARCHAR(1)                                             
		DECLARE @vcAssociatedControlType VARCHAR(10)                                                  
		DECLARE @numListID AS NUMERIC(9)                                                  
		DECLARE @WhereCondition VARCHAR(MAX)                       
		DECLARE @numFormFieldId AS NUMERIC  
		--DECLARE @vcFieldType CHAR(1)
		DECLARE @vcFieldType VARCHAR(15)
		                  
		SET @tintOrder=0                                                  
		SET @WhereCondition =''                 
                   
              
		CREATE TABLE #tempAvailableFields(numFormFieldId  NUMERIC(9),vcFormFieldName NVARCHAR(50),
		--vcFieldType CHAR(1),
		vcFieldType VARCHAR(15),vcAssociatedControlType NVARCHAR(50),numListID NUMERIC(18),
		vcListItemType CHAR(1),intRowNum INT,vcItemValue VARCHAR(3000))
		   
		INSERT INTO #tempAvailableFields (numFormFieldId,vcFormFieldName,vcFieldType,vcAssociatedControlType
				,numListID,vcListItemType,intRowNum)                         
		SELECT  Fld_id,REPLACE(Fld_label,' ','') AS vcFormFieldName,
			   CASE GRP_ID WHEN 4 then 'C' 
					WHEN 1 THEN 'D'
					--WHEN 9 THEN 'A' 
					WHEN 9 THEN CONVERT(VARCHAR(15), Fld_id)
					ELSE 'C' END AS vcFieldType,
				fld_type as vcAssociatedControlType,
				ISNULL(C.numListID, 0) numListID,
				CASE WHEN C.numListID > 0 THEN 'L'
					 ELSE ''
				END vcListItemType,ROW_NUMBER() OVER (ORDER BY Fld_id ASC) AS intRowNum
		FROM    CFW_Fld_Master C LEFT JOIN CFW_Validation V ON V.numFieldID = C.Fld_id
		WHERE   C.numDomainID = @numDomainId
				AND GRP_ID IN (5,9)

		  
		SELECT * FROM #tempAvailableFields
		
		/*
		DROP TABLE #tempAvailableFields
		DROP TABLE #tmpItemCode
		DROP TABLE #tmpItemCat
		*/	

		IF OBJECT_ID('tempdb..#tempAvailableFields') IS NOT NULL
		DROP TABLE #tempAvailableFields
		
		IF OBJECT_ID('tempdb..#tmpItemCode') IS NOT NULL
		DROP TABLE #tmpItemCode
		
		IF OBJECT_ID('tempdb..#tmpItemCat') IS NOT NULL
		DROP TABLE #tmpItemCat
		
		IF OBJECT_ID('tempdb..#fldValues') IS NOT NULL
		DROP TABLE #fldValues
		
		IF OBJECT_ID('tempdb..#tempSort') IS NOT NULL
		DROP TABLE #tempSort
		
		IF OBJECT_ID('tempdb..#fldDefaultValues') IS NOT NULL
		DROP TABLE #fldDefaultValues
		
		IF OBJECT_ID('tempdb..#tmpOnHandItems') IS NOT NULL
		DROP TABLE #tmpOnHandItems

		IF OBJECT_ID('tempdb..#tmpPagedItems') IS NOT NULL
		DROP TABLE #tmpPagedItems


		IF OBJECT_ID('tempdb..#TEMPItems') IS NOT NULL
		DROP TABLE #TEMPItems
    END


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Chintan
-- EXEC USP_Journal_EntryListForBankReconciliation 2233,'','',0,1
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Journal_DeleteOldBankReconciliationAdjustment')
DROP PROCEDURE USP_Journal_DeleteOldBankReconciliationAdjustment
GO
CREATE PROCEDURE [dbo].[USP_Journal_DeleteOldBankReconciliationAdjustment]
	@numDomainID NUMERIC(18,0)
	,@numReconcileID NUMERIC(18,0)
AS
BEGIN
	BEGIN TRY
	BEGIN TRANSACTION
		IF ISNULL(@numReconcileID,0) > 0
		BEGIN
			DELETE FROM General_Journal_Details WHERE numDomainId=@numDomainID AND numJournalId IN (SELECT ISNULL(numJournal_Id,0) FROM General_Journal_Header WHERE numReconcileID=@numReconcileID)
			DELETE FROM General_Journal_Header WHERE numDomainId=@numDomainID AND numReconcileID=@numReconcileID
		END
	COMMIT
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000)
		DECLARE @ErrorNumber INT
		DECLARE @ErrorSeverity INT
		DECLARE @ErrorState INT
		DECLARE @ErrorLine INT
		DECLARE @ErrorProcedure NVARCHAR(200)

		IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorNumber = ERROR_NUMBER(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE(),
			@ErrorLine = ERROR_LINE(),
			@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

		RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
	END CATCH
END
GO
/****** Object:  StoredProcedure [dbo].[USP_Journal_EntryListForBankReconciliation]    Script Date: 05/07/2009 22:01:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Chintan
-- EXEC USP_Journal_EntryListForBankReconciliation 2233,'','',0,1
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Journal_EntryListForBankReconciliation')
DROP PROCEDURE USP_Journal_EntryListForBankReconciliation
GO
CREATE PROCEDURE [dbo].[USP_Journal_EntryListForBankReconciliation]
               @numChartAcntId AS NUMERIC(9),
               @tintFlag       TINYINT=0,
               @numDomainId    AS NUMERIC(9),
               @bitDateHide AS BIT=0,
               @numReconcileID AS NUMERIC(9),
               @tintReconcile AS TINYINT=0 --0:not Reconcile 1:Reconsile for particular ReconID 2:All reconcile for particular ReconID and all non reconcile
AS
  BEGIN
  
  DECLARE @numCreditAmt NUMERIC(9)
  DECLARE @numDebitAmt NUMERIC(9)
  DECLARE @dtStatementDate AS DATETIME
  
  SELECT @dtStatementDate=dtStatementDate FROM BankReconcileMaster WHERE numReconcileID=@numReconcileID

  create table #TempBankRecon
  (numChartAcntId numeric(18),
  numTransactionId numeric(18),
  bitReconcile bit,
  EntryDate date,
  numDivisionID NUMERIC(18,0),
  CompanyName varchar(500),
  Deposit float,
  Payment float,
  Memo  varchar(250),
  TransactionType varchar(100),
  JournalId numeric(18),
  numCheckHeaderID numeric(18),
  vcReference varchar(1000),
  tintOppType INTEGER,bitCleared BIT,numCashCreditCardId NUMERIC(18),numOppId NUMERIC(18),numOppBizDocsId NUMERIC(18),
  numDepositId NUMERIC(18),numCategoryHDRID NUMERIC(9),tintTEType TINYINT,numCategory NUMERIC(18),numUserCntID NUMERIC(18),dtFromDate DATETIME,
  numBillId NUMERIC(18),numBillPaymentID NUMERIC(18),numLandedCostOppId NUMERIC(18),bitMatched BIT,vcCheckNo VARCHAR(20),tintType TINYINT,vcOrder VARCHAR(10)) /* tintType: 0=No Match, 3=Perfect Match, 2=only amount match with single bank statement row, 1 = only amount match with multiple bank statement row  */
  
  IF (@tintFlag = 1)
	SET @numDebitAmt = 0
  ELSE 	IF (@tintFlag = 2)
	SET @numCreditAmt = 0 
  
  insert into #TempBankRecon
  
    SELECT GJD.numChartAcntId AS numChartAcntId,
		   GJD.numTransactionId,
		   ISNULL(GJD.bitReconcile,0) bitReconcile,	
           GJH.datEntry_Date AS EntryDate,
		   ISNULL(DM.numDivisionID,0),
           CI.vcCompanyName AS CompanyName,
			(case when GJD.numCreditAmt=0 then GJD.numdebitAmt  end) as Deposit,        
			(case when GJD.numdebitAmt=0 then GJD.numCreditAmt end) as Payment,   
           --GJH.[datEntry_Date]  as EntryDate,
           GJD.varDescription as Memo,
            case when isnull(GJH.numCheckHeaderID,0) <> 0 THEN case (select isnull(OBD.numCheckNo,0) from OpportunityBizDocsPaymentDetails OBD where OBD.numBizDocsPaymentDetId=isnull(GJH.numBizDocsPaymentDetId,0))
					when 0 then 'Cash' else 'Checks' end 			
			when isnull(GJH.numCashCreditCardId,0) <> 0 And CCCD.bitMoneyOut=0 then 'Cash'
			when isnull(GJH.numCashCreditCardId,0) <> 0 And CCCD.bitMoneyOut=1 And CCCD.bitChargeBack=1  then 'Charge'
			when isnull(GJH.numOppId,0) <> 0 And isnull(GJH.numOppBizDocsId,0) <> 0 And isnull(GJH.numBizDocsPaymentDetId,0)=0 And Opp.tintOppType=1 then 'BizDocs Invoice'
			when isnull(GJH.numOppId,0) <> 0 And isnull(GJH.numOppBizDocsId,0) <> 0 And isnull(GJH.numBizDocsPaymentDetId,0)=0 And Opp.tintOppType=2 then 'BizDocs Purchase'
			when isnull(GJH.numDepositId,0) <> 0 then 'Deposit'
			when isnull(GJH.numBizDocsPaymentDetId,0)<>0 And Opp.tintOppType=1 then 'Receive Amt'
			when isnull(GJH.numBizDocsPaymentDetId,0)<>0 And Opp.tintOppType=2 then 'Vendor Amt'
			when isnull(GJH.numCategoryHDRID,0)<>0 then 'Time And Expenses'
			When ISNULL(GJH.numBillId,0) <>0 AND isnull(BH.bitLandedCost,0)=1 then 'Landed Cost' 
			When ISNULL(GJH.numBillId,0) <>0 then 'Bill' 
			When ISNULL(GJH.numBillPaymentID,0) <>0 then 'Pay Bill' 
			When GJH.numJournal_Id <>0 then 'Journal' End  as TransactionType,
			GJD.numJournalId AS JournalId,
			isnull(GJH.numCheckHeaderID,0) as numCheckHeaderID,
			ISNULL((SELECT Narration FROM VIEW_BIZPAYMENT WHERE numDomainID=GJH.numDomainID AND numBizDocsPaymentDetId = isnull(GJH.numBizDocsPaymentDetId,0)),GJH.varDescription) vcReference,
			isnull(Opp.tintOppType,0),ISNULL(GJD.bitCleared,0) AS bitCleared,
			ISNULL(GJH.numCashCreditCardId,0) AS numCashCreditCardId,ISNULL(GJH.numOppId,0) AS numOppId,
			isnull(GJH.numOppBizDocsId,0) AS numOppBizDocsId,isnull(GJH.numDepositId,0) AS numDepositId,
			isnull(GJH.numCategoryHDRID,0) AS numCategoryHDRID,TE.tintTEType,TE.numCategory,TE.numUserCntID,TE.dtFromDate,ISNULL(GJH.numBillId,0) AS numBillID,ISNULL(GJH.numBillPaymentID,0) AS numBillPaymentID,
			Case When isnull(BH.bitLandedCost,0)=1 then  isnull(BH.numOppId,0) else 0 end AS numLandedCostOppId,
			0 AS bitMatched
			,ISNULL(CAST(CheckHeader.numCheckNo AS VARCHAR),'')
			,0
			,''
    FROM   General_Journal_Header GJH
           INNER JOIN General_Journal_Details GJD
             ON GJH.numJournal_Id = GJD.numJournalId
           LEFT OUTER JOIN DivisionMaster AS DM
             ON GJD.numCustomerId = DM.numDivisionID
           LEFT OUTER JOIN CompanyInfo AS CI
             ON DM.numCompanyID = CI.numCompanyId
            Left outer join CashCreditCardDetails CCCD on GJH.numCashCreditCardId=CCCD.numCashCreditId        
			Left outer join (select * from OpportunityMaster WHERE tintOppType=1 ) Opp on GJH.numOppId=Opp.numOppId    
			LEFT OUTER JOIN TimeAndExpense TE ON GJH.numCategoryHDRID = TE.numCategoryHDRID
			LEFT OUTER JOIN BillHeader BH  ON ISNULL(GJH.numBillId,0) = BH.numBillId
			LEFT OUTER JOIN CheckHeader ON GJH.numCheckHeaderID = CheckHeader.numCheckHeaderID
    WHERE  
		   ((GJH.[datEntry_Date] <= @dtStatementDate AND @bitDateHide=1) OR @bitDateHide=0)
           AND GJD.numChartAcntId = @numChartAcntId
           AND GJD.numDomainId = @numDomainId
           AND (GJD.numCreditAmt <> @numCreditAmt OR @numCreditAmt IS null)
           AND (GJD.numDebitAmt <> @numDebitAmt OR @numDebitAmt IS null)
		   AND ISNULL(GJH.numReconcileID,0) = 0
           AND 1=(CASE WHEN @tintReconcile=0 THEN CASE WHEN ISNULL(GJD.bitReconcile,0)=0 THEN 1 ELSE 0 END
					   WHEN @tintReconcile=1 THEN CASE WHEN ISNULL(GJD.numReconcileID,0)=@numReconcileID THEN 1 ELSE 0 END
					   WHEN @tintReconcile=2 THEN CASE WHEN  (ISNULL(GJD.bitReconcile,0)=1 AND ISNULL(GJD.numReconcileID,0)=@numReconcileID) OR ISNULL(GJD.bitReconcile,0)=0 THEN 1 ELSE 0 END END)
			ORDER BY GJH.datEntry_Date

	SELECT
		B1.ID,
		dbo.FormatedDateFromDate(B1.dtEntryDate,@numDomainID) as dtEntryDate,
		ISNULL(vcReference,'') as [vcReference],
		[fltAmount],
		ISNULL(vcPayee,'') as vcPayee,
		ISNULL(vcDescription,'') as vcDescription,
		ISNULL(B1.bitCleared,0) bitCleared,
		ISNULL(B1.bitReconcile,0) bitReconcile,
		0 AS numBizTransactionId,
		0 AS bitMatched,
		0 AS bitDuplicate,
		0 AS tintType
	INTO 
		#TempStatememnt
	FROM
		BankReconcileFileData B1
	WHERE
		numReconcileID=@numReconcileID 
		AND ISNULL(B1.bitReconcile,0) = 0

	IF EXISTS (SELECT * FROM BankReconcileMatchRule WHERE numDomainID = @numDomainId AND @numChartAcntId IN (SELECT Id FROM dbo.SplitIDs(vcBankAccounts,',')))
	BEGIN
		DECLARE @TEMP TABLE
		(
			ID INT IDENTITY(1,1)
			,numRuleID NUMERIC(18,0)
		)

		INSERT INTO @TEMP
		(
			numRuleID
		)
		SELECT
			ID
		FROM
			BankReconcileMatchRule
		WHERE
			numDomainID = @numDomainId
			AND @numChartAcntId IN (SELECT Id FROM dbo.SplitIDs(vcBankAccounts,','))
		ORDER BY
			tintOrder


		DECLARE @i AS INT = 1
		DECLARE @numRuleID AS NUMERIC(18,0)
		DECLARE @bitMatchAllConditions AS BIT
		DECLARE @Count AS INT 
		SELECT @Count = COUNT(*) FROM @TEMP

		WHILE @i <= @Count
		BEGIN
			SELECT @numRuleID=numRuleID,@bitMatchAllConditions=bitMatchAllConditions FROM @TEMP T1 INNER JOIN BankReconcileMatchRule BRMR ON T1.numRuleID=BRMR.ID WHERE T1.ID = @i 

			UPDATE
				TS
			SET 
				numBizTransactionId = (CASE WHEN ISNULL(TEMP1.numTransactionId ,0) > 0 THEN TEMP1.numTransactionId ELSE 0 END),
				bitMatched = (CASE WHEN ISNULL(TEMP1.numTransactionId ,0) > 0 THEN 1 ELSE 0 END),
				bitDuplicate = (CASE WHEN ISNULL(TEMP2.ID ,0) > 0 THEN 1 ELSE 0 END),
				tintType = (CASE WHEN ISNULL(TEMP1.numTransactionId ,0) > 0 THEN 3 ELSE 0 END)
			FROM
				#TempStatememnt TS
			INNER JOIN
				BankReconcileFileData B1
			ON
				TS.ID = B1.ID
				AND ISNULL(TS.numBizTransactionId,0) = 0
				AND ISNULL(TS.bitMatched,0) = 0
			OUTER APPLY
			(
				SELECT TOP 1
					t1.numTransactionId
				FROM
					#TempBankRecon t1
				OUTER APPLY
				(
					SELECT 
						(CASE 
							WHEN @bitMatchAllConditions = 1
							THEN
								(SELECT
									(CASE WHEN COUNT(*) > 0 THEN 0 ELSE 1 END)
								FROM
								(
									SELECT
										numConditionID
										,(CASE tintConditionOperator
												WHEN 1 --contains
												THEN (CASE WHEN LOWER((CASE tintColumn WHEN 1 THEN B1.vcPayee WHEN 2 THEN B1.vcDescription WHEN 3 THEN B1.vcReference END)) LIKE '%' + LOWER(BRMRC.vcTextToMatch) + '%' AND BRMRC.numDivisionID = t1.numDivisionID THEN 1 ELSE 0 END)
												WHEN 2 --equals 
												THEN (CASE WHEN (CASE tintColumn WHEN 1 THEN B1.vcPayee WHEN 2 THEN B1.vcDescription WHEN 3 THEN B1.vcReference END) = BRMRC.vcTextToMatch AND BRMRC.numDivisionID = t1.numDivisionID THEN 1 ELSE 0 END)
												WHEN 3 --starts with
												THEN (CASE WHEN (CASE tintColumn WHEN 1 THEN B1.vcPayee WHEN 2 THEN B1.vcDescription WHEN 3 THEN B1.vcReference END) LIKE BRMRC.vcTextToMatch + '%' AND BRMRC.numDivisionID = t1.numDivisionID THEN 1 ELSE 0 END)
												WHEN 4 --ends with
												THEN (CASE WHEN (CASE tintColumn WHEN 1 THEN B1.vcPayee WHEN 2 THEN B1.vcDescription WHEN 3 THEN B1.vcReference END) LIKE '%' + BRMRC.vcTextToMatch AND BRMRC.numDivisionID = t1.numDivisionID THEN 1 ELSE 0 END)
												ELSE 
													0
											END) AS isMatched
									FROM
										BankReconcileMatchRuleCondition BRMRC
									WHERE
										numRuleID=@numRuleID
								) T3
								WHERE
									T3.isMatched = 0)
							ELSE
								(SELECT
									(CASE WHEN COUNT(*) > 0 THEN 1 ELSE 0 END)
								FROM
								(
									SELECT
										numConditionID
										,(CASE tintConditionOperator
												WHEN 1 --contains
												THEN (CASE WHEN (CASE tintColumn WHEN 1 THEN B1.vcPayee WHEN 2 THEN B1.vcDescription WHEN 3 THEN B1.vcReference END) LIKE '%' + BRMRC.vcTextToMatch + '%' AND BRMRC.numDivisionID = t1.numDivisionID THEN 1 ELSE 0 END)
												WHEN 2 --equals 
												THEN (CASE WHEN (CASE tintColumn WHEN 1 THEN B1.vcPayee WHEN 2 THEN B1.vcDescription WHEN 3 THEN B1.vcReference END) = BRMRC.vcTextToMatch AND BRMRC.numDivisionID = t1.numDivisionID THEN 1 ELSE 0 END)
												WHEN 3 --starts with
												THEN (CASE WHEN (CASE tintColumn WHEN 1 THEN B1.vcPayee WHEN 2 THEN B1.vcDescription WHEN 3 THEN B1.vcReference END) LIKE BRMRC.vcTextToMatch + '%' AND BRMRC.numDivisionID = t1.numDivisionID THEN 1 ELSE 0 END)
												WHEN 4 --ends with
												THEN (CASE WHEN (CASE tintColumn WHEN 1 THEN B1.vcPayee WHEN 2 THEN B1.vcDescription WHEN 3 THEN B1.vcReference END) LIKE '%' + BRMRC.vcTextToMatch AND BRMRC.numDivisionID = t1.numDivisionID THEN 1 ELSE 0 END)
												ELSE 
													0
											END) AS isMatched
									FROM
										BankReconcileMatchRuleCondition BRMRC
									WHERE
										numRuleID=@numRuleID
								) T4
								WHERE
									T4.isMatched = 1)
						END) AS bitMatched
				) TEMPConditions
				WHERE
					t1.EntryDate = B1.dtEntryDate
					AND TEMPConditions.bitMatched = 1
					AND (t1.Deposit = B1.fltAmount OR t1.Payment*-1=B1.fltAmount)
					AND ISNULL(t1.bitMatched,0) = 0
			) AS TEMP1
			OUTER APPLY
			(
				SELECT TOP 1
					t1.ID
				FROM
					BankReconcileFileData t1
				WHERE
					t1.numReconcileID <> @numReconcileID
					AND t1.numDomainID=@numDomainId
					AND (ISNULL(t1.bitReconcile,0) = 1 OR ISNULL(t1.bitCleared,0) = 1)
					AND t1.dtEntryDate = B1.dtEntryDate
					AND B1.vcPayee = t1.vcPayee
					AND B1.fltAmount = t1.fltAmount
					AND B1.vcReference = t1.vcReference
					AND B1.vcDescription=t1.vcDescription
			) AS TEMP2
			WHERE
				numReconcileID=@numReconcileID
				

			UPDATE 
				t2
			SET
				bitMatched = 1,
				tintType = 3,
				vcOrder=CONCAT('D',t1.ID)
			FROM
				#TempBankRecon t2
			INNER JOIN
				#TempStatememnt t1
			ON
				t2.numTransactionId=ISNULL(t1.numBizTransactionId,0)
			WHERE 
				ISNULL(t2.bitMatched,0)=0 

			SET @i = @i + 1
		END
	END

	UPDATE
		TS
	SET 
		numBizTransactionId = (CASE WHEN ISNULL(TEMP1.numTransactionId ,0) > 0 THEN TEMP1.numTransactionId ELSE 0 END),
		bitMatched = (CASE WHEN ISNULL(TEMP1.numTransactionId ,0) > 0 THEN 1 ELSE 0 END),
		bitDuplicate = (CASE WHEN ISNULL(TEMP2.ID ,0) > 0 THEN 1 ELSE 0 END),
		tintType = (CASE WHEN ISNULL(TEMP1.numTransactionId ,0) > 0 THEN 3 ELSE 0 END)
	FROM
		#TempStatememnt TS
	INNER JOIN
		BankReconcileFileData B1
	ON
		TS.ID = B1.ID
		AND ISNULL(TS.numBizTransactionId,0) = 0
		AND ISNULL(TS.bitMatched,0) = 0
	OUTER APPLY
	(
		SELECT TOP 1
			t1.numTransactionId
		FROM
			#TempBankRecon t1
		WHERE
			t1.EntryDate = B1.dtEntryDate
			AND (CHARINDEX(UPPER(t1.CompanyName),UPPER(B1.vcPayee)) > 0 OR CHARINDEX(UPPER(t1.CompanyName),UPPER(B1.vcDescription)) > 0 OR (CHARINDEX('CHECK',UPPER(B1.vcDescription)) > 0 AND LEN(t1.vcCheckNo) > 0 AND CHARINDEX(UPPER(t1.vcCheckNo),UPPER(B1.vcDescription)) > 0))
			AND (t1.Deposit = B1.fltAmount OR t1.Payment*-1=B1.fltAmount)
	) AS TEMP1
	OUTER APPLY
	(
		SELECT TOP 1
			t1.ID
		FROM
			BankReconcileFileData t1
		WHERE
			t1.numReconcileID <> @numReconcileID
			AND t1.numDomainID=@numDomainId
			AND (ISNULL(t1.bitReconcile,0) = 1 OR ISNULL(t1.bitCleared,0) = 1)
			AND t1.dtEntryDate = B1.dtEntryDate
			AND B1.vcPayee = t1.vcPayee
			AND B1.fltAmount = t1.fltAmount
			AND B1.vcReference = t1.vcReference
			AND B1.vcDescription=t1.vcDescription
	) AS TEMP2
	WHERE
		numReconcileID=@numReconcileID 
		AND ISNULL(Ts.bitMatched,0) = 0

	UPDATE 
		t2
	SET
		bitMatched = 1
		,tintType=t1.tintType
		,vcOrder=CONCAT('D',t1.ID)
	FROM
		#TempBankRecon t2
	INNER JOIN
		#TempStatememnt t1
	ON
		t2.numTransactionId = t1.numBizTransactionId
	WHERE
		ISNULL(t2.bitMatched,0) = 0

	------------------------------- AMOUNT MATCH ONLY SINGAL JOURNAL ENTRY IN BIZ ------------------------------------------
	UPDATE
		#TempStatememnt 
	SET
		bitMatched = 1
		,tintType = 2
		,numBizTransactionId = (SELECT numTransactionId FROM #TempBankRecon WHERE ISNULL(bitMatched,0) = 0 AND (Deposit = #TempStatememnt.fltAmount OR Payment*-1=#TempStatememnt.fltAmount))
	WHERE
		ISNULL(bitMatched,0) = 0
		AND (SELECT COUNT(*) FROM #TempBankRecon WHERE ISNULL(bitMatched,0) = 0 AND (Deposit = #TempStatememnt .fltAmount OR Payment*-1=#TempStatememnt .fltAmount)) = 1

	UPDATE 
		t2
	SET
		bitMatched = 1
		,tintType=t1.tintType
		,vcOrder=CONCAT('C',t1.ID)
	FROM
		#TempBankRecon t2
	INNER JOIN
		#TempStatememnt t1
	ON
		t2.numTransactionId = t1.numBizTransactionId
	WHERE
		ISNULL(t2.bitMatched,0) = 0

	------------------------------- AMOUNT MATCH ONLY MULTIPLE JOURNAL ENTRY IN BIZ ------------------------------------------
	UPDATE
		#TempStatememnt 
	SET
		tintType = 1
	WHERE
		ISNULL(bitMatched,0) = 0
		AND (SELECT COUNT(*) FROM #TempBankRecon WHERE ISNULL(bitMatched,0) = 0 AND (Deposit = #TempStatememnt.fltAmount OR Payment*-1=#TempStatememnt.fltAmount)) > 1

	UPDATE 
		t2
	SET 
		tintType=1
		,vcOrder=CONCAT('B',t1.ID)
	FROM
		#TempBankRecon t2
	INNER JOIN
		#TempStatememnt t1
	ON
		ISNULL(t1.tintType,0)=1
		AND (t2.Deposit = t1.fltAmount OR t2.Payment*-1=t1.fltAmount)
	WHERE
		ISNULL(t2.bitMatched,0) = 0

	 
	SELECT * FROM #TempBankRecon ORDER BY tintType DESC, vcOrder ASC,EntryDate ASC
	SELECT * FROM #TempStatememnt WHERE ISNULL(bitDuplicate,0)=0 ORDER BY tintType DESC, ID

	SELECT 
		(SELECT COUNT(*) FROM #TempStatememnt) AS TotalRecords
		,(SELECT COUNT(*) FROM #TempStatememnt WHERE bitDuplicate=1) AS DuplicateRecords
		,(SELECT COUNT(*) FROM #TempStatememnt WHERE bitMatched=1) AS MatchedRecords

	drop table #TempBankRecon    
	drop table #TempStatememnt       
  END
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageBankReconcileMaster')
DROP PROCEDURE USP_ManageBankReconcileMaster
GO
CREATE PROCEDURE USP_ManageBankReconcileMaster
	@tintMode AS TINYINT, 
    @numDomainID numeric(18, 0),
    @numReconcileID numeric(18, 0),
    @numCreatedBy numeric(18, 0),
    @dtStatementDate datetime,
    @monBeginBalance DECIMAL(20,5),
    @monEndBalance DECIMAL(20,5),
    @numChartAcntId numeric(18, 0),
    @monServiceChargeAmount DECIMAL(20,5),
    @numServiceChargeChartAcntId numeric(18, 0),
    @dtServiceChargeDate datetime,
    @monInterestEarnedAmount DECIMAL(20,5),
    @numInterestEarnedChartAcntId numeric(18, 0),
    @dtInterestEarnedDate DATETIME,
    @bitReconcileComplete BIT=0,
	@vcFileName VARCHAR(300)='',
	@vcFileData VARCHAR(MAX) = ''
AS

--Validation of closed financial year
EXEC dbo.USP_ValidateFinancialYearClosingDate @numDomainID,@dtStatementDate
	
	IF @tintMode=1 --SELECT
	BEGIN
		DECLARE @numCreditAmt AS DECIMAL(20,5),@numDebitAmt AS DECIMAL(20,5) 
		
			SELECT @numCreditAmt=ISNULL(SUM(numCreditAmt),0),@numDebitAmt=ISNULL(SUM(numDebitAmt),0) 
				FROM General_Journal_Details WHERE numDomainID=@numDomainID AND [numReconcileID] = @numReconcileID 
				AND numChartAcntId=(SELECT numChartAcntId FROM [BankReconcileMaster] WHERE  numDomainID=@numDomainID AND [numReconcileID] = @numReconcileID)
				AND numJournalId NOT IN (SELECT numJournal_Id FROM General_Journal_Header WHERE numDomainId=@numDomainID AND numReconcileID=@numReconcileID)
				--AND ISNULL(bitCleared,0)=1
	
			SELECT [numReconcileID],[numDomainID],[numCreatedBy],[dtCreatedDate],[dtStatementDate],[monBeginBalance]
      ,[monEndBalance],[numChartAcntId],[monServiceChargeAmount],[numServiceChargeChartAcntId],[dtServiceChargeDate]
      ,[monInterestEarnedAmount],[numInterestEarnedChartAcntId],[dtInterestEarnedDate],[bitReconcileComplete]
      ,[dtReconcileDate],@numCreditAmt AS Payment,@numDebitAmt AS Deposit,
			dbo.fn_GetContactName(ISNULL(numCreatedBy,0)) AS vcReconciledby,
			dbo.fn_GetChart_Of_AccountsName(ISNULL(numChartAcntId,0)) AS vcAccountName,ISNULL(vcFileName,'') AS vcFileName
			FROM   [BankReconcileMaster] WHERE  numDomainID=@numDomainID AND [numReconcileID] = @numReconcileID
	END
	ELSE IF @tintMode=2 --Insert/Update
	BEGIN
		IF @numReconcileID>0 --Update
			BEGIN
				UPDATE [BankReconcileMaster]
					SET    [dtStatementDate] = @dtStatementDate, [monBeginBalance]=@monBeginBalance, [monEndBalance]=@monEndBalance, [numChartAcntId] = @numChartAcntId, [monServiceChargeAmount] = @monServiceChargeAmount, [numServiceChargeChartAcntId] = @numServiceChargeChartAcntId,dtServiceChargeDate=@dtServiceChargeDate, [monInterestEarnedAmount] = @monInterestEarnedAmount, [numInterestEarnedChartAcntId] = @numInterestEarnedChartAcntId,dtInterestEarnedDate=@dtInterestEarnedDate
					WHERE  [numDomainID] = @numDomainID AND [numReconcileID] = @numReconcileID
			END
		ELSE --Insert
			BEGIN
				INSERT INTO [BankReconcileMaster] ([numDomainID], [numCreatedBy], [dtCreatedDate], [dtStatementDate], [monBeginBalance], [monEndBalance], [numChartAcntId], [monServiceChargeAmount], [numServiceChargeChartAcntId],dtServiceChargeDate,[monInterestEarnedAmount], [numInterestEarnedChartAcntId],dtInterestEarnedDate,vcFileName)
				SELECT @numDomainID, @numCreatedBy, GETUTCDATE(), @dtStatementDate, @monBeginBalance, @monEndBalance, @numChartAcntId, @monServiceChargeAmount, @numServiceChargeChartAcntId,@dtServiceChargeDate,@monInterestEarnedAmount, @numInterestEarnedChartAcntId,@dtInterestEarnedDate,@vcFileName
			
				SET @numReconcileID=SCOPE_IDENTITY()


				IF LEN(@vcFileName) > 0
				BEGIN
					DECLARE @hDocItem int
					EXEC sp_xml_preparedocument @hDocItem OUTPUT, @vcFileData

					INSERT INTO BankReconcileFileData
					(
						[numDOmainID],
						[numReconcileID],
						[dtEntryDate],
						[vcReference],
						[fltAmount],
						[vcPayee],
						[vcDescription]
					)
					SELECT 
						@numDomainID,
						@numReconcileID,
						dtEntryDate,
						vcReference,
						fltAmount,
						vcPayee,
						vcDescription
					FROM 
						OPENXML (@hDocItem,'/BankStatement/Trasactions',2)                                                                          
					WITH                       
					(                                                                          
						dtEntryDate Date, vcReference VARCHAR(500), fltAmount FLOAT, vcPayee VARCHAR(1000),vcDescription VARCHAR(1000)
					)
				END
			END	
			
			SELECT [numReconcileID], [numDomainID], [numCreatedBy], [dtCreatedDate], [dtStatementDate], [monBeginBalance], [monEndBalance], [numChartAcntId], [monServiceChargeAmount], [numServiceChargeChartAcntId], [monInterestEarnedAmount], [numInterestEarnedChartAcntId] 
			FROM   [BankReconcileMaster] WHERE  numDomainID=@numDomainID AND [numReconcileID] = @numReconcileID
	   END
	 ELSE IF @tintMode=3 --Delete
	 BEGIN
	    --Delete Service Charge,Interest Earned and Adjustment entry
	    DELETE FROM General_Journal_Details WHERE numJournalId IN (SELECT numJournal_Id FROM General_Journal_Header WHERE numDomainID=@numDomainID AND ISNULL(numReconcileID,0) = @numReconcileID)
	    DELETE FROM General_Journal_Header WHERE numDomainID=@numDomainID AND ISNULL(numReconcileID,0) = @numReconcileID
	 
	    --Set Journal_Details entry to bitReconcile=0 & bitCleared=0
        UPDATE dbo.General_Journal_Details SET bitReconcile=0,bitCleared=0,numReconcileID=NULL WHERE numDomainID=@numDomainID AND [numReconcileID] = @numReconcileID
        
		DELETE FROM [BankReconcileFileData] WHERE numDomainID=@numDomainID AND [numReconcileID] = @numReconcileID

	 	DELETE FROM [BankReconcileMaster] WHERE numDomainID=@numDomainID AND [numReconcileID] = @numReconcileID
	 END
	 ELSE IF @tintMode=4 -- Select based on numChartAcntId and bitReconcileComplete
	 BEGIN
		SELECT *,(SELECT ISNULL(SUM(numCreditAmt),0) FROM General_Journal_Details WHERE numDomainID=@numDomainID AND [numReconcileID] = BRM.numReconcileID /*AND ISNULL(bitReconcile,0)=1*/ AND numChartAcntId=BRM.numChartAcntId) AS monPayment
		,(SELECT ISNULL(SUM(numDebitAmt),0) FROM General_Journal_Details WHERE numDomainID=@numDomainID AND [numReconcileID] = BRM.numReconcileID /*AND ISNULL(bitReconcile,0)=1*/ AND numChartAcntId=BRM.numChartAcntId) AS monDeposit
	      FROM BankReconcileMaster BRM WHERE  numDomainID=@numDomainID AND numChartAcntId=@numChartAcntId 
			AND ISNULL(bitReconcileComplete,0)=@bitReconcileComplete ORDER BY numReconcileID DESC 
	 END
	 ELSE IF @tintMode=5 -- Complete Bank Reconcile
	 BEGIN
	    UPDATE General_Journal_Details SET bitReconcile=1,bitCleared=0 WHERE numDomainID=@numDomainID AND [numReconcileID] = @numReconcileID

		UPDATE BankReconcileFileData SET bitReconcile=1,bitCleared=0 WHERE numDomainID=@numDomainID AND [numReconcileID] = @numReconcileID AND ISNULL(bitCleared,0)=1
	 
		UPDATE BankReconcileMaster SET bitReconcileComplete=1,dtReconcileDate=GETUTCDATE(),numCreatedBy=@numCreatedBy 
			WHERE numDomainID=@numDomainID AND [numReconcileID] = @numReconcileID
	 END
	ELSE IF @tintMode=6 -- Get Last added entry for numChartAcntId
	BEGIN
	SELECT TOP 1 *
	    FROM BankReconcileMaster BRM WHERE  numDomainID=@numDomainID AND numChartAcntId=@numChartAcntId 
		AND ISNULL(bitReconcileComplete,0)=1 ORDER BY numReconcileID DESC 
	END
	ELSE IF @tintMode = 7 -- Select based on numChartAcntId
	BEGIN
		SELECT *,(SELECT ISNULL(SUM(numCreditAmt),0) FROM General_Journal_Details WHERE numDomainID=@numDomainID AND [numReconcileID] = BRM.numReconcileID /*AND ISNULL(bitReconcile,0)=1*/ AND numChartAcntId=BRM.numChartAcntId) AS monPayment
		,(SELECT ISNULL(SUM(numDebitAmt),0) FROM General_Journal_Details WHERE numDomainID=@numDomainID AND [numReconcileID] = BRM.numReconcileID /*AND ISNULL(bitReconcile,0)=1*/ AND numChartAcntId=BRM.numChartAcntId) AS monDeposit
	      FROM BankReconcileMaster BRM WHERE  numDomainID=@numDomainID AND numChartAcntId=@numChartAcntId 
			ORDER BY numReconcileID DESC 
	END
    
/****** Object:  StoredProcedure [dbo].[USP_ManageItemList]    Script Date: 07/26/2008 16:19:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj      
--Author:Anoop Jayaraj
--Modified By:Sachin Sadhu
--History: Added One Field numListType to save  bizdoc type wise BizDoc status     (added on 12thJun)    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_manageitemlist')
DROP PROCEDURE usp_manageitemlist
GO
CREATE PROCEDURE [dbo].[USP_ManageItemList]                
@numListItemID as numeric(9) OUTPUT,                  
@numDomainID as numeric(9)=0,                     
@vcData as varchar(100)='',             
@numListID as numeric(9)=0,                      
@numUserCntID as numeric(9),
@numListType numeric(9,0),
@tintOppOrOrder TINYINT,
@bitEnforceMinOrderAmount bit = 0,
@fltMinOrderAmount FLOAT = 0              
as                    
if @numListItemID =0                    
                    
begin         
	declare @sintOrder as smallint     
	Declare @numListItemIDPK as numeric(9)   

	update ListDetails set  sintOrder=0 where sintOrder is null   
	   
	select @sintOrder=(select max(sintOrder)+1 from ListDetails where numListID=@numListID)      
      
                 
	insert into ListDetails(numListID,vcData,numCreatedBY,bintCreatedDate,numModifiedBy,bintModifiedDate,bitDelete,numDomainID,constFlag,sintOrder,numListType,tintOppOrOrder)                    
	values(@numListID,@vcData,@numUserCntID,getutcdate(),@numUserCntID,getutcdate(),0,@numDomainID,0,@sintOrder,@numListType,@tintOppOrOrder)     
  
	Set @numListItemIDPK = SCOPE_IDENTITY()
	SET @numListItemID = SCOPE_IDENTITY()

	Declare @CurrentDate as datetime      
    
	Set @CurrentDate=getutcdate()    
	
	IF @bitEnforceMinOrderAmount = 1 AND @numListID = 21
	BEGIN
		INSERT INTO SalesOrderRule
			(numDomainId, numListItemID, numListID, bitEnforceMinOrderAmount, fltMinOrderAmount)
		VALUES
			(@numDomainID, @numListItemID, @numListID, @bitEnforceMinOrderAmount, @fltMinOrderAmount)
	END
	--If @numListID=64    
	--Begin    
	--Exec usp_InsertNewChartAccountDetails @numParntAcntId=10,@numAcntType=818,@vcCatgyName=@vcData,@vcCatgyDescription=@vcData,@numOriginalOpeningBal=null,    
	--     @numOpeningBal=null,@dtOpeningDate=@CurrentDate,@bitActive=1,@numAccountId=0,@bitFixed=0,@numDomainId=@numDomainID,@numListItemID=@numListItemIDPK    
	--End    
end                    
else                    
begin                    
	update ListDetails set vcData=@vcData,                   
	numModifiedBy=@numUserCntID,                  
	bintModifiedDate=getutcdate(),
	numListType=@numListType,
	tintOppOrOrder=@tintOppOrOrder            
	where numListItemID=@numListItemID     
	--To update Category Description in Chart_of_accounts table  
	--update Chart_Of_Accounts Set vcCatgyName=@vcData,vcCatgyDescription=@vcData Where numListItemID=@numListItemID        
            
	IF @bitEnforceMinOrderAmount = 1 AND @numListID = 21
	BEGIN
		IF EXISTS(SELECT * FROM SalesOrderRule WHERE numListItemID=@numListItemID)
		BEGIN
			UPDATE SalesOrderRule SET 
				bitEnforceMinOrderAmount = @bitEnforceMinOrderAmount, fltMinOrderAmount = @fltMinOrderAmount 
			WHERE numListItemID=@numListItemID    
		END
		ELSE
		BEGIN
			INSERT INTO SalesOrderRule
				(numDomainId, numListItemID, numListID, bitEnforceMinOrderAmount, fltMinOrderAmount)
			VALUES
				(@numDomainID, @numListItemID, @numListID, @bitEnforceMinOrderAmount, @fltMinOrderAmount)
		END
	END    
	ELSE IF @bitEnforceMinOrderAmount = 0 AND @numListID = 21
	BEGIN
		IF EXISTS(SELECT * FROM SalesOrderRule WHERE numListItemID=@numListItemID)
		BEGIN
			UPDATE SalesOrderRule SET 
				bitEnforceMinOrderAmount = @bitEnforceMinOrderAmount, fltMinOrderAmount = 0 
			WHERE numListItemID=@numListItemID    
		END
	END             
end
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManagePromotionOfferOrderBased')
DROP PROCEDURE USP_ManagePromotionOfferOrderBased
GO
CREATE PROCEDURE [dbo].[USP_ManagePromotionOfferOrderBased]
	@numProId AS NUMERIC(9) = 0,
	@vcProName AS VARCHAR(100),
	@numDomainId AS NUMERIC(18,0),
	@numUserCntID NUMERIC(18,0),
	@fltSubTotal FLOAT,
	@cSaleType CHAR,
	@byteMode TINYINT = 0,
	@fltOldSubTotal FLOAT = 0
	--@numProOrderBasedID AS NUMERIC(9) = 0
AS 
BEGIN

	IF @numProId = 0 
		BEGIN
			INSERT INTO PromotionOffer
			(
				vcProName, numDomainId, numCreatedBy, dtCreated, bitEnabled, IsOrderBasedPromotion
			)
			VALUES  
			(
				@vcProName, @numDomainId, @numUserCntID, GETUTCDATE(), 1, 1
			)
            
			SELECT SCOPE_IDENTITY()
		END
	ELSE IF @numProId > 0 
		BEGIN
			IF @byteMode = 0
			BEGIN			
				IF @cSaleType = 'I'
				BEGIN
					IF (SELECT COUNT(*) FROM PromotionOfferOrderBased WHERE numProId = @numProId AND fltSubTotal = @fltSubTotal) > 0
					BEGIN
						RAISERROR ( 'DUPLICATE_SUBTOTAL_AMOUNT',16, 1 )
 						RETURN;
					END

					IF (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId = @numProId AND tintType = 1 AND tintRecordType = 5) > 0
					BEGIN
						IF ((SELECT COUNT(*) FROM PromotionOfferOrderBased WHERE numProId = @numProId AND numDomainId = @numDomainId AND cSaleType = 'I') > 0)
						BEGIN
							IF((SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId = @numProId AND tintType = 1 AND tintRecordType = 5) > 1)
							BEGIN
							
								DELETE FROM PromotionOfferOrderBased WHERE numProId = @numProId AND numDomainId = @numDomainId AND cSaleType = 'C'

								INSERT INTO PromotionOfferOrderBased
								(
									numProId, numProItemId, numDomainId, fltSubTotal, cSaleType, cItems, bitEnabled
								)
								SELECT numProId, numValue, @numDomainId, @fltSubTotal, @cSaleType, 'M', 1 FROM PromotionOfferItems WHERE numProId = @numProId AND tintType = 1 AND tintRecordType = 5
								AND numValue NOT IN(SELECT numProItemId FROM PromotionOfferOrderBased WHERE numProId = @numProId AND numDomainId = @numDomainId AND fltSubTotal = @fltSubTotal)
							END
							ELSE IF((SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId = @numProId AND tintType = 1 AND tintRecordType = 5) = 1)
							BEGIN

								DELETE FROM PromotionOfferOrderBased WHERE numProId = @numProId AND numDomainId = @numDomainId AND cSaleType = 'C'

								INSERT INTO PromotionOfferOrderBased
								(
									numProId, numProItemId, numDomainId, fltSubTotal, cSaleType, cItems, bitEnabled
								)
								SELECT numProId, numValue, @numDomainId, @fltSubTotal, @cSaleType, 'S', 1 FROM PromotionOfferItems WHERE numProId = @numProId AND tintType = 1 AND tintRecordType = 5
								AND numValue NOT IN(SELECT numProItemId FROM PromotionOfferOrderBased WHERE numProId = @numProId AND numDomainId = @numDomainId AND fltSubTotal = @fltSubTotal)
							END
						END
						ELSE
						BEGIN
							IF((SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId = @numProId AND tintType = 1 AND tintRecordType = 5) > 1)
							BEGIN

								DELETE FROM PromotionOfferOrderBased WHERE numProId = @numProId AND numDomainId = @numDomainId AND cSaleType = 'C'

								INSERT INTO PromotionOfferOrderBased
								(
									numProId, numProItemId, numDomainId, fltSubTotal, cSaleType, cItems, bitEnabled
								)
								SELECT numProId, numValue, @numDomainId, @fltSubTotal, @cSaleType, 'M', 1 FROM PromotionOfferItems WHERE numProId = @numProId AND tintType = 1 AND tintRecordType = 5
							END
							ELSE
							BEGIN
							
								DELETE FROM PromotionOfferOrderBased WHERE numProId = @numProId AND numDomainId = @numDomainId AND cSaleType = 'C'

								INSERT INTO PromotionOfferOrderBased
								(
									numProId, numProItemId, numDomainId, fltSubTotal, cSaleType, cItems, bitEnabled
								)
								SELECT numProId, numValue, @numDomainId, @fltSubTotal, @cSaleType, 'S', 1 FROM PromotionOfferItems WHERE numProId = @numProId AND tintType = 1 AND tintRecordType = 5
							END
						END
					END
					ELSE
					BEGIN
						IF (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId = @numProId AND tintType = 1 AND tintRecordType = 5) = 0
						BEGIN

							RAISERROR ( 'SELECT_ITEMS_FOR_PROMOTIONS',16, 1 )
 							RETURN;
						END
					END
				END
				ELSE IF @cSaleType = 'C'
				BEGIN
				
					DELETE FROM PromotionOfferOrderBased WHERE numProId = @numProId AND numDomainId = @numDomainId
					DELETE FROM PromotionOfferItems WHERE numProId = @numProId AND tintType = 1 AND tintRecordType = 5

					INSERT INTO PromotionOfferOrderBased
					(
						numProId, cSaleType, numDomainId, bitEnabled
					)
					VALUES
					(	@numProId, @cSaleType, @numDomainId, 1	)
				END
				SELECT @numProId
			END
			ELSE IF @byteMode = 1
			BEGIN
				IF (SELECT COUNT(*) FROM PromotionOfferOrderBased WHERE numProId = @numProId AND fltSubTotal = @fltSubTotal) > 0
				BEGIN
					RAISERROR ( 'DUPLICATE_SUBTOTAL_AMOUNT',16, 1 )
 					RETURN;
				END

				CREATE TABLE #temp 
				(
					ID int IDENTITY (1, 1) NOT NULL ,
					numProOrderBasedID numeric(18,0)
				)
				INSERT INTO #temp(numProOrderBasedID)
					SELECT numProOrderBasedID FROM PromotionOfferOrderBased WHERE numProId = @numProId AND fltSubTotal = @fltOldSubTotal	

				UPDATE PromotionOfferOrderBased 
				SET fltSubTotal = @fltSubTotal 
				WHERE numProOrderBasedID IN ( SELECT numProOrderBasedID FROM #temp )
			END
		END
END
/****** Object:  StoredProcedure [dbo].[USP_ManageTabsInCuSFields]    Script Date: 07/26/2008 16:20:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_managetabsincusfields')
DROP PROCEDURE usp_managetabsincusfields
GO
CREATE PROCEDURE [dbo].[USP_ManageTabsInCuSFields]      
@byteMode as tinyint=0,      
@LocID as numeric(9)=0,      
@TabName as varchar(50)='',      
@TabID as numeric(9)=0  ,    
--@vcURL as varchar(1000)='',  
@numDomainID as numeric(9),
@vcURL as varchar(100)=''    
as      
      
      
if @byteMode=1      
begin      
insert into  CFw_Grp_Master(Grp_Name,Loc_Id,numDomainID,tintType)       
values(@TabName,@LocID,@numDomainID,0)      
      
end      
      
if @byteMode=2      
begin      
update  CFw_Grp_Master set Grp_Name=@TabName       
where Grp_id=@TabID      
      
end      
      
if @byteMode=3      
begin      
--Check if any custom fields associated with subtab 
IF (SELECT COUNT(*) FROM cfw_fld_master WHERE [subgrp]= @TabID)>0
BEGIN
	  RAISERROR ('CHILD_RECORD_FOUND',16,1);
	RETURN 
END
--delete permission
--[tintType]=1=subtab 
DELETE FROM [GroupTabDetails] WHERE [numTabId]=@TabID AND [tintType]=1
--delete subtabs
delete from  CFw_Grp_Master        
where Grp_id=@TabID  
    
      
end     
   
if @byteMode=4      
begin      
insert into  CFw_Grp_Master(Grp_Name,Loc_Id,numDomainID,tintType,vcURLF)       
values(@TabName,@LocID,@numDomainID,1,@vcURL)      
      
END
--Added by chintan, this mode will add existing subtabs with tintType=2 which can not be deleted  
if @byteMode=5 
begin      
insert into  CFw_Grp_Master(Grp_Name,Loc_Id,numDomainID,tintType,vcURLF)       
values(@TabName,@LocID,@numDomainID,2,@vcURL)      
      
END
--Added by chintan, this mode will add Default subtabs and give permission to all Roles by default
-- Only required Parameter is DomainID
IF @byteMode = 6 
    BEGIN      
        IF ( SELECT COUNT(*) FROM   domain WHERE  numDomainID = @numDomainID ) > 0 
        BEGIN
			DECLARE @i NUMERIC(9)	

			--ContactDetails
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=4,@TabName='Contact Details',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=4,@TabName='Areas of Interest',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=4,@TabName='Opportunities',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=4,@TabName='Survey History',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=4,@TabName='Correspondence',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			--SO
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=2,@TabName='Deal Details',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=2,@TabName='Milestones & Stages',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=2,@TabName='Associated Contacts',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=2,@TabName='Product/Service',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=2,@TabName='BizDocs',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=2,@TabName='Correspondence',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			--Added By Manish || To add Packaging/Shipping in Orders
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=2,@TabName='Shipping & Packaging',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			
			--PO
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=6,@TabName='Deal Details',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=6,@TabName='Milestones & Stages',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=6,@TabName='Associated Contacts',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=6,@TabName='Product/Service',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=6,@TabName='BizDocs',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=6,@TabName='Correspondence',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			--Added By Manish || To add Packaging/Shipping in Orders
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=6,@TabName='Shipping & Packaging',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			
			-- Case details
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=3,@TabName='Case Details',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=3,@TabName='Associated Contacts',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=3,@TabName='Correspondence',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			-- Project details
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=11,@TabName='Project Details',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=11,@TabName='Milestones And Stages',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=11,@TabName='Associated Contacts',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=11,@TabName='Project Income & Expense',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=11,@TabName='Correspondence',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			-- Leads locid has been changed from 1 to 14
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=14,@TabName='Lead Detail',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=14,@TabName='Areas of Interest',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=14,@TabName='Web Analysis',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=14,@TabName='Correspondence',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=14,@TabName='Assets',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=14,@TabName='Associations',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=14,@TabName='Survey History',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			-- Prospects
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=12,@TabName='Prospect Details',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=12,@TabName='Contacts',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=12,@TabName='Transactions',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=12,@TabName='Projects',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=12,@TabName='Default Settings',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=12,@TabName='Web Analysis',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=12,@TabName='Correspondence',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=12,@TabName='Assets',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=12,@TabName='Associations',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			-- Accounts
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=13,@TabName='Account Details',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=13,@TabName='Contacts',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=13,@TabName='Transactions',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=13,@TabName='Projects',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=13,@TabName='Cases',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=13,@TabName='Default Settings',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=13,@TabName='Web Analysis',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=13,@TabName='Correspondence',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=13,@TabName='Assets',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=13,@TabName='Contracts',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=13,@TabName='Associations',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			
			--Added By Sachin ||Issue raised by Customer:EasternBikes
			--Reason:To Make default entry for any new subscription
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=13,@TabName='Items',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=15,@TabName='Bought & Sold',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=15,@TabName='Vended',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			--end of code		
			--Added By Sachin||Issue raised by Customer:Coloroda State Uni
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=2,@TabName='Shipping & Packaging',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
            exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=6,@TabName='Shipping & Packaging',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			--end of Code

			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=17,@TabName='Action Items & Meetings',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=17,@TabName='Opportunities & Projects',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=17,@TabName='Cases',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=17,@TabName='Approval Requests',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			/*********Get Auth groups */
			---- Give permission by default
				DECLARE  @minGroupID NUMERIC(9)
				DECLARE  @maxGroupID NUMERIC(9)
				SELECT @minGroupID=MIN(numGroupID) FROM AuthenticationGroupMaster WHERE [numDomainID]=@numDomainID
				SELECT @maxGroupID=MAX(numGroupID) FROM AuthenticationGroupMaster WHERE [numDomainID]=@numDomainID
				WHILE  @minGroupID <= @maxGroupID
				  BEGIN
					PRINT 'GroupID=' + CONVERT(VARCHAR(20),@minGroupID)
							Set @i=0
							DECLARE  @minTabID NUMERIC(9)
							DECLARE  @maxTabID NUMERIC(9)
							SELECT @minTabID=MIN([Grp_id]) FROM [CFw_Grp_Master] WHERE [numDomainID]=@numDomainID 
							SELECT @maxTabID=max([Grp_id]) FROM [CFw_Grp_Master] WHERE [numDomainID]=@numDomainID 
							WHILE @minTabID<=@maxTabID
							BEGIN
								PRINT 'TabID=' + CONVERT(VARCHAR(20),@minTabID)
								---------------
								insert into GroupTabDetails
								(numGroupId,numTabId,bitallowed,numOrder,numRelationShip,tintType)
								VALUES (@minGroupID,@minTabID,1,@i+1,0,1)

								---------------
								SELECT @minTabID=MIN([Grp_id]) FROM [CFw_Grp_Master] WHERE [numDomainID]=@numDomainID AND [Grp_id]>@minTabID
							END
					
					SELECT @minGroupID = min(numGroupID)
					FROM AuthenticationGroupMaster WHERE [numDomainID]= @numDomainID AND [numGroupID] > @minGroupID
				  END		


        END
      
    END
GO
/****** Object:  StoredProcedure [dbo].[USP_OPPGetINItemsForAuthorizativeAccounting]    Script Date: 07/26/2008 16:20:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by Siva                                                                                                            
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_oppgetinitemsforauthorizativeaccounting')
DROP PROCEDURE usp_oppgetinitemsforauthorizativeaccounting
GO
CREATE PROCEDURE  [dbo].[USP_OPPGetINItemsForAuthorizativeAccounting]                                                                                                                                                
(                                                                                                                                                
 @numOppId as numeric(9)=null,                                                                                                                                                
 @numOppBizDocsId as numeric(9)=null,                                                                
 @numDomainID as numeric(9)=0 ,                                                      
 @numUserCntID as numeric(9)=0                                                                                                                                               
)                                                                                                                                                
                                                                                                                                            
as                           
begin

declare @numCurrencyID as numeric(9)
declare @fltExchangeRate as float

declare @fltCurrentExchangeRate as float
declare @tintType as tinyint                                                          
declare @vcPOppName as VARCHAR(100)                                                          
DECLARE @bitPPVariance AS BIT
DECLARE @numDivisionID AS NUMERIC(9)

select  @numCurrencyID=numCurrencyID, @fltExchangeRate=fltExchangeRate,@tintType=tintOppType,@vcPOppName=vcPOppName,@bitPPVariance=ISNULL(bitPPVariance,0),@numDivisionID=numDivisionID from  OpportunityMaster
where numOppID=@numOppId 

DECLARE @vcBaseCurrency AS VARCHAR(100);SET  @vcBaseCurrency=''
DECLARE @bitAutolinkUnappliedPayment AS BIT 
SELECT @vcBaseCurrency=ISNULL(C.varCurrSymbol,''),@bitAutolinkUnappliedPayment=ISNULL(bitAutolinkUnappliedPayment,0) FROM  dbo.Domain D LEFT JOIN Currency C ON D.numCurrencyID=C.numCurrencyID 
WHERE D.numDomainID=@numDomainID

DECLARE @vcForeignCurrency AS VARCHAR(100) ;SET  @vcForeignCurrency=''
SELECT @vcForeignCurrency=ISNULL(C.varCurrSymbol,'') FROM  Currency C WHERE numCurrencyID=@numCurrencyID

DECLARE @fltExchangeRateBizDoc AS FLOAT 
declare @numBizDocId as numeric(9)                                                           

DECLARE @vcBizDocID AS VARCHAR(100)
select @numBizDocId=numBizDocId,@fltExchangeRateBizDoc=fltExchangeRateBizDoc,@vcBizDocID=vcBizDocID  from OpportunityBizDocs where numOppBizDocsId=@numOppBizDocsId                                                         

if @numCurrencyID>0 set @fltCurrentExchangeRate=dbo.GetExchangeRate(@numDomainID,@numCurrencyID)
else set @fltCurrentExchangeRate=@fltExchangeRate                                                                                                                                          

declare @strSQL as varchar(8000)                                                          
declare @strSQLCusFields varchar(1000)                                                          
declare @strSQLEmpFlds varchar(500)                                                          
set @strSQLCusFields=''                                                          
set @strSQLEmpFlds=''                                                          
declare @intRowNum as int                                                          
declare @numFldID as varchar(15)                                                          
declare @vcFldname as varchar(50)                                                                                                                                          
                                                                
if @tintType=1 set @tintType=7                                                                
else set @tintType=8                                                                                                                      
select top 1 @numFldID=numfieldID,@vcFldname=vcFieldName,@intRowNum=(tintRow+1) from View_DynamicCustomColumns                                       
where Grp_id=@tintType and numDomainID=@numDomainID and numAuthGroupID=@numBizDocId and bitCustom=1 order by tintRow                                                          
 while @intRowNum>0                                               
 begin                                                          
  set @strSQLCusFields=@strSQLCusFields+',  dbo.GetCustFldValueBizdoc('+@numFldID+','+convert(varchar(2),@tintType)+',opp.numoppitemtCode) as ['+ @vcFldname+']'                                                     
  set @strSQLEmpFlds=@strSQLEmpFlds+',''-'' as ['+ @vcFldname+']'         
                                                 
  select top 1 @numFldID=numfieldID,@vcFldname=vcFieldName,@intRowNum=(tintRow+1) from View_DynamicCustomColumns                                                               
  where Grp_id=@tintType and numDomainID=@numDomainID and numAuthGroupID=@numBizDocId                                                          
  and bitCustom=1 and tintRow>@intRowNum order by tintRow                                                          
                                                            
  if @@rowcount=0 set @intRowNum=0                                                          
                                                           
 end                                                           
                                                       
       
         
SELECT 
	I.[vcItemName] as Item
	,charitemType as type
	,OBI.vcitemdesc as [desc]
	,OBI.numUnitHour as Unit
	,ISNULL(OBI.monPrice,0) as Price
	,ISNULL(OBI.monTotAmount,0) as Amount
	,ISNULL((opp.monTotAmount/opp.numUnitHour)*OBI.numUnitHour,0) AS  ItemTotalAmount
	,isnull(monListPrice,0) as listPrice
	,convert(varchar,i.numItemCode) as ItemCode
	,numoppitemtCode
	,L.vcdata as vcItemClassification
	,case when bitTaxable=0 then 'No' else 'Yes' end as Taxable
	,cast(monListPrice as varchar) + ' ' + @strSQLCusFields
	,isnull(i.numIncomeChartAcntId,0) as itemIncomeAccount
	,isnull(i.numAssetChartAcntId,0) as itemInventoryAsset,'NI' as ItemType        
,isnull(i.numCOGsChartAcntId,0) as itemCoGs,(CASE WHEN ISNULL(i.bitVirtualInventory,0) = 1 THEN 0 ELSE isnull(Opp.monAvgCost,0) END) as AverageCost ,isnull(OBI.bitDropShip,0) as  bitDropShip ,  (OBI.monTotAmtBefDiscount-OBI.monTotAmount) as DiscAmt,
NULLIF(Opp.numProjectID,0) numProjectID ,NULLIF(Opp.numClassID,0) numClassID,ISNULL(i.bitKitParent,0) AS bitKitParent,ISNULL(i.bitAssembly,0) AS bitAssembly
from  OpportunityItems opp
join  OpportunityBizDocItems OBI
on OBI.numOppItemID=Opp.numoppitemtCode       
left join item i on opp.numItemCode=i.numItemCode        
left join ListDetails L on i.numItemClassification=L.numListItemID        
where Opp.numOppId=@numOppId  and OBI.numOppBizDocID=@numOppBizDocsId
                 
                                                                                                                  
                                                         
  select  isnull(@numCurrencyID,0) as numCurrencyID, isnull(@fltExchangeRate,1) as fltExchangeRate,isnull(@fltCurrentExchangeRate,1) as CurrfltExchangeRate,isnull(@fltExchangeRateBizDoc,1) AS fltExchangeRateBizDoc ,@vcBaseCurrency AS vcBaseCurrency,@vcForeignCurrency AS vcForeignCurrency,ISNULL(@vcPOppName,'') AS vcPOppName,ISNULL(@vcBizDocID,'') AS vcBizDocID,ISNULL(@bitPPVariance,0) AS bitPPVariance,@numDivisionID AS numDivisionID,ISNULL(@bitAutolinkUnappliedPayment,0) AS bitAutolinkUnappliedPayment                  
     
     
                                                  
End
GO
/****** Object:  StoredProcedure [dbo].[USP_OPPItemDetails]    Script Date: 09/01/2009 18:59:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- exec USP_OPPItemDetails @numItemCode=31455,@OpportunityId=12387,@numDomainId=110
--created by anooop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_oppitemdetails')
DROP PROCEDURE usp_oppitemdetails
GO
CREATE PROCEDURE [dbo].[USP_OPPItemDetails]
    (
      @numUserCntID NUMERIC(9) = 0,
      @OpportunityId NUMERIC(9) = 0,
      @numDomainId NUMERIC(9),
      @ClientTimeZoneOffset  INT
    )
AS 
BEGIN
    IF @OpportunityId >0
    BEGIN
        DECLARE @DivisionID AS NUMERIC(9)
        DECLARE @TaxPercentage AS FLOAT
        DECLARE @tintOppType AS TINYINT
        DECLARE @fld_Name AS VARCHAR(500)
        DECLARE @strSQL AS VARCHAR(MAX)
        DECLARE @fld_ID AS NUMERIC(9)
        DECLARE @numBillCountry AS NUMERIC(9)
        DECLARE @numBillState AS NUMERIC(9)

                
        SELECT  @DivisionID = numDivisionID,
                @tintOppType = tintOpptype
        FROM    OpportunityMaster
        WHERE   numOppId = @OpportunityId
               

		CREATE TABLE #tempForm 
		(
			tintOrder TINYINT,vcDbColumnName nvarchar(50),vcOrigDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),
			vcListItemType varchar(3),numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,
			numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,bitIsAlphaNumeric BIT,bitIsNumeric BIT,
			bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),ListRelID numeric(9),intColumnWidth int,intVisible int
			,vcFieldDataType CHAR(1)
		)

		IF
			(
				SELECT 
					COUNT(*)
				FROM 
					View_DynamicColumns DC 
				WHERE 
					DC.numFormId=(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) and DC.numDomainID=@numDomainID AND numUserCntID = @numUserCntID AND
					ISNULL(DC.bitSettingField,0)=1 AND ISNULL(DC.bitDeleted,0)=0 AND ISNULL(DC.bitCustom,0)=0) > 0
			OR
			(
				SELECT 
					COUNT(*)
				FROM 
					View_DynamicCustomColumns
				WHERE 
					numFormId=(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) AND numUserCntID=@numUserCntID AND numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=0 AND ISNULL(bitCustom,0)=1
			) > 0
		BEGIN	

		INSERT INTO #tempForm
		select isnull(DC.tintRow,0)+1 as tintOrder,DDF.vcDbColumnName,DDF.vcOrigDbColumnName,ISNULL(DDF.vcCultureFieldName,DDF.vcFieldName),                  
		 DDF.vcAssociatedControlType,DDF.vcListItemType,DDF.numListID,DDF.vcLookBackTableName                                                 
		,DDF.bitCustom,DDF.numFieldId,DDF.bitAllowSorting,DDF.bitInlineEdit,
		DDF.bitIsRequired,DDF.bitIsEmail,DDF.bitIsAlphaNumeric,DDF.bitIsNumeric,DDF.bitIsLengthValidation,
		DDF.intMaxLength,DDF.intMinLength,DDF.bitFieldMessage,DDF.vcFieldMessage vcFieldMessage,DDF.ListRelID,DDF.intColumnWidth,Case when DC.numFieldID is null then 0 else 1 end,DDF.vcFieldDataType
		 FROM View_DynamicDefaultColumns DDF left join View_DynamicColumns DC on DC.numFormId=(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) and DDF.numFieldId=DC.numFieldId and DC.numUserCntID=@numUserCntID and DC.numDomainID=@numDomainID and numRelCntType=0 AND tintPageType=1 
		 where DDF.numFormId=(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) and DDF.numDomainID=@numDomainID AND ISNULL(DDF.bitSettingField,0)=1 AND ISNULL(DDF.bitDeleted,0)=0 AND ISNULL(DDF.bitCustom,0)=0
       
		 UNION
    
			select tintRow+1 as tintOrder,vcDbColumnName,vcFieldName,vcFieldName,                  
		 vcAssociatedControlType,'' as vcListItemType,numListID,''                                                 
		,bitCustom,numFieldId,bitAllowSorting,0 as bitAllowEdit,
		bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
		intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,''
		 from View_DynamicCustomColumns
		 where numFormId=(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=0
		 AND ISNULL(bitCustom,0)=1

		END
		ELSE
		BEGIN
			--Check if Master Form Configuration is created by administrator if NoColumns=0
			DECLARE @IsMasterConfAvailable BIT = 0
			DECLARE @numUserGroup NUMERIC(18,0)

			SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

			IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = (CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
			BEGIN
				SET @IsMasterConfAvailable = 1
			END

			--If MasterConfiguration is available then load it otherwise load default columns
			IF @IsMasterConfAvailable = 1
			BEGIN
				INSERT INTO #tempForm
				SELECT 
					(intRowNum + 1) as tintOrder, vcDbColumnName, vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
					vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName,bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,
					bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,
					bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth,1,vcFieldDataType
				FROM 
					View_DynamicColumnsMasterConfig
				WHERE
					View_DynamicColumnsMasterConfig.numFormId=(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) AND 
					View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
					View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
					View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
					ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
					ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
				UNION
				SELECT 
					tintRow+1 as tintOrder,vcDbColumnName,vcFieldName,vcFieldName,                  
					 vcAssociatedControlType,'' as vcListItemType,numListID,''                                                 
					,bitCustom,numFieldId,bitAllowSorting,0 as bitAllowEdit,
					bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
					intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,''
				FROM 
					View_DynamicCustomColumnsMasterConfig
				WHERE 
					View_DynamicCustomColumnsMasterConfig.numFormId=(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) AND 
					View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
					View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
					View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
					ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1
				ORDER BY 
					tintOrder ASC  
			END
		END


SET @strSQL = ''

DECLARE @avgCost int
SET @avgCost = ISNULL((select TOP 1 numCost from Domain where numDOmainId=@numDomainID),0)



--------------Make Dynamic Query--------------
SET @strSQL = @strSQL + '
Select 
Opp.numoppitemtCode,Opp.vcNotes,WItems.numBackOrder,CAST(ISNULL(Opp.numCost,0) AS DECIMAL(20,5)) AS numCost,
ISNULL(Opp.bitItemPriceApprovalRequired,0) AS bitItemPriceApprovalRequired,
ISNULL(Opp.numSOVendorID,0) AS numVendorID,'+
CONCAT('ISNULL(Opp.monTotAmount,0) - (ISNULL(Opp.numUnitHour,0) * (CASE ',@avgCost,' WHEN 3 THEN ISNULL(V.monCost,0) / dbo.fn_UOMConversion(numPurchaseUnit,I.numItemCode,',@numDomainID,',numBaseUnit) WHEN 2 THEN (CASE WHEN ISNULL(Opp.numSOVendorID,0) > 0 THEN Opp.numCost ELSE ISNULL(V.monCost,0) / dbo.fn_UOMConversion(numPurchaseUnit,I.numItemCode,',@numDomainID,',numBaseUnit) END) ELSE monAverageCost END)) AS vcMargin,')
+'
OM.tintOppType,
Opp.numItemCode,
I.charItemType,
ISNULL(I.bitAsset,0) as bitAsset,
ISNULL(I.bitRental,0) as bitRental,
ISNULL(I.bitSerialized,0) bitSerialized,
ISNULL(I.bitLotNo,0) bitLotNo,
Opp.numOppId,
Opp.bitWorkOrder,
Opp.fltDiscount,
ISNULL(Opp.bitDiscountType,0) bitDiscountType,
dbo.fn_UOMConversion(Opp.numUOMId, Opp.numItemCode,' + CAST(@numDomainId AS VARCHAR) +', NULL) AS UOMConversionFactor,
ISNULL(Opp.bitWorkOrder,0) bitWorkOrder,
ISNULL(Opp.numUnitHourReceived,0) numUnitHourReceived,
ISNULL((SELECT COUNT(*) FROM OppWarehouseSerializedItem WHERE numOppId=Opp.numOppID AND numOppItemID=Opp.numoppitemtCode),0) AS numSerialNoAssigned,
CASE WHEN (SELECT COUNT(*) FROM OpportunityBizDocs OB JOIN dbo.OpportunityBizDocItems OBI ON ob.numOppBizDocsId=obi.numOppBizDocID
					AND (ob.bitAuthoritativeBizDocs=1 OR ob.numBizDocId=304) AND obi.numOppItemID=Opp.numoppitemtCode AND ob.numOppId=Opp.numOppId)>0 THEN 1 ELSE 0 END  AS bitIsAuthBizDoc,
(CASE WHEN (SELECT COUNT(*) FROM OpportunityBizDocs OB JOIN dbo.OpportunityBizDocItems OBI ON OB.numOppBizDocsId=OBI.numOppBizDocID WHERE OB.numOppId = Opp.numOppId AND OBI.numOppItemID=Opp.numoppitemtCode AND OB.numBizDocId=296) > 0 THEN 1 ELSE 0 END) AS bitAddedFulFillmentBizDoc,
ISNULL(numPromotionID,0) AS numPromotionID,
ISNULL(( SELECT numReturnID FROM   Returns WHERE  numOppItemCode = Opp.numoppitemtCode AND numOppId = Opp.numOppID), 0) AS numReturnID,
dbo.fn_GetReturnItemQty(Opp.numoppitemtCode) AS ReturrnedQty,
[dbo].fn_GetReturnStatus(Opp.numoppitemtCode) AS ReturnStatus,
ISNULL(opp.numUOMId, 0) AS numUOM,
(SELECT vcUnitName FROM dbo.UOM WHERE numUOMId = opp.numUOMId AND ISNULL(bitEnabled,0) = 1)  AS  [vcBaseUOMName],
Opp.numUnitHour AS numOriginalUnitHour, 
isnull(M.vcPOppName,''Internal'') as Source,
numSourceID,
  CASE WHEN ISNULL(bitKitParent,0) = 1
                               AND ISNULL(bitAssembly,0) = 1 THEN 0
                          WHEN ISNULL(bitKitParent,0) = 1 THEN 1
                          ELSE 0
                     END as bitKitParent,
(CASE WHEN OM.tintOppType = 1 AND OM.tintOppStatus = 1 THEN 
(CASE WHEN (I.charItemType = ''p'' AND ISNULL(bitDropShip, 0) = 0 AND ISNULL(bitAsset,0)=0)THEN dbo.CheckOrderItemInventoryStatus(Opp.numItemCode,Opp.numUnitHour,Opp.numoppitemtCode,Opp.numWarehouseItmsID,(CASE WHEN ISNULL(bitKitParent,0) = 1
                               AND ISNULL(bitAssembly,0) = 1 THEN 0
                          WHEN ISNULL(bitKitParent,0) = 1 THEN 1
                          ELSE 0
                     END)) ELSE 0 END) ELSE 0 END) AS bitBackOrder,
(SELECT vcData FROM dbo.ListDetails WHERE 1=1 AND numListItemID = numShipClass ) AS [numShipClass],ISNULL(Opp.numQtyShipped,0) numQtyShipped,ISNULL(Opp.numUnitHourReceived,0) AS numUnitHourReceived,ISNULL(OM.fltExchangeRate,0) AS fltExchangeRate,ISNULL(OM.numCurrencyID,0) AS numCurrencyID,ISNULL(OM.numDivisionID,0) AS numDivisionID,
(CASE 
	WHEN OM.tintOppType = 1 AND OM.tintOppStatus = 1 AND UPPER(I.charItemType) = ''P'' AND bitDropShip = 0
	THEN (CASE WHEN OM.tintShipped = 1 THEN CAST(ISNULL(Opp.numUnitHour,0) AS VARCHAR) ELSE CAST(ISNULL(Opp.numQtyShipped,0) AS VARCHAR) END)
	WHEN OM.tintOppType = 2 AND OM.tintOppStatus = 1 AND UPPER(I.charItemType) = ''P'' AND bitDropShip = 0
	THEN (CASE WHEN OM.tintShipped = 1 THEN CAST(ISNULL(Opp.numUnitHour,0) AS VARCHAR) ELSE CAST(ISNULL(Opp.numUnitHourReceived,0) AS VARCHAR) END)
	ELSE '''' END) AS vcShippedReceived,
isnull(i.numIncomeChartAcntId,0) as itemIncomeAccount,isnull(i.numAssetChartAcntId,0) as itemInventoryAsset,isnull(i.numCOGsChartAcntId,0) as itemCoGs,
ISNULL(Opp.numRentalIN,0) as numRentalIN,
ISNULL(Opp.numRentalLost,0) as numRentalLost,
ISNULL(Opp.numRentalOut,0) as numRentalOut,
(SELECT vcData FROM dbo.ListDetails WHERE 1=1 AND numListItemID = numShipClass ) AS [numShipClass],ISNULL(bitFreeShipping,0) AS [IsFreeShipping],Opp.bitDiscountType,isnull(Opp.monLandedCost,0) as monLandedCost, ISNULL(Opp.vcPromotionDetail,'''') AS vcPromotionDetail,
(CASE WHEN I.charItemType = ''p'' AND ISNULL(bitDropShip, 0) = 0 THEN dbo.GetOppItemReleaseDates(om.numDomainId,Opp.numOppId,Opp.numoppitemtCode,1) ELSE '''' END) AS vcItemReleaseDate,'''' AS numPurchasedQty, '

--	IF @fld_Name = 'vcPathForTImage' 
		SET @strSQL = @strSQL + 'Opp.vcPathForTImage,'
--	ELSE IF @fld_Name = 'vcItemName' 
		SET @strSQL = @strSQL + 'Opp.vcItemName,'
--	ELSE IF @fld_Name = 'vcModelID,' 
		SET @strSQL = @strSQL + 'Opp.vcModelID,'
--	ELSE IF @fld_Name = 'vcWareHouse' 
		SET @strSQL = @strSQL + 'vcWarehouse,WItems.numWarehouseID,'
		SET @strSQL = @strSQL + 'WItems.numOnHand,isnull(WItems.numAllocation,0) as numAllocation,'
		SET @strSQL = @strSQL + 'WL.vcLocation,'
--	ELSE IF @fld_Name = 'ItemType' 
		SET @strSQL = @strSQL + 'Opp.vcType ItemType,Opp.vcType,'
--	ELSE IF @fld_Name = 'vcItemDesc' 
		SET @strSQL = @strSQL + 'ISNULL(Opp.vcItemDesc, '''') vcItemDesc,'
--	ELSE IF @fld_Name = 'Attributes' 
		SET @strSQL = @strSQL + 'Opp.vcAttributes,' --dbo.fn_GetAttributes(Opp.numWarehouseItmsID, I.bitSerialized) AS Attributes,
--	ELSE IF @fld_Name = 'bitDropShip' 
		SET @strSQL = @strSQL + 'ISNULL(bitDropShip, 0) as DropShip,CASE ISNULL(bitDropShip, 0) WHEN 1 THEN ''Yes'' ELSE '''' END AS bitDropShip,'
--	ELSE IF @fld_Name = 'numUnitHour' 
		SET @strSQL = @strSQL + 'CAST(( dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), Opp.numItemCode,om.numDomainId, ISNULL(opp.numUOMId, 0))* Opp.numUnitHour ) AS NUMERIC(18, 2)) AS numUnitHour,'
--	ELSE IF @fld_Name = 'monPrice' 
		SET @strSQL = @strSQL + 'Opp.monPrice,'
--	ELSE IF @fld_Name = 'monPriceUOM' 
		SET @strSQL = @strSQL + 'CAST(( dbo.fn_UOMConversion(ISNULL(opp.numUOMId, 0), Opp.numItemCode,om.numDomainId, ISNULL(I.numBaseUnit, 0)) * Opp.monPrice) AS NUMERIC(18, 4)) AS monPriceUOM,'
--	ELSE IF @fld_Name = 'monTotAmount' 
		SET @strSQL = @strSQL + 'Opp.monTotAmount,Opp.monTotAmtBefDiscount,'
--	ELSE IF @fld_Name = 'vcSKU' 
		SET @strSQL = @strSQL + 'ISNULL(WItems.vcWHSKU,ISNULL(I.vcSKU,'''')) [vcSKU],'
--	ELSE IF @fld_Name = 'vcManufacturer' 
		SET @strSQL = @strSQL + 'Opp.vcManufacturer,'
--	ELSE IF @fld_Name = 'vcItemClassification' 
		SET @strSQL = @strSQL + ' dbo.[fn_GetListItemName](I.[numItemClassification]) numItemClassification,'
--	ELSE IF @fld_Name = 'numItemGroup' 
		SET @strSQL = @strSQL + '( SELECT    [vcItemGroup] FROM      [ItemGroups] WHERE     [numItemGroupID] = I.numItemGroup ) numItemGroup,'
--	ELSE IF @fld_Name = 'vcUOMName' 
		SET @strSQL = @strSQL + 'ISNULL(u.vcUnitName, '''') numUOMId,'
--	ELSE IF @fld_Name = 'fltWeight' 
		SET @strSQL = @strSQL + 'I.fltWeight,'
--	ELSE IF @fld_Name = 'fltHeight' 
		SET @strSQL = @strSQL + 'I.fltHeight,'
--	ELSE IF @fld_Name = 'fltWidth' 
		SET @strSQL = @strSQL + 'I.fltWidth,'
--	ELSE IF @fld_Name = 'fltLength' 
		SET @strSQL = @strSQL + 'I.fltLength,'
--	ELSE IF @fld_Name = 'numBarCodeId' 
		SET @strSQL = @strSQL + 'I.numBarCodeId,'
		SET @strSQL = @strSQL + 'I.IsArchieve,ISNULL(I.numContainer,0) AS numContainer,ISNULL(I.numNoItemIntoContainer,0) AS numContainerQty,'
--	ELSE IF @fld_Name = 'vcWorkOrderStatus' 
		SET @strSQL = @strSQL + 'CASE WHEN ISNULL(Opp.bitWorkOrder, 0) = 1
				 THEN ( SELECT  dbo.GetListIemName(numWOStatus)
								+ CASE WHEN ISNULL(numWOStatus, 0) = 23184
									   THEN '' - ''
											+ CAST(ISNULL(dbo.fn_GetContactName(numAssignedTo),
														  '''') + '',''
											+ CONVERT(VARCHAR(20), DATEADD(minute, '+ CONVERT(VARCHAR(15),-@ClientTimeZoneOffset)  +', bintCompletedDate)) AS VARCHAR(100))
									   ELSE ''''
								  END
						FROM    WorkOrder
						WHERE   ISNULL(numOppId, 0) = Opp.numOppId
								AND numItemCode = Opp.numItemCode AND ISNULL(numParentWOID,0)=0
					  )
				 ELSE ''''
			END AS vcWorkOrderStatus,'
--	ELSE IF @fld_Name = 'DiscAmt' 
		SET @strSQL = @strSQL + 'CASE WHEN Opp.bitDiscountType = 0
				 THEN REPLACE(STR(ISNULL(Opp.monTotAmtBefDiscount, Opp.monTotAmount)
					  - Opp.monTotAmount,20,4), '' '', '''') + '' (''
					  + CAST(FORMAT(Opp.fltDiscount,''0.00##'') AS VARCHAR(50)) + ''%)''
				 ELSE REPLACE(STR(ISNULL(Opp.monTotAmtBefDiscount, Opp.monTotAmount)
					  - Opp.monTotAmount,20,4), '' '', '''')
			END AS DiscAmt,'	
--	ELSE IF @fld_Name = 'numProjectID' 
		SET @strSQL = @strSQL + ' ISNULL(Opp.numProjectID, 0) numProjectID,
		 CASE WHEN ( SELECT TOP 1
								ISNULL(OBI.numOppBizDocItemID, 0)
						FROM    dbo.OpportunityBizDocs OBD
								INNER JOIN dbo.OpportunityBizDocItems OBI ON OBD.numOppBizDocsID = OBI.numOppBizDocID
						WHERE   OBI.numOppItemID = Opp.numoppitemtCode
								AND ISNULL(OBD.bitAuthoritativeBizDocs, 0) = 1
								and isnull(OBD.numOppID,0)=OM.numOppID
					  ) > 0 THEN 1
				 ELSE 0
			END AS bitItemAddedToAuthBizDoc,'	
--	ELSE IF @fld_Name = 'numClassID' 
		SET @strSQL = @strSQL + 'ISNULL(Opp.numClassID, 0) numClassID,'	
--	ELSE IF @fld_Name = 'vcProjectStageName' 
		SET @strSQL = @strSQL + ' ISNULL(Opp.numProjectStageID, 0) numProjectStageID,
				CASE WHEN OPP.numProjectStageID > 0
				 THEN dbo.fn_GetProjectStageName(OPP.numProjectId,
												 OPP.numProjectStageID)
				 WHEN OPP.numProjectStageID = -1 THEN ''T&E''
				 WHEN OPP.numProjectId > 0
				 THEN CASE WHEN OM.tintOppType = 1 THEN ''S.O.''
						   ELSE ''P.O.''
					  END
				 ELSE ''-''
			END AS vcProjectStageName,'	
	   
	   IF EXISTS (SELECT * FROM #tempForm WHERE vcOrigDbColumnName='vcInclusionDetails')
	   BEGIN
			SET @strSQL = @strSQL + 'dbo.GetOrderAssemblyKitInclusionDetails(OM.numOppID,Opp.numoppitemtCode,Opp.numUnitHour) AS vcInclusionDetails,'
	   END     
	
	-- (CASE WHEN @tintOppType=1 THEN 26 ELSE 129 END) --

--------------Add custom fields to query----------------
SELECT TOP 1 @fld_Name= vcFieldName,@fld_ID = numFieldId FROM #tempForm WHERE ISNULL(bitCustomField,0)=1 ORDER BY numFieldId
WHILE @fld_ID > 0
BEGIN ------------------

	 PRINT @fld_Name
	
	       SET @strSQL = @strSQL + 'dbo.GetCustFldValueOppItems('
                                    + CONVERT(VARCHAR(15), @fld_ID)
                                    + ',Opp.numoppitemtCode,Opp.numItemCode) as ''' + @fld_Name + ''','
       

SELECT TOP 1 @fld_Name= vcFieldName,@fld_ID = numFieldId FROM #tempForm
WHERE ISNULL(bitCustomField,0)=1 AND numFieldId > @fld_ID ORDER BY numFieldId
 IF @@ROWCOUNT=0
 SET @fld_ID = 0
END ------------------


                                
--Purchase Opportunity ?
IF @tintOppType = 2 
    BEGIN
        SET @strSQL =  @strSQL + 'ISNULL((SELECT sum(numQty) FROM OppWarehouseSerializedItem WHERE numOppID=OM.numOppID AND numOppItemID=Opp.numoppitemtCode),0) AS SerialLotNo,
		  SUBSTRING(( SELECT  '', '' + vcSerialNo
                        + CASE WHEN ISNULL(I.bitLotNo, 0) = 1
                                THEN '' (''
                                    + CONVERT(VARCHAR(15), oppI.numQty)
                                    + '')''
                                ELSE ''''
                            END
                FROM    OppWarehouseSerializedItem oppI
                        JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID = whi.numWareHouseItmsDTLID
                WHERE   oppI.numOppID = Opp.numOppId
                        AND oppI.numOppItemID = Opp.numoppitemtCode
                ORDER BY vcSerialNo
                FOR
                XML PATH('''')
                ), 3, 200000) AS vcSerialLotNo,
		  CAST(Opp.numUnitHour AS FLOAT) numQty,
		  (SELECT vcUnitName FROM dbo.UOM WHERE numUOMId = opp.numUOMId AND ISNULL(bitEnabled,0) = 1)  AS  [vcBaseUOMName],
		  I.bitSerialized,
		  ISNULL(I.bitLotNo, 0) AS bitLotNo,
		  0 AS bitBarcodePrint,
		  Opp.numWarehouseItmsID as numWarehouseItmsID,'
	END
ELSE
	BEGIN	
        SET @strSQL =  @strSQL + ' ISNULL((SELECT sum(numQty) FROM OppWarehouseSerializedItem WHERE numOppID=OM.numOppID AND numOppItemID=Opp.numoppitemtCode),0) AS SerialLotNo,
		   SUBSTRING(( SELECT  '', '' + vcSerialNo
                        + CASE WHEN ISNULL(I.bitLotNo, 0) = 1
                                THEN '' (''
                                    + CONVERT(VARCHAR(15), oppI.numQty)
                                    + '')''
                                ELSE ''''
                            END
                FROM    OppWarehouseSerializedItem oppI
                        JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID = whi.numWareHouseItmsDTLID
                WHERE   oppI.numOppID = Opp.numOppId
                        AND oppI.numOppItemID = Opp.numoppitemtCode
                ORDER BY vcSerialNo
                FOR
                XML PATH('''')
                ), 3, 200000) AS vcSerialLotNo,
		   CAST(Opp.numUnitHour AS FLOAT) numQty,
		  I.bitSerialized,
		  ISNULL(I.bitLotNo, 0) AS bitLotNo,
		  Opp.numWarehouseItmsID as numWarehouseItmsID,'
	END


        
 SET @strSQL = left(@strSQL,len(@strSQL)-1) + '   
        FROM    OpportunityItems Opp
        LEFT JOIN OpportunityMaster OM ON Opp.numOppID = oM.numOppID
        JOIN item I ON Opp.numItemCode = i.numItemcode
        LEFT JOIN WareHouseItems WItems ON WItems.numWareHouseItemID = Opp.numWarehouseItmsID
        LEFT JOIN Warehouses W ON W.numWarehouseID = WItems.numWarehouseID
        LEFT JOIN WarehouseLocation WL ON WL.numWLocationID = WItems.numWLocationID
        LEFT JOIN OpportunityMaster M ON Opp.numSourceID = M.numOppID
        LEFT JOIN UOM u ON u.numUOMId = opp.numUOMId
		LEFT JOIN Vendor V ON I.numVendorID = V.numVendorID  AND V.numItemCode = I.numItemCode
WHERE   om.numDomainId = '+ CONVERT(VARCHAR(10),@numDomainId) + '
        AND Opp.numOppId = '+ CONVERT(VARCHAR(15),@OpportunityId) +'
ORDER BY opp.numSortOrder,numoppitemtCode ASC '
  
PRINT @strSQL;
EXEC (@strSQL);
SELECT  vcSerialNo,
       vcComments AS Comments,
        numOppItemID AS numoppitemtCode,
        W.numWarehouseItmsDTLID,
        numWarehouseItmsID AS numWItmsID,
        dbo.Fn_getattributes(W.numWarehouseItmsDTLID,
                             1) Attributes
FROM    WareHouseItmsDTL W
        JOIN OppWarehouseSerializedItem O ON O.numWarehouseItmsDTLID = W.numWareHouseItmsDTLID
WHERE   numOppID = @OpportunityId


select * from #tempForm order by tintOrder

drop table #tempForm

            END
            
    END
/****** Object:  StoredProcedure [dbo].[USP_OppManage]    Script Date: 07/26/2008 16:20:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OppManage')
DROP PROCEDURE usp_oppmanage
GO
CREATE PROCEDURE [dbo].[USP_OppManage] 
(                                                                          
  @numOppID as numeric=0 OUTPUT,                                                                          
  @numContactId numeric(9)=null,                                                                          
  @numDivisionId numeric(9)=null,                                                                          
  @tintSource numeric(9)=null,                                                                          
  @vcPOppName Varchar(100)='' OUTPUT,                                                                          
  @Comments varchar(1000)='',                                                                          
  @bitPublicFlag bit=0,                                                                          
  @numUserCntID numeric(9)=0,                                                                                 
  @monPAmount DECIMAL(20,5) =0,                                                 
  @numAssignedTo as numeric(9)=0,                                                                                                        
  @numDomainId numeric(9)=0,                                                                                                                                                                                   
  @strItems as text=null,                                                                          
  @strMilestone as text=null,                                                                          
  @dtEstimatedCloseDate datetime,                                                                          
  @CampaignID as numeric(9)=null,                                                                          
  @lngPConclAnalysis as numeric(9)=null,                                                                         
  @tintOppType as tinyint,                                                                                                                                             
  @tintActive as tinyint,                                                              
  @numSalesOrPurType as numeric(9),              
  @numRecurringId as numeric(9),
  @numCurrencyID as numeric(9)=0,
  @DealStatus as tinyint =0,-- 0=Open,1=Won,2=Lost
  @numStatus AS NUMERIC(18,0),
  @vcOppRefOrderNo VARCHAR(100),
  @numBillAddressId numeric(9)=0,
  @numShipAddressId numeric(9)=0,
  @bitStockTransfer BIT=0,
  @WebApiId INT = 0,
  @tintSourceType TINYINT=0,
  @bitDiscountType as bit,
  @fltDiscount  as float,
  @bitBillingTerms as bit,
  @intBillingDays as integer,
  @bitInterestType as bit,
  @fltInterest as float,
  @tintTaxOperator AS TINYINT,
  @numDiscountAcntType AS NUMERIC(9),
  @vcWebApiOrderNo VARCHAR(100)=NULL,
  @vcCouponCode		VARCHAR(100) = '',
  @vcMarketplaceOrderID VARCHAR(100)=NULL,
  @vcMarketplaceOrderDate  datetime=NULL,
  @vcMarketplaceOrderReportId VARCHAR(100)=NULL,
  @numPercentageComplete NUMERIC(9),
  @bitUseShippersAccountNo BIT = 0,
  @bitUseMarkupShippingRate BIT = 0,
  @numMarkupShippingRate NUMERIC(19,2) = 0,
  @intUsedShippingCompany INT = 0,
  @numShipmentMethod NUMERIC(18,0)=0,
  @dtReleaseDate DATE = NULL,
  @numPartner NUMERIC(18,0)=0,
  @tintClickBtn INT=0,
  @numPartenerContactId NUMERIC(18,0)=0,
  @numAccountClass NUMERIC(18,0) = 0,
  @numWillCallWarehouseID NUMERIC(18,0) = 0,
  @numVendorAddressID NUMERIC(18,0) = 0,
  @numShipFromWarehouse NUMERIC(18,0) = 0
)                                                                          
                                                                          
as  
BEGIN TRY
BEGIN TRANSACTION
	DECLARE @fltExchangeRate float                                 
	DECLARE @hDocItem int                                                                                                                            
	DECLARE @TotalAmount as DECIMAL(20,5)                                                                          
	DECLARE @tintOppStatus as tinyint
	DECLARE @bitNewOrder AS BIT = (CASE WHEN ISNULL(@numOppID,0) = 0 THEN 1 ELSE 0 END)
	
	IF ISNULL(@numContactId,0) = 0
	BEGIN
		RAISERROR('CONCAT_REQUIRED',16,1)
		RETURN
	END  
	
	IF ISNULL(@tintOppType,0) = 0
	BEGIN
		RAISERROR('OPP_TYPE_CAN_NOT_BE_0',16,1)
		RETURN
	END             

	IF CONVERT(VARCHAR(10),@strItems) <> ''
	BEGIN
		EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems

		--VALIDATE PROMOTION OF COUPON CODE
		DECLARE @TEMP TABLE
		(
			ID INT IDENTITY(1,1),
			numOppItemID NUMERIC(18,0),
			numPromotionID NUMERIC(18,0)
		)

		INSERT INTO @TEMP
		(
			numOppItemID,
			numPromotionID
		)
		SELECT
			numoppitemtCode,
			numPromotionID
		FROM
			OPENXML (@hDocItem,'/NewDataSet/Item',2)
		WITH
		(
			numoppitemtCode NUMERIC(18,0),
			numPromotionID NUMERIC(18,0)
		)

		IF (SELECT
				COUNT(*)
			FROM
				PromotionOffer PO
			WHERE
				numDomainId=@numDomainId
				AND bitRequireCouponCode = 1
				AND ISNULL(bitEnabled,0) = 1
				AND ISNULL(tintUsageLimit,0) > 0
				AND (intCouponCodeUsed 
					- (CASE WHEN ISNULL((SELECT COUNT(*) FROM OpportunityItems WHERE numOppID=ISNULL(@numOppID,0) AND numPromotionID=PO.numProId),0) > 0 THEN 1 ELSE 0 END) 
					+ (CASE WHEN ISNULL((SELECT COUNT(*) FROM @TEMP WHERE numPromotionID=PO.numProId),0) > 0 THEN 1 ELSE 0 END)) > tintUsageLimit) > 0
		BEGIN
			RAISERROR('USAGE_OF_COUPON_EXCEED',16,1)
			RETURN
		END
		ELSE
		BEGIN
			UPDATE
				PO
			SET 
				intCouponCodeUsed = ISNULL(intCouponCodeUsed,0) 
									- (CASE WHEN ISNULL((SELECT COUNT(*) FROM OpportunityItems WHERE numOppID=ISNULL(@numOppID,0) AND numPromotionID=PO.numProId),0) > 0 THEN 1 ELSE 0 END) 
									+ (CASE WHEN ISNULL(T1.intUsed,0) > 0 THEN 1 ELSE 0 END) 
			FROM
				PromotionOffer PO
			INNER JOIN
				(
					SELECT
						numPromotionID,
						COUNT(*) AS intUsed
					FROM
						@TEMP
					GROUP BY
						numPromotionID
				) AS T1
			ON
				PO.numProId = T1.numPromotionID
			WHERE
				numDomainId=@numDomainId
				AND bitRequireCouponCode = 1
				AND ISNULL(bitEnabled,0) = 1
		END
	END

	--IF NEW OPPERTUNITY
	IF @numOppID = 0                                                                          
	BEGIN
		DECLARE @bitIsInitalSalesOrder BIT
		IF(@DealStatus=1 AND @tintOppType=1)
		BEGIN
			SET @bitIsInitalSalesOrder=1
		END

		DECLARE @intOppTcode AS NUMERIC(9)  
  
		SELECT @intOppTcode = MAX(numOppId) FROM OpportunityMaster                

		SET @intOppTcode = ISNULL(@intOppTcode,0) + 1                                                
		
		SET @vcPOppName = @vcPOppName + '-'+ CONVERT(varchar(4),YEAR(GETUTCDATE()))  +'-A'+ CONVERT(varchar(10),@intOppTcode)                                  
  
		IF ISNULL(@numCurrencyID,0) = 0 
		BEGIN
			SET @fltExchangeRate=1
			SELECT @numCurrencyID= ISNULL(numCurrencyID,0) FROM dbo.Domain WHERE numDomainId=@numDomainId	
		END
		ELSE
		BEGIN 
			SET @fltExchangeRate = dbo.GetExchangeRate(@numDomainID,@numCurrencyID)
        END  
		                                                        
		IF ISNULL(@numAccountClass,0) = 0
		BEGIN                                                  
		DECLARE @tintDefaultClassType AS INT = 0
		SELECT @tintDefaultClassType = ISNULL(tintDefaultClassType,0) FROM Domain WHERE numDomainId = @numDomainID 

		IF @tintDefaultClassType = 1 --USER
		BEGIN
			SELECT 
				@numAccountClass=ISNULL(numDefaultClass,0) 
			FROM 
				dbo.UserMaster UM 
			JOIN 
				dbo.Domain D 
			ON 
				UM.numDomainID=D.numDomainId
			WHERE 
				D.numDomainId=@numDomainId 
				AND UM.numUserDetailId=@numUserCntID
		END
		ELSE IF @tintDefaultClassType = 2 --COMPANY
		BEGIN
			SELECT 
				@numAccountClass=ISNULL(numAccountClassID,0) 
			FROM 
				dbo.DivisionMaster DM 
			WHERE 
				DM.numDomainId=@numDomainId 
				AND DM.numDivisionID=@numDivisionID
		END
		ELSE
		BEGIN
			SET @numAccountClass = 0
		END
		END

        IF(@DealStatus=1 AND @tintOppType=1 AND @tintClickBtn=1)
		BEGIN
			SET @DealStatus=0
		END         
		--PRINT ('Deal Staus ' +@DealStatus)                                                      
		INSERT INTO OpportunityMaster
		(                                                                             
			numContactId,numDivisionId,txtComments,numCampainID,bitPublicFlag,tintSource,tintSourceType,vcPOppName,                                                                          
			intPEstimatedCloseDate,monPAmount,numCreatedBy,bintCreatedDate,numModifiedBy,bintModifiedDate,numDomainId,                                                                                                             
			numRecOwner,lngPConclAnalysis,tintOppType,numSalesOrPurType,numCurrencyID,fltExchangeRate,numAssignedTo,
			numAssignedBy,[tintOppStatus],numStatus,vcOppRefOrderNo,bitStockTransfer,bitBillingTerms,intBillingDays,
			bitInterestType,fltInterest,vcCouponCode,bitDiscountType,fltDiscount,bitPPVariance,vcMarketplaceOrderID,
			vcMarketplaceOrderReportId,numPercentageComplete,numAccountClass,tintTaxOperator,bitUseShippersAccountNo,
			bitUseMarkupShippingRate,numMarkupShippingRate,intUsedShippingCompany,numShipmentMethod,numPartner,numPartenerContact,dtReleaseDate,bitIsInitalSalesOrder,numVendorAddressID,numShipFromWarehouse
		)                                                                          
		Values                                                                          
		(                                                                          
			@numContactId,@numDivisionId,ISNULL(@Comments,''),@CampaignID,@bitPublicFlag,@tintSource,@tintSourceType,@vcPOppName,                                                                          
			@dtEstimatedCloseDate,@monPAmount,@numUserCntID,(CASE @tintSourceType WHEN 3 THEN @vcMarketplaceOrderDate ELSE GETUTCDATE() END),                                                                          
			@numUserCntID,GETUTCDATE(),@numDomainId,@numUserCntID,@lngPConclAnalysis,@tintOppType,@numSalesOrPurType,@numCurrencyID, 
			@fltExchangeRate,(CASE WHEN @numAssignedTo > 0 THEN @numAssignedTo ELSE NULL END),(CASE WHEN @numAssignedTo>0 THEN @numUserCntID ELSE NULL END),
			@DealStatus,@numStatus,@vcOppRefOrderNo,@bitStockTransfer,@bitBillingTerms,@intBillingDays,@bitInterestType,@fltInterest,
			@vcCouponCode,@bitDiscountType,@fltDiscount,1,@vcMarketplaceOrderID,@vcMarketplaceOrderReportId,@numPercentageComplete,
			@numAccountClass,@tintTaxOperator,@bitUseShippersAccountNo,@bitUseMarkupShippingRate,@numMarkupShippingRate,@intUsedShippingCompany,@numShipmentMethod,@numPartner,@numPartenerContactId,@dtReleaseDate,@bitIsInitalSalesOrder,@numVendorAddressID,@numShipFromWarehouse
		)                                                                                                                      
		
		SET @numOppID=(SELECT SCOPE_IDENTITY())                
                            
		
		--UPDATE OPPNAME AS PER NAME TEMPLATE
		EXEC dbo.USP_UpdateNameTemplateValue @tintOppType,@numDomainID,@numOppID

		--MAP CUSTOM FIELD	
		DECLARE @tintPageID AS TINYINT;
		SET @tintPageID = CASE WHEN @tintOppType=1 THEN 2 ELSE 6 END 
  	
		EXEC dbo.USP_AddParentChildCustomFieldMap
				@numDomainID = @numDomainID, --  numeric(18, 0)
				@numRecordID = @numOppID, --  numeric(18, 0)
				@numParentRecId = @numDivisionId, --  numeric(18, 0)
				@tintPageID = @tintPageID --  tinyint
 	
		IF ISNULL(@numStatus,0) > 0 AND @tintOppType=1 AND isnull(@DealStatus,0)=1
		BEGIN
	   		EXEC USP_ManageOpportunityAutomationQueue
					@numOppQueueID = 0, -- numeric(18, 0)
					@numDomainID = @numDomainID, -- numeric(18, 0)
					@numOppId = @numOppID, -- numeric(18, 0)
					@numOppBizDocsId = 0, -- numeric(18, 0)
					@numOrderStatus = @numStatus, -- numeric(18, 0)
					@numUserCntID = @numUserCntID, -- numeric(18, 0)
					@tintProcessStatus = 1, -- tinyint
					@tintMode = 1 -- TINYINT
		END
  
		-- INSERTING ITEMS                                                                                                                    
		IF CONVERT(VARCHAR(10),@strItems) <> '' AND @numOppID > 0
		BEGIN
			EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems       
	
			INSERT INTO OpportunityItems
			(
				numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,bitDropShip,bitDiscountType,fltDiscount,
				monTotAmtBefDiscount,vcItemName,vcModelID,vcManufacturer,vcPathForTImage,monVendorCost,numUOMId,bitWorkOrder,numVendorWareHouse,
				numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,numClassID,numToWarehouseItemID,vcAttributes,vcAttrValues,monAvgCost,bitItemPriceApprovalRequired,
				numPromotionID,bitPromotionTriggered,vcPromotionDetail,numSortOrder,numCost,vcNotes
			)
			SELECT 
				@numOppID,X.numItemCode,dbo.fn_UOMConversion(X.numUOM,numItemCode,@numDomainId,null) * x.numUnitHour,x.monPrice,x.monTotAmount,X.vcItemDesc,
				(CASE X.numWarehouseItmsID 
				WHEN -1 
				THEN 
					CASE 
						WHEN ((SELECT COUNT(*) FROM Item WHERE numDomainID=@numDomainId AND vcSKU= X.vcSKU) > 0)
							THEN
								(SELECT TOP 1 
									numWareHouseItemID 
								FROM 
									WareHouseItems 
								WHERE 
									[numItemID] = X.numItemCode 
									AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId))
						ELSE
							(SELECT 
								[numWareHouseItemID] 
							FROM 
								[WareHouseItems] 
							WHERE 
								[numItemID] = X.numItemCode 
								AND ISNULL(WareHouseItems.vcWHSKU,'') = ISNULL(X.vcSKU,'')
								AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId)) 
					END
				WHEN 0 
				THEN 
					CASE 
					WHEN ((SELECT COUNT(*) FROM Item WHERE numDomainID=@numDomainId AND vcSKU=X.vcSKU) > 0)
						THEN
							(SELECT TOP 1 
								numWareHouseItemID 
							FROM 
								WareHouseItems 
							WHERE 
								[numItemID] = X.numItemCode 
								AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId))
					ELSE
						(SELECT 
							[numWareHouseItemID] 
						FROM 
							[WareHouseItems] 
						WHERE 
							[numItemID] = X.numItemCode 
							AND ISNULL(WareHouseItems.vcWHSKU,'') = ISNULL(X.vcSKU,'')
							AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId)) 
					END
				ELSE  
					X.numWarehouseItmsID 
				END) AS numWarehouseItmsID,
				X.ItemType,X.DropShip,X.bitDiscountType,X.fltDiscount,X.monTotAmtBefDiscount,X.vcItemName,(SELECT vcModelID FROM item WHERE numItemCode = X.numItemCode),
				(SELECT vcManufacturer FROM item WHERE numItemCode = X.numItemCode),(SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = X.numItemCode AND BitDefault = 1 AND numdomainId = @numdomainId),
				(SELECT isnull(VN.monCost,0) FROM Vendor VN INNER JOIN Item IT ON IT.numItemCode=VN.numItemCode AND VN.numVendorID=IT.numVendorID WHERE  VN.numItemCode=X.numItemCode),
				X.numUOM,X.bitWorkOrder,X.numVendorWareHouse,X.numShipmentMethod,X.numSOVendorId,X.numProjectID,X.numProjectStageID,
				0,
				X.numToWarehouseItemID,X.Attributes,X.AttributeIDs,(SELECT (CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(I.monAverageCost,0) END) FROM Item I WHERE I.numDomainId=@numDomainId AND I.numItemCode=X.numItemCode),
				X.bitItemPriceApprovalRequired,X.numPromotionID,X.bitPromotionTriggered,X.vcPromotionDetail,X.numSortOrder,ISNULL(X.numCost,0),ISNULL(X.vcVendorNotes,'')
			FROM
				(
					SELECT 
						*
					FROM 
						OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
					WITH                       
					(                                                                          
						numoppitemtCode NUMERIC(9),numItemCode NUMERIC(9), numUnitHour FLOAT, monPrice DECIMAL(30,16), monTotAmount DECIMAL(20,5),vcItemDesc VARCHAR(1000),                                    
						numWarehouseItmsID NUMERIC(9), ItemType VARCHAR(30), DropShip BIT, bitDiscountType BIT,	fltDiscount DECIMAL(30,16), monTotAmtBefDiscount DECIMAL(20,5),
						vcItemName VARCHAR(300),numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId NUMERIC(9),
						numProjectID numeric(9),numProjectStageID numeric(9),numToWarehouseItemID NUMERIC(9),Attributes varchar(500),AttributeIDs VARCHAR(500),vcSKU VARCHAR(100),bitItemPriceApprovalRequired BIT,
						numPromotionID NUMERIC(18,0),bitPromotionTriggered BIT,vcPromotionDetail VARCHAR(2000),numSortOrder NUMERIC(18,0),numCost DECIMAL(20,5),vcVendorNotes VARCHAR(1000)
					)
				)X    
			ORDER BY 
				X.numoppitemtCode
    
			UPDATE 
				OpportunityItems                       
			SET 
				numItemCode=X.numItemCode,numOppId=@numOppID,numUnitHour=dbo.fn_UOMConversion(X.numUOM,X.numItemCode,@numDomainId,null) * X.numUnitHour,                      
				monPrice=x.monPrice,monTotAmount=x.monTotAmount,vcItemDesc=X.vcItemDesc,numWarehouseItmsID=X.numWarehouseItmsID,bitDropShip=X.DropShip,
				bitDiscountType=X.bitDiscountType,fltDiscount=X.fltDiscount,monTotAmtBefDiscount=X.monTotAmtBefDiscount,vcItemName=X.vcItemName,numUOMId=numUOM,
				bitWorkOrder=X.bitWorkOrder,numVendorWareHouse=X.numVendorWareHouse,numShipmentMethod=X.numShipmentMethod,numSOVendorId=X.numSOVendorId,
				numProjectId=X.numProjectID,numProjectStageId=X.numProjectStageID,numToWarehouseItemID = X.numToWarehouseItemID,vcAttributes=X.Attributes,
				numClassID=0,vcModelID=X.vcModelID,
				bitItemPriceApprovalRequired = X.bitItemPriceApprovalRequired,vcAttrValues=X.AttributeIDs,
				numPromotionID=X.numPromotionID,bitPromotionTriggered=X.bitPromotionTriggered,vcPromotionDetail=X.vcPromotionDetail,numSortOrder=X.numSortOrder,numCost=X.numCost,vcNotes=X.vcVendorNotes
			FROM 
			(
				SELECT 
					numoppitemtCode as numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,vcModelID,numWarehouseItmsID,DropShip,bitDiscountType,fltDiscount,
					monTotAmtBefDiscount,vcItemName,numUOM,bitWorkOrder,numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,numToWarehouseItemID,
					Attributes,bitItemPriceApprovalRequired,AttributeIDs,numPromotionID,bitPromotionTriggered,vcPromotionDetail,numSortOrder,ISNULL(numCost,0) numCost,ISNULL(vcVendorNotes,'') vcVendorNotes
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=2]',2)
				WITH  
					(                      
						numoppitemtCode NUMERIC(9),numItemCode NUMERIC(9),numUnitHour FLOAT,monPrice DECIMAL(30,16),monTotAmount DECIMAL(20,5),vcItemDesc VARCHAR(1000), vcModelID VARCHAR(300),
						numWarehouseItmsID NUMERIC(9),DropShip BIT,bitDiscountType BIT,fltDiscount DECIMAL(30,16),monTotAmtBefDiscount DECIMAL(20,5),vcItemName VARCHAR(300),
						numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId NUMERIC(9),numProjectID NUMERIC(9),
						numProjectStageID numeric(9),numToWarehouseItemID NUMERIC(9),Attributes varchar(500),bitItemPriceApprovalRequired BIT,AttributeIDs VARCHAR(500),
						numPromotionID NUMERIC(18,0),bitPromotionTriggered BIT,vcPromotionDetail VARCHAR(2000),numSortOrder NUMERIC(18,0),numCost DECIMAL(20,5),vcVendorNotes VARCHAR(1000)
					)
			)X 
			WHERE 
				numoppitemtCode=X.numOppItemID AND numOppID=@numOppID
				
			--INSERT KIT ITEMS                 
			INSERT INTO OpportunityKitItems
			(
				numOppId,
				numOppItemID,
				numChildItemID,
				numWareHouseItemId,
				numQtyItemsReq,
				numQtyItemsReq_Orig,
				numUOMId,
				numQtyShipped,
				monAvgCost
			)
			SELECT
				@numOppId,
				OI.numoppitemtCode,
				ID.numChildItemID,
				ID.numWareHouseItemId,
				(ID.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(ID.numUOMId,ID.numChildItemID,@numDomainID,I.numBaseUnit),1)) * OI.numUnitHour,
				ID.numQtyItemsReq,
				ID.numUOMId,
				0,
				ISNULL(I.monAverageCost,0)
			FROM 
				OpportunityItems OI 
			JOIN 
				ItemDetails ID 
			ON 
				OI.numItemCode=ID.numItemKitID 
			JOIN
				Item I
			ON
				ID.numChildItemID = I.numItemCode
			WHERE 
				OI.numOppId=@numOppId 

			IF (SELECT COUNT(*) FROM OPENXML (@hDocItem,'/NewDataSet/Item[bitHasKitAsChild=''true'']',2)) > 0
			BEGIN
				--INSERT KIT ITEM HAS ANOTHER KIT AS CHILD THEN SAVE CONFIGURATION OF KIT
				DECLARE @TempKitConfiguration TABLE
				(
					numItemCode NUMERIC(18,0),
					numWarehouseItemID NUMERIC(18,0),
					KitChildItems VARCHAR(MAX)
				)

				INSERT INTO 
					@TempKitConfiguration
				SELECT
					numItemCode,
					numWarehouseItmsID,
					KitChildItems
				FROM
					OPENXML (@hDocItem,'/NewDataSet/Item[bitHasKitAsChild=''true'']',2)
				WITH                       
				(
					numItemCode NUMERIC(9),numWarehouseItmsID NUMERIC(9), KitChildItems VARCHAR(MAX)
				) 

				INSERT INTO OpportunityKitChildItems
				(
					numOppID,
					numOppItemID,
					numOppChildItemID,
					numItemID,
					numWareHouseItemId,
					numQtyItemsReq,
					numQtyItemsReq_Orig,
					numUOMId,
					numQtyShipped,
					monAvgCost
				)
				SELECT 
					@numOppID,
					OKI.numOppItemID,
					OKI.numOppChildItemID,
					ItemDetails.numChildItemID,
					ItemDetails.numWareHouseItemId,
					(ItemDetails.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(ItemDetails.numUOMId,ItemDetails.numChildItemID,@numDomainID,Item.numBaseUnit),1)) * OKI.numQtyItemsReq,
					ItemDetails.numQtyItemsReq,
					ItemDetails.numUOMId,
					0,
					ISNULL(Item.monAverageCost,0)
				FROM
					(
						SELECT 
							t1.numItemCode,
							t1.numWarehouseItemID,
							SUBSTRING(
										SUBSTRING(items,0,CHARINDEX('-',items)),
										0,
										CHARINDEX('#',SUBSTRING(items,0,CHARINDEX('-',items)))
									  ) AS numKitItemID,
							SUBSTRING(
										SUBSTRING(items,0,CHARINDEX('-',items)),
										CHARINDEX('#',SUBSTRING(items,0,CHARINDEX('-',items))) + 1, 
										LEN(SUBSTRING(items,0,CHARINDEX('-',items))) - CHARINDEX('#',SUBSTRING(items,0,CHARINDEX('-',items)))
									  ) AS numKitWarehouseItemID,
							SUBSTRING(
										SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)), 
										0, 
										CHARINDEX('#',SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)))
									  ) As numKitChildItemID,
							SUBSTRING(
										SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)), 
										CHARINDEX('#',SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items))) + 1, 
										LEN(SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items))) - CHARINDEX('#',SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)))
									  ) As numKitChildItemWarehouseItemID
						FROM 
							@TempKitConfiguration t1
						CROSS APPLY
						(
							SELECT
								*
							FROM 
								dbo.Split((SELECT KitChildItems FROM @TempKitConfiguration WHERE numItemCode=t1.numItemCode AND numWarehouseItemID=t1.numWarehouseItemID),',') 
						) as t2
					) TempChildItems
				INNER JOIN
					OpportunityKitItems OKI
				ON
					OKI.numOppId=@numOppId
					AND OKI.numChildItemID=TempChildItems.numKitItemID
					AND OKI.numWarehouseItemId=TempChildItems.numKitWarehouseItemID
				INNER JOIN
					OpportunityItems OI
				ON
					OI.numItemCode = TempChildItems.numItemCode
					AND OI.numWarehouseItmsID = TempChildItems.numWarehouseItemID
					AND OI.numoppitemtcode = OKI.numOppItemID
				INNER JOIN
					ItemDetails
				ON
					TempChildItems.numKitItemID = ItemDetails.numItemKitID
					AND TempChildItems.numKitChildItemID = ItemDetails.numChildItemID
					AND (TempChildItems.numKitChildItemWarehouseItemID = ItemDetails.numWareHouseItemId  OR ISNULL(TempChildItems.numKitChildItemWarehouseItemID,0) = 0)
				INNER JOIN
					Item 
				ON
					ItemDetails.numChildItemID = Item.numItemCode
			END

			-- UPDATE UOM OF OPPORTUNITY ITEMS HAVE NOT UOM SET WHILE EXPORTED FROM MARKETPLACE (MEANS TINTSOURCETYPE = 3)
			IF ISNULL(@tintSourceType,0) = 3
			BEGIN
				UPDATE 
					OI 
				SET 
					numUOMID = ISNULL(I.numSaleUnit,0), 
					numUnitHour = dbo.fn_UOMConversion(ISNULL(I.numSaleUnit,0),OI.numItemCode,@numDomainId,null) * OI.numUnitHour
				FROM 
					OpportunityItems OI
				JOIN 
					Item I 
				ON 
					OI.numItemCode = I.numItemCode
				WHERE 
					numOppId = @numOppID
					AND I.numDomainID = @numDomainId
			END	
                                     
			INSERT INTO OppWarehouseSerializedItem
			(
				numOppID,
				numWarehouseItmsDTLID,
				numOppItemID,
				numWarehouseItmsID
			)                      
			SELECT 
				@numOppID,
				X.numWarehouseItmsDTLID,
				X.numoppitemtCode,
				X.numWItmsID 
			FROM
			(                                                                          
				SELECT
					* 
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/SerialNo',2)                                                                          
				WITH                        
				(                                               
				numWarehouseItmsDTLID numeric(9),                      
				numoppitemtCode numeric(9),                      
				numWItmsID numeric(9)                                                     
				)
			)X                                    
                                   
			UPDATE 
				OppWarehouseSerializedItem                       
			SET 
				numOppItemID=X.numoppitemtCode                                
			FROM                       
			(
				SELECT 
					numWarehouseItmsDTLID AS DTLID,
					O.numoppitemtCode 
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/SerialNo',2)                                                                         
				WITH
				(
					numWarehouseItmsDTLID NUMERIC(9),numoppitemtCode NUMERIC(9),numWItmsID NUMERIC(9)
				)                                
				JOIN 
					OpportunityItems O 
				ON 
					O.numWarehouseItmsID=numWItmsID and O.numOppID=@numOppID
			)X                 
			WHERE 
				numOppID=@numOppID 
				AND numWarehouseItmsDTLID=X.DTLID      
    
			EXEC USP_ManageSOWorkOrder @numDomainId,@numOppID,@strItems,@numUserCntID
                     
			EXEC sp_xml_removedocument @hDocItem                                     
		END          
	END
	ELSE                                                                                                                          
	BEGIN
		DECLARE @tempAssignedTo AS NUMERIC(9) = NULL                                                    
		SELECT 
			@tintOppStatus = tintOppStatus,
			@tempAssignedTo=ISNULL(numAssignedTo,0) 
		FROM   
			OpportunityMaster 
		WHERE  
			numOppID = @numOppID

		IF @tempAssignedTo <> @numAssignedTo
		BEGIN
			IF (SELECT COUNT(*) FROM BizDocComission WHERE ISNULL(numOppID,0) = @numOppID AND ISNULL(bitCommisionPaid,0) = 1) > 0
			BEGIN
				RAISERROR('COMMISSION_PAID_AGAINST_BIZDOC',16,1)
			END
		END
 
		--REVERTING BACK THE WAREHOUSE ITEMS                  
		IF @tintOppStatus = 1 
		BEGIN               
			EXEC USP_RevertDetailsOpp @numOppID,0,@numUserCntID                  
		END                  

		IF LEN(ISNULL(@vcOppRefOrderNo,''))>0
		BEGIN
			UPDATE dbo.OpportunityBizDocs SET vcRefOrderNo=@vcOppRefOrderNo WHERE numOppId=@numOppID
		END
		
		IF EXISTS (SELECT 1 FROM dbo.OpportunityMaster WHERE numOppId = @numOppID AND tintOppType=1 AND isnull(tintOppStatus,0)=1 AND ISNULL(numStatus,0)!= @numStatus)
		BEGIN
			EXEC USP_ManageOpportunityAutomationQueue
					@numOppQueueID = 0, -- numeric(18, 0)
					@numDomainID = @numDomainID, -- numeric(18, 0)
					@numOppId = @numOppID, -- numeric(18, 0)
					@numOppBizDocsId = 0, -- numeric(18, 0)
					@numOrderStatus = @numStatus, -- numeric(18, 0)
					@numUserCntID = @numUserCntID, -- numeric(18, 0)
					@tintProcessStatus = 1, -- tinyint
					@tintMode = 1 -- TINYINT
		END 
						
		UPDATE   
			OpportunityMaster
		SET      
			vcPOppName = @vcPOppName,txtComments = ISNULL(@Comments,''),bitPublicFlag = @bitPublicFlag,numCampainID = @CampaignID,tintSource = @tintSource,tintSourceType=@tintSourceType,
			intPEstimatedCloseDate = @dtEstimatedCloseDate,numModifiedBy = @numUserCntID,bintModifiedDate = GETUTCDATE(),lngPConclAnalysis = @lngPConclAnalysis,monPAmount = @monPAmount,
			tintActive = @tintActive,numSalesOrPurType = @numSalesOrPurType,numStatus = @numStatus,vcOppRefOrderNo=@vcOppRefOrderNo,
			bintOppToOrder=(CASE WHEN @tintOppStatus=0 AND @DealStatus=1 THEN GETUTCDATE() ELSE bintOppToOrder END),bitDiscountType=@bitDiscountType,fltDiscount=@fltDiscount,
			bitBillingTerms=@bitBillingTerms,intBillingDays=@intBillingDays,bitInterestType=@bitInterestType,fltInterest=@fltInterest,tintTaxOperator=@tintTaxOperator,
			numDiscountAcntType=@numDiscountAcntType,vcCouponCode = @vcCouponCode,numPercentageComplete =@numPercentageComplete,bitUseShippersAccountNo = @bitUseShippersAccountNo,
			bitUseMarkupShippingRate = @bitUseMarkupShippingRate,numMarkupShippingRate = @numMarkupShippingRate,intUsedShippingCompany = @intUsedShippingCompany, numContactID=@numContactId,numShipmentMethod=@numShipmentMethod,numPartner=@numPartner,numPartenerContact=@numPartenerContactId
		WHERE
			numOppId = @numOppID   
	   
		-- UPDATING IF ORGANIZATION IS ASSIGNED TO SOMEONE                                                      
		IF ( @tempAssignedTo <> @numAssignedTo AND @numAssignedTo <> '0' ) 
		BEGIN
			UPDATE
				OpportunityMaster 
			SET	
				numAssignedTo = @numAssignedTo, 
				numAssignedBy = @numUserCntID 
			WHERE 
				numOppId = @numOppID                                                    
		END                                                     
		ELSE IF(@numAssignedTo = 0) 
		BEGIN
			UPDATE 
				OpportunityMaster 
			SET 
				numAssignedTo = 0, 
				numAssignedBy = 0 
			WHERE
				numOppId = @numOppID
		END  
			                                                                    
		-- UPDATING OPP ITEMS
		IF CONVERT(VARCHAR(10),@strItems) <> '' AND @numOppID > 0                                                                         
		BEGIN
			EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems                                      
			   
			IF EXISTS 
			(
				SELECT 
					OI.numoppitemtCode
				FROM 
					OpportunityItems OI
				WHERE 
					numOppID=@numOppID 
					AND OI.numoppitemtCode NOT IN (SELECT numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) WITH (numoppitemtCode NUMERIC(9)))
					AND ISNULL((SELECT COUNT(*) FROM OppWarehouseSerializedItem WHERE numOppId=@numOppID AND numOppItemID=OI.numoppitemtCode),0) > 0
			)
			BEGIN
				RAISERROR('SERIAL/LOT#_ASSIGNED_TO_ITEM_DELETED',16,1)
			END

			--DELETE CHILD ITEMS OF KIT WITHIN KIT
			DELETE FROM 
				OpportunityKitChildItems 
			WHERE 
				numOppID=@numOppID AND 
				numOppItemID NOT IN (SELECT ISNULL(numoppitemtCode,0) FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) WITH (numoppitemtCode NUMERIC(9)))   

			--DELETE ITEMS
			DELETE FROM 
				OpportunityKitItems 
			WHERE 
				numOppID=@numOppID AND 
				numOppItemID NOT IN (SELECT ISNULL(numoppitemtCode,0) FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) WITH (numoppitemtCode NUMERIC(9)))                      
			                            
			DELETE FROM 
				OpportunityItems 
			WHERE 
				numOppID=@numOppID 
				AND numoppitemtCode NOT IN (SELECT numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) WITH (numoppitemtCode NUMERIC(9)))   
	          
			-- DELETE CORRESPONSING TIME AND EXPENSE ENTRIES OF DELETED ITEMS
			DELETE FROM
				TimeAndExpense
			WHERE
				numDomainID=@numDomainId
				AND numOppId=@numOppID
				AND numOppItemID NOT IN (SELECT numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) WITH (numoppitemtCode NUMERIC(9))) 

			--INSERT NEW ADDED ITEMS                        
			INSERT INTO OpportunityItems
			(
				numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,
				[vcItemName],[vcModelID],[vcManufacturer],vcPathForTImage,monVendorCost,numUOMId,bitWorkOrder,numVendorWareHouse,numToWarehouseItemID,numShipmentMethod,
				numSOVendorId,numProjectID,numProjectStageID,vcAttributes,numClassID,monAvgCost,bitItemPriceApprovalRequired,vcAttrValues,numPromotionID,bitPromotionTriggered,vcPromotionDetail,numSortOrder,numCost,vcNotes
			)
			SELECT 
				@numOppID,X.numItemCode,dbo.fn_UOMConversion(X.numUOM,numItemCode,@numDomainId,null) * x.numUnitHour,x.monPrice,x.monTotAmount,X.vcItemDesc,X.numWarehouseItmsID,
				X.ItemType,X.DropShip,X.bitDiscountType,X.fltDiscount,X.monTotAmtBefDiscount,X.vcItemName,(SELECT vcModelID FROM item WHERE numItemCode = X.numItemCode),
				(SELECT vcManufacturer FROM item WHERE numItemCode = X.numItemCode),
				(SELECT TOP 1 vcPathForTImage FROM dbo.ItemImages WHERE numItemCode = X.numItemCode AND BitDefault = 1 AND numDomainId = @numDomainId),
				(select isnull(VN.monCost,0) from Vendor VN INNER JOIN Item IT ON IT.numItemCode=VN.numItemCode and VN.numVendorID=IT.numVendorID where VN.numItemCode=X.numItemCode),
				X.numUOM,X.bitWorkOrder,X.numVendorWareHouse,X.numToWarehouseItemID,X.numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,Attributes,
				0,
				(SELECT (CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(I.monAverageCost,0) END) FROM Item I WHERE I.numDomainId=@numDomainId AND I.numItemCode=X.numItemCode), X.bitItemPriceApprovalRequired, X.AttributeIDs,
				X.numPromotionID,X.bitPromotionTriggered,X.vcPromotionDetail,X.numSortOrder,ISNULL(X.numCost,0),ISNULL(vcVendorNotes,'')
			FROM
			(
				SELECT 
					*
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
				WITH  
				(                      
					numoppitemtCode numeric(9),numItemCode numeric(9),numUnitHour FLOAT,monPrice DECIMAL(30,16),monTotAmount DECIMAL(20,5),vcItemDesc varchar(1000),                                    
					numWarehouseItmsID numeric(9),numToWarehouseItemID numeric(9),Op_Flag tinyint,ItemType varchar(30),DropShip bit,bitDiscountType bit,
					fltDiscount DECIMAL(30,16),monTotAmtBefDiscount DECIMAL(20,5),vcItemName VARCHAR(300),numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),
					numShipmentMethod NUMERIC(9),numSOVendorId numeric(9),numProjectID numeric(9),numProjectStageID numeric(9),Attributes varchar(500), 
					bitItemPriceApprovalRequired BIT,AttributeIDs VARCHAR(500),numPromotionID NUMERIC(18,0),bitPromotionTriggered BIT,vcPromotionDetail VARCHAR(2000)
					,numSortOrder NUMERIC(18,0),numCost DECIMAL(20,5),vcVendorNotes VARCHAR(1000)
				)
			)X 
			
			-- UPDATE ITEMS
			UPDATE 
				OpportunityItems                       
			SET 
				numItemCode=X.numItemCode,numOppId=@numOppID,numUnitHour=dbo.fn_UOMConversion(X.numUOM,X.numItemCode,@numDomainId,null) * x.numUnitHour,                      
				monPrice=x.monPrice,monTotAmount=x.monTotAmount,vcItemDesc=X.vcItemDesc,vcModelID=X.vcModelID,numWarehouseItmsID=X.numWarehouseItmsID,numToWarehouseItemID = X.numToWarehouseItemID,             
				bitDropShip=X.DropShip,bitDiscountType=X.bitDiscountType,fltDiscount=X.fltDiscount,monTotAmtBefDiscount=X.monTotAmtBefDiscount,vcItemName=X.vcItemName,
				numUOMId=numUOM,numVendorWareHouse=X.numVendorWareHouse,numShipmentMethod=X.numShipmentMethod,numSOVendorId=X.numSOVendorId,numProjectId=X.numProjectID,
				numProjectStageId=X.numProjectStageID,vcAttributes=X.Attributes,bitItemPriceApprovalRequired=X.bitItemPriceApprovalRequired,bitWorkOrder=X.bitWorkOrder,vcAttrValues=AttributeIDs,
				numPromotionID=X.numPromotionID,bitPromotionTriggered=X.bitPromotionTriggered,vcPromotionDetail=X.vcPromotionDetail,numSortOrder=X.numSortOrder,numCost=X.numCost,vcNotes=vcVendorNotes
			FROM 
			(
				SELECT 
					numoppitemtCode as numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,vcModelID,numWarehouseItmsID,numToWarehouseItemID,DropShip,bitDiscountType,
					fltDiscount,monTotAmtBefDiscount,vcItemName,numUOM,numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,Attributes,
					bitItemPriceApprovalRequired,bitWorkOrder,AttributeIDs,numPromotionID,bitPromotionTriggered,vcPromotionDetail,numSortOrder,ISNULL(numCost,0) AS numCost,ISNULL(vcVendorNotes,'') vcVendorNotes
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=2]',2)                                                                
				WITH  
				(                      
					numoppitemtCode NUMERIC(9),numItemCode NUMERIC(9),numUnitHour FLOAT,monPrice DECIMAL(30,16),monTotAmount DECIMAL(20,5),vcItemDesc VARCHAR(1000),vcModelID VARCHAR(300), numWarehouseItmsID NUMERIC(9), 
					numToWarehouseItemID NUMERIC(18,0),DropShip BIT,bitDiscountType BIT,fltDiscount DECIMAL(30,16),monTotAmtBefDiscount DECIMAL(20,5),vcItemName VARCHAR(300),numUOM NUMERIC(9),
					numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId NUMERIC(9),numProjectID NUMERIC(9),numProjectStageID NUMERIC(9),Attributes VARCHAR(500),
					bitItemPriceApprovalRequired BIT, bitWorkOrder BIT,AttributeIDs VARCHAR(500),numPromotionID NUMERIC(18,0),bitPromotionTriggered BIT,vcPromotionDetail VARCHAR(2000),numSortOrder NUMERIC(18,0),numCost DECIMAL(20,5),vcVendorNotes VARCHAR(1000)                           
				)
			)X 
			WHERE 
				numoppitemtCode=X.numOppItemID AND numOppId=@numOppID

			--INSERT NEW ADDED KIT ITEMS                 
			INSERT INTO OpportunityKitItems                                                                          
			(
				numOppId,
				numOppItemID,
				numChildItemID,
				numWareHouseItemId,
				numQtyItemsReq,
				numQtyItemsReq_Orig,
				numUOMId,
				numQtyShipped,
				monAvgCost
			)
			SELECT
				@numOppId,
				OI.numoppitemtCode,
				ID.numChildItemID,
				ID.numWareHouseItemId,
				ID.numQtyItemsReq * OI.numUnitHour,
				ID.numQtyItemsReq,
				ID.numUOMId,
				0,
				ISNULL(I.monAverageCost,0)
			FROM 
				OpportunityItems OI 
			JOIN 
				ItemDetails ID 
			ON 
				OI.numItemCode=ID.numItemKitID
			INNER JOIN
				Item I
			ON
				ID.numChildItemID = I.numItemCode
			WHERE 
				OI.numOppId=@numOppId 
				AND OI.numoppitemtCode NOT IN (SELECT ISNULL(numOppItemID,0) FROM OpportunityKitItems OKI WHERE OKI.numOppId=@numOppID)  
				
			IF (SELECT COUNT(*) FROM OPENXML (@hDocItem,'/NewDataSet/Item[bitHasKitAsChild=''true'']',2)) > 0
			BEGIN
				--INSERT KIT ITEM HAS ANOTHER KIT AS CHILD THEN SAVE CONFIGURATION OF KIT
				DELETE FROM @TempKitConfiguration

				INSERT INTO 
					@TempKitConfiguration
				SELECT
					numItemCode,
					numWarehouseItmsID,
					KitChildItems
				FROM
					OPENXML (@hDocItem,'/NewDataSet/Item[bitHasKitAsChild=''true'']',2)
				WITH                       
				(
					numItemCode NUMERIC(9),numWarehouseItmsID NUMERIC(9), KitChildItems VARCHAR(MAX)
				) 

				INSERT INTO OpportunityKitChildItems
				(
					numOppID,
					numOppItemID,
					numOppChildItemID,
					numItemID,
					numWareHouseItemId,
					numQtyItemsReq,
					numQtyItemsReq_Orig,
					numUOMId,
					numQtyShipped,
					monAvgCost
				)
				SELECT 
					@numOppID,
					OKI.numOppItemID,
					OKI.numOppChildItemID,
					ItemDetails.numChildItemID,
					ItemDetails.numWareHouseItemId,
					ItemDetails.numQtyItemsReq * OKI.numQtyItemsReq,
					ItemDetails.numQtyItemsReq,
					ItemDetails.numUOMId,
					0,
					ISNULL(Item.monAverageCost,0)
				FROM
					(
						SELECT 
							t1.numItemCode,
							t1.numWarehouseItemID,
							SUBSTRING(
										SUBSTRING(items,0,CHARINDEX('-',items)),
										0,
										CHARINDEX('#',SUBSTRING(items,0,CHARINDEX('-',items)))
									  ) AS numKitItemID,
							SUBSTRING(
										SUBSTRING(items,0,CHARINDEX('-',items)),
										CHARINDEX('#',SUBSTRING(items,0,CHARINDEX('-',items))) + 1, 
										LEN(SUBSTRING(items,0,CHARINDEX('-',items))) - CHARINDEX('#',SUBSTRING(items,0,CHARINDEX('-',items)))
									  ) AS numKitWarehouseItemID,
							SUBSTRING(
										SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)), 
										0, 
										CHARINDEX('#',SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)))
									  ) As numKitChildItemID,
							SUBSTRING(
										SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)), 
										CHARINDEX('#',SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items))) + 1, 
										LEN(SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items))) - CHARINDEX('#',SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)))
									  ) As numKitChildItemWarehouseItemID
						FROM 
							@TempKitConfiguration t1
						CROSS APPLY
						(
							SELECT
								*
							FROM 
								dbo.Split((SELECT KitChildItems FROM @TempKitConfiguration WHERE numItemCode=t1.numItemCode AND numWarehouseItemID=t1.numWarehouseItemID),',') 
						) as t2
					) TempChildItems
				INNER JOIN
					OpportunityKitItems OKI
				ON
					OKI.numOppId=@numOppId
					AND OKI.numChildItemID=TempChildItems.numKitItemID
					AND OKI.numWarehouseItemId=TempChildItems.numKitWarehouseItemID
				INNER JOIN
					OpportunityItems OI
				ON
					OI.numItemCode = TempChildItems.numItemCode
					AND OI.numWarehouseItmsID = TempChildItems.numWarehouseItemID
					AND OI.numoppitemtcode = OKI.numOppItemID
				INNER JOIN
					ItemDetails
				ON
					TempChildItems.numKitItemID = ItemDetails.numItemKitID
					AND TempChildItems.numKitChildItemID = ItemDetails.numChildItemID
					AND (TempChildItems.numKitChildItemWarehouseItemID = ItemDetails.numWareHouseItemId  OR ISNULL(TempChildItems.numKitChildItemWarehouseItemID,0) = 0)
				INNER JOIN
					Item 
				ON
					ItemDetails.numChildItemID = Item.numItemCode
			END                                  
			
			--UPDATE KIT ITEMS                 
			UPDATE 
				OKI 
			SET 
				numQtyItemsReq = numQtyItemsReq_Orig * OI.numUnitHour
			FROM 
				OpportunityKitItems OKI 
			JOIN 
				OpportunityItems OI 
			ON 
				OKI.numOppItemID=OI.numoppitemtCode 
				AND OKI.numOppId=OI.numOppId  
			WHERE 
				OI.numOppId=@numOppId 

			-- UPDATE CHILD ITEMS OF KIT WITHIN KIT
			UPDATE
				OKCI
			SET
				OKCI.numQtyItemsReq = (OKI.numQtyItemsReq * OKCI.numQtyItemsReq_Orig)
			FROM
				OpportunityKitChildItems OKCI
			INNER JOIN
				OpportunityKitItems OKI
			ON
				OKCI.numOppID = @numOppId
				AND OKCI.numOppItemID = OKI.numOppItemID
				AND OKCI.numOppChildItemID = OKI.numOppChildItemID                     
			                                    
			EXEC USP_ManageSOWorkOrder @numDomainId,@numOppID,@strItems,@numUserCntID
			   		                                                                      
			EXEC sp_xml_removedocument @hDocItem                                               
		END     

		--FOLLOWING DELETE STATEMENT IS COMMENTED BECUASE NOW WE ARE NOT ALLOWING USER TO EIDT SERIAL/LOT ITEM UNTILL USER REMOVED ASSIGNED SERIAL/LOT NUMBERS
	    --Delete previously added serial lot number added for opportunity
		--DELETE FROM OppWarehouseSerializedItem WHERE numOppID = @numOppID                                                         
	END


	SET @TotalAmount=0                                                           
	SELECT @TotalAmount = SUM(monTotAmount) FROM OpportunityItems WHERE numOppId=@numOppID                                                                          
  
	UPDATE OpportunityMaster SET monPamount=@TotalAmount WHERE numOppId=@numOppID

/*Added by chintan - While creating SO from PO it wasn't puting Item Qty to Allocation. this segment was being called from USP_OppSaveMilestone which No longer in use after changes anoop did. */     
--Updating the warehouse items              
            
			declare @bitApprovalforOpportunity bit
			declare @intOpportunityApprovalProcess int
			SELECT @bitApprovalforOpportunity=bitApprovalforOpportunity,@intOpportunityApprovalProcess=intOpportunityApprovalProcess 
					FROM Domain WHERE numDomainId=@numDomainID
			DECLARE @bitViolatePrice BIT
			DECLARE @intPendingApprovePending INT

			SET @bitIsInitalSalesOrder=(SELECT bitIsInitalSalesOrder FROM OpportunityMaster WHERE numOppId=@numOppID AND numDomainId=@numDomainID)
			--SET @bitViolatePrice=(SELECT bitMarginPriceViolated FROM Domain WHERE numDomainId=@numDomainID)
			IF EXISTS (SELECT numApprovalRuleID FROM ApprovalProcessItemsClassification WHERE numDomainId=@numDomainID AND ISNULL(bitMarginPriceViolated,0) = 1)
			BEGIN
				SET @bitViolatePrice = 1
			END
			
			IF(@bitViolatePrice=1 AND @bitIsInitalSalesOrder=1 AND @tintClickBtn = 1)
			BEGIN
				UPDATE OpportunityMaster SET tintOppStatus=@DealStatus WHERE numOppId=@numOppID
			END
			ELSE
			BEGIN
				IF(@bitApprovalforOpportunity=1)
				BEGIN
					IF(@DealStatus='1' OR @DealStatus='2')
					BEGIN
						IF(@intOpportunityApprovalProcess=1)
						BEGIN
							UPDATE OpportunityMaster SET intPromotionApprovalStatus=6,intChangePromotionStatus=@DealStatus
							WHERE numOppId=@numOppId
						END
						IF(@intOpportunityApprovalProcess=2)
						BEGIN
							UPDATE OpportunityMaster SET intPromotionApprovalStatus=1,intChangePromotionStatus=@DealStatus
							WHERE numOppId=@numOppId
						END
					END
				END

				UPDATE OpportunityMaster SET tintOppStatus = @DealStatus WHERE numOppId=@numOppID
				declare @tintShipped as tinyint               
				select @tintOppStatus=tintOppStatus,@tintOppType=tintOppType,@tintShipped=tintShipped from OpportunityMaster where numOppID=@numOppID              
				if @tintOppStatus=1               
					begin         
					PRINT 'inside USP_UpdatingInventoryonCloseDeal'
						exec USP_UpdatingInventoryonCloseDeal @numOppID,@numUserCntID
					end  
				declare @tintCRMType as numeric(9)        
				declare @AccountClosingDate as datetime        

				select @numDivisionID=numDivisionID,@AccountClosingDate=bintAccountClosingDate from  OpportunityMaster where numOppID=@numOppID         
				select @tintCRMType=tintCRMType from divisionmaster where numDivisionID =@numDivisionID 

				--When deal is won                                         
				IF @DealStatus = 1 
				BEGIN
					IF @AccountClosingDate IS NULL                   
					BEGIN
						UPDATE OpportunityMaster SET bintAccountClosingDate=GETUTCDATE() WHERE numOppID=@numOppID              
					END

					-- Promote Lead to Account when Sales/Purchase Order is created against it
					IF @tintCRMType=0 --Lead & Order
					BEGIN
						UPDATE 
							divisionmaster 
						SET 
							tintCRMType=2
							,bintLeadProm=getutcdate()
							,bintLeadPromBy=@numUserCntID
							,bintProsProm=getutcdate()
							,bintProsPromBy=@numUserCntID        
						WHERE 
							numDivisionID=@numDivisionID        
					END
					--Promote Prospect to Account
					ELSE IF @tintCRMType=1 
					BEGIN
						UPDATE 
							divisionmaster 
						SET 
							tintCRMType=2
							,bintProsProm=getutcdate()
							,bintProsPromBy=@numUserCntID        
						WHERE 
							numDivisionID=@numDivisionID        
					END
				END         
				--When Deal is Lost
				ELSE IF @DealStatus = 2
				BEGIN
					SELECT @AccountClosingDate=bintAccountClosingDate FROM OpportunityMaster WHERE numOppID=@numOppID         
					
					IF @AccountClosingDate IS NULL                   
					BEGIN
						UPDATE OpportunityMaster SET bintAccountClosingDate=GETUTCDATE() WHERE numOppID=@numOppID
					END
				END 
			END

			/*Added by chintan -  Biz usually automatically promotes the record from Lead/Prospect to Account When a Sales Opp/Order is made against it*/  
			-- Promote Lead to Prospect when Sales/Purchase Opp is created against it
			if @tintCRMType=0 AND @DealStatus = 0 --Lead & Open Opp
			begin        
				update divisionmaster set tintCRMType=1,bintLeadProm=getutcdate(),bintLeadPromBy=@numUserCntID,bintProsProm=getutcdate(),bintProsPromBy=@numUserCntID        
				where numDivisionID=@numDivisionID        
			end        

--Add/Update Address Details
DECLARE @vcStreet  varchar(100),@vcCity  varchar(50),@vcPostalCode  varchar(15),@vcCompanyName  varchar(100),@vcAddressName VARCHAR(50), @vcAltContact VARCHAR(200)      
DECLARE @numState  numeric(9),@numCountry  numeric(9), @numCompanyId  numeric(9), @numContact NUMERIC(18,0)
DECLARE @bitIsPrimary BIT, @bitAltContact BIT;
DECLARE @tintAddressOf TINYINT

select  @vcCompanyName=vcCompanyName,@numCompanyId=div.numCompanyID from CompanyInfo Com                            
join divisionMaster Div                            
on div.numCompanyID=com.numCompanyID                            
where div.numdivisionID=@numDivisionId

--Bill Address
IF @numBillAddressId>0
BEGIN
	SELECT 
		@vcStreet=ISNULL(vcStreet,'')
		,@vcCity=ISNULL(vcCity,'')
		,@vcPostalCode=ISNULL(vcPostalCode,'')
		,@numState=ISNULL(numState,0)
		,@numCountry=ISNULL(numCountry,0)
		,@bitIsPrimary=bitIsPrimary
		,@vcAddressName=vcAddressName
		,@tintAddressOf=ISNULL(tintAddressOf,0)
		,@numContact=numContact
		,@bitAltContact=bitAltContact
		,@vcAltContact=vcAltContact
    FROM 
		dbo.AddressDetails 
	WHERE 
		numDomainID = @numDomainID 
		AND numAddressID = @numBillAddressId

	IF @tintAddressOf = 2 AND @tintOppType = 2
	BEGIN
		SELECT
			@vcCompanyName = ISNULL(vcCompanyName,'')
			,@numCompanyId = ISNULL(CompanyInfo.numCompanyId,0)
		FROM
			AddressDetails
		INNER JOIN
			DivisionMaster 
		ON
			AddressDetails.numRecordID = DivisionMaster.numDivisionID
		INNER JOIN
			CompanyInfo
		ON
			DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
		WHERE 
			AddressDetails.numDomainID = @numDomainID 
			AND numAddressID = @numBillAddressId
	END

  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 0, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = @vcCompanyName, --  varchar(100)
	@numCompanyId = 0, --  numeric(9, 0)
	@vcAddressName = @vcAddressName,
	@bitCalledFromProcedure = 1,
	@numContact = @numContact,
	@bitAltContact = @bitAltContact,
	@vcAltContact = @vcAltContact
END
  
  
--Ship Address
IF @numShipmentMethod = 92 -- WILL CALL (PICK UP FROM Warehouse)
BEGIN
	SELECT  
		@vcStreet=ISNULL(vcWStreet,'')
		,@vcCity=ISNULL(vcWCity,'')
		,@vcPostalCode=ISNULL(vcWPinCode,'')
		,@numState=ISNULL(numWState,0)
		,@numCountry=ISNULL(numWCountry,0)
		,@vcAddressName=''
	FROM 
		dbo.Warehouses 
	WHERE 
		numDomainID = @numDomainID 
		AND numWareHouseID = @numWillCallWarehouseID

	UPDATE OpportunityMaster SET tintShipToType=2 WHERE numOppId=@numOppID
 
  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 1, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = '', --  varchar(100)
	@numCompanyId =0, --  numeric(9, 0)
	@vcAddressName = @vcAddressName,
	@bitCalledFromProcedure = 1,
	@numContact = 0,
	@bitAltContact = 0,
	@vcAltContact = ''
END
ELSE IF @numShipAddressId>0
BEGIN
	SELECT
		@vcStreet=ISNULL(vcStreet,'')
		,@vcCity=ISNULL(vcCity,'')
		,@vcPostalCode=ISNULL(vcPostalCode,'')
		,@numState=ISNULL(numState,0)
		,@numCountry=ISNULL(numCountry,0)
		,@bitIsPrimary=bitIsPrimary
		,@vcAddressName=vcAddressName
		,@tintAddressOf=ISNULL(tintAddressOf,0)
		,@numContact=numContact
		,@bitAltContact=bitAltContact
		,@vcAltContact=vcAltContact
    FROM  
		dbo.AddressDetails 
	WHERE 
		numDomainID = @numDomainID 
		AND numAddressID = @numShipAddressId
 

	IF @tintAddressOf = 2 AND @tintOppType = 2
	BEGIN
		SELECT
			@vcCompanyName = ISNULL(vcCompanyName,'')
			,@numCompanyId = ISNULL(CompanyInfo.numCompanyId,0)
		FROM
			AddressDetails
		INNER JOIN
			DivisionMaster 
		ON
			AddressDetails.numRecordID = DivisionMaster.numDivisionID
		INNER JOIN
			CompanyInfo
		ON
			DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
		WHERE 
			AddressDetails.numDomainID = @numDomainID 
			AND numAddressID = @numShipAddressId
	END


  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 1, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = @vcCompanyName, --  varchar(100)
	@numCompanyId =@numCompanyId, --  numeric(9, 0)
	@vcAddressName = @vcAddressName,
	@bitCalledFromProcedure = 1,
	@numContact = @numContact,
	@bitAltContact = @bitAltContact,
	@vcAltContact = @vcAltContact
END

-- IMPORTANT:KEEP THIS BELOW ADDRESS UPDATE LOGIC
-- GET TAX APPLICABLE TO DIVISION WHEN ORDER IS CREATED AFTER THAT NOT NEED TO CHANGE IT
IF @bitNewOrder = 1
BEGIN
	--INSERT TAX FOR DIVISION   
	IF @tintOppType=1 OR (@tintOppType=2 and (select isnull(bitPurchaseTaxCredit,0) from Domain where numDomainId=@numDomainId)=1)
	BEGIN
		INSERT INTO dbo.OpportunityMasterTaxItems 
		(
			numOppId,
			numTaxItemID,
			fltPercentage,
			tintTaxType,
			numTaxID
		) 
		SELECT 
			@numOppID,
			TI.numTaxItemID,
			TEMPTax.decTaxValue,
			TEMPTax.tintTaxType,
			0
		FROM 
			TaxItems TI 
		JOIN 
			DivisionTaxTypes DTT 
		ON 
			TI.numTaxItemID = DTT.numTaxItemID
		CROSS APPLY
		(
			SELECT
				decTaxValue,
				tintTaxType
			FROM
				dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,TI.numTaxItemID,@numOppId,0,NULL)
		) AS TEMPTax
		WHERE 
			DTT.numDivisionID=@numDivisionId 
			AND DTT.bitApplicable=1
		UNION 
		SELECT 
			@numOppID,
			0,
			TEMPTax.decTaxValue,
			TEMPTax.tintTaxType,
			0
		FROM 
			dbo.DivisionMaster 
		CROSS APPLY
		(
			SELECT
				decTaxValue,
				tintTaxType
			FROM
				dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,0,@numOppId,1,NULL)
		) AS TEMPTax
		WHERE 
			bitNoTax=0 
			AND numDivisionID=@numDivisionID	
		UNION 
		SELECT
			@numOppId
			,1
			,decTaxValue
			,tintTaxType
			,numTaxID
		FROM
			dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,1,@numOppId,1,NULL)		
	END	
END

--INSERT TAX FOR DIVISION   
IF @tintOppType=1 OR (@tintOppType=2 and (select isnull(bitPurchaseTaxCredit,0) from Domain where numDomainId=@numDomainId)=1)
BEGIN
--Delete Tax for Opportunity Items if item deleted 
DELETE FROM OpportunityItemsTaxItems WHERE numOppId=@numOppId AND numOppItemID NOT IN (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID)

--Insert Tax for Opportunity Items
INSERT INTO dbo.OpportunityItemsTaxItems 
(
	numOppId,
	numOppItemID,
	numTaxItemID,
	numTaxID
) 
SELECT 
	@numOppId,
	OI.numoppitemtCode,
	TI.numTaxItemID,
	0
FROM 
	dbo.OpportunityItems OI 
JOIN 
	dbo.ItemTax IT 
ON 
	OI.numItemCode=IT.numItemCode 
JOIN
	TaxItems TI 
ON 
	TI.numTaxItemID = IT.numTaxItemID 
WHERE 
	OI.numOppId=@numOppID 
	AND IT.bitApplicable=1 
	AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
UNION
SELECT 
	@numOppId,
	OI.numoppitemtCode,
	0,
	0
FROM 
	dbo.OpportunityItems OI 
JOIN 
	dbo.Item I 
ON 
	OI.numItemCode=I.numItemCode
WHERE 
	OI.numOppId=@numOppID 
	AND I.bitTaxable=1 
	AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
UNION
SELECT
	@numOppId,
	OI.numoppitemtCode,
	1,
	TD.numTaxID
FROM
	dbo.OpportunityItems OI 
INNER JOIN
	ItemTax IT
ON
	IT.numItemCode = OI.numItemCode
INNER JOIN
	TaxDetails TD
ON
	TD.numTaxID = IT.numTaxID
	AND TD.numDomainId = @numDomainId
WHERE
	OI.numOppId = @numOppID
	AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)

END
  
 UPDATE OpportunityMaster SET monDealAmount=dbo.GetDealAmount(@numOppID,getdate(),0) WHERE numOppId=@numOppID

	IF @tintOppType=1 AND @tintOppStatus=1 
	BEGIN
		EXEC USP_SalesFulfillmentQueue_Save @numDomainID,@numUserCntID,@numOppID,@numStatus
	END
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SET @numOppID = 0

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityMaster_GetDataForEDI940')
DROP PROCEDURE dbo.USP_OpportunityMaster_GetDataForEDI940
GO
CREATE PROCEDURE [dbo].[USP_OpportunityMaster_GetDataForEDI940]
	@numDomainID NUMERIC(18,0)
	,@numOppID NUMERIC(18,0)
AS 
BEGIN
	SELECT
		OpportunityMaster.numOppID
		,OpportunityMaster.vcPOppName
		,CompanyInfo.vcCompanyName
		,Domain.vcDomainName
		,OpportunityMaster.numDomainID
		,OpportunityMaster.numDivisionID
		,CONCAT(OpportunityMaster.numDomainID,'-',OpportunityMaster.numDivisionID) AS vcDomainDivisionID
		,ISNULL(OpportunityMaster.txtComments,'') vcComments
		,ISNULL(AdditionalContactsInformation.vcFirstName,'') vcFirstName
		,ISNULL(AdditionalContactsInformation.vcLastName,'') vcLastName
		,ISNULL(AdditionalContactsInformation.numCell,'') vcPhone
		,ISNULL((SELECT TOP 1 vcStreet FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainID,1)),'') AS [vcBillStreet]
		,ISNULL((SELECT TOP 1 vcCity FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainID,1)),'') AS [vcBillCity]
		,ISNULL((SELECT TOP 1 vcState FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainID,1)),'') AS [vcBillState]
		,ISNULL((SELECT TOP 1 vcCountry FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainID,1)),'') AS [vcBillCountry]
		,ISNULL((SELECT TOP 1 vcPostalCode FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainID,1)),'') AS [vcBillPostalCode]
		,ISNULL((SELECT TOP 1 vcStreet FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainID,2)),'') AS [vcShipStreet]
		,ISNULL((SELECT TOP 1 vcCity FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainID,2)),'') AS [vcShipCity]
		,ISNULL((SELECT TOP 1 vcState FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainID,2)),'') AS [vcShipState]
		,ISNULL((SELECT TOP 1 vcCountry FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainID,2)),'') AS [vcShipCountry]
		,ISNULL((SELECT TOP 1 vcPostalCode FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainID,2)),'') AS [vcShipPostalCode]
	FROM
		OpportunityMaster
	INNER JOIN
		Domain
	ON
		OpportunityMaster.numDomainID = Domain.numDomainId
	INNER JOIN
		DivisionMaster
	ON
		OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
	INNER JOIN
		CompanyInfo
	ON
		DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
	LEFT JOIN
		AdditionalContactsInformation
	ON
		OpportunityMaster.numContactId = AdditionalContactsInformation.numContactId
	WHERE
		OpportunityMaster.numDomainId=@numDomainID
		AND OpportunityMaster.numOppId = @numOppID


	SELECT
		Item.vcItemName
		,CASE 
			WHEN ISNULL(Item.numItemGroup,0) > 0 AND ISNULL(Item.bitMatrix,0) = 0
			THEN 
				(CASE WHEN LEN(ISNULL(WareHouseItems.vcWHSKU,'')) > 0 THEN WareHouseItems.vcWHSKU ELSE ISNULL(Item.vcSKU,'') END)
			ELSE
				(CASE WHEN LEN(ISNULL(OpportunityItems.vcSKU,'')) > 0 THEN OpportunityItems.vcSKU ELSE ISNULL(Item.vcSKU,'') END)
		END vcSKU
		,CAST((dbo.fn_UOMConversion(ISNULL(Item.numBaseUnit, 0),OpportunityItems.numItemCode,@numDomainID, ISNULL(OpportunityItems.numUOMId, 0))* OpportunityItems.numUnitHour) AS NUMERIC(18, 2)) AS numUnits
		,ISNULL(UOM.vcUnitName,'Units') vcUOM
		,(CASE 
			WHEN charItemType='P' THEN 'Inventory Item'
			WHEN charItemType='S' THEN 'Service' 
			WHEN charItemType='A' THEN 'Accessory' 
			WHEN charItemType='N' THEN 'Non-Inventory Item' 
		END) AS vcItemType
		,ISNULL(OpportunityItems.vcNotes,'') vcNotes
	FROM
		OpportunityItems
	INNER JOIN
		Item
	ON
		OpportunityItems.numItemCode = Item.numItemCode
	LEFT JOIN
		WareHouseItems
	ON
		OpportunityItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
	LEFT JOIN 
		UOM 
	ON
		OpportunityItems.numUOMId = UOM.numUOMId
	WHERE
		numOppId = @numOppID
		AND ISNULL(OpportunityItems.bitDropShip,0) = 0
END
GO
/****** Object:  StoredProcedure [dbo].[USP_OPPItemDetails]    Script Date: 09/01/2009 18:59:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- exec USP_OPPItemDetails @numItemCode=31455,@OpportunityId=12387,@numDomainId=110
--created by anooop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityMaster_GetItems')
DROP PROCEDURE USP_OpportunityMaster_GetItems
GO
CREATE PROCEDURE [dbo].[USP_OpportunityMaster_GetItems]
(
    @numUserCntID NUMERIC(9) = 0,
    @OpportunityId NUMERIC(9) = 0,
    @numDomainId NUMERIC(9),
    @ClientTimeZoneOffset  INT
)
AS 
BEGIN
    IF @OpportunityId >0
    BEGIN
        DECLARE @DivisionID AS NUMERIC(9)
        DECLARE @TaxPercentage AS FLOAT
        DECLARE @tintOppType AS TINYINT
        DECLARE @fld_Name AS VARCHAR(500)
        DECLARE @fld_ID AS NUMERIC(9)
        DECLARE @numBillCountry AS NUMERIC(9)
        DECLARE @numBillState AS NUMERIC(9)
    
        SELECT  
			@DivisionID = numDivisionID
			,@tintOppType = tintOpptype
        FROM 
			OpportunityMaster
        WHERE 
			numOppId = @OpportunityId
               

		CREATE TABLE #tempForm 
		(
			ID INT IDENTITY(1,1)
			,tintOrder TINYINT
			,vcDbColumnName NVARCHAR(50)
			,vcOrigDbColumnName NVARCHAR(50)
			,vcFieldName NVARCHAR(50)
			,vcAssociatedControlType NVARCHAR(50)
			,vcListItemType VARCHAR(3)
			,numListID NUMERIC(9)
			,vcLookBackTableName VARCHAR(50)
			,bitCustomField BIT
			,numFieldId NUMERIC
			,bitAllowSorting BIT
			,bitAllowEdit BIT
			,bitIsRequired BIT
			,bitIsEmail BIT
			,bitIsAlphaNumeric BIT
			,bitIsNumeric BIT
			,bitIsLengthValidation BIT
			,intMaxLength INT
			,intMinLength INT
			,bitFieldMessage BIT
			,vcFieldMessage VARCHAR(500)
			,ListRelID NUMERIC(9)
			,intColumnWidth INT
			,intVisible INT
			,vcFieldDataType CHAR(1)
			,numUserCntID NUMERIC(18,0)
		)

		
		IF (
				SELECT 
					COUNT(*)
				FROM 
					View_DynamicColumns DC 
				WHERE 
					DC.numFormId=(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) and DC.numDomainID=@numDomainID AND numUserCntID = @numUserCntID AND
					ISNULL(DC.bitSettingField,0)=1 AND ISNULL(DC.bitDeleted,0)=0 AND ISNULL(DC.bitCustom,0)=0) > 0
			OR
			(
				SELECT 
					COUNT(*)
				FROM 
					View_DynamicCustomColumns
				WHERE 
					numFormId=(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) AND numUserCntID=@numUserCntID AND numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=0 AND ISNULL(bitCustom,0)=1
			) > 0
		BEGIN
			INSERT INTO 
				#tempForm
			SELECT 
				ISNULL(DC.tintRow,0) + 1 as tintOrder
				,DDF.vcDbColumnName
				,DDF.vcOrigDbColumnName
				,ISNULL(DDF.vcCultureFieldName,DDF.vcFieldName)
				,DDF.vcAssociatedControlType
				,DDF.vcListItemType
				,DDF.numListID,DC.vcLookBackTableName                                                 
				,DDF.bitCustom,DC.numFieldId
				,DDF.bitAllowSorting
				,DDF.bitInlineEdit
				,DDF.bitIsRequired
				,DDF.bitIsEmail
				,DDF.bitIsAlphaNumeric
				,DDF.bitIsNumeric
				,DDF.bitIsLengthValidation
				,DDF.intMaxLength
				,DDF.intMinLength
				,DDF.bitFieldMessage
				,DDF.vcFieldMessage vcFieldMessage
				,DDF.ListRelID
				,DDF.intColumnWidth
				,CASE WHEN DC.numFieldID IS NULL THEN 0 ELSE 1 END
				,DDF.vcFieldDataType
				,DC.numUserCntID
			FROM  
				View_DynamicDefaultColumns DDF 
			LEFT JOIN 
				View_DynamicColumns DC 
			ON 
				DC.numFormId=(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) 
				AND DDF.numFieldId=DC.numFieldId 
				AND DC.numUserCntID=@numUserCntID 
				AND DC.numDomainID=@numDomainID 
				AND numRelCntType=0 
				AND tintPageType=1 
			WHERE 
				DDF.numFormId=(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) 
				AND DDF.numDomainID=@numDomainID 
				AND ISNULL(DDF.bitSettingField,0)=1 
				AND ISNULL(DDF.bitDeleted,0)=0 
				AND ISNULL(DDF.bitCustom,0)=0
			UNION
			SELECT 
				tintRow + 1 AS tintOrder
				,vcDbColumnName
				,vcFieldName
				,vcFieldName
				,vcAssociatedControlType
				,'' as vcListItemType
				,numListID
				,''                                                 
				,bitCustom
				,numFieldId
				,bitAllowSorting
				,0 as bitAllowEdit
				,bitIsRequired
				,bitIsEmail
				,bitIsAlphaNumeric
				,bitIsNumeric
				,bitIsLengthValidation
				,intMaxLength
				,intMinLength
				,bitFieldMessage
				,vcFieldMessage
				,ListRelID
				,intColumnWidth
				,1
				,''
				,0
			FROM 
				View_DynamicCustomColumns
			WHERE 
				numFormId=(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) 
				AND numUserCntID=@numUserCntID 
				AND numDomainID=@numDomainID 
				AND tintPageType=1 
				AND numRelCntType=0
				AND ISNULL(bitCustom,0)=1
		END
		ELSE
		BEGIN
			--Check if Master Form Configuration is created by administrator if NoColumns=0
			DECLARE @IsMasterConfAvailable BIT = 0
			DECLARE @numUserGroup NUMERIC(18,0)

			SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

			IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = (CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
			BEGIN
				SET @IsMasterConfAvailable = 1
			END

			--If MasterConfiguration is available then load it otherwise load default columns
			IF @IsMasterConfAvailable = 1
			BEGIN
				INSERT INTO #tempForm
				SELECT 
					(intRowNum + 1) as tintOrder, vcDbColumnName, vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
					vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName,bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,
					bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,
					bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth,1,vcFieldDataType
				FROM 
					View_DynamicColumnsMasterConfig
				WHERE
					View_DynamicColumnsMasterConfig.numFormId=(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) AND 
					View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
					View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
					View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
					ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
					ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
				UNION
				SELECT 
					tintRow+1 as tintOrder,vcDbColumnName,vcFieldName,vcFieldName,                  
					 vcAssociatedControlType,'' as vcListItemType,numListID,''                                                 
					,bitCustom,numFieldId,bitAllowSorting,0 as bitAllowEdit,
					bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
					intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,''
				FROM 
					View_DynamicCustomColumnsMasterConfig
				WHERE 
					View_DynamicCustomColumnsMasterConfig.numFormId=(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) AND 
					View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
					View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
					View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
					ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1
				ORDER BY 
					tintOrder ASC  
			END
		END

		
		DECLARE @strSQL VARCHAR(MAX) = ''

		DECLARE @avgCost int
		SET @avgCost = ISNULL((select TOP 1 numCost from Domain where numDOmainId=@numDomainID),0)



		--------------Make Dynamic Query--------------
		SET @strSQL = @strSQL + 'SELECT 
									Opp.numoppitemtCode
									,Opp.vcNotes
									,WItems.numBackOrder
									,CAST(ISNULL(Opp.numCost,0) AS DECIMAL(20,5)) AS numCost
									,ISNULL(Opp.bitItemPriceApprovalRequired,0) AS bitItemPriceApprovalRequired
									,ISNULL(Opp.numSOVendorID,0) AS numVendorID
									,' 
									+ CONCAT('ISNULL(Opp.monTotAmount,0) - (ISNULL(Opp.numUnitHour,0) * (CASE ',@avgCost,' WHEN 3 THEN ISNULL(V.monCost,0) / dbo.fn_UOMConversion(numPurchaseUnit,I.numItemCode,',@numDomainID,',numBaseUnit) WHEN 2 THEN (CASE WHEN ISNULL(Opp.numSOVendorID,0) > 0 THEN Opp.numCost ELSE ISNULL(V.monCost,0) / dbo.fn_UOMConversion(numPurchaseUnit,I.numItemCode,',@numDomainID,',numBaseUnit) END) ELSE monAverageCost END)) AS vcMargin,') + '
									OM.tintOppType
									,Opp.numItemCode
									,I.charItemType
									,ISNULL(I.bitAsset,0) as bitAsset
									,ISNULL(I.bitRental,0) as bitRental
									,ISNULL(I.bitSerialized,0) bitSerialized
									,ISNULL(I.bitLotNo,0) bitLotNo
									,Opp.numOppId
									,Opp.bitWorkOrder
									,Opp.fltDiscount
									,ISNULL(Opp.bitDiscountType,0) bitDiscountType
									,dbo.fn_UOMConversion(Opp.numUOMId, Opp.numItemCode,' + CAST(@numDomainId AS VARCHAR) +', NULL) AS UOMConversionFactor
									,ISNULL(Opp.bitWorkOrder,0) bitWorkOrder
									,ISNULL(Opp.numUnitHourReceived,0) numUnitHourReceived
									,ISNULL((SELECT COUNT(*) FROM OppWarehouseSerializedItem WHERE numOppId=Opp.numOppID AND numOppItemID=Opp.numoppitemtCode),0) AS numSerialNoAssigned
									,(CASE 
										WHEN (SELECT 
												COUNT(*) 
											FROM 
												OpportunityBizDocs OB 
											JOIN 
												dbo.OpportunityBizDocItems OBI 
											ON 
												ob.numOppBizDocsId=obi.numOppBizDocID 
												AND (ob.bitAuthoritativeBizDocs=1 OR ob.numBizDocId=304) 
												AND obi.numOppItemID=Opp.numoppitemtCode 
												AND ob.numOppId=Opp.numOppId)>0 
											THEN 1 
											ELSE 0 
									END) AS bitIsAuthBizDoc
									,(CASE 
										WHEN (SELECT 
													COUNT(*) 
												FROM 
													OpportunityBizDocs OB 
												JOIN 
													dbo.OpportunityBizDocItems OBI 
												ON 
													OB.numOppBizDocsId=OBI.numOppBizDocID 
												WHERE 
													OB.numOppId = Opp.numOppId 
													AND OBI.numOppItemID=Opp.numoppitemtCode 
													AND OB.numBizDocId=296) > 0 
											THEN 1 
											ELSE 0 
										END) AS bitAddedFulFillmentBizDoc
									,ISNULL(numPromotionID,0) AS numPromotionID
									,ISNULL(opp.numUOMId, 0) AS numUOM
									,(SELECT vcUnitName FROM dbo.UOM WHERE numUOMId = opp.numUOMId AND ISNULL(bitEnabled,0) = 1)  AS  [vcBaseUOMName]
									,Opp.numUnitHour AS numOriginalUnitHour 
									,ISNULL(M.vcPOppName,''Internal'') as Source
									,numSourceID
									,(CASE WHEN ISNULL(bitKitParent,0) = 1 AND ISNULL(bitAssembly,0) = 1 THEN 0 WHEN ISNULL(bitKitParent,0) = 1 THEN 1 ELSE 0 END) as bitKitParent
									,(CASE 
										WHEN OM.tintOppType = 1 AND OM.tintOppStatus = 1 
										THEN 
											(CASE 
												WHEN (I.charItemType = ''p'' AND ISNULL(bitDropShip, 0) = 0 AND ISNULL(bitAsset,0)=0)
												THEN 
													dbo.CheckOrderItemInventoryStatus(Opp.numItemCode,Opp.numUnitHour,Opp.numoppitemtCode,Opp.numWarehouseItmsID,(CASE WHEN ISNULL(bitKitParent,0) = 1 AND ISNULL(bitAssembly,0) = 1 THEN 0  WHEN ISNULL(bitKitParent,0) = 1 THEN 1 ELSE 0 END)) 
												ELSE 0 
											END) 
										ELSE 0 
									END) AS bitBackOrder
									,(SELECT vcData FROM dbo.ListDetails WHERE 1=1 AND numListItemID = numShipClass ) AS [numShipClass]
									,ISNULL(Opp.numQtyShipped,0) numQtyShipped
									,ISNULL(Opp.numUnitHourReceived,0) AS numUnitHourReceived
									,ISNULL(OM.fltExchangeRate,0) AS fltExchangeRate
									,ISNULL(OM.numCurrencyID,0) AS numCurrencyID
									,ISNULL(OM.numDivisionID,0) AS numDivisionID
									,(CASE 
										WHEN OM.tintOppType = 1 AND OM.tintOppStatus = 1 AND UPPER(I.charItemType) = ''P'' AND bitDropShip = 0
										THEN (CASE WHEN OM.tintShipped = 1 THEN CAST(ISNULL(Opp.numUnitHour,0) AS VARCHAR) ELSE CAST(ISNULL(Opp.numQtyShipped,0) AS VARCHAR) END)
										WHEN OM.tintOppType = 2 AND OM.tintOppStatus = 1 AND UPPER(I.charItemType) = ''P'' AND bitDropShip = 0
										THEN (CASE WHEN OM.tintShipped = 1 THEN CAST(ISNULL(Opp.numUnitHour,0) AS VARCHAR) ELSE CAST(ISNULL(Opp.numUnitHourReceived,0) AS VARCHAR) END)
										ELSE '''' 
									END) AS vcShippedReceived
									,ISNULL(I.numIncomeChartAcntId,0) AS itemIncomeAccount
									,ISNULL(I.numAssetChartAcntId,0) AS itemInventoryAsset
									,ISNULL(i.numCOGsChartAcntId,0) AS itemCoGs
									,ISNULL(Opp.numRentalIN,0) as numRentalIN
									,ISNULL(Opp.numRentalLost,0) as numRentalLost
									,ISNULL(Opp.numRentalOut,0) as numRentalOut
									,ISNULL(bitFreeShipping,0) AS [IsFreeShipping]
									,Opp.bitDiscountType,isnull(Opp.monLandedCost,0) as monLandedCost
									, ISNULL(Opp.vcPromotionDetail,'''') AS vcPromotionDetail
									,'''' AS numPurchasedQty
									,Opp.vcPathForTImage
									,Opp.vcItemName
									,Opp.vcModelID
									,vcWarehouse
									,WItems.numWarehouseID
									,WItems.numOnHand
									,ISNULL(WItems.numAllocation,0) as numAllocation
									,WL.vcLocation
									,Opp.vcType ItemType
									,Opp.vcType
									,ISNULL(Opp.vcItemDesc, '''') vcItemDesc
									,Opp.vcAttributes
									,ISNULL(bitDropShip, 0) as DropShip
									,CASE ISNULL(bitDropShip, 0) WHEN 1 THEN ''Yes'' ELSE '''' END AS bitDropShip
									,CAST(( dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), Opp.numItemCode,om.numDomainId, ISNULL(opp.numUOMId, 0))* Opp.numUnitHour ) AS NUMERIC(18, 2)) AS numUnitHour
									,Opp.monPrice
									,CAST(( dbo.fn_UOMConversion(ISNULL(opp.numUOMId, 0), Opp.numItemCode,om.numDomainId, ISNULL(I.numBaseUnit, 0)) * Opp.monPrice) AS NUMERIC(18, 4)) AS monPriceUOM
									,Opp.monTotAmount
									,Opp.monTotAmtBefDiscount
									,ISNULL(WItems.vcWHSKU,ISNULL(I.vcSKU,'''')) [vcSKU]
									,Opp.vcManufacturer
									,dbo.[fn_GetListItemName](I.[numItemClassification]) numItemClassification
									,(SELECT [vcItemGroup] FROM [ItemGroups] WHERE [numItemGroupID] = I.numItemGroup) numItemGroup
									,ISNULL(u.vcUnitName, '''') numUOMId
									,I.fltWeight
									,I.fltHeight
									,I.fltWidth
									,I.fltLength
									,I.numBarCodeId
									,I.IsArchieve
									,ISNULL(I.numContainer,0) AS numContainer
									,ISNULL(I.numNoItemIntoContainer,0) AS numContainerQty
									,CASE WHEN Opp.bitDiscountType = 0
										 THEN REPLACE(STR(ISNULL(Opp.monTotAmtBefDiscount, Opp.monTotAmount)
											  - Opp.monTotAmount,20,4), '' '', '''') + '' (''
											  + CAST(FORMAT(Opp.fltDiscount,''0.00##'') AS VARCHAR(50)) + ''%)''
										 ELSE REPLACE(STR(ISNULL(Opp.monTotAmtBefDiscount, Opp.monTotAmount)
											  - Opp.monTotAmount,20,4), '' '', '''')
									END AS DiscAmt
									,ISNULL(Opp.numProjectID, 0) numProjectID
									,CASE WHEN ( SELECT TOP 1
														ISNULL(OBI.numOppBizDocItemID, 0)
												FROM    dbo.OpportunityBizDocs OBD
														INNER JOIN dbo.OpportunityBizDocItems OBI ON OBD.numOppBizDocsID = OBI.numOppBizDocID
												WHERE   OBI.numOppItemID = Opp.numoppitemtCode
														AND ISNULL(OBD.bitAuthoritativeBizDocs, 0) = 1
														and isnull(OBD.numOppID,0)=OM.numOppID
											  ) > 0 THEN 1
										 ELSE 0
									END AS bitItemAddedToAuthBizDoc
									,ISNULL(Opp.numClassID, 0) numClassID
									,ISNULL(Opp.numProjectStageID, 0) numProjectStageID
									,CASE 
										WHEN OPP.numProjectStageID > 0 THEN dbo.fn_GetProjectStageName(OPP.numProjectId,OPP.numProjectStageID)
										WHEN OPP.numProjectStageID = -1 THEN ''T&E''
										WHEN OPP.numProjectId > 0 THEN CASE WHEN OM.tintOppType = 1 THEN ''S.O.'' ELSE ''P.O.'' END
										ELSE ''-''
									END AS vcProjectStageName
									,CAST(Opp.numUnitHour AS FLOAT) numQty
									,ISNULL((SELECT sum(numQty) FROM OppWarehouseSerializedItem WHERE numOppID=OM.numOppID AND numOppItemID=Opp.numoppitemtCode),0) AS SerialLotNo
									,Opp.numWarehouseItmsID,'


		
			   IF EXISTS (SELECT * FROM #tempForm WHERE vcOrigDbColumnName='vcWorkOrderStatus' AND ISNULL(numUserCntID,0) = @numUserCntID)
			   BEGIN
					SET @strSQL = @strSQL + 'CASE WHEN ISNULL(Opp.bitWorkOrder, 0) = 1
											 THEN ( SELECT  dbo.GetListIemName(numWOStatus)
															+ CASE WHEN ISNULL(numWOStatus, 0) = 23184
																   THEN '' - ''
																		+ CAST(ISNULL(dbo.fn_GetContactName(numAssignedTo),
																					  '''') + '',''
																		+ CONVERT(VARCHAR(20), DATEADD(minute, '+ CONVERT(VARCHAR(15),-@ClientTimeZoneOffset)  +', bintCompletedDate)) AS VARCHAR(100))
																   ELSE ''''
															  END
													FROM    WorkOrder
													WHERE   ISNULL(numOppId, 0) = Opp.numOppId
															AND numItemCode = Opp.numItemCode AND ISNULL(numParentWOID,0)=0
												  )
											 ELSE ''''
										END AS vcWorkOrderStatus,'
			   END     

			   IF EXISTS (SELECT * FROM #tempForm WHERE vcOrigDbColumnName='vcItemReleaseDate' AND ISNULL(numUserCntID,0) = @numUserCntID)
			   BEGIN
					SET @strSQL = @strSQL + '(CASE 
												WHEN I.charItemType = ''p'' AND ISNULL(bitDropShip, 0) = 0 
												THEN dbo.GetOppItemReleaseDates(om.numDomainId,Opp.numOppId,Opp.numoppitemtCode,1) 
												ELSE '''' 
											END) AS vcItemReleaseDate,'
			   END

			   IF EXISTS (SELECT * FROM #tempForm WHERE vcOrigDbColumnName='SerialLotNo' AND ISNULL(numUserCntID,0) = @numUserCntID)
			   BEGIN
					SET @strSQL = @strSQL + 'SUBSTRING((SELECT  
													'', '' + vcSerialNo + CASE WHEN ISNULL(I.bitLotNo, 0) = 1 THEN '' ('' + CONVERT(VARCHAR(15), oppI.numQty) + '')'' ELSE ''''	END
												FROM 
													OppWarehouseSerializedItem oppI
												JOIN 
													WareHouseItmsDTL whi 
												ON 
													oppI.numWareHouseItmsDTLID = whi.numWareHouseItmsDTLID
												WHERE 
													oppI.numOppID = Opp.numOppId
													AND oppI.numOppItemID = Opp.numoppitemtCode
												ORDER BY 
													vcSerialNo
												FOR
													XML PATH('''')), 3, 200000) AS vcSerialLotNo,'
			   END
			   
			   IF EXISTS (SELECT * FROM #tempForm WHERE vcOrigDbColumnName='vcInclusionDetails' AND ISNULL(numUserCntID,0) = @numUserCntID)
			   BEGIN
					SET @strSQL = @strSQL + 'dbo.GetOrderAssemblyKitInclusionDetails(OM.numOppID,Opp.numoppitemtCode,Opp.numUnitHour) AS vcInclusionDetails,'
			   END     
	
			-- (CASE WHEN @tintOppType=1 THEN 26 ELSE 129 END) --

		--------------Add custom fields to query----------------
		SELECT TOP 1 @fld_Name= vcFieldName,@fld_ID = numFieldId FROM #tempForm WHERE ISNULL(bitCustomField,0)=1 ORDER BY numFieldId
		WHILE @fld_ID > 0
		BEGIN 
			SET @strSQL = @strSQL + CONCAT('dbo.GetCustFldValueOppItems(',@fld_ID,',Opp.numoppitemtCode,Opp.numItemCode) as ''',@fld_Name,''',')
       
			SELECT TOP 1 @fld_Name= vcFieldName,@fld_ID = numFieldId FROM #tempForm WHERE ISNULL(bitCustomField,0)=1 AND numFieldId > @fld_ID ORDER BY numFieldId
		 
			IF @@ROWCOUNT=0
				SET @fld_ID = 0
		END

		 SET @strSQL = left(@strSQL,len(@strSQL)-1) + '   
				FROM    OpportunityItems Opp
				LEFT JOIN OpportunityMaster OM ON Opp.numOppID = oM.numOppID
				JOIN item I ON Opp.numItemCode = i.numItemcode
				LEFT JOIN WareHouseItems WItems ON WItems.numWareHouseItemID = Opp.numWarehouseItmsID
				LEFT JOIN Warehouses W ON W.numWarehouseID = WItems.numWarehouseID
				LEFT JOIN WarehouseLocation WL ON WL.numWLocationID = WItems.numWLocationID
				LEFT JOIN OpportunityMaster M ON Opp.numSourceID = M.numOppID
				LEFT JOIN UOM u ON u.numUOMId = opp.numUOMId
				LEFT JOIN Vendor V ON I.numVendorID = V.numVendorID  AND V.numItemCode = I.numItemCode
		WHERE   om.numDomainId = '+ CONVERT(VARCHAR(10),@numDomainId) + '
				AND Opp.numOppId = '+ CONVERT(VARCHAR(15),@OpportunityId) +'
		ORDER BY opp.numSortOrder,numoppitemtCode ASC '
  
		PRINT @strSQL;
		EXEC (@strSQL);
		SELECT  vcSerialNo,
			   vcComments AS Comments,
				numOppItemID AS numoppitemtCode,
				W.numWarehouseItmsDTLID,
				numWarehouseItmsID AS numWItmsID,
				dbo.Fn_getattributes(W.numWarehouseItmsDTLID,
									 1) Attributes
		FROM    WareHouseItmsDTL W
				JOIN OppWarehouseSerializedItem O ON O.numWarehouseItmsDTLID = W.numWareHouseItmsDTLID
		WHERE   numOppID = @OpportunityId


		select * from #tempForm order by tintOrder

		drop table #tempForm

    END         
END
GO
/****** Object:  StoredProcedure [dbo].[USP_TicklerActItems]    Script Date: 07/26/2008 16:21:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Created By Anoop jayaraj                                                        
                                                    
-- Modified by Tarun Juneja                                                    
-- date 26-08-2006                                                    
-- Reason:- Enhancement In Tickler                                                    
                                                                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_tickleractitems')
DROP PROCEDURE usp_tickleractitems
GO
CREATE PROCEDURE [dbo].[USP_TicklerActItems]                                                                        
@numUserCntID as numeric(9)=null,                                                                        
@numDomainID as numeric(9)=null,                                                                        
@startDate as datetime,                                                                        
@endDate as datetime,                                                                    
@ClientTimeZoneOffset Int,  --Added by Debasish to enable calculation of date according to client machine                                                                      
@bitFilterRecord bit=0,
@columnName varchar(50),
@columnSortOrder varchar(50),
@vcBProcessValue varchar(500),
@RegularSearchCriteria VARCHAR(MAX)='',
@CustomSearchCriteria VARCHAR(MAX)=''                                                
As                                                                         
  
DECLARE @tintPerformanceFilter AS TINYINT
DECLARE @tintActionItemsViewRights TINYINT  = 3

IF EXISTS (SELECT * FROM GroupAuthorization WHERE numDomainID=@numDomainID AND numModuleID=1 AND numPageID=2 AND numGroupID IN (SELECT ISNULL(numGroupID,0) FROM UserMaster WHERE numDomainID=@numDomainID AND numUserDetailId=@numUserCntID))
BEGIN
	SELECT @tintActionItemsViewRights=ISNULL(intViewAllowed,0) FROM GroupAuthorization WHERE numDomainID=@numDomainID AND numModuleID=1 AND numPageID=2 AND numGroupID IN (SELECT ISNULL(numGroupID,0) FROM UserMaster WHERE numDomainID=@numDomainID AND numUserDetailId=@numUserCntID)
END

IF PATINDEX('%cmp.vcPerformance=1%', @RegularSearchCriteria)>0 --Last 3 Months
BEGIN
	SET @tintPerformanceFilter = 1
END
ELSE IF PATINDEX('%cmp.vcPerformance=2%', @RegularSearchCriteria)>0 --Last 6 Months
BEGIN
	SET @tintPerformanceFilter = 2
END
ELSE IF PATINDEX('%cmp.vcPerformance=3%', @RegularSearchCriteria)>0 --Last 1 Year
BEGIN
	SET @tintPerformanceFilter = 3
END
ELSE
BEGIN
	SET @tintPerformanceFilter = 0
END

IF @columnName = 'vcPerformance'   
BEGIN
	SET @columnName = 'monDealAmount'
END

DECLARE @vcSelectedEmployess VARCHAR(100)
DECLARE @numCriteria INT
DECLARE @vcActionTypes VARCHAR(100)

SELECT 
	@vcSelectedEmployess = vcSelectedEmployee,
	@numCriteria = numCriteria,
	@vcActionTypes = vcActionTypes
FROM 
	TicklerListFilterConfiguration
WHERE
	numDomainID = @numDomainID AND
	numUserCntID = @numUserCntID


Create table #tempRecords (numContactID numeric(9),numDivisionID numeric(9),tintCRMType tinyint,
Id numeric(9),bitTask varchar(100),Startdate datetime,EndTime datetime,
itemDesc text,[Name] varchar(200),vcFirstName varchar(100),vcLastName varchar(100),numPhone varchar(15),numPhoneExtension varchar(7),
[Phone] varchar(30),vcCompanyName varchar(100),vcProfile VARCHAR(100), vcEmail varchar(50),Task varchar(100),Activity varchar(500),
Status varchar(100),numCreatedBy numeric(9),numRecOwner VARCHAR(500), numModifiedBy VARCHAR(500), numOrgTerId NUMERIC(18,0), numTerId VARCHAR(200),numAssignedTo varchar(1000),numAssignedBy varchar(50),
 caseid numeric(9),vcCasenumber varchar(50),casetimeId numeric(9),caseExpId numeric(9),type int,itemid varchar(50),
changekey varchar(50),DueDate varchar(50),bitFollowUpAnyTime bit,numNoOfEmployeesId varchar(50),vcComPhone varchar(50),numFollowUpStatus varchar(200),vcLastSalesOrderDate VARCHAR(100),monDealAmount DECIMAL(20,5), vcPerformance VARCHAR(100),vcColorScheme VARCHAR(50),
Slp_Name varchar(100),intTotalProgress INT,vcPoppName varchar(100), numOppId NUMERIC(18,0),numBusinessProcessID numeric(18,0),numCompanyRating varchar(500),numStatusID varchar(500),numCampaignID varchar(500) )                                                         
 
declare @strSql as varchar(MAX)                                                            
declare @strSql1 as varchar(MAX)                                                            
declare @strSql2 as varchar(MAX)                                                            
declare @vcCustomColumnName AS VARCHAR(8000)
declare @vcnullCustomColumnName AS VARCHAR(8000)
   -------Change Row Color-------
Declare @vcCSOrigDbCOlumnName as varchar(50),@vcCSLookBackTableName as varchar(50),@vcCSAssociatedControlType as varchar(50)
SET @vcCSOrigDbCOlumnName=''
SET @vcCSLookBackTableName=''

declare @tintOrder as tinyint                                                
declare @vcFieldName as varchar(50)                                                
declare @vcListItemType as varchar(3)                                           
declare @vcAssociatedControlType varchar(20)                                                
declare @numListID AS numeric(9)                                                
declare @vcDbColumnName varchar(20)                    
declare @WhereCondition varchar(2000)                     
declare @vcLookBackTableName varchar(2000)              
Declare @bitCustomField as BIT
DECLARE @bitAllowEdit AS CHAR(1)                 
Declare @numFieldId as numeric  
DECLARE @bitAllowSorting AS CHAR(1)   
declare @vcColumnName AS VARCHAR(500)       
       
Declare @ListRelID as numeric(9)             
set @tintOrder=0               


	CREATE TABLE #tempForm 
	(
		tintOrder TINYINT,vcDbColumnName nvarchar(50),vcFieldDataType nvarchar(50),vcOrigDbColumnName nvarchar(50),bitAllowFiltering BIT,vcFieldName nvarchar(50)
		,vcAssociatedControlType nvarchar(50),vcListItemType char(3),numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,numFieldId NUMERIC
		,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,bitIsAlphaNumeric BIT,bitIsNumeric BIT,bitIsLengthValidation BIT,intMaxLength int
		,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),ListRelID numeric(9),intColumnWidth NUMERIC(18,2)
	)
                
	--Custom Fields related to Organization
	INSERT INTO 
		#tempForm
	SELECT  
		tintRow + 1 AS tintOrder,vcDbColumnName,'',vcDbColumnName,1,vcFieldName,vcAssociatedControlType,'' as vcListItemType,numListID,'' as vcLookBackTableName                                                 
		,bitCustom,numFieldId,bitAllowSorting,0,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength
		,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth
	FROM 
		View_DynamicCustomColumns
	WHERE 
		numFormId=43 
		AND numUserCntID=@numUserCntID 
		AND numDomainID=@numDomainID 
		AND tintPageType=1
		AND ISNULL(bitCustom,0)=1
 
	SELECT TOP 1 
		@tintOrder = tintOrder + 1
		,@vcDbColumnName=vcDbColumnName
		,@vcFieldName=vcFieldName
		,@vcAssociatedControlType=vcAssociatedControlType
		,@vcListItemType=vcListItemType
		,@numListID=numListID
		,@vcLookBackTableName=vcLookBackTableName                                               
		,@bitCustomField=bitCustomField
		,@numFieldId=numFieldId
		,@bitAllowSorting=bitAllowSorting
		,@bitAllowEdit=0
		,@ListRelID=ListRelID
	FROM 
		#tempForm 
	ORDER BY 
		tintOrder ASC     
  
	SET @vcCustomColumnName=''
	SET @vcnullCustomColumnName=''

	WHILE @tintOrder>0                                                
	BEGIN  
		SET @vcColumnName= @vcDbColumnName
		SET @strSql = 'ALTER TABLE #tempRecords ADD [' + @vcColumnName + '] VARCHAR(800)'
		EXEC(@strSql)
		
		IF @bitCustomField = 1
		BEGIN 
			IF @vcAssociatedControlType = 'TextBox' OR @vcAssociatedControlType = 'TextArea'            
			BEGIN            
				SET @vcCustomColumnName= @vcCustomColumnName+', ISNULL((SELECT TOP 1 CFW.Fld_Value FROM CFW_FLD_Values AS CFW WHERE CFW.Fld_ID='+CAST(@numFieldId AS VARCHAR(50))+' AND CFW.RecId=Div.numDivisionID),'''') ['+ @vcColumnName +']'
				SET @vcnullCustomColumnName= @vcnullCustomColumnName+','''' AS  ['+ @vcColumnName +']'
			END  
			ELSE IF @vcAssociatedControlType = 'CheckBox'         
			BEGIN            
				SET @vcCustomColumnName= @vcCustomColumnName+',ISNULL((SELECT TOP 1 CFW.Fld_Value FROM CFW_FLD_Values AS CFW WHERE CFW.Fld_ID='+CAST(@numFieldId AS VARCHAR(50))+' AND CFW.RecId=Div.numDivisionID),0)  ['+ @vcColumnName + ']'
				SET @vcnullCustomColumnName= @vcnullCustomColumnName+',0 AS ['+ @vcColumnName + ']'
			END            
			ELSE IF @vcAssociatedControlType = 'DateField'        
			BEGIN            
				SET @vcCustomColumnName= @vcCustomColumnName+',dbo.FormatedDateFromDate((SELECT TOP 1 CFW.Fld_Value FROM CFW_FLD_Values AS CFW WHERE CFW.Fld_ID='+CAST(@numFieldId AS VARCHAR(50))+' AND CFW.RecId=Div.numDivisionID),'+convert(varchar(10),@numDomainId)+')  ['+ @vcColumnName +']'              
				SET @vcnullCustomColumnName= @vcnullCustomColumnName+','''' AS  ['+ @vcColumnName +']'
			END            
			ELSE IF @vcAssociatedControlType = 'SelectBox'           
			BEGIN            
				SET @vcCustomColumnName=@vcCustomColumnName+',(SELECT TOP 1 ISNULL(L.vcData,'''') FROM CFW_FLD_Values CFW left Join ListDetails L on L.numListItemID=CFW.Fld_Value WHERE  CFW.Fld_ID='+CAST(@numFieldId AS VARCHAR(50))+' AND CFW.RecId=Div.numDivisionID)'+' ['+ @vcColumnName +']'                                                         
				SET @vcnullCustomColumnName=@vcnullCustomColumnName+','''''+'AS  ['+ @vcColumnName +']'                                                         
			END
			ELSE
			BEGIN
				SET @vcCustomColumnName= CONCAT(@vcCustomColumnName,',(SELECT dbo.fn_GetCustFldStringValue(',@numFieldId,',CFW.RecId,CFW.Fld_Value',') FROM CFW_FLD_Values CFW left Join ListDetails L on L.numListItemID=CFW.Fld_Value WHERE  CFW.Fld_ID=',@numFieldId,' AND CFW.RecId=Div.numDivisionID)',' [', @vcColumnName ,']')
				SET @vcnullCustomColumnName=@vcnullCustomColumnName+','''''+'AS  ['+ @vcColumnName +']' 
			END
		END
		  
		SELECT TOP 1 
			@tintOrder=tintOrder + 1
			,@vcDbColumnName=vcDbColumnName
			,@vcFieldName=vcFieldName
			,@vcAssociatedControlType=vcAssociatedControlType
			,@vcListItemType=vcListItemType
			,@numListID=numListID
			,@vcLookBackTableName=vcLookBackTableName                                               
			,@bitCustomField=bitCustomField
			,@numFieldId=numFieldId
			,@bitAllowSorting=bitAllowSorting
			,@bitAllowEdit=0
			,@ListRelID=ListRelID
		FROM 
			#tempForm 
		WHERE 
			tintOrder > @tintOrder-1 
		ORDER BY 
			tintOrder asc            
 
		IF @@rowcount=0 set @tintOrder=0 
	END

 SET @strSql=''
 PRINT @vcCustomColumnName
--select top 1 @vcCSOrigDbCOlumnName=DFM.vcOrigDbCOlumnName,@vcCSLookBackTableName=DFM.vcLookBackTableName from DycFieldColorScheme DFCS join DycFieldMaster DFM on 
--DFCS.numModuleID=DFM.numModuleID and DFCS.numFieldID=DFM.numFieldID
--where DFCS.numDomainID=@numDomainID and DFM.numModuleID=1

Create table #tempColorScheme(vcOrigDbCOlumnName varchar(50),vcLookBackTableName varchar(50),vcFieldValue varchar(50),vcFieldValue1 varchar(50),vcColorScheme varchar(50),vcAssociatedControlType varchar(50))

insert into #tempColorScheme select DFM.vcOrigDbCOlumnName,DFM.vcLookBackTableName,DFCS.vcFieldValue,DFCS.vcFieldValue1,DFCS.vcColorScheme,DFFM.vcAssociatedControlType 
from DycFieldColorScheme DFCS join DycFormField_Mapping DFFM on DFCS.numFieldID=DFFM.numFieldID
join DycFieldMaster DFM on DFM.numModuleID=DFFM.numModuleID and DFM.numFieldID=DFFM.numFieldID
where DFCS.numDomainID=@numDomainID and DFFM.numFormID=43 AND DFCS.numFormID=43 and isnull(DFFM.bitAllowGridColor,0)=1

IF(SELECT COUNT(*) FROM #tempColorScheme)>0
BEGIN
   select top 1 @vcCSOrigDbCOlumnName=vcOrigDbCOlumnName,@vcCSLookBackTableName=vcLookBackTableName,@vcCSAssociatedControlType=vcAssociatedControlType from #tempColorScheme
END   
----------------------------                   


SET @strSql = @strSql+'insert into #tempRecords
Select * from (                                                    
 SELECT Addc.numContactID,Addc.numDivisionID,Div.tintCRMType,Comm.numCommId AS Id,convert(varchar(15),bitTask) as bitTask,                                               
 DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dtStartTime) as Startdate,                        
 DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dtEndTime) as EndTime ,                                                                        
 Comm.textDetails AS itemDesc,                                                                                                               
 AddC.vcFirstName + '' '' + AddC.vcLastName  as [Name], 
 AddC.vcFirstName,AddC.vcLastName,Addc.numPhone,AddC.numPhoneExtension,                               
 case when Addc.numPhone<>'''' then + AddC.numPhone +case when AddC.numPhoneExtension<>'''' then '' - '' + AddC.numPhoneExtension else '''' end  else '''' end as [Phone],                                                                       
 Comp.vcCompanyName As vcCompanyName , ISNULL((SELECT vcData FROM ListDetails WHERE numListID =21 AND numDomainID='+ CAST(@numDomainID AS VARCHAR) +' AND  numListItemID=Comp.vcProfile ) ,'''') vcProfile ,                                                        
 AddC.vcEmail As vcEmail ,         
dbo.GetListIemName(Comm.bitTask)AS Task,                
 listdetailsActivity.VcData As Activity ,                                                    
 listdetailsStatus.VcData As Status   ,  -- Priority column                                                                                             
 Comm.numCreatedBy,   
 dbo.fn_GetContactName(Comm.numCreatedBy) AS numRecOwner,
 dbo.fn_GetContactName(Comm.numModifiedBy) AS numModifiedBy,                                                                                                          
 ISNULL(Div.numTerId,0) AS numOrgTerId,
 (select TOP 1 vcData from ListDetails where numListItemID=Div.numTerId) AS numTerId,                                           
Substring( 
dbo.fn_GetContactName(numAssign) + 
ISNULL((SELECT SUBSTRING((SELECT '','' + dbo.fn_GetContactName(numContactId) FROM CommunicationAttendees WHERE numCommId=Comm.numCommId FOR XML PATH('''')),1,200000)),''''),0,50)
As numAssignedTo , 
dbo.fn_GetContactName(Comm.numAssignedBy) AS numAssignedBy,        
convert(varchar(50),comm.caseid) as caseid,                          
(select top 1 vcCaseNumber from cases where cases.numcaseid= comm.caseid )as vcCasenumber,                          
ISNULL(comm.casetimeId,0) casetimeId,                          
ISNULL(comm.caseExpId,0) caseExpId,                        
0 as type   ,                    
'''' as itemid ,                    
'''' as changekey ,cast(Null as datetime) as DueDate ,ISNULL(bitFollowUpAnyTime,0) bitFollowUpAnyTime,
dbo.GetListIemName(Comp.numNoOfEmployeesId) AS numNoOfEmployeesId,Div.vcComPhone,
dbo.GetListIemName(Div.numFollowUpStatus) AS numFollowUpStatus,' + CONCAT('ISNULL(dbo.FormatedDateFromDate(TempLastOrder.bintCreatedDate,',@numDomainID,'),'''')') + ' AS vcLastSalesOrderDate,ISNULL(TempPerformance.monDealAmount,0) monDealAmount,(CASE WHEN ISNULL(TempPerformance.monDealAmount,0) > 0 THEN CONCAT(FORMAT(TempPerformance.monDealAmount,''##,####.00''),'' ('',CAST(TempPerformance.fltProfitPer AS DECIMAL(18,2)),''%)'') ELSE '''' END) AS vcPerformance '

IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
	set @strSql=@strSql+',tCS.vcColorScheme'
ELSE
	set @strSql=@strSql+','''' as vcColorScheme'

SET @strSql =@strSql + ' ,ISNULL(Slp_Name,'''') AS Slp_Name,ISNULL(intTotalProgress,0) AS [intTotalProgress],ISNULL(vcPoppName,'''') AS [vcPoppName], ISNULL(Opp.numOppId,0) AS [numOppId],ISNULL(Opp.numBusinessProcessID,0) AS numBusinessProcessID'
SET @strSql =@strSql + ' ,(select TOP 1 vcData from ListDetails where numListItemID=Comp.numCompanyRating) AS numCompanyRating ,(select TOP 1 vcData from ListDetails where numListItemID=Div.numStatusID) AS numStatusID,(SELECT TOP 1 vcCampaignName FROM CampaignMaster WHERE numCampaignID= Opp.numCampainID) AS numCampaignID'
SET @strSql =@strSql+ @vcCustomColumnName
SET @strSql =@strSql + ' FROM  Communication Comm                              
 JOIN AdditionalContactsInformation AddC                                                  
 ON Comm.numContactId = AddC.numContactId                                  
 JOIN DivisionMaster Div                                                                         
 ON AddC.numDivisionId = Div.numDivisionID                                   
 JOIN CompanyInfo  Comp                                                                         
 ON Div.numCompanyID = Comp.numCompanyId     
 OUTER APPLY(' + CONCAT('SELECT MAX(bintCreatedDate) AS bintCreatedDate FROM OpportunityMaster WHERE numDomainID = ',@numDomainID,' AND numDivisionID=Div.numDivisionID AND tintOppType = 1 AND tintOppStatus = 1') + ' ) AS TempLastOrder
 OUTER APPLY(' + CONCAT('SELECT
																	SUM(monDealAmount) AS monDealAmount
																	,AVG(ProfitPer) AS fltProfitPer
																FROM
																(
																	SELECT
																		OM.numDivisionId
																		,monDealAmount
																		,AVG(((ISNULL(OI.monPrice,0) - (CASE D.numCost WHEN 3 THEN ISNULL(V.monCost,0) * dbo.fn_UOMConversion(I.numBaseUnit,I.numItemCode,I.numDomainID,I.numPurchaseUnit) WHEN 2 THEN ISNULL(OI.numCost,0) ELSE ISNULL(I.monAverageCost,0) END)) / ISNULL(NULLIF(OI.monPrice,0),1)) * 100) ProfitPer
																	FROM
																		OpportunityMaster OM
																	INNER JOIN
																		Domain D
																	ON
																		OM.numDomainID = D.numDomainID
																	INNER JOIN
																		OpportunityItems OI
																	ON
																		OM.numOppId = OI.numOppId
																	INNER JOIN
																		Item I
																	ON
																		OI.numItemCode = I.numItemCode
																	LEFT JOIN
																		Vendor V
																	ON
																		V.numVendorID = I.numVendorID
																		AND V.numItemCode = I.numItemCode
																	WHERE
																		OM.numDomainId = ',@numDomainID,'
																		AND OM.numDivisionId = Div.numDivisionID
																		AND ISNULL(I.bitContainer,0) = 0
																		AND tintOppType = 1
																		AND tintOppStatus=1
																		AND 1 = (CASE ',@tintPerformanceFilter,' 
																				WHEN 1 THEN (CASE WHEN OM.bintCreatedDate BETWEEN DATEADD(MONTH,-3,GETUTCDATE()) AND GETUTCDATE() THEN 1 ELSE 0 END)
																				WHEN 2 THEN (CASE WHEN OM.bintCreatedDate BETWEEN DATEADD(MONTH,-6,GETUTCDATE()) AND GETUTCDATE() THEN 1 ELSE 0 END)
																				WHEN 3 THEN (CASE WHEN OM.bintCreatedDate BETWEEN DATEADD(YEAR,-1,GETUTCDATE()) AND GETUTCDATE() THEN 1 ELSE 0 END)
																				ELSE 1
																				END)
																	GROUP BY
																		OM.numDivisionId
																		,OM.numOppId
																		,monDealAmount
																) T1
																GROUP BY
																	T1.numDivisionId )') + ' AS TempPerformance
 Left Join listdetails listdetailsActivity                                                  
 On Comm.numActivity = listdetailsActivity.NumlistItemID                                               
 Left Join listdetails listdetailsStatus                                                 
 On Comm.numStatus = listdetailsStatus.NumlistItemID
 LEFT JOIN OpportunityMaster Opp ON Opp.numDivisionId = Div.numDivisionID AND Opp.numContactId = AddC.numContactId  AND Opp.numOppID = Comm.numOppID 
 LEFT JOIN Sales_process_List_Master splm ON splm.Slp_Id = Opp.numBusinessProcessID 
 LEFT JOIN ProjectProgress PP On PP.numOppId = Opp.numOppId '
 

DECLARE @Prefix AS VARCHAR(5)
 
 IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
BEGIN
	 if @vcCSLookBackTableName = 'AdditionalContactsInformation'                  
		set @Prefix = 'AddC.'                  
    else if @vcCSLookBackTableName = 'DivisionMaster'                  
		set @Prefix = 'Div.'
	else if @vcCSLookBackTableName = 'CompanyInfo'                  
		set @Prefix = 'Comp.'  
	ELSE IF @vcCSLookBackTableName='Tickler'	
		set @Prefix = 'Comm.'  
--	else if @vcCSLookBackTableName = 'ProjectProgress'                  
--		set @Prefix = 'pp.'  	
--	else if @vcCSLookBackTableName = 'OpportunityMaster'                  
--		set @Prefix = 'om.'  
--	else if @vcCSLookBackTableName = 'Sales_process_List_Master'                  
--		set @Prefix = 'splm.'  		
	else
		set @Prefix = ''   
	
	
	IF @vcCSAssociatedControlType='DateField'
	BEGIN
	IF @vcCSOrigDbCOlumnName='FormattedDate'
			SET @vcCSOrigDbCOlumnName='dtStartTime'
	
	
			set @strSql=@strSql+' left join #tempColorScheme tCS on CONVERT(VARCHAR(10),DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',' + @Prefix + @vcCSOrigDbCOlumnName + '),111) >= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue as int),GETUTCDATE()),111)
			 and CONVERT(VARCHAR(10),DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',' + @Prefix + @vcCSOrigDbCOlumnName + '),111) <= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue1 as int),GETUTCDATE()),111)'
	END
	ELSE IF @vcCSAssociatedControlType='SelectBox' OR @vcCSAssociatedControlType='ListBox' OR @vcCSAssociatedControlType='CheckBox'  OR @vcCSAssociatedControlType='TextBox'
	BEGIN
		--PRINT 1
		set @strSql=@strSql+' left join #tempColorScheme tCS on tCS.vcFieldValue=' + @Prefix + @vcCSOrigDbCOlumnName
	END
END

set @strSql=@strSql+'  WHERE Comm.numDomainID = ' + Cast(@numDomainID as varchar(10))  + ' AND 1 = (CASE ' + CAST( @tintActionItemsViewRights AS VARCHAR) + 
' WHEN 3 THEN 1 
WHEN 2 THEN (CASE WHEN Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+ CONVERT(VARCHAR(15),@numUserCntID) + ' ) THEN 1 ELSE 0 END)
WHEN 1 THEN (CASE WHEN (Comm.numCreatedBy= ' + CONVERT(VARCHAR(15),@numUserCntID) + ' or Comm.numAssign= '+ CONVERT(VARCHAR(15),@numUserCntID) + ') THEN 1 ELSE 0 END) 
ELSE 0 END) '
 
 IF LEN(ISNULL(@vcBProcessValue,''))>0
BEGIN
	SET @strSql = @strSql + ' and ISNULL(Opp.numBusinessProcessID,0) in (SELECT Items FROM dbo.Split(''' +@vcBProcessValue + ''','',''))' 			
END

IF LEN(@vcActionTypes) > 0 
BEGIN
	SET @strSql = @strSql + ' AND Comm.bitTask IN (' + @vcActionTypes + ')'  
END
  
IF @numCriteria  = 1 --Action Items	Assigned
BEGIN
	set @strSql=@strSql+' AND 1=(Case when Comm.numAssign IN (' + CASE WHEN LEN(@vcSelectedEmployess) > 0 THEN @vcSelectedEmployess ELSE Cast(@numUserCntID as varchar(10)) END +') then 1 else 0 end)'
END
ELSE IF @numCriteria = 2 --Action Items Owned
BEGIN
	set @strSql=@strSql+' AND 1=(Case when Comm.numCreatedBy IN (' + CASE WHEN LEN(@vcSelectedEmployess) > 0 THEN @vcSelectedEmployess ELSE Cast(@numUserCntID as varchar(10)) END +' ) then 1 else 0 end)'
END
ELSE --Action Items Assigned Or Owned
BEGIN
	set @strSql=@strSql+' AND 1=(Case when (Comm.numAssign IN (' + CASE WHEN LEN(@vcSelectedEmployess) > 0 THEN @vcSelectedEmployess ELSE Cast(@numUserCntID as varchar(10)) END +')  or Comm.numCreatedBy IN (' + CASE WHEN LEN(@vcSelectedEmployess) > 0 THEN @vcSelectedEmployess ELSE Cast(@numUserCntID as varchar(10)) END +')) then 1 else 0 end)'
END

        
  
IF(LEN(@RegularSearchCriteria)>0)
BEGIN
IF PATINDEX('%cmp.vcPerformance%', @RegularSearchCriteria)>0
BEGIN
	-- WE ARE MANAGING CONDITION IN OUTER APPLY
	set @strSql=@strSql
END
ELSE
BEGIN
	SET @strSql=@strSql+' AND '+@RegularSearchCriteria
END
END

IF LEN(@CustomSearchCriteria) > 0
BEGIN
	SET @strSql=@strSql +' AND '+ @CustomSearchCriteria
END
                                   
set @strSql=@strSql+' AND (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',dtStartTime) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',dtStartTime) <= '''+Cast(@endDate as varchar(30))+''')                                                     
AND Comm.bitclosedflag=0                                                
 and Comm.bitTask <> 973  ) As X '              

IF LEN(ISNULL(@vcBProcessValue,''))=0 AND LEN(@RegularSearchCriteria) = 0 AND LEN(@CustomSearchCriteria) = 0
BEGIN
	If LEN(ISNULL(@vcActionTypes,'')) = 0 OR CHARINDEX('98989898989898',@vcActionTypes) > 0
	BEGIN
		SET @strSql1 =  ' insert into #tempRecords 
select                          
 ACI.numContactID,ACI.numDivisionID,Div.tintCRMType,ac.activityid as id ,case when alldayevent = 1 then ''5'' else ''0'' end as bitTask,                        
DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', startdatetimeutc) startdate,                        
DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dateadd(second,duration,startdatetimeutc)) as endtime,                        
activitydescription as itemdesc,                        
ACI.vcFirstName + '' '' + ACI.vcLastName name, 
 ACI.vcFirstName,ACI.vcLastName,ACI.numPhone,ACI.numPhoneExtension,                   
 case when ACI.numPhone<>'''' then + ACI.numPhone +case when ACI.numPhoneExtension<>'''' then '' - '' + ACI.numPhoneExtension else '''' end  else '''' end as [Phone],                        
 Substring ( Comp.vcCompanyName , 0 , 100 ) + ''..'' As vcCompanyName , ISNULL((SELECT vcData FROM ListDetails WHERE numListID =21 AND numDomainID='+ CAST(@numDomainID AS VARCHAR) +' AND  numListItemID=Comp.vcProfile ) ,'''') vcProfile ,                      
vcEmail,                        
''Calendar'' as task,                        
[subject] as Activity,                        
case when importance =0 then ''Low'' when importance =1 then ''Normal'' when importance =2 then ''High'' end as status,                        
0 as numcreatedby,
'''' as numRecOwner, 
'''' AS numModifiedBy,                  
0 as numorgterid,       
'''' as numterid,                 
''-/-'' as numAssignedTo, 
''-'' AS numAssignedBy,        
''0'' as caseid,                        
null as vccasenumber,                        
0 as casetimeid,                        
0 as caseexpid,                        
1 as type ,                    
isnull(itemid,'''') as itemid  ,                    
changekey  ,cast(Null as datetime) as DueDate,cast(0 as bit) as bitFollowUpAnyTime,
dbo.GetListIemName(Comp.numNoOfEmployeesId) AS numNoOfEmployeesId,Div.vcComPhone,
dbo.GetListIemName(Div.numFollowUpStatus) AS numFollowUpStatus,' + CONCAT('ISNULL(dbo.FormatedDateFromDate(TempLastOrder.bintCreatedDate,',@numDomainID,'),'''')') + ' AS vcLastSalesOrderDate,ISNULL(TempPerformance.monDealAmount,0) monDealAmount, (CASE WHEN ISNULL(TempPerformance.monDealAmount,0) > 0 THEN CONCAT(FORMAT(TempPerformance.monDealAmount,''##,####.00''),'' ('',CAST(TempPerformance.fltProfitPer AS DECIMAL(18,2)),''%)'') ELSE '''' END) vcPerformance,'''' as vcColorScheme,'''',0,'''',0 AS [numOppId],0 AS numBusinessProcessID '
SET @strSql1 =@strSql1 + ' ,(select TOP 1 vcData from ListDetails where numListItemID=Comp.numCompanyRating) AS numCompanyRating ,(select TOP 1 vcData from ListDetails where numListItemID=Comp.numCompanyRating) AS numCompanyRating,''-'' AS numCampaignID '+@vcnullCustomColumnName
SET @strSql1 = @strSql1+' from activity ac join             
activityresource ar on  ac.activityid=ar.activityid             
join resource res on ar.resourceid=res.resourceid                         
--join usermaster UM on um.numUserId = res.numUserCntId                
join AdditionalContactsInformation ACI on ACI.numContactId = res.numUserCntId                
 JOIN DivisionMaster Div                                                          
 ON ACI.numDivisionId = Div.numDivisionID                                   
 JOIN CompanyInfo  Comp                             
 ON Div.numCompanyID = Comp.numCompanyId
 OUTER APPLY(' + CONCAT('SELECT MAX(bintCreatedDate) AS bintCreatedDate FROM OpportunityMaster WHERE numDomainID = ',@numDomainID,' AND numDivisionID=Div.numDivisionID AND tintOppType = 1 AND tintOppStatus = 1') + ' ) AS TempLastOrder                 
 OUTER APPLY(' + CONCAT('SELECT
																	SUM(monDealAmount) AS monDealAmount
																	,AVG(ProfitPer) AS fltProfitPer
																FROM
																(
																	SELECT
																		OM.numDivisionId
																		,monDealAmount
																		,AVG(((ISNULL(OI.monPrice,0) - (CASE D.numCost WHEN 3 THEN ISNULL(V.monCost,0) * dbo.fn_UOMConversion(I.numBaseUnit,I.numItemCode,I.numDomainID,I.numPurchaseUnit) WHEN 2 THEN ISNULL(OI.numCost,0) ELSE ISNULL(I.monAverageCost,0) END)) / ISNULL(NULLIF(OI.monPrice,0),1)) * 100) ProfitPer
																	FROM
																		OpportunityMaster OM
																	INNER JOIN
																		Domain D
																	ON
																		OM.numDomainID = D.numDomainID
																	INNER JOIN
																		OpportunityItems OI
																	ON
																		OM.numOppId = OI.numOppId
																	INNER JOIN
																		Item I
																	ON
																		OI.numItemCode = I.numItemCode
																	LEFT JOIN
																		Vendor V
																	ON
																		V.numVendorID = I.numVendorID
																		AND V.numItemCode = I.numItemCode
																	WHERE
																		OM.numDomainId = ',@numDomainID,'
																		AND OM.numDivisionId = Div.numDivisionID
																		AND ISNULL(I.bitContainer,0) = 0
																		AND tintOppType = 1
																		AND tintOppStatus=1
																		AND 1 = (CASE ',@tintPerformanceFilter,' 
																				WHEN 1 THEN (CASE WHEN OM.bintCreatedDate BETWEEN DATEADD(MONTH,-3,GETUTCDATE()) AND GETUTCDATE() THEN 1 ELSE 0 END)
																				WHEN 2 THEN (CASE WHEN OM.bintCreatedDate BETWEEN DATEADD(MONTH,-6,GETUTCDATE()) AND GETUTCDATE() THEN 1 ELSE 0 END)
																				WHEN 3 THEN (CASE WHEN OM.bintCreatedDate BETWEEN DATEADD(YEAR,-1,GETUTCDATE()) AND GETUTCDATE() THEN 1 ELSE 0 END)
																				ELSE 1
																				END)
																	GROUP BY
																		OM.numDivisionId
																		,OM.numOppId
																		,monDealAmount
																) T1
																GROUP BY
																	T1.numDivisionId )') + ' AS TempPerformance
 where  res.numUserCntId = ' + Cast(@numUserCntID as varchar(10)) +'             
and ac.OriginalStartDateTimeUtc IS NULL            
and res.numDomainID = ' + Cast(@numDomainID as varchar(10)) +'  and 
(DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', startdatetimeutc) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', startdatetimeutc) <= '''+Cast(@endDate as varchar(30))+''')
--(startdatetimeutc between dateadd(month,-1,getutcdate()) and dateadd(month,1,getutcdate())) 
and [status] <> -1                                                     
and ac.activityid not in 
(select distinct(numActivityId) from communication Comm where Comm.numDomainID = ' + Cast(@numDomainID as varchar(10)) +'  
AND 1=(Case ' + Cast(@bitFilterRecord as varchar(10)) +' when 1 then Case when Comm.numAssign =' + Cast(@numUserCntID as varchar(10)) +'  then 1 else 0 end 
		else  Case when (Comm.numAssign =' + Cast(@numUserCntID as varchar(10)) +'  or Comm.numCreatedBy=' + Cast(@numUserCntID as varchar(10)) +' ) then 1 else 0 end end)   
		AND 1 = (CASE ' + CAST( @tintActionItemsViewRights AS VARCHAR) + 
' WHEN 3 THEN 1 
WHEN 2 THEN (CASE WHEN Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+ CONVERT(VARCHAR(15),@numUserCntID) + ' ) THEN 1 ELSE 0 END)
WHEN 1 THEN (CASE WHEN (Comm.numCreatedBy= ' + CONVERT(VARCHAR(15),@numUserCntID) + ' or Comm.numAssign= '+ CONVERT(VARCHAR(15),@numUserCntID) + ') THEN 1 ELSE 0 END) 
ELSE 0 END)                                                                   
-- AND (dtStartTime >= '''+Cast(@startDate as varchar(30))+''' and dtStartTime <= '''+Cast(@endDate as varchar(30))+''') 
) 
 Order by endtime'
 
	END

-- btDocType =2 for Document approval request, =1 for bizdoc
SET @strSql2 = ' insert into #tempRecords
select 
B.numContactID,B.numDivisionID,E.tintCRMType,CASE BA.btDocType WHEN 2 THEN BA.numOppBizDocsId WHEN 3 THEN BA.numOppBizDocsId ELSE A.numBizActionID END as ID,
convert(varchar(15),A.bitTask) as bitTask,
DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dtCreatedDate) as Startdate,                        
DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dtCreatedDate) as EndTime ,  
'''' as itemDesc,
vcFirstName + '' '' + vcLastName  as [Name],
B.vcFirstName , B.vcLastName ,B.numPhone,B.numPhoneExtension,  
case when B.numPhone<>'''' then + B.numPhone +case when B.numPhoneExtension<>'''' then '' - '' + B.numPhoneExtension else '''' end  else '''' end as [Phone],
vcCompanyName, ISNULL((SELECT vcData FROM ListDetails WHERE numListID =21 AND numDomainID='+ CAST(@numDomainID AS VARCHAR) +' AND  numListItemID=D.vcProfile ) ,'''') vcProfile ,
vcEmail,
CASE BA.btDocType WHEN 2  THEN  ''Document Approval Request'' ELSE ''BizDoc Approval Request'' END as Task,
dbo.GetListIemName(bitTask) as Activity,
'''' as Status,
A.numCreatedBy,
dbo.fn_GetContactName(A.numCreatedBy) as numRecOwner,
dbo.fn_GetContactName(A.numModifiedBy) as numModifiedBy,
 ISNULL(E.numTerId,0) AS numOrgTerId, 
 (select TOP 1 vcData from ListDetails where numListItemID=E.numTerId) AS numTerId, 
case When Len( dbo.fn_GetContactName(numAssign) ) > 12                                           
  Then Substring( dbo.fn_GetContactName(numAssign) , 0  ,  12 ) + ''..''                                           
  Else dbo.fn_GetContactName(numAssign)                                           
 end                                          
 numAssignedTo ,  
 ''-'' AS numAssignedBy,        
''0'' as caseid,
''0'' as vcCasenumber,
0 as CaseTimeId,
0 as CaseExpId,
CASE BA.btDocType WHEN 2 THEN 2 ELSE 3 END as [type], 
'''' AS itemid  ,      
 '''' AS changekey ,   
dtCreatedDate as DueDate  ,cast(0 as bit) as bitFollowUpAnyTime,
dbo.GetListIemName(D.numNoOfEmployeesId) AS numNoOfEmployeesId,E.vcComPhone,
dbo.GetListIemName(E.numFollowUpStatus) AS numFollowUpStatus,' + CONCAT('ISNULL(dbo.FormatedDateFromDate(TempLastOrder.bintCreatedDate,',@numDomainID,'),'''')') + ' AS vcLastSalesOrderDate,ISNULL(TempPerformance.monDealAmount,0) monDealAmount, (CASE WHEN ISNULL(TempPerformance.monDealAmount,0) > 0 THEN CONCAT(FORMAT(TempPerformance.monDealAmount,''##,####.00''),'' ('',CAST(TempPerformance.fltProfitPer AS DECIMAL(18,2)),''%)'') ELSE '''' END) AS vcPerformance,'''' as vcColorScheme,'''',0,'''', 0 AS [numOppId],0 AS numBusinessProcessID '
SET @strSql2 =@strSql2 + ' ,(select TOP 1 vcData from ListDetails where numListItemID=D.numCompanyRating) AS numCompanyRating ,(select TOP 1 vcData from ListDetails where numListItemID=D.numCompanyRating) AS numCompanyRating,''-'' AS numCampaignID '+@vcnullCustomColumnName
SET @strSql2 =@strSql2 +' 
FROM 
	BizDocAction A 
LEFT JOIN 
	dbo.BizActionDetails BA 
ON 
	BA.numBizActionId = A.numBizActionId, 
AdditionalContactsInformation B, 
CompanyInfo D,
DivisionMaster E
OUTER APPLY(' + CONCAT('SELECT MAX(bintCreatedDate) AS bintCreatedDate FROM OpportunityMaster WHERE numDomainID = ',@numDomainID,' AND numDivisionID=E.numDivisionID AND tintOppType = 1 AND tintOppStatus = 1') + ' ) AS TempLastOrder
OUTER APPLY(' + CONCAT('SELECT
																	SUM(monDealAmount) AS monDealAmount
																	,AVG(ProfitPer) AS fltProfitPer
																FROM
																(
																	SELECT
																		OM.numDivisionId
																		,monDealAmount
																		,AVG(((ISNULL(OI.monPrice,0) - (CASE D.numCost WHEN 3 THEN ISNULL(V.monCost,0) * dbo.fn_UOMConversion(I.numBaseUnit,I.numItemCode,I.numDomainID,I.numPurchaseUnit) WHEN 2 THEN ISNULL(OI.numCost,0) ELSE ISNULL(I.monAverageCost,0) END)) / ISNULL(NULLIF(OI.monPrice,0),1)) * 100) ProfitPer
																	FROM
																		OpportunityMaster OM
																	INNER JOIN
																		Domain D
																	ON
																		OM.numDomainID = D.numDomainID
																	INNER JOIN
																		OpportunityItems OI
																	ON
																		OM.numOppId = OI.numOppId
																	INNER JOIN
																		Item I
																	ON
																		OI.numItemCode = I.numItemCode
																	LEFT JOIN
																		Vendor V
																	ON
																		V.numVendorID = I.numVendorID
																		AND V.numItemCode = I.numItemCode
																	WHERE
																		OM.numDomainId = ',@numDomainID,'
																		AND OM.numDivisionId = E.numDivisionID
																		AND ISNULL(I.bitContainer,0) = 0
																		AND tintOppType = 1
																		AND tintOppStatus=1
																		AND 1 = (CASE ',@tintPerformanceFilter,' 
																				WHEN 1 THEN (CASE WHEN OM.bintCreatedDate BETWEEN DATEADD(MONTH,-3,GETUTCDATE()) AND GETUTCDATE() THEN 1 ELSE 0 END)
																				WHEN 2 THEN (CASE WHEN OM.bintCreatedDate BETWEEN DATEADD(MONTH,-6,GETUTCDATE()) AND GETUTCDATE() THEN 1 ELSE 0 END)
																				WHEN 3 THEN (CASE WHEN OM.bintCreatedDate BETWEEN DATEADD(YEAR,-1,GETUTCDATE()) AND GETUTCDATE() THEN 1 ELSE 0 END)
																				ELSE 1
																				END)
																	GROUP BY
																		OM.numDivisionId
																		,OM.numOppId
																		,monDealAmount
																) T1
																GROUP BY
																	T1.numDivisionId )') + ' AS TempPerformance
WHERE A.numContactId=b.numContactId and a.numDomainID=b.numDomainID  and 
D.numCompanyId=E.numCompanyId and 
D.numDomainID=a.numDomainID and 
E.numDivisionId=B.numDivisionId and 
a.numDomainID= ' + Cast(@numDomainID as varchar(10)) +' and a.numContactId = ' + Cast(@numUserCntID as varchar(10)) +'  and  
A.numStatus=0 
and (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dtCreatedDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dtCreatedDate) <= '''+Cast(@endDate as varchar(30))+''')'

END


declare @Nocolumns as tinyint                
set @Nocolumns=0       
         
Select @Nocolumns=isnull(count(*),0) from View_DynamicColumns where numFormId=43 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 
  
  
if @Nocolumns > 0            
begin      
INSERT INTO #tempForm
select tintRow+1 as tintOrder,vcDbColumnName,vcFieldDataType,vcOrigDbColumnName,bitAllowFiltering,ISNULL(vcCultureFieldName,vcFieldName),                  
 vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName                                                 
,bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth
 FROM View_DynamicColumns 
 where numFormId=43 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 
  AND ISNULL(bitSettingField,0)=1 AND ISNULL(bitCustom,0)=0
  order by tintOrder asc  
  
end            
else            
begin 

	--Check if Master Form Configuration is created by administrator if NoColumns=0
	DECLARE @IsMasterConfAvailable BIT = 0
	DECLARE @numUserGroup NUMERIC(18,0)

	SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

	IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = 43 AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
	BEGIN
		SET @IsMasterConfAvailable = 1
	END

	--If MasterConfiguration is available then load it otherwise load default columns
	IF @IsMasterConfAvailable = 1
	BEGIN
		INSERT INTO DycFormConfigurationDetails 
		(
			numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
		)
		SELECT 
			43,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,NULL,1,0,intColumnWidth
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=43 AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0

		INSERT INTO #tempForm
		SELECT 
			(intRowNum + 1) as tintOrder, vcDbColumnName,vcFieldDataType,vcOrigDbColumnName,bitAllowFiltering, ISNULL(vcCultureFieldName,vcFieldName), vcAssociatedControlType,
			vcListItemType,numListID,vcLookBackTableName, bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=43 AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
	ORDER BY 
		tintOrder ASC  
	END
	ELSE                                        
	BEGIN
		 INSERT INTO #tempForm
		select tintOrder,vcDbColumnName,vcFieldDataType
		,vcOrigDbColumnName,bitAllowFiltering,ISNULL(vcCultureFieldName,vcFieldName),                  
		 vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName                                                 
		,bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
		bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
		intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth
		 FROM View_DynamicDefaultColumns
		 where numFormId=43 and bitDefault=1 AND ISNULL(bitSettingField,0)=1 AND numDomainID=@numDomainID
		order by tintOrder asc  
	END 
end                                            




DECLARE @strSql3 AS VARCHAR(MAX)

SET @strSql3=   ' select  * from #tempRecords '

IF CHARINDEX(@columnName,@strSql) > 0 OR CHARINDEX(@columnName,@strSql1) > 0 OR CHARINDEX(@columnName,@strSql2) > 0
BEGIN
	SET @strSql3=@strSql3+' order by ' + @columnName +' ' + @columnSortOrder
END


 PRINT @strSql
exec (@strSql + @strSql1 + @strSql2 + @strSql3 )
drop table #tempRecords

SELECT * FROM #tempForm order by tintOrder

DROP TABLE #tempForm

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetPromotionsForSimilarItems')
-- exec USP_GetPromotionsForSimilarItems 72,170564,0
DROP PROCEDURE USP_GetPromotionsForSimilarItems 
GO
CREATE PROCEDURE USP_GetPromotionsForSimilarItems 
	@numDomainID NUMERIC(18,0),
	@numItemCode NUMERIC(18,0),
	@numWarehouseItemID AS NUMERIC(18,0),
	@numSiteID AS NUMERIC(18,0)
AS 
BEGIN
	--SELECT  * FROM PromotionOfferItems POI 
	--JOIN Promotionoffer PO ON PO.numProId = POI.numProId 
	--WHERE numValue = @numItemCode
	DECLARE @numItemClassification AS NUMERIC(18,0), @numShippingCountry AS NUMERIC(18,0)

	-- GET ITEM DETAILS
	SELECT
		@numItemClassification = numItemClassification
	FROM
		Item
	OUTER APPLY
	(
		SELECT TOP 1 numWareHouseItemID,monWListPrice FROM WareHouseItems WHERE numItemID=@numItemCode AND (numWareHouseItemID = @numWarehouseItemID OR ISNULL(@numWarehouseItemID,0) = 0)
	) AS WareHouseItems
	WHERE
		Item.numItemCode = @numItemCode

	SELECT @numShippingCountry = numFreeShippingCountry FROM ShippingPromotions WHERE numDomainId = @numDomainID 

	-- CHECK IF PROMOTION CAN BE TRIGGERED FROM THIS ITEM
	DECLARE @numPromotionID AS NUMERIC(18,0)

	SELECT TOP 1
		@numPromotionID = numProId 
	FROM 
		PromotionOffer PO
		left JOIN PromotionOfferSites POS ON PO.numProId=POS.numPromotionID
	WHERE 
		numDomainId=@numDomainID 
		AND POS.numSiteID = @numSiteID 
		AND ISNULL(bitEnabled,0)=1
		--AND ISNULL(bitApplyToInternalOrders,0)=1 
		AND 1 = (CASE WHEN bitRequireCouponCode = 1 THEN CASE WHEN ISNULL(intCouponCodeUsed,0) < ISNULL(tintUsageLimit,0) OR ISNULL(tintUsageLimit,0) = 0  THEN 1 ELSE 0 END ELSE 1 END)
		AND 1 = (CASE WHEN bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN CAST(GETDATE() AS DATE)>=dtValidFrom AND CAST(GETDATE() AS DATE)<=dtValidTo THEN 1 ELSE 0 END) END)
		AND 1 = (CASE 
					WHEN tintOfferBasedOn = 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 AND numValue=@numItemCode) > 0 THEN 1 ELSE 0 END)
					WHEN tintOfferBasedOn = 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=2 AND numValue=@numItemClassification) > 0 THEN 1 ELSE 0 END)
					ELSE 0
				END)
	ORDER BY
		CASE 
			--WHEN ISNULL(PO.bitRequireCouponCode,0)=1 AND PO.txtCouponCode=@vcCoupon THEN 1			
			WHEN PO.tintCustomersBasedOn = 1 AND PO.tintOfferBasedOn = 2 THEN 2
			WHEN PO.tintCustomersBasedOn = 1 AND PO.tintOfferBasedOn = 4 THEN 3
			WHEN PO.tintCustomersBasedOn = 2 AND PO.tintOfferBasedOn = 1 THEN 4
			WHEN PO.tintCustomersBasedOn = 2 AND PO.tintOfferBasedOn = 2 THEN 5
			WHEN PO.tintCustomersBasedOn = 2 AND PO.tintOfferBasedOn = 4 THEN 6
			WHEN PO.tintCustomersBasedOn = 3 AND PO.tintOfferBasedOn = 1 THEN 7
			WHEN PO.tintCustomersBasedOn = 3 AND PO.tintOfferBasedOn = 2 THEN 8
			WHEN PO.tintCustomersBasedOn = 3 AND PO.tintOfferBasedOn = 4 THEN 9
		END


	IF ISNULL(@numPromotionID,0) > 0
	BEGIN
		-- Get Top 1 Promotion Based on priority

		SELECT 
			numProId
			,vcProName
			,bitNeverExpires
			,bitRequireCouponCode
			,txtCouponCode
			,tintUsageLimit
			,intCouponCodeUsed
			,SP.bitFreeShiping
			,SP.monFreeShippingOrderAmount
			,SP.numFreeShippingCountry
			,SP.bitFixShipping1
			,SP.monFixShipping1OrderAmount
			,SP.monFixShipping1Charge
			,SP.bitFixShipping2
			,SP.monFixShipping2OrderAmount
			,SP.monFixShipping2Charge
			,tintOfferTriggerValueType
			,fltOfferTriggerValue
			,tintOfferBasedOn
			,fltDiscountValue
			,tintDiscountType
			,tintDiscoutBaseOn
			,dbo.FormatedDateFromDate(dtValidTo,@numDomainID) AS dtExpire
			,(CASE tintOfferBasedOn
				WHEN 1 THEN (SELECT Stuff((SELECT CONCAT(',',numValue) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 FOR XML PATH('')), 1, 1, ''))
				WHEN 2 THEN (SELECT Stuff((SELECT CONCAT(',',numValue) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=2 FOR XML PATH('')), 1, 1, ''))
			END) As vcPromotionItems
			,(CASE tintDiscoutBaseOn
				WHEN 1 THEN (SELECT Stuff((SELECT CONCAT(',',numValue) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=6 AND tintType=1 FOR XML PATH('')), 1, 1, ''))
				WHEN 2 THEN (SELECT Stuff((SELECT CONCAT(',',numValue) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=6 AND tintType=2 FOR XML PATH('')), 1, 1, ''))
				WHEN 3 THEN (SELECT Stuff((SELECT CONCAT(',',SimilarItems.numItemCode) FROM SimilarItems WHERE 1 = (CASE 
																														WHEN tintOfferBasedOn=1 THEN CASE WHEN numParentItemCode IN (SELECT numValue FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1) THEN 1 ELSE 0 END
																														WHEN tintOfferBasedOn=2 THEN CASE WHEN numParentItemCode IN (SELECT numItemCode FROM Item WHERE numDomainID=@numDomainID AND numItemClassification IN (SELECT numValue FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=2)) THEN 1 ELSE 0 END
																														ELSE 0
																													END) FOR XML PATH('')), 1, 1, ''))
			END) As vcItems
			,CONCAT('Buy '
					,CASE WHEN tintOfferTriggerValueType=1 THEN CAST(fltOfferTriggerValue AS VARCHAR) ELSE CONCAT('$',fltOfferTriggerValue) END
					,CASE tintOfferBasedOn
							WHEN 1 THEN CONCAT(' of "',(SELECT Stuff((SELECT CONCAT(', ',Item.vcItemName) FROM PromotionOfferItems INNER JOIN Item ON PromotionOfferItems.numValue = Item.numItemCode WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 FOR XML PATH('')), 1, 2, '')), '"')
							WHEN 2 THEN CONCAT(' of items belonging to classification(s) "',(SELECT Stuff((SELECT CONCAT(', ',ListDetails.vcData) FROM PromotionOfferItems INNER JOIN ListDetails ON PromotionOfferItems.numValue = ListDetails.numListItemID WHERE numListID=36 AND numProId=PO.numProId AND tintRecordType=5 AND tintType=2 FOR XML PATH('')), 1, 2, '')), '"')
						END
					,' & get '
					, CASE tintDiscountType WHEN 1 THEN CONCAT(fltDiscountValue,'% off on') WHEN 2 THEN CONCAT('$',fltDiscountValue, ' off on') WHEN 3 THEN CONCAT(fltDiscountValue,' of') END
					, CASE 
						WHEN tintDiscoutBaseOn = 1 THEN 
							CONCAT
							(
								CONCAT(' "',(SELECT Stuff((SELECT CONCAT(',',Item.vcItemName) FROM PromotionOfferItems INNER JOIN Item ON PromotionOfferItems.numValue = Item.numItemCode WHERE numProId=PO.numProId AND tintRecordType=6 AND tintType=1 FOR XML PATH('')), 1, 1, '')), '"')
								,(CASE WHEN PO.tintDiscountType = 3 THEN ' free' ELSE '' END)
								,'.'
							)
						WHEN tintDiscoutBaseOn = 2 THEN 
							CONCAT
							(
								CONCAT(' items belonging to classification(s) "',(SELECT Stuff((SELECT CONCAT(',',ListDetails.vcData) FROM PromotionOfferItems INNER JOIN ListDetails ON PromotionOfferItems.numValue = ListDetails.numListItemID WHERE numListID=36 AND numProId=PO.numProId AND tintRecordType=6 AND tintType=2 FOR XML PATH('')), 1, 1, '')), '"')
								,(CASE WHEN PO.tintDiscountType = 3 THEN ' free' ELSE '' END)
								,'.'
							)
						WHEN tintDiscoutBaseOn = 3 THEN ' related items.'
						ELSE '' 
					END
				) AS vcPromotionDescription
				,CONCAT
				(
					(CASE WHEN ISNULL(SP.bitFixShipping1,0)=1 THEN CONCAT('Spend $',SP.monFixShipping1OrderAmount,' and get $',SP.monFixShipping1Charge,' shipping. ') ELSE '' END)
					,(CASE WHEN ISNULL(SP.bitFixShipping2,0)=1  THEN CONCAT('Spend $',SP.monFixShipping2OrderAmount,' and get $',SP.monFixShipping2Charge,' shipping. ') ELSE '' END)
					,(CASE WHEN (SP.bitFreeShiping=1 AND SP.numFreeShippingCountry=@numShippingCountry) THEN CONCAT('Spend $',SP.monFreeShippingOrderAmount,' and shipping is FREE! ') ELSE '' END) 
				) AS vcShippingDescription,
				CASE 
					--WHEN ISNULL(PO.bitRequireCouponCode,0)=1 AND PO.txtCouponCode=@vcCoupon THEN 1
					WHEN PO.tintCustomersBasedOn = 1 AND PO.tintOfferBasedOn = 1 THEN 1
					WHEN PO.tintCustomersBasedOn = 1 AND PO.tintOfferBasedOn = 2 THEN 2
					WHEN PO.tintCustomersBasedOn = 1 AND PO.tintOfferBasedOn = 4 THEN 3
					WHEN PO.tintCustomersBasedOn = 2 AND PO.tintOfferBasedOn = 1 THEN 4
					WHEN PO.tintCustomersBasedOn = 2 AND PO.tintOfferBasedOn = 2 THEN 5
					WHEN PO.tintCustomersBasedOn = 2 AND PO.tintOfferBasedOn = 4 THEN 6
					WHEN PO.tintCustomersBasedOn = 3 AND PO.tintOfferBasedOn = 1 THEN 7
					WHEN PO.tintCustomersBasedOn = 3 AND PO.tintOfferBasedOn = 2 THEN 8
					WHEN PO.tintCustomersBasedOn = 3 AND PO.tintOfferBasedOn = 4 THEN 9
				END AS tintPriority
		FROM
			PromotionOffer PO
			left JOIN PromotionOfferSites POS ON PO.numProId=POS.numPromotionID
		OUTER APPLY
		(
			SELECT 
				 bitFreeShiping
				,monFreeShippingOrderAmount
				,numFreeShippingCountry
				,bitFixShipping1
				,monFixShipping1OrderAmount
				,monFixShipping1Charge
				,bitFixShipping2
				,monFixShipping2OrderAmount
				,monFixShipping2Charge
			FROM
				ShippingPromotions 
			WHERE
				ShippingPromotions.numDomainId = @numDomainID 
		) AS SP
		WHERE
			numDomainId=@numDomainID 
			AND numProId = @numPromotionID
			AND POS.numSiteID = @numSiteID 
	END
END
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetShippingPromotions' ) 
    DROP PROCEDURE USP_GetShippingPromotions
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE  [dbo].[USP_GetShippingPromotions]
    @numDomainID AS NUMERIC(9) = 0,
	@byteMode TinyInt=0,
	@numUserCntId NUMERIC = 0,
	@numShippingCountry AS NUMERIC(18,0)=0
AS 
BEGIN
	IF @byteMode = 0
    BEGIN 
		SELECT  monFreeShippingOrderAmount, * FROM ShippingPromotions WHERE numDomainID = @numDomainID
	END
	ELSE IF @byteMode = 1
	BEGIN
		DECLARE @vcShippingDescription VARCHAR(MAX)
		DECLARE @vcQualifiedShipping VARCHAR(MAX)
		DECLARE @decmPrice DECIMAL(18,2)
			--SET @decmPrice=(select ISNULL(SUM(monTotAmount),0) from CartItems where numDomainId = @numDomainID AND vcCookieId = @cookieId AND numUserCntId = @numUserCntId)
			SET @decmPrice=(select ISNULL(SUM(monTotAmount),0) from CartItems where numDomainId = @numDomainID AND numUserCntId = @numUserCntId)

		SELECT @vcShippingDescription=(CONCAT
					(
						(CASE WHEN ISNULL(bitFixShipping1,0)=1 AND @decmPrice < monFixShipping1OrderAmount THEN CONCAT('Spend $',(monFixShipping1OrderAmount-@decmPrice), CASE WHEN @decmPrice>0 THEN ' more ' ELSE '' END+' and get $',monFixShipping1Charge,' shipping. ') ELSE '' END)
						,(CASE WHEN ISNULL(bitFixShipping2,0)=1 AND @decmPrice < monFixShipping2OrderAmount THEN CONCAT('Spend $',monFixShipping2OrderAmount-@decmPrice, CASE WHEN @decmPrice>0 THEN ' more ' ELSE '' END+' and get $',monFixShipping2Charge,' shipping. ') ELSE '' END)
						,(CASE WHEN (bitFreeShiping=1 AND numFreeShippingCountry=@numShippingCountry) AND @decmPrice<monFreeShippingOrderAmount THEN CONCAT('Spend $',monFreeShippingOrderAmount-@decmPrice, CASE WHEN @decmPrice>0 THEN ' more ' ELSE '' END+' and shipping is FREE! ') ELSE '' END) 
					
					)),
					@vcQualifiedShipping=((CASE WHEN ISNULL(bitFixShipping1,0)=1 AND @decmPrice > monFixShipping1OrderAmount AND 1=(CASE WHEN ISNULL(bitFixShipping2,0)=1 AND @decmPrice<monFixShipping2OrderAmount THEN 1 WHEN ISNULL(bitFixShipping2,0)=0 THEN 1 ELSE 0 END) THEN 'You are qualified for $'+CAST(monFixShipping1Charge AS varchar)+' shipping'
							   WHEN ISNULL(bitFixShipping2,0)=1 AND @decmPrice > monFixShipping2OrderAmount  AND 1=(CASE WHEN ISNULL(bitFreeShiping,0)=1 AND @decmPrice<monFreeShippingOrderAmount THEN 1 WHEN ISNULL(bitFreeShiping,0)=0 THEN 1 ELSE 0 END) THEN 'You are qualified for $'+CAST(monFixShipping2Charge AS varchar)+' shipping'
							   WHEN (ISNULL(bitFreeShiping,0)=1 AND numFreeShippingCountry=@numShippingCountry AND @decmPrice>monFreeShippingOrderAmount) THEN 'You are qualified for Free Shipping' END
						))
			FROM
				PromotionOffer PO
				LEFT JOIN ShippingPromotions SP ON PO.numDomainId = SP.numDomainId
			WHERE
				PO.numDomainId=@numDomainID 

		SELECT @vcShippingDescription AS ShippingDescription, @vcQualifiedShipping AS QualifiedShipping
	END
END  
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ManagePromotionOfferCustomersDTL' ) 
    DROP PROCEDURE USP_ManagePromotionOfferCustomersDTL
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[USP_ManagePromotionOfferCustomersDTL]    
	@numProID			AS NUMERIC(9) = 0,  
	@numProOrgId		AS NUMERIC(9)=0,    
	@tintRuleAppType	AS TINYINT,  
	@numDivisionID		AS NUMERIC(9)=0,
	@numRelationship	AS NUMERIC(9)=0,  
	@numProfile			AS NUMERIC(9)=0,  
	@byteMode			AS TINYINT,
	@numDomainID		AS NUMERIC(9)=0  
AS    
	IF @byteMode = 0  
	BEGIN  
		IF NOT EXISTS(SELECT * FROM PromotionOfferorganizations 
			WHERE numProId = @numProId  
				AND numDivisionID = @numDivisionID 
				AND numRelationship = @numRelationship 
				AND numProfile = @numProfile 
				AND tintType = @tintRuleAppType)
		BEGIN
			IF @tintRuleAppType = 1 
			BEGIN
				DELETE FROM PromotionOfferorganizations 
				WHERE tintType = 2 
					AND numProId = @numProId 
					AND numDivisionID <> @numDivisionID 
			END
			ELSE IF @tintRuleAppType = 2 
			BEGIN
				DELETE FROM PromotionOfferorganizations 
					WHERE tintType = 1 
					AND numProId = @numProId 
					AND numDivisionID <> @numDivisionID 
			END
			
			/*Check For Duplicates 
			1- The customer is specific (level 1), and the item is specific (also level 1). If the user tries to create another rule that targets the customer by name, or that same item by name, validation would say �You�ve already created a promotion rule that targets this customer and item by name�*/
			/*IF (SELECT COUNT(*) 
				FROM PromotionOfferorganizations POO
					JOIN PromotionOfferItems POI ON POI.numProId = POO.numProId
					JOIN PromotionOffer PO ON PO.numProId = POI.numProId
				WHERE ISNULL(POO.numDivisionID,0) = @numDivisionID 
					AND POI.[numValue] IN (SELECT numValue 
												FROM [PromotionOfferItems] 
												WHERE numProID = @numProID)
					AND PO.[numDomainID] = @numDomainID) > 0
			BEGIN
				RAISERROR ( 'DUPLICATE-LEVEL1',16, 1 )
				RETURN ;
			END*/
			
			/* 2- The customer is targeted at the group level (level 2), and the item is specific (level 1). If the user tries to create another rule that targets this same Relationship / Profile combination and that same item in level 1 (L2 customer and L1 item both match), validation would say �You�ve already created a promotion rule that targets this relationship / profile and one or more item specifically�*/
			/*IF (SELECT COUNT(*) 
				FROM PromotionOfferorganizations POO
					JOIN PromotionOfferItems POI ON POI.numProId = POO.numProId
					JOIN PromotionOffer PO ON PO.numProId = POI.numProId
				WHERE ISNULL(POO.numRelationship,0) = @numRelationship
					AND ISNULL(POO.numProfile,0) = @numProfile
					AND POI.[numValue] IN (SELECT numValue 
												FROM [PromotionOfferItems] 
												WHERE numProID = @numProID)
					AND PO.[numDomainID] = @numDomainID) > 0
			BEGIN
				RAISERROR ( 'DUPLICATE-LEVEL2',16, 1 )
				RETURN ;
			END*/

			INSERT INTO [PromotionOfferorganizations] 
				(numProId, numDivisionID, numRelationship, numProfile, tintType) 
			VALUES (@numProId, @numDivisionID, @numRelationship, @numProfile, @tintRuleAppType)
		END  
		ELSE
		BEGIN
			RAISERROR ( 'DUPLICATE',16, 1 )
			RETURN ;
		END
	END
	ELSE IF @byteMode = 1  
	BEGIN  
		DELETE FROM PromotionOfferorganizations 
		WHERE numProOrgId = @numProOrgId 
	END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_PromotionOffer_EnableDisable')
DROP PROCEDURE USP_PromotionOffer_EnableDisable
GO
CREATE PROCEDURE [dbo].[USP_PromotionOffer_EnableDisable]
	@numDomainID NUMERIC(18,0),
	@numProId NUMERIC(18,0)
AS
BEGIN
	UPDATE
		PromotionOffer
	SET
		bitEnabled = (CASE WHEN ISNULL(bitEnabled,0)=1 THEN 0 ELSE 1 END)
	WHERE	
		numDomainId=@numDomainID
		AND numProId=@numProId


	IF EXISTS(SELECT * FROM PromotionOfferOrderBased WHERE numProID = @numProId AND numDomainId = @numDomainID)
	BEGIN
		UPDATE PromotionOfferOrderBased 
		SET bitEnabled = (CASE WHEN ISNULL(bitEnabled,0)=1 THEN 0 ELSE 1 END)
		WHERE numDomainId=@numDomainID
			AND numProId=@numProId
	END
END
