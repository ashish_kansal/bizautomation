/******************************************************************
Project: Release 7.7 Date: 14.JULY.2017
Comments: ALTER SCRIPTS
*******************************************************************/

/******************   SANDEEP  *****************/

BEGIN TRY
BEGIN TRANSACTION

DECLARE @numFieldID NUMERIC(18,0)

INSERT INTO DycFieldMaster
(
	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAllowFiltering
)
VALUES
(
	3,'Invoiced','vcInvoiced','vcInvoiced','OpportunityItems','N','R','Label',0,0,0,0,0,0,0
)

SET @numFieldID = SCOPE_IDENTITY()

INSERT INTO ReportFieldGroupMappingMaster
(
	numReportFieldGroupID,numFieldID,vcFieldName,vcAssociatedControlType,vcFieldDataType,bitAllowSorting,bitAllowGrouping,bitAllowAggregate,bitAllowFiltering
)
VALUES
(
	4,@numFieldID,'Invoiced','Label','N',1,0,1,0
)

INSERT INTO DycFieldMaster
(
	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAllowFiltering
)
VALUES
(
	3,'Backordered','vcBackordered','vcBackordered','OpportunityItems','N','R','Label',0,0,0,0,0,0,0
)

SET @numFieldID = SCOPE_IDENTITY()

INSERT INTO ReportFieldGroupMappingMaster
(
	numReportFieldGroupID,numFieldID,vcFieldName,vcAssociatedControlType,vcFieldDataType,bitAllowSorting,bitAllowGrouping,bitAllowAggregate,bitAllowFiltering
)
VALUES
(
	4,@numFieldID,'Backordered','Label','N',1,0,1,0
)

INSERT INTO DycFieldMaster
(
	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAllowFiltering
)
VALUES
(
	3,'Open Balance','vcOpenBalance','vcOpenBalance','OpportunityItems','N','R','Label',0,0,0,0,0,0,0
)

SET @numFieldID = SCOPE_IDENTITY()

INSERT INTO ReportFieldGroupMappingMaster
(
	numReportFieldGroupID,numFieldID,vcFieldName,vcAssociatedControlType,vcFieldDataType,bitAllowSorting,bitAllowGrouping,bitAllowAggregate,bitAllowFiltering
)
VALUES
(
	4,@numFieldID,'Open Balance','Label','N',1,0,1,0
)

COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH


--ALTER TABLE DepositeDetails ADD numTempOrigDepositID UNIQUEIDENTIFIER

ALTER TABLE Domain ADD bitElasticSearch BIT DEFAULT 1

USE [Production.2014]
GO

/****** Object:  Table [dbo].[ElasticSearchModifiedRecords]    Script Date: 10-Jul-17 10:41:46 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ElasticSearchModifiedRecords](
	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numDomainID] [numeric](18, 0) NOT NULL,
	[vcModule] [varchar](15) NOT NULL,
	[numRecordID] [numeric](18, 0) NOT NULL,
	[vcAction] [varchar](10) NOT NULL,
 CONSTRAINT [PK_ElasticSearchModifiedRecords] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING ON
GO


/******************   PRASANT  *****************/

ALTER TABLE Domain ADD numAuthorizePercentage NUMERIC(18,0) DEFAULT 0


ALTER TABLE OpportunityBizDocs ADD numSourceBizDocId NUMERIC(18,0)

/******************   ANUSHA  *****************/

BEGIN TRY
BEGIN TRANSACTION
begin
 --add new values for slide 1
 DECLARE @numFieldID AS NUMERIC(18,0)
 DECLARE @numFormFieldID AS NUMERIC(18,0)

 INSERT INTO DycFieldMaster
 (numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,[order],bitInResults,
 bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
 VALUES
 (6,'To','vcTo','vcTo','EmailHistory','V','R','TextArea','',0,10,1,0,0,1,1,0,0,1)
 
 SELECT @numFieldID = SCOPE_IDENTITY()

 INSERT INTO DynamicFormFieldMaster
 (numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,vcListItemType,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,
 vcLookBackTableName,bitDefault,[order],bitInResults,numDomainID,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
 VALUES
 (44,'To','R','TextBox','vcTo','',0,0,'V','vcTo',0,'EmailHistory',1,10,1,0,1,0,0,1)

 SELECT @numFormFieldID = SCOPE_IDENTITY()

 INSERT INTO DycFormField_Mapping
 (numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,
 bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
 VALUES
 (6,@numFieldID,44,0,0,'To','TextArea',10,1,0,1,1,0,0,1,1,@numFormFieldID)
 
end
COMMIT
END TRY
BEGIN CATCH
 IF @@TRANCOUNT > 0
  ROLLBACK TRANSACTION;

 SELECT 
  ERROR_MESSAGE(),
  ERROR_NUMBER(),
  ERROR_SEVERITY(),
  ERROR_STATE(),
  ERROR_LINE(),
  ISNULL(ERROR_PROCEDURE(), '-');
END CATCH