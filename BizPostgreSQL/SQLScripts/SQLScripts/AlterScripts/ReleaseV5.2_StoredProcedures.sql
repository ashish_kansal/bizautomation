/******************************************************************
Project: Release 5.2 Date: 09.Dec.2015
Comments: STORED PROCEDURES
*******************************************************************/

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='CheckAssemblyBuildStatus')
DROP FUNCTION CheckAssemblyBuildStatus
GO
CREATE FUNCTION [dbo].[CheckAssemblyBuildStatus] (@numItemKitID NUMERIC(18,0),@numQty NUMERIC(18,0))
RETURNS BIT
AS
BEGIN
    DECLARE @IsReadyToBuild BIT = 1

	DECLARE @Temp TABLE
	(
		numItemCode NUMERIC(18,0),
		numQtyRequired INT,
		numOnAllocation FLOAT,
		bitReadyToBuild BIT
	)

	INSERT INTO 
		@Temp
	SELECT
		numChildItemID,
		CAST(ISNULL(numQtyItemsReq * @numQty,0) AS INT),
		WareHouseItems.numAllocation,
		CASE WHEN  ISNULL(numQtyItemsReq * @numQty,0) > WareHouseItems.numAllocation THEN 0 ELSE 1 END AS bitReadyToBuild 
	FROM
		ItemDetails
	INNER JOIN
		WareHouseItems
	ON
		ItemDetails.numWareHouseItemId = WareHouseItems.numWareHouseItemID
	WHERE
		numItemKitID = @numItemKitID


	IF (SELECT COUNT(*) FROM @Temp WHERE  bitReadyToBuild = 0) > 0
	BEGIN
		SET @IsReadyToBuild = 0
	END

    RETURN @IsReadyToBuild
END
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='CheckOrderItemInventoryStatus')
DROP FUNCTION CheckOrderItemInventoryStatus
GO
CREATE FUNCTION [dbo].[CheckOrderItemInventoryStatus] 
(
	@numItemCode NUMERIC(18,0) = NULL,
	@numQuantity AS FLOAT = 0,
	@numOppItemCode AS NUMERIC(18,0),
	@numWarehouseItemID NUMERIC(18,0),
	@bitKitParent BIT 
)
RETURNS INT
AS
BEGIN

    DECLARE @bitBackOrder INT = 0
	DECLARE @numAllocation FLOAT
	DECLARE @numQtyShipped FLOAT = 0

	SELECT @numQtyShipped = ISNULL(numQtyShipped,0) FROM OpportunityItems WHERE numoppitemtCode = @numOppItemCode

	IF @bitKitParent = 1
	BEGIN
		DECLARE @TEMPTABLE TABLE
		(
			numItemCode NUMERIC(18,0),
			numParentItemID NUMERIC(18,0),
			numOppChildItemID NUMERIC(18,0),
			vcItemName VARCHAR(300),
			bitKitParent BIT,
			numWarehouseItmsID NUMERIC(18,0),
			numQtyItemsReq FLOAT,
			numQtyItemReq_Orig FLOAT,
			numAllocation FLOAT,
			numPrentItemBackOrder INT
		)

		INSERT INTO 
			@TEMPTABLE
		SELECT
			OKI.numChildItemID,
			0,
			OKI.numOppChildItemID,
			I.vcItemName,
			I.bitKitParent,
			OKI.numWareHouseItemId,
			OKI.numQtyItemsReq_Orig * (ISNULL(OI.numUnitHour,0) - ISNULL(OI.numQtyShipped,0)),
			OKI.numQtyItemsReq_Orig,
			ISNULL(WI.numAllocation,0),
			0
		FROM
			OpportunityKitItems OKI
		INNER JOIN
			WareHouseItems WI
		ON
			OKI.numWareHouseItemId = WI.numWareHouseItemID
		INNER JOIN
			OpportunityItems OI
		ON
			OKI.numOppItemID = OI.numoppitemtCode
		INNER JOIN
			Item I
		ON
			OKI.numChildItemID = I.numItemCode
		WHERE
			OI.numoppitemtCode = @numOppItemCode  

		INSERT INTO
			@TEMPTABLE
		SELECT
			OKCI.numItemID,
			t1.numItemCode,
			t1.numOppChildItemID,
			I.vcItemName,
			I.bitKitParent,
			OKCI.numWareHouseItemId,
			OKCI.numQtyItemsReq_Orig * t1.numQtyItemsReq,
			OKCI.numQtyItemsReq_Orig,
			ISNULL(WI.numAllocation,0),
			0
		FROM
			OpportunityKitChildItems OKCI
		INNER JOIN
			WareHouseItems WI
		ON
			OKCI.numWareHouseItemId = WI.numWareHouseItemID
		INNER JOIN
			@TEMPTABLE t1
		ON
			OKCI.numOppChildItemID = t1.numOppChildItemID
		INNER JOIN
			Item I
		ON
			OKCI.numItemID = I.numItemCode
		
		-- FIRST GET SUB KIT BACK ORDER NUMBER
		UPDATE 
			@TEMPTABLE 
		SET 
			numPrentItemBackOrder = (CASE 
								WHEN numAllocation > numQtyItemsReq
								THEN 0 
								ELSE
									(CASE WHEN numQtyItemReq_Orig > 0 THEN (numQtyItemsReq/numQtyItemReq_Orig) - CAST(numAllocation/numQtyItemReq_Orig AS INT) ELSE 0 END)
							END)
		WHERE
			bitKitParent = 0

		
		-- NOW USE SUB KIT BACK ORDER NUMBER TO GENERATE PARENT KIT BACK ORDER NUMBER
		UPDATE 
			t2
		SET 
			t2.numPrentItemBackOrder = (CASE WHEN t2.numQtyItemReq_Orig > 0 THEN CEILING((SELECT MAX(numPrentItemBackOrder) FROM @TEMPTABLE t1 WHERE t1.numParentItemID = t2.numItemCode) / t2.numQtyItemReq_Orig) ELSE 0 END)
		FROM
			@TEMPTABLE t2
		WHERE
			bitKitParent = 1

		SELECT @bitBackOrder = MAX(numPrentItemBackOrder) FROM @TEMPTABLE WHERE ISNULL(numParentItemID,0) = 0
	END
	ELSE
	BEGIN
		SELECT
			@numAllocation = numAllocation
		FROM
			WareHouseItems 
		WHERE
			numWareHouseItemID = @numWarehouseItemID

		IF (@numQuantity - @numQtyShipped) > @numAllocation
            SET @bitBackOrder = (@numQuantity - @numQtyShipped) - @numAllocation
		ELSE
			SET @bitBackOrder = 0
	END

	RETURN @bitBackOrder

END
GO


--created by anoop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='TF'AND NAME ='fn_calitemtaxamt')
DROP FUNCTION fn_calitemtaxamt
GO
CREATE FUNCTION [dbo].[fn_CalItemTaxAmt]
(
	@numDomainID as numeric(9),
	@numDivisionID as numeric(9),
	@numItemCode as numeric(9),
	@numTaxItemID as numeric(9),
	@numOppId as numeric(9),
	@bitAlwaysTaxApply AS BIT,
	@numReturnHeaderID AS NUMERIC(9)
) 
RETURNS @TEMP TABLE
(
	decTaxValue FLOAT, 
	tintTaxType TINYINT
)
AS
BEGIN

	SET @numOppId=NULLIF(@numOppId,0)
	SET @numReturnHeaderID=NULLIF(@numReturnHeaderID,0)

	declare @tintBaseTaxCalcOn as tinyint;
	declare @tintBaseTaxOnArea as tinyint;

	SELECT  @tintBaseTaxCalcOn = [tintBaseTax],@tintBaseTaxOnArea=tintBaseTaxOnArea FROM [Domain] WHERE [numDomainId] = @numDomainID

	declare @TaxPercentage as float
	DECLARE @tintTaxType AS TINYINT = 1 -- 1 = PERCENTAGE, 2 = FLAT AMOUNT
	declare @bitTaxApplicable as bit;set @bitTaxApplicable=0 ---Check Tax is applied for the Item

	if @numTaxItemID=0 and @numItemCode<>0
		select @bitTaxApplicable=bitTaxable from Item where numItemCode=@numItemCode
	else if @numItemCode<>0
		select @bitTaxApplicable=bitApplicable from ItemTax where numItemCode=@numItemCode and numTaxItemID=@numTaxItemID
	else if @numItemCode=0 
		set @bitTaxApplicable=1


	---Check Tax is applied for Company
	if @bitTaxApplicable=1 AND @bitAlwaysTaxApply=0
	begin
		set @bitTaxApplicable=0

		if @numTaxItemID=0 
			select @bitTaxApplicable=(case when bitNoTax=1 then 0 else 1 end) from DivisionMaster where numDivisionID=@numDivisionID
		else
			select @bitTaxApplicable=bitApplicable from DivisionTaxTypes where numDivisionID=@numDivisionID and numTaxItemID=@numTaxItemID
	end


	if @bitTaxApplicable=1
	begin

		declare @numCountry as numeric(9),@numState as numeric(9)
		declare @vcCity as varchar(100),@vcZipPostal as varchar(20)

		IF @numOppId>0
		BEGIN
		  DECLARE  @tintOppType  AS TINYINT,@tintBillType  AS TINYINT,@tintShipType  AS TINYINT
	 
			SELECT  @tintOppType = tintOppType,@tintBillType = tintBillToType,@tintShipType = tintShipToType
			  FROM   OpportunityMaster WHERE  numOppId = @numOppId
	 
			IF @tintBaseTaxCalcOn = 1 --Billing Address
			BEGIN
				IF @tintBillType IS NULL or (@tintBillType = 1 AND @tintOppType = 1) or (@tintBillType = 1 AND @tintOppType = 2)
					BEGIN
							Select @numState = ISNULL(AD.numState, 0),
							@numCountry = ISNULL(AD.numCountry, 0),
							@vcCity=isnull(AD.VcCity,''),@vcZipPostal=isnull(AD.vcPostalCode,'')
							from AddressDetails AD where AD.numDomainID=@numDomainID AND AD.numRecordID=@numDivisionID AND AD.tintAddressOf = 2 AND AD.tintAddressType = 1 AND AD.bitIsPrimary=1
					END
				ELSE IF @tintBillType = 0
					BEGIN
						Select @numState = ISNULL(AD1.numState, 0),
							@numCountry = ISNULL(AD1.numCountry, 0),
							@vcCity=isnull(AD1.VcCity,''),@vcZipPostal=isnull(AD1.vcPostalCode,'')
							 FROM   companyinfo [Com1] JOIN divisionmaster div1 ON com1.numCompanyID = div1.numCompanyID
								   JOIN Domain D1  ON D1.numDivisionID = div1.numDivisionID
								   JOIN dbo.AddressDetails AD1 ON AD1.numDomainID = div1.numDomainID AND AD1.numRecordID = div1.numDivisionID AND tintAddressOf = 2 AND tintAddressType = 1 AND bitIsPrimary=1
								   WHERE  D1.numDomainID = @numDomainID
					END
				 ELSE IF @tintBillType = 2 or @tintBillType = 3
					BEGIN
							Select @numState = ISNULL(numBillState, 0),
							@numCountry = ISNULL(numBillCountry, 0),
							@vcCity=isnull(VcBillCity,''),@vcZipPostal=isnull(vcBillPostCode,'')				
							FROM   OpportunityAddress WHERE  numOppID = @numOppId
					END
			
			END
			ELSE --Shipping Address(Default)
			BEGIN
				 IF @tintShipType IS NULL or (@tintShipType = 1 AND @tintOppType = 1) or (@tintShipType = 1 AND @tintOppType = 2) 
					BEGIN
							Select @numState = ISNULL(AD2.numState, 0),
							@numCountry = ISNULL(AD2.numCountry, 0),
							@vcCity=isnull(AD2.VcCity,''),@vcZipPostal=isnull(AD2.vcPostalCode,'')				
							from AddressDetails AD2 where AD2.numDomainID=@numDomainID AND AD2.numRecordID=@numDivisionID AND AD2.tintAddressOf = 2 AND AD2.tintAddressType = 2 AND AD2.bitIsPrimary=1
					END
	
				 ELSE IF @tintShipType = 0 
					BEGIN
							Select @numState = ISNULL(AD1.numState, 0),
							@numCountry = ISNULL(AD1.numCountry, 0),
							@vcCity=isnull(AD1.VcCity,''),@vcZipPostal=isnull(AD1.vcPostalCode,'')	
							FROM companyinfo [Com1] JOIN divisionmaster div1 ON com1.numCompanyID = div1.numCompanyID
								 JOIN Domain D1 ON D1.numDivisionID = div1.numDivisionID
								 JOIN dbo.AddressDetails AD1 ON AD1.numDomainID = div1.numDomainID AND AD1.numRecordID = div1.numDivisionID AND tintAddressOf = 2 AND tintAddressType = 2 AND bitIsPrimary=1
								 WHERE  D1.numDomainID = @numDomainID
					END
		
				 ELSE IF @tintShipType = 2 or @tintShipType = 3  
					BEGIN
						Select @numState = ISNULL(numShipState, 0),
							@numCountry = ISNULL(numShipCountry, 0),
							@vcCity=isnull(vcShipCity,''),@vcZipPostal=isnull(vcShipPostCode,'')
							FROM  OpportunityAddress WHERE  numOppID = @numOppId
					 END
			END
		END
		ELSE IF @numReturnHeaderID>0
		BEGIN
			IF @tintBaseTaxCalcOn = 1 --Billing Address
			BEGIN
							Select @numState = ISNULL(numBillState, 0),
							@numCountry = ISNULL(numBillCountry, 0),
							@vcCity=isnull(VcBillCity,''),@vcZipPostal=isnull(vcBillPostCode,'')				
							FROM   OpportunityAddress WHERE  numReturnHeaderID = @numReturnHeaderID
			
			END
			ELSE --Shipping Address(Default)
			BEGIN
		 
						Select @numState = ISNULL(numShipState, 0),
							@numCountry = ISNULL(numShipCountry, 0),
							@vcCity=isnull(vcShipCity,''),@vcZipPostal=isnull(vcShipPostCode,'')
							FROM  OpportunityAddress WHERE  numReturnHeaderID = @numReturnHeaderID
         
			END
		END
	ELSE
	BEGIN
	   IF @tintBaseTaxCalcOn = 2 --Shipping Address(Default)
			   SELECT   @numState = ISNULL(numState, 0),
						@numCountry = ISNULL(numCountry, 0),
						@vcCity=isnull(vcCity,''),@vcZipPostal=isnull(vcPostalCode,'')
			   FROM     dbo.AddressDetails
			   WHERE    numDomainID = @numDomainID
						AND numRecordID = @numDivisionID
						AND tintAddressOf = 2
						AND tintAddressType = 2
						AND bitIsPrimary=1
		ELSE --Billing Address
				SELECT   @numState = ISNULL(numState, 0),
						@numCountry = ISNULL(numCountry, 0),
						@vcCity=isnull(vcCity,''),@vcZipPostal=isnull(vcPostalCode,'')
			   FROM     dbo.AddressDetails
			   WHERE    numDomainID = @numDomainID
						AND numRecordID = @numDivisionID
						AND tintAddressOf = 2
						AND tintAddressType = 1
						AND bitIsPrimary=1
	END 



	If EXISTS (SELECT tintBaseTaxOnArea FROM TaxCountryConfi where numDomainID=@numDomainID and numCountry=@numCountry)
	BEGIN
		--TAKE FROM TAXCOUNTRYCONFI IF NOT THEN USE DEFAULT FROM DOMAIN
		SELECT @tintBaseTaxOnArea=tintBaseTaxOnArea FROM TaxCountryConfi WHERE numDomainID=@numDomainID AND numCountry=@numCountry 
	END
             
	SET @TaxPercentage=0 
             
	if @numCountry >0 and @numState>0
	begin
		if @numState>0        
		begin  
				if exists(select * from TaxDetails where numCountryID=@numCountry and numStateID=@numState and 
							(1=(Case 
									when @tintBaseTaxOnArea=0 then 1 --State
									when @tintBaseTaxOnArea=1 then Case When vcCity like '%' + @vcCity +'%' then 1 else 0 end --City
									when @tintBaseTaxOnArea=2 then Case When vcZipPostal=@vcZipPostal then 1 else 0 end --Zip/Postal
									else 0 end)) and numDomainID= @numDomainID and numTaxItemID=@numTaxItemID)            
						BEGIN
							select top 1 @TaxPercentage=decTaxPercentage, @tintTaxType=tintTaxType from TaxDetails where numCountryID=@numCountry and numStateID=@numState and 
								(1=(Case 
									when @tintBaseTaxOnArea=0 then 1 --State
									when @tintBaseTaxOnArea=1 then Case When vcCity like '%' + @vcCity +'%' then 1 else 0 end --City
									when @tintBaseTaxOnArea=2 then Case When vcZipPostal=@vcZipPostal then 1 else 0 end --Zip/Postal
									else 0 end)) and numDomainID= @numDomainID and numTaxItemID=@numTaxItemID           
						END
				else if exists(select * from TaxDetails where numCountryID=@numCountry and numStateID=@numState 
								and numDomainID= @numDomainID and numTaxItemID=@numTaxItemID and isnull(vcCity,'')='' and isnull(vcZipPostal,'')='')            
						BEGIN
							select top 1 @TaxPercentage=decTaxPercentage, @tintTaxType=tintTaxType from TaxDetails where numCountryID=@numCountry and numStateID=@numState
							 and numDomainID= @numDomainID and numTaxItemID=@numTaxItemID and isnull(vcCity,'')='' and isnull(vcZipPostal,'')=''      
						END
				else             
					select top 1 @TaxPercentage=decTaxPercentage, @tintTaxType=tintTaxType from TaxDetails where numCountryID=@numCountry and numStateID=0 and isnull(vcCity,'')='' and isnull(vcZipPostal,'')='' and numDomainID= @numDomainID   and numTaxItemID=@numTaxItemID                      
		end                  
	end
	else if @numCountry >0              
		select top 1 @TaxPercentage=decTaxPercentage, @tintTaxType=tintTaxType from TaxDetails where numCountryID=@numCountry and numStateID=0 and isnull(vcCity,'')='' and isnull(vcZipPostal,'')='' and numDomainID= @numDomainID  and numTaxItemID=@numTaxItemID
	end
	else
		set @TaxPercentage=0


	INSERT INTO @TEMP 
	(
		decTaxValue,
		tintTaxType
	)
	VALUES
	(
		ISNULL(@TaxPercentage,0), 
		ISNULL(@tintTaxType,1)
	) 

	RETURN
END

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_FindVendorCost')
DROP FUNCTION fn_FindVendorCost
GO
CREATE FUNCTION [dbo].[fn_FindVendorCost]
(
	@numItemCode NUMERIC,
	@numDivisionID as numeric(9)=0,                  
    @numDomainID as numeric(9)=0,            
	@units FLOAT
)
RETURNS money 
AS
BEGIN

 DECLARE @VendorCost money 
 
declare @numRelationship as numeric(9)                  
declare @numProfile as numeric(9)                  
declare @monListPrice MONEY
DECLARE @bitCalAmtBasedonDepItems bit

/*Profile and relationship id */
select @numRelationship=numCompanyType,@numProfile=vcProfile from DivisionMaster D                  
join CompanyInfo C on C.numCompanyId=D.numCompanyID                  
where numDivisionID =@numDivisionID       

IF EXISTS(SELECT ISNULL([monCost],0) ListPrice FROM [Vendor] V INNER JOIN dbo.Item I 
		ON V.numItemCode = I.numItemCode
		WHERE V.[numItemCode]=@numItemCode AND V.[numDomainID]=@numDomainID AND V.numVendorID=@numDivisionID)       
begin      
       SELECT @monListPrice=ISNULL([monCost],0) FROM [Vendor] V INNER JOIN dbo.Item I 
				ON V.numItemCode = I.numItemCode
				WHERE V.[numItemCode]=@numItemCode AND V.[numDomainID]=@numDomainID 
				AND V.numVendorID=@numDivisionID
end      
else      
begin      
	 SELECT @monListPrice=ISNULL([monCost],0) FROM [Vendor] WHERE [numItemCode]=@numItemCode AND [numDomainID]=@numDomainID
end      


SELECT TOP 1 @VendorCost=dbo.GetPriceBasedOnPriceBook(P.[numPricRuleID],@monListPrice ,@units,I.numItemCode)
		FROM    Item I 
        LEFT JOIN PriceBookRules P ON I.numDomainID = P.numDomainID
        LEFT JOIN PriceBookRuleDTL PDTL ON P.numPricRuleID = PDTL.numRuleID
        LEFT JOIN PriceBookRuleItems PBI ON P.numPricRuleID = PBI.numRuleID
        LEFT JOIN [PriceBookPriorities] PP ON PP.[Step2Value] = P.[tintStep2]
                                              AND PP.[Step3Value] = P.[tintStep3]
WHERE   numItemCode = @numItemCode AND tintRuleFor=2
AND 
(
((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 1
OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND (tintStep3= 1 AND PDTL.numValue = @numDivisionID)) -- Priority 2
OR ((tintStep2 = 3) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 3

OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 4
OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 5
OR ((tintStep2 = 3) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 6

OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 3)) -- Priority 7
OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND (tintStep3 = 3)) -- Priority 8
OR ((tintStep2 = 3) and (tintStep3 = 3)) -- Priority 9
)
ORDER BY PP.Priority ASC

if @VendorCost is null
	select @VendorCost=convert(money,@monListPrice)                       

if @VendorCost is null
		 select @VendorCost=(monTotAmount/(case when numUnitHour< 1 then 1 else numUnitHour end))                      
		 from OpportunityItems itm                                        
		 join OpportunityMaster mst                                        
		 on mst.numOppId=itm.numOppId                                        
		 where  tintOppType=2 and numItemCode=@numItemCode and mst.numDivisionID=@numDivisionID 
 	


 RETURN @VendorCost

END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetKitInventory]    Script Date: 08/22/2012  ******/

GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_GetKitInventory')
DROP FUNCTION fn_GetKitInventory
GO
CREATE FUNCTION [dbo].[fn_GetKitInventory]
(
	@numKitId NUMERIC
)          
RETURNS FLOAT          
AS          
BEGIN          
	DECLARE @OnHand AS FLOAT
	SET @OnHand = 0       
    
	;WITH CTE(numItemKitID,numItemCode,numWarehouseItmsID,numQtyItemsReq,numCalculatedQty) AS
	(
		SELECT 
			CONVERT(NUMERIC(18,0),0),
			numItemCode,
			ISNULL(Dtl.numWareHouseItemId,0) AS numWarehouseItmsID,
			DTL.numQtyItemsReq,
			CAST(DTL.numQtyItemsReq AS NUMERIC(9,0)) AS numCalculatedQty                                
		FROM 
			Item
		INNER JOIN 
			ItemDetails Dtl 
		ON 
			numChildItemID=numItemCode
		WHERE 
			numItemKitID=@numKitId 
		UNION ALL
		SELECT 
			Dtl.numItemKitID,
			i.numItemCode,
			ISNULL(Dtl.numWareHouseItemId,0) AS numWarehouseItmsID,
			DTL.numQtyItemsReq,
			CAST((DTL.numQtyItemsReq * c.numCalculatedQty) AS NUMERIC(9,0)) AS numCalculatedQty
		FROM 
			Item i                               
		INNER JOIN 
			ItemDetails Dtl 
		ON 
			Dtl.numChildItemID=i.numItemCode
		INNER JOIN 
			CTE c 
		ON 
			Dtl.numItemKitID = c.numItemCode 
		WHERE 
			Dtl.numChildItemID != @numKitId
	)

	SELECT 
		@OnHand = CAST(Floor(MIN(CASE WHEN ISNULL(numOnHand,0)=0 THEN 0 WHEN ISNULL(numOnHand,0)>=numQtyItemsReq AND numQtyItemsReq>0 THEN ISNULL(numOnHand,0)/numQtyItemsReq ELSE 0 end)) AS FLOAT) 
	FROM 
		cte c  
	LEFT OUTER JOIN 
		[WareHouseItems] WI 
	ON 
		WI.[numItemID] = c.numItemCode 
		AND WI.[numWareHouseItemID] = c.numWarehouseItmsID
	LEFT OUTER JOIN 
		Warehouses W 
	ON 
		W.numWareHouseID=WI.numWareHouseID
          
	RETURN @OnHand          
END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetKitInventoryByWarehouse]    Script Date: 05/30/2014  ******/
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'fn'
                    AND NAME = 'fn_GetKitInventoryByWarehouse' ) 
    DROP FUNCTION fn_GetKitInventoryByWarehouse
GO
CREATE FUNCTION [dbo].[fn_GetKitInventoryByWarehouse]
    (
      @numKitId NUMERIC,
      @numWarehouseID NUMERIC(18, 0)
    )
RETURNS FLOAT
AS BEGIN          
    DECLARE @OnHand AS FLOAT       
    SET @OnHand = 0 ;
    
	WITH CTE(numItemKitID, numItemCode, numWarehouseItmsID, numQtyItemsReq, numCalculatedQty) AS 
	( 
		SELECT   
			CONVERT(NUMERIC(18,0),0),
			numItemCode,
            ISNULL(Dtl.numWareHouseItemId, 0) AS numWarehouseItmsID,
            DTL.numQtyItemsReq,
            CAST(DTL.numQtyItemsReq AS NUMERIC(9, 0)) AS numCalculatedQty
        FROM
			Item
        INNER JOIN 
			ItemDetails Dtl 
		ON 
			numChildItemID = numItemCode
        WHERE 
			numItemKitID = @numKitId
        UNION ALL
        SELECT 
			Dtl.numItemKitID,
            i.numItemCode,
            ISNULL(Dtl.numWareHouseItemId, 0) AS numWarehouseItmsID,
            DTL.numQtyItemsReq,
            CAST(( DTL.numQtyItemsReq * c.numCalculatedQty ) AS NUMERIC(9, 0)) AS numCalculatedQty
        FROM 
			Item i
        INNER JOIN 
			ItemDetails Dtl 
		ON 
			Dtl.numChildItemID = i.numItemCode
        INNER JOIN 
			CTE c 
		ON 
			Dtl.numItemKitID = c.numItemCode
        WHERE    
			Dtl.numChildItemID != @numKitId
    )
    
	SELECT  
		@OnHand = CAST(FLOOR(MIN(CASE 
									WHEN ISNULL(numOnHand, 0) = 0 THEN 0
                                    WHEN ISNULL(numOnHand, 0) >= numQtyItemsReq AND numQtyItemsReq > 0 THEN ISNULL(numOnHand, 0) / numQtyItemsReq
                                    ELSE 0
                                    END)
						) AS FLOAT)
    FROM    
		cte c
    LEFT OUTER JOIN 
		[WareHouseItems] WI 
	ON 
		WI.[numItemID] = c.numItemCode
		AND WI.[numWareHouseItemID] = c.numWarehouseItmsID
    LEFT OUTER JOIN 
		Warehouses W 
	ON 
		W.numWareHouseID = WI.numWareHouseID
    WHERE 
		WI.numWareHouseID = @numWarehouseID
        
    RETURN @OnHand         
          
   END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_PopulateKitDesc]    Script Date: 07/26/2008 18:12:50 ******/

GO

GO
--created by anoop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_populateassemblydesc')
DROP FUNCTION fn_populateassemblydesc
GO
CREATE FUNCTION [dbo].[fn_PopulateAssemblyDesc](@numOppItemCode as numeric(9),@Units as FLOAT) returns varchar(300)

as
begin
	declare @Desc as varchar(300)
	set @Desc=''
	declare @numChildItem as numeric(9)
	declare @Quantity as integer
	set @numChildItem=0

	select top 1 @numChildItem=numChildItemID,@Quantity=numQtyItemsReq 
	from ItemDetails ID
    join Item I
    on ID.numChildItemID=I.numItemCode
    Join OpportunityItems OI
    on OI.numItemCode=ID.numItemKitID
    where numoppitemtCode=@numOppItemCode order by numChildItemID

	while @numChildItem>0
	begin

	select @Desc=@Desc+ vcItemName+'('+convert(varchar(30),@Quantity*@Units)+' '+ isnull(vcUnitofMeasure,'Units') +'), '  from Item where numItemCode=@numChildItem


	select top 1 @numChildItem=numChildItemID,@Quantity=numQtyItemsReq 
	from ItemDetails ID
    join Item I
    on ID.numChildItemID=I.numItemCode
    Join OpportunityItems OI
    on OI.numItemCode=ID.numItemKitID
    where numoppitemtCode=@numOppItemCode and numChildItemID>@numChildItem order by numChildItemID

	if @@rowcount=0 set @numChildItem=0
	end

	return @Desc
end
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'GetCommissionAmountOfBizDoc' ) 
    DROP PROCEDURE GetCommissionAmountOfBizDoc
GO
CREATE PROCEDURE GetCommissionAmountOfBizDoc
    (
      @numDomainId NUMERIC,
      @numOppBizDocID NUMERIC(9),
      @numOppID NUMERIC(9)
    )
AS 
    BEGIN

        CREATE TABLE #TempUserComission
            (
              numcontactid NUMERIC,
              numComission MONEY,
              numCOmissionID NUMERIC,
              vcContactName VARCHAR(250),
              numoppitemtCode NUMERIC,
              bitEmpContract BIT,
              vcItemName VARCHAR(300)
            ) ;
    

/*Do not include Shipping Item & Discount Item*/
        DECLARE @numShippingServiceItemID AS NUMERIC(18) ;
            SET @numShippingServiceItemID = 0
        DECLARE @numDiscountServiceItemID AS NUMERIC(18) ;
            SET @numDiscountServiceItemID = 0
        DECLARE @tintCommissionType AS TINYINT,
            @tintComAppliesTo TINYINT 
        DECLARE @bitIncludeTaxAndShippingInCommission AS BIT


--Get CommissionType (total amounts paid : 1 ,gross profit : 2 ,Project Gross Profit : 3)
--        SELECT  @numShippingServiceItemID = CASE WHEN ISNULL(bitIncludeTaxAndShippingInCommission,
--                                                             0) = 1
--                                                 THEN ISNULL(numShippingServiceItemID,
--                                                             0)
--                                                 WHEN ISNULL(bitIncludeTaxAndShippingInCommission,
--                                                             0) = 0 THEN 0
--                                                 ELSE ISNULL(numShippingServiceItemID,
--                                                             0)
--                                            END, -- Applying Domain Settings for Including Shipping Cost for Commission Calculations
				SELECT @numShippingServiceItemID =ISNULL(numShippingServiceItemID, 0), 
                @numDiscountServiceItemID = ISNULL(numDiscountServiceItemID, 0),
                @tintCommissionType = ISNULL(tintCommissionType, 1),
                @tintComAppliesTo = ISNULL(tintComAppliesTo, 3),
                @bitIncludeTaxAndShippingInCommission = ISNULL(bitIncludeTaxAndShippingInCommission, 1)
        FROM    dbo.Domain
        WHERE   numDomainId = @numDomainId

--If Commission set for Project Gross Profit then don't calculate for Order
        IF @tintCommissionType = 3 
            BEGIN
                SELECT  *
                FROM    #TempUserComission ;
                DROP TABLE #TempUserComission
	
                RETURN	
            END


--bug id 982: No need to check for biz docs AmountPaid and DealAmount must equal 
        IF EXISTS ( SELECT  *
                    FROM    opportunitymaster OM
                            LEFT JOIN opportunitybizdocs BD ON BD.numoppid = OM.numoppid
                    WHERE   tintopptype = 1
                            AND tintoppstatus = 1
                            AND numdomainid = @numDomainId
                            AND bitAuthoritativeBizDocs = 1 /*AND isnull(BD.monAmountPaid,0) >=BD.monDealAmount*/--bug id 982
                            AND BD.numOppBizDocsId = @numOppBizDocID /*AND isnull(BD.monAmountPaid,0)>0*/
                            AND BD.monDealAmount > 0 ) 
            BEGIN
                DECLARE @numassignedto NUMERIC,
                    @numrecowner NUMERIC,
                    @numItemCode NUMERIC,
                    @numUnitHour FLOAT,
                    @monTotAmount AS MONEY,
                    @numTotalUnitHour FLOAT,
                    @monTotalTotAmount AS MONEY,
                    @monCommission AS MONEY
                DECLARE @numComRuleID NUMERIC,
                    @tintComBasedOn TINYINT,
                    @tinComDuration TINYINT,
                    @tintComType TINYINT
                DECLARE @dFrom DATETIME,
                    @dTo DATETIME
                DECLARE @decCommission DECIMAL(18, 0),
                    @numDivisionID NUMERIC,
                    @bitCommContact AS BIT,
                    @numItemClassification NUMERIC
                DECLARE @numUserCntID NUMERIC,
                    @VendorCostAssignee MONEY,
                    @TotalVendorCostAssignee MONEY,
                    @numOppBizDocItemID NUMERIC,
                    @numoppitemtCode NUMERIC
                DECLARE @UserName AS VARCHAR(100),
                    @bitEmpContract AS BIT
                DECLARE @TempmonTotAmount AS MONEY,
                    @TempVendorCostAssignee MONEY
                DECLARE @numCOmissionID AS NUMERIC ;
                    SET @numCOmissionID = 0
                DECLARE @vcItemName AS VARCHAR(300)

			--Get AssignTo and Record Owner of Oppertunity
                SELECT  @numassignedto = numassignedto,
                        @numrecowner = numrecowner
                FROM    opportunitymaster OM
                        LEFT JOIN opportunitybizdocs BD ON BD.numoppid = OM.numoppid
                WHERE   tintopptype = 1
                        AND tintoppstatus = 1
                        AND numdomainid = @numDomainId
                        AND bitAuthoritativeBizDocs = 1
                        AND BD.numOppBizDocsId = @numOppBizDocID
			

			--Cursor for each Items belogs to Oppertunity Biz Docs
                DECLARE Biz_Items CURSOR
                    FOR SELECT  BDI.numItemCode,
                                SUM(ISNULL(BDI.numUnitHour, 0)),
                                SUM(ISNULL(BDI.monTotAmount, 0)),
                                SUM(ISNULL(OT.monVendorCost, 0)
                                    * ISNULL(BDI.[numUnitHour], 0)),
                                MAX(BDI.numOppBizDocItemID),
                                MAX(OT.numoppitemtCode)
                        FROM    opportunitymaster OM
                                JOIN opportunitybizdocs BD ON BD.numoppid = OM.numoppid
                                JOIN OpportunityBizDocItems BDI ON BD.numOppBizDocsId = BDI.numOppBizDocID
                                INNER JOIN OpportunityItems OT ON OT.numOppItemtCode = BDI.numOppItemID
                        WHERE   tintopptype = 1
                                AND tintoppstatus = 1
                                AND numdomainid = @numDomainId
                                AND bitAuthoritativeBizDocs = 1 /*AND isnull(BD.monAmountPaid,0) >=BD.monDealAmount*/ --bug id 982
                                AND BD.numOppBizDocsId = @numOppBizDocID /*AND isnull(BD.monAmountPaid,0)>0 */
                                AND BD.monDealAmount > 0
                                AND BDI.numItemCode NOT IN (
                                @numDiscountServiceItemID )  /*Do not include Discount Item*/
                                AND 1 = CASE WHEN BDI.numItemCode <> @numShippingServiceItemID
                                             THEN 1
                                             ELSE 0
                                        END /*Condition to include/exclude include Shipping Item */
                        GROUP BY BDI.numItemCode
                        UNION ALL
                        SELECT  BDI.numItemCode,
                                SUM(ISNULL(BDI.numUnitHour, 0)),
                                SUM(ISNULL(BDI.monTotAmount, 0)),
                                SUM(ISNULL(OT.monVendorCost, 0)
                                    * ISNULL(BDI.[numUnitHour], 0)),
                                MAX(BDI.numOppBizDocItemID),
                                MAX(OT.numoppitemtCode)
                        FROM    opportunitymaster OM
                                JOIN opportunitybizdocs BD ON BD.numoppid = OM.numoppid
                                JOIN OpportunityBizDocItems BDI ON BD.numOppBizDocsId = BDI.numOppBizDocID
                                INNER JOIN OpportunityItems OT ON OT.numOppItemtCode = BDI.numOppItemID
                        WHERE   tintopptype = 1
                                AND tintoppstatus = 1
                                AND numdomainid = @numDomainId
                                AND bitAuthoritativeBizDocs = 1 /*AND isnull(BD.monAmountPaid,0) >=BD.monDealAmount*/ --bug id 982
                                AND BD.numOppBizDocsId = @numOppBizDocID /*AND isnull(BD.monAmountPaid,0)>0 */
                                AND BD.monDealAmount > 0
                                AND BDI.numItemCode NOT IN (
                                @numDiscountServiceItemID )  /*Do not include Discount Item*/
                               AND 1 = CASE WHEN @bitIncludeTaxAndShippingInCommission = 1
                                             THEN ( CASE WHEN BDI.numItemCode = @numShippingServiceItemID
                                                         THEN 1
                                                         ELSE 0
                                                    END )
                                             ELSE 0
                                        END /*Condition to include/exclude include Shipping Item */
                                        
                        GROUP BY BDI.numItemCode
							
                OPEN Biz_Items

                FETCH NEXT FROM Biz_Items INTO @numItemCode, @numUnitHour,
                    @monTotAmount, @VendorCostAssignee, @numOppBizDocItemID,
                    @numoppitemtCode
                WHILE @@FETCH_STATUS = 0
                    BEGIN  
				
                        DECLARE @i AS INT ;
                            SET @i = 1
					
					--Loop for AssignTo i=1 and RecordOwner i=2 (set tintAssignTo=i)
                        WHILE @i < 3
                            BEGIN
					
                                IF @i = 1 
                                    SET @numUserCntID = @numassignedto
                                ELSE 
                                    IF @i = 2 
                                        SET @numUserCntID = @numrecowner
						
					--Check For Commission Contact	
                                IF EXISTS ( SELECT  UM.numUserId
                                            FROM    UserMaster UM
                                                    JOIN AdditionalContactsInformation A ON UM.numUserDetailId = A.numContactID
                                            WHERE   UM.numDomainID = @numDomainID
                                                    AND UM.numDomainID = A.numDomainID
                                                    AND A.numContactID = @numUserCntID ) 
                                    BEGIN
                                        SET @bitCommContact = 0
                                        SET @numDivisionID = 0
                                    END	
                                ELSE 
                                    BEGIN
                                        SET @bitCommContact = 1
                                        SELECT  @numDivisionID = A.numDivisionId
                                        FROM    AdditionalContactsInformation A
                                        WHERE   A.numContactId = @numUserCntID 
                                    END  		
						
                                SELECT  @TempmonTotAmount = @monTotAmount,
                                        @TempVendorCostAssignee = @VendorCostAssignee
						
						--SELECT @numUserCntID,@i,@numItemCode
					--Individual Items tintComAppliesTo=1 and tintType=1
                                IF EXISTS ( SELECT  CR.numComRuleID
                                            FROM    CommissionRules CR
                                                    JOIN CommissionRuleItems CRI ON CR.numComRuleID = CRI.numComRuleID
                                                    JOIN CommissionRuleContacts CRC ON CR.numComRuleId = CRC.numComRuleId
                                            WHERE   CR.numDomainID = @numDomainID
                                                    AND CR.tintComAppliesTo = 1
                                                    AND CR.tintAssignTo = @i
                                                    AND CRI.numValue = @numItemCode
                                                    AND CRI.tintType = 1
                                                    AND CRC.numValue = ( CASE @bitCommContact
                                                                           WHEN 0 THEN @numUserCntID
                                                                           WHEN 1 THEN @numDivisionID
                                                                         END )
                                                    AND CRC.bitCommContact = @bitCommContact )
                                    AND @tintComAppliesTo = 1 
                                    BEGIN
						
                                        SELECT TOP 1
                                                @numComRuleID = CR.numComRuleID,
                                                @tintComBasedOn = CR.tintComBasedOn,
                                                @tinComDuration = CR.tinComDuration,
                                                @tintComType = CR.tintComType
                                        FROM    CommissionRules CR
                                                JOIN CommissionRuleItems CRI ON CR.numComRuleID = CRI.numComRuleID
                                                JOIN CommissionRuleContacts CRC ON CR.numComRuleId = CRC.numComRuleId
                                        WHERE   CR.numDomainID = @numDomainID
                                                AND CR.tintComAppliesTo = 1
                                                AND CR.tintAssignTo = @i
                                                AND CRI.numValue = @numItemCode
                                                AND CRI.tintType = 1
                                                AND CRC.numValue = ( CASE @bitCommContact
                                                                       WHEN 0 THEN @numUserCntID
                                                                       WHEN 1 THEN @numDivisionID
                                                                     END )
                                                AND CRC.bitCommContact = @bitCommContact
                                    END
						
					--Items Classification	tintComAppliesTo=2 and tintType=2
                                ELSE 
                                    IF EXISTS ( SELECT  CR.numComRuleID
                                                FROM    CommissionRules CR
                                                        JOIN CommissionRuleItems CRI ON CR.numComRuleID = CRI.numComRuleID
                                                        JOIN CommissionRuleContacts CRC ON CR.numComRuleId = CRC.numComRuleId
                                                        JOIN Item I ON CRI.numValue = I.numItemClassification
                                                WHERE   CR.numDomainID = @numDomainID
                                                        AND CR.tintComAppliesTo = 2
                                                        AND CR.tintAssignTo = @i
                                                        AND I.numItemCode = @numItemCode
                                                        AND CRI.tintType = 2
                                                        AND CRC.numValue = ( CASE @bitCommContact
                                                                               WHEN 0 THEN @numUserCntID
                                                                               WHEN 1 THEN @numDivisionID
                                                                             END )
                                                        AND CRC.bitCommContact = @bitCommContact )
                                        AND @tintComAppliesTo = 2 
                                        BEGIN
						
                                            SELECT TOP 1
                                                    @numComRuleID = CR.numComRuleID,
                                                    @tintComBasedOn = CR.tintComBasedOn,
                                                    @tinComDuration = CR.tinComDuration,
                                                    @tintComType = CR.tintComType,
                                                    @numItemClassification = CRI.numValue
                                            FROM    CommissionRules CR
                                                    JOIN CommissionRuleItems CRI ON CR.numComRuleID = CRI.numComRuleID
                                                    JOIN CommissionRuleContacts CRC ON CR.numComRuleId = CRC.numComRuleId
                                                    JOIN Item I ON CRI.numValue = I.numItemClassification
                                            WHERE   CR.numDomainID = @numDomainID
                                                    AND CR.tintComAppliesTo = 2
                                                    AND CR.tintAssignTo = @i
                                                    AND I.numItemCode = @numItemCode
                                                    AND CRI.tintType = 2
                                                    AND CRC.numValue = ( CASE @bitCommContact
                                                                           WHEN 0 THEN @numUserCntID
                                                                           WHEN 1 THEN @numDivisionID
                                                                         END )
                                                    AND CRC.bitCommContact = @bitCommContact
                                        END
						
					--All Items	 tintComAppliesTo=3
                                    ELSE 
                                        IF EXISTS ( SELECT  CR.numComRuleID
                                                    FROM    CommissionRules CR
                                                            JOIN CommissionRuleContacts CRC ON CR.numComRuleId = CRC.numComRuleId
                                                    WHERE   CR.tintComAppliesTo = 3
                                                            AND CR.numDomainID = @numDomainID
                                                            AND CR.tintAssignTo = @i
                                                            AND CRC.numValue = ( CASE @bitCommContact
                                                                                   WHEN 0 THEN @numUserCntID
                                                                                   WHEN 1 THEN @numDivisionID
                                                                                 END )
                                                            AND CRC.bitCommContact = @bitCommContact )
                                            AND @tintComAppliesTo = 3 
                                            BEGIN
						
                                                SELECT TOP 1
                                                        @numComRuleID = CR.numComRuleID,
                                                        @tintComBasedOn = CR.tintComBasedOn,
                                                        @tinComDuration = CR.tinComDuration,
                                                        @tintComType = CR.tintComType
                                                FROM    CommissionRules CR
                                                        JOIN CommissionRuleContacts CRC ON CR.numComRuleId = CRC.numComRuleId
                                                WHERE   CR.tintComAppliesTo = 3
                                                        AND CR.numDomainID = @numDomainID
                                                        AND CR.tintAssignTo = @i
                                                        AND CRC.numValue = ( CASE @bitCommContact
                                                                               WHEN 0 THEN @numUserCntID
                                                                               WHEN 1 THEN @numDivisionID
                                                                             END )
                                                        AND CRC.bitCommContact = @bitCommContact
                                            END
					
                                IF @numComRuleID > 0 
                                    BEGIN
						--Get From and To date for Commission Calculation
                                        IF @tinComDuration = 1 --Month
                                            SELECT  @dFrom = DATEADD(m, DATEDIFF(m, 0, GETDATE()), 0),
                                                    @dTo = DATEADD(d, -1, DATEADD(m, DATEDIFF(m, 0, GETDATE()) + 1, 0)) 
	
                                        ELSE 
                                            IF @tinComDuration = 2 --Quarter
                                                SELECT  @dFrom = DATEADD(q, DATEDIFF(q, 0, GETDATE()), 0),
                                                        @dTo = DATEADD(d, -1, DATEADD(q, DATEDIFF(q, 0, GETDATE()) + 1, 0)) 
									
                                            ELSE 
                                                IF @tinComDuration = 3 --Year
                                                    SELECT  @dFrom = DATEADD(yy, DATEDIFF(yy, 0, GETDATE()), 0),
                                                            @dTo = DATEADD(d, -1, DATEADD(yy, DATEDIFF(yy, 0, GETDATE()) + 1, 0)) 
						
						
						--Total of Units and Amount for all oppertunity belogs to selected Item and selected AssignTo/RecordOwner
                                        SELECT  @numTotalUnitHour = ISNULL(SUM(BDI.numUnitHour), 0),
                                                @monTotalTotAmount = ISNULL(SUM(BDI.monTotAmount), 0),
                                                @TotalVendorCostAssignee = ISNULL(SUM(ISNULL(OT.monVendorCost, 0) * ISNULL(BDI.[numUnitHour], 0)), 0)
                                        FROM    opportunitymaster OM
                                                JOIN opportunitybizdocs BD ON BD.numoppid = OM.numoppid
                                                JOIN OpportunityBizDocItems BDI ON BD.numOppBizDocsId = BDI.numOppBizDocID
                                                INNER JOIN OpportunityItems OT ON OT.numOppItemtCode = BDI.numOppItemID
                                        WHERE   tintopptype = 1
                                                AND tintoppstatus = 1
                                                AND numdomainid = @numDomainId
                                                AND bitAuthoritativeBizDocs = 1 /*AND isnull(BD.monAmountPaid,0) >=BD.monDealAmount*/ --bug id 982
                                                AND BD.numOppBizDocsId <> @numOppBizDocID /*AND isnull(BD.monAmountPaid,0)>0*/
                                                AND BD.monDealAmount > 0 
							--AND (numassignedto=@numUserCntID OR numrecowner=@numUserCntID) 
                                                AND 1 = ( CASE @i
                                                            WHEN 1
                                                            THEN CASE @bitCommContact
                                                                   WHEN 0 THEN CASE WHEN numassignedto = @numUserCntID THEN 1
                                                                                    ELSE 0
                                                                               END
                                                                   WHEN 1 THEN CASE WHEN numassignedto IN ( SELECT  numContactId
                                                                                                            FROM    AdditionalContactsInformation AC
                                                                                                            WHERE   AC.numDivisionID = @numDivisionID ) THEN 1
                                                                                    ELSE 0
                                                                               END
                                                                 END
                                                            WHEN 2
                                                            THEN CASE @bitCommContact
                                                                   WHEN 0 THEN CASE WHEN numrecowner = @numUserCntID THEN 1
                                                                                    ELSE 0
                                                                               END
                                                                   WHEN 1 THEN CASE WHEN numrecowner IN ( SELECT    numContactId
                                                                                                          FROM      AdditionalContactsInformation AC
                                                                                                          WHERE     AC.numDivisionID = @numDivisionID ) THEN 1
                                                                                    ELSE 0
                                                                               END
                                                                 END
                                                          END )	 				 	 
							--BDI.numItemCode=@numItemCode
                                                AND 1 = ( CASE @tintComAppliesTo
                                                            WHEN 1
                                                            THEN CASE WHEN BDI.numItemCode = @numItemCode THEN 1
                                                                      ELSE 0
                                                                 END
                                                            WHEN 2
                                                            THEN CASE WHEN BDI.numItemCode IN ( SELECT  numItemCode
                                                                                                FROM    Item
                                                                                                WHERE   numItemClassification = @numItemClassification
                                                                                                        AND numDomainID = @numDomainID ) THEN 1
                                                                      ELSE 0
                                                                 END
                                                            WHEN 3 THEN 1
                                                            ELSE 0
                                                          END )
                                                AND BD.numOppBizDocsId IN (
                                                SELECT  OBD.numOppBizDocsId
                                                FROM    OpportunityBizDocsDetails BDD
                                                        INNER JOIN dbo.OpportunityBizDocs OBD ON BDD.numBizDocsId = OBD.numOppBizDocsId
                                                        INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OBD.numOppId
                                                WHERE   tintopptype = 1
                                                        AND tintoppstatus = 1
                                                        AND OM.numdomainid = @numDomainId
                                                        AND OBD.bitAuthoritativeBizDocs = 1
                                                        AND BDD.dtCreationDate BETWEEN @dFrom AND @dTo
                                                        AND 1 = ( CASE @i
                                                                    WHEN 1 THEN CASE @bitCommContact
                                                                                  WHEN 0 THEN CASE WHEN OM.numassignedto = @numUserCntID THEN 1
                                                                                                   ELSE 0
                                                                                              END
                                                                                  WHEN 1 THEN CASE WHEN OM.numassignedto IN ( SELECT    numContactId
                                                                                                                              FROM      AdditionalContactsInformation AC
                                                                                                                              WHERE     AC.numDivisionID = @numDivisionID ) THEN 1
                                                                                                   ELSE 0
                                                                                              END
                                                                                END
                                                                    WHEN 2 THEN CASE @bitCommContact
                                                                                  WHEN 0 THEN CASE WHEN OM.numrecowner = @numUserCntID THEN 1
                                                                                                   ELSE 0
                                                                                              END
                                                                                  WHEN 1 THEN CASE WHEN OM.numrecowner IN ( SELECT  numContactId
                                                                                                                            FROM    AdditionalContactsInformation AC
                                                                                                                            WHERE   AC.numDivisionID = @numDivisionID ) THEN 1
                                                                                                   ELSE 0
                                                                                              END
                                                                                END
                                                                  END )	 				 	 				 
														--(OM.numassignedto=@numUserCntID OR OM.numrecowner=@numUserCntID)
                                                GROUP BY OBD.numOppBizDocsId /*HAVING SUM(BDD.monAmount)>=MAX(OBD.monDealAmount)*/ ) --bug id 982
			
			
						--If Commission based on gross profit	
                                        IF @tintCommissionType = 2 
                                            BEGIN
                                                SET @TempmonTotAmount = @TempmonTotAmount
                                                    - @TempVendorCostAssignee
                                                SET @monTotalTotAmount = @monTotalTotAmount
                                                    - @TotalVendorCostAssignee
                                                    + @TempmonTotAmount
                                            END
                                        ELSE 
                                            SET @monTotalTotAmount = @monTotalTotAmount
                                                + @TempmonTotAmount
						
						
                                        SET @numTotalUnitHour = @numTotalUnitHour
                                            + @numUnitHour
						
                                        DECLARE @intFromNextTier AS INT
						
                                        IF @tintComBasedOn = 1 --Amount Sold
                                            BEGIN
                                                SELECT TOP 1
                                                        @decCommission = decCommission,
                                                        @intFromNextTier = intTo
                                                        + 1
                                                FROM    CommissionRuleDtl
                                                WHERE   numComRuleID = @numComRuleID
                                                        AND @monTotalTotAmount BETWEEN intFrom AND intTo
                                            END			
                                        ELSE 
                                            IF @tintComBasedOn = 2 --Units Sold
                                                BEGIN
                                                    SELECT TOP 1
                                                            @decCommission = decCommission,
                                                            @intFromNextTier = intTo
                                                            + 1
                                                    FROM    CommissionRuleDtl
                                                    WHERE   numComRuleID = @numComRuleID
                                                            AND @numTotalUnitHour BETWEEN intFrom AND intTo			
                                                END		
				
						
                                        DECLARE @nexttier AS VARCHAR(100)

                                        SELECT TOP 1
                                                @nexttier = CAST(numComRuleDtlID AS VARCHAR(10))
                                                + ','
                                                + CAST(numComRuleID AS VARCHAR(10))
                                                + ','
                                                + CAST(intFrom AS VARCHAR(10))
                                                + ','
                                                + CAST(intTo AS VARCHAR(10))
                                                + ','
                                                + CAST(decCommission AS VARCHAR(10))
                                        FROM    CommissionRuleDtl
                                        WHERE   numComRuleID = @numComRuleID
                                                AND intFrom >= @intFromNextTier
                                        ORDER BY intFrom 
							
							
                                        IF @tintComType = 1 --Percentage
                                            SET @monCommission = @TempmonTotAmount
                                                * @decCommission / 100
                                        ELSE 
                                            IF @tintComType = 2 --Flat
                                                SET @monCommission = @decCommission
                                                    * @numUnitHour
						
                                        IF @monCommission > 0 
                                            BEGIN
							
                                                SELECT  @UserName = ISNULL(adc.vcfirstname + ' ' + adc.vclastname, '-'),
                                                        @bitEmpContract = CASE WHEN um.numuserdetailid > 0 THEN 0
                                                                               ELSE 1
                                                                          END
                                                FROM    additionalcontactsinformation adc
                                                        LEFT JOIN usermaster um ON adc.numcontactid = um.numuserdetailid
                                                                                   AND um.numdomainid = @numDomainId
                                                                                   AND um.bitactivateflag = 1
                                                WHERE   adc.numcontactid = @numUserCntID
							
                                                SELECT  @vcItemName = vcItemName
                                                FROM    Item
                                                WHERE   numItemCode = @numItemCode
                                                        AND numdomainid = @numDomainId 

                                                IF NOT EXISTS ( SELECT  *
                                                                FROM    [BizDocComission]
                                                                WHERE   numDomainId = @numDomainId
                                                                        AND numUserCntID = @numUserCntID
                                                                        AND numOppBizDocId = @numOppBizDocID
                                                                        AND numOppBizDocItemID = @numOppBizDocItemID ) 
                                                    BEGIN
                                                        INSERT  INTO [BizDocComission]
                                                                (
                                                                  [numDomainId],
                                                                  [numUserCntID],
                                                                  [numOppBizDocId],
                                                                  [numComissionAmount],
                                                                  [numOppBizDocItemID],
                                                                  [numComRuleID],
                                                                  [tintComType],
                                                                  [tintComBasedOn],
                                                                  [decCommission],
                                                                  vcnexttier,
                                                                  numOppID
                                                                )
                                                        VALUES  (
                                                                  @numDomainId,
                                                                  @numUserCntID,
                                                                  @numOppBizDocID,
                                                                  @monCommission,
                                                                  @numOppBizDocItemID,
                                                                  @numComRuleID,
                                                                  @tintComType,
                                                                  @tintComBasedOn,
                                                                  @decCommission,
                                                                  @nexttier,
                                                                  @numOppID
                                                                ) ;	

                                                        SET @numCOmissionID = @@IDENTITY
                                                    END
                                                ELSE 
                                                    SELECT  @numCOmissionID = numCOmissionID
                                                    FROM    [BizDocComission]
                                                    WHERE   numDomainId = @numDomainId
                                                            AND numUserCntID = @numUserCntID
                                                            AND numOppBizDocId = @numOppBizDocID
                                                            AND numOppBizDocItemID = @numOppBizDocItemID

                                                INSERT  INTO #TempUserComission
                                                        SELECT  @numUserCntID,
                                                                @monCommission,
                                                                @numCOmissionID,
                                                                @UserName,
                                                                @numoppitemtCode,
                                                                @bitEmpContract,
                                                                @vcItemName ;

                                            END
                                        ELSE 
                                            BEGIN
                                                DELETE  FROM [BizDocComission]
                                                WHERE   numDomainId = @numDomainId
                                                        AND numUserCntID = @numUserCntID
                                                        AND numOppBizDocId = @numOppBizDocID
                                                        AND numOppBizDocItemID = @numOppBizDocItemID
                                            END
                                    END
                                SELECT  @numComRuleID = 0,
                                        @tintComBasedOn = 0,
                                        @tinComDuration = 0,
                                        @tintComType = 0,
                                        @monCommission = 0,
                                        @bitCommContact = 0,
                                        @numDivisionID = 0
                                SET @i = @i + 1
                            END
					
                        FETCH NEXT FROM Biz_Items INTO @numItemCode,
                            @numUnitHour, @monTotAmount, @VendorCostAssignee,
                            @numOppBizDocItemID, @numoppitemtCode
                    END ;

                CLOSE Biz_Items ;
                DEALLOCATE Biz_Items ;
            END

        SELECT  *
        FROM    #TempUserComission ;
        DROP TABLE #TempUserComission


--    DECLARE @numUserId NUMERIC
--    DECLARE @numUserCntID NUMERIC
--    DECLARE @vcContactName varchar(250)
--    DECLARE @AssignedAmount AS MONEY
--    DECLARE @OwnerAmount AS MONEY
--    DECLARE @bitCommOwner AS BIT
--    DECLARE @bitCommAssignee AS BIT
--    DECLARE @decCommOwner AS DECIMAL(10, 2)
--    DECLARE @decCommAssignee AS DECIMAL(10, 2)
--    DECLARE @CommAssignedAmt AS MONEY
--    DECLARE @CommOwnerAmt AS MONEY
--    DECLARE @TotalCommAmt AS MONEY
--    DECLARE @bitRoleComm AS BIT
--    DECLARE @CommRoleAmt AS MONEY
--    DECLARE @bitManagerOfOwner AS BIT
--    DECLARE @decManagerOfOwner AS DECIMAL(10, 2)
--    DECLARE @CommissionType TINYINT
--    DECLARE @VendorCostOwner MONEY
--    DECLARE @VendorCostAssignee MONEY
--    DECLARE @VendorCostManager MONEY
--    DECLARE @VendorCostRole MONEY
--	DECLARE @ManagerOfOwnerAmt  AS MONEY
--    DECLARE @CommManagerOfOwnerAmt AS MONEY
--    
--    
--
--    
--    CREATE TABLE #TempUserData
--    (numUserId numeric,
--    bitCommOwner numeric,
--    decCommOwner numeric,
--    bitCommAssignee numeric,
--    decCommAssignee numeric,
--    bitManagerOfOwner numeric,
--    decManagerOfOwner numeric,
--    bitRoleComm numeric)
--    
--    SELECT  @CommissionType = ISNULL(tintCommissionType, 1)
--    FROM    domain
--    WHERE   numDomainID = @numDomainID
--    
--    
--    
--    
--    declare Biz_Users CURSOR FOR
--       
--    SELECT um.numuserid,adc.numcontactid,Isnull(adc.vcfirstname
--                    + ' '
--                    + adc.vclastname,'-')  as UserName
--    FROM   usermaster um
--           JOIN additionalcontactsinformation adc
--             ON adc.numcontactid = um.numuserdetailid             
--           LEFT OUTER JOIN listdetails ld
--             ON adc.vcdepartment = ld.numlistitemid
--                AND ld.numlistid = 19
--    WHERE  um.bitactivateflag = 1
--           AND um.numdomainid = @numDomainId
--		   AND adc.numcontactid not in (select numUserCntId from BizDocComission BC
--		    where BC.numOppBizDocId = @numOppBizDocID)
--
--	OPEN Biz_Users
--
--    FETCH NEXT FROM Biz_Users into @numUserId,@numUserCntID,@vcContactName
--WHILE @@FETCH_STATUS = 0
--   BEGIN
--		set @bitCommOwner=0;
--		set @bitCommAssignee=0;
--		set @decCommOwner=0;
--		set @decCommAssignee=0;
--		set @bitRoleComm=0;
--		set @bitManagerOfOwner=0;
--		set @decManagerOfOwner=0;
--		
--		SELECT  @bitCommOwner = bitcommowner,
--            @bitCommAssignee = bitcommassignee,
--            @decCommOwner = ISNULL(fltcommowner, 0),
--            @decCommAssignee = ISNULL(fltcommassignee, 0),
--            @bitRoleComm = bitrolecomm,
--            @bitManagerOfOwner = bitManagerOfOwner,
--            @decManagerOfOwner = fltManagerOfOwner
--    FROM    usermaster
--    WHERE   numuserid = @numUserId
--    
--		insert into #TempUserData
--     SELECT @numUserId,@bitCommOwner,@decCommOwner,@bitCommAssignee,@decCommAssignee,@bitManagerOfOwner,@decManagerOfOwner,@bitRoleComm
--     
-----------------When user is Assignee of Deal ----------------------------
--	IF @bitCommAssignee = 1 AND @decCommAssignee <> 0
--	BEGIN
--		SELECT  @AssignedAmount = SUM(ISNULL(BD.monAmountPaid, 0)) --monpamount
--		FROM    opportunitymaster OM
--				LEFT JOIN opportunitybizdocs BD ON BD.numoppid = OM.numoppid
--		WHERE   tintopptype = 1
--				AND tintoppstatus = 1
--				AND numassignedto = @numUserCntID
--				AND numdomainid = @numDomainId
--				AND bitAuthoritativeBizDocs = 1 --
--				AND isnull(BD.monAmountPaid,0) >=BD.monDealAmount
--				AND BD.numOppBizDocsId=@numOppBizDocID
--		IF @CommissionType = 2 
--			BEGIN
--				SELECT  @VendorCostAssignee = SUM(ISNULL(OT.monVendorCost, 0)* ISNULL(BI.[numUnitHour],0))
--				FROM    opportunitymaster OM
--						LEFT JOIN opportunitybizdocs BD ON BD.numoppid = OM.numoppid
--						LEFT JOIN [OpportunityBizDocItems] BI ON BI.[numOppBizDocID] = BD.[numOppBizDocsId]
--						INNER JOIN OpportunityItems OT ON OT.numOppItemtCode=BI.numOppItemID 						
--				WHERE   tintopptype = 1
--						AND tintoppstatus = 1
--						AND numassignedto = @numUserCntID
--						AND OM.numdomainid = @numDomainId
--						AND bitAuthoritativeBizDocs = 1 --
--						AND isnull(BD.monAmountPaid,0) >0
--						AND  BD.numOppBizDocsId=@numOppBizDocID
--	           
--				SET @AssignedAmount = @AssignedAmount - @VendorCostAssignee ;
--				--RETURN @VendorCostAssignee
--			END            
--	END
--	
--   
-----------------When user is Owner of Deal ----------------------------
--	IF @bitCommOwner = 1 AND @decCommOwner <> 0
--	BEGIN
--		SELECT  @OwnerAmount = SUM(ISNULL(BD.monAmountPaid, 0)) --monpamount
--		FROM    opportunitymaster OM
--				LEFT JOIN opportunitybizdocs BD ON BD.numoppid = OM.numoppid
--		WHERE   tintopptype = 1
--				AND tintoppstatus = 1
--				AND numrecowner = @numUserCntID
--				AND numdomainid = @numDomainId
--				AND bitAuthoritativeBizDocs = 1 --
--				AND isnull(BD.monAmountPaid,0) >=BD.monDealAmount
--				AND BD.numOppBizDocsId=@numOppBizDocID
--	      
--		IF @CommissionType = 2 
--			BEGIN
--				SELECT  @VendorCostOwner = SUM(ISNULL(OT.monVendorCost, 0)* ISNULL(BI.[numUnitHour],0))
--				FROM    opportunitymaster OM
--						LEFT JOIN opportunitybizdocs BD ON BD.numoppid = OM.numoppid
--						LEFT JOIN [OpportunityBizDocItems] BI ON BI.[numOppBizDocID] = BD.[numOppBizDocsId]
--						INNER JOIN OpportunityItems OT ON OT.numOppItemtCode=BI.numOppItemID 						
--						
--				WHERE   tintopptype = 1
--						AND tintoppstatus = 1
--						AND numrecowner = @numUserCntID
--						AND OM.numdomainid = @numDomainId
--						AND bitAuthoritativeBizDocs = 1
--						AND BD.monAmountPaid > 0
--						AND BD.numOppBizDocsId=@numOppBizDocID
--	           
--				SET @OwnerAmount = @OwnerAmount - @VendorCostOwner ;
--	--            RETURN @VendorCostOwner
--			END     	
--	END
--    
-- ---------------When user is Manager of Deal Owner----------------------------
--	IF @bitManagerOfOwner = 1 AND @decManagerOfOwner <> 0
--	BEGIN
--		
--		
--		SELECT  @ManagerOfOwnerAmt = SUM(ISNULL(BD.monAmountPaid, 0)) --monpamount
--		FROM    opportunitymaster OM
--				LEFT JOIN opportunitybizdocs BD ON BD.numoppid = OM.numoppid
--		WHERE   tintopptype = 1
--				AND tintoppstatus = 1
--				AND numrecowner IN (SELECT [numContactId] FROM [AdditionalContactsInformation] WHERE [numManagerID]=@numUserCntID AND [numDomainID]=@numDomainID)
--				AND numdomainid = @numDomainId
--				AND bitAuthoritativeBizDocs = 1 --
--				AND isnull(BD.monAmountPaid,0) >=BD.monDealAmount
--				AND BD.numOppBizDocsId=@numOppBizDocID
--	       
--		IF @CommissionType = 2 
--			BEGIN
--				SELECT  @VendorCostManager = SUM(ISNULL(OT.monVendorCost, 0)* ISNULL(BI.[numUnitHour],0))
--				FROM    opportunitymaster OM
--						LEFT JOIN opportunitybizdocs BD ON BD.numoppid = OM.numoppid
--						LEFT JOIN [OpportunityBizDocItems] BI ON BI.[numOppBizDocID] = BD.[numOppBizDocsId]
--						INNER JOIN OpportunityItems OT ON OT.numOppItemtCode=BI.numOppItemID 						
--				WHERE   tintopptype = 1
--						AND tintoppstatus = 1
--						AND numrecowner IN (SELECT [numContactId] FROM [AdditionalContactsInformation] WHERE [numManagerID]=@numUserCntID AND [numDomainID]=@numDomainID)
--						AND OM.numdomainid = @numDomainId
--						AND bitAuthoritativeBizDocs = 1
--						AND BD.monAmountPaid > 0
--						AND BD.numOppBizDocsId=@numOppBizDocID
--	           
--				SET @ManagerOfOwnerAmt = @ManagerOfOwnerAmt - @VendorCostManager ;
----			 RETURN @ManagerOfOwnerAmt    
--			END     	
--	END
--                               
-----------------When user beloings to selceted Role ----------------------------
--    IF @bitRoleComm = 1 
--        BEGIN
--            SELECT  @CommRoleAmt = SUM(ISNULL(monamountpaid, 0)
--                                       * fltpercentage / 100)
--            FROM    opportunitymaster opp
--                    JOIN opportunitycontact oppcont ON opp.numoppid = oppcont.numoppid
--                    LEFT JOIN opportunitybizdocs oppbiz ON oppbiz.numoppid = opp.numoppid
--                    JOIN userroles ur ON oppcont.numrole = ur.numrole
--                    JOIN usermaster um ON ur.numusercntid = um.numuserid
--                                          AND oppcont.numcontactid = um.numuserdetailid
----               INNER JOIN opportunitybizdocsdetails obd
----                 ON obd.numbizdocsid = oppbiz.numoppbizdocsid
--            WHERE   tintopptype = 1
--                    AND opp.tintoppstatus = 1
--                    AND opp.numdomainid = @numDomainId
--                    AND oppcont.numcontactid = @numUserCntID
--                    AND bitAuthoritativeBizDocs = 1 --
--                    AND  isnull(oppbiz.monAmountPaid,0) >=oppbiz.monDealAmount
--                    AND oppbiz.numOppBizDocsId=@numOppBizDocID
----       RETURN @CommRoleAmt
--            IF @CommissionType = 2 
--                BEGIN
--                    SELECT  @VendorCostRole = SUM(ISNULL(OT.monVendorCost, 0)* ISNULL(BI.[numUnitHour],0) * fltpercentage / 100 )
--                    FROM    opportunitymaster opp
--                            JOIN opportunitycontact oppcont ON opp.numoppid = oppcont.numoppid
--                            LEFT JOIN opportunitybizdocs oppbiz ON oppbiz.numoppid = opp.numoppid
--                            JOIN userroles ur ON oppcont.numrole = ur.numrole
--                            JOIN usermaster um ON ur.numusercntid = um.numuserid
--                                                  AND oppcont.numcontactid = um.numuserdetailid
--                            LEFT JOIN [OpportunityBizDocItems] BI ON BI.[numOppBizDocID] = oppbiz.[numOppBizDocsId]
--							INNER JOIN OpportunityItems OT ON OT.numOppItemtCode=BI.numOppItemID 						
--                    WHERE   tintopptype = 1
--                            AND opp.tintoppstatus = 1
--                            AND opp.numdomainid = @numDomainId
--                            AND oppcont.numcontactid = @numUserCntID
--                            AND bitAuthoritativeBizDocs = 1 --
--                            AND oppbiz.monAmountPaid > 0
--                            AND oppbiz.numOppBizDocsId=@numOppBizDocID
--                                                    
--					SET @CommRoleAmt =@CommRoleAmt -  @VendorCostRole;
--                END 
--        END
-- ---------------------------------------------------------------------------------
--		
--		
--		IF @bitManagerOfOwner = 1
--        AND @decManagerOfOwner <> 0 
--        SET @CommManagerOfOwnerAmt = @ManagerOfOwnerAmt * @decManagerOfOwner / 100
--    IF @bitCommOwner = 1
--        AND @decCommOwner <> 0 
--        SET @CommOwnerAmt = @OwnerAmount * @decCommOwner / 100
--    IF @bitCommAssignee = 1
--        AND @decCommAssignee <> 0 
--        SET @CommAssignedAmt = @AssignedAmount * @decCommAssignee / 100
--
--
--    SET @TotalCommAmt = ISNULL(@CommManagerOfOwnerAmt, 0)
--        + ISNULL(@CommOwnerAmt, 0) + ISNULL(@CommAssignedAmt, 0)
--        + ISNULL(@CommRoleAmt, 0)
--        
--        if @TotalCommAmt>0
--			begin
--				
--				insert into BizDocComission select @numDomainId,@numUserCntID,@numOppBizDocID,@TotalCommAmt;
--				insert into #TempUserComission select @numUserCntID,@TotalCommAmt,@@IDENTITY,@vcContactName;
--			end
--        
--      
--      
--      set @TotalCommAmt=0;
--      set @CommManagerOfOwnerAmt=0;
--      set @CommOwnerAmt=0;
--      set @CommAssignedAmt=0;
--      set @CommRoleAmt=0;
--      
--      
--      FETCH NEXT FROM Biz_Users into @numUserId,@numUserCntID,@vcContactName
--   END;
--
--CLOSE Biz_Users;
--DEALLOCATE Biz_Users;
--
--    
--SELECT * FROM #TempUserComission;
----select * from #TempUserData
--
--DROP TABLE #TempUserComission;
--drop table #TempUserData;
    
    END

GO


/****** Object:  UserDefinedFunction [dbo].[GetCustFldValue]    Script Date: 07/26/2008 18:12:59 ******/

GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='getcustfldvalue')
DROP FUNCTION getcustfldvalue
GO
CREATE FUNCTION [dbo].[GetCustFldValue](@numFldId numeric(9),@pageId as tinyint,@numRecordId as numeric(9))    
 RETURNS varchar (1000)     
AS    
BEGIN    

DECLARE @Fld_type AS VARCHAR(100)
DECLARE @vcURL AS VARCHAR(MAX)
SELECT @Fld_type = ISNULL(Fld_type,''),@vcURL = ISNULL(vcURL,'') FROM CFW_Fld_Master WHERE Fld_id = @numFldId
    
declare @vcValue as  varchar(1000)    
if @pageId=1    
begin    
 select @vcValue=Fld_Value from CFW_FLD_Values where Fld_ID=@numFldId and RecId=@numRecordId    
    
end    
    
if @pageId=4    
begin    
 select @vcValue=Fld_Value from CFW_FLD_Values_Cont where Fld_ID=@numFldId and RecId=@numRecordId    
    
end    
    
if @pageId=3    
begin    
 select @vcValue=Fld_Value from CFW_FLD_Values_Case where Fld_ID=@numFldId and RecId=@numRecordId    
    
end    
    
    
if @pageId=5    
begin    
 select @vcValue=Fld_Value from CFW_FLD_Values_Item where Fld_ID=@numFldId and RecId=@numRecordId    
    
end    
    
if @pageId=2 or @pageId=6    
begin    
 select @vcValue=Fld_Value from CFW_Fld_Values_Opp where Fld_ID=@numFldId and RecId=@numRecordId    
    
end    
    
if @pageId=7 or @pageId=8    
begin    
 select @vcValue=Fld_Value from CFW_Fld_Values_Product where Fld_ID=@numFldId and RecId=@numRecordId    
    
end   
  
if @pageId=9  
begin  
 select @vcValue=Fld_Value from CFW_Fld_Values_Serialized_Items where Fld_ID=@numFldId and RecId=@numRecordId    
    
end  
if @pageId=11  
begin  
 select @vcValue=Fld_Value from CFW_Fld_Values_Pro where Fld_ID=@numFldId and RecId=@numRecordId    
    
end  
    
if @vcValue is null set @vcValue= (CASE WHEN @Fld_type = 'Link' THEN @vcURL ELSE '0' END)
    
RETURN @vcValue    
    
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='TF'AND NAME ='GetDFHistoricalSalesOppData')
DROP FUNCTION GetDFHistoricalSalesOppData
GO
CREATE FUNCTION [dbo].[GetDFHistoricalSalesOppData] 
 (
	  @numDFID AS NUMERIC(18,0),
      @numDomainID AS NUMERIC(9),
	  @dtFromDate AS DATETIME,
	  @dtToDate AS DATETIME
    )
RETURNS  @TEMP TABLE 
(
	numItemCode NUMERIC(18,0), 
	numWarehouseItmsID NUMERIC(18,0),
	numQtySold INTEGER
)
AS BEGIN 
		DECLARE @TEMPOPP TABLE
		(
			numOppID NUMERIC(18,0),
			numPercentageComplete NUMERIC(18,0),
			numItemCode NUMERIC(18,0),
			numWarehouseItmsID NUMERIC(18,0),
			numUnitHour FLOAT,
			charItemType CHAR(1),
			bitKitParent BIT,
			bitAssembly BIT
		)

	
		DECLARE @ITEMTABLE TABLE
		(
			numItemCode NUMERIC(18,0), 
			numWarehouseItmsID NUMERIC(18,0),
			numQtySold FLOAT
		)

		--GET Sales Opportunity(tintOppType = 1 AND tintOppStatus = 0) Created Between Dates
		INSERT INTO	
			@TEMPOPP
		SELECT
			OpportunityMaster.numOppId,
			(ISNULL((SELECT CAST(ISNULL(vcData,0) AS DECIMAL) FROM ListDetails WHERE numListItemID = OpportunityMaster.numPercentageComplete),0)/100),
			Item.numItemCode,
			OpportunityItems.numWarehouseItmsID,
			OpportunityItems.numUnitHour,
			Item.charItemType,
			Item.bitKitParent,
			Item.bitAssembly
		FROM
			OpportunityMaster
		LEFT JOIN
			OpportunityItems
		ON
			OpportunityMaster.numOppId = OpportunityItems.numOppId
		INNER JOIN
			Item
		ON
			OpportunityItems.numItemCode = Item.numItemCode
		LEFT JOIN
			WareHouseItems
		ON
			OpportunityItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
		WHERE
			OpportunityMaster.numDomainId = @numDomainID AND
			Item.charItemType = 'P' AND
			CONVERT(DATE,OpportunityMaster.bintCreatedDate) BETWEEN CONVERT(DATE,@dtFromDate) AND CONVERT(DATE,@dtToDate) AND
			OpportunityMaster.tintOppType = 1 AND 
			OpportunityMaster.tintOppStatus = 0 AND
			ISNULL(OpportunityMaster.numPercentageComplete,0) > 0 AND
			(
				(SELECT COUNT(*) FROM DemandForecastWarehouse WHERE numDFID = @numDFID) = 0 OR
				WareHouseItems.numWareHouseID IN (SELECT numWareHouseID FROM DemandForecastWarehouse WHERE numDFID = @numDFID)
			)
				
		--Get total quantity of each inventory or assembly item sold in Sales Opportunity(tintOppType = 1 AND tintOppStatus = 0)
		INSERT INTO 
			@ITEMTABLE
		SELECT
			TEMP.numItemCode,
			TEMP.numWarehouseItmsID,
			SUM(TEMP.QuantitySold) AS numQtySold
		FROM
			(SELECT
				t1.numOppId,
				t1.numItemCode,
				t1.numWarehouseItmsID, 
				CEILING(SUM(t1.numUnitHour) * ISNULL(t1.numPercentageComplete,0)) AS QuantitySold
			FROM
				@TEMPOPP AS t1
			WHERE
				(t1.bitKitParent = 0 OR (t1.bitKitParent = 1 AND t1.bitAssembly = 1)) 
			GROUP BY
				t1.numOppId,
				t1.numItemCode,
				t1.numWarehouseItmsID,
				t1.numPercentageComplete
			) AS TEMP
		GROUP BY
			TEMP.numItemCode,
			TEMP.numWarehouseItmsID


		--Get total quantity of each child inventory item based on quantity of kit item sold in Sales Opportunity(tintOppType = 1 AND tintOppStatus = 0)
		INSERT INTO 
			@ITEMTABLE
		SELECT
			TEMP.numItemCode,
			TEMP.numWarehouseItmsID,
			SUM(TEMP.QuantitySold) AS numQtySold
		FROM
			(SELECT
				t1.numOppId,
				t1.numPercentageComplete,
				KitChildItemsTable.numItemCode,
				KitChildItemsTable.numWarehouseItmsID, 
				CEILING((SUM(t1.numUnitHour) * ISNULL(t1.numPercentageComplete,0)) * ISNULL(SUM(KitChildItemsTable.numQtyItemsReq),0)) AS QuantitySold
			FROM
				@TEMPOPP AS t1
			CROSS APPLY
			(
				SELECT  
						numItemCode,
						ISNULL(Dtl.numWareHouseItemId, 0) AS numWarehouseItmsID,
						DTL.numQtyItemsReq
				FROM    
					Item
				INNER JOIN 
					ItemDetails Dtl 
				ON 
					numChildItemID = numItemCode
				INNER JOIN 
					WareHouseItems 
				ON 
					WareHouseItems.numWareHouseItemID = Dtl.numWareHouseItemId
				WHERE    
					numItemKitID = t1.numItemCode AND
					Item.charItemType = 'P'
			) AS KitChildItemsTable
			WHERE
				(t1.bitKitParent = 1 AND t1.bitAssembly = 0) 
			GROUP BY
				t1.numOppId,
				t1.numPercentageComplete,
				KitChildItemsTable.numItemCode,
				KitChildItemsTable.numWarehouseItmsID
			) AS TEMP
		GROUP BY
			TEMP.numItemCode,
			TEMP.numWarehouseItmsID

		INSERT INTO 
			@TEMP
		SELECT
			numItemCode, 
			numWarehouseItmsID,
			SUM(numQtySold)
		FROM	
			@ITEMTABLE
		GROUP BY
			numItemCode, 
			numWarehouseItmsID

		RETURN
END
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='TF'AND NAME ='GetDFHistoricalSalesOrderData')
DROP FUNCTION GetDFHistoricalSalesOrderData
GO
CREATE FUNCTION [dbo].[GetDFHistoricalSalesOrderData] 
 (
	  @numDFID AS NUMERIC(18,0),
      @numDomainID AS NUMERIC(9),
	  @dtFromDate AS DATETIME,
	  @dtToDate AS DATETIME
    )
RETURNS  @TEMP TABLE 
(
	numItemCode NUMERIC(18,0), 
	numWarehouseItmsID NUMERIC(18,0),
	numQtySold INTEGER
)
AS BEGIN 
	
		DECLARE @TEMPORDER TABLE
		(
			numOppID NUMERIC(18,0),
			numItemCode NUMERIC(18,0),
			numWarehouseItmsID NUMERIC(18,0),
			numUnitHour FLOAT,
			charItemType CHAR(1),
			bitKitParent BIT,
			bitAssembly BIT
		)

		DECLARE @ITEMTABLE TABLE
		(
			numItemCode NUMERIC(18,0), 
			numWarehouseItmsID NUMERIC(18,0),
			numQtySold FLOAT
		)


		INSERT INTO	
			@TEMPORDER
		SELECT
			OpportunityMaster.numOppId,			
			Item.numItemCode,
			OpportunityItems.numWarehouseItmsID,
			OpportunityItems.numUnitHour,
			Item.charItemType,
			Item.bitKitParent,
			Item.bitAssembly
		FROM
			OpportunityMaster
		LEFT JOIN
			OpportunityItems
		ON
			OpportunityMaster.numOppId = OpportunityItems.numOppId
		INNER JOIN
			Item
		ON
			OpportunityItems.numItemCode = Item.numItemCode
		LEFT JOIN
			WareHouseItems
		ON
			OpportunityItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
		WHERE
			OpportunityMaster.numDomainId = @numDomainID AND
			Item.charItemType = 'P' AND
			CONVERT(DATE,OpportunityMaster.bintCreatedDate) BETWEEN CONVERT(DATE,@dtFromDate) AND CONVERT(DATE,@dtToDate) AND
			OpportunityMaster.tintOppType = 1 AND 
			OpportunityMaster.tintOppStatus = 1 AND
			(
				(SELECT COUNT(*) FROM DemandForecastWarehouse WHERE numDFID = @numDFID) = 0 OR
				WareHouseItems.numWareHouseID IN (SELECT numWareHouseID FROM DemandForecastWarehouse WHERE numDFID = @numDFID)
			)

		-- Get total quantity of inventoty or assembly item in Sales Order(tintOppType = 1 AND tintOppStatus = 1)
		INSERT INTO
			@ITEMTABLE
		SELECT
			t1.numItemCode, 
			t1.numWarehouseItmsID,
			SUM(t1.numUnitHour) AS numQtySold
		FROM
			@TEMPORDER AS t1
		WHERE
			(t1.bitKitParent = 0 OR (t1.bitKitParent = 1 AND t1.bitAssembly = 1))
		GROUP BY
			t1.numItemCode,
			t1.numWarehouseItmsID


		-- Get sum of total child inventory items quantity based on kit quantity sold in Sales Order(tintOppType = 1 AND tintOppStatus = 1)
		INSERT INTO
			@ITEMTABLE
		SELECT
			KitChildItemsTable.numItemCode, 
			KitChildItemsTable.numWarehouseItmsID,
			SUM(ISNULL(t1.numUnitHour,0) * ISNULL(KitChildItemsTable.numQtyItemsReq,0)) AS numQtySold
		FROM
			@TEMPORDER AS t1
		CROSS APPLY
			(
				SELECT  
						numItemCode,
						ISNULL(Dtl.numWareHouseItemId, 0) AS numWarehouseItmsID,
						DTL.numQtyItemsReq
				FROM    
					Item
				INNER JOIN 
					ItemDetails Dtl 
				ON 
					numChildItemID = numItemCode
				INNER JOIN 
					WareHouseItems 
				ON 
					WareHouseItems.numWareHouseItemID = Dtl.numWareHouseItemId
				WHERE    
					numItemKitID = t1.numItemCode AND
					Item.charItemType = 'P'
			) AS KitChildItemsTable
		WHERE
			(t1.bitKitParent = 1 AND t1.bitAssembly = 0)
		GROUP BY
			KitChildItemsTable.numItemCode, 
			KitChildItemsTable.numWarehouseItmsID


		INSERT INTO 
			@TEMP
		SELECT
			numItemCode, 
			numWarehouseItmsID,
			SUM(numQtySold)
		FROM	
			@ITEMTABLE
		GROUP BY
			numItemCode, 
			numWarehouseItmsID

		RETURN
END

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='TF'AND NAME ='GetItemPurchaseVendorPrice')
DROP FUNCTION GetItemPurchaseVendorPrice
GO
CREATE FUNCTION [dbo].[GetItemPurchaseVendorPrice] 
(
      @numDivisionID AS NUMERIC(18,0),
	  @numItemCode AS NUMERIC(18,0),
	  @numDomainID AS NUMERIC(18,0), 
	  @numUnits AS FLOAT
)
RETURNS @TEMP TABLE (numDivisionID NUMERIC(9),intMinQty INT, monPrice MONEY, intLeadTimeDays INT)
AS BEGIN
	DECLARE @numMinimumQty INT = 0
	DECLARE @intLeadTimeDays INT = 0
	DECLARE @monListPrice MONEY = 0
	DECLARE @numRelationship INT = 0
	DECLARE @numProfile AS NUMERIC(9)


	IF ISNULL(@numDivisionID,0) = 0
	BEGIN
		-- CHECK IF PRIMARY VENDOR EXISTS
		SELECT 
			@numDivisionID=V.numVendorID, 
			@numMinimumQty=V.intMinQty,
			@intLeadTimeDays = ISNULL(V.intLeadTimeDays,0)
		FROM 
			Item I 
		INNER JOIN 
			[Vendor] V 
		ON 
			V.numVendorID=I.numVendorID  
		WHERE 
			V.numDomainID = @numDomainID AND
			I.numItemCode = @numItemCode 

		-- IF PRIMARY VENDOR IS NOT AVAILABLE THEN SELECT ANY VENDOR AVAILABLE
		IF ISNULL(@numDivisionID,0) = 0
		BEGIN
			SELECT TOP 1
				@numDivisionID=V.numVendorID,
				@numMinimumQty=V.intMinQty,
				@intLeadTimeDays = ISNULL(V.intLeadTimeDays,0)
			FROM 
				[Vendor] V 
			WHERE
				V.[numDomainID]=@numDomainID AND V.[numItemCode]=@numItemCode
		END
	END

	-- IF ANY VENDOR IS AVAILABLE THEN GO FOR GETTING PRICE
	IF ISNULL(@numDivisionID,0) > 0
	BEGIN
		SELECT 
			@numRelationship=numCompanyType,
			@numProfile=vcProfile 
		FROM 
			DivisionMaster D                  
		JOIN 
			CompanyInfo C 
		ON 
			C.numCompanyId=D.numCompanyID                  
		WHERE 
			numDivisionID =@numDivisionID   

		SELECT 
			@monListPrice=ISNULL([monCost],0),
			@numMinimumQty=V.intMinQty,
			@intLeadTimeDays = ISNULL(V.intLeadTimeDays,0)
		FROM 
			[Vendor] V 
		INNER JOIN 
			dbo.Item I 
		ON 
			V.numItemCode = I.numItemCode
		WHERE 
			V.[numItemCode]=@numItemCode AND 
			V.[numDomainID]=@numDomainID AND 
			V.numVendorID=@numDivisionID
	
		--CHECK IF PRICE RULE IS APPLICABLE
		IF (SELECT
			COUNT(*)
		FROM    
			Item I 
		LEFT JOIN PriceBookRules P ON I.numDomainID = P.numDomainID
		LEFT JOIN PriceBookRuleDTL PDTL ON P.numPricRuleID = PDTL.numRuleID
		LEFT JOIN PriceBookRuleItems PBI ON P.numPricRuleID = PBI.numRuleID
		LEFT JOIN [PriceBookPriorities] PP ON PP.[Step2Value] = P.[tintStep2] AND PP.[Step3Value] = P.[tintStep3]
		WHERE   
			numItemCode = @numItemCode AND 
			tintRuleFor=2 AND 
			(
				((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 1
				OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND (tintStep3= 1 AND PDTL.numValue = @numDivisionID)) -- Priority 2
				OR ((tintStep2 = 3) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 3

				OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 4
				OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 5
				OR ((tintStep2 = 3) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 6

				OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 3)) -- Priority 7
				OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND (tintStep3 = 3)) -- Priority 8
				OR ((tintStep2 = 3) and (tintStep3 = 3)) -- Priority 9
			)) > 0
		BEGIN
			SELECT
				@monListPrice = (CASE 
							WHEN dbo.GetPriceBasedOnPriceBook(P.[numPricRuleID],@monListPrice,@numUnits,I.numItemCode) > 0 
							THEN dbo.GetPriceBasedOnPriceBook(P.[numPricRuleID],@monListPrice,@numUnits,I.numItemCode) 
							ELSE @monListPrice
							END)
			FROM    
				Item I 
			LEFT JOIN PriceBookRules P ON I.numDomainID = P.numDomainID
			LEFT JOIN PriceBookRuleDTL PDTL ON P.numPricRuleID = PDTL.numRuleID
			LEFT JOIN PriceBookRuleItems PBI ON P.numPricRuleID = PBI.numRuleID
			LEFT JOIN [PriceBookPriorities] PP ON PP.[Step2Value] = P.[tintStep2] AND PP.[Step3Value] = P.[tintStep3]
			WHERE   
				numItemCode = @numItemCode AND 
				tintRuleFor=2 AND 
				(
					((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 1
					OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND (tintStep3= 1 AND PDTL.numValue = @numDivisionID)) -- Priority 2
					OR ((tintStep2 = 3) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 3

					OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 4
					OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 5
					OR ((tintStep2 = 3) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 6

					OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 3)) -- Priority 7
					OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND (tintStep3 = 3)) -- Priority 8
					OR ((tintStep2 = 3) and (tintStep3 = 3)) -- Priority 9
				)
			ORDER BY PP.Priority ASC
		END
		
	END


	INSERT INTO @TEMP (numDivisionID, intMinQty, monPrice, intLeadTimeDays) VALUES (@numDivisionID, @numMinimumQty, @monListPrice, @intLeadTimeDays)

	RETURN
END

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='TF'AND NAME ='GetItemQuantityToOrder')
DROP FUNCTION GetItemQuantityToOrder
GO
CREATE FUNCTION [dbo].[GetItemQuantityToOrder] 
 (
      @numItemCode AS NUMERIC(9),
	  @numWarehouseItemID AS NUMERIC(9),
      @numDomainID AS NUMERIC(9),
	  @numAnalysisDays AS FLOAT,
	  @numForecastDays AS FLOAT,
	  @numTotalQtySold AS FLOAT,
	  @numVendorID AS NUMERIC(18,0)
    )
RETURNS @TEMP TABLE 
(
	numItemCode NUMERIC(9), 
	numOnHand FLOAT,  
	numOnOrder FLOAT, 
	numAllocation FLOAT,
	numBackOrder FLOAT, 
	numReOrder FLOAT,
	numQtyToOrder INT, 
	dtOrderDate DATETIME, 
	vcOrderDate VARCHAR(50),
	numDivisionID NUMERIC(18,0), 
	monListPrice MONEY,
	intMinQty INT,
	intLeadTimeDays INT
)
AS BEGIN
	DECLARE @numQtySoldPerDay AS FLOAT = 0
	DECLARE @numRequiredForecastQuantity AS FLOAT = 0
	DECLARE @numQtyToOrder AS FLOAT = 0
	DECLARE @numOnHand AS FLOAT = 0
	DECLARE @numOnOrder AS FLOAT = 0
	DECLARE @numAllocation AS FLOAT = 0
	DECLARE @numBackOrder AS FLOAT = 0
	DECLARE @numReOrder AS FLOAT = 0
	DECLARE @dtOrderDate DATETIME
	DECLARE @numDivisionID NUMERIC(18,0) = 0
	DECLARE @monListPrice MONEY = 0
	DECLARE @numMinimumQty INT = 0
	DECLARE @intLeadTimeDays INT = 0

	-- Get Back Order Quantity of Item
	SELECT 
		@numOnHand = ISNULL(numOnHand,0),
		@numOnOrder = ISNULL(numOnOrder,0),
		@numAllocation = ISNULL(numAllocation,0),
		@numBackOrder = ISNULL(numBackOrder,0),
		@numReOrder = ISNULL(numReorder,0)
	FROM
		WareHouseItems
	WHERE
		numItemID = @numItemCode AND
		numWarehouseItemID = @numWarehouseItemID

	--If total quantity sold for selected period is greater then 0 then process further
	IF @numTotalQtySold > 0
	BEGIN
		SET @numQtySoldPerDay = @numTotalQtySold/@numAnalysisDays

		--If average quantity sold per day is greater then 0 then process further
		IF @numQtySoldPerDay > 0
		BEGIN
			SET @numRequiredForecastQuantity = @numQtySoldPerDay * ISNULL(@numForecastDays,0)

			-- If item already have some back order quantity then we have to add it to required quantity to purchase
			SET @numRequiredForecastQuantity = @numRequiredForecastQuantity + ISNULL(@numBackOrder,0)

			SET @numQtyToOrder = ISNULL(CEILING(@numRequiredForecastQuantity),0)


			--If item has enought quantity for required days then no need to 
			-- then there is no need to make order
			IF (@numOnHand + @numOnOrder) > @numQtyToOrder
			BEGIN
				SET @numQtyToOrder = 0
			END
			ELSE
			BEGIN
				-- Else get required quantity ot order after removing items on hand
				SET @numQtyToOrder = @numQtyToOrder - (@numOnHand + @numOnOrder)
			END
		END
	END

	IF ISNULL(@numQtyToOrder,0) > 0
	BEGIN
		-- When this function is called from USP_DemandForecast_Execute then vendor is passed null 
		-- so get performance improvment becaues vendor price checking is not happend for all items
		IF ISNULL(@numVendorID,0) > 0
		BEGIN
		-- Select Item Vendor Detail
			SELECT
				@numDivisionID = numDivisionID,
				@monListPrice = monPrice,
				@numMinimumQty = intMinQty,
				@intLeadTimeDays = intLeadTimeDays
			FROM
				dbo.GetItemPurchaseVendorPrice(@numVendorID,@numItemCode,@numDomainID,@numQtyToOrder)
		END

		DECLARE @numDays AS INT = 0

		-- Find number of days after which order needs to be placed
		-- Lets on hand = 60, itemes sold per days = 10 
		-- then number of days  = (60/10) - @numLaedTimeDays
		SET @numDays = FLOOR((@numOnHand + @numOnOrder)/@numQtySoldPerDay) - @intLeadTimeDays

		IF @numDays < 0
			SET @numDays = 0

		SET @dtOrderDate = DATEADD(d,@numDays,GETDATE())

		IF DATENAME(dw,@dtOrderDate) = 'Saturday'
		BEGIN
			SET @dtOrderDate = DATEADD(d,-1,@dtOrderDate)

			IF CAST(@dtOrderDate AS DATE) < CAST(GETDATE() AS DATE)
				SET @dtOrderDate = GETDATE()
		END
		ELSE IF DATENAME(dw,@dtOrderDate) = 'Sunday'
		BEGIN
			SET @dtOrderDate = DATEADD(d,-2,@dtOrderDate)

			IF CAST(@dtOrderDate AS DATE) < CAST(GETDATE() AS DATE)
				SET @dtOrderDate = GETDATE()
		END
		
		-- If order date goes more than required forecast date select date with number of forecast date
		IF CAST(@dtOrderDate AS DATE) > CAST(DATEADD(d,@numForecastDays,GETDATE()) AS DATE) 
		BEGIN
			SET @dtOrderDate = DATEADD(d,@numForecastDays,GETDATE())
		END
	END

	

	INSERT INTO @TEMP 
		(
			numItemCode, 
			numOnHand, 
			numOnOrder, 
			numAllocation, 
			numBackOrder, 
			numReOrder, 
			numQtyToOrder, 
			dtOrderDate, 
			vcOrderDate, 
			numDivisionID, 
			monListPrice, 
			intMinQty, 
			intLeadTimeDays
		) 
	VALUES 
		(
			@numItemCode,
			@numOnHand,
			@numOnOrder,
			@numAllocation,
			@numBackOrder, 
			@numReOrder, 
			CEILING(@numQtyToOrder),
			@dtOrderDate,
			(
				CASE WHEN CONVERT(DATE,@dtOrderDate) = CONVERT(DATE,GETDATE()) THEN
				'<b><font color=red>Today</font></b>'
				WHEN CONVERT(DATE,@dtOrderDate) = CONVERT(DATE,DATEADD(d,1,GETDATE())) THEN
				'<b><font color=purple>Tomorrow</font></b>'
				ELSE
					dbo.FormatedDateFromDate(@dtOrderDate,@numDomainID)
				END
			),
			@numDivisionID, 
			@monListPrice, 
			@numMinimumQty, 
			@intLeadTimeDays
		)

	RETURN
END

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetOrderShipReceiveStatus')
DROP FUNCTION GetOrderShipReceiveStatus
GO
CREATE FUNCTION [dbo].[GetOrderShipReceiveStatus] 
(
      @numOppID AS NUMERIC(18,0),
	  @tintOppType AS NUMERIC(18,0),
	  @tintshipped AS TINYINT
)
RETURNS VARCHAR(100)
AS 
BEGIN	
	DECLARE @vcShippedReceivedStatus VARCHAR(100) = ''

	DECLARE @TotalQty AS FLOAT  = 0

	IF @tintOppType = 1 --SALES ORDER
	BEGIN
		DECLARE @numQtyShipped AS FLOAT = 0

		SELECT @TotalQty = ISNULL((SELECT SUM(ISNULL(dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), OI.numItemCode,I.numDomainId, ISNULL(OI.numUOMId, 0)),1) * numUnitHour) FROM OpportunityItems OI INNER JOIN Item I ON OI.numItemCode = I.numItemCode WHERE UPPER(I.charItemType) = 'P' AND ISNULL(bitDropship,0) = 0 AND numOppId = @numOppID),0)

		-- IF ORDER IS CLOSED WITHOUT FULFILLMENT MARK ALL QTY IS FULFILLED
		IF ISNULL(@tintshipped,0) = 1
		BEGIN
			SET @numQtyShipped = @TotalQty
		END
		ELSE
		BEGIN
			SELECT @numQtyShipped = ISNULL((SELECT SUM(ISNULL(dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), OI.numItemCode,I.numDomainId, ISNULL(OI.numUOMId, 0)),1) * numQtyShipped) FROM OpportunityItems OI INNER JOIN Item I ON OI.numItemCode = I.numItemCode  WHERE UPPER(I.charItemType) = 'P' AND ISNULL(bitDropship,0) = 0 AND numOppId = @numOppID),0)
		END

		SELECT @vcShippedReceivedStatus =(CASE WHEN  @TotalQty = @numQtyShipped THEN '<b><font color="green">' ELSE '<b><font color="red">' END) + CONCAT(CAST(@TotalQty AS FLOAT),' / ',CAST(@numQtyShipped AS FLOAT)) + '</font></b>'
	END
	ELSE IF @tintOppType = 2 --PURCHASE ORDER
	BEGIN
		DECLARE @numQtyReceived AS FLOAT = 0

		SELECT @TotalQty = ISNULL((SELECT SUM(ISNULL(dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), OI.numItemCode,I.numDomainId, ISNULL(OI.numUOMId, 0)),1) * numUnitHour) FROM OpportunityItems OI INNER JOIN Item I ON OI.numItemCode = I.numItemCode  WHERE UPPER(I.charItemType) = 'P' AND ISNULL(bitDropship,0) = 0 AND numOppId = @numOppID),0)
		
		-- IF ORDER IS CLOSED WITHOUT FULFILLMENT MARK ALL QTY IS FULFILLED
		IF ISNULL(@tintshipped,0) = 1
		BEGIN
			SET @numQtyReceived = @TotalQty
		END
		ELSE
		BEGIN
			SELECT @numQtyReceived = ISNULL((SELECT SUM(ISNULL(dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), OI.numItemCode,I.numDomainId, ISNULL(OI.numUOMId, 0)),1) * numUnitHourReceived) FROM OpportunityItems OI INNER JOIN Item I ON OI.numItemCode = I.numItemCode  WHERE UPPER(I.charItemType) = 'P' AND ISNULL(bitDropship,0) = 0 AND numOppId = @numOppID),0)
		END

		SELECT @vcShippedReceivedStatus =(CASE WHEN  @TotalQty = @numQtyReceived THEN '<b><font color="green">' ELSE '<b><font color="red">' END) + CONCAT(CAST(@TotalQty AS FLOAT),' / ',CAST(@numQtyReceived AS FLOAT)) + '</font></b>'
	END

	RETURN @vcShippedReceivedStatus
END
GO
-- SELECT dbo.GetPriceBasedOnPriceBook(366,100,1)
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='getpricebasedonpricebook')
DROP FUNCTION getpricebasedonpricebook
GO
CREATE FUNCTION GetPriceBasedOnPriceBook
--Below function applies Price Rule on given Price an Qty and returns calculated value
    (
      @numRuleID NUMERIC,
      @monPrice MONEY,
      @units FLOAT,
      @ItemCode numeric
    )
RETURNS MONEY
AS BEGIN
    DECLARE @PriceBookPrice MONEY
    DECLARE @tintRuleType TINYINT
    DECLARE @tintPricingMethod TINYINT
    DECLARE @tintDiscountType TINYINT
    DECLARE @decDiscount FLOAT
    DECLARE @intQntyItems INT
    DECLARE @decMaxDedPerAmt FLOAT
--    DECLARE @intOperator INT
    
    SET @PriceBookPrice = 0
    SELECT  @tintRuleType = PB.[tintRuleType],
			@tintPricingMethod = PB.[tintPricingMethod],
			@tintDiscountType = PB.[tintDiscountType],
			@decDiscount = PB.[decDiscount],
			@intQntyItems = PB.[intQntyItems],
			@decMaxDedPerAmt = PB.[decMaxDedPerAmt]
	FROM    [PriceBookRules] PB
			LEFT OUTER JOIN [PriceBookRuleDTL] PD ON PB.[numPricRuleID] = PD.[numRuleID]
	WHERE   [numPricRuleID] = @numRuleID
	
	
	
    
    IF ( @tintPricingMethod = 1 ) -- Price table
        BEGIN
				SET @intQntyItems = 1;
			   SELECT TOP 1  @tintRuleType = [tintRuleType],
						@tintDiscountType = [tintDiscountType],
						@decDiscount = [decDiscount]
			   FROM     [PricingTable]
			   WHERE    [numPriceRuleID] = @numRuleID
						AND @units BETWEEN [intFromQty] AND [intToQty]

IF (@@ROWCOUNT=0)						
	RETURN @monPrice;


IF @tintRuleType = 2
	BEGIN
		SELECT @monPrice = dbo.[fn_GetVendorCost](@ItemCode)
	END
	
				-- SELECT dbo.GetPriceBasedOnPriceBook(366,500,20)
				 IF ( @tintDiscountType = 1 ) -- Percentage 
                BEGIN
					
					SET @decDiscount = @monPrice * ( @decDiscount /100);

					IF ( @tintRuleType = 1 )
							SET @PriceBookPrice =  (@monPrice - @decDiscount)  ;
					IF ( @tintRuleType = 2 )
							SET @PriceBookPrice = @monPrice + @decDiscount   ;
					
                END
               -- SELECT dbo.GetPriceBasedOnPriceBook(366,500,21)
				IF ( @tintDiscountType = 2 ) -- Flat discount 
					BEGIN
							
						IF ( @tintRuleType = 1 )
								SET @PriceBookPrice =  (@monPrice -   @decDiscount) ;
						IF ( @tintRuleType = 2 )
								SET @PriceBookPrice =  (@monPrice +  @decDiscount)  ;
					END
           
        END
        
    IF ( @tintPricingMethod = 2 ) -- Pricing Formula 
        BEGIN

		IF @tintRuleType = 2
		BEGIN
			SELECT @monPrice = dbo.[fn_GetVendorCost](@ItemCode)
		END
		  
    -- SELECT dbo.GetPriceBasedOnPriceBook(368,500,10)

            IF ( @tintDiscountType = 1 ) -- Percentage 
                BEGIN
					SET @decDiscount = @decDiscount * (@units/@intQntyItems);
					SET @decDiscount = @decDiscount * @monPrice /100;
--					RETURN @decDiscount
--					RETURN @decMaxDedPerAmt
					SET @decMaxDedPerAmt = @decMaxDedPerAmt * @monPrice /100;
					
					
					IF (@decDiscount > @decMaxDedPerAmt)
						SET @decDiscount = @decMaxDedPerAmt;
					
					
					IF ( @tintRuleType = 1 )
							SET @PriceBookPrice = @monPrice - @decDiscount  ;
					IF ( @tintRuleType = 2 )
							SET @PriceBookPrice = @monPrice + @decDiscount   ;
					
                END
            IF ( @tintDiscountType = 2 ) -- Flat discount 
                BEGIN
					SET @decDiscount = @decDiscount * (@units/@intQntyItems);
--					RETURN @decDiscount
--					RETURN @decMaxDedPerAmt

					IF (@decDiscount > @decMaxDedPerAmt)
						SET @decDiscount = @decMaxDedPerAmt;
					
					IF ( @tintRuleType = 1 )
							SET @PriceBookPrice =  (@monPrice -   @decDiscount)  ;
					IF ( @tintRuleType = 2 )
							SET @PriceBookPrice = (@monPrice +  @decDiscount)  ;
                END
			
        END


    RETURN @PriceBookPrice ;

   END
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetUnitPriceAfterPriceLevelApplication')
DROP FUNCTION GetUnitPriceAfterPriceLevelApplication
GO
CREATE FUNCTION [dbo].[GetUnitPriceAfterPriceLevelApplication]
(
	@numDomainID numeric(18,0), 
	@numItemCode NUMERIC(18,0), 
	@numQuantity FLOAT, 
	@numWarehouseItemID NUMERIC(18,0), 
	@isPriceRule BIT, 
	@numPriceRuleID NUMERIC(18,0),
	@numDivisionID NUMERIC(18,0)
)  
RETURNS FLOAT  
AS  
BEGIN  

DECLARE @finalUnitPrice FLOAT = 0
DECLARE @tintRuleType INT
DECLARE @tintDiscountType INT
DECLARE @decDiscount FLOAT
DECLARE @ItemPrice FLOAT

SET @tintRuleType = 0
SET @tintDiscountType = 0
SET @decDiscount  = 0
SET @ItemPrice = 0

IF @isPriceRule = 1
	SELECT TOP 1
		@tintRuleType = tintRuleType,
		@tintDiscountType = tintDiscountType,
		@decDiscount = decDiscount
	FROM 
		PricingTable 
	WHERE 
		numPriceRuleID = @numPriceRuleID AND
		(@numQuantity BETWEEN intFromQty AND intToQty)
ELSE
	SELECT TOP 1
		@tintRuleType = tintRuleType,
		@tintDiscountType = tintDiscountType,
		@decDiscount = decDiscount
	FROM 
		PricingTable 
	WHERE 
		numItemCode = @numItemCode AND
		((@numQuantity BETWEEN intFromQty AND intToQty) OR tintRuleType = 3)

IF @tintRuleType > 0 AND @tintDiscountType > 0
BEGIN
	IF @tintRuleType = 1 -- Deduct from List price
	BEGIN
		IF (SELECT ISNULL(charItemType,'') FROM Item WHERE numDomainID = @numDomainID AND numItemCode = @numItemCode) = 'P'
			If (SELECT COUNT(*) FROM Item WHERE numDomainID = @numDomainID AND numItemCode = @numItemCode AND (bitKitParent = 1 OR bitAssembly = 1)) > 0 -- Kit OR Assembly
				SELECT @ItemPrice = dbo.GetUnitPriceForKitOrAssembly(@numDomainID,@numItemCode,@numWarehouseItemID,@numQuantity)
			ELSE
				SELECT @ItemPrice = ISNULL(monWListPrice,0) FROM WareHouseItems WHERE numDomainID = @numDomainID AND numItemID = @numItemCode AND numWareHouseItemID = @numWarehouseItemID
		ELSE
			SELECT @ItemPrice = ISNULL(monListPrice,0) FROM Item WHERE numDomainID = @numDomainID AND numItemCode = @numItemCode

		IF @tintDiscountType = 1 -- Percentage
		BEGIN
			IF @decDiscount > 0
				SELECT @finalUnitPrice = @ItemPrice - (@ItemPrice * (@decDiscount/100))
		END
		ELSE IF @tintDiscountType = 2 -- Flat Amount
		BEGIN
				SELECT @finalUnitPrice = @ItemPrice - @decDiscount
		END
	END
	ELSE IF @tintRuleType = 2 -- Add to Primary Vendor Cost
	BEGIN
		If (SELECT COUNT(*) FROM Item WHERE numDomainID = @numDomainID AND numItemCode = @numItemCode AND (bitKitParent = 1 OR bitAssembly = 1)) > 0 -- Kit OR Assembly
			SELECT @ItemPrice = dbo.GetUnitPriceForKitOrAssembly(@numDomainID,@numItemCode,@numWarehouseItemID,@numQuantity)
		ELSE
			SELECT @ItemPrice = ISNULL(monCost,0) FROM Vendor WHERE numItemCode = @numItemCode AND numVendorID = (SELECT numVendorID FROM Item WHERE numItemCode = @numItemCode)

		IF @tintDiscountType = 1 -- Percentage
		BEGIN
			IF @decDiscount > 0
				SELECT @finalUnitPrice = @ItemPrice + (@ItemPrice * (@decDiscount/100))
		END
		ELSE IF @tintDiscountType = 2 -- Flat Amount
		BEGIN
			SELECT @finalUnitPrice = @ItemPrice + @decDiscount
		END
	END
	ELSE IF @tintRuleType = 3 -- Named Price
	BEGIN
		IF ISNULL(@numDivisionID,0) = 0
		BEGIN
			SELECT @finalUnitPrice = @decDiscount
		END
		ELSE
		BEGIN
			DECLARE @monPriceLevelPrice AS FLOAT = 0
			DECLARE @tintPriceLevel INT = 0
			SELECT @tintPriceLevel=ISNULL(tintPriceLevel,0) FROM DivisionMaster WHERE numDivisionID=@numDivisionID

			IF ISNULL(@tintPriceLevel,0) > 0
			BEGIN
				SET @decDiscount = 0

				SELECT 
					@monPriceLevelPrice = ISNULL(decDiscount,0)
				FROM
				(
					SELECT 
						ROW_NUMBER() OVER(PARTITION BY PricingTable.numItemCode ORDER BY numPricingID) Id,
						decDiscount
					FROM 
						PricingTable 
					INNER JOIN
						Item
					ON
						Item.numItemCode = @numItemCode 
						AND Item.numDomainID = @numDomainID
					WHERE 
						tintRuleType = 3 
				) TEMP
				WHERE
					Id BETWEEN @tintPriceLevel AND @tintPriceLevel

				IF ISNULL(@monPriceLevelPrice,0) > 0
				BEGIN
					SELECT @finalUnitPrice = @monPriceLevelPrice
				END
				ELSE
				BEGIN
					SELECT @finalUnitPrice = @decDiscount
				END
			END
			ELSE
			BEGIN
				SELECT @finalUnitPrice = @decDiscount
			END
		END
	END
END

return @finalUnitPrice
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetUnitPriceAfterPriceRuleApplication')
DROP FUNCTION GetUnitPriceAfterPriceRuleApplication
GO
CREATE FUNCTION [dbo].[GetUnitPriceAfterPriceRuleApplication]
(
	@numRuleID NUMERIC(18,0), 
	@numDomainID numeric(18,0), 
	@numItemCode NUMERIC(18,0), 
	@numQuantity FLOAT, 
	@numWarehouseItemID NUMERIC(18,0),
	@numDivisionID NUMERIC(18,0)
)  
RETURNS FLOAT  
AS  
BEGIN  

DECLARE @finalUnitPrice FLOAT = 0
DECLARE @ItemPrice FLOAT = 0
DECLARE @tintRuleType INT
DECLARE @tintPricingMethod INT
DECLARE @tintDiscountType INT
DECLARE @decDiscount FLOAT
DECLARE @decMaxDiscount FLOAT
DECLARE @tempDecMaxDiscount FLOAT
DECLARE @decDiscountPerQty INT

SELECT 
	@tintPricingMethod=tintPricingMethod, 
	@tintRuleType = tintRuleType, 
	@tintDiscountType = tintDiscountType,
	@decDiscount = decDiscount,
	@decMaxDiscount = decMaxDedPerAmt,
	@decDiscountPerQty = intQntyItems
FROM 
	PriceBookRules 
WHERE numPricRuleID = @numRuleID

IF @tintPricingMethod = 1 --Use Pricing Table
BEGIN
	EXEC @finalUnitPrice = GetUnitPriceAfterPriceLevelApplication @numDomainID = @numDomainID, @numItemCode = @numItemCode, @numQuantity = @numQuantity, @numWarehouseItemID = @numWarehouseItemID,  @isPriceRule = 1, @numPriceRuleID = @numRuleID, @numDivisionID = 0 -- PASS 0 FOR DIVISION ID BECAUSE THIS PRICE TABLE IS FROM PRICE BOOK RULE
END
ELSE IF @tintPricingMethod = 2 --Use Pricing Formula
BEGIN
	IF @tintRuleType = 1 --Deduct from List price
	BEGIN
		IF (SELECT ISNULL(charItemType,'') FROM Item WHERE numItemCode = @numItemCode) = 'P'
			If (SELECT COUNT(*) FROM Item WHERE numDomainID = @numDomainID AND numItemCode = @numItemCode AND (bitKitParent = 1 OR bitAssembly = 1)) > 0 -- Kit OR Assembly
				SELECT @ItemPrice = dbo.GetUnitPriceForKitOrAssembly(@numDomainID,@numItemCode,@numWarehouseItemID,@numQuantity)
			ELSE
				SELECT @ItemPrice = ISNULL(monWListPrice,0) FROM WareHouseItems WHERE numDomainID = @numDomainID AND numWareHouseItemID = @numWarehouseItemID
		ELSE
			SELECT @ItemPrice = ISNULL(monListPrice,0) FROM Item WHERE numDomainID = @numDomainID AND numItemCode = @numItemCode

		/* If price > 0 then apply rule calculation */
		IF @ItemPrice > 0
		BEGIN
			IF @decDiscount > 0
			BEGIN
				IF @numQuantity < @decDiscountPerQty
					SET @tempDecMaxDiscount = 1 * @decDiscount
				ELSE
					SET @tempDecMaxDiscount = (@numQuantity/@decDiscountPerQty) * @decDiscount

				IF @decMaxDiscount > 0 AND @tempDecMaxDiscount > @decMaxDiscount
					SET @tempDecMaxDiscount = @decMaxDiscount

				IF @tintDiscountType = 1 --Percentage
				BEGIN
					SELECT @finalUnitPrice = @ItemPrice - (@ItemPrice * (@tempDecMaxDiscount/100))
				END
				ELSE IF @tintDiscountType = 2 --Flat Amount
				BEGIN
					SELECT @finalUnitPrice = @ItemPrice - @tempDecMaxDiscount
				END
			END
		END
	END
	ELSE IF @tintRuleType = 2 --Add to Primary Vendor Cost
	BEGIN
		If (SELECT COUNT(*) FROM Item WHERE numDomainID = @numDomainID AND numItemCode = @numItemCode AND (bitKitParent = 1 OR bitAssembly = 1)) > 0 -- Kit OR Assembly
			SELECT @ItemPrice = dbo.GetUnitPriceForKitOrAssembly(@numDomainID,@numItemCode,@numWarehouseItemID,@numQuantity)
		ELSE
			SELECT @ItemPrice = ISNULL(monCost,0) FROM Vendor WHERE numItemCode = @numItemCode AND numVendorID = (SELECT numVendorID FROM Item WHERE numItemCode = @numItemCode)
		/* If price > 0 then apply rule calculation */
		IF @ItemPrice > 0
		BEGIN
			IF @decDiscount > 0
			BEGIN
				IF @numQuantity < @decDiscountPerQty
					SET @tempDecMaxDiscount = 1 * @decDiscount
				ELSE
					SET @tempDecMaxDiscount = (@numQuantity/@decDiscountPerQty) * @decDiscount

				IF @decMaxDiscount > 0 AND @tempDecMaxDiscount > @decMaxDiscount
					SET @tempDecMaxDiscount = @decMaxDiscount

				IF @tintDiscountType = 1 --Percentage
				BEGIN
					SELECT @finalUnitPrice = @ItemPrice + (@ItemPrice * (@tempDecMaxDiscount/100))
				END
				ELSE IF @tintDiscountType = 2 --Flat Amount
				BEGIN
					SELECT @finalUnitPrice = @ItemPrice + @tempDecMaxDiscount
				END
			END
		END
	END
END


return @finalUnitPrice
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_AddItemToExistingInvoice')
DROP PROCEDURE USP_AddItemToExistingInvoice
GO
CREATE PROCEDURE USP_AddItemToExistingInvoice
    @numOppID NUMERIC(9),
    @numOppBizDocID NUMERIC(9),
    @numItemCode NUMERIC(9),
    @numUnitHour FLOAT,
    @monPrice MONEY,
    @vcItemDesc VARCHAR(1000),
--    @vcNotes VARCHAR(500),
    @numCategoryHDRID NUMERIC(9),
    @numProId numeric(9),
	@numStageId numeric(9),
	@numClassID numeric(9)
AS 
BEGIN
    DECLARE @numOppItemID NUMERIC(9)

--    IF ( SELECT COUNT(*)
--         FROM   [OpportunityItems]
--         WHERE  numItemCode = @numItemCode
--                AND numOppID = @numOppID
--       ) = 0 
--        BEGIN
            DECLARE @vcItemName VARCHAR(300)
            DECLARE @vcModelID VARCHAR(200)
            DECLARE @vcManufacturer VARCHAR(250)
            DECLARE @vcPathForTImage VARCHAR(300)
            SELECT  @vcItemName = vcItemName,
                    @vcModelID = vcModelID,
                    @vcManufacturer = vcManufacturer,
                    @vcPathForTImage = (SELECT TOP 1 vcPathForTImage FROM dbo.ItemImages II WHERE II.numItemCode = numItemCode AND II.numDomainId= numDomainId AND II.bitDefault=1)
            FROM    Item
            WHERE   numItemCode = @numItemCode
			
            INSERT  INTO [OpportunityItems] ( [numOppId],
                                              [numItemCode],
                                              [numUnitHour],
                                              [monPrice],
                                              [monTotAmount],
                                              [numSourceID],
                                              [vcItemDesc],
                                              [numWarehouseItmsID],
                                              [vcType],
                                              [vcAttributes],
                                              [bitDropShip],
                                              [numUnitHourReceived],
                                              [bitDiscountType],
                                              [fltDiscount],
                                              [monTotAmtBefDiscount],
                                              vcItemName,
                                              vcModelID,
                                              vcManufacturer,
                                              vcPathForTImage,numProjectID,numClassID,numProjectStageID)
            VALUES  (
                      @numOppID,
                      @numItemCode,
                      @numUnitHour,
                      @monPrice,
                      ( @monPrice * @numUnitHour ),
                      NULL,
                      @vcItemDesc,
                      NULL,
                      'Service',
                      NULL,
                      NULL,
                      NULL,
                      0,
                      0,
                      ( @monPrice * @numUnitHour ),
                      @vcItemName,
                      @vcModelID,
                      @vcManufacturer,
                      @vcPathForTImage,@numProId,@numClassID,@numStageId
                    )
           SET @numOppItemID = @@IDENTITY
--        END
--    ELSE 
--        BEGIN
--            SELECT  @numOppItemID = [numoppitemtCode]
--            FROM    [OpportunityItems]
--            WHERE   [numItemCode] = @numItemCode
--                    AND [numOppId] = @numOppID
--        END
--            
--		
--    INSERT  INTO [OpportunityBizDocItems] ( [numOppBizDocID],
--                                            [numOppItemID],
--                                            [numItemCode],
--                                            [numUnitHour],
--                                            [monPrice],
--                                            [monTotAmount],
--                                            [vcItemDesc],
--                                            [numWarehouseItmsID],
--                                            [vcType],
--                                            [vcAttributes],
--                                            [bitDropShip],
--                                            [bitDiscountType],
--                                            [fltDiscount],
--                                            [monTotAmtBefDiscount],
--                                            [vcNotes],
--                                            [vcTrackingNo] )
--    VALUES  (
--              @numOppBizDocID,
--              @numOppItemID,
--              @numItemCode,
--              @numUnitHour,
--              @monPrice,
--              ( @monPrice * @numUnitHour ),
--              @vcItemDesc,
--              NULL,
--              'Service',
--              NULL,
--              NULL,
--              0,
--              0,
--              ( @monPrice * @numUnitHour ),
--              @vcNotes,
--              '' 
--                
--            )
--            DECLARE @numOppBizDocItemID NUMERIC 
--            SET @numOppBizDocItemID = @@IDENTITY
            
            IF @numCategoryHDRID>0
            BEGIN
				UPDATE dbo.TimeAndExpense SET numOppItemID=@numOppItemID,numOppId = @numOppId,numStageId=@numStageId WHERE numCategoryHDRID = @numCategoryHDRID
			END

END

/****** Object:  StoredProcedure [dbo].[USP_AddUpdateWareHouseForItems]    Script Date: 07/26/2008 16:14:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_AddUpdateWareHouseForItems')
DROP PROCEDURE USP_AddUpdateWareHouseForItems
GO
CREATE PROCEDURE [dbo].[USP_AddUpdateWareHouseForItems]  
@numItemCode as numeric(9)=0,  
@numWareHouseID as numeric(9)=0,
@numWareHouseItemID as numeric(9)=0 OUTPUT,
@vcLocation as varchar(250)='',
@monWListPrice as money =0,
@numOnHand as FLOAT=0,
@numReorder as FLOAT=0,
@vcWHSKU as varchar(100)='',
@vcBarCode as varchar(50)='',
@numDomainID AS NUMERIC(9),
@strFieldList as TEXT='',
@vcSerialNo as varchar(100)='',
@vcComments as varchar(1000)='',
@numQty as numeric(18)=0,
@byteMode as tinyint=0,
@numWareHouseItmsDTLID as numeric(18)=0,
@numUserCntID AS NUMERIC(9)=0,
@dtAdjustmentDate AS DATETIME=NULL,
@numWLocationID NUMERIC(9)=0
AS  
BEGIN
	DECLARE @numDomain AS NUMERIC(18,0)
	SET @numDomain = @numDomainID

	DECLARE @bitLotNo AS BIT;
	SET @bitLotNo=0  
	
	DECLARE @bitSerialized AS BIT;
	SET @bitSerialized=0  

	SELECT 
		@bitLotNo=ISNULL(Item.bitLotNo,0),
		@bitSerialized=ISNULL(Item.bitSerialized,0)
	FROM 
		Item 
	WHERE 
		numItemCode=@numItemCode 
		AND numDomainID=@numDomainID

	DECLARE @vcDescription AS VARCHAR(100)

	IF @byteMode=0 or @byteMode=1 or @byteMode=2 or @byteMode=5
	BEGIN
		--Insert/Update WareHouseItems
		IF @byteMode=0 or @byteMode=1
		BEGIN
			IF @numWareHouseItemID>0
			BEGIN
				UPDATE 
					WareHouseItems
				SET 
					numWareHouseID=@numWareHouseID,				
					numReorder=@numReorder,
					monWListPrice=@monWListPrice,
					vcLocation=@vcLocation,
					numWLocationID = @numWLocationID,
					vcWHSKU=@vcWHSKU,
					vcBarCode=@vcBarCode,
					dtModified=GETDATE()   
				WHERE 
					numItemID=@numItemCode 
					AND numDomainID=@numDomainID 
					AND numWareHouseItemID=@numWareHouseItemID
		
				IF NOT EXISTS(SELECT numItemCode FROM dbo.OpportunityItems WHERE numItemCode = @numItemCode)
				BEGIN
					IF ((SELECT ISNULL(numOnHand,0) FROM WareHouseItems where numItemID = @numItemCode AND numDomainId = @numDomainID and numWareHouseItemID = @numWareHouseItemID) = 0)
					BEGIN
						UPDATE 
							WareHouseItems 
						SET 
							numOnHand = @numOnHand 
						WHERE 
							numItemID = @numItemCode 
							AND numDomainID = @numDomainID 
							AND numWareHouseItemID = @numWareHouseItemID		
					END
				END
		
				SET @vcDescription='UPDATE WareHouse'
			END
			ELSE
			BEGIN
				INSERT INTO WareHouseItems 
				(
					numItemID, 
					numWareHouseID,
					numOnHand,
					numReorder,
					monWListPrice,
					vcLocation,
					vcWHSKU,
					vcBarCode,
					numDomainID,
					dtModified,
					numWLocationID
				)  
				VALUES
				(
					@numItemCode,
					@numWareHouseID,
					(Case When @bitLotNo=1 or @bitSerialized=1 then 0 else @numOnHand end),
					@numReorder,
					@monWListPrice,
					@vcLocation,
					@vcWHSKU,
					@vcBarCode,
					@numDomainID,
					GETDATE(),
					@numWLocationID
				)  

				SET @numWareHouseItemID = @@identity
				SET @vcDescription='INSERT WareHouse'
			END
		END

		--Insert/Update WareHouseItmsDTL

		DECLARE @OldQty FLOAT = 0

		IF @byteMode=0 or @byteMode=2 or @byteMode=5
		BEGIN
			IF @bitLotNo=1 or @bitSerialized=1
			BEGIN
				IF @bitSerialized=1
					SET @numQty=1
	
				IF @byteMode=0
				BEGIN
					SELECT TOP 1 
						@numWareHouseItmsDTLID=numWareHouseItmsDTLID,
						@OldQty=numQty 
					FROM 
						WareHouseItmsDTL 
					WHERE 
						numWareHouseItemID=@numWareHouseItemID 
						AND vcSerialNo=@vcSerialNo and numQty > 0
				END
				ELSE IF @numWareHouseItmsDTLID>0
				BEGIN
					SELECT TOP 1 
						@OldQty=numQty 
					FROM 
						WareHouseItmsDTL 
					WHERE 
						numWareHouseItmsDTLID=@numWareHouseItmsDTLID
				END

				IF @numWareHouseItmsDTLID>0
				BEGIN
					UPDATE 
						WareHouseItmsDTL 
					SET 
						vcComments = @vcComments,
						numQty=@numQty,
						vcSerialNo=@vcSerialNo
					WHERE 
						numWareHouseItmsDTLID=@numWareHouseItmsDTLID 
						AND numWareHouseItemID=@numWareHouseItemID
                
					SET @vcDescription=CASE WHEN @byteMode=5 THEN 'Inventory Adjustment (Update Lot/Serial#) : ' ELSE 'UPDATE Lot/Serial# : ' END + @vcSerialNo 
				END
				ELSE
				BEGIN
				   INSERT INTO WareHouseItmsDTL
				   (numWareHouseItemID,vcSerialNo,vcComments,numQty,bitAddedFromPO)  
				   VALUES 
				   (@numWareHouseItemID,@vcSerialNo,@vcComments,@numQty,0)
				
				   SET @numWareHouseItmsDTLID=@@identity

				   SET @vcDescription=CASE WHEN @byteMode=5 THEN 'Inventory Adjustment (Insert Lot/Serial#) : ' ELSE 'INSERT Lot/Serial# : ' END + @vcSerialNo 
				END

 				UPDATE 
					WareHouseItems 
				SET 
					numOnHand=numOnHand + (@numQty - @OldQty),
					dtModified=GETDATE() 
				WHERE 
					numWareHouseItemID = @numWareHouseItemID 
					AND [numDomainID] = @numDomainID
					AND (numOnHand + (@numQty - @OldQty))>=0
			END 
		END

		IF DATALENGTH(@strFieldList)>2
		BEGIN
			--Insert Custom Fields base on WareHouseItems/WareHouseItmsDTL 
			declare @hDoc as int     
			EXEC sp_xml_preparedocument @hDoc OUTPUT, @strFieldList                                                                        

			declare  @rows as integer                                        
	                                         
			SELECT @rows=count(*) FROM OPENXML (@hDoc,'/NewDataSet/CusFlds',2)                                                  
							WITH (Fld_ID numeric(9),Fld_Value varchar(100))                                        
	                                                                           

			if @rows>0                                        
			begin  
			  Create table #tempTable (ID INT IDENTITY PRIMARY KEY,Fld_ID numeric(9),Fld_Value varchar(100))   
	                                      
			  insert into #tempTable (Fld_ID,Fld_Value)                                        
			  SELECT * FROM OPENXML (@hDoc,'/NewDataSet/CusFlds',2)                                                  
			  WITH (Fld_ID numeric(9),Fld_Value varchar(100))                                           
	                                         
			  delete CFW_Fld_Values_Serialized_Items from CFW_Fld_Values_Serialized_Items C                                        
			  inner join #tempTable T on C.Fld_ID=T.Fld_ID 
				where C.RecId=(Case when @bitLotNo=1 or @bitSerialized=1 then @numWareHouseItmsDTLID else @numWareHouseItemID end) and bitSerialized=(Case when @bitLotNo=1 or @bitSerialized=1 then 1 else 0 end)                                       
	      
			  insert into CFW_Fld_Values_Serialized_Items(Fld_ID,Fld_Value,RecId,bitSerialized)                                                  
			  select Fld_ID,isnull(Fld_Value,'') as Fld_Value,(Case when @bitLotNo=1 or @bitSerialized=1 then @numWareHouseItmsDTLID else @numWareHouseItemID end),(Case when @bitLotNo=1 or @bitSerialized=1 then 1 else 0 end) from #tempTable 

			  drop table #tempTable                                        
			 end  

			 EXEC sp_xml_removedocument @hDoc 
		END	  
 
		EXEC dbo.USP_ManageWareHouseItems_Tracking
		@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
		@numReferenceID = @numItemCode, --  numeric(9, 0)
		@tintRefType = 1, --  tinyint
		@vcDescription = @vcDescription, --  varchar(100)
		@numModifiedBy = @numUserCntID,
		@ClientTimeZoneOffset = 0,
		@dtRecordDate = @dtAdjustmentDate,
		@numDomainID = @numDomain 
	END
	ELSE IF @byteMode=3
	BEGIN
		IF EXISTS (SELECT * FROM [OpportunityMaster] OM INNER JOIN [OpportunityItems] OI ON OM.[numOppId] = OI.[numOppId] WHERE OM.[numDomainId]=@numDomainID AND [numWarehouseItmsID]=@numWareHouseItemID)
		BEGIN
			RAISERROR('OpportunityItems_Depend',16,1);
			RETURN
		END

		IF (SELECT COUNT(*) FROM OpportunityKitItems WHERE numWareHouseItemId=@numWareHouseItemID) > 0
 		BEGIN
	  		RAISERROR('OpportunityKitItems_Depend',16,1);
			RETURN
		END
	
		IF (SELECT COUNT(*) FROM ItemDetails WHERE numWareHouseItemId=@numWareHouseItemID) > 0
 		BEGIN
	  		RAISERROR ('KitItems_Depend',16,1);
			RETURN
		END	
	    
		IF @bitLotNo=1 OR @bitSerialized=1
		BEGIN
			DELETE from CFW_Fld_Values_Serialized_Items where RecId in (select numWareHouseItmsDTLID from WareHouseItmsDTL where numWareHouseItemID=@numWareHouseItemID)
					and bitSerialized=(Case when @bitLotNo=1 or @bitSerialized=1 then 1 else 0 end) 
		END
		ELSE
		BEGIN
			DELETE from CFW_Fld_Values_Serialized_Items where RecId=@numWareHouseItemID
					and bitSerialized=(Case when @bitLotNo=1 or @bitSerialized=1 then 1 else 0 end) 
		END

		DELETE from WareHouseItmsDTL where numWareHouseItemID=@numWareHouseItemID 
		DELETE FROM WareHouseItems_Tracking WHERE numWareHouseItemID=@numWareHouseItemID and numDomainID=@numDomainID
		DELETE from WareHouseItems where numWareHouseItemID=@numWareHouseItemID and numDomainID=@numDomainID
	END
	ELSE IF @byteMode=4
	BEGIN
		IF @bitLotNo=1 or @bitSerialized=1
		BEGIN
			DELETE from CFW_Fld_Values_Serialized_Items where RecId=@numWareHouseItmsDTLID
			and bitSerialized=(Case when @bitLotNo=1 or @bitSerialized=1 then 1 else 0 end) 
		END

		update WHI SET numOnHand=WHI.numOnHand - WHID.numQty,dtModified=GETDATE()
		from WareHouseItems WHI join WareHouseItmsDTL WHID on WHI.numWareHouseItemID=WHID.numWareHouseItemID 
		where WHID.numWareHouseItmsDTLID=@numWareHouseItmsDTLID 
		AND WHI.numDomainID = @numDomainID
		AND (WHI.numOnHand - WHID.numQty)>=0

		SELECT @numWareHouseItemID=WHI.numWareHouseItemID,@numItemCode=numItemID 
		from WareHouseItems WHI join WareHouseItmsDTL WHID on WHI.numWareHouseItemID=WHID.numWareHouseItemID 
		where WHID.numWareHouseItmsDTLID=@numWareHouseItmsDTLID 
		AND WHI.numDomainID = @numDomainID


		EXEC dbo.USP_ManageWareHouseItems_Tracking
			@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
			@numReferenceID = @numItemCode, --  numeric(9, 0)
			@tintRefType = 1, --  tinyint
			@vcDescription = 'DELETE Lot/Serial#', --  varchar(100)
			@numModifiedBy = @numUserCntID,
			@ClientTimeZoneOffset = 0,
			@dtRecordDate = NULL,
			@numDomainID = @numDomain

		DELETE from WareHouseItmsDTL where numWareHouseItmsDTLID=@numWareHouseItmsDTLID
	END
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_AutoAssign_OppSerializedItem')
DROP PROCEDURE USP_AutoAssign_OppSerializedItem
GO
CREATE PROCEDURE [dbo].[USP_AutoAssign_OppSerializedItem]  
    @numDomainID as numeric(9)=0,  
    @numOppID as numeric(9)=0,  
    @numOppBizDocsId as numeric(9)=0
AS  
BEGIN
	IF (SELECT ISNULL(bitAutoSerialNoAssign,0) FROM DOMAIN WHERE @numDomainID=numDomainID) = 1
	BEGIN
		CREATE TABLE #tempSerializedItem 
		(
			ID numeric(18) IDENTITY(1,1) NOT NULL,
			numItemCode numeric(9),
			numOppItemtCode numeric(9),
			numWarehouseItmsID numeric(9),
			bitLotNo bit,
			bitSerialized bit,
			numUnitHour FLOAT
		)

		INSERT INTO 
			#tempSerializedItem 
		SELECT 
			I.numItemCode,
			numOppItemtCode,
			numWarehouseItmsID,
			I.bitLotNo,
			I.bitSerialized,
			oppItems.numUnitHour 
		FROM 
			OpportunityItems oppItems 
		JOIN 
			Item I 
		ON 
			oppItems.numItemCode=I.numItemCode 
		WHERE 
			I.numDomainId=@numDomainID 
			AND oppItems.numOppId=@numOppId 
			AND (ISNULL(I.bitLotNo,0)=1 OR ISNULL(I.bitSerialized,0)=1)

		IF (SELECT COUNT(*) FROM #tempSerializedItem)>0
		BEGIN
			DECLARE  @maxID NUMERIC(9),@minID NUMERIC(9)
			DECLARE @numItemCode NUMERIC(9),@numOppItemtCode NUMERIC(9),@numWarehouseItmsID NUMERIC(9),@bitLotNo BIT,@bitSerialized BIT,@numUnitHour FLOAT

			SELECT @maxID = max(ID),@minID = min(ID) FROM  #tempSerializedItem 

			CREATE TABLE #tempWareHouseItmsDTL 
			(                                                                    
				ID numeric(18) IDENTITY(1,1) NOT NULL,
				vcSerialNo VARCHAR(100),
				numQty FLOAT,
				numWareHouseItmsDTLID [numeric](18, 0)                                             
			)   

			WHILE @minID <= @maxID
			BEGIN
				SELECT 
					@numItemCode=numItemCode,
					@numOppItemtCode=numOppItemtCode,
					@numWarehouseItmsID=numWarehouseItmsID,
					@bitLotNo=bitLotNo,
					@bitSerialized=bitSerialized,
					@numUnitHour=numUnitHour 
				FROM 
					#tempSerializedItem 
				WHERE 
					ID = @minID  

				INSERT INTO #tempWareHouseItmsDTL 
				(
					vcSerialNo,numWarehouseItmsDTLID,numQty
				)  
				SELECT 
					vcSerialNo,numWareHouseItmsDTLID,ISNULL(numQty,0) AS TotalQty
				FROM   
					WareHouseItmsDTL   
				WHERE 
					ISNULL(numQty,0) > 0 
					AND numWareHouseItemID=@numWarehouseItmsID  
					and numWareHouseItmsDTLID NOT IN (SELECT numWarehouseItmsDTLID FROM OppWarehouseSerializedItem WHERE numOppID=@numOppID AND numWarehouseItmsID=@numWarehouseItmsID AND numOppBizDocsId=@numOppBizDocsId) 
				ORDER BY 
					TotalQty desc

				IF ((SELECT SUM(numQty) FROM #tempWareHouseItmsDTL)>=@numUnitHour)
				BEGIN
					DECLARE  @maxWareID NUMERIC(9),@minWareID NUMERIC(9)
					DECLARE  @numWarehouseItmsDTLID NUMERIC(9),@numQty FLOAT,@numUseQty FLOAT
	
					SELECT @maxWareID = max(ID),@minWareID = min(ID) FROM  #tempWareHouseItmsDTL 

					WHILE (@numUnitHour>0 AND @minWareID <= @maxWareID)
					BEGIN
						SELECT 
							@numWarehouseItmsDTLID = numWarehouseItmsDTLID,
							@numQty = numQty 
						FROM  
							#tempWareHouseItmsDTL  
						WHERE 
							ID=@minWareID

						IF @numUnitHour >= @numQty
						BEGIN
							SET @numUseQty=@numQty
	
							SET @numUnitHour=@numUnitHour-@numQty
						END
						else IF @numUnitHour < @numQty
						BEGIN
							SET @numUseQty=@numUnitHour

							SET @numUnitHour=0
						END
		
						INSERT INTO OppWarehouseSerializedItem
						(
							numWarehouseItmsDTLID,numOppID,numOppItemID,numWarehouseItmsID,numQty,numOppBizDocsId
						)                
						SELECT 
							@numWarehouseItmsDTLID,@numOppID,@numOppItemTcode,@numWarehouseItmsID,@numUseQty,@numOppBizDocsId

						SET @minWareID=@minWareID + 1
					END
				END

				TRUNCATE TABLE #tempWareHouseItmsDTL

				SET @minID=@minID + 1
			END

			DROP TABLE #tempWareHouseItmsDTL
		END

		DROP TABLE #tempSerializedItem
	END
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_BizRecurrence_WorkOrder')
DROP PROCEDURE USP_BizRecurrence_WorkOrder
GO
  
Create PROCEDURE [dbo].[USP_BizRecurrence_WorkOrder]   
	@numDomainID NUMERIC(18,0),
	@numUserCntID NUMERIC(18,0),
	@numOppID NUMERIC(18,0),
	@numParentOppID NUMERIC(18,0)
AS  
BEGIN  

IF (SELECT COUNT(*) FROM WorkOrder WHERE numOppId=@numOppID) = 0 AND
	(SELECT COUNT(*) FROM WorkOrder WHERE numOppId=@numParentOppID) > 0
BEGIN
   
	DECLARE @numItemCode AS numeric(9),@numUnitHour AS FLOAT,@numWarehouseItmsID AS numeric(9),@vcItemDesc AS VARCHAR(1000)
  
	DECLARE @numWOId AS NUMERIC(9),@vcInstruction AS VARCHAR(1000),@bintCompliationDate AS DATETIME,@numWOAssignedTo NUMERIC(9)


	DECLARE WOCursor CURSOR FOR 
		SELECT 
			X.numItemCode,
			dbo.fn_UOMConversion(X.numUOMId,numItemCode,@numDomainId,null) * x.numUnitHour AS numUnitHour,
			X.numWarehouseItmsID,
			X.vcItemDesc,
			X.vcInstruction,
			X.bintCompliationDate,
			X.numAssignedTo
		FROM
			(
				SELECT
					OpportunityItems.numItemCode,
					OpportunityItems.bitWorkOrder,
					OpportunityItems.numUOMId,
					OpportunityItems.numUnitHour,
					OpportunityItems.numWarehouseItmsID,
					OpportunityItems.vcItemDesc,
					WorkOrder.vcInstruction,
					WorkOrder.bintCompliationDate,
					WorkOrder.numAssignedTo
				FROM
					OpportunityItems
				INNER JOIN	
					WorkOrder
				ON
					WorkOrder.numItemCode = OpportunityItems.numItemCode AND
					WorkOrder.numWareHouseItemId = OpportunityItems.numWarehouseItmsID AND
					WorkOrder.numOppId = OpportunityItems.numOppId
				WHERE
					OpportunityItems.numOppId = @numParentOppID
			)X  
		WHERE 
			X.bitWorkOrder=1

	OPEN WOCursor;

	FETCH NEXT FROM WOCursor INTO @numItemCode, @numUnitHour,@numWarehouseItmsID,@vcItemDesc,@vcInstruction,@bintCompliationDate,@numWOAssignedTo

	WHILE @@FETCH_STATUS = 0
	BEGIN
	INSERT INTO WorkOrder
	(
		numItemCode, numQtyItemsReq, numWareHouseItemId, numCreatedBy, bintCreatedDate,	numDomainID,
		numWOStatus, numOppId, vcItemDesc, vcInstruction, bintCompliationDate, numAssignedTo
	)
	VALUES
	(
		@numItemCode,@numUnitHour,@numWarehouseItmsID,@numUserCntID,getutcdate(),@numDomainID,
		0,@numOppID,@vcItemDesc,@vcInstruction,@bintCompliationDate,@numWOAssignedTo
	)
	
	set @numWOId=@@IDENTITY
	
	IF @numWOAssignedTo>0
	BEGIN
		DECLARE @numDivisionId AS NUMERIC(9) ,@vcItemName AS VARCHAR(300)
	
		SELECT @vcItemName=ISNULL(vcItemName,'') FROM Item WHERE numItemCode=@numItemCode

		SET @vcItemName= 'Work Order for Item : ' + @vcItemName

		SELECT @numDivisionId=numDivisionId FROM AdditionalContactsInformation WHERE numContactId=@numWOAssignedTo

		EXEC dbo.usp_InsertCommunication
			@numCommId = 0, --  numeric(18, 0)
			@bitTask = 972, --  numeric(18, 0)
			@numContactId = @numUserCntID, --  numeric(18, 0)
			@numDivisionId = @numDivisionId, --  numeric(18, 0)
			@txtDetails = @vcItemName, --  text
			@numOppId = 0, --  numeric(18, 0)
			@numAssigned = @numWOAssignedTo, --  numeric(18, 0)
			@numUserCntID = @numUserCntID, --  numeric(18, 0)
			@numDomainId = @numDomainID, --  numeric(18, 0)
			@bitClosed = 0, --  tinyint
			@vcCalendarName = '', --  varchar(100)
			@dtStartTime = @bintCompliationDate, --  datetime
			@dtEndtime = @bintCompliationDate, --  datetime
			@numActivity = 0, --  numeric(9, 0)
			@numStatus = 0, --  numeric(9, 0)
			@intSnoozeMins = 0, --  int
			@tintSnoozeStatus = 0, --  tinyint
			@intRemainderMins = 0, --  int
			@tintRemStaus = 0, --  tinyint
			@ClientTimeZoneOffset = 0, --  int
			@bitOutLook = 0, --  tinyint
			@bitSendEmailTemplate = 0, --  bit
			@bitAlert = 0, --  bit
			@numEmailTemplate = 0, --  numeric(9, 0)
			@tintHours = 0, --  tinyint
			@CaseID = 0, --  numeric(18, 0)
			@CaseTimeId = 0, --  numeric(18, 0)
			@CaseExpId = 0, --  numeric(18, 0)
			@ActivityId = 0, --  numeric(18, 0)
			@bitFollowUpAnyTime = 0, --  bit
			@strAttendee=''
	END
		
	;WITH CTE(numItemKitID,numItemCode,numWarehouseItmsID,numCalculatedQty,txtItemDesc,sintOrder,numUOMId,numQtyItemsReq)
	AS
	(
	select CAST(@numItemCode AS NUMERIC(9)),numItemCode,isnull(Dtl.numWareHouseItemId,0) as numWarehouseItmsID
	,CAST(((DTL.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(Dtl.numUOMID,item.numItemCode,item.numDomainID,item.numBaseUnit),1)) * @numUnitHour)AS FLOAT) AS numCalculatedQty,ISNULL(Dtl.vcItemDesc,txtItemDesc) AS txtItemDesc,ISNULL(sintOrder,0) AS sintOrder,
	Dtl.numUOMId,DTL.numQtyItemsReq
	from item                                
	INNER join ItemDetails Dtl on numChildItemID=numItemCode
	where  numItemKitID=@numItemCode 
	--AND item.charItemType IN ('P','I')

	UNION ALL

	select CAST(Dtl.numItemKitID AS NUMERIC(9)),i.numItemCode,isnull(Dtl.numWareHouseItemId,0) as numWarehouseItmsID,
	CAST(((DTL.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(Dtl.numUOMID,i.numItemCode,i.numDomainID,i.numBaseUnit),1)) * c.numCalculatedQty) AS FLOAT) AS numCalculatedQty,ISNULL(Dtl.vcItemDesc,i.txtItemDesc) AS txtItemDesc,ISNULL(Dtl.sintOrder,0) AS sintOrder,
	Dtl.numUOMId,DTL.numQtyItemsReq
	from item i                               
	INNER JOIN ItemDetails Dtl on Dtl.numChildItemID=i.numItemCode
	INNER JOIN CTE c ON Dtl.numItemKitID = c.numItemCode 
	--WHERE i.charItemType IN ('P','I')
	where Dtl.numChildItemID!=@numItemCode
	)

	INSERT INTO [WorkOrderDetails] 
	(
		numWOId,numItemKitID,numChildItemID,numQtyItemsReq,numWareHouseItemId,vcItemDesc,sintOrder,numQtyItemsReq_Orig,numUOMId
	)
	SELECT 
		@numWOId,numItemKitID,numItemCode,numCalculatedQty,numWarehouseItmsID,txtItemDesc,sintOrder,numQtyItemsReq,numUOMId FROM CTE
	
	FETCH NEXT FROM WOCursor INTO @numItemCode, @numUnitHour,@numWarehouseItmsID,@vcItemDesc,@vcInstruction,@bintCompliationDate,@numWOAssignedTo
END

	CLOSE WOCursor;
	DEALLOCATE WOCursor;
 
END 

END  
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CalculateCommissionPaidBizDoc')
DROP PROCEDURE USP_CalculateCommissionPaidBizDoc
GO
Create PROCEDURE [dbo].[USP_CalculateCommissionPaidBizDoc]
	-- Add the parameters for the stored procedure here
	 @numComPayPeriodID AS NUMERIC(18,0),
	 @numDomainID AS NUMERIC(18,0),
	 @ClientTimeZoneOffset INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
SET NOCOUNT ON;

BEGIN TRY
BEGIN TRANSACTION
	DECLARE @dtPayStart DATE 
	DECLARE @dtPayEnd DATE

	SELECT @dtPayStart=dtStart,@dtPayEnd=dtEnd FROM CommissionPayPeriod WHERE numComPayPeriodID = @numComPayPeriodID

	--GET COMMISSION RULE OF DOMAIN
	DECLARE @TempCommissionRule TABLE
	(
		ID INT IDENTITY(1,1),
		numComRuleID NUMERIC(18,0),
		tintComBasedOn TINYINT,
		tintComType TINYINT,
		tintComAppliesTo TINYINT,
		tintComOrgType TINYINT,
		tintAssignTo TINYINT
	)

	INSERT INTO
		@TempCommissionRule
	SELECT
		CR.numComRuleID,
		CR.tintComBasedOn,
		CR.tintComType,
		CR.tintComAppliesTo,
		CR.tintComOrgType,
		CR.tintAssignTo
	FROM
		CommissionRules CR
	LEFT JOIN
		PriceBookPriorities PBP
	ON	
		CR.tintComAppliesTo = PBP.Step2Value 
		AND CR.tintComOrgType = PBP.Step3Value
	WHERE
		CR.numDomainID = @numDomainID
		AND ISNULL(PBP.[Priority],0) > 0
	ORDER BY
		ISNULL(PBP.[Priority],0) ASC

	--GET COMMISSION GLOBAL SETTINGS
	DECLARE @numDiscountServiceItemID NUMERIC(18,0)
	DECLARE @numShippingServiceItemID NUMERIC(18,0)
	DECLARE @tintCommissionType TINYINT
	DECLARE @bitIncludeTaxAndShippingInCommission BIT

	SELECT 
		@numDiscountServiceItemID=numDiscountServiceItemID, 
		@numShippingServiceItemID = numShippingServiceItemID, 
		@tintCommissionType=tintCommissionType, 
		@bitIncludeTaxAndShippingInCommission=bitIncludeTaxAndShippingInCommission 
	FROM 
		Domain 
	WHERE 
		numDomainID=@numDomainID

	--TEMPORARY TABLES TO HOLD DATA
	DECLARE @TempBizCommission TABLE
	(
		numUserCntID NUMERIC(18,0),
		numOppID NUMERIC(18,0),
		numOppBizDocID NUMERIC(18,0),
		numOppBizDocItemID NUMERIC(18,0),
		monCommission MONEY,
		numComRuleID NUMERIC(18,0),
		tintComType TINYINT,
		tintComBasedOn TINYINT,
		decCommission FLOAT
	)

	DECLARE @TABLEPAID TABLE
	(
		ID INT IDENTITY(1,1),
		numDivisionID NUMERIC(18,0),
		numRelationship NUMERIC(18,0),
		numProfile NUMERIC(18,0),
		numAssignedTo NUMERIC(18,0),
		numRecordOwner NUMERIC(18,0),
		numItemCode NUMERIC(18,0),
		numItemClassification NUMERIC(18,0),
		numOppID NUMERIC(18,0),
		numOppBizDocID NUMERIC(18,0),
		numOppBizDocItemID NUMERIC(18,0),
		numUnitHour FLOAT,
		monTotAmount MONEY,
		monVendorCost MONEY,
		monAvgCost MONEY
	)

	-- GET FULLY PAID INVOICES WITHIN PAY PERIOD
	INSERT INTO 
		@TABLEPAID
	SELECT 
		OppM.numDivisionID,
		ISNULL(CompanyInfo.numCompanyType,0),
		ISNULL(CompanyInfo.vcProfile,0),
		OppM.numAssignedTo,
		OppM.numRecOwner,
		OppBizItems.numItemCode,
		Item.numItemClassification,
		OppM.numOppID,
		OppBizItems.numOppBizDocID,
		OppBizItems.numOppBizDocItemID,
		ISNULL(OppBizItems.numUnitHour,0),
		ISNULL(OppBizItems.monTotAmount,0),
		(ISNULL(OppMItems.monVendorCost,0) * ISNULL(OppBizItems.numUnitHour,0)),
		(ISNULL(OppMItems.monAvgCost,0) * ISNULL(OppBizItems.numUnitHour,0))
	FROM 
		OpportunityBizDocs OppBiz
	INNER JOIN
		OpportunityMaster OppM
	ON
		OppBiz.numOppID = OppM.numOppID
	INNER JOIN
		DivisionMaster 
	ON
		OppM.numDivisionID = DivisionMaster.numDivisionID
	INNER JOIN
		CompanyInfo
	ON
		DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
	INNER JOIN
		OpportunityBizDocItems OppBizItems
	ON
		OppBiz.numOppBizDocsId = OppBizItems.numOppBizDocID
	INNER JOIN
		OpportunityItems OppMItems 
	ON
		OppBizItems.numOppItemID = OppMItems.numoppitemtCode
	INNER JOIN
		Item
	ON
		OppBizItems.numItemCode = Item.numItemCode
	LEFT JOIN
		BizDocComission BDC
	ON
		OppM.numOppId = BDC.numOppID 
		AND OppBiz.numOppBizDocsId = BDC.numOppBizDocId
		AND OppBizItems.numOppBizDocItemID = BDC.numOppBizDocItemID
	CROSS APPLY
	(
		SELECT 
			MAX(DM.dtDepositDate) dtDepositDate
		FROM 
			DepositMaster DM 
		JOIN 
			dbo.DepositeDetails DD
		ON 
			DM.numDepositId=DD.numDepositID 
		WHERE 
			DM.tintDepositePage=2 
			AND DM.numDomainId=@numDomainId
			AND DD.numOppID=OppBiz.numOppID 
			AND DD.numOppBizDocsID =OppBiz.numOppBizDocsID
	) TempDepositMaster
	WHERE
		OppM.numDomainId = @numDomainID 
		AND OppM.tintOppType = 1
		AND OppM.tintOppStatus = 1
		AND OppBiz.bitAuthoritativeBizDocs = 1
		AND ISNULL(OppBiz.monAmountPaid,0) >= OppBiz.monDealAmount
		AND (TempDepositMaster.dtDepositDate IS NOT NULL AND (CAST(DATEADD(MINUTE,-@ClientTimeZoneOffset,TempDepositMaster.dtDepositDate) AS DATE) BETWEEN @dtPayStart AND @dtPayEnd))
		AND OppBizItems.numItemCode NOT IN ( @numDiscountServiceItemID )  /*DO NOT INCLUDE DISCOUNT ITEM*/
		AND 1 = (
					CASE 
						WHEN OppBizItems.numItemCode = @numShippingServiceItemID 
						THEN 
							CASE 
								WHEN ISNULL(@bitIncludeTaxAndShippingInCommission,0) = 1
								THEN	
									1
								ELSE	
									0
							END
						ELSE 
							1 
					END
				)/*CONDITION TO INCLUDE/EXCLUDE INCLUDE SHIPPING ITEM */
		AND BDC.numOppBizDocItemID IS NULL --NO ENTRY AVAILABLE IN BizDocComission TABLE FOR BIZDOC ITEM


		-- LOOP ALL COMMISSION RULES
		DECLARE @i INT = 1
		DECLARE @COUNT INT = 0

		SELECT @COUNT=COUNT(*) FROM @TempCommissionRule

		DECLARE @TEMPITEM TABLE
		(
			ID INT,
			UniqueID INT,
			numUserCntID NUMERIC(18,0),
			numOppID NUMERIC(18,0),
			numOppBizDocID NUMERIC(18,0),
			numOppBizDocItemID NUMERIC(18,0),
			numItemCode NUMERIC(18,0),
			numUnitHour FLOAT,
			monTotAmount MONEY,
			monVendorCost MONEY,
			monAvgCost MONEY
		)

		WHILE @i <= @COUNT
		BEGIN
			DECLARE @numComRuleID NUMERIC(18,0)
			DECLARE @tintComBasedOn TINYINT
			DECLARE @tintComAppliesTo TINYINT
			DECLARE @tintComOrgType TINYINT
			DECLARE @tintComType TINYINT
			DECLARE @tintAssignTo TINYINT
			DECLARE @decCommission MONEY
			DECLARE @numTotalAmount AS MONEY
			DECLARE @numTotalUnit AS FLOAT
			DECLARE @RowCount AS INT

			--CLEAR PREVIOUS VALUES FROM TEMP TABLE
			DELETE FROM @TEMPITEM

			--FETCH COMMISSION RULE
			SELECT @numComRuleID=numComRuleID,@tintComBasedOn=tintComBasedOn,@tintComType=tintComType,@tintComAppliesTo=tintComAppliesTo,@tintComOrgType=tintComOrgType,@tintAssignTo=tintAssignTo FROM @TempCommissionRule WHERE ID=@i

			--FIND ALL BIZ DOC ITEMS WHERE COMMISSION IS NOT PAID AND CURRENT RULE ITEMS, ORGANIZATION AND ASSINGED TO FILTER CAN BE APPLIED
			INSERT INTO
				@TEMPITEM
			SELECT
				ROW_NUMBER() OVER (ORDER BY ID),
				ID,
				(CASE @tintAssignTo WHEN 1 THEN numAssignedTo ELSE numRecordOwner END),
				numOppID,
				numOppBizDocID,
				numOppBizDocItemID,
				numItemCode,
				numUnitHour,
				monTotAmount,
				monVendorCost,
				monAvgCost
			FROM
				@TABLEPAID
			WHERE
				1 = (CASE @tintComAppliesTo
						--SPECIFIC ITEMS
						WHEN 1 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleItems WHERE numComRuleID = @numComRuleID AND ISNULL(numValue,0) = numItemCode) > 0 THEN 1 ELSE 0 END
						-- ITEM WITH SPECIFIC CLASSIFICATIONS
						WHEN 2 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleItems WHERE numComRuleID = @numComRuleID AND ISNULL(numValue,0) = numItemClassification) > 0 THEN 1 ELSE 0 END
						-- ALL ITEMS
						ELSE 1
					END)
				AND 1 = (CASE @tintComOrgType
							-- SPECIFIC ORGANIZATION
							WHEN 1 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleOrganization WHERE numComRuleID = @numComRuleID AND ISNULL(numValue,0) = numDivisionID) > 0 THEN 1 ELSE 0 END
							-- ORGANIZATION WITH SPECIFIC RELATIONSHIP AND PROFILE
							WHEN 2 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleOrganization WHERE numComRuleID = @numComRuleID AND ISNULL(numValue,0) = numRelationship AND ISNULL(numProfile,0) = numProfile) > 0 THEN 1 ELSE 0 END
							-- ALL ORGANIZATIONS
							ELSE 1
						END)
				AND 1 = (CASE @tintAssignTo 
							-- ORDER ASSIGNED TO
							WHEN 1 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleContacts WHERE numComRuleID = @numComRuleID AND numValue=numAssignedTo) > 0 THEN 1 ELSE 0 END
							-- ORDER OWNER
							WHEN 2 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleContacts WHERE numComRuleID = @numComRuleID AND numValue=numRecordOwner) > 0 THEN 1 ELSE 0 END
							-- NO OPTION SELECTED IN RULE
							ELSE 0
						END)

			-- IF BIZDOC ITEMS ARE FOUND AFTER FILTER
			IF (SELECT COUNT(*) FROM @TEMPITEM) > 0
			BEGIN
				--CHECK COMMISSION BASED ON OPTION OF RULE
				--BASED ON AMOUNT SOLD
				IF @tintComBasedOn = 1
				BEGIN
					SELECT @numTotalAmount = SUM(monTotAmount) FROM @TEMPITEM

					IF (SELECT COUNT(*) FROM CommissionRuleDtl WHERE numComRuleID=@numComRuleID AND (@numTotalAmount BETWEEN intFrom AND intTo)) > 0
					BEGIN
						SELECT TOP 1 @decCommission=ISNULL(decCommission,0) FROM CommissionRuleDtl WHERE numComRuleID=@numComRuleID AND (@numTotalAmount BETWEEN intFrom AND intTo)
					END
				END
				--BASED ON UNITS SOLD
				ELSE IF @tintComBasedOn = 2
				BEGIN
					SELECT @numTotalUnit = SUM(numUnitHour) FROM @TEMPITEM

					IF (SELECT COUNT(*) FROM CommissionRuleDtl WHERE numComRuleID=@numComRuleID AND (@numTotalUnit BETWEEN intFrom AND intTo)) > 0
					BEGIN
						SELECT TOP 1 @decCommission=ISNULL(decCommission,0) FROM CommissionRuleDtl WHERE numComRuleID=@numComRuleID AND (@numTotalUnit BETWEEN intFrom AND intTo)
					END
				END

				--IF COMMISSION AMOUNT FOUND AFTER CHECKING COMMISSION FROM AND TO RANGE IN ABOVE STEP
				IF ISNULL(@decCommission,0) > 0
				BEGIN
					SELECT @RowCount=COUNT(*) FROM @TEMPITEM

					--CALCULATE COMMISSION AND STORE IN TEMPARORY TABLE
					INSERT INTO @TempBizCommission 
					(
						numUserCntID,
						numOppID,
						numOppBizDocID,
						numOppBizDocItemID,
						monCommission,
						numComRuleID,
						tintComType,
						tintComBasedOn,
						decCommission
					)
					SELECT
						numUserCntID,
						numOppID,
						numOppBizDocID,
						numOppBizDocItemID,
						CASE @tintComType 
							WHEN 1 --PERCENT
								THEN 
									CASE @tintCommissionType
										--TOTAL AMOUNT PAID
										WHEN 1 THEN monTotAmount * (@decCommission / 100)
										--ITEM GROSS PROFIT (VENDOR COST)
										WHEN 2 THEN (ISNULL(monTotAmount,0) - ISNULL(monVendorCost,0)) * (@decCommission / 100)
										--ITEM GROSS PROFIT (AVERAGE COST)
										WHEN 3 THEN(ISNULL(monTotAmount,0) - ISNULL(monAvgCost,0)) * (@decCommission / 100)
									END
							ELSE  --FLAT: COMMISSION AMOUNT IS EQUALLY DISTRIBUTED AMONG FOUND ENTERIES
								(@decCommission / @RowCount)
						END,
						@numComRuleID,
						@tintComType,
						@tintComBasedOn,
						@decCommission
					FROM 
						@TEMPITEM AS T1
					WHERE
						1 = (CASE @tintCommissionType 
								--TOTAL PAID INVOICE
								WHEN 1 THEN CASE WHEN (ISNULL(monTotAmount,0) > 0) THEN 1 ELSE 0 END 
								-- ITEM GROSS PROFIT (VENDOR COST)
								WHEN 2 THEN CASE WHEN ((ISNULL(monTotAmount,0) - ISNULL(monVendorCost,0)) > 0) THEN 1 ELSE 0 END 
								-- ITEM GROSS PROFIT (AVERAGE COST)
								WHEN 3 THEN CASE WHEN ((ISNULL(monTotAmount,0) - ISNULL(monAvgCost,0)) > 0) THEN 1 ELSE 0 END
								ELSE
									0 
							END) 
						AND (
								SELECT 
									COUNT(*) 
								FROM 
									@TempBizCommission 
								WHERE 
									numUserCntID=T1.numUserCntID 
									AND numOppID=T1.numOppID 
									AND numOppBizDocID = T1.numOppBizDocID 
									AND numOppBizDocItemID = T1.numOppBizDocItemID 
							) = 0
				END
			END

			SET @i = @i + 1

			SET @numComRuleID = 0
			SET @tintComBasedOn = 0
			SET @tintComAppliesTo = 0
			SET @tintComOrgType = 0
			SET @tintComType = 0
			SET @tintAssignTo = 0
			SET @decCommission = 0
			SET @numTotalAmount = 0
			SET @numTotalUnit = 0
			SET @RowCount = 0
		END

		--INSERT CALCULATED ENTERIES IN BIZDOCCOMMISSION TABLE
		INSERT INTO BizDocComission
		(	
			numDomainID,
			numUserCntID,
			numOppBizDocId,
			numComissionAmount,
			numOppBizDocItemID,
			numComRuleID,
			tintComType,
			tintComBasedOn,
			decCommission,
			numOppID,
			bitFullPaidBiz,
			numComPayPeriodID
		)
		SELECT
			@numDomainID,
			numUserCntID,
			numOppBizDocID,
			monCommission,
			numOppBizDocItemID,
			numComRuleID,
			tintComType,
			tintComBasedOn,
			decCommission,
			numOppID,
			1,
			@numComPayPeriodID
		FROM
			@TempBizCommission
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
END
GO

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CalculateCommissionUnPaidBizDoc')
DROP PROCEDURE USP_CalculateCommissionUnPaidBizDoc
GO
Create PROCEDURE [dbo].[USP_CalculateCommissionUnPaidBizDoc]
	-- Add the parameters for the stored procedure here
	 @numComPayPeriodID AS NUMERIC(18,0),
	 @numDomainID AS NUMERIC(18,0),
	 @ClientTimeZoneOffset INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
SET NOCOUNT ON;

BEGIN TRY
BEGIN TRANSACTION
	DECLARE @dtPayStart DATE 
	DECLARE @dtPayEnd DATE

	SELECT @dtPayStart=dtStart,@dtPayEnd=dtEnd FROM CommissionPayPeriod WHERE numComPayPeriodID = @numComPayPeriodID

	--GET COMMISSION RULE OF DOMAIN
	DECLARE @TempCommissionRule TABLE
	(
		ID INT IDENTITY(1,1),
		numComRuleID NUMERIC(18,0),
		tintComBasedOn TINYINT,
		tintComType TINYINT,
		tintComAppliesTo TINYINT,
		tintComOrgType TINYINT,
		tintAssignTo TINYINT
	)

	INSERT INTO
		@TempCommissionRule
	SELECT
		CR.numComRuleID,
		CR.tintComBasedOn,
		CR.tintComType,
		CR.tintComAppliesTo,
		CR.tintComOrgType,
		CR.tintAssignTo
	FROM
		CommissionRules CR
	LEFT JOIN
		PriceBookPriorities PBP
	ON	
		CR.tintComAppliesTo = PBP.Step2Value 
		AND CR.tintComOrgType = PBP.Step3Value
	WHERE
		CR.numDomainID = @numDomainID
		AND ISNULL(PBP.[Priority],0) > 0
	ORDER BY
		ISNULL(PBP.[Priority],0) ASC

	--GET COMMISSION GLOBAL SETTINGS
	DECLARE @numDiscountServiceItemID NUMERIC(18,0)
	DECLARE @numShippingServiceItemID NUMERIC(18,0)
	DECLARE @tintCommissionType TINYINT
	DECLARE @bitIncludeTaxAndShippingInCommission BIT

	SELECT 
		@numDiscountServiceItemID=numDiscountServiceItemID, 
		@numShippingServiceItemID = numShippingServiceItemID, 
		@tintCommissionType=tintCommissionType, 
		@bitIncludeTaxAndShippingInCommission=bitIncludeTaxAndShippingInCommission 
	FROM 
		Domain 
	WHERE 
		numDomainID=@numDomainID

	--TEMPORARY TABLES TO HOLD DATA
	DECLARE @TempBizCommission TABLE
	(
		numUserCntID NUMERIC(18,0),
		numOppID NUMERIC(18,0),
		numOppBizDocID NUMERIC(18,0),
		numOppBizDocItemID NUMERIC(18,0),
		monCommission MONEY,
		numComRuleID NUMERIC(18,0),
		tintComType TINYINT,
		tintComBasedOn TINYINT,
		decCommission FLOAT
	)

	DECLARE @TABLEPAID TABLE
	(
		ID INT IDENTITY(1,1),
		numDivisionID NUMERIC(18,0),
		numRelationship NUMERIC(18,0),
		numProfile NUMERIC(18,0),
		numAssignedTo NUMERIC(18,0),
		numRecordOwner NUMERIC(18,0),
		numItemCode NUMERIC(18,0),
		numItemClassification NUMERIC(18,0),
		numOppID NUMERIC(18,0),
		numOppBizDocID NUMERIC(18,0),
		numOppBizDocItemID NUMERIC(18,0),
		numUnitHour FLOAT,
		monTotAmount MONEY,
		monVendorCost MONEY,
		monAvgCost MONEY
	)

	-- GET FULLY PAID INVOICES WITHIN PAY PERIOD
	INSERT INTO 
		@TABLEPAID
	SELECT 
		OppM.numDivisionID,
		ISNULL(CompanyInfo.numCompanyType,0),
		ISNULL(CompanyInfo.vcProfile,0),
		OppM.numAssignedTo,
		OppM.numRecOwner,
		OppBizItems.numItemCode,
		Item.numItemClassification,
		OppM.numOppID,
		OppBizItems.numOppBizDocID,
		OppBizItems.numOppBizDocItemID,
		ISNULL(OppBizItems.numUnitHour,0),
		ISNULL(OppBizItems.monTotAmount,0),
		(ISNULL(OppMItems.monVendorCost,0) * ISNULL(OppBizItems.numUnitHour,0)),
		(ISNULL(OppMItems.monAvgCost,0) * ISNULL(OppBizItems.numUnitHour,0))
	FROM 
		OpportunityBizDocs OppBiz
	INNER JOIN
		OpportunityMaster OppM
	ON
		OppBiz.numOppID = OppM.numOppID
	INNER JOIN
		DivisionMaster 
	ON
		OppM.numDivisionID = DivisionMaster.numDivisionID
	INNER JOIN
		CompanyInfo
	ON
		DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
	INNER JOIN
		OpportunityBizDocItems OppBizItems
	ON
		OppBiz.numOppBizDocsId = OppBizItems.numOppBizDocID
	INNER JOIN
		OpportunityItems OppMItems 
	ON
		OppBizItems.numOppItemID = OppMItems.numoppitemtCode
	INNER JOIN
		Item
	ON
		OppBizItems.numItemCode = Item.numItemCode
	LEFT JOIN
		BizDocComission BDC
	ON
		OppM.numOppId = BDC.numOppID 
		AND OppBiz.numOppBizDocsId = BDC.numOppBizDocId
		AND OppBizItems.numOppBizDocItemID = BDC.numOppBizDocItemID
	WHERE
		OppM.numDomainId = @numDomainID 
		AND OppM.tintOppType = 1
		AND OppM.tintOppStatus = 1
		AND OppBiz.bitAuthoritativeBizDocs = 1
		AND ISNULL(OppBiz.monAmountPaid,0) < OppBiz.monDealAmount
		AND (OppBiz.dtFromDate IS NOT NULL AND (CAST(DATEADD(MINUTE,-@ClientTimeZoneOffset,OppBiz.dtFromDate) AS DATE) BETWEEN @dtPayStart AND @dtPayEnd))
		AND OppBizItems.numItemCode NOT IN ( @numDiscountServiceItemID )  /*DO NOT INCLUDE DISCOUNT ITEM*/
		AND 1 = (
					CASE 
						WHEN OppBizItems.numItemCode = @numShippingServiceItemID 
						THEN 
							CASE 
								WHEN ISNULL(@bitIncludeTaxAndShippingInCommission,0) = 1
								THEN	
									1
								ELSE	
									0
							END
						ELSE 
							1 
					END
				)/*CONDITION TO INCLUDE/EXCLUDE INCLUDE SHIPPING ITEM */
		AND BDC.numOppBizDocItemID IS NULL --NO ENTRY AVAILABLE IN BizDocComission TABLE FOR BIZDOC ITEM


		-- LOOP ALL COMMISSION RULES
		DECLARE @i INT = 1
		DECLARE @COUNT INT = 0

		SELECT @COUNT=COUNT(*) FROM @TempCommissionRule

		DECLARE @TEMPITEM TABLE
		(
			ID INT,
			UniqueID INT,
			numUserCntID NUMERIC(18,0),
			numOppID NUMERIC(18,0),
			numOppBizDocID NUMERIC(18,0),
			numOppBizDocItemID NUMERIC(18,0),
			numItemCode NUMERIC(18,0),
			numUnitHour FLOAT,
			monTotAmount MONEY,
			monVendorCost MONEY,
			monAvgCost MONEY
		)

		WHILE @i <= @COUNT
		BEGIN
			DECLARE @numComRuleID NUMERIC(18,0)
			DECLARE @tintComBasedOn TINYINT
			DECLARE @tintComAppliesTo TINYINT
			DECLARE @tintComOrgType TINYINT
			DECLARE @tintComType TINYINT
			DECLARE @tintAssignTo TINYINT
			DECLARE @decCommission MONEY
			DECLARE @numTotalAmount AS MONEY
			DECLARE @numTotalUnit AS FLOAT
			DECLARE @RowCount AS INT

			--CLEAR PREVIOUS VALUES FROM TEMP TABLE
			DELETE FROM @TEMPITEM

			--FETCH COMMISSION RULE
			SELECT @numComRuleID=numComRuleID,@tintComBasedOn=tintComBasedOn,@tintComType=tintComType,@tintComAppliesTo=tintComAppliesTo,@tintComOrgType=tintComOrgType,@tintAssignTo=tintAssignTo FROM @TempCommissionRule WHERE ID=@i

			--FIND ALL BIZ DOC ITEMS WHERE COMMISSION IS NOT PAID AND CURRENT RULE ITEMS, ORGANIZATION AND ASSINGED TO FILTER CAN BE APPLIED
			INSERT INTO
				@TEMPITEM
			SELECT
				ROW_NUMBER() OVER (ORDER BY ID),
				ID,
				(CASE @tintAssignTo WHEN 1 THEN numAssignedTo ELSE numRecordOwner END),
				numOppID,
				numOppBizDocID,
				numOppBizDocItemID,
				numItemCode,
				numUnitHour,
				monTotAmount,
				monVendorCost,
				monAvgCost
			FROM
				@TABLEPAID
			WHERE
				1 = (CASE @tintComAppliesTo
						--SPECIFIC ITEMS
						WHEN 1 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleItems WHERE numComRuleID = @numComRuleID AND ISNULL(numValue,0) = numItemCode) > 0 THEN 1 ELSE 0 END
						-- ITEM WITH SPECIFIC CLASSIFICATIONS
						WHEN 2 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleItems WHERE numComRuleID = @numComRuleID AND ISNULL(numValue,0) = numItemClassification) > 0 THEN 1 ELSE 0 END
						-- ALL ITEMS
						ELSE 1
					END)
				AND 1 = (CASE @tintComOrgType
							-- SPECIFIC ORGANIZATION
							WHEN 1 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleOrganization WHERE numComRuleID = @numComRuleID AND ISNULL(numValue,0) = numDivisionID) > 0 THEN 1 ELSE 0 END
							-- ORGANIZATION WITH SPECIFIC RELATIONSHIP AND PROFILE
							WHEN 2 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleOrganization WHERE numComRuleID = @numComRuleID AND ISNULL(numValue,0) = numRelationship AND ISNULL(numProfile,0) = numProfile) > 0 THEN 1 ELSE 0 END
							-- ALL ORGANIZATIONS
							ELSE 1
						END)
				AND 1 = (CASE @tintAssignTo 
							-- ORDER ASSIGNED TO
							WHEN 1 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleContacts WHERE numComRuleID = @numComRuleID AND numValue=numAssignedTo) > 0 THEN 1 ELSE 0 END
							-- ORDER OWNER
							WHEN 2 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleContacts WHERE numComRuleID = @numComRuleID AND numValue=numRecordOwner) > 0 THEN 1 ELSE 0 END
							-- NO OPTION SELECTED IN RULE
							ELSE 0
						END)

			-- IF BIZDOC ITEMS ARE FOUND AFTER FILTER
			IF (SELECT COUNT(*) FROM @TEMPITEM) > 0
			BEGIN
				--CHECK COMMISSION BASED ON OPTION OF RULE
				--BASED ON AMOUNT SOLD
				IF @tintComBasedOn = 1
				BEGIN
					SELECT @numTotalAmount = SUM(monTotAmount) FROM @TEMPITEM

					IF (SELECT COUNT(*) FROM CommissionRuleDtl WHERE numComRuleID=@numComRuleID AND (@numTotalAmount BETWEEN intFrom AND intTo)) > 0
					BEGIN
						SELECT TOP 1 @decCommission=ISNULL(decCommission,0) FROM CommissionRuleDtl WHERE numComRuleID=@numComRuleID AND (@numTotalAmount BETWEEN intFrom AND intTo)
					END
				END
				--BASED ON UNITS SOLD
				ELSE IF @tintComBasedOn = 2
				BEGIN
					SELECT @numTotalUnit = SUM(numUnitHour) FROM @TEMPITEM

					IF (SELECT COUNT(*) FROM CommissionRuleDtl WHERE numComRuleID=@numComRuleID AND (@numTotalUnit BETWEEN intFrom AND intTo)) > 0
					BEGIN
						SELECT TOP 1 @decCommission=ISNULL(decCommission,0) FROM CommissionRuleDtl WHERE numComRuleID=@numComRuleID AND (@numTotalUnit BETWEEN intFrom AND intTo)
					END
				END

				--IF COMMISSION AMOUNT FOUND AFTER CHECKING COMMISSION FROM AND TO RANGE IN ABOVE STEP
				IF ISNULL(@decCommission,0) > 0
				BEGIN
					SELECT @RowCount=COUNT(*) FROM @TEMPITEM

					--CALCULATE COMMISSION AND STORE IN TEMPARORY TABLE
					INSERT INTO @TempBizCommission 
					(
						numUserCntID,
						numOppID,
						numOppBizDocID,
						numOppBizDocItemID,
						monCommission,
						numComRuleID,
						tintComType,
						tintComBasedOn,
						decCommission
					)
					SELECT
						numUserCntID,
						numOppID,
						numOppBizDocID,
						numOppBizDocItemID,
						CASE @tintComType 
							WHEN 1 --PERCENT
								THEN 
									CASE @tintCommissionType
										--TOTAL AMOUNT PAID
										WHEN 1 THEN monTotAmount * (@decCommission / 100)
										--ITEM GROSS PROFIT (VENDOR COST)
										WHEN 2 THEN (ISNULL(monTotAmount,0) - ISNULL(monVendorCost,0)) * (@decCommission / 100)
										--ITEM GROSS PROFIT (AVERAGE COST)
										WHEN 3 THEN(ISNULL(monTotAmount,0) - ISNULL(monAvgCost,0)) * (@decCommission / 100)
									END
							ELSE  --FLAT: COMMISSION AMOUNT IS EQUALLY DISTRIBUTED AMONG FOUND ENTERIES
								(@decCommission / @RowCount)
						END,
						@numComRuleID,
						@tintComType,
						@tintComBasedOn,
						@decCommission
					FROM 
						@TEMPITEM AS T1
					WHERE
						1 = (CASE @tintCommissionType 
								--TOTAL PAID INVOICE
								WHEN 1 THEN CASE WHEN (ISNULL(monTotAmount,0) > 0) THEN 1 ELSE 0 END 
								-- ITEM GROSS PROFIT (VENDOR COST)
								WHEN 2 THEN CASE WHEN ((ISNULL(monTotAmount,0) - ISNULL(monVendorCost,0)) > 0) THEN 1 ELSE 0 END 
								-- ITEM GROSS PROFIT (AVERAGE COST)
								WHEN 3 THEN CASE WHEN ((ISNULL(monTotAmount,0) - ISNULL(monAvgCost,0)) > 0) THEN 1 ELSE 0 END
								ELSE
									0 
							END) 
						AND (
								SELECT 
									COUNT(*) 
								FROM 
									@TempBizCommission 
								WHERE 
									numUserCntID=T1.numUserCntID 
									AND numOppID=T1.numOppID 
									AND numOppBizDocID = T1.numOppBizDocID 
									AND numOppBizDocItemID = T1.numOppBizDocItemID 
							) = 0
				END
			END

			SET @i = @i + 1

			SET @numComRuleID = 0
			SET @tintComBasedOn = 0
			SET @tintComAppliesTo = 0
			SET @tintComOrgType = 0
			SET @tintComType = 0
			SET @tintAssignTo = 0
			SET @decCommission = 0
			SET @numTotalAmount = 0
			SET @numTotalUnit = 0
			SET @RowCount = 0
		END

		--INSERT CALCULATED ENTERIES IN BIZDOCCOMMISSION TABLE
		INSERT INTO BizDocComission
		(	
			numDomainID,
			numUserCntID,
			numOppBizDocId,
			numComissionAmount,
			numOppBizDocItemID,
			numComRuleID,
			tintComType,
			tintComBasedOn,
			decCommission,
			numOppID,
			bitFullPaidBiz,
			numComPayPeriodID
		)
		SELECT
			@numDomainID,
			numUserCntID,
			numOppBizDocID,
			monCommission,
			numOppBizDocItemID,
			numComRuleID,
			tintComType,
			tintComBasedOn,
			decCommission,
			numOppID,
			0,
			@numComPayPeriodID
		FROM
			@TempBizCommission
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
END
GO

/****** Object:  StoredProcedure [dbo].[USP_CheckCanbeDeleteOppertunity]    Script Date: 07/26/2008 16:20:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CheckCanbeDeleteOppertunity')
DROP PROCEDURE USP_CheckCanbeDeleteOppertunity
GO
CREATE PROCEDURE [dbo].[USP_CheckCanbeDeleteOppertunity]              
	@numOppId AS NUMERIC(18,0),
	@tintError AS TINYINT=0 output
AS
BEGIN             
	DECLARE @OppType AS VARCHAR(2)   
	DECLARE @itemcode AS NUMERIC     
	DECLARE @numWareHouseItemID AS NUMERIC                                 
	DECLARE @numUnits AS FLOAT                                            
	DECLARE @numQtyShipped AS FLOAT
	DECLARE @onHand AS FLOAT
	DECLARE @onOrder AS FLOAT                                                                                                                           
	DECLARE @numoppitemtCode AS NUMERIC(9)
	DECLARE @Kit AS BIT                   
	DECLARE @LotSerial AS BIT                   
	DECLARE @numQtyReceived AS FLOAT
	DECLARE @tintShipped AS TINYINT 

	SET @tintError=0          
	SELECT 
		@OppType=tintOppType,
		@tintShipped=tintShipped 
	FROM 
		OpportunityMaster 
	WHERE 
		numOppId=@numOppId
 
	IF @OppType=2
	BEGIN
		SELECT TOP 1 
			@numoppitemtCode=numoppitemtCode,
			@itemcode=OI.numItemCode,
			@numUnits=ISNULL(numUnitHour,0),
			@numQtyShipped=ISNULL(numQtyShipped,0),
			@numQtyReceived=ISNULL(numUnitHourReceived,0),
			@numWareHouseItemID=numWarehouseItmsID,
			@Kit= (CASE WHEN bitKitParent=1 AND bitAssembly=1 THEN 0 WHEN bitKitParent=1 THEN 1 ELSE 0 END),
			@LotSerial=(CASE WHEN I.bitLotNo=1 OR I.bitSerialized=1 THEN 1 ELSE 0 END) from OpportunityItems OI                                            
		JOIN 
			Item I                                            
		ON 
			OI.numItemCode=I.numItemCode 
			AND numOppId=@numOppId 
			AND (bitDropShip=0 OR bitDropShip is NULL)                                        
		WHERE 
			charitemtype='P' 
		ORDER BY 
			OI.numoppitemtCode

		WHILE @numoppitemtCode>0                               
		BEGIN  
			SELECT 
				@onHand = ISNULL(numOnHand, 0),
				@onOrder=ISNULL(numonOrder,0) 
			FROM 
				WareHouseItems 
			WHERE 
				numWareHouseItemID=@numWareHouseItemID 
  
			IF @onHand < @numQtyReceived
			BEGIN  
				IF @tintError=0 SET @tintError=1
			END  
				 
			IF @onOrder < (@numUnits-@numQtyReceived)
			BEGIN  
				if @tintError=0 set @tintError=1
			END  
	
			SELECT TOP 1 
				@numoppitemtCode=numoppitemtCode,
				@itemcode=OI.numItemCode,
				@numUnits=ISNULL(numUnitHour,0),
				@numQtyShipped=ISNULL(numQtyShipped,0),
				@numQtyReceived=ISNULL(numUnitHourReceived,0),
				@numWareHouseItemID=numWarehouseItmsID,
				@Kit= (CASE WHEN bitKitParent=1 AND bitAssembly=1 THEN 0 WHEN bitKitParent=1 THEN 1 ELSE 0 END),
				@LotSerial=(CASE WHEN I.bitLotNo=1 OR I.bitSerialized=1 THEN 1 ELSE 0 END) from OpportunityItems OI                                            
			JOIN 
				Item I                                            
			ON 
				OI.numItemCode=I.numItemCode 
				AND numOppId=@numOppId                                          
			WHERE 
				charitemtype='P' 
				AND OI.numoppitemtCode>@numoppitemtCode 
				AND (bitDropShip=0 OR bitDropShip IS NULL) 
			ORDER BY 
				OI.numoppitemtCode                                            
    
			IF @@rowcount=0 
				SET @numoppitemtCode=0    
		END  
	END
END
GO
/****** Object:  StoredProcedure [dbo].[USP_CheckCanbeReOpenOppertunity]    Script Date: 07/26/2008 16:20:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CheckCanbeReOpenOppertunity')
DROP PROCEDURE USP_CheckCanbeReOpenOppertunity
GO
CREATE PROCEDURE [dbo].[USP_CheckCanbeReOpenOppertunity]              
@intOpportunityId AS NUMERIC(18,0),
@bitCheck AS BIT=0
AS              
BEGIN         
	DECLARE @OppType AS VARCHAR(2)   
	DECLARE @Sel AS int   
	DECLARE @itemcode AS NUMERIC     
	DECLARE @numWareHouseItemID AS NUMERIC                                 
	DECLARE @numUnits AS FLOAT                                            
	DECLARE @numQtyShipped AS FLOAT
	DECLARE @onHand AS FLOAT                                                                                 
	DECLARE @numoppitemtCode AS NUMERIC(9)
	DECLARE @Kit AS BIT                   
	DECLARE @LotSerial AS BIT                   
	DECLARE @numQtyReceived AS FLOAT

	SET @Sel=0          
	SELECT 
		@OppType=tintOppType 
	FROM 
		OpportunityMaster 
	WHERE 
		numOppId=@intOpportunityId
 
	IF @OppType=2
	BEGIN
		SELECT TOP 1 
			@numoppitemtCode=numoppitemtCode,
			@itemcode=OI.numItemCode,
			@numUnits=ISNULL(numUnitHour,0),
			@numQtyShipped=ISNULL(numQtyShipped,0),
			@numQtyReceived=ISNULL(numUnitHourReceived,0),
			@numWareHouseItemID=numWarehouseItmsID,
			@Kit= (CASE WHEN bitKitParent=1 and bitAssembly=1 THEN 0 WHEN bitKitParent=1 THEN 1 ELSE 0 END),
			@LotSerial=(CASE WHEN I.bitLotNo=1 OR I.bitSerialized=1 THEN 1 ELSE 0 END) 
		FROM 
			OpportunityItems OI                                            
		JOIN 
			Item I                                            
		ON 
			OI.numItemCode=I.numItemCode 
			AND numOppId=@intOpportunityId 
			AND (bitDropShip=0 OR bitDropShip IS NULL)                                        
		WHERE 
			charitemtype='P' 
		ORDER BY 
			OI.numoppitemtCode

		WHILE @numoppitemtCode>0                               
		BEGIN  
			SELECT 
				@onHand = ISNULL(numOnHand, 0) 
			FROM
				WareHouseItems 
			WHERE 
				numWareHouseItemID=@numWareHouseItemID
   	 
			IF @onHand < @numUnits --- @numQtyReceived) 
			BEGIN  
				IF @Sel=0 SET @Sel=1
			END  
	
			SELECT TOP 1 
				@numoppitemtCode=numoppitemtCode,
				@itemcode=OI.numItemCode,
				@numUnits=ISNULL(numUnitHour,0),
				@numQtyShipped=ISNULL(numQtyShipped,0),
				@numQtyReceived=ISNULL(numUnitHourReceived,0),
				@numWareHouseItemID=numWarehouseItmsID,
				@Kit= (CASE WHEN bitKitParent=1 AND bitAssembly=1 THEN 0 WHEN bitKitParent=1 THEN 1 ELSE 0 END),
				@LotSerial=(CASE WHEN I.bitLotNo=1 OR I.bitSerialized=1 THEN 1 ELSE 0 END) from OpportunityItems OI                                            
			JOIN 
				Item I                                            
			ON 
				OI.numItemCode=I.numItemCode and numOppId=@intOpportunityId                                          
			WHERE 
				charitemtype='P' 
				AND OI.numoppitemtCode>@numoppitemtCode 
				AND (bitDropShip=0 OR bitDropShip IS NULL) 
			ORDER BY 
				OI.numoppitemtCode                                            
    
			IF @@rowcount=0 
				SET @numoppitemtCode=0    
		END  
	END      


	IF EXISTS (SELECT * FROM dbo.ReturnHeader WHERE numOppId = @intOpportunityId)         
	BEGIN
		SET @Sel = -1 * @OppType;
	END	
                                    
  
	SELECT @Sel
END
GO
/****** Object:  StoredProcedure [dbo].[usp_CompanyDivision]    Script Date: 07/26/2008 16:15:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_companydivision')
DROP PROCEDURE usp_companydivision
GO
CREATE PROCEDURE [dbo].[usp_CompanyDivision]                                         
 @numDivisionID  numeric=0,                                           
 @numCompanyID  numeric=0,                                           
 @vcDivisionName  varchar (100)='',                                                                
 @numGrpId   numeric=0,                                                                       
 @numTerID   numeric=0,                                          
 @bitPublicFlag  bit=0,                                          
 @tintCRMType  tinyint=0,                                          
 @numUserCntID  numeric=0,                                                                                                                                     
 @numDomainID  numeric=0,                                          
 @bitLeadBoxFlg  bit=0,  --Added this because when called form leadbox this value will be 1 else 0                                          
 @numStatusID  numeric=0,                                        
 @numCampaignID numeric=0,                            
 @numFollowUpStatus numeric(9)=0,                                                           
 @tintBillingTerms as tinyint,                                          
 @numBillingDays as numeric(9),                                         
 @tintInterestType as tinyint,                                          
 @fltInterest as float,                          
 @vcComPhone as varchar(50),                
 @vcComFax as varchar(50),        
 @numAssignedTo as numeric(9)=0,
 @bitNoTax as BIT,
 @bitSelectiveUpdate BIT=0,
@numCompanyDiff as numeric(9)=0,
@vcCompanyDiff as varchar(100)='',
@bitActiveInActive as bit=1,
@numCurrencyID AS numeric(9)=0,
@vcShipperAccountNo	VARCHAR(100),
@intShippingCompany INT = 0,
@numDefaultExpenseAccountID NUMERIC(18,0) = 0,
@numAccountClass NUMERIC(18,0) = 0,
@tintPriceLevel INT = 0    
AS                                                                       
BEGIN                                          
                                         
                                         
 IF @numDivisionId is null OR @numDivisionId=0                                          
 BEGIN                                                                       
  INSERT INTO DivisionMaster                      
(numCompanyID,vcDivisionName,numGrpId,numFollowUpStatus,                      
    bitPublicFlag,numCreatedBy,bintCreatedDate,                      
     numModifiedBy,bintModifiedDate,tintCRMType,numDomainID,                      
     bitLeadBoxFlg,numTerID,numStatusID,bintLeadProm,bintLeadPromBy,bintProsProm,bintProsPromBy,                      
     numRecOwner,tintBillingTerms,                      
     numBillingDays,tintInterestType,fltInterest,vcComPhone,vcComFax,numCampaignID,bitNoTax,numAssignedTo,numAssignedBy,numCompanyDiff,vcCompanyDiff,numCurrencyID,vcShippersAccountNo,intShippingCompany,numDefaultExpenseAccountID,numAccountClassID,tintPriceLevel)                     
   VALUES(@numCompanyID, @vcDivisionName, @numGrpId, 0,                                                                         
   @bitPublicFlag, @numUserCntID, getutcdate(), @numUserCntID,getutcdate(), @tintCRMType, @numDomainID, @bitLeadBoxFlg, @numTerID, @numStatusID,                                   
   NULL, NULL, NULL, NULL, @numUserCntID,0,0,0,0,@vcComPhone,@vcComFax,@numCampaignID,@bitNoTax,@numAssignedTo,@numUserCntID,@numCompanyDiff,@vcCompanyDiff,@numCurrencyID,@vcShipperAccountNo,@intShippingCompany,@numDefaultExpenseAccountID,ISNULL(@numAccountClass,0),ISNULL(@tintPriceLevel,0))                                       
                                    
  --Return the ID auto generated by the above INSERT.                                          
  SELECT @numDivisionId = @@IDENTITY
	DECLARE @numGroupID NUMERIC       
	SELECT TOP 1 @numGroupID=numGroupID FROM dbo.AuthenticationGroupMaster WHERE numDomainID=@numDomainID AND tintGroupType=2
	IF @numGroupID IS NULL SET @numGroupID = 0          
                                    
   insert into ExtarnetAccounts (numCompanyID,numDivisionID,numGroupID,numDomainID)                                  
     values(@numCompanyID,@numDivisionId,@numGroupID,@numDomainID)                                  
                              
                                         
                                       
       SELECT @numDivisionId                                    
                                     
 END                                          
 ELSE                                         
 BEGIN                                          


	IF @bitSelectiveUpdate = 0 
	BEGIN
			declare @numFollow as varchar(10)                          
			declare @binAdded as varchar(20)                          
			declare @PreFollow as varchar(10)                          
			declare @RowCount as varchar(2)                          
			set @PreFollow=(select count(*)  numFollowUpStatus from FollowUpHistory where numDivisionID= @numDivisionID and numDomainId= @numDomainID)                          
			set @RowCount=@@rowcount                          
			select   @numFollow=numFollowUpStatus,   @binAdded=bintModifiedDate from divisionmaster where numDivisionID=@numDivisionID and numDomainId= @numDomainID                    
			if @numFollow <>'0' and @numFollow <> @numFollowUpStatus                          
			begin                          
			select top 1  numFollowUpStatus from FollowUpHistory where numDivisionID= @numDivisionID and numDomainId= @numDomainID order by numFollowUpStatusID desc                          
			                    
			if @PreFollow<>0                          
			begin                          
			                   
			if @numFollow <> (select top 1  numFollowUpStatus from FollowUpHistory where numDivisionID= @numDivisionID and numDomainId= @numDomainID order by numFollowUpStatusID desc)                          
			begin                          
			                      
				  insert into FollowUpHistory(numFollowUpstatus,numDivisionID,bintAddedDate,numDomainId)                          
						 values(@numFollow,@numDivisionID,@binAdded,@numDomainID)                          
				end                          
			end                          
			else                          
			begin                          
			                     
			insert into FollowUpHistory(numFollowUpstatus,numDivisionID,bintAddedDate,numDomainId)                          
						 values(@numFollow,@numDivisionID,@binAdded,@numDomainID)                          
			end                          
			                         
			end                          
			              
			                                      
			UPDATE DivisionMaster SET  numCompanyID = @numCompanyID ,vcDivisionName = @vcDivisionName,                                      
			numGrpId = @numGrpId,                                                           
			numFollowUpStatus =@numFollowUpStatus,                                                                 
			numTerID = @numTerID,                                       
			bitPublicFlag =@bitPublicFlag,                                            
			numModifiedBy = @numUserCntID,                                            
			bintModifiedDate = getutcdate(),                                         
			tintCRMType = @tintCRMType,                                          
			numStatusID = @numStatusID,                                                                            
			tintBillingTerms = @tintBillingTerms,                                          
			numBillingDays = @numBillingDays,                                         
			tintInterestType = @tintInterestType,                                           
			fltInterest =@fltInterest,                
			vcComPhone=@vcComPhone,                
			vcComFax=@vcComFax,            
			numCampaignID=@numCampaignID,
			bitNoTax=@bitNoTax,numCompanyDiff=@numCompanyDiff,vcCompanyDiff=@vcCompanyDiff,bitActiveInActive=@bitActiveInActive,numCurrencyID=@numCurrencyID,
			vcShippersAccountNo = @vcShipperAccountNo,
			intShippingCompany = @intShippingCompany,
			numDefaultExpenseAccountID = @numDefaultExpenseAccountID,
			numAccountClassID=ISNULL(@numAccountClass,0),
			tintPriceLevel=ISNULL(@tintPriceLevel,0)
			WHERE numDivisionID = @numDivisionID   and numDomainId= @numDomainID     
			    
			    
			---Updating if organization is assigned to someone            
			declare @tempAssignedTo as numeric(9)          
			set @tempAssignedTo=null           
			select @tempAssignedTo=isnull(numAssignedTo,0) from DivisionMaster where numDivisionID = @numDivisionID and numDomainId= @numDomainID           
			print @tempAssignedTo          
			if (@tempAssignedTo<>@numAssignedTo and  @numAssignedTo<>'0')          
			begin            
			update DivisionMaster set numAssignedTo=@numAssignedTo ,numAssignedBy=@numUserCntID where numDivisionID = @numDivisionID  and numDomainId= @numDomainID           
			end           
			else if  (@numAssignedTo =0)          
			begin          
			update DivisionMaster set numAssignedTo=0 ,numAssignedBy=0 where numDivisionID = @numDivisionID  and numDomainId= @numDomainID        
			end                                            
			                      
			                                 
			SELECT @numDivisionID
 
 
 
		END                     
	END
	
	IF @bitSelectiveUpdate = 1
	BEGIN
		DECLARE @sql VARCHAR(1000)
		SET @sql = 'update DivisionMaster Set '

		IF(LEN(@vcComPhone)>0)
		BEGIN
			SET @sql =@sql + ' vcComPhone=''' + @vcComPhone + ''', '
		END
		IF(LEN(@vcComFax)>0)
		BEGIN
			SET @sql =@sql + ' vcComFax=''' + @vcComFax + ''', '
		END
		
		IF(@numUserCntID>0)
		BEGIN
			SET @sql =@sql + ' numModifiedBy='+ CONVERT(VARCHAR(10),@numUserCntID) +', '
		END
	
		SET @sql=SUBSTRING(@sql,0,LEN(@sql)) + ' '
		SET @sql =@sql + 'where numDivisionID= '+CONVERT(VARCHAR(10),@numDivisionID)

		PRINT @sql
		EXEC(@sql)
	END
	
	

end
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_createbizdocs')
DROP PROCEDURE usp_createbizdocs
GO
CREATE PROCEDURE [dbo].[USP_CreateBizDocs]                        
(                                              
 @numOppId as numeric(9)=null,                        
 @numBizDocId as numeric(9)=null,                        
 @numUserCntID as numeric(9)=null,                        
 @numOppBizDocsId as numeric(9)=NULL OUTPUT,
 @vcComments as varchar(1000),
 @bitPartialFulfillment as bit,
 @strBizDocItems as text='' ,
 @numShipVia as numeric(9),
 @vcTrackingURL as varchar(1000),
 @dtFromDate AS DATETIME,
 @numBizDocStatus as numeric(9) = 0,
 @bitRecurringBizDoc AS BIT = NULL,
 @numSequenceId AS VARCHAR(50) = '',
 @tintDeferred as tinyint=0,
 @monCreditAmount as money= 0 OUTPUT,
 @ClientTimeZoneOffset as int=0,
 @monDealAmount as money= 0 OUTPUT,
 @bitRentalBizDoc as bit=0,
 @numBizDocTempID as numeric(9)=0,
 @vcTrackingNo AS VARCHAR(500),
 @vcRefOrderNo AS VARCHAR(100),
 @OMP_SalesTaxPercent AS FLOAT = 0, --(joseph) Added to Get Online Marketplace Sales Tax Percentage
 @numFromOppBizDocsId AS NUMERIC(9)=0,
 @bitTakeSequenceId AS BIT=0,
 @bitAllItems AS BIT=0,
 @bitNotValidateBizDocFulfillment AS BIT=0,
 @fltExchangeRateBizDoc AS FLOAT=0,
 @bitRecur BIT = 0,
 @dtStartDate DATE = NULL,
 @dtEndDate DATE = NULL,
 @numFrequency SMALLINT = 0,
 @vcFrequency VARCHAR(20) = '',
 @numRecConfigID NUMERIC(18,0) = 0,
 @bitDisable bit = 0,
 @bitInvoiceForDeferred BIT = 0
)                        
AS 
BEGIN TRY
	DECLARE @hDocItem as INTEGER
	DECLARE @numDivisionID AS NUMERIC(9)
	DECLARE @numDomainID NUMERIC(9)
	DECLARE @tintOppType AS TINYINT	
	DECLARE @numFulfillmentOrderBizDocId NUMERIC(9);
	SET @numFulfillmentOrderBizDocId=296 
	    
	IF ISNULL(@fltExchangeRateBizDoc,0)=0
		SELECT @fltExchangeRateBizDoc=fltExchangeRate FROM OpportunityMaster where numOppID=@numOppId
	
	SELECT 
		@numDomainID=numDomainID,
		@numDivisionID=numDivisionID,
		@tintOppType=tintOppType 
	FROM 
		OpportunityMaster 
	WHERE 
		numOppID=@numOppId

	IF @numBizDocTempID=0
	BEGIN
		SELECT 
			@numBizDocTempID=numBizDocTempID 
		FROM 
			BizDocTemplate 
		WHERE 
			numBizDocId=@numBizDocId 
			AND numDomainID=@numDomainID and numOpptype=@tintOppType 
			AND tintTemplateType=0 
			AND isnull(bitDefault,0)=1 
			AND isnull(bitEnabled,0)=1
	END

	IF @numOppBizDocsId=0
	BEGIN
		IF (
			(@bitPartialFulfillment = 1 AND NOT EXISTS (SELECT * FROM   OpportunityBizDocs WHERE  numOppID = @numOppId AND numBizDocId = @numBizDocId AND bitPartialFulfilment = 0))
			 OR (NOT EXISTS (SELECT * FROM   OpportunityBizDocs WHERE  numOppID = @numOppId AND numBizDocId = @numBizDocId AND bitPartialFulfilment = 0)
				 AND NOT EXISTS (SELECT * FROM   OpportunityBizDocs WHERE  numOppID = @numOppId AND numBizDocId = @numBizDocId AND bitPartialFulfilment = 1))
			 OR (@bitRecurringBizDoc = 1) -- Added new flag to add unit qty in case of recurring bizdoc
			 OR (@numFromOppBizDocsId >0)
			)
		BEGIN                      
			DECLARE @tintShipped AS TINYINT	
			DECLARE @dtShipped AS DATETIME
			DECLARE @bitAuthBizdoc AS BIT
        
			SELECT 
				@tintShipped=ISNULL(tintShipped,0),
				@numDomainID=numDomainID 
			FROM 
				OpportunityMaster 
			WHERE numOppId=@numOppId                        
		
			IF @tintOppType = 1 AND @bitNotValidateBizDocFulfillment=0
				EXEC USP_ValidateBizDocFulfillment @numDomainID,@numOppId,@numFromOppBizDocsId,@numBizDocId
		

			IF @tintShipped =1 
				SET @dtShipped = GETUTCDATE();
			ELSE
				SET @dtShipped = null;
			IF @tintOppType = 1 AND (SELECT numAuthoritativeSales FROM [AuthoritativeBizDocs] WHERE numDomainID = @numDomainID) = @numBizDocId
				SET @bitAuthBizdoc = 1
			ELSE if @tintOppType = 2 AND (SELECT numAuthoritativePurchase FROM [AuthoritativeBizDocs] WHERE numDomainID = @numDomainID) = @numBizDocId
				SET @bitAuthBizdoc = 1
			ELSE 
				SET @bitAuthBizdoc = 0
		
			Declare @dtCreatedDate as datetime;SET @dtCreatedDate=Getutcdate()
			IF @tintDeferred=1
			BEGIn
				SET @dtCreatedDate=DateAdd(minute, @ClientTimeZoneOffset,@dtFromDate)
			END
	
			IF @bitTakeSequenceId=1
			BEGIN
				--Sequence #
				CREATE TABLE #tempSequence (numSequenceId bigint )
				INSERT INTO #tempSequence EXEC [dbo].[USP_GetScalerValue] @numDomainID ,33,@numBizDocId,0
				SELECT @numSequenceId =ISNULL(numSequenceId,0) FROM #tempSequence
				DROP TABLE #tempSequence
			
				--BizDoc Template ID
				CREATE TABLE #tempBizDocTempID (numBizDocTempID NUMERIC(9) )
				INSERT INTO #tempBizDocTempID EXEC [dbo].[USP_GetBizDocTemplateList] @numDomainID,@numBizDocId,1,1
				SELECT @numBizDocTempID =ISNULL(numBizDocTempID,0) FROM #tempBizDocTempID
				DROP TABLE #tempBizDocTempID
			END	
			
			
			IF @numFromOppBizDocsId>0 --If Created from Sales Fulfillment Workflow
			BEGIN
				INSERT INTO OpportunityBizDocs
					   (numOppId,numBizDocId,numCreatedBy,dtCreatedDate,numModifiedBy,dtModifiedDate,vcComments,bitPartialFulfilment,
						monShipCost,numShipVia,vcTrackingURL,dtFromDate,numBizDocStatus,[dtShippedDate],
						[bitAuthoritativeBizDocs],tintDeferred,bitRentalBizDoc,numBizDocTempID,/*vcTrackingNo,vcShippingMethod,dtDeliveryDate,*/vcRefOrderNo,numSequenceId,numBizDocStatusOLD,bitAutoCreated,[numMasterBizdocSequenceID])
				SELECT @numOppId,@numBizDocId,@numUserCntID,Getutcdate(),@numUserCntID,Getutcdate(),@vcComments,@bitPartialFulfillment,
						monShipCost,numShipVia,vcTrackingURL,dtFromDate,0,[dtShippedDate],
						@bitAuthBizdoc,tintDeferred,bitRentalBizDoc,@numBizDocTempID,/*vcTrackingNo,vcShippingMethod,dtDeliveryDate,*/vcRefOrderNo,@numSequenceId,0,1,@numSequenceId
				FROM OpportunityBizDocs OBD WHERE numOppId=@numOppId AND OBD.numOppBizDocsId=@numFromOppBizDocsId     
			END
		ELSE
		BEGIN
			INSERT INTO OpportunityBizDocs
                   (numOppId,numBizDocId,
                    --vcBizDocID,
                    numCreatedBy,
                    dtCreatedDate,
                    numModifiedBy,
                    dtModifiedDate,
                    vcComments,
                    bitPartialFulfilment,
                    --bitDiscountType,
                    --fltDiscount,
                    --monShipCost,
                    numShipVia,
                    vcTrackingURL,
                    --bitBillingTerms,
                    --intBillingDays,
                    dtFromDate,
                    --bitInterestType,
                    --fltInterest,
--                    numShipDoc,
                    numBizDocStatus,
                    --vcBizDocName,
                    [dtShippedDate],
                    [bitAuthoritativeBizDocs],tintDeferred,bitRentalBizDoc,numBizDocTempID,vcTrackingNo,
                    --vcShippingMethod,dtDeliveryDate,
                    vcRefOrderNo,numSequenceId,numBizDocStatusOLD,fltExchangeRateBizDoc,bitAutoCreated,[numMasterBizdocSequenceID])
        VALUES     (@numOppId,
                    @numBizDocId,
                    --@vcBizDocID,
                    @numUserCntID,
                    @dtCreatedDate,
                    @numUserCntID,
                    Getutcdate(),
                    @vcComments,
                    @bitPartialFulfillment,
                    --@bitDiscountType,
                    --@fltDiscount,
                    --@monShipCost,
                    @numShipVia,
                    @vcTrackingURL,
                    --@bitBillingTerms,
                    --@intBillingDays,
                    @dtFromDate,
                    --@bitInterestType,
                    --@fltInterest,
--                    @numShipDoc,
                    @numBizDocStatus,--@numBizDocStatus,--@numBizDocStatus,
                    --@vcBizDocName,
                    @dtShipped,
                    ISNULL(@bitAuthBizdoc,0),@tintDeferred,@bitRentalBizDoc,@numBizDocTempID,@vcTrackingNo,
                    --@vcShippingMethod,@dtDeliveryDate,
                    @vcRefOrderNo,@numSequenceId,0,@fltExchangeRateBizDoc,0,@numSequenceId)
        END
        
		SET @numOppBizDocsId = @@IDENTITY
		
	--Added By:Sachin Sadhu||Date:27thAug2014
	--Purpose :MAke entry in WFA queue 
IF EXISTS (SELECT 1 FROM dbo.OpportunityMaster WHERE numOppId = @numOppID AND tintOppType=1 AND isnull(tintOppStatus,0)=1 )
	BEGIN
		EXEC 
		USP_ManageOpportunityAutomationQueue			
		@numOppQueueID = 0, -- numeric(18, 0)							
		@numDomainID = @numDomainID, -- numeric(18, 0)							
		@numOppId = @numOppID, -- numeric(18, 0)							
		@numOppBizDocsId = @@IDENTITY, -- numeric(18, 0)							
		@numOrderStatus = 0, -- numeric(18, 0)					
		@numUserCntID = @numUserCntID, -- numeric(18, 0)					
		@tintProcessStatus = 1, -- tinyint						
		@tintMode = 1 -- TINYINT
   END 
   --end of script
		--Deferred BizDocs : Create Recurring entry only for create Account Journal
		if @tintDeferred=1
		BEGIN
			exec USP_RecurringAddOpportunity @numOppId,@numOppBizDocsId,0,4,@dtCreatedDate,0,@numDomainID,0,0,100,''
		END

		-- Update name template set for bizdoc
		--DECLARE @tintType TINYINT
		--SELECT @tintType = CASE @tintOppType WHEN 1 THEN 3 WHEN 2 THEN 4 END;
		EXEC dbo.USP_UpdateBizDocNameTemplate @tintOppType,@numDomainID,@numOppBizDocsId

		IF @numFromOppBizDocsId>0
		BEGIN
			insert into                       
		   OpportunityBizDocItems                                                                          
  		   (numOppBizDocID,numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,vcAttributes,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,vcNotes,/*vcTrackingNo,*/bitEmbeddedCost,monEmbeddedCost,/*monShipCost,vcShippingMethod,dtDeliveryDate,*/dtRentalStartDate,dtRentalReturnDate,tintTrackingStatus)
		   select @numOppBizDocsId,OBDI.numOppItemID,OI.numItemCode,OI.numUnitHour,OI.monPrice,OI.monTotAmount,OI.vcItemDesc,OI.numWarehouseItmsID,OBDI.vcType,
		   OBDI.vcAttributes,OBDI.bitDropShip,OBDI.bitDiscountType,OI.fltDiscount,OI.monTotAmtBefDiscount,OBDI.vcNotes,/*OBDI.vcTrackingNo,*/
		   OBDI.bitEmbeddedCost,OBDI.monEmbeddedCost,/*OBDI.monShipCost,OBDI.vcShippingMethod,OBDI.dtDeliveryDate,*/OBDI.dtRentalStartDate,OBDI.dtRentalReturnDate,OBDI.tintTrackingStatus
		   from OpportunityBizDocs OBD JOIN OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId=OBDI.numOppBizDocID 
		   JOIN OpportunityItems OI ON OBDI.numOppItemID=OI.numoppitemtCode
		   JOIN Item I ON OBDI.numItemCode=I.numItemCode
		   where  OBD.numOppId=@numOppId AND OBD.numOppBizDocsId=@numFromOppBizDocsId
		   AND OI.numoppitemtCode NOT IN(SELECT OBDI.numOppItemID FROM OpportunityBizDocs OBD JOIN dbo.OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId=OBDI.numOppBizDocId WHERE numOppId=@numOppId AND numBizDocId=@numBizDocId)
		END
		ELSE IF (@bitPartialFulfillment=1 and DATALENGTH(@strBizDocItems)>2)
		BEGIN
			IF DATALENGTH(@strBizDocItems)>2
			BEGIN
				EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strBizDocItems 

					IF @bitRentalBizDoc=1
					BEGIN
						update OI set OI.monPrice=OBZ.monPrice,OI.monTotAmount=OBZ.monTotAmount,OI.monTotAmtBefDiscount=OBZ.monTotAmount
						from OpportunityItems OI
						Join (SELECT * FROM OPENXML (@hDocItem,'/NewDataSet/BizDocItems',2)                                                                          
							WITH ( OppItemID numeric(9),Quantity FLOAT,monPrice money,monTotAmount money)) OBZ
						on OBZ.OppItemID=OI.numoppitemtCode
						where OBZ.OppItemID=OI.numoppitemtCode
					END
	
					insert into                       
				   OpportunityBizDocItems                                                                          
				   (numOppBizDocID,numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,vcAttributes,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,vcNotes,/*vcTrackingNo,vcShippingMethod,monShipCost,dtDeliveryDate,*/dtRentalStartDate,dtRentalReturnDate)
				   
				   select @numOppBizDocsId,numoppitemtCode,numItemCode,Quantity,CASE WHEN @tintOppType=2 THEN OBZ.monPrice ELSE OI.monPrice END,CASE WHEN @tintOppType=2 THEN OBZ.monPrice * Quantity ELSE (monTotAmount/numUnitHour)*Quantity END,vcItemDesc,numWarehouseItmsID,vcType,vcAttributes,bitDropShip,bitDiscountType,
				   case when bitDiscountType=0 then fltDiscount when bitDiscountType=1 then (fltDiscount/numUnitHour)*Quantity else fltDiscount end ,CASE WHEN @tintOppType=2 THEN OBZ.monPrice * Quantity ELSE (monTotAmtBefDiscount/numUnitHour)*Quantity END,Notes,/*TrackingNo,vcShippingMethod,monShipCost,dtDeliveryDate,*/dtRentalStartDate,dtRentalReturnDate  from OpportunityItems OI
				   Join (SELECT * FROM OPENXML (@hDocItem,'/NewDataSet/BizDocItems',2)                                                                          
				   WITH                       
				   (OppItemID numeric(9),                               
				    Quantity FLOAT,
					Notes varchar(500),
					monPrice MONEY,
					dtRentalStartDate datetime,dtRentalReturnDate datetime
					)) OBZ
				   on OBZ.OppItemID=OI.numoppitemtCode
				   where  numOppId=@numOppId AND ISNULL(OI.numUnitHour,0)>0 AND ISNULL(OI.numUnitHour,0)>0
				   
				EXEC sp_xml_removedocument @hDocItem 
			END
		END
		ELSE
		BEGIN
			IF @numBizDocId <> 304 AND @bitAuthBizdoc = 1 AND @bitInvoiceForDeferred = 1
			BEGIN
				DECLARE @numTop1DeferredBizDocID AS NUMERIC(18,0)

				SELECT TOP 1
					@numTop1DeferredBizDocID = 	OpportunityBizDocs.numOppBizDocsId
				FROM 
					OpportunityBizDocs 
				WHERE 
					numOppId=@numOppId 
					AND numOppBizDocsId NOT IN (SELECT ISNULL(numDeferredBizDocID,0) FROM OpportunityBizDocs WHERE numOppId=@numOppId)
					AND OpportunityBizDocs.numBizDocId = 304 -- 304 IS DEFERRED INVOICE BIZDOC
				ORDER BY
					numOppBizDocsId

				INSERT INTO OpportunityBizDocItems
  				(
					numOppBizDocID,
					numOppItemID,
					numItemCode,
					numUnitHour,
					monPrice,
					monTotAmount,
					vcItemDesc,
					numWarehouseItmsID,
					vcType,
					vcAttributes,
					bitDropShip,
					bitDiscountType,
					fltDiscount,
					monTotAmtBefDiscount
				)
				SELECT 
					@numOppBizDocsId,
					numoppitemtCode,
					OI.numItemCode,
					(CASE @bitRecurringBizDoc WHEN 1 THEN 1 ELSE ISNULL(TempBizDoc.numUnitHour,0) END) AS numUnitHour,
					OI.monPrice,
					(CASE @bitRecurringBizDoc 
						WHEN 1 THEN monPrice * 1 
						ELSE (OI.monTotAmount/OI.numUnitHour) * ISNULL(TempBizDoc.numUnitHour,0)
					END) AS monTotAmount,
					vcItemDesc,
					numWarehouseItmsID,
					vcType,vcAttributes,
					bitDropShip,
					bitDiscountType,
					(CASE 
						WHEN bitDiscountType=0 THEN fltDiscount 
						WHEN bitDiscountType=1 THEN ((fltDiscount/OI.numUnitHour) * ISNULL(TempBizDoc.numUnitHour,0))
						ELSE fltDiscount 
					END),
					(monTotAmtBefDiscount/OI.numUnitHour) * ISNULL(TempBizDoc.numUnitHour,0) 
				FROM 
					OpportunityItems OI
				JOIN 
					[dbo].[Item] AS I 
				ON 
					I.[numItemCode] = OI.[numItemCode]
				INNER JOIN
					(
						SELECT 
							OpportunityBizDocItems.numOppItemID,
							OpportunityBizDocItems.numUnitHour
						FROM 
							OpportunityBizDocs 
						JOIN 
							OpportunityBizDocItems 
						ON 
							OpportunityBizDocs.numOppBizDocsId  = OpportunityBizDocItems.numOppBizDocId
						WHERE 
							numOppId=@numOppId 
							AND OpportunityBizDocs.numOppBizDocsId = @numTop1DeferredBizDocID
					) TempBizDoc
				ON
					TempBizDoc.numOppItemID = OI.numoppitemtCode
				WHERE  
					numOppId=@numOppId 
					AND ISNULL(OI.numUnitHour,0) > 0
					AND (@bitRecurringBizDoc = 1 OR ISNULL(TempBizDoc.numUnitHour,0) > 0)


				UPDATE OpportunityBizDocs SET numDeferredBizDocID = @numTop1DeferredBizDocID WHERE numOppBizDocsId = @numOppBizDocsId
			END
			ELSE
			BEGIN
		   		INSERT INTO OpportunityBizDocItems                                                                          
  				(
					numOppBizDocID,
					numOppItemID,
					numItemCode,
					numUnitHour,
					monPrice,
					monTotAmount,
					vcItemDesc,
					numWarehouseItmsID,
					vcType,
					vcAttributes,
					bitDropShip,
					bitDiscountType,
					fltDiscount,
					monTotAmtBefDiscount
				)
				SELECT 
					@numOppBizDocsId,
					numoppitemtCode,
					OI.numItemCode,
					(CASE @bitRecurringBizDoc 
						WHEN 1 
						THEN 1 
						ELSE (OI.numUnitHour - ISNULL(TempBizDoc.numUnitHour,0)) 
					END) AS numUnitHour,
					OI.monPrice,
					(CASE @bitRecurringBizDoc 
						WHEN 1 THEN monPrice * 1 
						ELSE (OI.monTotAmount/OI.numUnitHour) * (OI.numUnitHour - ISNULL(TempBizDoc.numUnitHour,0))
					END) AS monTotAmount,
					vcItemDesc,
					numWarehouseItmsID,
					vcType,vcAttributes,
					bitDropShip,
					bitDiscountType,
					(CASE 
						WHEN bitDiscountType=0 THEN fltDiscount 
						WHEN bitDiscountType=1 THEN ((fltDiscount/OI.numUnitHour) * (OI.numUnitHour - ISNULL(TempBizDoc.numUnitHour,0)))
						ELSE fltDiscount 
					END),
					(monTotAmtBefDiscount/OI.numUnitHour) * (OI.numUnitHour - ISNULL(TempBizDoc.numUnitHour,0)) 
				FROM 
					OpportunityItems OI
				JOIN 
					[dbo].[Item] AS I 
				ON 
					I.[numItemCode] = OI.[numItemCode]
				OUTER APPLY
					(
						SELECT 
							SUM(OBDI.numUnitHour) AS numUnitHour
						FROM 
							OpportunityBizDocs OBD 
						JOIN 
							dbo.OpportunityBizDocItems OBDI 
						ON 
							OBDI.numOppBizDocId  = OBD.numOppBizDocsId
							AND OBDI.numOppItemID = OI.numoppitemtCode
						WHERE 
							numOppId=@numOppId 
							AND 1 = (CASE 
										WHEN @bitAuthBizdoc = 1 
										THEN (CASE WHEN (numBizDocId = 304 OR (numBizDocId = @numBizDocId AND ISNULL(numDeferredBizDocID,0) = 0)) THEN 1 ELSE 0 END)
										WHEN @numBizDocId = 304 --Deferred Income
										THEN (CASE WHEN (numBizDocId = @numBizDocId OR ISNULL(OBD.bitAuthoritativeBizDocs,0) = 1) THEN 1 ELSE 0 END)
										ELSE (CASE WHEN numBizDocId = @numBizDocId THEN 1 ELSE 0 END) 
									END)
					) TempBizDoc
				WHERE  
					numOppId=@numOppId 
					AND ISNULL(OI.numUnitHour,0) > 0
					AND (@bitRecurringBizDoc = 1 OR (ISNULL(OI.numUnitHour,0) - ISNULL(TempBizDoc.numUnitHour,0)) > 0)
			END
		END

		SET @numOppBizDocsID = @numOppBizDocsId
		--select @numOppBizDocsId
	end
		ELSE
		BEGIN
  			SET @numOppBizDocsId=0
			SET @monDealAmount = 0
			SET @monCreditAmount = 0
			RAISERROR ('NOT_ALLOWED',16,1);
		END
	END
	ELSE
	BEGIN
		UPDATE 
			OpportunityBizDocs
		SET    
			numModifiedBy = @numUserCntID,
			dtModifiedDate = Getdate(),
			vcComments = @vcComments,
			numShipVia = @numShipVia,
			vcTrackingURL = @vcTrackingURL,
			dtFromDate = @dtFromDate,
			numBizDocStatus = @numBizDocStatus,
			numSequenceId=@numSequenceId,
			numBizDocTempID=@numBizDocTempID,
			vcTrackingNo=@vcTrackingNo,
			vcRefOrderNo=@vcRefOrderNo,
			fltExchangeRateBizDoc=@fltExchangeRateBizDoc
		WHERE  
			numOppBizDocsId = @numOppBizDocsId

		--Added By:Sachin Sadhu||Date:27thAug2014
		--Purpose :MAke entry in WFA queue 
		IF EXISTS (SELECT 1 FROM dbo.OpportunityMaster WHERE numOppId = @numOppID AND tintOppType=1 AND isnull(tintOppStatus,0)=1 )
		BEGIN
			EXEC 
			USP_ManageOpportunityAutomationQueue			
			@numOppQueueID = 0, -- numeric(18, 0)							
			@numDomainID = @numDomainID, -- numeric(18, 0)							
			@numOppId = @numOppID, -- numeric(18, 0)							
			@numOppBizDocsId = @numOppBizDocsId, -- numeric(18, 0)							
			@numOrderStatus = 0, -- numeric(18, 0)					
			@numUserCntID = @numUserCntID, -- numeric(18, 0)					
			@tintProcessStatus = 1, -- tinyint						
			@tintMode = 1 -- TINYINT
		END 
		--end of code

   		IF DATALENGTH(@strBizDocItems)>2
		BEGIN
			EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strBizDocItems 

            DELETE FROM 
				OpportunityBizDocItems  
			WHERE 
				numOppItemID NOT IN 
				(
					SELECT 
						OppItemID 
					FROM OPENXML 
						(@hDocItem,'/NewDataSet/BizDocItems',2)                                                                          
					WITH                       
					   (OppItemID numeric(9))
				)
				AND numOppBizDocID=@numOppBizDocsId  
                  
                     
			IF @bitRentalBizDoc=1
			BEGIN
				update OI set OI.monPrice=OBZ.monPrice,OI.monTotAmount=OBZ.monTotAmount,OI.monTotAmtBefDiscount=OBZ.monTotAmount
				from OpportunityItems OI
				Join (SELECT * FROM OPENXML (@hDocItem,'/NewDataSet/BizDocItems',2)                                                                          
					WITH ( OppItemID numeric(9),Quantity FLOAT,monPrice money,monTotAmount money)) OBZ
				on OBZ.OppItemID=OI.numoppitemtCode
				where OBZ.OppItemID=OI.numoppitemtCode
			END

			update OBI set OBI.numUnitHour= Quantity,OBI.monPrice=CASE WHEN @tintOppType=2 THEN OBZ.monPrice ELSE OI.monPrice END,OBI.monTotAmount=CASE WHEN @tintOppType=2 THEN OBZ.monPrice * Quantity ELSE (OI.monTotAmount/OI.numUnitHour)*Quantity END,OBI.vcNotes= Notes,
			OBI.fltDiscount=case when OI.bitDiscountType=0 then OI.fltDiscount when OI.bitDiscountType=1 then (OI.fltDiscount/OI.numUnitHour)*Quantity else OI.fltDiscount end ,
			OBI.monTotAmtBefDiscount=(OI.monTotAmtBefDiscount/OI.numUnitHour)*Quantity ,                                                                    
			/*OBI.vcTrackingNo=TrackingNo,OBI.vcShippingMethod=OBZ.vcShippingMethod,OBI.monShipCost=OBZ.monShipCost,OBI.dtDeliveryDate=OBZ.dtDeliveryDate*/
			OBI.dtRentalStartDate=OBZ.dtRentalStartDate,OBI.dtRentalReturnDate=OBZ.dtRentalReturnDate
			from OpportunityBizDocItems OBI
			join OpportunityItems OI
			on OBI.numOppItemID=OI.numoppitemtCode
			Join (SELECT * FROM OPENXML (@hDocItem,'/NewDataSet/BizDocItems',2)                                                                          
			WITH                       
			( OppItemID numeric(9),                                     
			Quantity FLOAT,
			Notes varchar(500),
			monPrice MONEY,
			dtRentalStartDate datetime,dtRentalReturnDate datetime
			)) OBZ
			on OBZ.OppItemID=OI.numoppitemtCode
			where  OI.numOppId=@numOppId and OBI.numOppBizDocID=@numOppBizDocsId AND ISNULL(OI.numUnitHour,0)>0


            insert into                       
			OpportunityBizDocItems                                                                          
			(numOppBizDocID,numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,vcAttributes,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,vcNotes/*,vcTrackingNo,vcShippingMethod,monShipCost,dtDeliveryDate*/)                      
			select @numOppBizDocsId,numoppitemtCode,numItemCode,Quantity,CASE WHEN @tintOppType=2 THEN OBZ.monPrice ELSE OI.monPrice END,CASE WHEN @tintOppType=2 THEN OBZ.monPrice * Quantity ELSE (monTotAmount/numUnitHour)*Quantity END,vcItemDesc,numWarehouseItmsID,vcType,vcAttributes,bitDropShip,bitDiscountType,
			case when bitDiscountType=0 then fltDiscount when bitDiscountType=1 then (fltDiscount/numUnitHour)*Quantity else fltDiscount end ,(monTotAmtBefDiscount/numUnitHour)*Quantity,Notes/*,TrackingNo,vcShippingMethod,monShipCost,dtDeliveryDate*/ from OpportunityItems OI
			Join (SELECT * FROM OPENXML (@hDocItem,'/NewDataSet/BizDocItems',2)                                                                          
			WITH                       
			( OppItemID numeric(9),                                     
			Quantity FLOAT,
			Notes varchar(500),
			monPrice MONEY
			--TrackingNo varchar(500),
			--vcShippingMethod varchar(100),
			--monShipCost money,
			--dtDeliveryDate DATETIME
			)) OBZ
			on OBZ.OppItemID=OI.numoppitemtCode and OI.numoppitemtCode not in (select numOppItemID from OpportunityBizDocItems where numOppBizDocID =@numOppBizDocsId)
			where  numOppId=@numOppId AND ISNULL(OI.numUnitHour,0)>0
                 
			EXEC sp_xml_removedocument @hDocItem 
		END
	END

	IF @numOppBizDocsId>0
	BEGIN
		SELECT @monDealAmount=isnull(dbo.[GetDealAmount](@numOppId,GETUTCDATE(),@numOppBizDocsId),0)

		UPDATE 
			OpportunityBizDocs 
		SET 
			vcBizDocID=vcBizDocName + ISNULL(@numSequenceId,''),
			monDealAmount=@monDealAmount
		WHERE  
			numOppBizDocsId = @numOppBizDocsId
	END

	-- 1 if recurrence is enabled for authorative bizdoc
	IF ISNULL(@bitRecur,0) = 1
	BEGIN
		-- First Check if receurrece is already created for sales order because user should not 
		-- be able to create recurrence on both order and invoice
		IF (SELECT COUNT(*) FROM RecurrenceConfiguration WHERE numOppID = @numOppId AND numType=1) > 0 
		BEGIN
			RAISERROR ('RECURRENCE_ALREADY_CREATED_FOR_ORDER',16,1);
		END
		--User can not create recurrence on bizdoc where all items are added to bizdoc
		ELSE IF (SELECT dbo.CheckAllItemQuantityAddedToBizDoc(@numOppId,@numOppBizDocsID)) = 1
		BEGIN
			RAISERROR ('RECURRENCE_NOT_ALLOWED_ALL_ITEM_QUANTITY_ADDED_TO_INVOICE',16,1);
		END
		ELSE
		BEGIN

			EXEC USP_RecurrenceConfiguration_Insert
				@numDomainID = @numDomainId,
				@numUserCntID = @numUserCntId,
				@numOppID = @numOppID,
				@numOppBizDocID = @numOppBizDocsId,
				@dtStartDate = @dtStartDate,
				@dtEndDate =NULL,
				@numType = 2,
				@vcType = 'Invoice',
				@vcFrequency = @vcFrequency,
				@numFrequency = @numFrequency

			UPDATE OpportunityMaster SET vcRecurrenceType = 'Invoice' WHERE numOppId = @numOppID
		END
	END
	ELSE IF ISNULL(@numRecConfigID,0) > 0 AND ISNULL(@bitDisable,0)= 1
	BEGIN
		UPDATE
			RecurrenceConfiguration
		SET
			bitDisabled = 1,
			numDisabledBy = @numUserCntId,
			dtDisabledDate = GETDATE()
		WHERE	
			numRecConfigID = @numRecConfigID
	END 
 
END TRY
BEGIN CATCH
    DECLARE @ErrorMessage NVARCHAR(4000);
    DECLARE @ErrorSeverity INT;
    DECLARE @ErrorState INT;

    SELECT 
        @ErrorMessage = ERROR_MESSAGE(),
        @ErrorSeverity = ERROR_SEVERITY(),
        @ErrorState = ERROR_STATE();

    -- Use RAISERROR inside the CATCH block to return error
    -- information about the original error that caused
    -- execution to jump to the CATCH block.
    RAISERROR (@ErrorMessage, -- Message text.
               @ErrorSeverity, -- Severity.
               @ErrorState -- State.
               );
END CATCH;
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DeleteAssemblyWOAndInventory')
DROP PROCEDURE USP_DeleteAssemblyWOAndInventory
GO
CREATE PROCEDURE [dbo].[USP_DeleteAssemblyWOAndInventory]
	@numDomainID AS NUMERIC(18,0),
	@numUserCntID AS NUMERIC(18,0),
	@numWOID AS NUMERIC(18,0),
	@numOppID AS NUMERIC(18,0)
AS
BEGIN
SET XACT_ABORT ON;

BEGIN TRY 
BEGIN TRAN	
	DECLARE @numChildWOID AS NUMERIC(18,0)
	DECLARE @WorkOrder TABLE
	(
		ID INT IDENTITY(1,1),
		numWOID NUMERIC(18,0)
	)

	--GET ALL WORK ORDER CREATED FOR ORDER ASSEMBLY ITEM IN BOTTOM TO TOP ORDER
	;WITH CTE (numLevel,numWOID) AS
	(
		SELECT 
			1,
			numWOId
		FROM
			WorkOrder
		WHERE
			numParentWOID = @numWOID
			AND numWOStatus <> 23184
		UNION ALL
		SELECT 
			c.numLevel + 1,
			WorkOrder.numWOId		
		FROM
			WorkOrder
		INNER JOIN
			CTE c
		ON
			WorkOrder.numParentWOID = c.numWOID AND
			WorkOrder.numWOStatus <> 23184
	)

	INSERT INTO @WorkOrder SELECT numWOID FROM CTE ORDER BY numLevel DESC

	DECLARE @i AS INT = 1
	DECLARE @COUNT AS INT
	SELECT @COUNT=COUNT(*) FROM @WorkOrder

	--DELETE EACH WORK ORDER CREATED RECURSIVELY
	WHILE @i <= @COUNT
	BEGIN
		SELECT @numChildWOID=numWOID FROM @WorkOrder WHERE ID=@i

		EXEC USP_DeleteWorkOrder @numWOId=@numChildWOID,@numUserCntID=@numUserCntID,@numDomainID=@numDomainID,@bitOrderDelete=1

		SET @i = @i + 1 
	END

	--REVERT INVENTORY OF WORK ORDER ITEMS
	DECLARE @TEMP TABLE
	(
		ID INT IDENTITY(1,1),
		numWarehouseItemID NUMERIC(18,0),
		numQtyItemsReq FLOAT
	)

	INSERT INTO @TEMP SELECT numWareHouseItemId,numQtyItemsReq FROM WorkOrderDetails WHERE numWOId=@numWOId AND ISNULL(numWareHouseItemId,0) > 0


	SET @i = 1
	SET @COUNT = 0
	SELECT @COUNT = COUNT(*) FROM @TEMP

	DECLARE @numWareHouseItemID NUMERIC(18,0)
	DECLARE @numQtyItemsReq FLOAT
	DECLARE @Description VARCHAR(2000)
	DECLARE @onHand FLOAT
	DECLARE @onOrder FLOAT
	DECLARE @onAllocation FLOAT
	DECLARE @onBackOrder FLOAT

	WHILE @i <= @COUNT
	BEGIN
		SELECT @numWareHouseItemID=ISNULL(numWarehouseItemID,0),@numQtyItemsReq=ISNULL(numQtyItemsReq,0) FROM @TEMP WHERE ID = @i

		SET @Description = 'Items Allocation Reverted SO-WO Deleted (Qty:' + CAST(@numQtyItemsReq AS VARCHAR(20)) + ')'
		
		--GET CURRENT INEVENTORY STATUS
		SELECT
			@onHand = ISNULL(numOnHand,0),
			@onOrder = ISNULL(numOnOrder,0),
			@onAllocation = ISNULL(numAllocation,0),
			@onBackOrder = ISNULL(numBackOrder,0)
		FROM
			WareHouseItems
		WHERE
			numWareHouseItemID = @numWareHouseItemID

		--CHANGE INVENTORY
		IF @numQtyItemsReq >= @onBackOrder 		BEGIN			SET @numQtyItemsReq = @numQtyItemsReq - @onBackOrder			SET @onBackOrder = 0                            			IF (@onAllocation - @numQtyItemsReq >= 0)					SET @onAllocation = @onAllocation - @numQtyItemsReq									SET @onHand = @onHand + @numQtyItemsReq                                                          		END                                            		ELSE IF @numQtyItemsReq < @onBackOrder 		BEGIN			SET @onBackOrder = @onBackOrder - @numQtyItemsReq		END

		--UPDATE INVENTORY AND WAREHOUSE TRACKING
		EXEC USP_UpdateInventoryAndTracking 
		@numOnHand=@onHand,
		@numOnAllocation=@onAllocation,
		@numOnBackOrder=@onBackOrder,
		@numOnOrder=@onOrder,
		@numWarehouseItemID=@numWareHouseItemID,
		@numReferenceID=@numOppID,
		@tintRefType=3,
		@numDomainID=@numDomainID,
		@numUserCntID=@numUserCntID,
		@Description=@Description 

		SET @i = @i + 1
	END


	DELETE FROM WorkOrderDetails WHERE numWOId=@numWOID 
	DELETE FROM WorkOrder WHERE numWOId=@numWOID AND [numDomainID] = @numDomainID
COMMIT TRAN 
END TRY 
BEGIN CATCH		
    IF ( @@TRANCOUNT > 0 ) 
    BEGIN
        ROLLBACK TRAN
    END

	DECLARE @ErrorMessage NVARCHAR(4000);
    DECLARE @ErrorSeverity INT;
    DECLARE @ErrorState INT;

    SELECT @ErrorMessage = ERROR_MESSAGE(), @ErrorSeverity = ERROR_SEVERITY(), @ErrorState = ERROR_STATE();

    RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);
END CATCH	

SET XACT_ABORT OFF;
END
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DeleteWarehouseItem')
DROP PROCEDURE USP_DeleteWarehouseItem
GO
CREATE PROCEDURE USP_DeleteWarehouseItem
  @numWareHouseItmsDTLID NUMERIC(18,0),
  @numWareHouseItemID NUMERIC(18,0)
AS 
BEGIN
	DECLARE @Total FLOAT

	SELECT 
		@Total = numQty - ISNULL((SELECT 
									SUM(w.numQty) 
								FROM 
									OppWarehouseSerializedItem w 
								INNER JOIN 
									OpportunityMaster opp 
								ON 
									w.numOppId=opp.numOppId 
								WHERE 
									ISNULL(opp.tintOppType,0) <> 2 
									AND 1 = (CASE WHEN ISNULL(opp.bitStockTransfer,0) = 1 THEN CASE WHEN ISNULL(w.bitTransferComplete,0) = 0 THEN 1 ELSE 0 END ELSE 1 END) 
									AND w.numWarehouseItmsDTLID=WareHouseItmsDTL.numWarehouseItmsDTLID)
							,0) 
	FROM 
		WareHouseItmsDTL 
    WHERE  
		numWareHouseItmsDTLID = @numWareHouseItmsDTLID AND numWareHouseItemID=@numWareHouseItemID
     
    
    IF (SELECT numOnHand=numOnHand - @Total FROM WareHouseItems WHERE numWareHouseItemID = @numWareHouseItemID) >= 0
    BEGIN
		UPDATE 
			WareHouseItems 
		SET 
			numOnHand=numOnHand - @Total,
			dtModified = GETDATE()  
		WHERE 
			numWareHouseItemID = @numWareHouseItemID

        DELETE FROM WareHouseItmsDTL WHERE  numWareHouseItmsDTLID = @numWareHouseItmsDTLID AND numWareHouseItemID=@numWareHouseItemID
                     
        SELECT 1
    END
    ELSE 
	BEGIN
        SELECT 0 	
	END
END


GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DeleteWorkOrder')
DROP PROCEDURE USP_DeleteWorkOrder
GO
CREATE PROCEDURE [dbo].[USP_DeleteWorkOrder]
@numWOId AS NUMERIC(18,0),
@numUserCntID AS NUMERIC(18,0),
@numDomainID AS NUMERIC(18,0),
@bitOrderDelete AS BIT
AS
BEGIN
BEGIN TRY
BEGIN TRANSACTION 

	DECLARE @numWareHouseItemID AS NUMERIC(18,0),@numQtyItemsReq AS FLOAT,@numOppId AS NUMERIC(18,0),@numItemCode AS NUMERIC(18,0),@numWOStatus AS NUMERIC(18,0)
	DECLARE @onHand FLOAT
	DECLARE @onAllocation FLOAT
	DECLARE @onOrder FLOAT
	DECLARE @onBackOrder FLOAT
	DECLARE @Description VARCHAR(1000)

	DECLARE @numReferenceID AS NUMERIC(18,0)
	DECLARE @tintRefType TINYINT

	SELECT @numOppId=ISNULL(numOppId,0) FROM WorkOrder WHERE numWOId=@numWOId AND [WorkOrder].[numDomainID] = @numDomainID

	IF ISNULL(@numOppId,0) > 0
	BEGIN
		SET @numReferenceID = @numOppId
		SET @tintRefType = 3
	END
	ELSE
	BEGIN
		SET @numReferenceID = @numWOId
		SET @tintRefType = 2
	END

	--IF WORK ORDER IS CREATED FROM SALES ORDER AND WORK ORDER IS DELETED NOT WHOLE ORDER THEN EXECUTE FOLLOWING IF CODE
	IF @numOppId>0 AND ISNULL(@bitOrderDelete,0) = 0
	BEGIN
			UPDATE opp SET bitWorkOrder=0 FROM OpportunityItems opp JOIN WorkOrder wo 
			ON opp.numItemCode=wo.numItemCode AND ISNULL(wo.numOppId,0)=opp.numOppId WHERE wo.numWOId=@numWOId
			AND WO.[numDomainID] = @numDomainID
	END  


	SELECT 
		@numWareHouseItemID=numWareHouseItemID,
		@numQtyItemsReq=numQtyItemsReq,
		@numOppId=ISNULL(numOppId,0),
		@numItemCode=numItemCode,
		@numWOStatus=numWOStatus
	FROM 
		WorkOrder 
	WHERE 
		numWOId=@numWOId
		AND [WorkOrder].[numDomainID] = @numDomainID

	--IF NOT COMPLETED THEN DELETE
	IF @numWOStatus <> 23184
	BEGIN
		IF @numOppId>0
		BEGIN
			SET @Description = 'SO-WO Work Order Deleted (Qty:' + CAST(@numQtyItemsReq AS VARCHAR(10)) + ')'
		END
		ELSE
		BEGIN
			SET @Description = 'Work Order Deleted (Qty:' + CAST(@numQtyItemsReq AS VARCHAR(10)) + ')'
		END

		--GET CURRENT INEVENTORY STATUS
		SELECT
			@onHand = ISNULL(numOnHand,0),
			@onOrder = ISNULL(numOnOrder,0),
			@onAllocation = ISNULL(numAllocation,0),
			@onBackOrder = ISNULL(numBackOrder,0)
		FROM
			WareHouseItems
		WHERE
			numWareHouseItemID = @numWareHouseItemID

		--CHANGE INVENTORY
		IF @onOrder < @numQtyItemsReq
			SET @onOrder = 0
		ELSE
			SET @onOrder = @onOrder - @numQtyItemsReq

		EXEC USP_UpdateInventoryAndTracking 
		@numOnHand=@onHand,
		@numOnAllocation=@onAllocation,
		@numOnBackOrder=@onBackOrder,
		@numOnOrder=@onOrder,
		@numWarehouseItemID=@numWareHouseItemID,
		@numReferenceID=@numReferenceID,
		@tintRefType=@tintRefType,
		@numDomainID=@numDomainID,
		@numUserCntID=@numUserCntID,
		@Description=@Description 


		DECLARE @TEMP TABLE
		(
			ID INT IDENTITY(1,1),
			numWarehouseItemID NUMERIC(18,0),
			numQtyItemsReq FLOAT
		)

		INSERT INTO @TEMP SELECT numWareHouseItemId,numQtyItemsReq FROM WorkOrderDetails WHERE numWOId=@numWOId AND ISNULL(numWareHouseItemId,0) > 0

		DECLARE @i INT = 1
		DECLARE @COUNT INT

		SELECT @COUNT = COUNT(*) FROM @TEMP

		WHILE @i <= @COUNT
		BEGIN
			SELECT @numWareHouseItemID=ISNULL(numWarehouseItemID,0),@numQtyItemsReq=ISNULL(numQtyItemsReq,0) FROM @TEMP WHERE ID = @i

			IF @numOppId>0
			BEGIN
				SET @Description = 'Items Allocation Reverted SO-WO Work Order Deleted (Qty:' + CAST(@numQtyItemsReq AS VARCHAR(10)) + ')'
			END
			ELSE
			BEGIN
				SET @Description = 'Items Allocation Reverted Work Order Deleted (Qty:' + CAST(@numQtyItemsReq AS VARCHAR(10)) + ')'
			END

			--GET CURRENT INEVENTORY STATUS
			SELECT
				@onHand = ISNULL(numOnHand,0),
				@onOrder = ISNULL(numOnOrder,0),
				@onAllocation = ISNULL(numAllocation,0),
				@onBackOrder = ISNULL(numBackOrder,0)
			FROM
				WareHouseItems
			WHERE
				numWareHouseItemID = @numWareHouseItemID

			--CHANGE INVENTORY
			IF @numQtyItemsReq >= @onBackOrder 			BEGIN				SET @numQtyItemsReq = @numQtyItemsReq - @onBackOrder				SET @onBackOrder = 0                            				IF (@onAllocation - @numQtyItemsReq >= 0)						SET @onAllocation = @onAllocation - @numQtyItemsReq										SET @onHand = @onHand + @numQtyItemsReq                                                          			END                                            			ELSE IF @numQtyItemsReq < @onBackOrder 			BEGIN				SET @onBackOrder = @onBackOrder - @numQtyItemsReq			END

			--UPDATE INVENTORY AND WAREHOUSE TRACKING
			EXEC USP_UpdateInventoryAndTracking 
			@numOnHand=@onHand,
			@numOnAllocation=@onAllocation,
			@numOnBackOrder=@onBackOrder,
			@numOnOrder=@onOrder,
			@numWarehouseItemID=@numWareHouseItemID,
			@numReferenceID=@numReferenceID,
			@tintRefType=@tintRefType,
			@numDomainID=@numDomainID,
			@numUserCntID=@numUserCntID,
			@Description=@Description 

			SET @i = @i + 1
		END

		DELETE FROM WorkOrderDetails WHERE numWOId=@numWOId 

		DELETE FROM WorkOrder WHERE numWOId=@numWOId AND [numDomainID] = @numDomainID
	END
COMMIT
END TRY
BEGIN CATCH
  -- Whoops, there was an error
  IF @@TRANCOUNT > 0
     ROLLBACK

  -- Raise an error with the details of the exception
  DECLARE @ErrMsg nvarchar(4000), @ErrSeverity int
  SELECT @ErrMsg = ERROR_MESSAGE(),
         @ErrSeverity = ERROR_SEVERITY()

  RAISERROR(@ErrMsg, @ErrSeverity, 1)
END CATCH
END
GO
/****** Object:  StoredProcedure [dbo].[USP_GET_PACKAGING_RULE_AS_PER_ORDER_ITEMS]    Script Date: 30/07/2012 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GET_PACKAGING_RULE_AS_PER_ORDER_ITEMS' ) 
    DROP PROCEDURE USP_GET_PACKAGING_RULE_AS_PER_ORDER_ITEMS
GO
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/*
EXEC dbo.USP_GET_PACKAGING_RULE_AS_PER_ORDER_ITEMS
	@numOppID = 18597, -- NUMERIC(18)
	@numItemCode = 4626, -- NUMERIC(18,0)
	@numQty = 10, -- NUMERIC(18,0)
	@numPackageTypeID = 2, -- NUMERIC(18,0)
	@numDomainID = 1 -- NUMERIC(18,0)
*/
--- exec USP_GET_PACKAGING_RULE_AS_PER_ORDER_ITEMS @numShipClass=29691,@numPackageTypeID=4,@numQty=1,@numDomainID=1
--- exec USP_GET_PACKAGING_RULE_AS_PER_ORDER_ITEMS @numShipClass=29691,@numPackageTypeID=2,@numQty=15,@numDomainID=1
CREATE PROCEDURE USP_GET_PACKAGING_RULE_AS_PER_ORDER_ITEMS
(
	@numShipClass		NUMERIC(18),
	@numPackageTypeID	NUMERIC(18,0),
	@numQty				FLOAT,
	@numDomainID		NUMERIC(18,0)
)
AS 
BEGIN

--DECLARE @numShipClass AS NUMERIC(18)
--
--SELECT  @numShipClass = numShipClass FROM ITEM I
--INNER JOIN dbo.OpportunityItems OI ON OI.numItemCode = I.numItemCode
--INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OI.numOppId AND OM.numDomainId = I.numDomainID
--INNER JOIN dbo.OpportunityBizDocs OBD ON OM.numOppId = OBD.numOppId 
--INNER JOIN dbo.OpportunityBizDocItems OBDI ON OBDI.numOppBizDocID = OBD.numOppBizDocsId
--WHERE I.numItemCode = @numItemCode 
--AND OM.numOppId = @numOppID
--AND I.numDomainID = @numDomainID
--AND ISNULL(OBDI.numUnitHour,OI.numUnitHour) >= @numQty

DECLARE @strSQL AS NVARCHAR(MAX)
DECLARE @strWhere AS NVARCHAR(MAX)
DECLARE @numQtyToCheck AS NUMERIC(18,0)

SELECT @strWhere =  CASE WHEN  numFromQty < @numQty THEN ' ORDER BY numFromQty DESC'  
						 WHEN  numFromQty > @numQty THEN ' ORDER BY numFromQty ASC' 
						 WHEN  numFromQty = @numQty THEN ' ORDER BY numFromQty ASC'  
						 ELSE ' AND 1=1 ' 
					END
				FROM PackagingRules PR    
				LEFT JOIN dbo.CustomPackages CP ON CP.numCustomPackageID = PR.numPackagingTypeID 
				WHERE 1=1 
				AND PR.numDomainID = @numDomainID
				AND PR.numShipClassId = @numShipClass
				AND CP.numCustomPackageID = @numPackageTypeID 
PRINT @strWhere 				

SET @strSQL = '
				SELECT DISTINCT numPackagingRuleID,vcRuleName,numPackagingTypeID,numFromQty,numShipClassID,numDomainID,
								CP.numCustomPackageID ,CP.fltHeight,CP.fltLength,CP.fltTotalWeight,CP.fltWidth,CP.numPackageTypeID,CP.vcPackageName 
				FROM PackagingRules PR    
				LEFT JOIN dbo.CustomPackages CP ON CP.numCustomPackageID = PR.numPackagingTypeID 
				WHERE 1=1 
				AND PR.numDomainID = ' +  CONVERT(VARCHAR(10),@numDomainID) + 
				' AND PR.numShipClassId = ' + CONVERT(VARCHAR(10),@numShipClass) + 
				' AND CP.numCustomPackageID = ' + CONVERT(VARCHAR(10),@numPackageTypeID) 
				--+ ' AND ( numFromQty > ' + CONVERT(VARCHAR(10),@numQty) + ' OR numFromQty = ' + CONVERT(VARCHAR(10),@numQty) + ')'

SET @strSQL = @strSQL + ' ' + ISNULL(@strWhere,' AND 1=1 ') 
PRINT @strSQL
EXEC SP_EXECUTESQL @strSQL 

--(CASE WHEN numFromQty <= @numQty) THEN  
--ORDER BY numFromQty DESC

END
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GET_RULE_AS_PER_ORDER')
	DROP PROCEDURE USP_GET_RULE_AS_PER_ORDER
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[USP_GET_RULE_AS_PER_ORDER]
(
	@numOrderID			NUMERIC(18,0),
	@numBizDocItemID	NUMERIC(18,0),
	@numShipClass		NUMERIC(18,0),
	@numDomainID		NUMERIC(18,0),
	@numQty				FLOAT
)
AS 
BEGIN

--SET @OrderIDs = '26171,26172,26173'
--SET @numDomainID = 72
--SET @intShipBasedOn = 1
--SET @tintType = 1

-- WANT TO FETCH DATA
DECLARE @numPriceValue As MONEY
DECLARE @numWeightValue As NUMERIC(18,2)
DECLARE @numQtyValue As NUMERIC(18,0)
DECLARE @numCountryID AS NUMERIC(18)
DECLARE @numStateID AS NUMERIC(18)
DECLARE @Zip AS VARCHAR(50)
DECLARE @numZip AS NUMERIC(10)
DECLARE @numItemID AS NUMERIC(10)
DECLARE @numSourceCompanyID AS NUMERIC(10)
--DECLARE @numShipClass AS NUMERIC(10)

SET @numSourceCompanyID = 0
SELECT @numSourceCompanyID = ISNULL(numWebApiId,0) FROM dbo.OpportunityLinking
WHERE numChildOppID = @numOrderID

SELECT  @numPriceValue = ISNULL(OBDI.monPrice,ISNULL(OI.monPrice,0)), 
		@numWeightValue = ISNULL(I.fltWeight,0),
		@numQtyValue = @numQty,
		@Zip = AD.vcPostalCode ,
        @numCountryID = CASE WHEN ISNULL(OM.tintShipToType,0) IN (0,1) THEN AD.numCountry ELSE OppAD.numShipCountry END, 
        @numStateID = CASE WHEN ISNULL(OM.tintShipToType,0) IN (0,1) THEN AD.numState ELSE OppAD.numShipState END,
        @numItemID = OBDI.numItemCode
         --,@numShipClass=I.numShipClass
      FROM OpportunityMaster OM 
      INNER JOIN OpportunityItems OI ON OM.numOppId = OI.numOppId
      LEFT JOIN dbo.OpportunityBizDocs OBD ON OBD.numOppId = OM.numOppId
      LEFT JOIN dbo.OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId = OBDI.numOppBizDocID  AND OI.numoppitemtCode = OBDI.numOppItemID
      INNER JOIN Item I ON I.numItemCode = OI.numItemCode AND I.numDomainID = OM.numDomainID
      LEFT JOIN AddressDetails AD ON AD.numDomainID = OM.numDomainId AND AD.numRecordID = OM.numDivisionId AND AD.tintAddressOf = 2 AND AD.tintAddressType = 2 AND AD.bitIsPrimary=1
      LEFT JOIN OpportunityAddress OppAD ON OppAD.numOppID = OM.numOppID 
      WHERE 1=1
      AND OM.numOppId = @numOrderID
      AND (OBDI.numOppBizDocItemID = @numBizDocItemID OR ISNULL(@numBizDocItemID,0) = 0)
      AND I.numDomainID = @numDomainID

PRINT @numWeightValue             
PRINT 'PRICE : ' + CONVERT(VARCHAR(100),@numPriceValue)
PRINT 'Weight : ' + CONVERT(VARCHAR(100),@numWeightValue)
PRINT 'Quantity : ' + CONVERT(VARCHAR(100),@numQtyValue)
PRINT 'numCountryID : ' + CONVERT(VARCHAR(100),@numCountryID)
PRINT 'numStateID : ' + CONVERT(VARCHAR(100),@numStateID)
PRINT 'numItemCode : ' + CONVERT(VARCHAR(100),@numItemID)
PRINT '@numSourceCompanyID : ' + CONVERT(VARCHAR(100),@numSourceCompanyID)

PRINT @Zip
IF ISNUMERIC(@Zip) = 1 
BEGIN
	SET @numZip = CONVERT(NUMERIC(10),@Zip)
END
ELSE
BEGIN
	SET @numZip = 0
END
PRINT @numZip

      
SELECT DISTINCT @numZip,(CASE WHEN ISNUMERIC(SRSL.vcFromZip) = 1 THEN CONVERT(NUMERIC(18),SRSL.vcFromZip) ELSE 0 END),(CASE WHEN ISNUMERIC(SRSL.vcToZip) = 1 THEN CONVERT(NUMERIC(18),SRSL.vcToZip) ELSE 0 END),SR.*,SRC.*,
				ISNULL(SST1.vcServiceName,'') AS [Domestic],
				ISNULL(SST2.vcServiceName,'') AS [International],
				SST1.numShippingCompanyID AS [numShippingCompanyID1],
				SST2.numShippingCompanyID AS [numShippingCompanyID2]
FROM dbo.ShippingLabelRuleMaster SR 
INNER JOIN dbo.ShippingLabelRuleChild SRC ON SR.numShippingRuleID = SRC.numShippingRuleID                                                     
LEFT JOIN dbo.ShippingRuleStateList SRSL ON SR.numShippingRuleID = SRSL.numRuleID AND SR.numDomainID = SRSL.numDomainID
LEFT JOIN dbo.PromotionOfferItems POI ON SR.numShippingRuleID = POI.numProId
INNER JOIN ShippingServiceTypes SST1 ON SST1.intNsoftEnum = SRC.numDomesticShipID
INNER JOIN ShippingServiceTypes SST2 ON SST2.intNsoftEnum = SRC.numInternationalShipID
WHERE 1=1
AND SR.numDomainID = @numDomainID
AND  
(
	-- Check MarketPlace is Available or not for shipping
	(
		(SR.numSourceCompanyID = @numSourceCompanyID OR ISNULL(SR.numSourceCompanyID,0) = 0 OR ISNULL(@numSourceCompanyID,0) = 0)
	)
	AND	
	--Items Affected
	(
		(intItemAffected = 1 AND POI.numValue = @numItemID ) -- Priority 1
		OR 
		(intItemAffected = 2 AND POI.numValue = @numShipClass ) -- Priority 2
		OR 
		(intItemAffected = 3) -- Priority 3
	)
	
	--Shipping Based On
    AND 
    (
    1 = (CASE WHEN SR.tintShipBasedOn = 1 THEN CASE WHEN @numWeightValue BETWEEN SRC.numFromValue AND SRC.numToValue THEN 1 ELSE 0 END 
    	 	  WHEN SR.tintShipBasedOn = 2 THEN CASE WHEN @numPriceValue BETWEEN SRC.numFromValue AND SRC.numToValue THEN 1 ELSE 0 END 
    	 	  WHEN SR.tintShipBasedOn = 3 THEN CASE WHEN @numQtyValue BETWEEN SRC.numFromValue AND SRC.numToValue THEN 1 ELSE 0 END 
				 ELSE 0
            END)
    )
    
  AND
    (
	1 = (CASE WHEN SR.tintType = 1 AND SRSL.numRuleStateID IS NOT NULL THEN 
								CASE WHEN SR.numShippingRuleID  IN 
									  ( SELECT TOP 1 numRuleID FROM dbo.ShippingRuleStateList SRSL WHERE numDomainID = @numDomainID AND SRSL.numRuleID=SR.numShippingRuleID
										AND (SRSL.numCountryID = @numCountryID OR @numCountryID = 0)
										AND (SRSL.numStateID = @numStateID OR @numStateID = 0 )
										AND 
											(
											(
											 @numZip >= (CASE WHEN ISNUMERIC(SRSL.vcFromZip) = 1 THEN CONVERT(NUMERIC(18),SRSL.vcFromZip) ELSE 0 END)             
											 AND 
											 @numZip <= (CASE WHEN ISNUMERIC(SRSL.vcToZip) = 1 THEN CONVERT(NUMERIC(18),SRSL.vcToZip) ELSE 0 END) ) 
											 OR 
											 ISNULL(@numZip,0) = 0
											 )
										ORDER BY (CASE WHEN LEN(ISNULL(SRSL.vcToZip,0))>=5 THEN 1 WHEN ISNULL(SRSL.numStateID,0) > 0 THEN 2 WHEN ISNULL(SRSL.numCountryID,0) > 0 THEN 3 END )
										) THEN 1 ELSE 0 END
			  WHEN SR.tintType = 2 AND SRSL.numRuleStateID IS NOT NULL  THEN 
								CASE WHEN SR.numShippingRuleID not IN 
									  ( SELECT TOP 1 numRuleID FROM dbo.ShippingRuleStateList SRSL WHERE numDomainID = @numDomainID AND SRSL.numRuleID=SR.numShippingRuleID
										AND (SRSL.numCountryID = @numCountryID OR @numCountryID = 0)
										AND (SRSL.numStateID = @numStateID OR @numStateID = 0 )
										AND 
										(
											(
											 @numZip >= (CASE WHEN ISNUMERIC(SRSL.vcFromZip) = 1 THEN CONVERT(NUMERIC(18),SRSL.vcFromZip) ELSE 0 END)             
											 AND 
											 @numZip <= (CASE WHEN ISNUMERIC(SRSL.vcToZip) = 1 THEN CONVERT(NUMERIC(18),SRSL.vcToZip) ELSE 0 END) ) 
											 OR 
											  ISNULL(@numZip,0) = 0
											 )
										ORDER BY (CASE WHEN LEN(ISNULL(SRSL.vcToZip,0))>=5 THEN 1 
													   WHEN ISNULL(SRSL.numStateID,0) > 0 THEN 2 WHEN ISNULL(SRSL.numCountryID,0) > 0 THEN 3 END )
										) THEN 1 ELSE 0 END
			  ELSE 1
		END																	
    )       
)
) 
ORDER BY SR.numSourceCompanyID DESC ,intItemAffected,SR.numShippingRuleID 

END 
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetAutoAssign_OppSerializedItem')
DROP PROCEDURE USP_GetAutoAssign_OppSerializedItem
GO
CREATE PROCEDURE [dbo].[USP_GetAutoAssign_OppSerializedItem]  
    @numOppID as numeric(9)=0,  
    @numOppBizDocsId as numeric(9)=0,
    @numWarehouseItmsID as numeric(9)=0,
	@bitSerialized as bit,
    @bitLotNo as bit,
	@numUnitHour as FLOAT
as  

Declare @vcSerialNoList varchar(2000);SET @vcSerialNoList=''

Create table #tempWareHouseItmsDTL (                                                                    
ID numeric(18) IDENTITY(1,1) NOT NULL,vcSerialNo VARCHAR(100),numQty [numeric](18, 0),numWareHouseItmsDTLID [numeric](18, 0)                                             
 )   

INSERT INTO #tempWareHouseItmsDTL 
(
	vcSerialNo,numWarehouseItmsDTLID,numQty
)  
SELECT 
	vcSerialNo,numWareHouseItmsDTLID,ISNULL(numQty,0) as TotalQty
FROM  
	WareHouseItmsDTL   
WHERE 
	ISNULL(numQty,0) > 0
	AND numWareHouseItemID=@numWarehouseItmsID  
    AND numWareHouseItmsDTLID NOT IN (SELECT numWarehouseItmsDTLID FROM OppWarehouseSerializedItem WHERE numOppID=@numOppID AND numWarehouseItmsID=@numWarehouseItmsID AND numOppBizDocsId=@numOppBizDocsId) 
ORDER BY TotalQty desc

IF ((Select sum(numQty) from #tempWareHouseItmsDTL)>=@numUnitHour)
BEGIN
DECLARE  @maxWareID NUMERIC(9),@minWareID NUMERIC(9),@vcSerialNo varchar(100)
DECLARE  @numWarehouseItmsDTLID NUMERIC(9),@numQty NUMERIC(9),@numUseQty NUMERIC(9)
	
SELECT @maxWareID = max(ID),@minWareID = min(ID) FROM  #tempWareHouseItmsDTL 

WHILE (@numUnitHour>0 and @minWareID <= @maxWareID)
BEGIN
	SELECT @vcSerialNo =vcSerialNo,@numWarehouseItmsDTLID = numWarehouseItmsDTLID,@numQty = numQty FROM  #tempWareHouseItmsDTL  where ID=@minWareID

	IF @numUnitHour >= @numQty
		BEGIN
			SET @numUseQty=@numQty
	
			SET @numUnitHour=@numUnitHour-@numQty
		END
	else IF @numUnitHour < @numQty
		BEGIN
			SET @numUseQty=@numUnitHour

			SET @numUnitHour=0
		END
		
		IF @bitSerialized=1
			SET @vcSerialNoList = @vcSerialNoList + @vcSerialNo + ','
		ELSE IF @bitLotNo=1
			SET @vcSerialNoList = @vcSerialNoList + @vcSerialNo + '(' + Cast(@numUseQty as varchar(10)) + '),'

SET @minWareID=@minWareID + 1
END

END

drop table #tempWareHouseItmsDTL

select substring(@vcSerialNoList,0,LEN(@vcSerialNoList)) as vcSerialNoList
GO
/****** Object:  StoredProcedure [dbo].[USP_GetChildItemsForKitsAss]    Script Date: 07/26/2008 16:16:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetChildItemsForKitsAss')
DROP PROCEDURE USP_GetChildItemsForKitsAss
GO
CREATE PROCEDURE [dbo].[USP_GetChildItemsForKitsAss]                             
@numKitId as numeric(9)                            
as                            


WITH CTE(ID,numItemDetailID,numParentID,numItemKitID,numItemCode,vcItemName,monAverageCost,numAssetChartAcntId,numWarehouseItmsID,txtItemDesc,numQtyItemsReq,numOppChildItemID,
charItemType,ItemType,StageLevel,monListPrice,numBaseUnit,numCalculatedQty,numIDUOMId,sintOrder,numRowNumber,RStageLevel,numUOMQuantity)
AS
(
select CAST(CONCAT('#',ISNULL(Dtl.numItemDetailID,'0'),'#') AS VARCHAR(1000)),Dtl.numItemDetailID,CAST(0 AS NUMERIC(18,0)),convert(NUMERIC(18,0),0),numItemCode,vcItemName,monAverageCost,numAssetChartAcntId,isnull(Dtl.numWareHouseItemId,0) as numWarehouseItmsID,ISNULL(Dtl.vcItemDesc,txtItemDesc) AS txtItemDesc,DTL.numQtyItemsReq,0 as numOppChildItemID,
case when charItemType='P' then 'Inventory Item' when charItemType='S' then 'Service' when charItemType='A' then 'Accessory' when charItemType='N' then 'Non-Inventory Item' end as charItemType,charItemType as ItemType                            
,1,(CASE WHEN charItemType='P' THEN (SELECT monWListPrice FROM WarehouseItems WHERE numWareHouseItemID= Dtl.numWareHouseItemId) ELSE Item.monListPrice END) monListPrice,ISNULL(numBaseUnit,0),CAST(DTL.numQtyItemsReq AS NUMERIC(9,0)) AS numCalculatedQty,ISNULL(Dtl.numUOMId,ISNULL(numBaseUnit,0)) AS numIDUOMId,ISNULL(sintOrder,0) sintOrder,ROW_NUMBER() OVER (ORDER BY ISNULL(sintOrder,0)) AS numRowNumber,0,
(DTL.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(DTL.numUOMID,item.numItemCode,item.numDomainID,item.numBaseUnit),1))
from item                                
INNER join ItemDetails Dtl on numChildItemID=numItemCode
where  numItemKitID=@numKitId 

UNION ALL

select CAST(CONCAT(c.ID,'-#',ISNULL(Dtl.numItemDetailID,'0'),'#') AS VARCHAR(1000)),Dtl.numItemDetailID,C.numItemDetailID,Dtl.numItemKitID,i.numItemCode,i.vcItemName,i.monAverageCost,i.numAssetChartAcntId,isnull(Dtl.numWareHouseItemId,0) as numWarehouseItmsID,ISNULL(Dtl.vcItemDesc,i.txtItemDesc) AS txtItemDesc,DTL.numQtyItemsReq,0 as numOppChildItemID,
case when i.charItemType='P' then 'Inventory Item' when i.charItemType='S' then 'Service' when i.charItemType='A' then 'Accessory' when i.charItemType='N' then 'Non-Inventory Item' end as charItemType,i.charItemType as ItemType                            
,c.StageLevel + 1,(CASE WHEN i.charItemType='P' THEN (SELECT monWListPrice FROM WarehouseItems WHERE numWareHouseItemID= dtl.numWareHouseItemId) ELSE i.monListPrice END) monListPrice,ISNULL(i.numBaseUnit,0),CAST((DTL.numQtyItemsReq * c.numCalculatedQty) AS NUMERIC(9,0)) AS numCalculatedQty,ISNULL(Dtl.numUOMId,ISNULL(i.numBaseUnit,0)) AS numIDUOMId,ISNULL(Dtl.sintOrder,0) sintOrder,
ROW_NUMBER() OVER (ORDER BY ISNULL(Dtl.sintOrder,0)) AS numRowNumber,0,
(DTL.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(DTL.numUOMID,i.numItemCode,i.numDomainID,i.numBaseUnit),1))
from item i                               
INNER JOIN ItemDetails Dtl on Dtl.numChildItemID=i.numItemCode
INNER JOIN CTE c ON Dtl.numItemKitID = c.numItemCode 
where Dtl.numChildItemID!=@numKitId
)

SELECT * INTO #temp FROM CTE

;WITH Final AS (SELECT *,1 AS RStageLevel1 FROM #temp WHERE numitemcode NOT IN (SELECT numItemKitID FROM #temp)

UNION ALL

SELECT t.*,c.RStageLevel1 + 1 AS RStageLevel1 FROM  #temp t JOIN Final c ON t.numitemcode=c.numItemKitID
)

--SELECT DISTINCT * FROM Total ORDER BY StageLevel

UPDATE t set t.RStageLevel=f.RStageLevel from 
#temp t,(SELECT numitemcode,numItemKitID,MAX(RStageLevel1) AS RStageLevel FROM Final GROUP BY numitemcode,numItemKitID) f
WHERE t.numitemcode=f.numitemcode AND t.numItemKitID=f.numItemKitID 

--SELECT * FROM #temp

SELECT DISTINCT c.*,CASE WHEN isnull(c.numWarehouseItmsID,0)>0 THEN ISNULL(WI.[monWListPrice],0) ELSE c.monListPrice END monListPrice, 
convert(varchar(30),CASE WHEN isnull(c.numWarehouseItmsID,0)>0 THEN ISNULL(WI.[monWListPrice],0) ELSE c.monListPrice END)+'/'+isnull(UOM.vcUnitName,'Units') as UnitPrice,UOM.vcUnitName,ISNULL(UOM.numUOMId,0) numUOMId,
WI.numItemID,vcWareHouse,IDUOM.vcUnitName AS vcIDUnitName,
dbo.fn_UOMConversion(IDUOM.numUOMId,0,IDUOM.numDomainId,UOM.numUOMId) AS UOMConversionFactor,
c.numQtyItemsReq * dbo.fn_UOMConversion(UOM.numUOMId,0,IDUOM.numDomainId,IDUOM.numUOMId) AS numConQty,
(c.numQtyItemsReq * dbo.fn_UOMConversion(UOM.numUOMId,0,IDUOM.numDomainId,IDUOM.numUOMId)) * c.monAverageCost AS monAverageCostTotalForQtyRequired,
c.sintOrder,
ISNULL(WI.[numWareHouseItemID],0) AS numWareHouseItemID,ISNULL(numOnHand,0) numOnHand,isnull(numOnOrder,0) numOnOrder,isnull(numReorder,0) numReorder,isnull(numAllocation,0) numAllocation,isnull(numBackOrder,0) numBackOrder,c.numUOMQuantity
FROM #temp c  
LEFT JOIN UOM ON UOM.numUOMId=c.numBaseUnit
LEFT JOIN UOM IDUOM ON IDUOM.numUOMId=c.numIDUOMId
LEFT OUTER JOIN [WareHouseItems] WI ON WI.[numItemID] = c.numItemCode AND WI.[numWareHouseItemID] = c.numWarehouseItmsID
LEFT OUTER JOIN Warehouses W ON W.numWareHouseID=WI.numWareHouseID ORDER BY c.StageLevel

--SELECT c.*,CASE WHEN isnull(c.numWarehouseItmsID,0)>0 THEN ISNULL(WI.[monWListPrice],0) ELSE c.monListPrice END monListPrice, 
--convert(varchar(30),CASE WHEN isnull(c.numWarehouseItmsID,0)>0 THEN ISNULL(WI.[monWListPrice],0) ELSE c.monListPrice END)+'/'+isnull(UOM.vcUnitName,'Units') as UnitPrice,
--WI.numItemID,vcWareHouse
-- FROM CTE c  
--LEFT JOIN UOM ON UOM.numUOMId=c.numBaseUnit
--LEFT OUTER JOIN [WareHouseItems] WI ON WI.[numItemID] = c.numItemCode AND WI.[numWareHouseItemID] = c.numWarehouseItmsID
--LEFT OUTER JOIN Warehouses W ON W.numWareHouseID=WI.numWareHouseID



GO
/****** Object:  StoredProcedure [dbo].[usp_GetCompanyInfo]    Script Date: 07/26/2008 16:16:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--modified by anoop jayaraj                                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcompanyinfo')
DROP PROCEDURE usp_getcompanyinfo
GO
CREATE PROCEDURE [dbo].[usp_GetCompanyInfo]                                                                                                   
 @numDivisionID numeric,        
 @numDomainID as numeric(9)   ,        
@ClientTimeZoneOffset  int           
    --                                                         
AS                                                          
BEGIN                                                          
 SELECT CMP.numCompanyID, CMP.vcCompanyName,DM.numStatusID as numCompanyStatus,                                                          
  CMP.numCompanyRating, CMP.numCompanyType, DM.bitPublicFlag, DM.numDivisionID,                                                           
  DM. vcDivisionName, 
  
  isnull(AD1.vcStreet,'') as vcBillStreet, 
  isnull(AD1.vcCity,'') as vcBillCity, 
  isnull(dbo.fn_GetState(AD1.numState),'') as vcBilState,
  isnull(AD1.vcPostalCode,'') as vcBillPostCode,
  isnull(dbo.fn_GetListName(AD1.numCountry,0),'')  as vcBillCountry,
  isnull(AD2.vcStreet,'') as vcShipStreet,
  isnull(AD2.vcCity,'') as vcShipCity,
  isnull(dbo.fn_GetState(AD2.numState),'')  as vcShipState,
  isnull(AD2.vcPostalCode,'') as vcShipPostCode, 
  AD2.numCountry vcShipCountry,
  numFollowUpStatus,
  CMP.vcWebLabel1, CMP.vcWebLink1, CMP.vcWebLabel2, CMP.vcWebLink2,                                                           
  CMP.vcWebLabel3, CMP.vcWebLink3, CMP.vcWeblabel4, CMP.vcWebLink4,  
   tintBillingTerms,numBillingDays,
   --ISNULL(dbo.fn_GetListItemName(isnull(numBillingDays,0)),0) AS numBillingDaysName,
   (SELECT numNetDueInDays FROM dbo.BillingTerms WHERE numTermsID = numBillingDays AND numDomainID = DM.numDomainID) AS numBillingDaysName,
   tintInterestType,fltInterest,                                                          
   DM.bitPublicFlag, DM.numTerID,                                                           
  CMP.numCompanyIndustry, CMP.numCompanyCredit,                                                           
  isnull(CMP.txtComments,'') as txtComments, isnull(CMP.vcWebSite,'') vcWebSite, CMP.numAnnualRevID, CMP.numNoOfEmployeesID, dbo.fn_GetListItemName(CMP.numNoOfEmployeesID) as NoofEmp,                                                         
  CMP.vcProfile, DM.numCreatedBy, dbo.fn_GetContactName(DM.numRecOwner) as RecOwner,       
dbo.fn_GetContactName(DM.numCreatedBy)+' ' +convert(varchar(20),dateadd(minute,-@ClientTimeZoneOffset,DM.bintCreatedDate)) as vcCreatedBy,                                                  
  DM.numRecOwner,    
 dbo.fn_GetContactName(DM.numModifiedBy)+' ' +convert(varchar(20),dateadd(minute,-@ClientTimeZoneOffset,DM.bintModifiedDate)) as vcModifiedBy,vcComPhone,vcComFax,                                                 
  DM.numGrpID, CMP.vcHow,                                                          
  DM.tintCRMType,isnull(DM.bitNoTax,0) bitNoTax ,                                                      
   numCampaignID,numAssignedBy,numAssignedTo,            
(select  count(*) from dbo.GenericDocuments where numRecID= @numDivisionID and  vcDocumentSection='A' AND tintDocumentType=2) as DocumentCount ,          
(SELECT count(*)from CompanyAssociations where numDivisionID=@numDivisionID and bitDeleted=0 ) as AssociateCountFrom,          
(SELECT count(*)from CompanyAssociations where numAssociateFromDivisionID=@numDivisionID and bitDeleted=0 ) as AssociateCountTo,
 ISNULL(DM.numCurrencyID,0) AS numCurrencyID,ISNULL(DM.numDefaultPaymentMethod,0) AS numDefaultPaymentMethod,ISNULL(DM.numDefaultCreditCard,0) AS numDefaultCreditCard,ISNULL(DM.bitOnCreditHold,0) AS bitOnCreditHold,
 ISNULL(vcShippersAccountNo,'') AS [vcShippersAccountNo],
 ISNULL(intShippingCompany,0) AS [intShippingCompany],ISNULL(DM.bitEmailToCase,0) AS bitEmailToCase,
 ISNULL(numDefaultExpenseAccountID,0) AS [numDefaultExpenseAccountID],
 ISNULL(numDefaultShippingServiceID,0) AS [numDefaultShippingServiceID],
 ISNULL(numAccountClassID,0) As numAccountClassID,
 ISNULL(tintPriceLevel,0) AS tintPriceLevel
 FROM  CompanyInfo CMP                                                          
 join DivisionMaster DM    
	on DM.numCompanyID=CMP.numCompanyID
 LEFT JOIN dbo.AddressDetails AD1 ON AD1.numDomainID=DM.numDomainID 
   AND AD1.numRecordID=DM.numDivisionID AND AD1.tintAddressOf=2 AND AD1.tintAddressType=1 AND AD1.bitIsPrimary=1
   LEFT JOIN dbo.AddressDetails AD2 ON AD1.numDomainID=DM.numDomainID 
   AND AD2.numRecordID= DM.numDivisionID AND AD2.tintAddressOf=2 AND AD2.tintAddressType=2 AND AD2.bitIsPrimary=1

where                                            
   DM.numDivisionID=@numDivisionID   and DM.numDomainID=@numDomainID                                                                        
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetCompanyInfoDtlPl]    Script Date: 07/26/2008 16:16:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--usp_GetCompanyInfoDtlPl 91099,72,-333
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcompanyinfodtlpl')
DROP PROCEDURE usp_getcompanyinfodtlpl
GO
CREATE PROCEDURE [dbo].[usp_GetCompanyInfoDtlPl]                                                                                                                   
@numDivisionID numeric(9) ,                      
@numDomainID numeric(9) ,        
@ClientTimeZoneOffset as int                              
    --                                                                           
AS                                                                            
BEGIN                                                                            
 SELECT CMP.numCompanyID,               
 CMP.vcCompanyName,                
 (select vcdata from listdetails where numListItemID = DM.numStatusID) as numCompanyStatusName,
 DM.numStatusID as numCompanyStatus,                                                                           
 (select vcdata from listdetails where numListItemID = CMP.numCompanyRating) as numCompanyRatingName, 
 CMP.numCompanyRating,             
 (select vcdata from listdetails where numListItemID = CMP.numCompanyType) as numCompanyTypeName,
 CMP.numCompanyType,              
 CMP.numCompanyType as numCmptype,               
 DM.bitPublicFlag,                 
 DM.numDivisionID,                                   
 DM. vcDivisionName,        
dbo.getCompanyAddress(dm.numdivisionId,1,dm.numDomainID) as Address,
dbo.getCompanyAddress(dm.numdivisionId,2,dm.numDomainID) as ShippingAddress,          
  isnull(AD2.vcStreet,'') as vcShipStreet,
  isnull(AD2.vcCity,'') as vcShipCity,
  isnull(dbo.fn_GetState(AD2.numState),'')  as vcShipState,
 (select vcdata from listdetails where numListItemID = numFollowUpStatus) as numFollowUpStatusName,
 numFollowUpStatus,                
 CMP.vcWebLabel1,               
 CMP.vcWebLink1,               
 CMP.vcWebLabel2,               
 CMP.vcWebLink2,                                                                             
 CMP.vcWebLabel3,               
 CMP.vcWebLink3,               
 CMP.vcWeblabel4,               
 CMP.vcWebLink4,                                                                       
 tintBillingTerms,              
 numBillingDays,              
 tintInterestType,              
 fltInterest,                                                  
 isnull(AD2.vcPostalCode,'') as vcShipPostCode, 
 AD2.numCountry vcShipCountry,                          
 DM.bitPublicFlag,              
 (select vcdata from listdetails where numListItemID = DM.numTerID)  as numTerIDName,
 DM.numTerID,                                                                         
 (select vcdata from listdetails where numListItemID = CMP.numCompanyIndustry) as numCompanyIndustryName, 
  CMP.numCompanyIndustry,              
 (select vcdata from listdetails where numListItemID = CMP.numCompanyCredit) as  numCompanyCreditName,
 CMP.numCompanyCredit,                                                                             
 isnull(CMP.txtComments,'') as txtComments,               
 isnull(CMP.vcWebSite,'') vcWebSite,              
 (select vcdata from listdetails where numListItemID = CMP.numAnnualRevID) as  numAnnualRevIDName,
 CMP.numAnnualRevID,                
 (select vcdata from listdetails where numListItemID = CMP.numNoOfEmployeesID) as numNoOfEmployeesIDName,               
 numNoOfEmployeesID,                                                                           
    (select vcdata from listdetails where numListItemID = CMP.vcProfile) as vcProfileName, 
 CMP.vcProfile,              
 DM.numCreatedBy,               
 dbo.fn_GetContactName(DM.numRecOwner) as RecOwner,               
 dbo.fn_GetContactName(DM.numCreatedBy)+' ' + convert(varchar(20),dateadd(minute,-@ClientTimeZoneOffset,DM.bintCreatedDate)) as vcCreatedBy,               
 DM.numRecOwner,                
 dbo.fn_GetContactName(DM.numModifiedBy)+' ' +convert(varchar(20),dateadd(minute,-@ClientTimeZoneOffset,DM.bintModifiedDate)) as vcModifiedBy,                    
 isnull(vcComPhone,'') as vcComPhone, 
 vcComFax,                   
 DM.numGrpID,  
 (select vcGrpName from Groups where numGrpId= DM.numGrpID) as numGrpIDName,              
 (select vcdata from listdetails where numListItemID = CMP.vcHow) as vcInfoSourceName,
 CMP.vcHow as vcInfoSource,                                                                            
 DM.tintCRMType,                                                                         
 (SELECT vcCampaignName FROM CampaignMaster WHERE numCampaignID= DM.numCampaignID) as  numCampaignIDName,
 numCampaignID,              
 numAssignedBy, isnull(bitNoTax,0) bitNoTax,                     
 (select A.vcFirstName+' '+A.vcLastName                       
 from AdditionalContactsInformation A                  
 join DivisionMaster D                        
 on D.numDivisionID=A.numDivisionID
 where A.numContactID=DM.numAssignedTo) as numAssignedToName,numAssignedTo,
 (select  count(*) from dbo.GenericDocuments where numRecID= @numDivisionID and vcDocumentSection='A' AND tintDocumentType=2) as DocumentCount ,
 (SELECT count(*)from CompanyAssociations where numDivisionID=@numDivisionID and bitDeleted=0 ) as AssociateCountFrom,                            
 (SELECT count(*)from CompanyAssociations where numAssociateFromDivisionID=@numDivisionID and bitDeleted=0 ) as AssociateCountTo,
 (select vcdata from listdetails where numListItemID = DM.numCompanyDiff) as numCompanyDiffName,               
 DM.numCompanyDiff,DM.vcCompanyDiff,isnull(DM.bitActiveInActive,1) as bitActiveInActive,
(SELECT SUBSTRING(
	(SELECT ','+  A.vcFirstName+' ' +A.vcLastName + CASE WHEN ISNULL(SR.numContactType,0)>0 THEN '(' + dbo.fn_GetListItemName(SR.numContactType) + ')' ELSE '' END
	FROM ShareRecord SR JOIN UserMaster UM ON UM.numUserDetailId=SR.numAssignedTo 
							 JOIN AdditionalContactsInformation A ON UM.numUserDetailId=A.numContactID 
		 WHERE SR.numDomainID=DM.numDomainID AND SR.numModuleID=1 AND SR.numRecordID=DM.numDivisionID
				AND UM.numDomainID=DM.numDomainID and UM.numDomainID=A.numDomainID FOR XML PATH('')),2,200000)) AS numShareWith,
 DM.bitOnCreditHold,
 DM.numDefaultPaymentMethod,
 DM.numAccountClassID,
 DM.tintPriceLevel                                                         
 FROM  CompanyInfo CMP    
  join DivisionMaster DM    
  on DM.numCompanyID=CMP.numCompanyID                                                                                                                                        
   LEFT JOIN dbo.AddressDetails AD1 ON AD1.numDomainID=DM.numDomainID 
   AND AD1.numRecordID=DM.numDivisionID AND AD1.tintAddressOf=2 AND AD1.tintAddressType=1 AND AD1.bitIsPrimary=1
   LEFT JOIN dbo.AddressDetails AD2 ON AD2.numDomainID=DM.numDomainID 
   AND AD2.numRecordID= DM.numDivisionID AND AD2.tintAddressOf=2 AND AD2.tintAddressType=2 AND AD2.bitIsPrimary=1

 where DM.numDivisionID=@numDivisionID   and DM.numDomainID=@numDomainID                                                                                          
END
GO




SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetEmployer')
DROP PROCEDURE USP_GetEmployer
GO
CREATE PROCEDURE [dbo].[USP_GetEmployer]
(
	@numDomainID NUMERIC(18,0)
)
AS 
BEGIN
	SELECT 
		DM.numDivisionID,
		CMP.vcCompanyName
	FROM 
		CompanyInfo CMP
	INNER JOIN
		DivisionMaster DM
	ON
		CMP.numCompanyId = DM.numCompanyID
	INNER JOIN
		Domain D
	ON
		DM.numDivisionID = D.numDivisionID
	WHERE 
		D.numDomainId=@numDomainID
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetHeaderReturnDetails' ) 
    DROP PROCEDURE USP_GetHeaderReturnDetails
GO

-- EXEC USP_GetHeaderReturnDetails 53, 1
CREATE PROCEDURE [dbo].[USP_GetHeaderReturnDetails]
    (
      @numReturnHeaderID NUMERIC(9) = 0,
      @numDomainId NUMERIC(9),
      @tintReceiveType NUMERIC(9)=0
    )
AS 
    BEGIN
    
		DECLARE @tintType TINYINT,@tintReturnType TINYINT;SET @tintType=0
        
		SELECT @tintReturnType=tintReturnType FROM ReturnHeader WHERE numReturnHeaderID = @numReturnHeaderID    
   
		DECLARE @monEstimatedBizDocAmount AS money
    
		SET @tintType=CASE When @tintReturnType=1 AND @tintReceiveType=1 THEN 9 
				       When @tintReturnType=1 AND @tintReceiveType=2 THEN 7 
				       When @tintReturnType=1 AND @tintReceiveType=3 THEN 9 
				       When @tintReturnType=2 AND @tintReceiveType=2 THEN 8
				       When @tintReturnType=3 THEN 10
				       When @tintReturnType=4 THEN 9 END 
				       
		SELECT @monEstimatedBizDocAmount=monAmount + monTotalTax - monTotalDiscount FROM dbo.GetReturnDealAmount(@numReturnHeaderID,@tintType)
	
        SELECT  RH.numReturnHeaderID,
                vcRMA,
                ISNULL(vcBizDocName, '') AS vcBizDocName,
                RH.numDomainId,
                ISNULL(RH.numDivisionId, 0) AS [numDivisionId],
                ISNULL(RH.numContactId, 0) AS [numContactId],
                ISNULL(RH.numOppId,0) AS numOppId,
                tintReturnType,
                numReturnReason,
                numReturnStatus,
                monAmount,
                ISNULL(monBizDocAmount,0) AS monBizDocAmount,ISNULL(monBizDocUsedAmount,0) AS monAmountUsed,
                monTotalTax,
                monTotalDiscount,
                tintReceiveType,
                vcComments,
                RH.numCreatedBy,
                dtCreatedDate,
                RH.numModifiedBy,
                dtModifiedDate,
                numReturnItemID,
                RI.numItemCode,
                I.vcItemName,
				ISNULL(I.vcModelID,'') AS vcModelID,
				ISNULL(I.vcSKU,'') AS vcSKU,
                numUnitHour,
                numUnitHourReceived,
                FORMAT(monPrice,'0.################') monPrice,
				CONVERT(DECIMAL(18, 4),(ISNULL(monTotAmount,0) / RI.numUnitHour)) AS monUnitSalePrice,
                monTotAmount,
                vcItemDesc,
                RI.numWareHouseItemID,
                WItems.numWarehouseID,
                ISNULL(W.vcWareHouse,'') AS vcWareHouse,
                I.vcModelID,
                I.vcManufacturer,
                numUOMId,
				ISNULL(dbo.fn_GetUOMName(I.numBaseUnit),'') vcBaseUOM,
				ISNULL(dbo.fn_GetUOMName(numUOMId),'') vcUOM,
                dbo.fn_GetListItemName(numReturnStatus) AS [vcStatus],
                dbo.fn_GetContactName(RH.numCreatedby) AS CreatedBy,
                dbo.fn_GetContactName(RH.numModifiedBy) AS ModifiedBy,
                ISNULL(C2.vcCompanyName, '')
                + CASE WHEN ISNULL(D.numCompanyDiff, 0) > 0
                       THEN '  '
                            + ISNULL(dbo.fn_getlistitemname(D.numCompanyDiff),
                                     '') + ':' + ISNULL(D.vcCompanyDiff, '')
                       ELSE ''
                  END AS vcCompanyname,
                ISNULL(D.tintCRMType, 0) AS [tintCRMType],
                ISNULL(numAccountID, 0) AS [numAccountID],
                ISNULL(vcCheckNumber, '') AS [vcCheckNumber],
                ISNULL(IsCreateRefundReceipt, 0) AS [IsCreateRefundReceipt],
                ( SELECT    vcData
                  FROM      dbo.ListDetails
                  WHERE     numListItemID = numReturnStatus
                ) AS [ReturnStatusName],
                ( SELECT    vcData
                  FROM      dbo.ListDetails
                  WHERE     numListItemID = numReturnReason
                ) AS [ReturnReasonName],
                ISNULL(con.vcEmail, '') AS vcEmail,
                ISNULL(RH.numBizDocTempID, 0) AS numBizDocTempID,
                case when charItemType='P' then 'Inventory Item' when charItemType='S' then 'Service' when charItemType='A' then 'Accessory' when charItemType='N' then 'Non-Inventory Item' end as ItemType,
                charItemType,ISNULL(I.numCOGsChartAcntId,0) AS ItemCoGs
                ,ISNULL(I.numAssetChartAcntId,0) AS ItemInventoryAsset
                ,ISNULL(I.numIncomeChartAcntId,0) AS ItemIncomeAccount
                ,isnull(i.monAverageCost,'0') as AverageCost
                ,ISNULL(RH.numItemCode,0) AS [numItemCodeForAccount],
                CASE WHEN ISNULL(RH.numOppId,0) <> 0 THEN vcPOppName 
					 ELSE ''
				END AS [Source],ISNULL(I.bitSerialized,0) AS bitSerialized,ISNULL(I.bitLotNo,0) AS bitLotNo,
				SUBSTRING(
	(SELECT ',' + vcSerialNo + CASE WHEN isnull(I.bitLotNo,0)=1 THEN ' (' + CONVERT(VARCHAR(15),oppI.numQty) + ')' ELSE '' end 
	FROM OppWarehouseSerializedItem oppI JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID=whi.numWareHouseItmsDTLID where oppI.numReturnHeaderID=RH.numReturnHeaderID and oppI.numReturnItemID=RI.numReturnItemID
	ORDER BY vcSerialNo FOR XML PATH('')),2,200000) AS SerialLotNo,@monEstimatedBizDocAmount AS monEstimatedBizDocAmount,ISNULL(RH.numBillPaymentIDRef,0) AS numBillPaymentIDRef,ISNULL(RH.numDepositIDRef,0) AS numDepositIDRef
        FROM    dbo.ReturnHeader RH
                LEFT JOIN dbo.ReturnItems RI ON RH.numReturnHeaderID = RI.numReturnHeaderID
                LEFT JOIN dbo.OpportunityMaster OM ON OM.numOppID = RH.numOppID AND RH.numDomainID = OM.numDomainID 
                LEFT JOIN Item I ON I.numItemCode = RI.numItemCode
                LEFT JOIN divisionMaster D ON RH.numDivisionID = D.numDivisionID
                LEFT JOIN CompanyInfo C2 ON C2.numCompanyID = D.numCompanyID
                LEFT JOIN dbo.OpportunityAddress OA ON RH.numReturnHeaderID = OA.numReturnHeaderID
                LEFT JOIN AdditionalContactsInformation con ON con.numdivisionId = RH.numdivisionId
                                                               AND con.numContactid = RH.numContactId
                LEFT JOIN WareHouseItems WItems ON WItems.numWareHouseItemID = RI.numWareHouseItemID
				LEFT JOIN Warehouses W ON W.numWarehouseID = WItems.numWarehouseID
	    WHERE  RH.numDomainID = @numDomainId
                AND RH.numReturnHeaderID = @numReturnHeaderID
		
    END

GO
-- exec Usp_GetItemAttributesForAPI @numItemCode=822625,@numDomainID=170,@WebApiId = 5
/****** Subject:  StoredProcedure [dbo].[Usp_GetItemAttributesForAPI]    Script Date: 12th Jan,2015 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--created by Manish Anjara
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'Usp_GetItemAttributesForAPI' )
    DROP PROCEDURE Usp_GetItemAttributesForAPI
GO
CREATE PROCEDURE [dbo].[Usp_GetItemAttributesForAPI]
    @numItemCode AS NUMERIC(9) = 0 ,
    @numDomainID AS NUMERIC(9) ,
    @WebApiId AS INT
    --@byteMode AS TINYINT = 0
AS
    DECLARE @numDefaultWarehouseID AS NUMERIC(18)
    SELECT  @numDefaultWarehouseID = ISNULL(numwarehouseID, 0)
    FROM    [dbo].[WebAPIDetail] AS WAD
    WHERE   [WAD].[numDomainId] = @numDomainID
            AND [WAD].[WebApiId] = @WebApiId
    IF ISNULL(@numDefaultWarehouseID, 0) = 0
        BEGIN
            SELECT TOP 1
                    *
            FROM    [dbo].[WareHouseItems] AS WHI
            WHERE   [WHI].[numItemID] = @numItemCode
                    AND [WHI].[numDomainID] = @numDomainID
        END


    SELECT  DISTINCT
            FMST.fld_id ,
            Fld_label ,
            ISNULL(FMST.numlistid, 0) AS numlistid ,
            [LD].[numListItemID] ,
            [LD].[vcData]
    INTO    #tmpSpec
    FROM    CFW_Fld_Master FMST
            LEFT JOIN CFw_Grp_Master Tmst ON Tmst.Grp_id = FMST.subgrp
            JOIN CFW_Loc_Master Lmst ON Lmst.Loc_id = FMST.Grp_id
            JOIN [dbo].[ListMaster] AS LM ON [FMST].[numlistid] = LM.[numListID]
            JOIN [dbo].[ListDetails] AS LD ON LD.[numListID] = LM.[numListID]
                                              AND [FMST].[numlistid] = LD.[numListID]
            JOIN [dbo].[ItemGroupsDTL] AS IGD ON IGD.[numOppAccAttrID] = [FMST].[Fld_id]
            JOIN [dbo].[CFW_Fld_Values_Serialized_Items] AS CFVSI ON [CFVSI].[Fld_ID] = [FMST].[Fld_id]
                                                              AND CONVERT(NUMERIC, [CFVSI].[Fld_Value]) = [LD].[numListItemID]
                                                              AND [CFVSI].[RecId] IN (
                                                              SELECT
                                                              numWarehouseItemID
                                                              FROM
                                                              [dbo].[WareHouseItems]
                                                              WHERE
                                                              numwarehouseID = @numDefaultWarehouseID
                                                              AND numDomainID = @numDomainID
                                                              AND numItemID = @numItemCode )
    WHERE   FMST.numDomainID = @numDomainID
            AND Lmst.Loc_id = 9
    ORDER BY [fmst].[Fld_id]

	--SELECT * FROM [#tmpSpec] AS TS
    SELECT DISTINCT
            TS.fld_label [Name] ,
            STUFF(( SELECT  ',' + CAST([vcData] AS VARCHAR(200))
                    FROM    #tmpSpec
                    WHERE   [ts].[Fld_id] = #tmpSpec.[Fld_id]
                  FOR
                    XML PATH('')
                  ), 1, 1, '') [Value]
    FROM    [#tmpSpec] AS TS
    

    DECLARE @bitSerialize AS BIT                      
    DECLARE @str AS NVARCHAR(MAX)                 
    DECLARE @str1 AS NVARCHAR(MAX)               
    DECLARE @ColName AS VARCHAR(25)                      
    SET @str = ''                       
                        
    DECLARE @bitKitParent BIT
    DECLARE @numItemGroupID AS NUMERIC(9)                        
                        
    SELECT  @numItemGroupID = numItemGroup ,
            @bitSerialize = CASE WHEN bitSerialized = 0 THEN bitLotNo
                                 ELSE bitSerialized
                            END ,
            @bitKitParent = ( CASE WHEN ISNULL(bitKitParent, 0) = 1
                                        AND ISNULL(bitAssembly, 0) = 1 THEN 0
                                   WHEN ISNULL(bitKitParent, 0) = 1 THEN 1
                                   ELSE 0
                              END )
    FROM    Item
    WHERE   numItemCode = @numItemCode                        
    IF @bitSerialize = 1
        SET @ColName = 'numWareHouseItmsDTLID,1'              
    ELSE
        SET @ColName = 'numWareHouseItemID,0'              
              
--Create a Temporary table to hold data                                                            
    CREATE TABLE #tempTable
        (
          ID INT IDENTITY
                 PRIMARY KEY ,
          numCusFlDItemID NUMERIC(9)
        )                         
                        
    INSERT  INTO #tempTable
            ( numCusFlDItemID
            )
            SELECT DISTINCT
                    ( numOppAccAttrID )
            FROM    ItemGroupsDTL
            WHERE   numItemGroupID = @numItemGroupID
                    AND tintType = 2                       
                          
                 
    DECLARE @ID AS NUMERIC(9)                        
    DECLARE @numCusFlDItemID AS VARCHAR(20)                        
    DECLARE @fld_label AS VARCHAR(100) ,
        @fld_type AS VARCHAR(100)                        
    SET @ID = 0                        
    SELECT TOP 1
            @ID = ID ,
            @numCusFlDItemID = numCusFlDItemID ,
            @fld_label = fld_label ,
            @fld_type = fld_type
    FROM    #tempTable
            JOIN CFW_Fld_Master ON numCusFlDItemID = Fld_ID                        
                         
    WHILE @ID > 0
        BEGIN                        
                          
           -- SET @str = @str + ',  dbo.GetCustFldItems(' + @numCusFlDItemID + ',9,' + @ColName + ') as [' + @fld_label + ']'
	
            --IF @byteMode = 1
            SET @str = @str + ', dbo.GetCustFldItemsValue(' + @numCusFlDItemID + ',9,' + @ColName + ',''' + @fld_type + ''') as [' + @fld_label + '_c]'                                        
                          
            SELECT TOP 1
                    @ID = ID ,
                    @numCusFlDItemID = numCusFlDItemID ,
                    @fld_label = fld_label
            FROM    #tempTable
                    JOIN CFW_Fld_Master ON numCusFlDItemID = Fld_ID
                                           AND ID > @ID                        
            IF @@rowcount = 0
                SET @ID = 0                        
                          
        END                        
                       
--      

    DECLARE @KitOnHand AS FLOAT;
    SET @KitOnHand = 0

    SET @str1 = 'select '

    SET @str1 = @str1 + 'I.numItemCode,'
	
    SET @str1 = @str1
        + 'numWareHouseItemID,isnull(vcWarehouse,'''') + '': '' + isnull(WL.vcLocation,'''') vcWarehouse,W.numWareHouseID,
Case when @bitKitParent=1 then ISNULL(dbo.fn_GetKitInventoryByWarehouse(I.numItemCode, W.numWareHouseID),0) else isnull(numOnHand,0) end as [OnHand],
CAST((dbo.fn_UOMConversion(ISNULL(I.numBaseUnit,0),I.numItemCode,I.numDomainId,ISNULL(I.numPurchaseUnit,0)) * Case when @bitKitParent=1 THEN ISNULL(dbo.fn_GetKitInventoryByWarehouse(I.numItemCode, W.numWareHouseID),0) ELSE isnull(numOnHand,0) end) as numeric(18,2)) as PurchaseOnHand,
CAST((dbo.fn_UOMConversion(ISNULL(I.numBaseUnit,0),I.numItemCode,I.numDomainId,ISNULL(I.numSaleUnit,0)) * Case when @bitKitParent=1 THEN ISNULL(dbo.fn_GetKitInventoryByWarehouse(I.numItemCode, W.numWareHouseID),0) ELSE isnull(numOnHand,0) end) as numeric(18,2)) as SalesOnHand,
Case when @bitKitParent=1 THEN 0 ELSE isnull(numReorder,0) END as [Reorder] 
,Case when @bitKitParent=1 THEN 0 ELSE isnull(numOnOrder,0) END as [OnOrder]
,Case when @bitKitParent=1 THEN 0 ELSE isnull(numAllocation,0) END as [Allocation] 
,Case when @bitKitParent=1 THEN 0 ELSE isnull(numBackOrder,0) END as [BackOrder]
,0 as Op_Flag , isnull(WL.vcLocation,'''') Location,isnull(WL.numWLocationID,0) as numWLocationID,
round(isnull(monWListPrice,0),2) Price,isnull(vcWHSKU,'''') as SKU,isnull(vcBarCode,'''') as BarCode '
        + CASE WHEN @bitSerialize = 1 THEN ' '
               ELSE @str
          END
        + '                   
,(SELECT CASE WHEN ISNULL(subI.bitKitParent,0) = 1 AND ISNULL((SELECT COUNT(*) FROM OpportunityItems 
															   WHERE OpportunityItems.numItemCode = WareHouseItems.numItemID
															   AND WareHouseItems.numWareHouseItemID = OpportunityItems.numWarehouseItmsID),0) = 0 THEN 1
			ELSE 0 
	    END 
FROM Item subI WHERE subI.numItemCode = WareHouseItems.numItemID) [IsDeleteKitWarehouse],@bitKitParent [bitKitParent]
from WareHouseItems                           
join Warehouses W                             
on W.numWareHouseID=WareHouseItems.numWareHouseID 
left join WarehouseLocation WL on WL.numWLocationID = WareHouseItems.numWLocationID
join Item I on I.numItemCode=WareHouseItems.numItemID and WareHouseItems.numDomainId=I.numDomainId
where numItemID=' + CONVERT(VARCHAR(15), @numItemCode)
        + ' AND WareHouseItems.numWarehouseItemID IN (
                                                              SELECT
                                                              numWarehouseItemID
                                                              FROM
                                                              [dbo].[WareHouseItems]
                                                              WHERE
                                                              numwarehouseID = '
        + CONVERT(VARCHAR(15), @numDefaultWarehouseID)
        + '
                                                              AND numDomainID = '
        + CONVERT(VARCHAR(15), @numDomainID)
        + '
                                                              AND numItemID = '
        + CONVERT(VARCHAR(15), @numItemCode) + ' ) '
   
    PRINT ( @str1 )           

    EXECUTE sp_executeSQL @str1, N'@bitKitParent bit', @bitKitParent
                       

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetNamedPriceLevel')
DROP PROCEDURE USP_GetNamedPriceLevel
GO
CREATE PROCEDURE [dbo].[USP_GetNamedPriceLevel]
(
	@numDomainID NUMERIC(18,0)
)
AS 
BEGIN
	SELECT 
		Id,
		Value
	FROM
	(
		SELECT 
			ROW_NUMBER() OVER(PARTITION BY PricingTable.numItemCode ORDER BY numPricingID) Id,
			CONCAT('Price Level ',ROW_NUMBER() OVER(PARTITION BY PricingTable.numItemCode ORDER BY numPricingID)) Value
		FROM 
			PricingTable 
		INNER JOIN
			Item
		ON
			Item.numItemCode = PricingTable.numItemCode 
			AND Item.numDomainID = @numDomainID
		WHERE 
			tintRuleType = 3 
	) TEMP
	GROUP BY
		Id,
		Value
	ORDER BY
		Id
END
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetOrderItems' ) 
    DROP PROCEDURE USP_GetOrderItems
GO
CREATE PROCEDURE USP_GetOrderItems ( @tintMode TINYINT = 0,
                                     @numOppItemID NUMERIC(9) = 0,
                                     @numOppID NUMERIC(9) = 0,
                                     @numDomainId NUMERIC(9),
                                     @ClientTimeZoneOffset INT,
									 @vcOppItemIDs VARCHAR(2000)='' )
AS 
BEGIN
	
    IF @tintMode = 1 --Add Return grid data & For Recurring Get Original Units to split
        BEGIN
            SELECT  OI.numoppitemtCode,
                    OI.numItemCode,
                    OI.vcItemName,
                    OI.vcItemDesc,
                    OI.numUnitHour numOriginalUnitHour,
					CAST(( dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), OI.numItemCode,
                                    OM.numDomainId, ISNULL(OI.numUOMId, 0))
						* OI.numUnitHour ) AS NUMERIC(18, 2)) AS numUnitHour,
					OI.monPrice
            FROM    dbo.OpportunityMaster OM
                    INNER JOIN dbo.OpportunityItems OI ON OM.numOppId = OI.numOppId
                    INNER JOIN dbo.Item I ON I.numItemCode = OI.numItemCode
            WHERE   OM.numDomainId = @numDomainId
                    AND OM.numOppId = @numOppID
                    
        END
 
    IF @tintMode = 2 -- Get single item based on PK, or all items
		-- Used by Sales/Purchase Template Load items in New Sales Order
        BEGIN 
            SELECT  OI.numoppitemtCode,
                    OI.numOppId,
                    OI.numItemCode,
					OI.bitItemPriceApprovalRequired,
                    OI.numUnitHour numOriginalUnitHour,
                    OI.monPrice,
                    OI.monTotAmount,
                    OI.numSourceID,
                    OI.vcItemDesc,
                    OI.numWarehouseItmsID,
                    OI.vcType,
                    OI.vcAttributes,
                    OI.bitDropShip,
                    ISNULL(OI.numUnitHourReceived,0) AS numUnitHourReceived,
                    OI.bitDiscountType,
                    OI.fltDiscount,
                    OI.monTotAmtBefDiscount,
                    ISNULL(OI.numQtyShipped,0) AS numQtyShipped,
                    OI.vcItemName,
                    OI.vcModelID,
                    OI.vcPathForTImage,
                    OI.vcManufacturer,
                    OI.monVendorCost,
                    ISNULL(OI.numUOMId, 0) AS numUOMId,
                    ISNULL(OI.numUOMId, 0) numUOM, -- used from add edit order
                    ISNULL(U.vcUnitName, '') vcUOMName,
                    dbo.fn_UOMConversion(OI.numUOMId, OI.numItemCode,
                                         OM.numDomainId, NULL) AS UOMConversionFactor,
                    OI.bitWorkOrder,
                    isnull(OI.numVendorWareHouse,0) as numVendorWareHouse,
                    OI.numShipmentMethod,
                    OI.numSOVendorId,
                    OI.numProjectID,
                    OI.numClassID,
                    OI.numProjectStageID,
                    OI.numToWarehouseItemID,
                    WItems.numWarehouseID,
                    W.vcWareHouse AS Warehouse,
					CAST(( dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), OI.numItemCode,
                                    OM.numDomainId, ISNULL(OI.numUOMId, 0))
						* OI.numUnitHour ) AS NUMERIC(18, 2)) AS numUnitHour,
					isnull(OI.numClassID,0) as numClassID,
					isnull(OI.numProjectID,0) as numProjectID,
					dbo.fn_GetUOMName(I.numBaseUnit) AS vcBaseUOMName,
					OI.bitDropShip AS bitAllowDropShip,
					I.numVendorID,
					ISNULL(I.bitFreeShipping,0) AS [bitFreeShipping],
					ISNULL(I.txtItemDesc,'') txtItemDesc,
					ISNULL(I.vcModelID,'') vcModelID,
					ISNULL(I.vcManufacturer,'') vcManufacturer,
					ISNULL(I.numBarCodeId,'') numBarCodeId,
					ISNULL(I.bitSerialized,0) bitSerialized,
					ISNULL(I.bitKitParent,0) bitKitParent,
					ISNULL(I.bitAssembly,0) bitAssembly,
					ISNULL(I.IsArchieve,0) IsArchieve,
					ISNULL(I.bitLotNo,0) bitLotNo,
					ISNULL(WItems.monWListPrice,0) monWListPrice,
					ISNULL(I.bitTaxable, 0) bitTaxable,
                    I.charItemType,
                    I.bitKitParent,
					I.bitAssembly,
					I.bitSerialized,
					ISNULL(I.fltWeight,0) AS [fltWeight],
					ISNULL(I.fltLength,0) AS [fltLength],
					ISNULL(I.fltWidth,0) AS [fltWidth],
					ISNULL(I.fltHeight,0) AS [fltHeight],
					ISNULL(WItems.vcWHSKU,ISNULL(I.vcSKU,'')) AS [vcSKU],
					ISNULL(I.monAverageCost,0) AS monAverageCost,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numOnHand),0) AS VARCHAR(10)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numOnHand,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numOnOrder),0) AS VARCHAR(10)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numOnOrder,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numAllocation),0) AS VARCHAR(10)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numAllocation,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numBackOrder),0) AS VARCHAR(10)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numBackOrder
            FROM    dbo.OpportunityItems OI
                    INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OI.numOppId
                    INNER JOIN dbo.Item I ON I.numItemCode = OI.numItemCode
                    LEFT JOIN UOM U ON U.numUOMId = OI.numUOMId
                    LEFT JOIN WareHouseItems WItems ON WItems.numWareHouseItemID = OI.numWarehouseItmsID
                    LEFT JOIN Warehouses W ON W.numWarehouseID = WItems.numWarehouseID
            WHERE   OI.numOppId = @numOppID
                    AND ( OI.numoppitemtCode = @numOppItemID
                          OR @numOppItemID = 0
                        )
                    AND OM.numDomainId = @numDomainId
                    
        END
 
    IF @tintMode = 3 -- Get item for SO to PO and vice versa creation
        BEGIN
            DECLARE @tintOppType AS TINYINT
			SELECT @tintOppType=tintOppType FROM OpportunityMaster WHERE numOppId=@numOppID

			IF @tintOppType = 1
			BEGIN
				DECLARE @TempItems TABLE
				(
					ID INT IDENTITY(1,1),numItemCode NUMERIC(18,0),vcItemName VARCHAR(300),charItemType char,vcModelID VARCHAR(100),vcItemDesc TEXT,vcType VARCHAR(20),vcPathForTImage VARCHAR(200),
					numWarehouseItmsID NUMERIC(18,0),vcAttributes VARCHAR(300),bitDropShip BIT,numUnitHour INT,numUOM NUMERIC(18,0),vcUOMName VARCHAR(100),UOMConversionFactor DECIMAL(18,5),
					monPrice MONEY,numVendorID NUMERIC(18,0),vcPartNo VARCHAR(300),monVendorCost MONEY,numSOVendorId NUMERIC(18,0),numClassID NUMERIC(18,0),numProjectID NUMERIC(18,0),tintLevel TINYINT
				)

				INSERT INTO 
					@TempItems
				SELECT
					OI.numItemCode,OI.vcItemName,I.charItemType,OI.vcModelID,OI.vcItemDesc,OI.vcType,ISNULL(OI.vcPathForTImage, ''),ISNULL(OI.numWarehouseItmsID,0),ISNULL(OI.vcAttributes, ''),
					0,OI.numUnitHour,ISNULL(OI.numUOMId, 0),ISNULL(U.vcUnitName, ''),dbo.fn_UOMConversion(OI.numUOMId, OI.numItemCode,OM.numDomainId, NULL) AS UOMConversionFactor,
					OI.monPrice,ISNULL(I.numVendorID, 0),ISNULL(V.vcPartNo, ''),ISNULL(V.monCost, 0),ISNULL(OI.numSOVendorId, 0),0,0,0
				FROM    
					dbo.OpportunityItems OI
					INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OI.numOppId
					INNER JOIN dbo.Item I ON I.numItemCode = OI.numItemCode
					LEFT JOIN UOM U ON U.numUOMId = OI.numUOMId
					LEFT JOIN Vendor V ON V.numVendorID = I.numVendorID	AND I.numItemCode = V.numItemCode
				WHERE   
					OI.numOppId = @numOppID
					AND OM.numDomainId = @numDomainId
					AND ISNULL(I.bitKitParent,0) = 0
					AND ISNULL(I.bitAssembly,0) = 0


				--INSERT KIT CHILD ITEMS WHICH ARE NOT KIT AND NOT YET ADDED
				INSERT INTO 
					@TempItems
				SELECT
					OKI.numChildItemID,I.vcItemName,I.charItemType,I.vcModelID,I.txtItemDesc,(CASE WHEN I.charitemType = 'P' THEN 'Product' WHEN I.charitemType = 'S' THEN 'Service' END),
					ISNULL((SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = I.numItemCode AND bitDefault=1),''),ISNULL(OKI.numWareHouseItemId,0),
					ISNULL(dbo.USP_GetAttributes(OKI.numWarehouseItemID, bitSerialized),''),0,OKI.numQtyItemsReq,ISNULL(OKI.numUOMId, 0),ISNULL(U.vcUnitName, ''),
					dbo.fn_UOMConversion(OKI.numUOMId, OKI.numChildItemID,OM.numDomainId, NULL),
					(CASE WHEN I.charitemType = 'P' THEN WI.monWListPrice ELSE I.monListPrice END),
					ISNULL(I.numVendorID, 0),ISNULL(V.vcPartNo, ''),ISNULL(V.monCost, 0),0,0,0,1
				FROM    
					dbo.OpportunityKitItems OKI
				LEFT JOIN WarehouseItems WI ON OKI.numWareHouseItemId = WI.numWarehouseItemID
				INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OKI.numOppId
				INNER JOIN dbo.Item I ON I.numItemCode = OKI.numChildItemID
				LEFT JOIN UOM U ON U.numUOMId = OKI.numUOMId
				LEFT JOIN Vendor V ON V.numVendorID = I.numVendorID	AND I.numItemCode = V.numItemCode
				LEFT JOIN @TempItems t1 ON OKI.numChildItemID = t1.numItemCode AND ISNULL(OKI.numWareHouseItemId,0) = ISNULL(t1.numWarehouseItmsID,0)
				WHERE   
					OKI.numOppId = @numOppID
					AND OM.numDomainId = @numDomainId
					AND ISNULL(I.bitKitParent,0) = 0
					AND ISNULL(I.bitAssembly,0) = 0
					AND t1.ID IS NULL


				-- FOR KIT CHILD ITEMS WHICH ARE ALREADY ADDED AT PARENT LEVEL - UPDATE QTY ONLY
				UPDATE 
					t2
				SET 
					t2.numUnitHour = ISNULL(t2.numUnitHour,0) + ISNULL(OKI.numQtyItemsReq,0),
					t2.monPrice = (CASE 
									WHEN ISNULL(t2.monPrice,0) < (CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END)  
									THEN t2.monPrice
									ELSE (CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END)
									END)
				FROM
					@TempItems t2
				INNER JOIN
					OpportunityKitItems OKI 
				ON
					OKI.numChildItemID = t2.numItemCode AND ISNULL(OKI.numWareHouseItemId,0) = ISNULL(t2.numWarehouseItmsID,0)
				LEFT JOIN WarehouseItems WI ON OKI.numWareHouseItemId = WI.numWarehouseItemID
				INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OKI.numOppId
				INNER JOIN dbo.Item I ON I.numItemCode = OKI.numChildItemID
				LEFT JOIN UOM U ON U.numUOMId = OKI.numUOMId
				LEFT JOIN Vendor V ON V.numVendorID = I.numVendorID	AND I.numItemCode = V.numItemCode
				WHERE   
					OKI.numOppId = @numOppID
					AND OM.numDomainId = @numDomainId
					AND ISNULL(I.bitKitParent,0) = 0
					AND ISNULL(I.bitAssembly,0) = 0
					AND t2.tintLevel <> 1

				--INSERT SUB KIT CHILD ITEMS WHICH ARE NOT KIT AND NOT YET ADDED
				INSERT INTO 
					@TempItems
				SELECT
					OKCI.numItemID,I.vcItemName,I.charItemType,I.vcModelID,I.txtItemDesc,(CASE WHEN I.charitemType = 'P' THEN 'Product' WHEN I.charitemType = 'S' THEN 'Service' END),
					ISNULL((SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = I.numItemCode AND bitDefault=1),''),ISNULL(OKCI.numWareHouseItemId,0),
					ISNULL(dbo.USP_GetAttributes(OKCI.numWarehouseItemID, bitSerialized),''),0,OKCI.numQtyItemsReq,ISNULL(OKCI.numUOMId, 0),ISNULL(U.vcUnitName, ''),
					dbo.fn_UOMConversion(OKCI.numUOMId, OKCI.numItemID,OM.numDomainId, NULL),
					(CASE WHEN I.charitemType = 'P' THEN WI.monWListPrice ELSE I.monListPrice END),
					ISNULL(I.numVendorID, 0),ISNULL(V.vcPartNo, ''),ISNULL(V.monCost, 0),0,0,0,2
				FROM    
					dbo.OpportunityKitChildItems OKCI
				LEFT JOIN WarehouseItems WI ON OKCI.numWareHouseItemId = WI.numWarehouseItemID
				INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OKCI.numOppId
				INNER JOIN dbo.Item I ON I.numItemCode = OKCI.numItemID
				LEFT JOIN UOM U ON U.numUOMId = OKCI.numUOMId
				LEFT JOIN Vendor V ON V.numVendorID = I.numVendorID	AND I.numItemCode = V.numItemCode
				LEFT JOIN @TempItems t1 ON OKCI.numItemID = t1.numItemCode AND ISNULL(OKCI.numWareHouseItemId,0) = ISNULL(t1.numWarehouseItmsID,0)
				WHERE   
					OKCI.numOppId = @numOppID
					AND OM.numDomainId = @numDomainId
					AND ISNULL(I.bitKitParent,0) = 0
					AND ISNULL(I.bitAssembly,0) = 0
					AND t1.ID IS NULL

				-- FOR SUB KIT CHILD ITEMS WHICH ARE ALREADY ADDED AT PARENT LEVEL - UPDATE QTY ONLY
				UPDATE 
					t2
				SET 
					t2.numUnitHour = ISNULL(t2.numUnitHour,0) + ISNULL(OKCI.numQtyItemsReq,0),
					t2.monPrice = (CASE 
									WHEN ISNULL(t2.monPrice,0) < (CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END)  
									THEN t2.monPrice
									ELSE (CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END)
									END)
				FROM
					@TempItems t2
				INNER JOIN
					OpportunityKitChildItems OKCI 
				ON
					OKCI.numItemID = t2.numItemCode AND ISNULL(OKCI.numWareHouseItemId,0) = ISNULL(t2.numWarehouseItmsID,0)
				LEFT JOIN WarehouseItems WI ON OKCI.numWareHouseItemId = WI.numWarehouseItemID
				INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OKCI.numOppId
				INNER JOIN dbo.Item I ON I.numItemCode = OKCI.numItemID
				LEFT JOIN UOM U ON U.numUOMId = OKCI.numUOMId
				LEFT JOIN Vendor V ON V.numVendorID = I.numVendorID	AND I.numItemCode = V.numItemCode
				WHERE   
					OKCI.numOppId = @numOppID
					AND OM.numDomainId = @numDomainId
					AND ISNULL(I.bitKitParent,0) = 0
					AND ISNULL(I.bitAssembly,0) = 0
					AND t2.tintLevel <> 2

				SELECT * FROM @TempItems
			END
			ELSE
			BEGIN
				SELECT  OI.numoppitemtCode,
                    OI.numOppId,
					OI.bitItemPriceApprovalRequired,
                    OI.vcItemDesc,
                    ISNULL(OI.vcPathForTImage, '') vcPathForTImage,
                    ISNULL(OI.numWarehouseItmsID, 0) numWarehouseItmsID,
                    OI.vcItemName,
                    OI.vcModelID,
                    ISNULL(OI.vcAttributes, '') vcAttributes,
                    OI.vcType,
                    ISNULL(OI.bitDropShip, 0) bitDropShip,
                    OI.numItemCode,
                    OI.numUnitHour numOriginalUnitHour,
                    ISNULL(OI.numUOMId, 0) AS numUOM,
                    ISNULL(U.vcUnitName, '') vcUOMName,
                    dbo.fn_UOMConversion(OI.numUOMId, OI.numItemCode,
                                         OM.numDomainId, NULL) AS UOMConversionFactor,
                    ISNULL(I.numVendorID, 0) numVendorID,
                    ISNULL(V.monCost, 0) monVendorCost,
                    ISNULL(V.vcPartNo, '') vcPartNo,
                    OI.monPrice,
                    ISNULL(OI.numSOVendorId, 0) numSOVendorId,
                    ISNULL(I.charItemType, 'N') charItemType,
					CAST(( dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), OI.numItemCode,
                                    OM.numDomainId, ISNULL(OI.numUOMId, 0))
						* OI.numUnitHour ) AS NUMERIC(18, 2)) AS numUnitHour,
					isnull(OI.numClassID,0) as numClassID,isnull(OI.numProjectID,0) as numProjectID,ISNULL(I.fltWeight,0) AS [fltWeight],ISNULL(I.bitFreeShipping,0) AS [bitFreeShipping],
					ISNULL(I.fltLength,0) AS [fltLength],
					ISNULL(I.fltWidth,0) AS [fltWidth],
					ISNULL(I.fltHeight,0) AS [fltHeight],
					ISNULL((SELECT vcWHSKU FROM dbo.WareHouseItems WHERE numWarehouseItemID = OI.numWarehouseItmsID ),ISNULL(I.vcSKU,'')) AS [vcSKU]
				FROM    dbo.OpportunityItems OI
						INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OI.numOppId
						INNER JOIN dbo.Item I ON I.numItemCode = OI.numItemCode
						LEFT JOIN UOM U ON U.numUOMId = OI.numUOMId
						LEFT JOIN Vendor V ON V.numVendorID = I.numVendorID
												AND I.numItemCode = V.numItemCode
				WHERE   OI.numOppId = @numOppID
						AND OM.numDomainId = @numDomainId
			END

            
					--AND 1 = (CASE 
					--			WHEN @tintOppType = 1 
					--			THEN (CASE WHEN ISNULL(I.bitKitParent,0) = 0 THEN 1 ELSE 0 END)
					--			ELSE 1
					--		END)
        END
        IF @tintMode = 4 -- Add Edit Order items
        BEGIN 
            SELECT  OI.numoppitemtCode,
                    OI.numOppId,
                    OI.numItemCode,
					OI.bitItemPriceApprovalRequired,
					OI.numUnitHour AS numOrigUnitHour,
                    OI.numUnitHour,
                    OI.monPrice,
                    OI.monTotAmount,
                    OI.numSourceID,
                    OI.vcItemDesc,
                    OI.numWarehouseItmsID,
                    OI.vcType ItemType,
                    OI.vcAttributes Attributes,
                    OI.bitDropShip DropShip,
                    OI.numUnitHourReceived,
                    OI.bitDiscountType,
                    OI.fltDiscount,
                    OI.monTotAmtBefDiscount,
                    OI.numQtyShipped,
                    OI.vcItemName,
                    OI.vcModelID,
                    OI.vcPathForTImage,
                    OI.vcManufacturer,
                    OI.monVendorCost,
                    ISNULL(OI.numUOMId, 0) AS numUOM,
                    ISNULL(U.vcUnitName, '') vcUOMName,
                    ISNULL(dbo.fn_UOMConversion(OI.numUOMId, OI.numItemCode,
                                         OM.numDomainId, NULL),1) AS UOMConversionFactor,
                    OI.bitWorkOrder,
                    isnull(OI.numVendorWareHouse,0) as numVendorWareHouse,
                    OI.numShipmentMethod,
                    OI.numSOVendorId,
                    OI.numProjectID,
                    OI.numClassID,
                    OI.numProjectStageID,
                    OI.numToWarehouseItemID,
                    ISNULL(I.bitTaxable, 0) bitTaxable,
                   UPPER(I.charItemType) charItemType,
                    WItems.numWarehouseID,
                    W.vcWareHouse AS Warehouse,
                    0 as Op_Flag,
					CAST(( dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), OI.numItemCode,
                                    OM.numDomainId, ISNULL(OI.numUOMId, 0))
						* OI.numUnitHour ) AS NUMERIC(18, 2)) AS numUnitHour,
					isnull(OI.numClassID,0) as numClassID,isnull(OI.numProjectID,0) as numProjectID,
					CASE WHEN (SELECT COUNT(*) FROM OpportunityBizDocs OB JOIN dbo.OpportunityBizDocItems OBI ON ob.numOppBizDocsId=obi.numOppBizDocID
					AND (ob.bitAuthoritativeBizDocs=1 OR ob.numBizDocId=304) AND obi.numOppItemID=oi.numoppitemtCode AND ob.numOppId=OM.numOppId)>0 THEN 1 ELSE 0 END  AS bitIsAuthBizDoc,
					ISNULL(OI.numUnitHourReceived,0) AS numUnitHourReceived,ISNULL(numQtyShipped,0) AS numQtyShipped,dbo.fn_GetUOMName(I.numBaseUnit) AS vcBaseUOMName,ISNULL(I.fltWeight,0) AS [fltWeight],ISNULL(I.bitFreeShipping,0) AS [bitFreeShipping],
					ISNULL(I.fltLength,0) AS [fltLength],
					ISNULL(I.fltWidth,0) AS [fltWidth],
					ISNULL(I.fltHeight,0) AS [fltHeight],
					ISNULL(WItems.vcWHSKU,ISNULL(I.vcSKU,'')) AS [vcSKU],
					ISNULL(I.monAverageCost,0) AS monAverageCost,
					ISNULL(I.bitFreeShipping,0) AS [bitFreeShipping],
					ISNULL(I.txtItemDesc,'') txtItemDesc,
					ISNULL(I.vcModelID,'') vcModelID,
					ISNULL(I.vcManufacturer,'') vcManufacturer,
					ISNULL(I.numBarCodeId,'') numBarCodeId,
					ISNULL(I.bitSerialized,0) bitSerialized,
					ISNULL(I.bitKitParent,0) bitKitParent,
					ISNULL(I.bitAssembly,0) bitAssembly,
					ISNULL(I.IsArchieve,0) IsArchieve,
					ISNULL(I.bitLotNo,0) bitLotNo,
					ISNULL(WItems.monWListPrice,0) monWListPrice,
					ISNULL(I.bitTaxable, 0) bitTaxable,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numOnHand),0) AS VARCHAR(10)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numOnHand,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numOnOrder),0) AS VARCHAR(10)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numOnOrder,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numAllocation),0) AS VARCHAR(10)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numAllocation,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numBackOrder),0) AS VARCHAR(10)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numBackOrder,
					case when ISNULL(I.bitAssembly,0)=1 then dbo.fn_GetAssemblyPossibleWOQty(I.numItemCode) else 0 end AS numMaxWorkOrderQty,
					ISNULL((SELECT COUNT(*) FROM OppWarehouseSerializedItem WHERE numOppId=@numOppID AND numOppItemID=OI.numoppitemtCode),0) AS numSerialNoAssigned,
					(CASE WHEN (SELECT COUNT(*) FROM OpportunityBizDocs OB JOIN dbo.OpportunityBizDocItems OBI ON OB.numOppBizDocsId=OBI.numOppBizDocID WHERE OB.numOppId = OI.numOppId AND OBI.numOppItemID=OI.numoppitemtCode AND OB.numBizDocId=296) > 0 THEN 1 ELSE 0 END) AS bitAddedFulFillmentBizDoc
--					0 AS [Tax0],0 AS bitTaxable0
            FROM    dbo.OpportunityItems OI
                    INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OI.numOppId
                    INNER JOIN dbo.Item I ON I.numItemCode = OI.numItemCode
                    LEFT JOIN UOM U ON U.numUOMId = OI.numUOMId
                    LEFT JOIN WareHouseItems WItems ON WItems.numWareHouseItemID = OI.numWarehouseItmsID
                    LEFT JOIN Warehouses W ON W.numWarehouseID = WItems.numWarehouseID
            WHERE   OI.numOppId = @numOppID
                    AND OM.numDomainId = @numDomainId
                    
		SELECT  vcSerialNo,
				vcComments AS Comments,
				numOppItemID AS numoppitemtCode,
				W.numWarehouseItmsDTLID,
				numWarehouseItmsID AS numWItmsID,
				dbo.Fn_getattributes(W.numWarehouseItmsDTLID, 1) Attributes
		FROM    WareHouseItmsDTL W
				JOIN OppWarehouseSerializedItem O ON O.numWarehouseItmsDTLID = W.numWareHouseItmsDTLID
		WHERE   numOppID = @numOppID                
                   
        END
        
        
		IF @tintMode = 5 -- Get WebApi Order Items to enter WebApi Order line Item Details 
		BEGIN 
			SELECT OI.numOppItemtCode,OI.numOppId,OI.numItemCode,
			ISNULL((SELECT vcWHSKU FROM dbo.WareHouseItems WHERE WareHouseItems.numWarehouseItemID = OI.numWarehouseItmsID),ISNULL(IT.vcSKU,'')) AS [vcSKU],
			OI.vcItemDesc,OMAPI.vcAPIOppId
			FROM OpportunityItems OI 
			INNER JOIN dbo.OpportunityMaster OM ON OI.numOppId = OM.numOppId 
			INNER JOIN dbo.OpportunityMasterAPI OMAPI ON OM.numOppId = OMAPI.numOppId
			INNER JOIN dbo.Item IT ON OI.numItemCode = IT.numItemCode
			WHERE OI.numOppId = @numOppID and OM.numDomainID = @numDomainId 
        END

END
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetPackingDetail')
DROP PROCEDURE USP_GetPackingDetail
GO
CREATE PROCEDURE USP_GetPackingDetail
    @numDomainID AS NUMERIC(9),
	@vcBizDocsIds VARCHAR(MAX),
	@tintMode AS TINYINT 
AS
BEGIN
	-- PICK LIST
	IF @tintMode = 1
	BEGIN
		SELECT
			ISNULL(vcItemName,'') AS vcItemName,
			ISNULL((SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numDomainId=@numDomainID AND ItemImages.numItemCode=Item.numItemCode AND bitDefault=1 AND bitIsImage=1),'') AS vcImage,
			ISNULL(vcModelID,'') AS vcModelID,
			ISNULL(txtItemDesc,'') AS txtItemDesc,
			ISNULL(vcWareHouse,'') AS vcWareHouse,
			ISNULL(vcLocation,'') AS vcLocation,
			(CASE WHEN ISNULL(Item.numItemGroup,0) > 0 THEN WareHouseItems.vcWHSKU ELSE Item.vcSKU END) AS vcSKU,
			(CASE WHEN ISNULL(Item.numItemGroup,0) > 0 THEN WareHouseItems.vcBarCode ELSE Item.numBarCodeId END) AS vcUPC,
			CAST(SUM(numUnitHour) AS FLOAT) AS numUnitHour
		FROM
			OpportunityBizDocItems
		INNER JOIN
			WareHouseItems
		ON
			OpportunityBizDocItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
		INNER JOIN
			Warehouses
		ON
			WareHouseItems.numWareHouseID = Warehouses.numWareHouseID
		INNER JOIN
			Item
		ON
			OpportunityBizDocItems.numItemCode = Item.numItemCode
		WHERE
			numOppBizDocID IN ( SELECT * FROM dbo.splitIds(@vcBizDocsIds,','))
		GROUP BY
			Item.numItemCode,
			vcItemName,
			vcModelID,
			txtItemDesc,
			vcWareHouse,
			vcLocation,
			(CASE WHEN ISNULL(Item.numItemGroup,0) > 0 THEN WareHouseItems.vcWHSKU ELSE Item.vcSKU END),
			(CASE WHEN ISNULL(Item.numItemGroup,0) > 0 THEN WareHouseItems.vcBarCode ELSE Item.numBarCodeId END)
		ORDER BY
			vcWareHouse ASC,
			vcLocation ASC,
			vcItemName ASC
	END
	-- GROUP LIST
	ELSE IF @tintMode = 2
	BEGIN
		SELECT
			ISNULL(vcItemName,'') AS vcItemName,
			ISNULL((SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numDomainId=@numDomainID AND ItemImages.numItemCode=Item.numItemCode AND bitDefault=1 AND bitIsImage=1),'') AS vcImage,
			ISNULL(vcModelID,'') AS vcModelID,
			ISNULL(txtItemDesc,'') AS txtItemDesc,
			OpportunityBizDocs.numOppBizDocsId,
			(CASE WHEN ISNULL(Item.numItemGroup,0) > 0 THEN WareHouseItems.vcWHSKU ELSE Item.vcSKU END) AS vcSKU,
			CAST(numUnitHour AS FLOAT) AS numUnitHour
		FROM
			OpportunityBizDocs
		INNER JOIN
			OpportunityBizDocItems
		ON
			OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
		INNER JOIN
			WareHouseItems
		ON
			OpportunityBizDocItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
		INNER JOIN
			Warehouses
		ON
			WareHouseItems.numWareHouseID = Warehouses.numWareHouseID
		INNER JOIN
			Item
		ON
			OpportunityBizDocItems.numItemCode = Item.numItemCode
		WHERE
			numOppBizDocID IN ( SELECT * FROM dbo.splitIds(@vcBizDocsIds,','))
		ORDER BY
			vcWareHouse ASC,
			vcLocation ASC,
			vcItemName ASC
	END
END
--Created by anoop jayaraj

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetPOItemsForFulfillment')
DROP PROCEDURE USP_GetPOItemsForFulfillment
GO
CREATE PROCEDURE [dbo].[USP_GetPOItemsForFulfillment]
@numDomainID as numeric(9),
@numUserCntID numeric(9)=0,                                                                                                               
@SortChar char(1)='0',                                                            
 @CurrentPage int,                                                              
 @PageSize int,                                                              
 @TotRecs int output,                                                              
 @columnName as Varchar(50),                                                              
 @columnSortOrder as Varchar(10),
 @Filter AS VARCHAR(30),
 @FilterBy AS int
as


--Create a Temporary table to hold data                                                              
Create table #tempTable ( ID INT IDENTITY PRIMARY KEY, 
    numoppitemtCode numeric(9),
    numOppID  numeric(9),
     vcPOppName varchar(200),
    vcItemName varchar(500),
    vcModelID varchar(200),
    numUnitHour FLOAT,
    numUnitHourReceived FLOAT,
    numOnHand FLOAT,
	numOnOrder FLOAT,
	numAllocation FLOAT,
	numBackOrder FLOAT,
	vcWarehouse varchar(200),
	dtDueDate VARCHAR(25),bitSerialized BIT,numWarehouseItmsID NUMERIC(9),bitLotNo BIT,SerialLotNo VARCHAR(1000),
	fltExchangeRate float,numCurrencyID numeric(9),numDivisionID numeric(9),
	itemIncomeAccount numeric(9),itemInventoryAsset numeric(9),itemCoGs numeric(9),
	DropShip BIT,charItemType CHAR(1),ItemType VARCHAR(50),
	numProjectID numeric(9),numClassID numeric(9),numItemCode numeric(9),monPrice MONEY,bitPPVariance bit,Vendor varchar(max))
	
declare @strSql as varchar(8000)                                                              
set @strSql='Select numoppitemtCode,Opp.numOppID,vcPOppName,OI.vcItemName + '' - ''+ isnull(dbo.fn_GetAttributes(OI.numWarehouseItmsID,0),'''') as vcItemName ,OI.vcModelID,numUnitHour,isnull(numUnitHourReceived,0) as numUnitHourReceived,
isnull(numOnHand,0) as numOnHand,isnull(numOnOrder,0) as numOnOrder,isnull(numAllocation,0) as numAllocation,isnull(numBackOrder,0) as numBackOrder,vcWarehouse + '': '' + isnull(WL.vcLocation,'''') as  vcWarehouse,
[dbo].[FormatedDateFromDate](intPEstimatedCloseDate,'+convert(varchar(20),@numDomainID)+') AS dtDueDate,isnull(I.bitSerialized,0) as bitSerialized,OI.numWarehouseItmsID,isnull(I.bitLotNo,0) as bitLotNo
,SUBSTRING(
(SELECT '','' + vcSerialNo + CASE WHEN isnull(I.bitLotNo,0)=1 THEN '' ('' + CONVERT(VARCHAR(15),whi.numQty) + '')'' ELSE '''' end 
FROM OppWarehouseSerializedItem oppI JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID=whi.numWareHouseItmsDTLID where oppI.numOppID=OI.numOppId and oppI.numOppItemID=OI.numoppitemtCode 
ORDER BY vcSerialNo FOR XML PATH('''')),2,200000) AS SerialLotNo,
ISNULL(Opp.fltExchangeRate,0) AS fltExchangeRate,ISNULL(Opp.numCurrencyID,0) AS numCurrencyID,ISNULL(Opp.numDivisionID,0) AS numDivisionID,
isnull(I.numIncomeChartAcntId,0) as itemIncomeAccount,isnull(I.numAssetChartAcntId,0) as itemInventoryAsset,isnull(I.numCOGsChartAcntId,0) as itemCoGs,
isnull(OI.bitDropShip,0) as DropShip,I.charItemType,OI.vcType ItemType,ISNULL(OI.numProjectID, 0) numProjectID,ISNULL(OI.numClassID, 0) numClassID,
OI.numItemCode,OI.monPrice,isnull(Opp.bitPPVariance,0) as bitPPVariance,
ISNULL(com.vcCompanyName,''-'') as Vendor

from OpportunityMaster Opp
Join OpportunityItems OI On OI.numOppId=Opp.numOppId
Join Item I on I.numItemCode=OI.numItemCode
Left Join WareHouseItems WI on OI.numWarehouseItmsID=WI.numWareHouseItemID
Left Join Warehouses W on W.numWarehouseID=WI.numWarehouseID
LEFT join divisionmaster div on Opp.numDivisionId=div.numDivisionId 
LEFT join companyInfo com  on com.numCompanyid=div.numcompanyID
left join WarehouseLocation WL on WL.numWLocationID =WI.numWLocationID
where tintOppType=2 and tintOppstatus=1 and ISNULL(bitStockTransfer,0)=0 and tintShipped=0 and numUnitHour-isnull(numUnitHourReceived,0)>0  
AND I.[charItemType] IN (''P'',''N'',''S'') and (OI.bitDropShip=0 or OI.bitDropShip is null) and Opp.numDomainID='+convert(varchar(20),@numDomainID) 

--OI.[numWarehouseItmsID] IS NOT NULL
if @SortChar<>'0' set @strSql=@strSql + ' And Opp.vcPOppName like '''+@SortChar+'%'''                                                      

IF @FilterBy=1 --Item
	set	@strSql=@strSql + ' and OI.vcItemName like ''%' + @Filter + '%'''
ELSE IF @FilterBy=2 --Warehouse
	set	@strSql=@strSql + ' and W.vcWareHouse like ''%' + @Filter + '%'''
ELSE IF @FilterBy=3 --Purchase Order
	set	@strSql=@strSql + ' and Opp.vcPOppName like ''%' + @Filter + '%'''
ELSE IF @FilterBy=4 --Vendor
	set	@strSql=@strSql + ' and com.vcCompanyName LIKE ''%' + @Filter + '%'''		
ELSE IF @FilterBy=5 --BizDoc
	set	@strSql=@strSql + ' and Opp.numOppID in (SELECT numOppId FROM OpportunityBizDocs WHERE vcBizDocID like ''%' + @Filter + '%'')'

If @columnName = 'vcModelID' 
	set @columnName='OI.vcModelID'
								
set @strSql=@strSql + ' ORDER BY  ' + @columnName +' '+ @columnSortOrder

PRINT @strSql

insert into #tempTable(numoppitemtCode,
    numOppID ,
    vcPOppName,
    vcItemName,
    vcModelID,
    numUnitHour,
    numUnitHourReceived,
    numOnHand,
	numOnOrder,
	numAllocation,
	numBackOrder,
	vcWarehouse,
	dtDueDate,bitSerialized,numWarehouseItmsID,bitLotNo,SerialLotNo,fltExchangeRate,numCurrencyID,numDivisionID,
	itemIncomeAccount,itemInventoryAsset,itemCoGs,DropShip,charItemType,ItemType,numProjectID,numClassID,numItemCode,monPrice,bitPPVariance,Vendor)                                                              
exec (@strSql) 


declare @firstRec as integer                                                
declare @lastRec as integer                                                              
set @firstRec= (@CurrentPage-1) * @PageSize                                                              
set @lastRec= (@CurrentPage*@PageSize+1)                                           
set @TotRecs=(select count(*) from #tempTable) 


select *  from #tempTable where ID>@firstRec and ID < @lastRec

drop table #tempTable
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetPriceLevelItemPrice')
DROP PROCEDURE USP_GetPriceLevelItemPrice
GO
CREATE PROCEDURE [dbo].[USP_GetPriceLevelItemPrice]                                        
@numItemCode as numeric(9),
@numDomainID as numeric(9)=0,            
--@numPricingID as numeric(9)=0,
@numWareHouseItemID as numeric(9),
@numDivisionID as numeric(9)=0                  
as                 
BEGIN
	DECLARE @monListPrice AS MONEY;	SET @monListPrice=0
	DECLARE @monVendorCost AS MONEY;	SET @monVendorCost=0

	IF((@numWareHouseItemID>0) and exists(select * from item where numItemCode=@numItemCode and bitSerialized=0 and  charItemType='P'))      
	BEGIN      
		SELECT 
			@monListPrice=ISNULL(monWListPrice,0) 
		FROM 
			WareHouseItems 
		WHERE 
			numWareHouseItemID=@numWareHouseItemID      
		
		IF @monListPrice=0 
		BEGIN
			SELECT @monListPrice=monListPrice FROM Item WHERE numItemCode=@numItemCode       
		END
	END      
	ELSE      
	BEGIN      
		 select @monListPrice=monListPrice from Item where numItemCode=@numItemCode      
	END 

	SELECT @monVendorCost = dbo.[fn_GetVendorCost](@numItemCode)

	SELECT  
		[numPricingID],
		[numPriceRuleID],
		[intFromQty],
		[intToQty],
		[tintRuleType],
		[tintDiscountType],
        CASE 
			WHEN tintRuleType=1 AND tintDiscountType=1 --Deduct from List price & Percentage
				THEN @monListPrice - (@monListPrice * ( decDiscount /100))
            WHEN tintRuleType=1 AND tintDiscountType=2 --Deduct from List price & Flat discount
				THEN @monListPrice - decDiscount
            WHEN tintRuleType=2 AND tintDiscountType=1 --Add to Primary Vendor Cost & Percentage
				THEN @monVendorCost + (@monVendorCost * ( decDiscount /100))
            WHEN tintRuleType=2 AND tintDiscountType=2 --Add to Primary Vendor Cost & Flat discount
				THEN @monVendorCost + decDiscount
            WHEN tintRuleType=3 --Named Price
				THEN decDiscount
        END AS decDiscount
    FROM
		[PricingTable]
    WHERE
		ISNULL(numItemCode,0)=@numItemCode
    ORDER BY 
		[numPricingID] 
  
	DECLARE @numRelationship AS NUMERIC(9)
	DECLARE @numProfile AS NUMERIC(9)

	SELECT 
		@numRelationship=numCompanyType,
		@numProfile=vcProfile 
	FROM 
		DivisionMaster D                  
	JOIN 
		CompanyInfo C 
	ON 
		C.numCompanyId=D.numCompanyID
	WHERE 
		numDivisionID =@numDivisionID       

  
	SELECT 
		PT.numPricingID,
		PT.numPriceRuleID,
		PT.intFromQty,
		PT.intToQty,
		PT.tintRuleType,
		PT.tintDiscountType,
		CASE 
			WHEN PT.tintRuleType=1 AND PT.tintDiscountType=1 --Deduct from List price & Percentage
				THEN @monListPrice - (@monListPrice * ( PT.decDiscount /100))
            WHEN PT.tintRuleType=1 AND PT.tintDiscountType=2 --Deduct from List price & Flat discount
				THEN @monListPrice - PT.decDiscount
            WHEN PT.tintRuleType=2 AND PT.tintDiscountType=1 --Add to Primary Vendor Cost & Percentage
				THEN @monVendorCost + (@monVendorCost * ( PT.decDiscount /100))
            WHEN PT.tintRuleType=2 AND PT.tintDiscountType=2 --Add to Primary Vendor Cost & Flat discount
				THEN @monVendorCost + PT.decDiscount 
		END AS decDiscount
	FROM 
		Item I
    JOIN 
		PriceBookRules P 
	ON 
		I.numDomainID = P.numDomainID
    LEFT JOIN 
		PriceBookRuleDTL PDTL 
	ON 
		P.numPricRuleID = PDTL.numRuleID
    LEFT JOIN 
		PriceBookRuleItems PBI 
	ON 
		P.numPricRuleID = PBI.numRuleID
    LEFT JOIN 
		[PriceBookPriorities] PP 
	ON 
		PP.[Step2Value] = P.[tintStep2] 
		AND PP.[Step3Value] = P.[tintStep3]
    JOIN 
		[PricingTable] PT 
	ON 
		P.numPricRuleID = PT.numPriceRuleID
	WHERE
		I.numItemCode = @numItemCode 
		AND P.tintRuleFor=1 
		AND P.tintPricingMethod = 1
		AND 
		(
		((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 1
		OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND (tintStep3= 1 AND PDTL.numValue = @numDivisionID)) -- Priority 2
		OR ((tintStep2 = 3) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 3

		OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 4
		OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 5
		OR ((tintStep2 = 3) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 6

		OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 3)) -- Priority 7
		OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND (tintStep3 = 3)) -- Priority 8
		OR ((tintStep2 = 3) and (tintStep3 = 3)) -- Priority 9
		)
	ORDER BY PP.Priority ASC
END
      
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetPriceLevelWithWarehouseItemPrice')
DROP PROCEDURE USP_GetPriceLevelWithWarehouseItemPrice
GO
CREATE PROCEDURE [dbo].[USP_GetPriceLevelWithWarehouseItemPrice]                                        
@numItemCode as numeric(9),
@numDomainID as numeric(9)=0,            
@numWareHouseID as numeric(9),
@numDivisionID as numeric(9)=0                  
as                 

DECLARE @numWareHouseItemID AS BIGINT
SELECT TOP 1 @numWareHouseItemID = ISNULL(numWareHouseItemID,0) FROM dbo.WareHouseItems WHERE numWareHouseID = @numWareHouseID AND numItemID = @numItemCode 

SELECT  ISNULL(I.numItemCode,0) AS [numItemCode],
		ISNULL(W.vcWareHouse,'') AS [vcWareHouse],
		--ISNULL(I.monListPrice,ISNULL(WI.monWListPrice,0)) AS decDiscount,
		ISNULL(WI.monWListPrice,0) AS decDiscount,
		CONVERT(VARCHAR(10),W.numWareHouseID) + '~' + CONVERT(VARCHAR(10),WI.numWareHouseItemID) + '~' + CONVERT(VARCHAR(10),ISNULL(monWListPrice,0)) + '~' + ISNULL(W.vcWareHouse,'') AS [KeyValue]
FROM    Item I
LEFT JOIN dbo.WareHouseItems WI ON WI.numItemID = I.numItemCode AND WI.numDomainID = I.numDomainID
LEFT JOIN dbo.Warehouses W ON W.numWareHouseID = WI.numWareHouseID
WHERE ISNULL(I.numItemCode,0)= @numItemCode AND (W.numWareHouseID = ISNULL(@numWareHouseID,0) OR ISNULL(@numWareHouseID,0) = 0)

IF @numWareHouseID > 0
BEGIN

DECLARE @monListPrice AS MONEY;SET @monListPrice=0
DECLARE @monVendorCost AS MONEY;SET @monVendorCost=0

if ((@numWareHouseItemID>0) and exists(select * from item where numItemCode=@numItemCode and charItemType='P'))      
	begin      
		select @monListPrice=isnull(monWListPrice,0) from WareHouseItems where numWareHouseItemID=@numWareHouseItemID      
		if @monListPrice=0 select @monListPrice=monListPrice from Item where numItemCode=@numItemCode       
	end      
	else      
	begin      
		 select @monListPrice=monListPrice from Item where numItemCode=@numItemCode      
	end 

SELECT @monVendorCost = dbo.[fn_GetVendorCost](@numItemCode)

SELECT  [numPricingID],
		[numPriceRuleID],
		[intFromQty],
		[intToQty],
		[tintRuleType],
		[tintDiscountType],
        CASE WHEN tintRuleType=1 AND tintDiscountType=1 --Deduct from List price & Percentage
			THEN @monListPrice - (@monListPrice * ( decDiscount /100))
         WHEN tintRuleType=1 AND tintDiscountType=2 --Deduct from List price & Flat discount
			THEN @monListPrice - decDiscount
         WHEN tintRuleType=2 AND tintDiscountType=1 --Add to Primary Vendor Cost & Percentage
			THEN @monVendorCost + (@monVendorCost * ( decDiscount /100))
         WHEN tintRuleType=2 AND tintDiscountType=2 --Add to Primary Vendor Cost & Flat discount
			THEN @monVendorCost + decDiscount
         WHEN tintRuleType=3 --Named Price
			THEN decDiscount
        END AS decDiscount,ISNULL(W.vcWareHouse,'') AS [vcWareHouse],WI.numWareHouseID,WI.numWareHouseItemID
        FROM    [PricingTable] PT,dbo.WareHouseItems WI JOIN dbo.Warehouses W ON W.numWareHouseID = WI.numWareHouseID
WHERE   ISNULL(PT.numItemCode,0)=@numItemCode AND ISNULL(WI.numItemID,0)= @numItemCode AND (W.numWareHouseID = ISNULL(@numWareHouseID,0) OR ISNULL(@numWareHouseID,0) = 0)
ORDER BY [numPricingID] 
  
  
declare @numRelationship as numeric(9)                  
declare @numProfile as numeric(9)                  

  select @numRelationship=numCompanyType,@numProfile=vcProfile from DivisionMaster D                  
join CompanyInfo C on C.numCompanyId=D.numCompanyID                  
where numDivisionID =@numDivisionID       
  
SELECT  PT.numPricingID,
		PT.numPriceRuleID,
		PT.intFromQty,
		PT.intToQty,
		PT.tintRuleType,
		PT.tintDiscountType,
		CASE WHEN PT.tintRuleType=1 AND PT.tintDiscountType=1 --Deduct from List price & Percentage
				THEN @monListPrice - (@monListPrice * ( PT.decDiscount /100))
             WHEN PT.tintRuleType=1 AND PT.tintDiscountType=2 --Deduct from List price & Flat discount
				THEN @monListPrice - PT.decDiscount
             WHEN PT.tintRuleType=2 AND PT.tintDiscountType=1 --Add to Primary Vendor Cost & Percentage
				THEN @monVendorCost + (@monVendorCost * ( PT.decDiscount /100))
             WHEN PT.tintRuleType=2 AND PT.tintDiscountType=2 --Add to Primary Vendor Cost & Flat discount
				THEN @monVendorCost + PT.decDiscount END AS decDiscount,ISNULL(W.vcWareHouse,'') AS [vcWareHouse],WI.numWareHouseID,WI.numWareHouseItemID
FROM    Item I
        JOIN PriceBookRules P ON I.numDomainID = P.numDomainID
        LEFT JOIN PriceBookRuleDTL PDTL ON P.numPricRuleID = PDTL.numRuleID
        LEFT JOIN PriceBookRuleItems PBI ON P.numPricRuleID = PBI.numRuleID
        LEFT JOIN [PriceBookPriorities] PP ON PP.[Step2Value] = P.[tintStep2] AND PP.[Step3Value] = P.[tintStep3]
        JOIN [PricingTable] PT ON P.numPricRuleID = PT.numPriceRuleID
        ,dbo.WareHouseItems WI JOIN dbo.Warehouses W ON W.numWareHouseID = WI.numWareHouseID        
WHERE   I.numItemCode = @numItemCode AND P.tintRuleFor=1 AND P.tintPricingMethod = 1
AND 
(
((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 1
OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND (tintStep3= 1 AND PDTL.numValue = @numDivisionID)) -- Priority 2
OR ((tintStep2 = 3) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 3

OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 4
OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 5
OR ((tintStep2 = 3) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 6

OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 3)) -- Priority 7
OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND (tintStep3 = 3)) -- Priority 8
OR ((tintStep2 = 3) and (tintStep3 = 3)) -- Priority 9
) AND ISNULL(WI.numItemID,0)= @numItemCode AND (W.numWareHouseID = ISNULL(@numWareHouseID,0) OR ISNULL(@numWareHouseID,0) = 0)
ORDER BY PP.Priority ASC

END 
     
/****** Object:  StoredProcedure [dbo].[usp_GetRMASerializedindItems]    Script Date: 07/26/2008 16:18:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                           
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetRMASerializedindItems')
DROP PROCEDURE USP_GetRMASerializedindItems
GO
CREATE PROCEDURE [dbo].[USP_GetRMASerializedindItems]                            
@numDomainId as numeric(9)=0,      
@numReturnHeaderID as numeric(9)=0,  
@numReturnItemID  as numeric(9)=0
                         
as      
BEGIN
	
DECLARE @tintReturnType TINYINT,@numOppId NUMERIC(18)
SELECT @tintReturnType=tintReturnType,@numOppId=ISNULL(numOppId,0) FROM ReturnHeader WHERE numDomainId=@numDomainId AND numReturnHeaderID=@numReturnHeaderID
	
declare @numWareHouseItemID as numeric(9),@numItemID as numeric(9),@numOppItemCode AS NUMERIC(9),@numUnitHour FLOAT
declare @bitSerialize as BIT,@bitLotNo as BIT                     

declare @str as varchar(2000),@strSQL as varchar(2000),@ColName as varchar(50)  
set @str=''        
      
select @numWareHouseItemID=OI.numWarehouseItmsID,@numItemID=WI.numItemID,@numOppItemCode=OI.numoppitemtCode,@numUnitHour=RI.numUnitHour from 
ReturnItems RI JOIN OpportunityItems OI ON RI.numOppItemID=OI.numoppitemtCode
join WareHouseItems WI on OI.numWarehouseItmsID= WI.numWareHouseItemID      
where  RI.numReturnHeaderID=@numReturnHeaderID AND RI.numReturnItemID=@numReturnItemID    
AND OI.numOppId=@numOppId    
                      
declare @numItemGroupID as numeric(9)                      
                      
select @numItemGroupID=numItemGroup,@bitSerialize=ISNULL(bitSerialized,0),@bitLotNo=ISNULL(bitLotNo,0) 
from Item where numItemCode=@numItemID                      

set @ColName='WareHouseItmsDTL.numWareHouseItmsDTLID,1'                  
            
--Create a Temporary table to hold data                                                          
Create table #tempTable ( ID INT IDENTITY PRIMARY KEY,                                                                    
 numCusFlDItemID numeric(9)                                                       
 )                       
                      
insert into #tempTable                       
(numCusFlDItemID)                                                          
select numOppAccAttrID from ItemGroupsDTL where numItemGroupID=@numItemGroupID and tintType=2     
              
 declare @ID as numeric(9)                      
 declare @numCusFlDItemID as varchar(20)                      
 declare @fld_label as varchar(100),@fld_type as varchar(100)                              
 set @ID=0                      
 select top 1 @ID=ID,@numCusFlDItemID=numCusFlDItemID,@fld_label=fld_label,@fld_type=fld_type from #tempTable                       
 join CFW_Fld_Master on numCusFlDItemID=Fld_ID                                   
 while @ID>0                      
 begin                                    
   set @str=@str+',  dbo.GetCustFldItems('+@numCusFlDItemID+',9,'+@ColName+') as ['+ @fld_label+']'                                      
    
   set @str=@str+',  dbo.GetCustFldItemsValue('+@numCusFlDItemID+',9,'+@ColName+','''+@fld_type+''') as ['+ @fld_label+'Value]'                                        
                    
   select top 1 @ID=ID,@numCusFlDItemID=numCusFlDItemID,@fld_label=fld_label from #tempTable                       
   join CFW_Fld_Master on numCusFlDItemID=Fld_ID and ID >@ID                      
   if @@rowcount=0 set @ID=0                      
                        
 end        
      
select numWareHouseItemID,vcWareHouse,
ISNULL(u.vcUnitName,'') vcBaseUOMName
,@bitSerialize AS bitSerialize,@bitLotNo AS bitLotNo,I.vcItemName,@numUnitHour AS numUnitHour
from OpportunityItems Opp     
join WareHouseItems on Opp.numWarehouseItmsID=WareHouseItems.numWareHouseItemID      
join Warehouses on Warehouses.numWareHouseID=WareHouseItems.numWareHouseID 
 join item I on Opp.numItemCode=I.numItemcode  
 LEFT JOIN  UOM u ON u.numUOMId = I.numBaseUnit       
where Opp.numOppItemTcode=@numOppItemCode    
      
      
      
 set @strSQL='select WID.numWareHouseItmsDTLID,numWareHouseItemID,vcSerialNo,0 as Op_Flag,
  isnull(OSI.numQty,0) 
  as TotalQty,0 as UsedQty
  '+  @str  +',0 as bitAdded
 from OppWarehouseSerializedItem OSI join WareHouseItmsDTL WID     
 on WID.numWareHouseItmsDTLID= OSI.numWarehouseItmsDTLID                           
 where numOppID='+ convert(varchar(15),ISNULL(@numOppID,0))+' and numWareHouseItemID='+ convert(varchar(15),ISNULL(@numWareHouseItemID,0)) 
 print @strSQL                     
 exec (@strSQL)     
  
  
select Fld_label,fld_id,fld_type,numlistid,vcURL from #tempTable                       
 join CFW_Fld_Master on numCusFlDItemID=Fld_ID                    
drop table #tempTable   
  
END
GO

/****** Object:  StoredProcedure [dbo].[USP_GetSalesFulfillment]    Script Date: 05/15/2009 08:05:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
declare @p8 int
set @p8=63
exec USP_GetSalesFulfillment @numUserCntID=17,@numDomainID=72,@tintSortOrder=1,@SortChar='0',@CustName='',@CurrentPage=1,@PageSize=30,@TotRecs=@p8 
output,@columnName='opp.bintCreatedDate',@columnSortOrder='Desc',@ClientTimeZoneOffset=-330
select @p8 
*/
--created by anoop jayaraj                                              
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getsalesfulfillment')
DROP PROCEDURE usp_getsalesfulfillment
GO
CREATE PROCEDURE [dbo].[USP_GetSalesFulfillment] 
 @numUserCntID numeric(9)=0,                                                              
 @numDomainID numeric(9)=0,                                                              
 @tintSortOrder tinyint=0,                                                            
 @SortChar char(1)='0',                                                                                                                           
 @CustName varchar(100)='' ,                                                                    
 @CurrentPage int,                                                              
 @PageSize int,                                                              
 @TotRecs int output,                                                              
 @columnName as Varchar(50),                                                              
 @columnSortOrder as Varchar(10),                                                                                     
 @ClientTimeZoneOffset as INT,
 @numOrderStatus NUMERIC(9)                 
as                                              
                                                            
Create table #tempTable ( ID INT IDENTITY PRIMARY KEY, 
    numOppID numeric(9),
    numoppitemtCode numeric(9),
    Fulfilled numeric(9),
    QtyToFulfill numeric(9),
    bintCreatedDate datetime,
    Allocation FLOAT,
    OnHand FLOAT,
    numWareHouseItemID numeric(9),
    AmtPaid MONEY,bitSerialized BIT,bitLotNo BIT,numShipVia NUMERIC(9),QtyShipped numeric(9),bitKitParent bit
--    numOppBizDocsId numeric(9)
    )                                              
                                              
            
      
                                                             
declare @strSql as varchar(8000)                                                              
set @strSql='Select numOppID,numoppitemtCode,Fulfilled,numUnitHour- Fulfilled as QtyToFulfill,bintCreatedDate,numAllocation as Allocation,numOnHand,numWareHouseItemID,AmtPaid,bitSerialized,bitLotNo,numShipVia,QtyShipped,bitKitParent from (
   SELECT  Opp.numOppID,OppItems.numoppitemtCode,Opp.bintCreatedDate,isnull(sum(OppBizItems.numUnitHour),0) as Fulfilled,isnull(SUM(OppBiz.monAmountPaid),0) AS AmtPaid,
  OppItems.numUnitHour  ,numAllocation ,numOnHand,numWareHouseItemID,isnull(I.bitSerialized,0) as bitSerialized,isnull(I.bitLotNo,0) as bitLotNo,OppBiz.numShipVia,
  Case when Opp.tintShipped=1 then isnull(sum(OppItems.numUnitHour),0) else isnull(sum(OppItems.numQtyShipped),0) end as QtyShipped,
  CASE WHEN ISNULL(bitKitParent,0) = 1
                               AND ISNULL(bitAssembly,0) = 1 THEN 0
                          WHEN ISNULL(bitKitParent,0) = 1 THEN 1
                          ELSE 0
                     END as bitKitParent
  FROM OpportunityMaster Opp 
  INNER JOIN AdditionalContactsInformation ADC                                                               
  ON Opp.numContactId = ADC.numContactId                                                               
  INNER JOIN DivisionMaster Div                                                               
  ON Opp.numDivisionId = Div.numDivisionID AND ADC.numDivisionId = Div.numDivisionID                                                               
  INNER JOIN CompanyInfo C                                                               
  ON Div.numCompanyID = C.numCompanyId                                  
  left join AdditionalContactsInformation ADC1 
  on ADC1.numContactId=Opp.numRecOwner
   left join OpportunityBizDocs OppBiz 
  on Opp.numOppID=OppBiz.numOppID and OppBiz.numBizDocId=(select numAuthoritativeSales from AuthoritativeBizDocs where numDomainID=Div.numDomainID ) 
  Join  OpportunityItems OppItems
  on   OppItems.numOppID= Opp.numOppID
  left Join  OpportunityBizDocItems OppBizItems
  on  OppBizItems.numOppItemID= OppItems.numoppitemtCode and OppBiz.numOppBizDocsId= OppBizItems.numOppBizDocID                                                   
  left join WareHouseItems WI
  on WI.numWareHouseItemID=OppItems.numWarehouseItmsID 
  LEFT JOIN [Item] I ON I.[numItemCode] = OppItems.[numItemCode]
 WHERE Opp.tintOppstatus=1 and  Opp.tintOppType=1 and (WI.numWareHouseID in (select numWarehouseID from WareHouseForSalesFulfillment where numDomainID='+ convert(varchar(15),@numDomainID) +') or WI.numWareHouseID is null) 
  AND I.[charItemType]=''P'' and (OppItems.bitDropShip=0 or OppItems.bitDropShip is null)
  and Div.numDomainID=' + convert(varchar(15),@numDomainID)+''                       
  
if @tintSortOrder != 4  
	SET @strSql =@strSql + ' and Opp.tintShipped=0 '

                                  
if @CustName<>'' set @strSql=@strSql+'and C.vcCompanyName like '''+@CustName+'%'''
if @numOrderStatus<>0 set @strSql=@strSql+'and Opp.numStatus ='+ CONVERT(VARCHAR(10),@numOrderStatus)
                                                             
if @SortChar<>'0' set @strSql=@strSql + ' And Opp.vcPOppName like '''+@SortChar+'%'''                                                      


    
--if @columnName = 'OppBiz.dtCreatedDate'                                                                               
-- set @strSql=@strSql + ' ORDER BY  ' + @columnName +' '+ @columnSortOrder                                                                             
 set @strSql=@strSql + ' group by  numWareHouseItemID,numAllocation,numOnHand,Opp.bintCreatedDate,OppItems.numUnitHour,Opp.numOppID,OppItems.numoppitemtCode,I.bitSerialized,I.bitLotNo,OppBiz.numShipVia,OppItems.numQtyShipped,Opp.tintShipped,I.bitKitParent,I.bitAssembly)X where '

if @tintSortOrder=3 
BEGIN
   SET @strSql=@strSql + '  X.QtyShipped = 0'
END
ELSE if @tintSortOrder=4 
BEGIN
   SET @strSql=@strSql + '  X.QtyShipped > 0'
END
else 
BEGIN
DECLARE @strOperator VARCHAR(5)
 if @tintSortOrder=1  
	set @strOperator = '='
 ELSE 
	set @strOperator = '>' 

set @strSql=@strSql + ' X.numUnitHour-X.Fulfilled'+ @strOperator +'0' 
END

-- OLD filters
-- if @tintSortOrder=1  set @strSql=@strSql + '  AND isnull(numAllocation,0)>=numUnitHour- Fulfilled'        
-- if @tintSortOrder=2  set @strSql=@strSql + '  AND numWareHouseItemID>0' 
-- if @tintSortOrder=3  set @strSql=@strSql + '  AND isnull(numAllocation,0)<numUnitHour- Fulfilled'        
-- if @tintSortOrder=4  set @strSql=@strSql + '  AND ( (isnull(numUnitHour,0)< isnull(numAllocation,0)) or (isnull(numUnitHour,0)< isnull(numOnHand,0)) ) '

 if @tintSortOrder=2  set @strSql=@strSql + '  AND X.Fulfilled >0' 
 if @tintSortOrder=4 or @tintSortOrder=3  set @strSql=@strSql + '  AND (SELECT COUNT(*) FROM [OpportunityBizDocItems] WHERE [numOppItemID]=X.numoppitemtCode)=0'
--Sort Newest to oldest
	--set @strSql=@strSql + ' Order by bintCreatedDate desc'
       
print @strSql   
                                                         
insert into #tempTable(numOppId,numoppitemtCode,Fulfilled ,QtyToFulfill,bintCreatedDate,Allocation,OnHand,numWareHouseItemID,AmtPaid,bitSerialized,bitLotNo/*,numOppBizDocsId*/,numShipVia,QtyShipped,bitKitParent)
exec (@strSql)                                                               
                    
                  
                  
                                                            
declare @firstRec as integer                                                
declare @lastRec as integer                                                              
set @firstRec= (@CurrentPage-1) * @PageSize                                                              
set @lastRec= (@CurrentPage*@PageSize+1)                                           
set @TotRecs=(select count(*) from #tempTable)                                                
                    
declare @tintOrder as tinyint                                                          
declare @vcFieldName as varchar(50)             
declare @vcListItemType as varchar(3)                                                     
declare @vcListItemType1 as varchar(1)                                                         
declare @vcAssociatedControlType varchar(10)                                                          
declare @numListID AS numeric(9)                                                          
declare @vcDbColumnName varchar(40)                              
declare @WhereCondition varchar(2000)                               
declare @vcLookBackTableName varchar(2000)                        
 Declare @bitCustom as bit                
Declare @numFieldId as numeric  
DECLARE @bitAllowSorting AS CHAR(1)   
Declare @bitAllowEdit as bit
 Declare @ListRelID as numeric(9) 

set @tintOrder=0                                                          
set @WhereCondition =''                         
                           
declare @Nocolumns as tinyint                        
set @Nocolumns=0               

select @Nocolumns=isnull(sum(TotalRow),0)  from(            
Select count(*) TotalRow from View_DynamicColumns where numFormId=23 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1
Union 
Select count(*) TotalRow from View_DynamicCustomColumns where numFormId=23 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1) TotalRows

declare @DefaultNocolumns as tinyint                     
set @DefaultNocolumns=  @Nocolumns                       
declare @strColumns as varchar(2000)                        
set @strColumns=''    

CREATE TABLE #tempForm (tintOrder TINYINT,vcDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),
  vcListItemType char(3),numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,
  numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,bitIsAlphaNumeric BIT,bitIsNumeric BIT,
  bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),ListRelID numeric(9))
                     
while @DefaultNocolumns>0                        
begin                        
                        
 set @strColumns=@strColumns+',null'                        
 set @DefaultNocolumns=@DefaultNocolumns-1                        
end                  
 set @strSql = ''                    
set @strSql=' select ADC.numContactId,DM.numDivisionID,isnull(DM.numTerID,0) numTerID,opp.numRecOwner,DM.tintCRMType,opp.numOppId,OppItems.numoppitemtCode,Case when Opp.tintShipped=1 then isnull(OppItems.numUnitHour,0) else isnull(OppItems.numQtyShipped,0) end as numQtyShipped,Fulfilled,isnull(WI.numWareHouseItemID,0) numWareHouseItemID,T.bitSerialized,T.bitLotNo,Item.numItemCode,cast(OppItems.numUnitHour as numeric(9)) as QtyOrdered,T.bitKitParent'
     
if @Nocolumns > 0                    
begin  
INSERT INTO #tempForm
select tintRow+1 as tintOrder,vcDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
 vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName                                                 
,bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID 
 FROM View_DynamicColumns 
 where numFormId=23 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1
 
 
       UNION
    
     select tintRow+1 as tintOrder,vcDbColumnName,vcFieldName,                  
 vcAssociatedControlType,'' as vcListItemType,numListID,''                                                 
,bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID
 from View_DynamicCustomColumns
 where numFormId=23 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1
 AND ISNULL(bitCustom,0)=1
 
  order by tintOrder asc     
                         
end                    
else                    
begin                    
 
INSERT INTO #tempForm
select tintOrder,vcDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
 vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName                                                 
,bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID 
 FROM View_DynamicDefaultColumns
 where numFormId=23 and bitDefault=1 AND ISNULL(bitSettingField,0)=1 and numDomainID=@numDomainID
order by tintOrder asc   
end                                                    
                    set @DefaultNocolumns=  @Nocolumns                      
        

    select top 1 @tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,                
  @vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,@numListID=numListID,@vcLookBackTableName=vcLookBackTableName                                               
,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,@ListRelID=ListRelID
  from  #tempForm order by tintOrder asc            
                                              
while @tintOrder>0                                                          
begin     

  print @tintOrder                                                      
     if @bitCustom = 0              
 begin                                                     
                declare @Prefix as varchar(5)                        
       if @vcLookBackTableName = 'AdditionalContactsInformation'  set @Prefix = 'ADC.'                        
       if @vcLookBackTableName = 'DivisionMaster'  set @Prefix = 'DM.'                        
       if @vcLookBackTableName = 'OpportunityMaster' set @PreFix ='Opp.'                                               
	   if @vcLookBackTableName = 'Warehouses' set @PreFix ='W.'                                               

 if @vcAssociatedControlType='SelectBox'                                                          
        begin                                                          
                                                                          
     if @vcListItemType='LI'              
     begin                                                          
      set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcFieldName+'~'+ @vcDbColumnName+'~'+ @bitAllowSorting+']'                                                          
      set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName                                                          
     end                                                          
     else if @vcListItemType='S'                                                           
     begin                                                          
      set @strSql=@strSql+',S'+ convert(varchar(3),@tintOrder)+'.vcState'+' ['+ @vcFieldName+'~'+ @vcDbColumnName+'~'+ @bitAllowSorting+']'                                                          
      set @WhereCondition= @WhereCondition +' left join State S'+ convert(varchar(3),@tintOrder)+ ' on S'+convert(varchar(3),@tintOrder)+ '.numStateID='+@vcDbColumnName                                                          
     end                                                          
     else if @vcListItemType='T'                                                           
     begin                                                          
      set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcFieldName+'~'+ @vcDbColumnName+'~'+ @bitAllowSorting+']'                                                          
      set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName                                                          
     end                         
     else if   @vcListItemType='U'                                                       
    begin                         
                        
                         
    set @strSql=@strSql+',dbo.fn_GetContactName('+@Prefix+@vcDbColumnName+') ['+ @vcFieldName+'~'+ @vcDbColumnName+'~'+ @bitAllowSorting+']'                                                          
    end                        
    end                  
    else if @vcAssociatedControlType='DateField'                                                        
   begin                  
     set @strSql=@strSql+',case when convert(varchar(11),'+@Prefix+@vcDbColumnName+')= convert(varchar(11),getdate()) then ''<b><font color=red>Today</font></b>'''          
     set @strSql=@strSql+'when convert(varchar(11),'+@Prefix+@vcDbColumnName+')= convert(varchar(11),dateadd(day,-1,getdate())) then ''<b><font color=purple>YesterDay</font></b>'''          
     set @strSql=@strSql+'when convert(varchar(11),'+@Prefix+@vcDbColumnName+')= convert(varchar(11),dateadd(day,1,getdate())) then''<b><font color=orange>Tommorow</font></b>'' '          
     set @strSql=@strSql+'else dbo.FormatedDateFromDate('+@Prefix+@vcDbColumnName+','+convert(varchar(10),@numDomainId)+') end  ['+ @vcFieldName+'~'+ @vcDbColumnName+'~'+ @bitAllowSorting+']'                   
         
   end                
    else if @vcAssociatedControlType='TextBox'                                                        
   begin                 
    set @strSql=@strSql+','+ case                      
    when @vcDbColumnName='numAge' then 'year(getutcdate())-year(bintDOB)'               
    when @vcDbColumnName='monPAmount' then 'isnull((SELECT varCurrSymbol FROM [Currency] WHERE numCurrencyID = Opp.[numCurrencyID]),'''') + '' ''+ CONVERT(VARCHAR,CONVERT(MONEY,monPAmount),1)'
    when @vcDbColumnName='numDivisionID' then 'DM.numDivisionID'
    WHEN @vcDbColumnName='OrderAmt' then 'monPAmount'
    WHEN @vcDbColumnName='AmtPaid' then 'AmtPaid'
    WHEN @vcDbColumnName='vcItemName' then 'OppItems.vcItemName'
    WHEN @vcDbColumnName='vcModelID' then 'OppItems.vcModelID'
    WHEN @vcDbColumnName='vcItemDesc' then 'OppItems.vcItemDesc'
	WHEN @vcDbColumnName='vcPOppName' then 'Opp.vcPOppName + '' ('' + dbo.fn_GetListItemName(isnull(Opp.numStatus,0)) + '')'' '
	WHEN @vcDbColumnName='numInTransit' then 'dbo.fn_GetItemTransitCount(Item.numItemCode,Opp.numDomainID)'
    WHEN @vcDbColumnName='numQtyShipped' then 'Case when Opp.tintShipped=1 then isnull(OppItems.numUnitHour,0) else isnull(OppItems.numQtyShipped,0) end'
    
    WHEN @vcDbColumnName='numOnHand' then 'Case when T.bitKitParent=1 then dbo.fn_GetKitInventory(OppItems.numItemCode) else isnull(WI.numOnHand,0) END '
    WHEN @vcDbColumnName='numOnOrder' then 'Case when T.bitKitParent=1 then 0 else isnull(WI.numOnOrder,0) END '
    WHEN @vcDbColumnName='numAllocation' then 'Case when T.bitKitParent=1 then 0 else isnull(WI.numAllocation,0) END '
    WHEN @vcDbColumnName='numBackOrder' then 'Case when T.bitKitParent=1 then 0 else isnull(WI.numBackOrder,0) END '
    
    else @vcDbColumnName end+' ['+ @vcFieldName+'~'+ @vcDbColumnName+'~'+ @bitAllowSorting+']'                  
   end  
   else if  @vcAssociatedControlType='TextArea'                                              
   begin  
		set @strSql=@strSql+', '''' ' +' ['+ @vcFieldName+'~'+ @vcDbColumnName+'~'+ @bitAllowSorting+']'            
   end        
      

                                                      
  end              
Else                                                          
 Begin              
                 
   select @vcFieldName=FLd_label,@vcAssociatedControlType=fld_type,@vcDbColumnName='Cust'+Convert(varchar(10),Fld_Id)  from CFW_Fld_Master                                               
   where  CFW_Fld_Master.Fld_Id = @numFieldId                                          
                            
   if @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'            
   begin          
             
    set @strSql= @strSql+',CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value  ['+ @vcFieldName+'~'+ @vcDbColumnName+'~0]'                 
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Opp CFW'+ convert(varchar(3),@tintOrder)+ '               
    on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=Opp.numOppid   '                                                       
   end              
                 
    else if @vcAssociatedControlType = 'CheckBox'             
   begin          
             
    set @strSql= @strSql+',case when isnull(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,0)=0 then ''No'' when isnull(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,0)=1 then ''Yes'' end   ['+ @vcFieldName+'~'+ @vcDbColumnName+'~0]'             
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Opp CFW'+ convert(varchar(3),@tintOrder)+ '           
    on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=Opp.numOppid   '                                                   
   end          
   else if @vcAssociatedControlType = 'DateField'          
   begin               
                 
    set @strSql= @strSql+',dbo.FormatedDateFromDate(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,'+convert(varchar(10),@numDomainId)+')  ['+ @vcFieldName+'~'+ @vcDbColumnName+'~0]'                 
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Opp CFW'+ convert(varchar(3),@tintOrder)+ '               
    on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=Opp.numOppid    '                                                       
   end              
    else if @vcAssociatedControlType = 'SelectBox'                
   begin            
    set @vcDbColumnName = 'DCust'+Convert(varchar(10),@numFieldId)              
    set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcFieldName+'~'+ @vcDbColumnName+'~0]'                                             
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Opp CFW'+ convert(varchar(3),@tintOrder)+ '               
     on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=Opp.numOppid     '                                                       
    set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value'              
   end                 
 End                                        
                           
     select top 1 @tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,                
  @vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,@numListID=numListID,@vcLookBackTableName=vcLookBackTableName                                               
,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,@ListRelID=ListRelID
  from #tempForm WHERE tintOrder > @tintOrder-1 order by tintOrder asc            
 
  if @@rowcount=0 set @tintOrder=0 
                
end                               
                          
   IF @columnName='bintCreatedDate'
	SET @columnName='opp.bintCreatedDate'                        
                  
                                            
 set @strSql=@strSql+'                                                            
  FROM OpportunityMaster Opp     
  INNER JOIN AdditionalContactsInformation ADC  ON Opp.numContactId = ADC.numContactId                                                               
  INNER JOIN DivisionMaster DM  ON Opp.numDivisionId = DM.numDivisionID AND ADC.numDivisionId = DM.numDivisionID                                                               
   join #tempTable T on T.numOppId=Opp.numOppId                                                          
  INNER JOIN CompanyInfo C                                                               
  ON DM.numCompanyID = C.numCompanyId '+@WhereCondition+  '                       
  Join OpportunityItems OppItems
  on   OppItems.numoppitemtCode=T.numoppitemtCode
  left join WareHouseItems WI
  on WI.numWareHouseItemID=OppItems.numWarehouseItmsID 
  left join Warehouses W on W.numWareHouseID=WI.numWareHouseID
  Join Item
  on Item.numItemCode= OppItems.numItemCode                                       
  WHERE ID > '+convert(varchar(10),@firstRec)+ ' and ID <'+ convert(varchar(10),@lastRec)+' and opp.numOppId not in (SELECT numOppId FROM WorkOrder WHERE numDomainID='+ convert(varchar(15),@numDomainID) +' AND numWOStatus!=23184 AND numOppId IS NOT null) '
set @strSql=@strSql + ' ORDER BY  ' + @columnName +' '+ @columnSortOrder                   
                    
print @strSql                  
exec (@strSql)                                            
                          
drop table #tempTable
/****** Object:  StoredProcedure [dbo].[usp_GetTaxDetails]    Script Date: 07/26/2008 16:18:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_gettaxdetails')
DROP PROCEDURE usp_gettaxdetails
GO
CREATE PROCEDURE [dbo].[usp_GetTaxDetails]  
@numDomainId as numeric(9),
@numTaxItemID as numeric(9)=0,
@numCountry as numeric(9)=0,  
@numState as numeric(9)=0  
as   
  
select  
numTaxID,  
[dbo].[fn_GetListName](numCountryID,0) as country,  
case when numStateID = 0 then 'All' else [dbo].[fn_GetState](numStateID) end as [state] ,  
decTaxPercentage, Case when TaxDetails.numTaxItemID=0 or TaxDetails.numTaxItemID is null  then 'Sales Tax(Default)' else isnull(vcTaxName,'') end as vcTaxName
,isnull(vcCity,'') as vcCity,isnull(vcZipPostal,'') as vcZipPostal,
CASE WHEN tintTaxType = 2 THEN CONCAT(FORMAT(decTaxPercentage,'##0.0000'),' (Flat Amount)') ELSE CONCAT(FORMAT(decTaxPercentage,'##0.0000'),' %') END AS vcTax,
tintTaxType
from TaxDetails
left join TaxItems
on TaxItems.numTaxItemID=TaxDetails.numTaxItemID
where TaxDetails.numdomainid = @numDomainId 
and (TaxDetails.numTaxItemID=@numTaxItemID or @numTaxItemID=0)
and (TaxDetails.numCountryID=@numCountry or @numCountry=0)
and (TaxDetails.numStateID=@numState or @numState=0)
GO
/****** Object:  StoredProcedure [dbo].[USP_GetTaxPercentage]    Script Date: 07/26/2008 16:18:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---created by anoop jayaraj

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_gettaxpercentage')
DROP PROCEDURE usp_gettaxpercentage
GO
CREATE PROCEDURE [dbo].[USP_GetTaxPercentage]
@DivisionID as numeric(9)=0,
@numBillCountry as numeric(9)=0,
@numBillState  as numeric(9)=0,
@numDomainID as numeric(9),
@numTaxItemID as numeric(9),
@tintBaseTaxCalcOn TINYINT=2, -- Base tax calculation on Shipping Address(2) or Billing Address(1)
@tintBaseTaxOnArea TINYINT=0,
@vcCity varchar(100),
@vcZipPostal varchar(20)
as

  
DECLARE @TaxPercentage as float 
DECLARE @tintTaxType AS TINYINT

declare @bitTaxApplicable as bit 
set @TaxPercentage=0 
 
if @DivisionID>0 --Existing Customer
BEGIN
	IF @tintBaseTaxCalcOn = 1 --Billing Address
			select @numBillState=isnull(numState,0), @numBillCountry=isnull(numCountry,0),@vcCity=isnull(vcCity,''),@vcZipPostal=isnull(vcPostalCode,'')  from DivisionMaster DM LEFT JOIN dbo.AddressDetails AD1 ON AD1.numDomainID=DM.numDomainID 
			AND AD1.numRecordID=DM.numDivisionID AND AD1.tintAddressOf=2 AND AD1.tintAddressType=1 where numDivisionID=@DivisionID
	ELSE --Shipping Address
			select @numBillState=isnull([numState],0), @numBillCountry=isnull([numCountry],0),@vcCity=isnull(vcCity,''),@vcZipPostal=isnull(vcPostalCode,'')  from DivisionMaster DM  LEFT JOIN dbo.AddressDetails AD2 ON AD2.numDomainID=DM.numDomainID 
			AND AD2.numRecordID= DM.numDivisionID AND AD2.tintAddressOf=2 AND AD2.tintAddressType=2 AND AD2.bitIsPrimary=1 where numDivisionID=@DivisionID
             
	select @bitTaxApplicable=bitApplicable from DivisionTaxTypes where numDivisionID=  @DivisionID and numTaxItemID=@numTaxItemID

end 
else --New Customer
begin
	SET @bitTaxApplicable=1
END


SELECT @tintBaseTaxOnArea=tintBaseTaxOnArea FROM [Domain] WHERE [numDomainId] = @numDomainID

IF EXISTS (SELECT tintBaseTaxOnArea FROM TaxCountryConfi WHERE numDomainID=@numDomainID AND numCountry=@numBillCountry)
BEGIN
	--TAKE FROM TAXCOUNTRYCONFI IF NOT THEN USE DEFAULT FROM DOMAIN
	SELECT @tintBaseTaxOnArea=tintBaseTaxOnArea FROM TaxCountryConfi WHERE numDomainID=@numDomainID and numCountry=@numBillCountry 
END

if  @bitTaxApplicable=1 --If Tax Applicable
	begin
		if @numBillCountry >0 and @numBillState>0            
		begin            
			if @numBillState>0            
			begin            
				if exists(select * from TaxDetails where numCountryID=@numBillCountry and numStateID=@numBillState and 
						(1=(Case 
								when @tintBaseTaxOnArea=0 then 1 --State
								when @tintBaseTaxOnArea=1 then Case When vcCity like '%' + @vcCity + '%' then 1 else 0 end --City
								when @tintBaseTaxOnArea=2 then Case When vcZipPostal=@vcZipPostal then 1 else 0 end --Zip/Postal
								else 0 end)) and numDomainID= @numDomainID and numTaxItemID=@numTaxItemID)            
						BEGIN
							select top 1 @TaxPercentage=decTaxPercentage, @tintTaxType=tintTaxType from TaxDetails where numCountryID=@numBillCountry and numStateID=@numBillState and 
								(1=(Case 
								when @tintBaseTaxOnArea=0 then 1 --State
								when @tintBaseTaxOnArea=1 then Case When vcCity like '%' + @vcCity +'%' then 1 else 0 end --City
								when @tintBaseTaxOnArea=2 then Case When vcZipPostal=@vcZipPostal then 1 else 0 end --Zip/Postal
								else 0 end)) and numDomainID= @numDomainID and numTaxItemID=@numTaxItemID           
						END
				else if exists(select * from TaxDetails where numCountryID=@numBillCountry and numStateID=@numBillState 
							and numDomainID= @numDomainID and numTaxItemID=@numTaxItemID and isnull(vcCity,'')='' and isnull(vcZipPostal,'')='')            
						BEGIN
							select top 1 @TaxPercentage=decTaxPercentage, @tintTaxType=tintTaxType from TaxDetails where numCountryID=@numBillCountry and numStateID=@numBillState
							 and numDomainID= @numDomainID and numTaxItemID=@numTaxItemID  and isnull(vcCity,'')='' and isnull(vcZipPostal,'')=''         
						END
				 else             
					select top 1 @TaxPercentage=decTaxPercentage, @tintTaxType=tintTaxType from TaxDetails where numCountryID=@numBillCountry and numStateID=0 and isnull(vcCity,'')='' and isnull(vcZipPostal,'')='' and numDomainID= @numDomainID   and numTaxItemID=@numTaxItemID          
		   end                
		end            
		else if @numBillCountry >0            
			 select top 1 @TaxPercentage=decTaxPercentage, @tintTaxType=tintTaxType from TaxDetails where numCountryID=@numBillCountry and numStateID=0 and isnull(vcCity,'')='' and isnull(vcZipPostal,'')='' and numDomainID= @numDomainID  and numTaxItemID=@numTaxItemID
	end

select CONCAT(ISNULL(@TaxPercentage,0),'#',ISNULL(@tintTaxType,1))
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_GetTopItemSales' ) 
    DROP PROCEDURE usp_GetTopItemSales
GO
CREATE PROCEDURE [dbo].[USP_GetTopItemSales]
    (
      @numDomainId NUMERIC(18, 0),
      @numDivisionId NUMERIC(18, 0),
      @IsUnit BIT = 0
    )
AS 
    BEGIN 
    CREATE TABLE #temp(ItemName VARCHAR(2000),TotalUnits numeric(18,2),Revenue numeric(18,2))
        DECLARE @strSql VARCHAR(8000) 
        SET @strSql = ' Insert Into #temp SELECT TOP 10 I.vcItemName As ItemName,SUM(opp.numUnitHour) AS TotalUnits
        ,convert(decimal(10,2),SUM(opp.numUnitHour * monPrice)) AS Revenue
			   FROM  dbo.OpportunityItems opp
			   INNER JOIN dbo.OpportunityMaster om ON Opp.numOppID = oM.numOppID
			   JOIN item I ON Opp.numItemCode = i.numItemcode
			   WHERE   om.numDomainId = ' + CONVERT(VARCHAR, @numDomainId)
            + ' --and Opp.numOppId=12772 order by numoppitemtCode ASC 
			   AND OM.numDivisionID = ' + CONVERT(VARCHAR, @numDivisionId) + ' 
			   GROUP BY opp.numitemcode,I.vcItemName'
	PRINT @strSql
    EXECUTE(@strSql)
    	   
        IF @IsUnit = 0 
            BEGIN
			SELECT * FROM #temp ORDER BY TotalUnits ASC              
            END
        ELSE 
            BEGIN
		    SELECT * FROM #temp ORDER BY Revenue ASC 
            END
    
    DROP TABLE #temp 
    END
			
			 
/****** Object:  StoredProcedure [dbo].[USP_GetWareHouseItems]    Script Date: 07/26/2008 16:18:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec USP_GetWareHouseItems 6                            
--created by anoop jayaraj                              
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getwarehouseitems')
DROP PROCEDURE usp_getwarehouseitems
GO
CREATE PROCEDURE [dbo].[USP_GetWareHouseItems]                              
@numItemCode as numeric(9)=0,
@byteMode as tinyint=0,
@numWarehouseItemID AS NUMERIC(18,0) = NULL                             
as                              
                              
                          
                              
declare @bitSerialize as bit                      
declare @str as nvarchar(max)                 
declare @str1 as nvarchar(max)               
declare @ColName as varchar(500)                      
set @str=''                       

DECLARE @numDomainID AS INT 
DECLARE @numBaseUnit AS NUMERIC(18,0)
DECLARE @vcUnitName as VARCHAR(100) 
DECLARE @numSaleUnit AS NUMERIC(18,0)
DECLARE @vcSaleUnitName as VARCHAR(100) 
DECLARE @numPurchaseUnit AS NUMERIC(18,0)
DECLARE @vcPurchaseUnitName as VARCHAR(100)
DECLARE @bitLot AS BIT                     
DECLARE @bitKitParent BIT
declare @numItemGroupID as numeric(9)                        
                        
select 
	@numDomainID=numDomainID,
	@numBaseUnit = ISNULL(numBaseUnit,0),
	@numSaleUnit = ISNULL(numSaleUnit,0),
	@numPurchaseUnit = ISNULL(numPurchaseUnit,0),
	@numItemGroupID=numItemGroup,
	@bitLot = bitLotNo,
	@bitSerialize=CASE WHEN bitSerialized=0 THEN bitLotNo ELSE bitSerialized END,
	@bitKitParent = ( CASE 
						WHEN ISNULL(bitKitParent,0) = 1 AND ISNULL(bitAssembly,0) = 1 THEN 0
                        WHEN ISNULL(bitKitParent,0) = 1 THEN 1
                        ELSE 0
                     END ) 
FROM 
	Item 
WHERE 
	numItemCode=@numItemCode     
	
SELECT @vcUnitName=ISNULL(vcUnitName,'-') FROM UOM u WHERE u.numUOMId=@numBaseUnit
SELECT @vcSaleUnitName=ISNULL(vcUnitName,'-') FROM UOM u WHERE u.numUOMId=@numSaleUnit
SELECT @vcPurchaseUnitName=ISNULL(vcUnitName,'-') FROM UOM u WHERE u.numUOMId=@numPurchaseUnit
	
DECLARE @numSaleUOMFactor AS DECIMAL(28,14)
DECLARE @numPurchaseUOMFactor AS DECIMAL(28,14)

SELECT @numSaleUOMFactor = dbo.fn_UOMConversion(@numBaseUnit,@numItemCode,@numDOmainID,@numSaleUnit)
SELECT @numPurchaseUOMFactor = dbo.fn_UOMConversion(@numBaseUnit,@numItemCode,@numDOmainID,@numPurchaseUnit)


SET @ColName='WareHouseItems.numWareHouseItemID,0'              
              
--Create a Temporary table to hold data                                                            
create table #tempTable ( ID INT IDENTITY PRIMARY KEY,                                                                      
 numCusFlDItemID numeric(9)                                                         
 )                         
                        
insert into #tempTable                         
(numCusFlDItemID)                                                            
select distinct(numOppAccAttrID) from ItemGroupsDTL where numItemGroupID=@numItemGroupID and tintType=2                       
                          
                 
 declare @ID as numeric(9)                        
 declare @numCusFlDItemID as varchar(20)                        
 declare @fld_label as varchar(100),@fld_type as varchar(100)                        
 set @ID=0                        
 select top 1 @ID=ID,@numCusFlDItemID=numCusFlDItemID,@fld_label=fld_label,@fld_type=fld_type from #tempTable                         
 join CFW_Fld_Master on numCusFlDItemID=Fld_ID                        
                         
 while @ID>0                        
 begin                        
                          
   set @str=@str+',  dbo.GetCustFldItems('+@numCusFlDItemID+',9,'+@ColName+') as ['+ @fld_label+']'
	
	IF @byteMode=1                                        
		set @str=@str+',  dbo.GetCustFldItemsValue('+@numCusFlDItemID+',9,'+@ColName+','''+@fld_type+''') as ['+ @fld_label+'Value]'                                        
                          
   select top 1 @ID=ID,@numCusFlDItemID=numCusFlDItemID,@fld_label=fld_label from #tempTable                         
   join CFW_Fld_Master on numCusFlDItemID=Fld_ID and ID >@ID                        
   if @@rowcount=0 set @ID=0                        
                          
 end                        
                       
--      

DECLARE @KitOnHand AS NUMERIC(9);SET @KitOnHand=0

--IF @bitKitParent=1
	--SELECT @KitOnHand=ISNULL(dbo.fn_GetKitInventory(@numItemCode),0)          
  
set @str1='select '

IF @byteMode=1                                        
		set @str1 =@str1 +'I.numItemCode,'
	
set @str1 =@str1 + 'numWareHouseItemID,WareHouseItems.numWareHouseID,
ISNULL(vcWarehouse,'''') + (CASE WHEN ISNULL(WL.vcLocation,'''') = '''' THEN '''' ELSE '', '' + isnull(WL.vcLocation,'''') END) vcWarehouse,
ISNULL(vcWarehouse,'''') AS vcExternalLocation,
ISNULL(WL.vcLocation,'''') AS vcInternalLocation,
W.numWareHouseID,
Case when @bitKitParent=1 then CAST(ISNULL(dbo.fn_GetKitInventoryByWarehouse(I.numItemCode, W.numWareHouseID),0) AS DECIMAL(18,2)) ELSE CAST(ISNULL(numOnHand,0) AS FLOAT) END AS [OnHand],
CAST((@numPurchaseUOMFactor * Case when @bitKitParent=1 THEN ISNULL(dbo.fn_GetKitInventoryByWarehouse(I.numItemCode, W.numWareHouseID),0) ELSE isnull(numOnHand,0) end) as FLOAT) as PurchaseOnHand,
CAST((@numSaleUOMFactor * Case when @bitKitParent=1 THEN ISNULL(dbo.fn_GetKitInventoryByWarehouse(I.numItemCode, W.numWareHouseID),0) ELSE isnull(numOnHand,0) end) as FLOAT) as SalesOnHand,
Case when @bitKitParent=1 THEN 0 ELSE CAST(ISNULL(numReorder,0) AS FLOAT) END as [Reorder] 
,Case when @bitKitParent=1 THEN 0 ELSE CAST(ISNULL(numOnOrder,0) AS FLOAT) END as [OnOrder]
,Case when @bitKitParent=1 THEN 0 ELSE CAST(ISNULL(numAllocation,0) AS FLOAT) END as [Allocation] 
,Case when @bitKitParent=1 THEN 0 ELSE CAST(ISNULL(numBackOrder,0) AS FLOAT) END as [BackOrder],
CAST((@numSaleUOMFactor * Case when @bitKitParent=1 THEN ISNULL(dbo.fn_GetKitInventoryByWarehouse(I.numItemCode, W.numWareHouseID),0) ELSE isnull(numOnHand,0) end) as numeric(18,2)) as OnHandUOM,
CAST((@numPurchaseUOMFactor * Case when @bitKitParent=1 THEN 0 ELSE isnull(numOnOrder,0) END) AS DECIMAL(18,2)) as OnOrderUOM,
CAST((@numPurchaseUOMFactor * Case when @bitKitParent=1 THEN 0 ELSE isnull(numReorder,0) END) AS DECIMAL(18,2)) as ReorderUOM,
CAST((@numSaleUOMFactor * Case when @bitKitParent=1 THEN 0 ELSE isnull(numAllocation,0) END) AS DECIMAL(18,2)) as AllocationUOM,
CAST((@numSaleUOMFactor * Case when @bitKitParent=1 THEN 0 ELSE isnull(numBackOrder,0) END) AS DECIMAL(18,2)) as BackOrderUOM,
@vcUnitName As vcBaseUnit,
@vcSaleUnitName As vcSaleUnit,
@vcPurchaseUnitName As vcPurchaseUnit
,0 as Op_Flag , isnull(WL.vcLocation,'''') Location,isnull(WL.numWLocationID,0) as numWLocationID,
round(isnull(monWListPrice,0),2) Price,isnull(vcWHSKU,'''') as SKU,isnull(vcBarCode,'''') as BarCode '+ @str +'                   
,(SELECT CASE WHEN ISNULL(subI.bitKitParent,0) = 1 AND ISNULL((SELECT COUNT(*) FROM OpportunityItems 
															   WHERE OpportunityItems.numItemCode = WareHouseItems.numItemID
															   AND WareHouseItems.numWareHouseItemID = OpportunityItems.numWarehouseItmsID),0) = 0 THEN 1
			ELSE 0 
	    END 
FROM Item subI WHERE subI.numItemCode = WareHouseItems.numItemID) [IsDeleteKitWarehouse],@bitKitParent [bitKitParent],
(CASE WHEN (SELECT COUNT(*) FROM ItemDetails WHERE numChildItemID=WareHouseItems.numItemID AND numWareHouseItemId = WareHouseItems.numWareHouseItemID) > 0 THEN 1 ELSE 0 END) AS bitChildItemWarehouse
,' + (CASE WHEN @bitSerialize=1 THEN CONCAT('dbo.GetWarehouseSerialLot(',@numDomainID,',WareHouseItems.numWareHouseItemID,',@bitLot,')') ELSE '''''' END) + ' AS vcSerialLot,
CASE 
	WHEN ISNULL(I.numItemGroup,0) > 0 
	THEN dbo.fn_GetAttributes(WareHouseItems.numWareHouseItemID,0)
	ELSE ''''
END AS vcAttribute 
from WareHouseItems                           
join Warehouses W                             
on W.numWareHouseID=WareHouseItems.numWareHouseID 
left join WarehouseLocation WL on WL.numWLocationID =WareHouseItems.numWLocationID
join Item I on I.numItemCode=WareHouseItems.numItemID and WareHouseItems.numDomainId=I.numDomainId
where numItemID='+convert(varchar(15),@numItemCode) + ' AND (WareHouseItems.numWarehouseItemID = ' + CAST(@numWarehouseItemID AS VARCHAR(20)) + ' OR ' + CAST(ISNULL(@numWarehouseItemID,0) AS VARCHAR(20)) + ' = 0 )'              
   
print(@str1)           

EXECUTE sp_executeSQL @str1, N'@bitKitParent bit,@numSaleUOMFactor DECIMAL(28,14), @numPurchaseUOMFactor DECIMAL(28,14), @vcUnitName VARCHAR(100), @vcSaleUnitName VARCHAR(100), @vcPurchaseUnitName VARCHAR(100)', @bitKitParent, @numSaleUOMFactor, @numPurchaseUOMFactor, @vcUnitName, @vcSaleUnitName, @vcPurchaseUnitName
                       
                        
set @str1='select numWareHouseItmsDTLID,WDTL.numWareHouseItemID, vcSerialNo,WDTL.vcComments as Comments,WDTL.numQty, WDTL.numQty as OldQty,ISNULL(W.vcWarehouse,'''') + (CASE WHEN ISNULL(WL.vcLocation,'''') = '''' THEN '''' ELSE '', '' + isnull(WL.vcLocation,'''') END) vcWarehouse '+ case when @bitSerialize=1 then @str else '' end +'
,WDTL.dExpirationDate, WDTL.bitAddedFromPO
from WareHouseItmsDTL WDTL                             
join WareHouseItems                             
on WDTL.numWareHouseItemID=WareHouseItems.numWareHouseItemID                              
join Warehouses W                             
on W.numWareHouseID=WareHouseItems.numWareHouseID  
left join WarehouseLocation WL on WL.numWLocationID =WareHouseItems.numWLocationID
where ISNULL(numQty,0) > 0 and numItemID='+ convert(varchar(15),@numItemCode) + ' AND (WareHouseItems.numWarehouseItemID = ' + CAST(@numWarehouseItemID AS VARCHAR(20)) + ' OR ' + CAST(ISNULL(@numWarehouseItemID,0) AS VARCHAR(20)) + ' = 0 )'                          
print @str1                       
exec (@str1)                       
                      
                      
select Fld_label,fld_id,fld_type,numlistid,vcURL from #tempTable                         
 join CFW_Fld_Master on numCusFlDItemID=Fld_ID                      
drop table #tempTable


-----------------For Kendo UI testting purpose
--create table #tempColumnConfig ( field varchar(100),                                                                      
-- title varchar(100) , format varchar(100)                                                      
-- )  
--
--insert into #tempColumnConfig
--	select 'vcWarehouse','WareHouse','' union all
--	select 'OnHand','On Hand','' union all
--	select 'Reorder','Re Order','' union all
--	select 'Allocation','Allocation','' union all
--	select 'BackOrder','Back Order','' union all
--	select 'Price','Price','{0:c}'
--
--select * from #tempColumnConfig
--
--drop table #tempColumnConfig

-----------------

--set @str1='select case when COUNT(*)=1 then MIN(numWareHouseItmsDTLID) else 0 end as numWareHouseItmsDTLID,MIN(WDTL.numWareHouseItemID) as numWareHouseItemID, vcSerialNo,0 as Op_Flag,WDTL.vcComments as Comments'+ case when @bitSerialize=1 then @str else '' end +',W.vcWarehouse,COUNT(*) Qty
--from WareHouseItmsDTL WDTL                             
--join WareHouseItems WI                             
--on WDTL.numWareHouseItemID=WI.numWareHouseItemID                              
--join Warehouses W                             
--on W.numWareHouseID=WI.numWareHouseID  
--where (tintStatus is null or tintStatus=0)  and  numItemID='+ convert(varchar(15),@numItemCode) +'
--GROUP BY vcSerialNo,WDTL.vcComments,W.vcWarehouse'
--                          
--print @str1                       
--exec (@str1)                       
              
GO
/****** Object:  StoredProcedure [dbo].[USP_GetWorkOrder]    Script Date: 07/26/2008 16:16:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetWorkOrder')
DROP PROCEDURE USP_GetWorkOrder
GO
CREATE PROCEDURE [dbo].[USP_GetWorkOrder]                             
@numDomainID as numeric(9),
@ClientTimeZoneOffset  INT,
@numWOStatus as numeric(9),
@SortExpression AS VARCHAR(50),
@CurrentPage int,                                                                            
@PageSize INT,
@numUserCntID AS NUMERIC(9),
@vcWOId  AS VARCHAR(4000)                                                                       
as                            

SET @vcWOId=ISNULL(@vcWOId,'')

DECLARE @firstRec AS INTEGER
DECLARE @lastRec AS INTEGER
SET @firstRec= ((@CurrentPage-1) * @PageSize) + 1                                                                           
SET @lastRec= (@CurrentPage * @PageSize)    
    
DECLARE @TEMP TABLE
(
	ItemLevel INT, numParentWOID NUMERIC(18,0), numWOID NUMERIC(18,0), ID VARCHAR(1000), numItemCode NUMERIC(18,0),
	vcItemName VARCHAR(500), numQty FLOAT, numWarehouseItemID NUMERIC(18,0), vcWarehouse VARCHAR(200), numOnHand FLOAT,
	numOnOrder FLOAT, numAllocation FLOAT, numBackOrder FLOAT, vcInstruction VARCHAR(2000),
	numAssignedTo NUMERIC(18,0), vcAssignedTo VARCHAR(500), numCreatedBy NUMERIC(18,0), vcCreated VARCHAR(500),
	bintCompliationDate VARCHAR(50), numWOStatus NUMERIC(18,0), numOppID NUMERIC(18,0), vcOppName VARCHAR(1000),
	numPOID NUMERIC(18,0), vcPOName VARCHAR(1000), bitWorkOrder BIT, bitReadyToBuild BIT
)

DECLARE @TEMPWORKORDER TABLE
(
	ItemLevel INT, RowNumber INT, numParentWOID NUMERIC(18,0), numWOID NUMERIC(18,0), ID VARCHAR(1000), numItemCode NUMERIC(18,0),
	numOppID NUMERIC(18,0), numWarehouseItemID NUMERIC(18,0), numQty FLOAT, numWOStatus NUMERIC(18,0), vcInstruction VARCHAR(2000),
	numAssignedTo NUMERIC(18,0), bintCreatedDate DATETIME, bintCompliationDate DATETIME, numCreatedBy NUMERIC(18,0),  
	bitWorkOrder BIT, bitReadyToBuild BIT
) 

INSERT INTO 
	@TEMPWORKORDER
SELECT 
	1 AS ItemLevel,
	ROW_NUMBER() OVER(ORDER BY numWOId) AS RowNumber,
	CAST(0 AS NUMERIC(18,0)) AS numParentId,
	numWOId,
	CAST(CONCAT('#',ISNULL(numWOId,'0'),'#') AS VARCHAR(1000)) AS ID,
	numItemCode,
	numOppId,
	numWareHouseItemId,
	CAST(numQtyItemsReq AS FLOAT),
	numWOStatus,
	vcInstruction,
	numAssignedTo,
	bintCreatedDate,
	bintCompliationDate,
	numCreatedBy,
	1 AS bitWorkOrder,
	dbo.CheckAssemblyBuildStatus(numItemCode,numQtyItemsReq) AS bitReadyToBuild
FROM 
	WorkOrder
WHERE  
	numDomainID=@numDomainID 
	AND 1=(CASE WHEN ISNULL(@numWOStatus,0) = 0 THEN CASE WHEN WorkOrder.numWOStatus <> 23184 THEN 1 ELSE 0 END ELSE CASE WHEN WorkOrder.numWOStatus = @numWOStatus THEN 1 ELSE 0 END END)
	AND 1=(Case when @numUserCntID>0 then case when isnull(numAssignedTo,0)=@numUserCntID then 1 else 0 end else 1 end)
	AND 1=(Case when len(@vcWOId)>0 then Case when numWOId in (SELECT * from dbo.Split(@vcWOId,',')) then 1 else 0 end else 1 end)
	AND (ISNULL(numParentWOID,0) = 0 OR numParentWOID NOT IN (SELECT numWOId FROM WorkOrder WHERE numDomainID=@numDomainID))
ORDER BY
	bintCreatedDate DESC,
	numWOId ASC

DECLARE @TOTALROWCOUNT AS NUMERIC(18,0)

SELECT @TOTALROWCOUNT=COUNT(*) FROM @TEMPWORKORDER


;WITH CTEWorkOrder (ItemLevel,numParentWOID,numWOID,ID,numItemKitID,numQtyItemsReq,numWarehouseItemID,vcInstruction,numAssignedTo,
					bintCreatedDate,numCreatedBy,bintCompliationDate,numWOStatus,numOppID,bitWorkOrder,bitReadyToBuild) AS
(
	SELECT
		ItemLevel,
		numParentWOID,
		numWOId,
		ID,
		numItemCode,
		numQty,
		numWareHouseItemId,
		vcInstruction,
		numAssignedTo,
		bintCreatedDate,
		numCreatedBy,
		bintCompliationDate,
		numWOStatus,
		numOppId,
		1 AS bitWorkOrder,
		bitReadyToBuild
	FROM
		@TEMPWORKORDER t
	WHERE
		t.RowNumber BETWEEN @firstRec AND @lastRec
	UNION ALL
	SELECT 
		c.ItemLevel+2,
		c.numWOID,
		WorkOrder.numWOId,
		CAST(CONCAT(c.ID,'-','#',ISNULL(WorkOrder.numWOId,'0'),'#') AS VARCHAR(1000)),
		WorkOrder.numItemCode,
		CAST(WorkOrder.numQtyItemsReq AS FLOAT),
		WorkOrder.numWareHouseItemId,
		CAST(WorkOrder.vcInstruction AS VARCHAR(2000)),
		WorkOrder.numAssignedTo,
		WorkOrder.bintCreatedDate,
		WorkOrder.numCreatedBy,
		WorkOrder.bintCompliationDate,
		WorkOrder.numWOStatus,
		WorkOrder.numOppId,
		1 AS bitWorkOrder,
		dbo.CheckAssemblyBuildStatus(numItemCode,WorkOrder.numQtyItemsReq) AS bitReadyToBuild
	FROM 
		WorkOrder
	INNER JOIN 
		CTEWorkOrder c 
	ON 
		WorkOrder.numParentWOID = c.numWOID
)

INSERT 
	@TEMP
SELECT
	ItemLevel,
	numParentWOID,
	numWOID,
	ID,
	numItemCode,
	vcItemName,
	CTEWorkOrder.numQtyItemsReq,
	CTEWorkOrder.numWarehouseItemID,
	Warehouses.vcWareHouse,
	WareHouseItems.numOnHand,
	WareHouseItems.numOnOrder,
	WareHouseItems.numAllocation,
	WareHouseItems.numBackOrder,
	vcInstruction,
	CTEWorkOrder.numAssignedTo,
	dbo.fn_GetContactName(isnull(CTEWorkOrder.numAssignedTo,0)) AS vcAssignedTo,
	CTEWorkOrder.numCreatedBy,
	CAST(ISNULL(dbo.fn_GetContactName(CTEWorkOrder.numCreatedBy), '&nbsp;&nbsp;-') + ',' + convert(varchar(20),dateadd(minute,-@ClientTimeZoneOffset,CTEWorkOrder.bintCreatedDate)) AS VARCHAR(500)) ,
	convert(varchar(20),dateadd(minute,-@ClientTimeZoneOffset,bintCompliationDate)) AS bintCompliationDate,
	numWOStatus,
	CTEWorkOrder.numOppID,
	OpportunityMaster.vcPOppName,
	NULL,
	NULL,
	bitWorkOrder,
	bitReadyToBuild
FROM 
	CTEWorkOrder
INNER JOIN 
	Item
ON
	CTEWorkOrder.numItemKitID = Item.numItemCode
LEFT JOIN
	WareHouseItems
ON
	CTEWorkOrder.numWarehouseItemID = WareHouseItems.numWareHouseItemID
LEFT JOIN
	Warehouses
ON
	WareHouseItems.numWareHouseID = Warehouses.numWareHouseID
LEFT JOIN 
	OpportunityMaster
ON
	CTEWorkOrder.numOppID = OpportunityMaster.numOppId

INSERT INTO 
	@TEMP
SELECT
	t1.ItemLevel + 1,
	t1.numWOID,
	NULL,
	CONCAT(t1.ID,'-#0#'),
	WorkOrderDetails.numChildItemID,
	Item.vcItemName,
	WorkOrderDetails.numQtyItemsReq,
	WorkOrderDetails.numWarehouseItemID,
	Warehouses.vcWareHouse,
	WareHouseItems.numOnHand,
	WareHouseItems.numOnOrder,
	WareHouseItems.numAllocation,
	WareHouseItems.numBackOrder,
	vcInstruction,
	NULL,
	NULL,
	NULL,
	NULL,
	convert(varchar(20),dateadd(minute,-@ClientTimeZoneOffset,bintCompliationDate)) AS bintCompliationDate,
	numWOStatus,
	NULL,
	NULL,
	OpportunityMaster.numOppId,
	OpportunityMaster.vcPOppName,
	0 AS bitWorkOrder,
	0 AS bitReadyToBuild
FROM
	WorkOrderDetails
INNER JOIN
	@TEMP t1
ON
	WorkOrderDetails.numWOId = t1.numWOID
INNER JOIN 
	Item
ON
	WorkOrderDetails.numChildItemID = Item.numItemCode
LEFT JOIN
	WareHouseItems
ON
	WorkOrderDetails.numWarehouseItemID = WareHouseItems.numWareHouseItemID
LEFT JOIN
	Warehouses
ON
	WareHouseItems.numWareHouseID = Warehouses.numWareHouseID
LEFT JOIN
	OpportunityMaster 
ON
	WorkOrderDetails.numPOID = OpportunityMaster.numOppId

SELECT * FROM @TEMP ORDER BY numParentWOID,ItemLevel

SELECT @TOTALROWCOUNT AS TotalRowCount 

GO
--exec USP_INVENTORYDASHBOARD @numDomainID=72,@PeriodFrom='01/Jan/2008',@PeriodTo='31/Dec/2008',@Type=2,@Top=2
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_InventoryDashboard')
DROP PROCEDURE USP_InventoryDashboard
GO
CREATE PROCEDURE USP_InventoryDashboard
(@numDomainID int,
@PeriodFrom datetime,
@PeriodTo datetime,
@Type int,
@Top int)
as
begin
declare @Sql varchar(500);

create table #MostSold		
		(Item varchar(250),SalesUnit FLOAT);

if @type=1 -- Most Sold
	begin
		insert into #MostSold		
		select  vcItemName + ',' + ItemClass as Item, sum(numUnitHour) as SalesUnit 
		from VIEW_INVENTORYOPPSALES where numDomainID=@numDomainID
		 and OppDate between @PeriodFrom and @PeriodTo
		 group by vcItemName + ',' + ItemClass 

		set @Sql ='select top ' +  cast( @Top as varchar) + ' * from #MostSold order by SalesUnit Desc'
		exec (@SQl)
	end
else if @type=2 --- Highest Avg Cost
	begin				
		insert into #MostSold		
		select  vcItemName + ',' + ItemClass as Item, Max(AvgCost) as AvgCost 
		from VIEW_INVENTORYOPPSALES where numDomainID=@numDomainID
		 and OppDate between @PeriodFrom and @PeriodTo
		 group by vcItemName + ',' + ItemClass 

		set @Sql ='select top ' +  cast( @Top as varchar) + ' * from #MostSold order by SalesUnit Desc'
		exec (@SQl)
	end
else if @type=3 --- Highest Cogs
	begin
				
		insert into #MostSold		
		select  vcItemName + ',' + ItemClass as Item, sum(COGS) as COGS 
		from VIEW_INVENTORYOPPSALES where numDomainID=@numDomainID
		 and OppDate between @PeriodFrom and @PeriodTo
		 group by vcItemName + ',' + ItemClass 

		set @Sql ='select top ' +  cast( @Top as varchar) + ' * from #MostSold order by SalesUnit Desc'
		exec (@SQl)
	end
else if @type=4 --- Highest Profit
	begin
				
		insert into #MostSold		
		select  vcItemName + ',' + ItemClass as Item, sum(Profit) as Profit 
		from VIEW_INVENTORYOPPSALES where numDomainID=@numDomainID
		 and OppDate between @PeriodFrom and @PeriodTo
		 group by vcItemName + ',' + ItemClass 

		set @Sql ='select top ' +  cast( @Top as varchar) + ' * from #MostSold order by SalesUnit Desc'
		exec (@SQl)
	end
else if @type=5 --- Highest Return
	begin
				
		insert into #MostSold		
		select  vcItemName + ',' + ItemClass as Item, sum(SalesReturn) as [Return ]
		from VIEW_INVENTORYOPPSALES where numDomainID=@numDomainID
		 and OppDate between @PeriodFrom and @PeriodTo
		 group by vcItemName + ',' + ItemClass 

		set @Sql ='select top ' +  cast( @Top as varchar) + ' * from #MostSold order by SalesUnit Desc'
		exec (@SQl)
	end

	drop table #MostSold;
end
/****** Object:  StoredProcedure [dbo].[USP_ItemPricingRecomm]    Script Date: 02/19/2009 01:33:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- exec USP_ItemPricingRecomm @numItemCode=173028,@units=1,@numOppID=0,@numDivisionID=7052,@numDomainID=72,@tintOppType=1,@CalPrice=$50,@numWareHouseItemID=130634,@bitMode=0
--created by anoop jayaraj                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_itempricingrecomm')
DROP PROCEDURE usp_itempricingrecomm
GO
CREATE PROCEDURE [dbo].[USP_ItemPricingRecomm]                                        
@numItemCode as numeric(9),                                        
@units integer,                                  
@numOppID as numeric(9)=0,                                
@numDivisionID as numeric(9)=0,                  
@numDomainID as numeric(9)=0,            
@tintOppType as tinyint,          
@CalPrice as money,      
@numWareHouseItemID as numeric(9),
@bitMode tinyint=0,
@vcSelectedKitChildItems VARCHAR(MAX) = '',
@numOppItemID NUMERIC(18,0) = 0
as                 
BEGIN TRY

     
DECLARE @numRelationship AS NUMERIC(9)
DECLARE @numProfile AS NUMERIC(9)             
DECLARE @monListPrice MONEY
DECLARE @bitCalAmtBasedonDepItems BIT
DECLARE @numDefaultSalesPricing TINYINT
DECLARE @tintPriceLevel INT

/*Profile and relationship id */
SELECT 
	@numRelationship=numCompanyType,
	@numProfile=vcProfile,
	@tintPriceLevel = ISNULL(D.tintPriceLevel,0)
FROM 
	DivisionMaster D                  
JOIN 
	CompanyInfo C 
ON 
	C.numCompanyId=D.numCompanyID                  
WHERE 
	numDivisionID =@numDivisionID       
            
IF @tintOppType=1            
BEGIN
	SELECT
		@bitCalAmtBasedonDepItems=ISNULL(bitCalAmtBasedonDepItems,0) 
	FROM 
		Item 
	WHERE 
		numItemCode = @numItemCode
      
	/*Get List Price for item*/      
	IF((@numWareHouseItemID>0) AND EXISTS(SELECT * FROM Item WHERE numItemCode=@numItemCode AND charItemType='P'))      
	BEGIN      
		SELECT 
			@monListPrice=ISNULL(monWListPrice,0) 
		FROM 
			WareHouseItems 
		WHERE 
			numWareHouseItemID=@numWareHouseItemID      
		
		IF @monListPrice=0 
		BEGIN
			SELECT @monListPrice=monListPrice FROM Item WHERE numItemCode=@numItemCode       
		END
	END 
	ELSE
	BEGIN
		 SELECT 
			@monListPrice=monListPrice 
		FROM 
			Item 
		WHERE 
			numItemCode=@numItemCode      
	END      

	/*Use Calculated Price When item is Assembly/Kit,Calculate price based on sum of dependent items.*/
	IF @bitCalAmtBasedonDepItems = 1 
	BEGIN
		CREATE TABLE #TEMPSelectedKitChilds
		(
			ChildKitItemID NUMERIC(18,0),
			ChildKitWarehouseItemID NUMERIC(18,0),
			ChildKitChildItemID NUMERIC(18,0),
			ChildKitChildWarehouseItemID NUMERIC(18,0)
		)


		IF ISNULL(@numOppItemID,0) > 0
		BEGIN
			INSERT INTO 
				#TEMPSelectedKitChilds
			SELECT 
				OKI.numChildItemID,
				OKI.numWareHouseItemId,
				OKCI.numItemID,
				OKCI.numWareHouseItemId
			FROM
				OpportunityKitItems OKI
			INNER JOIN
				OpportunityKitChildItems OKCI
			ON
				OKI.numOppChildItemID = OKCI.numOppChildItemID
			WHERE
				OKI.numOppId = @numOppID
				AND OKI.numOppItemID = @numOppItemID
		END
		ELSE IF ISNULL(@vcSelectedKitChildItems,'') <> ''
		BEGIN
			INSERT INTO 
				#TEMPSelectedKitChilds
			SELECT 
				SUBSTRING
				(
					SUBSTRING(OutParam,0,CHARINDEX('-',OutParam)),
					0,
					CHARINDEX('#', SUBSTRING(OutParam,0,CHARINDEX('-',OutParam)))
				) AS numChildKitID,
				SUBSTRING
				(
					SUBSTRING(OutParam,0,CHARINDEX('-',OutParam)),
					CHARINDEX('#', SUBSTRING(OutParam,0,CHARINDEX('-',OutParam))) + 1,
					LEN(SUBSTRING(OutParam,0,CHARINDEX('-',OutParam)))
				) AS numChildKitWarehouseItemID,
				SUBSTRING
				(
					SUBSTRING(OutParam,CHARINDEX('-',OutParam) + 1, LEN(OutParam)),
					0,
					CHARINDEX('#', SUBSTRING(OutParam,CHARINDEX('-',OutParam) + 1, LEN(OutParam)))
				) AS numChildKitID,
				SUBSTRING
				(
					SUBSTRING(OutParam,CHARINDEX('-',OutParam) + 1, LEN(OutParam)),
					CHARINDEX('#', SUBSTRING(OutParam,CHARINDEX('-',OutParam) + 1, LEN(OutParam))) + 1,
					LEN(SUBSTRING(OutParam,CHARINDEX('-',OutParam) + 1, LEN(OutParam)))
				) AS numChildKitWarehouseItemID
		
			FROM 
				dbo.SplitString(@vcSelectedKitChildItems,',')
		END

		;WITH CTE (numItemCode, vcItemName, bitKitParent, numWarehouseItemID,numQtyItemsReq,numUOMId) AS
		(
			SELECT
				ID.numChildItemID,
				I.vcItemName,
				ISNULL(I.bitKitParent,0),
				ISNULL(ID.numWarehouseItemId,0),
				(ISNULL(ID.numQtyItemsReq,0) * ISNULL(dbo.fn_UOMConversion(ISNULL(ID.numUOMID, 0), I.numItemCode, @numDomainID, ISNULL(I.numBaseUnit, 0)),1)),
				ISNULL(numUOMId,0)
			FROM 
				[ItemDetails] ID
			INNER JOIN 
				[Item] I 
			ON 
				ID.[numChildItemID] = I.[numItemCode]
			WHERE   
				[numItemKitID] = @numItemCode
			UNION ALL
			SELECT
				ID.numChildItemID,
				I.vcItemName,
				ISNULL(I.bitKitParent,0),
				ISNULL(ID.numWarehouseItemId,0),
				(ISNULL(ID.numQtyItemsReq,0) * ISNULL(dbo.fn_UOMConversion(ISNULL(ID.numUOMID, 0), I.numItemCode, @numDomainID, ISNULL(I.numBaseUnit, 0)),1)),
				ISNULL(ID.numUOMId,0)
			FROM 
				CTE As Temp1
			INNER JOIN
				[ItemDetails] ID
			ON
				ID.numItemKitID = Temp1.numItemCode
			INNER JOIN 
				[Item] I 
			ON 
				ID.[numChildItemID] = I.[numItemCode]
			INNER JOIN
				#TEMPSelectedKitChilds
			ON
				ISNULL(Temp1.bitKitParent,0) = 1
				AND ID.numChildItemID = #TEMPSelectedKitChilds.ChildKitChildItemID
				AND ID.numWareHouseItemId = #TEMPSelectedKitChilds.ChildKitChildWarehouseItemID
		)

		SELECT  
				@CalPrice = ISNULL(  SUM(ISNULL(CASE WHEN I.[charItemType]='P' 
				THEN WI.[monWListPrice] 
				ELSE monListPrice END,0) * ID.[numQtyItemsReq] )  ,0)
		FROM    CTE ID
				INNER JOIN [Item] I ON ID.numItemCode = I.[numItemCode]
				left join  WareHouseItems WI
				on WI.numItemID=I.numItemCode and WI.[numWareHouseItemID]=ID.numWarehouseItemID
		WHERE
			ISNULL(ID.bitKitParent,0) = 0

		DROP TABLE #TEMPSelectedKitChilds
	END


	SELECT 
		@numDefaultSalesPricing = ISNULL(numDefaultSalesPricing,2) 
	FROM 
		Domain 
	WHERE 
		numDomainID = @numDomainID

	IF @numDefaultSalesPricing = 1 -- Use Price Level
	BEGIN
		DECLARE @newPrice MONEY
		DECLARE @finalUnitPrice FLOAT = 0
		DECLARE @tintRuleType INT
		DECLARE @tintDiscountType INT
		DECLARE @decDiscount FLOAT
		DECLARE @ItemPrice FLOAT

		SET @tintRuleType = 0
		SET @tintDiscountType = 0
		SET @decDiscount  = 0
		SET @ItemPrice = 0

		SELECT TOP 1
			@tintRuleType = tintRuleType,
			@tintDiscountType = tintDiscountType,
			@decDiscount = decDiscount
		FROM 
			PricingTable 
		WHERE 
			numItemCode = @numItemCode AND
			((@units BETWEEN intFromQty AND intToQty) OR tintRuleType = 3)

		IF @tintRuleType > 0 AND @tintDiscountType > 0
		BEGIN
			IF @tintRuleType = 1 -- Deduct from List price
			BEGIN
				IF (SELECT ISNULL(charItemType,'') FROM Item WHERE numDomainID = @numDomainID AND numItemCode = @numItemCode) = 'P'
					If (SELECT COUNT(*) FROM Item WHERE numDomainID = @numDomainID AND numItemCode = @numItemCode AND (bitKitParent = 1 OR bitAssembly = 1)) > 0 
						SELECT @ItemPrice = dbo.GetUnitPriceForKitOrAssembly(@numDomainID,@numItemCode,@numWarehouseItemID,@units)
					ELSE
						SELECT @ItemPrice = ISNULL(monWListPrice,0) FROM WareHouseItems WHERE numDomainID = @numDomainID AND numItemID = @numItemCode AND numWareHouseItemID = @numWarehouseItemID
				ELSE
					SELECT @ItemPrice = ISNULL(monListPrice,0) FROM Item WHERE numDomainID = @numDomainID AND numItemCode = @numItemCode

				IF @tintDiscountType = 1 -- Percentage
				BEGIN
					IF @decDiscount > 0
						SELECT @finalUnitPrice = @ItemPrice - (@ItemPrice * (@decDiscount/100))
				END
				ELSE IF @tintDiscountType = 2 -- Flat Amount
				BEGIN
					SELECT @finalUnitPrice = @ItemPrice - @decDiscount
				END
			END
			ELSE IF @tintRuleType = 2 -- Add to Primary Vendor Cost
			BEGIN
				If (SELECT COUNT(*) FROM Item WHERE numDomainID = @numDomainID AND numItemCode = @numItemCode AND (bitKitParent = 1 OR bitAssembly = 1)) > 0 
					SELECT @ItemPrice = dbo.GetUnitPriceForKitOrAssembly(@numDomainID,@numItemCode,@numWarehouseItemID,@units)
				ELSE
					SELECT @ItemPrice = ISNULL(monCost,0) FROM Vendor WHERE numItemCode = @numItemCode AND numVendorID = (SELECT numVendorID FROM Item WHERE numItemCode = @numItemCode)

				If @ItemPrice > 0
				BEGIN
					IF @tintDiscountType = 1 -- Percentage
					BEGIN
						IF @decDiscount > 0
							SELECT @finalUnitPrice = @ItemPrice + (@ItemPrice * (@decDiscount/100))
					END
					ELSE IF @tintDiscountType = 2 -- Flat Amount
					BEGIN
						SELECT @finalUnitPrice = @ItemPrice + @decDiscount
					END
				END
			END
			ELSE IF @tintRuleType = 3 -- Named Price
			BEGIN
				IF ISNULL(@tintPriceLevel,0) > 0
				BEGIN
					SET @decDiscount = 0
					SET @tintDiscountType = 2
					SET @finalUnitPrice = 0 
					
					SELECT 
						@finalUnitPrice = ISNULL(decDiscount,0)
					FROM
					(
						SELECT 
							ROW_NUMBER() OVER(PARTITION BY PricingTable.numItemCode ORDER BY numPricingID) Id,
							decDiscount
						FROM 
							PricingTable 
						INNER JOIN
							Item
						ON
							Item.numItemCode = @numItemCode 
							AND Item.numDomainID = @numDomainID
						WHERE 
							tintRuleType = 3 
					) TEMP
					WHERE
						Id BETWEEN @tintPriceLevel AND @tintPriceLevel

					IF ISNULL(@finalUnitPrice,0) = 0
					BEGIN
						SET @finalUnitPrice = @monListPrice
					END
					ELSE
					BEGIN
						SET @monListPrice = @finalUnitPrice
					END
				END
				ELSE
				BEGIN
					SET @finalUnitPrice = @decDiscount
					SET @monListPrice = @finalUnitPrice
					-- KEEP THIS LINE AT END
					SET @decDiscount = 0
					SET @tintDiscountType = 2
				END				
			END
		END

		IF @finalUnitPrice = 0
			SET @newPrice = @monListPrice
		ELSE
			SET @newPrice = @finalUnitPrice

		If @tintRuleType = 2
		BEGIN
			IF @finalUnitPrice > 0
			BEGIN
				IF @tintDiscountType = 1 -- Percentage
				BEGIN
					IF @newPrice > @monListPrice
						SET @decDiscount = 0
					ELSE
						If @monListPrice > 0
							SET @decDiscount = ((@monListPrice - @newPrice) * 100) / CAST(@monListPrice AS FLOAT)
						ELSE
							SET @decDiscount = 0
				END
				ELSE IF @tintDiscountType = 2 -- Flat Amount
				BEGIN
					If @newPrice > @monListPrice
						SET @decDiscount = 0
					ELSE
						SET @decDiscount = @monListPrice - @newPrice
				END
			END
			ELSE
			BEGIN
				SET @decDiscount = 0
				SET @tintDiscountType = 0
			END
		END

		SELECT 
			1 as numUintHour, 
			ISNULL(@newPrice,0) AS ListPrice,
			'' AS vcPOppName,
			'' AS bintCreatedDate,
			CASE 
				WHEN @tintRuleType = 0 THEN 'Price - List price'
				ELSE 
				   CASE @tintRuleType
				   WHEN 1 
						THEN 
							'Deduct from List price ' +  CASE 
														   WHEN @tintDiscountType = 1 THEN CONVERT(VARCHAR(20), @decDiscount) + '%'
														   ELSE 'flat ' + CONVERT(VARCHAR(20), CAST(@decDiscount AS int))
														   END 
				   WHEN 2 THEN 'Add to primary vendor cost '  +  CASE 
														   WHEN @tintDiscountType = 1 THEN CONVERT(VARCHAR(20), @decDiscount) + '%'
														   ELSE 'flat ' + CONVERT(VARCHAR(20), CAST(@decDiscount AS int))
														   END 
				   ELSE 'Flat ' + CONVERT(VARCHAR(20), @decDiscount)
				   END
			END AS OppStatus,
			CAST(0 AS NUMERIC) AS numPricRuleID,
			CAST(1 AS TINYINT) AS tintPricingMethod,
			CAST(@tintDiscountType AS TINYINT) AS tintDiscountTypeOriginal,
			CAST(@decDiscount AS FLOAT) AS decDiscountOriginal,
			CAST(@tintRuleType AS TINYINT) AS tintRuleType
	END
	ELSE -- Use Price Rule
	BEGIN
		/* Checks Pricebook if exist any.. */      
		SELECT TOP 1 1 AS numUnitHour,
				dbo.GetPriceBasedOnPriceBook(P.[numPricRuleID],@monListPrice,@units,I.numItemCode) 
				* (CASE WHEN P.tintPricingMethod =2 AND P.tintRuleType=2 THEN dbo.fn_UOMConversion(numBaseUnit,@numItemCode,@numDomainId,CASE WHEN ISNULL(numPurchaseUnit,0)>0 THEN numPurchaseUnit ELSE numBaseUnit END) ELSE 1 END)  AS ListPrice,
				vcRuleName AS vcPOppName,
				CONVERT(VARCHAR(20), NULL) AS bintCreatedDate,
				CASE WHEN numPricRuleID IS NULL THEN 'Price - List price'
					 ELSE 
					  CASE WHEN P.tintPricingMethod = 1
										 THEN 'Price Book Rule'
										 ELSE CASE PBT.tintRuleType
												WHEN 1 THEN 'Deduct from List price '
												WHEN 2 THEN 'Add to primary vendor cost '
												ELSE ''
											  END
											  + CASE WHEN tintDiscountType = 1
													 THEN CONVERT(VARCHAR(20), decDiscount)
														  + '%'
													 ELSE 'flat ' + CONVERT(VARCHAR(20), CAST(decDiscount AS int))
												END + ' for every '
											  + CONVERT(VARCHAR(20), CASE WHEN [intQntyItems] = 0 THEN 1
																		  ELSE intQntyItems
																	 END)
											  + ' units, until maximum of '
											  + CONVERT(VARCHAR(20), decMaxDedPerAmt)
											  + CASE WHEN tintDiscountType = 1 THEN ' %' ELSE ' Flat' END END
				END AS OppStatus,P.numPricRuleID,P.tintPricingMethod,PBT.tintDiscountTypeOriginal,CAST(PBT.decDiscountOriginal AS FLOAT) AS decDiscountOriginal,PBT.tintRuleType
		FROM    Item I
				LEFT JOIN PriceBookRules P ON I.numDomainID = P.numDomainID
				LEFT JOIN PriceBookRuleDTL PDTL ON P.numPricRuleID = PDTL.numRuleID
				LEFT JOIN PriceBookRuleItems PBI ON P.numPricRuleID = PBI.numRuleID
				LEFT JOIN [PriceBookPriorities] PP ON PP.[Step2Value] = P.[tintStep2] AND PP.[Step3Value] = P.[tintStep3]
				CROSS APPLY dbo.GetPriceBasedOnPriceBookTable(P.[numPricRuleID],CASE WHEN bitCalAmtBasedonDepItems=1 then @CalPrice ELSE @monListPrice END ,@units,I.numItemCode) as PBT
		WHERE   numItemCode = @numItemCode AND tintRuleFor=@tintOppType
		AND 
		(
		((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 1
		OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND (tintStep3= 1 AND PDTL.numValue = @numDivisionID)) -- Priority 2
		OR ((tintStep2 = 3) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 3

		OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 4
		OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 5
		OR ((tintStep2 = 3) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 6

		OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 3)) -- Priority 7
		OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND (tintStep3 = 3)) -- Priority 8
		OR ((tintStep2 = 3) and (tintStep3 = 3)) -- Priority 9
		)
		ORDER BY PP.Priority ASC
	END

	IF @bitMode=2  -- When this procedure is being called from USP_GetPriceOfItem for e-com
		RETURN 

                                   
	select 1 as numUnitHour,convert(money,ISNULL((monTotAmount/(case when numUnitHour< 1 then 1 else numUnitHour end)),0)) as ListPrice,vcPOppName,convert(varchar(20),bintCreatedDate) as bintCreatedDate,case when tintOppStatus=0 then 'Opportunity' when tintOppStatus=1 then 'Deal won'   
	when tintOppStatus=2 then 'Deal Lost' end as OppStatus,CAST(0 AS NUMERIC) AS numPricRuleID,CAST(0 AS TINYINT) AS tintPricingMethod,CAST(0 AS TINYINT) as tintDiscountTypeOriginal,CAST(0 AS FLOAT) as decDiscountOriginal,CAST(0 AS TINYINT) as tintRuleType                         
	from OpportunityItems itm                                        
	join OpportunityMaster mst                                        
	on mst.numOppId=itm.numOppId              
	where  tintOppType=1 and numItemCode=@numItemCode and mst.numDivisionID=@numDivisionID order by OppStatus desc  
  
  
	select 1 as numUnitHour,CASE WHEN @bitCalAmtBasedonDepItems=1 THEN convert(money,ISNULL(@CalPrice,0)) ELSE convert(money,ISNULL(@monListPrice,0)) End as ListPrice,'' as vcPOppName,convert(varchar(20),null) as bintCreatedDate,CASE WHEN @bitCalAmtBasedonDepItems=1 THEN 'Sum of dependent items' ELSE '' END as OppStatus
	,CAST(0 AS NUMERIC) AS numPricRuleID,CAST(0 AS TINYINT) AS tintPricingMethod ,CAST(0 AS TINYINT) as tintDiscountTypeOriginal,CAST(0 AS FLOAT) as decDiscountOriginal,CAST(0 AS TINYINT) as tintRuleType                    
END             
ELSE            
BEGIN
	If @numDivisionID=0
	BEGIN
		SELECT 
			@numDivisionID=V.numVendorID 
		FROM 
			[Vendor] V 
		INNER JOIN 
			Item I 
		ON 
			V.numVendorID = I.numVendorID 
			AND V.numItemCode=I.numItemCode 
		WHERE 
			V.[numItemCode]=@numItemCode 
			AND V.[numDomainID]=@numDomainID 
	END

	IF EXISTS
	(
		SELECT 
			ISNULL([monCost],0) ListPrice 
		FROM 
			[Vendor] V 
		INNER JOIN 
			dbo.Item I 
		ON 
			V.numItemCode = I.numItemCode
		WHERE
			V.[numItemCode]=@numItemCode 
			AND V.[numDomainID]=@numDomainID 
			AND V.numVendorID=@numDivisionID
	)       
	BEGIN      
		SELECT 
			@monListPrice=ISNULL([monCost],0) 
		FROM 
			[Vendor] V 
		INNER JOIN 
			dbo.Item I 
		ON 
			V.numItemCode = I.numItemCode
		WHERE 
			V.[numItemCode]=@numItemCode 
			AND V.[numDomainID]=@numDomainID 
			AND V.numVendorID=@numDivisionID
	END      
	ELSE
	BEGIN   
		SELECT 
			@monListPrice=ISNULL([monCost],0) 
		FROM 
			[Vendor] 
		WHERE 
			[numItemCode]=@numItemCode 
			AND [numDomainID]=@numDomainID
	END      
 

	SELECT TOP 1 
		1 AS numUnitHour,
		ISNULL(dbo.GetPriceBasedOnPriceBook(P.[numPricRuleID],@monListPrice,@units,I.numItemCode),0) AS ListPrice,
		vcRuleName AS vcPOppName,
		CONVERT(VARCHAR(20), NULL) AS bintCreatedDate,
		CASE 
		WHEN numPricRuleID IS NULL THEN 'Price - List price'
		ELSE  
				CASE 
				WHEN P.tintPricingMethod = 1 THEN 'Price Book Rule'
				ELSE 
					(CASE tintRuleType
						WHEN 1 THEN 'Deduct from List price '
						WHEN 2 THEN 'Add to primary vendor cost '
						ELSE ''
					END) + 
					(CASE 
						WHEN tintDiscountType = 1 THEN CONVERT(VARCHAR(20), decDiscount) + '%'
						ELSE 'flat ' + CONVERT(VARCHAR(20), CAST(decDiscount AS int))
					END) + ' for every ' + 
					CONVERT(VARCHAR(20), (CASE WHEN [intQntyItems] = 0 THEN 1 ELSE intQntyItems END))
					+ ' units, until maximum of '
					+ CONVERT(VARCHAR(20), decMaxDedPerAmt)
					+ (CASE WHEN tintDiscountType = 1 THEN ' %' ELSE ' Flat' END)
				END
		END AS OppStatus,
		P.numPricRuleID,
		P.tintPricingMethod,
		@numDivisionID AS numDivisionID
	FROM
		Item I 
	LEFT JOIN PriceBookRules P ON I.numDomainID = P.numDomainID
	LEFT JOIN PriceBookRuleDTL PDTL ON P.numPricRuleID = PDTL.numRuleID
	LEFT JOIN PriceBookRuleItems PBI ON P.numPricRuleID = PBI.numRuleID
	LEFT JOIN [PriceBookPriorities] PP ON PP.[Step2Value] = P.[tintStep2] AND PP.[Step3Value] = P.[tintStep3]
	WHERE   
		numItemCode = @numItemCode 
		AND tintRuleFor=@tintOppType
		AND (
			((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 1
			OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND (tintStep3= 1 AND PDTL.numValue = @numDivisionID)) -- Priority 2
			OR ((tintStep2 = 3) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 3

			OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 4
			OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 5
			OR ((tintStep2 = 3) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 6

			OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 3)) -- Priority 7
			OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND (tintStep3 = 3)) -- Priority 8
			OR ((tintStep2 = 3) and (tintStep3 = 3)) -- Priority 9
			)
	ORDER BY PP.Priority ASC


	select 1 as numUnitHour,convert(money,ISNULL((monTotAmount/(case when numUnitHour< 1 then 1 else numUnitHour end)),0)) as ListPrice,vcPOppName,convert(varchar(20),bintCreatedDate) as bintCreatedDate,case when tintOppStatus=0 then 'Opportunity' 
		 when tintOppStatus=1 then 'Deal won'  
		 when tintOppStatus=2 then 'Deal Lost' end as OppStatus
		 ,CAST(0 AS NUMERIC) AS numPricRuleID,CAST(0 AS TINYINT) AS tintPricingMethod,@numDivisionID as numDivisionID                         
		 from OpportunityItems itm                                        
		 join OpportunityMaster mst                                        
		 on mst.numOppId=itm.numOppId                                        
		 where  tintOppType=2 and numItemCode=@numItemCode and mst.numDivisionID=@numDivisionID order by OppStatus DESC

 		select 1 as numUnitHour,convert(money,ISNULL(@monListPrice,0)) as ListPrice,'' as vcPOppName,convert(varchar(20),null) as bintCreatedDate,''  as OppStatus,@numDivisionID as numDivisionID
 		,CAST(0 AS NUMERIC) AS numPricRuleID,CAST(0 AS TINYINT) AS tintPricingMethod
END

END TRY
BEGIN CATCH
    SELECT 0 AS ListPrice
    SELECT 0 AS ListPrice
    SELECT 0 AS ListPrice
END CATCH
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ItemUnitPriceApproval_Check' ) 
    DROP PROCEDURE USP_ItemUnitPriceApproval_Check
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Sandeep Patel
-- Create date: 1 July 2014
-- Description:	Checks wheather unit price approval is required or not for item
-- =============================================
CREATE PROCEDURE USP_ItemUnitPriceApproval_Check
	@numDomainID numeric(18,0),
	@numDivisionID numeric(18,0),
	@numItemCode numeric(18,0),
	@numQuantity FLOAT,
	@numUnitPrice float,
	@numTotalAmount float,
	@numWarehouseItemID numeric(18,0),
	@numAbovePercent float,
	@numAboveField float,
	@numBelowPercent float,
	@numBelowField float
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	DECLARE @IsApprovalRequired BIT
	DECLARE @ItemAbovePrice FLOAT
	DECLARE @ItemBelowPrice FLOAT

	SET @IsApprovalRequired = 0
	SET @ItemAbovePrice = 0
	SET @ItemBelowPrice = 0

	IF @numQuantity > 0
		SET @numUnitPrice = (@numTotalAmount / @numQuantity)
	ELSE
		SET @numUnitPrice = 0


	IF @numAboveField > 0
	BEGIN
		IF @numAboveField = 1 -- Primaty Vendor Cost
			IF (SELECT COUNT(*) FROM Item WHERE numDomainID = @numDomainID AND numItemCode = @numItemCode  AND (bitKitParent = 1 OR bitAssembly = 1)) > 0 -- Kit Or Assembly
				SELECT @ItemAbovePrice = dbo.GetUnitPriceForKitOrAssembly(@numDomainID,@numItemCode,@numWarehouseItemID,@numQuantity)
			ELSE
				SELECT @ItemAbovePrice = ISNULL(monCost,0) FROM Vendor WHERE numItemCode = @numItemCode AND numVendorID = (SELECT numVendorID FROM Item WHERE numItemCode = @numItemCode)
		ELSE IF @numAboveField = 2 -- Average Cost
			SELECT @ItemAbovePrice = ISNULL(monAverageCost,0) FROM Item WHERE numDomainID = @numDomainID AND numItemCode = @numItemCode

		IF @ItemAbovePrice > 0
		BEGIN
			IF @numUnitPrice < (@ItemAbovePrice + (@ItemAbovePrice * (@numAbovePercent/100)))
				SET @IsApprovalRequired = 1
		END
		ELSE IF @ItemAbovePrice = 0
		BEGIN
			SET @IsApprovalRequired = 1
		END
	END

	IF @IsApprovalRequired = 0 AND @numBelowField > 0
	BEGIN
		IF @numBelowField = 1 -- List Price
		BEGIN
			IF (SELECT ISNULL(charItemType,'') AS charItemType FROM Item WHERE numDomainID = @numDomainID AND numItemCode = @numItemCode) = 'P' -- Inventory Item
				If (SELECT COUNT(*) FROM Item WHERE numDomainID = @numDomainID AND numItemCode = @numItemCode AND (bitKitParent = 1 OR bitAssembly = 1)) > 0 -- Kit OR Assembly
					SELECT @ItemBelowPrice = dbo.GetUnitPriceForKitOrAssembly(@numDomainID,@numItemCode,@numWarehouseItemID,@numQuantity)
				ELSE
					SELECT @ItemBelowPrice = ISNULL(monWListPrice,0) FROM WareHouseItems WHERE numDomainID = @numDomainID AND numItemID = @numItemCode AND numWareHouseItemID = @numWarehouseItemID
			ELSE
				SELECT @ItemBelowPrice = ISNULL(monListPrice,0) FROM Item WHERE numDomainID = @numDomainID AND numItemCode = @numItemCode
		END
		ELSE IF @numBelowField = 2 -- Price Rule
		BEGIN
			/* Check if valid price book rules exists for sales in domain */
			IF (SELECT COUNT(*) FROM PriceBookRules WHERE numDomainId = @numDomainID AND tintRuleFor = 1 AND tintStep2 > 0 AND tintStep3 > 0) > 0
			BEGIN
				DECLARE @i INT = 0
				DECLARE @Count int = 0
				DECLARE @tempPriority INT
				DECLARE @tempNumPriceRuleID NUMERIC(18,0)
				DECLARE @numPriceRuleIDApplied NUMERIC(18,0) = 0
				DECLARE @TempTable TABLE (numID INT, numRuleID numeric(18,0), numPriority INT)

				INSERT INTO 
					@TempTable
				SELECT 
					ROW_NUMBER() OVER (ORDER BY PriceBookPriorities.Priority) AS id,
					numPricRuleID,
					PriceBookPriorities.Priority 
				FROM 
					PriceBookRules
				INNER JOIN
					PriceBookPriorities
				ON
					PriceBookRules.tintStep2 = PriceBookPriorities.Step2Value AND
					PriceBookRules.tintStep3 = PriceBookPriorities.Step3Value
				WHERE 
					PriceBookRules.numDomainId = @numDomainID AND 
					PriceBookRules.tintRuleFor = 1 AND 
					PriceBookRules.tintStep2 > 0 AND PriceBookRules.tintStep3 > 0
				ORDER BY
					PriceBookPriorities.Priority

				SELECT @Count = COUNT(*) FROM @TempTable

				/* Loop all price rule with priority */
				WHILE (@i < @count)
				BEGIN
					SELECT @tempNumPriceRuleID = numRuleID, @tempPriority = numPriority FROM @TempTable WHERE numID = (@i + 1)
					
					/* IF proprity is 9 then price rule is applied to all items and all customers. 
					So price rule must be applied to item or not.*/
					IF @tempPriority = 9
					BEGIN
						SET @numPriceRuleIDApplied = @tempNumPriceRuleID
						BREAK
					END
					/* Check if current item exists in rule. if eixts then exit loop with rule id else continie loop. if item not exist in any rule then nothing to check */
					ELSE
					BEGIN
						DECLARE @isRuleApplicable BIT = 0
						EXEC @isRuleApplicable = dbo.CheckIfPriceRuleApplicableToItem @numRuleID = @tempNumPriceRuleID, @numItemID = @numItemCode, @numDivisionID = @numDivisionID 

						IF @isRuleApplicable = 1
						BEGIN
							SET @numPriceRuleIDApplied = @tempNumPriceRuleID
							BREAK
						END
					END

					SET @i = @i + 1
				END

				/* If @numPriceRuleIDApplied > 0 Get final unit price limit after applying below rule */
				PRINT @numPriceRuleIDApplied
				IF @numPriceRuleIDApplied > 0
				BEGIN
					EXEC @ItemBelowPrice = GetUnitPriceAfterPriceRuleApplication @numRuleID = @numPriceRuleIDApplied, @numDomainID = @numDomainID, @numItemCode = @numItemCode, @numQuantity = @numQuantity, @numWarehouseItemID = @numWarehouseItemID, @numDivisionID = @numDivisionID
				END
			END
		END
		ELSE IF @numBelowField = 3 -- Price Level
		BEGIN
			EXEC @ItemBelowPrice = GetUnitPriceAfterPriceLevelApplication @numDomainID = @numDomainID, @numItemCode = @numItemCode, @numQuantity = @numQuantity, @numWarehouseItemID = @numWarehouseItemID, @isPriceRule = 0, @numPriceRuleID = 0, @numDivisionID = @numDivisionID
		END
		
		If @ItemBelowPrice > 0
		BEGIN
			IF @numUnitPrice < (@ItemBelowPrice - (@ItemBelowPrice * (@numBelowPercent/100)))
				SET @IsApprovalRequired = 1
		END
		ELSE IF @ItemBelowPrice = 0
		BEGIN
			SET @IsApprovalRequired = 1
		END
	END

	SELECT @IsApprovalRequired
END
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_ManageInventory' ) 
    DROP PROCEDURE usp_ManageInventory
GO
CREATE PROCEDURE [dbo].[usp_ManageInventory]
    @itemcode AS NUMERIC(9) = 0,
    @numWareHouseItemID AS NUMERIC(9) = 0,
    @monAmount AS MONEY,
    @tintOpptype AS TINYINT,
    @numUnits AS FLOAT,
    @QtyShipped AS FLOAT,
    @QtyReceived AS FLOAT,
    @monPrice AS MONEY,
    @tintFlag AS TINYINT,  --1: SO/PO insert/edit 2:SO/PO Close 3:SO/PO Re-Open 4:Sales Fulfillment Release 5:Sales Fulfillment Revert
    @numOppID as numeric(9)=0,
    @numoppitemtCode AS NUMERIC(9)=0,
    @numUserCntID AS NUMERIC(9)=0,
	@bitWorkOrder AS BIT = 0   
AS 
    DECLARE @onHand AS FLOAT                                    
    DECLARE @onOrder AS FLOAT 
    DECLARE @onBackOrder AS FLOAT
    DECLARE @onAllocation AS FLOAT
	DECLARE @onReOrder AS FLOAT
    DECLARE @monAvgCost AS MONEY 
    DECLARE @bitKitParent BIT
	DECLARE @numOrigUnits AS NUMERIC		
	DECLARE @description AS VARCHAR(100)
	DECLARE @bitAsset as BIT
	DECLARE @numDomainID AS NUMERIC
	DECLARE @bitReOrderPoint AS BIT = 0
	DECLARE @numReOrderPointOrderStatus AS NUMERIC(18,0) = 0   
      
	SELECT @bitAsset=ISNULL(bitAsset,0),@numDomainID=numDomainID from Item where numItemCode=@itemcode--Added By Sachin
	SELECT @bitReOrderPoint=ISNULL(bitReOrderPoint,0),@numReOrderPointOrderStatus=ISNULL(numReOrderPointOrderStatus,0) FROM Domain WHERE numDomainID = @numDomainID
    
	SELECT  @monAvgCost = ISNULL(monAverageCost, 0),
            @bitKitParent = ( CASE WHEN ISNULL(bitKitParent,0) = 1
                               AND ISNULL(bitAssembly,0) = 1 THEN 0
                          WHEN ISNULL(bitKitParent,0) = 1 THEN 1
                          ELSE 0
                     END )
    FROM    Item
    WHERE   numitemcode = @itemcode                                   
    
    IF @numWareHouseItemID>0
    BEGIN                     
		SELECT  @onHand = ISNULL(numOnHand, 0),
				@onAllocation = ISNULL(numAllocation, 0),
				@onOrder = ISNULL(numOnOrder, 0),
				@onBackOrder = ISNULL(numBackOrder, 0),
				@onReOrder = ISNULL(numReorder,0)
		FROM    WareHouseItems
		WHERE   numWareHouseItemID = @numWareHouseItemID 
    END
   
   DECLARE @dtItemReceivedDate AS DATETIME;SET @dtItemReceivedDate=NULL
   
   DECLARE @TotalOnHand AS NUMERIC;SET @TotalOnHand=0  
   SELECT @TotalOnHand=ISNULL((SUM(numOnHand) + SUM(numAllocation)), 0) FROM dbo.WareHouseItems WHERE numItemID=@itemcode
					
   SET @numOrigUnits=@numUnits
            
    IF @tintFlag = 1  --1: SO/PO insert/edit 
        BEGIN                                   
            IF @tintOpptype = 1 
            BEGIN  
			--When change below code also change to USP_ManageWorkOrderStatus  

				/* When QtyShipped has been updated form sales fulfillment then it need to be deducted */
				SET @description='SO insert/edit (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'
				
				IF @bitKitParent = 1 
                BEGIN
                    EXEC USP_UpdatingInventoryForEmbeddedKits @numoppitemtCode,@numOrigUnits, 0,@numOppID,0,@numUserCntID
                END 
				ELSE IF @bitWorkOrder = 1
				BEGIN
					SET @description='SO-WO insert/edit (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'
					-- MANAGE INVENTOY OF ASSEMBLY ITEMS
					EXEC USP_ManageInventoryAssemblyWO @numUserCntID,@numOppID,@itemcode,@numWareHouseItemID,@QtyShipped,1
				END
                ELSE
				BEGIN       
					SET @numUnits = @numUnits - @QtyShipped
                                                        
                    IF @onHand >= @numUnits 
                    BEGIN                                    
                        SET @onHand = @onHand - @numUnits                            
                        SET @onAllocation = @onAllocation + @numUnits                                    
                    END                                    
                    ELSE IF @onHand < @numUnits 
                    BEGIN                                    
                        SET @onAllocation = @onAllocation + @onHand                                    
                        SET @onBackOrder = @onBackOrder + @numUnits
                            - @onHand                                    
                        SET @onHand = 0                                    
                    END    
			                                 

					IF @bitAsset=0--Not Asset
					BEGIN 
						UPDATE  WareHouseItems
						SET     numOnHand = @onHand,
								numAllocation = @onAllocation,
								numBackOrder = @onBackOrder,dtModified = GETDATE() 
						WHERE   numWareHouseItemID = @numWareHouseItemID  


						-- IF REORDER POINT IS ENABLED FROM GLOABAL SETTING THEN CREATE PURCHASE OPPORTUNITY
						If (@onHand + @onOrder <= @onReOrder) AND @bitReOrderPoint = 1 AND @onReOrder > 0
						BEGIN
							BEGIN TRY
								DECLARE @numNewOppID AS NUMERIC(18,0) = 0
								EXEC USP_OpportunityMaster_CreatePO @numNewOppID OUTPUT,@numDomainID,@numUserCntID,@itemcode,@onBackOrder,@numWareHouseItemID,0,0,@numReOrderPointOrderStatus
							END TRY
							BEGIN CATCH
								--WE ARE NOT THROWING ERROR
							END CATCH
						END         
					END
				END  
            END                       
            ELSE IF @tintOpptype = 2 
            BEGIN  
				SET @description='PO insert/edit (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Received:' +  CAST(@QtyReceived AS VARCHAR(10)) + ')'
		            
				/* When QtyReceived has been updated form purchase fulfillment then it need to be deducted */
                SET @numUnits = @numUnits - @QtyReceived   
                                             
                SET @onOrder = @onOrder + @numUnits 
		    
                UPDATE  
					WareHouseItems
                SET     
					numOnHand = @onHand,
                    numAllocation = @onAllocation,
                    numBackOrder = @onBackOrder,
                    numOnOrder = @onOrder,dtModified = GETDATE() 
                WHERE   
					numWareHouseItemID = @numWareHouseItemID   
            END                                                                                                                                         
        END
	 
    ELSE IF @tintFlag = 2 --2:SO/PO Close
            BEGIN
	 	
                PRINT '@OppType=' + CONVERT(VARCHAR(5), @tintOpptype)           
                IF @tintOpptype = 1 
                    BEGIN   
					SET @description='SO Close (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'
                    
                    IF @bitKitParent = 1 
                    BEGIN
                        EXEC USP_UpdatingInventoryForEmbeddedKits @numoppitemtCode,
                            @numOrigUnits, 2,@numOppID,0,@numUserCntID
                    END  
					ELSE IF @bitWorkOrder = 1
					BEGIN
						SET @description='SO-WO Close (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'
						EXEC USP_ManageInventoryAssemblyWO @numUserCntID,@numOppID,@itemcode,@numWareHouseItemID,@QtyShipped,2
					END
                    ELSE
                    BEGIN       
                    
    /* When QtyShipped has been updated form sales fulfillment then it need to be deducted */
							SET @numUnits = @numUnits - @QtyShipped
					  IF @bitAsset=0--Not Asset
							  BEGIN
									SET @onAllocation = @onAllocation - @numUnits 
							  END
					  ELSE--Asset
							  BEGIN
									 SET @onAllocation = @onAllocation  
							  END
											           
		    
							UPDATE  WareHouseItems
							SET     numOnHand = @onHand,
									numAllocation = @onAllocation,
									numBackOrder = @onBackOrder,dtModified = GETDATE() 
							WHERE   numWareHouseItemID = @numWareHouseItemID   
					   END
	
                    END                
                ELSE IF @tintOpptype = 2 
                        BEGIN
					SET @description='PO Close (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Received:' +  CAST(@QtyReceived AS VARCHAR(10)) + ')'
                        
        /* When QtyReceived has been updated form purchase fulfillment then it need to be deducted */
                            SET @numUnits = @numUnits - @QtyReceived
            
							IF EXISTS(SELECT 1 FROM dbo.OpportunityMaster WHERE numOppID=@numOppID AND ISNULL(bitStockTransfer,0)=0)
							BEGIN
								--Updating the Average Cost
								IF @TotalOnHand + @numUnits <= 0
								BEGIN
									  SET @monAvgCost = @monAvgCost --( ( @TotalOnHand * @monAvgCost ) + (@numUnits * @monPrice))
								END
								ELSE
								BEGIN
									  SET @monAvgCost = ( ( @TotalOnHand * @monAvgCost )
												+ (@numUnits * @monPrice))
								/ ( @TotalOnHand + @numUnits )
								END    
		                            
								UPDATE  item
								SET     monAverageCost = @monAvgCost
								WHERE   numItemCode = @itemcode
							END
		    
--                            SELECT  @OldOnHand = SUM(numonhand)
--                            FROM    WareHouseItems WHI
--                            WHERE   WHI.numitemid = @itemcode     
--                            SET @OldCost = @OldOnHand * @monAvgCost     
      
                            IF @onOrder >= @numUnits 
                                BEGIN            
                                    PRINT 'in @onOrder >= @numUnits '
                                    SET @onOrder = @onOrder - @numUnits            
                                    IF @onBackOrder >= @numUnits 
                                        BEGIN            
                                            PRINT 'in @onBackOrder >= @numUnits '
                                            PRINT @numUnits
                                            SET @onBackOrder = @onBackOrder
                                                - @numUnits            
                                            SET @onAllocation = @onAllocation
                                                + @numUnits            
                                        END            
                                    ELSE 
                                        BEGIN            
                                            SET @onAllocation = @onAllocation
                                                + @onBackOrder            
                                            SET @numUnits = @numUnits
                                                - @onBackOrder            
                                            SET @onBackOrder = 0            
                                            SET @onHand = @onHand + @numUnits            
                                        END         
                                END            
                            ELSE IF @onOrder < @numUnits 
                                    BEGIN            
                                        PRINT 'in @onOrder < @numUnits '
                                        SET @onHand = @onHand + @onOrder
                                        SET @onOrder = @numUnits - @onOrder
                                    END         
   -- To Find New Stock Details    
--    Print '@numUnits='+convert(varchar(5),@numUnits)    
--    Print '@monPrice='+Convert(varchar(5),@monPrice)    
--                            SET @NewCost = @numUnits * @monPrice    
    ---Set @NewAverageCost=(@OldCost+@NewCost)/@onHand    
    --Print '@NewAverageCost='+Convert(varchar(5),@NewAverageCost)    
                            UPDATE  WareHouseItems
                            SET     numOnHand = @onHand,
                                    numAllocation = @onAllocation,
                                    numBackOrder = @onBackOrder,
                                    numOnOrder = @onOrder,dtModified = GETDATE() 
                            WHERE   numWareHouseItemID = @numWareHouseItemID  
			                
						SELECT @dtItemReceivedDate=dtItemReceivedDate FROM OpportunityMaster WHERE numOppId=@numOppId
			                
                --ASk Carl for more Info
--                            UPDATE  Item
--                            SET     monAverageCost = @NewAverageCost
--                            WHERE   numItemCode = @itemcode         
                        END
            END

ELSE IF @tintFlag = 3 --3:SO/PO Re-Open
            BEGIN
	 	
                PRINT '@OppType=' + CONVERT(VARCHAR(5), @tintOpptype)           
                IF @tintOpptype = 1 
                    BEGIN         
					SET @description='SO Re-Open (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'
                    
                    IF @bitKitParent = 1 
                        BEGIN
                            EXEC USP_UpdatingInventoryForEmbeddedKits @numoppitemtCode,
                                @numOrigUnits, 3,@numOppID,0,@numUserCntID
                        END 
					ELSE IF @bitWorkOrder = 1
					BEGIN
						SET @description='SO-WO Re-Open (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'
						EXEC USP_ManageInventoryAssemblyWO @numUserCntID,@numOppID,@itemcode,@numWareHouseItemID,@QtyShipped,3
					END
                     ELSE
                     BEGIN
    /* When QtyShipped has been updated form sales fulfillment then it need to be deducted */
                        SET @numUnits = @numUnits - @QtyShipped
  
					 IF @bitAsset=0--Not Asset
							  BEGIN
									SET @onAllocation = @onAllocation + @numUnits
							  END
					  ELSE--Asset
							  BEGIN
									 SET @onAllocation = @onAllocation 
							  END
                                    
	    
                        UPDATE  WareHouseItems
                        SET     numOnHand = @onHand,
                                numAllocation = @onAllocation,
                                numBackOrder = @onBackOrder,dtModified = GETDATE() 
                        WHERE   numWareHouseItemID = @numWareHouseItemID
					  END
						               
                    END                
                ELSE IF @tintOpptype = 2 
                        BEGIN
					SET @description='PO Re-Open (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Received:' +  CAST(@QtyReceived AS VARCHAR(10)) + ')'
                        
        /* When QtyReceived has been updated form purchase fulfillment then it need to be deducted */
                            SET @numUnits = @numUnits - @QtyReceived
            
            
							--Updating the Average Cost
							IF EXISTS(SELECT 1 FROM dbo.OpportunityMaster WHERE numOppID=@numOppID AND ISNULL(bitStockTransfer,0)=0)
							BEGIN
								--IF @onHand + @onOrder - @numUnits <> 0 
								--BEGIN
								IF @TotalOnHand - @numUnits <= 0
								BEGIN
									SET @monAvgCost = 0
	--								SET @monAvgCost = ( ( @TotalOnHand * @monAvgCost )
	--                                                     - (@numUnits * @monPrice))
								END
								ELSE
								BEGIN
									SET @monAvgCost = ( ( @TotalOnHand * @monAvgCost )
														 - (@numUnits * @monPrice))
										/ ( @TotalOnHand - @numUnits )
								END        
								--END
							--ELSE 
								--SET @monAvgCost = 0
                            
                            
								 UPDATE  item
								 SET     monAverageCost = @monAvgCost
								 WHERE   numItemCode = @itemcode
							END

                            IF @onHand >= @numUnits 
                                BEGIN     
                                     SET @onHand = @onHand - @numUnits            
                                     SET @onOrder = @onOrder + @numUnits
                                END         
                          
						   UPDATE  WareHouseItems
                            SET     numOnHand = @onHand,
                                    numOnOrder = @onOrder,dtModified = GETDATE() 
                            WHERE   numWareHouseItemID = @numWareHouseItemID  
                            
                        END
            END
            
            
            
             IF @numWareHouseItemID>0
				 BEGIN
					DECLARE @tintRefType AS TINYINT
					
					IF @tintOpptype = 1 --SO
					  SET @tintRefType=3
					ELSE IF @tintOpptype = 2 --PO
					  SET @tintRefType=4
					  
						DECLARE @numDomain AS NUMERIC(18,0)
						SET @numDomain = (SELECT TOP 1 [numDomainID] FROM WareHouseItems WHERE [WareHouseItems].[numWareHouseItemID] = @numWareHouseItemID)

						EXEC dbo.USP_ManageWareHouseItems_Tracking
						@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
						@numReferenceID = @numOppID, --  numeric(9, 0)
						@tintRefType = @tintRefType, --  tinyint
						@vcDescription = @description, --  varchar(100)
						@numModifiedBy = @numUserCntID,
						@dtRecordDate =  @dtItemReceivedDate,
						@numDomainID = @numDomain
                  END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageInventoryAssemblyWO')
DROP PROCEDURE USP_ManageInventoryAssemblyWO
GO
CREATE PROCEDURE [dbo].[USP_ManageInventoryAssemblyWO]
	@numUserCntID AS NUMERIC(18,0),
	@numOppID AS NUMERIC(9),
	@numItemCode as numeric(9),
	@numWarehouseItemID AS NUMERIC(18,0),
	@QtyShipped AS FLOAT,
	@tintMode AS TINYINT=0 --1: Work Order insert/edit 2:Work Order Close 3:Work Order Re-Open 4: Work Order Delete 5: Work Order Edit(Revert Before New Insert)
AS
BEGIN
	/*  
		WHEN WORK ORDER IS CREATED FOR ASSEMBY THEN BOTH SO AND WO PROCESS ARE CARRIED OUT SEPERATELY.
		FOR EXAMPLE: LETS SAY WE HAVE "ASSEMBY ITEM" WHICH REQUIRED 2 QTY OF "ASSEMBLY SUB ITEM" 
		CURRENT INVENTORY STATUS FOR PARENT ITEM: ONHAND:1 ONORDER:0 ONALLOCATION:0 ONBACKORDER:0
		CURRENT INVENTORY STATUS FOR CHILD ITEM: ONHAND:4 ONORDER:0 ONALLOCATION:0 ONBACKORDER:0

		-- WHEN WORK ORDER IS CREATED FOR 1 QTY:
		CURRENT INVENTORY STATUS FOR PARENT ITEM: ONHAND:0 ONORDER:1 ONALLOCATION:1 ONBACKORDER:0
		CURRENT INVENTORY STATUS FOR CHILD ITEM: ONHAND:2 ONORDER:0 ONALLOCATION:2 ONBACKORDER:0

		HERE PARENT ITEM HAVE BOTH SO AND WO IMPACT. SO ONHAND DECREASE AND ONALLOCATION IS INCREASED
		BUT ITS WORK ORDER SO WE HAVE TO INCERASE ONORDER ALSO BY QTY. CHILD ITEM HAVE ONLY WO IMPACT MEANS 
		ONHAND IS DECREASE AND ONALLOCATION IS INCREASED

		-- WHEN ASSEMBLY IS COMPLETED
		CURRENT INVENTORY STATUS FOR PARENT ITEM: ONHAND:1 ONORDER:0 ONALLOCATION:1 ONBACKORDER:0
		CURRENT INVENTORY STATUS FOR CHILD ITEM: ONHAND:2 ONORDER:0 ONALLOCATION:0 ONBACKORDER:0

		-- WHEN WORK ORDER IS CLOSED:
		CURRENT INVENTORY STATUS FOR PARENT ITEM: ONHAND:1 ONORDER:0 ONALLOCATION:0 ONBACKORDER:0
		CURRENT INVENTORY STATUS FOR CHILD ITEM: ONHAND:2 ONORDER:0 ONALLOCATION:0 ONBACKORDER:0
	*/
DECLARE @numWOID AS NUMERIC(18,0)
DECLARE @numParentWOID AS NUMERIC(18,0)
DECLARE	@numDomainID AS NUMERIC(18,0)
DECLARE @vcInstruction AS VARCHAR(1000)
DECLARE @bintCompliationDate DATETIME
DECLARE @numAssemblyQty FLOAT 
DECLARE @numWOStatus AS NUMERIC(18,0)
DECLARE @numWOAssignedTo AS NUMERIC(18,0)

SET XACT_ABORT ON;

BEGIN TRY 
BEGIN TRAN
	SELECT 
		@numWOID=numWOId,@numAssemblyQty=numQtyItemsReq,@numDomainID=numDomainID,
		@numWOStatus=ISNULL(numWOStatus,0),@vcInstruction=vcInstruction,
		@bintCompliationDate= bintCompliationDate, @numWOAssignedTo=numAssignedTo,@numParentWOID=ISNULL(numParentWOID,0)
	FROM 
		WorkOrder 
	WHERE 
		numOppId=@numOppID AND numItemCode=@numItemCode AND numWareHouseItemId=@numWarehouseItemID

	--IF WORK ORDER IS FOUND FOR ITEM AND QTY IS GREATER THEN 0 THEN PROCESS INVENTORY
	IF ISNULL(@numWOID,0) > 0 AND ISNULL(@numAssemblyQty,0) > 0
	BEGIN
		
		DECLARE @TEMPITEM AS TABLE
		(
			RowNo INT NOT NULL identity(1,1),
			numItemCode INT,
			numQty FLOAT,
			numOrgQtyRequired INT,
			numWarehouseItemID INT,
			bitAssembly BIT
		)

		INSERT INTO 
			@TEMPITEM 
		SELECT 
			numChildItemID,
			numQtyItemsReq,
			numQtyItemsReq_Orig,
			numWareHouseItemId,
			Item.bitAssembly 
		FROM 
			WorkOrderDetails 
		INNER JOIN 
			Item 
		ON 
			WorkOrderDetails.numChildItemID=Item.numItemCode 
		WHERE 
			numWOId = @numWOID 
			AND ISNULL(numWareHouseItemId,0) > 0

		DECLARE @i AS INT = 1
		DECLARE @Count AS INT

		SELECT @Count = COUNT(RowNo) FROM @TEMPITEM

		--IF ASSEMBLY SUB ITEMS ARE AVAILABLE THEN UPDATE INVENTORY OF SUB ITEMS
		IF ISNULL(@Count,0) > 0
		BEGIN
			DECLARE @Description AS VARCHAR(500)
			DECLARE @numChildItemCode AS INT
			DECLARE @numChildItemQty AS FLOAT
			DECLARE @numChildOrgQtyRequired AS INT
			DECLARE @bitChildIsAssembly AS BIT
			DECLARE @numQuantityToBuild AS FLOAT
			DECLARE @numChildItemWarehouseItemID AS INT
			DECLARE @numOnHand AS FLOAT
			DECLARE @numOnOrder AS FLOAT
			DECLARE @numOnAllocation AS FLOAT
			DECLARE @numOnBackOrder AS FLOAT

			--THIS BLOCKS UPDATE INVENTORY LEVEL OF TOP LEVEL PARENT ONLY AND NOT CALLED FOR ITS RECURSIVE CHILD
			IF @numParentWOID = 0
			BEGIN
				--GET ASSEMBY ITEM INVENTORY
				SELECT
					@numOnHand = ISNULL(numOnHand, 0),
					@numOnAllocation = ISNULL(numAllocation, 0),
					@numOnOrder = ISNULL(numOnOrder, 0),
					@numOnBackOrder = ISNULL(numBackOrder, 0)
				FROM
					WareHouseItems
				WHERE
					numWareHouseItemID = @numWarehouseItemID

				IF @tintMode = 1 --WO INSERT
				BEGIN
					-- IF HAVE ENOUGH ITEM QTY IN WAREHOUSE THEN 
					-- DECREASE ONHAND BY QTY AND 
					-- INCREASE ONALLOCATION BY QTY
					-- ONORDER GOES UP BY ASSEMBLY QTY
					IF @numOnHand >= @numAssemblyQty 
					BEGIN      
						SET @numOnHand = @numOnHand - @numAssemblyQty
						SET @numOnOrder = @numOnOrder + @numAssemblyQty
						SET @numOnAllocation = @numOnAllocation + @numAssemblyQty

						UPDATE 
							WareHouseItems
						SET    
							numOnHand = @numOnHand,
							numOnOrder= @numOnOrder,
							numAllocation = @numOnAllocation,
							dtModified = GETDATE() 
						WHERE   
							numWareHouseItemID = @numWareHouseItemID                                                              
					END    
					-- IF DO NOT HAVE ENOUGH ITEM QTY IN WAREHOUSE THEN 
					-- INCREASE ONALLOCATION BY QTY WE HAVE IN WAREHOUSE AND
					-- REMAINING QTY AFTER ALLLOCATING FROM ONHAND WILL GO TO BACKORDER
					-- SET ONHAND TO 0 BECAUSE NOTHING LEFT NOW IN WAREHOUSE FOR ITEM 
					-- ONORDER GOES UP BY ASSEMBLY QTY				 
					ELSE IF @numOnHand < @numAssemblyQty 
					BEGIN      
						SET @numOnAllocation = @numOnAllocation + @numOnHand
						SET @numOnBackOrder = @numOnBackOrder + (@numAssemblyQty - @numOnHand)
						SET @numOnOrder = @numOnOrder + @numAssemblyQty
						SET @numOnHand = 0
 
						UPDATE 
							WareHouseItems
						SET    
							numAllocation = @numOnAllocation,
							numBackOrder = @numOnBackOrder,
							numOnHand = @numOnHand,		
							numOnOrder= @numOnOrder,				
							dtModified = GETDATE() 
						WHERE   
							numWareHouseItemID = @numWareHouseItemID                                     
					END 
				--UPDATE ONORDER QTY OF ASSEMBLY ITEM BY QTY
				END
				ELSE IF @tintMode = 2 --WO CLOSE
				BEGIN
					--REMOVE SHIPPED QTY FROM ORIGIAL QUANTITY
					SET @numAssemblyQty = ISNULL(@numAssemblyQty,0) - ISNULL(@QtyShipped,0)

					--RELASE ALLOCATION
					SET @numOnAllocation = @numOnAllocation - @numAssemblyQty 
											           
					UPDATE  
						WareHouseItems
					SET     
						numAllocation = @numOnAllocation,
						dtModified = GETDATE() 
					WHERE   
						numWareHouseItemID = @numWareHouseItemID   
				END
				ELSE IF @tintMode = 3 --WO REOPEN
				BEGIN
					--REMOVE SHIPPED QTY FROM ORIGIAL QUANTITY
					SET @numAssemblyQty = @numAssemblyQty - @QtyShipped
  
					--ADD ITEM TO ALLOCATION
					SET @numOnAllocation = @numOnAllocation + @numAssemblyQty
                                    
					UPDATE  
						WareHouseItems
					SET     
						numAllocation = @numOnAllocation,
						dtModified = GETDATE() 
					WHERE   
						numWareHouseItemID = @numWareHouseItemID
				END
				ELSE IF (@tintMode = 4 OR  @tintMode=5) --WO DELETE OR EDIT
				BEGIN
					-- CHECK WHETHER ANY QTY IS SHIPPED OR NOT
					IF @QtyShipped > 0
					BEGIN
						IF @tintMode=4
								SET @numAssemblyQty = @numAssemblyQty - @QtyShipped
						ELSE IF @tintmode=5
								SET @numOnAllocation = @numOnAllocation + @QtyShipped 
					END 
	
					-- IF BACKORDER QTY IS GREATER THEN QTY TO REVERT THEN
					-- DECREASE BACKORDER QTY BY QTY TO REVERT
					IF @numAssemblyQty < @numOnBackOrder 
					BEGIN                  
						SET @numOnBackOrder = @numOnBackOrder - @numAssemblyQty
					END 
					-- IF BACKORDER QTY IS LESS THEN OR EQUAL TO QTY TO REVERT THEN
					-- DECREASE QTY TO REVERT BY QTY OF BACK ORDER AND
					-- SET BACKORDER QTY TO 0
					ELSE IF @numAssemblyQty >= @numOnBackOrder 
					BEGIN
						SET @numAssemblyQty = @numAssemblyQty - @numOnBackOrder
						SET @numOnBackOrder = 0
                        
						--REMOVE ITEM FROM ALLOCATION 
						IF (@numOnAllocation - @numAssemblyQty) >= 0
							SET @numOnAllocation = @numOnAllocation - @numAssemblyQty
						
						--ADD QTY TO ONHAND
						SET @numOnHand = @numOnHand + @numAssemblyQty
					END

					--THIS IF BLOCK SHOULD BE LAST OTHERWISE IT WILL CREATE PROBLEM IN INVENTORY LEVEL
					--IF WORK ORDER IS NOT COMPLETED THEN REVERT 
					--QTY FROM ONORDER BECAUSE WE HAVE TO REVERT
					IF @numWOStatus <> 23184
					BEGIN
						--DECREASE ONORDER BY ASSEMBLY QTY
						SET @numOnOrder = @numOnOrder - @numAssemblyQty
					END
					ELSE
					BEGIN
						--DECREASE ONHAND BY ASSEMBLY QTY
						--BECAUSE WHEN WORK ORDER IS COMPLETED ONHAND QTY OF PARENT IS INCREASED
						SET @numOnHand = @numOnHand - @numAssemblyQty
					END

					UPDATE 
						WareHouseItems
					SET    
						numOnHand = @numOnHand,
						numAllocation = @numOnAllocation,
						numBackOrder = @numOnBackOrder,
						numOnOrder = @numOnOrder,
						dtModified = GETDATE() 
					WHERE   
						numWareHouseItemID = @numWareHouseItemID				
				END
			END

			--CHILD ITEM INVENTORY IS CHANGED ONLY WHEN SALES ORDER WITH WORK ORDER IS INSERTED OR DELETED
			--THE INVENTORY MANGMENT FOR DELETE ACTION IS HANDLED FROM STORE PROCEDURE USP_DeleteAssemblyWOAndInventory
			IF @tintMode = 1
			BEGIN
				
				WHILE @i <= @Count
				BEGIN
					DECLARE @numNewOppID AS NUMERIC(18,0) = 0
					DECLARE @QtyToBuild AS FLOAT
					DECLARE @numQtyShipped AS FLOAT

					--GET CHILD ITEM DETAIL FROM TEMP TABLE
					SELECT @numChildItemCode=numItemCode,@numChildItemWarehouseItemID=numWarehouseItemID,@numChildItemQty=numQty,@numChildOrgQtyRequired=numOrgQtyRequired,@bitChildIsAssembly=bitAssembly FROM @TEMPITEM WHERE RowNo = @i
					
					--GET CHILD ITEM CURRENT INVENTORY DETAIL
					SELECT  
						@numOnHand = ISNULL(numOnHand, 0),
						@numOnAllocation = ISNULL(numAllocation, 0),
						@numOnOrder = ISNULL(numOnOrder, 0),
						@numOnBackOrder = ISNULL(numBackOrder, 0)
					FROM    
						WareHouseItems
					WHERE   
						numWareHouseItemID = @numChildItemWarehouseItemID
					
					IF ISNULL(@numParentWOID,0) = 0
					BEGIN
						SET @Description='Items Allocated For SO-WO (Qty:' + CAST(@numChildItemQty AS VARCHAR(10)) + ' Shipped:' +  CAST((@QtyShipped * @numChildOrgQtyRequired) AS VARCHAR(10)) + ')'
					END
					ELSE
					BEGIN
						SET @Description='Items Allocated For SO-WO Work Order (Qty:' + CAST(@numChildItemQty AS VARCHAR(10)) + ' Shipped:' +  CAST((@QtyShipped * @numChildOrgQtyRequired) AS VARCHAR(10)) + ')'
					END
					
					SET @numChildItemQty = @numChildItemQty - (@QtyShipped * @numChildOrgQtyRequired)
						
					--IF CHILD ITEM IS ASSEMBLY THEN SAME LOGIC LIKE PARENT IS APPLIED
					IF ISNULL(@bitChildIsAssembly,0) = 1
					BEGIN
						-- IF HAVE ENOUGH ITEM QTY IN WAREHOUSE THEN 
						-- DECREASE ONHAND BY QTY AND 
						-- INCREASE ONALLOCATION BY QTY
						-- ONORDER GOES UP BY ASSEMBLY QTY
						IF @numOnHand >= @numChildItemQty 
						BEGIN      
							SET @numOnHand = @numOnHand - @numChildItemQty
							SET @numOnAllocation = @numOnAllocation + @numChildItemQty  
							
							--DONOT PLACE THIS LOGIC AT BOTTOM BECAUSE WORK ORDER ARE CREATED RECURSIVELY WHICH CREATE PROBLEM IN WAREHOUSE TRAKING ENTRIES
							--UPDATE WAREHOUSE INVENTORY AND WAREHOUSE HISTORY
							EXEC USP_UpdateInventoryAndTracking 
							@numOnHand=@numOnHand,
							@numOnAllocation=@numOnAllocation,
							@numOnBackOrder=@numOnBackOrder,
							@numOnOrder=@numOnOrder,
							@numWarehouseItemID=@numChildItemWarehouseItemID,
							@numReferenceID=@numOppID,
							@tintRefType=3,
							@numDomainID=@numDomainID,
							@numUserCntID=@numUserCntID,
							@Description=@Description                                                         
						END    
						-- IF DO NOT HAVE ENOUGH ITEM QTY IN WAREHOUSE THEN 
						-- INCREASE ONALLOCATION BY QTY WE HAVE IN WAREHOUSE AND
						-- REMAINING QTY AFTER ALLLOCATING FROM ONHAND WILL GO TO BACKORDER
						-- SET ONHAND TO 0 BECAUSE NOTHING LEFT NOW IN WAREHOUSE FOR ITEM 
						-- ONORDER GOES UP BY ASSEMBLY QTY				 
						ELSE IF @numOnHand < @numChildItemQty 
						BEGIN      
							SET @numOnAllocation = @numOnAllocation + @numOnHand
							SET @numOnBackOrder = @numOnBackOrder + (@numChildItemQty - @numOnHand)
							SET @QtyToBuild = (@numChildItemQty - @numOnHand)
							SET @numQtyShipped = (@QtyShipped * @numChildOrgQtyRequired)
							SET @numOnHand = 0   

							--DONOT PLACE THIS LOGIC AT BOTTOM BECAUSE WORK ORDER ARE CREATED RECURSIVELY WHICH CREATE PROBLEM IN WAREHOUSE TRAKING ENTRIES
							--UPDATE WAREHOUSE INVENTORY AND WAREHOUSE HISTORY
							EXEC USP_UpdateInventoryAndTracking 
							@numOnHand=@numOnHand,
							@numOnAllocation=@numOnAllocation,
							@numOnBackOrder=@numOnBackOrder,
							@numOnOrder=@numOnOrder,
							@numWarehouseItemID=@numChildItemWarehouseItemID,
							@numReferenceID=@numOppID,
							@tintRefType=3,
							@numDomainID=@numDomainID,
							@numUserCntID=@numUserCntID,
							@Description=@Description
								
							EXEC USP_WorkOrder_InsertRecursive @numOppID,@numChildItemCode,@QtyToBuild,@numChildItemWarehouseItemID,@numDomainID,@numUserCntID,@numQtyShipped,@numWOID,0
						END 
					END
					ELSE
					BEGIN
						-- IF HAVE ENOUGH ITEM QTY IN WAREHOUSE THEN 
						-- DECREASE ONHAND BY QTY AND 
						-- INCREASE ONALLOCATION BY QTY
						IF @numOnHand >= @numChildItemQty 
						BEGIN                                    
							SET @numOnHand = @numOnHand - @numChildItemQty                            
							SET @numOnAllocation = @numOnAllocation + @numChildItemQty 
							
							--DONOT PLACE THIS LOGIC AT BOTTOM BECAUSE WORK ORDER ARE CREATED RECURSIVELY WHICH CREATE PROBLEM IN WAREHOUSE TRAKING ENTRIES
							--UPDATE WAREHOUSE INVENTORY AND WAREHOUSE HISTORY
							EXEC USP_UpdateInventoryAndTracking 
							@numOnHand=@numOnHand,
							@numOnAllocation=@numOnAllocation,
							@numOnBackOrder=@numOnBackOrder,
							@numOnOrder=@numOnOrder,
							@numWarehouseItemID=@numChildItemWarehouseItemID,
							@numReferenceID=@numOppID,
							@tintRefType=3,
							@numDomainID=@numDomainID,
							@numUserCntID=@numUserCntID,
							@Description=@Description                                   
						END    
						-- IF DO NOT HAVE ENOUGH ITEM QTY IN WAREHOUSE THEN 
						-- INCREASE ONALLOCATION BY QTY WE HAVE IN WAREHOUSE AND
						-- REMAINING QTY AFTER ALLLOCATING FROM ONHAND WILL GO TO BACKORDER
						-- SET ONHAND TO 0 BECAUSE NOTHING LEFT NOW IN WAREHOUSE FOR ITEM 				 
						ELSE IF @numOnHand < @numChildItemQty 
						BEGIN     
								
							SET @numOnAllocation = @numOnAllocation + @numOnHand  
							SET @numOnBackOrder = @numOnBackOrder + (@numChildItemQty - @numOnHand) 
							SET @QtyToBuild = (@numChildItemQty - @numOnHand) 
							SET @numOnHand = 0 

							--DONOT PLACE THIS LOGIC AT BOTTOM BECAUSE WORK ORDER ARE CREATED RECURSIVELY WHICH CREATE PROBLEM IN WAREHOUSE TRAKING ENTRIES
							--UPDATE WAREHOUSE INVENTORY AND WAREHOUSE HISTORY
							EXEC USP_UpdateInventoryAndTracking 
							@numOnHand=@numOnHand,
							@numOnAllocation=@numOnAllocation,
							@numOnBackOrder=@numOnBackOrder,
							@numOnOrder=@numOnOrder,
							@numWarehouseItemID=@numChildItemWarehouseItemID,
							@numReferenceID=@numOppID,
							@tintRefType=3,
							@numDomainID=@numDomainID,
							@numUserCntID=@numUserCntID,
							@Description=@Description

							--CREATE PURCHASE ORDER FOR QUANTITY WHICH GOES ON BACKORDER
							EXEC USP_OpportunityMaster_CreatePO @numNewOppID OUTPUT,@numDomainID,@numUserCntID,@numChildItemCode,@QtyToBuild,@numChildItemWarehouseItemID,1,1,0
								
							UPDATE WorkOrderDetails SET numPOID= @numNewOppID WHERE numWOId=@numWOID AND numChildItemID=@numChildItemCode AND numWareHouseItemId=@numChildItemWarehouseItemID                                 
						END 
					END
 
					
					SET @i = @i + 1
				END
			END
		END
	END
COMMIT TRAN 
END TRY 
BEGIN CATCH		
    IF ( @@TRANCOUNT > 0 ) 
    BEGIN
        ROLLBACK TRAN
    END

	DECLARE @ErrorMessage NVARCHAR(4000);
    DECLARE @ErrorSeverity INT;
    DECLARE @ErrorState INT;

    SELECT @ErrorMessage = ERROR_MESSAGE(), @ErrorSeverity = ERROR_SEVERITY(), @ErrorState = ERROR_STATE();

    RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);
END CATCH	

SET XACT_ABORT OFF;
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageInventoryWorkOrder')
DROP PROCEDURE USP_ManageInventoryWorkOrder
GO
CREATE PROCEDURE [dbo].[USP_ManageInventoryWorkOrder]
@numWOId AS NUMERIC(18,0),
@numDomainID AS NUMERIC(18,0),
@numUserCntID AS NUMERIC(18,0)
AS
BEGIN
BEGIN TRY 
BEGIN TRAN
		
	DECLARE @TEMPITEM AS TABLE
	(
		RowNo INT NOT NULL identity(1,1),
		numItemCode INT,
		numQty FLOAT,
		numOrgQtyRequired INT,
		numWarehouseItemID INT,
		bitAssembly BIT
	)

	INSERT INTO @TEMPITEM SELECT numChildItemID,numQtyItemsReq,numQtyItemsReq_Orig,numWareHouseItemId,Item.bitAssembly FROM WorkOrderDetails INNER JOIN Item ON WorkOrderDetails.numChildItemID=Item.numItemCode WHERE numWOId = @numWOID AND ISNULL(numWareHouseItemId,0) > 0

	DECLARE @i AS INT = 1
	DECLARE @Count AS INT

	SELECT @Count = COUNT(RowNo) FROM @TEMPITEM

	--IF ASSEMBLY SUB ITEMS ARE AVAILABLE THEN UPDATE INVENTORY OF SUB ITEMS
	IF ISNULL(@Count,0) > 0
	BEGIN
		DECLARE @Description AS VARCHAR(1000)
		DECLARE @numChildItemCode AS INT
		DECLARE @numChildItemQty AS FLOAT
		DECLARE @numChildOrgQtyRequired AS FLOAT
		DECLARE @bitChildIsAssembly AS BIT
		DECLARE @numQuantityToBuild AS FLOAT
		DECLARE @numChildItemWarehouseItemID AS INT
		DECLARE @numOnHand AS FLOAT
		DECLARE @numOnOrder AS FLOAT
		DECLARE @numOnAllocation AS FLOAT
		DECLARE @numOnBackOrder AS FLOAT

		WHILE @i <= @Count
		BEGIN
			DECLARE @numNewOppID AS NUMERIC(18,0) = 0
			DECLARE @QtyToBuild AS FLOAT
			DECLARE @numQtyShipped AS FLOAT

			--GET CHILD ITEM DETAIL FROM TEMP TABLE
			SELECT @numChildItemCode=numItemCode,@numChildItemWarehouseItemID=numWarehouseItemID,@numChildItemQty=numQty,@numChildOrgQtyRequired=numOrgQtyRequired,@bitChildIsAssembly=bitAssembly FROM @TEMPITEM WHERE RowNo = @i
					
			--GET CHILD ITEM CURRENT INVENTORY DETAIL
			SELECT  
				@numOnHand = ISNULL(numOnHand, 0),
				@numOnAllocation = ISNULL(numAllocation, 0),
				@numOnOrder = ISNULL(numOnOrder, 0),
				@numOnBackOrder = ISNULL(numBackOrder, 0)
			FROM    
				WareHouseItems
			WHERE   
				numWareHouseItemID = @numChildItemWarehouseItemID	

			SET @Description='Items Allocated For Work Order (Qty:' + CAST(@numChildItemQty AS VARCHAR(10)) + ')'
						
			--IF CHILD ITEM IS ASSEMBLY THEN SAME LOGIC LIKE PARENT IS APPLIED
			IF ISNULL(@bitChildIsAssembly,0) = 1
			BEGIN
				-- IF HAVE ENOUGH ITEM QTY IN WAREHOUSE THEN 
				-- DECREASE ONHAND BY QTY AND 
				-- INCREASE ONALLOCATION BY QTY
				-- ONORDER GOES UP BY ASSEMBLY QTY
				IF @numOnHand >= @numChildItemQty 
				BEGIN      
					SET @numOnHand = @numOnHand - @numChildItemQty
					SET @numOnAllocation = @numOnAllocation + @numChildItemQty  
					
					--DONOT PLACE THIS LOGIC AT BOTTOM BECAUSE WORK ORDER ARE CREATED RECURSIVELY WHICH CREATE PROBLEM IN WAREHOUSE TRAKING ENTRIES
					--UPDATE WAREHOUSE INVENTORY AND WAREHOUSE HISTORY
					EXEC USP_UpdateInventoryAndTracking 
					@numOnHand=@numOnHand,
					@numOnAllocation=@numOnAllocation,
					@numOnBackOrder=@numOnBackOrder,
					@numOnOrder=@numOnOrder,
					@numWarehouseItemID=@numChildItemWarehouseItemID,
					@numReferenceID=@numWOId,
					@tintRefType=2,
					@numDomainID=@numDomainID,
					@numUserCntID=@numUserCntID,
					@Description=@Description                                                         
				END    
				-- IF DO NOT HAVE ENOUGH ITEM QTY IN WAREHOUSE THEN 
				-- INCREASE ONALLOCATION BY QTY WE HAVE IN WAREHOUSE AND
				-- REMAINING QTY AFTER ALLLOCATING FROM ONHAND WILL GO TO BACKORDER
				-- SET ONHAND TO 0 BECAUSE NOTHING LEFT NOW IN WAREHOUSE FOR ITEM 
				-- ONORDER GOES UP BY ASSEMBLY QTY				 
				ELSE IF @numOnHand < @numChildItemQty 
				BEGIN      
					SET @numOnAllocation = @numOnAllocation + @numOnHand
					SET @numOnBackOrder = @numOnBackOrder + (@numChildItemQty - @numOnHand)
					SET @QtyToBuild = (@numChildItemQty - @numOnHand)
					SET @numQtyShipped = 0
					SET @numOnHand = 0   
					
					--DONOT PLACE THIS LOGIC AT BOTTOM BECAUSE WORK ORDER ARE CREATED RECURSIVELY WHICH CREATE PROBLEM IN WAREHOUSE TRAKING ENTRIES
					--UPDATE WAREHOUSE INVENTORY AND WAREHOUSE HISTORY	
					EXEC USP_UpdateInventoryAndTracking 
					@numOnHand=@numOnHand,
					@numOnAllocation=@numOnAllocation,
					@numOnBackOrder=@numOnBackOrder,
					@numOnOrder=@numOnOrder,
					@numWarehouseItemID=@numChildItemWarehouseItemID,
					@numReferenceID=@numWOId,
					@tintRefType=2,
					@numDomainID=@numDomainID,
					@numUserCntID=@numUserCntID,
					@Description=@Description 

					EXEC USP_WorkOrder_InsertRecursive 0,@numChildItemCode,@QtyToBuild,@numChildItemWarehouseItemID,@numDomainID,@numUserCntID,@numQtyShipped,@numWOID,1
				END 
			END
			ELSE
			BEGIN
				-- IF HAVE ENOUGH ITEM QTY IN WAREHOUSE THEN 
				-- DECREASE ONHAND BY QTY AND 
				-- INCREASE ONALLOCATION BY QTY
				IF @numOnHand >= @numChildItemQty 
				BEGIN                                    
					SET @numOnHand = @numOnHand - @numChildItemQty                            
					SET @numOnAllocation = @numOnAllocation + @numChildItemQty        
					
					--DONOT PLACE THIS LOGIC AT BOTTOM BECAUSE WORK ORDER ARE CREATED RECURSIVELY WHICH CREATE PROBLEM IN WAREHOUSE TRAKING ENTRIES
					--UPDATE WAREHOUSE INVENTORY AND WAREHOUSE HISTORY
					EXEC USP_UpdateInventoryAndTracking 
					@numOnHand=@numOnHand,
					@numOnAllocation=@numOnAllocation,
					@numOnBackOrder=@numOnBackOrder,
					@numOnOrder=@numOnOrder,
					@numWarehouseItemID=@numChildItemWarehouseItemID,
					@numReferenceID=@numWOId,
					@tintRefType=2,
					@numDomainID=@numDomainID,
					@numUserCntID=@numUserCntID,
					@Description=@Description                             
				END    
				-- IF DO NOT HAVE ENOUGH ITEM QTY IN WAREHOUSE THEN 
				-- INCREASE ONALLOCATION BY QTY WE HAVE IN WAREHOUSE AND
				-- REMAINING QTY AFTER ALLLOCATING FROM ONHAND WILL GO TO BACKORDER
				-- SET ONHAND TO 0 BECAUSE NOTHING LEFT NOW IN WAREHOUSE FOR ITEM 				 
				ELSE IF @numOnHand < @numChildItemQty 
				BEGIN     
								
					SET @numOnAllocation = @numOnAllocation + @numOnHand  
					SET @numOnBackOrder = @numOnBackOrder + (@numChildItemQty - @numOnHand) 
					SET @QtyToBuild = (@numChildItemQty - @numOnHand) 
					SET @numOnHand = 0 

					--DONOT PLACE THIS LOGIC AT BOTTOM BECAUSE WORK ORDER ARE CREATED RECURSIVELY WHICH CREATE PROBLEM IN WAREHOUSE TRAKING ENTRIES
					--UPDATE WAREHOUSE INVENTORY AND WAREHOUSE HISTORY
					EXEC USP_UpdateInventoryAndTracking 
					@numOnHand=@numOnHand,
					@numOnAllocation=@numOnAllocation,
					@numOnBackOrder=@numOnBackOrder,
					@numOnOrder=@numOnOrder,
					@numWarehouseItemID=@numChildItemWarehouseItemID,
					@numReferenceID=@numWOId,
					@tintRefType=2,
					@numDomainID=@numDomainID,
					@numUserCntID=@numUserCntID,
					@Description=@Description 

					--CREATE PURCHASE ORDER FOR QUANTITY WHICH GOES ON BACKORDER
					EXEC USP_OpportunityMaster_CreatePO @numNewOppID OUTPUT,@numDomainID,@numUserCntID,@numChildItemCode,@QtyToBuild,@numChildItemWarehouseItemID,1,1,0
								
					UPDATE WorkOrderDetails SET numPOID= @numNewOppID WHERE numWOId=@numWOID AND numChildItemID=@numChildItemCode AND numWareHouseItemId=@numChildItemWarehouseItemID                                 
				END 
			END
					
			SET @i = @i + 1
		END
	END
COMMIT TRAN 
END TRY 
BEGIN CATCH		
    IF ( @@TRANCOUNT > 0 ) 
    BEGIN
        ROLLBACK TRAN
    END

	DECLARE @ErrorMessage NVARCHAR(4000);
    DECLARE @ErrorSeverity INT;
    DECLARE @ErrorState INT;

    SELECT @ErrorMessage = ERROR_MESSAGE(), @ErrorSeverity = ERROR_SEVERITY(), @ErrorState = ERROR_STATE();

    RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);
END CATCH	
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ManageReturnBizDocs' ) 
    DROP PROCEDURE USP_ManageReturnBizDocs
GO

CREATE PROCEDURE [dbo].[USP_ManageReturnBizDocs]
    (
      @numReturnHeaderID NUMERIC(9) = 0,
      @tintReceiveType TINYINT,
      @numReturnStatus NUMERIC(9),
      @numDomainId NUMERIC(9),
      @numUserCntID NUMERIC(9),
      @numAccountID NUMERIC(9),
      @vcCheckNumber VARCHAR(50),
      @IsCreateRefundReceipt BIT,
      @numItemCode	NUMERIC(18,0)
    )
AS 
BEGIN
BEGIN TRY
BEGIN TRANSACTION
    DECLARE @tintType TINYINT,@tintReturnType TINYINT
	
    SELECT @tintReturnType=tintReturnType FROM ReturnHeader WHERE numReturnHeaderID = @numReturnHeaderID    
    
	PRINT @tintReturnType
	PRINT @tintReceiveType

    IF @tintReturnType=1 OR @tintReturnType=2 
    BEGIN
		DECLARE @i INT = 1
		DECLARE @COUNT AS INT 
		DECLARE @numTempOppID AS NUMERIC(18,0)
		DECLARE @numTempOppItemID AS NUMERIC(18,0)
		DECLARE @bitTempLotNo AS BIT
		DECLARE @numTempQty AS FLOAT 
		DECLARE @numTempWareHouseItmsDTLID AS INT
			
		DECLARE @TempOppSerial TABLE
		(
			ID INT IDENTITY(1,1),
			numWarehouseItmsDTLID NUMERIC(18,0),
			numQty FLOAT,
			bitLotNo NUMERIC(18,0),
			numOppId NUMERIC(18,0),
			numOppItemID NUMERIC(18,0)
		)

		INSERT INTO @TempOppSerial
		(
			numWarehouseItmsDTLID,
			numQty,
			bitLotNo,
			numOppId,
			numOppItemID
		)
		SELECT	
			OWSIReturn.numWarehouseItmsDTLID,
			OWSIReturn.numQty,
			I.bitLotNo,
			RH.numOppId,
			RI.numOppItemID
		FROM
			ReturnHeader RH
		INNER JOIN
			ReturnItems RI
		ON
			RH.numReturnHeaderID = RI.numReturnHeaderID
		INNER JOIN
			OppWarehouseSerializedItem OWSIReturn
		ON
			RH.numReturnHeaderID = OWSIReturn.numReturnHeaderID
			AND RI.numReturnItemID = OWSIReturn.numReturnItemID
		INNER JOIN
			OpportunityItems OI
		ON
			RI.numOppItemID = OI.numoppitemtCode
		INNER JOIN
			Item I
		ON
			OI.numItemCode = I.numItemCode
		WHERE
			RH.numReturnHeaderID = @numReturnHeaderID

		--SALES RETURN
		IF @tintReturnType = 1
		BEGIN
			-- MAKE SERIAL/LOT# NUUMBER AVAILABLE FOR USE AS ITEM IT IS RETURNED TO WAREHOUSE
			UPDATE WHIDL
				SET WHIDL.numQty = (CASE WHEN Item.bitLotNo = 1 THEN ISNULL(WHIDL.numQty,0) + ISNULL(OWSI.numQty,0) ELSE 1 END)
			FROM 
				WareHouseItmsDTL WHIDL
			INNER JOIN
				OppWarehouseSerializedItem OWSI
			ON
				WHIDL.numWareHouseItmsDTLID = OWSI.numWarehouseItmsDTLID
				AND WHIDL.numWareHouseItemID = OWSI.numWarehouseItmsID
			INNER JOIN
				WareHouseItems
			ON
				WHIDL.numWareHouseItemID = WareHouseItems.numWareHouseItemID
			INNER JOIN
				Item
			ON
				WareHouseItems.numItemID = Item.numItemCode
			WHERE
				OWSI.numReturnHeaderID=@numReturnHeaderID
		END
		ELSE IF @tintReturnType = 2
		BEGIN
			IF (SELECT
						COUNT(*)
				FROM
					WareHouseItmsDTL WHIDL
				INNER JOIN
					OppWarehouseSerializedItem OWSI
				ON
					WHIDL.numWareHouseItmsDTLID = OWSI.numWarehouseItmsDTLID
					AND WHIDL.numWareHouseItemID = OWSI.numWarehouseItmsID
				INNER JOIN
					WareHouseItems
				ON
					WHIDL.numWareHouseItemID = WareHouseItems.numWareHouseItemID
				INNER JOIN
					Item
				ON
					WareHouseItems.numItemID = Item.numItemCode
				WHERE
					numReturnHeaderID=@numReturnHeaderID
					AND 1 = (CASE 
								WHEN Item.bitLotNo = 1 AND ISNULL(WHIDL.numQty,0) < ISNULL(OWSI.numQty,0) THEN 1 
								WHEN Item.bitSerialized = 1 AND ISNULL(WHIDL.numQty,0) = 0 THEN 1 
								ELSE 0 
							END)
				) > 0
			BEGIN
				RAISERROR ('SERIAL/LOT#_USED', 16, 1 ) ;
			END
			ELSE
			BEGIN
				-- REMOVE SERIAL/LOT# NUUMBER FROM INVENTORY
				UPDATE WHIDL
					SET WHIDL.numQty = (CASE WHEN Item.bitLotNo = 1 THEN ISNULL(WHIDL.numQty,0) - ISNULL(OWSI.numQty,0) ELSE 0 END)
				FROM 
					WareHouseItmsDTL WHIDL
				INNER JOIN
					OppWarehouseSerializedItem OWSI
				ON
					WHIDL.numWareHouseItmsDTLID = OWSI.numWarehouseItmsDTLID
					AND WHIDL.numWareHouseItemID = OWSI.numWarehouseItmsID
				INNER JOIN
					WareHouseItems
				ON
					WHIDL.numWareHouseItemID = WareHouseItems.numWareHouseItemID
				INNER JOIN
					Item
				ON
					WareHouseItems.numItemID = Item.numItemCode
				WHERE
					numReturnHeaderID=@numReturnHeaderID
			END
		END

		--REMOVE ENTERIES SERIAL/LOT# ENTERIES FROM OppWarehouseSerializedItem TABLE WHICH ARE ASSOCIATED WITH ORDER BECAUSE NOW SERIALS ARE RETURNED TO/FROM WAREHOUSE
		SELECT @COUNT = COUNT(*) FROM @TempOppSerial

		WHILE @i <= @COUNT
		BEGIN
			SELECT
				@bitTempLotNo = bitLotNo,
				@numTempQty = numQty,
				@numTempOppID = numOppId,
				@numTempOppItemID = numOppItemID,
				@numTempWareHouseItmsDTLID = numWareHouseItmsDTLID
			FROM
				@TempOppSerial
			WHERE
				ID = @i

			-- LOT ITEM
			IF @bitTempLotNo = 1
			BEGIN
				-- IF RETURN QTY IS SAME AS ITEM ORDERED QTY DELETE ROW ELSE DECREASE QTY
				IF ISNULL((SELECT numQty FROM OppWarehouseSerializedItem WHERE numOppID=@numTempOppID AND numOppItemID=@numTempOppItemID AND numWarehouseItmsDTLID=@numTempWareHouseItmsDTLID),0) = @numTempQty
				BEGIN
					DELETE FROM OppWarehouseSerializedItem WHERE numOppID=@numTempOppID AND numOppItemID=@numTempOppItemID AND numWarehouseItmsDTLID=@numTempWareHouseItmsDTLID
				END
				ELSE
				BEGIN
					UPDATE 
						OppWarehouseSerializedItem
					SET 
						numQty = ISNULL(numQty,0) - ISNULL(@numTempQty,0)
					WHERE
						numOppID=@numTempOppID 
						AND numOppItemID=@numTempOppItemID 
						AND numWarehouseItmsDTLID=@numTempWareHouseItmsDTLID
				END
			END
			ELSE
			BEGIN
				DELETE FROM OppWarehouseSerializedItem WHERE numOppID=@numTempOppID AND numOppItemID=@numTempOppItemID AND numWarehouseItmsDTLID=@numTempWareHouseItmsDTLID
			END

			SET @i = @i + 1
		END

        EXEC usp_ManageRMAInventory @numReturnHeaderID,@numDomainId,@numUserCntID,1 
    END   
    
	SET @tintType=(CASE When @tintReturnType=1 AND @tintReceiveType=1 THEN 5 
				       When @tintReturnType=1 AND @tintReceiveType=2 THEN 3 
				       When @tintReturnType=2 AND @tintReceiveType=2 THEN 4
				       ELSE 0 
					END) 
		
	PRINT @tintType

	IF 	@tintType>0
	BEGIN
		EXEC dbo.USP_UpdateBizDocNameTemplate @tintType, @numDomainID,@numReturnHeaderID
    END    
    	
    DECLARE @numBizdocTempID AS NUMERIC(18, 0);
	SET @numBizdocTempID=0
        
    IF @tintReturnType=1 OR @tintReturnType=2 
    BEGIN
		IF @tintReceiveType = 2 AND @tintReturnType=1
		BEGIN
		SELECT TOP 1 @numBizdocTempID = numBizDocTempID FROM    BizDocTemplate
			WHERE   numOppType = 1 AND bitDefault = 1 AND bitEnabled = 1
					AND numDomainID = @numDomainID AND numBizDocID IN (SELECT TOP 1 numListItemID
					FROM    dbo.ListDetails WHERE   vcData = 'Sales Credit Memo' AND constFlag = 1 )
		END
		ELSE IF @tintReturnType=1 OR @tintReceiveType=1
        BEGIN 
            SELECT TOP 1 @numBizdocTempID = numBizdocTempID FROM BizDocTemplate
                WHERE   numOppType = 1 AND bitDefault = 1 AND bitEnabled = 1 AND numDomainID = @numDomainID
					AND numBizDocID = ( SELECT TOP 1 numListItemID 
                    FROM    dbo.ListDetails WHERE   vcData = 'Refund Receipt' AND constFlag = 1)
        END
        ELSE IF @tintReceiveType = 2 AND @tintReturnType=2
        BEGIN
                SELECT TOP 1 @numBizdocTempID = numBizDocTempID FROM    BizDocTemplate
                WHERE   numOppType = 2 AND bitDefault = 1 AND bitEnabled = 1
                        AND numDomainID = @numDomainID AND numBizDocID IN (SELECT TOP 1 numListItemID
                        FROM    dbo.ListDetails WHERE   vcData = 'Purchase Credit Memo' AND constFlag = 1 )
        END 
	END
		
	DECLARE @monAmount AS money,@monTotalTax  AS money,@monTotalDiscount AS  money  
		
	SET @tintType=CASE When @tintReturnType=1 AND @tintReceiveType=1 THEN 9 
				    When @tintReturnType=1 AND @tintReceiveType=2 THEN 7 
				    When @tintReturnType=1 AND @tintReceiveType=3 THEN 9 
				    When @tintReturnType=2 AND @tintReceiveType=2 THEN 8
				    When @tintReturnType=3 THEN 10
				    When @tintReturnType=4 THEN 9 
					END 
				       
	SELECT @monAmount=monAmount,@monTotalTax=monTotalTax,@monTotalDiscount=monTotalDiscount FROM dbo.GetReturnDealAmount(@numReturnHeaderID,@tintType)
		
    UPDATE  dbo.ReturnHeader
    SET     tintReceiveType = @tintReceiveType,
            numReturnStatus = @numReturnStatus,
            numModifiedBy = @numUserCntID,
            dtModifiedDate = GETUTCDATE(),
            numAccountID = @numAccountID,
            vcCheckNumber = @vcCheckNumber,
            IsCreateRefundReceipt = @IsCreateRefundReceipt,
            numBizdocTempID = CASE When @numBizdocTempID>0 THEN @numBizdocTempID ELSE numBizdocTempID END,
            monBizDocAmount= @monAmount + @monTotalTax - @monTotalDiscount,
            numItemCode = @numItemCode
    WHERE   numReturnHeaderID = @numReturnHeaderID
        
	DECLARE @numDepositIDRef AS NUMERIC(18,0)
	SELECT @numDepositIDRef = ISNULL(numDepositIDRef,0) FROM [dbo].[ReturnHeader] AS RH WHERE [RH].[numReturnHeaderID] = @numReturnHeaderID
	PRINT @numDepositIDRef

	IF @numDepositIDRef > 0
	BEGIN
		IF NOT EXISTS(SELECT * FROM [dbo].[DepositeDetails] AS DD WHERE [DD].[numDepositID] = @numDepositIDRef) AND @tintReceiveType = 1 AND @tintReturnType = 4
		BEGIN
			UPDATE dbo.DepositMaster SET [monAppliedAmount] = ISNULL((SELECT SUM([ReturnHeader].[monAmount]) FROM [dbo].[ReturnHeader] 
																	WHERE [ReturnHeader].[numDepositIDRef] = @numDepositIDRef),0)
											,[numReturnHeaderID] = @numReturnHeaderID	
											,[monRefundAmount] = [monDepositAmount] - ISNULL((SELECT SUM([ReturnHeader].[monAmount]) FROM [dbo].[ReturnHeader] WHERE [ReturnHeader].[numDepositIDRef] = @numDepositIDRef),0) 
			WHERE numDepositId=@numDepositIDRef

		END
		ELSE
		BEGIN
			UPDATE dbo.DepositMaster SET [monAppliedAmount] = @monAmount + ISNULL((SELECT SUM([DD].[monAmountPaid]) FROM [dbo].[DepositeDetails] AS DD WHERE [DD].[numDepositID] = @numDepositIDRef),0)
											,[monRefundAmount] = [monDepositAmount] - ISNULL((SELECT SUM([DD].[monAmountPaid]) FROM [dbo].[DepositeDetails] AS DD WHERE [DD].[numDepositID] = @numDepositIDRef),0)
			WHERE numDepositId=@numDepositIDRef						
		END
	END
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
END
GO
/****** Object:  StoredProcedure [USP_ManageReturnHeaderItems]    ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ManageReturnHeaderItems' ) 
    DROP PROCEDURE USP_ManageReturnHeaderItems
GO
CREATE PROCEDURE [USP_ManageReturnHeaderItems]
    (
      @numReturnHeaderID NUMERIC(18, 0) OUTPUT,
      @vcRMA VARCHAR(100),
      @vcBizDocName VARCHAR(100),
      @numDomainId NUMERIC(18, 0),
      @numUserCntID AS NUMERIC(9) = 0,
      @numDivisionId NUMERIC(18, 0),
      @numContactId NUMERIC(18, 0),
      @numOppId NUMERIC(18, 0),
      @tintReturnType TINYINT,
      @numReturnReason NUMERIC(18, 0),
      @numReturnStatus NUMERIC(18, 0),
      @monAmount MONEY,
      @monTotalTax MONEY,
      @monTotalDiscount MONEY,
      @tintReceiveType TINYINT,
      @vcComments TEXT,
      @strItems TEXT = '',
      @tintMode TINYINT = 0,
      @numBillAddressId NUMERIC(18, 0),
      @numShipAddressId NUMERIC(18, 0),
      @numDepositIDRef NUMERIC(18,0),
      @numBillPaymentIDRef NUMERIC(18,0),
	  @numParentID NUMERIC(18,0) = 0
    )
AS 
    BEGIN 
        DECLARE @hDocItem INT                                                                                                                                                                
 
        IF @numReturnHeaderID = 0 
            BEGIN             
				-- GET ACCOUNT CLASS IF ENABLED
				DECLARE @numAccountClass AS NUMERIC(18) = 0

				DECLARE @tintDefaultClassType AS INT = 0
				SELECT @tintDefaultClassType = ISNULL(tintDefaultClassType,0) FROM Domain WHERE numDomainId = @numDomainID 

				IF @tintDefaultClassType = 1 --USER
				BEGIN
					SELECT 
						@numAccountClass=ISNULL(numDefaultClass,0) 
					FROM 
						dbo.UserMaster UM 
					JOIN 
						dbo.Domain D 
					ON 
						UM.numDomainID=D.numDomainId
					WHERE 
						D.numDomainId=@numDomainId 
						AND UM.numUserDetailId=@numUserCntID
				END
				ELSE IF @tintDefaultClassType = 2 --COMPANY
				BEGIN
					SELECT 
						@numAccountClass=ISNULL(numAccountClassID,0) 
					FROM 
						dbo.DivisionMaster DM 
					WHERE 
						DM.numDomainId=@numDomainId 
						AND DM.numDivisionID=@numDivisionID
				END
				ELSE
				BEGIN
					SET @numAccountClass = 0
				END
                   
                       INSERT  INTO [ReturnHeader]([vcRMA],[vcBizDocName],[numDomainId],[numDivisionId],[numContactId],
                                  [numOppId],[tintReturnType],[numReturnReason],[numReturnStatus],[monAmount],
                                  [monTotalTax],[monTotalDiscount],[tintReceiveType],[vcComments],
                                  [numCreatedBy],[dtCreatedDate],monBizDocAmount,monBizDocUsedAmount,numDepositIDRef,numBillPaymentIDRef,numAccountClass,numParentID,IsUnappliedPayment)
                                SELECT  @vcRMA,@vcBizDocName,@numDomainId,@numDivisionId,@numContactId,@numOppId,
                                        @tintReturnType,@numReturnReason,@numReturnStatus,@monAmount,
                                        @monTotalTax,@monTotalDiscount,@tintReceiveType,@vcComments,@numUserCntID,GETUTCDATE(),0,0,@numDepositIDRef,@numBillPaymentIDRef,@numAccountClass,
										@numParentID,
										(CASE WHEN (SELECT [DM].[tintDepositePage] FROM [dbo].[DepositMaster] AS DM 
													WHERE DM.[numDepositId] = @numDepositIDRef AND [DM].[numDomainId] = @numDomainId) = 2 THEN 1
											  ELSE 0
										END)
                    
                        SET @numReturnHeaderID = SCOPE_IDENTITY()                                        
                        SELECT  @numReturnHeaderID

                        --Update DepositMaster if Refund UnApplied Payment
                        IF @numDepositIDRef>0 AND @tintReturnType=4
                        BEGIN
							UPDATE dbo.DepositMaster SET monRefundAmount=@monAmount WHERE numDepositId=@numDepositIDRef
						END
						
						--Update BillPaymentHeader if Refund UnApplied Payment
						IF @numBillPaymentIDRef>0 AND @tintReturnType=4
						BEGIN
							UPDATE dbo.BillPaymentHeader SET monRefundAmount=@monAmount WHERE numBillPaymentID=@numBillPaymentIDRef
						END
						
                        DECLARE @tintType TINYINT 
                        SET @tintType=CASE When @tintReturnType=1 OR @tintReturnType=2 THEN 7 
												WHEN @tintReturnType=3 THEN 6
												WHEN @tintReturnType=4 THEN 5 END
												
                        EXEC dbo.USP_UpdateBizDocNameTemplate
								@tintType =  @tintType, --  tinyint
								@numDomainID = @numDomainID, --  numeric(18, 0)
								@RecordID = @numReturnHeaderID --  numeric(18, 0)

                        DECLARE @numRMATempID AS NUMERIC(18, 0)
                        DECLARE @numBizdocTempID AS NUMERIC(18, 0)

                        IF @tintReturnType=1 
                        BEGIN
							SELECT TOP 1 @numRMATempID = numBizDocTempID FROM BizDocTemplate
							WHERE   numOppType = 1 AND bitDefault = 1 AND bitEnabled = 1
                                AND numDomainID = @numDomainID AND numBizDocID IN ( SELECT TOP 1 numListItemID 
									    FROM   dbo.ListDetails WHERE  vcData = 'RMA' AND constFlag = 1 )							
						END
						ELSE IF @tintReturnType=2
                        BEGIN
							SELECT TOP 1 @numRMATempID = numBizDocTempID FROM BizDocTemplate
							WHERE   numOppType = 2 AND bitDefault = 1 AND bitEnabled = 1
                                AND numDomainID = @numDomainID AND numBizDocID IN ( SELECT TOP 1 numListItemID 
									    FROM   dbo.ListDetails WHERE  vcData = 'RMA' AND constFlag = 1 )	
						END			    
                        ELSE IF @tintReturnType=3
                        BEGIN
							SELECT TOP 1 @numRMATempID = numBizDocTempID FROM    BizDocTemplate
							WHERE   numOppType = 1 AND bitDefault = 1 AND bitEnabled = 1
                                AND numDomainID = @numDomainID AND numBizDocID IN ( SELECT TOP 1 numListItemID 
                                FROM   dbo.ListDetails WHERE  vcData = 'Credit Memo' AND constFlag = 1 )							
						END
						ELSE IF @tintReturnType=4
                        BEGIN
							SELECT TOP 1 @numRMATempID = numBizDocTempID FROM    BizDocTemplate
							WHERE   numOppType = 1 AND bitDefault = 1 AND bitEnabled = 1
                                AND numDomainID = @numDomainID AND numBizDocID IN ( SELECT TOP 1 numListItemID 
                                FROM   dbo.ListDetails WHERE  vcData = 'Refund Receipt' AND constFlag = 1 )							
                                
                            SET @numBizdocTempID=@numRMATempID    
						END
                                               
                        UPDATE  dbo.ReturnHeader
                        SET     numRMATempID = ISNULL(@numRMATempID,0),
                                numBizdocTempID = ISNULL(@numBizdocTempID,0)
                        WHERE   numReturnHeaderID = @numReturnHeaderID
			                
			                --Add/Update Address Details
        SET @numOppID = NULLIF(@numOppID, 0)
		            
        DECLARE @vcStreet VARCHAR(100),
            @vcCity VARCHAR(50),
            @vcPostalCode VARCHAR(15),
            @vcCompanyName VARCHAR(100),
            @vcAddressName VARCHAR(50)      
        DECLARE @numState NUMERIC(9),
            @numCountry NUMERIC(9),
            @numCompanyId NUMERIC(9) 
        DECLARE @bitIsPrimary BIT ;

        SELECT  @vcCompanyName = vcCompanyName,
                @numCompanyId = div.numCompanyID
        FROM    CompanyInfo Com
                JOIN divisionMaster Div ON div.numCompanyID = com.numCompanyID
        WHERE   div.numdivisionID = @numDivisionId

--Bill Address
        IF @numBillAddressId > 0 
            BEGIN
                SELECT  @vcStreet = ISNULL(vcStreet, ''),
                        @vcCity = ISNULL(vcCity, ''),
                        @vcPostalCode = ISNULL(vcPostalCode, ''),
                        @numState = ISNULL(numState, 0),
                        @numCountry = ISNULL(numCountry, 0),
                        @bitIsPrimary = bitIsPrimary,
                        @vcAddressName = vcAddressName
                FROM    dbo.AddressDetails
                WHERE   numDomainID = @numDomainID
                        AND numAddressID = @numBillAddressId
                EXEC dbo.USP_UpdateOppAddress @numOppID = @numOppID, --  numeric(9, 0)
                    @byteMode = 0, --  tinyint
                    @vcStreet = @vcStreet, --  varchar(100)
                    @vcCity = @vcCity, --  varchar(50)
                    @vcPostalCode = @vcPostalCode, --  varchar(15)
                    @numState = @numState, --  numeric(9, 0)
                    @numCountry = @numCountry, --  numeric(9, 0)
                    @vcCompanyName = @vcCompanyName, --  varchar(100)
                    @numCompanyId = 0, --  numeric(9, 0)
                    @vcAddressName = @vcAddressName,
                    @numReturnHeaderID = @numReturnHeaderID
            END
    
  --Ship Address
        IF @numShipAddressId > 0 
            BEGIN
                SELECT  @vcStreet = ISNULL(vcStreet, ''),
                        @vcCity = ISNULL(vcCity, ''),
                        @vcPostalCode = ISNULL(vcPostalCode, ''),
                        @numState = ISNULL(numState, 0),
                        @numCountry = ISNULL(numCountry, 0),
                        @bitIsPrimary = bitIsPrimary,
                        @vcAddressName = vcAddressName
                FROM    dbo.AddressDetails
                WHERE   numDomainID = @numDomainID
                        AND numAddressID = @numShipAddressId
 
                EXEC dbo.USP_UpdateOppAddress @numOppID = @numOppID, --  numeric(9, 0)
                    @byteMode = 1, --  tinyint
                    @vcStreet = @vcStreet, --  varchar(100)
                    @vcCity = @vcCity, --  varchar(50)
                    @vcPostalCode = @vcPostalCode, --  varchar(15)
                    @numState = @numState, --  numeric(9, 0)
                    @numCountry = @numCountry, --  numeric(9, 0)
                    @vcCompanyName = @vcCompanyName, --  varchar(100)
                    @numCompanyId = @numCompanyId, --  numeric(9, 0)
                    @vcAddressName = @vcAddressName,
                    @numReturnHeaderID = @numReturnHeaderID
            END
            
							IF @tintReturnType=1
							BEGIN    
								IF @numOppId>0
								BEGIN
									INSERT dbo.OpportunityMasterTaxItems 
									(
										numReturnHeaderID,
										numTaxItemID,
										fltPercentage,
										tintTaxType
									) 
									SELECT 
										@numReturnHeaderID,
										numTaxItemID,
										fltPercentage,
										tintTaxType
									FROM 
										OpportunityMasterTaxItems 
									WHERE 
										numOppID=@numOppID
								END
								ELSE
								BEGIN 
															--Insert Tax for Division                       
										INSERT dbo.OpportunityMasterTaxItems (
											numReturnHeaderID,
											numTaxItemID,
											fltPercentage,
											tintTaxType
										) 
										SELECT 
											@numReturnHeaderID,
											TI.numTaxItemID,
											TEMPTax.decTaxValue,
											TEMPTax.tintTaxType
										FROM 
											TaxItems TI 
										JOIN 
											DivisionTaxTypes DTT 
										ON 
											TI.numTaxItemID = DTT.numTaxItemID
										OUTER APPLY
										(
											SELECT
												decTaxValue,
												tintTaxType
											FROM
												dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,TI.numTaxItemID,NULL,0,@numReturnHeaderID)   
										) AS TEMPTax
										WHERE 
											DTT.numDivisionID=@numDivisionId AND DTT.bitApplicable=1
										UNION 
										SELECT 
											@numReturnHeaderID,
											0,
											TEMPTax.decTaxValue,
											TEMPTax.tintTaxType
										FROM 
											dbo.DivisionMaster 
										OUTER APPLY
										(
											SELECT
												decTaxValue,
												tintTaxType
											FROM
												dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,0,NULL,1,@numReturnHeaderID)  
										) AS TEMPTax
										WHERE 
											bitNoTax=0 
											AND numDivisionID=@numDivisionID
								END			  
							END
          
          IF CONVERT(VARCHAR(10), @strItems) <> '' 
            BEGIN
                PRINT 1
                EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems                                                                                                             
                SELECT  *
                INTO    #temp
                FROM    OPENXML (@hDocItem, '/NewDataSet/Item',2) WITH ( numReturnItemID NUMERIC, numItemCode NUMERIC, numUnitHour FLOAT, numUnitHourReceived FLOAT, monPrice DECIMAL(30,16), monTotAmount MONEY, vcItemDesc VARCHAR(1000), numWareHouseItemID NUMERIC, vcModelID VARCHAR(200), vcManufacturer VARCHAR(250), numUOMId NUMERIC,numOppItemID NUMERIC, bitDiscountType BIT, fltDiscount FLOAT)

                EXEC sp_xml_removedocument @hDocItem 
                
                        INSERT  INTO [ReturnItems]
                                (
                                  [numReturnHeaderID],
                                  [numItemCode],
                                  [numUnitHour],
                                  [numUnitHourReceived],
                                  [monPrice],
                                  [monTotAmount],
                                  [vcItemDesc],
                                  [numWareHouseItemID],
                                  [vcModelID],
                                  [vcManufacturer],
                                  [numUOMId],numOppItemID,
								  bitDiscountType,fltDiscount
                                )
                                SELECT  @numReturnHeaderID,
                                        numItemCode,
                                        numUnitHour,
                                        0,
                                        monPrice,
                                        monTotAmount,
                                        vcItemDesc,
                                        NULLIF(numWareHouseItemID, 0) numWareHouseItemID,
                                        vcModelID,
                                        vcManufacturer,
                                        numUOMId,numOppItemID,
										bitDiscountType,fltDiscount
                                FROM    #temp
                                WHERE   numReturnItemID = 0
					
                     
                DROP TABLE #temp
   
						IF @tintReturnType=1
						BEGIN               
							IF @numOppId>0
								BEGIN
									INSERT INTO dbo.OpportunityItemsTaxItems (
										numReturnHeaderID,numReturnItemID,numTaxItemID
									)  SELECT @numReturnHeaderID,OI.numReturnItemID,IT.numTaxItemID 
									FROM dbo.ReturnItems OI JOIN dbo.OpportunityItemsTaxItems IT ON OI.numOppItemID=IT.numOppItemID 
									WHERE OI.numReturnHeaderID=@numReturnHeaderID AND IT.numOppId=@numOppId
								END
								ELSE
								BEGIN
										--Delete Tax for ReturnItems if item deleted 
						--DELETE FROM OpportunityItemsTaxItems WHERE numReturnHeaderID=@numReturnHeaderID AND numReturnItemID NOT IN (SELECT numReturnItemID FROM ReturnItems WHERE numReturnHeaderID=@numReturnHeaderID)

						--Insert Tax for ReturnItems
						INSERT INTO dbo.OpportunityItemsTaxItems (
							numReturnHeaderID,
							numReturnItemID,
							numTaxItemID
						) SELECT @numReturnHeaderID,OI.numReturnItemID,TI.numTaxItemID FROM dbo.ReturnItems OI JOIN dbo.ItemTax IT ON OI.numItemCode=IT.numItemCode JOIN
						TaxItems TI ON TI.numTaxItemID=IT.numTaxItemID WHERE OI.numReturnHeaderID=@numReturnHeaderID AND IT.bitApplicable=1  
						--AND OI.numReturnItemID NOT IN (SELECT numReturnItemID FROM OpportunityItemsTaxItems WHERE numReturnHeaderID=@numReturnHeaderID)
						UNION
						  select @numReturnHeaderID,OI.numReturnItemID,0 FROM dbo.ReturnItems OI JOIN dbo.Item I ON OI.numItemCode=I.numItemCode
						   WHERE OI.numReturnHeaderID=@numReturnHeaderID  AND I.bitTaxable=1
						--AND OI.numReturnItemID NOT IN (SELECT numReturnItemID FROM OpportunityItemsTaxItems WHERE numReturnHeaderID=@numReturnHeaderID)
							END
						END
            END 
            
            -- Update leads,prospects to Accounts
		EXEC dbo.USP_UpdateCRMTypeToAccounts @numDivisionID ,@numDomainID ,@numUserCntID
                          
            END    
        ELSE IF @numReturnHeaderID <> 0 
                BEGIN    
                   IF ISNULL(@tintMode, 0) = 0
                   BEGIN
				   			UPDATE  [ReturnHeader]
							SET     [vcRMA] = @vcRMA,
									[numReturnReason] = @numReturnReason,
									[numReturnStatus] = @numReturnStatus,
									[monAmount] = ISNULL(monAmount,0) + ISNULL(monTotalDiscount,0) - ISNULL(@monTotalDiscount,0),
									[monTotalDiscount] = @monTotalDiscount,
									[tintReceiveType] = @tintReceiveType,
									[vcComments] = @vcComments,
									[numModifiedBy] = @numUserCntID,
									[dtModifiedDate] = GETUTCDATE()
							WHERE   [numDomainId] = @numDomainId
									AND [numReturnHeaderID] = @numReturnHeaderID
				   END                 
                   ELSE IF ISNULL(@tintMode, 0) = 1 
                   BEGIN
                   	UPDATE  ReturnHeader
								SET     numReturnStatus = @numReturnStatus
								WHERE   numReturnHeaderID = @numReturnHeaderID
							
				   	IF CONVERT(VARCHAR(10), @strItems) <> '' 
					BEGIN
						EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems                                                                                                             
						SELECT  *
						INTO    #temp1
						FROM    OPENXML (@hDocItem, '/NewDataSet/Item',2) WITH ( numReturnItemID NUMERIC, numUnitHourReceived FLOAT, numWareHouseItemID NUMERIC)
						EXEC sp_xml_removedocument @hDocItem 

								
								UPDATE  [ReturnItems]
								SET     [numUnitHourReceived] = X.numUnitHourReceived,
										[numWareHouseItemID] =CASE WHEN X.numWareHouseItemID=0 THEN NULL ELSE X.numWareHouseItemID END 
								FROM    #temp1 AS X
								WHERE   X.numReturnItemID = ReturnItems.numReturnItemID
										AND ReturnItems.numReturnHeaderID = @numReturnHeaderID
						DROP TABLE #temp1
				END 
			END
       END            
    END
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_ManageRMAInventory' ) 
    DROP PROCEDURE usp_ManageRMAInventory
GO
CREATE PROCEDURE [dbo].[usp_ManageRMAInventory]
    @numReturnHeaderID AS NUMERIC(9) = 0,
    @numDomainId NUMERIC(9),
    @numUserCntID AS NUMERIC(9)=0,
    @tintFlag AS TINYINT  --1: Receive 2: Revert
AS 
  BEGIN
  	
  	DECLARE @tintReturnType AS TINYINT
	DECLARE @tintReceiveType AS TINYINT
  	DECLARE @numReturnItemID AS NUMERIC
	DECLARE @itemcode AS NUMERIC
	DECLARE @numUnits as FLOAT
	DECLARE @numWareHouseItemID as numeric
	DECLARE @monAmount as money 
	DECLARE @onHand AS FLOAT
	DECLARE @onOrder AS FLOAT
	DECLARE @onBackOrder AS FLOAT
	DECLARE @onAllocation AS FLOAT
	DECLARE @description AS VARCHAR(100)
    
  	SELECT 
		@tintReturnType=tintReturnType,
		@tintReceiveType=tintReceiveType 
	FROM 
		dbo.ReturnHeader 
	WHERE 
		numReturnHeaderID=@numReturnHeaderID
  	
  	SELECT TOP 1 
		@numReturnItemID=numReturnItemID,
		@itemcode=RI.numItemCode,
		@numUnits=RI.numUnitHourReceived,
  		@numWareHouseItemID=ISNULL(numWareHouseItemID,0),
		@monAmount=monTotAmount
	FROM 
		ReturnItems RI 
	JOIN 
		Item I 
	ON 
		RI.numItemCode=I.numItemCode                                          
	WHERE 
		numReturnHeaderID=@numReturnHeaderID 
		AND (charitemtype='P' OR 1=(CASE 
									WHEN @tintReturnType=1 THEN 
										CASE 
											WHEN ISNULL(I.bitKitParent,0) = 1 AND ISNULL(I.bitAssembly,0) = 1 THEN 0
											WHEN ISNULL(I.bitKitParent,0) = 1 THEN 1
											ELSE 0 
										END 
									ELSE 
										0 
									END)) 
	ORDER BY 
		RI.numReturnItemID
				                                   
	WHILE @numReturnItemID>0                                        
	BEGIN   
		IF @numWareHouseItemID>0
		BEGIN                     
			SELECT  
				@onHand = ISNULL(numOnHand, 0),
				@onAllocation = ISNULL(numAllocation, 0),
				@onOrder = ISNULL(numOnOrder, 0),
				@onBackOrder = ISNULL(numBackOrder, 0)
			FROM 
				WareHouseItems
			WHERE 
				numWareHouseItemID = @numWareHouseItemID 
			
			--Receive : SalesReturn 
			IF (@tintFlag=1 AND @tintReturnType=1)
			BEGIN
				SET @description='Sales Return Receive (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ')'
										
                IF @onBackOrder >= @numUnits 
                BEGIN            
                        SET @onBackOrder = @onBackOrder - @numUnits            
                        SET @onAllocation = @onAllocation + @numUnits            
                END            
                ELSE 
                BEGIN            
                        SET @onAllocation = @onAllocation + @onBackOrder            
                        SET @numUnits = @numUnits - @onBackOrder            
                        SET @onBackOrder = 0            
                        SET @onHand = @onHand + @numUnits            
                END         
			END
			--Revert : SalesReturn
			ELSE IF (@tintFlag=2 AND @tintReturnType=1)
			BEGIN
				SET @description='Sales Return Delete (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ')'
					
				IF @onHand  - @numUnits >= 0
					BEGIN						
						SET @onHand = @onHand - @numUnits
					END
				ELSE IF (@onHand + @onAllocation)  - @numUnits >= 0
					BEGIN						
						SET @numUnits = @numUnits - @onHand
                        SET @onHand = 0 
                            
                        SET @onBackOrder = @onBackOrder + @numUnits
                        SET @onAllocation = @onAllocation - @numUnits  
					END	
				ELSE IF  (@onHand + @onBackOrder + @onAllocation) - @numUnits >= 0
					BEGIN
						SET @numUnits = @numUnits - @onHand
                        SET @onHand = 0 
                            
                        SET @numUnits = @numUnits - @onAllocation  
                        SET @onAllocation = 0 
                            
						SET @onBackOrder = @onBackOrder + @numUnits
					END
			END
			--Revert : PurchaseReturn
			ELSE IF (@tintFlag=2 AND @tintReturnType=2) 
			BEGIN
				SET @description='Purchase Return Delete (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ')'
					
				IF @onBackOrder >= @numUnits 
                BEGIN            
                        SET @onBackOrder = @onBackOrder - @numUnits            
                        SET @onAllocation = @onAllocation + @numUnits            
                END            
                ELSE 
                BEGIN            
                        SET @onAllocation = @onAllocation + @onBackOrder            
                        SET @numUnits = @numUnits - @onBackOrder            
                        SET @onBackOrder = 0            
                        SET @onHand = @onHand + @numUnits            
                END      
			END
			--Receive : PurchaseReturn 
			ELSE IF (@tintFlag=1 AND @tintReturnType=2) 
			BEGIN
				SET @description='Purchase Return Receive (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ')'
				
                IF @onHand  - @numUnits >= 0
				BEGIN						
					SET @onHand = @onHand - @numUnits
				END
				ELSE
				BEGIN
					RAISERROR ( 'PurchaseReturn_Qty', 16, 1 ) ;
					RETURN ;
				END	
			END 
				
			UPDATE  
				WareHouseItems
            SET 
				numOnHand = @onHand,
                numAllocation = @onAllocation,
                numBackOrder = @onBackOrder,
                numOnOrder = @onOrder,
				dtModified = GETDATE() 
            WHERE 
				numWareHouseItemID = @numWareHouseItemID 
                            
            IF @numWareHouseItemID>0
			BEGIN
				DECLARE @tintRefType AS TINYINT;SET @tintRefType=5
					  
				EXEC dbo.USP_ManageWareHouseItems_Tracking
				@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
				@numReferenceID = @numReturnHeaderID, --  numeric(9, 0)
				@tintRefType = @tintRefType, --  tinyint
				@vcDescription = @description, --  varchar(100)
				@numModifiedBy = @numUserCntID,
				@numDomainId = @numDomainId
            END           
		END
		  
		SELECT TOP 1 
			@numReturnItemID=numReturnItemID,
			@itemcode=RI.numItemCode,
			@numUnits=RI.numUnitHourReceived,
  			@numWareHouseItemID=ISNULL(numWareHouseItemID,0),
			@monAmount=monTotAmount
		FROM 
			ReturnItems RI 
		JOIN 
			Item I 
		ON 
			RI.numItemCode=I.numItemCode                                          
		WHERE 
			numReturnHeaderID=@numReturnHeaderID 
			AND (charitemtype='P' OR 1=(CASE 
										WHEN @tintReturnType=1 THEN 
											CASE 
												WHEN ISNULL(I.bitKitParent,0) = 1 AND ISNULL(I.bitAssembly,0) = 1 THEN 0
												WHEN ISNULL(I.bitKitParent,0) = 1 THEN 1
												ELSE 0 
											END 
										ELSE 0 END)) 
			AND RI.numReturnItemID>@numReturnItemID 
		ORDER BY 
			RI.numReturnItemID
							
		IF @@ROWCOUNT=0 SET @numReturnItemID=0         
	END
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageSalesTemplateItems')
DROP PROCEDURE USP_ManageSalesTemplateItems
GO
CREATE PROCEDURE [dbo].[USP_ManageSalesTemplateItems]
    @numSalesTemplateItemID NUMERIC = 0,
    @numSalesTemplateID NUMERIC,
    @strItems TEXT,
    @numDomainID NUMERIC
AS 
    SET NOCOUNT ON
    IF @numSalesTemplateItemID = 0 
        BEGIN
            DECLARE @hDocItem INT
            IF CONVERT(VARCHAR(10), @strItems) <> '' 
                BEGIN
                    EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems
                    INSERT  INTO [SalesTemplateItems]
                            (
                              [numSalesTemplateID],
                              [numItemCode],
                              [numUnitHour],
                              [monPrice],
                              [monTotAmount],
                              [numSourceID],
                              [numWarehouseID],
                              [Warehouse],
                              [numWarehouseItmsID],
                              [ItemType],
                              [Attributes],
                              [AttrValues],
                              [FreeShipping],
                              [Weight],
                              [Op_Flag],
                              [bitDiscountType],
                              [fltDiscount],
                              [monTotAmtBefDiscount],
                              [ItemURL],
                              [numDomainID],
                              numUOM,
                              vcUOMName,
                              UOMConversionFactor
                                    
                            )
                            SELECT  @numSalesTemplateID,
                                    X.numItemCode,
                                    X.numUnitHour,
                                    X.monPrice,
                                    X.monTotAmount,
                                    X.numSourceID,
                                    X.numWarehouseId,
                                    X.Warehouse,
                                    X.numWarehouseItmsID,
                                    X.vcItemType,
                                    X.Attributes,
                                    X.AttrValues,
                                    X.FreeShipping,
                                    X.Weight,
                                    X.Op_Flag,
                                    X.bitDiscountType,
                                    X.fltDiscount,
                                    X.monTotAmtBefDiscount,
                                    X.ItemURL,
                                    @numDomainID,
                                    X.numUOM,
									X.vcUOMName,
									X.UOMConversionFactor
                            FROM    ( SELECT    *
                                      FROM      OPENXML (@hDocItem, '/NewDataSet/Table', 2)
                                                WITH ( numItemCode NUMERIC(18, 0), numUnitHour FLOAT, monPrice MONEY, monTotAmount MONEY, numSourceID NUMERIC(18, 0), numWarehouseId NUMERIC(18, 0), Warehouse VARCHAR(50), numWarehouseItmsID NUMERIC(18, 0), vcItemType VARCHAR(50), Attributes VARCHAR(1000), AttrValues VARCHAR(1000), FreeShipping BIT, Weight float, Op_Flag TINYINT, bitDiscountType BIT, fltDiscount FLOAT, monTotAmtBefDiscount MONEY, ItemURL VARCHAR(200),numUOM numeric,vcUOMName VARCHAR(50),UOMConversionFactor DECIMAL(30,16) )
                                    ) X
                            WHERE   X.numItemCode NOT IN (
                                    SELECT  numItemCode
                                    FROM    [SalesTemplateItems]
                                    WHERE   [numSalesTemplateID] = @numSalesTemplateID )
                    EXEC sp_xml_removedocument @hDocItem
                END


            SELECT  SCOPE_IDENTITY() AS InsertedID
        END			
    
    

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageSOWorkOrder')
DROP PROCEDURE USP_ManageSOWorkOrder
GO
CREATE PROCEDURE [dbo].[USP_ManageSOWorkOrder]
@numDomainID AS numeric(9)=0,
@numOppID AS numeric(9)=0,
@strItems AS TEXT,
@numUserCntID AS numeric(9)=0
AS
BEGIN
 DECLARE @hDocItem int                                                                                                                            

IF (SELECT COUNT(*) FROM WorkOrder WHERE numOppId=@numOppId)=0
BEGIN
 EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems  
  
  DECLARE @numItemCode AS numeric(9),@numUnitHour AS FLOAT,@numWarehouseItmsID AS numeric(9),@vcItemDesc AS VARCHAR(1000)
  
  DECLARE @numWOId AS NUMERIC(9),@vcInstruction AS VARCHAR(1000),@bintCompliationDate AS DATETIME,@numWOAssignedTo NUMERIC(9)
  
  DECLARE WOCursor CURSOR FOR 
  SELECT X.numItemCode,dbo.fn_UOMConversion(X.numUOM,numItemCode,@numDomainId,null) * x.numUnitHour AS numUnitHour,X.numWarehouseItmsID,X.vcItemDesc,X.vcInstruction,X.bintCompliationDate,X.numWOAssignedTo
	from(SELECT *FROM OPENXML (@hDocItem,'/NewDataSet/Item',2)                                                                          
		   WITH  (numItemCode numeric(9),numUnitHour FLOAT,numWarehouseItmsID numeric(9),numUOM NUMERIC(9),bitWorkOrder BIT,vcItemDesc VARCHAR(1000) ,
		   vcInstruction varchar(1000),bintCompliationDate DATETIME,numWOAssignedTo NUMERIC(9)
		  ))X  WHERE X.bitWorkOrder=1

OPEN WOCursor;

FETCH NEXT FROM WOCursor INTO @numItemCode, @numUnitHour,@numWarehouseItmsID,@vcItemDesc,@vcInstruction,@bintCompliationDate,@numWOAssignedTo

WHILE @@FETCH_STATUS = 0
BEGIN
	insert into WorkOrder(numItemCode,numQtyItemsReq,numWareHouseItemId,numCreatedBy,bintCreatedDate,numDomainID,numWOStatus,numOppId,vcItemDesc,vcInstruction,bintCompliationDate,numAssignedTo)
	values(@numItemCode,@numUnitHour,@numWarehouseItmsID,@numUserCntID,getutcdate(),@numDomainID,0,@numOppID,@vcItemDesc,@vcInstruction,@bintCompliationDate,@numWOAssignedTo)
	
	set @numWOId=@@IDENTITY
	

IF @numWOAssignedTo>0
BEGIN
	DECLARE @numDivisionId AS NUMERIC(9) ,@vcItemName AS VARCHAR(300)
	
	SELECT @vcItemName=ISNULL(vcItemName,'') FROM Item WHERE numItemCode=@numItemCode

      SET @vcItemName= 'Work Order for Item : ' + @vcItemName

      SELECT @numDivisionId=numDivisionId FROM AdditionalContactsInformation WHERE numContactId=@numWOAssignedTo

EXEC dbo.usp_InsertCommunication
	@numCommId = 0, --  numeric(18, 0)
	@bitTask = 972, --  numeric(18, 0)
	@numContactId = @numUserCntID, --  numeric(18, 0)
	@numDivisionId = @numDivisionId, --  numeric(18, 0)
	@txtDetails = @vcItemName, --  text
	@numOppId = 0, --  numeric(18, 0)
	@numAssigned = @numWOAssignedTo, --  numeric(18, 0)
	@numUserCntID = @numUserCntID, --  numeric(18, 0)
	@numDomainId = @numDomainID, --  numeric(18, 0)
	@bitClosed = 0, --  tinyint
	@vcCalendarName = '', --  varchar(100)
	@dtStartTime = @bintCompliationDate, --  datetime
	@dtEndtime = @bintCompliationDate, --  datetime
	@numActivity = 0, --  numeric(9, 0)
	@numStatus = 0, --  numeric(9, 0)
	@intSnoozeMins = 0, --  int
	@tintSnoozeStatus = 0, --  tinyint
	@intRemainderMins = 0, --  int
	@tintRemStaus = 0, --  tinyint
	@ClientTimeZoneOffset = 0, --  int
	@bitOutLook = 0, --  tinyint
	@bitSendEmailTemplate = 0, --  bit
	@bitAlert = 0, --  bit
	@numEmailTemplate = 0, --  numeric(9, 0)
	@tintHours = 0, --  tinyint
	@CaseID = 0, --  numeric(18, 0)
	@CaseTimeId = 0, --  numeric(18, 0)
	@CaseExpId = 0, --  numeric(18, 0)
	@ActivityId = 0, --  numeric(18, 0)
	@bitFollowUpAnyTime = 0, --  bit
	@strAttendee=''
END

INSERT INTO [WorkOrderDetails] 
(
	numWOId,numItemKitID,numChildItemID,numQtyItemsReq,numWareHouseItemId,vcItemDesc,sintOrder,numQtyItemsReq_Orig,numUOMId
)
SELECT 
	@numWOId,numItemKitID,numItemCode,
	CAST(((DTL.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(Dtl.numUOMID,item.numItemCode,item.numDomainID,item.numBaseUnit),1)) * @numUnitHour)AS FLOAT),
	isnull(Dtl.numWareHouseItemId,0),
	ISNULL(Dtl.vcItemDesc,txtItemDesc),
	ISNULL(sintOrder,0),
	DTL.numQtyItemsReq,
	Dtl.numUOMId 
FROM 
	item                                
INNER JOIN 
	ItemDetails Dtl 
ON 
	numChildItemID=numItemCode
WHERE 
	numItemKitID=@numItemCode
	
FETCH NEXT FROM WOCursor INTO @numItemCode, @numUnitHour,@numWarehouseItmsID,@vcItemDesc,@vcInstruction,@bintCompliationDate,@numWOAssignedTo
END

CLOSE WOCursor;
DEALLOCATE WOCursor;

EXEC sp_xml_removedocument @hDocItem    
 
END 
END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageWorkOrder')
DROP PROCEDURE USP_ManageWorkOrder
GO
CREATE PROCEDURE [dbo].[USP_ManageWorkOrder]
@numItemCode AS NUMERIC(18,0),
@numWareHouseItemID AS NUMERIC(18,0),
@Units AS INT,
@numDomainID AS NUMERIC(18,0),
@numUserCntID AS NUMERIC(18,0),
@vcInstruction AS VARCHAR(1000),
@bintCompliationDate AS DATETIME,
@numAssignedTo AS NUMERIC(9)
AS
BEGIN
BEGIN TRY
BEGIN TRAN
	DECLARE @numWOId AS NUMERIC(9),@numDivisionId AS NUMERIC(9) 
	DECLARE @txtItemDesc AS varchar(1000),@vcItemName AS VARCHAR(300)
	
	SELECT @txtItemDesc=ISNULL(txtItemDesc,''),@vcItemName=ISNULL(vcItemName,'') FROM Item WHERE numItemCode=@numItemCode

	INSERT INTO WorkOrder
	(
		numItemCode,numQtyItemsReq,numWareHouseItemId,numCreatedBy,
		bintCreatedDate,numDomainID,numWOStatus,vcItemDesc,vcInstruction,
		bintCompliationDate,numAssignedTo
	)
	VALUES
	(
		@numItemCode,@Units,@numWareHouseItemID,@numUserCntID,
		getutcdate(),@numDomainID,0,@txtItemDesc,@vcInstruction,
		@bintCompliationDate,@numAssignedTo
	)
	
	SET @numWOId=@@IDENTITY

	IF @numAssignedTo>0
	BEGIN

		SET @vcItemName= 'Work Order for Item : ' + @vcItemName
		SELECT @numDivisionId=numDivisionId FROM AdditionalContactsInformation WHERE numContactId=@numAssignedTo

		EXEC dbo.usp_InsertCommunication
		@numCommId = 0, --  numeric(18, 0)
		@bitTask = 972, --  numeric(18, 0)
		@numContactId = @numUserCntID, --  numeric(18, 0)
		@numDivisionId = @numDivisionId, --  numeric(18, 0)
		@txtDetails = @vcItemName, --  text
		@numOppId = 0, --  numeric(18, 0)
		@numAssigned = @numAssignedTo, --  numeric(18, 0)
		@numUserCntID = @numUserCntID, --  numeric(18, 0)
		@numDomainId = @numDomainID, --  numeric(18, 0)
		@bitClosed = 0, --  tinyint
		@vcCalendarName = '', --  varchar(100)
		@dtStartTime = @bintCompliationDate, --  datetime
		@dtEndtime = @bintCompliationDate, --  datetime
		@numActivity = 0, --  numeric(9, 0)
		@numStatus = 0, --  numeric(9, 0)
		@intSnoozeMins = 0, --  int
		@tintSnoozeStatus = 0, --  tinyint
		@intRemainderMins = 0, --  int
		@tintRemStaus = 0, --  tinyint
		@ClientTimeZoneOffset = 0, --  int
		@bitOutLook = 0, --  tinyint
		@bitSendEmailTemplate = 0, --  bit
		@bitAlert = 0, --  bit
		@numEmailTemplate = 0, --  numeric(9, 0)
		@tintHours = 0, --  tinyint
		@CaseID = 0, --  numeric(18, 0)
		@CaseTimeId = 0, --  numeric(18, 0)
		@CaseExpId = 0, --  numeric(18, 0)
		@ActivityId = 0, --  numeric(18, 0)
		@bitFollowUpAnyTime = 0, --  bit
		@strAttendee=''
END
	
	INSERT INTO [WorkOrderDetails] 
	(
		numWOId,numItemKitID,numChildItemID,numQtyItemsReq,numWareHouseItemId,vcItemDesc,sintOrder,numQtyItemsReq_Orig,numUOMId
	)
	SELECT 
		@numWOId,numItemKitID,numItemCode,
		CAST(((DTL.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(Dtl.numUOMID,item.numItemCode,item.numDomainID,item.numBaseUnit),1)) * @Units) AS FLOAT),
		isnull(Dtl.numWareHouseItemId,0),
		ISNULL(Dtl.vcItemDesc,txtItemDesc),
		ISNULL(sintOrder,0),
		DTL.numQtyItemsReq,
		Dtl.numUOMId 
	FROM 
		item                                
	INNER JOIN 
		ItemDetails Dtl 
	ON 
		numChildItemID=numItemCode
	WHERE 
		numItemKitID=@numItemCode

	--UPDATE ON ORDER OF ASSEMBLY
	UPDATE 
		WareHouseItems
	SET    
		numOnOrder= ISNULL(numOnOrder,0) + @Units,
		dtModified = GETDATE() 
	WHERE   
		numWareHouseItemID = @numWareHouseItemID 
	
	--MAKE ENTRY IN WAREHOUSE TRACKING TABLE
	DECLARE @Description AS VARCHAR(1000)
	SET @Description='Work Order Created (Qty:' + CAST(@Units AS VARCHAR(10)) + ')'

	EXEC dbo.USP_ManageWareHouseItems_Tracking
	@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
	@numReferenceID = @numWOId, --  numeric(9, 0)
	@tintRefType = 2, --  tinyint
	@vcDescription = @Description, --  varchar(100)
	@numModifiedBy = @numUserCntID,
	@numDomainID = @numDomainID

	EXEC USP_ManageInventoryWorkOrder @numWOId,@numDomainID,@numUserCntID
COMMIT TRAN 
END TRY 
BEGIN CATCH		
    IF ( @@TRANCOUNT > 0 ) 
    BEGIN
        ROLLBACK TRAN
    END

	DECLARE @ErrorMessage NVARCHAR(4000);
    DECLARE @ErrorSeverity INT;
    DECLARE @ErrorState INT;

    SELECT @ErrorMessage = ERROR_MESSAGE(), @ErrorSeverity = ERROR_SEVERITY(), @ErrorState = ERROR_STATE();

    RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);
END CATCH
END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO

-- exec USP_ManageWorkOrderStatus @numWOId=10086,@numWOStatus=23184,@numUserCntID=1,@numDomainID=1
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageWorkOrderStatus')
DROP PROCEDURE USP_ManageWorkOrderStatus
GO
CREATE PROCEDURE [dbo].[USP_ManageWorkOrderStatus]
@numWOId as numeric(9)=0,
@numWOStatus as numeric(9),
@numUserCntID AS NUMERIC(9)=0,
@numDomainID AS NUMERIC(18,0)
AS
BEGIN
BEGIN TRY
BEGIN TRANSACTION
	DECLARE @numDomain AS NUMERIC(18,0)
	DECLARE @CurrentWorkOrderStatus AS NUMERIC(18,0)
	SELECT @numDomain = @numDomainID

	--GET CURRENT STATUS OF WORK ORDER
	SELECT @CurrentWorkOrderStatus=numWOStatus FROM WorkOrder WHERE numWOId=@numWOId AND [numDomainID] = @numDomainID

	--ASSIGN NEW STATUS TO WORK ORDER
	UPDATE WorkOrder SET numWOStatus=@numWOStatus WHERE numWOId=@numWOId AND [numDomainID] = @numDomainID

	--IF CURRENT WORK ORDER STATUS IS NOT COMPLETED AND NEW WORK ORDER STATUS IS COMPLETED THEN MAKE INVENTORY CHANGES
	IF ISNULL(@CurrentWorkOrderStatus,0) <> 23184 AND @numWOStatus=23184
	BEGIN
		DECLARE @PendingChildWO NUMERIC(18,0)

		;WITH CTE (numWOID) AS
		(
			SELECT
				numWOID 
			FROM 
				WorkOrder
			WHERE 
				numParentWOID = @numWOId 
				AND numWOStatus <> 23184
			UNION ALL
			SELECT 
				WorkOrder.numWOID 
			FROM 
				WorkOrder
			INNER JOIN
				CTE c
			ON
				 WorkOrder.numParentWOID = c.numWOID
			WHERE 
				WorkOrder.numWOStatus <> 23184
		)

		SELECT @PendingChildWO = COUNT(*) FROM CTE

		--WE HAVE IMPLEMENTED CODE IN SUCH WAY THAT FIRST CHILD WORK ORDER GET COMPLETED
		--SO IF SOME REASON CHILD WORK ORDER IS NOT COMPLETED THAN RAISERROR
		IF ISNULL(@PendingChildWO,0) > 0
		BEGIN
			RAISERROR('CHILD WORK ORDER STILL NOT COMPLETED',16,1);
		END

		DECLARE @numWareHouseItemID AS NUMERIC(9),@numQtyItemsReq AS FLOAT,@numOppId AS NUMERIC(9)

		UPDATE WorkOrder SET bintCompletedDate=GETDATE() WHERE numWOId=@numWOId AND [numDomainID] = @numDomainID

		SELECT @numWareHouseItemID=numWareHouseItemID,@numQtyItemsReq=numQtyItemsReq,@numOppId=ISNULL(numOppId,0) FROM WorkOrder WHERE numWOId=@numWOId AND [numDomainID] = @numDomainID
	
		EXEC USP_UpdatingInventory 0,@numWareHouseItemID,@numQtyItemsReq,@numWOId,@numUserCntID,@numOppId
	
		--COMMENTED BECAUSE FOLLOWING INVENTORY CHANGES ARE DONE WHEN ORDER IS CREATED 
		--RATHER THEN WAITING FOR INVENTORY TO BE COMPLETED THROUGH STORE PROCEDURE 
		--CALLED USP_ManageInventoryAssemblyWO
		--IF @numOppId>0
		--  BEGIN
	
		--	declare @onHand as NUMERIC,@onOrder as NUMERIC,@onBackOrder as NUMERIC,@onAllocation as numeric                                    

		--	SELECT @onHand=isnull(numOnHand,0),@onAllocation=isnull(numAllocation,0),                                    
		--			@onOrder=isnull(numOnOrder,0),@onBackOrder=isnull(numBackOrder,0)                                     
		--			from WareHouseItems where numWareHouseItemID=@numWareHouseItemID  
		--			AND [numDomainID] = @numDomainID

		--	if @onHand>=@numQtyItemsReq                                    
		--		begin                                    
		--			set @onHand=@onHand-@numQtyItemsReq                            
		--			set @onAllocation=@onAllocation+@numQtyItemsReq                                    
		--		end                                    
		--	else if @onHand<@numQtyItemsReq                                    
		--		begin                                    
		--			set @onAllocation=@onAllocation+@onHand                                    
		--			set @onBackOrder=@onBackOrder+@numQtyItemsReq-@onHand                                    
		--			set @onHand=0                                    
		--		end   
			                                  
		--		 update WareHouseItems set numOnHand=@onHand,                                    
		--		 numAllocation=@onAllocation,                                    
		--		 numBackOrder=@onBackOrder,dtModified = GETDATE()                                     
		--		 where numWareHouseItemID=@numWareHouseItemID 
		--		 AND [numDomainID] = @numDomainID 
			 
		--		EXEC dbo.USP_ManageWareHouseItems_Tracking
		--			@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
		--			@numReferenceID = @numWOId, --  numeric(9, 0)
		--			@tintRefType = 2, --  tinyint
		--			@vcDescription = 'WorkOrder', --  varchar(100)
		--			@numModifiedBy = @numUserCntID
		--			,@ClientTimeZoneOffset = 0
		--			,@dtRecordDate = NULL
		--			,@numDomainID = @numDomain 
		--	END	
		 
		DECLARE WO_Items CURSOR FOR 
		SELECT numWareHouseItemId,numQtyItemsReq FROM WorkOrderDetails WD JOIN item i ON WD.numChildItemID=i.numItemCode WHERE numWOId=@numWOId AND i.charItemType IN ('P')
							
		OPEN WO_Items

		FETCH NEXT FROM WO_Items into @numWareHouseItemId,@numQtyItemsReq
		WHILE @@FETCH_STATUS = 0
			BEGIN  
				EXEC USP_UpdatingInventory 1,@numWareHouseItemID,@numQtyItemsReq,@numWOId,@numUserCntID,@numOppId

				FETCH NEXT FROM WO_Items into @numWareHouseItemId,@numQtyItemsReq
			END

		CLOSE WO_Items
		DEALLOCATE WO_Items
	END
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SET @numOppID = 0

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_OPPBizDocItems_GetWithKitChilds' ) 
    DROP PROCEDURE USP_OPPBizDocItems_GetWithKitChilds
GO
CREATE PROCEDURE [dbo].[USP_OPPBizDocItems_GetWithKitChilds]
(
    @numOppId AS NUMERIC(9) = NULL,
    @numOppBizDocsId AS NUMERIC(9) = NULL,
    @numDomainID AS NUMERIC(9) = 0      
)
AS 
BEGIN
    DECLARE @DivisionID AS NUMERIC(9)      
    DECLARE @tintTaxOperator AS TINYINT   
                                                                                                                                                                                                                                                                                                                                          
    SELECT  
		@DivisionID = numDivisionID, 
		@tintTaxOperator = [tintTaxOperator]           
    FROM    
		OpportunityMaster
    WHERE   
		numOppId = @numOppId                                                                                                                                                                          

    
                                                                                          
    DECLARE @strSQL AS VARCHAR(MAX)                                                                                          
    DECLARE @strSQL1 AS VARCHAR(8000)                                       
    DECLARE @strSQLCusFields VARCHAR(1000) = ''
    DECLARE @strSQLEmpFlds VARCHAR(500) = ''                                                                         
                                                                                              
    DECLARE @tintType AS TINYINT   
    DECLARE @tintOppType AS TINYINT                                                      
    DECLARE @intRowNum AS INT                                                  
    DECLARE @numFldID AS VARCHAR(15)                                          
    DECLARE @vcFldname AS VARCHAR(50)                                            
    DECLARE @numBizDocId AS NUMERIC(9)
    DECLARE @numBizDocTempID AS NUMERIC(9) = 0
	DECLARE @bitDisplayKitChild AS BIT = 0
        
                                           
    SELECT  
		@numBizDocTempID = ISNULL(numBizDocTempID, 0),
        @numBizDocId = numBizDocId
    FROM    
		OpportunityBizDocs
    WHERE   
		numOppBizDocsId = @numOppBizDocsId

	
	SELECT
		@bitDisplayKitChild = ISNULL(bitDisplayKitChild,0)
	FROM	
		BizDocTemplate
	WHERE
		numDomainID = @numDomainID
		AND numBizDocTempID = @numBizDocTempID


    SELECT  
		@tintType = tintOppType,
        @tintOppType = tintOppType
    FROM    
		OpportunityMaster
    WHERE   
		numOppID = @numOppId                                                                                          
                                                                                   
    IF @tintType = 1 
        SET @tintType = 7
    ELSE 
        SET @tintType = 8                                                                                      
                                                                                
    SELECT  
		*
    INTO    
		#Temp1
    FROM    
	( 
		SELECT   
			Opp.vcitemname AS vcItemName,
            ISNULL(OBD.numOppBizDocItemID, 0) OppBizDocItemID,
            CASE WHEN charitemType = 'P' THEN 'Product'
                    WHEN charitemType = 'S' THEN 'Service'
            END AS charitemType,
            CASE WHEN charitemType = 'P' THEN 'Inventory'
	                WHEN charitemType = 'N' THEN 'Non-Inventory'
                    WHEN charitemType = 'S' THEN 'Service'
            END AS vcItemType,
			CONVERT(VARCHAR(2500), OBD.vcitemDesc) AS txtItemDesc, 
			opp.numUnitHour AS numOrgTotalUnitHour,
			dbo.fn_UOMConversion(ISNULL(i.numBaseUnit, 0),i.numItemCode, @numDomainID,ISNULL(opp.numUOMId, 0)) * opp.numUnitHour AS numTotalUnitHour,                                   
            dbo.fn_UOMConversion(ISNULL(i.numBaseUnit, 0),i.numItemCode, @numDomainID,ISNULL(opp.numUOMId, 0)) * OBD.numUnitHour AS numUnitHour,
            CONVERT(DECIMAL(18, 4), (dbo.fn_UOMConversion(ISNULL(opp.numUOMId, 0), i.numItemCode, @numDomainID, ISNULL(i.numBaseUnit, 0)) * OBD.monPrice)) monPrice,
            CONVERT(VARCHAR, CONVERT(DECIMAL(18, 4), OBD.monTotAmount)) Amount,
            OBD.monTotAmount/*Fo calculating sum*/,
            ISNULL(CONVERT(VARCHAR, i.monListPrice), 0) AS monListPrice,
            i.numItemCode AS ItemCode,
            opp.numoppitemtCode,
            L.vcdata AS numItemClassification,
            CASE WHEN bitTaxable = 0 THEN 'No'
                    ELSE 'Yes'
            END AS Taxable,
            numIncomeChartAcntId AS itemIncomeAccount,
            'NI' AS ItemType,
            ISNULL(GJH.numJournal_Id, 0) AS numJournalId,
            ( SELECT TOP 1
                        ISNULL(GJD.numTransactionId, 0)
                FROM      General_Journal_Details GJD
                WHERE     GJH.numJournal_Id = GJD.numJournalId
                        AND GJD.chBizDocItems = 'NI'
                        AND GJD.numoppitemtCode = opp.numoppitemtCode
            ) AS numTransactionId,
            ISNULL(Opp.vcModelID, '') vcModelID,
            dbo.USP_GetAttributes(OBD.numWarehouseItmsID, bitSerialized) AS vcAttributes,
            ISNULL(vcPartNo, '') AS vcPartNo,
            (ISNULL(OBD.monTotAmtBefDiscount, OBD.monTotAmount) - OBD.monTotAmount ) AS DiscAmt,
			(CASE WHEN OBD.numUnitHour = 0 THEN 0 ELSE CONVERT(DECIMAL(18, 4),(dbo.fn_UOMConversion(ISNULL(opp.numUOMId, 0), i.numItemCode, @numDomainID, ISNULL(i.numBaseUnit, 0)) * (ISNULL(OBD.monTotAmount,0) / OBD.numUnitHour))) END) As monUnitSalePrice,
            OBD.vcNotes,
            OBD.vcTrackingNo,
            OBD.vcShippingMethod,
            OBD.monShipCost,
            [dbo].[FormatedDateFromDate](OBD.dtDeliveryDate,@numDomainID) dtDeliveryDate,
            Opp.numWarehouseItmsID,
            ISNULL(u.vcUnitName, '') numUOMId,
            ISNULL(Opp.vcManufacturer, '') AS vcManufacturer,
            ISNULL(V.monCost, 0) AS VendorCost,
            Opp.vcPathForTImage,
            i.[fltLength],
            i.[fltWidth],
            i.[fltHeight],
            i.[fltWeight],
            ISNULL(I.numVendorID, 0) numVendorID,
            Opp.bitDropShip AS DropShip,
            SUBSTRING(( SELECT  ' ,' + vcSerialNo
                                + CASE WHEN ISNULL(I.bitLotNo, 0) = 1
                                        THEN ' ('
                                            + CONVERT(VARCHAR(15), oppI.numQty)
                                            + ')'
                                        ELSE ''
                                    END
                        FROM    OppWarehouseSerializedItem oppI
                                JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID = whi.numWareHouseItmsDTLID
                        WHERE   oppI.numOppID = Opp.numOppId
                                AND oppI.numOppItemID = Opp.numoppitemtCode
                        ORDER BY vcSerialNo
                        FOR
                        XML PATH('')
                        ), 3, 200000) AS SerialLotNo,
            ISNULL(opp.numUOMId, 0) AS numUOM,
            ISNULL(u.vcUnitName, '') vcUOMName,
            dbo.fn_UOMConversion(opp.numUOMId, Opp.numItemCode,
                                    @numDomainID, NULL) AS UOMConversionFactor,
            ISNULL(Opp.numSOVendorId, 0) AS numSOVendorId,
            [dbo].FormatedDateTimeFromDate(OBD.dtRentalStartDate,
                                            @numDomainID) dtRentalStartDate,
            [dbo].FormatedDateTimeFromDate(OBD.dtRentalReturnDate,
                                            @numDomainID) dtRentalReturnDate,
            ISNULL(W.vcWareHouse, '') AS Warehouse,
            CASE WHEN charitemType = 'P' THEN WI.vcWHSKU
                    ELSE i.vcSKU
            END vcSKU,
            CASE WHEN charitemType = 'P' THEN WI.vcBarCode
                    ELSE i.numBarCodeId
            END vcBarcode,
            ISNULL(opp.numClassID, 0) AS numClassID,
            ISNULL(opp.numProjectID, 0) AS numProjectID,
            ISNULL(I.[numShipClass],0) AS numShipClass,OBD.numOppBizDocID,WL.vcLocation,ISNULL(bitSerialized,0) AS bitSerialized,ISNULL(bitLotNo,0) AS bitLotNo,
            opp.numOppId,OBD.numUnitHour AS numUnitHourOrig,ISNULL(I.numItemCode,0) AS [numItemCode],
			ISNULL(I.bitKitParent,0) AS bitKitParent,
			0 AS numOppChildItemID, 
			0 AS numOppKitChildItemID,
			0 AS bitChildItem,
			0 AS tintLevel
        FROM      
			OpportunityItems opp
        JOIN 
			OpportunityBizDocItems OBD 
		ON 
			OBD.numOppItemID = opp.numoppitemtCode
        LEFT JOIN 
			item i 
		ON 
			opp.numItemCode = i.numItemCode
        LEFT JOIN 
			ListDetails L 
		ON 
			i.numItemClassification = L.numListItemID
        LEFT JOIN 
			Vendor V 
		ON 
			V.numVendorID = @DivisionID
            AND V.numItemCode = opp.numItemCode
        LEFT JOIN 
			General_Journal_Header GJH 
		ON 
			opp.numoppid = GJH.numOppId
            AND GJH.numBizDocsPaymentDetId IS NULL
            AND GJH.numOppBizDocsId = @numOppBizDocsId
        LEFT JOIN 
			UOM u 
		ON 
			u.numUOMId = opp.numUOMId                                      
        LEFT JOIN 
			dbo.WareHouseItems WI 
		ON 
			opp.numWarehouseItmsID = WI.numWareHouseItemID
			AND WI.numDomainID = @numDomainID
        LEFT JOIN 
			dbo.Warehouses W 
		ON 
			WI.numDomainID = W.numDomainID
            AND WI.numWareHouseID = W.numWareHouseID
        LEFT JOIN 
			dbo.WarehouseLocation WL 
		ON 
			WL.numWLocationID = WI.numWLocationID
        WHERE     
			opp.numOppId = @numOppId
            AND (bitShowDeptItem IS NULL OR bitShowDeptItem = 0)
            AND OBD.numOppBizDocID = @numOppBizDocsId
       

    ) X

	--GET KIT CHILD ITEMS (NOW WE ARE SUPPORTING KIT WITHIN KIT BUT ONLY 1 LEVEL MEANS CHILD KIT INSIDE PARENT KIT CAN NOT HAVE ANOTHER KIT AS CHILD)
	IF (@bitDisplayKitChild=1 AND (SELECT COUNT(*) FROM OpportunityKitItems WHERE numOppId = @numOppId ) > 0)
	BEGIN
		DECLARE @TEMPTABLE TABLE(
			numOppItemCode NUMERIC(18,0),
			numOppChildItemID NUMERIC(18,0), 
			numOppKitChildItemID NUMERIC(18,0),
			numItemCode NUMERIC(18,0),
			numWarehouseItemID NUMERIC(18,0),
			numTotalQty NUMERIC(18,2),
			numQty NUMERIC(18,2),
			numUOMID NUMERIC(18,0),
			tintLevel TINYINT
		);

		--FIRST LEVEL CHILD
		INSERT INTO @TEMPTABLE
		(
			numOppItemCode,numOppChildItemID,numOppKitChildItemID,numItemCode,numWarehouseItemID,numTotalQty,numQty,numUOMID,tintLevel
		)
		SELECT 
			OKI.numOppItemID,
			OKI.numOppChildItemID,
			CAST(0 AS NUMERIC(18,0)),
			OKI.numChildItemID,
			ISNULL(OKI.numWareHouseItemId,0),
			CAST((t1.numOrgTotalUnitHour * OKI.numQtyItemsReq_Orig) AS NUMERIC(18,2)),
			CAST((t1.numUnitHourOrig * OKI.numQtyItemsReq_Orig) AS NUMERIC(18,2)),
			ISNULL(OKI.numUOMID,0),
			1
		FROM 
			#Temp1 t1
		INNER JOIN
			OpportunityKitItems OKI
		ON
			t1.numoppitemtCode = OKI.numOppItemID
		WHERE
			t1.bitKitParent = 1

		--SECOND LEVEL CHILD
		INSERT INTO @TEMPTABLE
		(
			numOppItemCode,numOppChildItemID,numOppKitChildItemID,numItemCode,numWarehouseItemID,numTotalQty,numQty,numUOMID,tintLevel
		)
		SELECT 
			OKCI.numOppItemID,
			OKCI.numOppChildItemID,
			OKCI.numOppKitChildItemID,
			OKCI.numItemID,
			ISNULL(OKCI.numWareHouseItemId,0),
			CAST((c.numTotalQty * OKCI.numQtyItemsReq_Orig) AS NUMERIC(18,2)),
			CAST((c.numQty * OKCI.numQtyItemsReq_Orig) AS NUMERIC(18,2)),
			ISNULL(OKCI.numUOMID,0),
			2
		FROM 
			OpportunityKitChildItems OKCI
		INNER JOIN
			@TEMPTABLE c
		ON
			OKCI.numOppID = @numOppId
			AND OKCI.numOppItemID = c.numOppItemCode
			AND OKCI.numOppChildItemID = c.numOppChildItemID

		INSERT INTO 
			#Temp1
		SELECT   
			(CASE WHEN t2.tintLevel = 1 THEN CONCAT('&nbsp;&nbsp;',I.vcItemName) WHEN t2.tintLevel = 2 THEN CONCAT('&nbsp;&nbsp;&nbsp;&nbsp;',I.vcItemName) ELSE I.vcItemName END) AS vcItemName,
            0 OppBizDocItemID,
            (CASE WHEN I.charitemType = 'P' THEN 'Product' WHEN charitemType = 'S' THEN 'Service' END) AS charitemType,
            (CASE WHEN charitemType = 'P' THEN 'Inventory'
	                WHEN charitemType = 'N' THEN 'Non-Inventory'
                    WHEN charitemType = 'S' THEN 'Service'
            END) AS vcItemType,
			CONVERT(VARCHAR(2500), I.txtItemDesc) AS txtItemDesc,   
			t2.numTotalQty AS numOrigTotalUnitHour,
			t2.numTotalQty AS numTotalUnitHour,
            t2.numQty AS numUnitHour,
            (ISNULL(dbo.fn_UOMConversion(ISNULL(t2.numUOMID, 0), i.numItemCode, @numDomainID, ISNULL(i.numBaseUnit, 0)),1) * (CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END)) monPrice,
            CONVERT(VARCHAR, CONVERT(DECIMAL(18, 4),((t2.numQty * ISNULL(dbo.fn_UOMConversion(ISNULL(t2.numUOMID, 0), i.numItemCode, @numDomainID, ISNULL(i.numBaseUnit, 0)),1)) * (CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END)))) Amount,
            ((t2.numQty * ISNULL(dbo.fn_UOMConversion(ISNULL(t2.numUOMID, 0), i.numItemCode, @numDomainID, ISNULL(i.numBaseUnit, 0)),1)) * (CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END)) AS monTotAmount/*Fo calculating sum*/,
            ISNULL(CONVERT(VARCHAR, (CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END)), 0) AS monListPrice,
            I.numItemCode AS ItemCode,
            t2.numOppItemCode,
            L.vcdata AS numItemClassification,
            CASE WHEN bitTaxable = 0 THEN 'No' ELSE 'Yes' END AS Taxable,
            numIncomeChartAcntId AS itemIncomeAccount,
            'NI' AS ItemType,
            0 AS numJournalId,
            0 AS numTransactionId,
            ISNULL(I.vcModelID, '') vcModelID,
            dbo.USP_GetAttributes(t2.numWarehouseItemID, bitSerialized) AS vcAttributes,
            ISNULL(vcPartNo, '') AS vcPartNo,
            0 AS DiscAmt,
			(ISNULL(dbo.fn_UOMConversion(ISNULL(t2.numUOMID, 0), i.numItemCode, @numDomainID, ISNULL(i.numBaseUnit, 0)),1) * (CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END)) As monUnitSalePrice,
            '' AS vcNotes,'' AS vcTrackingNo,'' AS vcShippingMethod,0 AS monShipCost,NULL AS dtDeliveryDate,t2.numWarehouseItemID,
            ISNULL(u.vcUnitName, '') numUOMId,ISNULL(I.vcManufacturer, '') AS vcManufacturer,ISNULL(V.monCost, 0) AS VendorCost,
            ISNULL((SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = I.numItemCode AND bitDefault=1),'') AS vcPathForTImage,
            I.[fltLength],I.[fltWidth],I.[fltHeight],I.[fltWeight],ISNULL(I.numVendorID, 0) numVendorID,0 AS DropShip,
            '' AS SerialLotNo,
            ISNULL(t2.numUOMId, 0) AS numUOM,
            ISNULL(u.vcUnitName, '') vcUOMName,
            dbo.fn_UOMConversion(t2.numUOMId, t2.numItemCode,@numDomainID, NULL) AS UOMConversionFactor,
            0 AS numSOVendorId,
            NULL AS dtRentalStartDate,
            NULL dtRentalReturnDate,
            ISNULL(W.vcWareHouse, '') AS Warehouse,
            CASE WHEN charitemType = 'P' THEN WI.vcWHSKU ELSE I.vcSKU END vcSKU,
            CASE WHEN charitemType = 'P' THEN WI.vcBarCode ELSE I.numBarCodeId END vcBarcode,
            ISNULL(I.numItemClass, 0) AS numClassID,
            0 AS numProjectID,
            ISNULL(I.[numShipClass],0) AS numShipClass,
			@numOppBizDocsId AS numOppBizDocID,WL.vcLocation,ISNULL(bitSerialized,0) AS bitSerialized,ISNULL(bitLotNo,0) AS bitLotNo,
            @numOppId AS numOppId,t2.numQty AS numUnitHourOrig,
			ISNULL(I.numItemCode,0) AS [numItemCode],
			ISNULL(I.bitKitParent,0) AS bitKitParent,
			ISNULL(t2.numOppChildItemID,0) AS numOppChildItemID, 
			ISNULL(t2.numOppKitChildItemID,0) AS numOppKitChildItemID,
			1 AS bitChildItem,
			t2.tintLevel AS tintLevel
		FROM
			@TEMPTABLE t2
		INNER JOIN
			Item I
		ON
			t2.numItemCode = I.numItemCode
		LEFT JOIN
			WareHouseItems WI
		ON
			WI.numWareHouseItemID = t2.numWarehouseItemID
		LEFT JOIN 
			dbo.Warehouses W 
		ON 
			WI.numDomainID = W.numDomainID
            AND WI.numWareHouseID = W.numWareHouseID
        LEFT JOIN 
			dbo.WarehouseLocation WL 
		ON 
			WL.numWLocationID = WI.numWLocationID
		LEFT JOIN 
			ListDetails L 
		ON 
			I.numItemClassification = L.numListItemID
		LEFT JOIN 
			Vendor V 
		ON 
			V.numVendorID = @DivisionID
            AND V.numItemCode = t2.numItemCode
		LEFT JOIN 
			UOM u 
		ON 
			u.numUOMId = t2.numUOMId 
	END


    DECLARE @strSQLUpdate AS VARCHAR(2000);SET @strSQLUpdate=''
   
    EXEC ('ALTER TABLE #Temp1 ADD [TotalTax] MONEY')

    IF @tintTaxOperator = 1 --add Sales tax
	BEGIN
		SET @strSQLUpdate = @strSQLUpdate
            + ',[TotalTax]= dbo.fn_CalBizDocTaxAmt('
            + CONVERT(VARCHAR(20), @numDomainID) + ',0,'
            + CONVERT(VARCHAR(20), @numOppId) +','+
            + 'numoppitemtCode,1,Amount)'
	END        
    ELSE IF @tintTaxOperator = 2 -- remove sales tax
	BEGIN
        SET @strSQLUpdate = @strSQLUpdate + ',[TotalTax]= 0'
	END
    ELSE 
	BEGIN
        SET @strSQLUpdate = @strSQLUpdate
            + ',[TotalTax]= dbo.fn_CalBizDocTaxAmt('
            + CONVERT(VARCHAR(20), @numDomainID) + ',0,'
            + CONVERT(VARCHAR(20), @numOppId) +','+
            + 'numoppitemtCode,1,Amount)'
    END

	DECLARE @vcTaxName AS VARCHAR(100)
    DECLARE @numTaxItemID AS NUMERIC(9)
    SET @numTaxItemID = 0
    
	SELECT TOP 1
        @vcTaxName = vcTaxName,
        @numTaxItemID = numTaxItemID
    FROM    
		TaxItems
    WHERE   
		numDomainID = @numDomainID

    WHILE @numTaxItemID > 0
    BEGIN
        EXEC ('ALTER TABLE #Temp1 ADD [' + @vcTaxName + '] MONEY')

        SET @strSQLUpdate = @strSQLUpdate + ',[' + @vcTaxName
            + ']= dbo.fn_CalBizDocTaxAmt('
            + CONVERT(VARCHAR(20), @numDomainID) + ','
            + CONVERT(VARCHAR(20), @numTaxItemID) + ','
            + CONVERT(VARCHAR(20), @numOppId) +','+
            + 'numoppitemtCode,1,Amount)'

        SELECT TOP 1
			@vcTaxName = vcTaxName,
            @numTaxItemID = numTaxItemID
        FROM    
			TaxItems
        WHERE   
			numDomainID = @numDomainID
            AND numTaxItemID > @numTaxItemID

        IF @@rowcount = 0 
            SET @numTaxItemID = 0
    END

    IF @strSQLUpdate <> '' 
    BEGIN
        SET @strSQLUpdate = 'UPDATE #Temp1 SET ItemCode=ItemCode' + @strSQLUpdate + ' WHERE ItemCode > 0 AND bitChildItem=0'
        EXEC (@strSQLUpdate)
    END

    DECLARE @vcDbColumnName NVARCHAR(50)

    SELECT TOP 1
            @numFldID = numfieldID,
            @vcFldname = vcFieldName,
            @vcDbColumnName = vcDbColumnName,
            @intRowNum = ( tintRow + 1 )
    FROM    View_DynamicCustomColumns
    WHERE   numFormID = @tintType
            AND Grp_id = 5
            AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId
            AND bitCustom = 1
            AND ISNULL(numRelCntType, 0) = @numBizDocTempID
    ORDER BY tintRow     
	                                                                                     
    WHILE @intRowNum > 0                                                                                  
    BEGIN    
        EXEC('ALTER TABLE #Temp1 ADD [' + @vcDbColumnName + '] varchar(100)')
	
        SET @strSQLUpdate = 'UPDATE #Temp1 set ItemCode=ItemCode,[' + @vcDbColumnName + ']= dbo.GetCustFldValueItem(' + @numFldID  + ',ItemCode) WHERE ItemCode > 0 AND bitChildItem=0'
            
		EXEC (@strSQLUpdate)
		                   
        SELECT 
			TOP 1
            @numFldID = numfieldID,
            @vcFldname = vcFieldName,
            @vcDbColumnName = vcDbColumnName,
            @intRowNum = ( tintRow + 1 )
        FROM    
			View_DynamicCustomColumns
        WHERE   
			numFormID = @tintType
            AND Grp_id = 5
            AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId
            AND bitCustom = 1
            AND tintRow >= @intRowNum
            AND ISNULL(numRelCntType, 0) = @numBizDocTempID
        ORDER BY 
			tintRow                                                                  
		           
        IF @@rowcount = 0 
            SET @intRowNum = 0                                                                                          
    END


    SELECT  
		ROW_NUMBER() OVER ( ORDER BY numoppitemtCode ) RowNumber,
        *
    FROM    
		#Temp1
    ORDER BY 
		numoppitemtCode ASC, numOppChildItemID ASC, numOppKitChildItemID ASC

    DROP TABLE #Temp1

    SELECT  
		ISNULL(tintOppStatus, 0)
    FROM    
		OpportunityMaster
    WHERE   
		numOppID = @numOppId                                                                                                 


	IF (SELECT 
			COUNT(*) 
		FROM 
			View_DynamicColumns 
		WHERE   
			numFormID = @tintType 
			AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId 
			AND vcFieldType = 'R'
			AND ISNULL(numRelCntType, 0) = @numBizDocTempID
		)=0
	BEGIN            
		INSERT INTO DycFormConfigurationDetails 
		(
			numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numAuthGroupID,numRelCntType
		)
		SELECT 
			numFormId,numFieldId,1,tintRow,@numDomainID,@numBizDocId,@numBizDocTempID
		FROM 
			View_DynamicDefaultColumns
		WHERE 
			numFormId=@tintType and bitDefault=1 and numDomainID=@numDomainID order by tintRow ASC	
	 
	 
		IF (SELECT 
				COUNT(*) 
			FROM 
				DycFrmConfigBizDocsSumm Ds 
			WHERE 
				numBizDocID=@numBizDocId 
				AND Ds.numFormID=@tintType 
				AND ISNULL(Ds.numBizDocTempID,0)=@numBizDocTempID 
				AND Ds.numDomainID=@numDomainID
			)=0
		BEGIN
			INSERT INTO DycFrmConfigBizDocsSumm 
			(
				numFormID,numBizDocID,numFieldID,numDomainID,numBizDocTempID
			)
			SELECT 
				@tintType,@numBizDocId,DFM.numFieldID,@numDomainID,@numBizDocTempID 
			FROM
				DycFormField_Mapping DFFM 
			JOIN 
				DycFieldMaster DFM 
			ON 
				DFFM.numModuleID=DFFM.numModuleID and DFFM.numFieldID=DFM.numFieldID
			WHERE 
				numFormID=16 
				AND DFFM.numDomainID IS NULL
		END
	END      

    SELECT  
		vcFieldName AS vcFormFieldName,
        vcDbColumnName,
        vcFieldDataType,
        tintRow AS intRowNum
    FROM    
		View_DynamicColumns
    WHERE   
		numFormID = @tintType
        AND numDomainID = @numDomainID
        AND numAuthGroupID = @numBizDocId
        AND vcFieldType = 'R'
        AND ISNULL(numRelCntType, 0) = @numBizDocTempID
    UNION
    SELECT  
		vcFieldName AS vcFormFieldName,
        vcDbColumnName,
        'C' vcFieldDataType,
        tintRow AS intRowNum
    FROM    
		View_DynamicCustomColumns
    WHERE   
		numFormID = @tintType
        AND Grp_id = 5
        AND numDomainID = @numDomainID
        AND numAuthGroupID = @numBizDocId
        AND bitCustom = 1
        AND ISNULL(numRelCntType, 0) = @numBizDocTempID
    ORDER BY 
		tintRow
END
GO
/****** Object:  StoredProcedure [dbo].[USP_OPPCheckCanBeShipped]    Script Date: 07/26/2008 16:20:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec USP_OPPCheckCanBeShipped 5853,1  
--created by anoop jayaraj              
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_oppcheckcanbeshipped')
DROP PROCEDURE usp_oppcheckcanbeshipped
GO
CREATE PROCEDURE [dbo].[USP_OPPCheckCanBeShipped]              
@intOpportunityId as numeric(9),
@bitCheck AS BIT=0
AS              
BEGIN         
	CREATE TABLE #tempItem(vcItemName VARCHAR(300))   

	---REVERTING BACK TO PREVIOUS STATE IF DEAL IS BEING EDITED AFTER IT IS CLOSED
	DECLARE @OppType AS VARCHAR(2)
	DECLARE @vcItemName AS VARCHAR(300)
	DECLARE @vcKitItemName AS VARCHAR(300)
	DECLARE @Sel AS TINYINT   
	DECLARE @itemcode AS NUMERIC     
	DECLARE @numWareHouseItemID AS NUMERIC                                 
	DECLARE @numUnits AS FLOAT                                            
	DECLARE @numQtyShipped AS FLOAT
	DECLARE @numAllocation AS FLOAT                                                                                                                          
	DECLARE @numoppitemtCode AS NUMERIC(9)
	DECLARE @Kit AS BIT                   
	DECLARE @numQtyReceived AS FLOAT
	DECLARE @bitAsset as BIT

	SET @Sel=0          
	SELECT @OppType=tintOppType from OpportunityMaster where numOppId=@intOpportunityId
                                            
	SELECT TOP 1 
		@numoppitemtCode=numoppitemtCode,
		@itemcode=OI.numItemCode,
		@numUnits=ISNULL(numUnitHour,0),
		@numQtyShipped=ISNULL(numQtyShipped,0),
		@numQtyReceived=ISNULL(numUnitHourReceived,0),
		@numWareHouseItemID=numWarehouseItmsID,
		@bitAsset=ISNULL(I.bitAsset,0),
		@Kit= (CASE WHEN bitKitParent=1 AND bitAssembly=1 THEN 0 WHEN bitKitParent=1 THEN 1 ELSE 0 END),
		@vcItemName=OI.vcItemName 
	FROM 
		OpportunityItems OI                                            
	JOIN 
		Item I                                            
	ON 
		OI.numItemCode=I.numItemCode 
		AND numOppId=@intOpportunityId 
		AND (bitDropShip=0 OR bitDropShip IS NULL)                                        
	WHERE 
		charitemtype='P'  
	ORDER BY 
		OI.numoppitemtCode        
		                                      
	WHILE @numoppitemtCode>0                               
	BEGIN  
		IF @OppType=1 AND @Kit = 1
		BEGIN
			DECLARE @TEMPTABLE TABLE
			(
				numItemCode NUMERIC(18,0),
				numParentItemID NUMERIC(18,0),
				numOppChildItemID NUMERIC(18,0),
				vcItemName VARCHAR(300),
				bitKitParent BIT,
				numWarehouseItmsID NUMERIC(18,0),
				numQtyItemsReq FLOAT,
				numAllocation FLOAT
			)

			INSERT INTO 
				@TEMPTABLE
			SELECT
				OKI.numChildItemID,
				0,
				OKI.numOppChildItemID,
				I.vcItemName,
				I.bitKitParent,
				OKI.numWareHouseItemId,
				OKI.numQtyItemsReq_Orig * (ISNULL(OI.numUnitHour,0) - ISNULL(OI.numQtyShipped,0)),
				ISNULL(WI.numAllocation,0)
			FROM
				OpportunityKitItems OKI
			INNER JOIN
				WareHouseItems WI
			ON
				OKI.numWareHouseItemId = WI.numWareHouseItemID
			INNER JOIN
				OpportunityItems OI
			ON
				OKI.numOppItemID = OI.numoppitemtCode
			INNER JOIN
				Item I
			ON
				OKI.numChildItemID = I.numItemCode
			WHERE
				OKI.numOppID = @intOpportunityId
				AND OKI.numOppItemID = @numoppitemtCode
				AND OI.numoppitemtCode = @numoppitemtCode  

			INSERT INTO
				@TEMPTABLE
			SELECT
				OKCI.numItemID,
				t1.numItemCode,
				t1.numOppChildItemID,
				I.vcItemName,
				I.bitKitParent,
				OKCI.numWareHouseItemId,
				OKCI.numQtyItemsReq_Orig * t1.numQtyItemsReq,
				ISNULL(WI.numAllocation,0)
			FROM
				OpportunityKitChildItems OKCI
			INNER JOIN
				WareHouseItems WI
			ON
				OKCI.numWareHouseItemId = WI.numWareHouseItemID
			INNER JOIN
				@TEMPTABLE t1
			ON
				OKCI.numOppChildItemID = t1.numOppChildItemID
			INNER JOIN
				Item I
			ON
				OKCI.numItemID = I.numItemCode
			WHERE
				OKCI.numOppID = @intOpportunityId
				AND OKCI.numOppItemID = @numoppitemtCode
	
			IF (SELECT COUNT(*) FROM @TEMPTABLE WHERE ISNULL(bitKitParent,0) = 0 AND ISNULL(numQtyItemsReq,0) > ISNULL(numAllocation,0)) > 0
			BEGIN
				INSERT INTO #tempItem values (@vcItemName)
			END    
        END
		ELSE
		BEGIN
			SELECT                                          
				@numAllocation=isnull(numAllocation,0)                                           
			FROM 
				WareHouseItems 
			WHERE 
				numWareHouseItemID=@numWareHouseItemID  
				                                            
			IF @OppType=1                                              
			BEGIN          
				/*below is added to be validated from Sales fulfillment*/
				IF @bitCheck =1
				BEGIN
					IF @numUnits-@numQtyShipped>0
					BEGIN  
						IF (@bitAsset=0 )
						BEGIN
							INSERT INTO #tempItem values (@vcItemName)     
						END              
					 END            	
				END
				 
				SET @numUnits = @numUnits - @numQtyShipped
				
				IF @numUnits > @numAllocation   
				BEGIN  
					If (@bitAsset=0 )
					BEGIN
						INSERT INTO #tempItem VALUES (@vcItemName)  
					END                                                             
				END            
			END    
			ELSE IF @OppType=2
			BEGIN
				IF @bitCheck =1
				BEGIN
   					IF @numUnits-@numQtyReceived>0
					BEGIN  
						INSERT INTO #tempItem VALUES (@vcItemName)                              
					END  
			   END
			END
		END
                           
		SELECT TOP 1 
			@numoppitemtCode=numoppitemtCode,
			@itemcode=OI.numItemCode,
			@numUnits=ISNULL(numUnitHour,0),
			@numQtyShipped=ISNULL(numQtyShipped,0),
			@numQtyReceived=ISNULL(numUnitHourReceived,0),
			@numWareHouseItemID=numWarehouseItmsID,@Kit= (case when bitKitParent=1 and bitAssembly=1 then 0 when bitKitParent=1 then 1 else 0 end),@vcItemName=OI.vcItemName from OpportunityItems OI                                            
		JOIN 
			Item I                                            
		ON 
			OI.numItemCode=I.numItemCode and numOppId=@intOpportunityId                                          
		WHERE 
			charitemtype='P' 
			AND OI.numoppitemtCode>@numoppitemtCode 
			AND (bitDropShip=0 or bitDropShip is null) 
		ORDER BY 
			OI.numoppitemtCode                                            
    
		IF @@rowcount=0 SET @numoppitemtCode=0    
	END   
  
	SELECT * FROM #tempItem
	DROP TABLE #tempItem
END
GO
/****** Object:  StoredProcedure [dbo].[USP_OPPItemDetails]    Script Date: 09/01/2009 18:59:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- exec USP_OPPItemDetails @numItemCode=31455,@OpportunityId=12387,@numDomainId=110
--created by anooop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_oppitemdetails')
DROP PROCEDURE usp_oppitemdetails
GO
CREATE PROCEDURE [dbo].[USP_OPPItemDetails]
    (
      @numUserCntID NUMERIC(9) = 0,
      @OpportunityId NUMERIC(9) = 0,
      @numDomainId NUMERIC(9),
      @ClientTimeZoneOffset  INT
    )
AS 
    BEGIN
        IF @OpportunityId >0
            BEGIN
                DECLARE @DivisionID AS NUMERIC(9)
                DECLARE @TaxPercentage AS FLOAT
                DECLARE @tintOppType AS TINYINT
                DECLARE @fld_Name AS VARCHAR(500)
                DECLARE @strSQL AS VARCHAR(MAX)
                DECLARE @fld_ID AS NUMERIC(9)
                DECLARE @numBillCountry AS NUMERIC(9)
                DECLARE @numBillState AS NUMERIC(9)

                
                SELECT  @DivisionID = numDivisionID,
                        @tintOppType = tintOpptype
                FROM    OpportunityMaster
                WHERE   numOppId = @OpportunityId
               

SET @strSQL = ''

--------------Make Dynamic Query--------------
SET @strSQL = @strSQL + '
Select 
Opp.numoppitemtCode,
ISNULL(Opp.bitItemPriceApprovalRequired,0) AS bitItemPriceApprovalRequired,
OM.tintOppType,
Opp.numItemCode,
I.charItemType,
ISNULL(I.bitAsset,0) as bitAsset,
ISNULL(I.bitRental,0) as bitRental,
Opp.numOppId,
Opp.bitWorkOrder,
Opp.fltDiscount,
ISNULL(( SELECT numReturnID FROM   Returns WHERE  numOppItemCode = Opp.numoppitemtCode AND numOppId = Opp.numOppID), 0) AS numReturnID,
dbo.fn_GetReturnItemQty(Opp.numoppitemtCode) AS ReturrnedQty,
[dbo].fn_GetReturnStatus(Opp.numoppitemtCode) AS ReturnStatus,
ISNULL(opp.numUOMId, 0) AS numUOM,
(SELECT vcUnitName FROM dbo.UOM WHERE numUOMId = opp.numUOMId AND ISNULL(bitEnabled,0) = 1)  AS  [vcBaseUOMName],
Opp.numUnitHour AS numOriginalUnitHour, 
isnull(M.vcPOppName,''Internal'') as Source,
numSourceID,
  CASE WHEN ISNULL(bitKitParent,0) = 1
                               AND ISNULL(bitAssembly,0) = 1 THEN 0
                          WHEN ISNULL(bitKitParent,0) = 1 THEN 1
                          ELSE 0
                     END as bitKitParent,
(CASE WHEN OM.tintOppType = 1 AND OM.tintOppStatus = 1 THEN 
(CASE WHEN (I.charItemType = ''p'' AND ISNULL(bitDropShip, 0) = 0 AND ISNULL(bitAsset,0)=0)THEN dbo.CheckOrderItemInventoryStatus(Opp.numItemCode,Opp.numUnitHour,Opp.numoppitemtCode,isnull(Opp.numWarehouseItmsID,0),(CASE WHEN ISNULL(bitKitParent,0) = 1
                               AND ISNULL(bitAssembly,0) = 1 THEN 0
                          WHEN ISNULL(bitKitParent,0) = 1 THEN 1
                          ELSE 0
                     END)) ELSE 0 END) ELSE 0 END) AS bitBackOrder,
(SELECT vcData FROM dbo.ListDetails WHERE 1=1 AND numListItemID = numShipClass ) AS [numShipClass],ISNULL(Opp.numQtyShipped,0) numQtyShipped,ISNULL(Opp.numUnitHourReceived,0) AS numUnitHourReceived,ISNULL(OM.fltExchangeRate,0) AS fltExchangeRate,ISNULL(OM.numCurrencyID,0) AS numCurrencyID,ISNULL(OM.numDivisionID,0) AS numDivisionID,
(CASE 
	WHEN OM.tintOppType = 1 AND OM.tintOppStatus = 1 AND UPPER(I.charItemType) = ''P'' AND bitDropShip = 0
	THEN (CASE WHEN OM.tintShipped = 1 THEN CAST(ISNULL(Opp.numUnitHour,0) AS VARCHAR) ELSE CAST(ISNULL(Opp.numQtyShipped,0) AS VARCHAR) END)
	WHEN OM.tintOppType = 2 AND OM.tintOppStatus = 1 AND UPPER(I.charItemType) = ''P'' AND bitDropShip = 0
	THEN (CASE WHEN OM.tintShipped = 1 THEN CAST(ISNULL(Opp.numUnitHour,0) AS VARCHAR) ELSE CAST(ISNULL(Opp.numUnitHourReceived,0) AS VARCHAR) END)
	ELSE '''' END) AS vcShippedReceived,
isnull(i.numIncomeChartAcntId,0) as itemIncomeAccount,isnull(i.numAssetChartAcntId,0) as itemInventoryAsset,isnull(i.numCOGsChartAcntId,0) as itemCoGs,
ISNULL(Opp.numRentalIN,0) as numRentalIN,
ISNULL(Opp.numRentalLost,0) as numRentalLost,
ISNULL(Opp.numRentalOut,0) as numRentalOut,
(SELECT vcData FROM dbo.ListDetails WHERE 1=1 AND numListItemID = numShipClass ) AS [numShipClass],ISNULL(bitFreeShipping,0) AS [IsFreeShipping],Opp.bitDiscountType,isnull(Opp.monLandedCost,0) as monLandedCost, '

--	IF @fld_Name = 'vcPathForTImage' 
		SET @strSQL = @strSQL + 'Opp.vcPathForTImage,'
--	ELSE IF @fld_Name = 'vcItemName' 
		SET @strSQL = @strSQL + 'Opp.vcItemName,'
--	ELSE IF @fld_Name = 'vcModelID,' 
		SET @strSQL = @strSQL + 'Opp.vcModelID,'
--	ELSE IF @fld_Name = 'vcWareHouse' 
		SET @strSQL = @strSQL + 'vcWarehouse,'
		SET @strSQL = @strSQL + 'WItems.numOnHand,isnull(WItems.numAllocation,0) as numAllocation,'
		SET @strSQL = @strSQL + 'WL.vcLocation,'
--	ELSE IF @fld_Name = 'ItemType' 
		SET @strSQL = @strSQL + 'Opp.vcType ItemType,Opp.vcType,'
--	ELSE IF @fld_Name = 'vcItemDesc' 
		SET @strSQL = @strSQL + 'ISNULL(Opp.vcItemDesc, '''') vcItemDesc,'
--	ELSE IF @fld_Name = 'Attributes' 
		SET @strSQL = @strSQL + 'Opp.vcAttributes,' --dbo.fn_GetAttributes(Opp.numWarehouseItmsID, I.bitSerialized) AS Attributes,
--	ELSE IF @fld_Name = 'bitDropShip' 
		SET @strSQL = @strSQL + 'ISNULL(bitDropShip, 0) as DropShip,CASE ISNULL(bitDropShip, 0) WHEN 1 THEN ''Yes'' ELSE '''' END AS bitDropShip,'
--	ELSE IF @fld_Name = 'numUnitHour' 
		SET @strSQL = @strSQL + 'CAST(( dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), Opp.numItemCode,om.numDomainId, ISNULL(opp.numUOMId, 0))* Opp.numUnitHour ) AS NUMERIC(18, 2)) AS numUnitHour,'
--	ELSE IF @fld_Name = 'monPrice' 
		SET @strSQL = @strSQL + 'Opp.monPrice,'
--	ELSE IF @fld_Name = 'monPriceUOM' 
		SET @strSQL = @strSQL + 'CAST(( dbo.fn_UOMConversion(ISNULL(opp.numUOMId, 0), Opp.numItemCode,om.numDomainId, ISNULL(I.numBaseUnit, 0)) * Opp.monPrice) AS NUMERIC(18, 4)) AS monPriceUOM,'
--	ELSE IF @fld_Name = 'monTotAmount' 
		SET @strSQL = @strSQL + 'Opp.monTotAmount,'
--	ELSE IF @fld_Name = 'vcSKU' 
		SET @strSQL = @strSQL + 'ISNULL(WItems.vcWHSKU,ISNULL(I.vcSKU,'''')) [vcSKU],'
--	ELSE IF @fld_Name = 'vcManufacturer' 
		SET @strSQL = @strSQL + 'Opp.vcManufacturer,'
--	ELSE IF @fld_Name = 'vcItemClassification' 
		SET @strSQL = @strSQL + ' dbo.[fn_GetListItemName](I.[numItemClassification]) numItemClassification,'
--	ELSE IF @fld_Name = 'numItemGroup' 
		SET @strSQL = @strSQL + '( SELECT    [vcItemGroup] FROM      [ItemGroups] WHERE     [numItemGroupID] = I.numItemGroup ) numItemGroup,'
--	ELSE IF @fld_Name = 'vcUOMName' 
		SET @strSQL = @strSQL + 'ISNULL(u.vcUnitName, '''') numUOMId,'
--	ELSE IF @fld_Name = 'fltWeight' 
		SET @strSQL = @strSQL + 'I.fltWeight,'
--	ELSE IF @fld_Name = 'fltHeight' 
		SET @strSQL = @strSQL + 'I.fltHeight,'
--	ELSE IF @fld_Name = 'fltWidth' 
		SET @strSQL = @strSQL + 'I.fltWidth,'
--	ELSE IF @fld_Name = 'fltLength' 
		SET @strSQL = @strSQL + 'I.fltLength,'
--	ELSE IF @fld_Name = 'numBarCodeId' 
		SET @strSQL = @strSQL + 'I.numBarCodeId,'
		SET @strSQL = @strSQL + 'I.IsArchieve,'
--	ELSE IF @fld_Name = 'vcWorkOrderStatus' 
		SET @strSQL = @strSQL + 'CASE WHEN ISNULL(Opp.bitWorkOrder, 0) = 1
				 THEN ( SELECT  dbo.GetListIemName(numWOStatus)
								+ CASE WHEN ISNULL(numWOStatus, 0) = 23184
									   THEN '' - ''
											+ CAST(ISNULL(dbo.fn_GetContactName(numAssignedTo),
														  '''') + '',''
											+ CONVERT(VARCHAR(20), DATEADD(minute, '+ CONVERT(VARCHAR(15),-@ClientTimeZoneOffset)  +', bintCompletedDate)) AS VARCHAR(100))
									   ELSE ''''
								  END
						FROM    WorkOrder
						WHERE   ISNULL(numOppId, 0) = Opp.numOppId
								AND numItemCode = Opp.numItemCode AND ISNULL(numParentWOID,0)=0
					  )
				 ELSE ''''
			END AS vcWorkOrderStatus,'
--	ELSE IF @fld_Name = 'DiscAmt' 
		SET @strSQL = @strSQL + 'CASE WHEN Opp.bitDiscountType = 0
				 THEN REPLACE(STR(ISNULL(Opp.monTotAmtBefDiscount, Opp.monTotAmount)
					  - Opp.monTotAmount,20,4), '' '', '''') + '' (''
					  + CAST(FORMAT(Opp.fltDiscount,''0.00##'') AS VARCHAR(50)) + ''%)''
				 ELSE REPLACE(STR(ISNULL(Opp.monTotAmtBefDiscount, Opp.monTotAmount)
					  - Opp.monTotAmount,20,4), '' '', '''')
			END AS DiscAmt,'	
--	ELSE IF @fld_Name = 'numProjectID' 
		SET @strSQL = @strSQL + ' ISNULL(Opp.numProjectID, 0) numProjectID,
		 CASE WHEN ( SELECT TOP 1
								ISNULL(OBI.numOppBizDocItemID, 0)
						FROM    dbo.OpportunityBizDocs OBD
								INNER JOIN dbo.OpportunityBizDocItems OBI ON OBD.numOppBizDocsID = OBI.numOppBizDocID
						WHERE   OBI.numOppItemID = Opp.numoppitemtCode
								AND ISNULL(OBD.bitAuthoritativeBizDocs, 0) = 1
								and isnull(OBD.numOppID,0)=OM.numOppID
					  ) > 0 THEN 1
				 ELSE 0
			END AS bitItemAddedToAuthBizDoc,'	
--	ELSE IF @fld_Name = 'numClassID' 
		SET @strSQL = @strSQL + 'ISNULL(Opp.numClassID, 0) numClassID,'	
--	ELSE IF @fld_Name = 'vcProjectStageName' 
		SET @strSQL = @strSQL + ' ISNULL(Opp.numProjectStageID, 0) numProjectStageID,
				CASE WHEN OPP.numProjectStageID > 0
				 THEN dbo.fn_GetProjectStageName(OPP.numProjectId,
												 OPP.numProjectStageID)
				 WHEN OPP.numProjectStageID = -1 THEN ''T&E''
				 WHEN OPP.numProjectId > 0
				 THEN CASE WHEN OM.tintOppType = 1 THEN ''S.O.''
						   ELSE ''P.O.''
					  END
				 ELSE ''-''
			END AS vcProjectStageName,'	
	        
	
	

--------------Add custom fields to query----------------
SELECT TOP 1 @fld_Name= vcFieldName,@fld_ID = numFieldId FROM View_DynamicCustomColumns
WHERE 
grp_id = 5 AND numDomainID=@numDomainId
AND numFormId=26 AND bitCustom=1 AND numUserCntID = @numUserCntID ORDER BY numFieldId
WHILE @fld_ID > 0
BEGIN ------------------

	 PRINT @fld_Name
	
	       SET @strSQL = @strSQL + 'dbo.GetCustFldValueItem('
                                    + CONVERT(VARCHAR(15), @fld_ID)
                                    + ',Opp.numItemCode) as ''' + @fld_Name + ''','
       

SELECT TOP 1 @fld_Name= vcFieldName,@fld_ID = numFieldId FROM View_DynamicCustomColumns
WHERE 
grp_id = 5 AND numDomainID=@numDomainId
AND numFormId=26 AND bitCustom=1  AND numUserCntID = @numUserCntID AND numFieldId > @fld_ID ORDER BY numFieldId
 IF @@ROWCOUNT=0
 SET @fld_ID = 0
END ------------------


                                
--Purchase Opportunity ?
IF @tintOppType = 2 
    BEGIN
        SET @strSQL =  @strSQL + 'ISNULL((SELECT sum(numQty) FROM OppWarehouseSerializedItem WHERE numOppID=OM.numOppID AND numOppItemID=Opp.numoppitemtCode),0) AS SerialLotNo,
		  SUBSTRING(( SELECT  '', '' + vcSerialNo
                        + CASE WHEN ISNULL(I.bitLotNo, 0) = 1
                                THEN '' (''
                                    + CONVERT(VARCHAR(15), oppI.numQty)
                                    + '')''
                                ELSE ''''
                            END
                FROM    OppWarehouseSerializedItem oppI
                        JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID = whi.numWareHouseItmsDTLID
                WHERE   oppI.numOppID = Opp.numOppId
                        AND oppI.numOppItemID = Opp.numoppitemtCode
                ORDER BY vcSerialNo
                FOR
                XML PATH('''')
                ), 3, 200000) AS vcSerialLotNo,
		  CAST(Opp.numUnitHour AS FLOAT) numQty,
		  (SELECT vcUnitName FROM dbo.UOM WHERE numUOMId = opp.numUOMId AND ISNULL(bitEnabled,0) = 1)  AS  [vcBaseUOMName],
		  I.bitSerialized,
		  ISNULL(I.bitLotNo, 0) AS bitLotNo,
		  isnull(Opp.numWarehouseItmsID,0) as numWarehouseItmsID,'
	END
ELSE
	BEGIN	
        SET @strSQL =  @strSQL + ' ISNULL((SELECT sum(numQty) FROM OppWarehouseSerializedItem WHERE numOppID=OM.numOppID AND numOppItemID=Opp.numoppitemtCode),0) AS SerialLotNo,
		   SUBSTRING(( SELECT  '', '' + vcSerialNo
                        + CASE WHEN ISNULL(I.bitLotNo, 0) = 1
                                THEN '' (''
                                    + CONVERT(VARCHAR(15), oppI.numQty)
                                    + '')''
                                ELSE ''''
                            END
                FROM    OppWarehouseSerializedItem oppI
                        JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID = whi.numWareHouseItmsDTLID
                WHERE   oppI.numOppID = Opp.numOppId
                        AND oppI.numOppItemID = Opp.numoppitemtCode
                ORDER BY vcSerialNo
                FOR
                XML PATH('''')
                ), 3, 200000) AS vcSerialLotNo,
		   CAST(Opp.numUnitHour AS FLOAT) numQty,
		  I.bitSerialized,
		  ISNULL(I.bitLotNo, 0) AS bitLotNo,
		  isnull(Opp.numWarehouseItmsID,0) as numWarehouseItmsID,'
	END


        
 SET @strSQL = left(@strSQL,len(@strSQL)-1) + '   
        FROM    OpportunityItems Opp
        LEFT JOIN OpportunityMaster OM ON Opp.numOppID = oM.numOppID
        JOIN item I ON Opp.numItemCode = i.numItemcode
        LEFT JOIN WareHouseItems WItems ON WItems.numWareHouseItemID = Opp.numWarehouseItmsID
        LEFT JOIN Warehouses W ON W.numWarehouseID = WItems.numWarehouseID
        LEFT JOIN WarehouseLocation WL ON WL.numWLocationID = WItems.numWLocationID
        LEFT JOIN OpportunityMaster M ON Opp.numSourceID = M.numOppID
        LEFT JOIN UOM u ON u.numUOMId = opp.numUOMId
WHERE   om.numDomainId = '+ CONVERT(VARCHAR(10),@numDomainId) + '
        AND Opp.numOppId = '+ CONVERT(VARCHAR(15),@OpportunityId) +'
ORDER BY numoppitemtCode ASC '
  
PRINT @strSQL;
EXEC (@strSQL);
SELECT  vcSerialNo,
       vcComments AS Comments,
        numOppItemID AS numoppitemtCode,
        W.numWarehouseItmsDTLID,
        numWarehouseItmsID AS numWItmsID,
        dbo.Fn_getattributes(W.numWarehouseItmsDTLID,
                             1) Attributes
FROM    WareHouseItmsDTL W
        JOIN OppWarehouseSerializedItem O ON O.numWarehouseItmsDTLID = W.numWareHouseItmsDTLID
WHERE   numOppID = @OpportunityId


CREATE TABLE #tempForm (tintOrder TINYINT,vcDbColumnName nvarchar(50),vcOrigDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),
  vcListItemType varchar(3),numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,
  numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,bitIsAlphaNumeric BIT,bitIsNumeric BIT,
  bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),ListRelID numeric(9),intColumnWidth int,intVisible int,vcFieldDataType CHAR(1))


IF
	(
		SELECT 
			COUNT(*)
		FROM 
			View_DynamicColumns DC 
		WHERE 
			DC.numFormId=26 and DC.numDomainID=@numDomainID AND numUserCntID = @numUserCntID AND
			ISNULL(DC.bitSettingField,0)=1 AND ISNULL(DC.bitDeleted,0)=0 AND ISNULL(DC.bitCustom,0)=0) > 0
	OR
	(
		SELECT 
			COUNT(*)
		FROM 
			View_DynamicCustomColumns
		WHERE 
			numFormId=26 AND numUserCntID=@numUserCntID AND numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=0 AND ISNULL(bitCustom,0)=1
	) > 0
BEGIN	

INSERT INTO #tempForm
select isnull(DC.tintRow,0)+1 as tintOrder,DDF.vcDbColumnName,DDF.vcOrigDbColumnName,ISNULL(DDF.vcCultureFieldName,DDF.vcFieldName),                  
 DDF.vcAssociatedControlType,DDF.vcListItemType,DDF.numListID,DDF.vcLookBackTableName                                                 
,DDF.bitCustom,DDF.numFieldId,DDF.bitAllowSorting,DDF.bitInlineEdit,
DDF.bitIsRequired,DDF.bitIsEmail,DDF.bitIsAlphaNumeric,DDF.bitIsNumeric,DDF.bitIsLengthValidation,
DDF.intMaxLength,DDF.intMinLength,DDF.bitFieldMessage,DDF.vcFieldMessage vcFieldMessage,DDF.ListRelID,DDF.intColumnWidth,Case when DC.numFieldID is null then 0 else 1 end,DDF.vcFieldDataType
 FROM View_DynamicDefaultColumns DDF left join View_DynamicColumns DC on DC.numFormId=26 and DDF.numFieldId=DC.numFieldId and DC.numUserCntID=@numUserCntID and DC.numDomainID=@numDomainID and numRelCntType=0 AND tintPageType=1 
 where DDF.numFormId=26 and DDF.numDomainID=@numDomainID AND ISNULL(DDF.bitSettingField,0)=1 AND ISNULL(DDF.bitDeleted,0)=0 AND ISNULL(DDF.bitCustom,0)=0
       
 UNION
    
    select tintRow+1 as tintOrder,vcDbColumnName,vcFieldName,vcFieldName,                  
 vcAssociatedControlType,'' as vcListItemType,numListID,''                                                 
,bitCustom,numFieldId,bitAllowSorting,0 as bitAllowEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,''
 from View_DynamicCustomColumns
 where numFormId=26 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=0
 AND ISNULL(bitCustom,0)=1

END
ELSE
BEGIN
	--Check if Master Form Configuration is created by administrator if NoColumns=0
	DECLARE @IsMasterConfAvailable BIT = 0
	DECLARE @numUserGroup NUMERIC(18,0)

	SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

	IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = 26 AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
	BEGIN
		SET @IsMasterConfAvailable = 1
	END

	--If MasterConfiguration is available then load it otherwise load default columns
	IF @IsMasterConfAvailable = 1
	BEGIN
		INSERT INTO #tempForm
		SELECT 
			(intRowNum + 1) as tintOrder, vcDbColumnName, vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
			vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName,bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,
			bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth,1,vcFieldDataType
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=26 AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcFieldName,vcFieldName,                  
			 vcAssociatedControlType,'' as vcListItemType,numListID,''                                                 
			,bitCustom,numFieldId,bitAllowSorting,0 as bitAllowEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,''
		FROM 
			View_DynamicCustomColumnsMasterConfig
		WHERE 
			View_DynamicCustomColumnsMasterConfig.numFormId=26 AND 
			View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1
		ORDER BY 
			tintOrder ASC  
	END
END


select * from #tempForm order by tintOrder

drop table #tempForm

            END
            
    END
/****** Object:  StoredProcedure [dbo].[USP_OppManage]    Script Date: 07/26/2008 16:20:19 ******/
--2 changes
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OppManage')
DROP PROCEDURE usp_oppmanage
GO
CREATE PROCEDURE [dbo].[USP_OppManage] 
(                                                                          
  @numOppID as numeric=0 OUTPUT,                                                                          
  @numContactId numeric(9)=null,                                                                          
  @numDivisionId numeric(9)=null,                                                                          
  @tintSource numeric(9)=null,                                                                          
  @vcPOppName Varchar(100)='' OUTPUT,                                                                          
  @Comments varchar(1000)='',                                                                          
  @bitPublicFlag bit=0,                                                                          
  @numUserCntID numeric(9)=0,                                                                                 
  @monPAmount money =0,                                                 
  @numAssignedTo as numeric(9)=0,                                                                                                        
  @numDomainId numeric(9)=0,                                                                                                                                                                                   
  @strItems as text=null,                                                                          
  @strMilestone as text=null,                                                                          
  @dtEstimatedCloseDate datetime,                                                                          
  @CampaignID as numeric(9)=null,                                                                          
  @lngPConclAnalysis as numeric(9)=null,                                                                         
  @tintOppType as tinyint,                                                                                                                                             
  @tintActive as tinyint,                                                              
  @numSalesOrPurType as numeric(9),              
  @numRecurringId as numeric(9),
  @numCurrencyID as numeric(9)=0,
  @DealStatus as tinyint =0,-- 0=Open,1=Won,2=Lost
  @numStatus AS NUMERIC(9),
  @vcOppRefOrderNo VARCHAR(100),
  @numBillAddressId numeric(9)=0,
  @numShipAddressId numeric(9)=0,
  @bitStockTransfer BIT=0,
  @WebApiId INT = 0,
  @tintSourceType TINYINT=0,
  @bitDiscountType as bit,
  @fltDiscount  as float,
  @bitBillingTerms as bit,
  @intBillingDays as integer,
  @bitInterestType as bit,
  @fltInterest as float,
  @tintTaxOperator AS TINYINT,
  @numDiscountAcntType AS NUMERIC(9),
  @vcWebApiOrderNo VARCHAR(100)=NULL,
  @vcCouponCode		VARCHAR(100) = '',
  @vcMarketplaceOrderID VARCHAR(100)=NULL,
  @vcMarketplaceOrderDate  datetime=NULL,
  @vcMarketplaceOrderReportId VARCHAR(100)=NULL,
  @numPercentageComplete NUMERIC(9),
  @bitUseShippersAccountNo BIT = 0,
  @bitUseMarkupShippingRate BIT = 0,
  @numMarkupShippingRate NUMERIC(19,2) = 0,
  @intUsedShippingCompany INT = 0
)                                                                          
                                                                          
as  
BEGIN TRY
BEGIN TRANSACTION
	DECLARE @fltExchangeRate float                                 
	DECLARE @hDocItem int                                                                                                                            
	DECLARE @TotalAmount as money                                                                          
	DECLARE @tintOppStatus as tinyint               

	--IF NEW OPPERTUNITY
	IF @numOppID = 0                                                                          
	BEGIN
		DECLARE @intOppTcode AS NUMERIC(9)  
  
		SELECT @intOppTcode = MAX(numOppId) FROM OpportunityMaster                

		SET @intOppTcode = ISNULL(@intOppTcode,0) + 1                                                
		
		SET @vcPOppName = @vcPOppName + '-'+ CONVERT(varchar(4),YEAR(GETUTCDATE()))  +'-A'+ CONVERT(varchar(10),@intOppTcode)                                  
  
		IF ISNULL(@numCurrencyID,0) = 0 
		BEGIN
			SET @fltExchangeRate=1
			SELECT @numCurrencyID= ISNULL(numCurrencyID,0) FROM dbo.Domain WHERE numDomainId=@numDomainId	
		END
		ELSE
		BEGIN 
			SET @fltExchangeRate = dbo.GetExchangeRate(@numDomainID,@numCurrencyID)
        END  
		                                                        
		-- GET ACCOUNT CLASS IF ENABLED
		DECLARE @numAccountClass AS NUMERIC(18) = 0

		DECLARE @tintDefaultClassType AS INT = 0
		SELECT @tintDefaultClassType = ISNULL(tintDefaultClassType,0) FROM Domain WHERE numDomainId = @numDomainID 

		IF @tintDefaultClassType = 1 --USER
		BEGIN
			SELECT 
				@numAccountClass=ISNULL(numDefaultClass,0) 
			FROM 
				dbo.UserMaster UM 
			JOIN 
				dbo.Domain D 
			ON 
				UM.numDomainID=D.numDomainId
			WHERE 
				D.numDomainId=@numDomainId 
				AND UM.numUserDetailId=@numUserCntID
		END
		ELSE IF @tintDefaultClassType = 2 --COMPANY
		BEGIN
			SELECT 
				@numAccountClass=ISNULL(numAccountClassID,0) 
			FROM 
				dbo.DivisionMaster DM 
			WHERE 
				DM.numDomainId=@numDomainId 
				AND DM.numDivisionID=@numDivisionID
		END
		ELSE
		BEGIN
			SET @numAccountClass = 0
		END
                                                                       
		INSERT INTO OpportunityMaster
		(                                                                             
			numContactId,numDivisionId,txtComments,numCampainID,bitPublicFlag,tintSource,tintSourceType,vcPOppName,                                                                          
			intPEstimatedCloseDate,monPAmount,numCreatedBy,bintCreatedDate,numModifiedBy,bintModifiedDate,numDomainId,                                                                                                             
			numRecOwner,lngPConclAnalysis,tintOppType,numSalesOrPurType,numCurrencyID,fltExchangeRate,numAssignedTo,
			numAssignedBy,[tintOppStatus],numStatus,vcOppRefOrderNo,bitStockTransfer,bitBillingTerms,intBillingDays,
			bitInterestType,fltInterest,vcCouponCode,bitDiscountType,fltDiscount,bitPPVariance,vcMarketplaceOrderID,
			vcMarketplaceOrderReportId,numPercentageComplete,numAccountClass,tintTaxOperator,bitUseShippersAccountNo,
			bitUseMarkupShippingRate,numMarkupShippingRate,intUsedShippingCompany
		)                                                                          
		Values                                                                          
		(                                                                          
			@numContactId,@numDivisionId,@Comments,@CampaignID,@bitPublicFlag,@tintSource,@tintSourceType,@vcPOppName,                                                                          
			@dtEstimatedCloseDate,@monPAmount,@numUserCntID,(CASE @tintSourceType WHEN 3 THEN @vcMarketplaceOrderDate ELSE GETUTCDATE() END),                                                                          
			@numUserCntID,GETUTCDATE(),@numDomainId,@numUserCntID,@lngPConclAnalysis,@tintOppType,@numSalesOrPurType,@numCurrencyID, 
			@fltExchangeRate,(CASE WHEN @numAssignedTo > 0 THEN @numAssignedTo ELSE NULL END),(CASE WHEN @numAssignedTo>0 THEN @numUserCntID ELSE NULL END),
			@DealStatus,@numStatus,@vcOppRefOrderNo,@bitStockTransfer,@bitBillingTerms,@intBillingDays,@bitInterestType,@fltInterest,
			@vcCouponCode,@bitDiscountType,@fltDiscount,1,@vcMarketplaceOrderID,@vcMarketplaceOrderReportId,@numPercentageComplete,
			@numAccountClass,@tintTaxOperator,@bitUseShippersAccountNo,@bitUseMarkupShippingRate,@numMarkupShippingRate,@intUsedShippingCompany
		)                                                                                                                      
		
		SET @numOppID=SCOPE_IDENTITY()                                                
  
		--UPDATE OPPNAME AS PER NAME TEMPLATE
		EXEC dbo.USP_UpdateNameTemplateValue @tintOppType,@numDomainID,@numOppID

		--MAP CUSTOM FIELD	
		DECLARE @tintPageID AS TINYINT;
		SET @tintPageID = CASE WHEN @tintOppType=1 THEN 2 ELSE 6 END 
  	
		EXEC dbo.USP_AddParentChildCustomFieldMap
				@numDomainID = @numDomainID, --  numeric(18, 0)
				@numRecordID = @numOppID, --  numeric(18, 0)
				@numParentRecId = @numDivisionId, --  numeric(18, 0)
				@tintPageID = @tintPageID --  tinyint
 	
		IF ISNULL(@numStatus,0) > 0 AND @tintOppType=1 AND isnull(@DealStatus,0)=1
		BEGIN
	   		EXEC USP_ManageOpportunityAutomationQueue
					@numOppQueueID = 0, -- numeric(18, 0)
					@numDomainID = @numDomainID, -- numeric(18, 0)
					@numOppId = @numOppID, -- numeric(18, 0)
					@numOppBizDocsId = 0, -- numeric(18, 0)
					@numOrderStatus = @numStatus, -- numeric(18, 0)
					@numUserCntID = @numUserCntID, -- numeric(18, 0)
					@tintProcessStatus = 1, -- tinyint
					@tintMode = 1 -- TINYINT
		END
  
		-- INSERTING ITEMS                                                                                                                    
		IF CONVERT(VARCHAR(10),@strItems) <> '' AND @numOppID > 0
		BEGIN
			EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems       
	
			INSERT INTO OpportunityItems
			(
				numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,bitDropShip,bitDiscountType,fltDiscount,
				monTotAmtBefDiscount,vcItemName,vcModelID,vcManufacturer,vcPathForTImage,monVendorCost,numUOMId,bitWorkOrder,numVendorWareHouse,
				numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,numClassID,numToWarehouseItemID,vcAttributes,monAvgCost,bitItemPriceApprovalRequired
			)
			SELECT 
				@numOppID,X.numItemCode,dbo.fn_UOMConversion(X.numUOM,numItemCode,@numDomainId,null) * x.numUnitHour,x.monPrice,x.monTotAmount,X.vcItemDesc,
				(CASE X.numWarehouseItmsID 
				WHEN -1 
				THEN 
					CASE 
						WHEN ((SELECT COUNT(*) FROM Item WHERE numDomainID=@numDomainId AND vcSKU= X.vcSKU) > 0)
							THEN
								(SELECT TOP 1 
									numWareHouseItemID 
								FROM 
									WareHouseItems 
								WHERE 
									[numItemID] = X.numItemCode 
									AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId))
						ELSE
							(SELECT 
								[numWareHouseItemID] 
							FROM 
								[WareHouseItems] 
							WHERE 
								[numItemID] = X.numItemCode 
								AND ISNULL(WareHouseItems.vcWHSKU,'') = ISNULL(X.vcSKU,'')
								AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId)) 
					END
				WHEN 0 
				THEN 
					CASE 
					WHEN ((SELECT COUNT(*) FROM Item WHERE numDomainID=@numDomainId AND vcSKU=X.vcSKU) > 0)
						THEN
							(SELECT TOP 1 
								numWareHouseItemID 
							FROM 
								WareHouseItems 
							WHERE 
								[numItemID] = X.numItemCode 
								AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId))
					ELSE
						(SELECT 
							[numWareHouseItemID] 
						FROM 
							[WareHouseItems] 
						WHERE 
							[numItemID] = X.numItemCode 
							AND ISNULL(WareHouseItems.vcWHSKU,'') = ISNULL(X.vcSKU,'')
							AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId)) 
					END
				ELSE  
					X.numWarehouseItmsID 
				END) AS numWarehouseItmsID,
				X.ItemType,X.DropShip,X.bitDiscountType,X.fltDiscount,X.monTotAmtBefDiscount,X.vcItemName,(SELECT vcModelID FROM item WHERE numItemCode = X.numItemCode),
				(SELECT vcManufacturer FROM item WHERE numItemCode = X.numItemCode),(SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = X.numItemCode AND BitDefault = 1 AND numdomainId = @numdomainId),
				(SELECT isnull(VN.monCost,0) FROM Vendor VN, Item IT WHERE IT.numItemCode=VN.numItemCode AND VN.numItemCode=X.numItemCode AND VN.numVendorID=IT.numVendorID),
				X.numUOM,X.bitWorkOrder,X.numVendorWareHouse,X.numShipmentMethod,X.numSOVendorId,X.numProjectID,X.numProjectStageID,
				0,
				X.numToWarehouseItemID,X.Attributes,(SELECT ISNULL(monAverageCost,0) FROM Item I WHERE I.numDomainId=@numDomainId AND I.numItemCode=X.numItemCode),
				X.bitItemPriceApprovalRequired 
			FROM
				(
					SELECT 
						*
					FROM 
						OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
					WITH                       
					(                                                                          
						numoppitemtCode NUMERIC(9),numItemCode NUMERIC(9), numUnitHour FLOAT, monPrice DECIMAL(30,16), monTotAmount MONEY,vcItemDesc VARCHAR(1000),                                    
						numWarehouseItmsID NUMERIC(9), ItemType VARCHAR(30), DropShip BIT, bitDiscountType BIT,	fltDiscount DECIMAL(30,16), monTotAmtBefDiscount MONEY,
						vcItemName VARCHAR(300),numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId NUMERIC(9),
						numProjectID numeric(9),numProjectStageID numeric(9),numToWarehouseItemID NUMERIC(9),Attributes varchar(200),vcSKU VARCHAR(100),bitItemPriceApprovalRequired BIT
					)
				)X    
			ORDER BY 
				X.numoppitemtCode
    
			UPDATE 
				OpportunityItems                       
			SET 
				numItemCode=X.numItemCode,numOppId=@numOppID,numUnitHour=dbo.fn_UOMConversion(X.numUOM,X.numItemCode,@numDomainId,null) * X.numUnitHour,                      
				monPrice=x.monPrice,monTotAmount=x.monTotAmount,vcItemDesc=X.vcItemDesc,numWarehouseItmsID=X.numWarehouseItmsID,bitDropShip=X.DropShip,
				bitDiscountType=X.bitDiscountType,fltDiscount=X.fltDiscount,monTotAmtBefDiscount=X.monTotAmtBefDiscount,vcItemName=X.vcItemName,numUOMId=numUOM,
				bitWorkOrder=X.bitWorkOrder,numVendorWareHouse=X.numVendorWareHouse,numShipmentMethod=X.numShipmentMethod,numSOVendorId=X.numSOVendorId,
				numProjectId=X.numProjectID,numProjectStageId=X.numProjectStageID,numToWarehouseItemID = X.numToWarehouseItemID,vcAttributes=X.Attributes,
				numClassID=0,
				bitItemPriceApprovalRequired = X.bitItemPriceApprovalRequired
			FROM 
			(
				SELECT 
					numoppitemtCode as numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,DropShip,bitDiscountType,fltDiscount,
					monTotAmtBefDiscount,vcItemName,numUOM,bitWorkOrder,numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,numToWarehouseItemID,
					Attributes,bitItemPriceApprovalRequired
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=2]',2)
				WITH  
					(                      
						numoppitemtCode NUMERIC(9),numItemCode NUMERIC(9),numUnitHour FLOAT,monPrice DECIMAL(30,16),monTotAmount MONEY,vcItemDesc VARCHAR(1000),                                    
						numWarehouseItmsID NUMERIC(9),DropShip BIT,bitDiscountType BIT,fltDiscount DECIMAL(30,16),monTotAmtBefDiscount MONEY,vcItemName VARCHAR(300),
						numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId NUMERIC(9),numProjectID NUMERIC(9),
						numProjectStageID numeric(9),numToWarehouseItemID NUMERIC(9),Attributes varchar(200),bitItemPriceApprovalRequired BIT
					)
			)X 
			WHERE 
				numoppitemtCode=X.numOppItemID   
				
			--INSERT KIT ITEMS                 
			INSERT INTO OpportunityKitItems
			(
				numOppId,
				numOppItemID,
				numChildItemID,
				numWareHouseItemId,
				numQtyItemsReq,
				numQtyItemsReq_Orig,
				numUOMId,
				numQtyShipped
			)
			SELECT
				@numOppId,
				OI.numoppitemtCode,
				ID.numChildItemID,
				ID.numWareHouseItemId,
				(ID.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(ID.numUOMId,ID.numChildItemID,@numDomainID,I.numBaseUnit),1)) * OI.numUnitHour,
				ID.numQtyItemsReq,
				ID.numUOMId,
				0
			FROM 
				OpportunityItems OI 
			JOIN 
				ItemDetails ID 
			ON 
				OI.numItemCode=ID.numItemKitID 
			JOIN
				Item I
			ON
				ID.numChildItemID = I.numItemCode
			WHERE 
				OI.numOppId=@numOppId 

			IF (SELECT COUNT(*) FROM OPENXML (@hDocItem,'/NewDataSet/Item[bitHasKitAsChild=''true'']',2)) > 0
			BEGIN
				--INSERT KIT ITEM HAS ANOTHER KIT AS CHILD THEN SAVE CONFIGURATION OF KIT
				DECLARE @TempKitConfiguration TABLE
				(
					numItemCode NUMERIC(18,0),
					numWarehouseItemID NUMERIC(18,0),
					KitChildItems VARCHAR(MAX)
				)

				INSERT INTO 
					@TempKitConfiguration
				SELECT
					numItemCode,
					numWarehouseItmsID,
					KitChildItems
				FROM
					OPENXML (@hDocItem,'/NewDataSet/Item[bitHasKitAsChild=''true'']',2)
				WITH                       
				(
					numItemCode NUMERIC(9),numWarehouseItmsID NUMERIC(9), KitChildItems VARCHAR(MAX)
				) 

				INSERT INTO OpportunityKitChildItems
				(
					numOppID,
					numOppItemID,
					numOppChildItemID,
					numItemID,
					numWareHouseItemId,
					numQtyItemsReq,
					numQtyItemsReq_Orig,
					numUOMId,
					numQtyShipped
				)
				SELECT 
					@numOppID,
					OKI.numOppItemID,
					OKI.numOppChildItemID,
					ItemDetails.numChildItemID,
					ItemDetails.numWareHouseItemId,
					(ItemDetails.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(ItemDetails.numUOMId,ItemDetails.numChildItemID,@numDomainID,Item.numBaseUnit),1)) * OKI.numQtyItemsReq,
					ItemDetails.numQtyItemsReq,
					ItemDetails.numUOMId,
					0
				FROM
					(
						SELECT 
							t1.numItemCode,
							t1.numWarehouseItemID,
							SUBSTRING(
										SUBSTRING(items,0,CHARINDEX('-',items)),
										0,
										CHARINDEX('#',SUBSTRING(items,0,CHARINDEX('-',items)))
									  ) AS numKitItemID,
							SUBSTRING(
										SUBSTRING(items,0,CHARINDEX('-',items)),
										CHARINDEX('#',SUBSTRING(items,0,CHARINDEX('-',items))) + 1, 
										LEN(SUBSTRING(items,0,CHARINDEX('-',items))) - CHARINDEX('#',SUBSTRING(items,0,CHARINDEX('-',items)))
									  ) AS numKitWarehouseItemID,
							SUBSTRING(
										SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)), 
										0, 
										CHARINDEX('#',SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)))
									  ) As numKitChildItemID,
							SUBSTRING(
										SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)), 
										CHARINDEX('#',SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items))) + 1, 
										LEN(SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items))) - CHARINDEX('#',SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)))
									  ) As numKitChildItemWarehouseItemID
						FROM 
							@TempKitConfiguration t1
						CROSS APPLY
						(
							SELECT
								*
							FROM 
								dbo.Split((SELECT KitChildItems FROM @TempKitConfiguration WHERE numItemCode=t1.numItemCode AND numWarehouseItemID=t1.numWarehouseItemID),',') 
						) as t2
					) TempChildItems
				INNER JOIN
					OpportunityKitItems OKI
				ON
					OKI.numOppId=@numOppId
					AND OKI.numChildItemID=TempChildItems.numKitItemID
					AND OKI.numWarehouseItemId=TempChildItems.numKitWarehouseItemID
				INNER JOIN
					OpportunityItems OI
				ON
					OI.numItemCode = TempChildItems.numItemCode
					AND OI.numWarehouseItmsID = TempChildItems.numWarehouseItemID
					AND OI.numoppitemtcode = OKI.numOppItemID
				INNER JOIN
					ItemDetails
				ON
					TempChildItems.numKitItemID = ItemDetails.numItemKitID
					AND TempChildItems.numKitChildItemID = ItemDetails.numChildItemID
					AND (TempChildItems.numKitChildItemWarehouseItemID = ItemDetails.numWareHouseItemId  OR ISNULL(TempChildItems.numKitChildItemWarehouseItemID,0) = 0)
				INNER JOIN
					Item 
				ON
					ItemDetails.numChildItemID = Item.numItemCode
			END

			-- UPDATE UOM OF OPPORTUNITY ITEMS HAVE NOT UOM SET WHILE EXPORTED FROM MARKETPLACE (MEANS TINTSOURCETYPE = 3)
			IF ISNULL(@tintSourceType,0) = 3
			BEGIN
				UPDATE 
					OI 
				SET 
					numUOMID = ISNULL(I.numSaleUnit,0), 
					numUnitHour = dbo.fn_UOMConversion(ISNULL(I.numSaleUnit,0),OI.numItemCode,@numDomainId,null) * OI.numUnitHour
				FROM 
					OpportunityItems OI
				JOIN 
					Item I 
				ON 
					OI.numItemCode = I.numItemCode
				WHERE 
					numOppId = @numOppID
					AND I.numDomainID = @numDomainId
			END	
                                     
			INSERT INTO OppWarehouseSerializedItem
			(
				numOppID,
				numWarehouseItmsDTLID,
				numOppItemID,
				numWarehouseItmsID
			)                      
			SELECT 
				@numOppID,
				X.numWarehouseItmsDTLID,
				X.numoppitemtCode,
				X.numWItmsID 
			FROM
			(                                                                          
				SELECT
					* 
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/SerialNo',2)                                                                          
				WITH                        
				(                                               
				numWarehouseItmsDTLID numeric(9),                      
				numoppitemtCode numeric(9),                      
				numWItmsID numeric(9)                                                     
				)
			)X                                    
                                   
			UPDATE 
				OppWarehouseSerializedItem                       
			SET 
				numOppItemID=X.numoppitemtCode                                
			FROM                       
			(
				SELECT 
					numWarehouseItmsDTLID AS DTLID,
					O.numoppitemtCode 
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/SerialNo',2)                                                                         
				WITH
				(
					numWarehouseItmsDTLID NUMERIC(9),numoppitemtCode NUMERIC(9),numWItmsID NUMERIC(9)
				)                                
				JOIN 
					OpportunityItems O 
				ON 
					O.numWarehouseItmsID=numWItmsID and O.numOppID=@numOppID
			)X                 
			WHERE 
				numOppID=@numOppID 
				AND numWarehouseItmsDTLID=X.DTLID      
    
			EXEC USP_ManageSOWorkOrder @numDomainId,@numOppID,@strItems,@numUserCntID
                     
			EXEC sp_xml_removedocument @hDocItem                                     
		END          
   
		SET @TotalAmount=0                                                           
		SELECT @TotalAmount = SUM(monTotAmount) FROM OpportunityItems WHERE numOppId=@numOppID                                                                          
  
		UPDATE OpportunityMaster SET monPamount=@TotalAmount WHERE numOppId=@numOppID                                                 
                      
		--INSERT TAX FOR DIVISION   
		IF @tintOppType=1 OR (@tintOppType=2 and (select isnull(bitPurchaseTaxCredit,0) from Domain where numDomainId=@numDomainId)=1)
		BEGIN
			INSERT INTO dbo.OpportunityMasterTaxItems 
			(
				numOppId,
				numTaxItemID,
				fltPercentage,
				tintTaxType
			) 
			SELECT 
				@numOppID,
				TI.numTaxItemID,
				TEMPTax.decTaxValue,
				TEMPTax.tintTaxType
			FROM 
				TaxItems TI 
			JOIN 
				DivisionTaxTypes DTT 
			ON 
				TI.numTaxItemID = DTT.numTaxItemID
			OUTER APPLY
			(
				SELECT
					decTaxValue,
					tintTaxType
				FROM
					dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,TI.numTaxItemID,@numOppId,0,NULL)
			) AS TEMPTax
			WHERE 
				DTT.numDivisionID=@numDivisionId 
				AND DTT.bitApplicable=1
			UNION 
			SELECT 
				@numOppID,
				0,
				TEMPTax.decTaxValue,
				TEMPTax.tintTaxType
			FROM 
				dbo.DivisionMaster 
			OUTER APPLY
			(
				SELECT
					decTaxValue,
					tintTaxType
				FROM
					dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,0,@numOppId,1,NULL)
			) AS TEMPTax
			WHERE 
				bitNoTax=0 
				AND numDivisionID=@numDivisionID
		END	
	END
	ELSE                                                                                                                          
	BEGIN
		DECLARE @tempAssignedTo AS NUMERIC(9) = NULL                                                    
		SELECT 
			@tintOppStatus = tintOppStatus,
			@tempAssignedTo=ISNULL(numAssignedTo,0) 
		FROM   
			OpportunityMaster 
		WHERE  
			numOppID = @numOppID

		IF @tempAssignedTo <> @numAssignedTo
		BEGIN
			IF (SELECT COUNT(*) FROM BizDocComission WHERE ISNULL(numOppID,0) = @numOppID AND ISNULL(bitCommisionPaid,0) = 1) > 0
			BEGIN
				RAISERROR('COMMISSION_PAID_AGAINST_BIZDOC',16,1)
			END
		END
 
		--REVERTING BACK THE WAREHOUSE ITEMS                  
		IF @tintOppStatus = 1 
		BEGIN               
			EXEC USP_RevertDetailsOpp @numOppID,0,@numUserCntID                  
		END                  

		IF LEN(ISNULL(@vcOppRefOrderNo,''))>0
		BEGIN
			UPDATE dbo.OpportunityBizDocs SET vcRefOrderNo=@vcOppRefOrderNo WHERE numOppId=@numOppID
		END
		
		IF EXISTS (SELECT 1 FROM dbo.OpportunityMaster WHERE numOppId = @numOppID AND tintOppType=1 AND isnull(tintOppStatus,0)=1 AND ISNULL(numStatus,0)!= @numStatus)
		BEGIN
			EXEC USP_ManageOpportunityAutomationQueue
					@numOppQueueID = 0, -- numeric(18, 0)
					@numDomainID = @numDomainID, -- numeric(18, 0)
					@numOppId = @numOppID, -- numeric(18, 0)
					@numOppBizDocsId = 0, -- numeric(18, 0)
					@numOrderStatus = @numStatus, -- numeric(18, 0)
					@numUserCntID = @numUserCntID, -- numeric(18, 0)
					@tintProcessStatus = 1, -- tinyint
					@tintMode = 1 -- TINYINT
		END 
						
		UPDATE   
			OpportunityMaster
		SET      
			vcPOppName = @vcPOppName,txtComments = @Comments,bitPublicFlag = @bitPublicFlag,numCampainID = @CampaignID,tintSource = @tintSource,tintSourceType=@tintSourceType,
			intPEstimatedCloseDate = @dtEstimatedCloseDate,numModifiedBy = @numUserCntID,bintModifiedDate = GETUTCDATE(),lngPConclAnalysis = @lngPConclAnalysis,monPAmount = @monPAmount,
			tintActive = @tintActive,numSalesOrPurType = @numSalesOrPurType,numStatus = @numStatus,tintOppStatus = @DealStatus,vcOppRefOrderNo=@vcOppRefOrderNo,
			bintOppToOrder=(CASE WHEN @tintOppStatus=0 AND @DealStatus=1 THEN GETUTCDATE() ELSE bintOppToOrder END),bitDiscountType=@bitDiscountType,fltDiscount=@fltDiscount,
			bitBillingTerms=@bitBillingTerms,intBillingDays=@intBillingDays,bitInterestType=@bitInterestType,fltInterest=@fltInterest,tintTaxOperator=@tintTaxOperator,
			numDiscountAcntType=@numDiscountAcntType,vcCouponCode = @vcCouponCode,numPercentageComplete =@numPercentageComplete,bitUseShippersAccountNo = @bitUseShippersAccountNo,
			bitUseMarkupShippingRate = @bitUseMarkupShippingRate,numMarkupShippingRate = @numMarkupShippingRate,intUsedShippingCompany = @intUsedShippingCompany
		WHERE
			numOppId = @numOppID   
	   
		-- UPDATING IF ORGANIZATION IS ASSIGNED TO SOMEONE                                                      
		IF ( @tempAssignedTo <> @numAssignedTo AND @numAssignedTo <> '0' ) 
		BEGIN
			UPDATE
				OpportunityMaster 
			SET	
				numAssignedTo = @numAssignedTo, 
				numAssignedBy = @numUserCntID 
			WHERE 
				numOppId = @numOppID                                                    
		END                                                     
		ELSE IF(@numAssignedTo = 0) 
		BEGIN
			UPDATE 
				OpportunityMaster 
			SET 
				numAssignedTo = 0, 
				numAssignedBy = 0 
			WHERE
				numOppId = @numOppID
		END  
			                                                                    
		-- UPDATING OPP ITEMS
		IF CONVERT(VARCHAR(10),@strItems) <> '' AND @numOppID > 0                                                                         
		BEGIN
			EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems                                      
			   
			--DELETE CHILD ITEMS OF KIT WITHIN KIT
			DELETE FROM 
				OpportunityKitChildItems 
			WHERE 
				numOppID=@numOppID AND 
				numOppItemID NOT IN (SELECT ISNULL(numoppitemtCode,0) FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) WITH (numoppitemtCode NUMERIC(9)))   

			--DELETE ITEMS
			DELETE FROM 
				OpportunityKitItems 
			WHERE 
				numOppID=@numOppID AND 
				numOppItemID NOT IN (SELECT ISNULL(numoppitemtCode,0) FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) WITH (numoppitemtCode NUMERIC(9)))                      
			                            
			DELETE FROM 
				OpportunityItems 
			WHERE 
				numOppID=@numOppID 
				AND numoppitemtCode NOT IN (SELECT numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) WITH (numoppitemtCode NUMERIC(9)))   
	          
			--INSERT NEW ADDED ITEMS                        
			INSERT INTO OpportunityItems
			(
				numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,
				[vcItemName],[vcModelID],[vcManufacturer],vcPathForTImage,monVendorCost,numUOMId,bitWorkOrder,numVendorWareHouse,numToWarehouseItemID,numShipmentMethod,
				numSOVendorId,numProjectID,numProjectStageID,vcAttributes,numClassID,monAvgCost,bitItemPriceApprovalRequired
			)
			SELECT 
				@numOppID,X.numItemCode,dbo.fn_UOMConversion(X.numUOM,numItemCode,@numDomainId,null) * x.numUnitHour,x.monPrice,x.monTotAmount,X.vcItemDesc,X.numWarehouseItmsID,
				X.ItemType,X.DropShip,X.bitDiscountType,X.fltDiscount,X.monTotAmtBefDiscount,X.vcItemName,(SELECT vcModelID FROM item WHERE numItemCode = X.numItemCode),
				(SELECT vcManufacturer FROM item WHERE numItemCode = X.numItemCode),
				(SELECT TOP 1 vcPathForTImage FROM dbo.ItemImages WHERE numItemCode = X.numItemCode AND BitDefault = 1 AND numDomainId = @numDomainId),
				(select isnull(VN.monCost,0) from Vendor VN, Item IT where IT.numItemCode=VN.numItemCode and  VN.numItemCode=X.numItemCode and  VN.numVendorID=IT.numVendorID),
				X.numUOM,X.bitWorkOrder,X.numVendorWareHouse,X.numToWarehouseItemID,X.numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,Attributes,
				0,
				(SELECT ISNULL(monAverageCost,0) FROM Item I WHERE I.numDomainId=@numDomainId AND I.numItemCode=X.numItemCode), X.bitItemPriceApprovalRequired 
			FROM
			(
				SELECT 
					*
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
				WITH  
				(                      
					numoppitemtCode numeric(9),numItemCode numeric(9),numUnitHour FLOAT,monPrice DECIMAL(30,16),monTotAmount money,vcItemDesc varchar(1000),                                    
					numWarehouseItmsID numeric(9),numToWarehouseItemID numeric(9),Op_Flag tinyint,ItemType varchar(30),DropShip bit,bitDiscountType bit,
					fltDiscount DECIMAL(30,16),monTotAmtBefDiscount MONEY,vcItemName VARCHAR(300),numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),
					numShipmentMethod NUMERIC(9),numSOVendorId numeric(9),numProjectID numeric(9),numProjectStageID numeric(9),Attributes varchar(200), 
					bitItemPriceApprovalRequired BIT
				)
			)X 
			
			-- UPDATE ITEMS
			UPDATE 
				OpportunityItems                       
			SET 
				numItemCode=X.numItemCode,numOppId=@numOppID,numUnitHour=dbo.fn_UOMConversion(X.numUOM,X.numItemCode,@numDomainId,null) * x.numUnitHour,                      
				monPrice=x.monPrice,monTotAmount=x.monTotAmount,vcItemDesc=X.vcItemDesc,numWarehouseItmsID=X.numWarehouseItmsID,numToWarehouseItemID = X.numToWarehouseItemID,             
				bitDropShip=X.DropShip,bitDiscountType=X.bitDiscountType,fltDiscount=X.fltDiscount,monTotAmtBefDiscount=X.monTotAmtBefDiscount,vcItemName=X.vcItemName,
				numUOMId=numUOM,numVendorWareHouse=X.numVendorWareHouse,numShipmentMethod=X.numShipmentMethod,numSOVendorId=X.numSOVendorId,numProjectId=X.numProjectID,
				numProjectStageId=X.numProjectStageID,vcAttributes=X.Attributes,bitItemPriceApprovalRequired=X.bitItemPriceApprovalRequired,bitWorkOrder=X.bitWorkOrder
			FROM 
			(
				SELECT 
					numoppitemtCode as numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,numToWarehouseItemID,DropShip,bitDiscountType,
					fltDiscount,monTotAmtBefDiscount,vcItemName,numUOM,numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,Attributes,
					bitItemPriceApprovalRequired,bitWorkOrder  
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=2]',2)                                                                
				WITH  
				(                      
					numoppitemtCode NUMERIC(9),numItemCode NUMERIC(9),numUnitHour FLOAT,monPrice DECIMAL(30,16),monTotAmount MONEY,vcItemDesc VARCHAR(1000),numWarehouseItmsID NUMERIC(9), 
					numToWarehouseItemID NUMERIC(18,0),DropShip BIT,bitDiscountType BIT,fltDiscount DECIMAL(30,16),monTotAmtBefDiscount MONEY,vcItemName VARCHAR(300),numUOM NUMERIC(9),
					numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId NUMERIC(9),numProjectID NUMERIC(9),numProjectStageID NUMERIC(9),Attributes VARCHAR(200),
					bitItemPriceApprovalRequired BIT, bitWorkOrder BIT                                              
				)
			)X 
			WHERE 
				numoppitemtCode=X.numOppItemID 

			--INSERT NEW ADDED KIT ITEMS                 
			INSERT INTO OpportunityKitItems                                                                          
			(
				numOppId,
				numOppItemID,
				numChildItemID,
				numWareHouseItemId,
				numQtyItemsReq,
				numQtyItemsReq_Orig,
				numUOMId,
				numQtyShipped
			)
			SELECT
				@numOppId,
				OI.numoppitemtCode,
				ID.numChildItemID,
				ID.numWareHouseItemId,
				ID.numQtyItemsReq * OI.numUnitHour,
				ID.numQtyItemsReq,
				ID.numUOMId,
				0
			FROM 
				OpportunityItems OI 
			JOIN 
				ItemDetails ID 
			ON 
				OI.numItemCode=ID.numItemKitID 
			WHERE 
				OI.numOppId=@numOppId 
				AND OI.numoppitemtCode NOT IN (SELECT ISNULL(numOppItemID,0) FROM OpportunityKitItems OKI WHERE OKI.numOppId=@numOppID)  
				
			IF (SELECT COUNT(*) FROM OPENXML (@hDocItem,'/NewDataSet/Item[bitHasKitAsChild=''true'']',2)) > 0
			BEGIN
				--INSERT KIT ITEM HAS ANOTHER KIT AS CHILD THEN SAVE CONFIGURATION OF KIT
				DELETE FROM @TempKitConfiguration

				INSERT INTO 
					@TempKitConfiguration
				SELECT
					numItemCode,
					numWarehouseItmsID,
					KitChildItems
				FROM
					OPENXML (@hDocItem,'/NewDataSet/Item[bitHasKitAsChild=''true'']',2)
				WITH                       
				(
					numItemCode NUMERIC(9),numWarehouseItmsID NUMERIC(9), KitChildItems VARCHAR(MAX)
				) 

				INSERT INTO OpportunityKitChildItems
				(
					numOppID,
					numOppItemID,
					numOppChildItemID,
					numItemID,
					numWareHouseItemId,
					numQtyItemsReq,
					numQtyItemsReq_Orig,
					numUOMId,
					numQtyShipped
				)
				SELECT 
					@numOppID,
					OKI.numOppItemID,
					OKI.numOppChildItemID,
					ItemDetails.numChildItemID,
					ItemDetails.numWareHouseItemId,
					ItemDetails.numQtyItemsReq * OKI.numQtyItemsReq,
					ItemDetails.numQtyItemsReq,
					ItemDetails.numUOMId,
					0
				FROM
					(
						SELECT 
							t1.numItemCode,
							t1.numWarehouseItemID,
							SUBSTRING(
										SUBSTRING(items,0,CHARINDEX('-',items)),
										0,
										CHARINDEX('#',SUBSTRING(items,0,CHARINDEX('-',items)))
									  ) AS numKitItemID,
							SUBSTRING(
										SUBSTRING(items,0,CHARINDEX('-',items)),
										CHARINDEX('#',SUBSTRING(items,0,CHARINDEX('-',items))) + 1, 
										LEN(SUBSTRING(items,0,CHARINDEX('-',items))) - CHARINDEX('#',SUBSTRING(items,0,CHARINDEX('-',items)))
									  ) AS numKitWarehouseItemID,
							SUBSTRING(
										SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)), 
										0, 
										CHARINDEX('#',SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)))
									  ) As numKitChildItemID,
							SUBSTRING(
										SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)), 
										CHARINDEX('#',SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items))) + 1, 
										LEN(SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items))) - CHARINDEX('#',SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)))
									  ) As numKitChildItemWarehouseItemID
						FROM 
							@TempKitConfiguration t1
						CROSS APPLY
						(
							SELECT
								*
							FROM 
								dbo.Split((SELECT KitChildItems FROM @TempKitConfiguration WHERE numItemCode=t1.numItemCode AND numWarehouseItemID=t1.numWarehouseItemID),',') 
						) as t2
					) TempChildItems
				INNER JOIN
					OpportunityKitItems OKI
				ON
					OKI.numOppId=@numOppId
					AND OKI.numChildItemID=TempChildItems.numKitItemID
					AND OKI.numWarehouseItemId=TempChildItems.numKitWarehouseItemID
				INNER JOIN
					OpportunityItems OI
				ON
					OI.numItemCode = TempChildItems.numItemCode
					AND OI.numWarehouseItmsID = TempChildItems.numWarehouseItemID
					AND OI.numoppitemtcode = OKI.numOppItemID
				INNER JOIN
					ItemDetails
				ON
					TempChildItems.numKitItemID = ItemDetails.numItemKitID
					AND TempChildItems.numKitChildItemID = ItemDetails.numChildItemID
					AND (TempChildItems.numKitChildItemWarehouseItemID = ItemDetails.numWareHouseItemId  OR ISNULL(TempChildItems.numKitChildItemWarehouseItemID,0) = 0)
			END                                  
			
			--UPDATE KIT ITEMS                 
			UPDATE 
				OKI 
			SET 
				numQtyItemsReq = numQtyItemsReq_Orig * OI.numUnitHour
			FROM 
				OpportunityKitItems OKI 
			JOIN 
				OpportunityItems OI 
			ON 
				OKI.numOppItemID=OI.numoppitemtCode 
				AND OKI.numOppId=OI.numOppId  
			WHERE 
				OI.numOppId=@numOppId 

			-- UPDATE CHILD ITEMS OF KIT WITHIN KIT
			UPDATE
				OKCI
			SET
				OKCI.numQtyItemsReq = (OKI.numQtyItemsReq * OKCI.numQtyItemsReq_Orig)
			FROM
				OpportunityKitChildItems OKCI
			INNER JOIN
				OpportunityKitItems OKI
			ON
				OKCI.numOppID = @numOppId
				AND OKCI.numOppItemID = OKI.numOppItemID
				AND OKCI.numOppChildItemID = OKI.numOppChildItemID                     
			                                    
			EXEC USP_ManageSOWorkOrder @numDomainId,@numOppID,@strItems,@numUserCntID
			   		                                                                      
			EXEC sp_xml_removedocument @hDocItem                                               
		END     

		--FOLLOWING DELETE STATEMENT IS COMMENTED BECUASE NOW WE ARE NOT ALLOWING USER TO EIDT SERIAL/LOT ITEM UNTILL USER REMOVED ASSIGNED SERIAL/LOT NUMBERS
	    --Delete previously added serial lot number added for opportunity
		--DELETE FROM OppWarehouseSerializedItem WHERE numOppID = @numOppID                                                         
	END


/*Added by chintan - While creating SO from PO it wasn't puting Item Qty to Allocation. this segment was being called from USP_OppSaveMilestone which No longer in use after changes anoop did. */     
--Updating the warehouse items              
declare @tintShipped as tinyint               
select @tintOppStatus=tintOppStatus,@tintOppType=tintOppType,@tintShipped=tintShipped from OpportunityMaster where numOppID=@numOppID              
if @tintOppStatus=1               
	begin         
	PRINT 'inside USP_UpdatingInventoryonCloseDeal'
		exec USP_UpdatingInventoryonCloseDeal @numOppID,@numUserCntID
	end              

declare @tintCRMType as numeric(9)        
declare @AccountClosingDate as datetime        

select @numDivisionID=numDivisionID,@AccountClosingDate=bintAccountClosingDate from  OpportunityMaster where numOppID=@numOppID         
select @tintCRMType=tintCRMType from divisionmaster where numDivisionID =@numDivisionID 

--When deal is won                                         
if @DealStatus = 1 
begin           
	 if @AccountClosingDate is null                   
	  update OpportunityMaster set bintAccountClosingDate=getutcdate() where numOppID=@numOppID              
end         
--When Deal is Lost
else if @DealStatus = 2
begin         
	 select @AccountClosingDate=bintAccountClosingDate from  OpportunityMaster where numOppID=@numOppID         
	 if @AccountClosingDate is null                   
	  update OpportunityMaster set bintAccountClosingDate=getutcdate() where numOppID=@numOppID
end                    

/*Added by chintan -  Biz usually automatically promotes the record from Lead/Prospect to Account When a Sales Opp/Order is made against it*/  
-- Promote Lead to Prospect when Sales/Purchase Opp is created against it
if @tintCRMType=0 AND @DealStatus = 0 --Lead & Open Opp
begin        
	update divisionmaster set tintCRMType=1,bintLeadProm=getutcdate(),bintLeadPromBy=@numUserCntID,bintProsProm=getutcdate(),bintProsPromBy=@numUserCntID        
	where numDivisionID=@numDivisionID        
end        
-- Promote Lead to Account when Sales/Purchase Order is created against it
ELSE if @tintCRMType=0 AND @DealStatus = 1 --Lead & Order
begin        
	update divisionmaster set tintCRMType=2,bintLeadProm=getutcdate(),bintLeadPromBy=@numUserCntID,bintProsProm=getutcdate(),bintProsPromBy=@numUserCntID        
	where numDivisionID=@numDivisionID        
end
--Promote Prospect to Account
else if @tintCRMType=1 AND @DealStatus = 1 
begin        
	update divisionmaster set tintCRMType=2,bintProsProm=getutcdate(),bintProsPromBy=@numUserCntID        
	where numDivisionID=@numDivisionID        
end

--Add/Update Address Details
DECLARE @vcStreet  varchar(100),@vcCity  varchar(50),@vcPostalCode  varchar(15),@vcCompanyName  varchar(100),@vcAddressName VARCHAR(50)      
DECLARE @numState  numeric(9),@numCountry  numeric(9), @numCompanyId  numeric(9) 
DECLARE @bitIsPrimary BIT;

select  @vcCompanyName=vcCompanyName,@numCompanyId=div.numCompanyID from CompanyInfo Com                            
join divisionMaster Div                            
on div.numCompanyID=com.numCompanyID                            
where div.numdivisionID=@numDivisionId

--Bill Address
IF @numBillAddressId>0
BEGIN
 SELECT  @vcStreet=ISNULL(vcStreet,''),@vcCity=ISNULL(vcCity,''),@vcPostalCode=ISNULL(vcPostalCode,''),
		@numState=ISNULL(numState,0),@numCountry=ISNULL(numCountry,0),@bitIsPrimary=bitIsPrimary,
		@vcAddressName=vcAddressName
            FROM  dbo.AddressDetails WHERE   numDomainID = @numDomainID AND numAddressID = @numBillAddressId
  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 0, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = @vcCompanyName, --  varchar(100)
	@numCompanyId = 0, --  numeric(9, 0)
	@vcAddressName = @vcAddressName
END
  
  
  --Ship Address
IF @numShipAddressId>0
BEGIN
	SELECT  @vcStreet=ISNULL(vcStreet,''),@vcCity=ISNULL(vcCity,''),@vcPostalCode=ISNULL(vcPostalCode,''),
		@numState=ISNULL(numState,0),@numCountry=ISNULL(numCountry,0),@bitIsPrimary=bitIsPrimary,
		@vcAddressName=vcAddressName
            FROM  dbo.AddressDetails WHERE   numDomainID = @numDomainID AND numAddressID = @numShipAddressId
 



  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 1, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = @vcCompanyName, --  varchar(100)
	@numCompanyId =@numCompanyId, --  numeric(9, 0)
	@vcAddressName = @vcAddressName
END

--Delete Tax for Opportunity Items if item deleted 
DELETE FROM OpportunityItemsTaxItems WHERE numOppId=@numOppId AND numOppItemID NOT IN (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID)

--Insert Tax for Opportunity Items
INSERT INTO dbo.OpportunityItemsTaxItems (
	numOppId,
	numOppItemID,
	numTaxItemID
) SELECT @numOppId,OI.numoppitemtCode,TI.numTaxItemID FROM dbo.OpportunityItems OI JOIN dbo.ItemTax IT ON OI.numItemCode=IT.numItemCode JOIN
TaxItems TI ON TI.numTaxItemID=IT.numTaxItemID WHERE OI.numOppId=@numOppID AND IT.bitApplicable=1 AND 
OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
UNION
  select @numOppId,OI.numoppitemtCode,0 FROM dbo.OpportunityItems OI JOIN dbo.Item I ON OI.numItemCode=I.numItemCode
   WHERE OI.numOppId=@numOppID  AND I.bitTaxable=1 AND
OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)

  
 UPDATE OpportunityMaster SET monDealAmount=dbo.GetDealAmount(@numOppID,getdate(),0) WHERE numOppId=@numOppID
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SET @numOppID = 0

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
GO
/****** Object:  StoredProcedure [dbo].[USP_OppManage_API]    Script Date: 06/01/2009 23:49:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OppManage_API')
DROP PROCEDURE USP_OppManage_API
GO
CREATE PROCEDURE [dbo].[USP_OppManage_API]
(                                                                          
  @numOppID as numeric=0 OUTPUT,                                                                          
  @numContactId numeric(9)=null,                                                                          
  @numDivisionId numeric(9)=null,                                                                          
  @tintSource numeric(9)=null,                                                                          
  @vcPOppName Varchar(100)='' OUTPUT,                                                                          
  @Comments varchar(1000)='',                                                                          
  @bitPublicFlag bit=0,                                                                          
  @numUserCntID numeric(9)=0,                                                                                 
  @monPAmount money =0,                                                 
  @numAssignedTo as numeric(9)=0,                                                                                                        
  @numDomainId numeric(9)=0,                                                                                                                                                                                   
  @strItems as text=null,                                                                          
  @strMilestone as text=null,                                                                          
  @dtEstimatedCloseDate datetime,                                                                          
  @CampaignID as numeric(9)=null,                                                                          
  @lngPConclAnalysis as numeric(9)=null,                                                                         
  @tintOppType as tinyint,                                                                                                                                             
  @tintActive as tinyint,                                                              
  @numSalesOrPurType as numeric(9),              
  @numRecurringId as numeric(9),
  @numCurrencyID as numeric(9)=0,
  @vcSource VARCHAR(50)=NULL                                                                   
)                                                                          
                                                                          
as  
 Declare @fltExchangeRate float                                 
 DECLARE @hDocItem int                                                                                                                            
 declare @TotalAmount as money  
 DECLARE @WebApiId AS int                                                                        
 if @numOppID = 0                                                                          
 begin                                                                          
  declare @intOppTcode as numeric(9)                           
  Select @intOppTcode=max(numOppId)from OpportunityMaster                
  set @intOppTcode =isnull(@intOppTcode,0) + 1                                                
  set @vcPOppName=@vcPOppName+ '-'+convert(varchar(4),year(getutcdate()))  +'-A'+ convert(varchar(10),@intOppTcode)                                  
  
  if @numCurrencyID=0
  BEGIN
  	 set   @fltExchangeRate=1
  	 SELECT @numCurrencyID = CASE [bitMultiCurrency] WHEN 1 THEN numCurrencyID ELSE 0 END  FROM [Domain] WHERE [numDomainId] = @numDomainID 
  END
  else set @fltExchangeRate=dbo.GetExchangeRate(@numDomainID,@numCurrencyID)

  insert into OpportunityMaster                                                                 
  (                                                                             
  numContactId,                                                                   
  numDivisionId,                                                                          
  txtComments,                                             
  numCampainID,                                                                          
  bitPublicFlag,                                                                          
  tintSource,                                                             
  vcPOppName,                                                                          
  intPEstimatedCloseDate,                                                                          
  monPAmount,                                                                           
  numCreatedBy,                                                                          
  bintCreatedDate,                                                                           
  numModifiedBy,                                                                          
  bintModifiedDate,                                                                          
  numDomainId,                                                                                                             
  numRecOwner,                                                                          
  lngPConclAnalysis,                                                                         
  tintOppType,                                                              
  numSalesOrPurType,              
  numCurrencyID,
  fltExchangeRate                                                                   
  )                                                                          
  Values                                                                          
  (                                                                          
  @numContactId,                                                                          
  @numDivisionId,                                                                          
  @Comments,                           
  @CampaignID,                                                                          
  @bitPublicFlag,                     
  @tintSource,                         
  @vcPOppName,                                                                          
  @dtEstimatedCloseDate,                                                                          
  @monPAmount,                                                                                
  @numUserCntID,                                                                          
  getutcdate(),                                                                          
  @numUserCntID,                                                           
  getutcdate(),                                                                          
  @numDomainId,                                                                                   
  @numUserCntID,                                                                          
  @lngPConclAnalysis,                                                                                                                                                                      
  @tintOppType,                                                                                                                                      
  @numSalesOrPurType,              
  @numCurrencyID, 
  @fltExchangeRate                                                                 
  )                                                                                                                      
  set @numOppID=scope_identity()  
  ---- insert recurring id 
  if (@numRecurringId >0)
  BEGIN
  	EXEC [dbo].[USP_RecurringAddOpportunity] @numOppID, @numRecurringId , null, 0, null,null
  END                                              
  ---- inserting Items                                                                           
                                                         
  if convert(varchar(10),@strItems) <>''                                                                          
  begin                      
   EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems                       
      insert into                       
   OpportunityItems                                                                          
   (numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,bitDropShip,[vcItemName],[vcModelID],[vcManufacturer],vcPathForTImage,monVendorCost)                      
   select 
   @numOppID,
   X.numItemCode AS numItemCode,
   x.numUnitHour,x.monPrice,x.monTotAmount,X.vcItemDesc,
   CASE X.numWarehouseItmsID WHEN -1 THEN (SELECT [numWareHouseItemID] FROM [WareHouseItems] WHERE [numItemID] = X.numItemCode AND numWareHouseID IN (SELECT numDefaultWareHouseID FROM [eCommerceDTL] WHERE numDomainID =@numDomainId))  WHEN 0 THEN (SELECT TOP 1 [numWareHouseItemID] FROM [WareHouseItems] WHERE [numItemID] = X.numItemCode) ELSE  X.numWarehouseItmsID END AS numWarehouseItmsID,
   X.ItemType,X.DropShip,(SELECT vcItemName FROM item WHERE numItemCode = X.numItemCode),(SELECT vcModelID FROM item WHERE numItemCode = X.numItemCode),(SELECT vcManufacturer FROM item WHERE numItemCode = X.numItemCode),(SELECT TOP 1 vcPathForTImage FROM dbo.ItemImages II WHERE II.numItemCode = X.numItemCode AND II.numDomainId= @numDomainID AND II.bitDefault=1),
   (select isnull(VN.monCost,0) from Vendor VN, Item IT where IT.numItemCode=VN.numItemCode and  VN.numItemCode=X.numItemCode and  VN.numVendorID=IT.numVendorID)
    from(                                                                          
   SELECT *FROM OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
   WITH                       
   (                                                                          
   numoppitemtCode numeric(9),                                     
   numItemCode numeric(9),                                                                          
   numUnitHour FLOAT,                                                                          
   monPrice money,                                                                       
   monTotAmount money,                                                                          
   vcItemDesc varchar(1000),                                    
   numWarehouseItmsID numeric(9),                          
   ItemType varchar(30),      
   DropShip BIT,
   WebApiId int
   ))X    
    
 SELECT TOP 1 @WebApiId=WebApiId FROM OPENXML (@hDocItem,'/NewDataSet/Item',2)                                                        
 WITH  
 (                                                        
  WebApiId int                                
 )
		IF(@WebApiId =1)  
		BEGIN
			UPDATE [WebAPIDetail] SET [vcNinthFldValue] = GETUTCDATE() WHERE [numDomainId]=@numDomainID AND [WebApiId]=@WebApiId
		END
		IF(@vcSource IS NOT NULL)
		BEGIN
			insert into OpportunityLinking(	[numParentOppID],[numChildOppID],[vcSource],[numParentOppBizDocID]) values(null,@numOppID,@vcSource,null);
		END
	
    
   Update OpportunityItems                       
   set numItemCode=X.numItemCode,    
   numOppId=@numOppID,                     
   numUnitHour=x.numUnitHour,                      
   monPrice=x.monPrice,                      
   monTotAmount=x.monTotAmount,                                  
   vcItemDesc=X.vcItemDesc,                      
   numWarehouseItmsID=X.numWarehouseItmsID,                 
   bitDropShip=X.DropShip                 
   from (SELECT numoppitemtCode as numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,DropShip  FROM OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=2]',2)                                                                
  
    
   WITH  (                      
    numoppitemtCode numeric(9),                                     
    numItemCode numeric(9),                                                                          
    numUnitHour FLOAT,                                                                          
    monPrice money,                                         
    monTotAmount money,                                                                          
    vcItemDesc varchar(1000),                                    
    numWarehouseItmsID numeric(9),      
    DropShip bit                                                      
   ))X where numoppitemtCode=X.numOppItemID                                          
                                     
   insert into OppWarehouseSerializedItem                                                                          
   (numOppID,numWarehouseItmsDTLID,numOppItemID,numWarehouseItmsID)                      
   select @numOppID,X.numWarehouseItmsDTLID,X.numoppitemtCode,X.numWItmsID from(                                                                          
   SELECT * FROM OPENXML (@hDocItem,'/NewDataSet/SerialNo',2)                                                                          
   WITH                        
   (                                               
   numWarehouseItmsDTLID numeric(9),                      
   numoppitemtCode numeric(9),                      
   numWItmsID numeric(9)                                                     
   ))X                                    
               
                                 
   update OppWarehouseSerializedItem                       
   set numOppItemID=X.numoppitemtCode                                
   from                       
   (SELECT numWarehouseItmsDTLID as DTLID,O.numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/SerialNo',2)                                                                         
   WITH(numWarehouseItmsDTLID numeric(9),numoppitemtCode numeric(9),numWItmsID numeric(9))                                
   join OpportunityItems O on O.numWarehouseItmsID=numWItmsID and O.numOppID=@numOppID)X                 
   where numOppID=@numOppID and numWarehouseItmsDTLID=X.DTLID      
    
--   insert into OpportunityKitItems(numOppKitItem,numChildItem,intQuantity,numWareHouseItemId)    
--   SELECT * FROM OPENXML (@hDocItem,'/NewDataSet/ChildItems',2)                                                                          
--   WITH                        
--   (                                               
--   numoppitemtCode numeric(9),                      
--   numItemCode numeric(9),    
--   numQtyItemsReq integer,  
--   numWarehouseItmsID numeric(9)                                                       
--   )    
      
    
    
    
    
                              
                          
   EXEC sp_xml_removedocument @hDocItem                                     
        end          
   set @TotalAmount=0                                                           
   select @TotalAmount=sum(monTotAmount)from OpportunityItems where numOppId=@numOppID                                                                          
  update OpportunityMaster set  monPamount=@TotalAmount                                                          
  where numOppId=@numOppID                                                 
                       
                
                                                                   
  insert into OpportunityStageDetails                                                  
  (                                                                    
  numOppId,                                                                    
  numstagepercentage,                                                                    
  vcStageDetail,                                                                    
  vcComments,                            
  numDomainId,                                                                    
  numCreatedBy,                                                                    
  bintCreatedDate,                                                                    
  numModifiedBy,                                                                    
  bintModifiedDate,                                                                    
  numAssignTo,                                                                    
  bintStageComDate,                                                                    
  bintDueDate,                                                                    
  bitAlert,                                                                    
  bitStageCompleted                                                                    
  )                                                                          
  values                      
  (                      
  @numOppID,                                                                    
  100,                      
  'Deal Closed',                      
  '',                                                                    
  @numDomainId,                                                                    
  @numUserCntID,                                                                    
  getutcdate(),                                   
  @numUserCntID,                                                                    
  getutcdate(),                                                                    
  0,                                                                    
  0,                                                                    
  0,                                                                    
  0,                                                                    
  0                                                                    
  )                                                                     
  insert into OpportunityStageDetails                                  
  (                                                             
  numOppId,                                                                    
  numstagepercentage,                                                                    
  vcStageDetail,                                                                    
  vcComments,                                                                
  numDomainId,                                                                    
  numCreatedBy,                                                                    
  bintCreatedDate,                                                                    
  numModifiedBy,                                                            
  bintModifiedDate,                                                                    
  numAssignTo,                                                                  
  bintStageComDate,                                                                    
  bintDueDate,                                                                    
  bitAlert,                                                                    
  bitStageCompleted                                                                    
  )                                 
  values                      
  (                  
  @numOppID,                                                                    
  0,                                                                    
  'Deal Lost',                                 
  '',                                                                    
 @numDomainId,                                                                    
  @numUserCntID,                                                                    
  getutcdate(),                                                                    
  @numUserCntID,                                                                    
  getutcdate(),                        
   0,                                                                    
   0,                                                                
  0,                                                                    
  0,                                                                    
  0                                                                    
  )                                                                                                     
 end                       
 else                                                                                                                          
 begin                  
  --Reverting back the warehouse items                  
  declare @tintOppStatus as tinyint                   
  select @tintOppStatus=tintOppStatus from OpportunityMaster where numOppID=@numOppID                  
  if @tintOppStatus=1                   
        begin                  
   exec USP_RevertDetailsOpp @numOppID                  
  end                  
                     
  update OpportunityMaster                      
  set                       
  vcPOppName=@vcPOppName,                                              
  txtComments=@Comments,                                                           
  bitPublicFlag=@bitPublicFlag,                                                                          
  numCampainID=@CampaignID,                                                                          
  tintSource=@tintSource,                                                                          
  intPEstimatedCloseDate=@dtEstimatedCloseDate,                                                                           
  numModifiedBy=@numUserCntID,                                                          
   bintModifiedDate=getutcdate(),                                                                                                                            
  lngPConclAnalysis=@lngPConclAnalysis,          
  monPAmount=@monPAmount,                                                                                                                                                                                           
  tintActive=@tintActive,                                                              
  numSalesOrPurType=@numSalesOrPurType              
  where numOppId=@numOppID               
        ---Updating if organization is assigned to someone                                                      
  declare @tempAssignedTo as numeric(9)                                                    
  set @tempAssignedTo=null                                      
  select @tempAssignedTo=isnull(numAssignedTo,0) from OpportunityMaster where numOppId=@numOppID                                                                          
  if (@tempAssignedTo<>@numAssignedTo and  @numAssignedTo<>'0')                                                    
  begin                                   
   update OpportunityMaster set numAssignedTo=@numAssignedTo ,numAssignedBy=@numUserCntID where numOppId=@numOppID                                                    
  end                                                     
  else if  (@numAssignedTo =0)                                                    
  begin                
   update OpportunityMaster set numAssignedTo=0 ,numAssignedBy=0 where numOppId = @numOppID                                                    
  end       
  ---- update recurring id 
  if (@numRecurringId >0)
  BEGIN
  	EXEC [dbo].[USP_RecurringAddOpportunity] @numOppID, @numRecurringId , null, 0, null,null
  END                                                               
  ---- Updating Items                                                                     
  if convert(varchar(10),@strItems) <>''                                                                          
  begin                                                                          
   EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems                                      
   Delete From OppWarehouseSerializedItem WHERE numOppID=@numOppID                                           
 delete from OpportunityItems where numOppID=@numOppID and numoppitemtCode not in                       
   (SELECT numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/Item',2)                                                                          
   WITH  (numoppitemtCode numeric(9)))                                   
   insert into OpportunityItems                                                                          
   (numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,bitDropShip)                                                                          
   select 
	   @numOppID,
	   X.numItemCode AS numItemCode,
	   x.numUnitHour,x.monPrice,x.monTotAmount,X.vcItemDesc,
	   CASE X.numWarehouseItmsID WHEN 0 THEN (SELECT TOP 1 [numWareHouseItemID] FROM [WareHouseItems] WHERE [numItemID] = X.numItemCode) ELSE  X.numWarehouseItmsID END AS numWarehouseItmsID,
	   X.ItemType,X.DropShip from(                                                                          
   SELECT *FROM OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
   WITH                       
   (                                                                          
   numoppitemtCode numeric(9),                                     
   numItemCode numeric(9),                                                                          
   numUnitHour FLOAT,                                                                          
   monPrice money,                                                                       
   monTotAmount money,                                                                          
   vcItemDesc varchar(1000),                                    
   numWarehouseItmsID numeric(9),                          
   ItemType varchar(30),      
   DropShip BIT,
   WebApiId int
   ))X  
--   select @numOppID,X.numItemCode,x.numUnitHour,x.monPrice,x.monTotAmount,X.vcItemDesc,X.numWarehouseItmsID,X.ItemType,X.DropShip from(                                                                          
--   SELECT *FROM OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
--   WITH  (                      
--    numoppitemtCode numeric(9),                                     
--    numItemCode numeric(9),                                                                          
--    numUnitHour numeric(9),                                                                          
--    monPrice money,                                                                       
--    monTotAmount money,                                                                          
--    vcItemDesc varchar(1000),                                    
--    numWarehouseItmsID numeric(9),                              
--    Op_Flag tinyint,                            
--    ItemType varchar(30),      
-- DropShip bit                  
--     ))X                                     
                                  
   Update OpportunityItems                       
   set numItemCode=X.numItemCode,    
   numOppId=@numOppID,                       
   numUnitHour=x.numUnitHour,                      
   monPrice=x.monPrice,                      
   monTotAmount=x.monTotAmount,                                  
   vcItemDesc=X.vcItemDesc,                      
   numWarehouseItmsID=X.numWarehouseItmsID,                 
   bitDropShip=X.DropShip                 
   from (SELECT numoppitemtCode as numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,DropShip  FROM OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=2]',2)                                                                
  
    
      
              
                
   WITH  (                      
    numoppitemtCode numeric(9),                                     
    numItemCode numeric(9),                                                                          
    numUnitHour FLOAT,                                                                          
    monPrice money,                                         
    monTotAmount money,                                                                          
    vcItemDesc varchar(1000),                                    
    numWarehouseItmsID numeric(9),      
    DropShip bit                                                      
   ))X where numoppitemtCode=X.numOppItemID                                  
                                    
         
-- delete from OpportunityKitItems where numOppKitItem in (select numoppitemtCode from OpportunityItems where numOppId=@numOppID)     
-- and numOppChildItemID not in                       
-- (SELECT numOppChildItemID FROM OPENXML (@hDocItem,'/NewDataSet/ChildItems',2)                                                                          
-- WITH  (numOppChildItemID numeric(9)))    
                             
   insert into OppWarehouseSerializedItem                                                                          
   (numOppID,numWarehouseItmsDTLID,numOppItemID,numWarehouseItmsID)                                                                   
   select @numOppID,X.numWarehouseItmsDTLID,X.numoppitemtCode,X.numWItmsID from(                                                                          
   SELECT * FROM OPENXML (@hDocItem,'/NewDataSet/SerialNo',2)                                                                          
   WITH  (                                                                          
   numWarehouseItmsDTLID numeric(9),numoppitemtCode numeric(9),numWItmsID numeric(9)                                                     
   ))X                                    
                     
   update OppWarehouseSerializedItem set numOppItemID=X.numoppitemtCode                                
   from (SELECT numWarehouseItmsDTLID as DTLID,O.numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/SerialNo',2)                                                                         
   WITH(numWarehouseItmsDTLID numeric(9),numoppitemtCode numeric(9),numWItmsID numeric(9))                                
   join OpportunityItems O on O.numWarehouseItmsID=numWItmsID and O.numOppID=@numOppID)X                                
   where numOppID=@numOppID and numWarehouseItmsDTLID=X.DTLID       
    
--   insert into OpportunityKitItems(numOppKitItem,numChildItem,intQuantity,numWareHouseItemId)    
--   SELECT * FROM OPENXML (@hDocItem,'/NewDataSet/ChildItems[Op_Flag=1]',2)                                                                          
--   WITH                        
--   (                                               
--   numoppitemtCode numeric(9),                      
--   numItemCode numeric(9),    
--   numQtyItemsReq integer,  
--   numWarehouseItmsID numeric(9)                                                       
--   )    
    
--   update OpportunityKitItems set numOppKitItem=X.numoppitemtCode,numWareHouseItemId=X.numWarehouseItmsID,numChildItem=X.numItemCode, intQuantity=X.numQtyItemsReq                               
--   from (SELECT numoppitemtCode,numItemCode,numQtyItemsReq,numOppChildItemID as OppChildItemID,numWarehouseItmsID FROM OPENXML (@hDocItem,'/NewDataSet/ChildItems[Op_Flag=2]',2)                                                                         
--   WITH(numOppChildItemID numeric(9),numoppitemtCode numeric(9),numItemCode numeric(9),numQtyItemsReq numeric(9),numWarehouseItmsID numeric(9)))X                                
--   where numOppChildItemID=X.OppChildItemID     
       
                                                                      
   EXEC sp_xml_removedocument @hDocItem                                               
end                                                                  
                                                                                                                               
                                             
 end

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityBizDocs_AutoFulFill')
DROP PROCEDURE USP_OpportunityBizDocs_AutoFulFill
GO
CREATE PROCEDURE [dbo].[USP_OpportunityBizDocs_AutoFulFill] 
	@numDomainID NUMERIC(18,0),
	@numUserCntID NUMERIC(18,0),
	@numOppId NUMERIC(18,0),
	@numOppBizDocID NUMERIC(18,0)
AS  
BEGIN 
BEGIN TRY
	DECLARE @vcItemType AS CHAR(1)
	DECLARE @bitDropShip AS BIT
	DECLARE @numOppItemID AS INT
	DECLARE @numWarehouseItemID AS INT
	DECLARE @numQty AS FLOAT
	DECLARE @numQtyShipped AS FLOAT
	DECLARE @numXmlQty AS FLOAT
	DECLARE @vcSerialLot AS VARCHAR(MAX)
	DECLARE @numSerialCount AS INT
	DECLARE @bitSerial AS BIT
	DECLARE @bitLot AS BIT
	DECLARE @bitAssembly AS BIT
	DECLARE @bitKit AS BIT

	DECLARE @i AS INT = 1
	DECLARE @COUNT AS INT  
	DECLARE @numNewQtyShipped AS FLOAT


	/* VERIFY BIZDOC IS EXISTS OR NOT */
	IF NOT EXISTS (SELECT numOppBizDocsId FROM OpportunityBizDocs WHERE numOppBizDocsId = @numOppBizDocID)
	BEGIN
		RAISERROR('FULFILLMENT_BIZDOC_IS_DELETED',16,1)
	END

	/* VERIFY WHETHER BIZDOC IS ALREADY FULLFILLED BY SOMEBODY ELSE */
	IF EXISTS (SELECT numOppBizDocsId FROM OpportunityBizDocs WHERE numOppBizDocsId = @numOppBizDocID AND bitFulfilled = 1) 
	BEGIN
		RAISERROR('FULFILLMENT_BIZDOC_ALREADY_FULFILLED',16,1)
	END

	IF EXISTS(SELECT numWOId FROm WorkOrder WHERE numOppId=@numOppId AND numWOStatus <> 23184) --WORK ORDER NOT YET COMPLETED
	BEGIN
		RAISERROR('WORK_ORDER_NOT_COMPLETED',16,1)
	END

	BEGIN TRANSACTION

		BEGIN /* REVERT SEIRAL/LOT# NUMBER ASSIGNED FROM PRODCUT&SERVICES TAB */
			UPDATE WHIDL
				SET WHIDL.numQty = (CASE WHEN Item.bitLotNo = 1 THEN ISNULL(WHIDL.numQty,0) + ISNULL(OWSI.numQty,0) ELSE 1 END)
			FROM 
				WareHouseItmsDTL WHIDL
			INNER JOIN
				OppWarehouseSerializedItem OWSI
			ON
				WHIDL.numWareHouseItmsDTLID = OWSI.numWarehouseItmsDTLID
				AND WHIDL.numWareHouseItemID = OWSI.numWarehouseItmsID
			INNER JOIN
				WareHouseItems
			ON
				WHIDL.numWareHouseItemID = WareHouseItems.numWareHouseItemID
			INNER JOIN
				Item
			ON
				WareHouseItems.numItemID = Item.numItemCode
			WHERE
				OWSI.numOppID=@numOppId
				AND OWSI.numOppBizDocsId IS NULL
		END

		BEGIN /* GET FULFILLMENT BIZDOC ITEMS */
			DECLARE @TempFinalTable TABLE
			(
				ID INT IDENTITY(1,1),
				numItemCode NUMERIC(18,0),
				vcItemType CHAR(1),
				bitSerial BIT,
				bitLot BIT,
				bitAssembly BIT,
				bitKit BIT,
				numOppItemID NUMERIC(18,0),
				numWarehouseItemID NUMERIC(18,0),
				numQtyShipped FLOAT,
				numQty FLOAT,
				bitDropShip BIT,
				vcSerialLot VARCHAR(MAX)
			)

			INSERT INTO @TempFinalTable
			(
				numItemCode,
				vcItemType,
				bitSerial,
				bitLot,
				bitAssembly,
				bitKit,
				numOppItemID,
				numWarehouseItemID,
				numQtyShipped,
				numQty,
				bitDropShip,
				vcSerialLot
			)
			SELECT
				OpportunityBizDocItems.numItemCode,
				ISNULL(Item.charItemType,''),
				ISNULL(Item.bitSerialized,0),
				ISNULL(Item.bitLotNo,0),
				ISNULL(Item.bitAssembly,0),
				ISNULL(Item.bitKitParent,0),
				ISNULL(OpportunityItems.numoppitemtcode,0),
				ISNULL(OpportunityBizDocItems.numWarehouseItmsID,0),
				ISNULL(OpportunityItems.numQtyShipped,0),
				ISNULL(OpportunityBizDocItems.numUnitHour,0),
				ISNULL(OpportunityItems.bitDropShip,0),
				''
			FROM
				OpportunityBizDocs
			INNER JOIN
				OpportunityBizDocItems
			ON
				OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
			INNER JOIN
				OpportunityItems
			ON
				OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtCode
			INNER JOIN
				Item
			ON
				OpportunityBizDocItems.numItemCode = Item.numItemCode
			WHERE
				numOppBizDocsId = @numOppBizDocID
				AND ISNULL(OpportunityItems.bitDropShip,0) = 0
		END

		BEGIN /* GET ALL ITEMS ADDED TO BIZDOC WITH ASSIGNED SERIAL/LOT# FOR SERIAL/LOT ITEM  */
			DECLARE @TempItemXML TABLE
			(
				numOppItemID NUMERIC(18,0),
				numQty FLOAT,
				numWarehouseItemID NUMERIC(18,0),
				vcSerialLot VARCHAR(MAX)
			)

			INSERT INTO
				@TempItemXML
			SELECT
				OBI.numOppItemID,
				OBI.numUnitHour,
				OBI.numWarehouseItmsID,
				STUFF((SELECT 
							',' + WIDL.vcSerialNo + (CASE WHEN ISNULL(I.bitLotNo,0) = 1 THEN '(' + CAST(OWSI.numQty AS VARCHAR) + ')'  ELSE '' END)
						FROM 
							OppWarehouseSerializedItem OWSI
						INNER JOIN
							WareHouseItmsDTL WIDL
						ON
							OWSI.numWarehouseItmsDTLID = WIDL.numWareHouseItmsDTLID
						INNER JOIN
							WareHouseItems WI
						ON
							OWSI.numWarehouseItmsID = WI.numWareHouseItemID
						INNER JOIN
							Item I
						ON
							WI.numItemID = I.numItemCode
						WHERE 
							OWSI.numOppID = @numOppId
							AND OWSI.numOppItemID = OBI.numOppItemID
							AND ISNULL(OWSI.numOppBizDocsId,0) = 0 --THIS CONDITION IS REQUIRED BECAUSE IT IS AUTOFULFILLMENT
					for xml path('')
				),1,1,'') AS vcSerialLot
			FROM 
				OpportunityBizDocItems OBI
			WHERE
				OBI.numOppBizDocID = @numOppBizDocID
		END

		BEGIN /* VALIDATE ALL THE DETAILS BEFORE FULFILLING ITEMS */
		
			SELECT @COUNT=COUNT(*) FROM @TempFinalTable

			DECLARE @TempLot TABLE
			(
				vcLotNo VARCHAR(100),
				numQty FLOAT 
			)

			WHILE @i <= @COUNT
			BEGIN
				SELECT
					@numOppItemID=numOppItemID,
					@numWarehouseItemID=numWarehouseItemID,
					@numQty=numQty,
					@bitSerial=bitSerial,
					@bitLot=bitLot 
				FROM 
					@TempFinalTable 
				WHERE 
					ID = @i

				--GET QUENTITY RECEIVED FROM XML PARAMETER
				SELECT 
					@numXmlQty = numQty, 
					@vcSerialLot=vcSerialLot, 
					@numSerialCount = (CASE WHEN (@bitSerial = 1 OR @bitLot = 1) THEN (SELECT COUNT(*) FROM dbo.SplitString(vcSerialLot,',')) ELSE 0 END)  
				FROM 
					@TempItemXML 
				WHERE 
					numOppItemID = @numOppItemID 

				--CHECK IF QUANTITY OF ITEM IN OpportunityBizDocItems TBALE IS SAME AS QUANTITY SUPPLIED FROM XML
				IF @numQty <> @numXmlQty
				BEGIN
					RAISERROR('QTY_MISMATCH',16,1)
				END

				--IF ITEM IS SERIAL ITEM CHEKC THAT VALID AND REQUIRED NUMBER OF SERIALS ARE PROVIDED BY USER
				IF @bitSerial = 1 
				BEGIN
					IF @numSerialCount <> @numQty
					BEGIN
						RAISERROR('REQUIRED_SERIALS_NOT_PROVIDED',16,1)
					END
					ELSE
					BEGIN
						IF (SELECT COUNT(*) FROM WareHouseItmsDTL WHERE numWareHouseItemID = @numWarehouseItemID AND numQty > 0 AND vcSerialNo IN (SELECT OutParam FROM dbo.SplitString(@vcSerialLot,','))) <> @numSerialCount
						BEGIN
							RAISERROR('INVALID_SERIAL_NUMBERS',16,1)
						END
					END
				END

				IF @bitLot = 1
				BEGIN
					-- DELETE DATA OF PREVIOUS ITERATION FROM TABLE
					DELETE FROM @TempLot

					--CONVERTS COMMA SEPERATED STRING TO LOTNo AND QTY 
					INSERT INTO
						@TempLot
					SELECT
						SUBSTRING(OutParam,0,PATINDEX('%(%', OutParam)),
						CAST(SUBSTRING(OutParam,(PATINDEX('%(%', OutParam) + 1),(PATINDEX('%)%', OutParam) - PATINDEX('%(%', OutParam) - 1)) AS INT)
					FROM
						dbo.SplitString(@vcSerialLot,',')

					--CHECKS WHETHER PROVIDED QTY OF LOT NO IS SAME IS REQUIRED QTY
					IF (SELECT SUM(numQty) FROM @TempLot) <> @numQty
					BEGIN
						RAISERROR('REQUIRED_LOTNO_NOT_PROVIDED',16,1)
					END
					ELSE
					BEGIN
						-- CHECKS WHETHER LOT NO EXISTS IN WareHouseItmsDTL
						IF (SELECT COUNT(*) FROM WareHouseItmsDTL WHERE numWareHouseItemID = @numWarehouseItemID AND vcSerialNo IN (SELECT vcLotNo FROM @TempLot)) <> (SELECT COUNT(*) FROM @TempLot)
						BEGIN
							RAISERROR('INVALID_LOT_NUMBERS',16,1)
						END
						ELSE
						BEGIN
							-- CHECKS WHETHER LOT NO HAVE ENOUGH QTY TO FULFILL ITEM QTY
							IF (SELECT 
									COUNT(*) 
								FROM 
									WareHouseItmsDTL 
								INNER JOIN
									@TempLot TEMPLOT
								ON
									WareHouseItmsDTL.vcSerialNo = TEMPLOT.vcLotNo
								WHERE 
									numWareHouseItemID = @numWarehouseItemID
									AND TEMPLOT.numQty > WareHouseItmsDTL.numQty) > 0
							BEGIN
								RAISERROR('SOME_LOTNO_DO_NOT_HAVE_ENOUGH_QTY',16,1)
							END
						END
					END
				END

		
				UPDATE @TempFinalTable SET vcSerialLot = @vcSerialLot WHERE ID=@i

				SET @i = @i + 1
			END
		END
	
		SET @i = 1
		SELECT @COUNT = COUNT(*) FROM @TempFinalTable
		DECLARE @vcDescription AS VARCHAR(300)
		DECLARE @numAllocation AS FLOAT

		DECLARE @j INT = 0
		DECLARE @ChildCount AS INT
		DECLARE @vcChildDescription AS VARCHAR(300)
		DECLARE @numChildAllocation AS FLOAT
		DECLARE @numOppChildItemID AS INT
		DECLARE @numChildItemCode AS INT
		DECLARE @bitChildIsKit AS BIT
		DECLARE @numChildWarehouseItemID AS INT
		DECLARE @numChildQty AS FLOAT
		DECLARE @numChildQtyShipped AS FLOAT

		DECLARE @TempKitSubItems TABLE
		(
			ID INT,
			numOppChildItemID NUMERIC(18,0),
			numChildItemCode NUMERIC(18,0),
			bitChildIsKit BIT,
			numChildWarehouseItemID NUMERIC(18,0),
			numChildQty FLOAT,
			numChildQtyShipped FLOAT
		)

		DECLARE @k AS INT = 1
		DECLARE @CountKitChildItems AS INT
		DECLARE @vcKitChildDescription AS VARCHAR(300)
		DECLARE @numKitChildAllocation AS FLOAT
		DECLARE @numOppKitChildItemID AS NUMERIC(18,0)
		DECLARE @numKitChildItemCode AS NUMERIC(18,0)
		DECLARE @numKitChildWarehouseItemID AS NUMERIC(18,0)
		DECLARE @numKitChildQty AS FLOAT
		DECLARE @numKitChildQtyShipped AS FLOAT

		DECLARE @TempKitChildKitSubItems TABLE
		(
			ID INT,
			numOppKitChildItemID NUMERIC(18,0),
			numKitChildItemCode NUMERIC(18,0),
			numKitChildWarehouseItemID NUMERIC(18,0),
			numKitChildQty FLOAT,
			numKitChildQtyShipped FLOAT
		)

		WHILE @i <= @COUNT
		BEGIN
			SELECT 
				@vcItemType=vcItemType,
				@bitSerial=bitSerial,
				@bitLot=bitLot,
				@bitAssembly=bitAssembly,
				@bitKit=bitKit,
				@bitDropShip=bitDropShip,
				@numOppItemID=numOppItemID,
				@numQty=numQty,
				@numQtyShipped = numQtyShipped,
				@numWarehouseItemID=numWarehouseItemID,
				@vcSerialLot = vcSerialLot
			FROM 
				@TempFinalTable 
			WHERE 
				ID = @i

			--MAKE INVENTIRY CHANGES IF INVENTORY ITEM
			IF (UPPER(@vcItemType) = 'P' AND @bitDropShip = 0)
			BEGIN
				SET @vcDescription='SO Qty Shipped (Qty:' + CAST(@numQty AS VARCHAR(10)) + ' Shipped:' +  CAST(@numQtyShipped AS VARCHAR(10)) + ')'
						 
				IF @bitKit = 1
				BEGIN
					-- CLEAR DATA OF PREVIOUS ITERATION
					DELETE FROM @TempKitSubItems

					-- GET KIT SUB ITEMS DETAIL
					INSERT INTO @TempKitSubItems
					(
						ID,
						numOppChildItemID,
						numChildItemCode,
						bitChildIsKit,
						numChildWarehouseItemID,
						numChildQty,
						numChildQtyShipped
					)
					SELECT 
						ROW_NUMBER() OVER(ORDER BY numOppChildItemID),
						ISNULL(numOppChildItemID,0),
						ISNULL(I.numItemCode,0),
						ISNULL(I.bitKitParent,0),
						ISNULL(numWareHouseItemId,0),
						((ISNULL(numQtyItemsReq_Orig,0) * ISNULL(dbo.fn_UOMConversion(OKI.numUOMId,OKI.numChildItemID,@numDomainID,I.numBaseUnit),1)) * @numQty),
						ISNULL(numQtyShipped,0)
					FROM 
						OpportunityKitItems OKI 
					JOIN 
						Item I 
					ON 
						OKI.numChildItemID=I.numItemCode    
					WHERE 
						charitemtype='P' 
						AND ISNULL(numWareHouseItemId,0) > 0 
						AND OKI.numOppID=@numOppID 
						AND OKI.numOppItemID=@numOppItemID 

					SET @j = 1
					SELECT @ChildCount=COUNT(*) FROM @TempKitSubItems

					--LOOP OVER ALL CHILD ITEMS AND RELESE ALLOCATION
					WHILE @j <= @ChildCount
					BEGIN
						SELECT
							@numOppChildItemID=numOppChildItemID,
							@numChildItemCode=numChildItemCode,
							@bitChildIsKit=bitChildIsKit,
							@numChildWarehouseItemID=numChildWarehouseItemID,
							@numChildQty=numChildQty,
							@numChildQtyShipped=numChildQtyShipped
						FROM
							@TempKitSubItems
						WHERE 
							ID = @j

						SELECT @numChildAllocation=ISNULL(numAllocation,0) FROM WareHouseItems WHERE numWareHouseItemID=@numChildWarehouseItemID

						-- IF CHILD ITEM IS KIT THEN LOOK OUT FOR ITS CHILD ITEMS WHICH ARE STORED IN OpportunityKitChildItems TABLE
						IF @bitChildIsKit = 1
						BEGIN
							-- IF KIT IS ADDED WITHIN KIT THEN THIS SUB KIT HAS ITS OWN ITEM CONFIGURATION WHICH ID STORED IN OpportunityKitChildItems
							IF EXISTS (SELECT numOppKitChildItemID FROM OpportunityKitChildItems WHERE numOppID=@numOppId AND numOppItemID=@numOppItemID AND numOppChildItemID=@numOppChildItemID)
							BEGIN
								-- CLEAR DATA OF PREVIOUS ITERATION
								DELETE FROM @TempKitChildKitSubItems

								-- GET SUB KIT SUB ITEMS DETAIL
								INSERT INTO @TempKitChildKitSubItems
								(
									ID,
									numOppKitChildItemID,
									numKitChildItemCode,
									numKitChildWarehouseItemID,
									numKitChildQty,
									numKitChildQtyShipped
								)
								SELECT 
									ROW_NUMBER() OVER(ORDER BY numOppKitChildItemID),
									ISNULL(numOppKitChildItemID,0),
									ISNULL(OKCI.numItemID,0),
									ISNULL(OKCI.numWareHouseItemId,0),
									((ISNULL(OKCI.numQtyItemsReq_Orig,0) * ISNULL(dbo.fn_UOMConversion(OKCI.numUOMId,OKCI.numItemID,@numDomainID,I.numBaseUnit),1)) * @numChildQty),
									ISNULL(numQtyShipped,0)
								FROM 
									OpportunityKitChildItems OKCI 
								JOIN 
									Item I 
								ON 
									OKCI.numItemID=I.numItemCode    
								WHERE 
									charitemtype='P' 
									AND ISNULL(numWareHouseItemId,0) > 0 
									AND OKCI.numOppID=@numOppID 
									AND OKCI.numOppItemID=@numOppItemID 
									AND OKCI.numOppChildItemID = @numOppChildItemID

								SET @k = 1
								SELECT @CountKitChildItems=COUNT(*) FROM @TempKitChildKitSubItems

								WHILE @k <= @CountKitChildItems
								BEGIN
									SELECT
										@numOppKitChildItemID=numOppKitChildItemID,
										@numKitChildItemCode=numKitChildItemCode,
										@numKitChildWarehouseItemID=numKitChildWarehouseItemID,
										@numKitChildQty=numKitChildQty,
										@numKitChildQtyShipped=numKitChildQtyShipped
									FROM
										@TempKitChildKitSubItems
									WHERE 
										ID = @k

									SELECT @numKitChildAllocation=ISNULL(numAllocation,0) FROM WareHouseItems WHERE numWareHouseItemID=@numKitChildWarehouseItemID

									--REMOVE QTY FROM ALLOCATION
									SET @numKitChildAllocation = @numKitChildAllocation - @numKitChildQty

									IF @numKitChildAllocation >= 0
									BEGIN
										UPDATE  
											WareHouseItems
										SET     
											numAllocation = @numKitChildAllocation,
											dtModified = GETDATE() 
										WHERE   
											numWareHouseItemID = @numKitChildWarehouseItemID      	
							
										UPDATE  
											OpportunityKitChildItems
										SET 
											numQtyShipped = ISNULL(numQtyShipped,0) + ISNULL(@numKitChildQty,0)
										WHERE   
											numOppKitChildItemID = @numOppKitChildItemID
											AND numOppChildItemID = @numOppChildItemID 
											AND numOppId=@numOppId
											AND numOppItemID=@numOppItemID

										SET @vcKitChildDescription = 'SO CHILD KIT Qty Shipped (Qty:' + CAST(@numKitChildQty AS VARCHAR(10)) + ' Shipped:' +  CAST(@numKitChildQtyShipped AS VARCHAR(10)) + ')'

										EXEC dbo.USP_ManageWareHouseItems_Tracking
											@numWareHouseItemID = @numKitChildWarehouseItemID,
											@numReferenceID = @numOppId,
											@tintRefType = 3,
											@vcDescription = @vcKitChildDescription,
											@numModifiedBy = @numUserCntID,
											@numDomainID = @numDomainID	 
									END
									ELSE
									BEGIN
										RAISERROR ('NOTSUFFICIENTQTY_ALLOCATION',16,1);
									END

									SET @k = @k + 1
								END
							END 

							UPDATE  
								OpportunityKitItems
							SET 
								numQtyShipped = ISNULL(numQtyShipped,0) + ISNULL(@numChildQty,0)
							WHERE   
								numOppChildItemID = @numOppChildItemID 
								AND numOppId=@numOppId
								AND numOppItemID=@numOppItemID

							SET @vcChildDescription = 'SO KIT Qty Shipped (Qty:' + CAST(@numChildQty AS VARCHAR(10)) + ' Shipped:' +  CAST(@numChildQtyShipped AS VARCHAR(10)) + ')'

							EXEC dbo.USP_ManageWareHouseItems_Tracking
								@numWareHouseItemID = @numChildWarehouseItemID,
								@numReferenceID = @numOppId,
								@tintRefType = 3,
								@vcDescription = @vcChildDescription,
								@numModifiedBy = @numUserCntID,
								@numDomainID = @numDomainID	
						END
						ELSE
						BEGIN
							--REMOVE QTY FROM ALLOCATION
							SET @numChildAllocation = @numChildAllocation - @numChildQty

							IF @numChildAllocation >= 0
							BEGIN
								UPDATE  
									WareHouseItems
								SET     
									numAllocation = @numChildAllocation,
									dtModified = GETDATE() 
								WHERE   
									numWareHouseItemID = @numChildWarehouseItemID      	
							
								UPDATE  
									OpportunityKitItems
								SET 
									numQtyShipped = ISNULL(numQtyShipped,0) + ISNULL(@numChildQty,0)
								WHERE   
									numOppChildItemID = @numOppChildItemID 
									AND numOppId=@numOppId
									AND numOppItemID=@numOppItemID

								SET @vcChildDescription = 'SO KIT Qty Shipped (Qty:' + CAST(@numChildQty AS VARCHAR(10)) + ' Shipped:' +  CAST(@numChildQtyShipped AS VARCHAR(10)) + ')'

								EXEC dbo.USP_ManageWareHouseItems_Tracking
									@numWareHouseItemID = @numChildWarehouseItemID,
									@numReferenceID = @numOppId,
									@tintRefType = 3,
									@vcDescription = @vcChildDescription,
									@numModifiedBy = @numUserCntID,
									@numDomainID = @numDomainID	
							END
							ELSE
							BEGIN
								RAISERROR ('NOTSUFFICIENTQTY_ALLOCATION',16,1);
							END
						END

						SET @j = @j + 1
					END
				END
				ELSE
				BEGIN
					SELECT @numAllocation = ISNULL(numAllocation, 0) FROM WareHouseItems WHERE numWareHouseItemID = @numWareHouseItemID
				       
					--REMOVE QTY FROM ALLOCATION
					SET @numAllocation = @numAllocation - @numQty	
				       
					IF (@numAllocation >= 0 )
					BEGIN
						UPDATE  
							WareHouseItems
						SET     
							numAllocation = @numAllocation,
							dtModified = GETDATE() 
						WHERE   
							numWareHouseItemID = @numWareHouseItemID      	

						IF @bitSerial = 1
						BEGIN								
							DELETE FROM OppWarehouseSerializedItem WHERE numOppID=@numOppID AND numOppItemID=@numOppItemID AND numWarehouseItmsID=@numWarehouseItemID

							INSERT INTO OppWarehouseSerializedItem
							(
								numWarehouseItmsDTLID,
								numOppID,
								numOppItemID,
								numWarehouseItmsID,
								numQty,
								numOppBizDocsId	
							)
							SELECT
								(SELECT TOP 1 numWarehouseItmsDTLID FROM WareHouseItmsDTL WHERE numWareHouseItemID=@numWarehouseItemID AND vcSerialNo=OutParam AND  numQty = 1),
								@numOppID,
								@numOppItemID,
								@numWarehouseItemID,
								1,
								@numOppBizDocID
							FROM
								dbo.SplitString(@vcSerialLot,',')


							UPDATE WareHouseItmsDTL SET numQty=0 WHERE numWareHouseItemID=@numWarehouseItemID AND vcSerialNo IN (SELECT OutParam FROM dbo.SplitString(@vcSerialLot,','))
						END

						IF @bitLot = 1
						BEGIN
							DELETE FROM @TempLot

							INSERT INTO
								@TempLot
							SELECT
								SUBSTRING(OutParam,0,PATINDEX('%(%', OutParam)),
								CAST(SUBSTRING(OutParam,(PATINDEX('%(%', OutParam) + 1),(PATINDEX('%)%', OutParam) - PATINDEX('%(%', OutParam) - 1)) AS INT)
							FROM
								dbo.SplitString(@vcSerialLot,',')

							DELETE FROM OppWarehouseSerializedItem WHERE numOppID=@numOppID AND numOppItemID=@numOppItemID AND numWarehouseItmsID=@numWarehouseItemID

							INSERT INTO OppWarehouseSerializedItem
							(
								numWarehouseItmsDTLID,
								numOppID,
								numOppItemID,
								numWarehouseItmsID,
								numQty,
								numOppBizDocsId	
							)
							SELECT
								(SELECT TOP 1 numWarehouseItmsDTLID FROM WareHouseItmsDTL WHERE numWareHouseItemID=@numWarehouseItemID AND vcSerialNo=TEMPLOT.vcLotNo AND numQty >= TEMPLOT.numQty),
								@numOppID,
								@numOppItemID,
								@numWarehouseItemID,
								TEMPLOT.numQty,
								@numOppBizDocID
							FROM
								@TempLot TEMPLOT


							UPDATE	TEMPWareHouseItmsDTL
								SET TEMPWareHouseItmsDTL.numQty = ISNULL(TEMPWareHouseItmsDTL.numQty,0) - ISNULL(TEMPLOT.numQty,0)
							FROM
								WareHouseItmsDTL TEMPWareHouseItmsDTL
							INNER JOIN
								@TempLot TEMPLOT
							ON
								TEMPWareHouseItmsDTL.vcSerialNo = TEMPLOT.vcLotNo
							WHERE
								TEMPWareHouseItmsDTL.numWareHouseItemID = @numWarehouseItemID
						END
					END
					ELSE
					BEGIN
						PRINT 'PARENT: ' + CAST(@numChildWarehouseItemID AS VARCHAR)
						RAISERROR ('NOTSUFFICIENTQTY_ALLOCATION',16,1);
					END
				END

				EXEC dbo.USP_ManageWareHouseItems_Tracking
							@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
							@numReferenceID = @numOppId, --  numeric(9, 0)
							@tintRefType = 3, --  tinyint
							@vcDescription = @vcDescription, --  varchar(100)
							@numModifiedBy = @numUserCntID,
							@numDomainID = @numDomainID	 
			END

			UPDATE  
				OpportunityItems
			SET 
				numQtyShipped = ISNULL(numQtyShipped,0) + ISNULL(@numQty,0)
			WHERE   
				numoppitemtCode = @numOppItemID 
				AND numOppId=@numOppId

			SET @i = @i + 1
		END

		UPDATE OpportunityBizDocs SET bitFulfilled = 1,dtShippedDate=GETUTCDATE() WHERE numOppBizDocsId=@numOppBizDocID
	COMMIT TRANSACTION
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
END


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityBizDocs_FulFill')
DROP PROCEDURE USP_OpportunityBizDocs_FulFill
GO
CREATE PROCEDURE [dbo].[USP_OpportunityBizDocs_FulFill] 
	@numDomainID NUMERIC(18,0),
	@numUserCntID NUMERIC(18,0),
	@numOppId NUMERIC(18,0),
	@numOppBizDocID NUMERIC(18,0),
	@vcItems VARCHAR(MAX),
	@dtShippedDate DATETIME
AS  
BEGIN 
BEGIN TRY
	DECLARE @vcItemType AS CHAR(1)
	DECLARE @bitDropShip AS BIT
	DECLARE @numOppItemID AS INT
	DECLARE @numWarehouseItemID AS INT
	DECLARE @numQty AS FLOAT
	DECLARE @numQtyShipped AS FLOAT
	DECLARE @numXmlQty AS FLOAT
	DECLARE @vcSerialLot AS VARCHAR(MAX)
	DECLARE @numSerialCount AS INT
	DECLARE @bitSerial AS BIT
	DECLARE @bitLot AS BIT
	DECLARE @bitAssembly AS BIT
	DECLARE @bitKit AS BIT

	DECLARE @i AS INT = 1
	DECLARE @COUNT AS INT  
	DECLARE @numNewQtyShipped AS FLOAT


	/* VERIFY BIZDOC IS EXISTS OR NOT */
	IF NOT EXISTS (SELECT numOppBizDocsId FROM OpportunityBizDocs WHERE numOppBizDocsId = @numOppBizDocID)
	BEGIN
		RAISERROR('FULFILLMENT_BIZDOC_IS_DELETED',16,1)
	END

	/* VERIFY WHETHER BIZDOC IS ALREADY FULLFILLED BY SOMEBODY ELSE */
	IF EXISTS (SELECT numOppBizDocsId FROM OpportunityBizDocs WHERE numOppBizDocsId = @numOppBizDocID AND bitFulfilled = 1) 
	BEGIN
		RAISERROR('FULFILLMENT_BIZDOC_ALREADY_FULFILLED',16,1)
	END

	IF EXISTS(SELECT numWOId FROm WorkOrder WHERE numOppId=@numOppId AND numWOStatus <> 23184) --WORK ORDER NOT YET COMPLETED
	BEGIN
		RAISERROR('WORK_ORDER_NOT_COMPLETED',16,1)
	END

	BEGIN TRANSACTION

		BEGIN /* REVERT SEIRAL/LOT# NUMBER ASSIGNED FROM PRODCUT&SERVICES TAB */
			UPDATE WHIDL
				SET WHIDL.numQty = (CASE WHEN Item.bitLotNo = 1 THEN ISNULL(WHIDL.numQty,0) + ISNULL(OWSI.numQty,0) ELSE 1 END)
			FROM 
				WareHouseItmsDTL WHIDL
			INNER JOIN
				OppWarehouseSerializedItem OWSI
			ON
				WHIDL.numWareHouseItmsDTLID = OWSI.numWarehouseItmsDTLID
				AND WHIDL.numWareHouseItemID = OWSI.numWarehouseItmsID
			INNER JOIN
				WareHouseItems
			ON
				WHIDL.numWareHouseItemID = WareHouseItems.numWareHouseItemID
			INNER JOIN
				Item
			ON
				WareHouseItems.numItemID = Item.numItemCode
			WHERE
				OWSI.numOppID=@numOppId
				AND OWSI.numOppBizDocsId IS NULL
		END

		BEGIN /* GET FULFILLMENT BIZDOC ITEMS */
			DECLARE @TempFinalTable TABLE
			(
				ID INT IDENTITY(1,1),
				numItemCode NUMERIC(18,0),
				vcItemType CHAR(1),
				bitSerial BIT,
				bitLot BIT,
				bitAssembly BIT,
				bitKit BIT,
				numOppItemID NUMERIC(18,0),
				numWarehouseItemID NUMERIC(18,0),
				numQtyShipped FLOAT,
				numQty FLOAT,
				bitDropShip BIT,
				vcSerialLot VARCHAR(MAX)
			)

			INSERT INTO @TempFinalTable
			(
				numItemCode,
				vcItemType,
				bitSerial,
				bitLot,
				bitAssembly,
				bitKit,
				numOppItemID,
				numWarehouseItemID,
				numQtyShipped,
				numQty,
				bitDropShip,
				vcSerialLot
			)
			SELECT
				OpportunityBizDocItems.numItemCode,
				ISNULL(Item.charItemType,''),
				ISNULL(Item.bitSerialized,0),
				ISNULL(Item.bitLotNo,0),
				ISNULL(Item.bitAssembly,0),
				ISNULL(Item.bitKitParent,0),
				ISNULL(OpportunityItems.numoppitemtcode,0),
				ISNULL(OpportunityBizDocItems.numWarehouseItmsID,0),
				ISNULL(OpportunityItems.numQtyShipped,0),
				ISNULL(OpportunityBizDocItems.numUnitHour,0),
				ISNULL(OpportunityItems.bitDropShip,0),
				''
			FROM
				OpportunityBizDocs
			INNER JOIN
				OpportunityBizDocItems
			ON
				OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
			INNER JOIN
				OpportunityItems
			ON
				OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtCode
			INNER JOIN
				Item
			ON
				OpportunityBizDocItems.numItemCode = Item.numItemCode
			WHERE
				numOppBizDocsId = @numOppBizDocID
				AND ISNULL(OpportunityItems.bitDropShip,0) = 0
		END

		BEGIN /* CONVERT vcItems XML to TABLE */
			DECLARE @TempItemXML TABLE
			(
				numOppItemID NUMERIC(18,0),
				numQty FLOAT,
				numWarehouseItemID NUMERIC(18,0),
				vcSerialLot VARCHAR(MAX)
			)

			DECLARE @idoc int
			EXEC sp_xml_preparedocument @idoc OUTPUT, @vcItems;

			INSERT INTO
				@TempItemXML
			SELECT
				numOppItemID,
				numQty,
				numWarehouseItemID,
				vcSerialLot
			FROM 
				OPENXML (@idoc, '/Items/Item',2)
			WITH 
			(
				numOppItemID NUMERIC(18,0),
				numQty FLOAT,
				numWarehouseItemID NUMERIC(18,0),
				vcSerialLot VARCHAR(MAX)
			);
		END

		BEGIN /* VALIDATE ALL THE DETAILS BEFORE FULFILLING ITEMS */
		
			SELECT @COUNT=COUNT(*) FROM @TempFinalTable

			DECLARE @TempLot TABLE
			(
				vcLotNo VARCHAR(100),
				numQty FLOAT 
			)

			WHILE @i <= @COUNT
			BEGIN
				SELECT
					@numOppItemID=numOppItemID,
					@numWarehouseItemID=numWarehouseItemID,
					@numQty=numQty,
					@bitSerial=bitSerial,
					@bitLot=bitLot 
				FROM 
					@TempFinalTable 
				WHERE 
					ID = @i

				--GET QUENTITY RECEIVED FROM XML PARAMETER
				SELECT 
					@numXmlQty = numQty, 
					@vcSerialLot=vcSerialLot, 
					@numSerialCount = (CASE WHEN (@bitSerial = 1 OR @bitLot = 1) THEN (SELECT COUNT(*) FROM dbo.SplitString(vcSerialLot,',')) ELSE 0 END)  
				FROM 
					@TempItemXML 
				WHERE 
					numOppItemID = @numOppItemID 

				--CHECK IF QUANTITY OF ITEM IN OpportunityBizDocItems TBALE IS SAME AS QUANTITY SUPPLIED FROM XML
				IF @numQty <> @numXmlQty
				BEGIN
					RAISERROR('QTY_MISMATCH',16,1)
				END

				--IF ITEM IS SERIAL ITEM CHEKC THAT VALID AND REQUIRED NUMBER OF SERIALS ARE PROVIDED BY USER
				IF @bitSerial = 1 
				BEGIN
					IF @numSerialCount <> @numQty
					BEGIN
						RAISERROR('REQUIRED_SERIALS_NOT_PROVIDED',16,1)
					END
					ELSE
					BEGIN
						IF (SELECT COUNT(*) FROM WareHouseItmsDTL WHERE numWareHouseItemID = @numWarehouseItemID AND numQty > 0 AND vcSerialNo IN (SELECT OutParam FROM dbo.SplitString(@vcSerialLot,','))) <> @numSerialCount
						BEGIN
							RAISERROR('INVALID_SERIAL_NUMBERS',16,1)
						END
					END
				END

				IF @bitLot = 1
				BEGIN
					-- DELETE DATA OF PREVIOUS ITERATION FROM TABLE
					DELETE FROM @TempLot

					--CONVERTS COMMA SEPERATED STRING TO LOTNo AND QTY 
					INSERT INTO
						@TempLot
					SELECT
						SUBSTRING(OutParam,0,PATINDEX('%(%', OutParam)),
						CAST(SUBSTRING(OutParam,(PATINDEX('%(%', OutParam) + 1),(PATINDEX('%)%', OutParam) - PATINDEX('%(%', OutParam) - 1)) AS INT)
					FROM
						dbo.SplitString(@vcSerialLot,',')

					--CHECKS WHETHER PROVIDED QTY OF LOT NO IS SAME IS REQUIRED QTY
					IF (SELECT SUM(numQty) FROM @TempLot) <> @numQty
					BEGIN
						RAISERROR('REQUIRED_LOTNO_NOT_PROVIDED',16,1)
					END
					ELSE
					BEGIN
						-- CHECKS WHETHER LOT NO EXISTS IN WareHouseItmsDTL
						IF (SELECT COUNT(*) FROM WareHouseItmsDTL WHERE numWareHouseItemID = @numWarehouseItemID AND vcSerialNo IN (SELECT vcLotNo FROM @TempLot)) <> (SELECT COUNT(*) FROM @TempLot)
						BEGIN
							RAISERROR('INVALID_LOT_NUMBERS',16,1)
						END
						ELSE
						BEGIN
							-- CHECKS WHETHER LOT NO HAVE ENOUGH QTY TO FULFILL ITEM QTY
							IF (SELECT 
									COUNT(*) 
								FROM 
									WareHouseItmsDTL 
								INNER JOIN
									@TempLot TEMPLOT
								ON
									WareHouseItmsDTL.vcSerialNo = TEMPLOT.vcLotNo
								WHERE 
									numWareHouseItemID = @numWarehouseItemID
									AND TEMPLOT.numQty > WareHouseItmsDTL.numQty) > 0
							BEGIN
								RAISERROR('SOME_LOTNO_DO_NOT_HAVE_ENOUGH_QTY',16,1)
							END
						END
					END
				END

		
				UPDATE @TempFinalTable SET vcSerialLot = @vcSerialLot WHERE ID=@i

				SET @i = @i + 1
			END
		END
	
		SET @i = 1
		SELECT @COUNT = COUNT(*) FROM @TempFinalTable
		DECLARE @vcDescription AS VARCHAR(300)
		DECLARE @numAllocation AS FLOAT

		DECLARE @j INT = 0
		DECLARE @ChildCount AS INT
		DECLARE @vcChildDescription AS VARCHAR(300)
		DECLARE @numChildAllocation AS FLOAT
		DECLARE @numOppChildItemID AS INT
		DECLARE @numChildItemCode AS INT
		DECLARE @bitChildIsKit AS BIT
		DECLARE @numChildWarehouseItemID AS INT
		DECLARE @numChildQty AS FLOAT
		DECLARE @numChildQtyShipped AS FLOAT

		DECLARE @TempKitSubItems TABLE
		(
			ID INT,
			numOppChildItemID NUMERIC(18,0),
			numChildItemCode NUMERIC(18,0),
			bitChildIsKit BIT,
			numChildWarehouseItemID NUMERIC(18,0),
			numChildQty FLOAT,
			numChildQtyShipped FLOAT
		)


		DECLARE @k AS INT = 1
		DECLARE @CountKitChildItems AS INT
		DECLARE @vcKitChildDescription AS VARCHAR(300)
		DECLARE @numKitChildAllocation AS FLOAT
		DECLARE @numOppKitChildItemID AS NUMERIC(18,0)
		DECLARE @numKitChildItemCode AS NUMERIC(18,0)
		DECLARE @numKitChildWarehouseItemID AS NUMERIC(18,0)
		DECLARE @numKitChildQty AS FLOAT
		DECLARE @numKitChildQtyShipped AS FLOAT

		DECLARE @TempKitChildKitSubItems TABLE
		(
			ID INT,
			numOppKitChildItemID NUMERIC(18,0),
			numKitChildItemCode NUMERIC(18,0),
			numKitChildWarehouseItemID NUMERIC(18,0),
			numKitChildQty FLOAT,
			numKitChildQtyShipped FLOAT
		)

		WHILE @i <= @COUNT
		BEGIN
			SELECT 
				@vcItemType=vcItemType,
				@bitSerial=bitSerial,
				@bitLot=bitLot,
				@bitAssembly=bitAssembly,
				@bitKit=bitKit,
				@bitDropShip=bitDropShip,
				@numOppItemID=numOppItemID,
				@numQty=numQty,
				@numQtyShipped = numQtyShipped,
				@numWarehouseItemID=numWarehouseItemID,
				@vcSerialLot = vcSerialLot
			FROM 
				@TempFinalTable 
			WHERE 
				ID = @i

			--MAKE INVENTIRY CHANGES IF INVENTORY ITEM
			IF (UPPER(@vcItemType) = 'P' AND @bitDropShip = 0)
			BEGIN
				SET @vcDescription='SO Qty Shipped (Qty:' + CAST(@numQty AS VARCHAR(10)) + ' Shipped:' +  CAST(@numQtyShipped AS VARCHAR(10)) + ')'
						 
				IF @bitKit = 1
				BEGIN
					-- CLEAR DATA OF PREVIOUS ITERATION
					DELETE FROM @TempKitSubItems

					-- GET KIT SUB ITEMS DETAIL
					INSERT INTO @TempKitSubItems
					(
						ID,
						numOppChildItemID,
						numChildItemCode,
						bitChildIsKit,
						numChildWarehouseItemID,
						numChildQty,
						numChildQtyShipped
					)
					SELECT 
						ROW_NUMBER() OVER(ORDER BY numOppChildItemID),
						ISNULL(numOppChildItemID,0),
						ISNULL(I.numItemCode,0),
						ISNULL(I.bitKitParent,0),
						ISNULL(numWareHouseItemId,0),
						((ISNULL(numQtyItemsReq_Orig,0) * ISNULL(dbo.fn_UOMConversion(OKI.numUOMId,OKI.numChildItemID,@numDomainID,I.numBaseUnit),1)) * @numQty),
						ISNULL(numQtyShipped,0)
					FROM 
						OpportunityKitItems OKI 
					JOIN 
						Item I 
					ON 
						OKI.numChildItemID=I.numItemCode    
					WHERE 
						charitemtype='P' 
						AND ISNULL(numWareHouseItemId,0) > 0 
						AND OKI.numOppID=@numOppID 
						AND OKI.numOppItemID=@numOppItemID 

					SET @j = 1
					SELECT @ChildCount=COUNT(*) FROM @TempKitSubItems

					--LOOP OVER ALL CHILD ITEMS AND RELESE ALLOCATION
					WHILE @j <= @ChildCount
					BEGIN
						SELECT
							@numOppChildItemID=numOppChildItemID,
							@numChildItemCode=numChildItemCode,
							@bitChildIsKit=bitChildIsKit,
							@numChildWarehouseItemID=numChildWarehouseItemID,
							@numChildQty=numChildQty,
							@numChildQtyShipped=numChildQtyShipped
						FROM
							@TempKitSubItems
						WHERE 
							ID = @j

						SELECT @numChildAllocation=ISNULL(numAllocation,0) FROM WareHouseItems WHERE numWareHouseItemID=@numChildWarehouseItemID

						-- IF CHILD ITEM IS KIT THEN LOOK OUT FOR ITS CHILD ITEMS WHICH ARE STORED IN OpportunityKitChildItems TABLE
						IF @bitChildIsKit = 1
						BEGIN
							-- IF KIT IS ADDED WITHIN KIT THEN THIS SUB KIT HAS ITS OWN ITEM CONFIGURATION WHICH ID STORED IN OpportunityKitChildItems
							IF EXISTS (SELECT numOppKitChildItemID FROM OpportunityKitChildItems WHERE numOppID=@numOppId AND numOppItemID=@numOppItemID AND numOppChildItemID=@numOppChildItemID)
							BEGIN
								-- CLEAR DATA OF PREVIOUS ITERATION
								DELETE FROM @TempKitChildKitSubItems

								-- GET SUB KIT SUB ITEMS DETAIL
								INSERT INTO @TempKitChildKitSubItems
								(
									ID,
									numOppKitChildItemID,
									numKitChildItemCode,
									numKitChildWarehouseItemID,
									numKitChildQty,
									numKitChildQtyShipped
								)
								SELECT 
									ROW_NUMBER() OVER(ORDER BY numOppKitChildItemID),
									ISNULL(numOppKitChildItemID,0),
									ISNULL(OKCI.numItemID,0),
									ISNULL(OKCI.numWareHouseItemId,0),
									((ISNULL(OKCI.numQtyItemsReq_Orig,0) * ISNULL(dbo.fn_UOMConversion(OKCI.numUOMId,OKCI.numItemID,@numDomainID,I.numBaseUnit),1)) * @numChildQty),
									ISNULL(numQtyShipped,0)
								FROM 
									OpportunityKitChildItems OKCI 
								JOIN 
									Item I 
								ON 
									OKCI.numItemID=I.numItemCode    
								WHERE 
									charitemtype='P' 
									AND ISNULL(numWareHouseItemId,0) > 0 
									AND OKCI.numOppID=@numOppID 
									AND OKCI.numOppItemID=@numOppItemID 
									AND OKCI.numOppChildItemID = @numOppChildItemID

								SET @k = 1
								SELECT @CountKitChildItems=COUNT(*) FROM @TempKitChildKitSubItems

								WHILE @k <= @CountKitChildItems
								BEGIN
									SELECT
										@numOppKitChildItemID=numOppKitChildItemID,
										@numKitChildItemCode=numKitChildItemCode,
										@numKitChildWarehouseItemID=numKitChildWarehouseItemID,
										@numKitChildQty=numKitChildQty,
										@numKitChildQtyShipped=numKitChildQtyShipped
									FROM
										@TempKitChildKitSubItems
									WHERE 
										ID = @k

									SELECT @numKitChildAllocation=ISNULL(numAllocation,0) FROM WareHouseItems WHERE numWareHouseItemID=@numKitChildWarehouseItemID

									--REMOVE QTY FROM ALLOCATION
									SET @numKitChildAllocation = @numKitChildAllocation - @numKitChildQty

									IF @numKitChildAllocation >= 0
									BEGIN
										UPDATE  
											WareHouseItems
										SET     
											numAllocation = @numKitChildAllocation,
											dtModified = GETDATE() 
										WHERE   
											numWareHouseItemID = @numKitChildWarehouseItemID      	
							
										UPDATE  
											OpportunityKitChildItems
										SET 
											numQtyShipped = ISNULL(numQtyShipped,0) + ISNULL(@numKitChildQty,0)
										WHERE   
											numOppKitChildItemID = @numOppKitChildItemID
											AND numOppChildItemID = @numOppChildItemID 
											AND numOppId=@numOppId
											AND numOppItemID=@numOppItemID

										SET @vcKitChildDescription = 'SO CHILD KIT Qty Shipped (Qty:' + CAST(@numKitChildQty AS VARCHAR(10)) + ' Shipped:' +  CAST(@numKitChildQtyShipped AS VARCHAR(10)) + ')'

										EXEC dbo.USP_ManageWareHouseItems_Tracking
											@numWareHouseItemID = @numKitChildWarehouseItemID,
											@numReferenceID = @numOppId,
											@tintRefType = 3,
											@vcDescription = @vcKitChildDescription,
											@numModifiedBy = @numUserCntID,
											@numDomainID = @numDomainID	 
									END
									ELSE
									BEGIN
										RAISERROR ('NOTSUFFICIENTQTY_ALLOCATION',16,1);
									END

									SET @k = @k + 1
								END
							END

							UPDATE  
								OpportunityKitItems
							SET 
								numQtyShipped = ISNULL(numQtyShipped,0) + ISNULL(@numChildQty,0)
							WHERE   
								numOppChildItemID = @numOppChildItemID 
								AND numOppId=@numOppId
								AND numOppItemID=@numOppItemID

							SET @vcChildDescription = 'SO KIT Qty Shipped (Qty:' + CAST(@numChildQty AS VARCHAR(10)) + ' Shipped:' +  CAST(@numChildQtyShipped AS VARCHAR(10)) + ')'

							EXEC dbo.USP_ManageWareHouseItems_Tracking
								@numWareHouseItemID = @numChildWarehouseItemID,
								@numReferenceID = @numOppId,
								@tintRefType = 3,
								@vcDescription = @vcChildDescription,
								@numModifiedBy = @numUserCntID,
								@numDomainID = @numDomainID	 
						END
						ELSE 
						BEGIN
							--REMOVE QTY FROM ALLOCATION
							SET @numChildAllocation = @numChildAllocation - @numChildQty
							IF @numChildAllocation >= 0
							BEGIN
								UPDATE  
									WareHouseItems
								SET     
									numAllocation = @numChildAllocation,
									dtModified = GETDATE() 
								WHERE   
									numWareHouseItemID = @numChildWarehouseItemID      	
							
								UPDATE  
									OpportunityKitItems
								SET 
									numQtyShipped = ISNULL(numQtyShipped,0) + ISNULL(@numChildQty,0)
								WHERE   
									numOppChildItemID = @numOppChildItemID 
									AND numOppId=@numOppId
									AND numOppItemID=@numOppItemID

								SET @vcChildDescription = 'SO KIT Qty Shipped (Qty:' + CAST(@numChildQty AS VARCHAR(10)) + ' Shipped:' +  CAST(@numChildQtyShipped AS VARCHAR(10)) + ')'

								EXEC dbo.USP_ManageWareHouseItems_Tracking
									@numWareHouseItemID = @numChildWarehouseItemID,
									@numReferenceID = @numOppId,
									@tintRefType = 3,
									@vcDescription = @vcChildDescription,
									@numModifiedBy = @numUserCntID,
									@numDomainID = @numDomainID	 
							END
							ELSE
							BEGIN
								RAISERROR ('NOTSUFFICIENTQTY_ALLOCATION',16,1);
							END
						END

						SET @j = @j + 1
					END
				END
				ELSE
				BEGIN
					SELECT @numAllocation = ISNULL(numAllocation, 0) FROM WareHouseItems WHERE numWareHouseItemID = @numWareHouseItemID
				       
					--REMOVE QTY FROM ALLOCATION
					SET @numAllocation = @numAllocation - @numQty	
				       
					IF (@numAllocation >= 0 )
					BEGIN
						UPDATE  
							WareHouseItems
						SET     
							numAllocation = @numAllocation,
							dtModified = GETDATE() 
						WHERE   
							numWareHouseItemID = @numWareHouseItemID      	

						IF @bitSerial = 1
						BEGIN								
							DELETE FROM OppWarehouseSerializedItem WHERE numOppID=@numOppID AND numOppItemID=@numOppItemID AND numWarehouseItmsID=@numWarehouseItemID

							INSERT INTO OppWarehouseSerializedItem
							(
								numWarehouseItmsDTLID,
								numOppID,
								numOppItemID,
								numWarehouseItmsID,
								numQty,
								numOppBizDocsId	
							)
							SELECT
								(SELECT TOP 1 numWarehouseItmsDTLID FROM WareHouseItmsDTL WHERE numWareHouseItemID=@numWarehouseItemID AND vcSerialNo=OutParam AND  numQty = 1),
								@numOppID,
								@numOppItemID,
								@numWarehouseItemID,
								1,
								@numOppBizDocID
							FROM
								dbo.SplitString(@vcSerialLot,',')


							UPDATE WareHouseItmsDTL SET numQty=0 WHERE numWareHouseItemID=@numWarehouseItemID AND vcSerialNo IN (SELECT OutParam FROM dbo.SplitString(@vcSerialLot,','))
						END

						IF @bitLot = 1
						BEGIN
							DELETE FROM @TempLot

							INSERT INTO
								@TempLot
							SELECT
								SUBSTRING(OutParam,0,PATINDEX('%(%', OutParam)),
								CAST(SUBSTRING(OutParam,(PATINDEX('%(%', OutParam) + 1),(PATINDEX('%)%', OutParam) - PATINDEX('%(%', OutParam) - 1)) AS INT)
							FROM
								dbo.SplitString(@vcSerialLot,',')

							DELETE FROM OppWarehouseSerializedItem WHERE numOppID=@numOppID AND numOppItemID=@numOppItemID AND numWarehouseItmsID=@numWarehouseItemID

							INSERT INTO OppWarehouseSerializedItem
							(
								numWarehouseItmsDTLID,
								numOppID,
								numOppItemID,
								numWarehouseItmsID,
								numQty,
								numOppBizDocsId	
							)
							SELECT
								(SELECT TOP 1 numWarehouseItmsDTLID FROM WareHouseItmsDTL WHERE numWareHouseItemID=@numWarehouseItemID AND vcSerialNo=TEMPLOT.vcLotNo AND numQty >= TEMPLOT.numQty),
								@numOppID,
								@numOppItemID,
								@numWarehouseItemID,
								TEMPLOT.numQty,
								@numOppBizDocID
							FROM
								@TempLot TEMPLOT


							UPDATE	TEMPWareHouseItmsDTL
								SET TEMPWareHouseItmsDTL.numQty = ISNULL(TEMPWareHouseItmsDTL.numQty,0) - ISNULL(TEMPLOT.numQty,0)
							FROM
								WareHouseItmsDTL TEMPWareHouseItmsDTL
							INNER JOIN
								@TempLot TEMPLOT
							ON
								TEMPWareHouseItmsDTL.vcSerialNo = TEMPLOT.vcLotNo
							WHERE
								TEMPWareHouseItmsDTL.numWareHouseItemID = @numWarehouseItemID
						END
					END
					ELSE
					BEGIN
						PRINT 'PARENT: ' + CAST(@numChildWarehouseItemID AS VARCHAR)
						RAISERROR ('NOTSUFFICIENTQTY_ALLOCATION',16,1);
					END
				END

				EXEC dbo.USP_ManageWareHouseItems_Tracking
							@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
							@numReferenceID = @numOppId, --  numeric(9, 0)
							@tintRefType = 3, --  tinyint
							@vcDescription = @vcDescription, --  varchar(100)
							@numModifiedBy = @numUserCntID,
							@numDomainID = @numDomainID	 
			END

			UPDATE  
				OpportunityItems
			SET 
				numQtyShipped = ISNULL(numQtyShipped,0) + ISNULL(@numQty,0)
			WHERE   
				numoppitemtCode = @numOppItemID 
				AND numOppId=@numOppId

			SET @i = @i + 1
		END

		UPDATE OpportunityBizDocs SET bitFulfilled = 1,dtShippedDate=@dtShippedDate WHERE numOppBizDocsId=@numOppBizDocID
	COMMIT TRANSACTION
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
END

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityBizDocs_RevertFulFillment')
DROP PROCEDURE USP_OpportunityBizDocs_RevertFulFillment
GO
CREATE PROCEDURE [dbo].[USP_OpportunityBizDocs_RevertFulFillment] 
	@numDomainID NUMERIC(18,0),
	@numUserCntID NUMERIC(18,0),
	@numOppId NUMERIC(18,0),
	@numOppBizDocID NUMERIC(18,0)
AS  
BEGIN 
BEGIN TRY

	DECLARE @vcItemType AS CHAR(1)
	DECLARE @bitDropShip AS BIT
	DECLARE @bitSerial AS BIT
	DECLARE @bitLot AS BIT
	DECLARE @bitAssembly AS BIT
	DECLARE @bitKit AS BIT
	DECLARE @numOppItemID AS INT
	DECLARE @numWarehouseItemID AS INT
	DECLARE @numQty AS FLOAT
	DECLARE @numQtyShipped AS FLOAT
	DECLARE @vcDescription AS VARCHAR(300)
	DECLARE @numAllocation AS FLOAT


	DECLARE @j INT = 0
	DECLARE @ChildCount AS INT
	DECLARE @vcChildDescription AS VARCHAR(300)
	DECLARE @numChildAllocation AS FLOAT
	DECLARE @numOppChildItemID AS INT
	DECLARE @numChildItemCode AS INT
	DECLARE @bitChildIsKit AS BIT
	DECLARE @numChildWarehouseItemID AS INT
	DECLARE @numChildQty AS FLOAT
	DECLARE @numChildQtyShipped AS FLOAT

	DECLARE @TempKitSubItems TABLE
	(
		ID INT,
		numOppChildItemID NUMERIC(18,0),
		numChildItemCode NUMERIC(18,0),
		bitChildIsKit BIT,
		numChildWarehouseItemID NUMERIC(18,0),
		numChildQty FLOAT,
		numChildQtyShipped FLOAT
	)

	DECLARE @k AS INT = 1
	DECLARE @CountKitChildItems AS INT
	DECLARE @vcKitChildDescription AS VARCHAR(300)
	DECLARE @numKitChildAllocation AS FLOAT
	DECLARE @numOppKitChildItemID AS NUMERIC(18,0)
	DECLARE @numKitChildItemCode AS NUMERIC(18,0)
	DECLARE @numKitChildWarehouseItemID AS NUMERIC(18,0)
	DECLARE @numKitChildQty AS FLOAT
	DECLARE @numKitChildQtyShipped AS FLOAT

	DECLARE @TempKitChildKitSubItems TABLE
	(
		ID INT,
		numOppKitChildItemID NUMERIC(18,0),
		numKitChildItemCode NUMERIC(18,0),
		numKitChildWarehouseItemID NUMERIC(18,0),
		numKitChildQty FLOAT,
		numKitChildQtyShipped FLOAT
	)

	DECLARE @TempFinalTable TABLE
	(
		ID INT IDENTITY(1,1),
		numItemCode NUMERIC(18,0),
		vcItemType CHAR(1),
		bitSerial BIT,
		bitLot BIT,
		bitAssembly BIT,
		bitKit BIT,
		numOppItemID NUMERIC(18,0),
		numWarehouseItemID NUMERIC(18,0),
		numQtyShipped FLOAT,
		numQty FLOAT,
		bitDropShip BIT,
		vcSerialLot VARCHAR(MAX)
	)

	INSERT INTO @TempFinalTable
	(
		numItemCode,
		vcItemType,
		bitSerial,
		bitLot,
		bitAssembly,
		bitKit,
		numOppItemID,
		numWarehouseItemID,
		numQtyShipped,
		numQty,
		bitDropShip,
		vcSerialLot
	)
	SELECT
		OpportunityBizDocItems.numItemCode,
		ISNULL(Item.charItemType,''),
		ISNULL(Item.bitSerialized,0),
		ISNULL(Item.bitLotNo,0),
		ISNULL(Item.bitAssembly,0),
		ISNULL(Item.bitKitParent,0),
		ISNULL(OpportunityItems.numoppitemtcode,0),
		ISNULL(OpportunityBizDocItems.numWarehouseItmsID,0),
		ISNULL(OpportunityItems.numQtyShipped,0),
		ISNULL(OpportunityBizDocItems.numUnitHour,0),
		ISNULL(OpportunityItems.bitDropShip,0),
		''
	FROM
		OpportunityBizDocs
	INNER JOIN
		OpportunityBizDocItems
	ON
		OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
	INNER JOIN
		OpportunityItems
	ON
		OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtCode
	INNER JOIN
		Item
	ON
		OpportunityBizDocItems.numItemCode = Item.numItemCode
	WHERE
		numOppBizDocsId = @numOppBizDocID
		AND ISNULL(OpportunityItems.bitDropShip,0) = 0

	BEGIN TRANSACTION
		DECLARE @i INT = 1
		DECLARE @COUNT INT = 0
		SELECT @COUNT = COUNT(*) FROM @TempFinalTable

		WHILE @i <= @COUNT
		BEGIN
			SELECT 
				@vcItemType=vcItemType,
				@bitSerial=bitSerial,
				@bitLot=bitLot,
				@bitAssembly=bitAssembly,
				@bitKit=bitKit,
				@bitDropShip=bitDropShip,
				@numOppItemID=numOppItemID,
				@numQty=numQty,
				@numQtyShipped = numQtyShipped,
				@numWarehouseItemID=numWarehouseItemID
			FROM 
				@TempFinalTable 
			WHERE 
				ID = @i

			IF @numQty > @numQtyShipped
			BEGIN
				RAISERROR ('INVALID_SHIPPED_QTY',16,1);
			END

			--REVERT QUANTITY TO ON ALLOCATION IF INVENTORY ITEM
			IF (UPPER(@vcItemType) = 'P' AND @bitDropShip = 0)
			BEGIN
				SET @vcDescription='SO Qty Shipped Return (Qty:' + CAST(@numQty AS VARCHAR(10)) + ' Shipped:' +  CAST(@numQtyShipped AS VARCHAR(10)) + ')'
				
				IF @bitKit = 1
				BEGIN
					-- CLEAR DATA OF PREVIOUS ITERATION
					DELETE FROM @TempKitSubItems

					-- GET KIT SUB ITEMS DETAIL
					INSERT INTO @TempKitSubItems
					(
						ID,
						numOppChildItemID,
						numChildItemCode,
						bitChildIsKit,
						numChildWarehouseItemID,
						numChildQty,
						numChildQtyShipped
					)
					SELECT 
						ROW_NUMBER() OVER(ORDER BY numOppChildItemID),
						ISNULL(numOppChildItemID,0),
						ISNULL(I.numItemCode,0),
						ISNULL(I.bitKitParent,0),
						ISNULL(OKI.numWareHouseItemId,0),
						((ISNULL(OKI.numQtyItemsReq_Orig,0) * ISNULL(dbo.fn_UOMConversion(OKI.numUOMId,OKI.numChildItemID,@numDomainID,I.numBaseUnit),1)) * @numQty),
						ISNULL(OKI.numQtyShipped,0)
					FROM 
						OpportunityKitItems OKI 
					JOIN 
						Item I 
					ON 
						OKI.numChildItemID=I.numItemCode    
					WHERE 
						charitemtype='P' 
						AND ISNULL(numWareHouseItemId,0) > 0 
						AND OKI.numOppID=@numOppID 
						AND OKI.numOppItemID=@numOppItemID 

					SET @j = 1
					SELECT @ChildCount=COUNT(*) FROM @TempKitSubItems

					--LOOP OVER ALL CHILD ITEMS AND REVERT ALLOCATION
					WHILE @j <= @ChildCount
					BEGIN
					
						SELECT
							@numOppChildItemID=numOppChildItemID,
							@numChildItemCode=numChildItemCode,
							@bitChildIsKit=bitChildIsKit,
							@numChildWarehouseItemID=numChildWarehouseItemID,
							@numChildQty=numChildQty,
							@numChildQtyShipped=numChildQtyShipped
						FROM
							@TempKitSubItems
						WHERE 
							ID = @j

						IF @numChildQty > @numChildQtyShipped
						BEGIN
							RAISERROR ('INVALID_KIT_SUB_ITEM_SHIPPED_QTY',16,1);
						END

						SELECT @numChildAllocation=ISNULL(numAllocation,0) FROM WareHouseItems WHERE numWareHouseItemID=@numChildWarehouseItemID

						IF @bitChildIsKit = 1
						BEGIN
							-- IF KIT IS ADDED WITHIN KIT THEN THIS SUB KIT HAS ITS OWN ITEM CONFIGURATION WHICH ID STORED IN OpportunityKitChildItems
							IF EXISTS (SELECT numOppKitChildItemID FROM OpportunityKitChildItems WHERE numOppID=@numOppId AND numOppItemID=@numOppItemID AND numOppChildItemID=@numOppChildItemID)
							BEGIN
								-- CLEAR DATA OF PREVIOUS ITERATION
								DELETE FROM @TempKitChildKitSubItems

								-- GET SUB KIT SUB ITEMS DETAIL
								INSERT INTO @TempKitChildKitSubItems
								(
									ID,
									numOppKitChildItemID,
									numKitChildItemCode,
									numKitChildWarehouseItemID,
									numKitChildQty,
									numKitChildQtyShipped
								)
								SELECT 
									ROW_NUMBER() OVER(ORDER BY numOppKitChildItemID),
									ISNULL(numOppKitChildItemID,0),
									ISNULL(OKCI.numItemID,0),
									ISNULL(OKCI.numWareHouseItemId,0),
									((ISNULL(OKCI.numQtyItemsReq_Orig,0) * ISNULL(dbo.fn_UOMConversion(OKCI.numUOMId,OKCI.numItemID,@numDomainID,I.numBaseUnit),1)) * @numChildQty),
									ISNULL(numQtyShipped,0)
								FROM 
									OpportunityKitChildItems OKCI 
								JOIN 
									Item I 
								ON 
									OKCI.numItemID=I.numItemCode    
								WHERE 
									charitemtype='P' 
									AND ISNULL(numWareHouseItemId,0) > 0 
									AND OKCI.numOppID=@numOppID 
									AND OKCI.numOppItemID=@numOppItemID 
									AND OKCI.numOppChildItemID = @numOppChildItemID

								SET @k = 1
								SELECT @CountKitChildItems=COUNT(*) FROM @TempKitChildKitSubItems


								--LOOP OVER ALL CHILD ITEMS AND REVERT ALLOCATION
								WHILE @k <= @CountKitChildItems
								BEGIN
									SELECT
										@numOppKitChildItemID=numOppKitChildItemID,
										@numKitChildItemCode=numKitChildItemCode,
										@numKitChildWarehouseItemID=numKitChildWarehouseItemID,
										@numKitChildQty=numKitChildQty,
										@numKitChildQtyShipped=numKitChildQtyShipped
									FROM
										@TempKitChildKitSubItems
									WHERE 
										ID = @k

									IF @numKitChildQty > @numKitChildQtyShipped
									BEGIN
										RAISERROR ('INVALID_KIT_SUB_ITEM_SHIPPED_QTY',16,1);
									END

									SELECT @numKitChildAllocation=ISNULL(numAllocation,0) FROM WareHouseItems WHERE numWareHouseItemID=@numKitChildWarehouseItemID

									--PLACE QTY BACK ON ALLOCATION
									SET @numKitChildAllocation = @numKitChildAllocation + @numKitChildQty
						
									-- UPDATE WAREHOUSE INVENTORY
									UPDATE  
										WareHouseItems
									SET     
										numAllocation = @numKitChildAllocation,
										dtModified = GETDATE() 
									WHERE   
										numWareHouseItemID = @numKitChildWarehouseItemID      	
						
									--DECREASE QTY SHIPPED OF ORDER ITEM	
									UPDATE  
										OpportunityKitChildItems
									SET 
										numQtyShipped = ISNULL(numQtyShipped,0) - ISNULL(@numKitChildQty,0)
									WHERE   
										numOppKitChildItemID = @numOppKitChildItemID
										AND numOppChildItemID = @numOppChildItemID 
										AND numOppId=@numOppId
										AND numOppItemID=@numOppItemID

									SET @vcKitChildDescription = 'SO CHILD KIT Qty Shipped Return (Qty:' + CAST(@numKitChildQty AS VARCHAR(10)) + ' Shipped:' +  CAST(@numKitChildQtyShipped AS VARCHAR(10)) + ')'

									-- CREATE WAREHOUSE INVENTORY TRACKING ENTRY
									EXEC dbo.USP_ManageWareHouseItems_Tracking
										@numWareHouseItemID = @numKitChildWarehouseItemID,
										@numReferenceID = @numOppId,
										@tintRefType = 3,
										@vcDescription = @vcKitChildDescription,
										@numModifiedBy = @numUserCntID,
										@numDomainID = @numDomainID	 
									
									SET @k = @k + 1
								END
							END

							--DECREASE QTY SHIPPED OF ORDER ITEM	
							UPDATE  
								OpportunityKitItems
							SET 
								numQtyShipped = ISNULL(numQtyShipped,0) - ISNULL(@numChildQty,0)
							WHERE   
								numOppChildItemID = @numOppChildItemID 
								AND numOppId=@numOppId
								AND numOppItemID=@numOppItemID

							SET @vcChildDescription = 'SO KIT Qty Shipped Return (Qty:' + CAST(@numChildQty AS VARCHAR(10)) + ' Shipped:' +  CAST(@numChildQtyShipped AS VARCHAR(10)) + ')'

							-- CREATE WAREHOUSE INVENTORY TRACKING ENTRY
							EXEC dbo.USP_ManageWareHouseItems_Tracking
								@numWareHouseItemID = @numChildWarehouseItemID,
								@numReferenceID = @numOppId,
								@tintRefType = 3,
								@vcDescription = @vcChildDescription,
								@numModifiedBy = @numUserCntID,
								@numDomainID = @numDomainID	 
						END
						ELSE
						BEGIN
							
							--PLACE QTY BACK ON ALLOCATION
							SET @numChildAllocation = @numChildAllocation + @numChildQty
						
							-- UPDATE WAREHOUSE INVENTORY
							UPDATE  
								WareHouseItems
							SET     
								numAllocation = @numChildAllocation,
								dtModified = GETDATE() 
							WHERE   
								numWareHouseItemID = @numChildWarehouseItemID      	
						
							--DECREASE QTY SHIPPED OF ORDER ITEM	
							UPDATE  
								OpportunityKitItems
							SET 
								numQtyShipped = ISNULL(numQtyShipped,0) - ISNULL(@numChildQty,0)
							WHERE   
								numOppChildItemID = @numOppChildItemID 
								AND numOppId=@numOppId
								AND numOppItemID=@numOppItemID

							SET @vcChildDescription = 'SO KIT Qty Shipped Return (Qty:' + CAST(@numChildQty AS VARCHAR(10)) + ' Shipped:' +  CAST(@numChildQtyShipped AS VARCHAR(10)) + ')'

							-- CREATE WAREHOUSE INVENTORY TRACKING ENTRY
							EXEC dbo.USP_ManageWareHouseItems_Tracking
								@numWareHouseItemID = @numChildWarehouseItemID,
								@numReferenceID = @numOppId,
								@tintRefType = 3,
								@vcDescription = @vcChildDescription,
								@numModifiedBy = @numUserCntID,
								@numDomainID = @numDomainID	
						END

						SET @j = @j + 1
					END
				END
				ELSE
				BEGIN
					
					SELECT @numAllocation = ISNULL(numAllocation, 0) FROM WareHouseItems WHERE numWareHouseItemID = @numWareHouseItemID
				       
					--PLACE QTY BACK ON ALLOCATION
					SET @numAllocation = @numAllocation + @numQty	

					-- UPDATE WAREHOUSE INVENTORY
					UPDATE  
						WareHouseItems
					SET     
						numAllocation = @numAllocation
						,dtModified = GETDATE() 
					WHERE   
						numWareHouseItemID = @numWareHouseItemID      	

					IF @bitSerial = 1
					BEGIN 
						--INCREASE SERIAL NUMBER
						UPDATE 
							WareHouseItmsDTL 
						SET 
							numQty=1 
						WHERE 
							numWareHouseItmsDTLID IN (
								SELECT 
									ISNULL(OWSI.numWareHouseItmsDTLID ,0)
								FROM 
									OppWarehouseSerializedItem OWSI
								WHERE 
									OWSI.numOppID = @numOppID
									AND OWSI.numOppItemID =  @numOppItemID
									AND OWSI.numWarehouseItmsID = @numWarehouseItemID
									AND OWSI.numOppBizDocsId = @numOppBizDocID
							)

						-- DELETE SERIAL NUMBER ENTERIES ASSOCIATED WITH BIZDOC
						DELETE FROM OppWarehouseSerializedItem WHERE numOppID=@numOppID AND numOppItemID= @numOppItemID AND numWarehouseItmsID=@numWarehouseItemID AND numOppBizDocsId=@numOppBizDocID		
					END

					IF @bitLot = 1
					BEGIN
						-- INCREASE LOT NOT QTY
						UPDATE WHDL
							SET WHDL.numQty = ISNULL(WHDL.numQty,0) + ISNULL(OWSI.numQty,0)
						FROM
							WareHouseItmsDTL  WHDL
						INNER JOIN
							OppWarehouseSerializedItem OWSI
						ON
							WHDL.numWareHouseItmsDTLID = OWSI.numWarehouseItmsDTLID
							AND OWSI.numOppID = @numOppID
							AND OWSI.numOppItemID =  @numOppItemID
							AND OWSI.numWarehouseItmsID = @numWarehouseItemID
							AND OWSI.numOppBizDocsId = @numOppBizDocID

						-- DELETE SERIAL NUMBER ENTERIES ASSOCIATED WITH BIZDOC
						DELETE FROM OppWarehouseSerializedItem WHERE numOppID=@numOppID AND numOppItemID= @numOppItemID AND numWarehouseItmsID=@numWarehouseItemID AND numOppBizDocsId=@numOppBizDocID
					END
				END

				-- CREATE WAREHOUSE INVENTORY TRACKING ENTRY
				EXEC dbo.USP_ManageWareHouseItems_Tracking
							@numWareHouseItemID = @numWareHouseItemID,
							@numReferenceID = @numOppId,
							@tintRefType = 3,
							@vcDescription = @vcDescription,
							@numModifiedBy = @numUserCntID,
							@numDomainID = @numDomainID	 
				
			END

			--DECREASE QTY SHIPPED OF ORDER ITEM	
			UPDATE 
				OpportunityItems
			SET     
				numQtyShipped = (ISNULL(numQtyShipped,0) - @numQty)
			WHERE   
				numoppitemtCode = @numOppItemID 
				AND numOppId=@numOppId

			SET @i = @i + 1
		END
	COMMIT TRANSACTION 
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRAN

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by Siva                                                                                                            
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityKitItemsForAuthorizativeAccounting')
DROP PROCEDURE USP_OpportunityKitItemsForAuthorizativeAccounting
GO
CREATE PROCEDURE  [dbo].[USP_OpportunityKitItemsForAuthorizativeAccounting]                                                                                                                                                
(                                                                                                                                                
 @numOppId as numeric(9)=null,                                                                                                                                                
 @numOppBizDocsId as numeric(9)=null                                                              
)                                                                                                                                                
                                                                                                                                            
as                           
BEGIN
     --OpportunityKitItems
    SELECT 
		I.[vcItemName] as Item,        
		charitemType as type,        
		'' as [desc],        
		(ISNULL(OBI.numUnitHour,0) * (OKI.numQtyItemsReq_Orig * ISNULL(dbo.fn_UOMConversion(OKI.numUOMId,OKI.numChildItemID,i.numDomainID,i.numBaseUnit),1))) as Unit,        
		0 as Price,        
		0 as Amount,0 AS ItemTotalAmount,       
		0 as listPrice,
		CONVERT(VARCHAR,i.numItemCode) as ItemCode,
		numOppChildItemID AS numoppitemtCode,        
		L.vcdata as vcItemClassification,
		CASE WHEN bitTaxable=0 THEN 'No' ELSE 'Yes' END AS Taxable,
		0 AS monListPrice,
		ISNULL(i.numIncomeChartAcntId,0) AS itemIncomeAccount,
		ISNULL(i.numAssetChartAcntId,0) as itemInventoryAsset,
		'NI' as ItemType,
		ISNULL(i.numCOGsChartAcntId,0) AS itemCoGs,
		ISNULL(i.monAverageCost,'0') AS AverageCost,
		ISNULL(OBI.bitDropShip,0) AS bitDropShip,  
		0 as DiscAmt,
		NULLIF(Opp.numProjectID,0) numProjectID,
		NULLIF(Opp.numClassID,0) numClassID 
	FROM  
		OpportunityItems opp 
	JOIN 
		OpportunityBizDocItems OBI 
	ON 
		OBI.numOppItemID=Opp.numoppitemtCode       
	JOIN 
		OpportunityKitItems OKI 
	ON 
		OKI.numOppItemID=opp.numoppitemtCode 
		AND Opp.numOppId=OKI.numOppId
	LEFT JOIN 
		Item i 
	ON 
		OKI.numChildItemID=i.numItemCode        
	LEFT JOIN 
		ListDetails L 
	ON 
		i.numItemClassification=L.numListItemID        
	WHERE 
		Opp.numOppId=@numOppId 
		AND OBI.numOppBizDocID=@numOppBizDocsId
		AND ISNULL(I.bitKitParent,0) = 0
	UNION ALL
	SELECT 
		I.[vcItemName] as Item,        
		charitemType as type,        
		'' as [desc],        
		(ISNULL(OBI.numUnitHour,0) * ISNULL(OKI.numQtyItemsReq_Orig,0) * (ISNULL(OKCI.numQtyItemsReq_Orig,0) * ISNULL(dbo.fn_UOMConversion(OKCI.numUOMId,OKCI.numItemID,i.numDomainID,i.numBaseUnit),1))) as Unit,        
		0 as Price,        
		0 as Amount,0 AS ItemTotalAmount,       
		0 as listPrice,
		CONVERT(VARCHAR,i.numItemCode) as ItemCode,
		OKI.numOppChildItemID AS numoppitemtCode,        
		L.vcdata as vcItemClassification,
		CASE WHEN bitTaxable=0 THEN 'No' ELSE 'Yes' END AS Taxable,
		0 AS monListPrice,
		ISNULL(i.numIncomeChartAcntId,0) AS itemIncomeAccount,
		ISNULL(i.numAssetChartAcntId,0) as itemInventoryAsset,
		'NI' as ItemType,
		ISNULL(i.numCOGsChartAcntId,0) AS itemCoGs,
		ISNULL(i.monAverageCost,'0') AS AverageCost,
		ISNULL(OBI.bitDropShip,0) AS bitDropShip,  
		0 as DiscAmt,
		NULLIF(Opp.numProjectID,0) numProjectID,
		NULLIF(Opp.numClassID,0) numClassID 
	FROM  
		OpportunityKitChildItems OKCI 
	INNER JOIN
		OpportunityKitItems OKI
	ON
		OKCI.numOppChildItemID = OKI.numOppChildItemID
	INNER JOIN
		OpportunityItems opp 
	ON
		OKCI.numOppItemID = opp.numoppitemtCode
		AND OKCI.numOppID = @numOppId 
	INNER JOIN 
		OpportunityBizDocItems OBI 
	ON 
		OBI.numOppItemID=Opp.numoppitemtCode       	
	LEFT JOIN 
		Item i 
	ON 
		OKCI.numItemID=i.numItemCode        
	LEFT JOIN 
		ListDetails L 
	ON 
		i.numItemClassification=L.numListItemID        
	WHERE 
		OKCI.numOppId=@numOppId 
		AND OBI.numOppBizDocID=@numOppBizDocsId
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityMaster_CreatePO')
DROP PROCEDURE dbo.USP_OpportunityMaster_CreatePO
GO
CREATE PROCEDURE [dbo].[USP_OpportunityMaster_CreatePO]
(
	@numOppID NUMERIC(18,0) OUTPUT,
	@numDomainID NUMERIC(18,0),
	@numUserCntID NUMERIC(18,0),
	@numItemCode NUMERIC(18,0),
	@numUnitHour FLOAT,
	@numWarehouseItemID NUMERIC(18,0),
	@bitFromWorkOrder BIT,
	@tintOppStatus TINYINT,
	@numOrderStatus AS NUMERIC(18,0) = 0
)
AS 
BEGIN
    DECLARE @ErrorMessage NVARCHAR(4000);SET @ErrorMessage=''
    DECLARE @ErrorSeverity INT;
    DECLARE @ErrorState INT;
    
	BEGIN TRY
	BEGIN TRAN
		DECLARE @tintMinOrderQty AS INTEGER
		DECLARE @numUnitPrice MONEY
		DECLARE @numContactID NUMERIC(18,0)
	    DECLARE @numDivisionID NUMERIC(18,0)
		DECLARE @numVendorID NUMERIC(18,0)
		DECLARE @fltExchangeRate float    
		DECLARE @numCurrencyID NUMERIC(18,0)
		DECLARE @vcPOppName VARCHAR(1000) = ''                         
		DECLARE @hDocItem int                                                                                                                            
		DECLARE @TotalAmount as money                                                                                     

		SELECT @numDivisionID = numVendorID FROM Item WHERE numDomainID=@numDomainID AND numItemCode=@numItemCode

		IF ISNULL(@numDivisionID,0) > 0
		BEGIN
			SELECT @numContactID = numContactId FROM AdditionalContactsInformation WHERE numDivisionId = @numDivisionID AND bitPrimaryContact = 1

			IF @numContactID IS NULL
			BEGIN
				SELECT TOP 1 @numContactID = numContactId FROM AdditionalContactsInformation WHERE numDivisionId = @numDivisionID
			END

			SELECT @tintMinOrderQty=ISNULL(intMinQty,0) FROm Vendor WHERE numVendorID = @numDivisionID AND numItemCode = @numItemCode AND numDomainID=@numDomainID
			SET @numUnitHour = @numUnitHour + @tintMinOrderQty
			SET @numUnitHour = IIF(@numUnitHour = 0, 1, @numUnitHour)

			SELECT @numUnitPrice = dbo.fn_FindVendorCost(@numItemCOde,@numDivisionID,@numDomainID,@numUnitHour)

		
			DECLARE @intOppTcode AS NUMERIC(9)
			SELECT @intOppTcode=max(numOppId) FROM OpportunityMaster                
			SET @intOppTcode =isnull(@intOppTcode,0) + 1                                                
			SET @vcPOppName= @vcPOppName + '-' + convert(varchar(4),year(getutcdate()))  + '-A' + convert(varchar(10),@intOppTcode)                                  
  
			SET @fltExchangeRate=1
			SELECT @numCurrencyID= ISNULL(numCurrencyID,0) FROM dbo.Domain WHERE numDomainId=@numDomainId                                                                       
  
			-- GET ACCOUNT CLASS IF ENABLED
			DECLARE @numAccountClass AS NUMERIC(18) = 0

			DECLARE @tintDefaultClassType AS INT = 0
			SELECT @tintDefaultClassType = ISNULL(tintDefaultClassType,0) FROM Domain WHERE numDomainId = @numDomainID 

			IF @tintDefaultClassType = 1 --USER
			BEGIN
				SELECT 
					@numAccountClass=ISNULL(numDefaultClass,0) 
				FROM 
					dbo.UserMaster UM 
				JOIN 
					dbo.Domain D 
				ON 
					UM.numDomainID=D.numDomainId
				WHERE 
					D.numDomainId=@numDomainId 
					AND UM.numUserDetailId=@numUserCntID
			END
			ELSE IF @tintDefaultClassType = 2 --COMPANY
			BEGIN
				SELECT 
					@numAccountClass=ISNULL(numAccountClassID,0) 
				FROM 
					dbo.DivisionMaster DM 
				WHERE 
					DM.numDomainId=@numDomainId 
					AND DM.numDivisionID=@numDivisionID
			END
			ELSE
			BEGIN
				SET @numAccountClass = 0
			END
                                                                       
			INSERT INTO OpportunityMaster                                                                          
			(                                                                             
				numContactId,numDivisionId,bitPublicFlag,tintSource,
				tintSourceType,vcPOppName,intPEstimatedCloseDate,numCreatedBy,bintCreatedDate,                                                                           
				numDomainId,numRecOwner,lngPConclAnalysis,tintOppType,                                                              
				numSalesOrPurType,numCurrencyID,fltExchangeRate,[tintOppStatus],numStatus,
				vcOppRefOrderNo,bitStockTransfer,bitBillingTerms,intBillingDays,bitInterestType,fltInterest,vcCouponCode,
				bitDiscountType,fltDiscount,bitPPVariance,numAccountClass,tintTaxOperator,bitUseShippersAccountNo,bitUseMarkupShippingRate,
				numMarkupShippingRate,intUsedShippingCompany,bitFromWorkOrder
			)                                                                          
			Values                                                                          
			(                                                                          
				@numContactId,@numDivisionId,0,0,1,@vcPOppName,DATEADD(D,1,GETUTCDATE()),@numUserCntID,GETUTCDATE(),
				@numDomainId,@numUserCntID,0,2,0,@numCurrencyID,@fltExchangeRate,@tintOppStatus,
				@numOrderStatus,NULL,0,0,0,0,0,'',0,0,1,@numAccountClass,0,0,0,0.00,0,@bitFromWorkOrder
			)                                                                                                                      
  
			set @numOppID=scope_identity()                                                
  
			--UPDATE OPPNAME AS PER NAME TEMPLATE
			EXEC dbo.USP_UpdateNameTemplateValue 2,@numDomainID,@numOppID

			EXEC dbo.USP_AddParentChildCustomFieldMap
			@numDomainID = @numDomainID,
			@numRecordID = @numOppID,
			@numParentRecId = @numDivisionId,
			@tintPageID = 6
 	
			--INSERTING ITEMS                                       
			IF @numOppID>0
			BEGIN                      

				INSERT INTO OpportunityItems                                                                          
				(
					numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,
					vcType,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,vcItemName,vcModelID,
					vcManufacturer,vcPathForTImage,monVendorCost,numUOMId,bitWorkOrder,numVendorWareHouse,
					numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,numClassID,numToWarehouseItemID,
					vcAttributes,monAvgCost,bitItemPriceApprovalRequired
				)
				SELECT 
					@numOppID,@numItemCode,@numUnitHour,@numUnitPrice,(@numUnitPrice * @numUnitHour),Item.txtItemDesc,@numWarehouseItemID,
					'',0,0,0,(@numUnitPrice * @numUnitHour),Item.vcItemName,Item.vcModelID,Item.vcManufacturer,
					(SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = Item.numItemCode AND BitDefault = 1 AND numdomainId = @numdomainId),
					(select isnull(VN.monCost,0) from Vendor VN, Item IT where IT.numItemCode=VN.numItemCode and  VN.numItemCode=Item.numItemCode and VN.numVendorID=IT.numVendorID),
					Item.numBaseUnit,0,NULL,NULL,NULL,NULL,NULL,
					0,
					NULL,'', (SELECT ISNULL(monAverageCost,0) FROM Item I WHERE I.numDomainId=@numDomainId AND I.numItemCode=Item.numItemCode),0
				FROM
					Item
				WHERE
					numItemCode = @numItemCode
			END          
	
			SET @TotalAmount=0                                                           
			SELECT @TotalAmount=sum(monTotAmount) FROM OpportunityItems WHERE numOppId=@numOppID                                                                          
			UPDATE OpportunityMaster SET monPamount=@TotalAmount WHERE numOppId=@numOppID                                                 
                      
			--Insert Tax for Division   
			IF (select isnull(bitPurchaseTaxCredit,0) from Domain where numDomainId=@numDomainId)=1
			BEGIN 
				INSERT dbo.OpportunityMasterTaxItems 
				(
					numOppId,
					numTaxItemID,
					fltPercentage,
					tintTaxType
				) 
				SELECT 
					@numOppID,
					TI.numTaxItemID,
					TEMPTax.decTaxValue,
					TEMPTax.tintTaxType
				FROM 
					TaxItems TI 
				JOIN 
					DivisionTaxTypes DTT ON TI.numTaxItemID = DTT.numTaxItemID
				OUTER APPLY
				(
					SELECT
						decTaxValue,
						tintTaxType
					FROM
						dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,TI.numTaxItemID,@numOppId,0,NULL)
				) AS TEMPTax
				WHERE 
					DTT.numDivisionID=@numDivisionId AND DTT.bitApplicable=1
				UNION 
				SELECT
					@numOppID,
					0,
					TEMPTax.decTaxValue,
					TEMPTax.tintTaxType
				FROM 
					dbo.DivisionMaster 
				OUTER APPLY
				(
					SELECT
						decTaxValue,
						tintTaxType
					FROM
						dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,0,@numOppId,1,NULL)
				) AS TEMPTax
				WHERE 
					bitNoTax=0 AND 
					numDivisionID=@numDivisionID
			END

			/*Added by chintan - While creating SO from PO it wasn't puting Item Qty to Allocation. this segment was being called from USP_OppSaveMilestone which No longer in use after changes anoop did. */     
			--Updating the warehouse items              
			DECLARE @tintShipped AS TINYINT               
			DECLARE @tintOppType AS TINYINT
			DECLARE @DealStatus AS TINYINT
			SELECT 
				@tintOppStatus=tintOppStatus,
				@tintOppType=tintOppType,
				@tintShipped=tintShipped
			FROM 
				OpportunityMaster 
			WHERE 
				numOppID=@numOppID              
		
			IF @tintOppStatus=1               
			BEGIN         
				EXEC USP_UpdatingInventoryonCloseDeal @numOppID,@numUserCntID
			END

			DECLARE @tintCRMType as numeric(9)        
			DECLARE @AccountClosingDate as datetime        

			select @numDivisionID=numDivisionID,@AccountClosingDate=bintAccountClosingDate from  OpportunityMaster where numOppID=@numOppID         
			select @tintCRMType=tintCRMType from divisionmaster where numDivisionID =@numDivisionID 

			IF @AccountClosingDate is null                   
			UPDATE OpportunityMaster set bintAccountClosingDate=getutcdate() where numOppID=@numOppID              
               
			/*Added by chintan -  Biz usually automatically promotes the record from Lead/Prospect to Account When a Sales Opp/Order is made against it*/  
			-- Promote Lead to Prospect when Sales/Purchase Opp is created against it
			IF @tintCRMType=0 AND @tintOppStatus = 0 --Lead & Open Opp
			BEGIN
				UPDATE divisionmaster set tintCRMType=1,bintLeadProm=getutcdate(),bintLeadPromBy=@numUserCntID,bintProsProm=getutcdate(),bintProsPromBy=@numUserCntID        
				WHERE numDivisionID=@numDivisionID        
			END        
			-- Promote Lead to Account when Sales/Purchase Order is created against it
			ELSE if @tintCRMType=0 AND @tintOppStatus = 1 --Lead & Order
			BEGIN 
				UPDATE divisionmaster set tintCRMType=2,bintLeadProm=getutcdate(),bintLeadPromBy=@numUserCntID,bintProsProm=getutcdate(),bintProsPromBy=@numUserCntID        
				WHERE numDivisionID=@numDivisionID        
			END
			--Promote Prospect to Account
			ELSE IF @tintCRMType=1 AND @tintOppStatus = 1 
			BEGIN        
				update divisionmaster set tintCRMType=2,bintProsProm=getutcdate(),bintProsPromBy=@numUserCntID        
				where numDivisionID=@numDivisionID        
			END

			--Add/Update Address Details
			DECLARE @vcStreet  varchar(100),@vcCity  varchar(50),@vcPostalCode  varchar(15),@vcCompanyName  varchar(100),@vcAddressName VARCHAR(50)      
			DECLARE @numState  numeric(9),@numCountry  numeric(9), @numCompanyId  numeric(9) 
			DECLARE @bitIsPrimary BIT;

			SELECT  @vcCompanyName=vcCompanyName,@numCompanyId=div.numCompanyID FROM CompanyInfo Com                            
			JOIN divisionMaster Div                            
			ON div.numCompanyID=com.numCompanyID                            
			WHERE div.numdivisionID=@numDivisionId

			--INSERT TAX FOR OPPORTUNITY ITEMS
			INSERT INTO dbo.OpportunityItemsTaxItems 
			(
				numOppId,
				numOppItemID,
				numTaxItemID
			) 
			SELECT 
				@numOppId,OI.numoppitemtCode,TI.numTaxItemID 
			FROM 
				dbo.OpportunityItems OI 
			JOIN dbo.ItemTax IT ON OI.numItemCode=IT.numItemCode 
			JOIN TaxItems TI ON TI.numTaxItemID=IT.numTaxItemID 
			WHERE 
				OI.numOppId=@numOppID 
				AND IT.bitApplicable=1 
				AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
			UNION
			SELECT 
				@numOppId,OI.numoppitemtCode,0 
			FROM 
				dbo.OpportunityItems OI 
			JOIN dbo.Item I ON OI.numItemCode=I.numItemCode
			WHERE 
				OI.numOppId=@numOppID  
				AND I.bitTaxable=1 
				AND	OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)

			UPDATE OpportunityMaster SET monDealAmount=dbo.GetDealAmount(@numOppID,getdate(),0) WHERE numOppId=@numOppID
		END
	COMMIT TRAN 
	END TRY
	BEGIN CATCH
		IF ( @@TRANCOUNT > 0 ) 
		BEGIN
			ROLLBACK TRAN
		END

		SELECT @ErrorMessage = ERROR_MESSAGE(),@ErrorSeverity = ERROR_SEVERITY(),@ErrorState = ERROR_STATE()
		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState);
	END CATCH;
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityMaster_MergeVendorPO')
DROP PROCEDURE USP_OpportunityMaster_MergeVendorPO
GO
CREATE PROCEDURE [dbo].[USP_OpportunityMaster_MergeVendorPO]  
	@numDomainID NUMERIC(18,0),
	@numUserCntID NUMERIC(18,0),
	@vcXML VARCHAR(4000)
AS  
BEGIN  
BEGIN TRY
BEGIN TRANSACTION
	DECLARE @docHandle int;

	DECLARE @Vendor TABLE
	(
		ID INT IDENTITY(1,1),
		numVendorID NUMERIC(18,0),
		numMasterPOID NUMERIC(18,0)
	)

	DECLARE @VendorPOToMerger TABLE
	(
		numVendorID NUMERIC(18,0),
		numPOID NUMERIC(18,0)
	)

	DECLARE @TEMPMasterItems TABLE
	(
		numoppitemtCode NUMERIC(18,0),numItemCode NUMERIC(18,0),numUnitHour FLOAT,monPrice MONEY,
		monTotAmount MONEY,vcItemDesc VARCHAR(1000),numWarehouseItmsID NUMERIC(18,0),numToWarehouseItemID NUMERIC(18,0),
		Op_Flag INT, ItemType VARCHAR(50),DropShip BIT,bitDiscountType BIT, fltDiscount FLOAT, monTotAmtBefDiscount MONEY,
		vcItemName VARCHAR(300),numUOM NUMERIC(18,0), bitWorkOrder BIT,numVendorWareHouse NUMERIC(18,0),
		numShipmentMethod NUMERIC(18,0),numSOVendorId NUMERIC(18,0),numProjectID NUMERIC(18,0),
		numProjectStageID NUMERIC(18,0),Attributes VARCHAR(200),bitItemPriceApprovalRequired BIT
	)

	DECLARE @TEMPChildItems TABLE
	(
		numRowID INT, numoppitemtCode NUMERIC(18,0),numItemCode NUMERIC(18,0),numUnitHour FLOAT,monPrice MONEY,
		monTotAmount MONEY,vcItemDesc VARCHAR(1000),numWarehouseItmsID NUMERIC(18,0),numToWarehouseItemID NUMERIC(18,0),
		Op_Flag INT, ItemType VARCHAR(50),DropShip BIT,bitDiscountType BIT, fltDiscount FLOAT, monTotAmtBefDiscount MONEY,
		vcItemName VARCHAR(300),numUOM NUMERIC(18,0), bitWorkOrder BIT,numVendorWareHouse NUMERIC(18,0),
		numShipmentMethod NUMERIC(18,0),numSOVendorId NUMERIC(18,0),numProjectID NUMERIC(18,0),
		numProjectStageID NUMERIC(18,0),Attributes VARCHAR(200),bitItemPriceApprovalRequired BIT
	)

	DECLARE @TEMPPO TABLE
	(
		ID INT,
		numPOID NUMERIC(18,0)
	)

	EXEC sp_xml_preparedocument @docHandle OUTPUT, @vcXML;

	--RETRIVE VENDOR AND MASTER PURCHASE ORDER IDS
	INSERT INTO 
		@Vendor
	SELECT 
		VendorID,
		MasterPOID
	FROM 
		OPENXML(@docHandle, N'/Vendors/Vendor',2) 
	WITH 
		(
			VendorID NUMERIC(18,0),
			MasterPOID NUMERIC(18,0)
		);  
	
	--RETIRIVE ORDERS OF EACH VENDOR
	INSERT INTO 
		@VendorPOToMerger
	SELECT 
		VendorID,
		OrderID
	FROM 
		OPENXML(@docHandle, N'/Vendors/Vendor/Orders/OrderID',2) 
	WITH 
		(
			VendorID NUMERIC(18,0) '../../VendorID',
			OrderID NUMERIC(18,0) '.'
		);  

	DECLARE @i AS INT = 1
	DECLARE @iCOUNT AS INT
	SELECT @iCOUNT=COUNT(*) FROM @Vendor

	--LOOP ALL VENDORS
	WHILE @i <= @iCOUNT
	BEGIN
		DECLARE @VendorID AS NUMERIC(18,0) = 0
		DECLARE @MasterPOID AS NUMERIC(18,0) = 0
		DECLARE @numOppID  AS NUMERIC(18,0) = NULL                                                                        
		DECLARE @numContactId NUMERIC(18,0)=null                                                                        
		DECLARE @numDivisionId numeric(9)=null                                                                          
		DECLARE @tintSource numeric(9)=null                                                                          
		DECLARE @vcPOppName Varchar(100)=''                                                                 
		DECLARE @Comments varchar(1000)=''                                                                          
		DECLARE @bitPublicFlag bit=0                                                                                                                                                         
		DECLARE @monPAmount money =0                                                 
		DECLARE @numAssignedTo as numeric(9)=0                                                                                                                                                                                                                                                                                         
		DECLARE @strItems as VARCHAR(MAX)=null                                                                          
		DECLARE @strMilestone as VARCHAR(100)=null                                                                          
		DECLARE @dtEstimatedCloseDate datetime                                                                          
		DECLARE @CampaignID as numeric(9)=null                                                                          
		DECLARE @lngPConclAnalysis as numeric(9)=null                                                                         
		DECLARE @tintOppType as tinyint                                                                                                                                             
		DECLARE @tintActive as tinyint                                                              
		DECLARE @numSalesOrPurType as numeric(9)              
		DECLARE @numRecurringId as numeric(9)
		DECLARE @numCurrencyID as numeric(9)=0
		DECLARE @DealStatus as tinyint =0
		DECLARE @numStatus AS NUMERIC(9)
		DECLARE @vcOppRefOrderNo VARCHAR(100)
		DECLARE @numBillAddressId numeric(9)=0
		DECLARE @numShipAddressId numeric(9)=0
		DECLARE @bitStockTransfer BIT=0
		DECLARE @WebApiId INT = 0
		DECLARE @tintSourceType TINYINT=0
		DECLARE @bitDiscountType as bit
		DECLARE @fltDiscount  as float
		DECLARE @bitBillingTerms as bit
		DECLARE @intBillingDays as integer
		DECLARE @bitInterestType as bit
		DECLARE @fltInterest as float
		DECLARE @tintTaxOperator AS TINYINT
		DECLARE @numDiscountAcntType AS NUMERIC(9)
		DECLARE @vcWebApiOrderNo VARCHAR(100)=NULL
		DECLARE @vcCouponCode		VARCHAR(100) = ''
		DECLARE @vcMarketplaceOrderID VARCHAR(100)=NULL
		DECLARE @vcMarketplaceOrderDate  datetime=NULL
		DECLARE @vcMarketplaceOrderReportId VARCHAR(100)=NULL
		DECLARE @numPercentageComplete NUMERIC(9)
		DECLARE @bitUseShippersAccountNo BIT = 0
		DECLARE @bitUseMarkupShippingRate BIT = 0
		DECLARE @numMarkupShippingRate NUMERIC(19,2) = 0
		DECLARE @intUsedShippingCompany INT = 0

		--GET VENDOR AND MASTER PO ID
		SELECT @VendorID=numVendorID, @MasterPOID=numMasterPOID FROM @Vendor WHERE ID = @i

		--GET MASTER PO FIELDS 
		SELECT
			@numOppID=numOppId,@numContactId =numContactId,@numDivisionId=numDivisionId,@tintSource=tintSource,@vcPOppName=vcPOppName,@Comments=txtComments,
			@bitPublicFlag=bitPublicFlag,@monPAmount=monPAmount,@numAssignedTo=numAssignedTo,@strMilestone=null,@dtEstimatedCloseDate=intPEstimatedCloseDate,
			@CampaignID=numCampainID,@lngPConclAnalysis=lngPConclAnalysis,@tintOppType=tintOppType,@tintActive=tintActive,@numSalesOrPurType=numSalesOrPurType,
			@numRecurringId=null,@numCurrencyID=numCurrencyID,@DealStatus=tintOppStatus,@numStatus=numStatus,@vcOppRefOrderNo=vcOppRefOrderNo,@numBillAddressId=0,
			@numShipAddressId=0,@bitStockTransfer=0,@WebApiId=0,@tintSourceType=tintSourceType,@bitDiscountType=bitDiscountType,@fltDiscount=fltDiscount,
			@bitBillingTerms=bitBillingTerms,@intBillingDays=intBillingDays,@bitInterestType=bitInterestType,@fltInterest=fltInterest,@tintTaxOperator=tintTaxOperator,
			@numDiscountAcntType=numDiscountAcntType,@vcWebApiOrderNo=vcWebApiOrderNo,@vcCouponCode=vcCouponCode,@vcMarketplaceOrderID=vcMarketplaceOrderID,
			@vcMarketplaceOrderDate=bintCreatedDate,@vcMarketplaceOrderReportId=vcMarketplaceOrderReportId,@numPercentageComplete= numPercentageComplete,
			@bitUseShippersAccountNo=bitUseShippersAccountNo,@bitUseMarkupShippingRate=bitUseMarkupShippingRate,@numMarkupShippingRate=numMarkupShippingRate,
			@intUsedShippingCompany= intUsedShippingCompany
		FROM
			OpportunityMaster
		WHERE
			numDomainId = @numDomainID
			AND numOppId = @MasterPOID 

		--CLEAR DATA OF PREVIOUS ITERATION
		DELETE FROM @TEMPMasterItems
		DELETE FROM @TEMPChildItems

		
		--GET ITEMS OF MASTER PO
		INSERT INTO 
			@TEMPMasterItems
		SELECT 
			numoppitemtCode,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,numToWarehouseItemID,
			2,vcType,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,vcItemName,numUOMId,bitWorkOrder,numVendorWareHouse,
			numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,vcAttributes,bitItemPriceApprovalRequired
		FROM 
			OpportunityItems 
		WHERE 
			numOppId = @MasterPOID

		--GET ITEMS OF PO NEEDS TO BE MERGED TO MASTER PO
		INSERT INTO 
			@TEMPChildItems
		SELECT 
			ROW_NUMBER() OVER(ORDER BY numoppitemtCode),numoppitemtCode,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,numToWarehouseItemID,
			1,vcType,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,vcItemName,numUOMId,bitWorkOrder,numVendorWareHouse,
			numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,vcAttributes,bitItemPriceApprovalRequired
		FROM 
			OpportunityItems 
		WHERE 
			numOppId IN (SELECT numPOID FROM @VendorPOToMerger WHERE numVendorID=@VendorID AND numPOID <> @MasterPOID)


		DECLARE @numChildItemCode NUMERIC(18,0) = 0
		DECLARE @numChildWaerehouseItemID NUMERIC(18,0) = 0
		DECLARE @numChildUnitHour FLOAT = 0
		DECLARE @monChildPrice NUMERIC(18,0) = 0
		DECLARE @K AS INT = 1
		DECLARE @KCOUNT AS INT
		SELECT @KCOUNT=COUNT(*) FROM @TEMPChildItems

		-- LOOP ITEMS OF EACH PO NEEDS TO MERGED
		WHILE @K <= @KCOUNT
		BEGIN
			SELECT 
				@numChildItemCode=numItemCode,
				@numChildWaerehouseItemID=ISNULL(numWarehouseItmsID,0),
				@numChildUnitHour=ISNULL(numUnitHour,0), 
				@monChildPrice=ISNULL(monPrice,0) 
			FROM 
				@TEMPChildItems 
			WHERE 
				numRowID = @K

			-- IF ITEM WITH SAME ITEM ID AND WAREHOUSE IS EXIST IN MASTER ORDER THEN UPDATE QTY IN MASTER PO AND SET PRICE TO WHICHEVER LOWER
			-- ELSE ADD ITEMS MASTER PO ITEMS
			IF EXISTS(SELECT numoppitemtCode FROM @TEMPMasterItems WHERE numItemCode=@numChildItemCode AND ISNULL(numWarehouseItmsID,0)=@numChildWaerehouseItemID)
			BEGIN
				UPDATE 
					@TEMPMasterItems
				SET 
					numUnitHour = ISNULL(numUnitHour,0) + @numChildUnitHour,
					monPrice = (CASE WHEN ISNULL(monPrice,0) > @monChildPrice THEN @monChildPrice ELSE monPrice END),
					monTotAmount = (ISNULL(numUnitHour,0) + @numChildUnitHour) * (CASE WHEN ISNULL(monPrice,0) > @monChildPrice THEN @monChildPrice ELSE monPrice END)
				WHERE
					numItemCode=@numChildItemCode 
					AND ISNULL(numWarehouseItmsID,0)=@numChildWaerehouseItemID
			END
			ELSE
			BEGIN
				INSERT INTO 
					@TEMPMasterItems
				SELECT 
					numoppitemtCode,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,numToWarehouseItemID,
					1,ItemType,DropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,vcItemName,numUOM,bitWorkOrder,numVendorWareHouse,
					numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,Attributes,bitItemPriceApprovalRequired
				FROM 
					@TEMPChildItems 
				WHERE 
					numRowID = @K

			END

			SET @K = @K + 1
		END

		-- Op_Flag is 2(UPDATE) FOR MASTER PO AND 1(INSERT) FOR OTHER
		SET @strItems =(
							SELECT 
								numoppitemtCode,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,numToWarehouseItemID,
								Op_Flag, ItemType,DropShip,	bitDiscountType, fltDiscount, monTotAmtBefDiscount,	vcItemName,numUOM, bitWorkOrder,
								numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,Attributes,bitItemPriceApprovalRequired
							FROM 
								@TEMPMasterItems
							FOR XML RAW ('Item'), ROOT ('NewDataSet'), ELEMENTS
						)
			
		--UPDATE MASTER PO AND ADD MERGE PO ITEMS
		EXEC USP_OppManage @numOppID=@numOppID OUTPUT,@numContactId=@numContactId,@numDivisionId=@numDivisionId,@tintSource=@tintSource,@vcPOppName=@vcPOppName output,
							@Comments=@Comments,@bitPublicFlag=@bitPublicFlag,@numUserCntID=@numUserCntID,@monPAmount=@monPAmount,@numAssignedTo=@numAssignedTo,
							@numDomainId=@numDomainId,@strItems=@strItems,@strMilestone=@strMilestone,@dtEstimatedCloseDate=@dtEstimatedCloseDate,@CampaignID=@CampaignID,
							@lngPConclAnalysis=@lngPConclAnalysis,@tintOppType=@tintOppType,@tintActive=@tintActive,@numSalesOrPurType=@numSalesOrPurType,
							@numRecurringId=@numRecurringId,@numCurrencyID=@numCurrencyID,@DealStatus=@DealStatus,@numStatus=@numStatus,@vcOppRefOrderNo=@vcOppRefOrderNo,
							@numBillAddressId=@numBillAddressId,@numShipAddressId=@numShipAddressId,@bitStockTransfer=@bitStockTransfer,@WebApiId=@WebApiId,
							@tintSourceType=@tintSourceType,@bitDiscountType=@bitDiscountType,@fltDiscount=@fltDiscount,@bitBillingTerms=@bitBillingTerms,
							@intBillingDays=@intBillingDays,@bitInterestType=@bitInterestType,@fltInterest=@fltInterest,@tintTaxOperator=@tintTaxOperator,
							@numDiscountAcntType=@numDiscountAcntType,@vcWebApiOrderNo=@vcWebApiOrderNo,@vcCouponCode=@vcCouponCode,@vcMarketplaceOrderID=@vcMarketplaceOrderID,
							@vcMarketplaceOrderDate=@vcMarketplaceOrderDate,@vcMarketplaceOrderReportId=@vcMarketplaceOrderReportId,@numPercentageComplete=@numPercentageComplete,
							@bitUseShippersAccountNo=@bitUseShippersAccountNo,@bitUseMarkupShippingRate=@bitUseMarkupShippingRate,
							@numMarkupShippingRate=@numMarkupShippingRate,@intUsedShippingCompany=@intUsedShippingCompany
		
		-- DELETE MERGED PO

		---- CLEAR DATA OF PREVIOUS ITERATION
		DELETE FROM @TEMPPO

		---- GET ALL POS EXCEPT MASTER PO
		INSERT INTO @TEMPPO SELECT Row_Number() OVER(ORDER BY numPOID), numPOID FROM @VendorPOToMerger WHERE numVendorID=@VendorID AND numPOID <> @MasterPOID

		DECLARE @j AS INT = 1
		DECLARE @jCOUNT AS INT
		SELECT @jCOUNT=COUNT(*) FROM @TEMPPO

		WHILE @j <= @jCOUNT
		BEGIN
			DECLARE @POID AS INT = 0
			SELECT @POID=numPOID FROM @TEMPPO WHERE ID = @j
			
			EXEC USP_DeleteOppurtunity @numOppId=@POID,@numDomainID=@numDomainID,@numUserCntID=@numUserCntID

			SET @j = @j + 1
		END

		SET @i = @i + 1
	END
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH	
END
GO
/****** Object:  StoredProcedure [dbo].[USP_OppShippingorReceiving]    Script Date: 07/26/2008 16:20:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- created by anoop jayaraj            
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_oppshippingorreceiving')
DROP PROCEDURE usp_oppshippingorreceiving
GO
CREATE PROCEDURE [dbo].[USP_OppShippingorReceiving]            
@OppID as numeric(9),
@numUserCntID AS NUMERIC(9)          
as
BEGIN TRY
	
	DECLARE @numDomain AS NUMERIC(18,0)
	SELECT @numDomain = OM.numDomainID FROM [dbo].[OpportunityMaster] AS OM WHERE [OM].[numOppId] = @OppID
	PRINT @numDomain
	-- Added by Manish Anjara : 17th Sep,2014
	-- Check whether order is already closed or not. If closed then show a warning message
	IF (SELECT ISNULL(tintshipped,0) FROM [dbo].[OpportunityMaster] WHERE [OpportunityMaster].[numOppId] = @OppID AND [OpportunityMaster].[numDomainId] = @numDomain) = 1
	BEGIN
		RAISERROR('ORDER_CLOSED', 16, 1)
	END		

   BEGIN TRANSACTION 
			
			update OpportunityMaster set tintshipped=1,bintAccountClosingDate=GETUTCDATE(), bintClosedDate=GETUTCDATE() where  numOppId=@OppID 
			UPDATE [OpportunityBizDocs] SET [dtShippedDate] = GETUTCDATE() WHERE [numOppId]=@OppID AND numBizDocID <> 296

			UPDATE [OpportunityBizDocs] SET [dtShippedDate] = GETUTCDATE() WHERE [numOppId]=@OppID AND numBizDocID = 296 AND dtShippedDate IS NULL
			
						
			declare @status as varchar(2)            
			declare @OppType as varchar(2)   
			declare @bitStockTransfer as BIT
			DECLARE @fltExchangeRate AS FLOAT 
         
			select @status=tintOppStatus,@OppType=tintOppType,@bitStockTransfer=ISNULL(bitStockTransfer,0),@fltExchangeRate=(CASE WHEN ISNULL(fltExchangeRate,0)=0 THEN 1 ELSE fltExchangeRate END) from OpportunityMaster
			where numOppId=@OppID
			
			-- Archive item based on Item's individual setting
			IF @OppType = 1
			BEGIN
				UPDATE Item SET IsArchieve = CASE WHEN EXISTS(SELECT OpportunityItems.numItemCode FROM dbo.OpportunityItems WHERE OpportunityItems.numItemCode = I.numItemCode) 
												  THEN 1
												  ELSE 0 
											 END 
				FROM Item I JOIN dbo.OpportunityItems OI ON I.numItemCode = OI.numItemCode  
				WHERE numOppId = @OppID 
				AND I.[numDomainID] = @numDomain

				AND ISNULL(I.bitArchiveItem,0) = 1 
			END
			----------------------------------------------------------------------------            
			--Updating Item Details            
			-----------------------------------------------------------------------            
            
			---Updating the inventory Items once the deal is won
			declare @numoppitemtCode as numeric
			declare @numUnits as FLOAT
			declare @numWarehouseItemID as numeric       
			declare @numToWarehouseItemID as numeric(9) --to be used with stock transfer
			declare @itemcode as numeric        
			declare @QtyShipped as FLOAT
			declare @QtyReceived as FLOAT
			Declare @monPrice as money
			declare @Kit as bit
			declare @bitWorkOrder AS BIT
			declare @bitSerialized AS BIT
			declare @bitLotNo AS BIT
    
			 select top 1 @numoppitemtCode=numoppitemtCode,@itemcode=OI.numItemCode,@numUnits=ISNULL(numUnitHour,0),@qtyShipped=ISNULL([numQtyShipped],0),@qtyReceived=ISNULL([numUnitHourReceived],0),
			 @numWareHouseItemID=ISNULL(numWarehouseItmsID,0),@numToWarehouseItemID=numToWarehouseItemID,@monPrice=isnull(monPrice,0) * @fltExchangeRate,@Kit= (case when bitKitParent=1 and bitAssembly=1 then 0 when bitKitParent=1 then 1 else 0 end),
			 @bitWorkOrder = ISNULL(bitWorkOrder,0),@bitSerialized=ISNULL(bitSerialized,0),@bitLotNo=ISNULL(bitLotNo,0)
			 from OpportunityItems OI join Item I                                                
			 on OI.numItemCode=I.numItemCode and numOppId=@OppID and (bitDropShip=0 or bitDropShip is null)                                            
			 where (charitemtype='P' OR 1=(CASE WHEN @OppType=1 THEN 
							CASE WHEN ISNULL(I.bitKitParent,0) = 1 AND ISNULL(I.bitAssembly,0) = 1 THEN 0
								 WHEN ISNULL(I.bitKitParent,0) = 1 THEN 1
								 ELSE 0 END 
							ELSE 0 END))  
			 AND I.[numDomainID] = @numDomain	
							order by OI.numoppitemtCode      

			 while @numoppitemtCode>0                
			 begin   
 
--			  if @Kit = 1
--			  begin
--					exec USP_UpdateKitItems @numoppitemtCode,@numUnits,@OppType,2,@OppID
--			  end
             
             
				IF @bitStockTransfer = 1
				BEGIN
					--Make inventory changes as if sales order was closed , using OpportunityItems.numWarehouseItmsID
					EXEC usp_ManageInventory @itemcode,@numWareHouseItemID,0,1,@numUnits,@qtyShipped,@qtyReceived,@monPrice,2,@OppID,@numoppitemtCode,@numUserCntID,@bitWorkOrder
					--Make inventory changes as if purchase order was closed, using OpportunityItems.numToWarehouseItemID
					EXEC usp_ManageInventory @itemcode,@numToWarehouseItemID,0,2,@numUnits,@qtyShipped,@qtyReceived,@monPrice,2,@OppID,@numoppitemtCode,@numUserCntID,@bitWorkOrder
					If @bitSerialized = 1 OR @bitLotNo = 1
					BEGIN
						--Transfer Serial/Lot Numbers
						EXEC USP_WareHouseItmsDTL_TransferSerialLotNo @OppID,@numoppitemtCode,@numWareHouseItemID,@numToWarehouseItemID,@bitSerialized,@bitLotNo
					END
				END
				ELSE
				BEGIN
					EXEC usp_ManageInventory @itemcode,@numWareHouseItemID,0,@OppType,@numUnits,@qtyShipped,@qtyReceived,@monPrice,2,@OppID,@numoppitemtCode,@numUserCntID,@bitWorkOrder
				END
        
			  select top 1 @numoppitemtCode=numoppitemtCode,@itemcode=OI.numItemCode,@numUnits=ISNULL(numUnitHour,0),@qtyShipped=ISNULL([numQtyShipped],0),@qtyReceived=ISNULL([numUnitHourReceived],0),
			 @numWareHouseItemID=ISNULL(numWarehouseItmsID,0),@numToWarehouseItemID=numToWarehouseItemID,@monPrice=isnull(monPrice,0) * @fltExchangeRate,@Kit= (case when bitKitParent=1 and bitAssembly=1 then 0 when bitKitParent=1 then 1 else 0 end),
			 @bitWorkOrder = ISNULL(bitWorkOrder,0),@bitSerialized=ISNULL(bitSerialized,0),@bitLotNo=ISNULL(bitLotNo,0)
			  from OpportunityItems OI
			  join Item I
			  on OI.numItemCode=I.numItemCode and numOppId=@OppID                                              
			  where (charitemtype='P' OR 1=(CASE WHEN @OppType=1 THEN 
							CASE WHEN ISNULL(I.bitKitParent,0) = 1 AND ISNULL(I.bitAssembly,0) = 1 THEN 0
								 WHEN ISNULL(I.bitKitParent,0) = 1 THEN 1
								 ELSE 0 END 
							ELSE 0 END)) and OI.numoppitemtCode>@numoppitemtCode and (bitDropShip=0 or bitDropShip is null) 
			  AND I.numDomainID = @numDomain
			  ORDER by OI.numoppitemtCode                                                
			  if @@rowcount=0 set @numoppitemtCode=0     
  
			 END
 COMMIT
END TRY
BEGIN CATCH
  -- Whoops, there was an error
  IF @@TRANCOUNT > 0
     ROLLBACK

  -- Raise an error with the details of the exception
  DECLARE @ErrMsg nvarchar(4000), @ErrSeverity int
  SELECT @ErrMsg = ERROR_MESSAGE(),
         @ErrSeverity = ERROR_SEVERITY()

  RAISERROR(@ErrMsg, @ErrSeverity, 1)
END CATCH

GO
/****** Object:  StoredProcedure [dbo].[USP_ReOpenOppertunity]    Script Date: 07/26/2008 16:20:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReOpenOppertunity')
DROP PROCEDURE USP_ReOpenOppertunity
GO
CREATE PROCEDURE [dbo].[USP_ReOpenOppertunity]            
@OppID as numeric(9),
@numUserCntID AS NUMERIC(9),
@numDomainID AS NUMERIC(9)
          
as          
BEGIN TRY
	-- Added by Manish Anjara : 17th Sep,2014
	-- Check whether order is already opened or not. If already opened then show a warning message
	IF (SELECT ISNULL(tintshipped,0) FROM [dbo].[OpportunityMaster] WHERE [OpportunityMaster].[numOppId] = @OppID AND [OpportunityMaster].[numDomainId] = @numDomainID) = 0
	BEGIN
		RAISERROR('ORDER_OPENED', 16, 1)
	END

   BEGIN TRANSACTION       
			declare @status as varchar(2)            
			declare @OppType as varchar(2)  
			DECLARE @fltExchangeRate AS FLOAT 
			          
			select @status=tintOppStatus,@OppType=tintOppType,@fltExchangeRate=(CASE WHEN ISNULL(fltExchangeRate,0)=0 THEN 1 ELSE fltExchangeRate END) from OpportunityMaster            
			where numOppId=@OppID AND [numDomainId] = @numDomainID

			update OpportunityMaster set tintshipped=0,bintAccountClosingDate=NULL,bintClosedDate=NULL where  numOppId=@OppID  AND [numDomainId] = @numDomainID
			UPDATE [OpportunityBizDocs] SET [dtShippedDate] = NULL WHERE [numOppId]=@OppID AND numBizDocID <> 296
        			
			-- WE ARE REVERTING RECEIVED (PURCHASED) QTY ONLY BECAUSE USER HAS TO DELETE FULFILLMENT ORDER TO REVERSE SHIPPED (SALED) QTY
			IF @OppType = 2
			BEGIN
				--SET Received qty to 0	
				UPDATE OpportunityItems SET numUnitHourReceived=0,numQtyShipped=0 WHERE [numOppId]=@OppID
				UPDATE OpportunityKitItems SET numQtyShipped=0 WHERE [numOppId]=@OppID
			END
			
			-- UnArchive item based on Item's individual setting
			IF @OppType = 1
			BEGIN
				UPDATE Item SET IsArchieve = CASE WHEN EXISTS(SELECT OpportunityItems.numItemCode FROM dbo.OpportunityItems 
															  WHERE OpportunityItems.numItemCode = I.numItemCode
															  AND OpportunityItems.numoppitemtCode = OI.numoppitemtCode) 
												  THEN 0
												  ELSE 1 
											 END 
				FROM Item I JOIN dbo.OpportunityItems OI ON I.numItemCode = OI.numItemCode  
				WHERE numOppId = @OppID 
				AND I.[numDomainId] = @numDomainID
				AND ISNULL(I.bitArchiveItem,0) = 1 
			END     
			----------------------------------------------------------------------------            
			--Updating Item Details            
			-----------------------------------------------------------------------            
            
			---Updating the inventory Items once the deal is won              
			declare @numoppitemtCode as numeric            
			declare @numUnits as FLOAT              
			declare @numWarehouseItemID as numeric       
			declare @itemcode as numeric        
			declare @QtyShipped as FLOAT
			declare @QtyReceived as FLOAT
			Declare @monPrice as money  
			declare @Kit as bit  
			declare @bitWorkOrder AS BIT
    
			 select top 1 @numoppitemtCode=numoppitemtCode,@itemcode=OI.numItemCode,@numUnits=ISNULL(numUnitHour,0),@qtyShipped=ISNULL([numQtyShipped],0),@qtyReceived=ISNULL([numUnitHourReceived],0),
			 @numWareHouseItemID=ISNULL(numWarehouseItmsID,0),@monPrice=isnull(monPrice,0) * @fltExchangeRate,@Kit= (case when bitKitParent=1 and bitAssembly=1 then 0 when bitKitParent=1 then 1 else 0 end),
			 @bitWorkOrder = bitWorkOrder
			 from OpportunityItems OI join Item I                                                
			 on OI.numItemCode=I.numItemCode and numOppId=@OppID and (bitDropShip=0 or bitDropShip is null)                                            
			 where (charitemtype='P' OR 1=(CASE WHEN @OppType=1 THEN 
							CASE WHEN ISNULL(I.bitKitParent,0) = 1 AND ISNULL(I.bitAssembly,0) = 1 THEN 0
								 WHEN ISNULL(I.bitKitParent,0) = 1 THEN 1
								 ELSE 0 END 
							ELSE 0 END))  
			 AND I.[numDomainId] = @numDomainID
			order by OI.numoppitemtCode      
            
			 while @numoppitemtCode>0                
			 begin   
 
				EXEC usp_ManageInventory @itemcode,@numWareHouseItemID,0,@OppType,@numUnits,@qtyShipped,@qtyReceived,@monPrice,3,@OppID,@numoppitemtCode,@numUserCntID,@bitWorkOrder
        
			  select top 1 @numoppitemtCode=numoppitemtCode,@itemcode=OI.numItemCode,@numUnits=ISNULL(numUnitHour,0),@qtyShipped=ISNULL([numQtyShipped],0),@qtyReceived=ISNULL([numUnitHourReceived],0),
			  @numWareHouseItemID=ISNULL(numWarehouseItmsID,0),@monPrice=isnull(monPrice,0) * @fltExchangeRate,@Kit= (case when bitKitParent=1 and bitAssembly=1 then 0 when bitKitParent=1 then 1 else 0 end) 
			  from OpportunityItems OI                                                
			  join Item I                                                
			  on OI.numItemCode=I.numItemCode and numOppId=@OppID                                              
			  where (charitemtype='P' OR 1=(CASE WHEN @OppType=1 THEN 
							CASE WHEN ISNULL(I.bitKitParent,0) = 1 AND ISNULL(I.bitAssembly,0) = 1 THEN 0
								 WHEN ISNULL(I.bitKitParent,0) = 1 THEN 1
								 ELSE 0 END 
							ELSE 0 END))
					and OI.numoppitemtCode>@numoppitemtCode and (bitDropShip=0 or bitDropShip is null) 
					AND I.[numDomainId] = @numDomainID
					order by OI.numoppitemtCode                                                
			  if @@rowcount=0 set @numoppitemtCode=0     
  
			 END
				

			IF @OppType=2
			BEGIN
				IF (SELECT
						COUNT(*)
					FROM
						OppWarehouseSerializedItem OWSI
					INNER JOIN
						WareHouseItmsDTL WHIDL
					ON
						OWSI.numWarehouseItmsDTLID = WHIDL.numWareHouseItmsDTLID 
						AND WHIDL.numWareHouseItemID = OWSI.numWarehouseItmsID 
					INNER JOIN
						WareHouseItems
					ON
						WHIDL.numWareHouseItemID = WareHouseItems.numWareHouseItemID
					INNER JOIN
						Item
					ON
						WareHouseItems.numItemID = Item.numItemCode
					WHERE
						OWSI.numOppID = @OppID
						AND 1 = (CASE 
									WHEN Item.bitLotNo = 1 AND ISNULL(WHIDL.numQty,0) < ISNULL(OWSI.numQty,0) THEN 1 
									WHEN Item.bitSerialized = 1 AND ISNULL(WHIDL.numQty,0) = 0 THEN 1 
									ELSE 0 
								END)
					) > 0
				BEGIN
					RAISERROR ('SERIAL/LOT#_USED', 16, 1 ) ;
				END
				ELSE
				BEGIN
					-- REMOVE SERIAL/LOT# NUMBER FROM INVENTORY BECAUSE PURCHASE ORDER IS DELETED
					UPDATE WHIDL
						SET WHIDL.numQty = (CASE WHEN Item.bitLotNo = 1 THEN ISNULL(WHIDL.numQty,0) - ISNULL(OWSI.numQty,0) ELSE 0 END)
					FROM 
						WareHouseItmsDTL WHIDL
					INNER JOIN
						OppWarehouseSerializedItem OWSI
					ON
						WHIDL.numWareHouseItmsDTLID = OWSI.numWarehouseItmsDTLID
						AND WHIDL.numWareHouseItemID = OWSI.numWarehouseItmsID
					INNER JOIN
						WareHouseItems
					ON
						WHIDL.numWareHouseItemID = WareHouseItems.numWareHouseItemID
					INNER JOIN
						Item
					ON
						WareHouseItems.numItemID = Item.numItemCode
					WHERE
						OWSI.numOppID = @OppID
				END
				
				DELETE FROM OppWarehouseSerializedItem WHERE numOppID=@OppID
			END
		
			
			IF @OppType=2
			BEGIn
			DELETE FROM dbo.General_Journal_Details WHERE numJournalId IN (SELECT numJournal_Id FROM dbo.General_Journal_Header WHERE numDomainID=@numDomainID AND numOppId=@OppID AND ISNULL(numOppBizDocsId,0)=0 
				AND isnull(numBillID,0)=0 AND isnull(numBillPaymentID,0)=0 AND isnull(numPayrollDetailID,0)=0
				AND isnull(numCheckHeaderID,0)=0 AND isnull(numReturnID,0)=0 
				AND isnull(numDepositId,0)=0)
			
			DELETE FROM General_Journal_Header WHERE numDomainID=@numDomainID AND numOppId=@OppID AND ISNULL(numOppBizDocsId,0)=0 
				AND isnull(numBillID,0)=0 AND isnull(numBillPaymentID,0)=0 AND isnull(numPayrollDetailID,0)=0
				AND isnull(numCheckHeaderID,0)=0 AND isnull(numReturnID,0)=0 
				AND isnull(numDepositId,0)=0
			END
COMMIT
END TRY
BEGIN CATCH
  -- Whoops, there was an error
  IF @@TRANCOUNT > 0
     ROLLBACK

  -- Raise an error with the details of the exception
  DECLARE @ErrMsg nvarchar(4000), @ErrSeverity int
  SELECT @ErrMsg = ERROR_MESSAGE(),
         @ErrSeverity = ERROR_SEVERITY()

  RAISERROR(@ErrMsg, @ErrSeverity, 1)
END CATCH


GO
/****** Object:  StoredProcedure [dbo].[USP_RepItems]    Script Date: 07/26/2008 16:20:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_repitems')
DROP PROCEDURE usp_repitems
GO
CREATE PROCEDURE [dbo].[USP_RepItems]        
  @numDomainID numeric,              
  @dtFromDate datetime,              
  @dtToDate datetime,              
  @numUserCntID numeric=0,                   
  @tintType numeric,
  @numSort numeric,
  @numDivisionId numeric
          
        
as        

create table #TempRepItems
(numItemCode numeric(9),
vcItemName varchar(300),
vcModelID varchar(250),
txtItemDesc varchar(1000),
Type varchar(100),
Amount numeric(9,2),       
TotalItem FLOAT,
OnHand FLOAT,
 OnOrder FLOAT,
 BackOrder FLOAT,
 Allocation FLOAT)
 
 DECLARE @vcSql varchar(2500);
 
 INSERT INTO #TempRepItems
 
SELECT  numItemCode,
        vcItemName,
        vcModelID,
        txtItemDesc,
        CASE WHEN charItemType = 'P' THEN 'Product'
             WHEN charItemType = 'S' THEN 'Service'
        END AS TYPE,
        0,
        0,

        ( SELECT    ISNULL(SUM(numOnHand), 0)
          FROM      WareHouseItems
          WHERE     numItemID = numItemCode
        ) AS OnHand,
        ( SELECT    ISNULL(SUM(numOnOrder), 0)
          FROM      WareHouseItems
          WHERE     numItemID = numItemCode
        ) AS OnOrder,
        ( SELECT    ISNULL(SUM(numBackOrder), 0)
          FROM      WareHouseItems
          WHERE     numItemID = numItemCode
        ) AS BackOrder,
        ( SELECT    ISNULL(SUM(numAllocation), 0)
          FROM      WareHouseItems
          WHERE     numItemID = numItemCode
        ) AS Allocation
FROM    item
WHERE   numDomainID = @numDomainID

UPDATE #TempRepItems
SET Amount=ISNULL(X.Amount,0),TotalItem=ISNULL(X.TotalItem,0)
FROM (
	SELECT OppItem.numItemCode,SUM(OppItem.numUnitHour) TotalItem,SUM(monTotAmount) Amount
					 FROM   OpportunityMaster Opp
							JOIN OpportunityItems OppItem ON OppItem.numOppId = Opp.numOppId
					 WHERE  OPP.numDomainId = @numDomainID AND Opp.tintOppType = 1
							AND Opp.tintOppStatus = 1
							AND Opp.numDivisionId = ( CASE @numDivisionId
														WHEN 0
														THEN Opp.numDivisionId
														ELSE @numDivisionId
													  END )
							AND Opp.bintCreatedDate >= @dtFromDate
							AND Opp.bintCreatedDate <= @dtToDate
	GROUP BY numItemCode
) X
WHERE #TempRepItems.numItemCode = X.numItemCode

set @vcSql='SELECT * FROM #TempRepItems WHERE TotalItem > 0 ORDER BY ' + (Case @numSort WHEN 0 THEN 'Amount' ELSE 'TotalItem'  END) + ' desc'

exec (@vcSql)
--
GO
/****** Object:  StoredProcedure [dbo].[USP_RevertDetailsOpp]    Script Date: 07/26/2008 16:20:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj      
/* deducts Qty from allocation and Adds back to onhand */
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_RevertDetailsOpp')
DROP PROCEDURE USP_RevertDetailsOpp
GO
CREATE PROCEDURE [dbo].[USP_RevertDetailsOpp] 
@numOppId NUMERIC,
@tintMode AS TINYINT=0, -- 0:Add/Edit 1:Delete
@numUserCntID AS NUMERIC(9)
AS ---reverting back to previous state if deal is being edited 	DECLARE @numDomain AS NUMERIC(18,0)	SELECT @numDomain = numDomainID FROM [dbo].[OpportunityMaster] AS OM WHERE [OM].[numOppId] = @numOppID    	DECLARE @OppType AS VARCHAR(2)                  DECLARE @itemcode AS NUMERIC           DECLARE @numWareHouseItemID AS NUMERIC                                       DECLARE @numToWarehouseItemID AS NUMERIC     DECLARE @numUnits AS FLOAT                                                  DECLARE @onHand AS FLOAT                                                DECLARE @onOrder AS FLOAT                                                DECLARE @onBackOrder AS FLOAT                                                  DECLARE @onAllocation AS FLOAT    DECLARE @numQtyShipped AS FLOAT    DECLARE @numUnitHourReceived AS FLOAT    DECLARE @numoppitemtCode AS NUMERIC(9)     DECLARE @monAmount AS MONEY     DECLARE @monAvgCost AS MONEY       DECLARE @Kit AS BIT                                            DECLARE @bitKitParent BIT    DECLARE @bitStockTransfer BIT     DECLARE @numOrigUnits AS FLOAT			    DECLARE @description AS VARCHAR(100)	DECLARE @bitWorkOrder AS BIT	--Added by :Sachin Sadhu||Date:18thSept2014	--For Rental/Asset Project	Declare @numRentalIN as FLOAT	Declare @numRentalOut as FLOAT	Declare @numRentalLost as FLOAT	DECLARE @bitAsset as BIT	--end sachin    						    SELECT TOP 1            @numoppitemtCode = numoppitemtCode,            @itemcode = OI.numItemCode,            @numUnits = ISNULL(numUnitHour,0),            @numWareHouseItemID = ISNULL(numWarehouseItmsID,0),            @Kit = ( CASE WHEN bitKitParent = 1                               AND bitAssembly = 1 THEN 0                          WHEN bitKitParent = 1 THEN 1                          ELSE 0                     END ),            @monAmount = ISNULL(monTotAmount,0) * (CASE WHEN ISNULL(fltExchangeRate,0)=0 THEN 1 ELSE fltExchangeRate END),            @monAvgCost = ISNULL(monAverageCost,0),            @numQtyShipped = ISNULL(numQtyShipped,0),            @numUnitHourReceived = ISNULL(numUnitHourReceived,0),            @bitKitParent=ISNULL(bitKitParent,0),            @numToWarehouseItemID =OI.numToWarehouseItemID,            @bitStockTransfer = ISNULL(OM.bitStockTransfer,0),            @OppType = tintOppType,		    @numRentalIN=ISNULL(oi.numRentalIN,0),			@numRentalOut=Isnull(oi.numRentalOut,0),			@numRentalLost=Isnull(oi.numRentalLost,0),			@bitAsset =ISNULL(I.bitAsset,0),			@bitWorkOrder = ISNULL(OI.bitWorkOrder,0)    FROM    OpportunityItems OI			JOIN dbo.OpportunityMaster OM ON OI.numOppId = OM.numOppId            JOIN Item I ON OI.numItemCode = I.numItemCode    WHERE   (charitemtype='P' OR 1=(CASE WHEN tintOppType=1 THEN 							CASE WHEN ISNULL(I.bitKitParent,0) = 1 AND ISNULL(I.bitAssembly,0) = 1 THEN 0								 WHEN ISNULL(I.bitKitParent,0) = 1 THEN 1								 ELSE 0 END 							ELSE 0 END)) AND OI.numOppId = @numOppId                           AND ( bitDropShip = 0                                 OR bitDropShip IS NULL                               )     ORDER BY OI.numoppitemtCode	PRINT '@numoppitemtCode:' + CAST(@numoppitemtCode AS VARCHAR(10))	PRINT '@numUnits:' + CAST(@numUnits AS VARCHAR(10))	PRINT '@numWareHouseItemID:' + CAST(@numWareHouseItemID AS VARCHAR(10))	PRINT '@numQtyShipped:' + CAST(@numQtyShipped AS VARCHAR(10))	PRINT '@numUnitHourReceived:' + CAST(@numUnitHourReceived AS VARCHAR(10))	PRINT '@numToWarehouseItemID:' + CAST(@numToWarehouseItemID AS VARCHAR(10))	PRINT '@bitStockTransfer:' + CAST(@bitStockTransfer AS VARCHAR(10))	    WHILE @numoppitemtCode > 0                                          BEGIN            --Kamal : Item Group take as inline Item--            IF @Kit = 1 --                BEGIN  --                    EXEC USP_UpdateKitItems @numoppitemtCode, @numUnits,--                        @OppType, 1,@numOppID  --                END                                      SET @numOrigUnits=@numUnits                        IF @bitStockTransfer=1            BEGIN				SET @OppType = 1			END                        PRINT '@OppType:' + CAST(@OppType AS VARCHAR(10))                  IF @numWareHouseItemID>0            BEGIN     				PRINT 'FROM REVERT : CONDITION : @numWareHouseItemID>0'                             				SELECT  @onHand = ISNULL(numOnHand, 0),						@onAllocation = ISNULL(numAllocation, 0),						@onOrder = ISNULL(numOnOrder, 0),						@onBackOrder = ISNULL(numBackOrder, 0)				FROM    WareHouseItems				WHERE   numWareHouseItemID = @numWareHouseItemID       								PRINT '@onHand:' + CAST(@onHand AS VARCHAR(10))				PRINT '@onAllocation:' + CAST(@onAllocation AS VARCHAR(10))				PRINT '@onOrder:' + CAST(@onOrder AS VARCHAR(10))				PRINT '@onBackOrder:' + CAST(@onBackOrder AS VARCHAR(10))                                                     END                        IF @OppType = 1                 BEGIN					SET @description='SO Deleted (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@numQtyShipped AS VARCHAR(10)) + ')'                                IF @Kit = 1				BEGIN					exec USP_UpdatingInventoryForEmbeddedKits @numoppitemtCode,@numOrigUnits,1,@numOppID,@tintMode,@numUserCntID				END				ELSE IF @bitWorkOrder = 1				BEGIN										IF @tintMode=0 -- EDIT					BEGIN						SET @description='SO-WO Edited (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@numQtyShipped AS VARCHAR(10)) + ')'						-- UPDATE WORK ORDER - REVERT INVENTOY OF ASSEMBLY ITEMS						EXEC USP_ManageInventoryAssemblyWO @numUserCntID,@numOppID,@itemcode,@numWareHouseItemID,@numQtyShipped,5					END					ELSE IF @tintMode=1 -- DELETE					BEGIN						DECLARE @numWOID AS NUMERIC(18,0)						DECLARE @numWOStatus AS NUMERIC(18,0)						SELECT @numWOID=numWOId,@numWOStatus=numWOStatus FROM WorkOrder WHERE numOppId=@numOppId AND numItemCode=@itemcode AND numWareHouseItemId=@numWareHouseItemID						SET @description='SO-WO Deleted (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@numQtyShipped AS VARCHAR(10)) + ')'						--IF WORK ORDER IS NOT COMPLETED THAN REMOVE ON ORDER QUANTITY						IF @numWOStatus <> 23184						BEGIN							IF @onOrder >= @numUnits								SET @onOrder = @onOrder - @numUnits							ELSE								SET @onOrder = 0						END						-- IF BACKORDER QTY IS GREATER THEN QTY TO REVERT THEN
						-- DECREASE BACKORDER QTY BY QTY TO REVERT
						IF @numUnits < @onBackOrder 
						BEGIN                  
							SET @onBackOrder = @onBackOrder - @numUnits
						END 
						-- IF BACKORDER QTY IS LESS THEN OR EQUAL TO QTY TO REVERT THEN
						-- DECREASE QTY TO REVERT BY QTY OF BACK ORDER AND
						-- SET BACKORDER QTY TO 0
						ELSE IF @numUnits >= @onBackOrder 
						BEGIN
							SET @numUnits = @numUnits - @onBackOrder
							SET @onBackOrder = 0
                        
							--REMOVE ITEM FROM ALLOCATION 
							IF (@onAllocation - @numUnits) >= 0
								SET @onAllocation = @onAllocation - @numUnits
						
							--ADD QTY TO ONHAND
							SET @onHand = @onHand + @numUnits
						END						UPDATE  WareHouseItems						SET     numOnHand = @onHand ,								numAllocation = @onAllocation,								numBackOrder = @onBackOrder,								numOnOrder = @onOrder,								dtModified = GETDATE()						WHERE   numWareHouseItemID = @numWareHouseItemID   						--IF WORK ORDER IS NOT COMPLETED THAN DELETE WORK ORDER AND ITS CHILDS WORK ORDER						IF @numWOStatus <> 23184						BEGIN							EXEC USP_DeleteAssemblyWOAndInventory  @numDomain,@numUserCntID,@numWOID,@numOppID						END					END				END					ELSE					BEGIN	                          IF @numQtyShipped>0								 BEGIN									IF @tintMode=0											SET @numUnits = @numUnits - @numQtyShipped									ELSE IF @tintmode=1											SET @onAllocation = @onAllocation + @numQtyShipped 								  END 								                                        IF @numUnits >= @onBackOrder                         BEGIN                            SET @numUnits = @numUnits - @onBackOrder                            SET @onBackOrder = 0                                                        IF (@onAllocation - @numUnits >= 0)								SET @onAllocation = @onAllocation - @numUnits								IF @bitAsset=1--Not Asset										BEGIN											  SET @onHand = @onHand +@numRentalIN+@numRentalLost+@numRentalOut     										END								ELSE										BEGIN											 SET @onHand = @onHand + @numUnits     										END                                                                                         END                                                                ELSE                         IF @numUnits < @onBackOrder                             BEGIN                  								IF (@onBackOrder - @numUnits >0)									SET @onBackOrder = @onBackOrder - @numUnits									--								IF @tintmode=1--									SET @onAllocation = @onAllocation + (@numUnits - @numQtyShipped)                            END                  							UPDATE  WareHouseItems						SET     numOnHand = @onHand ,								numAllocation = @onAllocation,								numBackOrder = @onBackOrder,								dtModified = GETDATE()						WHERE   numWareHouseItemID = @numWareHouseItemID       										                                                                                 				END								IF @numWareHouseItemID>0				BEGIN 						EXEC dbo.USP_ManageWareHouseItems_Tracking						@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)						@numReferenceID = @numOppId, --  numeric(9, 0)						@tintRefType = 3, --  tinyint						@vcDescription = @description, --  varchar(100)						@numModifiedBy = @numUserCntID,						@numDomainID = @numDomain 				END		          END                          IF @bitStockTransfer=1            BEGIN				SET @numWareHouseItemID = @numToWarehouseItemID;				SET @OppType = 2				SET @numUnits = @numOrigUnits				SELECT  @onHand = ISNULL(numOnHand, 0),                    @onAllocation = ISNULL(numAllocation, 0),                    @onOrder = ISNULL(numOnOrder, 0),                    @onBackOrder = ISNULL(numBackOrder, 0)				FROM    WareHouseItems				WHERE   numWareHouseItemID = @numWareHouseItemID   			END                    IF @OppType = 2                     BEGIN 					SET @description='PO Deleted (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Received:' +  CAST(@numUnitHourReceived AS VARCHAR(10)) + ')'						--Updating the Average Cost--                        IF @onHand + @onOrder - @numUnits <> 0 --                            BEGIN--                                SET @monAvgCost = ( ( @onHand + @onOrder )--                                                    * @monAvgCost - @monAmount )--                                    / ( @onHand + @onOrder - @numUnits )--                            END--                        ELSE --                            SET @monAvgCost = 0--                        UPDATE  item--                        SET     monAverageCost = @monAvgCost--                        WHERE   numItemCode = @itemcode										    --Partial Fulfillment						IF @tintmode=1 and  @onHand >= @numUnitHourReceived						BEGIN							SET @onHand= @onHand - @numUnitHourReceived							--SET @onOrder= @onOrder + @numUnitHourReceived						END											    SET @numUnits = @numUnits - @numUnitHourReceived 						IF (@onOrder - @numUnits)>=0						BEGIN							--Causing Negative Inventory Bug ID:494							SET @onOrder = @onOrder - @numUnits							END						ELSE IF (@onHand + @onOrder) - @numUnits >= 0						BEGIN													SET @onHand = @onHand - (@numUnits-@onOrder)							SET @onOrder = 0						END						ELSE IF  (@onHand + @onOrder + @onAllocation) - @numUnits >= 0						BEGIN							Declare @numDiff numeric								SET @numDiff = @numUnits - @onOrder							SET @onOrder = 0							SET @numDiff = @numDiff - @onHand							SET @onHand = 0							SET @onAllocation = @onAllocation - @numDiff							SET @onBackOrder = @onBackOrder + @numDiff						END					                            UPDATE  WareHouseItems                        SET     numOnHand = @onHand,                                numAllocation = @onAllocation,                                numBackOrder = @onBackOrder,                                numOnOrder = @onOrder,							dtModified = GETDATE()                        WHERE   numWareHouseItemID = @numWareHouseItemID                                                   EXEC dbo.USP_ManageWareHouseItems_Tracking						@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)						@numReferenceID = @numOppId, --  numeric(9, 0)						@tintRefType = 4, --  tinyint						@vcDescription = @description, --  varchar(100)						@numModifiedBy = @numUserCntID,						@numDomainID = @numDomain                                                              END                                                                               SELECT TOP 1                    @numoppitemtCode = numoppitemtCode,                    @itemcode = OI.numItemCode,                    @numUnits = numUnitHour,                    @numWareHouseItemID = ISNULL(numWarehouseItmsID,0),                    @Kit = ( CASE WHEN bitKitParent = 1                                       AND bitAssembly = 1 THEN 0                                  WHEN bitKitParent = 1 THEN 1                                  ELSE 0                             END ),                    @monAmount = ISNULL(monTotAmount,0)  * (CASE WHEN ISNULL(fltExchangeRate,0)=0 THEN 1 ELSE fltExchangeRate END),                    @monAvgCost = monAverageCost,                    @numQtyShipped = ISNULL(numQtyShipped,0),					@numUnitHourReceived = ISNULL(numUnitHourReceived,0),					@bitKitParent=ISNULL(bitKitParent,0),					@numToWarehouseItemID =OI.numToWarehouseItemID,					@bitStockTransfer = ISNULL(OM.bitStockTransfer,0),					@OppType = tintOppType            FROM    OpportunityItems OI					JOIN dbo.OpportunityMaster OM ON OI.numOppId = OM.numOppId                    JOIN Item I ON OI.numItemCode = I.numItemCode                                               WHERE   (charitemtype='P' OR 1=(CASE WHEN tintOppType=1 THEN 							CASE WHEN ISNULL(I.bitKitParent,0) = 1 AND ISNULL(I.bitAssembly,0) = 1 THEN 0								 WHEN ISNULL(I.bitKitParent,0) = 1 THEN 1								 ELSE 0 END 							ELSE 0 END))  							AND OI.numOppId = @numOppId                     AND OI.numoppitemtCode > @numoppitemtCode                    AND ( bitDropShip = 0                          OR bitDropShip IS NULL                        )            ORDER BY OI.numoppitemtCode                                                          IF @@rowcount = 0                 SET @numoppitemtCode = 0              END
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_SalesFulfillmentUpdateInventory')
DROP PROCEDURE USP_SalesFulfillmentUpdateInventory
GO
CREATE PROCEDURE  USP_SalesFulfillmentUpdateInventory
    @numDomainID NUMERIC(9),
    @numOppID NUMERIC(9),
    @numOppBizDocsId NUMERIC(9),
    @numUserCntID NUMERIC(9),
    @tintMode TINYINT --1:Release 2:Revert
  AS 
BEGIN
BEGIN TRY
BEGIN TRANSACTION      
		DECLARE @numDomain AS NUMERIC(9)
		SET @numDomain = @numDomainID
        
		DECLARE @onAllocation AS FLOAT
		DECLARE @numWarehouseItemID AS NUMERIC
		DECLARE @numOppItemID AS NUMERIC
		DECLARE @numUnits FLOAT    
        DECLARE @numOldQtyShipped AS FLOAT
		DECLARE @numNewQtyShipped AS FLOAT
		      
        DECLARE @Kit AS BIT,@vcItemName AS VARCHAR(300)                   
        DECLARE @vcError VARCHAR(500) 
		DECLARE @description AS VARCHAR(100)
         
        DECLARE @minRowNumber NUMERIC(9),@maxRowNumber NUMERIC(9)
         
        DECLARE @minKitRowNumber NUMERIC(9)
		DECLARE @maxKitRowNumber NUMERIC(9)
		DECLARE @KitWareHouseItemID NUMERIC(9)
		DECLARE @KitonAllocation AS FLOAT
		DECLARE @KitQtyShipped AS FLOAT
		DECLARE @KiyQtyItemsReq_Orig AS FLOAT
		DECLARE @KiyQtyItemsReq as FLOAT  
		DECLARE @Kitdescription AS VARCHAR(100)
		DECLARE @KitNewQtyShipped AS FLOAT
		DECLARE @KitOppChildItemID AS NUMERIC(9)
						
        SELECT 
			OBDI.numOppItemID,
			ISNULL(OBDI.numWarehouseItmsID, 0) AS numWarehouseItemID,
			ISNULL(OI.numQtyShipped,0) AS numQtyShipped,
			OBDI.numUnitHour,
			I.vcItemName,
			(CASE WHEN bitKitParent=1 AND bitAssembly=1 THEN 0 WHEN bitKitParent=1 THEN 1 ELSE 0 END) AS KIT,
			ROW_NUMBER() OVER(ORDER BY OBDI.numOppItemID) AS RowNumber 
		INTO 
			#tempItems
        FROM  
			dbo.OpportunityBizDocs OBD 
		JOIN 
			dbo.OpportunityBizDocItems OBDI 
		ON 
			OBD.numOppBizDocsId=OBDI.numOppBizDocID
		JOIN 
			OpportunityItems OI 
		ON 
			OBDI.numOppItemID=OI.numoppitemtCode 
		JOIN 
			Item I 
		ON 
			OI.numItemCode=I.numItemCode
        WHERE 
			OBD.numOppID=@numOppID 
			AND OBD.numOppBizDocsId=@numOppBizDocsId 
			AND OI.numOppID=@numOppID 
        	AND (I.charitemtype='P' OR 1=(CASE WHEN ISNULL(I.bitKitParent,0) = 1 AND ISNULL(I.bitAssembly,0) = 1 THEN 0
											WHEN ISNULL(I.bitKitParent,0) = 1 THEN 1 ELSE 0 END))
			 and (OI.bitDropShip=0 or OI.bitDropShip is null) AND ISNULL(OI.bitWorkOrder,0)=0                                   
											
        SELECT  
			@minRowNumber = MIN(RowNumber),
			@maxRowNumber = MAX(RowNumber) 
		FROM 
			#tempItems
        
        
        WHILE  @minRowNumber <= @maxRowNumber
		BEGIN
			SELECT 
				@numWarehouseItemID=numWarehouseItemID,
				@numOppItemID=numOppItemID,
				@numOldQtyShipped=ISNULL(numQtyShipped,0),
				@numUnits=numUnitHour,
				@Kit=Kit 
			FROM 
				#tempItems 
			WHERE 
				RowNumber=@minRowNumber
			
			IF @tintMode=1 --1:Release
			BEGIN
				SET @numNewQtyShipped = @numUnits-@numOldQtyShipped;
					  		
				IF @numNewQtyShipped>0
				BEGIN
					SET @description='SO Qty Shipped (Qty:' + CAST(@numUnits AS VARCHAR(18)) + ' Shipped:' +  CAST(@numNewQtyShipped AS VARCHAR(18)) + ')'
						 
					IF @Kit=1
					BEGIN
						SELECT 
							OKI.numOppChildItemID,
							OKI.numWareHouseItemId,
							numQtyItemsReq,
							numQtyItemsReq_Orig,
							numQtyShipped,
							ISNULL(numAllocation, 0) AS numAllocation,
							ROW_NUMBER() OVER(ORDER BY numOppChildItemID) AS RowNumber 
						INTO 
							#tempKits
						FROM 
							OpportunityKitItems OKI 
						JOIN 
							Item I 
						ON 
							OKI.numChildItemID=I.numItemCode  
						JOIN 
							WareHouseItems WI 
						ON 
							WI.numWareHouseItemID=OKI.numWareHouseItemID  
						WHERE 
							charitemtype='P' 
							AND OKI.numWareHouseItemId > 0 
							AND OKI.numWareHouseItemId IS NOT NULL
							AND numOppItemID=@numOppItemID 
					
						IF(SELECT COUNT(*) FROM #tempKits WHERE numAllocation - ((@numUnits * numQtyItemsReq_Orig) - numQtyShipped) < 0)>0
						BEGIN
							SET @vcError ='You do not have enough KIT inventory to support this shipment('+ @vcItemName +'). Modify your Qty-Shipped to an amount equal to or less than the Qty-On-Allocation and try again.' 
								
							RAISERROR ('NotSufficientQty_Allocation',16,1);
						END
						ELSE
						BEGIN
							SELECT  
								@minKitRowNumber = MIN(RowNumber),
								@maxKitRowNumber = MAX(RowNumber) 
							FROM 
								#tempKits

							WHILE @minKitRowNumber <= @maxKitRowNumber
							BEGIN
								SELECT 
									@KitOppChildItemID=numOppChildItemID,
									@KitWareHouseItemID=numWareHouseItemID,
									@KiyQtyItemsReq=numQtyItemsReq,
									@KitQtyShipped=numQtyShipped,
									@KiyQtyItemsReq_Orig=numQtyItemsReq_Orig,
									@KitonAllocation=numAllocation 
								FROM 
									#tempKits 
								WHERE 
									RowNumber=@minKitRowNumber
						
								SET @KitNewQtyShipped = (@numNewQtyShipped * @KiyQtyItemsReq_Orig) - @KitQtyShipped;
						
								SET @Kitdescription='SO KIT Qty Shipped (Qty:' + CAST(@KiyQtyItemsReq AS VARCHAR(10)) + ' Shipped:' +  CAST(@KitNewQtyShipped AS VARCHAR(10)) + ')'

								SET @KitonAllocation = @KitonAllocation - @KitNewQtyShipped
			        
								IF (@KitonAllocation >= 0 )
								BEGIN
									UPDATE
										WareHouseItems
									SET
										numAllocation = @KitonAllocation,
										dtModified = GETDATE() 
									WHERE 
										numWareHouseItemID = @KitWareHouseItemID      	
									
									UPDATE  
										[OpportunityKitItems]
									SET 
										[numQtyShipped] = @numUnits * @KiyQtyItemsReq_Orig 
									WHERE 
										[numOppChildItemID] = @KitOppChildItemID
						                
									EXEC dbo.USP_ManageWareHouseItems_Tracking
											@numWareHouseItemID = @KitWareHouseItemID, --  numeric(9, 0)
											@numReferenceID = @numOppId, --  numeric(9, 0)
											@tintRefType = 3, --  tinyint
											@vcDescription = @Kitdescription, --  varchar(100)
											@numModifiedBy = @numUserCntID,
											@numDomainID = @numDomain 
								END
			            
								SET @minKitRowNumber=@minKitRowNumber + 1			 		
							END				
						   
							UPDATE  
								[OpportunityItems]
							SET 
								[numQtyShipped] = @numUnits
							WHERE 
								[numoppitemtCode] = @numOppItemID
				                
							EXEC dbo.USP_ManageWareHouseItems_Tracking
									@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
									@numReferenceID = @numOppId, --  numeric(9, 0)
									@tintRefType = 3, --  tinyint
									@vcDescription = @description, --  varchar(100)
									@numModifiedBy = @numUserCntID,
									@numDomainID = @numDomain
						 END
					END
					ELSE
					BEGIN
						SELECT
							@onAllocation = ISNULL(numAllocation, 0) 
						FROM 
							WareHouseItems 
						WHERE 
							numWareHouseItemID = @numWareHouseItemID
				        
						SET @onAllocation = @onAllocation - @numNewQtyShipped	
				        
						IF (@onAllocation >= 0 )
						BEGIN
							UPDATE  
								WareHouseItems
							SET 
								numAllocation = @onAllocation,dtModified = GETDATE() 
							WHERE
								numWareHouseItemID = @numWareHouseItemID      	
							
							UPDATE  
								[OpportunityItems]
							SET 
								[numQtyShipped] = @numUnits
							WHERE
								[numoppitemtCode] = @numOppItemID AND numOppId=@numOppId
				                
							EXEC dbo.USP_ManageWareHouseItems_Tracking
									@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
									@numReferenceID = @numOppId, --  numeric(9, 0)
									@tintRefType = 3, --  tinyint
									@vcDescription = @description, --  varchar(100)
									@numModifiedBy = @numUserCntID,
									@numDomainID = @numDomain 
						END
						ELSE
						BEGIN
							SET @vcError ='You do not have enough inventory to support this shipment('+ @vcItemName +'). Modify your Qty-Shipped to an amount equal to or less than the Qty-On-Allocation and try again.' 
							RAISERROR ('NotSufficientQty_Allocation',16,1);
						END
					END
				END	 
			END
			ELSE IF @tintMode=2 --2:Revert
			BEGIN
				DECLARE @numAuthInvoice NUMERIC(18,0)
				
				SELECT 
					@numAuthInvoice=ISNULL(numAuthoritativeSales,0) 
				FROM 
					dbo.AuthoritativeBizDocs 
				WHERE 
					AuthoritativeBizDocs.numDomainId=@numDomainId 
				
				IF EXISTS (SELECT numOppBizDocsId FROM OpportunityMaster OM JOIN OpportunityBizDocs OBD ON OM.numOppID=OBD.numOppID
					WHERE OM.numDomainId=@numDomainId AND OM.numOppID=@numOppID  AND OBD.numBizDocId IN (@numAuthInvoice))
				BEGIN
					RAISERROR ('AlreadyInvoice_DoNotReturn',16,1);
					RETURN -1
				END		
						
				IF @numOldQtyShipped>0
				BEGIN
					SET @description='SO Qty Shipped Return (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped Return:' +  CAST(@numOldQtyShipped AS VARCHAR(10)) + ')'
					
					IF @Kit=1
					BEGIN
						SELECT 
							OKI.numOppChildItemID,
							OKI.numWareHouseItemId,
							numQtyItemsReq,
							numQtyItemsReq_Orig,
							numQtyShipped,
							ISNULL(numAllocation, 0) AS numAllocation,
							ROW_NUMBER() OVER(ORDER BY numOppChildItemID) AS RowNumber 
						INTO 
							#tempKitsReturn
						FROM 
							OpportunityKitItems OKI 
						JOIN 
							Item I 
						ON 
							OKI.numChildItemID=I.numItemCode  
						JOIN 
							WareHouseItems WI 
						ON 
							WI.numWareHouseItemID=OKI.numWareHouseItemID  
						WHERE 
							charitemtype='P' 
							AND OKI.numWareHouseItemId > 0 
							AND OKI.numWareHouseItemId IS NOT NULL 
							AND numOppItemID=@numOppItemID 
					
						SELECT  
							@minKitRowNumber = MIN(RowNumber),
							@maxKitRowNumber = MAX(RowNumber) 
						FROM 
							#tempKitsReturn

						WHILE  @minKitRowNumber <= @maxKitRowNumber
						BEGIN
							SELECT 
								@KitOppChildItemID=numOppChildItemID,
								@KitWareHouseItemID=numWareHouseItemID,
								@KiyQtyItemsReq=numQtyItemsReq,
								@KitQtyShipped=numQtyShipped,
								@KiyQtyItemsReq_Orig=numQtyItemsReq_Orig,
								@KitonAllocation=numAllocation 
							FROM 
								#tempKits 
							WHERE 
								RowNumber=@minKitRowNumber
						
							SET @KitNewQtyShipped = (@numOldQtyShipped * @KiyQtyItemsReq_Orig);
						
							SET @Kitdescription='SO KIT Qty Shipped Return (Qty:' + CAST(@KiyQtyItemsReq AS VARCHAR(10)) + ' Shipped:' +  CAST(@KitNewQtyShipped AS VARCHAR(10)) + ')'

							SET @KitonAllocation = @KitonAllocation + @KitNewQtyShipped
			        
							UPDATE  
								WareHouseItems
							SET 
								numAllocation = @KitonAllocation,
								dtModified = GETDATE() 
							WHERE
								numWareHouseItemID = @KitWareHouseItemID      	
									
							UPDATE 
								[OpportunityKitItems]
							SET 
								[numQtyShipped] = 0
							WHERE 
								[numOppChildItemID] = @KitOppChildItemID
						                
							EXEC dbo.USP_ManageWareHouseItems_Tracking
									@numWareHouseItemID = @KitWareHouseItemID, --  numeric(9, 0)
									@numReferenceID = @numOppId, --  numeric(9, 0)
									@tintRefType = 3, --  tinyint
									@vcDescription = @Kitdescription, --  varchar(100)
									@numModifiedBy = @numUserCntID,
									@numDomainID = @numDomain 
			            
							SET @minKitRowNumber=@minKitRowNumber + 1			 		
						END				
						   
						UPDATE  
							[OpportunityItems]
						SET 
							[numQtyShipped] = @numUnits
						WHERE 
							[numoppitemtCode] = @numOppItemID
				                
						EXEC dbo.USP_ManageWareHouseItems_Tracking
								@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
								@numReferenceID = @numOppId, --  numeric(9, 0)
								@tintRefType = 3, --  tinyint
								@vcDescription = @description, --  varchar(100)
								@numModifiedBy = @numUserCntID,
								@numDomainID = @numDomain 
					END
					ELSE
					BEGIN
						 SELECT @onAllocation = ISNULL(numAllocation, 0) FROM    WareHouseItems WHERE   numWareHouseItemID = @numWareHouseItemID
				        
						 SET @onAllocation = @onAllocation + @numOldQtyShipped	
				        
						IF (@onAllocation >= 0 )
						BEGIN
							UPDATE  
								WareHouseItems
							SET
								numAllocation = @onAllocation,
								dtModified = GETDATE() 
							WHERE 
								numWareHouseItemID = @numWareHouseItemID      	
							
							UPDATE
								[OpportunityItems]
							SET 
								[numQtyShipped] = 0
							WHERE 
								[numoppitemtCode] = @numOppItemID 
								AND numOppId=@numOppId
				                
							EXEC dbo.USP_ManageWareHouseItems_Tracking
									@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
									@numReferenceID = @numOppId, --  numeric(9, 0)
									@tintRefType = 3, --  tinyint
									@vcDescription = @description, --  varchar(100)
									@numModifiedBy = @numUserCntID,
									@numDomainID = @numDomain 
						END
					END 			
				END
				ELSE
				BEGIN
					SET @vcError ='You have not shipped qty for ('+ @vcItemName +').' 
					RAISERROR ('NoQty_ForShipped',16,1);
				END
			END
				  		
			SET @minRowNumber=@minRowNumber + 1			 		
		END	

COMMIT
END TRY
BEGIN CATCH
  -- Whoops, there was an error
  IF @@TRANCOUNT > 0
     ROLLBACK

  -- Raise an error with the details of the exception
  DECLARE @ErrMsg nvarchar(4000), @ErrSeverity int
  SELECT @ErrMsg = ERROR_MESSAGE(),
         @ErrSeverity = ERROR_SEVERITY()

  RAISERROR(@ErrMsg, @ErrSeverity, 1)
END CATCH
END
GO
    
/****** Object:  StoredProcedure [dbo].[USP_SaveJournalDetails]    Script Date: 07/26/2008 16:21:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Create by Siva                                                                                        
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_SaveJournalDetails' ) 
    DROP PROCEDURE USP_SaveJournalDetails
GO
CREATE PROCEDURE [dbo].[USP_SaveJournalDetails]
    @numJournalId AS NUMERIC(9) = 0 ,
    @strRow AS TEXT = '' ,
    @Mode AS TINYINT = 0 ,
    @numDomainId AS NUMERIC(9) = 0                       
--@RecurringMode as tinyint=0,    
AS 
BEGIN           
                
BEGIN TRY 
		-- INCLUDE ENCODING OTHER WISE IT WILL THROW ERROR WHEN SPECIAL CHARACTERS ARE AVAILABLE IN DESCRIPT FIELDS
		IF CHARINDEX('<?xml',@strRow) = 0
		BEGIN
			SET @strRow = CONCAT('<?xml version="1.0" encoding="ISO-8859-1"?>',@strRow)
		END

        BEGIN TRAN  
        
        ---Always set default Currency ID as base currency ID of domain, and Rate =1
        DECLARE @numBaseCurrencyID NUMERIC
        SELECT @numBaseCurrencyID = ISNULL(numCurrencyID,0) FROM dbo.Domain WHERE numDomainId=@numDomainId
        
        IF CONVERT(VARCHAR(100), @strRow) <> '' 
            BEGIN                                                                                                            
                DECLARE @hDoc3 INT                                                                                                                                                                
                EXEC sp_xml_preparedocument @hDoc3 OUTPUT, @strRow                                                                                                             
--                PRINT '---Insert xml into temp table ---'
                    SELECT numTransactionId ,numDebitAmt ,numCreditAmt ,numChartAcntId ,varDescription ,numCustomerId ,/*numDomainId ,*/bitMainDeposit ,bitMainCheck ,bitMainCashCredit ,numoppitemtCode ,chBizDocItems ,vcReference ,numPaymentMethod ,bitReconcile , 
                    ISNULL(NULLIF(numCurrencyID,0),@numBaseCurrencyID) AS numCurrencyID  ,ISNULL(NULLIF(fltExchangeRate,0.0000),1) AS fltExchangeRate ,numTaxItemID ,numBizDocsPaymentDetailsId ,numcontactid ,numItemID ,numProjectID ,numClassID ,numCommissionID ,numReconcileID ,bitCleared ,tintReferenceType ,numReferenceID ,numCampaignID 
                     INTO #temp FROM      OPENXML (@hdoc3,'/ArrayOfJournalEntryNew/JournalEntryNew',2)
												WITH (
												numTransactionId numeric(10, 0),numDebitAmt money,numCreditAmt money,numChartAcntId numeric(18, 0),varDescription varchar(1000),numCustomerId numeric(18, 0),/*numDomainId numeric(18, 0),*/bitMainDeposit bit,bitMainCheck bit,bitMainCashCredit bit,numoppitemtCode numeric(18, 0),chBizDocItems nchar(10),vcReference varchar(500),numPaymentMethod numeric(18, 0),bitReconcile bit,numCurrencyID numeric(18, 0),fltExchangeRate float,numTaxItemID numeric(18, 0),numBizDocsPaymentDetailsId numeric(18, 0),numcontactid numeric(9, 0),numItemID numeric(18, 0),numProjectID numeric(18, 0),numClassID numeric(18, 0),numCommissionID numeric(9, 0),numReconcileID numeric(18, 0),bitCleared bit,tintReferenceType tinyint,numReferenceID numeric(18, 0),numCampaignID NUMERIC(18,0)
													)

                 EXEC sp_xml_removedocument @hDoc3 
                                    

/*-----------------------Validation of balancing entry*/

IF EXISTS(SELECT * FROM #temp)
BEGIN
	DECLARE @SumTotal MONEY 
	SELECT @SumTotal = SUM(numDebitAmt)-SUM(numCreditAmt) FROM #temp

--	PRINT cast( @SumTotal AS DECIMAL(10,4))
	IF cast( @SumTotal AS DECIMAL(10,4)) <> 0.0000
	BEGIN
		 INSERT INTO [dbo].[GenealEntryAudit]
           ([numDomainID]
           ,[numJournalID]
           ,[numTransactionID]
           ,[dtCreatedDate]
           ,[varDescription])
		 SELECT @numDomainID,
				@numJournalId,
				0,
				GETUTCDATE(),
				'IM_BALANCE'

		 RAISERROR ( 'IM_BALANCE', 16, 1 ) ;
		 RETURN;
	END

	
END



DECLARE @numEntryDateSortOrder AS NUMERIC
DECLARE @datEntry_Date AS DATETIME
--Combine Date from datentryDate with time part of current time
SELECT @datEntry_Date=( CAST( CONVERT(VARCHAR,DATEPART(year, datEntry_Date)) + '-' + RIGHT('00000'+ CONVERT(VARCHAR,DATEPART(month, datEntry_Date)),2)+ 
+ '-' + CONVERT(VARCHAR,DATEPART(day, datEntry_Date))
+ ' ' + CONVERT(VARCHAR,DATEPART(hour, GETDATE()))
+ ':' + CONVERT(VARCHAR,DATEPART(minute, GETDATE()))
+ ':' + CONVERT(VARCHAR,DATEPART(second, GETDATE())) AS DATETIME)  
) 
 from dbo.General_Journal_Header WHERE numJournal_Id=@numJournalId AND numDomainId=@numDomainId

SET @numEntryDateSortOrder = convert(numeric,
RIGHT('00000'+ CONVERT(VARCHAR,DATEPART(year, @datEntry_Date)),4)+
RIGHT('00000'+ CONVERT(VARCHAR,DATEPART(month, @datEntry_Date)),2)+
RIGHT('00000'+ CONVERT(VARCHAR,DATEPART(day, @datEntry_Date)),2)+
RIGHT('00000'+ CONVERT(VARCHAR,DATEPART(hour, @datEntry_Date)),2)+
RIGHT('00000'+ CONVERT(VARCHAR,DATEPART(minute, @datEntry_Date)),2)+
RIGHT('00000'+ CONVERT(VARCHAR,DATEPART(second, @datEntry_Date)),2)+
RIGHT('00000'+ CONVERT(VARCHAR,DATEPART(millisecond, @datEntry_Date)),3))

/*-----------------------Validation of balancing entry*/

			

                IF @Mode = 1  /*Edit Journal entries*/
                    BEGIN             
						PRINT '---delete removed transactions ---'
                        DELETE  FROM General_Journal_Details
                        WHERE   numJournalId = @numJournalId
                                AND numTransactionId NOT IN (SELECT numTransactionId FROM #temp as X )
			                                                           
						--Update transactions
						PRINT '---updated existing transactions ---'
                        UPDATE  dbo.General_Journal_Details 
                        SET    		[numDebitAmt] = X.numDebitAmt,
									[numCreditAmt] = X.numCreditAmt,
									[numChartAcntId] = X.numChartAcntId,
									[varDescription] = X.varDescription,
									[numCustomerId] = NULLIF(X.numCustomerId,0),
									[bitMainDeposit] = X.bitMainDeposit,
									[bitMainCheck] = X.bitMainCheck,
									[bitMainCashCredit] = X.bitMainCashCredit,
									[numoppitemtCode] = NULLIF(X.numoppitemtCode,0),
									[chBizDocItems] = X.chBizDocItems,
									[vcReference] = X.vcReference,
									[numPaymentMethod] = X.numPaymentMethod,
									[numCurrencyID] = NULLIF(X.numCurrencyID,0),
									[fltExchangeRate] = X.fltExchangeRate,
									[numTaxItemID] = NULLIF(X.numTaxItemID,0),
									[numBizDocsPaymentDetailsId] = NULLIF(X.numBizDocsPaymentDetailsId,0),
									[numcontactid] = NULLIF(X.numcontactid,0),
									[numItemID] = NULLIF(X.numItemID,0),
									[numProjectID] =NULLIF( X.numProjectID,0),
									[numClassID] = NULLIF(X.numClassID,0),
									[numCommissionID] = NULLIF(X.numCommissionID,0),
									numCampaignID=NULLIF(X.numCampaignID,0),
									numEntryDateSortOrder1=@numEntryDateSortOrder
--									[tintReferenceType] = X.tintReferenceType,
--									[numReferenceID] = X.numReferenceID
                          FROM    #temp as X 
						  WHERE   X.numTransactionId = General_Journal_Details.numTransactionId
			               
		               
					    INSERT INTO [dbo].[GenealEntryAudit]
									([numDomainID]
									,[numJournalID]
									,[numTransactionID]
									,[dtCreatedDate]
									,[varDescription])
						SELECT GJD.numDomainID, GJD.[numJournalId], GJD.[numTransactionId], GETUTCDATE(),[GJD].[varDescription]
						FROM General_Journal_Details AS GJD                                             
						JOIN #temp AS X ON  X.numTransactionId = GJD.numTransactionId
						
                    END 
                IF @Mode = 0 OR @Mode = 1 /*-- Insert journal only*/
                    BEGIN    
						PRINT '---insert existing transactions ---'                                                                                               
                        INSERT  INTO [dbo].[General_Journal_Details]
                                ( [numJournalId] ,
                                  [numDebitAmt] ,
                                  [numCreditAmt] ,
                                  [numChartAcntId] ,
                                  [varDescription] ,
                                  [numCustomerId] ,
                                  [numDomainId] ,
                                  [bitMainDeposit] ,
                                  [bitMainCheck] ,
                                  [bitMainCashCredit] ,
                                  [numoppitemtCode] ,
                                  [chBizDocItems] ,
                                  [vcReference] ,
                                  [numPaymentMethod] ,
                                  [bitReconcile] ,
                                  [numCurrencyID] ,
                                  [fltExchangeRate] ,
                                  [numTaxItemID] ,
                                  [numBizDocsPaymentDetailsId] ,
                                  [numcontactid] ,
                                  [numItemID] ,
                                  [numProjectID] ,
                                  [numClassID] ,
                                  [numCommissionID] ,
                                  [numReconcileID] ,
                                  [bitCleared],
                                  [tintReferenceType],
								  [numReferenceID],[numCampaignID],numEntryDateSortOrder1
	                                
                                )
                                SELECT  @numJournalId ,
                                        [numDebitAmt] ,
                                        [numCreditAmt] ,
                                        CASE WHEN [numChartAcntId]=0 THEN NULL ELSE numChartAcntId END  ,
                                        [varDescription] ,
                                        NULLIF(numCustomerId, 0) ,
                                        @numDomainId ,
                                        [bitMainDeposit] ,
                                        [bitMainCheck] ,
                                        [bitMainCashCredit] ,
                                        NULLIF([numoppitemtCode], 0) ,
                                        [chBizDocItems] ,
                                        [vcReference] ,
                                        [numPaymentMethod] ,
                                        [bitReconcile] ,
                                        NULLIF([numCurrencyID], 0) ,
                                        [fltExchangeRate] ,
                                        NULLIF([numTaxItemID], 0) ,
                                        NULLIF([numBizDocsPaymentDetailsId], 0) ,
                                        NULLIF([numcontactid], 0) ,
                                        NULLIF([numItemID], 0) ,
                                        NULLIF([numProjectID], 0) ,
                                        NULLIF([numClassID], 0) ,
                                        NULLIF([numCommissionID], 0) ,
                                        NULLIF([numReconcileID], 0) ,
                                        [bitCleared],
                                        tintReferenceType,
										NULLIF(numReferenceID,0),NULLIF(numCampaignID,0),@numEntryDateSortOrder
                                        
                                FROM #temp as X    
                                WHERE X.numTransactionId =0
                        
                        --Set Default Class If enable User Level Class Accountng 
						DECLARE @numAccountClass AS NUMERIC(18);SET @numAccountClass=0                             
                        SELECT @numAccountClass=ISNULL(numAccountClass,0) FROM General_Journal_Header WHERE numDomainId=@numDomainId AND numJournal_Id=@numJournalId
                        IF @numAccountClass>0
                        BEGIN
							UPDATE General_Journal_Details SET numClassID=@numAccountClass WHERE numJournalId=@numJournalId AND numDomainId=@numDomainId
						END

						INSERT INTO [dbo].[GenealEntryAudit]
									([numDomainID]
									,[numJournalID]
									,[numTransactionID]
									,[dtCreatedDate]
									,[varDescription])
						SELECT @numDomainID, @numJournalId, X.numTransactionId, GETUTCDATE(),X.varDescription
						FROM #temp AS X WHERE X.numTransactionId = 0

                    END 
                    
                    
                    UPDATE dbo.General_Journal_Header 
						SET numAmount=(SELECT SUM(numDebitAmt) FROM dbo.General_Journal_Details WHERE numJournalID=@numJournalID)
						WHERE numJournal_Id=@numJournalId AND numDomainId=@numDomainId
						
						
                    drop table #temp
                    
                    -- Update leads,prospects to Accounts
					DECLARE @numDivisionID AS NUMERIC(18)
					DECLARE @numUserCntID AS NUMERIC(18)
					
					SELECT TOP 1 @numDivisionID = ISNULL(numCustomerId,0), @numUserCntID = ISNULL(numCreatedBy,0)
					FROM dbo.General_Journal_Details GJD
					JOIN dbo.General_Journal_Header GJH ON GJD.numJournalId = GJH.numJournal_Id AND GJD.numDomainId = GJH.numDomainId
					WHERE numJournalId = @numJournalId 
					AND GJH.numDomainId = @numDomainID
					
					IF @numDivisionID > 0 AND @numUserCntID > 0
					BEGIN
						EXEC dbo.USP_UpdateCRMTypeToAccounts @numDivisionID ,@numDomainID ,@numUserCntID
					END
            END
            
SELECT GJD.numTransactionId,GJD.numEntryDateSortOrder1,Row_number() OVER(ORDER BY GJD.numTransactionId) AS NewSortId  INTO #Temp1 
FROM General_Journal_Details GJD WHERE GJD.numDomainId = @numDomainID and  GJD.numEntryDateSortOrder1  LIKE SUBSTRING(CAST(@numEntryDateSortOrder AS VARCHAR(25)),1,12) + '%'

UPDATE  GJD SET GJD.numEntryDateSortOrder1 = (Temp1.numEntryDateSortOrder1 + Temp1.NewSortId) 
FROM General_Journal_Details GJD INNER JOIN #Temp1 AS Temp1 ON GJD.numTransactionId = Temp1.numTransactionId

IF object_id('TEMPDB.DBO.#Temp1') IS NOT NULL DROP TABLE #Temp1

        
        COMMIT TRAN 
    END TRY 
    BEGIN CATCH		
        DECLARE @strMsg VARCHAR(200)
        SET @strMsg = ERROR_MESSAGE()
        IF ( @@TRANCOUNT > 0 ) 
            BEGIN
				INSERT INTO [dbo].[GenealEntryAudit]
									([numDomainID]
									,[numJournalID]
									,[numTransactionID]
									,[dtCreatedDate]
									,[varDescription])
						SELECT @numDomainID, @numJournalId,0, GETUTCDATE(),@strMsg

                RAISERROR ( @strMsg, 16, 1 ) ;
                ROLLBACK TRAN
                RETURN 1
            END
    END CATCH	

            
--End                                              
---For Recurring Transaction                                              
                                              
--if @RecurringMode=1                                              
--Begin    
--  print 'SIVA--Recurring'    
--  print  '@numDomainId=============='+Convert(varchar(10),@numDomainId)                                     
--  Declare @numMaxJournalId as numeric(9)                                              
--  Select @numMaxJournalId=max(numJournal_Id) From General_Journal_Header Where numDomainId=@numDomainId                                              
--  insert into General_Journal_Details(numJournalId,numDebitAmt,numCreditAmt,numChartAcntId,varDescription,numCustomerId,numDomainId,bitMainCheck,bitMainCashCredit)                                              
--  Select @numMaxJournalId,numDebitAmt,numCreditAmt,numChartAcntId,varDescription,NULLIF(numCustomerId,0),numDomainId,bitMainCheck,bitMainCashCredit from General_Journal_Details Where numJournalId=@numJournalId  And numDomainId=@numDomainId                         
--   
--                   
--/*Commented by chintan Opening balance Will be updated by Trigger*/
----Exec USP_UpdateChartAcntOpnBalanceForJournalEntry @JournalId=@numMaxJournalId,@numDomainId=@numDomainId                                              
--End           
    END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_SimpleSearch')
DROP PROCEDURE USP_SimpleSearch
GO
CREATE PROCEDURE [dbo].[USP_SimpleSearch]                             
	@numDomainID NUMERIC (18,0),
	@numUserCntID NUMERIC(18,0),
	@searchText VARCHAR(100),
	@searchType VARCHAR(1),
	@searchCriteria VARCHAR(1),
	@orderType VARCHAR(1),
	@bizDocType AS INT ,
	@numSkip AS INT                                                                   
AS                          
	DECLARE @strSQL AS VARCHAR(MAX) = ''
	
	IF @searchType = '1' -- Organization
	BEGIN
		IF @searchCriteria = '1' --Organizations
		BEGIN
			EXEC USP_CompanyInfo_Search @numDomainID=@numDomainID,@numUserCntID=@numUserCntID,@isStartWithSearch=0,@searchText=@searchText
		END
		ELSE
		BEGIN
			--GET FIELDS CONFIGURED FOR ORGANIZATION SEARCH
			SELECT * INTO #tempOrgSearch FROM
			(
				SELECT 
					numFieldId,
					vcDbColumnName,
					ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName,
					vcLookBackTableName,
					tintRow AS tintOrder,
					0 as Custom
				FROM 
					View_DynamicColumns
				WHERE 
					numFormId=97 AND numUserCntID=@numUserCntID AND numDomainID=@numDomainID AND 
					tintPageType=1 AND bitCustom=0  AND ISNULL(bitSettingField,0)=1 AND numRelCntType=0
				UNION   
				SELECT 
					numFieldId,
					vcFieldName,
					vcFieldName,
					'CFW_FLD_Values',
					tintRow AS tintOrder,
					1 as Custom
				FROM 
					View_DynamicCustomColumns   
				WHERE 
					Grp_id=1 AND numFormId=97 AND numUserCntID=@numUserCntID AND numDomainID=@numDomainID AND 
					tintPageType=1 AND bitCustom=1 AND numRelCntType=0
			)Y
			
			DECLARE @searchSQL AS VARCHAR(MAX) = ''

			IF (SELECT COUNT(*) FROM #tempOrgSearch) > 0
			BEGIN
				SELECT  
					@searchSQL = STUFF(
										(SELECT ' OR ' + + CONCAT((CASE WHEN vcLookBackTableName = 'AddressDetails' THEN '' ELSE CONCAT(vcLookBackTableName,'.') END),IIF(Custom = 1,'Fld_Value',vcDbColumnName)) + ' LIKE ''%' + @searchText + '%''' FROM #tempOrgSearch FOR XML PATH(''))
										, 1
										, 4
										, ''
										)
			END
			ELSE
			BEGIN
				SET @searchSQL = 'CompanyInfo.vcCompanyName LIKE ''%' + @searchText + '%'''
			END

			CREATE TABLE #TEMPOrganization
			(
				numDivisionID NUMERIC(18,0),
				vcCompanyName VARCHAR(500)
			)


			SET @strSQL = 'INSERT INTO 
								#TEMPOrganization
							SELECT
								DivisionMaster.numDivisionID
								,CompanyInfo.vcCompanyName
							FROM
								DivisionMaster 
							INNER JOIN
								CompanyInfo 
							ON
								CompanyInfo.numCompanyId = DivisionMaster.numCompanyID
							LEFT JOIN
								AdditionalContactsInformation
							ON
								AdditionalContactsInformation.numDivisionId = DivisionMaster.numDivisionID
							LEFT JOIN
								OpportunityMaster
							ON
								OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
							LEFT JOIN
								Cases
							ON
								Cases.numDivisionID = DivisionMaster.numDivisionID
							LEFT JOIN
								ProjectsMaster
							ON
								ProjectsMaster.numDivisionId = DivisionMaster.numDivisionID
							OUTER APPLY 
								(
									SELECT
										isnull(vcStreet,'''') as vcBillStreet, 
										isnull(vcCity,'''') as vcBillCity, 
										isnull(dbo.fn_GetState(numState),'''') as numBillState,
										isnull(vcPostalCode,'''') as vcBillPostCode,
										isnull(dbo.fn_GetListName(numCountry,0),'''')  as numBillCountry
									FROM
										dbo.AddressDetails 
									WHERE 
										numDomainID = ' + CAST(@numDomainID AS VARCHAR) + '
										AND numRecordID=DivisionMaster.numDivisionID 
										AND tintAddressOf=2 
										AND tintAddressType=1 
								)  AD1
							OUTER APPLY
								(
									SELECT
										isnull(vcStreet,'''') as vcShipStreet,
										isnull(vcCity,'''') as vcShipCity,
										isnull(dbo.fn_GetState(numState),'''')  as numShipState,
										isnull(vcPostalCode,'''') as vcShipPostCode, 
										isnull(dbo.fn_GetListName(numCountry,0),'''') numShipCountry
									FROM
										dbo.AddressDetails 
									WHERE 
										numDomainID = ' + CAST(@numDomainID AS VARCHAR) + '
										AND numRecordID= DivisionMaster.numDivisionID 
										AND tintAddressOf=2 
										AND tintAddressType=2	
								) AD2	
							LEFT JOIN
								CFW_FLD_Values
							ON
								CFW_FLD_Values.RecId = DivisionMaster.numDivisionID
								AND CFW_FLD_Values.Fld_ID IN (SELECT numFieldID FROM #tempOrgSearch)
							WHERE
								DivisionMaster.numDomainID = ' + CAST(@numDomainID AS VARCHAR) + '
								AND (' + @searchSQL + ')
							GROUP BY
								DivisionMaster.numDivisionID
								,CompanyInfo.vcCompanyName'

			PRINT @strSQL
			EXEC (@strSQL)
			IF @searchCriteria = '2' --Items
			BEGIN
				DECLARE @TEMPORGITEM TABLE
				(
					numOppId NUMERIC(18,0),
					CreatedDate DATETIME,
					vcItemName VARCHAR(500),
					vcPOppName VARCHAR(200),
					vcOppType VARCHAR(50),
					numUnitHour FLOAT,
					monPrice NUMERIC(18,2),
					vcCompanyName  VARCHAR(200),
					tintType TINYINT
				)

				INSERT INTO @TEMPORGITEM
				(
					numOppId,
					CreatedDate,
					vcItemName,
					vcPOppName,
					vcOppType,
					numUnitHour,
					monPrice,
					vcCompanyName,
					tintType
				)
				SELECT
					numOppId,
					bintCreatedDate,
					vcItemName,
					vcPOppName,
					vcOppType,
					CAST(numUnitHour AS FLOAT) numUnitHour,
					CAST(monPrice AS decimal(18,2)) monPrice,
					vcCompanyName,
					tintType
				FROM
				(
					SELECT
						OpportunityMaster.numOppId,
						OpportunityMaster.bintCreatedDate,
						CONCAT(Item.vcItemName,',',Item.vcModelID) AS vcItemName,
						OpportunityMaster.vcPOppName,
						CASE 
							WHEN tintOppType = 1 THEN 'Sales ' + (CASE WHEN tintOppStatus = 1 THEN 'Order' ELSE 'Opportunity' END)
							WHEN tintOppType = 2 THEN 'Purchase ' + (CASE WHEN tintOppStatus = 1 THEN 'Order' ELSE 'Opportunity' END)
						END vcOppType,
						OpportunityItems.numUnitHour,
						OpportunityItems.monPrice,
						TEMP.vcCompanyName,
						1 AS tintType
					FROM
						OpportunityMaster
					INNER JOIN
						#TEMPOrganization TEMP
					ON
						OpportunityMaster.numDivisionId = TEMP.numDivisionID
					INNER JOIN
						OpportunityItems
					ON
						OpportunityMaster.numOppId = OpportunityItems.numOppId
					INNER JOIN
						Item
					ON
						OpportunityItems.numItemCode = Item.numItemCode
					WHERE
						OpportunityMaster.numDomainId = @numDomainID
					UNION
					SELECT
						ReturnHeader.numReturnHeaderID AS numOppID,
						ReturnHeader.dtCreatedDate AS bintCreatedDate,
						CONCAT(Item.vcItemName,',',Item.vcModelID) AS vcItemName,
						ReturnHeader.vcRMA AS vcPOppName,
						CASE 
							WHEN tintReturnType = 1 THEN 'Sales Return'
							WHEN tintReturnType = 2 THEN 'Purchase Return'
						END vcOppType,
						ReturnItems.numUnitHour,
						ReturnItems.monPrice,
						TEMP.vcCompanyName,
						2 AS tintType
					FROM
						ReturnHeader
					INNER JOIN
						#TEMPOrganization TEMP
					ON
						ReturnHeader.numDivisionId = TEMP.numDivisionID
					INNER JOIN	
						ReturnItems
					ON
						ReturnHeader.numReturnHeaderID = ReturnItems.numReturnHeaderID
					INNER JOIN
						Item
					ON
						Item.numItemCode = ReturnItems.numItemCode
					WHERE
						ReturnHeader.numDomainId = @numDomainID AND tintReturnType IN (1,2)
				) TEMPFinal

				SELECT  
					ISNULL(numOppId,0) AS numOppId,
					ISNULL(dbo.FormatedDateFromDate(CreatedDate,@numDomainID),'') bintCreatedDate,
					ISNULL(vcItemName,'') vcItemName,
					ISNULL(vcPOppName,'') vcPOppName,
					ISNULL(vcOppType,'') vcOppType,
					ISNULL(numUnitHour,0)numUnitHour,
					ISNULL(monPrice,0.00) monPrice,
					ISNULL(vcCompanyName,'') vcCompanyName,
					ISNULL(tintType,'') tintType
				FROM 
					@TEMPORGITEM 
				ORDER BY 
					CreatedDate DESC 
				OFFSET @numSkip ROWS FETCH NEXT 10 ROWS ONLY

				SELECT COUNT(numOppId) AS TotalItems FROM @TEMPORGITEM 
			END
			ELSE IF @searchCriteria = '3' -- Opp/Orders
			BEGIN			
				DECLARE @TEMPORGORDER TABLE
				(
					numOppId NUMERIC(18,0),
					CreatedDate DATETIME,
					vcPOppName VARCHAR(200),
					vcOppType VARCHAR(50),
					vcCompanyName  VARCHAR(200),
					tintType TINYINT
				)

				INSERT INTO 
					@TEMPORGORDER
				SELECT
					numOppId,
					bintCreatedDate,
					vcPOppName,
					vcOppType,
					vcCompanyName,
					tintType
				FROM
				(
					SELECT
						OpportunityMaster.numOppId,
						OpportunityMaster.vcPOppName,
						CASE 
							WHEN tintOppType = 1 THEN 'Sales ' + (CASE WHEN tintOppStatus = 1 THEN 'Order' ELSE 'Opportunity' END)
							WHEN tintOppType = 2 THEN 'Purchase ' + (CASE WHEN tintOppStatus = 1 THEN 'Order' ELSE 'Opportunity' END)
						END vcOppType,
						TEMP.vcCompanyName,
						bintCreatedDate,
						1 AS tintType
					FROM
						OpportunityMaster
					INNER JOIN
						#TEMPOrganization TEMP
					ON
						OpportunityMaster.numDivisionId = TEMP.numDivisionID
					WHERE
						numDomainId = @numDomainID
						AND tintOppType <> 0
					UNION 
					SELECT
						ReturnHeader.numReturnHeaderID AS numOppID,
						ReturnHeader.vcRMA AS vcPOppName,
						CASE 
							WHEN tintReturnType = 1 THEN 'Sales Return'
							WHEN tintReturnType = 2 THEN 'Purchase Return'
						END vcOppType,
						TEMP.vcCompanyName,
						dtCreatedDate AS bintCreatedDate,
						2 AS tintType
					FROM
						ReturnHeader
					INNER JOIN
						#TEMPOrganization TEMP
					ON
						ReturnHeader.numDivisionId = TEMP.numDivisionID
					WHERE
						numDomainId = @numDomainID AND tintReturnType IN (1,2)
				) TEMPFinal

				SELECT
					ISNULL(numOppId,0) numOppId,
					ISNULL(dbo.FormatedDateFromDate(CreatedDate,@numDomainID),'') bintCreatedDate,
					ISNULL(vcPOppName,'') vcPOppName,
					ISNULL(vcOppType,'') vcOppType,
					ISNULL(vcCompanyName,'') vcCompanyName,
					ISNULL(tintType,0) tintType
				FROM
					@TEMPORGORDER
				ORDER BY
					CreatedDate DESC
				OFFSET @numSkip ROWS FETCH NEXT 10 ROWS ONLY	
				
				SELECT COUNT(*) AS TotalRows FROM @TEMPORGORDER			
			END
			ELSE IF @searchCriteria = '4' -- BizDocs
			BEGIN
				DECLARE @TEMPORGBizDoc TABLE
				(
					numOppBizDocsId NUMERIC(18,0),
					CreatedDate DATETIME,
					vcBizDocID VARCHAR(200),
					vcBizDocType VARCHAR(100),
					vcCompanyName  VARCHAR(200)
				)

				INSERT INTO
					@TEMPORGBizDoc
				SELECT 
					numOppBizDocsId,
					OpportunityBizDocs.dtCreatedDate,
					vcBizDocID,
					ListDetails.vcData AS vcBizDocType,
					TEMP.vcCompanyName
				FROM 
					OpportunityBizDocs
				INNER JOIN
					OpportunityMaster
				ON
					OpportunityBizDocs.numOppId = OpportunityMaster.numOppId
				INNER JOIN
					#TEMPOrganization TEMP
				ON
					OpportunityMaster.numDivisionId = TEMP.numDivisionID
				INNER JOIN
					ListDetails
				ON
					ListDetails.numListID = 27
					AND OpportunityBizDocs.numBizDocId = ListDetails.numListItemID
				WHERE
					OpportunityMaster.numDomainId = @numDomainID

				SELECT
					ISNULL(numOppBizDocsId,0) numOppBizDocsId,
					ISNULL(dbo.FormatedDateFromDate(CreatedDate,@numDomainID),'') AS dtCreatedDate,
					ISNULL(vcBizDocID,'') vcBizDocID,
					ISNULL(vcBizDocType,'') vcBizDocType,
					ISNULL(vcCompanyName,'') vcCompanyName
				FROM
					@TEMPORGBizDoc
				ORDER BY
					CreatedDate DESC
				OFFSET @numSkip ROWS FETCH NEXT 10 ROWS ONLY
				
				SELECT COUNT(*) AS TotalRows FROM @TEMPORGBizDoc					
			END

			DROP TABLE #tempOrgSearch
			DROP TABLE #TEMPOrganization
		END
	END
	ELSE IF @searchType = '2' -- Item
	BEGIN
		CREATE TABLE #TempSearchedItem
		(
			numItemCode NUMERIC(18,0), 
			vcItemName VARCHAR(200), 
			vcPathForTImage VARCHAR(200), 
			vcCompanyName VARCHAR(200), 
			monListPrice NUMERIC(18,0), 
			txtItemDesc TEXT, 
			vcModelID VARCHAR(100), 
			numBarCodeId VARCHAR(100), 
			vcBarCode VARCHAR(100), 
			vcSKU VARCHAR(100),
			vcWHSKU VARCHAR(100), 
			vcPartNo VARCHAR(100),
			numWarehouseItemID NUMERIC(18,0), 
			vcAttributes VARCHAR(500)
		)

		DECLARE @strItemSearch AS VARCHAR(1000) = ''
		DECLARE @strAttributeSearch AS VARCHAR(1000) = ''
		DECLARE @strSearch AS VARCHAR(1000)

		IF CHARINDEX(',',@searchText) > 0
		BEGIN
			SET @strItemSearch = LTRIM(RTRIM(SUBSTRING(@searchText,0,CHARINDEX(',',@searchText))))
			SET @strAttributeSearch = SUBSTRING(@searchText,CHARINDEX(',',@searchText) + 1,LEN(@searchText)) 

			IF LEN(@strAttributeSearch) > 0
			BEGIN
				SET @strAttributeSearch = 'TEMPAttributes.vcAttributes LIKE ''%' + REPLACE(@strAttributeSearch,',','%'' AND TEMPAttributes.vcAttributes LIKE ''%') + '%'''
			END
			ELSE
			BEGIN
				SET @strAttributeSearch = ''
			END
		END
		ELSE
		BEGIN
			SET @strItemSearch = @searchText
		END

		DECLARE @vcCustomDisplayField AS VARCHAR(4000) = ''
		SELECT * INTO #TempItemCustomDisplayFields FROM
		(
			SELECT 
				numFieldId,
				vcFieldName
			FROM 
				View_DynamicCustomColumns   
			WHERE 
				Grp_id=5 AND numFormId=22 AND numUserCntID=@numUserCntID AND numDomainID=@numDomainID AND 
				tintPageType=1 AND bitCustom=1 AND numRelCntType=0
		)TD

		IF (SELECT COUNT(*) FROM #TempItemCustomDisplayFields) > 0
		BEGIN
			SELECT @vcCustomDisplayField = STUFF(
									(SELECT ', ' +  'ISNULL(dbo.GetCustFldValueItem(' + CAST(numFieldId AS VARCHAR) + ',numItemCode),'''') AS [' + vcFieldName + ']' FROM #TempItemCustomDisplayFields FOR XML PATH(''))
									, 1
									, 0
									, ''
								)
		END

		SELECT * INTO #TempItemSearchFields FROM
		(
			SELECT 
				numFieldId,
				vcDbColumnName,
				ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName,
				vcLookBackTableName,
				tintRow AS tintOrder,
				0 as Custom
			FROM 
				View_DynamicColumns
			WHERE 
				numFormId=22 AND numUserCntID=@numUserCntID AND numDomainID=@numDomainID AND 
				tintPageType=1 AND bitCustom=0  AND ISNULL(bitSettingField,0)=1 AND numRelCntType=1
				AND vcDbColumnName <> 'vcAttributes'
			UNION   
			SELECT 
				numFieldId,
				vcFieldName,
				vcFieldName,
				'CFW_FLD_Values',
				tintRow AS tintOrder,
				1 as Custom
			FROM 
				View_DynamicCustomColumns   
			WHERE 
				Grp_id=5 AND numFormId=22 AND numUserCntID=@numUserCntID AND numDomainID=@numDomainID AND 
				tintPageType=1 AND bitCustom=1 AND numRelCntType=1
		)Y

		IF (SELECT COUNT(*) FROM #TempItemSearchFields) > 0
		BEGIN
			SELECT  @strSearch = STUFF(
									(SELECT ' OR ' +  IIF(Custom = 1,'dbo.GetCustFldValueItem('+ CAST(numFieldId AS VARCHAR) +',numItemCode)',vcDbColumnName) + ' LIKE ''%' + @strItemSearch + '%''' FROM #TempItemSearchFields FOR XML PATH(''))
									, 1
									, 3
									, ''
								)
		END
		ELSE
		BEGIN
			SELECT @strSearch = 'vcItemName LIKE ''%' + @strItemSearch + '%'''
		END

		SET @strSQL = 'SELECT
							numItemCode, vcItemName, vcPathForTImage, vcCompanyName, MIN(monListPrice) monListPrice, txtItemDesc, 
							vcModelID, numBarCodeId, MIN(vcBarCode) vcBarCode, vcSKU, MIN(vcWHSKU) vcWHSKU, vcPartNo,
							bitSerialized, numItemGroup, Isarchieve, charItemType, 0 AS numWarehouseItemID, '''' vcAttributes
						FROM
						(
						SELECT
							Item.numItemCode,
							ISNULL(vcItemName,'''') AS vcItemName,
							ISNULL((SELECT TOP 1 vcPathForTImage from ItemImages WHERE numItemCode = Item.numItemCode),'''') as vcPathForTImage,
							ISNULL(vcCompanyName,'''') AS vcCompanyName,
							ISNULL(monListPrice,''0.00'') AS monListPrice,
							ISNULL(txtItemDesc,'''') AS txtItemDesc,
							ISNULL(vcModelID,'''') AS vcModelID,
							ISNULL(numBarCodeId,'''') AS numBarCodeId,
							ISNULL(vcBarCode,'''') AS vcBarCode,
							ISNULL(vcWHSKU,'''') AS vcWHSKU,
							ISNULL(vcSKU,'''') AS vcSKU,
							ISNULL(vcPartNo,'''') AS vcPartNo,
							ISNULL(bitSerialized,0) AS bitSerialized,
							ISNULL(numItemGroup,0) AS numItemGroup,
							ISNULL(Isarchieve,0) AS Isarchieve,
							ISNULL(charItemType,'''') AS charItemType
						FROM
							Item
						LEFT JOIN
							DivisionMaster 
						ON
							Item.numVendorID = DivisionMaster.numDivisionID
						LEFT JOIN
							CompanyInfo
						ON
							DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
						LEFT JOIN
							Vendor
						ON
							Item.numVendorID = Vendor.numVendorID AND
							Vendor.numItemCode = Item.numItemCode
						LEFT JOIN
							WareHouseItems
						ON
							Item.numItemCode = WareHouseItems.numItemID
						WHERE 
							Item.numDomainID = ' + CAST(@numDomainID AS VARCHAR) + ')V WHERE ' + @strSearch  + '
						GROUP BY
							numItemCode, vcItemName, vcPathForTImage, vcCompanyName, txtItemDesc, 
							vcModelID, numBarCodeId, vcSKU, bitSerialized, numItemGroup, Isarchieve, charItemType, vcPartNo'
	
		IF LEN(@strAttributeSearch) > 0
		BEGIN
			SET @strSQL = 'SELECT
								numItemCode, vcItemName, vcPathForTImage, vcCompanyName, TEMPAttributes.monWListPrice AS monListPrice, txtItemDesc, 
								vcModelID, numBarCodeId, TEMPAttributes.vcBarCode, vcSKU, TEMPAttributes.vcWHSKU, vcPartNo, TEMPAttributes.numWareHouseItemID,
								TEMPAttributes.vcAttributes
							FROM
								(' + @strSQL + ') TEMP
							OUTER APPLY
							(
								SELECT 
									WHI.numWareHouseItemID,
									WHI.vcBarCode,
									WHI.vcWHSKU,
									WHI.monWListPrice,
									STUFF(
												(SELECT 
												'', '' +  vcAttribute
												FROM  
												(
												SELECT
													CONCAT(	CFM.Fld_label, '' : '',
													(CASE 
														WHEN CFM.Fld_type = ''SelectBox''
														THEN 
															(select vcData from ListDetails where numListID= CFM.numlistid AND numListItemID=CFVSI.Fld_Value)
														ELSE
															CFVSI.Fld_Value
													END)) vcAttribute
												FROM
													CFW_Fld_Values_Serialized_Items CFVSI
												INNER JOIN
													CFW_Fld_Master CFM
												ON
													CFM.Fld_id = CFVSI.Fld_ID
												WHERE
													RecId = WHI.numWareHouseItemID
													AND bitSerialized = TEMP.bitSerialized
												) TEMP
												FOR XML PATH(''''))
											, 1
											, 1
											, ''''
											) AS vcAttributes
								FROM
									dbo.WareHouseItems WHI
								WHERE 
									WHI.numItemID = TEMP.numItemCode
									AND ISNULL(TEMP.numItemGroup,0) > 0
									AND ISNULL(TEMP.Isarchieve, 0) <> 1
									AND TEMP.charItemType NOT IN (''A'')
							) AS TEMPAttributes
							WHERE
								' + @strAttributeSearch
		END

		EXEC ('INSERT INTO #TempSearchedItem SELECT numItemCode, vcItemName, vcPathForTImage, vcCompanyName, monListPrice, txtItemDesc, vcModelID, numBarCodeId, vcBarCode, vcSKU, vcWHSKU, vcPartNo, numWarehouseItemID, vcAttributes FROM (' + @strSQL + ')TEMPFinal')

		IF @searchCriteria = '1' --Items
		BEGIN
			SET @strSQL = 'SELECT * ' + @vcCustomDisplayField + ' FROM #TempSearchedItem ORDER BY numItemCode OFFSET ' + CAST(@numSkip AS VARCHAR(100)) + ' ROWS FETCH NEXT 10 ROWS ONLY'
			EXEC (@strSQL)
			SELECT COUNT(*) AS TotalRows FROM #TempSearchedItem
		END
		ELSE IF @searchCriteria = '3' -- Opp/Orders
		BEGIN
			DECLARE @TEMPItemOrder TABLE
			(
				numOppId NUMERIC(18,0),
				CreatedDate DATETIME,
				vcPOppName VARCHAR(200),
				vcOppType VARCHAR(50),
				vcCompanyName  VARCHAR(200),
				tintType TINYINT
			)

			INSERT INTO 
				@TEMPItemOrder
			SELECT
				numOppId,
				bintCreatedDate,
				vcPOppName,
				vcOppType,
				vcCompanyName,
				tintType
			FROM
				(
					SELECT
						OpportunityMaster.numOppId,
						OpportunityMaster.vcPOppName,
						CASE 
							WHEN tintOppType = 1 THEN 'Sales ' + (CASE WHEN tintOppStatus = 1 THEN 'Order' ELSE 'Opportunity' END)
							WHEN tintOppType = 2 THEN 'Purchase ' + (CASE WHEN tintOppStatus = 1 THEN 'Order' ELSE 'Opportunity' END)
						END vcOppType,
						CompanyInfo.vcCompanyName,
						OpportunityMaster.bintCreatedDate,
						1 AS tintType
					FROM 
						OpportunityMaster
					INNER JOIN
						(
							SELECT
								numOppID
							FROM 
								OpportunityItems OI
							INNER JOIN
								#TempSearchedItem
							ON
								OI.numItemCode = #TempSearchedItem.numItemCode
								AND (OI.numWarehouseItmsID = #TempSearchedItem.numWarehouseItemID OR ISNULL(#TempSearchedItem.numWarehouseItemID,0) = 0)
							GROUP BY
								numOppID
						) TempOppItems
					ON
						OpportunityMaster.numOppId = TempOppItems.numOppId
					INNER JOIN
						DivisionMaster 
					ON
						OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
					INNER JOIN
						CompanyInfo
					ON
						DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
					WHERE
						OpportunityMaster.numDomainId =@numDomainID
						AND tintOppType <> 0
						AND 1 = (
								CASE @orderType
								 WHEN 0 THEN 1
								 WHEN 1 THEN (CASE WHEN (tintOppType = 1 AND tintOppStatus=1) THEN 1 ELSE 0 END)
								 WHEN 2 THEN (CASE WHEN (tintOppType = 2 AND tintOppStatus=1) THEN 1 ELSE 0 END)
								 WHEN 3 THEN (CASE WHEN (tintOppType = 1 AND ISNULL(tintOppStatus,0)=0) THEN 1 ELSE 0 END)
								 WHEN 4 THEN (CASE WHEN (tintOppType = 2 AND ISNULL(tintOppStatus,0)=0) THEN 1 ELSE 0 END)
								 WHEN 5 THEN 0
								 WHEN 6 THEN 0
								END
							)
					UNION
					SELECT
						ReturnHeader.numReturnHeaderID AS numOppID,
						ReturnHeader.vcRMA AS vcPOppName,
						CASE 
							WHEN tintReturnType = 1 THEN 'Sales Return'
							WHEN tintReturnType = 2 THEN 'Purchase Return'
						END vcOppType,
						CompanyInfo.vcCompanyName,
						ReturnHeader.dtCreatedDate AS bintCreatedDate,
						2 AS tintType
					FROM
						ReturnHeader
					INNER JOIN
						(
							SELECT
								numReturnHeaderID
							FROM 
								ReturnItems RI
							INNER JOIN
								#TempSearchedItem
							ON
								RI.numItemCode = #TempSearchedItem.numItemCode
								AND (RI.numWareHouseItemID = #TempSearchedItem.numWarehouseItemID OR ISNULL(#TempSearchedItem.numWarehouseItemID,0) = 0)
							GROUP BY
								numReturnHeaderID
						) TempReturnItems
					ON
						ReturnHeader.numReturnHeaderID = TempReturnItems.numReturnHeaderID
					INNER JOIN
						DivisionMaster 
					ON
						ReturnHeader.numDivisionId = DivisionMaster.numDivisionID
					INNER JOIN
						CompanyInfo
					ON
						DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
					WHERE
						ReturnHeader.numDomainId = @numDomainID 
						AND 1 = (
								CASE @orderType
								 WHEN 0 THEN (CASE WHEN tintReturnType = 1 OR tintReturnType = 2 THEN 1 ELSE 0 END)
								 WHEN 1 THEN 0
								 WHEN 2 THEN 0
								 WHEN 3 THEN 0
								 WHEN 4 THEN 0
								 WHEN 5 THEN (CASE WHEN tintReturnType = 1 THEN 1 ELSE 0 END)
								 WHEN 6 THEN (CASE WHEN tintReturnType = 2 THEN 1 ELSE 0 END)
								END
							)
				) TEMPFinal

			SELECT 
				ISNULL(numOppId,0) numOppId,
				ISNULL(dbo.FormatedDateFromDate(CreatedDate,@numDomainID),'') bintCreatedDate,
				ISNULL(vcPOppName,'') vcPOppName,
				ISNULL(vcOppType,'') vcOppType,
				ISNULL(vcCompanyName,'') vcCompanyName,
				ISNULL(tintType,0) tintType
			FROM
				@TEMPItemOrder
			ORDER BY
				CreatedDate DESC
			OFFSET @numSkip ROWS FETCH NEXT 10 ROWS ONLY	
				
			SELECT COUNT(*) AS TotalRows FROM @TEMPItemOrder	
		END
		ELSE IF @searchCriteria = '4' -- BizDocs
		BEGIN
			DECLARE @TEMPItemBizDoc TABLE
			(
				numOppBizDocsId NUMERIC(18,0),
				CreatedDate DATETIME,
				vcBizDocID VARCHAR(200),
				vcBizDocType VARCHAR(100),
				vcCompanyName  VARCHAR(200)
			)

			INSERT INTO
				@TEMPItemBizDoc
			SELECT 
				numOppBizDocsId,
				OpportunityBizDocs.dtCreatedDate,
				vcBizDocID,
				ListDetails.vcData AS vcBizDocType,
				CompanyInfo.vcCompanyName
			FROM 
				OpportunityBizDocs
			INNER JOIN
				(
					SELECT
						numOppBizDocID
					FROM 
						OpportunityBizDocItems OBI
					INNER JOIN
						#TempSearchedItem
					ON
						OBI.numItemCode = #TempSearchedItem.numItemCode
						AND (OBI.numWarehouseItmsID = #TempSearchedItem.numWarehouseItemID OR ISNULL(#TempSearchedItem.numWarehouseItemID,0) = 0)
					GROUP BY
						numOppBizDocID
				) TempOppBizDocItems
			ON
				OpportunityBizDocs.numOppBizDocsId = TempOppBizDocItems.numOppBizDocID
			INNER JOIN
				OpportunityMaster
			ON
				OpportunityBizDocs.numOppId = OpportunityMaster.numOppId
			INNER JOIN
				DivisionMaster
			ON
				OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
			INNER JOIN
				CompanyInfo
			ON
				DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
			INNER JOIN
				ListDetails
			ON
				ListDetails.numListID = 27
				AND OpportunityBizDocs.numBizDocId = ListDetails.numListItemID
			WHERE
				OpportunityMaster.numDomainId = @numDomainID
				AND (OpportunityBizDocs.numBizDocID = @bizDocType OR ISNULL(@bizDocType,0) = 0)

			SELECT
				ISNULL(numOppBizDocsId,0) numOppBizDocsId,
				ISNULL(dbo.FormatedDateFromDate(CreatedDate,@numDomainID),'') AS dtCreatedDate,
				ISNULL(vcBizDocID,'') vcBizDocID,
				ISNULL(vcBizDocType,'') vcBizDocType,
				ISNULL(vcCompanyName,'') vcCompanyName
			FROM
				@TEMPItemBizDoc
			ORDER BY
				CreatedDate DESC
			OFFSET @numSkip ROWS FETCH NEXT 10 ROWS ONLY
				
			SELECT COUNT(*) AS TotalRows FROM @TEMPItemBizDoc
		END

		
		DROP TABLE #TempItemSearchFields
		DROP TABLE #TempItemCustomDisplayFields
		DROP TABLE #TempSearchedItem
	END
	ELSE IF @searchType = '3' -- Opps/Orders
	BEGIN
		DECLARE @TEMPORDER TABLE
		(
			numOppId NUMERIC(18,0),
			CreatedDate DATETIME,
			vcPOppName VARCHAR(200),
			vcOppType VARCHAR(50),
			vcCompanyName  VARCHAR(200),
			tintType TINYINT
		)

		INSERT INTO 
			@TEMPORDER
		SELECT
			numOppId,
			bintCreatedDate,
			vcPOppName,
			vcOppType,
			vcCompanyName,
			tintType
		FROM
			(
				SELECT
					OpportunityMaster.numOppId,
					OpportunityMaster.vcPOppName,
					CASE 
						WHEN tintOppType = 1 THEN 'Sales ' + (CASE WHEN tintOppStatus = 1 THEN 'Order' ELSE 'Opportunity' END)
						WHEN tintOppType = 2 THEN 'Purchase ' + (CASE WHEN tintOppStatus = 1 THEN 'Order' ELSE 'Opportunity' END)
					END vcOppType,
					CompanyInfo.vcCompanyName,
					OpportunityMaster.bintCreatedDate,
					1 AS tintType
				FROM 
					OpportunityMaster
				INNER JOIN
					DivisionMaster 
				ON
					OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
				INNER JOIN
					CompanyInfo
				ON
					DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
				WHERE
					OpportunityMaster.numDomainId =@numDomainID
					AND tintOppType <> 0
					AND (vcPOppName LIKE N'%' + @searchText + '%' OR numOppId LIKE N'%' + @searchText + '%')
					AND 1 = (
							CASE @orderType
							 WHEN 0 THEN 1
							 WHEN 1 THEN (CASE WHEN (tintOppType = 1 AND tintOppStatus=1) THEN 1 ELSE 0 END)
							 WHEN 2 THEN (CASE WHEN (tintOppType = 2 AND tintOppStatus=1) THEN 1 ELSE 0 END)
							 WHEN 3 THEN (CASE WHEN (tintOppType = 1 AND ISNULL(tintOppStatus,0)=0) THEN 1 ELSE 0 END)
							 WHEN 4 THEN (CASE WHEN (tintOppType = 2 AND ISNULL(tintOppStatus,0)=0) THEN 1 ELSE 0 END)
							 WHEN 5 THEN 0
							 WHEN 6 THEN 0
							END
						)
				UNION
				SELECT
					ReturnHeader.numReturnHeaderID AS numOppID,
					ReturnHeader.vcRMA AS vcPOppName,
					CASE 
						WHEN tintReturnType = 1 THEN 'Sales Return'
						WHEN tintReturnType = 2 THEN 'Purchase Return'
					END vcOppType,
					CompanyInfo.vcCompanyName,
					ReturnHeader.dtCreatedDate AS bintCreatedDate,
					2 AS tintType
				FROM
					ReturnHeader
				INNER JOIN
					DivisionMaster 
				ON
					ReturnHeader.numDivisionId = DivisionMaster.numDivisionID
				INNER JOIN
					CompanyInfo
				ON
					DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
				WHERE
					ReturnHeader.numDomainId = @numDomainID 
					AND (vcRMA LIKE N'%' + @searchText + '%' OR ReturnHeader.numReturnHeaderID LIKE N'%' + @searchText + '%')
					AND 1 = (
							CASE @orderType
							 WHEN 0 THEN (CASE WHEN tintReturnType = 1 OR tintReturnType = 2 THEN 1 ELSE 0 END)
							 WHEN 1 THEN 0
							 WHEN 2 THEN 0
							 WHEN 3 THEN 0
							 WHEN 4 THEN 0
							 WHEN 5 THEN (CASE WHEN tintReturnType = 1 THEN 1 ELSE 0 END)
							 WHEN 6 THEN (CASE WHEN tintReturnType = 2 THEN 1 ELSE 0 END)
							END
						)
				) TEMPFinal

		SELECT 
			ISNULL(numOppId,0) numOppId,
			ISNULL(dbo.FormatedDateFromDate(CreatedDate,@numDomainID),'') bintCreatedDate,
			ISNULL(vcPOppName,'') vcPOppName,
			ISNULL(vcOppType,'') vcOppType,
			ISNULL(vcCompanyName,'') vcCompanyName,
			ISNULL(tintType,0) tintType
		FROM
			@TEMPORDER
		ORDER BY
			CreatedDate DESC
		OFFSET @numSkip ROWS FETCH NEXT 10 ROWS ONLY	
				
		SELECT COUNT(*) AS TotalRows FROM @TEMPORDER		
	END
	ELSE IF @searchType = '4' -- BizDocs
	BEGIN
		DECLARE @TEMPBizDoc TABLE
		(
			numOppBizDocsId NUMERIC(18,0),
			CreatedDate DATETIME,
			vcBizDocID VARCHAR(200),
			vcBizDocType VARCHAR(100),
			vcCompanyName  VARCHAR(200)
		)

		INSERT INTO
			@TEMPBizDoc
		SELECT 
			numOppBizDocsId,
			OpportunityBizDocs.dtCreatedDate,
			vcBizDocID,
			ListDetails.vcData AS vcBizDocType,
			CompanyInfo.vcCompanyName
		FROM 
			OpportunityBizDocs
		INNER JOIN
			OpportunityMaster
		ON
			OpportunityBizDocs.numOppId = OpportunityMaster.numOppId
		INNER JOIN
			DivisionMaster
		ON
			OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
		INNER JOIN
			CompanyInfo
		ON
			DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
		INNER JOIN
			ListDetails
		ON
			ListDetails.numListID = 27
			AND OpportunityBizDocs.numBizDocId = ListDetails.numListItemID
		WHERE
			OpportunityMaster.numDomainId = @numDomainID
			AND vcBizDocID LIKE N'%' + @searchText + '%'
			AND (OpportunityBizDocs.numBizDocID = @bizDocType OR ISNULL(@bizDocType,0) = 0)

		SELECT
			ISNULL(numOppBizDocsId,0) numOppBizDocsId,
			ISNULL(dbo.FormatedDateFromDate(CreatedDate,@numDomainID),'') AS dtCreatedDate,
			ISNULL(vcBizDocID,'') vcBizDocID,
			ISNULL(vcBizDocType,'') vcBizDocType,
			ISNULL(vcCompanyName,'') vcCompanyName
		FROM
			@TEMPBizDoc
		ORDER BY
			CreatedDate DESC
		OFFSET @numSkip ROWS FETCH NEXT 10 ROWS ONLY
				
		SELECT COUNT(*) AS TotalRows FROM @TEMPBizDoc
	END
	ELSE IF @searchType = '5' -- Contact
	BEGIN
		DECLARE @TableContact TABLE
		(
			numContactId NUMERIC(18,0),
			vcFirstName VARCHAR(100),
			vcLastname VARCHAR(100),
			vcFullName VARCHAR(200),
			vcEmail VARCHAR(100),
			numPhone VARCHAR(100),
			numPhoneExtension VARCHAR(100),
			vcCompanyName VARCHAR(200),
			bitPrimaryContact VARCHAR(5)
		)

		INSERT INTO
			@TableContact
		SELECT 
			ADC.numContactId,
			ISNULL(vcFirstName,'') AS vcFirstName,
			ISNULL(vcLastName,'') AS vcLastName,
			ISNULL(vcFirstName,'') + ' ' + ISNULL(vcLastName,'') AS vcFullName,
			ISNULL(vcEmail,'') AS vcEmail,
			ISNULL(ADC.numPhone,'') AS numPhone,
			ISNULL(ADC.numPhoneExtension,'') AS numPhoneExtension,
			ISNULL(CI.vcCompanyName,'') AS vcCompanyName,
			(CASE WHEN ISNULL(bitPrimaryContact,0) = 1 THEN 'Yes' ELSE 'No' END) AS bitPrimaryContact
		FROM 
			AdditionalContactsInformation ADC
		INNER JOIN
			DivisionMaster DM
		ON
			ADC.numDivisionId = DM.numDivisionID
		INNER JOIN
			CompanyInfo CI
		ON
			DM.numCompanyID = CI.numCompanyId
		WHERE 
			ADC.numDomainID = @numDomainID AND
			(vcFirstName LIKE '%' + @searchText + '%'
			OR vcLastName LIKE '%' + @searchText + '%'
			OR vcEmail LIKE '%' + @searchText + '%'
			OR numPhone LIKE '%' + @searchText + '%')

		SELECT 
			* 
		FROM 
			@TableContact	
		ORDER BY 
			vcCompanyName ASC, bitPrimaryContact DESC
		OFFSET 
			@numSkip 
		ROWS FETCH NEXT 
			10 
		ROWS ONLY

		SELECT COUNT(*) FROM @TableContact
	END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_UpdateBillingTerms')
	DROP PROCEDURE USP_UpdateBillingTerms
GO

CREATE PROCEDURE [dbo].[USP_UpdateBillingTerms]
(
	 @numDomainID  numeric=0,                                          
     @numDivisionID  numeric=0, 
	 @numCompanyID numeric=0,
	 @numCompanyCredit numeric=0,                                   
	 @vcDivisionName  varchar (100)='',
	 @numUserCntID  numeric=0,                                                                                                                                     
	 @tintCRMType  tinyint=0,                                          
	 @tintBillingTerms as tinyint,                                          
	 @numBillingDays as numeric(9),                                         
	 @tintInterestType as tinyint,                                          
	 @fltInterest as float,                          
	 @bitNoTax as BIT,
	 @numCurrencyID AS numeric(9)=0,
	 @numDefaultPaymentMethod AS numeric(9)=0,         
	 @numDefaultCreditCard AS numeric(9)=0,         
	 @bitOnCreditHold AS bit=0,
	 @vcShipperAccountNo VARCHAR(100) = '',
	 @intShippingCompany INT = 0,
	 @bitEmailToCase BIT=0,
	 @numDefaultExpenseAccountID NUMERIC(18,0) = 0,
	 @numDefaultShippingServiceID NUMERIC(18,0),
	 @numAccountClass NUMERIC(18,0) = 0,
	 @tintPriceLevel INT = 0
    )
AS 
	BEGIN

		UPDATE DivisionMaster 
		SET numCompanyID = @numCompanyID,
			vcDivisionName = @vcDivisionName,                                      
			numModifiedBy = @numUserCntID,                                            
			bintModifiedDate = GETUTCDATE(),                                         
			tintCRMType = @tintCRMType,                                          
			tintBillingTerms = @tintBillingTerms,                                          
			numBillingDays = @numBillingDays,                                         
			tintInterestType = @tintInterestType,                                           
			fltInterest =@fltInterest,                
			bitNoTax=@bitNoTax,
			numCurrencyID=@numCurrencyID,
			numDefaultPaymentMethod=@numDefaultPaymentMethod,                                                  
			numDefaultCreditCard=@numDefaultCreditCard,                                                 
			bitOnCreditHold=@bitOnCreditHold,
			vcShippersAccountNo = @vcShipperAccountNo,
			intShippingCompany = @intShippingCompany,
			bitEmailToCase = @bitEmailToCase,
			numDefaultExpenseAccountID = @numDefaultExpenseAccountID,
			numDefaultShippingServiceID = @numDefaultShippingServiceID,
			numAccountClassID = ISNULL(@numAccountClass,0),
			tintPriceLevel = ISNULL(@tintPriceLevel,0)
		WHERE numDivisionID = @numDivisionID AND numDomainId= @numDomainID     

		UPDATE CompanyInfo
		SET numCompanyCredit = @numCompanyCredit,               
			numModifiedBy = @numUserCntID,                
			bintModifiedDate = GETUTCDATE()               
		WHERE                 
			numCompanyId=@numCompanyId and numDomainID=@numDomainID    

	END

/****** Object:  StoredProcedure [dbo].[USP_UpdateCartItem]    Script Date: 11/08/2011 17:49:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_UpdateCartItem' ) 
                    DROP PROCEDURE USP_UpdateCartItem
                    Go
CREATE  PROCEDURE [dbo].[USP_UpdateCartItem]
(
		 @numUserCntId numeric(9,0),
	 @numDomainId numeric(9,0),
	 @vcCookieId varchar(100),
		-- @bitUserType BIT, --if 0 then anonomyous user 1 for logi user
		 @strXML text
)
AS
if convert(varchar(10),@strXML) <>''
BEGIN
		declare @i int
		EXEC sp_xml_preparedocument @i OUTPUT, @strXML 
		IF @numUserCntId <> 0
		BEGIN
		update CartItems
		set CartItems.monPrice = ox.monPrice,CartItems.numUnitHour = ox.numUnitHour,CartItems.monTotAmtBefDiscount = ox.monTotAmtBefDiscount,
		CartItems.monTotAmount = ox.monTotAmount,CartItems.bitDiscountType =ISNULL(ox.bitDiscountType,0),
		CartItems.vcShippingMethod =ox.vcShippingMethod,CartItems.numServiceTypeID =ox.numServiceTypeID,
		CartItems.decShippingCharge =ox.decShippingCharge,CartItems.tintServiceType =ox.tintServiceType,
		CartItems.numShippingCompany =ox.numShippingCompany,CartItems.dtDeliveryDate =ox.dtDeliveryDate,
		CartItems.fltDiscount =ox.fltDiscount,CartItems.vcCookieId = ox.vcCookieId


		from OpenXml(@i, '/NewDataSet/Table1',2)
		with (numDomainId NUMERIC(9,0),numUserCntId NUMERIC(9,0),vcCookieId VARCHAR(200),numItemCode NUMERIC(9,0), monPrice money, numUnitHour FLOAT,
		monTotAmtBefDiscount MONEY,monTotAmount MONEY ,bitDiscountType BIT,vcShippingMethod VARCHAR(200),numServiceTypeID NUMERIC(9,0),decShippingCharge money,
		tintServiceType TINYINT,numShippingCompany NUMERIC(9,0),dtDeliveryDate DATETIME,fltDiscount DECIMAL(18,2) ) ox
		where CartItems.numDomainId = ox.numDomainId AND CartItems.numUserCntId = ox.numUserCntId  AND 
		CartItems.numItemCode = ox.numItemCode
		END 
		ELSE
		BEGIN
		update CartItems
		set CartItems.monPrice = ox.monPrice,CartItems.numUnitHour = ox.numUnitHour,CartItems.monTotAmtBefDiscount = ox.monTotAmtBefDiscount,
		CartItems.monTotAmount = ox.monTotAmount,CartItems.bitDiscountType =ISNULL(ox.bitDiscountType,0),
		CartItems.vcShippingMethod =ox.vcShippingMethod,CartItems.numServiceTypeID =ox.numServiceTypeID,
		CartItems.decShippingCharge =ox.decShippingCharge,CartItems.tintServiceType =ox.tintServiceType,
		CartItems.numShippingCompany =ox.numShippingCompany,CartItems.dtDeliveryDate =ox.dtDeliveryDate,
		CartItems.fltDiscount =ox.fltDiscount


		from OpenXml(@i, '/NewDataSet/Table1',2)
		with (numDomainId NUMERIC(9,0),numUserCntId NUMERIC(9,0),vcCookieId VARCHAR(200),numItemCode NUMERIC(9,0), monPrice money, numUnitHour FLOAT,
		monTotAmtBefDiscount MONEY,monTotAmount MONEY ,bitDiscountType BIT,vcShippingMethod VARCHAR(200),numServiceTypeID NUMERIC(9,0),decShippingCharge MONEY,
		tintServiceType TINYINT,numShippingCompany NUMERIC(9,0),dtDeliveryDate DATETIME,fltDiscount DECIMAL(18,2) ) ox
		where CartItems.numDomainId = ox.numDomainId AND CartItems.numUserCntId = 0  AND 
		CartItems.numItemCode = ox.numItemCode
		 And CartItems.vcCookieId = ox.vcCookieId
		END

		exec sp_xml_removedocument @i
	
END

---Created by anoop Jayaraj

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_UpdateInventory')
DROP PROCEDURE USP_UpdateInventory
GO
CREATE PROCEDURE  USP_UpdateInventory
@numDomainID as numeric(9),
@numItemCode as NUMERIC(9),
@numOnHand as FLOAT,
@numWarehouseID as numeric(9)
as


Update W set W.numOnHand=@numOnHand ,dtModified = GETDATE() 
from WareHouseItems W join
Item I
on W.numItemID=I.numItemCode
where I.numDomainID=@numDomainID AND W.numDomainID =@numDomainID and numWareHouseID=@numWarehouseID 
and I.numItemCode=@numItemCode

GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_UpdateInventoryAdjustments' ) 
    DROP PROCEDURE USP_UpdateInventoryAdjustments
GO
CREATE PROCEDURE USP_UpdateInventoryAdjustments
    @numDomainId NUMERIC,
    @numUserCntID NUMERIC,
    @strItems VARCHAR(8000),
    @dtAdjustmentDate AS DATETIME
AS 
BEGIN
    
DECLARE @hDoc INT    
EXEC sp_xml_preparedocument @hDoc OUTPUT, @strItems    
  SELECT * INTO #temp FROM OPENXML (@hDoc,'/NewDataSet/Item',2)    
	WITH ( numWareHouseItemID NUMERIC(9),numItemCode NUMERIC(9),
		intAdjust FLOAT) 
		
  SELECT *,ROW_NUMBER() OVER(ORDER BY numItemCode) AS RowNo INTO #tempSerialLotNO FROM OPENXML (@hDoc,'/NewDataSet/SerialLotNo',2)    
	WITH (numItemCode NUMERIC(9),numWareHouseID NUMERIC(9),numWareHouseItemID NUMERIC(9),vcSerialNo VARCHAR(3000),byteMode TINYINT) 
		
EXEC sp_xml_removedocument @hDoc  

    
		    DECLARE @numWareHouseItemID AS NUMERIC(9)
			DECLARE @numItemCode NUMERIC(18)
			
			
		    SELECT @numWareHouseItemID = MIN(numWareHouseItemID) FROM #temp
			Declare @bitLotNo as bit;SET @bitLotNo=0  
			Declare @bitSerialized as bit;SET @bitSerialized=0  

				while @numWareHouseItemID>0    
				 begin
				  
				  select @bitLotNo=isnull(Item.bitLotNo,0),@bitSerialized=isnull(Item.bitSerialized,0)
					from item INNER JOIN #temp ON item.numItemCode=#temp.numItemCode where #temp.numWareHouseItemID=@numWareHouseItemID and numDomainID=@numDomainID

				  IF @bitLotNo=0 AND @bitSerialized=0
				  BEGIN		
						  UPDATE  WI SET numOnHand =numOnHand + #temp.intAdjust,dtModified=GETDATE()  FROM dbo.WareHouseItems WI INNER JOIN #temp ON WI.numWareHouseItemID = #temp.numWareHouseItemID
						  WHERE WI.numWareHouseItemID = @numWareHouseItemID AND WI.numDomainID=@numDomainId
						  
								 SELECT @numItemCode=numItemID FROM dbo.WareHouseItems WHERE numWareHouseItemID = @numWareHouseItemID AND numDomainID=@numDomainId
								
								DECLARE @numDomain AS NUMERIC(18,0)
								SET @numDomain = @numDomainID
								EXEC dbo.USP_ManageWareHouseItems_Tracking
								@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
								@numReferenceID = @numItemCode, --  numeric(9, 0)
								@tintRefType = 1, --  tinyint
								@vcDescription = 'Inventory Adjustment', --  varchar(100)
								@numModifiedBy = @numUserCntID,
								@dtRecordDate = @dtAdjustmentDate,
								@numDomainId = @numDomain
				  END	  
				    
				 SELECT TOP 1 @numWareHouseItemID = numWareHouseItemID FROM #temp WHERE numWareHouseItemID >@numWareHouseItemID 
				 order by numWareHouseItemID  
				    
				 if @@rowcount=0 set @numWareHouseItemID =0    
				 end    
    

    DROP TABLE #temp
    
    
    -------------Serial/Lot #s----------------
	DECLARE @minRowNo NUMERIC(18),@maxRowNo NUMERIC(18)
	SELECT @minRowNo = MIN(RowNo),@maxRowNo = Max(RowNo) FROM #tempSerialLotNO
		    
	DECLARE @numWareHouseID NUMERIC(9),@vcSerialNo VARCHAR(3000),@numWareHouseItmsDTLID AS NUMERIC(9),
			@vcComments VARCHAR(1000),@OldQty FLOAT,@numQty FLOAT,@byteMode TINYINT 		    
	DECLARE @posComma int, @strKeyVal varchar(20)
		    
	WHILE @minRowNo <= @maxRowNo
	BEGIN
		SELECT @numItemCode=numItemCode,@numWareHouseID=numWareHouseID,@numWareHouseItemID=numWareHouseItemID,
			@vcSerialNo=vcSerialNo,@byteMode=byteMode FROM #tempSerialLotNO WHERE RowNo=@minRowNo
		
		SET @posComma=0
		
		SET @vcSerialNo=RTRIM(@vcSerialNo)
		IF RIGHT(@vcSerialNo, 1)!=',' SET @vcSerialNo=@vcSerialNo+','

		SET @posComma=PatIndex('%,%', @vcSerialNo)
		WHILE @posComma>1
			BEGIN
				SET @strKeyVal=ltrim(rtrim(substring(@vcSerialNo, 1, @posComma-1)))
	
				DECLARE @posBStart INT,@posBEnd int, @strQty varchar(20),@strName varchar(20)

				SET @posBStart=PatIndex('%(%', @strKeyVal)
				SET @posBEnd=PatIndex('%)%', @strKeyVal)
				IF( @posBStart>1 AND @posBEnd>1)
				BEGIN
					SET @strName=ltrim(rtrim(substring(@strKeyVal, 1, @posBStart-1)))
					SET	@strQty=ltrim(rtrim(substring(@strKeyVal, @posBStart+1,len(@strKeyVal)-@posBStart-1)))
				END		
				ELSE
				BEGIN
					SET @strName=@strKeyVal
					SET	@strQty=1
				END
	  
			SET @numWareHouseItmsDTLID=0
			SET @OldQty=0
			SET @vcComments=''
			
			select top 1 @numWareHouseItmsDTLID=ISNULL(numWareHouseItmsDTLID,0),@OldQty=ISNULL(numQty,0),@vcComments=ISNULL(vcComments,'') 
					from WareHouseItmsDTL where numWareHouseItemID=@numWareHouseItemID and
					vcSerialNo=@strName AND ISNULL(numQty,0) > 0
			
			IF @byteMode=0 --Add
				SET @numQty=@OldQty + @strQty
			ELSE --Deduct
				SET @numQty=@OldQty - @strQty
			
			EXEC dbo.USP_AddUpdateWareHouseForItems
					@numItemCode = @numItemCode, --  numeric(9, 0)
					@numWareHouseID = @numWareHouseID, --  numeric(9, 0)
					@numWareHouseItemID =@numWareHouseItemID,
					@numDomainID = @numDomainId, --  numeric(9, 0)
					@vcSerialNo = @strName, --  varchar(100)
					@vcComments = @vcComments, -- varchar(1000)
					@numQty = @numQty, --  numeric(18, 0)
					@byteMode = 5, --  tinyint
					@numWareHouseItmsDTLID = @numWareHouseItmsDTLID, --  numeric(18, 0)
					@numUserCntID = @numUserCntID, --  numeric(9, 0)
					@dtAdjustmentDate = @dtAdjustmentDate
	  
			SET @vcSerialNo=substring(@vcSerialNo, @posComma +1, len(@vcSerialNo)-@posComma)
			SET @posComma=PatIndex('%,%',@vcSerialNo)
		END

		SET @minRowNo=@minRowNo+1
	END
	
	DROP TABLE #tempSerialLotNO	    
END


GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_UpdateInventoryAndTracking')
DROP PROCEDURE USP_UpdateInventoryAndTracking
GO
CREATE PROCEDURE [dbo].[USP_UpdateInventoryAndTracking]
@numOnHand AS FLOAT,
@numOnAllocation AS FLOAT,
@numOnBackOrder AS FLOAT,
@numOnOrder AS FLOAT,
@numWarehouseItemID AS NUMERIC(18,0),
@numReferenceID AS NUMERIC(18,0),
@tintRefType AS TINYINT,
@numDomainID AS NUMERIC(18,0),
@numUserCntID AS NUMERIC(18,0),
@Description AS VARCHAR(1000)
AS
BEGIN
BEGIN TRY 
BEGIN TRAN
	--UPDATE WAREHOUSE INVENTORY
	UPDATE  
		WareHouseItems
	SET     
		numOnHand = @numOnHand,
		numAllocation = @numOnAllocation,
		numBackOrder = @numOnBackOrder,
		numOnOrder = @numOnOrder,
		dtModified = GETDATE() 
	WHERE   
		numWareHouseItemID = @numWarehouseItemID 
			
	--UPDATE WAREHOUSE TRACKING
	EXEC dbo.USP_ManageWareHouseItems_Tracking
		@numWareHouseItemID = @numWarehouseItemID,
		@numReferenceID = @numReferenceID,
		@tintRefType = @tintRefType,
		@vcDescription = @Description, 
		@numModifiedBy = @numUserCntID,
		@numDomainID = @numDomainID           
COMMIT TRAN 
END TRY 
BEGIN CATCH		
    IF ( @@TRANCOUNT > 0 ) 
    BEGIN
        ROLLBACK TRAN
    END

	DECLARE @ErrorMessage NVARCHAR(4000);
    DECLARE @ErrorSeverity INT;
    DECLARE @ErrorState INT;

    SELECT @ErrorMessage = ERROR_MESSAGE(), @ErrorSeverity = ERROR_SEVERITY(), @ErrorState = ERROR_STATE();

    RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);
END CATCH	
END
/****** Object:  StoredProcedure [dbo].[USP_UpdateOppItems]    Script Date: 07/26/2008 16:21:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_updateoppitems')
DROP PROCEDURE usp_updateoppitems
GO
CREATE PROCEDURE [dbo].[USP_UpdateOppItems]
@numOppID as numeric(9)=0,
@numItemCode as numeric(9)=0,
@numUnitHour as FLOAT=0,
@monPrice as money,
@vcItemName VARCHAR(300),
@vcModelId VARCHAR(200),
@vcPathForTImage VARCHAR(300),
@vcItemDesc as varchar(1000),
@numWarehouseItmsID as numeric(9),
@ItemType as Varchar(50),
@DropShip as bit=0,
@numUOM as numeric(9)=0,
@numClassID as numeric(9)=0,
@numProjectID as numeric(9)=0

as                                                   

	
   if @numWarehouseItmsID=0 set @numWarehouseItmsID=null 
	
	DECLARE @vcManufacturer VARCHAR(250)
	SELECT @vcManufacturer = vcManufacturer FROM item WHERE [numItemCode]=@numItemCode
    insert into OpportunityItems                                                    
	(numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,[vcItemName],[vcModelID],[vcPathForTImage],[vcManufacturer],numUOMId,numClassID,numProjectID)
	values
	(@numOppID,@numItemCode,@numUnitHour,@monPrice,@numUnitHour*@monPrice,@vcItemDesc,@numWarehouseItmsID,@ItemType,@DropShip,0,0,@numUnitHour*@monPrice,@vcItemName,@vcModelId,@vcPathForTImage,@vcManufacturer,@numUOM,@numClassID,@numProjectID)
	
	select @@identity
	                                 
	update OpportunityMaster set  monPamount=(select sum(monTotAmount)from OpportunityItems where numOppId=@numOppID)                                                     
	where numOppId=@numOppID
GO
--Created by Anoop Jayaraj

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_UpdatePOItemsForFulfillment')
DROP PROCEDURE USP_UpdatePOItemsForFulfillment
GO
CREATE PROCEDURE  USP_UpdatePOItemsForFulfillment
    @numQtyReceived FLOAT,
    @numOppItemID NUMERIC(9),
    @vcError VARCHAR(200) = '' OUTPUT,
    @numUserCntID AS NUMERIC(9),
    @dtItemReceivedDate AS DATETIME 
AS 

--Transaction is invoked from ADO.net (Purchase Fulfillment) Do not add transaction code here.
-- BEGIN TRY
-- BEGIN TRANSACTION
			DECLARE @numDomain AS NUMERIC(18,0)
			DECLARE @onAllocation AS FLOAT          
			DECLARE @numWarehouseItemID AS NUMERIC       
			DECLARE @numOldQtyReceived AS FLOAT       
			DECLARE @numNewQtyReceived AS FLOAT
			DECLARE @onHand AS FLOAT         
			DECLARE @onOrder AS FLOAT            
			DECLARE @onBackOrder AS FLOAT              
			DECLARE @bitStockTransfer BIT
			DECLARE @numToWarehouseItemID NUMERIC
			DECLARE @numOppId NUMERIC
			DECLARE @numUnits FLOAT
			DECLARE @monPrice AS MONEY 
			DECLARE @numItemCode NUMERIC 
		
			SELECT  @numWarehouseItemID = ISNULL([numWarehouseItmsID], 0),
					@numOldQtyReceived = ISNULL([numUnitHourReceived], 0),
					@bitStockTransfer = ISNULL(OM.bitStockTransfer,0),
					@numToWarehouseItemID= ISNULL(numToWarehouseItemID,0),
					@numOppId=OM.numOppId,
					@numUnits=OI.numUnitHour,
					@monPrice=isnull(monPrice,0) * (CASE WHEN ISNULL(OM.fltExchangeRate,0)=0 THEN 1 ELSE OM.fltExchangeRate END),
					@numItemCode=OI.numItemCode,
					@numDomain = OM.[numDomainId]
			FROM    [OpportunityItems] OI INNER JOIN dbo.OpportunityMaster OM
					ON OI.numOppId = OM.numOppId
			WHERE   [numoppitemtCode] = @numOppItemID
    
    
    
			IF @bitStockTransfer = 1 --added by chintan
			BEGIN
			--ship item from FROM warehouse
				declare @p3 varchar(500)
				set @p3=''
				exec USP_UpdateQtyShipped @numQtyReceived,@numOppItemID,@numUserCntID,@vcError=@p3 OUTPUT
				IF LEN(@p3)>0 
				BEGIN
					RAISERROR ( @p3,16, 1 )
					RETURN ;

				END
		
			-- Receive item from To Warehouse
			SET @numWarehouseItemID=@numToWarehouseItemID
    
		   END  
    
			DECLARE @numTotalQuantityReceived FLOAT
			SET @numTotalQuantityReceived = @numQtyReceived + @numOldQtyReceived ;
			SET @numNewQtyReceived = @numQtyReceived;
			DECLARE @description AS VARCHAR(100)
			SET @description='PO Qty Received (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Received:' +  CAST(@numTotalQuantityReceived AS VARCHAR(10)) + ')'
			
		--    PRINT @numNewQtyReceived
			IF @numNewQtyReceived <= 0 
				RETURN 
  
					SELECT  @onHand = ISNULL(numOnHand, 0),
					@onAllocation = ISNULL(numAllocation, 0),
					@onOrder = ISNULL(numOnOrder, 0),
					@onBackOrder = ISNULL(numBackOrder, 0)
			FROM    WareHouseItems
			WHERE   numWareHouseItemID = @numWareHouseItemID
			
			           DECLARE @TotalOnHand AS FLOAT;SET @TotalOnHand=0  
			           SELECT @TotalOnHand=ISNULL((SUM(numOnHand) + SUM(numAllocation)), 0) FROM dbo.WareHouseItems WHERE numItemID=@numItemCode 
						
			           --Updating the Average Cost
			           DECLARE @monAvgCost AS MONEY 
			           SELECT  @monAvgCost = ISNULL(monAverageCost, 0) FROM    Item WHERE   numitemcode = @numItemCode  
    
  						SET @monAvgCost = ( ( @TotalOnHand * @monAvgCost )
                                             + (@numNewQtyReceived * @monPrice))
                            / ( @TotalOnHand + @numNewQtyReceived )
                            
                        UPDATE  item
                        SET     monAverageCost = @monAvgCost
                        WHERE   numItemCode = @numItemCode
    
			
  
		--	SELECT @onHand onHand,
		--            @onAllocation onAllocation,
		--            @onBackOrder onBackOrder,
		--            @onOrder onOrder
      
			IF @onOrder >= @numNewQtyReceived 
				BEGIN    
				PRINT '1 case'        
					SET @onOrder = @onOrder - @numNewQtyReceived             
					IF @onBackOrder >= @numNewQtyReceived 
						BEGIN            
							SET @onBackOrder = @onBackOrder - @numNewQtyReceived             
							SET @onAllocation = @onAllocation + @numNewQtyReceived             
						END            
					ELSE 
						BEGIN            
							SET @onAllocation = @onAllocation + @onBackOrder            
							SET @numNewQtyReceived = @numNewQtyReceived - @onBackOrder            
							SET @onBackOrder = 0            
							SET @onHand = @onHand + @numNewQtyReceived             
						END         
				END            
			ELSE IF @onOrder < @numNewQtyReceived 
				BEGIN            
				PRINT '2 case'        
					SET @onHand = @onHand + @onOrder
					SET @onOrder = @numNewQtyReceived - @onOrder
				END   

				SELECT @onHand onHand,
					@onAllocation onAllocation,
					@onBackOrder onBackOrder,
					@onOrder onOrder
                
			UPDATE  [OpportunityItems]
			SET     numUnitHourReceived = @numTotalQuantityReceived
			WHERE   [numoppitemtCode] = @numOppItemID

			UPDATE  WareHouseItems
			SET     numOnHand = @onHand,
					numAllocation = @onAllocation,
					numBackOrder = @onBackOrder,
					numOnOrder = @onOrder,dtModified = GETDATE() 
			WHERE   numWareHouseItemID = @numWareHouseItemID
    
  
	    UPDATE dbo.OpportunityMaster SET dtItemReceivedDate=@dtItemReceivedDate WHERE numOppId=@numOppId
		
		EXEC dbo.USP_ManageWareHouseItems_Tracking
			@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
			@numReferenceID = @numOppId, --  numeric(9, 0)
			@tintRefType = 4, --  tinyint
			@vcDescription = @description, --  varchar(100)
			@numModifiedBy = @numUserCntID,
			@dtRecordDate =  @dtItemReceivedDate,
			@numDomainID = @numDomain
          
    
		--    BEGIN
		--        DECLARE @vcOppName VARCHAR(200)
		--        SELECT TOP 1
		--                @vcOppName = ISNULL([vcPOppName], '')
		--        FROM    [OpportunityMaster]
		--        WHERE   [numOppId] IN ( SELECT  [numOppId]
		--                                FROM    [OpportunityItems]
		--                                WHERE   [numoppitemtCode] = @numOppItemID )
		--        SET @vcError = 'There is not enough Qty on Allocation to save quantity Received for ' + @vcOppName
		--        RETURN
		--    END
		--     
		
--COMMIT
--END TRY
--BEGIN CATCH
--   --Whoops, there was an error
--  IF @@TRANCOUNT > 0
--     ROLLBACK

--  -- Raise an error with the details of the exception
--  DECLARE @ErrMsg nvarchar(4000), @ErrSeverity int
--  SELECT @ErrMsg = ERROR_MESSAGE(),
--         @ErrSeverity = ERROR_SEVERITY()

--  RAISERROR(@ErrMsg, @ErrSeverity, 1)
--END CATCH
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_UpdateQtyShipped')
DROP PROCEDURE USP_UpdateQtyShipped
GO
CREATE PROCEDURE  USP_UpdateQtyShipped
    @numQtyShipped FLOAT,
    @numOppItemID NUMERIC(9),
    @numUserCntID NUMERIC(9),
    @vcError VARCHAR(500) ='' OUTPUT
AS 
BEGIN
BEGIN TRY
BEGIN TRANSACTION      
--        DECLARE @onHand AS NUMERIC            
--        DECLARE @onOrder AS NUMERIC            
--        DECLARE @onBackOrder AS NUMERIC    
		DECLARE @numDomain AS NUMERIC(18,0)          
        DECLARE @onAllocation AS FLOAT          
        DECLARE @numWarehouseItemID AS NUMERIC       
        DECLARE @numOldQtyShipped AS FLOAT       
        DECLARE @numNewQtyShipped AS FLOAT       
        DECLARE @numOppId NUMERIC
		DECLARE @numUnits FLOAT
        DECLARE @Kit AS BIT                   
		DECLARE @vcOppName VARCHAR(200)

         
        SELECT  @numWarehouseItemID = ISNULL([numWarehouseItmsID], 0),@numOldQtyShipped=ISNULL(numQtyShipped,0),
        @numOppId=numOppId,@numUnits=numUnitHour,@Kit= (case when bitKitParent=1 and bitAssembly=1 then 0 when bitKitParent=1 then 1 else 0 end),
		@numDomain = I.[numDomainID]
        FROM    OpportunityItems OI  join Item I on OI.numItemCode=I.numItemCode
        WHERE   [numoppitemtCode] = @numOppItemID
        
        SET @numNewQtyShipped = @numQtyShipped-@numOldQtyShipped;
        
        DECLARE @description AS VARCHAR(100)
		SET @description='SO Qty Shipped (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@numNewQtyShipped AS VARCHAR(10)) + ')'

		IF @Kit=1
		BEGIN
			
			SELECT OKI.numOppChildItemID,OKI.numWareHouseItemId,numQtyItemsReq,numQtyItemsReq_Orig,numQtyShipped,ISNULL(numAllocation, 0) AS numAllocation,
					ROW_NUMBER() OVER(ORDER BY numOppChildItemID) AS RowNumber INTO #tempKits
			from OpportunityKitItems OKI join Item I on OKI.numChildItemID=I.numItemCode  
				JOIN WareHouseItems WI ON WI.numWareHouseItemID=OKI.numWareHouseItemID  
			where charitemtype='P' and OKI.numWareHouseItemId>0 and OKI.numWareHouseItemId is not null 
				AND numOppItemID=@numOppItemID 
				
			IF(SELECT COUNT(*) FROM #tempKits WHERE numAllocation - ((@numQtyShipped * numQtyItemsReq_Orig) - numQtyShipped) < 0)>0
			BEGIN
				SELECT TOP 1 @vcOppName=ISNULL([vcPOppName],'') FROM [OpportunityMaster] WHERE [numOppId] IN (SELECT [numOppId] FROM [OpportunityItems] WHERE [numoppitemtCode]=@numOppItemID)
				SET @vcError ='You do not have enough KIT inventory to support this shipment('+ @vcOppName +'). Modify your Qty-Shipped to an amount equal to or less than the Qty-On-Allocation and try again.' 
			END
			ELSE
			BEGIN
					DECLARE @minRowNumber NUMERIC(9),@maxRowNumber NUMERIC(9),@KitWareHouseItemID NUMERIC(9)
					DECLARE @KitonAllocation as FLOAT,@KitQtyShipped as FLOAT,@KiyQtyItemsReq_Orig as FLOAT,@KiyQtyItemsReq as FLOAT  
					DECLARE @KitNewQtyShipped AS FLOAT,@KitOppChildItemID AS NUMERIC(9)
					
					SELECT  @minRowNumber = MIN(RowNumber),@maxRowNumber = MAX(RowNumber) FROM #tempKits

					WHILE  @minRowNumber <= @maxRowNumber
					BEGIN
						SELECT @KitOppChildItemID=numOppChildItemID,@KitWareHouseItemID=numWareHouseItemID,@KiyQtyItemsReq=numQtyItemsReq,
								@KitQtyShipped=numQtyShipped,@KiyQtyItemsReq_Orig=numQtyItemsReq_Orig,
								@KitonAllocation=numAllocation FROM #tempKits WHERE RowNumber=@minRowNumber
			
						SET @KitNewQtyShipped = (@numNewQtyShipped * @KiyQtyItemsReq_Orig) - @KitQtyShipped;
			
						DECLARE @Kitdescription AS VARCHAR(100)
						SET @Kitdescription='SO KIT Qty Shipped (Qty:' + CAST(@KiyQtyItemsReq AS VARCHAR(10)) + ' Shipped:' +  CAST(@KitNewQtyShipped AS VARCHAR(10)) + ')'

						SET @KitonAllocation = @KitonAllocation - @KitNewQtyShipped
        
						IF (@KitonAllocation >= 0 )
						BEGIN
							UPDATE  WareHouseItems
							SET     numAllocation = @KitonAllocation,dtModified = GETDATE() 
							WHERE   numWareHouseItemID = @KitWareHouseItemID      	
						
							UPDATE  [OpportunityKitItems]
							SET     [numQtyShipped] = @numQtyShipped * @KiyQtyItemsReq_Orig 
							WHERE   [numOppChildItemID] = @KitOppChildItemID
			                
							EXEC dbo.USP_ManageWareHouseItems_Tracking
									@numWareHouseItemID = @KitWareHouseItemID, --  numeric(9, 0)
									@numReferenceID = @numOppId, --  numeric(9, 0)
									@tintRefType = 3, --  tinyint
									@vcDescription = @Kitdescription, --  varchar(100)
									@numModifiedBy = @numUserCntID,
									@numDomainID = @numDomain 
						END
            
						SET @minRowNumber=@minRowNumber + 1			 		
					END				
			   
					UPDATE  [OpportunityItems]
					SET     [numQtyShipped] = @numQtyShipped
					WHERE   [numoppitemtCode] = @numOppItemID
	                
					EXEC dbo.USP_ManageWareHouseItems_Tracking
							@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
							@numReferenceID = @numOppId, --  numeric(9, 0)
							@tintRefType = 3, --  tinyint
							@vcDescription = @description, --  varchar(100)
							@numModifiedBy = @numUserCntID,
							@numDomainID = @numDomain 
			 END			
		END
		ELSE
		BEGIN
		
        SELECT  
--				@onHand = ISNULL(numOnHand, 0),
                @onAllocation = ISNULL(numAllocation, 0)
--                @onOrder = ISNULL(numOnOrder, 0),
--                @onBackOrder = ISNULL(numBackOrder, 0)
        FROM    WareHouseItems
        WHERE   numWareHouseItemID = @numWareHouseItemID
        
        
        SET @onAllocation = @onAllocation - @numNewQtyShipped
        
        IF (@onAllocation >= 0 )
            BEGIN
                UPDATE  WareHouseItems
                SET     numAllocation = @onAllocation,dtModified = GETDATE() 
                WHERE   numWareHouseItemID = @numWareHouseItemID      	
			
			
                UPDATE  [OpportunityItems]
                SET     [numQtyShipped] = @numQtyShipped
                WHERE   [numoppitemtCode] = @numOppItemID
                
					EXEC dbo.USP_ManageWareHouseItems_Tracking
						@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
						@numReferenceID = @numOppId, --  numeric(9, 0)
						@tintRefType = 3, --  tinyint
						@vcDescription = @description, --  varchar(100)
						@numModifiedBy = @numUserCntID,
						@numDomainID = @numDomain 
            END
            ELSE
            BEGIN
				SELECT TOP 1 @vcOppName=ISNULL([vcPOppName],'') FROM [OpportunityMaster] WHERE [numOppId] IN (SELECT [numOppId] FROM [OpportunityItems] WHERE [numoppitemtCode]=@numOppItemID)
				SET @vcError ='You do not have enough inventory to support this shipment('+ @vcOppName +'). Modify your Qty-Shipped to an amount equal to or less than the Qty-On-Allocation and try again.' 
				--RETURN
			END
       END

COMMIT
END TRY
BEGIN CATCH
  -- Whoops, there was an error
  IF @@TRANCOUNT > 0
     ROLLBACK

  -- Raise an error with the details of the exception
  DECLARE @ErrMsg nvarchar(4000), @ErrSeverity int
  SELECT @ErrMsg = ERROR_MESSAGE(),
         @ErrSeverity = ERROR_SEVERITY()

  RAISERROR(@ErrMsg, @ErrSeverity, 1)
END CATCH
END
GO
    

GO
/****** Object:  StoredProcedure [dbo].[USP_UpdatingInventory]    Script Date: 06/01/2009 23:48:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj       
--exec usp_updatinginventory 0,181193,1,0,1       
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_updatinginventory')
DROP PROCEDURE usp_updatinginventory
GO
CREATE PROCEDURE [dbo].[USP_UpdatingInventory]        
@byteMode as tinyint=0,  
@numWareHouseItemID as numeric(9)=0,  
@Units as FLOAT,
@numWOId AS NUMERIC(9)=0,
@numUserCntID AS NUMERIC(9)=0,
@numOppId AS NUMERIC(9)=0      
as      
BEGIN TRY
BEGIN TRANSACTION
  DECLARE @Description AS VARCHAR(200)
  DECLARE @numItemCode NUMERIC(18)
  DECLARE @numDomain AS NUMERIC(18,0)
  DECLARE @ParentWOID AS NUMERIC(18,0)

  SELECT @ParentWOID=numParentWOID FROM WorkOrder WHERE numWOId=@numWOId
  SELECT @numItemCode=numItemID,@numDomain = numDomainID from WareHouseItems where numWareHouseItemID = @numWareHouseItemID
  
if @byteMode=0  -- Aseeembly Item
begin  
	DECLARE @CurrentAverageCost MONEY
	DECLARE @TotalCurrentOnHand FLOAT
	DECLARE @newAverageCost MONEY
	
	SELECT @CurrentAverageCost = monAverageCost FROM dbo.Item WHERE numItemCode =@numItemCode 
	SELECT @TotalCurrentOnHand = ISNULL((SUM(numOnHand) + SUM(numAllocation)),0) FROM dbo.WareHouseItems WHERE numItemID =@numItemCode 
	PRINT @CurrentAverageCost
	PRINT @TotalCurrentOnHand
	
	
	SELECT @newAverageCost = SUM((numQtyItemsReq * dbo.fn_UOMConversion(ID.numUOMID,I.numItemCode,I.numDomainId,I.numBaseUnit)) * I.monAverageCost )
	FROM dbo.ItemDetails ID
	LEFT JOIN dbo.Item I ON I.numItemCode = ID.numChildItemID
	LEFT JOIN UOM ON UOM.numUOMId=i.numBaseUnit
	LEFT JOIN UOM IDUOM ON IDUOM.numUOMId=ID.numUOMId
	WHERE numItemKitID = @numItemCode
	PRINT @newAverageCost
	SET @newAverageCost = ((@CurrentAverageCost * @TotalCurrentOnHand)  + (@newAverageCost * @Units)) / (@TotalCurrentOnHand + @Units)
	
	PRINT @newAverageCost

	UPDATE item SET monAverageCost =@newAverageCost WHERE numItemCode = @numItemCode

	IF ISNULL(@numWOId,0) > 0
	BEGIN
		UPDATE WareHouseItems set numOnHand=ISNULL(numOnHand,0)+@Units,numOnOrder= CASE WHEN ISNULL(numOnOrder,0) < @Units THEN 0 ELSE ISNULL(numOnOrder,0)-@Units END,dtModified = GETDATE()  where numWareHouseItemID=@numWareHouseItemID  
	END
	ELSE
	BEGIN
		UPDATE WareHouseItems set numOnHand=ISNULL(numOnHand,0)+@Units,dtModified = GETDATE()  where numWareHouseItemID=@numWareHouseItemID  
	END
end  
else if @byteMode=1  --Aseembly Child item
begin  
	declare @onHand as FLOAT                                    
	declare @onOrder as FLOAT                                    
	declare @onBackOrder as FLOAT                                   
	declare @onAllocation as FLOAT    

	select                                     
		@onHand=isnull(numOnHand,0),                                    
		@onAllocation=isnull(numAllocation,0),                                    
		@onOrder=isnull(numOnOrder,0),                                    
		@onBackOrder=isnull(numBackOrder,0)                                     
	from 
		WareHouseItems 
	where 
		numWareHouseItemID=@numWareHouseItemID  


	IF ISNULL(@numWOId,0) > 0
	BEGIN
		--RELEASE QTY FROM ALLOCATION
		SET @onAllocation=@onAllocation-@Units
	END
	ELSE
	BEGIN
		--DECREASE QTY FROM ONHAND
		SET @onHand=@onHand-@Units
	END

	 --UPDATE INVENTORY
	UPDATE 
		WareHouseItems 
	SET      
		numOnHand=@onHand,
		numOnOrder=@onOrder,                         
		numAllocation=@onAllocation,
		numBackOrder=@onBackOrder,
		dtModified = GETDATE()                                    
	WHERE 
		numWareHouseItemID=@numWareHouseItemID   
END

IF @numWOId>0
BEGIN
    
	DECLARE @numReferenceID NUMERIC(18,0)
	DECLARE @tintRefType NUMERIC(18,0)

	IF ISNULL(@numOppId,0)>0 --FROM SALES ORDER 
	BEGIN
		IF @byteMode = 0 --Aseeembly Item
			SET @Description = 'SO-WO Work Order Completed (Qty: ' + CONVERT(varchar(10),@Units) + ')'
		ELSE IF @byteMode =1 --Aseembly Child item
			IF ISNULL(@ParentWOID,0) = 0
			BEGIN
				SET @Description = 'Items Used In SO-WO (Qty: ' + CONVERT(varchar(10),@Units) + ')'
			END
			ELSE
			BEGIN
				SET @Description = 'Items Used In SO-WO Work Order (Qty: ' + CONVERT(varchar(10),@Units) + ')'
			END

		SET @numReferenceID = @numOppId
		SET @tintRefType = 3
	END
	ELSE --FROM CREATE ASSEMBLY SCREEN FROM ITEM
	BEGIN
		IF @byteMode = 0 --Aseeembly Item
			SET @Description = 'Work Order Completed (Qty: ' + CONVERT(varchar(10),@Units) + ')'
		ELSE IF @byteMode =1 --Aseembly Child item
			SET @Description = 'Items Used In Work Order (Qty: ' + CONVERT(varchar(10),@Units) + ')'

		SET @numReferenceID = @numWOId
		SET @tintRefType=2
	END

	EXEC dbo.USP_ManageWareHouseItems_Tracking
				@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
				@numReferenceID = @numReferenceID, --  numeric(9, 0)
				@tintRefType = @tintRefType, --  tinyint
				@vcDescription = @Description, --  varchar(100)
				@numModifiedBy = @numUserCntID,
				@numDomainID = @numDomain
END
ELSE if @byteMode=0
BEGIN
DECLARE @desc AS VARCHAR(100)
SET @desc= 'Create Assembly:' + CONVERT(varchar(10),@Units) + ' Units';

  EXEC dbo.USP_ManageWareHouseItems_Tracking
				@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
				@numReferenceID = @numItemCode, --  numeric(9, 0)
				@tintRefType = 1, --  tinyint
				@vcDescription = @desc, --  varchar(100)
				@numModifiedBy = @numUserCntID,
				@numDomainID = @numDomain
END
ELSE if @byteMode=1
BEGIN
SET @Description = 'Item Used in Assembly (Qty: ' + CONVERT(varchar(10),@Units) + ')' 

EXEC dbo.USP_ManageWareHouseItems_Tracking
				@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
				@numReferenceID = @numItemCode, --  numeric(9, 0)
				@tintRefType = 1, --  tinyint
				@vcDescription = @Description, --  varchar(100)
				@numModifiedBy = @numUserCntID,
				@numDomainID = @numDomain
END 
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SET @numOppID = 0

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_UpdatingInventoryForEmbeddedKits')
DROP PROCEDURE USP_UpdatingInventoryForEmbeddedKits
GO
CREATE PROCEDURE [dbo].[USP_UpdatingInventoryForEmbeddedKits]    
	@numOppItemID AS NUMERIC(18,0),
	@Units AS FLOAT,
	@Mode AS TINYINT,
	@numOppID AS NUMERIC(9),
	@tintMode AS TINYINT=0, -- 0:Add/Edit 1:Delete
	@numUserCntID AS NUMERIC(9)
AS                            
BEGIN

	DECLARE @numDomain AS NUMERIC(18,0)
	DECLARE @bitReOrderPoint BIT = 0
	DECLARE @tintOppType AS TINYINT = 0
	DECLARE @numReOrderPointOrderStatus AS NUMERIC(18,0) = 0

	SELECT 
		@numDomain = numDomainID, 
		@tintOppType=tintOppType 
	FROM 
		OpportunityMaster
	WHERE 
		numOppId = @numOppID
	
	SELECT 
		@bitReOrderPoint=ISNULL(bitReOrderPoint,0),
		@numReOrderPointOrderStatus=ISNULL(numReOrderPointOrderStatus,0) 
	FROM 
		Domain 
	WHERE 
		numDomainId = @numDomain

	SELECT 
		OKI.numOppChildItemID,
		I.numItemCode,
		numWareHouseItemId,
		numQtyItemsReq,
		numQtyShipped,
		ROW_NUMBER() OVER(ORDER BY numOppChildItemID) AS RowNumber 
	INTO 
		#tempKits
	FROM 
		OpportunityKitItems OKI 
	JOIN 
		Item I 
	ON 
		OKI.numChildItemID=I.numItemCode    
	WHERE 
		charitemtype='P' 
		AND numWareHouseItemId > 0 
		AND numWareHouseItemId IS NOT NULL 
		AND OKI.numOppID=@numOppID 
		AND OKI.numOppItemID=@numOppItemID 
  

	DECLARE @minRowNumber INT
	DECLARE @maxRowNumber INT

	DECLARE @numOppChildItemID AS NUMERIC(18,0)
	DECLARE @numItemCode NUMERIC(18,0)
	DECLARE @numWareHouseItemID NUMERIC(18,0)
	
	DECLARE @onHand FLOAT
	DECLARE @onAllocation FLOAT
	DECLARE @onOrder FLOAT
	DECLARE @onBackOrder FLOAT
	DECLARE @onReOrder FLOAT
	DECLARE @numUnits FLOAT
	DECLARE @QtyShipped FLOAT
	

	SELECT  
		@minRowNumber = MIN(RowNumber),
		@maxRowNumber = MAX(RowNumber) 
	FROM 
		#tempKits


	DECLARE @description AS VARCHAR(300)

	WHILE  @minRowNumber <= @maxRowNumber
    BEGIN
		SELECT 
			@numOppChildItemID = numOppChildItemID,
			@numItemCode=numItemCode,
			@numWareHouseItemID=numWareHouseItemID,
			@numUnits=numQtyItemsReq,
			@QtyShipped=numQtyShipped 
		FROM 
			#tempKits 
		WHERE 
			RowNumber=@minRowNumber
    
		IF @Mode=0
				SET @description='SO KIT insert/edit (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'
		ELSE IF @Mode=1
				SET @description='SO KIT Deleted (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'
		ELSE IF @Mode=2
				SET @description='SO KIT Close (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'
		ELSE IF @Mode=3
				SET @description='SO KIT Re-Open (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'

		-- IF KIT ITEM CONTAINS ANOTHER KIT AS CHILD THEN WE HAVE TO UPDATE INVENTORY OF ITEMS OF KIT CHILD ITEMS
		IF EXISTS (SELECT [numOppKitChildItemID] FROM OpportunityKitChildItems WHERE numOppID=@numOppID AND numOppItemID=@numOppItemID AND numOppChildItemID = @numOppChildItemID)
		BEGIN
			EXEC dbo.USP_ManageWareHouseItems_Tracking
				@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
				@numReferenceID = @numOppID, --  numeric(9, 0)
				@tintRefType = 3, --  tinyint
				@vcDescription = @description, --  varchar(100)
				@numModifiedBy = @numUserCntID,
				@numDomainID = @numDomain


			EXEC USP_WareHouseItems_ManageInventoryKitWithinKit
				@numOppID = @numOppID,
				@numOppItemID = @numOppItemID,
				@numOppChildItemID = @numOppChildItemID,
				@Mode = @Mode,
				@tintMode = @tintMode,
				@numDomainID = @numDomain,
				@numUserCntID = @numUserCntID,
				@tintOppType = @tintOppType,
				@bitReOrderPoint = @bitReOrderPoint,
				@numReOrderPointOrderStatus = @numReOrderPointOrderStatus
		END
		ELSE
		BEGIN
    		SELECT 
				@onHand=ISNULL(numOnHand,0),
				@onAllocation=ISNULL(numAllocation,0),                                    
				@onOrder=ISNULL(numOnOrder,0),
				@onBackOrder=ISNULL(numBackOrder,0),
				@onReOrder = ISNULL(numReorder,0)                                     
			FROM 
				WareHouseItems 
			WHERE 
				numWareHouseItemID=@numWareHouseItemID 
					
    		IF @Mode=0 --insert/edit
    		BEGIN
    			SET @numUnits = @numUnits - @QtyShipped
    	
				IF @onHand>=@numUnits                                    
				BEGIN                                    
					SET @onHand=@onHand-@numUnits                            
					SET @onAllocation=@onAllocation+@numUnits                                    
				END                                    
				ELSE IF @onHand<@numUnits                                    
				BEGIN                                    
					SET @onAllocation=@onAllocation+@onHand                                    
					SET @onBackOrder=@onBackOrder+@numUnits-@onHand                                    
					SET @onHand=0                                    
				END  
			
				-- IF REORDER POINT IS ENABLED FROM GLOABAL SETTING THEN CREATE PURCHASE OPPORTUNITY
				If @tintOppType = 1 AND (@onHand + @onOrder <= @onReOrder) AND @onReOrder > 0 AND @bitReOrderPoint = 1
				BEGIN
					BEGIN TRY
						DECLARE @numNewOppID AS NUMERIC(18,0) = 0
						EXEC USP_OpportunityMaster_CreatePO @numNewOppID OUTPUT,@numDomain,@numUserCntID,@numItemCode,@onBackOrder,@numWareHouseItemID,0,0,@numReOrderPointOrderStatus
					END TRY
					BEGIN CATCH
						--WE ARE NOT TROWING ERROR
					END CATCH
				END                       
    		END
			ELSE IF @Mode=1 --Revert
			BEGIN
				IF @QtyShipped>0
				BEGIN
					IF @tintMode=0
						SET @numUnits = @numUnits - @QtyShipped
					ELSE IF @tintmode=1
						SET @onAllocation = @onAllocation + @QtyShipped 
				END 
								                    
				IF @numUnits >= @onBackOrder 
				BEGIN
					SET @numUnits = @numUnits - @onBackOrder
					SET @onBackOrder = 0
                            
					IF (@onAllocation - @numUnits >= 0)
						SET @onAllocation = @onAllocation - @numUnits
					SET @onHand = @onHand + @numUnits                                            
				END                                            
				ELSE IF @numUnits < @onBackOrder 
				BEGIN                  
					IF (@onBackOrder - @numUnits >0)
						SET @onBackOrder = @onBackOrder - @numUnits
				END 
			END
			ELSE IF @Mode=2 --Close
			BEGIN
				SET @numUnits = @numUnits - @QtyShipped  
				SET @onAllocation = @onAllocation - @numUnits            
			END
			ELSE IF @Mode=3 --Re-Open
			BEGIN
				SET @numUnits = @numUnits - @QtyShipped
				SET @onAllocation = @onAllocation + @numUnits            
			END

			UPDATE 
				WareHouseItems 
			SET 
				numOnHand=@onHand,
				numAllocation=@onAllocation,                                    
				numBackOrder=@onBackOrder,
				dtModified = GETDATE()  
			WHERE 
				numWareHouseItemID=@numWareHouseItemID 
	
			EXEC dbo.USP_ManageWareHouseItems_Tracking
				@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
				@numReferenceID = @numOppID, --  numeric(9, 0)
				@tintRefType = 3, --  tinyint
				@vcDescription = @description, --  varchar(100)
				@numModifiedBy = @numUserCntID,
				@numDomainID = @numDomain
		END
		
	
		SET @minRowNumber=@minRowNumber + 1
    END	
  
	DROP TABLE #tempKits
END
GO
/* FOR SO:Adds Qty on allocation and deducts from onhand*/
/* FOR PO:Adds Qty on Order and updates average cost */
--exec USP_UpdatingInventoryonCloseDeal 5833      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_updatinginventoryonclosedeal')
DROP PROCEDURE usp_updatinginventoryonclosedeal
GO
CREATE PROCEDURE [dbo].[USP_UpdatingInventoryonCloseDeal]
@numOppID as numeric(9),
@numUserCntID AS NUMERIC(9)        
AS
BEGIN TRY
   BEGIN TRANSACTION      
		
		declare @tintOpptype as tinyint        
		declare @itemcode as numeric                                    
		declare @numUnits as FLOAT                                      
		declare @numoppitemtCode as numeric(9)         
		declare @numWareHouseItemID as numeric(9) 
		declare @numToWarehouseItemID as numeric(9) --to be used with stock transfer
		declare @monAmount as money 
		declare @bitStockTransfer as bit
		declare @QtyShipped as FLOAT
		declare @QtyReceived as FLOAT
		DECLARE @fltExchangeRate AS FLOAT 
		DECLARE @bitWorkOrder AS BIT

		DECLARE @numDomain AS NUMERIC(18,0)
		SELECT @numDomain = OM.numDomainID FROM [dbo].[OpportunityMaster] AS OM WHERE [OM].[numOppId] = @numOppID
		select @tintOpptype=tintOppType,@bitStockTransfer=ISNULL(bitStockTransfer,0),@fltExchangeRate=(CASE WHEN ISNULL(fltExchangeRate,0)=0 THEN 1 ELSE fltExchangeRate END) from OpportunityMaster where numOppId=@numOppID
               
		 select top 1 @numoppitemtCode=numoppitemtCode,@itemcode=OI.numItemCode,@numUnits=numUnitHour,@qtyShipped=ISNULL([numQtyShipped],0),@qtyReceived=ISNULL([numUnitHourReceived],0),        
		 @numWareHouseItemID=ISNULL(numWarehouseItmsID,0),@numToWarehouseItemID=numToWarehouseItemID,@monAmount=monTotAmount * @fltExchangeRate,@bitWorkOrder=OI.bitWorkOrder
		 FROM OpportunityItems OI join Item I                                                
		 on OI.numItemCode=I.numItemCode   and numOppId=@numOppId                                          
		 where (charitemtype='P' OR 1=(CASE WHEN @tintOpptype=1 THEN 
							CASE WHEN ISNULL(I.bitKitParent,0) = 1 AND ISNULL(I.bitAssembly,0) = 1 THEN 0
								 WHEN ISNULL(I.bitKitParent,0) = 1 THEN 1
								 ELSE 0 END 
							ELSE 0 END)) and (bitDropShip=0 or bitDropShip is null)
		 AND I.[numDomainID] = @numDomain
		 ORDER by OI.numoppitemtCode                                       
		 while @numoppitemtCode>0                                        
		  begin   
	
	
				IF @bitStockTransfer = 1
				BEGIN
					--Make inventory changes as if sales order was placed , using OpportunityItems.numWarehouseItmsID
					EXEC usp_ManageInventory @itemcode,@numWareHouseItemID,@monAmount,1,@numUnits,@QtyShipped,@qtyReceived,0,1,@numOppID,@numoppitemtCode,@numUserCntID,@bitWorkOrder
					--Make inventory changes as if purchase order was placed , using OpportunityItems.numToWarehouseItemID
					EXEC usp_ManageInventory @itemcode,@numToWarehouseItemID,@monAmount,2,@numUnits,@QtyShipped,@qtyReceived,0,1,@numOppID,@numoppitemtCode,@numUserCntID,@bitWorkOrder
				END
				ELSE
				BEGIN
					EXEC usp_ManageInventory @itemcode,@numWareHouseItemID,@monAmount,@tintOpptype,@numUnits,@QtyShipped,@qtyReceived,0,1,@numOppID,@numoppitemtCode,@numUserCntID,@bitWorkOrder
				END
                                                                                                                                      
		   select top 1 @numoppitemtCode=numoppitemtCode,@itemcode=OI.numItemCode,@numUnits=numUnitHour,@qtyShipped=ISNULL([numQtyShipped],0),@qtyReceived=ISNULL([numUnitHourReceived],0),        
		 @numWareHouseItemID=ISNULL(numWarehouseItmsID,0),@numToWarehouseItemID=numToWarehouseItemID,@monAmount=monTotAmount * @fltExchangeRate,@bitWorkOrder=OI.bitWorkOrder
			from OpportunityItems OI join Item I         
		   on OI.numItemCode=I.numItemCode and numOppId=@numOppId               
		   where (charitemtype='P' OR 1=(CASE WHEN @tintOpptype=1 THEN 
							CASE WHEN ISNULL(I.bitKitParent,0) = 1 AND ISNULL(I.bitAssembly,0) = 1 THEN 0
								 WHEN ISNULL(I.bitKitParent,0) = 1 THEN 1
								 ELSE 0 END 
							ELSE 0 END)) and OI.numoppitemtCode>@numoppitemtCode and (bitDropShip=0 or bitDropShip is null)  
			AND I.[numDomainID] = @numDomain
			order by OI.numoppitemtCode                                                
		   if @@rowcount=0 set @numoppitemtCode=0         
		  END

COMMIT
END TRY
BEGIN CATCH
  -- Whoops, there was an error
  IF @@TRANCOUNT > 0
     ROLLBACK

  -- Raise an error with the details of the exception
  DECLARE @ErrMsg nvarchar(4000), @ErrSeverity int
  SELECT @ErrMsg = ERROR_MESSAGE(),
         @ErrSeverity = ERROR_SEVERITY()

  RAISERROR(@ErrMsg, @ErrSeverity, 1)
END CATCH
        
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_WareHouseItems_ManageInventoryKitWithinKit')
DROP PROCEDURE USP_WareHouseItems_ManageInventoryKitWithinKit
GO
CREATE PROCEDURE [dbo].[USP_WareHouseItems_ManageInventoryKitWithinKit]    
	@numOppID AS NUMERIC(18,0),
	@numOppItemID AS NUMERIC(18,0),
	@numOppChildItemID AS NUMERIC(18,0),
	@Mode as TINYINT,
	@tintMode AS TINYINT=0, -- 0:Add/Edit 1:Delete
	@numDomainID AS NUMERIC(18,0),
	@numUserCntID AS NUMERIC(18,0),
	@tintOppType AS TINYINT,
	@bitReOrderPoint AS BIT,
	@numReOrderPointOrderStatus AS NUMERIC(18,0)
AS
BEGIN	
	DECLARE @TableKitChildItem TABLE
	(
		RowNo INT,
		numItemCode NUMERIC(18,0),
		numWarehouseItemID NUMERIC(18,0),
		numUnits FLOAT,
		numQtyShipped FLOAT
	)

	INSERT INTO @TableKitChildItem
	(
		RowNo,
		numItemCode,
		numWarehouseItemID,
		numUnits,
		numQtyShipped
	)
	SELECT
		ROW_NUMBER() OVER(ORDER BY numOppKitChildItemID),
		numItemID,
		numWareHouseItemId,
		numQtyItemsReq,
		numQtyShipped
	FROM
		OpportunityKitChildItems
	JOIN 
		Item I 
	ON 
		OpportunityKitChildItems.numItemID=I.numItemCode    
	WHERE 
		charitemtype = 'P' 
		AND ISNULL(numWareHouseItemId,0) > 0 
		AND numOppID = @numOppID
		AND numOppItemID = @numOppItemID
		AND numOppChildItemID = @numOppChildItemID
  
	DECLARE @numItemCode NUMERIC(18,0)
	DECLARE @numWareHouseItemID NUMERIC(18,0)
	DECLARE @onHand AS FLOAT
	DECLARE @onAllocation AS FLOAT
	DECLARE @onOrder AS FLOAT
	DECLARE @onBackOrder AS FLOAT
	DECLARE @onReOrder AS FLOAT
	DECLARE @numUnits as FLOAT
	DECLARE @QtyShipped AS FLOAT

	DECLARE @i AS INT = 1
	DECLARE @COUNT AS INT

	SELECT @COUNT = COUNT(*) FROM @TableKitChildItem

	WHILE @i <= @COUNT
	BEGIN
		SELECT 
			@numItemCode=numItemCode,
			@numWareHouseItemID=numWarehouseItemID,
			@numUnits=ISNULL(numUnits,0),
			@QtyShipped=ISNULL(numQtyShipped,0)
		FROM 
			@TableKitChildItem 
		WHERE 
			RowNo=@i
		
		DECLARE @description AS VARCHAR(100)

		IF @Mode=0
			SET @description='SO CHILD KIT insert/edit (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'
		ELSE IF @Mode=1
			SET @description='SO CHILD KIT Deleted (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'
		ELSE IF @Mode=2
			SET @description='SO CHILD KIT Close (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'
		ELSE IF @Mode=3
			SET @description='SO CHILD KIT Re-Open (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'
			
    	SELECT 
			@onHand=ISNULL(numOnHand,0),
			@onAllocation=ISNULL(numAllocation,0),                                    
			@onOrder=ISNULL(numOnOrder,0),
			@onBackOrder=ISNULL(numBackOrder,0),
			@onReOrder = ISNULL(numReorder,0)                                     
		FROM 
			WareHouseItems 
		WHERE 
			numWareHouseItemID=@numWareHouseItemID 
					
    	IF @Mode=0 --insert/edit
    	BEGIN
    		SET @numUnits = @numUnits - @QtyShipped
    	
		    IF @onHand >= @numUnits                                    
			BEGIN                                    
				SET @onHand=@onHand-@numUnits                            
				SET @onAllocation=@onAllocation+@numUnits                                    
			END                                    
			ELSE IF @onHand < @numUnits                                    
			BEGIN                                    
				SET @onAllocation=@onAllocation+@onHand                                    
				SET @onBackOrder=@onBackOrder+@numUnits-@onHand                                    
				SET @onHand=0                                    
			END  
			
			-- IF REORDER POINT IS ENABLED FROM GLOABAL SETTING THEN CREATE PURCHASE OPPORTUNITY
			If @tintOppType = 1 AND (@onHand + @onOrder <= @onReOrder) AND @onReOrder > 0 AND @bitReOrderPoint = 1
			BEGIN
				BEGIN TRY
					DECLARE @numNewOppID AS NUMERIC(18,0) = 0
					EXEC USP_OpportunityMaster_CreatePO @numNewOppID OUTPUT,@numDomainID,@numUserCntID,@numItemCode,@onBackOrder,@numWareHouseItemID,0,0,@numReOrderPointOrderStatus
				END TRY
				BEGIN CATCH
					--WE ARE NOT TROWING ERROR
				END CATCH
			END                       
    	END
        ELSE IF @Mode=1 --Revert
		BEGIN
			IF @QtyShipped>0
			BEGIN
				IF @tintMode=0
					SET @numUnits = @numUnits - @QtyShipped
				ELSE IF @tintmode=1
					SET @onAllocation = @onAllocation + @QtyShipped 
			END 
								                    
            IF @numUnits >= @onBackOrder 
            BEGIN
                SET @numUnits = @numUnits - @onBackOrder
                SET @onBackOrder = 0
                            
                IF (@onAllocation - @numUnits >= 0)
					SET @onAllocation = @onAllocation - @numUnits

                SET @onHand = @onHand + @numUnits                                            
            END                                            
            ELSE IF @numUnits < @onBackOrder 
            BEGIN                  
				IF (@onBackOrder - @numUnits >0)
					SET @onBackOrder = @onBackOrder - @numUnits					
            END 
		END       
		ELSE IF @Mode=2 --Close
		BEGIN
			SET @numUnits = @numUnits - @QtyShipped
            SET @onAllocation = @onAllocation - @numUnits            
		END
		ELSE IF @Mode=3 --Re-Open
		BEGIN
			SET @numUnits = @numUnits - @QtyShipped
            SET @onAllocation = @onAllocation + @numUnits            
		END

		UPDATE 
			WareHouseItems 
		SET 
			numOnHand=@onHand,
			numAllocation=@onAllocation,                                    
			numBackOrder=@onBackOrder,
			dtModified = GETDATE()  
		WHERE 
			numWareHouseItemID=@numWareHouseItemID 
	
		EXEC dbo.USP_ManageWareHouseItems_Tracking
			@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
			@numReferenceID = @numOppID, --  numeric(9, 0)
			@tintRefType = 3, --  tinyint
			@vcDescription = @description, --  varchar(100)
			@numModifiedBy = @numUserCntID,
			@numDomainID = @numDomainID


		SET @i = @i + 1
	END
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_WarehouseItems_Save')
DROP PROCEDURE USP_WarehouseItems_Save
GO
CREATE PROCEDURE [dbo].[USP_WarehouseItems_Save]  
	@numDomainID AS NUMERIC(18,0),
	@numUserCntID AS NUMERIC(18,0),
	@numWareHouseItemID AS NUMERIC(18,0) OUTPUT,
	@numWareHouseID AS NUMERIC(18,0),
	@numWLocationID NUMERIC(18,0),
	@numItemCode NUMERIC(18,0),
	@vcLocation AS VARCHAR(250),
	@monWListPrice AS MONEY,
	@numOnHand AS FLOAT,
	@numReorder as FLOAT,
	@vcWHSKU AS VARCHAR(100),
	@vcBarCode AS VARCHAR(100),
	@strFieldList AS TEXT,
	@vcSerialLotNo AS TEXT
AS
BEGIN
BEGIN TRY
BEGIN TRANSACTION
	DECLARE @bitLotNo AS BIT = 0  
	DECLARE @bitSerialized AS BIT = 0  
	DECLARE @vcDescription AS VARCHAR(100)

	SELECT 
		@bitLotNo=ISNULL(Item.bitLotNo,0),
		@bitSerialized=ISNULL(Item.bitSerialized,0)
	FROM 
		Item 
	WHERE 
		numItemCode=@numItemCode 
		AND numDomainID=@numDomainID

	IF ISNULL(@numWareHouseItemID,0) = 0 
	BEGIN --INSERT
		INSERT INTO WareHouseItems 
		(
			numItemID, 
			numWareHouseID,
			numWLocationID,
			numOnHand,
			numReorder,
			monWListPrice,
			vcLocation,
			vcWHSKU,
			vcBarCode,
			numDomainID,
			dtModified
		)  
		VALUES
		(
			@numItemCode,
			@numWareHouseID,
			@numWLocationID,
			(CASE WHEN @bitLotNo=1 OR @bitSerialized=1 THEN 0 ELSE @numOnHand END),
			@numReorder,
			@monWListPrice,
			@vcLocation,
			@vcWHSKU,
			@vcBarCode,
			@numDomainID,
			GETDATE()
		)  

		SELECT @numWareHouseItemID = SCOPE_IDENTITY()

		SET @vcDescription='INSERT WareHouse'
	END
	ELSE 
	BEGIN --UPDATE
		UPDATE 
			WareHouseItems  
		SET 
			numReorder=@numReorder,
			monWListPrice=@monWListPrice,
			vcLocation=@vcLocation,
			vcWHSKU=@vcWHSKU,
			vcBarCode=@vcBarCode,
			dtModified=GETDATE()   
		WHERE 
			numItemID=@numItemCode 
			AND numDomainID=@numDomainID 
			AND numWareHouseItemID=@numWareHouseItemID

		SET @vcDescription='UPDATE WareHouse'
	END

	-- SAVE MATRIX ITEM ATTRIBUTES
	IF DATALENGTH(@strFieldList)>2
	BEGIN
		--INSERT CUSTOM FIELDS BASE ON WAREHOUSEITEMS
		DECLARE @hDoc AS INT     
		EXEC sp_xml_preparedocument @hDoc OUTPUT, @strFieldList                                                                        

		
	    DECLARE @TempTable TABLE 
		(
			ID INT IDENTITY(1,1),
			Fld_ID NUMERIC(18,0),
			Fld_Value VARCHAR(300)
		)   
	                                      
		INSERT INTO @TempTable 
		(
			Fld_ID,
			Fld_Value
		)    
		SELECT
			*          
		FROM 
			OPENXML (@hDoc,'/NewDataSet/CusFlds',2)                                                  
		WITH 
			(Fld_ID NUMERIC(18,0),Fld_Value VARCHAR(300))                                        
	                                                                           

		IF (SELECT COUNT(*) FROM @TempTable) > 0                                        
		BEGIN                           
			DELETE 
				CFW_Fld_Values_Serialized_Items 
			FROM 
				CFW_Fld_Values_Serialized_Items C                                        
			INNER JOIN 
				@TempTable T 
			ON 
				C.Fld_ID=T.Fld_ID 
			WHERE 
				C.RecId= @numWareHouseItemID  
	      
			INSERT INTO CFW_Fld_Values_Serialized_Items
			(
				Fld_ID,
				Fld_Value,
				RecId,
				bitSerialized
			)                                                  
			SELECT 
				Fld_ID,
				ISNULL(Fld_Value,'') AS Fld_Value,
				@numWareHouseItemID,
				0 
			FROM 
				@TempTable                                      
		END  

		EXEC sp_xml_removedocument @hDoc 
	END	  

	-- SAVE SERIAL/LOT# 
	IF (@bitSerialized = 1 OR @bitLotNo = 1) AND DATALENGTH(@vcSerialLotNo) > 0
	BEGIN
		DECLARE @bitDuplicateLot BIT
		EXEC  @bitDuplicateLot = USP_WareHouseItmsDTL_CheckForDuplicate @numDomainID,@numWarehouseID,@numWLocationID,@numItemCode,@bitSerialized,@vcSerialLotNo

		IF @bitDuplicateLot = 1
		BEGIN
			RAISERROR('DUPLICATE_SERIALLOT',16,1)
		END

		DECLARE @TempSerialLot TABLE
		(
			vcSerialLot VARCHAR(100),
			numQty INT,
			dtExpirationDate DATETIME
		)

		DECLARE @idoc INT
		EXEC sp_xml_preparedocument @idoc OUTPUT, @vcSerialLotNo;

		INSERT INTO
			@TempSerialLot
		SELECT
			vcSerialLot,
			numQty,
			NULLIF(dtExpirationDate, '1900-01-01 00:00:00.000')
		FROM 
			OPENXML (@idoc, '/SerialLots/SerialLot',2)
		WITH 
			(
				vcSerialLot VARCHAR(100),
				numQty INT,
				dtExpirationDate DATETIME
			);

		INSERT INTO WareHouseItmsDTL
		(
			numWareHouseItemID,
			vcSerialNo,
			numQty,
			dExpirationDate,
			bitAddedFromPO
		)  
		SELECT
			@numWarehouseItemID,
			vcSerialLot,
			numQty,
			dtExpirationDate,
			0
		FROM
			@TempSerialLot
		
		DECLARE @numTotalQty AS INT
		SELECT @numTotalQty = SUM(numQty) FROM @TempSerialLot

		UPDATE WarehouseItems SET numOnHand = ISNULL(numOnHand,0) + @numTotalQty WHERE numWareHouseItemID=@numWareHouseItemID

        SET @vcDescription = CONCAT(@vcDescription + ' (Serial/Lot# Qty Added:', @numTotalQty ,')')
	END
 
	DECLARE @dtDATE AS DATETIME = GETUTCDATE()

	EXEC dbo.USP_ManageWareHouseItems_Tracking
		@numWareHouseItemID = @numWareHouseItemID,
		@numReferenceID = @numItemCode,
		@tintRefType = 1,
		@vcDescription = @vcDescription,
		@numModifiedBy = @numUserCntID,
		@ClientTimeZoneOffset = 0,
		@dtRecordDate = @dtDATE,
		@numDomainID = @numDomainID 
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SET @numWareHouseItemID = 0

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
END

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_WorkOrder_InsertRecursive')
DROP PROCEDURE USP_WorkOrder_InsertRecursive
GO
CREATE PROCEDURE [dbo].[USP_WorkOrder_InsertRecursive]
	@numOppID AS NUMERIC(9)=0,
	@numItemCode AS NUMERIC(18,0),
	@numQty AS FLOAT,
	@numWarehouseItemID AS NUMERIC(18,0),
	@numDomainID AS NUMERIC(18,0),
	@numUserCntID AS NUMERIC(18,0)=0,
	@numQtyShipped AS FLOAT,
	@numParentWOID AS NUMERIC(18,0),
	@bitFromWorkOrderScreen AS BIT
AS
BEGIN
	DECLARE @numWOID NUMERIC(18,0)
	DECLARE @vcInstruction VARCHAR(2000)
	DECLARE @numAssignedTo NUMERIC(18,0)
	DECLARE @bintCompletionDate DATETIME

	SELECT @vcInstruction=vcInstruction,@numAssignedTo=numAssignedTo,@bintCompletionDate=bintCompliationDate FROM WorkOrder WHERE numWOId=@numParentWOID

	INSERT INTO WorkOrder
	(
		numItemCode,numQtyItemsReq,numWareHouseItemId,numCreatedBy,bintCreatedDate,numDomainID,numWOStatus,numOppId, numParentWOID,vcInstruction,numAssignedTo,bintCompliationDate
	)
	VALUES
	(
		@numItemCode,@numQty,@numWarehouseItemID,@numUserCntID,getutcdate(),@numDomainID,0,@numOppID,@numParentWOID,@vcInstruction,@numAssignedTo,@bintCompletionDate
	)

	SELECT @numWOID = SCOPE_IDENTITY()

	INSERT INTO [WorkOrderDetails] 
	(
		numWOId,numItemKitID,numChildItemID,numQtyItemsReq,numWareHouseItemId,vcItemDesc,sintOrder,numQtyItemsReq_Orig,numUOMId
	)
	SELECT 
		@numWOId,numItemKitID,numItemCode,
		CAST(((DTL.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(Dtl.numUOMId,item.numItemCode,item.numDomainID,item.numBaseUnit),1)) * @numQty)AS NUMERIC(9,0)),
		isnull(Dtl.numWareHouseItemId,0),
		ISNULL(Dtl.vcItemDesc,txtItemDesc),
		ISNULL(sintOrder,0),
		DTL.numQtyItemsReq,
		Dtl.numUOMId 
	FROM 
		item                                
	INNER JOIN 
		ItemDetails Dtl 
	ON 
		numChildItemID=numItemCode
	WHERE 
		numItemKitID=@numItemCode

	DECLARE @Description AS VARCHAR(1000)

	--UPDATE ON ORDER OF ASSEMBLY
	UPDATE 
		WareHouseItems
	SET    
		numOnOrder= ISNULL(numOnOrder,0) + @numQty,
		dtModified = GETDATE() 
	WHERE   
		numWareHouseItemID = @numWareHouseItemID 

	IF ISNULL(@bitFromWorkOrderScreen,0) = 0
	BEGIN
		
		SET @Description='SO-WO Work Order Created (Qty:' + CAST(@numQty AS VARCHAR(10)) + ')'

		--MAKE ENTRY IN WAREHOUSE TRACKING TABLE
		EXEC dbo.USP_ManageWareHouseItems_Tracking
		@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
		@numReferenceID = @numOppID, --  numeric(9, 0)
		@tintRefType = 3, --  tinyint
		@vcDescription = @Description, --  varchar(100)
		@numModifiedBy = @numUserCntID,
		@numDomainID = @numDomainID

		EXEC USP_ManageInventoryAssemblyWO @numUserCntID,@numOppID,@numItemCode,@numWarehouseItemID,@numQtyShipped,1
	END
	ELSE IF ISNULL(@bitFromWorkOrderScreen,0) = 1
	BEGIN
		SET @Description='Work Order Created (Qty:' + CAST(@numQty AS VARCHAR(10)) + ')'

		--MAKE ENTRY IN WAREHOUSE TRACKING TABLE
		EXEC dbo.USP_ManageWareHouseItems_Tracking
		@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
		@numReferenceID = @numWOId, --  numeric(9, 0)
		@tintRefType = 2, --  tinyint
		@vcDescription = @Description, --  varchar(100)
		@numModifiedBy = @numUserCntID,
		@numDomainID = @numDomainID

		EXEC USP_ManageInventoryWorkOrder @numWOID,@numDomainID,@numUserCntID
	END
END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_WorkOrderInsert')
DROP PROCEDURE USP_WorkOrderInsert
GO
CREATE PROCEDURE [dbo].[USP_WorkOrderInsert]
@numItemCode AS numeric(9),
@numUnitHour AS numeric(9),
@numWarehouseItmsID AS NUMERIC(9),
@numOppID AS numeric(9)=0,
@numDomainID AS numeric(9),
@numUserCntID AS numeric(9)
AS
BEGIN

  DECLARE @numWOId AS NUMERIC(9)
  
  DECLARE @txtItemDesc AS varchar(1000)
	
	SELECT @txtItemDesc=ISNULL(txtItemDesc,'') FROM Item WHERE numItemCode=@numItemCode

	insert into WorkOrder(numItemCode,numQtyItemsReq,numWareHouseItemId,numCreatedBy,bintCreatedDate,numDomainID,numWOStatus,numOppId,vcItemDesc)
	values(@numItemCode,@numUnitHour,@numWarehouseItmsID,@numUserCntID,getutcdate(),@numDomainID,0,@numOppID,@txtItemDesc)
	
	set @numWOId=@@IDENTITY
	
	
;WITH CTE(numItemKitID,numItemCode,numWarehouseItmsID,numCalculatedQty,txtItemDesc,sintOrder,numUOMId,numQtyItemsReq)
AS
(
select CAST(@numItemCode AS NUMERIC(9)),numItemCode,isnull(Dtl.numWareHouseItemId,0) as numWarehouseItmsID
,CAST(((DTL.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(Dtl.numUOMID,item.numItemCode,item.numDomainID,item.numBaseUnit),1)) * @numUnitHour)AS NUMERIC(9,0)) AS numCalculatedQty,ISNULL(Dtl.vcItemDesc,txtItemDesc) AS txtItemDesc,
ISNULL(Dtl.sintOrder,0),Dtl.numUOMId,DTL.numQtyItemsReq
from item                                
INNER join ItemDetails Dtl on numChildItemID=numItemCode
where  numItemKitID=@numItemCode 
--AND item.charItemType IN ('P','N')

UNION ALL

select CAST(Dtl.numItemKitID AS NUMERIC(9)),i.numItemCode,isnull(Dtl.numWareHouseItemId,0) as numWarehouseItmsID,
CAST(((DTL.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(Dtl.numUOMID,i.numItemCode,i.numDomainID,i.numBaseUnit),1)) * c.numCalculatedQty) AS NUMERIC(9,0)) AS numCalculatedQty,ISNULL(Dtl.vcItemDesc,i.txtItemDesc) AS txtItemDesc,
ISNULL(Dtl.sintOrder,0),Dtl.numUOMId,DTL.numQtyItemsReq
from item i                               
INNER JOIN ItemDetails Dtl on Dtl.numChildItemID=i.numItemCode
INNER JOIN CTE c ON Dtl.numItemKitID = c.numItemCode 
--WHERE i.charItemType IN ('P','N')
)

INSERT  INTO [WorkOrderDetails] (numWOId,numItemKitID,numChildItemID,numQtyItemsReq,numWareHouseItemId,vcItemDesc,sintOrder,numQtyItemsReq_Orig,numUOMId)
SELECT @numWOId,numItemKitID,numItemCode,numCalculatedQty,numWarehouseItmsID,txtItemDesc,sintOrder,numQtyItemsReq,numUOMId FROM CTE

END
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_OpportunityItems_OrderwiseItems' ) 
    DROP PROCEDURE USP_OpportunityItems_OrderwiseItems
GO
-- =============================================        
-- Author:  <Author,,Sachin Sadhu>        
-- Create date: <Create Date,,30Dec2013>        
-- Description: <Description,,To get Items Customer wise n order wise>        
-- Module:Accounts/Items tab        
-- ============================================= 
CREATE PROCEDURE [dbo].[USP_OpportunityItems_OrderwiseItems]         
 -- Add the parameters for the stored procedure here        
    @numDomainId NUMERIC(9) ,    
    @numDivisionId NUMERIC(9) ,    
    @keyword VARCHAR(50) ,    
    @startDate AS DATETIME =NULL,    
    @endDate AS DATETIME =NULL,    
    @imode AS INT ,    
    @CurrentPage INT = 0 ,    
    @PageSize INT = 0 ,    
    @TotRecs INT = 0 OUTPUT    
AS     
    BEGIN        
 -- SET NOCOUNT ON added to prevent extra result sets from        
 -- interfering with SELECT statements.        
        SET NOCOUNT ON;        
                
    -- Insert statements for procedure here        
        IF ( @imode = 1 )     
            BEGIN        
                   
                CREATE TABLE #tempItems    
                    (    
                      Units FLOAT,    
                      BasePrice DECIMAL(18,2) ,    
                      ItemName VARCHAR(300) ,    
                      base DECIMAL(18,2) ,    
                      ChargedAmount DECIMAL(18,2) ,    
                      ORDERID NUMERIC(9) ,    
                      ModelId VARCHAR(200) ,    
                      OrderDate VARCHAR(30) ,    
                      tintOppType TINYINT ,    
                      tintOppStatus TINYINT ,    
                      numContactId NUMERIC ,    
                      numDivisionId NUMERIC ,    
                      OppId NUMERIC ,    
                      OrderNo VARCHAR(300)    
                    )        
                                
                INSERT  INTO #tempItems    
                        SELECT  OI.numUnitHour AS Units ,    
                                OI.monPrice AS BasePrice ,    
                                OI.vcItemName AS ItemName ,    
                                OI.monAvgCost AS base ,    
                                OI.monTotAmount AS ChargedAmount ,    
                                Oi.numItemCode AS ORDERID ,    
                                ISNULL(oi.vcModelID, 'NA') AS ModelId ,    
                                dbo.FormatedDateFromDate(OM.bintCreatedDate,OM.numDomainId)   AS OrderDate,     
                                om.tintOppType ,    
                                om.tintOppStatus ,    
                                Om.numContactId ,    
                                ACI.numDivisionId ,    
                                Om.numOppId AS OppId ,    
								 Om.vcPOppName AS OrderNo  
                        FROM    dbo.OpportunityItems AS OI    
                                INNER JOIN dbo.OpportunityMaster OM ON OI.numOppId = OM.numOppId    
                                INNER JOIN dbo.AdditionalContactsInformation ACI ON ACI.numContactId = om.numContactId    
                        WHERE   Om.numDomainId = @numDomainId    
                                AND ACI.numDivisionId = @numDivisionId    
                                AND ( OI.vcItemName LIKE @keyword + '%'    
                                      OR OI.vcModelID LIKE @keyword + '%'    
      )    
                                AND ( OM.bintCreatedDate >= ISNULL(@startDate,OM.bintCreatedDate)    
                                      AND Om.bintCreatedDate <=ISNULL(@endDate,OM.bintCreatedDate)     
                                    )         
                                            
                DECLARE @firstRec AS INTEGER          
                DECLARE @lastRec AS INTEGER          
          
                SET @firstRec = ( @CurrentPage - 1 ) * @PageSize          
                SET @lastRec = ( @CurrentPage * @PageSize + 1 )          
                SET @TotRecs = ( SELECT COUNT(*)    
                                 FROM   #tempItems    
                               )          
                   
                SELECT  *    
                FROM    ( SELECT    * ,    
                                    ROW_NUMBER() OVER ( ORDER BY ItemName ) AS RowNumber    
                          FROM      #tempItems    
                        ) a    
                WHERE   RowNumber > @firstRec    
                        AND RowNumber < @lastRec    
                ORDER BY ItemName         
                            
                     
                DROP TABLE #tempItems          
            END        
                   
        ELSE     
            BEGIN       
                   
                CREATE TABLE #tempVItems    
                    (    
                      ItemName VARCHAR(300) ,    
                      MinOrderQty INT ,    
                      MinCost DECIMAL(18,2) ,    
                      ModelId VARCHAR(200) ,    
                      PartNo VARCHAR(100) ,    
                      numItemCode NUMERIC    
                    )        
                INSERT  INTO #tempVItems    
                        SELECT  i.vcItemName AS ItemName ,    
                                v.intMinQty AS MinOrderQty ,    
                                v.monCost AS MinCost ,    
                                I.vcModelID AS ModelId ,    
                                v.vcPartNo AS PartNo ,    
                                v.numItemCode AS numItemCode    
                        FROM    dbo.Item AS I    
                                INNER JOIN dbo.Vendor AS V ON I.numVendorID = V.numVendorID    
                        WHERE   i.numVendorID = @numDivisionId    
                                AND v.numItemCode = i.numItemCode    
                                AND v.numDomainID = @numDomainId        
                              AND ( I.vcItemName LIKE @keyword + '%'    
                                      OR I.vcModelID LIKE @keyword + '%'    
                                    )    
                 
                 
                DECLARE @VfirstRec AS INTEGER          
                DECLARE @VlastRec AS INTEGER          
          
                SET @VfirstRec = ( @CurrentPage - 1 ) * @PageSize          
                SET @VlastRec = ( @CurrentPage * @PageSize + 1 )          
                SET @TotRecs = ( SELECT COUNT(*)    
                                 FROM   #tempVItems    
                               )          
                     
                            
                SELECT  *    
                FROM    ( SELECT    * ,    
                                    ROW_NUMBER() OVER ( ORDER BY ItemName ) AS RowNumber    
                          FROM      #tempVItems    
                        ) a    
                WHERE   RowNumber > @VfirstRec    
                        AND RowNumber < @VlastRec    
                ORDER BY ItemName               
                            
                    
                DROP TABLE #tempVItems
            END                         
    END 