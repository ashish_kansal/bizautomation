/******************************************************************
Project: Release 6.4 Date: 24.NOV.2016
Comments: ALTER SCRIPTS
*******************************************************************/

/************************** SANDEEP ************************/

USE [Production.2014]
GO

/****** Object:  Table [dbo].[ListDetailsName]    Script Date: 18-Nov-16 5:18:36 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ListDetailsName](
	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numDomainID] [numeric](18, 0) NOT NULL,
	[numListID] [numeric](18, 0) NOT NULL,
	[numListItemID] [numeric](18, 0) NOT NULL,
	[vcName] [varchar](300) NOT NULL,
 CONSTRAINT [PK_ListDetailsName] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING ON
GO

ALTER TABLE [dbo].[ListDetailsName]  WITH CHECK ADD  CONSTRAINT [FK_ListDetailsName_ListDetails] FOREIGN KEY([numListItemID])
REFERENCES [dbo].[ListDetails] ([numListItemID])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[ListDetailsName] CHECK CONSTRAINT [FK_ListDetailsName_ListDetails]
GO

ALTER TABLE [dbo].[ListDetailsName]  WITH CHECK ADD  CONSTRAINT [FK_ListDetailsName_ListMaster] FOREIGN KEY([numListID])
REFERENCES [dbo].[ListMaster] ([numListID])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[ListDetailsName] CHECK CONSTRAINT [FK_ListDetailsName_ListMaster]
GO


-----------------------------------------------

USE [Production.2014]
GO

/****** Object:  Index [NCIX_ListDetailsName_20161118_171738]    Script Date: 18-Nov-16 5:19:31 PM ******/
CREATE NONCLUSTERED INDEX [NCIX_ListDetailsName_20161118_171738] ON [dbo].[ListDetailsName]
(
	[numDomainID] ASC
)
INCLUDE ( 	[numListID],
	[numListItemID]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

-------------------------------------

BEGIN TRY
BEGIN TRANSACTION

	
 -- 1. Leads - ModileID = 2

	INSERT INTO PageMaster 
		(numPageID, numModuleID, vcFileName, vcPageDesc, bitIsViewApplicable, bitIsAddApplicable, bitIsUpdateApplicable, bitIsDeleteApplicable, bitIsExportApplicable)
	VALUES
		(128,2,'','Assign To',0,0,1,0,0)


-- 2. Prospects - ModileID = 3

	INSERT INTO PageMaster 
		(numPageID, numModuleID, vcFileName, vcPageDesc, bitIsViewApplicable, bitIsAddApplicable, bitIsUpdateApplicable, bitIsDeleteApplicable, bitIsExportApplicable)
	VALUES
		(128,3,'','Assign To',0,0,1,0,0)
	
-- 3. Accounts - ModileID = 4

	INSERT INTO PageMaster 
		(numPageID, numModuleID, vcFileName, vcPageDesc, bitIsViewApplicable, bitIsAddApplicable, bitIsUpdateApplicable, bitIsDeleteApplicable, bitIsExportApplicable)
	VALUES
		(128,4,'','Assign To',0,0,1,0,0)

-- 6. Opportunities/Orders - ModileID = 10

	INSERT INTO PageMaster 
		(numPageID, numModuleID, vcFileName, vcPageDesc, bitIsViewApplicable, bitIsAddApplicable, bitIsUpdateApplicable, bitIsDeleteApplicable, bitIsExportApplicable)
	VALUES
		(128,10,'','Assign To',0,0,1,0,0)


	SELECT 
		* 
	INTO 
		#temp 
	FROM
	(
		SELECT 
			ROW_NUMBER() OVER(ORDER BY numDomainId DESC) AS Row, 
			numDomainId 
		FROM 
			Domain 
		WHERE 
			numDomainId <> -255
	) TABLE2
	
	DECLARE @RowCount INT
	SET @RowCount = (SELECT COUNT(Row) FROM #temp) 
	
	DECLARE @I INT
	DECLARE @numDomainId NUMERIC(18,0)
	
	SET @I = 1

	WHILE (@I <= @RowCount)
	BEGIN
			
		SELECT @numDomainId = numDomainID FROM #temp WHERE Row = @I
	
		-- 1. Leads - ModileID = 2
		INSERT INTO dbo.GroupAuthorization
			(numDomainID,numModuleID,numPageID,numGroupID,intExportAllowed,intPrintAllowed,intViewAllowed,intAddAllowed,intUpdateAllowed,intDeleteAllowed)
		SELECT
			@numDomainId,2,128,numGroupID,0,0,0,0,3,0
		FROM
			AuthenticationGroupMaster
		WHERE
			numDomainID = @numDomainId AND
			tintGroupType = 1

		-- 2. Prospects - ModileID = 3
		INSERT INTO dbo.GroupAuthorization
			(numDomainID,numModuleID,numPageID,numGroupID,intExportAllowed,intPrintAllowed,intViewAllowed,intAddAllowed,intUpdateAllowed,intDeleteAllowed)
		SELECT
			@numDomainId,3,128,numGroupID,0,0,0,0,3,0
		FROM
			AuthenticationGroupMaster
		WHERE
			numDomainID = @numDomainId AND
			tintGroupType = 1

		-- 3. Accounts - ModileID = 4
		INSERT INTO dbo.GroupAuthorization
			(numDomainID,numModuleID,numPageID,numGroupID,intExportAllowed,intPrintAllowed,intViewAllowed,intAddAllowed,intUpdateAllowed,intDeleteAllowed)
		SELECT
			@numDomainId,4,128,numGroupID,0,0,0,0,3,0
		FROM
			AuthenticationGroupMaster
		WHERE
			numDomainID = @numDomainId AND
			tintGroupType = 1

		-- 6. Opportunities/Orders - ModileID = 10
		INSERT INTO dbo.GroupAuthorization
			(numDomainID,numModuleID,numPageID,numGroupID,intExportAllowed,intPrintAllowed,intViewAllowed,intAddAllowed,intUpdateAllowed,intDeleteAllowed)
		SELECT
			@numDomainId,10,128,numGroupID,0,0,0,0,3,0
		FROM
			AuthenticationGroupMaster
		WHERE
			numDomainID = @numDomainId AND
			tintGroupType = 1

		SET @I = @I  + 1
	END
	
	DROP TABLE #temp
COMMIT
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		ERROR_MESSAGE(),
		ERROR_NUMBER(),
		ERROR_SEVERITY(),
		ERROR_STATE(),
		ERROR_LINE(),
		ISNULL(ERROR_PROCEDURE(), '-');
END CATCH



/************************** PRASANT ************************/


IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 29 AND numFormID = 43)
BEGIN
	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,29,43,'Organization Rating',0,0,0,0,1)
END
IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 30 AND numFormID = 43)
BEGIN
	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,30,43,'Organization Status',0,0,0,0,1)
END
IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 21 AND numFormID = 43)
BEGIN
	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,21,43,'Territory',0,0,0,0,1)
END
IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 7 AND numFormID = 43)
BEGIN
	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,7,43,'Campaign',0,0,0,0,1)
END