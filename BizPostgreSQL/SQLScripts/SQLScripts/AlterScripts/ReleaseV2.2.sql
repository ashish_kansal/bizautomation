/******************************************************************
Project: Release 2.2 Date: 10.10.2013
Comments: 
*******************************************************************/

/*******************Kamal Script******************/
--/************************************************************************************************/
--/************************************************************************************************/
ALTER TABLE dbo.DivisionMaster ADD
	numDefaultPaymentMethod numeric(18, 0) NULL,
	numDefaultCreditCard numeric(18, 0) NULL,
	bitOnCreditHold bit NULL

--SELECT * FROM dbo.ReturnHeader WHERE numDomainId=172 AND tintReturnType=1 and numBizDocTempID=978
--UPDATE ReturnHeader SET numBizDocTempID=974 WHERE numDomainId=172 AND tintReturnType=1 and numBizDocTempID=978
	
	

--SELECT * FROM ReportFieldGroupMappingMaster where vcFieldName like '%Date%' AND numReportFieldGroupID=3

UPDATE ReportFieldGroupMappingMaster SET vcFieldName='Order Created Date' WHERE vcFieldName like '%Created Date%' AND numReportFieldGroupID=3
UPDATE ReportFieldGroupMappingMaster SET vcFieldName='Order Due Date' WHERE vcFieldName like '%Due Date%' AND numReportFieldGroupID=3
UPDATE ReportFieldGroupMappingMaster SET vcFieldName='Order Estimated Created Date' WHERE vcFieldName like '%Closing Date%' AND numReportFieldGroupID=3
	
UPDATE ReportFieldGroupMappingMaster SET vcFieldName='BizDoc Billing Date' WHERE vcFieldName like '%Billing Date%' AND numReportFieldGroupID=5
UPDATE ReportFieldGroupMappingMaster SET vcFieldName='BizDoc Created Date' WHERE vcFieldName like '%Created Date%' AND numReportFieldGroupID=5
UPDATE ReportFieldGroupMappingMaster SET vcFieldName='BizDoc Due Date' WHERE vcFieldName like '%Due Date%' AND numReportFieldGroupID=5
UPDATE ReportFieldGroupMappingMaster SET vcFieldName='BizDoc Modified Date' WHERE vcFieldName like '%Modified Date%' AND numReportFieldGroupID=5
	
	
	
--SELECT * FROM ReportFieldGroupMaster
INSERT INTO [dbo].[ReportFieldGroupMaster]([vcFieldGroupName], [bitActive], [bitCustomFieldGroup], [numGroupID], [vcCustomTableName])
SELECT N'Leads Custom Field', 1, 1, 14, N'CFW_FLD_Values' UNION ALL
SELECT N'Prospect Custom Field', 1, 1, 12, N'CFW_FLD_Values' UNION ALL
SELECT N'Account Custom Field', 1, 1, 13, N'CFW_FLD_Values'	


--SELECT * FROM ReportModuleGroupFieldMappingMaster WHERE numReportFieldGroupID=15
INSERT INTO [dbo].[ReportModuleGroupFieldMappingMaster]([numReportModuleGroupID], [numReportFieldGroupID])
SELECT 1, 27 UNION ALL
SELECT 5, 27 UNION ALL
SELECT 14, 27 UNION ALL
SELECT 16, 27 UNION ALL
SELECT 1, 28 UNION ALL
SELECT 5, 28 UNION ALL
SELECT 14, 28 UNION ALL
SELECT 16, 28 UNION ALL
SELECT 1, 29 UNION ALL
SELECT 5, 29 UNION ALL
SELECT 14, 29 UNION ALL
SELECT 16, 29 

---Add Custom Fields to Other Module(Orders)
INSERT INTO [dbo].[ReportModuleGroupFieldMappingMaster]([numReportModuleGroupID], [numReportFieldGroupID])
SELECT 8, 15 UNION ALL
SELECT 8, 27 UNION ALL
SELECT 8, 28 UNION ALL
SELECT 8, 29 UNION ALL
SELECT 9, 15 UNION ALL
SELECT 9, 27 UNION ALL
SELECT 9, 28 UNION ALL
SELECT 9, 29 UNION ALL
SELECT 10, 15 UNION ALL
SELECT 10, 27 UNION ALL
SELECT 10, 28 UNION ALL
SELECT 10, 29 UNION ALL
SELECT 13, 15 UNION ALL
SELECT 13, 27 UNION ALL
SELECT 13, 28 UNION ALL
SELECT 13, 29
--=========================================================================================	
DECLARE @v sql_variant 
SET @v = N'0: BizForm Wizards
1: Regular Form (Grid Settings)
2: Other Form
3: WorkFlow Form'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'DynamicFormMaster', N'COLUMN', N'tintFlag'


INSERT INTO dbo.DynamicFormMaster (numFormId,vcFormName,cCustomFieldsAssociated,
	cAOIAssociated,bitDeleted,tintFlag,bitWorkFlow,vcLocationID,bitAllowGridColor)
SELECT 68,'Organization','Y','N',0,3,0,'1,12,13,14',0 UNION
SELECT 69,'Contacts','Y','N',0,3,0,'4',0 UNION
SELECT 70,'Opportunities & Orders','Y','N',0,3,0,'2,6',0 UNION
SELECT 71,'BizDocs','Y','N',0,3,0,'',0 UNION
SELECT 72,'Cases','Y','N',0,3,0,'3',0 UNION
SELECT 73,'Projects','Y','N',0,3,0,'11',0 UNION
SELECT 74,'Items','Y','N',0,3,0,'5',0

--=========================================================================================

ALTER TABLE dbo.OpportunityBizDocs ADD
	bitAutoCreated bit NULL	 
	
UPDATE OpportunityBizDocs SET bitAutoCreated=0

UPDATE dbo.OpportunityBizDocs SET bitAutoCreated=1 WHERE numOppBizDocsId IN (
SELECT numOppBizDocsId FROM dbo.OpportunityAutomationQueueExecution WHERE numRuleID IN (1,2))

--=========================================================================================

--SELECT * FROM dbo.ReportFieldGroupMaster
INSERT INTO [dbo].[ReportFieldGroupMaster]([vcFieldGroupName], [bitActive], [bitCustomFieldGroup], [numGroupID], [vcCustomTableName])
SELECT N'Item Price Level', 1, NULL, NULL, NULL

--SELECT *FROM dbo.ReportModuleGroupMaster
INSERT INTO [dbo].[ReportModuleGroupFieldMappingMaster]([numReportModuleGroupID], [numReportFieldGroupID])
SELECT 19, 30 

INSERT INTO [dbo].[ReportModuleGroupFieldMappingMaster]([numReportModuleGroupID], [numReportFieldGroupID])
SELECT 23, 30 


SELECT * FROM dbo.DycFieldMaster WHERE numModuleID=4 AND vcLookBackTableName='PricingRuleTable'

INSERT INTO [dbo].[DycFieldMaster]([numModuleID], [numDomainID], [vcFieldName], [vcDbColumnName], [vcOrigDbColumnName], [vcPropertyName], [vcLookBackTableName], [vcFieldDataType], [vcFieldType], [vcAssociatedControlType], [vcToolTip], [vcListItemType], [numListID], [PopupFunctionName], [order], [tintRow], [tintColumn], [bitInResults], [bitDeleted], [bitAllowEdit], [bitDefault], [bitSettingField], [bitAddField], [bitDetailField], [bitAllowSorting], [bitWorkFlowField], [bitImport], [bitExport], [bitAllowFiltering], [bitInlineEdit], [bitRequired], [intColumnWidth], [intFieldMaxLength])
SELECT 4, NULL, N'Qty From', N'intFromQty', N'intFromQty', NULL, N'PricingRuleTable', N'N', N'R', N'TextBox', NULL, NULL, 0, NULL, 18, NULL, NULL, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL
UNION
SELECT 4, NULL, N'Qty To', N'intToQty', N'intToQty', NULL, N'PricingRuleTable', N'N', N'R', N'TextBox', NULL, NULL, 0, NULL, 18, NULL, NULL, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL
UNION
SELECT 4, NULL, N'Rule Type', N'vcRuleType', N'vcRuleType', NULL, N'PricingRuleTable', N'V', N'R', N'TextBox', NULL, NULL, 0, NULL, 18, NULL, NULL, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL
UNION
SELECT 4, NULL, N'Discount Type', N'vcDiscountType', N'vcDiscountType', NULL, N'PricingRuleTable', N'V', N'R', N'TextBox', NULL, NULL, 0, NULL, 18, NULL, NULL, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL
UNION
SELECT 4, NULL, N'Discount', N'decDiscount', N'decDiscount', NULL, N'PricingRuleTable', N'M', N'R', N'TextBox', NULL, NULL, 0, NULL, 18, NULL, NULL, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL
UNION
SELECT 4, NULL, N'Price Level Price', N'monPriceLevelPrice', N'monPriceLevelPrice', NULL, N'PricingRuleTable', N'M', N'R', N'TextBox', NULL, NULL, 0, NULL, 18, NULL, NULL, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL
UNION
SELECT 4, NULL, N'Price Level Type', N'vcPriceLevelType', N'vcPriceLevelType', NULL, N'PricingRuleTable', N'V', N'R', N'TextBox', NULL, NULL, 0, NULL, 18, NULL, NULL, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL


INSERT INTO [dbo].[ReportFieldGroupMappingMaster]([numReportFieldGroupID], [numFieldID], [vcFieldName], 
[vcAssociatedControlType], [vcFieldDataType], [bitAllowSorting], [bitAllowGrouping], [bitAllowAggregate], [bitAllowFiltering])
SELECT 30, numFieldId, vcFieldName, vcDbColumnName, vcFieldDataType, 0, 0, 0, 0 FROM DycFieldMaster 
WHERE numModuleID=4 AND vcLookBackTableName='PricingRuleTable'

/*******************Manish Script******************/
/******************************************************************/
/******************************************************************
Project: PORTAL   Date: 30.Sep.2013
Comments: Portal Column settings, adding form ids, form fields 
*******************************************************************/
BEGIN TRANSACTION
ALTER TABLE dbo.eCommerceDTL ADD bitShowPriceUsingPriceLevel BIT
ROLLBACK
