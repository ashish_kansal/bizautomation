/******************************************************************
Project: Release 5.4 Date: 04.Feb.2016
Comments: STORED PROCEDURES
*******************************************************************/


/****** Object:  UserDefinedFunction [dbo].[fn_GetAttributes]    Script Date: 07/26/2008 18:12:28 ******/

GO

GO
--- Created By Anoop Jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_getattributes')
DROP FUNCTION fn_getattributes
GO
CREATE FUNCTION [dbo].[fn_GetAttributes] (@numRecID numeric,@bitSerialized  bit)
returns varchar(100)
as
begin
declare @strAttribute varchar(100)
declare @numCusFldID numeric(9)
declare @numCusFldValue varchar(100)
set @numCusFldID=0
set @strAttribute=''
select top 1 @numCusFldID=fld_id,@numCusFldValue=fld_value from CFW_Fld_Values_Serialized_Items where RecId=@numRecID and fld_value!='0' and fld_value!='' and bitSerialized=@bitSerialized order by fld_id
while @numCusFldID>0
begin
	if isnumeric(@numCusFldValue)=1
	begin
		--set @strAttribute=@strAttribute+@numCusFldValue
		select @strAttribute=@strAttribute+Fld_label from  CFW_Fld_Master where Fld_id=@numCusFldID AND Grp_id = 9
                
        IF LEN(@strAttribute) > 0
        BEGIN
			select @strAttribute=@strAttribute+':'+ vcData +',' from ListDetails where numListItemID=@numCusFldValue
        END
		
	end
	else
	begin
		select @strAttribute=@strAttribute+Fld_label from  CFW_Fld_Master where Fld_id=@numCusFldID AND Grp_id = 9
		IF LEN(@strAttribute) > 0
        BEGIN
			select @strAttribute=@strAttribute+':-,' 
        END
	end
	select top 1 @numCusFldID=fld_id,@numCusFldValue=fld_value from CFW_Fld_Values_Serialized_Items where RecId=@numRecID and fld_value!='0' and fld_value!='' and bitSerialized=@bitSerialized and fld_id>@numCusFldID order by fld_id
	if @@rowcount=0 set @numCusFldID=0
	
end

If LEN(@strAttribute) > 0
BEGIN
	SET @strAttribute = LEFT(@strAttribute, LEN(@strAttribute) - 1)
END

return @strAttribute
end
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_GetCustFldStringValue')
DROP FUNCTION fn_GetCustFldStringValue
GO
CREATE FUNCTION [dbo].[fn_GetCustFldStringValue]
(
	@numFldId NUMERIC(18,0),
	@numRecordID NUMERIC(18,0),
	@vcFldValue VARCHAR(MAX)
)  
RETURNS VARCHAR(500)   
AS  
BEGIN  
	DECLARE @vcValue as  varchar(500)  

	DECLARE @fld_type AS VARCHAR(20)
	SELECT @fld_type = Fld_type FROM dbo.CFW_Fld_Master WHERE Fld_id = @numFldId

	IF @fld_type = 'SelectBox' 
	BEGIN
		SET @vcValue = (SELECT vcData FROM dbo.ListDetails WHERE numListItemID = @vcFldValue)
	END
	ELSE IF @fld_type = 'CheckBoxList'
	BEGIN
		SELECT 
			@vcValue = STUFF((SELECT CONCAT(',', vcData) 
		FROM 
			ListDetails 
		WHERE 
			numlistitemid IN (SELECT ISNULL(Id,0) FROM dbo.SplitIDs(@vcFldValue,',')) FOR XML PATH('')), 1, 1, '')
	END
	ELSE IF @fld_type = 'CheckBox'
	BEGIN
		SET @vcValue = (CASE WHEN ISNULL(@vcFldValue,0)=1 THEN 'Yes' ELSE 'No' END)
	END
	ELSE
	BEGIN
		SET @vcValue = @vcFldValue
	END

	IF @vcValue IS NULL 
		SET @vcValue=''  

	RETURN @vcValue  
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CategoryProfile_Delete')
DROP PROCEDURE dbo.USP_CategoryProfile_Delete
GO
CREATE PROCEDURE [dbo].[USP_CategoryProfile_Delete]
(
	@ID NUMERIC(18,0),
	@numDomainID NUMERIC(18,0)
)
AS
BEGIN
	IF EXISTS (SELECT numItemID FROM ItemCategory WHERE numCategoryID IN (SELECT numCategoryID FROM Category WHERE numDomainID=@numDomainID AND numCategoryProfileID=@ID))
	BEGIN
		RAISERROR('ITEMS_EXITS',16,1)
		RETURN
	END
	ELSE 
	BEGIN
		DELETE FROM SiteCategories WHERE numCategoryID IN (SELECT ISNULL(numCategoryID,0) FROM Category WHERE numCategoryProfileID = @ID)
		DELETE FROM Category WHERE numCategoryProfileID = @ID
		DELETE FROM CategoryProfileSites WHERE numCategoryProfileID = @ID
		DELETE FROM CategoryProfile WHERE ID=@ID
	END
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CategoryProfile_GetAll')
DROP PROCEDURE dbo.USP_CategoryProfile_GetAll
GO
CREATE PROCEDURE [dbo].[USP_CategoryProfile_GetAll]
	@numDomainID NUMERIC(18,0)
AS
BEGIN
	SELECT ID,vcName FROM CategoryProfile WHERE numDomainID=@numDomainID
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CategoryProfile_GetWithSites')
DROP PROCEDURE dbo.USP_CategoryProfile_GetWithSites
GO
CREATE PROCEDURE [dbo].[USP_CategoryProfile_GetWithSites]
(
	@ID NUMERIC(18,0),
	@numDomainID NUMERIC(18,0)
)
AS
BEGIN
	SELECT ID,vcName FROM CategoryProfile WHERE ID=@ID

	SELECT 
		CategoryProfileSites.* ,
		Sites.vcSiteName
	FROM 
		CategoryProfileSites 
	INNER JOIN
		Sites
	ON
		CategoryProfileSites.numSiteID = Sites.numSiteID
	WHERE 
		numCategoryProfileID=@ID
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CategoryProfile_Save')
DROP PROCEDURE dbo.USP_CategoryProfile_Save
GO
CREATE PROCEDURE [dbo].[USP_CategoryProfile_Save]
	@ID NUMERIC(18,0),
	@numDomainID NUMERIC(18,0),
	@numUserCntID NUMERIC(18,0),
	@vcName VARCHAR(200),
	@vcSiteIDs VARCHAR(500)
AS
BEGIN

BEGIN TRY
BEGIN TRANSACTION
	IF @ID = 0 --INSERT 
	BEGIN
		INSERT INTO CategoryProfile 
		(
			vcName,
			numDomainID,
			numCreatedBy,
			dtCreated
		)
		VALUES
		(
			@vcName,
			@numDomainID,
			@numUserCntID,
			GETUTCDATE()
		)

		SELECT @ID = SCOPE_IDENTITY()
	END
	ELSE --UPDATE
	BEGIN
		UPDATE
			CategoryProfile
		SET
			vcName=@vcName,
			numModifiedBy=@numUserCntID,
			dtModified=GETUTCDATE()
		WHERE
			ID=@ID
	END

	IF LEN(@vcSiteIDs) = 0
	BEGIN
		-- REMOVE SITE CATEGORY ASSIGNEDMENT AS SITES DOES NOT BELONG TO THIS CATEGORY PROFILE
		DELETE FROM SiteCategories WHERE numSiteID IN (SELECT ISNULL(numSiteID,0) FROM CategoryProfileSites WHERE numCategoryProfileID=@ID)

		-- REMOVE SITES ASSIGNED TO CATEGORY PROFILE
		DELETE FROM CategoryProfileSites WHERE numCategoryProfileID=@ID
	END
	ELSE
	BEGIN
		DECLARE @TEMP TABLE
		(
			ID NUMERIC(18,0)
		)

		INSERT INTO @TEMP (ID) SELECT Id FROM dbo.SplitIDs(@vcSiteIDs,',')

		--DELETE SITE CATEGORY CONFIGURATION AS THEY BELONGS TO NEW CATEGORY PROFILE NOW
		DELETE FROM SiteCategories WHERE numSiteID IN (SELECT ISNULL(ID,0) FROM @TEMP)
		--DELETE SITES WHICH ARE ASSIGNED TO OTHER CATEGORY AS THEY BELONGS TO NEW CATEGORY PROFILE NOW
		DELETE FROM CategoryProfileSites WHERE numCategoryProfileID <> @ID AND numSiteID IN (SELECT ISNULL(ID,0) FROM @TEMP)


		--DELETE SITE CATEGORY CONFIGURATION FOR SITES AS THEY DOES NOT BELONGS TO THIS CATEGORY PROFILE
		DELETE FROM SiteCategories WHERE numSiteID IN (SELECT ISNULL(numSiteID,0) FROM CategoryProfileSites WHERE numCategoryProfileID=@ID AND numSiteID NOT IN (SELECT ISNULL(ID,0) FROM @TEMP))
		-- DELETE SITES WHICH ARE REMOVED FROM CATEGORY PROFILE
		DELETE FROM CategoryProfileSites WHERE numCategoryProfileID=@ID AND numSiteID NOT IN (SELECT ISNULL(ID,0) FROM @TEMP)

		-- INSERT NEW SELECTED SITES
		INSERT INTO CategoryProfileSites
		(
			numCategoryProfileID,
			numSiteID
		)
		SELECT
			@ID,
			ID
		FROM 
			@TEMP
		WHERE
			ID NOT IN (SELECT numSiteID FROM CategoryProfileSites WHERE numCategoryProfileID=@ID)

		INSERT INTO SiteCategories
		(
			numSiteID,
			numCategoryID
		)
		SELECT 
			CategoryProfileSites.numSiteID,
			Category.numCategoryID
		FROm 
			CategoryProfileSites 
		JOIN
			Category
		ON
			CategoryProfileSites.numCategoryProfileID = Category.numCategoryProfileID
		WHERE 
			CategoryProfileSites.numCategoryProfileID=@ID
		ORDER BY
			CategoryProfileSites.numSiteID
	END
COMMIT
END TRY
BEGIN CATCH
DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
END
--Created by anoop jayaraj                                                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_checkuseratlogin')
DROP PROCEDURE usp_checkuseratlogin
GO
CREATE PROCEDURE [dbo].[USP_CheckUserAtLogin]                                                              
(                                                                                    
@UserName as varchar(100)='',                                                                        
@vcPassword as varchar(100)='',
@vcLinkedinId as varchar(300)=''                                                                           
)                                                              
as                                                              

DECLARE @listIds VARCHAR(MAX)
IF(@vcLinkedinId='N')
begin
SELECT 
	@listIds = COALESCE(@listIds+',' ,'') + CAST(UPA.numUserID AS VARCHAR(100)) 
FROM 
	UserMaster U                              
Join 
	Domain D                              
on 
	D.numDomainID=U.numDomainID
Join  
	Subscribers S                            
on 
	S.numTargetDomainID=D.numDomainID
join
	UnitPriceApprover UPA
ON
	D.numDomainID = UPA.numDomainID
WHERE 
	vcEmailID=@UserName and vcPassword=@vcPassword

/*check subscription validity */
IF EXISTS( SELECT *
FROM UserMaster U                              
 Join Domain D                              
 on D.numDomainID=U.numDomainID
 Join  Subscribers S                            
 on S.numTargetDomainID=D.numDomainID
 WHERE vcEmailID=@UserName and vcPassword=@vcPassword and bitActive=1 AND getutcdate() > dtSubEndDate)
BEGIN
	RAISERROR('SUB_RENEW',16,1)
	RETURN;
END



                                                                
 SELECT top 1 U.numUserID,numUserDetailId,isnull(vcEmailID,'') vcEmailID,isnull(numGroupID,0) numGroupID,bitActivateFlag,                              
 isnull(vcMailNickName,'') vcMailNickName,isnull(txtSignature,'') txtSignature,isnull(U.numDomainID,0) numDomainID,                              
 isnull(Div.numDivisionID,0) numDivisionID,isnull(vcCompanyName,'') vcCompanyName,isnull(E.bitExchangeIntegration,0) bitExchangeIntegration,                              
 isnull(E.bitAccessExchange,0) bitAccessExchange,isnull(E.vcExchPath,'') vcExchPath,isnull(E.vcExchDomain,'') vcExchDomain,                              
 isnull(D.vcExchUserName,'') vcExchUserName ,isnull(vcFirstname,'')+' '+isnull(vcLastName,'') as ContactName,                              
 Case when E.bitAccessExchange=0 then  E.vcExchPassword when E.bitAccessExchange=1 then D.vcExchPassword end as vcExchPassword,                          
 tintCustomPagingRows,                         
 vcDateFormat,                         
 numDefCountry,                     
 tintComposeWindow,
 dateadd(day,-sintStartDate,getutcdate()) as StartDate,                        
 dateadd(day,sintNoofDaysInterval,dateadd(day,-sintStartDate,getutcdate())) as EndDate,                        
 tintAssignToCriteria,                         
 bitIntmedPage,                         
 tintFiscalStartMonth,numAdminID,isnull(A.numTeam,0) numTeam ,                
 isnull(D.vcCurrency,'') as vcCurrency, 
 isnull(D.numCurrencyID,0) as numCurrencyID, 
 isnull(D.bitMultiCurrency,0) as bitMultiCurrency,              
 bitTrial,isnull(tintChrForComSearch,0) as tintChrForComSearch,  
isnull(tintChrForItemSearch,0) as tintChrForItemSearch,              
 case when bitSMTPServer= 1 then vcSMTPServer else '' end as vcSMTPServer  ,          
 case when bitSMTPServer= 1 then  isnull(numSMTPPort,0)  else 0 end as numSMTPPort      
,isnull(bitSMTPAuth,0) bitSMTPAuth    
,isnull([vcSmtpPassword],'') vcSmtpPassword    
,isnull(bitSMTPSSL,0) bitSMTPSSL   ,isnull(bitSMTPServer,0) bitSMTPServer,       
isnull(IM.bitImap,0) bitImapIntegration, isnull(charUnitSystem,'E')   UnitSystem, datediff(day,getutcdate(),dtSubEndDate) as NoOfDaysLeft,
ISNULL(bitCreateInvoice,0) bitCreateInvoice,
ISNULL([numDefaultSalesBizDocId],0) numDefaultSalesBizDocId,
ISNULL([numDefaultPurchaseBizDocId],0) numDefaultPurchaseBizDocId,
vcDomainName,
S.numSubscriberID,
D.[tintBaseTax],
ISNULL(D.[numShipCompany],0) numShipCompany,
ISNULL(D.[bitEmbeddedCost],0) bitEmbeddedCost,
(Select isnull(numAuthoritativeSales,0) From AuthoritativeBizDocs Where numDomainId=D.numDomainID) numAuthoritativeSales,
(Select isnull(numAuthoritativePurchase,0) From AuthoritativeBizDocs Where numDomainId=D.numDomainID) numAuthoritativePurchase,
ISNULL(D.[numDefaultSalesOppBizDocId],0) numDefaultSalesOppBizDocId,
ISNULL(tintDecimalPoints,2) tintDecimalPoints,
ISNULL(D.tintBillToForPO,0) tintBillToForPO, 
ISNULL(D.tintShipToForPO,0) tintShipToForPO,
ISNULL(D.tintOrganizationSearchCriteria,0) tintOrganizationSearchCriteria,
ISNULL(D.tintLogin,0) tintLogin,
ISNULL(bitDocumentRepositary,0) bitDocumentRepositary,
ISNULL(D.vcPortalName,'') vcPortalName,
(SELECT COUNT(*) FROM dbo.Subscribers WHERE getutcdate() between dtSubStartDate and dtSubEndDate and bitActive=1),
ISNULL(D.bitGtoBContact,0) bitGtoBContact,
ISNULL(D.bitBtoGContact,0) bitBtoGContact,
ISNULL(D.bitGtoBCalendar,0) bitGtoBCalendar,
ISNULL(D.bitBtoGCalendar,0) bitBtoGCalendar,
ISNULL(bitExpenseNonInventoryItem,0) bitExpenseNonInventoryItem,
ISNULL(D.bitInlineEdit,0) bitInlineEdit,
ISNULL(U.numDefaultClass,0) numDefaultClass,
ISNULL(D.bitRemoveVendorPOValidation,0) bitRemoveVendorPOValidation,
ISNULL(D.bitTrakDirtyForm,1) bitTrakDirtyForm,
isnull(D.tintBaseTaxOnArea,0) tintBaseTaxOnArea,
ISNULL(D.bitAmountPastDue,0) bitAmountPastDue,
CONVERT(DECIMAL(18,2),ISNULL(D.monAmountPastDue,0)) monAmountPastDue,
ISNULL(D.bitSearchOrderCustomerHistory,0) bitSearchOrderCustomerHistory,
isnull(U.tintTabEvent,0) as tintTabEvent,
isnull(D.bitRentalItem,0) as bitRentalItem,
isnull(D.numRentalItemClass,0) as numRentalItemClass,
isnull(D.numRentalHourlyUOM,0) as numRentalHourlyUOM,
isnull(D.numRentalDailyUOM,0) as numRentalDailyUOM,
isnull(D.tintRentalPriceBasedOn,0) as tintRentalPriceBasedOn,
isnull(D.tintCalendarTimeFormat,24) as tintCalendarTimeFormat,
isnull(D.tintDefaultClassType,0) as tintDefaultClassType,
ISNULL(U.numDefaultWarehouse,0) numDefaultWarehouse,
ISNULL(D.tintPriceBookDiscount,0) tintPriceBookDiscount,
ISNULL(D.bitAutoSerialNoAssign,0) bitAutoSerialNoAssign,
ISNULL(IsEnableProjectTracking,0) AS [IsEnableProjectTracking],
ISNULL(IsEnableCampaignTracking,0) AS [IsEnableCampaignTracking],
ISNULL(IsEnableResourceScheduling,0) AS [IsEnableResourceScheduling],
ISNULL(D.numSOBizDocStatus,0) AS numSOBizDocStatus,
ISNULL(D.bitAutolinkUnappliedPayment,0) AS bitAutolinkUnappliedPayment,
ISNULL(numShippingServiceItemID,0) AS [numShippingServiceItemID],
ISNULL(D.numDiscountServiceItemID,0) AS numDiscountServiceItemID,
ISNULL(D.tintCommissionType,1) AS tintCommissionType,
ISNULL(D.bitDiscountOnUnitPrice,0) AS bitDiscountOnUnitPrice,
ISNULL(D.intShippingImageWidth,0) AS intShippingImageWidth ,
ISNULL(D.intShippingImageHeight,0) AS intShippingImageHeight,
ISNULL(D.numTotalInsuredValue,0) AS numTotalInsuredValue,
ISNULL(D.numTotalCustomsValue,0) AS numTotalCustomsValue,
ISNULL(D.bitUseBizdocAmount,1) AS [bitUseBizdocAmount],
ISNULL(D.bitDefaultRateType,1) AS [bitDefaultRateType],
ISNULL(D.bitIncludeTaxAndShippingInCommission,1) AS [bitIncludeTaxAndShippingInCommission],
ISNULL(D.[bitLandedCost],0) AS bitLandedCost,
ISNULL(D.vcLanedCostDefault,'') AS vcLanedCostDefault,
ISNULL(D.bitMinUnitPriceRule,0) AS bitMinUnitPriceRule,
ISNULL(D.numAbovePercent,0) AS numAbovePercent,
ISNULL(D.numAbovePriceField,0) AS numAbovePriceField,
ISNULL(D.numBelowPercent,0) AS numBelowPercent,
ISNULL(D.numBelowPriceField,0) AS numBelowPriceField,
ISNULL(D.numOrderStatusBeforeApproval,0) AS numOrderStatusBeforeApproval,
ISNULL(D.numOrderStatusAfterApproval,0) AS numOrderStatusAfterApproval,
ISNULL(@listIds,'') AS vcUnitPriceApprover,
ISNULL(D.numPODropShipBizDoc,0) AS numPODropShipBizDoc,
ISNULL(D.numPODropShipBizDocTemplate,0) AS numPODropShipBizDocTemplate,
ISNULL(D.IsEnableDeferredIncome,0) AS IsEnableDeferredIncome,
ISNULL(D.bitEnableItemLevelUOM,0) AS bitEnableItemLevelUOM,
ISNULL(D.bitPurchaseTaxCredit,0) AS bitPurchaseTaxCredit
/*,ISNULL(D.bitAllowPPVariance,0) AS bitAllowPPVariance*/
 FROM UserMaster U                              
 left join ExchangeUserDetails E                              
 on E.numUserID=U.numUserID                              
left join ImapUserDetails IM                              
 on IM.numUserCntID=U.numUserDetailID          
 Join Domain D                              
 on D.numDomainID=U.numDomainID                                           
 left join DivisionMaster Div                                            
 on D.numDivisionID=Div.numDivisionID                               
 left join CompanyInfo C                              
 on C.numCompanyID=Div.numDivisionID                               
 left join AdditionalContactsInformation A                              
 on  A.numContactID=U.numUserDetailId                            
 Join  Subscribers S                            
 on S.numTargetDomainID=D.numDomainID                             
 WHERE vcEmailID=@UserName and vcPassword=@vcPassword and bitActive IN (1,2) AND getutcdate() between dtSubStartDate and dtSubEndDate
             
select numTerritoryId from UserTerritory UT                              
join UserMaster U                              
on numUserDetailId =UT.numUserCntID                                         
where vcEmailID=@UserName and vcPassword=@vcPassword
end
else
begin
	SELECT 
	@listIds = COALESCE(@listIds+',' ,'') + CAST(UPA.numUserID AS VARCHAR(100)) 
FROM 
	UserMaster U                              
Join 
	Domain D                              
on 
	D.numDomainID=U.numDomainID
Join  
	Subscribers S                            
on 
	S.numTargetDomainID=D.numDomainID
join
	UnitPriceApprover UPA
ON
	D.numDomainID = UPA.numDomainID
WHERE 
	vcLinkedinId=@vcLinkedinId

/*check subscription validity */
IF EXISTS( SELECT *
FROM UserMaster U                              
 Join Domain D                              
 on D.numDomainID=U.numDomainID
 Join  Subscribers S                            
 on S.numTargetDomainID=D.numDomainID
 WHERE vcLinkedinId=@vcLinkedinId and bitActive=1 AND getutcdate() > dtSubEndDate)
BEGIN
	RAISERROR('SUB_RENEW',16,1)
	RETURN;
END



                                                                
 SELECT top 1 U.numUserID,numUserDetailId,isnull(vcEmailID,'') vcEmailID,isnull(numGroupID,0) numGroupID,bitActivateFlag,                              
 isnull(vcMailNickName,'') vcMailNickName,isnull(txtSignature,'') txtSignature,isnull(U.numDomainID,0) numDomainID,                              
 isnull(Div.numDivisionID,0) numDivisionID,isnull(vcCompanyName,'') vcCompanyName,isnull(E.bitExchangeIntegration,0) bitExchangeIntegration,                              
 isnull(E.bitAccessExchange,0) bitAccessExchange,isnull(E.vcExchPath,'') vcExchPath,isnull(E.vcExchDomain,'') vcExchDomain,                              
 isnull(D.vcExchUserName,'') vcExchUserName ,isnull(vcFirstname,'')+' '+isnull(vcLastName,'') as ContactName,                              
 Case when E.bitAccessExchange=0 then  E.vcExchPassword when E.bitAccessExchange=1 then D.vcExchPassword end as vcExchPassword,                          
 tintCustomPagingRows,                         
 vcDateFormat,                         
 numDefCountry,                     
 tintComposeWindow,
 dateadd(day,-sintStartDate,getutcdate()) as StartDate,                        
 dateadd(day,sintNoofDaysInterval,dateadd(day,-sintStartDate,getutcdate())) as EndDate,                        
 tintAssignToCriteria,                         
 bitIntmedPage,                         
 tintFiscalStartMonth,numAdminID,isnull(A.numTeam,0) numTeam ,                
 isnull(D.vcCurrency,'') as vcCurrency, 
 isnull(D.numCurrencyID,0) as numCurrencyID, 
 isnull(D.bitMultiCurrency,0) as bitMultiCurrency,              
 bitTrial,isnull(tintChrForComSearch,0) as tintChrForComSearch,  
isnull(tintChrForItemSearch,0) as tintChrForItemSearch,              
 case when bitSMTPServer= 1 then vcSMTPServer else '' end as vcSMTPServer  ,          
 case when bitSMTPServer= 1 then  isnull(numSMTPPort,0)  else 0 end as numSMTPPort      
,isnull(bitSMTPAuth,0) bitSMTPAuth    
,isnull([vcSmtpPassword],'') vcSmtpPassword    
,isnull(bitSMTPSSL,0) bitSMTPSSL   ,isnull(bitSMTPServer,0) bitSMTPServer,       
isnull(IM.bitImap,0) bitImapIntegration, isnull(charUnitSystem,'E')   UnitSystem, datediff(day,getutcdate(),dtSubEndDate) as NoOfDaysLeft,
ISNULL(bitCreateInvoice,0) bitCreateInvoice,
ISNULL([numDefaultSalesBizDocId],0) numDefaultSalesBizDocId,
ISNULL([numDefaultPurchaseBizDocId],0) numDefaultPurchaseBizDocId,
vcDomainName,
S.numSubscriberID,
D.[tintBaseTax],
ISNULL(D.[numShipCompany],0) numShipCompany,
ISNULL(D.[bitEmbeddedCost],0) bitEmbeddedCost,
(Select isnull(numAuthoritativeSales,0) From AuthoritativeBizDocs Where numDomainId=D.numDomainID) numAuthoritativeSales,
(Select isnull(numAuthoritativePurchase,0) From AuthoritativeBizDocs Where numDomainId=D.numDomainID) numAuthoritativePurchase,
ISNULL(D.[numDefaultSalesOppBizDocId],0) numDefaultSalesOppBizDocId,
ISNULL(tintDecimalPoints,2) tintDecimalPoints,
ISNULL(D.tintBillToForPO,0) tintBillToForPO, 
ISNULL(D.tintShipToForPO,0) tintShipToForPO,
ISNULL(D.tintOrganizationSearchCriteria,0) tintOrganizationSearchCriteria,
ISNULL(D.tintLogin,0) tintLogin,
ISNULL(bitDocumentRepositary,0) bitDocumentRepositary,
ISNULL(D.vcPortalName,'') vcPortalName,
(SELECT COUNT(*) FROM dbo.Subscribers WHERE getutcdate() between dtSubStartDate and dtSubEndDate and bitActive=1),
ISNULL(D.bitGtoBContact,0) bitGtoBContact,
ISNULL(D.bitBtoGContact,0) bitBtoGContact,
ISNULL(D.bitGtoBCalendar,0) bitGtoBCalendar,
ISNULL(D.bitBtoGCalendar,0) bitBtoGCalendar,
ISNULL(bitExpenseNonInventoryItem,0) bitExpenseNonInventoryItem,
ISNULL(D.bitInlineEdit,0) bitInlineEdit,
ISNULL(U.numDefaultClass,0) numDefaultClass,
ISNULL(D.bitRemoveVendorPOValidation,0) bitRemoveVendorPOValidation,
ISNULL(D.bitTrakDirtyForm,1) bitTrakDirtyForm,
isnull(D.tintBaseTaxOnArea,0) tintBaseTaxOnArea,
ISNULL(D.bitAmountPastDue,0) bitAmountPastDue,
CONVERT(DECIMAL(18,2),ISNULL(D.monAmountPastDue,0)) monAmountPastDue,
ISNULL(D.bitSearchOrderCustomerHistory,0) bitSearchOrderCustomerHistory,
isnull(U.tintTabEvent,0) as tintTabEvent,
isnull(D.bitRentalItem,0) as bitRentalItem,
isnull(D.numRentalItemClass,0) as numRentalItemClass,
isnull(D.numRentalHourlyUOM,0) as numRentalHourlyUOM,
isnull(D.numRentalDailyUOM,0) as numRentalDailyUOM,
isnull(D.tintRentalPriceBasedOn,0) as tintRentalPriceBasedOn,
isnull(D.tintCalendarTimeFormat,24) as tintCalendarTimeFormat,
isnull(D.tintDefaultClassType,0) as tintDefaultClassType,
ISNULL(U.numDefaultWarehouse,0) numDefaultWarehouse,
ISNULL(D.tintPriceBookDiscount,0) tintPriceBookDiscount,
ISNULL(D.bitAutoSerialNoAssign,0) bitAutoSerialNoAssign,
ISNULL(IsEnableProjectTracking,0) AS [IsEnableProjectTracking],
ISNULL(IsEnableCampaignTracking,0) AS [IsEnableCampaignTracking],
ISNULL(IsEnableResourceScheduling,0) AS [IsEnableResourceScheduling],
ISNULL(D.numSOBizDocStatus,0) AS numSOBizDocStatus,
ISNULL(D.bitAutolinkUnappliedPayment,0) AS bitAutolinkUnappliedPayment,
ISNULL(numShippingServiceItemID,0) AS [numShippingServiceItemID],
ISNULL(D.numDiscountServiceItemID,0) AS numDiscountServiceItemID,
ISNULL(D.tintCommissionType,1) AS tintCommissionType,
ISNULL(D.bitDiscountOnUnitPrice,0) AS bitDiscountOnUnitPrice,
ISNULL(D.intShippingImageWidth,0) AS intShippingImageWidth ,
ISNULL(D.intShippingImageHeight,0) AS intShippingImageHeight,
ISNULL(D.numTotalInsuredValue,0) AS numTotalInsuredValue,
ISNULL(D.numTotalCustomsValue,0) AS numTotalCustomsValue,
ISNULL(D.bitUseBizdocAmount,1) AS [bitUseBizdocAmount],
ISNULL(D.bitDefaultRateType,1) AS [bitDefaultRateType],
ISNULL(D.bitIncludeTaxAndShippingInCommission,1) AS [bitIncludeTaxAndShippingInCommission],
ISNULL(D.[bitLandedCost],0) AS bitLandedCost,
ISNULL(D.vcLanedCostDefault,'') AS vcLanedCostDefault,
ISNULL(D.bitMinUnitPriceRule,0) AS bitMinUnitPriceRule,
ISNULL(D.numAbovePercent,0) AS numAbovePercent,
ISNULL(D.numAbovePriceField,0) AS numAbovePriceField,
ISNULL(D.numBelowPercent,0) AS numBelowPercent,
ISNULL(D.numBelowPriceField,0) AS numBelowPriceField,
ISNULL(D.numOrderStatusBeforeApproval,0) AS numOrderStatusBeforeApproval,
ISNULL(D.numOrderStatusAfterApproval,0) AS numOrderStatusAfterApproval,
ISNULL(@listIds,'') AS vcUnitPriceApprover,
ISNULL(D.numPODropShipBizDoc,0) AS numPODropShipBizDoc,
ISNULL(D.numPODropShipBizDocTemplate,0) AS numPODropShipBizDocTemplate,
ISNULL(D.IsEnableDeferredIncome,0) AS IsEnableDeferredIncome,
ISNULL(D.bitEnableItemLevelUOM,0) AS bitEnableItemLevelUOM
/*,ISNULL(D.bitAllowPPVariance,0) AS bitAllowPPVariance*/
 FROM UserMaster U                              
 left join ExchangeUserDetails E                              
 on E.numUserID=U.numUserID                              
left join ImapUserDetails IM                              
 on IM.numUserCntID=U.numUserDetailID          
 Join Domain D                              
 on D.numDomainID=U.numDomainID                                           
 left join DivisionMaster Div                                            
 on D.numDivisionID=Div.numDivisionID                               
 left join CompanyInfo C                              
 on C.numCompanyID=Div.numDivisionID                               
 left join AdditionalContactsInformation A                              
 on  A.numContactID=U.numUserDetailId                            
 Join  Subscribers S                            
 on S.numTargetDomainID=D.numDomainID                             
 WHERE U.vcLinkedinId=@vcLinkedinId and bitActive IN (1,2) AND getutcdate() between dtSubStartDate and dtSubEndDate
             
select numTerritoryId from UserTerritory UT                              
join UserMaster U                              
on numUserDetailId =UT.numUserCntID                                         
where vcLinkedinId=@vcLinkedinId
end
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_CompanyInfo_Search' ) 
    DROP PROCEDURE USP_CompanyInfo_Search
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Sandeep Patel
-- Create date: 15 April 2014
-- Description:	Gets company info based on search fileds configured
-- =============================================
CREATE PROCEDURE USP_CompanyInfo_Search
	@numDomainID NUMERIC(18,0),
	@numUserCntID NUMERIC(18,0),
	@isStartWithSearch BIT = 0,
	@searchText VARCHAR(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

SELECT @searchText = REPLACE(@searchText,'''', '''''')

DECLARE @CustomerSelectFields AS VARCHAR(MAX)
DECLARE @CustomerSelectCustomFields AS VARCHAR(MAX)
DECLARE @ProspectsRights AS TINYINT                    
DECLARE @accountsRights AS TINYINT                   
DECLARE @leadsRights AS TINYINT 

SET @CustomerSelectFields = ''
SET @CustomerSelectCustomFields = ''
               
SET @ProspectsRights = ( SELECT TOP 1
                                MAX(GA.intViewAllowed) AS intViewAllowed
                         FROM   UserMaster UM ,
                                GroupAuthorization GA ,
                                PageMaster PM
                         WHERE  GA.numGroupID = UM.numGroupID
                                AND PM.numPageID = GA.numPageID
                                AND PM.numModuleID = GA.numModuleID
                                AND PM.numpageID = 2
                                AND PM.numModuleID = 3
                                AND UM.numUserID = ( SELECT numUserID
                                                     FROM   userMaster
                                                     WHERE  numUserDetailId = @numUserCntID
                                                   )
                         GROUP BY UM.numUserId ,
                                UM.vcUserName ,
                                PM.vcFileName ,
                                GA.numModuleID ,
                                GA.numPageID
                       )                        
                    
SET @accountsRights = ( SELECT TOP 1
                                MAX(GA.intViewAllowed) AS intViewAllowed
                        FROM    UserMaster UM ,
                                GroupAuthorization GA ,
                                PageMaster PM
                        WHERE   GA.numGroupID = UM.numGroupID
                                AND PM.numPageID = GA.numPageID
                                AND PM.numModuleID = GA.numModuleID
                                AND PM.numpageID = 2
                                AND PM.numModuleID = 4
                                AND UM.numUserID = ( SELECT numUserID
                                                     FROM   userMaster
                                                     WHERE  numUserDetailId = @numUserCntID
                                                   )
                        GROUP BY UM.numUserId ,
                                UM.vcUserName ,
                                PM.vcFileName ,
                                GA.numModuleID ,
                                GA.numPageID
                      )                
              
SET @leadsRights = ( SELECT TOP 1
                            MAX(GA.intViewAllowed) AS intViewAllowed
                     FROM   UserMaster UM ,
                            GroupAuthorization GA ,
                            PageMaster PM
                     WHERE  GA.numGroupID = UM.numGroupID
                            AND PM.numPageID = GA.numPageID
                            AND PM.numModuleID = GA.numModuleID
                            AND PM.numpageID = 2
                            AND PM.numModuleID = 2
                            AND UM.numUserID = ( SELECT numUserID
                                                 FROM   userMaster
                                                 WHERE  numUserDetailId = @numUserCntID
                                               )
                     GROUP BY UM.numUserId ,
                            UM.vcUserName ,
                            PM.vcFileName ,
                            GA.numModuleID ,
                            GA.numPageID
                   )                   
                    
-- START: Get organization display fields
SELECT * INTO #tempOrgDisplayFields FROM
(
	SELECT 
		numFieldId,
		vcDbColumnName,
		ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName,
		tintRow AS tintOrder,
		0 as Custom
    FROM 
		View_DynamicColumns
	WHERE 
		numFormId=96 AND numUserCntID=@numUserCntID AND numDomainID=@numDomainID AND 
		tintPageType=1 AND bitCustom=0  AND ISNULL(bitSettingField,0)=1 AND numRelCntType=0
	UNION   
	SELECT 
		numFieldId,
		vcFieldName,
		vcFieldName,
		tintRow AS tintOrder,
		1 as Custom
    FROM 
		View_DynamicCustomColumns   
	WHERE 
		Grp_id=1 AND numFormId=96 AND numUserCntID=@numUserCntID AND numDomainID=@numDomainID AND 
		tintPageType=1 AND bitCustom=1 AND numRelCntType=0
)X

IF EXISTS (SELECT * FROM #tempOrgDisplayFields)
BEGIN

	SELECT 
		@CustomerSelectFields = COALESCE (@CustomerSelectFields,'') + CAST(vcDbColumnName AS VARCHAR(100)) + ','	
	FROM 
		#tempOrgDisplayFields 
	WHERE
		Custom = 0
		
	-- Removes last , from string
	IF DATALENGTH(@CustomerSelectFields) > 0
		SET @CustomerSelectFields = LEFT(@CustomerSelectFields, LEN(@CustomerSelectFields) - 1)

	IF EXISTS(SELECT * FROM #tempOrgDisplayFields WHERE Custom = 1)
	BEGIN
		SELECT 
			@CustomerSelectCustomFields = COALESCE (@CustomerSelectCustomFields,'') + 'dbo.fn_GetCustFldOrganization(' + CAST(numFieldId AS VARCHAR(100)) + ',1,TEMP1.numCompanyID) AS [' + CAST (vcDbColumnName AS VARCHAR (100)) + '],'	
		FROM 
			#tempOrgDisplayFields 
		WHERE
			Custom = 1
	END

	-- Removes last , from string
	IF DATALENGTH(@CustomerSelectCustomFields) > 0
		SET @CustomerSelectCustomFields = LEFT(@CustomerSelectCustomFields, LEN(@CustomerSelectCustomFields) - 1)
END

-- END: Get organization display fields

-- START: Generate default select statement 
                    
DECLARE @strSQL AS VARCHAR(MAX)    

SET @strSQL = 'SELECT numDivisionID, numCompanyID, vcCompanyName' + 
	CASE @CustomerSelectFields
	WHEN '' THEN ''
	ELSE ',' + @CustomerSelectFields
	END
	 + 
	CASE @CustomerSelectCustomFields
	WHEN '' THEN ''
	ELSE ',' + @CustomerSelectCustomFields
	END
	 + ' FROM (SELECT 
	CMP.numDomainID,
	CMP.numCompanyID, 
	CMP.vcCompanyName, 
	DM.numDivisionID, 
	DM.vcDivisionName, 
	ISNULL((SELECT vcData FROM dbo.ListDetails WHERE numListID = 1 AND numDomainID = DM.numDomainID AND numListItemID = DM.numStatusID),'''') as numStatusID, 
	ISNULL((SELECT vcData FROM dbo.ListDetails WHERE numListID = 2 AND numDomainID = DM.numDomainID AND numListItemID = CMP.numCompanyRating),'''') as numCompanyRating, 
	ISNULL((SELECT vcData FROM dbo.ListDetails WHERE numListID = 3 AND numDomainID = DM.numDomainID AND numListItemID = CMP.numCompanyCredit),'''') as numCompanyCredit,
	ISNULL((SELECT vcData FROM dbo.ListDetails WHERE numListID = 4 AND numDomainID = DM.numDomainID AND numListItemID = CMP.numCompanyIndustry),'''') as numCompanyIndustry,
	ISNULL((SELECT vcData FROM dbo.ListDetails WHERE numListID = 5 AND (numDomainID = DM.numDomainID OR constFlag = 1) AND numListItemID = CMP.numCompanyType),'''') as numCompanyType,
	ISNULL((SELECT vcData FROM dbo.ListDetails WHERE numListID = 6 AND numDomainID = DM.numDomainID AND numListItemID = CMP.numAnnualRevID),'''') as numAnnualRevID,
	ISNULL((SELECT vcData FROM dbo.ListDetails WHERE numListID = 7 AND numDomainID = DM.numDomainID AND numListItemID = CMP.numNoOfEmployeesID),'''') as numNoOfEmployeesID,
	vcComPhone,
	vcComFax,
	ISNULL((SELECT vcData FROM dbo.ListDetails WHERE numListID = 18 AND numDomainID = DM.numDomainID AND numListItemID = CMP.vcHow),'''') as vcHow,
	ISNULL((SELECT vcData FROM dbo.ListDetails WHERE numListID = 21 AND numDomainID = DM.numDomainID AND numListItemID = CMP.vcProfile),'''') as vcProfile,
	ISNULL((SELECT vcData FROM dbo.ListDetails WHERE numListID = 438 AND numDomainID = DM.numDomainID AND numListItemID = DM.numCompanyDiff),'''') as numCompanyDiff,
	DM.vcCompanyDiff,
	isnull(CMP.txtComments,'''') as txtComments, 
	isnull(CMP.vcWebSite,'''') vcWebSite, 
	isnull(AD1.vcStreet,'''') as vcBillStreet, 
	isnull(AD1.vcCity,'''') as vcBillCity, 
	isnull(dbo.fn_GetState(AD1.numState),'''') as numBillState,
	isnull(AD1.vcPostalCode,'''') as vcBillPostCode,
	isnull(dbo.fn_GetListName(AD1.numCountry,0),'''') as numBillCountry,
	isnull(AD2.vcStreet,'''') as vcShipStreet,
	isnull(AD2.vcCity,'''') as vcShipCity,
	isnull(dbo.fn_GetState(AD2.numState),'''')  as numShipState,
	isnull(AD2.vcPostalCode,'''') as vcShipPostCode, 
	isnull(dbo.fn_GetListName(AD2.numCountry,0),'''') as numShipCountry,
	ISNULL((SELECT vcData FROM dbo.ListDetails WHERE numListID = 78 AND numDomainID = DM.numDomainID AND numListItemID = DM.numTerID),'''') as numTerID
FROM  
(
SELECT
	CMP.numCompanyId
FROM
	dbo.CompanyInfo CMP
INNER JOIN
	dbo.DivisionMaster d
ON
	CMP.numCompanyId = d.numCompanyID
INNER JOIN
	AdditionalContactsInformation AC
ON 
	AC.numDivisionID=d.numDivisionID AND ISNULL(AC.bitPrimaryContact,0)=1
WHERE 
	CMP.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' AND d.tintCRMType<>0 and d.tintCRMType=1' +
	CASE @ProspectsRights WHEN 0 THEN ' and d.numRecOwner=0 ' ELSE '' END +
    CASE @ProspectsRights WHEN 1 THEN ' and d.numRecOwner='+convert(varchar(15),@numUserCntID) ELSE '' END +                        
    CASE @ProspectsRights WHEN 2 THEN ' and (d.numTerID in (select numTerritoryID  from UserTerritory where numUserCntID='+convert(varchar(15),@numUserCntID)+') or d.numTerID=0)' ELSE '' END +
'
UNION
SELECT
	CMP.numCompanyId
FROM
	dbo.CompanyInfo CMP
INNER JOIN
	dbo.DivisionMaster d
ON
	CMP.numCompanyId = d.numCompanyID
INNER JOIN
	AdditionalContactsInformation AC
ON 
	AC.numDivisionID=d.numDivisionID AND ISNULL(AC.bitPrimaryContact,0)=1
WHERE 
	CMP.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' AND d.tintCRMType<>0 and d.tintCRMType=2' +
	CASE @accountsRights WHEN 0 THEN ' and d.numRecOwner=0 ' ELSE '' END +
    CASE @accountsRights WHEN 1 THEN ' and d.numRecOwner='+convert(varchar(15),@numUserCntID) ELSE '' END +                        
    CASE @accountsRights WHEN 2 THEN ' and (d.numTerID in (select numTerritoryID  from UserTerritory where numUserCntID='+convert(varchar(15),@numUserCntID)+') or d.numTerID=0)' ELSE '' END +
'
UNION
SELECT
	CMP.numCompanyId
FROM
	dbo.CompanyInfo CMP
INNER JOIN
	dbo.DivisionMaster d
ON
	CMP.numCompanyId = d.numCompanyID
INNER JOIN
	AdditionalContactsInformation AC
ON 
	AC.numDivisionID=d.numDivisionID AND ISNULL(AC.bitPrimaryContact,0)=1
WHERE 
	CMP.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' AND d.tintCRMType=0' +
	CASE @leadsRights WHEN 0 THEN ' and d.numRecOwner=0 ' ELSE '' END +
    CASE @leadsRights WHEN 1 THEN ' and d.numRecOwner='+convert(varchar(15),@numUserCntID) ELSE '' END +                        
    CASE @leadsRights WHEN 2 THEN ' and (d.numTerID in (select numTerritoryID  from UserTerritory where numUserCntID='+convert(varchar(15),@numUserCntID)+') or d.numTerID=0)' ELSE '' END +
') AS TEMP2
INNER JOIN
	CompanyInfo CMP
ON
	TEMP2.numCompanyId =  CMP.numCompanyId                                                 
INNER JOIN 
	DivisionMaster DM    
ON 
	DM.numCompanyID=CMP.numCompanyID
LEFT JOIN 
	dbo.AddressDetails AD1 
ON 
    AD1.numAddressID =  (
							SELECT 
								MAX(numAddressID) 
							FROM 
								AddressDetails 
							WHERE 
								AddressDetails.numDomainID = DM.numDomainID AND 
								AddressDetails.numRecordID = DM.numDivisionID AND 
								AddressDetails.tintAddressOf = 2 AND 
								AddressDetails.tintAddressType = 1 AND 
								AddressDetails.bitIsPrimary = 1
						  )
LEFT JOIN 
	AddressDetails AD2 
ON 
    AD2.numAddressID = (
							SELECT 
								MAX(numAddressID) 
							FROM 
								AddressDetails 
							WHERE 
								AddressDetails.numDomainID = DM.numDomainID AND 
								AddressDetails.numRecordID = DM.numDivisionID AND 
								AddressDetails.tintAddressOf = 2 AND 
								AddressDetails.tintAddressType = 2 AND 
								AddressDetails.bitIsPrimary = 1
						)) AS TEMP1'
	
-- END: Generate default select statement 

                  
-- START: Generate user defined search fields condition

DECLARE @searchWhereCondition varchar(MAX)
DECLARE @singleValueSearchFields AS varchar(MAX)   
DECLARE @multiValueSearchFields AS varchar(MAX) 
DECLARE @customSearchFields AS varchar(MAX)    


SET @searchWhereCondition = ''
SET @singleValueSearchFields = ''   
SET @multiValueSearchFields = ''
SET @customSearchFields = ''
                

SELECT * INTO #tempOrgSearch FROM
(
	SELECT 
		numFieldId,
		vcDbColumnName,
		ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName,
		tintRow AS tintOrder,
		0 as Custom
    FROM 
		View_DynamicColumns
	WHERE 
		numFormId=97 AND numUserCntID=@numUserCntID AND numDomainID=@numDomainID AND 
		tintPageType=1 AND bitCustom=0  AND ISNULL(bitSettingField,0)=1 AND numRelCntType=0
	UNION   
	SELECT 
		numFieldId,
		vcFieldName,
		vcFieldName,
		tintRow AS tintOrder,
		1 as Custom
    FROM 
		View_DynamicCustomColumns   
	WHERE 
		Grp_id=1 AND numFormId=97 AND numUserCntID=@numUserCntID AND numDomainID=@numDomainID AND 
		tintPageType=1 AND bitCustom=1 AND numRelCntType=0
)Y

IF @searchText != ''
BEGIN
	IF (SELECT COUNT(*) FROM #tempOrgSearch) > 0 
	BEGIN
		-- START: Remove white spaces from search values
		DECLARE @finalSearchText VARCHAR(MAX)
		DECLARE @TempTable TABLE (vcValue VARCHAR(MAX))

		WHILE LEN(@searchText) > 0
		BEGIN
			SET @finalSearchText = LEFT(@searchText, 
									ISNULL(NULLIF(CHARINDEX(',', @searchText) - 1, -1),
									LEN(@searchText)))
			SET @searchText = SUBSTRING(@searchText,
										 ISNULL(NULLIF(CHARINDEX(',', @searchText), 0),
										 LEN(@searchText)) + 1, LEN(@searchText))

			INSERT INTO @TempTable (vcValue) VALUES ( @finalSearchText )
		END
		
		--Removes white spaces
		UPDATE
			@TempTable
		SET
			vcValue = LTRIM(RTRIM(vcValue))
	
		--Converting table to comma seperated string
		SELECT @searchText = COALESCE(@searchText,'') +  CONVERT(VARCHAR(MAX),vcValue) + ',' FROM @TempTable
	
		--Remove last comma from final search string
		IF DATALENGTH(@searchText) > 0
			SET @searchText = LEFT(@searchText, LEN(@searchText) - 1)
	
		-- END: Remove white spaces from search values
	
		--START: Gets search fields where there is one-one relation 
		SELECT 
			@singleValueSearchFields = COALESCE(@singleValueSearchFields,'') + '(' + CAST (vcDbColumnName AS VARCHAR (50)) + ' LIKE '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END + REPLACE(@searchText, ',', '%'' OR ' + vcDbColumnName + ' LIKE  ' + CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END) + '%'')' + ' OR '	
		FROM 
			#tempOrgSearch 
		WHERE 
			numFieldId NOT IN (51,52,96,148,127) AND
			Custom = 0
			
		
		-- Removes last OR from string
		IF DATALENGTH(@singleValueSearchFields) > 0
			SET @singleValueSearchFields = LEFT(@singleValueSearchFields, LEN(@singleValueSearchFields) - 3)
			
		--END: Gets search fields where there is one-one relation 
		
		
		--START: Gets search fields where there is one-many relation 
		-- 1. CONTACT FIRST NAME
		IF (SELECT COUNT(*) FROM #tempOrgSearch WHERE numFieldId = 51) > 0
			SET @multiValueSearchFields = '(SELECT COUNT(*) FROM AdditionalContactsInformation ACI WHERE ACI.numDivisionID=TEMP1.numDivisionID AND (ACI.vcFirstName LIKE '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END +  REPLACE(@searchText, ',', '%'' OR ACI.vcFirstName LIKE  '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END) + '%'')) > 0'	
		
		-- 2. CONTACT LAST NAME
		IF (SELECT COUNT(*) FROM #tempOrgSearch WHERE numFieldId = 52) > 0
		BEGIN
			IF @multiValueSearchFields = ''
				SET @multiValueSearchFields = '(SELECT COUNT(*) FROM AdditionalContactsInformation ACI WHERE ACI.numDivisionID=TEMP1.numDivisionID AND (ACI.vcLastName LIKE '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END +  REPLACE(@searchText, ',', '%'' OR ACI.vcLastName LIKE  '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END) + '%'')) > 0'
			ELSE	
				SET @multiValueSearchFields = @multiValueSearchFields + ' OR (SELECT COUNT(*) FROM AdditionalContactsInformation ACI WHERE ACI.numDivisionID=TEMP1.numDivisionID AND (ACI.vcLastName LIKE '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END +  REPLACE(@searchText, ',', '%'' OR ACI.vcLastName LIKE  '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END) + '%'')) > 0'
		END	
		
		-- 3. ORDER NAME
		IF (SELECT COUNT(*) FROM #tempOrgSearch WHERE numFieldId = 96) > 0
		BEGIN
			IF @multiValueSearchFields = ''
				SET @multiValueSearchFields = '(SELECT COUNT(*) FROM OpportunityMaster OM WHERE OM.numDivisionId = TEMP1.numDivisionID AND (OM.vcPOppName LIKE '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END +  REPLACE(@searchText, ',', '%'' OR OM.vcPOppName LIKE  '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END) + '%'')) > 0'
			ELSE	
				SET @multiValueSearchFields = @multiValueSearchFields + ' OR (SELECT COUNT(*) FROM OpportunityMaster OM WHERE OM.numDivisionId = TEMP1.numDivisionID AND (OM.vcPOppName LIKE '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END +  REPLACE(@searchText, ',', '%'' OR OM.vcPOppName LIKE  '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END) + '%'')) > 0'
		END	
		
		-- 4.PROJECT NAME
		IF (SELECT COUNT(*) FROM #tempOrgSearch WHERE numFieldId = 148) > 0
		BEGIN
			IF @multiValueSearchFields = ''
				SET @multiValueSearchFields = '(SELECT COUNT(*) FROM dbo.ProjectsMaster PM WHERE PM.numDivisionId = TEMP1.numDivisionID AND (PM.vcProjectName LIKE '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END +  REPLACE(@searchText, ',', '%'' OR PM.vcProjectName LIKE  '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END) + '%'')) > 0'
			ELSE	
				SET @multiValueSearchFields = @multiValueSearchFields + ' OR (SELECT COUNT(*) FROM dbo.ProjectsMaster PM WHERE PM.numDivisionId = TEMP1.numDivisionID AND (PM.vcProjectName LIKE '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END +  REPLACE(@searchText, ',', '%'' OR PM.vcProjectName LIKE  '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END) + '%'')) > 0'
		END	
		
		-- 5. CASE NAME
		IF (SELECT COUNT(*) FROM #tempOrgSearch WHERE numFieldId = 127) > 0
		BEGIN		
			IF @multiValueSearchFields = ''
				SET @multiValueSearchFields = '(SELECT COUNT(*) FROM dbo.Cases C WHERE C.numDivisionId = TEMP1.numDivisionID AND (C.textSubject LIKE '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END +  REPLACE(@searchText, ',', '%'' OR C.textSubject LIKE  '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END) + '%'')) > 0'
			ELSE	
				SET @multiValueSearchFields = @multiValueSearchFields + ' OR (SELECT COUNT(*) FROM dbo.Cases C WHERE C.numDivisionId = TEMP1.numDivisionID AND (C.textSubject LIKE '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END +  REPLACE(@searchText, ',', '%'' OR C.textSubject LIKE  '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END) + '%'')) > 0'
		END	
		
		--END: Gets search fields where there is one-many relation 
		
		--START: Get custom search fields
		
		IF EXISTS (SELECT * FROM #tempOrgSearch WHERE Custom = 1)
		BEGIN
			SELECT 
				@customSearchFields = COALESCE (@customSearchFields,'') + '( dbo.fn_GetCustFldOrganization(' + CAST(numFieldId AS VARCHAR(20)) + ',1,TEMP1.numCompanyID) LIKE '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END + REPLACE(@searchText, ',', '%'' OR dbo.fn_GetCustFldOrganization(' + CAST(numFieldId AS VARCHAR(20)) + ',1,TEMP1.numCompanyID) LIKE  ' + CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END) + '%'')' + ' OR '	
			FROM 
				#tempOrgSearch 
			WHERE 
				Custom = 1
		END
		
		-- Removes last OR from string
		IF DATALENGTH(@customSearchFields) > 0
			SET @customSearchFields = LEFT(@customSearchFields, LEN(@customSearchFields) - 3)
		
		--END: Get custom search fields
		
		IF DATALENGTH(@singleValueSearchFields) > 0
			SET @searchWhereCondition = @singleValueSearchFields
			
		IF DATALENGTH(@multiValueSearchFields) > 0
			IF DATALENGTH(@searchWhereCondition) > 0
				SET @searchWhereCondition = @searchWhereCondition + ' OR ' + @multiValueSearchFields
			ELSE
				SET @searchWhereCondition = @multiValueSearchFields
				
		IF DATALENGTH(@customSearchFields) > 0
			IF 	DATALENGTH(@searchWhereCondition) = 0
				SET @searchWhereCondition = @customSearchFields
			ELSE
				SET @searchWhereCondition = @searchWhereCondition + ' OR ' + @customSearchFields
			
	END
	ELSE
	BEGIN
		SET @searchWhereCondition = '(vcCompanyName LIKE '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END +  REPLACE(@searchText, ',', '%'' OR vcCompanyName LIKE  '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END) + '%'')'
	END
END

-- END: Generate user defined search fields condition

IF DATALENGTH(@searchWhereCondition) > 0
		SET @strSQL = @strSQL + ' WHERE ' + @searchWhereCondition 
    
 
SET @strSQL = @strSQL + ' ORDER BY vcCompanyname'    

PRINT @strSQL
--SELECT @strSQL
EXEC (@strSQL)

END
GO
/****** Object:  StoredProcedure [dbo].[USP_ConEmpListBasedonTeam]    Script Date: 07/26/2008 16:15:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Created By Anoop Jayaraj          
-- exec USP_ConEmpListBasedonTeam @numDomainID=72,@numUserCntID=17
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_conemplistbasedonteam')
DROP PROCEDURE usp_conemplistbasedonteam
GO
CREATE PROCEDURE [dbo].[USP_ConEmpListBasedonTeam]
    @numDomainID AS NUMERIC(9) = 0,
    @numUserCntID AS NUMERIC(9) = 0
AS 
	
    SELECT  DISTINCT A.numContactID,
            A.vcFirstName + ' ' + A.vcLastName AS vcUserName
            --,[numTeam]
    FROM    UserMaster UM
            JOIN AdditionalContactsInformation A ON UM.numUserDetailId = A.numContactID
            LEFT OUTER JOIN [UserTeams] UT ON UT.[numUserCntID] = UM.[numUserDetailId]
    WHERE   UM.numDomainID = @numDomainID AND UM.intAssociate=1
            --AND [bitActivateFlag] = 1
            AND UT.[numTeam] IN ( SELECT    numTeam
                                  FROM      UserTeams
                                  WHERE     numUserCntID = @numUserCntID
                                            AND numDomainID = @numDomainID )
            
--    SELECT  A.numContactID,
--            A.vcFirstName + ' ' + A.vcLastName AS vcUserName
--    FROM    UserMaster UM
--            JOIN AdditionalContactsInformation A ON UM.numUserDetailId = A.numContactID
--    WHERE   D.numDomainID = @numDomainID
--            AND D.numDivisionID = A.numDivisionID
--            AND [bitActivateFlag] = 1
--            AND A.numTeam IN ( SELECT   numTeam
--                               FROM     UserTeams
--                               WHERE    numUserCntID = @numUserCntID
--                                        AND numDomainID = @numDomainID )
GO
/****** Object:  StoredProcedure [dbo].[USP_ConEmpListFromTerr]    Script Date: 07/26/2008 16:15:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Created By Anoop Jayaraj        
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_conemplistfromterr')
DROP PROCEDURE usp_conemplistfromterr
GO
CREATE PROCEDURE [dbo].[USP_ConEmpListFromTerr]       
@numDomainID as numeric(9)=0,  
@bitPartner as bit=0,  
@numContactID as numeric(9)=0,
@numTerID as numeric(9)=0          
as     
  
if @bitPartner=0  
begin      
 SELECT A.numContactID,A.vcFirstName+' '+A.vcLastName as vcUserName        
 from UserMaster UM       
 join AdditionalContactsInformation A      
 on UM.numUserDetailId=A.numContactID        
 where UM.numDomainID=@numDomainID and UM.intAssociate=1 AND
 A.numContactID in (select numUserCntID from UserTerritory where numTerritoryID=@numTerID and numDomainID=@numDomainID) 
 union  
 select  A.numContactID,vcCompanyName+' - '+A.vcFirstName+' '+A.vcLastName as vcUserName  
 from AdditionalContactsInformation A   
 join DivisionMaster D  
 on D.numDivisionID=A.numDivisionID  
 join ExtarnetAccounts E   
 on E.numDivisionID=D.numDivisionID  
 join ExtranetAccountsDtl DTL  
 on DTL.numExtranetID=E.numExtranetID  
 join CompanyInfo C  
 on C.numCompanyID=D.numCompanyID  
 where A.numDomainID=@numDomainID   
 and D.numDivisionID <>(select numDivisionID from domain where numDomainID=@numDomainID) and  D.numTerID=@numTerID
end  
if @bitPartner=1  
begin      
 SELECT A.numContactID,A.vcFirstName+' '+A.vcLastName as vcUserName        
 from UserMaster UM       
 join AdditionalContactsInformation A      
 on UM.numUserDetailId=A.numContactID        
 where UM.numDomainID=@numDomainID  and  UM.intAssociate=1 AND
 A.numContactID in (select numUserCntID from UserTerritory where numTerritoryID=@numTerID and numDomainID=@numDomainID)
 union  
 select  A.numContactID,vcCompanyName+' - '+A.vcFirstName+' '+A.vcLastName as vcUserName  
 from AdditionalContactsInformation A   
 join DivisionMaster D  
 on D.numDivisionID=A.numDivisionID  
 join CompanyInfo C  
 on C.numCompanyID=D.numCompanyID  
 where A.numContactID=@numContactID and  D.numTerID=@numTerID and D.numDivisionID <>(select numDivisionID from domain where numDomainID=@numDomainID)  
end
GO
/****** Object:  StoredProcedure [dbo].[USP_EcommerceSettings]    Script Date: 03/25/2009 16:46:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj              
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_ecommercesettings')
DROP PROCEDURE usp_ecommercesettings
GO
CREATE PROCEDURE [dbo].[USP_EcommerceSettings]              
@numDomainID as numeric(9),              
@vcPaymentGateWay as int ,                
@vcPGWUserId as varchar(100)='' ,                
@vcPGWPassword as varchar(100),              
@bitShowInStock as bit,              
@bitShowQOnHand as bit,
@bitCheckCreditStatus as BIT,
@numDefaultWareHouseID as numeric(9),
@numDefaultIncomeChartAcntId as numeric(9),
@numDefaultAssetChartAcntId as numeric(9),
@numDefaultCOGSChartAcntId as numeric(9),
@numRelationshipId as numeric(9),
@numProfileId as numeric(9),
@bitEnableDefaultAccounts BIT,
@bitHidePriceBeforeLogin	BIT,
@bitHidePriceAfterLogin	BIT,
@numRelationshipIdHidePrice	numeric(18, 0),
@numProfileIDHidePrice	numeric(18, 0),
@bitAuthOnlyCreditCard BIT=0,
@bitSendMail BIT = 1,
@vcGoogleMerchantID VARCHAR(1000)  = '',
@vcGoogleMerchantKey VARCHAR(1000) = '',
@IsSandbox BIT ,
@numAuthrizedOrderStatus NUMERIC,
@numSiteID numeric(18, 0),
@vcPaypalUserName VARCHAR(50)  = '',
@vcPaypalPassword VARCHAR(50)  = '',
@vcPaypalSignature VARCHAR(500)  = '',
@IsPaypalSandbox BIT,
@bitSkipStep2 BIT = 0,
@bitDisplayCategory BIT = 0,
@bitShowPriceUsingPriceLevel BIT = 0,
@bitEnableSecSorting BIT = 0,
@bitSortPriceMode   BIT=0,
@numPageSize    INT=15,
@numPageVariant  INT=6,
@bitAutoSelectWarehouse BIT =0,
@bitPreSellUp BIT = 0, 
@bitPostSellUp BIT = 0, 
@dcPostSellDiscount DECIMAL  = 0,
@numDefaultClass NUMERIC(18,0) = 0
as              
            
            
if not exists(select 'col1' from eCommerceDTL where numDomainID=@numDomainID AND [numSiteId] = @numSiteID)            
INSERT INTO eCommerceDTL
           (numDomainId,
            vcPaymentGateWay,
            vcPGWManagerUId,
            vcPGWManagerPassword,
            bitShowInStock,
            bitShowQOnHand,
            --tintItemColumns,
           -- numDefaultCategory,
            numDefaultWareHouseID,
            numDefaultIncomeChartAcntId,
            numDefaultAssetChartAcntId,
            numDefaultCOGSChartAcntId,
            --bitHideNewUsers,
            bitCheckCreditStatus,
            [numRelationshipId],
            [numProfileId],
            [bitEnableDefaultAccounts],
 --           [bitEnableCreditCart],
            [bitHidePriceBeforeLogin],
            [bitHidePriceAfterLogin],
            [numRelationshipIdHidePrice],
            [numProfileIDHidePrice],
--            numBizDocForCreditCard,
--            numBizDocForCreditTerm,
            bitAuthOnlyCreditCard,
--            numAuthrizedOrderStatus,
            bitSendEmail,
            vcGoogleMerchantID,
            vcGoogleMerchantKey,
            IsSandbox ,
            numAuthrizedOrderStatus,
			numSiteId,
			vcPaypalUserName,
			vcPaypalPassword,
			vcPaypalSignature,
			IsPaypalSandbox,
			bitSkipStep2,
			bitDisplayCategory,
			bitShowPriceUsingPriceLevel,
			bitEnableSecSorting,
			bitSortPriceMode,
			numPageSize,
			numPageVariant,
			bitAutoSelectWarehouse,
			[bitPreSellUp],
			[bitPostSellUp],
			[dcPostSellDiscount],
			numDefaultClass
			)

VALUES     (@numDomainID,
            @vcPaymentGateWay,
            @vcPGWUserId,
            @vcPGWPassword,
            @bitShowInStock,
            @bitShowQOnHand,
           -- @tintColumns,
           -- @numCategory,
            @numDefaultWareHouseID,
            @numDefaultIncomeChartAcntId,
            @numDefaultAssetChartAcntId,
            @numDefaultCOGSChartAcntId,
           -- @bitHideNewUsers,
            @bitCheckCreditStatus,
            @numRelationshipId,
            @numProfileId,
            @bitEnableDefaultAccounts,
 --           @bitEnableCreditCart,
            @bitHidePriceBeforeLogin,
            @bitHidePriceAfterLogin,
            @numRelationshipIdHidePrice,
            @numProfileIDHidePrice,
--            @numBizDocForCreditCard,
--            @numBizDocForCreditTerm,
            @bitAuthOnlyCreditCard,
--            @numAuthrizedOrderStatus,
            @bitSendMail,
            @vcGoogleMerchantID,
            @vcGoogleMerchantKey ,
            @IsSandbox,
            @numAuthrizedOrderStatus,
			@numSiteID,
			@vcPaypalUserName,
			@vcPaypalPassword,
			@vcPaypalSignature,
			@IsPaypalSandbox,
			@bitSkipStep2,
			@bitDisplayCategory,
			@bitShowPriceUsingPriceLevel,
			@bitEnableSecSorting,
			@bitSortPriceMode,
			@numPageSize,
			@numPageVariant,
			@bitAutoSelectWarehouse,
			@bitPreSellUp,
			@bitPostSellUp,
			@dcPostSellDiscount,
			@numDefaultClass
            )
 else            
 update eCommerceDTL                                   
   set              
 vcPaymentGateWay=@vcPaymentGateWay,                
 vcPGWManagerUId=@vcPGWUserId ,                
 vcPGWManagerPassword= @vcPGWPassword,              
 bitShowInStock=@bitShowInStock ,               
 bitShowQOnHand=@bitShowQOnHand,        
 --tintItemColumns= @tintColumns,      
 --numDefaultCategory=@numCategory,
 numDefaultWareHouseID =@numDefaultWareHouseID,
 numDefaultIncomeChartAcntId =@numDefaultIncomeChartAcntId ,
 numDefaultAssetChartAcntId =@numDefaultAssetChartAcntId ,
 numDefaultCOGSChartAcntId =@numDefaultCOGSChartAcntId ,
 --bitHideNewUsers =@bitHideNewUsers,
 bitCheckCreditStatus=@bitCheckCreditStatus ,
 numRelationshipId = @numRelationshipId,
 numProfileId = @numProfileId,
 bitEnableDefaultAccounts = @bitEnableDefaultAccounts,
 --[bitEnableCreditCart]=@bitEnableCreditCart,
 [bitHidePriceBeforeLogin]=@bitHidePriceBeforeLogin,
 [bitHidePriceAfterLogin]=@bitHidePriceAfterLogin,
 [numRelationshipIdHidePrice]=@numRelationshipIdHidePrice,
 [numProfileIDHidePrice]=@numProfileIDHidePrice,
-- numBizDocForCreditCard=@numBizDocForCreditCard,
-- numBizDocForCreditTerm=@numBizDocForCreditTerm,
 bitAuthOnlyCreditCard=@bitAuthOnlyCreditCard,
-- numAuthrizedOrderStatus=@numAuthrizedOrderStatus,
 bitSendEmail = @bitSendMail,
 vcGoogleMerchantID = @vcGoogleMerchantID ,
 vcGoogleMerchantKey =  @vcGoogleMerchantKey ,
 IsSandbox = @IsSandbox,
 numAuthrizedOrderStatus = @numAuthrizedOrderStatus,
	numSiteId = @numSiteID,
vcPaypalUserName = @vcPaypalUserName ,
vcPaypalPassword = @vcPaypalPassword,
vcPaypalSignature = @vcPaypalSignature,
IsPaypalSandbox = @IsPaypalSandbox,
bitSkipStep2 = @bitSkipStep2,
bitDisplayCategory = @bitDisplayCategory,
bitShowPriceUsingPriceLevel = @bitShowPriceUsingPriceLevel,
bitEnableSecSorting = @bitEnableSecSorting,
bitSortPriceMode=@bitSortPriceMode,
numPageSize=@numPageSize,
numPageVariant=@numPageVariant,
bitAutoSelectWarehouse=@bitAutoSelectWarehouse,
[bitPreSellUp] = @bitPreSellUp,
[bitPostSellUp] = @bitPostSellUp,
[dcPostSellDiscount] = @dcPostSellDiscount,
numDefaultClass=@numDefaultClass
 where numDomainId=@numDomainID AND [numSiteId] = @numSiteID
/****** Object:  UserDefinedFunction [dbo].[USP_GetAttributes]    Script Date: 07/26/2008 18:13:15 ******/

GO

GO
--- Created By Anoop Jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='usp_getattributes')
DROP FUNCTION usp_getattributes
GO
CREATE FUNCTION [dbo].[USP_GetAttributes] (@numRecID numeric,@bitSerialized  bit)
returns varchar(100)
as
begin
declare @strAttribute varchar(100)
declare @numCusFldID numeric(9)
declare @numCusFldValue varchar(100)
set @numCusFldID=0
set @strAttribute=''
select top 1 @numCusFldID=fld_id,@numCusFldValue=REPLACE(fld_value,'$','') from CFW_Fld_Values_Serialized_Items where RecId=@numRecID and fld_value!='0' and fld_value!='' and bitSerialized=@bitSerialized order by fld_id
while @numCusFldID>0
begin
	if isnumeric(@numCusFldValue)=1
	begin
		--set @strAttribute=@strAttribute+@numCusFldValue
		select @strAttribute=@strAttribute+Fld_label from  CFW_Fld_Master where Fld_id=@numCusFldID
		select @strAttribute=@strAttribute+':'+ vcData +',' from ListDetails where numListItemID=@numCusFldValue
	end
	else
	begin
		select @strAttribute=@strAttribute+Fld_label from  CFW_Fld_Master where Fld_id=@numCusFldID
		select @strAttribute=@strAttribute+':-,' 
	end
	select top 1 @numCusFldID=fld_id,@numCusFldValue=fld_value from CFW_Fld_Values_Serialized_Items where RecId=@numRecID and fld_value!='0' and fld_value!='' and bitSerialized=@bitSerialized and fld_id>@numCusFldID order by fld_id
	if @@rowcount=0 set @numCusFldID=0
	
end

If LEN(@strAttribute) > 0
BEGIN
	SET @strAttribute = LEFT(@strAttribute, LEN(@strAttribute) - 1)
END

RETURN @strAttribute
end
GO
/****** Object:  StoredProcedure [dbo].[USP_GetCategory]    Script Date: 08/08/2009 16:37:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                        
-- exec [dbo].[USP_GetCategory] 0,0,'',0,1,3
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcategory')
DROP PROCEDURE usp_getcategory
GO
CREATE PROCEDURE [dbo].[USP_GetCategory]                        
@numCatergoryId as numeric(9),                        
@byteMode as tinyint ,                  
@str as varchar(1000),                  
@numDomainID as numeric(9)=0,
@numSiteID AS NUMERIC(9)=0 ,
@numCurrentPage as numeric(9)=0,
@PageSize AS INT=20,
@numTotalPage as numeric(9) OUT,    
@numItemCode AS NUMERIC(9,0),
@SortChar CHAR(1) = '0',
@numCategoryProfileID NUMERIC(18,0)
               
as   

set nocount on                     
--selecting all categories                     
declare @strsql as varchar(8000)
DECLARE @strRowCount VARCHAR(8000)  


create table #TempCategory
(PKId numeric(9) identity,
numItemCode numeric(9),
vcItemName varchar(250));

                  
if @byteMode=0                        
BEGIN
	 ;WITH CategoryList
As 
( SELECT C1.numCategoryID,C1.vcCategoryName,C1.numDepCategory,C1.tintLevel,case when C1.bitLink=0 then 'No' when C1.bitLink=1 then 'Yes' end as Link,C1.vcLink as vcLink,CAST('' AS VARCHAR(1000)) as DepCategory,
	C1.intDisplayOrder,C1.vcDescription,
	CASE WHEN LEN(ISNULL(C1.vcDescription ,'')) > 0 THEN  
	CASE WHEN LEN(C1.vcDescription) > 40 THEN SUBSTRING(C1.vcDescription,0,40) + ' [...]' 
         ELSE C1.vcDescription + ' [...]'	END 
    ELSE
		 ISNULL(C1.vcDescription ,'')   
    END     
         AS vcShortDescription,1 AS [Level],
	CAST (REPLICATE('.',1) + C1.vcCategoryName as varchar(1000) )  as vcNewCategoryName,
	CAST(C1.numCategoryID AS VARCHAR(MAX)) AS [Order]
 FROM dbo.Category C1 WHERE C1.numDepCategory=0 AND  C1.numDomainID=@numDomainID AND C1.numCategoryProfileID = @numCategoryProfileID
 UNION ALL
 SELECT  C1.numCategoryID,C1.vcCategoryName,C1.numDepCategory,C1.tintLevel,case when C1.bitLink=0 then 'No' when C1.bitLink=1 then 'Yes' end as Link,C1.vcLink as vcLink,CL.vcCategoryName as DepCategory ,
	C1.intDisplayOrder,C1.vcDescription,
	CASE WHEN LEN(ISNULL(C1.vcDescription ,'')) > 0 THEN  
	CASE WHEN LEN(C1.vcDescription) > 40 THEN SUBSTRING(C1.vcDescription,0,40) + ' [...]' 
         ELSE C1.vcDescription + ' [...]'	END 
    ELSE
		 ISNULL(C1.vcDescription ,'')   
    END     
         AS vcShortDescription,CL.[Level] + 1 AS [Level],
	CAST (REPLICATE('.',[Level] * 3) + C1.vcCategoryName as varchar(1000) )  as vcNewCategoryName,
	[Order] + '.' + CAST(C1.numCategoryID AS VARCHAR(MAX)) AS [Order]
 FROM dbo.Category C1 
 INNER JOIN CategoryList CL
 ON CL.numCategoryID = C1.numDepCategory  WHERE  C1.numDomainID=@numDomainID  AND C1.numCategoryProfileID = @numCategoryProfileID
)   

SELECT *,(SELECT COUNT(*) FROM [SiteCategories] SC WHERE SC.[numSiteID] =@numSiteID AND SC.[numCategoryID]= C1.[numCategoryID]) AS ShowInECommerce,
ISNULL((SELECT COUNT(*) FROM [ItemCategory] WHERE [numCategoryID] = C1.[numCategoryID]),0) AS ItemCount FROM CategoryList C1 ORDER BY [Order]
end                                                
--deleting a category                      
else if @byteMode=1                        
begin     
	delete from SiteCategories where numCategoryID=@numCatergoryId                     
	delete from ItemCategory where numCategoryID=@numCatergoryId
	delete from Category where numCategoryID=@numCatergoryId       
	select 1                        
end                        
--selecting all catories where level is 1                       
else if @byteMode=2                      
begin                        
	select numCategoryID,vcCategoryName,intDisplayOrder,vcDescription from Category                      
	where ((tintlevel=1 )  or (tintlevel<>1 and numDepCategory=@numCatergoryId))  and numCategoryID!=@numCatergoryId                      
	and numDomainID=@numDomainID                    
	ORDER BY [vcCategoryName]
end                      
--selecting subCategories                      
else if @byteMode=3                      
begin                        
	select numCategoryID,vcCategoryName,intDisplayOrder,vcDescription  from Category                      
	where numDepCategory=@numCatergoryId                      
	ORDER BY [vcCategoryName]
end                      
                      
--selecting Items                      
if @byteMode=4                      
begin                        
	select numItemCode,vcItemName  from Item           
	where numDomainID=@numDomainID                       
end                      
                      
--selecting Item belongs to a category                      
else if @byteMode=5                      
begin    


	insert into #TempCategory
	                       
	select numItemCode,CONCAT(numItemCode,' - ',vcItemName) AS vcItemName  from Item                      
	join ItemCategory                       
	on numItemCode=numItemID                      
	where numCategoryID=@numCatergoryId           
	and numDomainID=@numDomainID                       
	ORDER BY [vcItemName]


	select @numTotalPage =COUNT(*) from #TempCategory

	if @numTotalPage<@PageSize 
		begin
			set @numTotalPage=1
		end
	else	
		begin
			set @numTotalPage=@numTotalPage/@PageSize
		end

	select * from #TempCategory  where PKId between (@numCurrentPage*@PageSize)- (@PageSize-1) and  @numCurrentPage*@PageSize

	select @numTotalPage

end                      
                    
else if @byteMode=6                    
begin                        

	select C1.numCategoryID,c1.vcCategoryName,((select Count(*) from Category C2 where C2.numDepCategory=C1.numCategoryID)+(select count(*) from ItemCategory C3 where C3.numCategoryID=C1.numCategoryID  ) ) as [Count],convert(integer,bitLink) as Type,vcLink,C1.intDisplayOrder,
	C1.vcDescription   
	from Category C1                     
	where tintlevel=1                     
	and numDomainID=@numDomainID   
	AND numCategoryProfileID=@numCategoryProfileID                   
                    
end                     
                    
else if @byteMode=7                    
begin                        

	select C1.numCategoryID,c1.vcCategoryName,((select Count(*) from Category C2 where C2.numDepCategory=C1.numCategoryID)+(select count(*) from ItemCategory C3 where C3.numCategoryID=C1.numCategoryID ) ) as [Count],convert(integer,bitLink) as Type,vcLink,C1.intDisplayOrder,
	C1.vcDescription,numDepCategory as Category
	from Category C1                     
	where C1.numDepCategory=@numCatergoryId and numDomainID=@numDomainID                      
	union                     
	select numItemCode,vcItemName,0 ,2,'',0,'',0  from Item                      
	join ItemCategory                       
	on numItemCode=numItemID                      
	where numCategoryID=@numCatergoryId  and numDomainID=@numDomainID                    
end                     
                
else if @byteMode=8                     
begin                    
     --kishan               
	set @strsql='select numItemCode,vcItemName,txtItemDesc,0,
	(
	SELECT TOP 1 II.vcPathForTImage 
    FROM dbo.ItemImages II 
        WHERE II.numItemCode = Item.numItemCode 
    AND II.bitDefault = 1 , AND II.bitIsImage = 1 
    AND II.numDomainID ='+ cast(@numDomainId as varchar(50))+') 
	vcPathForTImage ,
	1 as Type,monListPrice as price,0 as numQtyOnHand  from Item                    
	join ItemCategory                       
	on numItemCode=numItemID                      
	where numCategoryID in('+@str+')'                  
	exec (@strsql)                   
end                    
                  
else if @byteMode=9                  
begin                    
                    
	select distinct(numItemCode),vcItemName,txtItemDesc,0 , (SELECT TOP 1 vcPathForImage FROM ItemImages WHERE numDomainId = @numDomainId AND bitDefault = 1 AND numItemCode = Item.numItemCode) AS vcPathForImage,1 as Type,monListPrice as price,0 as numQtyOnHand from Item                      
	join ItemCategory                       
	on numItemCode=numItemID                      
	where ( vcItemName like '%'+@str+'%' or txtItemDesc like '%'+@str+'%') and numDomainID= @numDomainID                                    
end        
      
else if @byteMode=10      
begin       
	select ROW_NUMBER() OVER (ORDER BY Category Asc) AS RowNumber,* from (select   C1.numCategoryID,c1.vcCategoryName,convert(integer,bitLink) as Type,vcLink ,numDepCategory as Category,C1.intDisplayOrder,
	C1.vcDescription         
	from Category C1                     
	where  numDomainID=@numDomainID)X       
      
end 


--selecting Items not present in Category                   
else if @byteMode=11                    
begin     

	insert into #TempCategory
	         
	SELECT I.numItemCode, CONCAT(I.numItemCode,' - ',I.vcItemName) AS vcItemName FROM item  I LEFT OUTER JOIN [ItemCategory] IC ON IC.numItemID = I.numItemCode
	WHERE [numDomainID] =@numDomainID AND ISNULL(IC.numCategoryID,0) <> @numCatergoryId
	ORDER BY [vcItemName]
          
--	select numItemCode,vcItemName  from Item LEFT OUTER JOIN [ItemCategory] ON Item.[numItemCode] = [ItemCategory].[numCategoryID]
--	where [ItemCategory].[numCategoryID] <> @numCatergoryId
----numItemCode not in (select numItemID from ItemCategory where numCategoryID=@numCatergoryId)
--	and numDomainID=@numDomainID                      
--	ORDER BY [vcItemName]



	select @numTotalPage =COUNT(*) from #TempCategory

	set @numTotalPage=@numTotalPage/@PageSize

	select * from #TempCategory  where PKId between (@numCurrentPage*@PageSize)-(@PageSize-1) and  @numCurrentPage*@PageSize

	select @numTotalPage


end   


--selecting Items From Item Groups                  
else if @byteMode=12                
begin   

	insert into #TempCategory
	                     
	select numItemCode,vcItemName  from Item 
	where numItemGroup= @numCatergoryId  
	ORDER BY [vcItemName] 



	select @numTotalPage =COUNT(*) from #TempCategory

	set @numTotalPage=@numTotalPage/@PageSize

	select * from #TempCategory  where PKId between (@numCurrentPage*@PageSize)-(@PageSize-1) and  @numCurrentPage*@PageSize

	select @numTotalPage

              
end

-- Selecting Categories Site Wise and Sorted into Sort Order 
else if @byteMode=13
BEGIN
	SELECT  
		@numCategoryProfileID=CPS.numCategoryProfileID
	FROM 
		Sites S
	JOIN
		CategoryProfileSites CPS
	ON
		S.numSiteID=CPS.numSiteID
	JOIN
		CategoryProfile CP
	ON
		CPS.numCategoryProfileID = CP.ID
	WHERE 
		S.numSiteID=@numSiteID


	DECLARE @bParentCatrgory BIT;
	SELECT @bParentCatrgory=vcAttributeValue FROM PageElementDetail WHERE [numSiteID] = @numSiteID AND numAttributeID=39
	SET @bParentCatrgory=ISNULL(@bParentCatrgory,0)

	DECLARE @tintDisplayCategory AS TINYINT
	SELECT @tintDisplayCategory = ISNULL(bitDisplayCategory,0) FROM dbo.eCommerceDTL WHERE numSiteID = @numSiteId

	
	IF ISNULL(@tintDisplayCategory,0) = 0 
	BEGIN
		SELECT C.numCategoryID,
			   C.vcCategoryName,
			   C.tintLevel,
			   convert(integer,C.bitLink) as TYPE,
			   C.vcLink AS vcLink,
			   numDepCategory as Category,C.intDisplayOrder,C.vcDescription 
		FROM   [SiteCategories] SC
			   INNER JOIN [Category] C ON SC.[numCategoryID] = C.[numCategoryID]
		WHERE  SC.[numSiteID] = @numSiteID
			   AND SC.[numCategoryID] = C.[numCategoryID] 
			   AND 1 = ( CASE WHEN @bParentCatrgory = 1 THEN tintLevel ELSE 1 END )
			   ORDER BY tintLevel , 
						ISNULL(C.intDisplayOrder,0),
						Category	
	END	
	ELSE
	BEGIN
		SELECT C.numCategoryID,
			   C.vcCategoryName,
			   C.tintLevel,
			   convert(integer,C.bitLink) as TYPE,
			   C.vcLink AS vcLink,
			   numDepCategory as Category,C.intDisplayOrder,C.vcDescription 
		FROM   [SiteCategories] SC
			   INNER JOIN [Category] C ON SC.[numCategoryID] = C.[numCategoryID]
		WHERE  SC.[numSiteID] = @numSiteID
			   AND SC.[numCategoryID] = C.[numCategoryID] 
			   AND 1 = ( CASE WHEN @bParentCatrgory = 1 THEN tintLevel ELSE 1 END )
			   AND (
					SC.numCategoryID IN (
										SELECT numCategoryID FROM dbo.ItemCategory 
										WHERE numItemID IN (
															SELECT numItemID FROM WareHouseItems WI
															INNER JOIN dbo.Item I ON I.numItemCode = WI.numItemID
															GROUP BY numItemID 
															HAVING SUM(WI.numOnHand) > 0
															UNION ALL
															SELECT numItemID FROM WareHouseItems WI
															INNER JOIN dbo.Item I ON I.numItemCode = WI.numItemID
															GROUP BY numItemID 
															HAVING SUM(WI.numAllocation) > 0
														   )
									   )
					OR tintLevel = 1
					)				   
			   ORDER BY tintLevel , 
						ISNULL(C.intDisplayOrder,0),
						Category
	END			

END

else if @byteMode=14
BEGIN

	SELECT C.numCategoryID,
		   C.vcCategoryName,
		   C.tintLevel,
		   convert(integer,C.bitLink) as TYPE,
		   C.vcLink AS vcLink,
		   numDepCategory as Category,C.intDisplayOrder,C.vcPathForCategoryImage,C.vcDescription 
	FROM   [SiteCategories] SC
		   INNER JOIN [Category] C
			 ON SC.[numCategoryID] = C.[numCategoryID]
	WHERE  SC.[numSiteID] = @numSiteID
		   AND SC.[numCategoryID] = C.[numCategoryID]
		 ORDER BY ISNULL(C.intDisplayOrder,0),Category
END

else if @byteMode=15
BEGIN

DECLARE @bSubCatrgory BIT;

SELECT @bSubCatrgory=vcAttributeValue FROM PageElementDetail WHERE [numSiteID] = @numSiteID AND numAttributeID=40
SET @bSubCatrgory=ISNULL(@bSubCatrgory,0)

	SELECT C.numCategoryID,
		   C.vcCategoryName,
		   C.tintLevel,
		   convert(integer,C.bitLink) as TYPE,
		   C.vcLink AS vcLink,
		   numDepCategory as Category,C.intDisplayOrder,C.vcPathForCategoryImage,C.vcDescription 
		   
		   
	FROM   [SiteCategories] SC
		   INNER JOIN [Category] C
			 ON SC.[numCategoryID] = C.[numCategoryID]
	WHERE  SC.[numSiteID] = @numSiteID
		   AND SC.[numCategoryID] = C.[numCategoryID] AND numDepCategory=@numCatergoryId
		   AND 1=(CASE WHEN @bSubCatrgory=1 THEN 1 ELSE 0 END)
		    ORDER BY ISNULL(C.intDisplayOrder,0),Category
END

ELSE if @byteMode=16
BEGIN
 SELECT vcPathForCategoryImage FROM Category WHERE numCategoryID=@numCatergoryId AND numDomainID=@numDomainID
 
END
ELSE if @byteMode=17
BEGIN
 SELECT numCategoryID ,vcCategoryName ,vcDescription,intDisplayOrder,vcPathForCategoryImage,numDepCategory FROM Category  WHERE numCategoryID = @numCatergoryId
END
ELSE IF @byteMode = 18
BEGIN
	-- USED FOR HIERARCHICAL CATEGORY LIST
	SELECT 
		C1.numCategoryID,
		C1.vcCategoryName,
		NULLIF(numDepCategory,0) numDepCategory,
		ISNULL(numDepCategory,0) numDepCategory1,
		0 AS tintLevel,
		ISNULL((SELECT COUNT(*) FROM [ItemCategory] WHERE [numCategoryID] = C1.[numCategoryID]),0) AS ItemCount,
		ISNULL((SELECT COUNT(*) FROM [ItemCategory] WHERE [numCategoryID] IN (SELECT numCategoryID FROM category WHERE numDepCategory = C1.[numCategoryID])),0) AS ItemSubcategoryCount
	FROM 
		Category C1 
	WHERE 
		numDomainID =@numDomainID AND numCategoryProfileID = @numCategoryProfileID
END

ELSE IF @byteMode=19
BEGIN
	SELECT  
		@numCategoryProfileID=CPS.numCategoryProfileID
	FROM 
		Sites S
	JOIN
		CategoryProfileSites CPS
	ON
		S.numSiteID=CPS.numSiteID
	JOIN
		CategoryProfile CP
	ON
		CPS.numCategoryProfileID = CP.ID
	WHERE 
		S.numSiteID=@numSiteID

	BEGIN
	 ;WITH CategoryList
As 
( SELECT C1.numCategoryID,C1.vcCategoryName,C1.numDepCategory,C1.tintLevel,case when C1.bitLink=0 then 'No' when C1.bitLink=1 then 'Yes' end as Link,C1.vcLink as vcLink,CAST('' AS VARCHAR(1000)) as DepCategory,
	C1.intDisplayOrder,C1.vcDescription,
	CASE WHEN LEN(ISNULL(C1.vcDescription ,'')) > 0 THEN  
	CASE WHEN LEN(C1.vcDescription) > 40 THEN SUBSTRING(C1.vcDescription,0,40) + ' [...]' 
         ELSE C1.vcDescription + ' [...]'	END 
    ELSE
		 ISNULL(C1.vcDescription ,'')   
    END     
         AS vcShortDescription,1 AS [Level],
	CAST (REPLICATE('.',1) + C1.vcCategoryName as varchar(1000) )  as vcNewCategoryName,
	CAST(C1.numCategoryID AS VARCHAR(MAX)) AS [Order]
 FROM dbo.Category C1 WHERE C1.numDepCategory=0 AND  C1.numDomainID=@numDomainID AND C1.numCategoryProfileID=@numCategoryProfileID
 UNION ALL
 SELECT  C1.numCategoryID,C1.vcCategoryName,C1.numDepCategory,C1.tintLevel,case when C1.bitLink=0 then 'No' when C1.bitLink=1 then 'Yes' end as Link,C1.vcLink as vcLink,CL.vcCategoryName as DepCategory ,
	C1.intDisplayOrder,C1.vcDescription,
	CASE WHEN LEN(ISNULL(C1.vcDescription ,'')) > 0 THEN  
	CASE WHEN LEN(C1.vcDescription) > 40 THEN SUBSTRING(C1.vcDescription,0,40) + ' [...]' 
         ELSE C1.vcDescription + ' [...]'	END 
    ELSE
		 ISNULL(C1.vcDescription ,'')   
    END     
         AS vcShortDescription,CL.[Level] + 1 AS [Level],
	CAST (REPLICATE('.',[Level] * 3) + C1.vcCategoryName as varchar(1000) )  as vcNewCategoryName,
	[Order] + '.' + CAST(C1.numCategoryID AS VARCHAR(MAX)) AS [Order]
 FROM dbo.Category C1 
 INNER JOIN CategoryList CL
 ON CL.numCategoryID = C1.numDepCategory  WHERE  C1.numDomainID=@numDomainID AND C1.numCategoryProfileID=@numCategoryProfileID
)   

SELECT numCategoryID,vcCategoryName,(CASE WHEN  numDepCategory=0 THEN NULL ELSE numDepCategory END) AS numDepCategory ,tintLevel,Link,vcLink,DepCategory,(SELECT COUNT(*) FROM [SiteCategories] SC WHERE SC.[numSiteID] =@numSiteID AND SC.[numCategoryID]= C1.[numCategoryID]) AS ShowInECommerce,
ISNULL((SELECT COUNT(*) FROM [ItemCategory] WHERE [numCategoryID] = C1.[numCategoryID]),0) AS ItemCount FROM CategoryList C1 ORDER BY [Order]
END


DROP TABLE #TempCategory
END
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetChartAcntDetailsForBalanceSheet_New')
DROP PROCEDURE USP_GetChartAcntDetailsForBalanceSheet_New
GO
CREATE PROCEDURE [dbo].[USP_GetChartAcntDetailsForBalanceSheet_New]                                 
@numDomainId AS NUMERIC(9),                                                  
@dtFromDate AS DATETIME,                                                
@dtToDate AS DATETIME,
@ClientTimeZoneOffset INT, 
@numAccountClass AS NUMERIC(9)=0,
@DateFilter AS VARCHAR(20),
@ReportColumn AS VARCHAR(20)                                                        
AS                                                                
BEGIN                                                                
	DECLARE @PLAccountStruc VARCHAR(500)
	DECLARE @FinancialYearStartDate AS DATETIME
	SELECT @FinancialYearStartDate = dbo.GetFiscalStartDate(YEAR(GETDATE()),@numDomainId)

	DECLARE @TEMPQuarter AS TABLE
	(
		ID INT,
		StartDate DATETIME,
		EndDate DATETIME
	)

	INSERT INTO @TEMPQuarter VALUES (1,@FinancialYearStartDate,DATEADD(ms,-3,DATEADD(QUARTER,1,@FinancialYearStartDate)))
	INSERT INTO @TEMPQuarter VALUES (2,DATEADD(QUARTER,1,@FinancialYearStartDate),DATEADD(ms,-3,DATEADD(QUARTER,2,@FinancialYearStartDate)))
	INSERT INTO @TEMPQuarter VALUES (3,DATEADD(QUARTER,2,@FinancialYearStartDate),DATEADD(ms,-3,DATEADD(QUARTER,3,@FinancialYearStartDate)))
	INSERT INTO @TEMPQuarter VALUES (4,DATEADD(QUARTER,3,@FinancialYearStartDate),DATEADD(ms,-3,DATEADD(QUARTER,4,@FinancialYearStartDate)))

	IF @DateFilter = 'CurYear'
	BEGIN
		SELECT @dtFromDate = @FinancialYearStartDate,
			@dtToDate = DATEADD(ms,-3,DATEADD(YEAR,1,@FinancialYearStartDate));
	END
	ELSE IF @DateFilter = 'PreYear'
	BEGIN
		SELECT @dtFromDate = DATEADD(YEAR,-1,@FinancialYearStartDate),
			@dtToDate = DATEADD(ms,-3,DATEADD(YEAR,1,DATEADD(YEAR,-1,@FinancialYearStartDate)));
	END
	ELSE IF @DateFilter = 'CurPreYear'
	BEGIN
		SELECT @dtFromDate = DATEADD(YEAR,-1,@FinancialYearStartDate),
			@dtToDate = DATEADD(ms,-3,DATEADD(YEAR,1,@FinancialYearStartDate));
	END
	ELSE IF @DateFilter = 'CuQur'
	BEGIN
		SELECT @dtFromDate = StartDate, @dtToDate=EndDate FROM @TEMPQuarter WHERE GetDate() BETWEEN StartDate AND EndDate
	END
	ELSE IF @DateFilter = 'CurPreQur'
	BEGIN
		SELECT @dtFromDate = DATEADD(QUARTER,-1,StartDate), @dtToDate=EndDate FROM @TEMPQuarter WHERE GetDate() BETWEEN StartDate AND EndDate
	END
	ELSE IF @DateFilter = 'PreQur'
	BEGIN
		SELECT @dtFromDate = DATEADD(QUARTER,-1,StartDate), @dtToDate=DATEADD(QUARTER,-1,EndDate) FROM @TEMPQuarter WHERE GetDate() BETWEEN StartDate AND EndDate
	END
	ELSE IF @DateFilter = 'LastMonth'
	BEGIN
		select @dtFromDate = DATEADD(mm,-1,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)),
				@dtToDate = DATEADD(ms,-3,DATEADD(mm,0,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)));  
	END
	ELSE IF @DateFilter = 'ThisMonth'
	BEGIN
		select @dtFromDate = DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0),
				@dtToDate = DATEADD(ms,- 3,DATEADD(mm,0,DATEADD(mm,DATEDIFF(mm,0,GETDATE())+1,0)));  
	END
	ELSE IF @DateFilter = 'CurPreMonth'
	BEGIN
		select @dtFromDate = DATEADD(mm,-1,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)),
				@dtToDate = DATEADD(ms,- 3,DATEADD(mm,0,DATEADD(mm,DATEDIFF(mm,0,GETDATE())+1,0))); 
	END

	CREATE TABLE #VIEW_JOURNALBS
	(
		numDomainID NUMERIC(18,0),
		numAccountID NUMERIC(18,0),
		numAccountClass NUMERIC(18,0),
		COAvcAccountCode VARCHAR(50),
		datEntry_Date DATETIME,
		Debit MONEY,
		Credit MONEY
	)

	INSERT INTO #VIEW_JOURNALBS SELECT numDomainID,numAccountID,numAccountClass,COAvcAccountCode,datEntry_Date,Debit,Credit FROM VIEW_JOURNALBS WHERE numDomainID =@numDomainId AND (numAccountClass=@numAccountClass OR @numAccountClass=0) ORDER BY numAccountId; 

	DECLARE @view_journal TABLE
	(
		numAccountId NUMERIC(18,0),
		vcAccountCode VARCHAR(50),
		datEntry_Date SMALLDATETIME,
		Debit MONEY,
		Credit MONEY
	)

	INSERT INTO @view_journal SELECT numAccountId,vcAccountCode,datEntry_Date,Debit,Credit FROM view_journal WHERE numDomainId = @numDomainID AND (numAccountClass=@numAccountClass OR @numAccountClass=0);
	
	DECLARE @PLCHARTID NUMERIC(8)
	SELECT @PLCHARTID = numAccountId FROM dbo.Chart_Of_Accounts WHERE numDomainId=@numDomainID AND bitProfitLoss=1

	DECLARE @PLOPENING AS MONEY
	SELECT @PLOPENING = ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM @view_journal VJ WHERE numAccountId=@PLCHARTID

	;WITH DirectReport (ParentId, numAccountTypeID, vcAccountType,vcAccountCode, LEVEL,Struc)
	AS
	(
		SELECT 
			CAST(0 AS NUMERIC(18)) AS ParentId, 
			[ATD].[numAccountTypeID], 
			[ATD].[vcAccountType],
			[ATD].[vcAccountCode],
			1 AS LEVEL, 
			CAST(CAST([ATD].[vcAccountCode] AS VARCHAR) AS VARCHAR(100))  AS Struc
		FROM 
			[dbo].[AccountTypeDetail] AS ATD
		WHERE 
			[ATD].[numDomainID]=@numDomainId 
			AND [ATD].[vcAccountCode] IN ('0101','0102','0105')
		UNION ALL
		SELECT 
			ATD.[numParentID] AS ParentId, 
			[ATD].[numAccountTypeID], 
			[ATD].[vcAccountType],
			[ATD].[vcAccountCode],
			LEVEL + 1, 
			CAST(d.Struc + '#' + cast([ATD].[vcAccountCode] AS VARCHAR) AS VARCHAR(100))  AS Struc
		FROM 
			[dbo].[AccountTypeDetail] AS ATD 
		JOIN 
			[DirectReport] D 
		ON 
			D.[numAccountTypeID] = [ATD].[numParentID]
		WHERE 
			[ATD].[numDomainID]=@numDomainId 
	),
	DirectReport1 (ParentId, numAccountTypeID, vcAccountType,vcAccountCode, LEVEL,numAccountId,Struc, bitProfitLoss)
	AS
	(
		SELECT 
			ParentId, 
			numAccountTypeID, 
			vcAccountType,
			vcAccountCode, 
			LEVEL,
			CAST(NULL AS NUMERIC(18)),
			Struc,
			CAST(0 AS BIT)
		FROM 
			DirectReport 
		UNION ALL
		SELECT 
			COA.numParntAcntTypeId AS ParentId,
			CAST(NULL AS NUMERIC(18)), 
			[vcAccountName],
			COA.[vcAccountCode],
			LEVEL + 1,
			[COA].[numAccountId], 
			CAST(d.Struc + '#' + CAST(COA.[vcAccountCode] AS VARCHAR) AS VARCHAR(100)) AS Struc,
			COA.bitProfitLoss
		FROM 
			[dbo].[Chart_of_Accounts] AS COA 
		JOIN 
			[DirectReport1] D 
		ON 
			D.[numAccountTypeID] = COA.[numParntAcntTypeId]
		WHERE 
			[COA].[numDomainID]=@numDomainId 
			AND ISNULL(COA.bitActive,0) = 1
	)

  
	SELECT 
		ParentId, 
		numAccountTypeID, 
		vcAccountType, 
		LEVEL, 
		vcAccountCode,
		numAccountId,
		Struc,
		bitProfitLoss
	INTO 
		#tempDirectReport
	FROM 
		DirectReport1

	SELECT 
		COA.ParentId, 
		COA.numAccountTypeID, 
		COA.vcAccountType, 
		COA.LEVEL, 
		COA.vcAccountCode, 
		COA.numAccountId, 
		COA.Struc,
		(CASE 
			WHEN COA.vcAccountCode LIKE '0105%' AND COA.numAccountId <> @PLCHARTID THEN (ISNULL(Debit,0) * (-1) - ISNULL(Credit,0) * (-1)) 
			ELSE ISNULL(Debit,0) - ISNULL(Credit,0) 
		END) AS Amount,
		V.datEntry_Date
	INTO 
		#tempViewData
	FROM 
		#tempDirectReport COA 
	LEFT JOIN 
		#VIEW_JOURNALBS V 
	ON  
		V.COAvcAccountCode like COA.vcAccountCode + '%' 
		AND datEntry_Date <= @dtToDate
	WHERE 
		COA.[numAccountId] IS NOT NULL
	
	DECLARE @columns VARCHAR(8000);--SET @columns = '';
	DECLARE @SUMColumns VARCHAR(8000);--SET @SUMColumns = '';
	DECLARE @PivotColumns VARCHAR(8000);--SET @PivotColumns = '';
	DECLARE @Select VARCHAR(8000)
	DECLARE @Where VARCHAR(8000)
	SET @Select = 'SELECT ParentId, numAccountTypeID, vcAccountType, LEVEL, vcAccountCode, numAccountId,Struc'


	DECLARE @sql VARCHAR(MAX) = ''

	IF @ReportColumn = 'Year'
	BEGIN
		CREATE TABLE #tempYearMonth
		(
			intYear INT,
			intMonth INT,
			MonthYear VARCHAR(50),
			StartDate DATETIME,
			EndDate DATETIME,
			ProfitLossAmount MONEY
		)

		;WITH CTE AS 
		(
			SELECT
				YEAR(@dtFromDate) AS 'yr',
				MONTH(@dtFromDate) AS 'mm',
				DATENAME(mm, @dtFromDate) AS 'mon',
				(DATENAME(mm, @dtFromDate) + '_' + CAST(YEAR(@dtFromDate) AS VARCHAR)) AS MonthYear,
				CAST(DATEFROMPARTS(YEAR(@dtFromDate),MONTH(@dtFromDate),1) AS DATETIME) dtStartDate,
				DATEADD(MS,-2,CAST(DATEADD(MONTH,1,DATEFROMPARTS(YEAR(@dtFromDate),MONTH(@dtFromDate),1)) AS DATETIME)) dtEndDate,
				@dtFromDate 'new_date'
			UNION ALL
			SELECT
				YEAR(DATEADD(d,1,new_date)) AS 'yr',
				MONTH(DATEADD(d,1,new_date)) AS 'mm',
				DATENAME(mm, DATEADD(d,1,new_date)) AS 'mon',
				(DATENAME(mm, DATEADD(d,1,new_date)) + '_' + CAST(YEAR(DATEADD(d,1,new_date)) AS VARCHAR)) AS MonthYear,
				CAST(DATEFROMPARTS(YEAR(DATEADD(d,1,new_date)),MONTH(DATEADD(d,1,new_date)),1) AS DATETIME) dtStartDate,
				DATEADD(MS,-2,CAST(DATEADD(MONTH,1,DATEFROMPARTS(YEAR(DATEADD(d,1,new_date)),MONTH(DATEADD(d,1,new_date)),1)) AS DATETIME)) dtEndDate,
				DATEADD(d,1,new_date) 'new_date'
			FROM CTE
			WHERE DATEADD(d,1,new_date) < @dtToDate
		)
	
		INSERT INTO 
			#tempYearMonth
		SELECT 
			yr,mm,MonthYear,dtStartDate,dtEndDate,0
		FROM 
			CTE
		GROUP BY 
			yr, mm, MonthYear, dtStartDate, dtEndDate
		ORDER BY 
			yr, mm
		OPTION (MAXRECURSION 5000)

		INSERT INTO #tempYearMonth VALUES (5000,5000,'Total',@dtFromDate,@dtToDate,0)

		UPDATE 
			#tempYearMonth
		SET
			EndDate = (CASE WHEN EndDate < @dtToDate THEN EndDate ELSE @dtToDate END)

		UPDATE 
			#tempYearMonth
		SET
			ProfitLossAmount = (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM @view_journal VJ WHERE vcAccountCode LIKE '0103%' AND datEntry_Date between StartDate and EndDate)
			+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM @view_journal VJ WHERE vcAccountCode LIKE '0104%' AND datEntry_Date between StartDate and EndDate)
			+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM @view_journal VJ WHERE vcAccountCode LIKE '0106%' AND datEntry_Date between StartDate and EndDate)
			+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM @view_journal VJ WHERE (vcAccountCode LIKE '0103%' OR vcAccountCode LIKE '0104%' OR vcAccountCode LIKE '0106%') AND datEntry_Date <=  DATEADD(Minute,-1,StartDate))
			+ @PLOPENING

		SELECT @columns = STUFF((SELECT ', ISNULL(' + MonthYear  + ',0) AS [' + REPLACE(MonthYear,'_',' ') + ']' FROM #tempYearMonth ORDER BY intYear,intMonth FOR XML PATH('')), 1, 2, ''); 
		SELECT @PivotColumns = STUFF((SELECT ', ' + MonthYear FROM #tempYearMonth ORDER BY intYear,intMonth FOR XML PATH('')), 1, 2, ''); 

		SELECT TOP 1 @PLAccountStruc= '#' + Struc + '#' FROM #tempDirectReport WHERe bitProfitLoss = 1

		SET @sql = 'SELECT
						ParentId,
						numAccountId,
						numAccountTypeID,
						vcAccountType,
						LEVEL,
						vcAccountCode,
						Struc,
						[Type],' + @columns + '
					FROM
					(
						SELECT 
							#tempDirectReport.ParentId,
							#tempDirectReport.numAccountId,
							#tempDirectReport.numAccountTypeID,
							#tempDirectReport.vcAccountType,
							#tempDirectReport.LEVEL,
							#tempDirectReport.vcAccountCode,
							(''#'' + #tempDirectReport.Struc + ''#'') AS Struc,
							(CASE WHEN ISNULL(#tempDirectReport.numAccountId,0) > 0 THEN 1 ELSE 2 END) [Type],
							(CASE 
								WHEN bitProfitLoss = 1 
								THEN Period.ProfitLossAmount
								ELSE
									(CASE WHEN CHARINDEX((''#'' + #tempDirectReport.Struc + ''#''),''' + CAST(@PLAccountStruc AS VARCHAR) + ''') > 0 THEN Period.ProfitLossAmount ELSE 0 END) +
									ISNULL((SELECT sum(ISNULL(Debit,0)) FROM #VIEW_JOURNALBS VJ WHERE VJ.COAvcAccountCode like #tempDirectReport.vcAccountCode + ''%'' AND datEntry_Date <= Period.EndDate AND ISNULL(VJ.numAccountId,0) <> ' + CAST(@PLCHARTID AS VARCHAR(20))  + '),0) * (CASE WHEN #tempDirectReport.vcAccountCode LIKE ''0102%'' OR (#tempDirectReport.vcAccountCode LIKE ''0105%'' AND ISNULL(numAccountId,0) <> ' + CAST(@PLCHARTID AS VARCHAR) + ') THEN -1 ELSE 1 END) -
									ISNULL((SELECT sum(ISNULL(Credit,0)) FROM #VIEW_JOURNALBS VJ WHERE VJ.COAvcAccountCode like #tempDirectReport.vcAccountCode + ''%'' AND datEntry_Date <= Period.EndDate AND ISNULL(VJ.numAccountId,0) <> ' + CAST(@PLCHARTID AS VARCHAR(20))  + '),0) * (CASE WHEN #tempDirectReport.vcAccountCode LIKE ''0102%'' OR (#tempDirectReport.vcAccountCode LIKE ''0105%'' AND ISNULL(numAccountId,0) <> ' + CAST(@PLCHARTID AS VARCHAR) + ') THEN -1 ELSE 1 END)
							END) AS Amount,
							Period.MonthYear
						FROM 
							#tempDirectReport
						OUTER APPLY
						(
							SELECT
								MonthYear,
								StartDate,
								EndDate,
								ProfitLossAmount
							FROM
								#tempYearMonth
						) AS Period
					) AS SourceTable
					pivot
					(
						SUM(AMOUNT)
						FOR [MonthYear] IN ( ' + @pivotcolumns + ' )
					) AS P ORDER BY Struc, [Type] desc'

		EXECUTE (@sql)

		DROP TABLE #tempYearMonth
	END
	Else IF @ReportColumn = 'Quarter'
	BEGIN
		CREATE TABLE #TempYearMonthQuarter
		(
			intYear INT,
			intQuarter INT,
			MonthYear VARCHAR(50),
			StartDate DATETIME,
			EndDate DATETIME,
			ProfitLossAmount MONEY
		)

		;WITH CTE AS 
		(
			SELECT
				dbo.GetFiscalyear(@dtFromDate,@numDomainId) AS 'yr',
				dbo.GetFiscalQuarter(@dtFromDate,@numDomainId) AS 'qq',
				('Q' + cast(dbo.GetFiscalQuarter(@dtFromDate,@numDomainId) AS VARCHAR) + '_' + cast(dbo.GetFiscalyear(@dtFromDate,@numDomainId) AS VARCHAR)) AS MonthYear,
				CASE dbo.GetFiscalQuarter(@dtFromDate,@numDomainId) 
				WHEN 1 THEN dateadd(month,0,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId))
				WHEN 2 THEN dateadd(month,3,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)) 
				WHEN 3 THEN dateadd(month,6,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId))
				WHEN 4 THEN dateadd(month,9,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)) 
				END AS dtStartDate,
				CASE dbo.GetFiscalQuarter(@dtFromDate,@numDomainId) 
				WHEN 1 THEN dateadd(MS,-3,dateadd(month,3,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)))
				WHEN 2 THEN dateadd(MS,-3,dateadd(month,6,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)))
				WHEN 3 THEN dateadd(MS,-3,dateadd(month,9,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)))
				WHEN 4 THEN dateadd(MS,-3,dateadd(month,12,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)))
				END AS dtEndDate,
				@dtFromDate 'new_date'
			UNION ALL
			SELECT
				dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId) AS 'yr',
				dbo.GetFiscalQuarter(DATEADD(d,1,new_date),@numDomainId) AS 'qq',
				('Q' + cast(dbo.GetFiscalQuarter(DATEADD(d,1,new_date),@numDomainId) AS VARCHAR) + '_' + cast(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId) AS VARCHAR)) AS MonthYear,
				CASE dbo.GetFiscalQuarter(DATEADD(d,1,new_date),@numDomainId) 
				WHEN 1 THEN dateadd(month,0,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId))
				WHEN 2 THEN dateadd(month,3,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)) 
				WHEN 3 THEN dateadd(month,6,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId))
				WHEN 4 THEN dateadd(month,9,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)) 
				END AS dtStartDate,
				CASE dbo.GetFiscalQuarter(DATEADD(d,1,new_date),@numDomainId) 
				WHEN 1 THEN dateadd(MS,-3,dateadd(month,3,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)))
				WHEN 2 THEN dateadd(MS,-3,dateadd(month,6,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)))
				WHEN 3 THEN dateadd(MS,-3,dateadd(month,9,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)))
				WHEN 4 THEN dateadd(MS,-3,dateadd(month,12,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)))
				END AS dtEndDate,
				DATEADD(d,1,new_date) 'new_date'
			FROM CTE
			WHERE DATEADD(d,1,new_date) < @dtToDate
		)
	
		INSERT INTO 
			#TempYearMonthQuarter 		
		SELECT 
			yr,qq,MonthYear,dtStartDate,dtEndDate,0
		FROM 
			CTE
		GROUP BY 
			yr, qq, MonthYear, dtStartDate, dtEndDate
		ORDER BY 
			yr, qq
		OPTION
			(MAXRECURSION 5000)

		INSERT INTO #TempYearMonthQuarter VALUES (5000,5000,'Total',@dtFromDate,@dtToDate,0)

		UPDATE 
			#TempYearMonthQuarter
		SET
			EndDate = (CASE WHEN EndDate < @dtToDate THEN EndDate ELSE @dtToDate END)

		UPDATE 
			#TempYearMonthQuarter
		SET
			ProfitLossAmount = (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM @view_journal VJ WHERE vcAccountCode LIKE '0103%' AND datEntry_Date between StartDate and EndDate)
			+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM @view_journal VJ WHERE vcAccountCode LIKE '0104%' AND datEntry_Date between StartDate and EndDate)
			+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM @view_journal VJ WHERE vcAccountCode LIKE '0106%' AND datEntry_Date between StartDate and EndDate)
			+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM @view_journal VJ WHERE (vcAccountCode LIKE '0103%' OR vcAccountCode LIKE '0104%' OR vcAccountCode LIKE '0106%') AND datEntry_Date <=  DATEADD(Minute,-1,StartDate))
			+ @PLOPENING

		SELECT @columns = STUFF((SELECT ', ISNULL(' + MonthYear  + ',0) AS [' + REPLACE(MonthYear,'_',' ') + ']' FROM #TempYearMonthQuarter ORDER BY intYear,intQuarter FOR XML PATH('')), 1, 2, ''); 
		SELECT @PivotColumns = STUFF((SELECT ', ' + MonthYear FROM #TempYearMonthQuarter ORDER BY intYear,intQuarter FOR XML PATH('')), 1, 2, ''); 

		SELECT TOP 1 @PLAccountStruc= '#' + Struc + '#' FROM #tempDirectReport WHERe bitProfitLoss = 1

		SET @sql = 'SELECT
						ParentId,
						numAccountId,
						numAccountTypeID,
						vcAccountType,
						LEVEL,
						vcAccountCode,
						Struc,
						[Type],' + @columns + '
					FROM
					(
						SELECT 
							#tempDirectReport.ParentId,
							#tempDirectReport.numAccountId,
							#tempDirectReport.numAccountTypeID,
							#tempDirectReport.vcAccountType,
							#tempDirectReport.LEVEL,
							#tempDirectReport.vcAccountCode,
							(''#'' + #tempDirectReport.Struc + ''#'') AS Struc,
							(CASE WHEN ISNULL(#tempDirectReport.numAccountId,0) > 0 THEN 1 ELSE 2 END) [Type],
							(CASE 
								WHEN bitProfitLoss = 1 
								THEN Period.ProfitLossAmount
								ELSE
									(CASE WHEN CHARINDEX((''#'' + #tempDirectReport.Struc + ''#''),''' + CAST(@PLAccountStruc AS VARCHAR) + ''') > 0 THEN Period.ProfitLossAmount ELSE 0 END) +
									ISNULL((SELECT sum(ISNULL(Debit,0)) FROM #VIEW_JOURNALBS VJ WHERE VJ.COAvcAccountCode like #tempDirectReport.vcAccountCode + ''%'' AND datEntry_Date <= Period.EndDate AND ISNULL(VJ.numAccountId,0) <> ' + CAST(@PLCHARTID AS VARCHAR(20))  + '),0) * (CASE WHEN #tempDirectReport.vcAccountCode LIKE ''0102%'' OR (#tempDirectReport.vcAccountCode LIKE ''0105%'' AND ISNULL(numAccountId,0) <> ' + CAST(@PLCHARTID AS VARCHAR) + ') THEN -1 ELSE 1 END) -
									ISNULL((SELECT sum(ISNULL(Credit,0)) FROM #VIEW_JOURNALBS VJ WHERE VJ.COAvcAccountCode like #tempDirectReport.vcAccountCode + ''%'' AND datEntry_Date <= Period.EndDate AND ISNULL(VJ.numAccountId,0) <> ' + CAST(@PLCHARTID AS VARCHAR(20)) + '),0) * (CASE WHEN #tempDirectReport.vcAccountCode LIKE ''0102%'' OR (#tempDirectReport.vcAccountCode LIKE ''0105%'' AND ISNULL(numAccountId,0) <> ' + CAST(@PLCHARTID AS VARCHAR) + ') THEN -1 ELSE 1 END)
							END) AS Amount,							
							Period.MonthYear
						FROM 
							#tempDirectReport
						OUTER APPLY
						(
							SELECT
								MonthYear,
								StartDate,
								EndDate,
								ProfitLossAmount
							FROM
								#TempYearMonthQuarter
						) AS Period
					) AS SourceTable
					pivot
					(
						SUM(AMOUNT)
						FOR [MonthYear] IN ( ' + @pivotcolumns + ' )
					) AS P ORDER BY Struc, [Type] desc'

		PRINT @sql
		EXECUTE (@sql)

		DROP TABLE #TempYearMonthQuarter
	END
	ELSE
	BEGIN
		DECLARE @ProifitLossAmount AS MONEY = 0

		SET @ProifitLossAmount = (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM @view_journal VJ WHERE vcAccountCode  LIKE '0103%' AND datEntry_Date between @dtFromDate and @dtToDate)
								+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM @view_journal VJ WHERE vcAccountCode  LIKE '0104%' AND datEntry_Date between @dtFromDate and @dtToDate)
								+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM @view_journal VJ WHERE vcAccountCode  LIKE '0106%' AND datEntry_Date between @dtFromDate and @dtToDate)
								+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM @view_journal VJ WHERE (vcAccountCode LIKE '0103%' OR vcAccountCode LIKE '0104%' OR vcAccountCode LIKE '0106%') AND datEntry_Date <=  DATEADD(MILLISECOND,-3,@dtFromDate))
								+ @PLOPENING		

		SELECT TOP 1 @PLAccountStruc= '#' + Struc + '#' FROM #tempDirectReport WHERe bitProfitLoss = 1
		
		SELECT 
			#tempDirectReport.ParentId,
			#tempDirectReport.numAccountId,
			#tempDirectReport.numAccountTypeID,
			#tempDirectReport.vcAccountType,
			#tempDirectReport.LEVEL,
			#tempDirectReport.vcAccountCode,
			('#' + #tempDirectReport.Struc + '#') AS Struc,
			(CASE WHEN ISNULL(#tempDirectReport.numAccountId,0) > 0 THEN 1 ELSE 2 END) [Type],
			(CASE 
				WHEN bitProfitLoss = 1 
				THEN @ProifitLossAmount
				ELSE
					(CASE WHEN CHARINDEX(('#' + #tempDirectReport.Struc + '#'),@PLAccountStruc) > 0 THEN @ProifitLossAmount ELSE 0 END) +
					ISNULL((SELECT ISNULL(sum(Debit),0) FROM #VIEW_JOURNALBS VJ WHERE VJ.COAvcAccountCode like #tempDirectReport.vcAccountCode + '%' AND datEntry_Date <= @dtToDate AND ISNULL(VJ.numAccountId,0) <> @PLCHARTID),0) * (CASE WHEN #tempDirectReport.vcAccountCode LIKE '0102%' OR (#tempDirectReport.vcAccountCode LIKE '0105%' AND ISNULL(numAccountId,0) <> @PLCHARTID) THEN -1 ELSE 1 END) -
					ISNULL((SELECT ISNULL(sum(Credit),0) FROM #VIEW_JOURNALBS VJ WHERE VJ.COAvcAccountCode like #tempDirectReport.vcAccountCode + '%' AND datEntry_Date <= @dtToDate AND ISNULL(VJ.numAccountId,0) <> @PLCHARTID),0) * (CASE WHEN #tempDirectReport.vcAccountCode LIKE '0102%' OR (#tempDirectReport.vcAccountCode LIKE '0105%' AND ISNULL(numAccountId,0) <> @PLCHARTID) THEN -1 ELSE 1 END) 
			END) AS Amount
		FROM 
			#tempDirectReport
		ORDER BY 
			Struc, [Type] desc
	END

	DROP TABLE #tempViewData 
	DROP TABLE #tempDirectReport
	DROP TABLE #VIEW_JOURNALBS
END

/****** Object:  StoredProcedure [dbo].[usp_GetContactDTlPL]    Script Date: 07/26/2008 16:16:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By                                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcontactdtlpl')
DROP PROCEDURE usp_getcontactdtlpl
GO
CREATE PROCEDURE [dbo].[usp_GetContactDTlPL]                                                              
@numContactID as numeric(9)=0  ,    
@numDomainID as numeric(9)=0   ,  
@ClientTimeZoneOffset as int                                    
--                                                            
AS                                                              
BEGIN                                                              
                                                              
  SELECT                                      
 A.numContactId,            
A.vcFirstName,           
A.vcLastName,                                                                   
 D.numDivisionID,             
 C.numCompanyId,            
 C.vcCompanyName,             
 D.vcDivisionName,             
 D.numDomainID,        
 tintCRMType,            
 A.numPhone,
 A.numPhoneExtension,
 A.vcEmail,  
 A.numTeam, 
 A.numTeam as vcTeam,         
[dbo].[GetListIemName](A.numTeam) as vcTeamName,                                                             
 A.vcFax,   
 A.numContactType as vcContactType,
 A.numContactType,         
[dbo].[GetListIemName](A.numContactType) as vcContactTypeName,                         
 A.charSex,             
 A.bintDOB,            
 dbo.GetAge(A.bintDOB, getutcdate()) as Age,                                                               
 [dbo].[GetListIemName]( A.vcPosition) as vcPositionName, 
 A.vcPosition,            
 A.txtNotes,             
 A.numCreatedBy,                                                              
 A.numCell,            
 A.NumHomePhone,            
 A.vcAsstFirstName,
 A.vcAsstLastName,            
 A.numAsstPhone,
 A.numAsstExtn,                                                              
 A.vcAsstEmail,            
 A.charSex, 
 A.vcLinkedinId,
  A.vcLinkedinUrl,
 case when A.charSex='M' then 'Male' when A.charSex='F' then 'Female' else  '-' end as charSexName,            
[dbo].[GetListIemName]( A.vcDepartment) as vcDepartmentName,  
vcDepartment,                                                             
--    case when AddC.vcPStreet is null then '' when AddC.vcPStreet='' then '' else AddC.vcPStreet + ',' end +             
-- case when AddC.vcPCity is null then '' when AddC.vcPCity='' then ''  else AddC.vcPCity end +             
-- case when  AddC.vcPPostalCode is null then '' when  AddC.vcPPostalCode='' then '' else ','+ AddC.vcPPostalCode end +              
-- case when dbo.fn_GetState(AddC.vcPState) is null then '' else  ','+ dbo.fn_GetState(AddC.vcPState) end +              
-- case when dbo.fn_GetListName(AddC.vcPCountry,0) ='' then '' else ',' + dbo.fn_GetListName(AddC.vcPCountry,0) end      
 dbo.getContactAddress(A.numContactId) as [Address],            
-- AddC.vcPState,            
-- AddC.vcPCountry,            
-- AddC.vcContactLocation,                                    
--  AddC.vcStreet,                                                               
--    AddC.vcCity,             
-- AddC.vcState,             
-- AddC.intPostalCode,                                                               
--  AddC.vcCountry,                                                              
    A.bitOptOut,                                                                         
 dbo.fn_GetContactName(a.numManagerId) as ManagerName,
 a.numManagerId   as   Manager,
[dbo].[GetListIemName](A.vcCategory) as vcCategoryName  ,     
 A.vcCategory,        
 dbo.fn_GetContactName(A.numCreatedBy)+' '+ convert(varchar(20),dateadd(minute,-@ClientTimeZoneOffset,A.bintCreatedDate)) CreatedBy,                                      
 dbo.fn_GetContactName(A.numModifiedBy)+' '+ convert(varchar(20),dateadd(minute,-@ClientTimeZoneOffset,A.bintModifiedDate)) ModifiedBy,            
 vcTitle,            
 vcAltEmail,                                                          
 dbo.fn_GetContactName(A.numRecOwner) as RecordOwner,
 A.numEmpStatus,                  
[dbo].[GetListIemName]( A.numEmpStatus) as numEmpStatusName,                                                          
(select  count(*) from GenericDocuments   where numRecID=A.numContactId and  vcDocumentSection='C') as DocumentCount,                          
(SELECT count(*)from CompanyAssociations where numDivisionID=A.numContactId and bitDeleted=0 ) as AssociateCountFrom,                            
(SELECT count(*)from CompanyAssociations where numAssociateFromDivisionID=A.numContactId and bitDeleted=0 ) as AssociateCountTo,
 ISNULL((SELECT vcECampName FROM [ECampaign] WHERE numECampaignID = A.numECampaignID),'') vcECampName,
 ISNULL(A.numECampaignID,0) numECampaignID,
 ISNULL( (SELECT MAX([numConEmailCampID]) FROM [ConECampaign] WHERE [numContactID] = A.numContactID AND [numECampaignID]= A.numECampaignID AND ISNULL(bitEngaged,0)=1),0) numConEmailCampID,
 ISNULL(A.bitPrimaryContact,0) AS bitPrimaryContact
 FROM AdditionalContactsInformation A INNER JOIN                                                              
 DivisionMaster D ON A.numDivisionId = D.numDivisionID INNER JOIN                                                              
 CompanyInfo C ON D.numCompanyID = C.numCompanyId                                    
-- left join ContactAddress AddC on A.numContactId=AddC.numContactId                                  
 left join UserMaster U on U.numUserDetailId=A.numContactId                                                             
 WHERE A.numContactId = @numContactID and A.numDomainID=@numDomainID                                                     
END
GO
/****** Object:  StoredProcedure [dbo].[Usp_getCorrespondance]    Script Date: 07/26/2008 16:17:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcorrespondance')
DROP PROCEDURE usp_getcorrespondance
GO
CREATE PROCEDURE [dbo].[Usp_getCorrespondance]                          
@FromDate as datetime,                          
@toDate as datetime,                          
@numContactId as numeric,                          
@vcMessageFrom as varchar(200)='' ,                        
@tintSortOrder as numeric,                        
@SeachKeyword as varchar(100),                        
@CurrentPage as numeric,                        
@PageSize as numeric,                        
@TotRecs as numeric output,                        
@columnName as varchar(100),                        
@columnSortOrder as varchar(50),                        
@filter as numeric,                        
@numdivisionId as numeric =0,              
@numDomainID as numeric(9)=0,              
@numCaseID as numeric(9)=0,              
@ClientTimeZoneOffset INT,
@numOpenRecordID NUMERIC=0,
@tintMode TINYINT=0,
@bitOpenCommu BIT=0
as 

set @vcMessageFrom=replace(@vcMessageFrom,'''','|')
                         
if @filter>0 set @tintSortOrder=  @filter
               
if ((@filter <=2 and @tintSortOrder<=2) or (@tintSortOrder>2 and @filter>2))                 
begin  
                                                                      
 declare @strSql as varchar(MAX)       
                                  
  declare @firstRec as integer                                                        
  declare @lastRec as integer                                                        
  set @firstRec= (@CurrentPage-1) * @PageSize                                                        
  set @lastRec= (@CurrentPage*@PageSize+1)                 
  set @strSql=''                
 if (@filter <=2 and @tintSortOrder<=2 and @bitOpenCommu=0)                 
 begin     
 declare @strCondition as varchar(MAX);
 declare @vcContactEmail as varchar(MAX);
              
  if @numdivisionId =0 and  @vcMessageFrom <> '' 
 BEGIN 
	If @filter = 1 --ReceivedMessages
		set  @strCondition=' and (vcFrom like ''%'+ @vcMessageFrom+'%'')  ' 
	ELSE IF @filter=2 --SentMessages
		set  @strCondition=' and (vcTo like ''%'+ @vcMessageFrom+'%'')  ' 
	ELSE
		set  @strCondition=' and (vcFrom like ''%'+ @vcMessageFrom+'%'' or vcTo like ''%'+ @vcMessageFrom+'%'')  ' 
 END
  else if @numdivisionId <>0 
 BEGIN

 /* This approach does not work as numEmailId stores who sent email, id of sender
  set @strCondition=' and HDR.numEmailID in (
  SELECT numEmailID FROM dbo.EmailMaster WHERE numDomainID= ' + CONVERT(VARCHAR(15),@numDomainID) +' AND numContactID 
  IN (SELECT numcontactid FROM dbo.AdditionalContactsInformation WHERE numDivisionID = ' + convert(varchar(15),@numDivisionID) + ' AND numDomainID = ' + CONVERT(VARCHAR(15),@numDomainID) +' ))';
*/
-- bug fix id 1591,1628 
  DECLARE @TnumContactID NUMERIC(9);set @strCondition=''

  select TOP 1 @TnumContactID=numContactID,@vcContactEmail=replace(vcEmail,'''','''''') from AdditionalContactsInformation where numdivisionid=@numdivisionId and len(vcEmail)>2
  ORDER BY numContactID ASC
  
  WHILE @TnumContactID>0
  BEGIN
   if len(@strCondition)>0
    SET @strCondition=@strCondition + ' or '

	If @filter = 1 --ReceivedMessages
		set  @strCondition = @strCondition + ' (vcFrom like ''%'+ @vcContactEmail+'%'')  ' 
	ELSE IF @filter=2 --SentMessages
		set  @strCondition= @strCondition + ' (vcTo like ''%'+ @vcContactEmail+'%'')  ' 
	ELSE
		set  @strCondition= @strCondition + ' (vcFrom like ''%'+ @vcContactEmail+'%'' or vcTo like ''%'+ @vcContactEmail+'%'')  '

   select TOP 1 @TnumContactID=numContactID,@vcContactEmail=vcEmail from AdditionalContactsInformation where numdivisionid=@numdivisionId and len(vcEmail)>2 and numContactID>@TnumContactID
   ORDER BY numContactID ASC

   IF @@rowcount = 0 SET @TnumContactID = 0 
  END
 
  if len(@strCondition)>0
   SET @strCondition= 'and (' + @strCondition + ')'
  else
   SET @strCondition=' and (1=0)' 
  --set @strCondition=' and DTL.numEmailId  in (select numEmailId from EmailMaster where vcEmailID in (select vcEmail  from AdditionalContactsInformation where numdivisionid='''+ convert(varchar(15),@numdivisionId)+''' ))    '          
  END
  else if @numContactId>0 
 BEGIN
  select @vcContactEmail=isnull(replace(vcEmail,'''',''''''),'') from AdditionalContactsInformation where numContactID=@numContactId and len(vcEmail)>2

  if len(@vcContactEmail)>0
	If @filter = 1 --ReceivedMessages
		set  @strCondition=' and (vcFrom like ''%'+ @vcContactEmail +'%'')  ' 
	ELSE IF @filter=2 --SentMessages
		set  @strCondition=' and (vcTo like ''%'+ @vcContactEmail+'%'')  ' 
	ELSE
		set  @strCondition=' and (vcFrom like ''%'+ @vcContactEmail+'%'' or vcTo like ''%'+ @vcContactEmail+'%'')' 
  else
   SET @strCondition=' and (1=0)'
 END
 else 
  SET @strCondition=''
    
  set @strSql= '                        
  SELECT distinct(HDR.numEmailHstrID),convert(varchar(15),HDR.numEmailHstrID)+''~1~0''+convert(varchar(15),HDR.numUserCntId) as DelData,HDR.bintCreatedOn,
  dbo.FormatedDateTimeFromDate(DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+', bintCreatedOn),'+convert(varchar(15),@numDomainID)+') as [date],                
  vcSubject as [Subject],                          
  ''Email'' as [type] ,                          
  '''' as phone,'''' as assignedto ,                          
  ''0'' as caseid,                             
  null as vcCasenumber,                                 
  ''0''as caseTimeid,                                
  ''0''as caseExpid ,                          
  hdr.tinttype ,HDR.bintCreatedOn as dtCreatedDate,vcFrom + '','' + vcTo as [From] ,0 as bitClosedflag,ISNULL(HDR.bitHasAttachments,0) bitHasAttachments,
 CASE WHEN LEN(Cast(vcBodyText AS VARCHAR(1000)))>150 THEN SUBSTRING(vcBodyText,0,150) + ''...'' ELSE
   Cast(vcBodyText AS VARCHAR(1000)) END AS vcBody                     
  from EmailHistory HDR                                 
  LEFT JOIN [Correspondence] CR ON CR.numEmailHistoryID = HDR.numEmailHstrID '                    
   set  @strSql=@strSql +' where ISNULL(HDR.bitInvisible,0) = 0 AND HDR.numDomainID='+convert(varchar(15),@numDomainID)+' and  (bintCreatedOn between '''+ convert(varchar(30),@FromDate)+''' and '''+ convert(varchar(30),@ToDate)+''') '
   + ' AND (CR.numOpenRecordID = '+ convert(varchar(15),@numOpenRecordID) +' OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
  
 set  @strSql=@strSql+ @strCondition
--  if @numdivisionId =0 and  @vcMessageFrom <> '' set  @strSql=@strSql+ 'and (vcFrom like ''%'+ @vcMessageFrom+'%'' or vcTo like ''%'+ @vcMessageFrom+'%'')  '                      
--  else if @numdivisionId <>0 
-- BEGIN
--  set @strSql=@strSql+ 'and DTL.numEmailId  in (select numEmailId from EmailMaster where vcEmailID in (select vcEmail  from AdditionalContactsInformation where numdivisionid='''+ convert(varchar(15),@numdivisionId)+''' ))    '          
--  END
--  else if @numContactId>0 
-- BEGIN
--  declare @vcContactEmail as varchar(100);
--  select @vcContactEmail=vcEmail from AdditionalContactsInformation where numContactID=@numContactId
--
--  set  @strSql=@strSql+ 'and (vcFrom like ''%'+ @vcContactEmail+'%'' or vcTo like ''%'+ @vcContactEmail+'%'')    ' 
-- END

  IF @tintMode=1 SET @strSql=@strSql+ ' and  (CR.tintCorrType IN(1,3) OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
  IF @tintMode=2 SET @strSql=@strSql+ ' and  (CR.tintCorrType IN(2,4) OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
  IF @tintMode=5 SET @strSql=@strSql+ ' and  (CR.tintCorrType IN(5) OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
  IF @tintMode=6 SET @strSql=@strSql+ ' and  (CR.tintCorrType IN(6) OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
  
  if @filter =0 set  @strSql=@strSql+ ' and (hdr.tinttype=1 or hdr.tinttype=2 or  hdr.tinttype=5)'                  
  else if @filter =1 OR @filter =2 set  @strSql=@strSql+ ' and (hdr.tinttype=1 OR hdr.tinttype=2)'                                             
  if @filter > 2 set  @strSql=@strSql+ ' and hdr.tinttype=0'                 
                        
  if @tintSortOrder =0 and @SeachKeyword <> ''                 
  begin                
   set  @strSql=@strSql+ ' and (vcFrom like ''%'+ @SeachKeyword+'%'' or vcTo like ''%'+ @SeachKeyword+'%'' or vcsubject like ''%'+@SeachKeyword+'%'' or vcBody like ''%'+@SeachKeyword+'%'')'               
  end                
  if @tintSortOrder =1 and @SeachKeyword <> ''                 
  begin                
     set  @strSql=@strSql+ ' and (vcFrom like ''%'+ @SeachKeyword+'%'' or vcsubject like ''%'+@SeachKeyword+'%'' or  vcBody like ''%'+@SeachKeyword+'%'')'                 
  end                
  if @tintSortOrder =2 and @SeachKeyword <> ''                 
  begin                
     set  @strSql=@strSql+ ' and (vcTo like ''%'+ @SeachKeyword+'%'' or vcsubject like ''%'+@SeachKeyword+'%'' or  vcBody like ''%'+@SeachKeyword+'%'')'                 
  end                            
 end                
 if (@filter =0 and @tintSortOrder=0 and @bitOpenCommu=0) set  @strSql=@strSql+ ' union '                
                
 if ((@filter=0 or @filter>2) and (@tintSortOrder=0 or @tintSortOrder>2))                
 begin                
  set  @strSql=@strSql+ '                     
  SELECT c.numCommId as numEmailHstrID,convert(varchar(15),C.numCommId)+''~2~''+convert(varchar(15),C.numCreatedBy) as DelData,CASE WHEN c.bitTask = 973 THEN dtCreatedDate WHEN c.bitTask = 974 THEN dtCreatedDate WHEN c.bitTask = 971 THEN dtStartTime ELSE dtEndTime END AS bintCreatedOn,
  case when c.bitTask=973 then dbo.FormatedDateTimeFromDate(DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+', dtCreatedDate),'+convert(varchar(15),@numDomainID)+') 
  when c.bitTask=974 then dbo.FormatedDateTimeFromDate(DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+', dtEndTime),'+convert(varchar(15),@numDomainID)+') 
  when c.bitTask=971 THEN dbo.FormatedDateFromDate(DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+', dtEndTime),'+convert(varchar(15),@numDomainID)+') + '' '' + RIGHT(CONVERT(VARCHAR,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+', [dtStartTime]),100),7)  +'' - '' + RIGHT(CONVERT(VARCHAR,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+', [dtEndTime]),100),7)
  else  dbo.FormatedDateFromDate(DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+', dtEndTime),'+convert(varchar(15),@numDomainID)+') + '' '' + RIGHT(CONVERT(VARCHAR,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+', [dtStartTime]),100),7)  +'' - '' + RIGHT(CONVERT(VARCHAR,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+', [dtEndTime]),100),7)  END AS [date],
  convert(varchar(8000),textdetails) as [Subject],                                                                         
 dbo.fn_GetListItemName(c.bitTask)  AS [Type],                                                                 
  A1.vcFirstName +'' ''+ A1.vcLastName +'' ''+                                      
  case when A1.numPhone<>'''' then + A1.numPhone +case when A1.numPhoneExtension<>'''' then '' - '' + A1.numPhoneExtension else '''' end  else '''' end as [Phone],                        
  A2.vcFirstName+ '' ''+A2.vcLastName  as assignedto,                         
  c.caseid,                           
  (select top 1 vcCaseNumber from cases where cases.numcaseid= c.caseid )as vcCasenumber,                               
  isnull(caseTimeid,0)as caseTimeid,                              
  isnull(caseExpid,0)as caseExpid  ,                        
  1 as tinttype ,dtCreatedDate,'''' as [FROM]     ,isnull(C.bitClosedflag,0) as bitClosedflag,0 as bitHasAttachments,
'''' as vcBody
  from  Communication C                  
  JOIN AdditionalContactsInformation  A1                                           
  ON c.numContactId = A1.numContactId
  LEFT JOIN CommunicationLinkedOrganization clo ON C.numCommID=clo.numCommID                                                                             
  left join AdditionalContactsInformation A2 on A2.numContactID=c.numAssign
  left join UserMaster UM on A2.numContactid=UM.numUserDetailId and isnull(UM.bitActivateFlag,0)=1                                                                           
  left join listdetails on numActivity=numListID                           
  LEFT JOIN [Correspondence] CR ON CR.numCommID = C.numCommID
  where  C.numDomainID=' +convert(varchar(15),@numDomainID)                       
  + 'AND (numOpenRecordID = '+ convert(varchar(15),@numOpenRecordID) +' OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
  if (@numdivisionId =0 and @numContactId<>0) set  @strSql=@strSql+' and C.numContactId='+ convert(varchar(15),@numContactId)                        
  if @numdivisionId <> 0 set  @strSql=@strSql+' and  (C.numDivisionId='+ convert(varchar(15),@numdivisionId) + ' OR clo.numDivisionID='+ convert(varchar(15),@numdivisionId) + ') '             
  if @numCaseID>0  set  @strSql=@strSql+' and  C.CaseId='+ convert(varchar(15),@numCaseID)                       
  if @filter =0  
  BEGIN
	IF @tintMode <> 9
	BEGIN
		PRINT @tintMode
		SET @strSql=@strSql+ ' and dtStartTime  >= '''+ convert(varchar(30),@FromDate)+''' and dtStartTime  <= '''+ convert(varchar(30),@ToDate)  +''''
	END
  END
  else
    SET @strSql=@strSql+ ' and c.bitTask=' + CONVERT(varchar(12),@filter) + ' and ((dtStartTime  >= '''+ convert(varchar(30),@FromDate)+''' and dtStartTime  <= '''+ convert(varchar(30),@ToDate)  +''') OR (dtEndTime  >= '''+ convert(varchar(30),@FromDate)+''' and dtEndTime  <= '''+ convert(varchar(30),@ToDate)  +'''))'
  
  IF @tintMode=1 SET @strSql=@strSql+ ' and  (CR.tintCorrType IN(1,3) OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
  IF @tintMode=2 SET @strSql=@strSql+ ' and  (CR.tintCorrType IN(2,4) OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
  IF @tintMode=5 SET @strSql=@strSql+ ' and  (CR.tintCorrType IN(5) OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
  IF @tintMode=6 SET @strSql=@strSql+ ' and  (CR.tintCorrType IN(6) OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
                          
  IF @tintSortOrder =0 and @SeachKeyword <>'' 
	SET @strSql=@strSql+ ' and textdetails like ''%'+@SeachKeyword+'%'''
  ELSE
	IF ISNULL(@filter,0) = 0 
	BEGIN
		SET @strSql=@strSql+ ' and textdetails like ''%'+@SeachKeyword+'%'''
	END
	ELSE
	BEGIN
		SET @strSql=@strSql+ ' and c.bitTask=' + CONVERT(varchar(12),@filter) + ' and textdetails like ''%'+@SeachKeyword+'%'''                     
	END
  IF @bitOpenCommu=1  SET @strSql=@strSql+ ' and isnull(C.bitClosedflag,0)=0'
 end                
 set @strSql='With tblCorr as (SELECT ROW_NUMBER() OVER (ORDER BY '+@columnName+' '+ @columnSortOrder+') AS RowNumber,X.* from('+@strSql+')X)'                

 set @strSql=@strSql +'                       
  select  *  from tblCorr
  Where RowNumber > '+convert(varchar(10),@firstRec)+' and RowNumber < '+convert(varchar(10),@lastRec)+'                     
  union select 0,count(*),null,null,null,'','',null,null,null,null,null,null,null,null,null,null,0,0,null from tblCorr order by RowNumber'                 
                
-- set @strSql=@strSql +'                       
--  select  *,CASE WHEN [Type]=''Email'' THEN dbo.GetEmaillAdd(numEmailHstrID,4)+'', '' +dbo.GetEmaillAdd(numEmailHstrID,1) ELSE '''' END as [From] from tblCorr
--  Where RowNumber > '+convert(varchar(10),@firstRec)+' and RowNumber < '+convert(varchar(10),@lastRec)+'                     
--  union select 0,count(*),null,null,null,'','',null,null,null,null,null,null,null,null,null,null from tblCorr order by RowNumber'                 
                
 print @strSql                
 exec (@strSql)                
end                
else                
select 0,0
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetECommerceDetails')
DROP PROCEDURE USP_GetECommerceDetails
GO
CREATE PROCEDURE [dbo].[USP_GetECommerceDetails]      
@numDomainID as numeric(9)=0,
@numSiteID AS INTEGER = 0  
as                              
select                               
isnull(vcPaymentGateWay,'0') as vcPaymentGateWay,                
isnull(vcPGWManagerUId,'')as vcPGWManagerUId ,                
isnull(vcPGWManagerPassword,'') as vcPGWManagerPassword,              
isnull(bitShowInStock,0) as bitShowInStock,              
isnull(bitShowQOnHand,0) as bitShowQOnHand,        
--isnull(tintItemColumns,0) as tintItemColumns,
--isnull(numDefaultCategory,0) as numDefaultCategory,  
isnull([numDefaultWareHouseID],0) numDefaultWareHouseID,
isnull([numDefaultIncomeChartAcntId],0) numDefaultIncomeChartAcntId,
isnull([numDefaultAssetChartAcntId],0) numDefaultAssetChartAcntId,
isnull([numDefaultCOGSChartAcntId],0) numDefaultCOGSChartAcntId,
--isnull(bitHideNewUsers,0) as bitHideNewUsers,
isnull(bitCheckCreditStatus,0) as bitCheckCreditStatus,
isnull([numRelationshipId],0) numRelationshipId,
isnull([numProfileId],0) numProfileId,
isnull(bitEnableDefaultAccounts,0) as bitEnableDefaultAccounts,
--ISNULL([bitEnableCreditCart],0) AS bitEnableCreditCart,
ISNULL([bitHidePriceBeforeLogin],0) AS bitHidePriceBeforeLogin,
ISNULL([bitHidePriceAfterLogin],0) AS bitHidePriceAfterLogin,
ISNULL([numRelationshipIdHidePrice],0) AS numRelationshipIdHidePrice,
ISNULL([numProfileIDHidePrice],0) AS numProfileIDHidePrice,
ISNULL([numBizDocForCreditCard],0) AS numBizDocForCreditCard,
ISNULL([numBizDocForCreditTerm],0) AS numBizDocForCreditTerm,
ISNULL(bitAuthOnlyCreditCard,0) AS bitAuthOnlyCreditCard,
ISNULL(numAuthrizedOrderStatus,0) AS numAuthrizedOrderStatus,
ISNULL(bitSendEmail,1) AS bitSendEmail,
ISNULL(vcGoogleMerchantID , '') AS vcGoogleMerchantID,
ISNULL(vcGoogleMerchantKey,'') AS vcGoogleMerchantKey,
ISNULL(IsSandbox,0) AS IsSandbox,
ISNULL(numSiteID,0) AS numSiteID,
ISNULL(vcPaypalUserName,'') AS vcPaypalUserName,
ISNULL(vcPaypalPassword,'') AS vcPaypalPassword,
ISNULL(vcPaypalSignature,'') AS vcPaypalSignature,
ISNULL(IsPaypalSandbox,0) AS IsPaypalSandbox,
ISNULL(bitSkipStep2,0) AS [bitSkipStep2],
ISNULL(bitDisplayCategory,0) AS [bitDisplayCategory],
ISNULL(bitShowPriceUsingPriceLevel,0) AS [bitShowPriceUsingPriceLevel],
ISNULL(bitEnableSecSorting,0) AS [bitEnableSecSorting],
ISNULL(bitSortPriceMode,0) AS [bitSortPriceMode],
ISNULL(numPageSize,0) AS [numPageSize],
ISNULL(numPageVariant,0) AS [numPageVariant],
ISNULL(bitAutoSelectWarehouse,0) AS bitAutoSelectWarehouse,
ISNULL([bitPreSellUp],0) AS [bitPreSellUp],
ISNULL([bitPostSellUp],0) AS [bitPostSellUp],
ISNULL([dcPostSellDiscount],0) AS [dcPostSellDiscount],
ISNULL(numDefaultClass,0) numDefaultClass
FROM eCommerceDTL 
WHERE numDomainID=@numDomainID
AND ([numSiteId] = @numSiteID OR @numSiteID = 0)
/****** Object:  StoredProcedure [dbo].[USP_GetItemAttributesEcomm]    Script Date: 07/26/2008 16:17:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getitemattributesecomm')
DROP PROCEDURE usp_getitemattributesecomm
GO
CREATE PROCEDURE [dbo].[USP_GetItemAttributesEcomm]        
@numItemCode as numeric(9)=0,  
@numWarehouseID as numeric(9)=0       
as  
  
declare @bitSerialized as bit  
select @bitSerialized=bitSerialized from Item where numItemcode= @numItemCode  
  
if @bitSerialized=0  
begin  
 select distinct(CFW_Fld_Master.fld_id),Fld_label,fld_type,numlistid,vcURL from ItemGroupsDTL                   
 join CFW_Fld_Master on numOppAccAttrID=Fld_ID              
 join Item on numItemGroup=ItemGroupsDTL.numItemGroupID and tintType=2  
 LEFT JOIN WareHouseItems WI on WI.numItemID= numItemcode  
 LEFT JOIN CFW_Fld_Values_Serialized_Items on RecId=WI.numWareHouseItemID    
 where numItemcode=@numItemCode AND (numWarehouseID=@numWarehouseID OR charItemType <> 'P')
end   
else  
begin  
 select distinct(CFW_Fld_Master.fld_id),Fld_label,fld_type,numlistid,vcURL from ItemGroupsDTL                   
 join CFW_Fld_Master on numOppAccAttrID=Fld_ID               
 join Item on numItemGroup=ItemGroupsDTL.numItemGroupID and tintType=2  
 join  WareHouseItems WI on WI.numItemID= numItemcode  
 join  WareHouseItmsDTL WDTL on WDTL.numWareHouseItemID=WI.numWareHouseItemID  
 join CFW_Fld_Values_Serialized_Items on RecId=numWareHouseItmsDTLID    
 where numItemcode=@numItemCode and CFW_Fld_Values_Serialized_Items.bitSerialized=1 and numWarehouseID=@numWarehouseID  
  
end
GO
--created by anoop jayaraj

-- exec USP_GetOppItemBizDocs @numBizDocID=644,@numDomainID=169,@numOppID=476291,@numOppBizDocID=0,@bitRentalBizDoc=0
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetOppItemBizDocs' ) 
    DROP PROCEDURE USP_GetOppItemBizDocs
GO
CREATE PROCEDURE USP_GetOppItemBizDocs
    @numBizDocID AS NUMERIC(9),
    @numDomainID AS NUMERIC(9),
    @numOppID AS NUMERIC(9),
    @numOppBizDocID AS NUMERIC(9),
    @bitRentalBizDoc AS BIT = 0
AS 
BEGIN

	DECLARE @numSalesAuthoritativeBizDocID AS INT
	SELECT @numSalesAuthoritativeBizDocID = numAuthoritativeSales FROM [AuthoritativeBizDocs] WHERE numDomainID = @numDomainID


    IF @numOppBizDocID = 0 
        IF @bitRentalBizDoc = 1 
        BEGIN
            SELECT  *
            FROM    ( SELECT    OI.numoppitemtCode,
                                OI.numItemCode,
                                OI.vcItemName,
                                OI.vcModelID,
                                OI.numUnitHour AS QtyOrdered,
                                ( OI.numUnitHour
                                    - ISNULL(SUM(OBI.numUnitHour), 0) ) AS QtytoFulFilled,
                                ( OI.numUnitHour
                                    - ISNULL(SUM(OBI.numUnitHour), 0) ) AS QtytoFulFill,
                                ISNULL(numAllocation, 0) AS numAllocation,
                                ISNULL(numBackOrder, 0) AS numBackOrder,
                                CASE WHEN OBI.vcNotes IS NULL THEN OI.vcNotes ELSE OBI.vcNotes END AS vcNotes,
                                '' AS vcTrackingNo,
                                '' vcShippingMethod,
                                0 monShipCost,
                                GETDATE() dtDeliveryDate,
                                CONVERT(BIT, 1) AS Included,
                                ISNULL(OI.[numQtyShipped], 0) numQtyShipped,
                                ISNULL(OI.[numUnitHourReceived], 0) numQtyReceived,
                                I.[charItemType],
                                SUBSTRING(( SELECT  ',' + vcSerialNo
                                                    + CASE WHEN ISNULL(I.bitLotNo, 0) = 1 THEN ' (' + CONVERT(VARCHAR(15), oppI.numQty) + ')'
                                                            ELSE ''
                                                        END
                                            FROM    OppWarehouseSerializedItem oppI
                                                    JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID = whi.numWareHouseItmsDTLID
                                            WHERE   oppI.numOppID = @numOppID
                                                    AND oppI.numOppItemID = OI.numoppitemtCode
                                                    AND oppI.numOppBizDocsId = @numOppBizDocID
                                            ORDER BY vcSerialNo
                                            FOR
                                            XML PATH('')
                                            ), 2, 200000) AS SerialLotNo,
                                I.bitSerialized,
                                ISNULL(I.bitLotNo, 0) AS bitLotNo,
                                OI.numWarehouseItmsID,
                                ISNULL(I.numItemClassification, 0) AS numItemClassification,
                                I.vcSKU,
                                dbo.fn_GetAttributes(OI.numWarehouseItmsID,
                                                        I.bitSerialized) AS Attributes,
                                OM.bintCreatedDate AS dtRentalStartDate,
                                GETDATE() AS dtRentalReturnDate,
                                ISNULL(OI.numUOMId, 0) AS numUOM,
                                OI.monPrice AS monOppItemPrice,
                                ISNULL(OBI.monPrice, OI.monPrice) AS monOppBizDocPrice,
                                ISNULL(OI.vcItemDesc, '') AS [vcItemDesc],
								ISNULL(OI.bitDropShip,0) AS bitDropShip
                        FROM      OpportunityItems OI
                                LEFT JOIN OpportunityMaster OM ON OI.numOppID = oM.numOppID
                                LEFT JOIN OpportunityBizDocs OB ON OB.numOppId = OI.numOppId
                                                                    AND OB.numBizDocId = @numBizDocID
                                LEFT JOIN OpportunityBizDocItems OBI ON OB.numOppBizDocsId = OBI.numOppBizDocId
                                                                        AND OBI.numOppItemID = OI.numoppitemtCode
                                LEFT JOIN WareHouseItems WI ON WI.numWareHouseItemID = OI.numWarehouseItmsID
                                INNER JOIN Item I ON I.[numItemCode] = OI.[numItemCode]
                        WHERE     OI.numOppId = @numOppID
                                AND OI.numoppitemtCode NOT IN (
                                SELECT  OBDI.numOppItemID
                                FROM    OpportunityBizDocs OBD
                                        JOIN OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId = OBDI.numOppBizDocId
                                WHERE   OBD.numOppId = @numOppID )
                        GROUP BY  OI.numoppitemtCode,
                                OI.numItemCode,
                                OI.vcItemName,
                                OI.vcModelID,
								OI.vcNotes,
								OBI.vcNotes,
                                OI.numUnitHour,
                                numAllocation,
                                numBackOrder,
                                OI.numQtyShipped,
                                OI.[numUnitHourReceived],
                                I.[charItemType],
                                I.bitSerialized,
                                I.bitLotNo,
                                OI.numWarehouseItmsID,
                                I.numItemClassification,
                                I.vcSKU,
                                OI.numWarehouseItmsID,
                                OBI.dtRentalStartDate,
                                OBI.dtRentalReturnDate,
                                OI.numUOMId,
                                OM.bintCreatedDate,
                                OI.monPrice,
                                OBI.monPrice,
                                OI.vcItemDesc,OI.bitDropShip
                    ) X
            WHERE   X.QtytoFulFill > 0
        END
        ELSE 
        BEGIN
            SELECT  *
            FROM    ( SELECT    OI.numoppitemtCode,
                                OI.numItemCode,
                                OI.vcItemName,
                                OI.vcModelID,
                                OI.numUnitHour AS QtyOrdered,
                                ( OI.numUnitHour
                                    - ISNULL(SUM(OBI.numUnitHour), 0) ) AS QtytoFulFilled,
                                ( OI.numUnitHour
                                    - ISNULL(SUM(OBI.numUnitHour), 0) ) AS QtytoFulFill,
                                ISNULL(numAllocation, 0) AS numAllocation,
                                ISNULL(numBackOrder, 0) AS numBackOrder,
                                CASE WHEN OBI.vcNotes IS NULL THEN OI.vcNotes ELSE OBI.vcNotes END AS vcNotes,
                                '' AS vcTrackingNo,
                                '' vcShippingMethod,
                                0 monShipCost,
                                GETDATE() dtDeliveryDate,
                                CONVERT(BIT, 1) AS Included,
                                ISNULL(OI.[numQtyShipped], 0) numQtyShipped,
                                ISNULL(OI.[numUnitHourReceived], 0) numQtyReceived,
                                I.[charItemType],
                                SUBSTRING(( SELECT  ',' + vcSerialNo
                                                    + CASE WHEN ISNULL(I.bitLotNo, 0) = 1 THEN ' (' + CONVERT(VARCHAR(15), oppI.numQty) + ')'
                                                            ELSE ''
                                                        END
                                            FROM    OppWarehouseSerializedItem oppI
                                                    JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID = whi.numWareHouseItmsDTLID
                                            WHERE   oppI.numOppID = @numOppID
                                                    AND oppI.numOppItemID = OI.numoppitemtCode
                                                    AND oppI.numOppBizDocsId = @numOppBizDocID
                                            ORDER BY vcSerialNo
                                            FOR
                                            XML PATH('')
                                            ), 2, 200000) AS SerialLotNo,
                                I.bitSerialized,
                                ISNULL(I.bitLotNo, 0) AS bitLotNo,
                                OI.numWarehouseItmsID,
                                ISNULL(I.numItemClassification, 0) AS numItemClassification,
                                I.vcSKU,
                                dbo.fn_GetAttributes(OI.numWarehouseItmsID,
                                                        I.bitSerialized) AS Attributes,
                                OBI.dtRentalStartDate,
                                OBI.dtRentalReturnDate,
                                ISNULL(OI.numUOMId, 0) AS numUOM,
                                OI.monPrice AS monOppItemPrice,
                                ISNULL(OBI.monPrice, OI.monPrice) AS monOppBizDocPrice,
                                ISNULL(OI.vcItemDesc, '') AS [vcItemDesc],
								ISNULL(OI.bitDropShip,0) AS bitDropShip
                        FROM      OpportunityItems OI
                                LEFT JOIN OpportunityMaster OM ON OI.numOppID = oM.numOppID 
                                LEFT JOIN 
									OpportunityBizDocItems OBI 
								ON 
									OBI.numOppItemID = OI.numoppitemtCode 
                                    AND OBI.numOppBizDocId IN ( SELECT  
																	numOppBizDocsId
                                                                FROM    
																	OpportunityBizDocs OB
                                                                WHERE   
																	OB.numOppId = OI.numOppId
                                                                    AND 1 = (CASE 
																				WHEN @numBizDocId = @numSalesAuthoritativeBizDocID --Invoice OR Deferred Income
																				THEN 
																					(CASE WHEN numBizDocId = 304 OR (ISNULL(OB.bitAuthoritativeBizDocs,0) = 1 AND ISNULL(OB.numDeferredBizDocID,0) = 0) THEN 1 ELSE 0 END)
																				WHEN  @numBizDocId = 304
																				THEN (CASE WHEN ISNULL(OB.bitAuthoritativeBizDocs,0) = 1 THEN 1 ELSE 0 END)
																				ELSE 
																					(CASE WHEN numBizDocId = @numBizDocId THEN 1 ELSE 0 END) 
																				END) 
																)
                                LEFT JOIN WareHouseItems WI ON WI.numWareHouseItemID = OI.numWarehouseItmsID
                                INNER JOIN Item I ON I.[numItemCode] = OI.[numItemCode]
                        WHERE     OI.numOppId = @numOppID
                        GROUP BY  OI.numoppitemtCode,
                                OI.numItemCode,
                                OI.vcItemName,
                                OI.vcModelID,
								OI.vcNotes,
								OBI.vcNotes,
                                OI.numUnitHour,
                                numAllocation,
                                numBackOrder,
                                OI.numQtyShipped,
                                OI.[numUnitHourReceived],
                                I.[charItemType],
                                I.bitSerialized,
                                I.bitLotNo,
                                OI.numWarehouseItmsID,
                                I.numItemClassification,
                                I.vcSKU,
                                OI.numWarehouseItmsID,
                                OBI.dtRentalStartDate,
                                OBI.dtRentalReturnDate,
                                OI.numUOMId,
                                OI.monPrice,
                                OBI.monPrice,
                                OI.vcItemDesc,OI.bitDropShip
                    ) X
            WHERE   X.QtytoFulFill > 0
        END
    ELSE 
        IF @bitRentalBizDoc = 1 
        BEGIN
            SELECT  OI.numoppitemtCode,
                    OI.numItemCode,
                    OI.vcItemName,
                    OI.vcModelID,
                    OI.numUnitHour AS QtyOrdered,
                    OBI.numUnitHour AS QtytoFulFilled,
                    ( OI.numUnitHour - ISNULL(SUM(OBI.numUnitHour), 0) ) AS QtytoFulFill,
                    ISNULL(numAllocation, 0) AS numAllocation,
                    ISNULL(numBackOrder, 0) AS numBackOrder,
                    CASE WHEN OBI.vcNotes IS NULL THEN OI.vcNotes ELSE OBI.vcNotes END AS vcNotes,
                    ISNULL(OBI.vcTrackingNo, '') AS vcTrackingNo,
                    OBI.vcShippingMethod,
                    OBI.monShipCost,
                    OBI.dtDeliveryDate,
                    CONVERT(BIT, 1) AS Included,
                    ISNULL(OI.[numQtyShipped], 0) numQtyShipped,
                    ISNULL(OI.[numUnitHourReceived], 0) numQtyReceived,
                    I.[charItemType],
                    SUBSTRING(( SELECT  ',' + vcSerialNo
                                        + CASE WHEN ISNULL(I.bitLotNo, 0) = 1
                                                THEN ' ('
                                                    + CONVERT(VARCHAR(15), oppI.numQty)
                                                    + ')'
                                                ELSE ''
                                            END
                                FROM    OppWarehouseSerializedItem oppI
                                        JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID = whi.numWareHouseItmsDTLID
                                WHERE   oppI.numOppID = @numOppID
                                        AND oppI.numOppItemID = OI.numoppitemtCode
                                        AND oppI.numOppBizDocsId = @numOppBizDocID
                                ORDER BY vcSerialNo
                                FOR
                                XML PATH('')
                                ), 2, 200000) AS SerialLotNo,
                    I.bitSerialized,
                    ISNULL(I.bitLotNo, 0) AS bitLotNo,
                    OI.numWarehouseItmsID,
                    ISNULL(I.numItemClassification, 0) AS numItemClassification,
                    I.vcSKU,
                    dbo.fn_GetAttributes(OI.numWarehouseItmsID,
                                            I.bitSerialized) AS Attributes,
                    OBI.dtRentalStartDate,
                    OBI.dtRentalReturnDate,
                    ISNULL(OI.numUOMId, 0) AS numUOM,
                    OI.monPrice AS monOppItemPrice,
                    ISNULL(OBI.monPrice, OI.monPrice) AS monOppBizDocPrice,
                    ISNULL(OI.vcItemDesc, '') AS [vcItemDesc],
					ISNULL(OI.bitDropShip,0) AS bitDropShip
            FROM    OpportunityItems OI
                    LEFT JOIN OpportunityMaster OM ON OI.numOppID = oM.numOppID
                    JOIN OpportunityBizDocs OB ON OB.numOppId = OI.numOppId
                    JOIN OpportunityBizDocItems OBI ON OB.numOppBizDocsId = OBI.numOppBizDocId
                                                        AND OBI.numOppItemID = OI.numoppitemtCode
                    LEFT JOIN WareHouseItems WI ON WI.numWareHouseItemID = OI.numWarehouseItmsID
                    INNER JOIN Item I ON I.[numItemCode] = OI.[numItemCode]
            WHERE   OI.numOppId = @numOppID
                    AND OB.numOppBizDocsId = @numOppBizDocID
            GROUP BY OI.numoppitemtCode,
                    OI.numItemCode,
                    OI.vcItemName,
                    OI.vcModelID,
					OI.vcNotes,
                    OI.numUnitHour,
                    numAllocation,
                    numBackOrder,
                    OBI.vcNotes,
                    OBI.vcTrackingNo,
                    OBI.vcShippingMethod,
                    OBI.monShipCost,
                    OBI.dtDeliveryDate,
                    OBI.numUnitHour,
                    OI.[numQtyShipped],
                    OI.[numUnitHourReceived],
                    I.[charItemType],
                    I.bitSerialized,
                    I.bitLotNo,
                    OI.numWarehouseItmsID,
                    I.numItemClassification,
                    I.vcSKU,
                    OI.numWarehouseItmsID,
                    OBI.dtRentalStartDate,
                    OBI.dtRentalReturnDate,
                    OI.numUOMId,
                    OI.monPrice,
                    OBI.monPrice,
                    OI.vcItemDesc,OI.bitDropShip
        END
        ELSE 
        BEGIN
            SELECT  OI.numoppitemtCode,
                    OI.numItemCode,
                    OI.vcItemName,
                    OI.vcModelID,
                    OI.numUnitHour AS QtyOrdered,
                    OBI.numUnitHour AS QtytoFulFilled,
                    (OI.numUnitHour - ISNULL(SUM(OBI.numUnitHour), 0) - ISNULL(TEMPDeferredIncomeItem.numUnitHour,0)) AS QtytoFulFill,
                    ISNULL(numAllocation, 0) AS numAllocation,
                    ISNULL(numBackOrder, 0) AS numBackOrder,
                    CASE WHEN OBI.vcNotes IS NULL THEN OI.vcNotes ELSE OBI.vcNotes END AS vcNotes,
                    ISNULL(OBI.vcTrackingNo, '') AS vcTrackingNo,
                    OBI.vcShippingMethod,
                    OBI.monShipCost,
                    OBI.dtDeliveryDate,
                    CONVERT(BIT, 1) AS Included,
                    ISNULL(OI.[numQtyShipped], 0) numQtyShipped,
                    ISNULL(OI.[numUnitHourReceived], 0) numQtyReceived,
                    I.[charItemType],
                    SUBSTRING(( SELECT  ',' + vcSerialNo
                                        + CASE WHEN ISNULL(I.bitLotNo, 0) = 1
                                                THEN ' ('
                                                    + CONVERT(VARCHAR(15), oppI.numQty)
                                                    + ')'
                                                ELSE ''
                                            END
                                FROM    OppWarehouseSerializedItem oppI
                                        JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID = whi.numWareHouseItmsDTLID
                                WHERE   oppI.numOppID = @numOppID
                                        AND oppI.numOppItemID = OI.numoppitemtCode
                                        AND oppI.numOppBizDocsId = @numOppBizDocID
                                ORDER BY vcSerialNo
                                FOR
                                XML PATH('')
                                ), 2, 200000) AS SerialLotNo,
                    I.bitSerialized,
                    ISNULL(I.bitLotNo, 0) AS bitLotNo,
                    OI.numWarehouseItmsID,
                    ISNULL(I.numItemClassification, 0) AS numItemClassification,
                    I.vcSKU,
                    dbo.fn_GetAttributes(OI.numWarehouseItmsID,
                                            I.bitSerialized) AS Attributes,
                    OBI.dtRentalStartDate,
                    OBI.dtRentalReturnDate,
                    ISNULL(OI.numUOMId, 0) AS numUOM,
                    OI.monPrice AS monOppItemPrice,
                    ISNULL(OBI.monPrice, OI.monPrice) AS monOppBizDocPrice,
                    ISNULL(OI.vcItemDesc, '') AS [vcItemDesc],
					ISNULL(OI.bitDropShip,0) AS bitDropShip
            FROM    OpportunityItems OI
                    LEFT JOIN OpportunityMaster OM ON OI.numOppID = oM.numOppID
                    JOIN OpportunityBizDocs OB ON OB.numOppId = OI.numOppId
                    JOIN OpportunityBizDocItems OBI ON OB.numOppBizDocsId = OBI.numOppBizDocId
                                                        AND OBI.numOppItemID = OI.numoppitemtCode
                    LEFT JOIN WareHouseItems WI ON WI.numWareHouseItemID = OI.numWarehouseItmsID
                    INNER JOIN Item I ON I.[numItemCode] = OI.[numItemCode]
					OUTER APPLY
					(
						SELECT
							SUM(OpportunityBizDocItems.numUnitHour) AS numUnitHour
						FROM 
							OpportunityBizDocs
						INNER JOIN
							OpportunityBizDocItems 
						ON
							OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
						WHERE
							numOppId = @numOppID
							AND numOppBizDocID <> @numOppBizDocID
							AND	1 = (CASE 
									WHEN @numBizDocID = 304 --DEFERRED BIZDOC OR INVOICE
									THEN (CASE WHEN (numBizDocId = @numSalesAuthoritativeBizDocID OR numBizDocId=304) THEN 1 ELSE 0 END) -- WHEN DEFERRED BIZDOC(304) IS BEING ADDED SUBTRACT ITEMS ADDED TO INVOICE AND OTHER DEFERRED BIZDOC IN QtytoFulFill
									ELSE (CASE WHEN numBizDocId = @numBizDocID THEN 1 ELSE 0 END)
									END) 
							AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
					) TEMPDeferredIncomeItem
            WHERE   OI.numOppId = @numOppID
                    AND OB.numOppBizDocsId = @numOppBizDocID
            GROUP BY OI.numoppitemtCode,
                    OI.numItemCode,
                    OI.vcItemName,
                    OI.vcModelID,
					OI.vcNotes,
                    OI.numUnitHour,
                    numAllocation,
                    numBackOrder,
                    OBI.vcNotes,
                    OBI.vcTrackingNo,
                    OBI.vcShippingMethod,
                    OBI.monShipCost,
                    OBI.dtDeliveryDate,
                    OBI.numUnitHour,
                    OI.[numQtyShipped],
                    OI.[numUnitHourReceived],
                    I.[charItemType],
                    I.bitSerialized,
                    I.bitLotNo,
                    OI.numWarehouseItmsID,
                    I.numItemClassification,
                    I.vcSKU,
                    OI.numWarehouseItmsID,
                    OBI.dtRentalStartDate,
                    OBI.dtRentalReturnDate,
                    OI.numUOMId,
                    OI.monPrice,
                    OBI.monPrice,
                    OI.vcItemDesc,OI.bitDropShip,
					TEMPDeferredIncomeItem.numUnitHour
            UNION
            SELECT  *
            FROM    ( SELECT    OI.numoppitemtCode,
                                OI.numItemCode,
                                OI.vcItemName,
                                OI.vcModelID,
                                OI.numUnitHour AS QtyOrdered,
                                ( OI.numUnitHour
                                    - ISNULL(SUM(OBI.numUnitHour), 0) ) AS QtytoFulFilled,
                                ( OI.numUnitHour
                                    - ISNULL(SUM(OBI.numUnitHour), 0) ) AS QtytoFulFill,
                                ISNULL(numAllocation, 0) AS numAllocation,
                                ISNULL(numBackOrder, 0) AS numBackOrder,
                                CASE WHEN OBI.vcNotes IS NULL THEN OI.vcNotes ELSE OBI.vcNotes END AS vcNotes,
                                '' AS vcTrackingNo,
                                '' vcShippingMethod,
                                1 AS monShipCost,
                                NULL dtDeliveryDate,
                                CONVERT(BIT, 0) AS Included,
                                ISNULL(OI.[numQtyShipped], 0) numQtyShipped,
                                ISNULL(OI.[numUnitHourReceived], 0) numQtyReceived,
                                I.[charItemType],
                                SUBSTRING(( SELECT  ',' + vcSerialNo
                                                    + CASE WHEN ISNULL(I.bitLotNo, 0) = 1 THEN ' (' + CONVERT(VARCHAR(15), oppI.numQty) + ')'
                                                            ELSE ''
                                                        END
                                            FROM    OppWarehouseSerializedItem oppI
                                                    JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID = whi.numWareHouseItmsDTLID
                                            WHERE   oppI.numOppID = @numOppID
                                                    AND oppI.numOppItemID = OI.numoppitemtCode
                                                    AND oppI.numOppBizDocsId = @numOppBizDocID
                                            ORDER BY vcSerialNo
                                            FOR
                                            XML PATH('')
                                            ), 2, 200000) AS SerialLotNo,
                                I.bitSerialized,
                                ISNULL(I.bitLotNo, 0) AS bitLotNo,
                                OI.numWarehouseItmsID,
                                ISNULL(I.numItemClassification, 0) AS numItemClassification,
                                I.vcSKU,
                                dbo.fn_GetAttributes(OI.numWarehouseItmsID,
                                                        I.bitSerialized) AS Attributes,
                                OBI.dtRentalStartDate,
                                OBI.dtRentalReturnDate,
                                ISNULL(OI.numUOMId, 0) AS numUOM,
                                OI.monPrice AS monOppItemPrice,
                                ISNULL(OBI.monPrice, OI.monPrice) AS monOppBizDocPrice,
                                ISNULL(OI.vcItemDesc, '') AS [vcItemDesc],
								ISNULL(OI.bitDropShip,0) AS bitDropShip
                        FROM      OpportunityItems OI
                                LEFT JOIN OpportunityMaster OM ON OI.numOppID = oM.numOppID
                                LEFT JOIN OpportunityBizDocs OB ON OB.numOppId = OI.numOppId
                                                                    AND OB.numBizDocId = @numBizDocID
                                LEFT JOIN OpportunityBizDocItems OBI ON OB.numOppBizDocsId = OBI.numOppBizDocId
                                                                        AND OBI.numOppItemID = OI.numoppitemtCode
                                LEFT JOIN WareHouseItems WI ON WI.numWareHouseItemID = OI.numWarehouseItmsID
                                INNER JOIN Item I ON I.[numItemCode] = OI.[numItemCode]
                        WHERE     OI.numOppId = @numOppID
                                AND OI.numoppitemtCode NOT IN (
                                    SELECT  numOppItemID
                                    FROM    OpportunityBizDocItems OBI2
                                            JOIN OpportunityBizDocs OB2 ON OBI2.numOppBizDocID = OB2.numOppBizDocsId
                                    WHERE   numOppID = @numOppID
                                            AND numBizDocId = @numBizDocID )
                        GROUP BY  OI.numoppitemtCode,
                                OI.numItemCode,
                                OI.vcItemName,
                                OI.vcModelID,
								OI.vcNotes,
								OBI.vcNotes,
                                OI.numUnitHour,
                                numAllocation,
                                numBackOrder,
                                OBI.numUnitHour,
                                OI.[numQtyShipped],
                                OI.[numUnitHourReceived],
                                I.[charItemType],
                                I.bitSerialized,
                                I.bitLotNo,
                                OI.numWarehouseItmsID,
                                I.numItemClassification,
                                I.vcSKU,
                                OI.numWarehouseItmsID,
                                OBI.dtRentalStartDate,
                                OBI.dtRentalReturnDate,
                                OI.numUOMId,
                                OI.monPrice,
                                OBI.monPrice,
                                OI.vcItemDesc,OI.bitDropShip
                    ) X
            WHERE   X.QtytoFulFill > 0
        END
END


GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetOrderItems' ) 
    DROP PROCEDURE USP_GetOrderItems
GO
CREATE PROCEDURE USP_GetOrderItems ( @tintMode TINYINT = 0,
                                     @numOppItemID NUMERIC(9) = 0,
                                     @numOppID NUMERIC(9) = 0,
                                     @numDomainId NUMERIC(9),
                                     @ClientTimeZoneOffset INT,
									 @vcOppItemIDs VARCHAR(2000)='' )
AS 
BEGIN
	
    IF @tintMode = 1 --Add Return grid data & For Recurring Get Original Units to split
        BEGIN
            SELECT  OI.numoppitemtCode,
                    OI.numItemCode,
                    OI.vcItemName,
                    OI.vcItemDesc,
                    OI.numUnitHour numOriginalUnitHour,
					CAST(( dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), OI.numItemCode,
                                    OM.numDomainId, ISNULL(OI.numUOMId, 0))
						* OI.numUnitHour ) AS NUMERIC(18, 2)) AS numUnitHour,
					OI.monPrice
            FROM    dbo.OpportunityMaster OM
                    INNER JOIN dbo.OpportunityItems OI ON OM.numOppId = OI.numOppId
                    INNER JOIN dbo.Item I ON I.numItemCode = OI.numItemCode
            WHERE   OM.numDomainId = @numDomainId
                    AND OM.numOppId = @numOppID
                    
        END
 
    IF @tintMode = 2 -- Get single item based on PK, or all items
		-- Used by Sales/Purchase Template Load items in New Sales Order
        BEGIN 
            SELECT  OI.numoppitemtCode,
                    OI.numOppId,
                    OI.numItemCode,
					OI.bitItemPriceApprovalRequired,
                    OI.numUnitHour numOriginalUnitHour,
                    OI.monPrice,
                    OI.monTotAmount,
                    OI.numSourceID,
                    OI.vcItemDesc,
                    OI.numWarehouseItmsID,
                    OI.vcType,
                    OI.vcAttributes,
                    OI.bitDropShip,
                    ISNULL(OI.numUnitHourReceived,0) AS numUnitHourReceived,
                    OI.bitDiscountType,
                    OI.fltDiscount,
                    OI.monTotAmtBefDiscount,
                    ISNULL(OI.numQtyShipped,0) AS numQtyShipped,
                    OI.vcItemName,
                    OI.vcModelID,
                    OI.vcPathForTImage,
                    OI.vcManufacturer,
                    OI.monVendorCost,
                    ISNULL(OI.numUOMId, 0) AS numUOMId,
                    ISNULL(OI.numUOMId, 0) numUOM, -- used from add edit order
                    ISNULL(U.vcUnitName, '') vcUOMName,
                    dbo.fn_UOMConversion(OI.numUOMId, OI.numItemCode,
                                         OM.numDomainId, NULL) AS UOMConversionFactor,
                    OI.bitWorkOrder,
                    isnull(OI.numVendorWareHouse,0) as numVendorWareHouse,
                    OI.numShipmentMethod,
                    OI.numSOVendorId,
                    OI.numProjectID,
                    OI.numClassID,
                    OI.numProjectStageID,
                    OI.numToWarehouseItemID,
                    WItems.numWarehouseID,
                    W.vcWareHouse AS Warehouse,
					CAST(( dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), OI.numItemCode,
                                    OM.numDomainId, ISNULL(OI.numUOMId, 0))
						* OI.numUnitHour ) AS NUMERIC(18, 2)) AS numUnitHour,
					isnull(OI.numClassID,0) as numClassID,
					isnull(OI.numProjectID,0) as numProjectID,
					dbo.fn_GetUOMName(I.numBaseUnit) AS vcBaseUOMName,
					OI.bitDropShip AS bitAllowDropShip,
					I.numVendorID,
					ISNULL(I.bitFreeShipping,0) AS [bitFreeShipping],
					ISNULL(I.txtItemDesc,'') txtItemDesc,
					ISNULL(I.vcModelID,'') vcModelID,
					ISNULL(I.vcManufacturer,'') vcManufacturer,
					ISNULL(I.numBarCodeId,'') numBarCodeId,
					ISNULL(I.bitSerialized,0) bitSerialized,
					ISNULL(I.bitKitParent,0) bitKitParent,
					ISNULL(I.bitAssembly,0) bitAssembly,
					ISNULL(I.IsArchieve,0) IsArchieve,
					ISNULL(I.bitLotNo,0) bitLotNo,
					ISNULL(WItems.monWListPrice,0) monWListPrice,
					ISNULL(I.bitTaxable, 0) bitTaxable,
                    I.charItemType,
                    I.bitKitParent,
					I.bitAssembly,
					I.bitSerialized,
					ISNULL(I.fltWeight,0) AS [fltWeight],
					ISNULL(I.fltLength,0) AS [fltLength],
					ISNULL(I.fltWidth,0) AS [fltWidth],
					ISNULL(I.fltHeight,0) AS [fltHeight],
					ISNULL(WItems.vcWHSKU,ISNULL(I.vcSKU,'')) AS [vcSKU],
					ISNULL(I.monAverageCost,0) AS monAverageCost,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numOnHand),0) AS VARCHAR(10)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numOnHand,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numOnOrder),0) AS VARCHAR(10)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numOnOrder,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numAllocation),0) AS VARCHAR(10)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numAllocation,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numBackOrder),0) AS VARCHAR(10)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numBackOrder,
					ISNULL(OI.vcAttrValues,'') AS vcAttrValues
            FROM    dbo.OpportunityItems OI
                    INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OI.numOppId
                    INNER JOIN dbo.Item I ON I.numItemCode = OI.numItemCode
                    LEFT JOIN UOM U ON U.numUOMId = OI.numUOMId
                    LEFT JOIN WareHouseItems WItems ON WItems.numWareHouseItemID = OI.numWarehouseItmsID
                    LEFT JOIN Warehouses W ON W.numWarehouseID = WItems.numWarehouseID
            WHERE   OI.numOppId = @numOppID
                    AND ( OI.numoppitemtCode = @numOppItemID
                          OR @numOppItemID = 0
                        )
                    AND OM.numDomainId = @numDomainId
                    
        END
 
    IF @tintMode = 3 -- Get item for SO to PO and vice versa creation
        BEGIN
            DECLARE @tintOppType AS TINYINT
			SELECT @tintOppType=tintOppType FROM OpportunityMaster WHERE numOppId=@numOppID

			IF @tintOppType = 1
			BEGIN
				DECLARE @TempItems TABLE
				(
					ID INT IDENTITY(1,1),numItemCode NUMERIC(18,0),vcItemName VARCHAR(300),charItemType char,vcModelID VARCHAR(100),vcItemDesc TEXT,vcType VARCHAR(20),vcPathForTImage VARCHAR(200),
					numWarehouseItmsID NUMERIC(18,0),vcAttributes VARCHAR(500),bitDropShip BIT,numUnitHour INT,numUOM NUMERIC(18,0),vcUOMName VARCHAR(100),UOMConversionFactor DECIMAL(18,5),
					monPrice MONEY,numVendorID NUMERIC(18,0),vcPartNo VARCHAR(300),monVendorCost MONEY,numSOVendorId NUMERIC(18,0),numClassID NUMERIC(18,0),numProjectID NUMERIC(18,0),tintLevel TINYINT, vcAttrValues VARCHAR(500)
				)

				INSERT INTO 
					@TempItems
				SELECT
					OI.numItemCode,OI.vcItemName,I.charItemType,OI.vcModelID,OI.vcItemDesc,OI.vcType,ISNULL(OI.vcPathForTImage, ''),ISNULL(OI.numWarehouseItmsID,0),ISNULL(OI.vcAttributes, ''),
					0,OI.numUnitHour,ISNULL(OI.numUOMId, 0),ISNULL(U.vcUnitName, ''),dbo.fn_UOMConversion(OI.numUOMId, OI.numItemCode,OM.numDomainId, NULL) AS UOMConversionFactor,
					OI.monPrice,ISNULL(I.numVendorID, 0),ISNULL(V.vcPartNo, ''),ISNULL(V.monCost, 0),ISNULL(OI.numSOVendorId, 0),0,0,0,ISNULL(vcAttrValues,'')
				FROM    
					dbo.OpportunityItems OI
					INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OI.numOppId
					INNER JOIN dbo.Item I ON I.numItemCode = OI.numItemCode
					LEFT JOIN UOM U ON U.numUOMId = OI.numUOMId
					LEFT JOIN Vendor V ON V.numVendorID = I.numVendorID	AND I.numItemCode = V.numItemCode
				WHERE   
					OI.numOppId = @numOppID
					AND OM.numDomainId = @numDomainId
					AND ISNULL(I.bitKitParent,0) = 0
					AND ISNULL(I.bitAssembly,0) = 0


				--INSERT KIT CHILD ITEMS WHICH ARE NOT KIT AND NOT YET ADDED
				INSERT INTO 
					@TempItems
				SELECT
					OKI.numChildItemID,I.vcItemName,I.charItemType,I.vcModelID,I.txtItemDesc,(CASE WHEN I.charitemType = 'P' THEN 'Product' WHEN I.charitemType = 'S' THEN 'Service' END),
					ISNULL((SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = I.numItemCode AND bitDefault=1),''),ISNULL(OKI.numWareHouseItemId,0),
					ISNULL(dbo.USP_GetAttributes(OKI.numWarehouseItemID, bitSerialized),''),0,OKI.numQtyItemsReq,ISNULL(OKI.numUOMId, 0),ISNULL(U.vcUnitName, ''),
					dbo.fn_UOMConversion(OKI.numUOMId, OKI.numChildItemID,OM.numDomainId, NULL),
					(CASE WHEN I.charitemType = 'P' THEN WI.monWListPrice ELSE I.monListPrice END),
					ISNULL(I.numVendorID, 0),ISNULL(V.vcPartNo, ''),ISNULL(V.monCost, 0),0,0,0,1,''
				FROM    
					dbo.OpportunityKitItems OKI
				LEFT JOIN WarehouseItems WI ON OKI.numWareHouseItemId = WI.numWarehouseItemID
				INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OKI.numOppId
				INNER JOIN dbo.Item I ON I.numItemCode = OKI.numChildItemID
				LEFT JOIN UOM U ON U.numUOMId = OKI.numUOMId
				LEFT JOIN Vendor V ON V.numVendorID = I.numVendorID	AND I.numItemCode = V.numItemCode
				LEFT JOIN @TempItems t1 ON OKI.numChildItemID = t1.numItemCode AND ISNULL(OKI.numWareHouseItemId,0) = ISNULL(t1.numWarehouseItmsID,0)
				WHERE   
					OKI.numOppId = @numOppID
					AND OM.numDomainId = @numDomainId
					AND ISNULL(I.bitKitParent,0) = 0
					AND ISNULL(I.bitAssembly,0) = 0
					AND t1.ID IS NULL


				-- FOR KIT CHILD ITEMS WHICH ARE ALREADY ADDED AT PARENT LEVEL - UPDATE QTY ONLY
				UPDATE 
					t2
				SET 
					t2.numUnitHour = ISNULL(t2.numUnitHour,0) + ISNULL(OKI.numQtyItemsReq,0),
					t2.monPrice = (CASE 
									WHEN ISNULL(t2.monPrice,0) < (CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END)  
									THEN t2.monPrice
									ELSE (CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END)
									END)
				FROM
					@TempItems t2
				INNER JOIN
					OpportunityKitItems OKI 
				ON
					OKI.numChildItemID = t2.numItemCode AND ISNULL(OKI.numWareHouseItemId,0) = ISNULL(t2.numWarehouseItmsID,0)
				LEFT JOIN WarehouseItems WI ON OKI.numWareHouseItemId = WI.numWarehouseItemID
				INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OKI.numOppId
				INNER JOIN dbo.Item I ON I.numItemCode = OKI.numChildItemID
				LEFT JOIN UOM U ON U.numUOMId = OKI.numUOMId
				LEFT JOIN Vendor V ON V.numVendorID = I.numVendorID	AND I.numItemCode = V.numItemCode
				WHERE   
					OKI.numOppId = @numOppID
					AND OM.numDomainId = @numDomainId
					AND ISNULL(I.bitKitParent,0) = 0
					AND ISNULL(I.bitAssembly,0) = 0
					AND t2.tintLevel <> 1

				--INSERT SUB KIT CHILD ITEMS WHICH ARE NOT KIT AND NOT YET ADDED
				INSERT INTO 
					@TempItems
				SELECT
					OKCI.numItemID,I.vcItemName,I.charItemType,I.vcModelID,I.txtItemDesc,(CASE WHEN I.charitemType = 'P' THEN 'Product' WHEN I.charitemType = 'S' THEN 'Service' END),
					ISNULL((SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = I.numItemCode AND bitDefault=1),''),ISNULL(OKCI.numWareHouseItemId,0),
					ISNULL(dbo.USP_GetAttributes(OKCI.numWarehouseItemID, bitSerialized),''),0,OKCI.numQtyItemsReq,ISNULL(OKCI.numUOMId, 0),ISNULL(U.vcUnitName, ''),
					dbo.fn_UOMConversion(OKCI.numUOMId, OKCI.numItemID,OM.numDomainId, NULL),
					(CASE WHEN I.charitemType = 'P' THEN WI.monWListPrice ELSE I.monListPrice END),
					ISNULL(I.numVendorID, 0),ISNULL(V.vcPartNo, ''),ISNULL(V.monCost, 0),0,0,0,2,''
				FROM    
					dbo.OpportunityKitChildItems OKCI
				LEFT JOIN WarehouseItems WI ON OKCI.numWareHouseItemId = WI.numWarehouseItemID
				INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OKCI.numOppId
				INNER JOIN dbo.Item I ON I.numItemCode = OKCI.numItemID
				LEFT JOIN UOM U ON U.numUOMId = OKCI.numUOMId
				LEFT JOIN Vendor V ON V.numVendorID = I.numVendorID	AND I.numItemCode = V.numItemCode
				LEFT JOIN @TempItems t1 ON OKCI.numItemID = t1.numItemCode AND ISNULL(OKCI.numWareHouseItemId,0) = ISNULL(t1.numWarehouseItmsID,0)
				WHERE   
					OKCI.numOppId = @numOppID
					AND OM.numDomainId = @numDomainId
					AND ISNULL(I.bitKitParent,0) = 0
					AND ISNULL(I.bitAssembly,0) = 0
					AND t1.ID IS NULL

				-- FOR SUB KIT CHILD ITEMS WHICH ARE ALREADY ADDED AT PARENT LEVEL - UPDATE QTY ONLY
				UPDATE 
					t2
				SET 
					t2.numUnitHour = ISNULL(t2.numUnitHour,0) + ISNULL(OKCI.numQtyItemsReq,0),
					t2.monPrice = (CASE 
									WHEN ISNULL(t2.monPrice,0) < (CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END)  
									THEN t2.monPrice
									ELSE (CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END)
									END)
				FROM
					@TempItems t2
				INNER JOIN
					OpportunityKitChildItems OKCI 
				ON
					OKCI.numItemID = t2.numItemCode AND ISNULL(OKCI.numWareHouseItemId,0) = ISNULL(t2.numWarehouseItmsID,0)
				LEFT JOIN WarehouseItems WI ON OKCI.numWareHouseItemId = WI.numWarehouseItemID
				INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OKCI.numOppId
				INNER JOIN dbo.Item I ON I.numItemCode = OKCI.numItemID
				LEFT JOIN UOM U ON U.numUOMId = OKCI.numUOMId
				LEFT JOIN Vendor V ON V.numVendorID = I.numVendorID	AND I.numItemCode = V.numItemCode
				WHERE   
					OKCI.numOppId = @numOppID
					AND OM.numDomainId = @numDomainId
					AND ISNULL(I.bitKitParent,0) = 0
					AND ISNULL(I.bitAssembly,0) = 0
					AND t2.tintLevel <> 2

				SELECT * FROM @TempItems
			END
			ELSE
			BEGIN
				SELECT  OI.numoppitemtCode,
                    OI.numOppId,
					OI.bitItemPriceApprovalRequired,
                    OI.vcItemDesc,
                    ISNULL(OI.vcPathForTImage, '') vcPathForTImage,
                    ISNULL(OI.numWarehouseItmsID, 0) numWarehouseItmsID,
                    OI.vcItemName,
                    OI.vcModelID,
                    ISNULL(OI.vcAttributes, '') vcAttributes,
                    OI.vcType,
                    ISNULL(OI.bitDropShip, 0) bitDropShip,
                    OI.numItemCode,
                    OI.numUnitHour numOriginalUnitHour,
                    ISNULL(OI.numUOMId, 0) AS numUOM,
                    ISNULL(U.vcUnitName, '') vcUOMName,
                    dbo.fn_UOMConversion(OI.numUOMId, OI.numItemCode,
                                         OM.numDomainId, NULL) AS UOMConversionFactor,
                    ISNULL(I.numVendorID, 0) numVendorID,
                    ISNULL(V.monCost, 0) monVendorCost,
                    ISNULL(V.vcPartNo, '') vcPartNo,
                    OI.monPrice,
                    ISNULL(OI.numSOVendorId, 0) numSOVendorId,
                    ISNULL(I.charItemType, 'N') charItemType,
					CAST(( dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), OI.numItemCode,
                                    OM.numDomainId, ISNULL(OI.numUOMId, 0))
						* OI.numUnitHour ) AS NUMERIC(18, 2)) AS numUnitHour,
					isnull(OI.numClassID,0) as numClassID,isnull(OI.numProjectID,0) as numProjectID,ISNULL(I.fltWeight,0) AS [fltWeight],ISNULL(I.bitFreeShipping,0) AS [bitFreeShipping],
					ISNULL(I.fltLength,0) AS [fltLength],
					ISNULL(I.fltWidth,0) AS [fltWidth],
					ISNULL(I.fltHeight,0) AS [fltHeight],
					ISNULL((SELECT vcWHSKU FROM dbo.WareHouseItems WHERE numWarehouseItemID = OI.numWarehouseItmsID ),ISNULL(I.vcSKU,'')) AS [vcSKU],
					ISNULL(vcAttrValues,'') AS vcAttrValues
				FROM    dbo.OpportunityItems OI
						INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OI.numOppId
						INNER JOIN dbo.Item I ON I.numItemCode = OI.numItemCode
						LEFT JOIN UOM U ON U.numUOMId = OI.numUOMId
						LEFT JOIN Vendor V ON V.numVendorID = I.numVendorID
												AND I.numItemCode = V.numItemCode
				WHERE   OI.numOppId = @numOppID
						AND OM.numDomainId = @numDomainId
			END

            
					--AND 1 = (CASE 
					--			WHEN @tintOppType = 1 
					--			THEN (CASE WHEN ISNULL(I.bitKitParent,0) = 0 THEN 1 ELSE 0 END)
					--			ELSE 1
					--		END)
        END
        IF @tintMode = 4 -- Add Edit Order items
        BEGIN 
            SELECT  OI.numoppitemtCode,
                    OI.numOppId,
                    OI.numItemCode,
					OI.bitItemPriceApprovalRequired,
					OI.numUnitHour AS numOrigUnitHour,
                    OI.numUnitHour,
                    OI.monPrice,
                    OI.monTotAmount,
                    OI.numSourceID,
                    OI.vcItemDesc,
                    OI.numWarehouseItmsID,
                    OI.vcType ItemType,
                    OI.vcAttributes Attributes,
                    OI.bitDropShip DropShip,
                    OI.numUnitHourReceived,
                    OI.bitDiscountType,
                    OI.fltDiscount,
                    OI.monTotAmtBefDiscount,
                    OI.numQtyShipped,
                    OI.vcItemName,
                    OI.vcModelID,
                    OI.vcPathForTImage,
                    OI.vcManufacturer,
                    OI.monVendorCost,
                    ISNULL(OI.numUOMId, 0) AS numUOM,
                    ISNULL(U.vcUnitName, '') vcUOMName,
                    ISNULL(dbo.fn_UOMConversion(OI.numUOMId, OI.numItemCode,
                                         OM.numDomainId, NULL),1) AS UOMConversionFactor,
                    OI.bitWorkOrder,
                    isnull(OI.numVendorWareHouse,0) as numVendorWareHouse,
                    OI.numShipmentMethod,
                    OI.numSOVendorId,
                    OI.numProjectID,
                    OI.numClassID,
                    OI.numProjectStageID,
                    OI.numToWarehouseItemID,
                   UPPER(I.charItemType) charItemType,
                    WItems.numWarehouseID,
                    W.vcWareHouse AS Warehouse,
                    0 as Op_Flag,
					CAST(( dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), OI.numItemCode,
                                    OM.numDomainId, ISNULL(OI.numUOMId, 0))
						* OI.numUnitHour ) AS NUMERIC(18, 2)) AS numUnitHour,
					isnull(OI.numClassID,0) as numClassID,isnull(OI.numProjectID,0) as numProjectID,
					CASE WHEN (SELECT COUNT(*) FROM OpportunityBizDocs OB JOIN dbo.OpportunityBizDocItems OBI ON ob.numOppBizDocsId=obi.numOppBizDocID
					AND (ob.bitAuthoritativeBizDocs=1 OR ob.numBizDocId=304) AND obi.numOppItemID=oi.numoppitemtCode AND ob.numOppId=OM.numOppId)>0 THEN 1 ELSE 0 END  AS bitIsAuthBizDoc,
					ISNULL(OI.numUnitHourReceived,0) AS numUnitHourReceived,ISNULL(numQtyShipped,0) AS numQtyShipped,dbo.fn_GetUOMName(I.numBaseUnit) AS vcBaseUOMName,ISNULL(I.fltWeight,0) AS [fltWeight],ISNULL(I.bitFreeShipping,0) AS [bitFreeShipping],
					ISNULL(I.fltLength,0) AS [fltLength],
					ISNULL(I.fltWidth,0) AS [fltWidth],
					ISNULL(I.fltHeight,0) AS [fltHeight],
					ISNULL(WItems.vcWHSKU,ISNULL(I.vcSKU,'')) AS [vcSKU],
					ISNULL(I.monAverageCost,0) AS monAverageCost,
					ISNULL(I.bitFreeShipping,0) AS [bitFreeShipping],
					ISNULL(I.txtItemDesc,'') txtItemDesc,
					ISNULL(I.vcModelID,'') vcModelID,
					ISNULL(I.vcManufacturer,'') vcManufacturer,
					ISNULL(I.numBarCodeId,'') numBarCodeId,
					ISNULL(I.bitSerialized,0) bitSerialized,
					ISNULL(I.bitKitParent,0) bitKitParent,
					ISNULL(I.bitAssembly,0) bitAssembly,
					ISNULL(I.IsArchieve,0) IsArchieve,
					ISNULL(I.bitLotNo,0) bitLotNo,
					ISNULL(WItems.monWListPrice,0) monWListPrice,
					ISNULL(I.bitTaxable, 0) bitTaxable,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numOnHand),0) AS VARCHAR(10)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numOnHand,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numOnOrder),0) AS VARCHAR(10)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numOnOrder,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numAllocation),0) AS VARCHAR(10)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numAllocation,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numBackOrder),0) AS VARCHAR(10)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numBackOrder,
					case when ISNULL(I.bitAssembly,0)=1 then dbo.fn_GetAssemblyPossibleWOQty(I.numItemCode) else 0 end AS numMaxWorkOrderQty,
					ISNULL((SELECT COUNT(*) FROM OppWarehouseSerializedItem WHERE numOppId=@numOppID AND numOppItemID=OI.numoppitemtCode),0) AS numSerialNoAssigned,
					(CASE WHEN (SELECT COUNT(*) FROM OpportunityBizDocs OB JOIN dbo.OpportunityBizDocItems OBI ON OB.numOppBizDocsId=OBI.numOppBizDocID WHERE OB.numOppId = OI.numOppId AND OBI.numOppItemID=OI.numoppitemtCode AND OB.numBizDocId=296) > 0 THEN 1 ELSE 0 END) AS bitAddedFulFillmentBizDoc,
					ISNULL(OI.vcAttrValues,'') AS AttributeIDs
--					0 AS [Tax0],0 AS bitTaxable0
            FROM    dbo.OpportunityItems OI
                    INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OI.numOppId
                    INNER JOIN dbo.Item I ON I.numItemCode = OI.numItemCode
                    LEFT JOIN UOM U ON U.numUOMId = OI.numUOMId
                    LEFT JOIN WareHouseItems WItems ON WItems.numWareHouseItemID = OI.numWarehouseItmsID
                    LEFT JOIN Warehouses W ON W.numWarehouseID = WItems.numWarehouseID
            WHERE   OI.numOppId = @numOppID
                    AND OM.numDomainId = @numDomainId
                    
		SELECT  vcSerialNo,
				vcComments AS Comments,
				numOppItemID AS numoppitemtCode,
				W.numWarehouseItmsDTLID,
				numWarehouseItmsID AS numWItmsID,
				dbo.Fn_getattributes(W.numWarehouseItmsDTLID, 1) Attributes
		FROM    WareHouseItmsDTL W
				JOIN OppWarehouseSerializedItem O ON O.numWarehouseItmsDTLID = W.numWareHouseItmsDTLID
		WHERE   numOppID = @numOppID                
                   
        END
        
        
		IF @tintMode = 5 -- Get WebApi Order Items to enter WebApi Order line Item Details 
		BEGIN 
			SELECT OI.numOppItemtCode,OI.numOppId,OI.numItemCode,
			ISNULL((SELECT vcWHSKU FROM dbo.WareHouseItems WHERE WareHouseItems.numWarehouseItemID = OI.numWarehouseItmsID),ISNULL(IT.vcSKU,'')) AS [vcSKU],
			OI.vcItemDesc,OMAPI.vcAPIOppId
			FROM OpportunityItems OI 
			INNER JOIN dbo.OpportunityMaster OM ON OI.numOppId = OM.numOppId 
			INNER JOIN dbo.OpportunityMasterAPI OMAPI ON OM.numOppId = OMAPI.numOppId
			INNER JOIN dbo.Item IT ON OI.numItemCode = IT.numItemCode
			WHERE OI.numOppId = @numOppID and OM.numDomainID = @numDomainId 
        END

END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetPriceLevelAndDiscount')
DROP PROCEDURE USP_GetPriceLevelAndDiscount
GO
CREATE PROCEDURE [dbo].[USP_GetPriceLevelAndDiscount]     
(    
    @numDomainID NUMERIC(18, 0),
    @numItemCode NUMERIC(9),
    @numWareHouseItemID NUMERIC(9)
)   
AS

	CREATE TABLE #tmpPriceLevel ( numItemCode NUMERIC(9),numWareHouseItemID NUMERIC(9), monHighestPrice NUMERIC(18), monHighestDiscount NUMERIC(18) )
	
	DECLARE @monHighestDiscount AS NUMERIC(18);
	DECLARE @monHighestPrice AS NUMERIC(18);
	
    DECLARE @monListPrice AS NUMERIC(18,2) ;
        SET @monListPrice = 0
    DECLARE @monVendorCost AS NUMERIC(18,2) ;
        SET @monVendorCost = 0

    IF ( ( @numWareHouseItemID > 0 )
         AND EXISTS ( SELECT    *
                      FROM      item
                      WHERE     numItemCode = @numItemCode
                                AND charItemType = 'P' )
       ) 
        BEGIN      
            SELECT  @monListPrice = ISNULL(monWListPrice, 0)
            FROM    WareHouseItems
            WHERE   numWareHouseItemID = @numWareHouseItemID      
            IF @monListPrice = 0 
                SELECT  @monListPrice = monListPrice
                FROM    Item
                WHERE   numItemCode = @numItemCode       
        END      
    ELSE 
        BEGIN      
            SELECT  @monListPrice = monListPrice
            FROM    Item
            WHERE   numItemCode = @numItemCode      
        END 

    SELECT  @monVendorCost = dbo.[fn_GetVendorCost](@numItemCode)
	
	SELECT TOP 1 @monHighestPrice = CASE WHEN tintRuleType = 1
							  AND tintDiscountType = 1 --Deduct from List price & Percentage
							  THEN @monListPrice - ( @monListPrice * ( decDiscount / 100 ) )
							 WHEN tintRuleType = 1
								  AND tintDiscountType = 2 --Deduct from List price & Flat discount
								  THEN @monListPrice - decDiscount
							 WHEN tintRuleType = 2
								  AND tintDiscountType = 1 --Add to Primary Vendor Cost & Percentage
								  THEN @monVendorCost + ( @monVendorCost * ( decDiscount / 100 ) )
							 WHEN tintRuleType = 2
								  AND tintDiscountType = 2 --Add to Primary Vendor Cost & Flat discount
								  THEN @monVendorCost + decDiscount
							 WHEN tintRuleType = 3 --Named Price
								  THEN decDiscount
							 END,
			@monHighestDiscount = CASE WHEN tintRuleType = 1 AND tintDiscountType = 1 --Deduct from List price & Percentage
										 THEN decDiscount 
										 WHEN tintRuleType = 1 AND tintDiscountType = 2 --Deduct from List price & Flat discount
										 THEN (decDiscount * 100) / (@monListPrice - decDiscount)
										 WHEN tintRuleType = 2 AND tintDiscountType = 1 --Add to Primary Vendor Cost & Percentage
										 THEN decDiscount
										 WHEN tintRuleType = 2 AND tintDiscountType = 2 --Add to Primary Vendor Cost & Flat discount
										 THEN (decDiscount * 100) / (@monVendorCost + decDiscount)
										 WHEN tintRuleType = 3 --Named Price 
										 THEN 0
							 END				 
    FROM    [PricingTable]
    WHERE   ISNULL(numItemCode, 0) = @numItemCode
    ORDER BY [numPricingID] 

    
    INSERT INTO #tmpPriceLevel (numItemCode,numWareHouseItemID,monHighestPrice,monHighestDiscount) 
    VALUES (@numItemCode, @numWareHouseItemID, ISNULL(@monHighestPrice,0), ISNULL(@monHighestDiscount,0))
    
SELECT numItemCode,numWareHouseItemID, monHighestPrice, monHighestDiscount FROM #tmpPriceLevel

IF OBJECT_ID('tempdb..#tmpPriceLevel') IS NOT NULL
	DROP TABLE #tmpPriceLevel
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetSitesDetail')
DROP PROCEDURE USP_GetSitesDetail
GO
CREATE PROCEDURE [dbo].[USP_GetSitesDetail] @numSiteID NUMERIC(9)
AS 
    SELECT  S.[numDomainID],
            D.[numDivisionID],
            ISNULL(S.[bitIsActive], 1),
            ISNULL(E.[numDefaultWareHouseID],0) numDefaultWareHouseID,
            ISNULL(E.[numRelationshipId],0) numRelationshipId,
            ISNULL(E.[numProfileId],0) numProfileId,
            ISNULL(E.[bitEnableDefaultAccounts],0) bitEnableDefaultAccounts,
            ISNULL(E.[numDefaultCOGSChartAcntId],0) numDefaultCOGSChartAcntId,
            ISNULL(E.[numDefaultIncomeChartAcntId],0) numDefaultIncomeChartAcntId,
            ISNULL(E.[numDefaultAssetChartAcntId],0) numDefaultAssetChartAcntId,
            --ISNULL(E.[bitEnableCreditCart],0) bitEnableCreditCart,
            ISNULL(E.[bitHidePriceBeforeLogin],0) AS bitHidePriceBeforeLogin,
			ISNULL(E.[bitHidePriceAfterLogin],0) AS bitHidePriceAfterLogin,
			ISNULL(E.[numRelationshipIdHidePrice],0) AS numRelationshipIdHidePrice,
			ISNULL(E.[numProfileIDHidePrice],0) AS numProfileIDHidePrice,
			dbo.fn_GetContactEmail(1,0,numAdminID) AS DomainAdminEmail,
			S.vcLiveURL,
			ISNULL(E.bitAuthOnlyCreditCard,0) bitAuthOnlyCreditCard,
			ISNULL(D.numDefCountry,0) numDefCountry,
			ISNULL(E.bitSendEMail,0) bitSendMail,
			ISNULL((SELECT numAuthoritativeSales FROM dbo.AuthoritativeBizDocs WHERE numDomainId=S.numDomainID),0) AuthSalesBizDoc,
			ISNULL((SELECT TOP 1 numBizDocId FROM dbo.eCommercePaymentConfig WHERE numSiteID=@numSiteID AND numPaymentMethodId = 27261 AND bitEnable =1 ),0) numCreditTermBizDocID,
			ISNULL((SELECT TOP 1 numBizDocId FROM dbo.eCommercePaymentConfig WHERE numSiteID=@numSiteID AND numPaymentMethodId =1 AND bitEnable =1 ),0) numCreditCardBizDocID,
			ISNULL((SELECT TOP 1 numBizDocId FROM dbo.eCommercePaymentConfig WHERE numSiteID=@numSiteID AND numPaymentMethodId = 31488 AND bitEnable =1 ),0) numGoogleCheckoutBizDocID,
ISNULL((SELECT TOP 1 numBizDocId FROM dbo.eCommercePaymentConfig WHERE numSiteID=@numSiteID AND numPaymentMethodId = 35141 AND bitEnable =1 ),0) numPaypalCheckoutBizDocID,
ISNULL((SELECT TOP 1 numBizDocId FROM dbo.eCommercePaymentConfig WHERE numSiteID=@numSiteID AND numPaymentMethodId = 84 AND bitEnable =1 ),0) numSalesInquiryBizDocID,
			ISNULL(D.bitSaveCreditCardInfo,0) bitSaveCreditCardInfo,
			ISNULL(bitOnePageCheckout,0) bitOnePageCheckout,
			ISNULL(D.tintBaseTaxOnArea,0) tintBaseTaxOnArea,
			ISNULL(D.numShippingServiceItemID,0) numShippingServiceItemID,
			ISNULL(D.bitAutolinkUnappliedPayment,0) bitAutolinkUnappliedPayment,
			ISNULL(D.numDiscountServiceItemID,0) numDiscountServiceItemID,
			ISNULL(D.tintBaseTax,0) AS [tintBaseTax],
			ISNULL(E.bitSkipStep2,0) AS [bitSkipStep2],
			ISNULL(E.bitDisplayCategory,0) AS [bitDisplayCategory],
			ISNULL(E.bitShowPriceUsingPriceLevel,0) AS [bitShowPriceUsingPriceLevel],
			ISNULL(E.[bitPreSellUp],0) [bitPreSellUp],
			ISNULL(E.[bitPostSellUp],0) [bitPostSellUp],
			ISNULL(E.[dcPostSellDiscount],0) [dcPostSellDiscount],
			ISNULL(E.numDefaultClass,0) numDefaultClass
    FROM    Sites S
            INNER JOIN [Domain] D ON D.[numDomainId] = S.[numDomainID]
            LEFT OUTER JOIN [eCommerceDTL] E ON D.[numDomainId] = E.[numDomainId]
    WHERE   S.[numSiteID] = @numSiteID
 
 

--Created By Anoop Jayaraj    
    
    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetTaxAmountOppForTaxItem')
DROP PROCEDURE USP_GetTaxAmountOppForTaxItem
GO
CREATE PROCEDURE [dbo].[USP_GetTaxAmountOppForTaxItem]    
@numDomainID as numeric(9),    
@numTaxItemID as numeric(9),    
@numOppID as numeric(9),    
@numDivisionID as numeric(9),
@numOppBizDocID as numeric(9),
@tintMode AS TINYINT=0    
AS    
IF @tintMode=0 --Order
BEGIN
	SELECT ISNULL(dbo.fn_CalOppItemTotalTaxAmt(@numDomainID,@numTaxItemID,@numOppID,@numOppBizDocID),0)
END
ELSE IF @tintMode=5 OR @tintMode=7 OR @tintMode=8 OR @tintMode=9 --RMA Return
BEGIN
	DECLARE @fltPercentage FLOAT = 0
	DECLARE @TotalTaxAmt AS MONEY = 0    
	DECLARE @numUnitHour AS NUMERIC(18,0) = 0
	DECLARE @ItemAmount AS MONEY
	DECLARE @tintTaxType TINYINT = 1

	IF @numTaxItemID = 1
	BEGIN
		DECLARE @TEMP TABLE
		(
			ID INT IDENTITY(1,1),
			numReturnItemID NUMERIC(18,0),
			numUnitHour NUMERIC(18,0),
			ItemAmount MONEY,
			fltPercentage FLOAT,
			tintTaxType TINYINT
		)

		INSERT INTO @TEMP
		(
			numReturnItemID,
			numUnitHour,
			ItemAmount,
			fltPercentage,
			tintTaxType
		)
		SELECT 
			RI.numReturnItemID,
			(CASE WHEN @tintMode=5 OR @tintMode=6 THEN numUnitHour ELSE numUnitHourReceived END) * dbo.fn_UOMConversion(ISNULL(I.numBaseUnit,0),I.numItemCode,@numDomainID,ISNULL(RI.numUOMId, 0)),
			(CASE WHEN @tintMode=5 OR @tintMode=6 THEN numUnitHour ELSE numUnitHourReceived END) * monPrice,
			ISNULL(OMTI.fltPercentage,0),
			ISNULL(OMTI.tintTaxType,1)
		FROM 
			ReturnItems RI
		JOIN
			Item I
		ON
			RI.numItemCode = I.numItemCode
		JOIN
			OpportunityItemsTaxItems OITI
		ON
			RI.numReturnItemID = OITI.numReturnItemID
			AND RI.numReturnHeaderID = OITI.numReturnHeaderID
		JOIN
			OpportunityMasterTaxItems OMTI
		ON
			OITI.numReturnHeaderID = OMTI.numReturnHeaderID
			AND OITI.numTaxItemID = OMTI.numTaxItemID
			AND OITI.numTaxID = OMTI.numTaxID
		WHERE 
			RI.numReturnHeaderID=@numOppID  
			AND OITI.numTaxItemID = @numTaxItemID

		DECLARE @i AS INT = 1
		DECLARE @COUNT AS INT

		SELECT @COUNT = COUNT(*) FROM @TEMP

		WHILE @i <= @COUNT
		BEGIN
			SELECT 
				@numUnitHour = ISNULL(numUnitHour,0),
				@ItemAmount = ISNULL(ItemAmount,0),
				@fltPercentage = ISNULL(fltPercentage,0),
				@tintTaxType = ISNULL(tintTaxType,1)
			FROM 
				@TEMP 
			WHERE 
				ID = @i

			SET @TotalTaxAmt = @TotalTaxAmt + (CASE WHEN @tintTaxType = 2 THEN (ISNULL(@fltPercentage,0) * ISNULL(@numUnitHour,0)) ELSE ISNULL(@fltPercentage * @ItemAmount / 100,0) END)

			SET @i = @i + 1
		END
	END
	ELSE
	BEGIN
		SELECT 
			@fltPercentage=fltPercentage,
			@tintTaxType=ISNULL(tintTaxType,1)
		FROM 
			OpportunityMasterTaxItems 
		WHERE 
			numReturnHeaderID=@numOppID 
			AND numTaxItemID=@numTaxItemID    

		DECLARE @numReturnItemID AS NUMERIC(18) = 0
		
		SELECT TOP 1 
			@numReturnItemID=numReturnItemID,
			@numUnitHour = (CASE WHEN @tintMode=5 OR @tintMode=6 THEN numUnitHour ELSE numUnitHourReceived END) * dbo.fn_UOMConversion(ISNULL(I.numBaseUnit,0),I.numItemCode,@numDomainID,ISNULL(ReturnItems.numUOMId, 0)),
			@ItemAmount=(CASE WHEN @tintMode=5 OR @tintMode=6 THEN numUnitHour ELSE numUnitHourReceived END) * monPrice 
		FROM 
			ReturnItems
		JOIN
			Item I
		ON
			ReturnItems.numItemCode = I.numItemCode
		WHERE 
			numReturnHeaderID=@numOppID   
		ORDER BY 
			numReturnItemID 


		WHILE @numReturnItemID>0    
		BEGIN
			IF (select COUNT(*) from OpportunityItemsTaxItems where numReturnItemID=@numReturnItemID and numTaxItemID=@numTaxItemID)>0
			BEGIN
				SET @TotalTaxAmt = @TotalTaxAmt + (CASE WHEN @tintTaxType = 2 THEN (ISNULL(@fltPercentage,0) * ISNULL(@numUnitHour,0)) ELSE ISNULL(@fltPercentage * @ItemAmount / 100,0) END)
			END    
				    
			SELECT TOP 1 
				@numReturnItemID=numReturnItemID,
				@numUnitHour = (CASE WHEN @tintMode=5 OR @tintMode=6 THEN numUnitHour ELSE numUnitHourReceived END) * dbo.fn_UOMConversion(ISNULL(I.numBaseUnit,0),I.numItemCode,@numDomainID,ISNULL(ReturnItems.numUOMId, 0)),
				@ItemAmount=(CASE WHEN @tintMode=5 OR @tintMode=6 THEN numUnitHour ELSE numUnitHourReceived END) * monPrice 
			FROM 
				ReturnItems 
			JOIN
				Item I
			ON
				ReturnItems.numItemCode = I.numItemCode
			WHERE 
				numReturnHeaderID=@numOppID  and numReturnItemID>@numReturnItemID   
			ORDER BY 
				numReturnItemID     
				    
			IF @@rowcount=0 SET @numReturnItemID=0    
		END
	END
				 
	SELECT @TotalTaxAmt AS TotalTaxAmt				 
END  
    
/****** Object:  StoredProcedure [dbo].[USP_GetTimeAndExpense]    Script Date: 07/26/2008 16:18:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj    
-- SELECT TOP 1 * FROM [TimeAndExpense] ORDER BY [numCategoryHDRID] DESC 
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_gettimeandexpense')
DROP PROCEDURE usp_gettimeandexpense
GO
CREATE PROCEDURE [dbo].[USP_GetTimeAndExpense]
    @numUserCntID AS NUMERIC(9) = 0,
    @numDomainID AS NUMERIC(9) = 0,
    @strDate AS VARCHAR(10) = '',
    @numCategoryHDRID AS NUMERIC(9) = 0,
    @ClientTimeZoneOffset AS INT,
    @tintTEType AS TINYINT = 0,
    @numCaseId AS NUMERIC(9) = 0,
    @numProId AS NUMERIC(9) = 0,
    @numOppId AS NUMERIC(9) = 0,
    @numStageId AS NUMERIC(9) = 0,
    @numCategory AS TINYINT = 0
AS 
    IF ( @numCategoryHDRID = 0
         AND @tintTEType = 0
       ) 
        BEGIN                                  
            SELECT  CASE WHEN tintTEType = 0 THEN 'T & E'
                         WHEN tintTEType = 1
                         THEN CASE WHEN numCategory = 1
                                   THEN CASE WHEN ( SELECT  tintopptype
                                                    FROM    opportunitymaster
                                                    WHERE   numoppid = TE.numOppId
                                                  ) = 1 THEN 'Sales Time'
                                             ELSE 'Purch Time'
                                        END
                                   ELSE CASE WHEN ( SELECT  tintopptype
                                                    FROM    opportunitymaster
                                                    WHERE   numoppid = TE.numOppId
                                                  ) = 1 THEN 'Sales Exp'
                                             ELSE 'Purch Exp'
                                        END
                              END
                         WHEN tintTEType = 2
                         THEN CASE WHEN numCategory = 1 THEN 'Project Time'
                                   ELSE 'Project Exp'
                              END
                         WHEN tintTEType = 3
                         THEN CASE WHEN numCategory = 1 THEN 'Case Time'
                                   ELSE 'Case Exp'
                              END
                         WHEN tintTEType = 4
                         THEN 'Non-Paid Leave'
                    END AS chrFrom,
                    numCategoryHDRID,
                    tintTEType,
                    CASE WHEN numCategory = 1
                         THEN 'Time (' + ( CONVERT(VARCHAR, DATEADD(minute, -@ClientTimeZoneOffset, dtFromDate))
                                           + '-'
                                           + CONVERT(VARCHAR, DATEADD(minute, -@ClientTimeZoneOffset, dtToDate)) )
                              + ')'
                         WHEN numCategory = 2 AND ISNULL(bitReimburse,0) = 1 AND numType = 2  THEN 'Reimbursable Expense'
                         WHEN numCategory = 2 AND ISNULL(bitReimburse,0) = 1 AND numType = 6 THEN 'Billable + Reimbursable Expense'
                         WHEN numCategory = 2 AND ISNULL(bitReimburse,0) = 0 AND numType = 1 THEN 'Billable Expense'
                         --WHEN numCategory = 2 AND ISNULL(bitReimburse,0) = 0 AND numType = 2 THEN 'Non-Billable Expense'
                         --WHEN numCategory = 3 THEN 'Paid Leave'
                         --WHEN numCategory = 4 THEN 'Non Paid Leave'
                    END AS Category,
                    CASE WHEN numType = 1 THEN 'Billable'
                         WHEN numType = 2 THEN 'Non-Billable'
                         WHEN numType = 5 THEN 'Reimbursable Expense'
                         WHEN numType = 6 THEN 'Billable'
                         --WHEN numType = 3 THEN 'Paid Leave'
                         --WHEN numType = 4 THEN 'Non-Paid'
                    END AS [Type],
                    CASE WHEN numCategory = 1
                         THEN DATEADD(minute, -@ClientTimeZoneOffset,
                                      dtFromDate)
                         ELSE dtFromDate
                    END AS dtFromDate,
                    CASE WHEN numCategory = 1
                         THEN DATEADD(minute, -@ClientTimeZoneOffset, dtToDate)
                         ELSE dtToDate
                    END AS dtToDate,
                    bitFromFullDay,
                    bitToFullDay,
                    CONVERT(DECIMAL(10, 2), monAmount) AS [monAmount],
                    TE.numOppId,
                    OM.vcPOppName,
                    TE.numDivisionID,
                    txtDesc,
                    numUserCntID,
                    ISNULL(ACI.vcFirstName,'') + ' ' + ISNULL(ACI.vcLastName,'') AS [vcEmployee],
                    TE.numDomainID,
                    TE.numContractId,
                    numCaseid,
                    TE.numProid,
                    PM.vcProjectId,
                    numOppBizDocsId,
                    numStageId AS numStageID,
                    numCategory,
                    numtype,
                    CASE WHEN numCategory = 1
                         THEN CONVERT(VARCHAR, CONVERT(FLOAT, DATEDIFF(mi, dtFromDate, dtToDate)) / 60) 
                         WHEN numCategory = 2
                         THEN CONVERT(VARCHAR, CONVERT(DECIMAL(10, 2), monAmount))
                         WHEN numCategory = 3
                         THEN ( CASE WHEN bitFromFullDay = 0 THEN 'HDL' 
                                     WHEN bitFromFullDay = 1 THEN 'FDL'
                                END )
						 WHEN numCategory = 4
                         THEN ( CASE WHEN bitFromFullDay = 0 THEN 'HDL'
                                     WHEN bitFromFullDay = 1 THEN 'FDL'
                                END )
                    END AS Detail
                    ,CONVERT(VARCHAR, CONVERT(FLOAT, DATEDIFF(mi, dtFromDate, dtToDate)) / 60) [TimeValue]
                    ,CASE WHEN numCategory = 1 THEN CONVERT(DECIMAL(10, 2), (CONVERT(FLOAT, DATEDIFF(mi, dtFromDate, dtToDate)) / 60) * ISNULL(TE.monAmount,1)) 
						  WHEN numCategory = 2 THEN CONVERT(DECIMAL(10, 2),ISNULL(TE.monAmount,1))
						  ELSE 0
						  END AS [ExpenseValue]
                    ,(SELECT ISNULL(numItemCode,0) FROM item  WHERE numItemCode IN (SELECT numItemCode FROM dbo.OpportunityItems WHERE numoppitemtCode = TE.numOppItemID )) AS [numItemCode]
					,(SELECT ISNULL(numClassID,0) FROM dbo.OpportunityItems WHERE numoppitemtCode = TE.numOppItemID ) AS [numClassID]
            FROM    TimeAndExpense TE
            LEFT JOIN dbo.OpportunityMaster OM ON om.numoppId = TE.numOppId AND TE.numDomainID = OM.numDomainID
            LEFT JOIN dbo.ProjectsMaster PM ON PM.numProId = TE.numProID AND TE.numDomainID = PM.numDomainID
            LEFT JOIN AdditionalContactsInformation ACI ON ACI.numContactID = TE.numUserCntID
            WHERE   (numUserCntID = @numUserCntID OR @numUserCntID = 0)
                    AND TE.numDomainID = @numDomainID
                    AND (TE.numType = @numCategory OR @numCategory = 0)
                    AND                                   
--  (@strDate between dateadd(minute,-@ClientTimeZoneOffset,dtFromDate) and dateadd(minute,-@ClientTimeZoneOffset,dtToDate)   
-- or( day(dateadd(minute,-@ClientTimeZoneOffset,dtFromDate))=day(@strDate)   
--and month(dateadd(minute,-@ClientTimeZoneOffset,dtFromDate))=month(@strDate)   
--and year(dateadd(minute,-@ClientTimeZoneOffset,dtFromDate))=year(@strDate)))       
                    ( 
--						( @strDate >= CASE WHEN numCategory = 1
--                                         THEN DATEADD(minute,
--                                                      -@ClientTimeZoneOffset,
--                                                      dtFromDate)
--                                         ELSE dtFromDate
--                                    END
--                        AND @strDate <= CASE WHEN numCategory = 1
--                                             THEN DATEADD(minute,
--                                                          -@ClientTimeZoneOffset,
--                                                          dttoDate)
--                                             ELSE dttoDate
--                                        END
--                      )
--                      OR 
                      ( DAY(@strDate) = CASE WHEN numCategory = 1
                                                THEN DAY(DATEADD(minute, -@ClientTimeZoneOffset, dtFromDate))
                                                ELSE DAY(dtfromdate)
                                           END
                           AND MONTH(@strDate) = CASE WHEN numCategory = 1
                                                      THEN MONTH(DATEADD(minute, -@ClientTimeZoneOffset, dtFromDate))
                                                      ELSE MONTH(dtfromdate)
                                                 END
                           AND YEAR(@strDate) = CASE WHEN numCategory = 1
                                                     THEN YEAR(DATEADD(minute, -@ClientTimeZoneOffset, dtFromDate))
                                                     ELSE YEAR(dtfromdate)
                                                END
                         )
                    )                       
                
        END                                  
    ELSE 
        BEGIN              
      
DECLARE @TotalTime as DECIMAL(10, 2),@TotalExpense as DECIMAL(10, 2);
SET @TotalTime=0;SET @TotalExpense=0

DECLARE @TimeBudget as DECIMAL(10, 2),@ExpenseBudget as DECIMAL(10, 2);
SET @TimeBudget=0;SET @ExpenseBudget=0

DECLARE @bitTimeBudget as bit,@bitExpenseBudget as BIT
SET @bitTimeBudget = 0;SET @bitExpenseBudget=0


SELECT @TotalTime=isnull(CONVERT(DECIMAL(10, 2), CAST( SUM(monamount * CAST(CAST(DATEDIFF(minute, dtFromDate, dtToDate) AS DECIMAL(10,2))/60 AS DECIMAL(10,2)) ) AS DECIMAL(10,2))),0)
from  timeandexpense where numProID=@numProId and numStageID=@numStageId AND numCategory = 1 and tintTEType = 2 
AND numType= 1 GROUP BY numType

SELECT @TotalExpense=isnull([dbo].[fn_GetExpenseDtlsbyProStgID](@numProId,@numStageId,1),0)


select @TimeBudget=isnull(monTimeBudget,0),@ExpenseBudget=isnull(monExpenseBudget,0),@bitTimeBudget=isnull(bitTimeBudget,0),@bitExpenseBudget=isnull(bitExpenseBudget,0) from StagePercentageDetails where numProjectID=@numProId and numStageDetailsId=@numStageId

if @bitTimeBudget=1
   SET @TotalTime=@TimeBudget-@TotalTime

if @bitExpenseBudget=1
   SET @TotalExpense=@TotalExpense-@TotalExpense
            DECLARE @sql AS VARCHAR(8000)                              
            SET @sql = '          
 select           
 isnull(numCategoryHDRID,0)as numCategoryHDRID,                                  
  numCategory,                                  
  numType,                                  
  Case when numCategory =1 then dateadd(minute,' + CONVERT(VARCHAR(20), -@ClientTimeZoneOffset) + ',dtFromDate) else  dtFromDate end as dtFromDate,                                  
  Case when numCategory =1 then dateadd(minute,' + CONVERT(VARCHAR(20), -@ClientTimeZoneOffset) + ',dtToDate) else dtToDate end as dtToDate ,                                   
  bitFromFullDay,                                  
  bitToFullDay,                                  
  convert(decimal(10,2),monAmount) as monAmount,                                  
  numOppId,
  Case When numOppId>0 then dbo.GetOppName(T.numOppId) else '''' end as  vcPOppName,                     
  T.numDivisionID,                            
  D.numCompanyID,                                  
  txtDesc,                                  
numContractId,                      
numCaseid,                      
numProid,
Case When numProid>0 then (select vcProjectID from ProjectsMaster where numProId=T.numProId and numDomainId=T.numDomainId) else '''' end as  vcProjectID,                     
  numUserCntID,                                  
  T.numDomainID  ,                    
numOppBizDocsId  ,          
numStageId ,
Case when numStageId>0 and  numProid>0 then dbo.fn_GetProjectStageName(T.numProid,T.numStageId) else '''' end as vcProjectStageName,
bitReimburse,
Case when numCategory =2 then (SELECT ISNULL([numChartAcntId],0) FROM [General_Journal_Header] WHERE [numCategoryHDRID]=' + CONVERT(VARCHAR(20), @numCategoryHDRID) + ')
else 0 END as numExpenseAccountID
,' + CONVERT(VARCHAR(20),@TotalTime)+ ' TotalTime,' + CONVERT(VARCHAR(20),@TotalExpense)+ ' TotalExpense
,' + CONVERT(VARCHAR(20),@bitTimeBudget) + ' bitTimeBudget,' + CONVERT(VARCHAR(20),@bitExpenseBudget) + ' bitExpenseBudget
,(SELECT ISNULL(numItemCode,0) FROM item  WHERE numItemCode IN (SELECT numItemCode FROM dbo.OpportunityItems WHERE numoppitemtCode = numOppItemID )) AS [numItemCode]
,(SELECT ISNULL(numClassID,0) FROM dbo.OpportunityItems WHERE numoppitemtCode = numOppItemID ) AS [numClassID]
 from                                   
TimeAndExpense T                            
Left join DivisionMaster D                            
on  D.numDivisionID=T.numDivisionID                                   
 where  T.numDomainID=' + CONVERT(VARCHAR(20), @numDomainID)        
    
            IF @numCategoryHDRID <> 0
                AND @tintTEType <> 0 
                SET @sql = @sql + ' and numCategoryHDRID='
                    + CONVERT(VARCHAR(20), @numCategoryHDRID)    
            ELSE 
                BEGIN    
                    IF @tintTEType = 0 
                        SET @sql = @sql
                            + ' and tintTEType = 0 and numCategoryHDRID='
                            + CONVERT(VARCHAR(20), @numCategoryHDRID)
                            + '          
   and numUserCntID=' + CONVERT(VARCHAR(20), @numUserCntID)          
                    IF @tintTEType = 1 
                        SET @sql = @sql
                            + ' and tintTEType = 1  and numOppId = '
                            + CONVERT(VARCHAR(20), @numOppId) + '           
   and numStageId = ' + CONVERT(VARCHAR(20), @numStageId)
                            + ' and numCategory = '
                            + CONVERT(VARCHAR(20), @numCategory)          
                    IF @tintTEType = 2 
                        SET @sql = @sql
                            + ' and tintTEType = 2  and numProid = '
                            + CONVERT(VARCHAR(20), @numProid) + '           
   and numStageId = ' + CONVERT(VARCHAR(20), @numStageId)
                            + ' and numCategory = '
                            + CONVERT(VARCHAR(20), @numCategory)          
                    IF @tintTEType = 3 
                        SET @sql = @sql
                            + ' and tintTEType = 3  and numCaseid = '
                            + CONVERT(VARCHAR(20), @numCaseid) + '            
   and numStageId = ' + CONVERT(VARCHAR(20), @numStageId)
                            + ' and numCategory = '
                            + CONVERT(VARCHAR(20), @numCategory)          
                END    
            PRINT @sql          
            EXEC (@sql)          
                             
        END
GO
/****** Object:  StoredProcedure [dbo].[USP_GetTimeExpWeekly]    Script Date: 07/26/2008 16:18:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec USP_GetTimeExpWeekly 1,1,'8/6/2006'                
--created by anoop jayaraj                
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_gettimeexpweekly')
DROP PROCEDURE usp_gettimeexpweekly
GO
CREATE PROCEDURE [dbo].[USP_GetTimeExpWeekly]                
@numUserCntID as numeric(9)=0,                
@numDomainID as numeric(9)=0,                
@FromDate as datetime ,              
@numCategoryType as int=0 ,        
@ClientTimeZoneOffset as int              
as          
declare  @strDate as datetime     
set @strDate = @fromDate    

IF ISNULL(@numUserCntID,0) > 0
BEGIN
	
select      
'-'   as day0,    
 dbo.GetDaydetails(@numUserCntID,@strDate,@numCategoryType,@ClientTimeZoneOffset) as Day1,                
 dbo.GetDaydetails(@numUserCntID,convert(char(10),dateadd(day,1,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)   as Day2,                
 dbo.GetDaydetails(@numUserCntID,convert(char(10),dateadd(day,2,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)   as Day3,                
 dbo.GetDaydetails(@numUserCntID,convert(char(10),dateadd(day,3,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)   as Day4,                
 dbo.GetDaydetails(@numUserCntID,convert(char(10),dateadd(day,4,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)   as Day5,                
 dbo.GetDaydetails(@numUserCntID,convert(char(10),dateadd(day,5,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)   as Day6,                
 dbo.GetDaydetails(@numUserCntID,convert(char(10),dateadd(day,6,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)   as Day7,                
 dbo.GetDaydetails(@numUserCntID,convert(char(10),dateadd(day,7,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)   as Day8,    
 dbo.GetDaydetails(@numUserCntID,convert(char(10),dateadd(day,8,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)   as Day9,    
 dbo.GetDaydetails(@numUserCntID,convert(char(10),dateadd(day,9,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)   as Day10,    
 dbo.GetDaydetails(@numUserCntID,convert(char(10),dateadd(day,10,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)  as Day11,    
 dbo.GetDaydetails(@numUserCntID,convert(char(10),dateadd(day,11,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)  as Day12,                
 dbo.GetDaydetails(@numUserCntID,convert(char(10),dateadd(day,12,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)  as Day13,                
 dbo.GetDaydetails(@numUserCntID,convert(char(10),dateadd(day,13,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)  as Day14,                
 dbo.GetDaydetails(@numUserCntID,convert(char(10),dateadd(day,14,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)  as Day15,                
 dbo.GetDaydetails(@numUserCntID,convert(char(10),dateadd(day,15,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)  as Day16,                
 dbo.GetDaydetails(@numUserCntID,convert(char(10),dateadd(day,16,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)  as Day17,                
 dbo.GetDaydetails(@numUserCntID,convert(char(10),dateadd(day,17,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)  as Day18,    
 dbo.GetDaydetails(@numUserCntID,convert(char(10),dateadd(day,18,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)  as Day19,    
 dbo.GetDaydetails(@numUserCntID,convert(char(10),dateadd(day,19,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)  as Day20,    
 dbo.GetDaydetails(@numUserCntID,convert(char(10),dateadd(day,20,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)  as Day21,    
 dbo.GetDaydetails(@numUserCntID,convert(char(10),dateadd(day,21,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)  as Day22,                
 dbo.GetDaydetails(@numUserCntID,convert(char(10),dateadd(day,22,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)  as Day23,                
 dbo.GetDaydetails(@numUserCntID,convert(char(10),dateadd(day,23,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)  as Day24,                
 dbo.GetDaydetails(@numUserCntID,convert(char(10),dateadd(day,24,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)  as Day25,                
 dbo.GetDaydetails(@numUserCntID,convert(char(10),dateadd(day,25,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)  as Day26,                
 dbo.GetDaydetails(@numUserCntID,convert(char(10),dateadd(day,26,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)  as Day27,                
 dbo.GetDaydetails(@numUserCntID,convert(char(10),dateadd(day,27,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)  as Day28,    
 dbo.GetDaydetails(@numUserCntID,convert(char(10),dateadd(day,28,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)  as Day29,    
 dbo.GetDaydetails(@numUserCntID,convert(char(10),dateadd(day,29,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)  as Day30,    
 dbo.GetDaydetails(@numUserCntID,convert(char(10),dateadd(day,30,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)  as Day31,    
     
    
(        
 select isnull(sum(convert(float,DATEDIFF(mi, dtFromDate, dtToDate))/60),0)  as [time]               
 from TimeAndExpense where (dateadd(minute,-@ClientTimeZoneOffset,dtFromDate) between @strDate and dateadd(day,30,@strDate))                 
 and numCategory=1 and numUserCntID=@numUserCntID and numType=1 )        
        
as BillableHours,          
           
(        
 select isnull(sum(convert(float,DATEDIFF(mi, dtFromDate, dtToDate))/60),0)  as [time]               
 from TimeAndExpense where (dateadd(minute,-@ClientTimeZoneOffset,dtFromDate) between @strDate and dateadd(day,30,@strDate))                 
 and numCategory=1 and numUserCntID=@numUserCntID and numType=2 )        
        
as NonBillableHours,             
 (        
 select round(isnull(sum(monAmount),0),1)    as  monAmount           
 from TimeAndExpense                 
 where     
( dtFromDate   
between @strDate and dateadd(day,30,@strDate))                 
 and numCategory=2 and numUserCntID=@numUserCntID and numType=1 )        
        
        
 as BillableAmount,                
 (        
 select round(isnull(sum(monAmount),0),1)    as  monAmount           
 from TimeAndExpense                 
 where ( dtFromDate   
 between @strDate and dateadd(day,30,@strDate))                 
 and numCategory=2 and numUserCntID=@numUserCntID and numType=2 )  as NonBillableAmount

END

ELSE
BEGIN

select      
'-'   as day0,   		
 dbo.GetDayDetailsForDomain(@numDomainID,@strDate,@numCategoryType,@ClientTimeZoneOffset) as Day1,                
 dbo.GetDayDetailsForDomain(@numDomainID,convert(char(10),dateadd(day,1,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)   as Day2,                
 dbo.GetDayDetailsForDomain(@numDomainID,convert(char(10),dateadd(day,2,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)   as Day3,                
 dbo.GetDayDetailsForDomain(@numDomainID,convert(char(10),dateadd(day,3,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)   as Day4,                
 dbo.GetDayDetailsForDomain(@numDomainID,convert(char(10),dateadd(day,4,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)   as Day5,                
 dbo.GetDayDetailsForDomain(@numDomainID,convert(char(10),dateadd(day,5,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)   as Day6,                
 dbo.GetDayDetailsForDomain(@numDomainID,convert(char(10),dateadd(day,6,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)   as Day7,                
 dbo.GetDayDetailsForDomain(@numDomainID,convert(char(10),dateadd(day,7,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)   as Day8,    
 dbo.GetDayDetailsForDomain(@numDomainID,convert(char(10),dateadd(day,8,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)   as Day9,    
 dbo.GetDayDetailsForDomain(@numDomainID,convert(char(10),dateadd(day,9,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)   as Day10,    
 dbo.GetDayDetailsForDomain(@numDomainID,convert(char(10),dateadd(day,10,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)  as Day11,    
 dbo.GetDayDetailsForDomain(@numDomainID,convert(char(10),dateadd(day,11,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)  as Day12,                
 dbo.GetDayDetailsForDomain(@numDomainID,convert(char(10),dateadd(day,12,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)  as Day13,                
 dbo.GetDayDetailsForDomain(@numDomainID,convert(char(10),dateadd(day,13,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)  as Day14,                
 dbo.GetDayDetailsForDomain(@numDomainID,convert(char(10),dateadd(day,14,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)  as Day15,                
 dbo.GetDayDetailsForDomain(@numDomainID,convert(char(10),dateadd(day,15,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)  as Day16,                
 dbo.GetDayDetailsForDomain(@numDomainID,convert(char(10),dateadd(day,16,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)  as Day17,                
 dbo.GetDayDetailsForDomain(@numDomainID,convert(char(10),dateadd(day,17,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)  as Day18,    
 dbo.GetDayDetailsForDomain(@numDomainID,convert(char(10),dateadd(day,18,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)  as Day19,    
 dbo.GetDayDetailsForDomain(@numDomainID,convert(char(10),dateadd(day,19,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)  as Day20,    
 dbo.GetDayDetailsForDomain(@numDomainID,convert(char(10),dateadd(day,20,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)  as Day21,    
 dbo.GetDayDetailsForDomain(@numDomainID,convert(char(10),dateadd(day,21,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)  as Day22,                
 dbo.GetDayDetailsForDomain(@numDomainID,convert(char(10),dateadd(day,22,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)  as Day23,                
 dbo.GetDayDetailsForDomain(@numDomainID,convert(char(10),dateadd(day,23,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)  as Day24,                
 dbo.GetDayDetailsForDomain(@numDomainID,convert(char(10),dateadd(day,24,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)  as Day25,                
 dbo.GetDayDetailsForDomain(@numDomainID,convert(char(10),dateadd(day,25,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)  as Day26,                
 dbo.GetDayDetailsForDomain(@numDomainID,convert(char(10),dateadd(day,26,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)  as Day27,                
 dbo.GetDayDetailsForDomain(@numDomainID,convert(char(10),dateadd(day,27,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)  as Day28,    
 dbo.GetDayDetailsForDomain(@numDomainID,convert(char(10),dateadd(day,28,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)  as Day29,    
 dbo.GetDayDetailsForDomain(@numDomainID,convert(char(10),dateadd(day,29,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)  as Day30,    
 dbo.GetDayDetailsForDomain(@numDomainID,convert(char(10),dateadd(day,30,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)  as Day31,    
    
(        
 select isnull(sum(convert(float,DATEDIFF(mi, dtFromDate, dtToDate))/60),0)  as [time]               
 from TimeAndExpense where (dateadd(minute,-330,dtFromDate) between @strDate and dateadd(day,30,@strDate))                 
 and numCategory=1 and numDomainID=1 and numType=1 )        
        
as BillableHours,          
           
(        
 select isnull(sum(convert(float,DATEDIFF(mi, dtFromDate, dtToDate))/60),0)  as [time]               
 from TimeAndExpense where (dateadd(minute,-330,dtFromDate) between @strDate and dateadd(day,30,@strDate))                 
 and numCategory=1 and numDomainID=1 and numType=2 )        
        
as NonBillableHours,             
 (        
 select round(isnull(sum(monAmount),0),1)    as  monAmount           
 from TimeAndExpense                 
 where     
( dtFromDate   
between @strDate and dateadd(day,30,@strDate))                 
 and numCategory=2 and numDomainID=1 and numType=1 )        
        
        
 as BillableAmount,                
 (        
 select round(isnull(sum(monAmount),0),1)    as  monAmount           
 from TimeAndExpense                 
 where ( dtFromDate   
 between @strDate and dateadd(day,30,@strDate))                 
 and numCategory=2 and numDomainID=1 and numType=2 )  as NonBillableAmount

END 
GO
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetTrialBalance')
DROP PROCEDURE USP_GetTrialBalance
GO
CREATE PROCEDURE [dbo].[USP_GetTrialBalance]                                 
@numDomainId AS NUMERIC(9),                                                  
@dtFromDate AS DATETIME,                                                
@dtToDate AS DATETIME,
@ClientTimeZoneOffset INT, 
@numAccountClass AS NUMERIC(9)=0,
@DateFilter AS VARCHAR(20),
@ReportColumn AS VARCHAR(20)                                                        
AS                                                                
BEGIN        
	DECLARE @PLCHARTID NUMERIC(18,0) = 0

	SELECT @PLCHARTID=COA.numAccountId FROM Chart_of_Accounts COA WHERE numDomainID=@numDomainId AND bitProfitLoss=1

	DECLARE @FinancialYearStartDate AS DATETIME
	SELECT @FinancialYearStartDate = dbo.GetFiscalStartDate(YEAR(GETDATE()),@numDomainId)

	DECLARE @TEMPQuarter AS TABLE
	(
		ID INT,
		StartDate DATETIME,
		EndDate DATETIME
	)

	INSERT INTO @TEMPQuarter VALUES (1,@FinancialYearStartDate,DATEADD(ms,-3,DATEADD(QUARTER,1,@FinancialYearStartDate)))
	INSERT INTO @TEMPQuarter VALUES (2,DATEADD(QUARTER,1,@FinancialYearStartDate),DATEADD(ms,-3,DATEADD(QUARTER,2,@FinancialYearStartDate)))
	INSERT INTO @TEMPQuarter VALUES (3,DATEADD(QUARTER,2,@FinancialYearStartDate),DATEADD(ms,-3,DATEADD(QUARTER,3,@FinancialYearStartDate)))
	INSERT INTO @TEMPQuarter VALUES (4,DATEADD(QUARTER,3,@FinancialYearStartDate),DATEADD(ms,-3,DATEADD(QUARTER,4,@FinancialYearStartDate)))

	IF @DateFilter = 'CurYear'
	BEGIN
		SELECT @dtFromDate = @FinancialYearStartDate,
			@dtToDate = DATEADD(ms,-3,DATEADD(YEAR,1,@FinancialYearStartDate));
	END
	ELSE IF @DateFilter = 'PreYear'
	BEGIN
		SELECT @dtFromDate = DATEADD(YEAR,-1,@FinancialYearStartDate),
			@dtToDate = DATEADD(ms,-3,DATEADD(YEAR,1,DATEADD(YEAR,-1,@FinancialYearStartDate)));
	END
	ELSE IF @DateFilter = 'CurPreYear'
	BEGIN
		SELECT @dtFromDate = DATEADD(YEAR,-1,@FinancialYearStartDate),
			@dtToDate = DATEADD(ms,-3,DATEADD(YEAR,1,@FinancialYearStartDate));
	END
	ELSE IF @DateFilter = 'CuQur'
	BEGIN
		SELECT @dtFromDate = StartDate, @dtToDate=EndDate FROM @TEMPQuarter WHERE GetDate() BETWEEN StartDate AND EndDate
	END
	ELSE IF @DateFilter = 'CurPreQur'
	BEGIN
		SELECT @dtFromDate = DATEADD(QUARTER,-1,StartDate), @dtToDate=EndDate FROM @TEMPQuarter WHERE GetDate() BETWEEN StartDate AND EndDate
	END
	ELSE IF @DateFilter = 'PreQur'
	BEGIN
		SELECT @dtFromDate = DATEADD(QUARTER,-1,StartDate), @dtToDate=DATEADD(QUARTER,-1,EndDate) FROM @TEMPQuarter WHERE GetDate() BETWEEN StartDate AND EndDate
	END
	ELSE IF @DateFilter = 'LastMonth'
	BEGIN
		select @dtFromDate = DATEADD(mm,-1,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)),
				@dtToDate = DATEADD(ms,-3,DATEADD(mm,0,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)));  
	END
	ELSE IF @DateFilter = 'ThisMonth'
	BEGIN
		select @dtFromDate = DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0),
				@dtToDate = DATEADD(ms,- 3,DATEADD(mm,0,DATEADD(mm,DATEDIFF(mm,0,GETDATE())+1,0)));  
	END
	ELSE IF @DateFilter = 'CurPreMonth'
	BEGIN
		select @dtFromDate = DATEADD(mm,-1,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)),
				@dtToDate = DATEADD(ms,- 3,DATEADD(mm,0,DATEADD(mm,DATEDIFF(mm,0,GETDATE())+1,0))); 
	END

	CREATE TABLE #view_journal
	(
		numAccountId NUMERIC(18,0),
		vcAccountCode VARCHAR(50),
		COAvcAccountCode VARCHAR(100),
		datEntry_Date SMALLDATETIME,
		Debit MONEY,
		Credit MONEY
	)

	INSERT INTO #view_journal SELECT numAccountId,vcAccountCode,COAvcAccountCode,datEntry_Date,Debit,Credit FROM view_journal WHERE numDomainId = @numDomainID AND (numAccountClass=@numAccountClass OR @numAccountClass=0);

	DECLARE @dtFinYearFromJournal DATETIME ;
	DECLARE @dtFinYearFrom DATETIME ;

	SELECT @dtFinYearFromJournal = MIN(datEntry_Date) FROM #view_journal
	SET @dtFinYearFrom = (SELECT dtPeriodFrom FROM FINANCIALYEAR WHERE dtPeriodFrom <= @dtFromDate AND dtPeriodTo >= @dtFromDate AND numDomainId = @numDomainId)

	

	;WITH DirectReport (ParentId, numAccountTypeID, vcAccountType,vcAccountCode, LEVEL,Struc)
	AS
	(
		SELECT 
			CAST(0 AS NUMERIC(18)) AS ParentId, 
			[ATD].[numAccountTypeID], 
			[ATD].[vcAccountType],
			[ATD].[vcAccountCode],
			1 AS LEVEL, 
			CAST(CAST([ATD].[vcAccountCode] AS VARCHAR) AS VARCHAR(100))  AS Struc
		FROM 
			[dbo].[AccountTypeDetail] AS ATD
		WHERE 
			[ATD].[numDomainID]=@numDomainId 
			AND vcAccountCode IN ('0101','0102','0103','0104','0105','0106')
		UNION ALL
		SELECT 
			ATD.[numParentID] AS ParentId, 
			[ATD].[numAccountTypeID], 
			[ATD].[vcAccountType],
			[ATD].[vcAccountCode],
			LEVEL + 1, 
			CAST(d.Struc + '#' + cast([ATD].[vcAccountCode] AS VARCHAR) AS VARCHAR(100))  AS Struc
		FROM 
			[dbo].[AccountTypeDetail] AS ATD 
		JOIN 
			[DirectReport] D 
		ON 
			D.[numAccountTypeID] = [ATD].[numParentID]
		WHERE 
			[ATD].[numDomainID]=@numDomainId 
	),
	DirectReport1 (ParentId, numAccountTypeID, vcAccountType,vcAccountCode, LEVEL,numAccountId,Struc, bitProfitLoss)
	AS
	(
		SELECT 
			ParentId, 
			numAccountTypeID, 
			vcAccountType,
			vcAccountCode, 
			LEVEL,
			CAST(NULL AS NUMERIC(18)),
			Struc,
			CAST(0 AS BIT)
		FROM 
			DirectReport 
		UNION ALL
		SELECT 
			COA.numParntAcntTypeId AS ParentId,
			CAST(NULL AS NUMERIC(18)), 
			[vcAccountName],
			COA.[vcAccountCode],
			LEVEL + 1,
			[COA].[numAccountId], 
			CAST(d.Struc + '#' + CAST(COA.[vcAccountCode] AS VARCHAR) AS VARCHAR(100)) AS Struc,
			COA.bitProfitLoss
		FROM 
			[dbo].[Chart_of_Accounts] AS COA 
		JOIN 
			[DirectReport1] D 
		ON 
			D.[numAccountTypeID] = COA.[numParntAcntTypeId]
		WHERE 
			[COA].[numDomainID]=@numDomainId 
			AND ISNULL(COA.bitActive,0) = 1
	)

  
	SELECT 
		ParentId, 
		numAccountTypeID, 
		vcAccountType, 
		LEVEL, 
		vcAccountCode,
		numAccountId,
		Struc,
		bitProfitLoss
	INTO 
		#tempDirectReport
	FROM 
		DirectReport1

	DECLARE @columns VARCHAR(8000) = ''
	DECLARE @PivotColumns VARCHAR(8000) = ''
	DECLARE @sql VARCHAR(MAX) = ''

	IF @ReportColumn = 'Year'
	BEGIN
		CREATE TABLE #TempYearMonth
		(
			intYear INT,
			intMonth INT,
			MonthYear VARCHAR(50),
			StartDate DATETIME,
			EndDate DATETIME,
			ProfitLossAmount MONEY
		)
		
		;WITH CTE AS 
		(
			SELECT
				YEAR(@dtFromDate) AS 'yr',
				MONTH(@dtFromDate) AS 'mm',
				DATENAME(mm, @dtFromDate) AS 'mon',
				(DATENAME(mm, @dtFromDate) + '_' + CAST(YEAR(@dtFromDate) AS VARCHAR)) AS MonthYear,
				CAST(DATEFROMPARTS(YEAR(@dtFromDate),MONTH(@dtFromDate),1) AS DATETIME) dtStartDate,
				DATEADD(MS,-2,CAST(DATEADD(MONTH,1,DATEFROMPARTS(YEAR(@dtFromDate),MONTH(@dtFromDate),1)) AS DATETIME)) dtEndDate,
				@dtFromDate 'new_date'
			UNION ALL
			SELECT
				YEAR(DATEADD(d,1,new_date)) AS 'yr',
				MONTH(DATEADD(d,1,new_date)) AS 'mm',
				DATENAME(mm, DATEADD(d,1,new_date)) AS 'mon',
				(DATENAME(mm, DATEADD(d,1,new_date)) + '_' + CAST(YEAR(DATEADD(d,1,new_date)) AS VARCHAR)) AS MonthYear,
				CAST(DATEFROMPARTS(YEAR(DATEADD(d,1,new_date)),MONTH(DATEADD(d,1,new_date)),1) AS DATETIME) dtStartDate,
				DATEADD(MS,-2,CAST(DATEADD(MONTH,1,DATEFROMPARTS(YEAR(DATEADD(d,1,new_date)),MONTH(DATEADD(d,1,new_date)),1)) AS DATETIME)) dtEndDate,
				DATEADD(d,1,new_date) 'new_date'
			FROM CTE
			WHERE DATEADD(d,1,new_date) < @dtToDate
		)
	
		INSERT INTO 
			#tempYearMonth
		SELECT 
			yr,mm,MonthYear,dtStartDate,dtEndDate,0
		FROM 
			CTE
		GROUP BY 
			yr, mm, MonthYear, dtStartDate, dtEndDate
		ORDER BY 
			yr, mm
		OPTION (MAXRECURSION 5000)

		INSERT INTO #tempYearMonth VALUES (5000,5000,'Total',@dtFromDate,@dtToDate,0)

		UPDATE 
			#tempYearMonth
		SET
			EndDate = (CASE WHEN EndDate < @dtToDate THEN EndDate ELSE @dtToDate END)

		UPDATE 
			#tempYearMonth
		SET
			ProfitLossAmount = (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE numAccountId=@PLCHARTID AND vcAccountCode LIKE '0103%' AND datEntry_Date between StartDate and EndDate)
							+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE vcAccountCode LIKE '0104%' AND datEntry_Date between StartDate and EndDate)
							+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE vcAccountCode LIKE '0106%' AND datEntry_Date between StartDate and EndDate)
							+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE vcAccountCode LIKE '0103%' AND datEntry_Date between StartDate and EndDate)
							+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE (vcAccountCode LIKE '0103%' OR vcAccountCode LIKE '0104%' OR vcAccountCode  LIKE '0106%') AND datEntry_Date <=  DATEADD(MS,-3,StartDate))
							+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE numAccountId=@PLCHARTID AND datEntry_Date <=  DATEADD(MS,-3,StartDate))

		SELECT @columns = STUFF((SELECT ', ISNULL(' + MonthYear  + ',0) AS [' + REPLACE(MonthYear,'_',' ') + ']' FROM #tempYearMonth ORDER BY intYear,intMonth FOR XML PATH('')), 1, 2, ''); 
		SELECT @PivotColumns = STUFF((SELECT ', ' + MonthYear FROM #tempYearMonth ORDER BY intYear,intMonth FOR XML PATH('')), 1, 2, ''); 

		SELECT 
			COA.ParentId, 
			COA.numAccountTypeID, 
			COA.vcAccountType, 
			COA.LEVEL, 
			COA.vcAccountCode, 
			COA.numAccountId, 
			COA.Struc,
			Period.MonthYear,
			Period.ProfitLossAmount,
			(CASE 
				WHEN bitProfitLoss = 1 
				THEN -Period.ProfitLossAmount
				ELSE
					(CASE 
						WHEN COA.[vcAccountCode] LIKE '0103%' OR COA.[vcAccountCode] LIKE '0104%' OR COA.[vcAccountCode] LIKE '0106%' THEN 0 
						ELSE ISNULL(t2.OPENING,0) 
					END) + (ISNULL(Debit,0) - ISNULL(Credit,0)) 
			END) AS Amount
		INTO 
			#tempViewDataYear
		FROM 
			#tempDirectReport COA 
		OUTER APPLY
		(
			SELECT
				MonthYear,
				StartDate,
				EndDate,
				ProfitLossAmount
			FROM
				#tempYearMonth
		) AS Period
		OUTER APPLY
		(
			SELECT 
				SUM(Debit) AS Debit,
				SUM(Credit) AS Credit
			FROM
				#view_journal V 
			WHERE  
				V.numAccountID = COA.numAccountID 
				AND datEntry_Date BETWEEN Period.StartDate AND Period.EndDate
		) AS t1
		OUTER APPLY 
		(
			SELECT   
				SUM(Debit - Credit) AS OPENING
			FROM     
				#view_journal VJ
			WHERE    
				VJ.numAccountID = COA.numAccountID 
				AND datEntry_Date BETWEEN @dtFinYearFromJournal AND  DATEADD(MS,-3,Period.StartDate)
		)  AS t2

		SET @sql = 'SELECT
						ParentId,
						numAccountId,
						numAccountTypeID,
						vcAccountType,
						LEVEL,
						vcAccountCode,
						Struc,
						[Type],' + @columns + '
					FROM
					(
						SELECT 
							COA.ParentId, 
							COA.numAccountId,
							COA.numAccountTypeID, 
							COA.vcAccountType, 
							COA.LEVEL, 
							COA.vcAccountCode, 
							(''#'' + COA.Struc + ''#'') AS Struc,
							(CASE WHEN ISNULL(COA.numAccountId,0) > 0 THEN 1 ELSE 2 END) [Type],
							ISNULL(SUM(Amount),0) AS Amount,
							V.MonthYear
						FROM 
							#tempDirectReport COA 
						LEFT OUTER JOIN 
							#tempViewDataYear V 
						ON  
							V.Struc like COA.Struc + ''%'' 
						GROUP BY 
							COA.ParentId, COA.numAccountTypeID, COA.vcAccountType, COA.LEVEL, COA.vcAccountCode, COA.numAccountId,COA.Struc,COA.bitProfitLoss,V.MonthYear,V.ProfitLossAmount
						UNION
						SELECT 
							0, 
							NULL,
							NULL,
							''Total'',
							-1, 
							NULL, 
							''-1'',
							2,
							(SELECT 
								ISNULL(SUM(Debit) - SUM(Credit),0)
							FROM 
								VIEW_JOURNAL 
							WHERE 
								numDomainID = ' + CAST(@numDomainId AS VARCHAR) + '
								AND (vcAccountCode LIKE ''0101%'' 
								OR vcAccountCode LIKE ''0102%''
								OR vcAccountCode LIKE ''0103%''
								OR vcAccountCode LIKE ''0104%''
								OR vcAccountCode LIKE ''0105%''
								OR vcAccountCode LIKE ''0106%'')
								AND datEntry_Date BETWEEN #TempYearMonth.StartDate AND #TempYearMonth.EndDate),
							MonthYear
						FROM
							#TempYearMonth
					) AS SourceTable
					pivot
					(
						SUM(AMOUNT)
						FOR [MonthYear] IN ( ' + @pivotcolumns + ' )
					) AS P 
					ORDER BY Struc, [Type] desc'

		PRINT @sql
		EXECUTE (@sql)

		DROP TABLE #TempYearMonth
		DROP TABLE #tempViewDataYear
	END
	ELSE IF @ReportColumn = 'Quarter'
	BEGIN
		CREATE TABLE #TempYearMonthQuarter
		(
			intYear INT,
			intQuarter INT,
			MonthYear VARCHAR(50),
			StartDate DATETIME,
			EndDate DATETIME,
			ProfitLossAmount MONEY
		)

		;WITH CTE AS 
		(
			SELECT
				dbo.GetFiscalyear(@dtFromDate,@numDomainId) AS 'yr',
				dbo.GetFiscalQuarter(@dtFromDate,@numDomainId) AS 'qq',
				('Q' + cast(dbo.GetFiscalQuarter(@dtFromDate,@numDomainId) AS VARCHAR) + '_' + cast(dbo.GetFiscalyear(@dtFromDate,@numDomainId) AS VARCHAR)) AS MonthYear,
				CASE dbo.GetFiscalQuarter(@dtFromDate,@numDomainId) 
				WHEN 1 THEN dateadd(month,0,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId))
				WHEN 2 THEN dateadd(month,3,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)) 
				WHEN 3 THEN dateadd(month,6,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId))
				WHEN 4 THEN dateadd(month,9,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)) 
				END AS dtStartDate,
				CASE dbo.GetFiscalQuarter(@dtFromDate,@numDomainId) 
				WHEN 1 THEN dateadd(MS,-3,dateadd(month,3,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)))
				WHEN 2 THEN dateadd(MS,-3,dateadd(month,6,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)))
				WHEN 3 THEN dateadd(MS,-3,dateadd(month,9,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)))
				WHEN 4 THEN dateadd(MS,-3,dateadd(month,12,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)))
				END AS dtEndDate,
				@dtFromDate 'new_date'
			UNION ALL
			SELECT
				dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId) AS 'yr',
				dbo.GetFiscalQuarter(DATEADD(d,1,new_date),@numDomainId) AS 'qq',
				('Q' + cast(dbo.GetFiscalQuarter(DATEADD(d,1,new_date),@numDomainId) AS VARCHAR) + '_' + cast(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId) AS VARCHAR)) AS MonthYear,
				CASE dbo.GetFiscalQuarter(DATEADD(d,1,new_date),@numDomainId) 
				WHEN 1 THEN dateadd(month,0,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId))
				WHEN 2 THEN dateadd(month,3,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)) 
				WHEN 3 THEN dateadd(month,6,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId))
				WHEN 4 THEN dateadd(month,9,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)) 
				END AS dtStartDate,
				CASE dbo.GetFiscalQuarter(DATEADD(d,1,new_date),@numDomainId) 
				WHEN 1 THEN dateadd(MS,-3,dateadd(month,3,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)))
				WHEN 2 THEN dateadd(MS,-3,dateadd(month,6,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)))
				WHEN 3 THEN dateadd(MS,-3,dateadd(month,9,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)))
				WHEN 4 THEN dateadd(MS,-3,dateadd(month,12,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)))
				END AS dtEndDate,
				DATEADD(d,1,new_date) 'new_date'
			FROM CTE
			WHERE DATEADD(d,1,new_date) < @dtToDate
		)
	
		INSERT INTO 
			#TempYearMonthQuarter 		
		SELECT 
			yr,qq,MonthYear,dtStartDate,dtEndDate,0
		FROM 
			CTE
		GROUP BY 
			yr, qq, MonthYear, dtStartDate, dtEndDate
		ORDER BY 
			yr, qq
		OPTION
			(MAXRECURSION 5000)

		INSERT INTO #TempYearMonthQuarter VALUES (5000,5000,'Total',@dtFromDate,@dtToDate,0)

		UPDATE 
			#TempYearMonthQuarter
		SET
			EndDate = (CASE WHEN EndDate < @dtToDate THEN EndDate ELSE @dtToDate END)

		UPDATE 
			#TempYearMonthQuarter
		SET
			ProfitLossAmount = (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE numAccountId=@PLCHARTID AND vcAccountCode LIKE '0103%' AND datEntry_Date between StartDate and EndDate)
							+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE vcAccountCode LIKE '0104%' AND datEntry_Date between StartDate and EndDate)
							+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE vcAccountCode LIKE '0106%' AND datEntry_Date between StartDate and EndDate)
							+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE vcAccountCode LIKE '0103%' AND datEntry_Date between StartDate and EndDate)
							+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE (vcAccountCode LIKE '0103%' OR vcAccountCode LIKE '0104%' OR vcAccountCode  LIKE '0106%') AND datEntry_Date <=  DATEADD(MS,-3,StartDate))
							+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE numAccountId=@PLCHARTID AND datEntry_Date <=  DATEADD(MS,-3,StartDate))

		SELECT @columns = STUFF((SELECT ', ISNULL(' + MonthYear  + ',0) AS [' + REPLACE(MonthYear,'_',' ') + ']' FROM #TempYearMonthQuarter ORDER BY intYear,intQuarter FOR XML PATH('')), 1, 2, ''); 
		SELECT @PivotColumns = STUFF((SELECT ', ' + MonthYear FROM #TempYearMonthQuarter ORDER BY intYear,intQuarter FOR XML PATH('')), 1, 2, ''); 

		SELECT 
			COA.ParentId, 
			COA.numAccountTypeID, 
			COA.vcAccountType, 
			COA.LEVEL, 
			COA.vcAccountCode, 
			COA.numAccountId, 
			COA.Struc,
			Period.MonthYear,
			Period.ProfitLossAmount,
			(CASE 
				WHEN bitProfitLoss = 1 
				THEN -Period.ProfitLossAmount
				ELSE
					(CASE 
						WHEN COA.[vcAccountCode] LIKE '0103%' OR COA.[vcAccountCode] LIKE '0104%' OR COA.[vcAccountCode] LIKE '0106%' THEN 0 
						ELSE ISNULL(t2.OPENING,0) 
					END) + (ISNULL(Debit,0) - ISNULL(Credit,0))
			END) AS Amount
		INTO 
			#tempViewDataQuarter
		FROM 
			#tempDirectReport COA 
		OUTER APPLY
		(
			SELECT
				MonthYear,
				StartDate,
				EndDate,
				ProfitLossAmount
			FROM
				#TempYearMonthQuarter
		) AS Period
		OUTER APPLY
		(
			SELECT 
				SUM(Debit) AS Debit,
				SUM(Credit) AS Credit
			FROM
				#view_journal V 
			WHERE  
				V.numAccountID = COA.numAccountID 
				AND datEntry_Date BETWEEN Period.StartDate AND Period.EndDate
		) AS t1
		OUTER APPLY 
		(
			SELECT   
				SUM(Debit - Credit) AS OPENING
			FROM     
				#view_journal VJ
			WHERE    
				VJ.numAccountID = COA.numAccountID 
				AND datEntry_Date BETWEEN @dtFinYearFromJournal AND  DATEADD(MS,-3,Period.StartDate)
		)  AS t2
		
		SET @sql = 'SELECT
						ParentId,
						numAccountId,
						numAccountTypeID,
						vcAccountType,
						LEVEL,
						vcAccountCode,
						Struc,
						[Type],' + @columns + '
					FROM
					(
						SELECT 
							COA.ParentId, 
							COA.numAccountId,
							COA.numAccountTypeID, 
							COA.vcAccountType, 
							COA.LEVEL, 
							COA.vcAccountCode, 
							(''#'' + COA.Struc + ''#'') AS Struc,
							(CASE WHEN ISNULL(COA.numAccountId,0) > 0 THEN 1 ELSE 2 END) [Type],
							ISNULL(SUM(Amount),0)  AS Amount,
							V.MonthYear
						FROM 
							#tempDirectReport COA 
						LEFT OUTER JOIN 
							#tempViewDataQuarter V 
						ON  
							V.Struc like COA.Struc + ''%'' 
						GROUP BY 
							COA.ParentId, COA.numAccountTypeID, COA.vcAccountType, COA.LEVEL, COA.vcAccountCode, COA.numAccountId,COA.Struc,COA.bitProfitLoss,V.MonthYear,V.ProfitLossAmount
						UNION
						SELECT 
							0, 
							NULL,
							NULL,
							''Total'',
							-1, 
							NULL, 
							''-1'',
							2,
							(SELECT 
								ISNULL(SUM(Debit) - SUM(Credit),0)
							FROM 
								VIEW_JOURNAL 
							WHERE 
								numDomainID = ' + CAST(@numDomainId AS VARCHAR) + '
								AND (vcAccountCode LIKE ''0101%'' 
								OR vcAccountCode LIKE ''0102%''
								OR vcAccountCode LIKE ''0103%''
								OR vcAccountCode LIKE ''0104%''
								OR vcAccountCode LIKE ''0105%''
								OR vcAccountCode LIKE ''0106%'')
								AND datEntry_Date BETWEEN #TempYearMonthQuarter.StartDate AND #TempYearMonthQuarter.EndDate),
							MonthYear
						FROM
							#TempYearMonthQuarter
					) AS SourceTable
					pivot
					(
						SUM(AMOUNT)
						FOR [MonthYear] IN ( ' + @pivotcolumns + ' )
					) AS P 
					ORDER BY Struc, [Type] desc'

		PRINT @sql
		EXECUTE (@sql)

		DROP TABLE #TempYearMonthQuarter
		DROP TABLE #tempViewDataQuarter
	END
	ELSE
	BEGIN
		DECLARE @monProfitLossAmount AS MONEY = 0
		-- GETTING P&L VALUE
		SELECT @monProfitLossAmount = (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE numAccountId=@PLCHARTID AND vcAccountCode LIKE '0103%' AND datEntry_Date between @dtFromDate and @dtToDate)
			+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE vcAccountCode LIKE '0104%' AND datEntry_Date BETWEEN @dtFromDate AND @dtToDate)
			+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE vcAccountCode LIKE '0106%' AND datEntry_Date BETWEEN @dtFromDate AND @dtToDate)
			+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE vcAccountCode LIKE '0103%' AND datEntry_Date BETWEEN @dtFromDate AND @dtToDate)
			+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE (vcAccountCode LIKE '0103%' OR vcAccountCode LIKE '0104%' OR vcAccountCode  LIKE '0106%') AND datEntry_Date <=  DATEADD(MS,-3,@dtFromDate))
			+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE numAccountId=@PLCHARTID AND datEntry_Date <=  DATEADD(MS,-3,@dtFromDate))

		SELECT 
			COA.ParentId, 
			COA.numAccountTypeID, 
			COA.vcAccountType, 
			COA.LEVEL, 
			COA.vcAccountCode, 
			COA.numAccountId, 
			COA.Struc,
			(CASE 
				WHEN bitProfitLoss = 1 
				THEN -@monProfitLossAmount
				ELSE
					(CASE 
						WHEN COA.[vcAccountCode] LIKE '0103%' OR COA.[vcAccountCode] LIKE '0104%' OR COA.[vcAccountCode] LIKE '0106%' THEN 0 
						ELSE 
							ISNULL(t2.OPENING,0) 
					END) + (ISNULL(Debit,0) - ISNULL(Credit,0))
			END) AS Amount
		INTO 
			#tempViewData
		FROM 
			#tempDirectReport COA 
		OUTER APPLY
		(
			SELECT 
				SUM(Debit) AS Debit,
				SUM(Credit) AS Credit
			FROM
				#view_journal V 
			WHERE  
				V.numAccountID = COA.numAccountID 
				AND datEntry_Date BETWEEN @dtFromDate AND @dtToDate
		) AS t1
		OUTER APPLY 
		(
			SELECT   
				SUM(Debit - Credit) AS OPENING
			FROM     
				#view_journal VJ
			WHERE    
				VJ.numAccountID = COA.numAccountID 
				AND datEntry_Date BETWEEN @dtFinYearFromJournal AND  DATEADD(MS,-3,@dtFromDate)
		)  AS t2

		SELECT 
			COA.ParentId, 
			COA.numAccountId,
			COA.numAccountTypeID, 
			COA.vcAccountType, 
			COA.LEVEL, 
			COA.vcAccountCode, 
			('#' + COA.Struc + '#') AS Struc,
			(CASE WHEN ISNULL(COA.numAccountId,0) > 0 THEN 1 ELSE 2 END) [Type],
			ISNULL(SUM(Amount),0) AS Amount
		FROM 
			#tempDirectReport COA 
		LEFT OUTER JOIN 
			#tempViewData V 
		ON  
			V.Struc like COA.Struc + '%' 
		GROUP BY 
			COA.ParentId, COA.numAccountTypeID, COA.vcAccountType, COA.LEVEL, COA.vcAccountCode, COA.numAccountId,COA.Struc,COA.bitProfitLoss
		UNION
		SELECT 
			0, 
			NULL,
			-1, 
			'Total', 
			0, 
			NULL, 
			'-1' AS Struc,
			2,
			(SELECT 
				SUM(Debit) - SUM(Credit)
			FROM 
				VIEW_JOURNAL 
			WHERE 
				numDomainID = @numDomainId
				AND (vcAccountCode LIKE '0101%' 
				OR vcAccountCode LIKE '0102%'
				OR vcAccountCode LIKE '0103%'
				OR vcAccountCode LIKE '0104%'
				OR vcAccountCode LIKE '0105%'
				OR vcAccountCode LIKE '0106%')
				AND datEntry_Date BETWEEN @dtFromDate AND @dtToDate)
		ORDER BY
			Struc, [Type] desc

		DROP TABLE #tempViewData 
	END

	DROP TABLE #view_journal
	DROP TABLE #tempDirectReport
END

/****** Object:  StoredProcedure [dbo].[usp_GetUsersWithDomains]    Script Date: 07/26/2008 16:18:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj              
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getuserswithdomains')
DROP PROCEDURE usp_getuserswithdomains
GO
CREATE PROCEDURE [dbo].[usp_GetUsersWithDomains]                            
 @numUserID NUMERIC(9 )= 0,
 @numDomainID NUMERIC(9)
--                          
AS                            
BEGIN                            
 IF @numUserID = 0 AND @numDomainID > 0 --Check if request is made while session is on
 BEGIN                            
  SELECT numUserID, vcUserName,UserMaster.numGroupID,isnull(vcGroupName,'-')as vcGroupName,              
 vcUserDesc,ISNULL(ADC.vcfirstname,'') +' '+ ISNULL(ADC.vclastname,'') as Name,               
 UserMaster.numDomainID, vcDomainName, UserMaster.numUserDetailId ,isnull(UserMaster.SubscriptionId,'') as SubscriptionId
,isnull(UserMaster.tintDefaultRemStatus,0) tintDefaultRemStatus,isnull(UserMaster.numDefaultTaskType,0) numDefaultTaskType,
ISNULL(UserMaster.bitOutlook,0) bitOutlook,UserMaster.vcLinkedinId,ISNULL(UserMaster.intAssociate,0) as intAssociate
   FROM UserMaster                      
   join Domain                        
   on UserMaster.numDomainID =  Domain.numDomainID                       
   left join AdditionalContactsInformation ADC                      
   on ADC.numContactid=UserMaster.numUserDetailId                     
   left join AuthenticationGroupMaster GM                     
   on Gm.numGroupID= UserMaster.numGroupID                    
   where tintGroupType=1 -- Application Users                    
   ORDER BY  Domain.numDomainID, vcUserDesc                            
 END   
 ELSE IF @numUserID = -1 AND @numDomainID > 0 --Support Email
 BEGIN                            
  SELECT '' AS vcEmailId,vcImapUserName,isnull(bitUseUserName,0) bitUseUserName, isnull(ImapUserDTL.bitImap,0) bitImap,  
	ImapUserDTL.vcImapServerUrl,  
	isnull(ImapUserDTL.vcImapPassword,'') vcImapPassword,  
	ImapUserDTL.bitSSl,  
	ImapUserDTL.numPort 
FROM [ImapUserDetails] ImapUserDTL   
where ImapUserDTL.numUserCntId = @numUserID 
AND ImapUserDTL.numDomainID = @numDomainID

 END                         
 ELSE                            
 BEGIN                            
  SELECT       
 U.numUserID, vcUserName,numGroupID,vcUserDesc, U.numDomainID,vcDomainName, U.numUserDetailId,bitHourlyRate,bitActivateFlag,      
 monHourlyRate,bitSalary,numDailyHours,bitOverTime,numLimDailHrs,monOverTimeRate,bitMainComm,fltMainCommPer,      
 bitRoleComm,(select count(*) from UserRoles where UserRoles.numUserCntID=U.numUserDetailId)  as Roles ,vcEmailid,      
 vcPassword,isnull(ExcUserDTL.bitExchangeIntegration,0) bitExchangeIntegration, isnull(ExcUserDTL.bitAccessExchange,0) bitAccessExchange,       
 isnull(ExcUserDTL.vcExchPassword,'') as vcExchPassword, isnull(ExcUserDTL.vcExchPath,'') as vcExchPath ,       
 isnull(ExcUserDTL.vcExchDomain,'')  as vcExchDomain   ,isnull(U.SubscriptionId,'') as SubscriptionId ,  
 isnull(ImapUserDTL.bitImap,0) bitImap,  
	ImapUserDTL.vcImapServerUrl,  
	isnull(ImapUserDTL.vcImapPassword,'') vcImapPassword,  
	ImapUserDTL.bitSSl,  
	ImapUserDTL.numPort 
	,isnull(bitSMTPAuth,0) bitSMTPAuth
      ,isnull([vcSmtpPassword],'')vcSmtpPassword
      ,isnull([vcSMTPServer],0) vcSMTPServer
      ,isnull(numSMTPPort,0)numSMTPPort
      ,isnull(bitSMTPSSL,0) bitSMTPSSL
,	isnull(bitSMTPServer,0)bitSMTPServer
    ,isnull(bitUseUserName,0) bitUseUserName
,isnull(U.tintDefaultRemStatus,0) tintDefaultRemStatus,isnull(U.numDefaultTaskType,0) numDefaultTaskType,
ISNULL(U.bitOutlook,0) bitOutlook,
ISNULL(numDefaultClass,0) numDefaultClass,isnull(numDefaultWarehouse,0) numDefaultWarehouse,U.vcLinkedinId,ISNULL(U.intAssociate,0) as intAssociate
FROM UserMaster U                        
 join Domain D                       
 on   U.numDomainID =  D.numDomainID       
left join  ExchangeUserDetails ExcUserDTL      
 on ExcUserDTL.numUserID=U.numUserID                      
left join  [ImapUserDetails] ImapUserDTL   
on ImapUserDTL.numUserCntId = U.numUserDetailId   
AND ImapUserDTL.numDomainID = D.numDomainID
   WHERE  U.numUserID=@numUserID                            
 END                            
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetVendorShipmentMethod')
DROP PROCEDURE USP_GetVendorShipmentMethod
GO
CREATE PROCEDURE [dbo].[USP_GetVendorShipmentMethod]         
@numDomainID as numeric(9)=0,    
@numVendorid AS NUMERIC(9)=0,
@numAddressID AS NUMERIC(9)=0,
@tintMode AS TINYINT        
as        

IF @tintMode=1
BEGIN 
	SELECT LD.numListItemID,LD.vcData AS ShipmentMethod,ISNULL(VM.numListValue,0) AS numListValue,VM.bitPrimary
	from ListDetails LD LEFT JOIN VendorShipmentMethod VM ON VM.numListItemID=LD.numListItemID 
	AND VM.numVendorid=@numVendorid AND VM.numAddressID=@numAddressID AND VM.numDomainID=@numDomainID
	WHERE LD.numListID=338 AND (LD.numDomainID=@numDomainID or Ld.constFlag=1)
END
ELSE IF @tintMode=2
BEGIN 
	SELECT LD.numListItemID,LD.vcData AS ShipmentMethod,ISNULL(VM.numListValue,0) AS numListValue,VM.bitPrimary,
	cast(LD.numListItemID AS VARCHAR(10)) + '~' + cast(ISNULL(VM.numListValue,0) AS VARCHAR(10)) AS vcListValue
		from ListDetails LD JOIN VendorShipmentMethod VM ON VM.numListItemID=LD.numListItemID 
	AND VM.numVendorid=@numVendorid AND VM.numAddressID=@numAddressID AND VM.numDomainID=@numDomainID
	WHERE LD.numListID=338 AND (LD.numDomainID=@numDomainID or Ld.constFlag=1)
END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_InLineEditDataSave')
DROP PROCEDURE USP_InLineEditDataSave
GO
CREATE PROCEDURE [dbo].[USP_InLineEditDataSave] 
( 
 @numDomainID NUMERIC(9),
 @numUserCntID NUMERIC(9),
 @numFormFieldId NUMERIC(9),
 @bitCustom AS BIT = 0,
 @InlineEditValue VARCHAR(8000),
 @numContactID NUMERIC(9),
 @numDivisionID NUMERIC(9),
 @numWOId NUMERIC(9),
 @numProId NUMERIC(9),
 @numOppId NUMERIC(9),
 @numRecId NUMERIC(9),
 @vcPageName NVARCHAR(100),
 @numCaseId NUMERIC(9),
 @numWODetailId NUMERIC(9),
 @numCommID NUMERIC(9)
 )
AS 

declare @strSql as nvarchar(4000)
DECLARE @vcDbColumnName AS VARCHAR(100),@vcOrigDbColumnName AS VARCHAR(100),@vcLookBackTableName AS varchar(100)
DECLARE @vcListItemType as VARCHAR(3),@numListID AS numeric(9),@vcAssociatedControlType varchar(20)
declare @tempAssignedTo as numeric(9);set @tempAssignedTo=null           
DECLARE @Value1 AS VARCHAR(18),@Value2 AS VARCHAR(18)

IF  @bitCustom =0
BEGIN
	SELECT @vcDbColumnName=vcDbColumnName,@vcLookBackTableName=vcLookBackTableName,
		   @vcListItemType=vcListItemType,@numListID=numListID,@vcAssociatedControlType=vcAssociatedControlType,
		   @vcOrigDbColumnName=vcOrigDbColumnName
	 FROM DycFieldMaster WHERE numFieldId=@numFormFieldId
	 
	 
	if @vcLookBackTableName = 'DivisionMaster' 
	 BEGIN 
	 
	   IF @vcDbColumnName='numFollowUpStatus' --Add to FollowUpHistory
	    Begin
			declare @numFollow as varchar(10),@binAdded as varchar(20),@PreFollow as varchar(10),@RowCount as varchar(2)                            
			                      
			set @PreFollow=(select count(*)  numFollowUpStatus from FollowUpHistory where numDivisionID= @numDivisionID and numDomainId= @numDomainID)                          
			set @RowCount=@@rowcount  
			                        
			select   @numFollow=numFollowUpStatus, @binAdded=bintModifiedDate from divisionmaster where numDivisionID=@numDivisionID and numDomainId= @numDomainID                    
			
			if @numFollow <>'0' and @numFollow <> @InlineEditValue                          
			begin                          
				if @PreFollow<>0                          
					begin                          
			                   
						if @numFollow <> (select top 1  numFollowUpStatus from FollowUpHistory where numDivisionID= @numDivisionID and numDomainId= @numDomainID order by numFollowUpStatusID desc)                          
							begin                          
			                      	insert into FollowUpHistory(numFollowUpstatus,numDivisionID,bintAddedDate,numDomainId)                          
									values(@numFollow,@numDivisionID,@binAdded,@numDomainID)                          
							end                          
					end                          
				else                          
					begin                          
						insert into FollowUpHistory(numFollowUpstatus,numDivisionID,bintAddedDate,numDomainId)                          
						 values(@numFollow,@numDivisionID,@binAdded,@numDomainID)                          
					end                          
			   end 
			END
		
		 IF @vcDbColumnName='numAssignedTo' ---Updating if organization is assigned to someone
			BEGIN
				select @tempAssignedTo=isnull(numAssignedTo,0) from DivisionMaster where numDivisionID = @numDivisionID and numDomainId= @numDomainID           
			
				if (@tempAssignedTo<>@InlineEditValue and  @InlineEditValue<>'0')          
					begin            
						update DivisionMaster set numAssignedTo=@InlineEditValue ,numAssignedBy=@numUserCntID where numDivisionID = @numDivisionID  and numDomainId= @numDomainID           
					end           
				else if  (@InlineEditValue ='0')          
					begin          
						update DivisionMaster set numAssignedTo=0 ,numAssignedBy=0 where numDivisionID = @numDivisionID  and numDomainId= @numDomainID        
					end    
			END
	    	
		SET @strSql='update DivisionMaster set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numDivisionID=@numDivisionID and numDomainID=@numDomainID'
	 END 
	 ELSE if @vcLookBackTableName = 'AdditionalContactsInformation' 
	 BEGIN 
			IF @vcDbColumnName='bitPrimaryContact' AND @InlineEditValue='True'
			BEGIN
				DECLARE @bitPrimaryContactCheck AS BIT             
				DECLARE @numID AS NUMERIC(9) 
				
				SELECT TOP 1 @numID = numContactID,@bitPrimaryContactCheck = ISNULL(bitPrimaryContact,0)
					FROM AdditionalContactsInformation WHERE numDivisionID = @numDivisionID 
                   
					IF @@rowcount = 0 SET @numID = 0             
				
					WHILE @numID > 0            
					BEGIN            
						IF @bitPrimaryContactCheck = 1 
							UPDATE  AdditionalContactsInformation SET bitPrimaryContact = 0 WHERE numContactID = @numID  
                              
						SELECT TOP 1 @numID = numContactID,@bitPrimaryContactCheck = ISNULL(bitPrimaryContact,0)
						FROM AdditionalContactsInformation WHERE numDivisionID = @numDivisionID AND numContactID > @numID    
                                
						IF @@rowcount = 0 SET @numID = 0             
					END 
				END
				ELSE IF @vcDbColumnName='bitPrimaryContact' AND @InlineEditValue='False'
				BEGIN
					IF (SELECT COUNT(*) FROM AdditionalContactsInformation WHERE numDivisionID = @numDivisionID AND numContactId!=@numContactId AND ISNULL(bitPrimaryContact,0)=1) = 0
						SET @InlineEditValue='True'
				END 
				
		SET @strSql='update AdditionalContactsInformation set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numDivisionID=@numDivisionID and numDomainID=@numDomainID and numContactId=@numContactId'
	 END 
	 ELSE if @vcLookBackTableName = 'CompanyInfo' 
	 BEGIN 
		SET @strSql='update CompanyInfo set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numCompanyId=(select numCompanyId from divisionmaster where numDivisionID=@numDivisionID and numDomainID=@numDomainID) and numDomainID=@numDomainID'
	 END 
	 ELSE if @vcLookBackTableName = 'WorkOrder' 
	 BEGIN 
		DECLARE @numItemCode AS NUMERIC(9)
	 
		IF @numWODetailId>0
		BEGIN
			SET @strSql='update WorkOrderDetails set ' + @vcDbColumnName + '=@InlineEditValue where numWOId=@numWOId and numWODetailId=@numWODetailId'
			
			IF @vcDbColumnName='numQtyItemsReq'
			BEGIN 
				SELECT @numItemCode=numChildItemID FROM WorkOrderDetails WHERE numWOId=@numWOId and numWODetailId=@numWODetailId
				
				EXEC USP_UpdateWorkOrderDetailQuantity @numItemCode,@InlineEditValue,@numWOId
				--UPDATE WorkOrderDetails SET numQtyItemsReq=CONVERT(NUMERIC(9),numQtyItemsReq_Orig * CONVERT(NUMERIC(9,0),@InlineEditValue)) WHERE numWOId=@numWOId
			END 
		END 	
		ELSE
		BEGIN
			SET @strSql='update WorkOrder set ' + @vcDbColumnName + '=@InlineEditValue where numWOId=@numWOId and numDomainID=@numDomainID'
			
			IF @vcDbColumnName='numQtyItemsReq'
			BEGIN 
				SELECT @numItemCode=numItemCode FROM WorkOrder WHERE numWOId=@numWOId and numDomainID=@numDomainID
			
				EXEC USP_UpdateWorkOrderDetailQuantity @numItemCode,@InlineEditValue,@numWOId
				--UPDATE WorkOrderDetails SET numQtyItemsReq=CONVERT(NUMERIC(9),numQtyItemsReq_Orig * CONVERT(NUMERIC(9,0),@InlineEditValue)) WHERE numWOId=@numWOId
			END 
		END	
	 END
	 ELSE if @vcLookBackTableName = 'ProjectsMaster' 
	 BEGIN 
		IF @vcDbColumnName='numAssignedTo' ---Updating if Project is assigned to someone 
			BEGIN
				select @tempAssignedTo=isnull(numAssignedTo,0) from ProjectsMaster where numProId=@numProId and numDomainId= @numDomainID           
			
				if (@tempAssignedTo<>@InlineEditValue and  @InlineEditValue<>'0')          
					begin            
						update ProjectsMaster set numAssignedTo=@InlineEditValue ,numAssignedBy=@numUserCntID where numProId = @numProId  and numDomainId= @numDomainID           
					end           
				else if  (@InlineEditValue ='0')          
					begin          
						update ProjectsMaster set numAssignedTo=0 ,numAssignedBy=0 where numProId = @numProId  and numDomainId= @numDomainID        
					end    
			END
		else IF @vcDbColumnName='numProjectStatus'  ---Updating All Statge to 100% if Completed
			BEGIN
				IF @InlineEditValue='27492'
				BEGIN
					UPDATE PM SET dtCompletionDate=GETUTCDATE(),monTotalExpense=PIE.monExpense,
					monTotalIncome=PIE.monIncome,monTotalGrossProfit=PIE.monBalance,numProjectStatus=27492
					FROM ProjectsMaster PM,dbo.GetProjectIncomeExpense(@numDomainId,@numProId) PIE
					WHERE PM.numProId=@numProId
					
					EXEC USP_ManageProjectCommission @numDomainId,@numProId
						
					Update StagePercentageDetails set tinProgressPercentage=100,bitclose=1 where numProjectID=@numProId

					EXEC dbo.USP_GetProjectTotalProgress
							@numDomainID = @numDomainID, --  numeric(9, 0)
							@numProId = @numProId, --  numeric(9, 0)
							@tintMode = 1 --  tinyint
				END			
				ELSE
				BEGIN
					DELETE from BizDocComission WHERE numDomainID = @numDomainID AND numProId=@numProId AND numComissionID 
						NOT IN (SELECT BC.numComissionID FROM dbo.BizDocComission BC JOIN dbo.PayrollTracking PT 
							ON BC.numComissionID = PT.numComissionID AND BC.numDomainId = PT.numDomainId WHERE BC.numDomainID = @numDomainID AND ISNULL(BC.numProId,0) = @numProId)
				END
			END
			

		SET @strSql='update ProjectsMaster set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numProId=@numProId and numDomainID=@numDomainID'
	 END 
	 ELSE if @vcLookBackTableName = 'OpportunityMaster' 
	 BEGIN 
		IF @vcDbColumnName='numAssignedTo' ---Updating if organization is assigned to someone  
			BEGIN
				select @tempAssignedTo=isnull(numAssignedTo,0) from OpportunityMaster where numOppId=@numOppId and numDomainId= @numDomainID           
			
				if (@tempAssignedTo<>@InlineEditValue and  @InlineEditValue<>'0')          
					begin            
						update OpportunityMaster set numAssignedTo=@InlineEditValue ,numAssignedBy=@numUserCntID where numOppId = @numOppId  and numDomainId= @numDomainID           
					end           
				else if  (@InlineEditValue ='0')          
					begin          
						update OpportunityMaster set numAssignedTo=0 ,numAssignedBy=0 where numOppId = @numOppId  and numDomainId= @numDomainID        
					end    
			END
		ELSE IF @vcDbColumnName='tintActive'
		 BEGIN
		 	SET @InlineEditValue= CASE @InlineEditValue WHEN 'True' THEN 1 ELSE 0 END 
		 END
		 ELSE IF @vcDbColumnName='tintSource'
		 BEGIN
			IF CHARINDEX('~', @InlineEditValue)>0
			BEGIN
				SET @Value1=SUBSTRING(@InlineEditValue, 1,CHARINDEX('~', @InlineEditValue)-1) 
				SET @Value2=SUBSTRING(@InlineEditValue, CHARINDEX('~', @InlineEditValue)+1,LEN(@InlineEditValue)) 
			END	
			ELSE
			BEGIN
				SET @Value1=@InlineEditValue
				SET @Value2=0
			END
			
		 	update OpportunityMaster set tintSourceType=@Value2 where numOppId = @numOppId  and numDomainId= @numDomainID        
		 	
		 	SET @InlineEditValue= @Value1
		 END
		ELSE IF @vcDbColumnName='tintOppStatus' --Update Inventory  0=Open,1=Won,2=Lost
		 BEGIN
			declare @tintOppStatus AS TINYINT
			select @tintOppStatus=tintOppStatus from OpportunityMaster where numOppID=@numOppID              

			if @tintOppStatus=0 and @InlineEditValue='1' --Open to Won
				BEGIN
					select @numDivisionID=numDivisionID from  OpportunityMaster where numOppID=@numOppID   
					DECLARE @tintCRMType AS TINYINT      
					select @tintCRMType=tintCRMType from divisionmaster where numDivisionID =@numDivisionID 

					-- Promote Lead to Account when Sales/Purchase Order is created against it
					if @tintCRMType=0 --Lead & Order
					begin        
						update divisionmaster set tintCRMType=2,bintLeadProm=getutcdate(),bintLeadPromBy=@numUserCntID,bintProsProm=getutcdate(),bintProsPromBy=@numUserCntID        
						where numDivisionID=@numDivisionID        
					end
					--Promote Prospect to Account
					else if @tintCRMType=1 
					begin        
						update divisionmaster set tintCRMType=2,bintProsProm=getutcdate(),bintProsPromBy=@numUserCntID        
						where numDivisionID=@numDivisionID        
					end    

		 			EXEC USP_UpdatingInventoryonCloseDeal @numOppID,@numUserCntID

					update OpportunityMaster set bintOppToOrder=GETUTCDATE() where numOppId=@numOppId and numDomainID=@numDomainID
				END
			if (@tintOppStatus=1 and @InlineEditValue='2') or (@tintOppStatus=1 and @InlineEditValue='0') --Won to Lost or Won to Open
				EXEC USP_RevertDetailsOpp @numOppID,0,@numUserCntID 
		 END
		 ELSE IF @vcDbColumnName='vcOppRefOrderNo' --Update vcRefOrderNo OpportunityBizDocs
		 BEGIN
			 IF LEN(ISNULL(@InlineEditValue,''))>0
			 BEGIN
	   			UPDATE dbo.OpportunityBizDocs SET vcRefOrderNo=@InlineEditValue WHERE numOppId=@numOppID
			 END
		 END
		 ELSE IF @vcDbColumnName='numStatus' 
		 BEGIN
		 	 IF EXISTS (SELECT 1 FROM dbo.OpportunityMaster WHERE numOppId = @numOppID AND tintOppType=1 AND isnull(tintOppStatus,0)=1 AND ISNULL(numStatus,0)!= @InlineEditValue)
			 BEGIN
	   				EXEC USP_ManageOpportunityAutomationQueue
							@numOppQueueID = 0, -- numeric(18, 0)
							@numDomainID = @numDomainID, -- numeric(18, 0)
							@numOppId = @numOppID, -- numeric(18, 0)
							@numOppBizDocsId = 0, -- numeric(18, 0)
							@numOrderStatus = @InlineEditValue, -- numeric(18, 0)
							@numUserCntID = @numUserCntID, -- numeric(18, 0)
							@tintProcessStatus = 1, -- tinyint
							@tintMode = 1 -- TINYINT
			END 
		 END	
		
		SET @strSql='update OpportunityMaster set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numOppId=@numOppId and numDomainID=@numDomainID'
	 END
	 ELSE if @vcLookBackTableName = 'Cases' 
	 BEGIN 
	 		IF @vcDbColumnName='numAssignedTo' ---Updating if Case is assigned to someone  
			BEGIN
				select @tempAssignedTo=isnull(numAssignedTo,0) from Cases where numCaseId=@numCaseId and numDomainId= @numDomainID           
			
				if (@tempAssignedTo<>@InlineEditValue and  @InlineEditValue<>'0')          
					begin            
						update Cases set numAssignedTo=@InlineEditValue ,numAssignedBy=@numUserCntID where numCaseId = @numCaseId  and numDomainId= @numDomainID           
					end           
				else if  (@InlineEditValue ='0')          
					begin          
						update Cases set numAssignedTo=0 ,numAssignedBy=0 where numCaseId = @numCaseId  and numDomainId= @numDomainID        
					end    
			END
			
		SET @strSql='update Cases set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numCaseId=@numCaseId and numDomainID=@numDomainID'
	 END 
	 ELSE if @vcLookBackTableName = 'Tickler' 
	 BEGIN 	
		SET @strSql='update Communication set ' + @vcOrigDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,dtModifiedDate=getutcdate() where numCommId=@numCommId and numDomainID=@numDomainID'
	 END
	 
	 
	EXECUTE sp_executeSQL @strSql, N'@numDomainID NUMERIC(9),@numUserCntID NUMERIC(9),@numDivisionID NUMERIC(9),@numContactID NUMERIC(9),@InlineEditValue VARCHAR(8000),@numWOId NUMERIC(9),@numProId NUMERIC(9),@numOppId NUMERIC(9),@numCaseId NUMERIC(9),@numWODetailId NUMERIC(9),@numCommID Numeric(9)',
		     @numDomainID,@numUserCntID,@numDivisionID,@numContactID,@InlineEditValue,@numWOId,@numProId,@numOppId,@numCaseId,@numWODetailId,@numCommID;
		     
	IF @vcAssociatedControlType='SelectBox'                                                    
       BEGIN       
			IF @vcDbColumnName='tintSource' AND @vcLookBackTableName = 'OpportunityMaster'
			BEGIN
				SELECT dbo.fn_GetOpportunitySourceValue(@Value1,@Value2,@numDomainID) AS vcData                                             
			END
			ELSE IF @vcDbColumnName='numContactID'
			BEGIN
				SELECT dbo.fn_GetContactName(@InlineEditValue)
			END
			else
			BEGIN
				SELECT dbo.fn_getSelectBoxValue(@vcListItemType,@InlineEditValue,@vcDbColumnName) AS vcData                                             
			END
		end 
		ELSE
		BEGIN
			 IF @vcDbColumnName='tintActive'
		  	 BEGIN
			 	SET @InlineEditValue= CASE @InlineEditValue WHEN 1 THEN 'True' ELSE 'False' END 
			 END

			IF @vcAssociatedControlType = 'Check box' or @vcAssociatedControlType = 'Checkbox'
			BEGIN
	 			SET @InlineEditValue= Case @InlineEditValue WHEN 'True' THEN 'Yes' ELSE 'No' END
			END
			
		    SELECT @InlineEditValue AS vcData
		END  
		
	IF @vcLookBackTableName = 'AdditionalContactsInformation' AND @vcDbColumnName = 'numECampaignID'
	BEGIN
		--Manage ECampiagn When Campaign is changed from inline edit
		DECLARE @Date AS DATETIME 
		SELECT @Date = dbo.[GetUTCDateWithoutTime]()
		DECLARE @numECampaignID AS NUMERIC(18,0)
		SET @numECampaignID = @InlineEditValue

		IF @numECampaignID = -1 
		SET @numECampaignID=0
	 
 		EXEC [USP_ManageConEmailCampaign]
		@numConEmailCampID = 0, --  numeric(9, 0)
		@numContactID = @numContactID, --  numeric(9, 0)
		@numECampaignID = @numECampaignID, --  numeric(9, 0)
		@intStartDate = @Date, --  datetime
		@bitEngaged = 1, --  bit
		@numUserCntID = @numUserCntID --  numeric(9, 0)
	END  
END 
ELSE
BEGIN	 

	SET @InlineEditValue=Cast(@InlineEditValue as varchar(1000))
	SELECT @vcAssociatedControlType=Fld_type
	 FROM CFW_Fld_Master WHERE Fld_Id=@numFormFieldId 

	IF @vcAssociatedControlType = 'CheckBox'
	BEGIN
	    SET @InlineEditValue=(case when @InlineEditValue='False' then 0 ELSE 1 END)
	END

	IF @vcPageName='organization'
	BEGIN 
		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
			BEGIN
				UPDATE CFW_FLD_Values SET Fld_Value=@InlineEditValue WHERE FldDTLID=(SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
			END	
		ELSE
			BEGIN
				insert into CFW_FLD_Values (Fld_ID,Fld_Value,RecId) VALUES (@numFormFieldId,@InlineEditValue,@numRecId)         
			END

			--Added By :Sachin Sadhu ||Date:13thOct2014
			--Purpose  :To manage CustomFields in Queue
			EXEC USP_CFW_FLD_Values_CT
					@numDomainID =@numDomainID,
					@numUserCntID =@numUserCntID,
					@numRecordID =@numRecId ,
					@numFormFieldId  =@numFormFieldId
  
	  END
	ELSE IF @vcPageName='contact'
	BEGIN 
		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Cont CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
			BEGIN
				UPDATE CFW_FLD_Values_Cont SET Fld_Value=@InlineEditValue WHERE FldDTLID=(SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Cont CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
			END	
		ELSE
			BEGIN
				insert into CFW_FLD_Values_Cont (Fld_ID,Fld_Value,RecId) VALUES (@numFormFieldId,@InlineEditValue,@numRecId)         
			END
			
			--Added By :Sachin Sadhu ||Date:13thOct2014
			--Purpose  :To manage CustomFields in Queue
			EXEC USP_CFW_FLD_Values_Cont_CT
					@numDomainID =@numDomainID,
					@numUserCntID =@numUserCntID,
					@numRecordID =@numRecId ,
					@numFormFieldId  =@numFormFieldId

	END
	ELSE IF @vcPageName='project'
	BEGIN 
		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Pro CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
			BEGIN
				UPDATE CFW_FLD_Values_Pro SET Fld_Value=@InlineEditValue WHERE FldDTLID=(SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Pro CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
			END	
		ELSE
			BEGIN
				insert into CFW_FLD_Values_Pro (Fld_ID,Fld_Value,RecId) VALUES (@numFormFieldId,@InlineEditValue,@numRecId)         
			END

			
			--Added By :Sachin Sadhu ||Date:14thOct2014
			--Purpose  :To manage CustomFields in Queue
			EXEC USP_CFW_FLD_Values_Pro_CT
					@numDomainID =@numDomainID,
					@numUserCntID =@numUserCntID,
					@numRecordID =@numRecId ,
					@numFormFieldId  =@numFormFieldId
	END
	ELSE IF @vcPageName='opp'
	BEGIN 
		DECLARE @dtDate AS DATETIME = GETUTCDATE()

		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_Fld_Values_Opp CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
		BEGIN
			UPDATE 
				CFW_Fld_Values_Opp 
			SET 
				Fld_Value=@InlineEditValue ,
				numModifiedBy = @numUserCntID,
				bintModifiedDate = @dtDate
			WHERE 
				FldDTLID= (SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_Fld_Values_Opp CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
		END	
		ELSE
		BEGIN
			INSERT INTO CFW_Fld_Values_Opp (Fld_ID,Fld_Value,RecId,numModifiedBy,bintModifiedDate) VALUES (@numFormFieldId,@InlineEditValue,@numRecId,@numUserCntID,@dtDate)         
		END

		UPDATE OpportunityMaster SET numModifiedBy=@numUserCntID,bintModifiedDate=@dtDate WHERE numOppID = @numRecId 

		--Added By :Sachin Sadhu ||Date:14thOct2014
		--Purpose  :To manage CustomFields in Queue
		EXEC USP_CFW_Fld_Values_Opp_CT
				@numDomainID =@numDomainID,
				@numUserCntID =@numUserCntID,
				@numRecordID =@numRecId ,
				@numFormFieldId  =@numFormFieldId

	END
	ELSE IF @vcPageName='case'
	BEGIN 
		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Case CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
			BEGIN
				UPDATE CFW_FLD_Values_Case SET Fld_Value=@InlineEditValue WHERE FldDTLID=(SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Case CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
			END	
		ELSE
			BEGIN
				insert into CFW_FLD_Values_Case (Fld_ID,Fld_Value,RecId) VALUES (@numFormFieldId,@InlineEditValue,@numRecId)         
			END

			
			--Added By :Sachin Sadhu ||Date:14thOct2014
			--Purpose  :To manage CustomFields in Queue
			EXEC USP_CFW_FLD_Values_Case_CT
					@numDomainID =@numDomainID,
					@numUserCntID =@numUserCntID,
					@numRecordID =@numRecId ,
					@numFormFieldId  =@numFormFieldId
	END

	IF @vcAssociatedControlType = 'SelectBox'            
	BEGIN            
		 SELECT vcData FROM ListDetails WHERE numListItemID=@InlineEditValue            
	END
	ELSE IF @vcAssociatedControlType = 'CheckBoxList'
	BEGIN
		DECLARE @str AS VARCHAR(MAX)
		SET @str = 'SELECT STUFF((SELECT CONCAT('','', vcData) FROM ListDetails WHERE numlistitemid IN (' + @InlineEditValue + ') FOR XML PATH('''')), 1, 1, '''')'
		EXEC(@str)
	END
	ELSE IF @vcAssociatedControlType = 'CheckBox'
	BEGIN
	 	select CASE @InlineEditValue WHEN 1 THEN 'Yes' ELSE 'No' END AS vcData
	END
	ELSE
	BEGIN
		SELECT @InlineEditValue AS vcData
	END 
END

GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_Item_SearchForSalesOrder' )
    DROP PROCEDURE USP_Item_SearchForSalesOrder
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Sandeep Patel
-- Create date: 3 March 2014
-- Description:	Search items for sales order
-- =============================================
CREATE PROCEDURE USP_Item_SearchForSalesOrder
    @tintOppType AS TINYINT ,
    @numDomainID AS NUMERIC(9) ,
    @numDivisionID AS NUMERIC(9) = 0 ,
    @str AS VARCHAR(1000) ,
    @numUserCntID AS NUMERIC(9) ,
    @tintSearchOrderCustomerHistory AS TINYINT = 0 ,
    @numPageIndex AS INT ,
    @numPageSize AS INT ,
    @WarehouseID AS NUMERIC(18, 0) = NULL ,
    @TotalCount AS INT OUTPUT
AS
    BEGIN
  SET NOCOUNT ON;

DECLARE @strNewQuery NVARCHAR(MAX)
DECLARE @strSQL AS NVARCHAR(MAX)
SELECT  @str = REPLACE(@str, '''', '''''')

DECLARE @TableRowCount TABLE ( Value INT );
	
SELECT  *
INTO    #Temp1
FROM    ( SELECT    numFieldId ,
                    vcDbColumnName ,
                    ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                    tintRow AS tintOrder ,
                    0 AS Custom
          FROM      View_DynamicColumns
          WHERE     numFormId = 22
                    AND numUserCntID = @numUserCntID
                    AND numDomainID = @numDomainID
                    AND tintPageType = 1
                    AND bitCustom = 0
                    AND ISNULL(bitSettingField, 0) = 1
                    AND numRelCntType = 0
          UNION
          SELECT    numFieldId ,
                    vcFieldName ,
                    vcFieldName ,
                    tintRow AS tintOrder ,
                    1 AS Custom
          FROM      View_DynamicCustomColumns
          WHERE     Grp_id = 5
                    AND numFormId = 22
                    AND numUserCntID = @numUserCntID
                    AND numDomainID = @numDomainID
                    AND tintPageType = 1
                    AND bitCustom = 1
                    AND numRelCntType = 0
        ) X 
  
IF NOT EXISTS ( SELECT  *
                FROM    #Temp1 )
    BEGIN
        INSERT  INTO #Temp1
                SELECT  numFieldId ,
                        vcDbColumnName ,
                        ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                        tintorder ,
                        0
                FROM    View_DynamicDefaultColumns
                WHERE   numFormId = 22
                        AND bitDefault = 1
                        AND ISNULL(bitSettingField, 0) = 1
                        AND numDomainID = @numDomainID 
    END

CREATE TABLE #tempItemCode ( numItemCode NUMERIC(9) )
DECLARE @bitRemoveVendorPOValidation AS BIT
SELECT  
	@bitRemoveVendorPOValidation = ISNULL(bitRemoveVendorPOValidation,0)
FROM    
	domain
WHERE  
	numDomainID = @numDomainID

IF @tintOppType > 0
    BEGIN 
        IF CHARINDEX(',', @str) > 0
        BEGIN
			DECLARE @itemSearchText VARCHAR(100)
            DECLARE @vcAttribureSearch VARCHAR(MAX)
			DECLARE @vcAttribureSearchCondition VARCHAR(MAX)

			SET @itemSearchText = LTRIM(RTRIM(SUBSTRING(@str,0,CHARINDEX(',',@str))))

			--GENERATES ATTRIBUTE SEARCH CONDITION
			SET @vcAttribureSearch = SUBSTRING(@str,CHARINDEX(',',@str) + 1,LEN(@str))
	
			DECLARE @attributeSearchText VARCHAR(MAX)
			DECLARE @TempTable TABLE (vcValue VARCHAR(MAX))

			WHILE LEN(@vcAttribureSearch) > 0
			BEGIN
				SET @attributeSearchText = LEFT(@vcAttribureSearch, 
										ISNULL(NULLIF(CHARINDEX(',', @vcAttribureSearch) - 1, -1),
										LEN(@vcAttribureSearch)))
				SET @vcAttribureSearch = SUBSTRING(@vcAttribureSearch,
												ISNULL(NULLIF(CHARINDEX(',', @vcAttribureSearch), 0),
												LEN(@vcAttribureSearch)) + 1, LEN(@vcAttribureSearch))

				INSERT INTO @TempTable (vcValue) VALUES ( @attributeSearchText )
			END
		
			--REMOVES WHITE SPACES
			UPDATE
				@TempTable
			SET
				vcValue = LTRIM(RTRIM(vcValue))
	
			--CONVERTING TABLE TO COMMA SEPERATED STRING AFTER REMOVEING WHITE SPACES
			SELECT @vcAttribureSearch = COALESCE(@vcAttribureSearch,'') +  CONVERT(VARCHAR(MAX),vcValue) + ',' FROM @TempTable
	
			--REMOVE LAST COMMA FROM FINAL SEARCH STRING
			SET @vcAttribureSearch = LTRIM(RTRIM(@vcAttribureSearch))
			IF DATALENGTH(@vcAttribureSearch) > 0
				SET @vcAttribureSearch = LEFT(@vcAttribureSearch, LEN(@vcAttribureSearch) - 1)
	
			--START: GENERATES ATTRIBUTE SEARCH CONDITION
			SELECT 	@vcAttribureSearchCondition = '(TEMPMATRIX.Attributes LIKE ''%' + REPLACE(@vcAttribureSearch, ',', '%'' AND TEMPMATRIX.Attributes LIKE ''%') + '%'')' 

			--LOGIC FOR GENERATING FINAL SQL STRING
			IF @tintOppType = 1 OR ( @bitRemoveVendorPOValidation = 1 AND @tintOppType = 2)
                BEGIN      

				SET @strSQL = 'SELECT
									I.numItemCode,
									I.numVendorID,
									MIN(WHT.numWareHouseItemID) as numWareHouseItemID,
									dbo.fn_GetAttributes(WHT.numWareHouseItemId,I.bitSerialized) AS [Attributes]
								FROM
									dbo.Item I
								LEFT JOIN
									dbo.WareHouseItems WHT
								ON
									I.numItemCode = WHT.numItemID
								WHERE 
									((ISNULL(I.numItemGroup,0) > 0 AND I.vcItemName LIKE ''%' + @itemSearchText + '%'') OR (ISNULL(I.numItemGroup,0) = 0 AND I.vcItemName LIKE ''%' + @str + '%''))
									AND ISNULL(I.Isarchieve, 0) <> 1
									AND I.charItemType NOT IN ( ''A'' )
									AND I.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + '
									AND (WHT.numWareHouseID = ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0)'

				--- added Asset validation  by sojan
                IF @bitRemoveVendorPOValidation = 1
                    AND @tintOppType = 2
                    SET @strSQL = @strSQL
                        + ' and ISNULL(I.bitKitParent,0) = 0 AND ISNULL(I.bitAssembly,0) = 0 '

                IF @tintSearchOrderCustomerHistory = 1 AND @numDivisionID > 0
                BEGIN
                    SET @strSQL = @strSQL
                        + ' and I.numItemCode in (select distinct oppI.numItemCode from 
						OpportunityMaster oppM join OpportunityItems oppI on oppM.numOppId=oppI.numOppId where oppM.numDomainID='
                        + CONVERT(VARCHAR(20), @numDomainID)
                        + ' and oppM.numDivisionId='
                        + CONVERT(VARCHAR(20), @numDivisionId)
                        + ')'
                END

				SET @strSQL = @strSQL + ' GROUP BY
										I.numItemCode,
										I.numVendorID,
										dbo.fn_GetAttributes(numWareHouseItemId,I.bitSerialized)'

				SET @strSQL = 
				'
				SELECT    
					SRNO=ROW_NUMBER() OVER( ORDER BY  I.vcItemName),
					I.numItemCode,
					(SELECT TOP 1 vcPathForImage FROM ItemImages WHERE numItemCode = I.numItemCode) AS vcPathForImage ,
					(SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = I.numItemCode) AS vcPathForTImage ,
					ISNULL(vcItemName, '''') vcItemName ,
					CASE WHEN I.[charItemType] = ''P''
					THEN CONVERT(VARCHAR(100), ISNULL((SELECT TOP 1 ROUND(monWListPrice,2) FROM [WareHouseItems] WHERE [numItemID] = I.numItemCode), 0))
					ELSE CONVERT(VARCHAR(100), ROUND(monListPrice, 2))
					END AS monListPrice,
					ISNULL(vcSKU, '''') vcSKU ,
					ISNULL(numBarCodeId, 0) numBarCodeId ,
					ISNULL(vcModelID, '''') vcModelID ,
					ISNULL(txtItemDesc, '''') AS txtItemDesc ,
					ISNULL(C.vcCompanyName, '''') AS vcCompanyName,
					TEMPMATRIX.numWareHouseItemID,
					TEMPMATRIX.Attributes AS [vcAttributes],
					(CASE 
						WHEN ISNULL(I.bitKitParent,0) = 1 
						THEN 
							(
								CASE
									WHEN (SELECT COUNT(*) FROM ItemDetails INNER JOIN Item ON ItemDetails.numChildItemID = Item.numItemCode WHERE numItemKitID=I.numItemCode AND ISNULL(Item.bitKitParent,0) = 1) > 0
									THEN 1
									ELSE 0
								END
							)
						ELSE 0 
					END) bitHasKitAsChild
				FROM  
					(' + @strSQL +') AS TEMPMATRIX
				INNER JOIN    
					Item I
				ON
					TEMPMATRIX.numItemCode = I.numItemCode
				LEFT JOIN 
					DivisionMaster D 
				ON 
					I.numVendorID = D.numDivisionID
				LEFT JOIN 
					CompanyInfo C 
				ON 
					C.numCompanyID = D.numCompanyID
				WHERE  
				  ((ISNULL(I.numItemGroup,0) = 0 AND I.vcItemName LIKE ''%' + @str + '%'') OR ' + @vcAttribureSearchCondition + ')'

				

                INSERT  INTO @TableRowCount
                        EXEC
                            ( 'SELECT COUNT(*) FROM ( ' + @strSQL
                                + ') as t2'
                                    );

                SELECT  @TotalCount = Value
                FROM    @TableRowCount;

                SET @strNewQuery = 'select * from (' + @strSQL
                    + ') as t where SRNO> '
                    + CAST(( ( @numPageIndex - 1 ) * @numPageSize ) AS VARCHAR(10))
                    + ' and SRNO < '
                    + CAST(( ( @numPageIndex * @numPageSize ) + 1 ) AS VARCHAR(10))
                    + ' order by  vcItemName'

				--PRINT @strNewQuery
                EXEC(@strNewQuery)
				END
			ELSE
				BEGIN

				SET @strSQL = 'SELECT
									I.numItemCode,
									I.numVendorID,
									MIN(WHT.numWareHouseItemID) as numWareHouseItemID,
									dbo.fn_GetAttributes(WHT.numWareHouseItemId,I.bitSerialized) AS [Attributes]
								FROM
									dbo.Item I
								INNER JOIN
									dbo.Vendor V
								ON
									V.numItemCode = I.numItemCode
								LEFT JOIN
									dbo.WareHouseItems WHT
								ON
									I.numItemCode = WHT.numItemID
								WHERE 
									((ISNULL(I.numItemGroup,0) > 0 AND I.vcItemName LIKE ''%' + @itemSearchText + '%'') OR (ISNULL(I.numItemGroup,0) = 0 AND I.vcItemName LIKE ''%' + @str + '%''))
									AND ISNULL(I.Isarchieve, 0) <> 1
									AND I.charItemType NOT IN ( ''A'' )
									AND I.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + '
									AND (WHT.numWareHouseID = ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0)'

				--- added Asset validation  by sojan
                IF @tintSearchOrderCustomerHistory = 1 AND @numDivisionID > 0
                BEGIN
                    SET @strSQL = @strSQL
                        + ' and I.numItemCode in (select distinct oppI.numItemCode from 
						OpportunityMaster oppM join OpportunityItems oppI on oppM.numOppId=oppI.numOppId where oppM.numDomainID='
                        + CONVERT(VARCHAR(20), @numDomainID)
                        + ' and oppM.numDivisionId='
                        + CONVERT(VARCHAR(20), @numDivisionId)
                        + ')'
                END

				SET @strSQL = @strSQL + ' GROUP BY
										I.numItemCode,
										I.numVendorID,
										dbo.fn_GetAttributes(numWareHouseItemId,I.bitSerialized)'

				SET @strSQL = 
				'
				SELECT    
					SRNO=ROW_NUMBER() OVER( ORDER BY  I.vcItemName),
					I.numItemCode,
					(SELECT TOP 1 vcPathForImage FROM ItemImages WHERE numItemCode = I.numItemCode) AS vcPathForImage ,
					(SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = I.numItemCode) AS vcPathForTImage ,
					ISNULL(vcItemName, '''') vcItemName ,
					CASE WHEN I.[charItemType] = ''P''
					THEN CONVERT(VARCHAR(100), ISNULL((SELECT TOP 1 ROUND(monWListPrice,2) FROM [WareHouseItems] WHERE [numItemID] = I.numItemCode), 0))
					ELSE CONVERT(VARCHAR(100), ROUND(monListPrice, 2))
					END AS monListPrice,
					ISNULL(vcSKU, '''') vcSKU ,
					ISNULL(numBarCodeId, 0) numBarCodeId ,
					ISNULL(vcModelID, '''') vcModelID ,
					ISNULL(txtItemDesc, '''') AS txtItemDesc ,
					ISNULL(C.vcCompanyName, '''') AS vcCompanyName,
					TEMPMATRIX.numWareHouseItemID,
					TEMPMATRIX.Attributes AS [vcAttributes],
					(CASE 
						WHEN ISNULL(I.bitKitParent,0) = 1 
						THEN 
							(
								CASE
									WHEN (SELECT COUNT(*) FROM ItemDetails INNER JOIN Item ON ItemDetails.numChildItemID = Item.numItemCode WHERE numItemKitID=I.numItemCode AND ISNULL(Item.bitKitParent,0) = 1) > 0
									THEN 1
									ELSE 0
								END
							)
						ELSE 0 
					END) bitHasKitAsChild
				FROM  
					(' + @strSQL +') AS TEMPMATRIX
				INNER JOIN    
					Item I
				ON
					TEMPMATRIX.numItemCode = I.numItemCode
				LEFT JOIN 
					DivisionMaster D 
				ON 
					I.numVendorID = D.numDivisionID
				LEFT JOIN 
					CompanyInfo C 
				ON 
					C.numCompanyID = D.numCompanyID
				WHERE  
				 ((ISNULL(I.numItemGroup,0) = 0 AND I.vcItemName LIKE ''%' + @str + '%'') OR ' + @vcAttribureSearchCondition + ')'

                INSERT  INTO @TableRowCount
                        EXEC
                            ( 'SELECT COUNT(*) FROM ( ' + @strSQL
                                + ') as t2'
                                    );

                SELECT  @TotalCount = Value
                FROM    @TableRowCount;

                SET @strNewQuery = 'select * from (' + @strSQL
                    + ') as t where SRNO> '
                    + CAST(( ( @numPageIndex - 1 ) * @numPageSize ) AS VARCHAR(10))
                    + ' and SRNO < '
                    + CAST(( ( @numPageIndex * @numPageSize ) + 1 ) AS VARCHAR(10))
                    + ' order by  vcItemName'

				--PRINT @strNewQuery
                EXEC(@strNewQuery)
			END	
        END
        ELSE
        BEGIN
				DECLARE @vcWareHouseSearch VARCHAR(1000)
				DECLARE @vcVendorSearch VARCHAR(1000)
               

				SET @vcWareHouseSearch = ''
				SET @vcVendorSearch = ''

                DECLARE @tintOrder AS INT
                DECLARE @Fld_id AS NVARCHAR(20)
                DECLARE @Fld_Name AS VARCHAR(20)
                SET @strSQL = ''
                SELECT TOP 1
                        @tintOrder = tintOrder + 1 ,
                        @Fld_id = numFieldId ,
                        @Fld_Name = vcFieldName
                FROM    #Temp1
                WHERE   Custom = 1
                ORDER BY tintOrder
                WHILE @tintOrder > 0
                    BEGIN
                        SET @strSQL = @strSQL + ', dbo.GetCustFldValueItem('
                            + @Fld_id + ', I.numItemCode) as [' + @Fld_Name
                            + ']'

                        SELECT TOP 1
                                @tintOrder = tintOrder + 1 ,
                                @Fld_id = numFieldId ,
                                @Fld_Name = vcFieldName
                        FROM    #Temp1
                        WHERE   Custom = 1
                                AND tintOrder >= @tintOrder
                        ORDER BY tintOrder
                        IF @@rowcount = 0
                            SET @tintOrder = 0
                    END


				--Temp table for Item Search Configuration

                SELECT  *
                INTO    #tempSearch
                FROM    ( SELECT    numFieldId ,
                                    vcDbColumnName ,
                                    ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                                    tintRow AS tintOrder ,
                                    0 AS Custom
                          FROM      View_DynamicColumns
                          WHERE     numFormId = 22
                                    AND numUserCntID = @numUserCntID
                                    AND numDomainID = @numDomainID
                                    AND tintPageType = 1
                                    AND bitCustom = 0
                                    AND ISNULL(bitSettingField, 0) = 1
                                    AND numRelCntType = 1
                          UNION
                          SELECT    numFieldId ,
                                    vcFieldName ,
                                    vcFieldName ,
                                    tintRow AS tintOrder ,
                                    1 AS Custom
                          FROM      View_DynamicCustomColumns
                          WHERE     Grp_id = 5
                                    AND numFormId = 22
                                    AND numUserCntID = @numUserCntID
                                    AND numDomainID = @numDomainID
                                    AND tintPageType = 1
                                    AND bitCustom = 1
                                    AND numRelCntType = 1
                        ) X 
  
                IF NOT EXISTS ( SELECT  *
                                FROM    #tempSearch )
                    BEGIN
                        INSERT  INTO #tempSearch
                                SELECT  numFieldId ,
                                        vcDbColumnName ,
                                        ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                                        tintorder ,
                                        0
                                FROM    View_DynamicDefaultColumns
                                WHERE   numFormId = 22
                                        AND bitDefault = 1
                                        AND ISNULL(bitSettingField, 0) = 1
                                        AND numDomainID = @numDomainID 
                    END

				--Regular Search
                DECLARE @strSearch AS VARCHAR(8000) ,
                    @CustomSearch AS VARCHAR(4000) ,
                    @numFieldId AS NUMERIC(9)
                SET @strSearch = ''
                SELECT TOP 1
                        @tintOrder = tintOrder + 1 ,
                        @Fld_Name = vcDbColumnName ,
                        @numFieldId = numFieldId
                FROM    #tempSearch
                WHERE   Custom = 0
                ORDER BY tintOrder
                WHILE @tintOrder > -1
                    BEGIN
                        IF @Fld_Name = 'vcPartNo'
						BEGIN
                            INSERT  INTO #tempItemCode
                                    SELECT DISTINCT
                                            Item.numItemCode
                                    FROM    Vendor
                                            JOIN Item ON Item.numItemCode = Vendor.numItemCode
                                    WHERE   Item.numDomainID = @numDomainID
                                            AND Vendor.numDomainID = @numDomainID
                                            AND Vendor.vcPartNo IS NOT NULL
                                            AND LEN(Vendor.vcPartNo) > 0
                                            AND vcPartNo LIKE '%' + @str + '%'
							
							SET @vcVendorSearch = 'dbo.Vendor.vcPartNo LIKE ''%' + @str + '%'''
							
						END
                        ELSE IF @Fld_Name = 'vcBarCode'
						BEGIN
                            INSERT  INTO #tempItemCode
                                    SELECT DISTINCT
                                            Item.numItemCode
                                    FROM    Item
                                            JOIN WareHouseItems WI ON WI.numDomainID = @numDomainID
                                                            AND Item.numItemCode = WI.numItemID
                                            JOIN Warehouses W ON W.numDomainID = @numDomainID
                                                            AND W.numWareHouseID = WI.numWareHouseID
                                    WHERE   Item.numDomainID = @numDomainID
                                            AND ISNULL(Item.numItemGroup,
                                                        0) > 0
                                            AND WI.vcBarCode IS NOT NULL
                                            AND LEN(WI.vcBarCode) > 0
                                            AND WI.vcBarCode LIKE '%' + @str + '%'

							SET @vcWareHouseSearch = 'dbo.WareHouseItems.vcBarCode LIKE ''%' + @str + '%'''
						END
                        ELSE IF @Fld_Name = 'vcWHSKU'
						BEGIN
                            INSERT  INTO #tempItemCode
                                    SELECT DISTINCT
                                            Item.numItemCode
                                    FROM    Item
                                            JOIN WareHouseItems WI ON WI.numDomainID = @numDomainID
                                                        AND Item.numItemCode = WI.numItemID
                                            JOIN Warehouses W ON W.numDomainID = @numDomainID
                                                        AND W.numWareHouseID = WI.numWareHouseID
                                    WHERE   Item.numDomainID = @numDomainID
                                            AND ISNULL(Item.numItemGroup,
                                                        0) > 0
                                            AND WI.vcWHSKU IS NOT NULL
                                            AND LEN(WI.vcWHSKU) > 0
                                            AND WI.vcWHSKU LIKE '%'
                                            + @str + '%'

							IF LEN(@vcWareHouseSearch) > 0
								SET @vcWareHouseSearch = @vcWareHouseSearch + ' OR dbo.WareHouseItems.vcWHSKU LIKE ''%' + @str + '%'''
							ELSE
								SET @vcWareHouseSearch = 'dbo.WareHouseItems.vcWHSKU LIKE ''%' + @str + '%'''
						END
                        ELSE
                            SET @strSearch = @strSearch
                                + (CASE @Fld_Name WHEN 'numItemCode' THEN ' I.[' + @Fld_Name ELSE ' [' + @Fld_Name END)  + '] LIKE ''%' + @str
                                + '%'''

                        SELECT TOP 1
                                @tintOrder = tintOrder + 1 ,
                                @Fld_Name = vcDbColumnName ,
                                @numFieldId = numFieldId
                        FROM    #tempSearch
                        WHERE   tintOrder >= @tintOrder
                                AND Custom = 0
                        ORDER BY tintOrder
                        IF @@rowcount = 0
                            BEGIN
                                SET @tintOrder = -1
                            END
                        ELSE
                            IF @Fld_Name != 'vcPartNo'
                                AND @Fld_Name != 'vcBarCode'
                                AND @Fld_Name != 'vcWHSKU'
                                BEGIN
                                    SET @strSearch = @strSearch + ' or '
                                END
                    END

                IF ( SELECT COUNT(*)
                     FROM   #tempItemCode
                   ) > 0
                    SET @strSearch = @strSearch
                        + ' or  I.numItemCode in (select numItemCode from #tempItemCode)' 

				--Custom Search
                SET @CustomSearch = ''
                SELECT TOP 1
                        @tintOrder = tintOrder + 1 ,
                        @Fld_Name = vcDbColumnName ,
                        @numFieldId = numFieldId
                FROM    #tempSearch
                WHERE   Custom = 1
                ORDER BY tintOrder
                WHILE @tintOrder > -1
                    BEGIN
    
                        SET @CustomSearch = @CustomSearch
                            + CAST(@numFieldId AS VARCHAR(10)) 

                        SELECT TOP 1
                                @tintOrder = tintOrder + 1 ,
                                @Fld_Name = vcDbColumnName ,
                                @numFieldId = numFieldId
                        FROM    #tempSearch
                        WHERE   tintOrder >= @tintOrder
                                AND Custom = 1
                        ORDER BY tintOrder
                        IF @@rowcount = 0
                            BEGIN
                                SET @tintOrder = -1
                            END
                        ELSE
                            BEGIN
                                SET @CustomSearch = @CustomSearch + ' , '
                            END
                    END
	
                IF LEN(@CustomSearch) > 0
                    BEGIN
                        SET @CustomSearch = ' I.numItemCode in (SELECT RecId FROM dbo.CFW_FLD_Values_Item WHERE Fld_ID IN ('
                            + @CustomSearch + ') and Fld_Value like ''%'
                            + @str + '%'')'

                        IF LEN(@strSearch) > 0
                            SET @strSearch = @strSearch + ' OR '
                                + @CustomSearch
                        ELSE
                            SET @strSearch = @CustomSearch  
                    END


                IF @tintOppType = 1
                    OR ( @bitRemoveVendorPOValidation = 1
                         AND @tintOppType = 2
                       )
                    BEGIN      
                        SET @strSQL = 'select SRNO=ROW_NUMBER() OVER( ORDER BY  I.vcItemName), I.numItemCode,
									  (SELECT TOP 1 vcPathForImage from ItemImages WHERE numItemCode = I.numItemCode) as vcPathForImage,
									  (SELECT TOP 1 vcPathForTImage from ItemImages WHERE numItemCode = I.numItemCode) as vcPathForTImage,
									  isnull(vcItemName,'''') vcItemName,
									  CASE 
									  WHEN I.[charItemType]=''P''
									  THEN convert(varchar(100),ISNULL((SELECT TOP 1 ROUND(monWListPrice,2) FROM [WareHouseItems] WHERE [numItemID]=I.numItemCode),0)) 
									  ELSE 
										CONVERT(VARCHAR(100), ROUND(monListPrice, 2)) 
									  END AS monListPrice,
									  isnull(vcSKU,'''') vcSKU,
									  isnull(numBarCodeId,0) numBarCodeId,
									  isnull(vcModelID,'''') vcModelID,
									  isnull(txtItemDesc,'''') as txtItemDesc,
									  isnull(C.vcCompanyName,'''') as vcCompanyName,
									  WHT.numWareHouseItemID,
									  dbo.fn_GetAttributes(WHT.numWareHouseItemId,I.bitSerialized) AS [vcAttributes],
									  (CASE 
											WHEN ISNULL(I.bitKitParent,0) = 1 
											THEN 
												(
													CASE
														WHEN (SELECT COUNT(*) FROM ItemDetails INNER JOIN Item ON ItemDetails.numChildItemID = Item.numItemCode WHERE numItemKitID=I.numItemCode AND ISNULL(Item.bitKitParent,0) = 1) > 0
														THEN 1
														ELSE 0
													END
												)
											ELSE 0 
										END) bitHasKitAsChild'
									  + @strSQL
									  + ' from Item  I 
									  Left join DivisionMaster D              
									  on I.numVendorID=D.numDivisionID  
									  left join CompanyInfo C  
									  on C.numCompanyID=D.numCompanyID 
									  LEFT JOIN
											dbo.WareHouseItems WHT
									  ON
											WHT.numItemID = I.numItemCode AND
											WHT.numWareHouseItemID = (' +
																			CASE LEN(@vcWareHouseSearch)
																			WHEN 0 
																			THEN 
																				'SELECT 
																					TOP 1 numWareHouseItemID 
																				FROM 
																					dbo.WareHouseItems 
																				WHERE 
																					dbo.WareHouseItems.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' 
																					AND dbo.WareHouseItems.numItemID = I.numItemCode 
																					AND (dbo.WareHouseItems.numWareHouseID = ' +  CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20))  + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0)'
																			ELSE
																				'CASE 
																					WHEN (SELECT 
																								COUNT(*) 
																							FROM 
																								dbo.WareHouseItems 
																							WHERE 
																								dbo.WareHouseItems.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' 
																								AND dbo.WareHouseItems.numItemID = I.numItemCode 
																								AND (dbo.WareHouseItems.numWareHouseID = ' +  CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20))  + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0)' +
																								' AND (' + @vcWareHouseSearch + ')) > 0 
																					THEN
																						(SELECT 
																							TOP 1 numWareHouseItemID 
																						FROM 
																							dbo.WareHouseItems 
																						WHERE 
																							dbo.WareHouseItems.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' 
																							AND dbo.WareHouseItems.numItemID = I.numItemCode 
																							AND (dbo.WareHouseItems.numWareHouseID = ' +  CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20))  + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0) AND (' + @vcWareHouseSearch + '))
																					ELSE
																						(SELECT 
																							TOP 1 numWareHouseItemID 
																						FROM 
																							dbo.WareHouseItems 
																						WHERE 
																							dbo.WareHouseItems.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' 
																							AND dbo.WareHouseItems.numItemID = I.numItemCode 
																							AND (dbo.WareHouseItems.numWareHouseID = ' +  CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20))  + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0))
																					END' 
																			END
																		+
																	') 
										where ((I.charItemType <> ''P'') OR ((SELECT COUNT(*) FROM dbo.WareHouseItems WHERE numItemID = I.numItemCode AND numDomainID = '
									  + CONVERT(VARCHAR(20), @numDomainID)
									  + ' AND numWareHouseID =  ' +  CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20))  + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0) > 0)) AND ISNULL(I.Isarchieve,0) <> 1 And I.charItemType NOT IN(''A'') AND  I.numDomainID='
									  + CONVERT(VARCHAR(20), @numDomainID) + ' and ('
									  + @strSearch + ') '   
							      
						--- added Asset validation  by sojan
                        IF @bitRemoveVendorPOValidation = 1
                            AND @tintOppType = 2
                            SET @strSQL = @strSQL
                                + ' and ISNULL(I.bitKitParent,0) = 0 AND ISNULL(I.bitAssembly,0) = 0 '

                        IF @tintSearchOrderCustomerHistory = 1
                            AND @numDivisionID > 0
                            BEGIN
                                SET @strSQL = @strSQL
                                    + ' and I.numItemCode in (select distinct oppI.numItemCode from 
									OpportunityMaster oppM join OpportunityItems oppI on oppM.numOppId=oppI.numOppId where oppM.numDomainID='
                                    + CONVERT(VARCHAR(20), @numDomainID)
                                    + ' and oppM.numDivisionId='
                                    + CONVERT(VARCHAR(20), @numDivisionId)
                                    + ')'
                            END

                        INSERT  INTO @TableRowCount
                                EXEC
                                    ( 'SELECT COUNT(*) FROM ( ' + @strSQL
                                      + ') as t2'
                                    );
                        SELECT  @TotalCount = Value
                        FROM    @TableRowCount;

                        SET @strNewQuery = 'select * from (' + @strSQL
                            + ') as t where SRNO> '
                            + CAST(( ( @numPageIndex - 1 ) * @numPageSize ) AS VARCHAR(10))
                            + ' and SRNO < '
                            + CAST(( ( @numPageIndex * @numPageSize ) + 1 ) AS VARCHAR(10))
                            + ' order by  vcItemName'
                        
						--PRINT @strNewQuery
						EXEC(@strNewQuery)
                    END      
                ELSE
                    IF @tintOppType = 2
                        BEGIN      
                            SET @strSQL = 'SELECT SRNO=ROW_NUMBER() OVER( ORDER BY  I.vcItemName), I.numItemCode,
										  (SELECT TOP 1 vcPathForImage from ItemImages WHERE numItemCode = I.numItemCode) as vcPathForImage,
										  (SELECT TOP 1 vcPathForTImage from ItemImages WHERE numItemCode = I.numItemCode) as vcPathForTImage,
										  ISNULL(vcItemName,'''') vcItemName,
										  convert(varchar(200),round(monListPrice,2)) as monListPrice,
										  isnull(vcSKU,'''') vcSKU,
										  isnull(numBarCodeId,0) numBarCodeId,
										  isnull(vcModelID,'''') vcModelID,
										  isnull(txtItemDesc,'''') as txtItemDesc,
										  isnull(C.vcCompanyName,'''') as vcCompanyName,
										  WHT.numWareHouseItemID,
										  dbo.fn_GetAttributes(WHT.numWareHouseItemId,I.bitSerialized) AS [vcAttributes],
										  (CASE 
												WHEN ISNULL(I.bitKitParent,0) = 1 
												THEN 
													(
														CASE
															WHEN (SELECT COUNT(*) FROM ItemDetails INNER JOIN Item ON ItemDetails.numChildItemID = Item.numItemCode WHERE numItemKitID=I.numItemCode AND ISNULL(Item.bitKitParent,0) = 1) > 0
															THEN 1
															ELSE 0
														END
													)
												ELSE 0 
											END) bitHasKitAsChild'
										  + @strSQL
										  + '  from item I 
										  INNER JOIN
											dbo.Vendor V
										  ON
											V.numItemCode = I.numItemCode
										  Left join 
											DivisionMaster D              
										  ON 
											V.numVendorID=D.numDivisionID  
										  LEFT join 
											CompanyInfo C  
										  ON 
											C.numCompanyID=D.numCompanyID    
										  LEFT JOIN
											dbo.WareHouseItems WHT
										  ON
											WHT.numItemID = I.numItemCode AND
											WHT.numWareHouseItemID = (' +
																			CASE LEN(@vcWareHouseSearch)
																			WHEN 0 
																			THEN 
																				'SELECT 
																					TOP 1 numWareHouseItemID 
																				FROM 
																					dbo.WareHouseItems 
																				WHERE 
																					dbo.WareHouseItems.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' 
																					AND dbo.WareHouseItems.numItemID = I.numItemCode 
																					AND (dbo.WareHouseItems.numWareHouseID = ' +  CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20))  + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0)'
																			ELSE
																				'CASE 
																					WHEN (SELECT 
																								COUNT(*) 
																							FROM 
																								dbo.WareHouseItems 
																							WHERE 
																								dbo.WareHouseItems.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' 
																								AND dbo.WareHouseItems.numItemID = I.numItemCode 
																								AND (dbo.WareHouseItems.numWareHouseID = ' +  CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20))  + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0)' +
																								' AND (' + @vcWareHouseSearch + ')) > 0 
																					THEN
																						(SELECT 
																							TOP 1 numWareHouseItemID 
																						FROM 
																							dbo.WareHouseItems 
																						WHERE 
																							dbo.WareHouseItems.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' 
																							AND dbo.WareHouseItems.numItemID = I.numItemCode 
																							AND (dbo.WareHouseItems.numWareHouseID = ' +  CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20))  + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0) AND (' + @vcWareHouseSearch + '))
																					ELSE
																						(SELECT 
																							TOP 1 numWareHouseItemID 
																						FROM 
																							dbo.WareHouseItems 
																						WHERE 
																							dbo.WareHouseItems.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' 
																							AND dbo.WareHouseItems.numItemID = I.numItemCode 
																							AND (dbo.WareHouseItems.numWareHouseID = ' +  CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20))  + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0))
																					END' 
																			END
																		+
																	')
										       
										  WHERE (
													(I.charItemType <> ''P'') OR 
													((SELECT COUNT(*) FROM dbo.WareHouseItems WHERE numItemID = I.numItemCode AND numDomainID = '
										 + CONVERT(VARCHAR(20), @numDomainID)
										 + ' AND numWareHouseID =  ' +  CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20))  + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0) > 0)) AND ISNULL(I.Isarchieve,0) <> 1 and ISNULL(I.bitKitParent,0) = 0  AND ISNULL(I.bitAssembly,0) = 0 And I.charItemType NOT IN(''A'') AND V.numVendorID='
										 + CONVERT(VARCHAR(20), @numDivisionID)
										 + ' and ('+ CASE LEN(@vcVendorSearch)
																			WHEN 0 THEN ''
																			ELSE REPLACE(@vcVendorSearch,'dbo.Vendor','V') + ' OR '
																			END + @strSearch + ') ' 

							--- added Asset validation  by sojan
                            IF @tintSearchOrderCustomerHistory = 1
                                AND @numDivisionID > 0
                                BEGIN
                                    SET @strSQL = @strSQL
                                        + ' and I.numItemCode in (select distinct oppI.numItemCode from 
									OpportunityMaster oppM join OpportunityItems oppI on oppM.numOppId=oppI.numOppId where oppM.numDomainID='
                                        + CONVERT(VARCHAR(20), @numDomainID)
                                        + ' and oppM.numDivisionId='
                                        + CONVERT(VARCHAR(20), @numDivisionId)
                                        + ')'
                                END


                            INSERT  INTO @TableRowCount
                                    EXEC
                                        ( 'SELECT COUNT(*) FROM ( ' + @strSQL
                                          + ') as t2'
                                        );
                            SELECT  @TotalCount = Value
                            FROM    @TableRowCount;

                            SET @strNewQuery = 'select * from (' + @strSQL
                                + ') as t where SRNO> '
                                + CAST(( ( @numPageIndex - 1 ) * @numPageSize ) AS VARCHAR(10))
                                + ' and SRNO < '
                                + CAST(( ( @numPageIndex * @numPageSize ) + 1 ) AS VARCHAR(10))
                                + ' order by  vcItemName'
                            
							--PRINT @strNewQuery
							EXEC(@strNewQuery)
                        END
                    ELSE
                        SELECT  0  
            END
    END
ELSE
    SELECT  0  

SELECT  *
FROM    #Temp1
WHERE   vcDbColumnName NOT IN ( 'vcPartNo', 'vcBarCode', 'vcWHSKU' )ORDER BY tintOrder 
   


IF OBJECT_ID('dbo.Scores', 'U') IS NOT NULL
  DROP TABLE dbo.Scores

IF OBJECT_ID('tempdb..#Temp1') IS NOT NULL
BEGIN
    DROP TABLE #Temp1
END

IF OBJECT_ID('tempdb..#tempSearch') IS NOT NULL
BEGIN
    DROP TABLE #tempSearch
END

IF OBJECT_ID('tempdb..#tempItemCode') IS NOT NULL
BEGIN
    DROP TABLE #tempItemCode
END


    END
/****** Object:  StoredProcedure [dbo].[USP_ItemPricingRecomm]    Script Date: 02/19/2009 01:33:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- exec USP_ItemPricingRecomm @numItemCode=173028,@units=1,@numOppID=0,@numDivisionID=7052,@numDomainID=72,@tintOppType=1,@CalPrice=$50,@numWareHouseItemID=130634,@bitMode=0
--created by anoop jayaraj                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_itempricingrecomm')
DROP PROCEDURE usp_itempricingrecomm
GO
CREATE PROCEDURE [dbo].[USP_ItemPricingRecomm]                                        
@numItemCode as numeric(9),                                        
@units integer,                                  
@numOppID as numeric(9)=0,                                
@numDivisionID as numeric(9)=0,                  
@numDomainID as numeric(9)=0,            
@tintOppType as tinyint,          
@CalPrice as money,      
@numWareHouseItemID as numeric(9),
@bitMode tinyint=0,
@vcSelectedKitChildItems VARCHAR(MAX) = '',
@numOppItemID NUMERIC(18,0) = 0
as                 
BEGIN TRY

DECLARE @numRelationship AS NUMERIC(9)
DECLARE @numProfile AS NUMERIC(9)             
DECLARE @monListPrice MONEY
DECLARE @bitCalAmtBasedonDepItems BIT
DECLARE @numDefaultSalesPricing TINYINT
DECLARE @tintPriceLevel INT
DECLARE @decmUOMConversion decimal(18,2)
DECLARE @intLeadTimeDays INT

/*Profile and relationship id */
SELECT 
	@numRelationship=numCompanyType,
	@numProfile=vcProfile,
	@tintPriceLevel = ISNULL(D.tintPriceLevel,0)
FROM 
	DivisionMaster D                  
JOIN 
	CompanyInfo C 
ON 
	C.numCompanyId=D.numCompanyID                  
WHERE 
	numDivisionID =@numDivisionID       
            
IF @tintOppType=1            
BEGIN
	SELECT
		@bitCalAmtBasedonDepItems=ISNULL(bitCalAmtBasedonDepItems,0) 
	FROM 
		Item 
	WHERE 
		numItemCode = @numItemCode
      
	/*Get List Price for item*/      
	IF((@numWareHouseItemID>0) AND EXISTS(SELECT * FROM Item WHERE numItemCode=@numItemCode AND charItemType='P'))      
	BEGIN      
		SELECT 
			@monListPrice=ISNULL(monWListPrice,0) 
		FROM 
			WareHouseItems 
		WHERE 
			numWareHouseItemID=@numWareHouseItemID      
		
		IF @monListPrice=0 
		BEGIN
			SELECT @monListPrice=monListPrice FROM Item WHERE numItemCode=@numItemCode       
		END
	END 
	ELSE
	BEGIN
		 SELECT 
			@monListPrice=monListPrice 
		FROM 
			Item 
		WHERE 
			numItemCode=@numItemCode      
	END      

	/*Use Calculated Price When item is Assembly/Kit,Calculate price based on sum of dependent items.*/
	IF @bitCalAmtBasedonDepItems = 1 
	BEGIN
		CREATE TABLE #TEMPSelectedKitChilds
		(
			ChildKitItemID NUMERIC(18,0),
			ChildKitWarehouseItemID NUMERIC(18,0),
			ChildKitChildItemID NUMERIC(18,0),
			ChildKitChildWarehouseItemID NUMERIC(18,0)
		)


		IF ISNULL(@numOppItemID,0) > 0
		BEGIN
			INSERT INTO 
				#TEMPSelectedKitChilds
			SELECT 
				OKI.numChildItemID,
				OKI.numWareHouseItemId,
				OKCI.numItemID,
				OKCI.numWareHouseItemId
			FROM
				OpportunityKitItems OKI
			INNER JOIN
				OpportunityKitChildItems OKCI
			ON
				OKI.numOppChildItemID = OKCI.numOppChildItemID
			WHERE
				OKI.numOppId = @numOppID
				AND OKI.numOppItemID = @numOppItemID
		END
		ELSE IF ISNULL(@vcSelectedKitChildItems,'') <> ''
		BEGIN
			INSERT INTO 
				#TEMPSelectedKitChilds
			SELECT 
				SUBSTRING
				(
					SUBSTRING(OutParam,0,CHARINDEX('-',OutParam)),
					0,
					CHARINDEX('#', SUBSTRING(OutParam,0,CHARINDEX('-',OutParam)))
				) AS numChildKitID,
				SUBSTRING
				(
					SUBSTRING(OutParam,0,CHARINDEX('-',OutParam)),
					CHARINDEX('#', SUBSTRING(OutParam,0,CHARINDEX('-',OutParam))) + 1,
					LEN(SUBSTRING(OutParam,0,CHARINDEX('-',OutParam)))
				) AS numChildKitWarehouseItemID,
				SUBSTRING
				(
					SUBSTRING(OutParam,CHARINDEX('-',OutParam) + 1, LEN(OutParam)),
					0,
					CHARINDEX('#', SUBSTRING(OutParam,CHARINDEX('-',OutParam) + 1, LEN(OutParam)))
				) AS numChildKitID,
				SUBSTRING
				(
					SUBSTRING(OutParam,CHARINDEX('-',OutParam) + 1, LEN(OutParam)),
					CHARINDEX('#', SUBSTRING(OutParam,CHARINDEX('-',OutParam) + 1, LEN(OutParam))) + 1,
					LEN(SUBSTRING(OutParam,CHARINDEX('-',OutParam) + 1, LEN(OutParam)))
				) AS numChildKitWarehouseItemID
		
			FROM 
				dbo.SplitString(@vcSelectedKitChildItems,',')
		END

		;WITH CTE (numItemCode, vcItemName, bitKitParent, numWarehouseItemID,numQtyItemsReq,numUOMId) AS
		(
			SELECT
				ID.numChildItemID,
				I.vcItemName,
				ISNULL(I.bitKitParent,0),
				ISNULL(ID.numWarehouseItemId,0),
				(ISNULL(ID.numQtyItemsReq,0) * ISNULL(dbo.fn_UOMConversion(ISNULL(ID.numUOMID, 0), I.numItemCode, @numDomainID, ISNULL(I.numBaseUnit, 0)),1)),
				ISNULL(numUOMId,0)
			FROM 
				[ItemDetails] ID
			INNER JOIN 
				[Item] I 
			ON 
				ID.[numChildItemID] = I.[numItemCode]
			WHERE   
				[numItemKitID] = @numItemCode
			UNION ALL
			SELECT
				ID.numChildItemID,
				I.vcItemName,
				ISNULL(I.bitKitParent,0),
				ISNULL(ID.numWarehouseItemId,0),
				(ISNULL(ID.numQtyItemsReq,0) * ISNULL(dbo.fn_UOMConversion(ISNULL(ID.numUOMID, 0), I.numItemCode, @numDomainID, ISNULL(I.numBaseUnit, 0)),1)),
				ISNULL(ID.numUOMId,0)
			FROM 
				CTE As Temp1
			INNER JOIN
				[ItemDetails] ID
			ON
				ID.numItemKitID = Temp1.numItemCode
			INNER JOIN 
				[Item] I 
			ON 
				ID.[numChildItemID] = I.[numItemCode]
			INNER JOIN
				#TEMPSelectedKitChilds
			ON
				ISNULL(Temp1.bitKitParent,0) = 1
				AND ID.numChildItemID = #TEMPSelectedKitChilds.ChildKitChildItemID
				AND ID.numWareHouseItemId = #TEMPSelectedKitChilds.ChildKitChildWarehouseItemID
		)

		SELECT  
				@CalPrice = ISNULL(  SUM(ISNULL(CASE WHEN I.[charItemType]='P' 
				THEN WI.[monWListPrice] 
				ELSE monListPrice END,0) * ID.[numQtyItemsReq] )  ,0)
		FROM    CTE ID
				INNER JOIN [Item] I ON ID.numItemCode = I.[numItemCode]
				left join  WareHouseItems WI
				on WI.numItemID=I.numItemCode and WI.[numWareHouseItemID]=ID.numWarehouseItemID
		WHERE
			ISNULL(ID.bitKitParent,0) = 0

		DROP TABLE #TEMPSelectedKitChilds
	END


	SELECT 
		@numDefaultSalesPricing = ISNULL(numDefaultSalesPricing,2) 
	FROM 
		Domain 
	WHERE 
		numDomainID = @numDomainID

	IF @numDefaultSalesPricing = 1 -- Use Price Level
	BEGIN
		DECLARE @newPrice MONEY
		DECLARE @finalUnitPrice FLOAT = 0
		DECLARE @tintRuleType INT
		DECLARE @tintDiscountType INT
		DECLARE @decDiscount FLOAT
		DECLARE @ItemPrice FLOAT

		SET @tintRuleType = 0
		SET @tintDiscountType = 0
		SET @decDiscount  = 0
		SET @ItemPrice = 0

		SELECT TOP 1
			@tintRuleType = tintRuleType,
			@tintDiscountType = tintDiscountType,
			@decDiscount = decDiscount
		FROM 
			PricingTable 
		WHERE 
			numItemCode = @numItemCode AND
			((@units BETWEEN intFromQty AND intToQty) OR tintRuleType = 3)

		IF @tintRuleType > 0 AND @tintDiscountType > 0
		BEGIN
			IF @tintRuleType = 1 -- Deduct from List price
			BEGIN
				IF (SELECT ISNULL(charItemType,'') FROM Item WHERE numDomainID = @numDomainID AND numItemCode = @numItemCode) = 'P'
					If (SELECT COUNT(*) FROM Item WHERE numDomainID = @numDomainID AND numItemCode = @numItemCode AND (bitKitParent = 1 OR bitAssembly = 1)) > 0 
						SELECT @ItemPrice = dbo.GetUnitPriceForKitOrAssembly(@numDomainID,@numItemCode,@numWarehouseItemID,@units)
					ELSE
						SELECT @ItemPrice = ISNULL(monWListPrice,0) FROM WareHouseItems WHERE numDomainID = @numDomainID AND numItemID = @numItemCode AND numWareHouseItemID = @numWarehouseItemID
				ELSE
					SELECT @ItemPrice = ISNULL(monListPrice,0) FROM Item WHERE numDomainID = @numDomainID AND numItemCode = @numItemCode

				IF @tintDiscountType = 1 -- Percentage
				BEGIN
					IF @decDiscount > 0
						SELECT @finalUnitPrice = @ItemPrice - (@ItemPrice * (@decDiscount/100))
				END
				ELSE IF @tintDiscountType = 2 -- Flat Amount
				BEGIN
					SELECT @finalUnitPrice = @ItemPrice - @decDiscount
				END
			END
			ELSE IF @tintRuleType = 2 -- Add to Primary Vendor Cost
			BEGIN
				If (SELECT COUNT(*) FROM Item WHERE numDomainID = @numDomainID AND numItemCode = @numItemCode AND (bitKitParent = 1 OR bitAssembly = 1)) > 0 
					SELECT @ItemPrice = dbo.GetUnitPriceForKitOrAssembly(@numDomainID,@numItemCode,@numWarehouseItemID,@units)
				ELSE
					SELECT @ItemPrice = ISNULL(monCost,0) FROM Vendor WHERE numItemCode = @numItemCode AND numVendorID = (SELECT numVendorID FROM Item WHERE numItemCode = @numItemCode)

				If @ItemPrice > 0
				BEGIN
					IF @tintDiscountType = 1 -- Percentage
					BEGIN
						IF @decDiscount > 0
							SELECT @finalUnitPrice = @ItemPrice + (@ItemPrice * (@decDiscount/100))
					END
					ELSE IF @tintDiscountType = 2 -- Flat Amount
					BEGIN
						SELECT @finalUnitPrice = @ItemPrice + @decDiscount
					END
				END
			END
			ELSE IF @tintRuleType = 3 -- Named Price
			BEGIN
				IF ISNULL(@tintPriceLevel,0) > 0
				BEGIN
					SET @decDiscount = 0
					SET @tintDiscountType = 2
					SET @finalUnitPrice = 0 
					
					SELECT 
						@finalUnitPrice = ISNULL(decDiscount,0)
					FROM
					(
						SELECT 
							ROW_NUMBER() OVER(PARTITION BY PricingTable.numItemCode ORDER BY numPricingID) Id,
							decDiscount
						FROM 
							PricingTable 
						WHERE 
						    PricingTable.numItemCode = @numItemCode 
							AND tintRuleType = 3 
					) TEMP
					WHERE
						Id = @tintPriceLevel

					IF ISNULL(@finalUnitPrice,0) = 0
					BEGIN
						SET @finalUnitPrice = @monListPrice
					END
					ELSE
					BEGIN
						SET @monListPrice = @finalUnitPrice
					END
				END
				ELSE
				BEGIN
					SET @finalUnitPrice = @decDiscount
					SET @monListPrice = @finalUnitPrice
					-- KEEP THIS LINE AT END
					SET @decDiscount = 0
					SET @tintDiscountType = 2
				END				
			END
		END

		IF @finalUnitPrice = 0
			SET @newPrice = @monListPrice
		ELSE
			SET @newPrice = @finalUnitPrice

		If @tintRuleType = 2
		BEGIN
			IF @finalUnitPrice > 0
			BEGIN
				IF @tintDiscountType = 1 -- Percentage
				BEGIN
					IF @newPrice > @monListPrice
						SET @decDiscount = 0
					ELSE
						If @monListPrice > 0
							SET @decDiscount = ((@monListPrice - @newPrice) * 100) / CAST(@monListPrice AS FLOAT)
						ELSE
							SET @decDiscount = 0
				END
				ELSE IF @tintDiscountType = 2 -- Flat Amount
				BEGIN
					If @newPrice > @monListPrice
						SET @decDiscount = 0
					ELSE
						SET @decDiscount = @monListPrice - @newPrice
				END
			END
			ELSE
			BEGIN
				SET @decDiscount = 0
				SET @tintDiscountType = 0
			END
		END

		SELECT 
			1 as numUintHour, 
			ISNULL(@newPrice,0) AS ListPrice,
			'' AS vcPOppName,
			'' AS bintCreatedDate,
			CASE 
				WHEN @tintRuleType = 0 THEN 'Price - List price'
				ELSE 
				   CASE @tintRuleType
				   WHEN 1 
						THEN 
							'Deduct from List price ' +  CASE 
														   WHEN @tintDiscountType = 1 THEN CONVERT(VARCHAR(20), @decDiscount) + '%'
														   ELSE 'flat ' + CONVERT(VARCHAR(20), CAST(@decDiscount AS int))
														   END 
				   WHEN 2 THEN 'Add to primary vendor cost '  +  CASE 
														   WHEN @tintDiscountType = 1 THEN CONVERT(VARCHAR(20), @decDiscount) + '%'
														   ELSE 'flat ' + CONVERT(VARCHAR(20), CAST(@decDiscount AS int))
														   END 
				   ELSE 'Flat ' + CONVERT(VARCHAR(20), @decDiscount)
				   END
			END AS OppStatus,
			CAST(0 AS NUMERIC) AS numPricRuleID,
			CAST(1 AS TINYINT) AS tintPricingMethod,
			CAST(@tintDiscountType AS TINYINT) AS tintDiscountTypeOriginal,
			CAST(@decDiscount AS FLOAT) AS decDiscountOriginal,
			CAST(@tintRuleType AS TINYINT) AS tintRuleType
	END
	ELSE -- Use Price Rule
	BEGIN
		/* Checks Pricebook if exist any.. */      
		SELECT TOP 1 1 AS numUnitHour,
				dbo.GetPriceBasedOnPriceBook(P.[numPricRuleID],@monListPrice,@units,I.numItemCode) 
				* (CASE WHEN (P.tintRuleType=2 OR (P.tintRuleType=0 AND ISNULL(PBT.tintRuleType,0) = 2)) THEN dbo.fn_UOMConversion(numBaseUnit,@numItemCode,@numDomainId,CASE WHEN ISNULL(numPurchaseUnit,0)>0 THEN numPurchaseUnit ELSE numBaseUnit END) ELSE 1 END)  AS ListPrice,
				vcRuleName AS vcPOppName,
				CONVERT(VARCHAR(20), NULL) AS bintCreatedDate,
				CASE WHEN numPricRuleID IS NULL THEN 'Price - List price'
					 ELSE 
					  CASE WHEN P.tintPricingMethod = 1
										 THEN 'Price Book Rule'
										 ELSE CASE PBT.tintRuleType
												WHEN 1 THEN 'Deduct from List price '
												WHEN 2 THEN 'Add to primary vendor cost '
												ELSE ''
											  END
											  + CASE WHEN tintDiscountType = 1
													 THEN CONVERT(VARCHAR(20), decDiscount)
														  + '%'
													 ELSE 'flat ' + CONVERT(VARCHAR(20), CAST(decDiscount AS int))
												END + ' for every '
											  + CONVERT(VARCHAR(20), CASE WHEN [intQntyItems] = 0 THEN 1
																		  ELSE intQntyItems
																	 END)
											  + ' units, until maximum of '
											  + CONVERT(VARCHAR(20), decMaxDedPerAmt)
											  + CASE WHEN tintDiscountType = 1 THEN ' %' ELSE ' Flat' END END
				END AS OppStatus,P.numPricRuleID,P.tintPricingMethod,PBT.tintDiscountTypeOriginal,CAST(PBT.decDiscountOriginal AS FLOAT) AS decDiscountOriginal,PBT.tintRuleType
		FROM    Item I
				LEFT JOIN PriceBookRules P ON I.numDomainID = P.numDomainID
				LEFT JOIN PriceBookRuleDTL PDTL ON P.numPricRuleID = PDTL.numRuleID
				LEFT JOIN PriceBookRuleItems PBI ON P.numPricRuleID = PBI.numRuleID
				LEFT JOIN [PriceBookPriorities] PP ON PP.[Step2Value] = P.[tintStep2] AND PP.[Step3Value] = P.[tintStep3]
				CROSS APPLY dbo.GetPriceBasedOnPriceBookTable(P.[numPricRuleID],CASE WHEN bitCalAmtBasedonDepItems=1 then @CalPrice ELSE @monListPrice END ,@units,I.numItemCode) as PBT
		WHERE   numItemCode = @numItemCode AND tintRuleFor=@tintOppType
		AND 
		(
		((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 1
		OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND (tintStep3= 1 AND PDTL.numValue = @numDivisionID)) -- Priority 2
		OR ((tintStep2 = 3) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 3

		OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 4
		OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 5
		OR ((tintStep2 = 3) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 6

		OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 3)) -- Priority 7
		OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND (tintStep3 = 3)) -- Priority 8
		OR ((tintStep2 = 3) and (tintStep3 = 3)) -- Priority 9
		)
		ORDER BY PP.Priority ASC
	END

	IF @bitMode=2  -- When this procedure is being called from USP_GetPriceOfItem for e-com
		RETURN 

                                   
	select 1 as numUnitHour,convert(money,ISNULL((monTotAmount/(case when numUnitHour< 1 then 1 else numUnitHour end)),0)) as ListPrice,vcPOppName,convert(varchar(20),bintCreatedDate) as bintCreatedDate,case when tintOppStatus=0 then 'Opportunity' when tintOppStatus=1 then 'Deal won'   
	when tintOppStatus=2 then 'Deal Lost' end as OppStatus,CAST(0 AS NUMERIC) AS numPricRuleID,CAST(0 AS TINYINT) AS tintPricingMethod,CAST(0 AS TINYINT) as tintDiscountTypeOriginal,CAST(0 AS FLOAT) as decDiscountOriginal,CAST(0 AS TINYINT) as tintRuleType                         
	from OpportunityItems itm                                        
	join OpportunityMaster mst                                        
	on mst.numOppId=itm.numOppId              
	where  tintOppType=1 and numItemCode=@numItemCode and mst.numDivisionID=@numDivisionID order by OppStatus desc  
  
  
	select 1 as numUnitHour,CASE WHEN @bitCalAmtBasedonDepItems=1 THEN convert(money,ISNULL(@CalPrice,0)) ELSE convert(money,ISNULL(@monListPrice,0)) End as ListPrice,'' as vcPOppName,convert(varchar(20),null) as bintCreatedDate,CASE WHEN @bitCalAmtBasedonDepItems=1 THEN 'Sum of dependent items' ELSE '' END as OppStatus
	,CAST(0 AS NUMERIC) AS numPricRuleID,CAST(0 AS TINYINT) AS tintPricingMethod ,CAST(0 AS TINYINT) as tintDiscountTypeOriginal,CAST(0 AS FLOAT) as decDiscountOriginal,CAST(0 AS TINYINT) as tintRuleType                    
END             
ELSE            
BEGIN

	If @numDivisionID=0
	BEGIN
		SELECT 
			@numDivisionID=V.numVendorID 
		FROM 
			[Vendor] V 
		INNER JOIN 
			Item I 
		ON 
			V.numVendorID = I.numVendorID 
			AND V.numItemCode=I.numItemCode 
		WHERE 
			V.[numItemCode]=@numItemCode 
			AND V.[numDomainID]=@numDomainID 
	END

	IF EXISTS
	(
		SELECT 
			ISNULL([monCost],0) ListPrice 
		FROM 
			[Vendor] V 
		INNER JOIN 
			dbo.Item I 
		ON 
			V.numItemCode = I.numItemCode
		WHERE
			V.[numItemCode]=@numItemCode 
			AND V.[numDomainID]=@numDomainID 
			AND V.numVendorID=@numDivisionID
	)       
	BEGIN      
		SELECT 
			@monListPrice=ISNULL([monCost],0) 
		FROM 
			[Vendor] V 
		INNER JOIN 
			dbo.Item I 
		ON 
			V.numItemCode = I.numItemCode
		WHERE 
			V.[numItemCode]=@numItemCode 
			AND V.[numDomainID]=@numDomainID 
			AND V.numVendorID=@numDivisionID
	END      
	ELSE
	BEGIN   
		SELECT 
			@monListPrice=ISNULL([monCost],0) 
		FROM 
			[Vendor] 
		WHERE 
			[numItemCode]=@numItemCode 
			AND [numDomainID]=@numDomainID
	END      
 

	SELECT TOP 1 
		1 AS numUnitHour,
		ISNULL(dbo.GetPriceBasedOnPriceBook(P.[numPricRuleID],@monListPrice,@units,I.numItemCode),0) AS ListPrice,
		vcRuleName AS vcPOppName,
		CONVERT(VARCHAR(20), NULL) AS bintCreatedDate,
		CASE 
		WHEN numPricRuleID IS NULL THEN 'Price - List price'
		ELSE  
				CASE 
				WHEN P.tintPricingMethod = 1 THEN 'Price Book Rule'
				ELSE 
					(CASE tintRuleType
						WHEN 1 THEN 'Deduct from List price '
						WHEN 2 THEN 'Add to primary vendor cost '
						ELSE ''
					END) + 
					(CASE 
						WHEN tintDiscountType = 1 THEN CONVERT(VARCHAR(20), decDiscount) + '%'
						ELSE 'flat ' + CONVERT(VARCHAR(20), CAST(decDiscount AS int))
					END) + ' for every ' + 
					CONVERT(VARCHAR(20), (CASE WHEN [intQntyItems] = 0 THEN 1 ELSE intQntyItems END))
					+ ' units, until maximum of '
					+ CONVERT(VARCHAR(20), decMaxDedPerAmt)
					+ (CASE WHEN tintDiscountType = 1 THEN ' %' ELSE ' Flat' END)
				END
		END AS OppStatus,
		P.numPricRuleID,
		P.tintPricingMethod,
		@numDivisionID AS numDivisionID
	FROM
		Item I 
	LEFT JOIN PriceBookRules P ON I.numDomainID = P.numDomainID
	LEFT JOIN PriceBookRuleDTL PDTL ON P.numPricRuleID = PDTL.numRuleID
	LEFT JOIN PriceBookRuleItems PBI ON P.numPricRuleID = PBI.numRuleID
	LEFT JOIN [PriceBookPriorities] PP ON PP.[Step2Value] = P.[tintStep2] AND PP.[Step3Value] = P.[tintStep3]
	WHERE   
		numItemCode = @numItemCode 
		AND tintRuleFor=@tintOppType
		AND (
			((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 1
			OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND (tintStep3= 1 AND PDTL.numValue = @numDivisionID)) -- Priority 2
			OR ((tintStep2 = 3) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 3

			OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 4
			OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 5
			OR ((tintStep2 = 3) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 6

			OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 3)) -- Priority 7
			OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND (tintStep3 = 3)) -- Priority 8
			OR ((tintStep2 = 3) and (tintStep3 = 3)) -- Priority 9
			)
	ORDER BY PP.Priority ASC

	SET @decmUOMConversion=(SELECT 
								dbo.fn_UOMConversion(ISNULL(I.numPurchaseUnit, 0) , I.numItemCode,I.numDomainId, ISNULL(I.numBaseUnit, 0)) 
							FROM 
								Item I 
							WHERE 
								I.numItemCode=@numItemCode)
	SET @intLeadTimeDays=(SELECT 
							intLeadTimeDays 
						  FROM 
							Vendor 
						  WHERE 
							numItemCode=@numItemCode)

	select 1 as numUnitHour,convert(money,ISNULL((monTotAmount/(case when numUnitHour< 1 then 1 else numUnitHour end)),0)) as ListPrice,
	@decmUOMConversion *(convert(money,ISNULL((monTotAmount/(case when numUnitHour< 1 then 1 else numUnitHour end)),0)))
	as convrsPrice
	,vcPOppName,convert(varchar(20),bintCreatedDate) as bintCreatedDate,case when tintOppStatus=0 then 'Opportunity' 
		 when tintOppStatus=1 then 'Deal won'  
		 when tintOppStatus=2 then 'Deal Lost' end as OppStatus
		 ,CAST(0 AS NUMERIC) AS numPricRuleID,CAST(0 AS TINYINT) AS tintPricingMethod,@numDivisionID as numDivisionID                         
		 from OpportunityItems itm                                        
		 join OpportunityMaster mst                                        
		 on mst.numOppId=itm.numOppId                                        
		 where  tintOppType=2 and numItemCode=@numItemCode and mst.numDivisionID=@numDivisionID order by OppStatus,bintCreatedDate DESC

 		select 1 as numUnitHour,convert(money,ISNULL(@monListPrice,0)) as ListPrice,
		@decmUOMConversion*(Convert(money,ISNULL(@monListPrice,0))) as convrsPrice
		,@intLeadTimeDays as LeadTime,(SELECT intMinQty from Vendor where numItemCode=@numItemCode) as intMinQty,
		(SELECT DATEADD(day,@intLeadTimeDays,GETDATE())) AS EstimatedArrival
		,'' as vcPOppName,convert(varchar(20),null) as bintCreatedDate,''  as OppStatus,@numDivisionID as numDivisionID
 		,CAST(0 AS NUMERIC) AS numPricRuleID,CAST(0 AS TINYINT) AS tintPricingMethod
END

END TRY
BEGIN CATCH
    SELECT 0 AS ListPrice
    SELECT 0 AS ListPrice
    SELECT 0 AS ListPrice
END CATCH
--SELECT * FROM Category
--- Alterd By Chintan Prajapati
-- EXEC dbo.USP_ItemsForECommerce 9,60,1,1,15,0,''
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ItemsForECommerce' ) 
    DROP PROCEDURE USP_ItemsForECommerce
GO
CREATE PROCEDURE [dbo].[USP_ItemsForECommerce]
    @numSiteID AS NUMERIC(9) = 0,
    @numCategoryID AS NUMERIC(18) = 0,
    @numWareHouseID AS NUMERIC(9) = 0,
    @SortBy NVARCHAR(MAX) = 0,
    @CurrentPage INT,
    @PageSize INT,
    @TotRecs INT = 0 OUTPUT,
    @SearchText AS NVARCHAR(MAX) = '',--Comma seperated item ids
    @FilterQuery AS NVARCHAR(MAX) = '',----Comma seperated Attributes Ids
    @FilterRegularCondition AS NVARCHAR(MAX),
    --@FilterCustomCondition AS NVARCHAR(MAX),
    @FilterCustomFields AS NVARCHAR(MAX),
    @FilterCustomWhere AS NVARCHAR(MAX)
   
AS 
    BEGIN
		
		DECLARE @numDomainID AS NUMERIC(9)
		SELECT  @numDomainID = numDomainID
        FROM    [Sites]
        WHERE   numSiteID = @numSiteID
        
        DECLARE @tintDisplayCategory AS TINYINT
		SELECT @tintDisplayCategory = ISNULL(bitDisplayCategory,0) FROM dbo.eCommerceDTL WHERE numSiteID = @numSiteId
		PRINT @tintDisplayCategory 
		
		CREATE TABLE #tmpItemCat(numCategoryID NUMERIC(18,0))
		CREATE TABLE #tmpItemCode(numItemCode NUMERIC(18,0))
		IF LEN(@FilterCustomFields) > 0 AND LEN(@FilterCustomWhere) > 0
		BEGIN
--			DECLARE @strCustomSql AS NVARCHAR(MAX)
--			SET @strCustomSql = 'INSERT INTO #tmpItemCode(numItemCode)
--								 SELECT DISTINCT t2.RecId FROM CFW_Fld_Master t1 
--								 JOIN CFW_FLD_Values_Item AS t2 ON t1.Fld_ID = t2.Fld_ID 
--								 WHERE t1.Grp_id = 5 
--								 AND t1.numDomainID = (SELECT numDomainID FROM dbo.Sites WHERE numSiteID = ' + CONVERT(VARCHAR(10),@numSiteID) + ') 
--								 AND t1.fld_type <> ''Link'' AND ' + @FilterCustomCondition
			
			DECLARE @FilterCustomFieldIds AS NVARCHAR(MAX)
			SET @FilterCustomFieldIds = Replace(@FilterCustomFields,'[','')
			SET @FilterCustomFieldIds = Replace(@FilterCustomFieldIds,']','')
			
			DECLARE @strCustomSql AS NVARCHAR(MAX)
			SET @strCustomSql = 'INSERT INTO #tmpItemCode(numItemCode)
								 SELECT DISTINCT T.RecId FROM 
															(
																SELECT * FROM 
																	(
																		SELECT  t2.RecId, t1.fld_id, t2.Fld_Value FROM CFW_Fld_Master t1 
																		JOIN CFW_FLD_Values_Item AS t2 ON t1.Fld_ID = t2.Fld_ID 
																		WHERE t1.Grp_id = 5 
																		AND t1.numDomainID = ' + CONVERT(VARCHAR(10),@numDomainID) + '
																		AND t1.fld_type <> ''Link'' 
																		AND t1.fld_id IN (' + @FilterCustomFieldIds + ')
																	) p  PIVOT (MAX([Fld_Value]) FOR fld_id IN (' + @FilterCustomFields + ')) AS pvt 
															) T  WHERE 1=1 AND ' + @FilterCustomWhere
								 
			PRINT @strCustomSql
			EXEC SP_EXECUTESQL @strCustomSql								 					 
		END
		
        DECLARE @strSQL NVARCHAR(MAX)
        DECLARE @firstRec AS INTEGER
        DECLARE @lastRec AS INTEGER
        DECLARE @Where NVARCHAR(MAX)
        DECLARE @SortString NVARCHAR(MAX)
        DECLARE @searchPositionColumn NVARCHAR(MAX)
        DECLARE @row AS INTEGER
        DECLARE @data AS VARCHAR(100)
        DECLARE @dynamicFilterQuery NVARCHAR(MAX)
        DECLARE @checkFldType VARCHAR(100)
        DECLARE @count NUMERIC(9,0)
        DECLARE @whereNumItemCode NVARCHAR(MAX)
        DECLARE @filterByNumItemCode VARCHAR(100)
        
		SET @SortString = ''
        set @searchPositionColumn = ''
        SET @strSQL = ''
		SET @whereNumItemCode = ''
		SET @dynamicFilterQuery = ''
		 
        SET @firstRec = ( @CurrentPage - 1 ) * @PageSize
        SET @lastRec = ( @CurrentPage * @PageSize + 1 )
                
        DECLARE @bitAutoSelectWarehouse AS BIT
		SELECT @bitAutoSelectWarehouse = ISNULL([ECD].[bitAutoSelectWarehouse],0) FROM [dbo].[eCommerceDTL] AS ECD WHERE [ECD].[numSiteId] = @numSiteID
		AND [ECD].[numDomainId] = @numDomainID

		DECLARE @vcWarehouseIDs AS VARCHAR(1000)
        IF (@numWareHouseID = 0 OR @bitAutoSelectWarehouse = 1)
            BEGIN				
				IF @bitAutoSelectWarehouse = 1
					BEGIN
						SELECT @vcWarehouseIDs = ISNULL( STUFF((SELECT DISTINCT ',' + CAST(numWarehouseID AS VARCHAR(10)) FROM [dbo].[Warehouses] WHERE [Warehouses].[numDomainID] = @numDomainID FOR XML PATH('')),1, 1, '') , '') 
					END
				
				ELSE
					BEGIN
						SELECT  @numWareHouseID = ISNULL(numDefaultWareHouseID,0)
						FROM    [eCommerceDTL]
						WHERE   [numDomainID] = @numDomainID	

						SET @vcWarehouseIDs = @numWareHouseID
					END
            END
		ELSE
		BEGIN
			SET @vcWarehouseIDs = @numWareHouseID
		END

		PRINT @vcWarehouseIDs

		--PRIAMRY SORTING USING CART DROPDOWN
		DECLARE @Join AS NVARCHAR(MAX)
		DECLARE @SortFields AS NVARCHAR(MAX)
		DECLARE @vcSort AS VARCHAR(MAX)

		SET @Join = ''
		SET @SortFields = ''
		SET @vcSort = ''

		IF @SortBy = ''
			BEGIN
				SET @SortString = ' vcItemName Asc '
			END
		ELSE IF CHARINDEX('~0',@SortBy) > 0 OR CHARINDEX('~1',@SortBy) > 0
			BEGIN
				DECLARE @fldID AS NUMERIC(18,0)
				DECLARE @RowNumber AS VARCHAR(MAX)
				
				SELECT * INTO #fldValues FROM dbo.SplitIDsWithPosition(@SortBy,'~')
				SELECT @RowNumber = RowNumber, @fldID = id FROM #fldValues WHERE RowNumber = 1
				SELECT @vcSort = CASE WHEN Id = 0 THEN 'ASC' ELSE 'DESC' END FROM #fldValues WHERE RowNumber = 2
				
				SET @Join = @Join + ' LEFT JOIN CFW_FLD_Values_Item CFVI' + @RowNumber + ' ON I.numItemCode = CFVI' + @RowNumber + '.RecID AND CFVI' + @RowNumber + '.fld_Id = ' + CONVERT(VARCHAR(10),@fldID)
				---SET @SortString = 'CFVI' + @RowNumber + '.fld_Value' 
				SET @SortString = '[fld_Value' + @RowNumber + '] ' --+ @vcSort
				SET @SortFields = ', CFVI' + @RowNumber + '.fld_Value [fld_Value' + @RowNumber + ']'
			END
		ELSE
			BEGIN
				--SET @SortString = @SortBy	
				
				IF CHARINDEX('monListPrice',@SortBy) > 0
				BEGIN
					DECLARE @bitSortPriceMode AS BIT
					SELECT @bitSortPriceMode = ISNULL(bitSortPriceMode,0) FROM dbo.eCommerceDTL WHERE numSiteId = @numSiteID
					PRINT @bitSortPriceMode
					IF @bitSortPriceMode = 1
					BEGIN
						SET @SortString = ' '
					END
					ELSE
					BEGIN
						SET @SortString = @SortBy	
					END
				END
				ELSE
				BEGIN
					SET @SortString = @SortBy	
				END
				
			END	

		--SECONDARY SORTING USING DEFAULT SETTINGS FROM ECOMMERCE
		SELECT ROW_NUMBER() OVER (ORDER BY Custom,vcFieldName) AS [RowID],* INTO #tempSort FROM (
		SELECT  CONVERT(VARCHAR(9),numFieldID)+'~0' AS numFieldID,
				ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName,
				0 AS Custom,
				vcDbColumnName 
		FROM View_DynamicColumns
		WHERE numFormID = 84 
		AND ISNULL(bitSettingField,0) = 1 
		AND ISNULL(bitDeleted,0)=0 
		AND ISNULL(bitCustom,0) = 0
		AND numDomainID = @numDomainID
					       
		UNION     

		SELECT  CONVERT(VARCHAR(9),numFieldID)+'~1' AS numFieldID ,
				vcFieldName ,
				1 AS Custom,
				'' AS vcDbColumnName
		FROM View_DynamicCustomColumns          
		WHERE numFormID = 84
		AND numDomainID = @numDomainID
		AND grp_id = 5 
		AND vcAssociatedControlType = 'TextBox' 
		AND ISNULL(bitCustom,0) = 1 
		AND tintPageType=1  
		) TABLE1 ORDER BY Custom,vcFieldName
		
		--SELECT * FROM #tempSort
		DECLARE @DefaultSort AS NVARCHAR(MAX)
		DECLARE @DefaultSortFields AS NVARCHAR(MAX)
		DECLARE @DefaultSortJoin AS NVARCHAR(MAX)
		DECLARE @intCnt AS INT
		DECLARE @intTotalCnt AS INT	
		DECLARE @strColumnName AS VARCHAR(100)
		DECLARE @bitCustom AS BIT
		DECLARE @FieldID AS VARCHAR(100)				

		SET @DefaultSort = ''
		SET @DefaultSortFields = ''
		SET @DefaultSortJoin = ''		
		SET @intCnt = 0
		SET @intTotalCnt = ( SELECT COUNT(*) FROM #tempSort )
		SET @strColumnName = ''
		
		IF (SELECT ISNULL(bitEnableSecSorting,0) FROM EcommerceDtl WHERE numSiteID = @numSiteID ) = 1
		BEGIN
			CREATE TABLE #fldDefaultValues(RowNumber INT,Id VARCHAR(1000))

			WHILE(@intCnt < @intTotalCnt)
			BEGIN
				SET @intCnt = @intCnt + 1
				SELECT @FieldID = numFieldID, @strColumnName = vcDbColumnName, @bitCustom = Custom FROM #tempSort WHERE RowID = @intCnt
				
				--PRINT @FieldID
				--PRINT @strColumnName
				--PRINT @bitCustom

				IF @bitCustom = 0
					BEGIN
						SET @DefaultSort =  @strColumnName + ',' + @DefaultSort 
					END

				ELSE
					BEGIN
						DECLARE @fldID1 AS NUMERIC(18,0)
						DECLARE @RowNumber1 AS VARCHAR(MAX)
						DECLARE @vcSort1 AS VARCHAR(10)
						DECLARE @vcSortBy AS VARCHAR(1000)
						SET @vcSortBy = CONVERT(VARCHAR(100),@FieldID) + '~' + '0'
						--PRINT @vcSortBy

						INSERT INTO #fldDefaultValues(RowNumber,Id) SELECT RowNumber, id FROM dbo.SplitIDsWithPosition(@vcSortBy,'~')
						SELECT @RowNumber1 = RowNumber + 100 + @intCnt, @fldID1 = id FROM #fldDefaultValues WHERE RowNumber = 1
						SELECT @vcSort1 = 'ASC' 
						
						SET @DefaultSortJoin = @DefaultSortJoin + ' LEFT JOIN CFW_FLD_Values_Item CFVI' + @RowNumber1 + ' ON I.numItemCode = CFVI' + @RowNumber1 + '.RecID AND CFVI' + @RowNumber1 + '.fld_Id = ' + CONVERT(VARCHAR(10),@fldID1)
						SET @DefaultSort = '[fld_Value' + @RowNumber1 + '] ' + ',' + @DefaultSort 
						SET @DefaultSortFields = @DefaultSortFields + ', CFVI' + @RowNumber1 + '.fld_Value [fld_Value' + @RowNumber1 + ']'
					END

			END
			IF LEN(@DefaultSort) > 0
			BEGIN
				SET  @DefaultSort = LEFT(@DefaultSort,LEN(@DefaultSort) - 1)
				SET @SortString = @SortString + ',' 
			END
			--PRINT @SortString
			--PRINT @DefaultSort
			--PRINT @DefaultSortFields
		END
--        IF @SortBy = 1 
--            SET @SortString = ' vcItemName Asc '
--        ELSE IF @SortBy = 2 
--            SET @SortString = ' vcItemName Desc '
--        ELSE IF @SortBy = 3 
--            SET @SortString = ' monListPrice Asc '
--        ELSE IF @SortBy = 4 
--            SET @SortString = ' monListPrice Desc '
--        ELSE IF @SortBy = 5 
--            SET @SortString = ' bintCreatedDate Desc '
--        ELSE IF @SortBy = 6 
--            SET @SortString = ' bintCreatedDate ASC '	
--        ELSE IF @SortBy = 7 --sort by relevance, invokes only when search
--            BEGIN
--                 IF LEN(@SearchText) > 0 
--		            BEGIN
--						SET @SortString = ' SearchOrder ASC ';
--					    set @searchPositionColumn = ' ,SearchItems.RowNumber AS SearchOrder ' 						
--					END
--			END
--        ELSE 
--            SET @SortString = ' vcItemName Asc '
		
                                		
        IF LEN(@SearchText) > 0
			BEGIN
				PRINT 1
				IF CHARINDEX('SearchOrder',@SortString) > 0
				BEGIN
					SET @searchPositionColumn = ' ,SearchItems.RowNumber AS SearchOrder ' 
				END

				SET @Where = ' INNER JOIN dbo.SplitIDsWithPosition('''+ @SearchText +''','','') AS SearchItems 
                        ON SearchItems.Id = I.numItemCode  and SC.numSiteID = ' + CONVERT(VARCHAR(15), @numSiteID) + ' WHERE ISNULL(IsArchieve,0) = 0'
				END

		ELSE IF @SearchText = ''

			SET @Where = ' WHERE 1=2 '
			+ ' and SC.numSiteID = ' + CONVERT(VARCHAR(15), @numSiteID)
			+ ' AND ISNULL(IsArchieve,0) = 0'
		ELSE
			BEGIN
				PRINT 2
				SET @Where = ' WHERE 1=1 '
							+ ' and SC.numSiteID = ' + CONVERT(VARCHAR(15), @numSiteID)
							+ ' AND ISNULL(IsArchieve,0) = 0'


				PRINT @tintDisplayCategory							
				PRINT @Where

				IF @tintDisplayCategory = 1
				BEGIN
					
					;WITH CTE AS(
					SELECT C.numCategoryID,C.tintLevel,C.numDepCategory,CASE WHEN tintLevel=1 THEN 1 ELSE 0 END AS bitChild 
						FROM Category C WHERE (numCategoryID = @numCategoryID OR ISNULL(@numCategoryID,0) = 0) AND numDomainID = @numDomainID
					UNION ALL
					SELECT  C.numCategoryID,C.tintLevel,C.numDepCategory,CTE.bitChild FROM Category C JOIN CTE ON 
					C.numDepCategory=CTE.numCategoryID AND CTE.bitChild=1 AND numDomainID = @numDomainID)
					
					INSERT INTO #tmpItemCat(numCategoryID)												 
					SELECT DISTINCT C.numCategoryID FROM CTE C JOIN dbo.ItemCategory IC ON C.numCategoryID = IC.numCategoryID 
														WHERE numItemID IN ( 
																						
													 						SELECT numItemID FROM WareHouseItems WI
																			INNER JOIN dbo.Item I ON I.numItemCode = WI.numItemID AND I.numDomainID = @numDomainID
																			GROUP BY numItemID 
																			HAVING SUM(WI.numOnHand) > 0 
																			UNION ALL
													 						SELECT numItemID FROM WareHouseItems WI
																			INNER JOIN dbo.Item I ON I.numItemCode = WI.numItemID AND I.numDomainID = @numDomainID
																			GROUP BY numItemID 
																			HAVING SUM(WI.numAllocation) > 0
																		 )		
					SELECT numItemID INTO #tmpOnHandItems FROM 
					(													 					
						SELECT numItemID FROM WareHouseItems WI 
						INNER JOIN dbo.Item I ON I.numItemCode = WI.numItemID AND I.numDomainID = @numDomainID
						GROUP BY numItemID 
						HAVING SUM(WI.numOnHand) > 0
						UNION ALL
						SELECT numItemID FROM WareHouseItems WI 
						INNER JOIN dbo.Item I ON I.numItemCode = WI.numItemID AND I.numDomainID = @numDomainID
						GROUP BY numItemID 
						HAVING SUM(WI.numAllocation) > 0				
					) TABLE1
				END

			END
		
			--SELECT * FROM  #tmpOnHandItems
			--SELECT * FROM  #tmpItemCat
			
----		IF (SELECT COUNT(*) FROM #tmpItemCat) = 0 AND ISNULL(@numCategoryID,0) = 0
----		BEGIN
----			
----			INSERT INTO #tmpItemCat(numCategoryID)
----			SELECT DISTINCT C.numCategoryID FROM Category C
----			JOIN dbo.ItemCategory IC ON C.numCategoryID = IC.numCategoryID 
----							WHERE C.numDepCategory = (SELECT TOP 1 numCategoryID FROM dbo.Category 
----													  WHERE numDomainID = @numDomainID
----													  AND tintLevel = 1)
----							AND tintLevel <> 1
----							AND numDomainID = @numDomainID
----							AND numItemID IN (
----											 	SELECT numItemID FROM WareHouseItems WI
----												INNER JOIN dbo.Item I ON I.numItemCode = WI.numItemID
----												GROUP BY numItemID 
----												HAVING SUM(WI.numOnHand) > 0
----											 )
----			--SELECT * FROM #tmpItemCat								 
----		END
----		ELSE
----		BEGIN
----			INSERT INTO #tmpItemCat(numCategoryID)VALUES(@numCategoryID)
----		END

		       SET @strSQL = @strSQL + ' WITH Items AS 
	                                     ( 
	                                     SELECT   
                                               I.numItemCode,
                                               ROW_NUMBER() OVER ( Partition by I.numItemCode Order by I.numItemCode) RowID,
                                               vcItemName,
                                               I.bintCreatedDate,
                                               vcManufacturer,
                                               C.vcCategoryName,
                                               ISNULL(txtItemDesc,'''') txtItemDesc,
                                               ISNULL(CASE  WHEN I.[charItemType] = ''P''
															THEN ( UOM * ISNULL(W1.[monWListPrice], 0) )
															ELSE ( UOM * monListPrice )
													  END, 0) monListPrice,
                                               vcSKU,
                                               fltHeight,
                                               fltWidth,
                                               fltLength,
                                               IM.vcPathForImage,
                                               IM.vcPathForTImage,
                                               vcModelID,
                                               fltWeight,
                                               bitFreeShipping,
                                               UOM AS UOMConversionFactor,
											   UOMPurchase AS UOMPurchaseConversionFactor,
												( CASE WHEN charItemType <> ''S'' AND charItemType <> ''N''
													   THEN ( CASE WHEN bitShowInStock = 1
																   THEN ( CASE WHEN ( ISNULL(W.numOnHand,0)<=0) THEN CASE WHEN ' + CONVERT(VARCHAR(10),@numDomainID) + ' = 172 THEN ''<font color=red>ON HOLD</font>'' ELSE ''<font color=red>Out Of Stock</font>'' END
																			   WHEN bitAllowBackOrder = 1 THEN CASE WHEN ' + CONVERT(VARCHAR(10),@numDomainID) + ' = 172 THEN ''<font color=green>AVAILABLE</font>'' ELSE ''<font color=green>In Stock</font>'' END 
																			   ELSE CASE WHEN ' + CONVERT(VARCHAR(10),@numDomainID) + ' = 172 THEN ''<font color=green>AVAILABLE</font>'' ELSE ''<font color=green>In Stock</font>'' END  
																		  END )
																   ELSE ''''
															  END )
													   ELSE ''''
												  END ) AS InStock, C.vcDescription as CategoryDesc, ISNULL(W1.numWareHouseItemID,0) [numWareHouseItemID] ' 
								               +	@searchPositionColumn + @SortFields + @DefaultSortFields + '
								
                                         FROM      
                                                      Item AS I
                                         INNER JOIN   ItemCategory IC   ON   I.numItemCode = IC.numItemID
                                         LEFT JOIN    ItemImages IM     ON   I.numItemCode = IM.numItemCode AND IM.bitdefault=1 AND IM.bitIsImage = 1
                                         INNER JOIN   Category C        ON   IC.numCategoryID = C.numCategoryID
                                         INNER JOIN   SiteCategories SC ON   IC.numCategoryID = SC.numCategoryID
                                         INNER JOIN   eCommerceDTL E    ON   E.numDomainID = I.numDomainID AND E.numSiteId = SC.numSiteID ' + @Join + ' ' + @DefaultSortJoin + ' 
                                         OUTER APPLY (SELECT SUM(numOnHand) numOnHand FROM WareHouseItems W 
													  WHERE  W.numItemID = I.numItemCode
                                                      AND numWareHouseID IN (SELECT [OutParam] FROM dbo.[SplitString](''' + @vcWarehouseIDs + ''','',''))
                                                      AND W.numDomainID = ' + CONVERT(VARCHAR(10),@numDomainID) + ') AS W  
										 OUTER APPLY (SELECT TOP 1 numWareHouseItemID,monWListPrice FROM WareHouseItems W 
													  WHERE  W.numItemID = I.numItemCode 
													  AND W.numDomainID = ' + CONVERT(VARCHAR(10),@numDomainID) + '
													  AND numWareHouseID IN (SELECT [OutParam] FROM dbo.[SplitString](''' + @vcWarehouseIDs + ''','',''))) AS W1
										 OUTER APPLY (SELECT SUM(numAllocation) numAllocation FROM WareHouseItems W2 
													  WHERE  W2.numItemID = I.numItemCode
                                                      AND numWareHouseID IN (SELECT [OutParam] FROM dbo.[SplitString](''' + @vcWarehouseIDs + ''','',''))
                                                      AND W2.numDomainID = ' + CONVERT(VARCHAR(10),@numDomainID) + ') AS W2  			  
										 CROSS APPLY (SELECT dbo.fn_UOMConversion(ISNULL(I.numSaleUnit, 0),I.numItemCode, I.numDomainId,ISNULL(I.numBaseUnit, 0)) AS UOM) AS UOM
										 CROSS APPLY (SELECT dbo.fn_UOMConversion(ISNULL(I.numPurchaseUnit, 0),I.numItemCode, I.numDomainId,ISNULL(I.numBaseUnit, 0)) AS UOMPurchase) AS UOMPurchase '

			IF LEN(@FilterCustomFields) > 0 AND LEN(@FilterCustomWhere) > 0
			BEGIN
				SET @strSQL = @strSQL + ' INNER JOIN #tmpItemCode TP ON TP.numItemCode = I.numItemCode ' 
			END
			
			IF (SELECT COUNT(*) FROM #tmpItemCat) > 0
			BEGIN
				SET @strSQL = @strSQL + ' INNER JOIN #tmpItemCat TC ON TC.numCategoryID = IC.numCategoryID '
				SET @strSQL = @strSQL + ' LEFT JOIN #tmpOnHandItems TOIC ON TOIC.numItemID = I.numItemCode '
			END
			ELSE IF @numCategoryID>0
			BEGIN
				SET @Where = REPLACE(@Where,'WHERE',' WHERE IC.numCategoryID = ' + CONVERT(VARCHAR(10),@numCategoryID) + ' AND ')
			END 
			
			IF @tintDisplayCategory = 1
			BEGIN
				SET @Where = @Where + ' AND (ISNULL(W.numOnHand, 0) > 0 OR ISNULL(W2.numAllocation, 0) > 0) '
			END
			
			--PRINT @Where
			SET @strSQL = @strSQL + @Where
			
			IF LEN(@FilterRegularCondition) > 0
			BEGIN
				SET @strSQL = @strSQL + @FilterRegularCondition
			END                                         
			
---PRINT @SortString + ',' + @DefaultSort + ' ' + @vcSort 
	
            IF LEN(@whereNumItemCode) > 0
                                        BEGIN
								              SET @strSQL = @strSQL + @filterByNumItemCode	
								    	END
            SET @strSQL = @strSQL +     '   )
                                        , ItemSorted AS (SELECT  ROW_NUMBER() OVER ( ORDER BY  ' + @SortString + @DefaultSort + ' ' + @vcSort + ' ) AS Rownumber,* FROM  Items WHERE RowID = 1)'
            
            DECLARE @fldList AS NVARCHAR(MAX)
			SELECT @fldList=COALESCE(@fldList + ',','') + QUOTENAME(REPLACE(Fld_label,' ','') + '_C') FROM dbo.CFW_Fld_Master WHERE numDomainID=@numDomainID AND Grp_id=5
            
            SET @strSQL = @strSQL +' SELECT  Rownumber,numItemCode,txtItemDesc,vcManufacturer,monListPrice,vcItemName,vcCategoryName,vcPathForImage,vcpathForTImage,vcSKU,fltHeight,fltWidth,fltLength,vcModelID,fltWeight,bitFreeShipping,UOMConversionFactor,UOMPurchaseConversionFactor,InStock,CategoryDesc,numWareHouseItemID
                                     INTO #tempItem FROM ItemSorted ' 
--                                        + 'WHERE Rownumber > ' + CONVERT(VARCHAR(10), @firstRec) + '
--                                        AND   Rownumber < ' + CONVERT(VARCHAR(10), @lastRec) + '
--                                        order by Rownumber;' 
            
            SET @strSQL = @strSQL + ' SELECT * INTO #tmpPagedItems FROM #tempItem WHERE Rownumber > ' + CONVERT(VARCHAR(10), @firstRec) + '
                                      AND   Rownumber < ' + CONVERT(VARCHAR(10), @lastRec) + '
                                      order by Rownumber '
                                                                  
            IF LEN(ISNULL(@fldList,''))>0
            BEGIN
				PRINT 'flds1'
				SET @strSQL = @strSQL +'SELECT  Rownumber,numItemCode,txtItemDesc,vcManufacturer,monListPrice,vcItemName,vcCategoryName,vcPathForImage,vcpathForTImage,vcSKU,fltHeight,fltWidth,fltLength,vcModelID,fltWeight,bitFreeShipping,UOMConversionFactor,UOMPurchaseConversionFactor,InStock,CategoryDesc,numWareHouseItemID,' + @fldList + '
                                        FROM #tmpPagedItems I left join (
			SELECT  t2.RecId, REPLACE(t1.Fld_label,'' '','''') + ''_C'' as Fld_label, 
				CASE t1.fld_type WHEN ''SelectBox'' THEN isnull(dbo.fn_GetListItemName(t2.Fld_Value),'''')
								 WHEN ''CheckBox'' THEN isnull(dbo.fn_getCustomDataValue(t2.Fld_Value,9),'''')
								 WHEN ''DateField'' THEN dbo.FormatedDateFromDate(t2.Fld_Value,' + CAST(@numDomainId AS VARCHAR(18)) +')
								 ELSE Replace(ISNULL(t2.Fld_Value,''0''),''$'',''$$'') end AS Fld_Value FROM CFW_Fld_Master t1 
			JOIN CFW_FLD_Values_Item AS t2 ON t1.Fld_ID = t2.Fld_ID where t1.Grp_id = 5 and t1.numDomainID=' + CAST(@numDomainId AS VARCHAR(18)) +' AND t1.fld_type <> ''Link''
			AND t2.RecId IN (SELECT numItemCode FROM #tmpPagedItems )
			) p PIVOT (MAX([Fld_Value]) FOR Fld_label IN (' + @fldList + ')) AS pvt on I.numItemCode=pvt.RecId order by Rownumber;' 	
			END 
			ELSE
			BEGIN
				PRINT 'flds2'
				SET @strSQL = @strSQL +' SELECT  Rownumber,numItemCode,txtItemDesc,vcManufacturer,monListPrice,vcItemName,vcCategoryName,vcPathForImage,vcpathForTImage,vcSKU,fltHeight,fltWidth,fltLength,vcModelID,fltWeight,bitFreeShipping,UOMConversionFactor,UOMPurchaseConversionFactor,InStock,CategoryDesc,numWareHouseItemID
                                         FROM  #tmpPagedItems order by Rownumber;'
			END               
                                        
            
--            PRINT(@strSQL)        
--            EXEC ( @strSQL) ; 
			                           
            SET @strSQL = @strSQL + ' SELECT @TotRecs = COUNT(*) FROM #tempItem '                            
--             SET @strSQL = 'SELECT @TotRecs = COUNT(*) 
--                           FROM Item AS I
--                           INNER JOIN   ItemCategory IC ON I.numItemCode = IC.numItemID
--                           INNER JOIN   Category C ON IC.numCategoryID = C.numCategoryID
--                           INNER JOIN   SiteCategories SC ON IC.numCategoryID = SC.numCategoryID ' --+ @Where
--             
--			 IF LEN(@FilterCustomFields) > 0 AND LEN(@FilterCustomWhere) > 0
--			 BEGIN
--				 SET @strSQL = @strSQL + ' INNER JOIN #tmpItemCode TP ON TP.numItemCode = I.numItemCode ' 
--			 END
--			
--			 IF (SELECT COUNT(*) FROM #tmpItemCat) > 0
--			 BEGIN
--				SET @strSQL = @strSQL + ' INNER JOIN #tmpItemCat TC ON TC.numCategoryID = IC.numCategoryID '
--				SET @strSQL = @strSQL + ' INNER JOIN #tmpOnHandItems TOIC ON TOIC.numItemID = I.numItemCode '
--			 END
--			 ELSE IF @numCategoryID>0
--			 BEGIN
--				SET @Where = REPLACE(@Where,'WHERE',' WHERE IC.numCategoryID = ' + CONVERT(VARCHAR(10),@numCategoryID) + ' AND ')
--			 END 
--			
--			 SET @strSQL = @strSQL + @Where  
--			 
--			 IF LEN(@FilterRegularCondition) > 0
--			 BEGIN
--				SET @strSQL = @strSQL + @FilterRegularCondition
--			 END 
			
		--			--SELECT * FROM  #tempAvailableFields
		--SELECT * FROM  #tmpItemCode
		--SELECT * FROM  #tmpItemCat
		----SELECT * FROM  #fldValues
		--SELECT * FROM  #tempSort
		--SELECT * FROM  #fldDefaultValues
		--SELECT * FROM  #tmpOnHandItems
		----SELECT * FROM  #tmpPagedItems

			 PRINT  LEN(@strSQL)
			 DECLARE @tmpSQL AS VARCHAR(MAX)
			 SET @tmpSQL = @strSQL
			 PRINT  @tmpSQL
			 
             EXEC sp_executeSQL @strSQL, N'@TotRecs INT OUTPUT', @TotRecs OUTPUT
             
             
        DECLARE @tintOrder AS TINYINT                                                  
		DECLARE @vcFormFieldName AS VARCHAR(50)                                                  
		DECLARE @vcListItemType AS VARCHAR(1)                                             
		DECLARE @vcAssociatedControlType VARCHAR(10)                                                  
		DECLARE @numListID AS NUMERIC(9)                                                  
		DECLARE @WhereCondition VARCHAR(MAX)                       
		DECLARE @numFormFieldId AS NUMERIC  
		DECLARE @vcFieldType CHAR(1)
		                  
		SET @tintOrder=0                                                  
		SET @WhereCondition =''                 
                   
              
		CREATE TABLE #tempAvailableFields(numFormFieldId  NUMERIC(9),vcFormFieldName NVARCHAR(50),
		vcFieldType CHAR(1),vcAssociatedControlType NVARCHAR(50),numListID NUMERIC(18),
		vcListItemType CHAR(1),intRowNum INT,vcItemValue VARCHAR(3000))
		   
		INSERT INTO #tempAvailableFields (numFormFieldId,vcFormFieldName,vcFieldType,vcAssociatedControlType
				,numListID,vcListItemType,intRowNum)                         
		SELECT  Fld_id,REPLACE(Fld_label,' ','') AS vcFormFieldName,
			   CASE GRP_ID WHEN 4 then 'C' WHEN 1 THEN 'D' ELSE 'C' END AS vcFieldType,
				fld_type as vcAssociatedControlType,
				ISNULL(C.numListID, 0) numListID,
				CASE WHEN C.numListID > 0 THEN 'L'
					 ELSE ''
				END vcListItemType,ROW_NUMBER() OVER (ORDER BY Fld_id ASC) AS intRowNum
		FROM    CFW_Fld_Master C LEFT JOIN CFW_Validation V ON V.numFieldID = C.Fld_id
		WHERE   C.numDomainID = @numDomainId
				AND GRP_ID IN (5)

		  
		SELECT * FROM #tempAvailableFields
		
		/*
		DROP TABLE #tempAvailableFields
		DROP TABLE #tmpItemCode
		DROP TABLE #tmpItemCat
		*/	

		IF OBJECT_ID('tempdb..#tempAvailableFields') IS NOT NULL
		DROP TABLE #tempAvailableFields
		
		IF OBJECT_ID('tempdb..#tmpItemCode') IS NOT NULL
		DROP TABLE #tmpItemCode
		
		IF OBJECT_ID('tempdb..#tmpItemCat') IS NOT NULL
		DROP TABLE #tmpItemCat
		
		IF OBJECT_ID('tempdb..#fldValues') IS NOT NULL
		DROP TABLE #fldValues
		
		IF OBJECT_ID('tempdb..#tempSort') IS NOT NULL
		DROP TABLE #tempSort
		
		IF OBJECT_ID('tempdb..#fldDefaultValues') IS NOT NULL
		DROP TABLE #fldDefaultValues
		
		IF OBJECT_ID('tempdb..#tmpOnHandItems') IS NOT NULL
		DROP TABLE #tmpOnHandItems

		IF OBJECT_ID('tempdb..#tmpPagedItems') IS NOT NULL
		DROP TABLE #tmpPagedItems
    END


/****** Object:  StoredProcedure [dbo].[USP_LoadAttributesEcommerce]    Script Date: 07/26/2008 16:19:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec USP_LoadAttributes 14,66,'834,837'      
--created by anoop jayaraj      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_loadattributesecommerce')
DROP PROCEDURE usp_loadattributesecommerce
GO
CREATE PROCEDURE [dbo].[USP_LoadAttributesEcommerce]      
@numItemCode as numeric(9)=0,      
@numListID as numeric(9)=0,      
@strAtrr as varchar(1000)='',    
@numWareHouseID as numeric(9)=0      
as      
      
declare @bitSerialize as bit      
declare @ItemGroup as numeric(9)      
declare @strSQL as varchar(1000) 
DECLARE @chrItemType AS VARCHAR(5)
DECLARE @numDomainID AS NUMERIC(18,0)     
select @bitSerialize=bitSerialized,@ItemGroup=numItemgroup,@chrItemType=charItemType,@numDomainID=numDomainID from item where numItemCode=@numItemCode      
      
      
if @strAtrr!=''      
begin      
 Declare @Cnt int      
 Set @Cnt = 1      
 declare @SplitOn char(1)      
 set @SplitOn=','      
 set @strSQL='SELECT recid FROM CFW_Fld_Values_Serialized_Items where  bitSerialized ='+ convert(varchar(1),@bitSerialize)      
 While (Charindex(@SplitOn,@strAtrr)>0)      
 Begin      
  if  @Cnt=1 set @strSQL=@strSQL+' and fld_value=''' + (ltrim(rtrim(Substring(@strAtrr,1,Charindex(@SplitOn,@strAtrr)-1))))+''''      
  else set @strSQL=@strSQL+' or fld_value=''' + (ltrim(rtrim(Substring(@strAtrr,1,Charindex(@SplitOn,@strAtrr)-1))))+''''      
  Set @strAtrr = Substring(@strAtrr,Charindex(@SplitOn,@strAtrr)+1,len(@strAtrr))      
  Set @Cnt = @Cnt + 1      
 End      
 if @Cnt=1 set @strSQL=@strSQL+' and fld_value=''' + @strAtrr + ''' GROUP BY recid HAVING count(*) > '+convert(varchar(10), @Cnt-1)      
 else set @strSQL=@strSQL+' or fld_value=''' + @strAtrr + ''' GROUP BY recid HAVING count(*) > '+convert(varchar(10), @Cnt-1)      
end      
    
      
if @numItemCode>0      
begin      
 if @strAtrr=''      
	If @chrItemType <> 'P'
		select numListItemID,vcData from listdetails where numListID=@numListID AND numDomainID=@numDomainID
	ELSe
	  select numListItemID,vcData from listdetails where numlistitemid in(select distinct(fld_value) from WareHouseItems W      
	  join CFW_Fld_Values_Serialized_Items CSI on W.numWareHouseItemID=CSI.RecId      
	  join CFW_Fld_Master M on CSI.Fld_ID=M.Fld_ID                        
	  join ItemGroupsDTL on CSI.Fld_ID=ItemGroupsDTL.numOppAccAttrID      
	  where  M.numListID=@numListID and tintType=2 and CSI.bitSerialized=0 and numItemID=@numItemCode and W.numWareHouseID=@numWareHouseID)      
 else      
 begin      
	If @chrItemType <> 'P'
		select numListItemID,vcData from listdetails where numListID=@numListID AND numDomainID=@numDomainID
	ELSe
	BEGIN
  set @strSQL ='select numListItemID,vcData from listdetails where numlistitemid in(select distinct(fld_value) from WareHouseItems W      
  join CFW_Fld_Values_Serialized_Items CSI on W.numWareHouseItemID=CSI.RecId      
  join CFW_Fld_Master M on CSI.Fld_ID=M.Fld_ID      
  join ItemGroupsDTL on CSI.Fld_ID=ItemGroupsDTL.numOppAccAttrID     
  where  M.numListID='+convert(varchar(20),@numListID)+' and tintType=2 and CSI.bitSerialized=0 and numItemID='+convert(varchar(20),@numItemCode)+' and W.numWareHouseID='+convert(varchar(20),@numWareHouseID)+' and fld_value!=''0'' and fld_value!=''''     
 
  and W.numWareHouseItemID in ('+@strSQL+'))' 
  
  Print @strSQL       
  exec (@strSQL)      
  END
 end      
          
end         
else select 0
GO

GO
/****** Object:  StoredProcedure [dbo].[usp_ManageAddlContInfo]    Script Date: 04/02/2009 00:17:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Modified By Anoop jayaraj                  
 GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_manageaddlcontinfo')
DROP PROCEDURE usp_manageaddlcontinfo
GO
CREATE PROCEDURE [dbo].[usp_ManageAddlContInfo]                                    
 @numcontactId numeric=0,                                    
 @numContactType numeric=0,                                    
 @vcDepartment numeric(9)=0,                                    
 @vcCategory numeric(9)=0,                                    
 @vcGivenName varchar(100)='',                                    
 @vcFirstName varchar(50)='',                                    
 @vcLastName varchar(50)='',                                    
 @numDivisionId numeric,                                    
 @numPhone varchar(15)='',                                    
 @numPhoneExtension varchar(7)='',                                    
 @numCell varchar(15)='',                                    
 @NumHomePhone varchar(15)='',                                    
 @vcFax varchar(15)='',                                    
 @vcEmail varchar(50)='',                                    
 @VcAsstFirstName varchar(50)='',                                    
 @vcAsstLastName varchar(50)='',                                    
 @numAsstPhone varchar(15)='',                                    
 @numAsstExtn varchar(6)='',                                    
 @vcAsstEmail varchar(50)='',                                                      
 @charSex char(1)='',                                    
 @bintDOB datetime,                                    
 @vcPosition numeric(9)=0,                                                     
 @txtNotes text='',                                                     
 @numUserCntID numeric,                                                                                              
 @numDomainID numeric=1,                                                   
 @vcPStreet varchar(100)='',                                    
 @vcPCity varchar(50)='',                                    
 @vcPPostalCode varchar(15)='',                                    
 @vcPState numeric(9)=0,                                    
 @vcPCountry numeric(9)=0,                  
 @numManagerID numeric=0,        
 @numTeam as numeric(9)=0,        
 @numEmpStatus as numeric(9)=0,
 @vcTitle AS VARCHAR(100)='',
 @bitPrimaryContact AS BIT=0,
 @bitOptOut AS BIT=0,
 @vcLinkedinId VARCHAR(30)=NULL,
 @vcLinkedinUrl VARCHAR(300)=NULL
AS   


 SELECT @vcGivenName = @vcLastName + ', ' + @vcFirstName  + '.eml'                                 
   if @bintDOB ='Jan  1 1753 12:00AM' set  @bintDOB=null                      
IF @numContactId=0
     BEGIN   

IF @bitPrimaryContact = 1 
    BEGIN            
        DECLARE @numID AS NUMERIC(9)            
        DECLARE @bitPrimaryContactCheck AS BIT             
             
        SELECT TOP 1
                @numID = numContactID,
                @bitPrimaryContactCheck = ISNULL(bitPrimaryContact,0)
        FROM    AdditionalContactsInformation
        WHERE   numDivisionID = @numDivisionID 
                   
        IF @@rowcount = 0 
            SET @numID = 0             
        WHILE @numID > 0            
            BEGIN            
                IF @bitPrimaryContactCheck = 1 
                    UPDATE  AdditionalContactsInformation
                    SET     bitPrimaryContact = 0
                    WHERE   numContactID = @numID  
                              
                SELECT TOP 1
                        @numID = numContactID,
                        @bitPrimaryContactCheck = ISNULL(bitPrimaryContact,0)
                FROM    AdditionalContactsInformation
                WHERE   numDivisionID = @numDivisionID
                        AND numContactID > @numID    
                                
                IF @@rowcount = 0 
                    SET @numID = 0             
            END            
    END  
    ELSE IF @bitPrimaryContact = 0
    BEGIN
		IF (SELECT COUNT(*) FROM AdditionalContactsInformation WHERE numDivisionID = @numDivisionID AND numContactId!=@numContactId AND ISNULL(bitPrimaryContact,0)=1)=0
			SET @bitPrimaryContact = 1
    END                 
    
    
declare @bitAutoPopulateAddress bit 
declare @tintPoulateAddressTo tinyint 

select @bitAutoPopulateAddress= isnull(bitAutoPopulateAddress,'0'),@tintPoulateAddressTo=isnull(tintPoulateAddressTo,'0') from Domain where numDomainId = @numDomainId ;
if (@bitAutoPopulateAddress = 1)
	if(@tintPoulateAddressTo = 1) --if primary address is not specified then getting billing address    
	begin
		 
		  select                   
			@vcPStreet = AD1.vcStreet,
			@vcPCity= AD1.vcCity,
			@vcPState= AD1.numState,
			@vcPPostalCode= AD1.vcPostalCode,
			@vcPCountry= AD1.numCountry              
		  from divisionMaster  DM              
		   LEFT JOIN dbo.AddressDetails AD1 ON AD1.numDomainID=DM.numDomainID 
			AND AD1.numRecordID=DM.numDivisionID AND AD1.tintAddressOf=2 AND AD1.tintAddressType=1 AND AD1.bitIsPrimary=1
		  where numDivisionID=@numDivisionId    
	end  
	else if (@tintPoulateAddressTo = 2)-- Primary Address is Shipping Address
	begin
		  select                   
			@vcPStreet= AD2.vcStreet,
			@vcPCity=AD2.vcCity,
			@vcPState=AD2.numState,
			@vcPPostalCode=AD2.vcPostalCode,
			@vcPCountry=AD2.numCountry                   
		  from divisionMaster DM
		  LEFT JOIN dbo.AddressDetails AD2 ON AD2.numDomainID=DM.numDomainID 
		  AND AD2.numRecordID= DM.numDivisionID AND AD2.tintAddressOf=2 AND AD2.tintAddressType=2 AND AD2.bitIsPrimary=1
		  where numDivisionID=@numDivisionId    
	
	end                       
                       
 if (@numEmpStatus is null or @numEmpStatus=0) set @numEmpStatus=658        
 
    INSERT into AdditionalContactsInformation                                    
    (                  
   numContactType,                  
   vcDepartment,                  
   vcCategory,                  
   vcGivenName,                  
   vcFirstName,                  
   vcLastName,                   
   numDivisionId ,                  
   numPhone ,                  
   numPhoneExtension,                  
   numCell ,                  
   numHomePhone ,                          
   vcFax ,                  
   vcEmail ,                  
   VcAsstFirstName ,                  
   vcAsstLastName ,                  
   numAsstPhone ,                  
   numAsstExtn ,                  
   vcAsstEmail  ,                  
   charSex ,                  
   bintDOB ,                  
   vcPosition ,                  
   txtNotes ,                  
   numCreatedBy ,                                    
   bintCreatedDate,             
   numModifiedBy,            
   bintModifiedDate,                   
   numDomainID,                  
   numManagerID,                   
   numRecOwner,                  
   numTeam,                  
   numEmpStatus,
   vcTitle,bitPrimaryContact,bitOptOut,
   vcLinkedinId,vcLinkedinUrl                
   )                                    
    VALUES                                    
      (                  
   @numContactType,                  
   @vcDepartment,                  
   @vcCategory,                  
   @vcGivenName ,                  
   @vcFirstName ,                  
   @vcLastName,                   
   @numDivisionId ,                  
   @numPhone ,                                    
   @numPhoneExtension,                  
   @numCell ,                  
   @NumHomePhone ,                  
   @vcFax ,                  
   @vcEmail ,                  
   @VcAsstFirstName,                         
   @vcAsstLastName,                  
   @numAsstPhone,                  
   @numAsstExtn,                  
   @vcAsstEmail,                  
   @charSex,                  
   @bintDOB ,                  
   @vcPosition,                                   
   @txtNotes,                  
   @numUserCntID,                  
   getutcdate(),             
   @numUserCntID,                  
   getutcdate(),                   
   @numDomainID,                   
   @numManagerID,                   
   @numUserCntID,                   
   @numTeam,                  
   @numEmpStatus,
   @vcTitle,@bitPrimaryContact,@bitOptOut,
   @vcLinkedinId,@vcLinkedinUrl                  
   )                                    
                     
 set @numcontactId= @@IDENTITY                    

     INSERT INTO dbo.AddressDetails (
	 	vcAddressName,
	 	vcStreet,
	 	vcCity,
	 	vcPostalCode,
	 	numState,
	 	numCountry,
	 	bitIsPrimary,
	 	tintAddressOf,
	 	tintAddressType,
	 	numRecordID,
	 	numDomainID
	 ) VALUES ('Primary', @vcPStreet,@vcPCity,@vcPPostalCode,@vcPState,@vcPCountry,1,1,0,@numcontactId,@numDomainID)
                                                    
                               
 select @numcontactId                                                  
  END                                    
 ELSE if @numContactId>0                                                        
    BEGIN                                    
    UPDATE AdditionalContactsInformation SET                                    
      numContactType=@numContactType,                                    
      vcGivenName=@vcGivenName,                                    
      vcFirstName=@vcFirstName ,                                    
      vcLastName =@vcLastName ,                   
      numDivisionId =@numDivisionId ,                                    
      numPhone=@numPhone ,                                    
      numPhoneExtension=@numPhoneExtension,                                    
      numCell =@numCell ,                                    
      NumHomePhone =@NumHomePhone ,                                    
      vcFax=@vcFax ,                                    
      vcEmail=@vcEmail ,                                    
      VcAsstFirstName=@VcAsstFirstName,                                    
      vcAsstLastName=@vcAsstLastName,                                    
      numAsstPhone=@numAsstPhone,                                    
      numAsstExtn=@numAsstExtn,                                    
      vcAsstEmail=@vcAsstEmail,                                                      
      charSex=@charSex,                            
      bintDOB=@bintDOB ,                                    
      vcPosition=@vcPosition,                                                    
      txtNotes=@txtNotes,                                                     
      numModifiedBy=@numUserCntID,                                    
      bintModifiedDate=getutcdate(),                                    
      numManagerID=@numManagerID,
      bitPrimaryContact=@bitPrimaryContact                                    
     WHERE numContactId=@numContactId   
                    
     Update dbo.AddressDetails set                   
       vcStreet=@vcPStreet,                                    
       vcCity =@vcPCity,                                    
       vcPostalCode=@vcPPostalCode,                                     
       numState=@vcPState,                                    
       numCountry=@vcPCountry
	  where numRecordID=@numContactId AND bitIsPrimary=1 AND tintAddressOf=1 AND tintAddressType=0
 
     SELECT @numcontactId   
                                                
  END    
  ------Check the Email id Exist into the Email Master Table

DECLARE @numCount AS NUMERIC(18,0)
SET @numCount = 0
IF @vcEmail <> ''
BEGIN 
SELECT @numCount = COUNT(*) FROM dbo.EmailMaster WHERE vcEmailID =@vcEmail and numDomainId=@numDomainID
IF @numCount = 0 
BEGIN
	INSERT INTO EmailMaster(vcEmailId,vcName,numDomainId,numContactId)
	VALUES(@vcEmail,@vcFirstName,@numDomainID,@numcontactId)
END
else
begin
	update EmailMaster set numContactId=@numcontactId where vcEmailID =@vcEmail and numDomainId=@numDomainID
end
END 
--IF @vcAsstEmail <> ''
--BEGIN
--SELECT @numCount = COUNT(*) FROM dbo.EmailMaster WHERE vcEmailID =@vcAsstEmail
--IF @numCount = 0 
--BEGIN
--	INSERT INTO EmailMaster(vcEmailId,vcName,numDomainId,numContactId)
--	VALUES(@vcAsstEmail,@vcFirstName,@numDomainID,@numcontactId)
--END
--END
---------------------------------------------------------

/****** Object:  StoredProcedure [dbo].[USP_ManageCategory]    Script Date: 07/26/2008 16:19:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj        
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_managecategory')
DROP PROCEDURE usp_managecategory
GO
CREATE PROCEDURE [dbo].[USP_ManageCategory]        
@numCatergoryId as numeric(9),        
@vcCatgoryName as varchar(1000),       
@numDomainID as numeric(9)=0,
@vcDescription as VARCHAR(MAX),
@intDisplayOrder AS INT ,
@vcPathForCategoryImage AS VARCHAR(100),
@numDepCategory AS NUMERIC(9,0),
@numCategoryProfileID AS NUMERIC(18,0)
as 

	DECLARE @tintLevel AS TINYINT
	SET @tintLevel = 1

	IF	@numDepCategory <> 0
	BEGIN
		SELECT @tintLevel = (tintLevel + 1) FROM dbo.Category WHERE numCategoryID = @numDepCategory
	END       
       
	IF @numCatergoryId=0        
	BEGIN        
		INSERT INTO Category 
		(
			vcCategoryName,
			tintLevel,
			numDomainID,
			vcDescription,
			intDisplayOrder,
			vcPathForCategoryImage,
			numDepCategory,
			numCategoryProfileID
		)        
		VALUES
		(
			@vcCatgoryName,
			@tintLevel,
			@numDomainID,
			@vcDescription,
			@intDisplayOrder,
			@vcPathForCategoryImage,
			@numDepCategory,
			@numCategoryProfileID
		)        
	END        
	ELSE IF @numCatergoryId>0        
	BEGIN        
		UPDATE 
			Category 
		SET 
			vcCategoryName=@vcCatgoryName,
			vcDescription=@vcDescription,
			intDisplayOrder = @intDisplayOrder,
			tintLevel = @tintLevel,
			vcPathForCategoryImage = @vcPathForCategoryImage,
			numDepCategory = @numDepCategory      
		WHERE 
			numCategoryID=@numCatergoryId        
	END
GO
/****** Object:  StoredProcedure [dbo].[USP_ManageDivisions]    Script Date: 07/26/2008 16:19:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Modified by anoop jayaraj                                  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_managedivisions')
DROP PROCEDURE usp_managedivisions
GO
CREATE PROCEDURE [dbo].[USP_ManageDivisions]                                                        
 @numDivisionID  numeric=0,                                                         
 @numCompanyID  numeric=0,                                                         
 @vcDivisionName  varchar (100)='',                                                        
 @vcBillStreet  varchar (50)='',                                                        
 @vcBillCity   varchar (50)='',                                                        
 @vcBillState  NUMERIC=0,                                                        
 @vcBillPostCode  varchar (15)='',                                                        
 @vcBillCountry  numeric=0,                                                        
 @bitSameAddr  bit=0,                                                        
 @vcShipStreet  varchar (50)='',                                                        
 @vcShipCity   varchar (50)='',                                                        
 @vcShipState  varchar (50)='',                                                        
 @vcShipPostCode  varchar (15)='',                                                        
 @vcShipCountry  varchar (50)='',                                                        
 @numGrpId   numeric=0,                                                                                          
 @numTerID   numeric=0,                                                        
 @bitPublicFlag  bit=0,                                                        
 @tintCRMType  tinyint=0,                                                        
 @numUserCntID  numeric=0,                                                                                                                                                              
 @numDomainID  numeric=0,                                                        
 @bitLeadBoxFlg  bit=0,  --Added this because when called form leadbox this value will be 1 else 0                                                        
 @numStatusID  numeric=0,                                                      
 @numCampaignID numeric=0,                                          
 @numFollowUpStatus numeric(9)=0,                                                                                  
 @tintBillingTerms as tinyint,                                                        
 @numBillingDays as numeric(18,0),                                                       
 @tintInterestType as tinyint,                                                        
 @fltInterest as float,                                        
 @vcComFax as varchar(50)=0,                          
 @vcComPhone as varchar(50)=0,                
 @numAssignedTo as numeric(9)=0,    
 @bitNoTax as bit,
 @UpdateDefaultTax as bit=1,
@numCompanyDiff as numeric(9)=0,
@vcCompanyDiff as varchar(100)='',
@numCurrencyID	AS NUMERIC(9,0),
@numAccountClass AS NUMERIC(18,0) = 0                 
AS   

                                                     
 -- Added By Manish Anjara As On : 21st Jam,2013
 -- Check whether GroupID Or tintCRMType is not passed then we need to add record as prospect (means numGroupID = 0, tintCRMType = 1)
 IF ISNULL(@numGrpId,0) = 0 AND ISNULL(@tintCRMType,0) = 0
 BEGIN
 	SET @numGrpId = 0
 	SET @tintCRMType = 1
 END
                                                                                                               
 IF @numDivisionId is null OR @numDivisionId=0                                                 
 BEGIN    
 if @UpdateDefaultTax=1 set @bitNoTax=0
                                                                                  
    INSERT INTO DivisionMaster                                    
     (                      
      numCompanyID,                      
      vcDivisionName,                      
      numGrpId,                      
      numFollowUpStatus,               
      bitPublicFlag,                      
      numCreatedBy,                      
      bintCreatedDate,                                    
      numModifiedBy,                      
      bintModifiedDate,                      
      tintCRMType,                      
      numDomainID,                                    
      bitLeadBoxFlg,                      
      numTerID,                     
      numStatusID,                                    
      numRecOwner,                              
      tintBillingTerms,                                    
      numBillingDays,                      
      tintInterestType,                      
      fltInterest,                      
      numCampaignID,            
      vcComPhone,            
      vcComFax,    
   bitNoTax,numCompanyDiff,vcCompanyDiff,bitActiveInActive,numCurrencyID,numAccountClassID                
 )                                    
     VALUES                      
    (                      
    @numCompanyID,                       
    @vcDivisionName,                       
                      
    @numGrpId,                       
    @numFollowUpStatus,                               
    @bitPublicFlag,                       
    @numUserCntID,                       
    getutcdate(),                       
    @numUserCntID,                       
    getutcdate(),                       
    @tintCRMType,                       
    @numDomainID,                       
    @bitLeadBoxFlg,                       
    @numTerID,                       
    @numStatusID,                                                        
    @numUserCntID,                                
    0,                      
    @numBillingDays,                      
    0,                      
    0,                      
    @numCampaignID,            
    @vcComPhone,            
    @vcComFax,    
    @bitNoTax,@numCompanyDiff,@vcCompanyDiff,1,@numCurrencyID,@numAccountClass                          
  )                                  
	SELECT @numDivisionID = SCOPE_IDENTITY()
	  --added By Sachin Sadhu||Issued Founded on:1stFeb14||By:JJ-AudioJenex
	  IF	EXISTS(SELECT 'col1' FROM dbo.AddressDetails WHERE  bitIsPrimary=1 AND numDomainID=@numDomainID AND numRecordID=@numDivisionID )
         BEGIN
				INSERT INTO dbo.AddressDetails (
				vcAddressName,
				vcStreet,
				vcCity,
				vcPostalCode,
				numState,
				numCountry,
				bitIsPrimary,
				tintAddressOf,
				tintAddressType,
				numRecordID,
				numDomainID
				) 
				SELECT 'Address1',@vcBillStreet,@vcBillCity,@vcBillPostCode,@vcBillState,@vcBillCountry,0,2,1,@numDivisionID,@numDomainID
				union
				SELECT 'Address1',@vcShipStreet,@vcShipCity,@vcShipPostCode,@vcShipState,@vcShipCountry,0,2,2,@numDivisionID,@numDomainID
         END
         
     ELSE
			BEGIN
				INSERT INTO dbo.AddressDetails (
				vcAddressName,
				vcStreet,
				vcCity,
				vcPostalCode,
				numState,
				numCountry,
				bitIsPrimary,
				tintAddressOf,
				tintAddressType,
				numRecordID,
				numDomainID
				) 
				SELECT 'Address1',@vcBillStreet,@vcBillCity,@vcBillPostCode,@vcBillState,@vcBillCountry,1,2,1,@numDivisionID,@numDomainID
				union
				SELECT 'Address1',@vcShipStreet,@vcShipCity,@vcShipPostCode,@vcShipState,@vcShipCountry,1,2,2,@numDivisionID,@numDomainID
			END

       --End of Sachin Script                   
     



  if @UpdateDefaultTax=1 
  begin
   insert into DivisionTaxTypes
   select @numDivisionID,numTaxItemID,1 from TaxItems
   where numDomainID=@numDomainID
   union
   select @numDivisionID,0,1 
  end                                                


	                             
	DECLARE @numGroupID NUMERIC
	SELECT TOP 1 @numGroupID=numGroupID FROM dbo.AuthenticationGroupMaster WHERE numDomainID=@numDomainID AND tintGroupType=2
	IF @numGroupID IS NULL SET @numGroupID = 0 
	insert into ExtarnetAccounts (numCompanyID,numDivisionID,numGroupID,numDomainID)                                                
      values(@numCompanyID,@numDivisionID,@numGroupID,@numDomainID)                                                                      
                                                                          
     SELECT @numDivisionID                                                  
                                                   
 END                                                        
 ELSE if @numDivisionId>0                                                      
 BEGIN                                                                    
   --Inserting into Follow up History                                       
    declare @numFollow as varchar(10)                                        
    declare @binAdded as varchar(20)                                        
    declare @PreFollow as varchar(10)                                        
    declare @RowCount as varchar(2)                                        
    set @PreFollow=(select count(*)  numFollowUpStatus from FollowUpHistory where numDivisionID= @numDivisionID)                                        
    set @RowCount=@@rowcount                                        
    select   @numFollow=numFollowUpStatus,   @binAdded=bintModifiedDate from divisionmaster where numDivisionID=@numDivisionID                                         
    if @numFollow <>'0' and @numFollow <> @numFollowUpStatus                                        
     begin                                        
   select top 1  numFollowUpStatus from FollowUpHistory where numDivisionID= @numDivisionID order by numFollowUpStatusID desc                                        
                                         
  if @PreFollow<>0                                        
  begin                                        
                                         
   if @numFollow <> (select top 1  numFollowUpStatus from FollowUpHistory where numDivisionID= @numDivisionID order by numFollowUpStatusID desc)                                        
      begin                                        
                                          
            insert into FollowUpHistory(numFollowUpstatus,numDivisionID,bintAddedDate)                                        
                   values(@numFollow,@numDivisionID,@binAdded)                                        
          end                                        
  end                              
  else                                        
  begin                                        
                                         
  insert into FollowUpHistory(numFollowUpstatus,numDivisionID,bintAddedDate)                                        
                   values(@numFollow,@numDivisionID,@binAdded)                                   
  end                                        
                                     
  end                                        
    -----                                                      
                                                        
   UPDATE DivisionMaster                       
  SET                        
   numCompanyID = @numCompanyID ,                     
   vcDivisionName = @vcDivisionName,                                                        
     numGrpId = @numGrpId,                                                                       
     numFollowUpStatus =@numFollowUpStatus,                                                        
     numTerID = @numTerID,                                                   
     bitPublicFlag =@bitPublicFlag,                                                        
     numModifiedBy = @numUserCntID,                                                  
     bintModifiedDate = getutcdate(),                                                     
     tintCRMType = @tintCRMType,                                                      
     numStatusID = @numStatusID,                                                 
     tintBillingTerms = @tintBillingTerms,                                                      
     numBillingDays = @numBillingDays,                                                     
     tintInterestType = @tintInterestType,                                                       
     fltInterest =@fltInterest,                          
     vcComPhone=@vcComPhone,                          
     vcComFax= @vcComFax,                      
     numCampaignID=@numCampaignID,    
     bitNoTax=@bitNoTax,numCompanyDiff=@numCompanyDiff,vcCompanyDiff=@vcCompanyDiff,numCurrencyID = @numCurrencyID                                                                
   WHERE numDivisionID = @numDivisionID       

    Update dbo.AddressDetails set
       vcStreet=@vcBillStreet,                                    
       vcCity =@vcBillCity,                                    
       vcPostalCode=@vcBillPostCode,                                     
       numState=@vcBillState,                                    
       numCountry=@vcBillCountry
	  WHERE numDomainID=@numDomainID and numRecordID=@numDivisionID AND bitIsPrimary=1 AND tintAddressOf=2 AND tintAddressType=1         
	   Update dbo.AddressDetails set
       vcStreet=@vcShipStreet,                                    
       vcCity =@vcShipCity,                                    
       vcPostalCode=@vcShipPostCode,                                     
       numState=@vcShipState,                                    
       numCountry=@vcShipCountry
	  WHERE numDomainID=@numDomainID and numRecordID=@numDivisionID AND bitIsPrimary=1 AND tintAddressOf=2 AND tintAddressType=2         
	  
             
  end 

    ---Updating if organization is assigned to someone                
  declare @tempAssignedTo as numeric(9)              
  set @tempAssignedTo=null               
  select @tempAssignedTo=isnull(numAssignedTo,0) from DivisionMaster where numDivisionID = @numDivisionID                
print @tempAssignedTo              
  if (@tempAssignedTo<>@numAssignedTo and  @numAssignedTo<>'0')              
  begin                
    update DivisionMaster set numAssignedTo=@numAssignedTo ,numAssignedBy=@numUserCntID where numDivisionID = @numDivisionID                
  end               
  else if  (@numAssignedTo =0)              
  begin              
   update DivisionMaster set numAssignedTo=0 ,numAssignedBy=0 where numDivisionID = @numDivisionID              
                  
                
                                                                      
   SELECT @numDivisionID                                                   
 END
GO
GO
/****** Object:  StoredProcedure [dbo].[USP_ManageItemsAndKits]    Script Date: 02/15/2010 21:44:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Anoop Jayaraj                                                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_manageitemsandkits')
DROP PROCEDURE usp_manageitemsandkits
GO
CREATE PROCEDURE [dbo].[USP_ManageItemsAndKits]                                                                
@numItemCode as numeric(9) output,                                                                
@vcItemName as varchar(300),                                                                
@txtItemDesc as varchar(1000),                                                                
@charItemType as char(1),                                                                
@monListPrice as money,                                                                
@numItemClassification as numeric(9),                                                                
@bitTaxable as bit,                                                                
@vcSKU as varchar(50),                                                                                                                                      
@bitKitParent as bit,                                                                                   
--@dtDateEntered as datetime,                                                                
@numDomainID as numeric(9),                                                                
@numUserCntID as numeric(9),                                                                                                  
@numVendorID as numeric(9),                                                            
@bitSerialized as bit,                                               
@strFieldList as text,                                            
@vcModelID as varchar(200)='' ,                                            
@numItemGroup as numeric(9)=0,                                      
@numCOGSChartAcntId as numeric(9)=0,                                      
@numAssetChartAcntId as numeric(9)=0,                                      
@numIncomeChartAcntId as numeric(9)=0,                            
@monAverageCost as money,                          
@monLabourCost as money,                  
@fltWeight as float,                  
@fltHeight as float,                  
@fltLength as float,                  
@fltWidth as float,                  
@bitFreeshipping as bit,                
@bitAllowBackOrder as bit,              
@UnitofMeasure as varchar(30)='',              
@strChildItems as text,            
@bitShowDeptItem as bit,              
@bitShowDeptItemDesc as bit,        
@bitCalAmtBasedonDepItems as bit,    
@bitAssembly as BIT,
@intWebApiId INT=null,
@vcApiItemId AS VARCHAR(50)=null,
@numBarCodeId as VARCHAR(25)=null,
@vcManufacturer as varchar(250)=NULL,
@numBaseUnit AS NUMERIC(18)=0,
@numPurchaseUnit AS NUMERIC(18)=0,
@numSaleUnit AS NUMERIC(18)=0,
@bitLotNo as BIT,
@IsArchieve AS BIT,
@numItemClass as numeric(9)=0,
@tintStandardProductIDType AS TINYINT=0,
@vcExportToAPI VARCHAR(50),
@numShipClass NUMERIC,
@ProcedureCallFlag SMALLINT =0,
@vcCategories AS VARCHAR(2000) = '',
@bitAllowDropShip AS BIT=0,
@bitArchiveItem AS BIT = 0,
@bitAsset AS BIT = 0,
@bitRental AS BIT = 0,
@numCategoryProfileID AS NUMERIC(18,0) = 0
as                                                                
declare @hDoc as INT

if @numCOGSChartAcntId=0 set @numCOGSChartAcntId=null                      
if  @numAssetChartAcntId=0 set @numAssetChartAcntId=null  
if  @numIncomeChartAcntId=0 set @numIncomeChartAcntId=null     

DECLARE @ParentSKU VARCHAR(50)       
SET @ParentSKU = @vcSKU                                 
DECLARE @ItemID AS NUMERIC(9)
DECLARE @cnt AS INT

--if @dtDateEntered = 'Jan  1 1753 12:00:00:000AM' set @dtDateEntered=null 

IF ISNULL(@charItemType,'') = '' OR ISNULL(@charItemType,'') = '0'  SET @charItemType = 'N'

IF @intWebApiId = 2 
    AND LEN(ISNULL(@vcApiItemId, '')) > 0
    AND ISNULL(@numItemGroup, 0) > 0 
    BEGIN      
    
        -- check wether this id already Mapped in ITemAPI 
        SELECT  @cnt = COUNT([numItemID])
        FROM    [ItemAPI]
        WHERE   [WebApiId] = @intWebApiId
                AND [numDomainId] = @numDomainID
                AND [vcAPIItemID] = @vcApiItemId
        IF @cnt > 0 
            BEGIN
                SELECT  @ItemID = [numItemID]
                FROM    [ItemAPI]
                WHERE   [WebApiId] = @intWebApiId
                        AND [numDomainId] = @numDomainID
                        AND [vcAPIItemID] = @vcApiItemId
        --Update Existing Product coming from API
                SET @numItemCode = @ItemID
            END
    
    END

ELSE 
    IF @intWebApiId > 1 --AND  @intWebApiId <> 2
        AND LEN(ISNULL(@vcSKU, '')) > 0 
        BEGIN
            SET @ParentSKU = @vcSKU
    
       -- check wether this id already exist in Domain
       
            SELECT  @cnt = COUNT([numItemCode])
            FROM    [Item]
            WHERE   [numDomainId] = @numDomainID
                    AND ( vcSKU = @vcSKU
                          OR @vcSKU IN ( SELECT vcWHSKU
                                         FROM   dbo.WareHouseItems
                                         WHERE  numItemID = Item.[numItemCode]
                                                AND vcWHSKU = @vcSKU )
                        )
            IF @cnt > 0 
                BEGIN
                    SELECT  @ItemID = [numItemCode],
                            @ParentSKU = vcSKU
                    FROM    [Item]
                    WHERE   [numDomainId] = @numDomainID
                            AND ( vcSKU = @vcSKU
                                  OR @vcSKU IN (
                                  SELECT    vcWHSKU
                                  FROM      dbo.WareHouseItems
                                  WHERE     numItemID = Item.[numItemCode]
                                            AND vcWHSKU = @vcSKU )
                                )
                    SET @numItemCode = @ItemID
                END
            ELSE 
                BEGIN
			
    -- check wether this id already Mapped in ITemAPI 
                    SELECT  @cnt = COUNT([numItemID])
                    FROM    [ItemAPI]
                    WHERE   [WebApiId] = @intWebApiId
                            AND [numDomainId] = @numDomainID
                            AND [vcAPIItemID] = @vcApiItemId
                    IF @cnt > 0 
                        BEGIN
                            SELECT  @ItemID = [numItemID]
                            FROM    [ItemAPI]
                            WHERE   [WebApiId] = @intWebApiId
                                    AND [numDomainId] = @numDomainID
                                    AND [vcAPIItemID] = @vcApiItemId
        --Update Existing Product coming from API
                            SET @numItemCode = @ItemID
                        END
     
                END
        END
                                                         
if @numItemCode=0 or  @numItemCode is null
begin                                                                 
 insert into Item (                                             
  vcItemName,                                             
  txtItemDesc,                                             
  charItemType,                                             
  monListPrice,                                             
  numItemClassification,                                             
  bitTaxable,                                             
  vcSKU,                                             
  bitKitParent,                                             
  --dtDateEntered,                                              
  numVendorID,                                             
  numDomainID,                                             
  numCreatedBy,                                             
  bintCreatedDate,                                             
  bintModifiedDate,                                             
 numModifiedBy,                                              
  bitSerialized,                                     
  vcModelID,                                            
  numItemGroup,                                          
  numCOGsChartAcntId,                        
  numAssetChartAcntId,                                      
  numIncomeChartAcntId,                            
  monAverageCost,                          
  monCampaignLabourCost,                  
  fltWeight,                  
  fltHeight,                  
  fltWidth,                  
  fltLength,                  
  bitFreeShipping,                
  bitAllowBackOrder,              
  vcUnitofMeasure,            
  bitShowDeptItem,            
  bitShowDeptItemDesc,        
  bitCalAmtBasedonDepItems,    
  bitAssembly,
  numBarCodeId,
  vcManufacturer,numBaseUnit,numPurchaseUnit,numSaleUnit,bitLotNo,IsArchieve,numItemClass,tintStandardProductIDType,vcExportToAPI,numShipClass,
  bitAllowDropShip,bitArchiveItem,
  bitAsset,bitRental
   )                                                              
   values                                                                
    (                                                                
  @vcItemName,                                             
  @txtItemDesc,                                  
  @charItemType,                                             
  @monListPrice,                                             
  @numItemClassification,                                             
  @bitTaxable,                                             
  @ParentSKU,                       
  @bitKitParent,                                             
  --@dtDateEntered,                                             
  @numVendorID,                                             
  @numDomainID,                                             
  @numUserCntID,                                           
  getutcdate(),                                             
  getutcdate(),                                             
  @numUserCntID,                                             
  @bitSerialized,                                       
  @vcModelID,                                            
  @numItemGroup,                                      
  @numCOGsChartAcntId,                                      
  @numAssetChartAcntId,                                      
  @numIncomeChartAcntId,                            
  @monAverageCost,                          
  @monLabourCost,                  
  @fltWeight,                  
  @fltHeight,                  
  @fltWidth,                  
  @fltLength,                  
  @bitFreeshipping,                
  @bitAllowBackOrder,              
  @UnitofMeasure,            
  @bitShowDeptItem,            
  @bitShowDeptItemDesc,        
  @bitCalAmtBasedonDepItems,    
  @bitAssembly,                                                          
   @numBarCodeId,
@vcManufacturer,@numBaseUnit,@numPurchaseUnit,@numSaleUnit,@bitLotNo,@IsArchieve,@numItemClass,@tintStandardProductIDType,@vcExportToAPI,@numShipClass,
@bitAllowDropShip,@bitArchiveItem ,@bitAsset,@bitRental)                                             
 
 set @numItemCode = SCOPE_IDENTITY()
  
 IF  @intWebApiId > 1 
   BEGIN
     -- insert new product
     --insert this id into linking table
     INSERT INTO [ItemAPI] (
	 	[WebApiId],
	 	[numDomainId],
	 	[numItemID],
	 	[vcAPIItemID],
	 	[numCreatedby],
	 	[dtCreated],
	 	[numModifiedby],
	 	[dtModified],vcSKU
	 ) VALUES     (@intWebApiId,
                 @numDomainID,
                 @numItemCode,
                 @vcApiItemId,
	 			 @numUserCntID,
	 			 GETUTCDATE(),
	 			 @numUserCntID,
	 			 GETUTCDATE(),@vcSKU
	 			 ) 
   
	--update lastUpdated Date                  
	--UPDATE [WebAPIDetail] SET [vcThirteenthFldValue] = GETUTCDATE() WHERE [WebApiId] =@intWebApiId AND [numDomainId] = @numDomainID
   END
 
 
end         
else if    @numItemCode>0  AND @intWebApiId > 0 
BEGIN 
	IF @ProcedureCallFlag =1 
		BEGIN
		DECLARE @ExportToAPIList AS VARCHAR(30)
		SELECT @ExportToAPIList = vcExportToAPI FROM dbo.Item WHERE numItemCode = @numItemCode
		IF @ExportToAPIList != ''
			BEGIN
			IF NOT EXISTS(select * from (SELECT * FROM dbo.Split(@ExportToAPIList ,','))as A where items= @vcExportToAPI ) 
				BEGIN
					SET @vcExportToAPI = @ExportToAPIList + ',' + @vcExportToAPI
				End
			ELSE
				BEGIN
					SET @vcExportToAPI = @ExportToAPIList 
				End
			END
		--update ExportToAPI String value
		UPDATE dbo.Item SET vcExportToAPI=@vcExportToAPI where numItemCode=@numItemCode  AND numDomainID=@numDomainID
					
			SELECT @cnt = COUNT([numItemID]) FROM [ItemAPI] WHERE  [WebApiId] = @intWebApiId 
																AND [numDomainId] = @numDomainID
																AND [vcAPIItemID] = @vcApiItemId
																AND numItemID = @numItemCode
			IF @cnt > 0
			BEGIN
				--update lastUpdated Date 
				UPDATE [ItemAPI] SET [dtModified] =GETUTCDATE() WHERE [WebApiId] =@intWebApiId AND [numDomainId] = @numDomainID
			END
			ELSE
				--Insert ItemAPI mapping
				BEGIN
					INSERT INTO [ItemAPI] (
						[WebApiId],
						[numDomainId],
						[numItemID],
						[vcAPIItemID],
						[numCreatedby],
						[dtCreated],
						[numModifiedby],
						[dtModified],vcSKU
					) VALUES     (@intWebApiId,
						@numDomainID,
						@numItemCode,
						@vcApiItemId,
						@numUserCntID,
						GETUTCDATE(),
						@numUserCntID,
						GETUTCDATE(),@vcSKU
					)	
				END

		END   
END
                                                       
else if    @numItemCode>0  AND @intWebApiId <= 0                                                           
begin

	DECLARE @OldGroupID NUMERIC 
	SELECT @OldGroupID = ISNULL(numItemGroup,0) FROM [Item] WHERE numitemcode = @numItemCode AND [numDomainId] = @numDomainID
	

	DECLARE @monOldAverageCost AS DECIMAL(18,4);SET @monOldAverageCost=0
	SELECT @monOldAverageCost=ISNULL(monAverageCost,0) FROM Item WHERE numItemCode =@numItemCode AND [numDomainId] = @numDomainID
	   
 update item set vcItemName=@vcItemName,                           
  txtItemDesc=@txtItemDesc,                                             
  charItemType=@charItemType,                                             
  monListPrice= @monListPrice,
  numItemClassification=@numItemClassification,                                             
  bitTaxable=@bitTaxable,                                             
  vcSKU = (CASE WHEN ISNULL(@ParentSKU,'') = '' THEN @vcSKU ELSE @ParentSKU END),                                             
  bitKitParent=@bitKitParent,                                             
  --dtDateEntered=@dtDateEntered,                                             
  numVendorID=@numVendorID,                                             
  numDomainID=@numDomainID,                                             
  bintModifiedDate=getutcdate(),                                             
  numModifiedBy=@numUserCntID,                                   
  bitSerialized=@bitSerialized,                                                         
  vcModelID=@vcModelID,                                            
  numItemGroup=@numItemGroup,                                      
  numCOGsChartAcntId=@numCOGsChartAcntId,                                      
  numAssetChartAcntId=@numAssetChartAcntId,                                      
  numIncomeChartAcntId=@numIncomeChartAcntId,                            
  --monAverageCost=@monAverageCost,                          
  monCampaignLabourCost=@monLabourCost,                
  fltWeight=@fltWeight,                  
  fltHeight=@fltHeight,                  
  fltWidth=@fltWidth,                  
  fltLength=@fltLength,                  
  bitFreeShipping=@bitFreeshipping,                
  bitAllowBackOrder=@bitAllowBackOrder,              
  vcUnitofMeasure=@UnitofMeasure,            
  bitShowDeptItem=@bitShowDeptItem,            
  bitShowDeptItemDesc=@bitShowDeptItemDesc,        
  bitCalAmtBasedonDepItems=@bitCalAmtBasedonDepItems,    
  bitAssembly=@bitAssembly ,
numBarCodeId=@numBarCodeId,
vcManufacturer=@vcManufacturer,numBaseUnit=@numBaseUnit,numPurchaseUnit=@numPurchaseUnit,numSaleUnit=@numSaleUnit,bitLotNo=@bitLotNo,
IsArchieve = @IsArchieve,numItemClass=@numItemClass,tintStandardProductIDType=@tintStandardProductIDType,vcExportToAPI=@vcExportToAPI,
numShipClass=@numShipClass,bitAllowDropShip=@bitAllowDropShip, bitArchiveItem = @bitArchiveItem,bitAsset=@bitAsset,bitRental=@bitRental
where numItemCode=@numItemCode  AND numDomainID=@numDomainID

IF NOT EXISTS(SELECT numItemCode FROM dbo.OpportunityItems WHERE numItemCode = @numItemCode)
	BEGIN
		IF (((SELECT ISNULL(SUM(numOnHand),0) FROM WareHouseItems WHERE numItemID = @numItemCode AND numDomainId=@numDomainID) = 0) AND ((SELECT ISNULL(monAverageCost,0) FROM Item  WHERE numItemCode = @numItemCode AND numDomainId=@numDomainID ) = 0))
		BEGIN
			UPDATE Item SET monAverageCost=@monAverageCost  where numItemCode=@numItemCode  AND numDomainID=@numDomainID	
		END
		--UPDATE Item SET monAverageCost=@monAverageCost  where numItemCode=@numItemCode  AND numDomainID=@numDomainID	
	END
	
 --If Average Cost changed then add in Tracking
	IF ISNULL(@monAverageCost,0) != ISNULL(@monOldAverageCost,0)
	BEGIN
		DECLARE @numWareHouseItemID NUMERIC(9);SET @numWareHouseItemID=0
		DECLARE @numTotal int;SET @numTotal=0
		DECLARE @i int;SET @i=0
		DECLARE @vcDescription AS VARCHAR(100);SET @vcDescription=''
		
	    SELECT @numTotal=COUNT(*) FROM WareHouseItems where numItemID=@numItemCode AND numDomainId=@numDomainId
	     
		WHILE @i < @numTotal
		BEGIN
			SELECT @numWareHouseItemID = min(numWareHouseItemID) FROM WareHouseItems 
				WHERE numItemID=@numItemCode AND numDomainId=@numDomainId and numWareHouseItemID > @numWareHouseItemID 
				
			IF @numWareHouseItemID>0
			BEGIN
				SET @vcDescription='Average Cost Changed Manually OLD : ' + CAST(ISNULL(@monOldAverageCost,0) AS VARCHAR(20))
				
				DECLARE @numDomain AS NUMERIC(18,0)
				SET @numDomain = @numDomainID
				EXEC dbo.USP_ManageWareHouseItems_Tracking
					@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)		
					@numReferenceID = @numItemCode, --  numeric(9, 0)
					@tintRefType = 1, --  tinyint
					@vcDescription = @vcDescription, --  varchar(100)
					@tintMode = 0, --  tinyint
					@numModifiedBy = @numUserCntID, --  numeric(9, 0)
					@numDomainID = @numDomain
		    END
		    
		    SET @i = @i + 1
		END
	END
 
 DECLARE @bitCartFreeShipping as  bit 
 SELECT @bitCartFreeShipping = bitFreeShipping FROM dbo.CartItems where numItemCode = @numItemCode  AND numDomainID = @numDomainID 
 
 IF @bitCartFreeShipping <> @bitFreeshipping
 BEGIN
    UPDATE dbo.CartItems SET bitFreeShipping = @bitFreeshipping WHERE numItemCode = @numItemCode  AND numDomainID=@numDomainID
 END 
 
 
 IF  @intWebApiId > 1 
   BEGIN
      SELECT @cnt = COUNT([numItemID])
    FROM   [ItemAPI]
    WHERE  [WebApiId] = @intWebApiId
           AND [numDomainId] = @numDomainID
           AND [vcAPIItemID] = @vcApiItemId
    IF @cnt > 0
      BEGIN
      --update lastUpdated Date 
      UPDATE [ItemAPI] SET [dtModified] =GETUTCDATE() WHERE [WebApiId] =@intWebApiId AND [numDomainId] = @numDomainID
      END
      ELSE
      --Insert ItemAPI mapping
      BEGIN
        INSERT INTO [ItemAPI] (
	 	[WebApiId],
	 	[numDomainId],
	 	[numItemID],
	 	[vcAPIItemID],
	 	[numCreatedby],
	 	[dtCreated],
	 	[numModifiedby],
	 	[dtModified],vcSKU
	 ) VALUES     (@intWebApiId,
                 @numDomainID,
                 @numItemCode,
                 @vcApiItemId,
	 			 @numUserCntID,
	 			 GETUTCDATE(),
	 			 @numUserCntID,
	 			 GETUTCDATE(),@vcSKU
	 			 ) 
      END
   
   END                                                                               
-- kishan It is necessary to add item into itemTax table . 
 IF	@bitTaxable = 1
	BEGIN
		IF NOT EXISTS(SELECT * FROM dbo.ItemTax WHERE numItemCode = @numItemCode AND numTaxItemID = 0)
			BEGIN
			    	INSERT INTO dbo.ItemTax (
						numItemCode,
						numTaxItemID,
						bitApplicable
					) VALUES ( 
						/* numItemCode - numeric(18, 0) */ @numItemCode,
						/* numTaxItemID - numeric(18, 0)*/ 0,
						/* bitApplicable - bit */ 1 ) 						
			END
	END	 
      
IF @charItemType='S' or @charItemType='N'                                                                       
BEGIN
	delete from WareHouseItmsDTL where numWareHouseItemID in (select numWareHouseItemID from WareHouseItems where numItemID=@numItemCode AND [WareHouseItems].[numDomainID] = @numDomainID)
	delete from WareHouseItems where numItemID=@numItemCode AND [WareHouseItems].[numDomainID] = @numDomainID
END                                      
   
    if @bitKitParent=1  OR @bitAssembly = 1              
    begin              
  declare @hDoc1 as int                                            
  EXEC sp_xml_preparedocument @hDoc1 OUTPUT, @strChildItems                                                                        
                                                                                                 
                                                                
   update ItemDetails set                                                                         
    numQtyItemsReq=X.QtyItemsReq,numWareHouseItemId=X.numWarehouseItmsID,numUOMId=X.numUOMId,vcItemDesc=X.vcItemDesc,sintOrder=X.sintOrder                                                                                          
     From (SELECT QtyItemsReq,ItemKitID,ChildItemID,numWarehouseItmsID,numUOMId,vcItemDesc,sintOrder, numItemDetailID As ItemDetailID                             
    FROM OPENXML(@hDoc1,'/NewDataSet/Table1',2)                                                                         
     with(ItemKitID numeric(9),                                                          
     ChildItemID numeric(9),                                                                        
     QtyItemsReq numeric(9),  
     numWarehouseItmsID numeric(9),numUOMId NUMERIC(9),vcItemDesc VARCHAR(1000),sintOrder int, numItemDetailID NUMERIC(18,0)))X                                                                         
   where  numItemKitID=X.ItemKitID and numChildItemID=ChildItemID and numItemDetailID = X.ItemDetailID               
              
 end   
 ELSE
 BEGIN
 	DELETE FROM ItemDetails WHERE numItemKitID=@numItemCode
 END           
                                                               
end

declare  @rows as integer                                        
 EXEC sp_xml_preparedocument @hDoc OUTPUT, @strFieldList                                                                      
                                         
 SELECT @rows=count(*) FROM OPENXML (@hDoc,'/NewDataSet/CusFlds',2)                                                  
  WITH (Fld_ID numeric(9),Fld_Value varchar(100),RecId numeric(9))                                        
                                                                           
                                          
 if @rows>0                                        
 begin                                        
  Create table #tempTable ( ID INT IDENTITY PRIMARY KEY,                    
  Fld_ID numeric(9),                                        
  Fld_Value varchar(100),                                        
  RecId numeric(9),                                    
  bitSItems bit                                                                         
  )                                         
  insert into #tempTable (Fld_ID,Fld_Value,RecId,bitSItems)                                        
  SELECT * FROM OPENXML (@hDoc,'/NewDataSet/CusFlds',2)                                                  
  WITH (Fld_ID numeric(9),Fld_Value varchar(100),RecId numeric(9),bitSItems bit)                                           
                                         
  delete CFW_Fld_Values_Serialized_Items from CFW_Fld_Values_Serialized_Items C                                        
  inner join #tempTable T                          
  on   C.Fld_ID=T.Fld_ID and C.RecId=T.RecId and bitSerialized=bitSItems                                         
                                         
  drop table #tempTable                                        
                                                            
  insert into CFW_Fld_Values_Serialized_Items(Fld_ID,Fld_Value,RecId,bitSerialized)                                                  
  select Fld_ID,isnull(Fld_Value,'') as Fld_Value,RecId,bitSItems from(SELECT * FROM OPENXML (@hDoc,'/NewDataSet/CusFlds',2)                                                  
  WITH (Fld_ID numeric(9),Fld_Value varchar(100),RecId numeric(9),bitSItems bit)) X                                         
 end  

IF @numItemCode > 0
BEGIN
	DELETE 
		IC
	FROM 
		dbo.ItemCategory IC
	INNER JOIN 
		Category 
	ON 
		IC.numCategoryID = Category.numCategoryID
	WHERE 
		numItemID = @numItemCode		
		AND numCategoryProfileID = @numCategoryProfileID

	IF @vcCategories <> ''	
	BEGIN
        INSERT INTO ItemCategory( numItemID , numCategoryID )  SELECT  @numItemCode AS numItemID,ID from dbo.SplitIDs(@vcCategories,',')	
	END
END

 EXEC sp_xml_removedocument @hDoc              
                                          
/****** Object:  StoredProcedure [USP_ManageReturnHeaderItems]    ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ManageReturnHeaderItems' ) 
    DROP PROCEDURE USP_ManageReturnHeaderItems
GO
CREATE PROCEDURE [USP_ManageReturnHeaderItems]
    (
      @numReturnHeaderID NUMERIC(18, 0) OUTPUT,
      @vcRMA VARCHAR(100),
      @vcBizDocName VARCHAR(100),
      @numDomainId NUMERIC(18, 0),
      @numUserCntID AS NUMERIC(9) = 0,
      @numDivisionId NUMERIC(18, 0),
      @numContactId NUMERIC(18, 0),
      @numOppId NUMERIC(18, 0),
      @tintReturnType TINYINT,
      @numReturnReason NUMERIC(18, 0),
      @numReturnStatus NUMERIC(18, 0),
      @monAmount MONEY,
      @monTotalTax MONEY,
      @monTotalDiscount MONEY,
      @tintReceiveType TINYINT,
      @vcComments TEXT,
      @strItems TEXT = '',
      @tintMode TINYINT = 0,
      @numBillAddressId NUMERIC(18, 0),
      @numShipAddressId NUMERIC(18, 0),
      @numDepositIDRef NUMERIC(18,0),
      @numBillPaymentIDRef NUMERIC(18,0),
	  @numParentID NUMERIC(18,0) = 0
    )
AS 
BEGIN 
    DECLARE @hDocItem INT                                                                                                                                                                
 
    IF @numReturnHeaderID = 0 
    BEGIN             
		-- GET ACCOUNT CLASS IF ENABLED
		DECLARE @numAccountClass AS NUMERIC(18) = 0

		DECLARE @tintDefaultClassType AS INT = 0
		SELECT @tintDefaultClassType = ISNULL(tintDefaultClassType,0) FROM Domain WHERE numDomainId = @numDomainID 

		IF @tintDefaultClassType = 1 --USER
		BEGIN
			SELECT 
				@numAccountClass=ISNULL(numDefaultClass,0) 
			FROM 
				dbo.UserMaster UM 
			JOIN 
				dbo.Domain D 
			ON 
				UM.numDomainID=D.numDomainId
			WHERE 
				D.numDomainId=@numDomainId 
				AND UM.numUserDetailId=@numUserCntID
		END
		ELSE IF @tintDefaultClassType = 2 --COMPANY
		BEGIN
			SELECT 
				@numAccountClass=ISNULL(numAccountClassID,0) 
			FROM 
				dbo.DivisionMaster DM 
			WHERE 
				DM.numDomainId=@numDomainId 
				AND DM.numDivisionID=@numDivisionID
		END
		ELSE
		BEGIN
			SET @numAccountClass = 0
		END
                   
        INSERT  INTO [ReturnHeader]
		(
			[vcRMA],[vcBizDocName],[numDomainId],[numDivisionId],[numContactId],
            [numOppId],[tintReturnType],[numReturnReason],[numReturnStatus],[monAmount],
            [monTotalTax],[monTotalDiscount],[tintReceiveType],[vcComments],
            [numCreatedBy],[dtCreatedDate],monBizDocAmount,monBizDocUsedAmount,numDepositIDRef,
			numBillPaymentIDRef,numAccountClass,numParentID,IsUnappliedPayment
		)
        SELECT  
			@vcRMA,@vcBizDocName,@numDomainId,@numDivisionId,@numContactId,@numOppId,
            @tintReturnType,@numReturnReason,@numReturnStatus,@monAmount,@monTotalTax,
			@monTotalDiscount,@tintReceiveType,@vcComments,@numUserCntID,GETUTCDATE(),0,0,
			@numDepositIDRef,@numBillPaymentIDRef,@numAccountClass,@numParentID,
			(CASE WHEN (SELECT [DM].[tintDepositePage] FROM [dbo].[DepositMaster] AS DM 
						WHERE DM.[numDepositId] = @numDepositIDRef AND [DM].[numDomainId] = @numDomainId) = 2 THEN 1
					ELSE 0
			END)
                    
        SET @numReturnHeaderID = SCOPE_IDENTITY()                                        
        SELECT  @numReturnHeaderID

		--Update DepositMaster if Refund UnApplied Payment
		IF @numDepositIDRef>0 AND @tintReturnType=4
		BEGIN
			UPDATE dbo.DepositMaster SET monRefundAmount=@monAmount WHERE numDepositId=@numDepositIDRef
		END
						
		--Update BillPaymentHeader if Refund UnApplied Payment
		IF @numBillPaymentIDRef>0 AND @tintReturnType=4
		BEGIN
			UPDATE dbo.BillPaymentHeader SET monRefundAmount=@monAmount WHERE numBillPaymentID=@numBillPaymentIDRef
		END
						
        DECLARE @tintType TINYINT 
        SET @tintType=CASE When @tintReturnType=1 OR @tintReturnType=2 THEN 7 
								WHEN @tintReturnType=3 THEN 6
								WHEN @tintReturnType=4 THEN 5 END
												
        EXEC dbo.USP_UpdateBizDocNameTemplate
				@tintType =  @tintType, --  tinyint
				@numDomainID = @numDomainID, --  numeric(18, 0)
				@RecordID = @numReturnHeaderID --  numeric(18, 0)

        DECLARE @numRMATempID AS NUMERIC(18, 0)
        DECLARE @numBizdocTempID AS NUMERIC(18, 0)

        IF @tintReturnType=1 
        BEGIN
			SELECT TOP 1 @numRMATempID = numBizDocTempID FROM BizDocTemplate
			WHERE   numOppType = 1 AND bitDefault = 1 AND bitEnabled = 1
                AND numDomainID = @numDomainID AND numBizDocID IN ( SELECT TOP 1 numListItemID 
						FROM   dbo.ListDetails WHERE  vcData = 'RMA' AND constFlag = 1 )							
		END
		ELSE IF @tintReturnType=2
        BEGIN
			SELECT TOP 1 @numRMATempID = numBizDocTempID FROM BizDocTemplate
			WHERE   numOppType = 2 AND bitDefault = 1 AND bitEnabled = 1
                AND numDomainID = @numDomainID AND numBizDocID IN ( SELECT TOP 1 numListItemID 
						FROM   dbo.ListDetails WHERE  vcData = 'RMA' AND constFlag = 1 )	
		END			    
        ELSE IF @tintReturnType=3
        BEGIN
			SELECT TOP 1 @numRMATempID = numBizDocTempID FROM    BizDocTemplate
			WHERE   numOppType = 1 AND bitDefault = 1 AND bitEnabled = 1
                AND numDomainID = @numDomainID AND numBizDocID IN ( SELECT TOP 1 numListItemID 
                FROM   dbo.ListDetails WHERE  vcData = 'Credit Memo' AND constFlag = 1 )							
		END
		ELSE IF @tintReturnType=4
        BEGIN
			SELECT TOP 1 @numRMATempID = numBizDocTempID FROM    BizDocTemplate
			WHERE   numOppType = 1 AND bitDefault = 1 AND bitEnabled = 1
                AND numDomainID = @numDomainID AND numBizDocID IN ( SELECT TOP 1 numListItemID 
                FROM   dbo.ListDetails WHERE  vcData = 'Refund Receipt' AND constFlag = 1 )							
                                
            SET @numBizdocTempID=@numRMATempID    
		END
                                               
        UPDATE  dbo.ReturnHeader
        SET     numRMATempID = ISNULL(@numRMATempID,0),
                numBizdocTempID = ISNULL(@numBizdocTempID,0)
        WHERE   numReturnHeaderID = @numReturnHeaderID
			                
		--Add/Update Address Details
        SET @numOppID = NULLIF(@numOppID, 0)
		            
        DECLARE @vcStreet VARCHAR(100),
            @vcCity VARCHAR(50),
            @vcPostalCode VARCHAR(15),
            @vcCompanyName VARCHAR(100),
            @vcAddressName VARCHAR(50)      
        DECLARE @numState NUMERIC(9),
            @numCountry NUMERIC(9),
            @numCompanyId NUMERIC(9) 
        DECLARE @bitIsPrimary BIT ;

        SELECT  @vcCompanyName = vcCompanyName,
                @numCompanyId = div.numCompanyID
        FROM    CompanyInfo Com
                JOIN divisionMaster Div ON div.numCompanyID = com.numCompanyID
        WHERE   div.numdivisionID = @numDivisionId

		--Bill Address
        IF @numBillAddressId > 0 
        BEGIN
            SELECT  @vcStreet = ISNULL(vcStreet, ''),
                    @vcCity = ISNULL(vcCity, ''),
                    @vcPostalCode = ISNULL(vcPostalCode, ''),
                    @numState = ISNULL(numState, 0),
                    @numCountry = ISNULL(numCountry, 0),
                    @bitIsPrimary = bitIsPrimary,
                    @vcAddressName = vcAddressName
            FROM    dbo.AddressDetails
            WHERE   numDomainID = @numDomainID
                    AND numAddressID = @numBillAddressId
            EXEC dbo.USP_UpdateOppAddress @numOppID = @numOppID, --  numeric(9, 0)
                @byteMode = 0, --  tinyint
                @vcStreet = @vcStreet, --  varchar(100)
                @vcCity = @vcCity, --  varchar(50)
                @vcPostalCode = @vcPostalCode, --  varchar(15)
                @numState = @numState, --  numeric(9, 0)
                @numCountry = @numCountry, --  numeric(9, 0)
                @vcCompanyName = @vcCompanyName, --  varchar(100)
                @numCompanyId = 0, --  numeric(9, 0)
                @vcAddressName = @vcAddressName,
                @numReturnHeaderID = @numReturnHeaderID
        END
    
		--Ship Address
        IF @numShipAddressId > 0 
        BEGIN
            SELECT  @vcStreet = ISNULL(vcStreet, ''),
                    @vcCity = ISNULL(vcCity, ''),
                    @vcPostalCode = ISNULL(vcPostalCode, ''),
                    @numState = ISNULL(numState, 0),
                    @numCountry = ISNULL(numCountry, 0),
                    @bitIsPrimary = bitIsPrimary,
                    @vcAddressName = vcAddressName
            FROM    dbo.AddressDetails
            WHERE   numDomainID = @numDomainID
                    AND numAddressID = @numShipAddressId
 
            EXEC dbo.USP_UpdateOppAddress @numOppID = @numOppID, --  numeric(9, 0)
                @byteMode = 1, --  tinyint
                @vcStreet = @vcStreet, --  varchar(100)
                @vcCity = @vcCity, --  varchar(50)
                @vcPostalCode = @vcPostalCode, --  varchar(15)
                @numState = @numState, --  numeric(9, 0)
                @numCountry = @numCountry, --  numeric(9, 0)
                @vcCompanyName = @vcCompanyName, --  varchar(100)
                @numCompanyId = @numCompanyId, --  numeric(9, 0)
                @vcAddressName = @vcAddressName,
                @numReturnHeaderID = @numReturnHeaderID
        END
            
		IF @tintReturnType=1 OR (@tintReturnType=2 AND (SELECT ISNULL(bitPurchaseTaxCredit,0) FROM Domain WHERE numDomainId=@numDomainId) = 1) 
		BEGIN    
			IF @numOppId > 0
			BEGIN
				INSERT dbo.OpportunityMasterTaxItems 
				(
					numReturnHeaderID,
					numTaxItemID,
					fltPercentage,
					tintTaxType,
					numTaxID
				) 
				SELECT 
					@numReturnHeaderID,
					numTaxItemID,
					fltPercentage,
					tintTaxType,
					numTaxID
				FROM 
					OpportunityMasterTaxItems 
				WHERE 
					numOppID=@numOppID
			END
			ELSE
			BEGIN 
				--Insert Tax for Division                       
				INSERT dbo.OpportunityMasterTaxItems (
					numReturnHeaderID,
					numTaxItemID,
					fltPercentage,
					tintTaxType,
					numTaxID
				) 
				SELECT 
					@numReturnHeaderID,
					TI.numTaxItemID,
					TEMPTax.decTaxValue,
					TEMPTax.tintTaxType,
					numTaxID
				FROM 
					TaxItems TI 
				JOIN 
					DivisionTaxTypes DTT 
				ON 
					TI.numTaxItemID = DTT.numTaxItemID
				OUTER APPLY
				(
					SELECT
						decTaxValue,
						tintTaxType,
						numTaxID
					FROM
						dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,TI.numTaxItemID,NULL,0,@numReturnHeaderID)   
				) AS TEMPTax
				WHERE 
					DTT.numDivisionID=@numDivisionId AND DTT.bitApplicable=1
				UNION 
				SELECT 
					@numReturnHeaderID,
					0,
					TEMPTax.decTaxValue,
					TEMPTax.tintTaxType,
					numTaxID
				FROM 
					dbo.DivisionMaster 
				OUTER APPLY
				(
					SELECT
						decTaxValue,
						tintTaxType,
						numTaxID
					FROM
						dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,0,NULL,1,@numReturnHeaderID)  
				) AS TEMPTax
				WHERE 
					bitNoTax=0 
					AND numDivisionID=@numDivisionID
			END			  
		END
          
        IF CONVERT(VARCHAR(10), @strItems) <> '' 
        BEGIN
            EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems
			                                                                                                        
            SELECT  
				*
            INTO 
				#temp
            FROM 
				OPENXML (@hDocItem, '/NewDataSet/Item',2) 
			WITH 
			( 
				numReturnItemID NUMERIC, numItemCode NUMERIC, numUnitHour FLOAT, numUnitHourReceived FLOAT, monPrice DECIMAL(30,16), 
				monTotAmount MONEY, vcItemDesc VARCHAR(1000), numWareHouseItemID NUMERIC, vcModelID VARCHAR(200), 
				vcManufacturer VARCHAR(250), numUOMId NUMERIC,numOppItemID NUMERIC, bitDiscountType BIT, fltDiscount FLOAT
			)

            EXEC sp_xml_removedocument @hDocItem 
                
            INSERT  INTO [ReturnItems]
			(
				[numReturnHeaderID],
				[numItemCode],
				[numUnitHour],
				[numUnitHourReceived],
				[monPrice],
				[monTotAmount],
				[vcItemDesc],
				[numWareHouseItemID],
				[vcModelID],
				[vcManufacturer],
				[numUOMId],numOppItemID,
				bitDiscountType,fltDiscount
			)
			SELECT  
				@numReturnHeaderID,
				numItemCode,
				numUnitHour,
				0,
				monPrice,
				monTotAmount,
				vcItemDesc,
				NULLIF(numWareHouseItemID, 0) numWareHouseItemID,
				vcModelID,
				vcManufacturer,
				numUOMId,numOppItemID,
				bitDiscountType,
				fltDiscount
			FROM 
				#temp
			WHERE 
				numReturnItemID = 0
					 
            DROP TABLE #temp
   
			IF @tintReturnType=1 OR (@tintReturnType=2 AND (SELECT ISNULL(bitPurchaseTaxCredit,0) FROM Domain WHERE numDomainId=@numDomainId) = 1) 
			BEGIN               
				IF @numOppId>0
				BEGIN
					INSERT INTO dbo.OpportunityItemsTaxItems 
					(
						numReturnHeaderID,numReturnItemID,numTaxItemID,numTaxID
					)  
					SELECT 
						@numReturnHeaderID,OI.numReturnItemID,IT.numTaxItemID,IT.numTaxID
					FROM 
						dbo.ReturnItems OI 
					JOIN 
						dbo.OpportunityItemsTaxItems IT 
					ON 
						OI.numOppItemID=IT.numOppItemID 
					WHERE 
						OI.numReturnHeaderID=@numReturnHeaderID 
						AND IT.numOppId=@numOppId
				END
				ELSE
				BEGIN
					-- INSERT ITEM LEVEL CRV TAX TYPES
					INSERT INTO OpportunityMasterTaxItems
					(
						numReturnHeaderID,
						numTaxItemID,
						fltPercentage,
						tintTaxType,
						numTaxID
					) 
					SELECT 
						OI.numReturnHeaderID,
						1,
						TEMPTax.decTaxValue,
						TEMPTax.tintTaxType,
						TEMPTax.numTaxID
					FROM 
						ReturnItems OI
					INNER JOIN
						ItemTax IT
					ON
						IT.numItemCode = OI.numItemCode
					OUTER APPLY
					(
						SELECT
							decTaxValue,
							tintTaxType,
							numTaxID
						FROM
							dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,OI.numItemCode,IT.numTaxItemID,@numReturnHeaderID,1,NULL)
					) AS TEMPTax
					WHERE
						OI.numReturnHeaderID = @numReturnHeaderID
						AND IT.numTaxItemID = 1 -- CRV TAX
					GROUP BY
						OI.numReturnHeaderID,
						TEMPTax.decTaxValue,
						TEMPTax.tintTaxType,
						TEMPTax.numTaxID

					--Insert Tax for ReturnItems
					INSERT INTO dbo.OpportunityItemsTaxItems 
					(
						numReturnHeaderID,
						numReturnItemID,
						numTaxItemID,
						numTaxID
					) 
					SELECT 
						@numReturnHeaderID,
						OI.numReturnItemID,
						TI.numTaxItemID,
						0
					FROM 
						dbo.ReturnItems OI 
					JOIN 
						dbo.ItemTax IT 
					ON 
						OI.numItemCode=IT.numItemCode 
					JOIN
						TaxItems TI 
					ON 
						TI.numTaxItemID=IT.numTaxItemID 
					WHERE 
						OI.numReturnHeaderID=@numReturnHeaderID 
						AND IT.bitApplicable=1  
					UNION
					SELECT 
						@numReturnHeaderID,
						OI.numReturnItemID,
						0,
						0 
					FROM 
						dbo.ReturnItems OI 
					JOIN 
						dbo.Item I 
					ON 
						OI.numItemCode=I.numItemCode
					WHERE 
						OI.numReturnHeaderID=@numReturnHeaderID 
						AND I.bitTaxable=1
					UNION
					SELECT
						@numReturnHeaderID,
						OI.numReturnItemID,
						1,
						TD.numTaxID
					FROM
						dbo.ReturnItems OI 
					INNER JOIN
						ItemTax IT
					ON
						IT.numItemCode = OI.numItemCode
					INNER JOIN
						TaxDetails TD
					ON
						TD.numTaxID = IT.numTaxID
						AND TD.numDomainId = @numDomainId
					WHERE
						OI.numReturnHeaderID=@numReturnHeaderID
				END
			END
        END 
            
            -- Update leads,prospects to Accounts
		EXEC dbo.USP_UpdateCRMTypeToAccounts @numDivisionID ,@numDomainID ,@numUserCntID
                          
    END    
    ELSE IF @numReturnHeaderID <> 0 
    BEGIN    
        IF ISNULL(@tintMode, 0) = 0
        BEGIN
			UPDATE
				[ReturnHeader]
			SET 
				[vcRMA] = @vcRMA,
				[numReturnReason] = @numReturnReason,
				[numReturnStatus] = @numReturnStatus,
				[monAmount] = ISNULL(monAmount,0) + ISNULL(monTotalDiscount,0) - ISNULL(@monTotalDiscount,0),
				[monTotalDiscount] = @monTotalDiscount,
				[tintReceiveType] = @tintReceiveType,
				[vcComments] = @vcComments,
				[numModifiedBy] = @numUserCntID,
				[dtModifiedDate] = GETUTCDATE()
			WHERE 
				[numDomainId] = @numDomainId
				AND [numReturnHeaderID] = @numReturnHeaderID
		END                 
        ELSE IF ISNULL(@tintMode, 0) = 1 
        BEGIN
            UPDATE  
				ReturnHeader
			SET 
				numReturnStatus = @numReturnStatus
			WHERE 
				numReturnHeaderID = @numReturnHeaderID
							
			IF CONVERT(VARCHAR(10), @strItems) <> '' 
			BEGIN
				EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems
				                                                                                                    
				SELECT 
					*
				INTO 
					#temp1
				FROM OPENXML 
					(@hDocItem, '/NewDataSet/Item',2) 
				WITH 
				( 
					numReturnItemID NUMERIC, 
					numUnitHourReceived FLOAT, 
					numWareHouseItemID NUMERIC
				)
				
				EXEC sp_xml_removedocument @hDocItem 

				UPDATE  
					[ReturnItems]
				SET 
					[numUnitHourReceived] = X.numUnitHourReceived,
					[numWareHouseItemID] =CASE WHEN X.numWareHouseItemID=0 THEN NULL ELSE X.numWareHouseItemID END 
				FROM 
					#temp1 AS X
				WHERE 
					X.numReturnItemID = ReturnItems.numReturnItemID
								AND ReturnItems.numReturnHeaderID = @numReturnHeaderID
				
				DROP TABLE #temp1
			END 
		END
    END            
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageVendorShipmentMethod')
DROP PROCEDURE USP_ManageVendorShipmentMethod
GO
CREATE PROCEDURE [dbo].[USP_ManageVendorShipmentMethod] 
( 
@numDomainID as numeric(9)=0,    
@numVendorid AS NUMERIC(9)=0,
@numAddressID AS NUMERIC(9)=0,
@str AS TEXT = '',
@bitPrimary AS bit=0
)
AS 
DECLARE @hDoc INT
EXEC sp_xml_preparedocument @hDoc OUTPUT, @str

DELETE FROM VendorShipmentMethod WHERE numDomainID=@numDomainID AND numVendorid=@numVendorid AND numAddressID=@numAddressID
UPDATE VendorShipmentMethod SET bitPrimary=0 WHERE numDomainID=@numDomainID AND numVendorid=@numVendorid
UPDATE VendorShipmentMethod SET bitPrimary=1 WHERE numDomainID=@numDomainID AND numVendorid=@numVendorid AND numAddressID=@numAddressID

	INSERT INTO VendorShipmentMethod (
		numDomainID,numVendorid,numAddressID,bitPrimary,
		numListItemID,
		numListValue		
	) 
	SELECT 
		@numDomainID,@numVendorid,@numAddressID,@bitPrimary,
		X.numListItemID,
		X.numListValue
		FROM 	( SELECT    *
          FROM      OPENXML (@hDoc, '/NewDataSet/ShipmentMethod',2) WITH (numListItemID NUMERIC(9), numListValue INT)
        ) X

EXEC sp_xml_removedocument @hDoc

GO
/****** Object:  StoredProcedure [dbo].[USP_OPPBizDocItems]    Script Date: 07/26/2008 16:20:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                                                                                                                                                        
-- exec USP_OPPBizDocItems @numOppId=12388,@numOppBizDocsId=11988,@numDomainID=110
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_oppbizdocitems' ) 
    DROP PROCEDURE usp_oppbizdocitems
GO
CREATE PROCEDURE [dbo].[USP_OPPBizDocItems]
    (
      @numOppId AS NUMERIC(9) = NULL,
      @numOppBizDocsId AS NUMERIC(9) = NULL,
      @numDomainID AS NUMERIC(9) = 0      
    )
AS 
    DECLARE @DivisionID AS NUMERIC(9)      
    DECLARE @tintTaxOperator AS TINYINT   
                                                                                                                                                                                                                                                                                                                                          
    SELECT  @DivisionID = numDivisionID, @tintTaxOperator = [tintTaxOperator]           
    FROM    OpportunityMaster
    WHERE   numOppId = @numOppId                                                                                                                                                                          
--Decimal points to show in bizdoc 
    DECLARE @DecimalPoint AS TINYINT
    SELECT  @DecimalPoint = tintDecimalPoints
    FROM    domain
    WHERE   numDomainID = @numDomainID
                                                                                          
    DECLARE @strSQL AS VARCHAR(MAX)                                                                                          
    DECLARE @strSQL1 AS VARCHAR(8000)                                       
    DECLARE @strSQLCusFields VARCHAR(1000)                      
    DECLARE @strSQLEmpFlds VARCHAR(500)                                                                         
    SET @strSQLCusFields = ''                                                                                          
    SET @strSQLEmpFlds = ''                                                                                          
    DECLARE @tintType AS TINYINT   
    DECLARE @tintOppType AS TINYINT                                                      
    DECLARE @intRowNum AS INT                                                  
    DECLARE @numFldID AS VARCHAR(15)                                          
    DECLARE @vcFldname AS VARCHAR(50)                                            
    DECLARE @numBizDocId AS NUMERIC(9)
    DECLARE @numBizDocTempID AS NUMERIC(9) ;
        SET @numBizDocTempID = 0
                                           
    SELECT  @numBizDocTempID = ISNULL(numBizDocTempID, 0),
            @numBizDocId = numBizDocId
    FROM    OpportunityBizDocs
    WHERE   numOppBizDocsId = @numOppBizDocsId
    SELECT  @tintType = tintOppType,
            @tintOppType = tintOppType
    FROM    OpportunityMaster
    WHERE   numOppID = @numOppId                                                                                          
                                                                                   
    IF @tintType = 1 
        SET @tintType = 7
    ELSE 
        SET @tintType = 8                                                                                      
                                                                                
                                                                                        
                                                                                          
 

    SELECT  *
    INTO    #Temp1
    FROM    ( SELECT   Opp.vcitemname AS vcItemName,
                        ISNULL(OBD.numOppBizDocItemID, 0) OppBizDocItemID,
                        CASE WHEN charitemType = 'P' THEN 'Product'
                             WHEN charitemType = 'S' THEN 'Service'
                        END AS charitemType,
                        CASE WHEN charitemType = 'P' THEN 'Inventory'
	                         WHEN charitemType = 'N' THEN 'Non-Inventory'
                             WHEN charitemType = 'S' THEN 'Service'
                        END AS vcItemType,
                        CASE WHEN bitShowDeptItemDesc = 0
                             THEN CONVERT(VARCHAR(2500), OBD.vcitemDesc)
                             WHEN bitShowDeptItemDesc = 1
                                  AND bitKitParent = 1
                                  AND bitAssembly = 0
                             THEN dbo.fn_PopulateKitDesc(opp.numOppId,
                                                         opp.numoppitemtCode)
                             WHEN bitShowDeptItemDesc = 1
                                  AND bitKitParent = 1
                                  AND bitAssembly = 1
                             THEN dbo.fn_PopulateAssemblyDesc(opp.numoppitemtCode, OBD.numUnitHour)
                             ELSE CONVERT(VARCHAR(2500), OBD.vcitemDesc)
                        END AS txtItemDesc, 
                        dbo.fn_UOMConversion(ISNULL(i.numBaseUnit, 0),
                                             i.numItemCode, @numDomainID,
                                             ISNULL(opp.numUOMId, 0))
                        * OBD.numUnitHour AS numUnitHour,
                        CONVERT(DECIMAL(18, 4), (dbo.fn_UOMConversion(ISNULL(opp.numUOMId, 0), i.numItemCode, @numDomainID, ISNULL(i.numBaseUnit, 0)) * OBD.monPrice)) monPrice,
                        CONVERT(VARCHAR, CONVERT(DECIMAL(18, 4), OBD.monTotAmount)) Amount,
                        OBD.monTotAmount/*Fo calculating sum*/,
                        ISNULL(CONVERT(VARCHAR, i.monListPrice), 0) AS monListPrice,
                        i.numItemCode AS ItemCode,
                        opp.numoppitemtCode,
                        L.vcdata AS numItemClassification,
                        CASE WHEN bitTaxable = 0 THEN 'No'
                             ELSE 'Yes'
                        END AS Taxable,
                        numIncomeChartAcntId AS itemIncomeAccount,
                        'NI' AS ItemType,
                        ISNULL(GJH.numJournal_Id, 0) AS numJournalId,
                        ( SELECT TOP 1
                                    ISNULL(GJD.numTransactionId, 0)
                          FROM      General_Journal_Details GJD
                          WHERE     GJH.numJournal_Id = GJD.numJournalId
                                    AND GJD.chBizDocItems = 'NI'
                                    AND GJD.numoppitemtCode = opp.numoppitemtCode
                        ) AS numTransactionId,
                        ISNULL(Opp.vcModelID, '') vcModelID,
                        (CASE WHEN charitemType = 'P' THEN dbo.USP_GetAttributes(OBD.numWarehouseItmsID,bitSerialized) ELSE ISNULL(OBD.vcAttributes,0) END) AS vcAttributes,
                        ISNULL(vcPartNo, '') AS vcPartNo,
                        ( ISNULL(OBD.monTotAmtBefDiscount, OBD.monTotAmount)
                          - OBD.monTotAmount ) AS DiscAmt,
						(CASE WHEN OBD.numUnitHour = 0 THEN 0 ELSE CONVERT(DECIMAL(18, 4),ISNULL(OBD.monTotAmount,0) / OBD.numUnitHour) END) As monUnitSalePrice,
                        OBD.vcNotes,
                        OBD.vcTrackingNo,
                        OBD.vcShippingMethod,
                        OBD.monShipCost,
                        [dbo].[FormatedDateFromDate](OBD.dtDeliveryDate,
                                                     @numDomainID) dtDeliveryDate,
                        Opp.numWarehouseItmsID,
                        ISNULL(u.vcUnitName, '') numUOMId,
                        ISNULL(Opp.vcManufacturer, '') AS vcManufacturer,
                        ISNULL(V.monCost, 0) AS VendorCost,
                        Opp.vcPathForTImage,
                        i.[fltLength],
                        i.[fltWidth],
                        i.[fltHeight],
                        i.[fltWeight],
                        ISNULL(I.numVendorID, 0) numVendorID,
                        Opp.bitDropShip AS DropShip,
                        SUBSTRING(( SELECT  ' ,' + vcSerialNo
                                            + CASE WHEN ISNULL(I.bitLotNo, 0) = 1
                                                   THEN ' ('
                                                        + CONVERT(VARCHAR(15), oppI.numQty)
                                                        + ')'
                                                   ELSE ''
                                              END
                                    FROM    OppWarehouseSerializedItem oppI
                                            JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID = whi.numWareHouseItmsDTLID
                                    WHERE   oppI.numOppID = Opp.numOppId
                                            AND oppI.numOppItemID = Opp.numoppitemtCode
                                    ORDER BY vcSerialNo
                                  FOR
                                    XML PATH('')
                                  ), 3, 200000) AS SerialLotNo,
                        ISNULL(opp.numUOMId, 0) AS numUOM,
                        ISNULL(u.vcUnitName, '') vcUOMName,
                        dbo.fn_UOMConversion(opp.numUOMId, Opp.numItemCode,
                                             @numDomainID, NULL) AS UOMConversionFactor,
                        ISNULL(Opp.numSOVendorId, 0) AS numSOVendorId,
                        [dbo].FormatedDateTimeFromDate(OBD.dtRentalStartDate,
                                                       @numDomainID) dtRentalStartDate,
                        [dbo].FormatedDateTimeFromDate(OBD.dtRentalReturnDate,
                                                       @numDomainID) dtRentalReturnDate,
                        ISNULL(W.vcWareHouse, '') AS Warehouse,
                        CASE WHEN charitemType = 'P' THEN WI.vcWHSKU
                             ELSE i.vcSKU
                        END vcSKU,
                        CASE WHEN charitemType = 'P' THEN WI.vcBarCode
                             ELSE i.numBarCodeId
                        END vcBarcode,
                        ISNULL(opp.numClassID, 0) AS numClassID,
                        ISNULL(opp.numProjectID, 0) AS numProjectID,
                        ISNULL(I.[numShipClass],0) AS numShipClass,OBD.numOppBizDocID,WL.vcLocation,ISNULL(bitSerialized,0) AS bitSerialized,ISNULL(bitLotNo,0) AS bitLotNo,
                        opp.numOppId,OBD.numUnitHour AS numUnitHourOrig,ISNULL(I.numItemCode,0) AS [numItemCode]				
              FROM      OpportunityItems opp
                        JOIN OpportunityBizDocItems OBD ON OBD.numOppItemID = opp.numoppitemtCode
                        LEFT JOIN item i ON opp.numItemCode = i.numItemCode
                        LEFT JOIN ListDetails L ON i.numItemClassification = L.numListItemID
                        LEFT JOIN Vendor V ON V.numVendorID = @DivisionID
                                              AND V.numItemCode = opp.numItemCode
                        LEFT JOIN General_Journal_Header GJH ON opp.numoppid = GJH.numOppId
                                                                AND GJH.numBizDocsPaymentDetId IS NULL
                                                                AND GJH.numOppBizDocsId = @numOppBizDocsId
                        LEFT JOIN UOM u ON u.numUOMId = opp.numUOMId                                                                       
                        LEFT JOIN dbo.WareHouseItems WI ON opp.numWarehouseItmsID = WI.numWareHouseItemID
                                                           AND WI.numDomainID = @numDomainID
                        LEFT JOIN dbo.Warehouses W ON WI.numDomainID = W.numDomainID
                                                      AND WI.numWareHouseID = W.numWareHouseID
                        LEFT JOIN dbo.WarehouseLocation WL ON WL.numWLocationID = WI.numWLocationID
              WHERE     opp.numOppId = @numOppId
                        AND ( bitShowDeptItem IS NULL
                              OR bitShowDeptItem = 0
                            )
                        AND OBD.numOppBizDocID = @numOppBizDocsId
            ) X


    --IF @DecimalPoint = 1 
    --    BEGIN
    --        UPDATE  #Temp1
    --        SET     monPrice = CONVERT(DECIMAL(18, 1), monPrice),
    --                Amount = CONVERT(DECIMAL(18, 1), Amount)
    --    END
    --IF @DecimalPoint = 2 
    --    BEGIN
    --        UPDATE  #Temp1
    --        SET     monPrice = CONVERT(DECIMAL(18, 2), monPrice),
    --                Amount = CONVERT(DECIMAL(18, 2), Amount)
    --    END
    --IF @DecimalPoint = 3 
    --    BEGIN
    --        UPDATE  #Temp1
    --        SET     monPrice = CONVERT(DECIMAL(18, 3), monPrice),
    --                Amount = CONVERT(DECIMAL(18, 3), Amount)
    --    END
    --IF @DecimalPoint = 4 
    --    BEGIN
    --        UPDATE  #Temp1
    --        SET     monPrice = CONVERT(DECIMAL(18, 4), monPrice),
    --                Amount = CONVERT(DECIMAL(18, 4), Amount)
    --    END

    DECLARE @strSQLUpdate AS VARCHAR(2000);SET @strSQLUpdate=''
   
    EXEC ('ALTER TABLE #Temp1 ADD [TotalTax] MONEY')
	EXEC ('ALTER TABLE #Temp1 ADD [CRV] MONEY')

    IF @tintTaxOperator = 1 --add Sales tax
	BEGIN
        SET @strSQLUpdate = @strSQLUpdate
            + ',[TotalTax]= dbo.fn_CalBizDocTaxAmt('
            + CONVERT(VARCHAR(20), @numDomainID) + ',0,'
            + CONVERT(VARCHAR(20), @numOppId) +','+
            + 'numoppitemtCode,1,Amount,numUnitHour)'

		SET @strSQLUpdate = @strSQLUpdate
            + ',[CRV]= dbo.fn_CalBizDocTaxAmt('
            + CONVERT(VARCHAR(20), @numDomainID) + ',1,'
            + CONVERT(VARCHAR(20), @numOppId) +','+
            + 'numoppitemtCode,1,Amount,numUnitHour)'
	END
    ELSE IF @tintTaxOperator = 2 -- remove sales tax
	BEGIN
        SET @strSQLUpdate = @strSQLUpdate + ',[TotalTax]= 0'
		SET @strSQLUpdate = @strSQLUpdate + ',[CRV]= 0'
	END
    ELSE 
	BEGIN
        SET @strSQLUpdate = @strSQLUpdate
            + ',[TotalTax]= dbo.fn_CalBizDocTaxAmt('
            + CONVERT(VARCHAR(20), @numDomainID) + ',0,'
            + CONVERT(VARCHAR(20), @numOppId) +','+
            + 'numoppitemtCode,1,Amount,numUnitHour)'

		SET @strSQLUpdate = @strSQLUpdate
            + ',[CRV]= dbo.fn_CalBizDocTaxAmt('
            + CONVERT(VARCHAR(20), @numDomainID) + ',1,'
            + CONVERT(VARCHAR(20), @numOppId) +','+
            + 'numoppitemtCode,1,Amount,numUnitHour)'
	END


	DECLARE @vcTaxName AS VARCHAR(100)
    DECLARE @numTaxItemID AS NUMERIC(9)
    SET @numTaxItemID = 0
    SELECT TOP 1
            @vcTaxName = vcTaxName,
            @numTaxItemID = numTaxItemID
    FROM    TaxItems
    WHERE   numDomainID = @numDomainID


    WHILE @numTaxItemID > 0
    BEGIN
        EXEC ('ALTER TABLE #Temp1 ADD [' + @vcTaxName + '] MONEY')

        SET @strSQLUpdate = @strSQLUpdate + ',[' + @vcTaxName
            + ']= dbo.fn_CalBizDocTaxAmt('
            + CONVERT(VARCHAR(20), @numDomainID) + ','
            + CONVERT(VARCHAR(20), @numTaxItemID) + ','
            + CONVERT(VARCHAR(20), @numOppId) +','+
            + 'numoppitemtCode,1,Amount,numUnitHour)'
       

        SELECT TOP 1
                @vcTaxName = vcTaxName,
                @numTaxItemID = numTaxItemID
        FROM    TaxItems
        WHERE   numDomainID = @numDomainID
                AND numTaxItemID > @numTaxItemID

        IF @@rowcount = 0 
            SET @numTaxItemID = 0
    END

    IF @strSQLUpdate <> '' 
    BEGIN
        SET @strSQLUpdate = 'UPDATE #Temp1 SET ItemCode=ItemCode' + @strSQLUpdate + ' WHERE ItemCode > 0'
        EXEC (@strSQLUpdate)
    END

    DECLARE @vcDbColumnName NVARCHAR(50)
    SELECT TOP 1
            @numFldID = numfieldID,
            @vcFldname = vcFieldName,
            @vcDbColumnName = vcDbColumnName,
            @intRowNum = ( tintRow + 1 )
    FROM    View_DynamicCustomColumns
    WHERE   numFormID = @tintType
            AND Grp_id = 5
            AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId
            AND bitCustom = 1
            AND ISNULL(numRelCntType, 0) = @numBizDocTempID
    ORDER BY tintRow                                                                                          
    WHILE @intRowNum > 0                                                                                  
        BEGIN    
            EXEC
                ( 'alter table #Temp1 add [' + @vcDbColumnName
                  + '] varchar(100)'
                )
	
            SET @strSQLUpdate = 'update #Temp1 set ItemCode=ItemCode,['
                + @vcDbColumnName + ']=dbo.GetCustFldValueItem(' + @numFldID
                + ',ItemCode) where ItemCode>0'
            EXEC ( @strSQLUpdate
                )
		                   
            SELECT TOP 1
                    @numFldID = numfieldID,
                    @vcFldname = vcFieldName,
                    @vcDbColumnName = vcDbColumnName,
                    @intRowNum = ( tintRow + 1 )
            FROM    View_DynamicCustomColumns
            WHERE   numFormID = @tintType
                    AND Grp_id = 5
                    AND numDomainID = @numDomainID
                    AND numAuthGroupID = @numBizDocId
                    AND bitCustom = 1
                    AND tintRow >= @intRowNum
                    AND ISNULL(numRelCntType, 0) = @numBizDocTempID
            ORDER BY tintRow                                                                  
		           
            IF @@rowcount = 0 
                SET @intRowNum = 0                                                                                          
        END


    SELECT  Row_NUmber() OVER ( ORDER BY numoppitemtCode ) RowNumber,
            *
    FROM    #Temp1
    ORDER BY numoppitemtCode ASC 

    DROP TABLE #Temp1

    SELECT  ISNULL(tintOppStatus, 0)
    FROM    OpportunityMaster
    WHERE   numOppID = @numOppId                                                                                                 


IF (SELECT COUNT(*) FROM View_DynamicColumns WHERE   numFormID = @tintType AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId AND vcFieldType = 'R'AND ISNULL(numRelCntType, 0) = @numBizDocTempID)=0
BEGIN            
	INSERT INTO DycFormConfigurationDetails (numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numAuthGroupID,numRelCntType)
	select numFormId,numFieldId,1,tintRow,@numDomainID,@numBizDocId,@numBizDocTempID
	 FROM View_DynamicDefaultColumns
	 where numFormId=@tintType and bitDefault=1 and numDomainID=@numDomainID order by tintRow ASC	
	 
	 
	 IF(SELECT COUNT(*) FROM DycFrmConfigBizDocsSumm Ds where numBizDocID=@numBizDocId and Ds.numFormID=@tintType 
	and isnull(Ds.numBizDocTempID,0)=@numBizDocTempID and Ds.numDomainID=@numDomainID)=0
	BEGIN
		INSERT INTO DycFrmConfigBizDocsSumm (numFormID,numBizDocID,numFieldID,numDomainID,numBizDocTempID)
		 SELECT @tintType,@numBizDocId,DFM.numFieldID,@numDomainID,@numBizDocTempID FROM
		 DycFormField_Mapping DFFM 
			JOIN DycFieldMaster DFM on DFFM.numModuleID=DFFM.numModuleID and DFFM.numFieldID=DFM.numFieldID
			where numFormID=16 and DFFM.numDomainID is null
	END

END      


    SELECT  vcFieldName AS vcFormFieldName,
            vcDbColumnName,
            vcFieldDataType,
            tintRow AS intRowNum
    FROM    View_DynamicColumns
    WHERE   numFormID = @tintType
            AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId
            AND vcFieldType = 'R'
            AND ISNULL(numRelCntType, 0) = @numBizDocTempID
    UNION
    SELECT  vcFieldName AS vcFormFieldName,
            vcDbColumnName,
            'C' vcFieldDataType,
            tintRow AS intRowNum
    FROM    View_DynamicCustomColumns
    WHERE   numFormID = @tintType
            AND Grp_id = 5
            AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId
            AND bitCustom = 1
            AND ISNULL(numRelCntType, 0) = @numBizDocTempID
    ORDER BY tintRow
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_OPPBizDocItems_GetWithKitChilds' ) 
    DROP PROCEDURE USP_OPPBizDocItems_GetWithKitChilds
GO
CREATE PROCEDURE [dbo].[USP_OPPBizDocItems_GetWithKitChilds]
(
    @numOppId AS NUMERIC(9) = NULL,
    @numOppBizDocsId AS NUMERIC(9) = NULL,
    @numDomainID AS NUMERIC(9) = 0      
)
AS 
BEGIN
    DECLARE @DivisionID AS NUMERIC(9)      
    DECLARE @tintTaxOperator AS TINYINT   
                                                                                                                                                                                                                                                                                                                                          
    SELECT  
		@DivisionID = numDivisionID, 
		@tintTaxOperator = [tintTaxOperator]           
    FROM    
		OpportunityMaster
    WHERE   
		numOppId = @numOppId                                                                                                                                                                          

    
                                                                                          
    DECLARE @strSQL AS VARCHAR(MAX)                                                                                          
    DECLARE @strSQL1 AS VARCHAR(8000)                                       
    DECLARE @strSQLCusFields VARCHAR(1000) = ''
    DECLARE @strSQLEmpFlds VARCHAR(500) = ''                                                                         
                                                                                              
    DECLARE @tintType AS TINYINT   
    DECLARE @tintOppType AS TINYINT                                                      
    DECLARE @intRowNum AS INT                                                  
    DECLARE @numFldID AS VARCHAR(15)                                          
    DECLARE @vcFldname AS VARCHAR(50)                                            
    DECLARE @numBizDocId AS NUMERIC(9)
    DECLARE @numBizDocTempID AS NUMERIC(9) = 0
	DECLARE @bitDisplayKitChild AS BIT = 0
        
                                           
    SELECT  
		@numBizDocTempID = ISNULL(numBizDocTempID, 0),
        @numBizDocId = numBizDocId
    FROM    
		OpportunityBizDocs
    WHERE   
		numOppBizDocsId = @numOppBizDocsId

	
	SELECT
		@bitDisplayKitChild = ISNULL(bitDisplayKitChild,0)
	FROM	
		BizDocTemplate
	WHERE
		numDomainID = @numDomainID
		AND numBizDocTempID = @numBizDocTempID


    SELECT  
		@tintType = tintOppType,
        @tintOppType = tintOppType
    FROM    
		OpportunityMaster
    WHERE   
		numOppID = @numOppId                                                                                          
                                                                                   
    IF @tintType = 1 
        SET @tintType = 7
    ELSE 
        SET @tintType = 8                                                                                      
                                                                                
    SELECT  
		*
    INTO    
		#Temp1
    FROM    
	( 
		SELECT   
			Opp.vcitemname AS vcItemName,
            ISNULL(OBD.numOppBizDocItemID, 0) OppBizDocItemID,
            CASE WHEN charitemType = 'P' THEN 'Product'
                    WHEN charitemType = 'S' THEN 'Service'
            END AS charitemType,
            CASE WHEN charitemType = 'P' THEN 'Inventory'
	                WHEN charitemType = 'N' THEN 'Non-Inventory'
                    WHEN charitemType = 'S' THEN 'Service'
            END AS vcItemType,
			CONVERT(VARCHAR(2500), OBD.vcitemDesc) AS txtItemDesc, 
			opp.numUnitHour AS numOrgTotalUnitHour,
			dbo.fn_UOMConversion(ISNULL(i.numBaseUnit, 0),i.numItemCode, @numDomainID,ISNULL(opp.numUOMId, 0)) * opp.numUnitHour AS numTotalUnitHour,                                   
            dbo.fn_UOMConversion(ISNULL(i.numBaseUnit, 0),i.numItemCode, @numDomainID,ISNULL(opp.numUOMId, 0)) * OBD.numUnitHour AS numUnitHour,
            CONVERT(DECIMAL(18, 4), (dbo.fn_UOMConversion(ISNULL(opp.numUOMId, 0), i.numItemCode, @numDomainID, ISNULL(i.numBaseUnit, 0)) * OBD.monPrice)) monPrice,
            CONVERT(VARCHAR, CONVERT(DECIMAL(18, 4), OBD.monTotAmount)) Amount,
            OBD.monTotAmount/*Fo calculating sum*/,
            ISNULL(CONVERT(VARCHAR, i.monListPrice), 0) AS monListPrice,
            i.numItemCode AS ItemCode,
            opp.numoppitemtCode,
            L.vcdata AS numItemClassification,
            CASE WHEN bitTaxable = 0 THEN 'No'
                    ELSE 'Yes'
            END AS Taxable,
            numIncomeChartAcntId AS itemIncomeAccount,
            'NI' AS ItemType,
            ISNULL(GJH.numJournal_Id, 0) AS numJournalId,
            ( SELECT TOP 1
                        ISNULL(GJD.numTransactionId, 0)
                FROM      General_Journal_Details GJD
                WHERE     GJH.numJournal_Id = GJD.numJournalId
                        AND GJD.chBizDocItems = 'NI'
                        AND GJD.numoppitemtCode = opp.numoppitemtCode
            ) AS numTransactionId,
            ISNULL(Opp.vcModelID, '') vcModelID,
            (CASE WHEN charitemType = 'P' THEN dbo.USP_GetAttributes(OBD.numWarehouseItmsID, bitSerialized) ELSE ISNULL(OBD.vcAttributes,0) END) AS vcAttributes,
            ISNULL(vcPartNo, '') AS vcPartNo,
            (ISNULL(OBD.monTotAmtBefDiscount, OBD.monTotAmount) - OBD.monTotAmount ) AS DiscAmt,
			(CASE WHEN OBD.numUnitHour = 0 THEN 0 ELSE CONVERT(DECIMAL(18, 4),(dbo.fn_UOMConversion(ISNULL(opp.numUOMId, 0), i.numItemCode, @numDomainID, ISNULL(i.numBaseUnit, 0)) * (ISNULL(OBD.monTotAmount,0) / OBD.numUnitHour))) END) As monUnitSalePrice,
            OBD.vcNotes,
            OBD.vcTrackingNo,
            OBD.vcShippingMethod,
            OBD.monShipCost,
            [dbo].[FormatedDateFromDate](OBD.dtDeliveryDate,@numDomainID) dtDeliveryDate,
            Opp.numWarehouseItmsID,
            ISNULL(u.vcUnitName, '') numUOMId,
            ISNULL(Opp.vcManufacturer, '') AS vcManufacturer,
            ISNULL(V.monCost, 0) AS VendorCost,
            Opp.vcPathForTImage,
            i.[fltLength],
            i.[fltWidth],
            i.[fltHeight],
            i.[fltWeight],
            ISNULL(I.numVendorID, 0) numVendorID,
            Opp.bitDropShip AS DropShip,
            SUBSTRING(( SELECT  ' ,' + vcSerialNo
                                + CASE WHEN ISNULL(I.bitLotNo, 0) = 1
                                        THEN ' ('
                                            + CONVERT(VARCHAR(15), oppI.numQty)
                                            + ')'
                                        ELSE ''
                                    END
                        FROM    OppWarehouseSerializedItem oppI
                                JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID = whi.numWareHouseItmsDTLID
                        WHERE   oppI.numOppID = Opp.numOppId
                                AND oppI.numOppItemID = Opp.numoppitemtCode
                        ORDER BY vcSerialNo
                        FOR
                        XML PATH('')
                        ), 3, 200000) AS SerialLotNo,
            ISNULL(opp.numUOMId, 0) AS numUOM,
            ISNULL(u.vcUnitName, '') vcUOMName,
            dbo.fn_UOMConversion(opp.numUOMId, Opp.numItemCode,
                                    @numDomainID, NULL) AS UOMConversionFactor,
            ISNULL(Opp.numSOVendorId, 0) AS numSOVendorId,
            [dbo].FormatedDateTimeFromDate(OBD.dtRentalStartDate,
                                            @numDomainID) dtRentalStartDate,
            [dbo].FormatedDateTimeFromDate(OBD.dtRentalReturnDate,
                                            @numDomainID) dtRentalReturnDate,
            ISNULL(W.vcWareHouse, '') AS Warehouse,
            CASE WHEN charitemType = 'P' THEN WI.vcWHSKU
                    ELSE i.vcSKU
            END vcSKU,
            CASE WHEN charitemType = 'P' THEN WI.vcBarCode
                    ELSE i.numBarCodeId
            END vcBarcode,
            ISNULL(opp.numClassID, 0) AS numClassID,
            ISNULL(opp.numProjectID, 0) AS numProjectID,
            ISNULL(I.[numShipClass],0) AS numShipClass,OBD.numOppBizDocID,WL.vcLocation,ISNULL(bitSerialized,0) AS bitSerialized,ISNULL(bitLotNo,0) AS bitLotNo,
            opp.numOppId,OBD.numUnitHour AS numUnitHourOrig,ISNULL(I.numItemCode,0) AS [numItemCode],
			ISNULL(I.bitKitParent,0) AS bitKitParent,
			0 AS numOppChildItemID, 
			0 AS numOppKitChildItemID,
			0 AS bitChildItem,
			0 AS tintLevel	
        FROM      
			OpportunityItems opp
        JOIN 
			OpportunityBizDocItems OBD 
		ON 
			OBD.numOppItemID = opp.numoppitemtCode
        LEFT JOIN 
			item i 
		ON 
			opp.numItemCode = i.numItemCode
        LEFT JOIN 
			ListDetails L 
		ON 
			i.numItemClassification = L.numListItemID
        LEFT JOIN 
			Vendor V 
		ON 
			V.numVendorID = @DivisionID
            AND V.numItemCode = opp.numItemCode
        LEFT JOIN 
			General_Journal_Header GJH 
		ON 
			opp.numoppid = GJH.numOppId
            AND GJH.numBizDocsPaymentDetId IS NULL
            AND GJH.numOppBizDocsId = @numOppBizDocsId
        LEFT JOIN 
			UOM u 
		ON 
			u.numUOMId = opp.numUOMId                                      
        LEFT JOIN 
			dbo.WareHouseItems WI 
		ON 
			opp.numWarehouseItmsID = WI.numWareHouseItemID
			AND WI.numDomainID = @numDomainID
        LEFT JOIN 
			dbo.Warehouses W 
		ON 
			WI.numDomainID = W.numDomainID
            AND WI.numWareHouseID = W.numWareHouseID
        LEFT JOIN 
			dbo.WarehouseLocation WL 
		ON 
			WL.numWLocationID = WI.numWLocationID
        WHERE     
			opp.numOppId = @numOppId
            AND (bitShowDeptItem IS NULL OR bitShowDeptItem = 0)
            AND OBD.numOppBizDocID = @numOppBizDocsId
    ) X

	--GET KIT CHILD ITEMS (NOW WE ARE SUPPORTING KIT WITHIN KIT BUT ONLY 1 LEVEL MEANS CHILD KIT INSIDE PARENT KIT CAN NOT HAVE ANOTHER KIT AS CHILD)
	IF (@bitDisplayKitChild=1 AND (SELECT COUNT(*) FROM OpportunityKitItems WHERE numOppId = @numOppId ) > 0)
	BEGIN
		DECLARE @TEMPTABLE TABLE(
			numOppItemCode NUMERIC(18,0),
			numOppChildItemID NUMERIC(18,0), 
			numOppKitChildItemID NUMERIC(18,0),
			numItemCode NUMERIC(18,0),
			numWarehouseItemID NUMERIC(18,0),
			numTotalQty NUMERIC(18,2),
			numQty NUMERIC(18,2),
			numUOMID NUMERIC(18,0),
			tintLevel TINYINT
		);

		--FIRST LEVEL CHILD
		INSERT INTO @TEMPTABLE
		(
			numOppItemCode,numOppChildItemID,numOppKitChildItemID,numItemCode,numWarehouseItemID,numTotalQty,numQty,numUOMID,tintLevel
		)
		SELECT 
			OKI.numOppItemID,
			OKI.numOppChildItemID,
			CAST(0 AS NUMERIC(18,0)),
			OKI.numChildItemID,
			ISNULL(OKI.numWareHouseItemId,0),
			CAST((t1.numOrgTotalUnitHour * OKI.numQtyItemsReq_Orig) AS NUMERIC(18,2)),
			CAST((t1.numUnitHourOrig * OKI.numQtyItemsReq_Orig) AS NUMERIC(18,2)),
			ISNULL(OKI.numUOMID,0),
			1
		FROM 
			#Temp1 t1
		INNER JOIN
			OpportunityKitItems OKI
		ON
			t1.numoppitemtCode = OKI.numOppItemID
		WHERE
			t1.bitKitParent = 1

		--SECOND LEVEL CHILD
		INSERT INTO @TEMPTABLE
		(
			numOppItemCode,numOppChildItemID,numOppKitChildItemID,numItemCode,numWarehouseItemID,numTotalQty,numQty,numUOMID,tintLevel
		)
		SELECT 
			OKCI.numOppItemID,
			OKCI.numOppChildItemID,
			OKCI.numOppKitChildItemID,
			OKCI.numItemID,
			ISNULL(OKCI.numWareHouseItemId,0),
			CAST((c.numTotalQty * OKCI.numQtyItemsReq_Orig) AS NUMERIC(18,2)),
			CAST((c.numQty * OKCI.numQtyItemsReq_Orig) AS NUMERIC(18,2)),
			ISNULL(OKCI.numUOMID,0),
			2
		FROM 
			OpportunityKitChildItems OKCI
		INNER JOIN
			@TEMPTABLE c
		ON
			OKCI.numOppID = @numOppId
			AND OKCI.numOppItemID = c.numOppItemCode
			AND OKCI.numOppChildItemID = c.numOppChildItemID

		INSERT INTO 
			#Temp1
		SELECT   
			(CASE WHEN t2.tintLevel = 1 THEN CONCAT('&nbsp;&nbsp;',I.vcItemName) WHEN t2.tintLevel = 2 THEN CONCAT('&nbsp;&nbsp;&nbsp;&nbsp;',I.vcItemName) ELSE I.vcItemName END) AS vcItemName,
            0 OppBizDocItemID,
            (CASE WHEN I.charitemType = 'P' THEN 'Product' WHEN charitemType = 'S' THEN 'Service' END) AS charitemType,
            (CASE WHEN charitemType = 'P' THEN 'Inventory'
	                WHEN charitemType = 'N' THEN 'Non-Inventory'
                    WHEN charitemType = 'S' THEN 'Service'
            END) AS vcItemType,
			CONVERT(VARCHAR(2500), I.txtItemDesc) AS txtItemDesc,   
			t2.numTotalQty AS numOrigTotalUnitHour,
			t2.numTotalQty AS numTotalUnitHour,
            t2.numQty AS numUnitHour,
            (ISNULL(dbo.fn_UOMConversion(ISNULL(t2.numUOMID, 0), i.numItemCode, @numDomainID, ISNULL(i.numBaseUnit, 0)),1) * (CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END)) monPrice,
            CONVERT(VARCHAR, CONVERT(DECIMAL(18, 4),((t2.numQty * ISNULL(dbo.fn_UOMConversion(ISNULL(t2.numUOMID, 0), i.numItemCode, @numDomainID, ISNULL(i.numBaseUnit, 0)),1)) * (CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END)))) Amount,
            ((t2.numQty * ISNULL(dbo.fn_UOMConversion(ISNULL(t2.numUOMID, 0), i.numItemCode, @numDomainID, ISNULL(i.numBaseUnit, 0)),1)) * (CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END)) AS monTotAmount/*Fo calculating sum*/,
            ISNULL(CONVERT(VARCHAR, (CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END)), 0) AS monListPrice,
            I.numItemCode AS ItemCode,
            t2.numOppItemCode,
            L.vcdata AS numItemClassification,
            CASE WHEN bitTaxable = 0 THEN 'No' ELSE 'Yes' END AS Taxable,
            numIncomeChartAcntId AS itemIncomeAccount,
            'NI' AS ItemType,
            0 AS numJournalId,
            0 AS numTransactionId,
            ISNULL(I.vcModelID, '') vcModelID,
            dbo.USP_GetAttributes(t2.numWarehouseItemID, bitSerialized) AS vcAttributes,
            ISNULL(vcPartNo, '') AS vcPartNo,
            0 AS DiscAmt,
			(ISNULL(dbo.fn_UOMConversion(ISNULL(t2.numUOMID, 0), i.numItemCode, @numDomainID, ISNULL(i.numBaseUnit, 0)),1) * (CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END)) As monUnitSalePrice,
            '' AS vcNotes,'' AS vcTrackingNo,'' AS vcShippingMethod,0 AS monShipCost,NULL AS dtDeliveryDate,t2.numWarehouseItemID,
            ISNULL(u.vcUnitName, '') numUOMId,ISNULL(I.vcManufacturer, '') AS vcManufacturer,ISNULL(V.monCost, 0) AS VendorCost,
            ISNULL((SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = I.numItemCode AND bitDefault=1),'') AS vcPathForTImage,
            I.[fltLength],I.[fltWidth],I.[fltHeight],I.[fltWeight],ISNULL(I.numVendorID, 0) numVendorID,0 AS DropShip,
            '' AS SerialLotNo,
            ISNULL(t2.numUOMId, 0) AS numUOM,
            ISNULL(u.vcUnitName, '') vcUOMName,
            dbo.fn_UOMConversion(t2.numUOMId, t2.numItemCode,@numDomainID, NULL) AS UOMConversionFactor,
            0 AS numSOVendorId,
            NULL AS dtRentalStartDate,
            NULL dtRentalReturnDate,
            ISNULL(W.vcWareHouse, '') AS Warehouse,
            CASE WHEN charitemType = 'P' THEN WI.vcWHSKU ELSE I.vcSKU END vcSKU,
            CASE WHEN charitemType = 'P' THEN WI.vcBarCode ELSE I.numBarCodeId END vcBarcode,
            ISNULL(I.numItemClass, 0) AS numClassID,
            0 AS numProjectID,
            ISNULL(I.[numShipClass],0) AS numShipClass,
			@numOppBizDocsId AS numOppBizDocID,WL.vcLocation,ISNULL(bitSerialized,0) AS bitSerialized,ISNULL(bitLotNo,0) AS bitLotNo,
            @numOppId AS numOppId,t2.numQty AS numUnitHourOrig,
			ISNULL(I.numItemCode,0) AS [numItemCode],
			ISNULL(I.bitKitParent,0) AS bitKitParent,
			ISNULL(t2.numOppChildItemID,0) AS numOppChildItemID, 
			ISNULL(t2.numOppKitChildItemID,0) AS numOppKitChildItemID,
			1 AS bitChildItem,
			t2.tintLevel AS tintLevel
		FROM
			@TEMPTABLE t2
		INNER JOIN
			Item I
		ON
			t2.numItemCode = I.numItemCode
		LEFT JOIN
			WareHouseItems WI
		ON
			WI.numWareHouseItemID = t2.numWarehouseItemID
		LEFT JOIN 
			dbo.Warehouses W 
		ON 
			WI.numDomainID = W.numDomainID
            AND WI.numWareHouseID = W.numWareHouseID
        LEFT JOIN 
			dbo.WarehouseLocation WL 
		ON 
			WL.numWLocationID = WI.numWLocationID
		LEFT JOIN 
			ListDetails L 
		ON 
			I.numItemClassification = L.numListItemID
		LEFT JOIN 
			Vendor V 
		ON 
			V.numVendorID = @DivisionID
            AND V.numItemCode = t2.numItemCode
		LEFT JOIN 
			UOM u 
		ON 
			u.numUOMId = t2.numUOMId 
	END


    DECLARE @strSQLUpdate AS VARCHAR(2000);SET @strSQLUpdate=''
   
    EXEC ('ALTER TABLE #Temp1 ADD [TotalTax] MONEY')

    IF @tintTaxOperator = 1 --add Sales tax
	BEGIN
		SET @strSQLUpdate = @strSQLUpdate
            + ',[TotalTax]= (CASE WHEN ISNULL(bitChildItem,0) = 0 THEN dbo.fn_CalBizDocTaxAmt('
            + CONVERT(VARCHAR(20), @numDomainID) + ',0,'
            + CONVERT(VARCHAR(20), @numOppId) +','+
            + 'numoppitemtCode,1,Amount,numUnitHour) ELSE 0 END)'
	END        
    ELSE IF @tintTaxOperator = 2 -- remove sales tax
	BEGIN
        SET @strSQLUpdate = @strSQLUpdate + ',[TotalTax]= 0'
	END
    ELSE 
	BEGIN
        SET @strSQLUpdate = @strSQLUpdate
            + ',[TotalTax]= (CASE WHEN ISNULL(bitChildItem,0) = 0 THEN dbo.fn_CalBizDocTaxAmt('
            + CONVERT(VARCHAR(20), @numDomainID) + ',0,'
            + CONVERT(VARCHAR(20), @numOppId) +','+
            + 'numoppitemtCode,1,Amount,numUnitHour) ELSE 0 END)'
    END

	-- CRV TAX TYPE
	EXEC ('ALTER TABLE #Temp1 ADD [CRV] MONEY')

	IF @tintTaxOperator = 2
	BEGIN
		SET @strSQLUpdate = @strSQLUpdate
				+ ',[CRV]= (CASE WHEN ISNULL(bitChildItem,0) = 0 THEN dbo.fn_CalBizDocTaxAmt('
				+ CONVERT(VARCHAR(20), @numDomainID) + ',1,'
				+ CONVERT(VARCHAR(20), @numOppId) +','+
				+ 'numoppitemtCode,1,Amount,numUnitHour) ELSE 0 END)'
	END
	ELSE
	BEGIN
		SET @strSQLUpdate = @strSQLUpdate + ',[CRV]= 0'
	END

	DECLARE @vcTaxName AS VARCHAR(100)
    DECLARE @numTaxItemID AS NUMERIC(9)
    SET @numTaxItemID = 0
    
	SELECT TOP 1
        @vcTaxName = vcTaxName,
        @numTaxItemID = numTaxItemID
    FROM    
		TaxItems
    WHERE   
		numDomainID = @numDomainID

    WHILE @numTaxItemID > 0
    BEGIN
        EXEC ('ALTER TABLE #Temp1 ADD [' + @vcTaxName + '] MONEY')

		IF @tintTaxOperator = 2 -- remove sales tax
		BEGIN
			SET @strSQLUpdate = @strSQLUpdate + ',[' + @vcTaxName + ']=0' 
		END
		ELSE
		BEGIN
			SET @strSQLUpdate = @strSQLUpdate + ',[' + @vcTaxName
				+ ']= (CASE WHEN ISNULL(bitChildItem,0) = 0 THEN dbo.fn_CalBizDocTaxAmt('
				+ CONVERT(VARCHAR(20), @numDomainID) + ','
				+ CONVERT(VARCHAR(20), @numTaxItemID) + ','
				+ CONVERT(VARCHAR(20), @numOppId) +','+
				+ 'numoppitemtCode,1,Amount,numUnitHour) ELSE 0 END)'
		END

        SELECT TOP 1
			@vcTaxName = vcTaxName,
            @numTaxItemID = numTaxItemID
        FROM    
			TaxItems
        WHERE   
			numDomainID = @numDomainID
            AND numTaxItemID > @numTaxItemID

        IF @@rowcount = 0 
            SET @numTaxItemID = 0
    END

    IF @strSQLUpdate <> '' 
    BEGIN
        SET @strSQLUpdate = 'UPDATE #Temp1 SET ItemCode=ItemCode' + @strSQLUpdate + ' WHERE ItemCode > 0 AND bitChildItem=0'
        EXEC (@strSQLUpdate)
    END

    DECLARE @vcDbColumnName NVARCHAR(50)

    SELECT TOP 1
            @numFldID = numfieldID,
            @vcFldname = vcFieldName,
            @vcDbColumnName = vcDbColumnName,
            @intRowNum = ( tintRow + 1 )
    FROM    View_DynamicCustomColumns
    WHERE   numFormID = @tintType
            AND Grp_id = 5
            AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId
            AND bitCustom = 1
            AND ISNULL(numRelCntType, 0) = @numBizDocTempID
    ORDER BY tintRow     
	                                                                                     
    WHILE @intRowNum > 0                                                                                  
    BEGIN    
        EXEC('ALTER TABLE #Temp1 ADD [' + @vcDbColumnName + '] varchar(100)')
	
        SET @strSQLUpdate = 'UPDATE #Temp1 set ItemCode=ItemCode,[' + @vcDbColumnName + ']= dbo.GetCustFldValueItem(' + @numFldID  + ',ItemCode) WHERE ItemCode > 0 AND bitChildItem=0'
            
		EXEC (@strSQLUpdate)
		                   
        SELECT 
			TOP 1
            @numFldID = numfieldID,
            @vcFldname = vcFieldName,
            @vcDbColumnName = vcDbColumnName,
            @intRowNum = ( tintRow + 1 )
        FROM    
			View_DynamicCustomColumns
        WHERE   
			numFormID = @tintType
            AND Grp_id = 5
            AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId
            AND bitCustom = 1
            AND tintRow >= @intRowNum
            AND ISNULL(numRelCntType, 0) = @numBizDocTempID
        ORDER BY 
			tintRow                                                                  
		           
        IF @@rowcount = 0 
            SET @intRowNum = 0                                                                                          
    END


    SELECT  
		ROW_NUMBER() OVER ( ORDER BY numoppitemtCode ) RowNumber,
        *
    FROM    
		#Temp1
    ORDER BY 
		numoppitemtCode ASC, numOppChildItemID ASC, numOppKitChildItemID ASC

    DROP TABLE #Temp1

    SELECT  
		ISNULL(tintOppStatus, 0)
    FROM    
		OpportunityMaster
    WHERE   
		numOppID = @numOppId                                                                                                 


	IF (SELECT 
			COUNT(*) 
		FROM 
			View_DynamicColumns 
		WHERE   
			numFormID = @tintType 
			AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId 
			AND vcFieldType = 'R'
			AND ISNULL(numRelCntType, 0) = @numBizDocTempID
		)=0
	BEGIN            
		INSERT INTO DycFormConfigurationDetails 
		(
			numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numAuthGroupID,numRelCntType
		)
		SELECT 
			numFormId,numFieldId,1,tintRow,@numDomainID,@numBizDocId,@numBizDocTempID
		FROM 
			View_DynamicDefaultColumns
		WHERE 
			numFormId=@tintType and bitDefault=1 and numDomainID=@numDomainID order by tintRow ASC	
	 
	 
		IF (SELECT 
				COUNT(*) 
			FROM 
				DycFrmConfigBizDocsSumm Ds 
			WHERE 
				numBizDocID=@numBizDocId 
				AND Ds.numFormID=@tintType 
				AND ISNULL(Ds.numBizDocTempID,0)=@numBizDocTempID 
				AND Ds.numDomainID=@numDomainID
			)=0
		BEGIN
			INSERT INTO DycFrmConfigBizDocsSumm 
			(
				numFormID,numBizDocID,numFieldID,numDomainID,numBizDocTempID
			)
			SELECT 
				@tintType,@numBizDocId,DFM.numFieldID,@numDomainID,@numBizDocTempID 
			FROM
				DycFormField_Mapping DFFM 
			JOIN 
				DycFieldMaster DFM 
			ON 
				DFFM.numModuleID=DFFM.numModuleID and DFFM.numFieldID=DFM.numFieldID
			WHERE 
				numFormID=16 
				AND DFFM.numDomainID IS NULL
		END
	END      

    SELECT  
		vcFieldName AS vcFormFieldName,
        vcDbColumnName,
        vcFieldDataType,
        tintRow AS intRowNum
    FROM    
		View_DynamicColumns
    WHERE   
		numFormID = @tintType
        AND numDomainID = @numDomainID
        AND numAuthGroupID = @numBizDocId
        AND vcFieldType = 'R'
        AND ISNULL(numRelCntType, 0) = @numBizDocTempID
    UNION
    SELECT  
		vcFieldName AS vcFormFieldName,
        vcDbColumnName,
        'C' vcFieldDataType,
        tintRow AS intRowNum
    FROM    
		View_DynamicCustomColumns
    WHERE   
		numFormID = @tintType
        AND Grp_id = 5
        AND numDomainID = @numDomainID
        AND numAuthGroupID = @numBizDocId
        AND bitCustom = 1
        AND ISNULL(numRelCntType, 0) = @numBizDocTempID
    ORDER BY 
		tintRow
END
GO
/****** Object:  StoredProcedure [dbo].[USP_OPPItemDetails]    Script Date: 09/01/2009 18:59:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- exec USP_OPPItemDetails @numItemCode=31455,@OpportunityId=12387,@numDomainId=110
--created by anooop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_oppitemdetails')
DROP PROCEDURE usp_oppitemdetails
GO
CREATE PROCEDURE [dbo].[USP_OPPItemDetails]
    (
      @numUserCntID NUMERIC(9) = 0,
      @OpportunityId NUMERIC(9) = 0,
      @numDomainId NUMERIC(9),
      @ClientTimeZoneOffset  INT
    )
AS 
    BEGIN
        IF @OpportunityId >0
            BEGIN
                DECLARE @DivisionID AS NUMERIC(9)
                DECLARE @TaxPercentage AS FLOAT
                DECLARE @tintOppType AS TINYINT
                DECLARE @fld_Name AS VARCHAR(500)
                DECLARE @strSQL AS VARCHAR(MAX)
                DECLARE @fld_ID AS NUMERIC(9)
                DECLARE @numBillCountry AS NUMERIC(9)
                DECLARE @numBillState AS NUMERIC(9)

                
                SELECT  @DivisionID = numDivisionID,
                        @tintOppType = tintOpptype
                FROM    OpportunityMaster
                WHERE   numOppId = @OpportunityId
               

SET @strSQL = ''

--------------Make Dynamic Query--------------
SET @strSQL = @strSQL + '
Select 
Opp.numoppitemtCode,Opp.vcNotes,
ISNULL(Opp.bitItemPriceApprovalRequired,0) AS bitItemPriceApprovalRequired,
OM.tintOppType,
Opp.numItemCode,
I.charItemType,
ISNULL(I.bitAsset,0) as bitAsset,
ISNULL(I.bitRental,0) as bitRental,
Opp.numOppId,
Opp.bitWorkOrder,
Opp.fltDiscount,
ISNULL(( SELECT numReturnID FROM   Returns WHERE  numOppItemCode = Opp.numoppitemtCode AND numOppId = Opp.numOppID), 0) AS numReturnID,
dbo.fn_GetReturnItemQty(Opp.numoppitemtCode) AS ReturrnedQty,
[dbo].fn_GetReturnStatus(Opp.numoppitemtCode) AS ReturnStatus,
ISNULL(opp.numUOMId, 0) AS numUOM,
(SELECT vcUnitName FROM dbo.UOM WHERE numUOMId = opp.numUOMId AND ISNULL(bitEnabled,0) = 1)  AS  [vcBaseUOMName],
Opp.numUnitHour AS numOriginalUnitHour, 
isnull(M.vcPOppName,''Internal'') as Source,
numSourceID,
  CASE WHEN ISNULL(bitKitParent,0) = 1
                               AND ISNULL(bitAssembly,0) = 1 THEN 0
                          WHEN ISNULL(bitKitParent,0) = 1 THEN 1
                          ELSE 0
                     END as bitKitParent,
(CASE WHEN OM.tintOppType = 1 AND OM.tintOppStatus = 1 THEN 
(CASE WHEN (I.charItemType = ''p'' AND ISNULL(bitDropShip, 0) = 0 AND ISNULL(bitAsset,0)=0)THEN dbo.CheckOrderItemInventoryStatus(Opp.numItemCode,Opp.numUnitHour,Opp.numoppitemtCode,isnull(Opp.numWarehouseItmsID,0),(CASE WHEN ISNULL(bitKitParent,0) = 1
                               AND ISNULL(bitAssembly,0) = 1 THEN 0
                          WHEN ISNULL(bitKitParent,0) = 1 THEN 1
                          ELSE 0
                     END)) ELSE 0 END) ELSE 0 END) AS bitBackOrder,
(SELECT vcData FROM dbo.ListDetails WHERE 1=1 AND numListItemID = numShipClass ) AS [numShipClass],ISNULL(Opp.numQtyShipped,0) numQtyShipped,ISNULL(Opp.numUnitHourReceived,0) AS numUnitHourReceived,ISNULL(OM.fltExchangeRate,0) AS fltExchangeRate,ISNULL(OM.numCurrencyID,0) AS numCurrencyID,ISNULL(OM.numDivisionID,0) AS numDivisionID,
(CASE 
	WHEN OM.tintOppType = 1 AND OM.tintOppStatus = 1 AND UPPER(I.charItemType) = ''P'' AND bitDropShip = 0
	THEN (CASE WHEN OM.tintShipped = 1 THEN CAST(ISNULL(Opp.numUnitHour,0) AS VARCHAR) ELSE CAST(ISNULL(Opp.numQtyShipped,0) AS VARCHAR) END)
	WHEN OM.tintOppType = 2 AND OM.tintOppStatus = 1 AND UPPER(I.charItemType) = ''P'' AND bitDropShip = 0
	THEN (CASE WHEN OM.tintShipped = 1 THEN CAST(ISNULL(Opp.numUnitHour,0) AS VARCHAR) ELSE CAST(ISNULL(Opp.numUnitHourReceived,0) AS VARCHAR) END)
	ELSE '''' END) AS vcShippedReceived,
isnull(i.numIncomeChartAcntId,0) as itemIncomeAccount,isnull(i.numAssetChartAcntId,0) as itemInventoryAsset,isnull(i.numCOGsChartAcntId,0) as itemCoGs,
ISNULL(Opp.numRentalIN,0) as numRentalIN,
ISNULL(Opp.numRentalLost,0) as numRentalLost,
ISNULL(Opp.numRentalOut,0) as numRentalOut,
(SELECT vcData FROM dbo.ListDetails WHERE 1=1 AND numListItemID = numShipClass ) AS [numShipClass],ISNULL(bitFreeShipping,0) AS [IsFreeShipping],Opp.bitDiscountType,isnull(Opp.monLandedCost,0) as monLandedCost, '

--	IF @fld_Name = 'vcPathForTImage' 
		SET @strSQL = @strSQL + 'Opp.vcPathForTImage,'
--	ELSE IF @fld_Name = 'vcItemName' 
		SET @strSQL = @strSQL + 'Opp.vcItemName,'
--	ELSE IF @fld_Name = 'vcModelID,' 
		SET @strSQL = @strSQL + 'Opp.vcModelID,'
--	ELSE IF @fld_Name = 'vcWareHouse' 
		SET @strSQL = @strSQL + 'vcWarehouse,'
		SET @strSQL = @strSQL + 'WItems.numOnHand,isnull(WItems.numAllocation,0) as numAllocation,'
		SET @strSQL = @strSQL + 'WL.vcLocation,'
--	ELSE IF @fld_Name = 'ItemType' 
		SET @strSQL = @strSQL + 'Opp.vcType ItemType,Opp.vcType,'
--	ELSE IF @fld_Name = 'vcItemDesc' 
		SET @strSQL = @strSQL + 'ISNULL(Opp.vcItemDesc, '''') vcItemDesc,'
--	ELSE IF @fld_Name = 'Attributes' 
		SET @strSQL = @strSQL + 'Opp.vcAttributes,' --dbo.fn_GetAttributes(Opp.numWarehouseItmsID, I.bitSerialized) AS Attributes,
--	ELSE IF @fld_Name = 'bitDropShip' 
		SET @strSQL = @strSQL + 'ISNULL(bitDropShip, 0) as DropShip,CASE ISNULL(bitDropShip, 0) WHEN 1 THEN ''Yes'' ELSE '''' END AS bitDropShip,'
--	ELSE IF @fld_Name = 'numUnitHour' 
		SET @strSQL = @strSQL + 'CAST(( dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), Opp.numItemCode,om.numDomainId, ISNULL(opp.numUOMId, 0))* Opp.numUnitHour ) AS NUMERIC(18, 2)) AS numUnitHour,'
--	ELSE IF @fld_Name = 'monPrice' 
		SET @strSQL = @strSQL + 'Opp.monPrice,'
--	ELSE IF @fld_Name = 'monPriceUOM' 
		SET @strSQL = @strSQL + 'CAST(( dbo.fn_UOMConversion(ISNULL(opp.numUOMId, 0), Opp.numItemCode,om.numDomainId, ISNULL(I.numBaseUnit, 0)) * Opp.monPrice) AS NUMERIC(18, 4)) AS monPriceUOM,'
--	ELSE IF @fld_Name = 'monTotAmount' 
		SET @strSQL = @strSQL + 'Opp.monTotAmount,'
--	ELSE IF @fld_Name = 'vcSKU' 
		SET @strSQL = @strSQL + 'ISNULL(WItems.vcWHSKU,ISNULL(I.vcSKU,'''')) [vcSKU],'
--	ELSE IF @fld_Name = 'vcManufacturer' 
		SET @strSQL = @strSQL + 'Opp.vcManufacturer,'
--	ELSE IF @fld_Name = 'vcItemClassification' 
		SET @strSQL = @strSQL + ' dbo.[fn_GetListItemName](I.[numItemClassification]) numItemClassification,'
--	ELSE IF @fld_Name = 'numItemGroup' 
		SET @strSQL = @strSQL + '( SELECT    [vcItemGroup] FROM      [ItemGroups] WHERE     [numItemGroupID] = I.numItemGroup ) numItemGroup,'
--	ELSE IF @fld_Name = 'vcUOMName' 
		SET @strSQL = @strSQL + 'ISNULL(u.vcUnitName, '''') numUOMId,'
--	ELSE IF @fld_Name = 'fltWeight' 
		SET @strSQL = @strSQL + 'I.fltWeight,'
--	ELSE IF @fld_Name = 'fltHeight' 
		SET @strSQL = @strSQL + 'I.fltHeight,'
--	ELSE IF @fld_Name = 'fltWidth' 
		SET @strSQL = @strSQL + 'I.fltWidth,'
--	ELSE IF @fld_Name = 'fltLength' 
		SET @strSQL = @strSQL + 'I.fltLength,'
--	ELSE IF @fld_Name = 'numBarCodeId' 
		SET @strSQL = @strSQL + 'I.numBarCodeId,'
		SET @strSQL = @strSQL + 'I.IsArchieve,'
--	ELSE IF @fld_Name = 'vcWorkOrderStatus' 
		SET @strSQL = @strSQL + 'CASE WHEN ISNULL(Opp.bitWorkOrder, 0) = 1
				 THEN ( SELECT  dbo.GetListIemName(numWOStatus)
								+ CASE WHEN ISNULL(numWOStatus, 0) = 23184
									   THEN '' - ''
											+ CAST(ISNULL(dbo.fn_GetContactName(numAssignedTo),
														  '''') + '',''
											+ CONVERT(VARCHAR(20), DATEADD(minute, '+ CONVERT(VARCHAR(15),-@ClientTimeZoneOffset)  +', bintCompletedDate)) AS VARCHAR(100))
									   ELSE ''''
								  END
						FROM    WorkOrder
						WHERE   ISNULL(numOppId, 0) = Opp.numOppId
								AND numItemCode = Opp.numItemCode AND ISNULL(numParentWOID,0)=0
					  )
				 ELSE ''''
			END AS vcWorkOrderStatus,'
--	ELSE IF @fld_Name = 'DiscAmt' 
		SET @strSQL = @strSQL + 'CASE WHEN Opp.bitDiscountType = 0
				 THEN REPLACE(STR(ISNULL(Opp.monTotAmtBefDiscount, Opp.monTotAmount)
					  - Opp.monTotAmount,20,4), '' '', '''') + '' (''
					  + CAST(FORMAT(Opp.fltDiscount,''0.00##'') AS VARCHAR(50)) + ''%)''
				 ELSE REPLACE(STR(ISNULL(Opp.monTotAmtBefDiscount, Opp.monTotAmount)
					  - Opp.monTotAmount,20,4), '' '', '''')
			END AS DiscAmt,'	
--	ELSE IF @fld_Name = 'numProjectID' 
		SET @strSQL = @strSQL + ' ISNULL(Opp.numProjectID, 0) numProjectID,
		 CASE WHEN ( SELECT TOP 1
								ISNULL(OBI.numOppBizDocItemID, 0)
						FROM    dbo.OpportunityBizDocs OBD
								INNER JOIN dbo.OpportunityBizDocItems OBI ON OBD.numOppBizDocsID = OBI.numOppBizDocID
						WHERE   OBI.numOppItemID = Opp.numoppitemtCode
								AND ISNULL(OBD.bitAuthoritativeBizDocs, 0) = 1
								and isnull(OBD.numOppID,0)=OM.numOppID
					  ) > 0 THEN 1
				 ELSE 0
			END AS bitItemAddedToAuthBizDoc,'	
--	ELSE IF @fld_Name = 'numClassID' 
		SET @strSQL = @strSQL + 'ISNULL(Opp.numClassID, 0) numClassID,'	
--	ELSE IF @fld_Name = 'vcProjectStageName' 
		SET @strSQL = @strSQL + ' ISNULL(Opp.numProjectStageID, 0) numProjectStageID,
				CASE WHEN OPP.numProjectStageID > 0
				 THEN dbo.fn_GetProjectStageName(OPP.numProjectId,
												 OPP.numProjectStageID)
				 WHEN OPP.numProjectStageID = -1 THEN ''T&E''
				 WHEN OPP.numProjectId > 0
				 THEN CASE WHEN OM.tintOppType = 1 THEN ''S.O.''
						   ELSE ''P.O.''
					  END
				 ELSE ''-''
			END AS vcProjectStageName,'	
	        
	
	

--------------Add custom fields to query----------------
SELECT TOP 1 @fld_Name= vcFieldName,@fld_ID = numFieldId FROM View_DynamicCustomColumns
WHERE 
grp_id = 5 AND numDomainID=@numDomainId
AND numFormId=26 AND bitCustom=1 AND numUserCntID = @numUserCntID ORDER BY numFieldId
WHILE @fld_ID > 0
BEGIN ------------------

	 PRINT @fld_Name
	
	       SET @strSQL = @strSQL + 'dbo.GetCustFldValueItem('
                                    + CONVERT(VARCHAR(15), @fld_ID)
                                    + ',Opp.numItemCode) as ''' + @fld_Name + ''','
       

SELECT TOP 1 @fld_Name= vcFieldName,@fld_ID = numFieldId FROM View_DynamicCustomColumns
WHERE 
grp_id = 5 AND numDomainID=@numDomainId
AND numFormId=26 AND bitCustom=1  AND numUserCntID = @numUserCntID AND numFieldId > @fld_ID ORDER BY numFieldId
 IF @@ROWCOUNT=0
 SET @fld_ID = 0
END ------------------


                                
--Purchase Opportunity ?
IF @tintOppType = 2 
    BEGIN
        SET @strSQL =  @strSQL + 'ISNULL((SELECT sum(numQty) FROM OppWarehouseSerializedItem WHERE numOppID=OM.numOppID AND numOppItemID=Opp.numoppitemtCode),0) AS SerialLotNo,
		  SUBSTRING(( SELECT  '', '' + vcSerialNo
                        + CASE WHEN ISNULL(I.bitLotNo, 0) = 1
                                THEN '' (''
                                    + CONVERT(VARCHAR(15), oppI.numQty)
                                    + '')''
                                ELSE ''''
                            END
                FROM    OppWarehouseSerializedItem oppI
                        JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID = whi.numWareHouseItmsDTLID
                WHERE   oppI.numOppID = Opp.numOppId
                        AND oppI.numOppItemID = Opp.numoppitemtCode
                ORDER BY vcSerialNo
                FOR
                XML PATH('''')
                ), 3, 200000) AS vcSerialLotNo,
		  CAST(Opp.numUnitHour AS FLOAT) numQty,
		  (SELECT vcUnitName FROM dbo.UOM WHERE numUOMId = opp.numUOMId AND ISNULL(bitEnabled,0) = 1)  AS  [vcBaseUOMName],
		  I.bitSerialized,
		  ISNULL(I.bitLotNo, 0) AS bitLotNo,
		  isnull(Opp.numWarehouseItmsID,0) as numWarehouseItmsID,'
	END
ELSE
	BEGIN	
        SET @strSQL =  @strSQL + ' ISNULL((SELECT sum(numQty) FROM OppWarehouseSerializedItem WHERE numOppID=OM.numOppID AND numOppItemID=Opp.numoppitemtCode),0) AS SerialLotNo,
		   SUBSTRING(( SELECT  '', '' + vcSerialNo
                        + CASE WHEN ISNULL(I.bitLotNo, 0) = 1
                                THEN '' (''
                                    + CONVERT(VARCHAR(15), oppI.numQty)
                                    + '')''
                                ELSE ''''
                            END
                FROM    OppWarehouseSerializedItem oppI
                        JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID = whi.numWareHouseItmsDTLID
                WHERE   oppI.numOppID = Opp.numOppId
                        AND oppI.numOppItemID = Opp.numoppitemtCode
                ORDER BY vcSerialNo
                FOR
                XML PATH('''')
                ), 3, 200000) AS vcSerialLotNo,
		   CAST(Opp.numUnitHour AS FLOAT) numQty,
		  I.bitSerialized,
		  ISNULL(I.bitLotNo, 0) AS bitLotNo,
		  isnull(Opp.numWarehouseItmsID,0) as numWarehouseItmsID,'
	END


        
 SET @strSQL = left(@strSQL,len(@strSQL)-1) + '   
        FROM    OpportunityItems Opp
        LEFT JOIN OpportunityMaster OM ON Opp.numOppID = oM.numOppID
        JOIN item I ON Opp.numItemCode = i.numItemcode
        LEFT JOIN WareHouseItems WItems ON WItems.numWareHouseItemID = Opp.numWarehouseItmsID
        LEFT JOIN Warehouses W ON W.numWarehouseID = WItems.numWarehouseID
        LEFT JOIN WarehouseLocation WL ON WL.numWLocationID = WItems.numWLocationID
        LEFT JOIN OpportunityMaster M ON Opp.numSourceID = M.numOppID
        LEFT JOIN UOM u ON u.numUOMId = opp.numUOMId
WHERE   om.numDomainId = '+ CONVERT(VARCHAR(10),@numDomainId) + '
        AND Opp.numOppId = '+ CONVERT(VARCHAR(15),@OpportunityId) +'
ORDER BY numoppitemtCode ASC '
  
PRINT @strSQL;
EXEC (@strSQL);
SELECT  vcSerialNo,
       vcComments AS Comments,
        numOppItemID AS numoppitemtCode,
        W.numWarehouseItmsDTLID,
        numWarehouseItmsID AS numWItmsID,
        dbo.Fn_getattributes(W.numWarehouseItmsDTLID,
                             1) Attributes
FROM    WareHouseItmsDTL W
        JOIN OppWarehouseSerializedItem O ON O.numWarehouseItmsDTLID = W.numWareHouseItmsDTLID
WHERE   numOppID = @OpportunityId


CREATE TABLE #tempForm (tintOrder TINYINT,vcDbColumnName nvarchar(50),vcOrigDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),
  vcListItemType varchar(3),numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,
  numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,bitIsAlphaNumeric BIT,bitIsNumeric BIT,
  bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),ListRelID numeric(9),intColumnWidth int,intVisible int,vcFieldDataType CHAR(1))


IF
	(
		SELECT 
			COUNT(*)
		FROM 
			View_DynamicColumns DC 
		WHERE 
			DC.numFormId=26 and DC.numDomainID=@numDomainID AND numUserCntID = @numUserCntID AND
			ISNULL(DC.bitSettingField,0)=1 AND ISNULL(DC.bitDeleted,0)=0 AND ISNULL(DC.bitCustom,0)=0) > 0
	OR
	(
		SELECT 
			COUNT(*)
		FROM 
			View_DynamicCustomColumns
		WHERE 
			numFormId=26 AND numUserCntID=@numUserCntID AND numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=0 AND ISNULL(bitCustom,0)=1
	) > 0
BEGIN	

INSERT INTO #tempForm
select isnull(DC.tintRow,0)+1 as tintOrder,DDF.vcDbColumnName,DDF.vcOrigDbColumnName,ISNULL(DDF.vcCultureFieldName,DDF.vcFieldName),                  
 DDF.vcAssociatedControlType,DDF.vcListItemType,DDF.numListID,DDF.vcLookBackTableName                                                 
,DDF.bitCustom,DDF.numFieldId,DDF.bitAllowSorting,DDF.bitInlineEdit,
DDF.bitIsRequired,DDF.bitIsEmail,DDF.bitIsAlphaNumeric,DDF.bitIsNumeric,DDF.bitIsLengthValidation,
DDF.intMaxLength,DDF.intMinLength,DDF.bitFieldMessage,DDF.vcFieldMessage vcFieldMessage,DDF.ListRelID,DDF.intColumnWidth,Case when DC.numFieldID is null then 0 else 1 end,DDF.vcFieldDataType
 FROM View_DynamicDefaultColumns DDF left join View_DynamicColumns DC on DC.numFormId=26 and DDF.numFieldId=DC.numFieldId and DC.numUserCntID=@numUserCntID and DC.numDomainID=@numDomainID and numRelCntType=0 AND tintPageType=1 
 where DDF.numFormId=26 and DDF.numDomainID=@numDomainID AND ISNULL(DDF.bitSettingField,0)=1 AND ISNULL(DDF.bitDeleted,0)=0 AND ISNULL(DDF.bitCustom,0)=0
       
 UNION
    
    select tintRow+1 as tintOrder,vcDbColumnName,vcFieldName,vcFieldName,                  
 vcAssociatedControlType,'' as vcListItemType,numListID,''                                                 
,bitCustom,numFieldId,bitAllowSorting,0 as bitAllowEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,''
 from View_DynamicCustomColumns
 where numFormId=26 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=0
 AND ISNULL(bitCustom,0)=1

END
ELSE
BEGIN
	--Check if Master Form Configuration is created by administrator if NoColumns=0
	DECLARE @IsMasterConfAvailable BIT = 0
	DECLARE @numUserGroup NUMERIC(18,0)

	SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

	IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = 26 AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
	BEGIN
		SET @IsMasterConfAvailable = 1
	END

	--If MasterConfiguration is available then load it otherwise load default columns
	IF @IsMasterConfAvailable = 1
	BEGIN
		INSERT INTO #tempForm
		SELECT 
			(intRowNum + 1) as tintOrder, vcDbColumnName, vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
			vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName,bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,
			bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth,1,vcFieldDataType
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=26 AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcFieldName,vcFieldName,                  
			 vcAssociatedControlType,'' as vcListItemType,numListID,''                                                 
			,bitCustom,numFieldId,bitAllowSorting,0 as bitAllowEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,''
		FROM 
			View_DynamicCustomColumnsMasterConfig
		WHERE 
			View_DynamicCustomColumnsMasterConfig.numFormId=26 AND 
			View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1
		ORDER BY 
			tintOrder ASC  
	END
END


select * from #tempForm order by tintOrder

drop table #tempForm

            END
            
    END
/****** Object:  StoredProcedure [dbo].[USP_OppManage]    Script Date: 07/26/2008 16:20:19 ******/
--2 changes
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OppManage')
DROP PROCEDURE usp_oppmanage
GO
CREATE PROCEDURE [dbo].[USP_OppManage] 
(                                                                          
  @numOppID as numeric=0 OUTPUT,                                                                          
  @numContactId numeric(9)=null,                                                                          
  @numDivisionId numeric(9)=null,                                                                          
  @tintSource numeric(9)=null,                                                                          
  @vcPOppName Varchar(100)='' OUTPUT,                                                                          
  @Comments varchar(1000)='',                                                                          
  @bitPublicFlag bit=0,                                                                          
  @numUserCntID numeric(9)=0,                                                                                 
  @monPAmount money =0,                                                 
  @numAssignedTo as numeric(9)=0,                                                                                                        
  @numDomainId numeric(9)=0,                                                                                                                                                                                   
  @strItems as text=null,                                                                          
  @strMilestone as text=null,                                                                          
  @dtEstimatedCloseDate datetime,                                                                          
  @CampaignID as numeric(9)=null,                                                                          
  @lngPConclAnalysis as numeric(9)=null,                                                                         
  @tintOppType as tinyint,                                                                                                                                             
  @tintActive as tinyint,                                                              
  @numSalesOrPurType as numeric(9),              
  @numRecurringId as numeric(9),
  @numCurrencyID as numeric(9)=0,
  @DealStatus as tinyint =0,-- 0=Open,1=Won,2=Lost
  @numStatus AS NUMERIC(9),
  @vcOppRefOrderNo VARCHAR(100),
  @numBillAddressId numeric(9)=0,
  @numShipAddressId numeric(9)=0,
  @bitStockTransfer BIT=0,
  @WebApiId INT = 0,
  @tintSourceType TINYINT=0,
  @bitDiscountType as bit,
  @fltDiscount  as float,
  @bitBillingTerms as bit,
  @intBillingDays as integer,
  @bitInterestType as bit,
  @fltInterest as float,
  @tintTaxOperator AS TINYINT,
  @numDiscountAcntType AS NUMERIC(9),
  @vcWebApiOrderNo VARCHAR(100)=NULL,
  @vcCouponCode		VARCHAR(100) = '',
  @vcMarketplaceOrderID VARCHAR(100)=NULL,
  @vcMarketplaceOrderDate  datetime=NULL,
  @vcMarketplaceOrderReportId VARCHAR(100)=NULL,
  @numPercentageComplete NUMERIC(9),
  @bitUseShippersAccountNo BIT = 0,
  @bitUseMarkupShippingRate BIT = 0,
  @numMarkupShippingRate NUMERIC(19,2) = 0,
  @intUsedShippingCompany INT = 0,
  @numShipmentMethod NUMERIC(18,0)=0
)                                                                          
                                                                          
as  
BEGIN TRY
BEGIN TRANSACTION
	DECLARE @fltExchangeRate float                                 
	DECLARE @hDocItem int                                                                                                                            
	DECLARE @TotalAmount as money                                                                          
	DECLARE @tintOppStatus as tinyint               

	--IF NEW OPPERTUNITY
	IF @numOppID = 0                                                                          
	BEGIN
		DECLARE @intOppTcode AS NUMERIC(9)  
  
		SELECT @intOppTcode = MAX(numOppId) FROM OpportunityMaster                

		SET @intOppTcode = ISNULL(@intOppTcode,0) + 1                                                
		
		SET @vcPOppName = @vcPOppName + '-'+ CONVERT(varchar(4),YEAR(GETUTCDATE()))  +'-A'+ CONVERT(varchar(10),@intOppTcode)                                  
  
		IF ISNULL(@numCurrencyID,0) = 0 
		BEGIN
			SET @fltExchangeRate=1
			SELECT @numCurrencyID= ISNULL(numCurrencyID,0) FROM dbo.Domain WHERE numDomainId=@numDomainId	
		END
		ELSE
		BEGIN 
			SET @fltExchangeRate = dbo.GetExchangeRate(@numDomainID,@numCurrencyID)
        END  
		                                                        
		-- GET ACCOUNT CLASS IF ENABLED
		DECLARE @numAccountClass AS NUMERIC(18) = 0

		DECLARE @tintDefaultClassType AS INT = 0
		SELECT @tintDefaultClassType = ISNULL(tintDefaultClassType,0) FROM Domain WHERE numDomainId = @numDomainID 

		IF @tintDefaultClassType = 1 --USER
		BEGIN
			SELECT 
				@numAccountClass=ISNULL(numDefaultClass,0) 
			FROM 
				dbo.UserMaster UM 
			JOIN 
				dbo.Domain D 
			ON 
				UM.numDomainID=D.numDomainId
			WHERE 
				D.numDomainId=@numDomainId 
				AND UM.numUserDetailId=@numUserCntID
		END
		ELSE IF @tintDefaultClassType = 2 --COMPANY
		BEGIN
			SELECT 
				@numAccountClass=ISNULL(numAccountClassID,0) 
			FROM 
				dbo.DivisionMaster DM 
			WHERE 
				DM.numDomainId=@numDomainId 
				AND DM.numDivisionID=@numDivisionID
		END
		ELSE
		BEGIN
			SET @numAccountClass = 0
		END
                                                                       
		INSERT INTO OpportunityMaster
		(                                                                             
			numContactId,numDivisionId,txtComments,numCampainID,bitPublicFlag,tintSource,tintSourceType,vcPOppName,                                                                          
			intPEstimatedCloseDate,monPAmount,numCreatedBy,bintCreatedDate,numModifiedBy,bintModifiedDate,numDomainId,                                                                                                             
			numRecOwner,lngPConclAnalysis,tintOppType,numSalesOrPurType,numCurrencyID,fltExchangeRate,numAssignedTo,
			numAssignedBy,[tintOppStatus],numStatus,vcOppRefOrderNo,bitStockTransfer,bitBillingTerms,intBillingDays,
			bitInterestType,fltInterest,vcCouponCode,bitDiscountType,fltDiscount,bitPPVariance,vcMarketplaceOrderID,
			vcMarketplaceOrderReportId,numPercentageComplete,numAccountClass,tintTaxOperator,bitUseShippersAccountNo,
			bitUseMarkupShippingRate,numMarkupShippingRate,intUsedShippingCompany,numShipmentMethod
		)                                                                          
		Values                                                                          
		(                                                                          
			@numContactId,@numDivisionId,ISNULL(@Comments,''),@CampaignID,@bitPublicFlag,@tintSource,@tintSourceType,@vcPOppName,                                                                          
			@dtEstimatedCloseDate,@monPAmount,@numUserCntID,(CASE @tintSourceType WHEN 3 THEN @vcMarketplaceOrderDate ELSE GETUTCDATE() END),                                                                          
			@numUserCntID,GETUTCDATE(),@numDomainId,@numUserCntID,@lngPConclAnalysis,@tintOppType,@numSalesOrPurType,@numCurrencyID, 
			@fltExchangeRate,(CASE WHEN @numAssignedTo > 0 THEN @numAssignedTo ELSE NULL END),(CASE WHEN @numAssignedTo>0 THEN @numUserCntID ELSE NULL END),
			@DealStatus,@numStatus,@vcOppRefOrderNo,@bitStockTransfer,@bitBillingTerms,@intBillingDays,@bitInterestType,@fltInterest,
			@vcCouponCode,@bitDiscountType,@fltDiscount,1,@vcMarketplaceOrderID,@vcMarketplaceOrderReportId,@numPercentageComplete,
			@numAccountClass,@tintTaxOperator,@bitUseShippersAccountNo,@bitUseMarkupShippingRate,@numMarkupShippingRate,@intUsedShippingCompany,@numShipmentMethod
		)                                                                                                                      
		
		SET @numOppID=SCOPE_IDENTITY()                                                
  
		--UPDATE OPPNAME AS PER NAME TEMPLATE
		EXEC dbo.USP_UpdateNameTemplateValue @tintOppType,@numDomainID,@numOppID

		--MAP CUSTOM FIELD	
		DECLARE @tintPageID AS TINYINT;
		SET @tintPageID = CASE WHEN @tintOppType=1 THEN 2 ELSE 6 END 
  	
		EXEC dbo.USP_AddParentChildCustomFieldMap
				@numDomainID = @numDomainID, --  numeric(18, 0)
				@numRecordID = @numOppID, --  numeric(18, 0)
				@numParentRecId = @numDivisionId, --  numeric(18, 0)
				@tintPageID = @tintPageID --  tinyint
 	
		IF ISNULL(@numStatus,0) > 0 AND @tintOppType=1 AND isnull(@DealStatus,0)=1
		BEGIN
	   		EXEC USP_ManageOpportunityAutomationQueue
					@numOppQueueID = 0, -- numeric(18, 0)
					@numDomainID = @numDomainID, -- numeric(18, 0)
					@numOppId = @numOppID, -- numeric(18, 0)
					@numOppBizDocsId = 0, -- numeric(18, 0)
					@numOrderStatus = @numStatus, -- numeric(18, 0)
					@numUserCntID = @numUserCntID, -- numeric(18, 0)
					@tintProcessStatus = 1, -- tinyint
					@tintMode = 1 -- TINYINT
		END
  
		-- INSERTING ITEMS                                                                                                                    
		IF CONVERT(VARCHAR(10),@strItems) <> '' AND @numOppID > 0
		BEGIN
			EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems       
	
			INSERT INTO OpportunityItems
			(
				numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,bitDropShip,bitDiscountType,fltDiscount,
				monTotAmtBefDiscount,vcItemName,vcModelID,vcManufacturer,vcPathForTImage,monVendorCost,numUOMId,bitWorkOrder,numVendorWareHouse,
				numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,numClassID,numToWarehouseItemID,vcAttributes,vcAttrValues,monAvgCost,bitItemPriceApprovalRequired
			)
			SELECT 
				@numOppID,X.numItemCode,dbo.fn_UOMConversion(X.numUOM,numItemCode,@numDomainId,null) * x.numUnitHour,x.monPrice,x.monTotAmount,X.vcItemDesc,
				(CASE X.numWarehouseItmsID 
				WHEN -1 
				THEN 
					CASE 
						WHEN ((SELECT COUNT(*) FROM Item WHERE numDomainID=@numDomainId AND vcSKU= X.vcSKU) > 0)
							THEN
								(SELECT TOP 1 
									numWareHouseItemID 
								FROM 
									WareHouseItems 
								WHERE 
									[numItemID] = X.numItemCode 
									AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId))
						ELSE
							(SELECT 
								[numWareHouseItemID] 
							FROM 
								[WareHouseItems] 
							WHERE 
								[numItemID] = X.numItemCode 
								AND ISNULL(WareHouseItems.vcWHSKU,'') = ISNULL(X.vcSKU,'')
								AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId)) 
					END
				WHEN 0 
				THEN 
					CASE 
					WHEN ((SELECT COUNT(*) FROM Item WHERE numDomainID=@numDomainId AND vcSKU=X.vcSKU) > 0)
						THEN
							(SELECT TOP 1 
								numWareHouseItemID 
							FROM 
								WareHouseItems 
							WHERE 
								[numItemID] = X.numItemCode 
								AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId))
					ELSE
						(SELECT 
							[numWareHouseItemID] 
						FROM 
							[WareHouseItems] 
						WHERE 
							[numItemID] = X.numItemCode 
							AND ISNULL(WareHouseItems.vcWHSKU,'') = ISNULL(X.vcSKU,'')
							AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId)) 
					END
				ELSE  
					X.numWarehouseItmsID 
				END) AS numWarehouseItmsID,
				X.ItemType,X.DropShip,X.bitDiscountType,X.fltDiscount,X.monTotAmtBefDiscount,X.vcItemName,(SELECT vcModelID FROM item WHERE numItemCode = X.numItemCode),
				(SELECT vcManufacturer FROM item WHERE numItemCode = X.numItemCode),(SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = X.numItemCode AND BitDefault = 1 AND numdomainId = @numdomainId),
				(SELECT isnull(VN.monCost,0) FROM Vendor VN, Item IT WHERE IT.numItemCode=VN.numItemCode AND VN.numItemCode=X.numItemCode AND VN.numVendorID=IT.numVendorID),
				X.numUOM,X.bitWorkOrder,X.numVendorWareHouse,X.numShipmentMethod,X.numSOVendorId,X.numProjectID,X.numProjectStageID,
				0,
				X.numToWarehouseItemID,X.Attributes,X.AttributeIDs,(SELECT ISNULL(monAverageCost,0) FROM Item I WHERE I.numDomainId=@numDomainId AND I.numItemCode=X.numItemCode),
				X.bitItemPriceApprovalRequired 
			FROM
				(
					SELECT 
						*
					FROM 
						OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
					WITH                       
					(                                                                          
						numoppitemtCode NUMERIC(9),numItemCode NUMERIC(9), numUnitHour FLOAT, monPrice DECIMAL(30,16), monTotAmount MONEY,vcItemDesc VARCHAR(1000),                                    
						numWarehouseItmsID NUMERIC(9), ItemType VARCHAR(30), DropShip BIT, bitDiscountType BIT,	fltDiscount DECIMAL(30,16), monTotAmtBefDiscount MONEY,
						vcItemName VARCHAR(300),numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId NUMERIC(9),
						numProjectID numeric(9),numProjectStageID numeric(9),numToWarehouseItemID NUMERIC(9),Attributes varchar(500),AttributeIDs VARCHAR(500),vcSKU VARCHAR(100),bitItemPriceApprovalRequired BIT
					)
				)X    
			ORDER BY 
				X.numoppitemtCode
    
			UPDATE 
				OpportunityItems                       
			SET 
				numItemCode=X.numItemCode,numOppId=@numOppID,numUnitHour=dbo.fn_UOMConversion(X.numUOM,X.numItemCode,@numDomainId,null) * X.numUnitHour,                      
				monPrice=x.monPrice,monTotAmount=x.monTotAmount,vcItemDesc=X.vcItemDesc,numWarehouseItmsID=X.numWarehouseItmsID,bitDropShip=X.DropShip,
				bitDiscountType=X.bitDiscountType,fltDiscount=X.fltDiscount,monTotAmtBefDiscount=X.monTotAmtBefDiscount,vcItemName=X.vcItemName,numUOMId=numUOM,
				bitWorkOrder=X.bitWorkOrder,numVendorWareHouse=X.numVendorWareHouse,numShipmentMethod=X.numShipmentMethod,numSOVendorId=X.numSOVendorId,
				numProjectId=X.numProjectID,numProjectStageId=X.numProjectStageID,numToWarehouseItemID = X.numToWarehouseItemID,vcAttributes=X.Attributes,
				numClassID=0,
				bitItemPriceApprovalRequired = X.bitItemPriceApprovalRequired,vcAttrValues=X.AttributeIDs
			FROM 
			(
				SELECT 
					numoppitemtCode as numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,DropShip,bitDiscountType,fltDiscount,
					monTotAmtBefDiscount,vcItemName,numUOM,bitWorkOrder,numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,numToWarehouseItemID,
					Attributes,bitItemPriceApprovalRequired,AttributeIDs
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=2]',2)
				WITH  
					(                      
						numoppitemtCode NUMERIC(9),numItemCode NUMERIC(9),numUnitHour FLOAT,monPrice DECIMAL(30,16),monTotAmount MONEY,vcItemDesc VARCHAR(1000),                                    
						numWarehouseItmsID NUMERIC(9),DropShip BIT,bitDiscountType BIT,fltDiscount DECIMAL(30,16),monTotAmtBefDiscount MONEY,vcItemName VARCHAR(300),
						numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId NUMERIC(9),numProjectID NUMERIC(9),
						numProjectStageID numeric(9),numToWarehouseItemID NUMERIC(9),Attributes varchar(500),bitItemPriceApprovalRequired BIT,AttributeIDs VARCHAR(500)
					)
			)X 
			WHERE 
				numoppitemtCode=X.numOppItemID   
				
			--INSERT KIT ITEMS                 
			INSERT INTO OpportunityKitItems
			(
				numOppId,
				numOppItemID,
				numChildItemID,
				numWareHouseItemId,
				numQtyItemsReq,
				numQtyItemsReq_Orig,
				numUOMId,
				numQtyShipped
			)
			SELECT
				@numOppId,
				OI.numoppitemtCode,
				ID.numChildItemID,
				ID.numWareHouseItemId,
				(ID.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(ID.numUOMId,ID.numChildItemID,@numDomainID,I.numBaseUnit),1)) * OI.numUnitHour,
				ID.numQtyItemsReq,
				ID.numUOMId,
				0
			FROM 
				OpportunityItems OI 
			JOIN 
				ItemDetails ID 
			ON 
				OI.numItemCode=ID.numItemKitID 
			JOIN
				Item I
			ON
				ID.numChildItemID = I.numItemCode
			WHERE 
				OI.numOppId=@numOppId 

			IF (SELECT COUNT(*) FROM OPENXML (@hDocItem,'/NewDataSet/Item[bitHasKitAsChild=''true'']',2)) > 0
			BEGIN
				--INSERT KIT ITEM HAS ANOTHER KIT AS CHILD THEN SAVE CONFIGURATION OF KIT
				DECLARE @TempKitConfiguration TABLE
				(
					numItemCode NUMERIC(18,0),
					numWarehouseItemID NUMERIC(18,0),
					KitChildItems VARCHAR(MAX)
				)

				INSERT INTO 
					@TempKitConfiguration
				SELECT
					numItemCode,
					numWarehouseItmsID,
					KitChildItems
				FROM
					OPENXML (@hDocItem,'/NewDataSet/Item[bitHasKitAsChild=''true'']',2)
				WITH                       
				(
					numItemCode NUMERIC(9),numWarehouseItmsID NUMERIC(9), KitChildItems VARCHAR(MAX)
				) 

				INSERT INTO OpportunityKitChildItems
				(
					numOppID,
					numOppItemID,
					numOppChildItemID,
					numItemID,
					numWareHouseItemId,
					numQtyItemsReq,
					numQtyItemsReq_Orig,
					numUOMId,
					numQtyShipped
				)
				SELECT 
					@numOppID,
					OKI.numOppItemID,
					OKI.numOppChildItemID,
					ItemDetails.numChildItemID,
					ItemDetails.numWareHouseItemId,
					(ItemDetails.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(ItemDetails.numUOMId,ItemDetails.numChildItemID,@numDomainID,Item.numBaseUnit),1)) * OKI.numQtyItemsReq,
					ItemDetails.numQtyItemsReq,
					ItemDetails.numUOMId,
					0
				FROM
					(
						SELECT 
							t1.numItemCode,
							t1.numWarehouseItemID,
							SUBSTRING(
										SUBSTRING(items,0,CHARINDEX('-',items)),
										0,
										CHARINDEX('#',SUBSTRING(items,0,CHARINDEX('-',items)))
									  ) AS numKitItemID,
							SUBSTRING(
										SUBSTRING(items,0,CHARINDEX('-',items)),
										CHARINDEX('#',SUBSTRING(items,0,CHARINDEX('-',items))) + 1, 
										LEN(SUBSTRING(items,0,CHARINDEX('-',items))) - CHARINDEX('#',SUBSTRING(items,0,CHARINDEX('-',items)))
									  ) AS numKitWarehouseItemID,
							SUBSTRING(
										SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)), 
										0, 
										CHARINDEX('#',SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)))
									  ) As numKitChildItemID,
							SUBSTRING(
										SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)), 
										CHARINDEX('#',SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items))) + 1, 
										LEN(SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items))) - CHARINDEX('#',SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)))
									  ) As numKitChildItemWarehouseItemID
						FROM 
							@TempKitConfiguration t1
						CROSS APPLY
						(
							SELECT
								*
							FROM 
								dbo.Split((SELECT KitChildItems FROM @TempKitConfiguration WHERE numItemCode=t1.numItemCode AND numWarehouseItemID=t1.numWarehouseItemID),',') 
						) as t2
					) TempChildItems
				INNER JOIN
					OpportunityKitItems OKI
				ON
					OKI.numOppId=@numOppId
					AND OKI.numChildItemID=TempChildItems.numKitItemID
					AND OKI.numWarehouseItemId=TempChildItems.numKitWarehouseItemID
				INNER JOIN
					OpportunityItems OI
				ON
					OI.numItemCode = TempChildItems.numItemCode
					AND OI.numWarehouseItmsID = TempChildItems.numWarehouseItemID
					AND OI.numoppitemtcode = OKI.numOppItemID
				INNER JOIN
					ItemDetails
				ON
					TempChildItems.numKitItemID = ItemDetails.numItemKitID
					AND TempChildItems.numKitChildItemID = ItemDetails.numChildItemID
					AND (TempChildItems.numKitChildItemWarehouseItemID = ItemDetails.numWareHouseItemId  OR ISNULL(TempChildItems.numKitChildItemWarehouseItemID,0) = 0)
				INNER JOIN
					Item 
				ON
					ItemDetails.numChildItemID = Item.numItemCode
			END

			-- UPDATE UOM OF OPPORTUNITY ITEMS HAVE NOT UOM SET WHILE EXPORTED FROM MARKETPLACE (MEANS TINTSOURCETYPE = 3)
			IF ISNULL(@tintSourceType,0) = 3
			BEGIN
				UPDATE 
					OI 
				SET 
					numUOMID = ISNULL(I.numSaleUnit,0), 
					numUnitHour = dbo.fn_UOMConversion(ISNULL(I.numSaleUnit,0),OI.numItemCode,@numDomainId,null) * OI.numUnitHour
				FROM 
					OpportunityItems OI
				JOIN 
					Item I 
				ON 
					OI.numItemCode = I.numItemCode
				WHERE 
					numOppId = @numOppID
					AND I.numDomainID = @numDomainId
			END	
                                     
			INSERT INTO OppWarehouseSerializedItem
			(
				numOppID,
				numWarehouseItmsDTLID,
				numOppItemID,
				numWarehouseItmsID
			)                      
			SELECT 
				@numOppID,
				X.numWarehouseItmsDTLID,
				X.numoppitemtCode,
				X.numWItmsID 
			FROM
			(                                                                          
				SELECT
					* 
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/SerialNo',2)                                                                          
				WITH                        
				(                                               
				numWarehouseItmsDTLID numeric(9),                      
				numoppitemtCode numeric(9),                      
				numWItmsID numeric(9)                                                     
				)
			)X                                    
                                   
			UPDATE 
				OppWarehouseSerializedItem                       
			SET 
				numOppItemID=X.numoppitemtCode                                
			FROM                       
			(
				SELECT 
					numWarehouseItmsDTLID AS DTLID,
					O.numoppitemtCode 
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/SerialNo',2)                                                                         
				WITH
				(
					numWarehouseItmsDTLID NUMERIC(9),numoppitemtCode NUMERIC(9),numWItmsID NUMERIC(9)
				)                                
				JOIN 
					OpportunityItems O 
				ON 
					O.numWarehouseItmsID=numWItmsID and O.numOppID=@numOppID
			)X                 
			WHERE 
				numOppID=@numOppID 
				AND numWarehouseItmsDTLID=X.DTLID      
    
			EXEC USP_ManageSOWorkOrder @numDomainId,@numOppID,@strItems,@numUserCntID
                     
			EXEC sp_xml_removedocument @hDocItem                                     
		END          
   
		SET @TotalAmount=0                                                           
		SELECT @TotalAmount = SUM(monTotAmount) FROM OpportunityItems WHERE numOppId=@numOppID                                                                          
  
		UPDATE OpportunityMaster SET monPamount=@TotalAmount WHERE numOppId=@numOppID                                                 
                      
		--INSERT TAX FOR DIVISION   
		IF @tintOppType=1 OR (@tintOppType=2 and (select isnull(bitPurchaseTaxCredit,0) from Domain where numDomainId=@numDomainId)=1)
		BEGIN
			INSERT INTO dbo.OpportunityMasterTaxItems 
			(
				numOppId,
				numTaxItemID,
				fltPercentage,
				tintTaxType,
				numTaxID
			) 
			SELECT 
				@numOppID,
				TI.numTaxItemID,
				TEMPTax.decTaxValue,
				TEMPTax.tintTaxType,
				0
			FROM 
				TaxItems TI 
			JOIN 
				DivisionTaxTypes DTT 
			ON 
				TI.numTaxItemID = DTT.numTaxItemID
			OUTER APPLY
			(
				SELECT
					decTaxValue,
					tintTaxType
				FROM
					dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,TI.numTaxItemID,@numOppId,0,NULL)
			) AS TEMPTax
			WHERE 
				DTT.numDivisionID=@numDivisionId 
				AND DTT.bitApplicable=1
			UNION 
			SELECT 
				@numOppID,
				0,
				TEMPTax.decTaxValue,
				TEMPTax.tintTaxType,
				0
			FROM 
				dbo.DivisionMaster 
			OUTER APPLY
			(
				SELECT
					decTaxValue,
					tintTaxType
				FROM
					dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,0,@numOppId,1,NULL)
			) AS TEMPTax
			WHERE 
				bitNoTax=0 
				AND numDivisionID=@numDivisionID			
		END	
	END
	ELSE                                                                                                                          
	BEGIN
		DECLARE @tempAssignedTo AS NUMERIC(9) = NULL                                                    
		SELECT 
			@tintOppStatus = tintOppStatus,
			@tempAssignedTo=ISNULL(numAssignedTo,0) 
		FROM   
			OpportunityMaster 
		WHERE  
			numOppID = @numOppID

		IF @tempAssignedTo <> @numAssignedTo
		BEGIN
			IF (SELECT COUNT(*) FROM BizDocComission WHERE ISNULL(numOppID,0) = @numOppID AND ISNULL(bitCommisionPaid,0) = 1) > 0
			BEGIN
				RAISERROR('COMMISSION_PAID_AGAINST_BIZDOC',16,1)
			END
		END
 
		--REVERTING BACK THE WAREHOUSE ITEMS                  
		IF @tintOppStatus = 1 
		BEGIN               
			EXEC USP_RevertDetailsOpp @numOppID,0,@numUserCntID                  
		END                  

		IF LEN(ISNULL(@vcOppRefOrderNo,''))>0
		BEGIN
			UPDATE dbo.OpportunityBizDocs SET vcRefOrderNo=@vcOppRefOrderNo WHERE numOppId=@numOppID
		END
		
		IF EXISTS (SELECT 1 FROM dbo.OpportunityMaster WHERE numOppId = @numOppID AND tintOppType=1 AND isnull(tintOppStatus,0)=1 AND ISNULL(numStatus,0)!= @numStatus)
		BEGIN
			EXEC USP_ManageOpportunityAutomationQueue
					@numOppQueueID = 0, -- numeric(18, 0)
					@numDomainID = @numDomainID, -- numeric(18, 0)
					@numOppId = @numOppID, -- numeric(18, 0)
					@numOppBizDocsId = 0, -- numeric(18, 0)
					@numOrderStatus = @numStatus, -- numeric(18, 0)
					@numUserCntID = @numUserCntID, -- numeric(18, 0)
					@tintProcessStatus = 1, -- tinyint
					@tintMode = 1 -- TINYINT
		END 
						
		UPDATE   
			OpportunityMaster
		SET      
			vcPOppName = @vcPOppName,txtComments = ISNULL(@Comments,''),bitPublicFlag = @bitPublicFlag,numCampainID = @CampaignID,tintSource = @tintSource,tintSourceType=@tintSourceType,
			intPEstimatedCloseDate = @dtEstimatedCloseDate,numModifiedBy = @numUserCntID,bintModifiedDate = GETUTCDATE(),lngPConclAnalysis = @lngPConclAnalysis,monPAmount = @monPAmount,
			tintActive = @tintActive,numSalesOrPurType = @numSalesOrPurType,numStatus = @numStatus,tintOppStatus = @DealStatus,vcOppRefOrderNo=@vcOppRefOrderNo,
			bintOppToOrder=(CASE WHEN @tintOppStatus=0 AND @DealStatus=1 THEN GETUTCDATE() ELSE bintOppToOrder END),bitDiscountType=@bitDiscountType,fltDiscount=@fltDiscount,
			bitBillingTerms=@bitBillingTerms,intBillingDays=@intBillingDays,bitInterestType=@bitInterestType,fltInterest=@fltInterest,tintTaxOperator=@tintTaxOperator,
			numDiscountAcntType=@numDiscountAcntType,vcCouponCode = @vcCouponCode,numPercentageComplete =@numPercentageComplete,bitUseShippersAccountNo = @bitUseShippersAccountNo,
			bitUseMarkupShippingRate = @bitUseMarkupShippingRate,numMarkupShippingRate = @numMarkupShippingRate,intUsedShippingCompany = @intUsedShippingCompany, numContactID=@numContactId,@numShipmentMethod=@numShipmentMethod
		WHERE
			numOppId = @numOppID   
	   
		-- UPDATING IF ORGANIZATION IS ASSIGNED TO SOMEONE                                                      
		IF ( @tempAssignedTo <> @numAssignedTo AND @numAssignedTo <> '0' ) 
		BEGIN
			UPDATE
				OpportunityMaster 
			SET	
				numAssignedTo = @numAssignedTo, 
				numAssignedBy = @numUserCntID 
			WHERE 
				numOppId = @numOppID                                                    
		END                                                     
		ELSE IF(@numAssignedTo = 0) 
		BEGIN
			UPDATE 
				OpportunityMaster 
			SET 
				numAssignedTo = 0, 
				numAssignedBy = 0 
			WHERE
				numOppId = @numOppID
		END  
			                                                                    
		-- UPDATING OPP ITEMS
		IF CONVERT(VARCHAR(10),@strItems) <> '' AND @numOppID > 0                                                                         
		BEGIN
			EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems                                      
			   
			--DELETE CHILD ITEMS OF KIT WITHIN KIT
			DELETE FROM 
				OpportunityKitChildItems 
			WHERE 
				numOppID=@numOppID AND 
				numOppItemID NOT IN (SELECT ISNULL(numoppitemtCode,0) FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) WITH (numoppitemtCode NUMERIC(9)))   

			--DELETE ITEMS
			DELETE FROM 
				OpportunityKitItems 
			WHERE 
				numOppID=@numOppID AND 
				numOppItemID NOT IN (SELECT ISNULL(numoppitemtCode,0) FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) WITH (numoppitemtCode NUMERIC(9)))                      
			                            
			DELETE FROM 
				OpportunityItems 
			WHERE 
				numOppID=@numOppID 
				AND numoppitemtCode NOT IN (SELECT numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) WITH (numoppitemtCode NUMERIC(9)))   
	          
			--INSERT NEW ADDED ITEMS                        
			INSERT INTO OpportunityItems
			(
				numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,
				[vcItemName],[vcModelID],[vcManufacturer],vcPathForTImage,monVendorCost,numUOMId,bitWorkOrder,numVendorWareHouse,numToWarehouseItemID,numShipmentMethod,
				numSOVendorId,numProjectID,numProjectStageID,vcAttributes,numClassID,monAvgCost,bitItemPriceApprovalRequired,vcAttrValues
			)
			SELECT 
				@numOppID,X.numItemCode,dbo.fn_UOMConversion(X.numUOM,numItemCode,@numDomainId,null) * x.numUnitHour,x.monPrice,x.monTotAmount,X.vcItemDesc,X.numWarehouseItmsID,
				X.ItemType,X.DropShip,X.bitDiscountType,X.fltDiscount,X.monTotAmtBefDiscount,X.vcItemName,(SELECT vcModelID FROM item WHERE numItemCode = X.numItemCode),
				(SELECT vcManufacturer FROM item WHERE numItemCode = X.numItemCode),
				(SELECT TOP 1 vcPathForTImage FROM dbo.ItemImages WHERE numItemCode = X.numItemCode AND BitDefault = 1 AND numDomainId = @numDomainId),
				(select isnull(VN.monCost,0) from Vendor VN, Item IT where IT.numItemCode=VN.numItemCode and  VN.numItemCode=X.numItemCode and  VN.numVendorID=IT.numVendorID),
				X.numUOM,X.bitWorkOrder,X.numVendorWareHouse,X.numToWarehouseItemID,X.numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,Attributes,
				0,
				(SELECT ISNULL(monAverageCost,0) FROM Item I WHERE I.numDomainId=@numDomainId AND I.numItemCode=X.numItemCode), X.bitItemPriceApprovalRequired, X.AttributeIDs
			FROM
			(
				SELECT 
					*
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
				WITH  
				(                      
					numoppitemtCode numeric(9),numItemCode numeric(9),numUnitHour FLOAT,monPrice DECIMAL(30,16),monTotAmount money,vcItemDesc varchar(1000),                                    
					numWarehouseItmsID numeric(9),numToWarehouseItemID numeric(9),Op_Flag tinyint,ItemType varchar(30),DropShip bit,bitDiscountType bit,
					fltDiscount DECIMAL(30,16),monTotAmtBefDiscount MONEY,vcItemName VARCHAR(300),numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),
					numShipmentMethod NUMERIC(9),numSOVendorId numeric(9),numProjectID numeric(9),numProjectStageID numeric(9),Attributes varchar(500), 
					bitItemPriceApprovalRequired BIT,AttributeIDs VARCHAR(500)
				)
			)X 
			
			-- UPDATE ITEMS
			UPDATE 
				OpportunityItems                       
			SET 
				numItemCode=X.numItemCode,numOppId=@numOppID,numUnitHour=dbo.fn_UOMConversion(X.numUOM,X.numItemCode,@numDomainId,null) * x.numUnitHour,                      
				monPrice=x.monPrice,monTotAmount=x.monTotAmount,vcItemDesc=X.vcItemDesc,numWarehouseItmsID=X.numWarehouseItmsID,numToWarehouseItemID = X.numToWarehouseItemID,             
				bitDropShip=X.DropShip,bitDiscountType=X.bitDiscountType,fltDiscount=X.fltDiscount,monTotAmtBefDiscount=X.monTotAmtBefDiscount,vcItemName=X.vcItemName,
				numUOMId=numUOM,numVendorWareHouse=X.numVendorWareHouse,numShipmentMethod=X.numShipmentMethod,numSOVendorId=X.numSOVendorId,numProjectId=X.numProjectID,
				numProjectStageId=X.numProjectStageID,vcAttributes=X.Attributes,bitItemPriceApprovalRequired=X.bitItemPriceApprovalRequired,bitWorkOrder=X.bitWorkOrder,vcAttrValues=AttributeIDs
			FROM 
			(
				SELECT 
					numoppitemtCode as numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,numToWarehouseItemID,DropShip,bitDiscountType,
					fltDiscount,monTotAmtBefDiscount,vcItemName,numUOM,numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,Attributes,
					bitItemPriceApprovalRequired,bitWorkOrder,AttributeIDs
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=2]',2)                                                                
				WITH  
				(                      
					numoppitemtCode NUMERIC(9),numItemCode NUMERIC(9),numUnitHour FLOAT,monPrice DECIMAL(30,16),monTotAmount MONEY,vcItemDesc VARCHAR(1000),numWarehouseItmsID NUMERIC(9), 
					numToWarehouseItemID NUMERIC(18,0),DropShip BIT,bitDiscountType BIT,fltDiscount DECIMAL(30,16),monTotAmtBefDiscount MONEY,vcItemName VARCHAR(300),numUOM NUMERIC(9),
					numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId NUMERIC(9),numProjectID NUMERIC(9),numProjectStageID NUMERIC(9),Attributes VARCHAR(500),
					bitItemPriceApprovalRequired BIT, bitWorkOrder BIT,AttributeIDs VARCHAR(500)                                              
				)
			)X 
			WHERE 
				numoppitemtCode=X.numOppItemID 

			--INSERT NEW ADDED KIT ITEMS                 
			INSERT INTO OpportunityKitItems                                                                          
			(
				numOppId,
				numOppItemID,
				numChildItemID,
				numWareHouseItemId,
				numQtyItemsReq,
				numQtyItemsReq_Orig,
				numUOMId,
				numQtyShipped
			)
			SELECT
				@numOppId,
				OI.numoppitemtCode,
				ID.numChildItemID,
				ID.numWareHouseItemId,
				ID.numQtyItemsReq * OI.numUnitHour,
				ID.numQtyItemsReq,
				ID.numUOMId,
				0
			FROM 
				OpportunityItems OI 
			JOIN 
				ItemDetails ID 
			ON 
				OI.numItemCode=ID.numItemKitID 
			WHERE 
				OI.numOppId=@numOppId 
				AND OI.numoppitemtCode NOT IN (SELECT ISNULL(numOppItemID,0) FROM OpportunityKitItems OKI WHERE OKI.numOppId=@numOppID)  
				
			IF (SELECT COUNT(*) FROM OPENXML (@hDocItem,'/NewDataSet/Item[bitHasKitAsChild=''true'']',2)) > 0
			BEGIN
				--INSERT KIT ITEM HAS ANOTHER KIT AS CHILD THEN SAVE CONFIGURATION OF KIT
				DELETE FROM @TempKitConfiguration

				INSERT INTO 
					@TempKitConfiguration
				SELECT
					numItemCode,
					numWarehouseItmsID,
					KitChildItems
				FROM
					OPENXML (@hDocItem,'/NewDataSet/Item[bitHasKitAsChild=''true'']',2)
				WITH                       
				(
					numItemCode NUMERIC(9),numWarehouseItmsID NUMERIC(9), KitChildItems VARCHAR(MAX)
				) 

				INSERT INTO OpportunityKitChildItems
				(
					numOppID,
					numOppItemID,
					numOppChildItemID,
					numItemID,
					numWareHouseItemId,
					numQtyItemsReq,
					numQtyItemsReq_Orig,
					numUOMId,
					numQtyShipped
				)
				SELECT 
					@numOppID,
					OKI.numOppItemID,
					OKI.numOppChildItemID,
					ItemDetails.numChildItemID,
					ItemDetails.numWareHouseItemId,
					ItemDetails.numQtyItemsReq * OKI.numQtyItemsReq,
					ItemDetails.numQtyItemsReq,
					ItemDetails.numUOMId,
					0
				FROM
					(
						SELECT 
							t1.numItemCode,
							t1.numWarehouseItemID,
							SUBSTRING(
										SUBSTRING(items,0,CHARINDEX('-',items)),
										0,
										CHARINDEX('#',SUBSTRING(items,0,CHARINDEX('-',items)))
									  ) AS numKitItemID,
							SUBSTRING(
										SUBSTRING(items,0,CHARINDEX('-',items)),
										CHARINDEX('#',SUBSTRING(items,0,CHARINDEX('-',items))) + 1, 
										LEN(SUBSTRING(items,0,CHARINDEX('-',items))) - CHARINDEX('#',SUBSTRING(items,0,CHARINDEX('-',items)))
									  ) AS numKitWarehouseItemID,
							SUBSTRING(
										SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)), 
										0, 
										CHARINDEX('#',SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)))
									  ) As numKitChildItemID,
							SUBSTRING(
										SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)), 
										CHARINDEX('#',SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items))) + 1, 
										LEN(SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items))) - CHARINDEX('#',SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)))
									  ) As numKitChildItemWarehouseItemID
						FROM 
							@TempKitConfiguration t1
						CROSS APPLY
						(
							SELECT
								*
							FROM 
								dbo.Split((SELECT KitChildItems FROM @TempKitConfiguration WHERE numItemCode=t1.numItemCode AND numWarehouseItemID=t1.numWarehouseItemID),',') 
						) as t2
					) TempChildItems
				INNER JOIN
					OpportunityKitItems OKI
				ON
					OKI.numOppId=@numOppId
					AND OKI.numChildItemID=TempChildItems.numKitItemID
					AND OKI.numWarehouseItemId=TempChildItems.numKitWarehouseItemID
				INNER JOIN
					OpportunityItems OI
				ON
					OI.numItemCode = TempChildItems.numItemCode
					AND OI.numWarehouseItmsID = TempChildItems.numWarehouseItemID
					AND OI.numoppitemtcode = OKI.numOppItemID
				INNER JOIN
					ItemDetails
				ON
					TempChildItems.numKitItemID = ItemDetails.numItemKitID
					AND TempChildItems.numKitChildItemID = ItemDetails.numChildItemID
					AND (TempChildItems.numKitChildItemWarehouseItemID = ItemDetails.numWareHouseItemId  OR ISNULL(TempChildItems.numKitChildItemWarehouseItemID,0) = 0)
			END                                  
			
			--UPDATE KIT ITEMS                 
			UPDATE 
				OKI 
			SET 
				numQtyItemsReq = numQtyItemsReq_Orig * OI.numUnitHour
			FROM 
				OpportunityKitItems OKI 
			JOIN 
				OpportunityItems OI 
			ON 
				OKI.numOppItemID=OI.numoppitemtCode 
				AND OKI.numOppId=OI.numOppId  
			WHERE 
				OI.numOppId=@numOppId 

			-- UPDATE CHILD ITEMS OF KIT WITHIN KIT
			UPDATE
				OKCI
			SET
				OKCI.numQtyItemsReq = (OKI.numQtyItemsReq * OKCI.numQtyItemsReq_Orig)
			FROM
				OpportunityKitChildItems OKCI
			INNER JOIN
				OpportunityKitItems OKI
			ON
				OKCI.numOppID = @numOppId
				AND OKCI.numOppItemID = OKI.numOppItemID
				AND OKCI.numOppChildItemID = OKI.numOppChildItemID                     
			                                    
			EXEC USP_ManageSOWorkOrder @numDomainId,@numOppID,@strItems,@numUserCntID
			   		                                                                      
			EXEC sp_xml_removedocument @hDocItem                                               
		END     

		--FOLLOWING DELETE STATEMENT IS COMMENTED BECUASE NOW WE ARE NOT ALLOWING USER TO EIDT SERIAL/LOT ITEM UNTILL USER REMOVED ASSIGNED SERIAL/LOT NUMBERS
	    --Delete previously added serial lot number added for opportunity
		--DELETE FROM OppWarehouseSerializedItem WHERE numOppID = @numOppID                                                         
	END


/*Added by chintan - While creating SO from PO it wasn't puting Item Qty to Allocation. this segment was being called from USP_OppSaveMilestone which No longer in use after changes anoop did. */     
--Updating the warehouse items              
declare @tintShipped as tinyint               
select @tintOppStatus=tintOppStatus,@tintOppType=tintOppType,@tintShipped=tintShipped from OpportunityMaster where numOppID=@numOppID              
if @tintOppStatus=1               
	begin         
	PRINT 'inside USP_UpdatingInventoryonCloseDeal'
		exec USP_UpdatingInventoryonCloseDeal @numOppID,@numUserCntID
	end              

declare @tintCRMType as numeric(9)        
declare @AccountClosingDate as datetime        

select @numDivisionID=numDivisionID,@AccountClosingDate=bintAccountClosingDate from  OpportunityMaster where numOppID=@numOppID         
select @tintCRMType=tintCRMType from divisionmaster where numDivisionID =@numDivisionID 

--When deal is won                                         
if @DealStatus = 1 
begin           
	 if @AccountClosingDate is null                   
	  update OpportunityMaster set bintAccountClosingDate=getutcdate() where numOppID=@numOppID              
end         
--When Deal is Lost
else if @DealStatus = 2
begin         
	 select @AccountClosingDate=bintAccountClosingDate from  OpportunityMaster where numOppID=@numOppID         
	 if @AccountClosingDate is null                   
	  update OpportunityMaster set bintAccountClosingDate=getutcdate() where numOppID=@numOppID
end                    

/*Added by chintan -  Biz usually automatically promotes the record from Lead/Prospect to Account When a Sales Opp/Order is made against it*/  
-- Promote Lead to Prospect when Sales/Purchase Opp is created against it
if @tintCRMType=0 AND @DealStatus = 0 --Lead & Open Opp
begin        
	update divisionmaster set tintCRMType=1,bintLeadProm=getutcdate(),bintLeadPromBy=@numUserCntID,bintProsProm=getutcdate(),bintProsPromBy=@numUserCntID        
	where numDivisionID=@numDivisionID        
end        
-- Promote Lead to Account when Sales/Purchase Order is created against it
ELSE if @tintCRMType=0 AND @DealStatus = 1 --Lead & Order
begin        
	update divisionmaster set tintCRMType=2,bintLeadProm=getutcdate(),bintLeadPromBy=@numUserCntID,bintProsProm=getutcdate(),bintProsPromBy=@numUserCntID        
	where numDivisionID=@numDivisionID        
end
--Promote Prospect to Account
else if @tintCRMType=1 AND @DealStatus = 1 
begin        
	update divisionmaster set tintCRMType=2,bintProsProm=getutcdate(),bintProsPromBy=@numUserCntID        
	where numDivisionID=@numDivisionID        
end

--Add/Update Address Details
DECLARE @vcStreet  varchar(100),@vcCity  varchar(50),@vcPostalCode  varchar(15),@vcCompanyName  varchar(100),@vcAddressName VARCHAR(50)      
DECLARE @numState  numeric(9),@numCountry  numeric(9), @numCompanyId  numeric(9) 
DECLARE @bitIsPrimary BIT;

select  @vcCompanyName=vcCompanyName,@numCompanyId=div.numCompanyID from CompanyInfo Com                            
join divisionMaster Div                            
on div.numCompanyID=com.numCompanyID                            
where div.numdivisionID=@numDivisionId

--Bill Address
IF @numBillAddressId>0
BEGIN
 SELECT  @vcStreet=ISNULL(vcStreet,''),@vcCity=ISNULL(vcCity,''),@vcPostalCode=ISNULL(vcPostalCode,''),
		@numState=ISNULL(numState,0),@numCountry=ISNULL(numCountry,0),@bitIsPrimary=bitIsPrimary,
		@vcAddressName=vcAddressName
            FROM  dbo.AddressDetails WHERE   numDomainID = @numDomainID AND numAddressID = @numBillAddressId
  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 0, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = @vcCompanyName, --  varchar(100)
	@numCompanyId = 0, --  numeric(9, 0)
	@vcAddressName = @vcAddressName
END
  
  
  --Ship Address
IF @numShipAddressId>0
BEGIN
	SELECT  @vcStreet=ISNULL(vcStreet,''),@vcCity=ISNULL(vcCity,''),@vcPostalCode=ISNULL(vcPostalCode,''),
		@numState=ISNULL(numState,0),@numCountry=ISNULL(numCountry,0),@bitIsPrimary=bitIsPrimary,
		@vcAddressName=vcAddressName
            FROM  dbo.AddressDetails WHERE   numDomainID = @numDomainID AND numAddressID = @numShipAddressId
 



  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 1, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = @vcCompanyName, --  varchar(100)
	@numCompanyId =@numCompanyId, --  numeric(9, 0)
	@vcAddressName = @vcAddressName
END

--INSERT TAX FOR DIVISION   
IF @tintOppType=1 OR (@tintOppType=2 and (select isnull(bitPurchaseTaxCredit,0) from Domain where numDomainId=@numDomainId)=1)
BEGIN

-- DELETE ITEM LEVEL CRV TAX TYPES
DELETE FROM OpportunityMasterTaxItems WHERE numOppId=@numOppId AND numTaxItemID = 1

-- INSERT ITEM LEVEL CRV TAX TYPES
INSERT INTO OpportunityMasterTaxItems
(
	numOppId,
	numTaxItemID,
	fltPercentage,
	tintTaxType,
	numTaxID
) 
SELECT 
	OI.numOppId,
	1,
	TEMPTax.decTaxValue,
	TEMPTax.tintTaxType,
	TEMPTax.numTaxID
FROM 
	OpportunityItems OI
INNER JOIN
	ItemTax IT
ON
	OI.numItemCode = IT.numItemCode
OUTER APPLY
(
	SELECT
		decTaxValue,
		tintTaxType,
		numTaxID
	FROM
		dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,OI.numItemCode,IT.numTaxItemID,@numOppId,1,NULL)
) AS TEMPTax
WHERE
	OI.numOppId = @numOppID
	AND IT.numTaxItemID = 1 -- CRV TAX
GROUP BY
	OI.numOppId,
	TEMPTax.decTaxValue,
	TEMPTax.tintTaxType,
	TEMPTax.numTaxID

--Delete Tax for Opportunity Items if item deleted 
DELETE FROM OpportunityItemsTaxItems WHERE numOppId=@numOppId AND numOppItemID NOT IN (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID)

--Insert Tax for Opportunity Items
INSERT INTO dbo.OpportunityItemsTaxItems 
(
	numOppId,
	numOppItemID,
	numTaxItemID,
	numTaxID
) 
SELECT 
	@numOppId,
	OI.numoppitemtCode,
	TI.numTaxItemID,
	0
FROM 
	dbo.OpportunityItems OI 
JOIN 
	dbo.ItemTax IT 
ON 
	OI.numItemCode=IT.numItemCode 
JOIN
	TaxItems TI 
ON 
	TI.numTaxItemID = IT.numTaxItemID 
WHERE 
	OI.numOppId=@numOppID 
	AND IT.bitApplicable=1 
	AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
UNION
SELECT 
	@numOppId,
	OI.numoppitemtCode,
	0,
	0
FROM 
	dbo.OpportunityItems OI 
JOIN 
	dbo.Item I 
ON 
	OI.numItemCode=I.numItemCode
WHERE 
	OI.numOppId=@numOppID 
	AND I.bitTaxable=1 
	AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
UNION
SELECT
	@numOppId,
	OI.numoppitemtCode,
	1,
	TD.numTaxID
FROM
	dbo.OpportunityItems OI 
INNER JOIN
	ItemTax IT
ON
	IT.numItemCode = OI.numItemCode
INNER JOIN
	TaxDetails TD
ON
	TD.numTaxID = IT.numTaxID
	AND TD.numDomainId = @numDomainId
WHERE
	OI.numOppId = @numOppID
	AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)

END
  
 UPDATE OpportunityMaster SET monDealAmount=dbo.GetDealAmount(@numOppID,getdate(),0) WHERE numOppId=@numOppID
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SET @numOppID = 0

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_RecordHistory_FetchFromCDC')
DROP PROCEDURE dbo.USP_RecordHistory_FetchFromCDC
GO
CREATE PROCEDURE [dbo].[USP_RecordHistory_FetchFromCDC]

AS
BEGIN
	-- GET Opp/Order History
	EXEC USP_RecordHistory_FetchFromCDCOppOrder
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_RecordHistory_FetchFromCDCOppOrder')
DROP PROCEDURE dbo.USP_RecordHistory_FetchFromCDCOppOrder
GO
CREATE PROCEDURE [dbo].[USP_RecordHistory_FetchFromCDCOppOrder]

AS
BEGIN
	
	DECLARE @TEMPOpportunityHistory TABLE
	(
		vbStartlsn VARBINARY(128),
		numRecordID NUMERIC(18,0),
		numDomainID NUMERIC(18,0),
		dtDate DATETIME,
		numUserCntID NUMERIC(18,0),
		vcEvent VARCHAR(200),
		vcFieldName VARCHAR(200),
		vcOldValue VARCHAR(MAX),
		vcNewValue VARCHAR(MAX),
		vcDescription VARCHAR(MAX),
		vcHiddenDescription VARCHAR(MAX),
		numFieldID NUMERIC(18,0),
		bitCustomField BIT
	)
	
	DECLARE @TempTable TABLE
	(
		ID NUMERIC(18,0),
		vbStartlsn VARBINARY(128)
	)

	INSERT INTO 
		@TempTable
	SELECT
		ROW_NUMBER() OVER(ORDER BY __$start_lsn),
		__$start_lsn
	FROM
		[cdc].[dbo_OpportunityMaster_CT]
	WHERE
		__$operation IN (2,3)
	GROUP BY
		__$start_lsn

	DECLARE @i AS NUMERIC(18,0) = 1
	DECLARE @iCOUNT AS NUMERIC(18,0)
	DECLARE @vbStartlsn AS VARBINARY(128)
	DECLARE @vcUpdatemask AS VARBINARY(128)
	DECLARE @tintOperation AS TINYINT
	DECLARE @sqlVal AS VARBINARY(128)
	DECLARE @numOppID AS NUMERIC(18,0)
	DECLARE @numOppBizDocsID AS NUMERIC(18,0)
	DECLARE @vcBizDocID AS VARCHAR(300)
	DECLARE @numDomainID AS NUMERIC(18,0)
	DECLARE @bintModifiedDate DATETIME
	DECLARE @numModifiedBy AS NUMERIC(18,0)

	SELECT @iCOUNT = COUNT(*) FROM @TempTable

	WHILE @i <= @iCOUNT
	BEGIN
		SELECT @vbStartlsn = vbStartlsn FROM @TempTable WHERE ID=@i

		SELECT TOP 1
			@tintOperation = __$operation,
			@numOppID=numOppID,
			@numDomainID=numDomainID,
			@bintModifiedDate= (SELECT MAX(bintModifiedDate) FROM [cdc].[dbo_OpportunityMaster_CT] WHERE __$start_lsn = @vbStartlsn),
			@numModifiedBy=numModifiedBy
		FROM 
			[cdc].[dbo_OpportunityMaster_CT]
		WHERE 
			__$start_lsn = @vbStartlsn

		IF @tintOperation = 2 --INSERT
		BEGIN
			INSERT INTO @TEMPOpportunityHistory
			(
				vbStartlsn,
				numRecordID,
				numDomainID,
				dtDate,
				numUserCntID,
				vcEvent,
				numFieldID,
				bitCustomField
			)
			VALUES
			(
				@vbStartlsn,
				@numOppID,
				@numDomainID,
				@bintModifiedDate,
				@numModifiedBy,
				'New',
				NULL,
				0
			)
		END
		ELSE IF @tintOperation = 3 --UPDATE - (COLUMN VALUES BEFORE UPDATE)
		BEGIN
			
			DECLARE @TMEPChangedColumns TABLE 
			(
				ID INT,
				dtDate DATETIME,
				vcColumnName VARCHAR(200),
				vcOldValue VARCHAR(MAX),
				vcNewValue VARCHAR(MAX)
			)

			DELETE FROM @TMEPChangedColumns

			INSERT INTO @TMEPChangedColumns
			(
				ID,
				dtDate,
				vcColumnName,
				vcOldValue,
				vcNewValue
			)
			SELECT 
				ROW_NUMBER() OVER(ORDER BY TempUpdateBefore.__$start_lsn),
				DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()), sys.fn_cdc_map_lsn_to_time(TempUpdateBefore.__$start_lsn)), 
				TempUpdateBefore.column_name, 
				TempUpdateBefore.old_value, 
				TempUpdateAfter.new_value
			FROM
				( 
					SELECT 
						__$start_lsn, 
						column_name, 
						old_value
					FROM 
					(
						SELECT 
							__$start_lsn,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityMaster','numAssignedTo'),__$update_mask) = 1) THEN CAST(numAssignedTo AS VARCHAR(MAX)) ELSE NULL END AS numAssignedTo,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityMaster','bintModifiedDate'),__$update_mask) = 1) THEN CAST(bintModifiedDate AS VARCHAR(MAX)) ELSE NULL END AS bintModifiedDate,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityMaster','numModifiedBy'),__$update_mask) = 1) THEN CAST(numModifiedBy AS VARCHAR(MAX)) ELSE NULL END AS numModifiedBy,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityMaster','numCampainID'),__$update_mask) = 1) THEN CAST(numCampainID as VARCHAR(MAX)) ELSE NULL END AS numCampainID,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityMaster','numStatus'),__$update_mask) = 1) THEN CAST(numStatus as VARCHAR(MAX)) ELSE NULL END AS numStatus,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityMaster','txtComments'),__$update_mask) = 1) THEN CAST(txtComments as VARCHAR(MAX)) ELSE NULL END AS txtComments,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityMaster','vcPOppName'),__$update_mask) = 1) THEN CAST(vcPOppName AS VARCHAR(MAX)) ELSE NULL END AS vcPOppName,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityMaster','intPEstimatedCloseDate'),__$update_mask) = 1) THEN CAST(intPEstimatedCloseDate AS VARCHAR(MAX)) ELSE NULL END AS intPEstimatedCloseDate,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityMaster','tintSource'),__$update_mask) = 1) THEN CAST(tintSource AS VARCHAR(MAX)) ELSE NULL END AS tintSource,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityMaster','numSalesOrPurType'),__$update_mask) = 1) THEN CAST(numSalesOrPurType AS VARCHAR(MAX)) ELSE NULL END AS numSalesOrPurType,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityMaster','lngPConclAnalysis'),__$update_mask) = 1) THEN CAST(lngPConclAnalysis AS VARCHAR(MAX)) ELSE NULL END AS lngPConclAnalysis,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityMaster','tintActive'),__$update_mask) = 1) THEN CAST(tintActive AS VARCHAR(MAX)) ELSE NULL END AS tintActive,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityMaster','vcOppRefOrderNo'),__$update_mask) = 1) THEN CAST(vcOppRefOrderNo AS VARCHAR(MAX)) ELSE NULL END AS vcOppRefOrderNo,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityMaster','numBusinessProcessID'),__$update_mask) = 1) THEN CAST(numBusinessProcessID AS VARCHAR(MAX)) ELSE NULL END AS numBusinessProcessID,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityMaster','numRecOwner'),__$update_mask) = 1) THEN CAST(numRecOwner AS VARCHAR(MAX)) ELSE NULL END AS numRecOwner,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityMaster','tintOppStatus'),__$update_mask) = 1) THEN CAST(tintOppStatus AS VARCHAR(MAX)) ELSE NULL END AS tintOppStatus,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityMaster','bintClosedDate'),__$update_mask) = 1) THEN CAST(bintClosedDate AS VARCHAR(MAX)) ELSE NULL END AS bintClosedDate,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityMaster','tintshipped'),__$update_mask) = 1) THEN CAST(tintshipped AS VARCHAR(MAX)) ELSE NULL END AS tintshipped,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityMaster','numPercentageComplete'),__$update_mask) = 1) THEN CAST(numPercentageComplete AS VARCHAR(MAX)) ELSE NULL END AS numPercentageComplete
						FROM 
							cdc.fn_cdc_get_all_changes_dbo_OpportunityMaster(@vbStartlsn,@vbStartlsn, N'all update old')
						WHERE 
							__$operation = 3
					) as t1
					UNPIVOT 
					(
						old_value FOR column_name IN (numAssignedTo,bintModifiedDate,numModifiedBy,numCampainID,numStatus,txtComments,vcPOppName,intPEstimatedCloseDate,tintSource,numSalesOrPurType,lngPConclAnalysis,tintActive,vcOppRefOrderNo,numBusinessProcessID,numRecOwner,tintOppStatus,bintClosedDate,tintshipped,numPercentageComplete) 
					) as unp
				) as TempUpdateBefore
				INNER JOIN
				(
					SELECT 
						__$start_lsn,
						column_name, 
						new_value
					FROM 
					(
						SELECT 
							__$start_lsn,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityMaster','numAssignedTo'),__$update_mask) = 1) THEN CAST(numAssignedTo AS VARCHAR(MAX)) ELSE NULL END AS numAssignedTo,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityMaster','bintModifiedDate'),__$update_mask) = 1) THEN CAST(bintModifiedDate AS VARCHAR(MAX)) ELSE NULL END AS bintModifiedDate,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityMaster','numModifiedBy'),__$update_mask) = 1) THEN CAST(numModifiedBy AS VARCHAR(MAX)) ELSE NULL END AS numModifiedBy,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityMaster','numCampainID'),__$update_mask) = 1) THEN CAST(numCampainID as VARCHAR(MAX)) ELSE NULL END AS numCampainID,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityMaster','numStatus'),__$update_mask) = 1) THEN CAST(numStatus as VARCHAR(MAX)) ELSE NULL END AS numStatus,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityMaster','txtComments'),__$update_mask) = 1) THEN CAST(txtComments as VARCHAR(MAX)) ELSE NULL END AS txtComments,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityMaster','vcPOppName'),__$update_mask) = 1) THEN CAST(vcPOppName AS VARCHAR(MAX)) ELSE NULL END AS vcPOppName,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityMaster','intPEstimatedCloseDate'),__$update_mask) = 1) THEN CAST(intPEstimatedCloseDate AS VARCHAR(MAX)) ELSE NULL END AS intPEstimatedCloseDate,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityMaster','tintSource'),__$update_mask) = 1) THEN CAST(tintSource AS VARCHAR(MAX)) ELSE NULL END AS tintSource,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityMaster','numSalesOrPurType'),__$update_mask) = 1) THEN CAST(numSalesOrPurType AS VARCHAR(MAX)) ELSE NULL END AS numSalesOrPurType,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityMaster','lngPConclAnalysis'),__$update_mask) = 1) THEN CAST(lngPConclAnalysis AS VARCHAR(MAX)) ELSE NULL END AS lngPConclAnalysis,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityMaster','tintActive'),__$update_mask) = 1) THEN CAST(tintActive AS VARCHAR(MAX)) ELSE NULL END AS tintActive,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityMaster','vcOppRefOrderNo'),__$update_mask) = 1) THEN CAST(vcOppRefOrderNo AS VARCHAR(MAX)) ELSE NULL END AS vcOppRefOrderNo,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityMaster','numBusinessProcessID'),__$update_mask) = 1) THEN CAST(numBusinessProcessID AS VARCHAR(MAX)) ELSE NULL END AS numBusinessProcessID,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityMaster','numRecOwner'),__$update_mask) = 1) THEN CAST(numRecOwner AS VARCHAR(MAX)) ELSE NULL END AS numRecOwner,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityMaster','tintOppStatus'),__$update_mask) = 1) THEN CAST(tintOppStatus AS VARCHAR(MAX)) ELSE NULL END AS tintOppStatus,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityMaster','bintClosedDate'),__$update_mask) = 1) THEN CAST(bintClosedDate AS VARCHAR(MAX)) ELSE NULL END AS bintClosedDate,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityMaster','tintshipped'),__$update_mask) = 1) THEN CAST(tintshipped AS VARCHAR(MAX)) ELSE NULL END AS tintshipped,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityMaster','numPercentageComplete'),__$update_mask) = 1) THEN CAST(numPercentageComplete AS VARCHAR(MAX)) ELSE NULL END AS numPercentageComplete
						FROM 
							cdc.fn_cdc_get_all_changes_dbo_OpportunityMaster(@vbStartlsn, @vbStartlsn, N'all') -- 'all update old' is not necessary here
						WHERE 
							__$operation = 4
					) as t2
					UNPIVOT 
					(
						new_value FOR column_name IN (numAssignedTo,bintModifiedDate,numModifiedBy,numCampainID,numStatus,txtComments,vcPOppName,intPEstimatedCloseDate,tintSource,numSalesOrPurType,lngPConclAnalysis,tintActive,vcOppRefOrderNo,numBusinessProcessID,numRecOwner,tintOppStatus,bintClosedDate,tintshipped,numPercentageComplete) 
					) as unp 
				) as TempUpdateAfter -- after update
				ON 
					TempUpdateBefore.__$start_lsn = TempUpdateAfter.__$start_lsn 
					AND TempUpdateBefore.column_name = TempUpdateAfter.column_name

			DECLARE @j AS INT = 1
			DECLARE @jCOUNT AS INT
			DECLARE @dtDate AS DATETIME
			DECLARE @vcColumnName AS VARCHAR(200)
			DECLARE @vcOldValue AS VARCHAR(MAX)
			DECLARE @vcNewValue AS VARCHAR(MAX)

			SELECT @jCOUNT = COUNT(*) FROM @TMEPChangedColumns

			WHILE @j <= @jCOUNT
			BEGIN
				SELECT 
					@dtDate = dtDate,
					@vcColumnName = vcColumnName,
					@vcOldValue = vcOldValue,
					@vcNewValue = vcNewValue
				FROM 
					@TMEPChangedColumns
				WHERE
					ID = @j

				DECLARE @numNewModifiedBy As NUMERIC(18,0) = -1

				IF @vcColumnName = 'numModifiedBy'
				BEGIN
					-- KEEP VALUES IN TEMPORARY VARIABLE AND VALUES FOR ALL FIELDS UPDATE AFTER LOOP COMPLETES.
					SET @numNewModifiedBy = @vcNewValue
				END
				ELSE IF @vcColumnName = 'bintModifiedDate'
				BEGIN
					BEGIN TRY
						/* Opportunity Updated Items */
						INSERT INTO @TEMPOpportunityHistory
						(
							vbStartlsn,
							numRecordID,
							numDomainID,
							dtDate,
							numUserCntID,
							vcEvent,
							vcFieldName,
							vcOldValue,
							vcNewValue,
							vcDescription,
							vcHiddenDescription,
							numFieldID,
							bitCustomField
						)
						SELECT 
							@vbStartlsn,
							@numOppID,
							@numDomainID,
							@bintModifiedDate,
							@numModifiedBy,
							'Changed Item',
							(CASE TempUpdateBefore.column_name
								WHEN 'numUnitHour' THEN 'Unit Hour'
								WHEN 'monPrice' THEN 'Price'
								WHEN 'vcItemName' THEN 'Item Name'
								WHEN 'vcItemDesc' THEN 'Item Description'
								WHEN 'numWarehouseItmsID' THEN 'Warehouse'
								WHEN 'bitDropship' THEN 'Drop Ship'
								WHEN 'numUnitHourReceived' THEN 'Received Qty'
								WHEN 'bitDiscountType' THEN 'Discount Type'
								WHEN 'fltDiscount' THEN 'Discount'
								WHEN 'numQtyShipped' THEN 'Shipped Qty'
								WHEN 'numUOMId' THEN 'UOM'
								WHEN 'numToWarehouseItemID' THEN 'Stock Transfer To Warehouse'
								ELSE TempUpdateBefore.column_name
							END), 
							(CASE 
								WHEN TempUpdateBefore.column_name = 'numWarehouseItmsID' OR TempUpdateBefore.column_name = 'numToWarehouseItemID' THEN 
									(SELECT 
										CONCAT(W.vcWareHouse, CASE WHEN WL.vcLocation IS NOT NULL THEN CONCAT(' (',WL.vcLocation,')') ELSE '' END)
									FROM
										WareHouseItems WI
									INNER JOIN
										Warehouses W
									ON
										WI.numWareHouseID = W.numWareHouseID
									LEFT JOIN
										WarehouseLocation WL
									ON
										WI.numWLocationID = WL.numWLocationID
									WHERE 
										numWarehouseItemID = TempUpdateBefore.old_value)
								WHEN TempUpdateBefore.column_name = 'bitDiscountType' THEN
									(CASE WHEN TempUpdateBefore.old_value = 1 THEN 'Flat Amount' ELSE 'Percentage' END)
								WHEN TempUpdateBefore.column_name = 'numUOMId' THEN
									(SELECT vcUnitName FROM UOM WHERE numDomainId=@numDomainID AND numUOMId=TempUpdateBefore.old_value)
								ELSE
									TempUpdateBefore.old_value
							END), 
							(CASE 
								WHEN TempUpdateBefore.column_name = 'numWarehouseItmsID' OR TempUpdateBefore.column_name = 'numToWarehouseItemID' THEN 
									(SELECT 
										CONCAT(W.vcWareHouse, CASE WHEN WL.vcLocation IS NOT NULL THEN CONCAT(' (',WL.vcLocation,')') ELSE '' END)
									FROM
										WareHouseItems WI
									INNER JOIN
										Warehouses W
									ON
										WI.numWareHouseID = W.numWareHouseID
									LEFT JOIN
										WarehouseLocation WL
									ON
										WI.numWLocationID = WL.numWLocationID
									WHERE 
										numWarehouseItemID = TempUpdateAfter.new_value)
								WHEN TempUpdateBefore.column_name = 'bitDiscountType' THEN
									(CASE WHEN TempUpdateAfter.new_value = 1 THEN 'Flat Amount' ELSE 'Percentage' END)
								WHEN TempUpdateBefore.column_name = 'numUOMId' THEN
									(SELECT vcUnitName FROM UOM WHERE numDomainId=@numDomainID AND numUOMId=TempUpdateAfter.new_value)
								ELSE
									TempUpdateAfter.new_value
							END), 
							CONCAT('Item Code:',TempUpdateAfter.ItemCode,', Item Name:', TempUpdateAfter.ItemName),
							CONCAT('OppItemID:',TempUpdateAfter.OppItemID,', numWarehouseItemID:', TempUpdateAfter.WarehouseItemID),
							NULL,
							0
						FROM
							( 
								SELECT 
									__$start_lsn, 
									OppItemID,
									ItemCode,
									ItemName,
									WarehouseItemID,
									column_name, 
									old_value
								FROM 
								(
									SELECT 
										__$start_lsn,
										numOppItemtCode As OppItemID,
										numItemCode As ItemCode,
										vcItemName As ItemName,
										numWarehouseItmsID As WarehouseItemID,
										CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityItems','numItemCode'),__$update_mask) = 1) THEN CAST(numItemCode AS VARCHAR(MAX)) ELSE NULL END AS numItemCode,
										CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityItems','numUnitHour'),__$update_mask) = 1) THEN CAST(numUnitHour AS VARCHAR(MAX)) ELSE NULL END AS numUnitHour,
										CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityItems','monPrice'),__$update_mask) = 1) THEN CAST(monPrice AS VARCHAR(MAX)) ELSE NULL END AS monPrice,
										CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityItems','vcItemName'),__$update_mask) = 1) THEN CAST(vcItemName as VARCHAR(MAX)) ELSE NULL END AS vcItemName,
										CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityItems','vcItemDesc'),__$update_mask) = 1) THEN CAST(vcItemDesc as VARCHAR(MAX)) ELSE NULL END AS vcItemDesc,
										CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityItems','numWarehouseItmsID'),__$update_mask) = 1) THEN CAST(numWarehouseItmsID as VARCHAR(MAX)) ELSE NULL END AS numWarehouseItmsID,
										CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityItems','bitDropship'),__$update_mask) = 1) THEN CAST(bitDropship AS VARCHAR(MAX)) ELSE NULL END AS bitDropship,
										CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityItems','numUnitHourReceived'),__$update_mask) = 1) THEN CAST(numUnitHourReceived AS VARCHAR(MAX)) ELSE NULL END AS numUnitHourReceived,
										CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityItems','bitDiscountType'),__$update_mask) = 1) THEN CAST(bitDiscountType AS VARCHAR(MAX)) ELSE NULL END AS bitDiscountType,
										CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityItems','fltDiscount'),__$update_mask) = 1) THEN CAST(fltDiscount AS VARCHAR(MAX)) ELSE NULL END AS fltDiscount,
										CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityItems','numQtyShipped'),__$update_mask) = 1) THEN CAST(numQtyShipped AS VARCHAR(MAX)) ELSE NULL END AS numQtyShipped,
										CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityItems','numUOMId'),__$update_mask) = 1) THEN CAST(numUOMId AS VARCHAR(MAX)) ELSE NULL END AS numUOMId,
										CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityItems','numToWarehouseItemID'),__$update_mask) = 1) THEN CAST(numToWarehouseItemID AS VARCHAR(MAX)) ELSE NULL END AS numToWarehouseItemID
									FROM 
										cdc.fn_cdc_get_all_changes_dbo_OpportunityItems(@vbStartlsn,@vbStartlsn, N'all update old')
									WHERE 
										__$operation = 3
								) as t1
								UNPIVOT 
								(
									old_value FOR column_name IN (numItemCode,numUnitHour,monPrice,vcItemName,vcItemDesc,numWarehouseItmsID,bitDropship,numUnitHourReceived,bitDiscountType,fltDiscount,numToWarehouseItemID) 
								) as unp
							) as TempUpdateBefore
							INNER JOIN
							(
								SELECT 
									__$start_lsn,
									OppItemID,
									ItemCode,
									ItemName,
									WarehouseItemID,
									column_name, 
									new_value
								FROM 
								(
									SELECT 
										__$start_lsn,
										numOppItemtCode As OppItemID,
										numItemCode As ItemCode,
										vcItemName As ItemName,
										numWarehouseItmsID As WarehouseItemID,
										CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityItems','numItemCode'),__$update_mask) = 1) THEN CAST(numItemCode AS VARCHAR(MAX)) ELSE NULL END AS numItemCode,
										CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityItems','numUnitHour'),__$update_mask) = 1) THEN CAST(numUnitHour AS VARCHAR(MAX)) ELSE NULL END AS numUnitHour,
										CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityItems','monPrice'),__$update_mask) = 1) THEN CAST(monPrice AS VARCHAR(MAX)) ELSE NULL END AS monPrice,
										CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityItems','vcItemName'),__$update_mask) = 1) THEN CAST(vcItemName as VARCHAR(MAX)) ELSE NULL END AS vcItemName,
										CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityItems','vcItemDesc'),__$update_mask) = 1) THEN CAST(vcItemDesc as VARCHAR(MAX)) ELSE NULL END AS vcItemDesc,
										CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityItems','numWarehouseItmsID'),__$update_mask) = 1) THEN CAST(numWarehouseItmsID as VARCHAR(MAX)) ELSE NULL END AS numWarehouseItmsID,
										CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityItems','bitDropship'),__$update_mask) = 1) THEN CAST(bitDropship AS VARCHAR(MAX)) ELSE NULL END AS bitDropship,
										CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityItems','numUnitHourReceived'),__$update_mask) = 1) THEN CAST(numUnitHourReceived AS VARCHAR(MAX)) ELSE NULL END AS numUnitHourReceived,
										CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityItems','bitDiscountType'),__$update_mask) = 1) THEN CAST(bitDiscountType AS VARCHAR(MAX)) ELSE NULL END AS bitDiscountType,
										CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityItems','fltDiscount'),__$update_mask) = 1) THEN CAST(fltDiscount AS VARCHAR(MAX)) ELSE NULL END AS fltDiscount,
										CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityItems','numQtyShipped'),__$update_mask) = 1) THEN CAST(numQtyShipped AS VARCHAR(MAX)) ELSE NULL END AS numQtyShipped,
										CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityItems','numUOMId'),__$update_mask) = 1) THEN CAST(numUOMId AS VARCHAR(MAX)) ELSE NULL END AS numUOMId,
										CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityItems','numToWarehouseItemID'),__$update_mask) = 1) THEN CAST(numToWarehouseItemID AS VARCHAR(MAX)) ELSE NULL END AS numToWarehouseItemID
									FROM 
										cdc.fn_cdc_get_all_changes_dbo_OpportunityItems(@vbStartlsn,@vbStartlsn, N'all') -- 'all update old' is not necessary here
									WHERE 
										__$operation = 4
								) as t2
								UNPIVOT 
								(
									new_value FOR column_name IN (numItemCode,numUnitHour,monPrice,vcItemName,vcItemDesc,numWarehouseItmsID,bitDropship,numUnitHourReceived,bitDiscountType,fltDiscount,numToWarehouseItemID) 
								) as unp 
							) as TempUpdateAfter -- after update
							ON 
								TempUpdateBefore.__$start_lsn = TempUpdateAfter.__$start_lsn 
								AND TempUpdateBefore.column_name = TempUpdateAfter.column_name
								AND TempUpdateBefore.OppItemID = TempUpdateAfter.OppItemID
								AND ISNULL(TempUpdateBefore.WarehouseItemID,0) = ISNULL(TempUpdateAfter.WarehouseItemID,0)
					END TRY
					BEGIN CATCH
						/* If the specified LSN range does not fall within the change tracking timeline for the capture instance, the function returns error 208 
						("An insufficient number of arguments were supplied for the procedure or function cdc.fn_cdc_get_all_changes."). */
					END CATCH

					BEGIN TRY
						/* Opportunity Deleted Items */
						INSERT INTO @TEMPOpportunityHistory
						(
							vbStartlsn,
							numRecordID,
							numDomainID,
							dtDate,
							numUserCntID,
							vcEvent,
							vcDescription,
							vcHiddenDescription,
							numFieldID,
							bitCustomField
						)
						SELECT 
							@vbStartlsn,
							@numOppID,
							@numDomainID,
							@bintModifiedDate,
							@numModifiedBy,
							'Removed Item',
							CONCAT('Item Code:',TempDelete.ItemCode,', Item Name:', TempDelete.ItemName),
							CONCAT('OppItemID:',TempDelete.OppItemID,', numWarehouseItemID:', TempDelete.WarehouseItemID),
							NULL,
							0
						FROM
							( 
								SELECT 
									__$start_lsn,
									numOppItemtCode As OppItemID,
									numItemCode As ItemCode,
									vcItemName As ItemName,
									numWarehouseItmsID As WarehouseItemID
								FROM 
									[cdc].[dbo_OpportunityItems_CT]
								WHERE 
									__$operation = 1
									AND __$start_lsn = @vbStartlsn
							) AS TempDelete
					END TRY
					BEGIN CATCH
						/* If the specified LSN range does not fall within the change tracking timeline for the capture instance, the function returns error 208 
						("An insufficient number of arguments were supplied for the procedure or function cdc.fn_cdc_get_all_changes."). */
					END CATCH
				END
				ELSE IF @vcColumnName = 'vcPOppName'
				BEGIN
					-- IF UPDATE FIELD ID vcPOppName
					-- IF OPPID IS EXISTS IN TEMP TABLE AND COMMIT DATE TIME IS SAME AS FOUND IN TEMP TABLE
					-- THEN IGNORE CHANGE BECUASE IT IS MODIFIED AFTER OPP/ORDER INSERT AND NOT SEPERATE TRANSACTION
					IF NOT EXISTS(SELECT * FROM @TEMPOpportunityHistory WHERE vbStartlsn = @vbStartlsn AND dtDate = @dtDate)
					BEGIN
						INSERT INTO @TEMPOpportunityHistory
						(
							vbStartlsn,
							numRecordID,
							numDomainID,
							dtDate,
							numUserCntID,
							vcEvent,
							vcFieldName,
							vcOldValue,
							vcNewValue,
							numFieldID,
							bitCustomField
						)
						VALUES
						(
							@vbStartlsn,
							@numOppID,
							@numDomainID,
							@dtDate,
							@numModifiedBy,
							'Change',
							'Opp/Order Name',
							@vcOldValue,
							@vcNewValue,
							NULL,
							0
						)
					END
				END
				ELSE
				BEGIN
					INSERT INTO @TEMPOpportunityHistory
					(
						vbStartlsn,
						numRecordID,
						numDomainID,
						dtDate,
						numUserCntID,
						vcEvent,
						vcFieldName,
						vcOldValue,
						vcNewValue,
						numFieldID,
						bitCustomField
					)
					VALUES
					(
						@vbStartlsn,
						@numOppID,
						@numDomainID,
						@dtDate,
						@numModifiedBy,
						'Change',
						(CASE 
							WHEN @vcColumnName = 'numAssignedTo' THEN 'Assigned To'
							WHEN @vcColumnName = 'numCampainID' THEN 'Campaign'
							WHEN @vcColumnName = 'numStatus' THEN 'Status'
							WHEN @vcColumnName = 'txtComments' THEN 'Comments'
							WHEN @vcColumnName = 'intPEstimatedCloseDate' THEN 'Estimated Close Date'
							WHEN @vcColumnName = 'tintSource' THEN 'Source'
							WHEN @vcColumnName = 'numSalesOrPurType' THEN 'Sales/Purchase Type'
							WHEN @vcColumnName = 'lngPConclAnalysis' THEN 'Conclusion Reason'
							WHEN @vcColumnName = 'tintActive' THEN 'Active'
							WHEN @vcColumnName = 'vcOppRefOrderNo' THEN 'Customer PO#'
							WHEN @vcColumnName = 'numBusinessProcessID' THEN 'Business Process'
							WHEN @vcColumnName = 'numRecOwner' THEN 'Record Owner'
							WHEN @vcColumnName = 'tintOppStatus' THEN 'Deal Status'
							WHEN @vcColumnName = 'bintClosedDate' THEN 'Closed Date'
							WHEN @vcColumnName = 'tintshipped' THEN 'Order Closed'
							WHEN @vcColumnName = 'numPercentageComplete' THEN 'Percentage Complete'
						END),
						(CASE @vcColumnName
							WHEN 'numAssignedTo' THEN
								(SELECT vcUserName FROM UserMaster WHERE numDomainID=@numDomainID AND numUserDetailID = @vcOldValue)
							WHEN 'numCampainID' THEN
								ISNULL((SELECT vcCampaignName FROM CampaignMaster WHERE numDomainID=@numDomainID AND numCampaignID = @vcOldValue),'')
							WHEN 'numStatus' THEN
								ISNULL((SELECT vcData FROM ListDetails WHERE numDomainID=@numDomainID AND numListID=176 AND numListItemID=@vcOldValue),'')
							WHEN 'tintSource' THEN
								ISNULL((SELECT vcData FROM ListDetails WHERE numDomainID=@numDomainID AND numListID=9 AND numListItemID=@vcOldValue),'')
							WHEN 'numSalesOrPurType' THEN
								ISNULL((SELECT vcData FROM ListDetails WHERE numDomainID=@numDomainID AND numListID=45 AND numListItemID=@vcOldValue),'')
							WHEN 'tintActive' THEN
								(CASE WHEN @vcOldValue = 1 THEN 'Yes' ELSE 'No' END)
							WHEN 'vcOppRefOrderNo' THEN
								ISNULL((SELECT vcPOppName FROM OpportunityMaster WHERE numDomainID=@numDomainID AND numOppID=@vcOldValue),'')
							WHEN 'numBusinessProcessID' THEN
								ISNULL((SELECT Slp_Name FROM Sales_process_List_Master WHERE numDomainID=@numDomainID AND Slp_Id=@vcOldValue),'')
							WHEN 'numRecOwner' THEN
								(SELECT vcUserName FROM UserMaster WHERE numDomainID=@numDomainID AND numUserDetailID = @vcOldValue)
							WHEN 'tintOppStatus' THEN
								(CASE @vcOldValue WHEN 1 THEN 'Deal Won' WHEN 2 THEN 'Deal Lost' ELSE 'Open' END)
							WHEN 'tintshipped' THEN
								(CASE @vcOldValue WHEN 1 THEN 'Closed' ELSE 'Open' END)
							WHEN 'numPercentageComplete' THEN
								ISNULL((SELECT vcData FROM ListDetails WHERE numDomainID=@numDomainID AND numListID=50 AND numListItemID=@vcOldValue),'')
							ELSE
								@vcOldValue
						END),
						(CASE @vcColumnName
							WHEN 'numAssignedTo' THEN
								(SELECT vcUserName FROM UserMaster WHERE numDomainID=@numDomainID AND numUserDetailID = @vcNewValue)
							WHEN 'numCampainID' THEN
								ISNULL((SELECT vcCampaignName FROM CampaignMaster WHERE numDomainID=@numDomainID AND numCampaignID = @vcNewValue),'')
							WHEN 'numStatus' THEN
								ISNULL((SELECT vcData FROM ListDetails WHERE numDomainID=@numDomainID AND numListID=176 AND numListItemID=@vcNewValue),'')
							WHEN 'tintSource' THEN
								ISNULL((SELECT vcData FROM ListDetails WHERE numDomainID=@numDomainID AND numListID=9 AND numListItemID=@vcNewValue),'')
							WHEN 'numSalesOrPurType' THEN
								ISNULL((SELECT vcData FROM ListDetails WHERE numDomainID=@numDomainID AND numListID=45 AND numListItemID=@vcNewValue),'')
							WHEN 'tintActive' THEN
								(CASE WHEN @vcNewValue = 1 THEN 'Yes' ELSE 'No' END)
							WHEN 'vcOppRefOrderNo' THEN
								ISNULL((SELECT vcPOppName FROM OpportunityMaster WHERE numDomainID=@numDomainID AND numOppID=@vcNewValue),'')
							WHEN 'numBusinessProcessID' THEN
								ISNULL((SELECT Slp_Name FROM Sales_process_List_Master WHERE numDomainID=@numDomainID AND Slp_Id=@vcNewValue),'')
							WHEN 'numRecOwner' THEN
								(SELECT vcUserName FROM UserMaster WHERE numDomainID=@numDomainID AND numUserDetailID = @vcNewValue)
							WHEN 'tintOppStatus' THEN
								(CASE @vcNewValue WHEN 1 THEN 'Deal Won' WHEN 2 THEN 'Deal Lost' ELSE 'Open' END)
							WHEN 'tintshipped' THEN
								(CASE @vcNewValue WHEN 1 THEN 'Closed' ELSE 'Open' END)
							WHEN 'numPercentageComplete' THEN
								ISNULL((SELECT vcData FROM ListDetails WHERE numDomainID=@numDomainID AND numListID=50 AND numListItemID=@vcNewValue),'')
							ELSE
								@vcNewValue
						END),
						NULL,
						0
					)
				END

				SET @j = @j + 1
			END

			IF @numNewModifiedBy <> -1
			BEGIN
				UPDATE @TEMPOpportunityHistory SET numUserCntID = @numNewModifiedBy WHERE vbStartlsn = @vbStartlsn
			END
		END

		SET @i = @i + 1
	END

	--CLEAR RETRIVED DATA FROM CDC TABLE
	DELETE FROM [cdc].[dbo_OpportunityItems_CT] WHERE __$start_lsn IN (SELECT vbStartlsn FROM @TempTable)
	DELETE FROM [cdc].[dbo_OpportunityMaster_CT] WHERE __$start_lsn IN (SELECT vbStartlsn FROM @TempTable)

	/************************************ OPP/ORDER BIZDOCS ************************************/
	DELETE FROM @TempTable

	INSERT INTO 
		@TempTable
	SELECT
		ROW_NUMBER() OVER(ORDER BY __$start_lsn),
		__$start_lsn
	FROM
		[cdc].[dbo_OpportunityBizDocs_CT]
	WHERE
		__$operation IN (1,2,3)
	GROUP BY
		__$start_lsn

	SET @i = 1
	SELECT @iCOUNT = COUNT(*) FROM @TempTable


	WHILE @i <= @iCOUNT
	BEGIN
		SELECT @vbStartlsn = vbStartlsn FROM @TempTable WHERE ID=@i

		SELECT TOP 1
			@tintOperation = __$operation,
			@numOppID=[cdc].[dbo_OpportunityBizDocs_CT].numOppID,
			@numOppBizDocsID = numOppBizDocsId,
			@vcBizDocID = vcBizDocID,
			@numDomainID=numDomainID,
			@bintModifiedDate=[cdc].[dbo_OpportunityBizDocs_CT].dtModifiedDate,
			@numModifiedBy=[cdc].[dbo_OpportunityBizDocs_CT].numModifiedBy
		FROM 
			[cdc].[dbo_OpportunityBizDocs_CT]
		JOIN
			OpportunityMaster
		ON
			[cdc].[dbo_OpportunityBizDocs_CT].numOppID = OpportunityMaster.numOppID
		WHERE 
			__$start_lsn = @vbStartlsn

		IF @tintOperation = 2 --INSERT
		BEGIN
			INSERT INTO @TEMPOpportunityHistory
			(
				vbStartlsn,
				numRecordID,
				numDomainID,
				dtDate,
				numUserCntID,
				vcEvent,
				vcDescription,
				vcHiddenDescription,
				numFieldID,
				bitCustomField
			)
			VALUES
			(
				@vbStartlsn,
				@numOppID,
				@numDomainID,
				@bintModifiedDate,
				@numModifiedBy,
				'Added BizDoc',
				ISNULL((SELECT vcBizDocID FROM OpportunityBizDocs WHERE numOppID=@numOppID AND numOppBizDocsId=@numOppBizDocsID),''),
				CONCAT('OppID:',@numOppID,', OppBizDocsID:',@numOppBizDocsID),
				NULL,
				0
			)
		END
		ELSE IF @tintOperation = 1 --DELETE
		BEGIN
			INSERT INTO @TEMPOpportunityHistory
			(
				vbStartlsn,
				numRecordID,
				numDomainID,
				dtDate,
				numUserCntID,
				vcEvent,
				vcDescription,
				vcHiddenDescription,
				numFieldID,
				bitCustomField
			)
			VALUES
			(
				@vbStartlsn,
				@numOppID,
				@numDomainID,
				@bintModifiedDate,
				@numModifiedBy,
				'Removed BizDoc',
				@vcBizDocID,
				CONCAT('OppID:',@numOppID,', OppBizDocsID:',@numOppBizDocsID),
				NULL,
				0
			)
		END
		ELSE IF @tintOperation = 3 --UPDATE
		BEGIN
			DELETE FROM @TMEPChangedColumns

			INSERT INTO @TMEPChangedColumns
			(
				ID,
				dtDate,
				vcColumnName,
				vcOldValue,
				vcNewValue
			)
			SELECT 
				ROW_NUMBER() OVER(ORDER BY TempUpdateBefore.__$start_lsn),
				DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()), sys.fn_cdc_map_lsn_to_time(TempUpdateBefore.__$start_lsn)), 
				TempUpdateBefore.column_name, 
				TempUpdateBefore.old_value, 
				TempUpdateAfter.new_value
			FROM
				( 
					SELECT 
						__$start_lsn, 
						column_name, 
						old_value
					FROM 
					(
						SELECT 
							__$start_lsn,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityBizDocs','dtModifiedDate'),__$update_mask) = 1) THEN CAST(dtModifiedDate AS VARCHAR(MAX)) ELSE NULL END AS dtModifiedDate,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityBizDocs','numModifiedBy'),__$update_mask) = 1) THEN CAST(numModifiedBy AS VARCHAR(MAX)) ELSE NULL END AS numModifiedBy,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityBizDocs','monAmountPaid'),__$update_mask) = 1) THEN CAST(monAmountPaid as VARCHAR(MAX)) ELSE NULL END AS monAmountPaid,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityBizDocs','vcComments'),__$update_mask) = 1) THEN CAST(vcComments as VARCHAR(MAX)) ELSE NULL END AS vcComments,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityBizDocs','vcBizDocID'),__$update_mask) = 1) THEN CAST(vcBizDocID as VARCHAR(MAX)) ELSE NULL END AS vcBizDocID,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityBizDocs','numShipVia'),__$update_mask) = 1) THEN CAST(numShipVia AS VARCHAR(MAX)) ELSE NULL END AS numShipVia,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityBizDocs','vcTrackingNo'),__$update_mask) = 1) THEN CAST(vcTrackingNo AS VARCHAR(MAX)) ELSE NULL END AS vcTrackingNo,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityBizDocs','numBizDocStatus'),__$update_mask) = 1) THEN CAST(numBizDocStatus AS VARCHAR(MAX)) ELSE NULL END AS numBizDocStatus,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityBizDocs','dtFromDate'),__$update_mask) = 1) THEN CAST(dtFromDate AS VARCHAR(MAX)) ELSE NULL END AS dtFromDate
						FROM 
							cdc.fn_cdc_get_all_changes_dbo_OpportunityBizDocs(@vbStartlsn,@vbStartlsn, N'all update old')
						WHERE 
							__$operation = 3
					) as t1
					UNPIVOT 
					(
						old_value FOR column_name IN (dtModifiedDate,numModifiedBy,monAmountPaid,vcComments,vcBizDocID,numShipVia,vcTrackingNo,numBizDocStatus,dtFromDate) 
					) as unp
				) as TempUpdateBefore
				INNER JOIN
				(
					SELECT 
						__$start_lsn,
						column_name, 
						new_value
					FROM 
					(
						SELECT 
							__$start_lsn,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityBizDocs','dtModifiedDate'),__$update_mask) = 1) THEN CAST(dtModifiedDate AS VARCHAR(MAX)) ELSE NULL END AS dtModifiedDate,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityBizDocs','numModifiedBy'),__$update_mask) = 1) THEN CAST(numModifiedBy AS VARCHAR(MAX)) ELSE NULL END AS numModifiedBy,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityBizDocs','monAmountPaid'),__$update_mask) = 1) THEN CAST(monAmountPaid as VARCHAR(MAX)) ELSE NULL END AS monAmountPaid,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityBizDocs','vcComments'),__$update_mask) = 1) THEN CAST(vcComments as VARCHAR(MAX)) ELSE NULL END AS vcComments,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityBizDocs','vcBizDocID'),__$update_mask) = 1) THEN CAST(vcBizDocID as VARCHAR(MAX)) ELSE NULL END AS vcBizDocID,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityBizDocs','numShipVia'),__$update_mask) = 1) THEN CAST(numShipVia AS VARCHAR(MAX)) ELSE NULL END AS numShipVia,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityBizDocs','vcTrackingNo'),__$update_mask) = 1) THEN CAST(vcTrackingNo AS VARCHAR(MAX)) ELSE NULL END AS vcTrackingNo,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityBizDocs','numBizDocStatus'),__$update_mask) = 1) THEN CAST(numBizDocStatus AS VARCHAR(MAX)) ELSE NULL END AS numBizDocStatus,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityBizDocs','dtFromDate'),__$update_mask) = 1) THEN CAST(dtFromDate AS VARCHAR(MAX)) ELSE NULL END AS dtFromDate
						FROM 
							cdc.fn_cdc_get_all_changes_dbo_OpportunityBizDocs(@vbStartlsn, @vbStartlsn, N'all') -- 'all update old' is not necessary here
						WHERE 
							__$operation = 4
					) as t2
					UNPIVOT 
					(
						new_value FOR column_name IN (dtModifiedDate,numModifiedBy,monAmountPaid,vcComments,vcBizDocID,numShipVia,vcTrackingNo,numBizDocStatus,dtFromDate) 
					) as unp 
				) as TempUpdateAfter -- after update
				ON 
					TempUpdateBefore.__$start_lsn = TempUpdateAfter.__$start_lsn 
					AND TempUpdateBefore.column_name = TempUpdateAfter.column_name

			SET  @j = 1

			SELECT @jCOUNT = COUNT(*) FROM @TMEPChangedColumns

			WHILE @j <= @jCOUNT
			BEGIN
				SELECT 
					@dtDate = dtDate,
					@vcColumnName = vcColumnName,
					@vcOldValue = vcOldValue,
					@vcNewValue = vcNewValue
				FROM 
					@TMEPChangedColumns
				WHERE
					ID = @j

				SET @numNewModifiedBy = -1

				IF @vcColumnName = 'numModifiedBy'
				BEGIN
					-- KEEP VALUES IN TEMPORARY VARIABLE AND VALUES FOR ALL FIELDS UPDATE AFTER LOOP COMPLETES.
					SET @numNewModifiedBy = @vcNewValue
				END
				ELSE IF @vcColumnName = 'dtModifiedDate'
				BEGIN
					BEGIN TRY
						/* OpportunityBizDocs Updated Items */
						INSERT INTO @TEMPOpportunityHistory
						(
							vbStartlsn,
							numRecordID,
							numDomainID,
							dtDate,
							numUserCntID,
							vcEvent,
							vcFieldName,
							vcOldValue,
							vcNewValue,
							vcDescription,
							vcHiddenDescription,
							numFieldID,
							bitCustomField
						)
						SELECT 
							@vbStartlsn,
							@numOppID,
							@numDomainID,
							@bintModifiedDate,
							@numModifiedBy,
							'Changed BizDoc Item',
							(CASE TempUpdateBefore.column_name
								WHEN 'numUnitHour' THEN 'Unit Hour'
								WHEN 'vcNotes' THEN 'Notes'
								ELSE TempUpdateBefore.column_name
							END), 
							TempUpdateBefore.old_value, 
							TempUpdateAfter.new_value,
							CONCAT('Item Code:',TempUpdateAfter.ItemCode,', Item Name:', TempUpdateAfter.ItemName),
							CONCAT('OppBizDocItemID:',TempUpdateAfter.OppItemID,', numWarehouseItemID:', TempUpdateAfter.WarehouseItemID),
							NULL,
							0
						FROM
							( 
								SELECT 
									__$start_lsn, 
									OppItemID,
									ItemCode,
									ItemName,
									WarehouseItemID,
									column_name, 
									old_value
								FROM 
								(
									SELECT 
										__$start_lsn,
										numOppBizDocItemID As OppItemID,
										CDCOppBixDocItems.numItemCode As ItemCode,
										vcItemName As ItemName,
										numWarehouseItmsID As WarehouseItemID,
										CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityBizDocItems','numItemCode'),__$update_mask) = 1) THEN CAST(CDCOppBixDocItems.numItemCode AS VARCHAR(MAX)) ELSE NULL END AS numItemCode,
										CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityBizDocItems','numUnitHour'),__$update_mask) = 1) THEN CAST(numUnitHour AS VARCHAR(MAX)) ELSE NULL END AS numUnitHour,
										CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityBizDocItems','vcNotes'),__$update_mask) = 1) THEN CAST(vcNotes AS VARCHAR(MAX)) ELSE NULL END AS vcNotes
									FROM 
										cdc.fn_cdc_get_all_changes_dbo_OpportunityBizDocItems(@vbStartlsn,@vbStartlsn, N'all update old') AS CDCOppBixDocItems
									JOIN
										Item
									ON
										CDCOppBixDocItems.numItemCode = Item.numItemCode
									WHERE 
										__$operation = 3
								) as t1 
								UNPIVOT 
								(
									old_value FOR column_name IN (numItemCode,numUnitHour,vcNotes) 
								) as unp
							) as TempUpdateBefore
							INNER JOIN
							(
								SELECT 
									__$start_lsn,
									OppItemID,
									ItemCode,
									ItemName,
									WarehouseItemID,
									column_name, 
									new_value
								FROM 
								(
									SELECT 
										__$start_lsn,
										numOppBizDocItemID As OppItemID,
										CDCOppBixDocItems.numItemCode As ItemCode,
										Item.vcItemName As ItemName,
										numWarehouseItmsID As WarehouseItemID,
										CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityBizDocItems','numItemCode'),__$update_mask) = 1) THEN CAST(CDCOppBixDocItems.numItemCode AS VARCHAR(MAX)) ELSE NULL END AS numItemCode,
										CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityBizDocItems','numUnitHour'),__$update_mask) = 1) THEN CAST(numUnitHour AS VARCHAR(MAX)) ELSE NULL END AS numUnitHour,
										CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityBizDocItems','vcNotes'),__$update_mask) = 1) THEN CAST(vcNotes AS VARCHAR(MAX)) ELSE NULL END AS vcNotes
									FROM 
										cdc.fn_cdc_get_all_changes_dbo_OpportunityBizDocItems(@vbStartlsn,@vbStartlsn, N'all') AS CDCOppBixDocItems -- 'all update old' is not necessary here
									JOIN
										Item
									ON
										CDCOppBixDocItems.numItemCode = Item.numItemCode
									WHERE 
										__$operation = 4
								) as t2
								UNPIVOT 
								(
									new_value FOR column_name IN (numItemCode,numUnitHour,vcNotes) 
								) as unp 
							) as TempUpdateAfter -- after update
							ON 
								TempUpdateBefore.__$start_lsn = TempUpdateAfter.__$start_lsn 
								AND TempUpdateBefore.column_name = TempUpdateAfter.column_name
								AND TempUpdateBefore.OppItemID = TempUpdateAfter.OppItemID
								AND ISNULL(TempUpdateBefore.WarehouseItemID,0) = ISNULL(TempUpdateAfter.WarehouseItemID,0)
					END TRY
					BEGIN CATCH
						/* If the specified LSN range does not fall within the change tracking timeline for the capture instance, the function returns error 208 
						("An insufficient number of arguments were supplied for the procedure or function cdc.fn_cdc_get_all_changes."). */
					END CATCH
					
					BEGIN TRY
						/* OpportunityBizDocs Deleted Items */
						INSERT INTO @TEMPOpportunityHistory
						(
							vbStartlsn,
							numRecordID,
							numDomainID,
							dtDate,
							numUserCntID,
							vcEvent,
							vcDescription,
							vcHiddenDescription,
							numFieldID,
							bitCustomField
						)
						SELECT 
							@vbStartlsn,
							@numOppID,
							@numDomainID,
							@bintModifiedDate,
							@numModifiedBy,
							'Removed BizDoc Item',
							CONCAT('Item Code:',TempDelete.ItemCode,', Item Name:', TempDelete.ItemName),
							CONCAT('OppBizDocItemID:',TempDelete.OppItemID,', numWarehouseItemID:', TempDelete.WarehouseItemID),
							NULL,
							0
						FROM
							( 
								SELECT 
									__$start_lsn,
									CDCOppBixDocItems.numOppBizDocItemID As OppItemID,
									CDCOppBixDocItems.numItemCode As ItemCode,
									vcItemName As ItemName,
									numWarehouseItmsID As WarehouseItemID
								FROM 
									[cdc].[dbo_OpportunityBizDocItems_CT] CDCOppBixDocItems
								JOIN
									Item
								ON
									CDCOppBixDocItems.numItemCode = Item.numItemCode
								WHERE 
									__$operation = 1
									AND __$start_lsn = @vbStartlsn
							) AS TempDelete
					END TRY
					BEGIN CATCH
						/* If the specified LSN range does not fall within the change tracking timeline for the capture instance, the function returns error 208 
						("An insufficient number of arguments were supplied for the procedure or function cdc.fn_cdc_get_all_changes."). */
					END CATCH
				END
				ELSE
				BEGIN
					INSERT INTO @TEMPOpportunityHistory
					(
						vbStartlsn,
						numRecordID,
						numDomainID,
						dtDate,
						numUserCntID,
						vcEvent,
						vcFieldName,
						vcOldValue,
						vcNewValue,
						numFieldID,
						bitCustomField
					)
					VALUES
					(
						@vbStartlsn,
						@numOppID,
						@numDomainID,
						@dtDate,
						@numModifiedBy,
						'Changed BizDoc',
						CASE 
							WHEN @vcColumnName = 'monAmountPaid' THEN 'Paid Amount'
							WHEN @vcColumnName = 'vcComments' THEN 'Comments'
							WHEN @vcColumnName = 'vcBizDocID' THEN 'BizDoc ID'
							WHEN @vcColumnName = 'numShipVia' THEN 'Shipping Company'
							WHEN @vcColumnName = 'vcTrackingNo' THEN 'Tracking No'
							WHEN @vcColumnName = 'numBizDocStatus' THEN 'BizDoc Status'
							WHEN @vcColumnName = 'dtFromDate' THEN 'Billing Date'
						END,
						(
							CASE @vcColumnName
								WHEN 'numShipVia' THEN
									ISNULL((SELECT vcData FROM ListDetails WHERE numDomainID=@numDomainID AND numListID=82 AND numListItemID=@vcOldValue),'')
								WHEN 'numBizDocStatus' THEN
									ISNULL((SELECT vcData FROM ListDetails WHERE numDomainID=@numDomainID AND numListID=11 AND numListItemID=@vcOldValue),'')
								ELSE
									@vcOldValue
							END
						),
						(
							CASE @vcColumnName
								WHEN 'numShipVia' THEN
									ISNULL((SELECT vcData FROM ListDetails WHERE numDomainID=@numDomainID AND numListID=82 AND numListItemID=@vcNewValue),'')
								WHEN 'numBizDocStatus' THEN
									ISNULL((SELECT vcData FROM ListDetails WHERE numDomainID=@numDomainID AND numListID=11 AND numListItemID=@vcNewValue),'')
								ELSE
									@vcNewValue
							END
						),
						NULL,
						0
					)
				END

				SET @j = @j + 1
			END

			IF @numNewModifiedBy <> -1
			BEGIN
				UPDATE @TEMPOpportunityHistory SET numUserCntID = @numNewModifiedBy WHERE vbStartlsn = @vbStartlsn
			END
		END

		SET @i = @i + 1
	END

	--CLEAR RETRIVED DATA FROM CDC TABLE
	DELETE FROM [cdc].[dbo_OpportunityBizDocItems_CT] WHERE __$start_lsn IN (SELECT vbStartlsn FROM @TEMPOpportunityHistory)
	DELETE FROM [cdc].[dbo_OpportunityBizDocs_CT] WHERE __$start_lsn IN (SELECT vbStartlsn FROM @TEMPOpportunityHistory)
	

	/************************************ OPP/ORDER CUSTOM FIELDS **********************************/

	DELETE FROM @TempTable

	INSERT INTO 
		@TempTable
	SELECT
		ROW_NUMBER() OVER(ORDER BY __$start_lsn),
		__$start_lsn
	FROM
		[cdc].[dbo_CFW_Fld_Values_Opp_CT]
	WHERE
		__$operation IN (2,3)
	GROUP BY
		__$start_lsn

	SET @i = 1
	SELECT @iCOUNT = COUNT(*) FROM @TempTable
	DECLARE @Fld_ID NUMERIC(18,0)
	DECLARE @Fld_Value VARCHAR(MAX)

	WHILE @i <= @iCOUNT
	BEGIN
		SELECT @vbStartlsn = vbStartlsn FROM @TempTable WHERE ID=@i

		SELECT TOP 1
			@tintOperation = __$operation,
			@numOppID=numOppId,
			@numDomainID=numDomainID,
			@Fld_ID = Fld_Id,
			@Fld_Value = Fld_Value,
			@bintModifiedDate=[cdc].[dbo_CFW_Fld_Values_Opp_CT].bintModifiedDate,
			@numModifiedBy=[cdc].[dbo_CFW_Fld_Values_Opp_CT].numModifiedBy
		FROM 
			[cdc].[dbo_CFW_Fld_Values_Opp_CT]
		JOIN
			OpportunityMaster
		ON
			[cdc].[dbo_CFW_Fld_Values_Opp_CT].RecId = OpportunityMaster.numOppID
		WHERE 
			__$start_lsn = @vbStartlsn


		IF @tintOperation = 2 --INSERT
		BEGIN
			INSERT INTO @TEMPOpportunityHistory
			(
				vbStartlsn,
				numRecordID,
				numDomainID,
				dtDate,
				numUserCntID,
				vcEvent,
				vcOldValue,
				vcNewValue,
				vcDescription,
				vcHiddenDescription,
				numFieldID,
				bitCustomField
			)
			VALUES
			(
				@vbStartlsn,
				@numOppID,
				@numDomainID,
				@bintModifiedDate,
				@numModifiedBy,
				'Change',
				'',
				dbo.GetCustFldValue(@Fld_ID,NULL,@numOppID),
				'',
				'',
				@Fld_ID,
				1
			)
		END
		ELSE IF @tintOperation = 3 --UPDATE
		BEGIN 
			INSERT INTO @TEMPOpportunityHistory
			(
				vbStartlsn,
				numRecordID,
				numDomainID,
				dtDate,
				numUserCntID,
				vcEvent,
				vcOldValue,
				vcNewValue,
				numFieldID,
				vcFieldName,
				bitCustomField
			)
			SELECT 
				@vbStartlsn,
				@numOppID,
				@numDomainID,
				TempUpdateAfter.bintModifiedDate,
				TempUpdateAfter.numModifiedBy, 
				'Change',
				dbo.fn_GetCustFldStringValue(TempUpdateBefore.Fld_ID,@numOppID,TempUpdateBefore.old_value), 
				dbo.fn_GetCustFldStringValue(TempUpdateBefore.Fld_ID,@numOppID,TempUpdateAfter.new_value),
				TempUpdateBefore.Fld_ID, 
				(SELECT Fld_label FROM CFW_Fld_Master DFM WHERE DFM.Fld_id = TempUpdateBefore.Fld_ID) vcFieldName,
				1
			FROM
				( 
					SELECT 
						__$start_lsn, 
						Fld_id,
						RecId,
						numModifiedBy,
						bintModifiedDate,
						old_value
					FROM 
					(
						SELECT 
							__$start_lsn,
							Fld_id,
							RecId,
							numModifiedBy,
							bintModifiedDate,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_CFW_Fld_Values_Opp','Fld_Value'),__$update_mask) = 1) THEN CAST(Fld_Value AS VARCHAR(MAX)) ELSE NULL END AS Fld_Value
						FROM 
							cdc.fn_cdc_get_all_changes_dbo_CFW_Fld_Values_Opp(@vbStartlsn,@vbStartlsn, N'all update old')
						WHERE 
							__$operation = 3
					) as t1
					UNPIVOT 
					(
						old_value FOR column_name IN (Fld_Value) 
					) as unp
				) as TempUpdateBefore
				INNER JOIN
				(
					SELECT 
						__$start_lsn,
						Fld_id,
						RecId,
						numModifiedBy,
						bintModifiedDate,
						new_value
					FROM 
					(
						SELECT 
							__$start_lsn,
							Fld_id,
							RecId,
							numModifiedBy,
							bintModifiedDate,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_CFW_Fld_Values_Opp','Fld_Value'),__$update_mask) = 1) THEN CAST(Fld_Value AS VARCHAR(MAX)) ELSE NULL END AS Fld_Value
						FROM 
							cdc.fn_cdc_get_all_changes_dbo_CFW_Fld_Values_Opp(@vbStartlsn, @vbStartlsn, N'all') -- 'all update old' is not necessary here
						WHERE 
							__$operation = 4
					) as t2
					UNPIVOT 
					(
						new_value FOR column_name IN (Fld_Value) 
					) as unp 
				) as TempUpdateAfter -- after update
				ON 
					TempUpdateBefore.__$start_lsn = TempUpdateAfter.__$start_lsn 
					AND TempUpdateBefore.Fld_id = TempUpdateAfter.Fld_id
					AND TempUpdateBefore.RecId = TempUpdateAfter.RecId
		END

		SET @i = @i + 1
	END

	--CLEAR RETRIVED DATA FROM CDC TABLE
	DELETE FROM [cdc].[dbo_CFW_Fld_Values_Opp_CT] WHERE __$start_lsn IN (SELECT vbStartlsn FROM @TEMPOpportunityHistory)

	BEGIN TRY
	BEGIN TRANSACTION

		--INSERT DATA INTO OUR RECORD HISTORY TABLE
		INSERT INTO RecordHistory
		(
			numDomainID,
			numUserCntID,
			dtDate,
			numRecordID,
			numRHModuleMasterID,
			vcEvent,
			numFieldID,
			vcFieldName,
			bitCustomField,
			vcOldValue,
			vcNewValue,
			vcDescription,
			vcHiddenDescription
		)
		SELECT
			numDomainID,
			numUserCntID,
			ISNULL(dtDate,GETUTCDATE()),
			numRecordID,
			3,
			vcEvent,
			numFieldID,
			vcFieldName,
			bitCustomField,
			vcOldValue,
			vcNewValue,
			vcDescription,
			vcHiddenDescription
		FROM
			@TEMPOpportunityHistory
	COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000)
		DECLARE @ErrorNumber INT
		DECLARE @ErrorSeverity INT
		DECLARE @ErrorState INT
		DECLARE @ErrorLine INT
		DECLARE @ErrorProcedure NVARCHAR(200)

		IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorNumber = ERROR_NUMBER(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE(),
			@ErrorLine = ERROR_LINE(),
			@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

		RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
	END CATCH
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_RecordHistory_Get')
DROP PROCEDURE dbo.USP_RecordHistory_Get
GO
CREATE PROCEDURE [dbo].[USP_RecordHistory_Get]
(
	@numDomainID NUMERIC(18,0),
	@numModuleID NUMERIC(18,0),
	@numRecordID NUMERIC(18,0),
	@PageSize INT,
	@PageNumber INT,
	@TotalRecords INT OUTPUT
)
AS
BEGIN
	SELECT 
		@TotalRecords = COUNT(*) 
	FROM 
		RecordHistory 
	WHERE 
		RecordHistory.numDomainID = @numDomainID
		AND RecordHistory.numRHModuleMasterID = @numModuleID
		AND RecordHistory.numRecordID = @numRecordID

	SELECT 
		ISNULL(RecordHistory.vcEvent,'') vcEvent,
		ISNULL(UserMaster.vcUserName,'') vcUserName,
		ISNULL(RecordHistory.vcFieldName,'') vcFieldName,
		ISNULL(RecordHistory.vcOldValue,'') vcOldValue,
		ISNULL(RecordHistory.vcNewValue,'') vcNewValue,
		ISNULL(RecordHistory.vcDescription,'') vcDescription
	FROM 
		RecordHistory 
	LEFT JOIN
		UserMaster
	ON
		RecordHistory.numUserCntID = UserMaster.numUserDetailId
	WHERE
		RecordHistory.numDomainID = @numDomainID
		AND RecordHistory.numRHModuleMasterID = @numModuleID
		AND RecordHistory.numRecordID = @numRecordID
	ORDER BY 
		RecordHistory.dtDate DESC
	OFFSET 
		@PageSize * (@PageNumber - 1) ROWS
    FETCH NEXT 
		@PageSize
	ROWS ONLY OPTION (RECOMPILE);
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_RecordHistoryStatus_Get')
DROP PROCEDURE dbo.USP_RecordHistoryStatus_Get
GO
CREATE PROCEDURE [dbo].[USP_RecordHistoryStatus_Get]

AS
BEGIN
	SELECT * FROM RecordHistoryStatus WHERE Id=1
END

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_RecordHistoryStatus_Update')
DROP PROCEDURE dbo.USP_RecordHistoryStatus_Update
GO
CREATE PROCEDURE [dbo].[USP_RecordHistoryStatus_Update]
	@bitSuccess BIT
AS
BEGIN
	UPDATE 
		RecordHistoryStatus 
	SET 
		dtLastExecuted = GETDATE(),
		intNoOfTimesTried = (CASE WHEN @bitSuccess = 1 THEN 0 ELSE intNoOfTimesTried + 1 END),
		bitSuccess = @bitSuccess
	WHERE 
		Id = 1
END
/****** Object:  StoredProcedure [dbo].[usp_SetUsersWithDomains]    Script Date: 07/26/2008 16:21:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_setuserswithdomains')
DROP PROCEDURE usp_setuserswithdomains
GO
CREATE  PROCEDURE [dbo].[usp_SetUsersWithDomains]
 @numUserID NUMERIC(9),                                    
 @vcUserName VARCHAR(50),                                    
 @vcUserDesc VARCHAR(250),                              
 @numGroupId as numeric(9),                                           
 @numUserDetailID numeric ,                              
@numUserCntID as numeric(9),                              
@strTerritory as varchar(4000) ,                              
@numDomainID as numeric(9),
@strTeam as varchar(4000),
@vcEmail as varchar(100),
@vcPassword as varchar(100),          
@Active as BIT,
@numDefaultClass NUMERIC,
@numDefaultWarehouse as numeric(18),
@vcLinkedinId varchar(300)=null,
@intAssociate int=0
AS
BEGIN

            
if @numUserID=0             
begin 

DECLARE @APIPublicKey varchar(20)
SELECT @APIPublicKey = dbo.GenerateBizAPIPublicKey() 
           
 insert into UserMaster(vcUserName,vcUserDesc,numGroupId,numUserDetailId,numModifiedBy,bintModifiedDate,
	vcEmailID,vcPassword,numDomainID,numCreatedBy,bintCreatedDate,bitActivateFlag,numDefaultClass,numDefaultWarehouse,vcBizAPIPublicKey,vcBizAPISecretKey,bitBizAPIAccessEnabled,vcLinkedinId,intAssociate)
 values(@vcUserName,@vcUserDesc,@numGroupId,@numUserDetailID,@numUserCntID, getutcdate(),
	@vcEmail,@vcPassword,@numDomainID,@numUserCntID,getutcdate(),@Active,@numDefaultClass,@numDefaultWarehouse,@APIPublicKey,NEWID(),0,@vcLinkedinId,@intAssociate)            
 set @numUserID=@@identity          
           
INSERT INTO BizAPIThrottlePolicy 
(
	[RateLimitKey],
	[bitIsIPAddress],
	[numPerSecond],
	[numPerMinute],
	[numPerHour],
	[numPerDay],
	[numPerWeek]
)
VALUES
(
	@APIPublicKey,
	0,
	3,
	60,
	1200,
	28800,
	200000
)
    
EXECUTE [dbo].[Resource_Add]   @vcUserName  ,@vcUserDesc  ,@vcEmail  ,1  ,@numDomainID  ,@numUserDetailID      
      
      
end            
else            
begin            
   UPDATE UserMaster SET vcUserName = @vcUserName,                                    
    vcUserDesc = @vcUserDesc,                              
    numGroupId =@numGroupId,                                    
    numUserDetailId = @numUserDetailID ,                              
    numModifiedBy= @numUserCntID ,                              
    bintModifiedDate= getutcdate(),                          
    vcEmailID=@vcEmail,            
    vcPassword=@vcPassword,          
    bitActivateFlag=@Active,
    numDefaultClass=@numDefaultClass,
    numDefaultWarehouse=@numDefaultWarehouse,
	vcLinkedinId=@vcLinkedinId,
	intAssociate=@intAssociate
    WHERE numUserID = @numUserID          
          
  
 if not exists (select * from resource where numUserCntId = @numUserDetailID and numdomainId = @numDomainId)  
 begin   
  EXECUTE [dbo].[Resource_Add]   @vcUserName  ,@vcUserDesc  ,@vcEmail  ,1  ,@numDomainID  ,@numUserDetailID     
 end  
  
-- if not exists(select * from ExchangeUserDetails where numUserID=@numUserID)          
-- begin          
--  insert into  ExchangeUserDetails(numUserID,bitExchangeIntegration,bitAccessExchange,vcExchPassword ,vcExchPath,vcExchDomain)            
--  values(@numUserID,@bitExchangeIntegration,@bitAccessExchange,@vcExchPassword,@vcExchPath,@vcExchDomain)          
-- end          
-- else          
-- begin          
--  update ExchangeUserDetails set           
--     bitExchangeIntegration=@bitExchangeIntegration,          
--     bitAccessExchange=@bitAccessExchange,          
--     vcExchPassword =@vcExchPassword,          
--     vcExchPath=@vcExchPath,          
--     vcExchDomain=@vcExchDomain          
--  where numUserID=@numUserID          
-- end      
end            
    
--if not exists (select * from [ImapUserDetails] where numDomainId = @numDomainId and numUserCntId= @numUserCntID  )    
-- begin    
--  INSERT INTO [ImapUserDetails]    
--           ( bitImap    
--     ,[vcImapServerUrl]    
--           ,[vcImapPassword]    
--           ,[bitSSl]    
--           ,[numPort]    
--           ,[numDomainId]    
--           ,[numUserCntId])    
--     VALUES    
--           (@bitImap    
--     ,@vcImapServerUrl    
--           ,@vcImapPassword    
--           ,@bitImapSsl    
--           ,@numImapSSLPort    
--           ,@numDomainID    
--           ,@numUserDetailID)    
-- end    
--else    
-- begin    
--  UPDATE [ImapUserDetails]    
--     SET [bitImap] = @bitImap    
--     ,[vcImapServerUrl] = @vcImapServerUrl    
--     ,[vcImapPassword] = @vcImapPassword    
--     ,[bitSSl] = @bitImapSsl    
--     ,[numPort] = @numImapSSLPort    
--     ,[numDomainId] =@numDomainID    
--     ,[numUserCntId] = @numUserDetailID    
--   WHERE [numUserCntId] = @numUserDetailID and numDomainId = @numDomainID    
-- End      
                       
                               
                              
                              
declare @separator_position as integer                              
declare @strPosition as varchar(1000)                 
                            
                              
delete from UserTerritory where numUserCntID = @numUserDetailID                              
and numDomainId=@numDomainID                              
          
                              
   while patindex('%,%' , @strTerritory) <> 0                              
    begin -- Begin for While Loop                              
      select @separator_position =  patindex('%,%' , @strTerritory)                              
    select @strPosition = left(@strTerritory, @separator_position - 1)                              
       select @strTerritory = stuff(@strTerritory, 1, @separator_position,'')                              
     insert into UserTerritory (numUserCntID,numTerritoryID,numDomainID)                              
     values (@numUserDetailID,@strPosition,@numDomainID)                              
                               
                                
   end                              
                                           
                              
                              
delete from UserTeams where numUserCntID = @numUserDetailID                              
and numDomainId=@numDomainID                              
                               
                              
   while patindex('%,%' , @strTeam) <> 0                              
    begin -- Begin for While Loop                              
      select @separator_position =  patindex('%,%' , @strTeam)                              
    select @strPosition = left(@strTeam, @separator_position - 1)                  
       select @strTeam = stuff(@strTeam, 1, @separator_position,'')                              
     insert into UserTeams (numUserCntID,numTeam,numDomainID)                              
     values (@numUserDetailID,@strPosition,@numDomainID)                              
                               
                                
   end                 
                
                
--delete from EmailUsers where numUserCntID = @numUserDetailID                                            
--                                       
--   while patindex('%,%' , @strEmail) <> 0                         
--    begin -- Begin for While Loop                              
--      select @separator_position =  patindex('%,%' , @strEmail)                              
--    select @strPosition = left(@strEmail, @separator_position - 1)                              
--       select @strEmail = stuff(@strEmail, 1, @separator_position,'')                              
--     insert into EmailUsers (numContactID,numUserCntID)                              
--     values (@strPosition,@numUserDetailID)                              
--   end                             
                                 
                              
delete from ForReportsByTeam where  numUserCntID = @numUserDetailID                              
and numDomainId=@numDomainID and numTeam not in (select numTeam from UserTeams where numUserCntID = @numUserCntID                              
and numDomainId=@numDomainID )                              
                              
                                  
END
/****** Object:  StoredProcedure [dbo].[USP_ShoppingCart]    Script Date: 05/07/2009 22:09:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_shoppingcart')
DROP PROCEDURE usp_shoppingcart
GO
CREATE PROCEDURE [dbo].[USP_ShoppingCart]                            
@numDivID as numeric(18),                            
@numOppID as numeric(18)=null output ,                                              
@vcPOppName as varchar(200)='' output,  
@numContactId as numeric(18),                            
@numDomainId as numeric(18),
@vcSource VARCHAR(50)=NULL,
@numCampainID NUMERIC(9)=0,
@numCurrencyID as numeric(9),
@numBillAddressId numeric(9),
@numShipAddressId numeric(9),
@bitDiscountType BIT,
@fltDiscount FLOAT,
@numProId NUMERIC(9),
@numSiteID NUMERIC(9),
@tintOppStatus INT,
@monShipCost MONEY,
@tintSource AS BIGINT ,
@tintSourceType AS TINYINT ,                          
@numPaymentMethodId NUMERIC(9),
@txtComments varchar(1000)

as                            
declare @CRMType as integer               
declare @numRecOwner as integer                            
if @numOppID=  0                             
begin                            
select @CRMType=tintCRMType,@numRecOwner=numRecOwner from DivisionMaster where numDivisionID=@numDivID                            
if @CRMType= 0                             
begin                            
UPDATE DivisionMaster                               
   SET tintCRMType=1                              
   WHERE numDivisionID=@numDivID                              
                                                   
end                            
declare @TotalAmount as FLOAT
--declare @intOppTcode as numeric(9) 
--declare @numCurrencyID as numeric(9)
--declare @fltExchangeRate as float                          
--Select @intOppTcode=max(numOppId)from OpportunityMaster                              
--set @intOppTcode =isnull(@intOppTcode,0) + 1                              

DECLARE @numTemplateID NUMERIC
SELECT  @numTemplateID= numMirrorBizDocTemplateId FROM eCommercePaymentConfig WHERE  numSiteId=@numSiteID AND numPaymentMethodId = @numPaymentMethodId AND numDomainID = @numDomainID   

   Declare @fltExchangeRate float                                 
    
   IF @numCurrencyID=0 select @numCurrencyID=isnull(numCurrencyID,0) from Domain where numDomainID=@numDomainId                            
   if @numCurrencyID=0 set   @fltExchangeRate=1
  else set @fltExchangeRate=dbo.GetExchangeRate(@numDomainID,@numCurrencyID)                          
                            
  
	 -- GET ACCOUNT CLASS IF ENABLED
	DECLARE @numAccountClass AS NUMERIC(18) = 0

	DECLARE @tintDefaultClassType AS INT = 0
	SELECT @tintDefaultClassType = ISNULL(tintDefaultClassType,0) FROM Domain WHERE numDomainId = @numDomainID 

	IF @tintDefaultClassType = 1 --USER
	BEGIN
		SELECT 
			@numAccountClass=ISNULL(numDefaultClass,0) 
		FROM 
			dbo.UserMaster UM 
		JOIN 
			dbo.Domain D 
		ON 
			UM.numDomainID=D.numDomainId
		WHERE 
			D.numDomainId=@numDomainId 
			AND UM.numUserDetailId=@numContactId
	END
	ELSE IF @tintDefaultClassType = 2 --COMPANY
	BEGIN
		SELECT 
			@numAccountClass=ISNULL(numAccountClassID,0) 
		FROM 
			dbo.DivisionMaster DM 
		WHERE 
			DM.numDomainId=@numDomainId 
			AND DM.numDivisionID=@numDivID
	END
	ELSE
	BEGIN
		SET @numAccountClass = 0
	END
                            
--set @vcPOppName=@vcPOppName+ '-'+convert(varchar(4),year(getutcdate()))  +'-A'+ convert(varchar(10),@intOppTcode)                              
  --SELECT tintSource , tintSourceType FROM dbo.OpportunityMaster
insert into OpportunityMaster                              
  (                              
  numContactId,                              
  numDivisionId,                              
  txtComments,                              
  numCampainID,                              
  bitPublicFlag,                              
  tintSource, 
  tintSourceType ,                               
  vcPOppName,                              
  monPAmount,                              
  numCreatedBy,                              
  bintCreatedDate,                               
  numModifiedBy,                              
  bintModifiedDate,                              
  numDomainId,                               
  tintOppType, 
  tintOppStatus,                   
  intPEstimatedCloseDate,              
  numRecOwner,
  bitOrder,
  numCurrencyID,
  fltExchangeRate,fltDiscountTotal,bitDiscountTypeTotal,
  monShipCost,
  numOppBizDocTempID,numAccountClass
  )                              
 Values                              
  (                              
  @numContactId,                              
  @numDivID,                              
  @txtComments,                              
  @numCampainID,--  0,
  0,                              
  @tintSource,   
  @tintSourceType,                           
  ISNULL(@vcPOppName,'SO'),                             
  0,                                
  @numContactId,                              
  getutcdate(),                              
  @numContactId,                              
  getutcdate(),        
  @numDomainId,                              
  1,             
  @tintOppStatus,       
  getutcdate(),                                 
  @numRecOwner ,
  1,
  @numCurrencyID,
  @fltExchangeRate,@fltDiscount,@bitDiscountType
  ,@monShipCost,
  @numTemplateID,@numAccountClass
  
  )                               
set @numOppID=scope_identity()                             
--Update OppName as per Name Template
EXEC dbo.USP_UpdateNameTemplateValue 1,@numDomainId,@numOppID
SELECT @vcPOppName = vcPOppName FROM OpportunityMaster WHERE numOppID= @numOppID
end
delete  from    OpportunityItems where   numOppId= @numOppID                               

--if convert(varchar(10),@strItems) <>''                
--begin               
-- DECLARE @hDocItem int                
-- EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems                
                
insert into OpportunityItems                                                        
  (numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,vcAttributes,vcAttrValues,[vcItemName],[vcModelID],[vcManufacturer],vcPathForTImage, monVendorCost,numUOMId,bitDiscountType,fltDiscount,monTotAmtBefDiscount)                                                        
  --kishan                                                       
 select @numOppID,X.numItemCode,x.numUnitHour * x.decUOMConversionFactor,x.monPrice/x.decUOMConversionFactor,x.monTotAmount,X.vcItemDesc, NULLIF(X.numWarehouseItmsID,0),X.vcItemType,X.vcAttributes,X.vcAttrValues,(SELECT vcItemName FROM item WHERE numItemCode = X.numItemCode),(SELECT vcModelID FROM item WHERE numItemCode = X.numItemCode),(SELECT vcManufacturer FROM item WHERE numItemCode = X.numItemCode),
		(SELECT TOP 1  vcPathForTImage FROM dbo.ItemImages WHERE numItemCode = X.numItemCode AND bitDefault = 1 AND numDomainId = @numDomainId),   (select isnull(VN.monCost,0) from Vendor VN, Item IT where IT.numItemCode=VN.numItemCode and  VN.numItemCode=X.numItemCode and  VN.numVendorID=IT.numVendorID), 
		numUOM,bitDiscountType,fltDiscount,monTotAmtBefDiscount 
FROM dbo.CartItems X WHERE numUserCntId =@numContactId

declare @tintShipped as tinyint      
DECLARE @tintOppType AS TINYINT
      
select @tintOppStatus=tintOppStatus,@tintOppType=tintOppType,@tintShipped=tintShipped from OpportunityMaster where numOppID=@numOppID              
if @tintOppStatus=1               
	begin         
	PRINT 'inside USP_UpdatingInventoryonCloseDeal'
		exec USP_UpdatingInventoryonCloseDeal @numOppID,@numContactId
	end              

/*Commented by Chintan..reason:tobe executed only when order is confirmed*/
--exec USP_UpdatingInventoryonCloseDeal @numOppID,@numContactId

-- SELECT * FROM OPENXML (@hDocItem,'/NewDataSet/Table',2)                                                        
-- WITH  (                                                        
--  numoppitemtCode numeric(9),                   
--  numItemCode numeric(9),                                                        
--  numUnitHour numeric(9),                                                        
--  monPrice money,                                                     
--  monTotAmount money,                                                        
--  vcItemDesc varchar(1000),                  
--  numWarehouseItmsID numeric(9),        
--  vcItemType varchar(30),    
--  vcAttrValues varchar(50) ,
--  numUOM NUMERIC(18,0),
--  decUOMConversionFactor DECIMAL(18,5),bitDiscountType BIT,fltDiscount FLOAT,monTotAmtBefDiscount MONEY,
--  vcShippingMethod VARCHAR(100),decShippingCharge MONEY,dtDeliveryDate datetime
--  ))X                
--                
                
-- EXEC sp_xml_removedocument @hDocItem                
                
--end
                             
                            
select @TotalAmount=sum(monTotAmount)from OpportunityItems where numOppId=@numOppID                            
 update OpportunityMaster set  monPamount=@TotalAmount - @fltDiscount                           
 where numOppId=@numOppID                           
                          
--set @vcPOppName=(select  ISNULL(vcPOppName,numOppId) from OpportunityMaster where numOppId=@numOppID )

IF(@vcSource IS NOT NULL)
BEGIN
	insert into OpportunityLinking ([numParentOppID],[numChildOppID],[vcSource],[numParentOppBizDocID],numPromotionId,numSiteID)  values(null,@numOppID,@vcSource,NULL,@numProId,@numSiteID);
END

--Add/Update Address Details
DECLARE @vcStreet  varchar(100),@vcCity  varchar(50),@vcPostalCode  varchar(15),@vcCompanyName  varchar(100),@vcAddressName VARCHAR(50)      
DECLARE @numState  numeric(9),@numCountry  numeric(9), @numCompanyId  numeric(9) 
DECLARE @bitIsPrimary BIT;

--Bill Address
IF @numBillAddressId>0
BEGIN
 SELECT  @vcStreet=ISNULL(vcStreet,''),@vcCity=ISNULL(vcCity,''),@vcPostalCode=ISNULL(vcPostalCode,''),
		@numState=ISNULL(numState,0),@numCountry=ISNULL(numCountry,0),@bitIsPrimary=bitIsPrimary,
		@vcAddressName=vcAddressName
            FROM  dbo.AddressDetails WHERE   numDomainID = @numDomainID AND numAddressID = @numBillAddressId 
            
  
  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 0, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = '', --  varchar(100)
	@numCompanyId = 0, --  numeric(9, 0)
	@vcAddressName =@vcAddressName
	
        
END
  
  
  --Ship Address
IF @numShipAddressId>0
BEGIN
   	SELECT  @vcStreet=ISNULL(vcStreet,''),@vcCity=ISNULL(vcCity,''),@vcPostalCode=ISNULL(vcPostalCode,''),
		@numState=ISNULL(numState,0),@numCountry=ISNULL(numCountry,0),@bitIsPrimary=bitIsPrimary,
		@vcAddressName=vcAddressName
            FROM  dbo.AddressDetails WHERE   numDomainID = @numDomainID AND numAddressID = @numShipAddressId 
  
  
	select  @vcCompanyName=vcCompanyName,@numCompanyId=div.numCompanyID from CompanyInfo Com                            
	join divisionMaster Div                            
	on div.numCompanyID=com.numCompanyID                            
	where div.numdivisionID=@numDivID

  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 1, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = @vcCompanyName, --  varchar(100)
	@numCompanyId =@numCompanyId, --  numeric(9, 0)
	@vcAddressName =@vcAddressName
	
   	
END

--Insert Tax for Division                       
INSERT dbo.OpportunityMasterTaxItems 
(
	numOppId,
	numTaxItemID,
	fltPercentage,
	tintTaxType,
	numTaxID
) 
SELECT 
	@numOppID,
	TI.numTaxItemID,
	TEMPTax.decTaxValue,
	TEMPTax.tintTaxType,
	numTaxID
FROM 
	TaxItems TI 
JOIN 
	DivisionTaxTypes DTT 
ON 
	TI.numTaxItemID = DTT.numTaxItemID
OUTER APPLY
(
	SELECT
		decTaxValue,
		tintTaxType,
		numTaxID
	FROM
		dbo.fn_CalItemTaxAmt(@numDomainID,@numDivID,0,TI.numTaxItemID,@numOppId,0,NULL)
) AS TEMPTax
WHERE 
	DTT.numDivisionID=@numDivID AND DTT.bitApplicable=1
UNION 
SELECT 
	@numOppID,
	0,
	TEMPTax.decTaxValue,
	TEMPTax.tintTaxType,
	numTaxID
FROM 
	dbo.DivisionMaster 
OUTER APPLY
(
	SELECT
		decTaxValue,
		tintTaxType,
		numTaxID
	FROM
		dbo.fn_CalItemTaxAmt(@numDomainID,@numDivID,0,0,@numOppId,1,NULL)
) AS TEMPTax
WHERE 
	bitNoTax=0 
	AND numDivisionID=@numDivID

-- DELETE ITEM LEVEL CRV TAX TYPES
DELETE FROM OpportunityMasterTaxItems WHERE numOppId=@numOppId AND numTaxItemID = 1

-- INSERT ITEM LEVEL CRV TAX TYPES
INSERT INTO OpportunityMasterTaxItems
(
	numOppId,
	numTaxItemID,
	fltPercentage,
	tintTaxType,
	numTaxID
) 
SELECT 
	OI.numOppId,
	1,
	TEMPTax.decTaxValue,
	TEMPTax.tintTaxType,
	TEMPTax.numTaxID
FROM 
	OpportunityItems OI
INNER JOIN
	ItemTax IT
ON
	IT.numItemCode = OI.numItemCode
OUTER APPLY
(
	SELECT
		decTaxValue,
		tintTaxType,
		numTaxID
	FROM
		dbo.fn_CalItemTaxAmt(@numDomainID,@numDivID,OI.numItemCode,IT.numTaxItemID,@numOppId,1,NULL)
) AS TEMPTax
WHERE
	OI.numOppId = @numOppID
	AND IT.numTaxItemID = 1 -- CRV TAX
GROUP BY
	OI.numOppId,
	TEMPTax.decTaxValue,
	TEMPTax.tintTaxType,
	TEMPTax.numTaxID
  
  --Delete Tax for Opportunity Items if item deleted 
DELETE FROM OpportunityItemsTaxItems WHERE numOppId=@numOppId AND numOppItemID NOT IN (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID)

--Insert Tax for Opportunity Items
INSERT INTO dbo.OpportunityItemsTaxItems 
(
	numOppId,
	numOppItemID,
	numTaxItemID,
	numTaxID
) 
SELECT 
	@numOppId,
	OI.numoppitemtCode,
	TI.numTaxItemID,
	0 
FROM 
	dbo.OpportunityItems OI 
JOIN 
	dbo.ItemTax IT 
ON 
	OI.numItemCode=IT.numItemCode 
JOIN
	TaxItems TI 
ON 
	TI.numTaxItemID=IT.numTaxItemID 
WHERE 
	OI.numOppId=@numOppID 
	AND IT.bitApplicable=1 
	AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
UNION
SELECT 
	@numOppId,
	OI.numoppitemtCode,
	0,
	0 
FROM 
	dbo.OpportunityItems OI 
JOIN 
	dbo.Item I 
ON 
	OI.numItemCode=I.numItemCode
WHERE 
	OI.numOppId=@numOppID 
	AND I.bitTaxable=1 
	AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
UNION
SELECT
	@numOppId,
	OI.numoppitemtCode,
	1,
	TD.numTaxID
FROM
	dbo.OpportunityItems OI 
INNER JOIN
	ItemTax IT
ON
	IT.numItemCode = OI.numItemCode
INNER JOIN
	TaxDetails TD
ON
	TD.numTaxID = IT.numTaxID
	AND TD.numDomainId = @numDomainId
WHERE
	OI.numOppId = @numOppID
	AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
  
 

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_SimpleSearch')
DROP PROCEDURE USP_SimpleSearch
GO
CREATE PROCEDURE [dbo].[USP_SimpleSearch]                             
	@numDomainID NUMERIC (18,0),
	@numUserCntID NUMERIC(18,0),
	@searchText VARCHAR(100),
	@searchType VARCHAR(1),
	@searchCriteria VARCHAR(1),
	@orderType VARCHAR(1),
	@bizDocType AS INT ,
	@numSkip AS INT,@isStartWithSearch as int                                                                  
AS                          
	DECLARE @strSQL AS VARCHAR(MAX) = ''
	
	IF @searchType = '1' -- Organization
	BEGIN
		IF @searchCriteria = '1' --Organizations
		BEGIN
			EXEC USP_CompanyInfo_Search @numDomainID=@numDomainID,@numUserCntID=@numUserCntID,@isStartWithSearch=@isStartWithSearch,@searchText=@searchText
		END
		ELSE
		BEGIN
			--GET FIELDS CONFIGURED FOR ORGANIZATION SEARCH
			SELECT * INTO #tempOrgSearch FROM
			(
				SELECT 
					numFieldId,
					vcDbColumnName,
					ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName,
					vcLookBackTableName,
					tintRow AS tintOrder,
					0 as Custom
				FROM 
					View_DynamicColumns
				WHERE 
					numFormId=97 AND numUserCntID=@numUserCntID AND numDomainID=@numDomainID AND 
					tintPageType=1 AND bitCustom=0  AND ISNULL(bitSettingField,0)=1 AND numRelCntType=0
				UNION   
				SELECT 
					numFieldId,
					vcFieldName,
					vcFieldName,
					'CFW_FLD_Values',
					tintRow AS tintOrder,
					1 as Custom
				FROM 
					View_DynamicCustomColumns   
				WHERE 
					Grp_id=1 AND numFormId=97 AND numUserCntID=@numUserCntID AND numDomainID=@numDomainID AND 
					tintPageType=1 AND bitCustom=1 AND numRelCntType=0
			)Y
			
			DECLARE @searchSQL AS VARCHAR(MAX) = ''

			IF (SELECT COUNT(*) FROM #tempOrgSearch) > 0
			BEGIN
				if(@isStartWithSearch=0)
				begin
				SELECT  
					@searchSQL = STUFF(
										(SELECT ' OR ' + + CONCAT((CASE WHEN vcLookBackTableName = 'AddressDetails' THEN '' ELSE CONCAT(vcLookBackTableName,'.') END),IIF(Custom = 1,'Fld_Value',vcDbColumnName)) + ' LIKE ''%' + @searchText + '%''' FROM #tempOrgSearch FOR XML PATH(''))
										, 1
										, 4
										, ''
										)
				end
				else 
				begin
					SELECT  
					@searchSQL = STUFF(
										(SELECT ' OR ' + + CONCAT((CASE WHEN vcLookBackTableName = 'AddressDetails' THEN '' ELSE CONCAT(vcLookBackTableName,'.') END),IIF(Custom = 1,'Fld_Value',vcDbColumnName)) + ' LIKE '+ @searchText + '%''' FROM #tempOrgSearch FOR XML PATH(''))
										, 1
										, 4
										, ''
										)
				end
			END
			ELSE
			BEGIN
				if(@isStartWithSearch=0)
				begin
					SET @searchSQL = 'CompanyInfo.vcCompanyName LIKE ''%' + @searchText + '%'''
				end
				else
				begin
					SET @searchSQL = 'CompanyInfo.vcCompanyName LIKE '+ @searchText + '%'''
				end
			END

			CREATE TABLE #TEMPOrganization
			(
				numDivisionID NUMERIC(18,0),
				vcCompanyName VARCHAR(500)
			)


			SET @strSQL = 'INSERT INTO 
								#TEMPOrganization
							SELECT
								DivisionMaster.numDivisionID
								,CompanyInfo.vcCompanyName
							FROM
								DivisionMaster 
							INNER JOIN
								CompanyInfo 
							ON
								CompanyInfo.numCompanyId = DivisionMaster.numCompanyID
							LEFT JOIN
								AdditionalContactsInformation
							ON
								AdditionalContactsInformation.numDivisionId = DivisionMaster.numDivisionID
							LEFT JOIN
								OpportunityMaster
							ON
								OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
							LEFT JOIN
								Cases
							ON
								Cases.numDivisionID = DivisionMaster.numDivisionID
							LEFT JOIN
								ProjectsMaster
							ON
								ProjectsMaster.numDivisionId = DivisionMaster.numDivisionID
							OUTER APPLY 
								(
									SELECT
										isnull(vcStreet,'''') as vcBillStreet, 
										isnull(vcCity,'''') as vcBillCity, 
										isnull(dbo.fn_GetState(numState),'''') as numBillState,
										isnull(vcPostalCode,'''') as vcBillPostCode,
										isnull(dbo.fn_GetListName(numCountry,0),'''')  as numBillCountry
									FROM
										dbo.AddressDetails 
									WHERE 
										numDomainID = ' + CAST(@numDomainID AS VARCHAR) + '
										AND numRecordID=DivisionMaster.numDivisionID 
										AND tintAddressOf=2 
										AND tintAddressType=1 
								)  AD1
							OUTER APPLY
								(
									SELECT
										isnull(vcStreet,'''') as vcShipStreet,
										isnull(vcCity,'''') as vcShipCity,
										isnull(dbo.fn_GetState(numState),'''')  as numShipState,
										isnull(vcPostalCode,'''') as vcShipPostCode, 
										isnull(dbo.fn_GetListName(numCountry,0),'''') numShipCountry
									FROM
										dbo.AddressDetails 
									WHERE 
										numDomainID = ' + CAST(@numDomainID AS VARCHAR) + '
										AND numRecordID= DivisionMaster.numDivisionID 
										AND tintAddressOf=2 
										AND tintAddressType=2	
								) AD2	
							LEFT JOIN
								CFW_FLD_Values
							ON
								CFW_FLD_Values.RecId = DivisionMaster.numDivisionID
								AND CFW_FLD_Values.Fld_ID IN (SELECT numFieldID FROM #tempOrgSearch)
							WHERE
								DivisionMaster.numDomainID = ' + CAST(@numDomainID AS VARCHAR) + '
								AND (' + @searchSQL + ')
							GROUP BY
								DivisionMaster.numDivisionID
								,CompanyInfo.vcCompanyName'

			PRINT @strSQL
			EXEC (@strSQL)
			IF @searchCriteria = '2' --Items
			BEGIN
				DECLARE @TEMPORGITEM TABLE
				(
					numOppId NUMERIC(18,0),
					CreatedDate DATETIME,
					vcItemName VARCHAR(500),
					vcPOppName VARCHAR(200),
					vcOppType VARCHAR(50),
					numUnitHour FLOAT,
					monPrice NUMERIC(18,2),
					vcCompanyName  VARCHAR(200),
					tintType TINYINT
				)

				INSERT INTO @TEMPORGITEM
				(
					numOppId,
					CreatedDate,
					vcItemName,
					vcPOppName,
					vcOppType,
					numUnitHour,
					monPrice,
					vcCompanyName,
					tintType
				)
				SELECT
					numOppId,
					bintCreatedDate,
					vcItemName,
					vcPOppName,
					vcOppType,
					CAST(numUnitHour AS FLOAT) numUnitHour,
					CAST(monPrice AS decimal(18,2)) monPrice,
					vcCompanyName,
					tintType
				FROM
				(
					SELECT
						OpportunityMaster.numOppId,
						OpportunityMaster.bintCreatedDate,
						CONCAT(Item.vcItemName,',',Item.vcModelID) AS vcItemName,
						OpportunityMaster.vcPOppName,
						CASE 
							WHEN tintOppType = 1 THEN 'Sales ' + (CASE WHEN tintOppStatus = 1 THEN 'Order' ELSE 'Opportunity' END)
							WHEN tintOppType = 2 THEN 'Purchase ' + (CASE WHEN tintOppStatus = 1 THEN 'Order' ELSE 'Opportunity' END)
						END vcOppType,
						OpportunityItems.numUnitHour,
						OpportunityItems.monPrice,
						TEMP.vcCompanyName,
						1 AS tintType
					FROM
						OpportunityMaster
					INNER JOIN
						#TEMPOrganization TEMP
					ON
						OpportunityMaster.numDivisionId = TEMP.numDivisionID
					INNER JOIN
						OpportunityItems
					ON
						OpportunityMaster.numOppId = OpportunityItems.numOppId
					INNER JOIN
						Item
					ON
						OpportunityItems.numItemCode = Item.numItemCode
					WHERE
						OpportunityMaster.numDomainId = @numDomainID
					UNION
					SELECT
						ReturnHeader.numReturnHeaderID AS numOppID,
						ReturnHeader.dtCreatedDate AS bintCreatedDate,
						CONCAT(Item.vcItemName,',',Item.vcModelID) AS vcItemName,
						ReturnHeader.vcRMA AS vcPOppName,
						CASE 
							WHEN tintReturnType = 1 THEN 'Sales Return'
							WHEN tintReturnType = 2 THEN 'Purchase Return'
						END vcOppType,
						ReturnItems.numUnitHour,
						ReturnItems.monPrice,
						TEMP.vcCompanyName,
						2 AS tintType
					FROM
						ReturnHeader
					INNER JOIN
						#TEMPOrganization TEMP
					ON
						ReturnHeader.numDivisionId = TEMP.numDivisionID
					INNER JOIN	
						ReturnItems
					ON
						ReturnHeader.numReturnHeaderID = ReturnItems.numReturnHeaderID
					INNER JOIN
						Item
					ON
						Item.numItemCode = ReturnItems.numItemCode
					WHERE
						ReturnHeader.numDomainId = @numDomainID AND tintReturnType IN (1,2)
				) TEMPFinal

				SELECT  
					ISNULL(numOppId,0) AS numOppId,
					ISNULL(dbo.FormatedDateFromDate(CreatedDate,@numDomainID),'') bintCreatedDate,
					ISNULL(vcItemName,'') vcItemName,
					ISNULL(vcPOppName,'') vcPOppName,
					ISNULL(vcOppType,'') vcOppType,
					ISNULL(numUnitHour,0)numUnitHour,
					ISNULL(monPrice,0.00) monPrice,
					ISNULL(vcCompanyName,'') vcCompanyName,
					ISNULL(tintType,'') tintType
				FROM 
					@TEMPORGITEM 
				ORDER BY 
					CreatedDate DESC 
				OFFSET @numSkip ROWS FETCH NEXT 10 ROWS ONLY

				SELECT COUNT(numOppId) AS TotalItems FROM @TEMPORGITEM 
			END
			ELSE IF @searchCriteria = '3' -- Opp/Orders
			BEGIN			
				DECLARE @TEMPORGORDER TABLE
				(
					numOppId NUMERIC(18,0),
					CreatedDate DATETIME,
					vcPOppName VARCHAR(200),
					vcOppType VARCHAR(50),
					vcCompanyName  VARCHAR(200),
					tintType TINYINT
				)

				INSERT INTO 
					@TEMPORGORDER
				SELECT
					numOppId,
					bintCreatedDate,
					vcPOppName,
					vcOppType,
					vcCompanyName,
					tintType
				FROM
				(
					SELECT
						OpportunityMaster.numOppId,
						OpportunityMaster.vcPOppName,
						CASE 
							WHEN tintOppType = 1 THEN 'Sales ' + (CASE WHEN tintOppStatus = 1 THEN 'Order' ELSE 'Opportunity' END)
							WHEN tintOppType = 2 THEN 'Purchase ' + (CASE WHEN tintOppStatus = 1 THEN 'Order' ELSE 'Opportunity' END)
						END vcOppType,
						TEMP.vcCompanyName,
						bintCreatedDate,
						1 AS tintType
					FROM
						OpportunityMaster
					INNER JOIN
						#TEMPOrganization TEMP
					ON
						OpportunityMaster.numDivisionId = TEMP.numDivisionID
					WHERE
						numDomainId = @numDomainID
						AND tintOppType <> 0
					UNION 
					SELECT
						ReturnHeader.numReturnHeaderID AS numOppID,
						ReturnHeader.vcRMA AS vcPOppName,
						CASE 
							WHEN tintReturnType = 1 THEN 'Sales Return'
							WHEN tintReturnType = 2 THEN 'Purchase Return'
						END vcOppType,
						TEMP.vcCompanyName,
						dtCreatedDate AS bintCreatedDate,
						2 AS tintType
					FROM
						ReturnHeader
					INNER JOIN
						#TEMPOrganization TEMP
					ON
						ReturnHeader.numDivisionId = TEMP.numDivisionID
					WHERE
						numDomainId = @numDomainID AND tintReturnType IN (1,2)
				) TEMPFinal

				SELECT
					ISNULL(numOppId,0) numOppId,
					ISNULL(dbo.FormatedDateFromDate(CreatedDate,@numDomainID),'') bintCreatedDate,
					ISNULL(vcPOppName,'') vcPOppName,
					ISNULL(vcOppType,'') vcOppType,
					ISNULL(vcCompanyName,'') vcCompanyName,
					ISNULL(tintType,0) tintType
				FROM
					@TEMPORGORDER
				ORDER BY
					CreatedDate DESC
				OFFSET @numSkip ROWS FETCH NEXT 10 ROWS ONLY	
				
				SELECT COUNT(*) AS TotalRows FROM @TEMPORGORDER			
			END
			ELSE IF @searchCriteria = '4' -- BizDocs
			BEGIN
				DECLARE @TEMPORGBizDoc TABLE
				(
					numOppBizDocsId NUMERIC(18,0),
					CreatedDate DATETIME,
					vcBizDocID VARCHAR(200),
					vcBizDocType VARCHAR(100),
					vcCompanyName  VARCHAR(200)
				)

				INSERT INTO
					@TEMPORGBizDoc
				SELECT 
					numOppBizDocsId,
					OpportunityBizDocs.dtCreatedDate,
					vcBizDocID,
					ListDetails.vcData AS vcBizDocType,
					TEMP.vcCompanyName
				FROM 
					OpportunityBizDocs
				INNER JOIN
					OpportunityMaster
				ON
					OpportunityBizDocs.numOppId = OpportunityMaster.numOppId
				INNER JOIN
					#TEMPOrganization TEMP
				ON
					OpportunityMaster.numDivisionId = TEMP.numDivisionID
				INNER JOIN
					ListDetails
				ON
					ListDetails.numListID = 27
					AND OpportunityBizDocs.numBizDocId = ListDetails.numListItemID
				WHERE
					OpportunityMaster.numDomainId = @numDomainID

				SELECT
					ISNULL(numOppBizDocsId,0) numOppBizDocsId,
					ISNULL(dbo.FormatedDateFromDate(CreatedDate,@numDomainID),'') AS dtCreatedDate,
					ISNULL(vcBizDocID,'') vcBizDocID,
					ISNULL(vcBizDocType,'') vcBizDocType,
					ISNULL(vcCompanyName,'') vcCompanyName
				FROM
					@TEMPORGBizDoc
				ORDER BY
					CreatedDate DESC
				OFFSET @numSkip ROWS FETCH NEXT 10 ROWS ONLY
				
				SELECT COUNT(*) AS TotalRows FROM @TEMPORGBizDoc					
			END

			DROP TABLE #tempOrgSearch
			DROP TABLE #TEMPOrganization
		END
	END
	ELSE IF @searchType = '2' -- Item
	BEGIN
		CREATE TABLE #TempSearchedItem
		(
			numItemCode NUMERIC(18,0), 
			vcItemName VARCHAR(200), 
			vcPathForTImage VARCHAR(200), 
			vcCompanyName VARCHAR(200), 
			monListPrice NUMERIC(18,0), 
			txtItemDesc TEXT, 
			vcModelID VARCHAR(100), 
			numBarCodeId VARCHAR(100), 
			vcBarCode VARCHAR(100), 
			vcSKU VARCHAR(100),
			vcWHSKU VARCHAR(100), 
			vcPartNo VARCHAR(100),
			numWarehouseItemID NUMERIC(18,0), 
			vcAttributes VARCHAR(500)
		)

		DECLARE @strItemSearch AS VARCHAR(1000) = ''
		DECLARE @strAttributeSearch AS VARCHAR(1000) = ''
		DECLARE @strSearch AS VARCHAR(1000)

		IF CHARINDEX(',',@searchText) > 0
		BEGIN
			SET @strItemSearch = LTRIM(RTRIM(SUBSTRING(@searchText,0,CHARINDEX(',',@searchText))))
			SET @strAttributeSearch = SUBSTRING(@searchText,CHARINDEX(',',@searchText) + 1,LEN(@searchText)) 

			IF LEN(@strAttributeSearch) > 0
			BEGIN
				if(@isStartWithSearch=0)
				begin
					SET @strAttributeSearch = 'TEMPAttributes.vcAttributes LIKE ''%' + REPLACE(@strAttributeSearch,',','%'' AND TEMPAttributes.vcAttributes LIKE ''%') + '%'''
				end
				else
				begin
					SET @strAttributeSearch = 'TEMPAttributes.vcAttributes LIKE ''%' + REPLACE(@strAttributeSearch,',','%'' AND TEMPAttributes.vcAttributes LIKE ''%') + '%'''
				end
			END
			ELSE
			BEGIN
				SET @strAttributeSearch = ''
			END
		END
		ELSE
		BEGIN
			SET @strItemSearch = @searchText
		END

		DECLARE @vcCustomDisplayField AS VARCHAR(4000) = ''
		SELECT * INTO #TempItemCustomDisplayFields FROM
		(
			SELECT 
				numFieldId,
				vcFieldName
			FROM 
				View_DynamicCustomColumns   
			WHERE 
				Grp_id=5 AND numFormId=22 AND numUserCntID=@numUserCntID AND numDomainID=@numDomainID AND 
				tintPageType=1 AND bitCustom=1 AND numRelCntType=0
		)TD

		IF (SELECT COUNT(*) FROM #TempItemCustomDisplayFields) > 0
		BEGIN
			SELECT @vcCustomDisplayField = STUFF(
									(SELECT ', ' +  'ISNULL(dbo.GetCustFldValueItem(' + CAST(numFieldId AS VARCHAR) + ',numItemCode),'''') AS [' + vcFieldName + ']' FROM #TempItemCustomDisplayFields FOR XML PATH(''))
									, 1
									, 0
									, ''
								)
		END

		SELECT * INTO #TempItemSearchFields FROM
		(
			SELECT 
				numFieldId,
				vcDbColumnName,
				ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName,
				vcLookBackTableName,
				tintRow AS tintOrder,
				0 as Custom
			FROM 
				View_DynamicColumns
			WHERE 
				numFormId=22 AND numUserCntID=@numUserCntID AND numDomainID=@numDomainID AND 
				tintPageType=1 AND bitCustom=0  AND ISNULL(bitSettingField,0)=1 AND numRelCntType=1
				AND vcDbColumnName <> 'vcAttributes'
			UNION   
			SELECT 
				numFieldId,
				vcFieldName,
				vcFieldName,
				'CFW_FLD_Values',
				tintRow AS tintOrder,
				1 as Custom
			FROM 
				View_DynamicCustomColumns   
			WHERE 
				Grp_id=5 AND numFormId=22 AND numUserCntID=@numUserCntID AND numDomainID=@numDomainID AND 
				tintPageType=1 AND bitCustom=1 AND numRelCntType=1
		)Y

		IF (SELECT COUNT(*) FROM #TempItemSearchFields) > 0
		BEGIN
		if(@isStartWithSearch=0)
				begin
			SELECT  @strSearch = STUFF(
									(SELECT ' OR ' +  IIF(Custom = 1,'dbo.GetCustFldValueItem('+ CAST(numFieldId AS VARCHAR) +',numItemCode)',vcDbColumnName) + ' LIKE ''%' + @strItemSearch + '%''' FROM #TempItemSearchFields FOR XML PATH(''))
									, 1
									, 3
									, ''
								)
			end
			else
			begin
			SELECT  @strSearch = STUFF(
									(SELECT ' OR ' +  IIF(Custom = 1,'dbo.GetCustFldValueItem('+ CAST(numFieldId AS VARCHAR) +',numItemCode)',vcDbColumnName) + ' LIKE ''' + @strItemSearch + '%''' FROM #TempItemSearchFields FOR XML PATH(''))
									, 1
									, 3
									, ''
								)
			end
		END
		ELSE
		BEGIN
			if(@isStartWithSearch=0)
			begin
				SELECT @strSearch = 'vcItemName LIKE ''%' + @strItemSearch + '%'''
			end
			else
			begin
				SELECT @strSearch = 'vcItemName LIKE ' + @strItemSearch + '%'''
			end
		END

		SET @strSQL = 'SELECT
							numItemCode, vcItemName, vcPathForTImage, vcCompanyName, MIN(monListPrice) monListPrice, txtItemDesc, 
							vcModelID, numBarCodeId, MIN(vcBarCode) vcBarCode, vcSKU, MIN(vcWHSKU) vcWHSKU, vcPartNo,
							bitSerialized, numItemGroup, Isarchieve, charItemType, 0 AS numWarehouseItemID, '''' vcAttributes
						FROM
						(
						SELECT
							Item.numItemCode,
							ISNULL(vcItemName,'''') AS vcItemName,
							ISNULL((SELECT TOP 1 vcPathForTImage from ItemImages WHERE numItemCode = Item.numItemCode),'''') as vcPathForTImage,
							ISNULL(vcCompanyName,'''') AS vcCompanyName,
							ISNULL(monListPrice,''0.00'') AS monListPrice,
							ISNULL(txtItemDesc,'''') AS txtItemDesc,
							ISNULL(vcModelID,'''') AS vcModelID,
							ISNULL(numBarCodeId,'''') AS numBarCodeId,
							ISNULL(vcBarCode,'''') AS vcBarCode,
							ISNULL(vcWHSKU,'''') AS vcWHSKU,
							ISNULL(vcSKU,'''') AS vcSKU,
							ISNULL(vcPartNo,'''') AS vcPartNo,
							ISNULL(bitSerialized,0) AS bitSerialized,
							ISNULL(numItemGroup,0) AS numItemGroup,
							ISNULL(Isarchieve,0) AS Isarchieve,
							ISNULL(charItemType,'''') AS charItemType
						FROM
							Item
						LEFT JOIN
							DivisionMaster 
						ON
							Item.numVendorID = DivisionMaster.numDivisionID
						LEFT JOIN
							CompanyInfo
						ON
							DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
						LEFT JOIN
							Vendor
						ON
							Item.numVendorID = Vendor.numVendorID AND
							Vendor.numItemCode = Item.numItemCode
						LEFT JOIN
							WareHouseItems
						ON
							Item.numItemCode = WareHouseItems.numItemID
						WHERE 
							Item.numDomainID = ' + CAST(@numDomainID AS VARCHAR) + ')V WHERE ' + @strSearch  + '
						GROUP BY
							numItemCode, vcItemName, vcPathForTImage, vcCompanyName, txtItemDesc, 
							vcModelID, numBarCodeId, vcSKU, bitSerialized, numItemGroup, Isarchieve, charItemType, vcPartNo'
	
		IF LEN(@strAttributeSearch) > 0
		BEGIN
			SET @strSQL = 'SELECT
								numItemCode, vcItemName, vcPathForTImage, vcCompanyName, TEMPAttributes.monWListPrice AS monListPrice, txtItemDesc, 
								vcModelID, numBarCodeId, TEMPAttributes.vcBarCode, vcSKU, TEMPAttributes.vcWHSKU, vcPartNo, TEMPAttributes.numWareHouseItemID,
								TEMPAttributes.vcAttributes
							FROM
								(' + @strSQL + ') TEMP
							OUTER APPLY
							(
								SELECT 
									WHI.numWareHouseItemID,
									WHI.vcBarCode,
									WHI.vcWHSKU,
									WHI.monWListPrice,
									STUFF(
												(SELECT 
												'', '' +  vcAttribute
												FROM  
												(
												SELECT
													CONCAT(	CFM.Fld_label, '' : '',
													(CASE 
														WHEN CFM.Fld_type = ''SelectBox''
														THEN 
															(select vcData from ListDetails where numListID= CFM.numlistid AND numListItemID=CFVSI.Fld_Value)
														ELSE
															CFVSI.Fld_Value
													END)) vcAttribute
												FROM
													CFW_Fld_Values_Serialized_Items CFVSI
												INNER JOIN
													CFW_Fld_Master CFM
												ON
													CFM.Fld_id = CFVSI.Fld_ID
												WHERE
													RecId = WHI.numWareHouseItemID
													AND bitSerialized = TEMP.bitSerialized
												) TEMP
												FOR XML PATH(''''))
											, 1
											, 1
											, ''''
											) AS vcAttributes
								FROM
									dbo.WareHouseItems WHI
								WHERE 
									WHI.numItemID = TEMP.numItemCode
									AND ISNULL(TEMP.numItemGroup,0) > 0
									AND ISNULL(TEMP.Isarchieve, 0) <> 1
									AND TEMP.charItemType NOT IN (''A'')
							) AS TEMPAttributes
							WHERE
								' + @strAttributeSearch
		END

		EXEC ('INSERT INTO #TempSearchedItem SELECT numItemCode, vcItemName, vcPathForTImage, vcCompanyName, monListPrice, txtItemDesc, vcModelID, numBarCodeId, vcBarCode, vcSKU, vcWHSKU, vcPartNo, numWarehouseItemID, vcAttributes FROM (' + @strSQL + ')TEMPFinal')

		IF @searchCriteria = '1' --Items
		BEGIN
			SET @strSQL = 'SELECT * ' + @vcCustomDisplayField + ' FROM #TempSearchedItem ORDER BY numItemCode OFFSET ' + CAST(@numSkip AS VARCHAR(100)) + ' ROWS FETCH NEXT 10 ROWS ONLY'
			EXEC (@strSQL)
			SELECT COUNT(*) AS TotalRows FROM #TempSearchedItem
		END
		ELSE IF @searchCriteria = '3' -- Opp/Orders
		BEGIN
			DECLARE @TEMPItemOrder TABLE
			(
				numOppId NUMERIC(18,0),
				CreatedDate DATETIME,
				vcPOppName VARCHAR(200),
				vcOppType VARCHAR(50),
				vcCompanyName  VARCHAR(200),
				tintType TINYINT
			)

			INSERT INTO 
				@TEMPItemOrder
			SELECT
				numOppId,
				bintCreatedDate,
				vcPOppName,
				vcOppType,
				vcCompanyName,
				tintType
			FROM
				(
					SELECT
						OpportunityMaster.numOppId,
						OpportunityMaster.vcPOppName,
						CASE 
							WHEN tintOppType = 1 THEN 'Sales ' + (CASE WHEN tintOppStatus = 1 THEN 'Order' ELSE 'Opportunity' END)
							WHEN tintOppType = 2 THEN 'Purchase ' + (CASE WHEN tintOppStatus = 1 THEN 'Order' ELSE 'Opportunity' END)
						END vcOppType,
						CompanyInfo.vcCompanyName,
						OpportunityMaster.bintCreatedDate,
						1 AS tintType
					FROM 
						OpportunityMaster
					INNER JOIN
						(
							SELECT
								numOppID
							FROM 
								OpportunityItems OI
							INNER JOIN
								#TempSearchedItem
							ON
								OI.numItemCode = #TempSearchedItem.numItemCode
								AND (OI.numWarehouseItmsID = #TempSearchedItem.numWarehouseItemID OR ISNULL(#TempSearchedItem.numWarehouseItemID,0) = 0)
							GROUP BY
								numOppID
						) TempOppItems
					ON
						OpportunityMaster.numOppId = TempOppItems.numOppId
					INNER JOIN
						DivisionMaster 
					ON
						OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
					INNER JOIN
						CompanyInfo
					ON
						DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
					WHERE
						OpportunityMaster.numDomainId =@numDomainID
						AND tintOppType <> 0
						AND 1 = (
								CASE @orderType
								 WHEN 0 THEN 1
								 WHEN 1 THEN (CASE WHEN (tintOppType = 1 AND tintOppStatus=1) THEN 1 ELSE 0 END)
								 WHEN 2 THEN (CASE WHEN (tintOppType = 2 AND tintOppStatus=1) THEN 1 ELSE 0 END)
								 WHEN 3 THEN (CASE WHEN (tintOppType = 1 AND ISNULL(tintOppStatus,0)=0) THEN 1 ELSE 0 END)
								 WHEN 4 THEN (CASE WHEN (tintOppType = 2 AND ISNULL(tintOppStatus,0)=0) THEN 1 ELSE 0 END)
								 WHEN 5 THEN 0
								 WHEN 6 THEN 0
								END
							)
					UNION
					SELECT
						ReturnHeader.numReturnHeaderID AS numOppID,
						ReturnHeader.vcRMA AS vcPOppName,
						CASE 
							WHEN tintReturnType = 1 THEN 'Sales Return'
							WHEN tintReturnType = 2 THEN 'Purchase Return'
						END vcOppType,
						CompanyInfo.vcCompanyName,
						ReturnHeader.dtCreatedDate AS bintCreatedDate,
						2 AS tintType
					FROM
						ReturnHeader
					INNER JOIN
						(
							SELECT
								numReturnHeaderID
							FROM 
								ReturnItems RI
							INNER JOIN
								#TempSearchedItem
							ON
								RI.numItemCode = #TempSearchedItem.numItemCode
								AND (RI.numWareHouseItemID = #TempSearchedItem.numWarehouseItemID OR ISNULL(#TempSearchedItem.numWarehouseItemID,0) = 0)
							GROUP BY
								numReturnHeaderID
						) TempReturnItems
					ON
						ReturnHeader.numReturnHeaderID = TempReturnItems.numReturnHeaderID
					INNER JOIN
						DivisionMaster 
					ON
						ReturnHeader.numDivisionId = DivisionMaster.numDivisionID
					INNER JOIN
						CompanyInfo
					ON
						DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
					WHERE
						ReturnHeader.numDomainId = @numDomainID 
						AND 1 = (
								CASE @orderType
								 WHEN 0 THEN (CASE WHEN tintReturnType = 1 OR tintReturnType = 2 THEN 1 ELSE 0 END)
								 WHEN 1 THEN 0
								 WHEN 2 THEN 0
								 WHEN 3 THEN 0
								 WHEN 4 THEN 0
								 WHEN 5 THEN (CASE WHEN tintReturnType = 1 THEN 1 ELSE 0 END)
								 WHEN 6 THEN (CASE WHEN tintReturnType = 2 THEN 1 ELSE 0 END)
								END
							)
				) TEMPFinal

			SELECT 
				ISNULL(numOppId,0) numOppId,
				ISNULL(dbo.FormatedDateFromDate(CreatedDate,@numDomainID),'') bintCreatedDate,
				ISNULL(vcPOppName,'') vcPOppName,
				ISNULL(vcOppType,'') vcOppType,
				ISNULL(vcCompanyName,'') vcCompanyName,
				ISNULL(tintType,0) tintType
			FROM
				@TEMPItemOrder
			ORDER BY
				CreatedDate DESC
			OFFSET @numSkip ROWS FETCH NEXT 10 ROWS ONLY	
				
			SELECT COUNT(*) AS TotalRows FROM @TEMPItemOrder	
		END
		ELSE IF @searchCriteria = '4' -- BizDocs
		BEGIN
			DECLARE @TEMPItemBizDoc TABLE
			(
				numOppBizDocsId NUMERIC(18,0),
				CreatedDate DATETIME,
				vcBizDocID VARCHAR(200),
				vcBizDocType VARCHAR(100),
				vcCompanyName  VARCHAR(200)
			)

			INSERT INTO
				@TEMPItemBizDoc
			SELECT 
				numOppBizDocsId,
				OpportunityBizDocs.dtCreatedDate,
				vcBizDocID,
				ListDetails.vcData AS vcBizDocType,
				CompanyInfo.vcCompanyName
			FROM 
				OpportunityBizDocs
			INNER JOIN
				(
					SELECT
						numOppBizDocID
					FROM 
						OpportunityBizDocItems OBI
					INNER JOIN
						#TempSearchedItem
					ON
						OBI.numItemCode = #TempSearchedItem.numItemCode
						AND (OBI.numWarehouseItmsID = #TempSearchedItem.numWarehouseItemID OR ISNULL(#TempSearchedItem.numWarehouseItemID,0) = 0)
					GROUP BY
						numOppBizDocID
				) TempOppBizDocItems
			ON
				OpportunityBizDocs.numOppBizDocsId = TempOppBizDocItems.numOppBizDocID
			INNER JOIN
				OpportunityMaster
			ON
				OpportunityBizDocs.numOppId = OpportunityMaster.numOppId
			INNER JOIN
				DivisionMaster
			ON
				OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
			INNER JOIN
				CompanyInfo
			ON
				DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
			INNER JOIN
				ListDetails
			ON
				ListDetails.numListID = 27
				AND OpportunityBizDocs.numBizDocId = ListDetails.numListItemID
			WHERE
				OpportunityMaster.numDomainId = @numDomainID
				AND (OpportunityBizDocs.numBizDocID = @bizDocType OR ISNULL(@bizDocType,0) = 0)

			SELECT
				ISNULL(numOppBizDocsId,0) numOppBizDocsId,
				ISNULL(dbo.FormatedDateFromDate(CreatedDate,@numDomainID),'') AS dtCreatedDate,
				ISNULL(vcBizDocID,'') vcBizDocID,
				ISNULL(vcBizDocType,'') vcBizDocType,
				ISNULL(vcCompanyName,'') vcCompanyName
			FROM
				@TEMPItemBizDoc
			ORDER BY
				CreatedDate DESC
			OFFSET @numSkip ROWS FETCH NEXT 10 ROWS ONLY
				
			SELECT COUNT(*) AS TotalRows FROM @TEMPItemBizDoc
		END

		
		DROP TABLE #TempItemSearchFields
		DROP TABLE #TempItemCustomDisplayFields
		DROP TABLE #TempSearchedItem
	END
	ELSE IF @searchType = '3' -- Opps/Orders
	BEGIN
		DECLARE @TEMPORDER TABLE
		(
			numOppId NUMERIC(18,0),
			CreatedDate DATETIME,
			vcPOppName VARCHAR(200),
			vcOppType VARCHAR(50),
			vcCompanyName  VARCHAR(200),
			tintType TINYINT
		)

		INSERT INTO 
			@TEMPORDER
		SELECT
			numOppId,
			bintCreatedDate,
			vcPOppName,
			vcOppType,
			vcCompanyName,
			tintType
		FROM
			(
				SELECT
					OpportunityMaster.numOppId,
					OpportunityMaster.vcPOppName,
					CASE 
						WHEN tintOppType = 1 THEN 'Sales ' + (CASE WHEN tintOppStatus = 1 THEN 'Order' ELSE 'Opportunity' END)
						WHEN tintOppType = 2 THEN 'Purchase ' + (CASE WHEN tintOppStatus = 1 THEN 'Order' ELSE 'Opportunity' END)
					END vcOppType,
					CompanyInfo.vcCompanyName,
					OpportunityMaster.bintCreatedDate,
					1 AS tintType
				FROM 
					OpportunityMaster
				INNER JOIN
					DivisionMaster 
				ON
					OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
				INNER JOIN
					CompanyInfo
				ON
					DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
				WHERE
					OpportunityMaster.numDomainId =@numDomainID
					AND tintOppType <> 0
					AND vcPOppName LIKE CASE WHEN @isStartWithSearch=0 THEN N'%' + @searchText + '%' ELSE N'' + @searchText + '%' END OR numOppId LIKE CASE WHEN @isStartWithSearch=0 THEN N'%' + @searchText + '%' ELSE N'' + @searchText + '%' END
					AND 1 = (
							CASE @orderType
							 WHEN 0 THEN 1
							 WHEN 1 THEN (CASE WHEN (tintOppType = 1 AND tintOppStatus=1) THEN 1 ELSE 0 END)
							 WHEN 2 THEN (CASE WHEN (tintOppType = 2 AND tintOppStatus=1) THEN 1 ELSE 0 END)
							 WHEN 3 THEN (CASE WHEN (tintOppType = 1 AND ISNULL(tintOppStatus,0)=0) THEN 1 ELSE 0 END)
							 WHEN 4 THEN (CASE WHEN (tintOppType = 2 AND ISNULL(tintOppStatus,0)=0) THEN 1 ELSE 0 END)
							 WHEN 5 THEN 0
							 WHEN 6 THEN 0
							END
						)
				UNION
				SELECT
					ReturnHeader.numReturnHeaderID AS numOppID,
					ReturnHeader.vcRMA AS vcPOppName,
					CASE 
						WHEN tintReturnType = 1 THEN 'Sales Return'
						WHEN tintReturnType = 2 THEN 'Purchase Return'
					END vcOppType,
					CompanyInfo.vcCompanyName,
					ReturnHeader.dtCreatedDate AS bintCreatedDate,
					2 AS tintType
				FROM
					ReturnHeader
				INNER JOIN
					DivisionMaster 
				ON
					ReturnHeader.numDivisionId = DivisionMaster.numDivisionID
				INNER JOIN
					CompanyInfo
				ON
					DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
				WHERE
					ReturnHeader.numDomainId = @numDomainID 
					AND (vcRMA LIKE CASE WHEN @isStartWithSearch=0 THEN N'%' + @searchText + '%' ELSE N'' + @searchText + '%' END OR ReturnHeader.numReturnHeaderID LIKE  CASE WHEN @isStartWithSearch=0 THEN N'%' + @searchText + '%' ELSE N'' + @searchText + '%' END)
					AND 1 = (
							CASE @orderType
							 WHEN 0 THEN (CASE WHEN tintReturnType = 1 OR tintReturnType = 2 THEN 1 ELSE 0 END)
							 WHEN 1 THEN 0
							 WHEN 2 THEN 0
							 WHEN 3 THEN 0
							 WHEN 4 THEN 0
							 WHEN 5 THEN (CASE WHEN tintReturnType = 1 THEN 1 ELSE 0 END)
							 WHEN 6 THEN (CASE WHEN tintReturnType = 2 THEN 1 ELSE 0 END)
							END
						)
				) TEMPFinal

		SELECT 
			ISNULL(numOppId,0) numOppId,
			ISNULL(dbo.FormatedDateFromDate(CreatedDate,@numDomainID),'') bintCreatedDate,
			ISNULL(vcPOppName,'') vcPOppName,
			ISNULL(vcOppType,'') vcOppType,
			ISNULL(vcCompanyName,'') vcCompanyName,
			ISNULL(tintType,0) tintType
		FROM
			@TEMPORDER
		ORDER BY
			CreatedDate DESC
		OFFSET @numSkip ROWS FETCH NEXT 10 ROWS ONLY	
				
		SELECT COUNT(*) AS TotalRows FROM @TEMPORDER		
	END
	ELSE IF @searchType = '4' -- BizDocs
	BEGIN
		DECLARE @TEMPBizDoc TABLE
		(
			numOppBizDocsId NUMERIC(18,0),
			CreatedDate DATETIME,
			vcBizDocID VARCHAR(200),
			vcBizDocType VARCHAR(100),
			vcCompanyName  VARCHAR(200)
		)

		INSERT INTO
			@TEMPBizDoc
		SELECT 
			numOppBizDocsId,
			OpportunityBizDocs.dtCreatedDate,
			vcBizDocID,
			ListDetails.vcData AS vcBizDocType,
			CompanyInfo.vcCompanyName
		FROM 
			OpportunityBizDocs
		INNER JOIN
			OpportunityMaster
		ON
			OpportunityBizDocs.numOppId = OpportunityMaster.numOppId
		INNER JOIN
			DivisionMaster
		ON
			OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
		INNER JOIN
			CompanyInfo
		ON
			DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
		INNER JOIN
			ListDetails
		ON
			ListDetails.numListID = 27
			AND OpportunityBizDocs.numBizDocId = ListDetails.numListItemID
		WHERE
			OpportunityMaster.numDomainId = @numDomainID
			AND vcBizDocID LIKE CASE WHEN @isStartWithSearch=0 THEN N'%' + @searchText + '%' ELSE N'' + @searchText + '%' END
			AND (OpportunityBizDocs.numBizDocID = @bizDocType OR ISNULL(@bizDocType,0) = 0)

		SELECT
			ISNULL(numOppBizDocsId,0) numOppBizDocsId,
			ISNULL(dbo.FormatedDateFromDate(CreatedDate,@numDomainID),'') AS dtCreatedDate,
			ISNULL(vcBizDocID,'') vcBizDocID,
			ISNULL(vcBizDocType,'') vcBizDocType,
			ISNULL(vcCompanyName,'') vcCompanyName
		FROM
			@TEMPBizDoc
		ORDER BY
			CreatedDate DESC
		OFFSET @numSkip ROWS FETCH NEXT 10 ROWS ONLY
				
		SELECT COUNT(*) AS TotalRows FROM @TEMPBizDoc
	END
	ELSE IF @searchType = '5' -- Contact
	BEGIN
		DECLARE @TableContact TABLE
		(
			numContactId NUMERIC(18,0),
			vcFirstName VARCHAR(100),
			vcLastname VARCHAR(100),
			vcFullName VARCHAR(200),
			vcEmail VARCHAR(100),
			numPhone VARCHAR(100),
			numPhoneExtension VARCHAR(100),
			vcCompanyName VARCHAR(200),
			bitPrimaryContact VARCHAR(5)
		)

		INSERT INTO
			@TableContact
		SELECT 
			ADC.numContactId,
			ISNULL(vcFirstName,'') AS vcFirstName,
			ISNULL(vcLastName,'') AS vcLastName,
			ISNULL(vcFirstName,'') + ' ' + ISNULL(vcLastName,'') AS vcFullName,
			ISNULL(vcEmail,'') AS vcEmail,
			ISNULL(ADC.numPhone,'') AS numPhone,
			ISNULL(ADC.numPhoneExtension,'') AS numPhoneExtension,
			ISNULL(CI.vcCompanyName,'') AS vcCompanyName,
			(CASE WHEN ISNULL(bitPrimaryContact,0) = 1 THEN 'Yes' ELSE 'No' END) AS bitPrimaryContact
		FROM 
			AdditionalContactsInformation ADC
		INNER JOIN
			DivisionMaster DM
		ON
			ADC.numDivisionId = DM.numDivisionID
		INNER JOIN
			CompanyInfo CI
		ON
			DM.numCompanyID = CI.numCompanyId
		WHERE 
			ADC.numDomainID = @numDomainID AND
			(vcFirstName LIKE CASE WHEN @isStartWithSearch=0 THEN '%' + @searchText + '%' ELSE '' + @searchText + '%' END
			OR vcLastName LIKE CASE WHEN @isStartWithSearch=0 THEN '%' + @searchText + '%' ELSE '' + @searchText + '%' END
			OR vcEmail LIKE CASE WHEN @isStartWithSearch=0 THEN '%' + @searchText + '%' ELSE '' + @searchText + '%' END
			OR numPhone LIKE CASE WHEN @isStartWithSearch=0 THEN '%' + @searchText + '%' ELSE '' + @searchText + '%' END)

		SELECT 
			* 
		FROM 
			@TableContact	
		ORDER BY 
			vcCompanyName ASC, bitPrimaryContact DESC
		OFFSET 
			@numSkip 
		ROWS FETCH NEXT 
			10 
		ROWS ONLY

		SELECT COUNT(*) FROM @TableContact
	END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_TaxReports')
DROP PROCEDURE USP_TaxReports
GO
CREATE PROCEDURE [dbo].[USP_TaxReports]     
    @numDomainID numeric(18, 0),
    @dtFromDate DATETIME,
    @dtToDate DATETIME
AS                 
	SELECT 
		vcBizDocID,
		TI.vcTaxName,
		T.fltPercentage,
		T.tintTaxType,
		SUM(Case When T.tintOppType=1 then TaxAmount else 0 end) SalesTax,
		SUM(Case When T.tintOppType=2 then TaxAmount else 0 end) PurchaseTax 
	FROM 
	(
		SELECT 
		    OBD.vcBizDocID,
			OMTI.*,
			OM.tintOppType,
			ISNULL(dbo.fn_CalOppItemTotalTaxAmt(OM.numDomainId,OMTI.numTaxItemID,OM.numOppId,OBD.numOppBizDocsId),0) AS TaxAmount
		FROM 
			OpportunityMaster OM 
		JOIN 
			OpportunityBizDocs OBD 
		ON 
			OM.numOppId=OBD.numOppId
		JOIN 
			OpportunityMasterTaxItems OMTI 
		ON 
			OMTI.numOppId=OBD.numOppId
		WHERE 
			OM.numDomainID=@numDomainID 
			AND OBD.bitAuthoritativeBizDocs=1 
			AND OMTI.fltPercentage>0
			AND OBD.dtFromDate BETWEEN  @dtFromDate AND @dtToDate
	) T 
	JOIN 
	(
		SELECT 
			numTaxItemId,
			vcTaxName 
		FROM 
			TaxItems 
		WHERE 
			numDomainID=@numDomainID 
		UNION 
		SELECT 
			0,
			'Sales Tax'
		UNION 
		SELECT 
			1,
			'CRV'
	) TI 
	ON T.numTaxItemId=TI.numTaxItemID 
	WHERE 
		TaxAmount > 0
	GROUP BY 
		T.vcBizDocID,
		TI.vcTaxName,
		T.fltPercentage,
		T.tintTaxType
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_UpdateOppItemLabel')
DROP PROCEDURE USP_UpdateOppItemLabel
GO
CREATE PROCEDURE  USP_UpdateOppItemLabel
    @numOppID NUMERIC,
	@numoppitemtCode numeric(9),
    @vcItemName varchar(300),
    @vcModelID varchar(200),
    @vcItemDesc varchar(1000),
	@vcManufacturer varchar(250),
	@numProjectID numeric(9),
	@numClassID numeric(9),
	@vcNotes varchar(1000)
AS 
    BEGIN

 UPDATE  OpportunityItems
                SET     [vcItemName] = @vcItemName,
                        [vcModelID] = @vcModelID,
                        [vcItemDesc] = @vcItemDesc,
                        vcManufacturer = @vcManufacturer,
                        numProjectID = @numProjectID,
                        numClassID = @numClassID,
						vcNotes=@vcNotes,
						numProjectStageID=(Case when numProjectID<>@numProjectID then 0 else numProjectStageID end)
                WHERE   [numOppId] = @numOppID and numoppitemtCode = @numoppitemtCode

--        DECLARE @hDocItem INT
--        IF CONVERT(VARCHAR(10), @strItems) <> '' 
--            BEGIN
--                EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems
--                
--                SELECT    numoppitemtCode,
--                                    vcItemName,
--                                    vcModelID,
--                                    vcItemDesc,
--                                    vcManufacturer,
--                                    numProjectID,
--                                    numClassID into #tempOpp
--                          FROM      OPENXML (@hDocItem, '/NewDataSet/Item', 2)
--                                    WITH ( numoppitemtCode NUMERIC(9), vcItemName VARCHAR(300), vcModelID VARCHAR(200), vcItemDesc VARCHAR(1000),vcManufacturer VARCHAR(250),numProjectID NUMERIC,numClassID NUMERIC)
-- 
--
----Delete Associated Time and Expense Entry if Project is Changed    
--DECLARE @numCategoryHDRID NUMERIC(9), @numDomainID NUMERIC(9)
--
--DECLARE TimeAndExpense_cursor CURSOR FOR 
--SELECT numCategoryHDRID,numDomainID
--FROM #tempOpp X join OpportunityItems OPP on OPP.numoppitemtCode = X.numoppitemtCode
--Join TimeAndExpense TE on OPP.numOppId = TE.numOppId AND OPP.numoppitemtCode = TE.numOppItemID AND OPP.numProjectId=TE.numProId
--where OPP.numOppID=@numOppID and OPP.numProjectID <> X.numProjectID
--
--OPEN TimeAndExpense_cursor;
--
--FETCH NEXT FROM TimeAndExpense_cursor 
--INTO @numCategoryHDRID, @numDomainID;
--
--WHILE @@FETCH_STATUS = 0
--BEGIN
--	exec USP_DeleteTimExpLeave @numCategoryHDRID, @numDomainID
--
--    FETCH NEXT FROM TimeAndExpense_cursor 
--    INTO @numCategoryHDRID, @numDomainID;
--END
--CLOSE TimeAndExpense_cursor;
--DEALLOCATE TimeAndExpense_cursor;
--										
--
--
----Update Oppertunity Item
--                UPDATE  OPP
--                SET     [vcItemName] = X.[vcItemName],
--                        [vcModelID] = X.[vcModelID],
--                        [vcItemDesc] = X.vcItemDesc,
--                        vcManufacturer = X.vcManufacturer,
--                        numProjectID = X.numProjectID,
--                        numClassID = X.numClassID
--                FROM    #tempOpp as X join [OpportunityItems] OPP on OPP.numoppitemtCode = X.numoppitemtCode
--                WHERE   [numOppId] = @numOppID
--                EXEC sp_xml_removedocument @hDocItem
--            END
    END

	--created by anoop jayaraj
--	EXEC USP_GEtSFItemsForImporting 72,'76,318,538,758,772,1061,11409,11410,11628,11629,17089,18151,18193,18195,18196,18197',1
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getsfitemsforimporting')
DROP PROCEDURE usp_getsfitemsforimporting
GO
CREATE PROCEDURE USP_GEtSFItemsForImporting
    @numDomainID AS NUMERIC(9),
	@numBizDocId	AS BIGINT,
	@vcBizDocsIds		VARCHAR(MAX),
	@tintMode AS TINYINT 
AS
BEGIN
---------------------------------------------------------------------------------
    DECLARE  @strSql  AS VARCHAR(8000)
  DECLARE  @strFrom  AS VARCHAR(2000)

IF @tintMode=1 --PrintPickList
BEGIN
   SELECT
		ISNULL(vcItemName,'') AS vcItemName,
		ISNULL((SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numDomainId=187 AND bitDefault=1 AND bitIsImage=1),'') AS vcImage,
		ISNULL(vcModelID,'') AS vcModelID,
		ISNULL(txtItemDesc,'') AS txtItemDesc,
		ISNULL(vcWareHouse,'') AS vcWareHouse,
		ISNULL(vcLocation,'') AS vcLocation,
		(CASE WHEN ISNULL(Item.numItemGroup,0) > 0 THEN WareHouseItems.vcWHSKU ELSE Item.vcSKU END) AS vcSKU,
		(CASE WHEN ISNULL(Item.numItemGroup,0) > 0 THEN WareHouseItems.vcBarCode ELSE Item.numBarCodeId END) AS vcUPC,
		SUM(numUnitHour) AS numUnitHour
	FROM
		OpportunityBizDocItems
	INNER JOIN
		WareHouseItems
	ON
		OpportunityBizDocItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
	INNER JOIN
		Warehouses
	ON
		WareHouseItems.numWareHouseID = Warehouses.numWareHouseID
	INNER JOIN
		Item
	ON
		OpportunityBizDocItems.numItemCode = Item.numItemCode
	WHERE
		numOppBizDocID IN ( SELECT * FROM dbo.splitIds(@vcBizDocsIds,','))
	GROUP BY
		vcItemName,
		vcModelID,
		txtItemDesc,
		vcWareHouse,
		vcLocation,
		(CASE WHEN ISNULL(Item.numItemGroup,0) > 0 THEN WareHouseItems.vcWHSKU ELSE Item.vcSKU END),
		(CASE WHEN ISNULL(Item.numItemGroup,0) > 0 THEN WareHouseItems.vcBarCode ELSE Item.numBarCodeId END)
	ORDER BY
		vcWareHouse ASC,
		vcLocation ASC,
		vcItemName ASC
END
ELSE IF @tintMode=2 --PrintPackingSlip
BEGIN
		  
		SELECT	
				ROW_NUMBER() OVER(ORDER BY OM.numDomainID ASC) AS SRNO,
				OM.numOppId,
				OM.numDomainID,
				OM.tintTaxOperator,
				I.numItemCode,
				I.vcModelID,
				OM.vcPOppName,
				cast((OBDI.monTotAmtBefDiscount - OBDI.monTotAMount) as varchar(20)) + Case When isnull(OM.fltDiscount,0)> 0 then  '(' + cast(OM.fltDiscount as varchar(10)) + '%)' else '' end AS DiscAmt,
				OBDI.dtRentalStartDate,
				OBDI.dtRentalReturnDate,
				OBDI.monShipCost,
				OBDI.vcShippingMethod,
				OBDI.dtDeliveryDate,
				numItemClassification,
				OBDI.vcNotes,
				OBD.vcTrackingNo,
				I.vcManufacturer,
				--vcItemClassification,
				CASE 
					WHEN charItemType='P' 
					THEN 'Inventory Item' 
					WHEN charItemType='S' 
					THEN 'Service' 
					WHEN charItemType='A' 
					THEN 'Accessory' 
					WHEN charItemType='N' 
					THEN 'Non-Inventory Item' 
				END AS charItemType,charItemType AS [Type],
				OBDI.vcAttributes,
				OI.[numoppitemtCode] ,
				OI.vcItemName,
				OBDI.numUnitHour,
				OI.[numQtyShipped],
				OBDI.vcItemDesc AS txtItemDesc,
				vcUnitofMeasure AS numUOMId,
				monListPrice AS [Price],
				CONVERT(VARCHAR, CONVERT(DECIMAL(18, 4), OBDI.monTotAmount)) Amount,
				dbo.fn_GetListItemName(OBD.numShipVia) AS ShipVai,
				OBDI.monPrice,
				OBDI.monTotAmount,
				vcWareHouse,
				vcLocation,
				vcWStreet,
				vcWCity,
				vcWPinCode,
				(SELECT vcState FROM State WHERE numStateID = W.numWState) AS [vcState],
				dbo.fn_GetListItemName(W.numWCountry) AS [vcWCountry],							
				vcWHSKU,
				vcBarCode,
				SUBSTRING((SELECT ',' + vcSerialNo + CASE 
															WHEN isnull(I.bitLotNo,0)=1 
															THEN ' (' + CONVERT(VARCHAR(15),SERIALIZED_ITEM.numQty) + ')' 
															ELSE '' 
													 END 
							FROM OppWarehouseSerializedItem SERIALIZED_ITEM 
							JOIN WareHouseItmsDTL W_ITEM_DETAIL ON SERIALIZED_ITEM.numWareHouseItmsDTLID = W_ITEM_DETAIL.numWareHouseItmsDTLID 
							WHERE SERIALIZED_ITEM.numOppID = OI.numOppId 
							  AND SERIALIZED_ITEM.numOppItemID = OI.numoppitemtCode 
							  and SERIALIZED_ITEM.numOppBizDocsID=OBD.numOppBizDocsId
							ORDER BY vcSerialNo 
						  FOR XML PATH('')),2,200000) AS SerialLotNo,
				isnull(OBD.vcRefOrderNo,'') AS vcRefOrderNo,
				isnull(OM.bitBillingTerms,0) AS tintBillingTerms,
				isnull(intBillingDays,0) AS numBillingDays,
--				CASE 
--					WHEN ISNUMERIC(ISNULL(dbo.fn_GetListItemName(ISNULL(intBillingDays,0)),0)) = 1 
--					THEN ISNULL(dbo.fn_GetListItemName(ISNULL(intBillingDays,0)),0) 
--					ELSE 0 
--				END AS numBillingDaysName,
				CASE WHEN ISNUMERIC((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0))) = 1
					 THEN (SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0))
					 ELSE 0
				END AS numBillingDaysName,	 
				ISNULL(bitInterestType,0) AS tintInterestType,
				tintOPPType,
				dbo.fn_GetContactName(numApprovedBy) AS ApprovedBy,
				dtApprovedDate,
				OM.bintAccountClosingDate,
				tintShipToType,
				tintBillToType,
				tintshipped,
				dtShippedDate bintShippedDate,
				OM.numDivisionID,							
				ISNULL(numShipVia,0) AS numShipVia,
				ISNULL(vcTrackingURL,'') AS vcTrackingURL,
				CASE 
				    WHEN numShipVia IS NULL 
				    THEN '-'
				    ELSE dbo.fn_GetListItemName(numShipVia)
				END AS ShipVia,
				ISNULL(C.varCurrSymbol,'') varCurrSymbol,
				isnull(bitPartialFulfilment,0) bitPartialFulfilment,
				dbo.[fn_GetContactName](OM.[numRecOwner])  AS OrderRecOwner,
				dbo.[fn_GetContactName](DM.[numRecOwner]) AS AccountRecOwner,
				dbo.fn_GetContactName(OM.numAssignedTo) AS AssigneeName,
				ISNULL((SELECT vcEmail FROM dbo.AdditionalContactsInformation WHERE numContactID = OM.numAssignedTo),'') AS AssigneeEmail,
				ISNULL((SELECT numPhone FROM dbo.AdditionalContactsInformation WHERE numContactID = OM.numAssignedTo),'') AS AssigneePhone,
				dbo.fn_GetComapnyName(OM.numDivisionId) OrganizationName,
				DM.vcComPhone as OrganizationPhone,
				dbo.fn_GetContactName(OM.numContactID)  OrgContactName,
				dbo.getCompanyAddress(OM.numDivisionId,1,OM.numDomainId) CompanyBillingAddress,
				CASE 
					 WHEN ACI.numPhone<>'' 
					 THEN ACI.numPhone + CASE 
										    WHEN ACI.numPhoneExtension<>'' 
											THEN ' - ' + ACI.numPhoneExtension 
											ELSE '' 
										 END  
					 ELSE '' 
				END OrgContactPhone,
				ISNULL(ACI.vcEmail,'') AS OrgContactEmail,
				dbo.[fn_GetContactName](OM.[numRecOwner]) AS OnlyOrderRecOwner,
				(SELECT TOP 1 SD.vcSignatureFile FROM SignatureDetail SD 
												 JOIN OpportunityBizDocsDetails OBD ON SD.numSignID = OBD.numSignID 
												WHERE SD.numDomainID = @numDomainID 
												  AND OBD.numDomainID = @numDomainID 
												  AND OBD.numSignID IS not null 
				ORDER BY OBD.numBizDocsPaymentDetId DESC) AS vcSignatureFile,
				ISNULL(CMP.txtComments,'') AS vcOrganizationComments,
				OBD.vcRefOrderNo AS [P.O.No],
				OM.txtComments AS [Comments],
				D.vcBizDocImagePath  
	INTO #temp_Packing_List
	FROM OpportunityMaster AS OM
	join OpportunityBizDocs OBD on OM.numOppID=OBD.numOppID
	JOIN OpportunityBizDocItems OBDI on OBDI.numOppBizDocID=OBD.numOppBizDocsId
	JOIN OpportunityItems AS OI on OI.numoppitemtCode=OBDI.numOppItemID and OI.numoppID=OI.numOppID
	JOIN Item AS I ON OI.numItemCode = I.numItemCode AND I.numDomainID = @numDomainID
	LEFT OUTER JOIN WareHouseItems WI ON I.numItemCode = WI.numItemID AND WI.numWareHouseItemID = OBDI.numWarehouseItmsID
	LEFT OUTER JOIN Warehouses W ON WI.numWareHouseID = W.numWareHouseID 
	JOIN [DivisionMaster] DM ON DM.numDivisionID = OM.numDivisionID   
	JOIN CompanyInfo CMP ON DM.numCompanyId = CMP.numCompanyId and CMP.numDomainID = @numDomainID  
	JOIN dbo.AdditionalContactsInformation ACI ON ACI.numContactID = OM.numContactID
	JOIN Domain D ON D.numDomainID = OM.numDomainID
	LEFT OUTER JOIN [Currency] C ON C.numCurrencyID = OM.numCurrencyID
    WHERE OM.numDomainID = @numDomainID 
	  AND OBD.[numOppBizDocsId] IN ( SELECT * FROM dbo.splitIds(@vcBizDocsIds ,',')) ORDER BY OM.numOppId
	
	DECLARE @numFldID AS INT    
	DECLARE @intRowNum AS INT    	
	DECLARE @vcFldname AS VARCHAR(MAX)	
	
	SET @strSQL = ''
	SELECT TOP 1 @numFldID = numFormfieldID,
				 @vcFldname = Fld_label,
				 @intRowNum = (intRowNum+1) 
	FROM DynamicFormConfigurationDetails DTL                                                                                                
	JOIN CFW_Fld_Master FIELD_MASTER ON FIELD_MASTER.Fld_id = DTL.numFormFieldID                                                                                                 
	WHERE DTL.numFormID = 7 
	  AND FIELD_MASTER.Grp_id = 5 
	  AND DTL.numDomainID = @numDomainID 
	  AND numAuthGroupID = @numBizDocId
	  AND vcFieldType = 'C' 
	ORDER BY intRowNum

	/**** START: Added Sales Tax Column ****/

	DECLARE @strSQLUpdate AS VARCHAR(2000);
	
	EXEC ( 'ALTER TABLE #temp_Packing_List ADD [TotalTax] MONEY'  )
	EXEC ( 'ALTER TABLE #temp_Packing_List ADD [CRV] MONEY'  )


	DECLARE @i AS INT = 1
	DECLARE @Count AS INT 
	SELECT @Count = COUNT(*) FROM #temp_Packing_List

	DECLARE @numOppId AS BIGINT
	DECLARE @numOppItemCode AS BIGINT
	DECLARE @tintTaxOperator AS INT

	WHILE @i <= @Count
	BEGIN
		SET @strSQLUpdate=''

		SELECT 
			@numDomainID = numDomainID,
			@numOppId = numOppId,
			@tintTaxOperator = tintTaxOperator,
			@numOppItemCode = numoppitemtCode
		FROM
			#temp_Packing_List
		WHERE
			SRNO = @i


		IF @tintTaxOperator = 1 
		BEGIN
			 SET @strSQLUpdate = @strSQLUpdate
				+ ',[TotalTax]= dbo.fn_CalBizDocTaxAmt('
				+ CONVERT(VARCHAR(20), @numDomainID) + ',0,'
				+ CONVERT(VARCHAR(20), @numOppId) + ','
				+ CONVERT(VARCHAR(20), @numOppItemCode) + ',1,Amount,numUnitHour)'

			SET @strSQLUpdate = @strSQLUpdate
				+ ',[CRV]= dbo.fn_CalBizDocTaxAmt('
				+ CONVERT(VARCHAR(20), @numDomainID) + ',1,'
				+ CONVERT(VARCHAR(20), @numOppId) + ','
				+ CONVERT(VARCHAR(20), @numOppItemCode) + ',1,Amount,numUnitHour)'
		END
		ELSE IF @tintTaxOperator = 2 -- remove sales tax
		BEGIN
			SET @strSQLUpdate = @strSQLUpdate + ',[TotalTax]= 0'
			SET @strSQLUpdate = @strSQLUpdate + ',[CRV]= 0'
		END
		ELSE
		BEGIN
			 SET @strSQLUpdate = @strSQLUpdate
				+ ',[TotalTax]= dbo.fn_CalBizDocTaxAmt('
				+ CONVERT(VARCHAR(20), @numDomainID) + ',0,'
				+ CONVERT(VARCHAR(20), @numOppId) + ','
				+ CONVERT(VARCHAR(20), @numOppItemCode) + ',1,Amount,numUnitHour)'

			SET @strSQLUpdate = @strSQLUpdate
				+ ',[TotalTax]= dbo.fn_CalBizDocTaxAmt('
				+ CONVERT(VARCHAR(20), @numDomainID) + ',1,'
				+ CONVERT(VARCHAR(20), @numOppId) + ','
				+ CONVERT(VARCHAR(20), @numOppItemCode) + ',1,Amount,numUnitHour)'
		END

		IF @strSQLUpdate <> '' 
		BEGIN
			SET @strSQLUpdate = 'UPDATE #temp_Packing_List SET numItemCode=numItemCode' + @strSQLUpdate + ' WHERE numItemCode > 0 AND numOppId=' + CONVERT(VARCHAR(20), @numOppId) + ' AND numoppitemtCode=' + CONVERT(VARCHAR(20), @numOppItemCode)
			PRINT @strSQLUpdate
			EXEC (@strSQLUpdate)
		END

		SET @i = @i + 1
	END


	SET @strSQLUpdate = ''
	DECLARE @vcTaxName AS VARCHAR(100)
	DECLARE @numTaxItemID AS NUMERIC(9)
	SET @numTaxItemID = 0
	SELECT TOP 1 @vcTaxName = vcTaxName, @numTaxItemID = numTaxItemID FROM TaxItems WHERE numDomainID = @numDomainID

	WHILE @numTaxItemID > 0
	BEGIN

		EXEC ( 'ALTER TABLE #temp_Packing_List ADD [' + @vcTaxName + '] MONEY' )

		SET @strSQLUpdate = @strSQLUpdate + ',[' + @vcTaxName  + ']= dbo.fn_CalBizDocTaxAmt(numDomainID,'
						+ CONVERT(VARCHAR(20), @numTaxItemID) + ',numOppId,numoppitemtCode,1,Amount,numUnitHour)'
           

		SELECT TOP 1 @vcTaxName = vcTaxName, @numTaxItemID = numTaxItemID FROM TaxItems WHERE numDomainID = @numDomainID AND numTaxItemID > @numTaxItemID

		IF @@rowcount = 0 
			SET @numTaxItemID = 0
	END


	IF @strSQLUpdate <> '' 
	BEGIN
		SET @strSQLUpdate = 'UPDATE #temp_Packing_List SET numItemCode=numItemCode' + @strSQLUpdate + ' WHERE numItemCode > 0' 
		PRINT @strSQLUpdate
		EXEC (@strSQLUpdate)
	END

	/**** END: Added Sales Tax Column ****/
	
	PRINT 'START'                                                                                          
	PRINT 'QUERY : ' + @strSQL
	PRINT @intRowNum
	WHILE @intRowNum > 0                                                                                  
	BEGIN    
			PRINT 'FROM LOOP'
			PRINT @vcFldname
			PRINT @numFldID

			EXEC('ALTER TABLE #temp_Packing_List add [' + @vcFldname + '] varchar(100)')
		
			SET @strSQL = 'UPDATE #temp_Packing_List SET [' + @vcFldname + '] = dbo.GetCustFldValueItem(' + CONVERT(VARCHAR(10),@numFldID) + ',numItemCode) 
						   WHERE numItemCode > 0'

			PRINT 'QUERY : ' + @strSQL
			EXEC (@strSQL)
			                                                                                 	                                                                                       
			SELECT TOP 1 @numFldID = numFormfieldID,
						 @vcFldname = Fld_label,
						 @intRowNum = (intRowNum + 1) 
			FROM DynamicFormConfigurationDetails DETAIL                                                                                                
			JOIN CFW_Fld_Master MASTER ON MASTER.Fld_id = DETAIL.numFormFieldID                                                                                                 
			WHERE DETAIL.numFormID = 7 
			  AND MASTER.Grp_id = 5 
			  AND DETAIL.numDomainID = @numDomainID 
			 AND numAuthGroupID = @numBizDocId                                                 
			  AND vcFieldType = 'C' 
			  AND intRowNum >= @intRowNum 
			ORDER BY intRowNum                                                                  
			           
			IF @@rowcount=0 SET @intRowNum=0                                                                                                                                                
		END	
	PRINT 'END'	
	SELECT * FROM #temp_Packing_List
END
END