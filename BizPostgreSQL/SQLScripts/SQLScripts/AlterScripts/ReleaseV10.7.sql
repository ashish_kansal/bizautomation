/******************************************************************
Project: Release 10.7 Date: 19.DEC.2018
Comments: ALTER SCRIPTS
*******************************************************************/

/******************************************** SANDEEP *********************************************/

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,numFormID
)
VALUES
(
	35,409,'Class','SelectBox','',1,1,1,140
)

UPDATE DycFormField_Mapping SET bitImport=1,intSectionID=1 WHERE numFormID=140 AND numFieldId=409


DECLARE @numInclutionDetailFieldID INT
SELECT @numInclutionDetailFieldID=numFieldId FROM DycFieldMaster WHERE vcOrigDbColumnName='vcInclusionDetails'
UPDATE DycFieldMaster SET bitImport=1 WHERE numFieldID=@numInclutionDetailFieldID
INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,numFormID,intSectionID
)
VALUES
(
	3,@numInclutionDetailFieldID,'Inclusion Details','TextBox','vcInclusionDetails',1,1,1,136,1
)

ALTER TABLE Item ADD numExpenseChartAcntId NUMERIC(18,0)
ALTER TABLE Item ADD bitExpenseItem BIT