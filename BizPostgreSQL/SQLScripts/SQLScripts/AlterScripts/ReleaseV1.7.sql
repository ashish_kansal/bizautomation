/******************************************************************
Project: Release 1.7 Date: 04.07.2013
Comments: 
*******************************************************************/

/*******************Kamal Script******************/

--CalAmount to monDealAmount
UPDATE DycFieldMaster SET vcOrigDbColumnName='monDealAmount' WHERE numModuleID=3 AND numFieldId=104


--ADC.vcFirstName to vcFirstName
UPDATE DycFieldMaster SET vcOrigDbColumnName='vcFirstName' WHERE numModuleID=2 AND numFieldId=51

INSERT INTO [dbo].[DycFieldMaster]([numModuleID], [numDomainID], [vcFieldName], [vcDbColumnName], [vcOrigDbColumnName], [vcPropertyName], [vcLookBackTableName], [vcFieldDataType], [vcFieldType], [vcAssociatedControlType], [vcToolTip], [vcListItemType], [numListID], [PopupFunctionName], [order], [tintRow], [tintColumn], [bitInResults], [bitDeleted], [bitAllowEdit], [bitDefault], [bitSettingField], [bitAddField], [bitDetailField], [bitAllowSorting], [bitWorkFlowField], [bitImport], [bitExport], [bitAllowFiltering], [bitInlineEdit], [bitRequired], [intColumnWidth], [intFieldMaxLength])
SELECT 3, NULL, N'Manufacturer', N'vcManufacturer', N'vcManufacturer', NULL, N'OpportunityItems', N'N', N'R', N'TextBox', NULL, NULL, 0, NULL, 18, NULL, NULL, 1, 0, 0, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL


--ItemType to vcType
UPDATE DycFieldMaster SET vcOrigDbColumnName='vcType' WHERE numModuleID=3 AND numFieldId=256


--Add ListId into Item Class
UPDATE DycFieldMaster SET vcListItemType='LI',numListID=381 WHERE numModuleID=3 AND numFieldId=264


--OpportunityBizDocs Fields
INSERT INTO [dbo].[DycFieldMaster]([numModuleID], [numDomainID], [vcFieldName], [vcDbColumnName], [vcOrigDbColumnName], [vcPropertyName], [vcLookBackTableName], [vcFieldDataType], [vcFieldType], [vcAssociatedControlType], [vcToolTip], [vcListItemType], [numListID], [PopupFunctionName], [order], [tintRow], [tintColumn], [bitInResults], [bitDeleted], [bitAllowEdit], [bitDefault], [bitSettingField], [bitAddField], [bitDetailField], [bitAllowSorting], [bitWorkFlowField], [bitImport], [bitExport], [bitAllowFiltering], [bitInlineEdit], [bitRequired], [intColumnWidth], [intFieldMaxLength])
SELECT 3, NULL, N'Is Authoritative', N'bitAuthoritativeBizDocs', N'bitAuthoritativeBizDocs', NULL, N'OpportunityBizDocs', N'Y', N'R', N'CheckBox', NULL, NULL, 0, NULL, 18, NULL, NULL, 1, 0, 0, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL
UNION
SELECT 3, NULL, N'Created By', N'numCreatedBy', N'numCreatedBy', NULL, N'OpportunityBizDocs', N'N', N'R', N'SelectBox', NULL, 'U', 0, NULL, 18, NULL, NULL, 1, 0, 0, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL
UNION
SELECT 3, NULL, N'Created Date', N'dtCreatedDate', N'dtCreatedDate', NULL, N'OpportunityBizDocs', N'V', N'R', N'DateField', NULL, NULL, 0, NULL, 18, NULL, NULL, 1, 0, 0, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL
UNION
SELECT 3, NULL, N'Modified By', N'numModifiedBy', N'numModifiedBy', NULL, N'OpportunityBizDocs', N'N', N'R', N'SelectBox', NULL, 'U', 0, NULL, 18, NULL, NULL, 1, 0, 0, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL
UNION
SELECT 3, NULL, N'Modified Date', N'dtModifiedDate', N'dtModifiedDate', NULL, N'OpportunityBizDocs', N'V', N'R', N'DateField', NULL, NULL, 0, NULL, 18, NULL, NULL, 1, 0, 0, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL
UNION
SELECT 3, NULL, N'Deferred', N'tintDeferred', N'tintDeferred', NULL, N'OpportunityBizDocs', N'Y', N'R', N'CheckBox', NULL, NULL, 0, NULL, 18, NULL, NULL, 1, 0, 0, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL
UNION
SELECT 3, NULL, N'Billing Date', N'dtFromDate', N'dtFromDate', NULL, N'OpportunityBizDocs', N'V', N'R', N'DateField', NULL, NULL, 0, NULL, 18, NULL, NULL, 1, 0, 0, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL
UNION
SELECT 3, NULL, N'Due Date', N'dtDueDate', N'dtDueDate', NULL, N'OpportunityBizDocs', N'V', N'R', N'DateField', NULL, NULL, 0, NULL, 18, NULL, NULL, 1, 0, 0, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL
UNION
SELECT 3, NULL, N'Amount Due', N'monAmountDue', N'monAmountDue', NULL, N'OpportunityBizDocs', N'M', N'R', N'TextBox', NULL, NULL, 0, NULL, 18, NULL, NULL, 1, 0, 0, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL

SELECT * FROM dbo.DycFieldMaster WHERE numModuleID=3
--Item Field
INSERT INTO [dbo].[DycFieldMaster]([numModuleID], [numDomainID], [vcFieldName], [vcDbColumnName], [vcOrigDbColumnName], [vcPropertyName], [vcLookBackTableName], [vcFieldDataType], [vcFieldType], [vcAssociatedControlType], [vcToolTip], [vcListItemType], [numListID], [PopupFunctionName], [order], [tintRow], [tintColumn], [bitInResults], [bitDeleted], [bitAllowEdit], [bitDefault], [bitSettingField], [bitAddField], [bitDetailField], [bitAllowSorting], [bitWorkFlowField], [bitImport], [bitExport], [bitAllowFiltering], [bitInlineEdit], [bitRequired], [intColumnWidth], [intFieldMaxLength])
SELECT 4, NULL, N'Allow DropShip', N'bitAllowDropShip', N'bitAllowDropShip', N'bitAllowDropShip', N'Item', N'Y', N'R', N'CheckBox', NULL, N'', 0, NULL, NULL, NULL, NULL, 1, 0, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL UNION ALL
SELECT 4, NULL, N'Ship Class', N'numShipClass', N'numShipClass', N'numShipClass', N'Item', N'V', N'R', N'SelectBox', NULL, N'LI', 381, NULL, 0, NULL, NULL, 1, 0, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL


--Projects to ProjectsMaster
UPDATE DycFieldMaster SET vcLookBackTableName='ProjectsMaster' WHERE numModuleID=5 AND vcLookBackTableName='Projects'

--numReason to numCreatedBy
UPDATE DycFieldMaster SET vcOrigDbColumnName='numCreatedBy' WHERE numModuleID=5 AND vcOrigDbColumnName='numReason'

--Case Field
INSERT INTO [dbo].[DycFieldMaster]([numModuleID], [numDomainID], [vcFieldName], [vcDbColumnName], [vcOrigDbColumnName], [vcPropertyName], [vcLookBackTableName], [vcFieldDataType], [vcFieldType], [vcAssociatedControlType], [vcToolTip], [vcListItemType], [numListID], [PopupFunctionName], [order], [tintRow], [tintColumn], [bitInResults], [bitDeleted], [bitAllowEdit], [bitDefault], [bitSettingField], [bitAddField], [bitDetailField], [bitAllowSorting], [bitWorkFlowField], [bitImport], [bitExport], [bitAllowFiltering], [bitInlineEdit], [bitRequired], [intColumnWidth], [intFieldMaxLength])
SELECT 7, NULL, N'Created By', N'numCreatedBy', N'numCreatedBy', N'', N'Cases', N'N', N'R', N'SelectBox', NULL, N'U', 0, NULL, 16, NULL, NULL, 1, 0, 0, 0, 1, NULL, 0, 1, 1, NULL, NULL, 1, NULL, NULL, NULL, NULL UNION ALL
SELECT 7, NULL, N'Modified By', N'numModifiedBy', N'numModifiedBy', N'', N'Cases', N'N', N'R', N'SelectBox', NULL, N'U', 0, NULL, 16, NULL, NULL, 1, 0, 0, 0, 1, NULL, 0, 1, 1, NULL, NULL, 1, NULL, NULL, NULL, NULL UNION ALL
SELECT 7, NULL, N'Modified Date', N'bintModifiedDate', N'bintModifiedDate', N'', N'Cases', N'V', N'R', N'DateField', NULL, N'', 0, NULL, 19, NULL, NULL, 1, 0, 0, 0, 1, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL


///**************************************************************************************///


GO
CREATE TABLE [dbo].[ReportModuleMaster](
	[numReportModuleID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[vcModuleName] [varchar](100) NULL,
	[bitActive] [bit] NULL,
 CONSTRAINT [PK_ReportModuleMaster] PRIMARY KEY CLUSTERED 
(
	[numReportModuleID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


SET IDENTITY_INSERT [dbo].[ReportModuleMaster] ON;

INSERT INTO [dbo].[ReportModuleMaster]([numReportModuleID], [vcModuleName], [bitActive])
SELECT 1, N'Organization', 1 UNION ALL
SELECT 2, N'Contacts', 1 UNION ALL
SELECT 3, N'Opportunities/Orders', 1 UNION ALL
SELECT 4, N'Projects', 1 UNION ALL
SELECT 5, N'Cases', 1 UNION ALL
SELECT 6, N'Items', 1

SET IDENTITY_INSERT [dbo].[ReportModuleMaster] OFF;


///*****************************************///


GO
CREATE TABLE [dbo].[ReportModuleGroupMaster](
	[numReportModuleGroupID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numReportModuleID] [numeric](18, 0) NOT NULL,
	[vcGroupName] [varchar](50) NULL,
	[bitActive] [bit] NULL,
 CONSTRAINT [PK_ReportModuleGroupMaster] PRIMARY KEY CLUSTERED 
(
	[numReportModuleGroupID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


SET IDENTITY_INSERT [dbo].[ReportModuleGroupMaster] ON;

INSERT INTO [dbo].[ReportModuleGroupMaster]([numReportModuleGroupID], [numReportModuleID], [vcGroupName], [bitActive])
SELECT 1, 1, N'Organization', 1 UNION ALL
SELECT 2, 1, N'Organization Association', 0 UNION ALL
SELECT 3, 1, N'Organization With Assets', 0 UNION ALL
SELECT 4, 1, N'Orgnization Action Items', 0 UNION ALL
SELECT 5, 2, N'Contacts', 1 UNION ALL
SELECT 6, 2, N'Contacts & Action Items', 0 UNION ALL
SELECT 7, 2, N'Contacts & Organization', 0 UNION ALL
SELECT 8, 3, N'Opportunities BizDocs', 1 UNION ALL
SELECT 9, 3, N'Opportunities BizDocs Items', 1 UNION ALL
SELECT 10, 3, N'Opportunities Items', 1 UNION ALL
SELECT 11, 3, N'Opportunities Items BizDocs', 0 UNION ALL
SELECT 12, 3, N'Opportunities MileStones', 0 UNION ALL
SELECT 13, 3, N'Opportunities/Orders', 1 UNION ALL
SELECT 14, 4, N'Projects', 1 UNION ALL
SELECT 15, 4, N'Projects and Stages ', 0 UNION ALL
SELECT 16, 5, N'Cases', 1 UNION ALL
SELECT 17, 5, N'Cases Tasks', 0 UNION ALL
SELECT 18, 5, N'Cases with Contracts', 0 UNION ALL
SELECT 19, 6, N'Items', 1 UNION ALL
SELECT 20, 6, N'Items Maintenance & Repair', 0 UNION ALL
SELECT 21, 6, N'Items with Attributes', 0 UNION ALL
SELECT 22, 6, N'Items with Options & Accesories', 0 UNION ALL
SELECT 23, 6, N'WareHouse', 1

SET IDENTITY_INSERT [dbo].[ReportModuleGroupMaster] OFF;

///*****************************************///


GO
CREATE TABLE [dbo].[ReportFieldGroupMaster](
	[numReportFieldGroupID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[vcFieldGroupName] [varchar](100) NOT NULL,
	[bitActive] [bit] NOT NULL,
	[bitCustomFieldGroup] [bit] NULL,
	[numGroupID] [numeric](18, 0) NULL,
	[vcCustomTableName] [varchar](50) NULL,
 CONSTRAINT [PK_ReportFieldGroupMaster] PRIMARY KEY CLUSTERED 
(
	[numReportFieldGroupID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET IDENTITY_INSERT [dbo].[ReportFieldGroupMaster] ON;

INSERT INTO [dbo].[ReportFieldGroupMaster]([numReportFieldGroupID], [vcFieldGroupName], [bitActive], [bitCustomFieldGroup], [numGroupID], [vcCustomTableName])
SELECT 1, N'Organization', 1, NULL, NULL, NULL UNION ALL
SELECT 2, N'Contacts', 1, NULL, NULL, NULL UNION ALL
SELECT 3, N'Opportunities/Orders', 1, NULL, NULL, NULL UNION ALL
SELECT 4, N'Opportunities Items', 1, NULL, NULL, NULL UNION ALL
SELECT 5, N'BizDocs', 1, NULL, NULL, NULL UNION ALL
SELECT 6, N'BizDocs Items', 1, NULL, NULL, NULL UNION ALL
SELECT 7, N'Items', 1, NULL, NULL, NULL UNION ALL
SELECT 10, N'Items with Options & Accesories', 1, NULL, NULL, NULL UNION ALL
SELECT 11, N'WareHouses', 1, NULL, NULL, NULL UNION ALL
SELECT 15, N'Organization Custom Field', 1, 1, 1, N'CFW_FLD_Values' UNION ALL
SELECT 16, N'Contacts Custom Field', 1, 1, 4, N'CFW_FLD_Values_Cont' UNION ALL
SELECT 17, N'Sales Custom Field', 1, 1, 2, N'CFW_Fld_Values_Opp' UNION ALL
SELECT 18, N'Purchase Custom Field', 1, 1, 6, N'CFW_Fld_Values_Opp' UNION ALL
SELECT 19, N'Items Custom Field', 1, 1, 5, N'CFW_FLD_Values_Item' UNION ALL
SELECT 20, N'Items Attributes', 1, 1, 9, N'CFW_Fld_Values_Serialized_Items' UNION ALL
SELECT 21, N'Projects', 1, NULL, NULL, NULL UNION ALL
SELECT 22, N'Projects Custom Field', 1, 1, 11, N'CFW_FLD_Values_Pro' UNION ALL
SELECT 23, N'Cases', 1, NULL, NULL, NULL UNION ALL
SELECT 24, N'Cases Custom Field', 1, 1, 3, N'CFW_FLD_Values_Case'

SET IDENTITY_INSERT [dbo].[ReportFieldGroupMaster] OFF;


///*****************************************///


CREATE TABLE [dbo].[ReportModuleGroupFieldMappingMaster](
	[numReportModuleGroupID] [numeric](18, 0) NOT NULL,
	[numReportFieldGroupID] [numeric](18, 0) NOT NULL,
 CONSTRAINT [PK_ReportGroupFieldMaster] PRIMARY KEY CLUSTERED 
(
	[numReportModuleGroupID] ASC,
	[numReportFieldGroupID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]


INSERT INTO [dbo].[ReportModuleGroupFieldMappingMaster]([numReportModuleGroupID], [numReportFieldGroupID])
SELECT 1, 1 UNION ALL
SELECT 1, 15 UNION ALL
SELECT 5, 1 UNION ALL
SELECT 5, 2 UNION ALL
SELECT 5, 15 UNION ALL
SELECT 5, 16 UNION ALL
SELECT 8, 1 UNION ALL
SELECT 8, 2 UNION ALL
SELECT 8, 3 UNION ALL
SELECT 8, 5 UNION ALL
SELECT 8, 17 UNION ALL
SELECT 8, 18 UNION ALL
SELECT 9, 1 UNION ALL
SELECT 9, 2 UNION ALL
SELECT 9, 3 UNION ALL
SELECT 9, 4 UNION ALL
SELECT 9, 5 UNION ALL
SELECT 9, 6 UNION ALL
SELECT 9, 17 UNION ALL
SELECT 9, 18 UNION ALL
SELECT 10, 1 UNION ALL
SELECT 10, 2 UNION ALL
SELECT 10, 3 UNION ALL
SELECT 10, 4 UNION ALL
SELECT 10, 17 UNION ALL
SELECT 10, 18 UNION ALL
SELECT 13, 1 UNION ALL
SELECT 13, 2 UNION ALL
SELECT 13, 3 UNION ALL
SELECT 13, 17 UNION ALL
SELECT 13, 18 UNION ALL
SELECT 14, 1 UNION ALL
SELECT 14, 15 UNION ALL
SELECT 14, 21 UNION ALL
SELECT 14, 22 UNION ALL
SELECT 16, 1 UNION ALL
SELECT 16, 15 UNION ALL
SELECT 16, 23 UNION ALL
SELECT 16, 24 UNION ALL
SELECT 19, 7 UNION ALL
SELECT 19, 19 UNION ALL
SELECT 23, 7 UNION ALL
SELECT 23, 11 UNION ALL
SELECT 23, 19 UNION ALL
SELECT 23, 20


///*****************************************///


GO
CREATE TABLE [dbo].[ReportFieldGroupMappingMaster](
	[numReportFieldGroupID] [numeric](18, 0) NOT NULL,
	[numFieldID] [numeric](18, 0) NOT NULL,
	[vcFieldName] [nvarchar](50) NULL,
	[vcAssociatedControlType] [nvarchar](50) NULL,
	[vcFieldDataType] [char](1) NULL,
	[bitAllowSorting] [bit] NULL,
	[bitAllowGrouping] [bit] NULL,
	[bitAllowAggregate] [bit] NULL,
	[bitAllowFiltering] [bit] NULL,
 CONSTRAINT [PK_ReportFieldGroupMappingMaster] PRIMARY KEY CLUSTERED 
(
	[numReportFieldGroupID] ASC,
	[numFieldID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


///*****************************************///

GO
CREATE TABLE [dbo].[ReportListMaster](
	[numReportID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[vcReportName] [varchar](200) NULL,
	[vcReportDescription] [varchar](500) NULL,
	[numDomainID] [numeric](18, 0) NULL,
	[numReportModuleID] [numeric](18, 0) NULL,
	[numReportModuleGroupID] [numeric](18, 0) NULL,
	[tintReportType] [tinyint] NULL,
	[numCreatedBy] [numeric](18, 0) NULL,
	[numModifiedBy] [numeric](18, 0) NULL,
	[dtCreatedDate] [datetime] NULL,
	[dtModifiedDate] [datetime] NULL,
	[bitActive] [bit] NULL,
	[textQuery] [text] NULL,
	[numSortColumnmReportFieldGroupID] [numeric](18, 0) NULL,
	[numSortColumnFieldID] [numeric](18, 0) NULL,
	[vcSortColumnDirection] [varchar](10) NULL,
	[bitSortColumnCustom] [bit] NULL,
	[vcFilterLogic] [varchar](200) NULL,
	[numDateReportFieldGroupID] [numeric](18, 0) NULL,
	[numDateFieldID] [numeric](18, 0) NULL,
	[bitDateFieldColumnCustom] [bit] NULL,
	[vcDateFieldValue] [varchar](50) NULL,
	[dtFromDate] [datetime] NULL,
	[dtToDate] [datetime] NULL,
	[tintRecordFilter] [tinyint] NULL,
	[vcKPIMeasureFieldID] [varchar](50) NULL,
	[intNoRows] [int] NULL,
 CONSTRAINT [PK_ReportListMaster] PRIMARY KEY CLUSTERED 
(
	[numReportID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING ON
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0: Tabular, 1: Summary, 2:Matrix, 3:KPI, 4:ScoreCard' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ReportListMaster', @level2type=N'COLUMN',@level2name=N'tintReportType'


///*****************************************///


GO
CREATE TABLE [dbo].[ReportFieldsList](
	[numReportFieldID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numReportID] [numeric](18, 0) NOT NULL,
	[numFieldID] [numeric](18, 0) NOT NULL,
	[bitCustom] [bit] NOT NULL,
	[numReportFieldGroupID] [numeric](18, 0) NOT NULL,
 CONSTRAINT [PK_ReportFieldsMaster] PRIMARY KEY CLUSTERED 
(
	[numReportID] ASC,
	[numFieldID] ASC,
	[bitCustom] ASC,
	[numReportFieldGroupID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]


///*****************************************///


GO
CREATE TABLE [dbo].[ReportFilterList](
	[numReportFilterID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numReportID] [numeric](18, 0) NOT NULL,
	[numFieldID] [numeric](18, 0) NOT NULL,
	[bitCustom] [bit] NOT NULL,
	[vcFilterValue] [varchar](500) NOT NULL,
	[vcFilterOperator] [varchar](50) NOT NULL,
	[vcFilterText] [varchar](2000) NULL,
	[numReportFieldGroupID] [numeric](18, 0) NULL
) ON [PRIMARY]

GO

///*****************************************///

GO
CREATE TABLE [dbo].[ReportSummaryGroupList](
	[numReportGroupID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numReportID] [numeric](18, 0) NOT NULL,
	[numFieldID] [numeric](18, 0) NOT NULL,
	[bitCustom] [bit] NOT NULL,
	[numReportFieldGroupID] [numeric](18, 0) NOT NULL,
	[tintBreakType] [tinyint] NOT NULL,
 CONSTRAINT [PK_ReportGroupListMaster] PRIMARY KEY CLUSTERED 
(
	[numReportID] ASC,
	[numFieldID] ASC,
	[bitCustom] ASC,
	[numReportFieldGroupID] ASC,
	[tintBreakType] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0:Group Summary 1:Matrix Column, 2:Matrix Row' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ReportSummaryGroupList', @level2type=N'COLUMN',@level2name=N'tintBreakType'

///*****************************************///

GO
CREATE TABLE [dbo].[ReportMatrixAggregateList](
	[numReportAggregateID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numReportID] [numeric](18, 0) NOT NULL,
	[numFieldID] [numeric](18, 0) NOT NULL,
	[bitCustom] [bit] NOT NULL,
	[vcAggType] [varchar](50) NOT NULL,
	[numReportFieldGroupID] [numeric](18, 0) NULL
) ON [PRIMARY]

GO

///*****************************************///

GO
CREATE TABLE [dbo].[ReportKPIThresold](
	[numReportThresoldID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numReportID] [numeric](18, 0) NOT NULL,
	[vcThresoldType] [varchar](50) NULL,
	[decValue1] [decimal](18, 2) NULL,
	[decValue2] [decimal](18, 2) NULL,
	[decGoalValue] [decimal](18, 2) NULL,
 CONSTRAINT [PK_ReportKPIThresold] PRIMARY KEY CLUSTERED 
(
	[numReportThresoldID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

///*****************************************///

GO
CREATE TABLE [dbo].[ReportKPIGroupListMaster](
	[numReportKPIGroupID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numDomainID] [numeric](18, 0) NOT NULL,
	[vcKPIGroupName] [varchar](200) NULL,
	[vcKPIGroupDescription] [varchar](500) NULL,
	[tintKPIGroupReportType] [tinyint] NULL,
 CONSTRAINT [PK_ReportKPIList] PRIMARY KEY CLUSTERED 
(
	[numReportKPIGroupID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING ON
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'3:KPI 4:ScoreCard' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ReportKPIGroupListMaster', @level2type=N'COLUMN',@level2name=N'tintKPIGroupReportType'

///*****************************************///

GO
CREATE TABLE [dbo].[ReportKPIGroupDetailList](
	[numReportKPIGroupID] [numeric](18, 0) NULL,
	[numReportID] [numeric](18, 0) NULL
) ON [PRIMARY]


///*****************************************///


GO
CREATE TABLE [dbo].[UserReportList](
	[numUserReportID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numReportID] [numeric](18, 0) NOT NULL,
	[numDomainID] [numeric](18, 0) NOT NULL,
	[numUserCntID] [numeric](18, 0) NOT NULL,
	[tintReportType] [tinyint] NOT NULL,
 CONSTRAINT [PK_UserReportList] PRIMARY KEY CLUSTERED 
(
	[numUserReportID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0:Pre-defined 1:Custom Reports' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserReportList', @level2type=N'COLUMN',@level2name=N'tintReportType'


///*****************************************///


GO
CREATE TABLE [dbo].[ReportDashboard](
	[numDashBoardID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numDomainID] [numeric](18, 0) NULL,
	[numReportID] [numeric](18, 0) NOT NULL,
	[numUserCntID] [numeric](18, 0) NOT NULL,
	[tintReportType] [tinyint] NOT NULL,
	[tintChartType] [tinyint] NOT NULL,
	[vcHeaderText] [varchar](100) NULL,
	[vcFooterText] [varchar](100) NULL,
	[tintRow] [tinyint] NOT NULL,
	[tintColumn] [tinyint] NOT NULL,
	[vcXAxis] [varchar](50) NULL,
	[vcYAxis] [varchar](50) NULL,
	[vcAggType] [varchar](50) NULL,
	[tintReportCategory] [tinyint] NULL,
 CONSTRAINT [PK_ReportDashboard] PRIMARY KEY CLUSTERED 
(
	[numDashBoardID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING ON
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1:Table 2:Chart' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ReportDashboard', @level2type=N'COLUMN',@level2name=N'tintReportType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0:Custom Report 1:KPI/ScoreCard Group Report' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ReportDashboard', @level2type=N'COLUMN',@level2name=N'tintReportCategory'


ALTER TABLE dbo.ReportDashboard ADD
	intHeight int NULL,
	intWidth int NULL
///*****************************************///


GO
CREATE TABLE [dbo].[ReportDashboardAllowedReports](
	[numReportID] [numeric](18, 0) NULL,
	[numDomainID] [numeric](18, 0) NULL,
	[numGrpID] [numeric](18, 0) NULL,
	[tintReportCategory] [tinyint] NULL
) ON [PRIMARY]

GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0:Custom Report 1:KPI/ScoreCard Group Report' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ReportDashboardAllowedReports', @level2type=N'COLUMN',@level2name=N'tintReportCategory'


///*****************************************///

GO
CREATE TABLE [dbo].[ReportDashBoardSize](
	[numDomainID] [numeric](18, 0) NULL,
	[numUserCntID] [numeric](18, 0) NULL,
	[tintColumn] [tinyint] NULL,
	[tintSize] [tinyint] NULL
) ON [PRIMARY]

///*****************************************///


INSERT INTO [dbo].[ReportFieldGroupMappingMaster]([numReportFieldGroupID], [numFieldID], [vcFieldName], [vcAssociatedControlType], [vcFieldDataType], [bitAllowSorting], [bitAllowGrouping], [bitAllowAggregate], [bitAllowFiltering])
SELECT 1, numFieldId, N'Company Differentiation', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=1 AND vcOrigDbColumnName='numCompanyDiff' UNION ALL
SELECT 1, numFieldId, N'Company Differentiation Value', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=1 AND vcOrigDbColumnName='vcCompanyDiff' UNION ALL
SELECT 1, numFieldId, N'Organization Name', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=1 AND vcOrigDbColumnName='vcCompanyName' UNION ALL
SELECT 1, numFieldId, N'Organization Profile', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=1 AND vcOrigDbColumnName='vcProfile' UNION ALL
SELECT 1, numFieldId, N'Organization Relationship', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=1 AND vcOrigDbColumnName='numCompanyType' UNION ALL
SELECT 1, numFieldId, N'Campaign', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=1 AND vcOrigDbColumnName='numCampaignID' UNION ALL
SELECT 1, numFieldId, N'Organization Phone', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=1 AND vcOrigDbColumnName='vcComPhone' UNION ALL
SELECT 1, numFieldId, N'Assigned To', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=1 AND vcOrigDbColumnName='numAssignedTo' UNION ALL
SELECT 1, numFieldId, N'Employees', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=1 AND vcOrigDbColumnName='numNoOfEmployeesId' UNION ALL
SELECT 1, numFieldId, N'Annual Revenue', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=1 AND vcOrigDbColumnName='numAnnualRevID' UNION ALL
SELECT 1, numFieldId, N'Web', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=1 AND vcOrigDbColumnName='vcWebSite' UNION ALL
SELECT 1, numFieldId, N'Follow-up Status', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=1 AND vcOrigDbColumnName='numFollowUpStatus' UNION ALL
SELECT 1, numFieldId, N'Group', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=1 AND vcOrigDbColumnName='numGrpID' UNION ALL
SELECT 1, numFieldId, N'Territory', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=1 AND vcOrigDbColumnName='numTerID' UNION ALL
SELECT 1, numFieldId, N'Lead Source', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=1 AND vcOrigDbColumnName='vcHow' UNION ALL
SELECT 1, numFieldId, N'Industry', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=1 AND vcOrigDbColumnName='numCompanyIndustry' UNION ALL
SELECT 1, numFieldId, N'Organization Rating', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=1 AND vcOrigDbColumnName='numCompanyRating' UNION ALL
SELECT 1, numFieldId, N'Organization Status', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=1 AND vcOrigDbColumnName='numStatusID' UNION ALL
SELECT 1, numFieldId, N'Organization Credit Limit', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=1 AND vcOrigDbColumnName='numCompanyCredit' UNION ALL
SELECT 1, numFieldId, N'Organization Fax', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=1 AND vcOrigDbColumnName='vcComFax' UNION ALL
SELECT 1, numFieldId, N'Assigned By', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=1 AND vcOrigDbColumnName='numAssignedBy' UNION ALL
SELECT 1, numFieldId, N'Organization Created Date', N'DateField', N'D', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=1 AND vcOrigDbColumnName='bintCreatedDate' UNION ALL
SELECT 1, numFieldId, N'Organization Comments', N'TextArea', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=1 AND vcOrigDbColumnName='txtComments' UNION ALL
SELECT 1, numFieldId, N'Private', N'CheckBox', N'Y', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=1 AND vcOrigDbColumnName='bitPublicFlag' UNION ALL
SELECT 1, numFieldId, N'Active', N'CheckBox', N'Y', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=1 AND vcOrigDbColumnName='bitActiveInActive' UNION ALL
SELECT 1, numFieldId, N'Web Link 1', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=1 AND vcOrigDbColumnName='vcWebLink1' UNION ALL
SELECT 1, numFieldId, N'Web Link 2', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=1 AND vcOrigDbColumnName='vcWebLink2' UNION ALL
SELECT 1, numFieldId, N'Web Link 3', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=1 AND vcOrigDbColumnName='vcWebLink3' UNION ALL
SELECT 1, numFieldId, N'Web Link 4', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=1 AND vcOrigDbColumnName='vcWebLink4' UNION ALL
SELECT 1, numFieldId, N'Ship To Street', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=1 AND vcOrigDbColumnName='vcStreet' AND vcDbColumnName='vcShipStreet' UNION ALL
SELECT 1, numFieldId, N'Ship To City', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=1 AND vcOrigDbColumnName='vcCity' AND vcDbColumnName='vcShipCity' UNION ALL
SELECT 1, numFieldId, N'Ship To State', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=1 AND vcOrigDbColumnName='numState' AND vcDbColumnName='numShipState' UNION ALL
SELECT 1, numFieldId, N'Ship To Postal Code', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=1 AND vcOrigDbColumnName='vcPostalCode' AND vcDbColumnName='vcShipPostCode' UNION ALL
SELECT 1, numFieldId, N'Ship To Country', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=1 AND vcOrigDbColumnName='numCountry' AND vcDbColumnName='numShipCountry' UNION ALL
SELECT 1, numFieldId, N'Bill To Street', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=1 AND vcOrigDbColumnName='vcStreet' AND vcDbColumnName='vcBillStreet' UNION ALL
SELECT 1, numFieldId, N'Bill To City', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=1 AND vcOrigDbColumnName='vcCity' AND vcDbColumnName='vcBillCity' UNION ALL
SELECT 1, numFieldId, N'Bill To State', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=1 AND vcOrigDbColumnName='numState' AND vcDbColumnName='numBillState' UNION ALL
SELECT 1, numFieldId, N'Bill To Postal Code', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=1 AND vcOrigDbColumnName='vcPostalCode' AND vcDbColumnName='vcBillPostCode' UNION ALL
SELECT 1, numFieldId, N'Bill To Country', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=1 AND vcOrigDbColumnName='numCountry' AND vcDbColumnName='numBillCountry' UNION ALL
SELECT 1, numFieldId, N'Record Owner', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=1 AND vcOrigDbColumnName='numRecOwner' UNION ALL
SELECT 1, numFieldId, N'Relationship Type', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=2 AND vcOrigDbColumnName='tintCRMType' UNION ALL
SELECT 2, numFieldId, N'Contact First Name', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=2 AND vcOrigDbColumnName='vcFirstName' UNION ALL
SELECT 2, numFieldId, N'Contact Last Name', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=2 AND vcOrigDbColumnName='vcLastName' UNION ALL
SELECT 2, numFieldId, N'Contact Email', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=2 AND vcOrigDbColumnName='vcEmail' UNION ALL
SELECT 2, numFieldId, N'Contact Alternate Email', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=2 AND vcOrigDbColumnName='vcAltEmail' UNION ALL
SELECT 2, numFieldId, N'Date of Birth', N'DateField', N'D', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=2 AND vcOrigDbColumnName='bintDOB' UNION ALL
SELECT 2, numFieldId, N'Contact Type', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=2 AND vcOrigDbColumnName='numContactType' UNION ALL
SELECT 2, numFieldId, N'Manager', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=2 AND vcOrigDbColumnName='numManagerID' UNION ALL
SELECT 2, numFieldId, N'Contact Phone', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=2 AND vcOrigDbColumnName='numPhone' UNION ALL
SELECT 2, numFieldId, N'Contact Phone Ext', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=2 AND vcOrigDbColumnName='numPhoneExtension' UNION ALL
SELECT 2, numFieldId, N'Contact Cell Phone', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=2 AND vcOrigDbColumnName='numCell' UNION ALL
SELECT 2, numFieldId, N'Contact Home Phone', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=2 AND vcOrigDbColumnName='numHomePhone' UNION ALL
SELECT 2, numFieldId, N'Contact Fax', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=2 AND vcOrigDbColumnName='vcFax' UNION ALL
SELECT 2, numFieldId, N'Contact Category', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=2 AND vcOrigDbColumnName='vcCategory' UNION ALL
SELECT 2, numFieldId, N'Contact Team', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=2 AND vcOrigDbColumnName='numTeam' UNION ALL
SELECT 2, numFieldId, N'Contact Position', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=2 AND vcOrigDbColumnName='vcPosition' UNION ALL
SELECT 2, numFieldId, N'Contact Title', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=2 AND vcOrigDbColumnName='vcTitle' UNION ALL
SELECT 2, numFieldId, N'Department Name', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=2 AND vcOrigDbColumnName='vcDepartment' UNION ALL
SELECT 2, numFieldId, N'Gender', N'SelectBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=2 AND vcOrigDbColumnName='charSex' UNION ALL
SELECT 2, numFieldId, N'Opt-Out', N'CheckBox', N'Y', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=2 AND vcOrigDbColumnName='bitOptOut' UNION ALL
SELECT 2, numFieldId, N'Contact Status', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=2 AND vcOrigDbColumnName='numEmpStatus' UNION ALL
SELECT 2, numFieldId, N'Drip Campaign', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=2 AND vcOrigDbColumnName='numECampaignID' UNION ALL
SELECT 2, numFieldId, N'Assistant First Name', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=2 AND vcOrigDbColumnName='vcAsstFirstName' UNION ALL
SELECT 2, numFieldId, N'Assistant Last Name', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=2 AND vcOrigDbColumnName='vcAsstLastName' UNION ALL
SELECT 2, numFieldId, N'Assistant Email', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=2 AND vcOrigDbColumnName='vcAsstEmail' UNION ALL
SELECT 2, numFieldId, N'Assistant Phone', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=2 AND vcOrigDbColumnName='numAsstPhone' UNION ALL
SELECT 2, numFieldId, N'Assistant Phone Ext', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=2 AND vcOrigDbColumnName='numAsstExtn' UNION ALL
SELECT 2, numFieldId, N'Contact Street', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=2 AND vcOrigDbColumnName='vcStreet' UNION ALL
SELECT 2, numFieldId, N'Contact City', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=2 AND vcOrigDbColumnName='vcCity' UNION ALL
SELECT 2, numFieldId, N'Contact State', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=2 AND vcOrigDbColumnName='numState' UNION ALL
SELECT 2, numFieldId, N'Contact Country', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=2 AND vcOrigDbColumnName='numCountry' UNION ALL
SELECT 2, numFieldId, N'Contact Postal Code', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=2 AND vcOrigDbColumnName='vcPostalCode' UNION ALL
SELECT 2, numFieldId, N'Contact Record Owner', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=2 AND vcOrigDbColumnName='numRecOwner' UNION ALL
SELECT 2, numFieldId, N'Contact Created Date', N'DateField', N'D', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=2 AND vcOrigDbColumnName='bintCreatedDate' UNION ALL
SELECT 2, numFieldId, N'Comments', N'TextArea', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=2 AND vcOrigDbColumnName='txtNotes' UNION ALL
SELECT 2, numFieldId, N'Primary Contact', N'CheckBox', N'Y', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=2 AND vcOrigDbColumnName='bitPrimaryContact' UNION ALL
SELECT 3, numFieldId, N'Opp Name', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=3 AND vcOrigDbColumnName='vcPoppName' UNION ALL
SELECT 3, numFieldId, N'Source', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=3 AND vcOrigDbColumnName='tintSource' UNION ALL
SELECT 3, numFieldId, N'Assigned To', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=3 AND vcOrigDbColumnName='numAssignedTo' UNION ALL
SELECT 3, numFieldId, N'Order Status', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=3 AND vcOrigDbColumnName='numStatus' UNION ALL
SELECT 3, numFieldId, N'Conclusion Reason', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=3 AND vcOrigDbColumnName='lngPConclAnalysis' UNION ALL
SELECT 3, numFieldId, N'Order Sub-total', N'TextBox', N'M', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=3 AND vcOrigDbColumnName='monDealAmount' AND vcDbColumnName='CalAmount' UNION ALL
SELECT 3, numFieldId, N'Active', N'CheckBox', N'Y', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=3 AND vcOrigDbColumnName='tintActive' UNION ALL
SELECT 3, numFieldId, N'Due Date', N'DateField', N'D', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=3 AND vcOrigDbColumnName='intpEstimatedCloseDate' UNION ALL
SELECT 3, numFieldId, N'Record Owner', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=3 AND vcOrigDbColumnName='numRecOwner' UNION ALL
SELECT 3, numFieldId, N'Assigned By', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=3 AND vcOrigDbColumnName='numAssignedBy' UNION ALL
SELECT 3, numFieldId, N'Campaign', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=3 AND vcOrigDbColumnName='numCampainID' UNION ALL
SELECT 3, numFieldId, N'Closing Date', N'DateField', N'D', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=3 AND vcOrigDbColumnName='bintAccountClosingDate' UNION ALL
SELECT 3, numFieldId, N'Opp Type', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=3 AND vcOrigDbColumnName='tintOppType' UNION ALL
SELECT 3, numFieldId, N'Created Date', N'DateField', N'D', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=3 AND vcOrigDbColumnName='bintCreatedDate' UNION ALL
SELECT 3, numFieldId, N'Comments', N'TextArea', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=3 AND vcOrigDbColumnName='txtComments' UNION ALL
SELECT 3, numFieldId, N'Invoice Grand-total', N'TextBox', N'M', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=3 AND vcOrigDbColumnName='monDealAmount' AND vcDbColumnName='monDealAmount' AND vcLookBackTableName='OpportunityBizDocs' UNION ALL
SELECT 3, numFieldId, N'Total Amount Paid', N'TextBox', N'M', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=3 AND vcOrigDbColumnName='monAmountPaid' UNION ALL
SELECT 3, numFieldId, N'Order Currency', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=3 AND vcOrigDbColumnName='numCurrencyID' UNION ALL
SELECT 3, numFieldId, N'Customer PO# / Vendor Invoice#', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=3 AND vcOrigDbColumnName='vcOppRefOrderNo' UNION ALL
SELECT 3, numFieldId, N'Percentage Complete', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=3 AND vcOrigDbColumnName='numPercentageComplete' UNION ALL
SELECT 3, numFieldId, N'Marketplace Order ID', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=3 AND vcOrigDbColumnName='vcMarketplaceOrderID' UNION ALL
SELECT 4, numFieldId, N'Item Name', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=3 AND vcOrigDbColumnName='vcItemName' UNION ALL
SELECT 4, numFieldId, N'Model ID', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=3 AND vcOrigDbColumnName='vcModelID' UNION ALL
SELECT 4, numFieldId, N'Description', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=3 AND vcOrigDbColumnName='vcItemDesc' AND vcLookBackTableName='OpportunityItems' UNION ALL
SELECT 4, numFieldId, N'Type', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=3 AND vcOrigDbColumnName='vcType' UNION ALL
SELECT 4, numFieldId, N'Drop Ship', N'CheckBox', N'Y', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=3 AND vcOrigDbColumnName='bitDropShip' UNION ALL
SELECT 4, numFieldId, N'Units', N'TextBox', N'M', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=3 AND vcOrigDbColumnName='numUnitHour' AND vcLookBackTableName='OpportunityItems' UNION ALL
SELECT 4, numFieldId, N'Unit Price', N'TextBox', N'M', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=3 AND vcOrigDbColumnName='monPrice' AND vcLookBackTableName='OpportunityItems' UNION ALL
SELECT 4, numFieldId, N'Amount', N'TextBox', N'M', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=3 AND vcOrigDbColumnName='monTotAmount' AND vcLookBackTableName='OpportunityItems' UNION ALL
SELECT 4, numFieldId, N'Project', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=3 AND vcOrigDbColumnName='numProjectID' UNION ALL
SELECT 4, numFieldId, N'Class', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=3 AND vcOrigDbColumnName='numClassID' UNION ALL
SELECT 4, numFieldId, N'Unit of Measure', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=3 AND vcOrigDbColumnName='numUOMId' UNION ALL
SELECT 4, numFieldId, N'Manufacturer', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=3 AND vcOrigDbColumnName='vcManufacturer' UNION ALL
SELECT 5, numFieldId, N'Customer PO# / Vendor Invoice#', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=3 AND vcOrigDbColumnName='vcRefOrderNo' UNION ALL
SELECT 5, numFieldId, N'Shipping Company', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=3 AND vcOrigDbColumnName='numShipVia' UNION ALL
SELECT 5, numFieldId, N'BizDoc ID', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=3 AND vcOrigDbColumnName='vcBizDocID' UNION ALL
SELECT 5, numFieldId, N'BizDoc Type', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=3 AND vcOrigDbColumnName='numBizDocId' UNION ALL
SELECT 5, numFieldId, N'BizDoc Status', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=3 AND vcOrigDbColumnName='numBizDocStatus' UNION ALL
SELECT 5, numFieldId, N'Deal Amount', N'TextBox', N'M', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=3 AND vcOrigDbColumnName='monDealAmount' AND vcLookBackTableName='OpportunityBizDocs' UNION ALL
SELECT 5, numFieldId, N'Amount Paid', N'TextBox', N'M', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=3 AND vcOrigDbColumnName='monAmountPaid' UNION ALL
SELECT 5, numFieldId, N'Tracking No', N'Label', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=3 AND vcOrigDbColumnName='vcTrackingNo' AND vcLookBackTableName='OpportunityBizDocs' UNION ALL
SELECT 5, numFieldId, N'Amount Due', N'TextBox', N'M', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=3 AND vcOrigDbColumnName='monAmountDue' UNION ALL
SELECT 5, numFieldId, N'Billing Date', N'DateField', N'D', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=3 AND vcOrigDbColumnName='dtFromDate' UNION ALL
SELECT 5, numFieldId, N'Created By', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=3 AND vcOrigDbColumnName='numCreatedBy' UNION ALL
SELECT 5, numFieldId, N'Created Date', N'DateField', N'D', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=3 AND vcOrigDbColumnName='dtCreatedDate' UNION ALL
SELECT 5, numFieldId, N'Deferred', N'CheckBox', N'Y', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=3 AND vcOrigDbColumnName='tintDeferred' UNION ALL
SELECT 5, numFieldId, N'Due Date', N'DateField', N'D', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=3 AND vcOrigDbColumnName='dtDueDate' UNION ALL
SELECT 5, numFieldId, N'Is Authoritative', N'CheckBox', N'Y', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=3 AND vcOrigDbColumnName='bitAuthoritativeBizDocs' UNION ALL
SELECT 5, numFieldId, N'Modified By', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=3 AND vcOrigDbColumnName='numModifiedBy' UNION ALL
SELECT 5, numFieldId, N'Modified Date', N'DateField', N'D', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=3 AND vcOrigDbColumnName='dtModifiedDate' UNION ALL
SELECT 6, numFieldId, N'Units', N'TextBox', N'M', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=3 AND vcOrigDbColumnName='numUnitHour' AND vcLookBackTableName='OpportunityBizDocItems' UNION ALL
SELECT 6, numFieldId, N'Unit Price', N'TextBox', N'M', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=3 AND vcOrigDbColumnName='monPrice' AND vcLookBackTableName='OpportunityBizDocItems' UNION ALL
SELECT 6, numFieldId, N'Amount', N'TextBox', N'M', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=3 AND vcOrigDbColumnName='monTotAmount' AND vcLookBackTableName='OpportunityBizDocItems' UNION ALL
SELECT 7, numFieldId, N'Item', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=4 AND vcOrigDbColumnName='vcItemName' UNION ALL
SELECT 7, numFieldId, N'List Price (NI)', N'TextBox', N'M', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=4 AND vcOrigDbColumnName='monListPrice' UNION ALL
SELECT 7, numFieldId, N'Model ID', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=4 AND vcOrigDbColumnName='vcModelID' UNION ALL
SELECT 7, numFieldId, N'Manufacturer', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=4 AND vcOrigDbColumnName='vcManufacturer' UNION ALL
SELECT 7, numFieldId, N'UPC(NI)', N'TextBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=4 AND vcOrigDbColumnName='numBarCodeId' UNION ALL
SELECT 7, numFieldId, N'Width', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=4 AND vcOrigDbColumnName='fltWidth' UNION ALL
SELECT 7, numFieldId, N'Height', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=4 AND vcOrigDbColumnName='fltHeight' UNION ALL
SELECT 7, numFieldId, N'Weight', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=4 AND vcOrigDbColumnName='fltWeight' UNION ALL
SELECT 7, numFieldId, N'Length', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=4 AND vcOrigDbColumnName='fltLength' UNION ALL
SELECT 7, numFieldId, N'Average Cost', N'TextBox', N'M', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=4 AND vcOrigDbColumnName='monAverageCost' UNION ALL
SELECT 7, numFieldId, N'Is Serialized', N'CheckBox', N'Y', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=4 AND vcOrigDbColumnName='bitSerialized' UNION ALL
SELECT 7, numFieldId, N'Is Kit', N'CheckBox', N'Y', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=4 AND vcOrigDbColumnName='bitKitParent' UNION ALL
SELECT 7, numFieldId, N'Is Assembly', N'CheckBox', N'Y', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=4 AND vcOrigDbColumnName='bitAssembly' UNION ALL
SELECT 7, numFieldId, N'Is Archive', N'CheckBox', N'Y', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=4 AND vcOrigDbColumnName='IsArchieve' UNION ALL
SELECT 7, numFieldId, N'Is Lot No', N'CheckBox', N'Y', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=4 AND vcOrigDbColumnName='bitLotNo' UNION ALL
SELECT 7, numFieldId, N'Allow Back Order', N'CheckBox', N'Y', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=4 AND vcOrigDbColumnName='bitAllowBackOrder' UNION ALL
SELECT 7, numFieldId, N'Item Classification', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=4 AND vcOrigDbColumnName='numItemClassification' UNION ALL
SELECT 7, numFieldId, N'Item Group', N'SelectBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=4 AND vcOrigDbColumnName='numItemGroup' UNION ALL
SELECT 7, numFieldId, N'Purchase Unit', N'SelectBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=4 AND vcOrigDbColumnName='numPurchaseUnit' UNION ALL
SELECT 7, numFieldId, N'Sale Unit', N'SelectBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=4 AND vcOrigDbColumnName='numSaleUnit' UNION ALL
SELECT 7, numFieldId, N'Item Class', N'SelectBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=4 AND vcOrigDbColumnName='numItemClass' UNION ALL
SELECT 7, numFieldId, N'Labour Cost', N'TextBox', N'M', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=4 AND vcOrigDbColumnName='monCampaignLabourCost' UNION ALL
SELECT 7, numFieldId, N'Base Unit', N'SelectBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=4 AND vcOrigDbColumnName='numBaseUnit' UNION ALL
SELECT 7, numFieldId, N'SKU(NI)', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=4 AND vcOrigDbColumnName='vcSKU' UNION ALL
SELECT 7, numFieldId, N'Item Type', N'SelectBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=4 AND vcOrigDbColumnName='charItemType' UNION ALL
SELECT 7, numFieldId, N'Free Shipping', N'CheckBox', N'Y', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=4 AND vcOrigDbColumnName='bitFreeShipping' UNION ALL
SELECT 7, numFieldId, N'Has Sales Tax', N'CheckBox', N'Y', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=4 AND vcOrigDbColumnName='bitTaxable' UNION ALL
SELECT 7, numFieldId, N'Allow DropShip', N'CheckBox', N'Y', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=4 AND vcOrigDbColumnName='bitAllowDropShip' UNION ALL
SELECT 7, numFieldId, N'Ship Class', N'SelectBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=4 AND vcOrigDbColumnName='numShipClass' UNION ALL
SELECT 11, numFieldId, N'On Hand', N'TextBox', N'M', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=4 AND vcOrigDbColumnName='numOnHand' UNION ALL
SELECT 11, numFieldId, N'On Order', N'TextBox', N'M', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=4 AND vcOrigDbColumnName='numOnOrder' UNION ALL
SELECT 11, numFieldId, N'On Allocation', N'TextBox', N'M', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=4 AND vcOrigDbColumnName='numAllocation' UNION ALL
SELECT 11, numFieldId, N'On Backorder', N'TextBox', N'M', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=4 AND vcOrigDbColumnName='numBackOrder' UNION ALL
SELECT 11, numFieldId, N'WareHouse', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=4 AND vcOrigDbColumnName='vcWareHouse' UNION ALL
SELECT 11, numFieldId, N'Reorder', N'TextBox', N'M', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=4 AND vcOrigDbColumnName='numReorder' UNION ALL
SELECT 11, numFieldId, N'Warehouse Location', N'SelectBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=4 AND vcOrigDbColumnName='vcLocation' UNION ALL
SELECT 11, numFieldId, N'List Price (I)', N'TextBox', N'M', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=4 AND vcOrigDbColumnName='monWListPrice' UNION ALL
SELECT 11, numFieldId, N'UPC (M)', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=4 AND vcOrigDbColumnName='vcBarCode' UNION ALL
SELECT 11, numFieldId, N'SKU (M)', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=4 AND vcOrigDbColumnName='vcWHSKU' UNION ALL
SELECT 11, numFieldId, N'Street', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=4 AND vcOrigDbColumnName='vcWStreet' UNION ALL
SELECT 11, numFieldId, N'City', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=4 AND vcOrigDbColumnName='vcWCity' UNION ALL
SELECT 11, numFieldId, N'PinCode', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=4 AND vcOrigDbColumnName='vcWPinCode' UNION ALL
SELECT 11, numFieldId, N'Country', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=4 AND vcOrigDbColumnName='numWCountry' UNION ALL
SELECT 11, numFieldId, N'State', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=4 AND vcOrigDbColumnName='numWState' UNION ALL
SELECT 21, numFieldId, N'Project ID', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=5 AND vcOrigDbColumnName='vcProjectID' UNION ALL
SELECT 21, numFieldId, N'Project Name', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=5 AND vcOrigDbColumnName='vcProjectName' UNION ALL
SELECT 21, numFieldId, N'Assigned To', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=5 AND vcOrigDbColumnName='numAssignedTo' UNION ALL
SELECT 21, numFieldId, N'Internal Project Manager', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=5 AND vcOrigDbColumnName='numintPrjMgr' UNION ALL
SELECT 21, numFieldId, N'Customer Side Project Manager', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=5 AND vcOrigDbColumnName='numCustPrjMgr' UNION ALL
SELECT 21, numFieldId, N'Due Date', N'DateField', N'D', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=5 AND vcOrigDbColumnName='intDueDate' UNION ALL
SELECT 21, numFieldId, N'Contract', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=5 AND vcOrigDbColumnName='numContractId' UNION ALL
SELECT 21, numFieldId, N'Created By', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=5 AND vcOrigDbColumnName='numCreatedBy' UNION ALL
SELECT 21, numFieldId, N'Created Date', N'DateField', N'D', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=5 AND vcOrigDbColumnName='bintCreatedDate' UNION ALL
SELECT 21, numFieldId, N'Record Owner', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=5 AND vcOrigDbColumnName='numRecOwner' UNION ALL
SELECT 21, numFieldId, N'Assigned By', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=5 AND vcOrigDbColumnName='numAssignedBy' UNION ALL
SELECT 21, numFieldId, N'ClosingDate', N'DateField', N'D', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=5 AND vcOrigDbColumnName='bintProClosingDate' UNION ALL
SELECT 21, numFieldId, N'Project Type', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=5 AND vcOrigDbColumnName='numProjectType' UNION ALL
SELECT 21, numFieldId, N'Project Status', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=5 AND vcOrigDbColumnName='numProjectStatus' UNION ALL
SELECT 23, numFieldId, N'Subject', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=7 AND vcOrigDbColumnName='textSubject' UNION ALL
SELECT 23, numFieldId, N'Priority', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=7 AND vcOrigDbColumnName='numPriority' UNION ALL
SELECT 23, numFieldId, N'Origin', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=7 AND vcOrigDbColumnName='numOrigin' UNION ALL
SELECT 23, numFieldId, N'Resolve Date', N'DateField', N'D', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=7 AND vcOrigDbColumnName='intTargetResolveDate' UNION ALL
SELECT 23, numFieldId, N'Type', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=7 AND vcOrigDbColumnName='numType' UNION ALL
SELECT 23, numFieldId, N'Assigned To', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=7 AND vcOrigDbColumnName='numAssignedTo' UNION ALL
SELECT 23, numFieldId, N'Reason', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=7 AND vcOrigDbColumnName='numReason' UNION ALL
SELECT 23, numFieldId, N'Status', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=7 AND vcOrigDbColumnName='numStatus' UNION ALL
SELECT 23, numFieldId, N'Case Number', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=7 AND vcOrigDbColumnName='vcCaseNumber' UNION ALL
SELECT 23, numFieldId, N'Record Owner', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=7 AND vcOrigDbColumnName='numRecOwner' UNION ALL
SELECT 23, numFieldId, N'Assigned By', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=7 AND vcOrigDbColumnName='numAssignedBy' UNION ALL
SELECT 23, numFieldId, N'Created Date', N'DateField', N'D', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=7 AND vcOrigDbColumnName='bintCreatedDate' UNION ALL
SELECT 23, numFieldId, N'Contract', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=7 AND vcOrigDbColumnName='numContractId' UNION ALL
SELECT 23, numFieldId, N'Created By', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=7 AND vcOrigDbColumnName='numCreatedBy' UNION ALL
SELECT 23, numFieldId, N'Modified By', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=7 AND vcOrigDbColumnName='numModifiedBy' UNION ALL
SELECT 23, numFieldId, N'Modified Date', N'DateField', N'D', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=7 AND vcOrigDbColumnName='bintModifiedDate'


------------------------------------------

INSERT INTO [dbo].[DycFieldMaster]([numModuleID], [numDomainID], [vcFieldName], [vcDbColumnName], [vcOrigDbColumnName], [vcPropertyName], [vcLookBackTableName], [vcFieldDataType], [vcFieldType], [vcAssociatedControlType], [vcToolTip], [vcListItemType], [numListID], [PopupFunctionName], [order], [tintRow], [tintColumn], [bitInResults], [bitDeleted], [bitAllowEdit], [bitDefault], [bitSettingField], [bitAddField], [bitDetailField], [bitAllowSorting], [bitWorkFlowField], [bitImport], [bitExport], [bitAllowFiltering], [bitInlineEdit], [bitRequired], [intColumnWidth], [intFieldMaxLength])
SELECT 4, NULL, N'Created By', N'numCreatedBy', N'numCreatedBy', NULL, N'Item', N'N', N'R', N'SelectBox', NULL, 'U', 0, NULL, 18, NULL, NULL, 1, 0, 0, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL
UNION
SELECT 4, NULL, N'Created Date', N'bintCreatedDate', N'bintCreatedDate', NULL, N'Item', N'V', N'R', N'DateField', NULL, NULL, 0, NULL, 18, NULL, NULL, 1, 0, 0, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL
UNION
SELECT 4, NULL, N'Modified By', N'numModifiedBy', N'numModifiedBy', NULL, N'Item', N'N', N'R', N'SelectBox', NULL, 'U', 0, NULL, 18, NULL, NULL, 1, 0, 0, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL
UNION
SELECT 4, NULL, N'Modified Date', N'bintModifiedDate', N'bintModifiedDate', NULL, N'Item', N'V', N'R', N'DateField', NULL, NULL, 0, NULL, 18, NULL, NULL, 1, 0, 0, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL


INSERT INTO [dbo].[ReportFieldGroupMappingMaster]([numReportFieldGroupID], [numFieldID], [vcFieldName], [vcAssociatedControlType], [vcFieldDataType], [bitAllowSorting], [bitAllowGrouping], [bitAllowAggregate], [bitAllowFiltering])
SELECT 7, numFieldId, N'Created By', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=4 AND vcOrigDbColumnName='numCreatedBy' UNION ALL
SELECT 7, numFieldId, N'Created Date', N'DateField', N'D', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=4 AND vcOrigDbColumnName='bintCreatedDate' UNION ALL
SELECT 7, numFieldId, N'Modified By', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=4 AND vcOrigDbColumnName='numModifiedBy' UNION ALL
SELECT 7, numFieldId, N'Modified Date', N'DateField', N'D', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=4 AND vcOrigDbColumnName='bintModifiedDate'


/*******************Manish Script******************/

/******************************************************************
Project: BACRMUI   Date: 2.Jul.2013
Comments: Add AssignTo for New Case in Administration->Field Management
*******************************************************************/
BEGIN TRANSACTION

UPDATE dbo.DycFormField_Mapping SET bitAddField = 1, bitDetailField = 1 WHERE numFormID = 12 AND  vcFieldName  = 'Assigned To'
 
exec usp_GetLayoutInfoDdl @numUserCntId=0,@intcoulmn=0,@numDomainID=1,@PageId=3,@numRelation=0,@numFormID=12,@tintPageType=2

ROLLBACK 
/******************************************************************
Project: BACRMUI   Date: 2.Jul.2013
Comments: Add Item ID for Item Column search
*******************************************************************/
BEGIN TRANSACTION

INSERT INTO dbo.DycFormField_Mapping 
(numModuleID,numFieldID,numDomainID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,[order],
 tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,
 bitAllowFiltering,bitRequired,numFormFieldID,intSectionID,bitAllowGridColor)
SELECT numModuleID,numFieldID,numDomainID,22,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,[order],
	   tintRow,tintColumn,bitInResults,0,bitDefault,1,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,
	   bitAllowFiltering,bitRequired,numFormFieldID,intSectionID,bitAllowGridColor
FROM dbo.DycFormField_Mapping WHERE numFormID = 20 AND vcFieldName = 'Item ID'	   
 
exec USP_GetColumnConfiguration1 @numDomainID=1,@numUserCntId=1,@FormId=22,@numtype=1,@numViewID=0

ROLLBACK 

/******************************************************************
Project: BACRMUI   Date: 1.Jul.2013
Comments: . In case details "Description" field is missing from the "Available Fields" list (and from initial list grid). Add it. 
		    Also, "InternalComments" field should be re labeled to "Internal Comments". The space is needed
*******************************************************************/
BEGIN TRANSACTION
--SELECT * FROM dbo.DycFormField_Mapping WHERE numFormID = 12 AND vcFieldName = 'Description'

UPDATE dbo.DycFormField_Mapping SET bitDetailField = 1
WHERE numFormID = 12 AND vcFieldName = 'Description'

UPDATE dbo.DycFormField_Mapping SET vcFieldName = 'Internal Comments'
WHERE numFormID = 12 AND vcFieldName = 'InternalComments'

exec usp_GetLayoutInfoDdl @numUserCntId=1,@intcoulmn=0,@numDomainID=1,@PageId=3,@numRelation=0,@numFormID=12,@tintPageType=3
ROLLBACK

/******************************************************************
Project: BACRMUI   Date: 27.Jun.2013
Comments: Add ItemID in Available field for Bizdocs
*******************************************************************/
BEGIN TRANSACTION
INSERT INTO dbo.DycFormField_Mapping (
	numModuleID,
	numFieldID,
	numDomainID,
	numFormID,
	bitAllowEdit,
	bitInlineEdit,
	vcFieldName,
	vcAssociatedControlType,
	vcPropertyName,
	PopupFunctionName,
	[order],
	tintRow,
	tintColumn,
	bitInResults,
	bitDeleted,
	bitDefault,
	bitSettingField,
	bitAddField,
	bitDetailField,
	bitAllowSorting,
	bitWorkFlowField,
	bitImport,
	bitExport,
	bitAllowFiltering,
	bitRequired,
	numFormFieldID,
	intSectionID,
	bitAllowGridColor 
)
SELECT numModuleID,
		numFieldID,
		numDomainID,
		7,
		bitAllowEdit,
		bitInlineEdit,
		vcFieldName,
		vcAssociatedControlType,
		vcPropertyName,
		PopupFunctionName,
		[order],
		tintRow,
		tintColumn,
		bitInResults,
		bitDeleted,
		bitDefault,
		bitSettingField,
		bitAddField,
		bitDetailField,
		bitAllowSorting,
		bitWorkFlowField,
		bitImport,
		bitExport,
		bitAllowFiltering,
		bitRequired,
		NULL,
		NULL,
		0 FROM dbo.DycFieldMaster  WHERE vcDbColumnName = 'numItemCode' AND numFieldId = 211
UNION 
SELECT numModuleID,
		numFieldID,
		numDomainID,
		8,
		bitAllowEdit,
		bitInlineEdit,
		vcFieldName,
		vcAssociatedControlType,
		vcPropertyName,
		PopupFunctionName,
		[order],
		tintRow,
		tintColumn,
		bitInResults,
		bitDeleted,
		bitDefault,
		bitSettingField,
		bitAddField,
		bitDetailField,
		bitAllowSorting,
		bitWorkFlowField,
		bitImport,
		bitExport,
		bitAllowFiltering,
		bitRequired,
		NULL,
		NULL,
		0 FROM dbo.DycFieldMaster  WHERE vcDbColumnName = 'numItemCode' AND numFieldId = 211	 	
ROLLBACK

/******************************************************************
Project: BACRMUI   Date: 26.Jun.2013
Comments: Add Item Thumbnail Image for ItemList
*******************************************************************/
BEGIN TRANSACTION

INSERT INTO dbo.DycFormField_Mapping 
(numModuleID,numFieldID,numDomainID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,[order],
 tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,
 bitAllowFiltering,bitRequired,numFormFieldID,intSectionID,bitAllowGridColor)
SELECT numModuleID,numFieldID,numDomainID,21,bitAllowEdit,bitInlineEdit,'Item Image',vcAssociatedControlType,vcPropertyName,PopupFunctionName,[order],
	   tintRow,tintColumn,bitInResults,0,bitDefault,1,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,
	   bitAllowFiltering,bitRequired,numFormFieldID,intSectionID,bitAllowGridColor
FROM dbo.DycFormField_Mapping WHERE numFormID = 20 AND vcFieldName = 'Item Thumbnail Iamge'	   
 
exec USP_GetColumnConfiguration1 @numDomainID=1,@numUserCntId=1,@FormId=21,@numtype=0,@numViewID=0
-- exec usp_getFormConfigFields @numDomainId=1,@numFormId=29,@numAuthGroupId=1,@numBizDocTemplateID=0

ROLLBACK 

/******************************************************************
Project: BACRMUI   Date: 20.Jun.2013
Comments: Add Received Qty in Products & Services for PO only
*******************************************************************/
BEGIN TRANSACTION

INSERT INTO dbo.DycFieldMaster 
(numModuleID,numDomainID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,
vcToolTip,vcListItemType,numListID,PopupFunctionName,[order],tintRow,tintColumn,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,
bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,bitAllowFiltering,bitInlineEdit,bitRequired,intColumnWidth,intFieldMaxLength) 
SELECT 4,NULL,'Received','numUnitHourReceived','numUnitHourReceived','QtyReceived','OpportunityItems','N','R','TextBox',
'Received Quantity','',0,NULL,	62,	0,	1,	1,	0,	1,	0,	1,	1,	
1,	1,	1,	1,	NULL,	1,	NULL,	NULL,	NULL,	NULL

DECLARE @numFieldID AS BIGINT
SELECT @numFieldID = numFieldId FROM dbo.DycFieldMaster WHERE vcFieldName = 'Received' AND vcDbColumnName = 'numUnitHourReceived'
INSERT INTO dbo.DycFormField_Mapping 
(numModuleID,numFieldID,numDomainID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,[order],
 tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,
 bitAllowFiltering,bitRequired,numFormFieldID,intSectionID,bitAllowGridColor)
SELECT 4,@numFieldID,NULL,26,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,[order],
	   tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,1,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,
	   bitAllowFiltering,bitRequired,0,1,NULL
FROM dbo.DycFieldMaster WHERE vcFieldName = 'Received' AND vcDbColumnName = 'numUnitHourReceived'
 
exec USP_GetColumnConfiguration1 @numDomainID=1,@numUserCntId=1,@FormId=26,@numtype=0,@numViewID=0

-----------------
ALTER TABLE Item ADD bitArchiveItem BIT

ROLLBACK 

/******************************************************************
Project: BACRMUI   Date: 19.Jun.2013
Comments: Add Item Thumbnail Image for Products & Services
*******************************************************************/
BEGIN TRANSACTION

INSERT INTO dbo.DycFormField_Mapping 
(numModuleID,numFieldID,numDomainID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,[order],
 tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,
 bitAllowFiltering,bitRequired,numFormFieldID,intSectionID,bitAllowGridColor)
SELECT numModuleID,numFieldID,numDomainID,26,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,[order],
	   tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,1,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,
	   bitAllowFiltering,bitRequired,numFormFieldID,intSectionID,bitAllowGridColor
FROM dbo.DycFormField_Mapping WHERE numFormID = 20 AND vcFieldName = 'Item Thumbnail Image'	   
 
exec USP_GetColumnConfiguration1 @numDomainID=1,@numUserCntId=1,@FormId=26,@numtype=0,@numViewID=0
-- exec usp_getFormConfigFields @numDomainId=1,@numFormId=29,@numAuthGroupId=1,@numBizDocTemplateID=0

ROLLBACK 

/******************************************************************
Project: BACRMUI   Date: 18.Jun.2013
Comments: Add Item Filters
*******************************************************************/
BEGIN TRANSACTION
	INSERT dbo.DynamicFormMaster 
	( numFormId,vcFormName,cCustomFieldsAssociated,cAOIAssociated,bitDeleted,tintFlag,bitWorkFlow,vcLocationID,bitAllowGridColor )
	VALUES 
	( /* numFormId - numeric(10, 0) */ 76 ,
	  /* vcFormName - nvarchar(50) */ N'Item Filter',
	  /* cCustomFieldsAssociated - char(1) */ 'Y',
	  /* cAOIAssociated - char(1) */ 'N',
	  /* bitDeleted - bit */ 0,
	  /* tintFlag - tinyint */ 2,
	  /* bitWorkFlow - bit */ NULL,
	  /* vcLocationID - varchar(50) */ '',
	  /* bitAllowGridColor - bit */ NULL ) 
	  
	--SELECT * FROM dbo.DynamicFormMaster WHERE numFormID > 66
	--SELECT * FROM dbo.DycFormField_Mapping WHERE numFormID = 66  AND vcAssociatedControlType ='SelectBox'
	--DELETE FROM dbo.DycFormField_Mapping WHERE numFormID = 66
	
	INSERT INTO dbo.DycFormField_Mapping 
	(numModuleID,numFieldID,numDomainID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,[order],
	 tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,
	 bitAllowFiltering,bitRequired,numFormFieldID,intSectionID,bitAllowGridColor)
	 SELECT numModuleID,numFieldID,numDomainID,76,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,1,
	 tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,1,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,0,bitExport,
	 1,bitRequired,numFormFieldID,intSectionID,bitAllowGridColor FROM dbo.DycFormField_Mapping WHERE vcFieldName = 'Item Classification' AND numFormID = 20
	 UNION 
	 SELECT numModuleID,numFieldID,numDomainID,76,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,2,
	 tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,1,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,0,bitExport,
	 1,bitRequired,numFormFieldID,intSectionID,bitAllowGridColor FROM dbo.DycFormField_Mapping WHERE vcFieldName = 'Item Category' AND numFormID = 20
	 UNION 
	 SELECT numModuleID,numFieldID,numDomainID,76,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,3,
	 tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,1,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,0,bitExport,
	 1,bitRequired,numFormFieldID,intSectionID,bitAllowGridColor FROM dbo.DycFormField_Mapping WHERE vcFieldName = 'Item Group' AND numFormID = 20
	 UNION 
	 SELECT numModuleID,numFieldID,numDomainID,76,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,4,
	 tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,1,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,0,bitExport,
	 1,bitRequired,numFormFieldID,intSectionID,bitAllowGridColor FROM dbo.DycFormField_Mapping WHERE vcFieldName = 'Shipping Class' AND numFormID = 20
	 UNION 
	 SELECT numModuleID,numFieldID,numDomainID,76,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,5,
	 tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,1,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,0,bitExport,
	 1,bitRequired,numFormFieldID,intSectionID,bitAllowGridColor FROM dbo.DycFormField_Mapping WHERE vcFieldName = 'Item Class' AND numFormID = 20
	 	 
	 --SELECT * FROM dbo.DycFormField_Mapping WHERE numFormID = @numMaxFormID 
	 
RollBACk

/******************************************************************
Project: BACRMUI   Date: 17.Jun.2013
Comments: Add IsArchieve for Products & Services
*******************************************************************/
BEGIN TRANSACTION

INSERT INTO dbo.DycFormField_Mapping 
(numModuleID,numFieldID,numDomainID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,[order],
 tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,
 bitAllowFiltering,bitRequired,numFormFieldID,intSectionID,bitAllowGridColor)
SELECT numModuleID,numFieldID,numDomainID,26,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,[order],
	   tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,1,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,
	   bitAllowFiltering,bitRequired,numFormFieldID,intSectionID,bitAllowGridColor
FROM dbo.DycFormField_Mapping WHERE numFormID = 20 AND vcFieldName LIKE '%archi%'	   
 
exec USP_GetColumnConfiguration1 @numDomainID=1,@numUserCntId=1,@FormId=26,@numtype=0,@numViewID=0
-- exec usp_getFormConfigFields @numDomainId=1,@numFormId=29,@numAuthGroupId=1,@numBizDocTemplateID=0

ROLLBACK 


/******************************************************************
Project: BACRMUI   Date: 28.May.2013
Comments: Add IsReplied to get status of mail whether it is replied or not.
*******************************************************************/
BEGIN TRANSACTION
ALTER TABLE EmailHistory ADD IsReplied BIT
UPDATE EmailHistory SET IsReplied = 0 
--SELECT IsReplied,* FROM dbo.EmailHistory
ROLLBACK


/*******************Joseph Script******************/

/***********************************************************************************************
Project: Redirect Configurations  Date: 03/June/2013
Comments: 
************************************************************************************************/

INSERT TreeNavigationAuthorization ([numGroupID] ,[numTabID] ,[numPageNavID] ,[bitVisible] ,[numDomainID] ,tintType)
SELECT numGroupID,-1, 221, 1, numDomainID, 1 
FROM AuthenticationGroupMaster GR
WHERE NOT EXISTS (SELECT * FROM TreeNavigationAuthorization c
WHERE GR.numDomainID = c.numDomainID AND GR.numGroupID = c.numGroupID AND c.numTabID = -1 AND c.numPageNavID = 221)



/***********************************************************************************************
Project: Accounts List setting Bill City and Ship City in Settings: 18/June/2013
Comments: 
************************************************************************************************/

Update DycFormField_Mapping Set bitSettingField  = 1 where numFormId = 36 and vcFieldName IN ('Ship To City','Bill To City')
