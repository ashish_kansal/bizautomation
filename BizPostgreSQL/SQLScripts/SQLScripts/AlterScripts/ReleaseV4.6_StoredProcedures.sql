/******************************************************************
Project: Release 4.6 Date: 12.Jun.2015
Comments: STORED PROCEDURES
*******************************************************************/


GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_getOPPAddress')
DROP FUNCTION fn_getOPPAddress
GO
CREATE FUNCTION [dbo].[fn_getOPPAddress] (@numOppId numeric,@numDomainID  NUMERIC,@tintMode AS TINYINT)
RETURNS VARCHAR(1000)
AS
BEGIN
	DECLARE @strAddress varchar(1000)
	DECLARE  @tintOppType  AS TINYINT
	DECLARE  @tintBillType  AS TINYINT
	DECLARE  @tintShipType  AS TINYINT
	DECLARE @numBillToAddressID AS NUMERIC(18,0)
	DECLARE @numShipToAddressID AS NUMERIC(18,0)
 
	DECLARE @numParentOppID AS NUMERIC,@numDivisionID AS NUMERIC 
      
	SELECT  
		@tintOppType = tintOppType,
		@tintBillType = tintBillToType,
		@numBillToAddressID = numBillToAddressID,
		@tintShipType = tintShipToType,
		@numShipToAddressID = numShipToAddressID,
		@numDivisionID = numDivisionID
	FROM   
		OpportunityMaster 
	WHERE  
		numOppId = @numOppId

	-- When Creating PO from SO and Bill type is Customer selected 
	SELECT @numParentOppID=ISNULL(numParentOppID,0) FROM dbo.OpportunityLinking WHERE numChildOppID = @numOppId
            
	IF @tintMode=1 --Billing Address
	BEGIN
		If ISNULL(@numBillToAddressID,0) > 0
		BEGIN
			SELECT  
				@strAddress='<pre>' + isnull(AD.vcStreet,'') + '</pre>'
								+ isnull(AD.VcCity,'') + ' ,' + isnull(dbo.fn_GetState(AD.numState),'') + ' ' + isnull(AD.vcPostalCode,'')
								+ ' <br>' + isnull(dbo.fn_GetListItemName(AD.numCountry),'')
			FROM 
				AddressDetails AD 
			WHERE
				AD.numDomainID=@numDomainID 
				AND AD.numAddressID = @numBillToAddressID
		END
		ELSE
		BEGIN
				IF @tintBillType IS NULL OR (@tintBillType = 1 AND @tintOppType = 1) --Primary Bill Address or When Sales order and bill to is set to customer	 
				BEGIN
					SELECT  
						@strAddress='<pre>' + isnull(AD.vcStreet,'') + '</pre>'
									+ isnull(AD.VcCity,'') + ' ,' + isnull(dbo.fn_GetState(AD.numState),'') + ' ' + isnull(AD.vcPostalCode,'')
									+ ' <br>' + isnull(dbo.fn_GetListItemName(AD.numCountry),'')
					FROM 
						AddressDetails AD 
					WHERE 
						AD.numDomainID=@numDomainID AND AD.numRecordID=@numDivisionID 
							AND AD.tintAddressOf = 2 AND AD.tintAddressType = 1 AND AD.bitIsPrimary=1                    
				END
				ELSE IF @tintBillType = 1 AND @tintOppType = 2 -- When Create PO from SO and Bill to is set to Customer
				BEGIN
					SELECT @strAddress=dbo.fn_getOPPAddress(@numParentOppID,@numDomainID,@tintMode)
				END
				ELSE IF @tintBillType = 0
				BEGIN
					SELECT @strAddress='<pre>' + isnull(AD.vcStreet,'') + '</pre>'
												+ isnull(AD.VcCity,'') + ' ,' + isnull(dbo.fn_GetState(AD.numState),'') + ' ' + isnull(AD.vcPostalCode,'')
												+ ' <br>' + isnull(dbo.fn_GetListItemName(AD.numCountry),'')
										 FROM   companyinfo [Com1] JOIN divisionmaster div1 ON com1.numCompanyID = div1.numCompanyID
												JOIN Domain D1 ON D1.numDivisionID = div1.numDivisionID
												JOIN dbo.AddressDetails AD ON AD.numDomainID = div1.numDomainID AND AD.numRecordID = div1.numDivisionID AND tintAddressOf = 2 AND tintAddressType = 1 AND bitIsPrimary=1
														WHERE  D1.numDomainID = @numDomainID
				END
				ELSE IF @tintBillType = 2 OR @tintBillType = 3
				BEGIN
					SELECT @strAddress='<pre>' + vcBillStreet + ' </pre>' 
											   + VcBillCity + ' ,' + isnull(dbo.fn_GetState(numBillState),'') + ' ' + vcBillPostCode
											   + ' <br>' + isnull(dbo.fn_GetListItemName(numBillCountry),'')
									FROM   OpportunityAddress WHERE  numOppID = @numOppId
				END
		END
	END
	ELSE IF @tintMode=2 --Shipping Address
	BEGIN
		If ISNULL(@numShipToAddressID,0) > 0
		BEGIN
			SELECT  
				@strAddress='<pre>' + isnull(AD.vcStreet,'') + '</pre>'
								+ isnull(AD.VcCity,'') + ' ,' + isnull(dbo.fn_GetState(AD.numState),'') + ' ' + isnull(AD.vcPostalCode,'')
								+ ' <br>' + isnull(dbo.fn_GetListItemName(AD.numCountry),'')
			FROM 
				AddressDetails AD 
			WHERE
				AD.numDomainID=@numDomainID 
				AND AD.numAddressID = @numShipToAddressID
		END
		ELSE
		BEGIN
			IF @tintShipType IS NULL OR (@tintShipType = 1 AND @tintOppType = 1)
			BEGIN
					SELECT  @strAddress='<pre>' + isnull(AD.vcStreet,'') + '</pre>'
											+ isnull(AD.VcCity,'') + ' ,' + isnull(dbo.fn_GetState(AD.numState),'') + ' ' + isnull(AD.vcPostalCode,'')
											+ ' <br>' + isnull(dbo.fn_GetListItemName(AD.numCountry),'')
					FROM AddressDetails AD 
						WHERE AD.numDomainID=@numDomainID AND AD.numRecordID=@numDivisionID 
						AND AD.tintAddressOf = 2 AND AD.tintAddressType = 2 AND AD.bitIsPrimary=1
			END
			ELSE IF @tintShipType = 1 AND @tintOppType = 2 -- When Create PO from SO and Ship to is set to Customer 
			BEGIN
				SELECT @strAddress=dbo.fn_getOPPAddress(@numParentOppID,@numDomainID,@tintMode)
			END
			ELSE IF @tintShipType = 0
			BEGIN
				SELECT @strAddress='<pre>' + isnull(AD.vcStreet,'') + '</pre>'
											+ isnull(AD.VcCity,'') + ' ,' + isnull(dbo.fn_GetState(AD.numState),'') + ' ' + isnull(AD.vcPostalCode,'')
											+ ' <br>' + isnull(dbo.fn_GetListItemName(AD.numCountry),'')
									 FROM   companyinfo [Com1] JOIN divisionmaster div1 ON com1.numCompanyID = div1.numCompanyID
											JOIN Domain D1 ON D1.numDivisionID = div1.numDivisionID
											JOIN dbo.AddressDetails AD ON AD.numDomainID = div1.numDomainID AND AD.numRecordID = div1.numDivisionID AND tintAddressOf = 2 AND tintAddressType = 2 AND bitIsPrimary=1
													WHERE  D1.numDomainID = @numDomainID
			END
			ELSE IF @tintShipType = 2 OR @tintShipType = 3
			BEGIN
				SELECT @strAddress='<pre>' + vcShipStreet + ' </pre>' 
										   + VcShipCity + ' ,' + isnull(dbo.fn_GetState(numShipState),'') + ' ' + vcShipPostCode
										   + ' <br>' + isnull(dbo.fn_GetListItemName(numShipCountry),'')
								FROM   OpportunityAddress WHERE  numOppID = @numOppId
			END
		END
	END

return @strAddress
end
GO
/****** Object:  UserDefinedFunction [dbo].[GetFiscalQuarter]    Script Date: 07/26/2008 18:13:02 ******/

GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='getfiscalquarter')
DROP FUNCTION getfiscalquarter
GO
CREATE FUNCTION [dbo].[GetFiscalQuarter](@date datetime,@numDomainId numeric(4))
returns integer
as 
begin

declare @year as numeric(4)

set @year =dbo.getfiscalyear(@date,@numDomainId)




--print @date
--print @year
Declare @SQ1 as datetime 
Declare @SQ2 as datetime 
Declare @SQ3 as datetime 
Declare @SQ4 as datetime 
Declare @EQ1 as datetime 
Declare @EQ2 as datetime 
Declare @EQ3 as datetime 
Declare @EQ4 as datetime 
Declare @Quat as numeric(2)
--print '----quarter1------------------'


set @SQ1 = dateadd(month,0,dbo.GetFiscalStartDate(@year,@numDomainId)) 
set @EQ1 = dateadd(MS,-3,dateadd(month,3,dbo.GetFiscalStartDate(@year,@numDomainId)))
--print @sq1
--print @eq1
--print '----quarter2------------------'
set @SQ2 = dateadd(month,3,dbo.GetFiscalStartDate(@year,@numDomainId)) 
set @EQ2 = dateadd(MS,-3,dateadd(month,6,dbo.GetFiscalStartDate(@year,@numDomainId)))
--print @sq2
--print @eq2
--print '----quarter3------------------'
set @SQ3 = dateadd(month,6,dbo.GetFiscalStartDate(@year,@numDomainId)) 
set @EQ3 =  dateadd(MS,-3,dateadd(month,9,dbo.GetFiscalStartDate(@year,@numDomainId)))
--print @sq3
--print @eq3
--print '----quarter4------------------'
set @SQ4 = dateadd(month,9,dbo.GetFiscalStartDate(@year,@numDomainId)) 
set @EQ4 =  dateadd(MS,-3,dateadd(month,12,dbo.GetFiscalStartDate(@year,@numDomainId)))
--print @sq4
--print @eq4
--print '------------------------------'

if (@sQ1 <= @date and @eq1>=@date )
begin 
--print '1'
set @Quat =1
end
else
if @sQ2 <= @date  and @eq2 >= @date
begin 
--print '2'
set @Quat =2
end
else
if @sQ3 <= @date and @eq3 >= @date
begin 
--print '3'
set @Quat =3
end
else
if @sQ4 <= @date and @eq4 >= @date
begin 
--print '4'
set @Quat =4
end
--print @Quat
return @Quat
end
GO
/****** Object:  UserDefinedFunction [dbo].[GetFiscalyear]    Script Date: 07/26/2008 18:13:02 ******/

GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='getfiscalyear')
DROP FUNCTION getfiscalyear
GO
CREATE FUNCTION [dbo].[GetFiscalyear](@date datetime,@numDomainId numeric(9))
returns numeric(4)
as
begin 
declare @startMonth as varchar(2)
select @startMonth=isnull(tintfiscalStartMonth,1) from  domain where numDomainId=@numDomainId


declare @GetFiscalYear as numeric(4)

  If @date < (convert(varchar(4),Year(@date))+'/'+ @startMonth +'/'+'01')
	begin
		set @GetFiscalYear =    Year(@date) - 0 - 1
	end
else
	begin
	 set @GetFiscalYear =    Year(@date) - 0
	end
--print @GetFiscalYear
return @GetFiscalYear
end
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_CompanyInfo_Search' ) 
    DROP PROCEDURE USP_CompanyInfo_Search
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Sandeep Patel
-- Create date: 15 April 2014
-- Description:	Gets company info based on search fileds configured
-- =============================================
CREATE PROCEDURE USP_CompanyInfo_Search
	@numDomainID NUMERIC(18,0),
	@numUserCntID NUMERIC(18,0),
	@isStartWithSearch BIT = 0,
	@searchText VARCHAR(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

SELECT @searchText = REPLACE(@searchText,'''', '''''')

DECLARE @CustomerSelectFields AS VARCHAR(MAX)
DECLARE @CustomerSelectCustomFields AS VARCHAR(MAX)
DECLARE @ProspectsRights AS TINYINT                    
DECLARE @accountsRights AS TINYINT                   
DECLARE @leadsRights AS TINYINT 

SET @CustomerSelectFields = ''
SET @CustomerSelectCustomFields = ''
               
SET @ProspectsRights = ( SELECT TOP 1
                                MAX(GA.intViewAllowed) AS intViewAllowed
                         FROM   UserMaster UM ,
                                GroupAuthorization GA ,
                                PageMaster PM
                         WHERE  GA.numGroupID = UM.numGroupID
                                AND PM.numPageID = GA.numPageID
                                AND PM.numModuleID = GA.numModuleID
                                AND PM.numpageID = 2
                                AND PM.numModuleID = 3
                                AND UM.numUserID = ( SELECT numUserID
                                                     FROM   userMaster
                                                     WHERE  numUserDetailId = @numUserCntID
                                                   )
                         GROUP BY UM.numUserId ,
                                UM.vcUserName ,
                                PM.vcFileName ,
                                GA.numModuleID ,
                                GA.numPageID
                       )                        
                    
SET @accountsRights = ( SELECT TOP 1
                                MAX(GA.intViewAllowed) AS intViewAllowed
                        FROM    UserMaster UM ,
                                GroupAuthorization GA ,
                                PageMaster PM
                        WHERE   GA.numGroupID = UM.numGroupID
                                AND PM.numPageID = GA.numPageID
                                AND PM.numModuleID = GA.numModuleID
                                AND PM.numpageID = 2
                                AND PM.numModuleID = 4
                                AND UM.numUserID = ( SELECT numUserID
                                                     FROM   userMaster
                                                     WHERE  numUserDetailId = @numUserCntID
                                                   )
                        GROUP BY UM.numUserId ,
                                UM.vcUserName ,
                                PM.vcFileName ,
                                GA.numModuleID ,
                                GA.numPageID
                      )                
              
SET @leadsRights = ( SELECT TOP 1
                            MAX(GA.intViewAllowed) AS intViewAllowed
                     FROM   UserMaster UM ,
                            GroupAuthorization GA ,
                            PageMaster PM
                     WHERE  GA.numGroupID = UM.numGroupID
                            AND PM.numPageID = GA.numPageID
                            AND PM.numModuleID = GA.numModuleID
                            AND PM.numpageID = 2
                            AND PM.numModuleID = 2
                            AND UM.numUserID = ( SELECT numUserID
                                                 FROM   userMaster
                                                 WHERE  numUserDetailId = @numUserCntID
                                               )
                     GROUP BY UM.numUserId ,
                            UM.vcUserName ,
                            PM.vcFileName ,
                            GA.numModuleID ,
                            GA.numPageID
                   )                   
                    
-- START: Get organization display fields
SELECT * INTO #tempOrgDisplayFields FROM
(
	SELECT 
		numFieldId,
		vcDbColumnName,
		ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName,
		tintRow AS tintOrder,
		0 as Custom
    FROM 
		View_DynamicColumns
	WHERE 
		numFormId=96 AND numUserCntID=@numUserCntID AND numDomainID=@numDomainID AND 
		tintPageType=1 AND bitCustom=0  AND ISNULL(bitSettingField,0)=1 AND numRelCntType=0 AND vcDbColumnName <> 'numCompanyID'
	UNION   
	SELECT 
		numFieldId,
		vcFieldName,
		vcFieldName,
		tintRow AS tintOrder,
		1 as Custom
    FROM 
		View_DynamicCustomColumns   
	WHERE 
		Grp_id=1 AND numFormId=96 AND numUserCntID=@numUserCntID AND numDomainID=@numDomainID AND 
		tintPageType=1 AND bitCustom=1 AND numRelCntType=0
)X

IF EXISTS (SELECT * FROM #tempOrgDisplayFields)
BEGIN

	SELECT 
		@CustomerSelectFields = COALESCE (@CustomerSelectFields,'') + CAST(vcDbColumnName AS VARCHAR(100)) + ','	
	FROM 
		#tempOrgDisplayFields 
	WHERE
		Custom = 0
		
	-- Removes last , from string
	IF DATALENGTH(@CustomerSelectFields) > 0
		SET @CustomerSelectFields = LEFT(@CustomerSelectFields, LEN(@CustomerSelectFields) - 1)

	IF EXISTS(SELECT * FROM #tempOrgDisplayFields WHERE Custom = 1)
	BEGIN
		SELECT 
			@CustomerSelectCustomFields = COALESCE (@CustomerSelectCustomFields,'') + 'dbo.fn_GetCustFldOrganization(' + CAST(numFieldId AS VARCHAR(100)) + ',1,TEMP1.numCompanyID) AS [' + CAST (vcDbColumnName AS VARCHAR (100)) + '],'	
		FROM 
			#tempOrgDisplayFields 
		WHERE
			Custom = 1
	END

	-- Removes last , from string
	IF DATALENGTH(@CustomerSelectCustomFields) > 0
		SET @CustomerSelectCustomFields = LEFT(@CustomerSelectCustomFields, LEN(@CustomerSelectCustomFields) - 1)
END

-- END: Get organization display fields

-- START: Generate default select statement 
                    
DECLARE @strSQL AS VARCHAR(MAX)    

SET @strSQL = 'SELECT numDivisionID, numCompanyID, vcCompanyName' + 
	CASE @CustomerSelectFields
	WHEN '' THEN ''
	ELSE ',' + @CustomerSelectFields
	END
	 + 
	CASE @CustomerSelectCustomFields
	WHEN '' THEN ''
	ELSE ',' + @CustomerSelectCustomFields
	END
	 + ' FROM (SELECT 
	CMP.numDomainID,
	CMP.numCompanyID, 
	CMP.vcCompanyName, 
	DM.numDivisionID, 
	DM.vcDivisionName, 
	ISNULL((SELECT vcData FROM dbo.ListDetails WHERE numListID = 1 AND numDomainID = DM.numDomainID AND numListItemID = DM.numStatusID),'''') as numStatusID, 
	ISNULL((SELECT vcData FROM dbo.ListDetails WHERE numListID = 2 AND numDomainID = DM.numDomainID AND numListItemID = CMP.numCompanyRating),'''') as numCompanyRating, 
	ISNULL((SELECT vcData FROM dbo.ListDetails WHERE numListID = 3 AND numDomainID = DM.numDomainID AND numListItemID = CMP.numCompanyCredit),'''') as numCompanyCredit,
	ISNULL((SELECT vcData FROM dbo.ListDetails WHERE numListID = 4 AND numDomainID = DM.numDomainID AND numListItemID = CMP.numCompanyIndustry),'''') as numCompanyIndustry,
	ISNULL((SELECT vcData FROM dbo.ListDetails WHERE numListID = 5 AND (numDomainID = DM.numDomainID OR constFlag = 1) AND numListItemID = CMP.numCompanyType),'''') as numCompanyType,
	ISNULL((SELECT vcData FROM dbo.ListDetails WHERE numListID = 6 AND numDomainID = DM.numDomainID AND numListItemID = CMP.numAnnualRevID),'''') as numAnnualRevID,
	ISNULL((SELECT vcData FROM dbo.ListDetails WHERE numListID = 7 AND numDomainID = DM.numDomainID AND numListItemID = CMP.numNoOfEmployeesID),'''') as numNoOfEmployeesID,
	vcComPhone,
	vcComFax,
	ISNULL((SELECT vcData FROM dbo.ListDetails WHERE numListID = 18 AND numDomainID = DM.numDomainID AND numListItemID = CMP.vcHow),'''') as vcHow,
	ISNULL((SELECT vcData FROM dbo.ListDetails WHERE numListID = 21 AND numDomainID = DM.numDomainID AND numListItemID = CMP.vcProfile),'''') as vcProfile,
	ISNULL((SELECT vcData FROM dbo.ListDetails WHERE numListID = 438 AND numDomainID = DM.numDomainID AND numListItemID = DM.numCompanyDiff),'''') as numCompanyDiff,
	DM.vcCompanyDiff,
	isnull(CMP.txtComments,'''') as txtComments, 
	isnull(CMP.vcWebSite,'''') vcWebSite, 
	isnull(AD1.vcStreet,'''') as vcBillStreet, 
	isnull(AD1.vcCity,'''') as vcBillCity, 
	isnull(dbo.fn_GetState(AD1.numState),'''') as numBillState,
	isnull(AD1.vcPostalCode,'''') as vcBillPostCode,
	isnull(dbo.fn_GetListName(AD1.numCountry,0),'''') as numBillCountry,
	isnull(AD2.vcStreet,'''') as vcShipStreet,
	isnull(AD2.vcCity,'''') as vcShipCity,
	isnull(dbo.fn_GetState(AD2.numState),'''')  as numShipState,
	isnull(AD2.vcPostalCode,'''') as vcShipPostCode, 
	isnull(dbo.fn_GetListName(AD2.numCountry,0),'''') as numShipCountry,
	ISNULL((SELECT vcData FROM dbo.ListDetails WHERE numListID = 78 AND numDomainID = DM.numDomainID AND numListItemID = DM.numTerID),'''') as numTerID
FROM  
(
SELECT
	CMP.numCompanyId
FROM
	dbo.CompanyInfo CMP
INNER JOIN
	dbo.DivisionMaster d
ON
	CMP.numCompanyId = d.numCompanyID
INNER JOIN
	AdditionalContactsInformation AC
ON 
	AC.numDivisionID=d.numDivisionID AND ISNULL(AC.bitPrimaryContact,0)=1
WHERE 
	CMP.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' AND d.tintCRMType<>0 and d.tintCRMType=1' +
	CASE @ProspectsRights WHEN 0 THEN ' and d.numRecOwner=0 ' ELSE '' END +
    CASE @ProspectsRights WHEN 1 THEN ' and d.numRecOwner='+convert(varchar(15),@numUserCntID) ELSE '' END +                        
    CASE @ProspectsRights WHEN 2 THEN ' and (d.numTerID in (select numTerritoryID  from UserTerritory where numUserCntID='+convert(varchar(15),@numUserCntID)+') or d.numTerID=0)' ELSE '' END +
'
UNION
SELECT
	CMP.numCompanyId
FROM
	dbo.CompanyInfo CMP
INNER JOIN
	dbo.DivisionMaster d
ON
	CMP.numCompanyId = d.numCompanyID
INNER JOIN
	AdditionalContactsInformation AC
ON 
	AC.numDivisionID=d.numDivisionID AND ISNULL(AC.bitPrimaryContact,0)=1
WHERE 
	CMP.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' AND d.tintCRMType<>0 and d.tintCRMType=2' +
	CASE @accountsRights WHEN 0 THEN ' and d.numRecOwner=0 ' ELSE '' END +
    CASE @accountsRights WHEN 1 THEN ' and d.numRecOwner='+convert(varchar(15),@numUserCntID) ELSE '' END +                        
    CASE @accountsRights WHEN 2 THEN ' and (d.numTerID in (select numTerritoryID  from UserTerritory where numUserCntID='+convert(varchar(15),@numUserCntID)+') or d.numTerID=0)' ELSE '' END +
'
UNION
SELECT
	CMP.numCompanyId
FROM
	dbo.CompanyInfo CMP
INNER JOIN
	dbo.DivisionMaster d
ON
	CMP.numCompanyId = d.numCompanyID
INNER JOIN
	AdditionalContactsInformation AC
ON 
	AC.numDivisionID=d.numDivisionID AND ISNULL(AC.bitPrimaryContact,0)=1
WHERE 
	CMP.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' AND d.tintCRMType=0' +
	CASE @leadsRights WHEN 0 THEN ' and d.numRecOwner=0 ' ELSE '' END +
    CASE @leadsRights WHEN 1 THEN ' and d.numRecOwner='+convert(varchar(15),@numUserCntID) ELSE '' END +                        
    CASE @leadsRights WHEN 2 THEN ' and (d.numTerID in (select numTerritoryID  from UserTerritory where numUserCntID='+convert(varchar(15),@numUserCntID)+') or d.numTerID=0)' ELSE '' END +
') AS TEMP2
INNER JOIN
	CompanyInfo CMP
ON
	TEMP2.numCompanyId =  CMP.numCompanyId                                                 
INNER JOIN 
	DivisionMaster DM    
ON 
	DM.numCompanyID=CMP.numCompanyID
LEFT JOIN 
	dbo.AddressDetails AD1 
ON 
    AD1.numAddressID =  (
							SELECT 
								MAX(numAddressID) 
							FROM 
								AddressDetails 
							WHERE 
								AddressDetails.numDomainID = DM.numDomainID AND 
								AddressDetails.numRecordID = DM.numDivisionID AND 
								AddressDetails.tintAddressOf = 2 AND 
								AddressDetails.tintAddressType = 1 AND 
								AddressDetails.bitIsPrimary = 1
						  )
LEFT JOIN 
	AddressDetails AD2 
ON 
    AD2.numAddressID = (
							SELECT 
								MAX(numAddressID) 
							FROM 
								AddressDetails 
							WHERE 
								AddressDetails.numDomainID = DM.numDomainID AND 
								AddressDetails.numRecordID = DM.numDivisionID AND 
								AddressDetails.tintAddressOf = 2 AND 
								AddressDetails.tintAddressType = 2 AND 
								AddressDetails.bitIsPrimary = 1
						)) AS TEMP1'
	
-- END: Generate default select statement 

                  
-- START: Generate user defined search fields condition

DECLARE @searchWhereCondition varchar(MAX)
DECLARE @singleValueSearchFields AS varchar(MAX)   
DECLARE @multiValueSearchFields AS varchar(MAX) 
DECLARE @customSearchFields AS varchar(MAX)    


SET @searchWhereCondition = ''
SET @singleValueSearchFields = ''   
SET @multiValueSearchFields = ''
SET @customSearchFields = ''
                

SELECT * INTO #tempOrgSearch FROM
(
	SELECT 
		numFieldId,
		vcDbColumnName,
		ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName,
		tintRow AS tintOrder,
		0 as Custom
    FROM 
		View_DynamicColumns
	WHERE 
		numFormId=97 AND numUserCntID=@numUserCntID AND numDomainID=@numDomainID AND 
		tintPageType=1 AND bitCustom=0  AND ISNULL(bitSettingField,0)=1 AND numRelCntType=0
	UNION   
	SELECT 
		numFieldId,
		vcFieldName,
		vcFieldName,
		tintRow AS tintOrder,
		1 as Custom
    FROM 
		View_DynamicCustomColumns   
	WHERE 
		Grp_id=1 AND numFormId=97 AND numUserCntID=@numUserCntID AND numDomainID=@numDomainID AND 
		tintPageType=1 AND bitCustom=1 AND numRelCntType=0
)Y

IF @searchText != ''
BEGIN
	IF (SELECT COUNT(*) FROM #tempOrgSearch) > 0 
	BEGIN
		-- START: Remove white spaces from search values
		DECLARE @finalSearchText VARCHAR(MAX)
		DECLARE @TempTable TABLE (vcValue VARCHAR(MAX))

		WHILE LEN(@searchText) > 0
		BEGIN
			SET @finalSearchText = LEFT(@searchText, 
									ISNULL(NULLIF(CHARINDEX(',', @searchText) - 1, -1),
									LEN(@searchText)))
			SET @searchText = SUBSTRING(@searchText,
										 ISNULL(NULLIF(CHARINDEX(',', @searchText), 0),
										 LEN(@searchText)) + 1, LEN(@searchText))

			INSERT INTO @TempTable (vcValue) VALUES ( @finalSearchText )
		END
		
		--Removes white spaces
		UPDATE
			@TempTable
		SET
			vcValue = LTRIM(RTRIM(vcValue))
	
		--Converting table to comma seperated string
		SELECT @searchText = COALESCE(@searchText,'') +  CONVERT(VARCHAR(MAX),vcValue) + ',' FROM @TempTable
	
		--Remove last comma from final search string
		IF DATALENGTH(@searchText) > 0
			SET @searchText = LEFT(@searchText, LEN(@searchText) - 1)
	
		-- END: Remove white spaces from search values
	
		--START: Gets search fields where there is one-one relation 
		SELECT 
			@singleValueSearchFields = COALESCE(@singleValueSearchFields,'') + '(' + CAST (vcDbColumnName AS VARCHAR (50)) + ' LIKE '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END + REPLACE(@searchText, ',', '%'' OR ' + vcDbColumnName + ' LIKE  ' + CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END) + '%'')' + ' OR '	
		FROM 
			#tempOrgSearch 
		WHERE 
			numFieldId NOT IN (51,52,96,148,127) AND
			Custom = 0
			
		
		-- Removes last OR from string
		IF DATALENGTH(@singleValueSearchFields) > 0
			SET @singleValueSearchFields = LEFT(@singleValueSearchFields, LEN(@singleValueSearchFields) - 3)
			
		--END: Gets search fields where there is one-one relation 
		
		
		--START: Gets search fields where there is one-many relation 
		-- 1. CONTACT FIRST NAME
		IF (SELECT COUNT(*) FROM #tempOrgSearch WHERE numFieldId = 51) > 0
			SET @multiValueSearchFields = '(SELECT COUNT(*) FROM AdditionalContactsInformation ACI WHERE ACI.numDivisionID=TEMP1.numDivisionID AND (ACI.vcFirstName LIKE '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END +  REPLACE(@searchText, ',', '%'' OR ACI.vcFirstName LIKE  '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END) + '%'')) > 0'	
		
		-- 2. CONTACT LAST NAME
		IF (SELECT COUNT(*) FROM #tempOrgSearch WHERE numFieldId = 52) > 0
		BEGIN
			IF @multiValueSearchFields = ''
				SET @multiValueSearchFields = '(SELECT COUNT(*) FROM AdditionalContactsInformation ACI WHERE ACI.numDivisionID=TEMP1.numDivisionID AND (ACI.vcLastName LIKE '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END +  REPLACE(@searchText, ',', '%'' OR ACI.vcLastName LIKE  '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END) + '%'')) > 0'
			ELSE	
				SET @multiValueSearchFields = @multiValueSearchFields + ' OR (SELECT COUNT(*) FROM AdditionalContactsInformation ACI WHERE ACI.numDivisionID=TEMP1.numDivisionID AND (ACI.vcLastName LIKE '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END +  REPLACE(@searchText, ',', '%'' OR ACI.vcLastName LIKE  '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END) + '%'')) > 0'
		END	
		
		-- 3. ORDER NAME
		IF (SELECT COUNT(*) FROM #tempOrgSearch WHERE numFieldId = 96) > 0
		BEGIN
			IF @multiValueSearchFields = ''
				SET @multiValueSearchFields = '(SELECT COUNT(*) FROM OpportunityMaster OM WHERE OM.numDivisionId = TEMP1.numDivisionID AND (OM.vcPOppName LIKE '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END +  REPLACE(@searchText, ',', '%'' OR OM.vcPOppName LIKE  '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END) + '%'')) > 0'
			ELSE	
				SET @multiValueSearchFields = @multiValueSearchFields + ' OR (SELECT COUNT(*) FROM OpportunityMaster OM WHERE OM.numDivisionId = TEMP1.numDivisionID AND (OM.vcPOppName LIKE '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END +  REPLACE(@searchText, ',', '%'' OR OM.vcPOppName LIKE  '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END) + '%'')) > 0'
		END	
		
		-- 4.PROJECT NAME
		IF (SELECT COUNT(*) FROM #tempOrgSearch WHERE numFieldId = 148) > 0
		BEGIN
			IF @multiValueSearchFields = ''
				SET @multiValueSearchFields = '(SELECT COUNT(*) FROM dbo.ProjectsMaster PM WHERE PM.numDivisionId = TEMP1.numDivisionID AND (PM.vcProjectName LIKE '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END +  REPLACE(@searchText, ',', '%'' OR PM.vcProjectName LIKE  '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END) + '%'')) > 0'
			ELSE	
				SET @multiValueSearchFields = @multiValueSearchFields + ' OR (SELECT COUNT(*) FROM dbo.ProjectsMaster PM WHERE PM.numDivisionId = TEMP1.numDivisionID AND (PM.vcProjectName LIKE '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END +  REPLACE(@searchText, ',', '%'' OR PM.vcProjectName LIKE  '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END) + '%'')) > 0'
		END	
		
		-- 5. CASE NAME
		IF (SELECT COUNT(*) FROM #tempOrgSearch WHERE numFieldId = 127) > 0
		BEGIN		
			IF @multiValueSearchFields = ''
				SET @multiValueSearchFields = '(SELECT COUNT(*) FROM dbo.Cases C WHERE C.numDivisionId = TEMP1.numDivisionID AND (C.textSubject LIKE '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END +  REPLACE(@searchText, ',', '%'' OR C.textSubject LIKE  '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END) + '%'')) > 0'
			ELSE	
				SET @multiValueSearchFields = @multiValueSearchFields + ' OR (SELECT COUNT(*) FROM dbo.Cases C WHERE C.numDivisionId = TEMP1.numDivisionID AND (C.textSubject LIKE '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END +  REPLACE(@searchText, ',', '%'' OR C.textSubject LIKE  '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END) + '%'')) > 0'
		END	
		
		--END: Gets search fields where there is one-many relation 
		
		--START: Get custom search fields
		
		IF EXISTS (SELECT * FROM #tempOrgSearch WHERE Custom = 1)
		BEGIN
			SELECT 
				@customSearchFields = COALESCE (@customSearchFields,'') + '( dbo.fn_GetCustFldOrganization(' + CAST(numFieldId AS VARCHAR(20)) + ',1,TEMP1.numCompanyID) LIKE '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END + REPLACE(@searchText, ',', '%'' OR dbo.fn_GetCustFldOrganization(' + CAST(numFieldId AS VARCHAR(20)) + ',1,TEMP1.numCompanyID) LIKE  ' + CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END) + '%'')' + ' OR '	
			FROM 
				#tempOrgSearch 
			WHERE 
				Custom = 1
		END
		
		-- Removes last OR from string
		IF DATALENGTH(@customSearchFields) > 0
			SET @customSearchFields = LEFT(@customSearchFields, LEN(@customSearchFields) - 3)
		
		--END: Get custom search fields
		
		IF DATALENGTH(@singleValueSearchFields) > 0
			SET @searchWhereCondition = @singleValueSearchFields
			
		IF DATALENGTH(@multiValueSearchFields) > 0
			IF DATALENGTH(@searchWhereCondition) > 0
				SET @searchWhereCondition = @searchWhereCondition + ' OR ' + @multiValueSearchFields
			ELSE
				SET @searchWhereCondition = @multiValueSearchFields
				
		IF DATALENGTH(@customSearchFields) > 0
			IF 	DATALENGTH(@searchWhereCondition) = 0
				SET @searchWhereCondition = @customSearchFields
			ELSE
				SET @searchWhereCondition = @searchWhereCondition + ' OR ' + @customSearchFields
			
	END
	ELSE
	BEGIN
		SET @searchWhereCondition = '(vcCompanyName LIKE '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END +  REPLACE(@searchText, ',', '%'' OR vcCompanyName LIKE  '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END) + '%'')'
	END
END

-- END: Generate user defined search fields condition

IF DATALENGTH(@searchWhereCondition) > 0
		SET @strSQL = @strSQL + ' WHERE ' + @searchWhereCondition 
    
 
SET @strSQL = @strSQL + ' ORDER BY vcCompanyname'    

--SELECT @strSQL
EXEC (@strSQL)

END
GO
/****** Object:  StoredProcedure [dbo].[USP_EditCntInfo]    Script Date: 07/26/2008 16:15:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Anoop Jayaraj                                 
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_editcntinfo')
DROP PROCEDURE usp_editcntinfo
GO
CREATE PROCEDURE [dbo].[USP_EditCntInfo]                                                
@numContactID NUMERIC(9) ,    
@numDomainID as numeric(9)  
 ,  
@ClientTimeZoneOffset as int                     
                                              
AS                                                
BEGIN                                                
                                                
  SELECT                        
   A.numContactId, A.vcGivenName, A.vcFirstName,                                                 
                      A.vcLastName, D.numDivisionID, C.numCompanyId,                                                 
                      C.vcCompanyName, D.vcDivisionName, D.numDomainID,  
					  ISNULL((select vcdata from listdetails where numListItemID = C.numCompanyType),'') as vcCompanyTypeName,                                               
                      A.numPhone, A.numPhoneExtension, A.vcEmail,  A.numTeam,                                               
                      A.vcFax, A.numContactType,  A.charSex, A.bintDOB,                                                 
                      A.vcPosition, A.txtNotes, A.numCreatedBy,                                                
                      A.numCell,A.NumHomePhone,A.vcAsstFirstName,                                                
                      A.vcAsstLastName,A.numAsstPhone,A.numAsstExtn,                                                
                      A.vcAsstEmail,A.charSex, A.vcDepartment,                                                 
                      AD.vcStreet AS vcpStreet,                                            
                      AD.vcCity AS vcPCity, 
                      dbo.fn_GetState(AD.numState) as State, dbo.fn_GetListName(AD.numCountry,0) as Country,
                      AD.vcPostalCode AS vcPPostalCode,A.bitOptOut,vcDepartment,                                                
        A.numManagerID, A.vcCategory  , dbo.fn_GetContactName(A.numCreatedBy)+' '+ convert(varchar(20),dateadd(minute,-@ClientTimeZoneOffset,A.bintCreatedDate)) CreatedBy,                                            
          dbo.fn_GetContactName(A.numModifiedBy)+' '+ convert(varchar(20),dateadd(minute,-@ClientTimeZoneOffset,A.bintModifiedDate)) ModifiedBy,vcTitle,vcAltEmail,                                            
  dbo.fn_GetContactName(A.numRecOwner) as RecordOwner,                                      
  C.vcCompanyName,C.vcWebSite, 
--  dbo.fn_GetState(AddC.vcPState) as State, dbo.fn_GetListName(AddC.vcPCountry,0) as Country,
   A.numEmpStatus,                          
  isnull((select top 1 case when bitEngaged=0 then 'Disengaged' when bitEngaged=1 then 'Engaged' end as ECampStatus  from ConECampaign                          
where  numContactID=@numContactID order by numConEmailCampID desc),'-') as CampStatus ,                        
dbo.GetECamLastAct(@numContactID) as Activity ,       
(select  count(*) from dbo.GenericDocuments   where numRecID=@numContactID and  vcDocumentSection='C') as DocumentCount,
(SELECT count(*)from CompanyAssociations where numDivisionID=@numContactID and bitDeleted=0 ) as AssociateCountFrom,              
(SELECT count(*)from CompanyAssociations where numAssociateFromDivisionID=@numContactID and bitDeleted=0 ) as AssociateCountTo    ,        
vcItemId ,        
vcChangeKey,              
vcCompanyName,      
tintCRMType ,isnull((select top 1 numUserId from  UserMaster where numUserDetailId=A.numContactId),0) as numUserID,
dbo.fn_GetListName(C.numNoOfEmployeesId,0) AS NoofEmp,ISNULL(vcImageName,'') vcImageName
FROM         AdditionalContactsInformation A     
INNER JOIN  DivisionMaster D ON A.numDivisionId = D.numDivisionID     
INNER JOIN  CompanyInfo C ON D.numCompanyID = C.numCompanyId                      
LEFT JOIN AddressDetails AD ON  A.numDomainID = AD.numDomainID AND AD.numRecordID= A.numContactId AND AD.tintAddressOf=1 AND AD.tintAddressType=0 AND AD.bitIsPrimary=1
WHERE     A.numContactId = @numContactID and A.numDomainID=@numDomainID                  
                                   
END
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetAddressDetails' ) 
    DROP PROCEDURE USP_GetAddressDetails
GO
CREATE PROCEDURE USP_GetAddressDetails
    @tintMode TINYINT,
    @numDomainID NUMERIC,
    @numRecordID NUMERIC,
    @numAddressID NUMERIC,
    @tintAddressType TINYINT,
    @tintAddressOf TINYINT
AS 
BEGIN

    IF @tintMode = 1 
        BEGIN
            SELECT  numAddressID,
                    vcAddressName,
                    vcStreet,
                    vcCity,
                    vcPostalCode,
                    numState,
                    numCountry,
                    bitIsPrimary,
                    tintAddressOf,
                    tintAddressType,
                    '<pre>' + isnull(vcStreet,'') + '</pre> ' + isnull(vcCity,'') + ', '
                            + isnull(dbo.fn_GetState(numState),'') + ' '
                            + isnull(vcPostalCode,'') + ' <br>'
                            + isnull(dbo.fn_GetListItemName(numCountry),'') vcFullAddress
            FROM    dbo.AddressDetails
            WHERE   numDomainID = @numDomainID
                    AND numAddressID = @numAddressID
            
        END
    IF @tintMode = 2
        BEGIN
            SELECT  numAddressID,
                    vcAddressName,
					'<pre>' + isnull(vcStreet,'') + '</pre> ' + isnull(vcCity,'') + ', '
                            + isnull(dbo.fn_GetState(numState),'') + ' '
                            + isnull(vcPostalCode,'') + ' <br>'
                            + isnull(dbo.fn_GetListItemName(numCountry),'') vcFullAddress
            FROM    dbo.AddressDetails
            WHERE   numDomainID = @numDomainID
                    AND numRecordID = @numRecordID	
                    AND tintAddressOf = @tintAddressOf
                    AND tintAddressType = @tintAddressType
            ORDER BY bitIsPrimary desc
        END
    IF @tintMode = 3
        BEGIN
            SELECT  numAddressID,
                    vcAddressName,
                    vcStreet,
                    vcCity,
                    vcPostalCode,
                    numState,
                    numCountry,
                    bitIsPrimary,
                    tintAddressOf,
                    tintAddressType,
                    '<pre>' + isnull(vcStreet,'') + '</pre> ' + isnull(vcCity,'') + ', '
                            + isnull(dbo.fn_GetState(numState),'') + ' '
                            + isnull(vcPostalCode,'') + ' <br>'
                            + isnull(dbo.fn_GetListItemName(numCountry),'') vcFullAddress
            FROM    dbo.AddressDetails
            WHERE   numDomainID = @numDomainID
                    AND numRecordID = @numRecordID	
                    AND tintAddressOf = @tintAddressOf
                    AND tintAddressType = @tintAddressType
                    AND bitIsPrimary = 1
            ORDER BY bitIsPrimary desc
        END
    
        

            
END
/****** Object:  UserDefinedFunction [dbo].[USP_GetAttributes]    Script Date: 07/26/2008 18:13:15 ******/

GO

GO
--- Created By Anoop Jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='usp_getattributes')
DROP FUNCTION usp_getattributes
GO
CREATE FUNCTION [dbo].[USP_GetAttributes] (@numRecID numeric,@bitSerialized  bit)
returns varchar(100)
as
begin
declare @strAttribute varchar(100)
declare @numCusFldID numeric(9)
declare @numCusFldValue varchar(100)
set @numCusFldID=0
set @strAttribute=''
select top 1 @numCusFldID=fld_id,@numCusFldValue=REPLACE(fld_value,'$','') from CFW_Fld_Values_Serialized_Items where RecId=@numRecID and fld_value!='0' and fld_value!='' and bitSerialized=@bitSerialized order by fld_id
while @numCusFldID>0
begin
	if isnumeric(@numCusFldValue)=1
	begin
		--set @strAttribute=@strAttribute+@numCusFldValue
		select @strAttribute=@strAttribute+Fld_label from  CFW_Fld_Master where Fld_id=@numCusFldID
		select @strAttribute=@strAttribute+'  - '+ vcData +',' from ListDetails where numListItemID=@numCusFldValue
	end
	else
	begin
		select @strAttribute=@strAttribute+Fld_label from  CFW_Fld_Master where Fld_id=@numCusFldID
		select @strAttribute=@strAttribute+'  - -,' 
	end
	select top 1 @numCusFldID=fld_id,@numCusFldValue=fld_value from CFW_Fld_Values_Serialized_Items where RecId=@numRecID and fld_value!='0' and fld_value!='' and bitSerialized=@bitSerialized and fld_id>@numCusFldID order by fld_id
	if @@rowcount=0 set @numCusFldID=0
	
end
return @strAttribute
end
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetBizDocTemplate' ) 
    DROP PROCEDURE USP_GetBizDocTemplate
GO
-- USP_GetBizDocTemplate 72,0,0
-- =============================================  
-- Modified by: <Author,,Sachin Sadhu>  
-- Create date: <Create Date,,24thDec2013>  
-- Description: <Description,,add 3 fields:vcBizDocImagePath,vcBizDocFooter,vcPurBizDocFooter>  
-- =============================================  
CREATE PROCEDURE [dbo].[USP_GetBizDocTemplate]
    @numDomainID NUMERIC ,
    @tintTemplateType TINYINT ,
    @numBizDocTempID NUMERIC
AS 
    SET NOCOUNT ON    
    
    SELECT  [numDomainID] ,
            [numBizDocID] ,
            [txtBizDocTemplate] ,
            [txtCSS] ,
            ISNULL(bitEnabled, 0) AS bitEnabled ,
            [numOppType] ,
            ISNULL(vcTemplateName, '') AS vcTemplateName ,
            ISNULL(bitDefault, 0) AS bitDefault ,
            vcBizDocImagePath ,
            vcBizDocFooter ,
            vcPurBizDocFooter,
			numOrientation,
			bitKeepFooterBottom,
			ISNULL(numRelationship,0) AS numRelationship,
			ISNULL(numProfile,0) AS numProfile
    FROM    BizDocTemplate
    WHERE   [numDomainID] = @numDomainID
            AND tintTemplateType = @tintTemplateType
            AND ( numBizDocTempID = @numBizDocTempID
                  OR ( @numBizDocTempID = 0
                       AND @tintTemplateType != 0
                     )
                ) 
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetBizDocTemplateList')
DROP PROCEDURE USP_GetBizDocTemplateList
GO
CREATE PROCEDURE [dbo].[USP_GetBizDocTemplateList]
	@numDomainID NUMERIC,
	@numBizDocID NUMERIC,
	@numOppType numeric=0,
	@byteMode as tinyint,
	@numRelationship AS NUMERIC(18,0) = 0,
	@numProfile AS NUMERIC(18,0) = 0
AS
SET NOCOUNT ON

IF @byteMode=0
BEGIN

Declare @numAuthoritativePurchase as numeric(9);set @numAuthoritativePurchase=0
Declare @numAuthoritativeSales as numeric(9);set @numAuthoritativeSales=0

select @numAuthoritativePurchase=numAuthoritativePurchase,@numAuthoritativeSales=numAuthoritativeSales from AuthoritativeBizDocs where numDomainId=@numDomainID

SELECT numBizDocTempID,[numDomainID], 
	[numBizDocID],
	dbo.fn_GetListItemName(numBizDocID) +  case  when numOppType=1  then (case when isnull(@numAuthoritativeSales,0)=numBizDocId then ' - Authoritative' else '' end ) 
												 when numOppType=2  then (case when isnull(@numAuthoritativePurchase,0)=numBizDocId then ' - Authoritative' else '' end )  end as vcBizDocType ,
    Case When [numOppType]=1 then 'Sales' else 'Purchase' end as vcOppType,
    isnull(bitEnabled,0) as bitEnabled,
	vcTemplateName,isnull(bitDefault,0) as bitDefault,numOppType,
	dbo.fn_GetListName(numRelationship,0) Relationship,
	dbo.fn_GetListName(numProfile,0) [Profile]
FROM BizDocTemplate
WHERE [numDomainID] = @numDomainID and tintTemplateType=0 and
	(numBizDocID=@numBizDocID or @numBizDocID=0) and (numOppType=@numOppType or @numOppType=0)

END

ELSE IF @byteMode=1
BEGIN
	DECLARE @numBizDocTempID AS NUMERIC(18) = 0

	If ISNULL(@numRelationship,0) > 0 OR ISNULL(@numProfile,0) > 0
	BEGIN
		SELECT
			@numBizDocTempID = numBizDocTempID
		FROM
			BizDocTemplate
		WHERE 
			[numDomainID] = @numDomainID 
			AND tintTemplateType=0 
			AND numBizDocID=@numBizDocID 
			AND numOppType=@numOppType 
			AND ISNULL(bitEnabled,0)=1
			AND numRelationship = @numRelationship
			AND numProfile = @numProfile
	END

	IF ISNULL(@numBizDocTempID,0) = 0
	BEGIN
		SELECT 
			@numBizDocTempID=numBizDocTempID
		FROM 
			BizDocTemplate
		WHERE 
			[numDomainID] = @numDomainID 
			AND tintTemplateType=0 
			AND	numBizDocID=@numBizDocID 
			AND numOppType=@numOppType 
			AND isnull(bitDefault,0)=1 
			AND isnull(bitEnabled,0)=1
	END

	SELECT @numBizDocTempID as numBizDocTempID
END
/****** Object:  StoredProcedure [dbo].[USP_GetCategory]    Script Date: 08/08/2009 16:37:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                        
-- exec [dbo].[USP_GetCategory] 0,0,'',0,1,3
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcategory')
DROP PROCEDURE usp_getcategory
GO
CREATE PROCEDURE [dbo].[USP_GetCategory]                        
@numCatergoryId as numeric(9),                        
@byteMode as tinyint ,                  
@str as varchar(1000),                  
@numDomainID as numeric(9)=0,
@numSiteID AS NUMERIC(9)=0 ,
@numCurrentPage as numeric(9)=0,
@PageSize AS INT=20,
@numTotalPage as numeric(9) OUT,    
@numItemCode AS NUMERIC(9,0),
@SortChar CHAR(1) = '0'
               
as   

set nocount on                     
--selecting all categories                     
declare @strsql as varchar(8000)
DECLARE @strRowCount VARCHAR(8000)  


create table #TempCategory
(PKId numeric(9) identity,
numItemCode numeric(9),
vcItemName varchar(250));

                  
if @byteMode=0                        
BEGIN
	 ;WITH CategoryList
As 
( SELECT C1.numCategoryID,C1.vcCategoryName,C1.numDepCategory,C1.tintLevel,case when C1.bitLink=0 then 'No' when C1.bitLink=1 then 'Yes' end as Link,C1.vcLink as vcLink,CAST('' AS VARCHAR(1000)) as DepCategory,
	C1.intDisplayOrder,C1.vcDescription,
	CASE WHEN LEN(ISNULL(C1.vcDescription ,'')) > 0 THEN  
	CASE WHEN LEN(C1.vcDescription) > 40 THEN SUBSTRING(C1.vcDescription,0,40) + ' [...]' 
         ELSE C1.vcDescription + ' [...]'	END 
    ELSE
		 ISNULL(C1.vcDescription ,'')   
    END     
         AS vcShortDescription,1 AS [Level],
	CAST (REPLICATE('.',1) + C1.vcCategoryName as varchar(1000) )  as vcNewCategoryName,
	CAST(C1.numCategoryID AS VARCHAR(MAX)) AS [Order]
 FROM dbo.Category C1 WHERE C1.numDepCategory=0 AND  C1.numDomainID=@numDomainID 
 UNION ALL
 SELECT  C1.numCategoryID,C1.vcCategoryName,C1.numDepCategory,C1.tintLevel,case when C1.bitLink=0 then 'No' when C1.bitLink=1 then 'Yes' end as Link,C1.vcLink as vcLink,CL.vcCategoryName as DepCategory ,
	C1.intDisplayOrder,C1.vcDescription,
	CASE WHEN LEN(ISNULL(C1.vcDescription ,'')) > 0 THEN  
	CASE WHEN LEN(C1.vcDescription) > 40 THEN SUBSTRING(C1.vcDescription,0,40) + ' [...]' 
         ELSE C1.vcDescription + ' [...]'	END 
    ELSE
		 ISNULL(C1.vcDescription ,'')   
    END     
         AS vcShortDescription,CL.[Level] + 1 AS [Level],
	CAST (REPLICATE('.',[Level] * 3) + C1.vcCategoryName as varchar(1000) )  as vcNewCategoryName,
	[Order] + '.' + CAST(C1.numCategoryID AS VARCHAR(MAX)) AS [Order]
 FROM dbo.Category C1 
 INNER JOIN CategoryList CL
 ON CL.numCategoryID = C1.numDepCategory  WHERE  C1.numDomainID=@numDomainID 
)   

SELECT *,(SELECT COUNT(*) FROM [SiteCategories] SC WHERE SC.[numSiteID] =@numSiteID AND SC.[numCategoryID]= C1.[numCategoryID]) AS ShowInECommerce,
ISNULL((SELECT COUNT(*) FROM [ItemCategory] WHERE [numCategoryID] = C1.[numCategoryID]),0) AS ItemCount FROM CategoryList C1 ORDER BY [Order]
end                                                
--deleting a category                      
else if @byteMode=1                        
begin     
	delete from SiteCategories where numCategoryID=@numCatergoryId                     
	delete from ItemCategory where numCategoryID=@numCatergoryId
	delete from Category where numCategoryID=@numCatergoryId       
	select 1                        
end                        
--selecting all catories where level is 1                       
else if @byteMode=2                      
begin                        
	select numCategoryID,vcCategoryName,intDisplayOrder,vcDescription from Category                      
	where ((tintlevel=1 )  or (tintlevel<>1 and numDepCategory=@numCatergoryId))  and numCategoryID!=@numCatergoryId                      
	and numDomainID=@numDomainID                    
	ORDER BY [vcCategoryName]
end                      
--selecting subCategories                      
else if @byteMode=3                      
begin                        
	select numCategoryID,vcCategoryName,intDisplayOrder,vcDescription  from Category                      
	where numDepCategory=@numCatergoryId                      
	ORDER BY [vcCategoryName]
end                      
                      
--selecting Items                      
if @byteMode=4                      
begin                        
	select numItemCode,vcItemName  from Item           
	where numDomainID=@numDomainID                       
end                      
                      
--selecting Item belongs to a category                      
else if @byteMode=5                      
begin    


	insert into #TempCategory
	                       
	select numItemCode,CONCAT(numItemCode,' - ',vcItemName) AS vcItemName  from Item                      
	join ItemCategory                       
	on numItemCode=numItemID                      
	where numCategoryID=@numCatergoryId           
	and numDomainID=@numDomainID                       
	ORDER BY [vcItemName]


	select @numTotalPage =COUNT(*) from #TempCategory

	if @numTotalPage<@PageSize 
		begin
			set @numTotalPage=1
		end
	else	
		begin
			set @numTotalPage=@numTotalPage/@PageSize
		end

	select * from #TempCategory  where PKId between (@numCurrentPage*@PageSize)- (@PageSize-1) and  @numCurrentPage*@PageSize

	select @numTotalPage

end                      
                    
else if @byteMode=6                    
begin                        

	select C1.numCategoryID,c1.vcCategoryName,((select Count(*) from Category C2 where C2.numDepCategory=C1.numCategoryID)+(select count(*) from ItemCategory C3 where C3.numCategoryID=C1.numCategoryID  ) ) as [Count],convert(integer,bitLink) as Type,vcLink,C1.intDisplayOrder,
	C1.vcDescription   
	from Category C1                     
	where tintlevel=1                     
	and numDomainID=@numDomainID                      
                    
end                     
                    
else if @byteMode=7                    
begin                        

	select C1.numCategoryID,c1.vcCategoryName,((select Count(*) from Category C2 where C2.numDepCategory=C1.numCategoryID)+(select count(*) from ItemCategory C3 where C3.numCategoryID=C1.numCategoryID ) ) as [Count],convert(integer,bitLink) as Type,vcLink,C1.intDisplayOrder,
	C1.vcDescription,numDepCategory as Category
	from Category C1                     
	where C1.numDepCategory=@numCatergoryId and numDomainID=@numDomainID                      
	union                     
	select numItemCode,vcItemName,0 ,2,'',0,'',0  from Item                      
	join ItemCategory                       
	on numItemCode=numItemID                      
	where numCategoryID=@numCatergoryId  and numDomainID=@numDomainID                    
end                     
                
else if @byteMode=8                     
begin                    
     --kishan               
	set @strsql='select numItemCode,vcItemName,txtItemDesc,0,
	(
	SELECT TOP 1 II.vcPathForTImage 
    FROM dbo.ItemImages II 
        WHERE II.numItemCode = Item.numItemCode 
    AND II.bitDefault = 1 , AND II.bitIsImage = 1 
    AND II.numDomainID ='+ cast(@numDomainId as varchar(50))+') 
	vcPathForTImage ,
	1 as Type,monListPrice as price,0 as numQtyOnHand  from Item                    
	join ItemCategory                       
	on numItemCode=numItemID                      
	where numCategoryID in('+@str+')'                  
	exec (@strsql)                   
end                    
                  
else if @byteMode=9                  
begin                    
                    
	select distinct(numItemCode),vcItemName,txtItemDesc,0 , (SELECT TOP 1 vcPathForImage FROM ItemImages WHERE numDomainId = @numDomainId AND bitDefault = 1 AND numItemCode = Item.numItemCode) AS vcPathForImage,1 as Type,monListPrice as price,0 as numQtyOnHand from Item                      
	join ItemCategory                       
	on numItemCode=numItemID                      
	where ( vcItemName like '%'+@str+'%' or txtItemDesc like '%'+@str+'%') and numDomainID= @numDomainID                                    
end        
      
else if @byteMode=10      
begin       
	select ROW_NUMBER() OVER (ORDER BY Category Asc) AS RowNumber,* from (select   C1.numCategoryID,c1.vcCategoryName,convert(integer,bitLink) as Type,vcLink ,numDepCategory as Category,C1.intDisplayOrder,
	C1.vcDescription         
	from Category C1                     
	where  numDomainID=@numDomainID)X       
      
end 


--selecting Items not present in Category                   
else if @byteMode=11                    
begin     

	insert into #TempCategory
	         
	SELECT I.numItemCode, CONCAT(I.numItemCode,' - ',I.vcItemName) AS vcItemName FROM item  I LEFT OUTER JOIN [ItemCategory] IC ON IC.numItemID = I.numItemCode
	WHERE [numDomainID] =@numDomainID AND ISNULL(IC.numCategoryID,0) <> @numCatergoryId
	ORDER BY [vcItemName]
          
--	select numItemCode,vcItemName  from Item LEFT OUTER JOIN [ItemCategory] ON Item.[numItemCode] = [ItemCategory].[numCategoryID]
--	where [ItemCategory].[numCategoryID] <> @numCatergoryId
----numItemCode not in (select numItemID from ItemCategory where numCategoryID=@numCatergoryId)
--	and numDomainID=@numDomainID                      
--	ORDER BY [vcItemName]



	select @numTotalPage =COUNT(*) from #TempCategory

	set @numTotalPage=@numTotalPage/@PageSize

	select * from #TempCategory  where PKId between (@numCurrentPage*@PageSize)-(@PageSize-1) and  @numCurrentPage*@PageSize

	select @numTotalPage


end   


--selecting Items From Item Groups                  
else if @byteMode=12                
begin   

	insert into #TempCategory
	                     
	select numItemCode,vcItemName  from Item 
	where numItemGroup= @numCatergoryId  
	ORDER BY [vcItemName] 



	select @numTotalPage =COUNT(*) from #TempCategory

	set @numTotalPage=@numTotalPage/@PageSize

	select * from #TempCategory  where PKId between (@numCurrentPage*@PageSize)-(@PageSize-1) and  @numCurrentPage*@PageSize

	select @numTotalPage

              
end

-- Selecting Categories Site Wise and Sorted into Sort Order 
else if @byteMode=13
BEGIN

DECLARE @bParentCatrgory BIT;

SELECT @bParentCatrgory=vcAttributeValue FROM PageElementDetail WHERE [numSiteID] = @numSiteID AND numAttributeID=39
SET @bParentCatrgory=ISNULL(@bParentCatrgory,0)

DECLARE @tintDisplayCategory AS TINYINT
SELECT @tintDisplayCategory = ISNULL(bitDisplayCategory,0) FROM dbo.eCommerceDTL WHERE numSiteID = @numSiteId
PRINT @tintDisplayCategory 
--IF @tintDisplayCategory > 0
--	BEGIN
--	SET @strSQL = @strSQL + ' AND ISNULL((SELECT SUM(numOnHand) FROM WareHouseItems W WHERE W.numItemID = I.numItemCode and numWareHouseID= '+ convert(varchar(15),@numWareHouseID) + '),0) > 0 '	                   	
--	END
	PRINT @bParentCatrgory
	
	IF ISNULL(@tintDisplayCategory,0) = 0 
	BEGIN
		SELECT C.numCategoryID,
			   C.vcCategoryName,
			   C.tintLevel,
			   convert(integer,C.bitLink) as TYPE,
			   C.vcLink AS vcLink,
			   numDepCategory as Category,C.intDisplayOrder,C.vcDescription 
		FROM   [SiteCategories] SC
			   INNER JOIN [Category] C ON SC.[numCategoryID] = C.[numCategoryID]
		WHERE  SC.[numSiteID] = @numSiteID
			   AND SC.[numCategoryID] = C.[numCategoryID] 
			   AND 1 = ( CASE WHEN @bParentCatrgory = 1 THEN tintLevel ELSE 1 END )
			   ORDER BY tintLevel , 
						ISNULL(C.intDisplayOrder,0),
						Category	
	END	
	ELSE
	BEGIN
		SELECT C.numCategoryID,
			   C.vcCategoryName,
			   C.tintLevel,
			   convert(integer,C.bitLink) as TYPE,
			   C.vcLink AS vcLink,
			   numDepCategory as Category,C.intDisplayOrder,C.vcDescription 
		FROM   [SiteCategories] SC
			   INNER JOIN [Category] C ON SC.[numCategoryID] = C.[numCategoryID]
		WHERE  SC.[numSiteID] = @numSiteID
			   AND SC.[numCategoryID] = C.[numCategoryID] 
			   AND 1 = ( CASE WHEN @bParentCatrgory = 1 THEN tintLevel ELSE 1 END )
			   AND (
					SC.numCategoryID IN (
										SELECT numCategoryID FROM dbo.ItemCategory 
										WHERE numItemID IN (
															SELECT numItemID FROM WareHouseItems WI
															INNER JOIN dbo.Item I ON I.numItemCode = WI.numItemID
															GROUP BY numItemID 
															HAVING SUM(WI.numOnHand) > 0
															UNION ALL
															SELECT numItemID FROM WareHouseItems WI
															INNER JOIN dbo.Item I ON I.numItemCode = WI.numItemID
															GROUP BY numItemID 
															HAVING SUM(WI.numAllocation) > 0
														   )
									   )
					OR tintLevel = 1
					)				   
			   ORDER BY tintLevel , 
						ISNULL(C.intDisplayOrder,0),
						Category
	END			

END

else if @byteMode=14
BEGIN

	SELECT C.numCategoryID,
		   C.vcCategoryName,
		   C.tintLevel,
		   convert(integer,C.bitLink) as TYPE,
		   C.vcLink AS vcLink,
		   numDepCategory as Category,C.intDisplayOrder,C.vcPathForCategoryImage,C.vcDescription 
	FROM   [SiteCategories] SC
		   INNER JOIN [Category] C
			 ON SC.[numCategoryID] = C.[numCategoryID]
	WHERE  SC.[numSiteID] = @numSiteID
		   AND SC.[numCategoryID] = C.[numCategoryID]
		 ORDER BY ISNULL(C.intDisplayOrder,0),Category
END

else if @byteMode=15
BEGIN

DECLARE @bSubCatrgory BIT;

SELECT @bSubCatrgory=vcAttributeValue FROM PageElementDetail WHERE [numSiteID] = @numSiteID AND numAttributeID=40
SET @bSubCatrgory=ISNULL(@bSubCatrgory,0)

	SELECT C.numCategoryID,
		   C.vcCategoryName,
		   C.tintLevel,
		   convert(integer,C.bitLink) as TYPE,
		   C.vcLink AS vcLink,
		   numDepCategory as Category,C.intDisplayOrder,C.vcPathForCategoryImage,C.vcDescription 
		   
		   
	FROM   [SiteCategories] SC
		   INNER JOIN [Category] C
			 ON SC.[numCategoryID] = C.[numCategoryID]
	WHERE  SC.[numSiteID] = @numSiteID
		   AND SC.[numCategoryID] = C.[numCategoryID] AND numDepCategory=@numCatergoryId
		   AND 1=(CASE WHEN @bSubCatrgory=1 THEN 1 ELSE 0 END)
		    ORDER BY ISNULL(C.intDisplayOrder,0),Category
END

ELSE if @byteMode=16
BEGIN
 SELECT vcPathForCategoryImage FROM Category WHERE numCategoryID=@numCatergoryId AND numDomainID=@numDomainID
 
END
ELSE if @byteMode=17
BEGIN
 SELECT numCategoryID ,vcCategoryName ,vcDescription,intDisplayOrder,vcPathForCategoryImage,numDepCategory FROM Category  WHERE numCategoryID = @numCatergoryId
END
ELSE IF @byteMode = 18
BEGIN
	     --used for hierarchical category list
	     SELECT 
	C1.numCategoryID,C1.vcCategoryName,NULLIF(numDepCategory,0) numDepCategory,ISNULL(numDepCategory,0) numDepCategory1,0 AS tintLevel,
	ISNULL((SELECT COUNT(*) FROM [ItemCategory] WHERE [numCategoryID] = C1.[numCategoryID]),0) AS ItemCount,
	ISNULL((SELECT COUNT(*) FROM [ItemCategory] WHERE [numCategoryID] in (SELECT numCategoryID FROM category WHERE numDepCategory = C1.[numCategoryID])),0) AS ItemSubcategoryCount
	from Category C1 WHERE numDomainID =@numDomainID
--	AND C1.numCategoryID <> ISNULL(@numCatergoryId,0)
--	ORDER BY ISNULL(numDepCategory,0)
   

END

ELSE IF @byteMode=19
BEGIN
	BEGIN
	 ;WITH CategoryList
As 
( SELECT C1.numCategoryID,C1.vcCategoryName,C1.numDepCategory,C1.tintLevel,case when C1.bitLink=0 then 'No' when C1.bitLink=1 then 'Yes' end as Link,C1.vcLink as vcLink,CAST('' AS VARCHAR(1000)) as DepCategory,
	C1.intDisplayOrder,C1.vcDescription,
	CASE WHEN LEN(ISNULL(C1.vcDescription ,'')) > 0 THEN  
	CASE WHEN LEN(C1.vcDescription) > 40 THEN SUBSTRING(C1.vcDescription,0,40) + ' [...]' 
         ELSE C1.vcDescription + ' [...]'	END 
    ELSE
		 ISNULL(C1.vcDescription ,'')   
    END     
         AS vcShortDescription,1 AS [Level],
	CAST (REPLICATE('.',1) + C1.vcCategoryName as varchar(1000) )  as vcNewCategoryName,
	CAST(C1.numCategoryID AS VARCHAR(MAX)) AS [Order]
 FROM dbo.Category C1 WHERE C1.numDepCategory=0 AND  C1.numDomainID=@numDomainID 
 UNION ALL
 SELECT  C1.numCategoryID,C1.vcCategoryName,C1.numDepCategory,C1.tintLevel,case when C1.bitLink=0 then 'No' when C1.bitLink=1 then 'Yes' end as Link,C1.vcLink as vcLink,CL.vcCategoryName as DepCategory ,
	C1.intDisplayOrder,C1.vcDescription,
	CASE WHEN LEN(ISNULL(C1.vcDescription ,'')) > 0 THEN  
	CASE WHEN LEN(C1.vcDescription) > 40 THEN SUBSTRING(C1.vcDescription,0,40) + ' [...]' 
         ELSE C1.vcDescription + ' [...]'	END 
    ELSE
		 ISNULL(C1.vcDescription ,'')   
    END     
         AS vcShortDescription,CL.[Level] + 1 AS [Level],
	CAST (REPLICATE('.',[Level] * 3) + C1.vcCategoryName as varchar(1000) )  as vcNewCategoryName,
	[Order] + '.' + CAST(C1.numCategoryID AS VARCHAR(MAX)) AS [Order]
 FROM dbo.Category C1 
 INNER JOIN CategoryList CL
 ON CL.numCategoryID = C1.numDepCategory  WHERE  C1.numDomainID=@numDomainID 
)   

SELECT numCategoryID,vcCategoryName,(CASE WHEN  numDepCategory=0 THEN NULL ELSE numDepCategory END) AS numDepCategory ,tintLevel,Link,vcLink,DepCategory,(SELECT COUNT(*) FROM [SiteCategories] SC WHERE SC.[numSiteID] =@numSiteID AND SC.[numCategoryID]= C1.[numCategoryID]) AS ShowInECommerce,
ISNULL((SELECT COUNT(*) FROM [ItemCategory] WHERE [numCategoryID] = C1.[numCategoryID]),0) AS ItemCount FROM CategoryList C1 ORDER BY [Order]
END


DROP TABLE #TempCategory
END
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetChartAcntDetailsForBalanceSheet_New')
DROP PROCEDURE USP_GetChartAcntDetailsForBalanceSheet_New
GO
CREATE PROCEDURE [dbo].[USP_GetChartAcntDetailsForBalanceSheet_New]                                 
@numDomainId AS NUMERIC(9),                                                  
@dtFromDate AS DATETIME,                                                
@dtToDate AS DATETIME,
@ClientTimeZoneOffset INT, 
@numAccountClass AS NUMERIC(9)=0,
@DateFilter AS VARCHAR(20),
@ReportColumn AS VARCHAR(20)                                                        
AS                                                                
BEGIN                                                                

	DECLARE @FinancialYearStartDate AS DATETIME
	SELECT @FinancialYearStartDate = dbo.GetFiscalStartDate(YEAR(GETDATE()),@numDomainId)

	DECLARE @TEMPQuarter AS TABLE
	(
		ID INT,
		StartDate DATETIME,
		EndDate DATETIME
	)

	INSERT INTO @TEMPQuarter VALUES (1,@FinancialYearStartDate,DATEADD(ms,-3,DATEADD(QUARTER,1,@FinancialYearStartDate)))
	INSERT INTO @TEMPQuarter VALUES (2,DATEADD(QUARTER,1,@FinancialYearStartDate),DATEADD(ms,-3,DATEADD(QUARTER,2,@FinancialYearStartDate)))
	INSERT INTO @TEMPQuarter VALUES (3,DATEADD(QUARTER,2,@FinancialYearStartDate),DATEADD(ms,-3,DATEADD(QUARTER,3,@FinancialYearStartDate)))
	INSERT INTO @TEMPQuarter VALUES (4,DATEADD(QUARTER,3,@FinancialYearStartDate),DATEADD(ms,-3,DATEADD(QUARTER,4,@FinancialYearStartDate)))

	IF @DateFilter = 'CurYear'
	BEGIN
		SELECT @dtFromDate = @FinancialYearStartDate,
			@dtToDate = DATEADD(ms,-3,DATEADD(YEAR,1,@FinancialYearStartDate));
	END
	ELSE IF @DateFilter = 'PreYear'
	BEGIN
		SELECT @dtFromDate = DATEADD(YEAR,-1,@FinancialYearStartDate),
			@dtToDate = DATEADD(ms,-3,DATEADD(YEAR,1,DATEADD(YEAR,-1,@FinancialYearStartDate)));
	END
	ELSE IF @DateFilter = 'CurPreYear'
	BEGIN
		SELECT @dtFromDate = DATEADD(YEAR,-1,@FinancialYearStartDate),
			@dtToDate = DATEADD(ms,-3,DATEADD(YEAR,1,@FinancialYearStartDate));
	END
	ELSE IF @DateFilter = 'CuQur'
	BEGIN
		SELECT @dtFromDate = StartDate, @dtToDate=EndDate FROM @TEMPQuarter WHERE GetDate() BETWEEN StartDate AND EndDate
	END
	ELSE IF @DateFilter = 'CurPreQur'
	BEGIN
		SELECT @dtFromDate = DATEADD(QUARTER,-1,StartDate), @dtToDate=EndDate FROM @TEMPQuarter WHERE GetDate() BETWEEN StartDate AND EndDate
	END
	ELSE IF @DateFilter = 'PreQur'
	BEGIN
		SELECT @dtFromDate = DATEADD(QUARTER,-1,StartDate), @dtToDate=DATEADD(QUARTER,-1,EndDate) FROM @TEMPQuarter WHERE GetDate() BETWEEN StartDate AND EndDate
	END
	ELSE IF @DateFilter = 'LastMonth'
	BEGIN
		select @dtFromDate = DATEADD(mm,-1,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)),
				@dtToDate = DATEADD(ms,-3,DATEADD(mm,0,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)));  
	END
	ELSE IF @DateFilter = 'ThisMonth'
	BEGIN
		select @dtFromDate = DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0),
				@dtToDate = DATEADD(ms,- 3,DATEADD(mm,0,DATEADD(mm,DATEDIFF(mm,0,GETDATE())+1,0)));  
	END
	ELSE IF @DateFilter = 'CurPreMonth'
	BEGIN
		select @dtFromDate = DATEADD(mm,-1,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)),
				@dtToDate = DATEADD(ms,- 3,DATEADD(mm,0,DATEADD(mm,DATEDIFF(mm,0,GETDATE())+1,0))); 
	END

	CREATE TABLE #VIEW_JOURNALBS
	(
		numDomainID NUMERIC(18,0),
		numAccountClass NUMERIC(18,0),
		COAvcAccountCode VARCHAR(50),
		datEntry_Date DATETIME,
		Debit MONEY,
		Credit MONEY
	)

	INSERT INTO #VIEW_JOURNALBS SELECT numDomainID,numAccountClass,COAvcAccountCode,datEntry_Date,Debit,Credit FROM VIEW_JOURNALBS WHERE numDomainID =@numDomainId AND (numAccountClass=@numAccountClass OR @numAccountClass=0) ORDER BY numAccountId; 

	DECLARE @view_journal TABLE
	(
		numAccountId NUMERIC(18,0),
		vcAccountCode VARCHAR(50),
		datEntry_Date SMALLDATETIME,
		Debit MONEY,
		Credit MONEY
	)

	INSERT INTO @view_journal SELECT numAccountId,vcAccountCode,datEntry_Date,Debit,Credit FROM view_journal WHERE numDomainId = @numDomainID AND (numAccountClass=@numAccountClass OR @numAccountClass=0);
	
	DECLARE @PLCHARTID NUMERIC(8)
	DECLARE @PLCHARTAccountCode AS VARCHAR(100) = ''
	SELECT @PLCHARTID = numAccountId FROM dbo.Chart_Of_Accounts WHERE numDomainId=@numDomainID AND bitProfitLoss=1

	DECLARE @PLOPENING AS MONEY
	SELECT @PLOPENING = ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM @view_journal VJ WHERE numAccountId=@PLCHARTID

	;WITH DirectReport (ParentId, numAccountTypeID, vcAccountType,vcAccountCode, LEVEL,Struc)
	AS
	(
		SELECT 
			CAST(0 AS NUMERIC(18)) AS ParentId, 
			[ATD].[numAccountTypeID], 
			[ATD].[vcAccountType],
			[ATD].[vcAccountCode],
			1 AS LEVEL, 
			CAST(CAST([ATD].[vcAccountCode] AS VARCHAR) AS VARCHAR(100))  AS Struc
		FROM 
			[dbo].[AccountTypeDetail] AS ATD
		WHERE 
			[ATD].[numDomainID]=@numDomainId 
			AND [ATD].[vcAccountCode] IN ('0101','0102','0105')
		UNION ALL
		SELECT 
			ATD.[numParentID] AS ParentId, 
			[ATD].[numAccountTypeID], 
			[ATD].[vcAccountType],
			[ATD].[vcAccountCode],
			LEVEL + 1, 
			CAST(d.Struc + '#' + cast([ATD].[vcAccountCode] AS VARCHAR) AS VARCHAR(100))  AS Struc
		FROM 
			[dbo].[AccountTypeDetail] AS ATD 
		JOIN 
			[DirectReport] D 
		ON 
			D.[numAccountTypeID] = [ATD].[numParentID]
		WHERE 
			[ATD].[numDomainID]=@numDomainId 
	),
	DirectReport1 (ParentId, numAccountTypeID, vcAccountType,vcAccountCode, LEVEL,numAccountId,Struc, bitProfitLoss)
	AS
	(
		SELECT 
			ParentId, 
			numAccountTypeID, 
			vcAccountType,
			vcAccountCode, 
			LEVEL,
			CAST(NULL AS NUMERIC(18)),
			Struc,
			CAST(0 AS BIT)
		FROM 
			DirectReport 
		UNION ALL
		SELECT 
			COA.numParntAcntTypeId AS ParentId,
			CAST(NULL AS NUMERIC(18)), 
			[vcAccountName],
			COA.[vcAccountCode],
			LEVEL + 1,
			[COA].[numAccountId], 
			CAST(d.Struc + '#' + CAST(COA.[vcAccountCode] AS VARCHAR) AS VARCHAR(100)) AS Struc,
			COA.bitProfitLoss
		FROM 
			[dbo].[Chart_of_Accounts] AS COA 
		JOIN 
			[DirectReport1] D 
		ON 
			D.[numAccountTypeID] = COA.[numParntAcntTypeId]
		WHERE 
			[COA].[numDomainID]=@numDomainId 
			AND ISNULL(COA.bitActive,0) = 1
	)

  
	SELECT 
		ParentId, 
		numAccountTypeID, 
		vcAccountType, 
		LEVEL, 
		vcAccountCode,
		numAccountId,
		Struc,
		bitProfitLoss
	INTO 
		#tempDirectReport
	FROM 
		DirectReport1

	SELECT TOP 1 @PLCHARTAccountCode=Struc FROM #tempDirectReport WHERe bitProfitLoss = 1

	SELECT 
		COA.ParentId, 
		COA.numAccountTypeID, 
		COA.vcAccountType, 
		COA.LEVEL, 
		COA.vcAccountCode, 
		COA.numAccountId, 
		COA.Struc,
		(CASE 
			WHEN COA.vcAccountCode LIKE '0105%' AND COA.numAccountId <> @PLCHARTID THEN (ISNULL(Debit,0) * (-1) - ISNULL(Credit,0) * (-1)) 
			ELSE ISNULL(Debit,0) - ISNULL(Credit,0) 
		END) AS Amount,
		V.datEntry_Date
	INTO 
		#tempViewData
	FROM 
		#tempDirectReport COA 
	LEFT JOIN 
		#VIEW_JOURNALBS V 
	ON  
		V.COAvcAccountCode like COA.vcAccountCode + '%' 
		AND datEntry_Date <= @dtToDate
	WHERE 
		COA.[numAccountId] IS NOT NULL

	
	DECLARE @columns VARCHAR(8000);--SET @columns = '';
	DECLARE @SUMColumns VARCHAR(8000);--SET @SUMColumns = '';
	DECLARE @PivotColumns VARCHAR(8000);--SET @PivotColumns = '';
	DECLARE @Select VARCHAR(8000)
	DECLARE @Where VARCHAR(8000)
	SET @Select = 'SELECT ParentId, numAccountTypeID, vcAccountType, LEVEL, vcAccountCode, numAccountId,Struc'


	DECLARE @sql VARCHAR(MAX) = ''

	IF @ReportColumn = 'Year'
	BEGIN
		CREATE TABLE #tempYearMonth
		(
			intYear INT,
			intMonth INT,
			MonthYear VARCHAR(50),
			StartDate DATETIME,
			EndDate DATETIME,
			ProfitLossAmount MONEY
		)

		;WITH CTE AS 
		(
			SELECT
				YEAR(@dtFromDate) AS 'yr',
				MONTH(@dtFromDate) AS 'mm',
				DATENAME(mm, @dtFromDate) AS 'mon',
				(DATENAME(mm, @dtFromDate) + '_' + CAST(YEAR(@dtFromDate) AS VARCHAR)) AS MonthYear,
				CAST(DATEFROMPARTS(YEAR(@dtFromDate),MONTH(@dtFromDate),1) AS DATETIME) dtStartDate,
				DATEADD(MS,-2,CAST(DATEADD(MONTH,1,DATEFROMPARTS(YEAR(@dtFromDate),MONTH(@dtFromDate),1)) AS DATETIME)) dtEndDate,
				@dtFromDate 'new_date'
			UNION ALL
			SELECT
				YEAR(DATEADD(d,1,new_date)) AS 'yr',
				MONTH(DATEADD(d,1,new_date)) AS 'mm',
				DATENAME(mm, DATEADD(d,1,new_date)) AS 'mon',
				(DATENAME(mm, DATEADD(d,1,new_date)) + '_' + CAST(YEAR(DATEADD(d,1,new_date)) AS VARCHAR)) AS MonthYear,
				CAST(DATEFROMPARTS(YEAR(DATEADD(d,1,new_date)),MONTH(DATEADD(d,1,new_date)),1) AS DATETIME) dtStartDate,
				DATEADD(MS,-2,CAST(DATEADD(MONTH,1,DATEFROMPARTS(YEAR(DATEADD(d,1,new_date)),MONTH(DATEADD(d,1,new_date)),1)) AS DATETIME)) dtEndDate,
				DATEADD(d,1,new_date) 'new_date'
			FROM CTE
			WHERE DATEADD(d,1,new_date) < @dtToDate
		)
	
		INSERT INTO 
			#tempYearMonth
		SELECT 
			yr,mm,MonthYear,dtStartDate,dtEndDate,0
		FROM 
			CTE
		GROUP BY 
			yr, mm, MonthYear, dtStartDate, dtEndDate
		ORDER BY 
			yr, mm
		OPTION (MAXRECURSION 5000)

		INSERT INTO #tempYearMonth VALUES (5000,5000,'Total',@dtFromDate,@dtToDate,0)

		UPDATE 
			#tempYearMonth
		SET
			EndDate = (CASE WHEN EndDate < @dtToDate THEN EndDate ELSE @dtToDate END)

		UPDATE 
			#tempYearMonth
		SET
			ProfitLossAmount = (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM @view_journal VJ WHERE vcAccountCode LIKE '0103%' AND datEntry_Date between StartDate and EndDate)
			+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM @view_journal VJ WHERE vcAccountCode LIKE '0104%' AND datEntry_Date between StartDate and EndDate)
			+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM @view_journal VJ WHERE vcAccountCode LIKE '0106%' AND datEntry_Date between StartDate and EndDate)
			+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM @view_journal VJ WHERE (vcAccountCode LIKE '0103%' OR vcAccountCode LIKE '0104%' OR vcAccountCode LIKE '0106%') AND datEntry_Date <=  DATEADD(Minute,-1,StartDate))
			+ @PLOPENING

		SELECT @columns = STUFF((SELECT ', ISNULL(' + MonthYear  + ',0) AS [' + REPLACE(MonthYear,'_',' ') + ']' FROM #tempYearMonth ORDER BY intYear,intMonth FOR XML PATH('')), 1, 2, ''); 
		SELECT @PivotColumns = STUFF((SELECT ', ' + MonthYear FROM #tempYearMonth ORDER BY intYear,intMonth FOR XML PATH('')), 1, 2, ''); 

		SET @sql = 'SELECT
						ParentId,
						numAccountId,
						numAccountTypeID,
						vcAccountType,
						LEVEL,
						vcAccountCode,
						Struc,
						[Type],' + @columns + '
					FROM
					(
						SELECT 
							#tempDirectReport.ParentId,
							#tempDirectReport.numAccountId,
							#tempDirectReport.numAccountTypeID,
							#tempDirectReport.vcAccountType,
							#tempDirectReport.LEVEL,
							#tempDirectReport.vcAccountCode,
							(''#'' + #tempDirectReport.Struc + ''#'') AS Struc,
							(CASE WHEN ISNULL(#tempDirectReport.numAccountId,0) > 0 THEN 1 ELSE 2 END) [Type],
							(CASE WHEN ''#' + @PLCHARTAccountCode + '#'' LIKE ''%#''+ #tempDirectReport.vcAccountCode +''#%'' THEN Period.ProfitLossAmount ELSE 0 END) +
							ISNULL((SELECT sum(ISNULL(Debit,0)) FROM #VIEW_JOURNALBS VJ WHERE VJ.COAvcAccountCode like #tempDirectReport.vcAccountCode + ''%'' AND datEntry_Date <= Period.EndDate ),0) * (CASE WHEN #tempDirectReport.vcAccountCode LIKE ''0102%'' OR (#tempDirectReport.vcAccountCode LIKE ''0105%'' AND ISNULL(numAccountId,0) <> ' + CAST(@PLCHARTID AS VARCHAR) + ') THEN -1 ELSE 1 END) -
							ISNULL((SELECT sum(ISNULL(Credit,0)) FROM #VIEW_JOURNALBS VJ WHERE VJ.COAvcAccountCode like #tempDirectReport.vcAccountCode + ''%'' AND datEntry_Date <= Period.EndDate ),0) * (CASE WHEN #tempDirectReport.vcAccountCode LIKE ''0102%'' OR (#tempDirectReport.vcAccountCode LIKE ''0105%'' AND ISNULL(numAccountId,0) <> ' + CAST(@PLCHARTID AS VARCHAR) + ') THEN -1 ELSE 1 END) AS Amount,
							Period.MonthYear
						FROM 
							#tempDirectReport
						OUTER APPLY
						(
							SELECT
								MonthYear,
								StartDate,
								EndDate,
								ProfitLossAmount
							FROM
								#tempYearMonth
						) AS Period
					) AS SourceTable
					pivot
					(
						SUM(AMOUNT)
						FOR [MonthYear] IN ( ' + @pivotcolumns + ' )
					) AS P ORDER BY Struc, [Type] desc'

		PRINT @sql
		EXECUTE (@sql)

		DROP TABLE #tempYearMonth
	END
	Else IF @ReportColumn = 'Quarter'
	BEGIN
		CREATE TABLE #TempYearMonthQuarter
		(
			intYear INT,
			intQuarter INT,
			MonthYear VARCHAR(50),
			StartDate DATETIME,
			EndDate DATETIME,
			ProfitLossAmount MONEY
		)

		;WITH CTE AS 
		(
			SELECT
				dbo.GetFiscalyear(@dtFromDate,@numDomainId) AS 'yr',
				dbo.GetFiscalQuarter(@dtFromDate,@numDomainId) AS 'qq',
				('Q' + cast(dbo.GetFiscalQuarter(@dtFromDate,@numDomainId) AS VARCHAR) + '_' + cast(dbo.GetFiscalyear(@dtFromDate,@numDomainId) AS VARCHAR)) AS MonthYear,
				CASE dbo.GetFiscalQuarter(@dtFromDate,@numDomainId) 
				WHEN 1 THEN dateadd(month,0,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId))
				WHEN 2 THEN dateadd(month,3,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)) 
				WHEN 3 THEN dateadd(month,6,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId))
				WHEN 4 THEN dateadd(month,9,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)) 
				END AS dtStartDate,
				CASE dbo.GetFiscalQuarter(@dtFromDate,@numDomainId) 
				WHEN 1 THEN dateadd(MS,-3,dateadd(month,3,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)))
				WHEN 2 THEN dateadd(MS,-3,dateadd(month,6,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)))
				WHEN 3 THEN dateadd(MS,-3,dateadd(month,9,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)))
				WHEN 4 THEN dateadd(MS,-3,dateadd(month,12,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)))
				END AS dtEndDate,
				@dtFromDate 'new_date'
			UNION ALL
			SELECT
				dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId) AS 'yr',
				dbo.GetFiscalQuarter(DATEADD(d,1,new_date),@numDomainId) AS 'qq',
				('Q' + cast(dbo.GetFiscalQuarter(DATEADD(d,1,new_date),@numDomainId) AS VARCHAR) + '_' + cast(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId) AS VARCHAR)) AS MonthYear,
				CASE dbo.GetFiscalQuarter(DATEADD(d,1,new_date),@numDomainId) 
				WHEN 1 THEN dateadd(month,0,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId))
				WHEN 2 THEN dateadd(month,3,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)) 
				WHEN 3 THEN dateadd(month,6,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId))
				WHEN 4 THEN dateadd(month,9,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)) 
				END AS dtStartDate,
				CASE dbo.GetFiscalQuarter(DATEADD(d,1,new_date),@numDomainId) 
				WHEN 1 THEN dateadd(MS,-3,dateadd(month,3,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)))
				WHEN 2 THEN dateadd(MS,-3,dateadd(month,6,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)))
				WHEN 3 THEN dateadd(MS,-3,dateadd(month,9,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)))
				WHEN 4 THEN dateadd(MS,-3,dateadd(month,12,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)))
				END AS dtEndDate,
				DATEADD(d,1,new_date) 'new_date'
			FROM CTE
			WHERE DATEADD(d,1,new_date) < @dtToDate
		)
	
		INSERT INTO 
			#TempYearMonthQuarter 		
		SELECT 
			yr,qq,MonthYear,dtStartDate,dtEndDate,0
		FROM 
			CTE
		GROUP BY 
			yr, qq, MonthYear, dtStartDate, dtEndDate
		ORDER BY 
			yr, qq
		OPTION
			(MAXRECURSION 5000)

		INSERT INTO #TempYearMonthQuarter VALUES (5000,5000,'Total',@dtFromDate,@dtToDate,0)

		UPDATE 
			#TempYearMonthQuarter
		SET
			EndDate = (CASE WHEN EndDate < @dtToDate THEN EndDate ELSE @dtToDate END)

		UPDATE 
			#TempYearMonthQuarter
		SET
			ProfitLossAmount = (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM @view_journal VJ WHERE vcAccountCode LIKE '0103%' AND datEntry_Date between StartDate and EndDate)
			+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM @view_journal VJ WHERE vcAccountCode LIKE '0104%' AND datEntry_Date between StartDate and EndDate)
			+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM @view_journal VJ WHERE vcAccountCode LIKE '0106%' AND datEntry_Date between StartDate and EndDate)
			+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM @view_journal VJ WHERE (vcAccountCode LIKE '0103%' OR vcAccountCode LIKE '0104%' OR vcAccountCode LIKE '0106%') AND datEntry_Date <=  DATEADD(Minute,-1,StartDate))
			+ @PLOPENING

		SELECT @columns = STUFF((SELECT ', ISNULL(' + MonthYear  + ',0) AS [' + REPLACE(MonthYear,'_',' ') + ']' FROM #TempYearMonthQuarter ORDER BY intYear,intQuarter FOR XML PATH('')), 1, 2, ''); 
		SELECT @PivotColumns = STUFF((SELECT ', ' + MonthYear FROM #TempYearMonthQuarter ORDER BY intYear,intQuarter FOR XML PATH('')), 1, 2, ''); 

		
		SET @sql = 'SELECT
						ParentId,
						numAccountId,
						numAccountTypeID,
						vcAccountType,
						LEVEL,
						vcAccountCode,
						Struc,
						[Type],' + @columns + '
					FROM
					(
						SELECT 
							#tempDirectReport.ParentId,
							#tempDirectReport.numAccountId,
							#tempDirectReport.numAccountTypeID,
							#tempDirectReport.vcAccountType,
							#tempDirectReport.LEVEL,
							#tempDirectReport.vcAccountCode,
							(''#'' + #tempDirectReport.Struc + ''#'') AS Struc,
							(CASE WHEN ISNULL(#tempDirectReport.numAccountId,0) > 0 THEN 1 ELSE 2 END) [Type],
							(CASE WHEN ''#' + @PLCHARTAccountCode + '#'' LIKE ''%#''+ #tempDirectReport.vcAccountCode +''#%'' THEN Period.ProfitLossAmount ELSE 0 END) +
							ISNULL((SELECT sum(ISNULL(Debit,0)) FROM #VIEW_JOURNALBS VJ WHERE VJ.COAvcAccountCode like #tempDirectReport.vcAccountCode + ''%'' AND datEntry_Date <= Period.EndDate ),0) * (CASE WHEN #tempDirectReport.vcAccountCode LIKE ''0102%'' OR (#tempDirectReport.vcAccountCode LIKE ''0105%'' AND ISNULL(numAccountId,0) <> ' + CAST(@PLCHARTID AS VARCHAR) + ') THEN -1 ELSE 1 END) -
							ISNULL((SELECT sum(ISNULL(Credit,0)) FROM #VIEW_JOURNALBS VJ WHERE VJ.COAvcAccountCode like #tempDirectReport.vcAccountCode + ''%'' AND datEntry_Date <= Period.EndDate ),0) * (CASE WHEN #tempDirectReport.vcAccountCode LIKE ''0102%'' OR (#tempDirectReport.vcAccountCode LIKE ''0105%'' AND ISNULL(numAccountId,0) <> ' + CAST(@PLCHARTID AS VARCHAR) + ') THEN -1 ELSE 1 END) AS Amount,
							Period.MonthYear
						FROM 
							#tempDirectReport
						OUTER APPLY
						(
							SELECT
								MonthYear,
								StartDate,
								EndDate,
								ProfitLossAmount
							FROM
								#TempYearMonthQuarter
						) AS Period
					) AS SourceTable
					pivot
					(
						SUM(AMOUNT)
						FOR [MonthYear] IN ( ' + @pivotcolumns + ' )
					) AS P ORDER BY Struc, [Type] desc'

		PRINT @sql
		EXECUTE (@sql)

		DROP TABLE #TempYearMonthQuarter
	END
	ELSE
	BEGIN
		DECLARE @ProifitLossAmount AS MONEY = 0

		SET @ProifitLossAmount = (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM @view_journal VJ WHERE vcAccountCode  LIKE '0103%' AND datEntry_Date between @dtFromDate and @dtToDate)
								+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM @view_journal VJ WHERE vcAccountCode  LIKE '0104%' AND datEntry_Date between @dtFromDate and @dtToDate)
								+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM @view_journal VJ WHERE vcAccountCode  LIKE '0106%' AND datEntry_Date between @dtFromDate and @dtToDate)
								+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM @view_journal VJ WHERE (vcAccountCode LIKE '0103%' OR vcAccountCode LIKE '0104%' OR vcAccountCode LIKE '0106%') AND datEntry_Date <=  DATEADD(MILLISECOND,-3,@dtFromDate))
								+ @PLOPENING		

		SELECT 
			#tempDirectReport.ParentId,
			#tempDirectReport.numAccountId,
			#tempDirectReport.numAccountTypeID,
			#tempDirectReport.vcAccountType,
			#tempDirectReport.LEVEL,
			#tempDirectReport.vcAccountCode,
			('#' + #tempDirectReport.Struc + '#') AS Struc,
			(CASE WHEN ISNULL(#tempDirectReport.numAccountId,0) > 0 THEN 1 ELSE 2 END) [Type],
			(CASE WHEN '#' + @PLCHARTAccountCode + '#' LIKE '%#'+ #tempDirectReport.vcAccountCode +'#%' THEN @ProifitLossAmount ELSE 0 END) +
			ISNULL((SELECT ISNULL(sum(Debit),0) FROM #VIEW_JOURNALBS VJ WHERE VJ.COAvcAccountCode like #tempDirectReport.vcAccountCode + '%' AND datEntry_Date <= @dtToDate),0) * (CASE WHEN #tempDirectReport.vcAccountCode LIKE '0102%' OR (#tempDirectReport.vcAccountCode LIKE '0105%' AND ISNULL(numAccountId,0) <> @PLCHARTID) THEN -1 ELSE 1 END) -
			ISNULL((SELECT ISNULL(sum(Credit),0) FROM #VIEW_JOURNALBS VJ WHERE VJ.COAvcAccountCode like #tempDirectReport.vcAccountCode + '%' AND datEntry_Date <= @dtToDate),0) * (CASE WHEN #tempDirectReport.vcAccountCode LIKE '0102%' OR (#tempDirectReport.vcAccountCode LIKE '0105%' AND ISNULL(numAccountId,0) <> @PLCHARTID) THEN -1 ELSE 1 END) AS Amount
		FROM 
			#tempDirectReport
		ORDER BY 
			Struc, [Type] desc
	END

	DROP TABLE #tempViewData 
	DROP TABLE #tempDirectReport
	DROP TABLE #VIEW_JOURNALBS
END

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetChartAcntDetailsForProfitLoss_New')
DROP PROCEDURE USP_GetChartAcntDetailsForProfitLoss_New
GO
CREATE PROCEDURE [dbo].[USP_GetChartAcntDetailsForProfitLoss_New]
@numDomainId as numeric(9),                                          
@dtFromDate as datetime,                                        
@dtToDate as DATETIME,
@ClientTimeZoneOffset INT, --Added by Chintan to enable calculation of date according to client machine                                                   
@numAccountClass AS NUMERIC(9)=0,
@DateFilter AS VARCHAR(20),
@ReportColumn AS VARCHAR(20)
AS                                                        
BEGIN 
	DECLARE @FinancialYearStartDate AS DATETIME
	SELECT @FinancialYearStartDate = dbo.GetFiscalStartDate(YEAR(GETDATE()),@numDomainId)

	DECLARE @TEMPQuarter AS TABLE
	(
		ID INT,
		StartDate DATETIME,
		EndDate DATETIME
	)

	INSERT INTO @TEMPQuarter VALUES (1,@FinancialYearStartDate,DATEADD(ms,-3,DATEADD(QUARTER,1,@FinancialYearStartDate)))
	INSERT INTO @TEMPQuarter VALUES (2,DATEADD(QUARTER,1,@FinancialYearStartDate),DATEADD(ms,-3,DATEADD(QUARTER,2,@FinancialYearStartDate)))
	INSERT INTO @TEMPQuarter VALUES (3,DATEADD(QUARTER,2,@FinancialYearStartDate),DATEADD(ms,-3,DATEADD(QUARTER,3,@FinancialYearStartDate)))
	INSERT INTO @TEMPQuarter VALUES (4,DATEADD(QUARTER,3,@FinancialYearStartDate),DATEADD(ms,-3,DATEADD(QUARTER,4,@FinancialYearStartDate)))

	DECLARE @PLCHARTID NUMERIC(18,0)

	SELECT 
		@PLCHARTID=COA.numAccountId 
	FROM 
		Chart_of_Accounts COA 
	WHERE 
		numDomainID=@numDomainId 
		AND bitProfitLoss=1;

	CREATE TABLE #View_Journal
	(
		numAccountId NUMERIC(18,0),
		vcAccountCode VARCHAR(50),
		COAvcAccountCode VARCHAR(100),
		datEntry_Date SMALLDATETIME,
		Debit MONEY,
		Credit MONEY
	)

	INSERT INTO #View_Journal SELECT numAccountId,vcAccountCode,COAvcAccountCode, datEntry_Date,Debit,Credit FROM view_journal WHERE numDomainId = @numDomainID AND (numAccountClass=@numAccountClass OR @numAccountClass=0);

	IF @DateFilter = 'CurYear'
	BEGIN
		SELECT @dtFromDate = @FinancialYearStartDate,
		   @dtToDate = DATEADD(ms,-3,DATEADD(YEAR,1,@FinancialYearStartDate));
	END
	ELSE IF @DateFilter = 'PreYear'
	BEGIN
		SELECT @dtFromDate = DATEADD(YEAR,-1,@FinancialYearStartDate),
		   @dtToDate = DATEADD(ms,-3,DATEADD(YEAR,1,DATEADD(YEAR,-1,@FinancialYearStartDate)));
	END
	ELSE IF @DateFilter = 'CurPreYear'
	BEGIN
		SELECT @dtFromDate = DATEADD(YEAR,-1,@FinancialYearStartDate),
		   @dtToDate = DATEADD(ms,-3,DATEADD(YEAR,1,@FinancialYearStartDate));
	END
	ELSE IF @DateFilter = 'CuQur'
	BEGIN
		SELECT @dtFromDate = StartDate, @dtToDate=EndDate FROM @TEMPQuarter WHERE GetDate() BETWEEN StartDate AND EndDate
	END
	ELSE IF @DateFilter = 'CurPreQur'
	BEGIN
		SELECT @dtFromDate = DATEADD(QUARTER,-1,StartDate), @dtToDate=EndDate FROM @TEMPQuarter WHERE GetDate() BETWEEN StartDate AND EndDate
	END
	ELSE IF @DateFilter = 'PreQur'
	BEGIN
		SELECT @dtFromDate = DATEADD(QUARTER,-1,StartDate), @dtToDate=DATEADD(QUARTER,-1,EndDate) FROM @TEMPQuarter WHERE GetDate() BETWEEN StartDate AND EndDate
	END
	ELSE IF @DateFilter = 'LastMonth'
	BEGIN
		select @dtFromDate = DATEADD(mm,-1,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)),
				@dtToDate = DATEADD(ms,-3,DATEADD(mm,0,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)));  
	END
	ELSE IF @DateFilter = 'ThisMonth'
	BEGIN
		select @dtFromDate = DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0),
				@dtToDate = DATEADD(ms,- 3,DATEADD(mm,0,DATEADD(mm,DATEDIFF(mm,0,GETDATE())+1,0)));  
	END
	ELSE IF @DateFilter = 'CurPreMonth'
	BEGIN
		select @dtFromDate = DATEADD(mm,-1,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)),
				@dtToDate = DATEADD(ms,- 3,DATEADD(mm,0,DATEADD(mm,DATEDIFF(mm,0,GETDATE())+1,0))); 
	END

	;WITH DirectReport (ParentId, numAccountTypeID, vcAccountType,vcAccountCode, LEVEL,Struc)
	AS
	(
		SELECT 
			CAST(-1 AS NUMERIC(18)) AS ParentId, 
			[ATD].[numAccountTypeID], 
			[ATD].[vcAccountType],
			[ATD].[vcAccountCode],
			1 AS LEVEL, 
			CAST(CAST([ATD].[vcAccountCode] AS VARCHAR) AS VARCHAR(100))  AS Struc
		FROM 
			[dbo].[AccountTypeDetail] AS ATD
		WHERE 
			[ATD].[numDomainID]=@numDomainId 
			AND [ATD].[vcAccountCode] IN ('0103','0104','0106')
		UNION ALL
		SELECT 
			ATD.[numParentID] AS ParentId, 
			[ATD].[numAccountTypeID], 
			[ATD].[vcAccountType],
			[ATD].[vcAccountCode],
			LEVEL + 1, 
			CAST(d.Struc + '#' + cast([ATD].[vcAccountCode] AS VARCHAR) AS VARCHAR(100))  AS Struc
		FROM 
			[dbo].[AccountTypeDetail] AS ATD 
		JOIN 
			[DirectReport] D 
		ON 
			D.[numAccountTypeID] = [ATD].[numParentID]
		WHERE 
			[ATD].[numDomainID]=@numDomainId 
	),
	DirectReport1 (ParentId, numAccountTypeID, vcAccountType,vcAccountCode, LEVEL,numAccountId,Struc)
	AS
	(
		SELECT 
			ParentId, 
			numAccountTypeID, 
			vcAccountType,
			vcAccountCode, 
			LEVEL,
			CAST(NULL AS NUMERIC(18)),
			Struc
		FROM 
			DirectReport 
		UNION ALL
		SELECT 
			COA.numParntAcntTypeId AS ParentId,
			CAST(NULL AS NUMERIC(18)), COA.
			[vcAccountName],
			COA.[vcAccountCode],
			LEVEL + 1,
			[COA].[numAccountId], 
			CAST(d.Struc + '#' + CAST(COA.[vcAccountCode] AS VARCHAR) AS VARCHAR(100)) AS Struc
		FROM 
			[dbo].[Chart_of_Accounts] AS COA 
		JOIN 
			[DirectReport1] D 
		ON 
			D.[numAccountTypeID] = COA.[numParntAcntTypeId]
		WHERE 
			[COA].[numDomainID]=@numDomainId 
			AND COA.bitActive = 1
	)

  
	SELECT 
		ParentId, 
		numAccountTypeID, 
		vcAccountType, 
		LEVEL, 
		vcAccountCode,
		numAccountId,
		Struc 
	INTO 
		#tempDirectReport
	FROM 
		DirectReport1 

	INSERT INTO #tempDirectReport
	SELECT 0,-1,'Ordinary Income/Expense',0,NULL,NULL,'-1'
	UNION ALL
	SELECT 0,-2,'Other Income and Expenses',0,NULL,NULL,'-2'
	

	UPDATE 
		#tempDirectReport 
	SET 
		Struc='-1#' + Struc 
	WHERE 
		[vcAccountCode] NOT LIKE '010302%' 
		AND [vcAccountCode] NOT LIKE '010402%' 

	UPDATE 
		#tempDirectReport 
	SET 
		Struc='-2#' + Struc 
	WHERE 
		[vcAccountCode] LIKE '010302%' 
		OR [vcAccountCode] LIKE '010402%'

	UPDATE 
		#tempDirectReport 
	SET 
		[ParentId]=-2 
	WHERE 
		[vcAccountCode] IN ('010302','010402')

	SELECT 
		COA.ParentId, 
		COA.numAccountTypeID, 
		COA.vcAccountType, 
		COA.LEVEL, 
		COA.vcAccountCode, 
		COA.numAccountId, 
		COA.Struc,
		CASE 
			WHEN COA.vcAccountCode LIKE '010301%' OR COA.vcAccountCode LIKE '010302%' 
			THEN (ISNULL(Credit,0) - ISNULL(Debit,0))
			ELSE (ISNULL(Debit,0) - ISNULL(Credit,0)) 
		END AS Amount,
		V.datEntry_Date
	INTO 
		#tempViewData
	FROM 
		#tempDirectReport COA 
	JOIN 
		#View_Journal V 
	ON  
		V.COAvcAccountCode like COA.vcAccountCode + '%' 
		AND datEntry_Date BETWEEN  @dtFromDate AND @dtToDate 
	WHERE 
		COA.[numAccountId] IS NOT NULL

	DECLARE @columns VARCHAR(8000);--SET @columns = '';
	DECLARE @SUMColumns VARCHAR(8000);--SET @SUMColumns = '';
	DECLARE @PivotColumns VARCHAR(8000);--SET @PivotColumns = '';
	DECLARE @ProfitLossCurrentColumns VARCHAR(8000) = '';
	DECLARE @ProfitLossSumCurrentColumns VARCHAR(8000) = ',0';
	DECLARE @ProfitLossOpeningColumns VARCHAR(8000) = '';
	DECLARE @ProfitLossSumOpeningColumns VARCHAR(8000) = '';

	DECLARE @Select VARCHAR(8000)
	DECLARE @Where VARCHAR(8000)
	SET @Select = 'SELECT ParentId, numAccountTypeID, vcAccountType, LEVEL, vcAccountCode, numAccountId,(''#'' + Struc + ''#'') AS Struc'

	IF @ReportColumn = 'Year' OR @ReportColumn = 'Quarter'
	BEGIN
	; WITH CTE AS (
		SELECT
			(CASE WHEN @ReportColumn = 'Quarter' THEN dbo.GetFiscalyear(@dtFromDate,@numDomainId) ELSE YEAR(@dtFromDate) END) AS 'yr',
			MONTH(@dtFromDate) AS 'mm',
			DATENAME(mm, @dtFromDate) AS 'mon',
			DATEPART(d,@dtFromDate) AS 'dd',
			dbo.GetFiscalQuarter(@dtFromDate,@numDomainId) AS 'qq',
			@dtFromDate 'new_date'
		UNION ALL
		SELECT
			(CASE WHEN @ReportColumn = 'Quarter' THEN dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId) ELSE YEAR(DATEADD(d,1,new_date)) END) AS 'yr',
			MONTH(DATEADD(d,1,new_date)) AS 'mm',
			DATENAME(mm, DATEADD(d,1,new_date)) AS 'mon',
			DATEPART(d,@dtFromDate) AS 'dd',
			dbo.GetFiscalQuarter(DATEADD(d,1,new_date),@numDomainId) AS 'qq',
			DATEADD(d,1,new_date) 'new_date'
		FROM CTE
		WHERE DATEADD(d,1,new_date) < @dtToDate
		)
	
		SELECT yr AS 'Year1',qq AS 'Quarter1', mm as 'Month1', mon AS 'MonthName1', count(dd) AS 'Days1' into #tempYearMonth
		FROM CTE
		GROUP BY mon, yr, mm, qq
		ORDER BY yr, mm, qq
		OPTION (MAXRECURSION 5000)

		IF @ReportColumn = 'Year'
		BEGIN
			-- Profit / (Loss) Current dynamic columns
			-- We are just inserting rows calculation is made in code
			SELECT
				@ProfitLossCurrentColumns = COALESCE(@ProfitLossCurrentColumns + ',0 AS [' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + ']',
				',0 AS [' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + ']')
			FROM (SELECT DISTINCT Year1,MonthName1,Month1  FROM #tempYearMonth)T
			ORDER BY Year1,Month1

			-- Profit / (Loss) Opening dynamic columns
			SELECT
				@ProfitLossOpeningColumns = COALESCE(@ProfitLossOpeningColumns + ',ISNULL((select isnull(sum(credit),0)-isnull(sum(debit),0) from #View_Journal vj where (( vcaccountcode  like ''0103%'' or  vcaccountcode  like ''0104%'' or  vcaccountcode  like ''0106%'') OR numaccountid = ' + CAST(@PLCHARTID AS VARCHAR) + ') and datentry_date <  ''' + CONVERT(VARCHAR(50),DATEADD(DAY, 0, DATEADD(MONTH, Month1 - 1, DATEADD(YEAR, Year1-1900, 0))),23) + '''),0) AS [' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + ']',
				',ISNULL((select isnull(sum(credit),0)-isnull(sum(debit),0) from #View_Journal vj WHERE (( vcaccountcode  like ''0103%'' or  vcaccountcode  like ''0104%'' or  vcaccountcode  like ''0106%'') OR numaccountid = ' + CAST(@PLCHARTID AS VARCHAR) + ') and datentry_date <  ''' + CONVERT(VARCHAR(50),DATEADD(DAY, 0, DATEADD(MONTH, Month1 - 1, DATEADD(YEAR, Year1-1900, 0))),23) + '''),0) AS [' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + ']')
			FROM (SELECT DISTINCT Year1,MonthName1,Month1  FROM #tempYearMonth)T
			ORDER BY Year1,Month1

			SELECT @ProfitLossSumOpeningColumns = ',ISNULL((select isnull(sum(credit),0)-isnull(sum(debit),0) from #View_Journal vj WHERE (( vcaccountcode  like ''0103%'' or  vcaccountcode  like ''0104%'' or  vcaccountcode  like ''0106%'') OR numaccountid = ' + CAST(@PLCHARTID AS VARCHAR) + ') and datentry_date <  ''' + CONVERT(VARCHAR(50),@dtFromDate,23) + '''),0) AS [Total]'
			
			-- Chart of accounts dynamic columns
			SELECT
				@columns = COALESCE(@columns + ',ISNULL([' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + '],0) AS [' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + ']',
				',ISNULL([' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + '],0) AS [' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + ']'),
				@SUMColumns = COALESCE(@SUMColumns + '+ ISNULL([' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + '],0)',
				',ISNULL([' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + '],0)'),
				@PivotColumns = COALESCE(@PivotColumns + ',[' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + ']',
				'[' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + ']')
			FROM #tempYearMonth ORDER BY Year1,MONTH1

			SET @SUMColumns = @SUMColumns + ' AS Total, (CASE WHEN ISNULL(numAccountId,0) > 0 THEN 1 ELSE 2 END) [Type]'
			SET @Where = '	FROM (Select COA.ParentId, COA.numAccountTypeID, COA.vcAccountType, COA.LEVEL, COA.vcAccountCode, COA.numAccountId,COA.Struc,
					SUM(Case 
						When (COA.numAccountTypeID = -1 OR COA.numAccountTypeID = -2)
						THEN (Case WHEN V.vcAccountCode LIKE ''0104%'' OR V.vcAccountCode LIKE ''0106%'' THEN ISNULL(Amount,0) * -1
						ELSE ISNULL(Amount,0) END)
 					ELSE ISNULL(Amount,0) END) AS Amount,
					DATENAME(mm,datEntry_Date) + '' '' + CAST(YEAR(datEntry_Date) AS VARCHAR(4)) AS MonthYear 
					FROM #tempDirectReport COA LEFT OUTER JOIN #tempViewData V ON  
					V.Struc like COA.Struc + ''%'' 
					GROUP BY YEAR(datEntry_Date),MONTH(datEntry_Date),DATENAME(mm,datEntry_Date),COA.ParentId, COA.numAccountTypeID, COA.vcAccountType, COA.LEVEL, COA.vcAccountCode, COA.numAccountId,COA.Struc) AS t
					PIVOT
					(
					  SUM(Amount)
					  FOR [MonthYear] IN( ' + @PivotColumns +  ')
					) AS p 
					UNION SELECT 0, -3, ''Profit / (Loss) Current'', 0, NULL, NULL,''-3''' + @ProfitLossCurrentColumns + @ProfitLossSumCurrentColumns + ',2
					UNION SELECT 0, -4, ''Profit / (Loss) Opening'', 0, NULL, NULL,''-4''' + @ProfitLossOpeningColumns + @ProfitLossSumOpeningColumns + ',2 ORDER BY Struc'

		END
		Else IF @ReportColumn = 'Quarter'
		BEGIN

			-- Profit / (Loss) Current dynamic columns
			-- We are just inserting rows calculation is made in code
			SELECT
				@ProfitLossCurrentColumns = COALESCE(@ProfitLossCurrentColumns + ',0 AS [Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + ']',
				',0 AS [Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + ']')
			FROM (SELECT DISTINCT Year1,Quarter1  FROM #tempYearMonth)T
			ORDER BY Year1,Quarter1

			-- Profit / (Loss) Opening dynamic columns
			SELECT
				@ProfitLossOpeningColumns = COALESCE(@ProfitLossOpeningColumns + ',ISNULL((select isnull(sum(credit),0)-isnull(sum(debit),0) from #View_Journal vj where (( vcaccountcode  like ''0103%'' or  vcaccountcode  like ''0104%'' or  vcaccountcode  like ''0106%'') OR numaccountid = ' + CAST(@PLCHARTID AS VARCHAR) + ') and datentry_date <  ''' + CONVERT(VARCHAR(50),DATEADD(QUARTER,Quarter1 - 1,dbo.GetFiscalStartDate(Year1,@numDomainID)),23) + '''),0) AS [Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + ']',
				',ISNULL((select isnull(sum(credit),0)-isnull(sum(debit),0) from #View_Journal vj where (( vcaccountcode  like ''0103%'' or  vcaccountcode  like ''0104%'' or  vcaccountcode  like ''0106%'') OR numaccountid = ' + CAST(@PLCHARTID AS VARCHAR) + ') and datentry_date <  ''' + CONVERT(VARCHAR(50),DATEADD(QUARTER,Quarter1 - 1,dbo.GetFiscalStartDate(Year1,@numDomainID)),23) + '''),0) AS [Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + ']')
			FROM (SELECT DISTINCT Year1,Quarter1  FROM #tempYearMonth)T
			ORDER BY Year1,Quarter1

			SELECT @ProfitLossSumOpeningColumns = ',ISNULL((select isnull(sum(credit),0)-isnull(sum(debit),0) from #View_Journal vj where (( vcaccountcode  like ''0103%'' or  vcaccountcode  like ''0104%'' or  vcaccountcode  like ''0106%'') OR numaccountid = ' + CAST(@PLCHARTID AS VARCHAR) + ') and datentry_date <  ''' + CONVERT(VARCHAR(50),@dtFromDate,23) + '''),0) AS [Total]'

			
			-- Chart of accounts dynamic columns
			SELECT
				@columns = COALESCE(@columns + ',ISNULL([Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + '],0) AS [Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + ']',
				',ISNULL([Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + '],0) AS [Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + ']'),
				@SUMColumns = COALESCE(@SUMColumns + '+ ISNULL([Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + '],0)',
				',ISNULL([Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + '],0)'),
				@PivotColumns = COALESCE(@PivotColumns + ',[Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + ']',
				'[Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + ']')
			FROM (SELECT DISTINCT Year1,Quarter1  FROM #tempYearMonth)T
			ORDER BY Year1,Quarter1

			SET @SUMColumns = @SUMColumns + ' AS Total, (CASE WHEN ISNULL(numAccountId,0) > 0 THEN 1 ELSE 2 END) [Type]'
			SET @Where = '	FROM (Select COA.ParentId, COA.numAccountTypeID, COA.vcAccountType, COA.LEVEL, COA.vcAccountCode, COA.numAccountId,COA.Struc,
					SUM(Case 
							When(COA.numAccountTypeID = -1 OR COA.numAccountTypeID = -2) 
							THEN (Case WHEN V.vcAccountCode LIKE ''0104%'' OR V.vcAccountCode LIKE ''0106%'' THEN ISNULL(Amount,0) * -1
							ELSE ISNULL(Amount,0) END)
						ELSE ISNULL(Amount,0) END) AS Amount,
					''Q'' + cast(dbo.GetFiscalQuarter(datEntry_Date,' + CAST(@numDomainID AS VARCHAR(20)) + ') as varchar) + '' '' + CAST(dbo.GetFiscalyear(datEntry_Date,' + CAST(@numDomainID AS VARCHAR(20)) + ') AS VARCHAR(4)) AS MonthYear 
					FROM #tempDirectReport COA LEFT OUTER JOIN #tempViewData V ON  
					V.Struc like COA.Struc + ''%'' 
					GROUP BY dbo.GetFiscalyear(datEntry_Date,' + CAST(@numDomainID AS VARCHAR(20)) + '),dbo.GetFiscalQuarter(datEntry_Date,' + CAST(@numDomainID AS VARCHAR(20)) + '),COA.ParentId, COA.numAccountTypeID, COA.vcAccountType, COA.LEVEL, COA.vcAccountCode, COA.numAccountId,COA.Struc) AS t
					PIVOT
					(
					  SUM(Amount)
					  FOR [MonthYear] IN( ' + @PivotColumns +  ')
					) AS p 
					UNION SELECT 0, -3, ''Profit / (Loss) Current'', 0, NULL, NULL,''-3''' + @ProfitLossCurrentColumns + @ProfitLossSumCurrentColumns + ',2
					UNION SELECT 0, -4, ''Profit / (Loss) Opening'', 0, NULL, NULL,''-4''' + @ProfitLossOpeningColumns + @ProfitLossSumOpeningColumns + ',2 ORDER BY Struc'

			
		END

		DROP TABLE #tempYearMonth
	END
	ELSE
	BEGIN
		SELECT @ProfitLossSumOpeningColumns = ',ISNULL((select isnull(sum(credit),0)-isnull(sum(debit),0) from #View_Journal vj where (( vcaccountcode  like ''0103%'' or  vcaccountcode  like ''0104%'' or  vcaccountcode  like ''0106%'') OR numaccountid = ' + CAST(@PLCHARTID AS VARCHAR) + ') and datentry_date <  ''' + CONVERT(VARCHAR(50),@dtFromDate,23) + '''),0) AS [Total]'
			

		SET @columns = ',ISNULL(ISNULL(Amount,0),0) as Total, (CASE WHEN ISNULL(numAccountId,0) > 0 THEN 1 ELSE 2 END) [Type]';
		SET @SUMColumns = '';
		SET @PivotColumns = '';
		SET @Where = ' FROM (Select COA.ParentId, COA.numAccountTypeID, COA.vcAccountType, COA.LEVEL, COA.vcAccountCode, COA.numAccountId,COA.Struc,
				SUM(Case 
						When (COA.numAccountTypeID = -1 OR COA.numAccountTypeID = -2)
						THEN (Case WHEN V.vcAccountCode LIKE ''0104%'' OR V.vcAccountCode LIKE ''0106%'' THEN ISNULL(Amount,0) * -1
						ELSE ISNULL(Amount,0) END)
					ELSE ISNULL(Amount,0) END) AS Amount
				FROM #tempDirectReport COA LEFT OUTER JOIN #tempViewData V ON  
				V.Struc like COA.Struc + ''%'' 
				GROUP BY COA.ParentId, COA.numAccountTypeID, COA.vcAccountType, COA.LEVEL, COA.vcAccountCode, COA.numAccountId,COA.Struc) AS t
				UNION SELECT 0, -3, ''Profit / (Loss) Current'', 0, NULL, NULL,''-3'',0,2
				UNION SELECT 0, -4, ''Profit / (Loss) Opening'', 0, NULL, NULL,''-4''' + @ProfitLossSumOpeningColumns + ',2 ORDER BY Struc'
	END

	PRINT @Select
	PRINT @columns
	PRINT @SUMColumns
	PRINT @Where


	EXECUTE (@Select + @columns + @SUMColumns  + ' ' + @Where)

	DROP TABLE #View_Journal
	DROP TABLE #tempViewData 
	DROP TABLE #tempDirectReport
END
/****** Object:  StoredProcedure [dbo].[USP_GetChildItemsForKitsAss]    Script Date: 07/26/2008 16:16:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetChildItemsForKitsAss')
DROP PROCEDURE USP_GetChildItemsForKitsAss
GO
CREATE PROCEDURE [dbo].[USP_GetChildItemsForKitsAss]                             
@numKitId as numeric(9)                            
as                            


WITH CTE(numItemKitID,numItemCode,vcItemName,monAverageCost,numAssetChartAcntId,numWarehouseItmsID,txtItemDesc,numQtyItemsReq,numOppChildItemID,
charItemType,ItemType,StageLevel,monListPrice,numBaseUnit,numCalculatedQty,numIDUOMId,sintOrder,numRowNumber,RStageLevel)
AS
(
select convert(NUMERIC(18,0),0),numItemCode,vcItemName,monAverageCost,numAssetChartAcntId,isnull(Dtl.numWareHouseItemId,0) as numWarehouseItmsID,ISNULL(Dtl.vcItemDesc,txtItemDesc) AS txtItemDesc,DTL.numQtyItemsReq,0 as numOppChildItemID,
case when charItemType='P' then 'Inventory Item' when charItemType='S' then 'Service' when charItemType='A' then 'Accessory' when charItemType='N' then 'Non-Inventory Item' end as charItemType,charItemType as ItemType                            
,1,(CASE WHEN charItemType='P' THEN (SELECT monWListPrice FROM WarehouseItems WHERE numWareHouseItemID= Dtl.numWareHouseItemId) ELSE Item.monListPrice END) monListPrice,ISNULL(numBaseUnit,0),CAST(DTL.numQtyItemsReq AS NUMERIC(9,0)) AS numCalculatedQty,ISNULL(Dtl.numUOMId,ISNULL(numBaseUnit,0)) AS numIDUOMId,ISNULL(sintOrder,0) sintOrder,ROW_NUMBER() OVER (ORDER BY ISNULL(sintOrder,0)) AS numRowNumber,0
from item                                
INNER join ItemDetails Dtl on numChildItemID=numItemCode
where  numItemKitID=@numKitId 

UNION ALL

select Dtl.numItemKitID,i.numItemCode,i.vcItemName,i.monAverageCost,i.numAssetChartAcntId,isnull(Dtl.numWareHouseItemId,0) as numWarehouseItmsID,ISNULL(Dtl.vcItemDesc,i.txtItemDesc) AS txtItemDesc,DTL.numQtyItemsReq,0 as numOppChildItemID,
case when i.charItemType='P' then 'Inventory Item' when i.charItemType='S' then 'Service' when i.charItemType='A' then 'Accessory' when i.charItemType='N' then 'Non-Inventory Item' end as charItemType,i.charItemType as ItemType                            
,c.StageLevel + 1,(CASE WHEN i.charItemType='P' THEN (SELECT monWListPrice FROM WarehouseItems WHERE numWareHouseItemID= dtl.numWareHouseItemId) ELSE i.monListPrice END) monListPrice,ISNULL(i.numBaseUnit,0),CAST((DTL.numQtyItemsReq * c.numCalculatedQty) AS NUMERIC(9,0)) AS numCalculatedQty,ISNULL(Dtl.numUOMId,ISNULL(i.numBaseUnit,0)) AS numIDUOMId,ISNULL(Dtl.sintOrder,0) sintOrder,
ROW_NUMBER() OVER (ORDER BY ISNULL(Dtl.sintOrder,0)) AS numRowNumber,0
from item i                               
INNER JOIN ItemDetails Dtl on Dtl.numChildItemID=i.numItemCode
INNER JOIN CTE c ON Dtl.numItemKitID = c.numItemCode 
where Dtl.numChildItemID!=@numKitId
)

SELECT * INTO #temp FROM CTE

;WITH Final AS (SELECT *,1 AS RStageLevel1 FROM #temp WHERE numitemcode NOT IN (SELECT numItemKitID FROM #temp)

UNION ALL

SELECT t.*,c.RStageLevel1 + 1 AS RStageLevel1 FROM  #temp t JOIN Final c ON t.numitemcode=c.numItemKitID
)

--SELECT DISTINCT * FROM Total ORDER BY StageLevel

UPDATE t set t.RStageLevel=f.RStageLevel from 
#temp t,(SELECT numitemcode,numItemKitID,MAX(RStageLevel1) AS RStageLevel FROM Final GROUP BY numitemcode,numItemKitID) f
WHERE t.numitemcode=f.numitemcode AND t.numItemKitID=f.numItemKitID 

--SELECT * FROM #temp

SELECT DISTINCT c.*,CASE WHEN isnull(c.numWarehouseItmsID,0)>0 THEN ISNULL(WI.[monWListPrice],0) ELSE c.monListPrice END monListPrice, 
convert(varchar(30),CASE WHEN isnull(c.numWarehouseItmsID,0)>0 THEN ISNULL(WI.[monWListPrice],0) ELSE c.monListPrice END)+'/'+isnull(UOM.vcUnitName,'Units') as UnitPrice,UOM.vcUnitName,ISNULL(UOM.numUOMId,0) numUOMId,
WI.numItemID,vcWareHouse,IDUOM.vcUnitName AS vcIDUnitName,
dbo.fn_UOMConversion(IDUOM.numUOMId,0,IDUOM.numDomainId,UOM.numUOMId) AS UOMConversionFactor,
c.numQtyItemsReq * dbo.fn_UOMConversion(UOM.numUOMId,0,IDUOM.numDomainId,IDUOM.numUOMId) AS numConQty,
(c.numQtyItemsReq * dbo.fn_UOMConversion(UOM.numUOMId,0,IDUOM.numDomainId,IDUOM.numUOMId)) * c.monAverageCost AS monAverageCostTotalForQtyRequired,
c.sintOrder,
ISNULL(WI.[numWareHouseItemID],0) AS numWareHouseItemID,ISNULL(numOnHand,0) numOnHand,isnull(numOnOrder,0) numOnOrder,isnull(numReorder,0) numReorder,isnull(numAllocation,0) numAllocation,isnull(numBackOrder,0) numBackOrder
FROM #temp c  
LEFT JOIN UOM ON UOM.numUOMId=c.numBaseUnit
LEFT JOIN UOM IDUOM ON IDUOM.numUOMId=c.numIDUOMId
LEFT OUTER JOIN [WareHouseItems] WI ON WI.[numItemID] = c.numItemCode AND WI.[numWareHouseItemID] = c.numWarehouseItmsID
LEFT OUTER JOIN Warehouses W ON W.numWareHouseID=WI.numWareHouseID ORDER BY c.StageLevel

--SELECT c.*,CASE WHEN isnull(c.numWarehouseItmsID,0)>0 THEN ISNULL(WI.[monWListPrice],0) ELSE c.monListPrice END monListPrice, 
--convert(varchar(30),CASE WHEN isnull(c.numWarehouseItmsID,0)>0 THEN ISNULL(WI.[monWListPrice],0) ELSE c.monListPrice END)+'/'+isnull(UOM.vcUnitName,'Units') as UnitPrice,
--WI.numItemID,vcWareHouse
-- FROM CTE c  
--LEFT JOIN UOM ON UOM.numUOMId=c.numBaseUnit
--LEFT OUTER JOIN [WareHouseItems] WI ON WI.[numItemID] = c.numItemCode AND WI.[numWareHouseItemID] = c.numWarehouseItmsID
--LEFT OUTER JOIN Warehouses W ON W.numWareHouseID=WI.numWareHouseID



GO
--created by anoop jayaraj                                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getdomaindetails')
DROP PROCEDURE usp_getdomaindetails
GO
CREATE PROCEDURE [dbo].[USP_GetDomainDetails]                                          
@numDomainID as numeric(9)=0                                          
as           

DECLARE @listIds VARCHAR(MAX)
SELECT @listIds = COALESCE(@listIds+',' ,'') + CAST(numUserID AS VARCHAR(100)) FROM UnitPriceApprover WHERE numDomainID = @numDomainID
                               
select                                           
vcDomainName,                                           
isnull(vcDomainDesc,'') vcDomainDesc,                                           
isnull(bitExchangeIntegration,0) bitExchangeIntegration,                                           
isnull(bitAccessExchange,0) bitAccessExchange,                                           
isnull(vcExchUserName,'') vcExchUserName,                                           
isnull(vcExchPassword,'') vcExchPassword,                                           
isnull(vcExchPath,'') vcExchPath,                                           
isnull(vcExchDomain,'') vcExchDomain,                                         
tintCustomPagingRows,                                         
vcDateFormat,                                         
numDefCountry,                                         
tintComposeWindow,                                         
sintStartDate,                                         
sintNoofDaysInterval,                                         
tintAssignToCriteria,                                         
bitIntmedPage,                                         
tintFiscalStartMonth,                                    
--isnull(bitDeferredIncome,0) as bitDeferredIncome,                                  
isnull(tintPayPeriod,0) as tintPayPeriod,                                
isnull(numCurrencyID,0) as numCurrencyID,                            
isnull(vcCurrency,'') as vcCurrency,                      
isnull(charUnitSystem,'') as charUnitSystem ,                    
isnull(vcPortalLogo,'') as vcPortalLogo,                  
isnull(tintChrForComSearch,2) as tintChrForComSearch,                
isnull(tintChrForItemSearch,'') as tintChrForItemSearch,            
isnull(intPaymentGateWay,0) as intPaymentGateWay  ,        
 case when bitPSMTPServer= 1 then vcPSMTPServer else '' end as vcPSMTPServer  ,              
 case when bitPSMTPServer= 1 then  isnull(numPSMTPPort,0)  else 0 end as numPSMTPPort          
,isnull(bitPSMTPAuth,0) bitPSMTPAuth        
,isnull([vcPSmtpPassword],'')vcPSmtpPassword        
,isnull(bitPSMTPSSL,0) bitPSMTPSSL     
,isnull(bitPSMTPServer,0) bitPSMTPServer  ,isnull(vcPSMTPUserName,'') vcPSMTPUserName,      
--isnull(numDefaultCategory,0) numDefaultCategory,
isnull(numShipCompany,0)   numShipCompany,
isnull(bitAutoPopulateAddress,0) bitAutoPopulateAddress,
isnull(tintPoulateAddressTo,0) tintPoulateAddressTo,
isnull(bitMultiCompany,0) bitMultiCompany,
isnull(bitMultiCurrency,0) bitMultiCurrency,
ISNULL(bitCreateInvoice,0) bitCreateInvoice,
ISNULL([numDefaultSalesBizDocId],0) numDefaultSalesBizDocId,
ISNULL([bitSaveCreditCardInfo],0) bitSaveCreditCardInfo,
ISNULL([intLastViewedRecord],10) intLastViewedRecord,
ISNULL([tintCommissionType],1) tintCommissionType,
ISNULL([tintBaseTax],2) tintBaseTax,
ISNULL([numDefaultPurchaseBizDocId],0) numDefaultPurchaseBizDocId,
ISNULL(bitEmbeddedCost,0) bitEmbeddedCost,
ISNULL(numDefaultSalesOppBizDocId,0) numDefaultSalesOppBizDocId,
ISNULL(tintSessionTimeOut,1) tintSessionTimeOut,
ISNULL(tintDecimalPoints,2) tintDecimalPoints,
ISNULL(bitCustomizePortal,0) bitCustomizePortal,
ISNULL(tintBillToForPO,0) tintBillToForPO,
ISNULL(tintShipToForPO,0) tintShipToForPO,
ISNULL(tintOrganizationSearchCriteria,1) tintOrganizationSearchCriteria,
ISNULL(tintLogin,0) tintLogin,
lower(ISNULL(vcPortalName,'')) vcPortalName,
ISNULL(bitDocumentRepositary,0) bitDocumentRepositary,
D.numDomainID,
ISNULL(vcPSMTPDisplayName,'') vcPSMTPDisplayName,
ISNULL(bitGtoBContact,0) bitGtoBContact,
ISNULL(bitBtoGContact,0) bitBtoGContact,
ISNULL(bitGtoBCalendar,0) bitGtoBCalendar,
ISNULL(bitBtoGCalendar,0) bitBtoGCalendar,
ISNULL(bitExpenseNonInventoryItem,0) bitExpenseNonInventoryItem,
ISNULL(bitInlineEdit,0) bitInlineEdit,
ISNULL(bitRemoveVendorPOValidation,0) bitRemoveVendorPOValidation,
isnull(D.tintBaseTaxOnArea,0) tintBaseTaxOnArea,
ISNULL(D.bitAmountPastDue,0) bitAmountPastDue,
CONVERT(DECIMAL(18,2),ISNULL(D.monAmountPastDue,0)) monAmountPastDue,
ISNULL(bitSearchOrderCustomerHistory,0) bitSearchOrderCustomerHistory,
isnull(D.bitRentalItem,0) as bitRentalItem,
isnull(D.numRentalItemClass,0) as numRentalItemClass,
isnull(D.numRentalHourlyUOM,0) as numRentalHourlyUOM,
isnull(D.numRentalDailyUOM,0) as numRentalDailyUOM,
isnull(D.tintRentalPriceBasedOn,0) as tintRentalPriceBasedOn,
isnull(D.tintCalendarTimeFormat,24) as tintCalendarTimeFormat,
isnull(D.tintDefaultClassType,0) as tintDefaultClassType,
ISNULL(D.tintPriceBookDiscount,0) tintPriceBookDiscount,
ISNULL(D.bitAutoSerialNoAssign,0) as bitAutoSerialNoAssign,
ISNULL(D.bitIsShowBalance,0) AS [bitIsShowBalance],
ISNULL(numIncomeAccID,0) AS [numIncomeAccID],
ISNULL(numCOGSAccID,0) AS [numCOGSAccID],
ISNULL(numAssetAccID,0) AS [numAssetAccID],
ISNULL(IsEnableClassTracking,0) AS [IsEnableClassTracking],
ISNULL(IsEnableProjectTracking,0) AS [IsEnableProjectTracking],
ISNULL(IsEnableCampaignTracking,0) AS [IsEnableCampaignTracking],
ISNULL(IsEnableResourceScheduling,0) AS [IsEnableResourceScheduling],
ISNULL(numShippingServiceItemID,0) AS [numShippingServiceItemID],
ISNULL(D.numSOBizDocStatus,0) AS numSOBizDocStatus,
ISNULL(D.bitAutolinkUnappliedPayment,0) AS bitAutolinkUnappliedPayment,
ISNULL(D.numDiscountServiceItemID,0) AS numDiscountServiceItemID,
ISNULL(D.vcShipToPhoneNo,0) AS vcShipToPhoneNo,
ISNULL(D.vcGAUserEMail,'') AS vcGAUserEMail,
ISNULL(D.vcGAUserPassword,'') AS vcGAUserPassword,
ISNULL(D.vcGAUserProfileId,0) AS vcGAUserProfileId,
ISNULL(D.bitDiscountOnUnitPrice,0) AS bitDiscountOnUnitPrice,
ISNULL(D.intShippingImageWidth,0) AS intShippingImageWidth ,
ISNULL(D.intShippingImageHeight,0) AS intShippingImageHeight,
ISNULL(D.numTotalInsuredValue,0) AS numTotalInsuredValue,
ISNULL(D.numTotalCustomsValue,0) AS numTotalCustomsValue,
ISNULL(D.vcHideTabs,'') AS vcHideTabs,
ISNULL(D.bitUseBizdocAmount,0) AS bitUseBizdocAmount,
ISNULL(D.IsEnableUserLevelClassTracking,0) AS IsEnableUserLevelClassTracking,
ISNULL(D.bitDefaultRateType,0) AS bitDefaultRateType,
ISNULL(D.bitIncludeTaxAndShippingInCommission,0) AS bitIncludeTaxAndShippingInCommission,
/*,ISNULL(D.bitAllowPPVariance,0) AS bitAllowPPVariance*/
ISNULL(D.bitPurchaseTaxCredit,0) AS bitPurchaseTaxCredit,
ISNULL(D.bitLandedCost,0) AS bitLandedCost,
ISNULL(D.vcLanedCostDefault,'') AS vcLanedCostDefault,
ISNULL(D.bitMinUnitPriceRule,0) AS bitMinUnitPriceRule,
ISNULL(D.numAbovePercent,0) AS numAbovePercent,
ISNULL(D.numAbovePriceField,0) AS numAbovePriceField,
ISNULL(D.numBelowPercent,0) AS numBelowPercent,
ISNULL(D.numBelowPriceField,0) AS numBelowPriceField,
ISNULL(D.numOrderStatusBeforeApproval,0) AS numOrderStatusBeforeApproval,
ISNULL(D.numOrderStatusAfterApproval,0) AS numOrderStatusAfterApproval,
@listIds AS vcUnitPriceApprover,
ISNULL(D.numDefaultSalesPricing,2) AS numDefaultSalesPricing,
ISNULL(D.bitReOrderPoint,0) AS bitReOrderPoint,
ISNULL(D.numReOrderPointOrderStatus,0) AS numReOrderPointOrderStatus
from Domain D  
left join eCommerceDTL eComm  
on eComm.numDomainID=D.numDomainID  
where (D.numDomainID=@numDomainID OR @numDomainID=-1)


GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetIncomeExpenseStatementNew_Kamal' ) 
    DROP PROCEDURE USP_GetIncomeExpenseStatementNew_Kamal
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE USP_GetIncomeExpenseStatementNew_Kamal
@numDomainId AS NUMERIC(9),                                          
@dtFromDate AS DATETIME = NULL,                                        
@dtToDate AS DATETIME = NULL,
@ClientTimeZoneOffset INT, --Added by Chintan to enable calculation of date according to client machine   
@numAccountClass AS NUMERIC(9)=0,
@DateFilter AS VARCHAR(20),
@ReportColumn AS VARCHAR(20)

AS                                                          
BEGIN 

	DECLARE @FinancialYearStartDate AS DATETIME
	SELECT @FinancialYearStartDate = dbo.GetFiscalStartDate(YEAR(GETDATE()),@numDomainId)

	DECLARE @TEMPQuarter AS TABLE
	(
		ID INT,
		StartDate DATETIME,
		EndDate DATETIME
	)

	INSERT INTO @TEMPQuarter VALUES (1,@FinancialYearStartDate,DATEADD(ms,-3,DATEADD(QUARTER,1,@FinancialYearStartDate)))
	INSERT INTO @TEMPQuarter VALUES (2,DATEADD(QUARTER,1,@FinancialYearStartDate),DATEADD(ms,-3,DATEADD(QUARTER,2,@FinancialYearStartDate)))
	INSERT INTO @TEMPQuarter VALUES (3,DATEADD(QUARTER,2,@FinancialYearStartDate),DATEADD(ms,-3,DATEADD(QUARTER,3,@FinancialYearStartDate)))
	INSERT INTO @TEMPQuarter VALUES (4,DATEADD(QUARTER,3,@FinancialYearStartDate),DATEADD(ms,-3,DATEADD(QUARTER,4,@FinancialYearStartDate)))

	IF @DateFilter = 'CurYear'
	BEGIN
		SELECT @dtFromDate = @FinancialYearStartDate,
		   @dtToDate = DATEADD(ms,-3,DATEADD(YEAR,1,@FinancialYearStartDate));
	END
	ELSE IF @DateFilter = 'PreYear'
	BEGIN
		SELECT @dtFromDate = DATEADD(YEAR,-1,@FinancialYearStartDate),
		   @dtToDate = DATEADD(ms,-3,DATEADD(YEAR,1,DATEADD(YEAR,-1,@FinancialYearStartDate)));
	END
	ELSE IF @DateFilter = 'CurPreYear'
	BEGIN
		SELECT @dtFromDate = DATEADD(YEAR,-1,@FinancialYearStartDate),
		   @dtToDate = DATEADD(ms,-3,DATEADD(YEAR,1,@FinancialYearStartDate));
	END
	ELSE IF @DateFilter = 'CuQur'
	BEGIN
		SELECT @dtFromDate = StartDate, @dtToDate=EndDate FROM @TEMPQuarter WHERE GetDate() BETWEEN StartDate AND EndDate
	END
	ELSE IF @DateFilter = 'CurPreQur'
	BEGIN
		SELECT @dtFromDate = DATEADD(QUARTER,-1,StartDate), @dtToDate=EndDate FROM @TEMPQuarter WHERE GetDate() BETWEEN StartDate AND EndDate
	END
	ELSE IF @DateFilter = 'PreQur'
	BEGIN
		SELECT @dtFromDate = DATEADD(QUARTER,-1,StartDate), @dtToDate=DATEADD(QUARTER,-1,EndDate) FROM @TEMPQuarter WHERE GetDate() BETWEEN StartDate AND EndDate
	END
	ELSE IF @DateFilter = 'LastMonth'
	BEGIN
		select @dtFromDate = DATEADD(mm,-1,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)),
				@dtToDate = DATEADD(ms,-3,DATEADD(mm,0,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)));  
	END
	ELSE IF @DateFilter = 'ThisMonth'
	BEGIN
		select @dtFromDate = DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0),
				@dtToDate = DATEADD(ms,- 3,DATEADD(mm,0,DATEADD(mm,DATEDIFF(mm,0,GETDATE())+1,0)));  
	END
	ELSE IF @DateFilter = 'CurPreMonth'
	BEGIN
		select @dtFromDate = DATEADD(mm,-1,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)),
				@dtToDate = DATEADD(ms,- 3,DATEADD(mm,0,DATEADD(mm,DATEDIFF(mm,0,GETDATE())+1,0))); 
	END
 

;WITH DirectReport (ParentId, numAccountTypeID, vcAccountType,vcAccountCode, LEVEL,Struc)
AS
(
  -- anchor
  SELECT CAST(-1 AS NUMERIC(18)) AS ParentId, [ATD].[numAccountTypeID], [ATD].[vcAccountType],[ATD].[vcAccountCode],
   1 AS LEVEL, cast(cast([ATD].[vcAccountCode] AS varchar) AS varchar (100))  AS Struc
  FROM [dbo].[AccountTypeDetail] AS ATD
  WHERE [ATD].[numDomainID]=@numDomainId AND  [ATD].[vcAccountCode] IN('0103','0104','0106')
  UNION ALL
  -- recursive
  SELECT ATD.[numParentID] AS ParentId, [ATD].[numAccountTypeID], [ATD].[vcAccountType],[ATD].[vcAccountCode],
   LEVEL + 1, cast(d.Struc + '#' + cast([ATD].[vcAccountCode] AS varchar) AS varchar(100))  AS Struc
  FROM [dbo].[AccountTypeDetail] AS ATD JOIN [DirectReport] D ON D.[numAccountTypeID] = [ATD].[numParentID]
  WHERE [ATD].[numDomainID]=@numDomainId 
  ),
  DirectReport1 (ParentId, numAccountTypeID, vcAccountType,vcAccountCode, LEVEL,numAccountId,Struc)
AS
(
  -- anchor
  SELECT ParentId, numAccountTypeID, vcAccountType,vcAccountCode, LEVEL ,CAST(NULL AS NUMERIC(18)),Struc
  FROM DirectReport 
  UNION ALL
  -- recursive
  SELECT COA.numParntAcntTypeId AS ParentId,CAST(NULL AS NUMERIC(18)), COA.[vcAccountName],COA.[vcAccountCode],
   LEVEL + 1,[COA].[numAccountId], cast(d.Struc + '#' + cast(COA.[vcAccountCode] AS varchar) AS varchar(100))  AS Struc
  FROM [dbo].[Chart_of_Accounts] AS COA JOIN [DirectReport1] D ON D.[numAccountTypeID] = COA.[numParntAcntTypeId]
  WHERE [COA].[numDomainID]=@numDomainId AND COA.bitActive = 1
  )

  
SELECT ParentId, numAccountTypeID, vcAccountType, LEVEL, vcAccountCode,numAccountId,Struc INTO #tempDirectReport
FROM DirectReport1 

INSERT INTO #tempDirectReport
SELECT 0,-1,'Ordinary Income/Expense',0,NULL,NULL,'-1'
UNION ALL
SELECT 0,-2,'Other Income and Expenses',0,NULL,NULL,'-2'

UPDATE #tempDirectReport SET Struc='-1#' + Struc WHERE [vcAccountCode] NOT LIKE '010302%' AND 
[vcAccountCode] NOT LIKE '010402%' 

UPDATE #tempDirectReport SET Struc='-2#' + Struc WHERE [vcAccountCode] LIKE '010302%' OR 
[vcAccountCode] LIKE '010402%'

UPDATE #tempDirectReport SET [ParentId]=-2 WHERE [vcAccountCode] IN('010302','010402')


Select COA.ParentId, COA.numAccountTypeID, COA.vcAccountType, COA.LEVEL, COA.vcAccountCode, COA.numAccountId, COA.Struc,
CASE WHEN COA.vcAccountCode LIKE '010301%' OR COA.vcAccountCode LIKE '010302%' THEN (ISNULL(Credit,0) - ISNULL(Debit,0))
     ELSE (ISNULL(Debit,0) - ISNULL(Credit,0)) END AS Amount,V.datEntry_Date
INTO #tempViewData
FROM #tempDirectReport COA JOIN View_Journal V ON  /*V.[numAccountId] = COA.[numAccountId]*/ V.COAvcAccountCode like COA.vcAccountCode + '%' 
AND V.numDomainID=@numDomainId AND datEntry_Date BETWEEN  @dtFromDate AND @dtToDate
AND (V.numAccountClass=@numAccountClass OR @numAccountClass=0) 
WHERE COA.[numAccountId] IS NOT NULL

DECLARE @columns VARCHAR(8000);--SET @columns = '';
DECLARE @SUMColumns VARCHAR(8000);--SET @SUMColumns = '';
DECLARE @PivotColumns VARCHAR(8000);--SET @PivotColumns = '';
DECLARE @Select VARCHAR(8000)
DECLARE @Where VARCHAR(8000)
SET @Select = 'SELECT ParentId, numAccountTypeID, vcAccountType, LEVEL, vcAccountCode, numAccountId,(''#'' + Struc + ''#'') AS Struc'

IF @ReportColumn = 'Year' OR @ReportColumn = 'Quarter'
BEGIN
; WITH CTE AS (
	SELECT
		(CASE WHEN @ReportColumn = 'Quarter' THEN dbo.GetFiscalyear(@dtFromDate,@numDomainId) ELSE YEAR(@dtFromDate) END) AS 'yr',
		MONTH(@dtFromDate) AS 'mm',
		DATENAME(mm, @dtFromDate) AS 'mon',
		DATEPART(d,@dtFromDate) AS 'dd',
		dbo.GetFiscalQuarter(@dtFromDate,@numDomainId) AS 'qq',
		@dtFromDate 'new_date'
	UNION ALL
	SELECT
		(CASE WHEN @ReportColumn = 'Quarter' THEN dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId) ELSE YEAR(DATEADD(d,1,new_date)) END) AS 'yr',
		MONTH(DATEADD(d,1,new_date)) AS 'mm',
		DATENAME(mm, DATEADD(d,1,new_date)) AS 'mon',
		DATEPART(d,@dtFromDate) AS 'dd',
		dbo.GetFiscalQuarter(DATEADD(d,1,new_date),@numDomainId) AS 'qq',
		DATEADD(d,1,new_date) 'new_date'
	FROM CTE
	WHERE DATEADD(d,1,new_date) < @dtToDate
	)
	
SELECT yr AS 'Year1',qq AS 'Quarter1', mm as 'Month1', mon AS 'MonthName1', count(dd) AS 'Days1' into #tempYearMonth
FROM CTE
GROUP BY mon, yr, mm, qq
ORDER BY yr, mm, qq
OPTION (MAXRECURSION 5000)

	IF @ReportColumn = 'Year'
		BEGIN
		SELECT
			@columns = COALESCE(@columns + ',ISNULL([' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + '],0) AS [' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + ']',
			',ISNULL([' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + '],0) AS [' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + ']'),
			@SUMColumns = COALESCE(@SUMColumns + '+ ISNULL([' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + '],0)',
			',ISNULL([' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + '],0)'),
			@PivotColumns = COALESCE(@PivotColumns + ',[' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + ']',
			'[' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + ']')
		FROM #tempYearMonth ORDER BY Year1,MONTH1

		SET @SUMColumns = @SUMColumns + ' AS Total, (CASE WHEN ISNULL(numAccountId,0) > 0 THEN 1 ELSE 2 END) [Type]'
		SET @Where = '	FROM (Select COA.ParentId, COA.numAccountTypeID, COA.vcAccountType, COA.LEVEL, COA.vcAccountCode, COA.numAccountId,COA.Struc,
				SUM(Case 
					When (COA.numAccountTypeID = -1 OR COA.numAccountTypeID = -2)
					THEN (Case WHEN V.vcAccountCode LIKE ''0104%'' OR V.vcAccountCode LIKE ''0106%'' THEN Amount * -1
					ELSE Amount END)
 				ELSE Amount END) AS Amount,
				DATENAME(mm,datEntry_Date) + '' '' + CAST(YEAR(datEntry_Date) AS VARCHAR(4)) AS MonthYear 
				FROM #tempDirectReport COA LEFT OUTER JOIN #tempViewData V ON  
				V.Struc like COA.Struc + ''%'' 
				GROUP BY YEAR(datEntry_Date),MONTH(datEntry_Date),DATENAME(mm,datEntry_Date),COA.ParentId, COA.numAccountTypeID, COA.vcAccountType, COA.LEVEL, COA.vcAccountCode, COA.numAccountId,COA.Struc) AS t
				PIVOT
				(
				  SUM(Amount)
				  FOR [MonthYear] IN( ' + @PivotColumns +  ')
				) AS p ORDER BY Struc'

	END
	Else IF @ReportColumn = 'Quarter'
		BEGIN
		SELECT
		    @columns = COALESCE(@columns + ',ISNULL([Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + '],0) AS [Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + ']',
			',ISNULL([Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + '],0) AS [Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + ']'),
			@SUMColumns = COALESCE(@SUMColumns + '+ ISNULL([Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + '],0)',
			',ISNULL([Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + '],0)'),
			@PivotColumns = COALESCE(@PivotColumns + ',[Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + ']',
			'[Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + ']')
		FROM (SELECT DISTINCT Year1,Quarter1  FROM #tempYearMonth)T
		ORDER BY Year1,Quarter1

		SET @SUMColumns = @SUMColumns + ' AS Total, (CASE WHEN ISNULL(numAccountId,0) > 0 THEN 1 ELSE 2 END) [Type]'
		SET @Where = '	FROM (Select COA.ParentId, COA.numAccountTypeID, COA.vcAccountType, COA.LEVEL, COA.vcAccountCode, COA.numAccountId,COA.Struc,
				SUM(Case 
						When(COA.numAccountTypeID = -1 OR COA.numAccountTypeID = -2) 
						THEN (Case WHEN V.vcAccountCode LIKE ''0104%'' OR V.vcAccountCode LIKE ''0106%'' THEN Amount * -1
						ELSE Amount END)
					ELSE Amount END) AS Amount,
				''Q'' + cast(dbo.GetFiscalQuarter(datEntry_Date,' + CAST(@numDomainID AS VARCHAR(20)) + ') as varchar) + '' '' + CAST(dbo.GetFiscalyear(datEntry_Date,' + CAST(@numDomainID AS VARCHAR(20)) + ') AS VARCHAR(4)) AS MonthYear 
				FROM #tempDirectReport COA LEFT OUTER JOIN #tempViewData V ON  
				V.Struc like COA.Struc + ''%'' 
				GROUP BY dbo.GetFiscalyear(datEntry_Date,' + CAST(@numDomainID AS VARCHAR(20)) + '),dbo.GetFiscalQuarter(datEntry_Date,' + CAST(@numDomainID AS VARCHAR(20)) + '),COA.ParentId, COA.numAccountTypeID, COA.vcAccountType, COA.LEVEL, COA.vcAccountCode, COA.numAccountId,COA.Struc) AS t
				PIVOT
				(
				  SUM(Amount)
				  FOR [MonthYear] IN( ' + @PivotColumns +  ')
				) AS p ORDER BY Struc'
	END

	DROP TABLE #tempYearMonth
END
ELSE
BEGIN
	SET @columns = ',ISNULL(Amount,0) as Total, (CASE WHEN ISNULL(numAccountId,0) > 0 THEN 1 ELSE 2 END) [Type]';
	SET @SUMColumns = '';
	SET @PivotColumns = '';
	SET @Where = ' FROM (Select COA.ParentId, COA.numAccountTypeID, COA.vcAccountType, COA.LEVEL, COA.vcAccountCode, COA.numAccountId,COA.Struc,
				SUM(Case 
						When (COA.numAccountTypeID = -1 OR COA.numAccountTypeID = -2)
						THEN (Case WHEN V.vcAccountCode LIKE ''0104%'' OR V.vcAccountCode LIKE ''0106%'' THEN Amount * -1
						ELSE Amount END)
					ELSE Amount END) AS Amount
				FROM #tempDirectReport COA LEFT OUTER JOIN #tempViewData V ON  
				V.Struc like COA.Struc + ''%'' 
				GROUP BY COA.ParentId, COA.numAccountTypeID, COA.vcAccountType, COA.LEVEL, COA.vcAccountCode, COA.numAccountId,COA.Struc) AS t
				ORDER BY Struc'
END

PRINT @Select
PRINT @columns
PRINT @SUMColumns
PRINT @Where


EXECUTE (@Select + @columns + @SUMColumns  + ' ' + @Where)

DROP TABLE #tempViewData 
DROP TABLE #tempDirectReport

END
GO
/****** Object:  StoredProcedure [dbo].[USP_GetItemsAndKits]    Script Date: 07/26/2008 16:17:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getitemsandkits')
DROP PROCEDURE usp_getitemsandkits
GO
CREATE PROCEDURE [dbo].[USP_GetItemsAndKits]        
@numDomainID as numeric(9)=0,
@strItem as varchar(10),
@type AS int,
@bitAssembly AS BIT = 0,
@numItemCode AS NUMERIC(18,0) = 0      
as   
IF @type=0 
BEGIN   
	select numItemCode,vcItemName from Item where numDomainID =@numDomainID  and vcItemName like @strItem+'%'
END

ELSE IF @type=1 
BEGIN   
	IF (SELECT ISNULL(bitAssembly,0) FROM Item  WHERE numItemCode = @numItemCode) = 1
	BEGIN
		--ONLY NON INVENTORY, SERVICE AND INVENTORY ITEMS WITH PRIMARY VENDOR CAN BE ADDED TO ASSEMBLY
		SELECT 
			numItemCode,vcItemName,bitKitParent,bitAssembly 
		FROM 
			Item 
		WHERE 
			Item.numDomainID =@numDomainID
			AND vcItemName like @strItem+'%' 
			AND ISNULL(bitLotNo,0)<>1 
			AND ISNULL(bitSerialized,0)<>1 
			AND ISNULL(bitKitParent,0)<>1 /*and ISNULL(bitAssembly,0)<>1 and  */
			AND ISNULL(bitRental,0) <> 1
			AND ISNULL(bitAsset,0) <> 1
			AND 1=(CASE 
				   WHEN charitemtype='P' THEN
						CASE 
							WHEN ((SELECT COUNT(numWareHouseItemID) FROM WareHouseItems WHERE numDomainID=Item.numDomainID AND numItemID=Item.numItemCode) > 0 AND 
							     ((SELECT COUNT(numDivisionID) FROM DivisionMaster WHERE numDivisionID=Item.numVendorID) > 0 OR ISNULL(bitAssembly,0) = 1)) THEN 1 
							ELSE 
								0 
						END 
					ELSE 
						1 
					END) 
			AND numItemCode NOT IN (@numItemCode)
	END
	ELSE
	BEGIN
		select numItemCode,vcItemName,bitKitParent,bitAssembly from Item where numDomainID =@numDomainID
		AND vcItemName like @strItem+'%' 
		AND ISNULL(bitLotNo,0)<>1 and ISNULL(bitSerialized,0)<>1 
		AND ISNULL(bitKitParent,0)<>1 /*and ISNULL(bitAssembly,0)<>1 and  */
		AND 1=(CASE WHEN charitemtype='P' THEN
				CASE WHEN (SELECT COUNT(numWareHouseItemID) FROM WareHouseItems WHERE numDomainID=Item.numDomainID 
				AND numItemID=Item.numItemCode)>0 THEN 1 ELSE 0 END ELSE 1 END)
	END
END				
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetItemVendorDetails')
DROP PROCEDURE USP_GetItemVendorDetails
GO
CREATE PROCEDURE USP_GetItemVendorDetails
(
	@numVendorID BIGINT,
	@numDomainID NUMERIC(18),
	@numItemCode NUMERIC(18) = 0
)
AS 
BEGIN

	SELECT  
		ISNULL(V.numVendorTcode,0) AS [numVendorTcode],
		ISNULL(V.numVendorID,0) AS [numVendorID],
		ISNULL(V.vcPartNo,'') AS [vcPartNo],
		ISNULL(V.monCost,0) AS [monCost],
		ISNULL(V.numItemCode,0) AS [numItemCode],
		ISNULL(V.intMinQty,0) AS [intMinQty]
	FROM  
		dbo.Vendor V	
	WHERE 
		V.numVendorID = @numVendorID 
		AND V.numDomainID = @numDomainID
		AND V.numItemCode = @numItemCode
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetMirrorBizDocItems' ) 
    DROP PROCEDURE USP_GetMirrorBizDocItems
GO
--- EXEC USP_GetMirrorBizDocItems 53,7,1
CREATE PROCEDURE [dbo].[USP_GetMirrorBizDocItems]
    (
      @numReferenceID NUMERIC(9) = NULL,
      @numReferenceType TINYINT = NULL,
      @numDomainID AS NUMERIC(9) = 0      
    )
AS 

  IF @numReferenceType = 1 OR @numReferenceType = 2 OR @numReferenceType = 3 OR @numReferenceType = 4  
            BEGIN
    			EXEC USP_MirrorOPPBizDocItems @numReferenceID,@numDomainID
END
ELSE
BEGIN
    DECLARE @DivisionID AS NUMERIC(9)                                                                                                                                                                                                                                                                                                                                            
    DECLARE @tintReturnType AS NUMERIC(9)                                                                                                                                                                                                                                                                                                                                            
    
    SELECT  @DivisionID = numDivisionID,@tintReturnType=tintReturnType
    FROM    dbo.ReturnHeader
    WHERE   numReturnHeaderID = @numReferenceID                                                                                                                                                                          
--Decimal points to show in bizdoc 
    DECLARE @DecimalPoint AS TINYINT
    SELECT  @DecimalPoint = tintDecimalPoints
    FROM    domain
    WHERE   numDomainID = @numDomainID
                                                                                          
    DECLARE @strSQL AS VARCHAR(MAX)                                                                                          
    DECLARE @strSQL1 AS VARCHAR(8000)                                       
    DECLARE @strSQLCusFields VARCHAR(1000)                      
    DECLARE @strSQLEmpFlds VARCHAR(500)                                                                         
    SET @strSQLCusFields = ''                                                                                          
    SET @strSQLEmpFlds = ''                                                                                          
    DECLARE @tintType AS TINYINT   
    
    DECLARE @intRowNum AS INT                                                  
    DECLARE @numFldID AS VARCHAR(15)                                          
    DECLARE @vcFldname AS VARCHAR(50)                                            
    DECLARE @numBizDocId AS NUMERIC(9)
    DECLARE @numBizDocTempID AS NUMERIC(9) ;
    SET @numBizDocTempID = 0
        
    IF @numReferenceType = 5 OR @numReferenceType = 6 
        BEGIN
            SELECT  @numBizDocTempID = ISNULL(numRMATempID, 0)
            FROM    dbo.ReturnHeader
            WHERE   numReturnHeaderID = @numReferenceID                       
        END 
    ELSE IF @numReferenceType = 7 OR @numReferenceType = 8  OR @numReferenceType = 9  OR @numReferenceType = 10 
            BEGIN
                SELECT  @numBizDocTempID = ISNULL(numBizdocTempID, 0)
                FROM    dbo.ReturnHeader
                WHERE   numReturnHeaderID = @numReferenceID
            END  
	
    PRINT 'numBizdocTempID : ' + CONVERT(VARCHAR(10), @numBizdocTempID)
    SELECT  @numBizDocId = numBizDocId
    FROM    dbo.BizDocTemplate
    WHERE   numBizDocTempID = @numBizDocTempID
            AND numDomainID = @numDomainID
      
     IF @numReferenceType = 5 
     BEGIN
		SELECT @numBizDocID =  numListItemID FROM dbo.ListDetails WHERE vcData='RMA' AND constFlag=1
		
		SET @tintType = 7   
	 END
	 ELSE IF @numReferenceType = 6 
	 BEGIN
		SELECT @numBizDocID =  numListItemID FROM dbo.ListDetails WHERE vcData='RMA' AND constFlag=1 
		
		SET @tintType = 8   
	 END
	 ELSE IF @numReferenceType = 7 
     BEGIN
		SELECT @numBizDocID =  numListItemID FROM dbo.ListDetails WHERE vcData='Sales Credit Memo' AND constFlag=1 
		
		SET @tintType = 7   
	 END
	 ELSE IF @numReferenceType = 8
     BEGIN
		SELECT @numBizDocID =  numListItemID FROM dbo.ListDetails WHERE vcData='Purchase Credit Memo' AND constFlag=1 
		
		SET @tintType = 8 
	 END
	 ELSE IF @numReferenceType = 9
     BEGIN
		SELECT @numBizDocID =  numListItemID FROM dbo.ListDetails WHERE vcData='Refund Receipt' AND constFlag=1 
		
		SET @tintType = 7   
	 END
	 ELSE IF @numReferenceType = 10
     BEGIN
		SELECT @numBizDocID =  numListItemID FROM dbo.ListDetails WHERE vcData='Credit Memo' AND constFlag=1 
		
		SET @tintType = 7   
	 END
														  

    SELECT  *
    INTO    #Temp1
    FROM    ( SELECT    numReturnItemID,I.vcitemname AS vcItemName,
                        0 OppBizDocItemID,
                        CASE WHEN charitemType = 'P' THEN 'Product'
                             WHEN charitemType = 'S' THEN 'Service'
                        END AS charitemType,
                        RI.vcItemDesc AS txtItemDesc,                                      
--vcitemdesc as [desc],                                      
--                        dbo.fn_UOMConversion(ISNULL(i.numBaseUnit, 0),
--                                             i.numItemCode, @numDomainID,
--                                             ISNULL(RI.numUOMId, 0))
--                        * RI.numUnitHour AS numUnitHour,
						CASE WHEN @numReferenceType=5 OR @numReferenceType=6 THEN RI.numUnitHour ELSE RI.numUnitHourReceived END AS numUnitHour,
                       RI.monPrice,
                        (CASE WHEN @numReferenceType=5 OR @numReferenceType=6 THEN RI.numUnitHour ELSE RI.numUnitHourReceived END) 
                        * RI.monPrice AS  Amount,
                        (CASE WHEN @numReferenceType=5 OR @numReferenceType=6 THEN RI.numUnitHour ELSE RI.numUnitHourReceived END) 
                        * RI.monPrice AS monTotAmount/*Fo calculating sum*/,
                        RH.monTotalTax AS [Tax],
                        ISNULL(CONVERT(VARCHAR, i.monListPrice), 0) AS monListPrice,
                        i.numItemCode AS ItemCode,i.numItemCode,
                        RI.numItemCode AS [numoppitemtCode],
                        L.vcdata AS numItemClassification,
                        CASE WHEN bitTaxable = 0 THEN 'No'
                             ELSE 'Yes'
                        END AS Taxable,
                        numIncomeChartAcntId AS itemIncomeAccount,
                        'NI' AS ItemType,
                        0 AS numJournalId,
                        0 AS numTransactionId,
                        ISNULL(RI.vcModelID, '') vcModelID,
                        dbo.USP_GetAttributes(RI.numWarehouseItemID,
                                              bitSerialized) AS vcAttributes,
                        '' AS vcPartNo,
                        ( ISNULL(RH.monTotalDiscount, RI.monTotAmount)
                          - RI.monTotAmount ) AS DiscAmt,
                        '' AS vcNotes,
                        '' AS vcTrackingNo,
                        '' AS vcShippingMethod,
                        0 AS monShipCost,
                        [dbo].[FormatedDateFromDate](RH.dtCreatedDate,
                                                     @numDomainID) dtDeliveryDate,
                        RI.numWarehouseItemID,
                        ISNULL(u.vcUnitName, '') numUOMId,
                        ISNULL(RI.vcManufacturer, '') AS vcManufacturer,
                        ISNULL(V.monCost, 0) AS VendorCost,
                        '' AS vcPathForTImage,
                        i.[fltLength],
                        i.[fltWidth],
                        i.[fltHeight],
                        i.[fltWeight],
                        ISNULL(I.numVendorID, 0) numVendorID,
                        0 AS DropShip,
                        ISNULL(RI.numUOMId, 0) AS numUOM,
                        ISNULL(u.vcUnitName, '') vcUOMName,
                        dbo.fn_UOMConversion(RI.numUOMId, RI.numItemCode,
                                             @numDomainID, NULL) AS UOMConversionFactor,
                        ISNULL(W.vcWareHouse, '') AS Warehouse,
                        CASE WHEN charitemType = 'P' THEN WI.vcWHSKU
                             ELSE i.vcSKU
                        END vcSKU,
                        CASE WHEN charitemType = 'P' THEN WI.vcBarCode
                             ELSE i.numBarCodeId
                        END vcBarcode,
                        i.numShipClass,
                        [dbo].FormatedDateTimeFromDate(RH.dtCreatedDate,
                                                       @numDomainID) dtRentalStartDate,
                        [dbo].FormatedDateTimeFromDate(RH.dtCreatedDate,
                                                       @numDomainID) dtRentalReturnDate,
                       		SUBSTRING(
	(SELECT ',' + vcSerialNo + CASE WHEN isnull(I.bitLotNo,0)=1 THEN ' (' + CONVERT(VARCHAR(15),oppI.numQty) + ')' ELSE '' end 
	FROM OppWarehouseSerializedItem oppI JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID=whi.numWareHouseItmsDTLID where oppI.numReturnHeaderID=RH.numReturnHeaderID and oppI.numReturnItemID=RI.numReturnItemID
	ORDER BY vcSerialNo FOR XML PATH('')),2,200000) AS SerialLotNo
              FROM      dbo.ReturnHeader RH
                        JOIN dbo.ReturnItems RI ON RH.numReturnHeaderID = RI.numReturnHeaderID
                        LEFT JOIN dbo.Item i ON RI.numItemCode = i.numItemCode
                        LEFT JOIN ListDetails L ON i.numItemClassification = L.numListItemID
                        LEFT JOIN Vendor V ON V.numVendorID = @DivisionID
                                              AND V.numItemCode = RI.numItemCode
                        LEFT JOIN UOM u ON u.numUOMId = RI.numUOMId
                        LEFT JOIN dbo.WareHouseItems WI ON RI.numWarehouseItemID = WI.numWareHouseItemID
                                                           AND WI.numDomainID = @numDomainID
                        LEFT JOIN dbo.Warehouses W ON WI.numDomainID = W.numDomainID
                                                      AND WI.numWareHouseID = W.numWareHouseID
              WHERE     RH.numReturnHeaderID = @numReferenceID
            ) X


	--SELECT * FROM #Temp1
	
    IF @DecimalPoint = 1 
        BEGIN
            UPDATE  #Temp1
            SET     monPrice = CONVERT(DECIMAL(18, 1), monPrice),
                    Amount = CONVERT(DECIMAL(18, 1), Amount)
        END
    IF @DecimalPoint = 2 
        BEGIN
            UPDATE  #Temp1
            SET     monPrice = CONVERT(DECIMAL(18, 2), monPrice),
                    Amount = CONVERT(DECIMAL(18, 2), Amount)
        END
    IF @DecimalPoint = 3 
        BEGIN
            UPDATE  #Temp1
            SET     monPrice = CONVERT(DECIMAL(18, 3), monPrice),
                    Amount = CONVERT(DECIMAL(18, 3), Amount)
        END
    IF @DecimalPoint = 4 
        BEGIN
            UPDATE  #Temp1
            SET     monPrice = CONVERT(DECIMAL(18, 4), monPrice),
                    Amount = CONVERT(DECIMAL(18, 4), Amount)
        END

    DECLARE @strSQLUpdate AS VARCHAR(2000)
    DECLARE @vcTaxName AS VARCHAR(100)
    DECLARE @numTaxItemID AS NUMERIC(9)
    SET @numTaxItemID = 0
    SET @strSQLUpdate = ''
    SELECT TOP 1
            @vcTaxName = vcTaxName,
            @numTaxItemID = numTaxItemID
    FROM    TaxItems
    WHERE   numDomainID = @numDomainID

    EXEC ( 'alter table #Temp1 add [TotalTax] money'
        )

    IF @tintReturnType = 1
        SET @strSQLUpdate = @strSQLUpdate
            + ',[TotalTax]= dbo.fn_CalBizDocTaxAmt('
            + CONVERT(VARCHAR(20), @numDomainID) + ',0,'
            + CONVERT(VARCHAR(20), @numReferenceID)
            + ',numReturnItemID,2)*Amount/100'
    ELSE 
        IF @tintReturnType = 1
            SET @strSQLUpdate = @strSQLUpdate + ',[TotalTax]= 0'
        ELSE 
            IF @tintReturnType = 1
                SET @strSQLUpdate = @strSQLUpdate
                    + ',[TotalTax]= dbo.fn_CalBizDocTaxAmt('
                    + CONVERT(VARCHAR(20), @numDomainID) + ',0,'
                     + CONVERT(VARCHAR(20), @numReferenceID)
                    + ',numReturnItemID,2)*Amount/100'
            ELSE 
                SET @strSQLUpdate = @strSQLUpdate + ',[TotalTax]= 0'

    WHILE @numTaxItemID > 0
        BEGIN

            EXEC ( 'alter table #Temp1 add [' + @vcTaxName + '] money'
                )

            IF @tintReturnType = 1
                SET @strSQLUpdate = @strSQLUpdate + ',[' + @vcTaxName
                    + ']= dbo.fn_CalBizDocTaxAmt('
                    + CONVERT(VARCHAR(20), @numDomainID) + ','
                    + CONVERT(VARCHAR(20), @numTaxItemID) + ','
                    + CONVERT(VARCHAR(20), @numReferenceID) + ',numReturnItemID,2)*Amount/100'
            ELSE 
                SET @strSQLUpdate = @strSQLUpdate + ',[' + @vcTaxName + ']= 0'

            SELECT TOP 1
                    @vcTaxName = vcTaxName,
                    @numTaxItemID = numTaxItemID
            FROM    TaxItems
            WHERE   numDomainID = @numDomainID
                    AND numTaxItemID > @numTaxItemID

            IF @@rowcount = 0 
                SET @numTaxItemID = 0


        END
	
    PRINT 'QUERY  :' + @strSQLUpdate
	
    IF @strSQLUpdate <> '' 
        BEGIN
            SET @strSQLUpdate = 'update #Temp1 set ItemCode=ItemCode'
                + @strSQLUpdate + ' where ItemCode>0'
            PRINT @strSQLUpdate
            EXEC ( @strSQLUpdate
                )
        END

    DECLARE @vcDbColumnName NVARCHAR(50)
    SELECT TOP 1
            @numFldID = numfieldID,
            @vcFldname = vcFieldName,
            @vcDbColumnName = vcDbColumnName,
            @intRowNum = ( tintRow + 1 )
    FROM    View_DynamicCustomColumns
    WHERE   numFormID = @tintType
            AND Grp_id = 5
            AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId
            AND bitCustom = 1
            AND ISNULL(numRelCntType, 0) = @numBizDocTempID
    ORDER BY tintRow                                                                                          
    WHILE @intRowNum > 0                                                                                  
        BEGIN  
            PRINT @vcDbColumnName  
            EXEC
                ( 'alter table #Temp1 add [' + @vcDbColumnName
                  + '] varchar(100)'
                )
	
            SET @strSQLUpdate = 'update #Temp1 set ItemCode=ItemCode,['
                + @vcDbColumnName + ']=dbo.GetCustFldValueItem(' + @numFldID
                + ',ItemCode) where ItemCode>0'
            EXEC ( @strSQLUpdate
                )
		                   
            SELECT TOP 1
                    @numFldID = numfieldID,
                    @vcFldname = vcFieldName,
                    @vcDbColumnName = vcDbColumnName,
                    @intRowNum = ( tintRow + 1 )
            FROM    View_DynamicCustomColumns
            WHERE   numFormID = @tintType
                    AND Grp_id = 5
                    AND numDomainID = @numDomainID
                    AND numAuthGroupID = @numBizDocId
                    AND bitCustom = 1
                    AND tintRow >= @intRowNum
                    AND ISNULL(numRelCntType, 0) = @numBizDocTempID
            ORDER BY tintRow                                                                  
		           
            IF @@rowcount = 0 
                SET @intRowNum = 0                                                                                          
        END


    SELECT  Row_NUmber() OVER ( ORDER BY numoppitemtCode ) RowNumber,
            *
    FROM    #Temp1
    ORDER BY numoppitemtCode ASC 

    DROP TABLE #Temp1

IF (SELECT COUNT(*) FROM View_DynamicColumns WHERE   numFormID = @tintType AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId AND vcFieldType = 'R'AND ISNULL(numRelCntType, 0) = @numBizDocTempID)=0
BEGIN            
	INSERT INTO DycFormConfigurationDetails (numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numAuthGroupID,numRelCntType)
	select numFormId,numFieldId,1,tintRow,@numDomainID,@numBizDocId,@numBizDocTempID
	 FROM View_DynamicDefaultColumns
	 where numFormId=@tintType and bitDefault=1 and numDomainID=@numDomainID order by tintRow ASC	
	 
	 
	  IF(SELECT COUNT(*) FROM DycFrmConfigBizDocsSumm Ds where numBizDocID=@numBizDocId and Ds.numFormID=@tintType 
	and isnull(Ds.numBizDocTempID,0)=@numBizDocTempID and Ds.numDomainID=@numDomainID)=0
	BEGIN
		INSERT INTO DycFrmConfigBizDocsSumm (numFormID,numBizDocID,numFieldID,numDomainID,numBizDocTempID)
		 SELECT @tintType,@numBizDocId,DFM.numFieldID,@numDomainID,@numBizDocTempID FROM
		 DycFormField_Mapping DFFM 
			JOIN DycFieldMaster DFM on DFFM.numModuleID=DFFM.numModuleID and DFFM.numFieldID=DFM.numFieldID
			where numFormID=16 and DFFM.numDomainID is null
	END
	
END 

    SELECT  vcFieldName AS vcFormFieldName,
            vcDbColumnName,
            vcFieldDataType,
            tintRow AS intRowNum
    FROM    View_DynamicColumns
    WHERE   numFormID = @tintType
            AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId
            AND vcFieldType = 'R'
            AND ISNULL(numRelCntType, 0) = @numBizDocTempID
    UNION
    SELECT  vcFieldName AS vcFormFieldName,
            vcDbColumnName,
            'C' vcFieldDataType,
            tintRow AS intRowNum
    FROM    View_DynamicCustomColumns
    WHERE   numFormID = @tintType
            AND Grp_id = 5
            AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId
            AND bitCustom = 1
            AND ISNULL(numRelCntType, 0) = @numBizDocTempID
    ORDER BY tintRow
    
    END
GO
--- EXEC USP_GetMirrorBizDocItems 30,5,1
--created by anoop jayaraj      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetNewOppItemsForReturn')
DROP PROCEDURE USP_GetNewOppItemsForReturn
GO
CREATE PROCEDURE [dbo].[USP_GetNewOppItemsForReturn]      
@tintOppType as tinyint,      
@numDomainID as numeric(9),      
@numDivisionID as numeric(9)=0,      
@str as varchar(20) ,
@numUserCntID as numeric(9),
@tintSearchOrderCustomerHistory as tinyint=0     
as   

--IF @tintOppType=0
--BEGIN
--	SELECT '' AS NoValue
--	SELECT '' AS NoValue
--	RETURN 
--END

 select * into #Temp1 from (select numFieldId,vcDbColumnName,ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName,tintRow AS tintOrder,0 as Custom
    from View_DynamicColumns
   where numFormId=22 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 
        AND bitCustom=0 AND ISNULL(bitSettingField,0)=1 AND numRelCntType=0
union   
 select numFieldId,vcFieldName,vcFieldName,tintRow AS tintOrder,1 as Custom
    from View_DynamicCustomColumns  
   where Grp_id=5 AND numFormId=22 and numUserCntID=@numUserCntID and numDomainID=@numDomainID 
   AND tintPageType=1 AND bitCustom=1 AND numRelCntType=0)X 
  
  if not exists(select * from #Temp1)
  begin
    insert into #Temp1
     select numFieldId,vcDbColumnName,ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName,tintorder,0 from View_DynamicDefaultColumns                                                  
     where numFormId=22 and bitDefault = 1 and ISNULL(bitSettingField,0)=1 and numDomainID=@numDomainID 
  end

Create table #tempItemCode (numItemCode numeric(9))

if @tintOppType>0       
begin 
DECLARE @bitRemoveVendorPOValidation AS BIT

SELECT @bitRemoveVendorPOValidation=ISNULL(bitRemoveVendorPOValidation,0) FROM domain WHERE numDomainID=@numDomainID

Declare @strSQL as varchar(8000)
declare @tintOrder as int
declare @Fld_id as varchar(20)
declare @Fld_Name as varchar(20)
set @strSQL=''
	select top 1 @tintOrder=tintOrder+1,@Fld_id=numFieldId,@Fld_Name=vcFieldName from #Temp1 where Custom=1 order by tintOrder
	while @tintOrder>0
    begin
	set @strSQL=@strSQL+', dbo.GetCustFldValueItem('+@Fld_id+', I.numItemCode) as ['+ @Fld_Name+']'

	select top 1 @tintOrder=tintOrder+1,@Fld_id=numFieldId,@Fld_Name=vcFieldName from #Temp1 where Custom=1 and tintOrder>=@tintOrder order by tintOrder
	if @@rowcount=0 set @tintOrder=0
	end


--Temp table for Item Search Configuration

 select * into #tempSearch from (select numFieldId,vcDbColumnName,ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName,tintRow AS tintOrder,0 as Custom
    from View_DynamicColumns
   where numFormId=22 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 
        AND bitCustom=0  AND ISNULL(bitSettingField,0)=1 AND numRelCntType=1
union   
  select numFieldId,vcFieldName,vcFieldName,tintRow AS tintOrder,1 as Custom
    from View_DynamicCustomColumns   
   where Grp_id=5 AND numFormId=22 and numUserCntID=@numUserCntID and numDomainID=@numDomainID 
   AND tintPageType=1 AND bitCustom=1 AND numRelCntType=1)X 
  
  if not exists(select * from #tempSearch)
  begin
    insert into #tempSearch
    select numFieldId,vcDbColumnName,ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName,tintorder,0 from View_DynamicDefaultColumns                                                  
     where numFormId=22 and bitDefault = 1 and ISNULL(bitSettingField,0)=1 and numDomainID=@numDomainID 
  END

--Regular Search
Declare @strSearch as varchar(8000),@CustomSearch AS VARCHAR(4000),@numFieldId AS NUMERIC(9)
SET @strSearch=''
	select top 1 @tintOrder=tintOrder+1,@Fld_Name=vcDbColumnName,@numFieldId=numFieldId from #tempSearch WHERE Custom=0 order by tintOrder
	while @tintOrder>-1
    BEGIN
	IF @Fld_Name='vcPartNo'
		insert into #tempItemCode SELECT distinct Item.numItemCode FROM Vendor join Item on Item.numItemCode=Vendor.numItemCode 
										where Item.numDomainID=@numDomainID and Vendor.numDomainID=@numDomainID 
										and Vendor.vcPartNo is not null and len(Vendor.vcPartNo)>0 and vcPartNo LIKE '%'+@str+'%'

-- 		set @strSearch=@strSearch+ ' I.numItemCode in (SELECT distinct Item.numItemCode FROM Vendor join Item on Item.numItemCode=Vendor.numItemCode 
--										where Item.numDomainID='+convert(varchar(15),@numDomainID) + ' and Vendor.numDomainID='+convert(varchar(15),@numDomainID) + ' 
--										and Vendor.vcPartNo is not null and len(Vendor.vcPartNo)>0 and vcPartNo LIKE ''%'+@str+'%'')'
	ELSE IF @Fld_Name='vcBarCode'
		insert into #tempItemCode SELECT distinct Item.numItemCode FROM Item join WareHouseItems WI on WI.numDomainID=@numDomainID and Item.numItemCode=WI.numItemID join Warehouses W on W.numDomainID=@numDomainID and W.numWareHouseID=WI.numWareHouseID  
									 where Item.numDomainID=@numDomainID and isnull(Item.numItemGroup,0)>0 and WI.vcBarCode is not null and len(WI.vcBarCode)>0 and WI.vcBarCode LIKE '%'+@str+'%'

-- 		set @strSearch=@strSearch+ ' I.numItemCode in (SELECT distinct Item.numItemCode FROM Item join WareHouseItems WI on WI.numDomainID='+convert(varchar(15),@numDomainID) + ' and Item.numItemCode=WI.numItemID join Warehouses W on W.numDomainID='+convert(varchar(15),@numDomainID) + ' and W.numWareHouseID=WI.numWareHouseID  
--									 where Item.numDomainID='+convert(varchar(15),@numDomainID) + ' and isnull(Item.numItemGroup,0)>0 and WI.vcBarCode is not null and len(WI.vcBarCode)>0 and WI.vcBarCode LIKE ''%'+@str+'%'')'
	ELSE IF @Fld_Name='vcWHSKU'
		insert into #tempItemCode SELECT distinct Item.numItemCode FROM Item join WareHouseItems WI on WI.numDomainID=@numDomainID and Item.numItemCode=WI.numItemID join Warehouses W on W.numDomainID=@numDomainID and W.numWareHouseID=WI.numWareHouseID  
									 where Item.numDomainID=@numDomainID and isnull(Item.numItemGroup,0)>0 and WI.vcWHSKU is not null and len(WI.vcWHSKU)>0 and WI.vcWHSKU LIKE '%'+@str+'%'

-- 		set @strSearch=@strSearch+ ' I.numItemCode in (SELECT distinct Item.numItemCode FROM Item join WareHouseItems WI on WI.numDomainID='+convert(varchar(15),@numDomainID) + ' and Item.numItemCode=WI.numItemID join Warehouses W on W.numDomainID='+convert(varchar(15),@numDomainID) + ' and W.numWareHouseID=WI.numWareHouseID  
--									 where Item.numDomainID='+convert(varchar(15),@numDomainID) + ' and isnull(Item.numItemGroup,0)>0 and WI.vcWHSKU is not null and len(WI.vcWHSKU)>0 and WI.vcWHSKU LIKE ''%'+@str+'%'')'
	else    
		set @strSearch=@strSearch+' ['+ @Fld_Name + '] LIKE ''%'+@str+'%'''

	select top 1 @tintOrder=tintOrder+1,@Fld_Name=vcDbColumnName,@numFieldId=numFieldId from #tempSearch where tintOrder>=@tintOrder AND Custom=0 order by tintOrder
		if @@rowcount=0 
		begin
			set @tintOrder=-1
		END
		ELSE IF @Fld_Name!='vcPartNo' and @Fld_Name!='vcBarCode' and @Fld_Name!='vcWHSKU'
		begin
			set @strSearch=@strSearch + ' or '
		END
	END

IF (select count(*) from #tempItemCode)>0
	set @strSearch=@strSearch + ' or  I.numItemCode in (select numItemCode from #tempItemCode)' 
--Custom Search
SET @CustomSearch=''
select top 1 @tintOrder=tintOrder+1,@Fld_Name=vcDbColumnName,@numFieldId=numFieldId from #tempSearch WHERE Custom=1 order by tintOrder
	while @tintOrder>-1
    BEGIN
    
	set @CustomSearch=@CustomSearch + CAST(@numFieldId AS VARCHAR(10)) 

	select top 1 @tintOrder=tintOrder+1,@Fld_Name=vcDbColumnName,@numFieldId=numFieldId from #tempSearch where tintOrder>=@tintOrder AND Custom=1 order by tintOrder
		if @@rowcount=0 
		begin
			set @tintOrder=-1
		END
		ELSE
		begin
			set @CustomSearch=@CustomSearch + ' , '
		END
	END
	
IF LEN(@CustomSearch)>0
BEGIN
 	SET @CustomSearch= ' I.numItemCode in (SELECT RecId FROM dbo.CFW_FLD_Values_Item WHERE Fld_ID IN (' + @CustomSearch  + ') and Fld_Value like ''%' + @str + '%'')'

IF LEN(@strSearch)>0
       SET @strSearch=@strSearch + ' OR ' +  @CustomSearch
ELSE
       SET @strSearch=  @CustomSearch  
END


if @tintOppType=1 OR (@bitRemoveVendorPOValidation=1 AND @tintOppType=2)       
begin      
  set @strSQL='select TOP 20 I.numItemCode,isnull(vcItemName,'''') vcItemName,CASE WHEN I.[charItemType]=''P'' AND ISNULL(I.bitSerialized,0) = 0 AND  ISNULL(I.bitLotNo,0) = 0 THEN convert(varchar(100),ISNULL((SELECT TOP 1 ROUND(monWListPrice,2) FROM [WareHouseItems] WHERE [numItemID]=I.numItemCode),0)) ELSE CONVERT(VARCHAR(100), ROUND(monListPrice, 2)) END AS monListPrice,isnull(vcSKU,'''') vcSKU,isnull(numBarCodeId,0) numBarCodeId,isnull(vcModelID,'''') vcModelID,isnull(txtItemDesc,'''') as txtItemDesc,isnull(C.vcCompanyName,'''') as vcCompanyName'+@strSQL+' from Item  I     
  Left join DivisionMaster D              
  on I.numVendorID=D.numDivisionID  
  left join CompanyInfo C  
  on C.numCompanyID=D.numCompanyID  
  where ISNULL(I.bitSerialized,0) = 0 AND ISNULL(I.bitLotNo,0) = 0 AND ISNULL(I.Isarchieve,0) <> 1 And I.charItemType NOT IN(''A'') AND  I.numDomainID='+convert(varchar(20),@numDomainID)+' and (' +@strSearch+ ') '         
--- added Asset validation  by sojan

IF @bitRemoveVendorPOValidation=1 AND @tintOppType=2
	set @strSQL=@strSQL + ' and ISNULL(I.bitKitParent,0) = 0 '

if @tintSearchOrderCustomerHistory=1 and @numDivisionID>0
BEGIN
	set @strSQL=@strSQL + ' and I.numItemCode in (select distinct oppI.numItemCode from 
							OpportunityMaster oppM join OpportunityItems oppI on oppM.numOppId=oppI.numOppId where oppM.numDomainID='+convert(varchar(20),@numDomainID)+' and oppM.numDivisionId='+convert(varchar(20),@numDivisionId)+')'
END

set @strSQL=@strSQL + ' order by  vcItemName'
 print @strSQL
 exec (@strSQL)
end      
else if @tintOppType=2       
begin      
 set @strSQL='SELECT TOP 20 I.numItemCode,isnull(vcItemName,'''') vcItemName,convert(varchar(200),round(monListPrice,2)) as monListPrice,isnull(vcSKU,'''') vcSKU,isnull(numBarCodeId,0) numBarCodeId,isnull(vcModelID,'''') vcModelID,isnull(txtItemDesc,'''') as txtItemDesc,isnull(C.vcCompanyName,'''') as vcCompanyName'+@strSQL+'  from item I              
 join Vendor V              
 on V.numItemCode=I.numItemCode   
 Left join DivisionMaster D              
 on V.numVendorID=D.numDivisionID  
 left join CompanyInfo C  
 on C.numCompanyID=D.numCompanyID             
 where ISNULL(I.bitSerialized,0) = 0 AND ISNULL(I.bitLotNo,0) = 0 AND ISNULL(I.Isarchieve,0) <> 1 and ISNULL(I.bitKitParent,0) = 0 And I.charItemType NOT IN(''A'') AND V.numVendorID='+convert(varchar(20),@numDivisionID)+' and (' +@strSearch+ ') ' 
--- added Asset validation  by sojan

if @tintSearchOrderCustomerHistory=1 and @numDivisionID>0
BEGIN
	set @strSQL=@strSQL + ' and I.numItemCode in (select distinct oppI.numItemCode from 
							OpportunityMaster oppM join OpportunityItems oppI on oppM.numOppId=oppI.numOppId where oppM.numDomainID='+convert(varchar(20),@numDomainID)+' and oppM.numDivisionId='+convert(varchar(20),@numDivisionId)+')'
END

set @strSQL=@strSQL + ' order by  vcItemName'
 print @strSQL
 exec (@strSQL)
end
else
select 0  
end
ELSE
	select 0  

select * from  #Temp1 where vcDbColumnName not in ('vcPartNo','vcBarCode','vcWHSKU')ORDER BY tintOrder 

drop table #Temp1
         
drop table #tempItemCode
  
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_GetTrackingDetails' ) 
    DROP PROCEDURE usp_GetTrackingDetails
GO
--created by Joseph
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_GetTrackingDetails]
 @numDomainId AS NUMERIC(9), 
 @numWebApiId AS NUMERIC(9)
AS 
    BEGIN
	
		SELECT AOID.WebApiOrderItemId ,AOID.numDomainId,AOID.numWebApiId,AOID.numOppId,OBI.numItemCode,OBI.numOppBizDocItemID,
		AOID.numOppItemId,vcapiOppId,vcApiOppItemId,OB.numOppBizDocsId,OB.vcTrackingNo ,OB.monShipCost,OB.vcShippingMethod,LD.vcData AS ShippingCarrier,
		(SELECT TOP 1 vcAPIItemID FROM dbo.ItemAPI 
		WHERE numItemID = OBI.numItemCode 
		AND WebApiId = AOID.numWebApiId
		AND 1 = (CASE ISNULL(vcSKU,'') WHEN (SELECT vcSKU FROM Item WHERE numItemCode = numItemID AND numDomainID = @numDomainId ) THEN 1  
									   WHEN (SELECT vcWHSKU FROM dbo.WareHouseItems WI WHERE OBI.numWarehouseItmsID  = WI.numWareHouseItemID ) THEN 1
									   ELSE 0
				 END)  
		) AS vcAPIItemId,
		(SELECT COUNT(*) FROM dbo.OpportunityBizDocItems WHERE numOppBizDocID = OB.numOppBizDocsId AND tintTrackingStatus = 1) AS numTrackingNoUpdated,OBI.numOppBizDocID
		FROM WebApiOppItemDetails AOID 
		INNER JOIN dbo.OpportunityBizDocs OB ON AOID.numOppId  = OB.numOppId AND OB.bitAuthoritativeBizDocs = 1
		INNER JOIN dbo.OpportunityBizDocItems OBI  ON OBI.numOppBizDocID = OB.numOppBizDocsId
		INNER JOIN dbo.ListDetails LD ON LD.numlistItemID = OB.numShipVia

		WHERE  AOID.numWebApiId = @numWebApiId AND AOID.numDomainID = @numDomainId 
		AND len(AOID.vcAPIOppId)>2 AND len(OB.vcTrackingNo)>1
		AND (OBI.tintTrackingStatus = 0 or OBI.tintTrackingStatus IS NULL) 
		
    END
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetTrialBalance')
DROP PROCEDURE USP_GetTrialBalance
GO
CREATE PROCEDURE [dbo].[USP_GetTrialBalance]                                 
@numDomainId AS NUMERIC(9),                                                  
@dtFromDate AS DATETIME,                                                
@dtToDate AS DATETIME,
@ClientTimeZoneOffset INT, 
@numAccountClass AS NUMERIC(9)=0,
@DateFilter AS VARCHAR(20),
@ReportColumn AS VARCHAR(20)                                                        
AS                                                                
BEGIN        
	DECLARE @PLCHARTID NUMERIC(18,0) = 0
	DECLARE @PLCHARTAccountCode AS VARCHAR(100) = ''
	SELECT @PLCHARTID=COA.numAccountId,@PLCHARTAccountCode=vcAccountCode FROM Chart_of_Accounts COA WHERE numDomainID=@numDomainId AND bitProfitLoss=1

	DECLARE @FinancialYearStartDate AS DATETIME
	SELECT @FinancialYearStartDate = dbo.GetFiscalStartDate(YEAR(GETDATE()),@numDomainId)

	DECLARE @TEMPQuarter AS TABLE
	(
		ID INT,
		StartDate DATETIME,
		EndDate DATETIME
	)

	INSERT INTO @TEMPQuarter VALUES (1,@FinancialYearStartDate,DATEADD(ms,-3,DATEADD(QUARTER,1,@FinancialYearStartDate)))
	INSERT INTO @TEMPQuarter VALUES (2,DATEADD(QUARTER,1,@FinancialYearStartDate),DATEADD(ms,-3,DATEADD(QUARTER,2,@FinancialYearStartDate)))
	INSERT INTO @TEMPQuarter VALUES (3,DATEADD(QUARTER,2,@FinancialYearStartDate),DATEADD(ms,-3,DATEADD(QUARTER,3,@FinancialYearStartDate)))
	INSERT INTO @TEMPQuarter VALUES (4,DATEADD(QUARTER,3,@FinancialYearStartDate),DATEADD(ms,-3,DATEADD(QUARTER,4,@FinancialYearStartDate)))

	IF @DateFilter = 'CurYear'
	BEGIN
		SELECT @dtFromDate = @FinancialYearStartDate,
			@dtToDate = DATEADD(ms,-3,DATEADD(YEAR,1,@FinancialYearStartDate));
	END
	ELSE IF @DateFilter = 'PreYear'
	BEGIN
		SELECT @dtFromDate = DATEADD(YEAR,-1,@FinancialYearStartDate),
			@dtToDate = DATEADD(ms,-3,DATEADD(YEAR,1,DATEADD(YEAR,-1,@FinancialYearStartDate)));
	END
	ELSE IF @DateFilter = 'CurPreYear'
	BEGIN
		SELECT @dtFromDate = DATEADD(YEAR,-1,@FinancialYearStartDate),
			@dtToDate = DATEADD(ms,-3,DATEADD(YEAR,1,@FinancialYearStartDate));
	END
	ELSE IF @DateFilter = 'CuQur'
	BEGIN
		SELECT @dtFromDate = StartDate, @dtToDate=EndDate FROM @TEMPQuarter WHERE GetDate() BETWEEN StartDate AND EndDate
	END
	ELSE IF @DateFilter = 'CurPreQur'
	BEGIN
		SELECT @dtFromDate = DATEADD(QUARTER,-1,StartDate), @dtToDate=EndDate FROM @TEMPQuarter WHERE GetDate() BETWEEN StartDate AND EndDate
	END
	ELSE IF @DateFilter = 'PreQur'
	BEGIN
		SELECT @dtFromDate = DATEADD(QUARTER,-1,StartDate), @dtToDate=DATEADD(QUARTER,-1,EndDate) FROM @TEMPQuarter WHERE GetDate() BETWEEN StartDate AND EndDate
	END
	ELSE IF @DateFilter = 'LastMonth'
	BEGIN
		select @dtFromDate = DATEADD(mm,-1,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)),
				@dtToDate = DATEADD(ms,-3,DATEADD(mm,0,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)));  
	END
	ELSE IF @DateFilter = 'ThisMonth'
	BEGIN
		select @dtFromDate = DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0),
				@dtToDate = DATEADD(ms,- 3,DATEADD(mm,0,DATEADD(mm,DATEDIFF(mm,0,GETDATE())+1,0)));  
	END
	ELSE IF @DateFilter = 'CurPreMonth'
	BEGIN
		select @dtFromDate = DATEADD(mm,-1,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)),
				@dtToDate = DATEADD(ms,- 3,DATEADD(mm,0,DATEADD(mm,DATEDIFF(mm,0,GETDATE())+1,0))); 
	END

	CREATE TABLE #view_journal
	(
		numAccountId NUMERIC(18,0),
		vcAccountCode VARCHAR(50),
		COAvcAccountCode VARCHAR(100),
		datEntry_Date SMALLDATETIME,
		Debit MONEY,
		Credit MONEY
	)

	INSERT INTO #view_journal SELECT numAccountId,vcAccountCode,COAvcAccountCode,datEntry_Date,Debit,Credit FROM view_journal WHERE numDomainId = @numDomainID AND (numAccountClass=@numAccountClass OR @numAccountClass=0);

	DECLARE @dtFinYearFromJournal DATETIME ;
	DECLARE @dtFinYearFrom DATETIME ;

	SELECT @dtFinYearFromJournal = MIN(datEntry_Date) FROM #view_journal
	SET @dtFinYearFrom = (SELECT dtPeriodFrom FROM FINANCIALYEAR WHERE dtPeriodFrom <= @dtFromDate AND dtPeriodTo >= @dtFromDate AND numDomainId = @numDomainId)

	

	;WITH DirectReport (ParentId, numAccountTypeID, vcAccountType,vcAccountCode, LEVEL,Struc)
	AS
	(
		SELECT 
			CAST(0 AS NUMERIC(18)) AS ParentId, 
			[ATD].[numAccountTypeID], 
			[ATD].[vcAccountType],
			[ATD].[vcAccountCode],
			1 AS LEVEL, 
			CAST(CAST([ATD].[vcAccountCode] AS VARCHAR) AS VARCHAR(100))  AS Struc
		FROM 
			[dbo].[AccountTypeDetail] AS ATD
		WHERE 
			[ATD].[numDomainID]=@numDomainId 
			AND vcAccountCode IN ('0101','0102','0103','0104','0105','0106')
		UNION ALL
		SELECT 
			ATD.[numParentID] AS ParentId, 
			[ATD].[numAccountTypeID], 
			[ATD].[vcAccountType],
			[ATD].[vcAccountCode],
			LEVEL + 1, 
			CAST(d.Struc + '#' + cast([ATD].[vcAccountCode] AS VARCHAR) AS VARCHAR(100))  AS Struc
		FROM 
			[dbo].[AccountTypeDetail] AS ATD 
		JOIN 
			[DirectReport] D 
		ON 
			D.[numAccountTypeID] = [ATD].[numParentID]
		WHERE 
			[ATD].[numDomainID]=@numDomainId 
	),
	DirectReport1 (ParentId, numAccountTypeID, vcAccountType,vcAccountCode, LEVEL,numAccountId,Struc, bitProfitLoss)
	AS
	(
		SELECT 
			ParentId, 
			numAccountTypeID, 
			vcAccountType,
			vcAccountCode, 
			LEVEL,
			CAST(NULL AS NUMERIC(18)),
			Struc,
			CAST(0 AS BIT)
		FROM 
			DirectReport 
		UNION ALL
		SELECT 
			COA.numParntAcntTypeId AS ParentId,
			CAST(NULL AS NUMERIC(18)), 
			[vcAccountName],
			COA.[vcAccountCode],
			LEVEL + 1,
			[COA].[numAccountId], 
			CAST(d.Struc + '#' + CAST(COA.[vcAccountCode] AS VARCHAR) AS VARCHAR(100)) AS Struc,
			COA.bitProfitLoss
		FROM 
			[dbo].[Chart_of_Accounts] AS COA 
		JOIN 
			[DirectReport1] D 
		ON 
			D.[numAccountTypeID] = COA.[numParntAcntTypeId]
		WHERE 
			[COA].[numDomainID]=@numDomainId 
			AND ISNULL(COA.bitActive,0) = 1
	)

  
	SELECT 
		ParentId, 
		numAccountTypeID, 
		vcAccountType, 
		LEVEL, 
		vcAccountCode,
		numAccountId,
		Struc,
		bitProfitLoss
	INTO 
		#tempDirectReport
	FROM 
		DirectReport1

	DECLARE @columns VARCHAR(8000) = ''
	DECLARE @PivotColumns VARCHAR(8000) = ''
	DECLARE @sql VARCHAR(MAX) = ''

	IF @ReportColumn = 'Year'
	BEGIN
		CREATE TABLE #TempYearMonth
		(
			intYear INT,
			intMonth INT,
			MonthYear VARCHAR(50),
			StartDate DATETIME,
			EndDate DATETIME,
			ProfitLossAmount MONEY
		)
		
		;WITH CTE AS 
		(
			SELECT
				YEAR(@dtFromDate) AS 'yr',
				MONTH(@dtFromDate) AS 'mm',
				DATENAME(mm, @dtFromDate) AS 'mon',
				(DATENAME(mm, @dtFromDate) + '_' + CAST(YEAR(@dtFromDate) AS VARCHAR)) AS MonthYear,
				CAST(DATEFROMPARTS(YEAR(@dtFromDate),MONTH(@dtFromDate),1) AS DATETIME) dtStartDate,
				DATEADD(MS,-2,CAST(DATEADD(MONTH,1,DATEFROMPARTS(YEAR(@dtFromDate),MONTH(@dtFromDate),1)) AS DATETIME)) dtEndDate,
				@dtFromDate 'new_date'
			UNION ALL
			SELECT
				YEAR(DATEADD(d,1,new_date)) AS 'yr',
				MONTH(DATEADD(d,1,new_date)) AS 'mm',
				DATENAME(mm, DATEADD(d,1,new_date)) AS 'mon',
				(DATENAME(mm, DATEADD(d,1,new_date)) + '_' + CAST(YEAR(DATEADD(d,1,new_date)) AS VARCHAR)) AS MonthYear,
				CAST(DATEFROMPARTS(YEAR(DATEADD(d,1,new_date)),MONTH(DATEADD(d,1,new_date)),1) AS DATETIME) dtStartDate,
				DATEADD(MS,-2,CAST(DATEADD(MONTH,1,DATEFROMPARTS(YEAR(DATEADD(d,1,new_date)),MONTH(DATEADD(d,1,new_date)),1)) AS DATETIME)) dtEndDate,
				DATEADD(d,1,new_date) 'new_date'
			FROM CTE
			WHERE DATEADD(d,1,new_date) < @dtToDate
		)
	
		INSERT INTO 
			#tempYearMonth
		SELECT 
			yr,mm,MonthYear,dtStartDate,dtEndDate,0
		FROM 
			CTE
		GROUP BY 
			yr, mm, MonthYear, dtStartDate, dtEndDate
		ORDER BY 
			yr, mm
		OPTION (MAXRECURSION 5000)

		INSERT INTO #tempYearMonth VALUES (5000,5000,'Total',@dtFromDate,@dtToDate,0)

		UPDATE 
			#tempYearMonth
		SET
			EndDate = (CASE WHEN EndDate < @dtToDate THEN EndDate ELSE @dtToDate END)

		UPDATE 
			#tempYearMonth
		SET
			ProfitLossAmount = (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE numAccountId=@PLCHARTID AND vcAccountCode LIKE '0103%' AND datEntry_Date between StartDate and EndDate)
							+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE vcAccountCode LIKE '0104%' AND datEntry_Date between StartDate and EndDate)
							+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE vcAccountCode LIKE '0106%' AND datEntry_Date between StartDate and EndDate)
							+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE vcAccountCode LIKE '0103%' AND datEntry_Date between StartDate and EndDate)
							+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE (vcAccountCode LIKE '0103%' OR vcAccountCode LIKE '0104%' OR vcAccountCode  LIKE '0106%') AND datEntry_Date <=  DATEADD(MS,-3,StartDate))
							+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE numAccountId=@PLCHARTID AND datEntry_Date <=  DATEADD(MS,-3,StartDate))

		SELECT @columns = STUFF((SELECT ', ISNULL(' + MonthYear  + ',0) AS [' + REPLACE(MonthYear,'_',' ') + ']' FROM #tempYearMonth ORDER BY intYear,intMonth FOR XML PATH('')), 1, 2, ''); 
		SELECT @PivotColumns = STUFF((SELECT ', ' + MonthYear FROM #tempYearMonth ORDER BY intYear,intMonth FOR XML PATH('')), 1, 2, ''); 

		SELECT 
			COA.ParentId, 
			COA.numAccountTypeID, 
			COA.vcAccountType, 
			COA.LEVEL, 
			COA.vcAccountCode, 
			COA.numAccountId, 
			COA.Struc,
			Period.MonthYear,
			Period.ProfitLossAmount,
			(CASE 
				WHEN COA.[vcAccountCode] LIKE '0103%' OR COA.[vcAccountCode] LIKE '0104%' OR COA.[vcAccountCode] LIKE '0106%' THEN 0 
				ELSE ISNULL(t2.OPENING,0) 
			END) + (ISNULL(Debit,0) - ISNULL(Credit,0)) AS Amount
		INTO 
			#tempViewDataYear
		FROM 
			#tempDirectReport COA 
		OUTER APPLY
		(
			SELECT
				MonthYear,
				StartDate,
				EndDate,
				ProfitLossAmount
			FROM
				#tempYearMonth
		) AS Period
		OUTER APPLY
		(
			SELECT 
				SUM(Debit) AS Debit,
				SUM(Credit) AS Credit
			FROM
				#view_journal V 
			WHERE  
				V.numAccountID = COA.numAccountID 
				AND datEntry_Date BETWEEN Period.StartDate AND Period.EndDate
		) AS t1
		OUTER APPLY 
		(
			SELECT   
				SUM(Debit - Credit) AS OPENING
			FROM     
				#view_journal VJ
			WHERE    
				VJ.numAccountID = COA.numAccountID 
				AND datEntry_Date BETWEEN @dtFinYearFromJournal AND  DATEADD(MS,-3,Period.StartDate)
		)  AS t2
		
		SELECT TOP 1 @PLCHARTAccountCode=Struc FROM #tempDirectReport WHERe bitProfitLoss = 1

		SET @sql = 'SELECT
						ParentId,
						numAccountId,
						numAccountTypeID,
						vcAccountType,
						LEVEL,
						vcAccountCode,
						Struc,
						[Type],' + @columns + '
					FROM
					(
						SELECT 
							COA.ParentId, 
							COA.numAccountId,
							COA.numAccountTypeID, 
							COA.vcAccountType, 
							COA.LEVEL, 
							COA.vcAccountCode, 
							(''#'' + COA.Struc + ''#'') AS Struc,
							(CASE WHEN ISNULL(COA.numAccountId,0) > 0 THEN 1 ELSE 2 END) [Type],
							(CASE 
								WHEN ''#' + @PLCHARTAccountCode + '#'' LIKE ''%#''+ COA.vcAccountCode +''#%'' AND COA.bitProfitLoss <> 1 THEN -(ProfitLossAmount) + ISNULL(SUM(Amount),0)
								WHEN COA.bitProfitLoss = 1 THEN -(ProfitLossAmount) 
								ELSE ISNULL(SUM(Amount),0) 
							END) AS Amount,
							V.MonthYear
						FROM 
							#tempDirectReport COA 
						LEFT OUTER JOIN 
							#tempViewDataYear V 
						ON  
							V.Struc like COA.Struc + ''%'' 
						GROUP BY 
							COA.ParentId, COA.numAccountTypeID, COA.vcAccountType, COA.LEVEL, COA.vcAccountCode, COA.numAccountId,COA.Struc,COA.bitProfitLoss,V.MonthYear,V.ProfitLossAmount
						UNION
						SELECT 
							0, 
							NULL,
							NULL,
							''Total'',
							-1, 
							NULL, 
							''-1'',
							2,
							(SELECT 
								ISNULL(SUM(Debit) - SUM(Credit),0)
							FROM 
								VIEW_JOURNAL 
							WHERE 
								numDomainID = ' + CAST(@numDomainId AS VARCHAR) + '
								AND (vcAccountCode LIKE ''0101%'' 
								OR vcAccountCode LIKE ''0102%''
								OR vcAccountCode LIKE ''0103%''
								OR vcAccountCode LIKE ''0104%''
								OR vcAccountCode LIKE ''0105%''
								OR vcAccountCode LIKE ''0106%'')
								AND datEntry_Date BETWEEN #TempYearMonth.StartDate AND #TempYearMonth.EndDate),
							MonthYear
						FROM
							#TempYearMonth
					) AS SourceTable
					pivot
					(
						SUM(AMOUNT)
						FOR [MonthYear] IN ( ' + @pivotcolumns + ' )
					) AS P 
					ORDER BY Struc, [Type] desc'

		PRINT @sql
		EXECUTE (@sql)

		DROP TABLE #TempYearMonth
		DROP TABLE #tempViewDataYear
	END
	ELSE IF @ReportColumn = 'Quarter'
	BEGIN
		CREATE TABLE #TempYearMonthQuarter
		(
			intYear INT,
			intQuarter INT,
			MonthYear VARCHAR(50),
			StartDate DATETIME,
			EndDate DATETIME,
			ProfitLossAmount MONEY
		)

		;WITH CTE AS 
		(
			SELECT
				dbo.GetFiscalyear(@dtFromDate,@numDomainId) AS 'yr',
				dbo.GetFiscalQuarter(@dtFromDate,@numDomainId) AS 'qq',
				('Q' + cast(dbo.GetFiscalQuarter(@dtFromDate,@numDomainId) AS VARCHAR) + '_' + cast(dbo.GetFiscalyear(@dtFromDate,@numDomainId) AS VARCHAR)) AS MonthYear,
				CASE dbo.GetFiscalQuarter(@dtFromDate,@numDomainId) 
				WHEN 1 THEN dateadd(month,0,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId))
				WHEN 2 THEN dateadd(month,3,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)) 
				WHEN 3 THEN dateadd(month,6,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId))
				WHEN 4 THEN dateadd(month,9,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)) 
				END AS dtStartDate,
				CASE dbo.GetFiscalQuarter(@dtFromDate,@numDomainId) 
				WHEN 1 THEN dateadd(MS,-3,dateadd(month,3,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)))
				WHEN 2 THEN dateadd(MS,-3,dateadd(month,6,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)))
				WHEN 3 THEN dateadd(MS,-3,dateadd(month,9,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)))
				WHEN 4 THEN dateadd(MS,-3,dateadd(month,12,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)))
				END AS dtEndDate,
				@dtFromDate 'new_date'
			UNION ALL
			SELECT
				dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId) AS 'yr',
				dbo.GetFiscalQuarter(DATEADD(d,1,new_date),@numDomainId) AS 'qq',
				('Q' + cast(dbo.GetFiscalQuarter(DATEADD(d,1,new_date),@numDomainId) AS VARCHAR) + '_' + cast(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId) AS VARCHAR)) AS MonthYear,
				CASE dbo.GetFiscalQuarter(DATEADD(d,1,new_date),@numDomainId) 
				WHEN 1 THEN dateadd(month,0,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId))
				WHEN 2 THEN dateadd(month,3,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)) 
				WHEN 3 THEN dateadd(month,6,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId))
				WHEN 4 THEN dateadd(month,9,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)) 
				END AS dtStartDate,
				CASE dbo.GetFiscalQuarter(DATEADD(d,1,new_date),@numDomainId) 
				WHEN 1 THEN dateadd(MS,-3,dateadd(month,3,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)))
				WHEN 2 THEN dateadd(MS,-3,dateadd(month,6,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)))
				WHEN 3 THEN dateadd(MS,-3,dateadd(month,9,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)))
				WHEN 4 THEN dateadd(MS,-3,dateadd(month,12,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)))
				END AS dtEndDate,
				DATEADD(d,1,new_date) 'new_date'
			FROM CTE
			WHERE DATEADD(d,1,new_date) < @dtToDate
		)
	
		INSERT INTO 
			#TempYearMonthQuarter 		
		SELECT 
			yr,qq,MonthYear,dtStartDate,dtEndDate,0
		FROM 
			CTE
		GROUP BY 
			yr, qq, MonthYear, dtStartDate, dtEndDate
		ORDER BY 
			yr, qq
		OPTION
			(MAXRECURSION 5000)

		INSERT INTO #TempYearMonthQuarter VALUES (5000,5000,'Total',@dtFromDate,@dtToDate,0)

		UPDATE 
			#TempYearMonthQuarter
		SET
			EndDate = (CASE WHEN EndDate < @dtToDate THEN EndDate ELSE @dtToDate END)

		UPDATE 
			#TempYearMonthQuarter
		SET
			ProfitLossAmount = (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE numAccountId=@PLCHARTID AND vcAccountCode LIKE '0103%' AND datEntry_Date between StartDate and EndDate)
							+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE vcAccountCode LIKE '0104%' AND datEntry_Date between StartDate and EndDate)
							+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE vcAccountCode LIKE '0106%' AND datEntry_Date between StartDate and EndDate)
							+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE vcAccountCode LIKE '0103%' AND datEntry_Date between StartDate and EndDate)
							+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE (vcAccountCode LIKE '0103%' OR vcAccountCode LIKE '0104%' OR vcAccountCode  LIKE '0106%') AND datEntry_Date <=  DATEADD(MS,-3,StartDate))
							+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE numAccountId=@PLCHARTID AND datEntry_Date <=  DATEADD(MS,-3,StartDate))

		SELECT @columns = STUFF((SELECT ', ISNULL(' + MonthYear  + ',0) AS [' + REPLACE(MonthYear,'_',' ') + ']' FROM #TempYearMonthQuarter ORDER BY intYear,intQuarter FOR XML PATH('')), 1, 2, ''); 
		SELECT @PivotColumns = STUFF((SELECT ', ' + MonthYear FROM #TempYearMonthQuarter ORDER BY intYear,intQuarter FOR XML PATH('')), 1, 2, ''); 

		SELECT 
			COA.ParentId, 
			COA.numAccountTypeID, 
			COA.vcAccountType, 
			COA.LEVEL, 
			COA.vcAccountCode, 
			COA.numAccountId, 
			COA.Struc,
			Period.MonthYear,
			Period.ProfitLossAmount,
			(CASE 
				WHEN COA.[vcAccountCode] LIKE '0103%' OR COA.[vcAccountCode] LIKE '0104%' OR COA.[vcAccountCode] LIKE '0106%' THEN 0 
				ELSE ISNULL(t2.OPENING,0) 
			END) + (ISNULL(Debit,0) - ISNULL(Credit,0)) AS Amount
		INTO 
			#tempViewDataQuarter
		FROM 
			#tempDirectReport COA 
		OUTER APPLY
		(
			SELECT
				MonthYear,
				StartDate,
				EndDate,
				ProfitLossAmount
			FROM
				#TempYearMonthQuarter
		) AS Period
		OUTER APPLY
		(
			SELECT 
				SUM(Debit) AS Debit,
				SUM(Credit) AS Credit
			FROM
				#view_journal V 
			WHERE  
				V.numAccountID = COA.numAccountID 
				AND datEntry_Date BETWEEN Period.StartDate AND Period.EndDate
		) AS t1
		OUTER APPLY 
		(
			SELECT   
				SUM(Debit - Credit) AS OPENING
			FROM     
				#view_journal VJ
			WHERE    
				VJ.numAccountID = COA.numAccountID 
				AND datEntry_Date BETWEEN @dtFinYearFromJournal AND  DATEADD(MS,-3,Period.StartDate)
		)  AS t2
		
		SELECT TOP 1 @PLCHARTAccountCode=Struc FROM #tempDirectReport WHERe bitProfitLoss = 1

		SET @sql = 'SELECT
						ParentId,
						numAccountId,
						numAccountTypeID,
						vcAccountType,
						LEVEL,
						vcAccountCode,
						Struc,
						[Type],' + @columns + '
					FROM
					(
						SELECT 
							COA.ParentId, 
							COA.numAccountId,
							COA.numAccountTypeID, 
							COA.vcAccountType, 
							COA.LEVEL, 
							COA.vcAccountCode, 
							(''#'' + COA.Struc + ''#'') AS Struc,
							(CASE WHEN ISNULL(COA.numAccountId,0) > 0 THEN 1 ELSE 2 END) [Type],
							(CASE 
								WHEN ''#' + @PLCHARTAccountCode + '#'' LIKE ''%#''+ COA.vcAccountCode +''#%'' AND COA.bitProfitLoss <> 1 THEN -(ProfitLossAmount) + ISNULL(SUM(Amount),0)
								WHEN COA.bitProfitLoss = 1 THEN -(ProfitLossAmount) 
								ELSE ISNULL(SUM(Amount),0) 
							END) AS Amount,
							V.MonthYear
						FROM 
							#tempDirectReport COA 
						LEFT OUTER JOIN 
							#tempViewDataQuarter V 
						ON  
							V.Struc like COA.Struc + ''%'' 
						GROUP BY 
							COA.ParentId, COA.numAccountTypeID, COA.vcAccountType, COA.LEVEL, COA.vcAccountCode, COA.numAccountId,COA.Struc,COA.bitProfitLoss,V.MonthYear,V.ProfitLossAmount
						UNION
						SELECT 
							0, 
							NULL,
							NULL,
							''Total'',
							-1, 
							NULL, 
							''-1'',
							2,
							(SELECT 
								ISNULL(SUM(Debit) - SUM(Credit),0)
							FROM 
								VIEW_JOURNAL 
							WHERE 
								numDomainID = ' + CAST(@numDomainId AS VARCHAR) + '
								AND (vcAccountCode LIKE ''0101%'' 
								OR vcAccountCode LIKE ''0102%''
								OR vcAccountCode LIKE ''0103%''
								OR vcAccountCode LIKE ''0104%''
								OR vcAccountCode LIKE ''0105%''
								OR vcAccountCode LIKE ''0106%'')
								AND datEntry_Date BETWEEN #TempYearMonthQuarter.StartDate AND #TempYearMonthQuarter.EndDate),
							MonthYear
						FROM
							#TempYearMonthQuarter
					) AS SourceTable
					pivot
					(
						SUM(AMOUNT)
						FOR [MonthYear] IN ( ' + @pivotcolumns + ' )
					) AS P 
					ORDER BY Struc, [Type] desc'

		PRINT @sql
		EXECUTE (@sql)

		DROP TABLE #TempYearMonthQuarter
		DROP TABLE #tempViewDataQuarter
	END
	ELSE
	BEGIN
		DECLARE @monProfitLossAmount AS MONEY = 0
		-- GETTING P&L VALUE
		SELECT @monProfitLossAmount = (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE numAccountId=@PLCHARTID AND vcAccountCode LIKE '0103%' AND datEntry_Date between @dtFromDate and @dtToDate)
			+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE vcAccountCode LIKE '0104%' AND datEntry_Date BETWEEN @dtFromDate AND @dtToDate)
			+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE vcAccountCode LIKE '0106%' AND datEntry_Date BETWEEN @dtFromDate AND @dtToDate)
			+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE vcAccountCode LIKE '0103%' AND datEntry_Date BETWEEN @dtFromDate AND @dtToDate)
			+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE (vcAccountCode LIKE '0103%' OR vcAccountCode LIKE '0104%' OR vcAccountCode  LIKE '0106%') AND datEntry_Date <=  DATEADD(MS,-3,@dtFromDate))
			+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE numAccountId=@PLCHARTID AND datEntry_Date <=  DATEADD(MS,-3,@dtFromDate))

		SELECT 
			COA.ParentId, 
			COA.numAccountTypeID, 
			COA.vcAccountType, 
			COA.LEVEL, 
			COA.vcAccountCode, 
			COA.numAccountId, 
			COA.Struc,
			(CASE 
				WHEN COA.[vcAccountCode] LIKE '0103%' OR COA.[vcAccountCode] LIKE '0104%' OR COA.[vcAccountCode] LIKE '0106%' THEN 0 
				ELSE ISNULL(t2.OPENING,0) 
			END) + (ISNULL(Debit,0) - ISNULL(Credit,0)) AS Amount
		INTO 
			#tempViewData
		FROM 
			#tempDirectReport COA 
		OUTER APPLY
		(
			SELECT 
				SUM(Debit) AS Debit,
				SUM(Credit) AS Credit
			FROM
				#view_journal V 
			WHERE  
				V.numAccountID = COA.numAccountID 
				AND datEntry_Date BETWEEN @dtFromDate AND @dtToDate
		) AS t1
		OUTER APPLY 
		(
			SELECT   
				SUM(Debit - Credit) AS OPENING
			FROM     
				#view_journal VJ
			WHERE    
				VJ.numAccountID = COA.numAccountID 
				AND datEntry_Date BETWEEN @dtFinYearFromJournal AND  DATEADD(MS,-3,@dtFromDate)
		)  AS t2

		SELECT TOP 1 @PLCHARTAccountCode=Struc FROM #tempDirectReport WHERe bitProfitLoss = 1

		Select 
			COA.ParentId, 
			COA.numAccountId,
			COA.numAccountTypeID, 
			COA.vcAccountType, 
			COA.LEVEL, 
			COA.vcAccountCode, 
			('#' + COA.Struc + '#') AS Struc,
			(CASE WHEN ISNULL(COA.numAccountId,0) > 0 THEN 1 ELSE 2 END) [Type],
			(CASE 
				WHEN '#' + @PLCHARTAccountCode + '#' LIKE '%#'+ COA.vcAccountCode +'#%' AND COA.bitProfitLoss <> 1 THEN -(@monProfitLossAmount) + ISNULL(SUM(Amount),0)
				WHEN COA.bitProfitLoss = 1 THEN -(@monProfitLossAmount) 
				ELSE ISNULL(SUM(Amount),0) 
			END) AS Amount
		FROM 
			#tempDirectReport COA 
		LEFT OUTER JOIN 
			#tempViewData V 
		ON  
			V.Struc like COA.Struc + '%' 
		GROUP BY 
			COA.ParentId, COA.numAccountTypeID, COA.vcAccountType, COA.LEVEL, COA.vcAccountCode, COA.numAccountId,COA.Struc,COA.bitProfitLoss
		UNION
		SELECT 
			0, 
			NULL,
			-1, 
			'Total', 
			0, 
			NULL, 
			'-1' AS Struc,
			2,
			(SELECT 
				SUM(Debit) - SUM(Credit)
			FROM 
				VIEW_JOURNAL 
			WHERE 
				numDomainID = 187
				AND (vcAccountCode LIKE '0101%' 
				OR vcAccountCode LIKE '0102%'
				OR vcAccountCode LIKE '0103%'
				OR vcAccountCode LIKE '0104%'
				OR vcAccountCode LIKE '0105%'
				OR vcAccountCode LIKE '0106%')
				AND datEntry_Date BETWEEN @dtFromDate AND @dtToDate)
		ORDER BY
			Struc, [Type] desc

		DROP TABLE #tempViewData 
	END

	DROP TABLE #view_journal
	DROP TABLE #tempDirectReport
END

/****** Object:  StoredProcedure [dbo].[USP_GetWareHouseItems]    Script Date: 07/26/2008 16:18:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec USP_GetWareHouseItems 6                            
--created by anoop jayaraj                              
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getwarehouseitems')
DROP PROCEDURE usp_getwarehouseitems
GO
CREATE PROCEDURE [dbo].[USP_GetWareHouseItems]                              
@numItemCode as numeric(9)=0,
@byteMode as tinyint=0                              
as                              
                              
                          
                              
declare @bitSerialize as bit                      
declare @str as nvarchar(max)                 
declare @str1 as nvarchar(max)               
declare @ColName as varchar(25)                      
set @str=''                       
                        
DECLARE @bitKitParent BIT
declare @numItemGroupID as numeric(9)                        
                        
select @numItemGroupID=numItemGroup,@bitSerialize=CASE WHEN bitSerialized=0 THEN bitLotNo ELSE bitSerialized END,
@bitKitParent = ( CASE WHEN ISNULL(bitKitParent,0) = 1
                               AND ISNULL(bitAssembly,0) = 1 THEN 0
                          WHEN ISNULL(bitKitParent,0) = 1 THEN 1
                          ELSE 0
                     END ) from Item where numItemCode=@numItemCode                        
if @bitSerialize=1 set @ColName='numWareHouseItmsDTLID,1'              
else set @ColName='numWareHouseItemID,0'              
              
--Create a Temporary table to hold data                                                            
create table #tempTable ( ID INT IDENTITY PRIMARY KEY,                                                                      
 numCusFlDItemID numeric(9)                                                         
 )                         
                        
insert into #tempTable                         
(numCusFlDItemID)                                                            
select distinct(numOppAccAttrID) from ItemGroupsDTL where numItemGroupID=@numItemGroupID and tintType=2                       
                          
                 
 declare @ID as numeric(9)                        
 declare @numCusFlDItemID as varchar(20)                        
 declare @fld_label as varchar(100),@fld_type as varchar(100)                        
 set @ID=0                        
 select top 1 @ID=ID,@numCusFlDItemID=numCusFlDItemID,@fld_label=fld_label,@fld_type=fld_type from #tempTable                         
 join CFW_Fld_Master on numCusFlDItemID=Fld_ID                        
                         
 while @ID>0                        
 begin                        
                          
   set @str=@str+',  dbo.GetCustFldItems('+@numCusFlDItemID+',9,'+@ColName+') as ['+ @fld_label+']'
	
	IF @byteMode=1                                        
		set @str=@str+',  dbo.GetCustFldItemsValue('+@numCusFlDItemID+',9,'+@ColName+','''+@fld_type+''') as ['+ @fld_label+'Value]'                                        
                          
   select top 1 @ID=ID,@numCusFlDItemID=numCusFlDItemID,@fld_label=fld_label from #tempTable                         
   join CFW_Fld_Master on numCusFlDItemID=Fld_ID and ID >@ID                        
   if @@rowcount=0 set @ID=0                        
                          
 end                        
                       
--      

DECLARE @KitOnHand AS NUMERIC(9);SET @KitOnHand=0

--IF @bitKitParent=1
	--SELECT @KitOnHand=ISNULL(dbo.fn_GetKitInventory(@numItemCode),0)          
  
set @str1='select '

IF @byteMode=1                                        
		set @str1 =@str1 +'I.numItemCode,'
	
set @str1 =@str1 + 'numWareHouseItemID,isnull(vcWarehouse,'''') + '': '' + isnull(WL.vcLocation,'''') vcWarehouse,W.numWareHouseID,
Case when @bitKitParent=1 then ISNULL(dbo.fn_GetKitInventoryByWarehouse(I.numItemCode, W.numWareHouseID),0) else isnull(numOnHand,0) end as [OnHand],
CAST((dbo.fn_UOMConversion(ISNULL(I.numBaseUnit,0),I.numItemCode,I.numDomainId,ISNULL(I.numPurchaseUnit,0)) * Case when @bitKitParent=1 THEN ISNULL(dbo.fn_GetKitInventoryByWarehouse(I.numItemCode, W.numWareHouseID),0) ELSE isnull(numOnHand,0) end) as numeric(18,2)) as PurchaseOnHand,
CAST((dbo.fn_UOMConversion(ISNULL(I.numBaseUnit,0),I.numItemCode,I.numDomainId,ISNULL(I.numSaleUnit,0)) * Case when @bitKitParent=1 THEN ISNULL(dbo.fn_GetKitInventoryByWarehouse(I.numItemCode, W.numWareHouseID),0) ELSE isnull(numOnHand,0) end) as numeric(18,2)) as SalesOnHand,
Case when @bitKitParent=1 THEN 0 ELSE isnull(numReorder,0) END as [Reorder] 
,Case when @bitKitParent=1 THEN 0 ELSE isnull(numOnOrder,0) END as [OnOrder]
,Case when @bitKitParent=1 THEN 0 ELSE isnull(numAllocation,0) END as [Allocation] 
,Case when @bitKitParent=1 THEN 0 ELSE isnull(numBackOrder,0) END as [BackOrder]
,0 as Op_Flag , isnull(WL.vcLocation,'''') Location,isnull(WL.numWLocationID,0) as numWLocationID,
round(isnull(monWListPrice,0),2) Price,isnull(vcWHSKU,'''') as SKU,isnull(vcBarCode,'''') as BarCode '+ case when @bitSerialize=1 then  ' ' else  @str end +'                   
,(SELECT CASE WHEN ISNULL(subI.bitKitParent,0) = 1 AND ISNULL((SELECT COUNT(*) FROM OpportunityItems 
															   WHERE OpportunityItems.numItemCode = WareHouseItems.numItemID
															   AND WareHouseItems.numWareHouseItemID = OpportunityItems.numWarehouseItmsID),0) = 0 THEN 1
			ELSE 0 
	    END 
FROM Item subI WHERE subI.numItemCode = WareHouseItems.numItemID) [IsDeleteKitWarehouse],@bitKitParent [bitKitParent],
(CASE WHEN (SELECT COUNT(*) FROM ItemDetails WHERE numChildItemID=WareHouseItems.numItemID AND numWareHouseItemId = WareHouseItems.numWareHouseItemID) > 0 THEN 1 ELSE 0 END) AS bitChildItemWarehouse
from WareHouseItems                           
join Warehouses W                             
on W.numWareHouseID=WareHouseItems.numWareHouseID 
left join WarehouseLocation WL on WL.numWLocationID =WareHouseItems.numWLocationID
join Item I on I.numItemCode=WareHouseItems.numItemID and WareHouseItems.numDomainId=I.numDomainId
where numItemID='+convert(varchar(15),@numItemCode)                  
   
print(@str1)           

EXECUTE sp_executeSQL @str1, N'@bitKitParent bit',@bitKitParent
                       
                        
set @str1='select numWareHouseItmsDTLID,WDTL.numWareHouseItemID, vcSerialNo,0 as Op_Flag,WDTL.vcComments as Comments,WDTL.numQty,WDTL.numQty as OldQty,W.vcWarehouse '+ case when @bitSerialize=1 then @str else '' end +'
,WDTL.dExpirationDate 
from WareHouseItmsDTL WDTL                             
join WareHouseItems WI                             
on WDTL.numWareHouseItemID=WI.numWareHouseItemID                              
join Warehouses W                             
on W.numWareHouseID=WI.numWareHouseID  
where 1 = (CASE WHEN ' + CAST(@byteMode AS VARCHAR(1)) + ' = ''1'' THEN CASE WHEN ISNULL(numQty,0) > 0 THEN 1 ELSE 0 END ELSE 1 END) and  numItemID='+ convert(varchar(15),@numItemCode)                          
print @str1                       
exec (@str1)                       
                      
                      
select Fld_label,fld_id,fld_type,numlistid,vcURL from #tempTable                         
 join CFW_Fld_Master on numCusFlDItemID=Fld_ID                      
drop table #tempTable


-----------------For Kendo UI testting purpose
--create table #tempColumnConfig ( field varchar(100),                                                                      
-- title varchar(100) , format varchar(100)                                                      
-- )  
--
--insert into #tempColumnConfig
--	select 'vcWarehouse','WareHouse','' union all
--	select 'OnHand','On Hand','' union all
--	select 'Reorder','Re Order','' union all
--	select 'Allocation','Allocation','' union all
--	select 'BackOrder','Back Order','' union all
--	select 'Price','Price','{0:c}'
--
--select * from #tempColumnConfig
--
--drop table #tempColumnConfig

-----------------

--set @str1='select case when COUNT(*)=1 then MIN(numWareHouseItmsDTLID) else 0 end as numWareHouseItmsDTLID,MIN(WDTL.numWareHouseItemID) as numWareHouseItemID, vcSerialNo,0 as Op_Flag,WDTL.vcComments as Comments'+ case when @bitSerialize=1 then @str else '' end +',W.vcWarehouse,COUNT(*) Qty
--from WareHouseItmsDTL WDTL                             
--join WareHouseItems WI                             
--on WDTL.numWareHouseItemID=WI.numWareHouseItemID                              
--join Warehouses W                             
--on W.numWareHouseID=WI.numWareHouseID  
--where (tintStatus is null or tintStatus=0)  and  numItemID='+ convert(varchar(15),@numItemCode) +'
--GROUP BY vcSerialNo,WDTL.vcComments,W.vcWarehouse'
--                          
--print @str1                       
--exec (@str1)                       
              
GO

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_InsertAssemblyItems')
DROP PROCEDURE USP_InsertAssemblyItems
GO

/*
BEGIN TRANSACTION
declare @p8 bigint
set @p8=0
exec USP_InsertAssemblyItems @numItemKitID=197596,@numChildItemID=198001,@numQtyItemsReq=10,@numUOMId=3,@numWareHouseId=58,@vcItemDesc='Adding 1',@sintOrder=1,@numItemDetailID=@p8 output
select @p8

ROLLBACK
*/

CREATE PROCEDURE [dbo].[USP_InsertAssemblyItems]
@numItemKitID AS numeric(9)=0,
@numChildItemID AS numeric(9)=0,
@numQtyItemsReq AS numeric(9)=0,
@numUOMId AS numeric(9)=0,
@numWareHouseId AS numeric(9)=0,
@vcItemDesc AS TEXT,
@sintOrder AS INT=0,
@numItemDetailID	NUMERIC(18,0) OUTPUT
AS
BEGIN
 
IF (SELECT COUNT(*) FROM ItemDetails WHERE numItemKitID = @numItemKitID AND numChildItemID = @numChildItemID) = 0
BEGIN
	PRINT 'Manish'
	DECLARE @numWareHouseItemId numeric(9)
	set @numWareHouseItemId = ISNULL((SELECT TOP 1 numWareHouseItemID FROM dbo.WareHouseItems WHERE numWareHouseID=@numWareHouseId AND numItemID=@numChildItemID),0);
	PRINT @numWareHouseItemId

	insert into ItemDetails(numItemKitID,numChildItemID,numQtyItemsReq,numWareHouseItemId,numUOMId,vcItemDesc,sintOrder)
	values(@numItemKitID,@numChildItemID,@numQtyItemsReq, @numWareHouseItemId,@numUOMId,@vcItemDesc,@sintOrder)
	
	SET @numItemDetailID = @@IDENTITY
	PRINT @numItemDetailID
END 
END
--SELECT * FROM Category
--- Alterd By Chintan Prajapati
-- EXEC dbo.USP_ItemsForECommerce 9,60,1,1,15,0,''
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ItemsForECommerce' ) 
    DROP PROCEDURE USP_ItemsForECommerce
GO
CREATE PROCEDURE [dbo].[USP_ItemsForECommerce]
    @numSiteID AS NUMERIC(9) = 0,
    @numCategoryID AS NUMERIC(18) = 0,
    @numWareHouseID AS NUMERIC(9) = 0,
    @SortBy NVARCHAR(MAX) = 0,
    @CurrentPage INT,
    @PageSize INT,
    @TotRecs INT = 0 OUTPUT,
    @SearchText AS NVARCHAR(MAX) = '',--Comma seperated item ids
    @FilterQuery AS NVARCHAR(MAX) = '',----Comma seperated Attributes Ids
    @FilterRegularCondition AS NVARCHAR(MAX),
    --@FilterCustomCondition AS NVARCHAR(MAX),
    @FilterCustomFields AS NVARCHAR(MAX),
    @FilterCustomWhere AS NVARCHAR(MAX)
   
AS 
    BEGIN
		
		DECLARE @numDomainID AS NUMERIC(9)
		SELECT  @numDomainID = numDomainID
        FROM    [Sites]
        WHERE   numSiteID = @numSiteID
        
        DECLARE @tintDisplayCategory AS TINYINT
		SELECT @tintDisplayCategory = ISNULL(bitDisplayCategory,0) FROM dbo.eCommerceDTL WHERE numSiteID = @numSiteId
		PRINT @tintDisplayCategory 
		
		CREATE TABLE #tmpItemCat(numCategoryID NUMERIC(18,0))
		CREATE TABLE #tmpItemCode(numItemCode NUMERIC(18,0))
		IF LEN(@FilterCustomFields) > 0 AND LEN(@FilterCustomWhere) > 0
		BEGIN
--			DECLARE @strCustomSql AS NVARCHAR(MAX)
--			SET @strCustomSql = 'INSERT INTO #tmpItemCode(numItemCode)
--								 SELECT DISTINCT t2.RecId FROM CFW_Fld_Master t1 
--								 JOIN CFW_FLD_Values_Item AS t2 ON t1.Fld_ID = t2.Fld_ID 
--								 WHERE t1.Grp_id = 5 
--								 AND t1.numDomainID = (SELECT numDomainID FROM dbo.Sites WHERE numSiteID = ' + CONVERT(VARCHAR(10),@numSiteID) + ') 
--								 AND t1.fld_type <> ''Link'' AND ' + @FilterCustomCondition
			
			DECLARE @FilterCustomFieldIds AS NVARCHAR(MAX)
			SET @FilterCustomFieldIds = Replace(@FilterCustomFields,'[','')
			SET @FilterCustomFieldIds = Replace(@FilterCustomFieldIds,']','')
			
			DECLARE @strCustomSql AS NVARCHAR(MAX)
			SET @strCustomSql = 'INSERT INTO #tmpItemCode(numItemCode)
								 SELECT DISTINCT T.RecId FROM 
															(
																SELECT * FROM 
																	(
																		SELECT  t2.RecId, t1.fld_id, t2.Fld_Value FROM CFW_Fld_Master t1 
																		JOIN CFW_FLD_Values_Item AS t2 ON t1.Fld_ID = t2.Fld_ID 
																		WHERE t1.Grp_id = 5 
																		AND t1.numDomainID = ' + CONVERT(VARCHAR(10),@numDomainID) + '
																		AND t1.fld_type <> ''Link'' 
																		AND t1.fld_id IN (' + @FilterCustomFieldIds + ')
																	) p  PIVOT (MAX([Fld_Value]) FOR fld_id IN (' + @FilterCustomFields + ')) AS pvt 
															) T  WHERE 1=1 AND ' + @FilterCustomWhere
								 
			PRINT @strCustomSql
			EXEC SP_EXECUTESQL @strCustomSql								 					 
		END
		
        DECLARE @strSQL NVARCHAR(MAX)
        DECLARE @firstRec AS INTEGER
        DECLARE @lastRec AS INTEGER
        DECLARE @Where NVARCHAR(MAX)
        DECLARE @SortString NVARCHAR(MAX)
        DECLARE @searchPositionColumn NVARCHAR(MAX)
        DECLARE @row AS INTEGER
        DECLARE @data AS VARCHAR(100)
        DECLARE @dynamicFilterQuery NVARCHAR(MAX)
        DECLARE @checkFldType VARCHAR(100)
        DECLARE @count NUMERIC(9,0)
        DECLARE @whereNumItemCode NVARCHAR(MAX)
        DECLARE @filterByNumItemCode VARCHAR(100)
        
		SET @SortString = ''
        set @searchPositionColumn = ''
        SET @strSQL = ''
		SET @whereNumItemCode = ''
		SET @dynamicFilterQuery = ''
		 
        SET @firstRec = ( @CurrentPage - 1 ) * @PageSize
        SET @lastRec = ( @CurrentPage * @PageSize + 1 )
                
        DECLARE @bitAutoSelectWarehouse AS BIT
		SELECT @bitAutoSelectWarehouse = ISNULL([ECD].[bitAutoSelectWarehouse],0) FROM [dbo].[eCommerceDTL] AS ECD WHERE [ECD].[numSiteId] = @numSiteID
		AND [ECD].[numDomainId] = @numDomainID

		DECLARE @vcWarehouseIDs AS VARCHAR(1000)
        IF (@numWareHouseID = 0 OR @bitAutoSelectWarehouse = 1)
            BEGIN				
				IF @bitAutoSelectWarehouse = 1
					BEGIN
						SELECT @vcWarehouseIDs = ISNULL( STUFF((SELECT DISTINCT ',' + CAST(numWarehouseID AS VARCHAR(10)) FROM [dbo].[Warehouses] WHERE [Warehouses].[numDomainID] = @numDomainID FOR XML PATH('')),1, 1, '') , '') 
					END
				
				ELSE
					BEGIN
						SELECT  @numWareHouseID = ISNULL(numDefaultWareHouseID,0)
						FROM    [eCommerceDTL]
						WHERE   [numDomainID] = @numDomainID	

						SET @vcWarehouseIDs = @numWareHouseID
					END
            END
		ELSE
		BEGIN
			SET @vcWarehouseIDs = @numWareHouseID
		END

		PRINT @vcWarehouseIDs

		--PRIAMRY SORTING USING CART DROPDOWN
		DECLARE @Join AS NVARCHAR(MAX)
		DECLARE @SortFields AS NVARCHAR(MAX)
		DECLARE @vcSort AS VARCHAR(MAX)

		SET @Join = ''
		SET @SortFields = ''
		SET @vcSort = ''

		IF @SortBy = ''
			BEGIN
				SET @SortString = ' vcItemName Asc '
			END
		ELSE IF CHARINDEX('~0',@SortBy) > 0 OR CHARINDEX('~1',@SortBy) > 0
			BEGIN
				DECLARE @fldID AS NUMERIC(18,0)
				DECLARE @RowNumber AS VARCHAR(MAX)
				
				SELECT * INTO #fldValues FROM dbo.SplitIDsWithPosition(@SortBy,'~')
				SELECT @RowNumber = RowNumber, @fldID = id FROM #fldValues WHERE RowNumber = 1
				SELECT @vcSort = CASE WHEN Id = 0 THEN 'ASC' ELSE 'DESC' END FROM #fldValues WHERE RowNumber = 2
				
				SET @Join = @Join + ' LEFT JOIN CFW_FLD_Values_Item CFVI' + @RowNumber + ' ON I.numItemCode = CFVI' + @RowNumber + '.RecID AND CFVI' + @RowNumber + '.fld_Id = ' + CONVERT(VARCHAR(10),@fldID)
				---SET @SortString = 'CFVI' + @RowNumber + '.fld_Value' 
				SET @SortString = '[fld_Value' + @RowNumber + '] ' --+ @vcSort
				SET @SortFields = ', CFVI' + @RowNumber + '.fld_Value [fld_Value' + @RowNumber + ']'
			END
		ELSE
			BEGIN
				--SET @SortString = @SortBy	
				
				IF CHARINDEX('monListPrice',@SortBy) > 0
				BEGIN
					DECLARE @bitSortPriceMode AS BIT
					SELECT @bitSortPriceMode = ISNULL(bitSortPriceMode,0) FROM dbo.eCommerceDTL WHERE numSiteId = @numSiteID
					PRINT @bitSortPriceMode
					IF @bitSortPriceMode = 1
					BEGIN
						SET @SortString = ' '
					END
					ELSE
					BEGIN
						SET @SortString = @SortBy	
					END
				END
				ELSE
				BEGIN
					SET @SortString = @SortBy	
				END
				
			END	

		--SECONDARY SORTING USING DEFAULT SETTINGS FROM ECOMMERCE
		SELECT ROW_NUMBER() OVER (ORDER BY Custom,vcFieldName) AS [RowID],* INTO #tempSort FROM (
		SELECT  CONVERT(VARCHAR(9),numFieldID)+'~0' AS numFieldID,
				ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName,
				0 AS Custom,
				vcDbColumnName 
		FROM View_DynamicColumns
		WHERE numFormID = 84 
		AND ISNULL(bitSettingField,0) = 1 
		AND ISNULL(bitDeleted,0)=0 
		AND ISNULL(bitCustom,0) = 0
		AND numDomainID = @numDomainID
					       
		UNION     

		SELECT  CONVERT(VARCHAR(9),numFieldID)+'~1' AS numFieldID ,
				vcFieldName ,
				1 AS Custom,
				'' AS vcDbColumnName
		FROM View_DynamicCustomColumns          
		WHERE numFormID = 84
		AND numDomainID = @numDomainID
		AND grp_id = 5 
		AND vcAssociatedControlType = 'TextBox' 
		AND ISNULL(bitCustom,0) = 1 
		AND tintPageType=1  
		) TABLE1 ORDER BY Custom,vcFieldName
		
		--SELECT * FROM #tempSort
		DECLARE @DefaultSort AS NVARCHAR(MAX)
		DECLARE @DefaultSortFields AS NVARCHAR(MAX)
		DECLARE @DefaultSortJoin AS NVARCHAR(MAX)
		DECLARE @intCnt AS INT
		DECLARE @intTotalCnt AS INT	
		DECLARE @strColumnName AS VARCHAR(100)
		DECLARE @bitCustom AS BIT
		DECLARE @FieldID AS VARCHAR(100)				

		SET @DefaultSort = ''
		SET @DefaultSortFields = ''
		SET @DefaultSortJoin = ''		
		SET @intCnt = 0
		SET @intTotalCnt = ( SELECT COUNT(*) FROM #tempSort )
		SET @strColumnName = ''
		
		IF (SELECT ISNULL(bitEnableSecSorting,0) FROM EcommerceDtl WHERE numSiteID = @numSiteID ) = 1
		BEGIN
			CREATE TABLE #fldDefaultValues(RowNumber INT,Id VARCHAR(1000))

			WHILE(@intCnt < @intTotalCnt)
			BEGIN
				SET @intCnt = @intCnt + 1
				SELECT @FieldID = numFieldID, @strColumnName = vcDbColumnName, @bitCustom = Custom FROM #tempSort WHERE RowID = @intCnt
				
				--PRINT @FieldID
				--PRINT @strColumnName
				--PRINT @bitCustom

				IF @bitCustom = 0
					BEGIN
						SET @DefaultSort =  @strColumnName + ',' + @DefaultSort 
					END

				ELSE
					BEGIN
						DECLARE @fldID1 AS NUMERIC(18,0)
						DECLARE @RowNumber1 AS VARCHAR(MAX)
						DECLARE @vcSort1 AS VARCHAR(10)
						DECLARE @vcSortBy AS VARCHAR(1000)
						SET @vcSortBy = CONVERT(VARCHAR(100),@FieldID) + '~' + '0'
						--PRINT @vcSortBy

						INSERT INTO #fldDefaultValues(RowNumber,Id) SELECT RowNumber, id FROM dbo.SplitIDsWithPosition(@vcSortBy,'~')
						SELECT @RowNumber1 = RowNumber + 100 + @intCnt, @fldID1 = id FROM #fldDefaultValues WHERE RowNumber = 1
						SELECT @vcSort1 = 'ASC' 
						
						SET @DefaultSortJoin = @DefaultSortJoin + ' LEFT JOIN CFW_FLD_Values_Item CFVI' + @RowNumber1 + ' ON I.numItemCode = CFVI' + @RowNumber1 + '.RecID AND CFVI' + @RowNumber1 + '.fld_Id = ' + CONVERT(VARCHAR(10),@fldID1)
						SET @DefaultSort = '[fld_Value' + @RowNumber1 + '] ' + ',' + @DefaultSort 
						SET @DefaultSortFields = @DefaultSortFields + ', CFVI' + @RowNumber1 + '.fld_Value [fld_Value' + @RowNumber1 + ']'
					END

			END
			IF LEN(@DefaultSort) > 0
			BEGIN
				SET  @DefaultSort = LEFT(@DefaultSort,LEN(@DefaultSort) - 1)
				SET @SortString = @SortString + ',' 
			END
			--PRINT @SortString
			--PRINT @DefaultSort
			--PRINT @DefaultSortFields
		END
--        IF @SortBy = 1 
--            SET @SortString = ' vcItemName Asc '
--        ELSE IF @SortBy = 2 
--            SET @SortString = ' vcItemName Desc '
--        ELSE IF @SortBy = 3 
--            SET @SortString = ' monListPrice Asc '
--        ELSE IF @SortBy = 4 
--            SET @SortString = ' monListPrice Desc '
--        ELSE IF @SortBy = 5 
--            SET @SortString = ' bintCreatedDate Desc '
--        ELSE IF @SortBy = 6 
--            SET @SortString = ' bintCreatedDate ASC '	
--        ELSE IF @SortBy = 7 --sort by relevance, invokes only when search
--            BEGIN
--                 IF LEN(@SearchText) > 0 
--		            BEGIN
--						SET @SortString = ' SearchOrder ASC ';
--					    set @searchPositionColumn = ' ,SearchItems.RowNumber AS SearchOrder ' 						
--					END
--			END
--        ELSE 
--            SET @SortString = ' vcItemName Asc '
		
                                		
        IF LEN(@SearchText) > 0
			BEGIN
				PRINT 1
				IF CHARINDEX('SearchOrder',@SortString) > 0
				BEGIN
					SET @searchPositionColumn = ' ,SearchItems.RowNumber AS SearchOrder ' 
				END

				SET @Where = ' INNER JOIN dbo.SplitIDsWithPosition('''+ @SearchText +''','','') AS SearchItems 
                        ON SearchItems.Id = I.numItemCode  and SC.numSiteID = ' + CONVERT(VARCHAR(15), @numSiteID) + ' WHERE ISNULL(IsArchieve,0) = 0'
				END

		ELSE IF @SearchText = ''

			SET @Where = ' WHERE 1=2 '
			+ ' and SC.numSiteID = ' + CONVERT(VARCHAR(15), @numSiteID)
			+ ' AND ISNULL(IsArchieve,0) = 0'
		ELSE
			BEGIN
				PRINT 2
				SET @Where = ' WHERE 1=1 '
							+ ' and SC.numSiteID = ' + CONVERT(VARCHAR(15), @numSiteID)
							+ ' AND ISNULL(IsArchieve,0) = 0'


				PRINT @tintDisplayCategory							
				PRINT @Where

				IF @tintDisplayCategory = 1
				BEGIN
					
					;WITH CTE AS(
					SELECT C.numCategoryID,C.tintLevel,C.numDepCategory,CASE WHEN tintLevel=1 THEN 1 ELSE 0 END AS bitChild 
						FROM Category C WHERE (numCategoryID = @numCategoryID OR ISNULL(@numCategoryID,0) = 0) AND numDomainID = @numDomainID
					UNION ALL
					SELECT  C.numCategoryID,C.tintLevel,C.numDepCategory,CTE.bitChild FROM Category C JOIN CTE ON 
					C.numDepCategory=CTE.numCategoryID AND CTE.bitChild=1 AND numDomainID = @numDomainID)
					
					INSERT INTO #tmpItemCat(numCategoryID)												 
					SELECT DISTINCT C.numCategoryID FROM CTE C JOIN dbo.ItemCategory IC ON C.numCategoryID = IC.numCategoryID 
														WHERE numItemID IN ( 
																						
													 						SELECT numItemID FROM WareHouseItems WI
																			INNER JOIN dbo.Item I ON I.numItemCode = WI.numItemID AND I.numDomainID = @numDomainID
																			GROUP BY numItemID 
																			HAVING SUM(WI.numOnHand) > 0 
																			UNION ALL
													 						SELECT numItemID FROM WareHouseItems WI
																			INNER JOIN dbo.Item I ON I.numItemCode = WI.numItemID AND I.numDomainID = @numDomainID
																			GROUP BY numItemID 
																			HAVING SUM(WI.numAllocation) > 0
																		 )		
					SELECT numItemID INTO #tmpOnHandItems FROM 
					(													 					
						SELECT numItemID FROM WareHouseItems WI 
						INNER JOIN dbo.Item I ON I.numItemCode = WI.numItemID AND I.numDomainID = @numDomainID
						GROUP BY numItemID 
						HAVING SUM(WI.numOnHand) > 0
						UNION ALL
						SELECT numItemID FROM WareHouseItems WI 
						INNER JOIN dbo.Item I ON I.numItemCode = WI.numItemID AND I.numDomainID = @numDomainID
						GROUP BY numItemID 
						HAVING SUM(WI.numAllocation) > 0				
					) TABLE1
				END

			END
		
			--SELECT * FROM  #tmpOnHandItems
			--SELECT * FROM  #tmpItemCat
			
----		IF (SELECT COUNT(*) FROM #tmpItemCat) = 0 AND ISNULL(@numCategoryID,0) = 0
----		BEGIN
----			
----			INSERT INTO #tmpItemCat(numCategoryID)
----			SELECT DISTINCT C.numCategoryID FROM Category C
----			JOIN dbo.ItemCategory IC ON C.numCategoryID = IC.numCategoryID 
----							WHERE C.numDepCategory = (SELECT TOP 1 numCategoryID FROM dbo.Category 
----													  WHERE numDomainID = @numDomainID
----													  AND tintLevel = 1)
----							AND tintLevel <> 1
----							AND numDomainID = @numDomainID
----							AND numItemID IN (
----											 	SELECT numItemID FROM WareHouseItems WI
----												INNER JOIN dbo.Item I ON I.numItemCode = WI.numItemID
----												GROUP BY numItemID 
----												HAVING SUM(WI.numOnHand) > 0
----											 )
----			--SELECT * FROM #tmpItemCat								 
----		END
----		ELSE
----		BEGIN
----			INSERT INTO #tmpItemCat(numCategoryID)VALUES(@numCategoryID)
----		END

		       SET @strSQL = @strSQL + ' WITH Items AS 
	                                     ( 
	                                     SELECT   
                                               I.numItemCode,
                                               ROW_NUMBER() OVER ( Partition by I.numItemCode Order by I.numItemCode) RowID,
                                               vcItemName,
                                               I.bintCreatedDate,
                                               vcManufacturer,
                                               C.vcCategoryName,
                                               ISNULL(txtItemDesc,'''') txtItemDesc,
                                               ISNULL(CASE  WHEN I.[charItemType] = ''P''
															THEN ( UOM * ISNULL(W1.[monWListPrice], 0) )
															ELSE ( UOM * monListPrice )
													  END, 0) monListPrice,
                                               vcSKU,
                                               fltHeight,
                                               fltWidth,
                                               fltLength,
                                               IM.vcPathForImage,
                                               IM.vcPathForTImage,
                                               vcModelID,
                                               fltWeight,
                                               bitFreeShipping,
                                               UOM AS UOMConversionFactor,
												( CASE WHEN charItemType <> ''S'' AND charItemType <> ''N''
													   THEN ( CASE WHEN bitShowInStock = 1
																   THEN ( CASE WHEN ( ISNULL(W.numOnHand,0)<=0) THEN CASE WHEN ' + CONVERT(VARCHAR(10),@numDomainID) + ' = 172 THEN ''<font color=red>ON HOLD</font>'' ELSE ''<font color=red>Out Of Stock</font>'' END
																			   WHEN bitAllowBackOrder = 1 THEN CASE WHEN ' + CONVERT(VARCHAR(10),@numDomainID) + ' = 172 THEN ''<font color=green>AVAILABLE</font>'' ELSE ''<font color=green>In Stock</font>'' END 
																			   ELSE CASE WHEN ' + CONVERT(VARCHAR(10),@numDomainID) + ' = 172 THEN ''<font color=green>AVAILABLE</font>'' ELSE ''<font color=green>In Stock</font>'' END  
																		  END )
																   ELSE ''''
															  END )
													   ELSE ''''
												  END ) AS InStock, C.vcDescription as CategoryDesc, ISNULL(W1.numWareHouseItemID,0) [numWareHouseItemID] ' 
								               +	@searchPositionColumn + @SortFields + @DefaultSortFields + '
								
                                         FROM      
                                                      Item AS I
                                         INNER JOIN   ItemCategory IC   ON   I.numItemCode = IC.numItemID
                                         LEFT JOIN    ItemImages IM     ON   I.numItemCode = IM.numItemCode AND IM.bitdefault=1 AND IM.bitIsImage = 1
                                         INNER JOIN   Category C        ON   IC.numCategoryID = C.numCategoryID
                                         INNER JOIN   SiteCategories SC ON   IC.numCategoryID = SC.numCategoryID
                                         INNER JOIN   eCommerceDTL E    ON   E.numDomainID = I.numDomainID AND E.numSiteId = SC.numSiteID ' + @Join + ' ' + @DefaultSortJoin + ' 
                                         CROSS APPLY (SELECT SUM(numOnHand) numOnHand FROM WareHouseItems W 
													  WHERE  W.numItemID = I.numItemCode
                                                      AND numWareHouseID IN (SELECT [OutParam] FROM dbo.[SplitString](''' + @vcWarehouseIDs + ''','',''))
                                                      AND W.numDomainID = ' + CONVERT(VARCHAR(10),@numDomainID) + ') AS W  
										 CROSS APPLY (SELECT TOP 1 numWareHouseItemID,monWListPrice FROM WareHouseItems W 
													  WHERE  W.numItemID = I.numItemCode 
													  AND W.numDomainID = ' + CONVERT(VARCHAR(10),@numDomainID) + '
													  AND numWareHouseID IN (SELECT [OutParam] FROM dbo.[SplitString](''' + @vcWarehouseIDs + ''','',''))) AS W1
										 CROSS APPLY (SELECT SUM(numAllocation) numAllocation FROM WareHouseItems W2 
													  WHERE  W2.numItemID = I.numItemCode
                                                      AND numWareHouseID IN (SELECT [OutParam] FROM dbo.[SplitString](''' + @vcWarehouseIDs + ''','',''))
                                                      AND W2.numDomainID = ' + CONVERT(VARCHAR(10),@numDomainID) + ') AS W2  			  
										 CROSS APPLY (SELECT dbo.fn_UOMConversion(ISNULL(I.numSaleUnit, 0),I.numItemCode, I.numDomainId,ISNULL(I.numBaseUnit, 0)) AS UOM) AS UOM '

			IF LEN(@FilterCustomFields) > 0 AND LEN(@FilterCustomWhere) > 0
			BEGIN
				SET @strSQL = @strSQL + ' INNER JOIN #tmpItemCode TP ON TP.numItemCode = I.numItemCode ' 
			END
			
			IF (SELECT COUNT(*) FROM #tmpItemCat) > 0
			BEGIN
				SET @strSQL = @strSQL + ' INNER JOIN #tmpItemCat TC ON TC.numCategoryID = IC.numCategoryID '
				SET @strSQL = @strSQL + ' LEFT JOIN #tmpOnHandItems TOIC ON TOIC.numItemID = I.numItemCode '
			END
			ELSE IF @numCategoryID>0
			BEGIN
				SET @Where = REPLACE(@Where,'WHERE',' WHERE IC.numCategoryID = ' + CONVERT(VARCHAR(10),@numCategoryID) + ' AND ')
			END 
			
			IF @tintDisplayCategory = 1
			BEGIN
				SET @Where = @Where + ' AND (ISNULL(W.numOnHand, 0) > 0 OR ISNULL(W2.numAllocation, 0) > 0) '
			END
			
			--PRINT @Where
			SET @strSQL = @strSQL + @Where
			
			IF LEN(@FilterRegularCondition) > 0
			BEGIN
				SET @strSQL = @strSQL + @FilterRegularCondition
			END                                         
			
---PRINT @SortString + ',' + @DefaultSort + ' ' + @vcSort 
	
            IF LEN(@whereNumItemCode) > 0
                                        BEGIN
								              SET @strSQL = @strSQL + @filterByNumItemCode	
								    	END
            SET @strSQL = @strSQL +     '   )
                                        , ItemSorted AS (SELECT  ROW_NUMBER() OVER ( ORDER BY  ' + @SortString + @DefaultSort + ' ' + @vcSort + ' ) AS Rownumber,* FROM  Items WHERE RowID = 1)'
            
            DECLARE @fldList AS NVARCHAR(MAX)
			SELECT @fldList=COALESCE(@fldList + ',','') + QUOTENAME(REPLACE(Fld_label,' ','') + '_C') FROM dbo.CFW_Fld_Master WHERE numDomainID=@numDomainID AND Grp_id=5
            
            SET @strSQL = @strSQL +' SELECT  Rownumber,numItemCode,txtItemDesc,vcManufacturer,monListPrice,vcItemName,vcCategoryName,vcPathForImage,vcpathForTImage,vcSKU,fltHeight,fltWidth,fltLength,vcModelID,fltWeight,bitFreeShipping,UOMConversionFactor,InStock,CategoryDesc,numWareHouseItemID
                                     INTO #tempItem FROM ItemSorted ' 
--                                        + 'WHERE Rownumber > ' + CONVERT(VARCHAR(10), @firstRec) + '
--                                        AND   Rownumber < ' + CONVERT(VARCHAR(10), @lastRec) + '
--                                        order by Rownumber;' 
            
            SET @strSQL = @strSQL + ' SELECT * INTO #tmpPagedItems FROM #tempItem WHERE Rownumber > ' + CONVERT(VARCHAR(10), @firstRec) + '
                                      AND   Rownumber < ' + CONVERT(VARCHAR(10), @lastRec) + '
                                      order by Rownumber '
                                                                  
            IF LEN(ISNULL(@fldList,''))>0
            BEGIN
				PRINT 'flds1'
				SET @strSQL = @strSQL +'SELECT  Rownumber,numItemCode,txtItemDesc,vcManufacturer,monListPrice,vcItemName,vcCategoryName,vcPathForImage,vcpathForTImage,vcSKU,fltHeight,fltWidth,fltLength,vcModelID,fltWeight,bitFreeShipping,UOMConversionFactor,InStock,CategoryDesc,numWareHouseItemID,' + @fldList + '
                                        FROM #tmpPagedItems I left join (
			SELECT  t2.RecId, t1.Fld_label + ''_C'' as Fld_label, 
				CASE t1.fld_type WHEN ''SelectBox'' THEN isnull(dbo.fn_GetListItemName(t2.Fld_Value),'''')
								 WHEN ''CheckBox'' THEN isnull(dbo.fn_getCustomDataValue(t2.Fld_Value,9),'''')
								 WHEN ''DateField'' THEN dbo.FormatedDateFromDate(t2.Fld_Value,' + CAST(@numDomainId AS VARCHAR(18)) +')
								 ELSE Replace(ISNULL(t2.Fld_Value,''0''),''$'',''$$'') end AS Fld_Value FROM CFW_Fld_Master t1 
			JOIN CFW_FLD_Values_Item AS t2 ON t1.Fld_ID = t2.Fld_ID where t1.Grp_id = 5 and t1.numDomainID=' + CAST(@numDomainId AS VARCHAR(18)) +' AND t1.fld_type <> ''Link''
			AND t2.RecId IN (SELECT numItemCode FROM #tmpPagedItems )
			) p PIVOT (MAX([Fld_Value]) FOR Fld_label IN (' + @fldList + ')) AS pvt on I.numItemCode=pvt.RecId order by Rownumber;' 	
			END 
			ELSE
			BEGIN
				PRINT 'flds2'
				SET @strSQL = @strSQL +' SELECT  Rownumber,numItemCode,txtItemDesc,vcManufacturer,monListPrice,vcItemName,vcCategoryName,vcPathForImage,vcpathForTImage,vcSKU,fltHeight,fltWidth,fltLength,vcModelID,fltWeight,bitFreeShipping,UOMConversionFactor,InStock,CategoryDesc,numWareHouseItemID
                                         FROM  #tmpPagedItems order by Rownumber;'
			END               
                                        
            
--            PRINT(@strSQL)        
--            EXEC ( @strSQL) ; 
			                           
            SET @strSQL = @strSQL + ' SELECT @TotRecs = COUNT(*) FROM #tempItem '                            
--             SET @strSQL = 'SELECT @TotRecs = COUNT(*) 
--                           FROM Item AS I
--                           INNER JOIN   ItemCategory IC ON I.numItemCode = IC.numItemID
--                           INNER JOIN   Category C ON IC.numCategoryID = C.numCategoryID
--                           INNER JOIN   SiteCategories SC ON IC.numCategoryID = SC.numCategoryID ' --+ @Where
--             
--			 IF LEN(@FilterCustomFields) > 0 AND LEN(@FilterCustomWhere) > 0
--			 BEGIN
--				 SET @strSQL = @strSQL + ' INNER JOIN #tmpItemCode TP ON TP.numItemCode = I.numItemCode ' 
--			 END
--			
--			 IF (SELECT COUNT(*) FROM #tmpItemCat) > 0
--			 BEGIN
--				SET @strSQL = @strSQL + ' INNER JOIN #tmpItemCat TC ON TC.numCategoryID = IC.numCategoryID '
--				SET @strSQL = @strSQL + ' INNER JOIN #tmpOnHandItems TOIC ON TOIC.numItemID = I.numItemCode '
--			 END
--			 ELSE IF @numCategoryID>0
--			 BEGIN
--				SET @Where = REPLACE(@Where,'WHERE',' WHERE IC.numCategoryID = ' + CONVERT(VARCHAR(10),@numCategoryID) + ' AND ')
--			 END 
--			
--			 SET @strSQL = @strSQL + @Where  
--			 
--			 IF LEN(@FilterRegularCondition) > 0
--			 BEGIN
--				SET @strSQL = @strSQL + @FilterRegularCondition
--			 END 
			
		--			--SELECT * FROM  #tempAvailableFields
		--SELECT * FROM  #tmpItemCode
		--SELECT * FROM  #tmpItemCat
		----SELECT * FROM  #fldValues
		--SELECT * FROM  #tempSort
		--SELECT * FROM  #fldDefaultValues
		--SELECT * FROM  #tmpOnHandItems
		----SELECT * FROM  #tmpPagedItems

			 PRINT  LEN(@strSQL)
			 DECLARE @tmpSQL AS VARCHAR(MAX)
			 SET @tmpSQL = @strSQL
			 PRINT  @tmpSQL
			 
             EXEC sp_executeSQL @strSQL, N'@TotRecs INT OUTPUT', @TotRecs OUTPUT
             
             
        DECLARE @tintOrder AS TINYINT                                                  
		DECLARE @vcFormFieldName AS VARCHAR(50)                                                  
		DECLARE @vcListItemType AS VARCHAR(1)                                             
		DECLARE @vcAssociatedControlType VARCHAR(10)                                                  
		DECLARE @numListID AS NUMERIC(9)                                                  
		DECLARE @WhereCondition VARCHAR(MAX)                       
		DECLARE @numFormFieldId AS NUMERIC  
		DECLARE @vcFieldType CHAR(1)
		                  
		SET @tintOrder=0                                                  
		SET @WhereCondition =''                 
                   
              
		CREATE TABLE #tempAvailableFields(numFormFieldId  NUMERIC(9),vcFormFieldName NVARCHAR(50),
		vcFieldType CHAR(1),vcAssociatedControlType NVARCHAR(50),numListID NUMERIC(18),
		vcListItemType CHAR(1),intRowNum INT,vcItemValue VARCHAR(3000))
		   
		INSERT INTO #tempAvailableFields (numFormFieldId,vcFormFieldName,vcFieldType,vcAssociatedControlType
				,numListID,vcListItemType,intRowNum)                         
		SELECT  Fld_id,REPLACE(Fld_label,' ','') AS vcFormFieldName,
			   CASE GRP_ID WHEN 4 then 'C' WHEN 1 THEN 'D' ELSE 'C' END AS vcFieldType,
				fld_type as vcAssociatedControlType,
				ISNULL(C.numListID, 0) numListID,
				CASE WHEN C.numListID > 0 THEN 'L'
					 ELSE ''
				END vcListItemType,ROW_NUMBER() OVER (ORDER BY Fld_id ASC) AS intRowNum
		FROM    CFW_Fld_Master C LEFT JOIN CFW_Validation V ON V.numFieldID = C.Fld_id
		WHERE   C.numDomainID = @numDomainId
				AND GRP_ID IN (5)

		  
		SELECT * FROM #tempAvailableFields
		
		/*
		DROP TABLE #tempAvailableFields
		DROP TABLE #tmpItemCode
		DROP TABLE #tmpItemCat
		*/	

		IF OBJECT_ID('tempdb..#tempAvailableFields') IS NOT NULL
		DROP TABLE #tempAvailableFields
		
		IF OBJECT_ID('tempdb..#tmpItemCode') IS NOT NULL
		DROP TABLE #tmpItemCode
		
		IF OBJECT_ID('tempdb..#tmpItemCat') IS NOT NULL
		DROP TABLE #tmpItemCat
		
		IF OBJECT_ID('tempdb..#fldValues') IS NOT NULL
		DROP TABLE #fldValues
		
		IF OBJECT_ID('tempdb..#tempSort') IS NOT NULL
		DROP TABLE #tempSort
		
		IF OBJECT_ID('tempdb..#fldDefaultValues') IS NOT NULL
		DROP TABLE #fldDefaultValues
		
		IF OBJECT_ID('tempdb..#tmpOnHandItems') IS NOT NULL
		DROP TABLE #tmpOnHandItems

		IF OBJECT_ID('tempdb..#tmpPagedItems') IS NOT NULL
		DROP TABLE #tmpPagedItems
    END


GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MANAGE_ITEM_VENDOR')
DROP PROCEDURE USP_MANAGE_ITEM_VENDOR
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE dbo.USP_MANAGE_ITEM_VENDOR
(
	@numVendorTcode		NUMERIC(18,0) OUTPUT,
	@numItemKitID		NUMERIC(18,0),
	@numVendorID		NUMERIC(18,0),
	@vcPartNo			VARCHAR(100),
	@numDomainID		NUMERIC(18, 0),
	@monCost			MONEY ,
	@intMinQty			INT,
	@bitIsPrimaryVendor	BIT,
	@intMode			BIT,
	@intLeadTimeDays INT = 0
)
AS 
BEGIN
	SET @intLeadTimeDays = CASE WHEN ISNUMERIC(@intLeadTimeDays) = 1 THEN @intLeadTimeDays ELSE 0 END	
	IF @intMode = 0
	BEGIN		
		SELECT @numVendorTcode = numVendorTcode FROM dbo.Vendor  WHERE numDomainID = @numDomainID AND numItemCode = @numItemKitID AND numVendorID = @numVendorID
		
		IF ISNULL(@numVendorTcode,0) = 0    
			BEGIN
				INSERT INTO  dbo.Vendor ( numVendorID, vcPartNo, numDomainID, monCost, numItemCode, intMinQty, intLeadTimeDays ) 
								 VALUES ( @numVendorID,@vcPartNo,@numDomainID,@monCost,@numItemKitID,@intMinQty, @intLeadTimeDays) 	
			
				 PRINT @numVendorTcode 
				 SET @numVendorTcode = @@IDENTITY		 
				 PRINT @numVendorTcode 
			END
		ELSE
			BEGIN
					SELECT  @numItemKitID = numItemCode, @numDomainID = numDomainID, @numVendorID = numVendorID 
					FROM dbo.Vendor WHERE numVendorTcode = @numVendorTcode
											
					UPDATE dbo.Vendor	SET	vcPartNo = @vcPartNo, monCost = @monCost, intMinQty = @intMinQty, intLeadTimeDays=@intLeadTimeDays
					WHERE 1=1
					AND numVendorTcode = @numVendorTcode
					AND numDomainID = @numDomainID						
			END 
		
		IF @bitIsPrimaryVendor = 1
			BEGIN
				UPDATE item SET numVendorID = @numVendorID WHERE numItemCode = @numItemKitID AND numDomainID = @numDomainID
			END			 					
	END
	
--	 PRINT @numVendorTcode 
--	 SET @numVendorTcode = @@IDENTITY		 
--	 PRINT @numVendorTcode 
	 
	IF @intMode = 1
	BEGIN
		DELETE FROM dbo.Vendor WHERE 1=1 AND numVendorTcode = @numVendorTcode
	END
END
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageBizDocTemplate')
DROP PROCEDURE USP_ManageBizDocTemplate
GO
CREATE PROCEDURE [dbo].[USP_ManageBizDocTemplate]
    @numDomainID NUMERIC,
    @numBizDocID NUMERIC,
    @numOppType NUMERIC, 
    @txtBizDocTemplate TEXT,
    @txtCSS TEXT,
    @bitEnabled BIT,
    @tintTemplateType TINYINT,
	@numBizDocTempID NUMERIC,
	@vcTemplateName varchar(50)='',
	@bitDefault bit=0,
	@numOrientation int=1,
	@bitKeepFooterBottom bit = 0,
	@numRelationship NUMERIC(18,0) = 0,
	@numProfile NUMERIC(18,0) = 0
AS 
BEGIN TRY
	IF (ISNULL(@numRelationship,0) <> 0 OR ISNULL(@numProfile,0) <>0)
	BEGIN
		IF (SELECT 
				COUNT(numBizDocTempID) 
			FROM 
				BizDocTemplate 
			WHERE 
				[numDomainID] = @numDomainID 
				AND [numBizDocID] = @numBizDocID 
				AND [numOppType]=@numOppType 
				AND tintTemplateType=@tintTemplateType 
				AND (ISNULL(numProfile,0) > 0 OR ISNULL(numRelationship,0) > 0) 
				AND numProfile=@numProfile 
				AND numBizDocTempID <> @numBizDocTempID
				AND numRelationship=@numRelationship) > 0
		BEGIN
			RAISERROR('BIZDOC_RELATIONSHIP_PROFILE_EXISTS',16,1)
		END
	END


	IF @tintTemplateType=0
	BEGIN
		IF @bitDefault=1
		BEGIN
			IF (select count(numBizDocTempID) from BizDocTemplate where [numDomainID] = @numDomainID AND [numBizDocID] = @numBizDocID AND [numOppType]=@numOppType AND tintTemplateType=@tintTemplateType and isnull(bitDefault,0)=1)>0
			BEGIN
				Update BizDocTemplate set bitDefault=0 where [numDomainID] = @numDomainID AND [numBizDocID] = @numBizDocID AND [numOppType]=@numOppType AND tintTemplateType=@tintTemplateType and isnull(bitDefault,0)=1
			END
		END
		ELSE
		BEGIN
			IF (select count(numBizDocTempID) from BizDocTemplate where [numDomainID] = @numDomainID AND [numBizDocID] = @numBizDocID AND [numOppType]=@numOppType AND tintTemplateType=@tintTemplateType)=0
			BEGIN
				SET @bitDefault=1
			END
		END

		IF @numBizDocTempID>0
			BEGIN
				UPDATE  BizDocTemplate SET [txtBizDocTemplate] = @txtBizDocTemplate,[txtCSS] = @txtCSS,
					bitEnabled =@bitEnabled,vcTemplateName=@vcTemplateName,bitDefault=@bitDefault,numOrientation=@numOrientation,bitKeepFooterBottom=@bitKeepFooterBottom,
					[numBizDocID] = @numBizDocID,
					[numRelationship] = @numRelationship,
					[numProfile] = @numProfile
					WHERE numBizDocTempID = @numBizDocTempID 
			END
		ELSE
			BEGIN
				INSERT  INTO BizDocTemplate
                    ([numDomainID],[numBizDocID],[txtBizDocTemplate],[txtCSS],bitEnabled,[numOppType],tintTemplateType,vcTemplateName,bitDefault,numOrientation,bitKeepFooterBottom,numRelationship,numProfile)
				VALUES  (@numDomainID,@numBizDocID,@txtBizDocTemplate,@txtCSS,@bitEnabled,@numOppType,@tintTemplateType,@vcTemplateName,@bitDefault,@numOrientation,@bitKeepFooterBottom,@numRelationship,@numProfile)
            
				SELECT  SCOPE_IDENTITY() AS InsertedID
			END
	END
	ELSE
		BEGIN
		IF NOT EXISTS ( SELECT  * FROM    [BizDocTemplate] WHERE   numDomainID = @numDomainID AND numBizDocID = @numBizDocID AND [numOppType]=@numOppType AND tintTemplateType=@tintTemplateType) 
        BEGIN
            INSERT  INTO BizDocTemplate
                    (
                      [numDomainID],
                      [numBizDocID],
                      [txtBizDocTemplate],
                      [txtCSS],
					  bitEnabled,
					  [numOppType],tintTemplateType,numOrientation,bitKeepFooterBottom,numRelationship,numProfile
	              )
            VALUES  (
                      @numDomainID,
                      @numBizDocID,
                      @txtBizDocTemplate,
                      @txtCSS,
					  @bitEnabled,
					  @numOppType,@tintTemplateType,@numOrientation,@bitKeepFooterBottom,@numRelationship,@numProfile
	              )
            SELECT  SCOPE_IDENTITY() AS InsertedID
        END
    ELSE 
        BEGIN
            UPDATE  BizDocTemplate
            SET     
                    [txtBizDocTemplate] = @txtBizDocTemplate,
                    [txtCSS] = @txtCSS,
                    bitEnabled =@bitEnabled,
                    numOppType = @numOppType,tintTemplateType=@tintTemplateType,numOrientation=@numOrientation,bitKeepFooterBottom=@bitKeepFooterBottom,
					numRelationship = @numRelationship, numProfile = @numProfile
            WHERE   [numDomainID] = @numDomainID AND [numBizDocID] = @numBizDocID AND [numOppType]=@numOppType AND tintTemplateType=@tintTemplateType
        END
END
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
/****** Object:  StoredProcedure [dbo].[usp_ManageContInfo]    Script Date: 07/26/2008 16:19:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_managecontinfo')
DROP PROCEDURE usp_managecontinfo
GO
CREATE PROCEDURE [dbo].[usp_ManageContInfo]                  
 @numContactId numeric=0,                  
 @vcDepartment numeric(9)=0,                  
 @vcFirstName varchar(50)='',                  
 @vcLastName varchar(50)='',                  
 @vcEmail varchar(50)='',                  
 @vcSex char(1)='',                  
 @vcPosition  numeric(9)=0,                  
 @numPhone varchar(15)='',                  
 @numPhoneExtension varchar(7)='',                  
 @bintDOB DATETIME=null,                  
 @txtNotes text='',                  
 @numUserCntID numeric=0,                      
 @numCategory numeric=0,                   
 @numCell varchar(15)='',                  
 @NumHomePhone varchar(15)='',                  
 @vcFax varchar(15)='',                  
 @vcAsstFirstName varchar(50)='',                  
 @vcAsstLastName varchar(50)='',                  
 @numAsstPhone varchar(15)='',                  
 @numAsstExtn varchar(6)='',                  
 @vcAsstEmail varchar(50)='',                      
 @numContactType numeric=0 ,               
 @bitOptOut bit=0,                
 @numManagerId numeric=0,                
 @numTeam as numeric(9)=0,                
 @numEmpStatus as numeric(9)=0,        
 @vcAltEmail as varchar(100),        
 @vcTitle as varchar(100),
 @numECampaignID AS NUMERIC(9),
 @bitSelectiveUpdate AS BIT = 0, --Update fields whose value are supplied
 @bitPrimaryContact AS BIT=0        
AS                  
IF @bitSelectiveUpdate = 0
BEGIN

if @bintDOB ='Jan  1 1753 12:00AM' set  @bintDOB=null                  
       
        DECLARE @numDivisionID AS NUMERIC(9)            
        SELECT  @numDivisionID = numDivisionID FROM AdditionalContactsInformation WHERE numContactID = @numContactId   
           
 IF @bitPrimaryContact = 1 
    BEGIN            
        DECLARE @numID AS NUMERIC(9)            
        DECLARE @bitPrimaryContactCheck AS BIT             
             
        SELECT TOP 1
                @numID = numContactID,
                @bitPrimaryContactCheck = ISNULL(bitPrimaryContact,0)
        FROM    AdditionalContactsInformation
        WHERE   numDivisionID = @numDivisionID 
                   
        IF @@rowcount = 0 
            SET @numID = 0             
        WHILE @numID > 0            
            BEGIN            
                IF @bitPrimaryContactCheck = 1 
                    UPDATE  AdditionalContactsInformation
                    SET     bitPrimaryContact = 0
                    WHERE   numContactID = @numID  
                              
                SELECT TOP 1
                        @numID = numContactID,
                        @bitPrimaryContactCheck = ISNULL(bitPrimaryContact,0)
                FROM    AdditionalContactsInformation
                WHERE   numDivisionID = @numDivisionID
                        AND numContactID > @numID    
                                
                IF @@rowcount = 0 
                    SET @numID = 0             
            END            
    END 
    ELSE IF @bitPrimaryContact = 0
    BEGIN
		IF (SELECT COUNT(*) FROM AdditionalContactsInformation WHERE numDivisionID = @numDivisionID AND numContactId!=@numContactId AND ISNULL(bitPrimaryContact,0)=1)=0
			SET @bitPrimaryContact = 1
    END                 
    
    UPDATE AdditionalContactsInformation SET                  
     vcDepartment=@vcDepartment,                  
     vcFirstName=@vcFirstName ,                  
     vcLastName =@vcLastName ,                  
     numPhone=@numPhone ,                  
     numPhoneExtension=@numPhoneExtension,                  
     vcEmail=@vcEmail ,                  
     charSex=@vcSex ,                  
     vcPosition = @vcPosition,                  
     bintDOB=@bintDOB ,                  
     txtNotes=@txtNotes,                  
     numModifiedBy=@numUserCntID,                  
     bintModifiedDate=getutcdate(),                
     vcCategory = @numCategory,                  
     numCell=@numCell,                  
     NumHomePhone=@NumHomePhone,                  
     vcFax=@vcFax,                  
     vcAsstFirstName=@vcAsstFirstName,                  
     vcAsstLastName=@vcAsstLastName,                  
     numAsstPhone=@numAsstPhone,                  
     numAsstExtn=@numAsstExtn,                  
     vcAsstEmail=@vcAsstEmail,                     
     numContactType=@numContactType,                      
     bitOptOut=@bitOptOut,                
     numManagerId=@numManagerId,                
     numTeam=@numTeam,           
     numEmpStatus = @numEmpStatus,        
     vcTitle=@vcTitle,        
     vcAltEmail= @vcAltEmail,
     numECampaignID = @numECampaignID,
     bitPrimaryContact=@bitPrimaryContact  
 WHERE numContactId=@numContactId
 
 /*Added by chintan BugID-262*/
	 DECLARE @Date AS DATETIME 
	 SELECT @Date = dbo.[GetUTCDateWithoutTime]()
	 IF @numECampaignID = -1 
		SET @numECampaignID=0
	 
 	EXEC [USP_ManageConEmailCampaign]
	@numConEmailCampID = 0, --  numeric(9, 0)
	@numContactID = @numContactID, --  numeric(9, 0)
	@numECampaignID = @numECampaignID, --  numeric(9, 0)
	@intStartDate = @Date, --  datetime
	@bitEngaged = 1, --  bit
	@numUserCntID = @numUserCntID --  numeric(9, 0)
END


IF @bitSelectiveUpdate = 1
BEGIN
		DECLARE @sql VARCHAR(1000)
		SET @sql = 'update AdditionalContactsInformation Set '

		IF(LEN(@vcFirstName)>0)
		BEGIN
			SET @sql =@sql + ' vcFirstName=''' + @vcFirstName + ''', '
		END
		IF(LEN(@vcLastName)>0)
		BEGIN
			SET @sql =@sql + ' vcLastName=''' + @vcLastName + ''', '
		END
		
		IF(LEN(@vcEmail)>0)
		BEGIN
			SET @sql =@sql + ' vcEmail=''' + @vcEmail + ''', '
		END
		
		IF(LEN(@numPhone)>0)
		BEGIN
			SET @sql =@sql + ' numPhone=''' + @numPhone + ''', '
		END
		
		IF(LEN(@numPhoneExtension)>0)
		BEGIN
			SET @sql =@sql + ' numPhoneExtension=''' + @numPhoneExtension + ''', '
		END
		
		IF(LEN(@numCell)>0)
		BEGIN
			SET @sql =@sql + ' numCell=''' + @numCell + ''', '
		END
		
		IF(LEN(@vcFax)>0)
		BEGIN
			SET @sql =@sql + ' vcFax=''' + @vcFax + ''', '
		END
		
		SET @sql =@sql + ' bitOptOut=''' + CONVERT(VARCHAR(10),@bitOptOut) + ''', '
		
		IF(@numUserCntID>0)
		BEGIN
			SET @sql =@sql + ' numModifiedBy='+ CONVERT(VARCHAR(10),@numUserCntID) +', '
		END
		
	
	
		SET @sql=SUBSTRING(@sql,0,LEN(@sql)) + ' '
		SET @sql =@sql + 'where numContactId= '+CONVERT(VARCHAR(10),@numContactId)

		PRINT @sql
		EXEC(@sql) 
END 
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_ManageInventory' ) 
    DROP PROCEDURE usp_ManageInventory
GO
CREATE PROCEDURE [dbo].[usp_ManageInventory]
    @itemcode AS NUMERIC(9) = 0,
    @numWareHouseItemID AS NUMERIC(9) = 0,
    @monAmount AS MONEY,
    @tintOpptype AS TINYINT,
    @numUnits AS NUMERIC,
    @QtyShipped AS NUMERIC,
    @QtyReceived AS NUMERIC,
    @monPrice AS MONEY,
    @tintFlag AS TINYINT,  --1: SO/PO insert/edit 2:SO/PO Close 3:SO/PO Re-Open 4:Sales Fulfillment Release 5:Sales Fulfillment Revert
    @numOppID as numeric(9)=0,
    @numoppitemtCode AS NUMERIC(9)=0,
    @numUserCntID AS NUMERIC(9)=0,
	@bitWorkOrder AS BIT = 0   
AS 
    DECLARE @onHand AS NUMERIC                                    
    DECLARE @onOrder AS NUMERIC 
    DECLARE @onBackOrder AS NUMERIC
    DECLARE @onAllocation AS NUMERIC
	DECLARE @onReOrder AS NUMERIC
    DECLARE @monAvgCost AS MONEY 
    DECLARE @bitKitParent BIT
	DECLARE @numOrigUnits AS NUMERIC		
	DECLARE @description AS VARCHAR(100)
	DECLARE @bitAsset as BIT
	DECLARE @numDomainID AS NUMERIC
	DECLARE @bitReOrderPoint AS BIT = 0
	DECLARE @numReOrderPointOrderStatus AS NUMERIC(18,0) = 0
--    DECLARE @OldOnHand AS NUMERIC    
--    DECLARE @NewAverageCost AS MONEY    
--    DECLARE @OldCost AS MONEY    
--    DECLARE @NewCost AS MONEY    
      
	SELECT @bitAsset=ISNULL(bitAsset,0),@numDomainID=numDomainID from Item where numItemCode=@itemcode--Added By Sachin
	SELECT @bitReOrderPoint=ISNULL(bitReOrderPoint,0),@numReOrderPointOrderStatus=ISNULL(numReOrderPointOrderStatus,0) FROM Domain WHERE numDomainID = @numDomainID
    
	SELECT  @monAvgCost = ISNULL(monAverageCost, 0),
            @bitKitParent = ( CASE WHEN ISNULL(bitKitParent,0) = 1
                               AND ISNULL(bitAssembly,0) = 1 THEN 0
                          WHEN ISNULL(bitKitParent,0) = 1 THEN 1
                          ELSE 0
                     END )
    FROM    Item
    WHERE   numitemcode = @itemcode                                   
    
    IF @numWareHouseItemID>0
    BEGIN                     
		SELECT  @onHand = ISNULL(numOnHand, 0),
				@onAllocation = ISNULL(numAllocation, 0),
				@onOrder = ISNULL(numOnOrder, 0),
				@onBackOrder = ISNULL(numBackOrder, 0),
				@onReOrder = ISNULL(numReorder,0)
		FROM    WareHouseItems
		WHERE   numWareHouseItemID = @numWareHouseItemID 
    END
   
   DECLARE @dtItemReceivedDate AS DATETIME;SET @dtItemReceivedDate=NULL
   
   DECLARE @TotalOnHand AS NUMERIC;SET @TotalOnHand=0  
   SELECT @TotalOnHand=SUM(ISNULL(numOnHand, 0)) FROM dbo.WareHouseItems WHERE numItemID=@itemcode
					
   SET @numOrigUnits=@numUnits
            
    IF @tintFlag = 1  --1: SO/PO insert/edit 
        BEGIN                                   
            IF @tintOpptype = 1 
            BEGIN  
			--When change below code also change to USP_ManageWorkOrderStatus  

				/* When QtyShipped has been updated form sales fulfillment then it need to be deducted */
				SET @description='SO insert/edit (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'
				
				IF @bitKitParent = 1 
                BEGIN
                    EXEC USP_UpdatingInventoryForEmbeddedKits @numoppitemtCode,@numOrigUnits, 0,@numOppID,0,@numUserCntID
                END 
				ELSE IF @bitWorkOrder = 1
				BEGIN
					SET @description='SO-WO insert/edit (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'
					-- MANAGE INVENTOY OF ASSEMBLY ITEMS
					EXEC USP_ManageInventoryAssemblyWO @numUserCntID,@numOppID,@itemcode,@numWareHouseItemID,@QtyShipped,1
				END
                ELSE
				BEGIN       
					SET @numUnits = @numUnits - @QtyShipped
                                                        
                    IF @onHand >= @numUnits 
                    BEGIN                                    
                        SET @onHand = @onHand - @numUnits                            
                        SET @onAllocation = @onAllocation + @numUnits                                    
                    END                                    
                    ELSE IF @onHand < @numUnits 
                    BEGIN                                    
                        SET @onAllocation = @onAllocation + @onHand                                    
                        SET @onBackOrder = @onBackOrder + @numUnits
                            - @onHand                                    
                        SET @onHand = 0                                    
                    END    
			                                 

					IF @bitAsset=0--Not Asset
					BEGIN 
						UPDATE  WareHouseItems
						SET     numOnHand = @onHand,
								numAllocation = @onAllocation,
								numBackOrder = @onBackOrder,dtModified = GETDATE() 
						WHERE   numWareHouseItemID = @numWareHouseItemID  


						-- IF REORDER POINT IS ENABLED FROM GLOABAL SETTING THEN CREATE PURCHASE ORDER
						If (@onHand + @onOrder <= @onReOrder) AND @bitReOrderPoint = 1
						BEGIN
							BEGIN TRY
								DECLARE @numNewOppID AS NUMERIC(18,0) = 0
								EXEC USP_OpportunityMaster_CreatePO @numNewOppID OUTPUT,@numDomainID,@numUserCntID,@itemcode,@onBackOrder,@numWareHouseItemID,0,@numReOrderPointOrderStatus
							END TRY
							BEGIN CATCH
								--WE ARE NOT THROWING ERROR
							END CATCH
						END         
					END
				END  
            END                       
            ELSE IF @tintOpptype = 2 
            BEGIN  
				SET @description='PO insert/edit (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Received:' +  CAST(@QtyReceived AS VARCHAR(10)) + ')'
		            
				/* When QtyReceived has been updated form purchase fulfillment then it need to be deducted */
                SET @numUnits = @numUnits - @QtyReceived   
                                             
                SET @onOrder = @onOrder + @numUnits 
		    
                UPDATE  
					WareHouseItems
                SET     
					numOnHand = @onHand,
                    numAllocation = @onAllocation,
                    numBackOrder = @onBackOrder,
                    numOnOrder = @onOrder,dtModified = GETDATE() 
                WHERE   
					numWareHouseItemID = @numWareHouseItemID   
            END                                                                                                                                         
        END
	 
    ELSE IF @tintFlag = 2 --2:SO/PO Close
            BEGIN
	 	
                PRINT '@OppType=' + CONVERT(VARCHAR(5), @tintOpptype)           
                IF @tintOpptype = 1 
                    BEGIN   
					SET @description='SO Close (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'
                    
                    IF @bitKitParent = 1 
                    BEGIN
                        EXEC USP_UpdatingInventoryForEmbeddedKits @numoppitemtCode,
                            @numOrigUnits, 2,@numOppID,0,@numUserCntID
                    END  
					ELSE IF @bitWorkOrder = 1
					BEGIN
						SET @description='SO-WO Close (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'
						EXEC USP_ManageInventoryAssemblyWO @numUserCntID,@numOppID,@itemcode,@numWareHouseItemID,@QtyShipped,2
					END
                    ELSE
                    BEGIN       
                    
    /* When QtyShipped has been updated form sales fulfillment then it need to be deducted */
							SET @numUnits = @numUnits - @QtyShipped
					  IF @bitAsset=0--Not Asset
							  BEGIN
									SET @onAllocation = @onAllocation - @numUnits 
							  END
					  ELSE--Asset
							  BEGIN
									 SET @onAllocation = @onAllocation  
							  END
											           
		    
							UPDATE  WareHouseItems
							SET     numOnHand = @onHand,
									numAllocation = @onAllocation,
									numBackOrder = @onBackOrder,dtModified = GETDATE() 
							WHERE   numWareHouseItemID = @numWareHouseItemID   
					   END
	
                    END                
                ELSE IF @tintOpptype = 2 
                        BEGIN
					SET @description='PO Close (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Received:' +  CAST(@QtyReceived AS VARCHAR(10)) + ')'
                        
        /* When QtyReceived has been updated form purchase fulfillment then it need to be deducted */
                            SET @numUnits = @numUnits - @QtyReceived
            
							IF EXISTS(SELECT 1 FROM dbo.OpportunityMaster WHERE numOppID=@numOppID AND ISNULL(bitStockTransfer,0)=0)
							BEGIN
								--Updating the Average Cost
								IF @TotalOnHand + @numUnits = 0
								BEGIN
									  SET @monAvgCost = @monAvgCost --( ( @TotalOnHand * @monAvgCost ) + (@numUnits * @monPrice))
								END
								ELSE
								BEGIN
									  SET @monAvgCost = ( ( @TotalOnHand * @monAvgCost )
												+ (@numUnits * @monPrice))
								/ ( @TotalOnHand + @numUnits )
								END    
		                            
								UPDATE  item
								SET     monAverageCost = @monAvgCost
								WHERE   numItemCode = @itemcode
							END
		    
--                            SELECT  @OldOnHand = SUM(numonhand)
--                            FROM    WareHouseItems WHI
--                            WHERE   WHI.numitemid = @itemcode     
--                            SET @OldCost = @OldOnHand * @monAvgCost     
      
                            IF @onOrder >= @numUnits 
                                BEGIN            
                                    PRINT 'in @onOrder >= @numUnits '
                                    SET @onOrder = @onOrder - @numUnits            
                                    IF @onBackOrder >= @numUnits 
                                        BEGIN            
                                            PRINT 'in @onBackOrder >= @numUnits '
                                            PRINT @numUnits
                                            SET @onBackOrder = @onBackOrder
                                                - @numUnits            
                                            SET @onAllocation = @onAllocation
                                                + @numUnits            
                                        END            
                                    ELSE 
                                        BEGIN            
                                            SET @onAllocation = @onAllocation
                                                + @onBackOrder            
                                            SET @numUnits = @numUnits
                                                - @onBackOrder            
                                            SET @onBackOrder = 0            
                                            SET @onHand = @onHand + @numUnits            
                                        END         
                                END            
                            ELSE IF @onOrder < @numUnits 
                                    BEGIN            
                                        PRINT 'in @onOrder < @numUnits '
                                        SET @onHand = @onHand + @onOrder
                                        SET @onOrder = @numUnits - @onOrder
                                    END         
   -- To Find New Stock Details    
--    Print '@numUnits='+convert(varchar(5),@numUnits)    
--    Print '@monPrice='+Convert(varchar(5),@monPrice)    
--                            SET @NewCost = @numUnits * @monPrice    
    ---Set @NewAverageCost=(@OldCost+@NewCost)/@onHand    
    --Print '@NewAverageCost='+Convert(varchar(5),@NewAverageCost)    
                            UPDATE  WareHouseItems
                            SET     numOnHand = @onHand,
                                    numAllocation = @onAllocation,
                                    numBackOrder = @onBackOrder,
                                    numOnOrder = @onOrder,dtModified = GETDATE() 
                            WHERE   numWareHouseItemID = @numWareHouseItemID  
			                
						SELECT @dtItemReceivedDate=dtItemReceivedDate FROM OpportunityMaster WHERE numOppId=@numOppId
			                
                --ASk Carl for more Info
--                            UPDATE  Item
--                            SET     monAverageCost = @NewAverageCost
--                            WHERE   numItemCode = @itemcode         
                        END
            END

ELSE IF @tintFlag = 3 --3:SO/PO Re-Open
            BEGIN
	 	
                PRINT '@OppType=' + CONVERT(VARCHAR(5), @tintOpptype)           
                IF @tintOpptype = 1 
                    BEGIN         
					SET @description='SO Re-Open (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'
                    
                    IF @bitKitParent = 1 
                        BEGIN
                            EXEC USP_UpdatingInventoryForEmbeddedKits @numoppitemtCode,
                                @numOrigUnits, 3,@numOppID,0,@numUserCntID
                        END 
					ELSE IF @bitWorkOrder = 1
					BEGIN
						SET @description='SO-WO Re-Open (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'
						EXEC USP_ManageInventoryAssemblyWO @numUserCntID,@numOppID,@itemcode,@numWareHouseItemID,@QtyShipped,3
					END
                     ELSE
                     BEGIN
    /* When QtyShipped has been updated form sales fulfillment then it need to be deducted */
                        SET @numUnits = @numUnits - @QtyShipped
  
					 IF @bitAsset=0--Not Asset
							  BEGIN
									SET @onAllocation = @onAllocation + @numUnits
							  END
					  ELSE--Asset
							  BEGIN
									 SET @onAllocation = @onAllocation 
							  END
                                    
	    
                        UPDATE  WareHouseItems
                        SET     numOnHand = @onHand,
                                numAllocation = @onAllocation,
                                numBackOrder = @onBackOrder,dtModified = GETDATE() 
                        WHERE   numWareHouseItemID = @numWareHouseItemID
					  END
						               
                    END                
                ELSE IF @tintOpptype = 2 
                        BEGIN
					SET @description='PO Re-Open (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Received:' +  CAST(@QtyReceived AS VARCHAR(10)) + ')'
                        
        /* When QtyReceived has been updated form purchase fulfillment then it need to be deducted */
                            SET @numUnits = @numUnits - @QtyReceived
            
            
							--Updating the Average Cost
							IF EXISTS(SELECT 1 FROM dbo.OpportunityMaster WHERE numOppID=@numOppID AND ISNULL(bitStockTransfer,0)=0)
							BEGIN
								--IF @onHand + @onOrder - @numUnits <> 0 
								--BEGIN
								IF @TotalOnHand - @numUnits = 0
								BEGIN
									SET @monAvgCost = 0
	--								SET @monAvgCost = ( ( @TotalOnHand * @monAvgCost )
	--                                                     - (@numUnits * @monPrice))
								END
								ELSE
								BEGIN
									SET @monAvgCost = ( ( @TotalOnHand * @monAvgCost )
														 - (@numUnits * @monPrice))
										/ ( @TotalOnHand - @numUnits )
								END        
								--END
							--ELSE 
								--SET @monAvgCost = 0
                            
                            
								 UPDATE  item
								 SET     monAverageCost = @monAvgCost
								 WHERE   numItemCode = @itemcode
							END

                            IF @onHand >= @numUnits 
                                BEGIN     
                                     SET @onHand = @onHand - @numUnits            
                                     SET @onOrder = @onOrder + @numUnits
                                END         
                          
						   UPDATE  WareHouseItems
                            SET     numOnHand = @onHand,
                                    numOnOrder = @onOrder,dtModified = GETDATE() 
                            WHERE   numWareHouseItemID = @numWareHouseItemID  
                            
                        END
            END
            
            
            
             IF @numWareHouseItemID>0
				 BEGIN
					DECLARE @tintRefType AS TINYINT
					
					IF @tintOpptype = 1 --SO
					  SET @tintRefType=3
					ELSE IF @tintOpptype = 2 --PO
					  SET @tintRefType=4
					  
						DECLARE @numDomain AS NUMERIC(18,0)
						SET @numDomain = (SELECT TOP 1 [numDomainID] FROM WareHouseItems WHERE [WareHouseItems].[numWareHouseItemID] = @numWareHouseItemID)

						EXEC dbo.USP_ManageWareHouseItems_Tracking
						@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
						@numReferenceID = @numOppID, --  numeric(9, 0)
						@tintRefType = @tintRefType, --  tinyint
						@vcDescription = @description, --  varchar(100)
						@numModifiedBy = @numUserCntID,
						@dtRecordDate =  @dtItemReceivedDate,
						@numDomainID = @numDomain
                  END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageInventoryAssemblyWO')
DROP PROCEDURE USP_ManageInventoryAssemblyWO
GO
CREATE PROCEDURE [dbo].[USP_ManageInventoryAssemblyWO]
	@numUserCntID AS NUMERIC(18,0),
	@numOppID AS NUMERIC(9),
	@numItemCode as numeric(9),
	@numWarehouseItemID AS NUMERIC(18,0),
	@QtyShipped AS NUMERIC(18,0),
	@tintMode AS TINYINT=0 --1: Work Order insert/edit 2:Work Order Close 3:Work Order Re-Open 4: Work Order Delete 5: Work Order Edit(Revert Before New Insert)
AS
BEGIN
	/*  
		WHEN WORK ORDER IS CREATED FOR ASSEMBY THEN BOTH SO AND WO PROCESS ARE CARRIED OUT SEPERATELY.
		FOR EXAMPLE: LETS SAY WE HAVE "ASSEMBY ITEM" WHICH REQUIRED 2 QTY OF "ASSEMBLY SUB ITEM" 
		CURRENT INVENTORY STATUS FOR PARENT ITEM: ONHAND:1 ONORDER:0 ONALLOCATION:0 ONBACKORDER:0
		CURRENT INVENTORY STATUS FOR CHILD ITEM: ONHAND:4 ONORDER:0 ONALLOCATION:0 ONBACKORDER:0

		-- WHEN WORK ORDER IS CREATED FOR 1 QTY:
		CURRENT INVENTORY STATUS FOR PARENT ITEM: ONHAND:0 ONORDER:1 ONALLOCATION:1 ONBACKORDER:0
		CURRENT INVENTORY STATUS FOR CHILD ITEM: ONHAND:2 ONORDER:0 ONALLOCATION:2 ONBACKORDER:0

		HERE PARENT ITEM HAVE BOTH SO AND WO IMPACT. SO ONHAND DECREASE AND ONALLOCATION IS INCREASED
		BUT ITS WORK ORDER SO WE HAVE TO INCERASE ONORDER ALSO BY QTY. CHILD ITEM HAVE ONLY WO IMPACT MEANS 
		ONHAND IS DECREASE AND ONALLOCATION IS INCREASED

		-- WHEN ASSEMBLY IS COMPLETED
		CURRENT INVENTORY STATUS FOR PARENT ITEM: ONHAND:1 ONORDER:0 ONALLOCATION:1 ONBACKORDER:0
		CURRENT INVENTORY STATUS FOR CHILD ITEM: ONHAND:2 ONORDER:0 ONALLOCATION:0 ONBACKORDER:0

		-- WHEN WORK ORDER IS CLOSED:
		CURRENT INVENTORY STATUS FOR PARENT ITEM: ONHAND:1 ONORDER:0 ONALLOCATION:0 ONBACKORDER:0
		CURRENT INVENTORY STATUS FOR CHILD ITEM: ONHAND:2 ONORDER:0 ONALLOCATION:0 ONBACKORDER:0
	*/
DECLARE @numWOID AS NUMERIC(18,0)
DECLARE @numParentWOID AS NUMERIC(18,0)
DECLARE	@numDomainID AS NUMERIC(18,0)
DECLARE @vcInstruction AS VARCHAR(1000)
DECLARE @bintCompliationDate DATETIME
DECLARE @numAssemblyQty NUMERIC(18,0) 
DECLARE @numWOStatus AS NUMERIC(18,0)
DECLARE @numWOAssignedTo AS NUMERIC(18,0)

SET XACT_ABORT ON;

BEGIN TRY 
BEGIN TRAN
	SELECT 
		@numWOID=numWOId,@numAssemblyQty=numQtyItemsReq,@numDomainID=numDomainID,
		@numWOStatus=ISNULL(numWOStatus,0),@vcInstruction=vcInstruction,
		@bintCompliationDate= bintCompliationDate, @numWOAssignedTo=numAssignedTo,@numParentWOID=ISNULL(numParentWOID,0)
	FROM 
		WorkOrder 
	WHERE 
		numOppId=@numOppID AND numItemCode=@numItemCode AND numWareHouseItemId=@numWarehouseItemID

	--IF WORK ORDER IS FOUND FOR ITEM AND QTY IS GREATER THEN 0 THEN PROCESS INVENTORY
	IF ISNULL(@numWOID,0) > 0 AND ISNULL(@numAssemblyQty,0) > 0
	BEGIN
		
		DECLARE @TEMPITEM AS TABLE
		(
			RowNo INT NOT NULL identity(1,1),
			numItemCode INT,
			numQty INT,
			numOrgQtyRequired INT,
			numWarehouseItemID INT,
			bitAssembly BIT
		)

		INSERT INTO @TEMPITEM SELECT numChildItemID,numQtyItemsReq,numQtyItemsReq_Orig,numWareHouseItemId,Item.bitAssembly FROM WorkOrderDetails INNER JOIN Item ON WorkOrderDetails.numChildItemID=Item.numItemCode WHERE numWOId = @numWOID AND ISNULL(numWareHouseItemId,0) > 0

		DECLARE @i AS INT = 1
		DECLARE @Count AS INT

		SELECT @Count = COUNT(RowNo) FROM @TEMPITEM

		--IF ASSEMBLY SUB ITEMS ARE AVAILABLE THEN UPDATE INVENTORY OF SUB ITEMS
		IF ISNULL(@Count,0) > 0
		BEGIN
			DECLARE @Description AS VARCHAR(500)
			DECLARE @numChildItemCode AS INT
			DECLARE @numChildItemQty AS INT
			DECLARE @numChildOrgQtyRequired AS INT
			DECLARE @bitChildIsAssembly AS BIT
			DECLARE @numQuantityToBuild AS INT
			DECLARE @numChildItemWarehouseItemID AS INT
			DECLARE @numOnHand AS INT
			DECLARE @numOnOrder AS INT
			DECLARE @numOnAllocation AS INT
			DECLARE @numOnBackOrder AS INT

			--THIS BLOCKS UPDATE INVENTORY LEVEL OF TOP LEVEL PARENT ONLY AND NOT CALLED FOR ITS RECURSIVE CHILD
			IF @numParentWOID = 0
			BEGIN
				--GET ASSEMBY ITEM INVENTORY
				SELECT
					@numOnHand = ISNULL(numOnHand, 0),
					@numOnAllocation = ISNULL(numAllocation, 0),
					@numOnOrder = ISNULL(numOnOrder, 0),
					@numOnBackOrder = ISNULL(numBackOrder, 0)
				FROM
					WareHouseItems
				WHERE
					numWareHouseItemID = @numWarehouseItemID

				IF @tintMode = 1 --WO INSERT
				BEGIN
					-- IF HAVE ENOUGH ITEM QTY IN WAREHOUSE THEN 
					-- DECREASE ONHAND BY QTY AND 
					-- INCREASE ONALLOCATION BY QTY
					-- ONORDER GOES UP BY ASSEMBLY QTY
					IF @numOnHand >= @numAssemblyQty 
					BEGIN      
						SET @numOnHand = @numOnHand - @numAssemblyQty
						SET @numOnOrder = @numOnOrder + @numAssemblyQty
						SET @numOnAllocation = @numOnAllocation + @numAssemblyQty

						UPDATE 
							WareHouseItems
						SET    
							numOnHand = @numOnHand,
							numOnOrder= @numOnOrder,
							numAllocation = @numOnAllocation,
							dtModified = GETDATE() 
						WHERE   
							numWareHouseItemID = @numWareHouseItemID                                                              
					END    
					-- IF DO NOT HAVE ENOUGH ITEM QTY IN WAREHOUSE THEN 
					-- INCREASE ONALLOCATION BY QTY WE HAVE IN WAREHOUSE AND
					-- REMAINING QTY AFTER ALLLOCATING FROM ONHAND WILL GO TO BACKORDER
					-- SET ONHAND TO 0 BECAUSE NOTHING LEFT NOW IN WAREHOUSE FOR ITEM 
					-- ONORDER GOES UP BY ASSEMBLY QTY				 
					ELSE IF @numOnHand < @numAssemblyQty 
					BEGIN      
						SET @numOnAllocation = @numOnAllocation + @numOnHand
						SET @numOnBackOrder = @numOnBackOrder + (@numAssemblyQty - @numOnHand)
						SET @numOnOrder = @numOnOrder + @numAssemblyQty
						SET @numOnHand = 0
 
						UPDATE 
							WareHouseItems
						SET    
							numAllocation = @numOnAllocation,
							numBackOrder = @numOnBackOrder,
							numOnHand = @numOnHand,		
							numOnOrder= @numOnOrder,				
							dtModified = GETDATE() 
						WHERE   
							numWareHouseItemID = @numWareHouseItemID                                     
					END 
				--UPDATE ONORDER QTY OF ASSEMBLY ITEM BY QTY
				END
				ELSE IF @tintMode = 2 --WO CLOSE
				BEGIN
					--REMOVE SHIPPED QTY FROM ORIGIAL QUANTITY
					SET @numAssemblyQty = ISNULL(@numAssemblyQty,0) - ISNULL(@QtyShipped,0)

					--RELASE ALLOCATION
					SET @numOnAllocation = @numOnAllocation - @numAssemblyQty 
											           
					UPDATE  
						WareHouseItems
					SET     
						numAllocation = @numOnAllocation,
						dtModified = GETDATE() 
					WHERE   
						numWareHouseItemID = @numWareHouseItemID   
				END
				ELSE IF @tintMode = 3 --WO REOPEN
				BEGIN
					--REMOVE SHIPPED QTY FROM ORIGIAL QUANTITY
					SET @numAssemblyQty = @numAssemblyQty - @QtyShipped
  
					--ADD ITEM TO ALLOCATION
					SET @numOnAllocation = @numOnAllocation + @numAssemblyQty
                                    
					UPDATE  
						WareHouseItems
					SET     
						numAllocation = @numOnAllocation,
						dtModified = GETDATE() 
					WHERE   
						numWareHouseItemID = @numWareHouseItemID
				END
				ELSE IF (@tintMode = 4 OR  @tintMode=5) --WO DELETE OR EDIT
				BEGIN
					-- CHECK WHETHER ANY QTY IS SHIPPED OR NOT
					IF @QtyShipped > 0
					BEGIN
						IF @tintMode=4
								SET @numAssemblyQty = @numAssemblyQty - @QtyShipped
						ELSE IF @tintmode=5
								SET @numOnAllocation = @numOnAllocation + @QtyShipped 
					END 
	
					-- IF BACKORDER QTY IS GREATER THEN QTY TO REVERT THEN
					-- DECREASE BACKORDER QTY BY QTY TO REVERT
					IF @numAssemblyQty < @numOnBackOrder 
					BEGIN                  
						SET @numOnBackOrder = @numOnBackOrder - @numAssemblyQty
					END 
					-- IF BACKORDER QTY IS LESS THEN OR EQUAL TO QTY TO REVERT THEN
					-- DECREASE QTY TO REVERT BY QTY OF BACK ORDER AND
					-- SET BACKORDER QTY TO 0
					ELSE IF @numAssemblyQty >= @numOnBackOrder 
					BEGIN
						SET @numAssemblyQty = @numAssemblyQty - @numOnBackOrder
						SET @numOnBackOrder = 0
                        
						--REMOVE ITEM FROM ALLOCATION 
						IF (@numOnAllocation - @numAssemblyQty) >= 0
							SET @numOnAllocation = @numOnAllocation - @numAssemblyQty
						
						--ADD QTY TO ONHAND
						SET @numOnHand = @numOnHand + @numAssemblyQty
					END

					--THIS IF BLOCK SHOULD BE LAST OTHERWISE IT WILL CREATE PROBLEM IN INVENTORY LEVEL
					--IF WORK ORDER IS NOT COMPLETED THEN REVERT 
					--QTY FROM ONORDER BECAUSE WE HAVE TO REVERT
					IF @numWOStatus <> 23184
					BEGIN
						--DECREASE ONORDER BY ASSEMBLY QTY
						SET @numOnOrder = @numOnOrder - @numAssemblyQty
					END
					ELSE
					BEGIN
						--DECREASE ONHAND BY ASSEMBLY QTY
						--BECAUSE WHEN WORK ORDER IS COMPLETED ONHAND QTY OF PARENT IS INCREASED
						SET @numOnHand = @numOnHand - @numAssemblyQty
					END

					UPDATE 
						WareHouseItems
					SET    
						numOnHand = @numOnHand,
						numAllocation = @numOnAllocation,
						numBackOrder = @numOnBackOrder,
						numOnOrder = @numOnOrder,
						dtModified = GETDATE() 
					WHERE   
						numWareHouseItemID = @numWareHouseItemID				
				END
			END

			--CHILD ITEM INVENTORY IS CHANGED ONLY WHEN SALES ORDER WITH WORK ORDER IS INSERTED OR DELETED
			--THE INVENTORY MANGMENT FOR DELETE ACTION IS HANDLED FROM STORE PROCEDURE USP_DeleteAssemblyWOAndInventory
			IF @tintMode = 1
			BEGIN
				
				WHILE @i <= @Count
				BEGIN
					DECLARE @numNewOppID AS NUMERIC(18,0) = 0
					DECLARE @QtyToBuild AS NUMERIC(18,0)
					DECLARE @numQtyShipped AS NUMERIC(18,0)

					--GET CHILD ITEM DETAIL FROM TEMP TABLE
					SELECT @numChildItemCode=numItemCode,@numChildItemWarehouseItemID=numWarehouseItemID,@numChildItemQty=numQty,@numChildOrgQtyRequired=numOrgQtyRequired,@bitChildIsAssembly=bitAssembly FROM @TEMPITEM WHERE RowNo = @i
					
					--GET CHILD ITEM CURRENT INVENTORY DETAIL
					SELECT  
						@numOnHand = ISNULL(numOnHand, 0),
						@numOnAllocation = ISNULL(numAllocation, 0),
						@numOnOrder = ISNULL(numOnOrder, 0),
						@numOnBackOrder = ISNULL(numBackOrder, 0)
					FROM    
						WareHouseItems
					WHERE   
						numWareHouseItemID = @numChildItemWarehouseItemID
					
					IF ISNULL(@numParentWOID,0) = 0
					BEGIN
						SET @Description='Items Allocated For SO-WO (Qty:' + CAST(@numChildItemQty AS VARCHAR(10)) + ' Shipped:' +  CAST((@QtyShipped * @numChildOrgQtyRequired) AS VARCHAR(10)) + ')'
					END
					ELSE
					BEGIN
						SET @Description='Items Allocated For SO-WO Work Order (Qty:' + CAST(@numChildItemQty AS VARCHAR(10)) + ' Shipped:' +  CAST((@QtyShipped * @numChildOrgQtyRequired) AS VARCHAR(10)) + ')'
					END
					
					SET @numChildItemQty = @numChildItemQty - (@QtyShipped * @numChildOrgQtyRequired)
						
					--IF CHILD ITEM IS ASSEMBLY THEN SAME LOGIC LIKE PARENT IS APPLIED
					IF ISNULL(@bitChildIsAssembly,0) = 1
					BEGIN
						-- IF HAVE ENOUGH ITEM QTY IN WAREHOUSE THEN 
						-- DECREASE ONHAND BY QTY AND 
						-- INCREASE ONALLOCATION BY QTY
						-- ONORDER GOES UP BY ASSEMBLY QTY
						IF @numOnHand >= @numChildItemQty 
						BEGIN      
							SET @numOnHand = @numOnHand - @numChildItemQty
							SET @numOnAllocation = @numOnAllocation + @numChildItemQty  
							
							--DONOT PLACE THIS LOGIC AT BOTTOM BECAUSE WORK ORDER ARE CREATED RECURSIVELY WHICH CREATE PROBLEM IN WAREHOUSE TRAKING ENTRIES
							--UPDATE WAREHOUSE INVENTORY AND WAREHOUSE HISTORY
							EXEC USP_UpdateInventoryAndTracking 
							@numOnHand=@numOnHand,
							@numOnAllocation=@numOnAllocation,
							@numOnBackOrder=@numOnBackOrder,
							@numOnOrder=@numOnOrder,
							@numWarehouseItemID=@numChildItemWarehouseItemID,
							@numReferenceID=@numOppID,
							@tintRefType=3,
							@numDomainID=@numDomainID,
							@numUserCntID=@numUserCntID,
							@Description=@Description                                                         
						END    
						-- IF DO NOT HAVE ENOUGH ITEM QTY IN WAREHOUSE THEN 
						-- INCREASE ONALLOCATION BY QTY WE HAVE IN WAREHOUSE AND
						-- REMAINING QTY AFTER ALLLOCATING FROM ONHAND WILL GO TO BACKORDER
						-- SET ONHAND TO 0 BECAUSE NOTHING LEFT NOW IN WAREHOUSE FOR ITEM 
						-- ONORDER GOES UP BY ASSEMBLY QTY				 
						ELSE IF @numOnHand < @numChildItemQty 
						BEGIN      
							SET @numOnAllocation = @numOnAllocation + @numOnHand
							SET @numOnBackOrder = @numOnBackOrder + (@numChildItemQty - @numOnHand)
							SET @QtyToBuild = (@numChildItemQty - @numOnHand)
							SET @numQtyShipped = (@QtyShipped * @numChildOrgQtyRequired)
							SET @numOnHand = 0   

							--DONOT PLACE THIS LOGIC AT BOTTOM BECAUSE WORK ORDER ARE CREATED RECURSIVELY WHICH CREATE PROBLEM IN WAREHOUSE TRAKING ENTRIES
							--UPDATE WAREHOUSE INVENTORY AND WAREHOUSE HISTORY
							EXEC USP_UpdateInventoryAndTracking 
							@numOnHand=@numOnHand,
							@numOnAllocation=@numOnAllocation,
							@numOnBackOrder=@numOnBackOrder,
							@numOnOrder=@numOnOrder,
							@numWarehouseItemID=@numChildItemWarehouseItemID,
							@numReferenceID=@numOppID,
							@tintRefType=3,
							@numDomainID=@numDomainID,
							@numUserCntID=@numUserCntID,
							@Description=@Description
								
							EXEC USP_WorkOrder_InsertRecursive @numOppID,@numChildItemCode,@QtyToBuild,@numChildItemWarehouseItemID,@numDomainID,@numUserCntID,@numQtyShipped,@numWOID,0
						END 
					END
					ELSE
					BEGIN
						-- IF HAVE ENOUGH ITEM QTY IN WAREHOUSE THEN 
						-- DECREASE ONHAND BY QTY AND 
						-- INCREASE ONALLOCATION BY QTY
						IF @numOnHand >= @numChildItemQty 
						BEGIN                                    
							SET @numOnHand = @numOnHand - @numChildItemQty                            
							SET @numOnAllocation = @numOnAllocation + @numChildItemQty 
							
							--DONOT PLACE THIS LOGIC AT BOTTOM BECAUSE WORK ORDER ARE CREATED RECURSIVELY WHICH CREATE PROBLEM IN WAREHOUSE TRAKING ENTRIES
							--UPDATE WAREHOUSE INVENTORY AND WAREHOUSE HISTORY
							EXEC USP_UpdateInventoryAndTracking 
							@numOnHand=@numOnHand,
							@numOnAllocation=@numOnAllocation,
							@numOnBackOrder=@numOnBackOrder,
							@numOnOrder=@numOnOrder,
							@numWarehouseItemID=@numChildItemWarehouseItemID,
							@numReferenceID=@numOppID,
							@tintRefType=3,
							@numDomainID=@numDomainID,
							@numUserCntID=@numUserCntID,
							@Description=@Description                                   
						END    
						-- IF DO NOT HAVE ENOUGH ITEM QTY IN WAREHOUSE THEN 
						-- INCREASE ONALLOCATION BY QTY WE HAVE IN WAREHOUSE AND
						-- REMAINING QTY AFTER ALLLOCATING FROM ONHAND WILL GO TO BACKORDER
						-- SET ONHAND TO 0 BECAUSE NOTHING LEFT NOW IN WAREHOUSE FOR ITEM 				 
						ELSE IF @numOnHand < @numChildItemQty 
						BEGIN     
								
							SET @numOnAllocation = @numOnAllocation + @numOnHand  
							SET @numOnBackOrder = @numOnBackOrder + (@numChildItemQty - @numOnHand) 
							SET @QtyToBuild = (@numChildItemQty - @numOnHand) 
							SET @numOnHand = 0 

							--DONOT PLACE THIS LOGIC AT BOTTOM BECAUSE WORK ORDER ARE CREATED RECURSIVELY WHICH CREATE PROBLEM IN WAREHOUSE TRAKING ENTRIES
							--UPDATE WAREHOUSE INVENTORY AND WAREHOUSE HISTORY
							EXEC USP_UpdateInventoryAndTracking 
							@numOnHand=@numOnHand,
							@numOnAllocation=@numOnAllocation,
							@numOnBackOrder=@numOnBackOrder,
							@numOnOrder=@numOnOrder,
							@numWarehouseItemID=@numChildItemWarehouseItemID,
							@numReferenceID=@numOppID,
							@tintRefType=3,
							@numDomainID=@numDomainID,
							@numUserCntID=@numUserCntID,
							@Description=@Description

							--CREATE PURCHASE ORDER FOR QUANTITY WHICH GOES ON BACKORDER
							EXEC USP_OpportunityMaster_CreatePO @numNewOppID OUTPUT,@numDomainID,@numUserCntID,@numChildItemCode,@QtyToBuild,@numChildItemWarehouseItemID,1,0
								
							UPDATE WorkOrderDetails SET numPOID= @numNewOppID WHERE numWOId=@numWOID AND numChildItemID=@numChildItemCode AND numWareHouseItemId=@numChildItemWarehouseItemID                                 
						END 
					END
 
					
					SET @i = @i + 1
				END
			END
		END
	END
COMMIT TRAN 
END TRY 
BEGIN CATCH		
    IF ( @@TRANCOUNT > 0 ) 
    BEGIN
        ROLLBACK TRAN
    END

	DECLARE @ErrorMessage NVARCHAR(4000);
    DECLARE @ErrorSeverity INT;
    DECLARE @ErrorState INT;

    SELECT @ErrorMessage = ERROR_MESSAGE(), @ErrorSeverity = ERROR_SEVERITY(), @ErrorState = ERROR_STATE();

    RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);
END CATCH	

SET XACT_ABORT OFF;
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageInventoryWorkOrder')
DROP PROCEDURE USP_ManageInventoryWorkOrder
GO
CREATE PROCEDURE [dbo].[USP_ManageInventoryWorkOrder]
@numWOId AS NUMERIC(18,0),
@numDomainID AS NUMERIC(18,0),
@numUserCntID AS NUMERIC(18,0)
AS
BEGIN
BEGIN TRY 
BEGIN TRAN
		
	DECLARE @TEMPITEM AS TABLE
	(
		RowNo INT NOT NULL identity(1,1),
		numItemCode INT,
		numQty INT,
		numOrgQtyRequired INT,
		numWarehouseItemID INT,
		bitAssembly BIT
	)

	INSERT INTO @TEMPITEM SELECT numChildItemID,numQtyItemsReq,numQtyItemsReq_Orig,numWareHouseItemId,Item.bitAssembly FROM WorkOrderDetails INNER JOIN Item ON WorkOrderDetails.numChildItemID=Item.numItemCode WHERE numWOId = @numWOID AND ISNULL(numWareHouseItemId,0) > 0

	DECLARE @i AS INT = 1
	DECLARE @Count AS INT

	SELECT @Count = COUNT(RowNo) FROM @TEMPITEM

	--IF ASSEMBLY SUB ITEMS ARE AVAILABLE THEN UPDATE INVENTORY OF SUB ITEMS
	IF ISNULL(@Count,0) > 0
	BEGIN
		DECLARE @Description AS VARCHAR(1000)
		DECLARE @numChildItemCode AS INT
		DECLARE @numChildItemQty AS INT
		DECLARE @numChildOrgQtyRequired AS INT
		DECLARE @bitChildIsAssembly AS BIT
		DECLARE @numQuantityToBuild AS INT
		DECLARE @numChildItemWarehouseItemID AS INT
		DECLARE @numOnHand AS INT
		DECLARE @numOnOrder AS INT
		DECLARE @numOnAllocation AS INT
		DECLARE @numOnBackOrder AS INT

		WHILE @i <= @Count
		BEGIN
			DECLARE @numNewOppID AS NUMERIC(18,0) = 0
			DECLARE @QtyToBuild AS NUMERIC(18,0)
			DECLARE @numQtyShipped AS NUMERIC(18,0)

			--GET CHILD ITEM DETAIL FROM TEMP TABLE
			SELECT @numChildItemCode=numItemCode,@numChildItemWarehouseItemID=numWarehouseItemID,@numChildItemQty=numQty,@numChildOrgQtyRequired=numOrgQtyRequired,@bitChildIsAssembly=bitAssembly FROM @TEMPITEM WHERE RowNo = @i
					
			--GET CHILD ITEM CURRENT INVENTORY DETAIL
			SELECT  
				@numOnHand = ISNULL(numOnHand, 0),
				@numOnAllocation = ISNULL(numAllocation, 0),
				@numOnOrder = ISNULL(numOnOrder, 0),
				@numOnBackOrder = ISNULL(numBackOrder, 0)
			FROM    
				WareHouseItems
			WHERE   
				numWareHouseItemID = @numChildItemWarehouseItemID	

			SET @Description='Items Allocated For Work Order (Qty:' + CAST(@numChildItemQty AS VARCHAR(10)) + ')'
						
			--IF CHILD ITEM IS ASSEMBLY THEN SAME LOGIC LIKE PARENT IS APPLIED
			IF ISNULL(@bitChildIsAssembly,0) = 1
			BEGIN
				-- IF HAVE ENOUGH ITEM QTY IN WAREHOUSE THEN 
				-- DECREASE ONHAND BY QTY AND 
				-- INCREASE ONALLOCATION BY QTY
				-- ONORDER GOES UP BY ASSEMBLY QTY
				IF @numOnHand >= @numChildItemQty 
				BEGIN      
					SET @numOnHand = @numOnHand - @numChildItemQty
					SET @numOnAllocation = @numOnAllocation + @numChildItemQty  
					
					--DONOT PLACE THIS LOGIC AT BOTTOM BECAUSE WORK ORDER ARE CREATED RECURSIVELY WHICH CREATE PROBLEM IN WAREHOUSE TRAKING ENTRIES
					--UPDATE WAREHOUSE INVENTORY AND WAREHOUSE HISTORY
					EXEC USP_UpdateInventoryAndTracking 
					@numOnHand=@numOnHand,
					@numOnAllocation=@numOnAllocation,
					@numOnBackOrder=@numOnBackOrder,
					@numOnOrder=@numOnOrder,
					@numWarehouseItemID=@numChildItemWarehouseItemID,
					@numReferenceID=@numWOId,
					@tintRefType=2,
					@numDomainID=@numDomainID,
					@numUserCntID=@numUserCntID,
					@Description=@Description                                                         
				END    
				-- IF DO NOT HAVE ENOUGH ITEM QTY IN WAREHOUSE THEN 
				-- INCREASE ONALLOCATION BY QTY WE HAVE IN WAREHOUSE AND
				-- REMAINING QTY AFTER ALLLOCATING FROM ONHAND WILL GO TO BACKORDER
				-- SET ONHAND TO 0 BECAUSE NOTHING LEFT NOW IN WAREHOUSE FOR ITEM 
				-- ONORDER GOES UP BY ASSEMBLY QTY				 
				ELSE IF @numOnHand < @numChildItemQty 
				BEGIN      
					SET @numOnAllocation = @numOnAllocation + @numOnHand
					SET @numOnBackOrder = @numOnBackOrder + (@numChildItemQty - @numOnHand)
					SET @QtyToBuild = (@numChildItemQty - @numOnHand)
					SET @numQtyShipped = 0
					SET @numOnHand = 0   
					
					--DONOT PLACE THIS LOGIC AT BOTTOM BECAUSE WORK ORDER ARE CREATED RECURSIVELY WHICH CREATE PROBLEM IN WAREHOUSE TRAKING ENTRIES
					--UPDATE WAREHOUSE INVENTORY AND WAREHOUSE HISTORY	
					EXEC USP_UpdateInventoryAndTracking 
					@numOnHand=@numOnHand,
					@numOnAllocation=@numOnAllocation,
					@numOnBackOrder=@numOnBackOrder,
					@numOnOrder=@numOnOrder,
					@numWarehouseItemID=@numChildItemWarehouseItemID,
					@numReferenceID=@numWOId,
					@tintRefType=2,
					@numDomainID=@numDomainID,
					@numUserCntID=@numUserCntID,
					@Description=@Description 

					EXEC USP_WorkOrder_InsertRecursive 0,@numChildItemCode,@QtyToBuild,@numChildItemWarehouseItemID,@numDomainID,@numUserCntID,@numQtyShipped,@numWOID,1
				END 
			END
			ELSE
			BEGIN
				-- IF HAVE ENOUGH ITEM QTY IN WAREHOUSE THEN 
				-- DECREASE ONHAND BY QTY AND 
				-- INCREASE ONALLOCATION BY QTY
				IF @numOnHand >= @numChildItemQty 
				BEGIN                                    
					SET @numOnHand = @numOnHand - @numChildItemQty                            
					SET @numOnAllocation = @numOnAllocation + @numChildItemQty        
					
					--DONOT PLACE THIS LOGIC AT BOTTOM BECAUSE WORK ORDER ARE CREATED RECURSIVELY WHICH CREATE PROBLEM IN WAREHOUSE TRAKING ENTRIES
					--UPDATE WAREHOUSE INVENTORY AND WAREHOUSE HISTORY
					EXEC USP_UpdateInventoryAndTracking 
					@numOnHand=@numOnHand,
					@numOnAllocation=@numOnAllocation,
					@numOnBackOrder=@numOnBackOrder,
					@numOnOrder=@numOnOrder,
					@numWarehouseItemID=@numChildItemWarehouseItemID,
					@numReferenceID=@numWOId,
					@tintRefType=2,
					@numDomainID=@numDomainID,
					@numUserCntID=@numUserCntID,
					@Description=@Description                             
				END    
				-- IF DO NOT HAVE ENOUGH ITEM QTY IN WAREHOUSE THEN 
				-- INCREASE ONALLOCATION BY QTY WE HAVE IN WAREHOUSE AND
				-- REMAINING QTY AFTER ALLLOCATING FROM ONHAND WILL GO TO BACKORDER
				-- SET ONHAND TO 0 BECAUSE NOTHING LEFT NOW IN WAREHOUSE FOR ITEM 				 
				ELSE IF @numOnHand < @numChildItemQty 
				BEGIN     
								
					SET @numOnAllocation = @numOnAllocation + @numOnHand  
					SET @numOnBackOrder = @numOnBackOrder + (@numChildItemQty - @numOnHand) 
					SET @QtyToBuild = (@numChildItemQty - @numOnHand) 
					SET @numOnHand = 0 

					--DONOT PLACE THIS LOGIC AT BOTTOM BECAUSE WORK ORDER ARE CREATED RECURSIVELY WHICH CREATE PROBLEM IN WAREHOUSE TRAKING ENTRIES
					--UPDATE WAREHOUSE INVENTORY AND WAREHOUSE HISTORY
					EXEC USP_UpdateInventoryAndTracking 
					@numOnHand=@numOnHand,
					@numOnAllocation=@numOnAllocation,
					@numOnBackOrder=@numOnBackOrder,
					@numOnOrder=@numOnOrder,
					@numWarehouseItemID=@numChildItemWarehouseItemID,
					@numReferenceID=@numWOId,
					@tintRefType=2,
					@numDomainID=@numDomainID,
					@numUserCntID=@numUserCntID,
					@Description=@Description 

					--CREATE PURCHASE ORDER FOR QUANTITY WHICH GOES ON BACKORDER
					EXEC USP_OpportunityMaster_CreatePO @numNewOppID OUTPUT,@numDomainID,@numUserCntID,@numChildItemCode,@QtyToBuild,@numChildItemWarehouseItemID,1,0
								
					UPDATE WorkOrderDetails SET numPOID= @numNewOppID WHERE numWOId=@numWOID AND numChildItemID=@numChildItemCode AND numWareHouseItemId=@numChildItemWarehouseItemID                                 
				END 
			END
					
			SET @i = @i + 1
		END
	END
COMMIT TRAN 
END TRY 
BEGIN CATCH		
    IF ( @@TRANCOUNT > 0 ) 
    BEGIN
        ROLLBACK TRAN
    END

	DECLARE @ErrorMessage NVARCHAR(4000);
    DECLARE @ErrorSeverity INT;
    DECLARE @ErrorState INT;

    SELECT @ErrorMessage = ERROR_MESSAGE(), @ErrorSeverity = ERROR_SEVERITY(), @ErrorState = ERROR_STATE();

    RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);
END CATCH	
END
/****** Object:  StoredProcedure [dbo].[USP_ManagekitParentsForChildItems]    Script Date: 07/26/2008 16:19:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--create by anoop jayaraj  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_managekitparentsforchilditems')
DROP PROCEDURE usp_managekitparentsforchilditems
GO
CREATE PROCEDURE [dbo].[USP_ManagekitParentsForChildItems]  
@numItemKitID as numeric(9)=0,  
@numChildItemID as numeric(9)=0,  
@numQtyItemsReq as numeric(9)=0,  
@byteMode as tinyint  
as  
if @byteMode=0  
begin  
DECLARE @numWarehouseItemID AS NUMERIC(18,0) = 0
SELECT TOP 1 @numWarehouseItemID=ISNULL(numWareHouseItemID,0) FROM WareHouseItems WHERE numItemID = @numChildItemID

delete from ItemDetails where numItemKitID=@numItemKitID and numChildItemID=@numChildItemID  
insert into ItemDetails (numItemKitID,numChildItemID,numQtyItemsReq,numWareHouseItemId)  
values(@numItemKitID,@numChildItemID,@numQtyItemsReq,@numWarehouseItemID)  
end  
  
if @byteMode=1  
begin  
delete from ItemDetails where numItemKitID=@numItemKitID and numChildItemID=@numChildItemID  

end
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_MirrorOPPBizDocItems' ) 
    DROP PROCEDURE USP_MirrorOPPBizDocItems
GO
CREATE PROCEDURE [dbo].[USP_MirrorOPPBizDocItems]
    (
      @numOppId AS NUMERIC(9) = NULL,
      @numDomainID AS NUMERIC(9) = 0      
    )
AS 
    DECLARE @DivisionID AS NUMERIC(9)      
    DECLARE @tintTaxOperator AS TINYINT   
    DECLARE @tintType AS TINYINT   
    DECLARE @tintOppStatus AS TINYINT   
    
    DECLARE @tintOppType AS TINYINT   
    DECLARE @numBizDocTempID AS NUMERIC(9) ;
      SET @numBizDocTempID = 0
                                                                                                                                                                                                                                                                                                                                          
    SELECT  @DivisionID = numDivisionID, @tintTaxOperator = [tintTaxOperator]  ,
    @tintType = tintOppType,@tintOppStatus=tintOppStatus,
            @tintOppType = tintOppType   ,@numBizDocTempID = ISNULL(numOppBizDocTempID, 0)      
    FROM    OpportunityMaster
    WHERE   numOppId = @numOppId    
                                                                                                                                                                          
--Decimal points to show in bizdoc 
    DECLARE @DecimalPoint AS TINYINT
    SELECT  @DecimalPoint = tintDecimalPoints
    FROM    domain
    WHERE   numDomainID = @numDomainID
                                                                                          
    DECLARE @strSQL AS VARCHAR(MAX)                                                                                          
    DECLARE @strSQL1 AS VARCHAR(8000)                                       
    DECLARE @strSQLCusFields VARCHAR(1000)                      
    DECLARE @strSQLEmpFlds VARCHAR(500)                                                                         
    SET @strSQLCusFields = ''                                                                                          
    SET @strSQLEmpFlds = ''                                                                                          
                                                   
    DECLARE @intRowNum AS INT                                                  
    DECLARE @numFldID AS VARCHAR(15)                                          
    DECLARE @vcFldname AS VARCHAR(50)                                            
    DECLARE @numBizDocId AS NUMERIC(9)
                                          
    
       IF @tintOppType=1 AND @tintOppStatus=0
			SELECT  @numBizDocID = numListItemID FROM dbo.ListDetails WHERE numListID=27 AND vcData='Sales Opportunity' AND constFlag=1
       ELSE IF @tintOppType=1 AND @tintOppStatus=1
			SELECT  @numBizDocID = numListItemID FROM dbo.ListDetails WHERE numListID=27 AND vcData='Sales Order' AND constFlag=1
       ELSE IF @tintOppType=2 AND @tintOppStatus=0
			SELECT  @numBizDocID = numListItemID FROM dbo.ListDetails WHERE numListID=27 AND vcData='Purchase Opportunity' AND constFlag=1
       ELSE IF @tintOppType=2 AND @tintOppStatus=1
	 		SELECT  @numBizDocID = numListItemID FROM dbo.ListDetails WHERE numListID=27 AND vcData='Purchase Order' AND constFlag=1
                                                                              
    IF @tintType = 1 
        SET @tintType = 7
    ELSE 
        SET @tintType = 8                                                                                      
                                                                                
    SELECT  *
    INTO    #Temp1
    FROM    ( SELECT   Opp.vcitemname AS vcItemName,
                        0 OppBizDocItemID,
                        CASE WHEN charitemType = 'P' THEN 'Product'
                             WHEN charitemType = 'S' THEN 'Service'
                        END AS charitemType,
                        CASE WHEN bitShowDeptItemDesc = 0
                             THEN CONVERT(VARCHAR(500), Opp.vcitemDesc)
                             WHEN bitShowDeptItemDesc = 1
                                  AND bitKitParent = 1
                                  AND bitAssembly = 0
                             THEN dbo.fn_PopulateKitDesc(opp.numOppId,
                                                         opp.numoppitemtCode)
                             WHEN bitShowDeptItemDesc = 1
                                  AND bitKitParent = 1
                                  AND bitAssembly = 1
                             THEN dbo.fn_PopulateAssemblyDesc(opp.numoppitemtCode, Opp.numUnitHour)
                             ELSE CONVERT(VARCHAR(500), Opp.vcitemDesc)
                        END AS txtItemDesc,                                      
--vcitemdesc as [desc],                                      
                        dbo.fn_UOMConversion(ISNULL(i.numBaseUnit, 0),
                                             i.numItemCode, @numDomainID,
                                             ISNULL(opp.numUOMId, 0))
                        * Opp.numUnitHour AS numUnitHour,
                        CONVERT(VARCHAR, CONVERT(DECIMAL(18, 4), dbo.fn_UOMConversion(ISNULL(opp.numUOMId, 0), i.numItemCode, @numDomainID, ISNULL(i.numBaseUnit, 0))
                        * Opp.monPrice)) monPrice,
                        CONVERT(VARCHAR, CONVERT(DECIMAL(18, 4), Opp.monTotAmount)) Amount,
                        Opp.monTotAmount/*Fo calculating sum*/,
--                        CASE WHEN @tintOppType = 1
--                             THEN CASE WHEN bitTaxable = 0 THEN 0
--                                       WHEN bitTaxable = 1
--                                       THEN ( SELECT    fltPercentage
--                                              FROM      OpportunityBizDocTaxItems
--                                              WHERE     numOppBizDocID = @numOppBizDocsId
--                                                        AND numTaxItemID = 0
--                                            )
--                                  END
--                             ELSE 0
--                        END AS Tax,
                        ISNULL(CONVERT(VARCHAR, i.monListPrice), 0) AS monListPrice,
                        i.numItemCode AS ItemCode,i.numItemCode,
                        opp.numoppitemtCode,
                        L.vcdata AS numItemClassification,
                        CASE WHEN bitTaxable = 0 THEN 'No'
                             ELSE 'Yes'
                        END AS Taxable,
                        numIncomeChartAcntId AS itemIncomeAccount,
                        'NI' AS ItemType,
                        0 AS numJournalId,
                        0 AS numTransactionId,
                        ISNULL(Opp.vcModelID, '') vcModelID,
                        dbo.USP_GetAttributes(Opp.numWarehouseItmsID,
                                              bitSerialized) AS vcAttributes,
                        ISNULL(vcPartNo, '') AS vcPartNo,
                        ( ISNULL(Opp.monTotAmtBefDiscount, Opp.monTotAmount)
                          - Opp.monTotAmount ) AS DiscAmt,
                        '' vcNotes,
                        '' vcTrackingNo,
                        '' vcShippingMethod,
                        0 monShipCost,
                        NULL dtDeliveryDate,
                        Opp.numWarehouseItmsID,
--ISNULL(I.vcUnitofMeasure,'Units') vcUnitofMeasure,
                        ISNULL(u.vcUnitName, '') numUOMId,
                        ISNULL(Opp.vcManufacturer, '') AS vcManufacturer,
                        ISNULL(V.monCost, 0) AS VendorCost,
                        Opp.vcPathForTImage,
                        i.[fltLength],
                        i.[fltWidth],
                        i.[fltHeight],
                        i.[fltWeight],
                        ISNULL(I.numVendorID, 0) numVendorID,
                        Opp.bitDropShip AS DropShip,
                        SUBSTRING(( SELECT  ' ,' + vcSerialNo
                                            + CASE WHEN ISNULL(I.bitLotNo, 0) = 1
                                                   THEN ' ('
                                                        + CONVERT(VARCHAR(15), oppI.numQty)
                                                        + ')'
                                                   ELSE ''
                                              END
                                    FROM    OppWarehouseSerializedItem oppI
                                            JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID = whi.numWareHouseItmsDTLID
                                    WHERE   oppI.numOppID = Opp.numOppId
                                            AND oppI.numOppItemID = Opp.numoppitemtCode
                                    ORDER BY vcSerialNo
                                  FOR
                                    XML PATH('')
                                  ), 3, 200000) AS SerialLotNo,
                        ISNULL(opp.numUOMId, 0) AS numUOM,
                        ISNULL(u.vcUnitName, '') vcUOMName,
                        dbo.fn_UOMConversion(opp.numUOMId, Opp.numItemCode,
                                             @numDomainID, NULL) AS UOMConversionFactor,
                        ISNULL(Opp.numSOVendorId, 0) AS numSOVendorId,
                        NULL dtRentalStartDate,
                        NULL dtRentalReturnDate,
-- Added for packing slip
                        ISNULL(W.vcWareHouse, '') AS Warehouse,
                        CASE WHEN charitemType = 'P' THEN WI.vcWHSKU
                             ELSE i.vcSKU
                        END vcSKU,
                        CASE WHEN charitemType = 'P' THEN WI.vcBarCode
                             ELSE i.numBarCodeId
                        END vcBarcode,
                        ISNULL(opp.numClassID, 0) AS numClassID,
                        ISNULL(opp.numProjectID, 0) AS numProjectID,
                        i.numShipClass
              FROM      OpportunityItems opp
                        LEFT JOIN item i ON opp.numItemCode = i.numItemCode
                        LEFT JOIN ListDetails L ON i.numItemClassification = L.numListItemID
                        LEFT JOIN Vendor V ON V.numVendorID = @DivisionID
                                              AND V.numItemCode = opp.numItemCode
                        LEFT JOIN UOM u ON u.numUOMId = opp.numUOMId                                    
-- left join General_Journal_Details GJD on GJH.numJournal_Id=GJD.numJournalId And GJD.chBizDocItems='NI' And GJD.numoppitemtCode=opp.numoppitemtCode                                       
                        LEFT JOIN dbo.WareHouseItems WI ON opp.numWarehouseItmsID = WI.numWareHouseItemID
                                                           AND WI.numDomainID = @numDomainID
                        LEFT JOIN dbo.Warehouses W ON WI.numDomainID = W.numDomainID
                                                      AND WI.numWareHouseID = W.numWareHouseID
              WHERE     opp.numOppId = @numOppId
       
--For Kit Items where following condition is true( Show dependant items as line items on BizDoc (instead of kit item name))
-- Commented by chintan, Reason: Bug #1829 
/*
union
select Opp.vcitemname as Item,                                      
case when i.charitemType='P' then 'Product' when i.charitemType='S' then 'Service' end as type,                                      
convert(varchar(500),Opp.vcitemDesc) as [Desc],                                      
--vcitemdesc as [desc],                                      
Opp.numUnitHour*intQuantity as Unit,
CONVERT(VARCHAR,CONVERT(DECIMAL(18, 4), Opp.monPrice)) Price,
CONVERT(VARCHAR,CONVERT(DECIMAL(18, 4), Opp.monTotAmount)) Amount,
Opp.monTotAmount/*Fo calculating sum*/,
case when @tintOppType=1 then case when i.bitTaxable =0 then 0 when i.bitTaxable=1 then (select fltPercentage from OpportunityBizDocTaxItems where numOppBizDocID=@numOppBizDocsId and numTaxItemID=0) end else 0 end  as Tax,                                    
isnull(convert(varchar,i.monListPrice),0) as monListPrice,i.numItemCode as ItemCode,opp.numoppitemtCode,                                      
L.vcdata as vcItemClassification,case when i.bitTaxable=0 then 'No' else 'Yes' end as Taxable,i.numIncomeChartAcntId as itemIncomeAccount,                                      
'NI' as ItemType                                      
,isnull(GJH.numJournal_Id,0) as numJournalId,(SELECT TOP 1 isnull(GJD.numTransactionId, 0) FROM General_Journal_Details GJD WHERE GJH.numJournal_Id = GJD.numJournalId And GJD.chBizDocItems = 'NI' And GJD.numoppitemtCode = opp.numoppitemtCode) as numTransactionId,isnull(Opp.vcModelID,'') vcModelID, dbo.USP_GetAttributes(Opp.numWarehouseItmsID,i.bitSerialized) as Attributes,isnull(vcPartNo,'') as Part 
,(isnull(Opp.monTotAmtBefDiscount,Opp.monTotAmount)-Opp.monTotAmount) as DiscAmt,Opp.vcNotes,Opp.vcTrackingNo,Opp.vcShippingMethod,Opp.monShipCost,[dbo].[FormatedDateFromDate](Opp.dtDeliveryDate,@numDomainID) dtDeliveryDate,Opp.numWarehouseItmsID,
--ISNULL(I.vcUnitofMeasure,'Units') vcUnitofMeasure,
ISNULL(u.vcUnitName,'') vcUnitofMeasure,
isnull(i2.vcManufacturer,'') as vcManufacturer,
isnull(V.monCost,0) as VendorCost,
Opp.vcPathForTImage,i.[fltLength],i.[fltWidth],i.[fltHeight],i.[fltWeight],isnull(I.numVendorID,0) numVendorID,Opp.bitDropShip AS DropShip,
SUBSTRING(
(SELECT ',' + vcSerialNo + CASE WHEN isnull(I.bitLotNo,0)=1 THEN ' (' + CONVERT(VARCHAR(15),oppI.numQty) + ')' ELSE '' end 
FROM OppWarehouseSerializedItem oppI JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID=whi.numWareHouseItmsDTLID where oppI.numOppID=Opp.numOppId and oppI.numOppItemID=Opp.numoppitemtCode 
ORDER BY vcSerialNo FOR XML PATH('')),2,200000) AS SerialLotNo,isnull(opp.numUOMId,0) AS numUOM,ISNULL(u.vcUnitName,'') vcUOMName,dbo.fn_UOMConversion(opp.numUOMId,Opp.numItemCode,@numDomainID,null) AS UOMConversionFactor,
ISNULL(Opp.numSOVendorId,0) as numSOVendorId,
[dbo].FormatedDateTimeFromDate(Opp.dtRentalStartDate,@numDomainID) dtRentalStartDate,
[dbo].FormatedDateTimeFromDate(Opp.dtRentalReturnDate,@numDomainID) dtRentalReturnDate
from OpportunityItems opp       
join OpportunityBizDocItems OBD on Opp.numOppItemID= opp.numoppitemtCode                                   
join  OpportunityKitItems OppKitItems      
on OppKitItems.numOppKitItem=opp.numoppitemtCode                                     
join item i on OppKitItems.numChildItem=i.numItemCode      
join item i2 on opp.numItemCode=i2.numItemCode                                      
left join ListDetails L on i.numItemClassification=L.numListItemID
left join Vendor V on V.numVendorID=@DivisionID and V.numItemCode=opp.numItemCode                                       
left join General_Journal_Header GJH on opp.numoppid=GJH.numOppId And GJH.numBizDocsPaymentDetId is null And GJH.numOppBizDocsId=@numOppBizDocsId                                      
-- left join General_Journal_Details GJD on GJH.numJournal_Id=GJD.numJournalId And GJD.chBizDocItems='NI' And GJD.numoppitemtCode=opp.numoppitemtCode
LEFT JOIN  UOM u ON u.numUOMId = opp.numUOMId                                    
where opp.numOppId=@numOppId and i2.bitShowDeptItem=1      and   Opp.numOppBizDocID=@numOppBizDocsId   */
--union
--select case when numcategory=1 then                             
--case when isnull(te.numcontractid,0) = 0 then 'Time' else 'Time(Contract)' end                              
--when numcategory=2 then                             
--case when isnull(te.numcontractid,0) = 0 then 'Expense' else 'Expense(Contract)' end                              
--end  as item,                           
--case when numcategory=1 then 'Time' when numcategory=2 then 'Expense' end                                      
-- as Type,                                      
--convert(varchar(100),txtDesc) as [Desc],                                      
----txtDesc as [Desc],                                      
--convert(decimal(18,2),datediff(minute,te.dtfromdate,te.dttodate))/60 as unit,                                      
--convert(varchar(100),monamount) as Price,                                      
--case when isnull(te.numcontractid,0) = 0                    
--then                    
-- case when numCategory =1  then                    
--   isnull(convert(decimal(18,2),datediff(minute,dtfromdate,dttodate))*monAmount/60,0)                    
--   when numCategory =2  then                    
--   isnull(monamount,0)                    
--                     
--   end                    
--else                                  
--  0                    
--end as amount,                                      
--0.00 as Tax,'' as listPrice,0 as ItemCode,0 as numoppitemtCode,'' as vcItemClassification,                                      
--'No' as Taxable,0 as itemIncomeAccount,'TE' as ItemType                                      
--,0 as numJournalId,0 as numTransactionId ,'' vcModelID,''as Attributes,'' as Part,0 as DiscAmt ,'' as vcNotes,'' as vcTrackingNo                                      
--from timeandexpense TE                            
--                                    
--where                                       
--(numtype= 1)   and   numDomainID=@numDomainID and                                    
--(numOppBizDocsId = @numOppBizDocsId  or numoppid = @numOppId)
            ) X


    IF @DecimalPoint = 1 
        BEGIN
            UPDATE  #Temp1
            SET     monPrice = CONVERT(DECIMAL(18, 1), monPrice),
                    Amount = CONVERT(DECIMAL(18, 1), Amount)
        END
    IF @DecimalPoint = 2 
        BEGIN
            UPDATE  #Temp1
            SET     monPrice = CONVERT(DECIMAL(18, 2), monPrice),
                    Amount = CONVERT(DECIMAL(18, 2), Amount)
        END
    IF @DecimalPoint = 3 
        BEGIN
            UPDATE  #Temp1
            SET     monPrice = CONVERT(DECIMAL(18, 3), monPrice),
                    Amount = CONVERT(DECIMAL(18, 3), Amount)
        END
    IF @DecimalPoint = 4 
        BEGIN
            UPDATE  #Temp1
            SET     monPrice = CONVERT(DECIMAL(18, 4), monPrice),
                    Amount = CONVERT(DECIMAL(18, 4), Amount)
        END

    DECLARE @strSQLUpdate AS VARCHAR(2000);SET @strSQLUpdate=''
   
    EXEC ( 'alter table #Temp1 add [TotalTax] money'
        )

    IF @tintOppType = 1 AND @tintTaxOperator = 1 --add Sales tax
	--set @strSQLUpdate=@strSQLUpdate +',[TotalTax]= dbo.fn_CalSalesTaxAmt('+ convert(varchar(20),@numOppBizDocsId)+','+ convert(varchar(20),@numDomainID)+')*Amount/100'
        SET @strSQLUpdate = @strSQLUpdate
            + ',[TotalTax]= dbo.fn_CalBizDocTaxAmt('
            + CONVERT(VARCHAR(20), @numDomainID) + ',0,'
            + CONVERT(VARCHAR(20), @numOppId) +','+
            + 'numoppitemtCode,1)*Amount/100'
    ELSE IF @tintOppType = 1 AND @tintTaxOperator = 2 -- remove sales tax
            SET @strSQLUpdate = @strSQLUpdate + ',[TotalTax]= 0'
    ELSE IF @tintOppType = 1 
                SET @strSQLUpdate = @strSQLUpdate
                    + ',[TotalTax]= dbo.fn_CalBizDocTaxAmt('
                    + CONVERT(VARCHAR(20), @numDomainID) + ',0,'
                    + CONVERT(VARCHAR(20), @numOppId) +','+
                    + 'numoppitemtCode,1)*Amount/100'
    ELSE 
                SET @strSQLUpdate = @strSQLUpdate + ',[TotalTax]= 0'


 DECLARE @vcTaxName AS VARCHAR(100)
    DECLARE @numTaxItemID AS NUMERIC(9)
    SET @numTaxItemID = 0
    SELECT TOP 1
            @vcTaxName = vcTaxName,
            @numTaxItemID = numTaxItemID
    FROM    TaxItems
    WHERE   numDomainID = @numDomainID


    WHILE @numTaxItemID > 0
        BEGIN

            EXEC ( 'alter table #Temp1 add [' + @vcTaxName + '] money'
                )

            IF @tintOppType = 1 
                SET @strSQLUpdate = @strSQLUpdate + ',[' + @vcTaxName
                    + ']= dbo.fn_CalBizDocTaxAmt('
                    + CONVERT(VARCHAR(20), @numDomainID) + ','
                    + CONVERT(VARCHAR(20), @numTaxItemID) + ','
                    + CONVERT(VARCHAR(20), @numOppId) +','+
                    + 'numoppitemtCode,1)*Amount/100'
            ELSE 
                SET @strSQLUpdate = @strSQLUpdate + ',[' + @vcTaxName + ']= 0'

            SELECT TOP 1
                    @vcTaxName = vcTaxName,
                    @numTaxItemID = numTaxItemID
            FROM    TaxItems
            WHERE   numDomainID = @numDomainID
                    AND numTaxItemID > @numTaxItemID

            IF @@rowcount = 0 
                SET @numTaxItemID = 0


        END

    IF @strSQLUpdate <> '' 
        BEGIN
            SET @strSQLUpdate = 'update #Temp1 set ItemCode=ItemCode'
                + @strSQLUpdate + ' where ItemCode>0'
            PRINT @strSQLUpdate
            EXEC ( @strSQLUpdate
                )
        END

    DECLARE @vcDbColumnName NVARCHAR(50)
    SELECT TOP 1
            @numFldID = numfieldID,
            @vcFldname = vcFieldName,
            @vcDbColumnName = vcDbColumnName,
            @intRowNum = ( tintRow + 1 )
    FROM    View_DynamicCustomColumns
    WHERE   numFormID = @tintType
            AND Grp_id = 5
            AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId
            AND bitCustom = 1
            AND ISNULL(numRelCntType, 0) = @numBizDocTempID
    ORDER BY tintRow                                                                                          
    WHILE @intRowNum > 0                                                                                  
        BEGIN    
            EXEC
                ( 'alter table #Temp1 add [' + @vcDbColumnName
                  + '] varchar(100)'
                )
	
            SET @strSQLUpdate = 'update #Temp1 set ItemCode=ItemCode,['
                + @vcDbColumnName + ']=dbo.GetCustFldValueItem(' + @numFldID
                + ',ItemCode) where ItemCode>0'
            EXEC ( @strSQLUpdate
                )
		                   
            SELECT TOP 1
                    @numFldID = numfieldID,
                    @vcFldname = vcFieldName,
                    @vcDbColumnName = vcDbColumnName,
                    @intRowNum = ( tintRow + 1 )
            FROM    View_DynamicCustomColumns
            WHERE   numFormID = @tintType
                    AND Grp_id = 5
                    AND numDomainID = @numDomainID
                    AND numAuthGroupID = @numBizDocId
                    AND bitCustom = 1
                    AND tintRow >= @intRowNum
                    AND ISNULL(numRelCntType, 0) = @numBizDocTempID
            ORDER BY tintRow                                                                  
		           
            IF @@rowcount = 0 
                SET @intRowNum = 0                                                                                          
        END


    SELECT  Row_NUmber() OVER ( ORDER BY numoppitemtCode ) RowNumber,
            *
    FROM    #Temp1
    ORDER BY numoppitemtCode ASC 

    DROP TABLE #Temp1

--    SELECT  ISNULL(tintOppStatus, 0)
--    FROM    OpportunityMaster
--    WHERE   numOppID = @numOppId                                                                                                 

IF (SELECT COUNT(*) FROM View_DynamicColumns WHERE   numFormID = @tintType AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId AND vcFieldType = 'R'AND ISNULL(numRelCntType, 0) = @numBizDocTempID)=0
BEGIN            
	INSERT INTO DycFormConfigurationDetails (numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numAuthGroupID,numRelCntType)
	select numFormId,numFieldId,1,tintRow,@numDomainID,@numBizDocId,@numBizDocTempID
	 FROM View_DynamicDefaultColumns
	 where numFormId=@tintType AND bitDefault=1 AND numDomainID=@numDomainID AND vcLookBackTableName = (CASE @numBizDocId WHEN 40911 THEN 'CSGrid' ELSE REPLACE(vcLookBackTableName,'CSGrid','') END) order by tintRow ASC	
	 
	 
	  IF(SELECT COUNT(*) FROM DycFrmConfigBizDocsSumm Ds where numBizDocID=@numBizDocId and Ds.numFormID=@tintType 
	and isnull(Ds.numBizDocTempID,0)=@numBizDocTempID and Ds.numDomainID=@numDomainID)=0
	BEGIN
		INSERT INTO DycFrmConfigBizDocsSumm (numFormID,numBizDocID,numFieldID,numDomainID,numBizDocTempID)
		 SELECT @tintType,@numBizDocId,DFM.numFieldID,@numDomainID,@numBizDocTempID FROM
		 DycFormField_Mapping DFFM 
			JOIN DycFieldMaster DFM on DFFM.numModuleID=DFFM.numModuleID and DFFM.numFieldID=DFM.numFieldID
			where numFormID=16 and DFFM.numDomainID is null
	END
	
END      

    SELECT  vcFieldName AS vcFormFieldName,
            vcDbColumnName,
            vcFieldDataType,
            tintRow AS intRowNum
    FROM    View_DynamicColumns
    WHERE   numFormID = @tintType
            AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId
            AND vcFieldType = 'R'
            AND ISNULL(numRelCntType, 0) = @numBizDocTempID
    UNION
    SELECT  vcFieldName AS vcFormFieldName,
            vcDbColumnName,
            'C' vcFieldDataType,
            tintRow AS intRowNum
    FROM    View_DynamicCustomColumns
    WHERE   numFormID = @tintType
            AND Grp_id = 5
            AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId
            AND bitCustom = 1
            AND ISNULL(numRelCntType, 0) = @numBizDocTempID
    ORDER BY tintRow
GO
/****** Object:  StoredProcedure [dbo].[USP_OppBizDocs]    Script Date: 03/25/2009 15:23:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_oppbizdocs')
DROP PROCEDURE usp_oppbizdocs
GO
CREATE PROCEDURE [dbo].[USP_OppBizDocs]
(                        
 @byteMode as tinyint=null,                        
 @numOppId as numeric(9)=null,                        
 @numOppBizDocsId as numeric(9)=null,
 @ClientTimeZoneOffset AS INT=0
)                        
as                        
                        
DECLARE @numDomainID NUMERIC
                                          
if @byteMode= 1                        
begin 

BEGIN TRANSACTION
DELETE FROM [ProjectsOpportunities] WHERE numOppBizDocID=@numOppBizDocsId
--delete from OpportunityBizDocTaxItems where numOppBizDocID=@numOppBizDocsId

DELETE FROM [ShippingReportItems] WHERE [numShippingReportId] IN  (SELECT numShippingReportId from [ShippingReport] WHERE [numOppBizDocId] = @numOppBizDocsId)
DELETE FROM [ShippingBox] WHERE [numShippingReportId] IN  (SELECT numShippingReportId from [ShippingReport] WHERE [numOppBizDocId] = @numOppBizDocsId)
DELETE FROM [ShippingReport] WHERE [numOppBizDocId] = @numOppBizDocsId
SELECT @numDomainID = OM.numDOmainID FROM [OpportunityBizDocs] INNER JOIN [OpportunityMaster] OM ON [OpportunityBizDocs].[numOppId] = OM.[numOppId] WHERE [numOppBizDocsId]=@numOppBizDocsId
 --Delete All entries for current bizdoc
EXEC [USP_DeleteEmbeddedCost] @numOppBizDocsId, 0, @numDomainID 
--DELETE FROM [OpportunityBizDocsPaymentDetails] WHERE [numBizDocsPaymentDetId] IN (SELECT  [numBizDocsPaymentDetId] FROM [OpportunityBizDocsDetails] WHERE [numBizDocsPaymentDetId] IN (SELECT [numBizDocsPaymentDetId] FROM [EmbeddedCost] WHERE [numOppBizDocID]=@numOppBizDocsId))
--DELETE FROM [EmbeddedCostItems] WHERE [numEmbeddedCostID] IN (SELECT [numEmbeddedCostID] FROM [EmbeddedCost] WHERE [numOppBizDocID] = @numOppBizDocsId)
--DELETE FROM [EmbeddedCost] WHERE [numOppBizDocID] = @numOppBizDocsId
--DELETE FROM [OpportunityBizDocsDetails] WHERE [numBizDocsPaymentDetId] IN (SELECT [numBizDocsPaymentDetId] FROM [EmbeddedCost] WHERE [numOppBizDocID]=@numOppBizDocsId)


delete  from OpportunityBizDocItems where   numOppBizDocID=  @numOppBizDocsId              

--Delete Deferred BizDoc Recurring Entries
delete from OpportunityRecurring where numOppBizDocID=@numOppBizDocsId and tintRecurringType=4

UPDATE dbo.OpportunityRecurring SET numOppBizDocID=NULL WHERE numOppBizDocID=@numOppBizDocsId

DELETE FROM [RecurringTransactionReport] WHERE [numRecTranBizDocID] = @numOppBizDocsId

--Credit Balance
Declare @monCreditAmount as money;Set @monCreditAmount=0
Declare @numDivisionID as numeric(9);Set @numDivisionID=0

Select @monCreditAmount=isnull(oppBD.monCreditAmount,0),@numDivisionID=Om.numDivisionID,
@numOppId=OM.numOppId 
from OpportunityBizDocs oppBD join OpportunityMaster Om on OM.numOppId=oppBD.numOppId WHERE  numOppBizDocsId = @numOppBizDocsId

delete from [CreditBalanceHistory] where numOppBizDocsId=@numOppBizDocsId

Declare @tintOppType as tinyint
Select @tintOppType=tintOppType from OpportunityMaster WHERE  numOppId=@numOppId  and numDomainID= @numDomainID 
 
IF @tintOppType=1 --Sales
	update DivisionMaster set monSCreditBalance=isnull(monSCreditBalance,0) + isnull(@monCreditAmount,0) where numDivisionID=@numDivisionID and numDomainID=@numDomainID
else IF @tintOppType=2 --Purchase
	update DivisionMaster set monPCreditBalance=isnull(monPCreditBalance,0) + isnull(@monCreditAmount,0) where numDivisionID=@numDivisionID and numDomainID=@numDomainID


delete from DocumentWorkflow where numDocID=@numOppBizDocsId and cDocType='B'      

delete from BizDocAction where numBizActionId in(select numBizActionId from BizActionDetails where numOppBizDocsId =@numOppBizDocsId and btDocType=1) 
delete from BizActionDetails where numOppBizDocsId =@numOppBizDocsId and btDocType=1

delete from OpportunityBizDocs where numOppBizDocsId=@numOppBizDocsId                        


-- Rollback the transaction if there were any errors
IF @@ERROR <> 0
 BEGIN
    -- Rollback the transaction
    ROLLBACK
    RETURN
 END
 
 
COMMIT

                    
end                        
                        
if @byteMode= 2                        
begin  
SELECT @numDomainID = numDOmainID FROM [dbo].[OpportunityMaster] 
WHERE [numOppId] = @numOppId
--PRINT @numDomainID

DECLARE @BaseCurrencySymbol nVARCHAR(3);SET @BaseCurrencySymbol='$'
SELECT @BaseCurrencySymbol = ISNULL(varCurrSymbol,'$') FROM dbo.Currency 
														   WHERE numCurrencyID IN ( SELECT numCurrencyID FROM dbo.Domain 
																					WHERE numDomainId=@numDomainId)
--PRINT @BaseCurrencySymbol

																					                      
select *, isnull(X.monPAmount,0)-X.monAmountPaid as BalDue from (select  opp.numOppBizDocsId,                        
 opp.numOppId,                        
 opp.numBizDocId,                        
 ISNULL((SELECT TOP 1 numOrientation FROM BizDocTemplate WHERE numBizDocTempID = Opp.numBizDocTempID),0) AS numOrientation, 
 ISNULL((SELECT TOP 1 bitKeepFooterBottom FROM BizDocTemplate WHERE numBizDocTempID = Opp.numBizDocTempID),0) AS bitKeepFooterBottom,                  
 oppmst.vcPOppName,                        
 mst.vcData as BizDoc,                        
 opp.numCreatedBy,                        
DATEADD(minute,-@ClientTimeZoneOffset,opp.dtCreatedDate) CreatedDate,
 opp.vcRefOrderNo,                       
 vcBizDocID,
 CASE WHEN LEN(ISNULL(vcBizDocName,''))=0 THEN vcBizDocID ELSE vcBizDocName END vcBizDocName,      
  [dbo].[GetDealAmount](@numOppId ,getutcdate(),opp.numOppBizDocsId ) as monPAmount,
  case  when tintOppType=1  then (case when isnull(numAuthoritativeSales,0)=numBizDocId then 'Authoritative' + Case when isnull(Opp.tintDeferred,0)=1 then ' (Deferred)' else '' end else 'Non-Authoritative' end ) 
  when tintOppType=2  then (case when isnull(numAuthoritativePurchase,0)=numBizDocId then 'Authoritative' + Case when isnull(Opp.tintDeferred,0)=1 then ' (Deferred)' else '' end else 'Non-Authoritative' end )  end as BizDocType,
  isnull(monAmountPaid,0) monAmountPaid, 
  ISNULL(C.varCurrSymbol,'') varCurrSymbol ,
  ISNULL(Opp.[bitAuthoritativeBizDocs],0) bitAuthoritativeBizDocs,
  ISNULL(oppmst.[tintshipped] ,0) tintshipped,isnull(opp.numShipVia,0) as numShipVia,oppmst.numDivisionId,isnull(Opp.tintDeferred,0) as tintDeferred,
  ISNULL(Opp.numBizDocTempID,0) AS numBizDocTempID,con.numContactid,isnull(con.vcEmail,'') AS vcEmail,ISNULL(Opp.numBizDocStatus,0) AS numBizDocStatus,
--  case when isnumeric(ISNULL(dbo.fn_GetListItemName(isnull(oppmst.intBillingDays,0)),0))=1 
--	   then ISNULL(dbo.fn_GetListItemName(isnull(oppmst.intBillingDays,0)),0) 
--	   else 0 
--  end AS numBillingDaysName,
  CASE WHEN ISNUMERIC((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(oppmst.intBillingDays,0))) = 1
 	   THEN (SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(oppmst.intBillingDays,0))
	   ELSE 0
  END AS numBillingDaysName,
  Opp.dtFromDate,
  opp.dtFromDate AS BillingDate,
  ISNULL(C.fltExchangeRate,1) fltExchangeRateCurrent,
  ISNULL(oppmst.fltExchangeRate,1) fltExchangeRateOfOrder,
  @BaseCurrencySymbol BaseCurrencySymbol,0 AS numReturnHeaderID,0 AS tintReturnType,ISNULL(Opp.fltExchangeRateBizDoc,ISNULL(oppmst.fltExchangeRate,1)) AS fltExchangeRateBizDoc,
  CASE WHEN (SELECT COUNT(*) FROM dbo.ShippingReport WHERE numOppBizDocId = opp.numOppBizDocsId AND numDomainID = @numDomainID) > 0 THEN 1 ELSE 0 END AS [IsShippingReportGenerated],
  --CASE WHEN (SELECT ISNULL(vcTrackingNo,'') FROM dbo.OpportunityBizDocs WHERE numOppBizDocsId = opp.numOppBizDocsId) <> '' THEN 1 ELSE 0 END AS [ISTrackingNumGenerated]
  CASE WHEN (SELECT COUNT(*) FROM dbo.ShippingBox 
			 WHERE numShippingReportId IN (SELECT numShippingReportId FROM dbo.ShippingReport WHERE numOppBizDocId = opp.numOppBizDocsId AND numDomainID = @numDomainID)
			 AND LEN(ISNULL(vcTrackingNumber,'')) > 0) > 0 THEN 1 ELSE 0 END AS [ISTrackingNumGenerated],
  ISNULL((SELECT MAX(numShippingReportId) from ShippingReport SR 
		  WHERE SR.numOppBizDocId = opp.numoppbizdocsid 
		  AND SR.numDomainID = oppmst.numDomainID),0) AS numShippingReportId,
  ISNULL((SELECT MAX(SB.numShippingReportId) FROM [ShippingBox] SB 
		  JOIN dbo.ShippingReport SR ON SB.numShippingReportId = SR.numShippingReportId 
		  where SR.numOppBizDocId = opp.numoppbizdocsid AND SR.numDomainID = oppmst.numDomainID 
		  AND LEN(ISNULL(SB.vcTrackingNumber,''))>0),0) AS numShippingLabelReportId			 
 ,ISNULL(STUFF(( SELECT  ',' +  CASE WHEN LEN(ISNULL(W.[vcWareHouse],'')  + ':' + ISNULL([WL].[vcLocation],'')) > 0 THEN ISNULL(W.[vcWareHouse],'')  + ':' + ISNULL([WL].[vcLocation],'') ELSE '' END
                FROM    [dbo].[WarehouseLocation] AS WL
				JOIN [dbo].[Warehouses] W ON [WL].[numWarehouseID] = [W].[numWareHouseID]
                WHERE  [WL].[numWLocationID] IN (
                        SELECT  WHI.[numWLocationID]
                        FROM    [dbo].[WareHouseItems] AS WHI
                                JOIN [dbo].[OpportunityItems] AS OI ON [WHI].[numWareHouseItemID] = [OI].[numWarehouseItmsID]
                        WHERE   [OI].[numOppId] = oppmst.[numOppId])
              FOR
                XML PATH('')
              ), 1, 1, ''),'') AS [vcLocation],
		(CASE WHEN ISNULL(RecurrenceConfiguration.numRecConfigID,0) = 0 THEN 0 ELSE 1 END) AS bitRecur,
		ISNULL(Opp.bitRecurred,0) AS bitRecurred,
		ISNULL(CMP.numCompanyType,0) AS numCompanyType,
		ISNULL(CMP.vcProfile,0) AS vcProfile
 FROM                         
 OpportunityBizDocs  opp                        
 left join ListDetails mst                        
 on mst.numListItemID=opp.numBizDocId                        
 join OpportunityMaster oppmst                        
 on oppmst.numOppId= opp.numOppId                                
 left join  AuthoritativeBizDocs  AB
 on AB.numDomainId=  oppmst.numDomainID             
 LEFT OUTER JOIN [Currency] C
  ON C.numCurrencyID = oppmst.numCurrencyID
  JOIN divisionmaster div ON div.numDivisionId=oppmst.numDivisionId
  JOIN CompanyInfo CMP ON CMP.numCompanyId = div.numCompanyID
  JOIN AdditionalContactsInformation con ON con.numdivisionId = div.numdivisionId AND con.numContactid = oppmst.numContactId
  LEFT JOIN
	RecurrenceConfiguration
  ON
	RecurrenceConfiguration.numOppBizDocID = opp.numOppBizDocsId
where opp.numOppId=@numOppId

UNION ALL

select  0 AS numOppBizDocsId,                        
 RH.numOppId,                        
 0 AS numBizDocId,                        
 1 AS numOrientation,  
 0 AS bitKeepFooterBottom,                   
 '' AS vcPOppName,                        
 'RMA' as BizDoc,                        
 RH.numCreatedBy,                        
DATEADD(minute,-@ClientTimeZoneOffset,RH.dtCreatedDate) CreatedDate,
 '' vcRefOrderNo,                       
 RH.vcRMA AS vcBizDocID,
 RH.vcRMA AS vcBizDocName,      
  ISNULL(monAmount,0) as monPAmount,
  'RMA' as BizDocType,
  0 AS monAmountPaid, 
  '' varCurrSymbol ,
  0 bitAuthoritativeBizDocs,
  0 tintshipped,0 as numShipVia,RH.numDivisionId,0 as tintDeferred,
  0 AS numBizDocTempID,con.numContactid,isnull(con.vcEmail,'') AS vcEmail,0 AS numBizDocStatus,
  0 AS numBillingDaysName,RH.dtCreatedDate AS dtFromDate,
  RH.dtCreatedDate AS BillingDate,
  1 fltExchangeRateCurrent,
  1 fltExchangeRateOfOrder,
  @BaseCurrencySymbol BaseCurrencySymbol,RH.numReturnHeaderID,RH.tintReturnType,1 AS fltExchangeRateBizDoc
  ,0 [IsShippingReportGenerated],0 [ISTrackingNumGenerated],
  0 AS numShippingReportId,
  0 AS numShippingLabelReportId			 
  ,ISNULL(STUFF(( SELECT  ',' +  CASE WHEN LEN(ISNULL(W.[vcWareHouse],'')  + ':' + ISNULL([WL].[vcLocation],'')) > 0 THEN ISNULL(W.[vcWareHouse],'')  + ':' + ISNULL([WL].[vcLocation],'') ELSE '' END
                FROM    [dbo].[WarehouseLocation] AS WL
				JOIN [dbo].[Warehouses] W ON [WL].[numWarehouseID] = [W].[numWareHouseID]
                WHERE  [WL].[numWLocationID] IN (
                        SELECT  WHI.[numWLocationID]
                        FROM    [dbo].[WareHouseItems] AS WHI
                                JOIN [dbo].[OpportunityItems] AS OI ON [WHI].[numWareHouseItemID] = [OI].[numWarehouseItmsID]
                        WHERE   [OI].[numOppId] = RH.[numOppId])
              FOR
                XML PATH('')
              ), 1, 1, ''),'') AS [vcLocation],
			  0 AS bitRecur,
			  0 AS bitRecurred,
			  ISNULL(CMP.numCompanyType,0) AS numCompanyType,
		ISNULL(CMP.vcProfile,0) AS vcProfile		 
from                         
 ReturnHeader RH                        
  JOIN divisionmaster div ON div.numDivisionId=RH.numDivisionId
  JOIN CompanyInfo CMP ON CMP.numCompanyId = div.numCompanyID
  LEFT JOIN AdditionalContactsInformation con ON con.numdivisionId = div.numdivisionId AND con.numContactid = RH.numContactId
where RH.numOppId=@numOppId
)X         
                        
END




GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OPPGetINItems')
DROP PROCEDURE USP_OPPGetINItems
GO
-- =============================================  
-- Modified by: <Author,,Sachin Sadhu>  
-- Create date: <Create Date,,24thDec2013>  
-- Description: <Description,,add 3 fields:vcBizDocImagePath,vcBizDocFooter,vcPurBizDocFooter>  
-- =============================================  
CREATE PROCEDURE [dbo].[USP_OPPGetINItems]
(
          @byteMode        AS TINYINT  = NULL,
          @numOppId        AS NUMERIC(9)  = NULL,
          @numOppBizDocsId AS NUMERIC(9)  = NULL,
          @numDomainID     AS NUMERIC(9)  = 0,
          @numUserCntID    AS NUMERIC(9)  = 0,
          @ClientTimeZoneOffset INT=0)
AS
  IF @byteMode = 1
    BEGIN
      DECLARE  @BizDcocName  AS VARCHAR(100)
      DECLARE  @OppName  AS VARCHAR(100)
      DECLARE  @Contactid  AS VARCHAR(100)
      DECLARE  @shipAmount  AS MONEY
      DECLARE  @tintOppType  AS TINYINT
      DECLARE  @numlistitemid  AS VARCHAR(15)
      DECLARE  @RecOwner  AS VARCHAR(15)
      DECLARE  @MonAmount  AS VARCHAR(15)
      DECLARE  @OppBizDocID  AS VARCHAR(100)
      DECLARE  @bizdocOwner  AS VARCHAR(15)
      DECLARE  @tintBillType  AS TINYINT
      DECLARE  @tintShipType  AS TINYINT
      DECLARE  @numCustomerDivID AS NUMERIC 
      
      SELECT @RecOwner = numRecOwner,@MonAmount = CONVERT(VARCHAR(20),monPAmount),
             @Contactid = numContactId,@OppName = vcPOppName,
             @tintOppType = tintOppType,@tintBillType = tintBillToType,@tintShipType = tintShipToType
      FROM   OpportunityMaster WHERE  numOppId = @numOppId
      
      -- When Creating PO from SO and Bill type is Customer selected 
      SELECT @numCustomerDivID = ISNULL(numDivisionID,0) FROM dbo.OpportunityMaster WHERE 
			numOppID IN (SELECT  ISNULL(numParentOppID,0) FROM dbo.OpportunityLinking WHERE numChildOppID = @numOppId)
			
      --select @BizDcocName=vcBizDocID from OpportunityBizDocs where numOppBizDocsId=@numOppBizDocsId
      SELECT @BizDcocName = vcdata,
             @shipAmount = monShipCost,
             @numlistitemid = numlistitemid,
             @OppBizDocID = vcBizDocID,
             @bizdocOwner = OpportunityBizDocs.numCreatedBy
      FROM   listdetails JOIN OpportunityBizDocs ON numBizDocId = numlistitemid
      WHERE  numOppBizDocsId = @numOppBizDocsId
      
      SELECT @bizdocOwner AS BizDocOwner,
             @OppBizDocID AS BizDocID,
             @MonAmount AS Amount,
             isnull(@RecOwner,1) AS Owner,
             @BizDcocName AS BizDcocName,
             @numlistitemid AS BizDoc,
             @OppName AS OppName,
             VcCompanyName AS CompName,
             @shipAmount AS ShipAmount,
             dbo.fn_getOPPAddress(@numOppId,@numDomainID,1) AS BillAdd,
             dbo.fn_getOPPAddress(@numOppId,@numDomainID,2) AS ShipAdd,
             AD.VcStreet,
             AD.VcCity,
             isnull(dbo.fn_GetState(AD.numState),'') AS vcBilState,
             AD.vcPostalCode,
             isnull(dbo.fn_GetListItemName(AD.numCountry),
                    '') AS vcBillCountry,
             con.vcFirstName AS ConName,
             ISNULL(con.numPhone,'') numPhone,
             con.vcFax,
             isnull(con.vcEmail,'') AS vcEmail,
             div.numDivisionID,
             numContactid,
             isnull(vcFirstFldValue,'') vcFirstFldValue,
             isnull(vcSecndFldValue,'') vcSecndFldValue,
             isnull(bitTest,0) AS bitTest,
			dbo.fn_getOPPAddress(@numOppId,@numDomainID,1) AS BillToAddressName,
			dbo.fn_getOPPAddress(@numOppId,@numDomainID,1) AS ShipToAddressName,
             CASE 
               WHEN @tintBillType IS NULL THEN Com.vcCompanyName
             	-- When Sales order and bill to is set to customer 
               WHEN @tintBillType = 1 AND @tintOppType = 1 THEN Com.vcCompanyName
             -- When Create PO from SO and Bill to is set to Customer 
			   WHEN @tintBillType = 1 AND @tintOppType = 2 THEN dbo.fn_GetComapnyName(@numCustomerDivID)
               WHEN @tintBillType = 0 THEN (SELECT TOP 1 Com1.vcCompanyName
                                            FROM   companyinfo [Com1]
                                                   JOIN divisionmaster div1
                                                     ON com1.numCompanyID = div1.numCompanyID
                                                   JOIN Domain D1
                                                     ON D1.numDivisionID = div1.numDivisionID
                                                    JOIN dbo.AddressDetails AD1
														ON AD1.numDomainID = div1.numDomainID AND AD1.numRecordID = div1.numDivisionID AND tintAddressOf = 2 AND tintAddressType = 1 AND bitIsPrimary=1
                                            WHERE  D1.numDomainID = @numDomainID)
               WHEN @tintBillType = 2 THEN (SELECT vcBillCompanyName
                                            FROM   OpportunityAddress
                                            WHERE  numOppID = @numOppId)
				WHEN @tintBillType = 3 THEN  Com.vcCompanyName
              END AS BillToCompanyName,
             CASE 
               WHEN @tintShipType IS NULL THEN Com.vcCompanyName
               WHEN @tintShipType = 1 AND @tintOppType = 1 THEN Com.vcCompanyName
				-- When Create PO from SO and Ship to is set to Customer 
   			   WHEN @tintShipType = 1 AND @tintOppType = 2 THEN dbo.fn_GetComapnyName(@numCustomerDivID)
               WHEN @tintShipType = 0 THEN (SELECT TOP 1 Com1.vcCompanyName
                                            FROM   companyinfo [Com1]
                                                   JOIN divisionmaster div1
                                                     ON com1.numCompanyID = div1.numCompanyID
                                                   JOIN Domain D1
                                                     ON D1.numDivisionID = div1.numDivisionID
                                                    JOIN dbo.AddressDetails AD1
													 ON AD1.numDomainID = div1.numDomainID AND AD1.numRecordID = div1.numDivisionID AND tintAddressOf = 2 AND tintAddressType = 2 AND bitIsPrimary=1
                                            WHERE  D1.numDomainID = @numDomainID)
               WHEN @tintShipType = 2 THEN (SELECT vcShipCompanyName
                                            FROM   OpportunityAddress
                                            WHERE  numOppID = @numOppId)
				WHEN @tintShipType = 3 THEN (SELECT vcShipCompanyName
														FROM   OpportunityAddress
														WHERE  numOppID = @numOppId)
             END AS ShipToCompanyName
      FROM   companyinfo [Com] JOIN divisionmaster div ON com.numCompanyID = div.numCompanyID
             JOIN AdditionalContactsInformation con ON con.numdivisionId = div.numdivisionId
             JOIN Domain D ON D.numDomainID = div.numDomainID
             LEFT JOIN PaymentGatewayDTLID PGDTL ON PGDTL.intPaymentGateWay = D.intPaymentGateWay AND D.numDomainID = PGDTL.numDomainID
             LEFT JOIN AddressDetails AD --Billing add
				ON AD.numDomainID=DIV.numDomainID AND AD.numRecordID=div.numDivisionID AND AD.tintAddressOf = 2 AND AD.tintAddressType = 1 AND AD.bitIsPrimary=1
			 LEFT JOIN AddressDetails AD2--Shipping address
				ON AD2.numDomainID=DIV.numDomainID AND AD2.numRecordID=div.numDivisionID AND AD2.tintAddressOf = 2 AND AD2.tintAddressType = 2 AND AD2.bitIsPrimary=1	
      WHERE  numContactid = @Contactid AND Div.numDomainID = @numDomainID
    END
  IF @byteMode = 2
    BEGIN
      SELECT opp.vcBizDocID,
             Mst.fltDiscount AS decDiscount,
             isnull(opp.monAmountPaid,0) AS monAmountPaid,
             isnull(opp.vcComments,'') AS vcComments,
             CONVERT(VARCHAR(20),DATEADD(minute,-@ClientTimeZoneOffset,opp.dtCreatedDate)) dtCreatedDate,
             dbo.fn_GetContactName(opp.numCreatedby) AS numCreatedby,
             dbo.fn_GetContactName(opp.numModifiedBy) AS numModifiedBy,
             CONVERT(VARCHAR(20),DATEADD(minute,-@ClientTimeZoneOffset,opp.dtModifiedDate)) dtModifiedDate,
             CASE 
               WHEN isnull(opp.numViewedBy,0) = 0 THEN ''
               ELSE dbo.fn_GetContactName(opp.numViewedBy)
             END AS numViewedBy,
             CASE 
               WHEN opp.numViewedBy = 0 THEN NULL
               ELSE opp.dtViewedDate
             END AS dtViewedDate,
             isnull(Mst.bitBillingTerms,0) AS tintBillingTerms,
             isnull(Mst.intBillingDays,0) AS numBillingDays,
--			 case when isnumeric(ISNULL(dbo.fn_GetListItemName(isnull(Mst.intBillingDays,0)),0))=1 
--				  then ISNULL(dbo.fn_GetListItemName(isnull(Mst.intBillingDays,0)),0) 
--				  else 0 
--			 end AS numBillingDaysName,
--			 CASE WHEN ISNUMERIC((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(Mst.intBillingDays,0))) = 1
-- 				   THEN (SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(Mst.intBillingDays,0))
--				   ELSE 0
--			  END AS numBillingDaysName,
			 ISNULL(BTR.numNetDueInDays,0) AS numBillingDaysName,
			 BTR.vcTerms AS vcBillingTermsName,
             isnull(Mst.bitInterestType,0) AS tintInterestType,
             isnull(Mst.fltInterest,0) AS fltInterest,
             tintOPPType,
             Opp.numBizDocId,
             dbo.fn_GetContactName(numApprovedBy) AS ApprovedBy,
             dtApprovedDate,
             Mst.bintAccountClosingDate,
             tintShipToType,
             tintBillToType,
             tintshipped,
             dtShippedDate bintShippedDate,
             isnull(opp.monShipCost,0) AS monShipCost,
             (SELECT COUNT(* )
              FROM   DocumentWorkflow
              WHERE  numDocID = @numOppBizDocsId
                     AND numContactID = @numUserCntID
                     AND cDocType = 'B'
                     AND tintApprove = 0) AS AppReq,
             (SELECT COUNT(* )
              FROM   DocumentWorkflow
              WHERE  numDocID = @numOppBizDocsId
                     AND cDocType = 'B'
                     AND tintApprove = 1) AS Approved,
             (SELECT COUNT(* )
              FROM   DocumentWorkflow
              WHERE  numDocID = @numOppBizDocsId
                     AND cDocType = 'B'
                     AND tintApprove = 2) AS Declined,
             (SELECT COUNT(* )
              FROM   DocumentWorkflow
              WHERE  numDocID = @numOppBizDocsId
                     AND cDocType = 'B'
                     AND tintApprove = 0) AS Pending,
             ISNULL(BT.vcBizDocFooter,D.vcBizDocFooter) AS BizdocFooter,
             ISNULL(BT.vcBizDocImagePath,D.vcBizDocImagePath) As vcBizDocImagePath, 
             Mst.numDivisionID,
             ISNULL(BT.vcPurBizDocFooter,D.vcPurBizDocFooter) AS PurBizdocFooter, 
             isnull(Mst.fltDiscount,0) AS fltDiscount,
             isnull(Mst.bitDiscountType,0) AS bitDiscountType,
             isnull(Opp.numShipVia,0) AS numShipVia,
--             isnull(vcTrackingURL,'') AS vcTrackingURL,
			ISNULL( (SELECT vcShipFieldValue FROM dbo.ShippingFieldValues WHERE numDomainID=MSt.numDomainID AND intShipFieldID IN ( SELECT intShipFieldID FROM dbo.ShippingFields WHERE numListItemID=Opp.numShipVia  AND vcFieldName='Tracking URL')),'')		AS vcTrackingURL,
             isnull(Opp.numBizDocStatus,0) AS numBizDocStatus,
             CASE 
               WHEN Opp.numBizDocStatus IS NULL THEN '-'
               ELSE dbo.fn_GetListItemName(Opp.numBizDocStatus)
             END AS BizDocStatus,
             CASE 
               WHEN Opp.numShipVia IS NULL THEN '-'
               ELSE dbo.fn_GetListItemName(Opp.numShipVia)
             END AS ShipVia,
             ISNULL(C.varCurrSymbol,'') varCurrSymbol,
             (SELECT COUNT(*) FROM [ShippingReport] WHERE numOppBizDocId = opp.numOppBizDocsId )AS ShippingReportCount,
			 isnull(bitPartialFulfilment,0) bitPartialFulfilment,
			 ISNULL(Opp.vcBizDocName,'') vcBizDocName,
			 dbo.[fn_GetContactName](opp.[numModifiedBy])  + ', '
				+ CONVERT(VARCHAR(20),dateadd(MINUTE,-@ClientTimeZoneOffset,Opp.dtModifiedDate)) AS ModifiedBy,
			 dbo.[fn_GetContactName](Mst.[numRecOwner]) + ', ' +  dbo.[fn_GetContactName](Mst.[numAssignedTo]) AS OrderRecOwner,
			 dbo.[fn_GetContactName](DM.[numRecOwner]) + ', ' +  dbo.[fn_GetContactName](DM.[numAssignedTo]) AS AccountRecOwner,
			 Opp.dtFromDate,
			 Mst.tintTaxOperator,
			 dbo.GetAccountedEmbeddedCost(opp.numOppBizDocsID,1) AS monTotalEmbeddedCost,
			 dbo.GetAccountedEmbeddedCost(opp.numOppBizDocsID,2) AS monTotalAccountedCost,
			 ISNULL(BT.bitEnabled,0) bitEnabled,
			 ISNULL(BT.txtBizDocTemplate,'') txtBizDocTemplate, 
			 ISNULL(BT.txtCSS,'') txtCSS,
			 ISNULL(BT.numOrientation,0) numOrientation,
			 ISNULL(BT.bitKeepFooterBottom,0) bitKeepFooterBottom,
			 dbo.fn_GetContactName(Mst.numAssignedTo) AS AssigneeName,
			 ISNULL((SELECT vcEmail FROM dbo.AdditionalContactsInformation WHERE numContactID = Mst.numAssignedTo),'') AS AssigneeEmail,
			 ISNULL((SELECT numPhone FROM dbo.AdditionalContactsInformation WHERE numContactID = Mst.numAssignedTo),'') AS AssigneePhone,
			 dbo.fn_GetComapnyName(Mst.numDivisionId) OrganizationName,
			 dbo.fn_GetContactName(Mst.numContactID)  OrgContactName,

--CASE WHEN Mst.tintOppType = 1 THEN 
--dbo.getCompanyAddress((select ISNULL(DM.numDivisionID,0)  as numDivisionID from AdditionalContactsInformation AC INNER JOIN DivisionMaster DM 
--on AC.numDivisionID = DM.numDivisionID where numContactID = Mst.numCreatedBy),1,Mst.numDomainId)
--               ELSE dbo.getCompanyAddress(Mst.numDivisionId,1,Mst.numDomainId)
--             END AS CompanyBillingAddress1,

CASE WHEN Mst.tintOppType = 1 THEN 
dbo.getCompanyAddress((select ISNULL(AC.numDivisionID,0)  as numDivisionID from AdditionalContactsInformation AC
INNER JOIN Domain D ON D.numDomainID = AC.numDomainID where AC.numContactID = D.numAdminID AND D.numDomainID = Mst.numDomainId),1,Mst.numDomainId)
               ELSE dbo.getCompanyAddress(Mst.numDivisionId,1,Mst.numDomainId)
             END AS CompanyBillingAddress,

CASE WHEN Mst.tintOppType = 1 THEN 
dbo.getCompanyAddress((select ISNULL(AC.numDivisionID,0)  as numDivisionID from AdditionalContactsInformation AC
INNER JOIN Domain D ON D.numDomainID = AC.numDomainID where AC.numContactID = D.numAdminID AND D.numDomainID = Mst.numDomainId),1,Mst.numDomainId)
               ELSE dbo.getCompanyAddress(Mst.numDivisionId,2,Mst.numDomainId)
             END AS CompanyShippingAddress,
--
--			 dbo.getCompanyAddress(Mst.numDivisionId,1,Mst.numDomainId) CompanyBillingAddress,
--			 dbo.getCompanyAddress(Mst.numDivisionId,2,Mst.numDomainId) CompanyShippingAddress,
			 case when ACI.numPhone<>'' then ACI.numPhone +case when ACI.numPhoneExtension<>'' then ' - ' + ACI.numPhoneExtension else '' end  else '' END OrgContactPhone,
			 DM.vcComPhone as OrganizationPhone,
             ISNULL(ACI.vcEmail,'') AS OrgContactEmail,
 			 dbo.[fn_GetContactName](Mst.[numRecOwner]) AS OnlyOrderRecOwner,
 			 ISNULL(Opp.bitAuthoritativeBizDocs,0) bitAuthoritativeBizDocs,
			 ISNULL(Opp.tintDeferred,0) tintDeferred,isnull(Opp.monCreditAmount,0) AS monCreditAmount,
			 isnull(Opp.bitRentalBizDoc,0) as bitRentalBizDoc,isnull(BT.numBizDocTempID,0) as numBizDocTempID,isnull(BT.vcTemplateName,'') as vcTemplateName,
		     [dbo].[GetDealAmount](opp.numOppId ,getutcdate(),opp.numOppBizDocsId ) as monPAmount,
			 --(select top 1 SD.vcSignatureFile from SignatureDetail SD join OpportunityBizDocsDetails OBD on SD.numSignID=OBD.numSignID where SD.numDomainID = @numDomainID and OBD.numDomainID = @numDomainID and OBD.numSignID IS not null and OBD.numBizDocsId=opp.numOppBizDocsId order by OBD.numBizDocsPaymentDetId desc) as vcSignatureFile,
			 isnull(CMP.txtComments,'') as vcOrganizationComments,
			 isnull(Opp.vcTrackingNo,'') AS  vcTrackingNo,isnull(Opp.vcShippingMethod,'') AS  vcShippingMethod,
			 Opp.dtDeliveryDate,
			 CASE WHEN Opp.vcRefOrderNo IS NULL OR LEN(Opp.vcRefOrderNo)=0 THEN ISNULL(Mst.vcOppRefOrderNo,'') ELSE 
			  isnull(Opp.vcRefOrderNo,'') END AS vcRefOrderNo,ISNULL(Mst.numDiscountAcntType,0) AS numDiscountAcntType,
			 ISNULL(opp.numSequenceId,'') AS numSequenceId,
			 CASE WHEN ISNULL(Opp.numBizDocId,0)=296 THEN ISNULL((SELECT SUBSTRING((SELECT '$^$' + dbo.fn_GetListItemName(numBizDocStatus) +'#^#'+ dbo.fn_GetContactName(numUserCntID) +'#^#'+ dbo.FormatedDateTimeFromDate(DateAdd(minute, -@ClientTimeZoneOffset , dtCreatedDate),@numDomainId) + '#^#'+ isnull(DFCS.vcColorScheme,'NoForeColor')
					FROM OppFulfillmentBizDocsStatusHistory OFBS left join DycFieldColorScheme DFCS on OFBS.numBizDocStatus=DFCS.vcFieldValue AND DFCS.numDomainID=@numDomainID
					WHERE numOppId=opp.numOppId and numOppBizDocsId=opp.numOppBizDocsId FOR XML PATH('')),4,200000)),'') ELSE '' END AS vcBizDocsStatusList,ISNULL(Mst.numCurrencyID,0) AS numCurrencyID,
			ISNULL(Opp.fltExchangeRateBizDoc,Mst.fltExchangeRate) AS fltExchangeRateBizDoc,
			com1.vcCompanyName AS EmployerOrganizationName,div1.vcComPhone as EmployerOrganizationPhone,ISNULL(opp.bitAutoCreated,0) AS bitAutoCreated,
			ISNULL(RecurrenceConfiguration.numRecConfigID,0) AS numRecConfigID,
			(CASE WHEN RecurrenceConfiguration.numRecConfigID IS NULL THEN 0 ELSE 1 END) AS bitRecur,
			(CASE WHEN RecurrenceTransaction.numRecTranID IS NULL THEN 0 ELSE 1 END) AS bitRecurred,
			ISNULL(Mst.bitRecurred,0) AS bitOrderRecurred,
			dbo.FormatedDateFromDate(RecurrenceConfiguration.dtStartDate,@numDomainID) AS dtStartDate,
			RecurrenceConfiguration.vcFrequency AS vcFrequency,
			ISNULL(RecurrenceConfiguration.bitDisabled,0) AS bitDisabled
		FROM   OpportunityBizDocs opp JOIN OpportunityMaster Mst ON Mst.numOppId = opp.numOppId
             JOIN Domain D ON D.numDomainID = Mst.numDomainID
             LEFT OUTER JOIN [Currency] C ON C.numCurrencyID = Mst.numCurrencyID
             LEFT JOIN [DivisionMaster] DM ON DM.numDivisionID = Mst.numDivisionID   
			 LEFT JOIN CompanyInfo CMP ON DM.numCompanyId = CMP.numCompanyId and CMP.numDomainID = @numDomainID  
             LEFT JOIN dbo.AdditionalContactsInformation ACI ON ACI.numContactID = Mst.numContactID
             LEFT JOIN [BizDocTemplate] BT ON BT.numDomainID = @numDomainID AND BT.numBizDocID = Opp.numBizDocID AND BT.numOppType = Mst.tintOppType AND BT.tintTemplateType=0 and BT.numBizDocTempID=opp.numBizDocTempID
             LEFT JOIN BillingTerms BTR ON BTR.numTermsID = ISNULL(Mst.intBillingDays,0)
             JOIN divisionmaster div1 ON D.numDivisionID = div1.numDivisionID
			 JOIN companyinfo [Com1] ON com1.numCompanyID = div1.numCompanyID
			 LEFT JOIN RecurrenceConfiguration ON Opp.numOppBizDocsId = RecurrenceConfiguration.numOppBizDocID
			 LEFT JOIN RecurrenceTransaction ON Opp.numOppBizDocsId = RecurrenceTransaction.numRecurrOppBizDocID
      WHERE  opp.numOppBizDocsId = @numOppBizDocsId
             AND Mst.numDomainID = @numDomainID
    END
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OPPGetOppAddressDetails')
DROP PROCEDURE USP_OPPGetOppAddressDetails
GO
CREATE PROCEDURE [dbo].[USP_OPPGetOppAddressDetails]
(
    @numOppId        AS NUMERIC(9)  = NULL,
    @numDomainID     AS NUMERIC(9)  = 0
   )
AS

  DECLARE  @tintOppType  AS TINYINT, @tintBillType  AS TINYINT, @tintShipType  AS TINYINT
 
  DECLARE @numParentOppID AS NUMERIC,@numDivisionID AS NUMERIC,@numContactID AS NUMERIC, @numBillToAddressID NUMERIC(18,0), @numShipToAddressID NUMERIC(18,0)
      
  SELECT  @tintOppType = tintOppType,
		  @tintBillType = tintBillToType,
		  @tintShipType = tintShipToType,
          @numDivisionID = numDivisionID,
          @numContactID = numContactID,
		  @numBillToAddressID = ISNULL(numBillToAddressID,0),
		  @numShipToAddressID = ISNULL(numShipToAddressID,0)
      FROM   OpportunityMaster WHERE  numOppId = @numOppId


	 IF @tintOppType=2
		BEGIN
			--************Vendor Billing Address************
			IF @numBillToAddressID > 0
			BEGIN
				SELECT  
					'<pre>' + isnull(AD.vcStreet,'') + '</pre>' + isnull(AD.vcCity,'') + ', ' + isnull(dbo.fn_GetState(AD.numState),'') + ' ' + isnull(AD.vcPostalCode,'') + ' <br>' + isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcFullAddress,
					isnull(AD.vcStreet,'') AS vcStreet,
					isnull(AD.vcCity,'') AS vcCity,
					isnull(dbo.fn_GetState(AD.numState),'') AS vcState,
					isnull(AD.vcPostalCode,'') AS vcPostalCode,
					isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcCountry,
					ISNULL(AD.vcAddressName,'') AS vcAddressName,
            		(SELECT CMP.vcCompanyName FROM DivisionMaster DM JOIN CompanyInfo CMP ON DM.numCompanyId = CMP.numCompanyId and CMP.numDomainID = AD.numDomainID WHERE DM.numDivisionID = AD.numRecordID) AS vcCompanyName
				 FROM 
					AddressDetails AD 
				 WHERE	
					AD.numDomainID=@numDomainID 
					AND AD.numAddressID = @numBillToAddressID
			END
			ELSE
			BEGIN
				SELECT  '<pre>' + isnull(AD.vcStreet,'') + '</pre>' + isnull(AD.vcCity,'') + ', ' + isnull(dbo.fn_GetState(AD.numState),'') + ' ' + isnull(AD.vcPostalCode,'') + ' <br>' + isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcFullAddress,
						isnull(AD.vcStreet,'') AS vcStreet,
						isnull(AD.vcCity,'') AS vcCity,
						isnull(dbo.fn_GetState(AD.numState),'') AS vcState,
						isnull(AD.vcPostalCode,'') AS vcPostalCode,
						isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcCountry,
						ISNULL(AD.vcAddressName,'') AS vcAddressName,
            			CMP.vcCompanyName
				 FROM AddressDetails AD 
				 JOIN [DivisionMaster] DM ON DM.numDivisionID = AD.numRecordID   
				 JOIN CompanyInfo CMP ON DM.numCompanyId = CMP.numCompanyId and CMP.numDomainID = AD.numDomainID  
				 WHERE AD.numDomainID=@numDomainID AND AD.numRecordID=@numDivisionID 
				 AND AD.tintAddressOf = 2 AND AD.tintAddressType = 1 AND AD.bitIsPrimary=1
			END

			--************Vendor Shipping Address************
			IF @numShipToAddressID > 0
			BEGIN
				SELECT  
					'<pre>' + isnull(AD.vcStreet,'') + '</pre>' + isnull(AD.vcCity,'') + ', ' + isnull(dbo.fn_GetState(AD.numState),'') + ' ' + isnull(AD.vcPostalCode,'') + ' <br>' + isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcFullAddress,
					isnull(AD.vcStreet,'') AS vcStreet,
					isnull(AD.vcCity,'') AS vcCity,
					isnull(dbo.fn_GetState(AD.numState),'') AS vcState,
					isnull(AD.vcPostalCode,'') AS vcPostalCode,
					isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcCountry,
					ISNULL(AD.vcAddressName,'') AS vcAddressName,
            		(SELECT CMP.vcCompanyName FROM DivisionMaster DM JOIN CompanyInfo CMP ON DM.numCompanyId = CMP.numCompanyId and CMP.numDomainID = AD.numDomainID WHERE DM.numDivisionID = AD.numRecordID) AS vcCompanyName
				 FROM 
					AddressDetails AD 
				 WHERE	
					AD.numDomainID=@numDomainID 
					AND AD.numAddressID = @numShipToAddressID
			END
			ELSE
			BEGIN
				SELECT  '<pre>' + isnull(AD.vcStreet,'') + '</pre>' + isnull(AD.vcCity,'') + ', ' + isnull(dbo.fn_GetState(AD.numState),'') + ' ' + isnull(AD.vcPostalCode,'') + ' <br>' + isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcFullAddress,
					isnull(AD.vcStreet,'') AS vcStreet,
					isnull(AD.vcCity,'') AS vcCity,
					isnull(dbo.fn_GetState(AD.numState),'') AS vcState,
					isnull(AD.vcPostalCode,'') AS vcPostalCode,
					isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcCountry,
					ISNULL(AD.vcAddressName,'') AS vcAddressName,
					CMP.vcCompanyName
				FROM AddressDetails AD 
				JOIN [DivisionMaster] DM ON DM.numDivisionID = AD.numRecordID   
				JOIN CompanyInfo CMP ON DM.numCompanyId = CMP.numCompanyId and CMP.numDomainID = AD.numDomainID  
				WHERE AD.numDomainID=@numDomainID AND AD.numRecordID=@numDivisionID 
				AND AD.tintAddressOf = 2 AND AD.tintAddressType = 2 AND AD.bitIsPrimary=1
			END
			
			 --************Employer Billing Address************
			 SELECT '<pre>' + isnull(vcStreet,'') + '</pre>' + isnull(vcCity,'') + ', ' +isnull(vcState,'') + ' ' + isnull(vcPostalCode,'') + ' <br>' + isnull(vcCountry,'') AS vcFullAddress,
					isnull(vcStreet,'') AS vcStreet,
					isnull(vcCity,'') AS vcCity,
					isnull(vcState,'') AS vcState,
					isnull(vcPostalCode,'') AS vcPostalCode,
					isnull(vcCountry,'') AS vcCountry,
					ISNULL(vcAddressName,'') AS vcAddressName,isnull(vcCompanyName,'') AS vcCompanyName
					FROM fn_getOPPAddressDetails(@numOppId,@numDomainID,1) 
			 
			 
			 --************Employer Shipping Address************
			 SELECT '<pre>' + isnull(vcStreet,'') + '</pre>' + isnull(vcCity,'') + ', ' + isnull(vcState,'') + ' ' + isnull(vcPostalCode,'') + ' <br>' + isnull(vcCountry,'') AS vcFullAddress,
					isnull(vcStreet,'') AS vcStreet,
					isnull(vcCity,'') AS vcCity,
					isnull(vcState,'') AS vcState,
					isnull(vcPostalCode,'') AS vcPostalCode,
					isnull(vcCountry,'') AS vcCountry,
					ISNULL(vcAddressName,'') AS vcAddressName,isnull(vcCompanyName,'') AS vcCompanyName
					FROM fn_getOPPAddressDetails(@numOppId,@numDomainID,2) 
	  END

	IF @tintOppType=1 
		BEGIN
			--************Customer Billing Address************
			IF @numBillToAddressID > 0
			BEGIN
				SELECT  
					'<pre>' + isnull(AD.vcStreet,'') + '</pre>' + isnull(AD.vcCity,'') + ', ' + isnull(dbo.fn_GetState(AD.numState),'') + ' ' + isnull(AD.vcPostalCode,'') + ' <br>' + isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcFullAddress,
					isnull(AD.vcStreet,'') AS vcStreet,
					isnull(AD.vcCity,'') AS vcCity,
					isnull(dbo.fn_GetState(AD.numState),'') AS vcState,
					isnull(AD.vcPostalCode,'') AS vcPostalCode,
					isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcCountry,
					ISNULL(AD.vcAddressName,'') AS vcAddressName,
            		(SELECT CMP.vcCompanyName FROM DivisionMaster DM JOIN CompanyInfo CMP ON DM.numCompanyId = CMP.numCompanyId and CMP.numDomainID = AD.numDomainID WHERE DM.numDivisionID = AD.numRecordID) AS vcCompanyName
				 FROM 
					AddressDetails AD 
				 WHERE	
					AD.numDomainID=@numDomainID 
					AND AD.numAddressID = @numBillToAddressID
			END
			ELSE
			BEGIN
				SELECT 
					'<pre>' + isnull(vcStreet,'') + '</pre>' + isnull(vcCity,'') + ', ' + isnull(vcState,'') + ' ' + isnull(vcPostalCode,'') + ' <br>' + isnull(vcCountry,'') AS vcFullAddress,
					isnull(vcStreet,'') AS vcStreet,
					isnull(vcCity,'') AS vcCity,
					isnull(vcState,'') AS vcState,
					isnull(vcPostalCode,'') AS vcPostalCode,
					isnull(vcCountry,'') AS vcCountry,
					ISNULL(vcAddressName,'') AS vcAddressName,isnull(vcCompanyName,'') AS vcCompanyName
				FROM 
					fn_getOPPAddressDetails(@numOppId,@numDomainID,1) 
			END
		
			--************Customer Shipping Address************
			IF @numShipToAddressID > 0
			BEGIN
				SELECT  
					'<pre>' + isnull(AD.vcStreet,'') + '</pre>' + isnull(AD.vcCity,'') + ', ' + isnull(dbo.fn_GetState(AD.numState),'') + ' ' + isnull(AD.vcPostalCode,'') + ' <br>' + isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcFullAddress,
					isnull(AD.vcStreet,'') AS vcStreet,
					isnull(AD.vcCity,'') AS vcCity,
					isnull(dbo.fn_GetState(AD.numState),'') AS vcState,
					isnull(AD.vcPostalCode,'') AS vcPostalCode,
					isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcCountry,
					ISNULL(AD.vcAddressName,'') AS vcAddressName,
            		(SELECT CMP.vcCompanyName FROM DivisionMaster DM JOIN CompanyInfo CMP ON DM.numCompanyId = CMP.numCompanyId and CMP.numDomainID = AD.numDomainID WHERE DM.numDivisionID = AD.numRecordID) AS vcCompanyName
				 FROM 
					AddressDetails AD 
				 WHERE	
					AD.numDomainID=@numDomainID 
					AND AD.numAddressID = @numShipToAddressID
			END
			ELSE
			BEGIN
				SELECT 
					'<pre>' + isnull(vcStreet,'') + '</pre>' + vcCity + ', ' + isnull(vcState,'') + ' ' + isnull(vcPostalCode,'') + ' <br>' + isnull(vcCountry,'') AS vcFullAddress,
					isnull(vcStreet,'') AS vcStreet,
					isnull(vcCity,'') AS vcCity,
					isnull(vcState,'') AS vcState,
					isnull(vcPostalCode,'') AS vcPostalCode,
					isnull(vcCountry,'') AS vcCountry,
					ISNULL(vcAddressName,'') AS vcAddressName,isnull(vcCompanyName,'') AS vcCompanyName
				FROM 
					fn_getOPPAddressDetails(@numOppId,@numDomainID,2) 
			END
	
			--************Employer Billing Address************ --(For SO take Domain Address)
			SELECT  '<pre>' + isnull(AD.vcStreet,'') + '</pre>' + isnull(AD.vcCity,'') + ', ' + isnull(dbo.fn_GetState(AD.numState),'') + ' ' + isnull(AD.vcPostalCode,'') + ' <br>' + isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcFullAddress,
					isnull(AD.vcStreet,'') AS vcStreet,
					isnull(AD.vcCity,'') AS vcCity,
					isnull(dbo.fn_GetState(AD.numState),'') AS vcState,
					isnull(AD.vcPostalCode,'') AS vcPostalCode,
					isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcCountry,
					ISNULL(AD.vcAddressName,'') AS vcAddressName,vcCompanyName
             FROM Domain D1 JOIN divisionmaster div1 ON D1.numDivisionID = div1.numDivisionID
				  JOIN companyinfo [Com1] ON com1.numCompanyID = div1.numCompanyID
                  JOIN dbo.AddressDetails AD ON AD.numDomainID = div1.numDomainID AND AD.numRecordID = div1.numDivisionID 
              WHERE  D1.numDomainID = @numDomainID
				AND AD.tintAddressOf = 2 AND AD.tintAddressType = 1 AND AD.bitIsPrimary=1


			--************Employer Shipping Address************ --(For SO take Domain Address)
			SELECT  '<pre>' + isnull(AD.vcStreet,'') + '</pre>' + isnull(AD.vcCity,'') + ', ' + isnull(dbo.fn_GetState(AD.numState),'') + ' ' + isnull(AD.vcPostalCode,'') + ' <br>' + isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcFullAddress,
					isnull(AD.vcStreet,'') AS vcStreet,
					isnull(AD.vcCity,'') AS vcCity,
					isnull(dbo.fn_GetState(AD.numState),'') AS vcState,
					isnull(AD.vcPostalCode,'') AS vcPostalCode,
					isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcCountry,
					ISNULL(AD.vcAddressName,'') AS vcAddressName,vcCompanyName
             FROM Domain D1 JOIN divisionmaster div1 ON D1.numDivisionID = div1.numDivisionID
				  JOIN companyinfo [Com1] ON com1.numCompanyID = div1.numCompanyID
                  JOIN dbo.AddressDetails AD ON AD.numDomainID = div1.numDomainID AND AD.numRecordID = div1.numDivisionID 
			 WHERE AD.numDomainID=@numDomainID 
			 AND AD.tintAddressOf = 2 AND AD.tintAddressType = 2 AND AD.bitIsPrimary=1
	  END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityMaster_CreatePO')
DROP PROCEDURE dbo.USP_OpportunityMaster_CreatePO
GO
CREATE PROCEDURE [dbo].[USP_OpportunityMaster_CreatePO]
(
	@numOppID NUMERIC(18,0) OUTPUT,
	@numDomainID NUMERIC(18,0),
	@numUserCntID NUMERIC(18,0),
	@numItemCode NUMERIC(18,0),
	@numUnitHour NUMERIC(18,0),
	@numWarehouseItemID NUMERIC(18,0),
	@bitFromWorkOrder BIT,
	@numOrderStatus AS NUMERIC(18,0) = 0
)
AS 
BEGIN
    DECLARE @ErrorMessage NVARCHAR(4000);SET @ErrorMessage=''
    DECLARE @ErrorSeverity INT;
    DECLARE @ErrorState INT;
    
	BEGIN TRY
	BEGIN TRAN
		DECLARE @tintMinOrderQty AS INTEGER
		DECLARE @numUnitPrice MONEY
		DECLARE @numContactID NUMERIC(18,0)
	    DECLARE @numDivisionID NUMERIC(18,0)
		DECLARE @numVendorID NUMERIC(18,0)
		DECLARE @fltExchangeRate float    
		DECLARE @numCurrencyID NUMERIC(18,0)
		DECLARE @vcPOppName VARCHAR(1000) = ''                         
		DECLARE @hDocItem int                                                                                                                            
		DECLARE @TotalAmount as money                                                                          
		DECLARE @tintOppStatus as tinyint               
		DECLARE @tintDefaultClassType NUMERIC
		DECLARE @numDefaultClassID NUMERIC;SET @numDefaultClassID=0

		SELECT @numDivisionID = numVendorID FROM Item WHERE numDomainID=@numDomainID AND numItemCode=@numItemCode

		IF ISNULL(@numDivisionID,0) > 0
		BEGIN
			SELECT @numContactID = numContactId FROM AdditionalContactsInformation WHERE numDivisionId = @numDivisionID AND bitPrimaryContact = 1

			IF @numContactID IS NULL
			BEGIN
				SELECT TOP 1 @numContactID = numContactId FROM AdditionalContactsInformation WHERE numDivisionId = @numDivisionID
			END

			SELECT @tintMinOrderQty=ISNULL(intMinQty,0) FROm Vendor WHERE numVendorID = @numDivisionID AND numItemCode = @numItemCode AND numDomainID=@numDomainID
			SET @numUnitHour = @numUnitHour + @tintMinOrderQty
			SET @numUnitHour = IIF(@numUnitHour = 0, 1, @numUnitHour)

			SELECT @numUnitPrice = dbo.fn_FindVendorCost(@numItemCOde,@numDivisionID,@numDomainID,@numUnitHour)

			--Get Default Item Class for particular user based on Domain settings  
			SELECT @tintDefaultClassType=isnull(tintDefaultClassType,0) FROM dbo.Domain  WHERE numDomainID = @numDomainID 

			IF @tintDefaultClassType=0
				SELECT @numDefaultClassID=NULLIF(numDefaultClass,0) FROM dbo.UserMaster  WHERE numUserDetailId =@numUserCntID AND numDomainID = @numDomainID 
		
			DECLARE @intOppTcode AS NUMERIC(9)
			SELECT @intOppTcode=max(numOppId) FROM OpportunityMaster                
			SET @intOppTcode =isnull(@intOppTcode,0) + 1                                                
			SET @vcPOppName= @vcPOppName + '-' + convert(varchar(4),year(getutcdate()))  + '-A' + convert(varchar(10),@intOppTcode)                                  
  
			SET @fltExchangeRate=1
			SELECT @numCurrencyID= ISNULL(numCurrencyID,0) FROM dbo.Domain WHERE numDomainId=@numDomainId                                                                       
  
			--Set Default Class If enable User Level Class Accountng 
			DECLARE @numAccountClass AS NUMERIC(18);
			SET @numAccountClass=0
		
			SELECT 
				@numAccountClass=ISNULL(numDefaultClass,0) 
			FROM 
				dbo.UserMaster UM 
			JOIN 
				dbo.Domain D 
			ON 
				UM.numDomainID=D.numDomainId
			WHERE 
				D.numDomainId=@numDomainId 
				AND UM.numUserDetailId=@numUserCntID 
				AND ISNULL(D.IsEnableUserLevelClassTracking,0) = 1
                                                                       
			INSERT INTO OpportunityMaster                                                                          
			(                                                                             
				numContactId,numDivisionId,bitPublicFlag,tintSource,
				tintSourceType,vcPOppName,intPEstimatedCloseDate,numCreatedBy,bintCreatedDate,                                                                           
				numDomainId,numRecOwner,lngPConclAnalysis,tintOppType,                                                              
				numSalesOrPurType,numCurrencyID,fltExchangeRate,[tintOppStatus],numStatus,
				vcOppRefOrderNo,bitStockTransfer,bitBillingTerms,intBillingDays,bitInterestType,fltInterest,vcCouponCode,
				bitDiscountType,fltDiscount,bitPPVariance,numAccountClass,tintTaxOperator,bitUseShippersAccountNo,bitUseMarkupShippingRate,
				numMarkupShippingRate,intUsedShippingCompany,bitFromWorkOrder
			)                                                                          
			Values                                                                          
			(                                                                          
				@numContactId,@numDivisionId,0,0,1,@vcPOppName,DATEADD(D,1,GETUTCDATE()),@numUserCntID,GETUTCDATE(),
				@numDomainId,@numUserCntID,0,2,0,@numCurrencyID,@fltExchangeRate,1,
				@numOrderStatus,NULL,0,0,0,0,0,'',0,0,1,@numAccountClass,0,0,0,0.00,0,@bitFromWorkOrder
			)                                                                                                                      
  
			set @numOppID=scope_identity()                                                
  
			--UPDATE OPPNAME AS PER NAME TEMPLATE
			EXEC dbo.USP_UpdateNameTemplateValue 2,@numDomainID,@numOppID

			EXEC dbo.USP_AddParentChildCustomFieldMap
			@numDomainID = @numDomainID,
			@numRecordID = @numOppID,
			@numParentRecId = @numDivisionId,
			@tintPageID = 6
 	
			--INSERTING ITEMS                                       
			IF @numOppID>0
			BEGIN                      

				INSERT INTO OpportunityItems                                                                          
				(
					numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,
					vcType,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,vcItemName,vcModelID,
					vcManufacturer,vcPathForTImage,monVendorCost,numUOMId,bitWorkOrder,numVendorWareHouse,
					numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,numClassID,numToWarehouseItemID,
					vcAttributes,monAvgCost,bitItemPriceApprovalRequired
				)
				SELECT 
					@numOppID,@numItemCode,@numUnitHour,@numUnitPrice,(@numUnitPrice * @numUnitHour),Item.txtItemDesc,@numWarehouseItemID,
					'',0,0,0,(@numUnitPrice * @numUnitHour),Item.vcItemName,Item.vcModelID,Item.vcManufacturer,
					(SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = Item.numItemCode AND BitDefault = 1 AND numdomainId = @numdomainId),
					(select isnull(VN.monCost,0) from Vendor VN, Item IT where IT.numItemCode=VN.numItemCode and  VN.numItemCode=Item.numItemCode and VN.numVendorID=IT.numVendorID),
					Item.numBaseUnit,0,NULL,NULL,NULL,NULL,NULL,
					Case When @tintDefaultClassType=0 then @numDefaultClassID else (SELECT numItemClass FROM item WHERE numItemCode = Item.numItemCode) end,
					NULL,'', (SELECT ISNULL(monAverageCost,0) FROM Item I WHERE I.numDomainId=@numDomainId AND I.numItemCode=Item.numItemCode),0
				FROM
					Item
				WHERE
					numItemCode = @numItemCode
			END          
	
			SET @TotalAmount=0                                                           
			SELECT @TotalAmount=sum(monTotAmount) FROM OpportunityItems WHERE numOppId=@numOppID                                                                          
			UPDATE OpportunityMaster SET monPamount=@TotalAmount WHERE numOppId=@numOppID                                                 
                      
			--Insert Tax for Division   
			IF (select isnull(bitPurchaseTaxCredit,0) from Domain where numDomainId=@numDomainId)=1
			BEGIN 
				INSERT dbo.OpportunityMasterTaxItems 
				(
					numOppId,
					numTaxItemID,
					fltPercentage
				) 
				SELECT 
					@numOppID,
					TI.numTaxItemID,
					dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,TI.numTaxItemID,@numOppId,0,NULL) 
				FROM 
					TaxItems TI 
				JOIN 
					DivisionTaxTypes DTT ON TI.numTaxItemID = DTT.numTaxItemID
				WHERE 
					DTT.numDivisionID=@numDivisionId AND DTT.bitApplicable=1
				UNION 
				SELECT
					@numOppID,
					0,
					dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,0,@numOppId,1,NULL) 
				FROM 
					dbo.DivisionMaster 
				WHERE 
					bitNoTax=0 AND 
					numDivisionID=@numDivisionID
			END

			/*Added by chintan - While creating SO from PO it wasn't puting Item Qty to Allocation. this segment was being called from USP_OppSaveMilestone which No longer in use after changes anoop did. */     
			--Updating the warehouse items              
			DECLARE @tintShipped AS TINYINT               
			DECLARE @tintOppType AS TINYINT
			SELECT 
				@tintOppStatus=tintOppStatus,
				@tintOppType=tintOppType,
				@tintShipped=tintShipped 
			FROM 
				OpportunityMaster 
			WHERE 
				numOppID=@numOppID              
		
			IF @tintOppStatus=1               
			BEGIN         
				EXEC USP_UpdatingInventoryonCloseDeal @numOppID,@numUserCntID
			END

			DECLARE @tintCRMType as numeric(9)        
			DECLARE @AccountClosingDate as datetime        

			select @numDivisionID=numDivisionID,@AccountClosingDate=bintAccountClosingDate from  OpportunityMaster where numOppID=@numOppID         
			select @tintCRMType=tintCRMType from divisionmaster where numDivisionID =@numDivisionID 

			IF @AccountClosingDate is null                   
			UPDATE OpportunityMaster set bintAccountClosingDate=getutcdate() where numOppID=@numOppID              
               
			/*Added by chintan -  Biz usually automatically promotes the record from Lead/Prospect to Account When a Sales Opp/Order is made against it*/  
			-- Promote Lead to Prospect when Sales/Purchase Opp is created against it
			IF @tintCRMType=0 AND 1 = 0 --Lead & Open Opp
			BEGIN
				UPDATE divisionmaster set tintCRMType=1,bintLeadProm=getutcdate(),bintLeadPromBy=@numUserCntID,bintProsProm=getutcdate(),bintProsPromBy=@numUserCntID        
				WHERE numDivisionID=@numDivisionID        
			END        
			-- Promote Lead to Account when Sales/Purchase Order is created against it
			ELSE if @tintCRMType=0 AND 1 = 1 --Lead & Order
			BEGIN 
				UPDATE divisionmaster set tintCRMType=2,bintLeadProm=getutcdate(),bintLeadPromBy=@numUserCntID,bintProsProm=getutcdate(),bintProsPromBy=@numUserCntID        
				WHERE numDivisionID=@numDivisionID        
			END
			--Promote Prospect to Account
			ELSE IF @tintCRMType=1 AND 1 = 1 
			BEGIN        
				update divisionmaster set tintCRMType=2,bintProsProm=getutcdate(),bintProsPromBy=@numUserCntID        
				where numDivisionID=@numDivisionID        
			END

			--Add/Update Address Details
			DECLARE @vcStreet  varchar(100),@vcCity  varchar(50),@vcPostalCode  varchar(15),@vcCompanyName  varchar(100),@vcAddressName VARCHAR(50)      
			DECLARE @numState  numeric(9),@numCountry  numeric(9), @numCompanyId  numeric(9) 
			DECLARE @bitIsPrimary BIT;

			SELECT  @vcCompanyName=vcCompanyName,@numCompanyId=div.numCompanyID FROM CompanyInfo Com                            
			JOIN divisionMaster Div                            
			ON div.numCompanyID=com.numCompanyID                            
			WHERE div.numdivisionID=@numDivisionId

			--INSERT TAX FOR OPPORTUNITY ITEMS
			INSERT INTO dbo.OpportunityItemsTaxItems 
			(
				numOppId,
				numOppItemID,
				numTaxItemID
			) 
			SELECT 
				@numOppId,OI.numoppitemtCode,TI.numTaxItemID 
			FROM 
				dbo.OpportunityItems OI 
			JOIN dbo.ItemTax IT ON OI.numItemCode=IT.numItemCode 
			JOIN TaxItems TI ON TI.numTaxItemID=IT.numTaxItemID 
			WHERE 
				OI.numOppId=@numOppID 
				AND IT.bitApplicable=1 
				AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
			UNION
			SELECT 
				@numOppId,OI.numoppitemtCode,0 
			FROM 
				dbo.OpportunityItems OI 
			JOIN dbo.Item I ON OI.numItemCode=I.numItemCode
			WHERE 
				OI.numOppId=@numOppID  
				AND I.bitTaxable=1 
				AND	OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)

			UPDATE OpportunityMaster SET monDealAmount=dbo.GetDealAmount(@numOppID,getdate(),0) WHERE numOppId=@numOppID
		END
	COMMIT TRAN 
	END TRY
	BEGIN CATCH
		IF ( @@TRANCOUNT > 0 ) 
		BEGIN
			ROLLBACK TRAN
		END

		SELECT @ErrorMessage = ERROR_MESSAGE(),@ErrorSeverity = ERROR_SEVERITY(),@ErrorState = ERROR_STATE()
		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState);
	END CATCH;
END

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_SimpleSearch')
DROP PROCEDURE USP_SimpleSearch
GO
CREATE PROCEDURE [dbo].[USP_SimpleSearch]                             
	@numDomainID NUMERIC (18,0),
	@numUserCntID NUMERIC(18,0),
	@searchText VARCHAR(100),
	@searchType VARCHAR(1),
	@searchCriteria VARCHAR(1),
	@orderType VARCHAR(1),
	@bizDocType AS INT ,
	@numSkip AS INT                                                                   
AS                          
	DECLARE @strSQL AS VARCHAR(MAX) = ''
	
	IF @searchType = '1' -- Organization
	BEGIN
		IF @searchCriteria = '1' --Organizations
		BEGIN
			EXEC USP_CompanyInfo_Search @numDomainID=@numDomainID,@numUserCntID=@numUserCntID,@isStartWithSearch=0,@searchText=@searchText
		END
		ELSE
		BEGIN
			--GET FIELDS CONFIGURED FOR ORGANIZATION SEARCH
			SELECT * INTO #tempOrgSearch FROM
			(
				SELECT 
					numFieldId,
					vcDbColumnName,
					ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName,
					vcLookBackTableName,
					tintRow AS tintOrder,
					0 as Custom
				FROM 
					View_DynamicColumns
				WHERE 
					numFormId=97 AND numUserCntID=@numUserCntID AND numDomainID=@numDomainID AND 
					tintPageType=1 AND bitCustom=0  AND ISNULL(bitSettingField,0)=1 AND numRelCntType=0
				UNION   
				SELECT 
					numFieldId,
					vcFieldName,
					vcFieldName,
					'CFW_FLD_Values',
					tintRow AS tintOrder,
					1 as Custom
				FROM 
					View_DynamicCustomColumns   
				WHERE 
					Grp_id=1 AND numFormId=97 AND numUserCntID=@numUserCntID AND numDomainID=@numDomainID AND 
					tintPageType=1 AND bitCustom=1 AND numRelCntType=0
			)Y
			
			DECLARE @searchSQL AS VARCHAR(MAX) = ''

			IF (SELECT COUNT(*) FROM #tempOrgSearch) > 0
			BEGIN
				SELECT  
					@searchSQL = STUFF(
										(SELECT ' OR ' + + CONCAT((CASE WHEN vcLookBackTableName = 'AddressDetails' THEN '' ELSE CONCAT(vcLookBackTableName,'.') END),IIF(Custom = 1,'Fld_Value',vcDbColumnName)) + ' LIKE ''%' + @searchText + '%''' FROM #tempOrgSearch FOR XML PATH(''))
										, 1
										, 4
										, ''
										)
			END
			ELSE
			BEGIN
				SET @searchSQL = 'CompanyInfo.vcCompanyName LIKE ''%' + @searchText + '%'''
			END

			CREATE TABLE #TEMPOrganization
			(
				numDivisionID NUMERIC(18,0),
				vcCompanyName VARCHAR(500)
			)


			SET @strSQL = 'INSERT INTO 
								#TEMPOrganization
							SELECT
								DivisionMaster.numDivisionID
								,CompanyInfo.vcCompanyName
							FROM
								DivisionMaster 
							INNER JOIN
								CompanyInfo 
							ON
								CompanyInfo.numCompanyId = DivisionMaster.numCompanyID
							LEFT JOIN
								AdditionalContactsInformation
							ON
								AdditionalContactsInformation.numDivisionId = DivisionMaster.numDivisionID
							LEFT JOIN
								OpportunityMaster
							ON
								OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
							LEFT JOIN
								Cases
							ON
								Cases.numDivisionID = DivisionMaster.numDivisionID
							LEFT JOIN
								ProjectsMaster
							ON
								ProjectsMaster.numDivisionId = DivisionMaster.numDivisionID
							OUTER APPLY 
								(
									SELECT
										isnull(vcStreet,'''') as vcBillStreet, 
										isnull(vcCity,'''') as vcBillCity, 
										isnull(dbo.fn_GetState(numState),'''') as numBillState,
										isnull(vcPostalCode,'''') as vcBillPostCode,
										isnull(dbo.fn_GetListName(numCountry,0),'''')  as numBillCountry
									FROM
										dbo.AddressDetails 
									WHERE 
										numDomainID = ' + CAST(@numDomainID AS VARCHAR) + '
										AND numRecordID=DivisionMaster.numDivisionID 
										AND tintAddressOf=2 
										AND tintAddressType=1 
								)  AD1
							OUTER APPLY
								(
									SELECT
										isnull(vcStreet,'''') as vcShipStreet,
										isnull(vcCity,'''') as vcShipCity,
										isnull(dbo.fn_GetState(numState),'''')  as numShipState,
										isnull(vcPostalCode,'''') as vcShipPostCode, 
										isnull(dbo.fn_GetListName(numCountry,0),'''') numShipCountry
									FROM
										dbo.AddressDetails 
									WHERE 
										numDomainID = ' + CAST(@numDomainID AS VARCHAR) + '
										AND numRecordID= DivisionMaster.numDivisionID 
										AND tintAddressOf=2 
										AND tintAddressType=2	
								) AD2	
							LEFT JOIN
								CFW_FLD_Values
							ON
								CFW_FLD_Values.RecId = DivisionMaster.numDivisionID
								AND CFW_FLD_Values.Fld_ID IN (SELECT numFieldID FROM #tempOrgSearch)
							WHERE
								DivisionMaster.numDomainID = ' + CAST(@numDomainID AS VARCHAR) + '
								AND (' + @searchSQL + ')
							GROUP BY
								DivisionMaster.numDivisionID
								,CompanyInfo.vcCompanyName'

			PRINT @strSQL
			EXEC (@strSQL)
			IF @searchCriteria = '2' --Items
			BEGIN
				DECLARE @TEMPORGITEM TABLE
				(
					numOppId NUMERIC(18,0),
					bintCreatedDate DATETIME,
					vcItemName VARCHAR(500),
					vcPOppName VARCHAR(200),
					vcOppType VARCHAR(50),
					numUnitHour INT,
					monPrice NUMERIC(18,2),
					vcCompanyName  VARCHAR(200),
					tintType TINYINT
				)

				INSERT INTO @TEMPORGITEM
				(
					numOppId,
					bintCreatedDate,
					vcItemName,
					vcPOppName,
					vcOppType,
					numUnitHour,
					monPrice,
					vcCompanyName,
					tintType
				)
				SELECT
					numOppId,
					bintCreatedDate,
					vcItemName,
					vcPOppName,
					vcOppType,
					CAST(numUnitHour AS INT) numUnitHour,
					CAST(monPrice AS decimal(18,2)) monPrice,
					vcCompanyName,
					tintType
				FROM
				(
					SELECT
						OpportunityMaster.numOppId,
						OpportunityMaster.bintCreatedDate,
						CONCAT(Item.vcItemName,',',Item.vcModelID) AS vcItemName,
						OpportunityMaster.vcPOppName,
						CASE 
							WHEN tintOppType = 1 THEN 'Sales ' + (CASE WHEN tintOppStatus = 1 THEN 'Order' ELSE 'Opportunity' END)
							WHEN tintOppType = 2 THEN 'Purchase ' + (CASE WHEN tintOppStatus = 1 THEN 'Order' ELSE 'Opportunity' END)
						END vcOppType,
						OpportunityItems.numUnitHour,
						OpportunityItems.monPrice,
						TEMP.vcCompanyName,
						1 AS tintType
					FROM
						OpportunityMaster
					INNER JOIN
						#TEMPOrganization TEMP
					ON
						OpportunityMaster.numDivisionId = TEMP.numDivisionID
					INNER JOIN
						OpportunityItems
					ON
						OpportunityMaster.numOppId = OpportunityItems.numOppId
					INNER JOIN
						Item
					ON
						OpportunityItems.numItemCode = Item.numItemCode
					WHERE
						OpportunityMaster.numDomainId = @numDomainID
					UNION
					SELECT
						ReturnHeader.numReturnHeaderID AS numOppID,
						ReturnHeader.dtCreatedDate AS bintCreatedDate,
						CONCAT(Item.vcItemName,',',Item.vcModelID) AS vcItemName,
						ReturnHeader.vcRMA AS vcPOppName,
						CASE 
							WHEN tintReturnType = 1 THEN 'Sales Return'
							WHEN tintReturnType = 2 THEN 'Purchase Return'
						END vcOppType,
						ReturnItems.numUnitHour,
						ReturnItems.monPrice,
						TEMP.vcCompanyName,
						2 AS tintType
					FROM
						ReturnHeader
					INNER JOIN
						#TEMPOrganization TEMP
					ON
						ReturnHeader.numDivisionId = TEMP.numDivisionID
					INNER JOIN	
						ReturnItems
					ON
						ReturnHeader.numReturnHeaderID = ReturnItems.numReturnHeaderID
					INNER JOIN
						Item
					ON
						Item.numItemCode = ReturnItems.numItemCode
					WHERE
						ReturnHeader.numDomainId = @numDomainID AND tintReturnType IN (1,2)
				) TEMPFinal

				SELECT  
					ISNULL(numOppId,0) AS numOppId,
					ISNULL(dbo.FormatedDateFromDate(bintCreatedDate,@numDomainID),'') bintCreatedDate,
					ISNULL(vcItemName,'') vcItemName,
					ISNULL(vcPOppName,'') vcPOppName,
					ISNULL(vcOppType,'') vcOppType,
					ISNULL(numUnitHour,0)numUnitHour,
					ISNULL(monPrice,0.00) monPrice,
					ISNULL(vcCompanyName,'') vcCompanyName,
					ISNULL(tintType,'') tintType
				FROM 
					@TEMPORGITEM 
				ORDER BY 
					bintCreatedDate DESC 
				OFFSET @numSkip ROWS FETCH NEXT 10 ROWS ONLY

				SELECT COUNT(numOppId) AS TotalItems FROM @TEMPORGITEM 
			END
			ELSE IF @searchCriteria = '3' -- Opp/Orders
			BEGIN			
				DECLARE @TEMPORGORDER TABLE
				(
					numOppId NUMERIC(18,0),
					bintCreatedDate DATETIME,
					vcPOppName VARCHAR(200),
					vcOppType VARCHAR(50),
					vcCompanyName  VARCHAR(200),
					tintType TINYINT
				)

				INSERT INTO 
					@TEMPORGORDER
				SELECT
					numOppId,
					bintCreatedDate,
					vcPOppName,
					vcOppType,
					vcCompanyName,
					tintType
				FROM
				(
					SELECT
						OpportunityMaster.numOppId,
						OpportunityMaster.vcPOppName,
						CASE 
							WHEN tintOppType = 1 THEN 'Sales ' + (CASE WHEN tintOppStatus = 1 THEN 'Order' ELSE 'Opportunity' END)
							WHEN tintOppType = 2 THEN 'Purchase ' + (CASE WHEN tintOppStatus = 1 THEN 'Order' ELSE 'Opportunity' END)
						END vcOppType,
						TEMP.vcCompanyName,
						bintCreatedDate,
						1 AS tintType
					FROM
						OpportunityMaster
					INNER JOIN
						#TEMPOrganization TEMP
					ON
						OpportunityMaster.numDivisionId = TEMP.numDivisionID
					WHERE
						numDomainId = @numDomainID
						AND tintOppType <> 0
					UNION 
					SELECT
						ReturnHeader.numReturnHeaderID AS numOppID,
						ReturnHeader.vcRMA AS vcPOppName,
						CASE 
							WHEN tintReturnType = 1 THEN 'Sales Return'
							WHEN tintReturnType = 2 THEN 'Purchase Return'
						END vcOppType,
						TEMP.vcCompanyName,
						dtCreatedDate AS bintCreatedDate,
						2 AS tintType
					FROM
						ReturnHeader
					INNER JOIN
						#TEMPOrganization TEMP
					ON
						ReturnHeader.numDivisionId = TEMP.numDivisionID
					WHERE
						numDomainId = @numDomainID AND tintReturnType IN (1,2)
				) TEMPFinal

				SELECT
					ISNULL(numOppId,0) numOppId,
					ISNULL(dbo.FormatedDateFromDate(bintCreatedDate,@numDomainID),'') bintCreatedDate,
					ISNULL(vcPOppName,'') vcPOppName,
					ISNULL(vcOppType,'') vcOppType,
					ISNULL(vcCompanyName,'') vcCompanyName,
					ISNULL(tintType,0) tintType
				FROM
					@TEMPORGORDER
				ORDER BY
					bintCreatedDate DESC
				OFFSET @numSkip ROWS FETCH NEXT 10 ROWS ONLY	
				
				SELECT COUNT(*) AS TotalRows FROM @TEMPORGORDER			
			END
			ELSE IF @searchCriteria = '4' -- BizDocs
			BEGIN
				DECLARE @TEMPORGBizDoc TABLE
				(
					numOppBizDocsId NUMERIC(18,0),
					bintCreatedDate DATETIME,
					vcBizDocID VARCHAR(200),
					vcBizDocType VARCHAR(100),
					vcCompanyName  VARCHAR(200)
				)

				INSERT INTO
					@TEMPORGBizDoc
				SELECT 
					numOppBizDocsId,
					OpportunityBizDocs.dtCreatedDate,
					vcBizDocID,
					ListDetails.vcData AS vcBizDocType,
					TEMP.vcCompanyName
				FROM 
					OpportunityBizDocs
				INNER JOIN
					OpportunityMaster
				ON
					OpportunityBizDocs.numOppId = OpportunityMaster.numOppId
				INNER JOIN
					#TEMPOrganization TEMP
				ON
					OpportunityMaster.numDivisionId = TEMP.numDivisionID
				INNER JOIN
					ListDetails
				ON
					ListDetails.numListID = 27
					AND OpportunityBizDocs.numBizDocId = ListDetails.numListItemID
				WHERE
					OpportunityMaster.numDomainId = @numDomainID

				SELECT
					ISNULL(numOppBizDocsId,0) numOppBizDocsId,
					ISNULL(dbo.FormatedDateFromDate(bintCreatedDate,@numDomainID),'') AS dtCreatedDate,
					ISNULL(vcBizDocID,'') vcBizDocID,
					ISNULL(vcBizDocType,'') vcBizDocType,
					ISNULL(vcCompanyName,'') vcCompanyName
				FROM
					@TEMPORGBizDoc
				ORDER BY
					bintCreatedDate DESC
				OFFSET @numSkip ROWS FETCH NEXT 10 ROWS ONLY
				
				SELECT COUNT(*) AS TotalRows FROM @TEMPORGBizDoc					
			END

			DROP TABLE #tempOrgSearch
			DROP TABLE #TEMPOrganization
		END
	END
	ELSE IF @searchType = '2' -- Item
	BEGIN
		CREATE TABLE #TempSearchedItem
		(
			numItemCode NUMERIC(18,0), 
			vcItemName VARCHAR(200), 
			vcPathForTImage VARCHAR(200), 
			vcCompanyName VARCHAR(200), 
			monListPrice NUMERIC(18,0), 
			txtItemDesc TEXT, 
			vcModelID VARCHAR(100), 
			numBarCodeId VARCHAR(100), 
			vcBarCode VARCHAR(100), 
			vcSKU VARCHAR(100),
			vcWHSKU VARCHAR(100), 
			vcPartNo VARCHAR(100),
			numWarehouseItemID NUMERIC(18,0), 
			vcAttributes VARCHAR(500)
		)

		DECLARE @strItemSearch AS VARCHAR(1000) = ''
		DECLARE @strAttributeSearch AS VARCHAR(1000) = ''
		DECLARE @strSearch AS VARCHAR(1000)

		IF CHARINDEX(',',@searchText) > 0
		BEGIN
			SET @strItemSearch = LTRIM(RTRIM(SUBSTRING(@searchText,0,CHARINDEX(',',@searchText))))
			SET @strAttributeSearch = SUBSTRING(@searchText,CHARINDEX(',',@searchText) + 1,LEN(@searchText)) 

			IF LEN(@strAttributeSearch) > 0
			BEGIN
				SET @strAttributeSearch = 'TEMPAttributes.vcAttributes LIKE ''%' + REPLACE(@strAttributeSearch,',','%'' AND TEMPAttributes.vcAttributes LIKE ''%') + '%'''
			END
			ELSE
			BEGIN
				SET @strAttributeSearch = ''
			END
		END
		ELSE
		BEGIN
			SET @strItemSearch = @searchText
		END

		DECLARE @vcCustomDisplayField AS VARCHAR(4000) = ''
		SELECT * INTO #TempItemCustomDisplayFields FROM
		(
			SELECT 
				numFieldId,
				vcFieldName
			FROM 
				View_DynamicCustomColumns   
			WHERE 
				Grp_id=5 AND numFormId=22 AND numUserCntID=@numUserCntID AND numDomainID=@numDomainID AND 
				tintPageType=1 AND bitCustom=1 AND numRelCntType=0
		)TD

		IF (SELECT COUNT(*) FROM #TempItemCustomDisplayFields) > 0
		BEGIN
			SELECT @vcCustomDisplayField = STUFF(
									(SELECT ', ' +  'ISNULL(dbo.GetCustFldValueItem(' + CAST(numFieldId AS VARCHAR) + ',numItemCode),'''') AS [' + vcFieldName + ']' FROM #TempItemCustomDisplayFields FOR XML PATH(''))
									, 1
									, 0
									, ''
								)
		END

		SELECT * INTO #TempItemSearchFields FROM
		(
			SELECT 
				numFieldId,
				vcDbColumnName,
				ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName,
				vcLookBackTableName,
				tintRow AS tintOrder,
				0 as Custom
			FROM 
				View_DynamicColumns
			WHERE 
				numFormId=22 AND numUserCntID=@numUserCntID AND numDomainID=@numDomainID AND 
				tintPageType=1 AND bitCustom=0  AND ISNULL(bitSettingField,0)=1 AND numRelCntType=1
				AND vcDbColumnName <> 'vcAttributes'
			UNION   
			SELECT 
				numFieldId,
				vcFieldName,
				vcFieldName,
				'CFW_FLD_Values',
				tintRow AS tintOrder,
				1 as Custom
			FROM 
				View_DynamicCustomColumns   
			WHERE 
				Grp_id=5 AND numFormId=22 AND numUserCntID=@numUserCntID AND numDomainID=@numDomainID AND 
				tintPageType=1 AND bitCustom=1 AND numRelCntType=1
		)Y

		IF (SELECT COUNT(*) FROM #TempItemSearchFields) > 0
		BEGIN
			SELECT  @strSearch = STUFF(
									(SELECT ' OR ' +  IIF(Custom = 1,'dbo.GetCustFldValueItem('+ CAST(numFieldId AS VARCHAR) +',numItemCode)',vcDbColumnName) + ' LIKE ''%' + @strItemSearch + '%''' FROM #TempItemSearchFields FOR XML PATH(''))
									, 1
									, 3
									, ''
								)
		END
		ELSE
		BEGIN
			SELECT @strSearch = 'vcItemName LIKE ''%' + @strItemSearch + '%'''
		END

		SET @strSQL = 'SELECT
							numItemCode, vcItemName, vcPathForTImage, vcCompanyName, MIN(monListPrice) monListPrice, txtItemDesc, 
							vcModelID, numBarCodeId, MIN(vcBarCode) vcBarCode, vcSKU, MIN(vcWHSKU) vcWHSKU, vcPartNo,
							bitSerialized, numItemGroup, Isarchieve, charItemType, 0 AS numWarehouseItemID, '''' vcAttributes
						FROM
						(
						SELECT
							Item.numItemCode,
							ISNULL(vcItemName,'''') AS vcItemName,
							ISNULL((SELECT TOP 1 vcPathForTImage from ItemImages WHERE numItemCode = Item.numItemCode),'''') as vcPathForTImage,
							ISNULL(vcCompanyName,'''') AS vcCompanyName,
							ISNULL(monListPrice,''0.00'') AS monListPrice,
							ISNULL(txtItemDesc,'''') AS txtItemDesc,
							ISNULL(vcModelID,'''') AS vcModelID,
							ISNULL(numBarCodeId,'''') AS numBarCodeId,
							ISNULL(vcBarCode,'''') AS vcBarCode,
							ISNULL(vcWHSKU,'''') AS vcWHSKU,
							ISNULL(vcSKU,'''') AS vcSKU,
							ISNULL(vcPartNo,'''') AS vcPartNo,
							ISNULL(bitSerialized,0) AS bitSerialized,
							ISNULL(numItemGroup,0) AS numItemGroup,
							ISNULL(Isarchieve,0) AS Isarchieve,
							ISNULL(charItemType,'''') AS charItemType
						FROM
							Item
						LEFT JOIN
							DivisionMaster 
						ON
							Item.numVendorID = DivisionMaster.numDivisionID
						LEFT JOIN
							CompanyInfo
						ON
							DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
						LEFT JOIN
							Vendor
						ON
							Item.numVendorID = Vendor.numVendorID AND
							Vendor.numItemCode = Item.numItemCode
						LEFT JOIN
							WareHouseItems
						ON
							Item.numItemCode = WareHouseItems.numItemID
						WHERE 
							Item.numDomainID = ' + CAST(@numDomainID AS VARCHAR) + ')V WHERE ' + @strSearch  + '
						GROUP BY
							numItemCode, vcItemName, vcPathForTImage, vcCompanyName, txtItemDesc, 
							vcModelID, numBarCodeId, vcSKU, bitSerialized, numItemGroup, Isarchieve, charItemType, vcPartNo'
	
		IF LEN(@strAttributeSearch) > 0
		BEGIN
			SET @strSQL = 'SELECT
								numItemCode, vcItemName, vcPathForTImage, vcCompanyName, TEMPAttributes.monWListPrice AS monListPrice, txtItemDesc, 
								vcModelID, numBarCodeId, TEMPAttributes.vcBarCode, vcSKU, TEMPAttributes.vcWHSKU, vcPartNo, TEMPAttributes.numWareHouseItemID,
								TEMPAttributes.vcAttributes
							FROM
								(' + @strSQL + ') TEMP
							OUTER APPLY
							(
								SELECT 
									WHI.numWareHouseItemID,
									WHI.vcBarCode,
									WHI.vcWHSKU,
									WHI.monWListPrice,
									STUFF(
												(SELECT 
												'', '' +  vcAttribute
												FROM  
												(
												SELECT
													CONCAT(	CFM.Fld_label, '' : '',
													(CASE 
														WHEN CFM.Fld_type = ''SelectBox''
														THEN 
															(select vcData from ListDetails where numListID= CFM.numlistid AND numListItemID=CFVSI.Fld_Value)
														ELSE
															CFVSI.Fld_Value
													END)) vcAttribute
												FROM
													CFW_Fld_Values_Serialized_Items CFVSI
												INNER JOIN
													CFW_Fld_Master CFM
												ON
													CFM.Fld_id = CFVSI.Fld_ID
												WHERE
													RecId = WHI.numWareHouseItemID
													AND bitSerialized = TEMP.bitSerialized
												) TEMP
												FOR XML PATH(''''))
											, 1
											, 1
											, ''''
											) AS vcAttributes
								FROM
									dbo.WareHouseItems WHI
								WHERE 
									WHI.numItemID = TEMP.numItemCode
									AND ISNULL(TEMP.numItemGroup,0) > 0
									AND ISNULL(TEMP.Isarchieve, 0) <> 1
									AND TEMP.charItemType NOT IN (''A'')
							) AS TEMPAttributes
							WHERE
								' + @strAttributeSearch
		END

		EXEC ('INSERT INTO #TempSearchedItem SELECT numItemCode, vcItemName, vcPathForTImage, vcCompanyName, monListPrice, txtItemDesc, vcModelID, numBarCodeId, vcBarCode, vcSKU, vcWHSKU, vcPartNo, numWarehouseItemID, vcAttributes FROM (' + @strSQL + ')TEMPFinal')

		IF @searchCriteria = '1' --Items
		BEGIN
			SET @strSQL = 'SELECT * ' + @vcCustomDisplayField + ' FROM #TempSearchedItem ORDER BY numItemCode OFFSET ' + CAST(@numSkip AS VARCHAR(100)) + ' ROWS FETCH NEXT 10 ROWS ONLY'
			EXEC (@strSQL)
			SELECT COUNT(*) AS TotalRows FROM #TempSearchedItem
		END
		ELSE IF @searchCriteria = '3' -- Opp/Orders
		BEGIN
			DECLARE @TEMPItemOrder TABLE
			(
				numOppId NUMERIC(18,0),
				bintCreatedDate DATETIME,
				vcPOppName VARCHAR(200),
				vcOppType VARCHAR(50),
				vcCompanyName  VARCHAR(200),
				tintType TINYINT
			)

			INSERT INTO 
				@TEMPItemOrder
			SELECT
				numOppId,
				bintCreatedDate,
				vcPOppName,
				vcOppType,
				vcCompanyName,
				tintType
			FROM
				(
					SELECT
						OpportunityMaster.numOppId,
						OpportunityMaster.vcPOppName,
						CASE 
							WHEN tintOppType = 1 THEN 'Sales ' + (CASE WHEN tintOppStatus = 1 THEN 'Order' ELSE 'Opportunity' END)
							WHEN tintOppType = 2 THEN 'Purchase ' + (CASE WHEN tintOppStatus = 1 THEN 'Order' ELSE 'Opportunity' END)
						END vcOppType,
						CompanyInfo.vcCompanyName,
						OpportunityMaster.bintCreatedDate,
						1 AS tintType
					FROM 
						OpportunityMaster
					INNER JOIN
						(
							SELECT
								numOppID
							FROM 
								OpportunityItems OI
							INNER JOIN
								#TempSearchedItem
							ON
								OI.numItemCode = #TempSearchedItem.numItemCode
								AND (OI.numWarehouseItmsID = #TempSearchedItem.numWarehouseItemID OR ISNULL(#TempSearchedItem.numWarehouseItemID,0) = 0)
							GROUP BY
								numOppID
						) TempOppItems
					ON
						OpportunityMaster.numOppId = TempOppItems.numOppId
					INNER JOIN
						DivisionMaster 
					ON
						OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
					INNER JOIN
						CompanyInfo
					ON
						DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
					WHERE
						OpportunityMaster.numDomainId =@numDomainID
						AND tintOppType <> 0
						AND 1 = (
								CASE @orderType
								 WHEN 0 THEN 1
								 WHEN 1 THEN (CASE WHEN (tintOppType = 1 AND tintOppStatus=1) THEN 1 ELSE 0 END)
								 WHEN 2 THEN (CASE WHEN (tintOppType = 2 AND tintOppStatus=1) THEN 1 ELSE 0 END)
								 WHEN 3 THEN (CASE WHEN (tintOppType = 1 AND ISNULL(tintOppStatus,0)=0) THEN 1 ELSE 0 END)
								 WHEN 4 THEN (CASE WHEN (tintOppType = 2 AND ISNULL(tintOppStatus,0)=0) THEN 1 ELSE 0 END)
								 WHEN 5 THEN 0
								 WHEN 6 THEN 0
								END
							)
					UNION
					SELECT
						ReturnHeader.numReturnHeaderID AS numOppID,
						ReturnHeader.vcRMA AS vcPOppName,
						CASE 
							WHEN tintReturnType = 1 THEN 'Sales Return'
							WHEN tintReturnType = 2 THEN 'Purchase Return'
						END vcOppType,
						CompanyInfo.vcCompanyName,
						ReturnHeader.dtCreatedDate AS bintCreatedDate,
						2 AS tintType
					FROM
						ReturnHeader
					INNER JOIN
						(
							SELECT
								numReturnHeaderID
							FROM 
								ReturnItems RI
							INNER JOIN
								#TempSearchedItem
							ON
								RI.numItemCode = #TempSearchedItem.numItemCode
								AND (RI.numWareHouseItemID = #TempSearchedItem.numWarehouseItemID OR ISNULL(#TempSearchedItem.numWarehouseItemID,0) = 0)
							GROUP BY
								numReturnHeaderID
						) TempReturnItems
					ON
						ReturnHeader.numReturnHeaderID = TempReturnItems.numReturnHeaderID
					INNER JOIN
						DivisionMaster 
					ON
						ReturnHeader.numDivisionId = DivisionMaster.numDivisionID
					INNER JOIN
						CompanyInfo
					ON
						DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
					WHERE
						ReturnHeader.numDomainId = @numDomainID 
						AND 1 = (
								CASE @orderType
								 WHEN 0 THEN (CASE WHEN tintReturnType = 1 OR tintReturnType = 2 THEN 1 ELSE 0 END)
								 WHEN 1 THEN 0
								 WHEN 2 THEN 0
								 WHEN 3 THEN 0
								 WHEN 4 THEN 0
								 WHEN 5 THEN (CASE WHEN tintReturnType = 1 THEN 1 ELSE 0 END)
								 WHEN 6 THEN (CASE WHEN tintReturnType = 2 THEN 1 ELSE 0 END)
								END
							)
				) TEMPFinal

			SELECT 
				ISNULL(numOppId,0) numOppId,
				ISNULL(dbo.FormatedDateFromDate(bintCreatedDate,@numDomainID),'') bintCreatedDate,
				ISNULL(vcPOppName,'') vcPOppName,
				ISNULL(vcOppType,'') vcOppType,
				ISNULL(vcCompanyName,'') vcCompanyName,
				ISNULL(tintType,0) tintType
			FROM
				@TEMPItemOrder
			ORDER BY
				bintCreatedDate DESC
			OFFSET @numSkip ROWS FETCH NEXT 10 ROWS ONLY	
				
			SELECT COUNT(*) AS TotalRows FROM @TEMPItemOrder	
		END
		ELSE IF @searchCriteria = '4' -- BizDocs
		BEGIN
			DECLARE @TEMPItemBizDoc TABLE
			(
				numOppBizDocsId NUMERIC(18,0),
				bintCreatedDate DATETIME,
				vcBizDocID VARCHAR(200),
				vcBizDocType VARCHAR(100),
				vcCompanyName  VARCHAR(200)
			)

			INSERT INTO
				@TEMPItemBizDoc
			SELECT 
				numOppBizDocsId,
				OpportunityBizDocs.dtCreatedDate,
				vcBizDocID,
				ListDetails.vcData AS vcBizDocType,
				CompanyInfo.vcCompanyName
			FROM 
				OpportunityBizDocs
			INNER JOIN
				(
					SELECT
						numOppBizDocID
					FROM 
						OpportunityBizDocItems OBI
					INNER JOIN
						#TempSearchedItem
					ON
						OBI.numItemCode = #TempSearchedItem.numItemCode
						AND (OBI.numWarehouseItmsID = #TempSearchedItem.numWarehouseItemID OR ISNULL(#TempSearchedItem.numWarehouseItemID,0) = 0)
					GROUP BY
						numOppBizDocID
				) TempOppBizDocItems
			ON
				OpportunityBizDocs.numOppBizDocsId = TempOppBizDocItems.numOppBizDocID
			INNER JOIN
				OpportunityMaster
			ON
				OpportunityBizDocs.numOppId = OpportunityMaster.numOppId
			INNER JOIN
				DivisionMaster
			ON
				OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
			INNER JOIN
				CompanyInfo
			ON
				DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
			INNER JOIN
				ListDetails
			ON
				ListDetails.numListID = 27
				AND OpportunityBizDocs.numBizDocId = ListDetails.numListItemID
			WHERE
				OpportunityMaster.numDomainId = @numDomainID
				AND (OpportunityBizDocs.numBizDocID = @bizDocType OR ISNULL(@bizDocType,0) = 0)

			SELECT
				ISNULL(numOppBizDocsId,0) numOppBizDocsId,
				ISNULL(dbo.FormatedDateFromDate(bintCreatedDate,@numDomainID),'') AS dtCreatedDate,
				ISNULL(vcBizDocID,'') vcBizDocID,
				ISNULL(vcBizDocType,'') vcBizDocType,
				ISNULL(vcCompanyName,'') vcCompanyName
			FROM
				@TEMPItemBizDoc
			ORDER BY
				bintCreatedDate DESC
			OFFSET @numSkip ROWS FETCH NEXT 10 ROWS ONLY
				
			SELECT COUNT(*) AS TotalRows FROM @TEMPItemBizDoc
		END

		
		DROP TABLE #TempItemSearchFields
		DROP TABLE #TempItemCustomDisplayFields
		DROP TABLE #TempSearchedItem
	END
	ELSE IF @searchType = '3' -- Opps/Orders
	BEGIN
		DECLARE @TEMPORDER TABLE
		(
			numOppId NUMERIC(18,0),
			bintCreatedDate DATETIME,
			vcPOppName VARCHAR(200),
			vcOppType VARCHAR(50),
			vcCompanyName  VARCHAR(200),
			tintType TINYINT
		)

		INSERT INTO 
			@TEMPORDER
		SELECT
			numOppId,
			bintCreatedDate,
			vcPOppName,
			vcOppType,
			vcCompanyName,
			tintType
		FROM
			(
				SELECT
					OpportunityMaster.numOppId,
					OpportunityMaster.vcPOppName,
					CASE 
						WHEN tintOppType = 1 THEN 'Sales ' + (CASE WHEN tintOppStatus = 1 THEN 'Order' ELSE 'Opportunity' END)
						WHEN tintOppType = 2 THEN 'Purchase ' + (CASE WHEN tintOppStatus = 1 THEN 'Order' ELSE 'Opportunity' END)
					END vcOppType,
					CompanyInfo.vcCompanyName,
					OpportunityMaster.bintCreatedDate,
					1 AS tintType
				FROM 
					OpportunityMaster
				INNER JOIN
					DivisionMaster 
				ON
					OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
				INNER JOIN
					CompanyInfo
				ON
					DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
				WHERE
					OpportunityMaster.numDomainId =@numDomainID
					AND tintOppType <> 0
					AND (vcPOppName LIKE N'%' + @searchText + '%' OR numOppId LIKE N'%' + @searchText + '%')
					AND 1 = (
							CASE @orderType
							 WHEN 0 THEN 1
							 WHEN 1 THEN (CASE WHEN (tintOppType = 1 AND tintOppStatus=1) THEN 1 ELSE 0 END)
							 WHEN 2 THEN (CASE WHEN (tintOppType = 2 AND tintOppStatus=1) THEN 1 ELSE 0 END)
							 WHEN 3 THEN (CASE WHEN (tintOppType = 1 AND ISNULL(tintOppStatus,0)=0) THEN 1 ELSE 0 END)
							 WHEN 4 THEN (CASE WHEN (tintOppType = 2 AND ISNULL(tintOppStatus,0)=0) THEN 1 ELSE 0 END)
							 WHEN 5 THEN 0
							 WHEN 6 THEN 0
							END
						)
				UNION
				SELECT
					ReturnHeader.numReturnHeaderID AS numOppID,
					ReturnHeader.vcRMA AS vcPOppName,
					CASE 
						WHEN tintReturnType = 1 THEN 'Sales Return'
						WHEN tintReturnType = 2 THEN 'Purchase Return'
					END vcOppType,
					CompanyInfo.vcCompanyName,
					ReturnHeader.dtCreatedDate AS bintCreatedDate,
					2 AS tintType
				FROM
					ReturnHeader
				INNER JOIN
					DivisionMaster 
				ON
					ReturnHeader.numDivisionId = DivisionMaster.numDivisionID
				INNER JOIN
					CompanyInfo
				ON
					DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
				WHERE
					ReturnHeader.numDomainId = @numDomainID 
					AND (vcRMA LIKE N'%' + @searchText + '%' OR ReturnHeader.numReturnHeaderID LIKE N'%' + @searchText + '%')
					AND 1 = (
							CASE @orderType
							 WHEN 0 THEN (CASE WHEN tintReturnType = 1 OR tintReturnType = 2 THEN 1 ELSE 0 END)
							 WHEN 1 THEN 0
							 WHEN 2 THEN 0
							 WHEN 3 THEN 0
							 WHEN 4 THEN 0
							 WHEN 5 THEN (CASE WHEN tintReturnType = 1 THEN 1 ELSE 0 END)
							 WHEN 6 THEN (CASE WHEN tintReturnType = 2 THEN 1 ELSE 0 END)
							END
						)
				) TEMPFinal

		SELECT 
			ISNULL(numOppId,0) numOppId,
			ISNULL(dbo.FormatedDateFromDate(bintCreatedDate,@numDomainID),'') bintCreatedDate,
			ISNULL(vcPOppName,'') vcPOppName,
			ISNULL(vcOppType,'') vcOppType,
			ISNULL(vcCompanyName,'') vcCompanyName,
			ISNULL(tintType,0) tintType
		FROM
			@TEMPORDER
		ORDER BY
			bintCreatedDate DESC
		OFFSET @numSkip ROWS FETCH NEXT 10 ROWS ONLY	
				
		SELECT COUNT(*) AS TotalRows FROM @TEMPORDER		
	END
	ELSE IF @searchType = '4' -- BizDocs
	BEGIN
		DECLARE @TEMPBizDoc TABLE
		(
			numOppBizDocsId NUMERIC(18,0),
			bintCreatedDate DATETIME,
			vcBizDocID VARCHAR(200),
			vcBizDocType VARCHAR(100),
			vcCompanyName  VARCHAR(200)
		)

		INSERT INTO
			@TEMPBizDoc
		SELECT 
			numOppBizDocsId,
			OpportunityBizDocs.dtCreatedDate,
			vcBizDocID,
			ListDetails.vcData AS vcBizDocType,
			CompanyInfo.vcCompanyName
		FROM 
			OpportunityBizDocs
		INNER JOIN
			OpportunityMaster
		ON
			OpportunityBizDocs.numOppId = OpportunityMaster.numOppId
		INNER JOIN
			DivisionMaster
		ON
			OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
		INNER JOIN
			CompanyInfo
		ON
			DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
		INNER JOIN
			ListDetails
		ON
			ListDetails.numListID = 27
			AND OpportunityBizDocs.numBizDocId = ListDetails.numListItemID
		WHERE
			OpportunityMaster.numDomainId = @numDomainID
			AND vcBizDocID LIKE N'%' + @searchText + '%'
			AND (OpportunityBizDocs.numBizDocID = @bizDocType OR ISNULL(@bizDocType,0) = 0)

		SELECT
			ISNULL(numOppBizDocsId,0) numOppBizDocsId,
			ISNULL(dbo.FormatedDateFromDate(bintCreatedDate,@numDomainID),'') AS dtCreatedDate,
			ISNULL(vcBizDocID,'') vcBizDocID,
			ISNULL(vcBizDocType,'') vcBizDocType,
			ISNULL(vcCompanyName,'') vcCompanyName
		FROM
			@TEMPBizDoc
		ORDER BY
			bintCreatedDate DESC
		OFFSET @numSkip ROWS FETCH NEXT 10 ROWS ONLY
				
		SELECT COUNT(*) AS TotalRows FROM @TEMPBizDoc
	END
	ELSE IF @searchType = '5' -- Contact
	BEGIN
		DECLARE @TableContact TABLE
		(
			numContactId NUMERIC(18,0),
			vcFirstName VARCHAR(100),
			vcLastname VARCHAR(100),
			vcFullName VARCHAR(200),
			vcEmail VARCHAR(100),
			numPhone VARCHAR(100),
			numPhoneExtension VARCHAR(100),
			vcCompanyName VARCHAR(200),
			bitPrimaryContact VARCHAR(5)
		)

		INSERT INTO
			@TableContact
		SELECT 
			ADC.numContactId,
			ISNULL(vcFirstName,'') AS vcFirstName,
			ISNULL(vcLastName,'') AS vcLastName,
			ISNULL(vcFirstName,'') + ' ' + ISNULL(vcLastName,'') AS vcFullName,
			ISNULL(vcEmail,'') AS vcEmail,
			ISNULL(ADC.numPhone,'') AS numPhone,
			ISNULL(ADC.numPhoneExtension,'') AS numPhoneExtension,
			ISNULL(CI.vcCompanyName,'') AS vcCompanyName,
			(CASE WHEN ISNULL(bitPrimaryContact,0) = 1 THEN 'Yes' ELSE 'No' END) AS bitPrimaryContact
		FROM 
			AdditionalContactsInformation ADC
		INNER JOIN
			DivisionMaster DM
		ON
			ADC.numDivisionId = DM.numDivisionID
		INNER JOIN
			CompanyInfo CI
		ON
			DM.numCompanyID = CI.numCompanyId
		WHERE 
			ADC.numDomainID = @numDomainID AND
			(vcFirstName LIKE '%' + @searchText + '%'
			OR vcLastName LIKE '%' + @searchText + '%'
			OR vcEmail LIKE '%' + @searchText + '%'
			OR numPhone LIKE '%' + @searchText + '%')

		SELECT 
			* 
		FROM 
			@TableContact	
		ORDER BY 
			vcCompanyName ASC, bitPrimaryContact DESC
		OFFSET 
			@numSkip 
		ROWS FETCH NEXT 
			10 
		ROWS ONLY

		SELECT COUNT(*) FROM @TableContact
	END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_UpdateBillingTerms')
	DROP PROCEDURE USP_UpdateBillingTerms
GO

CREATE PROCEDURE [dbo].[USP_UpdateBillingTerms]
(
	 @numDomainID  numeric=0,                                          
     @numDivisionID  numeric=0, 
	 @numCompanyID numeric=0,
	 @numCompanyCredit numeric=0,                                   
	 @vcDivisionName  varchar (100)='',
	 @numUserCntID  numeric=0,                                                                                                                                     
	 @tintCRMType  tinyint=0,                                          
	 @tintBillingTerms as tinyint,                                          
	 @numBillingDays as numeric(9),                                         
	 @tintInterestType as tinyint,                                          
	 @fltInterest as float,                          
	 @bitNoTax as BIT,
	 @numCurrencyID AS numeric(9)=0,
	 @numDefaultPaymentMethod AS numeric(9)=0,         
	 @numDefaultCreditCard AS numeric(9)=0,         
	 @bitOnCreditHold AS bit=0,
	 @vcShipperAccountNo VARCHAR(100) = '',
	 @intShippingCompany INT = 0,
	 @bitEmailToCase BIT=0,
	 @numDefaultExpenseAccountID NUMERIC(18,0) = 0,
	 @numDefaultShippingServiceID NUMERIC(18,0)    
    )
AS 
	BEGIN

		UPDATE DivisionMaster 
		SET numCompanyID = @numCompanyID,
			vcDivisionName = @vcDivisionName,                                      
			numModifiedBy = @numUserCntID,                                            
			bintModifiedDate = GETUTCDATE(),                                         
			tintCRMType = @tintCRMType,                                          
			tintBillingTerms = @tintBillingTerms,                                          
			numBillingDays = @numBillingDays,                                         
			tintInterestType = @tintInterestType,                                           
			fltInterest =@fltInterest,                
			bitNoTax=@bitNoTax,
			numCurrencyID=@numCurrencyID,
			numDefaultPaymentMethod=@numDefaultPaymentMethod,                                                  
			numDefaultCreditCard=@numDefaultCreditCard,                                                 
			bitOnCreditHold=@bitOnCreditHold,
			vcShippersAccountNo = @vcShipperAccountNo,
			intShippingCompany = @intShippingCompany,
			bitEmailToCase = @bitEmailToCase,
			numDefaultExpenseAccountID = @numDefaultExpenseAccountID,
			numDefaultShippingServiceID = @numDefaultShippingServiceID			                                                  
		WHERE numDivisionID = @numDivisionID AND numDomainId= @numDomainID     

		UPDATE CompanyInfo
		SET numCompanyCredit = @numCompanyCredit,               
			numModifiedBy = @numUserCntID,                
			bintModifiedDate = GETUTCDATE()               
		WHERE                 
			numCompanyId=@numCompanyId and numDomainID=@numDomainID    

	END
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_UpdateContactImage' ) 
    DROP PROCEDURE USP_UpdateContactImage
GO

CREATE PROCEDURE USP_UpdateContactImage  
(  
 @numDomainID NUMERIC,  
 @numContactID NUMERIC,  
 @vcImageName VARCHAR(200)   
)  
AS  
BEGIN  
	-- SET NOCOUNT ON added to prevent extra result sets from  
	-- interfering with SELECT statements.  
	SET NOCOUNT ON;  
  
	UPDATE 
		AdditionalContactsInformation 
	SET 
		vcImageName=@vcImageName 
	WHERE 
		numDomainID=@numDomainID 
		AND numContactId=@numContactID   
   
END  


/****** Object:  StoredProcedure [dbo].[USP_UpdatedomainDetails]    Script Date: 05/07/2009 21:56:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                                      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_updatedomaindetails')
DROP PROCEDURE usp_updatedomaindetails
GO
CREATE PROCEDURE [dbo].[USP_UpdatedomainDetails]                                      
@numDomainID as numeric(9)=0,                                      
@vcDomainName as varchar(50)='',                                      
@vcDomainDesc as varchar(100)='',                                      
@bitExchangeIntegration as bit,                                      
@bitAccessExchange as bit,                                      
@vcExchUserName as varchar(100)='',                                      
@vcExchPassword as varchar(100)='',                                      
@vcExchPath as varchar(200)='',                                      
@vcExchDomain as varchar(100)='',                                    
@tintCustomPagingRows as tinyint,                                    
@vcDateFormat as varchar(30)='',                                    
@numDefCountry as numeric(9),                                    
@tintComposeWindow as tinyint,                                    
@sintStartDate as smallint,                                    
@sintNoofDaysInterval as smallint,                                    
@tintAssignToCriteria as tinyint,                                    
@bitIntmedPage as bit,                                    
@tintFiscalStartMonth as tinyint,                            
--@bitDeferredIncome as bit,                               
@tintPayPeriod as tinyint,                        
@numCurrencyID as numeric(9) ,                
@charUnitSystem as char(1)='',              
@vcPortalLogo as varchar(100)='',            
@tintChrForComSearch as tinyint  ,            
@intPaymentGateWay as int,    
@tintChrForItemSearch as tinyint,
@ShipCompany as numeric(9),
@bitAutoPopulateAddress as bit=null,
@tintPoulateAddressTo as tinyint=null,
@bitMultiCurrency as bit,
--@bitMultiCompany as BIT,
@bitCreateInvoice AS tinyint = null,
@numDefaultSalesBizDocID AS NUMERIC(9)=NULL,
@numDefaultPurchaseBizDocID AS NUMERIC(9)=NULL,
@bitSaveCreditCardInfo AS BIT = NULL,
@intLastViewedRecord AS INT=10,
@tintCommissionType AS TINYINT,
@tintBaseTax AS TINYINT,
@bitEmbeddedCost AS BIT,
@numDefaultSalesOppBizDocId NUMERIC=NULL,
@tintSessionTimeOut TINYINT=1,
@tintDecimalPoints TINYINT,
@bitCustomizePortal BIT=0,
@tintBillToForPO TINYINT,
@tintShipToForPO TINYINT,
@tintOrganizationSearchCriteria TINYINT,
@bitDocumentRepositary BIT=0,
@tintComAppliesTo TINYINT=0,
@bitGtoBContact BIT=0,
@bitBtoGContact BIT=0,
@bitGtoBCalendar BIT=0,
@bitBtoGCalendar BIT=0,
@bitExpenseNonInventoryItem BIT=0,
@bitInlineEdit BIT=0,
@bitRemoveVendorPOValidation BIT=0,
@tintBaseTaxOnArea tinyint=0,
@bitAmountPastDue BIT=0,
@monAmountPastDue money,
@bitSearchOrderCustomerHistory BIT=0,
@bitRentalItem as bit=0,
@numRentalItemClass as NUMERIC(9)=NULL,
@numRentalHourlyUOM as NUMERIC(9)=NULL,
@numRentalDailyUOM as NUMERIC(9)=NULL,
@tintRentalPriceBasedOn as tinyint=NULL,
@tintCalendarTimeFormat as tinyint=NULL,
@tintDefaultClassType as tinyint=NULL,
@tintPriceBookDiscount as tinyint=0,
@bitAutoSerialNoAssign as bit=0,
@bitIsShowBalance AS BIT = 0,
@numIncomeAccID NUMERIC(18, 0) = 0,
@numCOGSAccID NUMERIC(18, 0) = 0,
@numAssetAccID NUMERIC(18, 0) = 0,
@IsEnableClassTracking AS BIT = 0,
@IsEnableProjectTracking AS BIT = 0,
@IsEnableCampaignTracking AS BIT = 0,
@IsEnableResourceScheduling AS BIT = 0,
@ShippingServiceItem  AS NUMERIC(9)=0,
@numSOBizDocStatus AS NUMERIC(9)=0,
@bitAutolinkUnappliedPayment AS BIT=0,
@numDiscountServiceItemID AS NUMERIC(9)=0,
@vcShipToPhoneNumber as VARCHAR(50)='',
@vcGAUserEmailId as VARCHAR(50)='',
@vcGAUserPassword as VARCHAR(50)='',
@vcGAUserProfileId as VARCHAR(50)='',
@bitDiscountOnUnitPrice AS BIT=0,
@intShippingImageWidth INT = 0,
@intShippingImageHeight INT = 0,
@numTotalInsuredValue NUMERIC(10,2) = 0,
@numTotalCustomsValue NUMERIC(10,2) = 0,
@vcHideTabs VARCHAR(1000) = '',
@bitUseBizdocAmount BIT = 0,
@IsEnableUserLevelClassTracking BIT = 0,
@bitDefaultRateType BIT = 0,
@bitIncludeTaxAndShippingInCommission  BIT = 0,
/*,@bitAllowPPVariance AS BIT=0*/
@bitPurchaseTaxCredit BIT=0,
@bitLandedCost BIT=0,
@vcLanedCostDefault VARCHAR(10)='',
@bitMinUnitPriceRule BIT = 0,
@numAbovePercent AS NUMERIC(18,2),
@numAbovePriceField AS NUMERIC(18,0),
@numBelowPercent AS NUMERIC(18,2),
@numBelowPriceField AS NUMERIC(18,0),
@numOrderStatusBeforeApproval AS NUMERIC(18,0),
@numOrderStatusAfterApproval AS NUMERIC(18,0),
@vcUnitPriceApprover AS VARCHAR(100),
@numDefaultSalesPricing AS TINYINT,
@bitReOrderPoint AS BIT = 0,
@numReOrderPointOrderStatus AS NUMERIC(18,0) = 0
as                                      
                                      
 update Domain                                       
   set                                       
   vcDomainName=@vcDomainName,                                      
   vcDomainDesc=@vcDomainDesc,                                      
   bitExchangeIntegration=@bitExchangeIntegration,                                      
   bitAccessExchange=@bitAccessExchange,                                      
   vcExchUserName=@vcExchUserName,                                      
   vcExchPassword=@vcExchPassword,                                      
   vcExchPath=@vcExchPath,                                      
   vcExchDomain=@vcExchDomain,                                    
   tintCustomPagingRows =@tintCustomPagingRows,                                    
   vcDateFormat=@vcDateFormat,                                    
   numDefCountry =@numDefCountry,                                    
   tintComposeWindow=@tintComposeWindow,                                    
   sintStartDate =@sintStartDate,                                    
   sintNoofDaysInterval=@sintNoofDaysInterval,                                    
   tintAssignToCriteria=@tintAssignToCriteria,                                    
   bitIntmedPage=@bitIntmedPage,                                    
   tintFiscalStartMonth=@tintFiscalStartMonth,                            
--   bitDeferredIncome=@bitDeferredIncome,                          
   tintPayPeriod=@tintPayPeriod,
   vcCurrency=isnull((select varCurrSymbol from Currency Where numCurrencyID=@numCurrencyID  ),'')  ,                      
  numCurrencyID=@numCurrencyID,                
  charUnitSystem= @charUnitSystem,              
  vcPortalLogo=@vcPortalLogo ,            
  tintChrForComSearch=@tintChrForComSearch  ,      
  intPaymentGateWay=@intPaymentGateWay,
 tintChrForItemSearch=@tintChrForItemSearch,
 numShipCompany= @ShipCompany,
 bitAutoPopulateAddress = @bitAutoPopulateAddress,
 tintPoulateAddressTo =@tintPoulateAddressTo,
 --bitMultiCompany=@bitMultiCompany ,
 bitMultiCurrency=@bitMultiCurrency,
 bitCreateInvoice= @bitCreateInvoice,
 [numDefaultSalesBizDocId] =@numDefaultSalesBizDocId,
 bitSaveCreditCardInfo=@bitSaveCreditCardInfo,
 intLastViewedRecord=@intLastViewedRecord,
 [tintCommissionType]=@tintCommissionType,
 [tintBaseTax]=@tintBaseTax,
 [numDefaultPurchaseBizDocId]=@numDefaultPurchaseBizDocID,
 bitEmbeddedCost=@bitEmbeddedCost,
 numDefaultSalesOppBizDocId=@numDefaultSalesOppBizDocId,
 tintSessionTimeOut=@tintSessionTimeOut,
 tintDecimalPoints=@tintDecimalPoints,
 bitCustomizePortal=@bitCustomizePortal,
 tintBillToForPO = @tintBillToForPO,
 tintShipToForPO = @tintShipToForPO,
 tintOrganizationSearchCriteria=@tintOrganizationSearchCriteria,
-- vcPortalName=@vcPortalName,
 bitDocumentRepositary=@bitDocumentRepositary,
 --tintComAppliesTo=@tintComAppliesTo,THIS FIELD IS REMOVED FROM GLOBAL SETTINGS
 bitGtoBContact=@bitGtoBContact,
 bitBtoGContact=@bitBtoGContact,
 bitGtoBCalendar=@bitGtoBCalendar,
 bitBtoGCalendar=@bitBtoGCalendar,
 bitExpenseNonInventoryItem=@bitExpenseNonInventoryItem,
 bitInlineEdit=@bitInlineEdit,
 bitRemoveVendorPOValidation=@bitRemoveVendorPOValidation,
tintBaseTaxOnArea=@tintBaseTaxOnArea,
bitAmountPastDue = @bitAmountPastDue,
monAmountPastDue = @monAmountPastDue,
bitSearchOrderCustomerHistory=@bitSearchOrderCustomerHistory,
bitRentalItem=@bitRentalItem,
numRentalItemClass=@numRentalItemClass,
numRentalHourlyUOM=@numRentalHourlyUOM,
numRentalDailyUOM=@numRentalDailyUOM,
tintRentalPriceBasedOn=@tintRentalPriceBasedOn,
tintCalendarTimeFormat=@tintCalendarTimeFormat,
tintDefaultClassType=@tintDefaultClassType,
tintPriceBookDiscount=@tintPriceBookDiscount,
bitAutoSerialNoAssign=@bitAutoSerialNoAssign,
bitIsShowBalance = @bitIsShowBalance,
numIncomeAccID = @numIncomeAccID,
numCOGSAccID = @numCOGSAccID,
numAssetAccID = @numAssetAccID,
IsEnableClassTracking = @IsEnableClassTracking,
IsEnableProjectTracking = @IsEnableProjectTracking,
IsEnableCampaignTracking = @IsEnableCampaignTracking,
IsEnableResourceScheduling = @IsEnableResourceScheduling,
numShippingServiceItemID = @ShippingServiceItem,
numSOBizDocStatus=@numSOBizDocStatus,
bitAutolinkUnappliedPayment=@bitAutolinkUnappliedPayment,
numDiscountServiceItemID=@numDiscountServiceItemID,
vcShipToPhoneNo = @vcShipToPhoneNumber,
vcGAUserEMail = @vcGAUserEmailId,
vcGAUserPassword = @vcGAUserPassword,
vcGAUserProfileId = @vcGAUserProfileId,
bitDiscountOnUnitPrice=@bitDiscountOnUnitPrice,
intShippingImageWidth = @intShippingImageWidth ,
intShippingImageHeight = @intShippingImageHeight,
numTotalInsuredValue = @numTotalInsuredValue,
numTotalCustomsValue = @numTotalCustomsValue,
vcHideTabs = @vcHideTabs,
bitUseBizdocAmount = @bitUseBizdocAmount,
IsEnableUserLevelClassTracking = @IsEnableUserLevelClassTracking,
bitDefaultRateType = @bitDefaultRateType,
bitIncludeTaxAndShippingInCommission = @bitIncludeTaxAndShippingInCommission,
/*,bitAllowPPVariance=@bitAllowPPVariance*/
bitPurchaseTaxCredit=@bitPurchaseTaxCredit,
bitLandedCost=@bitLandedCost,
vcLanedCostDefault=@vcLanedCostDefault,
bitMinUnitPriceRule = @bitMinUnitPriceRule,
numAbovePercent = @numAbovePercent,
numAbovePriceField = @numAbovePriceField,
numBelowPercent = @numBelowPercent,
numBelowPriceField = @numBelowPriceField,
numOrderStatusBeforeApproval = @numOrderStatusBeforeApproval,
numOrderStatusAfterApproval = @numOrderStatusAfterApproval,
numDefaultSalesPricing = @numDefaultSalesPricing,
bitReOrderPoint=@bitReOrderPoint,
numReOrderPointOrderStatus=@numReOrderPointOrderStatus
 where numDomainId=@numDomainID

DELETE FROM UnitPriceApprover WHERE numDomainID = @numDomainID

INSERT INTO UnitPriceApprover
	(
		numDomainID,
		numUserID
	)
SELECT
     @numDomainID,
	 Split.a.value('.', 'VARCHAR(100)') AS String
FROM  
(
SELECT 
        CAST ('<M>' + REPLACE(@vcUnitPriceApprover, ',', '</M><M>') + '</M>' AS XML) AS String  
) AS A 
CROSS APPLY 
	String.nodes ('/M') AS Split(a); 

GO
/****** Object:  StoredProcedure [dbo].[USP_UpdateOppLinkingDTls]    Script Date: 05/07/2009 22:12:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_updateopplinkingdtls')
DROP PROCEDURE usp_updateopplinkingdtls
GO
CREATE PROCEDURE [dbo].[USP_UpdateOppLinkingDTls]        
@numOppID as numeric(9)=0,            
@tintOppType as tinyint,        
@tintBillToType as tinyint,        
@tintShipToType as tinyint,        
@vcBillStreet as varchar(50),                  
@vcBillCity as varchar(50),                  
@numBillState as numeric(9),                  
@vcBillPostCode as varchar(15),                  
@numBillCountry as numeric(9),                          
@vcShipStreet as varchar(50),                  
@vcShipCity as varchar(50),                  
@numShipState as numeric(9),                  
@vcShipPostCode as varchar(15),                  
@numShipCountry as numeric(9),    
@vcBillCompanyName as varchar(100),    
@vcShipCompanyName as varchar(100),
@numParentOppID as numeric(9),
@numParentOppBizDocID AS NUMERIC(9),
@numBillToAddressID AS NUMERIC(18,0) = 0,
@numShipToAddressID AS NUMERIC(18,0) = 0
AS        
                    
UPDATE 
	OpportunityMaster 
SET 
	tintBillToType=@tintBillToType,
	numBillToAddressID = @numBillToAddressID,
	tintShipToType=@tintShipToType,
	numShipToAddressID = @numShipToAddressID
WHERE 
	numOppID=@numOppID        
        
IF @tintBillToType = 2 OR @tintShipToType = 2 
BEGIN
    IF (SELECT COUNT(*) FROM OpportunityAddress WHERE [numOppID]=@numOppID) = 0
	BEGIN
		INSERT  INTO OpportunityAddress ( numOppID )
		VALUES  ( @numOppID )      
	END
END

IF @tintBillToType = 2 
BEGIN
    UPDATE  OpportunityAddress
    SET     vcBillCompanyName = @vcBillCompanyName,
            vcBillStreet = @vcBillStreet,
            vcBillCity = @vcBillCity,
            numBillState = @numBillState,
            vcBillPostCode = @vcBillPostCode,
            numBillCountry = @numBillCountry
    WHERE   numOppID = @numOppID
	
END
    
IF @tintShipToType = 2 
BEGIN
PRINT 'hi'
    UPDATE  OpportunityAddress
    SET     vcShipCompanyName = @vcShipCompanyName,
            vcShipStreet = @vcShipStreet,
            vcShipCity = @vcShipCity,
            numShipState = @numShipState,
            vcShipPostCode = @vcShipPostCode,
            numShipCountry = @numShipCountry
    WHERE   numOppID = @numOppID

END


if @numParentOppID>0 
BEGIN
IF @numParentOppBizDocID = 0 
SET @numParentOppBizDocID = NULL
insert into OpportunityLinking (numParentOppID,numChildOppID,numParentOppBizDocID) values(@numParentOppID,@numOppID,@numParentOppBizDocID)
end



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_UpdatingInventoryForEmbeddedKits')
DROP PROCEDURE USP_UpdatingInventoryForEmbeddedKits
GO
CREATE PROCEDURE [dbo].[USP_UpdatingInventoryForEmbeddedKits]    
	@numOppItemID as numeric(9),
	@Units as integer,
	@Mode as TINYINT,
	@numOppID AS NUMERIC(9),
	@tintMode AS TINYINT=0, -- 0:Add/Edit 1:Delete
	@numUserCntID AS NUMERIC(9)
as                            


--WITH CTE(numItemKitID,numItemCode,numWareHouseItemId,numQtyItemsReq,numOppChildItemID,
--charItemType,StageLevel,numBaseUnit)
--AS
--(
--select convert(NUMERIC(18,0),0),numItemCode,isnull(Dtl.numWareHouseItemId,0) as numWareHouseItemId
--,convert(NUMERIC(18,0),DTL.numQtyItemsReq * @Units) AS numQtyItemsReq,0 as numOppChildItemID,charItemType,1,ISNULL(numBaseUnit,0)
--from item                                
--INNER join ItemDetails Dtl on numChildItemID=numItemCode
--where  numItemKitID=@numKitId
--
--UNION ALL
--
--select Dtl.numItemKitID,i.numItemCode,isnull(Dtl.numWareHouseItemId,0) as numWareHouseItemId,
--convert(NUMERIC(18,0),DTL.numQtyItemsReq * c.numQtyItemsReq) AS numQtyItemsReq,0 as numOppChildItemID,i.charItemType,c.StageLevel + 1,ISNULL(i.numBaseUnit,0)
--from item i                               
--INNER JOIN ItemDetails Dtl on Dtl.numChildItemID=i.numItemCode
--INNER JOIN CTE c ON Dtl.numItemKitID = c.numItemCode
--where Dtl.numChildItemID!=@numKitId
--)
--
--SELECT * ,ROW_NUMBER() OVER(ORDER BY StageLevel) AS RowNumber INTO #tempKits FROM CTE c where charitemtype='P' and numWareHouseItemId>0 and numWareHouseItemId is not null 
DECLARE @numDomain AS NUMERIC(18,0)
DECLARE @bitReOrderPoint BIT = 0
DECLARE @tintOppType AS TINYINT = 0

SELECT @numDomain = numDomainID, @tintOppType=tintOppType FROM [dbo].[OpportunityMaster] AS OM WHERE [OM].[numOppId] = @numOppID
SELECT @bitReOrderPoint=ISNULL(bitReOrderPoint,0) FROM Domain WHERE numDomainId = @numDomain

SELECT I.numItemCode,numWareHouseItemId,numQtyItemsReq,numQtyShipped,ROW_NUMBER() OVER(ORDER BY numOppChildItemID) AS RowNumber INTO #tempKits
from OpportunityKitItems OKI join Item I on OKI.numChildItemID=I.numItemCode    
	where charitemtype='P' and numWareHouseItemId>0 and numWareHouseItemId is not null 
			AND OKI.numOppID=@numOppID AND OKI.numOppItemID=@numOppItemID 
  
  --SELECT * FROM #tempKits
  
DECLARE @minRowNumber NUMERIC(9),@maxRowNumber NUMERIC(9),@numWareHouseItemID NUMERIC(9),@numItemCode NUMERIC(18,0)
DECLARE @onHand as numeric(9),@onAllocation as numeric(9),@onOrder as numeric(9),@onBackOrder as numeric(9),@onReOrder AS NUMERIC(18,0)
DECLARE @numUnits as INTEGER,@QtyShipped AS NUMERIC(9)

SELECT  @minRowNumber = MIN(RowNumber),@maxRowNumber = MAX(RowNumber) FROM #tempKits

WHILE  @minRowNumber <= @maxRowNumber
    BEGIN
    
    SELECT @numItemCode=numItemCode,@numWareHouseItemID=numWareHouseItemID,@numUnits=numQtyItemsReq,@QtyShipped=numQtyShipped FROM #tempKits WHERE RowNumber=@minRowNumber
    
    DECLARE @description AS VARCHAR(100)
	IF @Mode=0
			SET @description='SO KIT insert/edit (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'
	ELSE IF @Mode=1
			SET @description='SO KIT Deleted (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'
	ELSE IF @Mode=2
			SET @description='SO KIT Close (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'
	ELSE IF @Mode=3
			SET @description='SO KIT Re-Open (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'
			
    	SELECT @onHand=isnull(numOnHand,0),@onAllocation=isnull(numAllocation,0),                                    
				   @onOrder=isnull(numOnOrder,0),@onBackOrder=isnull(numBackOrder,0),
				   @onReOrder = ISNULL(numReorder,0)                                     
					from WareHouseItems where numWareHouseItemID=@numWareHouseItemID 
					
    	IF @Mode=0 --insert/edit
    	BEGIN
    		SET @numUnits = @numUnits - @QtyShipped
    	
		    IF @onHand>=@numUnits                                    
			BEGIN                                    
				SET @onHand=@onHand-@numUnits                            
				SET @onAllocation=@onAllocation+@numUnits                                    
			END                                    
			ELSE IF @onHand<@numUnits                                    
			BEGIN                                    
				SET @onAllocation=@onAllocation+@onHand                                    
				SET @onBackOrder=@onBackOrder+@numUnits-@onHand                                    
				SET @onHand=0                                    
			END  
			
			-- IF REORDER POINT IS ENABLED FROM GLOABAL SETTING THEN CREATE PURCHASE ORDER
			If @tintOppType = 1 AND (@onHand + @onOrder <= @onReOrder) AND @bitReOrderPoint = 1
			BEGIN
				BEGIN TRY
					DECLARE @numNewOppID AS NUMERIC(18,0) = 0
					EXEC USP_OpportunityMaster_CreatePO @numNewOppID OUTPUT,@numDomain,@numUserCntID,@numItemCode,@onBackOrder,@numWareHouseItemID,0 
				END TRY
				BEGIN CATCH
					--WE ARE NOT TROWING ERROR
				END CATCH
			END                       
    	END
        
        ELSE IF @Mode=1 --Revert
			BEGIN
					IF @QtyShipped>0
						BEGIN
							IF @tintMode=0
								SET @numUnits = @numUnits - @QtyShipped
							ELSE IF @tintmode=1
								SET @onAllocation = @onAllocation + @QtyShipped 
						END 
								                    
                    IF @numUnits >= @onBackOrder 
                        BEGIN
                            SET @numUnits = @numUnits - @onBackOrder
                            SET @onBackOrder = 0
                            
                            IF (@onAllocation - @numUnits >= 0)
								SET @onAllocation = @onAllocation - @numUnits
                            SET @onHand = @onHand + @numUnits                                            
                        END                                            
                    ELSE 
                        IF @numUnits < @onBackOrder 
                            BEGIN                  
								IF (@onBackOrder - @numUnits >0)
									SET @onBackOrder = @onBackOrder - @numUnits
									
--								IF @tintmode=1
--									SET @onAllocation = @onAllocation + (@numUnits - @numQtyShipped)
                            END 
			 END
       
		ELSE IF @Mode=2 --Close
			BEGIN
				  SET @numUnits = @numUnits - @QtyShipped
				  
                  SET @onAllocation = @onAllocation - @numUnits            
			 END
		ELSE IF @Mode=3 --Re-Open
			BEGIN
				  SET @numUnits = @numUnits - @QtyShipped
			
                  SET @onAllocation = @onAllocation + @numUnits            
			 END

       update WareHouseItems set numOnHand=@onHand,numAllocation=@onAllocation,                                    
					numBackOrder=@onBackOrder,dtModified = GETDATE()  where numWareHouseItemID=@numWareHouseItemID 
	

		
		EXEC dbo.USP_ManageWareHouseItems_Tracking
	@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
	@numReferenceID = @numOppID, --  numeric(9, 0)
	@tintRefType = 3, --  tinyint
	@vcDescription = @description, --  varchar(100)
	@numModifiedBy = @numUserCntID,
	@numDomainID = @numDomain
	
	SET @minRowNumber=@minRowNumber + 1			 		
     --SELECT  @minRowNumber = MIN(RowNumber) FROM #tempKits WHERE  [RowNumber] > @minRowNumber
    END	
  
  
  DROP TABLE #tempKits

GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON


GO
-- USP_GetItemsForInventoryAdjustment 1 , 58 , 0 , ''
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetItemsForInventoryAdjustment')
DROP PROCEDURE USP_GetItemsForInventoryAdjustment
GO
CREATE PROCEDURE [dbo].[USP_GetItemsForInventoryAdjustment]
@numDomainID numeric(18,0),
@numWarehouseID numeric(18,0),
@numItemGroupID NUMERIC = 0,
@KeyWord AS VARCHAR(1000) = '',
@numCurrentPage as numeric(9)=0,
@PageSize AS INT=0,
@numTotalPage as numeric(9) OUT,    
@SortChar CHAR(1) = '0',
@numUserCntID NUMERIC(18,0)

as

declare @strsql as varchar(8000)
declare @strRowCount as varchar(8000)
    /*paging logic*/
    DECLARE  @firstRec  AS INTEGER
    DECLARE  @lastRec  AS INTEGER
    SET @firstRec = (@numCurrentPage
                       - 1)
                      * @PageSize
    SET @lastRec = (@numCurrentPage
                      * @PageSize
                      + 1);

DECLARE @strColumns VARCHAR(MAX) = ''
DECLARE @strJoin VARCHAR(MAX) = ''
DECLARE @numFormID NUMERIC(18,0) = 126

DECLARE @TempForm TABLE  
(
	ID INT IDENTITY(1,1),tintOrder TINYINT,vcDbColumnName nvarchar(50),vcOrigDbColumnName nvarchar(50),vcFieldName nvarchar(50),
	vcAssociatedControlType nvarchar(50), vcListItemType varchar(3),numListID numeric(9),vcLookBackTableName varchar(50),
	bitCustomField BIT,numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,
	bitIsAlphaNumeric BIT,bitIsNumeric BIT,bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,
	vcFieldMessage VARCHAR(500),ListRelID numeric(9),intColumnWidth int,bitAllowFiltering BIT,vcFieldDataType CHAR(1)
)


DECLARE @Nocolumns AS TINYINT;
SET @Nocolumns = 0

SELECT 
	@Nocolumns=ISNULL(SUM(TotalRow),0) 
FROM
	(            
		SELECT	
			COUNT(*) TotalRow 
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
		UNION 
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicCustomColumns 
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
	) TotalRows

IF @Nocolumns=0
BEGIN
	INSERT INTO DycFormConfigurationDetails 
	(
		numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,tintPageType,bitCustom,intColumnWidth
	)
	SELECT 
		@numFormId,numFieldId,0,Row_number() over(order by tintRow desc),@numDomainID,@numUserCntID,1,0,intColumnWidth
	FROM 
		View_DynamicDefaultColumns
	WHERE 
		numFormId=@numFormId AND bitDefault=1 AND ISNULL(bitSettingField,0)=1 AND numDomainID=@numDomainID
	ORDER BY 
		tintOrder ASC 
END


INSERT INTO @TempForm
SELECT 
	tintRow+1 as tintOrder,	vcDbColumnName,	vcOrigDbColumnName,	ISNULL(vcCultureFieldName,vcFieldName), vcAssociatedControlType,
	vcListItemType,numListID,vcLookBackTableName,bitCustom,numFieldId,bitAllowSorting,bitInlineEdit, bitIsRequired,bitIsEmail,
	bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,
	ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType  
FROM 
	View_DynamicColumns 
WHERE 
	numFormId=@numFormId 
	AND numUserCntID=@numUserCntID 
	AND numDomainID=@numDomainID 
	AND tintPageType=1 
	AND ISNULL(bitSettingField,0)=1 
	AND ISNULL(bitCustom,0)=0  
UNION
    SELECT 
		tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName, vcAssociatedControlType,
		'' as vcListItemType,numListID,'',bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,bitIsRequired,bitIsEmail,
		bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,
		ListRelID,intColumnWidth,0,'' 
 from View_DynamicCustomColumns
 where numFormId=@numFormId and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 
 AND ISNULL(bitCustom,0)=1  

DECLARE @bitCustom AS BIT
DECLARE @tintOrder AS TINYINT
DECLARE @numFieldId AS NUMERIC(18,0)
DECLARE @vcFieldName AS VARCHAR(200)
DECLARE @vcAssociatedControlType AS VARCHAR(200)
DECLARE @vcDbColumnName AS VARCHAR(200)

DECLARE @i AS INT = 1
DECLARE @COUNT AS INT = 0
SELECT @COUNT=COUNT(*) FROM @TempForm

WHILE @i <= @COUNT
BEGIN
	SELECT  
		@bitCustom = bitCustomField,
		@tintOrder = tintOrder,
		@vcFieldName = vcFieldName,
		@numFieldId=numFieldId,
        @vcAssociatedControlType = vcAssociatedControlType,
        @vcDbColumnName = vcDbColumnName
    FROM    
		@TempForm
    WHERE   
		ID = @i        

	IF @bitCustom = 1 
    BEGIN      
        IF @vcAssociatedControlType = 'TextBox' OR @vcAssociatedControlType = 'TextArea' 
        BEGIN                    
			SET @strColumns = @strColumns + ',CFW'  + CONVERT(VARCHAR(3), @tintOrder)  + '.Fld_Value  [' + @vcDbColumnName + ']'                   
            SET @strJoin = @strJoin + ' LEFT JOIN CFW_FLD_Values_Item CFW' + CONVERT(VARCHAR(3), @tintOrder) + ' ON CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
                                + CONVERT(VARCHAR(10), @numFieldId)
                                + 'and CFW' + CONVERT(VARCHAR(3), @tintOrder)
                                + '.RecId=I.numItemCode '                                                         
        END   
        ELSE IF @vcAssociatedControlType = 'CheckBox' 
        BEGIN            
            SET @strColumns = @strColumns + ',CASE WHEN ISNULL(CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Value,0)=0 then ''No'' when isnull(CFW' + CONVERT(VARCHAR(3), @tintOrder)
                                    + '.Fld_Value,0)=1 then ''Yes'' end   ['
                                    + @vcDbColumnName
                                    + ']'              
            SET @strJoin = @strJoin + ' LEFT JOIN CFW_FLD_Values_Item CFW' + CONVERT(VARCHAR(3), @tintOrder) + ' ON CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
                                    + CONVERT(VARCHAR(10), @numFieldId)
                                    + 'and CFW'
                                    + CONVERT(VARCHAR(3), @tintOrder)
                                    + '.RecId=I.numItemCode   '                                                     
        END                
        ELSE IF @vcAssociatedControlType = 'DateField' 
        BEGIN              
            SET @strColumns = @strColumns + ',dbo.FormatedDateFromDate(CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Value,' + CONVERT(VARCHAR(10), @numDomainId) 
									+ ')  [' + @vcDbColumnName + ']'                   
            SET @strJoin = @strJoin + ' LEFT JOIN CFW_FLD_Values_Item CFW' + CONVERT(VARCHAR(3), @tintOrder) + ' ON CFW' + CONVERT(VARCHAR(3), @tintOrder) 
										+ '.Fld_Id='
                                        + CONVERT(VARCHAR(10), @numFieldId)
                                        + 'and CFW'
                                        + CONVERT(VARCHAR(3), @tintOrder)
                                        + '.RecId=I.numItemCode   '                                                         
        END                
        ELSE IF @vcAssociatedControlType = 'SelectBox' 
        BEGIN                
            SET @vcDbColumnName = 'Cust' + CONVERT(VARCHAR(10), @numFieldId)                
            SET @strColumns = @strColumns + ',L' + CONVERT(VARCHAR(3), @tintOrder) + '.vcData' + ' [' + @vcDbColumnName + ']'                                                          
            SET @strJoin = @strJoin + ' LEFT JOIN CFW_FLD_Values_Item CFW' + CONVERT(VARCHAR(3), @tintOrder) + ' ON CFW' + CONVERT(VARCHAR(3), @tintOrder) 
									+ '.Fld_Id=' + CONVERT(VARCHAR(10), @numFieldId) + ' and CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.RecId=I.numItemCode    '                                                         
            SET @strJoin = @strJoin + ' LEFT JOIN ListDetails L'+ CONVERT(VARCHAR(3), @tintOrder) + ' on L' + CONVERT(VARCHAR(3), @tintOrder)
                                            + '.numListItemID=CFW'
                                            + CONVERT(VARCHAR(3), @tintOrder)
                                            + '.Fld_Value'                
        END                 
    END       


	SET @i = @i + 1
END


SET @strsql = ''
SET @strRowCount = ''

SET @strsql = @strsql +
'WITH WarehouseItems AS (
	SELECT 
		ROW_NUMBER() OVER(ORDER by WI.numWareHouseItemID) AS RowNumber,
		WI.numWareHouseItemID,
		W.vcWareHouse,
		WI.numOnHand,
		I.monAverageCost,
		(ISNULL(WI.numOnHand,0) * ISNULL(I.monAverageCost,0)) AS monCurrentValue,
		CASE WHEN I.bitSerialized =1 THEN 1 WHEN I.bitLotNo =1 THEN 1 ELSE 0 END AS IsLotSerializedItem,
		I.numAssetChartAcntId,
		CASE 
			WHEN '+convert(varchar(20), @numItemGroupID)+' > 0 
			THEN dbo.fn_GetAttributes(WI.numWareHouseItemID,I.bitSerialized) + CONVERT(VARCHAR(10),I.numItemGroup)
			ELSE ''''
		END AS vcAttribute,
		isnull(I.bitSerialized,0) as bitSerialized,
		isnull(I.bitLotNo,0) as bitLotNo,
		isnull(WL.vcLocation,'''') Location,
		I.numItemcode,
		I.vcItemName,
		ISNULL(CompanyInfo.vcCompanyName,'''') AS vcCompanyName,
		ISNULL(I.monListPrice,0) AS monListPrice,
		ISNULL(I.txtItemDesc,'''') AS txtItemDesc,
		ISNULL(I.vcModelID,'''') AS vcModelID,
		ISNULL(I.numBarCodeId,'''') AS numBarCodeId,
		ISNULL(I.vcManufacturer,'''') AS vcManufacturer,
		ISNULL(I.fltHeight,'''') AS fltHeight,
		ISNULL(I.fltLength,'''') AS fltLength,
		ISNULL(I.fltWeight,'''') AS fltWeight,
		ISNULL(I.fltWidth,'''') AS fltWidth,
		ISNULL(ItemGroups.vcItemGroup,'''') AS numItemGroup,
		ISNULL((SELECT vcData FROM ListDetails WHERE numListID = 175 AND numListItemID = I.numPurchaseUnit) ,'''') AS numPurchaseUnit,
		ISNULL((SELECT vcData FROM ListDetails WHERE numListID = 175 AND numListItemID = I.numSaleUnit) ,'''') AS numSaleUnit,
		ISNULL((SELECT vcData FROM ListDetails WHERE numListID = 175 AND numListItemID = I.numBaseUnit) ,'''') AS numBaseUnit,
		ISNULL(I.vcSKU,'''') AS vcSKU,
		ISNULL(TEMPVendor.monCost,0) AS monCost,
		ISNULL(TEMPVendor.intMinQty,0) AS intMinQty,
		ISNULL((SELECT vcData FROM ListDetails WHERE numListID = 461 AND numListItemID = I.numItemClass) ,'''') AS numShipClass'
		+
		ISNULL(@strColumns,'')
		+ ' FROM 
		dbo.WareHouseItems WI 
		INNER JOIN dbo.Warehouses W ON WI.numDomainID = W.numDomainID AND WI.numWareHouseID = W.numWareHouseID
		LEFT JOIN WarehouseLocation WL on WL.numWLocationID =WI.numWLocationID
		INNER JOIN dbo.Item I ON I.numItemCode = WI.numItemID 
		OUTER APPLY
		(
			SELECT
				numVendorID,
				monCost,
				intMinQty
			FROM
				Vendor
			WHERE
				Vendor.numItemCode = I.numItemCode AND Vendor.numVendorID = I.numVendorID
		) AS TEMPVendor
		LEFT JOIN DivisionMaster ON TEMPVendor.numVendorID = DivisionMaster.numDivisionID
		LEFT JOIN CompanyInfo ON DivisionMaster.numCompanyID  = CompanyInfo.numCompanyId
		LEFT JOIN ItemGroups ON I.numItemGroup = ItemGroups.numItemGroupID '
		+ 
		ISNULL(@strJoin,'')
		+ ' WHERE 
		I.charItemType = ''P'' AND 
		ISNULL(bitAssembly,0)=0 AND 
		I.numDomainID = '+ convert(varchar(20), @numDomainID) + 
		' AND  WI.numDomainID=' + convert(varchar(20), @numDomainID) +
		' AND WI.numWareHouseID='+ CONVERT(VARCHAR(20),@numWarehouseID) +
		' AND (ISNULL(I.numItemGroup,0) ='+CONVERT(VARCHAR(20), @numItemGroupID)+')'

SET @strRowCount = @strRowCount + 
' SELECT 
COUNT(*) AS TotalRowCount
FROM dbo.WareHouseItems WI INNER JOIN dbo.Warehouses W ON WI.numDomainID = W.numDomainID AND WI.numWareHouseID = W.numWareHouseID
left join WarehouseLocation WL on WL.numWLocationID =WI.numWLocationID
INNER JOIN dbo.Item I ON I.numItemCode = WI.numItemID
WHERE 
I.charItemType = ''P'' and ISNULL(bitAssembly,0)=0  AND I.numDomainID = '+ convert(varchar(20), @numDomainID)  +' AND  WI.numDomainID=' + convert(varchar(20), @numDomainID)+'  AND WI.numWareHouseID='+ CONVERT(VARCHAR(20),@numWarehouseID) +
' AND (ISNULL(I.numItemGroup,0) ='+CONVERT(VARCHAR(20), @numItemGroupID)+') '


 IF @KeyWord <> '' 
        BEGIN 
            IF CHARINDEX('vcCompanyName', @KeyWord) > 0 
                BEGIN
                    SET @strSql = @strSql
                        + ' and I.numItemCode in (select Vendor.numItemCode
								from Vendor join
divisionMaster div on div.numdivisionid=Vendor.numVendorid        
join companyInfo com  on com.numCompanyID=div.numCompanyID  WHERE Vendor.numDomainID='
                        + CONVERT(VARCHAR(15), @numDomainID)
                        + '  
									and Vendor.numItemCode= I.numItemCode '
                        + @KeyWord + ')'
                         
					SET @strRowCount =  @strRowCount  +	 ' and I.numItemCode in (select Vendor.numItemCode
								from Vendor join
divisionMaster div on div.numdivisionid=Vendor.numVendorid        
join companyInfo com  on com.numCompanyID=div.numCompanyID  WHERE Vendor.numDomainID='
                        + CONVERT(VARCHAR(15), @numDomainID)
                        + '  
									and Vendor.numItemCode= I.numItemCode '
                        + @KeyWord + ')' 		
                END
            ELSE 
            BEGIN
            	SET @strSql = @strSql + ' and  1 = 1 ' + @KeyWord 
                SET @strRowCount =  @strRowCount  + ' and  1 = 1 ' + @KeyWord 
            END
                
        END


if @SortChar<>'0'
	BEGIN
	  set @strSql=@strSql + ' AND vcItemName like '''+@SortChar+'%'')'  
	  set @strRowCount=@strRowCount + ' AND vcItemName like '''+@SortChar+'%'''  
	END
ELSE
   BEGIN
      set @strSql=@strSql + ' ) '  
   END


 





   set @strSql=@strSql + ' SELECT * FROM WarehouseItems WHERE 
                            RowNumber > '+CONVERT(VARCHAR(20),@firstRec) +' AND RowNumber < '+CONVERT(VARCHAR(20),@lastRec) + @strRowCount
 
   
  
PRINT(@strSql)
exec(@strSql)

SELECT * FROM @TempForm

GO
--exec USP_GetItemsForInventoryAdjustment @numDomainID=156,@numWarehouseID=1039,@numItemGroupID=0,@KeyWord='',@numCurrentPage=1,@PageSize=5,@numTotalPage=0,@SortChar='0'


