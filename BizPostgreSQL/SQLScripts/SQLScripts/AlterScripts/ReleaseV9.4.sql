/******************************************************************
Project: Release 9.4 Date: 02.APR.2018
Comments: ALTER SCRIPTS
*******************************************************************/

/*************************** NEELAM *******************************/

BEGIN TRY
BEGIN TRANSACTION

	DECLARE @numFieldID NUMERIC(18,0)

	INSERT INTO DycFieldMaster
	(
		numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcLookBackTableName,vcPropertyName, vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,tintRow,tintColumn,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitDetailField,bitAllowSorting,bitAllowFiltering
	)
	VALUES
	(
		3,'Customer Part#','CustomerPartNo','CustomerPartNo','CustomerPartNumber','CustomerPartNo','N','R','TextBox','',0,1,1,1,0,0,0,1,1,0,1
	)

	SET @numFieldID = SCOPE_IDENTITY()

	INSERT INTO DycFormField_Mapping
	(
		numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering
	)
	VALUES
	(
		3,@numFieldID,7,0,0,'Customer Part#','TextBox',1,1,1,0,0,1,0,1,0,1
	)

	INSERT INTO DycFormField_Mapping
	(
		numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,bitAddField 
	)
	VALUES
	(
		4,@numFieldID,91,0,0,1
	)

	INSERT INTO DycFormField_Mapping
	(
		numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,
		vcPropertyName,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowFiltering
	)
	VALUES
	(
		3,@numFieldID,26,1,1,'Customer Part#','TextBox',
		'CustomerPartNo',1,0,1,1,1
	)

	INSERT INTO DycFormField_Mapping
	(
		numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering
	)
	VALUES
	(
		3,@numFieldID,21,1,1,'Customer Part#','TextBox',1,1,1,0,0,1,0,1,0,1
	)
	--40853
COMMIT
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		ERROR_MESSAGE(),
		ERROR_NUMBER(),
		ERROR_SEVERITY(),
		ERROR_STATE(),
		ERROR_LINE(),
		ISNULL(ERROR_PROCEDURE(), '-');
END CATCH
------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------Manage Authorisation for CustomePart#-----------------------------------------------------------------------------------

INSERT INTO PageMaster
		(numPageID,numModuleID,vcFileName,vcPageDesc,bitIsViewApplicable,bitIsAddApplicable,bitIsDeleteApplicable,bitIsExportApplicable,bitIsUpdateApplicable)
	VALUES
		(136,10,'frmNewOrder.aspx','Customer Part#',0,1,0,0,1)

------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------NewTable for CustomePart#-----------------------------------------------------------------------------------

/****** Object:  Table [dbo].[CustomerPartNumber]    Script Date: 31-03-2018 12:54:08 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[CustomerPartNumber](
	[CustomerPartNoID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numItemCode] [numeric](18, 0) NOT NULL,
	[numCompanyId] [numeric](18, 0) NOT NULL,
	[numDomainID] [numeric](18, 0) NOT NULL,
	[CustomerPartNo] [varchar](100) NOT NULL,
 CONSTRAINT [PK_CustomerPartNumber] PRIMARY KEY CLUSTERED 
(
	[CustomerPartNoID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[CustomerPartNumber]  WITH CHECK ADD  CONSTRAINT [FK_CustomerPartNumber_Domain] FOREIGN KEY([numDomainID])
REFERENCES [dbo].[Domain] ([numDomainId])
GO

ALTER TABLE [dbo].[CustomerPartNumber] CHECK CONSTRAINT [FK_CustomerPartNumber_Domain]
GO

ALTER TABLE [dbo].[CustomerPartNumber]  WITH CHECK ADD  CONSTRAINT [FK_CustomerPartNumber_Item1] FOREIGN KEY([numItemCode])
REFERENCES [dbo].[Item] ([numItemCode])
GO

ALTER TABLE [dbo].[CustomerPartNumber] CHECK CONSTRAINT [FK_CustomerPartNumber_Item1]
GO


------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------Updated label from Warehouse Stock OnHand To On-Hand-----------------------------------------------------------------------------------

--select * from DycFormField_Mapping where vcFieldName like '%Warehouse Stock OnHand%' and numFormID in (27,129,26)

UPDATE DycFormField_Mapping SET vcFieldName = 'On-Hand' WHERE vcFieldName LIKE '%Warehouse Stock OnHand%' AND numFormID IN (27,129,26)
------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------Added Column names to SO Bizdoc-----------------------------------------------------------------------------------
BEGIN TRY
BEGIN TRANSACTION

	DECLARE @numFieldID NUMERIC(18,0)

	INSERT INTO DycFieldMaster
	(
		numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcLookBackTableName, vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,tintRow,tintColumn,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitDetailField,bitAllowSorting,bitAllowFiltering
	)
	VALUES
	(
		3,'Available','numAvailable','numAvailable','WareHouseItems','N','R','TextBox','',0,1,1,1,0,0,0,1,1,0,1
	)

	SET @numFieldID = SCOPE_IDENTITY()

	INSERT INTO DycFormField_Mapping
	(
		numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering
	)
	VALUES
	(
		3,@numFieldID,7,0,0,'Available','TextBox',1,1,1,0,0,1,0,1,0,1
	)
COMMIT
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		ERROR_MESSAGE(),
		ERROR_NUMBER(),
		ERROR_SEVERITY(),
		ERROR_STATE(),
		ERROR_LINE(),
		ISNULL(ERROR_PROCEDURE(), '-');
END CATCH
-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------
--dtReleaseDate -- numFieldID = 40730
INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,
	tintRow,bitInResults,bitDeleted,bitDefault,bitAllowFiltering
)
VALUES
(
	3,40730,7,0,0,'Item Release Date','TextBox', 1,1,0,0,1
)

--vcWarehouse -- numFieldID = 199
INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,
	tintRow,bitInResults,bitDeleted,bitDefault,bitAllowFiltering
)
VALUES
(
	3,199,7,0,0,'WH Location','TextBox', 1,1,0,0,1
)

--numonhand -- numFieldID = 195
INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,
	tintRow,bitInResults,bitDeleted,bitDefault,bitAllowFiltering
)
VALUES
(
	3,195,7,0,0,'On-Hand','TextBox', 1,1,0,0,1
)
------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------Changes to referring string - Lead source Admin-----------------------------------------------------------------------------------

ALTER TABLE TrackingVisitorsHDR ADD numListItemID NUMERIC(18,0)
------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------Changes to PO Fulfillment Table------------------------------------------------------------------------------------

ALTER TABLE PurchaseOrderFulfillmentSettings DROP column numBizDocTempID 
------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------Changes to Item Template------------------------------------------------------------------------------------

ALTER TABLE OpportunitySalesTemplate ADD tintAppliesTo INT

