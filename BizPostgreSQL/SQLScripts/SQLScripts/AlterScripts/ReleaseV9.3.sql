/******************************************************************
Project: Release 9.3 Date: 15.MARCH.2018
Comments: ALTER SCRIPTS
*******************************************************************/

/*************************** SANDEEP *******************************/

ALTER TABLE ItemDetails ALTER COLUMN [numQtyItemsReq] FLOAT

-------------------------------

ALTER TABLE OpportunityBizDocs ADD vcVendorInvoice VARCHAR(100)
ALTER TABLE OpportunityBizDocItems ADD numVendorInvoiceBizDocID NUMERIC(18,0)
ALTER TABLE OpportunityBizDocItems ADD numVendorInvoiceUnitReceived FLOAT

ALTER TABLE WareHouseItems_Tracking ALTER COLUMN [numOnHand] FLOAT
ALTER TABLE WareHouseItems_Tracking ALTER COLUMN [numOnOrder] FLOAT
ALTER TABLE WareHouseItems_Tracking ALTER COLUMN [numReorder] FLOAT
ALTER TABLE WareHouseItems_Tracking ALTER COLUMN [numAllocation] FLOAT
ALTER TABLE WareHouseItems_Tracking ALTER COLUMN [numBackOrder] FLOAT

------------------------------

SET IDENTITY_INSERT ListDetails ON
INSERT INTO ListDetails
( numListItemID,numListID,vcData,numCreatedBY,bitDelete,numDomainID,constFlag,sintOrder)
VALUES
(305,27,'Vendor Invoice',1,0,1,1,1)
SET IDENTITY_INSERT ListDetails OFF

------------------------------

BEGIN TRY
BEGIN TRANSACTION
	SELECT 
		* 
	INTO 
		#temp 
	FROM
	(
		SELECT 
			ROW_NUMBER() OVER(ORDER BY numDomainId ASC) AS Row, 
			numDomainId 
		FROM 
			Domain 
		WHERE 
			numDomainId > 0
	) TABLE2
	
	DECLARE @RowCount INT
	SET @RowCount = (SELECT COUNT(Row) FROM #temp) 
	
	DECLARE @I INT
	DECLARE @numDomainId NUMERIC(18,0)
	
	SET @I = 1

	WHILE (@I <= @RowCount)
	BEGIN
			
		SELECT @numDomainId = numDomainID FROM #temp WHERE Row = @I
	
		INSERT INTO BizDocTemplate
		(
			numDomainID,numBizDocID,numOppType,txtBizDocTemplate,txtCSS,bitEnabled,tintTemplateType,vcTemplateName,bitDefault,txtNewBizDocTemplate
		)
		VALUES
		(
			@numDomainId,305,2,'&lt;table width=&quot;100%&quot; border=&quot;0&quot; cellpadding=&quot;0&quot;&gt;
    &lt;tbody&gt;
        &lt;tr&gt;
            &lt;td valign=&quot;top&quot; align=&quot;left&quot;&gt;
            #Logo#
            &lt;/td&gt;
            &lt;td align=&quot;right&quot; class=&quot;title&quot; style=&quot;white-space: nowrap;&quot;&gt;
            #Customer/VendorOrganizationName#
            &lt;/td&gt;
            &lt;td align=&quot;right&quot; class=&quot;title&quot; style=&quot;white-space: nowrap;&quot;&gt;
            #BizDocTemplateName#
            &lt;/td&gt;
        &lt;/tr&gt;
    &lt;/tbody&gt;
&lt;/table&gt;
&lt;table width=&quot;100%&quot; border=&quot;0&quot; cellpadding=&quot;0&quot;&gt;
    &lt;tbody&gt;
        &lt;tr&gt;
            &lt;td align=&quot;right&quot;&gt;
            &lt;table width=&quot;100%&quot;&gt;
                &lt;tbody&gt;
                    &lt;tr&gt;
                        &lt;td class=&quot;RowHeader&quot;&gt;
                        Discount
                        &lt;/td&gt;
                        &lt;td class=&quot;RowHeader&quot;&gt;
                        Billing Terms
                        &lt;/td&gt;
                        &lt;td class=&quot;RowHeader&quot;&gt;
                        #ChangeDueDate#
                        &lt;/td&gt;
                        &lt;td class=&quot;RowHeader&quot;&gt;
                        ID#
                        &lt;/td&gt;
                    &lt;/tr&gt;
                    &lt;tr&gt;
                        &lt;td class=&quot;normal1&quot;&gt;
                        #Discount#
                        &lt;/td&gt;
                        &lt;td class=&quot;normal1&quot;&gt;
                        #BillingTerms#
                        &lt;/td&gt;
                        &lt;td class=&quot;normal1&quot;&gt;
                        #DueDate#
                        &lt;/td&gt;
                        &lt;td class=&quot;normal1&quot;&gt;
                        #BizDocID#
                        &lt;/td&gt;
                    &lt;/tr&gt;
                &lt;/tbody&gt;
            &lt;/table&gt;
            &lt;/td&gt;
        &lt;/tr&gt;
        &lt;tr&gt;
            &lt;td&gt;
            &lt;table height=&quot;100%&quot; width=&quot;100%&quot;&gt;
                &lt;tbody&gt;
                    &lt;tr&gt;
                        &lt;td class=&quot;RowHeader&quot;&gt;
                        #EmployerChangeBillToHeader#
                        &lt;/td&gt;
                        &lt;td class=&quot;RowHeader&quot;&gt;
                        #EmployerChangeShipToHeader#
                        &lt;/td&gt;
                        &lt;td class=&quot;RowHeader&quot;&gt;
                        Status
                        &lt;/td&gt;
                    &lt;/tr&gt;
                    &lt;tr&gt;
                        &lt;td class=&quot;normal1&quot;&gt;
                        #Customer/VendorOrganizationName#&lt;br /&gt;
                        #EmployerBillToAddress#
                        &lt;/td&gt;
                        &lt;td class=&quot;normal1&quot;&gt;
                        #EmployerShipToAddress#
                        &lt;/td&gt;
                        &lt;td&gt;
                        &lt;table height=&quot;100%&quot; width=&quot;100%&quot;&gt;
                            &lt;tbody&gt;
                                &lt;tr&gt;
                                    &lt;td colspan=&quot;2&quot; class=&quot;normal1&quot;&gt;
                                    #BizDocStatus#
                                    &lt;/td&gt;
                                &lt;/tr&gt;
                                &lt;tr&gt;
                                    &lt;td class=&quot;normal1&quot;&gt;
                                    #AmountPaidPopUp#
                                    &lt;/td&gt;
                                    &lt;td class=&quot;normal1&quot;&gt;
                                    #Currency#&amp;nbsp;#AmountPaid#
                                    &lt;/td&gt;
                                &lt;/tr&gt;
                                &lt;tr&gt;
                                    &lt;td class=&quot;normal1&quot;&gt;
                                    Balance Due:
                                    &lt;/td&gt;
                                    &lt;td class=&quot;normal1&quot;&gt;
                                    #Currency#&amp;nbsp;#BalanceDue#
                                    &lt;/td&gt;
                                &lt;/tr&gt;
                            &lt;/tbody&gt;
                        &lt;/table&gt;
                        &lt;/td&gt;
                    &lt;/tr&gt;
                &lt;/tbody&gt;
            &lt;/table&gt;
            &lt;/td&gt;
        &lt;/tr&gt;
        &lt;tr class=&quot;normal1&quot;&gt;
            &lt;td&gt;
            &lt;table height=&quot;100%&quot; width=&quot;100%&quot;&gt;
                &lt;tbody&gt;
                    &lt;tr&gt;
                        &lt;td class=&quot;RowHeader&quot;&gt;
                        &lt;asp:label id=&quot;Label1&quot; runat=&quot;server&quot;&gt;&lt;/asp:label&gt;
                        &amp;nbsp;#
                        &lt;/td&gt;
                        &lt;td class=&quot;RowHeader&quot;&gt;
                        Deal or Order ID
                        &lt;/td&gt;
                    &lt;/tr&gt;
                    &lt;tr&gt;
                        &lt;td&gt;
                        #P.O.NO#
                        &lt;/td&gt;
                        &lt;td class=&quot;normal1&quot;&gt;
                        #OrderID#
                        &lt;/td&gt;
                    &lt;/tr&gt;
                &lt;/tbody&gt;
            &lt;/table&gt;
            &lt;/td&gt;
        &lt;/tr&gt;
        &lt;tr&gt;
            &lt;td class=&quot;RowHeader&quot;&gt;
            Comments
            &lt;/td&gt;
        &lt;/tr&gt;
        &lt;tr class=&quot;normal1&quot;&gt;
            &lt;td&gt;
            #Comments#
            &lt;/td&gt;
        &lt;/tr&gt;
        &lt;tr class=&quot;normal1&quot;&gt;
            &lt;td&gt;
            #Products#
            &lt;/td&gt;
        &lt;/tr&gt;
    &lt;/tbody&gt;
&lt;/table&gt;
&lt;table width=&quot;100%&quot;&gt;
    &lt;tbody&gt;
        &lt;tr&gt;
            &lt;td align=&quot;right&quot;&gt;
            #BizDocSummary#
            &lt;/td&gt;
        &lt;/tr&gt;
        &lt;tr&gt;
            &lt;td&gt;
            #FooterImage#
            &lt;/td&gt;
        &lt;/tr&gt;
    &lt;/tbody&gt;
&lt;/table&gt;','.title{color: #696969; /*padding:10px;*/font: bold 30px Arial, Helvetica, sans-serif;}
.RowHeader{background-color: #dedede;color: #333;font-family: Arial;font-size: 8pt;}
.RowHeader.hyperlink{color: #333;}
.ItemHeader, .ItemStyle, .AltItemStyle{border-width: 1px;padding: 8px;font: normal 12px/17px arial;border-style: solid;border-color: #666666;background-color: #dedede;}
.ItemHeader{border-width: 1px;padding: 8px;border-style: solid;border-color: #666666;}
.ItemStyle td{border-width: 1px;padding: 8px;border-style: solid;border-color: #666666;background-color: #ffffff;}
.AltItemStyle{background-color: White;border-color: black;padding: 8px;}
.AltItemStyle td{padding: 8px;}
.ItemHeader td{border-width: 1px;padding: 8px;font-weight: bold;border-style: solid;border-color: #666666;background-color: #dedede;}
.ItemHeader td th{border-width: 1px;padding: 8px;border-style: solid;border-color: #666666;background-color: #dedede;}
#tblBizDocSumm{font: normal 12px verdana;color: #333;border-width: 1px;border-color: #666666;border-collapse: collapse;background-color: #dedede;}
#tblBizDocSumm td{border-width: 1px;padding: 8px;border-style: solid;border-color: #666666;}
.WordWrapSerialNo{width: 30%;word-break: break-all;}',1,0,'Vendor Invoice',1,'&lt;table width=&quot;100%&quot; border=&quot;0&quot; cellpadding=&quot;0&quot;&gt;
    &lt;tbody&gt;
        &lt;tr&gt;
            &lt;td valign=&quot;top&quot; align=&quot;left&quot;&gt;
            #Logo#
            &lt;/td&gt;
            &lt;td align=&quot;right&quot; class=&quot;title&quot; style=&quot;white-space: nowrap;&quot;&gt;
            #Customer/VendorOrganizationName#
            &lt;/td&gt;
            &lt;td align=&quot;right&quot; class=&quot;title&quot; style=&quot;white-space: nowrap;&quot;&gt;
            #BizDocTemplateName#
            &lt;/td&gt;
        &lt;/tr&gt;
    &lt;/tbody&gt;
&lt;/table&gt;
&lt;table width=&quot;100%&quot; border=&quot;0&quot; cellpadding=&quot;0&quot;&gt;
    &lt;tbody&gt;
        &lt;tr&gt;
            &lt;td align=&quot;right&quot;&gt;
            &lt;table width=&quot;100%&quot;&gt;
                &lt;tbody&gt;
                    &lt;tr&gt;
                        &lt;td class=&quot;RowHeader&quot;&gt;
                        Discount
                        &lt;/td&gt;
                        &lt;td class=&quot;RowHeader&quot;&gt;
                        Billing Terms
                        &lt;/td&gt;
                        &lt;td class=&quot;RowHeader&quot;&gt;
                        #ChangeDueDate#
                        &lt;/td&gt;
                        &lt;td class=&quot;RowHeader&quot;&gt;
                        ID#
                        &lt;/td&gt;
                    &lt;/tr&gt;
                    &lt;tr&gt;
                        &lt;td class=&quot;normal1&quot;&gt;
                        #Discount#
                        &lt;/td&gt;
                        &lt;td class=&quot;normal1&quot;&gt;
                        #BillingTerms#
                        &lt;/td&gt;
                        &lt;td class=&quot;normal1&quot;&gt;
                        #DueDate#
                        &lt;/td&gt;
                        &lt;td class=&quot;normal1&quot;&gt;
                        #BizDocID#
                        &lt;/td&gt;
                    &lt;/tr&gt;
                &lt;/tbody&gt;
            &lt;/table&gt;
            &lt;/td&gt;
        &lt;/tr&gt;
        &lt;tr&gt;
            &lt;td&gt;
            &lt;table height=&quot;100%&quot; width=&quot;100%&quot;&gt;
                &lt;tbody&gt;
                    &lt;tr&gt;
                        &lt;td class=&quot;RowHeader&quot;&gt;
                        #EmployerChangeBillToHeader#
                        &lt;/td&gt;
                        &lt;td class=&quot;RowHeader&quot;&gt;
                        #EmployerChangeShipToHeader#
                        &lt;/td&gt;
                        &lt;td class=&quot;RowHeader&quot;&gt;
                        Status
                        &lt;/td&gt;
                    &lt;/tr&gt;
                    &lt;tr&gt;
                        &lt;td class=&quot;normal1&quot;&gt;
                        #Customer/VendorOrganizationName#&lt;br /&gt;
                        #EmployerBillToAddress#
                        &lt;/td&gt;
                        &lt;td class=&quot;normal1&quot;&gt;
                        #EmployerShipToAddress#
                        &lt;/td&gt;
                        &lt;td&gt;
                        &lt;table height=&quot;100%&quot; width=&quot;100%&quot;&gt;
                            &lt;tbody&gt;
                                &lt;tr&gt;
                                    &lt;td colspan=&quot;2&quot; class=&quot;normal1&quot;&gt;
                                    #BizDocStatus#
                                    &lt;/td&gt;
                                &lt;/tr&gt;
                                &lt;tr&gt;
                                    &lt;td class=&quot;normal1&quot;&gt;
                                    #AmountPaidPopUp#
                                    &lt;/td&gt;
                                    &lt;td class=&quot;normal1&quot;&gt;
                                    #Currency#&amp;nbsp;#AmountPaid#
                                    &lt;/td&gt;
                                &lt;/tr&gt;
                                &lt;tr&gt;
                                    &lt;td class=&quot;normal1&quot;&gt;
                                    Balance Due:
                                    &lt;/td&gt;
                                    &lt;td class=&quot;normal1&quot;&gt;
                                    #Currency#&amp;nbsp;#BalanceDue#
                                    &lt;/td&gt;
                                &lt;/tr&gt;
                            &lt;/tbody&gt;
                        &lt;/table&gt;
                        &lt;/td&gt;
                    &lt;/tr&gt;
                &lt;/tbody&gt;
            &lt;/table&gt;
            &lt;/td&gt;
        &lt;/tr&gt;
        &lt;tr class=&quot;normal1&quot;&gt;
            &lt;td&gt;
            &lt;table height=&quot;100%&quot; width=&quot;100%&quot;&gt;
                &lt;tbody&gt;
                    &lt;tr&gt;
                        &lt;td class=&quot;RowHeader&quot;&gt;
                        &lt;asp:label id=&quot;Label1&quot; runat=&quot;server&quot;&gt;&lt;/asp:label&gt;
                        &amp;nbsp;#
                        &lt;/td&gt;
                        &lt;td class=&quot;RowHeader&quot;&gt;
                        Deal or Order ID
                        &lt;/td&gt;
                    &lt;/tr&gt;
                    &lt;tr&gt;
                        &lt;td&gt;
                        #P.O.NO#
                        &lt;/td&gt;
                        &lt;td class=&quot;normal1&quot;&gt;
                        #OrderID#
                        &lt;/td&gt;
                    &lt;/tr&gt;
                &lt;/tbody&gt;
            &lt;/table&gt;
            &lt;/td&gt;
        &lt;/tr&gt;
        &lt;tr&gt;
            &lt;td class=&quot;RowHeader&quot;&gt;
            Comments
            &lt;/td&gt;
        &lt;/tr&gt;
        &lt;tr class=&quot;normal1&quot;&gt;
            &lt;td&gt;
            #Comments#
            &lt;/td&gt;
        &lt;/tr&gt;
        &lt;tr class=&quot;normal1&quot;&gt;
            &lt;td&gt;
            #Products#
            &lt;/td&gt;
        &lt;/tr&gt;
    &lt;/tbody&gt;
&lt;/table&gt;
&lt;table width=&quot;100%&quot;&gt;
    &lt;tbody&gt;
        &lt;tr&gt;
            &lt;td align=&quot;right&quot;&gt;
            #BizDocSummary#
            &lt;/td&gt;
        &lt;/tr&gt;
        &lt;tr&gt;
            &lt;td&gt;
            #FooterImage#
            &lt;/td&gt;
        &lt;/tr&gt;
    &lt;/tbody&gt;
&lt;/table&gt;'
		)

		SET @I = @I  + 1
	END
	
	DROP TABLE #temp
COMMIT
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		ERROR_MESSAGE(),
		ERROR_NUMBER(),
		ERROR_SEVERITY(),
		ERROR_STATE(),
		ERROR_LINE(),
		ISNULL(ERROR_PROCEDURE(), '-');
END CATCH

---------------------------------------------

UPDATE DycFieldMaster SET vcFieldType='R',vcFieldDataType='N' WHERE numFieldID=258

INSERT INTO DynamicFormMaster
(
	numFormId,vcFormName,cCustomFieldsAssociated,cAOIAssociated,bitDeleted,tintFlag
)
VALUES
(
	135,'Purchase Fullfilment','N','N',0,1
)

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
)
VALUES
(
	4,211,135,0,0,'Item Code','Label',1,1,0,0,1,0,0
)

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
)
VALUES
(
	4,189,135,0,0,'Item to receive','TextBox',2,1,0,1,1,0,0
)

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
)
VALUES
(
	3,252,135,0,0,'Model ID','TextBox',3,1,0,0,1,0,0
)

-----------------------

DECLARE @numFieldID NUMERIC(18,0)
INSERT INTO DycFieldMaster
(
	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType
	,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitDetailField,bitAllowFiltering
)
VALUES
(
	3,'SKU','vcSKU','vcSKU','SKU','OpportunityItems','V','R','TextBox','',0,1,0,1,0,1,1,1
)

SET @numFieldID = SCOPE_IDENTITY()
INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
)
VALUES
(
	3,@numFieldID,135,0,0,'SKU','TextBox',4,1,0,0,1,0,0
)

-----------------------------

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
)
VALUES
(
	4,203,135,0,0,'UPC','TextBox',5,1,0,0,1,0,0
)

------------------------------------

DECLARE @numFieldID NUMERIC(18,0)
SELECT @numFieldID=numFieldId FROM DycFieldMaster WHERE vcOrigDbColumnName='vcNotes' AND vcLookBackTableName='OpportunityItems'
INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
)
VALUES
(
	3,@numFieldID,135,0,0,'Notes','TextArea',6,1,0,0,1,0,0
)

----------------------------------------

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
)
VALUES
(
	3,253,135,0,0,'Description','TextArea',7,1,0,0,1,0,0
),
(
	3,348,135,0,0,'Unit of Measure','TextBox',8,1,0,0,1,0,0
)

-----------------------------------------

DECLARE @numFieldID NUMERIC(18,0)
SELECT @numFieldID=numFieldId FROM DycFieldMaster WHERE vcOrigDbColumnName='vcAttributes' AND vcLookBackTableName='OpportunityItems'
INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
)
VALUES
(
	3,@numFieldID,135,0,0,'Attributes','TextBox',9,1,0,0,1,0,0
)

--------------------------------

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
)
VALUES
(
	4,352,135,0,0,'Image','TextBox',10,1,0,0,1,0,0
)


----------------------------

DECLARE @numFieldID NUMERIC(18,0)
SELECT @numFieldID=numFieldId FROM DycFieldMaster WHERE vcOrigDbColumnName='SerialLotNo' AND vcLookBackTableName='OpportunityItems'
INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
)
VALUES
(
	3,@numFieldID,135,0,0,'Serial/Lot #s','TextBox',11,1,0,0,1,0,0
)


--------------------------------------

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
)
VALUES
(
	4,233,135,0,0,'Location','SelectBox',12,1,0,1,1,0,0
),
(
	4,195,135,0,0,'On Hand','TextBox',13,1,0,1,1,1,0
),
(
	4,196,135,0,0,'On Order','TextBox',14,1,0,1,1,1,0
),
(
	4,197,135,0,0,'On Allocation','TextBox',15,1,0,0,1,1,0
),
(
	4,198,135,0,0,'On Backorder','TextBox',16,1,0,0,1,1,0
)

-----------------------------------------

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
)
VALUES
(
	3,258,135,0,0,'Qty Ordered','TextBox',17,1,0,1,1,1,0
),
(
	3,493,135,0,0,'Qty Received','TextBox',18,1,0,1,1,1,0
)

------------------------------------------------------------

DECLARE @numFieldID NUMERIC(18,0)
INSERT INTO DycFieldMaster
(
	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType
	,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitDetailField,bitAllowFiltering
)
VALUES
(
	3,'Remaining Qty','numRemainingQty','numRemainingQty','RemainingQty','OpportunityItems','V','R','Label','',0,1,0,0,0,1,0,0
)

SET @numFieldID = SCOPE_IDENTITY()
INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
)
VALUES
(
	3,@numFieldID,135,0,0,'Remaining Qty','Label',19,1,0,1,1,0,0
)

-------------------------------------------------------------------

DECLARE @numFieldID NUMERIC(18,0)
INSERT INTO DycFieldMaster
(
	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType
	,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitDetailField,bitAllowFiltering
)
VALUES
(
	3,'Qty to Ship/Receive','numQtyToShipReceive','numQtyToShipReceive','QtyToShipReceive','OpportunityItems','V','R','TextBox','',0,1,0,0,0,1,0,0
)

SET @numFieldID = SCOPE_IDENTITY()
INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
)
VALUES
(
	3,@numFieldID,135,0,0,'Qty to Receive','TextBox',20,1,0,1,1,0,0
)

------------------------------------------------------------------------------

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
)
VALUES
(
	1,3,135,0,0,'Vendor','TextBox',21,1,0,1,1,1,0
)

---------------------------------------------------------------------------------

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
)
VALUES
(
	3,96,135,0,0,'P.O.','TextBox',22,1,0,1,1,0,0
)

--------------------------------------
--------- Sales Fulfillment ----------
--------------------------------------

DELETE FROM DycFormConfigurationDetails WHERE numFormId=23
DELETE FROm DynamicFormField_Validation WHERE numFormId=23
DELETE FROM DynamicFormConfigurationDetails WHERE numFormId=23
DELETE FROM DycFormField_Mapping WHERE numFormID=23 AND numFieldID IN (51,52,195,196,197,198,199,251,252,253,254,255,258,265,108,116,117,109,119,243)

----------------

UPDATE DycFormField_Mapping SET bitDefault = 0, bitAllowSorting=0, bitAllowFiltering=0 WHERE numFormID=23

----------------

DECLARE @numFieldID NUMERIC(18,0)
SELECT @numFieldID=numFieldId FROM DycFieldMaster WHERE vcOrigDbColumnName='dtReleaseDate' AND vcLookBackTableName='OpportunityMaster'
INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
)
VALUES
(
	3,@numFieldID,23,0,0,'Order Release Date','DateField',1,1,0,1,1,1,1
)

----------------
DECLARE @numFieldID NUMERIC(18,0)
SELECT @numFieldID=numFieldId FROM DycFieldMaster WHERE vcOrigDbColumnName='vcBizDocID' AND vcLookBackTableName='OpportunityBizDocs'
INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
)
VALUES
(
	3,@numFieldID,23,0,0,'FO','TextBox',2,1,0,1,1,1,1
)

----------------

DECLARE @numFieldID NUMERIC(18,0)
INSERT INTO DycFieldMaster
(
	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitAllowFiltering
)
VALUES
(
	3,'Priced/Boxed/Tracked','vcPricedBoxedTracked','vcPricedBoxedTracked','OpportunityMaster','V','R','Label','',0,1,0,0,0,1,0,0,0,0,0
)
SET @numFieldID = SCOPE_IDENTITY()

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
)
VALUES
(
	3,@numFieldID,23,0,0,'Priced/Boxed/Tracked','Label',3,1,0,1,1,0,0
)

------------------------

UPDATE DycFormField_Mapping SET vcFieldName='Order / Status',bitDefault=1, bitAllowSorting=1, bitAllowFiltering=1,[order]=4 WHERE numFormID=23 AND numFieldID=96
UPDATE DycFormField_Mapping SET vcFieldName='Customer',bitDefault=1, bitAllowSorting=1, bitAllowFiltering=1,[order]=5 WHERE numFormID=23 AND numFieldID=3

-------------------------

DECLARE @numFieldID NUMERIC(18,0)
INSERT INTO DycFieldMaster
(
	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitAllowFiltering
)
VALUES
(
	3,'Fulfillment Completed','vcFulfillmentStatus','vcFulfillmentStatus','OpportunityMaster','V','R','Label','',0,1,0,0,0,1,0,0,0,0,0
)
SET @numFieldID = SCOPE_IDENTITY()

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
)
VALUES
(
	3,@numFieldID,23,0,0,'Fulfillment Completed','Label',6,1,0,1,1,0,0
)

-----------------------

DECLARE @numFieldID NUMERIC(18,0)
SELECT @numFieldID=numFieldId FROM DycFieldMaster WHERE vcOrigDbColumnName='vcInventoryStatus' AND vcLookBackTableName='OpportunityItems'
INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
)
VALUES
(
	3,@numFieldID,23,0,0,'Inventory Status','Label',7,1,0,0,1,0,1
)

------------------

DECLARE @numFieldID NUMERIC(18,0)
SELECT @numFieldID=numFieldId FROM DycFieldMaster WHERE vcOrigDbColumnName='numShipmentMethod' AND vcLookBackTableName='OpportunityMaster' AND numListID=82
UPDATE DycFormField_Mapping SET numFieldID=@numFieldID,vcFieldName='Ship Via',[order]=8,bitAllowFiltering=1 WHERE numFieldID=246 AND numFormID=23

---------------------------------------------

DECLARE @numFieldID NUMERIC(18,0)
SELECT @numFieldID=numFieldId FROM DycFieldMaster WHERE vcOrigDbColumnName='numDefaultShippingServiceID' AND vcLookBackTableName='DivisionMaster'
INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
)
VALUES
(
	3,@numFieldID,23,0,0,'Shipping Service','SelectBox',9,1,0,0,1,0,1
)

------------------------------------------------

DECLARE @numFieldID NUMERIC(18,0)
INSERT INTO DycFieldMaster
(
	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitAllowFiltering
)
VALUES
(
	1,'Shipping Instructions','vcDescription','vcDescription','DivisionMasterShippingConfiguration','V','R','TextBox','',0,1,0,1,0,1,0,0,0,0,0
)
SET @numFieldID = SCOPE_IDENTITY()

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
)
VALUES
(
	3,@numFieldID,23,0,0,'Shipping Instructions','TextBox',10,1,0,0,1,0,0
)

----------------------------------------------------


DECLARE @numFieldID NUMERIC(18,0)
INSERT INTO DycFieldMaster
(
	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitAllowFiltering
)
VALUES
(
	1,'Signature Type','vcSignatureType','vcSignatureType','DivisionMasterShippingConfiguration','N','R','SelectBox','SGT',0,1,0,1,0,1,0,0,1,0,1
)
SET @numFieldID = SCOPE_IDENTITY()

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
)
VALUES
(
	3,@numFieldID,23,0,0,'Signature Type','SelectBox',11,1,0,0,1,0,1
)


------------------------------------------------------

UPDATE DycFormField_Mapping SET [order]=12 WHERE numFieldID=111 AND numFormID=23
UPDATE DycFormField_Mapping SET vcFieldName='Total Amount Paid',numFieldID=269,[order]=13 WHERE numFieldID=99 AND numFormID=23

--------------------------------------

DECLARE @numFieldID NUMERIC(18,0)
SELECT @numFieldID=numFieldId FROM DycFieldMaster WHERE vcOrigDbColumnName='numContactID' AND vcLookBackTableName='OpportunityMaster'
INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
)
VALUES
(
	3,@numFieldID,23,0,0,'Contact','SelectBox',14,1,0,0,1,0,0
)

------------------------------------------

DECLARE @numFieldID NUMERIC(18,0)
SELECT @numFieldID=numFieldId FROM DycFieldMaster WHERE vcOrigDbColumnName='vcOrderedShipped' AND vcLookBackTableName='OpportunityMaster'
INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
)
VALUES
(
	3,@numFieldID,23,0,0,'Ordered / Shipped','Label',15,1,0,0,1,0,0
)


------------------------------------------

UPDATE DycFormField_Mapping SET [order]=16 WHERE numFieldID=97 AND numFormID=23

-------------------------------------------

DECLARE @numFieldID NUMERIC(18,0)
SELECT @numFieldID=numFieldId FROM DycFieldMaster WHERE vcOrigDbColumnName='numPartner' AND vcLookBackTableName='OpportunityMaster'
INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
)
VALUES
(
	3,@numFieldID,23,0,0,'Partner Source','SelectBox',17,1,0,0,1,0,1
)

---------------------------------------------------

UPDATE DycFormField_Mapping SET [order]=18 WHERE numFieldID=100 AND numFormID=23
UPDATE DycFormField_Mapping SET [order]=19 WHERE numFieldID=112 AND numFormID=23

----------------------------------------------------

DECLARE @numFieldID NUMERIC(18,0)
SELECT @numFieldID=numFieldId FROM DycFieldMaster WHERE vcOrigDbColumnName='vcOppRefOrderNo' AND vcLookBackTableName='OpportunityMaster'
INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
)
VALUES
(
	3,@numFieldID,23,0,0,'Customer PO#','TextBox',20,1,0,0,1,0,1
)

------------------------------------------------------

DECLARE @numFieldID NUMERIC(18,0)
SELECT @numFieldID=numFieldId FROM DycFieldMaster WHERE vcOrigDbColumnName='txtComments' AND vcLookBackTableName='OpportunityMaster'
INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
)
VALUES
(
	3,@numFieldID,23,0,0,'Sales Order Comments','TextArea',21,1,0,0,1,0,0
)

----------------------------------------------------------

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
)
VALUES
(
	3,219,23,0,0,'Ship To State','TextBox',22,1,0,0,1,0,1
)


UPDATE DycFormField_Mapping SET vcAssociatedControlType='Label' WHERE numFieldID=269 AND numFormID=23

/****************************** PRIYA **********************************/
ALTER TABLE GenericDocuments
  ADD BizDocOppType VARCHAR(10),
	BizDocType VARCHAR(10),
	BizDocTemplate VARCHAR(10);

------------------------------------------------

DECLARE @numParentId NUMERIC(18,0)
DECLARE @numTabId NUMERIC(18,0)
DECLARE @numModuleId NUMERIC(18,0)


SET @numParentId = (SELECT numPageNavID FROM PageNavigationDTL WHERE vcPageNavName = 'Marketing')
SET @numTabId = (SELECT numTabID FROM PageNavigationDTL WHERE vcPageNavName = 'Marketing')
SET @numModuleId = (SELECT numModuleID FROM PageNavigationDTL WHERE vcPageNavName = 'Marketing')


UPDATE PageNavigationDTL 
SET numParentID = @numParentId, numTabID = @numTabId, numModuleID = @numModuleId ---numParentID = 17
WHERE vcPageNavName = 'e-Mail Templates';


---------------------------------------------------------------
ALTER TABLE EmailMergeModule ALTER COLUMN vcModuleName VARCHAR(300)

Declare @numModuleId As numeric

SET @numModuleId = (SELECT MAX(numModuleID) + 1 FROM EmailMergeModule)

INSERT INTO EmailMergeModule
(
  numModuleID,vcModuleName,tintModuleType
)
VALUES
(
	@numModuleId, 'Organization(Leads/Prospects/Accounts) And Contacts' , 0
)
---------------------------------------------------
Update EmailMergeModule
SET tintModuleType = 2
WHERE vcModuleName IN('Items','Projects','Cases','Tickler','Documents','Leads/Prospects/Accounts')
--------------------------------------------------------------------------------------------------

Declare @numModuleId numeric

SET @numModuleId = (SELECT numModuleID FROM EmailMergeModule WHERE vcModuleName = 'Organization(Leads/Prospects/Accounts) And Contacts' AND tintModuleType = 0)

INSERT INTO EmailMergeFields
(
	vcMergeField, vcMergeFieldValue, numModuleID, tintModuleType
)
VALUES
--Org. Name
(
	'Org. Profile', '##OrgProfile##', @numModuleId , 0 
),
(
	'Org. Assign to', '##OrgAssignto##' , @numModuleId, 0
),
(
	'Org. Billing Name', '##OrgBillingName##' , @numModuleId, 0
),
(
	'Org. Billing Street', '##OrgBillingStreet##' , @numModuleId, 0
),
(
	'Org. Billing City', '##OrgBillingCity##' , @numModuleId, 0
),
(
	'Org. Billing State', '##OrgBillingState##' , @numModuleId, 0
),
(
	'Org. Billing Postal', '##OrgBillingPostal##' , @numModuleId, 0
),
(
	'Org. Billing Country', '##OrgBillingCountry##' , @numModuleId, 0
),
(
	'Org. Shipping Name', '##OrgShippingName##' , @numModuleId, 0
),
(
	'Org. Shipping Street', '##OrgShippingStreet##' , @numModuleId, 0
),
(
	'Org. Shipping City', '##OrgShippingCity##' , @numModuleId, 0
),
(
	'Org. Shipping State', '##OrgShippingState##' , @numModuleId, 0
),
(
	'Org. Shipping Postal', '##OrgShippingPostal##' , @numModuleId, 0
),
(
	'Org. Shipping Country', '##OrgShippingCountry##' , @numModuleId, 0
),
(
	'Org. Credit Limit', '##OrgCreditLimit##' , @numModuleId, 0
),
(
	'Org. Net Terms', '##OrgNetTerms##' , @numModuleId, 0
),
(
	'Org. Payment Method', '##OrgPaymentMethod##' , @numModuleId, 0
),
(
	'Org. Preferred Ship Via', '##OrgPreferredShipVia##' , @numModuleId, 0
),
(
	'Org. Preferred Parcel Shipping Service', '##OrgPreferredParcelShippingService##' , @numModuleId, 0
),
(
	'Org. Last 4 digits of primary credit card', '##OrgLast4digitsofprimarycreditcard##' , @numModuleId, 0
),
-- My Signature
(
	'Contact First Name', '##ContactFirstName##', @numModuleId , 0 
),
(
	'Contact Last Name', '##ContactLastName##', @numModuleId , 0 
),
(
	'Contact Email', '##ContactEmail##', @numModuleId , 0 
),
(
	'Contact Phone', '##ContactPhone##', @numModuleId , 0 
),
(
	'Contact Phone Extension', '##ContactPhoneExt##', @numModuleId , 0 
),
(
	'Contact Cell Phone', '##ContactCellPhone##', @numModuleId , 0 
),
(
	'Contact Ecommerce password', '##ContactEcommercepassword##' , @numModuleId, 0
),
(
	'My Signature', '##Signature##' , @numModuleId, 0
)

UPDATE EmailMergeFields
SET tintModuleType = 0 , numModuleID = 45
WHERE vcMergeField = 'Org Name' ANd numModuleID = 1 and  isnull(tintModuleType,0) = 0

UPDATE EmailMergeFields SET vcMergeField = 'Org. Name' WHERE vcMergeField = 'Org Name' AND numModuleID = 1 AND tintModuleType = 0
---------------------------------------------------------------------------


Declare @numModuleId numeric

SET @numModuleId = (SELECT numModuleID FROM EmailMergeModule WHERE vcModuleName = 'Opportunity/Order' AND tintModuleType = 0)

INSERT INTO EmailMergeFields
(
	vcMergeField, vcMergeFieldValue, numModuleID, tintModuleType
)
VALUES

(
	'Opp/Order Assigned to', '##OpportunityAssigneeName##', @numModuleId , 0 
),
(
	'Opp/Order Sub-Total', '##OppOrderSubTotal##', @numModuleId , 0 
),
(
	'Opp/Order Name/ID', '##OpportunityName##', @numModuleId , 0 
),
(
	'Opp/Order Contact', '##OppOrderContact##' , @numModuleId, 0
),
(
	'Opp/Order Customer PO#', '##OppOrderCustomerPO##' , @numModuleId, 0
),
(
	'Opp/Order Invoice Grand-total', '##OppOrderInvoiceGrandtotal##' , @numModuleId, 0
),
(
	'Opp/Order Total Amount Paid', '##OppOrderTotalAmountPaid##' , @numModuleId, 0
),
(
	'Opp/Order Billing Address Name', '##OppOrderBillingAddressName##' , @numModuleId, 0
),
(
	'Opp/Order Billing Street', '##OppOrderBillingStreet##' , @numModuleId, 0
),
(
	'Opp/Order Billing City', '##OppOrderBillingCity##' , @numModuleId, 0
),
(
	'Opp/Order Billing State', '##OppOrderBillingState##' , @numModuleId, 0
),
(
	'Opp/Order Billing Postal', '##OppOrderBillingPostal##' , @numModuleId, 0
),
(
	'Opp/Order Billing Country', '##OppOrderBillingCountry##' , @numModuleId, 0
),
(
	'Opp/Order Shipping Address Name', '##OppOrderShippingAddressName##' , @numModuleId, 0
),
(
	'Opp/Order Shipping Street', '##OppOrderShippingStreet##' , @numModuleId, 0
),
(
	'Opp/Order Shipping City', '##OppOrderShippingCity##' , @numModuleId, 0
),
(
	'Opp/Order Shipping State', '##OppOrderShippingState##' , @numModuleId, 0
),
(
	'Opp/Order Shipping Postal', '##OppOrderShippingPostal##' , @numModuleId, 0
),
(
	'Opp/Order Shipping Country', '##OppOrderShippingCountry##' , @numModuleId, 0
),
(
	'Opp/Order Ship Via', '##OppOrderShipVia##' , @numModuleId, 0
),
(
	'Opp/Order Comments', '##OppOrderComments##' , @numModuleId, 0
),
(
	'Opp/Order Release Date', '##OppOrderReleaseDate##' , @numModuleId, 0
),
(
	'Opp/Order Order Close Date', '##OppOrderEstimatedCloseDate##' , @numModuleId, 0
),
(
	'Opp/Order Source', '##OppOrderSource##' , @numModuleId, 0
)

-- My Signature

UPDATE EmailMergeFields SET vcMergeField = 'My Signature' WHERE vcMergeField = 'Signature' AND numModuleID = 2 AND tintModuleType = 0

UPDATE EmailMergeFields 
SET tintModuleType = 2 WHERE vcMergeField = 'Tracking Numbers' AND numModuleID = 2 AND tintModuleType = 0
------------------------------------------------------------------------------------------------

Declare @numModuleId numeric

SET @numModuleId = (SELECT numModuleID FROM EmailMergeModule WHERE vcModuleName = 'Leads/Prospects/Accounts' AND tintModuleType = 0)
INSERT INTO EmailMergeFields
(
	vcMergeField, vcMergeFieldValue, numModuleID, tintModuleType
)
VALUES
--Org. Name
(
	'Org. Profile', '##OrgProfile##', @numModuleId , 0 
),
(
	'Org. Assign to', '##OrgAssignto##' , @numModuleId, 0
),
(
	'Org. Billing Name', '##OrgBillingName##' , @numModuleId, 0
),
(
	'Org. Billing Street', '##OrgBillingStreet##' , @numModuleId, 0
),
(
	'Org. Billing City', '##OrgBillingCity##' , @numModuleId, 0
),
(
	'Org. Billing State', '##OrgBillingState##' , @numModuleId, 0
),
(
	'Org. Billing Postal', '##OrgBillingPostal##' , @numModuleId, 0
),
(
	'Org. Billing Country', '##OrgBillingCountry##' , @numModuleId, 0
),
(
	'Org. Shipping Name', '##OrgShippingName##' , @numModuleId, 0
),
(
	'Org. Shipping Street', '##OrgShippingStreet##' , @numModuleId, 0
),
(
	'Org. Shipping City', '##OrgShippingCity##' , @numModuleId, 0
),
(
	'Org. Shipping State', '##OrgShippingState##' , @numModuleId, 0
),
(
	'Org. Shipping Postal', '##OrgShippingPostal##' , @numModuleId, 0
),
(
	'Org. Shipping Country', '##OrgShippingCountry##' , @numModuleId, 0
),
(
	'Org. Credit Limit', '##OrgCreditLimit##' , @numModuleId, 0
),
(
	'Org. Net Terms', '##OrgNetTerms##' , @numModuleId, 0
),
(
	'Org. Payment Method', '##OrgPaymentMethod##' , @numModuleId, 0
),
(
	'Org. Preferred Ship Via', '##OrgPreferredShipVia##' , @numModuleId, 0
),
(
	'Org. Preferred Parcel Shipping Service', '##OrgPreferredParcelShippingService##' , @numModuleId, 0
),
(
	'Org. Last 4 digits of primary credit card', '##OrgLast4digitsofprimarycreditcard##' , @numModuleId, 0
)
-- My Signature

UPDATE EmailMergeFields
SET tintModuleType = 2
WHERE vcMergeField = 'Contact Phone Ext' ANd numModuleID = 1 and tintModuleType = 0

UPDATE EmailMergeFields
SET tintModuleType = 0
WHERE vcMergeField = 'Org Name' ANd numModuleID = 1 and  isnull(tintModuleType,0) = 0

UPDATE EmailMergeFields SET vcMergeField = 'Org. Name' WHERE vcMergeField = 'Org Name' AND numModuleID = 1 AND tintModuleType = 0

UPDATE EmailMergeFields SET vcMergeField = 'My Signature' WHERE vcMergeField = 'Signature' AND numModuleID = 1 AND tintModuleType = 0

-------------------------------------------------------------------------------------------------------------------------------------




/***************************** NEELAM *********************************/

------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------Changes to Leads Menus------------------------------------------------------------------------------------

DECLARE @numPageNavID INT, @numParentID INT

--SELECT * FROM TabMaster WHERE numTabName LIKE '%Relationships%' AND tintTabType=1
SELECT @numPageNavID = numtabID FROM TabMaster WHERE numTabName LIKE '%Relationships%' AND tintTabType=1
--SELECT @numPageNavID 

--SELECT * FROM PageNavigationDTL WHERE vcPageNavName='Leads'
SELECT @numParentID=numParentID  FROM PageNavigationDTL WHERE vcPageNavName='Leads'
--SELECT @numParentID

UPDATE PageNavigationDTL SET vcNavURL ='../Leads/frmLeadList.aspx', vcaddurl='~/include/frmAddOrganization.aspx?RelID=1&FormID=34', bitAddIsPopUp=1
WHERE numPageNavID=@numPageNavID AND numParentID=@numParentID


UPDATE ShortCutBar SET vcLinkName='Leads', Link='../Leads/frmLeadList.aspx', newlink='../include/frmAddOrganization.aspx?RelID=1&FormID=34'
WHERE vcLinkName='MyLeads'

DELETE FROM ShortCutBar WHERE vcLinkName='WebLeads'

DELETE FROM ShortCutBar WHERE vcLinkName='PublicLeads'


------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------Shipping Service/Signature Type------------------------------------------------------------------------------------
BEGIN TRY
BEGIN TRANSACTION

	DECLARE @numFieldID NUMERIC(18,0)

	INSERT INTO DycFieldMaster
	(
		numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,tintRow,tintColumn,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitDetailField,bitAllowSorting,bitAllowFiltering
	)
	VALUES
	(
		3,'Shipping Service','numDefaultShippingServiceID','numDefaultShippingServiceID','ShippingService','OpportunityMaster','N','R','SelectBox','',0,1,1,1,0,0,0,1,1,0,1
	)

	SET @numFieldID = SCOPE_IDENTITY()

	INSERT INTO DycFormField_Mapping
	(
		numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering
	)
	VALUES
	(
		3,@numFieldID,39,0,0,'Shipping Service','SelectBox','ShippingService',1,1,1,0,0,1,0,1,0,1
	)
COMMIT
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		ERROR_MESSAGE(),
		ERROR_NUMBER(),
		ERROR_SEVERITY(),
		ERROR_STATE(),
		ERROR_LINE(),
		ISNULL(ERROR_PROCEDURE(), '-');
END CATCH

BEGIN TRY
BEGIN TRANSACTION

	DECLARE @numFieldID NUMERIC(18,0)

	INSERT INTO DycFieldMaster
	(
		numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,tintRow,tintColumn,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitDetailField,bitAllowSorting,bitAllowFiltering
	)
	VALUES
	(
		3,'Signature Type','vcSignatureType','vcSignatureType','SignatureType','OpportunityMaster','N','R','SelectBox','',0,1,1,1,0,0,0,1,1,0,1
	)

	SET @numFieldID = SCOPE_IDENTITY()

	INSERT INTO DycFormField_Mapping
	(
		numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering
	)
	VALUES
	(
		3,@numFieldID,39,0,0,'Signature Type','SelectBox','SignatureType',1,1,1,0,0,1,0,1,0,1
	)
COMMIT
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		ERROR_MESSAGE(),
		ERROR_NUMBER(),
		ERROR_SEVERITY(),
		ERROR_STATE(),
		ERROR_LINE(),
		ISNULL(ERROR_PROCEDURE(), '-');
END CATCH
------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------CREATE TABLE FOR PO Fulfillment Settings------------------------------------------------------------------------------------
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[PurchaseOrderFulfillmentSettings](
	[numPOFulfillmentID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numDomainID] [numeric](18, 0) NOT NULL,
	[bitPartialQtyOrderStatus] [bit] NULL,
	[numPartialReceiveOrderStatus] [numeric](18, 0) NULL,
	[bitAllQtyOrderStatus] [bit] NULL,
	[numFullReceiveOrderStatus] [numeric](18, 0) NULL,
	[bitBizDoc] [bit] NULL,
	[numBizDocID] [numeric](18, 0) NULL,
	[bitClosePO] [bit] NULL,
	[numCreatedBY] [numeric](18, 0) NULL,
	[bintCreatedDate] [datetime] NULL,
	[numModifiedBy] [numeric](18, 0) NULL,
	[bintModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_PurchaseOrderFulfillmentSettings] PRIMARY KEY CLUSTERED 
(
	[numPOFulfillmentID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[PurchaseOrderFulfillmentSettings]  WITH CHECK ADD  CONSTRAINT [FK_PurchaseOrderFulfillmentSettings_PurchaseOrderFulfillmentSettings] FOREIGN KEY([numDomainID])
REFERENCES [dbo].[Domain] ([numDomainId])
GO

ALTER TABLE [dbo].[PurchaseOrderFulfillmentSettings] CHECK CONSTRAINT [FK_PurchaseOrderFulfillmentSettings_PurchaseOrderFulfillmentSettings]
GO