/******************************************************************
Project: Release 12.6 Date: 09.SEP.2019
Comments: ALTER SCRIPTS
*******************************************************************/


/******************************************** SANDEEP *********************************************/

UPDATE BizDocTemplate SET txtBizDocTemplate=REPLACE(txtBizDocTemplate,'#Customer/VendorBillToCompanyName#','#OppOrderBillToCompanyName#')
UPDATE BizDocTemplate SET txtBizDocTemplate=REPLACE(txtBizDocTemplate,'#Customer/VendorBillToAddress#','#OppOrderBillToAddress#')
UPDATE BizDocTemplate SET txtBizDocTemplate=REPLACE(txtBizDocTemplate,'#Customer/VendorBillToStreet#','#OppOrderBillToStreet#')
UPDATE BizDocTemplate SET txtBizDocTemplate=REPLACE(txtBizDocTemplate,'#Customer/VendorBillToCity#','#OppOrderBillToCity#')
UPDATE BizDocTemplate SET txtBizDocTemplate=REPLACE(txtBizDocTemplate,'#Customer/VendorBillToPostal#','#OppOrderBillToPostal#')
UPDATE BizDocTemplate SET txtBizDocTemplate=REPLACE(txtBizDocTemplate,'#Customer/VendorBillToState#','#OppOrderBillToState#')
UPDATE BizDocTemplate SET txtBizDocTemplate=REPLACE(txtBizDocTemplate,'#Customer/VendorBillToCountry#','#OppOrderBillToCountry#')
UPDATE BizDocTemplate SET txtBizDocTemplate=REPLACE(txtBizDocTemplate,'#Customer/VendorBillToAddressName#','#OppOrderBillToAddressName#')
UPDATE BizDocTemplate SET txtBizDocTemplate=REPLACE(txtBizDocTemplate,'#Customer/VendorShipToCompanyName#','#OppOrderShipToCompanyName#')
UPDATE BizDocTemplate SET txtBizDocTemplate=REPLACE(txtBizDocTemplate,'#Customer/VendorShipToAddress#','#OppOrderShipToAddress#')
UPDATE BizDocTemplate SET txtBizDocTemplate=REPLACE(txtBizDocTemplate,'#Customer/VendorShipToStreet#','#OppOrderShipToStreet#')
UPDATE BizDocTemplate SET txtBizDocTemplate=REPLACE(txtBizDocTemplate,'#Customer/VendorShipToCity#','#OppOrderShipToCity#')
UPDATE BizDocTemplate SET txtBizDocTemplate=REPLACE(txtBizDocTemplate,'#Customer/VendorShipToPostal#','#OppOrderShipToPostal#')
UPDATE BizDocTemplate SET txtBizDocTemplate=REPLACE(txtBizDocTemplate,'#Customer/VendorShipToState#','#OppOrderShipToState#')
UPDATE BizDocTemplate SET txtBizDocTemplate=REPLACE(txtBizDocTemplate,'#Customer/VendorShipToCountry#','#OppOrderShipToCountry#')
UPDATE BizDocTemplate SET txtBizDocTemplate=REPLACE(txtBizDocTemplate,'#Customer/VendorShipToAddressName#','#OppOrderShipToAddressName#')
UPDATE BizDocTemplate SET txtBizDocTemplate=REPLACE(txtBizDocTemplate,'#Customer/VendorChangeShipToHeader(Ship To)#','#OppOrderChangeShipToHeader(Ship To)#')
UPDATE BizDocTemplate SET txtBizDocTemplate=REPLACE(txtBizDocTemplate,'#Customer/VendorChangeShipToHeader#','#OppOrderChangeShipToHeader#')
UPDATE BizDocTemplate SET txtBizDocTemplate=REPLACE(txtBizDocTemplate,'#Customer/VendorChangeBillToHeader(Bill To)#','#OppOrderChangeBillToHeader(Bill To)#')
UPDATE BizDocTemplate SET txtBizDocTemplate=REPLACE(txtBizDocTemplate,'#Customer/VendorChangeBillToHeader#','#OppOrderChangeBillToHeader#')

-----------------------

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,tintROw,tintColumn,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
)
VALUES
(
	3,259,135,0,0,'Unit Price','TextBox',38,1,38,1,0,0,1,0,0
),
(
	3,260,135,0,0,'Amount','TextBox',39,1,39,1,0,0,1,0,0
)
-------------------------------------


ALTER TABLE Domain ADD bitDoNotShowDropshipPOWindow BIT DEFAULT 0
ALTER TABLE Domain ADD tintReceivePaymentTo TINYINT DEFAULT 2
ALTER TABLE Domain ADD numReceivePaymentBankAccount NUMERIC(18,0) DEFAULT 0
-----------------------------------------

UPDATE Domain SET bitDoNotShowDropshipPOWindow=0

----------------------------------------

BEGIN TRY
BEGIN TRANSACTION

	INSERT INTO PageMaster 
	(
		numPageID,numModuleID,vcFileName,vcPageDesc,bitIsViewApplicable,bitIsAddApplicable,bitIsUpdateApplicable,bitIsDeleteApplicable,bitIsExportApplicable
	)
	VALUES
	(
		150,35,'frmAmtPaid.aspx','Receive Payment: Group with other undeposited Funds',1,0,0,0,0
	),
	(
		151,35,'frmAmtPaid.aspx','Receive Payment: Deposit to drop down',1,0,0,0,0
	)

	SELECT 
		* 
	INTO 
		#temp 
	FROM
	(
		SELECT 
			ROW_NUMBER() OVER(ORDER BY numDomainId DESC) AS Row, 
			numDomainId 
		FROM 
			Domain 
		WHERE 
			numDomainId <> -255
	) TABLE2
	
	DECLARE @RowCount INT
	SET @RowCount = (SELECT COUNT(Row) FROM #temp) 
	
	DECLARE @I INT
	DECLARE @numDomainId NUMERIC(18,0)
	
	SET @I = 1

	WHILE (@I <= @RowCount)
	BEGIN
			
		SELECT @numDomainId = numDomainID FROM #temp WHERE Row = @I
	
		INSERT INTO dbo.GroupAuthorization
			(numDomainID,numModuleID,numPageID,numGroupID,intExportAllowed,intPrintAllowed,intViewAllowed,intAddAllowed,intUpdateAllowed,intDeleteAllowed)
		SELECT
			@numDomainId,35,150,numGroupID,0,0,3,0,0,0
		FROM
			AuthenticationGroupMaster
		WHERE
			numDomainID = @numDomainId AND
			tintGroupType = 1

		INSERT INTO dbo.GroupAuthorization
			(numDomainID,numModuleID,numPageID,numGroupID,intExportAllowed,intPrintAllowed,intViewAllowed,intAddAllowed,intUpdateAllowed,intDeleteAllowed)
		SELECT
			@numDomainId,35,151,numGroupID,0,0,3,0,0,0
		FROM
			AuthenticationGroupMaster
		WHERE
			numDomainID = @numDomainId AND
			tintGroupType = 1

		SET @i = @i + 1
	END
COMMIT
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		ERROR_MESSAGE(),
		ERROR_NUMBER(),
		ERROR_SEVERITY(),
		ERROR_STATE(),
		ERROR_LINE(),
		ISNULL(ERROR_PROCEDURE(), '-');
END CATCH