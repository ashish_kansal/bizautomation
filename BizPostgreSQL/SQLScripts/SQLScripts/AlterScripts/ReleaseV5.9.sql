/******************************************************************
Project: Release 5.9 Date: 16.JULY.2016
Comments: ALTER SCRIPTS
*******************************************************************/

/*************************** PRASANT ****************************/

ALTER TABLE CartItems ADD bitParentPromotion BIT DEFAULT 0

ALTER TABLE CartItems ADD PromotionID NUMERIC(18,0)
ALTER TABLE CartItems ADD PromotionDesc VARCHAR(MAX)


ALTER TABLE eCommerceDTL ADD vcPreSellUp VARCHAR(200)
ALTER TABLE eCommerceDTL ADD vcPostSellUp VARCHAR(200)

ALTER TABLE ItemExtendedDetails ADD txtShortDesc NTEXT

ALTER TABLE OpportunityMaster ADD bitIsInitalSalesOrder BIT DEFAULT 0

ALTER TABLE DOMAIN ADD bitMarginPriceViolated BIT DEFAULT 0

BEGIN TRY
BEGIN TRANSACTION

DECLARE @numFieldID AS NUMERIC(18,0)
DECLARE @numFormFieldID AS NUMERIC(18,0)

INSERT INTO DycFieldMaster
(numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,[order],tintRow,tintColumn,
bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numListID)
VALUES
(4,'Item Short HTML Description','txtShortDesc','txtShortDesc','txtShortDesc','ItemExtendedDetails','V','R','EditBox',46,1,1,1,1,0,0,1,1,1,1,1,0)

SELECT @numFieldID = SCOPE_IDENTITY()

INSERT INTO DynamicFormFieldMaster
(numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
 bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
VALUES
(20,'Item Short HTML Description','R','EditBox','txtShortDesc',0,0,'V','txtShortDesc',1,'ItemExtendedDetails',0,7,1,'txtShortDesc',1,7,1,1,1,0)

SELECT @numFormFieldID = SCOPE_IDENTITY()

INSERT INTO DycFormField_Mapping
(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
VALUES
(4,@numFieldID,20,1,1,'Item Short HTML Description','EditBox','txtShortDesc',7,1,7,1,0,0,1,1,1,0,1,@numFormFieldID)

UPDATE DycFieldMaster SET bitImport=1 WHERE numFieldId=@numFieldID
 
COMMIT
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		ERROR_MESSAGE(),
		ERROR_NUMBER(),
		ERROR_SEVERITY(),
		ERROR_STATE(),
		ERROR_LINE(),
		ISNULL(ERROR_PROCEDURE(), '-');
END CATCH


/*************************** SANDEEP ****************************/

ALTER TABLE WorkOrder ADD
monAverageCost DECIMAL(19,4)

=====================================================

ALTER TABLE WorkOrderDetails ADD
monAverageCost DECIMAL(19,4)

=====================================================

USE [Production.2014]
GO

/****** Object:  Table [dbo].[AssembledItem]    Script Date: 02-Jul-16 1:03:12 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[AssembledItem](
	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numDomainID] [numeric](18, 0) NOT NULL,
	[numItemCode] [numeric](18, 0) NOT NULL,
	[numWarehouseItemID] [numeric](18, 0) NOT NULL,
	[numAssembledQty] [numeric](18, 0) NOT NULL,
	[monAverageCost] [decimal](19, 4) NULL,
	[numCreatedBy] [numeric](18, 0) NOT NULL,
	[dtCreatedDate] [datetime] NOT NULL,
	[numDisassembledQty] [numeric](18, 0) NULL,
 CONSTRAINT [PK_ItemAssembled] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[AssembledItem]  WITH CHECK ADD  CONSTRAINT [FK_ItemAssembled_Item] FOREIGN KEY([numItemCode])
REFERENCES [dbo].[Item] ([numItemCode])
GO

ALTER TABLE [dbo].[AssembledItem] CHECK CONSTRAINT [FK_ItemAssembled_Item]
GO

============================================================================


USE [Production.2014]
GO

/****** Object:  Index [NCI_AssembledItem_DomainID]    Script Date: 24-Jun-16 1:48:56 PM ******/
CREATE NONCLUSTERED INDEX [NCI_AssembledItem_DomainID] ON [dbo].[AssembledItem]
(
	[numDomainID] ASC,
	[numItemCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

========================================================================================

USE [Production.2014]
GO

/****** Object:  Table [dbo].[AssembledItemChilds]    Script Date: 24-Jun-16 1:49:23 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[AssembledItemChilds](
	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numAssembledItemID] [numeric](18, 0) NOT NULL,
	[numItemCode] [numeric](18, 0) NOT NULL,
	[numQuantity] [float] NOT NULL,
	[numWarehouseItemID] [numeric](18, 0) NOT NULL,
	[monAverageCost] [decimal](19, 4) NOT NULL,
	[fltQtyRequiredForSingleBuild] [float] NOT NULL,
 CONSTRAINT [PK_AssembledItemChilds] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[AssembledItemChilds]  WITH CHECK ADD  CONSTRAINT [FK_AssembledItemChilds_AssembledItem] FOREIGN KEY([numAssembledItemID])
REFERENCES [dbo].[AssembledItem] ([ID])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[AssembledItemChilds] CHECK CONSTRAINT [FK_AssembledItemChilds_AssembledItem]
GO

ALTER TABLE [dbo].[AssembledItemChilds]  WITH CHECK ADD  CONSTRAINT [FK_AssembledItemChilds_Item] FOREIGN KEY([numItemCode])
REFERENCES [dbo].[Item] ([numItemCode])
GO

ALTER TABLE [dbo].[AssembledItemChilds] CHECK CONSTRAINT [FK_AssembledItemChilds_Item]
GO

=================================================================================================================================

USE [Production.2014]
GO

/****** Object:  Index [NIC_AssembledItemChilds_numAssembledItemID]    Script Date: 24-Jun-16 1:50:09 PM ******/
CREATE NONCLUSTERED INDEX [NIC_AssembledItemChilds_numAssembledItemID] ON [dbo].[AssembledItemChilds]
(
	[numAssembledItemID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

=======================================================================================================================================

INSERT INTO AccountTypeMaster(vcAccountCode,vcAccountType,numParentID) VALUES ('010105','Other Current Assets',2)

========================================================================================================================================
-- TO CHECK ACCOUNT TYE CODE
--SELECT * FROM AccountTypeDetail WHERE vcAccountCode  LIKE '0101%' AND LEN(vcAccountCode) = 6 ORDER BY vcAccountCode DESC
BEGIN TRY
BEGIN TRANSACTION

	DECLARE @TEMP TABLE
	(
		ID INT IDENTITY(1,1),
		numDomainID NUMERIC(18,0)
	)

	INSERT INTO @TEMP (numDomainID) SELECT numDomainId FROM Domain WHERE numDomainId > 0

	DECLARE @I AS INT = 1
	DECLARE @Count AS INT
	DECLARE @numDomainID AS INT

	SELECT @Count=COUNT(*) FROM @TEMP


	WHILE @i <= @Count
	BEGIN
		SELECT @numDomainID=numDomainID FROM @TEMP WHERE ID=@i

		IF EXISTS(SELECT * FROM AccountTypeDetail WHERE numDomainID=@numDomainID AND vcAccountCode='010105')
		BEGIN
			UPDATE AccountTypeDetail SET bitSystem=1 WHERE numDomainID=@numDomainID AND vcAccountCode='010105'
		END
		ELSE
		BEGIN
			INSERT INTO AccountTypeDetail
			(
				vcAccountCode,
				vcAccountType,
				numParentID,
				numDomainID,
				dtCreateDate,
				dtModifiedDate,
				bitSystem
			)
			VALUES
			(
				'010105',
				'Other Current Assets',
				(SELECT numAccountTypeID FROM AccountTypeDetail WHERE numDomainID=@numDomainID AND vcAccountCode='0101'),
				@numDomainID,
				GETUTCDATE(),
				GETUTCDATE(),
				1
			)
		END


		SET @i = @i + 1
	END

COMMIT
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		ERROR_MESSAGE(),
		ERROR_NUMBER(),
		ERROR_SEVERITY(),
		ERROR_STATE(),
		ERROR_LINE(),
		ISNULL(ERROR_PROCEDURE(), '-');
END CATCH

======================================================
DECLARE @numFieldID AS NUMERIC(18,0)

SELECT TOP 1 @numFieldID=numFieldId FROM DycFieldMaster WHERE vcDbColumnName LIKE '%vcPromotionDetail%'

INSERT INTO DycFormField_Mapping(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,bitAddField) VALUES (4,@numFieldID,92,0,0,1)

===================================================================================

DECLARE @numFieldID AS NUMERIC(18,0)

INSERT INTO DycFieldMaster(numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,bitInlineEdit,bitDeleted,bitAllowEdit,bitDefault,numListID,bitInResults)
VALUES (4,'Sales Tax','vcTax','vcTax','Item','M','R','Label',1,0,0,0,0,1)

SELECT @numFieldID = SCOPE_IDENTITY()
INSERT INTO DycFormField_Mapping(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,bitAddField) VALUES (4,@numFieldID,92,0,0,1)

