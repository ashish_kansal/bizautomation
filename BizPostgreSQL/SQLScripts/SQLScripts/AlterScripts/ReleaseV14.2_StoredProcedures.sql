/******************************************************************
Project: Release 14.2 Date: 23.SEP.2020
Comments: STORE PROCEDURES
*******************************************************************/

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetBizDocItemProfitAmountOrMargin')
DROP FUNCTION GetBizDocItemProfitAmountOrMargin
GO
CREATE FUNCTION [dbo].[GetBizDocItemProfitAmountOrMargin]
(
	@numDomainID NUMERIC(18,0)
	,@numOppID NUMERIC(18,0)
	,@numOppBizDocID NUMERIC(18,0)
	,@numOppBizDocItemID NUMERIC(18,0)
	,@tintMode TINYINT --1: Profit Amount, 2:Profit Margin
)    
RETURNS DECIMAL(20,5)    
AS    
BEGIN  
	DECLARE @Value AS DECIMAL(20,5)

	DECLARE @numDiscountItemID NUMERIC(18,0)
	DECLARE @ProfitCost INT
	SELECT @numDiscountItemID=numDiscountServiceItemID,@ProfitCost=numCost FROM Domain WHERE numDomainId=@numDomainID

	If @tintMode = 1
	BEGIN
		SELECT
			@Value = SUM(ISNULL(OBDI.monTotAmount,0) - (ISNULL(OBDI.numUnitHour,0) * (CASE 
																				WHEN ISNULL(TEMPMatchedPO.numoppitemtCode,TEMPPO.numoppitemtCode) IS NOT NULL 
																				THEN ISNULL(ISNULL(TEMPMatchedPO.monPrice,TEMPPO.monPrice),0)
																				ELSE
																					(CASE @ProfitCost 
																						WHEN 3 THEN ISNULL(V.monCost,0) * dbo.fn_UOMConversion(numBaseUnit,I.numItemCode,@numDomainID,numPurchaseUnit) 
																						ELSE ISNULL(OBDI.monAverageCost,0) 
																					END)
																			END)))
		FROM
			OpportunityBizDocs OBD
		INNER JOIN
			OpportunityMaster OM
		ON
			OBD.numOppId = OM.numOppId
		INNER JOIN
			OpportunityBizDocItems OBDI
		ON
			OBD.numOppBizDocsId = OBDI.numOppBizDocID
		INNER JOIN
			OpportunityItems OI
		ON
			OBDI.numOppItemID = OI.numoppitemtCode
		INNER JOIN
			Item I
		ON
			OBDI.numItemCode = I.numItemCode
		OUTER APPLY
		(
			SELECT TOP 1
				OMInner.numOppID
				,OMInner.vcPOppName
				,OIInner.numoppitemtCode
				,OIInner.monPrice
			FROM
				SalesOrderLineItemsPOLinking SOLIPL
			INNER JOIN
				OpportunityMaster OMInner
			ON
				SOLIPL.numPurchaseOrderID = OMInner.numOppId
			INNER JOIN
				OpportunityItems OIInner
			ON
				OMInner.numOppId = OIInner.numOppId
				AND SOLIPL.numPurchaseOrderItemID = OIInner.numoppitemtCode
			WHERE
				SOLIPL.numSalesOrderID = OM.numOppId
				AND SOLIPL.numSalesOrderItemID = OI.numoppitemtCode
		) TEMPMatchedPO
		OUTER APPLY
		(
			SELECT TOP 1
				OMInner.numOppID
				,OMInner.vcPOppName
				,OIInner.numoppitemtCode
				,OIInner.monPrice
			FROM
				OpportunityLinking OL
			INNER JOIN
				OpportunityMaster OMInner
			ON
				OL.numChildOppID = OMInner.numOppId
			INNER JOIN
				OpportunityItems OIInner
			ON
				OMInner.numOppId = OIInner.numOppId
			WHERE
				OL.numParentOppID = OM.numOppId
				AND OIInner.numItemCode = OI.numItemCode
				AND OIInner.vcAttrValues = OI.vcAttrValues
		) TEMPPO
		Left JOIN 
			Vendor V 
		ON 
			V.numVendorID=I.numVendorID 
			AND V.numItemCode=I.numItemCode
		WHERE
			OBD.numOppId = @numOppID
			AND OBD.numOppBizDocsId = @numOppBizDocID
			AND OBDI.numOppBizDocItemID = @numOppBizDocItemID
			AND I.numItemCode NOT IN (@numDiscountItemID)
	END
	ELSE IF @tintMode = 2
	BEGIN
		SELECT
			@Value = (SUM(Profit)/CASE WHEN SUM(monTotAmount) = 0 THEN 1.0 ELSE SUM(monTotAmount) END) * 100
		FROM
		(
			SELECT
				ISNULL(OBDI.monTotAmount,0) monTotAmount
				,ISNULL(OBDI.monTotAmount,0) - (ISNULL(OBDI.numUnitHour,0) * (CASE 
																		WHEN ISNULL(TEMPMatchedPO.numoppitemtCode,TEMPPO.numoppitemtCode) IS NOT NULL 
																		THEN ISNULL(ISNULL(TEMPMatchedPO.monPrice,TEMPPO.monPrice),0)
																		ELSE (CASE @ProfitCost 
																				WHEN 3 THEN ISNULL(V.monCost,0) * dbo.fn_UOMConversion(numBaseUnit,I.numItemCode,@numDomainID,numPurchaseUnit) 
																				ELSE ISNULL(OBDI.monAverageCost,0) 
																				END)
																	END)) Profit
			FROM
				OpportunityBizDocs OBD
			INNER JOIN
				OpportunityMaster OM
			ON
				OBD.numOppId = OM.numOppId
			INNER JOIN
				OpportunityBizDocItems OBDI
			ON
				OBD.numOppBizDocsId = OBDI.numOppBizDocID
			INNER JOIN
				OpportunityItems OI
			ON
				OBDI.numOppItemID = OI.numoppitemtCode
			INNER JOIN
				Item I
			ON
				OBDI.numItemCode = I.numItemCode
			OUTER APPLY
			(
				SELECT TOP 1
					OMInner.numOppID
					,OMInner.vcPOppName
					,OIInner.numoppitemtCode
					,OIInner.monPrice
				FROM
					SalesOrderLineItemsPOLinking SOLIPL
				INNER JOIN
					OpportunityMaster OMInner
				ON
					SOLIPL.numPurchaseOrderID = OMInner.numOppId
				INNER JOIN
					OpportunityItems OIInner
				ON
					OMInner.numOppId = OIInner.numOppId
					AND SOLIPL.numPurchaseOrderItemID = OIInner.numoppitemtCode
				WHERE
					SOLIPL.numSalesOrderID = OM.numOppId
					AND SOLIPL.numSalesOrderItemID = OI.numoppitemtCode
			) TEMPMatchedPO
			OUTER APPLY
			(
				SELECT TOP 1
					OMInner.numOppID
					,OMInner.vcPOppName
					,OIInner.numoppitemtCode
					,OIInner.monPrice
				FROM
					OpportunityLinking OL
				INNER JOIN
					OpportunityMaster OMInner
				ON
					OL.numChildOppID = OMInner.numOppId
				INNER JOIN
					OpportunityItems OIInner
				ON
					OMInner.numOppId = OIInner.numOppId
				WHERE
					OL.numParentOppID = OM.numOppId
					AND OIInner.numItemCode = OI.numItemCode
					AND OIInner.vcAttrValues = OI.vcAttrValues
			) TEMPPO
			Left JOIN 
				Vendor V 
			ON 
				V.numVendorID=I.numVendorID 
				AND V.numItemCode=I.numItemCode
			WHERE
				OBD.numOppId = @numOppID
				AND OBD.numOppBizDocsId = @numOppBizDocID
				AND OBDI.numOppBizDocItemID = @numOppBizDocItemID
				AND I.numItemCode NOT IN (@numDiscountItemID)
		) TEMP
	END

	RETURN CAST(@Value AS DECIMAL(20,5))
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetOrderItemProfitAmountOrMargin')
DROP FUNCTION GetOrderItemProfitAmountOrMargin
GO
CREATE FUNCTION [dbo].[GetOrderItemProfitAmountOrMargin]
(
	@numDomainID NUMERIC(18,0)
	,@numOppID NUMERIC(18,0)
	,@numOppItemID NUMERIC(18,0)
	,@tintMode TINYINT --1: Profit Amount, 2:Profit Margin
)    
RETURNS DECIMAL(20,5)    
AS    
BEGIN  
	DECLARE @Value AS DECIMAL(20,5)

	DECLARE @numDiscountItemID NUMERIC(18,0)
	DECLARE @ProfitCost INT
	SELECT @numDiscountItemID=numDiscountServiceItemID,@ProfitCost=numCost FROM Domain WHERE numDomainId=@numDomainID

	If @tintMode = 1
	BEGIN
		SELECT
			@Value = SUM(ISNULL(monTotAmount,0) - (ISNULL(numUnitHour,0) * (CASE 
																				WHEN ISNULL(TEMPMatchedPO.numoppitemtCode,TEMPPO.numoppitemtCode) IS NOT NULL 
																				THEN ISNULL(ISNULL(TEMPMatchedPO.monPrice,TEMPPO.monPrice),0)
																				ELSE
																					(CASE @ProfitCost 
																						WHEN 3 THEN ISNULL(V.monCost,0) * dbo.fn_UOMConversion(numBaseUnit,I.numItemCode,@numDomainID,numPurchaseUnit) 
																						ELSE ISNULL(OI.monAvgCost,0) 
																					END)
																			END)))
		FROM
			OpportunityItems OI
		INNER JOIN
			OpportunityMaster OM
		ON
			OI.numOppId = OM.numOppID
		INNER JOIN
			Item I
		ON
			OI.numItemCode = I.numItemCode
		OUTER APPLY
		(
			SELECT TOP 1
				OMInner.numOppID
				,OMInner.vcPOppName
				,OIInner.numoppitemtCode
				,OIInner.monPrice
			FROM
				SalesOrderLineItemsPOLinking SOLIPL
			INNER JOIN
				OpportunityMaster OMInner
			ON
				SOLIPL.numPurchaseOrderID = OMInner.numOppId
			INNER JOIN
				OpportunityItems OIInner
			ON
				OMInner.numOppId = OIInner.numOppId
				AND SOLIPL.numPurchaseOrderItemID = OIInner.numoppitemtCode
			WHERE
				SOLIPL.numSalesOrderID = OM.numOppId
				AND SOLIPL.numSalesOrderItemID = OI.numoppitemtCode
		) TEMPMatchedPO
		OUTER APPLY
		(
			SELECT TOP 1
				OMInner.numOppID
				,OMInner.vcPOppName
				,OIInner.numoppitemtCode
				,OIInner.monPrice
			FROM
				OpportunityLinking OL
			INNER JOIN
				OpportunityMaster OMInner
			ON
				OL.numChildOppID = OMInner.numOppId
			INNER JOIN
				OpportunityItems OIInner
			ON
				OMInner.numOppId = OIInner.numOppId
			WHERE
				OL.numParentOppID = OM.numOppId
				AND OIInner.numItemCode = OI.numItemCode
				AND OIInner.vcAttrValues = OI.vcAttrValues
		) TEMPPO
		Left JOIN 
			Vendor V 
		ON 
			V.numVendorID=I.numVendorID 
			AND V.numItemCode=I.numItemCode
		WHERE
			OM.numDomainId=@numDomainID
			AND OM.numOppId = @numOppID
			AND OI.numoppitemtCode = @numOppItemID
			AND I.numItemCode NOT IN (@numDiscountItemID)
	END
	ELSE IF @tintMode = 2
	BEGIN
		SELECT
			@Value = (SUM(Profit)/CASE WHEN SUM(monTotAmount) = 0 THEN 1.0 ELSE SUM(monTotAmount) END) * 100
		FROM
		(
			SELECT
				ISNULL(monTotAmount,0) monTotAmount
				,ISNULL(monTotAmount,0) - (ISNULL(numUnitHour,0) * (CASE 
																		WHEN ISNULL(TEMPMatchedPO.numoppitemtCode,TEMPPO.numoppitemtCode) IS NOT NULL 
																		THEN ISNULL(ISNULL(TEMPMatchedPO.monPrice,TEMPPO.monPrice),0)
																		ELSE (CASE @ProfitCost 
																				WHEN 3 THEN ISNULL(V.monCost,0) * dbo.fn_UOMConversion(numBaseUnit,I.numItemCode,@numDomainID,numPurchaseUnit) 
																				ELSE ISNULL(OI.monAvgCost,0) 
																				END)
																	END)) Profit
			FROM
				OpportunityItems OI
			INNER JOIN
				OpportunityMaster OM
			ON
				OI.numOppId = OM.numOppID
			INNER JOIN
				DivisionMaster DM
			ON
				OM.numDivisionId = DM.numDivisionID
			INNER JOIN
				CompanyInfo CI
			ON
				DM.numCompanyID = CI.numCompanyId
			INNER JOIN
				Item I
			ON
				OI.numItemCode = I.numItemCode
			OUTER APPLY
			(
				SELECT TOP 1
					OMInner.numOppID
					,OMInner.vcPOppName
					,OIInner.numoppitemtCode
					,OIInner.monPrice
				FROM
					SalesOrderLineItemsPOLinking SOLIPL
				INNER JOIN
					OpportunityMaster OMInner
				ON
					SOLIPL.numPurchaseOrderID = OMInner.numOppId
				INNER JOIN
					OpportunityItems OIInner
				ON
					OMInner.numOppId = OIInner.numOppId
					AND SOLIPL.numPurchaseOrderItemID = OIInner.numoppitemtCode
				WHERE
					SOLIPL.numSalesOrderID = OM.numOppId
					AND SOLIPL.numSalesOrderItemID = OI.numoppitemtCode
			) TEMPMatchedPO
			OUTER APPLY
			(
				SELECT TOP 1
					OMInner.numOppID
					,OMInner.vcPOppName
					,OIInner.numoppitemtCode
					,OIInner.monPrice
				FROM
					OpportunityLinking OL
				INNER JOIN
					OpportunityMaster OMInner
				ON
					OL.numChildOppID = OMInner.numOppId
				INNER JOIN
					OpportunityItems OIInner
				ON
					OMInner.numOppId = OIInner.numOppId
				WHERE
					OL.numParentOppID = OM.numOppId
					AND OIInner.numItemCode = OI.numItemCode
					AND OIInner.vcAttrValues = OI.vcAttrValues
			) TEMPPO
			Left JOIN 
				Vendor V 
			ON 
				V.numVendorID=I.numVendorID 
				AND V.numItemCode=I.numItemCode
			WHERE
				OM.numDomainId=@numDomainID
				AND OM.numOppId = @numOppID
				AND OI.numoppitemtCode = @numOppItemID
				AND I.numItemCode NOT IN (@numDiscountItemID)
		) TEMP
	END

	RETURN CAST(@Value AS DECIMAL(20,5))
END
GO
GO
--select dbo.GetDealAmount(1133,getdate(),1610)
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetOrderProfitAmountOrMargin')
DROP FUNCTION GetOrderProfitAmountOrMargin
GO
CREATE FUNCTION [dbo].[GetOrderProfitAmountOrMargin]
(
	@numDomainID NUMERIC(18,0)
	,@numOppID NUMERIC(18,0)
	,@tintMode TINYINT --1: Profit Amount, 2:Profit Margin
)    
RETURNS DECIMAL(20,5)    
AS    
BEGIN   
	DECLARE @Value AS DECIMAL(20,5)
	DECLARE @numShippingItemID NUMERIC(18,0)
	DECLARE @numDiscountItemID NUMERIC(18,0)
	DECLARE @ProfitCost INT
	SELECT @numShippingItemID=numShippingServiceItemID,@numDiscountItemID=numDiscountServiceItemID,@ProfitCost=numCost FROM Domain WHERE numDomainId=@numDomainID

	If @tintMode = 1
	BEGIN
		SELECT
			@Value = SUM(ISNULL(monTotAmount,0) - (ISNULL(numUnitHour,0) * (CASE 
																				WHEN ISNULL(TEMPMatchedPO.numoppitemtCode,TEMPPO.numoppitemtCode) IS NOT NULL 
																				THEN ISNULL(ISNULL(TEMPMatchedPO.monPrice,TEMPPO.monPrice),0)
																				ELSE
																					(CASE @ProfitCost 
																						WHEN 3 THEN ISNULL(V.monCost,0) * dbo.fn_UOMConversion(numBaseUnit,I.numItemCode,@numDomainID,numPurchaseUnit) 
																						ELSE ISNULL(OI.monAvgCost,0) 
																					END)
																			END)))
		FROM
			OpportunityItems OI
		INNER JOIN
			OpportunityMaster OM
		ON
			OI.numOppId = OM.numOppID
		INNER JOIN
			Item I
		ON
			OI.numItemCode = I.numItemCode
		OUTER APPLY
		(
			SELECT TOP 1
				OMInner.numOppID
				,OMInner.vcPOppName
				,OIInner.numoppitemtCode
				,OIInner.monPrice
			FROM
				SalesOrderLineItemsPOLinking SOLIPL
			INNER JOIN
				OpportunityMaster OMInner
			ON
				SOLIPL.numPurchaseOrderID = OMInner.numOppId
			INNER JOIN
				OpportunityItems OIInner
			ON
				OMInner.numOppId = OIInner.numOppId
				AND SOLIPL.numPurchaseOrderItemID = OIInner.numoppitemtCode
			WHERE
				SOLIPL.numSalesOrderID = OM.numOppId
				AND SOLIPL.numSalesOrderItemID = OI.numoppitemtCode
		) TEMPMatchedPO
		OUTER APPLY
		(
			SELECT TOP 1
				OMInner.numOppID
				,OMInner.vcPOppName
				,OIInner.numoppitemtCode
				,OIInner.monPrice
			FROM
				OpportunityLinking OL
			INNER JOIN
				OpportunityMaster OMInner
			ON
				OL.numChildOppID = OMInner.numOppId
			INNER JOIN
				OpportunityItems OIInner
			ON
				OMInner.numOppId = OIInner.numOppId
			WHERE
				OL.numParentOppID = OM.numOppId
				AND OIInner.numItemCode = OI.numItemCode
				AND OIInner.vcAttrValues = OI.vcAttrValues
		) TEMPPO
		Left JOIN 
			Vendor V 
		ON 
			V.numVendorID=I.numVendorID 
			AND V.numItemCode=I.numItemCode
		WHERE
			OM.numDomainId=@numDomainID
			AND OM.numOppId = @numOppID
			AND I.numItemCode NOT IN (@numShippingItemID,@numDiscountItemID)
	END
	ELSE IF @tintMode = 2
	BEGIN
		SELECT
			@Value = (SUM(Profit)/CASE WHEN SUM(monTotAmount) = 0 THEN 1.0 ELSE SUM(monTotAmount) END) * 100
		FROM
		(
			SELECT
				ISNULL(monTotAmount,0) monTotAmount
				,ISNULL(monTotAmount,0) - (ISNULL(numUnitHour,0) * (CASE 
																		WHEN ISNULL(TEMPMatchedPO.numoppitemtCode,TEMPPO.numoppitemtCode) IS NOT NULL 
																		THEN ISNULL(ISNULL(TEMPMatchedPO.monPrice,TEMPPO.monPrice),0)
																		ELSE (CASE @ProfitCost 
																				WHEN 3 THEN ISNULL(V.monCost,0) * dbo.fn_UOMConversion(numBaseUnit,I.numItemCode,@numDomainID,numPurchaseUnit) 
																				ELSE ISNULL(OI.monAvgCost,0) 
																				END)
																	END)) Profit
			FROM
				OpportunityItems OI
			INNER JOIN
				OpportunityMaster OM
			ON
				OI.numOppId = OM.numOppID
			INNER JOIN
				DivisionMaster DM
			ON
				OM.numDivisionId = DM.numDivisionID
			INNER JOIN
				CompanyInfo CI
			ON
				DM.numCompanyID = CI.numCompanyId
			INNER JOIN
				Item I
			ON
				OI.numItemCode = I.numItemCode
			OUTER APPLY
			(
				SELECT TOP 1
					OMInner.numOppID
					,OMInner.vcPOppName
					,OIInner.numoppitemtCode
					,OIInner.monPrice
				FROM
					SalesOrderLineItemsPOLinking SOLIPL
				INNER JOIN
					OpportunityMaster OMInner
				ON
					SOLIPL.numPurchaseOrderID = OMInner.numOppId
				INNER JOIN
					OpportunityItems OIInner
				ON
					OMInner.numOppId = OIInner.numOppId
					AND SOLIPL.numPurchaseOrderItemID = OIInner.numoppitemtCode
				WHERE
					SOLIPL.numSalesOrderID = OM.numOppId
					AND SOLIPL.numSalesOrderItemID = OI.numoppitemtCode
			) TEMPMatchedPO
			OUTER APPLY
			(
				SELECT TOP 1
					OMInner.numOppID
					,OMInner.vcPOppName
					,OIInner.numoppitemtCode
					,OIInner.monPrice
				FROM
					OpportunityLinking OL
				INNER JOIN
					OpportunityMaster OMInner
				ON
					OL.numChildOppID = OMInner.numOppId
				INNER JOIN
					OpportunityItems OIInner
				ON
					OMInner.numOppId = OIInner.numOppId
				WHERE
					OL.numParentOppID = OM.numOppId
					AND OIInner.numItemCode = OI.numItemCode
					AND OIInner.vcAttrValues = OI.vcAttrValues
			) TEMPPO
			Left JOIN 
				Vendor V 
			ON 
				V.numVendorID=I.numVendorID 
				AND V.numItemCode=I.numItemCode
			WHERE
				OM.numDomainId=@numDomainID
				AND OM.numOppId = @numOppID
				AND I.numItemCode NOT IN (@numShippingItemID,@numDiscountItemID)
		) TEMP
	END

	RETURN CAST(@Value AS DECIMAL(20,5)) -- Set Accuracy of Two precision
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetPercentageChangeInGrossProfit')
DROP FUNCTION GetPercentageChangeInGrossProfit
GO
CREATE FUNCTION [dbo].[GetPercentageChangeInGrossProfit]
(
	 @numDomainID AS NUMERIC(18,0),
	 @dtStartDate DATE,
	 @dtEndDate DATE,
	 @tintDateRange TINYINT --1:YTD TO SAME PERIOD LAST YEAR, 2:MTD TO SAME PERIOD LAST YEAR
)    
RETURNS NUMERIC(18,2)
AS    
BEGIN   
	DECLARE @GrossProfitChange NUMERIC(18,2) = 0.00
	DECLARE @GrossProfit1 INT
	DECLARE @GrossProfit2 INT
	DECLARE @numShippingItemID NUMERIC(18,0)
	DECLARE @numDiscountItemID NUMERIC(18,0)
	DECLARE @ProfitCost INT

	SELECT 
		@numShippingItemID=numShippingServiceItemID
		,@numDiscountItemID=numDiscountServiceItemID
		,@ProfitCost=numCost 
	FROM 
		Domain 
	WHERE 
		numDomainId=@numDomainID


	SELECT 
		@GrossProfit1 = SUM(Profit)
	FROM
	(
		SELECT
			ISNULL(monTotAmount,0) - (ISNULL(numUnitHour,0) * (CASE 
																	WHEN ISNULL(TEMPMatchedPO.numoppitemtCode,TEMPPO.numoppitemtCode) IS NOT NULL 
																	THEN ISNULL(ISNULL(TEMPMatchedPO.monPrice,TEMPPO.monPrice),0)
																	ELSE (CASE @ProfitCost 
																			WHEN 3 THEN ISNULL(V.monCost,0) * dbo.fn_UOMConversion(numBaseUnit,I.numItemCode,@numDomainID,numPurchaseUnit) 
																			ELSE monAverageCost 
																		END)
																END)) Profit
		FROM
			OpportunityItems OI
		INNER JOIN
			OpportunityMaster OM
		ON
			OI.numOppId = OM.numOppID
		INNER JOIN
			DivisionMaster DM
		ON
			OM.numDivisionId = DM.numDivisionID
		INNER JOIN
			CompanyInfo CI
		ON
			DM.numCompanyID = CI.numCompanyId
		INNER JOIN
			Item I
		ON
			OI.numItemCode = I.numItemCode
		OUTER APPLY
		(
			SELECT TOP 1
				OMInner.numOppID
				,OMInner.vcPOppName
				,OIInner.numoppitemtCode
				,OIInner.monPrice
			FROM
				SalesOrderLineItemsPOLinking SOLIPL
			INNER JOIN
				OpportunityMaster OMInner
			ON
				SOLIPL.numPurchaseOrderID = OMInner.numOppId
			INNER JOIN
				OpportunityItems OIInner
			ON
				OMInner.numOppId = OIInner.numOppId
				AND SOLIPL.numPurchaseOrderItemID = OIInner.numoppitemtCode
			WHERE
				SOLIPL.numSalesOrderID = OM.numOppId
				AND SOLIPL.numSalesOrderItemID = OI.numoppitemtCode
		) TEMPMatchedPO
		OUTER APPLY
		(
			SELECT TOP 1
				OMInner.numOppID
				,OMInner.vcPOppName
				,OIInner.numoppitemtCode
				,OIInner.monPrice
			FROM
				OpportunityLinking OL
			INNER JOIN
				OpportunityMaster OMInner
			ON
				OL.numChildOppID = OMInner.numOppId
			INNER JOIN
				OpportunityItems OIInner
			ON
				OMInner.numOppId = OIInner.numOppId
			WHERE
				OL.numParentOppID = OM.numOppId
				AND OIInner.numItemCode = OI.numItemCode
				AND OIInner.vcAttrValues = OI.vcAttrValues
		) TEMPPO
		Left JOIN 
			Vendor V 
		ON 
			V.numVendorID=I.numVendorID 
			AND V.numItemCode=I.numItemCode
		WHERE
			OM.numDomainId=@numDomainID
			AND OM.bintCreatedDate BETWEEN  @dtStartDate AND @dtEndDate
			AND ISNULL(OM.tintOppType,0)=1
			AND ISNULL(OM.tintOppStatus,0)=1
			AND I.numItemCode NOT IN (@numShippingItemID,@numDiscountItemID)
	) TEMP

	SELECT 
		@GrossProfit2 = SUM(Profit)
	FROM
	(
		SELECT
			ISNULL(monTotAmount,0) - (ISNULL(numUnitHour,0) * (CASE 
																	WHEN ISNULL(TEMPMatchedPO.numoppitemtCode,TEMPPO.numoppitemtCode) IS NOT NULL 
																	THEN ISNULL(ISNULL(TEMPMatchedPO.monPrice,TEMPPO.monPrice),0)
																	ELSE (CASE @ProfitCost 
																			WHEN 3 THEN ISNULL(V.monCost,0) * dbo.fn_UOMConversion(numBaseUnit,I.numItemCode,@numDomainID,numPurchaseUnit) 
																			ELSE ISNULL(OI.monAvgCost,0) 
																		END)
																END)) Profit
		FROM
			OpportunityItems OI
		INNER JOIN
			OpportunityMaster OM
		ON
			OI.numOppId = OM.numOppID
		INNER JOIN
			DivisionMaster DM
		ON
			OM.numDivisionId = DM.numDivisionID
		INNER JOIN
			CompanyInfo CI
		ON
			DM.numCompanyID = CI.numCompanyId
		INNER JOIN
			Item I
		ON
			OI.numItemCode = I.numItemCode
		OUTER APPLY
		(
			SELECT TOP 1
				OMInner.numOppID
				,OMInner.vcPOppName
				,OIInner.numoppitemtCode
				,OIInner.monPrice
			FROM
				SalesOrderLineItemsPOLinking SOLIPL
			INNER JOIN
				OpportunityMaster OMInner
			ON
				SOLIPL.numPurchaseOrderID = OMInner.numOppId
			INNER JOIN
				OpportunityItems OIInner
			ON
				OMInner.numOppId = OIInner.numOppId
				AND SOLIPL.numPurchaseOrderItemID = OIInner.numoppitemtCode
			WHERE
				SOLIPL.numSalesOrderID = OM.numOppId
				AND SOLIPL.numSalesOrderItemID = OI.numoppitemtCode
		) TEMPMatchedPO
		OUTER APPLY
		(
			SELECT TOP 1
				OMInner.numOppID
				,OMInner.vcPOppName
				,OIInner.numoppitemtCode
				,OIInner.monPrice
			FROM
				OpportunityLinking OL
			INNER JOIN
				OpportunityMaster OMInner
			ON
				OL.numChildOppID = OMInner.numOppId
			INNER JOIN
				OpportunityItems OIInner
			ON
				OMInner.numOppId = OIInner.numOppId
			WHERE
				OL.numParentOppID = OM.numOppId
				AND OIInner.numItemCode = OI.numItemCode
				AND OIInner.vcAttrValues = OI.vcAttrValues
		) TEMPPO
		Left JOIN 
			Vendor V 
		ON 
			V.numVendorID=I.numVendorID 
			AND V.numItemCode=I.numItemCode
		WHERE
			OM.numDomainId=@numDomainID
			AND 1 = (CASE 
						WHEN @tintDateRange=1 
						THEN (CASE WHEN OM.bintCreatedDate BETWEEN DATEADD(YEAR,-1,@dtStartDate) AND DATEADD(YEAR,-1,@dtEndDate) THEN 1 ELSE 0 END)
						WHEN @tintDateRange=2 
						THEN (CASE WHEN OM.bintCreatedDate BETWEEN DATEADD(MONTH,-1,@dtStartDate) AND DATEADD(MONTH,-1,@dtEndDate) THEN 1 ELSE 0 END)
						ELSE 0
					END)
			AND ISNULL(OM.tintOppType,0)=1
			AND ISNULL(OM.tintOppStatus,0)=1
			AND I.numItemCode NOT IN (@numShippingItemID,@numDiscountItemID)
	) TEMP


	SET @GrossProfitChange = 100.0 * (ISNULL(@GrossProfit1,0) - ISNULL(@GrossProfit2,0)) / (CASE WHEN ISNULL(@GrossProfit2,0) = 0 THEN 1 ELSE ISNULL(@GrossProfit2,0) END)


	RETURN ISNULL(@GrossProfitChange,0.00)
END
GO
GO
IF EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[StagePercentageDetailsTaskTimeLog_Delete]'))
	DROP TRIGGER [dbo].[StagePercentageDetailsTaskTimeLog_Delete]
GO

CREATE TRIGGER dbo.StagePercentageDetailsTaskTimeLog_Delete ON dbo.StagePercentageDetailsTaskTimeLog AFTER DELETE
AS 
BEGIN
	IF EXISTS (SELECT 
					CL.numContractId  
				FROM 
					ContractsLog CL 
				INNER JOIN
					Contracts C
				ON
					CL.numContractId = C.numContractId
				INNER JOIN	
					deleted
				ON
					C.numDomainID = deleted.numDomainID
					AND CL.numReferenceId = deleted.numTaskID
					AND vcDetails='1')
	BEGIN
		DECLARE @numContractsLogId NUMERIC(18,0)
		DECLARE @numContactID NUMERIC(18,0)
		DECLARE @numUsedTime INT

		SELECT 
			@numContractsLogId=numContractsLogId
			,@numContactID = CL.numContractId
			,@numUsedTime = numUsedTime
		FROM
			ContractsLog CL 
		INNER JOIN
			Contracts C
		ON
			CL.numContractId = C.numContractId
		INNER JOIN	
			deleted
		ON
			C.numDomainId = deleted.numDomainID
			AND CL.numReferenceId = deleted.numTaskID
			AND vcDetails='1'

		UPDATE Contracts SET timeUsed=ISNULL(timeUsed,0)-ISNULL(@numUsedTime,0),dtmModifiedOn=GETUTCDATE() WHERE numContractId=@numContactID
		DELETE FROM ContractsLog WHERE numContractsLogId=@numContractsLogId
	END
END
GO
GO
IF EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[StagePercentageDetailsTaskTimeLog_Update]'))
	DROP TRIGGER [dbo].[StagePercentageDetailsTaskTimeLog_Update]
GO

CREATE TRIGGER dbo.StagePercentageDetailsTaskTimeLog_Update ON dbo.StagePercentageDetailsTaskTimeLog AFTER UPDATE
AS 
BEGIN
	IF EXISTS (SELECT 
					CL.numContractId  
				FROM 
					ContractsLog CL 
				INNER JOIN
					Contracts C
				ON
					CL.numContractId = C.numContractId
				INNER JOIN	
					INSERTED
				ON
					C.numDomainID = inserted.numDomainID
					AND CL.numReferenceId = inserted.numTaskID
					AND vcDetails='1')
	BEGIN
		DECLARE @numDomainID NUMERIC(18,0)
		DECLARE @numContractsLogId NUMERIC(18,0)
		DECLARE @numContactID NUMERIC(18,0)
		DECLARE @numUsedTime INT
		DECLARE @numTaskID NUMERIC(18,0)

		SELECT 
			@numDomainID=inserted.numDomainID
			,@numContractsLogId=numContractsLogId
			,@numContactID = CL.numContractId
			,@numUsedTime = numUsedTime
			,@numTaskID = inserted.numTaskID
		FROM
			ContractsLog CL 
		INNER JOIN
			Contracts C
		ON
			CL.numContractId = C.numContractId
		INNER JOIN	
			INSERTED
		ON
			C.numDomainID = inserted.numDomainID
			AND CL.numReferenceId = inserted.numTaskID
			AND vcDetails='1'

		
		IF ISNULL((SELECT TOP 1 tintAction FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=@numTaskID ORDER BY ID DESC),0) = 4
		BEGIN
			UPDATE Contracts SET timeUsed=ISNULL(timeUsed,0)-ISNULL(@numUsedTime,0),dtmModifiedOn=GETUTCDATE() WHERE numContractId=@numContactID

			-- GET UPDATED TIME
			SET @numUsedTime = CAST(dbo.GetTimeSpendOnTaskByProject(@numDomainID,@numTaskID,2) AS INT)
			
			UPDATE ContractsLog SET numUsedTime=(@numUsedTime * 60) WHERE numContractsLogId=@numContractsLogId	
		END
		ELSE
		BEGIN
			UPDATE Contracts SET timeUsed=ISNULL(timeUsed,0)-ISNULL(@numUsedTime,0),dtmModifiedOn=GETUTCDATE() WHERE numContractId=@numContactID
			DELETE FROM ContractsLog WHERE numContractsLogId=@numContractsLogId
		END
	END
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CalculateCommission')
DROP PROCEDURE USP_CalculateCommission
GO
CREATE PROCEDURE [dbo].[USP_CalculateCommission]
	-- Add the parameters for the stored procedure here
	 @numComPayPeriodID AS NUMERIC(18,0),
	 @numDomainID AS NUMERIC(18,0),
	 @ClientTimeZoneOffset INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
SET NOCOUNT ON;

BEGIN TRY
BEGIN TRANSACTION
	DECLARE @dtPayStart DATE
	DECLARE @dtPayEnd DATE

	DECLARE @TempBizCommission TABLE
	(
		numUserCntID NUMERIC(18,0),
		numOppID NUMERIC(18,0),
		numOppBizDocID NUMERIC(18,0),
		numOppBizDocItemID NUMERIC(18,0),
		monCommission DECIMAL(20,5),
		numComRuleID NUMERIC(18,0),
		tintComType TINYINT,
		tintComBasedOn TINYINT,
		decCommission FLOAT,
		tintAssignTo TINYINT,
		numOppItemID NUMERIC(18,0),
		monInvoiceSubTotal DECIMAL(20,5),
		monOrderSubTotal DECIMAL(20,5)
	)

	DECLARE @TABLEPAID TABLE
	(
		ID INT IDENTITY(1,1),
		numDivisionID NUMERIC(18,0),
		numRelationship NUMERIC(18,0),
		numProfile NUMERIC(18,0),
		numAssignedTo NUMERIC(18,0),
		numRecordOwner NUMERIC(18,0),
		numItemCode NUMERIC(18,0),
		numItemClassification NUMERIC(18,0),
		numOppID NUMERIC(18,0),
		numOppItemID NUMERIC(18,0),
		numOppBizDocID NUMERIC(18,0),
		numOppBizDocItemID NUMERIC(18,0),
		numUnitHour FLOAT,
		monTotAmount DECIMAL(20,5),
		monInvoiceSubTotal DECIMAL(20,5),
		monOrderSubTotal DECIMAL(20,5),
		monVendorCost DECIMAL(20,5),
		monAvgCost DECIMAL(20,5),
		numPartner NUMERIC(18,0),
		numPOID NUMERIC(18,0),
		numPOItemID NUMERIC(18,0),
		monPOCost DECIMAL(20,5)
	)

	SELECT 
		@dtPayStart=dtStart
		,@dtPayEnd=dtEnd 
	FROM 
		CommissionPayPeriod 
	WHERE 
		numComPayPeriodID = @numComPayPeriodID

	--GET COMMISSION GLOBAL SETTINGS
	DECLARE @numDiscountServiceItemID NUMERIC(18,0)
	DECLARE @numShippingServiceItemID NUMERIC(18,0)
	DECLARE @tintCommissionType TINYINT
	DECLARE @bitCommissionBasedOn BIT
	DECLARE @tintCommissionBasedOn TINYINT
	DECLARE @bitIncludeTaxAndShippingInCommission BIT

	SELECT 
		@numDiscountServiceItemID=numDiscountServiceItemID, 
		@numShippingServiceItemID = numShippingServiceItemID, 
		@tintCommissionType=tintCommissionType, 
		@bitIncludeTaxAndShippingInCommission=bitIncludeTaxAndShippingInCommission
		,@tintCommissionBasedOn=ISNULL(tintCommissionBasedOn,1)
		,@bitCommissionBasedOn=ISNULL(bitCommissionBasedOn,0)
	FROM 
		Domain 
	WHERE 
		numDomainID=@numDomainID

	DELETE 
		BC 
	FROM 
		BizDocComission BC
	LEFT JOIN
		OpportunityMaster OM
	ON
		BC.numOppID = OM.numOppId
	WHERE 
		BC.numDomainId=@numDomainID
		AND ISNULL(BC.numOppID,0) > 0
		AND ISNULL(BC.bitCommisionPaid,0)=0
		AND OM.numOppId IS NULL

	DELETE 
		BC 
	FROM 
		BizDocComission BC
	LEFT JOIN
		OpportunityItems OI
	ON
		BC.numOppItemID = OI.numoppitemtCode
	WHERE 
		BC.numDomainId=@numDomainID 
		AND ISNULL(BC.numOppItemID,0) > 0
		AND ISNULL(BC.bitCommisionPaid,0)=0
		AND OI.numoppitemtCode IS NULL

	DELETE 
		BC 
	FROM 
		BizDocComission BC
	LEFT JOIN
		OpportunityBizDocs OBD
	ON
		BC.numOppBizDocId = OBD.numOppBizDocsId
	WHERE 
		BC.numDomainId=@numDomainID
		AND ISNULL(BC.numOppBizDocId,0) > 0
		AND ISNULL(BC.bitCommisionPaid,0)=0
		AND OBD.numOppBizDocsId IS NULL

	DELETE 
		BC 
	FROM 
		BizDocComission BC
	LEFT JOIN
		OpportunityBizDocItems OBDI
	ON
		BC.numOppBizDocItemID = OBDI.numOppBizDocItemID
	WHERE 
		BC.numDomainId=@numDomainID 
		AND ISNULL(BC.numOppBizDocItemID,0) > 0
		AND ISNULL(BC.bitCommisionPaid,0)=0
		AND OBDI.numOppBizDocItemID IS NULL

	-- DELETE UNPAID COMMISSION ENTRIES BETWEEN PAY PERIOD DATE RANGE	
	IF @tintCommissionType = 3 --Sales Order Sub-Total Amount
	BEGIN
		DELETE FROM 
			BizDocComission 
		WHERE 
			numDomainId=@numDomainID 
			AND ISNULL(bitCommisionPaid,0)=0 
			AND numOppItemID IN (SELECT 
									OI.numoppitemtCode
								FROM 
									OpportunityMaster OM
								INNER JOIN
									OpportunityItems OI
								ON
									OM.numOppId = OI.numOppId
								WHERE
									OM.numDomainId = @numDomainID 
									AND OM.tintOppType = 1
									AND OM.tintOppStatus = 1
									AND CAST(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,OM.bintCreatedDate) AS DATE) BETWEEN @dtPayStart AND @dtPayEnd)

		INSERT INTO 
			@TABLEPAID
		SELECT 
			OM.numDivisionID,
			ISNULL(CompanyInfo.numCompanyType,0),
			ISNULL(CompanyInfo.vcProfile,0),
			OM.numAssignedTo,
			OM.numRecOwner,
			OI.numItemCode,
			Item.numItemClassification,
			OM.numOppID,
			OI.numoppitemtCode,
			0,
			0,
			ISNULL(OI.numUnitHour,0),
			(CASE WHEN ISNULL(OM.fltExchangeRate,0)=0 OR ISNULL(OM.fltExchangeRate,0)=1 THEN ISNULL(OI.monTotAmount,0) ELSE ROUND(ISNULL(OI.monTotAmount,0) * OM.fltExchangeRate,2) END),
			0,
			(CASE WHEN ISNULL(OM.fltExchangeRate,0)=0 OR ISNULL(OM.fltExchangeRate,0)=1 THEN ISNULL(OI.monTotAmount,0) ELSE ROUND(ISNULL(OI.monTotAmount,0) * OM.fltExchangeRate,2) END),
			(ISNULL(OI.monVendorCost,0) * dbo.fn_UOMConversion(Item.numPurchaseUnit,Item.numItemCode,@numDomainID,Item.numBaseUnit) * ISNULL(OI.numUnitHour,0)),
			(ISNULL(OI.monAvgCost,0) * ISNULL(OI.numUnitHour,0)),
			OM.numPartner,
			ISNULL(TEMPMatchedPO.numOppId,TEMPPO.numOppId),
			ISNULL(TEMPMatchedPO.numoppitemtCode,TEMPPO.numoppitemtCode),
			ISNULL(ISNULL(TEMPMatchedPO.monPrice,TEMPPO.monPrice),0)
		FROM 
			OpportunityMaster OM
		INNER JOIN
			DivisionMaster 
		ON
			OM.numDivisionID = DivisionMaster.numDivisionID
		INNER JOIN
			CompanyInfo
		ON
			DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
		INNER JOIN
			OpportunityItems OI 
		ON
			OM.numOppId = OI.numOppId
		INNER JOIN
			Item
		ON
			OI.numItemCode = Item.numItemCode
		OUTER APPLY
		(
			SELECT TOP 1
				OMInner.numOppID
				,OMInner.vcPOppName
				,OIInner.numoppitemtCode
				,OIInner.monPrice
			FROM
				SalesOrderLineItemsPOLinking SOLIPL
			INNER JOIN
				OpportunityMaster OMInner
			ON
				SOLIPL.numPurchaseOrderID = OMInner.numOppId
			INNER JOIN
				OpportunityItems OIInner
			ON
				OMInner.numOppId = OIInner.numOppId
				AND SOLIPL.numPurchaseOrderItemID = OIInner.numoppitemtCode
			WHERE
				SOLIPL.numSalesOrderID = OM.numOppId
				AND SOLIPL.numSalesOrderItemID = OI.numoppitemtCode
		) TEMPMatchedPO
		OUTER APPLY
		(
			SELECT TOP 1
				OMInner.numOppID
				,OMInner.vcPOppName
				,OIInner.numoppitemtCode
				,OIInner.monPrice
			FROM
				OpportunityLinking OL
			INNER JOIN
				OpportunityMaster OMInner
			ON
				OL.numChildOppID = OMInner.numOppId
			INNER JOIN
				OpportunityItems OIInner
			ON
				OMInner.numOppId = OIInner.numOppId
			WHERE
				OL.numParentOppID = OM.numOppId
				AND OIInner.numItemCode = OI.numItemCode
				AND OIInner.vcAttrValues = OI.vcAttrValues
		) TEMPPO
		LEFT JOIN
			BizDocComission
		ON
			OM.numOppID = BizDocComission.numOppID
			AND OI.numoppitemtCode = BizDocComission.numOppItemID
			AND ISNULL(bitCommisionPaid,0) = 1
		WHERE
			OM.numDomainId = @numDomainID 
			AND OM.tintOppType = 1
			AND OM.tintOppStatus = 1
			AND CAST(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,OM.bintCreatedDate) AS DATE) BETWEEN @dtPayStart AND @dtPayEnd
			AND BizDocComission.numComissionID IS NULL
			AND OI.numItemCode NOT IN ( @numDiscountServiceItemID )  /*DO NOT INCLUDE DISCOUNT ITEM*/
			AND 1 = (
						CASE 
							WHEN OI.numItemCode = @numShippingServiceItemID 
							THEN 
								CASE 
									WHEN ISNULL(@bitIncludeTaxAndShippingInCommission,0) = 1
									THEN	
										1
									ELSE	
										0
								END
							ELSE 
								1 
						END
					)/*CONDITION TO INCLUDE/EXCLUDE INCLUDE SHIPPING ITEM */
	END
	ELSE IF @tintCommissionType = 2 --Invoice Sub-Total Amount (Paid or Unpaid)
	BEGIN
		DELETE FROM 
			BizDocComission 
		WHERE 
			numDomainId=@numDomainID
			AND ISNULL(numOppBizDocID,0) = 0
			AND ISNULL(bitCommisionPaid,0)=0 
			AND numOppItemID IN (SELECT 
									OI.numoppitemtCode
								FROM 
									OpportunityMaster OM
								INNER JOIN
									OpportunityItems OI
								ON
									OM.numOppId = OI.numOppId
								WHERE
									OM.numDomainId = @numDomainID 
									AND OM.tintOppType = 1
									AND OM.tintOppStatus = 1
									AND CAST(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,OM.bintCreatedDate) AS DATE) BETWEEN @dtPayStart AND @dtPayEnd)

		DELETE FROM 
			BizDocComission 
		WHERE 
			numDomainId=@numDomainID 
			AND ISNULL(bitCommisionPaid,0)=0 
			AND numOppBizDocItemID IN (SELECT 
											ISNULL(numOppBizDocItemID,0)
										FROM 
											OpportunityBizDocs OppBiz
										INNER JOIN
											OpportunityMaster OppM
										ON
											OppBiz.numOppID = OppM.numOppID
										INNER JOIN
											OpportunityBizDocItems OppBizItems
										ON
											OppBiz.numOppBizDocsId = OppBizItems.numOppBizDocID
										OUTER APPLY
										(
											SELECT 
												MAX(DM.dtDepositDate) dtDepositDate
											FROM 
												DepositMaster DM 
											JOIN 
												dbo.DepositeDetails DD
											ON 
												DM.numDepositId=DD.numDepositID 
											WHERE 
												DM.tintDepositePage=2 
												AND DM.numDomainId=@numDomainId
												AND DD.numOppID=OppBiz.numOppID 
												AND DD.numOppBizDocsID =OppBiz.numOppBizDocsID
										) TempDepositMaster
										WHERE
											OppM.numDomainId = @numDomainID 
											AND OppM.tintOppType = 1
											AND OppM.tintOppStatus = 1
											AND OppBiz.bitAuthoritativeBizDocs = 1
											AND ((TempDepositMaster.dtDepositDate IS NOT NULL AND (CAST(TempDepositMaster.dtDepositDate AS DATE) BETWEEN @dtPayStart AND @dtPayEnd)) OR CAST(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,OppBiz.dtCreatedDate) AS DATE) BETWEEN @dtPayStart AND @dtPayEnd))

		INSERT INTO 
			@TABLEPAID
		SELECT 
			OM.numDivisionID,
			ISNULL(CompanyInfo.numCompanyType,0),
			ISNULL(CompanyInfo.vcProfile,0),
			OM.numAssignedTo,
			OM.numRecOwner,
			OppBizItems.numItemCode,
			Item.numItemClassification,
			OM.numOppID,
			OppMItems.numoppitemtCode,
			OppBizItems.numOppBizDocID,
			OppBizItems.numOppBizDocItemID,
			ISNULL(OppBizItems.numUnitHour,0),
			(CASE WHEN ISNULL(OM.fltExchangeRate,0)=0 OR ISNULL(OM.fltExchangeRate,0)=1 THEN ISNULL(OppBizItems.monTotAmount,0) ELSE ROUND(ISNULL(OppBizItems.monTotAmount,0) * OM.fltExchangeRate,2) END),
			(CASE WHEN ISNULL(OM.fltExchangeRate,0)=0 OR ISNULL(OM.fltExchangeRate,0)=1 THEN ISNULL(OppBizItems.monTotAmount,0) ELSE ROUND(ISNULL(OppBizItems.monTotAmount,0) * OM.fltExchangeRate,2) END),
			(CASE WHEN ISNULL(OM.fltExchangeRate,0)=0 OR ISNULL(OM.fltExchangeRate,0)=1 THEN ISNULL(OppMItems.monTotAmount,0) ELSE ROUND(ISNULL(OppMItems.monTotAmount,0) * OM.fltExchangeRate,2) END),
			(ISNULL(OppMItems.monVendorCost,0) * dbo.fn_UOMConversion(Item.numPurchaseUnit,Item.numItemCode,@numDomainID,Item.numBaseUnit) * ISNULL(OppBizItems.numUnitHour,0)),
			(ISNULL(OppMItems.monAvgCost,0) * ISNULL(OppBizItems.numUnitHour,0)),
			OM.numPartner,
			ISNULL(TEMPMatchedPO.numOppId,TEMPPO.numOppId),
			ISNULL(TEMPMatchedPO.numoppitemtCode,TEMPPO.numoppitemtCode),
			ISNULL(ISNULL(TEMPMatchedPO.monPrice,TEMPPO.monPrice),0)
		FROM 
			OpportunityMaster OM
		INNER JOIN	
			OpportunityBizDocs OBD
		ON
			OM.numOppId = OBD.numOppId
		INNER JOIN
			DivisionMaster 
		ON
			OM.numDivisionID = DivisionMaster.numDivisionID
		INNER JOIN
			CompanyInfo
		ON
			DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
		INNER JOIN
			OpportunityBizDocItems OppBizItems
		ON
			OBD.numOppBizDocsId = OppBizItems.numOppBizDocID
		INNER JOIN
			OpportunityItems OppMItems 
		ON
			OppBizItems.numOppItemID = OppMItems.numoppitemtCode
		INNER JOIN
			Item
		ON
			OppBizItems.numItemCode = Item.numItemCode
		OUTER APPLY
		(
			SELECT TOP 1
				OMInner.numOppID
				,OMInner.vcPOppName
				,OIInner.numoppitemtCode
				,OIInner.monPrice
			FROM
				SalesOrderLineItemsPOLinking SOLIPL
			INNER JOIN
				OpportunityMaster OMInner
			ON
				SOLIPL.numPurchaseOrderID = OMInner.numOppId
			INNER JOIN
				OpportunityItems OIInner
			ON
				OMInner.numOppId = OIInner.numOppId
				AND SOLIPL.numPurchaseOrderItemID = OIInner.numoppitemtCode
			WHERE
				SOLIPL.numSalesOrderID = OM.numOppId
				AND SOLIPL.numSalesOrderItemID = OppMItems.numoppitemtCode
		) TEMPMatchedPO
		OUTER APPLY
		(
			SELECT TOP 1
				OMInner.numOppID
				,OMInner.vcPOppName
				,OIInner.numoppitemtCode
				,OIInner.monPrice
			FROM
				OpportunityLinking OL
			INNER JOIN
				OpportunityMaster OMInner
			ON
				OL.numChildOppID = OMInner.numOppId
			INNER JOIN
				OpportunityItems OIInner
			ON
				OMInner.numOppId = OIInner.numOppId
			WHERE
				OL.numParentOppID = OM.numOppId
				AND OIInner.numItemCode = OppMItems.numItemCode
				AND OIInner.vcAttrValues = OppMItems.vcAttrValues
		) TEMPPO
		LEFT JOIN
			BizDocComission BDC1
		ON
			OM.numOppID = BDC1.numOppID
			AND OppMItems.numoppitemtCode = BDC1.numOppItemID
			AND ISNULL(bitCommisionPaid,0) = 1
		LEFT JOIN
			BizDocComission BDC2
		ON
			OM.numOppID = BDC2.numOppID
			AND OppMItems.numoppitemtCode = BDC2.numOppItemID
			AND ISNULL(BDC2.bitCommisionPaid,0) = 1
			AND OppBizItems.numOppBizDocID = BDC2.numOppBizDocId 
			AND OppBizItems.numOppBizDocItemID =  BDC2.numOppBizDocItemID
		WHERE
			OM.numDomainId = @numDomainID 
			AND OM.tintOppType = 1
			AND OM.tintOppStatus = 1
			AND OBD.bitAuthoritativeBizDocs = 1
			AND CAST(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,OBD.dtCreatedDate) AS DATE) BETWEEN @dtPayStart AND @dtPayEnd
			AND OppBizItems.numItemCode NOT IN ( @numDiscountServiceItemID )  /*DO NOT INCLUDE DISCOUNT ITEM*/
			AND BDC1.numComissionID IS NULL
			AND BDC2.numComissionID IS NULL
			AND 1 = (
						CASE 
							WHEN OppBizItems.numItemCode = @numShippingServiceItemID 
							THEN 
								CASE 
									WHEN ISNULL(@bitIncludeTaxAndShippingInCommission,0) = 1
									THEN	
										1
									ELSE	
										0
								END
							ELSE 
								1 
						END
					)/*CONDITION TO INCLUDE/EXCLUDE INCLUDE SHIPPING ITEM */
	END
	ELSE
	BEGIN
		DELETE FROM 
			BizDocComission 
		WHERE 
			numDomainId=@numDomainID
			AND ISNULL(numOppBizDocID,0) = 0
			AND ISNULL(bitCommisionPaid,0)=0 
			AND numOppItemID IN (SELECT 
									OI.numoppitemtCode
								FROM 
									OpportunityMaster OM
								INNER JOIN
									OpportunityItems OI
								ON
									OM.numOppId = OI.numOppId
								WHERE
									OM.numDomainId = @numDomainID 
									AND OM.tintOppType = 1
									AND OM.tintOppStatus = 1
									AND CAST(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,OM.bintCreatedDate) AS DATE) BETWEEN @dtPayStart AND @dtPayEnd)

		DELETE FROM 
			BizDocComission 
		WHERE 
			numDomainId=@numDomainID 
			AND ISNULL(bitCommisionPaid,0)=0 
			AND numOppBizDocItemID IN (SELECT 
											ISNULL(numOppBizDocItemID,0)
										FROM 
											OpportunityBizDocs OppBiz
										INNER JOIN
											OpportunityMaster OppM
										ON
											OppBiz.numOppID = OppM.numOppID
										INNER JOIN
											OpportunityBizDocItems OppBizItems
										ON
											OppBiz.numOppBizDocsId = OppBizItems.numOppBizDocID
										OUTER APPLY
										(
											SELECT 
												MAX(DM.dtDepositDate) dtDepositDate
											FROM 
												DepositMaster DM 
											JOIN 
												dbo.DepositeDetails DD
											ON 
												DM.numDepositId=DD.numDepositID 
											WHERE 
												DM.tintDepositePage=2 
												AND DM.numDomainId=@numDomainId
												AND DD.numOppID=OppBiz.numOppID 
												AND DD.numOppBizDocsID =OppBiz.numOppBizDocsID
										) TempDepositMaster
										WHERE
											OppM.numDomainId = @numDomainID 
											AND OppM.tintOppType = 1
											AND OppM.tintOppStatus = 1
											AND OppBiz.bitAuthoritativeBizDocs = 1
											AND ((TempDepositMaster.dtDepositDate IS NOT NULL AND (CAST(TempDepositMaster.dtDepositDate AS DATE) BETWEEN @dtPayStart AND @dtPayEnd)) OR CAST(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,OppBiz.dtCreatedDate) AS DATE) BETWEEN @dtPayStart AND @dtPayEnd))

		INSERT INTO 
			@TABLEPAID
		SELECT 
			OppM.numDivisionID,
			ISNULL(CompanyInfo.numCompanyType,0),
			ISNULL(CompanyInfo.vcProfile,0),
			OppM.numAssignedTo,
			OppM.numRecOwner,
			OppBizItems.numItemCode,
			Item.numItemClassification,
			OppM.numOppID,
			OppMItems.numoppitemtCode,
			OppBizItems.numOppBizDocID,
			OppBizItems.numOppBizDocItemID,
			ISNULL(OppBizItems.numUnitHour,0),
			(CASE WHEN ISNULL(OppM.fltExchangeRate,0)=0 OR ISNULL(OppM.fltExchangeRate,0)=1 THEN ISNULL(OppBizItems.monTotAmount,0) ELSE ROUND(ISNULL(OppBizItems.monTotAmount,0) * OppM.fltExchangeRate,2) END),
			(CASE WHEN ISNULL(OppM.fltExchangeRate,0)=0 OR ISNULL(OppM.fltExchangeRate,0)=1 THEN ISNULL(OppBizItems.monTotAmount,0) ELSE ROUND(ISNULL(OppBizItems.monTotAmount,0) * OppM.fltExchangeRate,2) END),
			(CASE WHEN ISNULL(OppM.fltExchangeRate,0)=0 OR ISNULL(OppM.fltExchangeRate,0)=1 THEN ISNULL(OppMItems.monTotAmount,0) ELSE ROUND(ISNULL(OppMItems.monTotAmount,0) * OppM.fltExchangeRate,2) END),
			(ISNULL(OppMItems.monVendorCost,0) * dbo.fn_UOMConversion(Item.numPurchaseUnit,Item.numItemCode,@numDomainID,Item.numBaseUnit) * ISNULL(OppBizItems.numUnitHour,0)),
			(ISNULL(OppMItems.monAvgCost,0) * ISNULL(OppBizItems.numUnitHour,0)),
			OppM.numPartner,
			ISNULL(TEMPMatchedPO.numOppId,TEMPPO.numOppId),
			ISNULL(TEMPMatchedPO.numoppitemtCode,TEMPPO.numoppitemtCode),
			ISNULL(ISNULL(TEMPMatchedPO.monPrice,TEMPPO.monPrice),0)
		FROM 
			OpportunityBizDocs OppBiz
		INNER JOIN
			OpportunityMaster OppM
		ON
			OppBiz.numOppID = OppM.numOppID
		INNER JOIN
			DivisionMaster 
		ON
			OppM.numDivisionID = DivisionMaster.numDivisionID
		INNER JOIN
			CompanyInfo
		ON
			DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
		INNER JOIN
			OpportunityBizDocItems OppBizItems
		ON
			OppBiz.numOppBizDocsId = OppBizItems.numOppBizDocID
		INNER JOIN
			OpportunityItems OppMItems 
		ON
			OppBizItems.numOppItemID = OppMItems.numoppitemtCode
		INNER JOIN
			Item
		ON
			OppBizItems.numItemCode = Item.numItemCode
		OUTER APPLY
		(
			SELECT TOP 1
				OMInner.numOppID
				,OMInner.vcPOppName
				,OIInner.numoppitemtCode
				,OIInner.monPrice
			FROM
				SalesOrderLineItemsPOLinking SOLIPL
			INNER JOIN
				OpportunityMaster OMInner
			ON
				SOLIPL.numPurchaseOrderID = OMInner.numOppId
			INNER JOIN
				OpportunityItems OIInner
			ON
				OMInner.numOppId = OIInner.numOppId
				AND SOLIPL.numPurchaseOrderItemID = OIInner.numoppitemtCode
			WHERE
				SOLIPL.numSalesOrderID = OppM.numOppId
				AND SOLIPL.numSalesOrderItemID = OppMItems.numoppitemtCode
		) TEMPMatchedPO
		OUTER APPLY
		(
			SELECT TOP 1
				OMInner.numOppID
				,OMInner.vcPOppName
				,OIInner.numoppitemtCode
				,OIInner.monPrice
			FROM
				OpportunityLinking OL
			INNER JOIN
				OpportunityMaster OMInner
			ON
				OL.numChildOppID = OMInner.numOppId
			INNER JOIN
				OpportunityItems OIInner
			ON
				OMInner.numOppId = OIInner.numOppId
			WHERE
				OL.numParentOppID = OppM.numOppId
				AND OIInner.numItemCode = OppMItems.numItemCode
				AND OIInner.vcAttrValues = OppMItems.vcAttrValues
		) TEMPPO
		LEFT JOIN
			BizDocComission BDC1
		ON
			OppM.numOppID = BDC1.numOppID
			AND OppMItems.numoppitemtCode = BDC1.numOppItemID
			AND ISNULL(bitCommisionPaid,0) = 1
		LEFT JOIN
			BizDocComission BDC2
		ON
			OppM.numOppID = BDC2.numOppID
			AND OppMItems.numoppitemtCode = BDC2.numOppItemID
			AND ISNULL(BDC2.bitCommisionPaid,0) = 1
			AND OppBizItems.numOppBizDocID = BDC2.numOppBizDocId 
			AND OppBizItems.numOppBizDocItemID =  BDC2.numOppBizDocItemID
		OUTER APPLY
		(
			SELECT 
				MAX(DM.dtDepositDate) dtDepositDate
			FROM 
				DepositMaster DM 
			JOIN 
				dbo.DepositeDetails DD
			ON 
				DM.numDepositId=DD.numDepositID 
			WHERE 
				DM.tintDepositePage=2 
				AND DM.numDomainId=@numDomainId
				AND DD.numOppID=OppBiz.numOppID 
				AND DD.numOppBizDocsID =OppBiz.numOppBizDocsID
		) TempDepositMaster
		WHERE
			OppM.numDomainId = @numDomainID 
			AND OppM.tintOppType = 1
			AND OppM.tintOppStatus = 1
			AND OppBiz.bitAuthoritativeBizDocs = 1
			AND BDC1.numComissionID IS NULL
			AND BDC2.numComissionID IS NULL
			AND ISNULL(OppBiz.monAmountPaid,0) >= OppBiz.monDealAmount
			AND (TempDepositMaster.dtDepositDate IS NOT NULL AND (CAST(TempDepositMaster.dtDepositDate AS DATE) BETWEEN @dtPayStart AND @dtPayEnd))
			AND OppBizItems.numItemCode NOT IN ( @numDiscountServiceItemID )  /*DO NOT INCLUDE DISCOUNT ITEM*/
			AND 1 = (
						CASE 
							WHEN OppBizItems.numItemCode = @numShippingServiceItemID 
							THEN 
								CASE 
									WHEN ISNULL(@bitIncludeTaxAndShippingInCommission,0) = 1
									THEN	
										1
									ELSE	
										0
								END
							ELSE 
								1 
						END
					)/*CONDITION TO INCLUDE/EXCLUDE INCLUDE SHIPPING ITEM */
	END	

	--GET COMMISSION RULE OF DOMAIN
	DECLARE @TempCommissionRule TABLE
	(
		ID INT IDENTITY(1,1),
		numComRuleID NUMERIC(18,0),
		tintComBasedOn TINYINT,
		tintComType TINYINT,
		tintComAppliesTo TINYINT,
		tintComOrgType TINYINT,
		tintAssignTo TINYINT
	)

	INSERT INTO
		@TempCommissionRule
	SELECT
		CR.numComRuleID,
		CR.tintComBasedOn,
		CR.tintComType,
		CR.tintComAppliesTo,
		CR.tintComOrgType,
		CR.tintAssignTo
	FROM
		CommissionRules CR
	LEFT JOIN
		PriceBookPriorities PBP
	ON	
		CR.tintComAppliesTo = PBP.Step2Value 
		AND CR.tintComOrgType = PBP.Step3Value
	WHERE
		CR.numDomainID = @numDomainID
		AND ISNULL(PBP.[Priority],0) > 0
	ORDER BY
		ISNULL(PBP.[Priority],0) ASC

	-- LOOP ALL COMMISSION RULES
	DECLARE @i INT = 1
	DECLARE @COUNT INT = 0

	SELECT @COUNT=COUNT(*) FROM @TempCommissionRule

	DECLARE @TEMPITEM TABLE
	(
		ID INT,
		UniqueID INT,
		numUserCntID NUMERIC(18,0),
		numOppID NUMERIC(18,0),
		numOppBizDocID NUMERIC(18,0),
		numOppBizDocItemID NUMERIC(18,0),
		numItemCode NUMERIC(18,0),
		numUnitHour FLOAT,
		monTotAmount DECIMAL(20,5),
		monVendorCost DECIMAL(20,5),
		monAvgCost DECIMAL(20,5),
		numOppItemID NUMERIC(18,0),
		monInvoiceSubTotal DECIMAL(20,5),
		monOrderSubTotal DECIMAL(20,5),
		numPOID NUMERIC(18,0),
		numPOItemID NUMERIC(18,0),
		monPOCost DECIMAL(20,5)
	)

	WHILE @i <= @COUNT
	BEGIN
		DECLARE @numComRuleID NUMERIC(18,0)
		DECLARE @tintComBasedOn TINYINT
		DECLARE @tintComAppliesTo TINYINT
		DECLARE @tintComOrgType TINYINT
		DECLARE @tintComType TINYINT
		DECLARE @tintAssignTo TINYINT
		DECLARE @numTotalAmount AS DECIMAL(20,5)
		DECLARE @numTotalUnit AS FLOAT

		--CLEAR PREVIOUS VALUES FROM TEMP TABLE
		DELETE FROM @TEMPITEM

		--FETCH COMMISSION RULE
		SELECT @numComRuleID=numComRuleID,@tintComBasedOn=tintComBasedOn,@tintComType=tintComType,@tintComAppliesTo=tintComAppliesTo,@tintComOrgType=tintComOrgType,@tintAssignTo=tintAssignTo FROM @TempCommissionRule WHERE ID=@i

		--FIND ALL BIZ DOC ITEMS WHERE COMMISSION IS NOT PAID AND CURRENT RULE ITEMS, ORGANIZATION AND ASSINGED TO FILTER CAN BE APPLIED
		INSERT INTO
			@TEMPITEM
		SELECT
			ROW_NUMBER() OVER (ORDER BY ID),
			ID,
			(CASE @tintAssignTo WHEN 1 THEN numAssignedTo WHEN 2 THEN numRecordOwner ELSE numPartner END),
			numOppID,
			numOppBizDocID,
			numOppBizDocItemID,
			numItemCode,
			numUnitHour,
			monTotAmount,
			monVendorCost,
			monAvgCost,
			numOppItemID,
			monInvoiceSubTotal,
			monOrderSubTotal,
			numPOID,
			numPOItemID,
			monPOCost
		FROM
			@TABLEPAID
		WHERE
			1 = (CASE @tintComAppliesTo
					--SPECIFIC ITEMS
					WHEN 1 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleItems WHERE numComRuleID = @numComRuleID AND ISNULL(numValue,0) = numItemCode) > 0 THEN 1 ELSE 0 END
					-- ITEM WITH SPECIFIC CLASSIFICATIONS
					WHEN 2 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleItems WHERE numComRuleID = @numComRuleID AND ISNULL(numValue,0) = numItemClassification) > 0 THEN 1 ELSE 0 END
					-- ALL ITEMS
					ELSE 1
				END)
			AND 1 = (CASE @tintComOrgType
						-- SPECIFIC ORGANIZATION
						WHEN 1 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleOrganization WHERE numComRuleID = @numComRuleID AND ISNULL(numValue,0) = numDivisionID) > 0 THEN 1 ELSE 0 END
						-- ORGANIZATION WITH SPECIFIC RELATIONSHIP AND PROFILE
						WHEN 2 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleOrganization WHERE numComRuleID = @numComRuleID AND ISNULL(numValue,0) = numRelationship AND ISNULL(numProfile,0) = numProfile) > 0 THEN 1 ELSE 0 END
						-- ALL ORGANIZATIONS
						ELSE 1
					END)
			AND 1 = (CASE @tintAssignTo 
						-- ORDER ASSIGNED TO
						WHEN 1 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleContacts WHERE numComRuleID = @numComRuleID AND numValue=numAssignedTo) > 0 THEN 1 ELSE 0 END
						-- ORDER OWNER
						WHEN 2 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleContacts WHERE numComRuleID = @numComRuleID AND numValue=numRecordOwner) > 0 THEN 1 ELSE 0 END
						-- Order Partner
						WHEN 3 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleContacts WHERE numComRuleID = @numComRuleID AND numValue=numPartner) > 0 THEN 1 ELSE 0 END
						-- NO OPTION SELECTED IN RULE
						ELSE 0
					END)


		--CALCULATE COMMISSION AND STORE IN TEMPARORY TABLE
		INSERT INTO @TempBizCommission 
		(
			numUserCntID,
			numOppID,
			numOppBizDocID,
			numOppBizDocItemID,
			monCommission,
			numComRuleID,
			tintComType,
			tintComBasedOn,
			decCommission,
			tintAssignTo,
			numOppItemID,
			monInvoiceSubTotal,
			monOrderSubTotal
		)
		SELECT
			numUserCntID,
			numOppID,
			numOppBizDocID,
			numOppBizDocItemID,
			CASE @tintComType 
				WHEN 1 --PERCENT
					THEN 
						CASE 
							WHEN ISNULL(@bitCommissionBasedOn,0) = 1
							THEN
								(CASE 
									WHEN numPOItemID IS NOT NULL
									THEN (ISNULL(monTotAmount,0) - ISNULL(monPOCost,0)) * (T2.decCommission / CAST(100 AS FLOAT))
									ELSE 
										CASE @tintCommissionBasedOn
											--ITEM GROSS PROFIT (VENDOR COST)
											WHEN 1 THEN (ISNULL(monTotAmount,0) - ISNULL(monVendorCost,0)) * (T2.decCommission / CAST(100 AS FLOAT))
											--ITEM GROSS PROFIT (AVERAGE COST)
											WHEN 2 THEN(ISNULL(monTotAmount,0) - ISNULL(monAvgCost,0)) * (T2.decCommission / CAST(100 AS FLOAT))
										END
								END)
							ELSE
								monTotAmount * (T2.decCommission / CAST(100 AS FLOAT))
						END
				ELSE  --FLAT
					T2.decCommission
			END,
			@numComRuleID,
			@tintComType,
			@tintComBasedOn,
			T2.decCommission,
			@tintAssignTo,
			numOppItemID,
			monInvoiceSubTotal,
			monOrderSubTotal
		FROM 
			@TEMPITEM AS T1
		CROSS APPLY
		(
			SELECT TOP 1 
				ISNULL(decCommission,0) AS decCommission
			FROM 
				CommissionRuleDtl 
			WHERE 
				numComRuleID=@numComRuleID 
				AND ISNULL(decCommission,0) > 0
				AND 1 = (CASE 
						WHEN @tintComBasedOn = 1 --BASED ON AMOUNT SOLD 
						THEN (CASE WHEN (T1.monTotAmount BETWEEN intFrom AND intTo) THEN 1 ELSE 0 END)
						WHEN @tintComBasedOn = 2 --BASED ON UNITS SOLD
						THEN (CASE WHEN (T1.numUnitHour BETWEEN intFrom AND intTo) THEN 1 ELSE 0 END)
						ELSE 0
						END)
		) AS T2
		WHERE
			(CASE 
				WHEN ISNULL(@bitCommissionBasedOn,0) = 1
				THEN
					(CASE 
						WHEN numPOItemID IS NOT NULL
						THEN (ISNULL(monTotAmount,0) - ISNULL(monPOCost,0)) * (T2.decCommission / CAST(100 AS FLOAT))
						ELSE
							CASE @tintCommissionBasedOn
								--ITEM GROSS PROFIT (VENDOR COST)
								WHEN 1 THEN (ISNULL(monTotAmount,0) - ISNULL(monVendorCost,0)) * (T2.decCommission / CAST(100 AS FLOAT))
								--ITEM GROSS PROFIT (AVERAGE COST)
								WHEN 2 THEN(ISNULL(monTotAmount,0) - ISNULL(monAvgCost,0)) * (T2.decCommission / CAST(100 AS FLOAT))
							END
					END)
				ELSE
					monTotAmount * (T2.decCommission / CAST(100 AS FLOAT))
			END) > 0 
			AND (
					SELECT 
						COUNT(*) 
					FROM 
						@TempBizCommission TempBDC
					WHERE 
						TempBDC.numUserCntID=T1.numUserCntID 
						AND TempBDC.numOppID=T1.numOppID 
						AND TempBDC.numOppBizDocID = T1.numOppBizDocID 
						AND TempBDC.numOppBizDocItemID = T1.numOppBizDocItemID
						AND TempBDC.numOppItemID = T1.numOppItemID 
						AND tintAssignTo = @tintAssignTo
				) = 0

		SET @i = @i + 1

		SET @numComRuleID = 0
		SET @tintComBasedOn = 0
		SET @tintComAppliesTo = 0
		SET @tintComOrgType = 0
		SET @tintComType = 0
		SET @tintAssignTo = 0
		SET @numTotalAmount = 0
		SET @numTotalUnit = 0
	END

	--INSERT CALCULATED ENTERIES IN BIZDOCCOMMISSION TABLE
	INSERT INTO BizDocComission
	(	
		numDomainID,
		numUserCntID,
		numOppBizDocId,
		numComissionAmount,
		numOppBizDocItemID,
		numComRuleID,
		tintComType,
		tintComBasedOn,
		decCommission,
		numOppID,
		bitFullPaidBiz,
		numComPayPeriodID,
		tintAssignTo,
		numOppItemID,
		monInvoiceSubTotal,
		monOrderSubTotal,
		bitDomainCommissionBasedOn,
		tintDomainCommissionBasedOn
	)
	SELECT
		@numDomainID,
		numUserCntID,
		numOppBizDocID,
		monCommission,
		numOppBizDocItemID,
		numComRuleID,
		tintComType,
		tintComBasedOn,
		decCommission,
		numOppID,
		1,
		@numComPayPeriodID,
		tintAssignTo,
		numOppItemID,
		monInvoiceSubTotal,
		monOrderSubTotal,
		@bitCommissionBasedOn,
		@tintCommissionBasedOn
	FROM
		@TempBizCommission
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CalculateCommissionUnPaidBizDoc')
DROP PROCEDURE USP_CalculateCommissionUnPaidBizDoc
GO
CREATE PROCEDURE [dbo].[USP_CalculateCommissionUnPaidBizDoc]
	-- Add the parameters for the stored procedure here
	 @numComPayPeriodID AS NUMERIC(18,0),
	 @numDomainID AS NUMERIC(18,0),
	 @ClientTimeZoneOffset INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
SET NOCOUNT ON;

BEGIN TRY
BEGIN TRANSACTION
	DECLARE @dtPayStart DATE 
	DECLARE @dtPayEnd DATE

	SELECT 
		@dtPayStart=dtStart
		,@dtPayEnd=dtEnd 
	FROM 
		CommissionPayPeriod 
	WHERE 
		numComPayPeriodID = @numComPayPeriodID

	-- DELETE UNPAID COMMISSION ENTRIES BETWEEN PAY PERIOD DATE RANGE
	DELETE FROM 
		BizDocComission 
	WHERE 
		numDomainId=@numDomainID 
		AND ISNULL(bitCommisionPaid,0)=0 
		AND numOppBizDocItemID IN (SELECT 
										ISNULL(numOppBizDocItemID,0)
									FROM 
										OpportunityBizDocs OppBiz
									INNER JOIN
										OpportunityMaster OppM
									ON
										OppBiz.numOppID = OppM.numOppID
									INNER JOIN
										OpportunityBizDocItems OppBizItems
									ON
										OppBiz.numOppBizDocsId = OppBizItems.numOppBizDocID
									WHERE
										OppM.numDomainId = @numDomainID 
										AND OppM.tintOppType = 1
										AND OppM.tintOppStatus = 1
										AND OppBiz.bitAuthoritativeBizDocs = 1
										AND ISNULL(OppBiz.monAmountPaid,0) < OppBiz.monDealAmount
										AND (OppBiz.dtCreatedDate IS NOT NULL AND (CAST(DATEADD(MINUTE,-@ClientTimeZoneOffset,OppBiz.dtCreatedDate) AS DATE) BETWEEN @dtPayStart AND @dtPayEnd)))
 
	--GET COMMISSION RULE OF DOMAIN
	DECLARE @TempCommissionRule TABLE
	(
		ID INT IDENTITY(1,1),
		numComRuleID NUMERIC(18,0),
		tintComBasedOn TINYINT,
		tintComType TINYINT,
		tintComAppliesTo TINYINT,
		tintComOrgType TINYINT,
		tintAssignTo TINYINT
	)

	INSERT INTO
		@TempCommissionRule
	SELECT
		CR.numComRuleID,
		CR.tintComBasedOn,
		CR.tintComType,
		CR.tintComAppliesTo,
		CR.tintComOrgType,
		CR.tintAssignTo
	FROM
		CommissionRules CR
	LEFT JOIN
		PriceBookPriorities PBP
	ON	
		CR.tintComAppliesTo = PBP.Step2Value 
		AND CR.tintComOrgType = PBP.Step3Value
	WHERE
		CR.numDomainID = @numDomainID
		AND ISNULL(PBP.[Priority],0) > 0
	ORDER BY
		ISNULL(PBP.[Priority],0) ASC

	--GET COMMISSION GLOBAL SETTINGS
	DECLARE @numDiscountServiceItemID NUMERIC(18,0)
	DECLARE @numShippingServiceItemID NUMERIC(18,0)
	DECLARE @tintCommissionType TINYINT
	DECLARE @bitIncludeTaxAndShippingInCommission BIT
	DECLARE @bitCommissionBasedOn BIT
	DECLARE @tintCommissionBasedOn TINYINT

	SELECT 
		@numDiscountServiceItemID=numDiscountServiceItemID, 
		@numShippingServiceItemID = numShippingServiceItemID, 
		@tintCommissionType=tintCommissionType, 
		@bitIncludeTaxAndShippingInCommission=bitIncludeTaxAndShippingInCommission
		,@tintCommissionBasedOn=ISNULL(tintCommissionBasedOn,1)
		,@bitCommissionBasedOn=ISNULL(bitCommissionBasedOn,0) 
	FROM 
		Domain 
	WHERE 
		numDomainID=@numDomainID

	--TEMPORARY TABLES TO HOLD DATA
	DECLARE @TempBizCommission TABLE
	(
		numUserCntID NUMERIC(18,0),
		numOppID NUMERIC(18,0),
		numOppBizDocID NUMERIC(18,0),
		numOppBizDocItemID NUMERIC(18,0),
		monCommission DECIMAL(20,5),
		numComRuleID NUMERIC(18,0),
		tintComType TINYINT,
		tintComBasedOn TINYINT,
		decCommission FLOAT,
		tintAssignTo TINYINT
	)

	DECLARE @TABLEPAID TABLE
	(
		ID INT IDENTITY(1,1),
		numDivisionID NUMERIC(18,0),
		numRelationship NUMERIC(18,0),
		numProfile NUMERIC(18,0),
		numAssignedTo NUMERIC(18,0),
		numRecordOwner NUMERIC(18,0),
		numItemCode NUMERIC(18,0),
		numItemClassification NUMERIC(18,0),
		numOppID NUMERIC(18,0),
		numOppBizDocID NUMERIC(18,0),
		numOppBizDocItemID NUMERIC(18,0),
		numUnitHour FLOAT,
		monTotAmount DECIMAL(20,5),
		monVendorCost DECIMAL(20,5),
		monAvgCost DECIMAL(20,5),
		numPartner NUMERIC(18,0),
		numPOID NUMERIC(18,0),
		numPOItemID NUMERIC(18,0),
		monPOCost DECIMAL(20,5)
	)

	-- GET FULLY PAID INVOICES WITHIN PAY PERIOD
	INSERT INTO 
		@TABLEPAID
	SELECT 
		OppM.numDivisionID,
		ISNULL(CompanyInfo.numCompanyType,0),
		ISNULL(CompanyInfo.vcProfile,0),
		OppM.numAssignedTo,
		OppM.numRecOwner,
		OppBizItems.numItemCode,
		Item.numItemClassification,
		OppM.numOppID,
		OppBizItems.numOppBizDocID,
		OppBizItems.numOppBizDocItemID,
		ISNULL(OppBizItems.numUnitHour,0),
		(CASE WHEN ISNULL(OppM.fltExchangeRate,0)=0 OR ISNULL(OppM.fltExchangeRate,0)=1 THEN ISNULL(OppBizItems.monTotAmount,0) ELSE ROUND(ISNULL(OppBizItems.monTotAmount,0) * OppM.fltExchangeRate,2) END),
		(ISNULL(OppMItems.monVendorCost,0) * ISNULL(OppBizItems.numUnitHour,0)),
		(ISNULL(OppMItems.monAvgCost,0) * ISNULL(OppBizItems.numUnitHour,0)),
		OppM.numPartner,
		ISNULL(TEMPMatchedPO.numOppId,TEMPPO.numOppId),
		ISNULL(TEMPMatchedPO.numoppitemtCode,TEMPPO.numoppitemtCode),
		ISNULL(ISNULL(TEMPMatchedPO.monPrice,TEMPPO.monPrice),0)
	FROM 
		OpportunityBizDocs OppBiz
	INNER JOIN
		OpportunityMaster OppM
	ON
		OppBiz.numOppID = OppM.numOppID
	INNER JOIN
		DivisionMaster 
	ON
		OppM.numDivisionID = DivisionMaster.numDivisionID
	INNER JOIN
		CompanyInfo
	ON
		DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
	INNER JOIN
		OpportunityBizDocItems OppBizItems
	ON
		OppBiz.numOppBizDocsId = OppBizItems.numOppBizDocID
	INNER JOIN
		OpportunityItems OppMItems 
	ON
		OppBizItems.numOppItemID = OppMItems.numoppitemtCode
	INNER JOIN
		Item
	ON
		OppBizItems.numItemCode = Item.numItemCode
	OUTER APPLY
	(
		SELECT TOP 1
			OMInner.numOppID
			,OMInner.vcPOppName
			,OIInner.numoppitemtCode
			,OIInner.monPrice
		FROM
			SalesOrderLineItemsPOLinking SOLIPL
		INNER JOIN
			OpportunityMaster OMInner
		ON
			SOLIPL.numPurchaseOrderID = OMInner.numOppId
		INNER JOIN
			OpportunityItems OIInner
		ON
			OMInner.numOppId = OIInner.numOppId
			AND SOLIPL.numPurchaseOrderItemID = OIInner.numoppitemtCode
		WHERE
			SOLIPL.numSalesOrderID = OppM.numOppId
			AND SOLIPL.numSalesOrderItemID = OppMItems.numoppitemtCode
	) TEMPMatchedPO
	OUTER APPLY
	(
		SELECT TOP 1
			OMInner.numOppID
			,OMInner.vcPOppName
			,OIInner.numoppitemtCode
			,OIInner.monPrice
		FROM
			OpportunityLinking OL
		INNER JOIN
			OpportunityMaster OMInner
		ON
			OL.numChildOppID = OMInner.numOppId
		INNER JOIN
			OpportunityItems OIInner
		ON
			OMInner.numOppId = OIInner.numOppId
		WHERE
			OL.numParentOppID = OppM.numOppId
			AND OIInner.numItemCode = OppMItems.numItemCode
			AND OIInner.vcAttrValues = OppMItems.vcAttrValues
	) TEMPPO
	WHERE
		OppM.numDomainId = @numDomainID 
		AND OppM.tintOppType = 1
		AND OppM.tintOppStatus = 1
		AND OppBiz.bitAuthoritativeBizDocs = 1
		AND ISNULL(OppBiz.monAmountPaid,0) < OppBiz.monDealAmount
		AND (OppBiz.dtCreatedDate IS NOT NULL AND (CAST(DATEADD(MINUTE,-@ClientTimeZoneOffset,OppBiz.dtCreatedDate) AS DATE) BETWEEN @dtPayStart AND @dtPayEnd))
		AND OppBizItems.numItemCode NOT IN ( @numDiscountServiceItemID )  /*DO NOT INCLUDE DISCOUNT ITEM*/
		AND 1 = (
					CASE 
						WHEN OppBizItems.numItemCode = @numShippingServiceItemID 
						THEN 
							CASE 
								WHEN ISNULL(@bitIncludeTaxAndShippingInCommission,0) = 1
								THEN	
									1
								ELSE	
									0
							END
						ELSE 
							1 
					END
				)/*CONDITION TO INCLUDE/EXCLUDE INCLUDE SHIPPING ITEM */


		-- LOOP ALL COMMISSION RULES
		DECLARE @i INT = 1
		DECLARE @COUNT INT = 0

		SELECT @COUNT=COUNT(*) FROM @TempCommissionRule

		DECLARE @TEMPITEM TABLE
		(
			ID INT,
			UniqueID INT,
			numUserCntID NUMERIC(18,0),
			numOppID NUMERIC(18,0),
			numOppBizDocID NUMERIC(18,0),
			numOppBizDocItemID NUMERIC(18,0),
			numItemCode NUMERIC(18,0),
			numUnitHour FLOAT,
			monTotAmount DECIMAL(20,5),
			monVendorCost DECIMAL(20,5),
			monAvgCost DECIMAL(20,5),
			numPOID NUMERIC(18,0),
			numPOItemID NUMERIC(18,0),
			monPOCost DECIMAL(20,5)
		)

		WHILE @i <= @COUNT
		BEGIN
			DECLARE @numComRuleID NUMERIC(18,0)
			DECLARE @tintComBasedOn TINYINT
			DECLARE @tintComAppliesTo TINYINT
			DECLARE @tintComOrgType TINYINT
			DECLARE @tintComType TINYINT
			DECLARE @tintAssignTo TINYINT
			DECLARE @numTotalAmount AS DECIMAL(20,5)
			DECLARE @numTotalUnit AS FLOAT

			--CLEAR PREVIOUS VALUES FROM TEMP TABLE
			DELETE FROM @TEMPITEM

			--FETCH COMMISSION RULE
			SELECT @numComRuleID=numComRuleID,@tintComBasedOn=tintComBasedOn,@tintComType=tintComType,@tintComAppliesTo=tintComAppliesTo,@tintComOrgType=tintComOrgType,@tintAssignTo=tintAssignTo FROM @TempCommissionRule WHERE ID=@i

			--FIND ALL BIZ DOC ITEMS WHERE COMMISSION IS NOT PAID AND CURRENT RULE ITEMS, ORGANIZATION AND ASSINGED TO FILTER CAN BE APPLIED
			INSERT INTO
				@TEMPITEM
			SELECT
				ROW_NUMBER() OVER (ORDER BY ID),
				ID,
				(CASE @tintAssignTo WHEN 1 THEN numAssignedTo WHEN 2 THEN numRecordOwner ELSE numPartner END),
				numOppID,
				numOppBizDocID,
				numOppBizDocItemID,
				numItemCode,
				numUnitHour,
				monTotAmount,
				monVendorCost,
				monAvgCost,
				numPOID,
				numPOItemID,
				monPOCost
			FROM
				@TABLEPAID
			WHERE
				1 = (CASE @tintComAppliesTo
						--SPECIFIC ITEMS
						WHEN 1 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleItems WHERE numComRuleID = @numComRuleID AND ISNULL(numValue,0) = numItemCode) > 0 THEN 1 ELSE 0 END
						-- ITEM WITH SPECIFIC CLASSIFICATIONS
						WHEN 2 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleItems WHERE numComRuleID = @numComRuleID AND ISNULL(numValue,0) = numItemClassification) > 0 THEN 1 ELSE 0 END
						-- ALL ITEMS
						ELSE 1
					END)
				AND 1 = (CASE @tintComOrgType
							-- SPECIFIC ORGANIZATION
							WHEN 1 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleOrganization WHERE numComRuleID = @numComRuleID AND ISNULL(numValue,0) = numDivisionID) > 0 THEN 1 ELSE 0 END
							-- ORGANIZATION WITH SPECIFIC RELATIONSHIP AND PROFILE
							WHEN 2 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleOrganization WHERE numComRuleID = @numComRuleID AND ISNULL(numValue,0) = numRelationship AND ISNULL(numProfile,0) = numProfile) > 0 THEN 1 ELSE 0 END
							-- ALL ORGANIZATIONS
							ELSE 1
						END)
				AND 1 = (CASE @tintAssignTo 
							-- ORDER ASSIGNED TO
							WHEN 1 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleContacts WHERE numComRuleID = @numComRuleID AND numValue=numAssignedTo) > 0 THEN 1 ELSE 0 END
							-- ORDER OWNER
							WHEN 2 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleContacts WHERE numComRuleID = @numComRuleID AND numValue=numRecordOwner) > 0 THEN 1 ELSE 0 END
							-- ORDER OWNER
							WHEN 3 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleContacts WHERE numComRuleID = @numComRuleID AND numValue=numPartner) > 0 THEN 1 ELSE 0 END
							-- NO OPTION SELECTED IN RULE
							ELSE 0
						END)

			--CALCULATE COMMISSION AND STORE IN TEMPARORY TABLE
			INSERT INTO @TempBizCommission 
			(
				numUserCntID,
				numOppID,
				numOppBizDocID,
				numOppBizDocItemID,
				monCommission,
				numComRuleID,
				tintComType,
				tintComBasedOn,
				decCommission,
				tintAssignTo
			)
			SELECT
				numUserCntID,
				numOppID,
				numOppBizDocID,
				numOppBizDocItemID,
				CASE @tintComType 
					WHEN 1 --PERCENT
						THEN 
							CASE 
								WHEN ISNULL(@bitCommissionBasedOn,1) = 1
								THEN
									(CASE 
										WHEN numPOItemID IS NOT NULL
										THEN (ISNULL(monTotAmount,0) - ISNULL(monPOCost,0)) * (T2.decCommission / CAST(100 AS FLOAT))
										ELSE CASE @tintCommissionBasedOn
												--ITEM GROSS PROFIT (VENDOR COST)
												WHEN 1 THEN (ISNULL(monTotAmount,0) - ISNULL(monVendorCost,0)) * (T2.decCommission / CAST(100 AS FLOAT))
												--ITEM GROSS PROFIT (AVERAGE COST)
												WHEN 2 THEN(ISNULL(monTotAmount,0) - ISNULL(monAvgCost,0)) * (T2.decCommission / CAST(100 AS FLOAT))
											END
									END)
								ELSE
									monTotAmount * (T2.decCommission / CAST(100 AS FLOAT))
							END
					ELSE  --FLAT
						T2.decCommission
				END,
				@numComRuleID,
				@tintComType,
				@tintComBasedOn,
				T2.decCommission,
				@tintAssignTo
			FROM 
				@TEMPITEM AS T1
			CROSS APPLY
			(
				SELECT TOP 1 
					ISNULL(decCommission,0) AS decCommission
				FROM 
					CommissionRuleDtl 
				WHERE 
					numComRuleID=@numComRuleID 
					AND ISNULL(decCommission,0) > 0
					AND 1 = (CASE 
							WHEN @tintComBasedOn = 1 --BASED ON AMOUNT SOLD 
							THEN (CASE WHEN (T1.monTotAmount BETWEEN intFrom AND intTo) THEN 1 ELSE 0 END)
							WHEN @tintComBasedOn = 2 --BASED ON UNITS SOLD
							THEN (CASE WHEN (T1.numUnitHour BETWEEN intFrom AND intTo) THEN 1 ELSE 0 END)
							ELSE 0
							END)
			) AS T2
			WHERE
				(CASE 
					WHEN ISNULL(@bitCommissionBasedOn,1) = 1
					THEN
						(CASE 
							WHEN numPOItemID IS NOT NULL
							THEN (ISNULL(monTotAmount,0) - ISNULL(monPOCost,0)) * (T2.decCommission / CAST(100 AS FLOAT))
							ELSE CASE @tintCommissionBasedOn
									--ITEM GROSS PROFIT (VENDOR COST)
									WHEN 1 THEN (ISNULL(monTotAmount,0) - ISNULL(monVendorCost,0)) * (T2.decCommission / CAST(100 AS FLOAT))
									--ITEM GROSS PROFIT (AVERAGE COST)
									WHEN 2 THEN(ISNULL(monTotAmount,0) - ISNULL(monAvgCost,0)) * (T2.decCommission / CAST(100 AS FLOAT))
								END
						END)
					ELSE
						monTotAmount * (T2.decCommission / CAST(100 AS FLOAT))
				END) > 0 
				AND (
						SELECT 
							COUNT(*) 
						FROM 
							@TempBizCommission 
						WHERE 
							numUserCntID=T1.numUserCntID 
							AND numOppID=T1.numOppID 
							AND numOppBizDocID = T1.numOppBizDocID 
							AND numOppBizDocItemID = T1.numOppBizDocItemID
							AND tintAssignTo = @tintAssignTo
					) = 0

			SET @i = @i + 1

			SET @numComRuleID = 0
			SET @tintComBasedOn = 0
			SET @tintComAppliesTo = 0
			SET @tintComOrgType = 0
			SET @tintComType = 0
			SET @tintAssignTo = 0
			SET @numTotalAmount = 0
			SET @numTotalUnit = 0
		END

		--INSERT CALCULATED ENTERIES IN BIZDOCCOMMISSION TABLE
		INSERT INTO BizDocComission
		(	
			numDomainID,
			numUserCntID,
			numOppBizDocId,
			numComissionAmount,
			numOppBizDocItemID,
			numComRuleID,
			tintComType,
			tintComBasedOn,
			decCommission,
			numOppID,
			bitFullPaidBiz,
			numComPayPeriodID,
			tintAssignTo
		)
		SELECT
			@numDomainID,
			numUserCntID,
			numOppBizDocID,
			monCommission,
			numOppBizDocItemID,
			numComRuleID,
			tintComType,
			tintComBasedOn,
			decCommission,
			numOppID,
			0,
			@numComPayPeriodID,
			tintAssignTo
		FROM
			@TempBizCommission
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetAccountReceivableAging_Invoice]    Script Date: 05/24/2010 03:48:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetAccountReceivableAging_Invoice')
DROP PROCEDURE USP_GetAccountReceivableAging_Invoice
GO
CREATE PROCEDURE [dbo].[USP_GetAccountReceivableAging_Invoice]
(
    @numDomainId AS NUMERIC(9) = 0,
	@numDivisionId AS NUMERIC(9) = NULL,
	@vcFlag VARCHAR(20),
	@numAccountClass AS NUMERIC(9)=0,
	@dtFromDate AS DATE = NULL,
	@dtToDate AS DATE = NULL,
	@ClientTimeZoneOffset INT = 0,
	@vcRegularSearch VARCHAR(MAX) = '',
	@bitCustomerStatement BIT = 0
)
AS
  BEGIN
	DECLARE @numDefaultARAccount NUMERIC(18,0)
	SET @numDefaultARAccount = ISNULL((SELECT numAccountID FROM AccountingCharges WHERE numDomainID=@numDomainId AND numChargeTypeId=4),0)

    DECLARE  @AuthoritativeSalesBizDocId  AS INTEGER
    SELECT @AuthoritativeSalesBizDocId = isnull(numAuthoritativeSales,0)
    FROM   AuthoritativeBizDocs
    WHERE  numDomainId = @numDomainId;--287
    ------------------------------------------      
    DECLARE @baseCurrency AS NUMERIC
	SELECT @baseCurrency=numCurrencyID FROM dbo.Domain WHERE numDomainId=@numDomainId
	/*when any Order is placed in Foreign currency (other than base currency)then show it in purple color */
	------------------------------------------      
	IF @dtFromDate IS NULL
		SET @dtFromDate = '1990-01-01 00:00:00.000'
	IF @dtToDate IS NULL
		SET @dtToDate = GETUTCDATE()

	SET @numDivisionId = ISNULL(@numDivisionId,0)

    DECLARE @strSql VARCHAR(MAX);
	Declare @strSql1 VARCHAR(MAX);
	Declare @strCredit varchar(MAX); SET @strCredit=''
    
    
    SET @strSql = ' SELECT DISTINCT OM.[numOppId],
                        OM.[vcPOppName],
                        OM.[tintOppType],
                        OB.[numOppBizDocsId],
                        OB.[vcBizDocID],
                        isnull(OB.monDealAmount * OM.fltExchangeRate,0) TotalAmount,
                        (ISNULL(TablePayments.monPaidAmount,0) * OM.fltExchangeRate) AmountPaid,
                        isnull(OB.monDealAmount
                                 * OM.fltExchangeRate
                                 - ISNULL(TablePayments.monPaidAmount,0)
                                     * OM.fltExchangeRate,0) BalanceDue,
                        CASE ISNULL(bitBillingTerms,0) 
                          --WHEN 1 THEN [dbo].[FormatedDateFromDate](DATEADD(dd,convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0)),OB.dtFromDate),
                          WHEN 1 THEN [dbo].[FormatedDateFromDate](DATEADD(dd,convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0)),OB.dtFromDate),
                                                                   '+ CONVERT(VARCHAR(15),@numDomainId) +')
                          WHEN 0 THEN [dbo].[FormatedDateFromDate](ob.[dtFromDate],'+ CONVERT(VARCHAR(15),@numDomainId) +')
                        END AS DueDate,
                        bitBillingTerms,
                        intBillingDays,
                        OB.dtCreatedDate,
                        ISNULL(C.[varCurrSymbol],'''') [varCurrSymbol],CO.vcCompanyName,ADC.vcFirstname + '' ''+ ADC.vcLastName AS vcContactName,
                           dbo.fn_GetContactName(OM.numRecOwner) AS RecordOwner,
						   dbo.fn_GetContactName(OM.numAssignedTo) AS AssignedTo
						   ,CASE WHEN ADC.numPhone IS NULL THEN '''' ELSE + ADC.numPhone + (CASE WHEN numPhoneExtension IS NULL THEN '''' WHEN numPhoneExtension='''' THEN '''' ELSE '','' + ADC.numPhoneExtension END) END AS vcCustPhone,
						   OM.numCurrencyID,OB.vcRefOrderNo,[dbo].[FormatedDateFromDate](ob.[dtFromDate],'+ CONVERT(VARCHAR(15),@numDomainId) +') AS dtFromDate,
						    -1 AS [numJournal_Id],-1 as numReturnHeaderID,
							CASE WHEN ISNULL(BT.[numDiscount],0) > 0 AND ISNULL(BT.[numInterestPercentage],0) > 0 
								 THEN '+ '''-''+' + 'CONVERT(VARCHAR(10),ISNULL(BT.[numDiscount],0)) +' + '''% if paid ''+' + ' CONVERT(VARCHAR(10),ISNULL([BT].[numDiscountPaidInDays],0)) +' + ''' days early / ''+' + ' CONVERT(VARCHAR(10),ISNULL([BT].[numInterestPercentage],0)) +' + '''% if paid ''+' + ' CONVERT(VARCHAR(10),ISNULL([BT].[numInterestPercentageIfPaidAfterDays],0)) +' + ''' days or later ''' +
								 ' WHEN ISNULL(BT.[numDiscount],0) > 0 THEN '+ '''-''+' + 'CONVERT(VARCHAR(10),ISNULL(BT.[numDiscount],0)) +' + '''% if paid ''+' + ' CONVERT(VARCHAR(10),ISNULL([BT].[numDiscountPaidInDays],0)) +' + ''' days early ''' + 
								 ' WHEN ISNULL(BT.[numInterestPercentage],0) > 0  THEN +CONVERT(VARCHAR(10),ISNULL([BT].[numInterestPercentage],0))+' + '''% if paid ''+' + ' CONVERT(VARCHAR(10),ISNULL([BT].[numInterestPercentageIfPaidAfterDays],0))+' + ''' days or later ''' +
								' END AS [vcTerms],
							ISNULL(BT.[numDiscount],0) AS [numDiscount],
							ISNULL(BT.[numInterestPercentage],0) AS [numInterest],
							0 AS tintLevel
							,ISNULL(OB.numARAccountID,'+ CAST(@numDefaultARAccount AS VARCHAR) +') AS numChartAcntId
        INTO #TempRecords                
        FROM   [OpportunityMaster] OM
               INNER JOIN [DivisionMaster] DM
                 ON OM.[numDivisionId] = DM.[numDivisionID]
                  INNER JOIN CompanyInfo CO ON CO.numCompanyId = DM.numCompanyID
                  LEFT JOIN AdditionalContactsInformation ADC ON ADC.numDivisionId=DM.numDivisionID AND ADC.numContactId=OM.numContactId
               LEFT OUTER JOIN OpportunityBizDocs OB ON OB.numOppId = OM.numOppID
               LEFT OUTER JOIN [Currency] C ON OM.[numCurrencyID] = C.[numCurrencyID]
			   LEFT JOIN [dbo].[BillingTerms] AS BT ON [OM].[intBillingDays] = [BT].[numTermsID] 
			   OUTER APPLY
			   (
					SELECT 
						SUM(monAmountPaid) AS monPaidAmount
					FROM
						DepositeDetails DD
					INNER JOIN
						DepositMaster DM
					ON
						DD.numDepositID=DM.numDepositId
					WHERE 
						numOppID = OM.numOppID
						AND numOppBizDocsID=OB.numOppBizDocsID
						AND CONVERT(DATE,DM.dtDepositDate) <= ''' + CAST(@dtToDate AS VARCHAr) + '''
				) TablePayments
        WHERE  OM.[tintOppType] = 1
               AND OM.[tintOppStatus] = 1
               AND om.[numDomainId] = '+ CONVERT(VARCHAR(15),@numDomainId) +'
               AND OB.[numBizDocId] = '+ CONVERT(VARCHAR(15),ISNULL(@AuthoritativeSalesBizDocId,0)) +' 
			   AND (OM.numDivisionId = ' + CAST(@numDivisionId AS VARCHAR) + ' OR ' + CAST(@numDivisionId AS VARCHAR) + ' = 0)
				AND (OM.numAccountClass= ' + CAST(@numAccountClass AS VARCHAR) + ' OR ' + CAST(@numAccountClass AS VARCHAR) + '=0)
				AND '+ (CASE 
							WHEN ISNULL(@bitCustomerStatement,0) = 1 
							THEN 'CONVERT(DATE,OB.[dtCreatedDate])' 
							ELSE 'CONVERT(DATE,DATEADD(DAY,CASE WHEN OM.bitBillingTerms = 1 
								 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(OM.intBillingDays,0)), 0))
								 ELSE 0 
									END, dtFromDate))' 
						END) + ' <= ''' + CAST(@dtToDate AS VARCHAr) + '''
               AND OB.bitAuthoritativeBizDocs=1
               AND ISNULL(OB.tintDeferred,0) <> 1
               AND isnull(OB.monDealAmount * OM.fltExchangeRate,0) != 0 '     
       
       SET @strSql1 = '  UNION 
		 SELECT 
				-1 [numOppId],
				''Journal'' [vcPOppName],
				0 [tintOppType],
				0 [numOppBizDocsId],
				''Journal'' [vcBizDocID],
				CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt ELSE -1*GJD.numCreditAmt END TotalAmount,
				0 AmountPaid,
				CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt ELSE -1*GJD.numCreditAmt END BalanceDue,
				[dbo].[FormatedDateFromDate](GJH.datEntry_Date,'+ CONVERT(VARCHAR(15),@numDomainId) +') AS DueDate,
				0 bitBillingTerms,
				0 intBillingDays,
				GJH.datCreatedDate,
                ISNULL(C.[varCurrSymbol], '''') [varCurrSymbol]
                ,CO.vcCompanyName,ADC.vcFirstname + '' ''+ ADC.vcLastName AS vcContactName,
                           '''' AS RecordOwner,
						   '''' AS AssignedTo
						   ,CASE WHEN ADC.numPhone IS NULL THEN '''' ELSE + ADC.numPhone + (CASE WHEN numPhoneExtension IS NULL THEN '''' WHEN numPhoneExtension='''' THEN '''' ELSE '','' + ADC.numPhoneExtension END) END AS vcCustPhone,
						   GJD.numCurrencyID,'''' as vcRefOrderNo,NULL, ISNULL(GJH.numJournal_Id,0) AS [numJournal_Id],-1 as numReturnHeaderID,
							CASE WHEN ISNULL(BT.[numDiscount],0) > 0 AND ISNULL(BT.[numInterestPercentage],0) > 0 
								 THEN '+ '''-''+' + 'CONVERT(VARCHAR(10),ISNULL(BT.[numDiscount],0)) +' + '''% if paid ''+' + ' CONVERT(VARCHAR(10),ISNULL([BT].[numDiscountPaidInDays],0)) +' + ''' days early / ''+' + ' CONVERT(VARCHAR(10),ISNULL([BT].[numInterestPercentage],0)) +' + '''% if paid ''+' + ' CONVERT(VARCHAR(10),ISNULL([BT].[numInterestPercentageIfPaidAfterDays],0)) +' + ''' days or later ''' +
								 ' WHEN ISNULL(BT.[numDiscount],0) > 0 THEN '+ '''-''+' + 'CONVERT(VARCHAR(10),ISNULL(BT.[numDiscount],0)) +' + '''% if paid ''+' + ' CONVERT(VARCHAR(10),ISNULL([BT].[numDiscountPaidInDays],0)) +' + ''' days early ''' + 
								 ' WHEN ISNULL(BT.[numInterestPercentage],0) > 0  THEN +CONVERT(VARCHAR(10),ISNULL([BT].[numInterestPercentage],0))+' + '''% if paid ''+' + ' CONVERT(VARCHAR(10),ISNULL([BT].[numInterestPercentageIfPaidAfterDays],0))+' + ''' days or later ''' +
								' END AS [vcTerms],
							ISNULL(BT.[numDiscount],0) AS [numDiscount],
							ISNULL(BT.[numInterestPercentage],0) AS [numInterest],
							0 AS tintLevel,
							GJD.numChartAcntId
		 FROM   dbo.General_Journal_Header GJH
				INNER JOIN dbo.General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId
															  AND GJH.numDomainId = GJD.numDomainId
				INNER JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = GJD.numChartAcntId
				INNER JOIN [DivisionMaster] DM ON GJD.numCustomerID = DM.[numDivisionID]
				INNER JOIN CompanyInfo CO ON CO.numCompanyId = DM.numCompanyID
                Left JOIN AdditionalContactsInformation ADC ON ADC.numContactId=GJD.numContactId 
				LEFT OUTER JOIN [Currency] C ON GJD.[numCurrencyID] = C.[numCurrencyID]
				LEFT JOIN [dbo].[BillingTerms] AS BT ON [DM].[numBillingDays] = [BT].[numTermsID]
		 WHERE  GJH.numDomainId ='+ CONVERT(VARCHAR(15),@numDomainId) +'
				AND COA.vcAccountCode LIKE ''01010105%'' 
				AND ISNULL(numOppId,0)=0 
				AND ISNULL(numOppBizDocsId,0)=0 
				AND ISNULL(GJH.numDepositID,0)=0 
				AND ISNULL(GJH.numReturnID,0)=0
				AND (GJD.numCustomerId = ' + CAST(@numDivisionId AS VARCHAR) + ' OR ' + CAST(@numDivisionId AS VARCHAR) + '  = 0)
				AND (GJH.numAccountClass= ' + CAST(@numAccountClass AS VARCHAR) + ' OR ' + CAST(@numAccountClass AS VARCHAR) + '=0)
				AND CONVERT(DATE,GJH.[datEntry_Date]) <= ''' + CAST(@dtToDate AS VARCHAr) + ''''
   
	 SET @strCredit = ' UNION SELECT -1 [numOppId],
									CASE WHEN tintDepositePage=2 THEN ''Unapplied Payment'' ELSE ''Credit Memo'' END AS [vcPOppName],
									0 [tintOppType],
									0 [numOppBizDocsId],
									''''  [vcBizDocID],
									(ISNULL(monDepositAmount,0) - ISNULL(monAppliedAmount,0)) * -1 AS TotalAmount,
									0 AmountPaid,
									(ISNULL(monDepositAmount,0) - ISNULL(monAppliedAmount,0)) * -1 AS BalanceDue,
									[dbo].[FormatedDateFromDate](DM.dtDepositDate,'+ CONVERT(VARCHAR(15),@numDomainId) +') AS DueDate,
									0 bitBillingTerms,
									0 intBillingDays,
									DM.dtCreationDate AS [datCreatedDate],
									'''' [varCurrSymbol],
									dbo.fn_GetComapnyName(DM.numDivisionId) vcCompanyName,
									'''' vcContactName,
									'''' AS RecordOwner,
									'''' AS AssignedTo,
									'''' AS vcCustPhone,
									ISNULL(DM.numCurrencyID,0) AS [numCurrencyID],
									'''' as vcRefOrderNo,
									NULL, 
									-1 AS [numJournal_Id],isnull(numReturnHeaderID,0) as numReturnHeaderID,
									'''' AS [vcTerms],
									0 AS [numDiscount],
									0 AS [numInterest],
									0 AS tintLeve,
									0 AS numChartAcntId
					FROM  DepositMaster DM 
					WHERE DM.numDomainId='+ CONVERT(VARCHAR(15),@numDomainId) +' AND tintDepositePage IN(2,3)  
					AND ((ISNULL(monDepositAmount,0) - ISNULL(monAppliedAmount,0)) > 0)
					AND (DM.numDivisionId = ' + CAST(@numDivisionId AS VARCHAR) + ' OR ' + CAST(@numDivisionId AS VARCHAR) + ' = 0)
					AND (DM.numAccountClass= ' + CAST(@numAccountClass AS VARCHAR) + ' OR ' + CAST(@numAccountClass AS VARCHAR) + '=0)
					AND CONVERT(DATE,DM.dtDepositDate) <= ''' + CAST(@dtToDate AS VARCHAr) + '''
					UNION
					SELECT 
						-1 [numOppId]
						,''Refund'' AS [vcPOppName]
						,0 [tintOppType]
						,0 [numOppBizDocsId]
						,''''  [vcBizDocID]
						,(CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt ELSE -1 * GJD.numCreditAmt END) AS TotalAmount
						,0 AmountPaid
						,(CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt ELSE -1 * GJD.numCreditAmt END) AS BalanceDue
						,[dbo].[FormatedDateFromDate](GJH.datEntry_Date,'+ CONVERT(VARCHAR(15),@numDomainId) +') AS DueDate
						,0 bitBillingTerms
						,0 intBillingDays
						,RH.dtCreatedDate AS [datCreatedDate]
						,'''' [varCurrSymbol]
						,dbo.fn_GetComapnyName(DIV.numDivisionId) vcCompanyName
						,'''' vcContactName
						,'''' AS RecordOwner
						,'''' AS AssignedTo
						,'''' AS vcCustPhone
						,ISNULL(DIV.numCurrencyID,0) AS [numCurrencyID]
						,'''' as vcRefOrderNo
						,NULL
						,GJH.numJournal_Id AS [numJournal_Id]
						,isnull(numReturnHeaderID,0) as numReturnHeaderID
						,'''' AS [vcTerms]
						,0 AS [numDiscount]
						,0 AS [numInterest]
						,0 AS tintLevel
						,GJD.numChartAcntId
					 FROM   dbo.ReturnHeader RH JOIN dbo.General_Journal_Header GJH ON RH.numReturnHeaderID = GJH.numReturnID
					 JOIN [dbo].[DivisionMaster] AS DIV ON DIV.[numDivisionID] = RH.[numDivisionId]
					JOIN General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId 
					JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = GJD.numChartAcntId AND COA.vcAccountCode LIKE ''0101010501''
					WHERE 
						RH.tintReturnType=4 
						AND RH.numDomainId=' + CAST(@numDomainId AS VARCHAR) + '  
						AND ISNULL(RH.numParentID,0) = 0
						AND ISNULL(RH.IsUnappliedPayment,0) = 0
						AND (monBizDocAmount > 0) AND ISNULL(RH.numBillPaymentIDRef,0)=0
						AND (RH.numDivisionId = ' + CAST(@numDivisionId AS VARCHAR) + ' OR ' + CAST(@numDivisionId AS VARCHAR) + ' = 0)
						AND (RH.numAccountClass= ' + CAST(@numAccountClass AS VARCHAR) + ' OR ' + CAST(@numAccountClass AS VARCHAR) + '=0)
						AND CONVERT(DATE,GJH.[datEntry_Date]) <= ''' + CAST(@dtToDate AS VARCHAr) + ''''
	            	
    IF (@vcFlag = '0+30')
      BEGIN
      
      SET @strSql =@strSql +
						'AND dateadd(DAY,CASE 
                                 --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                 WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                 ELSE 0
                               END,OB.dtFromDate) BETWEEN dbo.GetUTCDateWithoutTime() AND dateadd(DAY,31,dbo.GetUTCDateWithoutTime())
               AND dbo.GetUTCDateWithoutTime() <= dateadd(DAY,CASE 
                                                                --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                                WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                                ELSE 0
                                                              END,OB.dtFromDate)'
                                                              
		SET @strSql1 =@strSql1 + 'AND GJH.datEntry_Date BETWEEN dbo.GetUTCDateWithoutTime() AND dateadd(DAY,31,dbo.GetUTCDateWithoutTime())'
		
		SET @strCredit =@strCredit + 'AND DM.dtDepositDate BETWEEN dbo.GetUTCDateWithoutTime() AND dateadd(DAY,31,dbo.GetUTCDateWithoutTime())'
      END
    ELSE
      IF (@vcFlag = '30+60')
        BEGIN
         SET @strSql =@strSql +
                 'AND dateadd(DAY,CASE 
                                   --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                   WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                   ELSE 0
                                 END,OB.dtFromDate) BETWEEN dateadd(DAY,31,dbo.GetUTCDateWithoutTime()) AND dateadd(DAY,61,dbo.GetUTCDateWithoutTime())
                 AND dbo.GetUTCDateWithoutTime() <= dateadd(DAY,CASE 
                                                                  --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                                  WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                                  ELSE 0
                                                                END,OB.dtFromDate)'
		SET @strSql1 =@strSql1 + 'AND GJH.datEntry_Date BETWEEN dateadd(DAY,31,dbo.GetUTCDateWithoutTime()) AND dateadd(DAY,61,dbo.GetUTCDateWithoutTime())'
        
        SET @strCredit =@strCredit + 'AND DM.dtDepositDate BETWEEN dateadd(DAY,31,dbo.GetUTCDateWithoutTime()) AND dateadd(DAY,61,dbo.GetUTCDateWithoutTime())'
                 
        END
      ELSE
        IF (@vcFlag = '60+90')
          BEGIN
          SET @strSql =@strSql +
                   'AND dateadd(DAY,CASE 
                                     --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                     WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                     ELSE 0
                                   END,OB.dtFromDate) BETWEEN dateadd(DAY,61,dbo.GetUTCDateWithoutTime()) AND dateadd(DAY,91,dbo.GetUTCDateWithoutTime())
                   AND dbo.GetUTCDateWithoutTime() <= dateadd(DAY,CASE 
                                                                    --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                                    WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                                    ELSE 0
                                                                  END,OB.dtFromDate)'
			SET @strSql1 =@strSql1 + 'AND GJH.datEntry_Date BETWEEN dateadd(DAY,61,dbo.GetUTCDateWithoutTime()) AND dateadd(DAY,91,dbo.GetUTCDateWithoutTime())'
			
			SET @strCredit =@strCredit + 'AND DM.dtDepositDate BETWEEN dateadd(DAY,61,dbo.GetUTCDateWithoutTime()) AND dateadd(DAY,91,dbo.GetUTCDateWithoutTime())'
			
          END
        ELSE
          IF (@vcFlag = '90+')
            BEGIN
             SET @strSql =@strSql +
                     'AND dateadd(DAY,CASE 
                                       --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                       WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                       ELSE 0
                                     END,OB.dtFromDate) > dateadd(DAY,91,dbo.GetUTCDateWithoutTime())
                     AND dbo.GetUTCDateWithoutTime() <= dateadd(DAY,CASE 
                                                                      --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                                      WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                                      ELSE 0
                                                                    END,OB.dtFromDate)'
			SET @strSql1 =@strSql1 + 'AND GJH.datEntry_Date  > dateadd(DAY,91,dbo.GetUTCDateWithoutTime())'
            
            SET @strCredit =@strCredit + 'AND DM.dtDepositDate  > dateadd(DAY,91,dbo.GetUTCDateWithoutTime())'
                     
            END
          ELSE
            IF (@vcFlag = '0-30')
              BEGIN
               SET @strSql =@strSql +
                       'AND dbo.GetUTCDateWithoutTime() > dateadd(DAY,CASE 
                                                                       --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                                       WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                                       ELSE 0
                                                                     END,OB.dtFromDate)
                       AND datediff(DAY,dateadd(DAY,CASE 
                                                      --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                      WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                      ELSE 0
                                                    END,OB.dtFromDate),dbo.GetUTCDateWithoutTime()) BETWEEN 0 AND 30 '
				SET @strSql1 =@strSql1 + 'AND datediff(DAY,GJH.datEntry_Date, dbo.GetUTCDateWithoutTime()) BETWEEN 0 AND 30 '
				
				SET @strCredit =@strCredit + 'AND datediff(DAY,DM.dtDepositDate, dbo.GetUTCDateWithoutTime()) BETWEEN 0 AND 30 '
				
              END
            ELSE
              IF (@vcFlag = '30-60')
                BEGIN
                  SET @strSql =@strSql +
                         'AND dbo.GetUTCDateWithoutTime() > dateadd(DAY,CASE 
                                                                         --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                                         WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                                         ELSE 0
                                                                       END,OB.dtFromDate)
                         AND datediff(DAY,dateadd(DAY,CASE 
                                                        --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                        WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                        ELSE 0
                                                      END,OB.dtFromDate),dbo.GetUTCDateWithoutTime()) BETWEEN 31 AND 60'
                SET @strSql1 =@strSql1 + 'AND datediff(DAY,GJH.datEntry_Date, dbo.GetUTCDateWithoutTime()) BETWEEN 31 AND 60 '
                
                SET @strCredit =@strCredit + 'AND datediff(DAY,DM.dtDepositDate, dbo.GetUTCDateWithoutTime()) BETWEEN 31 AND 60 '
                
                END
              ELSE
                IF (@vcFlag = '60-90')
                  BEGIN
					SET @strSql =@strSql +
                           'AND dbo.GetUTCDateWithoutTime() > dateadd(DAY,CASE 
                                                                           --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                                           WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                                           ELSE 0
                                                                         END,OB.dtFromDate)
                           AND datediff(DAY,dateadd(DAY,CASE 
                                                          --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                          WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                          ELSE 0
                                                        END,OB.dtFromDate),dbo.GetUTCDateWithoutTime()) BETWEEN 61 AND 90'
					SET @strSql1 =@strSql1 + 'AND datediff(DAY,GJH.datEntry_Date, dbo.GetUTCDateWithoutTime()) BETWEEN 61 AND 90 '
                    
                    SET @strCredit =@strCredit + 'AND datediff(DAY,DM.dtDepositDate, dbo.GetUTCDateWithoutTime()) BETWEEN 61 AND 90 '
                           
                  END
                ELSE
                  IF (@vcFlag = '90-')
                    BEGIN
                      SET @strSql =@strSql +
                             'AND dbo.GetUTCDateWithoutTime() > dateadd(DAY,CASE 
                                                                             --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                                             WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																															   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                                             ELSE 0
                                                                           END,OB.dtFromDate)
                             AND datediff(DAY,dateadd(DAY,CASE 
                                                            --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                            WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                            ELSE 0
                                                          END,OB.dtFromDate),dbo.GetUTCDateWithoutTime()) > 90'
					SET @strSql1 =@strSql1 + 'AND datediff(DAY,GJH.datEntry_Date, dbo.GetUTCDateWithoutTime()) > 90'
                     
                    SET @strCredit =@strCredit + 'AND datediff(DAY,DM.dtDepositDate, dbo.GetUTCDateWithoutTime()) > 90'   
                    END
     
	        
    SET @strSql =@strSql + @strSql1 +  @strCredit

	

    SET @strSql = CONCAT(@strSql,' SELECT *,CASE WHEN DATEDIFF (day ,[dbo].[FormatedDateFromDate](DueDate,',@numDomainID,') ,GETDATE()) > 0 
										  THEN DATEDIFF (day ,[dbo].[FormatedDateFromDate](DueDate,',@numDomainID,') ,GETDATE()) 
										  ELSE NULL 
									 END AS DaysLate,
									 CASE WHEN DATEDIFF (day ,[dbo].[FormatedDateFromDate](DueDate,',@numDomainID,') ,GETDATE()) > 0 
										  THEN + ''',' Past Due (', ''' + CAST(DATEDIFF (day ,[dbo].[FormatedDateFromDate](DueDate,',@numDomainID,') ,GETDATE()) AS VARCHAR(10))+''',')','''
										  ELSE NULL 
									 END AS [Status(DaysLate)],
									 CASE WHEN ',@baseCurrency,' <> numCurrencyID THEN 2 ELSE 1 END AS CurrencyCount,
									 dbo.fn_GetChart_Of_AccountsName(numChartAcntId) vcARAccount
							FROM #TempRecords WHERE (ISNULL(BalanceDue,0) <> 0 or [numOppId]=-1)')
	
	IF ISNULL(@vcRegularSearch,'') <> ''
	BEGIN
		SET @strSql = CONCAT(@strSql,' AND ',@vcRegularSearch)
	END
 
   SET @strSql = CONCAT(@strSql,' DROP TABLE #TempRecords')
   
	PRINT CAST(@strSql AS NTEXt)
	EXEC(@strSql)
	        
     
  END
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetAddressDetails' ) 
    DROP PROCEDURE USP_GetAddressDetails
GO
CREATE PROCEDURE USP_GetAddressDetails
    @tintMode TINYINT,
    @numDomainID NUMERIC,
    @numRecordID NUMERIC,
    @numAddressID NUMERIC,
    @tintAddressType TINYINT,
    @tintAddressOf TINYINT
AS 
BEGIN

    IF @tintMode = 1 
    BEGIN
        SELECT  numAddressID,
                vcAddressName,
                vcStreet,
                vcCity,
                vcPostalCode,
                numState,
                numCountry,
                bitIsPrimary,
				bitResidential,
                tintAddressOf,
                tintAddressType,
                '<pre>' + isnull(vcStreet,'') + '</pre> ' + isnull(vcCity,'') + ', '
                        + isnull(dbo.fn_GetState(numState),'') + ' '
                        + isnull(vcPostalCode,'') + ' <br>'
                        + isnull(dbo.fn_GetListItemName(numCountry),'') vcFullAddress
				,ISNULL((SELECT TOP 1 numWarehouseID FROM Warehouses WHERE numDomainID=@numDomainID AND numAddressID=AddressDetails.numAddressID),0) AS numWarehouseID
				,ISNULL(numContact,0) numContact
				,ISNULL(bitAltContact,0) bitAltContact
				,ISNULL(vcAltContact,'') vcAltContact
				,(CASE 
					WHEN tintAddressOf = 2 
					THEN ISNULL((SELECT CI.vcCompanyName FROM DivisionMaster DM INNER JOIN CompanyInfo CI ON DM.numCompanyID=CI.numCompanyId WHERE DM.numDomainID=@numDomainID AND DM.numDivisionID=AddressDetails.numRecordID),'')
					ELSE ISNULL((SELECT CI.vcCompanyName FROM AdditionalContactsInformation ACI INNER JOIN DivisionMaster DM ON ACI.numDivisionId = DM.numDivisionID INNER JOIN CompanyInfo CI ON DM.numCompanyID=CI.numCompanyId WHERE ACI.numDomainID=@numDomainID AND DM.numDivisionID=AddressDetails.numRecordID),'')
				END) vcCompanyName
        FROM    dbo.AddressDetails
        WHERE   numDomainID = @numDomainID
                AND numAddressID = @numAddressID
            
    END
    IF @tintMode = 2
    BEGIN
        SELECT  numAddressID,
                vcAddressName,
				vcStreet,
                vcCity,
                isnull(vcPostalCode,'') vcPostalCode,
                numState,
                numCountry,
				'<pre>' + isnull(vcStreet,'') + '</pre> ' + isnull(vcCity,'') + ', '
                        + isnull(dbo.fn_GetState(numState),'') + ' '
                        + isnull(vcPostalCode,'') + ' <br>'
                        + isnull(dbo.fn_GetListItemName(numCountry),'') vcFullAddress
				,ISNULL(numContact,0) numContact
				,ISNULL(bitAltContact,0) bitAltContact
				,ISNULL(vcAltContact,'') vcAltContact
				,(CASE WHEN @tintAddressOf=2 THEN ISNULL((SELECT TOP 1 numContactID FROM AdditionalCOntactsInformation WHERE numDomainID=@numDomainID AND numDivisionId=AddressDetails.numRecordID),0) ELSE 0 END) numPrimaryContact
					
				,isnull(vcStreet + ', ','')  + isnull(vcCity + ', ','') 
                        + isnull(dbo.fn_GetState(numState) + ', ','') 
                        + isnull(vcPostalCode + ', ','') 
                        + isnull(dbo.fn_GetListItemName(numCountry),'') FullAddress
				,(CASE 
					WHEN tintAddressOf = 2 
					THEN ISNULL((SELECT CI.vcCompanyName FROM DivisionMaster DM INNER JOIN CompanyInfo CI ON DM.numCompanyID=CI.numCompanyId WHERE DM.numDomainID=@numDomainID AND DM.numDivisionID=AddressDetails.numRecordID),'')
					ELSE ISNULL((SELECT CI.vcCompanyName FROM AdditionalContactsInformation ACI INNER JOIN DivisionMaster DM ON ACI.numDivisionId = DM.numDivisionID INNER JOIN CompanyInfo CI ON DM.numCompanyID=CI.numCompanyId WHERE ACI.numDomainID=@numDomainID AND DM.numDivisionID=AddressDetails.numRecordID),'')
				END) vcCompanyName
        FROM    dbo.AddressDetails
        WHERE   numDomainID = @numDomainID
                AND numRecordID = @numRecordID	
                AND tintAddressOf = @tintAddressOf
                AND tintAddressType = @tintAddressType
        ORDER BY bitIsPrimary desc
    END
    IF @tintMode = 3
        BEGIN
            SELECT  numAddressID,
                    vcAddressName,
                    vcStreet,
                    vcCity,
                    vcPostalCode,
                    numState,
                    numCountry,
                    bitIsPrimary,
					bitResidential,
                    tintAddressOf,
                    tintAddressType,
                    '<pre>' + isnull(vcStreet,'') + '</pre> ' + isnull(vcCity,'') + ', '
                            + isnull(dbo.fn_GetState(numState),'') + ' '
                            + isnull(vcPostalCode,'') + ' <br>'
                            + isnull(dbo.fn_GetListItemName(numCountry),'') vcFullAddress
            FROM    dbo.AddressDetails
            WHERE   numDomainID = @numDomainID
                    AND numRecordID = @numRecordID	
                    AND tintAddressOf = @tintAddressOf
                    AND tintAddressType = @tintAddressType
                    AND bitIsPrimary = 1
            ORDER BY bitIsPrimary desc
        END

		IF @tintMode = 4
        BEGIN
            SELECT  numAddressID,
                    vcAddressName,
					isnull(vcPostalCode,'') vcPostalCode
					,ISNULL(numContact,0) numContact
					,ISNULL(bitAltContact,0) bitAltContact
					,ISNULL(vcAltContact,'') vcAltContact
					,isnull(vcStreet + ', ','') + isnull(vcCity + ', ','')
                            + isnull(dbo.fn_GetState(numState) + ', ','')
                            + isnull(vcPostalCode + ', ','') 
                            + isnull(dbo.fn_GetListItemName(numCountry),'') FullAddress
					, ISNULL((SELECT TOP 1 vcStateCode FROM ShippingStateMaster where vcStateName= isnull(dbo.fn_GetState(numState),'')),'') AS StateAbbr
					, ISNULL((SELECT TOP 1 vcCountryCode from ShippingCountryMaster where vcCountryName= isnull(dbo.fn_GetListItemName(numCountry),'')),'') AS CountryAbbr
		
            FROM    dbo.AddressDetails
            WHERE   numDomainID = @numDomainID
                    AND numRecordID = @numRecordID	
                    --AND tintAddressOf = @tintAddressOf
                    --AND tintAddressType = @tintAddressType
					AND numAddressID = @numAddressID
            ORDER BY bitIsPrimary desc
        END
    
        

            
END
/****** Object:  StoredProcedure [dbo].[Usp_getCorrespondance]    Script Date: 07/26/2008 16:17:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcorrespondance')
DROP PROCEDURE usp_getcorrespondance
GO
CREATE PROCEDURE [dbo].[usp_getcorrespondance]                          
@FromDate as datetime,                          
@toDate as datetime,                          
@numContactId as numeric,                          
@vcMessageFrom as varchar(200)='' ,                        
@tintSortOrder as numeric,                        
@SeachKeyword as varchar(100),                        
@CurrentPage as INT,                        
@PageSize as INT,                        
@TotRecs as numeric output,                        
@columnName as varchar(100),                        
@columnSortOrder as varchar(50),                        
@filter as numeric,                        
@numdivisionId as numeric =0,              
@numDomainID as numeric(9)=0,              
@numCaseID as numeric(9)=0,              
@ClientTimeZoneOffset INT,
@numOpenRecordID NUMERIC=0,
@tintMode TINYINT=0,
@bitOpenCommu BIT=0,
@numUserCntID NUMERIC=0,
@vcTypes VARCHAR(200)
as 
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
DECLARE @vcUserLoginEmail AS VARCHAR(500)=''
SET @vcUserLoginEmail = (SELECT TOP 1 replace(vcEmailID,'''','''''') FROM UserMaster where numUserDetailId=@numUserCntID)
set @vcMessageFrom=replace(@vcMessageFrom,'''','|')
                         
if @filter>0 set @tintSortOrder=  @filter
              
if ((@filter <=2 and @tintSortOrder<=2) or (@tintSortOrder>2 and @filter>2))                 
begin  
                                                                  
 declare @strSql as nvarchar(MAX)  =''     
                                  
  declare @firstRec as integer                                                        
  declare @lastRec as integer                                                        
  set @firstRec= (@CurrentPage-1) * @PageSize                                                        
  set @lastRec= (@CurrentPage*@PageSize+1)                 
  set @strSql=''                

 if (@filter <=2 and @tintSortOrder<=2 and @bitOpenCommu=0)                 
 begin  
 
 declare @strCondition as varchar(MAX);
 declare @vcContactEmail as varchar(MAX)='';
              
  if @numdivisionId =0 and  @vcMessageFrom <> '' 
 BEGIN 
	If @filter = 1 --ReceivedMessages
		set  @strCondition=' and (vcFrom like ''%'+ REPLACE(@vcMessageFrom,'''','''''')+'%'')  ' 
	ELSE IF @filter=2 --SentMessages
		set  @strCondition=' and (vcTo like ''%'+ REPLACE(@vcMessageFrom,'''','''''')+'%'')  ' 
	ELSE
		set  @strCondition=' and (vcFrom like ''%'+ REPLACE(@vcMessageFrom,'''','''''')+'%'' or vcTo like ''%'+ REPLACE(@vcMessageFrom,'''','''''')+'%'')  ' 
 END
  else if @numdivisionId <>0 
 BEGIN

 /* This approach does not work as numEmailId stores who sent email, id of sender
  set @strCondition=' and HDR.numEmailID in (
  SELECT numEmailID FROM dbo.EmailMaster WHERE numDomainID= ' + CONVERT(VARCHAR(15),@numDomainID) +' AND numContactID 
  IN (SELECT numcontactid FROM dbo.AdditionalContactsInformation WHERE numDivisionID = ' + convert(varchar(15),@numDivisionID) + ' AND numDomainID = ' + CONVERT(VARCHAR(15),@numDomainID) +' ))';
*/
-- bug fix id 1591,1628 
  DECLARE @TnumContactID NUMERIC(9);set @strCondition=''

  select TOP 1 @TnumContactID=numContactID,@vcContactEmail=replace(vcEmail,'''','''''') from AdditionalContactsInformation where numdivisionid=@numdivisionId and len(vcEmail)>2
  ORDER BY numContactID ASC
  SET @vcContactEmail = ISNULL(@vcContactEmail,'')
  WHILE @TnumContactID>0
  BEGIN
   if len(@strCondition)>0
    SET @strCondition=@strCondition + ' or '

	If @filter = 1 --ReceivedMessages
		set  @strCondition = @strCondition + ' (vcFrom like ''%'+ REPLACE(@vcContactEmail,'''','''''') +'%'')  ' 
	ELSE IF @filter=2 --SentMessages
		set  @strCondition= @strCondition + ' (vcTo like ''%'+ REPLACE(@vcContactEmail,'''','''''') +'%'')  ' 
	ELSE
		set  @strCondition= @strCondition + ' (vcFrom like ''%'+ REPLACE(@vcContactEmail,'''','''''')+'%'' or vcTo like ''%'+ REPLACE(@vcContactEmail,'''','''''')+'%'')  '

   select TOP 1 @TnumContactID=numContactID,@vcContactEmail=vcEmail from AdditionalContactsInformation where numdivisionid=@numdivisionId and len(vcEmail)>2 and numContactID>@TnumContactID
   ORDER BY numContactID ASC

   IF @@rowcount = 0 SET @TnumContactID = 0 
  END
 
  if len(@strCondition)>0
   SET @strCondition= 'and (' + @strCondition + ')'
  else if @tintMode <> 6 AND @tintMode <> 5
   SET @strCondition=' and (1=0)' 
  --set @strCondition=' and DTL.numEmailId  in (select numEmailId from EmailMaster where vcEmailID in (select vcEmail  from AdditionalContactsInformation where numdivisionid='''+ convert(varchar(15),@numdivisionId)+''' ))    '          
  END
  else if @numContactId>0 
 BEGIN
  select @vcContactEmail=isnull(replace(vcEmail,'''',''''''),'') from AdditionalContactsInformation where numContactID=@numContactId and len(vcEmail)>2
  SET @vcContactEmail = ISNULL(@vcContactEmail,'')
  if len(@vcContactEmail)>0
	If @filter = 1 --ReceivedMessages
		set  @strCondition=' and (vcFrom like ''%'+ REPLACE(@vcContactEmail,'''','''''') +'%'')  ' 
	ELSE IF @filter=2 --SentMessages
		set  @strCondition=' and (vcTo like ''%'+ REPLACE(@vcContactEmail,'''','''''')+'%'')  ' 
	ELSE
		set  @strCondition=' and (vcFrom like ''%'+ REPLACE(@vcContactEmail,'''','''''')+'%'' or vcTo like ''%'+ REPLACE(@vcContactEmail,'''','''''')+'%'')' 
  else if @tintMode <> 6 AND @tintMode <> 5
   SET @strCondition=' and (1=0)'
 END
 else 
  SET @strCondition=''

  CREATE TABLE #TEMPData
  (
		numTotalRecords INT
		,tintCountType TINYINT
		,numEmailHstrID NUMERIC(18,0)
		,DelData VARCHAR(200)
		,bintCreatedOn DATETIME
		,[date] VARCHAR(100)
		,[Subject] VARCHAR(MAX)
		,[type] VARCHAR(100)
		,phone VARCHAR(100)
		,assignedto VARCHAR(100)
		,caseid NUMERIC(18,0)
		,vcCasenumber VARCHAR(200)
		,caseTimeid NUMERIC(18,0)
		,caseExpid NUMERIC(18,0)
		,tinttype NUMERIC(18,0)
		,dtCreatedDate DATETIME
		,[From] VARCHAR(MAX)
		,bitClosedflag BIT
		,bitHasAttachments BIT
		,vcBody VARCHAR(MAX)
		,InlineEdit VARCHAR(1000)
		,numCreatedBy NUMERIC(18,0)
		,numOrgTerId NUMERIC(18,0)
		,TypeClass VARCHAR(100)
  )


  set @strSql= 'INSERT INTO 
					#TEMPData 
				SELECT 
					COUNT(*) OVER() AS numTotalRecords
					,1 AS tintCountType
					,HDR.numEmailHstrID
					,convert(varchar(15),HDR.numEmailHstrID)+''~1~0''+convert(varchar(15),HDR.numUserCntId) as DelData
					,HDR.dtReceivedOn AS bintCreatedOn
					,dbo.FormatedDateTimeFromDate(DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+', dtReceivedOn),'+convert(varchar(15),@numDomainID)+') as [date]
					,vcSubject as [Subject]
					,CASE WHEN vcFrom like ''%'+CAST(REPLACE(@vcUserLoginEmail,'''','''''') AS VARCHAR(300))+'%'' THEN ''Email Out'' ELSE ''Email In'' END as [type]
					,'''' as phone
					,'''' as assignedto
					,0 as caseid
					,null as vcCasenumber
					,0 as caseTimeid
					,0 as caseExpid
					,hdr.tinttype
					,HDR.dtReceivedOn as dtCreatedDate
					,vcFrom + '','' + vcTo as [From]
					,0 as bitClosedflag
					,ISNULL(HDR.bitHasAttachments,0) bitHasAttachments
					,CASE WHEN LEN(Cast(vcBodyText AS VARCHAR(1000)))>150 THEN SUBSTRING(vcBodyText,0,150) + ''...'' ELSE Cast(vcBodyText AS VARCHAR(1000)) END AS vcBody
					,''''  AS InlineEdit
					,HDR.numUserCntId AS numCreatedBy
					,0 AS numOrgTerId
					,CASE WHEN vcFrom like ''%'+CAST(REPLACE(@vcUserLoginEmail,'''','''''') AS VARCHAR(300))+'%'' THEN ''redColorLabel'' ELSE ''greenColorLabel'' END as TypeClass
			  FROM EmailHistory HDR                                 
			  LEFT JOIN [Correspondence] CR ON CR.numEmailHistoryID = HDR.numEmailHstrID ' 
     

   set  @strSql=@strSql +' where ISNULL(HDR.bitInvisible,0) = 0 AND HDR.numNodeId IN (SELECT numNodeID FROM InboxTreeSort WHERE vcNodeName=''Sent Mail'' or vcNodeName=''Inbox'' or vcNodeName=''Outbox'' or vcNodeName=''Sent Items'') AND HDR.numDomainID='+convert(varchar(15),@numDomainID)
   + ' AND (CR.numOpenRecordID = '+ convert(varchar(15),@numOpenRecordID) +' OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
   + (CASE WHEN ISNULL(@vcTypes,'') <> '' THEN ' AND 1 = 0' ELSE ' AND 1 = 1' END)
   
   IF(@FromDate IS NOT NULL AND @FromDate<>'')
   BEGIN
  SET @strSql = @strSql +' and  (dtReceivedOn between '''+ convert(varchar(30),@FromDate)+''' and '''+ convert(varchar(30),@ToDate)+''') '
  END
 set  @strSql=@strSql+ @strCondition


  IF @tintMode=1 SET @strSql=@strSql+ ' and  (CR.tintCorrType IN(1,3) OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
  IF @tintMode=2 SET @strSql=@strSql+ ' and  (CR.tintCorrType IN(2,4) OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
  IF @tintMode=5 SET @strSql=@strSql+ ' and  (CR.tintCorrType IN(5) OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
  IF @tintMode=6 SET @strSql=@strSql+ ' and  (CR.tintCorrType IN(6) OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
  
  if @filter =0 set  @strSql=@strSql+ ' and (hdr.tinttype=1 or hdr.tinttype=2 or  hdr.tinttype=5)'                  
  else if @filter =1 OR @filter =2 set  @strSql=@strSql+ ' and (hdr.tinttype=1 OR hdr.tinttype=2)'                                             
  if @filter > 2 set  @strSql=@strSql+ ' and hdr.tinttype=0'                 
                        
  if @tintSortOrder =0 and @SeachKeyword <> ''                 
  begin                
   set  @strSql=@strSql+ ' and (vcFrom like ''%'+ @SeachKeyword+'%'' or vcTo like ''%'+ @SeachKeyword+'%'' or vcsubject like ''%'+@SeachKeyword+'%'' or vcBody like ''%'+@SeachKeyword+'%'')'               
  end                
  if @tintSortOrder =1 and @SeachKeyword <> ''                 
  begin                
     set  @strSql=@strSql+ ' and (vcFrom like ''%'+ @SeachKeyword+'%'' or vcsubject like ''%'+@SeachKeyword+'%'' or  vcBody like ''%'+@SeachKeyword+'%'')'                 
  end                
  if @tintSortOrder =2 and @SeachKeyword <> ''                 
  begin                
     set  @strSql=@strSql+ ' and (vcTo like ''%'+ @SeachKeyword+'%'' or vcsubject like ''%'+@SeachKeyword+'%'' or  vcBody like ''%'+@SeachKeyword+'%'')'                 
  end                            
 end                

 

 if ((@filter=0 or @filter>2) and (@tintSortOrder=0 or @tintSortOrder>2))                
 begin                
  set  @strSql=@strSql+ '                     
  INSERT INTO #TEMPData SELECT COUNT(*) OVER() AS numTotalRecords, 2 AS tintCountType, c.numCommId as numEmailHstrID,convert(varchar(15),C.numCommId)+''~2~''+convert(varchar(15),C.numCreatedBy) as DelData,CASE WHEN c.bitTask = 973 THEN dtCreatedDate WHEN c.bitTask = 974 THEN dtCreatedDate WHEN c.bitTask = 971 THEN dtStartTime ELSE dtEndTime END AS bintCreatedOn,
  case when c.bitTask=973 then dbo.FormatedDateTimeFromDate(DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+', dtCreatedDate),'+convert(varchar(15),@numDomainID)+') 
  when c.bitTask=974 then dbo.FormatedDateTimeFromDate(DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+', dtEndTime),'+convert(varchar(15),@numDomainID)+') 
  when c.bitTask=971 THEN dbo.FormatedDateFromDate(DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+', dtEndTime),'+convert(varchar(15),@numDomainID)+') + '' '' + RIGHT(CONVERT(VARCHAR,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+', [dtStartTime]),100),7)  +'' - '' + RIGHT(CONVERT(VARCHAR,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+', [dtEndTime]),100),7)
  else  dbo.FormatedDateFromDate(DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+', dtEndTime),'+convert(varchar(15),@numDomainID)+') + '' '' + RIGHT(CONVERT(VARCHAR,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+', [dtStartTime]),100),7)  +'' - '' + RIGHT(CONVERT(VARCHAR,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+', [dtEndTime]),100),7)  END AS [date],
  convert(varchar(8000),textdetails) as [Subject],                                                                         
 dbo.fn_GetListItemName(c.bitTask)  AS [Type],                                                                 
  A1.vcFirstName +'' ''+ A1.vcLastName +'' ''+                                      
  case when A1.numPhone<>'''' then + A1.numPhone +case when A1.numPhoneExtension<>'''' then '' - '' + A1.numPhoneExtension else '''' end  else '''' end as [Phone],                        
  A2.vcFirstName+ '' ''+A2.vcLastName  as assignedto,                         
  c.caseid,                           
  (select top 1 vcCaseNumber from cases where cases.numcaseid= c.caseid )as vcCasenumber,                               
  isnull(caseTimeid,0)as caseTimeid,                              
  isnull(caseExpid,0)as caseExpid  ,                        
  1 as tinttype ,dtCreatedDate,'''' as [FROM]     ,isnull(C.bitClosedflag,0) as bitClosedflag,0 as bitHasAttachments,
'''' as vcBody,
''id="Tickler~184~False~''+CAST(c.bitTask AS VARCHAR)+''~''+CAST(c.numCommId AS VARCHAR)+''" class="editable_textarea" onmousemove="bgColor=''''lightgoldenRodYellow''''"  title="Double click to edit..." bgcolor="lightgoldenRodYellow" '' AS InlineEdit,
C.numCreatedBy,
ISNULL(Div.numTerId,0) AS numOrgTerId,
''blueColorLabel'' as TypeClass
  from  Communication C                  
  JOIN AdditionalContactsInformation  A1                                           
  ON c.numContactId = A1.numContactId
  LEFT JOIN CommunicationLinkedOrganization clo ON C.numCommID=clo.numCommID                                                                             
  left join AdditionalContactsInformation A2 on A2.numContactID=c.numAssign
  left join UserMaster UM on A2.numContactid=UM.numUserDetailId and isnull(UM.bitActivateFlag,0)=1                                                                           
  left join listdetails on numActivity=numListID                           
  LEFT JOIN [Correspondence] CR ON CR.numCommID = C.numCommID
  JOIN DivisionMaster Div                                                                         
 ON A1.numDivisionId = Div.numDivisionID    
  where  C.numDomainID=' +convert(varchar(15),@numDomainID)
  + (CASE WHEN ISNULL(@vcTypes,'') <> '' THEN ' AND C.bitTask IN (SELECT Id FROM dbo.SplitIDs(''' + ISNULL(@vcTypes,'') + ''','',''))' ELSE '' END) + 
  + ' AND (numOpenRecordID = '+ convert(varchar(15),@numOpenRecordID) +' OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
  if (@numdivisionId =0 and @numContactId<>0) set  @strSql=@strSql+' and C.numContactId='+ convert(varchar(15),@numContactId)                        
  if @numdivisionId <> 0 set  @strSql=@strSql+' and  (C.numDivisionId='+ convert(varchar(15),@numdivisionId) + ' OR clo.numDivisionID='+ convert(varchar(15),@numdivisionId) + ') '             
  if @numCaseID>0  set  @strSql=@strSql+' and  C.CaseId='+ convert(varchar(15),@numCaseID)                       
  if @filter =0  
  BEGIN
	IF @tintMode <> 9
	BEGIN
		
		IF(@FromDate IS NOT NULL AND @FromDate<>'')
		BEGIN
			SET @strSql=@strSql+ ' and dtStartTime  >= '''+ convert(varchar(30),@FromDate)+''' and dtStartTime  <= '''+ convert(varchar(30),@ToDate)  +''''
		END
	END
  END
  else
    SET @strSql=@strSql+ ' and c.bitTask=' + CONVERT(varchar(12),@filter)  
		IF(@FromDate IS NOT NULL AND @FromDate<>'')
		BEGIN
			SET @strSql=@strSql+ ' and ((dtStartTime  >= '''+ convert(varchar(30),@FromDate)+''' and dtStartTime  <= '''+ convert(varchar(30),@ToDate)  +''') OR (dtEndTime  >= '''+ convert(varchar(30),@FromDate)+''' and dtEndTime  <= '''+ convert(varchar(30),@ToDate)  +'''))'
		END
  IF @tintMode=1 SET @strSql=@strSql+ ' and  (CR.tintCorrType IN(1,3) OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
  IF @tintMode=2 SET @strSql=@strSql+ ' and  (CR.tintCorrType IN(2,4) OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
  IF @tintMode=5 SET @strSql=@strSql+ ' and  (CR.tintCorrType IN(5) OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
  IF @tintMode=6 SET @strSql=@strSql+ ' and  (CR.tintCorrType IN(6) OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
                          
  IF @tintSortOrder =0 and @SeachKeyword <>'' 
	SET @strSql=@strSql+ ' and textdetails like ''%'+@SeachKeyword+'%'''
  ELSE
	IF ISNULL(@filter,0) = 0 
	BEGIN
		IF @SeachKeyword <>''
		BEGIN
			SET @strSql=@strSql+ ' and textdetails like ''%'+@SeachKeyword+'%'''
		END
	END
	ELSE
	BEGIN
		SET @strSql=@strSql+ ' and c.bitTask=' + CONVERT(varchar(12),@filter)
		
		IF @SeachKeyword <>''
		BEGIN
			SET @strSql= @strSql + ' and textdetails like ''%'+@SeachKeyword+'%'''
		END                     
	END
  IF @bitOpenCommu=1  SET @strSql=@strSql+ ' and isnull(C.bitClosedflag,0)=0'
 end                




 --set @strSql='With tblCorr as (SELECT ROW_NUMBER() OVER (ORDER BY '+@columnName+' '+ @columnSortOrder+') AS RowNumber,X.* from('+@strSql+')X)'                

 set @strSql= CONCAT(@strSql,'; SELECT ROW_NUMBER() OVER(ORDER BY bintCreatedOn DESC) RowNumber, * FROM #TEMPData ORDER BY bintCreatedOn DESC OFFSET ',((@CurrentPage-1) * @PageSize),' ROWS FETCH NEXT ',@PageSize,' ROWS ONLY;')
                    

print CAST(@strSql AS NTEXT)          

set @strSql = CONCAT(@strSql,'; SET @TotRecs = ISNULL((SELECT SUM(numTotalRecords) FROM (SELECT tintCountType,numTotalRecords FROM #TEMPData GROUP BY tintCountType,numTotalRecords) X),0)')
EXEC sp_executesql @strSql,N'@TotRecs INT OUTPUT',@TotRecs OUTPUT;
                   
IF OBJECT_ID(N'tempdb..#TEMPData') IS NOT NULL
BEGIN
	DROP TABLE #TEMPData
END
                
              
end              
END
GO

/****** Object:  StoredProcedure [dbo].[USP_GetDealsList1]    Script Date: 05/07/2009 22:12:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getdealslist1')
DROP PROCEDURE usp_getdealslist1
GO
CREATE PROCEDURE [dbo].[USP_GetDealsList1]
               @numUserCntID              NUMERIC(9)  = 0,
               @numDomainID               NUMERIC(9)  = 0,
               @tintSalesUserRightType    TINYINT  = 0,
               @tintPurchaseUserRightType TINYINT  = 0,
               @tintSortOrder             TINYINT  = 4,
               @CurrentPage               INT,
               @PageSize                  INT,
               @TotRecs                   INT  OUTPUT,
               @columnName                AS VARCHAR(300),
               @columnSortOrder           AS VARCHAR(10),
               @numCompanyID              AS NUMERIC(9)  = 0,
               @bitPartner                AS BIT  = 0,
               @ClientTimeZoneOffset      AS INT,
               @tintShipped               AS TINYINT,
               @intOrder                  AS TINYINT,
               @intType                   AS TINYINT,
               @tintFilterBy              AS TINYINT  = 0,
               @numOrderStatus			  AS NUMERIC,
			   @vcRegularSearchCriteria varchar(MAX)='',
			   @vcCustomSearchCriteria varchar(MAX)='',
			   @SearchText VARCHAR(300) = '',
			   @SortChar char(1)='0',
			   @tintDashboardReminderType TINYINT = 0
AS
BEGIN
	DECLARE @PageId  AS TINYINT
	DECLARE @numFormId  AS INT 
	DECLARE @tintDecimalPoints TINYINT

	SELECT
		@tintDecimalPoints=ISNULL(tintDecimalPoints,0)
	FROM
		Domain 
	WHERE 
		numDomainId=@numDomainID

	SET @PageId = 0

	IF @inttype = 1
	BEGIN
		SET @PageId = 2
		SET @inttype = 3
		SET @numFormId=39
	END
	ELSE IF @inttype = 2
	BEGIN
		SET @PageId = 6
		SET @inttype = 4
		SET @numFormId=41
	END

	CREATE TABLE #tempForm 
	(
		tintOrder TINYINT,vcDbColumnName nvarchar(200),vcOrigDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),vcListItemType varchar(3),
		numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,
		bitIsAlphaNumeric BIT,bitIsNumeric BIT,bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),
		ListRelID numeric(9),intColumnWidth int,bitAllowFiltering BIT,vcFieldDataType CHAR(1)
	)

	-- FIRST CHECK IF GRID COLUMNS ARE CONFIGURED
	DECLARE  @Nocolumns  AS TINYINT = 0

	SELECT 
		@Nocolumns=ISNULL(SUM(TotalRow),0) 
	FROM
	(            
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND numRelCntType = @numFormId
			AND 1 = (CASE WHEN @numFormId=39 AND numFieldID=771 THEN 0 ELSE 1 END)
		UNION 
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicCustomColumns 
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND numRelCntType = @numFormId
	) TotalRows

	-- IF GRID COLUMNS ARE NOT CONFIGURES THEN CHECK IF MASTER FORM CONFIGURATION IS CREATED BY ADMINISTRATOR
	DECLARE @IsMasterConfAvailable BIT = 0
	DECLARE @numUserGroup NUMERIC(18,0)

	IF @Nocolumns=0
	BEGIN
		SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

		IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = @numFormId AND numRelCntType=@numFormId AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
		BEGIN
			SET @IsMasterConfAvailable = 1
		END
	END

	--IF MASTERCONFIGURATION IS AVAILABLE THEN LOAD IT OTHERWISE LOAD DEFAULT COLUMNS
	IF @IsMasterConfAvailable = 1
	BEGIN
		INSERT INTO DycFormConfigurationDetails 
		(
			numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
		)
		SELECT 
			@numFormId,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,@numFormId,1,0,intColumnWidth
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.numRelCntType=@numFormId AND 
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
			AND 1 = (CASE WHEN @numFormId=39 AND numFieldID=771 THEN 0 ELSE 1 END)
		UNION
		SELECT 
			@numFormId,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,@numFormId,1,1,intColumnWidth
		FROM 
			View_DynamicCustomColumnsMasterConfig
		WHERE 
			View_DynamicCustomColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.numRelCntType=@numFormId AND 
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1

		INSERT INTO 
			#tempForm
		SELECT 
			(intRowNum + 1) as tintOrder, vcDbColumnName,	vcOrigDbColumnName,	ISNULL(vcCultureFieldName,vcFieldName), vcAssociatedControlType,
			vcListItemType, numListID, vcLookBackTableName,	bitCustom, numFieldId, bitAllowSorting,	bitInlineEdit, bitIsRequired,
			bitIsEmail,	bitIsAlphaNumeric, bitIsNumeric, bitIsLengthValidation,	intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,
			ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType 
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.numRelCntType=@numFormId AND 
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName, vcAssociatedControlType,'' as vcListItemType,numListID,'',
			bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,''
		FROM 
			View_DynamicCustomColumnsMasterConfig
		WHERE 
			View_DynamicCustomColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.numRelCntType=@numFormId AND 
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1
		ORDER BY 
			tintOrder ASC  
	END
	ELSE                                        
	BEGIN
		IF @Nocolumns=0
		BEGIN
			INSERT INTO DycFormConfigurationDetails 
			(
				numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
			)
			SELECT 
				@numFormId,numFieldId,0,Row_number() over(order by tintRow desc),@numDomainID,@numUserCntID,@numFormId,1,0,intColumnWidth
			FROM 
				View_DynamicDefaultColumns
			WHERE 
				numFormId=@numFormId 
				AND bitDefault=1 
				AND ISNULL(bitSettingField,0)=1 
				AND numDomainID=@numDomainID
				AND 1 = (CASE WHEN @numFormId=39 AND numFieldID=771 THEN 0 ELSE 1 END)
			ORDER BY 
				tintOrder asc   
		END

		INSERT INTO 
			#tempForm
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
			vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName,bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,bitFieldMessage,
			vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType  
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND ISNULL(bitSettingField,0)=1 
			AND ISNULL(bitCustom,0)=0  
			AND numRelCntType = @numFormId
			AND 1 = (CASE WHEN @numFormId=39 AND numFieldID=771 THEN 0 ELSE 1 END)
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName,vcAssociatedControlType,'' as vcListItemType,numListID,'',bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,'' 
		FROM 
			View_DynamicCustomColumns
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND ISNULL(bitCustom,0)=1 
			AND numRelCntType = @numFormId
		ORDER BY 
			tintOrder asc  
	END
	UPDATE #tempForm SET vcFieldMessage = CAST((SELECT numFieldId FROM DycFieldMaster WHERE vcDbColumnName='intUsedShippingCompany' AND numModuleID=3)  AS varchar)+'#'+CAST((SELECT numFieldId FROM DycFieldMaster WHERE vcDbColumnName='numShippingService' AND numModuleID=3)  AS varchar) 
	WHERE vcDbColumnName='vcOrderedShipped'
	UPDATE #tempForm SET vcFieldMessage = CAST((SELECT numFieldId FROM DycFieldMaster WHERE vcDbColumnName='monAmountPaid' AND numModuleID=3)  AS varchar)
	WHERE vcDbColumnName='vcPOppName'
	DECLARE  @strColumns AS VARCHAR(MAX)
	SET @strColumns = ' ADC.numContactId, ADC.vcEmail, Div.numDivisionID,ISNULL(Div.numTerID,0) numTerID, opp.numRecOwner as numRecOwner, Div.tintCRMType, opp.numOppId, ISNULL(Opp.monDealAmount,0) monDealAmount 
	,ISNULL(opp.numShippingService,0) AS numShippingService,ADC.vcFirstName+'' ''+ADC.vcLastName AS vcContactName,ADC.numPhone AS numContactPhone,ADC.numPhoneExtension AS numContactPhoneExtension,ADC.vcEmail AS vcContactEmail,Opp.vcPOppName,cmp.vcCompanyName,ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0) as monAmountPaid '
	--Corr.bitIsEmailSent,

	DECLARE  @tintOrder  AS TINYINT;SET @tintOrder = 0
	DECLARE  @vcFieldName  AS VARCHAR(50)
	DECLARE  @vcListItemType  AS VARCHAR(3)
	DECLARE  @vcListItemType1  AS VARCHAR(1)
	DECLARE  @vcAssociatedControlType VARCHAR(30)
	DECLARE  @numListID  AS NUMERIC(9)
	DECLARE  @vcDbColumnName VARCHAR(200)
	DECLARE  @WhereCondition VARCHAR(2000);SET @WhereCondition = ''
	DECLARE  @vcLookBackTableName VARCHAR(2000)
	DECLARE  @bitCustom  AS BIT
	Declare @numFieldId as numeric  
	DECLARE @bitAllowSorting AS CHAR(1)   
	DECLARE @bitAllowEdit AS CHAR(1)                 
	DECLARE @vcColumnName AS VARCHAR(500)          
	
	Declare @ListRelID as numeric(9) 
	DECLARE @SearchQuery AS VARCHAR(MAX) = ''        

	SELECT TOP 1 
		@tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,@vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,
		@numListID=numListID,@vcLookBackTableName=vcLookBackTableName ,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,
		@ListRelID=ListRelID
	FROM 
		#tempForm 
	ORDER BY 
		tintOrder ASC            

	WHILE @tintOrder > 0
    BEGIN
		IF @bitCustom = 0
		BEGIN
			DECLARE  @Prefix  AS VARCHAR(10)

			IF @vcLookBackTableName = 'AdditionalContactsInformation'
				SET @Prefix = 'ADC.'
			IF @vcLookBackTableName = 'DivisionMaster'
				SET @Prefix = 'Div.'
			IF @vcLookBackTableName = 'OpportunityMaster'
				SET @PreFix = 'Opp.'
			IF @vcLookBackTableName = 'OpportunityRecurring'
				SET @PreFix = 'OPR.'
			IF @vcLookBackTableName = 'CompanyInfo'
				SET @PreFix = 'cmp.'
			IF @vcLookBackTableName = 'ProjectProgress'
				SET @PreFix = 'PP.'
			SET @vcColumnName= @vcDbColumnName+'~'+ convert(varchar(10),@numFieldId) +'~0'

			IF @vcAssociatedControlType = 'SelectBox'
			BEGIN
				-- KEEP THIS IF CONDITION SEPERATE FROM IF ELSE
				IF @vcDbColumnName = 'numStatus'
				BEGIN
					SET @strColumns=@strColumns+ ' ,(SELECT COUNT(OpportunityItems.numoppitemtCode) FROM OpportunityItems WHERE OpportunityItems.bitItemPriceApprovalRequired=1 AND OpportunityItems.numOppId=Opp.numOppId) ApprovalMarginCount'
				END


				IF @vcDbColumnName = 'numPartner'
				BEGIN
					SET @strColumns=@strColumns+ ' ,(Opp.numPartner) '+' ['+ @vcColumnName+']'    
					SET @strColumns=@strColumns+ ' ,(SELECT D.vcPartnerCode+''-''+C.vcCompanyName FROM DivisionMaster AS D LEFT JOIN CompanyInfo AS C ON D.numCompanyID=C.numCompanyID WHERE D.numDivisionID=Opp.numPartner) AS vcPartner' 
				END
				ELSE IF @vcDbColumnName = 'vcSignatureType'
				BEGIN
					SET @strColumns=@strColumns+ ' ,(SELECT CASE WHEN vcSignatureType = 0 THEN ''Service Default''
																	WHEN vcSignatureType = 1 THEN ''Adult Signature Required''
																	WHEN vcSignatureType = 2 THEN ''Direct Signature''
																	WHEN vcSignatureType = 3 THEN ''InDirect Signature''
																	WHEN vcSignatureType = 4 THEN ''No Signature Required''
																ELSE '''' END 
					FROM DivisionMaster AS D INNER JOIN DivisionMasterShippingConfiguration AS DMSC ON DMSC.numDivisionID=D.numDivisionID 
					WHERE D.numDivisionID=Div.numDivisionID) '  + ' [' + @vcColumnName + ']'
				END
				ELSE IF @vcDbColumnName = 'numShippingService'
				BEGIN
					SET @strColumns=@strColumns+ ' ,ISNULL((SELECT vcShipmentService FROM ShippingService WHERE (numDomainID=Opp.numDomainID OR ISNULL(numDomainID,0)=0) AND numShipmentServiceID = ISNULL(Opp.numShippingService,0)),'''')' + ' [' + @vcColumnName + ']'
				END
				ELSE IF @vcDbColumnName = 'tintEDIStatus'
				BEGIN
					--SET @strColumns = @strColumns + ',(CASE Opp.tintEDIStatus WHEN 1 THEN ''850 Received'' WHEN 2 THEN ''850 Acknowledged'' WHEN 3 THEN ''940 Sent'' WHEN 4 THEN ''940 Acknowledged'' WHEN 5 THEN ''856 Received'' WHEN 6 THEN ''856 Acknowledged'' WHEN 7 THEN ''856 Sent'' ELSE '''' END)' + ' [' + @vcColumnName + ']'
					SET @strColumns = @strColumns + ',(CASE Opp.tintEDIStatus WHEN 2 THEN ''850 Acknowledged'' WHEN 3 THEN ''940 Sent'' WHEN 4 THEN ''940 Acknowledged'' WHEN 5 THEN ''856 Received'' WHEN 6 THEN ''856 Acknowledged'' WHEN 7 THEN ''856 Sent'' WHEN 11 THEN ''850 Partially Created'' WHEN 12 THEN ''850 SO Created'' ELSE '''' END)' + ' [' + @vcColumnName + ']'
				END
				ELSE IF @vcDbColumnName = 'numPartenerContact'
				BEGIN
					SET @strColumns=@strColumns+ ' ,(Opp.numPartenerContact) '+' ['+ @vcColumnName+']'    
					SET @strColumns=@strColumns+ ' ,(SELECT A.vcFirstName+'' ''+ A.vcLastName FROM AdditionalContactsInformation AS A WHERE A.numContactId=Opp.numPartenerContact) AS vcPartenerContact' 
				END
				ELSE IF @vcDbColumnName = 'tintInvoicing'
				BEGIN
					SET @strColumns=CONCAT(@strColumns,', CONCAT(ISNULL((SELECT SUBSTRING((SELECT ''$^$'' + CONCAT(numOppBizDocsId,''#^#'',vcBizDocID,''#^#'',ISNULL(bitisemailsent,0),''#^#'',ISNULL(monDealAmount,0),''#^#'',ISNULL(monAmountPaid,0),''#^#'', convert(varchar(10), cast(dtFromDate as date), 101),''#^#'')
										FROM OpportunityBizDocs WHERE numOppId=Opp.numOppId AND numBizDocId = ISNULL((SELECT TOP 1 numAuthoritativeSales FROM AuthoritativeBizDocs WHERE numDomainId=' + CAST (@numDomainID AS VARCHAR) + '),287) FOR XML PATH('''')),4,200000)),'''')',
										CASE 
										WHEN CHARINDEX('tintInvoicing=3',@vcRegularSearchCriteria) > 0
											THEN ', (SELECT 
													CONCAT(''('',SUM(X.InvoicedQty),'' Out of '',SUM(X.OrderedQty),'')'')
												FROM 
												(
													SELECT
														OI.numoppitemtCode,
														ISNULL(OI.numUnitHour,0) AS OrderedQty,
														ISNULL(TempInvoice.InvoicedQty,0) AS InvoicedQty
													FROM
														OpportunityItems OI
													INNER JOIN
														Item I
													ON
														OI.numItemCode = I.numItemCode
													OUTER APPLY
													(
														SELECT
															SUM(OpportunityBizDocItems.numUnitHour) AS InvoicedQty
														FROM
															OpportunityBizDocs
														INNER JOIN
															OpportunityBizDocItems 
														ON
															OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
														WHERE
															OpportunityBizDocs.numOppId = Opp.numOppId
															AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
															AND ISNULL(OpportunityBizDocs.bitAuthoritativeBizDocs,0) = 1
													) AS TempInvoice
													WHERE
														OI.numOppID = Opp.numOppId
												) X)'

											WHEN CHARINDEX('tintInvoicing=4',@vcRegularSearchCriteria) > 0
											THEN ', (SELECT 
													CONCAT(''('',(X.InvoicedPercentage),'' % '','')'')
												FROM 
												(

													SELECT fltBreakupPercentage  AS InvoicedPercentage
													FROM dbo.OpportunityRecurring OPR 
														INNER JOIN dbo.OpportunityMaster OM ON OPR.numOppID = OM.numOppID
														LEFT JOIN dbo.OpportunityBizDocs OBD ON OBD.numOppBizDocsID = OPR.numOppBizDocID
													WHERE OPR.numOppId = Opp.numOppId 
														and tintRecurringType <> 4
												) X)'

											ELSE 
													','''''
											END,') [', @vcColumnName,']')


				END
				ELSE IF @vcDbColumnName = 'tintSource'
				BEGIN
					SET @strColumns = @strColumns + ',ISNULL(dbo.fn_GetOpportunitySourceValue(ISNULL(Opp.tintSource,0),ISNULL(Opp.tintSourceType,0),Opp.numDomainID),''Internal Order'')' + ' [' + @vcColumnName + ']'

					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'ISNULL(dbo.fn_GetOpportunitySourceValue(ISNULL(Opp.tintSource,0),ISNULL(Opp.tintSourceType,0),Opp.numDomainID),''Internal Order'') LIKE ''%' + @SearchText + '%'''
					END
				END
				ELSE IF @vcDbColumnName = 'numCampainID'
				BEGIN
					SET @strColumns = @strColumns + ', (SELECT vcCampaignName FROM CampaignMaster WHERE numCampaignID= Opp.numCampainID) ' + ' [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'ISNULL((SELECT vcCampaignName FROM CampaignMaster WHERE numCampaignID= Opp.numCampainID),'''') LIKE ''%' + @SearchText + '%'''
					END
				END
				ELSE IF @vcDbColumnName = 'numContactID'
				BEGIN
					SET @strColumns = @strColumns + ',dbo.fn_GetContactName(' + @Prefix + @vcDbColumnName + ') [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'dbo.fn_GetContactName(' + @Prefix + @vcDbColumnName + ') LIKE ''%' + @SearchText + '%'''
					END
				END
				ELSE IF @vcListItemType = 'LI'
				BEGIN
					SET @strColumns = @strColumns + ',L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData' + ' [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData LIKE ''%' + @SearchText + '%'''
					END

					SET @WhereCondition = @WhereCondition + ' left Join ListDetails L' + CONVERT(VARCHAR(3),@tintOrder) + ' on L' + CONVERT(VARCHAR(3),@tintOrder) + '.numListItemID=' + @vcDbColumnName
				END
				ELSE IF @vcListItemType = 'S'
				BEGIN
					SET @strColumns = @strColumns + ',S' + CONVERT(VARCHAR(3),@tintOrder) + '.vcState' + ' [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'S' + CONVERT(VARCHAR(3),@tintOrder) + '.vcState LIKE ''%' + @SearchText + '%'''
					END

					SET @WhereCondition = @WhereCondition + ' left join State S' + CONVERT(VARCHAR(3),@tintOrder) + ' on S' + CONVERT(VARCHAR(3),@tintOrder) + '.numStateID=' + @vcDbColumnName
				END
				ELSE IF @vcListItemType = 'BP'
				BEGIN
					SET @strColumns = @strColumns + ',SPLM.Slp_Name' + ' [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'SPLM.Slp_Name LIKE ''%' + @SearchText + '%'''
					END

					SET @WhereCondition = @WhereCondition + ' LEFT JOIN Sales_process_List_Master SPLM on SPLM.Slp_Id = OPP.numBusinessProcessID '
				END
				ELSE IF @vcListItemType = 'PP'
				BEGIN
					SET @strColumns = @strColumns + ',ISNULL(PP.intTotalProgress,0)' + ' [' + @vcColumnName + ']'

					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'ISNULL(PP.intTotalProgress,0) LIKE ''%' + @SearchText + '%'''
					END
				END
				ELSE IF @vcListItemType = 'T'
				BEGIN
					SET @strColumns = @strColumns + ',L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData' + ' [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData LIKE ''%' + @SearchText + '%'''
					END

					SET @WhereCondition = @WhereCondition + ' left Join ListDetails L' + CONVERT(VARCHAR(3),@tintOrder) + ' on L' + CONVERT(VARCHAR(3),@tintOrder) + '.numListItemID=' + @vcDbColumnName
				END
				ELSE IF @vcListItemType = 'U'
				BEGIN
					SET @strColumns = @strColumns + ',dbo.fn_GetContactName(' + @Prefix + @vcDbColumnName + ') [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'dbo.fn_GetContactName(' + @Prefix + @vcDbColumnName + ') LIKE ''%' + @SearchText + '%'''
					END
				END
				ELSE IF @vcListItemType = 'WI'
				BEGIN
					SET @strColumns = @strColumns + ', Case when isnull(OL.numSiteID,0)>0 then Sites.vcSiteName else WebAPI.vcProviderName end [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + '(Case when isnull(OL.numSiteID,0)>0 then Sites.vcSiteName else WebAPI.vcProviderName end) LIKE ''%' + @SearchText + '%'''
					END

					set @WhereCondition= @WhereCondition +' left JOIN OpportunityLinking OL on OL.numChildOppID=Opp.numOppId left join WebAPI on WebAPI.WebApiId=isnull(OL.numWebApiId,0)
													left join Sites on isnull(OL.numSiteID,0)=Sites.numSIteID and Sites.numDomainID=' + convert(varchar(15),@numDomainID)                                                       
				END 
            END
			ELSE IF @vcAssociatedControlType = 'DateField'
			BEGIN
				IF @Prefix ='OPR.'
				BEGIN
					SET @strColumns = @strColumns + ', (SELECT TOP 1 dbo.FormatedDateFromDate( DateAdd(minute,+'+convert(varchar(15),-@ClientTimeZoneOffset)+', dtRecurringDate)'+ ','+ CONVERT(VARCHAR(10),@numDomainId) +') FROM  dbo.OpportunityRecurring WHERE numOppID=OPP.numOppID AND ISNULL(numOppBizDocID,0)=0 and dtRecurringDate > dbo.GetUTCDateWithoutTime()) AS  ['+ @vcColumnName + ']'

					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + '(SELECT TOP 1 dbo.FormatedDateFromDate( DateAdd(minute,+'+convert(varchar(15),-@ClientTimeZoneOffset)+', dtRecurringDate)'+ ','+ CONVERT(VARCHAR(10),@numDomainId) +') FROM  dbo.OpportunityRecurring WHERE numOppID=OPP.numOppID AND ISNULL(numOppBizDocID,0)=0 and dtRecurringDate > dbo.GetUTCDateWithoutTime()) LIKE ''%' + @SearchText + '%'''
					END
				END
				ELSE IF @vcDbColumnName = 'dtReleaseDate'
				BEGIN
					SET @strColumns = @strColumns
                                + ',case when convert(varchar(11),'
                                + @Prefix
                                + @vcDbColumnName
                                + ')= convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getdate())) then ''<b><font color=red>Today</font></b>'''
					SET @strColumns = @strColumns
									+ 'when convert(varchar(11),'
									+ @Prefix
									+ @vcDbColumnName
									+ ')= convert(varchar(11),dateadd(day,-1,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getdate()))) then ''<b><font color=purple>Yesterday</font></b>'''
					SET @strColumns = @strColumns
									+ 'when convert(varchar(11),'
									+ @Prefix
									+ @vcDbColumnName
									+ ')= convert(varchar(11),dateadd(day,1,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getdate()))) then''<b><font color=orange>Tommorow</font></b>'' '
					SET @strColumns = @strColumns
									+ 'else dbo.FormatedDateFromDate('
									+ @Prefix
									+ @vcDbColumnName
									+ ','
									+ CONVERT(VARCHAR(10),@numDomainId)
									+ ') end  ['
									+ @vcColumnName + ']' 	

					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + @Prefix + @vcDbColumnName + ' LIKE ''%' + @SearchText + '%'''
					END

				END
				ELSE
				BEGIN
					SET @strColumns = @strColumns
                                + ',case when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','
                                + @Prefix
                                + @vcDbColumnName
                                + ') )= convert(varchar(11),getdate()) then ''<b><font color=red>Today</font></b>'''
					SET @strColumns = @strColumns
									+ 'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','
									+ @Prefix
									+ @vcDbColumnName
									+ ') )= convert(varchar(11),dateadd(day,-1,getdate())) then ''<b><font color=purple>Yesterday</font></b>'''
					SET @strColumns = @strColumns
									+ 'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','
									+ @Prefix
									+ @vcDbColumnName
									+ ') )= convert(varchar(11),dateadd(day,1,getdate())) then''<b><font color=orange>Tommorow</font></b>'' '
					SET @strColumns = @strColumns
									+ 'else dbo.FormatedDateFromDate(DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','
									+ @Prefix
									+ @vcDbColumnName
									+ '),'
									+ CONVERT(VARCHAR(10),@numDomainId)
									+ ') end  ['
									+ @vcColumnName + ']' 	

					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + @Prefix + @vcDbColumnName + ' LIKE ''%' + @SearchText + '%'''
					END
				 END	
            END
            ELSE
            IF @vcAssociatedControlType = 'TextBox'
            BEGIN
				IF @vcDbColumnName = 'vcCompactContactDetails'
				BEGIN
					SET @strColumns=@strColumns+ ' ,'''' AS vcCompactContactDetails'   
				END 
				ELSE
				BEGIN 
                SET @strColumns = @strColumns
                                  + ','
                                  + CASE 
                                      WHEN @vcDbColumnName = 'numAge' THEN 'year(getutcdate())-year(bintDOB)'
                                      WHEN @vcDbColumnName = 'monPAmount' THEN 'isnull((SELECT varCurrSymbol FROM [Currency] WHERE numCurrencyID = Opp.[numCurrencyID]),'''') + '' ''+ CONVERT(VARCHAR,CONVERT(DECIMAL(20,5),monPAmount),1)'
                                      WHEN @vcDbColumnName = 'numDivisionID' THEN 'Div.numDivisionID'
                                      WHEN @vcDbColumnName = 'tintOppStatus' THEN 'CASE tintOppStatus WHEN 0 then ''Open'' WHEN 1 THEN ''Closed-Won'' WHEN 2 THEN ''Closed-Lost'' END '
                                      WHEN @vcDbColumnName = 'tintOppType' THEN 'CASE tintOppType WHEN 1 then ''Sales'' WHEN 2 THEN ''Purchase'' Else ''NA'' END '
									  WHEN @vcDbColumnName = 'vcPOppName' THEN 'dbo.CheckChildRecord(Opp.numOppId,Opp.numDomainID)'
									  WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'vcBizDocsList' THEN 'ISNULL((SELECT SUBSTRING((SELECT ''$^$'' + CAST(numOppBizDocsId AS VARCHAR(18)) +''#^#''+ vcBizDocID
										FROM OpportunityBizDocs WHERE numOppId=Opp.numOppId AND 1 = (Case When bitAuthoritativeBizDocs=1 AND 1=' + (CASE WHEN @inttype=1 THEN '1' ELSE '0' END) + '  Then 0 ELSE 1 END) FOR XML PATH('''')),4,200000)),'''')'
                                      ELSE @Prefix + @vcDbColumnName
                                    END
                                  + ' ['
                                  + @vcColumnName + ']'
				END
				IF(@vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'vcBizDocsList')
				BEGIN
					SET @strColumns=@strColumns+','+' (SELECT COUNT(I.vcItemName) FROM 
													 OpportunityItems AS t LEFT JOIN  Item as I ON 
													 t.numItemCode=I.numItemCode WHERE 
													 t.numOppId=Opp.numOppId AND t.numoppItemtCode NOT IN 
													 (SELECT numOppItemId FROM OpportunityBizDocs JOIN 
													 OpportunityBizDocItems ON OpportunityBizDocs.numOppBizDocsId=OpportunityBizDocItems.numOppBizDocID  
													 WHERE OpportunityBizDocs.numOppId=Opp.numOppId 
													 AND 1 = (Case When bitAuthoritativeBizDocs=1 Or numBizDocID=293 Then 0 ELSE 1 END) 
													 AND ISNULL(bitAuthoritativeBizDocs,0)=1)) AS [List_Item_Approval_UNIT] '
				END
				
				IF LEN(@SearchText) > 0
				BEGIN
					SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + (CASE 
                                      WHEN @vcDbColumnName = 'numAge' THEN 'year(getutcdate())-year(bintDOB)'
                                      WHEN @vcDbColumnName = 'monPAmount' THEN 'isnull((SELECT varCurrSymbol FROM [Currency] WHERE numCurrencyID = Opp.[numCurrencyID]),'''') + '' ''+ CONVERT(VARCHAR,CONVERT(DECIMAL(20,5),monPAmount),1)'
                                      WHEN @vcDbColumnName = 'numDivisionID' THEN 'Div.numDivisionID'
                                      WHEN @vcDbColumnName = 'tintOppStatus' THEN 'CASE tintOppStatus WHEN 0 then ''Open'' WHEN 1 THEN ''Closed-Won'' WHEN 2 THEN ''Closed-Lost'' END '
                                      WHEN @vcDbColumnName = 'tintOppType' THEN 'CASE tintOppType WHEN 1 then ''Sales'' WHEN 2 THEN ''Purchase'' Else ''NA'' END '
									  WHEN @vcDbColumnName = 'vcPOppName' THEN 'dbo.CheckChildRecord(Opp.numOppId,Opp.numDomainID)'
									  WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'vcBizDocsList' THEN 'ISNULL((SELECT SUBSTRING((SELECT ''$^$'' + CAST(numOppBizDocsId AS VARCHAR(18)) +''#^#''+ vcBizDocID
										FROM OpportunityBizDocs WHERE numOppId=Opp.numOppId AND 1 = (Case When bitAuthoritativeBizDocs=1 Or numBizDocID=293 Then 0 ELSE 1 END)  FOR XML PATH('''')),4,200000)),'''')'
                                      ELSE @Prefix + @vcDbColumnName
                                    END) + ' LIKE ''%' + @SearchText + '%'''
				END
            END
            ELSE
            IF @vcAssociatedControlType = 'TextArea'
            BEGIN
				SET @strColumns=@strColumns+','+  @Prefix + @vcDbColumnName +' ['+ @vcColumnName+']' 

				IF LEN(@SearchText) > 0
				BEGIN
					SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + @Prefix + @vcDbColumnName + ' LIKE ''%' + @SearchText + '%'''
				END
            END
            ELSE IF  @vcAssociatedControlType='Label'                                              
			BEGIN
				SET @strColumns=@strColumns + ',' + CASE               
				WHEN @vcDbColumnName = 'vcTrackingDetail' THEN 'STUFF((SELECT 
																			CONCAT(''<br/>'',OpportunityBizDocs.vcBizDocID,'': '',vcTrackingDetail)
																		FROM 
																			ShippingReport 
																		INNER JOIN 
																			OpportunityBizDocs 
																		ON 
																			ShippingReport.numOppBizDocId=OpportunityBizDocs.numOppBizDocsId 
																		WHERE 
																			ShippingReport.numOppId=Opp.numOppID
																			AND ISNULL(ShippingReport.vcTrackingDetail,'''') <> ''''
																		FOR XML PATH(''''), TYPE).value(''(./text())[1]'',''varchar(max)''), 1, 5, '''')'     
				WHEN @vcDbColumnName = 'CalAmount' THEN 'Opp.monDealAmount'--'[dbo].[getdealamount](Opp.numOppId,Getutcdate(),0)'
				WHEN @vcDbColumnName = 'vcInventoryStatus' THEN 'ISNULL(dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID,1),'''')'
				WHEN @vcDbColumnName = 'vcOrderedShipped' THEN CONCAT('dbo.GetOrderShipReceiveStatus(Opp.numOppId,1,Opp.tintshipped,',@tintDecimalPoints,')')
				WHEN @vcDbColumnName = 'vcOrderedReceived' THEN CONCAT('dbo.GetOrderShipReceiveStatus(Opp.numOppId,2,Opp.tintshipped,',@tintDecimalPoints,')')
				WHEN @vcDbColumnName = 'vcShipStreet' THEN 'dbo.fn_getOPPState(Opp.numOppId,Opp.numDomainID,2)'
				WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monDealAmount' THEN 'ISNULL((SELECT SUM(ISNULL(monDealAmount,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
				WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monAmountPaid' THEN 'ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
				ELSE @Prefix + @vcDbColumnName END +' ['+ @vcColumnName+']'   
				
				IF @vcDbColumnName='vcOrderedShipped'
				BEGIN
				DECLARE @temp VARCHAR(MAX)
				SET @temp = '(SELECT 
				STUFF((SELECT '', '' + CAST((CAST(numOppBizDocsId AS varchar)+''~''+CAST(ISNULL(numShippingReportId,0) AS varchar) ) AS VARCHAR(MAX)) [text()]
				FROM OpportunityBizDocs
				LEFT JOIN ShippingReport
				ON ShippingReport.numOppBizDocId=OpportunityBizDocs.numOppBizDocsId
				WHERE OpportunityBizDocs.numOppId=Opp.numOppId  AND OpportunityBizDocs.bitShippingGenerated=1  FOR XML PATH(''''), TYPE)
						.value(''.'',''NVARCHAR(MAX)''),1,2,'' ''))'
					SET @strColumns = @strColumns + ' , (SELECT SUBSTRING('+ @temp +', 2, 200000))AS ShippingIcons '
					SET @strColumns = @strColumns + ' , CASE WHEN (SELECT COUNT(*) FROM ShippingBox INNER JOIN ShippingReport ON ShippingBox.numShippingReportId = ShippingReport.numShippingReportId WHERE ShippingReport.numOppID = Opp.numOppID AND LEN(vcTrackingNumber) > 0) > 0 THEN 1 ELSE 0 END [ISTrackingNumGenerated]'
					SET @strColumns = @strColumns + ' ,dbo.fn_GetListItemName(intUsedShippingCompany) AS ShipVia '
					SET @strColumns = @strColumns + ' ,Opp.intUsedShippingCompany AS intUsedShippingCompany '
					--SET @strColumns = @strColumns + ' ,Opp.numShippingService AS numShippingService '
					SET @strColumns = @strColumns + ' ,(SELECT numFieldId FROM DycFieldMaster WHERE vcDbColumnName=''intUsedShippingCompany'' AND numModuleID=3) AS ShipViaFieldId '
					SET @strColumns = @strColumns + ' ,(SELECT numFieldId FROM DycFieldMaster WHERE vcDbColumnName=''numShippingService'' AND numModuleID=3) AS ShippingServiceFieldId '
					SET @strColumns=@strColumns+ ' ,ISNULL((SELECT vcShipmentService FROM ShippingService WHERE (numDomainID=Opp.numDomainID OR ISNULL(numDomainID,0)=0) AND numShipmentServiceID = ISNULL(Opp.numShippingService,0)),'''')' + ' AS vcShipmentService'
				
				END

				IF LEN(@SearchText) > 0
				BEGIN
					SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + (CASE                    
					WHEN @vcDbColumnName = 'CalAmount' THEN 'Opp.monDealAmount'
					WHEN @vcDbColumnName = 'vcInventoryStatus' THEN 'ISNULL(dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID,1),'''')'
					WHEN @vcDbColumnName = 'vcOrderedShipped' THEN CONCAT('dbo.GetOrderShipReceiveStatus(Opp.numOppId,1,Opp.tintshipped,',@tintDecimalPoints,')')
					WHEN @vcDbColumnName = 'vcOrderedReceived' THEN CONCAT('dbo.GetOrderShipReceiveStatus(Opp.numOppId,2,Opp.tintshipped,',@tintDecimalPoints,')')
					WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monDealAmount' THEN 'ISNULL((SELECT SUM(ISNULL(monDealAmount,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
					WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monAmountPaid' THEN 'ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
					else @Prefix + @vcDbColumnName END) + ' LIKE ''%' + @SearchText + '%'''
				END    
			END
            ELSE IF @vcAssociatedControlType='Popup' and @vcDbColumnName='numShareWith'                                                   
			BEGIN
				SET @strColumns = @strColumns + ',(SELECT SUBSTRING(
								(SELECT '','' + A.vcFirstName+'' ''+A.vcLastName + CASE WHEN ISNULL(SR.numContactType,0)>0 THEN ''('' + dbo.fn_GetListItemName(SR.numContactType) + '')'' ELSE '''' END
								FROM ShareRecord SR JOIN UserMaster UM ON UM.numUserDetailId=SR.numAssignedTo 
								JOIN AdditionalContactsInformation A ON UM.numUserDetailId=A.numContactID 
								WHERE SR.numDomainID=Opp.numDomainID AND SR.numModuleID=3 AND SR.numRecordID=Opp.numOppId
								AND UM.numDomainID=Opp.numDomainID and UM.numDomainID=A.numDomainID FOR XML PATH('''')),2,200000)) ['+ @vcColumnName+']'                
			END 
        END
		ELSE
        BEGIN
			SET @vcColumnName= @vcDbColumnName +'~'+ convert(varchar(10),@numFieldId)+'~1'

			IF @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'
            BEGIN
              SET @strColumns = @strColumns
                              + ',CFW'
                              + CONVERT(VARCHAR(3),@tintOrder)
                              + '.Fld_Value  ['
                              + @vcColumnName + ']'
              SET @WhereCondition = @WhereCondition
                                      + ' left Join CFW_FLD_Values_Opp CFW'
                                      + CONVERT(VARCHAR(3),@tintOrder)
                                      + ' on CFW'
                                      + CONVERT(VARCHAR(3),@tintOrder)
                                      + '.Fld_Id='
                                      + CONVERT(VARCHAR(10),@numFieldId)
                                      + 'and CFW'
                                      + CONVERT(VARCHAR(3),@tintOrder)
                                      + '.RecId=Opp.numOppid   '
			END
			ELSE
            IF @vcAssociatedControlType = 'CheckBox' 
            BEGIN
                SET @strColumns = @strColumns
                                + ',case when isnull(CFW'
                                + CONVERT(VARCHAR(3),@tintOrder)
                                + '.Fld_Value,0)=0 then 0 when isnull(CFW'
                                + CONVERT(VARCHAR(3),@tintOrder)
                                + '.Fld_Value,0)=1 then 1 end   ['
                                + @vcColumnName + ']'
                SET @WhereCondition = @WhereCondition
                                        + ' left Join CFW_FLD_Values_Opp CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + ' on CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.Fld_Id='
                                        + CONVERT(VARCHAR(10),@numFieldId)
                                        + 'and CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.RecId=Opp.numOppid   '
            END
            ELSE
            IF @vcAssociatedControlType = 'DateField'
            BEGIN
                  SET @strColumns = @strColumns
                                  + ',dbo.FormatedDateFromDate(CFW'
                                  + CONVERT(VARCHAR(3),@tintOrder)
                                  + '.Fld_Value,'
                                  + CONVERT(VARCHAR(10),@numDomainId)
                                  + ')  ['
                                  + @vcColumnName + ']'
                  SET @WhereCondition = @WhereCondition
                                          + ' left Join CFW_FLD_Values_Opp CFW'
                                          + CONVERT(VARCHAR(3),@tintOrder)
                                          + ' on CFW'
                                          + CONVERT(VARCHAR(3),@tintOrder)
                                          + '.Fld_Id='
                                          + CONVERT(VARCHAR(10),@numFieldId)
                                          + 'and CFW'
                                          + CONVERT(VARCHAR(3),@tintOrder)
                                          + '.RecId=Opp.numOppid    '
            END
            ELSE
            IF @vcAssociatedControlType = 'SelectBox'
            BEGIN
                SET @vcDbColumnName = 'DCust'
                                            + CONVERT(VARCHAR(10),@numFieldId)
                SET @strColumns = @strColumns
                                + ',L'
                                + CONVERT(VARCHAR(3),@tintOrder)
                                + '.vcData'
                                + ' ['
                                + @vcColumnName + ']'

                SET @WhereCondition = @WhereCondition
                                        + ' left Join CFW_FLD_Values_Opp CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + ' on CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.Fld_Id='
                                        + CONVERT(VARCHAR(10),@numFieldId)
                                        + 'and CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.RecId=Opp.numOppid '

                SET @WhereCondition = @WhereCondition
                                        + ' left Join ListDetails L'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + ' on L'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.numListItemID=CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.Fld_Value'
            END
			ELSE
			BEGIN
				SET @strColumns = @strColumns + CONCAT(',dbo.GetCustFldValueOpp(',@numFieldId,',opp.numOppID)') + ' [' + @vcColumnName + ']'
			END

			IF LEN(@SearchText) > 0
			BEGIN
				SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + CONCAT('dbo.GetCustFldValueOpp(',@numFieldId,',opp.numOppID) LIKE ''%',@SearchText,'%''')
			END
		END
      
     
		SELECT TOP 1 
			@tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,@vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,
			@numListID=numListID,@vcLookBackTableName=vcLookBackTableName,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,
			@ListRelID=ListRelID
		FROM 
			#tempForm 
		WHERE 
			tintOrder > @tintOrder-1 
		ORDER BY 
			tintOrder asc            
 
		IF @@rowcount=0 SET @tintOrder=0 
	END 

	

	DECLARE @strShareRedordWith AS VARCHAR(300);
	SET @strShareRedordWith=' Opp.numOppId in (select SR.numRecordID FROM ShareRecord SR WHERE SR.numDomainID='+ convert(varchar(15),@numDomainID )+' AND SR.numModuleID=3 AND SR.numAssignedTo='+convert(varchar(15),@numUserCntId)+') '

	DECLARE @strExternalUser AS VARCHAR(MAX) = ''
	SET @strExternalUser = CONCAT(' (NOT EXISTS (SELECT numUserDetailID FROM UserMaster WHERE numDomainID=',@numDomainID,' AND numUserDetailID=',@numUserCntId,') AND EXISTS (SELECT EAD.numExtranetDtlID FROM ExtranetAccountsDtl EAD INNER JOIN ExtarnetAccounts EA ON EAD.numExtranetID=EA.numExtranetID WHERE EAD.numDomainID=',@numDomainID,' AND EAD.numContactID=',@numUserCntID,' AND EA.numDivisionID=Div.numDivisionID))')

	DECLARE @StrSql AS VARCHAR(MAX) = ''

	SET @StrSql = @StrSql + ' FROM OpportunityMaster Opp                                               
                                 LEFT JOIN AdditionalContactsInformation ADC ON Opp.numContactId = ADC.numContactId                                                               
                                 INNER JOIN DivisionMaster Div ON Opp.numDivisionId = Div.numDivisionID 
								 INNER JOIN CompanyInfo cmp ON Div.numCompanyID = cmp.numCompanyId 
								 LEFT JOIN ProjectProgress PP On PP.numOppId = Opp.numOppId ' + @WhereCondition
								 --INNER JOIN Correspondence Corr ON opp.numOppId = Corr.numOpenRecordID 
	-------Change Row Color-------
	
	DECLARE @vcCSOrigDbCOlumnName AS VARCHAR(50) = ''
	DECLARE @vcCSLookBackTableName AS VARCHAR(50) = ''
	DECLARE @vcCSAssociatedControlType AS VARCHAR(50)

	CREATE TABLE #tempColorScheme
	(
		vcOrigDbCOlumnName VARCHAR(50),
		vcLookBackTableName VARCHAR(50),
		vcFieldValue VARCHAR(50),
		vcFieldValue1 VARCHAR(50),
		vcColorScheme VARCHAR(50),
		vcAssociatedControlType varchar(50)
	)

	INSERT INTO 
		#tempColorScheme 
	SELECT 
		DFM.vcOrigDbCOlumnName,
		DFM.vcLookBackTableName,
		DFCS.vcFieldValue,
		DFCS.vcFieldValue1,
		DFCS.vcColorScheme,
		DFFM.vcAssociatedControlType 
	FROM 
		DycFieldColorScheme DFCS 
	JOIN 
		DycFormField_Mapping DFFM 
	ON 
		DFCS.numFieldID=DFFM.numFieldID
	JOIN 
		DycFieldMaster DFM 
	ON 
		DFM.numModuleID=DFFM.numModuleID and DFM.numFieldID=DFFM.numFieldID
	WHERE
		DFCS.numDomainID=@numDomainID 
		AND DFFM.numFormID=@numFormId 
		AND DFCS.numFormID=@numFormId 
		AND isnull(DFFM.bitAllowGridColor,0)=1

	IF(SELECT COUNT(*) FROM #tempColorScheme)>0
	BEGIN
		SELECT TOP 1 @vcCSOrigDbCOlumnName=vcOrigDbCOlumnName,@vcCSLookBackTableName=vcLookBackTableName,@vcCSAssociatedControlType=vcAssociatedControlType from #tempColorScheme
	END                        

	IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
	BEGIN
		SET @strColumns=@strColumns + ',tCS.vcColorScheme'
	END

	IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
	BEGIN
		IF @vcCSLookBackTableName = 'AdditionalContactsInformation'                  
			SET @Prefix = 'ADC.'                  
		IF @vcCSLookBackTableName = 'DivisionMaster'                  
			SET @Prefix = 'Div.'
		if @vcCSLookBackTableName = 'CompanyInfo'                  
			SET @Prefix = 'CMP.'   
		if @vcCSLookBackTableName = 'OpportunityMaster'                  
			SET @Prefix = 'Opp.'   
 
		IF @vcCSAssociatedControlType='DateField'
		BEGIN
			SET @strSql=@strSql+' LEFT JOIN #tempColorScheme tCS ON CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) >= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue as int),GETUTCDATE()),111)
				 and CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) <= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue1 as int),GETUTCDATE()),111)'
		END
		ELSE IF @vcCSAssociatedControlType='SelectBox' OR @vcCSAssociatedControlType='ListBox' OR @vcCSAssociatedControlType='CheckBox'
		BEGIN
			SET @strSql=@strSql+' LEFT JOIN #tempColorScheme tCS on tCS.vcFieldValue=' + @Prefix + @vcCSOrigDbCOlumnName
		END
	END
                          
	IF @columnName like 'CFW.Cust%'             
	BEGIN            
		SET @strSql = @strSql + ' LEFT JOIN CFW_Fld_Values_Opp CFW on CFW.RecId=Opp.numOppid and CFW.fld_id= '+ REPLACE(@columnName,'CFW.Cust','') +' '                                                     
		DECLARE @vcSortFieldType VARCHAR(50)

		SELECT @vcSortFieldType = Fld_type FROM CFW_Fld_Master WHERE Fld_id=CAST(REPLACE(@columnName,'CFW.Cust','') AS INT)

		IF ISNULL(@vcSortFieldType,'') = 'DateField'
		BEGIN
			SET @columnName='(CASE WHEN ISDATE(CFW.Fld_Value) = 1 THEN CAST(CFW.Fld_Value AS DATE) ELSE NULL END)'
		END
		ELSE IF CAST(REPLACE(@columnName,'CFW.Cust','') AS INT) IN (12745,12846)
		BEGIN
			SET @columnName='(CASE WHEN ISNUMERIC(CFW.Fld_Value) = 1 THEN CAST(CFW.Fld_Value AS FLOAT) ELSE NULL END)'
		END
		ELSE
		BEGIN
			SET @columnName='CFW.Fld_Value'
		END            
	END 

	IF @bitPartner = 1
		SET @strSql = @strSql + ' LEFT JOIN OpportunityContact OppCont ON OppCont.numOppId=Opp.numOppId AND OppCont.bitPartner=1 AND OppCont.numContactId=' + CONVERT(VARCHAR(15),@numUserCntID)
                    
                
	IF @tintFilterBy = 1 --Partially Fulfilled Orders 
		SET @strSql = @strSql + ' LEFT JOIN AdditionalContactsInformation ADC1 ON ADC1.numContactId=Opp.numRecOwner                                                               
                             LEFT OUTER JOIN [OpportunityBizDocs] OB ON OB.[numOppId] = Opp.[numOppId] 
                             WHERE Opp.numDomainID=' + CONVERT(VARCHAR(15),@numDomainID) + ' and Opp.tintOppstatus=1 and (Opp.tintShipped=' + CONVERT(VARCHAR(15),@tintShipped) + ' OR ' + CONVERT(VARCHAR(15),@tintShipped) + '=2)'
	ELSE
		SET @strSql = @strSql + ' LEFT JOIN AdditionalContactsInformation ADC1 on ADC1.numContactId=Opp.numRecOwner                                                               
                             WHERE Opp.numDomainID=' + CONVERT(VARCHAR(15),@numDomainID) + ' and Opp.tintOppstatus=1 and (Opp.tintShipped='+ CONVERT(VARCHAR(15),@tintShipped)  + ' OR ' + CONVERT(VARCHAR(15),@tintShipped) + '=2)'
  
  
	IF @intOrder <> 0
    BEGIN
      IF @intOrder = 1
        SET @strSql = @strSql + ' and Opp.bitOrder = 0 '
      IF @intOrder = 2
        SET @strSql = @strSql + ' and Opp.bitOrder = 1'
    END
    
	IF @numCompanyID <> 0
		SET @strSql = @strSql + ' And Div.numCompanyID ='+ CONVERT(VARCHAR(15),@numCompanyID)

	IF @SortChar <> '0' 
		SET @strSql = @strSql + ' And Opp.vcPOppName like ''' + @SortChar + '%'''
  

	IF @inttype = 3
	BEGIN
		IF @tintSalesUserRightType = 0
			SET @strSql = @strSql + ' AND opp.tintOppType=0'
		ELSE IF @tintSalesUserRightType = 1
			SET @strSql = CONCAT(@strSql,' AND (Opp.numRecOwner= ',@numUserCntID,' or Opp.numAssignedTo=',@numUserCntID
						,(CASE WHEN @bitPartner = 1 THEN ' or ( OppCont.bitPartner=1 and OppCont.numContactId='+ CONVERT(VARCHAR(15),@numUserCntID) + ')' ELSE '' END)
						,' or ',@strShareRedordWith
						,' or ',@strExternalUser,')')
		ELSE IF @tintSalesUserRightType = 2
			SET @strSql = @strSql + ' AND (Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+ CONVERT(VARCHAR(15),@numUserCntID) + ' ) 
									 or Opp.numAssignedTo= '+ CONVERT(VARCHAR(15),@numUserCntID)
									+ ' or ' + @strShareRedordWith +')'
	END
	ELSE IF @inttype = 4
	BEGIN
		IF @tintPurchaseUserRightType = 0
			SET @strSql = @strSql + ' AND opp.tintOppType=0'
		ELSE IF @tintPurchaseUserRightType = 1
			SET @strSql = CONCAT(@strSql,' AND (Opp.numRecOwner= ',@numUserCntID
						, ' or Opp.numAssignedTo= ',@numUserCntID
						, CASE WHEN @bitPartner = 1 THEN ' or ( OppCont.bitPartner=1 and OppCont.numContactId='+ CONVERT(VARCHAR(15),@numUserCntID) + ')' ELSE '' END
						, ' or ' + @strShareRedordWith
						,' or ',@strExternalUser,')')
		ELSE IF @tintPurchaseUserRightType = 2
			SET @strSql = @strSql + ' AND (Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+ CONVERT(VARCHAR(15),@numUserCntID) + ' ) 
									 or Opp.numAssignedTo= '+ CONVERT(VARCHAR(15),@numUserCntID)
									+ ' or ' + @strShareRedordWith +')'
	END

	
					
	IF @tintSortOrder <> '0'
		SET @strSql = @strSql + '  AND Opp.tintOppType= '+ CONVERT(VARCHAR(1),@tintSortOrder)
    
    
	IF @tintFilterBy = 1 --Partially Fulfilled Orders
		SET @strSql = @strSql + ' AND OB.bitPartialFulfilment = 1 AND OB.bitAuthoritativeBizDocs = 1 '
	ELSE IF @tintFilterBy = 2 --Fulfilled Orders
		SET @strSql = @strSql + ' AND Opp.[numOppId] IN (SELECT OM.numOppId FROM [OpportunityMaster] OM
                      			INNER JOIN [OpportunityBizDocs] OB ON OB.[numOppId] = OM.[numOppId]
                      			INNER JOIN [OpportunityItems] OI ON OM.[numOppId] = OI.[numOppId]
                      			INNER JOIN [OpportunityBizDocItems] BI ON BI.[numOppBizDocID] = OB.[numOppBizDocsId]
                      			Where OM.numDomainId=' + CONVERT(VARCHAR(15),@numDomainId) + 'AND OB.[bitAuthoritativeBizDocs] = 1 GROUP BY OM.[numOppId]
                      			HAVING   COUNT(OI.[numUnitHour]) = COUNT(BI.[numUnitHour])) '
	ELSE IF @tintFilterBy = 3 --Orders without Authoritative-BizDocs
		SET @strSql = @strSql + ' AND Opp.[numOppId] not IN (SELECT DISTINCT OM.[numOppId]
                      		  FROM [OpportunityMaster] OM JOIN [OpportunityBizDocs] OB ON OM.[numOppId] = OB.[numOppId]
                      		  Where OM.numDomainId=' + CONVERT(VARCHAR(15),@numDomainId) + ' AND OM.tintOppstatus=1 AND (OM.tintShipped=' + CONVERT(VARCHAR(15),@tintShipped) + ' OR ' + CONVERT(VARCHAR(15),@tintShipped) + '=2)' + ' AND OM.tintOppType= '+ CONVERT(VARCHAR(1),@tintSortOrder) + ' AND isnull(OB.[bitAuthoritativeBizDocs],0)=1) '
	ELSE IF @tintFilterBy = 4 --Orders with items yet to be added to Invoices
		SET @strSql = @strSql + ' AND Opp.[numOppId] IN (SELECT distinct OPPM.numOppId FROM [OpportunityMaster] OPPM
                                  INNER JOIN [OpportunityItems] OPPI ON OPPM.[numOppId] = OPPI.[numOppId]
                                  Where OPPM.numDomainId=' + CONVERT(VARCHAR(15),@numDomainId) + ' 
								  and OPPI.numoppitemtCode not in (select numOppItemID from [OpportunityBizDocs] OB INNER JOIN [OpportunityBizDocItems] BI
                      			  ON BI.[numOppBizDocID] = OB.[numOppBizDocsId] where OB.[numOppId] = OPPM.[numOppId]))'      
	ELSE IF @tintFilterBy = 6 --Orders with deferred Income/Invoices
		SET @strSql = @strSql + ' AND Opp.[numOppId] IN (SELECT DISTINCT OM.[numOppId]
                      		 FROM [OpportunityMaster] OM LEFT OUTER JOIN [OpportunityBizDocs] OB ON OM.[numOppId] = OB.[numOppId]
                      		 Where OM.numDomainId=' + CONVERT(VARCHAR(15),@numDomainId) + ' AND OB.[bitAuthoritativeBizDocs] =1 and OB.tintDeferred=1)'

	IF @numOrderStatus <>0 
		SET @strSql = @strSql + ' AND Opp.numStatus = ' + CONVERT(VARCHAR(15),@numOrderStatus);

	IF CHARINDEX('OI.vcInventoryStatus',@vcRegularSearchCriteria) > 0
	BEGIN
		IF CHARINDEX('OI.vcInventoryStatus=1',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'OI.vcInventoryStatus=1',' CHARINDEX(''Not Applicable'', dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID,1)) > 0 ')
		IF CHARINDEX('OI.vcInventoryStatus=2',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'OI.vcInventoryStatus=2',' CHARINDEX(''Shipped'', dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID,1)) > 0 ')
		IF CHARINDEX('OI.vcInventoryStatus=3',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'OI.vcInventoryStatus=3',' CHARINDEX(''BO'', dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID,1)) > 0 AND CHARINDEX(''Shippable'', dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID,1)) = 0 ')
		IF CHARINDEX('OI.vcInventoryStatus=4',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'OI.vcInventoryStatus=4',' CHARINDEX(''Shippable'', dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID,1)) > 0 ')
	END
	
	IF CHARINDEX('Opp.tintInvoicing',@vcRegularSearchCriteria) > 0
	BEGIN
		IF CHARINDEX('Opp.tintInvoicing=0',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintInvoicing=0',' CHARINDEX(''All'', dbo.CheckOrderInvoicingStatus(Opp.numOppID,Opp.numDomainID,1)) > 0 ')
		IF CHARINDEX('Opp.tintInvoicing=1',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintInvoicing=1',' (SELECT (CASE WHEN SUM(ISNULL(InvoicedQty,0)) = SUM(ISNULL(OrderedQty,0)) THEN 1 ELSE 0 END)
	FROM 
	(
		SELECT OI.numoppitemtCode, ISNULL(OI.numUnitHour,0) AS OrderedQty, ISNULL(TempInvoice.InvoicedQty,0) AS InvoicedQty
		FROM OpportunityItems OI
		INNER JOIN Item I ON OI.numItemCode = I.numItemCode
		OUTER APPLY
		(
			SELECT SUM(OpportunityBizDocItems.numUnitHour) AS InvoicedQty
			FROM OpportunityBizDocs
				INNER JOIN OpportunityBizDocItems ON OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
			WHERE OpportunityBizDocs.numOppId = Opp.numOppID
				AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
				AND numBizDocId IN (SELECT numAuthoritativeSales FROM AuthoritativeBizDocs WHERE numDomainId = Opp.numDomainID)
		) AS TempInvoice
		WHERE
		OI.numOppID = Opp.numOppID
	) X) > 0 ')
		IF CHARINDEX('Opp.tintInvoicing=2',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintInvoicing=2',' (SELECT COUNT(*) 
		FROM OpportunityBizDocs 
		WHERE numOppId = Opp.numOppID 
			AND numBizDocId IN (SELECT numAuthoritativeSales FROM AuthoritativeBizDocs WHERE numDomainId = Opp.numDomainID)) = 0 ')
		IF CHARINDEX('Opp.tintInvoicing=3',@vcRegularSearchCriteria) > 0 
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintInvoicing=3',' (SELECT (CASE WHEN SUM(ISNULL(InvoicedQty,0)) > 0 THEN SUM(ISNULL(OrderedQty,0)) - SUM(ISNULL(InvoicedQty,0)) ELSE 0 END)
	FROM 
	(
		SELECT OI.numoppitemtCode, ISNULL(OI.numUnitHour,0) AS OrderedQty, ISNULL(TempInvoice.InvoicedQty,0) AS InvoicedQty
		FROM OpportunityItems OI
		INNER JOIN Item I ON OI.numItemCode = I.numItemCode
		OUTER APPLY
		(
			SELECT SUM(OpportunityBizDocItems.numUnitHour) AS InvoicedQty
			FROM OpportunityBizDocs
				INNER JOIN OpportunityBizDocItems ON OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
			WHERE OpportunityBizDocs.numOppId = Opp.numOppID
				AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
				AND numBizDocId IN (SELECT numAuthoritativeSales FROM AuthoritativeBizDocs WHERE numDomainId = Opp.numDomainID)
		) AS TempInvoice
		WHERE
		OI.numOppID = Opp.numOppID
	) X) > 0 ')
		
		IF CHARINDEX('Opp.tintInvoicing=4',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintInvoicing=4',' (SELECT (CASE WHEN SUM(ISNULL(InvoicedQty,0)) > 0 THEN SUM(ISNULL(OrderedQty,0)) - SUM(ISNULL(InvoicedQty,0)) ELSE 0 END)
	FROM 
	(
		SELECT OI.numoppitemtCode, ISNULL(OI.numUnitHour,0) AS OrderedQty, ISNULL(TempInvoice.InvoicedQty,0) AS InvoicedQty
		FROM OpportunityItems OI
		INNER JOIN Item I ON OI.numItemCode = I.numItemCode
		OUTER APPLY
		(
			SELECT SUM(OpportunityBizDocItems.numUnitHour) AS InvoicedQty
			FROM OpportunityBizDocs
				INNER JOIN OpportunityBizDocItems ON OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
			WHERE OpportunityBizDocs.numOppId = Opp.numOppID
				AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
				AND numBizDocId IN (SELECT numAuthoritativeSales FROM AuthoritativeBizDocs WHERE numDomainId = Opp.numDomainID)
		) AS TempInvoice
		WHERE
		OI.numOppID = Opp.numOppID
	) X) > 0 ')

		IF CHARINDEX('Opp.tintInvoicing=5',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintInvoicing=5',' 
				Opp.[numOppId] IN (SELECT DISTINCT OM.[numOppId]
                      		 FROM [OpportunityMaster] OM INNER JOIN [OpportunityBizDocs] OB ON OM.[numOppId] = OB.[numOppId]
                      		 Where OM.numDomainId = Opp.numDomainID AND OB.[bitAuthoritativeBizDocs] = 1 and OB.tintDeferred=1) ')
	END
	
	IF CHARINDEX('Opp.tintEDIStatus',@vcRegularSearchCriteria) > 0
	BEGIN
		--IF CHARINDEX('Opp.tintEDIStatus=1',@vcRegularSearchCriteria) > 0
		--	SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintEDIStatus=1',' 1 = (SELECT CASE WHEN Opp.tintEDIStatus = 1 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.tintEDIStatus=2',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintEDIStatus=2',' 1 = (SELECT CASE WHEN Opp.tintEDIStatus = 2 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.tintEDIStatus=3',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintEDIStatus=3',' 1 = (SELECT CASE WHEN Opp.tintEDIStatus = 3 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.tintEDIStatus=4',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintEDIStatus=4',' 1 = (SELECT CASE WHEN Opp.tintEDIStatus = 4 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.tintEDIStatus=5',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintEDIStatus=5',' 1 = (SELECT CASE WHEN Opp.tintEDIStatus = 5 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.tintEDIStatus=6',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintEDIStatus=6',' 1 = (SELECT CASE WHEN Opp.tintEDIStatus = 6 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.tintEDIStatus=7',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintEDIStatus=7',' 1 = (SELECT CASE WHEN Opp.tintEDIStatus = 7 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.tintEDIStatus=7',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintEDIStatus=11',' 1 = (SELECT CASE WHEN Opp.tintEDIStatus = 11 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.tintEDIStatus=7',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintEDIStatus=12',' 1 = (SELECT CASE WHEN Opp.tintEDIStatus = 12 THEN 1 ELSE 0 END) ')
	END

	IF CHARINDEX('Opp.vcSignatureType',@vcRegularSearchCriteria) > 0
	BEGIN
		IF CHARINDEX('Opp.vcSignatureType=0',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.vcSignatureType=0',' 1 = (SELECT CASE WHEN (SELECT COUNT(*) FROM DivisionMaster AS D 
																			LEFT JOIN DivisionMasterShippingConfiguration AS DMSC ON DMSC.numDivisionID=D.numDivisionID 
																			WHERE D.numDivisionID=Div.numDivisionID AND vcSignatureType=0 ) > 0 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.vcSignatureType=1',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.vcSignatureType=1','  1 = (SELECT CASE WHEN (SELECT COUNT(*) FROM DivisionMaster AS D 
																			LEFT JOIN DivisionMasterShippingConfiguration AS DMSC ON DMSC.numDivisionID=D.numDivisionID 
																			WHERE D.numDivisionID=Div.numDivisionID AND vcSignatureType=1 ) > 0 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.vcSignatureType=2',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.vcSignatureType=2',' 1 = (SELECT CASE WHEN (SELECT COUNT(*) FROM DivisionMaster AS D 
																			LEFT JOIN DivisionMasterShippingConfiguration AS DMSC ON DMSC.numDivisionID=D.numDivisionID 
																			WHERE D.numDivisionID=Div.numDivisionID AND vcSignatureType=2 ) > 0 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.vcSignatureType=3',@vcRegularSearchCriteria) > 0								   
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.vcSignatureType=3',' 1 = (SELECT CASE WHEN (SELECT COUNT(*) FROM DivisionMaster AS D 
																			LEFT JOIN DivisionMasterShippingConfiguration AS DMSC ON DMSC.numDivisionID=D.numDivisionID 
																			WHERE D.numDivisionID=Div.numDivisionID AND vcSignatureType=3 ) > 0 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.vcSignatureType=4',@vcRegularSearchCriteria) > 0								   
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.vcSignatureType=4',' 1 = (SELECT CASE WHEN (SELECT COUNT(*) FROM DivisionMaster AS D 
																			LEFT JOIN DivisionMasterShippingConfiguration AS DMSC ON DMSC.numDivisionID=D.numDivisionID 
																			WHERE D.numDivisionID=Div.numDivisionID AND vcSignatureType=4 ) > 0 THEN 1 ELSE 0 END) ')
	END

	IF CHARINDEX('OBD.monAmountPaid',@vcRegularSearchCriteria) > 0
	BEGIN
		IF CHARINDEX('OBD.monAmountPaid like ''%0%''',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'OBD.monAmountPaid like ''%0%''',' 1 = (SELECT ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM  OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)) ')
		IF CHARINDEX('OBD.monAmountPaid like ''%1%''',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'OBD.monAmountPaid like ''%1%''','  1 = (SELECT (CASE WHEN (SELECT (CASE WHEN (SUM(ISNULL(monDealAmount,0)) = SUM(ISNULL(monAmountPaid,0)) AND SUM(ISNULL(monAmountPaid,0)) <> 0) THEN 1 ELSE 0 END) FROM OpportunityBizDocs BD WHERE BD.numOppId =  Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0) = 1) > 0 THEN 1 ELSE 0 END))  ')
		IF CHARINDEX('OBD.monAmountPaid like ''%2%''',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'OBD.monAmountPaid like ''%2%''',' 1 = (SELECT CASE WHEN (SELECT (CASE WHEN ((SUM(ISNULL(monDealAmount,0)) - SUM(ISNULL(monAmountPaid,0))) > 0 AND SUM(ISNULL(monAmountPaid,0)) <> 0) THEN 1 ELSE 0 END) FROM OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0) = 1) > 0 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('OBD.monAmountPaid like ''%3%''',@vcRegularSearchCriteria) > 0								   
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'OBD.monAmountPaid like ''%3%''',' 1 = (SELECT CASE WHEN (SELECT (CASE WHEN (SUM(ISNULL(monAmountPaid,0)) = 0) THEN 1 ELSE 0 END) FROM OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0) = 1) > 0 THEN 1 ELSE 0 END) ')
	END
	
	
	IF @vcRegularSearchCriteria<>'' 
	BEGIN
		SET @strSql=@strSql +' AND ' + @vcRegularSearchCriteria 
	END
	
	


	IF @vcCustomSearchCriteria<>'' 
	BEGIN
		SET @strSql = @strSql +' AND ' +  @vcCustomSearchCriteria
	END

	IF LEN(@SearchQuery) > 0
	BEGIN
		SET @strSql = @strSql + ' AND (' + @SearchQuery + ') '
	END

	IF ISNULL(@tintDashboardReminderType,0) = 19
	BEGIN
		SET @strSql = CONCAT(@strSql,' AND Opp.numOppID IN (','SELECT DISTINCT
																	OMInner.numOppID
																FROM 
																	StagePercentageDetails SPDInner
																INNER JOIN 
																	OpportunityMaster OMInner
																ON 
																	SPDInner.numOppID=OMInner.numOppId 
																WHERE 
																	SPDInner.numDomainId=',@numDOmainID,'
																	AND OMInner.numDomainId=',@numDOmainID,' 
																	AND OMInner.tintOppType = 1
																	AND ISNULL(OMInner.tintOppStatus,0) = 1
																	AND tinProgressPercentage <> 100',') ')

	END
	ELSE IF ISNULL(@tintDashboardReminderType,0) = 10
	BEGIN
		SET @strSql = CONCAT(@strSql,' AND Opp.numOppID IN (','SELECT DISTINCT
																	OMInner.numOppId
																FROM
																	OpportunityMaster OMInner
																INNER JOIN
																	OpportunityItems OIInner
																ON
																	OIInner.numOppId = OMInner.numOppID
																INNER JOIN
																	Item IInner
																ON
																	OIInner.numItemCode = IInner.numItemCode
																OUTER APPLY
																(
																	SELECT
																		SUM(OpportunityBizDocItems.numUnitHour) AS BilledQty
																	FROM
																		OpportunityBizDocs
																	INNER JOIN
																		OpportunityBizDocItems 
																	ON
																		OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																	WHERE
																		OpportunityBizDocs.numOppId = OMInner.numOppID
																		AND OpportunityBizDocItems.numOppItemID = OIInner.numoppitemtCode
																		AND OpportunityBizDocs.numBizDocId = (SELECT ISNULL(numAuthoritativePurchase,0) FROM AuthoritativeBizDocs WHERE numDomainId=',@numDomainID,')
																) AS TempBilled
																WHERE
																	OMInner.numDomainId = ',@numDomainID,'
																	AND ISNULL(OMInner.bitStockTransfer,0) = 0
																	AND OMInner.tintOppType = 2
																	AND OMInner.tintOppStatus  = 1
																	AND ISNULL(OIInner.numUnitHour,0) <> ISNULL(TempBilled.BilledQty,0)',') ')
	END
	ELSE IF ISNULL(@tintDashboardReminderType,0) = 11
	BEGIN
		SET @strSql = CONCAT(@strSql,' AND Opp.numOppID IN (','SELECT DISTINCT
																	OMInner.numOppId
																FROM
																	OpportunityMaster OMInner
																INNER JOIN
																	OpportunityItems OIInner
																ON
																	OIInner.numOppId = OMInner.numOppId
																INNER JOIN
																	Item IInner
																ON
																	OIInner.numItemCode = IInner.numItemCode
																OUTER APPLY
																(
																	SELECT
																		SUM(OpportunityBizDocItems.numUnitHour) AS PickPacked
																	FROM
																		OpportunityBizDocs
																	INNER JOIN
																		OpportunityBizDocItems 
																	ON
																		OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																	WHERE
																		OpportunityBizDocs.numOppId = OMInner.numOppId
																		AND ISNULL(OpportunityBizDocs.numBizDocId,0) = ',(SELECT ISNULL(numDefaultSalesShippingDoc,0) FROM Domain WHERE numDomainId=@numDomainID),'
																		AND OpportunityBizDocItems.numOppItemID = OIInner.numoppitemtCode
																) AS TempFulFilled
																WHERE
																	OMInner.numDomainId = ',@numDomainID,'
																	AND OMInner.tintOppType = 1
																	AND OMInner.tintOppStatus = 1
																	AND UPPER(IInner.charItemType) = ''P''
																	AND ISNULL(OMInner.tintshipped,0) = 0
																	AND ISNULL(OIInner.bitDropShip,0) = 0
																	AND ISNULL(OIInner.numUnitHour,0) <> ISNULL(TempFulFilled.PickPacked,0)',') ')
	END
	ELSE IF ISNULL(@tintDashboardReminderType,0) = 12
	BEGIN
		SET @strSql = CONCAT(@strSql,' AND Opp.numOppID IN (','SELECT DISTINCT
																	OMInner.numOppId
																FROM
																	OpportunityMaster OMInner
																INNER JOIN
																	OpportunityItems OIInner
																ON
																	OIInner.numOppId = OMInner.numOppId
																INNER JOIN
																	Item IInner
																ON
																	OIInner.numItemCode = IInner.numItemCode
																OUTER APPLY
																(
																	SELECT
																		SUM(OpportunityBizDocItems.numUnitHour) AS FulFilledQty
																	FROM
																		OpportunityBizDocs
																	INNER JOIN
																		OpportunityBizDocItems 
																	ON
																		OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																	WHERE
																		OpportunityBizDocs.numOppId = OMInner.numOppId
																		AND ISNULL(OpportunityBizDocs.numBizDocId,0) = 296
																		AND ISNULL(OpportunityBizDocs.bitFulFilled,0) = 1
																		AND OpportunityBizDocItems.numOppItemID = OIInner.numoppitemtCode
																) AS TempFulFilled
																WHERE
																	OMInner.numDomainId = ',@numDomainID,'
																	AND OMInner.tintOppType = 1
																	AND OMInner.tintOppStatus = 1
																	AND ISNULL(OMInner.tintshipped,0) = 0
																	AND UPPER(IInner.charItemType) = ''P''
																	AND ISNULL(OIInner.bitDropShip,0) = 0
																	AND ISNULL(OIInner.numUnitHour,0) <> ISNULL(TempFulFilled.FulFilledQty,0)',') ')
	END
	ELSE IF ISNULL(@tintDashboardReminderType,0) = 13
	BEGIN
		SET @strSql = CONCAT(@strSql,' AND Opp.numOppID IN (','SELECT DISTINCT
																	OMInner.numOppId
																FROM
																	OpportunityMaster OMInner
																INNER JOIN
																	OpportunityItems OIInner
																ON
																	OIInner.numOppId = OMInner.numOppID
																INNER JOIN
																	Item IInner
																ON
																	OIInner.numItemCode = IInner.numItemCode
																OUTER APPLY
																(
																	SELECT
																		SUM(OpportunityBizDocItems.numUnitHour) AS InvoicedQty
																	FROM
																		OpportunityBizDocs
																	INNER JOIN
																		OpportunityBizDocItems 
																	ON
																		OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																	WHERE
																		OpportunityBizDocs.numOppId = OMInner.numOppID
																		AND OpportunityBizDocItems.numOppItemID = OIInner.numoppitemtCode
																		AND OpportunityBizDocs.numBizDocId = ISNULL((SELECT TOP 1 numAuthoritativeSales FROM AuthoritativeBizDocs WHERE numDomainId=',@numDomainID,'),287)
																) AS TempInvoice
																WHERE
																	OMInner.numDomainId = ',@numDomainID,'
																	AND OMInner.tintOppType = 1
																	AND OMInner.tintOppStatus  = 1
																	AND ISNULL(OIInner.numUnitHour,0) <> ISNULL(TempInvoice.InvoicedQty,0)',') ')
	END
	ELSE IF ISNULL(@tintDashboardReminderType,0) = 14
	BEGIN
		DECLARE @TEMPOppID TABLE
		(
			numOppID NUMERIC(18,0)
		)

		INSERT INTO @TEMPOppID
		(
			numOppID
		)
		SELECT DISTINCT
			OM.numOppID
		FROM
			OpportunityMaster OM
		INNER JOIN
			OpportunityItems OI
		ON
			OM.numOppId = OI.numOppId
		WHERE   
			OM.numDomainId = @numDomainId
			AND OM.tintOppType = 1
			AND OM.tintOppStatus=1
			AND ISNULL(OI.bitDropShip, 0) = 1
			AND 1 = (CASE WHEN (SELECT COUNT(*) FROM OpportunityLinking OMInner INNER JOIN OpportunityItems OIInner ON OMInner.numChildOppID=OIInner.numOppId WHERE OMInner.numParentOppID = OM.numOppID AND OIInner.numItemCode=OI.numItemCode AND ISNULL(OIInner.numWarehouseItmsID,0)=ISNULL(OI.numWarehouseItmsID,0)) > 0 THEN 0 ELSE 1 END)


		If ISNULL((SELECT bitReOrderPoint FROM Domain WHERE numDomainId=@numDomainID),0) = 1
		BEGIN
			INSERT INTO @TEMPOppID
			(
				numOppID
			)
			SELECT DISTINCT
				OM.numOppID
			FROM
				OpportunityMaster OM
			INNER JOIN
				OpportunityItems OI
			ON
				OI.numOppId = OM.numOppId
			INNER JOIN 
				Item I 
			ON 
				I.numItemCode = OI.numItemCode
			INNER JOIN 
				WarehouseItems WI 
			ON 
				OI.numWarehouseItmsID = WI.numWareHouseItemID
			WHERE   
				OM.numDomainId = @numDomainId
				AND ISNULL((SELECT bitReOrderPoint FROM Domain WHERE numDomainId=@numDomainID),0) = 1
				AND (ISNULL(WI.numOnHand,0) + ISNULL(WI.numOnOrder,0) <= ISNULL(WI.numReOrder,0))
				AND OM.tintOppType = 1
				AND OM.tintOppStatus=1
				AND ISNULL(I.bitAssembly, 0) = 0
				AND ISNULL(I.bitKitParent, 0) = 0
				AND ISNULL(OI.bitDropship,0) = 0
				AND OM.numOppId NOT IN (SELECT numOppID FROM @TEMPOppID)

			IF (SELECT COUNT(*) FROM @TEMPOppID) > 0
			BEGIN
				DECLARE @tmpIds VARCHAR(MAX) = ''
				SELECT @tmpIds = CONCAT(@tmpIds,numOppID,', ') FROM @TEMPOppID
				SET @strSql = CONCAT(@strSql,' AND Opp.numOppID IN (',SUBSTRING(@tmpIds, 0, LEN(@tmpIds)),') ')
			END
			ELSE
			BEGIN
				SET @strSql = CONCAT(@strSql,' AND Opp.numOppID IN (0) ')
			END
		END
	END
	ELSE IF ISNULL(@tintDashboardReminderType,0) = 15
	BEGIN
		SET @strSql = CONCAT(@strSql,' AND Opp.numOppID IN (','SELECT
																	OMInner.numOppID
																FROM
																	OpportunityMaster OMInner
																WHERE
																	numDomainId=',@numDomainID,'
																	AND tintOppType=1
																	AND tintOppStatus=1
																	AND ISNULL(OMInner.tintshipped,0) = 0
																	AND dtReleaseDate IS NOT NULL
																	AND dtReleaseDate < DateAdd(minute,',@ClientTimeZoneOffset * -1,',GETUTCDATE())
																	AND (SELECT 
																			COUNT(*) 
																		FROM 
																		(
																			SELECT
																				OIInner.numoppitemtCode,
																				ISNULL(OIInner.numUnitHour,0) AS OrderedQty,
																				ISNULL(TempFulFilled.FulFilledQty,0) AS FulFilledQty
																			FROM
																				OpportunityItems OIInner
																			INNER JOIN
																				Item IInner
																			ON
																				OIInner.numItemCode = IInner.numItemCode
																			OUTER APPLY
																			(
																				SELECT
																					SUM(OpportunityBizDocItems.numUnitHour) AS FulFilledQty
																				FROM
																					OpportunityBizDocs
																				INNER JOIN
																					OpportunityBizDocItems 
																				ON
																					OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																				WHERE
																					OpportunityBizDocs.numOppId = OMInner.numOppId
																					AND ISNULL(OpportunityBizDocs.numBizDocId,0) = 296
																					AND ISNULL(OpportunityBizDocs.bitFulFilled,0) = 1
																					AND OpportunityBizDocItems.numOppItemID = OIInner.numoppitemtCode
																			) AS TempFulFilled
																			WHERE
																				OIInner.numOppID = OMInner.numOppID
																				AND UPPER(IInner.charItemType) = ''P''
																				AND ISNULL(OIInner.bitDropShip,0) = 0
																		) X
																		WHERE
																			X.OrderedQty <> X.FulFilledQty) > 0',') ')
	END



	DECLARE  @firstRec  AS INTEGER
	DECLARE  @lastRec  AS INTEGER
  
	SET @firstRec = (@CurrentPage - 1) * @PageSize
	SET @lastRec = (@CurrentPage * @PageSize + 1)



	-- EXECUTE FINAL QUERY
	DECLARE @strFinal AS NVARCHAR(MAX)
	IF @CurrentPage = -1 AND @PageSize = -1
	BEGIN
		SET @strFinal = CONCAT('SELECT COUNT(*) OVER() AS TotalRecords, ',@strColumns,@strSql,' ORDER BY ',@columnName,' ',@columnSortOrder,' OFFSET ',0,' ROWS FETCH NEXT ',25,' ROWS ONLY;')
	END
	ELSE
	BEGIN
		SET @strFinal = CONCAT('SELECT COUNT(*) OVER() AS TotalRecords, ',@strColumns,@strSql,' ORDER BY ',@columnName,' ',@columnSortOrder,' OFFSET ',(@CurrentPage - 1) * @PageSize,' ROWS FETCH NEXT ',@PageSize,' ROWS ONLY;')
	END
	PRINT CAST(@strFinal AS NTEXT)
	EXEC sp_executesql @strFinal

	SELECT * FROM #tempForm

	DROP TABLE #tempForm

	DROP TABLE #tempColorScheme
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetDomain')
DROP PROCEDURE USP_GetDomain
GO
CREATE PROCEDURE [dbo].[USP_GetDomain]
    (
      @numDomainID INT,
      @numType INT,
      @vcDomainCode VARCHAR(50) = '',
      @numSubscriberID INT
    )
AS 
    BEGIN
        IF @numType = 1 
            BEGIN
                SELECT  numDomainID,
                        vcDomainName,
                        numParentDomainID,
                        vcDomainCode
                FROM    Domain
                WHERE   numDomainID = @numDomainID
                        AND numSubscriberID = @numSubscriberID ;
            END
        ELSE 
            IF @numType = 2 
                BEGIN
                    SELECT  DN.numDomainID,
                            DN.vcDomainName,
                            DN.numParentDomainID,
                            DN.vcDomainCode,
                            DP.vcDomainName AS ParentDomainName
                    FROM    Domain DN INNER JOIN Domain DP ON DP.numDomainID = DN.numParentDomainID
                    WHERE   
                            DN.vcDomainCode LIKE @vcDomainCode + '%'
                            AND DN.numSubscriberID = @numSubscriberID
                            AND DP.numSubscriberID = @numSubscriberID
                    ORDER BY DN.vcDomainCode ;
                END
		 ELSE 
            IF @numType = 3 
                BEGIN
                    SELECT  DN.numDomainID,
                            DN.vcDomainName,
                            DN.numParentDomainID,
                            DN.vcDomainCode,
                           '' AS ParentDomainName
                    FROM    Domain DN 
                END
    END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetGrossProfitEstimate')
DROP PROCEDURE USP_GetGrossProfitEstimate
GO
CREATE PROCEDURE [dbo].[USP_GetGrossProfitEstimate] 
( 
@numDomainID as numeric(9)=0,    
@numOppID AS NUMERIC(9)=0
)
AS
BEGIN
	DECLARE @avgCost INT
	DECLARE @numShippingServiceItemID NUMERIC(18,0)
	SELECT 
		@avgCost=numCost
		,@numShippingServiceItemID=ISNULL(numShippingServiceItemID,0)
	FROM 
		Domain 
	WHERE 
		numDOmainId=@numDomainID

	DECLARE @monShippingProfit DECIMAL(20,5)
	DECLARE @monShippingAverageCost DECIMAL(20,5) = 0
	DECLARE @monMarketplaceShippingCost DECIMAL(20,5)

	SELECT @monMarketplaceShippingCost = ISNULL(monMarketplaceShippingCost,0) FROM OpportunityMaster WHERE numDomainID=@numDomainID AND numOppID=@numOppID 

	IF ISNULL(@monMarketplaceShippingCost,0) > 0
	BEGIN
		IF EXISTS (SELECT * FROM OpportunityItems WHERE numOppID=@numOppID AND numItemCode=@numShippingServiceItemID)
		BEGIN
			SET @monShippingAverageCost = @monMarketplaceShippingCost / ISNULL((SELECT COUNT(*) FROM OpportunityItems WHERE numOppID=@numOppID AND numItemCode=@numShippingServiceItemID),0)
			SET @monShippingProfit = ISNULL((SELECT SUM(monTotAmount) FROM OpportunityItems WHERE numOppID=@numOppID AND numItemCode=@numShippingServiceItemID),0) - @monMarketplaceShippingCost
		END
		ELSE
		BEGIN
			SET @monShippingProfit = 0
		END
	END
	ELSE
	BEGIN
		SET @monShippingProfit = 0
	END

	SELECT 
		numOppId
		,@monShippingProfit AS monShippingProfit
		,vcPOppName
		,vcItemName
		,monAverageCost
		,vcVendor
		,(ISNULL(monTotAmount,0) * fltExchangeRate)/ISNULL(NULLIF(numUnitHour,0),1) AS monPrice
		,VendorCost
		,numUnitHour
		,(ISNULL(monTotAmount,0) * fltExchangeRate) - (CASE WHEN numItemCode=@numShippingServiceItemID THEN (ISNULL(numUnitHour,0) *  @monShippingAverageCost) ELSE (ISNULL(numUnitHour,0) * (CASE @avgCost WHEN 3 THEN VendorCost * dbo.fn_UOMConversion(numBaseUnit,numItemCode,@numDomainID,numPurchaseUnit) WHEN 2 THEN numCost * dbo.fn_UOMConversion(numBaseUnit,numItemCode,@numDomainID,numUOMId) ELSE monAverageCost END)) END) AS Profit
		,((((ISNULL(monTotAmount,0) * fltExchangeRate)/ISNULL(NULLIF(numUnitHour,0),1)) - (CASE 
																							WHEN numItemCode=@numShippingServiceItemID 
																							THEN @monShippingAverageCost
																							ELSE
																								(CASE 
																									WHEN numPOItemID IS NOT NULL 
																									THEN ISNULL(monPOCost,0)
																									ELSE
																										(CASE @avgCost 
																											WHEN 3 THEN VendorCost * dbo.fn_UOMConversion(numBaseUnit,numItemCode,@numDomainID,numPurchaseUnit) 																									
																											ELSE monAverageCost 
																										END) 
																								END)
																							END)) / (CASE WHEN ((ISNULL(monTotAmount,0) * fltExchangeRate)/ISNULL(NULLIF(numUnitHour,0),1))=0 then 1 ELSE ((ISNULL(monTotAmount,0) * fltExchangeRate)/ISNULL(NULLIF(numUnitHour,0),1)) end)) * 100 ProfitPer
		,bitItemPriceApprovalRequired
		,(CASE WHEN numPOID IS NOT NULL THEN CONCAT('<a href="javascript:OpenOpp(',numPOID,')">',vcPOName,'</a>') ELSE '' END) vcPurchaseOrder
	FROM
	(
		SELECT 
			opp.numOppId
			,Opp.vcPOppName
			,OppI.vcItemName
			,(CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(monAverageCost,0) END) monAverageCost,
			V.numVendorID AS numVendorID
			,dbo.fn_getcomapnyname(V.numVendorID) as vcVendor
			,oppI.numUnitHour
			,ISNULL(OPPI.monPrice,0) AS monPrice
			,ISNULL(OPPI.monTotAmount,0) AS monTotAmount
			,ISNULL(V.monCost,0) VendorCost
			,ISNULL(OppI.bitItemPriceApprovalRequired,0) AS bitItemPriceApprovalRequired
			,ISNULL(OppI.numCost,0) numCost
			,I.numItemCode
			,numBaseUnit
			,numPurchaseUnit
			,ISNULL(OppI.numUOMId,numBaseUnit) AS numUOMId
			,ISNULL(Opp.fltExchangeRate,1) fltExchangeRate
			,ISNULL(TEMPMatchedPO.numOppId,TEMPPO.numOppId) AS numPOID
			,ISNULL(TEMPMatchedPO.vcPOppName,TEMPPO.vcPOppName) vcPOName
			,ISNULL(TEMPMatchedPO.numoppitemtCode,TEMPPO.numoppitemtCode) AS numPOItemID
			,ISNULL(TEMPMatchedPO.monPrice,TEMPPO.monPrice) AS monPOCost
		FROM 
			OpportunityMaster Opp 
		INNER JOIN 
			OpportunityItems OppI 
		ON 
			opp.numOppId=OppI.numOppId 
		OUTER APPLY
		(
			SELECT TOP 1
				OM.numOppID
				,OM.vcPOppName
				,OI.numoppitemtCode
				,OI.monPrice
			FROM
				SalesOrderLineItemsPOLinking SOLIPL
			INNER JOIN
				OpportunityMaster OM
			ON
				SOLIPL.numPurchaseOrderID = OM.numOppId
			INNER JOIN
				OpportunityItems OI
			ON
				OM.numOppId = OI.numOppId
				AND SOLIPL.numPurchaseOrderItemID = OI.numoppitemtCode
			WHERE
				SOLIPL.numSalesOrderID = Opp.numOppId
				AND SOLIPL.numSalesOrderItemID = OppI.numoppitemtCode
		) TEMPMatchedPO
		OUTER APPLY
		(
			SELECT TOP 1
				OM.numOppID
				,OM.vcPOppName
				,OI.numoppitemtCode
				,OI.monPrice
			FROM
				OpportunityLinking OL
			INNER JOIN
				OpportunityMaster OM
			ON
				OL.numChildOppID = OM.numOppId
			INNER JOIN
				OpportunityItems OI
			ON
				OM.numOppId = OI.numOppId
			WHERE
				OL.numParentOppID = Opp.numOppId
				AND OI.numItemCode = OppI.numItemCode
				AND OI.vcAttrValues = OppI.vcAttrValues
		) TEMPPO
		INNER JOIN 
			Item I 
		ON 
			OppI.numItemCode=i.numItemcode
		Left JOIN 
			Vendor V 
		ON 
			V.numVendorID=I.numVendorID 
			AND V.numItemCode=I.numItemCode
		WHERE 
			Opp.numDomainId=@numDomainID 
			AND opp.numOppId=@numOppID
			AND ISNULL(I.bitContainer,0) = 0
	) temp
END
/****** Object:  StoredProcedure [dbo].[USP_GetOppAddress]    Script Date: 05/08/2009 15:45:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj    
-- Modified by Ajit Singh on 01/10/2008 for Division ID in each select statement. 
-- like "@numDivisionID as DivisionID"
-- EXEC dbo.USP_GetOppAddress 2967,2
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_getoppaddress' ) 
    DROP PROCEDURE usp_getoppaddress
GO
CREATE PROCEDURE [dbo].[USP_GetOppAddress]
    @numOppID AS NUMERIC(9) = 0,
    @byteMode AS TINYINT,
    @numReturnHeaderID AS NUMERIC(9) = 0
AS 
    DECLARE @tintType AS TINYINT
    DECLARE @numDomainID AS NUMERIC(9) 
    DECLARE @numDivisionID AS NUMERIC(9)
    DECLARE @numContactID AS NUMERIC(9)
    DECLARE @Name AS VARCHAR(100)
    DECLARE @Phone AS VARCHAR(20)

    IF @byteMode = 0 -- Get Billing Address
        BEGIN
            SELECT  @tintType = tintBillToType,
                    @numDomainID = numDomainID,
                    @numDivisionID = numDivisionID,
                    @numContactID = numContactID
            FROM    OpportunityMaster
            WHERE   numOppId = @numOppID
            SELECT  @Name = ISNULL(vcFirstName, '') + ' ' + ISNULL(vcLastname, ''),
                    @Phone = (CASE WHEN LEN(ISNULL(numPhone,'')) > 0 THEN numPhone ELSE vcComPhone END)
            FROM    AdditionalContactsInformation
			INNER JOIN
				DivisionMaster 
			ON
				AdditionalContactsInformation.numDivisionId = DivisionMaster.numDivisionID
            WHERE   numContactID = @numContactID
            IF ( @tintType IS NULL
                 OR @tintType = 1
               ) 
                SELECT  @Name AS Name,
                        @Phone AS Phone,
                        dbo.fn_GetComapnyName(@numDivisionID) AS Company,
                        @numDivisionID AS DivisionID,
                        AD.VcStreet AS Street,
                        AD.VcCity AS City,
                        AD.numState AS State,
                        AD.vcPostalCode AS PostCode,
                        AD.numCountry AS Country,
						AD.numContact,
						(CASE WHEN ISNULL(AD.bitAltContact,0)=0 THEN CONCAT(AdditionalContactsInformation.vcFirstName,' ',AdditionalContactsInformation.vcLastName) ELSE '' END) vcContact,
						AD.bitAltContact,
						AD.vcAltContact
                FROM    dbo.AddressDetails AD
				LEFT JOIN
					AdditionalContactsInformation
				ON
					AD.numContact = AdditionalContactsInformation.numContactId
                WHERE   AD.numRecordID = @numDivisionID
                        AND AD.tintAddressOf = 2
                        AND AD.tintAddressType = 1
                        AND AD.bitIsPrimary = 1
            ELSE 
                IF @tintType = 0 
                    SELECT  @Name AS Name,
                            @Phone AS Phone,
                            Com1.vcCompanyname AS Company,
                            @numDivisionID AS DivisionID,
                            AD1.VcStreet AS Street,
                            AD1.VcCity AS City,
                            AD1.numState AS State,
                            AD1.vcPostalCode AS PostCode,
                            AD1.numCountry AS Country,
							AD1.numContact,
							(CASE WHEN ISNULL(AD1.bitAltContact,0)=0 THEN CONCAT(AdditionalContactsInformation.vcFirstName,' ',AdditionalContactsInformation.vcLastName) ELSE '' END) vcContact,
							AD1.bitAltContact,
							AD1.vcAltContact
                    FROM    companyinfo Com1
                            JOIN divisionmaster div1 ON com1.numCompanyID = div1.numCompanyID
                            JOIN Domain D1 ON D1.numDivisionID = div1.numDivisionID
                            JOIN dbo.AddressDetails AD1 ON AD1.numDomainID = div1.numDomainID
                                                           AND AD1.numRecordID = div1.numDivisionID
                                                           AND tintAddressOf = 2
                                                           AND tintAddressType = 1
                                                           AND bitIsPrimary = 1
							LEFT JOIN
								AdditionalContactsInformation
							ON
								AD1.numContact = AdditionalContactsInformation.numContactId
                    WHERE   D1.numDomainID = @numDomainID
                ELSE 
                    IF @tintType = 2 
                        SELECT  @Name AS Name,
                                @Phone AS Phone,
                                vcBillCompanyName AS Company,
                                numBillCompanyID AS DivisionID,
                                vcBillStreet AS Street,
                                vcBillCity AS City,
                                numBillState AS State,
                                vcBillPostCode AS PostCode,
                                numBillCountry AS Country,
                                dbo.GetCompanyNameFromContactID(@numContactID, @numDomainID) AS OppCompanyName,
								numBillingContact AS numContact,
								(CASE WHEN ISNULL(bitAltBillingContact,0)=0 THEN CONCAT(AdditionalContactsInformation.vcFirstName,' ',AdditionalContactsInformation.vcLastName) ELSE '' END) vcContact,
								bitAltBillingContact AS bitAltContact,
								vcAltBillingContact AS vcAltContact
                        FROM    OpportunityAddress
						LEFT JOIN
								AdditionalContactsInformation
							ON
								OpportunityAddress.numBillingContact = AdditionalContactsInformation.numContactId
                        WHERE   numOppID = @numOppID
                    ELSE 
                        IF @tintType = 3 
                            SELECT  @Name AS Name,
                                    @Phone AS Phone,
                                    vcBillCompanyName AS Company,
                                    numBillCompanyID AS DivisionID,
                                    vcBillStreet AS Street,
                                    vcBillCity AS City,
                                    numBillState AS State,
                                    vcBillPostCode AS PostCode,
                                    numBillCountry AS Country,
                                    dbo.GetCompanyNameFromContactID(@numContactID, @numDomainID) AS OppCompanyName,
									numBillingContact AS numContact,
									(CASE WHEN ISNULL(bitAltBillingContact,0)=0 THEN CONCAT(AdditionalContactsInformation.vcFirstName,' ',AdditionalContactsInformation.vcLastName) ELSE '' END) vcContact,
									bitAltBillingContact AS bitAltContact,
									vcAltBillingContact AS vcAltContact
                            FROM    OpportunityAddress
							LEFT JOIN
								AdditionalContactsInformation
							ON
								OpportunityAddress.numBillingContact = AdditionalContactsInformation.numContactId
                            WHERE   numOppID = @numOppID
        END
    ELSE 
        IF @byteMode = 1 -- Get Shipping Address
            BEGIN
                SELECT  @tintType = tintShipToType,
                        @numDomainID = numDomainID,
                        @numDivisionID = numDivisionID,
                        @numContactID = numContactID
                FROM    OpportunityMaster
                WHERE   numOppId = @numOppID
                SELECT  @Name = ISNULL(vcFirstName, '') + ' '
                        + ISNULL(vcLastname, ''),
                        @Phone = (CASE WHEN LEN(ISNULL(numPhone,'')) > 0 THEN numPhone ELSE vcComPhone END)
                FROM    AdditionalContactsInformation
				INNER JOIN
					DivisionMaster 
				ON
					AdditionalContactsInformation.numDivisionId = DivisionMaster.numDivisionID
                WHERE   numContactID = @numContactID

                IF (@tintType IS NULL OR @tintType = 1)
				BEGIN
                    SELECT  @Name AS Name,
                            @Phone AS Phone,
                            dbo.fn_GetComapnyName(@numDivisionID) AS Company,
                            @numDivisionID AS DivisionID,
                            AD.VcStreet AS Street,
                            AD.VcCity AS City,
                            AD.numState AS State,
                            AD.vcPostalCode AS PostCode,
                            AD.numCountry AS Country,
							AD.numContact,
							(CASE WHEN ISNULL(AD.bitAltContact,0)=0 THEN CONCAT(AdditionalContactsInformation.vcFirstName,' ',AdditionalContactsInformation.vcLastName) ELSE '' END) vcContact,
							AD.bitAltContact,
							AD.vcAltContact,
							dbo.fn_GetComapnyName(@numDivisionID) AS vcShipCompanyName
                    FROM    dbo.AddressDetails AD
					LEFT JOIN
						AdditionalContactsInformation
					ON
						AD.numContact = AdditionalContactsInformation.numContactId
                    WHERE   AD.numRecordID = @numDivisionID
                            AND AD.tintAddressOf = 2
                            AND AD.tintAddressType = 2
                            AND AD.bitIsPrimary = 1
				END
                ELSE IF @tintType = 0
				BEGIN
                    SELECT  (CASE 
								WHEN ISNULL(bitAltContact,0)=1 AND LEN(ISNULL(vcAltContact,'')) > 0
								THEN vcAltContact
								WHEN ISNULL(numContact,0) > 0
								THEN ISNULL(vcFirstName, '') + ' ' + ISNULL(vcLastname, '')
								ELSE @Name
							END) AS Name,
                            (CASE 
								WHEN ISNULL(bitAltContact,0)=1 AND LEN(ISNULL(vcAltContact,'')) > 0
								THEN (SELECT ISNULL(DM.vcComPhone,0) FROM DivisionMaster DM WHERE DM.numDivisionID=@numDivisionID)
								WHEN ISNULL(numContact,0) > 0
								THEN (CASE WHEN LEN(ISNULL(numPhone,'')) > 0 THEN  numPhone ELSE (SELECT ISNULL(DM.vcComPhone,0) FROM DivisionMaster DM WHERE DM.numDivisionID=@numDivisionID) END)
								ELSE @Phone
							END) AS Phone,
                            com1.vcCompanyname AS Company,
                            @numDivisionID AS DivisionID,
                            AD1.VcStreet AS Street,
                            AD1.VcCity AS City,
                            AD1.numState AS State,
                            AD1.vcPostalCode AS PostCode,
                            AD1.numCountry AS Country,
							AD1.numContact,
							(CASE WHEN ISNULL(AD1.bitAltContact,0)=0 THEN CONCAT(AdditionalContactsInformation.vcFirstName,' ',AdditionalContactsInformation.vcLastName) ELSE '' END) vcContact,
							AD1.bitAltContact,
							AD1.vcAltContact,
							com1.vcCompanyname AS vcShipCompanyName
                    FROM    companyinfo Com1
                            JOIN divisionmaster div1 ON com1.numCompanyID = div1.numCompanyID
                            JOIN Domain D1 ON D1.numDivisionID = div1.numDivisionID
                            JOIN dbo.AddressDetails AD1 ON AD1.numDomainID = div1.numDomainID
                                                            AND AD1.numRecordID = div1.numDivisionID
                                                            AND tintAddressOf = 2
                                                            AND tintAddressType = 2
                                                            AND bitIsPrimary = 1
							LEFT JOIN
								AdditionalContactsInformation
							ON
								AD1.numContact = AdditionalContactsInformation.numContactId
                    WHERE   D1.numDomainID = @numDomainID
                END    
				ELSE IF @tintType = 2 
				BEGIN
                    SELECT  (CASE 
								WHEN ISNULL(bitAltShippingContact,0)=1 AND LEN(ISNULL(vcAltShippingContact,'')) > 0
								THEN vcAltShippingContact
								WHEN ISNULL(numShippingContact,0) > 0
								THEN ISNULL(vcFirstName, '') + ' ' + ISNULL(vcLastname, '')
								ELSE @Name
							END) AS Name,
                            (CASE 
								WHEN ISNULL(bitAltShippingContact,0)=1 AND LEN(ISNULL(vcAltShippingContact,'')) > 0
								THEN (SELECT ISNULL(DM.vcComPhone,0) FROM DivisionMaster DM WHERE DM.numDivisionID=@numDivisionID)
								WHEN ISNULL(numShippingContact,0) > 0
								THEN (CASE WHEN LEN(ISNULL(numPhone,'')) > 0 THEN  numPhone ELSE (SELECT ISNULL(DM.vcComPhone,0) FROM DivisionMaster DM WHERE DM.numDivisionID=@numDivisionID) END)
								ELSE @Phone
							END) AS Phone,
                            dbo.fn_GetComapnyName(@numDivisionID) AS Company,
							@numDivisionID AS DivisionID,
                            vcShipStreet AS Street,
                            vcShipCity AS City,
                            numShipState AS State,
                            vcShipPostCode AS PostCode,
                            numShipCountry AS Country,
                            dbo.GetCompanyNameFromContactID(@numContactID, @numDomainID) AS OppCompanyName,
							numShippingContact AS numContact,
							(CASE WHEN ISNULL(bitAltShippingContact,0)=0 THEN CONCAT(AdditionalContactsInformation.vcFirstName,' ',AdditionalContactsInformation.vcLastName) ELSE '' END) vcContact,
							bitAltShippingContact AS bitAltContact,
							vcAltShippingContact AS vcAltContact,
							vcShipCompanyName
                    FROM    OpportunityAddress
					LEFT JOIN
						AdditionalContactsInformation
					ON
						OpportunityAddress.numShippingContact = AdditionalContactsInformation.numContactId
                    WHERE   numOppID = @numOppID
                END
				ELSE IF @tintType = 3
				BEGIN
                    SELECT  (CASE 
								WHEN ISNULL(bitAltShippingContact,0)=1 AND LEN(ISNULL(vcAltShippingContact,'')) > 0
								THEN vcAltShippingContact
								WHEN ISNULL(numShippingContact,0) > 0
								THEN ISNULL(vcFirstName, '') + ' ' + ISNULL(vcLastname, '')
								ELSE @Name
							END) AS Name,
                                (CASE 
								WHEN ISNULL(bitAltShippingContact,0)=1 AND LEN(ISNULL(vcAltShippingContact,'')) > 0
								THEN (SELECT ISNULL(DM.vcComPhone,0) FROM DivisionMaster DM WHERE DM.numDivisionID=@numDivisionID)
								WHEN ISNULL(numShippingContact,0) > 0
								THEN (CASE WHEN LEN(ISNULL(numPhone,'')) > 0 THEN  numPhone ELSE (SELECT ISNULL(DM.vcComPhone,0) FROM DivisionMaster DM WHERE DM.numDivisionID=@numDivisionID) END)
								ELSE @Phone
							END) AS Phone,
                            dbo.fn_GetComapnyName(@numDivisionID) AS Company,
							@numDivisionID AS DivisionID,
                            vcShipStreet AS Street,
                            vcShipCity AS City,
                            numShipState AS State,
                            vcShipPostCode AS PostCode,
                            numShipCountry AS Country,
                            dbo.GetCompanyNameFromContactID(@numContactID, @numDomainID) AS OppCompanyName,
							numShippingContact AS numContact,
							(CASE WHEN ISNULL(bitAltShippingContact,0)=0 THEN CONCAT(AdditionalContactsInformation.vcFirstName,' ',AdditionalContactsInformation.vcLastName) ELSE '' END) vcContact,
							bitAltShippingContact AS bitAltContact,
							vcAltShippingContact AS vcAltContact,
							vcShipCompanyName
                    FROM    OpportunityAddress
					LEFT JOIN
						AdditionalContactsInformation
					ON
						OpportunityAddress.numShippingContact = AdditionalContactsInformation.numContactId
                    WHERE   numOppID = @numOppID
				END
            END
    
    IF @byteMode = 2 -- Get Billing Address
        BEGIN
    
            SELECT  @numDomainID = numDomainID,
                    @numDivisionID = numDivisionID,
                    @numContactID = numContactID
            FROM    dbo.ReturnHeader
            WHERE   numReturnHeaderID = @numReturnHeaderID
      
            SELECT  @Name = ISNULL(vcFirstName, '') + ' ' + ISNULL(vcLastname, ''),
                    @Phone = numPhone
            FROM    AdditionalContactsInformation
            WHERE   numContactID = @numContactID
      
            SELECT  @Name AS Name,
                    @Phone AS Phone,
                    vcBillCompanyName AS Company,
                    @numDivisionID AS DivisionID,
                    vcBillStreet AS Street,
                    vcBillCity AS City,
                    numBillState AS State,
                    vcBillPostCode AS PostCode,
                    numBillCountry AS Country,
                    dbo.GetCompanyNameFromContactID(@numContactID,
                                                    @numDomainID) AS OppCompanyName
            FROM    OpportunityAddress
            WHERE   numReturnHeaderID = @numReturnHeaderID
		
        END
	
	IF @byteMode = 3 -- Get Shipping Address
        BEGIN
    
			DECLARE @numAddressID AS NUMERIC(18,0)
			DECLARE @vcCompanyName AS VARCHAR(100)
			
						
            
            SELECT  @numAddressID = numAddressID, @numDivisionId = numDivisionId
            FROM    dbo.ProjectsMaster
            WHERE   numProId = @numOppID
						
            SELECT  @Name = ISNULL(vcFirstName, '') + ' ' + ISNULL(vcLastname, ''),
                    @Phone = numPhone,
                    @numContactID = numContactId,
                    @vcCompanyName = ''
            FROM    AdditionalContactsInformation
            WHERE numDomainID = @numDomainId
			AND numDivisionId = @numDivisionId
			AND ISNULL(bitPrimaryContact,0) = 1
      
            SELECT  (CASE 
						WHEN ISNULL(bitAltContact,0)=1 AND LEN(ISNULL(vcAltContact,'')) > 0
						THEN vcAltContact
						WHEN ISNULL(numContact,0) > 0
						THEN ISNULL(vcFirstName, '') + ' ' + ISNULL(vcLastname, '')
						ELSE @Name
					END) AS Name,
                    (CASE 
						WHEN ISNULL(bitAltContact,0)=1 AND LEN(ISNULL(vcAltContact,'')) > 0
						THEN (SELECT ISNULL(DM.vcComPhone,0) FROM DivisionMaster DM WHERE DM.numDivisionID=@numDivisionID)
						WHEN ISNULL(numContact,0) > 0
						THEN (CASE WHEN LEN(ISNULL(numPhone,'')) > 0 THEN  numPhone ELSE (SELECT ISNULL(DM.vcComPhone,0) FROM DivisionMaster DM WHERE DM.numDivisionID=@numDivisionID) END)
						ELSE @Phone
					END) AS Phone,
                    @vcCompanyName AS Company,
                    @numDivisionID AS DivisionID,
                    vcStreet AS Street,
                    vcCity AS City,
                    numState AS State,
                    vcPostalCode AS PostCode,
                    numCountry AS Country,
                    dbo.GetCompanyNameFromContactID(@numContactID,@numDomainID) AS OppCompanyName
            FROM   dbo.AddressDetails
			LEFT JOIN
				AdditionalContactsInformation
			ON
				AddressDetails.numContact = AdditionalContactsInformation.numContactId
            WHERE  numRecordID = @numOppID
		
        END        
        

GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetOrderItems' ) 
    DROP PROCEDURE USP_GetOrderItems
GO
CREATE PROCEDURE USP_GetOrderItems ( @tintMode TINYINT = 0,
                                     @numOppItemID NUMERIC(9) = 0,
                                     @numOppID NUMERIC(9) = 0,
                                     @numDomainId NUMERIC(9),
                                     @ClientTimeZoneOffset INT,
									 @vcOppItemIDs VARCHAR(2000)='' )
AS 
BEGIN
	
    IF @tintMode = 1 --Add Return grid data & For Recurring Get Original Units to split
        BEGIN
            SELECT  OI.numoppitemtCode,
                    OI.numItemCode,
                    OI.vcItemName,
                    OI.vcItemDesc,
					OI.vcNotes as txtNotes,ISNULL(OI.numCost,0) AS numCost,
                    OI.numUnitHour numOriginalUnitHour,
					CAST(( dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), OI.numItemCode,
                                    OM.numDomainId, ISNULL(OI.numUOMId, 0))
						* OI.numUnitHour ) AS NUMERIC(18, 2)) AS numUnitHour,
					OI.monPrice
            FROM    dbo.OpportunityMaster OM
                    INNER JOIN dbo.OpportunityItems OI ON OM.numOppId = OI.numOppId
                    INNER JOIN dbo.Item I ON I.numItemCode = OI.numItemCode
            WHERE   OM.numDomainId = @numDomainId
                    AND OM.numOppId = @numOppID
                    
        END
 
    IF @tintMode = 2 -- Get single item based on PK, or all items
		-- Used by Sales/Purchase Template Load items in New Sales Order
        BEGIN 
            SELECT  OI.numoppitemtCode,
                    OI.numOppId,
                    OI.numItemCode,
					OI.bitItemPriceApprovalRequired,
                    OI.numUnitHour numOriginalUnitHour,
                    OI.monPrice,
                    OI.monTotAmount,
                    OI.numSourceID,
					ISNULL(OI.numCost,0) AS numCost,
                    OI.vcItemDesc,
					OI.vcNotes as txtNotes,
                    OI.numWarehouseItmsID,
                    OI.vcType,
                    OI.vcAttributes,
                    OI.bitDropShip,
                    ISNULL(OI.numUnitHourReceived,0) AS numUnitHourReceived,
                    OI.bitDiscountType,
                    OI.fltDiscount,
                    OI.monTotAmtBefDiscount,
                    ISNULL(OI.numQtyShipped,0) AS numQtyShipped,
                    OI.vcItemName,
                    OI.vcModelID,
                    OI.vcPathForTImage,
                    OI.vcManufacturer,
                    OI.monVendorCost,
                    ISNULL(OI.numUOMId, 0) AS numUOMId,
                    ISNULL(OI.numUOMId, 0) numUOM, -- used from add edit order
                    ISNULL(U.vcUnitName, '') vcUOMName,
                    dbo.fn_UOMConversion(OI.numUOMId, OI.numItemCode,
                                         OM.numDomainId, NULL) AS UOMConversionFactor,
                    OI.bitWorkOrder,
                    isnull(OI.numVendorWareHouse,0) as numVendorWareHouse,
                    OI.numShipmentMethod,
                    OI.numSOVendorId,
                    OI.numProjectID,
                    OI.numClassID,
                    OI.numProjectStageID,
                    OI.numToWarehouseItemID,
                    WItems.numWarehouseID,
                    W.vcWareHouse AS Warehouse,
					CAST(( dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), OI.numItemCode,
                                    OM.numDomainId, ISNULL(OI.numUOMId, 0))
						* OI.numUnitHour ) AS NUMERIC(18, 2)) AS numUnitHour,
					isnull(OI.numClassID,0) as numClassID,
					isnull(OI.numProjectID,0) as numProjectID,
					ISNULL(dbo.fn_GetUOMName(I.numBaseUnit),'') AS vcBaseUOMName,
					OI.bitDropShip AS bitAllowDropShip,
					I.numVendorID,
					ISNULL(I.bitFreeShipping,0) AS [bitFreeShipping],
					ISNULL(I.txtItemDesc,'') txtItemDesc,
					ISNULL(I.vcModelID,'') vcModelID,
					ISNULL(I.vcManufacturer,'') vcManufacturer,
					ISNULL(I.numBarCodeId,'') numBarCodeId,
					ISNULL(I.bitSerialized,0) bitSerialized,
					ISNULL(I.bitKitParent,0) bitKitParent,
					ISNULL(I.bitAssembly,0) bitAssembly,
					ISNULL(I.IsArchieve,0) IsArchieve,
					ISNULL(I.bitLotNo,0) bitLotNo,
					ISNULL(WItems.monWListPrice,0) monWListPrice,
					ISNULL(I.bitTaxable, 0) bitTaxable,
					ISNULL(I.numItemClassification,0) AS numItemClassification,
                    I.charItemType,
                    I.bitKitParent,
					I.bitAssembly,
					I.bitSerialized,
					ISNULL(I.fltWeight,0) AS [fltWeight],
					ISNULL(I.fltLength,0) AS [fltLength],
					ISNULL(I.fltWidth,0) AS [fltWidth],
					ISNULL(I.fltHeight,0) AS [fltHeight],
					CASE WHEN I.numItemGroup > 0 AND ISNULL(I.bitMatrix,0)=0 THEN ISNULL(WItems.vcWHSKU,ISNULL(I.vcSKU,'')) ELSE ISNULL(I.vcSKU,'') END AS [vcSKU],
					(CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(I.monAverageCost,0) END)  AS monAverageCost,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numOnHand),0) AS VARCHAR(30)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numOnHand,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numOnOrder),0) AS VARCHAR(30)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numOnOrder,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numAllocation),0) AS VARCHAR(30)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numAllocation,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numBackOrder),0) AS VARCHAR(30)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numBackOrder,
					ISNULL(OI.vcAttrValues,'') AS vcAttrValues,
					ISNULL(OI.numSortOrder,0) numSortOrder
					,(CASE WHEN OM.tintOppType=2 THEN ISNULL(vcNotes,'') ELSE '' END) AS vcVendorNotes
					,CPN.CustomerPartNo
            FROM    dbo.OpportunityItems OI
                    INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OI.numOppId
					INNER JOIN DivisionMaster DM ON OM.numDivisionId = DM.numDivisionID
                    INNER JOIN dbo.Item I ON I.numItemCode = OI.numItemCode
                    LEFT JOIN UOM U ON U.numUOMId = OI.numUOMId
                    LEFT JOIN WareHouseItems WItems ON WItems.numWareHouseItemID = OI.numWarehouseItmsID
                    LEFT JOIN Warehouses W ON W.numWarehouseID = WItems.numWarehouseID
					LEFT JOIN CustomerPartNumber CPN ON CPN.numItemCode = I.numItemCode 
						AND CPN.numCompanyId=DM.numCompanyID
            WHERE   OI.numOppId = @numOppID
                    AND ( OI.numoppitemtCode = @numOppItemID
                          OR @numOppItemID = 0
                        )
                    AND OM.numDomainId = @numDomainId
                    
        END
 
    IF @tintMode = 3 -- Get item for SO to PO and vice versa creation
        BEGIN
            DECLARE @tintOppType AS TINYINT
			SELECT @tintOppType=tintOppType FROM OpportunityMaster WHERE numOppId=@numOppID

			IF @tintOppType = 1
			BEGIN
				DECLARE @TempItems TABLE
				(
					ID INT IDENTITY(1,1),numoppitemtCode NUMERIC(18,0),numItemCode NUMERIC(18,0),vcItemName VARCHAR(300),txtNotes Varchar(MAX),charItemType char,vcModelID VARCHAR(100),vcItemDesc TEXT,vcType VARCHAR(20),vcPathForTImage VARCHAR(200),
					numWarehouseItmsID NUMERIC(18,0),vcAttributes VARCHAR(500),bitDropShip BIT,numOrigUnitHour FLOAT,numUnitHour FLOAT,numUOM NUMERIC(18,0),vcUOMName VARCHAR(100),UOMConversionFactor DECIMAL(18,5),
					monPrice DECIMAL(20,5),numVendorID NUMERIC(18,0),numCost decimal(30, 16),vcPartNo VARCHAR(300),monVendorCost DECIMAL(20,5),numSOVendorId NUMERIC(18,0),numClassID NUMERIC(18,0),numProjectID NUMERIC(18,0),tintLevel TINYINT, vcAttrValues VARCHAR(500),
					ItemRequiredDate DATETIME,numPOID NUMERIC(18,0), numPOItemID NUMERIC(18,0),vcPOName VARCHAR(300)
				)

				INSERT INTO 
					@TempItems
				SELECT
					OI.numoppitemtCode,OI.numItemCode,OI.vcItemName,OI.vcNotes as txtNotes,I.charItemType,OI.vcModelID,OI.vcItemDesc,OI.vcType,ISNULL(OI.vcPathForTImage, ''),ISNULL(OI.numWarehouseItmsID,0),ISNULL(OI.vcAttributes, ''),
					ISNULL(OI.bitDropShip, 0),ISNULL(OI.numUnitHour,0),OI.numUnitHour * dbo.fn_UOMConversion(I.numBaseUnit, OI.numItemCode,OM.numDomainId, OI.numUOMId),ISNULL(OI.numUOMId, 0),ISNULL(U.vcUnitName, ''),dbo.fn_UOMConversion(OI.numUOMId, OI.numItemCode,OM.numDomainId, NULL) AS UOMConversionFactor,
					OI.monPrice,ISNULL(I.numVendorID, 0),ISNULL(OI.numCost,0),ISNULL(V.vcPartNo, ''),ISNULL(V.monCost, 0),ISNULL(OI.numSOVendorId, 0),0,0,0,ISNULL(vcAttrValues,'')
					,(CASE WHEN ISNULL(OI.ItemReleaseDate,'') = '' THEN
							 ISNULL(OM.dtReleaseDate,'') 
						ELSE OI.ItemReleaseDate 
						END ),ISNULL(SOLIPL.numPurchaseOrderID,0),ISNULL(SOLIPL.numPurchaseOrderItemID,0),ISNULL(OMPO.vcPOppName,'')
				FROM    
					dbo.OpportunityItems OI
					INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OI.numOppId
					INNER JOIN dbo.Item I ON I.numItemCode = OI.numItemCode
					LEFT JOIN UOM U ON U.numUOMId = OI.numUOMId
					LEFT JOIN Vendor V ON V.numVendorID = I.numVendorID	AND I.numItemCode = V.numItemCode
					LEFT JOIN SalesOrderLineItemsPOLinking SOLIPL ON OI.numOppId=SOLIPL.numSalesOrderID AND OI.numoppitemtCode=SOLIPL.numSalesOrderItemID
					LEFT JOIN OpportunityMaster OMPO ON SOLIPL.numPurchaseOrderID = OMPO.numOppId
				WHERE   
					OI.numOppId = @numOppID
					AND OM.numDomainId = @numDomainId
					AND ISNULL(I.bitKitParent,0) = 0
					AND ISNULL(I.bitAssembly,0) = 0
				ORDER BY
					OI.numSortOrder


				--INSERT KIT CHILD ITEMS WHICH ARE NOT KIT AND NOT YET ADDED
				INSERT INTO 
					@TempItems
				SELECT
					0,OKI.numChildItemID,'' as txtNotes,I.vcItemName,I.charItemType,I.vcModelID,I.txtItemDesc,(CASE WHEN I.charitemType = 'P' THEN 'Product' WHEN I.charitemType = 'S' THEN 'Service' END),
					ISNULL((SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = I.numItemCode AND bitDefault=1),''),ISNULL(OKI.numWareHouseItemId,0),
					ISNULL(dbo.USP_GetAttributes(OKI.numWarehouseItemID, bitSerialized),''),0,ISNULL(OKI.numQtyItemsReq,0),ISNULL(OKI.numQtyItemsReq,0)  * dbo.fn_UOMConversion(I.numBaseUnit, I.numItemCode,OM.numDomainId, OKI.numUOMId),ISNULL(OKI.numUOMId, 0),ISNULL(U.vcUnitName, ''),
					dbo.fn_UOMConversion(OKI.numUOMId, OKI.numChildItemID,OM.numDomainId, NULL),
					(CASE WHEN I.charitemType = 'P' THEN WI.monWListPrice ELSE I.monListPrice END),
					ISNULL(I.numVendorID, 0),ISNULL(t1.numCost,0),ISNULL(V.vcPartNo, ''),ISNULL(V.monCost, 0),0,0,0,1,'',
					t1.ItemRequiredDate,0,0,''
				FROM    
					dbo.OpportunityKitItems OKI
				INNER JOIN @TempItems t1 ON OKI.numChildItemID = t1.numItemCode AND ISNULL(OKI.numWareHouseItemId,0) = ISNULL(t1.numWarehouseItmsID,0)
				LEFT JOIN WarehouseItems WI ON OKI.numWareHouseItemId = WI.numWarehouseItemID
				INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OKI.numOppId
				INNER JOIN dbo.Item I ON I.numItemCode = OKI.numChildItemID
				LEFT JOIN UOM U ON U.numUOMId = OKI.numUOMId
				LEFT JOIN Vendor V ON V.numVendorID = I.numVendorID	AND I.numItemCode = V.numItemCode
				WHERE   
					OKI.numOppId = @numOppID
					AND OM.numDomainId = @numDomainId
					AND ISNULL(I.bitKitParent,0) = 0
					AND ISNULL(I.bitAssembly,0) = 0
					AND t1.ID IS NULL


				-- FOR KIT CHILD ITEMS WHICH ARE ALREADY ADDED AT PARENT LEVEL - UPDATE QTY ONLY
				UPDATE 
					t2
				SET 
					t2.numOrigUnitHour = ISNULL(t2.numOrigUnitHour,0) + ISNULL(OKI.numQtyItemsReq,0),
					t2.numUnitHour = ISNULL(t2.numUnitHour,0) + (ISNULL(OKI.numQtyItemsReq,0) * dbo.fn_UOMConversion(I.numBaseUnit, I.numItemCode,OM.numDomainId, OKI.numUOMId)),
					t2.monPrice = (CASE 
									WHEN ISNULL(t2.monPrice,0) < (CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END)  
									THEN t2.monPrice
									ELSE (CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END)
									END)
				FROM
					@TempItems t2
				INNER JOIN
					OpportunityKitItems OKI 
				ON
					OKI.numChildItemID = t2.numItemCode AND ISNULL(OKI.numWareHouseItemId,0) = ISNULL(t2.numWarehouseItmsID,0)
				LEFT JOIN WarehouseItems WI ON OKI.numWareHouseItemId = WI.numWarehouseItemID
				INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OKI.numOppId
				INNER JOIN dbo.Item I ON I.numItemCode = OKI.numChildItemID
				LEFT JOIN UOM U ON U.numUOMId = OKI.numUOMId
				LEFT JOIN Vendor V ON V.numVendorID = I.numVendorID	AND I.numItemCode = V.numItemCode
				WHERE   
					OKI.numOppId = @numOppID
					AND OM.numDomainId = @numDomainId
					AND ISNULL(I.bitKitParent,0) = 0
					AND ISNULL(I.bitAssembly,0) = 0
					AND t2.tintLevel <> 1

				--INSERT SUB KIT CHILD ITEMS WHICH ARE NOT KIT AND NOT YET ADDED
				INSERT INTO 
					@TempItems
				SELECT
					0,OKCI.numItemID,'' as txtNotes,I.vcItemName,I.charItemType,I.vcModelID,I.txtItemDesc,(CASE WHEN I.charitemType = 'P' THEN 'Product' WHEN I.charitemType = 'S' THEN 'Service' END),
					ISNULL((SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = I.numItemCode AND bitDefault=1),''),ISNULL(OKCI.numWareHouseItemId,0),
					ISNULL(dbo.USP_GetAttributes(OKCI.numWarehouseItemID, bitSerialized),''),0,ISNULL(OKCI.numQtyItemsReq,0),ISNULL(OKCI.numQtyItemsReq,0) * dbo.fn_UOMConversion(I.numBaseUnit, I.numItemCode,OM.numDomainId, OKCI.numUOMId),ISNULL(OKCI.numUOMId, 0),ISNULL(U.vcUnitName, ''),
					dbo.fn_UOMConversion(OKCI.numUOMId, OKCI.numItemID,OM.numDomainId, NULL),
					(CASE WHEN I.charitemType = 'P' THEN WI.monWListPrice ELSE I.monListPrice END),
					ISNULL(I.numVendorID, 0),ISNULL(t1.numCost,0),ISNULL(V.vcPartNo, ''),ISNULL(V.monCost, 0),0,0,0,2,'',
					t1.ItemRequiredDate,0,0,''
				FROM    
					dbo.OpportunityKitChildItems OKCI
				INNER JOIN @TempItems t1 ON OKCI.numItemID = t1.numItemCode AND ISNULL(OKCI.numWareHouseItemId,0) = ISNULL(t1.numWarehouseItmsID,0)
				LEFT JOIN WarehouseItems WI ON OKCI.numWareHouseItemId = WI.numWarehouseItemID
				INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OKCI.numOppId
				INNER JOIN dbo.Item I ON I.numItemCode = OKCI.numItemID
				LEFT JOIN UOM U ON U.numUOMId = OKCI.numUOMId
				LEFT JOIN Vendor V ON V.numVendorID = I.numVendorID	AND I.numItemCode = V.numItemCode
				WHERE   
					OKCI.numOppId = @numOppID
					AND OM.numDomainId = @numDomainId
					AND ISNULL(I.bitKitParent,0) = 0
					AND ISNULL(I.bitAssembly,0) = 0
					AND t1.ID IS NULL

				-- FOR SUB KIT CHILD ITEMS WHICH ARE ALREADY ADDED AT PARENT LEVEL - UPDATE QTY ONLY
				UPDATE 
					t2
				SET 
					t2.numOrigUnitHour = ISNULL(t2.numOrigUnitHour,0) + ISNULL(OKCI.numQtyItemsReq,0),
					t2.numUnitHour = ISNULL(t2.numUnitHour,0) + (ISNULL(OKCI.numQtyItemsReq,0) * dbo.fn_UOMConversion(I.numBaseUnit, I.numItemCode,OM.numDomainId, OKCI.numUOMId)),
					t2.monPrice = (CASE 
									WHEN ISNULL(t2.monPrice,0) < (CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END)  
									THEN t2.monPrice
									ELSE (CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END)
									END)
				FROM
					@TempItems t2
				INNER JOIN
					OpportunityKitChildItems OKCI 
				ON
					OKCI.numItemID = t2.numItemCode AND ISNULL(OKCI.numWareHouseItemId,0) = ISNULL(t2.numWarehouseItmsID,0)
				LEFT JOIN WarehouseItems WI ON OKCI.numWareHouseItemId = WI.numWarehouseItemID
				INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OKCI.numOppId
				INNER JOIN dbo.Item I ON I.numItemCode = OKCI.numItemID
				LEFT JOIN UOM U ON U.numUOMId = OKCI.numUOMId
				LEFT JOIN Vendor V ON V.numVendorID = I.numVendorID	AND I.numItemCode = V.numItemCode
				WHERE   
					OKCI.numOppId = @numOppID
					AND OM.numDomainId = @numDomainId
					AND ISNULL(I.bitKitParent,0) = 0
					AND ISNULL(I.bitAssembly,0) = 0
					AND t2.tintLevel <> 2

				SELECT * FROM @TempItems
			END
			ELSE
			BEGIN
				SELECT  OI.numoppitemtCode,
                    OI.numOppId,
					OI.vcNotes as txtNotes,
					OI.bitItemPriceApprovalRequired,
                    OI.vcItemDesc,
                    ISNULL(OI.vcPathForTImage, '') vcPathForTImage,
                    ISNULL(OI.numWarehouseItmsID, 0) numWarehouseItmsID,
                    OI.vcItemName,
                    OI.vcModelID,
					ISNULL(OI.numCost,0) numCost,
                    ISNULL(OI.vcAttributes, '') vcAttributes,
                    OI.vcType,
                    ISNULL(OI.bitDropShip, 0) bitDropShip,
                    OI.numItemCode,
					OI.numUnitHour numOrigUnitHour,
                    OI.numUnitHour numOriginalUnitHour,
                    ISNULL(OI.numUOMId, 0) AS numUOM,
                    ISNULL(U.vcUnitName, '') vcUOMName,
                    dbo.fn_UOMConversion(OI.numUOMId, OI.numItemCode,
                                         OM.numDomainId, NULL) AS UOMConversionFactor,
                    ISNULL(I.numVendorID, 0) numVendorID,
                    ISNULL(V.monCost, 0) monVendorCost,
                    ISNULL(V.vcPartNo, '') vcPartNo,
                    OI.monPrice,
                    ISNULL(OI.numSOVendorId, 0) numSOVendorId,
                    ISNULL(I.charItemType, 'N') charItemType,
					CAST(( dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), OI.numItemCode,
                                    OM.numDomainId, ISNULL(OI.numUOMId, 0))
						* OI.numUnitHour ) AS NUMERIC(18, 2)) AS numUnitHour,
					isnull(OI.numClassID,0) as numClassID,isnull(OI.numProjectID,0) as numProjectID,ISNULL(I.fltWeight,0) AS [fltWeight],ISNULL(I.bitFreeShipping,0) AS [bitFreeShipping],
					ISNULL(I.fltLength,0) AS [fltLength],
					ISNULL(I.fltWidth,0) AS [fltWidth],
					ISNULL(I.fltHeight,0) AS [fltHeight],
					ISNULL((SELECT vcWHSKU FROM dbo.WareHouseItems WHERE numWarehouseItemID = OI.numWarehouseItmsID ),ISNULL(I.vcSKU,'')) AS [vcSKU],
					ISNULL(vcAttrValues,'') AS vcAttrValues,
					(CASE WHEN ISNULL(OI.ItemReleaseDate,'') = '' THEN
							 ISNULL(OM.dtReleaseDate,'') 
						ELSE  OI.ItemReleaseDate
						END ) AS ItemRequiredDate, 0 AS numPOID,0 AS numPOItemID,'' AS vcPOName
				FROM    dbo.OpportunityItems OI
						INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OI.numOppId
						INNER JOIN dbo.Item I ON I.numItemCode = OI.numItemCode
						LEFT JOIN UOM U ON U.numUOMId = OI.numUOMId
						LEFT JOIN Vendor V ON V.numVendorID = I.numVendorID
												AND I.numItemCode = V.numItemCode
				WHERE   OI.numOppId = @numOppID
						AND OM.numDomainId = @numDomainId
			END
        END
        IF @tintMode = 4 -- Add Edit Order items
        BEGIN 
            SELECT  OI.numoppitemtCode,
                    OI.numOppId,
					OI.vcNotes as txtNotes,
                    OI.numItemCode,
					OI.bitItemPriceApprovalRequired,
					OI.numUnitHour AS numOrigUnitHour,
                    OI.numUnitHour,
                    OI.monPrice,
                    OI.monTotAmount,
                    OI.numSourceID,
                    OI.vcItemDesc,
					OI.vcNotes,
					ISNULL(OI.numCost,0) numCost,
                    OI.numWarehouseItmsID,
                    OI.vcType ItemType,
                    OI.vcAttributes Attributes,
                    OI.bitDropShip DropShip,
                    OI.numUnitHourReceived,
                    OI.bitDiscountType,
                    OI.fltDiscount,
                    OI.monTotAmtBefDiscount,
					ISNULL(OI.monTotAmtBefDiscount,0) - ISNULL(OI.monTotAmount,0) AS TotalDiscountAmount,
                    OI.numQtyShipped,
                    OI.vcItemName,
                    OI.vcModelID,
                    OI.vcPathForTImage,
                    OI.vcManufacturer,
                    OI.monVendorCost,
                    ISNULL(OI.numUOMId, 0) AS numUOM,
                    ISNULL(U.vcUnitName, '') vcUOMName,
                    ISNULL(dbo.fn_UOMConversion(OI.numUOMId, OI.numItemCode,
                                         OM.numDomainId, NULL),1) AS UOMConversionFactor,
                    OI.bitWorkOrder,
                    isnull(OI.numVendorWareHouse,0) as numVendorWareHouse,
                    OI.numShipmentMethod,
                    OI.numSOVendorId,
                    OI.numProjectStageID,
                    OI.numToWarehouseItemID,
                   UPPER(I.charItemType) charItemType,
                    WItems.numWarehouseID,
                    W.vcWareHouse AS Warehouse,
                    0 as Op_Flag,
					CAST(( dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), OI.numItemCode,
                                    OM.numDomainId, ISNULL(OI.numUOMId, 0))
						* OI.numUnitHour ) AS NUMERIC(18, 2)) AS numUnitHour,
					isnull(OI.numClassID,0) as numClassID,isnull(OI.numProjectID,0) as numProjectID,
					CASE WHEN (SELECT COUNT(*) FROM OpportunityBizDocs OB JOIN dbo.OpportunityBizDocItems OBI ON ob.numOppBizDocsId=obi.numOppBizDocID
					AND (ob.bitAuthoritativeBizDocs=1 OR ob.numBizDocId=304) AND obi.numOppItemID=oi.numoppitemtCode AND ob.numOppId=OM.numOppId)>0 THEN 1 ELSE 0 END  AS bitIsAuthBizDoc,
					ISNULL(OI.numUnitHourReceived,0) AS numUnitHourReceived,ISNULL(numQtyShipped,0) AS numQtyShipped,ISNULL(dbo.fn_GetUOMName(I.numBaseUnit),'') AS vcBaseUOMName,ISNULL(I.fltWeight,0) AS [fltWeight],ISNULL(I.bitFreeShipping,0) AS [bitFreeShipping],
					ISNULL(I.fltLength,0) AS [fltLength],
					ISNULL(I.fltWidth,0) AS [fltWidth],
					ISNULL(I.fltHeight,0) AS [fltHeight],
					ISNULL(WItems.vcWHSKU,ISNULL(I.vcSKU,'')) AS [vcSKU],
					(CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(I.monAverageCost,0) END) AS monAverageCost,
					ISNULL(I.bitFreeShipping,0) AS [bitFreeShipping],
					ISNULL(I.txtItemDesc,'') txtItemDesc,
					ISNULL(I.numBarCodeId,'') numBarCodeId,
					ISNULL(I.bitSerialized,0) bitSerialized,
					ISNULL(I.bitKitParent,0) bitKitParent,
					ISNULL(I.bitAssembly,0) bitAssembly,
					ISNULL(I.IsArchieve,0) IsArchieve,
					ISNULL(I.bitLotNo,0) bitLotNo,
					ISNULL(WItems.monWListPrice,0) monWListPrice,
					ISNULL(I.bitTaxable, 0) bitTaxable,
					ISNULL(I.numItemClassification,0) AS numItemClassification,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numOnHand),0) AS VARCHAR(30)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numOnHand,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numOnOrder),0) AS VARCHAR(30)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numOnOrder,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numAllocation),0) AS VARCHAR(30)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numAllocation,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numBackOrder),0) AS VARCHAR(30)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numBackOrder,
					case when ISNULL(I.bitAssembly,0)=1 then dbo.fn_GetAssemblyPossibleWOQty(I.numItemCode,WItems.numWareHouseID) else 0 end AS numMaxWorkOrderQty,
					ISNULL((SELECT COUNT(*) FROM OppWarehouseSerializedItem WHERE numOppId=@numOppID AND numOppItemID=OI.numoppitemtCode),0) AS numSerialNoAssigned,
					(CASE WHEN (SELECT COUNT(*) FROM OpportunityBizDocs OB JOIN dbo.OpportunityBizDocItems OBI ON OB.numOppBizDocsId=OBI.numOppBizDocID WHERE OB.numOppId = OI.numOppId AND OBI.numOppItemID=OI.numoppitemtCode AND OB.numBizDocId=296) > 0 THEN 1 ELSE 0 END) AS bitAddedFulFillmentBizDoc,
					ISNULL(OI.vcAttrValues,'') AS AttributeIDs,
					ISNULL(I.numContainer,0) AS numContainer
					,ISNULL(I.numNoItemIntoContainer,0) AS numContainerQty
					,ISNULL(numPromotionID,0) AS numPromotionID,
					ISNULL(bitPromotionTriggered,0) AS bitPromotionTriggered,
					ISNULL(vcPromotionDetail,'') AS vcPromotionDetail,
					CASE WHEN ISNULL(OI.numSortOrder,0)=0 THEN row_number() OVER (ORDER BY OI.numOppId) ELSE ISNULL(OI.numSortOrder,0) END AS numSortOrder
					,ISNULL(vcNotes,'') AS vcVendorNotes
					,ISNULL(CPN.CustomerPartNo,'') CustomerPartNo
					,OI.ItemRequiredDate AS ItemRequiredDate
					,ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate) ItemReleaseDate
					,(CASE WHEN ISNULL(bitMarkupDiscount,0) = 0 THEN '0' ELSE '1' END) AS bitMarkupDiscount
					,ISNULL(vcChildKitSelectedItems,'') KitChildItems
					,(CASE 
						WHEN ISNULL(I.bitKitParent,0) = 1 
						THEN 
							(CASE 
								WHEN EXISTS (SELECT ID.numItemDetailID FROM ItemDetails ID INNER JOIN Item IInner ON ID.numChildItemID=IInner.numItemCode WHERE ID.numItemKitID=I.numItemCode AND ISNULL(IInner.bitKitParent,0)=1)
								THEN 1
								ELSE 0
							END)
						ELSE 0 
					END) bitHasKitAsChild
					,ISNULL(I.bitFreeShipping,0) IsFreeShipping
					,(CASE 
						WHEN ISNULL(I.bitKitParent,0)=1
								AND (CASE 
										WHEN EXISTS (SELECT ID.numItemDetailID FROM ItemDetails ID INNER JOIN Item IInner ON ID.numChildItemID=IInner.numItemCode WHERE ID.numItemKitID=I.numItemCode AND ISNULL(IInner.bitKitParent,0)=1)
										THEN 1
										ELSE 0
									END) = 0
						THEN 
							dbo.GetKitWeightBasedOnItemSelection(I.numItemCode,OI.vcChildKitSelectedItems)
						ELSE 
							ISNULL(I.fltWeight,0) 
					END) fltItemWeight
					,ISNULL(I.fltWidth,0) fltItemWidth
					,ISNULL(I.fltHeight,0) fltItemHeight
					,ISNULL(I.fltLength,0) fltItemLength
					,ISNULL(OI.numWOQty,0) numWOQty
					,ISNULL(I.bitSOWorkOrder,0) bitAlwaysCreateWO
            FROM    dbo.OpportunityItems OI
                    INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OI.numOppId
					INNER JOIN DivisionMaster DM ON OM.numDivisionId=DM.numDivisionID
                    INNER JOIN dbo.Item I ON I.numItemCode = OI.numItemCode
                    LEFT JOIN UOM U ON U.numUOMId = OI.numUOMId
                    LEFT JOIN WareHouseItems WItems ON WItems.numWareHouseItemID = OI.numWarehouseItmsID
                    LEFT JOIN Warehouses W ON W.numWarehouseID = WItems.numWarehouseID
					LEFT JOIN CustomerPartNumber CPN ON CPN.numItemCode = I.numItemCode AND CPN.numCompanyId=DM.numCompanyID
            WHERE   OI.numOppId = @numOppID
                    AND OM.numDomainId = @numDomainId
			ORDER BY OI.numSortOrder
                    
			SELECT  
				vcSerialNo,
				vcComments AS Comments,
				numOppItemID AS numoppitemtCode,
				W.numWarehouseItmsDTLID,
				numWarehouseItmsID AS numWItmsID,
				dbo.Fn_getattributes(W.numWarehouseItmsDTLID, 1) Attributes
			FROM    
				WareHouseItmsDTL W
			JOIN 
				OppWarehouseSerializedItem O 
			ON 
				O.numWarehouseItmsDTLID = W.numWareHouseItmsDTLID
			WHERE   
				numOppID = @numOppID         
        END
        
        
		IF @tintMode = 5 -- Get WebApi Order Items to enter WebApi Order line Item Details 
		BEGIN 
			SELECT OI.numOppItemtCode,OI.numOppId,OI.numItemCode,OI.vcNotes as txtNotes,ISNULL(OI.numCost,0) numCost,
			ISNULL((SELECT vcWHSKU FROM dbo.WareHouseItems WHERE WareHouseItems.numWarehouseItemID = OI.numWarehouseItmsID),ISNULL(IT.vcSKU,'')) AS [vcSKU],
			OI.vcItemDesc,OMAPI.vcAPIOppId
			FROM OpportunityItems OI 
			INNER JOIN dbo.OpportunityMaster OM ON OI.numOppId = OM.numOppId 
			INNER JOIN dbo.OpportunityMasterAPI OMAPI ON OM.numOppId = OMAPI.numOppId
			INNER JOIN dbo.Item IT ON OI.numItemCode = IT.numItemCode
			WHERE OI.numOppId = @numOppID and OM.numDomainID = @numDomainId 
        END

	IF @tintMode = 6 -- Add Edit Order items
        BEGIN 
            SELECT  OI.numoppitemtCode,
                    OI.numOppId,
					OI.vcNotes as txtNotes,
                    OI.numItemCode,
					OI.bitItemPriceApprovalRequired,
					OI.numUnitHour AS numOrigUnitHour,
                    (OI.numUnitHour-ISNULL((SELECT SUM(numUnitHour) FROM ReturnItems where 
					numReturnHeaderID IN (SELECT numReturnHeaderID FROM ReturnHeader 
					WHERE numOppId=@numOppID) AND numOppItemID=OI.numoppitemtCode),0))
					AS numUnitHour,
                    OI.monPrice,
                    OI.monTotAmount,
                    OI.numSourceID,
                    OI.vcItemDesc,
					OI.numCost,
                    OI.numWarehouseItmsID,
                    OI.vcType ItemType,
                    OI.vcAttributes Attributes,
                    OI.bitDropShip DropShip,
                    OI.numUnitHourReceived,
                    OI.bitDiscountType,
                    OI.fltDiscount,
                    OI.monTotAmtBefDiscount,
                    OI.numQtyShipped,
                    OI.vcItemName,
                    OI.vcModelID,
                    OI.vcPathForTImage,
                    OI.vcManufacturer,
                    OI.monVendorCost,
                    ISNULL(OI.numUOMId, 0) AS numUOM,
                    ISNULL(U.vcUnitName, '') vcUOMName,
                    ISNULL(dbo.fn_UOMConversion(OI.numUOMId, OI.numItemCode,
                                         OM.numDomainId, NULL),1) AS UOMConversionFactor,
                    OI.bitWorkOrder,
                    isnull(OI.numVendorWareHouse,0) as numVendorWareHouse,
                    OI.numShipmentMethod,
                    OI.numSOVendorId,
                    OI.numProjectID,
                    OI.numClassID,
                    OI.numProjectStageID,
                    OI.numToWarehouseItemID,
                   UPPER(I.charItemType) charItemType,
                    WItems.numWarehouseID,
                    W.vcWareHouse AS Warehouse,
                    0 as Op_Flag,
					CAST(( dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), OI.numItemCode,
                                    OM.numDomainId, ISNULL(OI.numUOMId, 0))
						* OI.numUnitHour ) AS NUMERIC(18, 2)) AS numUnitHour,
					isnull(OI.numClassID,0) as numClassID,isnull(OI.numProjectID,0) as numProjectID,
					CASE WHEN (SELECT COUNT(*) FROM OpportunityBizDocs OB JOIN dbo.OpportunityBizDocItems OBI ON ob.numOppBizDocsId=obi.numOppBizDocID
					AND (ob.bitAuthoritativeBizDocs=1 OR ob.numBizDocId=304) AND obi.numOppItemID=oi.numoppitemtCode AND ob.numOppId=OM.numOppId)>0 THEN 1 ELSE 0 END  AS bitIsAuthBizDoc,
					ISNULL(OI.numUnitHourReceived,0) AS numUnitHourReceived,ISNULL(numQtyShipped,0) AS numQtyShipped,ISNULL(dbo.fn_GetUOMName(I.numBaseUnit),'') AS vcBaseUOMName,ISNULL(I.fltWeight,0) AS [fltWeight],ISNULL(I.bitFreeShipping,0) AS [bitFreeShipping],
					ISNULL(I.fltLength,0) AS [fltLength],
					ISNULL(I.fltWidth,0) AS [fltWidth],
					ISNULL(I.fltHeight,0) AS [fltHeight],
					ISNULL(WItems.vcWHSKU,ISNULL(I.vcSKU,'')) AS [vcSKU],
					(CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(I.monAverageCost,0) END) AS monAverageCost,
					ISNULL(I.bitFreeShipping,0) AS [bitFreeShipping],
					ISNULL(I.txtItemDesc,'') txtItemDesc,
					ISNULL(I.vcModelID,'') vcModelID,
					ISNULL(I.vcManufacturer,'') vcManufacturer,
					ISNULL(I.numBarCodeId,'') numBarCodeId,
					ISNULL(I.bitSerialized,0) bitSerialized,
					ISNULL(I.bitKitParent,0) bitKitParent,
					ISNULL(I.bitAssembly,0) bitAssembly,
					ISNULL(I.IsArchieve,0) IsArchieve,
					ISNULL(I.bitLotNo,0) bitLotNo,
					ISNULL(WItems.monWListPrice,0) monWListPrice,
					ISNULL(I.bitTaxable, 0) bitTaxable,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numOnHand),0) AS VARCHAR(30)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numOnHand,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numOnOrder),0) AS VARCHAR(30)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numOnOrder,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numAllocation),0) AS VARCHAR(30)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numAllocation,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numBackOrder),0) AS VARCHAR(30)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numBackOrder,
					case when ISNULL(I.bitAssembly,0)=1 then dbo.fn_GetAssemblyPossibleWOQty(I.numItemCode,WItems.numWareHouseID) else 0 end AS numMaxWorkOrderQty,
					ISNULL((SELECT COUNT(*) FROM OppWarehouseSerializedItem WHERE numOppId=@numOppID AND numOppItemID=OI.numoppitemtCode),0) AS numSerialNoAssigned,
					(CASE WHEN (SELECT COUNT(*) FROM OpportunityBizDocs OB JOIN dbo.OpportunityBizDocItems OBI ON OB.numOppBizDocsId=OBI.numOppBizDocID WHERE OB.numOppId = OI.numOppId AND OBI.numOppItemID=OI.numoppitemtCode AND OB.numBizDocId=296) > 0 THEN 1 ELSE 0 END) AS bitAddedFulFillmentBizDoc,
					ISNULL(OI.vcAttrValues,'') AS AttributeIDs
					,(CASE WHEN OM.tintOppType=2 THEN ISNULL(vcNotes,'') ELSE '' END) AS vcVendorNotes
					,(CASE WHEN ISNULL(bitMarkupDiscount,0) = 0 THEN '0' ELSE '1' END) AS bitMarkupDiscount 
--					0 AS [Tax0],0 AS bitTaxable0
            FROM    dbo.OpportunityItems OI
                    INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OI.numOppId
                    INNER JOIN dbo.Item I ON I.numItemCode = OI.numItemCode
                    LEFT JOIN UOM U ON U.numUOMId = OI.numUOMId
                    LEFT JOIN WareHouseItems WItems ON WItems.numWareHouseItemID = OI.numWarehouseItmsID
                    LEFT JOIN Warehouses W ON W.numWarehouseID = WItems.numWarehouseID
            WHERE   OI.numOppId = @numOppID
                    AND OM.numDomainId = @numDomainId
					AND (OI.numUnitHour-ISNULL((SELECT SUM(numUnitHour) FROM ReturnItems where 
					numReturnHeaderID IN (SELECT numReturnHeaderID FROM ReturnHeader 
					WHERE numOppId=@numOppID) AND numOppItemID=OI.numoppitemtCode),0))>0
                    
		SELECT  vcSerialNo,
				vcComments AS Comments,
				numOppItemID AS numoppitemtCode,
				W.numWarehouseItmsDTLID,
				numWarehouseItmsID AS numWItmsID,
				dbo.Fn_getattributes(W.numWarehouseItmsDTLID, 1) Attributes
		FROM    WareHouseItmsDTL W
				JOIN OppWarehouseSerializedItem O ON O.numWarehouseItmsDTLID = W.numWareHouseItmsDTLID
		WHERE   numOppID = @numOppID AND numOppItemID IN (SELECT OI.numoppitemtCode FROM OpportunityItems OI WHERE OI.numOppId = @numOppID AND (OI.numUnitHour-ISNULL((SELECT SUM(numUnitHour) FROM ReturnItems where 
					numReturnHeaderID IN (SELECT numReturnHeaderID FROM ReturnHeader 
					WHERE numOppId=@numOppID) AND numOppItemID=OI.numoppitemtCode),0))>0)
					              
                   
END
END
GO
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetProjectExpenseReports')
DROP PROCEDURE USP_GetProjectExpenseReports
GO
CREATE PROCEDURE [dbo].[USP_GetProjectExpenseReports]
(
    @numDomainID NUMERIC(18,0),
    @numProId NUMERIC(18,0)
)
AS 
BEGIN
	DECLARE @numCost TINYINT
	DECLARE @numCurrencyID NUMERIC(18,0)
	DECLARE @varCurrSymbol VARCHAR(10)
	SELECT @numCost = ISNULL(numCost,1),@numCurrencyID=ISNULL(numCurrencyID,0) FROM Domain WHERE numDomainId=@numDomainID

	SELECT @varCurrSymbol = ISNULL(varCurrSymbol,'$') FROM Currency WHERE numCurrencyID = @numCurrencyID

	DECLARE @TEMP TABLE
	(
		vcItem TEXT
		,vcUnits TEXT
		,vcIncomeSource TEXT
		,vcExpenseSource TEXT
		,vcIncomeExpense TEXT
		,vcNetIncome TEXT
		,monIncome DECIMAL(20,5)
		,monExpense DECIMAL(20,5)
		,monNetIncome DECIMAL(20,5)
		,bitExpensePending BIT
	)

	-- Sales Orders
	INSERT INTO @TEMP
	(
		vcItem
		,vcUnits
		,vcIncomeSource
		,vcExpenseSource
		,monIncome
		,monExpense
		,bitExpensePending
	)
	SELECT 
		CONCAT(ISNULL(OI.vcItemName,I.vcItemName),' <i style="color:#afabab">(',(CASE 
																						WHEN I.charItemType='P' 
																						THEN CONCAT('Inventory',(CASE WHEN ISNULL(OI.bitDropship,0) = 1 THEN ' Drop ship' ELSE '' END))
																						WHEN I.charItemType='N' THEN 'Non-Inventory' 
																						WHEN I.charItemType='S' THEN 'Service' 
																						WHEN I.charItemType='A' THEN 'Accessory' 
																					END),')</i> ',OI.vcItemDesc)
		,CONCAT(dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0),I.numItemCode, @numDomainID,ISNULL(OI.numUOMId, 0)) * OI.numUnitHour,' (',ISNULL(UOM.vcUnitName,'-'),')')
		,CONCAT('<a href="../opportunity/frmOpportunities.aspx?OpID=',OM.numOppID,'" target="_blank">',vcPOppName,'</a>')
		,(CASE 
			WHEN I.charItemType='S' AND ISNULL(I.bitExpenseItem,0) = 1 
			THEN (CASE 
					WHEN BH.numBillID IS NULL 
					THEN '<span class="text-orange">Pending</span>' 
					ELSE CONCAT('<a href="javascript:void(0)" onclick="OpenBizInvoice(0,0,',BH.numBillID,',2);">',((CASE WHEN ISNULL(BH.bitLandedCost,0)=1 THEN 'Laned Cost-' ELSE 'Bill-' END) + CONVERT(VARCHAR(10),BH.numBillID)),'</a>') 
				END)
			ELSE (CASE @numCost 
					WHEN 3 THEN 'Primary Vendor Cost'
					WHEN 2 THEN 'Products & Services Cost' 
					ELSE (CASE WHEN I.charItemType='P' THEN 'Average Cost' ELSE 'Primary Vendor Cost' END)
				END)
		END)
		,OI.monTotAmount
		,(CASE 
			WHEN I.charItemType='S' AND ISNULL(I.bitExpenseItem,0) = 1 
			THEN (CASE 
					WHEN BH.numBillID IS NULL 
					THEN 0
					ELSE ISNULL(BD.monAmount,0)
				END)
			ELSE (CASE @numCost 
					WHEN 3 THEN ISNULL(OI.numUnitHour,0) * ISNULL(OI.monVendorCost,0)
					WHEN 2 THEN ISNULL(OI.numUnitHour,0) * ISNULL(OI.numCost,0)
					ELSE (CASE 
							WHEN I.charItemType='P' 
							THEN ((ISNULL(OI.numUnitHour,0) - ISNULL(TEMPFulfilledItems.numUnitHour,0)) * ISNULL(OI.monAvgCost,0)) + ISNULL(TEMPFulfilledItems.monCost,0)
							ELSE ISNULL(OI.numUnitHour,0) * ISNULL(OI.monVendorCost,0) 
						END)
				END)
		END)
		,(CASE 
			WHEN I.charItemType='S' AND ISNULL(I.bitExpenseItem,0) = 1 
			THEN (CASE 
					WHEN BH.numBillID IS NULL 
					THEN 1
					ELSE 0
				END)
			ELSE 0
		END)
	FROM 
		OpportunityMaster OM
	INNER JOIN
		OpportunityItems OI
	ON
		OM.numOppId = OI.numOppId
	INNER JOIN
		Item I
	ON
		OI.numItemCode = I.numItemCode
	LEFT JOIN
		UOM
	ON
		OI.numUOMId = UOM.numUOMId
	LEFT JOIN
		BillDetails BD
	ON
		OI.numoppitemtCode = BD.numOppItemID
	LEFT JOIN
		BillHeader BH
	ON
		BD.numBillID = BH.numBillID
	OUTER APPLY
	(
		SELECT 
			SUM(numUnitHour) numUnitHour
			,SUM(ISNULL(OBDI.numUnitHour,0) * ISNULL(OBDI.monAverageCost,0)) monCost
		FROM 
			OpportunityBizDocs OBD
		INNER JOIN
			OpportunityBizDocItems OBDI
		ON
			OBD.numOppBizDocsId = OBDI.numOppBizDocID
		WHERE
			OBD.numOppId = OM.numOppID
			AND OBD.numBizDocId = 296
			AND OBDI.numOppItemID = OI.numoppitemtCode
	) TEMPFulfilledItems
	WHERE
		OM.numDomainId=@numDomainID
		AND OM.tintOppType = 1
		AND OM.tintOppStatus = 1
		AND ISNULL(OI.numProjectID,OM.numProjectID) = @numProId
		

	-- Bills
	INSERT INTO @TEMP
	(
		vcItem
		,vcUnits
		,vcIncomeSource
		,vcExpenseSource
		,monIncome
		,monExpense
		,bitExpensePending
	)
	SELECT
		ISNULL(BD.vcDescription,'')
		,''
		,''
		,(CASE WHEN(BD.numBillID>0) THEN  Case When isnull(BH.bitLandedCost,0)=1 THEN 'Laned Cost-' ELSE 'Bill-' END + CONVERT(VARCHAR(10),BH.numBillID) END)
		,0
		,ISNULL(monAmount,0)
		,0
	FROM	
		BillHeader BH
	INNER JOIN
		BillDetails BD
	ON
		BH.numBillID = BD.numBillID
	WHERE
		BH.numDomainID = @numDomainID
		AND BD.numProjectID = @numProId
		AND ISNULL(BD.numOppItemID,0) = 0

	UPDATE
		@TEMP
	SET 
		vcNetIncome = (CASE 
						WHEN ISNULL(bitExpensePending,0) = 1 THEN CONCAT('<span class="text-orange">',@varCurrSymbol,' ',FORMAT(ISNULL(monIncome,0) - ISNULL(monExpense,0), '#,##0.00###'),'</span>')
						WHEN ISNULL(monIncome,0) - ISNULL(monExpense,0) >= 0 
						THEN CONCAT('<span class="text-green">',@varCurrSymbol,' ',FORMAT(ISNULL(monIncome,0) - ISNULL(monExpense,0), '#,##0.00###'),'</span>') 
						ELSE CONCAT('<span class="text-red">',@varCurrSymbol,' ',FORMAT(ISNULL(monIncome,0) - ISNULL(monExpense,0), '#,##0.00###'),'</span>') 
					END)
		,monNetIncome = ISNULL(monIncome,0) - ISNULL(monExpense,0)
		,vcIncomeExpense = CONCAT(FORMAT(ISNULL(monIncome,0), '#,##0.00###'),(CASE WHEN ISNULL(bitExpensePending,0) = 1 THEN '' ELSE CONCAT(' (',FORMAT(ISNULL(monExpense,0), '#,##0.00###'),')') END))
			
	SELECT * FROM @TEMP

	;WITH CTE AS 
	(SELECT
		ISNULL((SELECT SUM(monIncome) FROM @TEMP),0) AS monIncomeAR
		,ISNULL((SELECT SUM(monExpense) FROM @TEMP),0) AS monNonLaborExpense
		,ISNULL((SELECT SUM(monNetIncome) FROM @TEMP),0) AS monTotalIncome
		,0 AS monExpenseAP
		,ISNULL((SELECT 
					SUM(dbo.GetTimeSpendOnTaskInMinutes(@numDomainID,SPDT.numTaskId) * (ISNULL(SPDT.monHourlyRate,UM.monHourlyRate)/60))
				FROM
					Sales_process_List_Master SPLM
				INNER JOIN
					StagePercentageDetails SPD
				ON
					SPLM.Slp_Id = SPD.slp_id
				INNER JOIN
					StagePercentageDetailsTask SPDT
				ON
					SPD.numStageDetailsId = SPDT.numStageDetailsId
				INNER JOIN
					UserMaster UM
				ON
					SPDT.numAssignTo = UM.numUserDetailId
				WHERE
					SPLM.numdomainid=@numDomainID
					AND SPLM.numProjectId = @numProID
					AND EXISTS (SELECT SPDTTL.ID FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID = SPDT.numTaskId AND tintAction=4)),0) AS monLaborExpense
		,ISNULL((SELECT SUM(monIncome) FROM @TEMP WHERE ISNULL(bitExpensePending,0) = 1),0) AS monIncomePendingExpense)

	SELECT *,ISNULL(monTotalIncome,0) - ISNULL(monLaborExpense,0) AS monGrossProfit FROM CTE
		


--IF @tintMode=0 --Use Order Amounts
--BEGIN
--            SELECT 1 as iTEType,'Sales Order' AS vcTimeExpenseType,I.[vcItemName],
--                    CASE numType
--                      WHEN 1 THEN ISNULL(OPP.numUnitHour, 0)
--                      ELSE ISNULL(CONVERT(VARCHAR(100), CONVERT(DECIMAL(10, 1), ( CONVERT(DECIMAL(10, 1), DATEDIFF(minute, TE.dtFromDate, TE.dtToDate)) / 60 ))),
--                                  0)
--                    END AS [numUnitHour],
--                    ISNULL(OPP.[monPrice], 0) monPrice,
--					ISNULL(OPP.[monPrice], 0) * CASE numType
--                      WHEN 1 THEN ISNULL(OPP.numUnitHour, 0)
--                      ELSE ISNULL(CONVERT(VARCHAR(100), CONVERT(DECIMAL(10, 1), ( CONVERT(DECIMAL(10, 1), DATEDIFF(minute, TE.dtFromDate, TE.dtToDate)) / 60 ))),
--                                  0)
--                    END - (CASE WHEN I.charItemType='P' THEN CASE WHEN ISNULL(Opp.monAvgCost,0)=0 THEN ISNULL(Opp.monVendorCost,0) ELSE ISNULL(Opp.monAvgCost,0) END ELSE 0 END)  AS ExpenseAmount,
--                    dbo.[FormatedDateFromDate](TE.[dtTCreatedOn], @numDomainID) AS dtDateEntered,
--                    dbo.fn_GetContactName(TE.numTCreatedBy) AS vcCreatedBy,TE.numOppId,1 as numOppType,
--					TE.[numCategoryHDRID],isnull(OPP.numProjectStageID,0) as numProjectStageID,0 as numBillId,
--					Case When OPP.numProjectStageID>0 then dbo.fn_GetProjectStageName(OPP.numProjectId,OPP.numProjectStageID) when OPP.numProjectStageID=-1 then 'T&E' else 'S.O.' end as vcFrom,
--					OPP.numProjectId as numProId,TE.[dtTCreatedOn] as dtCreatedOn,isnull((SELECT SUBSTRING((SELECT ',' + Cast(OB.numOppBizDocsId as varchar(18)) from  [OpportunityBizDocs] OB 
--					INNER JOIN [OpportunityBizDocItems] OBI ON OBI.numOppBizDocID = OB.numOppBizDocsId AND OPP.numoppitemtCode = OBI.numOppItemID 
--					WHERE OB.bitAuthoritativeBizDocs=1 and OB.numOppId = OM.numOppId FOR XML PATH('')),2,200000)),'') AS vcBizDoc
--					,0 AS numDepositId,0 AS numCheckHeaderID,0 AS numJournalId,ISNULL(Opp.monAvgCost,0) AS monAvgCost,ISNULL(Opp.monVendorCost,0) AS monVendorCost
--            FROM    [TimeAndExpense] TE
--                    INNER JOIN OpportunityMaster OM ON TE.numOppId = OM.numOppId
--                    INNER JOIN OpportunityItems OPP ON OPP.numOppId = TE.numOppId
--							AND OPP.numoppitemtCode = TE.numOppItemID AND OPP.numProjectId=TE.numProId
--					INNER JOIN Item I ON OPP.[numItemCode] = I.[numItemCode]
--            WHERE   [numProId] = @numProId
--                    AND TE.[numDomainID] = @numDomainID
--                    AND numType =1 --Billable 
--					AND OM.[tintOppType] = 1

--			UNION ALL
	
--			SELECT 1 as iTEType,'Sales Order' AS vcTimeExpenseType,I.[vcItemName],
--                    ISNULL(OPP.numUnitHour, 0) AS [numUnitHour],
--                    ISNULL(OPP.[monPrice], 0) monPrice,
--					ISNULL(OPP.[monPrice], 0) * ISNULL(OPP.numUnitHour, 0) - (CASE WHEN I.charItemType='P' THEN CASE WHEN ISNULL(Opp.monAvgCost,0)=0 THEN ISNULL(Opp.monVendorCost,0) ELSE ISNULL(Opp.monAvgCost,0) END ELSE 0 END)  AS ExpenseAmount,
--                    dbo.[FormatedDateFromDate](OM.bintCreatedDate, @numDomainID) AS dtDateEntered,
--                    dbo.fn_GetContactName(OM.numCreatedBy) AS vcCreatedBy,OM.numOppId,1 as numOppType,
--					0 as numCategoryHDRID,isnull(OPP.numProjectStageID,0) as numProjectStageID,0 as numBillId,
--					Case When OPP.numProjectStageID>0 then dbo.fn_GetProjectStageName(OPP.numProjectId,OPP.numProjectStageID) when OPP.numProjectStageID=-1 then 'T&E' else 'S.O.' end as vcFrom,
--					OPP.numProjectId as numProId,OM.bintCreatedDate as dtCreatedOn,isnull((SELECT SUBSTRING((SELECT ',' + Cast(OB.numOppBizDocsId as varchar(18)) from  [OpportunityBizDocs] OB 
--					INNER JOIN [OpportunityBizDocItems] OBI ON OBI.numOppBizDocID = OB.numOppBizDocsId AND OPP.numoppitemtCode = OBI.numOppItemID 
--					WHERE OB.bitAuthoritativeBizDocs=1 and OB.numOppId = OM.numOppId FOR XML PATH('')),2,200000)),'') AS vcBizDoc,0 AS numDepositId,0 AS numCheckHeaderID,0 AS numJournalId
--					,ISNULL(Opp.monAvgCost,0) AS monAvgCost,ISNULL(Opp.monVendorCost,0) AS monVendorCost
--            FROM    OpportunityMaster OM INNER JOIN OpportunityItems OPP ON OPP.numOppId = OM.numOppId
--					INNER JOIN Item I ON OPP.[numItemCode] = I.[numItemCode]
--            WHERE  OM.[numDomainID] = @numDomainID AND OPP.numProjectID = @numProId
--                    AND OPP.numProjectStageID=0 AND OM.[tintOppType] = 1

--			UNION ALL
			
--			SELECT  2 as iTEType,'Non-Billable Time' AS vcTimeExpenseType,
--                    ISNULL(CONVERT(VARCHAR(25),txtDesc) + '..' ,'') AS [vcItemName],
--                    ISNULL(CONVERT(VARCHAR(100), CONVERT(DECIMAL(10, 1), ( CONVERT(DECIMAL(10, 1), DATEDIFF(minute, TE.dtFromDate, TE.dtToDate)) / 60 ))),0)
--                    AS [numUnitHour],
--                    0 monPrice,
--					0 AS ExpenseAmount,
--                    dbo.[FormatedDateFromDate](TE.[dtTCreatedOn], @numDomainID) AS dtDateEntered,
--                    dbo.fn_GetContactName(TE.numTCreatedBy) AS vcCreatedBy,0 as numOppId,0 as numOppType,
--					TE.[numCategoryHDRID],isnull(TE.numStageId,0) as numProjectStageID,0 as numBillId,
--					Case When TE.numStageId>0 then dbo.fn_GetProjectStageName(TE.numProId,TE.numStageId) else 'Time' end as vcFrom,
--					TE.numProId,TE.[dtTCreatedOn] as dtCreatedOn,'' as vcBizDoc,0 AS numDepositId,0 AS numCheckHeaderID,0 AS numJournalId
--					,0 AS monAvgCost,0 AS monVendorCost
--            FROM    [TimeAndExpense] TE
--            WHERE   [numProId] = @numProId
--                    AND TE.[numDomainID] = @numDomainID
--                    AND numType =2 --Non Billable

--		    UNION ALL

--            select 3 as iTEType,'Purchase Order' AS vcTimeExpenseType,I.[vcItemName],
--                    ISNULL(OPP.numUnitHour, 0) as numUnitHour,
--                    ISNULL(OPP.[monPrice], 0) monPrice,
--	   			    (OPP.[monPrice] * OPP.[numUnitHour]) AS ExpenseAmount,
--					dbo.[FormatedDateFromDate](OM.bintCreatedDate, @numDomainID) AS dtDateEntered,
--                    dbo.fn_GetContactName(OM.numCreatedBy) AS vcCreatedBy,OM.numOppId,2 as numOppType,
--					0 as numCategoryHDRID,isnull(OPP.numProjectStageID,0) as numProjectStageID,0 as numBillId,
--					Case When OPP.numProjectStageID>0 then dbo.fn_GetProjectStageName(OPP.numProjectId,OPP.numProjectStageID) else 'P.O.' end as vcFrom,
--					OPP.numProjectID as numProId,OM.bintCreatedDate as dtCreatedOn,isnull((SELECT SUBSTRING((SELECT ',' + Cast(OB.numOppBizDocsId as varchar(18)) from  [OpportunityBizDocs] OB 
--					INNER JOIN [OpportunityBizDocItems] OBI ON OBI.numOppBizDocID = OB.numOppBizDocsId AND OPP.numoppitemtCode = OBI.numOppItemID 
--					WHERE OB.bitAuthoritativeBizDocs=1 and OB.numOppId = OM.numOppId FOR XML PATH('')),2,200000)),'') AS vcBizDoc,0 AS numDepositId,0 AS numCheckHeaderID,0 AS numJournalId
--					,ISNULL(Opp.monAvgCost,0) AS monAvgCost,ISNULL(Opp.monVendorCost,0) AS monVendorCost
--			 from  [OpportunityMaster] OM 
--						JOIN [OpportunityItems] OPP ON OM.[numOppId] = OPP.[numOppId]
--                        LEFT OUTER JOIN [Item] I ON OPP.[numItemCode] = I.[numItemCode]
--						LEFT OUTER JOIN [Chart_Of_Accounts] COA ON I.[numCOGsChartAcntId] = COA.[numAccountId]
--			 where  OM.numDomainId=@numDomainID  
--					AND OPP.numProjectID = @numProId
--					AND OM.[tintOppType] = 2	

--			 Union ALL

--			select 4 as iTEType,'Bill' as vcTimeExpenseType,BD.vcDescription as vcItemName,0 as numUnitHour,0 as monPrice,BD.monAmount AS ExpenseAmount,
--				dbo.[FormatedDateFromDate](BH.dtCreatedDate, @numDomainID) AS dtDateEntered,
--                    dbo.fn_GetContactName(BH.numCreatedBy) AS vcCreatedBy,0 as numOppId,0 as numOppType,
--					0 as numCategoryHDRID,isnull(PO.numStageID,0) as numProjectStageID,PO.numBillId,
--					Case When PO.numStageID>0 then dbo.fn_GetProjectStageName(PO.numProID,PO.numStageID) else 'Bill' end as vcFrom,
--					PO.numProID,BH.dtCreatedDate as dtCreatedOn,'' as vcBizDoc,0 AS numDepositId,0 AS numCheckHeaderID,0 AS numJournalId
--					,0 AS monAvgCost,0 AS monVendorCost
--			 from  ProjectsOpportunities PO 
--				   JOIN BillHeader BH ON BH.numBillID = PO.numBillID
--				   JOIN BillDetails BD ON BH.numBillID=BD.numBillID AND PO.numProID=BD.numProjectID
--			 where  PO.numDomainId=@numDomainID  
--					AND PO.numProID = @numProId
--					and isnull(PO.numBillId,0)>0
					
--			Union ALL

--			select 4 as iTEType,'Bill' as vcTimeExpenseType,BD.vcDescription as vcItemName,0 as numUnitHour,0 as monPrice,BD.monAmount AS ExpenseAmount,
--				dbo.[FormatedDateFromDate](BH.dtCreatedDate, @numDomainID) AS dtDateEntered,
--                    dbo.fn_GetContactName(BH.numCreatedBy) AS vcCreatedBy,0 as numOppId,0 as numOppType,
--					0 as numCategoryHDRID,0 as numProjectStageID,BD.numBillId,
--					'Bill' as vcFrom,
--					BD.numProjectID,BH.dtCreatedDate as dtCreatedOn,'' as vcBizDoc,0 AS numDepositId,0 AS numCheckHeaderID,0 AS numJournalId
--					,0 AS monAvgCost,0 AS monVendorCost
--			 from  BillHeader BH 
--				   JOIN BillDetails BD ON BH.numBillID=BD.numBillID 
--			 where  BH.numDomainId=@numDomainID  
--					AND BD.numProjectID = @numProId
							
			
--			UNION ALL

--			SELECT  5 as iTEType,'Employee Expense' AS vcTimeExpenseType,
--                    ISNULL(CONVERT(VARCHAR(25),txtDesc) + '..' ,'') AS [vcItemName],
--                    ISNULL(CONVERT(VARCHAR(100), CONVERT(DECIMAL(10, 1), ( CONVERT(DECIMAL(10, 1), DATEDIFF(minute, TE.dtFromDate, TE.dtToDate)) / 60 ))),0)
--                    AS [numUnitHour],
--                    isnull(UM.monHourlyRate,0) monPrice,
--					isnull(Cast(datediff(minute,TE.dtfromdate,dttodate) as float)/Cast(60 as float),0) * isnull(UM.monHourlyRate,0) AS ExpenseAmount,
--                    dbo.[FormatedDateFromDate](TE.[dtTCreatedOn], @numDomainID) AS dtDateEntered,
--                    dbo.fn_GetContactName(TE.numTCreatedBy) AS vcCreatedBy,0 as numOppId,0 as numOppType,
--					TE.[numCategoryHDRID],isnull(TE.numStageId,0) as numProjectStageID,0 as numBillId,
--					Case When TE.numStageId>0 then dbo.fn_GetProjectStageName(TE.numProId,TE.numStageId) else 'Expense' end as vcFrom,
--					TE.numProId,TE.[dtTCreatedOn] as dtCreatedOn,'' as vcBizDoc,0 AS numDepositId,0 AS numCheckHeaderID,0 AS numJournalId
--					,0 AS monAvgCost,0 AS monVendorCost
--            FROM    [TimeAndExpense] TE join UserMaster UM on UM.numUserDetailId=TE.numUserCntID
--            WHERE   [numProId] = @numProId
--                    AND TE.[numDomainID] = @numDomainID
--                    AND numType in (1,2) --Non Billable & Billable
--					AND TE.numUserCntID>0
----					AND bitReimburse=1 and  numCategory=2  

--			UNION ALL
			
--			select 6 as iTEType,'Deposit' as vcTimeExpenseType,DED.vcMemo as vcItemName,0 as numUnitHour,0 as monPrice,DED.monAmountPaid AS ExpenseAmount,
--				dbo.[FormatedDateFromDate](DEM.dtCreationDate, @numDomainID) AS dtDateEntered,
--                    dbo.fn_GetContactName(DEM.numCreatedBy) AS vcCreatedBy,0 as numOppId,0 as numOppType,
--					0 as numCategoryHDRID,0 as numProjectStageID,0 AS numBillId,
--					'' as vcFrom,
--					DED.numProjectID AS numProID,DEM.dtCreationDate as dtCreatedOn,'' as vcBizDoc,DEM.numDepositId,0 AS numCheckHeaderID,0 AS numJournalId
--					,0 AS monAvgCost,0 AS monVendorCost
--			 from  DepositMaster DEM JOIN DepositeDetails DED ON DEM.numDepositID=DED.numDepositID
--			 WHERE DEM.tintDepositePage=1 AND DEM.numDomainId=@numDomainID  
--					AND DED.numProjectID = @numProId
			
--			UNION ALL
					
--			select 7 as iTEType,'Check' as vcTimeExpenseType,CD.vcDescription as vcItemName,0 as numUnitHour,0 as monPrice,CD.monAmount AS ExpenseAmount,
--				dbo.[FormatedDateFromDate](CH.dtCreatedDate, @numDomainID) AS dtDateEntered,
--                    dbo.fn_GetContactName(CH.numCreatedBy) AS vcCreatedBy,0 as numOppId,0 as numOppType,
--					0 as numCategoryHDRID,0 as numProjectStageID,0 AS numBillId,
--					'' as vcFrom,
--					CD.numProjectID AS numProID,CH.dtCreatedDate as dtCreatedOn,'' as vcBizDoc,0 AS numDepositId,CH.numCheckHeaderID,0 AS numJournalId
--					,0 AS monAvgCost,0 AS monVendorCost
--			 from  CheckHeader CH JOIN CheckDetails CD ON CH.numCheckHeaderID=CD.numCheckHeaderID
--			 WHERE CH.tintReferenceType=1 AND CH.numDomainId=@numDomainID  
--					AND CD.numProjectID = @numProId		
			
--			UNION ALL
			
--			select 9 as iTEType,'Journal' as vcTimeExpenseType,GD.varDescription as vcItemName,0 as numUnitHour,0 as monPrice,GD.numDebitAmt AS ExpenseAmount,
--				dbo.[FormatedDateFromDate](GH.datCreatedDate, @numDomainID) AS dtDateEntered,
--                    dbo.fn_GetContactName(GH.numCreatedBy) AS vcCreatedBy,0 as numOppId,0 as numOppType,
--					0 as numCategoryHDRID,0 as numProjectStageID,0 AS numBillId,
--					'' as vcFrom,
--					GD.numProjectID AS numProID,GH.datCreatedDate as dtCreatedOn,'' as vcBizDoc,0 AS numDepositId,0 AS numCheckHeaderID,GD.numJournalId
--					,0 AS monAvgCost,0 AS monVendorCost
--			 from  dbo.General_Journal_Header GH JOIN dbo.General_Journal_Details GD ON GH.numJournal_Id = GD.numJournalId
--					JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = GD.numChartAcntId
--			 WHERE ISNULL(GH.numCheckHeaderID,0)=0 AND ISNULL(GH.numBillPaymentID,0)=0 AND ISNULL(GH.numBillID,0)=0
--					AND ISNULL(GH.numDepositId,0)=0 AND ISNULL(GH.numOppId,0)=0 AND ISNULL(GH.numOppBizDocsId,0)=0
--					AND ISNULL(GH.numReturnId,0)=0
--					AND GH .numDomainId=@numDomainID  AND GD.numProjectID = @numProId
--					AND ISNULL(GD.numDebitAmt,0)>0	AND (COA.vcAccountCode LIKE '0103%' OR  COA.vcAccountCode LIKE '0104%' OR  COA.vcAccountCode LIKE '0106%')	
				
--			UNION ALL
					
--			 select 8 as iTEType,'Journal' as vcTimeExpenseType,GD.varDescription as vcItemName,0 as numUnitHour,0 as monPrice,GD.numCreditAmt AS ExpenseAmount,
--				dbo.[FormatedDateFromDate](GH.datCreatedDate, @numDomainID) AS dtDateEntered,
--                    dbo.fn_GetContactName(GH.numCreatedBy) AS vcCreatedBy,0 as numOppId,0 as numOppType,
--					0 as numCategoryHDRID,0 as numProjectStageID,0 AS numBillId,
--					'' as vcFrom,
--					GD.numProjectID AS numProID,GH.datCreatedDate as dtCreatedOn,'' as vcBizDoc,0 AS numDepositId,0 AS numCheckHeaderID,GD.numJournalId
--					,0 AS monAvgCost,0 AS monVendorCost
--			 from  dbo.General_Journal_Header GH JOIN dbo.General_Journal_Details GD ON GH.numJournal_Id = GD.numJournalId
--					JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = GD.numChartAcntId
--			 WHERE ISNULL(GH.numCheckHeaderID,0)=0 AND ISNULL(GH.numBillPaymentID,0)=0 AND ISNULL(GH.numBillID,0)=0
--					AND ISNULL(GH.numDepositId,0)=0 AND ISNULL(GH.numOppId,0)=0 AND ISNULL(GH.numOppBizDocsId,0)=0
--					AND ISNULL(GH.numReturnId,0)=0
--					AND GH .numDomainId=@numDomainID  AND GD.numProjectID = @numProId
--					AND ISNULL(GD.numCreditAmt,0)>0  AND (COA.vcAccountCode LIKE '0103%' OR  COA.vcAccountCode LIKE '0104%' OR  COA.vcAccountCode LIKE '0106%')
					
								
--			order by dtCreatedOn
-- END 

--ELSE IF @tintMode=1 --Use Auth BizDoc Amounts
--BEGIN
--            SELECT 1 as iTEType,'Sales Order' AS vcTimeExpenseType,I.[vcItemName],
--                    ISNULL(X.[numUnitHour], 0) AS [numUnitHour],
--                    ISNULL(OPP.[monPrice], 0) monPrice,
--					ISNULL(OPP.[monPrice], 0) * ISNULL(X.numUnitHour, 0) - (CASE WHEN I.charItemType='P' THEN CASE WHEN ISNULL(Opp.monAvgCost,0)=0 THEN ISNULL(Opp.monVendorCost,0) ELSE ISNULL(Opp.monAvgCost,0) END ELSE 0 END) AS ExpenseAmount,
--                    dbo.[FormatedDateFromDate](TE.[dtTCreatedOn], @numDomainID) AS dtDateEntered,
--                    dbo.fn_GetContactName(TE.numTCreatedBy) AS vcCreatedBy,TE.numOppId,1 as numOppType,
--					TE.[numCategoryHDRID],isnull(OPP.numProjectStageID,0) as numProjectStageID,0 as numBillId,
--					Case When OPP.numProjectStageID>0 then dbo.fn_GetProjectStageName(OPP.numProjectId,OPP.numProjectStageID) when OPP.numProjectStageID=-1 then 'T&E' else 'S.O.' end as vcFrom,
--					OPP.numProjectId as numProId,TE.[dtTCreatedOn] as dtCreatedOn,isnull((SELECT SUBSTRING((SELECT ',' + Cast(OB.numOppBizDocsId as varchar(18)) from  [OpportunityBizDocs] OB 
--					INNER JOIN [OpportunityBizDocItems] OBI ON OBI.numOppBizDocID = OB.numOppBizDocsId AND OPP.numoppitemtCode = OBI.numOppItemID 
--					WHERE OB.bitAuthoritativeBizDocs=1 and OB.numOppId = OM.numOppId FOR XML PATH('')),2,200000)),'') AS vcBizDoc,0 AS numDepositId,0 AS numCheckHeaderID,0 AS numJournalId
--					,ISNULL(Opp.monAvgCost,0) AS monAvgCost,ISNULL(Opp.monVendorCost,0) AS monVendorCost
--            FROM    [TimeAndExpense] TE
--                    INNER JOIN OpportunityMaster OM ON TE.numOppId = OM.numOppId
--                    INNER JOIN OpportunityItems OPP ON OPP.numOppId = TE.numOppId
--							AND OPP.numoppitemtCode = TE.numOppItemID AND OPP.numProjectId=TE.numProId
--					INNER JOIN Item I ON OPP.[numItemCode] = I.[numItemCode]
--					Inner Join (select sum(ISNULL(OBI.[numUnitHour], 0)) as numUnitHour,OM.numOppId,OPP.numoppitemtCode from  [TimeAndExpense] TE
--                    INNER JOIN OpportunityMaster OM ON TE.numOppId = OM.numOppId
--                    INNER JOIN OpportunityItems OPP ON OPP.numOppId = TE.numOppId
--							AND OPP.numoppitemtCode = TE.numOppItemID AND OPP.numProjectId=TE.numProId
--					INNER JOIN [OpportunityBizDocs] OB ON OB.numOppId = OM.numOppId
--					INNER JOIN [OpportunityBizDocItems] OBI ON OBI.numOppBizDocID = OB.numOppBizDocsId 
--						AND OPP.numoppitemtCode = OBI.numOppItemID 
--					WHERE [numProId] = @numProId AND TE.[numDomainID] = @numDomainID AND numType =1 and OB.bitAuthoritativeBizDocs=1
--						group by OM.numOppId,OPP.numoppitemtCode) X on
--					X.numOppId = OM.numOppId and OPP.numoppitemtCode = X.numoppitemtCode
--            WHERE   [numProId] = @numProId
--                    AND TE.[numDomainID] = @numDomainID
--                    AND numType =1 --Billable 
--					AND OM.[tintOppType] = 1

--			UNION ALL
			
--			SELECT 1 as iTEType,'Sales Order' AS vcTimeExpenseType,I.[vcItemName],
--                    ISNULL(X.numUnitHour, 0) AS [numUnitHour],
--                    ISNULL(OPP.[monPrice], 0) monPrice,
--					ISNULL(OPP.[monPrice], 0) * ISNULL(X.numUnitHour, 0) - (CASE WHEN I.charItemType='P' THEN CASE WHEN ISNULL(Opp.monAvgCost,0)=0 THEN ISNULL(Opp.monVendorCost,0) ELSE ISNULL(Opp.monAvgCost,0) END ELSE 0 END)  AS ExpenseAmount,
--                    dbo.[FormatedDateFromDate](OM.bintCreatedDate, @numDomainID) AS dtDateEntered,
--                    dbo.fn_GetContactName(OM.numCreatedBy) AS vcCreatedBy,OM.numOppId,1 as numOppType,
--					0 as numCategoryHDRID,isnull(OPP.numProjectStageID,0) as numProjectStageID,0 as numBillId,
--					Case When OPP.numProjectStageID>0 then dbo.fn_GetProjectStageName(OPP.numProjectId,OPP.numProjectStageID) when OPP.numProjectStageID=-1 then 'T&E' else 'S.O.' end as vcFrom,
--					OPP.numProjectId as numProId,OM.bintCreatedDate as dtCreatedOn,isnull((SELECT SUBSTRING((SELECT ',' + Cast(OB.numOppBizDocsId as varchar(18)) from  [OpportunityBizDocs] OB 
--					INNER JOIN [OpportunityBizDocItems] OBI ON OBI.numOppBizDocID = OB.numOppBizDocsId AND OPP.numoppitemtCode = OBI.numOppItemID 
--					WHERE OB.bitAuthoritativeBizDocs=1 and OB.numOppId = OM.numOppId FOR XML PATH('')),2,200000)),'') AS vcBizDoc,0 AS numDepositId,0 AS numCheckHeaderID,0 AS numJournalId
--					,ISNULL(Opp.monAvgCost,0) AS monAvgCost,ISNULL(Opp.monVendorCost,0) AS monVendorCost
--            FROM    OpportunityMaster OM INNER JOIN OpportunityItems OPP ON OPP.numOppId = OM.numOppId
--					INNER JOIN Item I ON OPP.[numItemCode] = I.[numItemCode]
--					Inner Join (select sum(ISNULL(OBI.[numUnitHour], 0)) as numUnitHour,OM.numOppId,OPP.numoppitemtCode from  
--                    OpportunityMaster OM INNER JOIN OpportunityItems OPP ON OPP.numOppId = OM.numOppId
--					INNER JOIN [OpportunityBizDocs] OB ON OB.numOppId = OM.numOppId
--					INNER JOIN [OpportunityBizDocItems] OBI ON OBI.numOppBizDocID = OB.numOppBizDocsId 
--						AND OPP.numoppitemtCode = OBI.numOppItemID 
--					WHERE OM.[numDomainID] = @numDomainID AND OPP.numProjectID = @numProId AND OPP.numProjectStageID=0 and OB.bitAuthoritativeBizDocs=1
--						group by OM.numOppId,OPP.numoppitemtCode) X on
--					X.numOppId = OM.numOppId and OPP.numoppitemtCode = X.numoppitemtCode
--            WHERE  OM.[numDomainID] = @numDomainID AND OPP.numProjectID = @numProId
--                    AND OPP.numProjectStageID=0 AND OM.[tintOppType] = 1

--			Union ALL

--			SELECT  2 as iTEType,'Non-Billable Time' AS vcTimeExpenseType,
--                    ISNULL(CONVERT(VARCHAR(25),txtDesc) + '..' ,'') AS [vcItemName],
--                    ISNULL(CONVERT(VARCHAR(100), CONVERT(DECIMAL(10, 1), ( CONVERT(DECIMAL(10, 1), DATEDIFF(minute, TE.dtFromDate, TE.dtToDate)) / 60 ))),0)
--                    AS [numUnitHour],
--                    0 monPrice,
--					0 AS ExpenseAmount,
--                    dbo.[FormatedDateFromDate](TE.[dtTCreatedOn], @numDomainID) AS dtDateEntered,
--                    dbo.fn_GetContactName(TE.numTCreatedBy) AS vcCreatedBy,0 as numOppId,0 as numOppType,
--					TE.[numCategoryHDRID],isnull(TE.numStageId,0) as numProjectStageID,0 as numBillId,
--					Case When TE.numStageId>0 then dbo.fn_GetProjectStageName(TE.numProId,TE.numStageId) else 'Time' end as vcFrom,
--					TE.numProId,TE.[dtTCreatedOn] as dtCreatedOn,'' as vcBizDoc,0 AS numDepositId,0 AS numCheckHeaderID,0 AS numJournalId
--					,0 AS monAvgCost,0 AS monVendorCost
--            FROM    [TimeAndExpense] TE
--            WHERE   [numProId] = @numProId
--                    AND TE.[numDomainID] = @numDomainID
--                    AND numType =2 --Non Billable

--		    UNION ALL

--            select 3 as iTEType,'Purchase Order' AS vcTimeExpenseType,I.[vcItemName],
--                    ISNULL(X.[numUnitHour], 0) as numUnitHour,
--                    ISNULL(OPP.[monPrice], 0) monPrice,
--	   			    (OPP.[monPrice] * X.[numUnitHour]) AS ExpenseAmount,
--					dbo.[FormatedDateFromDate](OM.bintCreatedDate, @numDomainID) AS dtDateEntered,
--                    dbo.fn_GetContactName(OM.numCreatedBy) AS vcCreatedBy,OM.numOppId,2 as numOppType,
--					0 as numCategoryHDRID,isnull(OPP.numProjectStageID,0) as numProjectStageID,0 as numBillId,
--					Case When OPP.numProjectStageID>0 then dbo.fn_GetProjectStageName(OPP.numProjectId,OPP.numProjectStageID) else 'P.O.' end as vcFrom,
--					OPP.numProjectID as numProId,OM.bintCreatedDate as dtCreatedOn,isnull((SELECT SUBSTRING((SELECT ',' + Cast(OB.numOppBizDocsId as varchar(18)) from  [OpportunityBizDocs] OB 
--					INNER JOIN [OpportunityBizDocItems] OBI ON OBI.numOppBizDocID = OB.numOppBizDocsId AND OPP.numoppitemtCode = OBI.numOppItemID 
--					WHERE OB.bitAuthoritativeBizDocs=1 and OB.numOppId = OM.numOppId FOR XML PATH('')),2,200000)),'') AS vcBizDoc,0 AS numDepositId,0 AS numCheckHeaderID,0 AS numJournalId
--					,ISNULL(Opp.monAvgCost,0) AS monAvgCost,ISNULL(Opp.monVendorCost,0) AS monVendorCost
--			 from  [OpportunityMaster] OM 
--						JOIN [OpportunityItems] OPP ON OM.[numOppId] = OPP.[numOppId]
--                        LEFT OUTER JOIN [Item] I ON OPP.[numItemCode] = I.[numItemCode]
--						LEFT OUTER JOIN [Chart_Of_Accounts] COA ON I.[numCOGsChartAcntId] = COA.[numAccountId]
--						Inner Join (select sum(ISNULL(OBI.[numUnitHour], 0)) as numUnitHour,OM.numOppId,OPP.numoppitemtCode from  
--                    OpportunityMaster OM 
--                    JOIN [OpportunityItems] OPP ON OM.[numOppId] = OPP.[numOppId]
--					INNER JOIN [OpportunityBizDocs] OB ON OB.numOppId = OM.numOppId
--					INNER JOIN [OpportunityBizDocItems] OBI ON OBI.numOppBizDocID = OB.numOppBizDocsId 
--						AND OPP.numoppitemtCode = OBI.numOppItemID 
--					WHERE OM.numDomainId=@numDomainID AND OPP.numProjectID = @numProId AND OM.[tintOppType] = 2 and OB.bitAuthoritativeBizDocs=1
--						group by OM.numOppId,OPP.numoppitemtCode) X on
--					X.numOppId = OM.numOppId and OPP.numoppitemtCode = X.numoppitemtCode
--			 where  OM.numDomainId=@numDomainID  
--					AND OPP.numProjectID = @numProId
--					AND OM.[tintOppType] = 2

--			 Union ALL
			
--			select 4 as iTEType,'Bill' as vcTimeExpenseType,BD.vcDescription as vcItemName,0 as numUnitHour,0 as monPrice,BD.monAmount AS ExpenseAmount,
--				dbo.[FormatedDateFromDate](BH.dtCreatedDate, @numDomainID) AS dtDateEntered,
--                    dbo.fn_GetContactName(BH.numCreatedBy) AS vcCreatedBy,0 as numOppId,0 as numOppType,
--					0 as numCategoryHDRID,isnull(PO.numStageID,0) as numProjectStageID,PO.numBillId,
--					Case When PO.numStageID>0 then dbo.fn_GetProjectStageName(PO.numProID,PO.numStageID) else 'Bill' end as vcFrom,
--					PO.numProID,BH.dtCreatedDate as dtCreatedOn,'' as vcBizDoc,0 AS numDepositId,0 AS numCheckHeaderID,0 AS numJournalId
--					,0 AS monAvgCost,0 AS monVendorCost
--			 from  ProjectsOpportunities PO 
--				   JOIN BillHeader BH ON BH.numBillID = PO.numBillID
--				   JOIN BillDetails BD ON BH.numBillID=BD.numBillID AND PO.numProID=BD.numProjectID
--			 where  PO.numDomainId=@numDomainID  
--					AND PO.numProID = @numProId
--					and isnull(PO.numBillId,0)>0
			
--			UNION ALL

--			SELECT  5 as iTEType,'Employee Expense' AS vcTimeExpenseType,
--                    ISNULL(CONVERT(VARCHAR(25),txtDesc) + '..' ,'') AS [vcItemName],
--                    ISNULL(CONVERT(VARCHAR(100), CONVERT(DECIMAL(10, 1), ( CONVERT(DECIMAL(10, 1), DATEDIFF(minute, TE.dtFromDate, TE.dtToDate)) / 60 ))),0)
--                    AS [numUnitHour],
--                    isnull(UM.monHourlyRate,0) monPrice,
--					isnull(Cast(datediff(minute,TE.dtfromdate,dttodate) as float)/Cast(60 as float),0) * isnull(UM.monHourlyRate,0) AS ExpenseAmount,
--                    dbo.[FormatedDateFromDate](TE.[dtTCreatedOn], @numDomainID) AS dtDateEntered,
--                    dbo.fn_GetContactName(TE.numTCreatedBy) AS vcCreatedBy,0 as numOppId,0 as numOppType,
--					TE.[numCategoryHDRID],isnull(TE.numStageId,0) as numProjectStageID,0 as numBillId,
--					Case When TE.numStageId>0 then dbo.fn_GetProjectStageName(TE.numProId,TE.numStageId) else 'Expense' end as vcFrom,
--					TE.numProId,TE.[dtTCreatedOn] as dtCreatedOn,'' as vcBizDoc,0 AS numDepositId,0 AS numCheckHeaderID,0 AS numJournalId
--					,0 AS monAvgCost,0 AS monVendorCost
--            FROM    [TimeAndExpense] TE join UserMaster UM on UM.numUserDetailId=TE.numUserCntID
--            WHERE   [numProId] = @numProId
--                    AND TE.[numDomainID] = @numDomainID
--                    AND numType in (1,2) --Non Billable & Billable
--					AND TE.numUserCntID>0
----					AND bitReimburse=1 and  numCategory=2  

--			select 6 as iTEType,'Deposit' as vcTimeExpenseType,DED.vcMemo as vcItemName,0 as numUnitHour,0 as monPrice,DED.monAmountPaid AS ExpenseAmount,
--				dbo.[FormatedDateFromDate](DEM.dtCreationDate, @numDomainID) AS dtDateEntered,
--                    dbo.fn_GetContactName(DEM.numCreatedBy) AS vcCreatedBy,0 as numOppId,0 as numOppType,
--					0 as numCategoryHDRID,0 as numProjectStageID,0 AS numBillId,
--					'' as vcFrom,
--					DED.numProjectID AS numProID,DEM.dtCreationDate as dtCreatedOn,'' as vcBizDoc,DEM.numDepositId,0 AS numCheckHeaderID,0 AS numJournalId
--					,0 AS monAvgCost,0 AS monVendorCost
--			 from  DepositMaster DEM JOIN DepositeDetails DED ON DEM.numDepositID=DED.numDepositID
--			 WHERE DEM.tintDepositePage=1 AND DEM.numDomainId=@numDomainID  
--					AND DED.numProjectID = @numProId
				
--			UNION ALL
					
--			select 7 as iTEType,'Check' as vcTimeExpenseType,CD.vcDescription as vcItemName,0 as numUnitHour,0 as monPrice,CD.monAmount AS ExpenseAmount,
--				dbo.[FormatedDateFromDate](CH.dtCreatedDate, @numDomainID) AS dtDateEntered,
--                    dbo.fn_GetContactName(CH.numCreatedBy) AS vcCreatedBy,0 as numOppId,0 as numOppType,
--					0 as numCategoryHDRID,0 as numProjectStageID,0 AS numBillId,
--					'' as vcFrom,
--					CD.numProjectID AS numProID,CH.dtCreatedDate as dtCreatedOn,'' as vcBizDoc,0 AS numDepositId,CH.numCheckHeaderID,0 AS numJournalId
--					,0 AS monAvgCost,0 AS monVendorCost
--			 from  CheckHeader CH JOIN CheckDetails CD ON CH.numCheckHeaderID=CD.numCheckHeaderID
--			 WHERE CH.tintReferenceType=1 AND CH.numDomainId=@numDomainID  
--					AND CD.numProjectID = @numProId		
			
--			UNION ALL
			
--			select 9 as iTEType,'Journal' as vcTimeExpenseType,GD.varDescription as vcItemName,0 as numUnitHour,0 as monPrice,GD.numDebitAmt AS ExpenseAmount,
--				dbo.[FormatedDateFromDate](GH.datCreatedDate, @numDomainID) AS dtDateEntered,
--                    dbo.fn_GetContactName(GH.numCreatedBy) AS vcCreatedBy,0 as numOppId,0 as numOppType,
--					0 as numCategoryHDRID,0 as numProjectStageID,0 AS numBillId,
--					'' as vcFrom,
--					GD.numProjectID AS numProID,GH.datCreatedDate as dtCreatedOn,'' as vcBizDoc,0 AS numDepositId,0 AS numCheckHeaderID,GD.numJournalId
--					,0 AS monAvgCost,0 AS monVendorCost
--			 from  dbo.General_Journal_Header GH JOIN dbo.General_Journal_Details GD ON GH.numJournal_Id = GD.numJournalId
--					JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = GD.numChartAcntId
--			 WHERE ISNULL(GH.numCheckHeaderID,0)=0 AND ISNULL(GH.numBillPaymentID,0)=0 AND ISNULL(GH.numBillID,0)=0
--					AND ISNULL(GH.numDepositId,0)=0 AND ISNULL(GH.numOppId,0)=0 AND ISNULL(GH.numOppBizDocsId,0)=0
--					AND ISNULL(GH.numReturnId,0)=0
--					AND GH .numDomainId=@numDomainID  AND GD.numProjectID = @numProId
--					AND ISNULL(GD.numDebitAmt,0)>0	AND (COA.vcAccountCode LIKE '0103%' OR  COA.vcAccountCode LIKE '0104%' OR  COA.vcAccountCode LIKE '0106%')	
				
--			UNION ALL
					
--			 select 8 as iTEType,'Journal' as vcTimeExpenseType,GD.varDescription as vcItemName,0 as numUnitHour,0 as monPrice,GD.numCreditAmt AS ExpenseAmount,
--				dbo.[FormatedDateFromDate](GH.datCreatedDate, @numDomainID) AS dtDateEntered,
--                    dbo.fn_GetContactName(GH.numCreatedBy) AS vcCreatedBy,0 as numOppId,0 as numOppType,
--					0 as numCategoryHDRID,0 as numProjectStageID,0 AS numBillId,
--					'' as vcFrom,
--					GD.numProjectID AS numProID,GH.datCreatedDate as dtCreatedOn,'' as vcBizDoc,0 AS numDepositId,0 AS numCheckHeaderID,GD.numJournalId
--					,0 AS monAvgCost,0 AS monVendorCost
--			 from  dbo.General_Journal_Header GH JOIN dbo.General_Journal_Details GD ON GH.numJournal_Id = GD.numJournalId
--					JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = GD.numChartAcntId
--			 WHERE ISNULL(GH.numCheckHeaderID,0)=0 AND ISNULL(GH.numBillPaymentID,0)=0 AND ISNULL(GH.numBillID,0)=0
--					AND ISNULL(GH.numDepositId,0)=0 AND ISNULL(GH.numOppId,0)=0 AND ISNULL(GH.numOppBizDocsId,0)=0
--					AND ISNULL(GH.numReturnId,0)=0
--					AND GH .numDomainId=@numDomainID  AND GD.numProjectID = @numProId
--					AND ISNULL(GD.numCreditAmt,0)>0  AND (COA.vcAccountCode LIKE '0103%' OR  COA.vcAccountCode LIKE '0104%' OR  COA.vcAccountCode LIKE '0106%')
									
--			order by dtCreatedOn
-- END 
	   
END        
        

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetVendorShipmentMethod')
DROP PROCEDURE USP_GetVendorShipmentMethod
GO
CREATE PROCEDURE [dbo].[USP_GetVendorShipmentMethod]         
@numDomainID as numeric(9)=0,    
@numVendorid AS NUMERIC(9)=0,
@numAddressID AS NUMERIC(9)=0,
@numWarehouseID AS NUMERIC(18,0) = 0,
@tintMode AS TINYINT        
AS    
BEGIN
	IF @tintMode=1
	BEGIN 
		IF EXISTS (SELECT numShipmentMethod FROM VendorShipmentMethod VM WHERE VM.numDomainID=@numDomainID AND VM.numVendorid=@numVendorid AND ISNULL(numWarehouseID,0) = @numWarehouseID)
		BEGIN
			SELECT 
				LD.numListItemID
				,LD.vcData AS ShipmentMethod
				,ISNULL(VM.numListValue,0) AS numListValue
				,VM.bitPrimary
				,ISNULL(VM.bitPreferredMethod,0) bitPreferredMethod
			FROM 
				ListDetails LD 
			LEFT JOIN 
				VendorShipmentMethod VM 
			ON 
				VM.numDomainID=@numDomainID
				AND VM.numVendorid=@numVendorid
				AND VM.numAddressID=@numAddressID 
				AND VM.numListItemID=LD.numListItemID
				AND VM.numWarehouseID=@numWarehouseID
			WHERE 
				LD.numListID=338 
				AND (LD.numDomainID=@numDomainID OR Ld.constFlag=1)
		END
		ELSE
		BEGIN
			SELECT 
				LD.numListItemID
				,LD.vcData AS ShipmentMethod
				,ISNULL(VM.numListValue,0) AS numListValue
				,VM.bitPrimary
				,ISNULL(VM.bitPreferredMethod,0) bitPreferredMethod
			FROM 
				ListDetails LD 
			LEFT JOIN 
				VendorShipmentMethod VM 
			ON 
				VM.numDomainID=@numDomainID
				AND VM.numVendorid=@numVendorid
				AND VM.numAddressID=@numAddressID 
				AND VM.numListItemID=LD.numListItemID
				AND ISNULL(VM.numWarehouseID,0)=0
			WHERE 
				LD.numListID=338 
				AND (LD.numDomainID=@numDomainID OR Ld.constFlag=1)
		END
	END
	ELSE IF @tintMode=2
	BEGIN 
		IF ISNULL(@numAddressID,0) = -1
		BEGIN
			SET @numAddressID = ISNULL((SELECT TOP 1 
											AD.numAddressID 
										FROM 
											AddressDetails AD 
										WHERE 
											AD.numDomainID=@numDomainID 
											AND AD.numRecordID=@numVendorid 
											AND AD.tintAddressOf=2 
											AND AD.tintAddressType=2 
										ORDER BY ISNULL(AD.bitIsPrimary,0) DESC),0)
		END

		SELECT LD.numListItemID,LD.vcData AS ShipmentMethod,ISNULL(VM.numListValue,0) AS numListValue,VM.bitPrimary,
		cast(LD.numListItemID AS VARCHAR(10)) + '~' + cast(ISNULL(VM.numListValue,0) AS VARCHAR(10)) AS vcListValue
			from ListDetails LD JOIN VendorShipmentMethod VM ON VM.numListItemID=LD.numListItemID 
		AND VM.numVendorid=@numVendorid AND VM.numAddressID=@numAddressID AND VM.numDomainID=@numDomainID
		WHERE LD.numListID=338 AND (LD.numDomainID=@numDomainID or Ld.constFlag=1) ORDER BY ISNULL(bitPrimary,0) DESC, ISNULL(bitPreferredMethod,0) DESC
	END
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Item_GetMatchedPO')
DROP PROCEDURE USP_Item_GetMatchedPO
GO
CREATE PROCEDURE [dbo].[USP_Item_GetMatchedPO]
	@numDomainId NUMERIC(18,0)
	,@vcItems VARCHAR(MAX)
AS
BEGIN
	DECLARE @TEMP TABLE
	(
		[Index] INT 
		,ItemCode NUMERIC(18,0)
        ,Item VARCHAR(300)
        ,Attributes VARCHAR(1000)
        ,AttributeValues VARCHAR(500)
        ,UnitHour FLOAT
        ,POs VARCHAR(MAX)
	)

	DECLARE @hDoc as INTEGER
	EXEC sp_xml_preparedocument @hDoc OUTPUT, @vcItems

	INSERT INTO
		@TEMP
	SELECT
		[Index]
		,ItemCode
        ,Item
        ,Attributes
        ,AttributeValues
        ,UnitHour
		,''
	FROM
		OPENXML(@hDoc,'/NewDataSet/Table1',2)                                                                          
	WITH                       
	(
		[Index] INT 
		,ItemCode NUMERIC(18,0)
        ,Item VARCHAR(300)
        ,Attributes VARCHAR(1000)
        ,AttributeValues VARCHAR(500)
        ,UnitHour FLOAT
        ,POs VARCHAR(MAX)
	)

	EXEC sp_xml_removedocument @hDoc

	UPDATE	
		TP
	SET
		POs = CONCAT('[',STUFF((SELECT 
									CONCAT(', {"numOppID":',OM.numOppId,', "numOppItemID":',OI.numoppitemtCode,', "POName":"',ISNULL(OM.vcPOppName,''),'", "Vendor":"',ISNULL(CI.vcCompanyName,''),'","Price":',ISNULL(OI.monPrice,0),'}') 
								FROM 
									OpportunityMaster OM
								INNER JOIN
									DivisionMaster DM
								ON
									OM.numDivisionId = DM.numDivisionID
								INNER JOIN
									CompanyInfo CI
								ON
									DM.numCompanyID = CI.numCompanyId
								INNER JOIN
									OpportunityItems OI
								ON
									OM.numOppId = OI.numOppId
								WHERE 
									OM.numDomainId = @numDomainId
									AND OM.tintOppType = 2
									AND OM.tintOppStatus = 1
									AND ISNULL(OM.tintshipped,0) = 0
									AND OI.numItemCode = TP.ItemCode
									AND OI.numUnitHour = TP.UnitHour
									AND ISNULL(OI.vcAttrValues,'') = ISNULL(TP.AttributeValues,'')
								FOR XML PATH('')),1,1,''),']')
	FROM
		@TEMP TP


	SELECT * FROM @TEMP
END
GO
GO
/****** Object:  StoredProcedure [dbo].[USP_ManageItemsAndKits]    Script Date: 02/15/2010 21:44:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Anoop Jayaraj                                                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_manageitemsandkits')
DROP PROCEDURE usp_manageitemsandkits
GO
CREATE PROCEDURE [dbo].[USP_ManageItemsAndKits]                                                                
@numItemCode as numeric(9) output,                                                                
@vcItemName as varchar(300),                                                                
@txtItemDesc as varchar(2000),                                                                
@charItemType as char(1),                                                                
@monListPrice as DECIMAL(20,5),                                                                
@numItemClassification as numeric(9),                                                                
@bitTaxable as bit,                                                                
@vcSKU as varchar(50),                                                                                                                                      
@bitKitParent as bit,                                                                                   
--@dtDateEntered as datetime,                                                                
@numDomainID as numeric(9),                                                                
@numUserCntID as numeric(9),                                                                                                  
@numVendorID as numeric(9),                                                            
@bitSerialized as bit,                                               
@strFieldList as text,                                            
@vcModelID as varchar(200)='' ,                                            
@numItemGroup as numeric(9)=0,                                      
@numCOGSChartAcntId as numeric(9)=0,                                      
@numAssetChartAcntId as numeric(9)=0,                                      
@numIncomeChartAcntId as numeric(9)=0,                            
@monAverageCost as DECIMAL(20,5),                          
@monLabourCost as DECIMAL(20,5),                  
@fltWeight as float,                  
@fltHeight as float,                  
@fltLength as float,                  
@fltWidth as float,                  
@bitFreeshipping as bit,                
@bitAllowBackOrder as bit,              
@UnitofMeasure as varchar(30)='',              
@strChildItems as text,            
@bitShowDeptItem as bit,              
@bitShowDeptItemDesc as bit,        
@bitCalAmtBasedonDepItems as bit,    
@bitAssembly as BIT,
@intWebApiId INT=null,
@vcApiItemId AS VARCHAR(50)=null,
@numBarCodeId as VARCHAR(25)=null,
@vcManufacturer as varchar(250)=NULL,
@numBaseUnit AS NUMERIC(18)=0,
@numPurchaseUnit AS NUMERIC(18)=0,
@numSaleUnit AS NUMERIC(18)=0,
@bitLotNo as BIT,
@IsArchieve AS BIT,
@numItemClass as numeric(9)=0,
@tintStandardProductIDType AS TINYINT=0,
@vcExportToAPI VARCHAR(50),
@numShipClass NUMERIC,
@ProcedureCallFlag SMALLINT =0,
@vcCategories AS VARCHAR(2000) = '',
@bitAllowDropShip AS BIT=0,
@bitArchiveItem AS BIT = 0,
@bitAsset AS BIT = 0,
@bitRental AS BIT = 0,
@numCategoryProfileID AS NUMERIC(18,0) = 0,
@bitVirtualInventory BIT,
@bitContainer BIT,
@numContainer NUMERIC(18,0)=0,
@numNoItemIntoContainer NUMERIC(18,0),
@bitMatrix BIT = 0 ,
@vcItemAttributes VARCHAR(2000) = '',
@numManufacturer NUMERIC(18,0),
@vcASIN as varchar(50),
@tintKitAssemblyPriceBasedOn TINYINT = 1,
@fltReorderQty FLOAT = 0,
@bitSOWorkOrder BIT = 0,
@bitKitSingleSelect BIT = 0,
@bitExpenseItem BIT = 0,
@numExpenseChartAcntId NUMERIC(18,0) = 0,
@numBusinessProcessId NUMERIC(18,0)=0,
@bitTimeContractFromSalesOrder BIT =0 
AS
BEGIN     
	SET @vcManufacturer = CASE WHEN ISNULL(@numManufacturer,0) > 0 THEN ISNULL((SELECT vcCompanyName FROM DivisionMaster INNER JOIN CompanyInfo ON DivisionMaster.numCompanyID=CompanyInfo.numCompanyId WHERE numDivisionID=@numManufacturer),'')  ELSE ISNULL(@vcManufacturer,'') END

	DECLARE @bitAttributesChanged AS BIT  = 1
	DECLARE @bitOppOrderExists AS BIT = 0

	IF ISNULL(@numItemCode,0) > 0
	BEGIN
		IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityItems OI WHERE OI.numItemCode =@numItemCode) > 0
		BEGIN
			SET @bitOppOrderExists = 1
		END
		ELSE IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityKitItems OI WHERE OI.numChildItemID =@numItemCode) > 0
		BEGIN
			SET @bitOppOrderExists = 1
		END
		ELSE IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityKitChildItems OI WHERE OI.numItemID =@numItemCode) > 0
		BEGIN
			SET @bitOppOrderExists = 1
		END
	END
	ELSE 
	BEGIN
		IF (ISNULL(@vcItemName,'') = '')
		BEGIN
			RAISERROR('ITEM_NAME_NOT_SELECTED',16,1)
			RETURN
		END
        ELSE IF (ISNULL(@charItemType,'') = '0')
		BEGIN
			RAISERROR('ITEM_TYPE_NOT_SELECTED',16,1)
			RETURN
		END
        ELSE
		BEGIN 
		    If (@charItemType = 'P')
			BEGIN
				IF ((SELECT COUNT(*) FROM Chart_Of_Accounts WHERE numDomainId=@numDomainID AND numAccountId=@numAssetChartAcntId) = 0)
				BEGIN
					RAISERROR('INVALID_ASSET_ACCOUNT',16,1)
					RETURN
				END
			END

			-- This is common check for CharItemType P and Other
			IF ((SELECT COUNT(*) FROM Chart_Of_Accounts WHERE numDomainId=@numDomainID AND numAccountId=@numCOGSChartAcntId) = 0)
			BEGIN
				RAISERROR('INVALID_COGS_ACCOUNT',16,1)
				RETURN
			END

			IF ((SELECT COUNT(*) FROM Chart_Of_Accounts WHERE numDomainId=@numDomainID AND numAccountId=@numIncomeChartAcntId) = 0)
			BEGIN
				RAISERROR('INVALID_INCOME_ACCOUNT',16,1)
				RETURN
			END

			IF ISNULL(@bitExpenseItem,0) = 1
			BEGIN
				IF ((SELECT COUNT(*) FROM Chart_Of_Accounts WHERE numDomainId=@numDomainID AND numAccountId=@numExpenseChartAcntId) = 0)
				BEGIN
					RAISERROR('INVALID_EXPENSE_ACCOUNT',16,1)
					RETURN
				END
			END
		END
	END

	--INSERT CUSTOM FIELDS BASE ON WAREHOUSEITEMS
	DECLARE @hDoc AS INT     
	                                                                        	
	DECLARE @TempTable TABLE 
	(
		ID INT IDENTITY(1,1),
		Fld_ID NUMERIC(18,0),
		Fld_Value NUMERIC(18,0)
	)   

	IF ISNULL(@bitMatrix,0) = 1 AND ISNULL(@numItemGroup,0) = 0
	BEGIN
		RAISERROR('ITEM_GROUP_NOT_SELECTED',16,1)
		RETURN
	END
	ELSE IF ISNULL(@bitMatrix,0) = 1 AND LEN(ISNULL(@vcItemAttributes,'')) = 0 
	BEGIN
		RAISERROR('ITEM_ATTRIBUTES_NOT_SELECTED',16,1)
		RETURN
	END
	ELSE IF ISNULL(@bitMatrix,0) = 1 
	BEGIN
		DECLARE @bitDuplicate AS BIT = 0
	
		IF DATALENGTH(ISNULL(@vcItemAttributes,''))>2
		BEGIN
			SET @vcItemAttributes = CONCAT('<?xml version="1.0" encoding="iso-8859-1"?>',@vcItemAttributes)

			EXEC sp_xml_preparedocument @hDoc OUTPUT, @vcItemAttributes
	                                      
			INSERT INTO @TempTable 
			(
				Fld_ID,
				Fld_Value
			)    
			SELECT
				*          
			FROM 
				OPENXML (@hDoc,'/NewDataSet/CusFlds',2)                                                  
			WITH 
				(Fld_ID NUMERIC(18,0),Fld_Value NUMERIC(18,0)) 
			ORDER BY 
				Fld_ID       

			DECLARE @strAttribute VARCHAR(500) = ''
			DECLARE @i AS INT = 1
			DECLARE @COUNT AS INT 
			DECLARE @numFldID AS NUMERIC(18,0)
			DECLARE @numFldValue AS NUMERIC(18,0)
			SELECT @COUNT = COUNT(*) FROM @TempTable

			WHILE @i <= @COUNT
			BEGIN
				SELECT @numFldID=Fld_ID,@numFldValue=Fld_Value FROM @TempTable WHERE ID=@i

				IF ISNUMERIC(@numFldValue)=1
				BEGIN
					SELECT @strAttribute=@strAttribute+Fld_label FROM CFW_Fld_Master WHERE Fld_id=@numFldID AND Grp_id = 9
                
					IF LEN(@strAttribute) > 0
					BEGIN
						SELECT @strAttribute=@strAttribute+':'+ vcData +',' FROM ListDetails WHERE numListItemID=@numFldValue
					END
				END

				SET @i = @i + 1
			END

			SET @strAttribute = SUBSTRING(@strAttribute,0,LEN(@strAttribute))

			IF
				(SELECT
					COUNT(*)
				FROM
					Item
				INNER JOIN
					ItemAttributes
				ON
					Item.numItemCode = ItemAttributes.numItemCode
					AND ItemAttributes.numDomainID = @numDomainID                     
				WHERE
					Item.numItemCode <> @numItemCode
					AND Item.vcItemName = @vcItemName
					AND Item.numItemGroup = @numItemGroup
					AND Item.numDomainID = @numDomainID
					AND dbo.fn_GetItemAttributes(@numDomainID,Item.numItemCode) = @strAttribute
				) > 0
			BEGIN
				RAISERROR('ITEM_WITH_ATTRIBUTES_ALREADY_EXISTS',16,1)
				RETURN
			END

			If ISNULL(@numItemCode,0) > 0 AND dbo.fn_GetItemAttributes(@numDomainID,@numItemCode) = @strAttribute
			BEGIN
				SET @bitAttributesChanged = 0
			END

			EXEC sp_xml_removedocument @hDoc 
		END	
	END
	ELSE IF ISNULL(@bitMatrix,0) = 0
	BEGIN
		-- REMOVE ATTRIBUTES CONFIGURATION OF ITEM AND WAREHOUSE
		DELETE FROM ItemAttributes WHERE numDomainID=@numDomainID AND numItemCode=@numItemCode
	END

	IF ISNULL(@numItemGroup,0) > 0 AND ISNULL(@bitMatrix,0) = 1
	BEGIN
		IF (SELECT COUNT(*) FROM ItemGroupsDTL WHERE numItemGroupID = @numItemGroup AND tintType=2) = 0
		BEGIN
			RAISERROR('ITEM_GROUP_NOT_CONFIGURED',16,1)
			RETURN
		END
		ELSE IF EXISTS (SELECT 
							CFM.Fld_id
						FROM 
							CFW_Fld_Master CFM
						LEFT JOIN
							ListDetails LD
						ON
							CFM.numlistid = LD.numListID
						WHERE
							CFM.numDomainID = @numDomainID
							AND CFM.Fld_id IN (SELECT numOppAccAttrID FROM ItemGroupsDTL WHERE numItemGroupID=@numItemGroup AND tintType = 2)
						GROUP BY
							CFM.Fld_id HAVING COUNT(LD.numListItemID) = 0)
		BEGIN
			RAISERROR('ITEM_GROUP_NOT_CONFIGURED',16,1)
			RETURN
		END
	END

BEGIN TRY
BEGIN TRANSACTION

	

	IF @numCOGSChartAcntId=0 SET @numCOGSChartAcntId=NULL
	IF @numAssetChartAcntId=0 SET @numAssetChartAcntId=NULL  
	IF @numIncomeChartAcntId=0 SET @numIncomeChartAcntId=NULL     
	IF @numExpenseChartAcntId=0 SET @numExpenseChartAcntId=NULL

	DECLARE @ParentSKU VARCHAR(50) = @vcSKU                        
	DECLARE @ItemID AS NUMERIC(9)
	DECLARE @cnt AS INT

	IF ISNULL(@charItemType,'') = '' OR ISNULL(@charItemType,'') = '0'  
		SET @charItemType = 'N'

	IF @intWebApiId = 2 AND LEN(ISNULL(@vcApiItemId, '')) > 0 AND ISNULL(@numItemGroup, 0) > 0 
    BEGIN 
        -- check wether this id already Mapped in ITemAPI 
        SELECT 
			@cnt = COUNT([numItemID])
        FROM 
			[ItemAPI]
        WHERE 
			[WebApiId] = @intWebApiId
            AND [numDomainId] = @numDomainID
            AND [vcAPIItemID] = @vcApiItemId

        IF @cnt > 0 
        BEGIN
            SELECT 
				@ItemID = [numItemID]
            FROM 
				[ItemAPI]
            WHERE 
				[WebApiId] = @intWebApiId
                AND [numDomainId] = @numDomainID
                AND [vcAPIItemID] = @vcApiItemId
        
			--Update Existing Product coming from API
            SET @numItemCode = @ItemID
        END
    END
	ELSE IF @intWebApiId > 1 AND LEN(ISNULL(@vcSKU, '')) > 0 
    BEGIN
        SET @ParentSKU = @vcSKU
		 
        SELECT 
			@cnt = COUNT([numItemCode])
        FROM 
			[Item]
        WHERE 
			[numDomainId] = @numDomainID
            AND (vcSKU = @vcSKU OR @vcSKU IN (SELECT vcWHSKU FROM dbo.WareHouseItems WHERE numItemID = Item.[numItemCode] AND vcWHSKU = @vcSKU))
            
		IF @cnt > 0 
        BEGIN
            SELECT 
				@ItemID = [numItemCode],
                @ParentSKU = vcSKU
            FROM 
				[Item]
            WHERE 
				[numDomainId] = @numDomainID
                AND (vcSKU = @vcSKU OR @vcSKU IN (SELECT vcWHSKU FROM dbo.WareHouseItems WHERE numItemID = Item.[numItemCode] AND vcWHSKU = @vcSKU))

            SET @numItemCode = @ItemID
        END
		ELSE 
		BEGIN
			-- check wether this id already Mapped in ITemAPI 
			SELECT 
				@cnt = COUNT([numItemID])
			FROM 
				[ItemAPI]
			WHERE 
				[WebApiId] = @intWebApiId
				AND [numDomainId] = @numDomainID
				AND [vcAPIItemID] = @vcApiItemId
                    
			IF @cnt > 0 
			BEGIN
				SELECT 
					@ItemID = [numItemID]
				FROM 
					[ItemAPI]
				WHERE 
					[WebApiId] = @intWebApiId
					AND [numDomainId] = @numDomainID
					AND [vcAPIItemID] = @vcApiItemId
        
				--Update Existing Product coming from API
				SET @numItemCode = @ItemID
			END
		END
    END
                                                         
	IF @numItemCode=0 OR @numItemCode IS NULL
	BEGIN
		INSERT INTO Item 
		(                                             
			vcItemName,txtItemDesc,charItemType,monListPrice,numItemClassification,bitTaxable,vcSKU,bitKitParent,
			numVendorID,numDomainID,numCreatedBy,bintCreatedDate,bintModifiedDate,numModifiedBy,bitSerialized,
			vcModelID,numItemGroup,numCOGsChartAcntId,numAssetChartAcntId,numIncomeChartAcntId,monAverageCost,                          
			monCampaignLabourCost,fltWeight,fltHeight,fltWidth,fltLength,bitFreeShipping,bitAllowBackOrder,vcUnitofMeasure,
			bitShowDeptItem,bitShowDeptItemDesc,bitCalAmtBasedonDepItems,bitAssembly,numBarCodeId,vcManufacturer,numBaseUnit,
			numPurchaseUnit,numSaleUnit,bitLotNo,IsArchieve,numItemClass,tintStandardProductIDType,vcExportToAPI,numShipClass,
			bitAllowDropShip,bitArchiveItem,bitAsset,bitRental,bitVirtualInventory,bitContainer,numContainer,numNoItemIntoContainer
			,bitMatrix,numManufacturer,vcASIN,tintKitAssemblyPriceBasedOn,fltReorderQty,bitSOWorkOrder,bitKitSingleSelect,bitExpenseItem,numExpenseChartAcntId,numBusinessProcessId,bitTimeContractFromSalesOrder 
		)                                                              
		VALUES                                                                
		(                                                                
			@vcItemName,@txtItemDesc,@charItemType,@monListPrice,@numItemClassification,@bitTaxable,@ParentSKU,@bitKitParent,
			@numVendorID,@numDomainID,@numUserCntID,getutcdate(),getutcdate(),@numUserCntID,@bitSerialized,@vcModelID,@numItemGroup,                                      
			@numCOGsChartAcntId,@numAssetChartAcntId,@numIncomeChartAcntId,@monAverageCost,@monLabourCost,@fltWeight,@fltHeight,@fltWidth,                  
			@fltLength,@bitFreeshipping,@bitAllowBackOrder,@UnitofMeasure,@bitShowDeptItem,@bitShowDeptItemDesc,@bitCalAmtBasedonDepItems,    
			@bitAssembly,@numBarCodeId,@vcManufacturer,@numBaseUnit,@numPurchaseUnit,@numSaleUnit,@bitLotNo,@IsArchieve,@numItemClass,
			@tintStandardProductIDType,@vcExportToAPI,@numShipClass,@bitAllowDropShip,@bitArchiveItem ,@bitAsset,@bitRental,
			@bitVirtualInventory,@bitContainer,@numContainer,@numNoItemIntoContainer
			,@bitMatrix,@numManufacturer,@vcASIN,@tintKitAssemblyPriceBasedOn,@fltReorderQty,@bitSOWorkOrder,@bitKitSingleSelect,@bitExpenseItem,@numExpenseChartAcntId,@numBusinessProcessId,@bitTimeContractFromSalesOrder 
		)                                             
 
		SET @numItemCode = SCOPE_IDENTITY()
  
		IF  @intWebApiId > 1 
		BEGIN
			 -- insert new product
			 --insert this id into linking table
			 INSERT INTO [ItemAPI] (
	 			[WebApiId],
	 			[numDomainId],
	 			[numItemID],
	 			[vcAPIItemID],
	 			[numCreatedby],
	 			[dtCreated],
	 			[numModifiedby],
	 			[dtModified],vcSKU
			 ) VALUES     
			 (
				@intWebApiId,
				@numDomainID,
				@numItemCode,
				@vcApiItemId,
	 			@numUserCntID,
	 			GETUTCDATE(),
	 			@numUserCntID,
	 			GETUTCDATE(),@vcSKU
	 		) 
		END
 
		IF @charItemType = 'P'
		BEGIN
			
			DECLARE @TEMPWarehouse TABLE
			(
				ID INT IDENTITY(1,1)
				,numWarehouseID NUMERIC(18,0)
			)
		
			INSERT INTO @TEMPWarehouse (numWarehouseID) SELECT numWareHouseID FROM Warehouses WHERE numDomainID=@numDomainID

			DECLARE @j AS INT = 1
			DECLARE @jCount AS INT
			DECLARE @numTempWarehouseID NUMERIC(18,0)
			SELECT @jCount = COUNT(*) FROM @TEMPWarehouse

			WHILE @j <= @jCount
			BEGIN
				SELECT @numTempWarehouseID=numWarehouseID FROM @TEMPWarehouse WHERE ID=@j

				EXEC USP_WarehouseItems_Save @numDomainID,
											@numUserCntID,
											0,
											@numTempWarehouseID,
											0,
											@numItemCode,
											'',
											@monListPrice,
											0,
											0,
											'',
											'',
											'',
											'',
											0
 
				SET @j = @j + 1
			END
		END 
	END         
	ELSE IF @numItemCode>0 AND @intWebApiId > 0 
	BEGIN 
		IF @ProcedureCallFlag = 1 
		BEGIN
			DECLARE @ExportToAPIList AS VARCHAR(30)
			SELECT @ExportToAPIList = vcExportToAPI FROM dbo.Item WHERE numItemCode = @numItemCode

			IF @ExportToAPIList != ''
			BEGIN
				IF NOT EXISTS(select * from (SELECT * FROM dbo.Split(@ExportToAPIList ,','))as A where items= @vcExportToAPI ) 
				BEGIN
					SET @vcExportToAPI = @ExportToAPIList + ',' + @vcExportToAPI
				End
				ELSE
				BEGIN
					SET @vcExportToAPI = @ExportToAPIList 
				End
			END

			--update ExportToAPI String value
			UPDATE dbo.Item SET vcExportToAPI=@vcExportToAPI where numItemCode=@numItemCode  AND numDomainID=@numDomainID
					
			SELECT 
				@cnt = COUNT([numItemID]) 
			FROM 
				[ItemAPI] 
			WHERE  
				[WebApiId] = @intWebApiId 
				AND [numDomainId] = @numDomainID
				AND [vcAPIItemID] = @vcApiItemId
				AND numItemID = @numItemCode

			IF @cnt > 0
			BEGIN
				--update lastUpdated Date 
				UPDATE [ItemAPI] SET [dtModified] =GETUTCDATE() WHERE [WebApiId] =@intWebApiId AND [numDomainId] = @numDomainID
			END
			ELSE
			--Insert ItemAPI mapping
			BEGIN
				INSERT INTO [ItemAPI] 
				(
					[WebApiId],
					[numDomainId],
					[numItemID],
					[vcAPIItemID],
					[numCreatedby],
					[dtCreated],
					[numModifiedby],
					[dtModified],vcSKU
				) VALUES 
				(
					@intWebApiId,
					@numDomainID,
					@numItemCode,
					@vcApiItemId,
					@numUserCntID,
					GETUTCDATE(),
					@numUserCntID,
					GETUTCDATE(),@vcSKU
				)	
			END
		END   
	END                                            
	ELSE IF @numItemCode > 0 AND @intWebApiId <= 0                                                           
	BEGIN
		DECLARE @OldGroupID NUMERIC 
		SELECT @OldGroupID = ISNULL(numItemGroup,0) FROM [Item] WHERE numitemcode = @numItemCode AND [numDomainId] = @numDomainID
	
		DECLARE @monOldAverageCost AS DECIMAL(18,4);SET @monOldAverageCost=0
		SELECT @monOldAverageCost=(CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(monAverageCost,0) END) FROM Item WHERE numItemCode =@numItemCode AND [numDomainId] = @numDomainID
	   
		UPDATE 
			Item 
		SET 
			vcItemName=@vcItemName,txtItemDesc=@txtItemDesc,charItemType=@charItemType,monListPrice= @monListPrice,numItemClassification=@numItemClassification,                                             
			bitTaxable=@bitTaxable,vcSKU = (CASE WHEN ISNULL(@ParentSKU,'') = '' THEN @vcSKU ELSE @ParentSKU END),bitKitParent=@bitKitParent,                                             
			numVendorID=@numVendorID,numDomainID=@numDomainID,bintModifiedDate=getutcdate(),numModifiedBy=@numUserCntID,bitSerialized=@bitSerialized,                                                         
			vcModelID=@vcModelID,numItemGroup=@numItemGroup,numCOGsChartAcntId=@numCOGsChartAcntId,numAssetChartAcntId=@numAssetChartAcntId,                                      
			numIncomeChartAcntId=@numIncomeChartAcntId,monCampaignLabourCost=@monLabourCost,fltWeight=@fltWeight,fltHeight=@fltHeight,fltWidth=@fltWidth,                  
			fltLength=@fltLength,bitFreeShipping=@bitFreeshipping,bitAllowBackOrder=@bitAllowBackOrder,vcUnitofMeasure=@UnitofMeasure,bitShowDeptItem=@bitShowDeptItem,            
			bitShowDeptItemDesc=@bitShowDeptItemDesc,bitCalAmtBasedonDepItems=@bitCalAmtBasedonDepItems,bitAssembly=@bitAssembly,numBarCodeId=@numBarCodeId,
			vcManufacturer=@vcManufacturer,numBaseUnit=@numBaseUnit,numPurchaseUnit=@numPurchaseUnit,numSaleUnit=@numSaleUnit,bitLotNo=@bitLotNo,
			IsArchieve = @IsArchieve,numItemClass=@numItemClass,tintStandardProductIDType=@tintStandardProductIDType,vcExportToAPI=@vcExportToAPI,
			numShipClass=@numShipClass,bitAllowDropShip=@bitAllowDropShip, bitArchiveItem = @bitArchiveItem,bitAsset=@bitAsset,bitRental=@bitRental,
			bitVirtualInventory = @bitVirtualInventory,numContainer=@numContainer,numNoItemIntoContainer=@numNoItemIntoContainer,bitMatrix=@bitMatrix,numManufacturer=@numManufacturer,
			vcASIN = @vcASIN,tintKitAssemblyPriceBasedOn=@tintKitAssemblyPriceBasedOn,fltReorderQty=@fltReorderQty,bitSOWorkOrder=@bitSOWorkOrder,bitKitSingleSelect=@bitKitSingleSelect
			,bitExpenseItem=@bitExpenseItem,numExpenseChartAcntId=@numExpenseChartAcntId,numBusinessProcessId=@numBusinessProcessId,bitTimeContractFromSalesOrder =@bitTimeContractFromSalesOrder 
		WHERE 
			numItemCode=@numItemCode 
			AND numDomainID=@numDomainID

		IF NOT EXISTS(SELECT numItemCode FROM dbo.OpportunityItems WHERE numItemCode = @numItemCode)
		BEGIN
			IF (((SELECT ISNULL(SUM(numOnHand),0) FROM WareHouseItems WHERE numItemID = @numItemCode AND numDomainId=@numDomainID) = 0) AND ((SELECT ISNULL(bitVirtualInventory,0) FROM Item  WHERE numItemCode = @numItemCode AND numDomainId=@numDomainID) = 0))
			BEGIN
				UPDATE Item SET monAverageCost=@monAverageCost  where numItemCode=@numItemCode  AND numDomainID=@numDomainID	
			END
		END
	
		--If Average Cost changed then add in Tracking
		IF ISNULL(@monAverageCost,0) != ISNULL(@monOldAverageCost,0)
		BEGIN
			DECLARE @numWareHouseItemID NUMERIC(9);SET @numWareHouseItemID=0
			DECLARE @numTotal int;SET @numTotal=0
			SET @i=0
			DECLARE @vcDescription AS VARCHAR(100);SET @vcDescription=''
		
			SELECT @numTotal=COUNT(*) FROM WareHouseItems where numItemID=@numItemCode AND numDomainId=@numDomainId
	     
			WHILE @i < @numTotal
			BEGIN
				SELECT @numWareHouseItemID = min(numWareHouseItemID) FROM WareHouseItems 
					WHERE numItemID=@numItemCode AND numDomainId=@numDomainId and numWareHouseItemID > @numWareHouseItemID 
				
				IF @numWareHouseItemID>0
				BEGIN
					SET @vcDescription='Average Cost Changed Manually OLD : ' + CAST(ISNULL(@monOldAverageCost,0) AS VARCHAR(20))
				
					DECLARE @numDomain AS NUMERIC(18,0)
					SET @numDomain = @numDomainID
					EXEC dbo.USP_ManageWareHouseItems_Tracking
						@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)		
						@numReferenceID = @numItemCode, --  numeric(9, 0)
						@tintRefType = 1, --  tinyint
						@vcDescription = @vcDescription, --  varchar(100)
						@tintMode = 0, --  tinyint
						@numModifiedBy = @numUserCntID, --  numeric(9, 0)
						@numDomainID = @numDomain
				END
		    
				SET @i = @i + 1
			END
		END
 
		DECLARE @bitCartFreeShipping as  bit 
		SELECT @bitCartFreeShipping = bitFreeShipping FROM dbo.CartItems where numItemCode = @numItemCode  AND numDomainID = @numDomainID 
 
		IF @bitCartFreeShipping <> @bitFreeshipping
		BEGIN
			UPDATE dbo.CartItems SET bitFreeShipping = @bitFreeshipping WHERE numItemCode = @numItemCode  AND numDomainID=@numDomainID
		END 
 
		IF @intWebApiId > 1 
		BEGIN
			SELECT 
				@cnt = COUNT([numItemID])
			FROM 
				[ItemAPI]
			WHERE 
				[WebApiId] = @intWebApiId
				AND [numDomainId] = @numDomainID
				AND [vcAPIItemID] = @vcApiItemId

			IF @cnt > 0
			BEGIN
				--update lastUpdated Date 
				UPDATE [ItemAPI] SET [dtModified] =GETUTCDATE() WHERE [WebApiId] =@intWebApiId AND [numDomainId] = @numDomainID
			END
			ELSE --Insert ItemAPI mapping
			BEGIN
				INSERT INTO [ItemAPI] 
				(
	 				[WebApiId],
	 				[numDomainId],
	 				[numItemID],
	 				[vcAPIItemID],
	 				[numCreatedby],
	 				[dtCreated],
	 				[numModifiedby],
	 				[dtModified],vcSKU
				 )
				 VALUES 
				 (
					@intWebApiId,
					@numDomainID,
					@numItemCode,
					@vcApiItemId,
	 				@numUserCntID,
	 				GETUTCDATE(),
	 				@numUserCntID,
	 				GETUTCDATE(),@vcSKU
	 			) 
			END
		END   
		                                                                            
		-- kishan It is necessary to add item into itemTax table . 
		IF	@bitTaxable = 1
		BEGIN
			IF NOT EXISTS(SELECT * FROM dbo.ItemTax WHERE numItemCode = @numItemCode AND numTaxItemID = 0)
			BEGIN
			    INSERT INTO dbo.ItemTax 
				(
					numItemCode,
					numTaxItemID,
					bitApplicable
				)
				VALUES 
				( 
					@numItemCode,
					0,
					1 
				) 						
			END
		END	 
      
		IF @charItemType='S' or @charItemType='N'                                                                       
		BEGIN
			DELETE FROM WareHouseItmsDTL where numWareHouseItemID in (select numWareHouseItemID from WareHouseItems where numItemID=@numItemCode AND [WareHouseItems].[numDomainID] = @numDomainID)
			DELETE FROM WareHouseItems where numItemID=@numItemCode AND [WareHouseItems].[numDomainID] = @numDomainID AND numWareHouseItemID NOT IN (SELECT OI.numWarehouseItmsID FROM OpportunityItems OI WHERE OI.numItemCode=@numItemCode)
		END                                      
   
		IF @bitKitParent=1 OR @bitAssembly = 1              
		BEGIN              
			IF DATALENGTH(ISNULL(@strChildItems,'')) > 0
			BEGIN
				SET @strChildItems = CONCAT('<?xml version="1.0" encoding="iso-8859-1"?>',@strChildItems)
			END

			DECLARE @hDoc1 AS INT                                            
			EXEC sp_xml_preparedocument @hDoc1 OUTPUT, @strChildItems                                                                        
                                             
			UPDATE 
				ItemDetails 
			SET
				numQtyItemsReq=X.QtyItemsReq
				,numUOMId=X.numUOMId
				,vcItemDesc=X.vcItemDesc
				,sintOrder=X.sintOrder                                                                                          
			From 
			(
				SELECT 
					QtyItemsReq,ItemKitID,ChildItemID,numUOMId,vcItemDesc,sintOrder, numItemDetailID As ItemDetailID                             
				FROM 
					OPENXML(@hDoc1,'/NewDataSet/Table1',2)                                                                         
				WITH
				(
					ItemKitID NUMERIC(9),                                                          
					ChildItemID NUMERIC(9),                                                                        
					QtyItemsReq DECIMAL(30, 16),
					numUOMId NUMERIC(9),
					vcItemDesc VARCHAR(1000),
					sintOrder int,
					numItemDetailID NUMERIC(18,0)
				)
			)X                                                                         
			WHERE 
				numItemKitID=X.ItemKitID 
				AND numChildItemID=ChildItemID 
				AND numItemDetailID = X.ItemDetailID

			EXEC sp_xml_removedocument @hDoc1
		 END   
		 ELSE
		 BEGIN
 			DELETE FROM ItemDetails WHERE numItemKitID=@numItemCode
		 END                                                
	END

	IF ISNULL(@numItemGroup,0) = 0 OR ISNULL(@bitMatrix,0) = 1
	BEGIN
		UPDATE WareHouseItems SET monWListPrice = @monListPrice, vcWHSKU=@vcSKU, vcBarCode=@numBarCodeId WHERE numDomainID = @numDomainID AND numItemID = @numItemCode
	END

	-- SAVE MATRIX ITEM ATTRIBUTES
	IF @numItemCode > 0 AND ISNULL(@bitAttributesChanged,0)=1 AND ISNULL(@bitMatrix,0) = 1 AND LEN(ISNULL(@vcItemAttributes,'')) > 0 AND ISNULL(@bitOppOrderExists,0) <> 1 AND (SELECT COUNT(*) FROM @TempTable) > 0
	BEGIN
		-- DELETE PREVIOUS ITEM ATTRIBUTES
		DELETE FROM ItemAttributes WHERE numDomainID=@numDomainID AND numItemCode=@numItemCode

		-- INSERT NEW ITEM ATTRIBUTES
		INSERT INTO ItemAttributes
		(
			numDomainID
			,numItemCode
			,FLD_ID
			,FLD_Value
		)
		SELECT
			@numDomainID
			,@numItemCode
			,Fld_ID
			,Fld_Value
		FROM
			@TempTable

		IF (SELECT COUNT(*) FROM WarehouseItems WHERE numDomainID=@numDomainID AND numItemID=@numItemCode) > 0
		BEGIN

			-- DELETE PREVIOUS WAREHOUSE ATTRIBUTES
			DELETE FROM CFW_Fld_Values_Serialized_Items WHERE RecId IN (SELECT ISNULL(numWarehouseItemID,0) FROM WarehouseItems WHERE numDomainID=@numDomainID AND numItemID=@numItemCode)

			-- INSERT NEW WAREHOUSE ATTRIBUTES
			INSERT INTO CFW_Fld_Values_Serialized_Items
			(
				Fld_ID,
				Fld_Value,
				RecId,
				bitSerialized
			)                                                  
			SELECT 
				Fld_ID,
				ISNULL(CAST(Fld_Value AS VARCHAR),'') AS Fld_Value,
				TEMPWarehouse.numWarehouseItemID,
				0 
			FROM 
				@TempTable  
			OUTER APPLY
			(
				SELECT
					numWarehouseItemID
				FROM 
					WarehouseItems 
				WHERE 
					numDomainID=@numDomainID 
					AND numItemID=@numItemCode
			) AS TEMPWarehouse
		END
	END	  
COMMIT

	IF @numItemCode > 0
	BEGIN
		DELETE 
			IC
		FROM 
			dbo.ItemCategory IC
		INNER JOIN 
			Category 
		ON 
			IC.numCategoryID = Category.numCategoryID
		WHERE 
			numItemID = @numItemCode		
			AND numCategoryProfileID = @numCategoryProfileID

		IF @vcCategories <> ''	
		BEGIN
			INSERT INTO ItemCategory( numItemID , numCategoryID )  SELECT  @numItemCode AS numItemID,ID from dbo.SplitIDs(@vcCategories,',')	
		END
	END
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH        
END
/****** Object:  StoredProcedure [dbo].[USP_OPPBizDocItems]    Script Date: 07/26/2008 16:20:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                                                                                                                                                        
-- exec USP_OPPBizDocItems @numOppId=12388,@numOppBizDocsId=11988,@numDomainID=110
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_oppbizdocitems' ) 
    DROP PROCEDURE usp_oppbizdocitems
GO
CREATE PROCEDURE [dbo].[USP_OPPBizDocItems]
    (
      @numOppId AS NUMERIC(9) = NULL,
      @numOppBizDocsId AS NUMERIC(9) = NULL,
      @numDomainID AS NUMERIC(9) = 0      
    )
AS 
    DECLARE @DivisionID AS NUMERIC(9)      
    DECLARE @tintTaxOperator AS TINYINT   
                                                                                                                                                                                                                                                                                                                                          
    SELECT  @DivisionID = numDivisionID, @tintTaxOperator = [tintTaxOperator]           
    FROM    OpportunityMaster
    WHERE   numOppId = @numOppId                                                                                                                                                                          
--Decimal points to show in bizdoc 
    DECLARE @DecimalPoint AS TINYINT
    SELECT  @DecimalPoint = tintDecimalPoints
    FROM    domain
    WHERE   numDomainID = @numDomainID
                                                                                          
    DECLARE @strSQL AS VARCHAR(MAX)                                                                                          
    DECLARE @strSQL1 AS VARCHAR(8000)                                       
    DECLARE @strSQLCusFields VARCHAR(1000)                      
    DECLARE @strSQLEmpFlds VARCHAR(500)                                                                         
    SET @strSQLCusFields = ''                                                                                          
    SET @strSQLEmpFlds = ''                                                                                          
    DECLARE @tintType AS TINYINT   
    DECLARE @tintOppType AS TINYINT                                                      
    DECLARE @intRowNum AS INT                                                  
    DECLARE @numFldID AS VARCHAR(15)                                          
    DECLARE @vcFldname AS VARCHAR(50)                                            
    DECLARE @numBizDocId AS NUMERIC(9)
    DECLARE @numBizDocTempID AS NUMERIC(9) ;
        SET @numBizDocTempID = 0
                                           
    SELECT  @numBizDocTempID = ISNULL(numBizDocTempID, 0),
            @numBizDocId = numBizDocId
    FROM    OpportunityBizDocs
    WHERE   numOppBizDocsId = @numOppBizDocsId
    SELECT  @tintType = tintOppType,
            @tintOppType = tintOppType
    FROM    OpportunityMaster
    WHERE   numOppID = @numOppId                                                                                          
                                                                                   
    IF @tintType = 1 
        SET @tintType = 7
    ELSE 
        SET @tintType = 8                                                               
                                                                                       

    SELECT  *
    INTO    #Temp1
    FROM    ( SELECT   Opp.vcitemname AS vcItemName,Opp.numCost,Opp.vcNotes as txtNotes,
                        ISNULL(OBD.numOppBizDocItemID, 0) OppBizDocItemID,
                        CASE WHEN charitemType = 'P' THEN 'Product'
                             WHEN charitemType = 'S' THEN 'Service'
                        END AS charitemType,
                        CASE WHEN charitemType = 'P' THEN 'Inventory'
	                         WHEN charitemType = 'N' THEN 'Non-Inventory'
                             WHEN charitemType = 'S' THEN 'Service'
                        END AS vcItemType,
                        CASE WHEN bitShowDeptItemDesc = 0
                             THEN CONVERT(VARCHAR(2500), OBD.vcitemDesc)
                             WHEN bitShowDeptItemDesc = 1
                                  AND bitKitParent = 1
                                  AND bitAssembly = 0
                             THEN dbo.fn_PopulateKitDesc(opp.numOppId,
                                                         opp.numoppitemtCode)
                             WHEN bitShowDeptItemDesc = 1
                                  AND bitKitParent = 1
                                  AND bitAssembly = 1
                             THEN dbo.fn_PopulateAssemblyDesc(opp.numoppitemtCode, OBD.numUnitHour)
                             ELSE CONVERT(VARCHAR(2500), OBD.vcitemDesc)
                        END AS txtItemDesc, 
						ISNULL(OBD.numUnitHour,0) numOrigUnitHour,
                        dbo.fn_UOMConversion(ISNULL(i.numBaseUnit, 0),
                                             i.numItemCode, @numDomainID,
                                             ISNULL(opp.numUOMId, 0))
                        * OBD.numUnitHour AS numUnitHour,
                        CONVERT(DECIMAL(20,5), (dbo.fn_UOMConversion(ISNULL(opp.numUOMId, 0), i.numItemCode, @numDomainID, ISNULL(i.numBaseUnit, 0)) * OBD.monPrice)) monPrice,
                        CONVERT(VARCHAR, CONVERT(DECIMAL(20,5), OBD.monTotAmount)) Amount,
                        OBD.monTotAmount/*Fo calculating sum*/,
                        ISNULL(CONVERT(VARCHAR, i.monListPrice), 0) AS monListPrice,
                        i.numItemCode AS ItemCode,
                        opp.numoppitemtCode,
                        L.vcdata AS numItemClassification,
                        CASE WHEN bitTaxable = 0 THEN 'No'
                             ELSE 'Yes'
                        END AS Taxable,
                        numIncomeChartAcntId AS itemIncomeAccount,
                        'NI' AS ItemType,
                        ISNULL(GJH.numJournal_Id, 0) AS numJournalId,
                        ( SELECT TOP 1
                                    ISNULL(GJD.numTransactionId, 0)
                          FROM      General_Journal_Details GJD
                          WHERE     GJH.numJournal_Id = GJD.numJournalId
                                    AND GJD.chBizDocItems = 'NI'
                                    AND GJD.numoppitemtCode = opp.numoppitemtCode
                        ) AS numTransactionId,
                        ISNULL(Opp.vcModelID, '') vcModelID,
                        (CASE WHEN charitemType = 'P' THEN dbo.USP_GetAttributes(OBD.numWarehouseItmsID,bitSerialized) ELSE ISNULL(OBD.vcAttributes,0) END) AS vcAttributes,
                        ISNULL(vcPartNo, '') AS vcPartNo,
                        ( ISNULL(OBD.monTotAmtBefDiscount, OBD.monTotAmount)
                          - OBD.monTotAmount ) AS DiscAmt,
						(CASE WHEN OBD.numUnitHour = 0 THEN 0 ELSE CONVERT(DECIMAL(20,5),ISNULL(OBD.monTotAmount,0) / OBD.numUnitHour) END) As monUnitSalePrice,
                        OBD.vcNotes,
                        OBD.vcTrackingNo,
                        OBD.vcShippingMethod,
                        OBD.monShipCost,
                        [dbo].[FormatedDateFromDate](OBD.dtDeliveryDate,
                                                     @numDomainID) dtDeliveryDate,
                        Opp.numWarehouseItmsID,
                        ISNULL(u.vcUnitName, '') numUOMId,
                        ISNULL(Opp.vcManufacturer, '') AS vcManufacturer,
                        ISNULL(V.monCost, 0) AS VendorCost,
                        Opp.vcPathForTImage,
                        i.[fltLength],
                        i.[fltWidth],
                        i.[fltHeight],
                        (CASE 
							WHEN ISNULL(bitKitParent,0)=1 
									AND (CASE 
											WHEN EXISTS (SELECT ID.numItemDetailID FROM ItemDetails ID INNER JOIN Item IInner ON ID.numChildItemID=IInner.numItemCode WHERE ID.numItemKitID=I.numItemCode AND ISNULL(IInner.bitKitParent,0)=1)
											THEN 1
											ELSE 0
										END) = 0
							THEN 
								dbo.GetKitWeightBasedOnItemSelection(I.numItemCode,opp.vcChildKitSelectedItems)
							ELSE 
								ISNULL(fltWeight,0) 
						END) [fltWeight],
                        ISNULL(I.numVendorID, 0) numVendorID,
                        Opp.bitDropShip AS DropShip,
                        SUBSTRING(( SELECT  ' ,' + vcSerialNo
                                            + CASE WHEN ISNULL(I.bitLotNo, 0) = 1
                                                   THEN ' ('
                                                        + CONVERT(VARCHAR(15), oppI.numQty)
                                                        + ')'
                                                   ELSE ''
                                              END
                                    FROM    OppWarehouseSerializedItem oppI
                                            JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID = whi.numWareHouseItmsDTLID
                                    WHERE   oppI.numOppID = Opp.numOppId
                                            AND oppI.numOppItemID = Opp.numoppitemtCode
                                    ORDER BY vcSerialNo
                                  FOR
                                    XML PATH('')
                                  ), 3, 200000) AS SerialLotNo,
                        ISNULL(opp.numUOMId, 0) AS numUOM,
                        ISNULL(u.vcUnitName, '') vcUOMName,
                        dbo.fn_UOMConversion(opp.numUOMId, Opp.numItemCode,
                                             @numDomainID, NULL) AS UOMConversionFactor,
                        ISNULL(Opp.numSOVendorId, 0) AS numSOVendorId,
                        [dbo].FormatedDateTimeFromDate(OBD.dtRentalStartDate,
                                                       @numDomainID) dtRentalStartDate,
                        [dbo].FormatedDateTimeFromDate(OBD.dtRentalReturnDate,
                                                       @numDomainID) dtRentalReturnDate,
                        ISNULL(W.vcWareHouse, '') AS Warehouse,
						(CASE WHEN ISNULL(WI.numWareHouseItemID,0) > 0 THEN CONCAT(W.vcWareHouse,CASE WHEN WL.numWLocationID > 0 THEN CONCAT('(',WL.vcLocation ,')') ELSE '' END) ELSE '' END) vcWarehouse,
					--	ISNULL(W.vcWareHouse, '') AS vcWareHouse,
                        CASE WHEN charitemType = 'P' THEN WI.vcWHSKU
                             ELSE i.vcSKU
                        END vcSKU,
                        CASE WHEN charitemType = 'P' THEN WI.vcBarCode
                             ELSE i.numBarCodeId
                        END vcBarcode,
                        ISNULL(opp.numClassID, 0) AS numClassID,
                        ISNULL(opp.numProjectID, 0) AS numProjectID,
                        ISNULL(I.[numShipClass],0) AS numShipClass,OBD.numOppBizDocID,WL.vcLocation,ISNULL(bitSerialized,0) AS bitSerialized,ISNULL(bitLotNo,0) AS bitLotNo,
                        opp.numOppId,OBD.numUnitHour AS numUnitHourOrig,ISNULL(I.numItemCode,0) AS [numItemCode], 
						ISNULL(opp.vcPromotionDetail, '') AS vcPromotionDetail

						, CASE WHEN ISNULL(opp.ItemReleaseDate,'') = '' THEN
							 ISNULL(OM.dtReleaseDate,'') 
						ELSE opp.ItemReleaseDate 
						END AS ItemRequiredDate,
						ISNULL(SOLIPL.numPurchaseOrderID,0) numPOID,ISNULL(SOLIPL.numPurchaseOrderItemID,0) numPOItemID,ISNULL(OMPO.vcPOppName,'') vcPOName
						,ISNULL(opp.vcAttrValues,'') vcAttrValues
              FROM      OpportunityItems opp
                        JOIN OpportunityBizDocItems OBD ON OBD.numOppItemID = opp.numoppitemtCode
                        LEFT JOIN item i ON opp.numItemCode = i.numItemCode
                        LEFT JOIN ListDetails L ON i.numItemClassification = L.numListItemID
                        LEFT JOIN Vendor V ON V.numVendorID = @DivisionID
                                              AND V.numItemCode = opp.numItemCode
                        LEFT JOIN General_Journal_Header GJH ON opp.numoppid = GJH.numOppId
                                                                AND GJH.numBizDocsPaymentDetId IS NULL
                                                                AND GJH.numOppBizDocsId = @numOppBizDocsId
                        LEFT JOIN UOM u ON u.numUOMId = opp.numUOMId                                                                       
                        LEFT JOIN dbo.WareHouseItems WI ON opp.numWarehouseItmsID = WI.numWareHouseItemID
                                                           AND WI.numDomainID = @numDomainID
                        LEFT JOIN dbo.Warehouses W ON WI.numDomainID = W.numDomainID
                                                      AND WI.numWareHouseID = W.numWareHouseID
                        LEFT JOIN dbo.WarehouseLocation WL ON WL.numWLocationID = WI.numWLocationID
						LEFT JOIN OpportunityMaster OM ON opp.numOppId = OM.numOppId
						LEFT JOIN SalesOrderLineItemsPOLinking SOLIPL ON opp.numOppId=SOLIPL.numSalesOrderID AND opp.numoppitemtCode=SOLIPL.numSalesOrderItemID
						LEFT JOIN OpportunityMaster OMPO ON SOLIPL.numPurchaseOrderID = OMPO.numOppId
              WHERE     opp.numOppId = @numOppId
                        AND ( bitShowDeptItem IS NULL
                              OR bitShowDeptItem = 0
                            )
                        AND OBD.numOppBizDocID = @numOppBizDocsId
            ) X


    --IF @DecimalPoint = 1 
    --    BEGIN
    --        UPDATE  #Temp1
    --        SET     monPrice = CONVERT(DECIMAL(18, 1), monPrice),
    --                Amount = CONVERT(DECIMAL(18, 1), Amount)
    --    END
    --IF @DecimalPoint = 2 
    --    BEGIN
    --        UPDATE  #Temp1
    --        SET     monPrice = CONVERT(DECIMAL(18, 2), monPrice),
    --                Amount = CONVERT(DECIMAL(18, 2), Amount)
    --    END
    --IF @DecimalPoint = 3 
    --    BEGIN
    --        UPDATE  #Temp1
    --        SET     monPrice = CONVERT(DECIMAL(18, 3), monPrice),
    --                Amount = CONVERT(DECIMAL(18, 3), Amount)
    --    END
    --IF @DecimalPoint = 4 
    --    BEGIN
    --        UPDATE  #Temp1
    --        SET     monPrice = CONVERT(DECIMAL(20,5), monPrice),
    --                Amount = CONVERT(DECIMAL(20,5), Amount)
    --    END

    DECLARE @strSQLUpdate AS VARCHAR(2000);SET @strSQLUpdate=''
   
    EXEC ('ALTER TABLE #Temp1 ADD [TotalTax] DECIMAL(20,5)')
	EXEC ('ALTER TABLE #Temp1 ADD [CRV] DECIMAL(20,5)')

    IF @tintTaxOperator = 1 --add Sales tax
	BEGIN
        SET @strSQLUpdate = @strSQLUpdate
            + ',[TotalTax]= dbo.fn_CalBizDocTaxAmt('
            + CONVERT(VARCHAR(20), @numDomainID) + ',0,'
            + CONVERT(VARCHAR(20), @numOppId) +','+
            + 'numoppitemtCode,1,Amount,numUnitHour)'

		SET @strSQLUpdate = @strSQLUpdate
            + ',[CRV]= dbo.fn_CalBizDocTaxAmt('
            + CONVERT(VARCHAR(20), @numDomainID) + ',1,'
            + CONVERT(VARCHAR(20), @numOppId) +','+
            + 'numoppitemtCode,1,Amount,numUnitHour)'
	END
    ELSE IF @tintTaxOperator = 2 -- remove sales tax
	BEGIN
        SET @strSQLUpdate = @strSQLUpdate + ',[TotalTax]= 0'
		SET @strSQLUpdate = @strSQLUpdate + ',[CRV]= 0'
	END
    ELSE 
	BEGIN
        SET @strSQLUpdate = @strSQLUpdate
            + ',[TotalTax]= dbo.fn_CalBizDocTaxAmt('
            + CONVERT(VARCHAR(20), @numDomainID) + ',0,'
            + CONVERT(VARCHAR(20), @numOppId) +','+
            + 'numoppitemtCode,1,Amount,numUnitHour)'

		SET @strSQLUpdate = @strSQLUpdate
            + ',[CRV]= dbo.fn_CalBizDocTaxAmt('
            + CONVERT(VARCHAR(20), @numDomainID) + ',1,'
            + CONVERT(VARCHAR(20), @numOppId) +','+
            + 'numoppitemtCode,1,Amount,numUnitHour)'
	END


	DECLARE @vcTaxName AS VARCHAR(100)
    DECLARE @numTaxItemID AS NUMERIC(9)
    SET @numTaxItemID = 0
    SELECT TOP 1
            @vcTaxName = vcTaxName,
            @numTaxItemID = numTaxItemID
    FROM    TaxItems
    WHERE   numDomainID = @numDomainID


    WHILE @numTaxItemID > 0
    BEGIN
        EXEC ('ALTER TABLE #Temp1 ADD [' + @vcTaxName + '] DECIMAL(20,5)')

        SET @strSQLUpdate = @strSQLUpdate + ',[' + @vcTaxName
            + ']= dbo.fn_CalBizDocTaxAmt('
            + CONVERT(VARCHAR(20), @numDomainID) + ','
            + CONVERT(VARCHAR(20), @numTaxItemID) + ','
            + CONVERT(VARCHAR(20), @numOppId) +','+
            + 'numoppitemtCode,1,Amount,numUnitHour)'
       

        SELECT TOP 1
                @vcTaxName = vcTaxName,
                @numTaxItemID = numTaxItemID
        FROM    TaxItems
        WHERE   numDomainID = @numDomainID
                AND numTaxItemID > @numTaxItemID

        IF @@rowcount = 0 
            SET @numTaxItemID = 0
    END

    IF @strSQLUpdate <> '' 
    BEGIN
        SET @strSQLUpdate = 'UPDATE #Temp1 SET ItemCode=ItemCode' + @strSQLUpdate + ' WHERE ItemCode > 0'
        EXEC (@strSQLUpdate)
    END

    DECLARE @vcDbColumnName NVARCHAR(50)
    SELECT TOP 1
            @numFldID = numfieldID,
            @vcFldname = vcFieldName,
            @vcDbColumnName = vcDbColumnName,
            @intRowNum = ( tintRow + 1 )
    FROM    View_DynamicCustomColumns
    WHERE   numFormID = @tintType
            AND Grp_id = 5
            AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId
            AND bitCustom = 1
            AND ISNULL(numRelCntType, 0) = @numBizDocTempID
    ORDER BY tintRow                                                                                          
    WHILE @intRowNum > 0                                                                                  
        BEGIN    
            EXEC
                ( 'alter table #Temp1 add [' + @vcDbColumnName
                  + '] varchar(100)'
                )
	
            SET @strSQLUpdate = 'update #Temp1 set ItemCode=ItemCode,['
                + @vcDbColumnName + ']=dbo.GetCustFldValueItem(' + @numFldID
                + ',ItemCode) where ItemCode>0'
            EXEC ( @strSQLUpdate
                )
		                   
            SELECT TOP 1
                    @numFldID = numfieldID,
                    @vcFldname = vcFieldName,
                    @vcDbColumnName = vcDbColumnName,
                    @intRowNum = ( tintRow + 1 )
            FROM    View_DynamicCustomColumns
            WHERE   numFormID = @tintType
                    AND Grp_id = 5
                    AND numDomainID = @numDomainID
                    AND numAuthGroupID = @numBizDocId
                    AND bitCustom = 1
                    AND tintRow >= @intRowNum
                    AND ISNULL(numRelCntType, 0) = @numBizDocTempID
            ORDER BY tintRow                                                                  
		           
            IF @@rowcount = 0 
                SET @intRowNum = 0                                                                                          
        END


    SELECT  Row_NUmber() OVER ( ORDER BY numoppitemtCode ) RowNumber,
            *
    FROM    #Temp1
    ORDER BY numoppitemtCode ASC 

    DROP TABLE #Temp1

    SELECT  ISNULL(tintOppStatus, 0)
    FROM    OpportunityMaster
    WHERE   numOppID = @numOppId                                                                                                 


IF (SELECT COUNT(*) FROM View_DynamicColumns WHERE   numFormID = @tintType AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId AND vcFieldType = 'R'AND ISNULL(numRelCntType, 0) = @numBizDocTempID)=0
BEGIN            
	INSERT INTO DycFormConfigurationDetails (numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numAuthGroupID,numRelCntType)
	select numFormId,numFieldId,1,tintRow,@numDomainID,@numBizDocId,@numBizDocTempID
	 FROM View_DynamicDefaultColumns
	 where numFormId=@tintType and bitDefault=1 and numDomainID=@numDomainID order by tintRow ASC	
	 
	 
	 IF(SELECT COUNT(*) FROM DycFrmConfigBizDocsSumm Ds where numBizDocID=@numBizDocId and Ds.numFormID=@tintType 
	and isnull(Ds.numBizDocTempID,0)=@numBizDocTempID and Ds.numDomainID=@numDomainID)=0
	BEGIN
		INSERT INTO DycFrmConfigBizDocsSumm (numFormID,numBizDocID,numFieldID,numDomainID,numBizDocTempID)
		 SELECT @tintType,@numBizDocId,DFM.numFieldID,@numDomainID,@numBizDocTempID FROM
		 DycFormField_Mapping DFFM 
			JOIN DycFieldMaster DFM on DFFM.numModuleID=DFFM.numModuleID and DFFM.numFieldID=DFM.numFieldID
			where numFormID=16 and DFFM.numDomainID is null
	END

END      


    SELECT  vcFieldName AS vcFormFieldName,
            vcDbColumnName,
            vcFieldDataType,
            tintRow AS intRowNum
    FROM    View_DynamicColumns
    WHERE   numFormID = @tintType
            AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId
            AND vcFieldType = 'R'
            AND ISNULL(numRelCntType, 0) = @numBizDocTempID
    UNION
    SELECT  vcFieldName AS vcFormFieldName,
            vcDbColumnName,
            'C' vcFieldDataType,
            tintRow AS intRowNum
    FROM    View_DynamicCustomColumns
    WHERE   numFormID = @tintType
            AND Grp_id = 5
            AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId
            AND bitCustom = 1
            AND ISNULL(numRelCntType, 0) = @numBizDocTempID
    ORDER BY tintRow
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_OPPBizDocItems_GetWithKitChilds' ) 
    DROP PROCEDURE USP_OPPBizDocItems_GetWithKitChilds
GO
CREATE PROCEDURE [dbo].[USP_OPPBizDocItems_GetWithKitChilds]
(
    @numOppId AS NUMERIC(9) = NULL,
    @numOppBizDocsId AS NUMERIC(9) = NULL,
    @numDomainID AS NUMERIC(9) = 0      
)
AS 
BEGIN
    DECLARE @DivisionID AS NUMERIC(9)      
    DECLARE @tintTaxOperator AS TINYINT
	DECLARE @tintCommitAllocation TINYINT
	SELECT @tintCommitAllocation=ISNULL(tintCommitAllocation,1) FROM Domain WHERE numDomainId=@numDomainID
                                                                                                                                                                                                                                                                                                                                          
    SELECT  
		@DivisionID = numDivisionID, 
		@tintTaxOperator = [tintTaxOperator]           
    FROM    
		OpportunityMaster
    WHERE   
		numOppId = @numOppId                                                                                                                                                                          

    
                                                                                          
    DECLARE @strSQL AS VARCHAR(MAX)                                                                                          
    DECLARE @strSQL1 AS VARCHAR(8000)                                       
    DECLARE @strSQLCusFields VARCHAR(1000) = ''
    DECLARE @strSQLEmpFlds VARCHAR(500) = ''                                                                         
                                                                                              
    DECLARE @tintType AS TINYINT   
    DECLARE @tintOppType AS TINYINT                                                      
    DECLARE @intRowNum AS INT                                                  
    DECLARE @numFldID AS VARCHAR(15)                                          
    DECLARE @vcFldname AS VARCHAR(50)                                            
    DECLARE @numBizDocId AS NUMERIC(9)
    DECLARE @numBizDocTempID AS NUMERIC(9) = 0
	DECLARE @bitDisplayKitChild AS BIT = 0
        
                                           
    SELECT  
		@numBizDocTempID = ISNULL(numBizDocTempID, 0),
        @numBizDocId = numBizDocId
    FROM    
		OpportunityBizDocs
    WHERE   
		numOppBizDocsId = @numOppBizDocsId

	
	SELECT
		@bitDisplayKitChild = ISNULL(bitDisplayKitChild,0)
	FROM	
		BizDocTemplate
	WHERE
		numDomainID = @numDomainID
		AND numBizDocTempID = @numBizDocTempID


    SELECT  
		@tintType = tintOppType,
        @tintOppType = tintOppType
    FROM    
		OpportunityMaster
    WHERE   
		numOppID = @numOppId                                                                                          
                                                                                   
    IF @tintType = 1 
        SET @tintType = 7
    ELSE 
        SET @tintType = 8     
		
		
	IF (SELECT 
			COUNT(*) 
		FROM 
			View_DynamicColumns 
		WHERE   
			numFormID = @tintType 
			AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId 
			AND vcFieldType = 'R'
			AND ISNULL(numRelCntType, 0) = @numBizDocTempID
		)=0
	BEGIN            
		INSERT INTO DycFormConfigurationDetails 
		(
			numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numAuthGroupID,numRelCntType
		)
		SELECT 
			numFormId,numFieldId,1,tintRow,@numDomainID,@numBizDocId,@numBizDocTempID
		FROM 
			View_DynamicDefaultColumns
		WHERE 
			numFormId=@tintType and bitDefault=1 and numDomainID=@numDomainID order by tintRow ASC	
	 
	 
		IF (SELECT 
				COUNT(*) 
			FROM 
				DycFrmConfigBizDocsSumm Ds 
			WHERE 
				numBizDocID=@numBizDocId 
				AND Ds.numFormID=@tintType 
				AND ISNULL(Ds.numBizDocTempID,0)=@numBizDocTempID 
				AND Ds.numDomainID=@numDomainID
			)=0
		BEGIN
			INSERT INTO DycFrmConfigBizDocsSumm 
			(
				numFormID,numBizDocID,numFieldID,numDomainID,numBizDocTempID
			)
			SELECT 
				@tintType,@numBizDocId,DFM.numFieldID,@numDomainID,@numBizDocTempID 
			FROM
				DycFormField_Mapping DFFM 
			JOIN 
				DycFieldMaster DFM 
			ON 
				DFFM.numModuleID=DFFM.numModuleID and DFFM.numFieldID=DFM.numFieldID
			WHERE 
				numFormID=16 
				AND DFFM.numDomainID IS NULL
		END
	END      

	IF OBJECT_ID('tempdb..#TempBizDocProductGridColumns') IS NOT NULL DROP TABLE #TempBizDocProductGridColumns
	SELECT * INTO #TempBizDocProductGridColumns FROM (
    SELECT  
		vcFieldName AS vcFormFieldName,
        vcDbColumnName,
        vcFieldDataType,
        tintRow AS intRowNum
    FROM    
		View_DynamicColumns
    WHERE   
		numFormID = @tintType
        AND numDomainID = @numDomainID
        AND numAuthGroupID = @numBizDocId
        AND vcFieldType = 'R'
        AND ISNULL(numRelCntType, 0) = @numBizDocTempID
    UNION
    SELECT  
		vcFieldName AS vcFormFieldName,
        vcDbColumnName,
        'C' vcFieldDataType,
        tintRow AS intRowNum
    FROM    
		View_DynamicCustomColumns
    WHERE   
		numFormID = @tintType
        AND Grp_id = 5
        AND numDomainID = @numDomainID
        AND numAuthGroupID = @numBizDocId
        AND bitCustom = 1
        AND ISNULL(numRelCntType, 0) = @numBizDocTempID
    ) t1   
	ORDER BY 
		intRowNum                                                                              
                                                                                
    SELECT  
		*
    INTO    
		#Temp1
    FROM    
	( 
		SELECT   
			Opp.vcitemname AS vcItemName,
            ISNULL(OBD.numOppBizDocItemID, 0) OppBizDocItemID,
			(CASE 
				WHEN OM.tintOppType = 1 AND OM.tintOppStatus = 1 
				THEN 
					(CASE 
						WHEN (I.charItemType = 'p' AND ISNULL(Opp.bitDropShip, 0) = 0 AND ISNULL(I.bitAsset,0)=0)
						THEN 
							CAST(dbo.CheckOrderItemInventoryStatus(Opp.numItemCode,Opp.numUnitHour,Opp.numoppitemtCode,Opp.numWarehouseItmsID,(CASE WHEN ISNULL(bitKitParent,0) = 1 AND ISNULL(bitAssembly,0) = 1 THEN 0  WHEN ISNULL(bitKitParent,0) = 1 THEN 1 ELSE 0 END)) AS VARCHAR)
						ELSE ''
					END) 
				ELSE ''
			END) vcBackordered,
			ISNULL(WI.numBackOrder,0) AS numBackOrder,
            CASE WHEN charitemType = 'P' THEN 'Product'
                    WHEN charitemType = 'S' THEN 'Service'
            END AS charitemType,
            CASE WHEN charitemType = 'P' THEN 'Inventory'
	                WHEN charitemType = 'N' THEN 'Non-Inventory'
                    WHEN charitemType = 'S' THEN 'Service'
            END AS vcItemType,
			CONVERT(VARCHAR(2500), OBD.vcitemDesc) AS txtItemDesc, 
			opp.numUnitHour AS numOrgTotalUnitHour,
			dbo.fn_UOMConversion(ISNULL(i.numBaseUnit, 0),i.numItemCode, @numDomainID,ISNULL(opp.numUOMId, 0)) * opp.numUnitHour AS numTotalUnitHour,                                   
            dbo.fn_UOMConversion(ISNULL(i.numBaseUnit, 0),i.numItemCode, @numDomainID,ISNULL(opp.numUOMId, 0)) * OBD.numUnitHour AS numUnitHour,
            CONVERT(DECIMAL(20,5), (dbo.fn_UOMConversion(ISNULL(opp.numUOMId, 0), i.numItemCode, @numDomainID, ISNULL(i.numBaseUnit, 0)) * OBD.monPrice)) monPrice,
            CONVERT(VARCHAR, CONVERT(DECIMAL(20,5), OBD.monTotAmount)) Amount,
            OBD.monTotAmount/*Fo calculating sum*/,
            ISNULL(CONVERT(VARCHAR, i.monListPrice), 0) AS monListPrice,
            i.numItemCode AS ItemCode,
            opp.numoppitemtCode,
            L.vcdata AS numItemClassification,
            CASE WHEN bitTaxable = 0 THEN 'No'
                    ELSE 'Yes'
            END AS Taxable,
            numIncomeChartAcntId AS itemIncomeAccount,
            'NI' AS ItemType,
            ISNULL(GJH.numJournal_Id, 0) AS numJournalId,
            ( SELECT TOP 1
                        ISNULL(GJD.numTransactionId, 0)
                FROM      General_Journal_Details GJD
                WHERE     GJH.numJournal_Id = GJD.numJournalId
                        AND GJD.chBizDocItems = 'NI'
                        AND GJD.numoppitemtCode = opp.numoppitemtCode
            ) AS numTransactionId,
            ISNULL(Opp.vcModelID, '') vcModelID,
            (CASE WHEN charitemType = 'P' THEN dbo.USP_GetAttributes(OBD.numWarehouseItmsID, bitSerialized) ELSE ISNULL(OBD.vcAttributes,0) END) AS vcAttributes,
            ISNULL(vcPartNo, '') AS vcPartNo,
           (CASE WHEN ISNULL(opp.bitMarkupDiscount,0) = 1 
					THEN 0
					ELSE (ISNULL(OBD.monTotAmtBefDiscount, OBD.monTotAmount) - OBD.monTotAmount ) 
			END)
            AS DiscAmt,		
			(CASE WHEN OBD.numUnitHour = 0 THEN 0 ELSE CONVERT(DECIMAL(20,5),(dbo.fn_UOMConversion(ISNULL(opp.numUOMId, 0), i.numItemCode, @numDomainID, ISNULL(i.numBaseUnit, 0)) * (ISNULL(OBD.monTotAmount,0) / OBD.numUnitHour))) END) As monUnitSalePrice,
            ISNULL(OBD.vcNotes,ISNULL(opp.vcNotes,'')) vcNotes,
            OBD.vcTrackingNo,
            OBD.vcShippingMethod,
            OBD.monShipCost,
            [dbo].[FormatedDateFromDate](OBD.dtDeliveryDate,@numDomainID) dtDeliveryDate,
            Opp.numWarehouseItmsID,
            ISNULL(u.vcUnitName, '') numUOMId,
            ISNULL(Opp.vcManufacturer, '') AS vcManufacturer,
            ISNULL(V.monCost, 0) AS VendorCost,
            Opp.vcPathForTImage,
            i.[fltLength],
            i.[fltWidth],
            i.[fltHeight],
            i.[fltWeight],
            ISNULL(I.numVendorID, 0) numVendorID,
            Opp.bitDropShip AS DropShip,
            SUBSTRING(( SELECT  ' ,' + vcSerialNo
                                + CASE WHEN ISNULL(I.bitLotNo, 0) = 1
                                        THEN ' ('
                                            + CONVERT(VARCHAR(15), oppI.numQty)
                                            + ')'
                                        ELSE ''
                                    END
                        FROM    OppWarehouseSerializedItem oppI
                                JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID = whi.numWareHouseItmsDTLID
                        WHERE   oppI.numOppID = Opp.numOppId
                                AND oppI.numOppItemID = Opp.numoppitemtCode
                        ORDER BY vcSerialNo
                        FOR
                        XML PATH('')
                        ), 3, 200000) AS SerialLotNo,
            ISNULL(opp.numUOMId, 0) AS numUOM,
            ISNULL(u.vcUnitName, '') vcUOMName,
            dbo.fn_UOMConversion(opp.numUOMId, Opp.numItemCode,
                                    @numDomainID, NULL) AS UOMConversionFactor,
            ISNULL(Opp.numSOVendorId, 0) AS numSOVendorId,
            [dbo].FormatedDateTimeFromDate(OBD.dtRentalStartDate,
                                            @numDomainID) dtRentalStartDate,
            [dbo].FormatedDateTimeFromDate(OBD.dtRentalReturnDate,
                                            @numDomainID) dtRentalReturnDate,
            ISNULL(W.vcWareHouse, '') AS Warehouse,
            CASE WHEN charitemType = 'P' AND ISNULL(numItemGroup,0) > 0 AND ISNULL(bitMatrix,0)=0 THEN ISNULL(WI.vcWHSKU,I.vcSKU) ELSE I.vcSKU END vcSKU,
            CASE WHEN charitemType = 'P' AND ISNULL(numItemGroup,0) > 0 AND ISNULL(bitMatrix,0)=0 THEN ISNULL(WI.vcBarCode,I.numBarCodeId) ELSE I.numBarCodeId END vcBarcode,
			CASE WHEN charitemType = 'P' AND ISNULL(numItemGroup,0) > 0 AND ISNULL(bitMatrix,0)=0 THEN ISNULL(WI.vcBarCode,I.numBarCodeId) ELSE I.numBarCodeId END numBarCodeId,
            ISNULL(opp.numClassID, 0) AS numClassID,
            ISNULL(opp.numProjectID, 0) AS numProjectID,
            ISNULL(I.[numShipClass],0) AS numShipClass,OBD.numOppBizDocID
			,(CASE 
				WHEN OB.numBizDocId IN (29397) 
				THEN  STUFF((
					SELECT 
						CONCAT(',',WLInner.vcLocation,' (',ISNULL(WIInner.numOnHand,0) + ISNULL(WIInner.numAllocation,0),')')
					FROM 
						WareHouseItems WIInner
					INNER JOIN
						WarehouseLocation WLInner
					ON
						WIInner.numWLocationID = WLInner.numWLocationID
					WHERE 
						WIInner.numItemID = opp.numItemCode
						AND WIInner.numWareHouseID=WI.numWareHouseID
						AND (ISNULL(WIInner.numOnHand,0) + ISNULL(WIInner.numAllocation,0)) > 0
					FOR XML PATH, TYPE).value(N'.[1]', N'nvarchar(max)'),1,1,'')
				ELSE WL.vcLocation 
			END) vcLocation
			,ISNULL(bitSerialized,0) AS bitSerialized,ISNULL(bitLotNo,0) AS bitLotNo,
            opp.numOppId,OBD.numUnitHour AS numUnitHourOrig,ISNULL(I.numItemCode,0) AS [numItemCode],
			ISNULL(I.bitKitParent,0) AS bitKitParent,
			0 AS numOppChildItemID, 
			0 AS numOppKitChildItemID,
			0 AS bitChildItem,
			0 AS tintLevel,
			ISNULL(opp.vcPromotionDetail,'') AS vcPromotionDetail,
			(CASE WHEN ISNULL(WI.numWareHouseItemID,0) > 0 THEN W.vcWareHouse ELSE '' END) vcWarehouse,
			(CASE WHEN (SELECT COUNT(*) FROM #TempBizDocProductGridColumns WHERE vcDbColumnName='vcInclusionDetails') > 0 THEN dbo.GetOrderAssemblyKitInclusionDetails(opp.numOppID,Opp.numoppitemtCode,OBD.numUnitHour,@tintCommitAllocation,1) ELSE '' END) AS vcInclusionDetails,
			Opp.numSortOrder
			,CAST(ISNULL(Opp.numCost,0) AS DECIMAL(20,5)) AS numCost, 
			ISNULL((SELECT SUM(numOnHand) FROM WarehouseItems WHERE WarehouseItems.numDomainID=@numDomainID AND WarehouseItems.numItemID=I.numItemCode AND WarehouseItems.numWarehouseID=W.numWareHouseID),0) numOnHand
			,dbo.FormatedDateFromDate(opp.ItemReleaseDate,@numDomainID) dtReleaseDate
			,(CASE WHEN I.bitKitParent = 1 THEN ISNULL(dbo.fn_GetKitAvailabilityByWarehouse(I.numItemCode,W.numWareHouseID),0) ELSE (ISNULL(numOnHand,0) + ISNULL(numAllocation,0)) END) AS numAvailable
			,CPN.CustomerPartNo
			,ISNULL(opp.bitWinningBid,0) bitWinningBid
			,ISNULL(opp.numParentOppItemID,0) numParentOppItemID
			,ISNULL(opp.numUnitHour,0) - ISNULL(TEMPBizDocQty.numBizDocQty,0) AS numRemainingQty
			,dbo.FormatedDateFromDate(opp.ItemRequiredDate,@numDomainID) ItemRequiredDate
        FROM      
			OpportunityItems opp
        JOIN 
			OpportunityBizDocItems OBD 
		ON 
			OBD.numOppItemID = opp.numoppitemtCode
		JOIN
			OpportunityBizDocs OB
		ON
			OBD.numOppBizDocID=OB.numOppBizDocsID
        LEFT JOIN 
			item i 
		ON 
			opp.numItemCode = i.numItemCode
        LEFT JOIN 
			ListDetails L 
		ON 
			i.numItemClassification = L.numListItemID
        LEFT JOIN 
			Vendor V 
		ON 
			V.numVendorID = @DivisionID
            AND V.numItemCode = opp.numItemCode
        LEFT JOIN 
			General_Journal_Header GJH 
		ON 
			opp.numoppid = GJH.numOppId
            AND GJH.numBizDocsPaymentDetId IS NULL
            AND GJH.numOppBizDocsId = @numOppBizDocsId
        LEFT JOIN 
			UOM u 
		ON 
			u.numUOMId = opp.numUOMId                                      
        LEFT JOIN 
			dbo.WareHouseItems WI 
		ON 
			opp.numWarehouseItmsID = WI.numWareHouseItemID
			AND WI.numDomainID = @numDomainID
        LEFT JOIN 
			dbo.Warehouses W 
		ON 
			WI.numDomainID = W.numDomainID
            AND WI.numWareHouseID = W.numWareHouseID
        LEFT JOIN 
			dbo.WarehouseLocation WL 
		ON 
			WL.numWLocationID = WI.numWLocationID
		LEFT JOIN
			dbo.OpportunityMaster OM
		ON 
			OM.numOppId = opp.numOppId
		LEFT JOIN	
			DivisionMaster DM
		ON
			OM.numDivisionId= DM.numDivisionID
		LEFT JOIN 
			CustomerPartNumber CPN
		ON 
			opp.numItemCode = CPN.numItemCode AND CPN.numDomainId = @numDomainID AND CPN.numCompanyId = DM.numCompanyID
		OUTER APPLY
		(
			SELECT
				SUM(OBDIInner.numUnitHour) AS numBizDocQty
			FROM
				OpportunityBizDocs OBDInner
			INNER JOIN
				OpportunityBizDocItems OBDIInner
			ON
				OBDInner.numOppBizDocsId = OBDIInner.numOppBizDocID
			WHERE
				OBDInner.numOppId = @numOppId
				AND OBDInner.numBizDocId = OB.numBizDocId
				AND OBDIInner.numOppItemID = OBD.numOppItemID
		) TEMPBizDocQty
        WHERE     
			opp.numOppId = @numOppId
            AND (bitShowDeptItem IS NULL OR bitShowDeptItem = 0)
            AND OBD.numOppBizDocID = @numOppBizDocsId
    ) X
	ORDER BY
		numSortOrder
	OPTION ( OPTIMIZE FOR (@numOppId UNKNOWN, @numOppBizDocsId UNKNOWN))

	--GET KIT CHILD ITEMS (NOW WE ARE SUPPORTING KIT WITHIN KIT BUT ONLY 1 LEVEL MEANS CHILD KIT INSIDE PARENT KIT CAN NOT HAVE ANOTHER KIT AS CHILD)
	IF (@bitDisplayKitChild=1 AND (SELECT COUNT(*) FROM OpportunityKitItems WHERE numOppId = @numOppId ) > 0)
	BEGIN
		DECLARE @TEMPTABLE TABLE(
			numOppItemCode NUMERIC(18,0),
			numOppChildItemID NUMERIC(18,0), 
			numOppKitChildItemID NUMERIC(18,0),
			numItemCode NUMERIC(18,0),
			numWarehouseItemID NUMERIC(18,0),
			numTotalQty FLOAT,
			numQty FLOAT,
			numUOMID NUMERIC(18,0),
			numQtyShipped FLOAT,
			tintLevel TINYINT,
			numSortOrder INT
		);

		--FIRST LEVEL CHILD
		INSERT INTO @TEMPTABLE
		(
			numOppItemCode,numOppChildItemID,numOppKitChildItemID,numItemCode,numWarehouseItemID,numTotalQty,numQty,numUOMID,numQtyShipped,tintLevel,numSortOrder
		)
		SELECT 
			OKI.numOppItemID,
			OKI.numOppChildItemID,
			CAST(0 AS NUMERIC(18,0)),
			OKI.numChildItemID,
			ISNULL(OKI.numWareHouseItemId,0),
			CAST((t1.numOrgTotalUnitHour * OKI.numQtyItemsReq_Orig) AS FLOAT),
			CAST((t1.numUnitHour * OKI.numQtyItemsReq_Orig) AS FLOAT),
			ISNULL(OKI.numUOMID,0),
			ISNULL(numQtyShipped,0)
			,1
			,t1.numSortOrder
		FROM 
			#Temp1 t1
		INNER JOIN
			OpportunityKitItems OKI
		ON
			t1.numoppitemtCode = OKI.numOppItemID
		WHERE
			t1.bitKitParent = 1

		--SECOND LEVEL CHILD
		INSERT INTO @TEMPTABLE
		(
			numOppItemCode,numOppChildItemID,numOppKitChildItemID,numItemCode,numWarehouseItemID,numTotalQty,numQty,numUOMID,numQtyShipped,tintLevel,numSortOrder
		)
		SELECT 
			OKCI.numOppItemID,
			OKCI.numOppChildItemID,
			OKCI.numOppKitChildItemID,
			OKCI.numItemID,
			ISNULL(OKCI.numWareHouseItemId,0),
			CAST((c.numTotalQty * OKCI.numQtyItemsReq_Orig) AS NUMERIC(18,2)),
			CAST((c.numQty * OKCI.numQtyItemsReq_Orig) AS NUMERIC(18,2)),
			ISNULL(OKCI.numUOMID,0),
			ISNULL(OKCI.numQtyShipped,0),
			2
			,c.numSortOrder
		FROM 
			OpportunityKitChildItems OKCI
		INNER JOIN
			@TEMPTABLE c
		ON
			OKCI.numOppID = @numOppId
			AND OKCI.numOppItemID = c.numOppItemCode
			AND OKCI.numOppChildItemID = c.numOppChildItemID

		INSERT INTO 
			#Temp1
		SELECT   
			(CASE WHEN t2.tintLevel = 1 THEN CONCAT('&nbsp;&nbsp;',I.vcItemName) WHEN t2.tintLevel = 2 THEN CONCAT('&nbsp;&nbsp;&nbsp;&nbsp;',I.vcItemName) ELSE I.vcItemName END) AS vcItemName,
            0 OppBizDocItemID
			,(CASE WHEN (t2.numTotalQty-ISNULL(t2.numQtyShipped,0)) > ISNULL(WI.numAllocation,0) THEN ((t2.numTotalQty-ISNULL(t2.numQtyShipped,0)) - ISNULL(WI.numAllocation,0)) * dbo.fn_UOMConversion(ISNULL(i.numBaseUnit, 0),i.numItemCode, @numDomainID,ISNULL(t2.numUOMID, 0))  ELSE 0 END) AS vcBackordered
			,ISNULL(WI.numBackOrder,0) AS numBackOrder
            ,(CASE WHEN I.charitemType = 'P' THEN 'Product' WHEN charitemType = 'S' THEN 'Service' END) AS charitemType,
            (CASE WHEN charitemType = 'P' THEN 'Inventory'
	                WHEN charitemType = 'N' THEN 'Non-Inventory'
                    WHEN charitemType = 'S' THEN 'Service'
            END) AS vcItemType,
			CONVERT(VARCHAR(2500), I.txtItemDesc) AS txtItemDesc,   
			t2.numTotalQty AS numOrigTotalUnitHour,
			t2.numTotalQty AS numTotalUnitHour,
            t2.numQty AS numUnitHour,
            (ISNULL(dbo.fn_UOMConversion(ISNULL(t2.numUOMID, 0), i.numItemCode, @numDomainID, ISNULL(i.numBaseUnit, 0)),1) * (CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END)) monPrice,
            CONVERT(VARCHAR, CONVERT(DECIMAL(20,5),((t2.numQty * ISNULL(dbo.fn_UOMConversion(ISNULL(t2.numUOMID, 0), i.numItemCode, @numDomainID, ISNULL(i.numBaseUnit, 0)),1)) * (CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END)))) Amount,
            ((t2.numQty * ISNULL(dbo.fn_UOMConversion(ISNULL(t2.numUOMID, 0), i.numItemCode, @numDomainID, ISNULL(i.numBaseUnit, 0)),1)) * (CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END)) AS monTotAmount/*Fo calculating sum*/,
            ISNULL(CONVERT(VARCHAR, (CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END)), 0) AS monListPrice,
            I.numItemCode AS ItemCode,
            t2.numOppItemCode,
            L.vcdata AS numItemClassification,
            CASE WHEN bitTaxable = 0 THEN 'No' ELSE 'Yes' END AS Taxable,
            numIncomeChartAcntId AS itemIncomeAccount,
            'NI' AS ItemType,
            0 AS numJournalId,
            0 AS numTransactionId,
            ISNULL(I.vcModelID, '') vcModelID,
            dbo.USP_GetAttributes(t2.numWarehouseItemID, bitSerialized) AS vcAttributes,
            ISNULL(vcPartNo, '') AS vcPartNo,
            0 AS DiscAmt,
			(ISNULL(dbo.fn_UOMConversion(ISNULL(t2.numUOMID, 0), i.numItemCode, @numDomainID, ISNULL(i.numBaseUnit, 0)),1) * (CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END)) As monUnitSalePrice,
            '' AS vcNotes,'' AS vcTrackingNo,'' AS vcShippingMethod,0 AS monShipCost,NULL AS dtDeliveryDate,t2.numWarehouseItemID,
            ISNULL(u.vcUnitName, '') numUOMId,ISNULL(I.vcManufacturer, '') AS vcManufacturer,ISNULL(V.monCost, 0) AS VendorCost,
            ISNULL((SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = I.numItemCode AND bitDefault=1),'') AS vcPathForTImage,
            I.[fltLength],I.[fltWidth],I.[fltHeight],I.[fltWeight],ISNULL(I.numVendorID, 0) numVendorID,0 AS DropShip,
            '' AS SerialLotNo,
            ISNULL(t2.numUOMId, 0) AS numUOM,
            ISNULL(u.vcUnitName, '') vcUOMName,
            dbo.fn_UOMConversion(t2.numUOMId, t2.numItemCode,@numDomainID, NULL) AS UOMConversionFactor,
            0 AS numSOVendorId,
            NULL AS dtRentalStartDate,
            NULL dtRentalReturnDate,
            ISNULL(W.vcWareHouse, '') AS Warehouse,
            CASE WHEN charitemType = 'P' AND ISNULL(numItemGroup,0) > 0 AND ISNULL(bitMatrix,0)=0 THEN ISNULL(WI.vcWHSKU,I.vcSKU) ELSE I.vcSKU END vcSKU,
            CASE WHEN charitemType = 'P' AND ISNULL(numItemGroup,0) > 0 AND ISNULL(bitMatrix,0)=0 THEN ISNULL(WI.vcBarCode,I.numBarCodeId) ELSE I.numBarCodeId END vcBarcode,
			CASE WHEN charitemType = 'P' AND ISNULL(numItemGroup,0) > 0 AND ISNULL(bitMatrix,0)=0 THEN ISNULL(WI.vcBarCode,I.numBarCodeId) ELSE I.numBarCodeId END numBarCodeId,
            ISNULL(I.numItemClass, 0) AS numClassID,
            0 AS numProjectID,
            ISNULL(I.[numShipClass],0) AS numShipClass,
			@numOppBizDocsId AS numOppBizDocID,WL.vcLocation,ISNULL(bitSerialized,0) AS bitSerialized,ISNULL(bitLotNo,0) AS bitLotNo,
            @numOppId AS numOppId,t2.numQty AS numUnitHourOrig,
			ISNULL(I.numItemCode,0) AS [numItemCode],
			ISNULL(I.bitKitParent,0) AS bitKitParent,
			ISNULL(t2.numOppChildItemID,0) AS numOppChildItemID, 
			ISNULL(t2.numOppKitChildItemID,0) AS numOppKitChildItemID,
			1 AS bitChildItem,
			t2.tintLevel AS tintLevel,
			'' AS vcPromotionDetail,
			(CASE WHEN ISNULL(WI.numWareHouseItemID,0) > 0 THEN W.vcWareHouse ELSE '' END) vcWarehouse,
			'' vcInclusionDetails
			,0
			,0
			,ISNULL((SELECT SUM(numOnHand) FROM WarehouseItems WHERE WarehouseItems.numDomainID=@numDomainID AND WarehouseItems.numItemID=I.numItemCode AND WarehouseItems.numWarehouseID=W.numWareHouseID),0) numOnHand
			, ''
			,(CASE WHEN I.bitKitParent = 1 THEN ISNULL(dbo.fn_GetKitAvailabilityByWarehouse(I.numItemCode,W.numWareHouseID),0) ELSE (ISNULL(numOnHand,0) + ISNULL(numAllocation,0)) END) AS numAvailable
			,''
			,0
			,0
			,0
			,NULL
		FROM
			@TEMPTABLE t2
		INNER JOIN
			Item I
		ON
			t2.numItemCode = I.numItemCode
		LEFT JOIN
			WareHouseItems WI
		ON
			WI.numWareHouseItemID = t2.numWarehouseItemID
		LEFT JOIN 
			dbo.Warehouses W 
		ON 
			WI.numDomainID = W.numDomainID
            AND WI.numWareHouseID = W.numWareHouseID
        LEFT JOIN 
			dbo.WarehouseLocation WL 
		ON 
			WL.numWLocationID = WI.numWLocationID
		LEFT JOIN 
			ListDetails L 
		ON 
			I.numItemClassification = L.numListItemID
		LEFT JOIN 
			Vendor V 
		ON 
			V.numVendorID = @DivisionID
            AND V.numItemCode = t2.numItemCode
		LEFT JOIN 
			UOM u 
		ON 
			u.numUOMId = t2.numUOMId 
	END


    DECLARE @strSQLUpdate AS VARCHAR(2000);SET @strSQLUpdate=''
   
    EXEC ('ALTER TABLE #Temp1 ADD [TotalTax] DECIMAL(20,5)')

    IF @tintTaxOperator = 1 --add Sales tax
	BEGIN
		SET @strSQLUpdate = @strSQLUpdate
            + ',[TotalTax]= (CASE WHEN ISNULL(bitChildItem,0) = 0 THEN dbo.fn_CalBizDocTaxAmt('
            + CONVERT(VARCHAR(20), @numDomainID) + ',0,'
            + CONVERT(VARCHAR(20), @numOppId) +','+
            + 'numoppitemtCode,1,Amount,numUnitHour) ELSE 0 END)'
	END        
    ELSE IF @tintTaxOperator = 2 -- remove sales tax
	BEGIN
        SET @strSQLUpdate = @strSQLUpdate + ',[TotalTax]= 0'
	END
    ELSE 
	BEGIN
        SET @strSQLUpdate = @strSQLUpdate
            + ',[TotalTax]= (CASE WHEN ISNULL(bitChildItem,0) = 0 THEN dbo.fn_CalBizDocTaxAmt('
            + CONVERT(VARCHAR(20), @numDomainID) + ',0,'
            + CONVERT(VARCHAR(20), @numOppId) +','+
            + 'numoppitemtCode,1,Amount,numUnitHour) ELSE 0 END)'
    END

	-- CRV TAX TYPE
	EXEC ('ALTER TABLE #Temp1 ADD [CRV] DECIMAL(20,5)')

	IF @tintTaxOperator = 2
	BEGIN
		SET @strSQLUpdate = @strSQLUpdate
				+ ',[CRV]= (CASE WHEN ISNULL(bitChildItem,0) = 0 THEN dbo.fn_CalBizDocTaxAmt('
				+ CONVERT(VARCHAR(20), @numDomainID) + ',1,'
				+ CONVERT(VARCHAR(20), @numOppId) +','+
				+ 'numoppitemtCode,1,Amount,numUnitHour) ELSE 0 END)'
	END
	ELSE
	BEGIN
		SET @strSQLUpdate = @strSQLUpdate + ',[CRV]= 0'
	END

	DECLARE @vcTaxName AS VARCHAR(100)
    DECLARE @numTaxItemID AS NUMERIC(9)
    SET @numTaxItemID = 0
    
	SELECT TOP 1
        @vcTaxName = vcTaxName,
        @numTaxItemID = numTaxItemID
    FROM    
		TaxItems
    WHERE   
		numDomainID = @numDomainID

    WHILE @numTaxItemID > 0
    BEGIN
        EXEC ('ALTER TABLE #Temp1 ADD [' + @vcTaxName + '] DECIMAL(20,5)')

		IF @tintTaxOperator = 2 -- remove sales tax
		BEGIN
			SET @strSQLUpdate = @strSQLUpdate + ',[' + @vcTaxName + ']=0' 
		END
		ELSE
		BEGIN
			SET @strSQLUpdate = @strSQLUpdate + ',[' + @vcTaxName
				+ ']= (CASE WHEN ISNULL(bitChildItem,0) = 0 THEN dbo.fn_CalBizDocTaxAmt('
				+ CONVERT(VARCHAR(20), @numDomainID) + ','
				+ CONVERT(VARCHAR(20), @numTaxItemID) + ','
				+ CONVERT(VARCHAR(20), @numOppId) +','+
				+ 'numoppitemtCode,1,Amount,numUnitHour) ELSE 0 END)'
		END

        SELECT TOP 1
			@vcTaxName = vcTaxName,
            @numTaxItemID = numTaxItemID
        FROM    
			TaxItems
        WHERE   
			numDomainID = @numDomainID
            AND numTaxItemID > @numTaxItemID

        IF @@rowcount = 0 
            SET @numTaxItemID = 0
    END

    IF @strSQLUpdate <> '' 
    BEGIN
        SET @strSQLUpdate = 'UPDATE #Temp1 SET ItemCode=ItemCode' + @strSQLUpdate + ' WHERE ItemCode > 0 AND bitChildItem=0'
        EXEC (@strSQLUpdate)
    END

    DECLARE @vcDbColumnName NVARCHAR(50)

    SELECT TOP 1
            @numFldID = numfieldID,
            @vcFldname = vcFieldName,
            @vcDbColumnName = vcDbColumnName,
            @intRowNum = ( tintRow + 1 )
    FROM    View_DynamicCustomColumns
    WHERE   numFormID = @tintType
            AND Grp_id = 5
            AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId
            AND bitCustom = 1
            AND ISNULL(numRelCntType, 0) = @numBizDocTempID
    ORDER BY tintRow     
	                                                                                     
    WHILE @intRowNum > 0                                                                                  
    BEGIN    
        EXEC('ALTER TABLE #Temp1 ADD [' + @vcDbColumnName + '] varchar(100)')
	
        SET @strSQLUpdate = 'UPDATE #Temp1 set ItemCode=ItemCode,[' + @vcDbColumnName + ']= dbo.GetCustFldValueOppItems(' + @numFldID  + ',numoppitemtCode,ItemCode) WHERE ItemCode > 0 AND bitChildItem=0'
            
		EXEC (@strSQLUpdate)
		                   
        SELECT 
			TOP 1
            @numFldID = numfieldID,
            @vcFldname = vcFieldName,
            @vcDbColumnName = vcDbColumnName,
            @intRowNum = ( tintRow + 1 )
        FROM    
			View_DynamicCustomColumns
        WHERE   
			numFormID = @tintType
            AND Grp_id = 5
            AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId
            AND bitCustom = 1
            AND tintRow >= @intRowNum
            AND ISNULL(numRelCntType, 0) = @numBizDocTempID
        ORDER BY 
			tintRow                                                                  
		           
        IF @@rowcount = 0 
            SET @intRowNum = 0                                                                                          
    END


    SELECT  
		ROW_NUMBER() OVER ( ORDER BY numSortOrder) RowNumber,
        *
    FROM    
		#Temp1
    ORDER BY 
		numSortOrder ASC, tintLevel ASC, numoppitemtCode ASC, numOppChildItemID ASC, numOppKitChildItemID ASC

    DROP TABLE #Temp1

    SELECT  
		ISNULL(tintOppStatus, 0)
    FROM    
		OpportunityMaster
    WHERE   
		numOppID = @numOppId                                                                                                 


	SELECT * FROM #TempBizDocProductGridColumns ORDER BY intRowNum

	IF OBJECT_ID('tempdb..#TempBizDocProductGridColumns') IS NOT NULL DROP TABLE #TempBizDocProductGridColumns
END
GO
/****** Object:  StoredProcedure [dbo].[USP_OppManage]    Script Date: 07/26/2008 16:20:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OppManage')
DROP PROCEDURE usp_oppmanage
GO
CREATE PROCEDURE [dbo].[USP_OppManage] 
(                                                                          
  @numOppID as numeric=0 OUTPUT,                                                                          
  @numContactId numeric(9)=null,                                                                          
  @numDivisionId numeric(9)=null,                                                                          
  @tintSource numeric(9)=null,                                                                          
  @vcPOppName Varchar(100)='' OUTPUT,                                                                          
  @Comments varchar(1000)='',                                                                          
  @bitPublicFlag bit=0,                                                                          
  @numUserCntID numeric(9)=0,                                                                                 
  @monPAmount DECIMAL(20,5) =0,                                                 
  @numAssignedTo as numeric(9)=0,                                                                                                        
  @numDomainId numeric(9)=0,                                                                                                                                                                                   
  @strItems as text=null,                                                                          
  @strMilestone as text=null,                                                                          
  @dtEstimatedCloseDate datetime,                                                                          
  @CampaignID as numeric(9)=null,                                                                          
  @lngPConclAnalysis as numeric(9)=null,                                                                         
  @tintOppType as tinyint,                                                                                                                                             
  @tintActive as tinyint,                                                              
  @numSalesOrPurType as numeric(9),              
  @numRecurringId as numeric(9),
  @numCurrencyID as numeric(9)=0,
  @DealStatus as tinyint =0,-- 0=Open,1=Won,2=Lost
  @numStatus AS NUMERIC(18,0),
  @vcOppRefOrderNo VARCHAR(100),
  @numBillAddressId numeric(9)=0,
  @numShipAddressId numeric(9)=0,
  @bitStockTransfer BIT=0,
  @WebApiId INT = 0,
  @tintSourceType TINYINT=0,
  @bitDiscountType as bit,
  @fltDiscount  as float,
  @bitBillingTerms as bit,
  @intBillingDays as integer,
  @bitInterestType as bit,
  @fltInterest as float,
  @tintTaxOperator AS TINYINT,
  @numDiscountAcntType AS NUMERIC(9),
  @vcWebApiOrderNo VARCHAR(100)=NULL,
  @vcCouponCode		VARCHAR(100) = '',
  @vcMarketplaceOrderID VARCHAR(100)=NULL,
  @vcMarketplaceOrderDate  datetime=NULL,
  @vcMarketplaceOrderReportId VARCHAR(100)=NULL,
  @numPercentageComplete NUMERIC(9),
  @bitUseShippersAccountNo BIT = 0,
  @bitUseMarkupShippingRate BIT = 0,
  @numMarkupShippingRate NUMERIC(19,2) = 0,
  @intUsedShippingCompany INT = 0,
  @numShipmentMethod NUMERIC(18,0)=0,
  @dtReleaseDate DATE = NULL,
  @numPartner NUMERIC(18,0)=0,
  @tintClickBtn INT=0,
  @numPartenerContactId NUMERIC(18,0)=0,
  @numAccountClass NUMERIC(18,0) = 0,
  @numWillCallWarehouseID NUMERIC(18,0) = 0,
  @numVendorAddressID NUMERIC(18,0) = 0,
  @numShipFromWarehouse NUMERIC(18,0) = 0,
  @numShippingService NUMERIC(18,0) = 0,
  @ClientTimeZoneOffset INT = 0,
  @vcCustomerPO VARCHAR(100)=NULL,
  @bitDropShipAddress BIT = 0,
  @PromCouponCode AS VARCHAR(100) = NULL,
  @numPromotionId AS NUMERIC(18,0) = 0,
  @dtExpectedDate AS DATETIME = null,
  @numProjectID NUMERIC(18,0) = 0
  -- USE PARAMETER WITH OPTIONAL VALUE BECAUSE PROCEDURE IS CALLED FROM OTHER PROCEDURE WHICH WILL NOT PASS NEW PARAMETER VALUE
)                                                                          
                                                                          
as  
BEGIN TRY
BEGIN TRANSACTION
	DECLARE @fltExchangeRate float                                 
	DECLARE @hDocItem int                                                                                                                            
	DECLARE @TotalAmount as DECIMAL(20,5)                                                                          
	DECLARE @tintOppStatus as tinyint
	DECLARE @bitNewOrder AS BIT = (CASE WHEN ISNULL(@numOppID,0) = 0 THEN 1 ELSE 0 END)
	
	IF ISNULL(@numContactId,0) = 0
	BEGIN
		RAISERROR('CONTACT_REQUIRED',16,1)
		RETURN
	END  
	
	IF ISNULL(@tintOppType,0) = 0
	BEGIN
		RAISERROR('OPP_TYPE_CAN_NOT_BE_0',16,1)
		RETURN
	END             

	IF ISNULL(@numOppID,0) > 0 AND EXISTS (SELECT numOppId FROM OpportunityMaster WHERE numDomainId=@numDomainId AND numOppId=@numOppID AND ISNULL(tintshipped,0) = 1) 
	BEGIN	 
		RAISERROR('OPP/ORDER_IS_CLOSED',16,1)
		RETURN
	END

	DECLARE @dtItemRelease AS DATE
	SET @dtItemRelease = DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())

	DECLARE @numCompany AS NUMERIC(18)
	DECLARE @numRelationship NUMERIC(18,0)
	DECLARE @numProfile NUMERIC(18,0)
	DECLARE @tintCommitAllocation AS TINYINT
	DECLARE @bitAllocateInventoryOnPickList AS BIT

	SELECT @tintCommitAllocation=ISNULL(tintCommitAllocation,1) FROM Domain WHERE numDomainId=@numDomainId

	SELECT 
		@numCompany = D.numCompanyID
		,@bitAllocateInventoryOnPickList=ISNULL(bitAllocateInventoryOnPickList,0)
		,@numRelationship=numCompanyType
		,@numProfile=vcProfile
	FROM 
		DivisionMaster D
	JOIN 
		CompanyInfo C 
	ON 
		C.numCompanyId=D.numCompanyID
	WHERE 
		D.numDivisionID = @numDivisionId

	IF @numOppID = 0 AND CONVERT(VARCHAR(10),@strItems) <> ''
	BEGIN
		EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems

		--VALIDATE PROMOTION OF COUPON CODE
		DECLARE @TEMP TABLE
		(
			ID INT IDENTITY(1,1),
			numOppItemID NUMERIC(18,0),
			numPromotionID NUMERIC(18,0)
		)

		INSERT INTO @TEMP
		(
			numOppItemID,
			numPromotionID
		)
		SELECT
			numoppitemtCode,
			numPromotionID
		FROM
			OPENXML (@hDocItem,'/NewDataSet/Item',2)
		WITH
		(
			numoppitemtCode NUMERIC(18,0),
			numPromotionID NUMERIC(18,0)
		)
		WHERE
			numPromotionID > 0

		EXEC sp_xml_removedocument @hDocItem 

		IF (SELECT COUNT(*) FROM @TEMP) > 0
		BEGIN
			-- IF ITEM PROMOTION USED IN ORDER EXIPIRES THAN RAISE ERROR
			IF  (SELECT 
						COUNT(PO.numProId)
					FROM 
						PromotionOffer PO
					LEFT JOIN
						PromotionOffer POOrder
					ON
						PO.numOrderPromotionID = POOrder.numProId
					INNER JOIN
						@TEMP T1
					ON
						PO.numProId = T1.numPromotionID
					WHERE 
						PO.numDomainId=@numDomainID 
						AND ISNULL(PO.IsOrderBasedPromotion,0) = 0
						AND ISNULL(PO.bitEnabled,0) = 1
						AND 1 = (CASE 
									WHEN ISNULL(PO.numOrderPromotionID,0) > 0 
									THEN (CASE WHEN POOrder.bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN GETUTCDATE()>=POOrder.dtValidFrom AND GETUTCDATE()<=POOrder.dtValidTo THEN 1 ELSE 0 END) END)
									ELSE (CASE WHEN PO.bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN GETUTCDATE()>=PO.dtValidFrom AND GETUTCDATE()<=PO.dtValidTo THEN 1 ELSE 0 END) END)
								END)
						AND 1 = (CASE 
									WHEN ISNULL(PO.numOrderPromotionID,0) > 0 
									THEN
										(CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numOrderPromotionID AND tintType=2 AND numRelationship=@numRelationship AND numProfile=@numProfile) > 0 THEN 1 ELSE 0 END)
									ELSE
										(CASE PO.tintCustomersBasedOn 
											WHEN 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numProId AND tintType=1 AND numDivisionID=@numDivisionID) > 0 THEN 1 ELSE 0 END)
											WHEN 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numProId AND tintType=2 AND numRelationship=@numRelationship AND numProfile=@numProfile) > 0 THEN 1 ELSE 0 END)
											WHEN 3 THEN 1
											ELSE 0
										END)
								END)
				) <> (SELECT COUNT(*) FROM @TEMP)
			BEGIN
				RAISERROR('ITEM_PROMOTION_EXPIRED',16,1)
				RETURN
			END

			-- NOW IF COUPON BASED ITEM PROMOTION IS USED THEN VALIDATE COUPON AND ITS USAGE
			IF 
			(
				SELECT 
					COUNT(*)
				FROM
					PromotionOffer PO
				INNER JOIN
					@TEMP T1
				ON
					PO.numProId = T1.numPromotionID
				WHERE
					PO.numDomainId = @numDomainId
					AND ISNULL(IsOrderBasedPromotion,0) = 0
					AND ISNULL(numOrderPromotionID,0) > 0 
			) = 1
			BEGIN
				IF ISNULL(@PromCouponCode,0) <> ''
				BEGIN
					IF (SELECT 
							COUNT(*)
						FROM
							PromotionOffer PO
						INNER JOIN 
							PromotionOffer POOrder
						ON
							PO.numOrderPromotionID = POOrder.numProId
						INNER JOIN
							@TEMP T1
						ON
							PO.numProId = T1.numPromotionID
						INNER JOIN
							DiscountCodes DC
						ON
							POOrder.numProId = DC.numPromotionID
						WHERE
							PO.numDomainId = @numDomainId
							AND ISNULL(PO.IsOrderBasedPromotion,0) = 0
							AND ISNULL(DC.vcDiscountCode,0) = @PromCouponCode) = 0
					BEGIN
						RAISERROR('INVALID_COUPON_CODE_ITEM_PROMOTION',16,1)
						RETURN
					END
					ELSE
					BEGIN
						-- IF THERE IS COUPON CODE USAGE LIMIT THAN VALIDATE USAGE LIMIT
						IF (SELECT 
								COUNT(*)
							FROM
								PromotionOffer PO
							INNER JOIN 
								PromotionOffer POOrder
							ON
								PO.numOrderPromotionID = POOrder.numProId
							INNER JOIN
								@TEMP T1
							ON
								PO.numProId = T1.numPromotionID
							INNER JOIN
								DiscountCodes DC
							ON
								POOrder.numProId = DC.numPromotionID
							WHERE
								PO.numDomainId = @numDomainId
								AND ISNULL(PO.IsOrderBasedPromotion,0) = 0
								AND ISNULL(DC.vcDiscountCode,0) = @PromCouponCode
								AND 1 = (CASE WHEN ISNULL(DC.CodeUsageLimit,0) <> 0 THEN 1 ELSE 0 END)
								AND ISNULL((SELECT intCodeUsed FROM DiscountCodeUsage WHERE DiscountCodeUsage.numDiscountId=DC.numDiscountId AND numDivisionId=@numDivisionId),0) >= DC.CodeUsageLimit) > 0
						BEGIN
							RAISERROR('ITEM_PROMOTION_COUPON_CODE_USAGE_LIMIT_EXCEEDED',16,1)
							RETURN
						END
					END
				END
				ELSE
				BEGIN
					RAISERROR('COUPON_CODE_REQUIRED_ITEM_PROMOTION',16,1)
					RETURN
				END
			END
		END
	END

	--IF NEW OPPERTUNITY
	IF @numOppID = 0                                                                          
	BEGIN
		DECLARE @bitIsInitalSalesOrder BIT
		IF(@DealStatus=1 AND @tintOppType=1)
		BEGIN
			SET @bitIsInitalSalesOrder=1
		END

		DECLARE @intOppTcode AS NUMERIC(9)  
  
		SELECT @intOppTcode = MAX(numOppId) FROM OpportunityMaster                

		SET @intOppTcode = ISNULL(@intOppTcode,0) + 1                                                
		
		SET @vcPOppName = @vcPOppName + '-'+ CONVERT(varchar(4),YEAR(GETUTCDATE()))  +'-A'+ CONVERT(varchar(10),@intOppTcode)                                  
  
		IF ISNULL(@numCurrencyID,0) = 0 
		BEGIN
			SET @fltExchangeRate=1
			SELECT @numCurrencyID= ISNULL(numCurrencyID,0) FROM dbo.Domain WHERE numDomainId=@numDomainId	
		END
		ELSE
		BEGIN 
			SET @fltExchangeRate = dbo.GetExchangeRate(@numDomainID,@numCurrencyID)
        END  
		                                                        
		IF ISNULL(@numAccountClass,0) = 0
		BEGIN                                                  
		DECLARE @tintDefaultClassType AS INT = 0
		SELECT @tintDefaultClassType = ISNULL(tintDefaultClassType,0) FROM Domain WHERE numDomainId = @numDomainID 

		IF @tintDefaultClassType = 1 --USER
		BEGIN
			SELECT 
				@numAccountClass=ISNULL(numDefaultClass,0) 
			FROM 
				dbo.UserMaster UM 
			JOIN 
				dbo.Domain D 
			ON 
				UM.numDomainID=D.numDomainId
			WHERE 
				D.numDomainId=@numDomainId 
				AND UM.numUserDetailId=@numUserCntID
		END
		ELSE IF @tintDefaultClassType = 2 --COMPANY
		BEGIN
			SELECT 
				@numAccountClass=ISNULL(numAccountClassID,0) 
			FROM 
				dbo.DivisionMaster DM 
			WHERE 
				DM.numDomainId=@numDomainId 
				AND DM.numDivisionID=@numDivisionID
		END
		ELSE
		BEGIN
			SET @numAccountClass = 0
		END
		END

        IF(@DealStatus=1 AND @tintOppType=1 AND @tintClickBtn=1)
		BEGIN
			SET @DealStatus=0
		END         
		--PRINT ('Deal Staus ' +@DealStatus)  

		-------- VALIDATE ORDER BASED PROMOTION -------------

		DECLARE @numDiscountID NUMERIC(18,0) = NULL
		IF @numPromotionId > 0 
		BEGIN
			-- VALIDTE IF ITS ORDER BASED PROMOTION (BOTH ITEM AND ORDER BASED PROMOTION CAN HAVE COUPON CODE)
			IF EXISTS (SELECT numProId FROM PromotionOffer WHERE numDomainId=@numDomainId AND numProId=@numPromotionId AND ISNULL(IsOrderBasedPromotion,0) = 1)
			BEGIN
				-- CHECK IF ORDER PROMOTION IS STILL VALID
				IF NOT EXISTS (SELECT 
									PO.numProId
								FROM 
									PromotionOffer PO
								INNER JOIN 
									PromotionOfferOrganizations PORG
								ON 
									PO.numProId = PORG.numProId
								WHERE 
									numDomainId=@numDomainID 
									AND PO.numProId=@numPromotionId
									AND ISNULL(IsOrderBasedPromotion,0) = 1
									AND ISNULL(bitEnabled,0) = 1
									AND 1 = (CASE WHEN bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN GETUTCDATE()>=dtValidFrom AND GETUTCDATE()<=dtValidTo THEN 1 ELSE 0 END) END)
									AND numRelationship=@numRelationship 
									AND numProfile=@numProfile
				)
				BEGIN
					RAISERROR('ORDER_PROMOTION_EXPIRED',16,1)
					RETURN
				END
				ELSE IF ISNULL((SELECT ISNULL(bitRequireCouponCode,0) FROM PromotionOffer WHERE numDomainId=@numDomainId AND numProId=@numPromotionId),0) = 1
				BEGIN
					IF ISNULL(@PromCouponCode,0) <> ''
					BEGIN
						IF (SELECT 
								COUNT(*)
							FROM
								PromotionOffer PO
							INNER JOIN
								DiscountCodes DC
							ON
								PO.numProId = DC.numPromotionID
							WHERE
								PO.numDomainId = @numDomainId
								AND PO.numProId = @numPromotionId
								AND ISNULL(IsOrderBasedPromotion,0) = 1
								AND ISNULL(bitRequireCouponCode,0) = 1
								AND ISNULL(DC.vcDiscountCode,0) = @PromCouponCode) = 0
						BEGIN
							RAISERROR('INVALID_COUPON_CODE_ORDER_PROMOTION',16,1)
							RETURN
						END
						ELSE
						BEGIN
							-- IF THERE IS COUPON CODE USAGE LIMIT THAN VALIDATE USAGE LIMIT
							IF (SELECT 
									COUNT(*)
								FROM
									PromotionOffer PO
								INNER JOIN
									DiscountCodes DC
								ON
									PO.numProId = DC.numPromotionID
								WHERE
									PO.numDomainId = @numDomainId
									AND PO.numProId = @numPromotionId
									AND ISNULL(IsOrderBasedPromotion,0) = 1
									AND ISNULL(bitRequireCouponCode,0) = 1
									AND ISNULL(DC.vcDiscountCode,0) = @PromCouponCode
									AND 1 = (CASE WHEN ISNULL(DC.CodeUsageLimit,0) <> 0 THEN 1 ELSE 0 END)
									AND ISNULL((SELECT intCodeUsed FROM DiscountCodeUsage WHERE DiscountCodeUsage.numDiscountId=DC.numDiscountId AND numDivisionId=@numDivisionId),0) >= DC.CodeUsageLimit) > 0
							BEGIN
								RAISERROR('ORDER_PROMOTION_COUPON_CODE_USAGE_LIMIT_EXCEEDED',16,1)
								RETURN
							END
							ELSE
							BEGIN			
								SELECT
									@numDiscountID=numDiscountId
								FROM
									DiscountCodes DC
								WHERE
									DC.numPromotionID=@numPromotionId
									AND ISNULL(DC.vcDiscountCode,'')=@PromCouponCode
									
								IF EXISTS (SELECT DC.numDiscountID FROM DiscountCodes DC INNER JOIN DiscountCodeUsage DCU ON DC.numDiscountID=DCU.numDIscountID WHERE DC.numPromotionID=@numPromotionId AND DCU.numDivisionID=@numDivisionID AND ISNULL(DC.vcDiscountCode,'')=@PromCouponCode)
								BEGIN
									UPDATE
										DCU
									SET
										DCU.intCodeUsed = ISNULL(DCU.intCodeUsed,0) + 1
									FROM 
										DiscountCodes DC 
									INNER JOIN 
										DiscountCodeUsage DCU 
									ON 
										DC.numDiscountID=DCU.numDIscountID 
									WHERE 
										DC.numPromotionID=@numPromotionId 
										AND DCU.numDivisionID=@numDivisionID 
										AND ISNULL(DC.vcDiscountCode,'')=@PromCouponCode
								END
								ELSE
								BEGIN
									INSERT INTO DiscountCodeUsage
									(
										numDiscountId
										,numDivisionId
										,intCodeUsed
									)
									SELECT 
										DC.numDiscountId
										,@numDivisionId
										,1
									FROM
										DiscountCodes DC
									WHERE
										DC.numPromotionID=@numPromotionId
										AND ISNULL(DC.vcDiscountCode,'')=@PromCouponCode
								END
							END
						END
					END
					ELSE
					BEGIN
						RAISERROR('COUPON_CODE_REQUIRED_ORDER_PROMOTION',16,1)
						RETURN
					END
				END
			END
		END

		-------- ORDER BASED Promotion Logic-------------
		                                                    
		INSERT INTO OpportunityMaster
		(                                                                             
			numContactId,numDivisionId,txtComments,numCampainID,bitPublicFlag,tintSource,tintSourceType,vcPOppName,                                                                          
			intPEstimatedCloseDate,monPAmount,numCreatedBy,bintCreatedDate,numModifiedBy,bintModifiedDate,numDomainId,                                                                                                             
			numRecOwner,lngPConclAnalysis,tintOppType,numSalesOrPurType,numCurrencyID,fltExchangeRate,numAssignedTo,
			numAssignedBy,[tintOppStatus],numStatus,vcOppRefOrderNo,bitStockTransfer,bitBillingTerms,intBillingDays,
			bitInterestType,fltInterest,vcCouponCode,bitDiscountType,fltDiscount,bitPPVariance,vcMarketplaceOrderID,
			vcMarketplaceOrderReportId,numPercentageComplete,numAccountClass,tintTaxOperator,bitUseShippersAccountNo,
			bitUseMarkupShippingRate,numMarkupShippingRate,intUsedShippingCompany,numShipmentMethod,numShippingService,numPartner,numPartenerContact,dtReleaseDate,bitIsInitalSalesOrder,numVendorAddressID,numShipFromWarehouse
			,vcCustomerPO#,bitDropShipAddress,numDiscountID,dtExpectedDate,numProjectID
		)                                                                          
		Values                                                                          
		(                                                                          
			@numContactId,@numDivisionId,ISNULL(@Comments,''),@CampaignID,@bitPublicFlag,@tintSource,@tintSourceType,@vcPOppName,                                                                          
			@dtEstimatedCloseDate,@monPAmount,@numUserCntID,(CASE @tintSourceType WHEN 3 THEN @vcMarketplaceOrderDate ELSE GETUTCDATE() END),                                                                          
			@numUserCntID,GETUTCDATE(),@numDomainId,@numUserCntID,@lngPConclAnalysis,@tintOppType,@numSalesOrPurType,@numCurrencyID, 
			@fltExchangeRate,(CASE WHEN @numAssignedTo > 0 THEN @numAssignedTo ELSE NULL END),(CASE WHEN @numAssignedTo>0 THEN @numUserCntID ELSE NULL END),
			@DealStatus,@numStatus,@vcOppRefOrderNo,@bitStockTransfer,@bitBillingTerms,@intBillingDays,@bitInterestType,@fltInterest,
			@PromCouponCode,@bitDiscountType,@fltDiscount,1,@vcMarketplaceOrderID,@vcMarketplaceOrderReportId,@numPercentageComplete,
			@numAccountClass,@tintTaxOperator,@bitUseShippersAccountNo,@bitUseMarkupShippingRate,@numMarkupShippingRate,@intUsedShippingCompany,@numShipmentMethod,@numShippingService,@numPartner,@numPartenerContactId,@dtReleaseDate,@bitIsInitalSalesOrder,@numVendorAddressID,@numShipFromWarehouse
			,@vcCustomerPO,@bitDropShipAddress,@numDiscountID,@dtExpectedDate,@numProjectID
		)                                                                                                                      
		
		SET @numOppID=(SELECT SCOPE_IDENTITY())                
                            
        EXEC dbo.usp_OppDefaultAssociateContacts @numOppId=@numOppID,@numDomainId=@numDomainId                    
		--UPDATE OPPNAME AS PER NAME TEMPLATE
		EXEC dbo.USP_UpdateNameTemplateValue @tintOppType,@numDomainID,@numOppID

		SELECT @vcPOppName=ISNULL(vcPOppName,'') FROM OpportunityMaster WHERE numOppId=@numOppID

		--MAP CUSTOM FIELD	
		DECLARE @tintPageID AS TINYINT;
		SET @tintPageID = CASE WHEN @tintOppType=1 THEN 2 ELSE 6 END 
  	
		EXEC dbo.USP_AddParentChildCustomFieldMap
				@numDomainID = @numDomainID, --  numeric(18, 0)
				@numRecordID = @numOppID, --  numeric(18, 0)
				@numParentRecId = @numDivisionId, --  numeric(18, 0)
				@tintPageID = @tintPageID --  tinyint
 	
		IF ISNULL(@numStatus,0) > 0 AND @tintOppType=1 AND isnull(@DealStatus,0)=1
		BEGIN
	   		EXEC USP_ManageOpportunityAutomationQueue
					@numOppQueueID = 0, -- numeric(18, 0)
					@numDomainID = @numDomainID, -- numeric(18, 0)
					@numOppId = @numOppID, -- numeric(18, 0)
					@numOppBizDocsId = 0, -- numeric(18, 0)
					@numOrderStatus = @numStatus, -- numeric(18, 0)
					@numUserCntID = @numUserCntID, -- numeric(18, 0)
					@tintProcessStatus = 1, -- tinyint
					@tintMode = 1 -- TINYINT
		END
  
		-- INSERTING ITEMS                                                                                                                    
		IF CONVERT(VARCHAR(10),@strItems) <> '' AND @numOppID > 0
		BEGIN
			EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems       
	
			INSERT INTO OpportunityItems
			(
				numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,bitDropShip,bitDiscountType,fltDiscount,
				monTotAmtBefDiscount,vcItemName,vcModelID,vcManufacturer,vcPathForTImage,monVendorCost,numUOMId,bitWorkOrder,numVendorWareHouse,
				numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,numClassID,numToWarehouseItemID,vcAttributes,vcAttrValues,monAvgCost,bitItemPriceApprovalRequired,
				numPromotionID,bitPromotionTriggered,vcPromotionDetail,numSortOrder,numCost,vcNotes,vcInstruction,bintCompliationDate,dtPlannedStart,numWOAssignedTo,numShipToAddressID,ItemReleaseDate
				,bitMarkupDiscount,vcChildKitSelectedItems,numWOQty
			)
			SELECT 
				@numOppID,X.numItemCode,dbo.fn_UOMConversion(X.numUOM,numItemCode,@numDomainId,null) * x.numUnitHour,x.monPrice,x.monTotAmount,X.vcItemDesc,
				(CASE X.numWarehouseItmsID 
				WHEN -1 
				THEN 
					CASE 
						WHEN ((SELECT COUNT(*) FROM Item WHERE numDomainID=@numDomainId AND vcSKU= X.vcSKU) > 0)
							THEN
								(SELECT TOP 1 
									numWareHouseItemID 
								FROM 
									WareHouseItems 
								WHERE 
									[numItemID] = X.numItemCode 
									AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId))
						ELSE
							(SELECT 
								[numWareHouseItemID] 
							FROM 
								[WareHouseItems] 
							WHERE 
								[numItemID] = X.numItemCode 
								AND ISNULL(WareHouseItems.vcWHSKU,'') = ISNULL(X.vcSKU,'')
								AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId)) 
					END
				WHEN 0 
				THEN 
					CASE 
					WHEN ((SELECT COUNT(*) FROM Item WHERE numDomainID=@numDomainId AND vcSKU=X.vcSKU) > 0)
						THEN
							(SELECT TOP 1 
								numWareHouseItemID 
							FROM 
								WareHouseItems 
							WHERE 
								[numItemID] = X.numItemCode 
								AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId))
					ELSE
						(SELECT 
							[numWareHouseItemID] 
						FROM 
							[WareHouseItems] 
						WHERE 
							[numItemID] = X.numItemCode 
							AND ISNULL(WareHouseItems.vcWHSKU,'') = ISNULL(X.vcSKU,'')
							AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId)) 
					END
				ELSE  
					X.numWarehouseItmsID 
				END) AS numWarehouseItmsID,
				X.ItemType,X.DropShip,X.bitDiscountType,X.fltDiscount,X.monTotAmtBefDiscount,X.vcItemName,(SELECT vcModelID FROM item WHERE numItemCode = X.numItemCode),
				(SELECT vcManufacturer FROM item WHERE numItemCode = X.numItemCode),(SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = X.numItemCode AND BitDefault = 1 AND numdomainId = @numdomainId),
				(SELECT isnull(VN.monCost,0) FROM Vendor VN INNER JOIN Item IT ON IT.numItemCode=VN.numItemCode AND VN.numVendorID=IT.numVendorID WHERE  VN.numItemCode=X.numItemCode),
				X.numUOM,X.bitWorkOrder,X.numVendorWareHouse,X.numShipmentMethod,X.numSOVendorId,X.numProjectID,X.numProjectStageID,
				0,
				X.numToWarehouseItemID,X.Attributes,X.AttributeIDs,(SELECT (CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(I.monAverageCost,0) END) FROM Item I WHERE I.numDomainId=@numDomainId AND I.numItemCode=X.numItemCode),
				X.bitItemPriceApprovalRequired,X.numPromotionID,X.bitPromotionTriggered,X.vcPromotionDetail,X.numSortOrder,ISNULL(X.numCost,0),ISNULL(X.vcVendorNotes,'')
				,X.vcInstruction,DATEADD(MINUTE,@ClientTimeZoneOffset,X.bintCompliationDate),DATEADD(MINUTE,@ClientTimeZoneOffset,X.dtPlannedStart),X.numWOAssignedTo,X.numShipToAddressID,ISNULL(ItemReleaseDate,@dtItemRelease),X.bitMarkupDiscount,ISNULL(X.KitChildItems,''),X.numWOQty
			FROM
				(
					SELECT 
						*
					FROM 
						OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
					WITH                       
					(                                                                          
						numoppitemtCode NUMERIC(9),numItemCode NUMERIC(9), numUnitHour FLOAT, monPrice DECIMAL(30,16), monTotAmount DECIMAL(20,5),vcItemDesc VARCHAR(2000),                                    
						numWarehouseItmsID NUMERIC(9), ItemType VARCHAR(30), DropShip BIT, bitDiscountType BIT,	fltDiscount DECIMAL(30,16), monTotAmtBefDiscount DECIMAL(20,5),
						vcItemName VARCHAR(300),numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId NUMERIC(9),
						numProjectID numeric(9),numProjectStageID numeric(9),numToWarehouseItemID NUMERIC(9),Attributes varchar(500),AttributeIDs VARCHAR(500),vcSKU VARCHAR(100)
						,bitItemPriceApprovalRequired BIT,numPromotionID NUMERIC(18,0),bitPromotionTriggered BIT,vcPromotionDetail VARCHAR(2000),numSortOrder NUMERIC(18,0)
						,numCost DECIMAL(20,5),vcVendorNotes VARCHAR(1000),vcInstruction VARCHAR(1000),bintCompliationDate DATETIME,dtPlannedStart DATETIME,numWOAssignedTo NUMERIC(18,0)
						,numShipToAddressID  NUMERIC(18,0),ItemReleaseDate DATETIME,bitMarkupDiscount BIT,KitChildItems VARCHAR(MAX),numWOQty FLOAT
					)
				)X    
			ORDER BY 
				X.numoppitemtCode
    
			-- UPDATE EXISTING CUSTOMER PART NO
			UPDATE
				CPN
			SET
				CPN.CustomerPartNo = ISNULL(TEMPCPN.CustomerPartNo,'')
			FROM
				CustomerPartNumber CPN
			INNER JOIN
			(
				SELECT 
					numItemCode
					,MIN(CustomerPartNo) AS CustomerPartNo
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
				WITH                       
				(                                                                          
					CustomerPartNo VARCHAR(300), numItemCode NUMERIC(18,0)
				)
				GROUP BY
					numItemCode
			) AS TEMPCPN
			ON
				CPN.numCompanyId = @numCompany
				AND CPN.numItemCode = TEMPCPN.numItemCode
			WHERE
				CPN.numDomainID=@numDomainID
				AND CPN.numCompanyId = @numCompany
				AND LEN(ISNULL(TEMPCPN.CustomerPartNo,'')) > 0
				AND ISNULL(TEMPCPN.CustomerPartNo,'') <> '0'
			
			-- INSERT NEW CUSTOMER PART NO
			INSERT INTO CustomerPartNumber
			(
				numCompanyId, numDomainID, CustomerPartNo, numItemCode 
			)
			SELECT 
				@numCompany, @numDomainID, TEMPCPN.CustomerPartNo,TEMPCPN.numItemCode
			FROM
			(
				SELECT 
					numItemCode
					,MIN(CustomerPartNo) AS CustomerPartNo
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
				WITH                       
				(                                                                          
					CustomerPartNo VARCHAR(300), numItemCode NUMERIC(18,0)
				)
				GROUP BY
					numItemCode
			) TEMPCPN 
			LEFT JOIN
				CustomerPartNumber CPN
			ON
				CPN.numCompanyId = @numCompany
				AND CPN.numItemCode = TEMPCPN.numItemCode
			WHERE
				CPN.CustomerPartNoID IS NULL
				AND LEN(ISNULL(TEMPCPN.CustomerPartNo,'')) > 0
				AND ISNULL(TEMPCPN.CustomerPartNo,'') <> '0'

			UPDATE 
				OpportunityItems                       
			SET 
				numItemCode=X.numItemCode,numOppId=@numOppID,numUnitHour=dbo.fn_UOMConversion(X.numUOM,X.numItemCode,@numDomainId,null) * X.numUnitHour,                      
				monPrice=x.monPrice,monTotAmount=x.monTotAmount,vcItemDesc=X.vcItemDesc,numWarehouseItmsID=X.numWarehouseItmsID,bitDropShip=X.DropShip,
				bitDiscountType=X.bitDiscountType,fltDiscount=X.fltDiscount,monTotAmtBefDiscount=X.monTotAmtBefDiscount,vcItemName=X.vcItemName,numUOMId=numUOM,
				bitWorkOrder=X.bitWorkOrder,numVendorWareHouse=X.numVendorWareHouse,numShipmentMethod=X.numShipmentMethod,numSOVendorId=X.numSOVendorId,
				numProjectId=X.numProjectID,numProjectStageId=X.numProjectStageID,numToWarehouseItemID = X.numToWarehouseItemID,vcAttributes=X.Attributes,
				numClassID=0,vcModelID=X.vcModelID,
				bitItemPriceApprovalRequired = X.bitItemPriceApprovalRequired,vcAttrValues=X.AttributeIDs,
				numPromotionID=X.numPromotionID,bitPromotionTriggered=X.bitPromotionTriggered,vcPromotionDetail=X.vcPromotionDetail,numSortOrder=X.numSortOrder,numCost=X.numCost
				,vcNotes=X.vcVendorNotes,vcInstruction=X.vcInstruction,bintCompliationDate=DATEADD(MINUTE,@ClientTimeZoneOffset,X.bintCompliationDate),dtPlannedStart=DATEADD(MINUTE,@ClientTimeZoneOffset,X.dtPlannedStart),numWOAssignedTo=X.numWOAssignedTo,ItemRequiredDate=X.ItemRequiredDate
				,bitMarkupDiscount=X.bitMarkupDiscount,vcChildKitSelectedItems=ISNULL(X.KitChildItems,'')
			FROM 
			(
				SELECT 
					numoppitemtCode as numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,vcModelID,numWarehouseItmsID,DropShip,bitDiscountType,fltDiscount,
					monTotAmtBefDiscount,vcItemName,numUOM,bitWorkOrder,numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,numToWarehouseItemID,
					Attributes,bitItemPriceApprovalRequired,AttributeIDs,numPromotionID,bitPromotionTriggered,vcPromotionDetail,numSortOrder,ISNULL(numCost,0) numCost
					,ISNULL(vcVendorNotes,'') vcVendorNotes,vcInstruction,bintCompliationDate,dtPlannedStart,numWOAssignedTo,ItemRequiredDate,bitMarkupDiscount,KitChildItems
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=2]',2)
				WITH  
					(                      
						numoppitemtCode NUMERIC(9),numItemCode NUMERIC(9),numUnitHour FLOAT,monPrice DECIMAL(30,16),monTotAmount DECIMAL(20,5),vcItemDesc VARCHAR(2000), vcModelID VARCHAR(300),
						numWarehouseItmsID NUMERIC(9),DropShip BIT,bitDiscountType BIT,fltDiscount DECIMAL(30,16),monTotAmtBefDiscount DECIMAL(20,5),vcItemName VARCHAR(300),
						numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId NUMERIC(9),numProjectID NUMERIC(9),
						numProjectStageID numeric(9),numToWarehouseItemID NUMERIC(9),Attributes varchar(500),bitItemPriceApprovalRequired BIT,AttributeIDs VARCHAR(500),
						numPromotionID NUMERIC(18,0),bitPromotionTriggered BIT,vcPromotionDetail VARCHAR(2000),numSortOrder NUMERIC(18,0),numCost DECIMAL(20,5),vcVendorNotes VARCHAR(1000)
						,vcInstruction VARCHAR(1000),bintCompliationDate DATETIME,dtPlannedStart DATETIME,numWOAssignedTo NUMERIC(18,0),ItemRequiredDate DATETIME,bitMarkupDiscount BIT,KitChildItems VARCHAR(MAX)
					)
			)X 
			WHERE 
				numoppitemtCode=X.numOppItemID AND numOppID=@numOppID
				
			--INSERT KIT ITEM HAS ANOTHER KIT AS CHILD THEN SAVE CONFIGURATION OF KIT
			DECLARE @TempKitConfiguration TABLE
			(
				numOppItemID NUMERIC(18,0),
				numItemCode NUMERIC(18,0),
				numKitItemID NUMERIC(18,0),
				numKitChildItemID NUMERIC(18,0),
				numQty FLOAT,
				numSequence INT
			)

			INSERT INTO @TempKitConfiguration
			(
				numOppItemID,
				numItemCode,
				numKitItemID,
				numKitChildItemID,
				numQty,
				numSequence
			)
			SELECT
				numoppitemtCode,
				numItemCode,
				numKitItemID,
				numChildItemID,
				numQty,
				numSequence
			FROM
				OpportunityItems
			CROSS APPLY
			(
				SELECT 
					Reverse(ParseName(Replace(Reverse(OutParam), '-', '.'), 1)) As numKitItemID
					,Reverse(ParseName(Replace(Reverse(OutParam), '-', '.'), 2)) As numChildItemID
					,Reverse(ParseName(Replace(Reverse(OutParam), '-', '.'), 3)) As numQty
					,Reverse(ParseName(Replace(Reverse(OutParam), '-', '.'), 4)) As numSequence
				FROM  
				(
					SELECT 
						OutParam 
					FROM 
						SplitString(vcChildKitSelectedItems,',')
				) X
			) Y
			WHERE
				numOppId = @numOppID
				AND LEN(ISNULL(vcChildKitSelectedItems,'')) > 0
			ORDER BY
				ISNULL(Y.numSequence,0)

			--INSERT KIT ITEMS 
			INSERT INTO OpportunityKitItems
			(
				numOppId,
				numOppItemID,
				numChildItemID,
				numWareHouseItemId,
				numQtyItemsReq,
				numQtyItemsReq_Orig,
				numUOMId,
				numQtyShipped,
				monAvgCost
			)
			SELECT
				@numOppId,
				OI.numoppitemtCode,
				T1.numKitChildItemID,
				ISNULL((SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=T1.numKitChildItemID AND WareHouseItems.numWareHouseID = W.numWarehouseID ORDER BY numWareHouseItemID),0),
				T1.numQty * OI.numUnitHour,
				T1.numQty,
				I.numBaseUnit,
				0,
				ISNULL(I.monAverageCost,0)
			FROM 
				OpportunityItems OI 
			LEFT JOIN
				WarehouseItems WI
			ON
				OI.numWarehouseItmsID = WI.numWarehouseItemID
			LEFT JOIN
				Warehouses W
			ON
				WI.numWarehouseID = W.numWarehouseID
			INNER JOIN
				@TempKitConfiguration T1
			ON
				T1.numOppItemID=OI.numoppitemtCode
				AND T1.numItemCode = OI.numItemCode
			JOIN
				Item I
			ON
				T1.numKitChildItemID = I.numItemCode
			WHERE 
				OI.numOppId=@numOppId
				AND (SELECT COUNT(*) FROM @TempKitConfiguration t2 WHERE t2.numOppItemID=OI.numoppitemtCode AND t2.numItemCode=OI.numItemCode AND ISNULL(t2.numKitItemID,0) = 0) > 0
			ORDER BY
				ISNULL(T1.numSequence,0)

			INSERT INTO OpportunityKitItems
			(
				numOppId,
				numOppItemID,
				numChildItemID,
				numWareHouseItemId,
				numQtyItemsReq,
				numQtyItemsReq_Orig,
				numUOMId,
				numQtyShipped,
				monAvgCost
			)
			SELECT
				@numOppId,
				OI.numoppitemtCode,
				ID.numChildItemID,
				ISNULL((SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ID.numChildItemID AND WareHouseItems.numWareHouseID = W.numWarehouseID ORDER BY numWareHouseItemID),0),
				(ID.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(ID.numUOMId,ID.numChildItemID,@numDomainID,I.numBaseUnit),1)) * OI.numUnitHour,
				ID.numQtyItemsReq,
				ID.numUOMId,
				0,
				ISNULL(I.monAverageCost,0)
			FROM 
				OpportunityItems OI 
			LEFT JOIN
				WarehouseItems WI
			ON
				OI.numWarehouseItmsID = WI.numWarehouseItemID
			LEFT JOIN
				Warehouses W
			ON
				WI.numWarehouseID = W.numWarehouseID
			JOIN 
				ItemDetails ID 
			ON 
				OI.numItemCode=ID.numItemKitID 
			JOIN
				Item I
			ON
				ID.numChildItemID = I.numItemCode
			WHERE 
				OI.numOppId=@numOppId
				AND (SELECT COUNT(*) FROM @TempKitConfiguration t2 WHERE t2.numOppItemID=OI.numoppitemtCode AND t2.numItemCode=OI.numItemCode AND ISNULL(t2.numKitItemID,0) = 0) = 0
			ORDER BY
				ISNULL(ID.sintOrder,0)

			IF (SELECT 
					COUNT(*) 
				FROM 
					OpportunityKitItems OI 
				INNER JOIN
					Item I
				ON
					OI.numChildItemID = I.numItemCode
				WHERE 
					numOppId = @numOppID
					AND ISNULL(charItemType,0) = 'P'
					AND numWareHouseItemId = 0) > 0
			BEGIN
				RAISERROR('SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS',16,1)
			END

			IF (SELECT COUNT(*) FROM OPENXML (@hDocItem,'/NewDataSet/Item[bitHasKitAsChild=''true'']',2)) > 0
			BEGIN
				IF (SELECT 
						COUNT(*) 
					FROM 
					(
						SELECT 
							t1.numOppItemID
							,t1.numItemCode
							,numKitItemID
							,numKitChildItemID
						FROM 
							@TempKitConfiguration t1
						WHERE
							ISNULL(t1.numKitItemID,0) > 0
					) TempChildItems
					INNER JOIN
						OpportunityKitItems OKI
					ON
						OKI.numOppId=@numOppId
						AND OKI.numOppItemID=TempChildItems.numOppItemID
						AND OKI.numChildItemID=TempChildItems.numKitItemID
					INNER JOIN
						OpportunityItems OI
					ON
						OI.numItemCode = TempChildItems.numItemCode
						AND OI.numoppitemtcode = OKI.numOppItemID
					LEFT JOIN
						WarehouseItems WI
					ON
						OI.numWarehouseItmsID = WI.numWarehouseItemID
					LEFT JOIN
						Warehouses W
					ON
						WI.numWarehouseID = W.numWarehouseID
					INNER JOIN
						ItemDetails
					ON
						TempChildItems.numKitItemID = ItemDetails.numItemKitID
						AND TempChildItems.numKitChildItemID = ItemDetails.numChildItemID
					INNER JOIN
						Item 
					ON
						ItemDetails.numChildItemID = Item.numItemCode
					INNER JOIN
						Item IMain
					ON
						OKI.numChildItemID = IMain.numItemCode
					WHERE
						Item.charItemType = 'P'
						AND ISNULL(IMain.bitKitParent,0) = 1
						AND ISNULL((SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ItemDetails.numChildItemID AND numWareHouseID = W.numWarehouseID),0) = 0) > 0
				BEGIN
					RAISERROR('SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS',16,1)
				END

				INSERT INTO OpportunityKitChildItems
				(
					numOppID,
					numOppItemID,
					numOppChildItemID,
					numItemID,
					numWareHouseItemId,
					numQtyItemsReq,
					numQtyItemsReq_Orig,
					numUOMId,
					numQtyShipped,
					monAvgCost
				)
				SELECT 
					@numOppID,
					OKI.numOppItemID,
					OKI.numOppChildItemID,
					ItemDetails.numChildItemID,
					(SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ItemDetails.numChildItemID AND WareHouseItems.numWareHouseID = W.numWarehouseID ORDER BY numWareHouseItemID),
					(ItemDetails.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(ItemDetails.numUOMId,ItemDetails.numChildItemID,@numDomainID,Item.numBaseUnit),1)) * OKI.numQtyItemsReq,
					ItemDetails.numQtyItemsReq,
					ItemDetails.numUOMId,
					0,
					ISNULL(Item.monAverageCost,0)
				FROM
				(
					SELECT 
						t1.numOppItemID
						,t1.numItemCode
						,numKitItemID
						,numKitChildItemID
					FROM 
						@TempKitConfiguration t1
					WHERE
						ISNULL(t1.numKitItemID,0) > 0
				) TempChildItems
				INNER JOIN
					OpportunityKitItems OKI
				ON
					OKI.numOppId=@numOppId
					AND OKI.numOppItemID=TempChildItems.numOppItemID
					AND OKI.numChildItemID=TempChildItems.numKitItemID
				INNER JOIN
					OpportunityItems OI
				ON
					OI.numItemCode = TempChildItems.numItemCode
					AND OI.numoppitemtcode = OKI.numOppItemID
				LEFT JOIN
					WarehouseItems WI
				ON
					OI.numWarehouseItmsID = WI.numWarehouseItemID
				LEFT JOIN
					Warehouses W
				ON
					WI.numWarehouseID = W.numWarehouseID
				INNER JOIN
					ItemDetails
				ON
					TempChildItems.numKitItemID = ItemDetails.numItemKitID
					AND TempChildItems.numKitChildItemID = ItemDetails.numChildItemID
				INNER JOIN
					Item 
				ON
					ItemDetails.numChildItemID = Item.numItemCode
			END

			-- UPDATE UOM OF OPPORTUNITY ITEMS HAVE NOT UOM SET WHILE EXPORTED FROM MARKETPLACE (MEANS TINTSOURCETYPE = 3)
			IF ISNULL(@tintSourceType,0) = 3
			BEGIN
				UPDATE 
					OI 
				SET 
					numUOMID = ISNULL(I.numSaleUnit,0), 
					numUnitHour = dbo.fn_UOMConversion(ISNULL(I.numSaleUnit,0),OI.numItemCode,@numDomainId,null) * OI.numUnitHour
				FROM 
					OpportunityItems OI
				JOIN 
					Item I 
				ON 
					OI.numItemCode = I.numItemCode
				WHERE 
					numOppId = @numOppID
					AND I.numDomainID = @numDomainId
			END	
                                     
			INSERT INTO OppWarehouseSerializedItem
			(
				numOppID,
				numWarehouseItmsDTLID,
				numOppItemID,
				numWarehouseItmsID
			)                      
			SELECT 
				@numOppID,
				X.numWarehouseItmsDTLID,
				X.numoppitemtCode,
				X.numWItmsID 
			FROM
			(                                                                          
				SELECT
					* 
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/SerialNo',2)                                                                          
				WITH                        
				(                                               
				numWarehouseItmsDTLID numeric(9),                      
				numoppitemtCode numeric(9),                      
				numWItmsID numeric(9)                                                     
				)
			)X                                    
                                   
			UPDATE 
				OppWarehouseSerializedItem                       
			SET 
				numOppItemID=X.numoppitemtCode                                
			FROM                       
			(
				SELECT 
					numWarehouseItmsDTLID AS DTLID,
					O.numoppitemtCode 
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/SerialNo',2)                                                                         
				WITH
				(
					numWarehouseItmsDTLID NUMERIC(9),numoppitemtCode NUMERIC(9),numWItmsID NUMERIC(9)
				)                                
				JOIN 
					OpportunityItems O 
				ON 
					O.numWarehouseItmsID=numWItmsID and O.numOppID=@numOppID
			)X                 
			WHERE 
				numOppID=@numOppID 
				AND numWarehouseItmsDTLID=X.DTLID      
    
			IF NOT (@tintCommitAllocation=2 AND @bitAllocateInventoryOnPickList=1)
			BEGIN
				EXEC USP_ManageSOWorkOrder @numDomainId,@numOppID,@numUserCntID
			END
                     
			EXEC sp_xml_removedocument @hDocItem                                     
		END          
	END
	ELSE                                                                                                                          
	BEGIN

		DECLARE @tempAssignedTo AS NUMERIC(9) = NULL                                                    
		SELECT 
			@tintOppStatus = tintOppStatus,
			@tempAssignedTo=ISNULL(numAssignedTo,0) 
		FROM   
			OpportunityMaster 
		WHERE  
			numOppID = @numOppID

		IF @tempAssignedTo <> @numAssignedTo
		BEGIN
			IF (SELECT COUNT(*) FROM BizDocComission WHERE ISNULL(numOppID,0) = @numOppID AND ISNULL(bitCommisionPaid,0) = 1) > 0
			BEGIN
				RAISERROR('COMMISSION_PAID_AGAINST_BIZDOC',16,1)
			END
		END
 
		--REVERTING BACK THE WAREHOUSE ITEMS                  
		IF @tintOppStatus = 1 AND @tintCommitAllocation=1
		BEGIN               
			EXEC USP_RevertDetailsOpp @numOppID,0,0,@numUserCntID                  
		END                  

		IF LEN(ISNULL(@vcOppRefOrderNo,''))>0
		BEGIN
			UPDATE dbo.OpportunityBizDocs SET vcRefOrderNo=@vcOppRefOrderNo WHERE numOppId=@numOppID
		END
		
		IF EXISTS (SELECT 1 FROM dbo.OpportunityMaster WHERE numOppId = @numOppID AND tintOppType=1 AND isnull(tintOppStatus,0)=1 AND ISNULL(numStatus,0)!= @numStatus)
		BEGIN
			EXEC USP_ManageOpportunityAutomationQueue
					@numOppQueueID = 0, -- numeric(18, 0)
					@numDomainID = @numDomainID, -- numeric(18, 0)
					@numOppId = @numOppID, -- numeric(18, 0)
					@numOppBizDocsId = 0, -- numeric(18, 0)
					@numOrderStatus = @numStatus, -- numeric(18, 0)
					@numUserCntID = @numUserCntID, -- numeric(18, 0)
					@tintProcessStatus = 1, -- tinyint
					@tintMode = 1 -- TINYINT
		END 
						
		UPDATE   
			OpportunityMaster
		SET      
			vcPOppName = @vcPOppName,txtComments = ISNULL(@Comments,''),bitPublicFlag = @bitPublicFlag,numCampainID = @CampaignID,tintSource = @tintSource,tintSourceType=@tintSourceType,
			intPEstimatedCloseDate = @dtEstimatedCloseDate,numModifiedBy = @numUserCntID,bintModifiedDate = GETUTCDATE(),lngPConclAnalysis = @lngPConclAnalysis,monPAmount = @monPAmount,
			tintActive = @tintActive,numSalesOrPurType = @numSalesOrPurType,numStatus = @numStatus,vcOppRefOrderNo=@vcOppRefOrderNo,
			bintOppToOrder=(CASE WHEN @tintOppStatus=0 AND @DealStatus=1 THEN GETUTCDATE() ELSE bintOppToOrder END),bitDiscountType=@bitDiscountType,fltDiscount=@fltDiscount,
			bitBillingTerms=@bitBillingTerms,intBillingDays=@intBillingDays,bitInterestType=@bitInterestType,fltInterest=@fltInterest,tintTaxOperator=@tintTaxOperator,
			numDiscountAcntType=@numDiscountAcntType,numPercentageComplete =@numPercentageComplete,bitUseShippersAccountNo = @bitUseShippersAccountNo,
			intUsedShippingCompany = @intUsedShippingCompany, numContactID=@numContactId,numShipmentMethod=@numShipmentMethod,numShippingService=@numShippingService
			,numPartner=@numPartner,numPartenerContact=@numPartenerContactId,numProjectID=@numProjectID
		WHERE
			numOppId = @numOppID   
	   
		-- UPDATING IF ORGANIZATION IS ASSIGNED TO SOMEONE                                                      
		IF ( @tempAssignedTo <> @numAssignedTo AND @numAssignedTo <> '0' ) 
		BEGIN
			UPDATE
				OpportunityMaster 
			SET	
				numAssignedTo = @numAssignedTo, 
				numAssignedBy = @numUserCntID 
			WHERE 
				numOppId = @numOppID                                                    
		END                                                     
		ELSE IF(@numAssignedTo = 0) 
		BEGIN
			UPDATE 
				OpportunityMaster 
			SET 
				numAssignedTo = 0, 
				numAssignedBy = 0 
			WHERE
				numOppId = @numOppID
		END  
			                                                                    
		-- UPDATING OPP ITEMS
		IF CONVERT(VARCHAR(10),@strItems) <> '' AND @numOppID > 0                                                                         
		BEGIN
			EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems                                      
			   
			IF EXISTS 
			(
				SELECT 
					OI.numoppitemtCode
				FROM 
					OpportunityItems OI
				WHERE 
					numOppID=@numOppID 
					AND OI.numoppitemtCode NOT IN (SELECT numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) WITH (numoppitemtCode NUMERIC(9)))
					AND ISNULL((SELECT COUNT(*) FROM OppWarehouseSerializedItem WHERE numOppId=@numOppID AND numOppItemID=OI.numoppitemtCode),0) > 0
			)
			BEGIN
				RAISERROR('SERIAL/LOT#_ASSIGNED_TO_ITEM_DELETED',16,1)
			END

			DELETE FROM OpportunityKitChildItems WHERE numOppID=@numOppID   
			DELETE FROM OpportunityKitItems WHERE numOppID=@numOppID                  
			DELETE FROM OpportunityItems WHERE numOppID=@numOppID AND numoppitemtCode NOT IN (SELECT numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) WITH (numoppitemtCode NUMERIC(9))) 
	          
			-- DELETE CORRESPONSING TIME AND EXPENSE ENTRIES OF DELETED ITEMS
			DELETE FROM
				TimeAndExpense
			WHERE
				numDomainID=@numDomainId
				AND numOppId=@numOppID
				AND numOppItemID NOT IN (SELECT numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) WITH (numoppitemtCode NUMERIC(9))) 

			--INSERT NEW ADDED ITEMS                        
			INSERT INTO OpportunityItems
			(
				numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,
				[vcItemName],[vcModelID],[vcManufacturer],vcPathForTImage,monVendorCost,numUOMId,bitWorkOrder,numVendorWareHouse,numToWarehouseItemID,numShipmentMethod,
				numSOVendorId,numProjectID,numProjectStageID,vcAttributes,numClassID,monAvgCost,bitItemPriceApprovalRequired,vcAttrValues,numPromotionID,bitPromotionTriggered
				,vcPromotionDetail,numSortOrder,numCost,vcNotes,vcInstruction,bintCompliationDate,dtPlannedStart,numWOAssignedTo,bitMarkupDiscount,ItemReleaseDate,vcChildKitSelectedItems,numWOQty
			)
			SELECT 
				@numOppID,X.numItemCode,dbo.fn_UOMConversion(X.numUOM,numItemCode,@numDomainId,null) * x.numUnitHour,x.monPrice,x.monTotAmount,X.vcItemDesc,X.numWarehouseItmsID,
				X.ItemType,X.DropShip,X.bitDiscountType,X.fltDiscount,X.monTotAmtBefDiscount,X.vcItemName,(SELECT vcModelID FROM item WHERE numItemCode = X.numItemCode),
				(SELECT vcManufacturer FROM item WHERE numItemCode = X.numItemCode),
				(SELECT TOP 1 vcPathForTImage FROM dbo.ItemImages WHERE numItemCode = X.numItemCode AND BitDefault = 1 AND numDomainId = @numDomainId),
				(select isnull(VN.monCost,0) from Vendor VN INNER JOIN Item IT ON IT.numItemCode=VN.numItemCode and VN.numVendorID=IT.numVendorID where VN.numItemCode=X.numItemCode),
				X.numUOM,X.bitWorkOrder,X.numVendorWareHouse,X.numToWarehouseItemID,X.numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,Attributes,
				0,
				(SELECT (CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(I.monAverageCost,0) END) FROM Item I WHERE I.numDomainId=@numDomainId AND I.numItemCode=X.numItemCode), X.bitItemPriceApprovalRequired, X.AttributeIDs,
				X.numPromotionID,X.bitPromotionTriggered,X.vcPromotionDetail,X.numSortOrder,ISNULL(X.numCost,0),ISNULL(vcVendorNotes,'')
				,X.vcInstruction,DATEADD(MINUTE,@ClientTimeZoneOffset,X.bintCompliationDate),DATEADD(MINUTE,@ClientTimeZoneOffset,X.dtPlannedStart),X.numWOAssignedTo,X.bitMarkupDiscount,@dtItemRelease,ISNULL(KitChildItems,''),X.numWOQty
			FROM
			(
				SELECT 
					*
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
				WITH  
				(                      
					numoppitemtCode numeric(9),numItemCode numeric(9),numUnitHour FLOAT,monPrice DECIMAL(30,16),monTotAmount DECIMAL(20,5),vcItemDesc varchar(2000),                                    
					numWarehouseItmsID numeric(9),numToWarehouseItemID numeric(9),Op_Flag tinyint,ItemType varchar(30),DropShip bit,bitDiscountType bit,
					fltDiscount DECIMAL(30,16),monTotAmtBefDiscount DECIMAL(20,5),vcItemName VARCHAR(300),numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),
					numShipmentMethod NUMERIC(9),numSOVendorId numeric(9),numProjectID numeric(9),numProjectStageID numeric(9),Attributes varchar(500), 
					bitItemPriceApprovalRequired BIT,AttributeIDs VARCHAR(500),numPromotionID NUMERIC(18,0),bitPromotionTriggered BIT,vcPromotionDetail VARCHAR(2000)
					,numSortOrder NUMERIC(18,0),numCost DECIMAL(20,5),vcVendorNotes VARCHAR(1000),vcInstruction VARCHAR(1000),bintCompliationDate DATETIME,dtPlannedStart DATETIME
					,numWOAssignedTo NUMERIC(18,0),bitMarkupDiscount BIT,KitChildItems VARCHAR(MAX),numWOQty FLOAT
				)
			)X 
			
			-- UPDATE EXISTING CUSTOMER PART NO
			UPDATE
				CPN
			SET
				CPN.CustomerPartNo = ISNULL(TEMPCPN.CustomerPartNo,'')
			FROM
				CustomerPartNumber CPN
			INNER JOIN
			(
				SELECT 
					numItemCode
					,MIN(CustomerPartNo) AS CustomerPartNo
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item',2)                                                                          
				WITH                       
				(                                                                          
					CustomerPartNo VARCHAR(300), numItemCode NUMERIC(18,0)
				)
				GROUP BY
					numItemCode
			) AS TEMPCPN
			ON
				CPN.numCompanyId = @numCompany
				AND CPN.numItemCode = TEMPCPN.numItemCode
			WHERE
				CPN.numDomainID=@numDomainID
				AND CPN.numCompanyId = @numCompany
				AND LEN(ISNULL(TEMPCPN.CustomerPartNo,'')) > 0
				AND ISNULL(TEMPCPN.CustomerPartNo,'') <> '0'
			
			-- INSERT NEW CUSTOMER PART NO
			INSERT INTO CustomerPartNumber
			(
				numCompanyId, numDomainID, CustomerPartNo, numItemCode 
			)
			SELECT 
				@numCompany, @numDomainID, TEMPCPN.CustomerPartNo,TEMPCPN.numItemCode
			FROM
			(
				SELECT 
					numItemCode
					,MIN(CustomerPartNo) AS CustomerPartNo
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item',2)                                                                          
				WITH                       
				(                                                                          
					CustomerPartNo VARCHAR(300), numItemCode NUMERIC(18,0)
				)
				GROUP BY
					numItemCode
			) TEMPCPN 
			LEFT JOIN
				CustomerPartNumber CPN
			ON
				CPN.numCompanyId = @numCompany
				AND CPN.numItemCode = TEMPCPN.numItemCode
			WHERE
				CPN.CustomerPartNoID IS NULL
				AND LEN(ISNULL(TEMPCPN.CustomerPartNo,'')) > 0
				AND ISNULL(TEMPCPN.CustomerPartNo,'') <> '0'
			
			-- UPDATE ITEMS
			UPDATE 
				OpportunityItems                       
			SET 
				numItemCode=X.numItemCode,numOppId=@numOppID,numUnitHour=dbo.fn_UOMConversion(X.numUOM,X.numItemCode,@numDomainId,null) * x.numUnitHour,                      
				monPrice=x.monPrice,monTotAmount=x.monTotAmount,vcItemDesc=X.vcItemDesc,vcModelID=X.vcModelID,numWarehouseItmsID=X.numWarehouseItmsID,numToWarehouseItemID = X.numToWarehouseItemID,             
				bitDropShip=X.DropShip,bitDiscountType=X.bitDiscountType,fltDiscount=X.fltDiscount,monTotAmtBefDiscount=X.monTotAmtBefDiscount,vcItemName=X.vcItemName,
				numUOMId=numUOM,numVendorWareHouse=X.numVendorWareHouse,numShipmentMethod=X.numShipmentMethod,numSOVendorId=X.numSOVendorId,numProjectId=X.numProjectID,
				numProjectStageId=X.numProjectStageID,vcAttributes=X.Attributes,bitItemPriceApprovalRequired=X.bitItemPriceApprovalRequired,bitWorkOrder=X.bitWorkOrder,vcAttrValues=AttributeIDs,
				numPromotionID=X.numPromotionID,bitPromotionTriggered=X.bitPromotionTriggered,vcPromotionDetail=X.vcPromotionDetail,numSortOrder=X.numSortOrder,numCost=X.numCost,vcNotes=vcVendorNotes
				,bitMarkupDiscount=X.bitMarkupDiscount,ItemRequiredDate=X.ItemRequiredDate,vcChildKitSelectedItems=ISNULL(KitChildItems,'')
			FROM 
			(
				SELECT 
					numoppitemtCode as numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,vcModelID,numWarehouseItmsID,numToWarehouseItemID,DropShip,bitDiscountType,
					fltDiscount,monTotAmtBefDiscount,vcItemName,numUOM,numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,Attributes,
					bitItemPriceApprovalRequired,bitWorkOrder,AttributeIDs,numPromotionID,bitPromotionTriggered,vcPromotionDetail,numSortOrder,ISNULL(numCost,0) AS numCost,ISNULL(vcVendorNotes,'') vcVendorNotes,
					ItemRequiredDate,bitMarkupDiscount,KitChildItems
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=2]',2)                                                                
				WITH  
				(                      
					numoppitemtCode NUMERIC(9),numItemCode NUMERIC(9),numUnitHour FLOAT,monPrice DECIMAL(30,16),monTotAmount DECIMAL(20,5),vcItemDesc VARCHAR(2000),vcModelID VARCHAR(300), numWarehouseItmsID NUMERIC(9), 
					numToWarehouseItemID NUMERIC(18,0),DropShip BIT,bitDiscountType BIT,fltDiscount DECIMAL(30,16),monTotAmtBefDiscount DECIMAL(20,5),vcItemName VARCHAR(300),numUOM NUMERIC(9),
					numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId NUMERIC(9),numProjectID NUMERIC(9),numProjectStageID NUMERIC(9),Attributes VARCHAR(500),
					bitItemPriceApprovalRequired BIT, bitWorkOrder BIT,AttributeIDs VARCHAR(500),numPromotionID NUMERIC(18,0),bitPromotionTriggered BIT,vcPromotionDetail VARCHAR(2000),numSortOrder NUMERIC(18,0),numCost DECIMAL(20,5),vcVendorNotes VARCHAR(1000),
					ItemRequiredDate DATETIME, bitMarkupDiscount BIT,KitChildItems VARCHAR(MAX)
				)
			)X 
			WHERE 
				numoppitemtCode=X.numOppItemID AND numOppId=@numOppID

			--INSERT KIT ITEM HAS ANOTHER KIT AS CHILD THEN SAVE CONFIGURATION OF KIT
			DELETE FROM @TempKitConfiguration

			INSERT INTO @TempKitConfiguration
			(
				numOppItemID,
				numItemCode,
				numKitItemID,
				numKitChildItemID,
				numQty,
				numSequence
			)
			SELECT
				numoppitemtCode,
				numItemCode,
				numKitItemID,
				numChildItemID,
				numQty,
				numSequence
			FROM
				OpportunityItems
			CROSS APPLY
			(
				SELECT 
					Reverse(ParseName(Replace(Reverse(OutParam), '-', '.'), 1)) As numKitItemID
					,Reverse(ParseName(Replace(Reverse(OutParam), '-', '.'), 2)) As numChildItemID
					,Reverse(ParseName(Replace(Reverse(OutParam), '-', '.'), 3)) As numQty
					,Reverse(ParseName(Replace(Reverse(OutParam), '-', '.'), 4)) As numSequence
				FROM  
				(
					SELECT 
						OutParam 
					FROM 
						SplitString(vcChildKitSelectedItems,',')
				) X
			) Y
			WHERE
				numOppId = @numOppID
				AND LEN(ISNULL(vcChildKitSelectedItems,'')) > 0
			ORDER BY
				ISNULL(Y.numSequence,0)

			--INSERT KIT ITEMS 
			INSERT INTO OpportunityKitItems
			(
				numOppId,
				numOppItemID,
				numChildItemID,
				numWareHouseItemId,
				numQtyItemsReq,
				numQtyItemsReq_Orig,
				numUOMId,
				numQtyShipped,
				monAvgCost
			)
			SELECT
				@numOppId,
				OI.numoppitemtCode,
				T1.numKitChildItemID,
				ISNULL((SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=T1.numKitChildItemID AND WareHouseItems.numWareHouseID = W.numWarehouseID ORDER BY numWareHouseItemID),0),
				T1.numQty * OI.numUnitHour,
				T1.numQty,
				I.numBaseUnit,
				0,
				ISNULL(I.monAverageCost,0)
			FROM 
				OpportunityItems OI 
			LEFT JOIN
				WarehouseItems WI
			ON
				OI.numWarehouseItmsID = WI.numWarehouseItemID
			LEFT JOIN
				Warehouses W
			ON
				WI.numWarehouseID = W.numWarehouseID
			INNER JOIN
				@TempKitConfiguration T1
			ON
				T1.numOppItemID=OI.numoppitemtCode
				AND T1.numItemCode = OI.numItemCode
			JOIN
				Item I
			ON
				T1.numKitChildItemID = I.numItemCode
			WHERE 
				OI.numOppId=@numOppId
				AND (SELECT COUNT(*) FROM @TempKitConfiguration t2 WHERE t2.numOppItemID=OI.numoppitemtCode AND t2.numItemCode=OI.numItemCode AND ISNULL(t2.numKitItemID,0) = 0) > 0
			ORDER BY
				ISNULL(T1.numSequence,0)

			INSERT INTO OpportunityKitItems
			(
				numOppId,
				numOppItemID,
				numChildItemID,
				numWareHouseItemId,
				numQtyItemsReq,
				numQtyItemsReq_Orig,
				numUOMId,
				numQtyShipped,
				monAvgCost
			)
			SELECT
				@numOppId,
				OI.numoppitemtCode,
				ID.numChildItemID,
				ISNULL((SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ID.numChildItemID AND WareHouseItems.numWareHouseID = W.numWarehouseID ORDER BY numWareHouseItemID),0),
				(ID.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(ID.numUOMId,ID.numChildItemID,@numDomainID,I.numBaseUnit),1)) * OI.numUnitHour,
				ID.numQtyItemsReq,
				ID.numUOMId,
				0,
				ISNULL(I.monAverageCost,0)
			FROM 
				OpportunityItems OI 
			LEFT JOIN
				WarehouseItems WI
			ON
				OI.numWarehouseItmsID = WI.numWarehouseItemID
			LEFT JOIN
				Warehouses W
			ON
				WI.numWarehouseID = W.numWarehouseID
			JOIN 
				ItemDetails ID 
			ON 
				OI.numItemCode=ID.numItemKitID 
			JOIN
				Item I
			ON
				ID.numChildItemID = I.numItemCode
			WHERE 
				OI.numOppId=@numOppId
				AND (SELECT COUNT(*) FROM @TempKitConfiguration t2 WHERE t2.numOppItemID=OI.numoppitemtCode AND t2.numItemCode=OI.numItemCode AND ISNULL(t2.numKitItemID,0) = 0) = 0
			ORDER BY
				ISNULL(ID.sintOrder,0)

			IF (SELECT 
					COUNT(*) 
				FROM 
					OpportunityKitItems OI 
				INNER JOIN
					Item I
				ON
					OI.numChildItemID = I.numItemCode
				WHERE 
					numOppId=@numOppID
					AND ISNULL(charItemType,0) = 'P'
					AND numWareHouseItemId = 0) > 0
			BEGIN
				RAISERROR('SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS',16,1)
			END

			IF (SELECT 
					COUNT(*) 
				FROM 
				(
					SELECT 
						t1.numOppItemID
						,t1.numItemCode
						,numKitItemID
						,numKitChildItemID
					FROM 
						@TempKitConfiguration t1
					WHERE
						ISNULL(t1.numKitItemID,0) > 0
				) TempChildItems
				INNER JOIN
					OpportunityKitItems OKI
				ON
					OKI.numOppId=@numOppId
					AND OKI.numOppItemID=TempChildItems.numOppItemID
					AND OKI.numChildItemID=TempChildItems.numKitItemID
				INNER JOIN
					OpportunityItems OI
				ON
					OI.numItemCode = TempChildItems.numItemCode
					AND OI.numoppitemtcode = OKI.numOppItemID
				LEFT JOIN
					WarehouseItems WI
				ON
					OI.numWarehouseItmsID = WI.numWarehouseItemID
				LEFT JOIN
					Warehouses W
				ON
					WI.numWarehouseID = W.numWarehouseID
				INNER JOIN
					ItemDetails
				ON
					TempChildItems.numKitItemID = ItemDetails.numItemKitID
					AND TempChildItems.numKitChildItemID = ItemDetails.numChildItemID
				INNER JOIN
					Item 
				ON
					ItemDetails.numChildItemID = Item.numItemCode
				INNER JOIN
					Item IMain
				ON
					OKI.numChildItemID = IMain.numItemCode
				WHERE
					Item.charItemType = 'P'
					AND ISNULL(IMain.bitKitParent,0) = 1
					AND ISNULL((SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ItemDetails.numChildItemID AND numWareHouseID = W.numWarehouseID),0) = 0) > 0
			BEGIN
				RAISERROR('SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS',16,1)
			END

			INSERT INTO OpportunityKitChildItems
			(
				numOppID,
				numOppItemID,
				numOppChildItemID,
				numItemID,
				numWareHouseItemId,
				numQtyItemsReq,
				numQtyItemsReq_Orig,
				numUOMId,
				numQtyShipped,
				monAvgCost
			)
			SELECT 
				@numOppID,
				OKI.numOppItemID,
				OKI.numOppChildItemID,
				ItemDetails.numChildItemID,
				(SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ItemDetails.numChildItemID AND WareHouseItems.numWareHouseID = W.numWarehouseID ORDER BY numWareHouseItemID),
				(ItemDetails.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(ItemDetails.numUOMId,ItemDetails.numChildItemID,@numDomainID,Item.numBaseUnit),1)) * OKI.numQtyItemsReq,
				ItemDetails.numQtyItemsReq,
				ItemDetails.numUOMId,
				0,
				ISNULL(Item.monAverageCost,0)
			FROM
			(
				SELECT 
					t1.numOppItemID
					,t1.numItemCode
					,numKitItemID
					,numKitChildItemID
				FROM 
					@TempKitConfiguration t1
				WHERE
					ISNULL(t1.numKitItemID,0) > 0
			) TempChildItems
			INNER JOIN
				OpportunityKitItems OKI
			ON
				OKI.numOppId=@numOppId
				AND OKI.numOppItemID=TempChildItems.numOppItemID
				AND OKI.numChildItemID=TempChildItems.numKitItemID
			INNER JOIN
				OpportunityItems OI
			ON
				OI.numItemCode = TempChildItems.numItemCode
				AND OI.numoppitemtcode = OKI.numOppItemID
			LEFT JOIN
				WarehouseItems WI
			ON
				OI.numWarehouseItmsID = WI.numWarehouseItemID
			LEFT JOIN
				Warehouses W
			ON
				WI.numWarehouseID = W.numWarehouseID
			INNER JOIN
				ItemDetails
			ON
				TempChildItems.numKitItemID = ItemDetails.numItemKitID
				AND TempChildItems.numKitChildItemID = ItemDetails.numChildItemID
			INNER JOIN
				Item 
			ON
				ItemDetails.numChildItemID = Item.numItemCode                                                  
			                                    
			IF NOT (@tintCommitAllocation=2 AND @bitAllocateInventoryOnPickList=1)
			BEGIN
				EXEC USP_ManageSOWorkOrder @numDomainId,@numOppID,@numUserCntID
			END
			   		                                                                      
			EXEC sp_xml_removedocument @hDocItem                                               
		END     

		IF @tintOppType=1 AND @tintOppStatus=1 --Sales Order
		BEGIN
			IF EXISTS (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID AND ISNULL(numWarehouseItmsID,0) > 0 AND ISNULL(bitDropShip,0) = 0 AND (ISNULL(numUnitHour,0) - ISNULL(numQtyShipped,0)) < 0)
			BEGIN
				RAISERROR('EDITED_ITEMS_QTY_IS_LESS_THEN_SHIPPED_QTY',16,1)
				RETURN
			END

			IF EXISTS (SELECT numoppitemtCode FROM OpportunityItems INNER JOIN WorkOrder ON OpportunityItems.numOppID=WorkOrder.numOppID AND OpportunityItems.numoppitemtCode=WorkOrder.numOppItemID AND ISNULL(WorkOrder.numParentWOID,0)=0 AND WorkOrder.numWOStatus=23184 AND ISNULL(OpportunityItems.numUnitHour,0) > ISNULL(WorkOrder.numQtyItemsReq,0) WHERE OpportunityItems.numOppID=@numOppID)
			BEGIN
				RAISERROR('EDITED_ITEMS_QTY_CAN_NOT_BE_GREATER_THEN_COMPLETED_WORK_ORDER',16,1)
				RETURN
			END
		END
		ELSE IF @tintOppType=2 AND @tintOppStatus=1 --Purchase Order
		BEGIN
			IF EXISTS (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID AND ISNULL(numWarehouseItmsID,0) > 0 AND ISNULL(bitDropShip,0) = 0 AND (ISNULL(numUnitHour,0) - ISNULL(numUnitHourReceived,0)) < 0)
			BEGIN
				RAISERROR('EDITED_ITEMS_QTY_IS_LESS_THEN_RECEIVED_QTY',16,1)
				RETURN
			END
		END

		--FOLLOWING DELETE STATEMENT IS COMMENTED BECUASE NOW WE ARE NOT ALLOWING USER TO EIDT SERIAL/LOT ITEM UNTILL USER REMOVED ASSIGNED SERIAL/LOT NUMBERS
	    --Delete previously added serial lot number added for opportunity
		--DELETE FROM OppWarehouseSerializedItem WHERE numOppID = @numOppID                                                         
	END

	IF @tintOppStatus = 1
	BEGIN
		IF (SELECT COUNT(*) FROM OpportunityItems WHERE numOppId=@numOppID AND ISNULL(numWarehouseItmsID,0) > 0 AND ISNULL(bitDropShip,0)=0) <>
			(SELECT COUNT(*) FROM OpportunityItems INNER JOIN WareHouseItems ON OpportunityItems.numWarehouseItmsID=WareHouseItems.numWareHouseItemID AND OpportunityItems.numItemCode=WareHouseItems.numItemID WHERE numOppId=@numOppID AND OpportunityItems.numWarehouseItmsID > 0 AND ISNULL(bitDropShip,0)=0)
		BEGIN
			RAISERROR('INVALID_ITEM_WAREHOUSES',16,1)
			RETURN
		END
	END

	IF(SELECT 
			COUNT(*) 
		FROM 
			OpportunityBizDocItems OBDI 
		INNER JOIN 
			OpportunityBizDocs OBD 
		ON 
			OBDI.numOppBizDocID=OBD.numOppBizDocsID 
		WHERE 
			OBD.numOppId=@numOppID AND OBDI.numOppItemID NOT IN (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID)) > 0
	BEGIN
		RAISERROR('ITEM_USED_IN_BIZDOC',16,1)
		RETURN
	END

	IF(SELECT 
			COUNT(*)
		FROM
			OpportunityItems OI
		INNER JOIN
		(
			SELECT
				numOppItemID
				,SUM(numUnitHour) numInvoiceBillQty
			FROM 
				OpportunityBizDocItems OBDI 
			INNER JOIN 
				OpportunityBizDocs OBD 
			ON 
				OBDI.numOppBizDocID=OBD.numOppBizDocsID 
			WHERE 
				OBD.numOppId=@numOppID
				AND ISNULL(OBD.bitAuthoritativeBizDocs,0) = 1
			GROUP BY
				numOppItemID
		) AS TEMP
		ON
			TEMP.numOppItemID = OI.numoppitemtCode
		WHERE
			OI.numOppId = @numOppID
			AND OI.numUnitHour < Temp.numInvoiceBillQty
		) > 0
	BEGIN
		RAISERROR('ORDERED_QUANTITY_CAN_NOT_BE_LESS_THEN_INVOICE/BILL_QTY',16,1)
		RETURN
	END

	IF @tintOppType=1 AND @tintCommitAllocation=2 AND @bitAllocateInventoryOnPickList=1
	BEGIN
		IF(SELECT 
				COUNT(*)
			FROM
			(
				SELECT
					numOppItemID
					,SUM(numUnitHour) PackingSlipUnits
				FROM 
					OpportunityBizDocItems OBDI 
				INNER JOIN 
					OpportunityBizDocs OBD 
				ON 
					OBDI.numOppBizDocID=OBD.numOppBizDocsID 
				WHERE 
					OBD.numOppId=@numOppID
					AND OBD.numBizDocId=29397 --Picking Slip
				GROUP BY
					numOppItemID
			) AS TEMP
			INNER JOIN
				OpportunityItems OI
			ON
				TEMP.numOppItemID = OI.numoppitemtCode
			WHERE
				OI.numUnitHour < PackingSlipUnits
			) > 0
		BEGIN
			RAISERROR('ORDERED_QUANTITY_CAN_NOT_BE_LESS_THEN_PACKING_SLIP_QTY',16,1)
			RETURN
		END
	END

	SET @TotalAmount=0                                                           
	SELECT @TotalAmount = SUM(monTotAmount) FROM OpportunityItems WHERE numOppId=@numOppID                                                                          
  
	UPDATE OpportunityMaster SET monPamount=@TotalAmount WHERE numOppId=@numOppID

/*Added by chintan - While creating SO from PO it wasn't puting Item Qty to Allocation. this segment was being called from USP_OppSaveMilestone which No longer in use after changes anoop did. */     
--Updating the warehouse items              
            
			
			DECLARE @bitViolatePrice BIT
			DECLARE @intPendingApprovePending INT

			SET @bitIsInitalSalesOrder=(SELECT bitIsInitalSalesOrder FROM OpportunityMaster WHERE numOppId=@numOppID AND numDomainId=@numDomainID)
			--SET @bitViolatePrice=(SELECT bitMarginPriceViolated FROM Domain WHERE numDomainId=@numDomainID)
			IF EXISTS (SELECT numApprovalRuleID FROM ApprovalProcessItemsClassification WHERE numDomainId=@numDomainID AND ISNULL(bitMarginPriceViolated,0) = 1)
			BEGIN
				SET @bitViolatePrice = 1
			END
			
			IF(@bitViolatePrice=1 AND @bitIsInitalSalesOrder=1 AND @tintClickBtn = 1)
			BEGIN
				UPDATE OpportunityMaster SET tintOppStatus=@DealStatus WHERE numOppId=@numOppID
			END
			ELSE
			BEGIN
				UPDATE OpportunityMaster SET tintOppStatus = @DealStatus WHERE numOppId=@numOppID
				DECLARE @tintShipped AS TINYINT               
				SELECT @tintOppStatus=tintOppStatus,@tintOppType=tintOppType,@tintShipped=tintShipped from OpportunityMaster where numOppID=@numOppID               
				
				IF @tintOppStatus=1 AND @tintCommitAllocation=1             
				BEGIN         
					EXEC USP_UpdatingInventoryonCloseDeal @numOppID,0,@numUserCntID
				END  
				declare @tintCRMType as numeric(9)        
				declare @AccountClosingDate as datetime        

				select @numDivisionID=numDivisionID,@AccountClosingDate=bintAccountClosingDate from OpportunityMaster where numOppID=@numOppID         
				select @tintCRMType=tintCRMType from divisionmaster where numDivisionID =@numDivisionID 

				--When deal is won                                         
				IF @DealStatus = 1 
				BEGIN
					IF @AccountClosingDate IS NULL                   
					BEGIN
						UPDATE OpportunityMaster SET bintAccountClosingDate=GETUTCDATE() WHERE numOppID=@numOppID              
					END

					-- Promote Lead to Account when Sales/Purchase Order is created against it
					IF @tintCRMType=0 --Lead & Order
					BEGIN
						UPDATE 
							divisionmaster 
						SET 
							tintCRMType=2
							,bintLeadProm=getutcdate()
							,bintLeadPromBy=@numUserCntID
							,bintProsProm=getutcdate()
							,bintProsPromBy=@numUserCntID        
						WHERE 
							numDivisionID=@numDivisionID        
					END
					--Promote Prospect to Account
					ELSE IF @tintCRMType=1 
					BEGIN
						UPDATE 
							divisionmaster 
						SET 
							tintCRMType=2
							,bintProsProm=getutcdate()
							,bintProsPromBy=@numUserCntID        
						WHERE 
							numDivisionID=@numDivisionID        
					END
				END         
				--When Deal is Lost
				ELSE IF @DealStatus = 2
				BEGIN
					SELECT @AccountClosingDate=bintAccountClosingDate FROM OpportunityMaster WHERE numOppID=@numOppID         
					
					IF @AccountClosingDate IS NULL                   
					BEGIN
						UPDATE OpportunityMaster SET bintAccountClosingDate=GETUTCDATE() WHERE numOppID=@numOppID
					END
				END 
			END

			/*Added by chintan -  Biz usually automatically promotes the record from Lead/Prospect to Account When a Sales Opp/Order is made against it*/  
			-- Promote Lead to Prospect when Sales/Purchase Opp is created against it
			if @tintCRMType=0 AND @DealStatus = 0 --Lead & Open Opp
			begin        
				update divisionmaster set tintCRMType=1,bintLeadProm=getutcdate(),bintLeadPromBy=@numUserCntID,bintProsProm=getutcdate(),bintProsPromBy=@numUserCntID        
				where numDivisionID=@numDivisionID        
			end        

--Add/Update Address Details
DECLARE @vcStreet  varchar(100),@vcCity  varchar(50),@vcPostalCode  varchar(15),@vcCompanyName  varchar(100),@vcAddressName VARCHAR(50), @vcAltContact VARCHAR(200)      
DECLARE @numState  numeric(9),@numCountry  numeric(9), @numCompanyId  numeric(9), @numContact NUMERIC(18,0)
DECLARE @bitIsPrimary BIT, @bitAltContact BIT;
DECLARE @tintAddressOf TINYINT

select  @vcCompanyName=vcCompanyName,@numCompanyId=div.numCompanyID from CompanyInfo Com                            
join divisionMaster Div                            
on div.numCompanyID=com.numCompanyID                            
where div.numdivisionID=@numDivisionId

IF EXISTS (SELECT numOppId FROM OpportunityMaster WHERE numOppId=@numOppID AND ISNULL(tintBillToType,0) = 0 AND ISNULL(numBillToAddressID,0) = 0)
BEGIN
	SELECT 
		@vcStreet=ISNULL(vcStreet,'')
		,@vcCity=ISNULL(vcCity,'')
		,@vcPostalCode=ISNULL(vcPostalCode,'')
		,@numState=ISNULL(numState,0)
		,@numCountry=ISNULL(numCountry,0)
		,@bitIsPrimary=bitIsPrimary
		,@vcAddressName=vcAddressName
		,@tintAddressOf=ISNULL(tintAddressOf,0)
		,@numContact=numContact
		,@bitAltContact=bitAltContact
		,@vcAltContact=vcAltContact
    FROM 
		dbo.AddressDetails AD
	WHERE 
		AD.numDomainID=@numDomainID 
		AND AD.numRecordID=@numDivisionID 
		AND AD.tintAddressOf = 2 
		AND AD.tintAddressType = 1 
		AND AD.bitIsPrimary=1       

	UPDATE OpportunityMaster SET tintBillToType=2,numBillToAddressID=0 WHERE numOppId=@numOppID

  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 0, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = @vcCompanyName, --  varchar(100)
	@numCompanyId = 0, --  numeric(9, 0)
	@vcAddressName = @vcAddressName,
	@bitCalledFromProcedure = 1,
	@numContact = @numContact,
	@bitAltContact = @bitAltContact,
	@vcAltContact = @vcAltContact
END

IF EXISTS (SELECT numOppId FROM OpportunityMaster WHERE numOppId=@numOppID AND ISNULL(tintShipToType,0) = 0 AND ISNULL(numShipToAddressID,0) = 0)
BEGIN
	SELECT 
		@vcStreet=ISNULL(vcStreet,'')
		,@vcCity=ISNULL(vcCity,'')
		,@vcPostalCode=ISNULL(vcPostalCode,'')
		,@numState=ISNULL(numState,0)
		,@numCountry=ISNULL(numCountry,0)
		,@bitIsPrimary=bitIsPrimary
		,@vcAddressName=vcAddressName
		,@tintAddressOf=ISNULL(tintAddressOf,0)
		,@numContact=numContact
		,@bitAltContact=bitAltContact
		,@vcAltContact=vcAltContact
    FROM 
		dbo.AddressDetails AD
	WHERE 
		AD.numDomainID=@numDomainID 
		AND AD.numRecordID=@numDivisionID 
		AND AD.tintAddressOf = 2 
		AND AD.tintAddressType = 2 
		AND AD.bitIsPrimary=1       

	UPDATE OpportunityMaster SET tintShipToType=2,numShipToAddressID=0 WHERE numOppId=@numOppID

  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 1, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = @vcCompanyName, --  varchar(100)
	@numCompanyId =0, --  numeric(9, 0)
	@vcAddressName = @vcAddressName,
	@bitCalledFromProcedure = 1,
	@numContact = @numContact,
	@bitAltContact = @bitAltContact,
	@vcAltContact = @vcAltContact
END

--Bill Address
IF @numBillAddressId>0
BEGIN
	SELECT 
		@vcStreet=ISNULL(vcStreet,'')
		,@vcCity=ISNULL(vcCity,'')
		,@vcPostalCode=ISNULL(vcPostalCode,'')
		,@numState=ISNULL(numState,0)
		,@numCountry=ISNULL(numCountry,0)
		,@bitIsPrimary=bitIsPrimary
		,@vcAddressName=vcAddressName
		,@tintAddressOf=ISNULL(tintAddressOf,0)
		,@numContact=numContact
		,@bitAltContact=bitAltContact
		,@vcAltContact=vcAltContact
    FROM 
		dbo.AddressDetails 
	WHERE 
		numDomainID = @numDomainID 
		AND numAddressID = @numBillAddressId

	IF @tintAddressOf = 2 AND @tintOppType = 2
	BEGIN
		SELECT
			@vcCompanyName = ISNULL(vcCompanyName,'')
			,@numCompanyId = ISNULL(CompanyInfo.numCompanyId,0)
		FROM
			AddressDetails
		INNER JOIN
			DivisionMaster 
		ON
			AddressDetails.numRecordID = DivisionMaster.numDivisionID
		INNER JOIN
			CompanyInfo
		ON
			DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
		WHERE 
			AddressDetails.numDomainID = @numDomainID 
			AND numAddressID = @numBillAddressId
	END

	UPDATE OpportunityMaster SET tintBillToType=2,numBillToAddressID=0 WHERE numOppId=@numOppID

  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 0, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = @vcCompanyName, --  varchar(100)
	@numCompanyId = 0, --  numeric(9, 0)
	@vcAddressName = @vcAddressName,
	@bitCalledFromProcedure = 1,
	@numContact = @numContact,
	@bitAltContact = @bitAltContact,
	@vcAltContact = @vcAltContact
END
  
--Ship Address
IF @intUsedShippingCompany = 92 -- WILL CALL (PICK UP FROM Warehouse)
BEGIN
	IF EXISTS (SELECT numWareHouseID FROM Warehouses WHERE Warehouses.numDomainID = @numDomainID AND numWareHouseID = @numWillCallWarehouseID AND ISNULL(numAddressID,0) > 0)
	BEGIN
		SELECT  
			@vcStreet=ISNULL(vcStreet,'')
			,@vcCity=ISNULL(vcCity,'')
			,@vcPostalCode=ISNULL(AddressDetails.vcPostalCode,'')
			,@numState=ISNULL(numState,0)
			,@numCountry=ISNULL(numCountry,0)
			,@vcAddressName=vcAddressName
		FROM 
			dbo.Warehouses 
		INNER JOIN
			AddressDetails
		ON
			Warehouses.numAddressID = AddressDetails.numAddressID
		WHERE 
			Warehouses.numDomainID = @numDomainID 
			AND numWareHouseID = @numWillCallWarehouseID
	END
	ELSE
	BEGIN
	SELECT  
		@vcStreet=ISNULL(vcWStreet,'')
		,@vcCity=ISNULL(vcWCity,'')
		,@vcPostalCode=ISNULL(vcWPinCode,'')
		,@numState=ISNULL(numWState,0)
		,@numCountry=ISNULL(numWCountry,0)
		,@vcAddressName=''
	FROM 
		dbo.Warehouses 
	WHERE 
		numDomainID = @numDomainID 
		AND numWareHouseID = @numWillCallWarehouseID
	END

	UPDATE OpportunityMaster SET tintShipToType=2 WHERE numOppId=@numOppID
 
  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 1, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = '', --  varchar(100)
	@numCompanyId =0, --  numeric(9, 0)
	@vcAddressName = @vcAddressName,
	@bitCalledFromProcedure = 1,
	@numContact = 0,
	@bitAltContact = 0,
	@vcAltContact = ''
END
ELSE IF @numShipAddressId>0
BEGIN
	SELECT
		@vcStreet=ISNULL(vcStreet,'')
		,@vcCity=ISNULL(vcCity,'')
		,@vcPostalCode=ISNULL(vcPostalCode,'')
		,@numState=ISNULL(numState,0)
		,@numCountry=ISNULL(numCountry,0)
		,@bitIsPrimary=bitIsPrimary
		,@vcAddressName=vcAddressName
		,@tintAddressOf=ISNULL(tintAddressOf,0)
		,@numContact=numContact
		,@bitAltContact=bitAltContact
		,@vcAltContact=vcAltContact
    FROM  
		dbo.AddressDetails 
	WHERE 
		numDomainID = @numDomainID 
		AND numAddressID = @numShipAddressId
 

	IF @tintAddressOf = 2 AND @tintOppType = 2
	BEGIN
		SELECT
			@vcCompanyName = ISNULL(vcCompanyName,'')
			,@numCompanyId = ISNULL(CompanyInfo.numCompanyId,0)
		FROM
			AddressDetails
		INNER JOIN
			DivisionMaster 
		ON
			AddressDetails.numRecordID = DivisionMaster.numDivisionID
		INNER JOIN
			CompanyInfo
		ON
			DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
		WHERE 
			AddressDetails.numDomainID = @numDomainID 
			AND numAddressID = @numShipAddressId
	END

	UPDATE OpportunityMaster SET tintShipToType=2,numShipToAddressID=0 WHERE numOppId=@numOppID

  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 1, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = @vcCompanyName, --  varchar(100)
	@numCompanyId =@numCompanyId, --  numeric(9, 0)
	@vcAddressName = @vcAddressName,
	@bitCalledFromProcedure = 1,
	@numContact = @numContact,
	@bitAltContact = @bitAltContact,
	@vcAltContact = @vcAltContact
END

-- IMPORTANT:KEEP THIS BELOW ADDRESS UPDATE LOGIC
-- GET TAX APPLICABLE TO DIVISION WHEN ORDER IS CREATED AFTER THAT NOT NEED TO CHANGE IT
IF @bitNewOrder = 1
BEGIN
	--INSERT TAX FOR DIVISION   
	IF @tintOppType=1 OR (@tintOppType=2 and (select isnull(bitPurchaseTaxCredit,0) from Domain where numDomainId=@numDomainId)=1)
	BEGIN
		INSERT INTO dbo.OpportunityMasterTaxItems 
		(
			numOppId,
			numTaxItemID,
			fltPercentage,
			tintTaxType,
			numTaxID
		) 
		SELECT 
			@numOppID,
			TI.numTaxItemID,
			TEMPTax.decTaxValue,
			TEMPTax.tintTaxType,
			0
		FROM 
			TaxItems TI 
		JOIN 
			DivisionTaxTypes DTT 
		ON 
			TI.numTaxItemID = DTT.numTaxItemID
		CROSS APPLY
		(
			SELECT
				decTaxValue,
				tintTaxType
			FROM
				dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,TI.numTaxItemID,@numOppId,0,NULL)
		) AS TEMPTax
		WHERE 
			DTT.numDivisionID=@numDivisionId 
			AND DTT.bitApplicable=1
		UNION 
		SELECT 
			@numOppID,
			0,
			TEMPTax.decTaxValue,
			TEMPTax.tintTaxType,
			0
		FROM 
			dbo.DivisionMaster 
		CROSS APPLY
		(
			SELECT
				decTaxValue,
				tintTaxType
			FROM
				dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,0,@numOppId,1,NULL)
		) AS TEMPTax
		WHERE 
			bitNoTax=0 
			AND numDivisionID=@numDivisionID	
		UNION 
		SELECT
			@numOppId
			,1
			,decTaxValue
			,tintTaxType
			,numTaxID
		FROM
			dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,1,@numOppId,1,NULL)		
	END	
END

--INSERT TAX FOR DIVISION   
IF @tintOppType=1 OR (@tintOppType=2 and (select isnull(bitPurchaseTaxCredit,0) from Domain where numDomainId=@numDomainId)=1)
BEGIN
--Delete Tax for Opportunity Items if item deleted 
DELETE FROM OpportunityItemsTaxItems WHERE numOppId=@numOppId AND numOppItemID NOT IN (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID)

--Insert Tax for Opportunity Items
INSERT INTO dbo.OpportunityItemsTaxItems 
(
	numOppId,
	numOppItemID,
	numTaxItemID,
	numTaxID
) 
SELECT 
	@numOppId,
	OI.numoppitemtCode,
	TI.numTaxItemID,
	0
FROM 
	dbo.OpportunityItems OI 
JOIN 
	dbo.ItemTax IT 
ON 
	OI.numItemCode=IT.numItemCode 
JOIN
	TaxItems TI 
ON 
	TI.numTaxItemID = IT.numTaxItemID 
WHERE 
	OI.numOppId=@numOppID 
	AND IT.bitApplicable=1 
	AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
UNION
SELECT 
	@numOppId,
	OI.numoppitemtCode,
	0,
	0
FROM 
	dbo.OpportunityItems OI 
JOIN 
	dbo.Item I 
ON 
	OI.numItemCode=I.numItemCode
WHERE 
	OI.numOppId=@numOppID 
	AND I.bitTaxable=1 
	AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
UNION
SELECT
	@numOppId,
	OI.numoppitemtCode,
	1,
	TD.numTaxID
FROM
	dbo.OpportunityItems OI 
INNER JOIN
	ItemTax IT
ON
	IT.numItemCode = OI.numItemCode
INNER JOIN
	TaxDetails TD
ON
	TD.numTaxID = IT.numTaxID
	AND TD.numDomainId = @numDomainId
WHERE
	OI.numOppId = @numOppID
	AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)

END

	UPDATE 
		OI
	SET
		OI.vcInclusionDetail = [dbo].[GetOrderAssemblyKitInclusionDetails](OI.numOppID,OI.numoppitemtCode,OI.numUnitHour,1,1)
	FROM 
		OpportunityItems OI
	INNER JOIN
		Item I
	ON
		OI.numItemCode = I.numItemCode
	WHERE
		numOppId=@numOppID
		AND ISNULL(I.bitKitParent,0) = 1
  
	UPDATE OpportunityMaster SET monDealAmount=dbo.GetDealAmount(@numOppID,getdate(),0) WHERE numOppId=@numOppID

	IF @tintOppType=1 AND @tintOppStatus=1 
	BEGIN
		EXEC USP_SalesFulfillmentQueue_Save @numDomainID,@numUserCntID,@numOppID,@numStatus
		EXEC USP_OpportunityMaster_CreateBackOrderPO @numDomainID,@numUserCntID,@numOppID
	END

	IF EXISTS (SELECT 
					numoppitemtCode 
				FROM 
					OpportunityItems 
				INNER JOIN 
					Item 
				ON 
					OpportunityItems.numItemCode=Item.numItemCode 
				WHERE 
					numOppId=@numOppID 
					AND charItemType='P' 
					AND ISNULL(numWarehouseItmsID,0) = 0 
					AND ISNULL(bitDropShip,0) = 0)
	BEGIN
		RAISERROR('WAREHOUSE_REQUIRED',16,1)
	END

	IF (SELECT COUNT(*) FROM WorkOrder WHERE numDomainID=@numDomainID AND numOppId=@numOppId AND ISNULL(numOppItemID,0) > 0 AND numOppItemID NOT IN (SELECT OIInner.numoppitemtCode FROM OpportunityItems OIInner WHERE OIInner.numOppId=@numOppID)) > 0
	BEGIN
		RAISERROR ( 'WORK_ORDER_EXISTS', 16, 1 ) ;
	END

	IF (SELECT 
			COUNT(*) 
		FROM 
			WorkOrder 
		INNER JOIN 
			OpportunityItems 
		ON 
			OpportunityItems.numOppId=@numOppID
			AND OpportunityItems.numoppitemtCode=WorkOrder.numOppItemID
		WHERE 
			WorkOrder.numDomainID=@numDomainID 
			AND WorkOrder.numOppId=@numOppID
			AND WorkOrder.numWareHouseItemId <> OpportunityItems.numWarehouseItmsID) > 0
	BEGIN
		RAISERROR ('CAN_NOT_CHANGE_ASSEMBLY_WAREHOUSE_WORK_ORDER_EXISTS', 16, 1 ) ;
	END

	IF @tintOppType = 1 AND @tintOppStatus=1
	BEGIN
		EXEC USP_OpportunityItems_CheckWarehouseMapping @numDomainID,@numOppID
	END
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SET @numOppID = 0

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityMaster_DisassociatePO')
DROP PROCEDURE USP_OpportunityMaster_DisassociatePO
GO
CREATE PROCEDURE [dbo].[USP_OpportunityMaster_DisassociatePO]
	@numDomainID NUMERIC(18,0)
	,@numOppID NUMERIC(18,0)
	,@vcOppItemIds VARCHAR(MAX)
AS
BEGIN
	IF ISNULL(@vcOppItemIds,'') <> ''
	BEGIN
		IF EXISTS (SELECT numComissionID FROM BizDocComission WHERE numDomainId=@numDomainID AND ISNULL(monCommissionPaid,0) <> 0 AND numOppItemID IN (SELECT Id FROM dbo.SplitIDs(@vcOppItemIds,',')))
		BEGIN
			RAISERROR('COMMISSION_PAID',16,1)
		END

		IF EXISTS (SELECT numComissionID FROM BizDocComission INNER JOIN OpportunityBizDocItems OBDI ON BizDocComission.numOppBizDocItemID=OBDI.numOppBizDocItemID WHERE numDomainId=@numDomainID AND ISNULL(monCommissionPaid,0) <> 0 AND OBDI.numOppItemID IN (SELECT Id FROM dbo.SplitIDs(@vcOppItemIds,',')))
		BEGIN
			RAISERROR('COMMISSION_PAID',16,1)
		END

		DELETE FROM SalesOrderLineItemsPOLinking WHERE numDomainId=@numDomainID AND numSalesOrderID=@numOppID AND numSalesOrderItemID IN (SELECT Id FROM dbo.SplitIDs(@vcOppItemIds,','))
	END
END
GO
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                                                        
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityMaster_GetSOBackOrderItems')
DROP PROCEDURE USP_OpportunityMaster_GetSOBackOrderItems
GO
CREATE PROCEDURE [dbo].[USP_OpportunityMaster_GetSOBackOrderItems]
(
	@numDomainID NUMERIC(18,0)
	,@numOppID NUMERIC(18,0)
)
AS
BEGIN
	IF NOT EXISTS (SELECT * FROM OpportunityMaster WHERE numDomainId=@numDomainID AND numOppId=@numOppID)
	BEGIN
		RAISERROR('INVALID_OPPID',16,1)
		RETURN
	END 

	DECLARE @tintDefaultCost AS NUMERIC(18,0)
	DECLARE @bitReOrderPoint AS BIT
	DECLARE @numReOrderPointOrderStatus AS NUMERIC(18,0)
	DECLARE @tintOppStautsForAutoPOBackOrder AS TINYINT
	DECLARE @tintUnitsRecommendationForAutoPOBackOrder AS TINYINT
	DECLARE @bitIncludeRequisitions AS BIT

	SELECT 
		@tintDefaultCost = ISNULL(numCost,0)
		,@bitReOrderPoint=ISNULL(bitReOrderPoint,0)
		,@numReOrderPointOrderStatus=ISNULL(numReOrderPointOrderStatus,0)
		,@tintOppStautsForAutoPOBackOrder=ISNULL(tintOppStautsForAutoPOBackOrder,0)
		,@tintUnitsRecommendationForAutoPOBackOrder=ISNULL(tintUnitsRecommendationForAutoPOBackOrder,1)
		,@bitIncludeRequisitions=ISNULL(bitIncludeRequisitions,0)
	FROM 
		Domain 
	WHERE 
		numDomainId = @numDomainID


	SELECT
		@numReOrderPointOrderStatus AS numReOrderPointOrderStatus
		,@tintOppStautsForAutoPOBackOrder AS tintOppStautsForAutoPOBackOrder
		,@tintUnitsRecommendationForAutoPOBackOrder AS tintUnitsRecommendationForAutoPOBackOrder

	IF @tintOppStautsForAutoPOBackOrder IN (0,1)
	BEGIN
		SELECT
			OI.numItemCode
			,CONCAT(I.vcItemName, CASE WHEN dbo.fn_GetAttributes(OI.numWarehouseItmsID,0) <> '' THEN CONCAT(' (',dbo.fn_GetAttributes(OI.numWarehouseItmsID,0),')') ELSE '' END) AS vcItemName
			,I.vcModelID
			,(CASE WHEN ISNULL(I.numItemGroup,0) > 0 AND bitMatrix = 0 THEN ISNULL(WI.vcWHSKU,ISNULL(I.vcSKU,''))  ELSE ISNULL(I.vcSKU,'') END) vcSKU
			,V.vcNotes
			,I.charItemType
			,ISNULL((SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numDomainId=@numDomainID AND numItemCode=I.numItemCode AND bitDefault =1), '') AS vcPathForTImage
			,ISNULL(OI.numWarehouseItmsID,0) AS numWarehouseItemID
			,(CASE 
				WHEN @tintUnitsRecommendationForAutoPOBackOrder=1 --Add �Re-Order Qty� or �Vendor Min Order Qty� (whichever is greater) + �Back Order Qty� - �On Order + Requisitions�
				THEN 
					(CASE WHEN ISNULL(I.fltReorderQty,0) > ISNULL(v.intMinQty,0) THEN ISNULL(I.fltReorderQty,0) ELSE ISNULL(v.intMinQty,0) END) 
					+ ISNULL(WI.numBackOrder,0) - (ISNULL(numOnOrder,0) + (CASE WHEN @bitIncludeRequisitions = 1 THEN ISNULL((SELECT 
																						SUM(numUnitHour) 
																					FROM 
																						OpportunityItems 
																					INNER JOIN 
																						OpportunityMaster 
																					ON 
																						OpportunityItems.numOppID=OpportunityMaster.numOppId 
																					WHERE 
																						OpportunityMaster.numDomainId=@numDomainID 
																						AND OpportunityMaster.tintOppType=2 
																						AND OpportunityMaster.tintOppStatus=0 
																						AND OpportunityItems.numItemCode=OI.numItemCode 
																						AND OpportunityItems.numWarehouseItmsID=OI.numWarehouseItmsID),0) ELSE 0 END))
				WHEN @tintUnitsRecommendationForAutoPOBackOrder=2 --Add �Re-Order Qty� or �Vendor Min Order Qty� or �Back-Order Qty� (whichever is greater) - �On Order + Requisitions�
				THEN 
					(CASE
						WHEN ISNULL(I.fltReorderQty,0) >= ISNULL(v.intMinQty,0) AND ISNULL(I.fltReorderQty,0) >= ISNULL(WI.numBackOrder,0) THEN ISNULL(I.fltReorderQty,0)
						WHEN ISNULL(v.intMinQty,0) >= ISNULL(I.fltReorderQty,0) AND ISNULL(v.intMinQty,0) >= ISNULL(WI.numBackOrder,0) THEN ISNULL(v.intMinQty,0)
						WHEN ISNULL(WI.numBackOrder,0) >= ISNULL(I.fltReorderQty,0) AND ISNULL(WI.numBackOrder,0) >= ISNULL(v.intMinQty,0) THEN ISNULL(WI.numBackOrder,0)
						ELSE ISNULL(I.fltReorderQty,0)
					END) - (ISNULL(numOnOrder,0) + (CASE WHEN @bitIncludeRequisitions = 1 THEN ISNULL((SELECT 
																SUM(numUnitHour) 
															FROM 
																OpportunityItems 
															INNER JOIN 
																OpportunityMaster 
															ON 
																OpportunityItems.numOppID=OpportunityMaster.numOppId 
															WHERE 
																OpportunityMaster.numDomainId=@numDomainID 
																AND OpportunityMaster.tintOppType=2 
																AND OpportunityMaster.tintOppStatus=0 
																AND OpportunityItems.numItemCode=OI.numItemCode 
																AND OpportunityItems.numWarehouseItmsID=OI.numWarehouseItmsID),0) ELSE 0 END))
				WHEN @tintUnitsRecommendationForAutoPOBackOrder=3 --Add �On-Order Qty� � + Requisitions� - �Back Order� + �Re-Order Qty� or �Vendor Min Order Qty� (whichever is greater)
				THEN 
					(ISNULL(numOnOrder,0) + (CASE WHEN @bitIncludeRequisitions = 1 THEN ISNULL((SELECT 
																SUM(numUnitHour) 
															FROM 
																OpportunityItems 
															INNER JOIN 
																OpportunityMaster 
															ON 
																OpportunityItems.numOppID=OpportunityMaster.numOppId 
															WHERE 
																OpportunityMaster.numDomainId=@numDomainID 
																AND OpportunityMaster.tintOppType=2 
																AND OpportunityMaster.tintOppStatus=0 
																AND OpportunityItems.numItemCode=OI.numItemCode 
																AND OpportunityItems.numWarehouseItmsID=OI.numWarehouseItmsID),0) ELSE 0 END)) - ISNULL(WI.numBackOrder,0) + (CASE
					WHEN ISNULL(I.fltReorderQty,0) >= ISNULL(v.intMinQty,0) THEN ISNULL(I.fltReorderQty,0) ELSE ISNULL(v.intMinQty,0) END)
				ELSE ISNULL(WI.numBackOrder,0) + ISNULL(V.intMinQty,0) 
			END) AS numUnitHour
			,ISNULL(WI.numBackOrder,0) AS numBackOrder
			,ISNULL(I.numBaseUnit,0) AS numUOMID
			,dbo.fn_UOMConversion(OI.numUOMId, OI.numItemCode,OM.numDomainId,I.numBaseUnit) AS fltUOMConversionFactor 
			,ISNULL(I.numVendorID, 0) AS numVendorID
			,CASE WHEN @tintDefaultCost = 3 THEN (ISNULL(V.monCost, 0) / dbo.fn_UOMConversion(I.numPurchaseUnit,I.numItemCode,I.numDomainID,I.numBaseUnit)) ELSE ISNULL(I.monAverageCost,0) END AS monCost
			,ISNULL(V.intMinQty,0) AS intMinQty
			,ISNULL(@tintDefaultCost,0) AS tintDefaultCost
			,ISNULL(dbo.fn_UOMConversion(I.numPurchaseUnit,I.numItemCode,I.numDomainID,I.numBaseUnit),1) AS fltUOMConversionFactorForPurchase
		FROM    
			dbo.OpportunityItems OI
			INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OI.numOppId
			INNER JOIN dbo.Item I ON I.numItemCode = OI.numItemCode
			INNER JOIN WarehouseItems WI ON OI.numWarehouseItmsID = WI.numWareHouseItemID
			LEFT JOIN Vendor V ON V.numVendorID = I.numVendorID	AND I.numItemCode = V.numItemCode
		WHERE   
			OI.numOppId = @numOppID
			AND OM.numDomainId = @numDomainId
			AND ISNULL(@bitReOrderPoint,0) = 1
			AND (ISNULL((SELECT SUM(WIInner.numOnHand) FROM WarehouseItems WIInner WHERE WIInner.numItemID = WI.numItemID AND WIInner.numWareHouseID=WI.numWareHouseID),0) + ISNULL(WI.numOnOrder,0) <= ISNULL(WI.numReOrder,0))
			AND OM.tintOppType = 1
			AND OM.tintOppStatus=1
			AND ISNULL(I.bitAssembly, 0) = 0
			AND ISNULL(I.bitKitParent, 0) = 0
			AND ISNULL(OI.bitDropship,0) = 0
			AND ISNULL(WI.numBackorder,0) > 0
		UNION
		SELECT
			IChild.numItemCode
			,CONCAT(IChild.vcItemName, CASE WHEN dbo.fn_GetAttributes(WI.numWareHouseItemID,0) <> '' THEN CONCAT(' (',dbo.fn_GetAttributes(WI.numWareHouseItemID,0),')') ELSE '' END) AS vcItemName
			,IChild.vcModelID
			,(CASE WHEN ISNULL(IChild.numItemGroup,0) > 0 AND IChild.bitMatrix = 0 THEN ISNULL(WI.vcWHSKU,ISNULL(IChild.vcSKU,''))  ELSE ISNULL(IChild.vcSKU,'') END) vcSKU
			,V.vcNotes
			,IChild.charItemType
			,ISNULL((SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numDomainId=@numDomainID AND numItemCode=IChild.numItemCode AND bitDefault =1), '') AS vcPathForTImage
			,ISNULL(WI.numWareHouseItemID,0) AS numWarehouseItemID
			,(CASE 
				WHEN @tintUnitsRecommendationForAutoPOBackOrder=1 --Add �Re-Order Qty� or �Vendor Min Order Qty� (whichever is greater) + �Back Order Qty� - �On Order + Requisitions�
				THEN 
					(CASE WHEN ISNULL(IChild.fltReorderQty,0) > ISNULL(v.intMinQty,0) THEN ISNULL(IChild.fltReorderQty,0) ELSE ISNULL(v.intMinQty,0) END) 
					+ ISNULL(WI.numBackOrder,0) - (ISNULL(numOnOrder,0) + (CASE WHEN @bitIncludeRequisitions = 1 THEN ISNULL((SELECT 
																						SUM(numUnitHour) 
																					FROM 
																						OpportunityItems 
																					INNER JOIN 
																						OpportunityMaster 
																					ON 
																						OpportunityItems.numOppID=OpportunityMaster.numOppId 
																					WHERE 
																						OpportunityMaster.numDomainId=@numDomainID 
																						AND OpportunityMaster.tintOppType=2 
																						AND OpportunityMaster.tintOppStatus=0 
																						AND OpportunityItems.numItemCode=IChild.numItemCode 
																						AND OpportunityItems.numWarehouseItmsID=WI.numWareHouseItemID),0) ELSE 0 END))
				WHEN @tintUnitsRecommendationForAutoPOBackOrder=2 --Add �Re-Order Qty� or �Vendor Min Order Qty� or �Back-Order Qty� (whichever is greater) - �On Order + Requisitions�
				THEN 
					(CASE
						WHEN ISNULL(IChild.fltReorderQty,0) >= ISNULL(v.intMinQty,0) AND ISNULL(IChild.fltReorderQty,0) >= ISNULL(WI.numBackOrder,0) THEN ISNULL(IChild.fltReorderQty,0)
						WHEN ISNULL(v.intMinQty,0) >= ISNULL(IChild.fltReorderQty,0) AND ISNULL(v.intMinQty,0) >= ISNULL(WI.numBackOrder,0) THEN ISNULL(v.intMinQty,0)
						WHEN ISNULL(WI.numBackOrder,0) >= ISNULL(IChild.fltReorderQty,0) AND ISNULL(WI.numBackOrder,0) >= ISNULL(v.intMinQty,0) THEN ISNULL(WI.numBackOrder,0)
						ELSE ISNULL(IChild.fltReorderQty,0)
					END) - (ISNULL(numOnOrder,0) + (CASE WHEN @bitIncludeRequisitions = 1 THEN ISNULL((SELECT 
																SUM(numUnitHour) 
															FROM 
																OpportunityItems 
															INNER JOIN 
																OpportunityMaster 
															ON 
																OpportunityItems.numOppID=OpportunityMaster.numOppId 
															WHERE 
																OpportunityMaster.numDomainId=@numDomainID 
																AND OpportunityMaster.tintOppType=2 
																AND OpportunityMaster.tintOppStatus=0 
																AND OpportunityItems.numItemCode=IChild.numItemCode 
																AND OpportunityItems.numWarehouseItmsID=WI.numWareHouseItemID),0) ELSE 0 END))
				WHEN @tintUnitsRecommendationForAutoPOBackOrder=3 --Add �On-Order Qty� � + Requisitions� - �Back Order� + �Re-Order Qty� or �Vendor Min Order Qty� (whichever is greater)
				THEN 
					(ISNULL(numOnOrder,0) + (CASE WHEN @bitIncludeRequisitions = 1 THEN ISNULL((SELECT 
																SUM(numUnitHour) 
															FROM 
																OpportunityItems 
															INNER JOIN 
																OpportunityMaster 
															ON 
																OpportunityItems.numOppID=OpportunityMaster.numOppId 
															WHERE 
																OpportunityMaster.numDomainId=@numDomainID 
																AND OpportunityMaster.tintOppType=2 
																AND OpportunityMaster.tintOppStatus=0 
																AND OpportunityItems.numItemCode=IChild.numItemCode 
																AND OpportunityItems.numWarehouseItmsID=WI.numWareHouseItemID),0) ELSE 0 END)) - ISNULL(WI.numBackOrder,0) + (CASE
					WHEN ISNULL(IChild.fltReorderQty,0) >= ISNULL(v.intMinQty,0) THEN ISNULL(IChild.fltReorderQty,0) ELSE ISNULL(v.intMinQty,0) END)
				ELSE ISNULL(WI.numBackOrder,0) + ISNULL(V.intMinQty,0) 
			END) AS numUnitHour
			,ISNULL(WI.numBackOrder,0) AS numBackOrder
			,ISNULL(IChild.numBaseUnit,0) AS numUOMID
			,dbo.fn_UOMConversion(OKI.numUOMId, IChild.numItemCode,OM.numDomainId,IChild.numBaseUnit) AS fltUOMConversionFactor 
			,ISNULL(IChild.numVendorID, 0) AS numVendorID
			,CASE WHEN @tintDefaultCost = 3 OR @tintDefaultCost = 2 THEN (ISNULL(V.monCost, 0) / dbo.fn_UOMConversion(IChild.numPurchaseUnit,IChild.numItemCode,IChild.numDomainID,IChild.numBaseUnit)) ELSE ISNULL(IChild.monAverageCost,0) END AS monCost
			,ISNULL(V.intMinQty,0) AS intMinQty
			,ISNULL(@tintDefaultCost,0) AS tintDefaultCost
			,ISNULL(dbo.fn_UOMConversion(IChild.numPurchaseUnit,IChild.numItemCode,IChild.numDomainID,IChild.numBaseUnit),1) AS fltUOMConversionFactorForPurchase
		FROM
			OpportunityItems OI
		INNER JOIN 
			dbo.OpportunityMaster OM 
		ON 
			OM.numOppId = OI.numOppId
		INNER JOIN
			Item I
		ON
			OI.numItemCode = I.numItemCode
		INNER JOIN
			OpportunityKitItems OKI
		ON
			OI.numoppitemtCode = OKI.numOppItemID
		INNER JOIN
			Item IChild
		ON
			OKI.numChildItemID = IChild.numItemCode
			AND ISNULL(IChild.bitKitParent,0) = 0
		INNER JOIN
			Vendor V
		ON
			IChild.numVendorID = V.numVendorID
			AND IChild.numItemCode = V.numItemCode
		INNER JOIN
			WareHouseItems WI
		ON
			OKI.numWareHouseItemId = WI.numWareHouseItemID
		WHERE
			OI.numOppId=@numOppID
			AND OM.tintOppType = 1
			AND OM.tintOppStatus=1
			AND ISNULL(bitDropShip,0) = 0
			AND ISNULL(bitWorkOrder,0) = 0
			AND ISNULL(I.bitKitParent,0) = 1
			AND ISNULL(WI.numBackorder,0) > 0
			AND ISNULL((SELECT SUM(WIInner.numOnHand) FROM WarehouseItems WIInner WHERE WIInner.numItemID = WI.numItemID AND WIInner.numWareHouseID=WI.numWareHouseID),0) + ISNULL(WI.numOnOrder,0) <= ISNULL(WI.numReorder,0)
		UNION
		SELECT
			IChild.numItemCode
			,CONCAT(IChild.vcItemName, CASE WHEN dbo.fn_GetAttributes(WI.numWareHouseItemID,0) <> '' THEN CONCAT(' (',dbo.fn_GetAttributes(WI.numWareHouseItemID,0),')') ELSE '' END) AS vcItemName
			,IChild.vcModelID
			,(CASE WHEN ISNULL(IChild.numItemGroup,0) > 0 AND IChild.bitMatrix = 0 THEN ISNULL(WI.vcWHSKU,ISNULL(IChild.vcSKU,''))  ELSE ISNULL(IChild.vcSKU,'') END) vcSKU
			,V.vcNotes
			,IChild.charItemType
			,ISNULL((SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numDomainId=@numDomainID AND numItemCode=IChild.numItemCode AND bitDefault =1), '') AS vcPathForTImage
			,ISNULL(WI.numWareHouseItemID,0) AS numWarehouseItemID
			,(CASE 
				WHEN @tintUnitsRecommendationForAutoPOBackOrder=1 --Add �Re-Order Qty� or �Vendor Min Order Qty� (whichever is greater) + �Back Order Qty� - �On Order + Requisitions�
				THEN 
					(CASE WHEN ISNULL(IChild.fltReorderQty,0) > ISNULL(v.intMinQty,0) THEN ISNULL(IChild.fltReorderQty,0) ELSE ISNULL(v.intMinQty,0) END) 
					+ ISNULL(WI.numBackOrder,0) - (ISNULL(numOnOrder,0) + (CASE WHEN @bitIncludeRequisitions = 1 THEN ISNULL((SELECT 
																						SUM(numUnitHour) 
																					FROM 
																						OpportunityItems 
																					INNER JOIN 
																						OpportunityMaster 
																					ON 
																						OpportunityItems.numOppID=OpportunityMaster.numOppId 
																					WHERE 
																						OpportunityMaster.numDomainId=@numDomainID 
																						AND OpportunityMaster.tintOppType=2 
																						AND OpportunityMaster.tintOppStatus=0 
																						AND OpportunityItems.numItemCode=OKCI.numItemID 
																						AND OpportunityItems.numWarehouseItmsID=WI.numWareHouseItemID),0) ELSE 0 END))
				WHEN @tintUnitsRecommendationForAutoPOBackOrder=2 --Add �Re-Order Qty� or �Vendor Min Order Qty� or �Back-Order Qty� (whichever is greater) - �On Order + Requisitions�
				THEN 
					(CASE
						WHEN ISNULL(IChild.fltReorderQty,0) >= ISNULL(v.intMinQty,0) AND ISNULL(IChild.fltReorderQty,0) >= ISNULL(WI.numBackOrder,0) THEN ISNULL(IChild.fltReorderQty,0)
						WHEN ISNULL(v.intMinQty,0) >= ISNULL(IChild.fltReorderQty,0) AND ISNULL(v.intMinQty,0) >= ISNULL(WI.numBackOrder,0) THEN ISNULL(v.intMinQty,0)
						WHEN ISNULL(WI.numBackOrder,0) >= ISNULL(IChild.fltReorderQty,0) AND ISNULL(WI.numBackOrder,0) >= ISNULL(v.intMinQty,0) THEN ISNULL(WI.numBackOrder,0)
						ELSE ISNULL(IChild.fltReorderQty,0)
					END) - (ISNULL(numOnOrder,0) + (CASE WHEN @bitIncludeRequisitions = 1 THEN ISNULL((SELECT 
																SUM(numUnitHour) 
															FROM 
																OpportunityItems 
															INNER JOIN 
																OpportunityMaster 
															ON 
																OpportunityItems.numOppID=OpportunityMaster.numOppId 
															WHERE 
																OpportunityMaster.numDomainId=@numDomainID 
																AND OpportunityMaster.tintOppType=2 
																AND OpportunityMaster.tintOppStatus=0 
																AND OpportunityItems.numItemCode=OKCI.numItemID 
																AND OpportunityItems.numWarehouseItmsID=WI.numWareHouseItemID),0) ELSE 0 END))
				WHEN @tintUnitsRecommendationForAutoPOBackOrder=3 --Add �On-Order Qty� � + Requisitions� - �Back Order� + �Re-Order Qty� or �Vendor Min Order Qty� (whichever is greater)
				THEN 
					(ISNULL(numOnOrder,0) + (CASE WHEN @bitIncludeRequisitions = 1 THEN ISNULL((SELECT 
																SUM(numUnitHour) 
															FROM 
																OpportunityItems 
															INNER JOIN 
																OpportunityMaster 
															ON 
																OpportunityItems.numOppID=OpportunityMaster.numOppId 
															WHERE 
																OpportunityMaster.numDomainId=@numDomainID 
																AND OpportunityMaster.tintOppType=2 
																AND OpportunityMaster.tintOppStatus=0 
																AND OpportunityItems.numItemCode=OKCI.numItemID 
																AND OpportunityItems.numWarehouseItmsID=WI.numWareHouseItemID),0) ELSE 0 END)) - ISNULL(WI.numBackOrder,0) + (CASE
					WHEN ISNULL(IChild.fltReorderQty,0) >= ISNULL(v.intMinQty,0) THEN ISNULL(IChild.fltReorderQty,0) ELSE ISNULL(v.intMinQty,0) END)
				ELSE ISNULL(WI.numBackOrder,0) + ISNULL(V.intMinQty,0) 
			END) AS numUnitHour
			,ISNULL(WI.numBackOrder,0) AS numBackOrder
			,ISNULL(IChild.numBaseUnit,0) AS numUOMID
			,dbo.fn_UOMConversion(OKCI.numUOMId, OKCI.numItemID,OM.numDomainId,IChild.numBaseUnit) AS fltUOMConversionFactor 
			,ISNULL(IChild.numVendorID, 0) AS numVendorID
			,CASE WHEN (@tintDefaultCost = 3 OR @tintDefaultCost=2) THEN (ISNULL(V.monCost, 0) / dbo.fn_UOMConversion(IChild.numPurchaseUnit,IChild.numItemCode,IChild.numDomainID,IChild.numBaseUnit)) ELSE ISNULL(IChild.monAverageCost,0) END AS monCost
			,ISNULL(V.intMinQty,0) AS intMinQty
			,ISNULL(@tintDefaultCost,0) AS tintDefaultCost
			,ISNULL(dbo.fn_UOMConversion(IChild.numPurchaseUnit,IChild.numItemCode,IChild.numDomainID,IChild.numBaseUnit),1) AS fltUOMConversionFactorForPurchase
		FROM
			OpportunityItems OI
		INNER JOIN 
			dbo.OpportunityMaster OM 
		ON 
			OM.numOppId = OI.numOppId
		INNER JOIN
			Item I
		ON
			OI.numItemCode = I.numItemCode
		INNER JOIN
			OpportunityKitItems OKI
		ON
			OI.numoppitemtCode = OKI.numOppItemID
		INNER JOIN
			OpportunityKitChildItems OKCI
		ON
			OKCI.numOppChildItemID = OKI.numOppChildItemID
		INNER JOIN
			Item IChild
		ON
			OKCI.numItemID = IChild.numItemCode
			AND ISNULL(IChild.bitKitParent,0) = 0
		INNER JOIN
			Vendor V
		ON
			IChild.numVendorID = V.numVendorID
			AND IChild.numItemCode = V.numItemCode
		INNER JOIN
			WareHouseItems WI
		ON
			OKCI.numWareHouseItemId = WI.numWareHouseItemID
		WHERE
			OI.numOppId=@numOppID
			AND OM.tintOppType = 1
			AND OM.tintOppStatus=1
			AND ISNULL(bitDropShip,0) = 0
			AND ISNULL(bitWorkOrder,0) = 0
			AND ISNULL(I.bitKitParent,0) = 1
			AND ISNULL(WI.numBackorder,0) > 0
			AND ISNULL((SELECT SUM(WIInner.numOnHand) FROM WarehouseItems WIInner WHERE WIInner.numItemID = WI.numItemID AND WIInner.numWareHouseID=WI.numWareHouseID),0) + ISNULL(WI.numOnOrder,0) <= ISNULL(WI.numReorder,0)
	END
END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                                                         
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityMaster_GetSODropshipItems')
DROP PROCEDURE USP_OpportunityMaster_GetSODropshipItems
GO
CREATE PROCEDURE [dbo].[USP_OpportunityMaster_GetSODropshipItems]
(
	@numDomainID NUMERIC(18,0)
	,@numOppID NUMERIC(18,0)
)
AS
BEGIN
	IF NOT EXISTS (SELECT * FROM OpportunityMaster WHERE numDomainId=@numDomainID AND numOppId=@numOppID)
	BEGIN
		RAISERROR('INVALID_OPPID',16,1)
		RETURN
	END 

	DECLARE @numDivisionID AS NUMERIC(18,0)
	DECLARE @tintDefaultCost AS NUMERIC(18,0)

	SELECT @tintDefaultCost = ISNULL(numCost,0) FROM Domain WHERE numDomainId = @numDomainID
	SELECT @numDivisionID=numDivisionId FROM OpportunityMaster WHERE numDomainId=@numDomainID AND numOppId=@numOppID

	SELECT
		OI.numItemCode
		,CONCAT(I.vcItemName, CASE WHEN dbo.fn_GetAttributes(OI.numWarehouseItmsID,0) <> '' THEN CONCAT(' (',dbo.fn_GetAttributes(OI.numWarehouseItmsID,0),')') ELSE '' END) AS vcItemName
		,I.vcModelID
		,(CASE WHEN ISNULL(I.numItemGroup,0) > 0 AND bitMatrix = 0 THEN ISNULL(WI.vcWHSKU,ISNULL(I.vcSKU,''))  ELSE ISNULL(I.vcSKU,'') END) vcSKU
		,OI.vcNotes
		,I.charItemType
		,ISNULL((SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numDomainId=@numDomainID AND numItemCode=I.numItemCode AND bitDefault =1), '') AS vcPathForTImage
		,ISNULL(OI.numWarehouseItmsID,0) AS numWarehouseItemID
		,OI.numUnitHour
		,ISNULL(OI.numUOMId,0) AS numUOMID
		,ISNULL(U.vcUnitName, '-') vcUOMName
		,dbo.fn_UOMConversion(OI.numUOMId, OI.numItemCode,OM.numDomainId,I.numBaseUnit) AS fltUOMConversionFactor 
		,ISNULL(I.numVendorID, 0) AS numVendorID
		,CASE WHEN @tintDefaultCost = 3 THEN (ISNULL(V.monCost, 0) / dbo.fn_UOMConversion(I.numPurchaseUnit,I.numItemCode,I.numDomainID,I.numBaseUnit)) ELSE ISNULL(I.monAverageCost,0) END AS monCost
		,ISNULL(dbo.fn_UOMConversion(I.numPurchaseUnit,I.numItemCode,I.numDomainID,I.numBaseUnit),1) AS fltUOMConversionFactorForPurchase
		,ISNULL(V.intMinQty, 0) AS intMinQty
		,ISNULL(@tintDefaultCost,0) AS tintDefaultCost
	FROM    
		dbo.OpportunityItems OI
		INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OI.numOppId
		INNER JOIN dbo.Item I ON I.numItemCode = OI.numItemCode
		LEFT JOIN WarehouseItems WI ON OI.numWarehouseItmsID = WI.numWareHouseItemID
		LEFT JOIN UOM U ON U.numUOMId = OI.numUOMId
		LEFT JOIN Vendor V ON V.numVendorID = I.numVendorID	AND I.numItemCode = V.numItemCode
	WHERE   
		OI.numOppId = @numOppID
		AND OM.numDomainId = @numDomainId
		AND OM.tintOppType = 1
		AND OM.tintOppStatus=1
		AND ISNULL(OI.bitDropShip, 0) = 1
		AND 1 = (CASE WHEN (SELECT COUNT(*) FROM OpportunityLinking OMInner INNER JOIN OpportunityItems OIInner ON OMInner.numChildOppID=OIInner.numOppId WHERE OMInner.numParentOppID = @numOppID AND OIInner.numItemCode=OI.numItemCode AND ISNULL(OIInner.numWarehouseItmsID,0)=ISNULL(OI.numWarehouseItmsID,0)) > 0 THEN 0 ELSE 1 END)


	EXEC usp_getoppaddress @numOppID,1,0

	SELECT dbo.fn_getOPPAddress(@numOppID,@numDomainId,2) AS ShippingAdderss
END
/****** Object:  StoredProcedure [dbo].[USP_Projects]    Script Date: 07/26/2008 16:20:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_projects')
DROP PROCEDURE usp_projects
GO
CREATE PROCEDURE [dbo].[USP_Projects]                          
(                          
@numProId numeric(9)=null ,  
@numDomainID as numeric(9),  
@ClientTimeZoneOffset as int         
                         
)                          
as                          
                          
begin                          
                                          
 select  pro.numProId,                          
  pro.vcProjectName,                         
  pro.numintPrjMgr,                          
  pro.numOppId,                    
  pro.intDueDate,                          
  pro.numCustPrjMgr,                          
  pro.numDivisionId,                          
  pro.txtComments,      
  Div.tintCRMType,                          
  div.vcDivisionName,                          
  com.vcCompanyName,
  A.vcFirstname + ' ' + A.vcLastName AS vcContactName,
  isnull(A.vcEmail,'') AS vcEmail,
         isnull(A.numPhone,'') AS Phone,
         isnull(A.numPhoneExtension,'') AS PhoneExtension,
		 		 (SELECT TOP 1 ISNULL(vcData,'') FROM ListDetails WHERE numListID=5 AND numListItemID=com.numCompanyType AND numDomainId=@numDomainID) AS vcCompanyType,
pro.numcontractId,                       
  dbo.fn_GetContactName(pro.numCreatedBy)+' '+convert(varchar(20),dateadd(minute,-@ClientTimeZoneOffset,pro.bintCreatedDate)) as vcCreatedBy ,                          
  dbo.fn_GetContactName(pro.numModifiedBy)+' '+convert(varchar(20),dateadd(minute,-@ClientTimeZoneOffset,pro.bintModifiedDate)) as vcModifiedby,                        
  dbo.fn_GetContactName(pro.numCompletedBy)+' '+convert(varchar(20),dateadd(minute,-@ClientTimeZoneOffset,pro.dtCompletionDate)) as vcFinishedby,                        
  pro.numDomainId,                          
  dbo.fn_GetContactName(pro.numRecOwner) as vcRecOwner,  pro.numRecOwner,   div.numTerID,       
  pro.numAssignedby,pro.numAssignedTo,          
(select  count(*) from dbo.GenericDocuments where numRecID=@numProId and  vcDocumentSection='P') as DocumentCount,
	
 numAccountID,
 '' TimeAndMaterial,
isnull((select sum(isnull(monTimeBudget,0)) from StagePercentageDetails where numProjectID=@numProId),0) TimeBudget,
isnull((select sum(isnull(monExpenseBudget,0)) from StagePercentageDetails where numProjectID=@numProId),0) ExpenseBudget,
isnull((select min(isnull(dtStartDate,getdate()))-1 from StagePercentageDetails where numProjectID=@numProId),getdate()) dtStartDate,
isnull((select max(isnull(dtEndDate,getdate()))+1 from StagePercentageDetails where numProjectID=@numProId),getdate()) dtEndDate,
ISNULL(numProjectType,0) AS numProjectType,
isnull(dbo.fn_GetExpenseDtlsbyProStgID(pro.numProId,0,0),0) as UsedExpense,                        
isnull(dbo.fn_GeTimeDtlsbyProStgID_BT_NBT(pro.numProId,0,1),'Billable Time (0)  Non Billable Time (0)') as UsedTime,
isnull(Pro.numProjectStatus,0) as numProjectStatus,
(SELECT ISNULL(vcStreet,'') + ',<pre>' + ISNULL(vcCity,'') + ',<pre>' +  dbo.[fn_GetState]([numState]) + ',<pre>' + dbo.fn_GetListItemName(numCountry) + ',<pre>' + vcPostalCode 
 FROM dbo.AddressDetails WHERE numDomainID = pro.numDomainID AND numRecordID = pro.numAddressID AND tintAddressOf = 4 AND tintAddressType = 2 AND bitIsPrimary = 1 )as ShippingAddress,
Pro.numBusinessProcessID,
SLP.Slp_Name AS vcProcessName,
pro.dtCompletionDate AS vcProjectedFinish,
NULL AS dtActualStartDate,
pro.dtmEndDate,
pro.dtmStartDate,
ISNULL(pro.numClientBudget,0) AS numClientBudget,
0 AS numClientBudgetBalance,
(CASE 
	WHEN EXISTS (SELECT CInner.numContractId FROM Contracts CInner WHERE CInner.numDomainId=@numDomainID AND CInner.numDivisonId=pro.numDivisionID AND intType=1) 
	THEN 1
	ELSE 0
END) AS bitContractExists,
0 AS numClientBudgetBalance,
(CASE 
	WHEN EXISTS (SELECT CInner.numContractId FROM Contracts CInner WHERE CInner.numDomainId=@numDomainID AND CInner.numDivisonId=pro.numDivisionID AND intType=1) 
	THEN dbo.fn_SecondsConversion(ISNULL((SELECT TOP 1 ISNULL(CAST(timeLeft AS INT),0)-ISNULL(CAST(timeUsed AS INT),0) FROM Contracts CInner WHERE CInner.numDomainId=@numDomainID AND CInner.numDivisonId=pro.numDivisionID AND intType=1 ORDER BY CInner.numContractID DESC),0))
	ELSE '' 
END) AS timeLeft,
(CASE 
	WHEN EXISTS (SELECT CInner.numContractId FROM Contracts CInner WHERE CInner.numDomainId=@numDomainID AND CInner.numDivisonId=pro.numDivisionID AND intType=1) 
	THEN ISNULL((SELECT TOP 1 ISNULL(CAST(timeLeft AS INT),0)-ISNULL(CAST(timeUsed AS INT),0) FROM Contracts CInner WHERE CInner.numDomainId=@numDomainID AND CInner.numDivisonId=pro.numDivisionID AND intType=1 ORDER BY CInner.numContractID DESC),0)
	ELSE 0
END) AS timeLeftInMinutes
from ProjectsMaster pro                          
  left join DivisionMaster div                          
  on pro.numDivisionID=div.numDivisionID    
  LEFT JOIN AdditionalContactsInformation A ON pro.numCustPrjMgr = A.numContactId
  left join CompanyInfo com                          
  on div.numCompanyID=com.numCompanyId         
  LEFT JOIN dbo.AddressDetails AD ON AD.numAddressID = pro.numAddressID       
		LEFT JOIN Sales_process_List_Master AS SLP ON pro.numBusinessProcessID=SLP.Slp_Id          
  where numProId=@numProId    and pro.numDomainID=@numDomainID                      
                          
end
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ProjectsMaster_GetAssociatedContacts')
DROP PROCEDURE USP_ProjectsMaster_GetAssociatedContacts
GO
CREATE PROCEDURE [dbo].[USP_ProjectsMaster_GetAssociatedContacts]
(
	@numDomainID NUMERIC(18,0)
	,@numProId NUMERIC(18,0)
)
AS
BEGIN
	DECLARE @TEMPContacts TABLE
	(
		numContactID NUMERIC(18,0)
		,vcContact VARCHAR(200)
		,vcCompanyname VARCHAR(300)
		,tinUserType TINYINT
		,ActivityID NUMERIC(18,0)
		,Email VARCHAR(150)
		,vcStatus VARCHAR(200)
		,vcPosition VARCHAR(200)
		,vcEmail VARCHAR(200)
		,numPhone VARCHAR(200)
		,numPhoneExtension VARCHAR(200)
		,bitDefault BIT
		,vcUserName VARCHAR(200)
		,numRights TINYINT
		,numProjectRole NUMERIC(18,0)
	)

	INSERT INTO 
		@TEMPContacts
	SELECT 
		AC.numContactId
		,CONCAT(ISNULL(vcFirstName,''),' ',ISNULL(vcLastName,'')) AS vcContact
		,CI.vcCompanyName AS vcCompanyname
		,CASE WHEN UM.numUserDetailId IS NULL THEN 2 ELSE 1 END AS tinUserType
		,0 AS ActivityID
		,AC.vcEmail as Email
		,'' vcStatus
		,dbo.GetListIemName(AC.vcPosition) vcPosition
		,AC.vcEmail
		,AC.numPhone
		,AC.numPhoneExtension
		,1
		,CONCAT(ISNULL(vcFirstName,''),' ',ISNULL(vcLastName,''))
		,1
		,0
	FROM
		ProjectsMaster PM
	INNER JOIN 
		AdditionalContactsInformation AC 
	ON 
		PM.numIntPrjMgr=AC.numContactID  
	LEFT JOIN
		DivisionMaster DM 
	ON 
		DM.numDivisionID=AC.numDivisionID 
	LEFT JOIN 
		CompanyInfo CI 
	ON 
		DM.numCompanyId=CI.numCompanyId 
	LEFT JOIN 
		UserMaster UM  
	ON 
		UM.numUserDetailID=AC.numContactID
	WHERE 
		PM.numDomainID=@numDomainId
		AND PM.numProId=@numProId

	INSERT INTO 
		@TEMPContacts
	SELECT 
		AC.numContactId
		,CONCAT(ISNULL(vcFirstName,''),' ',ISNULL(vcLastName,'')) AS vcContact
		,CI.vcCompanyName AS vcCompanyname
		,CASE WHEN UM.numUserDetailId IS NULL THEN 2 ELSE 1 END AS tinUserType
		,0 AS ActivityID
		,AC.vcEmail as Email
		,'' vcStatus
		,dbo.GetListIemName(AC.vcPosition) vcPosition
		,AC.vcEmail
		,AC.numPhone
		,AC.numPhoneExtension
		,1
		,CONCAT(ISNULL(vcFirstName,''),' ',ISNULL(vcLastName,''))
		,1
		,0
	FROM
		ProjectsMaster PM
	INNER JOIN 
		AdditionalContactsInformation AC 
	ON 
		PM.numCustPrjMgr=AC.numContactID  
	LEFT JOIN
		DivisionMaster DM 
	ON 
		DM.numDivisionID=AC.numDivisionID 
	LEFT JOIN 
		CompanyInfo CI 
	ON 
		DM.numCompanyId=CI.numCompanyId 
	LEFT JOIN 
		UserMaster UM  
	ON 
		UM.numUserDetailID=AC.numContactID
	WHERE 
		PM.numDomainID=@numDomainId
		AND PM.numProId=@numProId
		AND AC.numContactId NOT IN (SELECT TC.numContactID FROM @TEMPContacts TC)

	INSERT INTO 
		@TEMPContacts
	SELECT DISTINCT
		AC.numContactId
		,CONCAT(ISNULL(vcFirstName,''),' ',ISNULL(vcLastName,'')) AS vcContact
		,CI.vcCompanyName AS vcCompanyname
		,CASE WHEN UM.numUserDetailId IS NULL THEN 2 ELSE 1 END AS tinUserType
		,0 AS ActivityID
		,AC.vcEmail as Email
		,'' vcStatus
		,dbo.GetListIemName(AC.vcPosition) vcPosition
		,AC.vcEmail
		,AC.numPhone
		,AC.numPhoneExtension
		,1
		,CONCAT(ISNULL(vcFirstName,''),' ',ISNULL(vcLastName,''))
		,1
		,0
	FROM
		ProjectsMaster PM
	INNER JOIN
		StagePercentageDetailsTask SPDT
	ON
		PM.numProId = SPDT.numProjectId
	INNER JOIN 
		AdditionalContactsInformation AC 
	ON 
		SPDT.numAssignTo=AC.numContactID  
	LEFT JOIN
		DivisionMaster DM 
	ON 
		DM.numDivisionID=AC.numDivisionID 
	LEFT JOIN 
		CompanyInfo CI 
	ON 
		DM.numCompanyId=CI.numCompanyId 
	LEFT JOIN 
		UserMaster UM  
	ON 
		UM.numUserDetailID=AC.numContactID
	WHERE 
		PM.numDomainID=@numDomainId
		AND PM.numProId=@numProId
		AND AC.numContactId NOT IN (SELECT TC.numContactID FROM @TEMPContacts TC)

	INSERT INTO 
		@TEMPContacts
	SELECT DISTINCT
		PC.numContactID
		,CONCAT(ISNULL(vcFirstName,''),' ',ISNULL(vcLastName,'')) AS vcContact
		,CI.vcCompanyName AS vcCompanyname
		,CASE WHEN UM.numUserDetailId IS NULL THEN 2 ELSE 1 END AS tinUserType
		,0 AS ActivityID
		,AC.vcEmail as Email
		,'' vcStatus
		,dbo.GetListIemName(AC.vcPosition) vcPosition
		,AC.vcEmail
		,AC.numPhone
		,AC.numPhoneExtension
		,0
		,CONCAT(ISNULL(vcFirstName,''),' ',ISNULL(vcLastName,''))
		,1
		,0
	FROM 
		ProjectsMaster PM
	INNER JOIN
		ProjectsContacts PC 
	ON
		PM.numProId = PC.numProId
	INNER JOIN 
		AdditionalContactsInformation AC 
	ON 
		PC.numContactID=AC.numContactID  
	LEFT JOIN
		DivisionMaster DM 
	ON 
		DM.numDivisionID=AC.numDivisionID 
	LEFT JOIN 
		CompanyInfo CI 
	ON 
		DM.numCompanyId=CI.numCompanyId 
	LEFT JOIN 
		UserMaster UM  
	ON 
		UM.numUserDetailID=AC.numContactID
	WHERE 
		PM.numDomainID=@numDomainId
		AND PM.numProId=@numProId
		AND AC.numContactId NOT IN (SELECT TC.numContactID FROM @TEMPContacts TC)

	SELECT * FROM @TEMPContacts ORDER BY vcContact
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportListMaster_EmployeeBenefitToCompany')
DROP PROCEDURE USP_ReportListMaster_EmployeeBenefitToCompany
GO
CREATE PROCEDURE [dbo].[USP_ReportListMaster_EmployeeBenefitToCompany]
	@numDomainID NUMERIC(18,0)
	,@ClientTimeZoneOffset INT
	,@vcTimeLine VARCHAR(50)
	,@vcFilterBy TINYINT  -- 1:Assign TO, 2: Record Owner, 3: Teams (Based on Assigned To), 4: Employees
	,@vcFilterValue VARCHAR(MAX)
AS
BEGIN 
	DECLARE @numShippingItemID NUMERIC(18,0)
	DECLARE @numDiscountItemID NUMERIC(18,0)
	SELECT @numShippingItemID = numShippingServiceItemID,@numDiscountItemID=numDiscountServiceItemID FROM Domain WHERE numDomainId=@numDomainID

	DECLARE @dtStartDate DATETIME --= DATEADD(MINUTE,-@ClientTimeZoneOffset,DATEADD(MONTH,-12,GETUTCDATE()))
	DECLARE @dtEndDate AS DATETIME = DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())

	IF @vcTimeLine = 'Last12Months'
	BEGIN
		SET @dtStartDate = DATEADD(MONTH, DATEDIFF(MONTH, 0, @dtEndDate), 0)
	END
	IF @vcTimeLine = 'Last6Months'
	BEGIN
		SET @dtStartDate = DATEADD(MONTH, DATEDIFF(MONTH, 0, @dtEndDate), 0)
	END
	IF @vcTimeLine = 'Last3Months'
	BEGIN
		SET @dtStartDate = DATEADD(MONTH, DATEDIFF(MONTH, 0, @dtEndDate), 0)
	END
	IF @vcTimeLine = 'Last30Days'
	BEGIN
		SET @dtStartDate = DATEADD(DAY, DATEDIFF(DAY, -30, @dtEndDate), 0)
	END
	IF @vcTimeLine = 'Last7Days'
	BEGIN
		SET @dtStartDate = DATEADD(DAY, DATEDIFF(DAY, -7, @dtEndDate), 0)
	END
	
	-- FETCH ALLREADY PAID COMMISSION DATA AND STORE IN TEMPORARY TABLE
	SELECT * INTO #tempPayrollTracking FROM PayrollTracking WHERE numDomainId=@numDomainId

	DECLARE @TempCommissionPaidCreditMemoOrRefund TABLE
	(
		numUserCntID NUMERIC(18,0),
		numReturnHeaderID NUMERIC(18,0),
		tintReturnType TINYINT,
		numReturnItemID NUMERIC(18,0),
		monCommission DECIMAL(20,5),
		numComRuleID NUMERIC(18,0),
		tintComType TINYINT,
		tintComBasedOn TINYINT,
		decCommission FLOAT,
		tintAssignTo TINYINT
	)    

	INSERT INTO 
		@TempCommissionPaidCreditMemoOrRefund 
	SELECT 
		numUserCntID,
		numReturnHeaderID,
		tintReturnType,
		numReturnItemID,
		monCommission,
		numComRuleID,
		tintComType,
		tintComBasedOn,
		decCommission,
		tintAssignTo
	FROM 
		dbo.GetCommissionPaidCreditMemoOrRefund(@numDomainId,@ClientTimeZoneOffset,@dtStartDate,@dtEndDate)

	DECLARE @TEMP TABLE
	(
		ID INT IDENTITY(1,1)
		,numUserCntID NUMERIC(18,0)
		,vcUserName VARCHAR(300)
		,monHourlyRate DECIMAL(20,5)
		,TotalHrsWorked FLOAT
		,TotalPayroll DECIMAL(20,5)
		,ProfitMinusPayroll DECIMAL(20,5)
	)

	INSERT INTO @TEMP
	(
		numUserCntID
		,vcUserName
		,monHourlyRate
	)
	SELECT 
		UM.numUserDetailId
		,CONCAT(ADC.vcfirstname,' ',adc.vclastname) vcUserName
		,ISNULL(UM.monHourlyRate,0)
	FROM  
		dbo.UserMaster UM 
	JOIN 
		dbo.AdditionalContactsInformation adc 
	ON 
		adc.numDomainID=@numDomainId 
		AND adc.numcontactid = um.numuserdetailid
	JOIN
		userteams UT
	ON 
		UT.numDomainID=@numDomainId 
		AND UM.numUserDetailId=UT.numUserCntID
	WHERE 
		UM.numDomainId=@numDomainId
		AND UM.bitActivateFlag = 1
		AND 1 = (CASE WHEN @vcFilterBy = 4  THEN (CASE WHEN UT.numUserCntID IN (SELECT Id FROM dbo.SplitIDs((@vcFilterValue),',')) THEN 1 ELSE 0 END) 
					ELSE 
				(CASE WHEN LEN(ISNULL(@vcFilterValue,'')) > 0 THEN (CASE WHEN UT.numTeam IN (SELECT Id FROM dbo.SplitIDs((@vcFilterValue),',')) THEN 1 ELSE 0 END) ELSE 1 END)
					END)
		


	DECLARE @i INT = 1
	DECLARE @COUNT INT = 0
	DECLARE @numUserCntID NUMERIC(18,0)
	DECLARE @decTotalHrsWorked as decimal(10,2)
	DECLARE @decTotalPaidLeaveHrs AS DECIMAL(10,2)
	DECLARE @monExpense DECIMAL(20,5)
	DECLARE @monReimburse DECIMAL(20,5)
	DECLARE @monCommPaidInvoice DECIMAL(20,5)
	DECLARE @monCommPaidInvoiceDepositedToBank DECIMAL(20,5)
	DECLARE @monCommUNPaidInvoice DECIMAL(20,5)
	DECLARE @monCommPaidCreditMemoOrRefund DECIMAL(20,5)
	DECLARE @monCommUnPaidCreditMemoOrRefund DECIMAL(20,5)
	DECLARE @GrossProfit DECIMAL(20,5)
    
	SELECT @COUNT=COUNT(*) FROM @TEMP

	-- LOOP OVER EACH EMPLOYEE
	WHILE @i <= @COUNT
	BEGIN
		SELECT @decTotalHrsWorked=0,@decTotalPaidLeaveHrs=0,@monExpense=0,@monReimburse=0,@monCommPaidInvoice=0,@monCommPaidInvoiceDepositedToBank=0,@monCommUNPaidInvoice=0, @numUserCntID=0, @GrossProfit=0

		SELECT @numUserCntID = numUserCntID FROM @TEMP WHERE ID=@i

		SELECT 
			@decTotalHrsWorked=sum(x.Hrs) 
		FROM 
		(
			SELECT  
				ISNULL(SUM(CAST(DATEDIFF(MINUTE,dtfromdate,dttodate) AS FLOAT)/CAST(60 AS FLOAT)),0) AS Hrs            
			FROM 
				TimeAndExpense 
			WHERE 
				(numType=1 OR numType=2) 
				AND numCategory=1                 
				AND (
						(DATEADD(MINUTE,-@ClientTimeZoneOffset,dtFromDate) BETWEEN @dtStartDate AND @dtEndDate) 
						OR 
						(DATEADD(MINUTE,-@ClientTimeZoneOffset,dtToDate) BETWEEN @dtStartDate AND @dtEndDate)
					) 
				AND numUserCntID=@numUserCntID  
				AND numDomainID=@numDomainId
				AND numCategoryHDRID NOT IN (SELECT numCategoryHDRID FROM #tempPayrollTracking WHERE ISNULL(numCategoryHDRID,0)>0)
		) x

		-- CALCULATE PAID LEAVE HRS
		SELECT 
			@decTotalPaidLeaveHrs=ISNULL(
											SUM( 
													CASE 
													WHEN dtfromdate = dttodate 
													THEN 
														24 - (CASE WHEN bitfromfullday = 0 THEN 12 ELSE 0 END)
													ELSE 
														(Datediff(DAY,dtfromdate,dttodate) + 1) * 24 - (CASE WHEN bitfromfullday = 0 THEN 12 ELSE 0 END) - (CASE WHEN bittofullday = 0 THEN 12 ELSE 0 END)
														END
												)
											,0)
		FROM   
			TimeAndExpense
		WHERE  
			numtype = 3 
			AND numcategory = 3 
			AND numusercntid = @numUserCntID 
			AND numdomainid = @numDomainId 
			AND (
					(DATEADD(MINUTE,-@ClientTimeZoneOffset,dtFromDate) BETWEEN @dtStartDate AND @dtEndDate)
					Or 
					(DATEADD(MINUTE,-@ClientTimeZoneOffset,dtToDate) BETWEEN @dtStartDate AND @dtEndDate)
				)
			AND numCategoryHDRID NOT IN (SELECT numCategoryHDRID FROM #tempPayrollTracking WHERE ISNULL(numCategoryHDRID,0)>0)		

		-- CALCULATE EXPENSES
		SELECT 
			@monExpense=ISNULL((SUM(CAST(monAmount AS FLOAT))),0)    
		FROM 
			TimeAndExpense 
		WHERE 
			numCategory=2 
			AND numType in (1,2) 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainId 
			AND ((dtFromDate BETWEEN @dtStartDate And @dtEndDate) Or (dtToDate BETWEEN @dtStartDate And @dtEndDate)) 
			AND numCategoryHDRID NOT IN (SELECT numCategoryHDRID FROM #tempPayrollTracking WHERE ISNULL(numCategoryHDRID,0)>0)		

		-- CALCULATE REIMBURSABLE EXPENSES
		SELECT 
			@monReimburse=Isnull((SUM(CAST(monamount AS FLOAT))),0)
		FROM   
			TimeAndExpense 
		WHERE  
			bitreimburse = 1 
			AND numcategory = 2 
			AND numusercntid = @numUserCntID 
			AND numdomainid = @numDomainId
			AND dtfromdate BETWEEN @dtStartDate AND @dtEndDate
			AND numCategoryHDRID NOT IN (SELECT numCategoryHDRID FROM #tempPayrollTracking WHERE ISNULL(numCategoryHDRID,0)>0)        

		-- CALCULATE COMMISSION PAID INVOICE
		SELECT 
			@monCommPaidInvoice=ISNULL(SUM(Amount),0) 
		FROM 
			(
				SELECT 
					oppBiz.numOppID,
					oppBiz.numOppBizDocsID,
					ISNULL(SUM(BC.numComissionAmount - ISNULL(PT.monCommissionAmt,0)),0) AS Amount
				FROM 
					OpportunityMaster Opp 
				INNER JOIN OpportunityBizDocs oppBiz on oppBiz.numOppId=opp.numOppId 
				INNER JOIN BizDocComission BC on BC.numOppBizDocId=oppBiz.numOppBizDocsId 
				LEFT JOIN #tempPayrollTracking PT ON PT.numComissionID=BC.numComissionID
				CROSS APPLY
				(
					SELECT 
						MAX(DM.dtDepositDate) dtDepositDate
					FROM 
						DepositMaster DM 
					JOIN dbo.DepositeDetails DD	ON DM.numDepositId=DD.numDepositID 
					WHERE 
						DM.tintDepositePage = 2
						AND DM.numDomainId=@numDomainId
						AND DD.numOppID=oppBiz.numOppID 
						AND DD.numOppBizDocsID =oppBiz.numOppBizDocsID
				) TEMPDeposit
				WHERE 
					Opp.numDomainId=@numDomainId 
					AND (BC.numUserCntId=@numUserCntID AND ISNULL(BC.tintAssignTo,0) <> 3)
					AND BC.bitCommisionPaid=0 
					AND oppBiz.bitAuthoritativeBizDocs = 1 
					AND isnull(oppBiz.monAmountPaid,0) >=oppBiz.monDealAmount 
					AND CAST(DATEADD(MINUTE,-@ClientTimeZoneOffset,TEMPDeposit.dtDepositDate) AS DATE) BETWEEN CAST(@dtStartDate AS DATE) AND CAST(@dtEndDate AS DATE)
				GROUP BY 
					oppBiz.numOppID,
					oppBiz.numOppBizDocsID
			) A

		-- CALCULATE COMMISSION UNPAID INVOICE 
		SELECT 
			@monCommUNPaidInvoice=isnull(sum(BC.numComissionAmount - ISNULL(PT.monCommissionAmt,0)),0) 
		FROM 
			OpportunityMaster Opp 
		INNER JOIN OpportunityBizDocs oppBiz on oppBiz.numOppId=opp.numOppId 
		INNER JOIN BizDocComission BC on BC.numOppBizDocId=oppBiz.numOppBizDocsId
		LEFT JOIN #tempPayrollTracking PT ON PT.numComissionID=BC.numComissionID 
		WHERE 
			Opp.numDomainId=@numDomainId 
			AND (BC.numUserCntId=@numUserCntID AND ISNULL(BC.tintAssignTo,0) <> 3)
			AND BC.bitCommisionPaid=0 
			AND oppBiz.bitAuthoritativeBizDocs = 1 
			AND ISNULL(oppBiz.monAmountPaid,0) <oppBiz.monDealAmount
			AND ((DATEADD(MINUTE,-@ClientTimeZoneOffset,oppBiz.dtFromDate) BETWEEN @dtStartDate AND @dtEndDate))	

		SET @monCommPaidCreditMemoOrRefund = ISNULL((SELECT SUM(monCommission) FROM @TempCommissionPaidCreditMemoOrRefund WHERE (numUserCntId=@numUserCntID AND ISNULL(tintAssignTo,0) <> 3)),0)


		SET @GrossProfit = ISNULL((SELECT
										SUM(dbo.GetOrderItemProfitAmountOrMargin(@numDomainID,OM.numOppId,OI.numoppitemtCode,1)) Profit
									FROM
										OpportunityMaster OM
									INNER JOIN
										OpportunityItems OI
									ON
										OI.numOppId = OM.numOppID
									INNER JOIN
										Item I
									ON
										OI.numItemCode = I.numItemCode
									WHERE
										OM.numDomainId=@numDomainID
										AND OM.numAssignedTo = @numUserCntID
										AND ISNULL(OI.monTotAmount,0) <> 0
										AND ISNULL(OI.numUnitHour,0) <> 0
										AND ISNULL(OM.tintOppType,0)=1
										AND ISNULL(OM.tintOppStatus,0)=1
										AND DATEADD(MINUTE,-@ClientTimeZoneOffset,OM.bintCreatedDate) BETWEEN @dtStartDate AND @dtEndDate
										AND I.numItemCode NOT IN (@numShippingItemID,@numDiscountItemID)),0)

		UPDATE 
			@TEMP 
		SET    
			TotalHrsWorked=@decTotalHrsWorked,
			TotalPayroll= @decTotalHrsWorked * monHourlyRate + @monReimburse + @monCommPaidInvoice + @monCommUNPaidInvoice - ISNULL(@monCommPaidCreditMemoOrRefund,0),
			ProfitMinusPayroll = @GrossProfit - (@decTotalHrsWorked * monHourlyRate + @monReimburse + @monCommPaidInvoice + @monCommUNPaidInvoice - ISNULL(@monCommPaidCreditMemoOrRefund,0))
		WHERE 
			numUserCntID=@numUserCntID 

		SET @i = @i + 1
	END

	SELECT vcUserName,TotalHrsWorked FROM @TEMP ORDER BY TotalHrsWorked DESC
	SELECT vcUserName,TotalPayroll FROM @TEMP ORDER BY TotalPayroll DESC
	SELECT vcUserName,ProfitMinusPayroll FROM @TEMP ORDER BY ProfitMinusPayroll DESC
	DROP TABLE #tempPayrollTracking
END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportListMaster_EmployeeSalesPerformancePanel1')
DROP PROCEDURE USP_ReportListMaster_EmployeeSalesPerformancePanel1
GO

CREATE PROCEDURE [dbo].[USP_ReportListMaster_EmployeeSalesPerformancePanel1]
	@numDomainID NUMERIC(18,0)
	,@ClientTimeZoneOffset INT
	,@vcTimeLine VARCHAR(50)
	,@vcFilterBy TINYINT  -- 1:Assign TO, 2: Record Owner, 3: Teams (Based on Assigned To), 4: Employees
	,@vcFilterValue VARCHAR(MAX)
	--,@vcRunDisplay VARCHAR(MAX) -- added by hitesh 
AS
BEGIN 
	DECLARE @numShippingItemID NUMERIC(18,0)
	DECLARE @numDiscountItemID NUMERIC(18,0)
	SELECT @numShippingItemID=numShippingServiceItemID,@numDiscountItemID=numDiscountServiceItemID FROM Domain WHERE numDomainId=@numDomainID
	---alter sp
 DECLARE @dtStartDate DATETIME --= DATEADD(MINUTE,-@ClientTimeZoneOffset,DATEADD(MONTH,-12,GETUTCDATE()))  
 DECLARE @dtEndDate AS DATETIME = DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()) 
 -------
	DECLARE @TotalSalesReturn FLOAT = 0.0
	----alterd sp-----------

IF @vcTimeLine = 'Last12Months'  
 BEGIN
 set  @dtStartDate =   dateadd(month,datediff(month,0,@dtEndDate)-12,0)  
  --SET @dtStartDate = DATEADD(MONTH, DATEDIFF(MONTH, 0, @dtEndDate), 0)  
 END  
ELSE IF @vcTimeLine = 'Last6Months'  
 BEGIN
 set  @dtStartDate =   dateadd(month,datediff(month,0,@dtEndDate)-6,0)  
  --SET @dtStartDate = DATEADD(MONTH, DATEDIFF(MONTH, 0, @dtEndDate), 0)  
 END  
ELSE IF @vcTimeLine = 'Last3Months'  
BEGIN  
set  @dtStartDate =   dateadd(month,datediff(month,0,@dtEndDate)-3,0)
--SET @dtStartDate = DATEADD(MONTH, DATEDIFF(MONTH, 0, @dtEndDate), 0)  
END  
ELSE IF @vcTimeLine = 'Last30Days'  
BEGIN  
SET @dtStartDate = DATEADD(DAY, DATEDIFF(DAY, -30, @dtEndDate), 0)  
END  
ELSE IF @vcTimeLine = 'Last7Days'  
	BEGIN  
	SET @dtStartDate = DATEADD(DAY, DATEDIFF(DAY, -7, @dtEndDate), 0) 
	END
ELSE IF @vcTimeLine = 'Today' 
BEGIN 
SET @dtStartDate = DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())
END
ELSE IF @vcTimeLine = 'Yesterday'
BEGIN 
SET @dtStartDate = DATEADD(DAY,-1,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))
 END 
ELSE  
BEGIN
 Set @dtStartDate = LEFT(@vcTimeLine,CHARINDEX('-',@vcTimeLine)-1)
 set @dtEndDate = RIGHT(@vcTimeLine,CHARINDEX('-',@vcTimeLine)-1)
 END
  
	-----------------------------------
	SELECT 
		UserMaster.numUserDetailId,CAst(@dtStartDate as date) as StartDate,CAST(@dtEndDate  as date) as EndDate,
		UserMaster.vcUserName,@vcFilterBy as vcFilterBy
		,(SELECT COUNT(*) FROM OpportunityMaster WHERE numDomainId=@numDomainID AND numAssignedTo=TEMP1.numAssignedTo AND tintOppType=1 AND tintOppStatus=1 AND bintOppToOrder IS NOT NULL) * 100.0 / (SELECT COUNT(*) FROM OpportunityMaster WHERE numDomainId=@numDomainID AND numAssignedTo=TEMP1.numAssignedTo AND tintOppType=1 AND ((tintOppStatus=1 AND bintOppToOrder IS NOT NULL) OR tintOppStatus=2)) * 1.0 WonPercent
	FROM
	(
		SELECT DISTINCT
			--numAssignedTo
			CASE @vcFilterBy  
		   WHEN 1  -- Assign To  
		   THEN numAssignedTo
		   --else numRecOwner
		   WHEN 2 
		   THEN numRecOwner
		   else  numAssignedTo
		   End as numAssignedTo
		FROM
			OpportunityMaster
		WHERE
			numDomainId=@numDomainID
			AND tintOppType=1
			AND ((tintOppStatus=1 AND bintOppToOrder IS NOT NULL) OR tintOppStatus=2)
		--	--alter sp
			AND (  
      (DATEADD(MINUTE,-@ClientTimeZoneOffset,bintCreatedDate) BETWEEN @dtStartDate AND @dtEndDate)   
      OR   
      (DATEADD(MINUTE,-@ClientTimeZoneOffset,bintCreatedDate) BETWEEN @dtStartDate AND @dtEndDate)  
     ) 
	 AND
	 1=(CASE   
     WHEN LEN(ISNULL(@vcFilterValue,0)) > 0 AND @vcFilterBy IN (1,2,3)  
     THEN   
      (CASE @vcFilterBy  
       WHEN 1  -- Assign To  
       THEN (CASE WHEN numAssignedTo IN (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,',')) THEN 1 ELSE 0 END)  
       WHEN 2  -- Record Owner  
       THEN (CASE WHEN numRecOwner IN (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,',')) THEN 1 ELSE 0 END)  
       WHEN 3  -- Teams (Based on Assigned To)  
       --THEN (CASE WHEN numAssignedTo IN (SELECT numUserCntID FROM UserTeams WHERE numDomainID=@numDomainID AND numTeam IN (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,','))) THEN 1 ELSE 0 END)
	   THEN (CASE WHEN numAssignedTo IN (SELECT numContactId FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numTeam IN (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,','))) THEN 1 ELSE 0 END)  
      END)  
     ELSE 1   
    END)  
	 ------
	) TEMP1
	INNER JOIN
		UserMaster
	ON
		TEMP1.numAssignedTo = UserMaster.numUserDetailId
	WHERE
		UserMaster.numDomainID = @numDomainID
		

--#2
	SELECT
		UserMaster.numUserDetailId,
		@vcFilterBy as vcFilterBy,CAst(@dtStartDate as date) as StartDate,CAST(@dtEndDate  as date) as EndDate
		,UserMaster.vcUserName
		,Temp2.Profit
	FROM
	(
		SELECT DISTINCT
			CASE @vcFilterBy  
		   WHEN 1  -- Assign To  
		   THEN numAssignedTo
		   --else numRecOwner
		   WHEN 2 
		   THEN numRecOwner
		   else  numAssignedTo
		   End as numAssignedTo
		FROM
			OpportunityMaster
		WHERE
			numDomainId=@numDomainID
			AND tintOppType=1
			AND tintOppStatus = 1
				AND (  
      (DATEADD(MINUTE,-@ClientTimeZoneOffset,bintCreatedDate) BETWEEN @dtStartDate AND @dtEndDate)   
      OR   
      (DATEADD(MINUTE,-@ClientTimeZoneOffset,bintCreatedDate) BETWEEN @dtStartDate AND @dtEndDate)  
     ) 
			 AND
	 1=(CASE   
     WHEN LEN(ISNULL(@vcFilterValue,0)) > 0 AND @vcFilterBy IN (1,2,3)  
     THEN   
      (CASE @vcFilterBy  
       WHEN 1  -- Assign To  
       THEN (CASE WHEN numAssignedTo IN (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,',')) THEN 1 ELSE 0 END)  
       WHEN 2  -- Record Owner  
       THEN (CASE WHEN numRecOwner IN (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,',')) THEN 1 ELSE 0 END)  
       WHEN 3  -- Teams (Based on Assigned To)  
       --THEN (CASE WHEN numAssignedTo IN (SELECT numUserCntID FROM UserTeams WHERE numDomainID=@numDomainID AND numTeam IN (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,','))) THEN 1 ELSE 0 END)  
	   THEN (CASE WHEN numAssignedTo IN (SELECT numContactId FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numTeam IN (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,','))) THEN 1 ELSE 0 END)  
      END)  
     ELSE 1   
    END)  
	) TEMP1
	INNER JOIN
		UserMaster
	ON
		TEMP1.numAssignedTo = UserMaster.numUserDetailId
		AND UserMaster.numDomainID = @numDomainID
	CROSS APPLY
	(
		SELECT
			SUM(dbo.GetOrderItemProfitAmountOrMargin(@numDomainID,OM.numOppId,OI.numoppitemtCode,1)) Profit
		FROM
			OpportunityMaster OM
		INNER JOIN
			OpportunityItems OI
		ON
			OM.numOppID = OI.numOppId
		INNER JOIN
			Item I
		ON
			OI.numItemCode = I.numItemCode		
		WHERE
			OM.numDomainId=@numDomainID
			AND OM.numAssignedTo = TEMP1.numAssignedTo
			AND ISNULL(OI.monTotAmount,0) <> 0
			AND ISNULL(OI.numUnitHour,0) <> 0
			AND ISNULL(OM.tintOppType,0)=1
			AND ISNULL(OM.tintOppStatus,0)=1
			AND I.numItemCode NOT IN (@numShippingItemID,@numDiscountItemID)
			AND ((DATEADD(MINUTE,-@ClientTimeZoneOffset,OM.bintCreatedDate) BETWEEN @dtStartDate AND @dtEndDate) OR (DATEADD(MINUTE,-@ClientTimeZoneOffset,Om.bintCreatedDate) BETWEEN @dtStartDate AND @dtEndDate))
	) TEMP2
	ORDER BY
		 TEMP2.Profit DESC
--#3
	SELECT
		UserMaster.numUserDetailId
		,UserMaster.vcUserName,@vcFilterBy as vcFilterBy,CAst(@dtStartDate as date) as StartDate,CAST(@dtEndDate  as date) as EndDate
		,AVG(ProfitPercentByOrder) AS AvgGrossProfitMargin
	FROM
	(
		SELECT DISTINCT
			--numAssignedTo
			CASE @vcFilterBy  
		   WHEN 1  -- Assign To  
		   THEN numAssignedTo
		   --else numRecOwner
		   WHEN 2 
		   THEN numRecOwner
		   else  numAssignedTo
		   End as numAssignedTo
		FROM
			OpportunityMaster
		WHERE
			numDomainId=@numDomainID
			AND tintOppType=1
			AND tintOppStatus = 1
				AND (  
      (DATEADD(MINUTE,-@ClientTimeZoneOffset,bintCreatedDate) BETWEEN @dtStartDate AND @dtEndDate)   
      OR   
      (DATEADD(MINUTE,-@ClientTimeZoneOffset,bintCreatedDate) BETWEEN @dtStartDate AND @dtEndDate)  
     ) 
			 AND
	 1=(CASE   
     WHEN LEN(ISNULL(@vcFilterValue,0)) > 0 AND @vcFilterBy IN (1,2,3)  
     THEN   
      (CASE @vcFilterBy  
       WHEN 1  -- Assign To  
       THEN (CASE WHEN numAssignedTo IN (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,',')) THEN 1 ELSE 0 END)  
       WHEN 2  -- Record Owner  
       THEN (CASE WHEN numRecOwner IN (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,',')) THEN 1 ELSE 0 END)  
       WHEN 3  -- Teams (Based on Assigned To)  
       --THEN (CASE WHEN numAssignedTo IN (SELECT numUserCntID FROM UserTeams WHERE numDomainID=@numDomainID AND numTeam IN (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,','))) THEN 1 ELSE 0 END)  
	   THEN (CASE WHEN numAssignedTo IN (SELECT numContactId FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numTeam IN (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,','))) THEN 1 ELSE 0 END)  
      END)  
     ELSE 1   
    END)  
	) TEMP1
	INNER JOIN
		UserMaster
	ON
		TEMP1.numAssignedTo = UserMaster.numUserDetailId
		AND UserMaster.numDomainID = @numDomainID
	CROSS APPLY
	(
		SELECT
			OM.numOppID
			,(SUM(dbo.GetOrderItemProfitAmountOrMargin(@numDomainID,OM.numOppId,OI.numoppitemtCode,1))/SUM(ISNULL(monTotAmount,0))) * 100.0 ProfitPercentByOrder
		FROM
			OpportunityMaster OM
		INNER JOIN
			OpportunityItems OI
		ON
			OM.numOppID = OI.numOppId
		INNER JOIN
			Item I
		ON
			OI.numItemCode = I.numItemCode
		WHERE
			OM.numDomainId=@numDomainID
			AND OM.numAssignedTo = TEMP1.numAssignedTo
			AND ISNULL(monTotAmount,0) <> 0
			AND ISNULL(numUnitHour,0) <> 0
			AND ISNULL(OM.tintOppType,0)=1
			AND ISNULL(OM.tintOppStatus,0)=1
			AND I.numItemCode NOT IN (@numShippingItemID,@numDiscountItemID)
			AND (  
		  (DATEADD(MINUTE,-@ClientTimeZoneOffset,OM.bintCreatedDate) BETWEEN @dtStartDate AND @dtEndDate)   
		  OR   
		  (DATEADD(MINUTE,-@ClientTimeZoneOffset,Om.bintCreatedDate) BETWEEN @dtStartDate AND @dtEndDate) 
		  )
		GROUP BY
			OM.numOppId
	) TEMP2
	GROUP BY
		UserMaster.vcUserName,UserMaster.numUserDetailId
	HAVING
		AVG(ProfitPercentByOrder) > 0
	ORDER BY
		AVG(ProfitPercentByOrder) DESC


--#4---Gross revenue
		SELECT
		UserMaster.numUserDetailId,UserMaster.vcUserName,Temp3.InvoiceTotal,@vcFilterBy as vcFilterBy,CAst(@dtStartDate as date) as StartDate,CAST(@dtEndDate  as date) as EndDate
	FROM
	(
		SELECT DISTINCT
			--numAssignedTo
			CASE @vcFilterBy  
		   WHEN 1  -- Assign To  
		   THEN numAssignedTo
		   --else numRecOwner
		   WHEN 2 
		   THEN numRecOwner
		   else  numAssignedTo
		   End as numAssignedTo
		FROM
			OpportunityMaster
		WHERE
			numDomainId=@numDomainID
			AND tintOppType=1
			AND tintOppStatus = 1
				AND (  
      (DATEADD(MINUTE,-@ClientTimeZoneOffset,bintCreatedDate) BETWEEN @dtStartDate AND @dtEndDate)   
      OR   
      (DATEADD(MINUTE,-@ClientTimeZoneOffset,bintCreatedDate) BETWEEN @dtStartDate AND @dtEndDate)  
     ) 
			AND
	 1=(CASE   
     WHEN LEN(ISNULL(@vcFilterValue,0)) > 0 AND @vcFilterBy IN (1,2,3)  
     THEN   
      (CASE @vcFilterBy  
       WHEN 1  -- Assign To  
       THEN (CASE WHEN numAssignedTo IN (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,',')) THEN 1 ELSE 0 END)  
       WHEN 2  -- Record Owner  
       THEN (CASE WHEN numRecOwner IN (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,',')) THEN 1 ELSE 0 END)  
       WHEN 3  -- Teams (Based on Assigned To)  
       --THEN (CASE WHEN numAssignedTo IN (SELECT numUserCntID FROM UserTeams WHERE numDomainID=@numDomainID AND numTeam IN (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,','))) THEN 1 ELSE 0 END)  
	   THEN (CASE WHEN numAssignedTo IN (SELECT numContactId FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numTeam IN (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,','))) THEN 1 ELSE 0 END)  
      END)  
     ELSE 1   
    END)  
			
	) TEMP1
	INNER JOIN
		UserMaster
	ON
		TEMP1.numAssignedTo = UserMaster.numUserDetailId
		AND UserMaster.numDomainID = @numDomainID
	CROSS APPLY
	(
		SELECT
			Sum(
			ISNULL(OBD.monDealAmount,0) 
			 )
			 InvoiceTotal
		FROM
		
		--	OpportunityItems OI
		--INNER JOIN
		
			OpportunityMaster OM
		--ON
		--	OI.numOppId = OM.numOppID
		Inner join 
			OpportunityBizDocs OBD 
		ON
			OBD.numOppId = OM.numOppId and OBD.bitAuthoritativeBizDocs=1
		--INNER JOIN
		--	Item I
		--ON
		--	OI.numItemCode = I.numItemCode
		--Left JOIN 
		--	Vendor V 
		--ON 
		--	V.numVendorID=I.numVendorID 
		--	AND V.numItemCode=I.numItemCode
		WHERE
			OM.numDomainId=@numDomainID

			and CASE @vcFilterBy  
		   WHEN 1  -- Assign To  
		   THEN Om.numAssignedTo
		   WHEN 2 
		   THEN OM.numRecOwner
		   else  OM.numAssignedTo
		   End  = TEMP1.numAssignedTo
			--AND OM.numRecOwner = TEMP1.numAssignedTo
			--AND ISNULL(OI.monTotAmount,0) <> 0
			--AND ISNULL(OI.numUnitHour,0) <> 0
			AND ISNULL(OM.tintOppType,0)=1
			AND ISNULL(OM.tintOppStatus,0)=1
			AND OBD.numBizDocId = 287
			
			--AND I.numItemCode NOT IN (@numShippingItemID,@numDiscountItemID) /*if we keep this condition then sales order invoice total will not match. also it was producing duplicates.*/
			AND (  
      (DATEADD(MINUTE,-@ClientTimeZoneOffset,OM.bintCreatedDate) BETWEEN @dtStartDate AND @dtEndDate)   
      OR   
      (DATEADD(MINUTE,-@ClientTimeZoneOffset,Om.bintCreatedDate) BETWEEN @dtStartDate AND @dtEndDate) 
	  )

	) TEMP3
	
	
	ORDER BY
		 TEMP3.InvoiceTotal DESC
		-----
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportListMaster_ItemClassificationProfitPreBuildReport')
DROP PROCEDURE USP_ReportListMaster_ItemClassificationProfitPreBuildReport
GO
CREATE PROCEDURE [dbo].[USP_ReportListMaster_ItemClassificationProfitPreBuildReport]
	@numDomainID NUMERIC(18,0)
AS
BEGIN 
	DECLARE @numShippingItemID NUMERIC(18,0)
	DECLARE @numDiscountItemID NUMERIC(18,0)
	SELECT @numShippingItemID=ISNULL(numShippingServiceItemID,0),@numDiscountItemID=ISNULL(numDiscountServiceItemID,0) FROM Domain WHERE numDomainId=@numDomainID


	SELECT 
		vcItemClassification
		,(SUM(Profit)/SUM(monTotAmount)) * 100 BlendedProfit
	FROM
	(
		SELECT
			LD.vcData AS vcItemClassification
			,ISNULL(monTotAmount,0) monTotAmount
			,ISNULL(dbo.GetOrderItemProfitAmountOrMargin(@numDomainID,OM.numOppId,OI.numoppitemtCode,1),0) Profit
		FROM
			OpportunityMaster OM
		INNER JOIN
			OpportunityItems OI
		ON
			OM.numOppID = OI.numOppId
		INNER JOIN
			Item I
		ON
			OI.numItemCode = I.numItemCode
		INNER JOIN
			ListDetails LD
		ON
			I.numItemClassification = LD.numListItemID
			AND LD.numListID=36
			AND LD.numDomainID=@numDomainID
		WHERE
			OM.numDomainId=@numDomainID
			AND ISNULL(OI.monTotAmount,0) <> 0
			AND ISNULL(OI.numUnitHour,0) <> 0
			AND ISNULL(OM.tintOppType,0)=1
			AND ISNULL(OM.tintOppStatus,0)=1
			AND OM.bintCreatedDate >= DATEADD(MONTH, -12, GETUTCDATE())
			AND I.numItemCode NOT IN (@numShippingItemID,@numDiscountItemID)
	) TEMP
	GROUP BY
		vcItemClassification
	HAVING 
		(SUM(Profit)/SUM(monTotAmount)) * 100 > 0
	ORDER BY
		(SUM(Profit)/SUM(monTotAmount)) * 100 DESC
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportListMaster_PartnerRMPLast12Months')
DROP PROCEDURE USP_ReportListMaster_PartnerRMPLast12Months
GO
CREATE PROCEDURE [dbo].[USP_ReportListMaster_PartnerRMPLast12Months]
	@numDomainID NUMERIC(18,0)
AS
BEGIN 
	DECLARE @numShippingItemID NUMERIC(18,0)
	DECLARE @numDiscountItemID NUMERIC(18,0)
	DECLARE @ProfitCost INT
	SELECT @numShippingItemID=numShippingServiceItemID,@numDiscountItemID=numDiscountServiceItemID,@ProfitCost=numCost FROM Domain WHERE numDomainId=@numDomainID

	SELECT
		CI.vcCompanyName
		,SUM(OM.monDealAmount) AS monDealAmount
	FROM
		OpportunityMaster OM
	INNER JOIN
		OpportunityItems OI
	ON
		OM.numOppID = OI.numOppId
	INNER JOIN
		DivisionMaster DM
	ON
		OM.numPartner = DM.numDivisionID
	INNER JOIN
		CompanyInfo CI
	ON
		DM.numCompanyID = CI.numCompanyId
	WHERE
		OM.numDomainId=@numDomainID
		AND ISNULL(OM.tintOppType,0)=1
		AND ISNULL(OM.tintOppStatus,0)=1
		AND OM.bintCreatedDate >= DATEADD(MONTH, -12, GETUTCDATE())
	GROUP BY
		CI.vcCompanyName
	HAVING
		SUM(monDealAmount) > 0
	ORDER BY
		SUM(monDealAmount) DESC
		
	SELECT
		vcCompanyName
		,(SUM(Profit)/SUM(monTotAmount)) * 100 BlendedProfit
	FROM
	(
		SELECT
			CI.vcCompanyName
			,ISNULL(monTotAmount,0) monTotAmount
			,ISNULL(dbo.GetOrderItemProfitAmountOrMargin(@numDomainID,OM.numOppId,OI.numoppitemtCode,1),0) Profit
		FROM
			OpportunityMaster OM
		INNER JOIN
			OpportunityItems OI
		ON
			OI.numOppId = OM.numOppID
		INNER JOIN
			DivisionMaster DM
		ON
			OM.numPartner = DM.numDivisionID
		INNER JOIN
			CompanyInfo CI
		ON
			DM.numCompanyID = CI.numCompanyId
		INNER JOIN
			Item I
		ON
			OI.numItemCode = I.numItemCode
		WHERE
			OM.numDomainId=@numDomainID
			AND ISNULL(OI.monTotAmount,0) <> 0
			AND ISNULL(OI.numUnitHour,0) <> 0
			AND ISNULL(OM.tintOppType,0)=1
			AND ISNULL(OM.tintOppStatus,0)=1
			AND OM.bintCreatedDate >= DATEADD(MONTH, -12, GETUTCDATE())
			AND I.numItemCode NOT IN (@numShippingItemID,@numDiscountItemID)
	) TEMP2
	GROUP BY
		vcCompanyName
	HAVING
		(SUM(Profit)/SUM(monTotAmount)) * 100 > 0
	ORDER BY
		(SUM(Profit)/SUM(monTotAmount)) * 100 DESC

	SELECT
		*
	FROM
	(
		SELECT
			CI.vcCompanyName
			,SUM(dbo.GetOrderItemProfitAmountOrMargin(@numDomainID,OM.numOppId,OI.numoppitemtCode,1)) Profit
		FROM
			OpportunityMaster OM
		INNER JOIN
			OpportunityItems OI
		ON
			OI.numOppId = OM.numOppID
		INNER JOIN
			DivisionMaster DM
		ON
			OM.numPartner = DM.numDivisionID
		INNER JOIN
			CompanyInfo CI
		ON
			DM.numCompanyID = CI.numCompanyId
		INNER JOIN
			Item I
		ON
			OI.numItemCode = I.numItemCode
		Left JOIN 
			Vendor V 
		ON 
			V.numVendorID=I.numVendorID 
			AND V.numItemCode=I.numItemCode
		WHERE
			OM.numDomainId=@numDomainID
			AND ISNULL(OI.monTotAmount,0) <> 0
			AND ISNULL(OI.numUnitHour,0) <> 0
			AND ISNULL(OM.tintOppType,0)=1
			AND ISNULL(OM.tintOppStatus,0)=1
			AND OM.bintCreatedDate >= DATEADD(MONTH, -12, GETUTCDATE())
			AND I.numItemCode NOT IN (@numShippingItemID,@numDiscountItemID)
		GROUP BY
			CI.vcCompanyName
	) TEMP2
	ORDER BY
		Profit DESC
END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportListMaster_RPELastMonthAndSamePeriodLastYear')
DROP PROCEDURE USP_ReportListMaster_RPELastMonthAndSamePeriodLastYear
GO
CREATE PROCEDURE [dbo].[USP_ReportListMaster_RPELastMonthAndSamePeriodLastYear]
	@numDomainID NUMERIC(18,0)
	,@ClientTimeZoneOffset INT
	,@vcFilterValue VARCHAR(MAX)
AS
BEGIN 
	DECLARE @ExpenseLastMonth DECIMAL(20,5)
	DECLARE @ExpenseSamePeriodLastYear DECIMAL(20,5)
	DECLARE @ProfitLastMonth DECIMAL(20,5)
	DECLARE @ProfitSamePeriodLastYear DECIMAL(20,5)
	DECLARE @RevenueLastMonth DECIMAL(20,5)
	DECLARE @RevenueSamePeriodLastYear DECIMAL(20,5)
	DECLARE @numShippingItemID NUMERIC(18,0)
	DECLARE @numDiscountItemID NUMERIC(18,0)
	DECLARE @dtLastMonthFromDate AS DATETIME                                       
	DECLARE @dtLastMonthToDate AS DATETIME
	DECLARE @dtSameMonthLYFromDate AS DATETIME                                       
	DECLARE @dtSameMonthLYToDate AS DATETIME

	DECLARE @FinancialYearStartDate AS DATETIME
	SELECT @FinancialYearStartDate = dbo.GetFiscalStartDate(YEAR(GETDATE()),@numDomainID)

	--SELECT @dtLastMonthFromDate = DATEADD(mm,-1,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)), @dtLastMonthToDate = DATEADD(ms,-3,DATEADD(mm,0,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)));
	--SELECT @dtSameMonthLYFromDate = DATEADD(YEAR,-1,@dtLastMonthFromDate),@dtSameMonthLYToDate = DATEADD(YEAR,-1,@dtLastMonthToDate);

	SELECT @dtSameMonthLYFromDate = DATEADD(YEAR,-1,@dtLastMonthFromDate),@dtSameMonthLYToDate = DATEADD(YEAR,-1,@dtLastMonthToDate);
	DECLARE @StartDate AS DATETIME 
	SET @StartDate= DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)

	IF @vcFilterValue = 'ThisWeekVSLastWeek'
	BEGIN
		SET @dtLastMonthFromDate = DATEADD(WEEK,0,@StartDate)
		SET @dtLastMonthToDate= DATEADD(WEEK,-1,DATEADD(mm,0,@StartDate))
	END
	IF @vcFilterValue = 'ThisMonthVSLastMonth'
	BEGIN
		SET @dtLastMonthFromDate = DATEADD(MM,0,@StartDate)
		SET @dtLastMonthToDate= DATEADD(MM,-1,DATEADD(mm,0,@StartDate))
	END
	IF @vcFilterValue = 'ThisQTDVSLastQTD'
	BEGIN
		SET @dtLastMonthFromDate = DATEADD(QQ,0,@StartDate)
		SET @dtLastMonthToDate= DATEADD(QQ,-1,DATEADD(mm,0,@StartDate))
	END
	IF @vcFilterValue = 'ThisYTDVSLastYTD'
	BEGIN
		SET @dtLastMonthFromDate = DATEADD(YY,0,@StartDate)
		SET @dtLastMonthToDate= DATEADD(YY,-1,DATEADD(mm,0,@StartDate))
	END
	


	SELECT 
		@numShippingItemID=numShippingServiceItemID
		,@numDiscountItemID=numDiscountServiceItemID
	FROM 
		Domain 
	WHERE 
		numDomainId=@numDomainID


	-- GET EXPENSE YTD VS SAME PERIOD LAST YEAR
	SELECT @ExpenseLastMonth = SUM(numDebitAmt) - SUM(numCreditAmt) FROM General_Journal_Details INNER JOIN General_Journal_Header ON General_Journal_Details.numJournalId=General_Journal_Header.numJournal_Id WHERE General_Journal_Details.numDomainId=@numDomainID AND datEntry_Date BETWEEN  @dtLastMonthFromDate AND @dtLastMonthToDate AND General_Journal_Details.numChartAcntId IN (SELECT numAccountId FROM Chart_Of_Accounts WHERE numDomainId=@numDomainID AND bitActive = 1 AND numAcntTypeId IN (SELECT numAccountTypeID FROM AccountTypeDetail WHERE numDomainID=@numDomainID AND vcAccountCode LIKE '0104%'))
	SELECT @ExpenseSamePeriodLastYear = SUM(numDebitAmt) - SUM(numCreditAmt) FROM General_Journal_Details INNER JOIN General_Journal_Header ON General_Journal_Details.numJournalId=General_Journal_Header.numJournal_Id WHERE General_Journal_Details.numDomainId=@numDomainID AND datEntry_Date BETWEEN  @dtSameMonthLYFromDate AND @dtSameMonthLYToDate AND General_Journal_Details.numChartAcntId IN (SELECT numAccountId FROM Chart_Of_Accounts WHERE numDomainId=@numDomainID AND bitActive = 1 AND numAcntTypeId IN (SELECT numAccountTypeID FROM AccountTypeDetail WHERE numDomainID=@numDomainID AND vcAccountCode LIKE '0104%'))

	-- GET PROFIT YTD VS SAME PERIOD LAST YEAR
	SELECT 
		@ProfitLastMonth = SUM(Profit)
	FROM
	(
		SELECT
			ISNULL(dbo.GetOrderItemProfitAmountOrMargin(@numDomainID,OM.numOppId,OI.numoppitemtCode,1),0) Profit
		FROM
			OpportunityMaster OM
		INNER JOIN
			OpportunityItems OI
		ON
			OM.numOppID=OI.numOppId
		INNER JOIN
			Item I
		ON
			OI.numItemCode = I.numItemCode
		WHERE
			OM.numDomainId=@numDomainID
			AND OM.bintCreatedDate BETWEEN  @dtLastMonthFromDate AND @dtLastMonthToDate
			AND ISNULL(OM.tintOppType,0)=1
			AND ISNULL(OM.tintOppStatus,0)=1
			AND I.numItemCode NOT IN (@numShippingItemID,@numDiscountItemID)
	) TEMP

	SELECT 
		@ProfitSamePeriodLastYear = SUM(Profit)
	FROM
	(
		SELECT
			ISNULL(dbo.GetOrderItemProfitAmountOrMargin(@numDomainID,OM.numOppId,OI.numoppitemtCode,1),0) Profit
		FROM
			OpportunityMaster OM
		INNER JOIN
			OpportunityItems OI
		ON
			OM.numOppID = OI.numOppId
		INNER JOIN
			Item I
		ON
			OI.numItemCode = I.numItemCode
		WHERE
			OM.numDomainId=@numDomainID
			AND OM.bintCreatedDate BETWEEN  @dtSameMonthLYFromDate AND @dtSameMonthLYToDate
			AND ISNULL(OM.tintOppType,0)=1
			AND ISNULL(OM.tintOppStatus,0)=1
			AND I.numItemCode NOT IN (@numShippingItemID,@numDiscountItemID)
	) TEMP

	-- GET REVENUE YTD VS SAME PERIOD LAST YEAR
	SELECT 
		@RevenueLastMonth = SUM(monTotAmount)
	FROM
	(
		SELECT
			ISNULL(monTotAmount,0) monTotAmount
		FROM
			OpportunityItems OI
		INNER JOIN
			OpportunityMaster OM
		ON
			OI.numOppId = OM.numOppID
		INNER JOIN
			DivisionMaster DM
		ON
			OM.numDivisionId = DM.numDivisionID
		INNER JOIN
			CompanyInfo CI
		ON
			DM.numCompanyID = CI.numCompanyId
		INNER JOIN
			Item I
		ON
			OI.numItemCode = I.numItemCode
		Left JOIN 
			Vendor V 
		ON 
			V.numVendorID=I.numVendorID 
			AND V.numItemCode=I.numItemCode
		WHERE
			OM.numDomainId=@numDomainID
			AND OM.bintCreatedDate BETWEEN  @dtLastMonthFromDate AND @dtLastMonthToDate
			AND ISNULL(OM.tintOppType,0)=1
			AND ISNULL(OM.tintOppStatus,0)=1
	) TEMP

	SELECT 
		@RevenueSamePeriodLastYear = SUM(monTotAmount)
	FROM
	(
		SELECT
			ISNULL(monTotAmount,0) monTotAmount
		FROM
			OpportunityItems OI
		INNER JOIN
			OpportunityMaster OM
		ON
			OI.numOppId = OM.numOppID
		INNER JOIN
			DivisionMaster DM
		ON
			OM.numDivisionId = DM.numDivisionID
		INNER JOIN
			CompanyInfo CI
		ON
			DM.numCompanyID = CI.numCompanyId
		INNER JOIN
			Item I
		ON
			OI.numItemCode = I.numItemCode
		Left JOIN 
			Vendor V 
		ON 
			V.numVendorID=I.numVendorID 
			AND V.numItemCode=I.numItemCode
		WHERE
			OM.numDomainId=@numDomainID
			AND OM.bintCreatedDate BETWEEN  @dtSameMonthLYFromDate AND @dtSameMonthLYToDate
			AND ISNULL(OM.tintOppType,0)=1
			AND ISNULL(OM.tintOppStatus,0)=1
	) TEMP

	SELECT
		100.0 * (ISNULL(@RevenueLastMonth,0) - ISNULL(@RevenueSamePeriodLastYear,0)) / CASE WHEN ISNULL(@RevenueSamePeriodLastYear,0) = 0 THEN 1 ELSE @RevenueSamePeriodLastYear END As RevenueDifference
		,100.0 * (ISNULL(@ProfitLastMonth,0) - ISNULL(@ProfitSamePeriodLastYear,0)) / CASE WHEN ISNULL(@ProfitSamePeriodLastYear,0) = 0 THEN 1 ELSE @ProfitSamePeriodLastYear END As ProfitDifference
		,100.0 * (ISNULL(@ExpenseLastMonth,0) - ISNULL(@ExpenseSamePeriodLastYear,0)) / CASE WHEN ISNULL(@ExpenseSamePeriodLastYear,0) = 0 THEN 1 ELSE @ExpenseSamePeriodLastYear END As ExpenseDifference
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportListMaster_RPEYTDAndSamePeriodLastYear')
DROP PROCEDURE USP_ReportListMaster_RPEYTDAndSamePeriodLastYear
GO
CREATE PROCEDURE [dbo].[USP_ReportListMaster_RPEYTDAndSamePeriodLastYear]
	@numDomainID NUMERIC(18,0)
	,@vcFilterBy TINYINT --added  for Filter the Period of Measure as mentioned in Slide 5
AS
BEGIN 
	DECLARE @ExpenseYTD DECIMAL(20,5)
	DECLARE @ExpenseSamePeriodLastYear DECIMAL(20,5)
	DECLARE @ProfitYTD DECIMAL(20,5)
	DECLARE @ProfitSamePeriodLastYear DECIMAL(20,5)
	DECLARE @RevenueYTD DECIMAL(20,5)
	DECLARE @RevenueSamePeriodLastYear DECIMAL(20,5)
	DECLARE @numShippingItemID NUMERIC(18,0)
	DECLARE @numDiscountItemID NUMERIC(18,0)
	DECLARE @dtCYFromDate AS DATETIME                                       
	DECLARE @dtCYToDate AS DATETIME
	DECLARE @dtLYFromDate AS DATETIME                                       
	DECLARE @dtLYToDate AS DATETIME

	DECLARE @FinancialYearStartDate AS DATETIME
	SELECT @FinancialYearStartDate = dbo.GetFiscalStartDate(YEAR(GETDATE()),@numDomainID)

	--SELECT @dtCYFromDate = @FinancialYearStartDate,@dtCYToDate = dateadd(ms, -3, dateadd(day, datediff(day, 0, getdate())+1, 0));
	--SELECT @dtLYFromDate = DATEADD(YEAR,-1,@FinancialYearStartDate),@dtLYToDate = DATEADD(MS,-1,DATEADD(YEAR,-1,DATEADD(MS, -3, DATEADD(DAY, DATEDIFF(DAY, 0, GETDATE())+1, 0))));
	---alter sp
	IF @vcFilterBy= 1
	BEGIN 
	SELECT @dtCYFromDate = @FinancialYearStartDate,@dtCYToDate = dateadd(ms, -3, dateadd(day, datediff(day, 0, getdate())+1, 0));
	SELECT @dtLYFromDate = DATEADD(YEAR,-1,@FinancialYearStartDate),@dtLYToDate = DATEADD(MS,-1,DATEADD(YEAR,-1,DATEADD(MS, -3, DATEADD(DAY, DATEDIFF(DAY, 0, GETDATE())+1, 0))));
	END
	ELSE IF @vcFilterBy=2
	BEGIN
	select @dtCYFromDate= DATEADD(month, datediff(month, 0, getdate()), 0)
	select @dtCYToDate =dateadd(ms, -3, dateadd(day, datediff(day, 0, getdate())+1, 0))
	select @dtLYFromDate =dateadd(YEAR,-1,DATEADD(month, datediff(month, 0, getdate()), 0))
	select @dtLYToDate = DATEADD(MS,-1,DATEADD(YEAR,-1,DATEADD(MS, -3, DATEADD(DAY, DATEDIFF(DAY, 0, GETDATE())+1, 0))))
	END
	ELSE IF @vcFilterBy=3
	BEGIN
	select @dtCYFromDate =DATEADD(ms,-1,DATEADD(DAY, 2 -  DATEPART(WEEKDAY, GETDATE()), GETDATE()))
	select @dtCYToDate = dateadd(ms, -3, dateadd(day, datediff(day, 0, getdate())+1, 0)) 
	select @dtLYFromDate =DATEADD(year,-1,DATEADD(ms,-1,DATEADD(DAY, 2 -  DATEPART(WEEKDAY, GETDATE()), GETDATE())))
	select @dtLYToDate = DATEADD(MS,-1,DATEADD(YEAR,-1,DATEADD(MS, -3, DATEADD(DAY, DATEDIFF(DAY, 0, GETDATE())+1, 0))))
	END
	ELSE
	BEGIN
	SELECT @dtCYFromDate = @FinancialYearStartDate,@dtCYToDate = dateadd(ms, -3, dateadd(day, datediff(day, 0, getdate())+1, 0));
	SELECT @dtLYFromDate = DATEADD(YEAR,-1,@FinancialYearStartDate),@dtLYToDate = DATEADD(MS,-1,DATEADD(YEAR,-1,DATEADD(MS, -3, DATEADD(DAY, DATEDIFF(DAY, 0, GETDATE())+1, 0))));
	END
	------
	SELECT 
		@numShippingItemID=numShippingServiceItemID
		,@numDiscountItemID=numDiscountServiceItemID
	FROM 
		Domain 
	WHERE 
		numDomainId=@numDomainID


	-- GET EXPENSE YTD VS SAME PERIOD LAST YEAR
	SELECT @ExpenseYTD = SUM(numDebitAmt) - SUM(numCreditAmt) FROM General_Journal_Details INNER JOIN General_Journal_Header ON General_Journal_Details.numJournalId=General_Journal_Header.numJournal_Id WHERE General_Journal_Details.numDomainId=@numDomainID AND datEntry_Date BETWEEN  @dtCYFromDate AND @dtCYToDate AND General_Journal_Details.numChartAcntId IN (SELECT numAccountId FROM Chart_Of_Accounts WHERE numDomainId=@numDomainID AND bitActive = 1 AND numAcntTypeId IN (SELECT numAccountTypeID FROM AccountTypeDetail WHERE numDomainID=@numDomainID AND vcAccountCode LIKE '0104%'))
	SELECT @ExpenseSamePeriodLastYear = SUM(numDebitAmt) - SUM(numCreditAmt) FROM General_Journal_Details INNER JOIN General_Journal_Header ON General_Journal_Details.numJournalId=General_Journal_Header.numJournal_Id WHERE General_Journal_Details.numDomainId=@numDomainID AND datEntry_Date BETWEEN  @dtLYFromDate AND @dtLYToDate AND General_Journal_Details.numChartAcntId IN (SELECT numAccountId FROM Chart_Of_Accounts WHERE numDomainId=@numDomainID AND bitActive = 1 AND numAcntTypeId IN (SELECT numAccountTypeID FROM AccountTypeDetail WHERE numDomainID=@numDomainID AND vcAccountCode LIKE '0104%'))

	-- GET PROFIT YTD VS SAME PERIOD LAST YEAR
	SELECT 
		@ProfitYTD = SUM(Profit)
	FROM
	(
		SELECT
			ISNULL(dbo.GetOrderItemProfitAmountOrMargin(@numDomainID,OM.numOppId,OI.numoppitemtCode,1),0) Profit
		FROM
			OpportunityMaster OM
		INNER JOIN
			OpportunityItems OI
		ON
			OM.numOppID = OI.numOppId
		INNER JOIN
			DivisionMaster DM
		ON
			OM.numDivisionId = DM.numDivisionID
		INNER JOIN
			CompanyInfo CI
		ON
			DM.numCompanyID = CI.numCompanyId
		INNER JOIN
			Item I
		ON
			OI.numItemCode = I.numItemCode
		WHERE
			OM.numDomainId=@numDomainID
			AND OM.bintCreatedDate BETWEEN  @dtCYFromDate AND @dtCYToDate
			AND ISNULL(OM.tintOppType,0)=1
			AND ISNULL(OM.tintOppStatus,0)=1
			AND I.numItemCode NOT IN (@numShippingItemID,@numDiscountItemID)
	) TEMP

	SELECT 
		@ProfitSamePeriodLastYear = SUM(Profit)
	FROM
	(
		SELECT
			ISNULL(dbo.GetOrderItemProfitAmountOrMargin(@numDomainID,OM.numOppId,OI.numoppitemtCode,1),0) Profit
		FROM
			OpportunityMaster OM
		INNER JOIN
			OpportunityItems OI
		ON
			OM.numOppID = OI.numOppId
		INNER JOIN
			DivisionMaster DM
		ON
			OM.numDivisionId = DM.numDivisionID
		INNER JOIN
			CompanyInfo CI
		ON
			DM.numCompanyID = CI.numCompanyId
		INNER JOIN
			Item I
		ON
			OI.numItemCode = I.numItemCode
		WHERE
			OM.numDomainId=@numDomainID
			AND OM.bintCreatedDate BETWEEN  @dtLYFromDate AND @dtLYToDate
			AND ISNULL(OM.tintOppType,0)=1
			AND ISNULL(OM.tintOppStatus,0)=1
			AND I.numItemCode NOT IN (@numShippingItemID,@numDiscountItemID)
	) TEMP

	-- GET REVENUE YTD VS SAME PERIOD LAST YEAR
	SELECT 
		@RevenueYTD = SUM(monTotAmount)
	FROM
	(
		SELECT
			ISNULL(monTotAmount,0) monTotAmount
		FROM
			OpportunityItems OI
		INNER JOIN
			OpportunityMaster OM
		ON
			OI.numOppId = OM.numOppID
		INNER JOIN
			DivisionMaster DM
		ON
			OM.numDivisionId = DM.numDivisionID
		INNER JOIN
			CompanyInfo CI
		ON
			DM.numCompanyID = CI.numCompanyId
		INNER JOIN
			Item I
		ON
			OI.numItemCode = I.numItemCode
		Left JOIN 
			Vendor V 
		ON 
			V.numVendorID=I.numVendorID 
			AND V.numItemCode=I.numItemCode
		WHERE
			OM.numDomainId=@numDomainID
			AND OM.bintCreatedDate BETWEEN  @dtCYFromDate AND @dtCYToDate
			AND ISNULL(OM.tintOppType,0)=1
			AND ISNULL(OM.tintOppStatus,0)=1
	) TEMP

	SELECT 
		@RevenueSamePeriodLastYear = SUM(monTotAmount)
	FROM
	(
		SELECT
			ISNULL(monTotAmount,0) monTotAmount
		FROM
			OpportunityItems OI
		INNER JOIN
			OpportunityMaster OM
		ON
			OI.numOppId = OM.numOppID
		INNER JOIN
			DivisionMaster DM
		ON
			OM.numDivisionId = DM.numDivisionID
		INNER JOIN
			CompanyInfo CI
		ON
			DM.numCompanyID = CI.numCompanyId
		INNER JOIN
			Item I
		ON
			OI.numItemCode = I.numItemCode
		Left JOIN 
			Vendor V 
		ON 
			V.numVendorID=I.numVendorID 
			AND V.numItemCode=I.numItemCode
		WHERE
			OM.numDomainId=@numDomainID
			AND OM.bintCreatedDate BETWEEN  @dtLYFromDate AND @dtLYToDate
			AND ISNULL(OM.tintOppType,0)=1
			AND ISNULL(OM.tintOppStatus,0)=1
	) TEMP

	SELECT
		100.0 * (ISNULL(@RevenueYTD,0) - ISNULL(@RevenueSamePeriodLastYear,0)) / CASE WHEN ISNULL(@RevenueSamePeriodLastYear,0) = 0 THEN 1 ELSE @RevenueSamePeriodLastYear END As RevenueDifference
		,100.0 * (ISNULL(@ProfitYTD,0) - ISNULL(@ProfitSamePeriodLastYear,0)) / CASE WHEN ISNULL(@ProfitSamePeriodLastYear,0) = 0 THEN 1 ELSE @ProfitSamePeriodLastYear END As ProfitDifference
		,100.0 * (ISNULL(@ExpenseYTD,0) - ISNULL(@ExpenseSamePeriodLastYear,0)) / CASE WHEN ISNULL(@ExpenseSamePeriodLastYear,0) = 0 THEN 1 ELSE @ExpenseSamePeriodLastYear END As ExpenseDifference
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportListMaster_Top10CustomerByProfitAmountPreBuildReport')
DROP PROCEDURE USP_ReportListMaster_Top10CustomerByProfitAmountPreBuildReport
GO
CREATE PROCEDURE [dbo].[USP_ReportListMaster_Top10CustomerByProfitAmountPreBuildReport]
	@numDomainID NUMERIC(18,0)
	,@ClientTimeZoneOffset INT
	,@vcTimeLine VARCHAR(50)
	,@tintControlField INT
	,@numRecordCount INT
AS
BEGIN 
	DECLARE @numShippingItemID NUMERIC(18,0)
	DECLARE @numDiscountItemID NUMERIC(18,0)
	DECLARE @ProfitCost INT
	SELECT @numShippingItemID=numShippingServiceItemID,@numDiscountItemID=numDiscountServiceItemID,@ProfitCost=numCost FROM Domain WHERE numDomainId=@numDomainID


	SELECT TOP (@numRecordCount)
		numDivisionID
		,(CASE tintCRMType WHEN 1 THEN CONCAT('~/prospects/frmProspects.aspx?DivID=',numDivisionID) WHEN 2 THEN CONCAT('~/Account/frmAccounts.aspx?DivID=',numDivisionID) ELSE CONCAT('~/Leads/frmLeads.aspx?DivID=',numDivisionID) END) AS URL
		,vcCompanyName
		,SUM(Profit) Profit
	FROM
	(
		SELECT
			DM.numDivisionID
			,tintCRMType
			,CI.vcCompanyName
			,ISNULL(dbo.GetOrderItemProfitAmountOrMargin(@numDomainID,OM.numOppId,OI.numoppitemtCode,1),0) Profit
			,ISNULL(monTotAmount,0) monTotAmount
		FROM
			OpportunityMaster OM
		INNER JOIN
			OpportunityItems OI
		ON
			OM.numOppID = OI.numOppId
		INNER JOIN
			DivisionMaster DM
		ON
			OM.numDivisionId = DM.numDivisionID
		INNER JOIN
			CompanyInfo CI
		ON
			DM.numCompanyID = CI.numCompanyId
		INNER JOIN
			Item I
		ON
			OI.numItemCode = I.numItemCode
		WHERE
			OM.numDomainId=@numDomainID
			AND ISNULL(OI.monTotAmount,0) <> 0
			AND ISNULL(OI.numUnitHour,0) <> 0
			AND ISNULL(OM.tintOppType,0)=1
			AND ISNULL(OM.tintOppStatus,0)=1
			AND I.numItemCode NOT IN (@numShippingItemID,@numDiscountItemID)
	) TEMP
	GROUP BY
		numDivisionID
		,tintCRMType
		,vcCompanyName

	HAVING
		(CASE
		   WHEN @tintControlField=2 THEN (SUM(Profit)/SUM(monTotAmount)) * 100
		   WHEN @tintControlField=1 THEN SUM(Profit) 
		END ) > 0
		--(SUM(Profit)/SUM(monTotAmount)) * 100 > 0
	ORDER BY
		CASE
		   WHEN @tintControlField=2 THEN (SUM(Profit)/ SUM(monTotAmount)) * 100 
		   WHEN @tintControlField=1 THEN SUM(Profit) 
		END DESC
		--(SUM(Profit)/SUM(monTotAmount)) * 100 DESC
	--HAVING
	--	SUM(Profit) > 0
	--ORDER BY
	--	SUM(Profit) DESC
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportListMaster_Top10CustomerByProfitPercentPreBuildReport')
DROP PROCEDURE USP_ReportListMaster_Top10CustomerByProfitPercentPreBuildReport
GO
CREATE PROCEDURE [dbo].[USP_ReportListMaster_Top10CustomerByProfitPercentPreBuildReport]
	@numDomainID NUMERIC(18,0)
	,@ClientTimeZoneOffset INT
	,@vcTimeLine VARCHAR(50)
	,@tintControlField INT
	,@numRecordCount INT
AS
BEGIN 
	DECLARE @numShippingItemID NUMERIC(18,0)
	DECLARE @numDiscountItemID NUMERIC(18,0)
	SELECT @numShippingItemID=numShippingServiceItemID,@numDiscountItemID=numDiscountServiceItemID FROM Domain WHERE numDomainId=@numDomainID


	SELECT TOP (@numRecordCount)
		numDivisionID
		,vcCompanyName
		,(CASE tintCRMType WHEN 1 THEN CONCAT('~/prospects/frmProspects.aspx?DivID=',numDivisionID) WHEN 2 THEN CONCAT('~/Account/frmAccounts.aspx?DivID=',numDivisionID) ELSE CONCAT('~/Leads/frmLeads.aspx?DivID=',numDivisionID) END) AS URL
		,(SUM(Profit)/SUM(monTotAmount)) * 100 BlendedProfit
	FROM
	(
		SELECT
			DM.numDivisionID
			,tintCRMType
			,CI.vcCompanyName
			,ISNULL(monTotAmount,0) monTotAmount
			,ISNULL(dbo.GetOrderItemProfitAmountOrMargin(@numDomainID,OM.numOppId,OI.numoppitemtCode,1),0) Profit
		FROM
			OpportunityMaster OM
		INNER JOIN
			OpportunityItems OI
		ON
			OM.numOppID = OI.numOppId
		INNER JOIN
			DivisionMaster DM
		ON
			OM.numDivisionId = DM.numDivisionID
		INNER JOIN
			CompanyInfo CI
		ON
			DM.numCompanyID = CI.numCompanyId
		INNER JOIN
			Item I
		ON
			OI.numItemCode = I.numItemCode
		Left JOIN 
			Vendor V 
		ON 
			V.numVendorID=I.numVendorID 
			AND V.numItemCode=I.numItemCode
		WHERE
			OM.numDomainId=@numDomainID
			AND ISNULL(monTotAmount,0) <> 0
			AND ISNULL(numUnitHour,0) <> 0
			AND ISNULL(OM.tintOppType,0)=1
			AND ISNULL(OM.tintOppStatus,0)=1
			AND I.numItemCode NOT IN (@numShippingItemID,@numDiscountItemID)
	) TEMP
	GROUP BY
		numDivisionID
		,tintCRMType
		,vcCompanyName
	HAVING
		(CASE
		   WHEN @tintControlField=2 THEN (SUM(Profit)/SUM(monTotAmount)) * 100 
		   WHEN @tintControlField=1 THEN SUM(Profit) 
		END) > 0 
		--(SUM(Profit)/SUM(monTotAmount)) * 100 > 0
	ORDER BY
		CASE
		   WHEN @tintControlField=2 THEN (SUM(Profit)/SUM(monTotAmount)) * 100 
		   WHEN @tintControlField=1 THEN SUM(Profit) 
		END DESC
		--(SUM(Profit)/SUM(monTotAmount)) * 100 DESC
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportListMaster_Top10ItemByProfitMargin')
DROP PROCEDURE USP_ReportListMaster_Top10ItemByProfitMargin
GO
CREATE PROCEDURE [dbo].[USP_ReportListMaster_Top10ItemByProfitMargin]
	@numDomainID NUMERIC(18,0)
AS
BEGIN 
	DECLARE @numShippingItemID NUMERIC(18,0)
	DECLARE @numDiscountItemID NUMERIC(18,0)
	SELECT @numShippingItemID=numShippingServiceItemID,@numDiscountItemID=numDiscountServiceItemID FROM Domain WHERE numDomainId=@numDomainID


	SELECT TOP 10
		numItemCode
		,vcItemName
		,CONCAT('~/Items/frmKitDetails.aspx?ItemCode=',numItemCode,'&frm=All Items') AS URL
		,(SUM(Profit)/SUM(monTotAmount)) * 100 BlendedProfit
	FROM
	(
		SELECT
			I.numItemCode
			,I.vcItemName
			,ISNULL(monTotAmount,0) monTotAmount
			,ISNULL(dbo.GetOrderItemProfitAmountOrMargin(@numDomainID,OM.numOppId,OI.numoppitemtCode,1),0) Profit
		FROM
			OpportunityMaster OM
		INNER JOIN
			OpportunityItems OI
		ON
			OM.numOppID = OI.numOppId
		INNER JOIN
			Item I
		ON
			OI.numItemCode = I.numItemCode
		WHERE
			OM.numDomainId=@numDomainID
			AND ISNULL(monTotAmount,0) <> 0
			AND ISNULL(numUnitHour,0) <> 0
			AND ISNULL(OM.tintOppType,0)=1
			AND ISNULL(OM.tintOppStatus,0)=1
			AND I.numItemCode NOT IN (@numShippingItemID,@numDiscountItemID)
	) TEMP
	GROUP BY
		numItemCode
		,vcItemName
	HAVING
		(SUM(Profit)/SUM(monTotAmount)) * 100 > 0
	ORDER BY
		(SUM(Profit)/SUM(monTotAmount)) * 100 DESC
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportListMaster_Top10ItemByProfitPreBuildReport')
DROP PROCEDURE USP_ReportListMaster_Top10ItemByProfitPreBuildReport
GO
CREATE PROCEDURE [dbo].[USP_ReportListMaster_Top10ItemByProfitPreBuildReport]
	@numDomainID NUMERIC(18,0)
	--,@ClientTimeZoneOffset INT
	,@vcTimeLine VARCHAR(50)
	,@vcGroupBy VARCHAR(50)
	,@vcTeritorry VARCHAR(MAX)
	,@vcFilterBy TINYINT  -- 1:Assign TO, 2: Record Owner, 3: Teams (Based on Assigned To) 
	,@vcFilterValue VARCHAR(MAX)
	,@dtFromDate DATE
	,@dtToDate DATE
	,@numRecordCount INT
	,@tintControlField INT
AS
BEGIN 
	DECLARE @numShippingItemID NUMERIC(18,0)
	DECLARE @numDiscountItemID NUMERIC(18,0)

	SELECT @numShippingItemID=numShippingServiceItemID,@numDiscountItemID=numDiscountServiceItemID FROM Domain WHERE numDomainId=@numDomainID


	SELECT TOP (@numRecordCount)
		numItemCode
		,vcItemName,
		(CASE WHEN @tintControlField = 1 THEN SUM(Profit) 
		   WHEN @tintControlField = 2 THEN (SUM(Profit)/SUM(monTotAmount)) * 100 
		   WHEN @tintControlField = 3 THEN SUM(monTotAmount)  END) as Profit 	 		
		--,SUM(Profit) Profit
	FROM
	(
		SELECT
			I.numItemCode
			,I.vcItemName
			,ISNULL(monTotAmount,0) monTotAmount
			,ISNULL(dbo.GetOrderItemProfitAmountOrMargin(@numDomainID,OM.numOppId,OI.numoppitemtCode,1),0) Profit
		FROM
			OpportunityMaster OM
		INNER JOIN
			OpportunityItems OI
		ON
			OM.numOppID = OI.numOppId
		INNER JOIN
			Item I
		ON
			OI.numItemCode = I.numItemCode
		Left JOIN 
			Vendor V 
		ON 
			V.numVendorID=I.numVendorID 
			AND V.numItemCode=I.numItemCode
		WHERE
			OM.numDomainId=@numDomainID
			AND ISNULL(OM.tintOppType,0)=1
			AND ISNULL(OM.tintOppStatus,0)=1
			AND OM.bintCreatedDate >= DATEADD(MONTH, -12, GETUTCDATE())
			AND I.numItemCode NOT IN (@numShippingItemID,@numDiscountItemID)
	) TEMP	
	GROUP BY
		numItemCode
		,vcItemName
	HAVING
		   (CASE 
		   WHEN @tintControlField = 1 THEN SUM(Profit) 
		   WHEN @tintControlField = 2 THEN (SUM(Profit)/SUM(monTotAmount)) * 100 
		   WHEN @tintControlField = 3 THEN SUM(monTotAmount) 
		   END) > 0
		
	ORDER BY
		   (CASE 
		   WHEN @tintControlField = 1 THEN SUM(Profit) 
		   WHEN @tintControlField = 2 THEN (SUM(Profit)/SUM(monTotAmount)) * 100 
		   WHEN @tintControlField = 3 THEN SUM(monTotAmount) 
		   END) DESC
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportQueryExecute')
DROP PROCEDURE USP_ReportQueryExecute
GO
CREATE PROCEDURE [dbo].[USP_ReportQueryExecute]     
    @numDomainID NUMERIC(18,0),
    @numUserCntID NUMERIC(18,0),
	@numReportID NUMERIC(18,0),
    @ClientTimeZoneOffset INT, 
    @textQuery NTEXT,
	@numCurrentPage NUMERIC(18,0)
AS                 
BEGIN
	DECLARE @tintReportType INT
	DECLARE @numReportModuleID INT
	SELECT @tintReportType=ISNULL(tintReportType,0),@numReportModuleID=ISNULL(numReportModuleID,0) FROM ReportListMaster WHERE numReportID=@numReportID

	IF CHARINDEX('WareHouseItems.numBackOrder',CAST(@textQuery AS VARCHAR(MAX))) > 0 AND @numReportModuleID <> 6
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'WareHouseItems.numBackOrder','(CASE WHEN ISNULL((SELECT SUM(WIInner.numOnHand) FROM WareHouseItems WIInner WHERE WIInner.numItemID=WareHouseItems.numItemID AND WIInner.numWareHouseItemID <> WareHouseItems.numWareHouseItemID AND WIInner.numWareHouseID = WareHouseItems.numWareHouseID),0) >= WareHouseItems.numBackOrder THEN 0 ELSE ISNULL(WareHouseItems.numBackOrder,0) END)')
	END
	
	IF CHARINDEX('isnull(OpportunityItems.numRemainingQtyToPick,0)',CAST(@textQuery AS VARCHAR(MAX))) > 0	
	BEGIN	
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'isnull(OpportunityItems.numRemainingQtyToPick,0)','(isnull(OpportunityItems.numUnitHour,0) - isnull(OpportunityItems.numQtyPicked,0))')	
	END	

	IF CHARINDEX('OpportunityItems.numRemainingQtyToPick',CAST(@textQuery AS VARCHAR(MAX))) > 0	
	BEGIN	
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'OpportunityItems.numRemainingQtyToPick','(isnull(OpportunityItems.numUnitHour,0) - isnull(OpportunityItems.numQtyPicked,0))')	
	END		

	IF CHARINDEX('isnull(OpportunityItems.numRemainingQtyToShip,0)',CAST(@textQuery AS VARCHAR(MAX))) > 0	
	BEGIN	
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'isnull(OpportunityItems.numRemainingQtyToShip,0)','(isnull(OpportunityItems.numUnitHour,0) - isnull(OpportunityItems.numQtyShipped,0))')	
	END	

	IF CHARINDEX('OpportunityItems.numRemainingQtyToShip',CAST(@textQuery AS VARCHAR(MAX))) > 0	
	BEGIN	
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'OpportunityItems.numRemainingQtyToShip','(isnull(OpportunityItems.numUnitHour,0) - isnull(OpportunityItems.numQtyShipped,0))')	
	END	

	IF CHARINDEX('isnull(OpportunityItems.numRemainingQtyToInvoice,0)',CAST(@textQuery AS VARCHAR(MAX))) > 0	
	BEGIN	
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'isnull(OpportunityItems.numRemainingQtyToInvoice,0)','(isnull(OpportunityItems.numUnitHour,0) - isnull((SELECT SUM(numUnitHour) FROM OpportunityBizDocItems INNER JOIN OpportunityBizDocs ON OpportunityBizDocItems.numOppBizDocID = OpportunityBizDocs.numOppBizDocsId  WHERE OpportunityBizDocs.numOppId = OpportunityItems.numOppId AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtCode AND ISNULL(bitAuthoritativeBizDocs, 0) = 1 AND ISNULL(OpportunityBizDocs.numBizDocID,0) = 287),0))')	
	END	

	IF CHARINDEX('OpportunityItems.numRemainingQtyToInvoice',CAST(@textQuery AS VARCHAR(MAX))) > 0	
	BEGIN	
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'OpportunityItems.numRemainingQtyToInvoice','(isnull(OpportunityItems.numUnitHour,0) - isnull((SELECT SUM(numUnitHour) FROM OpportunityBizDocItems INNER JOIN OpportunityBizDocs ON OpportunityBizDocItems.numOppBizDocID = OpportunityBizDocs.numOppBizDocsId  WHERE OpportunityBizDocs.numOppId = OpportunityItems.numOppId AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtCode AND ISNULL(bitAuthoritativeBizDocs, 0) = 1 AND ISNULL(OpportunityBizDocs.numBizDocID,0) = 287),0))')	
	END	

	IF CHARINDEX('AND OpportunityItems.monPendingSales',CAST(@textQuery AS VARCHAR(MAX))) > 0	
	BEGIN	
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'AND OpportunityItems.monPendingSales','AND (ISNULL((ISNULL(ISNULL(dbo.fn_UOMConversion(ISNULL(Item.numBaseUnit, 0), OpportunityItems.numItemCode, Item.numDomainId, ISNULL(OpportunityItems.numUOMId, 0)), 1) * OpportunityItems.numUnitHour, 0) - isnull(OpportunityItems.numQtyShipped, 0) ) * (isnull(OpportunityItems.monPrice, 0)), 0))')	
	END	
	
	IF CHARINDEX('AND OpportunityItems.numPendingToBill',CAST(@textQuery AS VARCHAR(MAX))) > 0	
	BEGIN	
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'AND OpportunityItems.numPendingToBill','AND ISNULL((isnull(OpportunityItems.numUnitHourReceived, 0)) - (ISNULL(( SELECT SUM(numUnitHour)  FROM	 OpportunityBizDocItems  INNER JOIN	OpportunityBizDocs ON OpportunityBizDocItems.numOppBizDocID = OpportunityBizDocs.numOppBizDocsId  WHERE OpportunityBizDocs.numOppId = OpportunityItems.numOppId AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtCode AND ISNULL(bitAuthoritativeBizDocs, 0) = 1), 0)), 0)')	
	END	
		
	IF CHARINDEX('AND OpportunityItems.numPendingtoReceive',CAST(@textQuery AS VARCHAR(MAX))) > 0	
	BEGIN	
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'AND OpportunityItems.numPendingtoReceive','AND isnull((isnull((SELECT SUM(numUnitHour) FROM OpportunityBizDocItems INNER JOIN OpportunityBizDocs ON OpportunityBizDocItems.numOppBizDocID = OpportunityBizDocs.numOppBizDocsId WHERE OpportunityBizDocs.numOppId = OpportunityItems.numOppId AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtCode AND ISNULL(bitAuthoritativeBizDocs, 0) = 1), 0)) - (isnull(OpportunityItems.numUnitHourReceived, 0)), 0)')	
	END

	IF CHARINDEX('AND WareHouseItems.vcLocation',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'AND WareHouseItems.vcLocation','AND WareHouseItems.numWLocationID')
	END

	IF CHARINDEX('ReturnItems.monPrice',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'ReturnItems.monPrice','CAST(ReturnItems.monPrice AS NUMERIC(18,4))')
	END

	IF CHARINDEX('OpportunityItems.monPrice',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'OpportunityItems.monPrice','CAST(OpportunityItems.monPrice AS NUMERIC(18,4))')
	END

	IF CHARINDEX('isnull(category.numCategoryID,0)',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'isnull(category.numCategoryID,0)' ,'(SELECT ISNULL(STUFF((SELECT '','' + CONVERT(VARCHAR(50) , Category.vcCategoryName) FROM ItemCategory LEFT JOIN Category ON ItemCategory.numCategoryID = Category.numCategoryID WHERE ItemCategory.numItemID = Item.numItemCode FOR XML PATH('''')), 1, 1, ''''),''''))')
	END

	IF CHARINDEX('dbo.FormatedDateFromDate(dateadd(minute,-@ClientTimeZoneOffset,OpportunityBizDocs.dtFullPaymentDate),@numDomainId)',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'dbo.FormatedDateFromDate(dateadd(minute,-@ClientTimeZoneOffset,OpportunityBizDocs.dtFullPaymentDate),@numDomainId)' ,'(CASE 
																																															WHEN ISNULL(OpportunityMaster.tintOppType,0) = 2 AND ISNULL(OpportunityBizDocs.monDealAmount,0) > 0 AND ISNULL(OpportunityBizDocs.monAmountPaid,0) >= ISNULL(OpportunityBizDocs.monDealAmount,0)
																																															THEN dbo.FormatedDateFromDate((SELECT MAX(BPH.dtPaymentDate) FROM BillPaymentDetails BPD INNER JOIN BillPaymentHeader BPH ON BPD.numBillPaymentID=BPH.numBillPaymentID  WHERE BPD.numOppBizDocsID=OpportunityBizDocs.numOppBizDocsId),@numDomainID)
																																															WHEN ISNULL(OpportunityMaster.tintOppType,0) = 1 AND ISNULL(OpportunityBizDocs.monDealAmount,0) > 0 AND ISNULL(OpportunityBizDocs.monAmountPaid,0) >= ISNULL(OpportunityBizDocs.monDealAmount,0)
																																															THEN dbo.FormatedDateFromDate((SELECT MAX(dtCreatedDate) FROM DepositeDetails WHERE DepositeDetails.numOppBizDocsID=OpportunityBizDocs.numOppBizDocsId),@numDomainID)
																																															ELSE ''''
																																														END)')
	END

	--KEEP IT BELOW ABOVE REPLACEMENT
	IF CHARINDEX('OpportunityBizDocs.dtFullPaymentDate',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'OpportunityBizDocs.dtFullPaymentDate' ,'CAST((CASE 
																												WHEN ISNULL(OpportunityMaster.tintOppType,0) = 2 AND ISNULL(OpportunityBizDocs.monDealAmount,0) > 0 AND ISNULL(OpportunityBizDocs.monAmountPaid,0) >= ISNULL(OpportunityBizDocs.monDealAmount,0)
																												THEN (SELECT MAX(BPH.dtPaymentDate) FROM BillPaymentDetails BPD INNER JOIN BillPaymentHeader BPH ON BPD.numBillPaymentID=BPH.numBillPaymentID  WHERE BPD.numOppBizDocsID=OpportunityBizDocs.numOppBizDocsId)
																												WHEN ISNULL(OpportunityMaster.tintOppType,0) = 1 AND ISNULL(OpportunityBizDocs.monDealAmount,0) > 0 AND ISNULL(OpportunityBizDocs.monAmountPaid,0) >= ISNULL(OpportunityBizDocs.monDealAmount,0)
																												THEN (SELECT MAX(dtCreatedDate) FROM DepositeDetails WHERE DepositeDetails.numOppBizDocsID=OpportunityBizDocs.numOppBizDocsId)
																												ELSE ''''
																											END) AS DATE)')
	END

	IF CHARINDEX('isnull(OpportunityMaster.monProfit,''0'')',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'isnull(OpportunityMaster.monProfit,''0'')' ,'ISNULL(dbo.GetOrderItemProfitAmountOrMargin(OpportunityMaster.numDomainID,OpportunityItems.numOppId,OpportunityItems.numoppitemtCode,1),0)')
	END

	-- THIS ONE SHOULD BE AFTER ABOVE ONE
	IF CHARINDEX('OpportunityMaster.monProfit',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'OpportunityMaster.monProfit' ,'ISNULL(dbo.GetOrderItemProfitAmountOrMargin(OpportunityMaster.numDomainID,OpportunityItems.numOppId,OpportunityItems.numoppitemtCode,1),0)')
	END

	IF CHARINDEX('isnull(OpportunityBizDocItems.monProfit,''0'')',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'isnull(OpportunityBizDocItems.monProfit,''0'')','ISNULL(dbo.GetBizDocItemProfitAmountOrMargin(OpportunityMaster.numDomainID,OpportunityMaster.numOppId,OpportunityBizDocItems.numOppBizDocID,OpportunityBizDocItems.numOppBizDocItemID,1),0)')
	END

	-- THIS ONE SHOULD BE AFTER ABOVE ONE
	IF CHARINDEX('OpportunityBizDocItems.monProfit',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'OpportunityBizDocItems.monProfit','ISNULL(dbo.GetBizDocItemProfitAmountOrMargin(OpportunityMaster.numDomainID,OpportunityMaster.numOppId,OpportunityBizDocItems.numOppBizDocID,OpportunityBizDocItems.numOppBizDocItemID,1),0)')
	END

	IF CHARINDEX('ISNULL(PT.numItemCode,0)=Item.numItemCode',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'ISNULL(PT.numItemCode,0)=Item.numItemCode' ,'Item.numItemCode=PT.numItemCode')
	END

	IF CHARINDEX('isnull(OpportunityMaster.numProfitPercent,0)',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'isnull(OpportunityMaster.numProfitPercent,0)' ,'ISNULL(dbo.GetOrderItemProfitAmountOrMargin(OpportunityMaster.numDomainID,OpportunityItems.numOppId,OpportunityItems.numoppitemtCode,2),0)')
	END
	
	-- THIS ONE SHOULD BE AFTER ABOVE ONE
	IF CHARINDEX('OpportunityMaster.numProfitPercent',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'OpportunityMaster.numProfitPercent' ,'ISNULL(dbo.GetOrderItemProfitAmountOrMargin(OpportunityMaster.numDomainID,OpportunityItems.numOppId,OpportunityItems.numoppitemtCode,2),0)')
	END

	IF CHARINDEX('isnull(OpportunityBizDocItems.numProfitPercent,0)',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'isnull(OpportunityBizDocItems.numProfitPercent,0)','ISNULL(dbo.GetBizDocItemProfitAmountOrMargin(OpportunityMaster.numDomainID,OpportunityItems.numOppId,OpportunityItems.numoppitemtCode,2),0)')
	END
	
	-- THIS ONE SHOULD BE AFTER ABOVE ONE
	IF CHARINDEX('OpportunityBizDocItems.numProfitPercent',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'OpportunityBizDocItems.numProfitPercent' ,'ISNULL(dbo.GetBizDocItemProfitAmountOrMargin(OpportunityMaster.numDomainID,OpportunityItems.numOppId,OpportunityItems.numoppitemtCode,2),0)')
	END

	IF ISNULL(@numCurrentPage,0) > 0 
		AND ISNULL(@tintReportType,0) <> 3 
		AND ISNULL(@tintReportType,0) <> 4 
		AND ISNULL(@tintReportType,0) <> 5 
		AND CHARINDEX('Select top',CAST(@textQuery AS VARCHAR(MAX))) <> 1
		AND (SELECT COUNT(*) FROM ReportSummaryGroupList WHERE numReportID=@numReportID) = 0
	BEGIN
		SET @textQuery = CONCAT(CAST(@textQuery AS VARCHAR(MAX)),' OFFSET ',(@numCurrentPage-1) * 100 ,' ROWS FETCH NEXT 100 ROWS ONLY OPTION (OPTIMIZE FOR (@numDomainID UNKNOWN));')
	END
	ELSE
	BEGIN
		SET @textQuery = CONCAT(CAST(@textQuery AS VARCHAR(MAX)),' OPTION (OPTIMIZE FOR (@numDomainID UNKNOWN));')
	END
	 
	 print CAST(@textQuery AS ntext)
	EXECUTE sp_executesql @textQuery,N'@numDomainID numeric(18, 0),@numUserCntID numeric(18,0),@ClientTimeZoneOffset int',@numDomainID=@numDomainID,@numUserCntID=@numUserCntID,@ClientTimeZoneOffset=@ClientTimeZoneOffset
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_SalesOrderLineItemsPOLinking_Save')
DROP PROCEDURE USP_SalesOrderLineItemsPOLinking_Save
GO
CREATE PROCEDURE [dbo].[USP_SalesOrderLineItemsPOLinking_Save]
	@numDomainID NUMERIC(18,0)
	,@numSalesOrderID NUMERIC(18,0)
	,@numSalesOrderItemID NUMERIC(18,0)
	,@numPurchaseOrderID NUMERIC(18,0)
	,@numPurchaseOrderItemID NUMERIC(18,0)
AS
BEGIN
	IF EXISTS (SELECT ID FROM SalesOrderLineItemsPOLinking WHERE numDomainID=@numDomainID AND numSalesOrderID=@numSalesOrderID AND numSalesOrderItemID=@numSalesOrderItemID)
	BEGIN
		UPDATE
			SalesOrderLineItemsPOLinking
		SET
			numPurchaseOrderID=@numPurchaseOrderID
			,numPurchaseOrderItemID=@numPurchaseOrderItemID
		WHERE 
			numDomainID=@numDomainID 
			AND numSalesOrderID=@numSalesOrderID 
			AND numSalesOrderItemID=@numSalesOrderItemID
	END
	ELSE
	BEGIN
		INSERT INTO SalesOrderLineItemsPOLinking
		(
			numDomainID,numSalesOrderID,numSalesOrderItemID,numPurchaseOrderID,numPurchaseOrderItemID
		)
		VALUES
		(
			@numDomainID,@numSalesOrderID,@numSalesOrderItemID,@numPurchaseOrderID,@numPurchaseOrderItemID
		)
	END
END
GO
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_StagePercentageDetailsTaskTimeLog_GetByTaskID')
DROP PROCEDURE dbo.USP_StagePercentageDetailsTaskTimeLog_GetByTaskID
GO
CREATE PROCEDURE [dbo].[USP_StagePercentageDetailsTaskTimeLog_GetByTaskID]
(
	@numDomainID NUMERIC(18,0)
	,@numTaskID NUMERIC(18,0)
	,@ClientTimeZoneOffset INT
	,@bitForProject BIT=0
	,@vcTotalTimeSpendOnTask VARCHAR(50) OUTPUT
)
AS 
BEGIN
	IF(@bitForProject=1)
	BEGIN
		SET @vcTotalTimeSpendOnTask = dbo.GetTimeSpendOnTaskByProject(@numDomainID,@numTaskID,0)
	END
	ELSE
	BEGIN
		SET @vcTotalTimeSpendOnTask = dbo.GetTimeSpendOnTask(@numDomainID,@numTaskID,0)
	END

	SELECT
		SPDTTL.ID
		,SPDTTL.numTaskID
		,SPDTTL.tintAction
		,ISNULL(SPDTTL.numProcessedQty,0) numProcessedQty
		,ISNULL(SPDTTL.numReasonForPause,0) numReasonForPause
		,dbo.fn_GetContactName(SPDTTL.numUserCntID) vcEmployee
		,CASE SPDTTL.tintAction
			WHEN 1 THEN 'Started'
			WHEN 2 THEN 'Paused'
			WHEN 3 THEN 'Resumed'
			WHEN 4 THEN 'Finished'
			ELSE ''
		END vcAction
		,CONCAT(dbo.FormatedDateFromDate(DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,SPDTTL.dtActionTime),@numDomainID),' ',CONVERT(VARCHAR(5),DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,SPDTTL.dtActionTime), 108),' ',RIGHT(CONVERT(VARCHAR(30),DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,SPDTTL.dtActionTime),9),2)) vcActionTime
		,DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,SPDTTL.dtActionTime) dtActionTime
		,CASE WHEN tintAction = 2 THEN CAST(numProcessedQty AS VARCHAR) ELSE '' END vcProcessedQty
		,ISNULL(LD.vcData,'') vcReasonForPause
		,ISNULL(SPDTTL.vcNotes,'') vcNotes
		,(CASE WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog S WHERE S.numTaskID = @numTaskID AND tintAction=4) THEN 1 ELSE 0 END) bitTaskFinished
	FROM
		StagePercentageDetailsTaskTimeLog SPDTTL
	LEFT JOIN
		ListDetails LD
	ON
		LD.numListID = 52
		AND SPDTTL.numReasonForPause = LD.numListItemID
	WHERE
		SPDTTL.numDomainID=@numDomainID
		AND SPDTTL.numTaskID=@numTaskID
	ORDER BY
		ID 
END
GO
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_StagePercentageDetailsTaskTimeLog_Save')
DROP PROCEDURE dbo.USP_StagePercentageDetailsTaskTimeLog_Save
GO
CREATE PROCEDURE [dbo].[USP_StagePercentageDetailsTaskTimeLog_Save]
(
	@ID NUMERIC(18,0)
	,@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@numTaskID NUMERIC(18,0)
	,@tintAction TINYINT
	,@dtActionTime DATETIME
	,@numProcessedQty FLOAT
	,@numReasonForPause NUMERIC(18,0)
	,@vcNotes VARCHAR(1000)
	,@bitManualEntry BIT
)
AS 
BEGIN

	IF NOT EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numDomainID=@numDomainID AND numTaskID=@numTaskID AND tintAction=4)
	BEGIN
		IF @tintAction=1 AND EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numDomainID=@numDomainID AND numTaskID=@numTaskID AND tintAction=1 AND ID <> ISNULL(@ID,0))
		BEGIN
			RAISERROR('TASK_IS_ALREADY_STARTED',16,1)
		END
		ELSE
		BEGIN
			IF EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE ID=@ID)
			BEGIN
				UPDATE
					StagePercentageDetailsTaskTimeLog
				SET
					tintAction = @tintAction
					,dtActionTime =  DATEADD(MINUTE, DATEDIFF(MINUTE,0,@dtActionTime),0)
					,numProcessedQty=@numProcessedQty
					,vcNotes = @vcNotes
					,bitManualEntry=@bitManualEntry
					,numModifiedBy = @numUserCntID
					,numReasonForPause = @numReasonForPause
					,dtModifiedDate = GETUTCDATE()
				WHERE
					ID = @ID			
			END
			ELSE
			BEGIN
				IF EXISTS (SELECT numTaskId FROM StagePercentageDetailsTask WHERE numDomainID=@numDomainID AND numTaskId=@numTaskID)
				BEGIN
					DECLARE @numTaskAssignee AS NUMERIC(18,0)
					SELECT @numTaskAssignee=numAssignTo FROM StagePercentageDetailsTask WHERE numDomainID=@numDomainID AND numTaskId=@numTaskID

					INSERT INTO StagePercentageDetailsTaskTimeLog
					(
						numDomainID
						,numUserCntID
						,numTaskID
						,tintAction
						,dtActionTime
						,numProcessedQty
						,numReasonForPause
						,vcNotes
						,bitManualEntry
					)
					VALUES
					(
						@numDomainID
						,@numTaskAssignee
						,@numTaskID
						,@tintAction
						,DATEADD(MINUTE, DATEDIFF(MINUTE,0,@dtActionTime),0)
						,@numProcessedQty
						,@numReasonForPause
						,@vcNotes
						,@bitManualEntry
					)
				END
				ELSE
				BEGIN
					RAISERROR('TASK_DOES_NOT_EXISTS',16,1)
				END
			END
		END
	END
	ELSE
	BEGIN
		RAISERROR('TASK_IS_MARKED_AS_FINISHED',16,1)
	END
	DECLARE @numProjectId NUMERIC=0
	SET @numProjectId = ISNULL((SELECT TOP 1 numProjectId FROM StagePercentageDetailsTask where numTaskId=@numTaskID),0)
	IF(@numProjectId>0)
	BEGIN
		DECLARE @dtmActualStartDate AS DATETIME
		DECLARE @dtmActualFinishDate AS DATETIME
		SET @dtmActualStartDate = (SELECT MIN(dtActionTime) FROM StagePercentageDetailsTaskTimeLog where numDomainID=@numDomainID AND numTaskID IN(SELECT numTaskId FROM StagePercentageDetailsTask WHERE numDomainID=@numDomainID AND numProjectId=@numProjectId))
		
		DECLARE @intTotalProgress AS INT =0
		DECLARE @intTotalTaskCount AS INT=0
		DECLARE @intTotalTaskClosed AS INT =0
		SET @intTotalTaskCount=(SELECT COUNT(*) FROM StagePercentageDetailsTask WHERE numProjectId=@numProjectId AND numDomainID=@numDomainID)
		SET @intTotalTaskClosed=(SELECT COUNT(distinct numTaskID) FROM StagePercentageDetailsTaskTimeLog WHERE numTaskId IN (SELECT numTaskId FROM StagePercentageDetailsTask WHERE numProjectId=@numProjectId AND numDomainID=@numDomainID) AND tintAction=4)
		IF(@intTotalTaskCount=@intTotalTaskClosed)
		BEGIN
			SET @dtmActualFinishDate = (SELECT MAX(dtActionTime) FROM StagePercentageDetailsTaskTimeLog where numDomainID=@numDomainID AND numTaskID IN(SELECT numTaskId FROM StagePercentageDetailsTask WHERE numDomainID=@numDomainID AND numProjectId=@numProjectId))
			UPDATE ProjectsMaster SET dtCompletionDate=@dtmActualFinishDate WHERE numProId=@numProjectId
		END
		IF(@intTotalTaskCount>0 AND @intTotalTaskClosed>0)
		BEGIN
			SET @intTotalProgress=ROUND(((@intTotalTaskClosed*100)/@intTotalTaskCount),0)
		END
		IF((SELECT COUNT(*) FROM ProjectProgress WHERE ISNULL(numProId,0)=@numProjectId AND numDomainId=@numDomainID)>0)
		BEGIN
			UPDATE ProjectProgress SET intTotalProgress=@intTotalProgress WHERE ISNULL(numProId,0)=@numProjectId AND numDomainId=@numDomainID
		END
		ELSE
		BEGIN
			INSERT INTO ProjectProgress(
				numProId,
				numOppId,
				numDomainId,
				intTotalProgress,
				numWorkOrderId
			)VALUES(
				@numProjectId,
				NULL,
				@numDomainID,
				@intTotalProgress,
				NULL
			)
		END

		DECLARE @numContractsLogId NUMERIC(18,0)
		DECLARE @numContractID NUMERIC(18,0)
		DECLARE @numUsedTime INT
		DECLARE @balanceContractTime INT

		-- REMOVE EXISTING CONTRACT LOG ENTRY
		IF EXISTS (SELECT 
					CL.numContractId  
				FROM 
					ContractsLog CL 
				WHERE
					CL.numDivisionID = @numDomainID
					AND CL.numReferenceId = @numTaskID
					AND vcDetails='1')
		BEGIN
			SELECT 
				@numContractsLogId=numContractsLogId
				,@numContractID = CL.numContractId
				,@numUsedTime = numUsedTime
			FROM
				ContractsLog CL 
			INNER JOIN
				Contracts C
			ON
				CL.numContractId = C.numContractId
			WHERE
				C.numDomainId = @numDomainID
				AND CL.numReferenceId = @numTaskID
				AND vcDetails='1'

			UPDATE Contracts SET timeUsed=ISNULL(timeUsed,0)-ISNULL(@numUsedTime,0),dtmModifiedOn=GETUTCDATE() WHERE numContractId=@numContractID
			DELETE FROM ContractsLog WHERE numContractsLogId=@numContractsLogId
		END

		IF EXISTS (SELECT 
						C.numContractId 
					FROM 
						ProjectsMaster PM 
					INNER JOIN 
						Contracts C 
					ON 
						PM.numDivisionId=C.numDivisonId 
					WHERE 
						PM.numDomainID = @numDomainID
						AND PM.numProID = @numProjectId
						AND C.numDomainId=@numDomainID
						AND C.numDivisonId=PM.numDivisionId 
						AND C.intType=1)
		BEGIN
			IF EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numDomainID = @numDomainID AND numTaskId = @numTaskID AND tintAction = 4)
			BEGIN
				SELECT TOP 1
					@numContractID =  C.numContractId
				FROM
					ProjectsMaster PM
				INNER JOIN 
					Contracts C 
				ON 
					PM.numDivisionId=C.numDivisonId 
				WHERE 
					PM.numDomainID = @numDomainID
					AND PM.numProID = @numProjectId
					AND C.numDomainId=@numDomainID
					AND C.numDivisonId=PM.numDivisionId 
					AND C.intType=1
				ORDER BY
					C.numContractId DESC

				-- GET UPDATED TIME
				SET @numUsedTime = CAST(dbo.GetTimeSpendOnTaskByProject(@numDomainID,@numTaskID,2) AS INT)

				-- CREATE NEW ENTRY FOR
				UPDATE Contracts SET timeUsed=timeUsed+(ISNULL(@numUsedTime,0) * 60),dtmModifiedOn=GETUTCDATE() WHERE numContractId=@numContractID
				SET @balanceContractTime = (SELECT ISNULL((ISNULL(timeLeft,0)-ISNULL(timeUsed,0)),0) FROM Contracts WHERE numContractId=@numContractID)
				INSERT INTO ContractsLog 
				(
					intType, numDivisionID, numReferenceId, dtmCreatedOn, numCreatedBy,vcDetails,numUsedTime,numBalance,numContractId
				)
				SELECT 
					1,Contracts.numDivisonId,@numTaskID,GETUTCDATE(),0,1,(ISNULL(@numUsedTime,0) * 60),@balanceContractTime,numContractId
				FROM 
					Contracts
				WHERE 
					numContractId = @numContractID
			END
		END
	END
END
GO
/****** Object:  StoredProcedure [dbo].[USP_UpdateOppItems]    Script Date: 07/26/2008 16:21:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_updateoppitems')
DROP PROCEDURE usp_updateoppitems
GO
CREATE PROCEDURE [dbo].[USP_UpdateOppItems]
@numOppID as numeric(9)=0,
@numItemCode as numeric(9)=0,
@numUnitHour as FLOAT=0,
@monPrice as DECIMAL(20,5),
@vcItemName VARCHAR(300),
@vcModelId VARCHAR(200),
@vcPathForTImage VARCHAR(300),
@vcItemDesc as varchar(1000),
@numWarehouseItmsID as numeric(9),
@ItemType as Varchar(50),
@DropShip as bit=0,
@numUOM as numeric(9)=0,
@numClassID as numeric(9)=0,
@numProjectID as numeric(9)=0,
@vcNotes varchar(1000)=null,
@dtItemRequiredDate DATETIME = null,
@vcAttributes VARCHAR(MAX),
@vcAttributeIDs VARCHAR(MAX)
as                                                   
	SET @vcNotes = (CASE WHEN @vcNotes='&nbsp;' THEN '' ELSE @vcNotes END)
	SET @vcItemDesc = (CASE WHEN ISNULL(@vcItemDesc,'')='' THEN ISNULL((SELECT txtItemDesc FROM Item WHERE numItemCode=@numItemCode),'') ELSE @vcItemDesc END)
	
   if @numWarehouseItmsID=0 set @numWarehouseItmsID=null 
	
	DECLARE @vcManufacturer VARCHAR(250)
	SELECT @vcManufacturer = vcManufacturer FROM item WHERE [numItemCode]=@numItemCode
    insert into OpportunityItems                                                    
	(numOppId,numItemCode,vcNotes,numUnitHour,monPrice,numCost,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,[vcItemName],[vcModelID],[vcPathForTImage],[vcManufacturer],numUOMId,numClassID,numProjectID,numSortOrder,ItemRequiredDate,vcAttributes,vcAttrValues)
	values
	(@numOppID,@numItemCode,@vcNotes,@numUnitHour,@monPrice,@monPrice,@numUnitHour*@monPrice,@vcItemDesc,@numWarehouseItmsID,@ItemType,@DropShip,0,0,@numUnitHour*@monPrice,@vcItemName,@vcModelId,@vcPathForTImage,@vcManufacturer,@numUOM,@numClassID,@numProjectID,ISNULL((SELECT MAX(numSortOrder) FROM OpportunityItems WHERE numOppID=@numOppID),0) + 1,@dtItemRequiredDate,@vcAttributes,@vcAttributeIDs)
	
	select SCOPE_IDENTITY()
	                                 
	update OpportunityMaster set  monPamount=(select sum(monTotAmount)from OpportunityItems where numOppId=@numOppID)                                                     
	where numOppId=@numOppID
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Vendor_GetDetail')
DROP PROCEDURE USP_Vendor_GetDetail
GO
CREATE PROCEDURE [dbo].[USP_Vendor_GetDetail]
(
	@numDomainID NUMERIC(18,0)
	,@numDivisionID NUMERIC(18,0)
	,@numItemCode NUMERIC(18,0)
	,@vcAttrValues VARCHAR(500)
)
AS
BEGIN
	SELECT TOP 10
		ISNULL(OI.numUnitHour,0) - ISNULL(OI.numUnitHourReceived,0) numQty
		,dbo.FormatedDateFromDate(OI.ItemRequiredDate,@numDomainID) vcDueDate
	FROM
		OpportunityMaster OM
	INNER JOIN
		OpportunityItems OI
	ON
		OM.numOppId = OI.numOppId
	WHERE
		OM.numDomainId = @numDomainID
		AND OM.numDivisionId = @numDivisionID
		AND OM.tintOppType = 2
		AND OM.tintOppType = 1
		AND ISNULL(OM.tintshipped,0) = 0
		AND OI.numItemCode = @numItemCode
		AND ISNULL(OI.vcAttrValues,'') = ISNULL(@vcAttrValues,'')
		AND ISNULL(OI.numUnitHour,0) - ISNULL(OI.numUnitHourReceived,0) > 0

	SELECT 
		SUM(ISNULL(OI.numUnitHour,0) - ISNULL(OI.numUnitHourReceived,0) * (monTotAmount/(CASE WHEN ISNULL(OI.numUnitHour,0) = 0 THEN 1 ELSE OI.numUnitHour END))) AS TotalPOAmount
		,SUM(ISNULL(OI.numUnitHour,0) - ISNULL(OI.numUnitHourReceived,0)) AS TotalQty
		,SUM(ISNULL(I.fltWeight,0) * (ISNULL(OI.numUnitHour,0) - ISNULL(OI.numUnitHourReceived,0))) AS TotalWeight
		,STUFF((SELECT 
					' <b>Buy</b> '+ CASE WHEN u.intType=1 THEN '$'+CAST(u.vcBuyingQty AS VARCHAR(MAX)) ELSE CAST(u.vcBuyingQty AS VARCHAR(MAX)) END+' ' + CASE WHEN u.intType=1 THEN 'Amount' WHEN u.intType=2 THEN 'Quantity' WHEN u.intType=3 THEN 'Lbs' ELSE '' END+' <b>Get</b> ' + (CASE WHEN u.tintDiscountType = 1 THEN CONCAT('$',u.vcIncentives,' off') ELSE CONCAT(u.vcIncentives,'% off') END) + '<br/>'
				FROM 
					PurchaseIncentives u
				WHERE 
					u.numPurchaseIncentiveId = numPurchaseIncentiveId 
					AND numDivisionId=@numDivisionID
				ORDER BY 
					u.numPurchaseIncentiveId
				FOR XML PATH(''), TYPE).value('.', 'varchar(max)'),1,1,'')  AS Purchaseincentives
	FROM 
		OpportunityMaster OM
	INNER JOIN
		OpportunityItems OI
	ON
		OM.numOppId = OI.numOppId 
	INNER JOIN 
		Item AS I 
	ON 
		OI.numItemCode=I.numItemCode 
	WHERE 
		OM.numDomainId = @numDomainID
		AND OM.numDivisionId = @numDivisionID
		AND OM.tintOppType = 2
		AND OM.tintOppType = 1
		AND ISNULL(OM.tintshipped,0) = 0
END
GO
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_WorkOrder_GetTasks')
DROP PROCEDURE dbo.USP_WorkOrder_GetTasks
GO
CREATE PROCEDURE [dbo].[USP_WorkOrder_GetTasks]
(
	@numDomainID NUMERIC(18,0)
	,@numWOID NUMERIC(18,0)
	,@numStageDetailsId NUMERIC(18,0)
	,@ClientTimeZoneOffset INT
	,@tintMode TINYINT
)
AS 
BEGIN
	IF @tintMode = 1
	BEGIN
		DECLARE @numQtyToBuild FLOAT
		DECLARE @dtPlannedStartDate DATETIME
		SELECT 
			@numQtyToBuild=numQtyItemsReq
			,@dtPlannedStartDate=ISNULL(WorkOrder.dtmStartDate,WorkOrder.bintCreatedDate) 
		FROM 
			WorkOrder 
		WHERE
			WorkOrder.numDomainID=@numDomainID 
			AND WorkOrder.numWOId=@numWOID

		DECLARE @TempTaskAssignee TABLE
		(
			numAssignedTo NUMERIC(18,0)
			,dtLastTaskCompletionTime DATETIME
		)

		DECLARE @TempTasks TABLE
		(
			ID INT IDENTITY(1,1)
			,numTaskID NUMERIC(18,0)
			,numTaskTimeInMinutes NUMERIC(18,0)
			,numTaskAssignee NUMERIC(18,0)
			,intTaskType INT --1:Parallel, 2:Sequential
			,dtPlannedStartDate DATETIME
		)

		INSERT INTO @TempTasks
		(
			numTaskID
			,numTaskTimeInMinutes
			,intTaskType
			,numTaskAssignee
		)
		SELECT 
			SPDT.numTaskID
			,((ISNULL(SPDT.numHours,0) * 60) + ISNULL(SPDT.numMinutes,0)) * @numQtyToBuild
			,ISNULL(SPDT.intTaskType,1)
			,SPDT.numAssignTo
		FROM 
			StagePercentageDetailsTask SPDT
		INNER JOIN
			WorkOrder WO
		ON
			SPDT.numWorkOrderId = WO.numWOId
		WHERE 
			SPDT.numDomainId=@numDomainID
			AND SPDT.numWorkOrderId = @numWOID

		UPDATE @TempTasks SET dtPlannedStartDate=@dtPlannedStartDate

		INSERT INTO @TempTaskAssignee
		(
			numAssignedTo
		)
		SELECT DISTINCT
			numTaskAssignee
		FROM
			@TempTasks

		DECLARE @i INT = 1
		DECLARE @iCount INT 	
		SELECT @iCount = COUNT(*) FROM @TempTasks

		DECLARE @numTaskAssignee NUMERIC(18,0)
		DECLARE @intTaskType INT
		DECLARE @numWorkScheduleID NUMERIC(18,0)
		DECLARE @numTempUserCntID NUMERIC(18,0)
		DECLARE @dtStartDate DATETIME
		DECLARE @numTotalTaskInMinutes NUMERIC(18,0)
		DECLARE @numProductiveTimeInMinutes NUMERIC(18,0)
		DECLARE @tmStartOfDay TIME(7)
		DECLARE @numTimeLeftForDay NUMERIC(18,0)
		DECLARE @vcWorkDays VARCHAR(20)
		DECLARE @bitParallelStartSet BIT = 0

		WHILE @i <= @iCount
		BEGIN
			SELECT
				@numTaskAssignee=numTaskAssignee
				,@numTotalTaskInMinutes=numTaskTimeInMinutes
				,@intTaskType = intTaskType
			FROM
				@TempTasks 
			WHERE
				ID=@i

			-- GET TASK COMPLETION TIME WHICH WILL BE START DATETIME FOR NEXT TASK
			SELECT
				@numWorkScheduleID = WS.ID
				,@numProductiveTimeInMinutes=(ISNULL(WS.numProductiveHours,0) * 60) + ISNULL(WS.numProductiveMinutes,0)
				,@tmStartOfDay = tmStartOfDay
				,@vcWorkDays=CONCAT(',',vcWorkDays,',')
				,@dtStartDate = DATEADD(MINUTE,@ClientTimeZoneOffset,CONVERT(DATETIME,CONVERT(VARCHAR(8),DATEADD(MINUTE,-@ClientTimeZoneOffset,@dtPlannedStartDate),112)) + tmStartOfDay)
			FROM
				WorkSchedule WS
			INNER JOIN
				UserMaster
			ON
				WS.numUserCntID = UserMaster.numUserDetailId
			WHERE 
				WS.numUserCntID = @numTaskAssignee

			IF @intTaskType=1 AND EXISTS (SELECT numAssignedTo FROM @TempTaskAssignee WHERE numAssignedTo = @numTaskAssignee AND dtLastTaskCompletionTime IS NOT NULL)
			BEGIN
				SELECT @dtStartDate = dtLastTaskCompletionTime FROM @TempTaskAssignee WHERE numAssignedTo = @numTaskAssignee
			END
			ELSE IF EXISTS (SELECT numAssignedTo FROM @TempTaskAssignee WHERE numAssignedTo = @numTaskAssignee AND dtLastTaskCompletionTime IS NOT NULL)
			BEGIN
				SELECT @dtStartDate = dtLastTaskCompletionTime FROM @TempTaskAssignee WHERE numAssignedTo = @numTaskAssignee
			END

			UPDATE @TempTasks SET dtPlannedStartDate=@dtStartDate WHERE ID=@i 

			IF @numProductiveTimeInMinutes > 0 AND @numTotalTaskInMinutes > 0
			BEGIN
				WHILE @numTotalTaskInMinutes > 0
				BEGIN
					-- IF ITS WORKING DAY AND ALSO NOT HOLIDAY THEN PROCEED OR INCRESE DATE TO NEXT DAY
					IF CHARINDEX(CONCAT(',',DATEPART(WEEKDAY,@dtStartDate),','),@vcWorkDays) > 0 AND NOT EXISTS (SELECT ID FROM WorkScheduleDaysOff WHERE numWorkScheduleID=@numWorkScheduleID AND CAST(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,@dtStartDate) AS DATE) BETWEEN dtDayOffFrom AND dtDayOffTo)
					BEGIN
						IF CAST(@dtStartDate AS DATE) = CAST(GETUTCDATE() AS DATE)
						BEGIN
							-- CHECK TIME LEFT FOR DAY BASED
							SET @numTimeLeftForDay = DATEDIFF(MINUTE,@dtStartDate,DATEADD(MINUTE,@numProductiveTimeInMinutes,DATEADD(MINUTE,@ClientTimeZoneOffset,CONVERT(DATETIME,CONVERT(VARCHAR(8),DATEADD(MINUTE,-@ClientTimeZoneOffset,@dtStartDate),112)) + @tmStartOfDay)))
						END
						ELSE
						BEGIN
							SET @numTimeLeftForDay = @numProductiveTimeInMinutes
						END

						IF @numTimeLeftForDay > 0
						BEGIN
							IF @numTimeLeftForDay > @numTotalTaskInMinutes
							BEGIN
								SET @dtStartDate = DATEADD(MINUTE,@numTotalTaskInMinutes,@dtStartDate)
							END
							ELSE
							BEGIN
								SET @dtStartDate = DATEADD(MINUTE,@ClientTimeZoneOffset,CONVERT(DATETIME,CONVERT(VARCHAR(8),DATEADD(MINUTE,-@ClientTimeZoneOffset,DATEADD(DAY,1,@dtStartDate)),112)) + @tmStartOfDay)
							END

							SET @numTotalTaskInMinutes = @numTotalTaskInMinutes - @numTimeLeftForDay
						END
						ELSE
						BEGIN
							SET @dtStartDate = DATEADD(MINUTE,@ClientTimeZoneOffset,CONVERT(DATETIME,CONVERT(VARCHAR(8),DATEADD(MINUTE,-@ClientTimeZoneOffset,DATEADD(DAY,1,@dtStartDate)),112)) + @tmStartOfDay)
						END
					END
					ELSE
					BEGIN
						SET @dtStartDate = DATEADD(MINUTE,@ClientTimeZoneOffset,CONVERT(DATETIME,CONVERT(VARCHAR(8),DATEADD(MINUTE,-@ClientTimeZoneOffset,DATEADD(DAY,1,@dtStartDate)),112)) + @tmStartOfDay)					
					END				
				END
			END	

			UPDATE @TempTaskAssignee SET dtLastTaskCompletionTime=@dtStartDate WHERE numAssignedTo=@numTaskAssignee

			SET @i = @i + 1
		END

		SELECT	
			SPDT.numTaskId
			,SPDT.numAssignTo
			,SPDT.numReferenceTaskId
			,SPDT.vcTaskName
			,dbo.fn_GetContactName(SPDT.numAssignTo) vcAssignedTo
			,'Work Station 1' AS vcWorkStation
			,CASE
				WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId AND tintAction=4) THEN ISNULL(WO.numQtyItemsReq,0)
				ELSE ISNULL((SELECT SUM(numProcessedQty) FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId),0)
			END AS numProcessedQty
			,(CASE
				WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId AND tintAction=4) THEN 0
				ELSE ISNULL(WO.numQtyItemsReq,0) - ISNULL((SELECT SUM(numProcessedQty) FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId),0)
			END) AS numRemainingQty
			,CASE ISNULL((SELECT TOP 1 tintAction FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId ORDER BY dtActionTime DESC, ID DESC),0) 
				WHEN 4 THEN '<img src="../images/comflag.png" />'
				WHEN 3 THEN CONCAT('<ul class="list-inline" style="margin-bottom:0px;"><li style="vertical-align:middle;padding-left:0px;padding-right:0px;"><button class="btn btn-xs btn-info" onclick="return ShowTaskTimeLogWindow(',SPDT.numTaskId,');"><i class="fa fa-clock-o"></i></button></li><li style="vertical-align:middle;padding-right:0px;"><button class="btn btn-xs btn-flat btn-success btn-task-start" onclick="return TaskStarted(this,',SPDT.numTaskId,',0);">Start</button></li><li style="vertical-align:middle;padding-right:0px;"><label class="taskTimerInitial"></label></li></ul>')
				WHEN 2 THEN CONCAT('<ul class="list-inline" style="margin-bottom:0px;"><li style="vertical-align:middle;padding-left:0px;padding-right:0px;"><button class="btn btn-xs btn-info" onclick="return ShowTaskTimeLogWindow(',SPDT.numTaskId,');"><i class="fa fa-clock-o"></i></button></li><li style="vertical-align:middle;padding-right:0px;"><button class="btn btn-xs btn-flat bg-purple" onclick="return TaskStarted(this,',SPDT.numTaskId,',1);">Resume</button></li></ul>')
				WHEN 1 THEN CONCAT('<ul class="list-inline" style="margin-bottom:0px;"><li style="vertical-align:middle;padding-left:0px;padding-right:0px;"><button class="btn btn-xs btn-info" onclick="return ShowTaskTimeLogWindow(',SPDT.numTaskId,');"><i class="fa fa-clock-o"></i></button></li><li style="vertical-align:middle;padding-right:0px;"><button class="btn btn-xs btn-flat btn-warning" onclick="return ShowTaskPausedWindow(this,',SPDT.numTaskId,');">Pause</button></li><li style="vertical-align:middle;padding-right:0px;"><input type="text" onkeydown="return ProcessedUnitsChanged(this,',SPDT.numTaskId,',event);" class="form-control txtUnitsProcessed" style="width:50px;padding:1px 2px 1px 2px;height:23px;" /></li><li style="vertical-align:middle;padding-right:0px;"><label class="taskTimerInitial"></label></li></ul>')
				ELSE CONCAT('<ul class="list-inline" style="margin-bottom:0px;"><li style="vertical-align:middle;padding-left:0px;padding-right:0px;"><button class="btn btn-xs btn-info" onclick="return ShowTaskTimeLogWindow(',SPDT.numTaskId,');"><i class="fa fa-clock-o"></i></button></li><li style="vertical-align:middle;padding-right:0px;"><button class="btn btn-xs btn-flat btn-success btn-task-start" onclick="return TaskStarted(this,',SPDT.numTaskId,',0);">Start</button></li></ul>')
			END  AS vcTaskControls
			,FORMAT(FLOOR((((ISNULL(SPDT.numHours,0) * 60) + ISNULL(SPDT.numMinutes,0)) * @numQtyToBuild) / 60),'00') + ':' + FORMAT((((ISNULL(SPDT.numHours,0) * 60) + ISNULL(SPDT.numMinutes,0)) * CAST(@numQtyToBuild AS DECIMAL)) % 60,'00')  vcEstimatedTaskTime
			,(((ISNULL(SPDT.numHours,0) * 60) + ISNULL(SPDT.numMinutes,0)) * @numQtyToBuild) numTaskEstimationInMinutes
			,(CASE ISNULL((SELECT TOP 1 tintAction FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=SPDT.numTaskId ORDER BY SPDTTL.dtActionTime DESC,SPDTTL.ID DESC),0) 
				WHEN 4 THEN 0
				WHEN 3 THEN dbo.GetTimeSpendOnTask(SPDT.numDomainID,SPDT.numTaskId,2)
				WHEN 2 THEN dbo.GetTimeSpendOnTask(SPDT.numDomainID,SPDT.numTaskId,2)
				WHEN 1 THEN 0
			END) numTimeSpentInMinutes
			,(CASE ISNULL((SELECT TOP 1 tintAction FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId ORDER BY dtActionTime DESC,ID DESC),0) 
				WHEN 4 THEN NULL
				WHEN 3 THEN DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,(SELECT TOP 1 dtActionTime FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=SPDT.numTaskId ORDER BY SPDTTL.dtActionTime DESC,SPDTTL.ID DESC))
				WHEN 2 THEN NULL
				WHEN 1 THEN DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,(SELECT TOP 1 dtActionTime FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=SPDT.numTaskId ORDER BY SPDTTL.dtActionTime DESC,SPDTTL.ID DESC))
			END) dtLastStartDate
			,CASE WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId AND tintAction=4)
				THEN dbo.GetTimeSpendOnTaskByProject(SPDT.numDomainID,SPDT.numTaskId,1)
				ELSE ''
			END vcActualTaskTimeHtml
			,(CASE WHEN dbo.GetTimeSpendOnTask(SPDT.numDomainID,SPDT.numTaskId,0) = 'Invalid time sequence' THEN '00:00' ELSE dbo.GetTimeSpendOnTask(SPDT.numDomainID,SPDT.numTaskId,0) END) vcActualTaskTime
			,CASE 
				WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId AND tintAction=1)
				THEN CONCAT('<span>',dbo.FormatedDateTimeFromDate(DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,(SELECT dtActionTime FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId AND tintAction=1)),@numDomainID),'</span>')
				ELSE CONCAT('<i style="color:#a6a6a6">',dbo.FormatedDateTimeFromDate((CASE
										WHEN TT.dtPlannedStartDate IS NULL
										THEN DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,ISNULL(WO.dtmStartDate,WO.bintCreatedDate))
										ELSE DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,TT.dtPlannedStartDate)
									END),@numDomainID),' (planned)</i>')
			END dtPlannedStartDate
			,CASE WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId AND tintAction=1)
				THEN DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,(SELECT dtActionTime FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId AND tintAction=1))
				ELSE ''
			END AS dtStartDate
			,CASE WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId AND tintAction=4)
				THEN DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,(SELECT dtActionTime FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId AND tintAction=4))
				ELSE ''
			END AS dtFinishDate
			,CASE WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId AND tintAction=4)
				THEN dbo.FormatedDateTimeFromDate(DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,(SELECT dtActionTime FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId AND tintAction=4)),@numDomainID)
				ELSE ''
			END AS vcFinishDate
			,CASE WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId AND tintAction=4)
				THEN 1
				ELSE 0
			END AS bitTaskCompleted
		FROM 
			StagePercentageDetailsTask SPDT
		INNER JOIN
			WorkOrder WO
		ON
			SPDT.numWorkOrderId = WO.numWOId
		LEFT JOIN
			@TempTasks TT
		ON
			SPDT.numTaskId = TT.numTaskID
		WHERE
			SPDT.numStageDetailsId = @numStageDetailsId
			AND SPDT.numWorkOrderId = @numWOID
	END
	ELSE IF @tintMode = 2
	BEGIN
		SELECT	
			SPDT.numTaskId
			,SPDT.numReferenceTaskId
			,SPDT.numStageDetailsId
			,SPDT.vcTaskName
			,ISNULL(SPDT.numHours,0) numHours
			,ISNULL(SPDT.numMinutes,0) numMinutes
			,SPDT.numAssignTo
			,ADC.numTeam
			,CONCAT(FORMAT(ISNULL(SPDT.numHours,0),'00'),':',FORMAT(ISNULL(SPDT.numMinutes,0),'00')) vcEstimatedTaskTime
			,(CASE WHEN dbo.GetTimeSpendOnTask(SPDT.numDomainID,SPDT.numTaskId,0) = 'Invalid time sequence' THEN '00:00' ELSE dbo.GetTimeSpendOnTask(SPDT.numDomainID,SPDT.numTaskId,0) END) vcActualTaskTime
			,CASE WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId AND tintAction=1)
				THEN DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,(SELECT dtActionTime FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId AND tintAction=1))
				ELSE ''
			END AS dtStartDate
			,CASE WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId AND tintAction=4)
				THEN DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,(SELECT dtActionTime FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId AND tintAction=4))
				ELSE ''
			END AS dtFinishDate
			,CASE WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId)
				THEN 1
				ELSE 0
			END bitTaskStarted
		FROM 
			StagePercentageDetailsTask SPDT
		INNER JOIN
			WorkOrder WO
		ON
			SPDT.numWorkOrderId = WO.numWOId
		INNER JOIN
			AdditionalContactsInformation ADC
		ON
			SPDT.numAssignTo = ADC.numContactId
		LEFT JOIN
			@TempTasks TT
		ON
			SPDT.numTaskId = TT.numTaskID
		WHERE
			SPDT.numStageDetailsId = @numStageDetailsId
			AND SPDT.numWorkOrderId = @numWOID
	END
END
GO

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetCompanyDetailsFrom850')
DROP PROCEDURE dbo.USP_GetCompanyDetailsFrom850
GO

CREATE PROCEDURE [dbo].[USP_GetCompanyDetailsFrom850]
	@numDivisionId NUMERIC(18,0)
	,@vcMarketplace VARCHAR(300)
AS 
BEGIN
	SELECT 
		ISNULL(DM.numAssignedTo,ACI.numContactId) AS numContactID
		,DM.numDivisionID
		,ISNULL(DMEmployer.tintInbound850PickItem,0) AS tintInbound850PickItem
		,CI.numCompanyId
		,CI.vcCompanyName
		,ISNULL((SELECT ID FROM eChannelHub WHERE vcMarketplace=ISNULL(@vcMarketplace,'')),0) numMarketplaceID
	FROM
		DivisionMaster DM
	INNER JOIN 
		CompanyInfo CI
	ON 
		DM.numCompanyID = CI.numCompanyId
	INNER JOIN
		AdditionalContactsInformation ACI 
	ON
		DM.numDivisionID = ACI.numDivisionId
	INNER JOIN 
		Domain D
	ON
		DM.numDomainID = D.numDomainID
	INNER JOIN
		DivisionMaster DMEmployer
	ON
		D.numDivisionID = DMEmployer.numDivisionID
	WHERE
		DM.numDivisionID = @numDivisionId
		AND ISNULL(ACI.bitPrimaryContact,0)=1
END
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetItemDetailsFor850')
DROP PROCEDURE dbo.USP_GetItemDetailsFor850
GO

CREATE PROCEDURE [dbo].[USP_GetItemDetailsFor850]
	@Inbound850PickItem INT
	,@vcItemIdentification VARCHAR(100)
	,@numDivisionId NUMERIC(18,0)
	,@numCompanyId NUMERIC(18,0)
	,@numDomainId NUMERIC(18,0)
	,@numOrderSource NUMERIC(18,0)
AS 
BEGIN

	If EXISTS (SELECT ID FROM ItemMarketplaceMapping WHERE numDomainID=@numDomainId AND numMarketplaceID=@numOrderSource AND ISNULL(vcMarketplaceUniqueID,'')=ISNULL(@vcItemIdentification,''))
	BEGIN
		SELECT
			Item.* 
		FROM 
			ItemMarketplaceMapping 
		INNER JOIN
			Item
		ON
			ItemMarketplaceMapping.numItemCode = Item.numItemCode
		WHERE 
			ItemMarketplaceMapping.numDomainID=@numDomainId 
			AND ItemMarketplaceMapping.numMarketplaceID=@numOrderSource 
			AND ISNULL(ItemMarketplaceMapping.vcMarketplaceUniqueID,'')=ISNULL(@vcItemIdentification,'')
	END
	ELSE
	BEGIN
		IF @Inbound850PickItem = 1 --SKU
		BEGIN
			SELECT * FROM ITEM WHERE numDomainID = @numDomainId AND vcSKU = @vcItemIdentification
		END
		ELSE IF @Inbound850PickItem = 2  -- UPC
		BEGIN
			SELECT * FROM ITEM WHERE numDomainID = @numDomainId AND numBarCodeId = @vcItemIdentification
		END
		ELSE IF @Inbound850PickItem = 3  -- ItemName
		BEGIN
			SELECT * FROM ITEM WHERE numDomainID = @numDomainId AND vcItemName = @vcItemIdentification
		END
		ELSE IF @Inbound850PickItem = 4   -- BizItemID
		BEGIN
			SELECT * FROM ITEM WHERE numDomainID = @numDomainId AND numItemCode = @vcItemIdentification
		END
		ELSE IF @Inbound850PickItem = 5   -- ASIN
		BEGIN
			SELECT * FROM ITEM WHERE numDomainID = @numDomainId AND vcASIN = @vcItemIdentification
		END
		ELSE IF @Inbound850PickItem = 6  -- CustomerPart#
		BEGIN
			SELECT * FROM ITEM I
			INNER JOIN CustomerPartNumber CPN ON I.numItemCode = CPN.numItemCode
			WHERE CPN.numDomainId = @numDomainId AND CPN.numCompanyId = @numCompanyId AND CPN.CustomerPartNo = @vcItemIdentification
		END
		ELSE IF @Inbound850PickItem = 7   -- VendorPart#
		BEGIN
			SELECT * FROM ITEM I
			INNER JOIN Vendor V ON I.numItemCode = V.numItemCode
			WHERE V.numDomainID = @numDomainId AND V.numVendorID = @numDivisionId AND V.vcPartNo = @vcItemIdentification
		END
	END
END

GO


GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MigrationDataFromDomainToDomain')
DROP PROCEDURE USP_MigrationDataFromDomainToDomain
GO
CREATE PROCEDURE [dbo].[USP_MigrationDataFromDomainToDomain]    
@sourceDomainId AS NUMERIC(18,0)=0, -- 153 Kirk & Matz
@destinationDomainId AS NUMERIC(18,0)=0, --172 Boneta, Inc
@sourceGroupId AS NUMERIC(18,0)=0,--710
@destinationGroupId AS NUMERIC(18,0)=0,--858
@taskToPerform AS VARCHAR(100)=NULL--'1,2,3'
AS
BEGIN
BEGIN TRY
BEGIN TRANSACTION;  

-----------------------------------------------------------Dropdown List Sync----------------------------------------------------------------------
IF(1 IN(SELECT Items FROM dbo.Split(@taskToPerform,',')))
BEGIN
PRINT 'Start Dropdown sync'
CREATE TABLE #tempSrcListMaster(
	numExistListID NUMERIC(18,0),
	vcListName VARCHAR(MAX), 
	bitDeleted Bit, 
	bitFixed Bit, 
	bitFlag Bit, 
	vcDataType VARCHAR(MAX), 
	numModuleID NUMERIC(18,0),
	ID NUMERIC(18,0) IDENTITY(1,1)
)

INSERT INTO #tempSrcListMaster
SELECT numListID,vcListName,bitDeleted,bitFixed,bitFlag,vcDataType,numModuleID FROM listmaster WHERE numDomainID=@sourceDomainId OR bitFlag=1

DECLARE @totalListRecords AS INT=0
SET @totalListRecords =(SELECT COUNT(*) FROM #tempSrcListMaster)
DECLARE @i AS INT =1
DECLARE @vcListName AS  VARCHAR(MAX)
DECLARE @bitDeleted AS BIT
DECLARE @bitFixed AS BIT
DECLARE @bitFlag AS BIT
DECLARE @vcDataType AS VARCHAR(MAX)
DECLARE @numModuleID AS NUMERIC(18,0)
DECLARE @numListID AS NUMERIC(18,0)
DECLARE @numExistListID AS NUMERIC(18,0)

WHILE(@i<=@totalListRecords)
BEGIN
	SELECT @numExistListID=numExistListID,@vcListName=vcListName,@bitDeleted=bitDeleted,@bitFixed=bitFixed,@bitFlag=bitFlag,@vcDataType=vcDataType,@numModuleID=numModuleID FROM #tempSrcListMaster WHERE ID=@i
	IF(@bitFlag=1)
	BEGIN
		SET @numListID =@numExistListID
	END
	ELSE IF NOT EXISTS(SELECT * FROM ListMaster WHERE vcListName=@vcListName AND numDomainID=@destinationDomainId AND numModuleID=@numModuleID)
	BEGIN
		INSERT INTO ListMaster(vcListName, numCreatedBy, bintCreatedDate, numModifiedBy, bintModifiedDate, bitDeleted, bitFixed, numDomainID, bitFlag, vcDataType, numModuleID)
		VALUES(@vcListName,1,GETDATE(),1,GETDATE(),@bitDeleted,@bitFixed,@destinationDomainId,@bitFlag,@vcDataType,@numModuleID)
		SET @numListID = SCOPE_IDENTITY()
	END
	ELSE
	BEGIN
		SET @numListID =(SELECT TOP 1 numListID FROM ListMaster WHERE vcListName=@vcListName AND numDomainID=@destinationDomainId AND numModuleID=@numModuleID)
	END

	INSERT INTO ListDetails(numListID, vcData, bitDelete, numDomainID, constFlag, sintOrder, numListType, tintOppOrOrder,numListItemGroupId)
	SELECT @numListID,vcData, bitDelete, @destinationDomainId, constFlag, sintOrder, numListType, tintOppOrOrder,
	CASE WHEN ISNULL(numListItemGroupId,0)>0 THEN 
	(SELECT TOP 1 numListItemID FROM ListDetails WHERE numDomainID=@destinationDomainId AND numListID=@numListID AND 
	vcData=(SELECT TOP 1 vcData FROM ListDetails As L where L.numListItemID=numListItemGroupId AND numDomainID=@sourceDomainId)) ELSE 0 END 
	FROM ListDetails WHERE 
	numDomainID=@sourceDomainId AND  numListID=@numExistListID AND 
	vcData NOT IN(SELECT vcData FROM ListDetails WHERE numDomainID=@destinationDomainId AND numListID=@numListID)


	INSERT INTO ListOrder(numListId,numListItemId,numDomainId,intSortOrder)
	SELECT @numListID,(SELECT TOP 1 numListItemId FROM ListDetails WHERE numListId=@numListID AND vcData=LD.vcData),@destinationDomainId,intSortOrder FROM 
	ListOrder AS LO LEFT JOIN ListDetails AS LD ON Lo.numListItemId=LD.numListItemID where LO.numListId=@numExistListID AND LO.numDomainId=@sourceDomainId
	SET @i = @i+1
END
DROP TABLE #tempSrcListMaster
END
-----------------------------------------------------------Biz Form Wizard Configuration Sync----------------------------------------------------------------------
IF(2 IN(SELECT Items FROM dbo.Split(@taskToPerform,',')))
BEGIN
CREATE TABLE #tempFormFieldGroupConfigurarionMaster(
	numOldFormFieldGroupId NUMERIC(18,0),
	numFormFieldGroupId NUMERIC(18,0),
	ID NUMERIC(18,0) IDENTITY(1,1)
)
INSERT INTO #tempFormFieldGroupConfigurarionMaster(numOldFormFieldGroupId,numFormFieldGroupId)
SELECT numFormFieldGroupId,0 FROM FormFieldGroupConfigurarion WHERE numDomainId=@sourceDomainId 

SET @i=1;
SELECT @totalListRecords = COUNT(*) FROM #tempFormFieldGroupConfigurarionMaster
DECLARE @numFormFieldGroupId AS NUMERIC(18,0)=0
DECLARE @numOldFormFieldGroupId AS NUMERIC(18,0)=0

WHILE(@i<=@totalListRecords)
BEGIN
	SELECT @numOldFormFieldGroupId = numOldFormFieldGroupId FROM #tempFormFieldGroupConfigurarionMaster WHERE ID=@i
	IF EXISTS(SELECT numFormFieldGroupId FROM FormFieldGroupConfigurarion WHERE vcGroupName=(SELECT TOP 1 vcGroupName FROM FormFieldGroupConfigurarion WHERE numFormFieldGroupId=@numOldFormFieldGroupId) AND numDomainId=@destinationDomainId)
	BEGIN
		SELECT @numFormFieldGroupId = numFormFieldGroupId FROM FormFieldGroupConfigurarion WHERE vcGroupName=(SELECT TOP 1 vcGroupName FROM #tempFormFieldGroupConfigurarionMaster WHERE ID=@i) AND numDomainId=@destinationDomainId
	END
	ELSE
	BEGIN
		INSERT INTO FormFieldGroupConfigurarion(numFormId, numGroupId, vcGroupName, numDomainId, numOrder)
		SELECT F.numFormId,F.numGroupId,F.vcGroupName,@destinationDomainId,F.numOrder FROM #tempFormFieldGroupConfigurarionMaster AS T LEFT JOIN FormFieldGroupConfigurarion AS F ON T.numOldFormFieldGroupId=F.numFormFieldGroupId WHERE F.numDomainId=@sourceDomainId AND ID=@i 
		SET @numFormFieldGroupId=SCOPE_IDENTITY();
	END
	UPDATE #tempFormFieldGroupConfigurarionMaster SET numFormFieldGroupId=@numFormFieldGroupId WHERE ID=@i
	SET @i=@i+1;
END
--ADD Custom Text Field to DycFieldMaster
CREATE TABLE #tempDycFieldMaster(
	numOldFieldId NUMERIC(18,0),
	numFieldId NUMERIC(18,0),
	ID NUMERIC(18,0) IDENTITY(1,1)
)
INSERT INTO #tempDycFieldMaster(numOldFieldId,numFieldId)
SELECT  numFieldId,0 FROM DycFieldMaster where numDomainID>0 AND numDomainID=@sourceDomainId
SET @i=1;
DECLARE @numFieldId AS NUMERIC(18,0)=0
DECLARE @numOldFieldId AS NUMERIC(18,0)=0
SELECT @totalListRecords = COUNT(*) FROM #tempDycFieldMaster
WHILE(@i<=@totalListRecords)
BEGIN
	SELECT @numOldFieldId = numOldFieldId FROM #tempDycFieldMaster WHERE ID=@i
	IF EXISTS(SELECT numFieldId FROM DycFieldMaster WHERE vcFieldName=(SELECT TOP 1 vcFieldName FROM DycFieldMaster WHERE numFieldId=@numOldFieldId) AND numDomainId=@destinationDomainId)
	BEGIN
		SELECT @numFieldId = numFieldId FROM DycFieldMaster WHERE vcFieldName=(SELECT TOP 1 vcFieldName FROM #tempDycFieldMaster WHERE ID=@i) AND numDomainId=@destinationDomainId
	END
	ELSE
	BEGIN
		INSERT INTO DycFieldMaster(numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, intColumnWidth, intFieldMaxLength, intWFCompare, vcGroup, vcWFCompareField)
		SELECT numModuleID, @destinationDomainId, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, intColumnWidth, intFieldMaxLength, intWFCompare, vcGroup, vcWFCompareField
		FROM #tempDycFieldMaster AS T LEFT JOIN DycFieldMaster AS D ON T.numOldFieldId=D.numFieldId WHERE D.numDomainId=@sourceDomainId AND ID=@i 
		SET @numFieldId=SCOPE_IDENTITY();
	END
	UPDATE #tempDycFieldMaster SET numFieldId=@numFieldId WHERE ID=@i
	SET @i=@i+1;
END

DELETE FROM DycFormField_Mapping WHERE  numDomainID>0 AND numDomainID=@destinationDomainId
INSERT INTO DycFormField_Mapping(numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
SELECT numModuleID, CASE WHEN (SELECT COUNT(*) FROM #tempDycFieldMaster WHERE numOldFieldId=D.numFieldId) > 0 THEN (SELECT TOP 1  numFieldId FROM #tempDycFieldMaster WHERE numOldFieldId=D.numFieldId) ELSE D.numFieldId END, @destinationDomainId, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitRequired, CASE WHEN (SELECT COUNT(*) FROM #tempDycFieldMaster WHERE numOldFieldId=D.numFormFieldID) > 0 THEN (SELECT TOP 1  numFieldId FROM #tempDycFieldMaster WHERE numOldFieldId=D.numFormFieldID) ELSE D.numFieldId END, intSectionID, bitAllowGridColor
FROM DycFormField_Mapping AS D WHERE numDomainID=@sourceDomainId

DELETE FROM DynamicFormField_Validation WHERE  numDomainID>0 AND  numDomainId=@destinationDomainId
INSERT INTO DynamicFormField_Validation(numFormId, numFormFieldId, numDomainId, vcNewFormFieldName, bitIsRequired, bitIsNumeric, bitIsAlphaNumeric, bitIsEmail, bitIsLengthValidation, intMaxLength, intMinLength, bitFieldMessage, vcFieldMessage, vcToolTip)
SELECT numFormId, CASE WHEN (SELECT COUNT(*) FROM #tempDycFieldMaster WHERE numOldFieldId=D.numFormFieldId) > 0 THEN (SELECT TOP 1  numFieldId FROM #tempDycFieldMaster WHERE numOldFieldId=D.numFormFieldId) ELSE D.numFormFieldId END, @destinationDomainId, vcNewFormFieldName, bitIsRequired, bitIsNumeric, bitIsAlphaNumeric, bitIsEmail, bitIsLengthValidation, intMaxLength, intMinLength, bitFieldMessage, vcFieldMessage, vcToolTip FROM DynamicFormField_Validation AS D WHERE numDomainID=@sourceDomainId

--Dynamic Configuration For User
DELETE FROM BizFormWizardMasterConfiguration  WHERE  numDomainId=@destinationDomainId AND numGroupID=@destinationGroupId  AND bitCustom=0

INSERT INTO BizFormWizardMasterConfiguration
		(
			numFormID,
			numFieldID,
			intColumnNum,
			intRowNum,
			numDomainID,
			numGroupID,
			numRelCntType,
			tintPageType,
			bitCustom,
			bitGridConfiguration,
			numFormFieldGroupId
		)
SELECT  numFormID,
			CASE WHEN (SELECT COUNT(*) FROM #tempDycFieldMaster WHERE numOldFieldId=D.numFieldId) > 0 THEN (SELECT TOP 1  numFieldId FROM #tempDycFieldMaster WHERE numOldFieldId=D.numFieldId) ELSE D.numFieldId END,
			intColumnNum,
			intRowNum,
			@destinationDomainId,
			@destinationGroupId,
			numRelCntType,
			tintPageType,
			bitCustom,
			bitGridConfiguration,
			numFormFieldGroupId FROM BizFormWizardMasterConfiguration AS D WHERE numDomainID=@sourceDomainId AND numGroupID=@sourceGroupId  AND bitCustom=0



CREATE TABLE #tempDycUserMaster(
	numUserCntID NUMERIC(18,0),
	ID NUMERIC(18,0) IDENTITY(1,1)
)
INSERT INTO #tempDycUserMaster(numUserCntID) SELECT numUserDetailId FROM UserMaster WHERE numGroupID=@destinationGroupId AND numDomainID=@destinationDomainId
SET @i=1;
DECLARE @numUserCntID AS NUMERIC(18,0)=0
SELECT @totalListRecords = COUNT(*) FROM #tempDycUserMaster

WHILE(@i<=@totalListRecords)
BEGIN
	
	SELECT @numUserCntID = numUserCntID FROM #tempDycUserMaster WHERE ID=@i
	DELETE FROM DycFormConfigurationDetails WHERE numDomainId=@destinationDomainId AND numUserCntID=@numUserCntID  AND bitCustom=0
	INSERT INTO DycFormConfigurationDetails(numFormID,
				numFieldID,
				intColumnNum,
				intRowNum,
				numDomainID,
				numAuthGroupID,
				numRelCntType,
				tintPageType,
				bitCustom,
				numUserCntID,
				numFormFieldGroupId,
				bitDefaultByAdmin)
	SELECT  numFormID,
			numFieldID,
			intColumnNum,
			intRowNum,
			@destinationDomainId,
			@destinationGroupId,
			numRelCntType,
			tintPageType,
			bitCustom,
			@numUserCntID
			bitGridConfiguration,
			numFormFieldGroupId,
			1
FROM BizFormWizardMasterConfiguration AS D WHERE numDomainID=@destinationDomainId AND numGroupID=@destinationGroupId  AND bitCustom=0
	
	SET @i = @i+1
END


DROP TABLE #tempFormFieldGroupConfigurarionMaster
DROP TABLE #tempDycFieldMaster
DROP TABLE #tempDycUserMaster
END
-----------------------------------------------------------Global Setting Configuration----------------------------------------------------------------------
--ADD Custom Text Field to DycFieldMaster
IF(3 IN(SELECT Items FROM dbo.Split(@taskToPerform,',')))
BEGIN
CREATE TABLE #tempTabMaster(
	numOldTabId NUMERIC(18,0),
	numTabId NUMERIC(18,0),
	bitFixed BIT,
	ID NUMERIC(18,0) IDENTITY(1,1)
)
INSERT INTO #tempTabMaster(numOldTabId, numTabId,bitFixed)
SELECT numTabId,0,bitFixed FROM TabMaster WHERE numDomainID=@sourceDomainId OR bitFixed=1

SET @i=1;
DECLARE @numOldTabId AS NUMERIC(18,0)=0
DECLARE @numTabId AS NUMERIC(18,0)=0
DECLARE @bitTabFixed AS BIT=0
SELECT @totalListRecords = COUNT(*) FROM #tempTabMaster
SELECT * FROM #tempTabMaster
WHILE(@i<=@totalListRecords)
BEGIN
	SET @numTabId=0
	SET @numOldTabId=0
	SELECT @numOldTabId=numOldTabId,@bitTabFixed=bitFixed FROM #tempTabMaster WHERE ID=@i
	PRINT @numOldTabId
	IF(@bitTabFixed=1)
	BEGIN
		PRINT 'FIXED'
		SELECT @numTabId =@numOldTabId
	END
	ELSE IF EXISTS(SELECT numTabId FROM TabMaster WHERE numTabName=(SELECT TOP 1 numTabName FROM TabMaster WHERE numTabId=@numOldTabId) AND numDomainId=@destinationDomainId)
	BEGIN
		PRINT 'EXIST'
		SELECT @numTabId = numTabId FROM TabMaster WHERE numTabName=(SELECT TOP 1 numTabName FROM TabMaster WHERE numTabId=@numOldTabId) AND numDomainId=@destinationDomainId
	END
	ELSE
	BEGIN
		PRINT 'NEW'
		INSERT INTO TabMaster(numTabName, Remarks, tintTabType, vcURL, bitFixed, numDomainID, vcImage, vcAddURL, bitAddIsPopUp)
		SELECT numTabName, Remarks, tintTabType, vcURL, bitFixed, @destinationDomainId, vcImage, vcAddURL, bitAddIsPopUp FROM TabMaster WHERE numTabId=@numOldTabId
		SET @numTabId = SCOPE_IDENTITY()
	END
	UPDATE #tempTabMaster SET numTabId=@numTabId WHERE ID=@i
	SET @i=@i+1;
END
delete from GroupTabDetails where numGroupID=@destinationGroupId  
insert into GroupTabDetails(numGroupId,numTabId,bitallowed,numOrder,numRelationShip,tintType,[numProfileID])
SELECT @destinationGroupId, CASE WHEN ISNULL(T.numTabId,0)>0 THEN T.numTabId ELSE TB.numTabID END,TB.bitallowed,TB.numOrder,0,TB.tintType,0 FROM GroupTabDetails AS TB LEFT JOIN #tempTabMaster AS T ON TB.numTabId=T.numOldTabId 

DELETE FROM TreeNavigationAuthorization WHERE numDomainID=@destinationDomainId AND numGroupID=@destinationGroupId

INSERT INTO TreeNavigationAuthorization (numGroupID, numTabID, numPageNavID, bitVisible, numDomainID, tintType)
SELECT  @destinationGroupId, CASE WHEN ISNULL(TB.numTabId,0)>0 THEN TB.numTabId ELSE T.numTabID END, numPageNavID, bitVisible, @destinationDomainId, tintType FROM TreeNavigationAuthorization AS T LEFT JOIN #tempTabMaster 
AS TB ON T.numTabID=TB.numOldTabId WHERE T.numDomainID=@sourceDomainId AND T.numGroupID=@sourceGroupId
DROP TABLE #tempTabMaster
END

COMMIT;  
END TRY 
BEGIN CATCH 
	SELECT ERROR_MESSAGE()
	ROLLBACK TRANSACTION
END CATCH
END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Project_GetTasks')
DROP PROCEDURE USP_Project_GetTasks
GO
CREATE PROCEDURE [dbo].[USP_Project_GetTasks]
(
	@numDomainID NUMERIC(18,0)
	,@numProId NUMERIC(18,0)
	,@numStageDetailsId NUMERIC(18,0)
	,@ClientTimeZoneOffset INT
	,@tintMode TINYINT
)
AS 
BEGIN
	IF @tintMode = 1
	BEGIN
		DECLARE @numQtyToBuild FLOAT
		DECLARE @dtPlannedStartDate DATETIME
		SELECT 
			@numQtyToBuild=1
			,@dtPlannedStartDate=ISNULL(ProjectsMaster.dtmStartDate,ProjectsMaster.bintCreatedDate) 
		FROM 
			ProjectsMaster 
		WHERE
			ProjectsMaster.numDomainID=@numDomainID 
			AND ProjectsMaster.numProId=@numProId

		DECLARE @TempTaskAssignee TABLE
		(
			numAssignedTo NUMERIC(18,0)
			,dtLastTaskCompletionTime DATETIME
		)

		DECLARE @TempTasks TABLE
		(
			ID INT IDENTITY(1,1)
			,numTaskID NUMERIC(18,0)
			,numTaskTimeInMinutes NUMERIC(18,0)
			,numTaskAssignee NUMERIC(18,0)
			,intTaskType INT --1:Parallel, 2:Sequential
			,dtPlannedStartDate DATETIME
		)

		INSERT INTO @TempTasks
		(
			numTaskID
			,numTaskTimeInMinutes
			,intTaskType
			,numTaskAssignee
		)
		SELECT 
			SPDT.numTaskID
			,((ISNULL(SPDT.numHours,0) * 60) + ISNULL(SPDT.numMinutes,0)) * @numQtyToBuild
			,ISNULL(SPDT.intTaskType,1)
			,SPDT.numAssignTo
		FROM 
			StagePercentageDetailsTask SPDT
		INNER JOIN
			ProjectsMaster PO
		ON
			SPDT.numProjectId = PO.numProId
		WHERE 
			SPDT.numDomainId=@numDomainID
			AND SPDT.numProjectId = @numProId

		UPDATE @TempTasks SET dtPlannedStartDate=@dtPlannedStartDate

		INSERT INTO @TempTaskAssignee
		(
			numAssignedTo
		)
		SELECT DISTINCT
			numTaskAssignee
		FROM
			@TempTasks

		DECLARE @i INT = 1
		DECLARE @iCount INT 	
		SELECT @iCount = COUNT(*) FROM @TempTasks

		DECLARE @numTaskAssignee NUMERIC(18,0)
		DECLARE @intTaskType INT
		DECLARE @numWorkScheduleID NUMERIC(18,0)
		DECLARE @numTempUserCntID NUMERIC(18,0)
		DECLARE @dtStartDate DATETIME
		DECLARE @numTotalTaskInMinutes NUMERIC(18,0)
		DECLARE @numProductiveTimeInMinutes NUMERIC(18,0)
		DECLARE @tmStartOfDay TIME(7)
		DECLARE @numTimeLeftForDay NUMERIC(18,0)
		DECLARE @vcWorkDays VARCHAR(20)
		DECLARE @bitParallelStartSet BIT = 0

		WHILE @i <= @iCount
		BEGIN
			SELECT
				@numTaskAssignee=numTaskAssignee
				,@numTotalTaskInMinutes=numTaskTimeInMinutes
				,@intTaskType = intTaskType
			FROM
				@TempTasks 
			WHERE
				ID=@i

			-- GET TASK COMPLETION TIME WHICH WILL BE START DATETIME FOR NEXT TASK
			SELECT
				@numWorkScheduleID = WS.ID
				,@numProductiveTimeInMinutes=(ISNULL(WS.numProductiveHours,0) * 60) + ISNULL(WS.numProductiveMinutes,0)
				,@tmStartOfDay = tmStartOfDay
				,@vcWorkDays=vcWorkDays
				,@dtStartDate = DATEADD(MINUTE,@ClientTimeZoneOffset,CONVERT(DATETIME,CONVERT(VARCHAR(8),DATEADD(MINUTE,-@ClientTimeZoneOffset,@dtPlannedStartDate),112)) + tmStartOfDay)
			FROM
				WorkSchedule WS
			INNER JOIN
				UserMaster
			ON
				WS.numUserCntID = UserMaster.numUserDetailId
			WHERE 
				WS.numUserCntID = @numTaskAssignee

			IF @intTaskType=1 AND EXISTS (SELECT numAssignedTo FROM @TempTaskAssignee WHERE numAssignedTo = @numTaskAssignee AND dtLastTaskCompletionTime IS NOT NULL)
			BEGIN
				SELECT @dtStartDate = dtLastTaskCompletionTime FROM @TempTaskAssignee WHERE numAssignedTo = @numTaskAssignee
			END
			ELSE IF EXISTS (SELECT numAssignedTo FROM @TempTaskAssignee WHERE numAssignedTo = @numTaskAssignee AND dtLastTaskCompletionTime IS NOT NULL)
			BEGIN
				SELECT @dtStartDate = dtLastTaskCompletionTime FROM @TempTaskAssignee WHERE numAssignedTo = @numTaskAssignee
			END

			UPDATE @TempTasks SET dtPlannedStartDate=@dtStartDate WHERE ID=@i 

			IF @numProductiveTimeInMinutes > 0 AND @numTotalTaskInMinutes > 0
			BEGIN
				WHILE @numTotalTaskInMinutes > 0
				BEGIN
					-- IF ITS WORKING DAY AND ALSO NOT HOLIDAY THEN PROCEED OR INCRESE DATE TO NEXT DAY
					IF DATEPART(WEEKDAY,@dtStartDate) IN (SELECT Id FROM dbo.SplitIDs(@vcWorkDays,',')) AND NOT EXISTS (SELECT ID FROM WorkScheduleDaysOff WHERE numWorkScheduleID=@numWorkScheduleID AND CAST(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,@dtStartDate) AS DATE) BETWEEN dtDayOffFrom AND dtDayOffTo)
					BEGIN
						IF CAST(@dtStartDate AS DATE) = CAST(GETUTCDATE() AS DATE)
						BEGIN
							-- CHECK TIME LEFT FOR DAY BASED
							SET @numTimeLeftForDay = DATEDIFF(MINUTE,@dtStartDate,DATEADD(MINUTE,@numProductiveTimeInMinutes,DATEADD(MINUTE,@ClientTimeZoneOffset,CONVERT(DATETIME,CONVERT(VARCHAR(8),DATEADD(MINUTE,-@ClientTimeZoneOffset,@dtStartDate),112)) + @tmStartOfDay)))
						END
						ELSE
						BEGIN
							SET @numTimeLeftForDay = @numProductiveTimeInMinutes
						END

						IF @numTimeLeftForDay > 0
						BEGIN
							IF @numTimeLeftForDay > @numTotalTaskInMinutes
							BEGIN
								SET @dtStartDate = DATEADD(MINUTE,@numTotalTaskInMinutes,@dtStartDate)
							END
							ELSE
							BEGIN
								SET @dtStartDate = DATEADD(MINUTE,@ClientTimeZoneOffset,CONVERT(DATETIME,CONVERT(VARCHAR(8),DATEADD(MINUTE,-@ClientTimeZoneOffset,DATEADD(DAY,1,@dtStartDate)),112)) + @tmStartOfDay)
							END

							SET @numTotalTaskInMinutes = @numTotalTaskInMinutes - @numTimeLeftForDay
						END
						ELSE
						BEGIN
							SET @dtStartDate = DATEADD(MINUTE,@ClientTimeZoneOffset,CONVERT(DATETIME,CONVERT(VARCHAR(8),DATEADD(MINUTE,-@ClientTimeZoneOffset,DATEADD(DAY,1,@dtStartDate)),112)) + @tmStartOfDay)
						END
					END
					ELSE
					BEGIN
						SET @dtStartDate = DATEADD(MINUTE,@ClientTimeZoneOffset,CONVERT(DATETIME,CONVERT(VARCHAR(8),DATEADD(MINUTE,-@ClientTimeZoneOffset,DATEADD(DAY,1,@dtStartDate)),112)) + @tmStartOfDay)					
					END				
				END
			END	

			UPDATE @TempTaskAssignee SET dtLastTaskCompletionTime=@dtStartDate WHERE numAssignedTo=@numTaskAssignee

			SET @i = @i + 1
		END

		SELECT	
			SPDT.numTaskId
			,SPDT.numReferenceTaskId
			,SPDT.vcTaskName
			,A.vcEmail
			,SPDT.numAssignTo AS numAssignedTo 
			,ISNULL(SPDT.bitTimeAddedToContract,0) AS bitTimeAddedToContract
			,dbo.fn_GetContactName(SPDT.numAssignTo) vcAssignedTo
			,ISNULL(L.vcData,'-') AS vcWorkStation
			,ISNULL((SELECT SUM(numProcessedQty) FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId),0) AS numProcessedQty
			,1 - ISNULL((SELECT SUM(numProcessedQty) FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId),0) AS numRemainingQty
			,CASE ISNULL((SELECT TOP 1 tintAction FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId ORDER BY dtActionTime DESC,ID DESC),0) 
				WHEN 4 THEN '<img src="../images/comflag.png" />'
				WHEN 3 THEN CONCAT('<ul class="list-inline" style="margin-bottom:0px;"><li style="vertical-align:middle;padding-left:0px;padding-right:0px;"><button class="btn btn-xs btn-info" onclick="return ShowTaskTimeLogWindow(',SPDT.numTaskId,');"><i class="fa fa-clock-o"></i></button></li><li style="vertical-align:middle;padding-right:0px;"><button class="btn btn-xs btn-flat btn-warning" onclick="return ShowTaskPausedWindow(this,',SPDT.numTaskId,');">Pause</button></li><li style="vertical-align:middle;padding-right:0px;"><button class="btn btn-xs btn-flat btn-danger btn-task-finish" onclick="return TaskFinished(this,',SPDT.numTaskId,',4);">Finish</button></li><li style="vertical-align:middle;padding-right:0px;"><label class="taskTimerInitial"></label></li></ul>')
				WHEN 2 THEN CONCAT('<ul class="list-inline" style="margin-bottom:0px;"><li style="vertical-align:middle;padding-left:0px;padding-right:0px;"><button class="btn btn-xs btn-info" onclick="return ShowTaskTimeLogWindow(',SPDT.numTaskId,');"><i class="fa fa-clock-o"></i></button></li><li style="vertical-align:middle;padding-right:0px;"><button class="btn btn-xs btn-flat bg-purple" onclick="return TaskStarted(this,',SPDT.numTaskId,',1);">Resume</button></li><li style="vertical-align:middle;padding-right:0px;"><button class="btn btn-xs btn-flat btn-danger btn-task-finish" onclick="return TaskFinished(this,',SPDT.numTaskId,',4);">Finish</button></li></ul>')
				WHEN 1 THEN CONCAT('<ul class="list-inline" style="margin-bottom:0px;"><li style="vertical-align:middle;padding-left:0px;padding-right:0px;"><button class="btn btn-xs btn-info" onclick="return ShowTaskTimeLogWindow(',SPDT.numTaskId,');"><i class="fa fa-clock-o"></i></button></li><li style="vertical-align:middle;padding-right:0px;"><button class="btn btn-xs btn-flat btn-warning" onclick="return ShowTaskPausedWindow(this,',SPDT.numTaskId,');">Pause</button></li><li style="vertical-align:middle;padding-right:0px;"><button class="btn btn-xs btn-flat btn-danger btn-task-finish" onclick="return TaskFinished(this,',SPDT.numTaskId,',4);">Finish</button></li><li style="vertical-align:middle;padding-right:0px;"><label class="taskTimerInitial"></label></li></ul>')
				ELSE CONCAT('<ul class="list-inline" style="margin-bottom:0px;"><li style="vertical-align:middle;padding-left:0px;padding-right:0px;"><button class="btn btn-xs btn-info" onclick="return ShowTaskTimeLogWindow(',SPDT.numTaskId,');"><i class="fa fa-clock-o"></i></button></li><li style="vertical-align:middle;padding-right:0px;"><button class="btn btn-xs btn-flat btn-success btn-task-start" onclick="return TaskStarted(this,',SPDT.numTaskId,',0);">Start</button></li><li style="vertical-align:middle;padding-right:0px;"><button class="btn btn-xs btn-flat btn-danger btn-task-finish" onclick="return TaskFinished(this,',SPDT.numTaskId,',4);">Finish</button></li></ul>')
			END  AS vcTaskControls
			,FORMAT((((ISNULL(SPDT.numHours,0) * 60) + ISNULL(SPDT.numMinutes,0)) * @numQtyToBuild) / 60,'00') + ':' + FORMAT((((ISNULL(SPDT.numHours,0) * 60) + ISNULL(SPDT.numMinutes,0)) * CAST(@numQtyToBuild AS DECIMAL)) % 60.0,'00')  vcEstimatedTaskTime
			,(((ISNULL(SPDT.numHours,0) * 60) + ISNULL(SPDT.numMinutes,0)) * @numQtyToBuild) numTaskEstimationInMinutes
			,(CASE ISNULL((SELECT TOP 1 tintAction FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=SPDT.numTaskId ORDER BY SPDTTL.dtActionTime DESC,SPDTTL.ID DESC),0) 
				WHEN 4 THEN 0
				WHEN 3 THEN dbo.GetTimeSpendOnTaskByProject(SPDT.numDomainID,SPDT.numTaskId,2)
				WHEN 2 THEN dbo.GetTimeSpendOnTaskByProject(SPDT.numDomainID,SPDT.numTaskId,2)
				WHEN 1 THEN 0
			END) numTimeSpentInMinutes
			,(CASE ISNULL((SELECT TOP 1 tintAction FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId ORDER BY dtActionTime DESC,ID DESC),0) 
				WHEN 4 THEN NULL
				WHEN 3 THEN DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,(SELECT TOP 1 dtActionTime FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=SPDT.numTaskId ORDER BY SPDTTL.dtActionTime DESC,SPDTTL.ID DESC))
				WHEN 2 THEN NULL
				WHEN 1 THEN DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,(SELECT TOP 1 dtActionTime FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=SPDT.numTaskId ORDER BY SPDTTL.dtActionTime DESC,SPDTTL.ID DESC))
			END) dtLastStartDate
			,(CASE WHEN dbo.GetTimeSpendOnTaskByProject(SPDT.numDomainID,SPDT.numTaskId,0) = 'Invalid time sequence' THEN '00:00' ELSE dbo.GetTimeSpendOnTaskByProject(SPDT.numDomainID,SPDT.numTaskId,0) END) vcActualTaskTime
			,CASE WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId AND tintAction=4)
				THEN dbo.GetTimeSpendOnTaskByProject(SPDT.numDomainID,SPDT.numTaskId,1)
				ELSE ''
			END vcActualTaskTimeHtml
			,CASE 
				WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId AND tintAction=1)
				THEN CONCAT('<span>',dbo.FormatedDateTimeFromDate(DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,(SELECT dtActionTime FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId AND tintAction=1)),@numDomainID),'</span>')
				ELSE CONCAT('<i style="color:#a6a6a6">',dbo.FormatedDateTimeFromDate((CASE
										WHEN TT.dtPlannedStartDate IS NULL
										THEN DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,ISNULL(PO.dtmStartDate,PO.bintCreatedDate))
										ELSE DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,TT.dtPlannedStartDate)
									END),@numDomainID),' (planned)</i>')
			END dtPlannedStartDate
			,CASE WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId AND tintAction=1)
				THEN DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,(SELECT dtActionTime FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId AND tintAction=1))
				ELSE ''
			END AS dtStartDate
			,CASE WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId AND tintAction=4)
				THEN DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,(SELECT dtActionTime FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId AND tintAction=4))
				ELSE ''
			END AS dtFinishDate
			,CASE WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId AND tintAction=4)
				THEN dbo.FormatedDateTimeFromDate(DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,(SELECT dtActionTime FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId AND tintAction=4)),@numDomainID)
				ELSE ''
			END AS vcFinishDate
			,CASE WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId AND tintAction=4)
				THEN 1
				ELSE 0
			END AS bitTaskCompleted
			,(SELECT COUNT(*) FROM TopicMaster WHERE intRecordType=4 AND numRecordId=SPDT.numTaskId) AS TopicCount
			,(CASE 
				WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskNotes SPDTN WHERE SPDTN.numTaskID=SPDT.numTaskId) 
				THEN CONCAT('<i class="fa fa-file-text-o" style="font-size: 23px;" onclick="return OpenTaskNotes(',SPDT.numTaskId,')"></i>') 
				ELSE CONCAT('<i class="fa fa-file-o" style="font-size: 23px;" onclick="return OpenTaskNotes(',SPDT.numTaskId,')"></i>') 
			END) vcNotesLink
		FROM 
			StagePercentageDetailsTask SPDT
		INNER JOIN
			ProjectsMaster PO
		ON
			SPDT.numProjectId = PO.numProId
		LEFT JOIN
			@TempTasks TT
		ON
			SPDT.numTaskId = TT.numTaskID
		LEFT JOIN
			AdditionalContactsInformation AS A
		ON
			SPDT.numAssignTo=A.numContactId
		LEFT JOIN
			ListDetails AS L
		ON
			A.numTeam=L.numListItemID
		WHERE
			SPDT.numStageDetailsId = @numStageDetailsId
			AND SPDT.numProjectId = @numProId
	END
	ELSE IF @tintMode = 2
	BEGIN
		SELECT	
			SPDT.numTaskId
			,SPDT.numReferenceTaskId
			,SPDT.numStageDetailsId
			,SPDT.vcTaskName
			,ISNULL(SPDT.numHours,0) numHours
			,ISNULL(SPDT.numMinutes,0) numMinutes
			,SPDT.numAssignTo
			,SPDT.numAssignTo AS numAssignedTo 
			,ISNULL(SPDT.bitTimeAddedToContract,0) AS bitTimeAddedToContract
			,ADC.numTeam
			,CONCAT(FORMAT(ISNULL(SPDT.numHours,0),'00'),':',FORMAT(ISNULL(SPDT.numMinutes,0),'00')) vcEstimatedTaskTime
			,(CASE WHEN dbo.GetTimeSpendOnTaskByProject(SPDT.numDomainID,SPDT.numTaskId,0) = 'Invalid time sequence' THEN '00:00' ELSE dbo.GetTimeSpendOnTaskByProject(SPDT.numDomainID,SPDT.numTaskId,0) END) vcActualTaskTime
			,CASE WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId AND tintAction=1)
				THEN DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,(SELECT dtActionTime FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId AND tintAction=1))
				ELSE ''
			END AS dtStartDate
			,CASE WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId AND tintAction=4)
				THEN DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,(SELECT dtActionTime FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId AND tintAction=4))
				ELSE ''
			END AS dtFinishDate
			,CASE WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId)
				THEN 1
				ELSE 0
			END bitTaskStarted,
			(SELECT COUNT(*) FROM TopicMaster WHERE intRecordType=4 AND numRecordId=SPDT.numTaskId) AS TopicCount
		FROM 
			StagePercentageDetailsTask SPDT
		INNER JOIN
			ProjectsMaster PO
		ON
			SPDT.numProjectId = PO.numProId
		LEFT JOIN
			AdditionalContactsInformation ADC
		ON
			SPDT.numAssignTo = ADC.numContactId
		LEFT JOIN
			@TempTasks TT
		ON
			SPDT.numTaskId = TT.numTaskID
		WHERE
			SPDT.numStageDetailsId = @numStageDetailsId
			AND SPDT.numProjectId = @numProId
	END
END
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_TicklerActItemsV2')
DROP PROCEDURE USP_TicklerActItemsV2
GO
CREATE Proc [dbo].[USP_TicklerActItemsV2]       
@numUserCntID as numeric(9)=null,                                                                        
@numDomainID as numeric(9)=null,                                                                        
@startDate as datetime,                                                                        
@endDate as datetime,                                                                    
@ClientTimeZoneOffset Int,  --Added by Debasish to enable calculation of date according to client machine             
@bitFilterRecord bit=0,
@columnName varchar(50), 
@columnSortOrder varchar(50),
@vcBProcessValue varchar(500),
@RegularSearchCriteria VARCHAR(MAX)='',
@CustomSearchCriteria VARCHAR(MAX)='', 
@OppStatus int,
@CurrentPage int,                                                              
@PageSize int
AS
BEGIN
	SET @endDate = DATEADD(MILLISECOND,-2,DATEADD(DAY,1,@endDate))

--SET @PageSize=10
DECLARE @tintActionItemsViewRights TINYINT  = 3
DECLARE @bitREQPOApproval AS BIT
DECLARE @bitARInvoiceDue AS BIT
DECLARE @bitAPBillsDue AS BIT
DECLARE @vchARInvoiceDue AS VARCHAR(500)
DECLARE @vchAPBillsDue AS VARCHAR(500)
DECLARE @decReqPOMinValue AS DECIMAL(18,2)
DECLARE @vchREQPOApprovalEmp AS VARCHAR(500)=''
DECLARE @numDomainDivisionID NUMERIC(18,0)
DECLARE @dynamicPartQuery AS NVARCHAR(MAX)=''
DECLARE @ActivityRegularSearch AS VARCHAR(MAX)=''
DECLARE @OtherRegularSearch AS VARCHAR(MAX)=''
DECLARE @IndivRecordPaging AS NVARCHAR(MAX)=''
--SET @IndivRecordPaging=' '
DECLARE @TotalRecordCount INT =0
SET @IndivRecordPaging=  ' ORDER BY dtDueDate DESC OFFSET '+CAST(((@CurrentPage - 1 ) * @PageSize) AS VARCHAR) +' ROWS FETCH NEXT '+CAST(@PageSize AS VARCHAR)+' ROWS ONLY'

SELECT TOP 1 
	@bitREQPOApproval=bitREQPOApproval,
	@vchREQPOApprovalEmp=vchREQPOApprovalEmp,
	@decReqPOMinValue=ISNULL(decReqPOMinValue,0),
	@bitARInvoiceDue=bitARInvoiceDue,
	@bitAPBillsDue = bitAPBillsDue,
	@vchARInvoiceDue = vchARInvoiceDue,
	@vchAPBillsDue = vchAPBillsDue,
	@numDomainDivisionID=numDivisionID
FROM 
	Domain 
WHERE 
	numDomainId=@numDomainID

IF EXISTS (SELECT * FROM GroupAuthorization WHERE numDomainID=@numDomainID AND numModuleID=1 AND numPageID=2 AND numGroupID IN (SELECT ISNULL(numGroupID,0) FROM UserMaster WHERE numDomainID=@numDomainID AND numUserDetailId=@numUserCntID))
BEGIN
	SELECT @tintActionItemsViewRights=ISNULL(intViewAllowed,0) FROM GroupAuthorization WHERE numDomainID=@numDomainID AND numModuleID=1 AND numPageID=2 AND numGroupID IN (SELECT ISNULL(numGroupID,0) FROM UserMaster WHERE numDomainID=@numDomainID AND numUserDetailId=@numUserCntID)
END
--12 - Communication task
--1 - Case Task
--2 - Project Task
--3 - Opportunity Task
--4 - Sales Order Task
--5 - Work Order Management
--6 - Work Center Task
--7 - Requisition Approval
--8 - PO Approval
--9 - Price Margin Approval
--10 - A/R Invoice Due
--11 - A/P Bill Due

DECLARE @dynamicQuery AS NVARCHAR(MAX)=''
DECLARE @dynamicQueryCount AS NVARCHAR(MAX)=''

Create table #tempRecords (RecordCount NUMERIC(9),RecordId NUMERIC(9),numTaskId numeric(9),IsTaskClosed bit,TaskTypeName VARCHAR(100),TaskType NUMERIC(9),
OrigDescription NVARCHAR(MAX),Description NVARCHAR(MAX),TotalProgress INT,numAssignToId NUMERIC(9),
Priority NVARCHAR(MAX),PriorityId  NUMERIC(9),ActivityId NUMERIC(9),OrgName NVARCHAR(MAX),CompanyRating NVARCHAR(MAX),
numContactID numeric(9),numDivisionID numeric(9),tintCRMType tinyint,
Id numeric(9),bitTask varchar(100),Startdate datetime,EndTime datetime,
itemDesc text,[Name] varchar(200),vcFirstName varchar(100),vcLastName varchar(100),numPhone varchar(15),numPhoneExtension varchar(7),
[Phone] varchar(30),vcCompanyName varchar(100),vcProfile VARCHAR(100), vcEmail varchar(50),Task varchar(100),Activity varchar(MAX),
Status varchar(100),numCreatedBy numeric(9),numRecOwner VARCHAR(500), numModifiedBy VARCHAR(500), numOrgTerId NUMERIC(18,0), numTerId VARCHAR(200),numAssignedTo varchar(1000),numAssignedBy varchar(50),
 caseid numeric(9),vcCasenumber varchar(50),casetimeId numeric(9),caseExpId numeric(9),type int,itemid varchar(50),
changekey varchar(50),dtDueDate DATETIME, DueDate varchar(50),bitFollowUpAnyTime bit,numNoOfEmployeesId varchar(50),vcComPhone varchar(50),numFollowUpStatus varchar(200),dtLastFollowUp varchar(500),vcLastSalesOrderDate VARCHAR(100),monDealAmount DECIMAL(20,5), vcPerformance VARCHAR(100),vcColorScheme VARCHAR(50),
Slp_Name varchar(100),intTotalProgress INT,vcPoppName varchar(100), numOppId NUMERIC(18,0),numBusinessProcessID numeric(18,0),
numCompanyRating varchar(500),numStatusID varchar(500),numCampaignID varchar(500),[Action-Item Participants] NVARCHAR(MAX),HtmlLink varchar(500),
Location nvarchar(150),OrignalDescription text)                                                         
 

declare @strSql as varchar(MAX)                                                            
declare @strSql1 as varchar(MAX)                         
declare @strSql2 as varchar(MAX)                                                            
declare @vcCustomColumnName AS VARCHAR(8000)
declare @vcnullCustomColumnName AS VARCHAR(8000)
   -------Change Row Color-------
Declare @vcCSOrigDbCOlumnName as varchar(50),@vcCSLookBackTableName as varchar(50),@vcCSAssociatedControlType as varchar(50)
SET @vcCSOrigDbCOlumnName=''
SET @vcCSLookBackTableName=''

declare @tintOrder as tinyint                                                
declare @vcFieldName as varchar(50)                                                
declare @vcListItemType as varchar(3)                                           
declare @vcAssociatedControlType varchar(20)                                                
declare @numListID AS numeric(9)                                                
declare @vcDbColumnName varchar(20)                    
declare @WhereCondition varchar(2000)                     
declare @vcLookBackTableName varchar(2000)              
Declare @bitCustomField as BIT
DECLARE @bitAllowEdit AS CHAR(1)                 
Declare @numFieldId as numeric  
DECLARE @bitAllowSorting AS CHAR(1)   
declare @vcColumnName AS VARCHAR(500)       
declare @vcCustomColumnNameOnly AS VARCHAR(500)       


IF(@RegularSearchCriteria='')
BEGIN
	SET @RegularSearchCriteria =@RegularSearchCriteria + ' 1=1'
END
if(@OppStatus=0)
BEGIN
SET @RegularSearchCriteria =@RegularSearchCriteria + ' AND IsTaskClosed = 0'
END
if(@OppStatus=1)
BEGIN
SET @RegularSearchCriteria = @RegularSearchCriteria + ' AND IsTaskClosed = 1'
END
 SET @RegularSearchCriteria  = REPLACE(@RegularSearchCriteria,'TaskTypeName','TaskType')

 SET @ActivityRegularSearch = ''
 SET @OtherRegularSearch = ''

 SET @ActivityRegularSearch = ISNULL((SELECT STUFF( 
(
	SELECT  'AND ' + [Items] FROM 
	(
	  SELECT [Items] FROM SplitByString(@RegularSearchCriteria,'AND') WHERE Items<>'' AND 1=
	  (CASE WHEN Items LIKE '%IsTaskClosed%' THEN 0
	 WHEN Items LIKE '%TaskType%'  THEN 0
	 WHEN  Items LIKE '%numAssignToId%'  THEN 0
	 WHEN  Items LIKE '%PriorityId%'  THEN 0
	 WHEN  Items LIKE '%ActivityId%'  THEN 0
	 WHEN  Items LIKE '%Description%'   THEN 0
	 WHEN  Items LIKE '%OrgName%'   THEN 0 ELSE 1 END)
	) AS T FOR XML PATH('')
)
,1,3,'') AS [Items]),'')
SET @ActivityRegularSearch = REPLACE(@ActivityRegularSearch,'&gt;','>')
SET @ActivityRegularSearch = REPLACE(@ActivityRegularSearch,'&lt;','<')
PRINT @ActivityRegularSearch
 SET @OtherRegularSearch = ISNULL((SELECT STUFF( 
(
	SELECT  'AND ' + [Items] FROM 
	(
	  SELECT [Items] FROM SplitByString(@RegularSearchCriteria,'AND') WHERE Items<>'' AND 1=
	  (CASE WHEN Items LIKE '%IsTaskClosed%' THEN 1
	 WHEN Items LIKE '%TaskType%'  THEN 1
	 WHEN  Items LIKE '%numAssignToId%'  THEN 1
	 WHEN  Items LIKE '%PriorityId%'  THEN 1
	 WHEN  Items LIKE '%ActivityId%'  THEN 1
	 WHEN  Items LIKE '%Description%'   THEN 1
	 WHEN  Items LIKE '%OrgName%'   THEN 1 ELSE 0 END)
	) AS T FOR XML PATH('')
)
,1,3,'') AS [Items]),'')
SET @OtherRegularSearch = ISNULL(REPLACE(@OtherRegularSearch,'T.',''),'')
SET @OtherRegularSearch = REPLACE(@OtherRegularSearch,'&gt;','>')
SET @OtherRegularSearch = REPLACE(@OtherRegularSearch,'&lt;','<')
IF(LEN(@OtherRegularSearch)=0)
BEGIN
	SET @OtherRegularSearch = ' 1=1 '
END
PRINT @OtherRegularSearch
Declare @ListRelID as numeric(9)             
set @tintOrder=0   

CREATE TABLE #tempForm 
	(
		tintOrder INT,vcDbColumnName nvarchar(50),vcFieldDataType nvarchar(50),vcOrigDbColumnName nvarchar(50),bitAllowFiltering BIT,vcFieldName nvarchar(50)
		,vcAssociatedControlType nvarchar(50),vcListItemType char(3),numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,numFieldId NUMERIC
		,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,bitIsAlphaNumeric BIT,bitIsNumeric BIT,bitIsLengthValidation BIT,intMaxLength int
		,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),ListRelID numeric(9),intColumnWidth NUMERIC(18,2)
	)
                
	--Custom Fields related to Organization
	INSERT INTO 
		#tempForm
	SELECT  
		tintRow + 1 AS tintOrder,vcDbColumnName,'',vcDbColumnName,1,vcFieldName,vcAssociatedControlType,'' as vcListItemType,numListID,'' as vcLookBackTableName                                                 
		,bitCustom,numFieldId,bitAllowSorting,0,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength
		,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth
	FROM 
		View_DynamicCustomColumns
	WHERE 
		numFormId=43 
		AND numUserCntID=@numUserCntID 
		AND numDomainID=@numDomainID 
		AND tintPageType=1
		AND ISNULL(bitCustom,0)=1
 
	SELECT TOP 1 
		@tintOrder = tintOrder + 1
		,@vcDbColumnName=vcDbColumnName
		,@vcFieldName=vcFieldName
		,@vcAssociatedControlType=vcAssociatedControlType
		,@vcListItemType=vcListItemType
		,@numListID=numListID
		,@vcLookBackTableName=vcLookBackTableName                                               
		,@bitCustomField=bitCustomField
		,@numFieldId=numFieldId
		,@bitAllowSorting=bitAllowSorting
		,@bitAllowEdit=0
		,@ListRelID=ListRelID
	FROM 
		#tempForm 
	ORDER BY 
		tintOrder ASC     
  
	SET @vcCustomColumnName=''
	SET @vcnullCustomColumnName=''
	SET @vcCustomColumnNameOnly = ''
	WHILE @tintOrder>0      
	BEGIN  
		SET @vcColumnName= @vcDbColumnName
		SET @strSql = 'ALTER TABLE #tempRecords ADD [' + @vcColumnName + '] NVARCHAR(MAX)'
		SET @vcCustomColumnNameOnly = @vcCustomColumnNameOnly+','+@vcColumnName
		EXEC(@strSql)
		
		IF @bitCustomField = 1
		BEGIN 
			IF @vcAssociatedControlType = 'TextBox' OR @vcAssociatedControlType = 'TextArea'            
			BEGIN            
				SET @vcCustomColumnName= @vcCustomColumnName+', ISNULL((SELECT TOP 1 CFW.Fld_Value FROM CFW_FLD_Values AS CFW WHERE CFW.Fld_ID='+CAST(@numFieldId AS VARCHAR(50))+' AND CFW.RecId=Div.numDivisionID),'''') ['+ @vcColumnName +']'
				SET @vcnullCustomColumnName= @vcnullCustomColumnName+','''' AS  ['+ @vcColumnName +']'
			END  
			ELSE IF @vcAssociatedControlType = 'CheckBox'         
			BEGIN            
				SET @vcCustomColumnName= @vcCustomColumnName+',ISNULL((SELECT TOP 1 CFW.Fld_Value FROM CFW_FLD_Values AS CFW WHERE CFW.Fld_ID='+CAST(@numFieldId AS VARCHAR(50))+' AND CFW.RecId=Div.numDivisionID),0)  ['+ @vcColumnName + ']'
				SET @vcnullCustomColumnName= @vcnullCustomColumnName+',0 AS ['+ @vcColumnName + ']'
			END            
			ELSE IF @vcAssociatedControlType = 'DateField'        
			BEGIN            
				SET @vcCustomColumnName= @vcCustomColumnName+',dbo.FormatedDateFromDate((SELECT TOP 1 CFW.Fld_Value FROM CFW_FLD_Values AS CFW WHERE CFW.Fld_ID='+CAST(@numFieldId AS VARCHAR(50))+' AND CFW.RecId=Div.numDivisionID),'+convert(varchar(10),@numDomainId)+')  ['+ @vcColumnName +']'              
				SET @vcnullCustomColumnName= @vcnullCustomColumnName+','''' AS  ['+ @vcColumnName +']'
			END            
			ELSE IF @vcAssociatedControlType = 'SelectBox'           
			BEGIN            
				SET @vcCustomColumnName=@vcCustomColumnName+',(SELECT TOP 1 ISNULL(L.vcData,'''') FROM CFW_FLD_Values CFW left Join ListDetails L on L.numListItemID=CFW.Fld_Value WHERE  CFW.Fld_ID='+CAST(@numFieldId AS VARCHAR(50))+' AND CFW.RecId=Div.numDivisionID)'+' ['+ @vcColumnName +']'                                                         
				SET @vcnullCustomColumnName=@vcnullCustomColumnName+','''''+'AS  ['+ @vcColumnName +']'                                                         
			END
			ELSE IF @vcAssociatedControlType = 'CheckBoxList'
			BEGIN
				SET @vcCustomColumnName=@vcCustomColumnName+',(SELECT STUFF((SELECT CONCAT('', '', vcData) FROM ListDetails WHERE numListID= ' + CAST(@numListID AS varchar) + ' AND numlistitemid IN (SELECT ISNULL(Id,0) FROM dbo.SplitIDs(ISNULL((SELECT CFWInner.Fld_Value FROM CFW_FLD_Values CFWInner WHERE CFWInner.Fld_ID='+CAST(@numFieldId AS VARCHAR(50))+' AND CFWInner.RecId=Div.numDivisionID),''''),'','')) FOR XML PATH('''')), 1, 1, ''''))'+' ['+ @vcColumnName +']'
				SET @vcnullCustomColumnName=@vcnullCustomColumnName+','''''+'AS  ['+ @vcColumnName +']'
			END
			ELSE
			BEGIN
				SET @vcCustomColumnName= CONCAT(@vcCustomColumnName,',(SELECT dbo.fn_GetCustFldStringValue(',@numFieldId,',CFW.RecId,CFW.Fld_Value',') FROM CFW_FLD_Values CFW left Join ListDetails L on L.numListItemID=CFW.Fld_Value WHERE  CFW.Fld_ID=',@numFieldId,' AND CFW.RecId=Div.numDivisionID)',' [', @vcColumnName ,']')
				SET @vcnullCustomColumnName=@vcnullCustomColumnName+','''''+'AS  ['+ @vcColumnName +']' 
			END
		END
		  
		SELECT TOP 1 
			@tintOrder=tintOrder + 1
			,@vcDbColumnName=vcDbColumnName
			,@vcFieldName=vcFieldName
			,@vcAssociatedControlType=vcAssociatedControlType
			,@vcListItemType=vcListItemType
			,@numListID=numListID
			,@vcLookBackTableName=vcLookBackTableName                                               
			,@bitCustomField=bitCustomField
			,@numFieldId=numFieldId
			,@bitAllowSorting=bitAllowSorting
			,@bitAllowEdit=0
			,@ListRelID=ListRelID
		FROM 
			#tempForm 
		WHERE 
			tintOrder > @tintOrder-1 
		ORDER BY 
			tintOrder asc            
 
		IF @@rowcount=0 set @tintOrder=0 
	END
SET @strSql=''

Create table #tempColorScheme(vcOrigDbCOlumnName varchar(50),vcLookBackTableName varchar(50),vcFieldValue varchar(50),vcFieldValue1 varchar(50),vcColorScheme varchar(50),vcAssociatedControlType varchar(50))

insert into #tempColorScheme select DFM.vcOrigDbCOlumnName,DFM.vcLookBackTableName,DFCS.vcFieldValue,DFCS.vcFieldValue1,DFCS.vcColorScheme,DFFM.vcAssociatedControlType 
from DycFieldColorScheme DFCS join DycFormField_Mapping DFFM on DFCS.numFieldID=DFFM.numFieldID
join DycFieldMaster DFM on DFM.numModuleID=DFFM.numModuleID and DFM.numFieldID=DFFM.numFieldID
where DFCS.numDomainID=@numDomainID and DFFM.numFormID=43 AND DFCS.numFormID=43 and isnull(DFFM.bitAllowGridColor,0)=1

IF(SELECT COUNT(*) FROM #tempColorScheme)>0
BEGIN
   select top 1 @vcCSOrigDbCOlumnName=vcOrigDbCOlumnName,@vcCSLookBackTableName=vcLookBackTableName,@vcCSAssociatedControlType=vcAssociatedControlType from #tempColorScheme
END   
-------------------- 0 - TASK----------------------------

IF(LEN(@CustomSearchCriteria)>0)
BEGIN
SET @CustomSearchCriteria = ' AND '+@CustomSearchCriteria
END
IF(LEN(@ActivityRegularSearch)=0)
BEGIN
	SET @ActivityRegularSearch = '1=1'
END
DECLARE @StaticColumns AS NVARCHAR(MAX)=''

SET @StaticColumns = ' RecordCount,RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
dtDueDate,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating,
numContactID,numDivisionID,tintCRMType,Id,bitTask,Startdate,EndTime,itemDesc,
[Name],vcFirstName,vcLastName,numPhone,numPhoneExtension,[Phone],vcCompanyName,vcProfile,vcEmail,Task,Status,numRecOwner,numModifiedBy,numTerId,numAssignedBy,caseid,vcCasenumber,
casetimeId,caseExpId,type,itemid,bitFollowUpAnyTime,numNoOfEmployeesId,vcComPhone,numFollowUpStatus,dtLastFollowUp,vcLastSalesOrderDate,monDealAmount,vcPerformance'
SET @dynamicQuery = ' INSERT INTO #tempRecords(
 '+@StaticColumns +@vcCustomColumnNameOnly+'
) SELECT COUNT(*) OVER() RecordCount,* FROM (SELECT 
Comm.numCommId AS RecordId,
Comm.numCreatedBy AS numCreatedBy, 
ISNULL(Div.numTerId,0) AS numOrgTerId,
-2 AS numTaskId,
bitClosedFlag AS IsTaskClosed,
''<a onclick="openCommTask(''+CAST(Comm.numCommId AS VARCHAR)+'',0)" href="javascript:void(0)">''+dbo.GetListIemName(Comm.bitTask)+''</a>'' As TaskTypeName,
Comm.bitTask AS TaskType,
'''' AS OrigDescription,
comm.textDetails AS Description,
DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dtStartTime) AS dtDueDate,
DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dtStartTime) as DueDate,
''-'' As TotalProgress,
dbo.fn_GetContactName(numAssign) As numAssignedTo,
numAssign As numAssignToId,
listdetailsStatus.VcData As Priority, -- Priority column   
 listdetailsActivity.VcData As Activity ,
listdetailsStatus.NumlistItemID As PriorityId, -- Priority column   
 listdetailsActivity.NumlistItemID As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(Div.numDivisionID AS VARCHAR)+''">''+Comp.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating,
 Addc.numContactID,Addc.numDivisionID,Div.tintCRMType,Comm.numCommId AS Id,convert(varchar(15),bitTask) as bitTask,
  DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dtStartTime) as Startdate,                        
 DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dtEndTime) as EndTime ,                                                                        
 Comm.textDetails AS itemDesc,  
 AddC.vcFirstName + '' '' + AddC.vcLastName  as [Name], 
 AddC.vcFirstName,AddC.vcLastName,Addc.numPhone,AddC.numPhoneExtension,                               
 case when Addc.numPhone<>'''' then + AddC.numPhone +case when AddC.numPhoneExtension<>'''' then '' - '' + AddC.numPhoneExtension else '''' end  else '''' end as [Phone], 
  Comp.vcCompanyName As vcCompanyName , ISNULL((SELECT vcData FROM ListDetails WHERE numListID =21 AND numDomainID='+ CAST(@numDomainID AS VARCHAR) +' AND  numListItemID=Comp.vcProfile ) ,'''') vcProfile ,                                                        
 AddC.vcEmail As vcEmail ,    
 dbo.GetListIemName(Comm.bitTask)AS Task,   
 listdetailsStatus.VcData As Status,dbo.fn_GetContactName(Comm.numCreatedBy) AS numRecOwner,dbo.fn_GetContactName(Comm.numModifiedBy) AS numModifiedBy,  
  (select TOP 1 vcData from ListDetails where numListItemID=Div.numTerId) AS numTerId,dbo.fn_GetContactName(Comm.numAssignedBy) AS numAssignedBy,
  convert(varchar(50),comm.caseid) as caseid,(select top 1 vcCaseNumber from cases where cases.numcaseid= comm.caseid )as vcCasenumber,
  ISNULL(comm.casetimeId,0) casetimeId,ISNULL(comm.caseExpId,0) caseExpId,0 as type,'''' as itemid,ISNULL(bitFollowUpAnyTime,0) bitFollowUpAnyTime,  
  dbo.GetListIemName(Comp.numNoOfEmployeesId) AS numNoOfEmployeesId,Div.vcComPhone,
dbo.GetListIemName(Div.numFollowUpStatus) AS numFollowUpStatus,' + 
CONCAT('ISNULL(dbo.FormatedDateFromDate(Div.dtLastFollowUp,',@numDomainID,'),'''')') + ' AS dtLastFollowUp,
' + CONCAT('ISNULL(dbo.FormatedDateFromDate(TempLastOrder.bintCreatedDate,',@numDomainID,'),'''')') + ' AS vcLastSalesOrderDate,ISNULL(TempPerformance.monDealAmount,0) monDealAmount,
(CASE WHEN ISNULL(TempPerformance.monDealAmount,0) > 0 THEN CONCAT(FORMAT(TempPerformance.monDealAmount,''##,####.00''),'' ('',CAST(TempPerformance.fltProfitPer AS DECIMAL(18,2)),''%)'') ELSE '''' END) AS vcPerformance
  '+@vcCustomColumnName+'
 FROM  Communication Comm                              
 JOIN AdditionalContactsInformation AddC                                                  
 ON Comm.numContactId = AddC.numContactId                                  
 JOIN DivisionMaster Div                                                                         
 ON AddC.numDivisionId = Div.numDivisionID                                   
 JOIN CompanyInfo  Comp                                                                         
 ON Div.numCompanyID = Comp.numCompanyId  
 OUTER APPLY(' + CONCAT('SELECT MAX(bintCreatedDate) AS bintCreatedDate FROM OpportunityMaster WHERE numDomainID = ',@numDomainID,' AND numDivisionID=Div.numDivisionID AND tintOppType = 1 AND tintOppStatus = 1') + ' ) AS TempLastOrder
 OUTER APPLY(' + CONCAT('SELECT
																	SUM(monDealAmount) AS monDealAmount
																	,AVG(ProfitPer) AS fltProfitPer
																FROM
																(
																	SELECT
																		OM.numDivisionId
																		,monDealAmount
																		,AVG(((ISNULL(OI.monPrice,0) - (CASE D.numCost WHEN 3 THEN ISNULL(V.monCost,0) * dbo.fn_UOMConversion(I.numBaseUnit,I.numItemCode,I.numDomainID,I.numPurchaseUnit) WHEN 2 THEN ISNULL(OI.numCost,0) ELSE ISNULL(I.monAverageCost,0) END)) / ISNULL(NULLIF(OI.monPrice,0),1)) * 100) ProfitPer
																	FROM
																		OpportunityMaster OM
																	INNER JOIN
																		Domain D
																	ON
																		OM.numDomainID = D.numDomainID
																	INNER JOIN
																		OpportunityItems OI
																	ON
																		OM.numOppId = OI.numOppId
																	INNER JOIN
																		Item I
																	ON
																		OI.numItemCode = I.numItemCode
																	LEFT JOIN
																		Vendor V
																	ON
																		V.numVendorID = I.numVendorID
																		AND V.numItemCode = I.numItemCode
																	WHERE
																		OM.numDomainId = ',@numDomainID,'
																		AND OM.numDivisionId = Div.numDivisionID
																		AND ISNULL(I.bitContainer,0) = 0
																		AND tintOppType = 1
																		AND tintOppStatus=1
																	GROUP BY
																		OM.numDivisionId
																		,OM.numOppId
																		,monDealAmount
																) T1
																GROUP BY
																	T1.numDivisionId )') + ' AS TempPerformance
 LEFT JOIN OpportunityMaster Opp ON Opp.numDivisionId = Div.numDivisionID AND Opp.numContactId = AddC.numContactId  AND Opp.numOppID = Comm.numOppID 
 LEFT JOIN Sales_process_List_Master splm ON splm.Slp_Id = Opp.numBusinessProcessID 
 LEFT JOIN ProjectProgress PP On PP.numOppId = Opp.numOppId
  Left Join listdetails listdetailsStatus                                                 
 On Comm.numStatus = listdetailsStatus.NumlistItemID
  Left Join listdetails listdetailsActivity                                                  
 On Comm.numActivity = listdetailsActivity.NumlistItemID
 LEFT JOIN ListDetails AS listCompanyRating
 ON Comp.numCompanyRating=listCompanyRating.NumlistItemID
WHERE '+@ActivityRegularSearch+' AND
	Comm.numDomainID = ' + Cast(@numDomainID as varchar(10))  + ' AND 1 = (CASE ' + CAST( @tintActionItemsViewRights AS VARCHAR) + 
' WHEN 3 THEN 1 
WHEN 2 THEN (CASE WHEN Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+ CONVERT(VARCHAR(15),@numUserCntID) + ' ) THEN 1 ELSE 0 END)
WHEN 1 THEN (CASE WHEN (Comm.numCreatedBy= ' + CONVERT(VARCHAR(15),@numUserCntID) + ' or Comm.numAssign= '+ CONVERT(VARCHAR(15),@numUserCntID) + ') THEN 1 ELSE 0 END) 
ELSE 0 END)  AND (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',dtStartTime) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',dtStartTime) <= '''+Cast(@endDate as varchar(30))+''')
'+@CustomSearchCriteria+'
 )   Q WHERE '+@OtherRegularSearch+'
  '+@IndivRecordPaging+'
'


EXEC (@dynamicQuery)
PRINT 'HI'
PRINT CAST(@dynamicQuery AS TEXT)
PRINT LEN(@ActivityRegularSearch)
-------------------- 0 - Email Communication----------------------------
--''<a onclick="openCommTask(''+CAST(ac.activityid AS VARCHAR)+'',1)" href="javascript:void(0)">Calendar</a>'' As TaskTypeName,
IF(LEN(@ActivityRegularSearch)=0 OR RTRIM(LTRIM(@ActivityRegularSearch))='1=1')
BEGIN
DECLARE @FormattedItems As VARCHAR(MAX)
SET @FormattedItems ='
RecordCount,RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
dtDueDate,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating'
SET @dynamicQuery =  ' INSERT INTO #tempRecords(
'+@FormattedItems+'
) SELECT  COUNT(*) OVER() RecordCount,* FROM (  SELECT 
ac.activityid AS RecordId,
0 AS numCreatedBy,
ISNULL(Div.numTerId,0) AS numOrgTerId,
-2 AS numTaskId,
case when alldayevent = 1 then ''1'' else ''0'' end AS IsTaskClosed,
''<a href="javascript:void(0)">Calendar</a>'' As TaskTypeName,
12 AS TaskType,
CASE WHEN ISNULL(ac.Comments,'''') ='''' THEN ac.ActivityDescription ELSE ISNULL(ac.Comments,'''') END AS OrigDescription,
(SELECT DISTINCT  
replace(replace((SELECT 
CASE(ISNULL(bitpartycalendarTitle,1)) WHEN 1 THEN ''<b><font color=red>Title</font></b>'' + CASE(ISNULL(ac.Subject,'''')) WHEN '''' THEN '''' else ac.Subject END + ''<br>'' else ''''  END  
+ CASE(ISNULL(bitpartycalendarLocation,1)) WHEN 1 THEN ''<b><font color=green>Location</font></b>'' + CASE(ISNULL(ac.location,'''')) WHEN '''' THEN '''' else ac.location END + ''<br>'' else ''''  END  
+ CASE ISNULL(bitpartycalendarDescription,1) 
	WHEN 1 
	THEN CONCAT(''<b><font color=#3c8dbc>Description</font></b>'',
				(CASE 
					WHEN LEN(CASE WHEN ISNULL(ac.Comments,'''') ='''' THEN ac.ActivityDescription ELSE ISNULL(ac.Comments,'''') END) > 100 
					THEN CONCAT(CAST((CASE WHEN ISNULL(ac.Comments,'''') ='''' THEN ac.ActivityDescription ELSE ISNULL(ac.Comments,'''') END) AS VARCHAR(100)),CONCAT(''...'',''<a href="#" role="button" title="Description" data-toggle="popover" data-content="'',(CASE WHEN ISNULL(ac.Comments,'''') ='''' THEN ac.ActivityDescription ELSE ISNULL(ac.Comments,'''') END) ,''"> more</a>'')) 
					ELSE (CASE WHEN ISNULL(ac.Comments,'''') ='''' THEN ac.ActivityDescription ELSE ISNULL(ac.Comments,'''') END) 
				END)) 
	ELSE ''''  
	END  
FROM Domain where numDomainId='+ CAST(@numDomainID AS VARCHAR) +' 
FOR XML PATH('''')
),''&lt;'', ''<''), ''&gt;'', ''>''))   AS Description,
DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dateadd(second,duration,startdatetimeutc)) AS dtDueDate,
DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dateadd(second,duration,startdatetimeutc)) as DueDate,
''0'' As TotalProgress,
''-'' As numAssignedTo,
''0'' As numAssignToId,
'''' As Priority, -- Priority column   
'''' As Activity ,
0 As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(Div.numDivisionID AS VARCHAR)+''">''+Comp.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating
 From activity ac join             
activityresource ar on  ac.activityid=ar.activityid             
join resource res on ar.resourceid=res.resourceid                         
--join usermaster UM on um.numUserId = res.numUserCntId                
join AdditionalContactsInformation ACI on ACI.numContactId = res.numUserCntId                
 JOIN DivisionMaster Div                                                          
 ON ACI.numDivisionId = Div.numDivisionID                                   
 JOIN CompanyInfo  Comp                             
 ON Div.numCompanyID = Comp.numCompanyId
  LEFT JOIN ListDetails AS listCompanyRating
 ON Comp.numCompanyRating=listCompanyRating.NumlistItemID
 OUTER APPLY(' + CONCAT('SELECT MAX(bintCreatedDate) AS bintCreatedDate FROM OpportunityMaster WHERE numDomainID = ',@numDomainID,' AND numDivisionID=Div.numDivisionID AND tintOppType = 1 AND tintOppStatus = 1') + ' ) AS TempLastOrder                 
 OUTER APPLY(' + CONCAT('SELECT
																	SUM(monDealAmount) AS monDealAmount
																	,AVG(ProfitPer) AS fltProfitPer
																FROM
																(
																	SELECT
																		OM.numDivisionId
																		,monDealAmount
																		,AVG(((ISNULL(OI.monPrice,0) - (CASE D.numCost WHEN 3 THEN ISNULL(V.monCost,0) * dbo.fn_UOMConversion(I.numBaseUnit,I.numItemCode,I.numDomainID,I.numPurchaseUnit) WHEN 2 THEN ISNULL(OI.numCost,0) ELSE ISNULL(I.monAverageCost,0) END)) / ISNULL(NULLIF(OI.monPrice,0),1)) * 100) ProfitPer
																	FROM
																		OpportunityMaster OM
																	INNER JOIN
																		Domain D
																	ON
																		OM.numDomainID = D.numDomainID
																	INNER JOIN
																		OpportunityItems OI
																	ON
																		OM.numOppId = OI.numOppId
																	INNER JOIN
																		Item I
																	ON
																		OI.numItemCode = I.numItemCode
																	LEFT JOIN
																		Vendor V
																	ON
																		V.numVendorID = I.numVendorID
																		AND V.numItemCode = I.numItemCode
																	WHERE
																		OM.numDomainId = ',@numDomainID,'
																		AND OM.numDivisionId = Div.numDivisionID
																		AND ISNULL(I.bitContainer,0) = 0
																		AND tintOppType = 1
																		AND tintOppStatus=1
																		
																	GROUP BY
																		OM.numDivisionId
																		,OM.numOppId
																		,monDealAmount
																) T1
																GROUP BY
																	T1.numDivisionId )') + ' AS TempPerformance

 where  res.numUserCntId = ' + Cast(@numUserCntID as varchar(10)) +'             
and ac.OriginalStartDateTimeUtc IS NULL            
and res.numDomainID = ' + Cast(@numDomainID as varchar(10)) +'  and 
(DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', startdatetimeutc) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', startdatetimeutc) <= '''+Cast(@endDate as varchar(30))+''')
--(startdatetimeutc between dateadd(month,-1,getutcdate()) and dateadd(month,1,getutcdate())) 
and [status] <> -1                                                     
and ac.activityid not in 
(select distinct(numActivityId) from communication Comm where Comm.numDomainID = ' + Cast(@numDomainID as varchar(10)) +'  
AND 1=(Case ' + Cast(@bitFilterRecord as varchar(10)) +' when 1 then Case when Comm.numAssign =' + Cast(@numUserCntID as varchar(10)) +'  then 1 else 0 end 
		else  Case when (Comm.numAssign =' + Cast(@numUserCntID as varchar(10)) +'  or Comm.numCreatedBy=' + Cast(@numUserCntID as varchar(10)) +' ) then 1 else 0 end end)   
		AND 1 = (CASE ' + CAST( @tintActionItemsViewRights AS VARCHAR) + 
' WHEN 3 THEN 1 
WHEN 2 THEN (CASE WHEN Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+ CONVERT(VARCHAR(15),@numUserCntID) + ' ) THEN 1 ELSE 0 END)
WHEN 1 THEN (CASE WHEN (Comm.numCreatedBy= ' + CONVERT(VARCHAR(15),@numUserCntID) + ' or Comm.numAssign= '+ CONVERT(VARCHAR(15),@numUserCntID) + ') THEN 1 ELSE 0 END) 
ELSE 0 END)                                                                   

)) Q WHERE '+@OtherRegularSearch+'  '+@IndivRecordPaging+'
'

EXEC (@dynamicQuery)
-------------------- 1 - CASE----------------------------

SET @dynamicQuery =  ' INSERT INTO #tempRecords(
RecordCount,
RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
dtDueDate,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating
) SELECT  COUNT(*) OVER() RecordCount,* FROM (  SELECT 
C.numCaseId AS RecordId,
C.numCreatedBy AS numCreatedBy,
ISNULL(Div.numTerId,0) AS numOrgTerId,
0 AS numTaskId,
CASE WHEN C.numStatus=1008 THEN 1 ELSE 0 END AS IsTaskClosed,
''<a onclick="openCaseDetails(''+CAST(C.numCaseId AS VARCHAR)+'')" href="javascript:void(0)">Case</a>'' As TaskTypeName,
1 AS TaskType,
'''' AS OrigDescription,
C.textSubject AS Description,
cast(C.intTargetResolveDate as datetime) AS dtDueDate,
cast(C.intTargetResolveDate as datetime) as DueDate,
''0'' As TotalProgress,
dbo.fn_GetContactName(C.numAssignedTo) As numAssignedTo,
C.numAssignedTo As numAssignToId,
listdetailsStatus.VcData As Priority, -- Priority column   
'''' As Activity ,
listdetailsStatus.NumlistItemID As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(Div.numDivisionID AS VARCHAR)+''">''+Com.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating

 FROM 
	Cases  C                          
INNER JOIN  
	AdditionalContactsInformation  AddC                          
ON 
	C.numContactId = AddC.numContactId                           
INNER JOIN 
	DivisionMaster Div                           
ON 
	AddC.numDivisionId = Div.numDivisionID AND  
	AddC.numDivisionId = Div.numDivisionID                           
INNER JOIN  
	CompanyInfo  Com                          
ON 
	Div.numCompanyID = Com.numCompanyId    
 LEFT JOIN ListDetails AS listCompanyRating
 ON Com.numCompanyRating=listCompanyRating.NumlistItemID
  Left Join listdetails listdetailsStatus                                                 
 On C.numPriority = listdetailsStatus.NumlistItemID
WHERE
	C.numDomainID = ' + Cast(@numDomainID as varchar(10))  + ' AND C.numStatus <> 136  AND 1 = (CASE ' + CAST( @tintActionItemsViewRights AS VARCHAR) + 
' WHEN 3 THEN 1 
WHEN 2 THEN (CASE WHEN Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+ CONVERT(VARCHAR(15),@numUserCntID) + ' ) THEN 1 ELSE 0 END)
WHEN 1 THEN (CASE WHEN (C.numCreatedBy= ' + CONVERT(VARCHAR(15),@numUserCntID) + ' or C.numAssignedTo= '+ CONVERT(VARCHAR(15),@numUserCntID) + ') THEN 1 ELSE 0 END) 
ELSE 0 END)  AND (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',intTargetResolveDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',intTargetResolveDate) <= '''+Cast(@endDate as varchar(30))+''') 
) Q WHERE '+@OtherRegularSearch+' '+@IndivRecordPaging+'
'
EXEC (@dynamicQuery)

-------------------- 2 - Project Task ----------------------------

SET @dynamicQuery =  ' INSERT INTO #tempRecords(
RecordCount,
RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
dtDueDate,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating
)  SELECT  COUNT(*) OVER() RecordCount,* FROM (SELECT
T.numProjectId AS RecordId,
T.numCreatedBy AS numCreatedBy,
ISNULL(Div.numTerId,0) AS numOrgTerId,
T.numTaskId AS numTaskId,
T.bitTaskClosed AS IsTaskClosed,
''<a onclick="openProjectTask(''+CAST(T.numProjectId AS VARCHAR)+'')" href="javascript:void(0)">Project Task</a>'' As TaskTypeName,
2 AS TaskType,
'''' AS OrigDescription,
T.vcTaskName AS Description,
CASE WHEN S.bitIsDueDaysUsed=1 THEN CONVERT(VARCHAR(10),DATEADD(DAY,S.intDueDays,CAST(S.dtStartDate AS DATE)), 101)
		ELSE CONVERT(VARCHAR(10),DATEADD(DAY,(SELECT TOP 1 CAST(Items AS DECIMAL(18,2)) FROM Split(dbo.fn_calculateTotalHoursMinutesByStageDetails(T.numStageDetailsId,T.numOppId,T.numProjectId),''_'')),CAST(S.dtStartDate AS DATE)), 101) END AS dtDueDate,
CASE WHEN S.bitIsDueDaysUsed=1 THEN CONVERT(VARCHAR(10),DATEADD(DAY,S.intDueDays,CAST(S.dtStartDate AS DATE)), 101)
		ELSE CONVERT(VARCHAR(10),DATEADD(DAY,(SELECT TOP 1 CAST(Items AS DECIMAL(18,2)) FROM Split(dbo.fn_calculateTotalHoursMinutesByStageDetails(T.numStageDetailsId,T.numOppId,T.numProjectId),''_'')),CAST(S.dtStartDate AS DATE)), 101) END as DueDate,
ISNULL((SELECT TOP 1 intTotalProgress FROM ProjectProgress WHERE numProId = T.numProjectId),0) As TotalProgress,
dbo.fn_GetContactName(T.numAssignTo) As numAssignedTo,
T.numAssignTo As numAssignToId,
'''' As Priority, -- Priority column   
'''' As Activity ,
0 As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(Div.numDivisionID AS VARCHAR)+''">''+Com.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating

 FROM 
	StagePercentageDetailsTask AS T
	LEFT JOIN StagePercentageDetails AS S
	ON S.numStageDetailsId=T.numStageDetailsId AND S.numProjectID=T.numProjectID
	LEFT JOIN ProjectsMaster AS OP 
	ON T.numProjectID=OP.numProId
	LEFT JOIN StagePercentageMaster AS SM
	ON S.numStagePercentageId=SM.numStagePercentageId                        
INNER JOIN 
	DivisionMaster Div                           
ON 
	OP.numDivisionId = Div.numDivisionID                       
INNER JOIN  
	CompanyInfo  Com                          
ON 
	Div.numCompanyID = Com.numCompanyId    
LEFT JOIN ListDetails AS listCompanyRating
 ON Com.numCompanyRating=listCompanyRating.NumlistItemID
WHERE
	T.numDomainID = ' + Cast(@numDomainID as varchar(10))  + ' AND T.numProjectId>0 AND s.numProjectId>0 AND T.bitSavedTask=1 AND T.bitTaskClosed=0 AND 1 = (CASE ' + CAST( @tintActionItemsViewRights AS VARCHAR) + 
' WHEN 3 THEN 1 
WHEN 2 THEN (CASE WHEN Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+ CONVERT(VARCHAR(15),@numUserCntID) + ' ) THEN 1 ELSE 0 END)
WHEN 1 THEN (CASE WHEN (T.numCreatedBy= ' + CONVERT(VARCHAR(15),@numUserCntID) + ' or T.numAssignTo= '+ CONVERT(VARCHAR(15),@numUserCntID) + ') THEN 1 ELSE 0 END) 
ELSE 0 END) ) Q WHERE '+@OtherRegularSearch+' AND (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) <= '''+Cast(@endDate as varchar(30))+''') 
'+@IndivRecordPaging+'
'

EXEC (@dynamicQuery)

------------------ 3 - Opportunity  Task ----------------------------

SET @dynamicQuery =  ' INSERT INTO #tempRecords(
RecordCount,
RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
dtDueDate,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating
) SELECT  COUNT(*) OVER() RecordCount,* FROM ( SELECT 
T.numOppId AS RecordId,
T.numCreatedBy AS numCreatedBy,
ISNULL(Div.numTerId,0) AS numOrgTerId,
T.numTaskId AS numTaskId,
T.bitTaskClosed AS IsTaskClosed,
''<a onclick="openTask(''+CAST(T.numOppId AS VARCHAR)+'',''+CAST(S.numStagePercentageId AS VARCHAR)+'',''+CAST(S.tinProgressPercentage AS VARCHAR)+'',''+CAST(S.tintConfiguration AS VARCHAR)+'')" href="javascript:void(0)">Opportunity Task</a>'' As TaskTypeName,
3 AS TaskType,
'''' AS OrigDescription,
T.vcTaskName AS Description,
CASE WHEN S.bitIsDueDaysUsed=1 THEN CONVERT(VARCHAR(10),DATEADD(DAY,S.intDueDays,CAST(S.dtStartDate AS DATE)), 101)
		ELSE CONVERT(VARCHAR(10),DATEADD(DAY,(SELECT TOP 1 CAST(Items AS DECIMAL(18,2)) FROM Split(dbo.fn_calculateTotalHoursMinutesByStageDetails(T.numStageDetailsId,T.numOppId,T.numProjectId),''_'')),CAST(S.dtStartDate AS DATE)), 101) END AS dtDueDate,
CASE WHEN S.bitIsDueDaysUsed=1 THEN CONVERT(VARCHAR(10),DATEADD(DAY,S.intDueDays,CAST(S.dtStartDate AS DATE)), 101)
		ELSE CONVERT(VARCHAR(10),DATEADD(DAY,(SELECT TOP 1 CAST(Items AS DECIMAL(18,2)) FROM Split(dbo.fn_calculateTotalHoursMinutesByStageDetails(T.numStageDetailsId,T.numOppId,T.numProjectId),''_'')),CAST(S.dtStartDate AS DATE)), 101) END as DueDate,
ISNULL((SELECT TOP 1 intTotalProgress FROM ProjectProgress WHERE numOppId = T.numOppId),0) As TotalProgress,
dbo.fn_GetContactName(T.numAssignTo) As numAssignedTo,
T.numAssignTo As numAssignToId,
'''' As Priority, -- Priority column   
'''' As Activity ,
0 As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(Div.numDivisionID AS VARCHAR)+''">''+Com.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating

 FROM 
	StagePercentageDetailsTask AS T
	LEFT JOIN StagePercentageDetails AS S
	ON S.numStageDetailsId=T.numStageDetailsId AND S.numOppId=T.numOppId
	LEFT JOIN OpportunityMaster AS OP 
	ON T.numOppId=OP.numOppId
	LEFT JOIN StagePercentageMaster AS SM
	ON S.numStagePercentageId=SM.numStagePercentageId                        
INNER JOIN 
	DivisionMaster Div                           
ON 
	OP.numDivisionId = Div.numDivisionID                       
INNER JOIN  
	CompanyInfo  Com                          
ON 
	Div.numCompanyID = Com.numCompanyId    
LEFT JOIN ListDetails AS listCompanyRating
 ON Com.numCompanyRating=listCompanyRating.NumlistItemID
WHERE
	T.numDomainID = ' + Cast(@numDomainID as varchar(10))  + ' AND OP.tintOppType=1 AND OP.tintOppStatus=0 AND T.numOppId>0 AND s.numOppId>0 AND T.bitSavedTask=1 AND T.bitTaskClosed=0 AND 1 = (CASE ' + CAST( @tintActionItemsViewRights AS VARCHAR) + 
' WHEN 3 THEN 1 
WHEN 2 THEN (CASE WHEN Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+ CONVERT(VARCHAR(15),@numUserCntID) + ' ) THEN 1 ELSE 0 END)
WHEN 1 THEN (CASE WHEN (T.numCreatedBy= ' + CONVERT(VARCHAR(15),@numUserCntID) + ' or T.numAssignTo= '+ CONVERT(VARCHAR(15),@numUserCntID) + ') THEN 1 ELSE 0 END) 
ELSE 0 END) ) Q WHERE '+@OtherRegularSearch+' AND (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) <= '''+Cast(@endDate as varchar(30))+''')
'+@IndivRecordPaging+' 
 '
 
 EXEC (@dynamicQuery)

------------------ 4 -  Sales Order Task ----------------------------

SET @dynamicQuery =  ' INSERT INTO #tempRecords(
RecordCount,
RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
dtDueDate,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating
)SELECT  COUNT(*) OVER() RecordCount,* FROM (  SELECT 
T.numOppId AS RecordId,
T.numCreatedBy AS numCreatedBy,
ISNULL(Div.numTerId,0) AS numOrgTerId,
T.numTaskId AS numTaskId,
T.bitTaskClosed AS IsTaskClosed,
''<a onclick="openTask(''+CAST(T.numOppId AS VARCHAR)+'',''+CAST(S.numStagePercentageId AS VARCHAR)+'',''+CAST(S.tinProgressPercentage AS VARCHAR)+'',''+CAST(S.tintConfiguration AS VARCHAR)+'')" href="javascript:void(0)">Sales Order Task</a>'' As TaskTypeName,
4 AS TaskType,
'''' AS OrigDescription,
T.vcTaskName AS Description,
CASE WHEN S.bitIsDueDaysUsed=1 THEN CONVERT(VARCHAR(10),DATEADD(DAY,S.intDueDays,CAST(S.dtStartDate AS DATE)), 101)
		ELSE CONVERT(VARCHAR(10),DATEADD(DAY,(SELECT TOP 1 CAST(Items AS DECIMAL(18,2)) FROM Split(dbo.fn_calculateTotalHoursMinutesByStageDetails(T.numStageDetailsId,T.numOppId,T.numProjectId),''_'')),CAST(S.dtStartDate AS DATE)), 101) END AS dtDueDate,
CASE WHEN S.bitIsDueDaysUsed=1 THEN CONVERT(VARCHAR(10),DATEADD(DAY,S.intDueDays,CAST(S.dtStartDate AS DATE)), 101)
		ELSE CONVERT(VARCHAR(10),DATEADD(DAY,(SELECT TOP 1 CAST(Items AS DECIMAL(18,2)) FROM Split(dbo.fn_calculateTotalHoursMinutesByStageDetails(T.numStageDetailsId,T.numOppId,T.numProjectId),''_'')),CAST(S.dtStartDate AS DATE)), 101) END as DueDate,
ISNULL((SELECT TOP 1 intTotalProgress FROM ProjectProgress WHERE numOppId = T.numOppId),0) As TotalProgress,
dbo.fn_GetContactName(T.numAssignTo) As numAssignedTo,
T.numAssignTo As numAssignToId,
'''' As Priority, -- Priority column   
'''' As Activity ,
0 As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(Div.numDivisionID AS VARCHAR)+''">''+Com.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating

 FROM 
	StagePercentageDetailsTask AS T
	LEFT JOIN StagePercentageDetails AS S
	ON S.numStageDetailsId=T.numStageDetailsId AND S.numOppId=T.numOppId
	LEFT JOIN OpportunityMaster AS OP 
	ON T.numOppId=OP.numOppId
	LEFT JOIN StagePercentageMaster AS SM
	ON S.numStagePercentageId=SM.numStagePercentageId                        
INNER JOIN 
	DivisionMaster Div                           
ON 
	OP.numDivisionId = Div.numDivisionID                       
INNER JOIN  
	CompanyInfo  Com                          
ON 
	Div.numCompanyID = Com.numCompanyId    
LEFT JOIN ListDetails AS listCompanyRating
 ON Com.numCompanyRating=listCompanyRating.NumlistItemID
WHERE
	T.numDomainID = ' + Cast(@numDomainID as varchar(10))  + ' AND OP.tintOppType=1 AND OP.tintOppStatus=1 AND T.numOppId>0 AND s.numOppId>0 AND T.bitSavedTask=1 AND T.bitTaskClosed=0 AND 1 = (CASE ' + CAST( @tintActionItemsViewRights AS VARCHAR) + 
' WHEN 3 THEN 1 
WHEN 2 THEN (CASE WHEN Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+ CONVERT(VARCHAR(15),@numUserCntID) + ' ) THEN 1 ELSE 0 END)
WHEN 1 THEN (CASE WHEN (T.numCreatedBy= ' + CONVERT(VARCHAR(15),@numUserCntID) + ' or T.numAssignTo= '+ CONVERT(VARCHAR(15),@numUserCntID) + ') THEN 1 ELSE 0 END) 
ELSE 0 END) ) Q WHERE '+@OtherRegularSearch+' AND (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) <= '''+Cast(@endDate as varchar(30))+''')
'+@IndivRecordPaging+'
 '
 EXEC (@dynamicQuery)
  
------------------ 5 -  Work Order Task ----------------------------

SET @dynamicQuery =  ' INSERT INTO #tempRecords(
RecordCount,
RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
dtDueDate,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating
)SELECT  COUNT(*) OVER() RecordCount,* FROM ( SELECT 
WorkOrder.numWOID AS RecordId,
WorkOrder.numCreatedBy AS numCreatedBy,
ISNULL(Div.numTerId,0) AS numOrgTerId,
WorkOrder.numWOID AS numTaskId,
(CASE WHEN WorkOrder.numWOStatus = 23184 THEN 1 ELSE 0 END) AS IsTaskClosed,
CONCAT(''<a href="../items/frmWorkOrder.aspx?WOID='',WorkOrder.numWOID,''" target="_blank">Work Order Management</a>'') As TaskTypeName,
5 AS TaskType,
'''' AS OrigDescription,
CONCAT(''<b>'',Item.vcItemName,''</b>'','' ('',WorkOrder.numQtyItemsReq,'')'','' <b>Work Order Status :'', ''</b>'',''<label class="lblWorkOrderStatus" id="'',WorkOrder.numWOID,''"><i class="fa fa-refresh fa-spin"></i></lable>'') AS Description,
DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',WorkOrder.dtmEndDate) AS dtDueDate,
DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',WorkOrder.dtmEndDate) as DueDate,
dbo.GetTotalProgress(WorkOrder.numDomainID,WorkOrder.numWOID,1,1,'''',0) As TotalProgress,
dbo.fn_GetContactName(WorkOrder.numAssignedTo) As numAssignedTo,
WorkOrder.numAssignedTo As numAssignToId,
'''' As Priority, -- Priority column   
'''' As Activity ,
0 As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(Div.numDivisionID AS VARCHAR)+''">''+Com.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating

 FROM 
	WorkOrder
INNER JOIN 
	Item
ON
	WorkOrder.numItemCode = Item.numItemCode
LEFT JOIN 
	OpportunityMaster
ON 
	WorkOrder.numOppId=OpportunityMaster.numOppId
INNER JOIN 
	DivisionMaster Div                           
ON 
	Div.numDivisionId = (CASE WHEN OpportunityMaster.numOppID IS NOT NULL THEN OpportunityMaster.numDivisionID ELSE ' + CAST(@numDomainDivisionID AS VARCHAR) + ' END)                        
INNER JOIN  
	CompanyInfo  Com                          
ON 
	Div.numCompanyID = Com.numCompanyId    
LEFT JOIN 
	ListDetails AS listCompanyRating
ON 
	Com.numCompanyRating=listCompanyRating.NumlistItemID
WHERE
	WorkOrder.numDomainID = ' + Cast(@numDomainID as varchar(10))  + ' 
	AND WorkOrder.numWOStatus <> 23184
	AND WorkOrder.numAssignedTo= '+ CONVERT(VARCHAR(15),@numUserCntID) + '
	AND ((DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',WorkOrder.dtmStartDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',WorkOrder.dtmStartDate) <= '''+Cast(@endDate as varchar(30))+''') OR (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',WorkOrder.dtmEndDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',WorkOrder.dtmEndDate) <= '''+Cast(@endDate as varchar(30))+'''))
) Q WHERE '+@OtherRegularSearch+' '+@IndivRecordPaging+'
 '
 EXEC (@dynamicQuery)
------------------ 6 -  Work Center Task ----------------------------

SET @dynamicQuery =  ' INSERT INTO #tempRecords(
RecordCount,
RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
dtDueDate,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating
) SELECT  COUNT(*) OVER() RecordCount,* FROM ( SELECT 
T.numTaskId AS RecordId,
T.numCreatedBy AS numCreatedBy,
ISNULL(Div.numTerId,0) AS numOrgTerId,
T.numTaskId AS numTaskId,
(CASE WHEN EXISTS (SELECT * FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=T.numTaskID AND SPDTTL.tintAction=4) THEN 1 ELSE 0 END) AS IsTaskClosed,
CONCAT(''<a href="../items/frmWorkOrder.aspx?WOID='',W.numWOID,''" target="_blank">Work Center Task</a>'') As TaskTypeName,
6 AS TaskType,
'''' AS OrigDescription,
T.vcTaskName AS Description,
DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',W.dtmEndDate) AS dtDueDate,
DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',W.dtmEndDate) as DueDate,
(CASE 
	WHEN EXISTS (SELECT * FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=T.numTaskID AND SPDTTL.tintAction=4) THEN CAST(100 AS INT)
	ELSE CAST(((ISNULL((SELECT SUM(numProcessedQty) FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=T.numTaskID),0) / W.numQtyItemsReq) * 100) AS INT)
END) As TotalProgress,
dbo.fn_GetContactName(T.numAssignTo) As numAssignedTo,
T.numAssignTo As numAssignToId,
'''' As Priority, -- Priority column   
'''' As Activity ,
0 As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(Div.numDivisionID AS VARCHAR)+''">''+Com.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating

 FROM 
	StagePercentageDetailsTask AS T
	INNER JOIN WorkOrder AS W 
	ON T.numWorkOrderId=W.numWOId     
LEFT JOIN OpportunityMaster AS OP 
	ON W.numOppId=OP.numOppId
INNER JOIN 
	DivisionMaster Div                           
ON 
	Div.numDivisionId = (CASE WHEN OP.numOppID IS NOT NULL THEN OP.numDivisionID ELSE ' + CAST(@numDomainDivisionID AS VARCHAR) + ' END)                         
INNER JOIN  
	CompanyInfo  Com                          
ON 
	Div.numCompanyID = Com.numCompanyId    
LEFT JOIN ListDetails AS listCompanyRating
 ON Com.numCompanyRating=listCompanyRating.NumlistItemID
WHERE
	W.numDomainID = ' + Cast(@numDomainID as varchar(10))  + '
	AND W.numWOStatus <> 23184 
	AND NOT EXISTS (SELECT * FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=T.numTaskID AND SPDTTL.tintAction=4) 
	AND T.numAssignTo = ' + CAST(@numUserCntID AS VARCHAR) + '
	AND ((DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',W.dtmStartDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',W.dtmStartDate) <= '''+Cast(@endDate as varchar(30))+''') OR (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',W.dtmEndDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',W.dtmEndDate) <= '''+Cast(@endDate as varchar(30))+''')) 
) Q WHERE '+@OtherRegularSearch+' '+@IndivRecordPaging+'
'
EXEC (@dynamicQuery)

------------------ 7 -  Requisition Approval ----------------------------

IF(@bitREQPOApproval=1 AND @numUserCntID IN(SELECT * FROM Split(@vchREQPOApprovalEmp,',') WHERE Items<>''))
BEGIN
SET @dynamicQuery =  ' INSERT INTO #tempRecords(
RecordCount,
RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
dtDueDate,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating
) SELECT  COUNT(*) OVER() RecordCount,* FROM( SELECT 
OP.numOppId AS RecordId,
OP.numCreatedBy AS numCreatedBy,
ISNULL(Div.numTerId,0) AS numOrgTerId,
0 AS numTaskId,
CASE WHEN ISNULL(OP.intReqPOApproved,0)=0 THEN 0 ELSE 1 END AS IsTaskClosed,
''<a  onclick="openSalesOrder(''+CAST(OP.numOppId AS VARCHAR)+'')" href="javascript:void(0)">Requisition Approval</a>'' As TaskTypeName,
7 AS TaskType,
'''' AS OrigDescription,
''<b>For :</b> <a class="underLineHyperLink" onclick="openSalesOrder(''+CAST(OP.numOppId AS VARCHAR)+'')" href="javascript:void(0)">''+OP.vcPOppName+''</a> &nbsp;<i onclick="ApproveRejectPORequistion(''+CAST(OP.numOppId AS VARCHAR)+'',1)"  class="fa fa-thumbs-up cursor"></i>  &nbsp;<i onclick="ApproveRejectPORequistion(''+CAST(OP.numOppId AS VARCHAR)+'',2)"  class="fa fa-thumbs-down cursor"></i>'' AS Description,
OP.intPEstimatedCloseDate AS dtDueDate,
OP.intPEstimatedCloseDate as DueDate,
0 As TotalProgress,
dbo.fn_GetContactName('+CAST(@numUserCntID AS VARCHAR(200))+') As numAssignedTo,
'+CAST(@numUserCntID AS VARCHAR(200))+' As numAssignToId,
'''' As Priority, -- Priority column   
'''' As Activity ,
0 As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(Div.numDivisionID AS VARCHAR)+''">''+Com.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating

 FROM  
 OpportunityMaster AS OP 
INNER JOIN 
	DivisionMaster Div                           
ON 
	OP.numDivisionId = Div.numDivisionID                       
INNER JOIN  
	CompanyInfo  Com                          
ON 
	Div.numCompanyID = Com.numCompanyId    
LEFT JOIN ListDetails AS listCompanyRating
 ON Com.numCompanyRating=listCompanyRating.NumlistItemID
WHERE
	OP.numDomainID = ' + Cast(@numDomainID as varchar(10))  + ' AND OP.tintOppType = 2 AND OP.tintOppStatus=0 
	AND 1 = (CASE WHEN '+CAST(@decReqPOMinValue AS VARCHAR(100))+'>0 AND Op.monPAmount>'+CAST(@decReqPOMinValue AS VARCHAR(100))+' THEN 1  WHEN '+CAST(@decReqPOMinValue AS VARCHAR(100))+'=0 THEN 1  ELSE 0 END ) ) AS Q
WHERE '+@OtherRegularSearch+' AND (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) <= '''+Cast(@endDate as varchar(30))+''') 
'+@IndivRecordPaging+'
'
EXEC (@dynamicQuery)

END
------------------ 8 -  PO Approval ----------------------------
IF(@bitREQPOApproval=1 AND @numUserCntID IN(SELECT * FROM Split(@vchREQPOApprovalEmp,',') WHERE Items<>''))
BEGIN
SET @dynamicQuery =  ' INSERT INTO #tempRecords(
RecordCount,
RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
dtDueDate,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating
) SELECT  COUNT(*) OVER() RecordCount,* FROM ( SELECT
OP.numOppId AS RecordId,
OP.numCreatedBy AS numCreatedBy,
ISNULL(Div.numTerId,0) AS numOrgTerId,
0 AS numTaskId,
CASE WHEN ISNULL(OP.intReqPOApproved,0)=0 THEN 0 ELSE 1 END AS IsTaskClosed,
''<a onclick="openSalesOrder(''+CAST(OP.numOppId AS VARCHAR)+'')" href="javascript:void(0)">PO Approval</a>'' As TaskTypeName,
8 AS TaskType,
'''' AS OrigDescription,
''<b>For:</b> <a href="javascript:void(0)"  class="underLineHyperLink" onclick="openSalesOrder(''+CAST(OP.numOppId AS VARCHAR)+'')">''+OP.vcPOppName+''</a> &nbsp;<i onclick="ApproveRejectPORequistion(''+CAST(OP.numOppId AS VARCHAR)+'',1)"  class="fa fa-thumbs-up cursor"></i>  &nbsp;<i onclick="ApproveRejectPORequistion(''+CAST(OP.numOppId AS VARCHAR)+'',2)"  class="fa fa-thumbs-down cursor"></i> '' AS Description,
OP.intPEstimatedCloseDate AS dtDueDate,
OP.intPEstimatedCloseDate as DueDate,
0 As TotalProgress,
dbo.fn_GetContactName('+CAST(@numUserCntID AS VARCHAR(200))+') As numAssignedTo,
'+CAST(@numUserCntID AS VARCHAR(200))+' As numAssignToId,
'''' As Priority, -- Priority column   
'''' As Activity ,
0 As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(Div.numDivisionID AS VARCHAR)+''">''+Com.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating

 FROM  
 OpportunityMaster AS OP 
INNER JOIN 
	DivisionMaster Div                           
ON 
	OP.numDivisionId = Div.numDivisionID                       
INNER JOIN  
	CompanyInfo  Com                          
ON 
	Div.numCompanyID = Com.numCompanyId    
LEFT JOIN ListDetails AS listCompanyRating
 ON Com.numCompanyRating=listCompanyRating.NumlistItemID
WHERE
	OP.numDomainID = ' + Cast(@numDomainID as varchar(10))  + ' AND OP.tintOppType = 2 AND OP.tintOppStatus=1 
	AND 1 = (CASE WHEN '+CAST(@decReqPOMinValue AS VARCHAR(100))+'>0 AND Op.monPAmount>'+CAST(@decReqPOMinValue AS VARCHAR(100))+' THEN 1  WHEN '+CAST(@decReqPOMinValue AS VARCHAR(100))+'=0 THEN 1  ELSE 0 END )) AS Q
	WHERE  '+@OtherRegularSearch+' AND  (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) <= '''+Cast(@endDate as varchar(30))+''') 
'+@IndivRecordPaging+'
'
EXEC (@dynamicQuery)

END

------------------ 9 -  Price Margin Approval ----------------------------

SET @dynamicQuery =  ' INSERT INTO #tempRecords(
RecordCount,
RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
dtDueDate,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating
) SELECT COUNT(*) OVER() RecordCount,* FROM (SELECT
OP.numOppId AS RecordId,
OP.numCreatedBy AS numCreatedBy,
ISNULL(Div.numTerId,0) AS numOrgTerId,
0 AS numTaskId,
ISNULL(OP.bitReqPOApproved,0) AS IsTaskClosed,
''<a onclick="OpenMarginApproval(''+CAST(OP.numOppId AS VARCHAR)+'')" href="javascript:void(0)">Price Margin Approval</a>'' As TaskTypeName,
9 AS TaskType,
'''' AS OrigDescription,
''<b>For : </b> <a href="javascript:void(0)"  onclick="openSalesOrder(''+CAST(OP.numOppId AS VARCHAR)+'')">''+OP.vcPOppName+''</a> : '' +dbo.ItemUnitPriceApproval_CheckAndGetItemDetails(OP.numOppId,OP.numDomainId)+''  &nbsp;<i onclick="ApproveRejectPriceMarginApproval(''+CAST(OP.numOppId AS VARCHAR)+'',1)" class="fa fa-thumbs-up cursor"></i>  &nbsp;<i onclick="ApproveRejectPriceMarginApproval(''+CAST(OP.numOppId AS VARCHAR)+'',2)" class="fa fa-thumbs-down cursor"></i>''  AS Description,
OP.bintCreatedDate AS dtDueDate,
OP.bintCreatedDate as DueDate,
0 As TotalProgress,
dbo.fn_GetContactName('+CAST(@numUserCntID AS VARCHAR(200))+') As numAssignedTo,
'+CAST(@numUserCntID AS VARCHAR(200))+' As numAssignToId,
'''' As Priority, -- Priority column   
'''' As Activity ,
0 As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(Div.numDivisionID AS VARCHAR)+''">''+Com.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating

 FROM  
 OpportunityMaster AS OP 
INNER JOIN 
	DivisionMaster Div                           
ON 
	OP.numDivisionId = Div.numDivisionID                       
INNER JOIN  
	CompanyInfo  Com                          
ON 
	Div.numCompanyID = Com.numCompanyId    
LEFT JOIN ListDetails AS listCompanyRating
 ON Com.numCompanyRating=listCompanyRating.NumlistItemID
WHERE
	OP.numDomainID = ' + Cast(@numDomainID as varchar(10))  + ' AND OP.tintOppType = 1 AND (SELECT COUNT(OpportunityItems.numoppitemtCode) FROM OpportunityItems 
						WHERE OpportunityItems.bitItemPriceApprovalRequired=1 AND OpportunityItems.numOppId=OP.numOppId) >0
	AND '+CAST(@numUserCntID AS VARCHAR(100))+' IN (SELECT numUserID FROM UnitPriceApprover WHERE numDomainId= ' + Cast(@numDomainID as varchar(10))  + ' )) AS Q
	WHERE  '+@OtherRegularSearch+' AND  (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) <= '''+Cast(@endDate as varchar(30))+''') 
'+@IndivRecordPaging+'

'
EXEC (@dynamicQuery)

------------------------10 - A/R Invoice Due------------------------------------
IF(@bitARInvoiceDue=1 AND @numUserCntID IN(SELECT * FROM Split(@vchARInvoiceDue,',') WHERE Items<>''))
BEGIN
 DECLARE  @AuthoritativeSalesBizDocId  AS INTEGER
    SELECT @AuthoritativeSalesBizDocId = isnull(numAuthoritativeSales,0)
    FROM   AuthoritativeBizDocs
    WHERE  numDomainId = @numDomainId;
SET @dynamicQuery=  ' INSERT INTO #tempRecords(
RecordCount,
RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
dtDueDate,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating
) SELECT  COUNT(*) OVER() RecordCount,* FROM ( SELECT
OM.numOppId AS RecordId,
OM.numCreatedBy AS numCreatedBy,
ISNULL(DM.numTerId,0) AS numOrgTerId,
OB.numOppBizDocsId AS numTaskId,
0 AS IsTaskClosed,
''<a href="javascript:void(0)"  onclick="openBizDoc(''+CAST(OM.numOppId AS VARCHAR)+'',''+CAST(OB.numOppBizDocsId AS VARCHAR)+'')">A/R Invoice Due</a>'' As TaskTypeName,
10 AS TaskType,
'''' AS OrigDescription,
''<b>For : </b> <a href="javascript:void(0)" class="underLineHyperLink" onclick="openBizDoc(''+CAST(OM.numOppId AS VARCHAR)+'',''+CAST(OB.numOppBizDocsId AS VARCHAR)+'')">''+CAST(OB.[vcBizDocID] AS VARCHAR)+''</a> of <a href="javascript:void(0)" class="underLineHyperLink" onclick="openSalesOrder(''+CAST(OM.numOppId AS VARCHAR)+'')">''+CAST(OM.[vcPOppName] AS VARCHAR)+''</a> <b>Balance Due</b> <a href="javascript:void(0)" onclick="OpenAmtPaid(''+CAST(OM.numOppId AS VARCHAR)+'',''+CAST(OB.numOppBizDocsId AS VARCHAR)+'',''+CAST(OM.[numDivisionId] AS VARCHAR)+'')"   class="underLineHyperLink">'' + CAST(ISNULL(C.[varCurrSymbol],'''') AS VARCHAR) + '''' + CAST(CAST(isnull(OB.monDealAmount  - ISNULL(TablePayments.monPaidAmount,0),0) AS DECIMAL(18,2)) AS VARCHAR)+''</a>''  AS Description,
 CASE ISNULL(bitBillingTerms,0) 
                          WHEN 1 THEN DATEADD(dd,convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0)),OB.dtFromDate)
                          WHEN 0 THEN ob.[dtFromDate]
                        END AS dtDueDate,
 CASE ISNULL(bitBillingTerms,0) 
                          WHEN 1 THEN [dbo].[FormatedDateFromDate](DATEADD(dd,convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0)),OB.dtFromDate),
                                                                   1)
                          WHEN 0 THEN [dbo].[FormatedDateFromDate](ob.[dtFromDate],1)
                        END as DueDate,
0 As TotalProgress,
'''' As numAssignedTo,
0 As numAssignToId,
'''' As Priority, -- Priority column   
'''' As Activity ,
0 As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(DM.numDivisionID AS VARCHAR)+''">''+Com.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating

 FROM   [OpportunityMaster] OM INNER JOIN [DivisionMaster] DM ON OM.[numDivisionId] = DM.[numDivisionID]
				LEFT JOIN CompanyInfo  Com ON DM.numCompanyID = Com.numCompanyId    
				LEFT JOIN ListDetails AS listCompanyRating
 ON Com.numCompanyRating=listCompanyRating.NumlistItemID
               LEFT OUTER JOIN OpportunityBizDocs OB ON OB.numOppId = OM.numOppID
               LEFT OUTER JOIN [Currency] C ON OM.[numCurrencyID] = C.[numCurrencyID]
			   OUTER APPLY
			   (
					SELECT 
						SUM(monAmountPaid) AS monPaidAmount
					FROM
						DepositeDetails DD
					INNER JOIN
						DepositMaster DM
					ON
						DD.numDepositID=DM.numDepositId
					WHERE 
						numOppID = OM.numOppID
						AND numOppBizDocsID=OB.numOppBizDocsID
						AND CONVERT(DATE,DM.dtDepositDate) <= DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +','''+Cast(@endDate as varchar(100))+''')
				) TablePayments
        WHERE  OM.[tintOppType] = 1
               AND OM.[tintOppStatus] = 1
               AND om.[numDomainId] = ' + Cast(@numDomainId as varchar(10))  + '
               AND OB.[numBizDocId] = '+CAST(@AuthoritativeSalesBizDocId As VARCHAR)+'
               AND OB.bitAuthoritativeBizDocs=1
               AND ISNULL(OB.tintDeferred,0) <> 1
			   AND (OM.numAccountClass= 0 OR 0=0)
			   AND CONVERT(DATE,OB.[dtCreatedDate]) <= DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +','''+Cast(@endDate as varchar(100))+''')
               AND isnull(OB.monDealAmount
                            ,0) > 0  AND isnull(OB.monDealAmount * OM.fltExchangeRate
                        - ISNULL(TablePayments.monPaidAmount,0) * OM.fltExchangeRate,0) != 0
	) AS Q
	WHERE  '+@OtherRegularSearch+' AND  (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) <= '''+Cast(@endDate as varchar(30))+''') 
'+@IndivRecordPaging+'
'
EXEC (@dynamicQuery)
END
------------------------11 - A/P Bill Due------------------------------------
IF(@bitAPBillsDue=1 AND @numUserCntID IN(SELECT * FROM Split(@vchAPBillsDue,',') WHERE Items<>''))
BEGIN   
   DECLARE  @AuthoritativePurchaseBizDocId  AS INTEGER
    SELECT  @AuthoritativePurchaseBizDocId= isnull(numAuthoritativePurchase,0)
    FROM   AuthoritativeBizDocs
    WHERE  numDomainId =  @numDomainId
	

SET @dynamicQuery = '  INSERT INTO #tempRecords(
RecordCount,
RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
dtDueDate,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating
) SELECT  COUNT(*) OVER() RecordCount,* FROM (SELECT
OM.numOppId AS RecordId,
OM.numCreatedBy AS numCreatedBy,
ISNULL(DM.numTerId,0) AS numOrgTerId,
OB.numOppBizDocsId AS numTaskId,
0 AS IsTaskClosed,
''<a href="javascript:void(0)" onclick="openPurchaseBill(''+CAST(DM.[numDivisionID] As VARCHAR)+'')">A/P Bill Due<a/>'' As TaskTypeName,
11 AS TaskType,
'''' AS OrigDescription,
''<b>For : </b> <a href="javascript:void(0)" class="underLineHyperLink" onclick="openBizDoc(''+CAST(OM.numOppId AS VARCHAR)+'',''+CAST(OB.numOppBizDocsId AS VARCHAR)+'')">''+CAST(OB.[vcBizDocID] AS VARCHAR)+''</a> of <a href="javascript:void(0)" class="underLineHyperLink" onclick="openSalesOrder(''+CAST(OM.numOppId AS VARCHAR)+'')">''+CAST(OM.[vcPOppName] AS VARCHAR)+''</a> <b>Balance Due : </b> <a href="javascript:void(0)" onclick="openBizDoc(''+CAST(OM.numOppId AS VARCHAR)+'',''+CAST(OB.numOppBizDocsId AS VARCHAR)+'')"   class="underLineHyperLink">'' + CAST(ISNULL(C.[varCurrSymbol],'''') AS VARCHAR) + '''' + CAST(CAST(isnull((OB.monDealAmount * OM.fltExchangeRate)  - (ISNULL(TablePayment.monPaidAmount,0)* OM.fltExchangeRate),0) AS DECIMAL(18,2))  AS VARCHAR)+''</a>''  AS Description,
 CASE ISNULL(bitBillingTerms,0) 
                          WHEN 1 THEN DATEADD(dd,convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0)),OB.dtFromDate)
                          WHEN 0 THEN ob.[dtFromDate]
                        END AS dtDueDate,
 CASE ISNULL(bitBillingTerms,0) 
                          WHEN 1 THEN [dbo].[FormatedDateFromDate](DATEADD(dd,convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0)),OB.dtFromDate),
                                                                   1)
                          WHEN 0 THEN [dbo].[FormatedDateFromDate](ob.[dtFromDate],1)
                        END as DueDate,
0 As TotalProgress,
'''' As numAssignedTo,
0 As numAssignToId,
'''' As Priority, -- Priority column   
'''' As Activity ,
0 As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(DM.numDivisionID AS VARCHAR)+''">''+Com.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating

   FROM   [OpportunityMaster] OM
               INNER JOIN [DivisionMaster] DM
                 ON OM.[numDivisionId] = DM.[numDivisionID]
				 LEFT JOIN CompanyInfo  Com ON DM.numCompanyID = Com.numCompanyId    
				LEFT JOIN ListDetails AS listCompanyRating
 ON Com.numCompanyRating=listCompanyRating.NumlistItemID
               LEFT OUTER JOIN OpportunityBizDocs OB
                 ON OB.numOppId = OM.numOppID
               LEFT OUTER JOIN [Currency] C
                 ON OM.[numCurrencyID] = C.[numCurrencyID]
				 OUTER APPLY
				(
					SELECT
						SUm(BPD.monAmount) monPaidAmount
					FROM	
						BillPaymentDetails BPD
					INNER JOIN
						BillPaymentHeader BPH
					ON
						BPD.numBillPaymentID=BPH.numBillPaymentID
					WHERE
						BPD.numOppBizDocsID = OB.numOppBizDocsId
						AND CAST(BPH.dtPaymentDate AS DATE) <= DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +','''+Cast(@endDate as varchar(100))+''')
				) TablePayment
        WHERE  OM.[tintOppType] = 2
               AND OM.[tintOppStatus] = 1
               AND om.[numDomainId] = 1
               AND OB.[numBizDocId] = '+CAST(@AuthoritativePurchaseBizDocId AS VARCHAR)+' 
               AND OB.bitAuthoritativeBizDocs=1
               AND ISNULL(OB.tintDeferred,0) <> 1
			   AND (OM.numAccountClass= 0 OR 0=0)
			   AND CONVERT(DATE,OB.[dtCreatedDate]) <= DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +','''+Cast(@endDate as varchar(100))+''')
               AND  isnull(OB.monDealAmount * OM.fltExchangeRate,0) > 0 
			   AND isnull(OB.monDealAmount * OM.fltExchangeRate
                        - ISNULL(TablePayment.monPaidAmount,0) * OM.fltExchangeRate,0) > 0
	) AS Q
	WHERE  '+@OtherRegularSearch+' AND  (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) <= '''+Cast(@endDate as varchar(30))+''') 
'+@IndivRecordPaging+'
'

EXEC (@dynamicQuery)
END

END

SET @FormattedItems =''
SET @FormattedItems=' UPDATE #tempRecords SET DueDate = CASE WHEN convert(varchar(11),DueDate)=convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getdate())) then ''<b><font color=red>Today</font></b>''
WHEN convert(varchar(11),DueDate)=convert(varchar(11),DateAdd(day,-1,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getdate()))) then ''<b><font color=purple>Yesterday</font></b>''
WHEN convert(varchar(11),DueDate)=convert(varchar(11),DateAdd(day,1,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getdate()))) then ''<b><font color=orange>Tommorow</font></b>''
ELSE  dbo.FormatedDateFromDate(DueDate,'+ CONVERT(VARCHAR(10),@numDomainId)+') END '

 EXEC (@FormattedItems)
 DECLARE @strSql3 VARCHAR(MAX)=''
 IF LEN(@columnName) > 0
BEGIN
	SET @strSql3= CONCAT(@strSql3,' order by ',CASE WHEN @columnName = 'Action-Item Participants' THEN CONCAT('[','Action-Item Participants',']') ELSE @columnName END,' ',@columnSortOrder)
END
ELSE
BEGIN
	SET @strSql3= CONCAT(@strSql3,' ORDER BY dtDueDate DESC  ')
END
PRINT @strSql3
SET @TotalRecordCount=(SELECT ISNULL(SUM(DISTINCT RecordCount),0) FROM #tempRecords)


SET @dynamicQuery = 'SELECT '+CAST(@TotalRecordCount AS varchar(400))+' AS TotalRecords,T.*
 FROM #tempRecords T WHERE 1=1 '+@strSql3


EXEC(@dynamicQuery)
DECLARE @TempColumns TABLE
	(
        vcFieldName VARCHAR(200)
        ,vcDbColumnName VARCHAR(200)
        ,vcOrigDbColumnName VARCHAR(200)
        ,bitAllowSorting BIT
        ,numFieldId INT
        ,bitAllowEdit BIT
        ,bitCustomField BIT
        ,vcAssociatedControlType VARCHAR(50)
        ,vcListItemType  VARCHAR(200)
        ,numListID INT
        ,ListRelID INT
        ,vcLookBackTableName VARCHAR(200)
        ,bitAllowFiltering BIT
        ,vcFieldDataType  VARCHAR(20)
		,intColumnWidth INT
		,bitClosedColumn BIT
		,bitFieldMessage BIT DEFAULT 0
		,vcFieldMessage VARCHAR(500)
		,bitIsRequired BIT DEFAULT 0
		,bitIsNumeric BIT DEFAULT 0
		,bitIsAlphaNumeric BIT DEFAULT 0
		,bitIsEmail BIT DEFAULT 0
		,bitIsLengthValidation  BIT DEFAULT 0
		,intMaxLength INT DEFAULT 0
		,intMinLength INT DEFAULT 0
	)



		declare @Nocolumns as tinyint         
set @Nocolumns=0       
         
Select @Nocolumns=isnull(count(*),0) from View_DynamicColumns where numFormId=43 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 
  
  
if @Nocolumns > 0            
begin      
INSERT INTO #tempForm
select tintRow+1 as tintOrder,vcDbColumnName,vcFieldDataType,vcOrigDbColumnName,bitAllowFiltering,ISNULL(vcCultureFieldName,vcFieldName),                  
 vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName                                                 
,bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth
 FROM View_DynamicColumns 
 where numFormId=43 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 
  AND ISNULL(bitSettingField,0)=1 AND ISNULL(bitCustom,0)=0
  order by tintOrder asc  
  
end            
else            
begin 

	--Check if Master Form Configuration is created by administrator if NoColumns=0
	DECLARE @IsMasterConfAvailable BIT = 0
	DECLARE @numUserGroup NUMERIC(18,0)

	SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

	IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = 43 AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
	BEGIN
		SET @IsMasterConfAvailable = 1
	END

	--If MasterConfiguration is available then load it otherwise load default columns
	IF @IsMasterConfAvailable = 1
	BEGIN
		INSERT INTO DycFormConfigurationDetails 
		(
			numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
		)
		SELECT 
			43,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,NULL,1,0,intColumnWidth
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=43 AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0

		INSERT INTO #tempForm
		SELECT 
			(intRowNum + 1) as tintOrder, vcDbColumnName,vcFieldDataType,vcOrigDbColumnName,bitAllowFiltering, ISNULL(vcCultureFieldName,vcFieldName), vcAssociatedControlType,
			vcListItemType,numListID,vcLookBackTableName, bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=43 AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
	ORDER BY 
		tintOrder ASC  
	END
	ELSE                                        
	BEGIN
		 INSERT INTO #tempForm
		select tintOrder,vcDbColumnName,vcFieldDataType
		,vcOrigDbColumnName,bitAllowFiltering,ISNULL(vcCultureFieldName,vcFieldName),                  
		 vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName                                                 
		,bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
		bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
		intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth
		 FROM View_DynamicDefaultColumns
		 where numFormId=43 and bitDefault=1 AND ISNULL(bitSettingField,0)=1 AND numDomainID=@numDomainID
		order by tintOrder asc  
	END 
enD

DELETE FROM #tempForm WHERE vcOrigDbColumnName IN('numActivity','numStatus','intTotalProgress','textDetails','vcCompanyName','bitTask','numAssignedTo','FormattedDate')
--UPDATE #tempForm SET bitCustomField=1
INSERT INTO #tempForm
		(
			tintOrder
			,vcFieldName
			,vcDbColumnName
			,vcOrigDbColumnName
			,bitAllowSorting
			,numFieldId
			,bitAllowEdit
			,bitCustomField
			,vcAssociatedControlType
			,vcListItemType
			,numListID
			,ListRelID
			,vcLookBackTableName
			,bitAllowFiltering
			,vcFieldDataType
			
		)
		VALUES
		(-9,'Type','TaskTypeName','TaskTypeName',0,1,0,0,'SelectBox','',0,0,'T',1,'V')
		,(-8,'Descriptions & Comments','Description','Description',0,184,1,0,'TextArea','',0,0,'T',1,'V')
		,(-7,'Due Date','DueDate','DueDate',1,3,0,0,'DateField','',0,0,'T',1,'V')
		,(-6,'Total Progress','TotalProgress','TotalProgress',1,4,0,0,'TextBox','',0,0,'T',0,'V')
		,(-5,'Assigned to','numAssignedTo','numAssignedTo',0,5,0,0,'SelectBox','',0,0,'T',1,'V')
		,(-4,'Priority','Priority','Priority',0,183,1,0,'SelectBox','',0,0,'T',1,'V')
		,(-3,'Activity','Activity','Activity',0,182,1,0,'SelectBox','',0,0,'T',1,'V')
		,(-2,'Organization (Rating)','OrgName','OrgName',0,8,0,0,'TextBox','',0,0,'T',1,'V')
UPDATE
		TC
	SET
		TC.intColumnWidth = ISNULL(DFCD.intColumnWidth,0)
	FROM
		#tempForm TC
	INNER JOIN
		DycFormConfigurationDetails DFCD
	ON
		TC.numFieldId = DFCD.numFieldId
	WHERE
		DFCD.numDomainId = @numDomainID
		AND DFCD.numUserCntID = @numUserCntID
		AND DFCD.numFormId = 43
		AND DFCD.tintPageType = 1 


	SELECT * FROM #tempForm order by tintOrder asc  
END
GO
