/******************************************************************
Project: Release 7.9 Date: 12.AUGUST.2017
Comments: ALTER SCRIPTS
*******************************************************************/

/******************   SANDEEP  *****************/

USE [Production.2014]
GO

/****** Object:  Table [dbo].[ElasticSearchLastReindex]    Script Date: 10-Aug-17 12:25:10 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ElasticSearchLastReindex](
	[ID] [int] NOT NULL,
	[LastReindexDate] [date] NOT NULL
) ON [PRIMARY]

GO


INSERT INTO ElasticSearchLastReindex (ID,LastReindexDate) VALUES (1,GETDATE())

UPDATE DycFormField_Mapping SET numFieldID=190 WHERE numFormID=86 AND numFieldID=234
UPDATE DycFormConfigurationDetails SET numFieldID=190 WHERE numFormID=86 AND numFieldID=234