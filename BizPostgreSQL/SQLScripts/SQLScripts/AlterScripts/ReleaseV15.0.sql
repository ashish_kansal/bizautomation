/******************************************************************
Project: Release 15.0 Date: 08.MAR.2021
Comments: ALTER SCRIPTS
*******************************************************************/

/******************************************** PRASHANT *********************************************/

INSERT INTO PageElementMaster
(
	numElementID,vcElementName,vcUserControlPath,vcTagName,bitCustomization,bitAdd,bitDelete,numSiteID
)
VALUES
(
	(SELECT MAX(numElementID) FROM PageElementMaster)+1,'OrderOverview','~/UserControls/OrderOverview.ascx','{#OrderOverview#}',1,0,0,0
)
INSERT INTO PageElementAttributes
(
	numElementID,vcAttributeName,vcControlType,bitEditor
)
VALUES
(
	(SELECT MAX(numElementID) FROM PageElementMaster)+1,'Html Customize','HtmlEditor',1
)


/******************************************** SANDEEP *********************************************/

ALTER TABLE ItemDetails ADD bitUseInDynamicSKU BIT DEFAULT 0

-- ALREADY UPDATED ON PRODUCTION

ALTER TABLE DivisionMaster ADD vcBuiltWithJson NVARCHAR(MAX)

UPDATE DynamicFormMaster SET tintPageType=1 WHERE numBizFormModuleID=4 AND numFormId IN (7,8,26,58,118,119,120,121,129)
UPDATE DynamicFormMaster SET tintPageType=3 WHERE numBizFormModuleID=4 AND numFormId IN (105,106,107,108,113)