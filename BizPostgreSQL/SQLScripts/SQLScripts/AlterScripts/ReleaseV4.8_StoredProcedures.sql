/******************************************************************
Project: Release 4.8 Date: 29.July.2015
Comments: STORED PROCEDURES
*******************************************************************/

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='DeleteComissionDetails')
DROP PROCEDURE DeleteComissionDetails
GO
CREATE PROCEDURE DeleteComissionDetails
    (     
      @numDomainId NUMERIC,
      @numOppBizDocID numeric(9)      
    )

AS BEGIN
BEGIN TRY
BEGIN TRANSACTION
	create table #TempComJournal
	(numJournalId numeric(9));

	insert into #TempComJournal

	select GJD.numJournalId from General_Journal_Details GJD where GJD.numCommissionID in 
	(select BC.numComissionID from BizDocComission BC where BC.numOppBizDocId=@numOppBizDocId)

	delete from General_Journal_Details  where numJournalId in (select numJournalId from #TempComJournal);
	delete from General_Journal_Header  where numJOurnal_Id in (select numJournalId from #TempComJournal);

	DELETE FROM BizDocComission WHERE numOppBizDocId=@numOppBizDocId;


	UPDATE WHDL
		SET WHDL.numQty = (CASE WHEN ISNULL(Item.bitLotNo,0) = 1 THEN (ISNULL(WHDL.numQty,0) + ISNULL(OWSI.numQty,0)) ELSE 1  END)
	FROM
		WareHouseItmsDTL WHDL
	INNER JOIN
		OppWarehouseSerializedItem OWSI
	ON
		WHDL.numWareHouseItmsDTLID = OWSI.numWareHouseItmsDTLID
	INNER JOIN
		WarehouseItems WI
	ON
		WHDL.numWarehouseItemID = WI.numWarehouseItemID
	INNER JOIN
		Item
	ON
		WI.numItemID = Item.numItemCode
	WHERE 
		OWSI.numOppBizDocsId=@numOppBizDocId

	DELETE FROM OppWarehouseSerializedItem WHERE numOppBizDocsId=@numOppBizDocId;
COMMIT TRANSACTION
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH  
end

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetOrderShipReceiveStatus')
DROP FUNCTION GetOrderShipReceiveStatus
GO
CREATE FUNCTION [dbo].[GetOrderShipReceiveStatus] 
(
      @numOppID AS NUMERIC(18,0),
	  @tintOppType AS NUMERIC(18,0),
	  @tintshipped AS TINYINT
)
RETURNS VARCHAR(100)
AS 
BEGIN	
	DECLARE @vcShippedReceivedStatus VARCHAR(100) = ''

	DECLARE @TotalQty AS INT  = 0

	IF @tintOppType = 1 --SALES ORDER
	BEGIN
		DECLARE @numQtyShipped AS INT = 0

		SELECT @TotalQty = ISNULL((SELECT SUM(numUnitHour) FROM OpportunityItems INNER JOIN Item ON OpportunityItems.numItemCode = Item.numItemCode  WHERE UPPER(Item.charItemType) = 'P' AND ISNULL(bitDropship,0) = 0 AND numOppId = @numOppID),0)

		-- IF ORDER IS CLOSED WITHOUT FULFILLMENT MARK ALL QTY IS FULFILLED
		IF ISNULL(@tintshipped,0) = 1
		BEGIN
			SET @numQtyShipped = @TotalQty
		END
		ELSE
		BEGIN
			SELECT @numQtyShipped = ISNULL((SELECT SUM(numQtyShipped) FROM OpportunityItems INNER JOIN Item ON OpportunityItems.numItemCode = Item.numItemCode  WHERE UPPER(Item.charItemType) = 'P' AND ISNULL(bitDropship,0) = 0 AND numOppId = @numOppID),0)
		END

		SELECT @vcShippedReceivedStatus =(CASE WHEN  @TotalQty = @numQtyShipped THEN '<b><font color="green">' ELSE '<b><font color="red">' END) + CONCAT(CAST(@TotalQty AS INT),' / ',CAST(@numQtyShipped AS INT)) + '</font></b>'
	END
	ELSE IF @tintOppType = 2 --PURCHASE ORDER
	BEGIN
		DECLARE @numQtyReceived AS INT = 0

		SELECT @TotalQty = ISNULL((SELECT SUM(numUnitHour) FROM OpportunityItems INNER JOIN Item ON OpportunityItems.numItemCode = Item.numItemCode  WHERE UPPER(Item.charItemType) = 'P' AND ISNULL(bitDropship,0) = 0 AND numOppId = @numOppID),0)
		
		-- IF ORDER IS CLOSED WITHOUT FULFILLMENT MARK ALL QTY IS FULFILLED
		IF ISNULL(@tintshipped,0) = 1
		BEGIN
			SET @numQtyReceived = @TotalQty
		END
		ELSE
		BEGIN
			SELECT @numQtyReceived = ISNULL((SELECT SUM(numUnitHourReceived) FROM OpportunityItems INNER JOIN Item ON OpportunityItems.numItemCode = Item.numItemCode  WHERE UPPER(Item.charItemType) = 'P' AND ISNULL(bitDropship,0) = 0 AND numOppId = @numOppID),0)
		END

		SELECT @vcShippedReceivedStatus =(CASE WHEN  @TotalQty = @numQtyReceived THEN '<b><font color="green">' ELSE '<b><font color="red">' END) + CONCAT(CAST(@TotalQty AS INT),' / ',CAST(@numQtyReceived AS INT)) + '</font></b>'
	END

	RETURN @vcShippedReceivedStatus
END


GO
/****** Object:  StoredProcedure [dbo].[USP_AddUpdateWareHouseForItems]    Script Date: 07/26/2008 16:14:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_AddUpdateWareHouseForItems')
DROP PROCEDURE USP_AddUpdateWareHouseForItems
GO
CREATE PROCEDURE [dbo].[USP_AddUpdateWareHouseForItems]  
@numItemCode as numeric(9)=0,  
@numWareHouseID as numeric(9)=0,
@numWareHouseItemID as numeric(9)=0 OUTPUT,
@vcLocation as varchar(250)='',
@monWListPrice as money =0,
@numOnHand as numeric(18,3)=0,
@numReorder as numeric(18,3)=0,
@vcWHSKU as varchar(100)='',
@vcBarCode as varchar(50)='',
@numDomainID AS NUMERIC(9),
@strFieldList as TEXT='',
@vcSerialNo as varchar(100)='',
@vcComments as varchar(1000)='',
@numQty as numeric(18)=0,
@byteMode as tinyint=0,
@numWareHouseItmsDTLID as numeric(18)=0,
@numUserCntID AS NUMERIC(9)=0,
@dtAdjustmentDate AS DATETIME=NULL,
@numWLocationID NUMERIC(9)=0
as  

DECLARE @numDomain AS NUMERIC(18,0)
SET @numDomain = @numDomainID
Declare @bitLotNo as bit;SET @bitLotNo=0  
Declare @bitSerialized as bit;SET @bitSerialized=0  

select @bitLotNo=isnull(Item.bitLotNo,0),@bitSerialized=isnull(Item.bitSerialized,0)
from item where numItemCode=@numItemCode and numDomainID=@numDomainID

DECLARE @vcDescription AS VARCHAR(100)
IF @byteMode=0 or @byteMode=1 or @byteMode=2 or @byteMode=5
BEGIN
--Insert/Update WareHouseItems
IF @byteMode=0 or @byteMode=1
BEGIN
IF @numWareHouseItemID>0
  BEGIN
		UPDATE WareHouseItems  SET numWareHouseID=@numWareHouseID,                                                              
				--numOnHand=(Case When @bitLotNo=1 or @bitSerialized=1 then numOnHand else @numOnHand end),
				numReorder=@numReorder,monWListPrice=@monWListPrice,vcLocation=@vcLocation,numWLocationID = @numWLocationID
				,vcWHSKU=@vcWHSKU,vcBarCode=@vcBarCode ,dtModified=GETDATE()   
		WHERE numItemID=@numItemCode and numDomainID=@numDomainID and numWareHouseItemID=@numWareHouseItemID
		
		IF NOT EXISTS(SELECT numItemCode FROM dbo.OpportunityItems WHERE numItemCode = @numItemCode)
			BEGIN
				IF ((SELECT ISNULL(numOnHand,0) FROM WareHouseItems where numItemID = @numItemCode AND numDomainId = @numDomainID and numWareHouseItemID = @numWareHouseItemID) = 0) --AND ((SELECT ISNULL(monAverageCost,0) FROM Item  WHERE numItemCode = @numItemCode AND numDomainId=@numDomainID ) = 0))
					BEGIN
						UPDATE WareHouseItems SET numOnHand = @numOnHand WHERE numItemID = @numItemCode AND numDomainID = @numDomainID AND numWareHouseItemID = @numWareHouseItemID		
					END
			END
		
		SET @vcDescription='UPDATE WareHouse'
  END
ELSE
  BEGIN
	 insert into WareHouseItems (numItemID, numWareHouseID,numOnHand,numReorder,monWListPrice,vcLocation,vcWHSKU,vcBarCode,numDomainID,dtModified,numWLocationID)  
			values(@numItemCode,@numWareHouseID,(Case When @bitLotNo=1 or @bitSerialized=1 then 0 else @numOnHand end),@numReorder,@monWListPrice,@vcLocation,@vcWHSKU,@vcBarCode,@numDomainID,GETDATE(),@numWLocationID)  

	 --PRINT @numWareHouseItemID
	 SET @numWareHouseItemID = @@identity
	 --PRINT @numWareHouseItemID
	 
		SET @vcDescription='INSERT WareHouse'
  END
END

--Insert/Update WareHouseItmsDTL
--DEclare @numWareHouseItmsDTLID as numeric(18);SET @numWareHouseItmsDTLID=0
DECLARE @OldQty INT;SET @OldQty=0

IF @byteMode=0 or @byteMode=2 or @byteMode=5
BEGIN

IF @bitLotNo=1 or @bitSerialized=1
BEGIN
	IF @bitSerialized=1
		SET @numQty=1
	
    IF @byteMode=0
	BEGIN
		select top 1 @numWareHouseItmsDTLID=numWareHouseItmsDTLID,@OldQty=numQty from WareHouseItmsDTL where numWareHouseItemID=@numWareHouseItemID and
			vcSerialNo=@vcSerialNo and numQty > 0
	END
	ELSE IF @numWareHouseItmsDTLID>0
	BEGIN
		select top 1 @OldQty=numQty from WareHouseItmsDTL where numWareHouseItmsDTLID=@numWareHouseItmsDTLID
	END

	IF @numWareHouseItmsDTLID>0
		BEGIN
				UPDATE WareHouseItmsDTL SET vcComments = @vcComments,numQty=@numQty,vcSerialNo=@vcSerialNo
                where numWareHouseItmsDTLID=@numWareHouseItmsDTLID and numWareHouseItemID=@numWareHouseItemID
                
                SET @vcDescription=CASE WHEN @byteMode=5 THEN 'Inventory Adjustment (Update Lot/Serial#) : ' ELSE 'UPDATE Lot/Serial# : ' END + @vcSerialNo 
		END
	ELSE
		BEGIN
			   INSERT INTO WareHouseItmsDTL(numWareHouseItemID,vcSerialNo,vcComments,numQty)  
			   Values (@numWareHouseItemID,@vcSerialNo,@vcComments,@numQty)
				
			   set @numWareHouseItmsDTLID=@@identity

               SET @vcDescription=CASE WHEN @byteMode=5 THEN 'Inventory Adjustment (Insert Lot/Serial#) : ' ELSE 'INSERT Lot/Serial# : ' END + @vcSerialNo 
		END

 	   update WareHouseItems SET numOnHand=numOnHand + (@numQty - @OldQty) ,dtModified=GETDATE() 
	   where numWareHouseItemID = @numWareHouseItemID 
	   AND [numDomainID] = @numDomainID
	   AND (numOnHand + (@numQty - @OldQty))>=0
END
END

IF DATALENGTH(@strFieldList)>2
BEGIN
	--Insert Custom Fields base on WareHouseItems/WareHouseItmsDTL 
	declare @hDoc as int     
	EXEC sp_xml_preparedocument @hDoc OUTPUT, @strFieldList                                                                        

	declare  @rows as integer                                        
	                                         
	SELECT @rows=count(*) FROM OPENXML (@hDoc,'/NewDataSet/CusFlds',2)                                                  
					WITH (Fld_ID numeric(9),Fld_Value varchar(100))                                        
	                                                                           

	if @rows>0                                        
	begin  
	  Create table #tempTable (ID INT IDENTITY PRIMARY KEY,Fld_ID numeric(9),Fld_Value varchar(100))   
	                                      
	  insert into #tempTable (Fld_ID,Fld_Value)                                        
	  SELECT * FROM OPENXML (@hDoc,'/NewDataSet/CusFlds',2)                                                  
	  WITH (Fld_ID numeric(9),Fld_Value varchar(100))                                           
	                                         
	  delete CFW_Fld_Values_Serialized_Items from CFW_Fld_Values_Serialized_Items C                                        
	  inner join #tempTable T on C.Fld_ID=T.Fld_ID 
		where C.RecId=(Case when @bitLotNo=1 or @bitSerialized=1 then @numWareHouseItmsDTLID else @numWareHouseItemID end) and bitSerialized=(Case when @bitLotNo=1 or @bitSerialized=1 then 1 else 0 end)                                       
	      
	  insert into CFW_Fld_Values_Serialized_Items(Fld_ID,Fld_Value,RecId,bitSerialized)                                                  
	  select Fld_ID,isnull(Fld_Value,'') as Fld_Value,(Case when @bitLotNo=1 or @bitSerialized=1 then @numWareHouseItmsDTLID else @numWareHouseItemID end),(Case when @bitLotNo=1 or @bitSerialized=1 then 1 else 0 end) from #tempTable 

	  drop table #tempTable                                        
	 end  

	 EXEC sp_xml_removedocument @hDoc 
END	  
 
 EXEC dbo.USP_ManageWareHouseItems_Tracking
	@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
	@numReferenceID = @numItemCode, --  numeric(9, 0)
	@tintRefType = 1, --  tinyint
	@vcDescription = @vcDescription, --  varchar(100)
	@numModifiedBy = @numUserCntID,
	@ClientTimeZoneOffset = 0,
	@dtRecordDate = @dtAdjustmentDate,
	@numDomainID = @numDomain 
END

ELSE IF @byteMode=3
BEGIN

	IF exists (SELECT * FROM [OpportunityMaster] OM INNER JOIN [OpportunityItems] OI ON OM.[numOppId] = OI.[numOppId] WHERE OM.[numDomainId]=@numDomainID AND [numWarehouseItmsID]=@numWareHouseItemID)
	BEGIN
		raiserror('OpportunityItems_Depend',16,1);
		RETURN ;
	END

     IF (SELECT COUNT(*) FROM OpportunityKitItems WHERE numWareHouseItemId=@numWareHouseItemID) >0
 	 BEGIN
	  	RAISERROR ('OpportunityKitItems_Depend',16,1);
        RETURN
	  END
	
	IF (SELECT COUNT(*) FROM ItemDetails WHERE numWareHouseItemId=@numWareHouseItemID) >0
 	 BEGIN
	  	RAISERROR ('KitItems_Depend',16,1);
        RETURN
	  END	
	    
IF @bitLotNo=1 or @bitSerialized=1
BEGIN
	DELETE from CFW_Fld_Values_Serialized_Items where RecId in (select numWareHouseItmsDTLID from WareHouseItmsDTL where numWareHouseItemID=@numWareHouseItemID)
			and bitSerialized=(Case when @bitLotNo=1 or @bitSerialized=1 then 1 else 0 end) 
END
ELSE
BEGIN
	DELETE from CFW_Fld_Values_Serialized_Items where RecId=@numWareHouseItemID
			and bitSerialized=(Case when @bitLotNo=1 or @bitSerialized=1 then 1 else 0 end) 
END

DELETE from WareHouseItmsDTL where numWareHouseItemID=@numWareHouseItemID 

DELETE FROM WareHouseItems_Tracking WHERE numWareHouseItemID=@numWareHouseItemID and numDomainID=@numDomainID

DELETE from WareHouseItems where numWareHouseItemID=@numWareHouseItemID and numDomainID=@numDomainID

END

ELSE IF @byteMode=4
BEGIN
IF @bitLotNo=1 or @bitSerialized=1
BEGIN
	DELETE from CFW_Fld_Values_Serialized_Items where RecId=@numWareHouseItmsDTLID
			and bitSerialized=(Case when @bitLotNo=1 or @bitSerialized=1 then 1 else 0 end) 
END

update WHI SET numOnHand=WHI.numOnHand - WHID.numQty,dtModified=GETDATE()
from WareHouseItems WHI join WareHouseItmsDTL WHID on WHI.numWareHouseItemID=WHID.numWareHouseItemID 
where WHID.numWareHouseItmsDTLID=@numWareHouseItmsDTLID 
AND WHI.numDomainID = @numDomainID
AND (WHI.numOnHand - WHID.numQty)>=0

SELECT @numWareHouseItemID=WHI.numWareHouseItemID,@numItemCode=numItemID 
from WareHouseItems WHI join WareHouseItmsDTL WHID on WHI.numWareHouseItemID=WHID.numWareHouseItemID 
where WHID.numWareHouseItmsDTLID=@numWareHouseItmsDTLID 
AND WHI.numDomainID = @numDomainID


EXEC dbo.USP_ManageWareHouseItems_Tracking
	@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
	@numReferenceID = @numItemCode, --  numeric(9, 0)
	@tintRefType = 1, --  tinyint
	@vcDescription = 'DELETE Lot/Serial#', --  varchar(100)
	@numModifiedBy = @numUserCntID,
	@ClientTimeZoneOffset = 0,
	@dtRecordDate = NULL,
	@numDomainID = @numDomain

DELETE from WareHouseItmsDTL where numWareHouseItmsDTLID=@numWareHouseItmsDTLID


END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_AutoAssign_OppSerializedItem')
DROP PROCEDURE USP_AutoAssign_OppSerializedItem
GO
CREATE PROCEDURE [dbo].[USP_AutoAssign_OppSerializedItem]  
    @numDomainID as numeric(9)=0,  
    @numOppID as numeric(9)=0,  
    @numOppBizDocsId as numeric(9)=0
as  

IF (select isnull(bitAutoSerialNoAssign,0) from domain where @numDomainID=numDomainID)=1
BEGIN
Create table #tempSerializedItem (ID numeric(18) IDENTITY(1,1) NOT NULL,numItemCode numeric(9),numOppItemtCode numeric(9),numWarehouseItmsID numeric(9),bitLotNo bit,bitSerialized bit,numUnitHour numeric(18,2))

insert into #tempSerializedItem 
select I.numItemCode,numOppItemtCode,numWarehouseItmsID,I.bitLotNo,I.bitSerialized,oppItems.numUnitHour from OpportunityItems oppItems 
join Item I on oppItems.numItemCode=I.numItemCode where 
I.numDomainId=@numDomainID and oppItems.numOppId=@numOppId and (isnull(I.bitLotNo,0)=1 or isnull(I.bitSerialized,0)=1)

IF (select count(*) from #tempSerializedItem)>0
BEGIN

DECLARE  @maxID NUMERIC(9),@minID NUMERIC(9)
Declare @numItemCode numeric(9),@numOppItemtCode numeric(9),@numWarehouseItmsID numeric(9),@bitLotNo bit,@bitSerialized bit,@numUnitHour numeric(18,2)

SELECT @maxID = max(ID),@minID = min(ID) FROM  #tempSerializedItem 

Create table #tempWareHouseItmsDTL (                                                                    
ID numeric(18) IDENTITY(1,1) NOT NULL,vcSerialNo VARCHAR(100),numQty [numeric](18, 0),numWareHouseItmsDTLID [numeric](18, 0)                                             
 )   

WHILE @minID <= @maxID
BEGIN

SELECT @numItemCode=numItemCode,@numOppItemtCode=numOppItemtCode,
	   @numWarehouseItmsID=numWarehouseItmsID,@bitLotNo=bitLotNo,
		@bitSerialized=bitSerialized,@numUnitHour=numUnitHour FROM #tempSerializedItem WHERE ID = @minID  

INSERT INTO #tempWareHouseItmsDTL 
(
	vcSerialNo,numWarehouseItmsDTLID,numQty
)  
SELECT 
	vcSerialNo,numWareHouseItmsDTLID,ISNULL(numQty,0) AS TotalQty
FROM   
	WareHouseItmsDTL   
WHERE 
	ISNULL(numQty,0) > 0 
	AND numWareHouseItemID=@numWarehouseItmsID  
    and numWareHouseItmsDTLID NOT IN (SELECT numWarehouseItmsDTLID FROM OppWarehouseSerializedItem WHERE numOppID=@numOppID AND numWarehouseItmsID=@numWarehouseItmsID AND numOppBizDocsId=@numOppBizDocsId) 
ORDER BY 
	TotalQty desc

IF ((Select sum(numQty) from #tempWareHouseItmsDTL)>=@numUnitHour)
BEGIN
DECLARE  @maxWareID NUMERIC(9),@minWareID NUMERIC(9)
DECLARE  @numWarehouseItmsDTLID NUMERIC(9),@numQty NUMERIC(9),@numUseQty NUMERIC(9)
	
SELECT @maxWareID = max(ID),@minWareID = min(ID) FROM  #tempWareHouseItmsDTL 

WHILE (@numUnitHour>0 and @minWareID <= @maxWareID)
BEGIN
	SELECT @numWarehouseItmsDTLID = numWarehouseItmsDTLID,@numQty = numQty FROM  #tempWareHouseItmsDTL  where ID=@minWareID

	IF @numUnitHour >= @numQty
		BEGIN
			SET @numUseQty=@numQty
	
			SET @numUnitHour=@numUnitHour-@numQty
		END
	else IF @numUnitHour < @numQty
		BEGIN
			SET @numUseQty=@numUnitHour

			SET @numUnitHour=0
		END
		
	Insert into OppWarehouseSerializedItem(numWarehouseItmsDTLID,numOppID,numOppItemID,numWarehouseItmsID,numQty,numOppBizDocsId)                
		SELECT @numWarehouseItmsDTLID,@numOppID,@numOppItemTcode,@numWarehouseItmsID,@numUseQty,@numOppBizDocsId                                            



SET @minWareID=@minWareID + 1
END

END

Truncate table #tempWareHouseItmsDTL

SET @minID=@minID + 1
END

drop table #tempWareHouseItmsDTL
END

drop table #tempSerializedItem
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p' AND NAME ='USP_BizRecurrence_Order')
DROP PROCEDURE USP_BizRecurrence_Order
GO
  
Create PROCEDURE [dbo].[USP_BizRecurrence_Order]  
	@numRecConfigID NUMERIC(18,0), 
	@numDomainID NUMERIC(18,0),
	@numUserCntID NUMERIC(18,0),
	@numOppID NUMERIC(18,0),
	@numRecurOppID NUMERIC(18,0) OUT,
	@numFrequency INTEGER,
	@dtEndDate DATE,
	@Date AS DATE
AS  
BEGIN  
BEGIN TRANSACTION;

BEGIN TRY

--DECLARE @Date AS DATE = GETDATE()

DECLARE @numDivisionID AS NUMERIC(18,0)
DECLARE @numOppType AS TINYINT
DECLARE @tintOppStatus AS TINYINT
DECLARE @DealStatus AS TINYINT
DECLARE @numStatus AS NUMERIC(9)
DECLARE @numCurrencyID AS NUMERIC(9)

DECLARE @intOppTcode AS NUMERIC(18,0)
DECLARE @numNewOppID AS NUMERIC(18,0)
DECLARE @fltExchangeRate AS FLOAT
DECLARE @numAccountClass AS NUMERIC(18) = 0
DECLARE @tintDefaultClassType NUMERIC(18,0)
DECLARE @numDefaultClassID NUMERIC(18,0) = 0
 


--Get Existing Opportunity Detail
SELECT 
	@numDivisionID = numDivisionId, 
	@numOppType=tintOppType, 
	@tintOppStatus = tintOppStatus,
	@numCurrencyID=numCurrencyID,
	@numStatus = numStatus,
	@DealStatus = tintOppStatus
FROM 
	OpportunityMaster 
WHERE 
	numOppId = @numOppID


--Get Default Item Class for particular user based on Domain settings  
SELECT 
	@tintDefaultClassType=ISNULL(tintDefaultClassType,0) 
FROM 
	dbo.Domain 
WHERE 
	numDomainID = @numDomainID 

IF @tintDefaultClassType=0
BEGIN
	SELECT 
		@numDefaultClassID=NULLIF(numDefaultClass,0) 
	FROM 
		dbo.UserMaster 
	WHERE 
		numUserDetailId =@numUserCntID AND 
		numDomainID = @numDomainID 
END                                               

IF ISNULL(@numCurrencyID,0) = 0 
BEGIN
	SET @fltExchangeRate=1
	SELECT @numCurrencyID= ISNULL(numCurrencyID,0) FROM dbo.Domain WHERE numDomainId=@numDomainId	
END
ELSE 
BEGIN
	SET @fltExchangeRate=dbo.GetExchangeRate(@numDomainID,@numCurrencyID)
END

SELECT 
	@numAccountClass=ISNULL(numDefaultClass,0) 
FROM 
	dbo.UserMaster UM 
JOIN 
	dbo.Domain D 
ON 
	UM.numDomainID=D.numDomainId
WHERE 
	D.numDomainId=@numDomainId AND 
	UM.numUserDetailId=@numUserCntID AND 
	ISNULL(D.IsEnableUserLevelClassTracking,0)=1

INSERT INTO OpportunityMaster                                                                          
(                                                                             
	numContactId, numDivisionId, txtComments, numCampainID, bitPublicFlag, tintSource, tintSourceType, vcPOppName, 
	intPEstimatedCloseDate, numCreatedBy, bintCreatedDate, numDomainId, numRecOwner, lngPConclAnalysis, tintOppType, 
	numSalesOrPurType, numCurrencyID, fltExchangeRate, numAssignedTo, numAssignedBy, [tintOppStatus], numStatus, 
	vcOppRefOrderNo, bitStockTransfer,bitBillingTerms, intBillingDays,bitInterestType,fltInterest,vcCouponCode, 
	bitDiscountType,fltDiscount,bitPPVariance, vcMarketplaceOrderID, vcMarketplaceOrderReportId, numPercentageComplete,
	numAccountClass, tintTaxOperator, bitUseShippersAccountNo, bitUseMarkupShippingRate, numMarkupShippingRate, 
	intUsedShippingCompany, bintAccountClosingDate
)                                                                          
SELECT
	numContactId, numDivisionId, txtComments, numCampainID, bitPublicFlag, tintSource, tintSourceType, vcPOppName, 
	@Date, @numUserCntID, @Date, numDomainId, @numUserCntID, lngPConclAnalysis, tintOppType, 
	numSalesOrPurType, @numCurrencyID, @fltExchangeRate, numAssignedTo, numAssignedBy, [tintOppStatus], numStatus, vcOppRefOrderNo, 
	bitStockTransfer,bitBillingTerms, intBillingDays,bitInterestType,fltInterest,vcCouponCode, bitDiscountType, fltDiscount,
	bitPPVariance, vcMarketplaceOrderID, vcMarketplaceOrderReportId, numPercentageComplete, numAccountClass,tintTaxOperator, 
	bitUseShippersAccountNo, bitUseMarkupShippingRate, numMarkupShippingRate, intUsedShippingCompany, @Date 
FROM
	OpportunityMaster
WHERE                                                                        
	numOppId = @numOppID

SET @numNewOppID=SCOPE_IDENTITY() 

EXEC USP_OpportunityMaster_CT @numDomainID=@numDomainID,@numUserCntID=@numUserCntID,@numRecordID=@numNewOppID                                             
  
--Update OppName as per Name Template
EXEC dbo.USP_UpdateNameTemplateValue @numOppType,@numDomainID,@numNewOppID

--Map Custom Field	
DECLARE @tintPageID AS TINYINT;
SET @tintPageID=CASE WHEN @numOppType=1 THEN 2 ELSE 6 END 
  	
EXEC dbo.USP_AddParentChildCustomFieldMap @numDomainID = @numDomainID, @numRecordID = @numNewOppID, @numParentRecId = @numDivisionId, @tintPageID = @tintPageID
 	
IF ISNULL(@numStatus,0) > 0 AND @numOppType=1 AND isnull(@DealStatus,0)=1
BEGIN
	EXEC USP_ManageOpportunityAutomationQueue
			@numOppQueueID = 0, -- numeric(18, 0)
			@numDomainID = @numDomainID, -- numeric(18, 0)
			@numOppId = @numNewOppID, -- numeric(18, 0)
			@numOppBizDocsId = 0, -- numeric(18, 0)
			@numOrderStatus = @numStatus, -- numeric(18, 0)
			@numUserCntID = @numUserCntID, -- numeric(18, 0)
			@tintProcessStatus = 1, -- tinyint
			@tintMode = 1 -- TINYINT
END


IF (SELECT COUNT(*) FROM OpportunityItems WHERE numOppId = @numOppID) > 0
BEGIN
	-- INSERT OPPORTUNITY ITEMS

	INSERT INTO OpportunityItems                                                                          
	(
		numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,bitDropShip,bitDiscountType,
		fltDiscount,monTotAmtBefDiscount,vcItemName,vcModelID,vcManufacturer,vcPathForTImage,monVendorCost,numUOMId,bitWorkOrder,
		numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,numClassID,numToWarehouseItemID,vcAttributes,
		monAvgCost,bitItemPriceApprovalRequired,numRecurParentOppItemID
	)
	SELECT 
		@numNewOppID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,bitDropShip,bitDiscountType,
		fltDiscount,monTotAmtBefDiscount,vcItemName,vcModelID,vcManufacturer,vcPathForTImage,monVendorCost,numUOMId,bitWorkOrder,
		numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,numClassID,numToWarehouseItemID,vcAttributes,
		monAvgCost,bitItemPriceApprovalRequired,OpportunityItems.numoppitemtCode
	FROM
		OpportunityItems
	WHERE
		numOppID = @numOppID 
           
	EXEC USP_BizRecurrence_WorkOrder @numDomainId,@numUserCntID,@numNewOppID,@numOppID

	INSERT INTO OpportunityKitItems                                                                          
	(
		numOppId,numOppItemID,numChildItemID,numWareHouseItemId,numQtyItemsReq,numQtyItemsReq_Orig,numUOMId,numQtyShipped
	)
	SELECT 
		@numNewOppId,OI.numoppitemtCode,ID.numChildItemID,ID.numWareHouseItemId,ID.numQtyItemsReq * OI.numUnitHour,ID.numQtyItemsReq,ID.numUOMId,0
	FROM 
		OpportunityItems OI 
	JOIN 
		ItemDetails ID 
	ON 
		OI.numItemCode=ID.numItemKitID 
	WHERE 
		OI.numOppId=@numNewOppID AND 
		OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityKitItems OKI WHERE OKI.numOppId=@numNewOppID)
END

DECLARE @TotalAmount AS FLOAT = 0                                                           
   
SELECT @TotalAmount = sum(monTotAmount) FROM OpportunityItems WHERE numOppId=@numNewOppID                                                                          
UPDATE OpportunityMaster SET monPamount=@TotalAmount WHERE numOppId=@numNewOppID 

--Insert Tax for Division   

IF @numOppType=1 OR (@numOppType=2 and (select isnull(bitPurchaseTaxCredit,0) from Domain where numDomainId=@numDomainId)=1)
BEGIN                   
	INSERT dbo.OpportunityMasterTaxItems 
	(
		numOppId, numTaxItemID,	fltPercentage
	) 
	SELECT 
		@numNewOppID,TI.numTaxItemID,dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,TI.numTaxItemID,@numNewOppID,0,NULL) 
	FROM 
		TaxItems TI 
	JOIN 
		DivisionTaxTypes DTT 
	ON 
		TI.numTaxItemID = DTT.numTaxItemID
	WHERE 
		DTT.numDivisionID=@numDivisionId AND 
		DTT.bitApplicable=1
	UNION 
	SELECT 
		@numNewOppID,0,dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,0,@numNewOppID,1,NULL) 
	FROM 
		dbo.DivisionMaster 
	WHERE 
		bitNoTax=0 AND 
		numDivisionID=@numDivisionID
END


--Updating the warehouse items              
DECLARE @tintShipped AS TINYINT = 0                

IF @tintOppStatus=1               
BEGIN       
	EXEC USP_UpdatingInventoryonCloseDeal @numNewOppID,@numUserCntID
END              

DECLARE @tintCRMType AS NUMERIC(9)
       
DECLARE @AccountClosingDate AS DATETIME 
SELECT @numDivisionID=numDivisionID,@AccountClosingDate=bintAccountClosingDate FROM OpportunityMaster WHERE numOppID=@numNewOppID         
SELECT @tintCRMType=tintCRMType FROM divisionmaster WHERE numDivisionID = @numDivisionID 

                 
--Add/Update Address Details
DECLARE @vcStreet varchar(100), @vcCity varchar(50), @vcPostalCode varchar(15), @vcCompanyName varchar(100), @vcAddressName VARCHAR(50)      
DECLARE @numState numeric(9),@numCountry numeric(9), @numCompanyId numeric(9) 
DECLARE @bitIsPrimary BIT

SELECT  
	@vcCompanyName=vcCompanyName,
	@numCompanyId=div.numCompanyID 
FROM 
	CompanyInfo Com                            
JOIN 
	divisionMaster Div                            
ON 
	div.numCompanyID=com.numCompanyID                            
WHERE 
	div.numdivisionID=@numDivisionId

--Bill Address
IF (SELECT COUNT(*) FROM OpportunityAddress WHERE numOppID = @numOppID) > 0
BEGIN
	SELECT  
		@vcStreet=ISNULL(vcBillStreet,''),
		@vcCity=ISNULL(vcBillCity,''),
		@vcPostalCode=ISNULL(vcBillPostCode,''),
		@numState=ISNULL(numBillState,0),
		@numCountry=ISNULL(numBillCountry,0),
		@vcAddressName=vcAddressName
	FROM  
		dbo.OpportunityAddress 
	WHERE   
		numOppID = @numOppID

  	EXEC dbo.USP_UpdateOppAddress
		@numOppID = @numNewOppID,
		@byteMode = 0,
		@vcStreet = @vcStreet,
		@vcCity = @vcCity,
		@vcPostalCode = @vcPostalCode,
		@numState = @numState,
		@numCountry = @numCountry,
		@vcCompanyName = @vcCompanyName,
		@numCompanyId = 0,
		@vcAddressName = @vcAddressName
END
  
--Ship Address
IF (SELECT COUNT(*) FROM OpportunityAddress WHERE numOppID = @numOppID) > 0
BEGIN
	SELECT  
		@vcStreet=ISNULL(vcShipStreet,''),
		@vcCity=ISNULL(vcShipCity,''),
		@vcPostalCode=ISNULL(vcShipPostCode,''),
		@numState=ISNULL(numShipState,0),
		@numCountry=ISNULL(numShipCountry,0),
		@vcAddressName=vcAddressName
    FROM  
		dbo.OpportunityAddress
	WHERE   
		numOppID = @numOppID
 
  	EXEC dbo.USP_UpdateOppAddress
		@numOppID = @numNewOppID,
		@byteMode = 1,
		@vcStreet = @vcStreet,
		@vcCity = @vcCity,
		@vcPostalCode = @vcPostalCode,
		@numState = @numState,
		@numCountry = @numCountry,
		@vcCompanyName = @vcCompanyName,
		@numCompanyId =@numCompanyId,
		@vcAddressName = @vcAddressName
END

--Delete Tax for Opportunity Items if item deleted 
DELETE FROM OpportunityItemsTaxItems WHERE numOppId=@numNewOppID AND numOppItemID NOT IN (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numNewOppID)

--Insert Tax for Opportunity Items
INSERT INTO dbo.OpportunityItemsTaxItems 
(
	numOppId,
	numOppItemID,
	numTaxItemID
) 
SELECT 
	@numNewOppID,
	OI.numoppitemtCode,
	TI.numTaxItemID 
FROM 
	dbo.OpportunityItems OI 
JOIN 
	dbo.ItemTax IT 
ON 
	OI.numItemCode=IT.numItemCode 
JOIN
	TaxItems TI 
ON 
	TI.numTaxItemID=IT.numTaxItemID 
WHERE 
	OI.numOppId=@numNewOppID AND 
	IT.bitApplicable=1 AND 
	OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numNewOppID)
UNION
SELECT 
	@numNewOppID,
	OI.numoppitemtCode,
	0 
FROM 
	dbo.OpportunityItems OI 
JOIN 
	dbo.Item I 
ON 
	OI.numItemCode=I.numItemCode
WHERE 
	OI.numOppId=@numNewOppID AND 
	I.bitTaxable=1 AND
	OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numNewOppID)

  
UPDATE OpportunityMaster SET monDealAmount=dbo.GetDealAmount(@numNewOppID,@Date,0) WHERE numOppId=@numNewOppID


IF @numNewOppID > 0
BEGIN
	-- Insert newly created opportunity to transaction
	INSERT INTO RecurrenceTransaction ([numRecConfigID],[numRecurrOppID],[dtCreatedDate]) VALUES (@numRecConfigID,@numNewOppID,GETDATE())

	-- Insert newly created opportunity to transaction history - Records are not deleted in this table when parent record is delete
	INSERT INTO RecurrenceTransactionHistory ([numRecConfigID],[numRecurrOppID],[dtCreatedDate]) VALUES (@numRecConfigID,@numNewOppID,GETDATE())

	--Get next recurrence date for order 
	DECLARE @dtNextRecurrenceDate DATE = NULL

	SET @dtNextRecurrenceDate = (CASE @numFrequency
								WHEN 1 THEN DATEADD(D,1,@Date) --Daily
								WHEN 2 THEN DATEADD(D,7,@Date) --Weekly
								WHEN 3 THEN DATEADD(M,1,@Date) --Monthly
								WHEN 4 THEN DATEADD(M,4,@Date)  --Quarterly
								END)
	
	--Increase value of number of transaction completed by 1
	UPDATE RecurrenceConfiguration SET numTransaction = ISNULL(numTransaction,0) + 1 WHERE numRecConfigID = @numRecConfigID
			
	PRINT @dtNextRecurrenceDate
	PRINT @dtEndDate

	-- Set recurrence status as completed if next recurrence date is greater than end date
	If @dtNextRecurrenceDate > @dtEndDate
	BEGIN
		UPDATE RecurrenceConfiguration SET bitCompleted = 1 WHERE numRecConfigID = @numRecConfigID
	END
	ELSE -- Set next recurrence date
	BEGIN
		UPDATE RecurrenceConfiguration SET dtNextRecurrenceDate = @dtNextRecurrenceDate WHERE numRecConfigID = @numRecConfigID
	END
END


UPDATE OpportunityMaster SET bitRecurred = 1 WHERE numOppId = @numNewOppID

SET @numRecurOppID = @numNewOppID

END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000);
    DECLARE @ErrorSeverity INT;
    DECLARE @ErrorState INT;

    SELECT @ErrorMessage = ERROR_MESSAGE(), @ErrorSeverity = ERROR_SEVERITY(), @ErrorState = ERROR_STATE();

    RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);

	IF @@TRANCOUNT > 0
        ROLLBACK TRANSACTION;
END CATCH

IF @@TRANCOUNT > 0
    COMMIT TRANSACTION;

END  

GO
/* 
UnComment this  Only for dubug purpose
UPDATE item SET numAssetChartAcntId=NULL,numCOGsChartAcntId=NULL,numIncomeChartAcntId=NULL WHERE numDomainID=@numDomainId
DELETE FROM dbo.AccountingCharges WHERE numDomainID=@numDomainId                                
DELETE FROM dbo.COAShippingMapping WHERE numDomainID=@numDomainId
DELETE FROM dbo.COARelationships WHERE numDomainID=@numDomainId
DELETE FROM dbo.ChartAccountOpening WHERE numDomainId=@numDomainId
DELETE FROM dbo.Chart_Of_Accounts WHERE numDomainId=@numDomainId
DELETE FROM dbo.AccountTypeDetail WHERE numDomainId=@numDomainId
*/    
--exec USP_ChartAcntDefaultValues @numDomainId=204,@numUserCntId=116784
--SELECT * FROM chart_of_Accounts where numDomainID = 183
--select * from AccountTypeDetail where numDomainID=110
     
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ChartAcntDefaultValues' ) 
    DROP PROCEDURE USP_ChartAcntDefaultValues
GO
CREATE PROCEDURE [dbo].[USP_ChartAcntDefaultValues]
    @numDomainId AS NUMERIC(9) = 0,
    @numUserCntId AS NUMERIC(9) = 0
AS 
    BEGIN        
 
        DECLARE @intCount AS NUMERIC(9)   
        DECLARE @dtTodayDate AS DATETIME
        SET @dtTodayDate = GETDATE()                                                 
        SET @intCount = 0                                  
                                           
        SELECT  @intCount = COUNT(*)
        FROM    Chart_Of_Accounts
        WHERE   numDomainId = @numDomainId                                                    
        
        
        IF @intCount = 0 
            BEGIN      
----INSERTING DEFAULT ACCOUNT TYPES
                EXEC USP_AccountTypeDefaultValue @numDomainId = @numDomainId ;


 --Check for Current Fiancial Year
        IF ( SELECT COUNT([numFinYearId])
             FROM   [FinancialYear]
             WHERE  [numDomainId] = @numDomainId
                    AND [bitCurrentYear] = 1
           ) = 0 
            BEGIN
            
				DECLARE @dtPeriodFrom AS DATETIME,@dtPeriodTo AS DATETIME
				
				SELECT	@dtPeriodFrom=DATEADD(YEAR, DATEDIFF(YEAR, 0, GETDATE()), 0),
						@dtPeriodTo=DATEADD(YEAR, DATEDIFF(YEAR, -1, GETDATE()), -1)
	
				EXEC dbo.USP_ManageFinancialYear
					@numDomainId = @numDomainId, --  numeric(9, 0)
					@dtPeriodFrom = @dtPeriodFrom, --  datetime
					@dtPeriodTo = @dtPeriodTo, --  datetime
					@vcFinYearDesc = '', --  varchar(50)
					@bitCloseStatus = 0, --  bit
					@bitAuditStatus = 0, --  bit
					@bitCurrentYear = 1 --  bit
            END
            
                


DECLARE @numAssetAccountTypeID AS NUMERIC(9,0)
DECLARE @numLiabilityAccountTypeID  AS NUMERIC(9,0)
DECLARE @numIncomeAccountTypeID AS NUMERIC(9,0) 
DECLARE @numExpenseAccountTypeID  AS NUMERIC(9,0)
DECLARE @numEquityAccountTypeID  AS NUMERIC(9,0)
DECLARE @numCOGSAccountTypeID  AS NUMERIC(9,0)

SELECT @numAssetAccountTypeID = numAccountTypeId FROM dbo.AccountTypeDetail WHERE vcAccountCode ='0101' AND numDomainID = @numDomainID 
SELECT @numLiabilityAccountTypeID = numAccountTypeId FROM dbo.AccountTypeDetail WHERE vcAccountCode ='0102' AND numDomainID = @numDomainID 
SELECT @numIncomeAccountTypeID = numAccountTypeId FROM dbo.AccountTypeDetail WHERE vcAccountCode ='0103' AND numDomainID = @numDomainID 
SELECT @numExpenseAccountTypeID = numAccountTypeId FROM dbo.AccountTypeDetail WHERE vcAccountCode ='0104' AND numDomainID = @numDomainID 
SELECT @numEquityAccountTypeID = numAccountTypeId FROM dbo.AccountTypeDetail WHERE vcAccountCode ='0105' AND numDomainID = @numDomainID  

DECLARE @numLoanAccountTypeID AS NUMERIC(9)
SELECT @numLoanAccountTypeID = numAccountTypeID FROM dbo.AccountTypeDetail WHERE vcAccountCode = '01020201'  AND numDomainID = @numDomainID  
PRINT @numLoanAccountTypeID

DECLARE @numParentForCOGSAccID AS NUMERIC(9)
SELECT @numParentForCOGSAccID = numParentID FROM dbo.AccountTypeDetail WHERE numDomainID = @numDomainID AND vcAccountCode = '0101'
PRINT @numParentForCOGSAccID


SELECT @numCOGSAccountTypeID = numAccountTypeId FROM dbo.AccountTypeDetail WHERE vcAccountCode ='0106' AND numDomainID = @numDomainID  
DECLARE @numRSccountTypeID AS NUMERIC(9)
SELECT @numRSccountTypeID = numAccountTypeID FROM dbo.AccountTypeDetail WHERE vcAccountCode = '01050101'  AND numDomainID = @numDomainID  
PRINT @numRSccountTypeID

EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numEquityAccountTypeID,
    @numParntAcntTypeID = @numRSccountTypeID, @vcAccountName = 'Profit & Loss',
    @vcAccountDescription = 'Profit & Loss',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 1, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0
   
PRINT 18
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numEquityAccountTypeID,
    @numParntAcntTypeID = @numRSccountTypeID, @vcAccountName = 'Retained Earnings',
    @vcAccountDescription = 'Retained Earnings', @monOriginalOpeningBal = $0,
    @monOpeningBal = $0, @dtOpeningDate = @dtTodayDate, @bitActive = 1,
    @numAccountId = 0, @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0,
    @numUserCntId = 1, @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0


EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numEquityAccountTypeID,
    @numParntAcntTypeID = @numRSccountTypeID, @vcAccountName = 'Opening Balance Equity',
    @vcAccountDescription = 'Opening Balance Equity', @monOriginalOpeningBal = $0,
    @monOpeningBal = $0, @dtOpeningDate = @dtTodayDate, @bitActive = 1,
    @numAccountId = 0, @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0,
    @numUserCntId = 1, @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

   
------------------------------------ ASSETS ENTRY --------------------------------------------
    
DECLARE @numBankAccountTypeID AS NUMERIC(9)
SELECT @numBankAccountTypeID = numAccountTypeID FROM dbo.AccountTypeDetail WHERE vcAccountCode = '01010101'  AND numDomainID = @numDomainID  
PRINT @numBankAccountTypeID

--select numAccountTypeID,'UnDepositedFunds','UnDepositedFunds','0101010101',numAccountTypeID,0,0,0,1,0,1,0,0  from AccountTypeDetail where numDomainID=1 and vcAccountCode='01010101'
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numAssetAccountTypeID,
    @numParntAcntTypeID = @numBankAccountTypeID , @vcAccountName = 'UnDeposited Funds',
    @vcAccountDescription = 'UnDepositedFunds',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0
    
-----------------
 
DECLARE @numPropAccountTypeID AS NUMERIC(9)
SELECT @numPropAccountTypeID = numAccountTypeID FROM dbo.AccountTypeDetail WHERE vcAccountCode = '01010201'  AND numDomainID = @numDomainID  
PRINT @numPropAccountTypeID   
    
PRINT 1
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numAssetAccountTypeID,
    @numParntAcntTypeID = @numPropAccountTypeID, @vcAccountName = 'Office Equipment',
    @vcAccountDescription = 'Office equipment that is owned and controlled by the business',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

PRINT 2
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numAssetAccountTypeID,
    @numParntAcntTypeID = @numPropAccountTypeID,
    @vcAccountName = 'Less Accumulated Depreciation on Office Equipment',
    @vcAccountDescription = 'The total amount of office equipment cost that has been consumed by the entity (based on the useful life)',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

PRINT 3
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numAssetAccountTypeID,
    @numParntAcntTypeID = @numPropAccountTypeID, @vcAccountName = 'Computer Equipment',
    @vcAccountDescription = 'Computer equipment that is owned and controlled by the business',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

--PRINT 4
--EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numAssetAccountTypeID,
--    @numParntAcntTypeID = @numPropAccountTypeID,
--    @vcAccountName = 'Less Accumulated Depreciation on Computer Equipment',
--    @vcAccountDescription = 'The total amount of computer equipment cost that has been consumed by the business (based on the useful life)',
--    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
--    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
--    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
--    @bitProfitLoss = 0, @bitDepreciation = 0,
--    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
--    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0
-------------------------

DECLARE @numARAccountTypeID AS NUMERIC(9)
SELECT @numARAccountTypeID = numAccountTypeID FROM dbo.AccountTypeDetail WHERE vcAccountCode = '01010105'  AND numDomainID = @numDomainID  
PRINT @numARAccountTypeID  

EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numAssetAccountTypeID,
    @numParntAcntTypeID = @numARAccountTypeID, @vcAccountName = 'Account Receivable',
    @vcAccountDescription = 'Account Receivable.',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0
    
PRINT 5
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numAssetAccountTypeID,
    @numParntAcntTypeID = @numARAccountTypeID, @vcAccountName = 'Prepayments',
    @vcAccountDescription = 'An expenditure that has been paid for in advance.',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numAssetAccountTypeID,
    @numParntAcntTypeID = @numARAccountTypeID, @vcAccountName = 'Sales Clearing',
    @vcAccountDescription = 'Sales Clearing.',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0


DECLARE @numStockAccountTypeID AS NUMERIC(9)
SELECT @numStockAccountTypeID = numAccountTypeID FROM dbo.AccountTypeDetail WHERE vcAccountCode = '01010104'  AND numDomainID = @numDomainID  
PRINT @numStockAccountTypeID  

PRINT 6
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numAssetAccountTypeID,
    @numParntAcntTypeID = @numStockAccountTypeID, @vcAccountName = 'Inventory',
    @vcAccountDescription = 'Items available for sale including all costs of production.',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numAssetAccountTypeID,
    @numParntAcntTypeID = @numStockAccountTypeID, @vcAccountName = 'Work In Progress',
    @vcAccountDescription = 'Work In Progress',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0    

EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numAssetAccountTypeID,
    @numParntAcntTypeID = @numStockAccountTypeID, @vcAccountName = 'Finished Goods',
    @vcAccountDescription = 'Finished Goods',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0     
---------------------------------------------------------------------------------------------------------

------------------------------------ LIABILITIES ENTRY --------------------------------------------
DECLARE @numDutyTaxAccountTypeID AS NUMERIC(9)
SELECT @numDutyTaxAccountTypeID = numAccountTypeID FROM dbo.AccountTypeDetail WHERE vcAccountCode = '01020101'  AND numDomainID = @numDomainID  
PRINT @numDutyTaxAccountTypeID

--select numAccountTypeID,'Sales Tax Payable','Sales Tax Payable','0102010101',numAccountTypeID,0,0,0,1,0,1,0,0  from AccountTypeDetail where numDomainID=1 and vcAccountCode='01020101'   
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numLiabilityAccountTypeID,
    @numParntAcntTypeID =  @numDutyTaxAccountTypeID , @vcAccountName = 'Sales Tax Payable',
    @vcAccountDescription = 'Sales Tax Payable',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

DECLARE @numAPAccountTypeID AS NUMERIC(9)
SELECT @numAPAccountTypeID = numAccountTypeID FROM dbo.AccountTypeDetail WHERE vcAccountCode = '01020102'  AND numDomainID = @numDomainID  
PRINT @numAPAccountTypeID


EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numLiabilityAccountTypeID,
    @numParntAcntTypeID =  @numAPAccountTypeID , @vcAccountName = 'Deduction from Employee Payroll',
    @vcAccountDescription = 'Deduction from Employee Payroll',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numLiabilityAccountTypeID,
    @numParntAcntTypeID =  @numAPAccountTypeID , @vcAccountName = 'Accounts Payable',
    @vcAccountDescription = 'Accounts Payable',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0
        
PRINT 7
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numLiabilityAccountTypeID,
    @numParntAcntTypeID = @numAPAccountTypeID, @vcAccountName = 'Employee Tax Payable',
    @vcAccountDescription = 'The amount of tax that has been deducted from wages or salaries paid to employes and is due to be paid',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

PRINT 8
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numLiabilityAccountTypeID,
    @numParntAcntTypeID = @numAPAccountTypeID, @vcAccountName = 'Income Tax Payable',
    @vcAccountDescription = 'The amount of income tax that is due to be paid, also resident withholding tax paid on interest received.',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

--PRINT 9
--EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = 1490,
--    @numParntAcntTypeID = 1491, @vcAccountName = 'Suspense',
--    @vcAccountDescription = 'An entry that allows an unknown transaction to be entered, so the accounts can still be worked on in balance and the entry can be dealt with later.',
--    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
--    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
--    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
--    @bitProfitLoss = 0, @bitDepreciation = 0,
--    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
--    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

--SELECT * FROM dbo.AccountTypeDetail WHERE vcAccountCode = '010201' AND numDomainID = @numDomainID  
--SELECT * FROM dbo.AccountTypeDetail WHERE vcAccountCode = '01020101' AND numDomainID = @numDomainID  
--SELECT * FROM dbo.AccountTypeDetail WHERE vcAccountCode = '01020102' AND numDomainID = @numDomainID  

DECLARE @numCLAccountTypeID AS NUMERIC(9)
SELECT @numCLAccountTypeID = numAccountTypeID FROM dbo.AccountTypeDetail WHERE vcAccountCode = '01020102'  AND numDomainID = @numDomainID  
PRINT @numCLAccountTypeID

PRINT 10
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numLiabilityAccountTypeID,
    @numParntAcntTypeID = @numCLAccountTypeID, @vcAccountName = 'Unpaid Expense Claims',
    @vcAccountDescription = 'Automatically updates this account for payroll entries created using Payroll and will store the payroll amount to be paid to the employee for the pay run. This account enables you to maintain separate accounts for employee Wages Payable amounts and Accounts Payable amounts',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

PRINT 11
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numLiabilityAccountTypeID,
    @numParntAcntTypeID = @numAPAccountTypeID, @vcAccountName = 'Wages Payable',
    @vcAccountDescription = 'Office equipment that is owned and controlled by the business',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

PRINT 14
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numLiabilityAccountTypeID,
    @numParntAcntTypeID = @numCLAccountTypeID, @vcAccountName = 'Tracking Transfers',
    @vcAccountDescription = 'Transfers between tracking categories',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

DECLARE @numWPAccountTypeID AS NUMERIC(9)
SELECT @numWPAccountTypeID = numAccountId FROM dbo.Chart_Of_Accounts WHERE numDomainID = @numDomainID  AND vcAccountName = 'Wages Payable'
PRINT @numWPAccountTypeID
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numLiabilityAccountTypeID,
    @numParntAcntTypeID = @numCLAccountTypeID, @vcAccountName = 'Wages & Salaries Payable',
    @vcAccountDescription = 'Wages & Salaries Payable',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 1, @numParentAccId = @numWPAccountTypeID
    
--SELECT * FROM dbo.AccountTypeDetail WHERE numDomainID = @numDomainID  
--SELECT * FROM dbo.AccountTypeDetail WHERE numAccountTypeID = 1490 AND numDomainID = @numDomainID  
--SELECT * FROM dbo.AccountTypeDetail WHERE vcAccountCode = '010202' AND numDomainID = @numDomainID  
--SELECT * FROM dbo.AccountTypeDetail WHERE vcAccountCode = '010202' AND numDomainID = @numDomainID  
--SELECT * FROM dbo.Chart_Of_Accounts WHERE  numDomainID = @numDomainID   AND bitActive = 0 AND vcAccountName IN ('Sales Tax Payable', 'Loan','Tracking Transfers')


PRINT 15
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numLiabilityAccountTypeID,
    @numParntAcntTypeID = @numLoanAccountTypeID , @vcAccountName = 'Loan',
    @vcAccountDescription = 'Money that has been borrowed from a creditor',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount =0, @numParentAccId = 0
---------------------------------------------------------------------------------------------------

------------------------------------ EQUITY ENTRY --------------------------------------------

--SELECT * FROM dbo.AccountTypeDetail WHERE vcAccountCode = '0105' AND numDomainID = @numDomainID  
DECLARE @numEquityID AS NUMERIC(9)
SELECT @numEquityID = numAccountTypeID FROM dbo.AccountTypeDetail WHERE vcAccountCode = '0105'  AND numDomainID = @numDomainID  
PRINT @numEquityID

---------------------------------------------------------------------------------------------------

------------------------------------ EXPENSE ENTRY --------------------------------------------

----SELECT * FROM dbo.AccountTypeDetail WHERE vcAccountCode = '010601' AND numDomainID = @numDomainID  
--DECLARE @numCOGSID AS NUMERIC(9)
--SELECT @numCOGSID = numAccountTypeID FROM dbo.AccountTypeDetail WHERE numDomainID = @numDomainID   AND vcAccountCode = '01040101' 
--PRINT @numCOGSID
----select numAccountTypeID,'COGS Control Account','COGS','0104010401',numAccountTypeID,0,0,0,1,0,1,0,0  from AccountTypeDetail where numDomainID=1 and vcAccountCode='01040104'
--EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numExpenseAccountTypeID,
--    @numParntAcntTypeID = @numCOGSID, @vcAccountName = 'COGS',
--    @vcAccountDescription = 'COGS Control Account',
--    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
--    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
--    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
--    @bitProfitLoss = 0, @bitDepreciation = 0,
--    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
--    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

--SELECT * FROM dbo.AccountTypeDetail WHERE vcAccountCode = '01040105' AND numDomainID = @numDomainID  
DECLARE @numOtherDirectExpenseID AS NUMERIC(9)
SELECT @numOtherDirectExpenseID = numAccountTypeID FROM dbo.AccountTypeDetail WHERE numDomainID = @numDomainID   AND vcAccountCode = '01040101' 
PRINT @numOtherDirectExpenseID
--select numAccountTypeID,'Billable Time & Expenses','Billable Time & Expenses','0104010101',numAccountTypeID,0,0,0,1,0,1,0,0  from AccountTypeDetail where numDomainID=1 and vcAccountCode='01040101'
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numExpenseAccountTypeID,
    @numParntAcntTypeID = @numOtherDirectExpenseID, @vcAccountName = 'Billable Time & Expenses',
    @vcAccountDescription = 'Office equipment that is owned and controlled by the business',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

--select numAccountTypeID,'Purchase Account','Purchase','0104010402',numAccountTypeID,0,0,0,1,0,1,0,0  from AccountTypeDetail where numDomainID=1 and vcAccountCode='01040104'
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numExpenseAccountTypeID,
    @numParntAcntTypeID = @numOtherDirectExpenseID, @vcAccountName = 'Purchase',
    @vcAccountDescription = 'Purchase Account',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numExpenseAccountTypeID,
    @numParntAcntTypeID = @numOtherDirectExpenseID, @vcAccountName = 'Reconciliation Discrepancies',
    @vcAccountDescription = 'Reconciliation Discrepancies',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numExpenseAccountTypeID,
    @numParntAcntTypeID = @numOtherDirectExpenseID, @vcAccountName = 'Foreign Exchange Gain/Loss',
    @vcAccountDescription = 'Foreign Exchange Gain/Loss',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0
    
--01040104
--DECLARE @numPriceExpenseID AS NUMERIC(9)
--SELECT @numPriceExpenseID = numAccountTypeID FROM dbo.AccountTypeDetail WHERE numDomainID = @numDomainID   AND vcAccountCode = '01040104' 
--PRINT @numPriceExpenseID 

--PRINT 20
--EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numExpenseAccountTypeID,
--    @numParntAcntTypeID = @numOtherDirectExpenseID, @vcAccountName = 'Price Variance',
--    @vcAccountDescription = 'Price Variance',
--    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
--    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
--    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
--    @bitProfitLoss = 0, @bitDepreciation = 0,
--    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
--    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0
    
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numExpenseAccountTypeID,
    @numParntAcntTypeID = @numOtherDirectExpenseID, @vcAccountName = 'Inventory Adjustment',
    @vcAccountDescription = 'Inventory Adjustment',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0


--SELECT * FROM dbo.AccountTypeDetail WHERE numDomainID = @numDomainID   AND vcAccountCode = '010601' 
DECLARE @numCOGSExpenseID AS NUMERIC(9)
SELECT @numCOGSExpenseID = numAccountTypeID FROM dbo.AccountTypeDetail WHERE numDomainID = @numDomainID   AND vcAccountCode = '010601' 
PRINT @numCOGSExpenseID 
       
PRINT 20
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numCOGSAccountTypeID,
    @numParntAcntTypeID = @numCOGSExpenseID, @vcAccountName = 'Cost of Goods Sold',
    @vcAccountDescription = 'Costs of goods made by the business include material, labor, and other modification costs.',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0


EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numCOGSAccountTypeID,
    @numParntAcntTypeID = @numCOGSExpenseID, @vcAccountName = 'Purchase Price Variance',
    @vcAccountDescription = 'Stores difference of purchase price when Cost of Product is different then what is entered in Purchase Order.',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0


    
--SELECT * FROM dbo.AccountTypeDetail WHERE vcAccountCode = '010401' AND numDomainID = @numDomainID  
DECLARE @numDirectExpenseID AS NUMERIC(9)
SELECT @numDirectExpenseID = numAccountTypeID FROM dbo.AccountTypeDetail WHERE numDomainID = @numDomainID   AND vcAccountCode = '010401' 
PRINT @numDirectExpenseID 
--       
----SELECT * FROM dbo.AccountTypeDetail WHERE vcAccountCode = '010401' AND numDomainID = @numDomainID  
--DECLARE @numDirectExpenseID AS NUMERIC(9)
--SELECT @numDirectExpenseID = numAccountTypeID FROM dbo.AccountTypeDetail WHERE numDomainID = @numDomainID   AND vcAccountCode = '010401' 
--PRINT @numDirectExpenseID 

--SELECT * FROM dbo.AccountTypeDetail WHERE vcAccountCode = '01040103' AND numDomainID = @numDomainID  
DECLARE @numOpeExpenseID AS NUMERIC(9)
SELECT @numOpeExpenseID = numAccountTypeID FROM dbo.AccountTypeDetail WHERE numDomainID = @numDomainID   AND vcAccountCode = '01040103' 
PRINT @numOpeExpenseID 

--PRINT 28
--EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numExpenseAccountTypeID,
--    @numParntAcntTypeID = @numOpeExpenseID, @vcAccountName = 'Office Equipment',
--    @vcAccountDescription = 'Office equipment that is owned and controlled by the business',
--    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
--    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
--    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
--    @bitProfitLoss = 0, @bitDepreciation = 0,
--    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
--    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0
  
PRINT 31
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numExpenseAccountTypeID,
    @numParntAcntTypeID = @numOpeExpenseID, @vcAccountName = 'Utilities',
    @vcAccountDescription = 'Expenses incurred for lighting, powering or heating the premises',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0
     
PRINT 32 
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numExpenseAccountTypeID,
    @numParntAcntTypeID = @numOpeExpenseID, @vcAccountName = 'Automobile Expenses',
    @vcAccountDescription = 'Expenses incurred on the running of company automobiles.',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

PRINT 35 
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numExpenseAccountTypeID,
    @numParntAcntTypeID = @numOpeExpenseID, @vcAccountName = 'Rent',
    @vcAccountDescription = 'The payment to lease a building or area.',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

PRINT 42
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numExpenseAccountTypeID,
    @numParntAcntTypeID = @numOpeExpenseID, @vcAccountName = 'Bad Debts',
    @vcAccountDescription = 'Noncollectable accounts receivable which have been written off.',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

--SELECT * FROM dbo.AccountTypeDetail WHERE vcAccountCode = '010402' AND numDomainID = @numDomainID  
DECLARE @numIndirectExpenseID AS NUMERIC(9)
SELECT @numIndirectExpenseID = numAccountTypeID FROM dbo.AccountTypeDetail WHERE numDomainID = @numDomainID   AND vcAccountCode = '010402' 
PRINT @numIndirectExpenseID 
        
--SELECT * FROM dbo.AccountTypeDetail WHERE vcAccountCode = '01040201' AND numDomainID = @numDomainID  
DECLARE @numAdminExpenseID AS NUMERIC(9)
SELECT @numAdminExpenseID = numAccountTypeID FROM dbo.AccountTypeDetail WHERE numDomainID = @numDomainID   AND vcAccountCode = '01040201' 
PRINT @numAdminExpenseID 

       
PRINT 26
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numExpenseAccountTypeID,
    @numParntAcntTypeID = @numAdminExpenseID, @vcAccountName = 'Postage & Delivery',
    @vcAccountDescription = 'Expenses incurred on postage & delivery costs.',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0
        
PRINT 23
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numExpenseAccountTypeID,
    @numParntAcntTypeID = @numAdminExpenseID, @vcAccountName = 'Janitorial Expenses',
    @vcAccountDescription = 'Expenses incurred for cleaning business property.',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0
    
PRINT 24
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numExpenseAccountTypeID,
    @numParntAcntTypeID = @numAdminExpenseID, @vcAccountName = 'Consulting & Accounting',
    @vcAccountDescription = 'Expenses related to paying consultants',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0
        
PRINT 27
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numExpenseAccountTypeID,
    @numParntAcntTypeID = @numAdminExpenseID, @vcAccountName = 'General Expenses',
    @vcAccountDescription = 'General expenses related to the running of the business.',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0
    
PRINT 33
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numExpenseAccountTypeID,
    @numParntAcntTypeID = @numAdminExpenseID, @vcAccountName = 'Office Expenses',
    @vcAccountDescription = 'General expenses related to the running of the business office.',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

PRINT 36
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numExpenseAccountTypeID,
    @numParntAcntTypeID = @numAdminExpenseID, @vcAccountName = 'Repairs and Maintenance',
    @vcAccountDescription = 'Expenses incurred on a damaged or run down asset that will bring the asset back to its original condition.',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0
 
PRINT 39 
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numExpenseAccountTypeID,
    @numParntAcntTypeID = @numAdminExpenseID, @vcAccountName = 'Dues & Subscriptions',
    @vcAccountDescription = 'E.g. Magazines, professional bodies',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

--SELECT * FROM dbo.AccountTypeDetail WHERE vcAccountCode = '01040202' AND numDomainID = @numDomainID  
DECLARE @numStaffExpenseID AS NUMERIC(9)
SELECT @numStaffExpenseID = numAccountTypeID FROM dbo.AccountTypeDetail WHERE numDomainID = @numDomainID   AND vcAccountCode = '01040202' 
PRINT @numStaffExpenseID 

PRINT 37 
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numExpenseAccountTypeID,
    @numParntAcntTypeID = @numStaffExpenseID, @vcAccountName = 'Wages and Salaries',
    @vcAccountDescription = 'Payment to employees in exchange for their resources',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

PRINT 41 
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numExpenseAccountTypeID,
    @numParntAcntTypeID = @numStaffExpenseID, @vcAccountName = 'Travel',
    @vcAccountDescription = 'Expenses incurred from travel which has a business purpose',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0
    

SELECT * FROM dbo.Chart_Of_Accounts WHERE numDomainID = 1 AND vcAccountName = 'Wages and Salaries'
DECLARE @numWSID AS NUMERIC(9)
SELECT @numWSID = numAccountID FROM dbo.Chart_Of_Accounts WHERE numDomainID = @numDomainID   AND vcAccountName = 'Wages and Salaries'
PRINT @numWSID 

EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numExpenseAccountTypeID,
    @numParntAcntTypeID = @numStaffExpenseID , @vcAccountName = 'Employee payroll expense',
    @vcAccountDescription = 'Employee payroll expense',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 1, @numParentAccId = @numWSID

EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numExpenseAccountTypeID,
    @numParntAcntTypeID = @numStaffExpenseID, @vcAccountName = 'Contract Employee Payroll Expense',
    @vcAccountDescription = 'Contract Employee Payroll Expense',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 1, @numParentAccId = @numWSID
  
---------------------------------------------------------------------------------------------------

------------------------------------ INCOME ENTRY --------------------------------------------
--SELECT * FROM dbo.AccountTypeDetail WHERE vcAccountCode = '010301' AND numDomainID = @numDomainID  
DECLARE @numDirectIncomeAccountTypeID AS NUMERIC(9)
SELECT @numDirectIncomeAccountTypeID = numAccountTypeID FROM dbo.AccountTypeDetail WHERE vcAccountCode = '010301'  AND numDomainID = @numDomainID  
PRINT @numDirectIncomeAccountTypeID

--select numAccountTypeID,'Sales Discounts','Sales Discounts','01030101',numAccountTypeID,0,0,0,1,0,1,0,0  from AccountTypeDetail where numDomainID=1 and vcAccountCode='010301'
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numIncomeAccountTypeID,
    @numParntAcntTypeID = @numDirectIncomeAccountTypeID , @vcAccountName = 'Sales Discounts',
    @vcAccountDescription = 'Sales Discounts',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

--select numAccountTypeID,'Shipping Income','Shipping Income','01030201',numAccountTypeID,0,0,0,1,0,1,0,0  from AccountTypeDetail where numDomainID=1 and vcAccountCode='010302'
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numAssetAccountTypeID,
    @numParntAcntTypeID = @numDirectIncomeAccountTypeID, @vcAccountName = 'Shipping Income',
    @vcAccountDescription = 'Shipping Income',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

--select numAccountTypeID,'Late Charges','Late Charges','01030202',numAccountTypeID,0,0,0,1,0,1,0,0  from AccountTypeDetail where numDomainID=1 and vcAccountCode='010302'
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numAssetAccountTypeID,
    @numParntAcntTypeID = @numDirectIncomeAccountTypeID, @vcAccountName = 'Late Charges',
    @vcAccountDescription = 'Late Charges',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0
        
PRINT 46 
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numIncomeAccountTypeID,
    @numParntAcntTypeID =  @numDirectIncomeAccountTypeID, @vcAccountName = 'Other Revenue',
    @vcAccountDescription = 'Any other income that does not relate to normal business activities and is not recurring',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numIncomeAccountTypeID,
    @numParntAcntTypeID = @numDirectIncomeAccountTypeID , @vcAccountName = 'Sales',
    @vcAccountDescription = 'Sales',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0
    
---------------------------------------------------------------------------------------------------

--SELECT * FROM dbo.Chart_Of_Accounts WHERE  numDomainID = @numDomainID   AND vcAccountCode = '01020201'
--DELETE FROM dbo.ChartAccountOpening WHERE  numDomainID = @numDomainID   AND numAccountId = (SELECT numAccountId FROM dbo.Chart_Of_Accounts WHERE  numDomainID = @numDomainID   AND vcAccountCode = '01020201')
--DELETE FROM dbo.Chart_Of_Accounts WHERE  numDomainID = @numDomainID   AND vcAccountCode = '01020201'

----------------

-- Create Credit Card as a new account under Current liability.
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numLiabilityAccountTypeID,
    @numParntAcntTypeID =  @numCLAccountTypeID, @vcAccountName = 'Credit Card',
    @vcAccountDescription = 'Credit Card',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

--- Adding new account "Purchase Clearing" under Current liability.
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numLiabilityAccountTypeID,
    @numParntAcntTypeID =  @numCLAccountTypeID, @vcAccountName = 'Purchase Clearing',
    @vcAccountDescription = 'Purchase Clearing',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0
-- From Accounts payable remove �Historical adjustments� and �rounding� unless it is required as default for some specific purpose. 
--SELECT * FROM dbo.Chart_Of_Accounts WHERE vcAccountCode = '0102010212' AND numDomainID = @numDomainID   
--DELETE FROM dbo.ChartAccountOpening WHERE  numDomainID = @numDomainID   AND numAccountId = (SELECT numAccountId FROM dbo.Chart_Of_Accounts WHERE  numDomainID = @numDomainID   AND vcAccountCode = '0102010212')
--DELETE FROM dbo.Chart_Of_Accounts WHERE vcAccountCode = '0102010212' AND numDomainID = @numDomainID  
--
----SELECT * FROM dbo.Chart_Of_Accounts WHERE vcAccountCode = '0102010211' AND numDomainID = @numDomainID  
--DELETE FROM dbo.ChartAccountOpening WHERE  numDomainID = @numDomainID   AND numAccountId = (SELECT numAccountId FROM dbo.Chart_Of_Accounts WHERE  numDomainID = @numDomainID   AND vcAccountCode = '0102010211')
--DELETE FROM dbo.Chart_Of_Accounts WHERE vcAccountCode = '0102010211' AND numDomainID = @numDomainID  

PRINT 12
--EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numLiabilityAccountTypeID,
--    @numParntAcntTypeID = @numCLAccountTypeID, @vcAccountName = 'Historical Adjustment',
--    @vcAccountDescription = 'For accountant adjustments',
--    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
--    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
--    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
--    @bitProfitLoss = 0, @bitDepreciation = 0,
--    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
--    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

PRINT 13
--EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numLiabilityAccountTypeID,
--    @numParntAcntTypeID = @numCLAccountTypeID, @vcAccountName = 'Rounding',
--    @vcAccountDescription = 'An adjustment entry to allow for rounding',
--    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
--    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
--    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
--    @bitProfitLoss = 0, @bitDepreciation = 0,
--    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
--    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0
    
--Remove Suspense A/c from Current Liability
--SELECT * FROM dbo.AccountTypeDetail WHERE vcAccountCode = '01020104' AND numDomainID = @numDomainID  
--DELETE FROM dbo.ChartAccountOpening WHERE  numDomainID = @numDomainID   AND numAccountId = (SELECT numAccountId FROM dbo.Chart_Of_Accounts WHERE  numDomainID = @numDomainID   AND vcAccountCode = '0102010212')
--DELETE FROM dbo.AccountTypeDetail WHERE vcAccountCode = '0102010212' AND numDomainID = @numDomainID  

--From Current liability change the name of �Loans (Liability)� account type to Short Term borrowings.
--SELECT * FROM dbo.AccountTypeDetail WHERE numDomainID = @numDomainID   AND vcAccountCode = '01020103'
--UPDATE dbo.AccountTypeDetail SET vcAccountType = 'Short Term Borrowings' WHERE vcAccountCode = '01020103' AND numDomainID = @numDomainID  


--Interest income should be under indirect income instead of direct income.
--SELECT * FROM dbo.AccountTypeDetail WHERE numDomainID = @numDomainID   AND vcAccountCode = '010302' 
DECLARE @numIndirectIncomeAccountTypeID AS NUMERIC(9,0)
SELECT @numIndirectIncomeAccountTypeID = numAccountTypeID FROM dbo.AccountTypeDetail WHERE vcAccountCode = '010302'  AND numDomainID = @numDomainID  
PRINT @numIndirectIncomeAccountTypeID

EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numIncomeAccountTypeID,
    @numParntAcntTypeID =  @numIndirectIncomeAccountTypeID, @vcAccountName = 'Interest Income',
    @vcAccountDescription = 'Interest income',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0
    
--Account with the name of �Dividend income� should be there under indirect income.
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numIncomeAccountTypeID,
    @numParntAcntTypeID =  @numIndirectIncomeAccountTypeID, @vcAccountName = 'Dividend Income',
    @vcAccountDescription = 'Dividend income',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

--------------------------------------------------------------------------------------------------------------------------------------
-- DONE 																															--																																		    
--Under expense side COGS accounts should not be under any other category except COGS so remove the same from other direct expense. --
--Remove �office equipment� from operating expense type.																			--
--------------------------------------------------------------------------------------------------------------------------------------

--Transfer following accounts to Administrative Expense from Operating Expense.
--Printing & Stationary,   telephone & Internet, Income Tax expense, Entertainment, Insurance.
PRINT 34 
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numExpenseAccountTypeID,
    @numParntAcntTypeID = @numAdminExpenseID, @vcAccountName = 'Printing & Stationery',
    @vcAccountDescription = 'Expenses incurred by the entity as a result of printing and stationery',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

PRINT 40
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numExpenseAccountTypeID,
    @numParntAcntTypeID = @numAdminExpenseID, @vcAccountName = 'Telephone & Internet',
    @vcAccountDescription = 'Expenditure incurred from any business-related phone calls, phone lines, or internet connections',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0
        
PRINT 44 
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numExpenseAccountTypeID,
    @numParntAcntTypeID = @numAdminExpenseID, @vcAccountName = 'Income Tax Expense',
    @vcAccountDescription = 'A percentage of total earnings paid to the government',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0 , @numParentAccId = 0

PRINT 25
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numExpenseAccountTypeID,
    @numParntAcntTypeID = @numAdminExpenseID, @vcAccountName = 'Entertainment',
    @vcAccountDescription = 'Expenses paid by company for the business but are not deductable for income tax purposes.',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

PRINT 29
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numExpenseAccountTypeID,
    @numParntAcntTypeID = @numAdminExpenseID, @vcAccountName = 'Insurance',
    @vcAccountDescription = 'Expenses incurred for insuring the business'' assets',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0
           
--Advertising expense should be moved to Selling & Distribution from operating expense.
--SELECT * FROM dbo.AccountTypeDetail WHERE numDomainID = @numDomainID   AND vcAccountCode = '01040102' 
DECLARE @numSDccountTypeID AS NUMERIC(9,0)
SELECT @numSDccountTypeID = numAccountTypeID FROM dbo.AccountTypeDetail WHERE vcAccountCode = '01040102'  AND numDomainID = @numDomainID  
PRINT @numSDccountTypeID
   
PRINT 21
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numExpenseAccountTypeID,
    @numParntAcntTypeID = @numSDccountTypeID, @vcAccountName = 'Advertising',
    @vcAccountDescription = 'Expenses incurred for advertising while trying to increase sales',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0
    
--�Interest Expense� should be under Finance Cost rather than Operating Expense.
--SELECT * FROM dbo.AccountTypeDetail WHERE numDomainID = @numDomainID   AND vcAccountCode = '01040203' 
DECLARE @numFCccountTypeID AS NUMERIC(9,0)
SELECT @numFCccountTypeID = numAccountTypeID FROM dbo.AccountTypeDetail WHERE vcAccountCode = '01040203'  AND numDomainID = @numDomainID  
PRINT @numFCccountTypeID

PRINT 45 
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numExpenseAccountTypeID,
    @numParntAcntTypeID = @numFCccountTypeID, @vcAccountName = 'Interest Expense',
    @vcAccountDescription = 'Any interest expenses paid to your tax authority, business bank accounts or credit card accounts.',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0
    
--Move bank Service charges under finance Costs.
PRINT 22
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numExpenseAccountTypeID,
    @numParntAcntTypeID = @numFCccountTypeID, @vcAccountName = 'Bank Service Charges',
    @vcAccountDescription = 'Fees charged by your bank for transactions regarding your bank account(s).',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0
    
--Move �Payroll tax expense� to Staff expense from operating expense.

--SELECT * FROM dbo.AccountTypeDetail WHERE numDomainID = @numDomainID   AND vcAccountCode = '01040202' 
DECLARE @numSEccountTypeID AS NUMERIC(9,0)
SELECT @numSEccountTypeID = numAccountTypeID FROM dbo.AccountTypeDetail WHERE vcAccountCode = '01040202'  AND numDomainID = @numDomainID  
PRINT @numSEccountTypeID
    
PRINT 38 
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numExpenseAccountTypeID,
    @numParntAcntTypeID = @numSEccountTypeID, @vcAccountName = 'Payroll Tax Expense',
    @vcAccountDescription = '', @monOriginalOpeningBal = $0,
    @monOpeningBal = $0, @dtOpeningDate = @dtTodayDate, @bitActive = 1,
    @numAccountId = 0, @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0,
    @numUserCntId = 1, @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0
    
--Move �Depreciation� from administrative expense to Non-cash Expense.
--SELECT * FROM dbo.AccountTypeDetail WHERE numDomainID = @numDomainID   AND vcAccountCode = '010404' 
DECLARE @numNCEccountTypeID AS NUMERIC(9,0)
SELECT @numNCEccountTypeID = numAccountTypeID FROM dbo.AccountTypeDetail WHERE vcAccountCode = '01040205'  AND numDomainID = @numDomainID  
PRINT @numNCEccountTypeID

PRINT 43 
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numExpenseAccountTypeID,
    @numParntAcntTypeID = @numNCEccountTypeID, @vcAccountName = 'Depreciation',
    @vcAccountDescription = 'The amount of the asset''s cost (based on the useful life) that was consumed during the period',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0
    
--Create new account with the name �Amortization� under non-cash expense.   

PRINT 43 
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numExpenseAccountTypeID,
    @numParntAcntTypeID = @numNCEccountTypeID, @vcAccountName = 'Amortization',
    @vcAccountDescription = 'Amortization',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0
    
--Legal Expense should be under Administrative Expenses.
PRINT 30
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numExpenseAccountTypeID,
    @numParntAcntTypeID = @numAdminExpenseID, @vcAccountName = 'Legal Expenses',
    @vcAccountDescription = 'Expenses incurred on any legal matters',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

--Owner�s contribution, owner�s draw, and common stock should be under Shareholder�s Fund.

--SELECT * FROM dbo.AccountTypeDetail WHERE numDomainID = @numDomainID   AND vcAccountCode = '010502' 
DECLARE @numOCccountTypeID AS NUMERIC(9,0)
SELECT @numOCccountTypeID = numAccountTypeID FROM dbo.AccountTypeDetail WHERE vcAccountCode = '010502'  AND numDomainID = @numDomainID  
PRINT @numOCccountTypeID
        
PRINT 16
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numEquityAccountTypeID,
    @numParntAcntTypeID = @numOCccountTypeID, @vcAccountName = 'Owner''s Contribution',
    @vcAccountDescription = 'Funds contributed by the owner',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

PRINT 17
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numEquityAccountTypeID,
    @numParntAcntTypeID = @numOCccountTypeID, @vcAccountName = 'Owner''s Draw',
    @vcAccountDescription = 'Withdrawals by the owners',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

--SELECT * FROM dbo.AccountTypeDetail WHERE numDomainID = @numDomainID   AND vcAccountCode = '01050102' 
DECLARE @numSFccountTypeID AS NUMERIC(9,0)
SELECT @numSFccountTypeID = numAccountTypeID FROM dbo.AccountTypeDetail WHERE vcAccountCode = '01050102'  AND numDomainID = @numDomainID  
PRINT @numSFccountTypeID

PRINT 19
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numEquityAccountTypeID,
    @numParntAcntTypeID = @numSFccountTypeID, @vcAccountName = 'Common Stock',
    @vcAccountDescription = 'The value of shares purchased by the shareholders',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0
        
--Change the name of �Capital & Reserves� to �Reserves & Surplus�.
--SELECT * FROM dbo.AccountTypeDetail WHERE numDomainID = @numDomainID   AND vcAccountCode = '01050101' 
--UPDATE AccountTypeDetail SET vcAccountType = 'Reserves & Surplus' WHERE numDomainID = @numDomainID   AND vcAccountCode = '01050101' 

--�Profit & Loss� and �retained Earning� should be under Reserves & Surplus.
--SELECT * FROM dbo.AccountTypeDetail WHERE vcAccountCode = '01050101' AND numDomainID = @numDomainID  


--add 2 new COA under : Indirect expense->UnCategorized Expense
--					  : Indirect Income->UnCategorized Income
    
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numIncomeAccountTypeID,
    @numParntAcntTypeID = @numIndirectIncomeAccountTypeID , @vcAccountName = 'UnCategorized Income',
    @vcAccountDescription = 'UnCategorized Income',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

------- Add new Account type UnCategorized Expense and move account "UnCategorized Expense" under it 

DECLARE @numUnCategorizedExpenseID AS NUMERIC(9,0)
SELECT @numUnCategorizedExpenseID = numAccountTypeID FROM dbo.AccountTypeDetail WHERE vcAccountCode = '01040206'  AND numDomainID = @numDomainID  
PRINT 'numUnCategorizedExpenseID : ' + CONVERT(VARCHAR(10),@numUnCategorizedExpenseID)

EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numExpenseAccountTypeID,
    @numParntAcntTypeID = @numUnCategorizedExpenseID , @vcAccountName = 'UnCategorized Expense',
    @vcAccountDescription = 'UnCategorized Expense',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

----------------

--SELECT * FROM dbo.AccountingCharges WHERE numDomainID = 1
DELETE FROM dbo.AccountingCharges WHERE numDomainID = @numDomainID

DECLARE @strSQL AS NVARCHAR(MAX)
DECLARE @ST AS VARCHAR(10)
SELECT @ST = numAccountId FROM dbo.Chart_Of_Accounts WHERE numDomainId = @numDomainId AND vcAccountName = 'Sales Tax Payable'
PRINT 'Sales Tax Payable : ' + @ST

DECLARE @AR AS VARCHAR(10)
SELECT @AR = numAccountId FROM dbo.Chart_Of_Accounts WHERE numDomainId = @numDomainId AND vcAccountName = 'Account Receivable'
PRINT 'Account Receivable : ' + @AR

DECLARE @SC AS VARCHAR(10)
SELECT @SC = numAccountId FROM dbo.Chart_Of_Accounts WHERE numDomainId = @numDomainId AND vcAccountName = 'Sales Clearing'
PRINT 'Sales Clearing : ' + @SC

DECLARE @DG AS VARCHAR(10)
SELECT @DG = numAccountId FROM dbo.Chart_Of_Accounts WHERE numDomainId = @numDomainId AND vcAccountName = 'Sales Discounts'
PRINT 'Sales Discounts : ' + @DG

DECLARE @SI AS VARCHAR(10)
SELECT @SI = numAccountId FROM dbo.Chart_Of_Accounts WHERE numDomainId = @numDomainId AND vcAccountName = 'Shipping Income'
PRINT 'Shipping Income: ' + @SI

DECLARE @LC AS VARCHAR(10)
SELECT @LC = numAccountId FROM dbo.Chart_Of_Accounts WHERE numDomainId = @numDomainId AND vcAccountName = 'Late Charges'
PRINT 'Late Charges :' + @LC

DECLARE @AP AS VARCHAR(10)
SELECT @AP = numAccountId FROM dbo.Chart_Of_Accounts WHERE numDomainId = @numDomainId AND vcAccountName = 'Accounts Payable'
PRINT 'Account Payable : ' + @AP

DECLARE @UF AS VARCHAR(10)
SELECT @UF = numAccountId FROM dbo.Chart_Of_Accounts WHERE numDomainId = @numDomainId AND vcAccountName = 'UnDeposited Funds'
PRINT 'UnDeposited Funds : ' + @UF

DECLARE @BE AS VARCHAR(10)
SELECT @BE = numAccountId FROM dbo.Chart_Of_Accounts WHERE numDomainId = @numDomainId AND vcAccountName = 'Billable Time & Expenses'
PRINT 'Billable Time & Expenses : ' + @BE

DECLARE @CG AS VARCHAR(10)
SELECT @CG = numAccountId FROM dbo.Chart_Of_Accounts WHERE numDomainId = @numDomainId AND vcAccountName = 'Cost of Goods Sold'
PRINT 'Cost of Goods Sold : ' + @CG

DECLARE @EP AS VARCHAR(10)
SELECT @EP = numAccountId FROM dbo.Chart_Of_Accounts WHERE numDomainId = @numDomainId AND vcAccountName = 'Employee Payroll Expense'
PRINT 'Employee Payroll Expense : ' + @EP

DECLARE @CP AS VARCHAR(10)
SELECT @CP = numAccountId FROM dbo.Chart_Of_Accounts WHERE numDomainId = @numDomainId AND vcAccountName = 'Contract Employee Payroll Expense'
PRINT 'Contract Employee Payroll Expense : ' + @CP

DECLARE @PL AS VARCHAR(10)
SELECT @PL = ISNULL(numAccountId,0) FROM dbo.Chart_Of_Accounts WHERE numDomainId = @numDomainId AND vcAccountName = 'Wages & Salaries Payable'
--SELECT ISNULL(numAccountId,0) FROM dbo.Chart_Of_Accounts WHERE numDomainId = 1 AND vcAccountName = 'Payroll Liability'
--SET @WSP = '1010'
PRINT 'Wages & Salaries Payable : ' + @PL

DECLARE @WP AS VARCHAR(10)
SELECT @WP = ISNULL(numAccountId,0) FROM dbo.Chart_Of_Accounts WHERE numDomainId = @numDomainId AND vcAccountName = 'Work in Progress'
--SET @WP = '1010'
PRINT 'Wok in Progress : ' + @WP

DECLARE @DP AS VARCHAR(10)       
SELECT @DP = ISNULL(numAccountId,0) FROM dbo.Chart_Of_Accounts WHERE numDomainId = @numDomainId AND vcAccountName = 'Deduction from Employee Payroll'
--SET @DP = '1010'
PRINT 'Deductions form Employee Payroll : ' + @DP

DECLARE @UI AS VARCHAR(10)       
SELECT @UI = ISNULL(numAccountId,0) FROM dbo.Chart_Of_Accounts WHERE numDomainId = @numDomainId AND vcAccountName = 'UnCategorized Income'
--SET @DP = '1010'
PRINT 'UnCategorized Income : ' + @UI

DECLARE @UE AS VARCHAR(10)       
SELECT @UE = ISNULL(numAccountId,0) FROM dbo.Chart_Of_Accounts WHERE numDomainId = @numDomainId AND ISNULL(vcAccountName,'') = 'UnCategorized Expense'
--SET @DP = '1010'
PRINT 'UnCategorized Expense : ' + CONVERT(VARCHAR(10),@UE)

DECLARE @RC AS VARCHAR(10)       
SELECT @RC = ISNULL(numAccountId,0) FROM dbo.Chart_Of_Accounts WHERE numDomainId = @numDomainId AND vcAccountName = 'Reconciliation Discrepancies'
--SET @DP = '1010'
PRINT 'Reconciliation Discrepancies : ' + @RC

DECLARE @FE AS VARCHAR(10)       
SELECT @FE = ISNULL(numAccountId,0) FROM dbo.Chart_Of_Accounts WHERE numDomainId = @numDomainId AND vcAccountName = 'Foreign Exchange Gain/Loss'
PRINT 'Foreign Exchange Gain/Loss : ' + ISNULL(@FE,'')

DECLARE @IA AS VARCHAR(10)       
SELECT @IA = ISNULL(numAccountId,0) FROM dbo.Chart_Of_Accounts WHERE numDomainId = @numDomainId AND vcAccountName = 'Inventory Adjustment'
PRINT 'Inventory Adjustment : ' + ISNULL(@IA,'')

DECLARE @OE AS VARCHAR(10)       
SELECT @OE = ISNULL(numAccountId,0) FROM dbo.Chart_Of_Accounts WHERE numDomainId = @numDomainId AND vcAccountName = 'Opening Balance Equity'
PRINT 'Opening Balance Equity : ' + ISNULL(@OE,'')

DECLARE @PC AS VARCHAR(10)       
SELECT @PC = ISNULL(numAccountId,0) FROM dbo.Chart_Of_Accounts WHERE numDomainId = @numDomainId AND vcAccountName = 'Purchase Clearing'

PRINT 'Purchase Clearing : ' + ISNULL(@PC,'')

DECLARE @PV AS VARCHAR(10)       
SELECT @PV = ISNULL(numAccountId,0) FROM dbo.Chart_Of_Accounts WHERE numDomainId = @numDomainId AND vcAccountName = 'Purchase Price Variance'

PRINT 'Purchase Price Variance : ' + ISNULL(@PV,'')


--SET @strSQL = ''
SET @strSQL = ' exec USP_ManageDefaultAccountsForDomain @numDomainID = ' + CONVERT(VARCHAR(10),@numDomainId) + ',@str=''<NewDataSet>
																			  <Table1>
																				<chChargeCode>EP</chChargeCode>
																				<numAccountID>' + ISNULL(@EP,'0') + '</numAccountID>
																			  </Table1>
																			  <Table1>
																				<chChargeCode>CP</chChargeCode>
																				<numAccountID>' + ISNULL(@CP,'0') + '</numAccountID>
																			  </Table1>
																			  <Table1>
																				<chChargeCode>ST</chChargeCode>
																				<numAccountID>' + ISNULL(@ST,'0') + '</numAccountID>
																			  </Table1>
																			  <Table1>
																				<chChargeCode>AR</chChargeCode>
																				<numAccountID>' + ISNULL(@AR,'0') + '</numAccountID>
																			  </Table1>
																			  <Table1>
																				<chChargeCode>DG</chChargeCode>
																				<numAccountID>' + ISNULL(@DG,'0') + '</numAccountID>
																			  </Table1>
																			  <Table1>
																				<chChargeCode>SI</chChargeCode>
																				<numAccountID>' + ISNULL(@SI,'0') + '</numAccountID>
																			  </Table1>
																			  <Table1>
																				<chChargeCode>LC</chChargeCode>
																				<numAccountID>' + ISNULL(@LC,'0') + '</numAccountID>
																			  </Table1>
																			  <Table1>
																				<chChargeCode>AP</chChargeCode>
																				<numAccountID>' + ISNULL(@AP,'0') + '</numAccountID>
																			  </Table1>
																			  <Table1>
																				<chChargeCode>UF</chChargeCode>
																				<numAccountID>' + ISNULL(@UF,'0') + '</numAccountID>
																			  </Table1>
																			  <Table1>
																				<chChargeCode>BE</chChargeCode>
																				<numAccountID>' + ISNULL(@BE,'0') + '</numAccountID>
																			  </Table1>
																			  <Table1>
																				<chChargeCode>CG</chChargeCode>
																				<numAccountID>' + ISNULL(@CG,'0') + '</numAccountID>
																			  </Table1>
																			  <Table1>
																				<chChargeCode>PL</chChargeCode>
																				<numAccountID>' + ISNULL(@PL,'0') + '</numAccountID>
																			  </Table1>
																			  <Table1>
																				<chChargeCode>WP</chChargeCode>
																				<numAccountID>' + ISNULL(@WP,'0') + '</numAccountID>
																			  </Table1>
																			  <Table1>
																				<chChargeCode>DP</chChargeCode>
																				<numAccountID>' + ISNULL(@DP,'0') + '</numAccountID>
																			  </Table1>
																			  <Table1>
																				<chChargeCode>UI</chChargeCode>
																				<numAccountID>' + ISNULL(@UI,'0') + '</numAccountID>
																			  </Table1>
																			  <Table1>
																				<chChargeCode>UE</chChargeCode>
																				<numAccountID>' + ISNULL(@UE,'0') + '</numAccountID>
																			  </Table1>
																			  <Table1>
																				<chChargeCode>RC</chChargeCode>
																				<numAccountID>' + ISNULL(@RC,'0') + '</numAccountID>
																			  </Table1>
																			  <Table1>
																				<chChargeCode>FE</chChargeCode>
																				<numAccountID>' + ISNULL(@FE,'0') + '</numAccountID>
																			  </Table1>
																			  <Table1>
																				<chChargeCode>IA</chChargeCode>
																				<numAccountID>' + ISNULL(@IA,'0') + '</numAccountID>
																			  </Table1>
																			  <Table1>
																				<chChargeCode>OE</chChargeCode>
																				<numAccountID>' + ISNULL(@OE,'0') + '</numAccountID>
																			  </Table1>
																			  <Table1>
																				<chChargeCode>PC</chChargeCode>
																				<numAccountID>' + ISNULL(@PC,'0') + '</numAccountID>
																			  </Table1>
																			  <Table1>
																				<chChargeCode>PV</chChargeCode>
																				<numAccountID>' + ISNULL(@PV,'0') + '</numAccountID>
																			  </Table1>
																			  <Table1>
																				<chChargeCode>SC</chChargeCode>
																				<numAccountID>' + ISNULL(@SC,'0') + '</numAccountID>
																			  </Table1>
																			</NewDataSet>'''

PRINT CAST(@strSQL AS VARCHAR(MAX))
EXEC SP_EXECUTESQL @strSQL

------- Setting up Default Accounts for Income, Asset & COGS of domain. ---------------------------------------
DECLARE @AssetAccountID AS VARCHAR(10)
SELECT @AssetAccountID = numAccountId FROM dbo.Chart_Of_Accounts WHERE vcAccountName = 'Inventory' AND numDomainID = @numDomainID
PRINT 'AssetAccountID : ' + @AssetAccountID

DECLARE @IncomeAccountID AS VARCHAR(10)
SELECT @IncomeAccountID = numAccountId FROM dbo.Chart_Of_Accounts WHERE vcAccountName = 'Sales' AND numDomainID = @numDomainID
PRINT 'IncomeAccountID : ' + @IncomeAccountID

DECLARE @COGSAccountID AS VARCHAR(10)
SELECT @COGSAccountID = numAccountId FROM dbo.Chart_Of_Accounts WHERE vcAccountName = 'Cost of Goods Sold' AND numDomainID = @numDomainID
PRINT 'COGSAccountID : ' + @COGSAccountID

UPDATE dbo.Domain SET numAssetAccID = @AssetAccountID, numIncomeAccID = @IncomeAccountID, numCOGSAccID = @COGSAccountID WHERE numDomainId = @numDomainID
------------------------------------------------------------

DECLARE @numModuleID AS INT

------- UPDATE COARelationships FOR DEFAULT ACCOUNT SETTING FOR Accounts Receivable & Accounts Payable --------
DECLARE @numCustRelationshipID AS NUMERIC(10)
DECLARE @numEmpRelationshipID AS NUMERIC(10)
DECLARE @numVendorRelationShipID AS NUMERIC(10)

DECLARE @numListID AS NUMERIC(10)
SELECT @numListID = numListID FROM dbo.ListMaster WHERE vcListName = 'Relationship'  AND bitFlag = 1
PRINT @numListID

SELECT @numCustRelationshipID = numListItemID FROM dbo.ListDetails WHERE numListID = @numListID
AND constFlag = 1 
--AND numDomainID = @numDomainID
AND vcData = 'Customer'
PRINT @numCustRelationshipID

SELECT @numEmpRelationshipID = numListItemID FROM dbo.ListDetails WHERE numListID = @numListID
AND constFlag = 1 
--AND numDomainID = @numDomainID
AND vcData = 'Employer'
PRINT @numEmpRelationshipID

SELECT @numVendorRelationShipID = numListItemID FROM dbo.ListDetails WHERE numListID = @numListID
AND constFlag = 1 
--AND numDomainID = @numDomainID
AND vcData = 'Vendor'
PRINT @numVendorRelationShipID


DELETE FROM dbo.COARelationships WHERE numDomainID = @numDomainID

INSERT INTO dbo.COARelationships (numRelationshipID,numARParentAcntTypeID,numAPParentAcntTypeID,numARAccountId,numAPAccountId,numDomainID,dtCreateDate,dtModifiedDate,numCreatedBy,numModifiedBy) 
SELECT @numCustRelationshipID,@numARAccountTypeID,@numAPAccountTypeID,@AR,@AP,@numDomainID,GETDATE(),NULL,NULL,NULL
UNION ALL
SELECT @numEmpRelationshipID,@numARAccountTypeID,@numAPAccountTypeID,@AR,@AP,@numDomainID,GETDATE(),NULL,NULL,NULL
UNION ALL
SELECT @numVendorRelationShipID,@numARAccountTypeID,@numAPAccountTypeID,@AR,@AP,@numDomainID,GETDATE(),NULL,NULL,NULL
---------------------------------------------------------------------------------------------------------------------                   
 
------- UPDATE COAShippingMapping FOR DEFAULT SHIPPING METHOD MAPPING SETTING ---------------------------------
--SELECT * FROM dbo.COAShippingMapping WHERE numDomainID = @numDomainID
DELETE FROM dbo.COAShippingMapping WHERE numDomainID = @numDomainID
--SELECT * FROM dbo.Chart_Of_Accounts WHERE vcAccountName = 'Shipping Income' AND numDomainId = @numDomainID AND bitActive = 0
DECLARE @numShipIncomeID AS NUMERIC(10)
SELECT @numShipIncomeID = numAccountId FROM dbo.Chart_Of_Accounts WHERE vcAccountName = 'Shipping Income' AND numDomainId = @numDomainID --AND bitActive = 0
PRINT @numShipIncomeID

INSERT INTO dbo.COAShippingMapping (numShippingMethodID,numIncomeAccountID,numDomainID) 
SELECT 88,@numShipIncomeID,@numDomainID
UNION ALL
SELECT 91,@numShipIncomeID,@numDomainID
UNION ALL
SELECT 90,@numShipIncomeID,@numDomainID

--Script to Correct  Account type for COA -added by chintan
                SELECT  *
                INTO    #TempAccounts
                FROM    ( SELECT    numAccountId,
                                    vcAccountName,
                                    COA.vcAccountCode,
                                    COA.numParntAcntTypeId,
                                    COA.numAcntTypeId OLDnumAcntTypeId,
                                    ( SELECT    ATD.numAccountTypeID
                                      FROM      dbo.AccountTypeDetail ATD
                                      WHERE     ATD.numDomainID = COA.numDomainID
                                                AND ATD.vcAccountCode = SUBSTRING(COA.vcAccountCode, 1, 4)
                                    ) NEWnumAcntTypeId
                          FROM      dbo.Chart_Of_Accounts COA
                          WHERE     COA.numDomainId = @numDomainId
                        ) X
                WHERE   X.OLDnumAcntTypeId <> X.NEWnumAcntTypeId

--                SELECT  *
--                FROM    #TempAccounts

                UPDATE  dbo.Chart_Of_Accounts
                SET     numAcntTypeId = X.NEWnumAcntTypeId
                FROM    #TempAccounts X
                WHERE   X.numAccountID = dbo.Chart_Of_Accounts.numAccountId

                DROP TABLE #TempAccounts
--
--
----- MAPPING CUSTOMER AND VENDOR RELATION WITH ITS DEFAULT COA.
--
--

                DECLARE @numCustCount INT ;

                SET @numCustCount = 0
                SELECT  @numCustCount = COUNT(*)
                FROM    [ListDetails]
                WHERE   [numListID] = 5
                        AND ( constFlag = 1
                              OR [numDomainID] = @numDomainId
                            )
                        AND vcData = 'Customer' ;


                IF @numCustCount > 0 
                    BEGIN
                        INSERT  INTO COARelationships
                                SELECT  ( SELECT    numListItemId
                                          FROM      [ListDetails]
                                          WHERE     [numListID] = 5
                                                    AND ( constFlag = 1
                                                          OR [numDomainID] = @numDomainId
                                                        )
                                                    AND vcData = 'Customer'
                                        ),
                                        numAcntTypeID,
                                        NULL,
                                        numAccountId,
                                        NULL,
                                        @numDomainID,
                                        GETUTCDATE(),
                                        NULL,
                                        @numUserCntId,
                                        NULL
                                FROM    chart_of_accounts
                                WHERE   numDomainID = @numDomainId
                                        AND vcAccountCode = '0101010501'
                    END
                SET @numCustCount = 0
                SELECT  @numCustCount = COUNT(*)
                FROM    [ListDetails]
                WHERE   [numListID] = 5
                        AND ( constFlag = 1
                              OR [numDomainID] = @numDomainId
                            )
                        AND vcData = 'Vendor' ;
                IF @numCustCount > 0 
                    BEGIN
                        INSERT  INTO COARelationships
                                SELECT  ( SELECT    numListItemId
                                          FROM      [ListDetails]
                                          WHERE     [numListID] = 5
                                                    AND ( constFlag = 1
                                                          OR [numDomainID] = @numDomainId
                                                        )
                                                    AND vcData = 'Vendor'
                                        ),
                                        NULL,
                                        numAcntTypeID,
                                        NULL,
                                        numAccountId,
                                        @numDomainID,
                                        GETUTCDATE(),
                                        NULL,
                                        @numUserCntId,
                                        NULL
                                FROM    chart_of_accounts
                                WHERE   numDomainID = @numDomainId
                                        AND vcAccountCode = '0102010201'
                    END
            END                                                    
    END
/****** Object:  StoredProcedure [dbo].[USP_CheckCanbeDeleteOppertunity]    Script Date: 07/26/2008 16:20:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CheckCanbeDeleteOppertunity')
DROP PROCEDURE USP_CheckCanbeDeleteOppertunity
GO
CREATE PROCEDURE [dbo].[USP_CheckCanbeDeleteOppertunity]              
	@numOppId as numeric(9),
	@tintError AS TINYINT=0 output
as              
              
DECLARE @OppType AS VARCHAR(2)   
DECLARE @itemcode AS NUMERIC     
DECLARE @numWareHouseItemID AS NUMERIC                                 
DECLARE @numUnits AS NUMERIC                                            
DECLARE @numQtyShipped AS NUMERIC
DECLARE @onHand AS NUMERIC,@onOrder AS NUMERIC                                                                                                                           
DECLARE @numoppitemtCode AS NUMERIC(9)
DECLARE @Kit AS BIT                   
DECLARE @LotSerial AS BIT                   
DECLARE @numQtyReceived AS NUMERIC(9)
declare @tintShipped as tinyint 

set @tintError=0          
select @OppType=tintOppType,@tintShipped=tintShipped from OpportunityMaster where numOppId=@numOppId
 
IF @OppType=2
BEGIN
 select top 1 @numoppitemtCode=numoppitemtCode,@itemcode=OI.numItemCode,@numUnits=ISNULL(numUnitHour,0),@numQtyShipped=ISNULL(numQtyShipped,0),@numQtyReceived=ISNULL(numUnitHourReceived,0),
 @numWareHouseItemID=numWarehouseItmsID,@Kit= (case when bitKitParent=1 and bitAssembly=1 then 0 when bitKitParent=1 then 1 else 0 end),
 @LotSerial=(CASE WHEN I.bitLotNo=1 OR I.bitSerialized=1 THEN 1 ELSE 0 END) from OpportunityItems OI                                            
 join Item I                                            
 on OI.numItemCode=I.numItemCode and numOppId=@numOppId and (bitDropShip=0 or bitDropShip is null)                                        
 where charitemtype='P'  order by OI.numoppitemtCode

while @numoppitemtCode>0                               
 begin  
  select @onHand = ISNULL(numOnHand, 0),@onOrder=ISNULL(numonOrder,0) from WareHouseItems where numWareHouseItemID=@numWareHouseItemID 
  
				IF @onHand < @numQtyReceived
				 begin  
					if @tintError=0 set @tintError=1
				 end  
				 
				 IF @onOrder < (@numUnits-@numQtyReceived)
				 begin  
					if @tintError=0 set @tintError=1
				 end  
	
  select top 1 @numoppitemtCode=numoppitemtCode,@itemcode=OI.numItemCode,@numUnits=ISNULL(numUnitHour,0),@numQtyShipped=ISNULL(numQtyShipped,0),@numQtyReceived=ISNULL(numUnitHourReceived,0),
  @numWareHouseItemID=numWarehouseItmsID,@Kit= (case when bitKitParent=1 and bitAssembly=1 then 0 when bitKitParent=1 then 1 else 0 end),
  @LotSerial=(CASE WHEN I.bitLotNo=1 OR I.bitSerialized=1 THEN 1 ELSE 0 END) from OpportunityItems OI                                            
  join Item I                                            
  on OI.numItemCode=I.numItemCode and numOppId=@numOppId                                          
  where charitemtype='P' and OI.numoppitemtCode>@numoppitemtCode and (bitDropShip=0 or bitDropShip is null) order by OI.numoppitemtCode                                            
    
  if @@rowcount=0 set @numoppitemtCode=0    
 end  
END                                           
  
GO
/****** Object:  StoredProcedure [dbo].[USP_CheckCanbeReOpenOppertunity]    Script Date: 07/26/2008 16:20:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CheckCanbeReOpenOppertunity')
DROP PROCEDURE USP_CheckCanbeReOpenOppertunity
GO
CREATE PROCEDURE [dbo].[USP_CheckCanbeReOpenOppertunity]              
@intOpportunityId as numeric(9),
@bitCheck AS BIT=0
as              
              
DECLARE @OppType AS VARCHAR(2)   
DECLARE @Sel AS int   
DECLARE @itemcode AS NUMERIC     
DECLARE @numWareHouseItemID AS NUMERIC                                 
DECLARE @numUnits AS NUMERIC                                            
DECLARE @numQtyShipped AS NUMERIC
DECLARE @onHand AS NUMERIC                                                                                                                          
DECLARE @numoppitemtCode AS NUMERIC(9)
DECLARE @Kit AS BIT                   
DECLARE @LotSerial AS BIT                   
DECLARE @numQtyReceived AS NUMERIC(9)

set @Sel=0          
select @OppType=tintOppType from OpportunityMaster where numOppId=@intOpportunityId
 
IF @OppType=2
BEGIN
 select top 1 @numoppitemtCode=numoppitemtCode,@itemcode=OI.numItemCode,@numUnits=ISNULL(numUnitHour,0),@numQtyShipped=ISNULL(numQtyShipped,0),@numQtyReceived=ISNULL(numUnitHourReceived,0),
 @numWareHouseItemID=numWarehouseItmsID,@Kit= (case when bitKitParent=1 and bitAssembly=1 then 0 when bitKitParent=1 then 1 else 0 end),
 @LotSerial=(CASE WHEN I.bitLotNo=1 OR I.bitSerialized=1 THEN 1 ELSE 0 END) from OpportunityItems OI                                            
 join Item I                                            
 on OI.numItemCode=I.numItemCode and numOppId=@intOpportunityId and (bitDropShip=0 or bitDropShip is null)                                        
 where charitemtype='P'  order by OI.numoppitemtCode

while @numoppitemtCode>0                               
 begin  
  select @onHand = ISNULL(numOnHand, 0) from WareHouseItems where numWareHouseItemID=@numWareHouseItemID                                              
   	 IF @onHand < @numUnits --- @numQtyReceived) 
     begin  
		if @Sel=0 set @Sel=1
     end  
	
  select top 1 @numoppitemtCode=numoppitemtCode,@itemcode=OI.numItemCode,@numUnits=ISNULL(numUnitHour,0),@numQtyShipped=ISNULL(numQtyShipped,0),@numQtyReceived=ISNULL(numUnitHourReceived,0),
  @numWareHouseItemID=numWarehouseItmsID,@Kit= (case when bitKitParent=1 and bitAssembly=1 then 0 when bitKitParent=1 then 1 else 0 end),
  @LotSerial=(CASE WHEN I.bitLotNo=1 OR I.bitSerialized=1 THEN 1 ELSE 0 END) from OpportunityItems OI                                            
  join Item I                                            
  on OI.numItemCode=I.numItemCode and numOppId=@intOpportunityId                                          
  where charitemtype='P' and OI.numoppitemtCode>@numoppitemtCode and (bitDropShip=0 or bitDropShip is null) order by OI.numoppitemtCode                                            
    
  if @@rowcount=0 set @numoppitemtCode=0    
 end  
END      


 IF EXISTS ( SELECT  *
			FROM    dbo.ReturnHeader
			WHERE   numOppId = @intOpportunityId)         
	BEGIN
		SET @Sel = -1 * @OppType;
	END	
                                    
  
select @Sel
GO
/****** Object:  StoredProcedure [dbo].[USP_OPPCheckCanBeShipped]    Script Date: 07/26/2008 16:20:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON            
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CheckOrderedAndInvoicedOrBilledQty')
DROP PROCEDURE USP_CheckOrderedAndInvoicedOrBilledQty
GO
CREATE PROCEDURE [dbo].[USP_CheckOrderedAndInvoicedOrBilledQty]              
@numOppID AS NUMERIC(18,0)
AS             
BEGIN
	DECLARE @TMEP TABLE
	(
		ID INT,
		vcDescription VARCHAR(100),
		bitTrue BIT
	)

	INSERT INTO 
		@TMEP
	VALUES
		(1,'Fulfillment bizdoc is added but not yet fulfilled',0),
		(2,'Invoice/Bill and Ordered Qty is not same',0),
		(3,'Ordered & Fulfilled Qty is not same',0)

	DECLARE @tintOppType AS TINYINT
	DECLARE @tintOppStatus AS TINYINT

	SELECT @tintOppType=tintOppType,@tintOppStatus=tintOppStatus FROM OpportunityMaster WHERe numOppID=@numOppID


	-- CHECK IF FULFILLMENT BIZDOC IS ADDED TO SALES ORDER BUT IT IS NOT FULFILLED FROM SALES FULFILLMENT SCREEN
	IF @tintOppType = 1 AND @tintOppStatus = 1  --SALES ORDER
	BEGIN
		IF (SELECT COUNT(*) FROM OpportunityBizDocs WHERE numOppId=@numOppID AND numBizDocId=296 AND ISNULL(bitFulFilled,0) = 0) > 0
		BEGIN
			UPDATE @TMEP SET bitTrue = 1 WHERE ID = 1
		END

		-- CHECK IF ALL ITEMS WHITHIN SALES ORDER ARE FULFILLED(SHIPPED) OR NOT
		IF (SELECT 
				COUNT(*) 
			FROM 
			(
				SELECT
					OI.numoppitemtCode,
					ISNULL(OI.numUnitHour,0) AS OrderedQty,
					ISNULL(TempFulFilled.FulFilledQty,0) AS FulFilledQty
				FROM
					OpportunityItems OI
				INNER JOIN
					Item I
				ON
					OI.numItemCode = I.numItemCode
				OUTER APPLY
				(
					SELECT
						SUM(OpportunityBizDocItems.numUnitHour) AS FulFilledQty
					FROM
						OpportunityBizDocs
					INNER JOIN
						OpportunityBizDocItems 
					ON
						OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
					WHERE
						OpportunityBizDocs.numOppId = @numOppID
						AND ISNULL(OpportunityBizDocs.numBizDocId,0) = 296
						AND ISNULL(OpportunityBizDocs.bitFulFilled,0) = 1
						AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
				) AS TempFulFilled
				WHERE
					OI.numOppID = @numOppID
					AND UPPER(I.charItemType) = 'P'
					AND ISNULL(OI.bitDropShip,0) = 0
			) X
			WHERE
				X.OrderedQty <> X.FulFilledQty) > 0
		BEGIN
			UPDATE @TMEP SET bitTrue = 1 WHERE ID = 3
		END
	END

	-- IF INVOICED/BILLED QTY OF ITEMS IS NOT SAME AS ORDERED QTY THEN ORDER CAN NOT BE SHIPPED/RECEIVED
	IF (SELECT 
			COUNT(*) 
		FROM 
		(
			SELECT
				OI.numoppitemtCode,
				ISNULL(OI.numUnitHour,0) AS OrderedQty,
				ISNULL(TempInvoice.InvoicedQty,0) AS InvoicedQty
			FROM
				OpportunityItems OI
			INNER JOIN
				Item I
			ON
				OI.numItemCode = I.numItemCode
			OUTER APPLY
			(
				SELECT
					SUM(OpportunityBizDocItems.numUnitHour) AS InvoicedQty
				FROM
					OpportunityBizDocs
				INNER JOIN
					OpportunityBizDocItems 
				ON
					OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
				WHERE
					OpportunityBizDocs.numOppId = @numOppID
					AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
					AND ISNULL(OpportunityBizDocs.bitAuthoritativeBizDocs,0) = 1
			) AS TempInvoice
			WHERE
				OI.numOppID = @numOppID
		) X
		WHERE
			X.OrderedQty <> X.InvoicedQty) > 0
	BEGIN
		UPDATE @TMEP SET bitTrue = 1 WHERE ID = 2
	END

	SELECT * FROM @TMEP
END

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_createbizdocs')
DROP PROCEDURE usp_createbizdocs
GO
CREATE PROCEDURE [dbo].[USP_CreateBizDocs]                        
(                                              
 @numOppId as numeric(9)=null,                        
 @numBizDocId as numeric(9)=null,                        
 @numUserCntID as numeric(9)=null,                        
 @numOppBizDocsId as numeric(9)=NULL OUTPUT,
 @vcComments as varchar(1000),
 @bitPartialFulfillment as bit,
 @strBizDocItems as text='' ,
 --@bitDiscountType as bit,
 --@fltDiscount  as float,
 --@monShipCost as money,
 @numShipVia as numeric(9),
 @vcTrackingURL as varchar(1000),
 --@bitBillingTerms as bit,
 --@intBillingDays as integer,
 @dtFromDate AS DATETIME,
 --@bitInterestType as bit,
 --@fltInterest as float,
-- @numShipDoc as numeric(9),
 @numBizDocStatus as numeric(9) = 0,
 @bitRecurringBizDoc AS BIT = NULL,
 @numSequenceId AS VARCHAR(50) = '',
 --@tintTaxOperator AS tinyint,
 @tintDeferred as tinyint=0,
 @monCreditAmount as money= 0 OUTPUT,
 @ClientTimeZoneOffset as int=0,
 @monDealAmount as money= 0 OUTPUT,
 @bitRentalBizDoc as bit=0,
 @numBizDocTempID as numeric(9)=0,
 @vcTrackingNo AS VARCHAR(500),
 --@vcShippingMethod AS VARCHAR(100),
 @vcRefOrderNo AS VARCHAR(100),
 --@dtDeliveryDate AS DATETIME,
 @OMP_SalesTaxPercent AS FLOAT = 0, --(joseph) Added to Get Online Marketplace Sales Tax Percentage
 @numFromOppBizDocsId AS NUMERIC(9)=0,
 @bitTakeSequenceId AS BIT=0,
 @bitAllItems AS BIT=0,
 @bitNotValidateBizDocFulfillment AS BIT=0,
 @fltExchangeRateBizDoc AS FLOAT=0,
 @bitRecur BIT = 0,
 @dtStartDate DATE = NULL,
 @dtEndDate DATE = NULL,
 @numFrequency SMALLINT = 0,
 @vcFrequency VARCHAR(20) = '',
 @numRecConfigID NUMERIC(18,0) = 0,
 @bitDisable bit = 0
)                        
as 

BEGIN TRY

DECLARE @hDocItem as INTEGER
declare @numDivisionID as numeric(9)
DECLARE @numDomainID NUMERIC(9)
DECLARE @tintOppType AS TINYINT	
DECLARE @numFulfillmentOrderBizDocId NUMERIC(9);SET @numFulfillmentOrderBizDocId=296 
	    
IF ISNULL(@fltExchangeRateBizDoc,0)=0
	SELECT @fltExchangeRateBizDoc=fltExchangeRate FROM OpportunityMaster where numOppID=@numOppId
	
select @numDomainID=numDomainID,@numDivisionID=numDivisionID,@tintOppType=tintOppType from OpportunityMaster where numOppID=@numOppId

IF @numBizDocTempID=0
BEGIN
	select @numBizDocTempID=numBizDocTempID from BizDocTemplate where numBizDocId=@numBizDocId 
	and numDomainID=@numDomainID and numOpptype=@tintOppType 
	and tintTemplateType=0 and isnull(bitDefault,0)=1 and isnull(bitEnabled,0)=1
END

if @numOppBizDocsId=0
BEGIN
IF (
	(@bitPartialFulfillment = 1 AND NOT EXISTS (SELECT * FROM   OpportunityBizDocs WHERE  numOppID = @numOppId AND numBizDocId = @numBizDocId AND bitPartialFulfilment = 0))
     OR (NOT EXISTS (SELECT * FROM   OpportunityBizDocs WHERE  numOppID = @numOppId AND numBizDocId = @numBizDocId AND bitPartialFulfilment = 0)
         AND NOT EXISTS (SELECT * FROM   OpportunityBizDocs WHERE  numOppID = @numOppId AND numBizDocId = @numBizDocId AND bitPartialFulfilment = 1))
     OR (@bitRecurringBizDoc = 1) -- Added new flag to add unit qty in case of recurring bizdoc
     OR (@numFromOppBizDocsId >0)
    )
begin                      
		
		
		--DECLARE  @vcBizDocID  AS VARCHAR(100)
        --DECLARE  @numBizMax  AS NUMERIC(9)
        DECLARE @tintShipped AS TINYINT	
        DECLARE @dtShipped AS DATETIME
        DECLARE @bitAuthBizdoc AS BIT
        
        
		select @tintShipped=ISNULL(tintShipped,0),@numDomainID=numDomainID from OpportunityMaster where numOppId=@numOppId                        
		
		IF @tintOppType = 1 AND @bitNotValidateBizDocFulfillment=0
			EXEC USP_ValidateBizDocFulfillment @numDomainID,@numOppId,@numFromOppBizDocsId,@numBizDocId
		
--		Select @numBizMax=count(*) from  OpportunityBizDocs                        
--		IF @numBizMax > 0
--          BEGIN
--				SELECT @numBizMax = MAX(numOppBizDocsId) FROM   OpportunityBizDocs
--          END
--		SET @numBizMax = @numBizMax
--        SET @vcBizDocID = @vcBizDocID + '-BD-' + CONVERT(VARCHAR(10),@numBizMax)
        IF @tintShipped =1 
			SET @dtShipped = GETUTCDATE();
		ELSE
			SET @dtShipped = null;
		IF @tintOppType = 1 AND (SELECT numAuthoritativeSales FROM [AuthoritativeBizDocs] WHERE numDomainID = @numDomainID) = @numBizDocId
			SET @bitAuthBizdoc = 1
		ELSE if @tintOppType = 2 AND (SELECT numAuthoritativePurchase FROM [AuthoritativeBizDocs] WHERE numDomainID = @numDomainID) = @numBizDocId
			SET @bitAuthBizdoc = 1
		ELSE 
			SET @bitAuthBizdoc = 0
		
		Declare @dtCreatedDate as datetime;SET @dtCreatedDate=Getutcdate()
		IF @tintDeferred=1
		BEGIn
			SET @dtCreatedDate=DateAdd(minute, @ClientTimeZoneOffset,@dtFromDate)
		END
	
	
		IF @bitTakeSequenceId=1
		BEGIN
			--Sequence #
			CREATE TABLE #tempSequence (numSequenceId bigint )
			INSERT INTO #tempSequence EXEC [dbo].[USP_GetScalerValue] @numDomainID ,33,@numBizDocId,0
			SELECT @numSequenceId =ISNULL(numSequenceId,0) FROM #tempSequence
			DROP TABLE #tempSequence
			
			--BizDoc Template ID
			CREATE TABLE #tempBizDocTempID (numBizDocTempID NUMERIC(9) )
			INSERT INTO #tempBizDocTempID EXEC [dbo].[USP_GetBizDocTemplateList] @numDomainID,@numBizDocId,1,1
			SELECT @numBizDocTempID =ISNULL(numBizDocTempID,0) FROM #tempBizDocTempID
			DROP TABLE #tempBizDocTempID
		END	
			
			
		IF @numFromOppBizDocsId>0 --If Created from Sales Fulfillment Workflow
		BEGIN
			INSERT INTO OpportunityBizDocs
                   (numOppId,numBizDocId,numCreatedBy,dtCreatedDate,numModifiedBy,dtModifiedDate,vcComments,bitPartialFulfilment,
                    monShipCost,numShipVia,vcTrackingURL,dtFromDate,numBizDocStatus,[dtShippedDate],
                    [bitAuthoritativeBizDocs],tintDeferred,bitRentalBizDoc,numBizDocTempID,/*vcTrackingNo,vcShippingMethod,dtDeliveryDate,*/vcRefOrderNo,numSequenceId,numBizDocStatusOLD,bitAutoCreated,[numMasterBizdocSequenceID])
            select @numOppId,@numBizDocId,@numUserCntID,Getutcdate(),@numUserCntID,Getutcdate(),@vcComments,@bitPartialFulfillment,
                    monShipCost,numShipVia,vcTrackingURL,dtFromDate,0,[dtShippedDate],
                    @bitAuthBizdoc,tintDeferred,bitRentalBizDoc,@numBizDocTempID,/*vcTrackingNo,vcShippingMethod,dtDeliveryDate,*/vcRefOrderNo,@numSequenceId,0,1,@numSequenceId
		    from OpportunityBizDocs OBD where  numOppId=@numOppId AND OBD.numOppBizDocsId=@numFromOppBizDocsId
		             
		END
		ELSE
		BEGIN
			INSERT INTO OpportunityBizDocs
                   (numOppId,numBizDocId,
                    --vcBizDocID,
                    numCreatedBy,
                    dtCreatedDate,
                    numModifiedBy,
                    dtModifiedDate,
                    vcComments,
                    bitPartialFulfilment,
                    --bitDiscountType,
                    --fltDiscount,
                    --monShipCost,
                    numShipVia,
                    vcTrackingURL,
                    --bitBillingTerms,
                    --intBillingDays,
                    dtFromDate,
                    --bitInterestType,
                    --fltInterest,
--                    numShipDoc,
                    numBizDocStatus,
                    --vcBizDocName,
                    [dtShippedDate],
                    [bitAuthoritativeBizDocs],tintDeferred,bitRentalBizDoc,numBizDocTempID,vcTrackingNo,
                    --vcShippingMethod,dtDeliveryDate,
                    vcRefOrderNo,numSequenceId,numBizDocStatusOLD,fltExchangeRateBizDoc,bitAutoCreated,[numMasterBizdocSequenceID])
        VALUES     (@numOppId,
                    @numBizDocId,
                    --@vcBizDocID,
                    @numUserCntID,
                    @dtCreatedDate,
                    @numUserCntID,
                    Getutcdate(),
                    @vcComments,
                    @bitPartialFulfillment,
                    --@bitDiscountType,
                    --@fltDiscount,
                    --@monShipCost,
                    @numShipVia,
                    @vcTrackingURL,
                    --@bitBillingTerms,
                    --@intBillingDays,
                    @dtFromDate,
                    --@bitInterestType,
                    --@fltInterest,
--                    @numShipDoc,
                    @numBizDocStatus,--@numBizDocStatus,--@numBizDocStatus,
                    --@vcBizDocName,
                    @dtShipped,
                    ISNULL(@bitAuthBizdoc,0),@tintDeferred,@bitRentalBizDoc,@numBizDocTempID,@vcTrackingNo,
                    --@vcShippingMethod,@dtDeliveryDate,
                    @vcRefOrderNo,@numSequenceId,0,@fltExchangeRateBizDoc,0,@numSequenceId)
        END
        
		SET @numOppBizDocsId = @@IDENTITY
		
	--Added By:Sachin Sadhu||Date:27thAug2014	--Purpose :MAke entry in WFA queue 
IF EXISTS (SELECT 1 FROM dbo.OpportunityMaster WHERE numOppId = @numOppID AND tintOppType=1 AND isnull(tintOppStatus,0)=1 )
	BEGIN
		EXEC 
		USP_ManageOpportunityAutomationQueue					@numOppQueueID = 0, -- numeric(18, 0)									@numDomainID = @numDomainID, -- numeric(18, 0)									@numOppId = @numOppID, -- numeric(18, 0)									@numOppBizDocsId = @@IDENTITY, -- numeric(18, 0)									@numOrderStatus = 0, -- numeric(18, 0)							@numUserCntID = @numUserCntID, -- numeric(18, 0)							@tintProcessStatus = 1, -- tinyint								@tintMode = 1 -- TINYINT
   END 
   --end of script
		--Deferred BizDocs : Create Recurring entry only for create Account Journal
		if @tintDeferred=1
		BEGIN
			exec USP_RecurringAddOpportunity @numOppId,@numOppBizDocsId,0,4,@dtCreatedDate,0,@numDomainID,0,0,100,''
		END

		-- Update name template set for bizdoc
		--DECLARE @tintType TINYINT
		--SELECT @tintType = CASE @tintOppType WHEN 1 THEN 3 WHEN 2 THEN 4 END;
		EXEC dbo.USP_UpdateBizDocNameTemplate @tintOppType,@numDomainID,@numOppBizDocsId

		IF @numFromOppBizDocsId>0
		BEGIN
			insert into                       
		   OpportunityBizDocItems                                                                          
  		   (numOppBizDocID,numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,vcAttributes,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,vcNotes,/*vcTrackingNo,*/bitEmbeddedCost,monEmbeddedCost,/*monShipCost,vcShippingMethod,dtDeliveryDate,*/dtRentalStartDate,dtRentalReturnDate,tintTrackingStatus)
		   select @numOppBizDocsId,OBDI.numOppItemID,OI.numItemCode,OI.numUnitHour,OI.monPrice,OI.monTotAmount,OI.vcItemDesc,OI.numWarehouseItmsID,OBDI.vcType,
		   OBDI.vcAttributes,OBDI.bitDropShip,OBDI.bitDiscountType,OI.fltDiscount,OI.monTotAmtBefDiscount,OBDI.vcNotes,/*OBDI.vcTrackingNo,*/
		   OBDI.bitEmbeddedCost,OBDI.monEmbeddedCost,/*OBDI.monShipCost,OBDI.vcShippingMethod,OBDI.dtDeliveryDate,*/OBDI.dtRentalStartDate,OBDI.dtRentalReturnDate,OBDI.tintTrackingStatus
		   from OpportunityBizDocs OBD JOIN OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId=OBDI.numOppBizDocID 
		   JOIN OpportunityItems OI ON OBDI.numOppItemID=OI.numoppitemtCode
		   JOIN Item I ON OBDI.numItemCode=I.numItemCode
		   where  OBD.numOppId=@numOppId AND OBD.numOppBizDocsId=@numFromOppBizDocsId
		   AND OI.numoppitemtCode NOT IN(SELECT OBDI.numOppItemID FROM OpportunityBizDocs OBD JOIN dbo.OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId=OBDI.numOppBizDocId WHERE numOppId=@numOppId AND numBizDocId=@numBizDocId)
		END
		ELSE IF /*@bitPartialFulfillment=1 or*/ (@bitPartialFulfillment=1 and DATALENGTH(@strBizDocItems)>2)
			BEGIN
				
			if DATALENGTH(@strBizDocItems)>2
			begin
				EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strBizDocItems 

					IF @bitRentalBizDoc=1
					BEGIN
						update OI set OI.monPrice=OBZ.monPrice,OI.monTotAmount=OBZ.monTotAmount,OI.monTotAmtBefDiscount=OBZ.monTotAmount
						from OpportunityItems OI
						Join (SELECT * FROM OPENXML (@hDocItem,'/NewDataSet/BizDocItems',2)                                                                          
							WITH ( OppItemID numeric(9),Quantity numeric(18,10),monPrice money,monTotAmount money)) OBZ
						on OBZ.OppItemID=OI.numoppitemtCode
						where OBZ.OppItemID=OI.numoppitemtCode
					END
	
					insert into                       
				   OpportunityBizDocItems                                                                          
				   (numOppBizDocID,numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,vcAttributes,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,vcNotes,/*vcTrackingNo,vcShippingMethod,monShipCost,dtDeliveryDate,*/dtRentalStartDate,dtRentalReturnDate)
				   
				   select @numOppBizDocsId,numoppitemtCode,numItemCode,Quantity,CASE WHEN @tintOppType=2 THEN OBZ.monPrice ELSE OI.monPrice END,CASE WHEN @tintOppType=2 THEN OBZ.monPrice * Quantity ELSE (monTotAmount/numUnitHour)*Quantity END,vcItemDesc,numWarehouseItmsID,vcType,vcAttributes,bitDropShip,bitDiscountType,
				   case when bitDiscountType=0 then fltDiscount when bitDiscountType=1 then (fltDiscount/numUnitHour)*Quantity else fltDiscount end ,CASE WHEN @tintOppType=2 THEN OBZ.monPrice * Quantity ELSE (monTotAmtBefDiscount/numUnitHour)*Quantity END,Notes,/*TrackingNo,vcShippingMethod,monShipCost,dtDeliveryDate,*/dtRentalStartDate,dtRentalReturnDate  from OpportunityItems OI
				   Join (SELECT * FROM OPENXML (@hDocItem,'/NewDataSet/BizDocItems',2)                                                                          
				   WITH                       
				   (OppItemID numeric(9),                               
				    Quantity numeric(18,10),
					Notes varchar(500),
					--TrackingNo varchar(500),
					--vcShippingMethod varchar(100),
					--monShipCost money,
					--dtDeliveryDate datetime
					monPrice MONEY,
					dtRentalStartDate datetime,dtRentalReturnDate datetime
					)) OBZ
				   on OBZ.OppItemID=OI.numoppitemtCode
				   where  numOppId=@numOppId AND ISNULL(OI.numUnitHour,0)>0 AND ISNULL(OI.numUnitHour,0)>0
				   
				EXEC sp_xml_removedocument @hDocItem 
			END

		end
		ELSE
		BEGIN
		   	INSERT INTO OpportunityBizDocItems                                                                          
  			(
				numOppBizDocID,
				numOppItemID,
				numItemCode,
				numUnitHour,
				monPrice,
				monTotAmount,
				vcItemDesc,
				numWarehouseItmsID,
				vcType,
				vcAttributes,
				bitDropShip,
				bitDiscountType,
				fltDiscount,
				monTotAmtBefDiscount
			)
			SELECT 
				@numOppBizDocsId,
				numoppitemtCode,
				OI.numItemCode,
				(CASE @bitRecurringBizDoc 
					WHEN 1 
					THEN 1 
					ELSE (OI.numUnitHour - ISNULL(TempBizDoc.numUnitHour,0)) 
				END) AS numUnitHour,
				OI.monPrice,
			    (CASE @bitRecurringBizDoc 
					WHEN 1 THEN monPrice * 1 
					ELSE (OI.monTotAmount/OI.numUnitHour) * (OI.numUnitHour - ISNULL(TempBizDoc.numUnitHour,0))
				END) AS monTotAmount,
				vcItemDesc,
				numWarehouseItmsID,
				vcType,vcAttributes,
				bitDropShip,
				bitDiscountType,
				(CASE 
					WHEN bitDiscountType=0 THEN fltDiscount 
					WHEN bitDiscountType=1 THEN ((fltDiscount/OI.numUnitHour) * (OI.numUnitHour - ISNULL(TempBizDoc.numUnitHour,0)))
					ELSE fltDiscount 
				END),
				(monTotAmtBefDiscount/OI.numUnitHour) * (OI.numUnitHour - ISNULL(TempBizDoc.numUnitHour,0)) 
			FROM 
				OpportunityItems OI
			JOIN 
				[dbo].[Item] AS I 
			ON 
				I.[numItemCode] = OI.[numItemCode]
			OUTER APPLY
				(
					SELECT 
						SUM(OBDI.numUnitHour) AS numUnitHour
					FROM 
						OpportunityBizDocs OBD 
					JOIN 
						dbo.OpportunityBizDocItems OBDI 
					ON 
						OBDI.numOppBizDocId  = OBD.numOppBizDocsId
						AND OBDI.numOppItemID = OI.numoppitemtCode
					WHERE 
						numOppId=@numOppId 
						AND numBizDocId=@numBizDocId
				) TempBizDoc
			WHERE  
				numOppId=@numOppId 
				AND ISNULL(OI.numUnitHour,0) > 0
				AND (ISNULL(OI.numUnitHour,0) <> ISNULL(TempBizDoc.numUnitHour,0))
		END


               
--		insert into OpportunityBizDocTaxItems(numOppBizDocID, numTaxItemID, fltPercentage)
--		select @numOppBizDocsId,numTaxItemID,dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,numTaxItemID,@numOppId,0) from TaxItems
--        where numDomainID= @numDomainID
--        union 
--        select @numOppBizDocsId,0,dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,0,@numOppId,0)

		SET @numOppBizDocsID = @numOppBizDocsId
		--select @numOppBizDocsId
	end
	ELSE
	  BEGIN
  			SET @numOppBizDocsId=0
			SET @monDealAmount = 0
			SET @monCreditAmount = 0
			RAISERROR ('NOT_ALLOWED',16,1);
	  END
--	IF @tintTaxOperator = 3 -- Remove default Sales Tax percentage defined and Add Sales Tax percentage Used for Online Market Place
--		BEGIN
--				DELETE FROM [OpportunityBizDocTaxItems] WHERE [numOppBizDocID] = @numOppBizDocsId AND [numTaxItemID] = 0 
--				insert into OpportunityBizDocTaxItems(numOppBizDocID, numTaxItemID, fltPercentage)
--					select @numOppBizDocsId,0,@OMP_SalesTaxPercent
--		END	
end
else
BEGIN
	  UPDATE OpportunityBizDocs
	  SET    numModifiedBy = @numUserCntID,
			 dtModifiedDate = Getdate(),
			 vcComments = @vcComments,
			 --bitDiscountType = @bitDiscountType,
			 --fltDiscount = @fltDiscount,
			 --monShipCost = @monShipCost,
			 numShipVia = @numShipVia,
			 vcTrackingURL = @vcTrackingURL,
			 --bitBillingTerms = @bitBillingTerms,
			 --intBillingDays = @intBillingDays,
			 dtFromDate = @dtFromDate,
			 --bitInterestType = @bitInterestType,
			 --fltInterest = @fltInterest,
--			 numShipDoc = @numShipDoc,
			 numBizDocStatus = @numBizDocStatus,
			 numSequenceId=@numSequenceId,
			 --[tintTaxOperator]=@tintTaxOperator,
			 numBizDocTempID=@numBizDocTempID,
			 vcTrackingNo=@vcTrackingNo,
			 --vcShippingMethod=@vcShippingMethod,dtDeliveryDate=@dtDeliveryDate,
			 vcRefOrderNo=@vcRefOrderNo,
			 fltExchangeRateBizDoc=@fltExchangeRateBizDoc
			 --,numBizDocStatusOLD=ISNULL(numBizDocStatus,0)
	  WHERE  numOppBizDocsId = @numOppBizDocsId
	  --SELECT @numOppBizDocsId
	--Added By:Sachin Sadhu||Date:27thAug2014	--Purpose :MAke entry in WFA queue IF EXISTS (SELECT 1 FROM dbo.OpportunityMaster WHERE numOppId = @numOppID AND tintOppType=1 AND isnull(tintOppStatus,0)=1 )
	BEGIN
		EXEC 
		USP_ManageOpportunityAutomationQueue					@numOppQueueID = 0, -- numeric(18, 0)									@numDomainID = @numDomainID, -- numeric(18, 0)									@numOppId = @numOppID, -- numeric(18, 0)									@numOppBizDocsId = @numOppBizDocsId, -- numeric(18, 0)									@numOrderStatus = 0, -- numeric(18, 0)							@numUserCntID = @numUserCntID, -- numeric(18, 0)							@tintProcessStatus = 1, -- tinyint								@tintMode = 1 -- TINYINT
   END 
   --end of code
   	IF DATALENGTH(@strBizDocItems)>2
			BEGIN

				EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strBizDocItems 

                    delete from OpportunityBizDocItems  
					where numOppItemID not in (SELECT OppItemID FROM OPENXML (@hDocItem,'/NewDataSet/BizDocItems',2)                                                                          
					WITH                       
				   ( OppItemID numeric(9)
					)) and numOppBizDocID=@numOppBizDocsId  
                  
                     
					IF @bitRentalBizDoc=1
					BEGIN
						update OI set OI.monPrice=OBZ.monPrice,OI.monTotAmount=OBZ.monTotAmount,OI.monTotAmtBefDiscount=OBZ.monTotAmount
						from OpportunityItems OI
						Join (SELECT * FROM OPENXML (@hDocItem,'/NewDataSet/BizDocItems',2)                                                                          
							WITH ( OppItemID numeric(9),Quantity numeric(18,10),monPrice money,monTotAmount money)) OBZ
						on OBZ.OppItemID=OI.numoppitemtCode
						where OBZ.OppItemID=OI.numoppitemtCode
					END

					update OBI set OBI.numUnitHour= Quantity,OBI.monPrice=CASE WHEN @tintOppType=2 THEN OBZ.monPrice ELSE OI.monPrice END,OBI.monTotAmount=CASE WHEN @tintOppType=2 THEN OBZ.monPrice * Quantity ELSE (OI.monTotAmount/OI.numUnitHour)*Quantity END,OBI.vcNotes= Notes,
					OBI.fltDiscount=case when OI.bitDiscountType=0 then OI.fltDiscount when OI.bitDiscountType=1 then (OI.fltDiscount/OI.numUnitHour)*Quantity else OI.fltDiscount end ,
					OBI.monTotAmtBefDiscount=(OI.monTotAmtBefDiscount/OI.numUnitHour)*Quantity ,                                                                    
					/*OBI.vcTrackingNo=TrackingNo,OBI.vcShippingMethod=OBZ.vcShippingMethod,OBI.monShipCost=OBZ.monShipCost,OBI.dtDeliveryDate=OBZ.dtDeliveryDate*/
				    OBI.dtRentalStartDate=OBZ.dtRentalStartDate,OBI.dtRentalReturnDate=OBZ.dtRentalReturnDate
					from OpportunityBizDocItems OBI
					join OpportunityItems OI
					on OBI.numOppItemID=OI.numoppitemtCode
					Join (SELECT * FROM OPENXML (@hDocItem,'/NewDataSet/BizDocItems',2)                                                                          
					WITH                       
				   ( OppItemID numeric(9),                                     
				   Quantity numeric(18,10),
					Notes varchar(500),
					--TrackingNo varchar(500),
					--vcShippingMethod varchar(100),
					--monShipCost money,
					--dtDeliveryDate datetime,
					monPrice MONEY,
					dtRentalStartDate datetime,dtRentalReturnDate datetime
					)) OBZ
				   on OBZ.OppItemID=OI.numoppitemtCode
				   where  OI.numOppId=@numOppId and OBI.numOppBizDocID=@numOppBizDocsId AND ISNULL(OI.numUnitHour,0)>0


                   insert into                       
				   OpportunityBizDocItems                                                                          
				   (numOppBizDocID,numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,vcAttributes,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,vcNotes/*,vcTrackingNo,vcShippingMethod,monShipCost,dtDeliveryDate*/)                      
				   select @numOppBizDocsId,numoppitemtCode,numItemCode,Quantity,CASE WHEN @tintOppType=2 THEN OBZ.monPrice ELSE OI.monPrice END,CASE WHEN @tintOppType=2 THEN OBZ.monPrice * Quantity ELSE (monTotAmount/numUnitHour)*Quantity END,vcItemDesc,numWarehouseItmsID,vcType,vcAttributes,bitDropShip,bitDiscountType,
				   case when bitDiscountType=0 then fltDiscount when bitDiscountType=1 then (fltDiscount/numUnitHour)*Quantity else fltDiscount end ,(monTotAmtBefDiscount/numUnitHour)*Quantity,Notes/*,TrackingNo,vcShippingMethod,monShipCost,dtDeliveryDate*/ from OpportunityItems OI
				   Join (SELECT * FROM OPENXML (@hDocItem,'/NewDataSet/BizDocItems',2)                                                                          
				   WITH                       
				   ( OppItemID numeric(9),                                     
				   Quantity numeric(18,10),
					Notes varchar(500),
					monPrice MONEY
					--TrackingNo varchar(500),
					--vcShippingMethod varchar(100),
					--monShipCost money,
					--dtDeliveryDate DATETIME
					)) OBZ
				   on OBZ.OppItemID=OI.numoppitemtCode and OI.numoppitemtCode not in (select numOppItemID from OpportunityBizDocItems where numOppBizDocID =@numOppBizDocsId)
				   where  numOppId=@numOppId AND ISNULL(OI.numUnitHour,0)>0
                 
				EXEC sp_xml_removedocument @hDocItem 
			END


		
		/*Note: by chintan
			numTaxItemID= 0 which stands for default sales tax
		*/
--		IF @tintTaxOperator = 1 -- Add Sales Tax Blindly which doesn't  depends on if company/item has sales tax enabled
--		BEGIN
--				DECLARE @SalesTax float
--				SELECT @SalesTax = dbo.fn_CalSalesTaxAmt(@numOppBizDocsId,@numDomainID);
--				IF @SalesTax > 0 
--				BEGIN
--					DELETE FROM [OpportunityBizDocTaxItems] WHERE [numOppBizDocID]=@numOppBizDocsId AND [numTaxItemID]=0
--					insert into OpportunityBizDocTaxItems(numOppBizDocID, numTaxItemID, fltPercentage)
--					select @numOppBizDocsId,0,@SalesTax
--				END
--				
--		END
--		ELSE IF @tintTaxOperator = 2 -- Remove Sales Tax Blindly which doesn't  depends on if company/item has sales tax enabled
--		BEGIN
--				DELETE FROM [OpportunityBizDocTaxItems] WHERE [numOppBizDocID] = @numOppBizDocsId AND [numTaxItemID] = 0 
--		END
END

IF @numOppBizDocsId>0
BEGIN
	select @monDealAmount=isnull(dbo.[GetDealAmount](@numOppId,GETUTCDATE(),@numOppBizDocsId),0)

	--Credit Balance
--	Declare @CAError as int;Set @CAError=0
--
--   	Declare @monOldCreditAmount as money;Set @monOldCreditAmount=0
--	Select @monOldCreditAmount=isnull(monCreditAmount,0) from OpportunityBizDocs WHERE  numOppBizDocsId = @numOppBizDocsId
--
--	if @monCreditAmount>0
--	BEGIN
--			IF (@monDealAmount + @monOldCreditAmount) >= @monCreditAmount
--			BEGIN
--				IF exists (select * from [CreditBalanceHistory] where  numOppBizDocsId=@numOppBizDocsId)
--						Update [CreditBalanceHistory] set [monAmount]=@monCreditAmount * -1 where numOppBizDocsId=@numOppBizDocsId
--				ELSE
--						 INSERT INTO [CreditBalanceHistory]
--							(numOppBizDocsId,[monAmount],[dtCreateDate],[dtCreatedBy],[numDomainId],numDivisionID)
--						VALUES (@numOppBizDocsId,@monCreditAmount * -1,GETDATE(),@numUserCntID,@numDomainId,@numDivisionID)
--			END	
--			ELSE
--			BEGIN	
--				SET @monCreditAmount=0
--				SET @CAError=-1
--			END
--	  END
--	  ELSE
--	  BEGIN
--			delete from [CreditBalanceHistory] where numOppBizDocsId=@numOppBizDocsId
--	  END
--	
--	IF @tintOppType=1 --Sales
--	  update DivisionMaster set monSCreditBalance=isnull(monSCreditBalance,0) + isnull(@monOldCreditAmount,0) - isnull(@monCreditAmount,0) where numDivisionID=@numDivisionID and numDomainID=@numDomainID
--    else IF @tintOppType=2 --Purchase
--	  update DivisionMaster set monPCreditBalance=isnull(monPCreditBalance,0) + isnull(@monOldCreditAmount,0) - isnull(@monCreditAmount,0) where numDivisionID=@numDivisionID and numDomainID=@numDomainID
--		

	  --Update OpportunityBizDocs set monCreditAmount=@monCreditAmount   WHERE  numOppBizDocsId = @numOppBizDocsId
	  Update OpportunityBizDocs set vcBizDocID=vcBizDocName + ISNULL(@numSequenceId,''),
			monDealAmount=@monDealAmount
			WHERE  numOppBizDocsId = @numOppBizDocsId

	 --SET @monCreditAmount =Case when @CAError=-1 then -1 else @monCreditAmount END
END

-- 1 if recurrence is enabled for authorative bizdoc
IF ISNULL(@bitRecur,0) = 1
BEGIN
	-- First Check if receurrece is already created for sales order because user should not 
	-- be able to create recurrence on both order and invoice
	IF (SELECT COUNT(*) FROM RecurrenceConfiguration WHERE numOppID = @numOppId AND numType=1) > 0 
	BEGIN
		RAISERROR ('RECURRENCE_ALREADY_CREATED_FOR_ORDER',16,1);
	END
	--User can not create recurrence on bizdoc where all items are added to bizdoc
	ELSE IF (SELECT dbo.CheckAllItemQuantityAddedToBizDoc(@numOppId,@numOppBizDocsID)) = 1
	BEGIN
		RAISERROR ('RECURRENCE_NOT_ALLOWED_ALL_ITEM_QUANTITY_ADDED_TO_INVOICE',16,1);
	END
	ELSE
	BEGIN

		EXEC USP_RecurrenceConfiguration_Insert
			@numDomainID = @numDomainId,
			@numUserCntID = @numUserCntId,
			@numOppID = @numOppID,
			@numOppBizDocID = @numOppBizDocsId,
			@dtStartDate = @dtStartDate,
			@dtEndDate =NULL,
			@numType = 2,
			@vcType = 'Invoice',
			@vcFrequency = @vcFrequency,
			@numFrequency = @numFrequency

		UPDATE OpportunityMaster SET vcRecurrenceType = 'Invoice' WHERE numOppId = @numOppID
	END
END
ELSE IF ISNULL(@numRecConfigID,0) > 0 AND ISNULL(@bitDisable,0)= 1
BEGIN
	UPDATE
		RecurrenceConfiguration
	SET
		bitDisabled = 1,
		numDisabledBy = @numUserCntId,
		dtDisabledDate = GETDATE()
	WHERE	
		numRecConfigID = @numRecConfigID
END 
 
 
	 END TRY
BEGIN CATCH
    DECLARE @ErrorMessage NVARCHAR(4000);
    DECLARE @ErrorSeverity INT;
    DECLARE @ErrorState INT;

    SELECT 
        @ErrorMessage = ERROR_MESSAGE(),
        @ErrorSeverity = ERROR_SEVERITY(),
        @ErrorState = ERROR_STATE();

    -- Use RAISERROR inside the CATCH block to return error
    -- information about the original error that caused
    -- execution to jump to the CATCH block.
    RAISERROR (@ErrorMessage, -- Message text.
               @ErrorSeverity, -- Severity.
               @ErrorState -- State.
               );
END CATCH;
--modified by anoop jayaraj              
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_deleteoppurtunity')
DROP PROCEDURE usp_deleteoppurtunity
GO
CREATE PROCEDURE [dbo].[USP_DeleteOppurtunity]
 @numOppId numeric(9) ,        
 @numDomainID as numeric(9),
 @numUserCntID AS NUMERIC(9)                       
AS                          
             
       
if exists(select * from OpportunityMaster where numOppID=@numOppId and numDomainID=@numDomainID)         
BEGIN 

SET XACT_ABORT ON;

BEGIN TRY 
BEGIN TRAN

	IF (SELECT COUNT(*) FROM RecurrenceConfiguration WHERE numOppID = @numOppId AND numDomainID=@numDomainID AND ISNULL(bitMarkForDelete,0) = 0) > 0
	BEGIN
		RAISERROR ( 'RECURRING ORDER OR BIZDOC', 16, 1 )
		RETURN
	END
	ELSE IF (SELECT COUNT(*) FROM RecurrenceTransaction INNER JOIN RecurrenceConfiguration ON RecurrenceTransaction.numRecConfigID = RecurrenceConfiguration.numRecConfigID WHERE RecurrenceTransaction.numRecurrOppID = @numOppId AND ISNULL(RecurrenceConfiguration.bitDisabled,0) = 0) > 0
	BEGIN	
		RAISERROR ( 'RECURRED ORDER', 16, 1 ) ;
		RETURN ;
	END
	ELSE IF (SELECT COUNT(*) FROM OpportunityBizdocs WHERE numOppId=@numOppId AND numBizDocId=296 AND ISNULL(bitFulfilled,0) = 1) > 0
	BEGIN
		RAISERROR ( 'FULFILLED_ITEMS', 16, 1 ) ;
		RETURN ;
	END

      IF EXISTS ( SELECT  *
                FROM    dbo.CaseOpportunities
                WHERE   numOppID = @numOppId
                        AND numDomainID = @numDomainID )         
        BEGIN
            RAISERROR ( 'CASE DEPENDANT', 16, 1 ) ;
            RETURN ;
        END	
	 IF EXISTS ( SELECT  *
			FROM    dbo.ReturnHeader
			WHERE   numOppId = @numOppId
					AND numDomainId= @numDomainID )         
	BEGIN
		RAISERROR ( 'RETURN_EXIST', 16, 1 ) ;
		RETURN ;
	END	

      
      
      

DECLARE @tintError TINYINT;SET @tintError=0

EXECUTE USP_CheckCanbeDeleteOppertunity @numOppId,@tintError OUTPUT
  
  IF @tintError=1
  BEGIN
  	 RAISERROR ( 'OppItems DEPENDANT', 16, 1 ) ;
     RETURN ;
  END
  
  EXEC dbo.USP_ValidateFinancialYearClosingDate
	@numDomainID = @numDomainID, --  numeric(18, 0)
	@tintRecordType = 2, --  tinyint
	@numRecordID = @numOppId --  numeric(18, 0)
	
  --Credit Balance
   Declare @monCreditAmount as money;Set @monCreditAmount=0
   Declare @monCreditBalance as money;Set @monCreditBalance=0
   Declare @numDivisionID as numeric(9);Set @numDivisionID=0
   Declare @tintOppType as tinyint

   Select @numDivisionID=numDivisionID,@tintOppType=tintOppType from OpportunityMaster WHERE  numOppId=@numOppId  and numDomainID= @numDomainID 
   
    IF @tintOppType=1 --Sales
      Select @monCreditBalance=isnull(monSCreditBalance,0) from DivisionMaster WHERE  numDivisionID=@numDivisionID  and numDomainID= @numDomainID 
    else IF @tintOppType=2 --Purchase
      Select @monCreditBalance=isnull(monPCreditBalance,0) from DivisionMaster WHERE  numDivisionID=@numDivisionID  and numDomainID= @numDomainID 
   
   Select @monCreditAmount=sum(monAmount) from CreditBalanceHistory where numReturnID in (select numReturnID from [Returns] WHERE [numOppId] = @numOppId)
	
	IF ( @monCreditAmount > @monCreditBalance )         
        BEGIN
            RAISERROR ( 'CreditBalance DEPENDANT', 16, 1 ) ;
            RETURN ;
        END	
        
  declare @tintOppStatus as tinyint    
 declare @tintShipped as tinyint                
  select @tintOppStatus=tintOppStatus,@tintShipped=tintShipped from OpportunityMaster where numOppID=@numOppId                
  
  DECLARE @isOrderItemsAvailable AS INT = 0	SELECT 		@isOrderItemsAvailable = COUNT(*) 	FROM    		OpportunityItems OI	JOIN 		dbo.OpportunityMaster OM ON OI.numOppId = OM.numOppId	JOIN 		Item I ON OI.numItemCode = I.numItemCode	WHERE   		(charitemtype='P' OR 1=(CASE WHEN tintOppType=1 THEN 								CASE WHEN ISNULL(I.bitKitParent,0) = 1 AND ISNULL(I.bitAssembly,0) = 1 THEN 0									 WHEN ISNULL(I.bitKitParent,0) = 1 THEN 1									 ELSE 0 END 								ELSE 0 END)) AND OI.numOppId = @numOppId							   AND ( bitDropShip = 0									 OR bitDropShip IS NULL								   )   if (@tintOppStatus=1 and @tintShipped=0) OR  @tintShipped=1                 begin        exec USP_RevertDetailsOpp @numOppId,1,@numUserCntID                  end                                 
                        
  DELETE FROM [OpportunitySalesTemplate] WHERE [numOppId] =@numOppId 
  delete OpportunityLinking where   numChildOppID=  @numOppId or   numParentOppID=    @numOppId          
  delete RECENTITEMS where numRecordID =  @numOppId and chrRecordType='O'             
                

	IF @tintOppType = 1 AND @tintOppStatus = 1 -- SALES ORDER
	BEGIN
		-- MAKE SERIAL/LOT# NUUMBER AVAILABLE FOR USE AS SALES ORDER IS DELETED
		UPDATE WHIDL
			SET WHIDL.numQty = (CASE WHEN Item.bitLotNo = 1 THEN ISNULL(WHIDL.numQty,0) + ISNULL(OWSI.numQty,0) ELSE 1 END)
		FROM 
			WareHouseItmsDTL WHIDL
		INNER JOIN
			OppWarehouseSerializedItem OWSI
		ON
			WHIDL.numWareHouseItmsDTLID = OWSI.numWarehouseItmsDTLID
			AND WHIDL.numWareHouseItemID = OWSI.numWarehouseItmsID
		INNER JOIN
			WareHouseItems
		ON
			WHIDL.numWareHouseItemID = WareHouseItems.numWareHouseItemID
		INNER JOIN
			Item
		ON
			WareHouseItems.numItemID = Item.numItemCode
		WHERE
			OWSI.numOppID = @numOppId
	END
	ELSE IF @tintOppType = 2 AND @tintOppStatus = 1 -- PURCHASE ORDER
	BEGIN
		IF (SELECT
				COUNT(*)
			FROM
				OppWarehouseSerializedItem OWSI
			INNER JOIN
				WareHouseItmsDTL WHIDL
			ON
				OWSI.numWarehouseItmsDTLID = WHIDL.numWareHouseItmsDTLID 
				AND WHIDL.numWareHouseItemID = OWSI.numWarehouseItmsID 
			INNER JOIN
				WareHouseItems
			ON
				WHIDL.numWareHouseItemID = WareHouseItems.numWareHouseItemID
			INNER JOIN
				Item
			ON
				WareHouseItems.numItemID = Item.numItemCode
			WHERE
				OWSI.numOppID = @numOppId
				AND 1 = (CASE 
							WHEN Item.bitLotNo = 1 AND ISNULL(WHIDL.numQty,0) < ISNULL(OWSI.numQty,0) THEN 1 
							WHEN Item.bitSerialized = 1 AND ISNULL(WHIDL.numQty,0) = 0 THEN 1 
							ELSE 0 
						END)
			) > 0
		BEGIN
			RAISERROR ('SERIAL/LOT#_USED', 16, 1 ) ;
		END
		ELSE
		BEGIN
			-- REMOVE SERIAL/LOT# NUMBER FROM INVENTORY BECAUSE PURCHASE ORDER IS DELETED
			UPDATE WHIDL
				SET WHIDL.numQty = (CASE WHEN Item.bitLotNo = 1 THEN ISNULL(WHIDL.numQty,0) - ISNULL(OWSI.numQty,0) ELSE 0 END)
			FROM 
				WareHouseItmsDTL WHIDL
			INNER JOIN
				OppWarehouseSerializedItem OWSI
			ON
				WHIDL.numWareHouseItmsDTLID = OWSI.numWarehouseItmsDTLID
				AND WHIDL.numWareHouseItemID = OWSI.numWarehouseItmsID
			INNER JOIN
				WareHouseItems
			ON
				WHIDL.numWareHouseItemID = WareHouseItems.numWareHouseItemID
			INNER JOIN
				Item
			ON
				WareHouseItems.numItemID = Item.numItemCode
			WHERE
				OWSI.numOppID = @numOppId
		END
	END
									        
  delete from OppWarehouseSerializedItem where numOppID=  @numOppId                  
                  
                        
  delete from OpportunityItemLinking where numNewOppID=@numOppId                      
                         
  delete from OpportunityAddress  where numOppID=@numOppId


  DELETE FROM OpportunityBizDocKitItems WHERE [numOppBizDocItemID] IN 
  (SELECT [numOppBizDocItemID] FROM [OpportunityBizDocItems] WHERE [numOppBizDocID] IN 
	(select numOppBizDocsId from OpportunityBizDocs where numOppId=@numOppId))
	
  delete from OpportunityBizDocItems where numOppBizDocID in                        
  (select numOppBizDocsId from OpportunityBizDocs where numOppId=@numOppId)

--  delete from OpportunityBizDocTaxItems where numOppBizDocID in                        
--  (select numOppBizDocsId from OpportunityBizDocs where numOppId=@numOppId)
--  

  delete from OpportunityItemsTaxItems WHERE numOppID=@numOppID
  delete from OpportunityMasterTaxItems WHERE numOppID=@numOppID
  
  delete from OpportunityBizDocDtl where numBizDocID in                        
  (select numOppBizDocsId from OpportunityBizDocs where numOppId=@numOppId)                        
           
   DELETE FROM OpportunityBizDocsPaymentDetails WHERE [numBizDocsPaymentDetId] IN 
	 (SELECT [numBizDocsPaymentDetId] FROM [OpportunityBizDocsDetails] where numBizDocsId in                        
		(select numOppBizDocsId from OpportunityBizDocs where numOppId=@numOppId)) 
               
  delete from OpportunityBizDocsDetails where numBizDocsId in                        
  (select numOppBizDocsId from OpportunityBizDocs where numOppId=@numOppId)  

DELETE FROM [ShippingReportItems] WHERE [numShippingReportId] IN  (SELECT numShippingReportId from [ShippingReport] WHERE [numOppBizDocId] IN (select numOppBizDocsId from OpportunityBizDocs where numOppId=@numOppId))
DELETE FROM [ShippingBox] WHERE [numShippingReportId] IN  (SELECT numShippingReportId from [ShippingReport] WHERE [numOppBizDocId] IN (select numOppBizDocsId from OpportunityBizDocs where numOppId=@numOppId))
DELETE FROM [ShippingReport] WHERE [numOppBizDocId] IN (select numOppBizDocsId from OpportunityBizDocs where numOppId=@numOppId)

delete from DocumentWorkflow where numDocID in 
(select numOppBizDocsId from OpportunityBizDocs where numOppId=@numOppId) and cDocType='B'      

delete from BizDocAction where numBizActionId in(select numBizActionId from BizActionDetails where numOppBizDocsId in
(select numOppBizDocsId from OpportunityBizDocs where numOppId=@numOppId) and btDocType=1)

delete from BizActionDetails where numOppBizDocsId in
(select numOppBizDocsId from OpportunityBizDocs where numOppId=@numOppId) and btDocType=1

  delete from OpportunityBizDocs where numOppId=@numOppId                        
                          
  delete from OpportunityContact where numOppId=@numOppId                        
                          
  delete from OpportunityDependency where numOpportunityId=@numOppId                        
                          
  delete from TimeAndExpense where numOppId=@numOppId   and numDomainID= @numDomainID                     
  
  --Credit Balance
  delete from CreditBalanceHistory where numReturnID in (select numReturnID from [Returns] WHERE [numOppId] = @numOppId)

    IF @tintOppType=1 --Sales
		update DivisionMaster set monSCreditBalance=isnull(monSCreditBalance,0) - isnull(@monCreditAmount,0) where numDivisionID=@numDivisionID and numDomainID=@numDomainID
    else IF @tintOppType=2 --Purchase
		update DivisionMaster set monPCreditBalance=isnull(monPCreditBalance,0) - isnull(@monCreditAmount,0) where numDivisionID=@numDivisionID and numDomainID=@numDomainID
  

  DELETE FROM [Returns] WHERE [numOppId] = @numOppId
                         
  delete from OpportunityKitItems where numOppId=@numOppId                        
                          
  delete from OpportunityItems where numOppId=@numOppId                        
                          
  DELETE FROM dbo.ProjectProgress WHERE numOppId =@numOppId AND numDomainId=@numDomainID
                          
  delete from OpportunityStageDetails where numOppId=@numOppId
                          
  delete from OpportunitySubStageDetails where numOppId=@numOppId
                                               
  DELETE FROM [OpportunityRecurring] WHERE [numOppId] = @numOppId
  
  DELETE FROM [OpportunityMasterAPI] WHERE [numOppId] = @numOppId
  
  DELETE FROM [WebApiOrderDetails] WHERE [numOppId] = @numOppId
  
  DELETE FROM [WebApiOppItemDetails] WHERE [numOppId] = @numOppId
  
  /*
  added by chintan: reason: when bizdoc is deleted and journal entries are not deleted in exceptional case, 
  it throws Foreign key reference error when deleting order.
  */
  DELETE FROM dbo.General_Journal_Details WHERE numJournalId IN (SELECT numJournal_Id FROM dbo.General_Journal_Header WHERE numOppId=@numOppId AND numDomainId=@numDomainID)
  DELETE FROM dbo.General_Journal_Header WHERE numOppId=@numOppId AND numDomainId= @numDomainID
 
  IF ISNULL(@tintOppStatus,0)=0 OR ISNULL(@isOrderItemsAvailable,0) = 0 OR (SELECT COUNT(*) FROM WareHouseItems_Tracking WHERE numDomainID=@numDomainID AND numReferenceID=@numOppId AND tintRefType IN (3,4) AND vcDescription LIKE '%Deleted%' AND CAST(dtCreatedDate AS DATE) = CAST(GETUTCDATE() AS DATE)) > 0
	delete from OpportunityMaster where numOppId=@numOppId and numDomainID= @numDomainID             
  ELSE
	RAISERROR ( 'INVENTORY IM-BALANCE', 16, 1 ) ;


COMMIT TRAN 

    END TRY 
    BEGIN CATCH		
        DECLARE @strMsg VARCHAR(200)
        SET @strMsg = ERROR_MESSAGE()
        IF ( @@TRANCOUNT > 0 ) 
            BEGIN
             RAISERROR ( @strMsg, 16, 1 ) ;
                ROLLBACK TRAN
                RETURN 1
            END
    END CATCH	

SET XACT_ABORT OFF;
END 

GO
/****** Object:  StoredProcedure [dbo].[Usp_DeleteSalesReturn]    Script Date: 01/22/2009 01:36:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'Usp_DeleteSalesReturn' ) 
    DROP PROCEDURE Usp_DeleteSalesReturn
GO
CREATE PROCEDURE [dbo].[Usp_DeleteSalesReturn]
    @numReturnHeaderID NUMERIC(9),
    @numDomainId NUMERIC(9)
AS 
    BEGIN
	
        IF EXISTS ( SELECT  numReturnHeaderID
                    FROM    ReturnHeader
                    WHERE   numReturnHeaderID = @numReturnHeaderID
                            AND ISNULL(tintReceiveType, 0) <> 0 ) 
            BEGIN
                RAISERROR ( 'CreditMemo_Refund', 16, 1 ) ;
                RETURN ;
            END

		IF EXISTS (SELECT numDepositID FROM dbo.DepositMaster WHERE ISNULL(numReturnHeaderID,0) = @numReturnHeaderID
						 AND numDomainId=@numDomainId
						 And ISNULL(monRefundAmount,0)>0) 
            BEGIN
                RAISERROR ( 'Deposit_Refund_Payment', 16, 1 ) ;
                RETURN ;
            END
            
        IF EXISTS (SELECT numBillPaymentID FROM dbo.BillPaymentHeader WHERE ISNULL(numReturnHeaderID,0) = @numReturnHeaderID
					AND numDomainId=@numDomainId
					And ISNULL(monRefundAmount,0)>0) 
            BEGIN
                RAISERROR ( 'BillPayment_Refund_Payment', 16, 1 ) ;
                RETURN ;
            END

		DELETE FROM OppWarehouseSerializedItem WHERE numReturnHeaderID=@numReturnHeaderID
		
        DELETE  FROM ReturnItems
        WHERE   numReturnHeaderID = @numReturnHeaderID
        
        /*Start : If Refund against Deposit or Bill Payment*/
        DECLARE @numBillPaymentIDRef AS NUMERIC(18);SET @numBillPaymentIDRef=0
        DECLARE @numDepositIDRef AS NUMERIC(18);SET @numDepositIDRef=0
        DECLARE @monAmount AS MONEY;SET @monAmount=0
        
        SELECT @numBillPaymentIDRef=ISNULL(numBillPaymentIDRef,0),
			   @numDepositIDRef=ISNULL(numDepositIDRef,0),
			   @monAmount=ISNULL(monAmount,0)
			 FROM ReturnHeader WHERE numReturnHeaderID = @numReturnHeaderID
        
        IF @numBillPaymentIDRef > 0
        BEGIN
			UPDATE dbo.BillPaymentHeader SET monRefundAmount = ISNULL(monRefundAmount,0) - @monAmount
				WHERE numDomainID=@numDomainId AND numBillPaymentID=@numBillPaymentIDRef
        END

        DELETE  FROM [dbo].[ReturnItems]
        WHERE   [ReturnItems].[numReturnHeaderID] = @numReturnHeaderID 
        DELETE  FROM ReturnHeader
        WHERE   numReturnHeaderID = @numReturnHeaderID
                AND [numDomainID] = @numDomainId 
  
    END

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_DeleteSalesReturnDetails' ) 
    DROP PROCEDURE USP_DeleteSalesReturnDetails
GO

CREATE PROCEDURE [dbo].[USP_DeleteSalesReturnDetails]
    (
      @numReturnHeaderID NUMERIC(9),
      @numReturnStatus	NUMERIC(18,0),
      @numDomainId NUMERIC(9),
      @numUserCntID NUMERIC(9)
    )
AS 
BEGIN
BEGIN TRY
BEGIN TRANSACTION

    DECLARE @tintReturnType TINYINT,@tintReceiveType TINYINT
    
    SELECT @tintReturnType=tintReturnType,@tintReceiveType=tintReceiveType FROM ReturnHeader WHERE numReturnHeaderID = @numReturnHeaderID    
   
    IF (@tintReturnType=1 AND @tintReceiveType=2) OR @tintReturnType=3
    BEGIN
		IF EXISTS(SELECT numDepositID FROM DepositMaster WHERE tintDepositePage=3 AND numReturnHeaderID=@numReturnHeaderID AND ISNULL(monAppliedAmount,0)>0)
		BEGIN
			RAISERROR ( 'CreditMemo_PAID', 16, 1 ) ;
			RETURN ;
		END	
    END
     
    IF (@tintReturnType=2 AND @tintReceiveType=2) OR @tintReturnType=3
    BEGIN
		IF EXISTS(SELECT numReturnHeaderID FROM BillPaymentHeader WHERE numReturnHeaderID=@numReturnHeaderID AND ISNULL(monAppliedAmount,0)>0)
		BEGIN
			RAISERROR ( 'CreditMemo_PAID', 16, 1 ) ;
			RETURN ;
		END	
    END
 
    IF (@tintReturnType=1 or @tintReturnType=2) 
    BEGIN
		DECLARE @i INT = 1
		DECLARE @COUNT AS INT 
		DECLARE @numTempOppID AS NUMERIC(18,0)
		DECLARE @numTempOppItemID AS NUMERIC(18,0)
		DECLARE @bitTempLotNo AS BIT
		DECLARE @numTempQty AS INT 
		DECLARE @numTempWarehouseItemID AS INT
		DECLARE @numTempWareHouseItmsDTLID AS INT
			
		DECLARE @TempOppSerial TABLE
		(
			ID INT IDENTITY(1,1),
			numOppId NUMERIC(18,0),
			numOppItemID NUMERIC(18,0),
			numWarehouseItemID NUMERIC(18,0),
			numWarehouseItmsDTLID NUMERIC(18,0),
			numQty INT,
			bitLotNo NUMERIC(18,0)
		)

		INSERT INTO @TempOppSerial
		(
			numOppId,
			numOppItemID,
			numWarehouseItemID,
			numWarehouseItmsDTLID,
			numQty,
			bitLotNo
		)
		SELECT	
			RH.numOppId,
			RI.numOppItemID,
			OWSIReturn.numWarehouseItmsID,
			OWSIReturn.numWarehouseItmsDTLID,
			OWSIReturn.numQty,
			I.bitLotNo
		FROM
			ReturnHeader RH
		INNER JOIN
			ReturnItems RI
		ON
			RH.numReturnHeaderID = RI.numReturnHeaderID
		INNER JOIN
			OppWarehouseSerializedItem OWSIReturn
		ON
			RH.numReturnHeaderID = OWSIReturn.numReturnHeaderID
			AND RI.numReturnItemID = OWSIReturn.numReturnItemID
		INNER JOIN
			OpportunityItems OI
		ON
			RI.numOppItemID = OI.numoppitemtCode
		INNER JOIN
			Item I
		ON
			OI.numItemCode = I.numItemCode
		WHERE
			RH.numReturnHeaderID = @numReturnHeaderID

		IF @tintReturnType = 1 --SALES RETURN
		BEGIN
			--CHECK IF SERIAL/LOT# ARE NOT USED IN SALES ORDER
			IF (SELECT
						COUNT(*)
				FROM
					WareHouseItmsDTL WHIDL
				INNER JOIN
					OppWarehouseSerializedItem OWSI
				ON
					WHIDL.numWareHouseItmsDTLID = OWSI.numWarehouseItmsDTLID
					AND WHIDL.numWareHouseItemID = OWSI.numWarehouseItmsID
				INNER JOIN
					WareHouseItems
				ON
					WHIDL.numWareHouseItemID = WareHouseItems.numWareHouseItemID
				INNER JOIN
					Item
				ON
					WareHouseItems.numItemID = Item.numItemCode
				WHERE
					numReturnHeaderID=@numReturnHeaderID
					AND 1 = (CASE 
								WHEN Item.bitLotNo = 1 AND ISNULL(WHIDL.numQty,0) < ISNULL(OWSI.numQty,0) THEN 1 
								WHEN Item.bitSerialized = 1 AND ISNULL(WHIDL.numQty,0) = 0 THEN 1 
								ELSE 0 
							END)
				) > 0
			BEGIN
				RAISERROR ('Serial_LotNo_Used', 16, 1 ) ;
			END
			ELSE
			BEGIN
				-- MAKE SERIAL QTY 0 OR DECREASE LOT QUANTITY BECAUSE IT IS USED IN ORDER 
				UPDATE WHIDL
					SET WHIDL.numQty = (CASE WHEN Item.bitLotNo = 1 THEN ISNULL(WHIDL.numQty,0) - ISNULL(OWSI.numQty,0) ELSE 0 END)
				FROM 
					WareHouseItmsDTL WHIDL
				INNER JOIN
					OppWarehouseSerializedItem OWSI
				ON
					WHIDL.numWareHouseItmsDTLID = OWSI.numWarehouseItmsDTLID
					AND WHIDL.numWareHouseItemID = OWSI.numWarehouseItmsID
				INNER JOIN
					WareHouseItems
				ON
					WHIDL.numWareHouseItemID = WareHouseItems.numWareHouseItemID
				INNER JOIN
					Item
				ON
					WareHouseItems.numItemID = Item.numItemCode
				WHERE
					OWSI.numReturnHeaderID=@numReturnHeaderID
			END
		END
		ELSE IF @tintReturnType = 2 --PURCHASE RETURN
		BEGIN
			-- MAKE SERIAL QTY 1 OR INCREASE LOT QUANTITY BECAUSE IT IS RETURNED TO WAREHOUSE
			UPDATE WHIDL
				SET WHIDL.numQty = (CASE WHEN Item.bitLotNo = 1 THEN ISNULL(WHIDL.numQty,0) + ISNULL(OWSI.numQty,0) ELSE 1 END)
			FROM 
				WareHouseItmsDTL WHIDL
			INNER JOIN
				OppWarehouseSerializedItem OWSI
			ON
				WHIDL.numWareHouseItmsDTLID = OWSI.numWarehouseItmsDTLID
				AND WHIDL.numWareHouseItemID = OWSI.numWarehouseItmsID
			INNER JOIN
				WareHouseItems
			ON
				WHIDL.numWareHouseItemID = WareHouseItems.numWareHouseItemID
			INNER JOIN
				Item
			ON
				WareHouseItems.numItemID = Item.numItemCode
			WHERE
				OWSI.numReturnHeaderID=@numReturnHeaderID
		END

		--RE INSERT ENTERIES SERIAL/LOT# ENTERIES WITH ORDER IN OppWarehouseSerializedItem TABLE WHICH ARE ASSOCIATED BECAUSE NOW SERIALS ARE RETURNED TO/FROM WAREHOUSE
		SELECT @COUNT = COUNT(*) FROM @TempOppSerial

		WHILE @i <= @COUNT
		BEGIN
			SELECT
				@bitTempLotNo = bitLotNo,
				@numTempQty = numQty,
				@numTempOppID = numOppId,
				@numTempOppItemID = numOppItemID,
				@numTempWareHouseItmsDTLID = numWareHouseItmsDTLID,
				@numTempWarehouseItemID = numWarehouseItemID
			FROM
				@TempOppSerial
			WHERE
				ID = @i


			IF EXISTS (SELECT * FROM OppWarehouseSerializedItem WHERE numOppID=@numTempOppID AND numOppItemID=@numTempOppItemID AND numWarehouseItmsID=@numTempWarehouseItemID AND numWarehouseItmsDTLID=@numTempWareHouseItmsDTLID)
			BEGIN
				UPDATE 
					OppWarehouseSerializedItem
				SET 
					numQty = ISNULL(numQty,0) + ISNULL(@numTempQty,0)
				WHERE
					numOppID=@numTempOppID 
					AND numOppItemID=@numTempOppItemID 
					AND numWarehouseItmsID=@numTempWarehouseItemID
					AND numWarehouseItmsDTLID=@numTempWareHouseItmsDTLID
			END
			ELSE
			BEGIN
				INSERT INTO OppWarehouseSerializedItem
				(
					numWarehouseItmsDTLID,
					numOppID,
					numOppItemID,
					numWarehouseItmsID,
					numQty
				)
				VALUES
				(
					@numTempWareHouseItmsDTLID,
					@numTempOppID,
					@numTempOppItemID,
					@numTempWarehouseItemID,
					@numTempQty
				)
			END

			SET @i = @i + 1
		END
    END
	
	IF @tintReturnType=1 OR @tintReturnType=2 
    BEGIN
		EXEC usp_ManageRMAInventory @numReturnHeaderID,@numDomainId,@numUserCntID,2
    END
         	              
    UPDATE  
		dbo.ReturnHeader
    SET     
		numReturnStatus = @numReturnStatus,
        numBizdocTempID = CASE WHEN @tintReturnType=1 OR @tintReturnType=2 THEN NULL ELSE numBizdocTempID END,
        vcBizDocName =CASE WHEN @tintReturnType=1 OR @tintReturnType=2 THEN '' ELSE vcBizDocName END ,
        IsCreateRefundReceipt = 0,
        tintReceiveType=0,monBizDocAmount=0, numItemCode = 0
    WHERE   
		numReturnHeaderID = @numReturnHeaderID
        AND	numDomainId = @numDomainId
    
     IF (@tintReturnType=1 AND @tintReceiveType=2) OR @tintReturnType=3
     BEGIN
		DELETE FROM DepositMaster WHERE tintDepositePage=3 AND numReturnHeaderID=@numReturnHeaderID
	 END	
     
     IF (@tintReturnType=2 AND @tintReceiveType=2) OR @tintReturnType=3
     BEGIN
		DELETE FROM BillPaymentHeader WHERE numReturnHeaderID=@numReturnHeaderID
     END
    
	PRINT @tintReceiveType 
	PRINT @tintReturnType

	 IF @tintReceiveType = 1 AND @tintReturnType = 4
	 BEGIN
		DECLARE @monAppliedAmount AS MONEY
		DECLARE @numDepositIDRef AS NUMERIC(18,0)

		SELECT @numDepositIDRef = ISNULL(numDepositIDRef,0) FROM [dbo].[ReturnHeader] AS RH WHERE [RH].[numReturnHeaderID] = @numReturnHeaderID

		SELECT 
			@monAppliedAmount = ISNULL([RH].[monAmount],0)
		FROM 
			[dbo].[ReturnHeader] AS RH 
		WHERE 
			[RH].[numReturnHeaderID] = @numReturnHeaderID 
			AND [RH].[numDomainId] = @numDomainId

		IF @numDepositIDRef > 0			
		BEGIN
			UPDATE 
				[dbo].[DepositMaster] 
			SET 
				[monAppliedAmount] = ISNULL([monAppliedAmount],0) - @monAppliedAmount
				,[monRefundAmount] = [monDepositAmount] - ISNULL((SELECT SUM([ReturnHeader].[monAmount]) FROM [dbo].[ReturnHeader] WHERE [ReturnHeader].[numDepositIDRef] = @numDepositIDRef),0)
				,[numReturnHeaderID] = NULL
			WHERE 
				[numDepositId] = @numDepositIDRef 
				AND [numDomainId] = @numDomainId
		END
	END
	
	DELETE FROM [dbo].[CheckHeader] WHERE [CheckHeader].[numReferenceID] = @numReturnHeaderID
    
	DELETE FROM 
		dbo.General_Journal_Details 
	WHERE 
		numJournalId IN (SELECT numJournal_Id FROM General_Journal_Header WHERE numReturnID=@numReturnHeaderID AND [General_Journal_Header].[numDomainId] = @numDomainId)
		AND [General_Journal_Details].[numDomainId] = @numDomainId
    
	DELETE FROM dbo.General_Journal_Header WHERE numReturnID=@numReturnHeaderID AND [numDomainId] = @numDomainId
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
END
/****** Object:  StoredProcedure [dbo].[usp_getAssets]    Script Date: 07/26/2008 16:16:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getassets')
DROP PROCEDURE usp_getassets
GO
CREATE PROCEDURE [dbo].[usp_getAssets]      
@numOppId as numeric =0 ,      
@numDomainId as numeric =0 ,    
@numItemCode as numeric =0      
as      
-- THIS PROCEDURE IS NOT USED AND ITS LOGIC IS NOT RELEVANT TO BIZ NOW SO DO NOT USE IT
--if @numOppId <> 0    
--begin    
-- (select OI.vcitemName,'' as vcserialno ,0 as numWareHouseItmsDtlId,It.numItemCode,numunithour as unit,0 as [type]      
-- from opportunityitems OI      
-- join Item IT on IT.numItemcode = OI.numItemcode        
-- where OI.numoppid = @numOppid and bitserialized <> 1 and numDomainId = @numDomainId      
-- union      
-- select       
-- OI.vcitemname,vcSerialNo,o.numWareHouseItmsDtlId ,It.numItemCode ,1 as unit,1 as [type]      
-- from  OppWarehouseSerializedItem O                          
-- left join  WareHouseItmsDTL W                        
-- on O.numWarehouseItmsDTLID=W.numWareHouseItmsDTLID                                
-- join opportunityitems OI on OI.numoppitemtcode = o.numOppItemID       
-- join Item IT on IT.numItemcode = OI.numItemcode                
-- where o.numOppID=@numOppid and numDomainId = @numDomainId      
-- ) order by it.numItemCode desc      
    
--end    
--if @numItemCode <>0    
--begin     
-- select  It.numItemCode ,0 as numWareHouseItmsDTLID, vcitemName ,'' vcSerialNo ,1 as unit,0 as type from WareHouseItems                       
-- join Warehouses W                         
-- on W.numWareHouseID=WareHouseItems.numWareHouseID    
-- join Item It on It.numItemCode=WareHouseItems.numItemID                                
-- where numItemID=@numItemCode and bitSerialized=0 and It.numDomainid =@numDomainId    
    
-- union     
-- select numItemID as numItemCode ,numWareHouseItmsDTLID, vcitemName ,isnull(vcSerialNo,'') ,1 as unit,1 as type from WareHouseItmsDTL WDTL                         
-- join WareHouseItems WI                          
-- on WDTL.numWareHouseItemID=WI.numWareHouseItemID      
-- join Warehouses W                    
-- on W.numWareHouseID=WI.numWareHouseID   
--join Item It on It.numItemCode=wi.numItemID           
-- where (tintStatus is null or tintStatus=0)  and  numItemID=@numItemCode and it.numDomainid =@numDomainId    
--end    
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetAutoAssign_OppSerializedItem')
DROP PROCEDURE USP_GetAutoAssign_OppSerializedItem
GO
CREATE PROCEDURE [dbo].[USP_GetAutoAssign_OppSerializedItem]  
    @numOppID as numeric(9)=0,  
    @numOppBizDocsId as numeric(9)=0,
    @numWarehouseItmsID as numeric(9)=0,
	@bitSerialized as bit,
    @bitLotNo as bit,
	@numUnitHour as numeric(18)
as  

Declare @vcSerialNoList varchar(2000);SET @vcSerialNoList=''

Create table #tempWareHouseItmsDTL (                                                                    
ID numeric(18) IDENTITY(1,1) NOT NULL,vcSerialNo VARCHAR(100),numQty [numeric](18, 0),numWareHouseItmsDTLID [numeric](18, 0)                                             
 )   

INSERT INTO #tempWareHouseItmsDTL 
(
	vcSerialNo,numWarehouseItmsDTLID,numQty
)  
SELECT 
	vcSerialNo,numWareHouseItmsDTLID,ISNULL(numQty,0) as TotalQty
FROM  
	WareHouseItmsDTL   
WHERE 
	ISNULL(numQty,0) > 0
	AND numWareHouseItemID=@numWarehouseItmsID  
    AND numWareHouseItmsDTLID NOT IN (SELECT numWarehouseItmsDTLID FROM OppWarehouseSerializedItem WHERE numOppID=@numOppID AND numWarehouseItmsID=@numWarehouseItmsID AND numOppBizDocsId=@numOppBizDocsId) 
ORDER BY TotalQty desc

IF ((Select sum(numQty) from #tempWareHouseItmsDTL)>=@numUnitHour)
BEGIN
DECLARE  @maxWareID NUMERIC(9),@minWareID NUMERIC(9),@vcSerialNo varchar(100)
DECLARE  @numWarehouseItmsDTLID NUMERIC(9),@numQty NUMERIC(9),@numUseQty NUMERIC(9)
	
SELECT @maxWareID = max(ID),@minWareID = min(ID) FROM  #tempWareHouseItmsDTL 

WHILE (@numUnitHour>0 and @minWareID <= @maxWareID)
BEGIN
	SELECT @vcSerialNo =vcSerialNo,@numWarehouseItmsDTLID = numWarehouseItmsDTLID,@numQty = numQty FROM  #tempWareHouseItmsDTL  where ID=@minWareID

	IF @numUnitHour >= @numQty
		BEGIN
			SET @numUseQty=@numQty
	
			SET @numUnitHour=@numUnitHour-@numQty
		END
	else IF @numUnitHour < @numQty
		BEGIN
			SET @numUseQty=@numUnitHour

			SET @numUnitHour=0
		END
		
		IF @bitSerialized=1
			SET @vcSerialNoList = @vcSerialNoList + @vcSerialNo + ','
		ELSE IF @bitLotNo=1
			SET @vcSerialNoList = @vcSerialNoList + @vcSerialNo + '(' + Cast(@numUseQty as varchar(10)) + '),'

SET @minWareID=@minWareID + 1
END

END

drop table #tempWareHouseItmsDTL

select substring(@vcSerialNoList,0,LEN(@vcSerialNoList)) as vcSerialNoList
GO
/****** Object:  StoredProcedure [dbo].[Usp_getCorrespondance]    Script Date: 07/26/2008 16:17:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcorrespondance')
DROP PROCEDURE usp_getcorrespondance
GO
CREATE PROCEDURE [dbo].[Usp_getCorrespondance]                          
@FromDate as datetime,                          
@toDate as datetime,                          
@numContactId as numeric,                          
@vcMessageFrom as varchar(200)='' ,                        
@tintSortOrder as numeric,                        
@SeachKeyword as varchar(100),                        
@CurrentPage as numeric,                        
@PageSize as numeric,                        
@TotRecs as numeric output,                        
@columnName as varchar(100),                        
@columnSortOrder as varchar(50),                        
@filter as numeric,                        
@numdivisionId as numeric =0,              
@numDomainID as numeric(9)=0,              
@numCaseID as numeric(9)=0,              
@ClientTimeZoneOffset INT,
@numOpenRecordID NUMERIC=0,
@tintMode TINYINT=0,
@bitOpenCommu BIT=0
as 

set @vcMessageFrom=replace(@vcMessageFrom,'''','|')
                         
if @filter>0 set @tintSortOrder=  @filter
               
if ((@filter <=2 and @tintSortOrder<=2) or (@tintSortOrder>2 and @filter>2))                 
begin  
                                                                      
 declare @strSql as varchar(8000)       
                                  
  declare @firstRec as integer                                                        
  declare @lastRec as integer                                                        
  set @firstRec= (@CurrentPage-1) * @PageSize                                                        
  set @lastRec= (@CurrentPage*@PageSize+1)                 
  set @strSql=''                
 if (@filter <=2 and @tintSortOrder<=2 and @bitOpenCommu=0)                 
 begin     
 declare @strCondition as varchar(4500);
 declare @vcContactEmail as varchar(100);
              
  if @numdivisionId =0 and  @vcMessageFrom <> '' 
 BEGIN 
	If @filter = 1 --ReceivedMessages
		set  @strCondition=' and (vcFrom like ''%'+ @vcMessageFrom+'%'')  ' 
	ELSE IF @filter=2 --SentMessages
		set  @strCondition=' and (vcTo like ''%'+ @vcMessageFrom+'%'')  ' 
	ELSE
		set  @strCondition=' and (vcFrom like ''%'+ @vcMessageFrom+'%'' or vcTo like ''%'+ @vcMessageFrom+'%'')  ' 
 END
  else if @numdivisionId <>0 
 BEGIN

 /* This approach does not work as numEmailId stores who sent email, id of sender
  set @strCondition=' and HDR.numEmailID in (
  SELECT numEmailID FROM dbo.EmailMaster WHERE numDomainID= ' + CONVERT(VARCHAR(15),@numDomainID) +' AND numContactID 
  IN (SELECT numcontactid FROM dbo.AdditionalContactsInformation WHERE numDivisionID = ' + convert(varchar(15),@numDivisionID) + ' AND numDomainID = ' + CONVERT(VARCHAR(15),@numDomainID) +' ))';
*/
-- bug fix id 1591,1628 
  DECLARE @TnumContactID NUMERIC(9);set @strCondition=''

  select TOP 1 @TnumContactID=numContactID,@vcContactEmail=replace(vcEmail,'''','''''') from AdditionalContactsInformation where numdivisionid=@numdivisionId and len(vcEmail)>2
  ORDER BY numContactID ASC
  
  WHILE @TnumContactID>0
  BEGIN
   if len(@strCondition)>0
    SET @strCondition=@strCondition + ' or '

	If @filter = 1 --ReceivedMessages
		set  @strCondition = @strCondition + ' (vcFrom like ''%'+ @vcContactEmail+'%'')  ' 
	ELSE IF @filter=2 --SentMessages
		set  @strCondition= @strCondition + ' (vcTo like ''%'+ @vcContactEmail+'%'')  ' 
	ELSE
		set  @strCondition= @strCondition + ' (vcFrom like ''%'+ @vcContactEmail+'%'' or vcTo like ''%'+ @vcContactEmail+'%'')  '

   select TOP 1 @TnumContactID=numContactID,@vcContactEmail=vcEmail from AdditionalContactsInformation where numdivisionid=@numdivisionId and len(vcEmail)>2 and numContactID>@TnumContactID
   ORDER BY numContactID ASC

   IF @@rowcount = 0 SET @TnumContactID = 0 
  END
 
  if len(@strCondition)>0
   SET @strCondition= 'and (' + @strCondition + ')'
  else
   SET @strCondition=' and (1=0)' 
  --set @strCondition=' and DTL.numEmailId  in (select numEmailId from EmailMaster where vcEmailID in (select vcEmail  from AdditionalContactsInformation where numdivisionid='''+ convert(varchar(15),@numdivisionId)+''' ))    '          
  END
  else if @numContactId>0 
 BEGIN
  select @vcContactEmail=isnull(replace(vcEmail,'''',''''''),'') from AdditionalContactsInformation where numContactID=@numContactId and len(vcEmail)>2

  if len(@vcContactEmail)>0
	If @filter = 1 --ReceivedMessages
		set  @strCondition=' and (vcFrom like ''%'+ @vcContactEmail +'%'')  ' 
	ELSE IF @filter=2 --SentMessages
		set  @strCondition=' and (vcTo like ''%'+ @vcContactEmail+'%'')  ' 
	ELSE
		set  @strCondition=' and (vcFrom like ''%'+ @vcContactEmail+'%'' or vcTo like ''%'+ @vcContactEmail+'%'')' 
  else
   SET @strCondition=' and (1=0)'
 END
 else 
  SET @strCondition=''
    
  set @strSql= '                        
  SELECT distinct(HDR.numEmailHstrID),convert(varchar(15),HDR.numEmailHstrID)+''~1~0''+convert(varchar(15),HDR.numUserCntId) as DelData,HDR.bintCreatedOn,
  dbo.FormatedDateTimeFromDate(DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+', bintCreatedOn),'+convert(varchar(15),@numDomainID)+') as [date],                
  vcSubject as [Subject],                          
  ''Email'' as [type] ,                          
  '''' as phone,'''' as assignedto ,                          
  ''0'' as caseid,                             
  null as vcCasenumber,                                 
  ''0''as caseTimeid,                                
  ''0''as caseExpid ,                          
  hdr.tinttype ,HDR.bintCreatedOn as dtCreatedDate,vcFrom + '','' + vcTo as [From] ,0 as bitClosedflag,ISNULL(HDR.bitHasAttachments,0) bitHasAttachments,
 CASE WHEN LEN(Cast(vcBodyText AS VARCHAR(1000)))>150 THEN SUBSTRING(vcBodyText,0,150) + ''...'' ELSE
   Cast(vcBodyText AS VARCHAR(1000)) END AS vcBody                     
  from EmailHistory HDR                                 
  LEFT JOIN [Correspondence] CR ON CR.numEmailHistoryID = HDR.numEmailHstrID '                    
   set  @strSql=@strSql +' where ISNULL(HDR.bitInvisible,0) = 0 AND HDR.numDomainID='+convert(varchar(15),@numDomainID)+' and  (bintCreatedOn between '''+ convert(varchar(30),@FromDate)+''' and '''+ convert(varchar(30),@ToDate)+''') '
   + ' AND (CR.numOpenRecordID = '+ convert(varchar(15),@numOpenRecordID) +' OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
  
 set  @strSql=@strSql+ @strCondition
--  if @numdivisionId =0 and  @vcMessageFrom <> '' set  @strSql=@strSql+ 'and (vcFrom like ''%'+ @vcMessageFrom+'%'' or vcTo like ''%'+ @vcMessageFrom+'%'')  '                      
--  else if @numdivisionId <>0 
-- BEGIN
--  set @strSql=@strSql+ 'and DTL.numEmailId  in (select numEmailId from EmailMaster where vcEmailID in (select vcEmail  from AdditionalContactsInformation where numdivisionid='''+ convert(varchar(15),@numdivisionId)+''' ))    '          
--  END
--  else if @numContactId>0 
-- BEGIN
--  declare @vcContactEmail as varchar(100);
--  select @vcContactEmail=vcEmail from AdditionalContactsInformation where numContactID=@numContactId
--
--  set  @strSql=@strSql+ 'and (vcFrom like ''%'+ @vcContactEmail+'%'' or vcTo like ''%'+ @vcContactEmail+'%'')    ' 
-- END

  IF @tintMode=1 SET @strSql=@strSql+ ' and  (CR.tintCorrType IN(1,3) OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
  IF @tintMode=2 SET @strSql=@strSql+ ' and  (CR.tintCorrType IN(2,4) OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
  IF @tintMode=5 SET @strSql=@strSql+ ' and  (CR.tintCorrType IN(5) OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
  IF @tintMode=6 SET @strSql=@strSql+ ' and  (CR.tintCorrType IN(6) OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
  
  if @filter =0 set  @strSql=@strSql+ ' and (hdr.tinttype=1 or hdr.tinttype=2 or  hdr.tinttype=5)'                  
  else if @filter =1 OR @filter =2 set  @strSql=@strSql+ ' and (hdr.tinttype=1 OR hdr.tinttype=2)'                                             
  if @filter > 2 set  @strSql=@strSql+ ' and hdr.tinttype=0'                 
                        
  if @tintSortOrder =0 and @SeachKeyword <> ''                 
  begin                
   set  @strSql=@strSql+ ' and (vcFrom like ''%'+ @SeachKeyword+'%'' or vcTo like ''%'+ @SeachKeyword+'%'' or vcsubject like ''%'+@SeachKeyword+'%'' or vcBody like ''%'+@SeachKeyword+'%'')'               
  end                
  if @tintSortOrder =1 and @SeachKeyword <> ''                 
  begin                
     set  @strSql=@strSql+ ' and (vcFrom like ''%'+ @SeachKeyword+'%'' or vcsubject like ''%'+@SeachKeyword+'%'' or  vcBody like ''%'+@SeachKeyword+'%'')'                 
  end                
  if @tintSortOrder =2 and @SeachKeyword <> ''                 
  begin                
     set  @strSql=@strSql+ ' and (vcTo like ''%'+ @SeachKeyword+'%'' or vcsubject like ''%'+@SeachKeyword+'%'' or  vcBody like ''%'+@SeachKeyword+'%'')'                 
  end                            
 end                
 if (@filter =0 and @tintSortOrder=0 and @bitOpenCommu=0) set  @strSql=@strSql+ ' union '                
                
 if ((@filter=0 or @filter>2) and (@tintSortOrder=0 or @tintSortOrder>2))                
 begin                
  set  @strSql=@strSql+ '                     
  SELECT c.numCommId as numEmailHstrID,convert(varchar(15),C.numCommId)+''~2~''+convert(varchar(15),C.numCreatedBy) as DelData,CASE WHEN c.bitTask = 973 THEN dtCreatedDate WHEN c.bitTask = 974 THEN dtCreatedDate WHEN c.bitTask = 971 THEN dtStartTime ELSE dtEndTime END AS bintCreatedOn,
  case when c.bitTask=973 then dbo.FormatedDateTimeFromDate(DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+', dtCreatedDate),'+convert(varchar(15),@numDomainID)+') 
  when c.bitTask=974 then dbo.FormatedDateTimeFromDate(DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+', dtEndTime),'+convert(varchar(15),@numDomainID)+') 
  when c.bitTask=971 THEN dbo.FormatedDateFromDate(DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+', dtEndTime),'+convert(varchar(15),@numDomainID)+') + '' '' + RIGHT(CONVERT(VARCHAR,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+', [dtStartTime]),100),7)  +'' - '' + RIGHT(CONVERT(VARCHAR,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+', [dtEndTime]),100),7)
  else  dbo.FormatedDateFromDate(DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+', dtEndTime),'+convert(varchar(15),@numDomainID)+') + '' '' + RIGHT(CONVERT(VARCHAR,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+', [dtStartTime]),100),7)  +'' - '' + RIGHT(CONVERT(VARCHAR,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+', [dtEndTime]),100),7)  END AS [date],
  convert(varchar(8000),textdetails) as [Subject],                                                                         
 dbo.fn_GetListItemName(c.bitTask)  AS [Type],                                                                 
  A1.vcFirstName +'' ''+ A1.vcLastName +'' ''+                                      
  case when A1.numPhone<>'''' then + A1.numPhone +case when A1.numPhoneExtension<>'''' then '' - '' + A1.numPhoneExtension else '''' end  else '''' end as [Phone],                        
  A2.vcFirstName+ '' ''+A2.vcLastName  as assignedto,                         
  c.caseid,                           
  (select top 1 vcCaseNumber from cases where cases.numcaseid= c.caseid )as vcCasenumber,                               
  isnull(caseTimeid,0)as caseTimeid,                              
  isnull(caseExpid,0)as caseExpid  ,                        
  1 as tinttype ,dtCreatedDate,'''' as [FROM]     ,isnull(C.bitClosedflag,0) as bitClosedflag,0 as bitHasAttachments,
'''' as vcBody
  from  Communication C                  
  JOIN AdditionalContactsInformation  A1                                           
  ON c.numContactId = A1.numContactId
  LEFT JOIN CommunicationLinkedOrganization clo ON C.numCommID=clo.numCommID                                                                             
  left join AdditionalContactsInformation A2 on A2.numContactID=c.numAssign
  left join UserMaster UM on A2.numContactid=UM.numUserDetailId and isnull(UM.bitActivateFlag,0)=1                                                                           
  left join listdetails on numActivity=numListID                           
  LEFT JOIN [Correspondence] CR ON CR.numCommID = C.numCommID
  where  C.numDomainID=' +convert(varchar(15),@numDomainID)                       
  + 'AND (numOpenRecordID = '+ convert(varchar(15),@numOpenRecordID) +' OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
  if (@numdivisionId =0 and @numContactId<>0) set  @strSql=@strSql+' and C.numContactId='+ convert(varchar(15),@numContactId)                        
  if @numdivisionId <> 0 set  @strSql=@strSql+' and  (C.numDivisionId='+ convert(varchar(15),@numdivisionId) + ' OR clo.numDivisionID='+ convert(varchar(15),@numdivisionId) + ') '             
  if @numCaseID>0  set  @strSql=@strSql+' and  C.CaseId='+ convert(varchar(15),@numCaseID)                       
  if @filter =0  
  BEGIN
	IF @tintMode <> 9
	BEGIN
		PRINT @tintMode
		SET @strSql=@strSql+ ' and dtStartTime  >= '''+ convert(varchar(30),@FromDate)+''' and dtStartTime  <= '''+ convert(varchar(30),@ToDate)  +''''
	END
  END
  else
    SET @strSql=@strSql+ ' and c.bitTask=' + CONVERT(varchar(12),@filter) + ' and ((dtStartTime  >= '''+ convert(varchar(30),@FromDate)+''' and dtStartTime  <= '''+ convert(varchar(30),@ToDate)  +''') OR (dtEndTime  >= '''+ convert(varchar(30),@FromDate)+''' and dtEndTime  <= '''+ convert(varchar(30),@ToDate)  +'''))'
  
  IF @tintMode=1 SET @strSql=@strSql+ ' and  (CR.tintCorrType IN(1,3) OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
  IF @tintMode=2 SET @strSql=@strSql+ ' and  (CR.tintCorrType IN(2,4) OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
  IF @tintMode=5 SET @strSql=@strSql+ ' and  (CR.tintCorrType IN(5) OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
  IF @tintMode=6 SET @strSql=@strSql+ ' and  (CR.tintCorrType IN(6) OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
                          
  IF @tintSortOrder =0 and @SeachKeyword <>'' 
	SET @strSql=@strSql+ ' and textdetails like ''%'+@SeachKeyword+'%'''
  ELSE
	IF ISNULL(@filter,0) = 0 
	BEGIN
		SET @strSql=@strSql+ ' and textdetails like ''%'+@SeachKeyword+'%'''
	END
	ELSE
	BEGIN
		SET @strSql=@strSql+ ' and c.bitTask=' + CONVERT(varchar(12),@filter) + ' and textdetails like ''%'+@SeachKeyword+'%'''                     
	END
  IF @bitOpenCommu=1  SET @strSql=@strSql+ ' and isnull(C.bitClosedflag,0)=0'
 end                
 set @strSql='With tblCorr as (SELECT ROW_NUMBER() OVER (ORDER BY '+@columnName+' '+ @columnSortOrder+') AS RowNumber,X.* from('+@strSql+')X)'                

 set @strSql=@strSql +'                       
  select  *  from tblCorr
  Where RowNumber > '+convert(varchar(10),@firstRec)+' and RowNumber < '+convert(varchar(10),@lastRec)+'                     
  union select 0,count(*),null,null,null,'','',null,null,null,null,null,null,null,null,null,null,0,0,null from tblCorr order by RowNumber'                 
                
-- set @strSql=@strSql +'                       
--  select  *,CASE WHEN [Type]=''Email'' THEN dbo.GetEmaillAdd(numEmailHstrID,4)+'', '' +dbo.GetEmaillAdd(numEmailHstrID,1) ELSE '''' END as [From] from tblCorr
--  Where RowNumber > '+convert(varchar(10),@firstRec)+' and RowNumber < '+convert(varchar(10),@lastRec)+'                     
--  union select 0,count(*),null,null,null,'','',null,null,null,null,null,null,null,null,null,null from tblCorr order by RowNumber'                 
                
 print @strSql                
 exec (@strSql)                
end                
else                
select 0,0
/****** Object:  StoredProcedure [dbo].[USP_GetDealsList1]    Script Date: 05/07/2009 22:12:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj

--exec USP_GetDealsList1 
--@numUserCntID=85098,
--@numDomainID=110,
--@tintSalesUserRightType=3,
--@tintPurchaseUserRightType=3,
--@tintSortOrder=1,
--@SortChar=0,
--@FirstName='',
--@LastName='',
--@CustName='',
--@CurrentPage=1,
--@PageSize=50,
--@TotRecs=0,
--@columnName='vcCompanyName',
--@columnSortOrder='Asc',
--@numCompanyID=0,
--@bitPartner=0,
--@ClientTimeZoneOffset=-180,
--@tintShipped=0,
--@intOrder=0,
--@intType=1,
--@tintFilterBy=0


GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getdealslist1')
DROP PROCEDURE usp_getdealslist1
GO
CREATE PROCEDURE [dbo].[USP_GetDealsList1]
               @numUserCntID              NUMERIC(9)  = 0,
               @numDomainID               NUMERIC(9)  = 0,
               @tintSalesUserRightType    TINYINT  = 0,
               @tintPurchaseUserRightType TINYINT  = 0,
               @tintSortOrder             TINYINT  = 4,
               @SortChar                  CHAR(1)  = '0',
               @FirstName                 VARCHAR(100)  = '',
               @LastName                  VARCHAR(100)  = '',
               @CustName                  VARCHAR(100)  = '',
               @CurrentPage               INT,
               @PageSize                  INT,
               @TotRecs                   INT  OUTPUT,
               @columnName                AS VARCHAR(50),
               @columnSortOrder           AS VARCHAR(10),
               @numCompanyID              AS NUMERIC(9)  = 0,
               @bitPartner                AS BIT  = 0,
               @ClientTimeZoneOffset      AS INT,
               @tintShipped               AS TINYINT,
               @intOrder                  AS TINYINT,
               @intType                   AS TINYINT,
               @tintFilterBy              AS TINYINT  = 0,
               @numOrderStatus			  AS NUMERIC,
			   @vcRegularSearchCriteria varchar(1000)='',
			   @vcCustomSearchCriteria varchar(1000)='' 
AS
  DECLARE  @PageId  AS TINYINT
  DECLARE  @numFormId  AS INT 
  
  SET @PageId = 0
  IF @inttype = 1
  BEGIN
  	SET @PageId = 2
  	SET @inttype = 3
  	SET @numFormId=39
  END
  IF @inttype = 2
  BEGIN
  	SET @PageId = 6
    SET @inttype = 4
    SET @numFormId=41
  END
  --Create a Temporary table to hold data
  CREATE TABLE #tempTable (
    ID              INT   IDENTITY   PRIMARY KEY,
    numOppId        NUMERIC(9),intTotalProgress INT,
	vcInventoryStatus	VARCHAR(100))
    
 DECLARE @strShareRedordWith AS VARCHAR(300);
 SET @strShareRedordWith=' Opp.numOppId in (select SR.numRecordID FROM ShareRecord SR WHERE SR.numDomainID='+ convert(varchar(15),@numDomainID )+' AND SR.numModuleID=3 AND SR.numAssignedTo='+convert(varchar(15),@numUserCntId)+') '

  DECLARE  @strSql  AS VARCHAR(8000)
  SET @strSql = 'SELECT  Opp.numOppId,PP.intTotalProgress,RTRIM(LTRIM(REPLACE(SUBSTRING(dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID),CHARINDEX(''>'',dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID)) + 1, CHARINDEX(''<font>'',dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID))),''<font>'',''''))) AS [vcInventoryStatus]'
  
  DECLARE  @strOrderColumn  AS VARCHAR(100);SET @strOrderColumn=@columnName
--  
--   IF LEN(@columnName)>0
--   BEGIN
--	 IF CHARINDEX(@columnName,@strSql)=0
--	 BEGIN
--		SET @strSql= @strSql + ',' + @columnName
--	 	SET @strOrderColumn=@columnName
--	 END
--   END
   
  SET @strSql = @strSql + ' FROM OpportunityMaster Opp                                                               
                                 INNER JOIN AdditionalContactsInformation ADC ON Opp.numContactId = ADC.numContactId                                                               
                                 INNER JOIN DivisionMaster Div ON Opp.numDivisionId = Div.numDivisionID AND ADC.numDivisionId = Div.numDivisionID
								 INNER JOIN CompanyInfo cmp ON Div.numCompanyID = cmp.numCompanyId 
								 LEFT JOIN ProjectProgress PP On PP.numOppId = Opp.numOppId '
                                 --left JOIN OpportunityLinking OL on OL.numChildOppID=Opp.numOppId '
								 
  IF @bitPartner = 1
    SET @strSql = @strSql + ' left join OpportunityContact OppCont on OppCont.numOppId=Opp.numOppId and OppCont.bitPartner=1 and OppCont.numContactId=' + CONVERT(VARCHAR(15),@numUserCntID)
                    
   
   if @columnName like 'CFW.Cust%'             
	begin            
		SET @strSql = @strSql + ' left Join CFW_Fld_Values_Opp CFW on CFW.RecId=Opp.numOppid  and CFW.fld_id= '+replace(@columnName,'CFW.Cust','') 
		SET @strOrderColumn = 'CFW.Fld_Value'   
	end                                     
  ELSE if @columnName like 'DCust%'            
	begin            
	 SET @strSql = @strSql + ' left Join CFW_Fld_Values_Opp CFW on CFW.RecId=Opp.numOppid   and CFW.fld_id= '+replace(@columnName,'DCust','')                                                                                               
	 SET @strSql = @strSql + ' left Join ListDetails LstCF on LstCF.numListItemID=CFW.Fld_Value  '            
	 SET @strOrderColumn = 'LstCF.vcData'  
	end       

             
  IF @tintFilterBy = 1 --Partially Fulfilled Orders 
    SET @strSql = @strSql + ' left join AdditionalContactsInformation ADC1 on ADC1.numContactId=Opp.numRecOwner                                                               
                             LEFT OUTER JOIN [OpportunityBizDocs] OB ON OB.[numOppId] = Opp.[numOppId] 
                             WHERE Opp.numDomainID=' + CONVERT(VARCHAR(15),@numDomainID) + ' and Opp.tintOppstatus=1 and Opp.tintShipped=' + CONVERT(VARCHAR(15),@tintShipped)
  ELSE
    SET @strSql = @strSql + ' left join AdditionalContactsInformation ADC1 on ADC1.numContactId=Opp.numRecOwner                                                               
                             WHERE Opp.numDomainID=' + CONVERT(VARCHAR(15),@numDomainID) + ' and Opp.tintOppstatus=1 and Opp.tintShipped='+ CONVERT(VARCHAR(15),@tintShipped) 
            
                    
  IF @FirstName <> ''
    SET @strSql = @strSql + ' and ADC.vcFirstName  like ''' + @FirstName + '%'''
    
  IF @LastName <> '' 
    SET @strSql = @strSql + ' and ADC.vcLastName like ''' + @LastName + '%'''
  
  IF @CustName <> ''
    SET @strSql = @strSql + ' and cmp.vcCompanyName like ''' + @CustName + '%'''
  
  
  IF @intOrder <> 0
    BEGIN
      IF @intOrder = 1
        SET @strSql = @strSql + ' and Opp.bitOrder = 0 '
      IF @intOrder = 2
        SET @strSql = @strSql + ' and Opp.bitOrder = 1'
    END
    
  IF @numCompanyID <> 0
    SET @strSql = @strSql + ' And Div.numCompanyID ='+ CONVERT(VARCHAR(15),@numCompanyID)
    
  IF @SortChar <> '0'
    SET @strSql = @strSql + ' And Opp.vcPOppName like ''' + @SortChar + '%'''
  
  
  IF @tintSalesUserRightType = 0 OR @tintPurchaseUserRightType = 0
    SET @strSql = @strSql + ' AND opp.tintOppType =0'
  ELSE IF @tintSalesUserRightType = 1 OR @tintPurchaseUserRightType = 1
    SET @strSql = @strSql + ' AND (Opp.numRecOwner= ' + CONVERT(VARCHAR(15),@numUserCntID)
                    + ' or Opp.numAssignedTo= '+ CONVERT(VARCHAR(15),@numUserCntID)
                    --+ ' or  Opp.numOppId in (select distinct(numOppId) from OpportunityStageDetails where  numAssignTo ='+ CONVERT(VARCHAR(15),@numUserCntID)+ ')'
                    + CASE WHEN @bitPartner = 1 THEN ' or ( OppCont.bitPartner=1 and OppCont.numContactId='+ CONVERT(VARCHAR(15),@numUserCntID) + ')' ELSE '' END
                    + ' or ' + @strShareRedordWith +')'
  ELSE IF @tintSalesUserRightType = 2 OR @tintPurchaseUserRightType = 2
      SET @strSql = @strSql + ' AND (Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+ CONVERT(VARCHAR(15),@numUserCntID) + ' ) 
								or div.numTerID=0 or Opp.numAssignedTo= '+ CONVERT(VARCHAR(15),@numUserCntID)
								 --+ ' or  Opp.numOppId in (select distinct(numOppId) from OpportunityStageDetails where numAssignTo ='+ CONVERT(VARCHAR(15),@numUserCntID)+ ')' 
								+ ' or ' + @strShareRedordWith +')'
	
								
  IF @tintSortOrder <> '0'
    SET @strSql = @strSql + '  AND Opp.tintOppType= '+ CONVERT(VARCHAR(1),@tintSortOrder)
    
    
 IF @tintFilterBy = 1 --Partially Fulfilled Orders
    SET @strSql = @strSql + ' AND OB.bitPartialFulfilment = 1 AND OB.bitAuthoritativeBizDocs = 1 '
 ELSE IF @tintFilterBy = 2 --Fulfilled Orders
    SET @strSql = @strSql + ' AND Opp.[numOppId] IN (SELECT OM.numOppId FROM [OpportunityMaster] OM
                      			INNER JOIN [OpportunityBizDocs] OB ON OB.[numOppId] = OM.[numOppId]
                      			INNER JOIN [OpportunityItems] OI ON OM.[numOppId] = OI.[numOppId]
                      			INNER JOIN [OpportunityBizDocItems] BI ON BI.[numOppBizDocID] = OB.[numOppBizDocsId]
                      			Where OM.numDomainId=' + CONVERT(VARCHAR(15),@numDomainId) + 'AND OB.[bitAuthoritativeBizDocs] = 1 GROUP BY OM.[numOppId]
                      			HAVING   COUNT(OI.[numUnitHour]) = COUNT(BI.[numUnitHour])) '
 ELSE IF @tintFilterBy = 3 --Orders without Authoritative-BizDocs
    SET @strSql = @strSql + ' AND Opp.[numOppId] not IN (SELECT DISTINCT OM.[numOppId]
                      		  FROM [OpportunityMaster] OM JOIN [OpportunityBizDocs] OB ON OM.[numOppId] = OB.[numOppId]
                      		  Where OM.numDomainId=' + CONVERT(VARCHAR(15),@numDomainId) + ' AND OM.tintOppstatus=1 AND OM.tintShipped=' + CONVERT(VARCHAR(15),@tintShipped) + ' AND OM.tintOppType= '+ CONVERT(VARCHAR(1),@tintSortOrder) + ' AND isnull(OB.[bitAuthoritativeBizDocs],0)=1) '
--ELSE IF @tintFilterBy = 4 --Orders with child records
--    SET @strSql = @strSql
--                    + ' AND Opp.[numOppId] IN (SELECT numParentOppID FROM dbo.OpportunityLinking OL INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OL.numParentOppID WHERE OM.numDomainId='+ CONVERT(VARCHAR(15),@numDomainID) + ' )  '

 ELSE IF @tintFilterBy = 4 --Orders with items yet to be added to Invoices
    SET @strSql = @strSql + ' AND Opp.[numOppId] IN (SELECT distinct OPPM.numOppId FROM [OpportunityMaster] OPPM
                                  INNER JOIN [OpportunityItems] OPPI ON OPPM.[numOppId] = OPPI.[numOppId]
                                  Where OPPM.numDomainId=' + CONVERT(VARCHAR(15),@numDomainId) + ' 
								  and OPPI.numoppitemtCode not in (select numOppItemID from [OpportunityBizDocs] OB INNER JOIN [OpportunityBizDocItems] BI
                      			  ON BI.[numOppBizDocID] = OB.[numOppBizDocsId] where OB.[numOppId] = OPPM.[numOppId]))'      
 ELSE IF @tintFilterBy = 6 --Orders with deferred Income/Invoices
    SET @strSql = @strSql + ' AND Opp.[numOppId] IN (SELECT DISTINCT OM.[numOppId]
                      		 FROM [OpportunityMaster] OM LEFT OUTER JOIN [OpportunityBizDocs] OB ON OM.[numOppId] = OB.[numOppId]
                      		 Where OM.numDomainId=' + CONVERT(VARCHAR(15),@numDomainId) + ' AND OB.[bitAuthoritativeBizDocs] =1 and OB.tintDeferred=1)'

IF @numOrderStatus <>0 
	SET @strSql = @strSql + ' AND Opp.numStatus = ' + CONVERT(VARCHAR(15),@numOrderStatus);

IF CHARINDEX('vcInventoryStatus',@vcRegularSearchCriteria) > 0
BEGIN
	IF CHARINDEX('vcInventoryStatus=1',@vcRegularSearchCriteria) > 0
		SET @strSql = @strSql + ' and ' + ' CHARINDEX(''Not Applicable'', dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID)) > 0 '
	IF CHARINDEX('vcInventoryStatus=2',@vcRegularSearchCriteria) > 0
		SET @strSql = @strSql + ' and ' + ' CHARINDEX(''Allocation Cleared'', dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID)) > 0 '
	IF CHARINDEX('vcInventoryStatus=3',@vcRegularSearchCriteria) > 0
		SET @strSql = @strSql + ' and ' + ' CHARINDEX(''Back Order'', dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID)) > 0 '
	IF CHARINDEX('vcInventoryStatus=4',@vcRegularSearchCriteria) > 0
		SET @strSql = @strSql + ' and ' + ' CHARINDEX(''Ready to Ship'', dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID)) > 0 '
END
ELSE IF @vcRegularSearchCriteria<>'' 
BEGIN
	SET @strSql=@strSql+' and ' + @vcRegularSearchCriteria 
END
	
IF @vcCustomSearchCriteria<>'' 
	set @strSql=@strSql+' and Opp.numOppid in (select distinct CFW.RecId from CFW_Fld_Values_Opp CFW where ' + @vcCustomSearchCriteria + ')'

 --SET @strSql = 'SELECT numOppId from (' + @strSql + ') a'

 IF LEN(@strOrderColumn)>0
	IF @strOrderColumn = 'OI.vcInventoryStatus'
		BEGIN
			SET @strSql =  @strSql + ' ORDER BY  RTRIM(LTRIM(REPLACE(SUBSTRING(dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID),CHARINDEX(''>'',dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID)) + 1, CHARINDEX(''<font>'',dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID))),''<font>'','''')))'  + ' ' + @columnSortOrder    
		END
	ELSE
		BEGIN
	SET @strSql =  @strSql + ' ORDER BY  ' + @strOrderColumn + ' ' + @columnSortOrder    
		END

	
  PRINT @strSql
  INSERT INTO #tempTable (numOppId,intTotalProgress,vcInventoryStatus) EXEC( @strSql)
  
  DECLARE  @firstRec  AS INTEGER
  DECLARE  @lastRec  AS INTEGER
  SET @firstRec = (@CurrentPage - 1) * @PageSize
  SET @lastRec = (@CurrentPage * @PageSize + 1)
  
  SET @TotRecs = (SELECT COUNT(* ) FROM #tempTable)
  
  DECLARE  @tintOrder  AS TINYINT;SET @tintOrder = 0
  DECLARE  @vcFieldName  AS VARCHAR(50)
  DECLARE  @vcListItemType  AS VARCHAR(3)
  DECLARE  @vcListItemType1  AS VARCHAR(1)
  DECLARE  @vcAssociatedControlType VARCHAR(30)
  DECLARE  @numListID  AS NUMERIC(9)
  DECLARE  @vcDbColumnName VARCHAR(40)
  DECLARE  @WhereCondition VARCHAR(2000);SET @WhereCondition = ''
  DECLARE  @vcLookBackTableName VARCHAR(2000)
  DECLARE  @bitCustom  AS BIT
  Declare @numFieldId as numeric  
  DECLARE @bitAllowSorting AS CHAR(1)   
  DECLARE @bitAllowEdit AS CHAR(1)                 
  DECLARE @vcColumnName AS VARCHAR(500)                  
  DECLARE  @Nocolumns  AS TINYINT;SET @Nocolumns = 0

  SELECT @Nocolumns=isnull(sum(TotalRow),0) from(            
	SELECT count(*) TotalRow from View_DynamicColumns where numFormId=@numFormId and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType = @numFormId
	UNION 
	SELECT count(*) TotalRow from View_DynamicCustomColumns where numFormId=@numFormId and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType = @numFormId) TotalRows


  CREATE TABLE #tempForm (tintOrder TINYINT,vcDbColumnName nvarchar(50),vcOrigDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),
  vcListItemType varchar(3),numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,
  numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,bitIsAlphaNumeric BIT,bitIsNumeric BIT,
  bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),ListRelID numeric(9),intColumnWidth int,bitAllowFiltering BIT,vcFieldDataType CHAR(1))
    
  SET @strSql = ''
  SET @strSql = 'select ADC.numContactId,DM.numDivisionID,isnull(DM.numTerID,0) numTerID,opp.numRecOwner as numRecOwner,DM.tintCRMType,opp.numOppId'

--Check if Master Form Configuration is created by administrator if NoColumns=0
DECLARE @IsMasterConfAvailable BIT = 0
DECLARE @numUserGroup NUMERIC(18,0)

IF @Nocolumns=0
BEGIN
	SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

	IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = @numFormId AND numRelCntType=@numFormId AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
	BEGIN
		SET @IsMasterConfAvailable = 1
	END
END


--If MasterConfiguration is available then load it otherwise load default columns
IF @IsMasterConfAvailable = 1
BEGIN
	INSERT INTO #tempForm
	SELECT 
		(intRowNum + 1) as tintOrder, vcDbColumnName,	vcOrigDbColumnName,	ISNULL(vcCultureFieldName,vcFieldName), vcAssociatedControlType,
		vcListItemType, numListID, vcLookBackTableName,	bitCustom, numFieldId, bitAllowSorting,	bitInlineEdit, bitIsRequired,
		bitIsEmail,	bitIsAlphaNumeric, bitIsNumeric, bitIsLengthValidation,	intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,
		ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType 
	FROM 
		View_DynamicColumnsMasterConfig
	WHERE
		View_DynamicColumnsMasterConfig.numFormId=@numFormId AND 
		View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
		View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
		View_DynamicColumnsMasterConfig.numRelCntType=@numFormId AND 
		View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
		ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
		ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
	UNION
    SELECT 
		tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName, vcAssociatedControlType,'' as vcListItemType,numListID,'',
		bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
		intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,0,''
	FROM 
		View_DynamicCustomColumnsMasterConfig
	WHERE 
		View_DynamicCustomColumnsMasterConfig.numFormId=@numFormId AND 
		View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
		View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
		View_DynamicCustomColumnsMasterConfig.numRelCntType=@numFormId AND 
		View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
		ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1
	ORDER BY 
		tintOrder ASC  
END
ELSE                                        
BEGIN
if @Nocolumns=0
BEGIN
INSERT INTO DycFormConfigurationDetails (numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth)
select @numFormId,numFieldId,0,Row_number() over(order by tintRow desc),@numDomainID,@numUserCntID,@numFormId,1,0,intColumnWidth
 FROM View_DynamicDefaultColumns
 where numFormId=@numFormId and bitDefault=1 AND ISNULL(bitSettingField,0)=1 and numDomainID=@numDomainID
order by tintOrder asc   

END

    
    INSERT INTO #tempForm
select tintRow+1 as tintOrder,vcDbColumnName,vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
 vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName                                                 
,bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType  
 FROM View_DynamicColumns 
 where numFormId=@numFormId and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 
  AND ISNULL(bitSettingField,0)=1 AND ISNULL(bitCustom,0)=0  AND numRelCntType = @numFormId
 
 UNION
    
     select tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName,                  
 vcAssociatedControlType,'' as vcListItemType,numListID,''                                                 
,bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,0,'' 
 from View_DynamicCustomColumns
 where numFormId=@numFormId and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 
 AND ISNULL(bitCustom,0)=1  AND numRelCntType = @numFormId
 
  order by tintOrder asc  
END


--  SET @DefaultNocolumns = @Nocolumns
Declare @ListRelID as numeric(9) 

  select top 1 @tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,                
  @vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,@numListID=numListID,@vcLookBackTableName=vcLookBackTableName                                               
,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,
@ListRelID=ListRelID
  from  #tempForm order by tintOrder asc            

  WHILE @tintOrder > 0
    BEGIN
      IF @bitCustom = 0
        BEGIN
          DECLARE  @Prefix  AS VARCHAR(5)
          IF @vcLookBackTableName = 'AdditionalContactsInformation'
            SET @Prefix = 'ADC.'
          IF @vcLookBackTableName = 'DivisionMaster'
            SET @Prefix = 'DM.'
          IF @vcLookBackTableName = 'OpportunityMaster'
            SET @PreFix = 'Opp.'
          IF @vcLookBackTableName = 'OpportunityRecurring'
            SET @PreFix = 'OPR.'
            
		 SET @vcColumnName= @vcDbColumnName+'~'+ convert(varchar(10),@numFieldId) +'~0'
            
          IF @vcAssociatedControlType = 'SelectBox'
            BEGIN
            IF @vcDbColumnName = 'tintSource'
            BEGIN
              SET @strSql = @strSql + ',ISNULL(dbo.fn_GetOpportunitySourceValue(ISNULL(Opp.tintSource,0),ISNULL(Opp.tintSourceType,0),Opp.numDomainID),''Internal Order'')' + ' [' + @vcColumnName + ']'
            END
            
			IF @vcDbColumnName = 'vcInventoryStatus'
            BEGIN
			SET @strSql = @strSql + ', ISNULL(dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID),'''') ' + ' [' + @vcColumnName + ']'
			END

            ELSE IF @vcDbColumnName = 'numCampainID'
            BEGIN
              SET @strSql = @strSql + ', (SELECT vcCampaignName FROM CampaignMaster WHERE numCampaignID= Opp.numCampainID) ' + ' [' + @vcColumnName + ']'
            END
            
            ELSE IF @vcListItemType = 'LI'
            BEGIN
                  SET @strSql = @strSql + ',L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData' + ' [' + @vcColumnName + ']'
                  SET @WhereCondition = @WhereCondition + ' left Join ListDetails L' + CONVERT(VARCHAR(3),@tintOrder) + ' on L' + CONVERT(VARCHAR(3),@tintOrder) + '.numListItemID=' + @vcDbColumnName
            END
            ELSE IF @vcListItemType = 'S'
            BEGIN
                    SET @strSql = @strSql + ',S' + CONVERT(VARCHAR(3),@tintOrder) + '.vcState' + ' [' + @vcColumnName + ']'
                    SET @WhereCondition = @WhereCondition + ' left join State S' + CONVERT(VARCHAR(3),@tintOrder) + ' on S' + CONVERT(VARCHAR(3),@tintOrder) + '.numStateID=' + @vcDbColumnName
            END
            ELSE IF @vcListItemType = 'BP'
            BEGIN
                    SET @strSql = @strSql + ',SPLM.Slp_Name' + ' [' + @vcColumnName + ']'
                    SET @WhereCondition = @WhereCondition + ' LEFT JOIN Sales_process_List_Master SPLM on SPLM.Slp_Id = OPP.numBusinessProcessID '
                  END
            ELSE IF @vcListItemType = 'PP'
            BEGIN
                              SET @strSql = @strSql + ',ISNULL(T.intTotalProgress,0)' + ' [' + @vcColumnName + ']'

--                SET @strSql = @strSql 
--                                + ',ISNULL(PP.intTotalProgress,0)'
--                                + ' [' +
--                                @vcColumnName + ']'
--                SET @WhereCondition = @WhereCondition
--                                        + ' LEFT JOIN ProjectProgress PP ON PP.numDomainID='+ convert(varchar(15),@numDomainID )+' and OPP.numOppID = PP.numOppID '
            END
            ELSE IF @vcListItemType = 'T'
                    BEGIN
                      SET @strSql = @strSql + ',L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData' + ' [' + @vcColumnName + ']'
                      SET @WhereCondition = @WhereCondition + ' left Join ListDetails L' + CONVERT(VARCHAR(3),@tintOrder) + ' on L' + CONVERT(VARCHAR(3),@tintOrder) + '.numListItemID=' + @vcDbColumnName
                    END
             ELSE IF @vcListItemType = 'U'
                      BEGIN
                        SET @strSql = @strSql + ',dbo.fn_GetContactName(' + @Prefix + @vcDbColumnName + ') [' + @vcColumnName + ']'
                      END
              ELSE IF @vcListItemType = 'WI'
				BEGIN
					SET @strSql = @strSql + ', Case when isnull(OL.numSiteID,0)>0 then Sites.vcSiteName else WebAPI.vcProviderName end [' + @vcColumnName + ']'
					set @WhereCondition= @WhereCondition +' left JOIN OpportunityLinking OL on OL.numChildOppID=Opp.numOppId left join WebAPI on WebAPI.WebApiId=isnull(OL.numWebApiId,0)
													left join Sites on isnull(OL.numSiteID,0)=Sites.numSIteID and Sites.numDomainID=' + convert(varchar(15),@numDomainID)                                                       
				END 
            END
          ELSE
            IF @vcAssociatedControlType = 'DateField'
              BEGIN
              if @Prefix ='OPR.'
				 BEGIN
				  	SET @strSql = @strSql + ', (SELECT TOP 1 dbo.FormatedDateFromDate( DateAdd(minute,+'+convert(varchar(15),-@ClientTimeZoneOffset)+', dtRecurringDate)'+ ','+ CONVERT(VARCHAR(10),@numDomainId) +') FROM  dbo.OpportunityRecurring WHERE numOppID=OPP.numOppID AND ISNULL(numOppBizDocID,0)=0 and dtRecurringDate > dbo.GetUTCDateWithoutTime()) AS  ['+ @vcColumnName + ']'
				 END
				 else
				 BEGIN
					SET @strSql = @strSql
                                + ',case when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','
                                + @Prefix
                                + @vcDbColumnName
                                + ') )= convert(varchar(11),getdate()) then ''<b><font color=red>Today</font></b>'''
					SET @strSql = @strSql
									+ 'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','
									+ @Prefix
									+ @vcDbColumnName
									+ ') )= convert(varchar(11),dateadd(day,-1,getdate())) then ''<b><font color=purple>YesterDay</font></b>'''
					SET @strSql = @strSql
									+ 'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','
									+ @Prefix
									+ @vcDbColumnName
									+ ') )= convert(varchar(11),dateadd(day,1,getdate())) then''<b><font color=orange>Tommorow</font></b>'' '
					SET @strSql = @strSql
									+ 'else dbo.FormatedDateFromDate('
									+ @Prefix
									+ @vcDbColumnName
									+ ','
									+ CONVERT(VARCHAR(10),@numDomainId)
									+ ') end  ['
									+ @vcColumnName + ']' 	
				 END
                
                                
					
						
              END
            ELSE
              IF @vcAssociatedControlType = 'TextBox'
                BEGIN
                  SET @strSql = @strSql
                                  + ','
                                  + CASE 
                                      WHEN @vcDbColumnName = 'numAge' THEN 'year(getutcdate())-year(bintDOB)'
                                      WHEN @vcDbColumnName = 'monPAmount' THEN 'isnull((SELECT varCurrSymbol FROM [Currency] WHERE numCurrencyID = Opp.[numCurrencyID]),'''') + '' ''+ CONVERT(VARCHAR,CONVERT(MONEY,monPAmount),1)'
                                      WHEN @vcDbColumnName = 'numDivisionID' THEN 'DM.numDivisionID'
                                      WHEN @vcDbColumnName = 'tintOppStatus' THEN 'CASE tintOppStatus WHEN 0 then ''Open'' WHEN 1 THEN ''Closed-Won'' WHEN 2 THEN ''Closed-Lost'' END '
                                      WHEN @vcDbColumnName = 'tintOppType' THEN 'CASE tintOppType WHEN 1 then ''Sales'' WHEN 2 THEN ''Purchase'' Else ''NA'' END '
                                      WHEN @vcDbColumnName = 'vcPOppName' THEN 'Opp.vcPOppName'
									  WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'vcBizDocsList' THEN 'ISNULL((SELECT SUBSTRING((SELECT ''$^$'' + CAST(numOppBizDocsId AS VARCHAR(18)) +''#^#''+ vcBizDocID
	FROM OpportunityBizDocs WHERE numOppId=Opp.numOppId FOR XML PATH('''')),4,200000)),'''')'
                                      ELSE @vcDbColumnName
                                    END
                                  + ' ['
                                  + @vcColumnName + ']'
                END
              ELSE
                IF @vcAssociatedControlType = 'TextArea'
                  BEGIN
						set @strSql=@strSql+','+  @Prefix + @vcDbColumnName +' ['+ @vcColumnName+']'  
                  END
                   else if  @vcAssociatedControlType='Label'                                              
begin  
 set @strSql=@strSql+','+ case                    
	WHEN @vcDbColumnName = 'CalAmount' THEN 'Opp.monDealAmount'--'[dbo].[getdealamount](Opp.numOppId,Getutcdate(),0)'
	WHEN @vcDbColumnName = 'vcOrderedShipped' THEN 'dbo.GetOrderShipReceiveStatus(Opp.numOppId,1,Opp.tintshipped)'
	WHEN @vcDbColumnName = 'vcOrderedReceived' THEN 'dbo.GetOrderShipReceiveStatus(Opp.numOppId,2,Opp.tintshipped)'
    WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monDealAmount' THEN 'ISNULL((SELECT SUM(ISNULL(monDealAmount,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
    WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monAmountPaid' THEN 'ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
    else @vcDbColumnName END +' ['+ @vcColumnName+']'            
 END

                   	else if @vcAssociatedControlType='Popup' and @vcDbColumnName='numShareWith'                                                   
		begin      
			 set @strSql=@strSql+',(SELECT SUBSTRING(
	(SELECT '','' + A.vcFirstName+'' ''+A.vcLastName + CASE WHEN ISNULL(SR.numContactType,0)>0 THEN ''('' + dbo.fn_GetListItemName(SR.numContactType) + '')'' ELSE '''' END
	FROM ShareRecord SR JOIN UserMaster UM ON UM.numUserDetailId=SR.numAssignedTo 
							 JOIN AdditionalContactsInformation A ON UM.numUserDetailId=A.numContactID 
		 WHERE SR.numDomainID=Opp.numDomainID AND SR.numModuleID=3 AND SR.numRecordID=Opp.numOppId
				AND UM.numDomainID=Opp.numDomainID and UM.numDomainID=A.numDomainID FOR XML PATH('''')),2,200000)) ['+ @vcColumnName+']'                
		 end 
        END
      ELSE
        BEGIN
        
            SET @vcColumnName= @vcDbColumnName +'~'+ convert(varchar(10),@numFieldId)+'~1'

          IF @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'
            BEGIN
              SET @strSql = @strSql
                              + ',CFW'
                              + CONVERT(VARCHAR(3),@tintOrder)
                              + '.Fld_Value  ['
                              + @vcColumnName + ']'
              SET @WhereCondition = @WhereCondition
                                      + ' left Join CFW_FLD_Values_Opp CFW'
                                      + CONVERT(VARCHAR(3),@tintOrder)
                                      + '               
                                                                                                                            on CFW'
                                      + CONVERT(VARCHAR(3),@tintOrder)
                                      + '.Fld_Id='
                                      + CONVERT(VARCHAR(10),@numFieldId)
                                      + 'and CFW'
                                      + CONVERT(VARCHAR(3),@tintOrder)
                                      + '.RecId=Opp.numOppid   '
            END
          ELSE
            IF @vcAssociatedControlType = 'CheckBox' 
              BEGIN
                SET @strSql = @strSql
                                + ',case when isnull(CFW'
                                + CONVERT(VARCHAR(3),@tintOrder)
                                + '.Fld_Value,0)=0 then ''No'' when isnull(CFW'
                                + CONVERT(VARCHAR(3),@tintOrder)
                                + '.Fld_Value,0)=1 then ''Yes'' end   ['
                                + @vcColumnName + ']'
                SET @WhereCondition = @WhereCondition
                                        + ' left Join CFW_FLD_Values_Opp CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '           
                                                                                                                                  on CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.Fld_Id='
                                        + CONVERT(VARCHAR(10),@numFieldId)
                                        + 'and CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.RecId=Opp.numOppid   '
              END
            ELSE
              IF @vcAssociatedControlType = 'DateField'
                BEGIN
                  SET @strSql = @strSql
                                  + ',dbo.FormatedDateFromDate(CFW'
                                  + CONVERT(VARCHAR(3),@tintOrder)
                                  + '.Fld_Value,'
                                  + CONVERT(VARCHAR(10),@numDomainId)
                                  + ')  ['
                                  + @vcColumnName + ']'
                  SET @WhereCondition = @WhereCondition
                                          + ' left Join CFW_FLD_Values_Opp CFW'
                                          + CONVERT(VARCHAR(3),@tintOrder)
                                          + '               
                                                                                                                                        on CFW'
                                          + CONVERT(VARCHAR(3),@tintOrder)
                                          + '.Fld_Id='
                                          + CONVERT(VARCHAR(10),@numFieldId)
                                          + 'and CFW'
                                          + CONVERT(VARCHAR(3),@tintOrder)
                                          + '.RecId=Opp.numOppid    '
                END
              ELSE
                IF @vcAssociatedControlType = 'SelectBox'
                  BEGIN
                    SET @vcDbColumnName = 'DCust'
                                            + CONVERT(VARCHAR(10),@numFieldId)
                    SET @strSql = @strSql
                                    + ',L'
                                    + CONVERT(VARCHAR(3),@tintOrder)
                                    + '.vcData'
                                    + ' ['
                                    + @vcColumnName + ']'
                    SET @WhereCondition = @WhereCondition
                                            + ' left Join CFW_FLD_Values_Opp CFW'
                                            + CONVERT(VARCHAR(3),@tintOrder)
                                            + '               
                                                                                                                                               on CFW'
                                            + CONVERT(VARCHAR(3),@tintOrder)
                                            + '.Fld_Id='
                                            + CONVERT(VARCHAR(10),@numFieldId)
                                            + 'and CFW'
                                            + CONVERT(VARCHAR(3),@tintOrder)
                                            + '.RecId=Opp.numOppid     '
                    SET @WhereCondition = @WhereCondition
                                            + ' left Join ListDetails L'
                                            + CONVERT(VARCHAR(3),@tintOrder)
                                            + ' on L'
                                            + CONVERT(VARCHAR(3),@tintOrder)
                                            + '.numListItemID=CFW'
                                            + CONVERT(VARCHAR(3),@tintOrder)
                                            + '.Fld_Value'
                  END
        END
      
     
       select top 1 @tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,                
  @vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,@numListID=numListID,@vcLookBackTableName=vcLookBackTableName                                               
,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,
@ListRelID=ListRelID
  from #tempForm WHERE tintOrder > @tintOrder-1 order by tintOrder asc            
 
  if @@rowcount=0 set @tintOrder=0 
 END 
  
  -------Change Row Color-------
Declare @vcCSOrigDbCOlumnName as varchar(50),@vcCSLookBackTableName as varchar(50),@vcCSAssociatedControlType as varchar(50)
SET @vcCSOrigDbCOlumnName=''
SET @vcCSLookBackTableName=''

--select top 1 @vcCSOrigDbCOlumnName=DFM.vcOrigDbCOlumnName,@vcCSLookBackTableName=DFM.vcLookBackTableName from DycFieldColorScheme DFCS join DycFieldMaster DFM on 
--DFCS.numModuleID=DFM.numModuleID and DFCS.numFieldID=DFM.numFieldID
--where DFCS.numDomainID=@numDomainID and DFM.numModuleID=1

Create table #tempColorScheme(vcOrigDbCOlumnName varchar(50),vcLookBackTableName varchar(50),vcFieldValue varchar(50),vcFieldValue1 varchar(50),vcColorScheme varchar(50),vcAssociatedControlType varchar(50))

insert into #tempColorScheme select DFM.vcOrigDbCOlumnName,DFM.vcLookBackTableName,DFCS.vcFieldValue,DFCS.vcFieldValue1,DFCS.vcColorScheme,DFFM.vcAssociatedControlType 
from DycFieldColorScheme DFCS join DycFormField_Mapping DFFM on DFCS.numFieldID=DFFM.numFieldID
join DycFieldMaster DFM on DFM.numModuleID=DFFM.numModuleID and DFM.numFieldID=DFFM.numFieldID
where DFCS.numDomainID=@numDomainID and DFFM.numFormID=@numFormId AND DFCS.numFormID=@numFormId and isnull(DFFM.bitAllowGridColor,0)=1

IF(SELECT COUNT(*) FROM #tempColorScheme)>0
BEGIN
   select top 1 @vcCSOrigDbCOlumnName=vcOrigDbCOlumnName,@vcCSLookBackTableName=vcLookBackTableName,@vcCSAssociatedControlType=vcAssociatedControlType from #tempColorScheme
END   
----------------------------                       

IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
BEGIN
set @strSql=@strSql+',tCS.vcColorScheme'
END

  
  SET @strSql = @strSql + ' FROM OpportunityMaster Opp                                                               
                            INNER JOIN AdditionalContactsInformation ADC ON Opp.numContactId = ADC.numContactId                                                               
							INNER JOIN DivisionMaster DM ON Opp.numDivisionId = DM.numDivisionID AND ADC.numDivisionId = DM.numDivisionID                                                               
                            INNER JOIN CompanyInfo cmp ON DM.numCompanyID = cmp.numCompanyId '
  
  IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
BEGIN
	 if @vcCSLookBackTableName = 'AdditionalContactsInformation'                  
		set @Prefix = 'ADC.'                  
     if @vcCSLookBackTableName = 'DivisionMaster'                  
		set @Prefix = 'DM.'
	 if @vcCSLookBackTableName = 'CompanyInfo'                  
		set @Prefix = 'CMP.'   
	 if @vcCSLookBackTableName = 'OpportunityMaster'                  
		set @Prefix = 'Opp.'   
 
	IF @vcCSAssociatedControlType='DateField'
	BEGIN
			set @strSql=@strSql+' left join #tempColorScheme tCS on CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) >= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue as int),GETUTCDATE()),111)
			 and CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) <= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue1 as int),GETUTCDATE()),111)'
	END
	ELSE IF @vcCSAssociatedControlType='SelectBox' OR @vcCSAssociatedControlType='ListBox' OR @vcCSAssociatedControlType='CheckBox'
	BEGIN
		set @strSql=@strSql+' left join #tempColorScheme tCS on tCS.vcFieldValue=' + @Prefix + @vcCSOrigDbCOlumnName
	END
END
                          
if @columnName like 'CFW.Cust%'             
begin            
 SET @strSql = @strSql + ' left Join CFW_Fld_Values_Opp CFW on CFW.RecId=Opp.numOppid and CFW.fld_id= '+replace(@columnName,'CFW.Cust','') +' '                                                     
 set @columnName='CFW.Fld_Value'            
end 

SET @strSql = @strSql   + @WhereCondition + ' join (select ID,numOppId,intTotalProgress,vcInventoryStatus FROM  #tempTable )T on T.numOppId=Opp.numOppId                                              
                          WHERE ID > ' + CONVERT(VARCHAR(10),@firstRec) + ' and ID <' + CONVERT(VARCHAR(10),@lastRec)
    
IF (@tintFilterBy = 5) --Sort alphabetically
   SET @strSql = @strSql +' order by ID '
ELSE IF @columnName = 'OI.vcInventoryStatus'  
   SET @strSql =  @strSql + ' ORDER BY  T.vcInventoryStatus '  + ' ' + @columnSortOrder     
else  IF (@columnName = 'numAssignedBy' OR @columnName = 'numAssignedTo')
   SET @strSql = @strSql + ' ORDER BY  opp.' +  @columnName + ' ' + @columnSortOrder 
else  IF (@columnName = 'PP.intTotalProgress')
   SET @strSql = @strSql + ' ORDER BY  T.intTotalProgress ' + @columnSortOrder 
ELSE IF @columnName <> 'opp.bintCreatedDate' and @columnName <> 'vcPOppName' and @columnName <> 'bintCreatedDate' and @columnName <> 'vcCompanyName'
   SET @strSql = @strSql + ' ORDER BY  ' +  @columnName + ' '+ @columnSortOrder
ELSE 
   SET @strSql = @strSql +' order by ID '

print @strSql                      
              
exec (@strSql)  

SELECT * FROM #tempForm

DROP TABLE #tempForm

drop table #tempColorScheme

DROP TABLE #tempTable
/****** Object:  StoredProcedure [dbo].[USP_GetImportWareHouseItems]    Script Date: 07/26/2008 16:18:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                              
GO
-- exec USP_GetImportWareHouseItems @numDomainID=1,@numItemGroupID=0,@byteMode=1,@bitSerialized=0,@bitLotNo=0
-- exec USP_GetImportWareHouseItems @numDomainID=1,@numItemGroupID=125,@byteMode=1,@bitSerialized=0,@bitLotNo=0
-- exec USP_GetImportWareHouseItems @numDomainID=1,@numItemGroupID=125,@byteMode=2,@bitSerialized=1,@bitLotNo=0
-- exec USP_GetImportWareHouseItems @numDomainID=1,@numItemGroupID=125,@byteMode=2,@bitSerialized=0,@bitLotNo=1
-- exec USP_GetImportWareHouseItems @numDomainID=150,@numItemGroupID=384,@byteMode=2,@bitSerialized=1,@bitLotNo=0
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetImportWareHouseItems' ) 
    DROP PROCEDURE USP_GetImportWareHouseItems
GO
CREATE PROCEDURE [dbo].[USP_GetImportWareHouseItems]
    @numDomainID AS NUMERIC(9) = 0,
    @numItemGroupID AS NUMERIC(9) = 0,
    @byteMode AS TINYINT = 0,
    @bitSerialized AS BIT = 0,
    @bitLotNo AS BIT = 0
AS 
    DECLARE @str AS VARCHAR(2000) ;
    SET @str = ''               
    DECLARE @str1 AS VARCHAR(2000)               
    DECLARE @ColName AS VARCHAR(25)                      
                        
--Create a Temporary table to hold data                                                            
    CREATE TABLE #tempTable
        (
          ID INT IDENTITY
                 PRIMARY KEY,
          vcFormFieldName NVARCHAR(50),
          numFormFieldId NUMERIC(18),
          tintOrder TINYINT,
          cCtype CHAR(1),
          vcAssociatedControlType NVARCHAR(50),
          vcDbColumnName NVARCHAR(50),
          vcLookBackTableName VARCHAR(50)
        )                         
	
    DECLARE @strSQL AS NVARCHAR(MAX)
    DECLARE @strWHERE AS NVARCHAR(MAX)
	
    SET @strWHERE = ' WHERE 1=1 AND numFormID = 48 AND ISNULL(DFFM.bitImport,0) <> 0 '
    SET @strSQL = '                        
    INSERT INTO #tempTable ( vcFormFieldName,numFormFieldId,tintOrder,cCtype,vcAssociatedControlType,vcDbColumnName,vcLookBackTableName )
            SELECT  DFM.vcFieldName AS vcFormFieldName,
                    DFM.numFieldID AS numFormFieldId,
                    ISNULL(DFFM.[order], 0) as tintOrder,
                    ''R'' as cCtype,
                    DFM.vcAssociatedControlType AS vcAssociatedControlType,
                    DFM.vcDbColumnName AS vcDbColumnName,
                    ISNULL(DFM.vcLookBackTableName, '''') AS vcLookBackTableName
            FROM    dbo.DycFieldMaster DFM
            INNER JOIN dycFormField_Mapping DFFM ON DFM.numFieldId = DFFM.numFieldID ' 
            
    IF ISNULL(@numItemGroupID, 0) > 0 
        BEGIN
            SET @strWHERE = @strWHERE
                + ' AND DFM.numFieldID IN (SELECT numFieldId FROM dycFormField_Mapping WHERE numFormID = 48 )  '								      
			
            SET @strSQL = @strSQL + @strWHERE + ' 
            UNION
            SELECT  Fld_label AS vcFormFieldName,
                    Fld_id AS numFormFieldId,
                    100 AS tintOrder,
                    ''C'' AS cCtype,
                    Fld_Type AS vcAssociatedControlType,
                    '''' AS vcDbColumnName,
                    '''' AS vcLookBackTableName
            FROM    CFW_Fld_Master
                    JOIN ItemGroupsDTL ON numOppAccAttrID = Fld_id
            WHERE   numItemGroupID = ' + CAST(@numItemGroupID AS VARCHAR(100))
                + ' AND tintType = 2
            ORDER BY tintOrder ' 
            
        END
    ELSE 
        BEGIN
            SET @strWHERE = @strWHERE
                + ' AND DFM.numFieldID NOT IN (SELECT numFieldId FROM dycFormField_Mapping WHERE numFormID = 48 AND vcFieldName IN (''UPC (M)'',''SKU (M)'',''Serial No'',''Comments'',''Lot Qty''))'
			
            SET @strSQL = @strSQL + @strWHERE + ' ORDER BY tintOrder ' 
            
        END
                  
    PRINT @strSQL
    EXEC SP_EXECUTESQL @strSQL
	
    --SELECT  * FROM    #tempTable
        
    DECLARE @ID AS INT,
        @vcFormFieldName AS NVARCHAR(50),
        @numFormFieldId AS NUMERIC(18),
        @tintOrder AS TINYINT,
        @cCtype AS CHAR(1),
        @vcAssociatedControlType AS NVARCHAR(50),
        @vcDbColumnName AS NVARCHAR(50),
        @vcLookBackTableName VARCHAR(20)

    IF @byteMode = 1 
        BEGIN     
            SELECT TOP 1
                    @ID = ID,
                    @vcFormFieldName = vcFormFieldName,
                    @numFormFieldId = numFormFieldId,
                    @tintOrder = tintOrder,
                    @cCtype = cCtype,
                    @vcAssociatedControlType = vcAssociatedControlType,
                    @vcDbColumnName = vcDbColumnName,
                    @vcLookBackTableName = vcLookBackTableName
            FROM    #tempTable                         
                         
            WHILE @ID > 0                        
                BEGIN 
                    IF @ID > 1 
                        SET @str = @str + ','

                    IF @vcDbColumnName = 'numWareHouseID' 
                        SET @vcDbColumnName = 'vcWareHouse'

                    IF @cCtype = 'R' 
                        IF @vcDbColumnName = 'vcLocation' 
                            BEGIN
                                SET @str = @str + 'WL.' + @vcDbColumnName
                                    + ' as [' + @vcFormFieldName + ']'    
                            END
                        ELSE 
                            BEGIN
                                SET @str = @str + @vcLookBackTableName + '.'
                                    + @vcDbColumnName + ' as ['
                                    + @vcFormFieldName + ']'                                        
                            END

                    ELSE 
                        SET @str = @str + ' dbo.GetCustFldItemsValue('
                            + CONVERT(VARCHAR(15), @numFormFieldId)
                            + ',9,WareHouseItems.numWareHouseItemID,(CASE WHEN Item.bitSerialized=1 or Item.bitLotNo=1 THEN 1 ELSE 0 end),'''
                            + @vcAssociatedControlType + ''') as ['
                            + @vcFormFieldName + ']'      
                                                              
                    SELECT TOP 1
                            @ID = ID,
                            @vcFormFieldName = vcFormFieldName,
                            @numFormFieldId = numFormFieldId,
                            @tintOrder = tintOrder,
                            @cCtype = cCtype,
                            @vcAssociatedControlType = vcAssociatedControlType,
                            @vcDbColumnName = vcDbColumnName,
                            @vcLookBackTableName = vcLookBackTableName
                    FROM    #tempTable
                    WHERE   ID > @ID 
   
                    IF @@rowcount = 0 
                        SET @ID = 0                        
                END                  
                      
            SET @str = RTRIM(@str)       
            PRINT 'SQL :' + @str                       
            SET @str1 = ' select ' + @str
                + ' from Item 
						LEFT join WareHouseItems on Item.numItemCode=WareHouseItems.numItemID
						LEFT join Warehouses on Warehouses.numWareHouseID=WareHouseItems.numWareHouseID                                    
						LEFT JOIN WareHouseItmsDTL ON WareHouseItmsDTL.numWareHouseItemID = dbo.WareHouseItems.numWareHouseItemID
						LEFT JOIN dbo.WarehouseLocation WL ON WareHouseItems.numWLocationID = WL.numWLocationID and WareHouseItems.numDomainID = WL.numDomainID 
						where Item.numDomainID='
                + CONVERT(VARCHAR(15), @numDomainID)
                + ' AND Item.numItemGroup='
                + CONVERT(VARCHAR(15), @numItemGroupID)
                + ' ORDER BY dbo.WareHouseItems.numWareHouseItemID DESC'
				   
            PRINT ( @str1 )
            EXEC ( @str1
                ) 
                           
        END

    ELSE 
        IF @byteMode = 2 
            BEGIN
                SELECT TOP 1
                        @ID = ID,
                        @vcFormFieldName = vcFormFieldName,
                        @numFormFieldId = numFormFieldId,
                        @tintOrder = tintOrder,
                        @cCtype = cCtype,
                        @vcAssociatedControlType = vcAssociatedControlType,
                        @vcDbColumnName = vcDbColumnName,
                        @vcLookBackTableName = vcLookBackTableName
                FROM    #tempTable                         
                         
                WHILE @ID > 0                        
                    BEGIN 
                        IF @ID > 1 
                            SET @str = @str + ','

                        IF @vcDbColumnName = 'numWareHouseID' 
                            SET @vcDbColumnName = 'vcWareHouse'

                        IF @cCtype = 'R' 
                            IF @vcDbColumnName = 'vcLocation' 
                                BEGIN
                                    SET @str = @str + 'WL.' + @vcDbColumnName
                                        + ' as [' + @vcFormFieldName + ']'    
                                END
                            ELSE 
                                BEGIN
                                    SET @str = @str + @vcLookBackTableName
                                        + '.' + @vcDbColumnName + ' as ['
                                        + @vcFormFieldName + ']'                                        	
                                END
								
                        ELSE 
                            SET @str = @str + ' dbo.GetCustFldItemsValue('
                                + CONVERT(VARCHAR(15), @numFormFieldId)
                                + ',9,numWareHouseItmsDTLID,(CASE WHEN Item.bitSerialized=1 or Item.bitLotNo=1 THEN 1 ELSE 0 end),'''
                                + @vcAssociatedControlType + ''') as ['
                                + @vcFormFieldName + ']'                                        
							
                        SELECT TOP 1
                                @ID = ID,
                                @vcFormFieldName = vcFormFieldName,
                                @numFormFieldId = numFormFieldId,
                                @tintOrder = tintOrder,
                                @cCtype = cCtype,
                                @vcAssociatedControlType = vcAssociatedControlType,
                                @vcDbColumnName = vcDbColumnName,
                                @vcLookBackTableName = vcLookBackTableName
                        FROM    #tempTable
                        WHERE   ID > @ID                        
   
                        IF @@rowcount = 0 
                            SET @ID = 0                        
                    END                        
                
                PRINT @str1       
                SET @str1 = ' SELECT   DISTINCT ' + @str 
							--+ ',vcSerialNo AS ' + CASE WHEN @bitSerialized = 1 THEN '[Serial No]' WHEN @bitLotNo = 1 THEN '[LOT No]' END 
                    + ',WareHouseItmsDTL.vcComments AS Comments'

                IF @bitLotNo = 1 
                    SET @str1 = @str1 + ',WareHouseItmsDTL.numQty AS Qty'

                SET @str1 = @str1
                    + ' FROM ITEM 
						LEFT JOIN WareHouseItems  ON Item.numItemCode=WareHouseItems.numItemID
						LEFT JOIN Warehouses  ON Warehouses.numWareHouseID=WareHouseItems.numWareHouseID      
						LEFT JOIN WareHouseItmsDTL ON WareHouseItmsDTL.numWareHouseItemID = dbo.WareHouseItems.numWareHouseItemID
						LEFT JOIN dbo.WarehouseLocation WL ON WareHouseItems.numWLocationID = WL.numWLocationID and WareHouseItems.numDomainID = WL.numDomainID 
						WHERE Item.numDomainID='
                    + CONVERT(VARCHAR(15), @numDomainID)
                    + ' and Item.numItemGroup='
                    + CONVERT(VARCHAR(15), @numItemGroupID)
                    + '
						AND ISNULL(WareHouseItmsDTL.numQty,0)>0 AND (Item.bitSerialized='
                    + CONVERT(VARCHAR(15), @bitSerialized)
                    + ' AND Item.bitLotNo=' + CONVERT(VARCHAR(15), @bitLotNo)
                    + ')'
                    + ' ORDER BY dbo.WareHouseItems.numWareHouseItemID DESC'
                  
                SET @str1 = REPLACE(@str1, '[Serial No]', '[Serial/Lot #s]')
                SET @str1 = REPLACE(@str1, 'WareHouseItmsDTL.vcSerialNo',
                                    'NULL')
                SET @str1 = REPLACE(@str1, 'WareHouseItmsDTL.vcComments',
                                    'NULL')
                SET @str1 = REPLACE(@str1, 'WareHouseItmsDTL.vcSerialNo',
                                    'NULL')
                SET @str1 = REPLACE(@str1, 'WareHouseItmsDTL.numQty', 'NULL')
                SET @str1 = REPLACE(@str1, 'WareHouseItmsDTL.numQty', 'NULL')
				
                PRINT ( @str1 )           
                EXEC ( @str1
                    )
            END

        ELSE 
            BEGIN	
                SELECT  vcFormFieldName,
                        numFormFieldId,
                        tintOrder,
                        cCtype,
                        vcAssociatedControlType,
                        vcDbColumnName,
                        vcLookBackTableName
                FROM    #tempTable
                ORDER BY tintOrder
            END

    DROP TABLE #tempTable
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetItemBarcodeList')
DROP PROCEDURE USP_GetItemBarcodeList
GO
CREATE PROCEDURE [dbo].[USP_GetItemBarcodeList]                              
@numDomainID as numeric(9)=0,    
@strItemIdList as text,
@bitAllItems as bit=0,
@tinyBarcodeBasedOn tinyint=0
as                
      
Create table #tempTable (                  
  numItemCode numeric(9)                                      
  )  

IF @bitAllItems=0
   insert into #tempTable select Item FROM dbo.DelimitedSplit8K(@strItemIdList,',')  
else
   insert into #tempTable select numItemCode from item where charItemType='P' and  numDomainID=@numDomainID

declare @str1 as varchar(8000);SET @str1=''               

declare @str as varchar(8000);SET @str=''' '''               

--Create a Temporary table to hold data                                                            
create table #tempCustTable ( ID INT IDENTITY PRIMARY KEY,                                                                      
 numCusFlDItemID numeric(9),
 fld_label varchar(50),
fld_type varchar(50)                                                         
 )                         
 declare @ID as numeric(9)                        
 declare @numCusFlDItemID as varchar(20)                        
 declare @fld_label as varchar(100),@fld_type as varchar(100)                        
 set @ID=0                        

If @tinyBarcodeBasedOn=0 --UPC/Barcode
BEGIN
                        
insert into #tempCustTable(numCusFlDItemID,fld_label,fld_type)                                                            
select distinct numOppAccAttrID,fld_label,fld_type from CFW_Fld_Master join ItemGroupsDTL on numOppAccAttrID=Fld_id where numDomainID=@numDomainID and tintType=2 
            
 select top 1 @ID=ID,@numCusFlDItemID=numCusFlDItemID,@fld_label=fld_label,@fld_type=fld_type from #tempCustTable                         
                         
 while @ID>0                        
 begin    
	IF @ID=1  SET @str='''' + @fld_label
	Else   SET @str= @str + ' + ''  ' + @fld_label  
               
	set @str=@str+ ':'' + dbo.GetCustFldItemsValue('+@numCusFlDItemID+',9,numWareHouseItemID,(CASE WHEN I.bitSerialized=0 THEN I.bitLotNo ELSE I.bitSerialized end),'''+@fld_type+''')'                                        
                          
   select top 1 @ID=ID,@numCusFlDItemID=numCusFlDItemID,@fld_label=fld_label from #tempCustTable                         
   where ID >@ID                        
   if @@rowcount=0 set @ID=0                        
 end 

set @str1 ='select I.numItemCode,I.vcItemName,I.numBarCodeId,'''' as vcWareHouse,'''' as vcAttribute 
			from Item I 
			join #tempTable T on I.numItemCode = T.numItemCode 
			where I.numDomainID='+convert(varchar(15),@numDomainID) + ' 
			and isnull(numItemGroup,0)=0 
			and I.numBarCodeId is not null 
			and len(I.numBarCodeId)>1 
 
			Union

			select I.numItemCode,I.vcItemName,WI.vcBarCode as numBarCodeId,W.vcWareHouse,' + @str + ' as vcAttribute  
			from Item I 
			join #tempTable T on I.numItemCode=T.numItemCode
			join WareHouseItems WI on WI.numDomainID='+convert(varchar(15),@numDomainID) + ' and I.numItemCode=WI.numItemID  
			join Warehouses W on W.numDomainID='+convert(varchar(15),@numDomainID) + ' and W.numWareHouseID=WI.numWareHouseID  
			where I.numDomainID='+convert(varchar(15),@numDomainID) + ' 
			and isnull(numItemGroup,0)>0 
			and WI.vcBarCode is not null 
			and len(WI.vcBarCode)>1'


END

ELSE If @tinyBarcodeBasedOn=1 --SKU
	BEGIN
	set @str1 ='select I.numItemCode,I.vcItemName,I.vcSKU as numBarCodeId,'''' as vcWareHouse,'''' as vcAttribute from Item I join #tempTable T on I.numItemCode=T.numItemCode 
	where I.numDomainID='+convert(varchar(15),@numDomainID) + ' and I.vcSKU is not null and len(I.vcSKU)>1'
	END

ELSE If @tinyBarcodeBasedOn=2 --Vendor Part#
	BEGIN
	set @str1 ='select I.numItemCode,I.vcItemName,Vendor.vcPartNo as numBarCodeId,com.vcCompanyName as vcWareHouse,'''' as vcAttribute
	from Vendor 
	join Item I on I.numItemCode= Vendor.numItemCode
	join #tempTable T on I.numItemCode=T.numItemCode  
	join divisionMaster div on div.numdivisionid=Vendor.numVendorid        
	join companyInfo com on com.numCompanyID=div.numCompanyID        
	WHERE I.numDomainID='+convert(varchar(15),@numDomainID) + ' and Vendor.numDomainID='+convert(varchar(15),@numDomainID) + ' 
	and Vendor.vcPartNo is not null and len(Vendor.vcPartNo)>1'
	END

ELSE If @tinyBarcodeBasedOn=3 --Serial #/LOT #
	BEGIN
	                        
	insert into #tempCustTable(numCusFlDItemID,fld_label,fld_type)                                                            
	select distinct numOppAccAttrID,fld_label,fld_type from CFW_Fld_Master join ItemGroupsDTL on numOppAccAttrID=Fld_id where numDomainID=@numDomainID and tintType=2 
	            
	 select top 1 @ID=ID,@numCusFlDItemID=numCusFlDItemID,@fld_label=fld_label,@fld_type=fld_type from #tempCustTable                         
	                         
	 while @ID>0                        
	 begin    
		IF @ID=1  SET @str='''' + @fld_label
		Else   SET @str= @str + ' + ''  ' + @fld_label  
	               
		set @str=@str+ ':'' + dbo.GetCustFldItemsValue('+@numCusFlDItemID+',9,numWareHouseItmsDTLID,(CASE WHEN I.bitSerialized=0 THEN I.bitLotNo ELSE I.bitSerialized end),'''+@fld_type+''')'                                        
	                          
	   select top 1 @ID=ID,@numCusFlDItemID=numCusFlDItemID,@fld_label=fld_label from #tempCustTable                         
	   where ID >@ID                        
	   if @@rowcount=0 set @ID=0                        
	 end 

	set @str1 ='select I.numItemCode,I.vcItemName,WDTL.vcSerialNo as numBarCodeId,W.vcWareHouse,' + @str + ' as vcAttribute 
	from Item I join #tempTable T on I.numItemCode=T.numItemCode
	join WareHouseItems WI on WI.numDomainID='+convert(varchar(15),@numDomainID) + ' and I.numItemCode=WI.numItemID  
	join Warehouses W on W.numDomainID='+convert(varchar(15),@numDomainID) + ' and W.numWareHouseID=WI.numWareHouseID  
	join WareHouseItmsDTL WDTL on WDTL.numWareHouseItemID=WI.numWareHouseItemID    
	where I.numDomainID='+convert(varchar(15),@numDomainID) + ' and (I.bitSerialized=1 or I.bitLotNo=1) and WDTL.vcSerialNo is not null and len(WDTL.vcSerialNo)>1
	and ISNULL(WDTL.numQty,0) > 0 '

	END

ELSE If @tinyBarcodeBasedOn = 4 --Item Code
	BEGIN
	set @str1 ='SELECT I.numItemCode,I.vcItemName,I.numItemCode AS numBarCodeId,'''' AS vcWareHouse,'''' AS vcAttribute 
				FROM Item I JOIN #tempTable T ON I.numItemCode = T.numItemCode 
				WHERE I.numDomainID = ' + CONVERT(VARCHAR(15),@numDomainID)
	END


print(@str1)           
exec (@str1)  

drop table #tempCustTable
drop table  #tempTable
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetItemFromUPCSKU')
DROP PROCEDURE USP_GetItemFromUPCSKU
GO
CREATE PROCEDURE [dbo].[USP_GetItemFromUPCSKU]                              
@numDomainID as numeric(9)=0,
@vcUPCSKU as varchar(50)=''    
as                
     
declare @sql as nvarchar(max)
 
set @sql ='select I.numItemCode,I.vcItemName,0 as numWareHouseID,0 as numWareHouseItemID from Item I 
where I.numDomainID=@numDomainID and isnull(numItemGroup,0)=0 
and ((I.numBarCodeId is not null and len(I.numBarCodeId)>1 and I.numBarCodeId=@vcUPCSKU)
	OR (I.vcSKU is not null and len(I.vcSKU)>0 and I.vcSKU=@vcUPCSKU))
 
Union

select I.numItemCode,I.vcItemName,W.numWareHouseID,WI.numWareHouseItemID from Item I 
	join WareHouseItems WI on WI.numDomainID=@numDomainID and I.numItemCode=WI.numItemID  
	join Warehouses W on W.numDomainID=@numDomainID and W.numWareHouseID=WI.numWareHouseID  
	where I.numDomainID=@numDomainID and isnull(numItemGroup,0)>0 
			and ((WI.vcBarCode is not null and len(WI.vcBarCode)>0 and WI.vcBarCode=@vcUPCSKU)
				OR (WI.vcWHSKU is not null and len(WI.vcWHSKU)>0 and WI.vcWHSKU =@vcUPCSKU))

UNION

select I.numItemCode,I.vcItemName,0 as numWareHouseID,0 as numWareHouseItemID
from Vendor join Item I on I.numItemCode= Vendor.numItemCode
WHERE I.numDomainID=@numDomainID and Vendor.numDomainID=@numDomainID 
and Vendor.vcPartNo is not null and len(Vendor.vcPartNo)>0 and Vendor.vcPartNo=@vcUPCSKU

UNION

select I.numItemCode,I.vcItemName,W.numWareHouseID,WI.numWareHouseItemID
from Item I 
join WareHouseItems WI on WI.numDomainID=@numDomainID and I.numItemCode=WI.numItemID  
join Warehouses W on W.numDomainID=@numDomainID and W.numWareHouseID=WI.numWareHouseID  
join WareHouseItmsDTL WDTL on WDTL.numWareHouseItemID=WI.numWareHouseItemID    
where I.numDomainID=@numDomainID and (I.bitSerialized=1 or I.bitLotNo=1) 
and WDTL.vcSerialNo is not null and len(WDTL.vcSerialNo)>0 and WDTL.vcSerialNo=@vcUPCSKU
and ISNULL(WDTL.numQty,0) > 0 '


print @sql
EXECUTE sp_executeSQL @sql, N'@numDomainID numeric(9),@vcUPCSKU varchar(50)',@numDomainID, @vcUPCSKU

GO
/****** Object:  StoredProcedure [dbo].[USP_GetItemsAttrWarehouse]    Script Date: 07/26/2008 16:17:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec USP_GetWareHouseItems 6                            
--created by anoop jayaraj                              
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getitemsattrwarehouse')
DROP PROCEDURE usp_getitemsattrwarehouse
GO
CREATE PROCEDURE [dbo].[USP_GetItemsAttrWarehouse]                              
@numItemCode as numeric(9)=0,          
@strAtrr as varchar(100),          
@numWarehouseItemID as numeric(9)=0                            
as                              
                              
                          
                              
declare @bitSerialize as bit                      
declare @str as varchar(2000)                    
set @str=''                       
                        
                        
declare @numItemGroupID as numeric(9)                        
                        
select @numItemGroupID=numItemGroup,@bitSerialize=bitSerialized from Item where numItemCode=@numItemCode                           
              
                        
                       
select numWareHouseItemID,vcWarehouse,W.numWareHouseID,isnull(numOnHand,0) as [OnHand] ,isnull(numOnOrder,0) as [OnOrder] ,isnull(numReorder,0) as [Reorder] ,isnull(numAllocation,0) as [Allocation] ,isnull(numBackOrder,0) as [BackOrder],            
 0 as Op_Flag from WareHouseItems                           
 join Warehouses W                             
 on W.numWareHouseID=WareHouseItems.numWareHouseID                                    
 where numWarehouseItemID=@numWarehouseItemID          
          
if @bitSerialize=1          
begin          
declare @strSQL varchar(1000)          
set @strSQL=''          
if @strAtrr!=''            
begin            
            
 Declare @Cnt int            
   Set @Cnt = 1            
  declare @SplitOn char(1)            
  set @SplitOn=','            
  set @strSQL='SELECT recid            
     FROM CFW_Fld_Values_Serialized_Items where  bitSerialized ='+ convert(varchar(1),@bitSerialize)            
   While (Charindex(@SplitOn,@strAtrr)>0)            
   Begin            
    if  @Cnt=1 set @strSQL=@strSQL+' and fld_value=''' + (ltrim(rtrim(Substring(@strAtrr,1,Charindex(@SplitOn,@strAtrr)-1))))+''''            
    else set @strSQL=@strSQL+' or fld_value=''' + (ltrim(rtrim(Substring(@strAtrr,1,Charindex(@SplitOn,@strAtrr)-1))))+''''            
    Set @strAtrr = Substring(@strAtrr,Charindex(@SplitOn,@strAtrr)+1,len(@strAtrr))            
    Set @Cnt = @Cnt + 1            
   End            
  if  @Cnt=1 set @strSQL=@strSQL+' and fld_value=''' + @strAtrr + ''' GROUP BY recid            
   HAVING count(*) > '+convert(varchar(10), @Cnt-1)             
    else set @strSQL=@strSQL+' or fld_value=''' + @strAtrr + ''' GROUP BY recid            
   HAVING count(*) > '+convert(varchar(10), @Cnt-1)            
             
            
            
end           
          
          
 --Create a Temporary table to hold data                                                            
 Create table #tempTable ( ID INT IDENTITY PRIMARY KEY,                                                                      
  numCusFlDItemID numeric(9)                                                         
  )                         
                         
 insert into #tempTable                         
 (numCusFlDItemID)                                                            
 select distinct(numOppAccAttrID) from ItemGroupsDTL where numItemGroupID=@numItemGroupID and tintType=2                    
                           
                         
  declare @ID as numeric(9)                        
  declare @numCusFlDItemID as varchar(20)                        
  declare @fld_label as varchar(100)                        
  set @ID=0                        
  select top 1 @ID=ID,@numCusFlDItemID=numCusFlDItemID,@fld_label=fld_label from #tempTable                         
  join CFW_Fld_Master on numCusFlDItemID=Fld_ID                        
                          
  while @ID>0                        
  begin                        
                           
    set @str=@str+',  dbo.GetCustFldItems('+@numCusFlDItemID+',9,numWareHouseItmsDTLID,1) as ['+ @fld_label+']'                                        
                           
    select top 1 @ID=ID,@numCusFlDItemID=numCusFlDItemID,@fld_label=fld_label from #tempTable                         
    join CFW_Fld_Master on numCusFlDItemID=Fld_ID and ID >@ID                        
    if @@rowcount=0 set @ID=0                        
                           
  end          
          
          
 set @str='select numWareHouseItmsDTLID,WDTL.numWareHouseItemID, vcSerialNo,0 as Op_Flag,WDTL.vcComments as Comments'+ case when @bitSerialize=1 then @str else '' end +' from WareHouseItmsDTL WDTL                             
 join WareHouseItems WI                             
 on WDTL.numWareHouseItemID=WI.numWareHouseItemID                              
 where ISNULL(WDTL.numQty,0)>0 and WI.numWarehouseItemID='+ convert(varchar(15),@numWarehouseItemID)              
    if @strAtrr!=''  set @str=@str+' and  numWareHouseItmsDTLID in ('+ @strSQL +')'          
                        
 print @str                       
 exec (@str)          
          
          
                      
 select Fld_label,fld_id,fld_type,numlistid,vcURL from #tempTable                         
  join CFW_Fld_Master on numCusFlDItemID=Fld_ID    
                      
 drop table #tempTable             
end            
else        
begin      
 select numWareHouseItmsDTLID,numWareHouseItemID, vcSerialNo,0 as Op_Flag,vcComments as Comments from    WareHouseItmsDTL where      numWareHouseItemID=@numWarehouseItemID    
     
end
GO
/****** Object:  StoredProcedure [dbo].[USP_GetOppSerializedIndItems]    Script Date: 07/26/2008 16:18:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec USP_GetWareHouseItems 6                          
--created by anoop jayaraj                            
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getoppserializedinditems')
DROP PROCEDURE usp_getoppserializedinditems
GO
CREATE PROCEDURE [dbo].[USP_GetOppSerializedIndItems]                            
@numOppItemCode as numeric(9)=0,      
@numOppID as numeric(9)=0,  
@tintOppType  as tinyint                         
as      
-- THIS PROCEDURE IS NOT USED IN BIZ AND ITS CODE IS NOT RELEVANT TO CURRENT IMPLEMENTATION SO DO NOT USE IT
--declare @numWareHouseItemID as numeric(9)      
--declare @numItemID as numeric(9)      
--declare @bitSerialize as bit                    
--declare @bitLotNo as bit                    

--declare @str as varchar(2000)  
--declare @strSQL as varchar(2000)                       
--declare @ColName as varchar(50)                    
--set @str=''        
      
--select @numWareHouseItemID=numWarehouseItmsID,@numItemID=numItemID from   OpportunityItems      
--join WareHouseItems       
--on OpportunityItems.numWarehouseItmsID= WareHouseItems.numWareHouseItemID      
--where  numOppItemTcode=@numOppItemCode         
                      
                      
--declare @numItemGroupID as numeric(9)                      
                      
--select @numItemGroupID=numItemGroup,@bitSerialize=ISNULL(bitSerialized,0),@bitLotNo=ISNULL(bitLotNo,0) from Item where numItemCode=@numItemID                      
--set @ColName='WareHouseItmsDTL.numWareHouseItmsDTLID,1'                  
            
----Create a Temporary table to hold data                                                          
--Create table #tempTable ( ID INT IDENTITY PRIMARY KEY,                                                                    
-- numCusFlDItemID numeric(9)                                                       
-- )                       
                      
--insert into #tempTable                       
--(numCusFlDItemID)                                                          
--select numOppAccAttrID from ItemGroupsDTL where numItemGroupID=@numItemGroupID and tintType=2     
              
-- declare @ID as numeric(9)                      
-- declare @numCusFlDItemID as varchar(20)                      
-- declare @fld_label as varchar(100)                      
-- set @ID=0                      
-- select top 1 @ID=ID,@numCusFlDItemID=numCusFlDItemID,@fld_label=fld_label from #tempTable                       
-- join CFW_Fld_Master on numCusFlDItemID=Fld_ID                                   
-- while @ID>0                      
-- begin                                    
--   set @str=@str+',  dbo.GetCustFldItems('+@numCusFlDItemID+',9,'+@ColName+') as ['+ @fld_label+']'                                      
                        
--   select top 1 @ID=ID,@numCusFlDItemID=numCusFlDItemID,@fld_label=fld_label from #tempTable                       
--   join CFW_Fld_Master on numCusFlDItemID=Fld_ID and ID >@ID                      
--   if @@rowcount=0 set @ID=0                      
                        
-- end        
      
--select numWareHouseItemID,vcWareHouse,
----CAST((dbo.fn_UOMConversion(ISNULL(I.numBaseUnit,0),Opp.numItemCode,I.numDomainId,ISNULL(opp.numUOMId,0)) * Opp.numUnitHour) as numeric(18,0)) as numUnitHour
--numUnitHour,ISNULL(u.vcUnitName,'') vcBaseUOMName
--,@bitSerialize AS bitSerialize,@bitLotNo AS bitLotNo from OpportunityItems Opp     
--join WareHouseItems      
--on Opp.numWarehouseItmsID=WareHouseItems.numWareHouseItemID      
--join Warehouses      
--on Warehouses.numWareHouseID=WareHouseItems.numWareHouseID 
-- join item I on Opp.numItemCode=I.numItemcode  
-- LEFT JOIN  UOM u ON u.numUOMId = I.numBaseUnit       
--where Opp.numOppItemTcode=@numOppItemCode      
      
       
      
           
-- set @strSQL='select WareHouseItmsDTL.numWareHouseItmsDTLID,numWareHouseItemID,vcSerialNo,0 as Op_Flag,
--  isnull(WareHouseItmsDTL.numQty,0) - isnull((select sum(w.numQty) from OppWarehouseSerializedItem w INNER JOIN OpportunityMaster opp ON (w.numOppId=opp.numOppId AND opp.tintOppType=1) where isnull(opp.tintshipped,0)=0 and w.numWarehouseItmsDTLID=WareHouseItmsDTL.numWarehouseItmsDTLID),0) + isnull(OppWarehouseSerializedItem.numQty,0) as TotalQty,isnull(OppWarehouseSerializedItem.numQty,0) as UsedQty
--  '+  @str  +',1 as bitAdded
-- from   OppWarehouseSerializedItem      
-- join WareHouseItmsDTL      
-- on WareHouseItmsDTL.numWareHouseItmsDTLID= OppWarehouseSerializedItem.numWarehouseItmsDTLID                           
-- where numOppID='+ convert(varchar(15),@numOppID)+' and  numWareHouseItemID='+ convert(varchar(15),@numWareHouseItemID)                        
-- print @strSQL                     
-- exec (@strSQL)     
  
  
--select Fld_label,fld_id,fld_type,numlistid,vcURL from #tempTable                       
-- join CFW_Fld_Master on numCusFlDItemID=Fld_ID                    
--drop table #tempTable   
  
--if @tintOppType=1  
--begin   
-- set @strSQL='select numWareHouseItmsDTLID,numWareHouseItemID,vcSerialNo,0 as Op_Flag,
--  isnull(numQty,0) - isnull((select sum(w.numQty) from OppWarehouseSerializedItem w INNER JOIN OpportunityMaster opp ON (w.numOppId=opp.numOppId AND opp.tintOppType=1) where isnull(opp.tintshipped,0)=0 and w.numWarehouseItmsDTLID=WareHouseItmsDTL.numWarehouseItmsDTLID),0) as TotalQty,
-- cast(0 as numeric(9,0)) as UsedQty '+  @str  +',0 as bitAdded
-- from   WareHouseItmsDTL   
--    where (tintStatus is null or tintStatus=0) and  numWareHouseItemID='+ convert(varchar(15),@numWareHouseItemID) +'  
--    and numWareHouseItmsDTLID not in(select numWarehouseItmsDTLID from OppWarehouseSerializedItem where numOppID='+ convert(varchar(15),@numOppID)+' and  numWarehouseItmsID='+ convert(varchar(15),@numWareHouseItemID) +')'                    
-- print @strSQL                     
-- exec (@strSQL)   
--end
GO
/****** Object:  StoredProcedure [dbo].[usp_GetOppserializedinditems_BizDoc]    Script Date: 07/26/2008 16:18:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                           
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_GetOppserializedinditems_BizDoc')
DROP PROCEDURE usp_GetOppserializedinditems_BizDoc
GO
CREATE PROCEDURE [dbo].[usp_GetOppserializedinditems_BizDoc]                            
@numOppItemCode as numeric(9)=0,      
@numOppID as numeric(9)=0,  
@tintOppType  as TINYINT,
@numBizDocID as numeric(9)=0
                         
as      
declare @numWareHouseItemID as numeric(9)      
declare @numItemID as numeric(9)      
declare @bitSerialize as bit                    
declare @bitLotNo as bit                    

declare @str as varchar(2000)  
declare @strSQL as varchar(2000)                       
declare @ColName as varchar(50)                    
set @str=''        
      
select @numWareHouseItemID=numWarehouseItmsID,@numItemID=numItemID from   OpportunityItems      
join WareHouseItems       
on OpportunityItems.numWarehouseItmsID= WareHouseItems.numWareHouseItemID      
where  numOppItemTcode=@numOppItemCode         
                      
                      
declare @numItemGroupID as numeric(9)                      
                      
select @numItemGroupID=numItemGroup,@bitSerialize=ISNULL(bitSerialized,0),@bitLotNo=ISNULL(bitLotNo,0) from Item where numItemCode=@numItemID                      
set @ColName='WareHouseItmsDTL.numWareHouseItmsDTLID,1'                  
            
--Create a Temporary table to hold data                                                          
Create table #tempTable ( ID INT IDENTITY PRIMARY KEY,                                                                    
 numCusFlDItemID numeric(9)                                                       
 )                       
                      
insert into #tempTable                       
(numCusFlDItemID)                                                          
select numOppAccAttrID from ItemGroupsDTL where numItemGroupID=@numItemGroupID and tintType=2     
              
 declare @ID as numeric(9)                      
 declare @numCusFlDItemID as varchar(20)                      
 declare @fld_label as varchar(100),@fld_type as varchar(100)                              
 set @ID=0                      
 select top 1 @ID=ID,@numCusFlDItemID=numCusFlDItemID,@fld_label=fld_label,@fld_type=fld_type from #tempTable                       
 join CFW_Fld_Master on numCusFlDItemID=Fld_ID                                   
 while @ID>0                      
 begin                                    
   set @str=@str+',  dbo.GetCustFldItems('+@numCusFlDItemID+',9,'+@ColName+') as ['+ @fld_label+']'                                      
    
   set @str=@str+',  dbo.GetCustFldItemsValue('+@numCusFlDItemID+',9,'+@ColName+','''+@fld_type+''') as ['+ @fld_label+'Value]'                                        
                    
   select top 1 @ID=ID,@numCusFlDItemID=numCusFlDItemID,@fld_label=fld_label from #tempTable                       
   join CFW_Fld_Master on numCusFlDItemID=Fld_ID and ID >@ID                      
   if @@rowcount=0 set @ID=0                      
                        
 end        
      
select numWareHouseItemID,vcWareHouse,
--CAST((dbo.fn_UOMConversion(ISNULL(I.numBaseUnit,0),Opp.numItemCode,I.numDomainId,ISNULL(opp.numUOMId,0)) * Opp.numUnitHour) as numeric(18,0)) as numUnitHour
ISNULL(u.vcUnitName,'') vcBaseUOMName
,@bitSerialize AS bitSerialize,@bitLotNo AS bitLotNo
--,isnull(OBI.numUnitHour,0) as numUnitHour 
from OpportunityItems Opp     
join WareHouseItems on Opp.numWarehouseItmsID=WareHouseItems.numWareHouseItemID      
join Warehouses on Warehouses.numWareHouseID=WareHouseItems.numWareHouseID 
 join item I on Opp.numItemCode=I.numItemcode  
-- LEFT join OpportunityBizDocs OB on OB.numOppId=Opp.numOppId AND OB.numOppBizDocsId=@numBizDocID 
-- left join OpportunityBizDocItems OBI
--	on OB.numOppBizDocsId=OBI.numOppBizDocId and OBI.numOppItemID=Opp.numoppitemtCode 
 LEFT JOIN  UOM u ON u.numUOMId = I.numBaseUnit       
where Opp.numOppItemTcode=@numOppItemCode    
      
       
      
           
 set @strSQL='select WareHouseItmsDTL.numWareHouseItmsDTLID,numWareHouseItemID,vcSerialNo,0 as Op_Flag,
  isnull(WareHouseItmsDTL.numQty,0) as TotalQty,isnull(OppWarehouseSerializedItem.numQty,0) as UsedQty
  '+  @str  +',1 as bitAdded
 from   OppWarehouseSerializedItem      
 join WareHouseItmsDTL      
 on WareHouseItmsDTL.numWareHouseItmsDTLID= OppWarehouseSerializedItem.numWarehouseItmsDTLID                           
 where numOppID='+ convert(varchar(15),@numOppID)+' and  numWareHouseItemID='+ convert(varchar(15),@numWareHouseItemID) +' and numOppBizDocsId=' + convert(varchar(15),@numBizDocID)                        
 print @strSQL                     
 exec (@strSQL)     
  
  
select Fld_label,fld_id,fld_type,numlistid,vcURL from #tempTable                       
 join CFW_Fld_Master on numCusFlDItemID=Fld_ID                    
drop table #tempTable   
  
if @tintOppType=1  
begin   
 set @strSQL='select numWareHouseItmsDTLID,numWareHouseItemID,vcSerialNo,0 as Op_Flag,
  isnull(numQty,0) as TotalQty,
 cast(0 as numeric(9,0)) as UsedQty '+  @str  +',0 as bitAdded
 from   WareHouseItmsDTL   
    where ISNULL(numQty,0) > 0 AND numWareHouseItemID='+ convert(varchar(15),@numWareHouseItemID) +'  
    and numWareHouseItmsDTLID not in(select numWarehouseItmsDTLID from OppWarehouseSerializedItem where numOppID='+ convert(varchar(15),@numOppID)+' and  numWarehouseItmsID='+ convert(varchar(15),@numWareHouseItemID) +' and numOppBizDocsId=' + convert(varchar(15),@numBizDocID) +')'                    
 print @strSQL                     
 exec (@strSQL)   
end
GO

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetOppSerialLotNumber')
DROP PROCEDURE USP_GetOppSerialLotNumber
GO
CREATE PROCEDURE [dbo].[USP_GetOppSerialLotNumber]
	@numOppID AS NUMERIC(18,0),
	@numOppItemID AS NUMERIC(18,0),
	@numWarehouseItemID AS NUMERIC(18,0)
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @numDomainID AS NUMERIC(18,0)
	SELECT @numDomainID= numDomainId FROM OpportunityMaster WHERE numOppId = @numOppID


	-- Available Serail Lot Numbers
	SELECT
		ROW_NUMBER() OVER(ORDER BY dExpirationDate DESC) AS OrderNo,
		numWareHouseItmsDTLID,
		vcSerialNo,
		numQty,
		dbo.FormatedDateFromDate(dExpirationDate,@numDomainID) AS dExpirationDate
	FROM
		WareHouseItmsDTL
	WHERE
		numWareHouseItemID = @numWarehouseItemID AND
		ISNULL(numQty,0) > 0 
	ORDER BY
		dExpirationDate DESC

    -- Selected Serail Lot Numbers
	SELECT
		ROW_NUMBER() OVER(ORDER BY WareHouseItmsDTL.dExpirationDate DESC) AS OrderNo,
		WareHouseItmsDTL.numWareHouseItmsDTLID,
		WareHouseItmsDTL.vcSerialNo,
		dbo.FormatedDateFromDate(WareHouseItmsDTL.dExpirationDate,@numDomainID) AS dExpirationDate,
		OppWarehouseSerializedItem.numQty,
		OppWarehouseSerializedItem.numOppBizDocsId
	FROM
		OppWarehouseSerializedItem
	JOIN
		WareHouseItmsDTL
	ON
		OppWarehouseSerializedItem.numWarehouseItmsDTLID = WareHouseItmsDTL.numWareHouseItmsDTLID
	WHERE
		numOppID = @numOppID AND
		numOppItemID = @numOppItemID AND
		numWareHouseItemID = @numWarehouseItemID
	ORDER BY
		WareHouseItmsDTL.dExpirationDate DESC

END



GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetOrderItems' ) 
    DROP PROCEDURE USP_GetOrderItems
GO
CREATE PROCEDURE USP_GetOrderItems ( @tintMode TINYINT = 0,
                                     @numOppItemID NUMERIC(9) = 0,
                                     @numOppID NUMERIC(9) = 0,
                                     @numDomainId NUMERIC(9),
                                     @ClientTimeZoneOffset INT,
									 @vcOppItemIDs VARCHAR(2000)='' )
AS 
BEGIN
	
    IF @tintMode = 1 --Add Return grid data & For Recurring Get Original Units to split
        BEGIN
            SELECT  OI.numoppitemtCode,
                    OI.numItemCode,
                    OI.vcItemName,
                    OI.vcItemDesc,
                    OI.numUnitHour numOriginalUnitHour,
					CAST(( dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), OI.numItemCode,
                                    OM.numDomainId, ISNULL(OI.numUOMId, 0))
						* OI.numUnitHour ) AS NUMERIC(18, 2)) AS numUnitHour,
					OI.monPrice
            FROM    dbo.OpportunityMaster OM
                    INNER JOIN dbo.OpportunityItems OI ON OM.numOppId = OI.numOppId
                    INNER JOIN dbo.Item I ON I.numItemCode = OI.numItemCode
            WHERE   OM.numDomainId = @numDomainId
                    AND OM.numOppId = @numOppID
                    
        END
 
    IF @tintMode = 2 -- Get single item based on PK, or all items
		-- Used by Sales/Purchase Template Load items in New Sales Order
        BEGIN 
            SELECT  OI.numoppitemtCode,
                    OI.numOppId,
                    OI.numItemCode,
					OI.bitItemPriceApprovalRequired,
                    OI.numUnitHour numOriginalUnitHour,
                    OI.monPrice,
                    OI.monTotAmount,
                    OI.numSourceID,
                    OI.vcItemDesc,
                    OI.numWarehouseItmsID,
                    OI.vcType,
                    OI.vcAttributes,
                    OI.bitDropShip,
                    ISNULL(OI.numUnitHourReceived,0) AS numUnitHourReceived,
                    OI.bitDiscountType,
                    OI.fltDiscount,
                    OI.monTotAmtBefDiscount,
                    ISNULL(OI.numQtyShipped,0) AS numQtyShipped,
                    OI.vcItemName,
                    OI.vcModelID,
                    OI.vcPathForTImage,
                    OI.vcManufacturer,
                    OI.monVendorCost,
                    ISNULL(OI.numUOMId, 0) AS numUOMId,
                    ISNULL(OI.numUOMId, 0) numUOM, -- used from add edit order
                    ISNULL(U.vcUnitName, '') vcUOMName,
                    dbo.fn_UOMConversion(OI.numUOMId, OI.numItemCode,
                                         OM.numDomainId, NULL) AS UOMConversionFactor,
                    OI.bitWorkOrder,
                    isnull(OI.numVendorWareHouse,0) as numVendorWareHouse,
                    OI.numShipmentMethod,
                    OI.numSOVendorId,
                    OI.numProjectID,
                    OI.numClassID,
                    OI.numProjectStageID,
                    OI.numToWarehouseItemID,
                    WItems.numWarehouseID,
                    W.vcWareHouse AS Warehouse,
					CAST(( dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), OI.numItemCode,
                                    OM.numDomainId, ISNULL(OI.numUOMId, 0))
						* OI.numUnitHour ) AS NUMERIC(18, 2)) AS numUnitHour,
					isnull(OI.numClassID,0) as numClassID,
					isnull(OI.numProjectID,0) as numProjectID,
					dbo.fn_GetUOMName(I.numBaseUnit) AS vcBaseUOMName,
					OI.bitDropShip AS bitAllowDropShip,
					I.numVendorID,
					ISNULL(I.bitFreeShipping,0) AS [bitFreeShipping],
					ISNULL(I.txtItemDesc,'') txtItemDesc,
					ISNULL(I.vcModelID,'') vcModelID,
					ISNULL(I.vcManufacturer,'') vcManufacturer,
					ISNULL(I.numBarCodeId,'') numBarCodeId,
					ISNULL(I.bitSerialized,0) bitSerialized,
					ISNULL(I.bitKitParent,0) bitKitParent,
					ISNULL(I.bitAssembly,0) bitAssembly,
					ISNULL(I.IsArchieve,0) IsArchieve,
					ISNULL(I.bitLotNo,0) bitLotNo,
					ISNULL(WItems.monWListPrice,0) monWListPrice,
					ISNULL(I.bitTaxable, 0) bitTaxable,
                    I.charItemType,
                    I.bitKitParent,
					I.bitAssembly,
					I.bitSerialized,
					ISNULL(I.fltWeight,0) AS [fltWeight],
					ISNULL(I.fltLength,0) AS [fltLength],
					ISNULL(I.fltWidth,0) AS [fltWidth],
					ISNULL(I.fltHeight,0) AS [fltHeight],
					ISNULL(WItems.vcWHSKU,ISNULL(I.vcSKU,'')) AS [vcSKU],
					ISNULL(I.monAverageCost,0) AS monAverageCost,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numOnHand),0) AS VARCHAR(10)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numOnHand,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numOnOrder),0) AS VARCHAR(10)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numOnOrder,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numAllocation),0) AS VARCHAR(10)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numAllocation,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numBackOrder),0) AS VARCHAR(10)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numBackOrder
            FROM    dbo.OpportunityItems OI
                    INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OI.numOppId
                    INNER JOIN dbo.Item I ON I.numItemCode = OI.numItemCode
                    LEFT JOIN UOM U ON U.numUOMId = OI.numUOMId
                    LEFT JOIN WareHouseItems WItems ON WItems.numWareHouseItemID = OI.numWarehouseItmsID
                    LEFT JOIN Warehouses W ON W.numWarehouseID = WItems.numWarehouseID
            WHERE   OI.numOppId = @numOppID
                    AND ( OI.numoppitemtCode = @numOppItemID
                          OR @numOppItemID = 0
                        )
                    AND OM.numDomainId = @numDomainId
                    
        END
 
    IF @tintMode = 3 -- Get item for SO to PO and vice versa creation
        BEGIN
            
            SELECT  OI.numoppitemtCode,
                    OI.numOppId,
					OI.bitItemPriceApprovalRequired,
                    OI.vcItemDesc,
                    ISNULL(OI.vcPathForTImage, '') vcPathForTImage,
                    ISNULL(OI.numWarehouseItmsID, 0) numWarehouseItmsID,
                    OI.vcItemName,
                    OI.vcModelID,
                    ISNULL(OI.vcAttributes, '') vcAttributes,
                    OI.vcType,
                    ISNULL(OI.bitDropShip, 0) bitDropShip,
                    OI.numItemCode,
                    OI.numUnitHour numOriginalUnitHour,
                    ISNULL(OI.numUOMId, 0) AS numUOM,
                    ISNULL(U.vcUnitName, '') vcUOMName,
                    dbo.fn_UOMConversion(OI.numUOMId, OI.numItemCode,
                                         OM.numDomainId, NULL) AS UOMConversionFactor,
                    ISNULL(I.numVendorID, 0) numVendorID,
                    ISNULL(V.monCost, 0) monVendorCost,
                    ISNULL(V.vcPartNo, '') vcPartNo,
                    OI.monPrice,
                    ISNULL(OI.numSOVendorId, 0) numSOVendorId,
                    ISNULL(I.charItemType, 'N') charItemType,
					CAST(( dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), OI.numItemCode,
                                    OM.numDomainId, ISNULL(OI.numUOMId, 0))
						* OI.numUnitHour ) AS NUMERIC(18, 2)) AS numUnitHour,
					isnull(OI.numClassID,0) as numClassID,isnull(OI.numProjectID,0) as numProjectID,ISNULL(I.fltWeight,0) AS [fltWeight],ISNULL(I.bitFreeShipping,0) AS [bitFreeShipping],
					ISNULL(I.fltLength,0) AS [fltLength],
					ISNULL(I.fltWidth,0) AS [fltWidth],
					ISNULL(I.fltHeight,0) AS [fltHeight],
					ISNULL((SELECT vcWHSKU FROM dbo.WareHouseItems WHERE numWarehouseItemID = OI.numWarehouseItmsID ),ISNULL(I.vcSKU,'')) AS [vcSKU]
            FROM    dbo.OpportunityItems OI
                    INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OI.numOppId
                    INNER JOIN dbo.Item I ON I.numItemCode = OI.numItemCode
                    LEFT JOIN UOM U ON U.numUOMId = OI.numUOMId
                    LEFT JOIN Vendor V ON V.numVendorID = I.numVendorID
                                          AND I.numItemCode = V.numItemCode
            WHERE   OI.numOppId = @numOppID
                    AND OM.numDomainId = @numDomainId
            
        END
        IF @tintMode = 4 -- Add Edit Order items
        BEGIN 
            SELECT  OI.numoppitemtCode,
                    OI.numOppId,
                    OI.numItemCode,
					OI.bitItemPriceApprovalRequired,
                    OI.numUnitHour,
                    OI.monPrice,
                    OI.monTotAmount,
                    OI.numSourceID,
                    OI.vcItemDesc,
                    OI.numWarehouseItmsID,
                    OI.vcType ItemType,
                    OI.vcAttributes Attributes,
                    OI.bitDropShip DropShip,
                    OI.numUnitHourReceived,
                    OI.bitDiscountType,
                    OI.fltDiscount,
                    OI.monTotAmtBefDiscount,
                    OI.numQtyShipped,
                    OI.vcItemName,
                    OI.vcModelID,
                    OI.vcPathForTImage,
                    OI.vcManufacturer,
                    OI.monVendorCost,
                    ISNULL(OI.numUOMId, 0) AS numUOM,
                    ISNULL(U.vcUnitName, '') vcUOMName,
                    dbo.fn_UOMConversion(OI.numUOMId, OI.numItemCode,
                                         OM.numDomainId, NULL) AS UOMConversionFactor,
                    OI.bitWorkOrder,
                    isnull(OI.numVendorWareHouse,0) as numVendorWareHouse,
                    OI.numShipmentMethod,
                    OI.numSOVendorId,
                    OI.numProjectID,
                    OI.numClassID,
                    OI.numProjectStageID,
                    OI.numToWarehouseItemID,
                    ISNULL(I.bitTaxable, 0) bitTaxable,
                   UPPER(I.charItemType) charItemType,
                    WItems.numWarehouseID,
                    W.vcWareHouse AS Warehouse,
                    0 as Op_Flag,
					CAST(( dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), OI.numItemCode,
                                    OM.numDomainId, ISNULL(OI.numUOMId, 0))
						* OI.numUnitHour ) AS NUMERIC(18, 2)) AS numUnitHour,
					isnull(OI.numClassID,0) as numClassID,isnull(OI.numProjectID,0) as numProjectID,
					CASE WHEN (SELECT COUNT(*) FROM OpportunityBizDocs OB JOIN dbo.OpportunityBizDocItems OBI ON ob.numOppBizDocsId=obi.numOppBizDocID
					AND ob.bitAuthoritativeBizDocs=1 AND obi.numOppItemID=oi.numoppitemtCode AND ob.numOppId=OM.numOppId)>0 THEN 1 ELSE 0 END  AS bitIsAuthBizDoc,
					ISNULL(OI.numUnitHourReceived,0) AS numUnitHourReceived,ISNULL(numQtyShipped,0) AS numQtyShipped,dbo.fn_GetUOMName(I.numBaseUnit) AS vcBaseUOMName,ISNULL(I.fltWeight,0) AS [fltWeight],ISNULL(I.bitFreeShipping,0) AS [bitFreeShipping],
					ISNULL(I.fltLength,0) AS [fltLength],
					ISNULL(I.fltWidth,0) AS [fltWidth],
					ISNULL(I.fltHeight,0) AS [fltHeight],
					ISNULL(WItems.vcWHSKU,ISNULL(I.vcSKU,'')) AS [vcSKU],
					ISNULL(I.monAverageCost,0) AS monAverageCost,
					ISNULL(I.bitFreeShipping,0) AS [bitFreeShipping],
					ISNULL(I.txtItemDesc,'') txtItemDesc,
					ISNULL(I.vcModelID,'') vcModelID,
					ISNULL(I.vcManufacturer,'') vcManufacturer,
					ISNULL(I.numBarCodeId,'') numBarCodeId,
					ISNULL(I.bitSerialized,0) bitSerialized,
					ISNULL(I.bitKitParent,0) bitKitParent,
					ISNULL(I.bitAssembly,0) bitAssembly,
					ISNULL(I.IsArchieve,0) IsArchieve,
					ISNULL(I.bitLotNo,0) bitLotNo,
					ISNULL(WItems.monWListPrice,0) monWListPrice,
					ISNULL(I.bitTaxable, 0) bitTaxable,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numOnHand),0) AS VARCHAR(10)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numOnHand,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numOnOrder),0) AS VARCHAR(10)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numOnOrder,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numAllocation),0) AS VARCHAR(10)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numAllocation,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numBackOrder),0) AS VARCHAR(10)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numBackOrder,
					case when ISNULL(I.bitAssembly,0)=1 then dbo.fn_GetAssemblyPossibleWOQty(I.numItemCode) else 0 end AS numMaxWorkOrderQty,
					ISNULL((SELECT COUNT(*) FROM OppWarehouseSerializedItem WHERE numOppId=@numOppID AND numOppItemID=OI.numoppitemtCode),0) AS numSerialNoAssigned,
					(CASE WHEN (SELECT COUNT(*) FROM OpportunityBizDocs OB JOIN dbo.OpportunityBizDocItems OBI ON OB.numOppBizDocsId=OBI.numOppBizDocID WHERE OB.numOppId = OI.numOppId AND OBI.numOppItemID=OI.numoppitemtCode AND OB.numBizDocId=296) > 0 THEN 1 ELSE 0 END) AS bitAddedFulFillmentBizDoc
--					0 AS [Tax0],0 AS bitTaxable0
            FROM    dbo.OpportunityItems OI
                    INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OI.numOppId
                    INNER JOIN dbo.Item I ON I.numItemCode = OI.numItemCode
                    LEFT JOIN UOM U ON U.numUOMId = OI.numUOMId
                    LEFT JOIN WareHouseItems WItems ON WItems.numWareHouseItemID = OI.numWarehouseItmsID
                    LEFT JOIN Warehouses W ON W.numWarehouseID = WItems.numWarehouseID
            WHERE   OI.numOppId = @numOppID
                    AND OM.numDomainId = @numDomainId
                    
		SELECT  vcSerialNo,
				vcComments AS Comments,
				numOppItemID AS numoppitemtCode,
				W.numWarehouseItmsDTLID,
				numWarehouseItmsID AS numWItmsID,
				dbo.Fn_getattributes(W.numWarehouseItmsDTLID, 1) Attributes
		FROM    WareHouseItmsDTL W
				JOIN OppWarehouseSerializedItem O ON O.numWarehouseItmsDTLID = W.numWareHouseItmsDTLID
		WHERE   numOppID = @numOppID                
                   
        END
        
        
		IF @tintMode = 5 -- Get WebApi Order Items to enter WebApi Order line Item Details 
		BEGIN 
			SELECT OI.numOppItemtCode,OI.numOppId,OI.numItemCode,
			ISNULL((SELECT vcWHSKU FROM dbo.WareHouseItems WHERE WareHouseItems.numWarehouseItemID = OI.numWarehouseItmsID),ISNULL(IT.vcSKU,'')) AS [vcSKU],
			OI.vcItemDesc,OMAPI.vcAPIOppId
			FROM OpportunityItems OI 
			INNER JOIN dbo.OpportunityMaster OM ON OI.numOppId = OM.numOppId 
			INNER JOIN dbo.OpportunityMasterAPI OMAPI ON OM.numOppId = OMAPI.numOppId
			INNER JOIN dbo.Item IT ON OI.numItemCode = IT.numItemCode
			WHERE OI.numOppId = @numOppID and OM.numDomainID = @numDomainId 
        END

END
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetPackingDetail')
DROP PROCEDURE USP_GetPackingDetail
GO
CREATE PROCEDURE USP_GetPackingDetail
    @numDomainID AS NUMERIC(9),
	@vcBizDocsIds VARCHAR(MAX),
	@tintMode AS TINYINT 
AS
BEGIN
	-- PICK LIST
	IF @tintMode = 1
	BEGIN
		SELECT
			ISNULL(vcItemName,'') AS vcItemName,
			ISNULL((SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numDomainId=@numDomainID AND ItemImages.numItemCode=Item.numItemCode AND bitDefault=1 AND bitIsImage=1),'') AS vcImage,
			ISNULL(vcModelID,'') AS vcModelID,
			ISNULL(txtItemDesc,'') AS txtItemDesc,
			ISNULL(vcWareHouse,'') AS vcWareHouse,
			ISNULL(vcLocation,'') AS vcLocation,
			(CASE WHEN ISNULL(Item.numItemGroup,0) > 0 THEN WareHouseItems.vcWHSKU ELSE Item.vcSKU END) AS vcSKU,
			(CASE WHEN ISNULL(Item.numItemGroup,0) > 0 THEN WareHouseItems.vcBarCode ELSE Item.numBarCodeId END) AS vcUPC,
			CAST(SUM(numUnitHour) AS INT) AS numUnitHour
		FROM
			OpportunityBizDocItems
		INNER JOIN
			WareHouseItems
		ON
			OpportunityBizDocItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
		INNER JOIN
			Warehouses
		ON
			WareHouseItems.numWareHouseID = Warehouses.numWareHouseID
		INNER JOIN
			Item
		ON
			OpportunityBizDocItems.numItemCode = Item.numItemCode
		WHERE
			numOppBizDocID IN ( SELECT * FROM dbo.splitIds(@vcBizDocsIds,','))
		GROUP BY
			Item.numItemCode,
			vcItemName,
			vcModelID,
			txtItemDesc,
			vcWareHouse,
			vcLocation,
			(CASE WHEN ISNULL(Item.numItemGroup,0) > 0 THEN WareHouseItems.vcWHSKU ELSE Item.vcSKU END),
			(CASE WHEN ISNULL(Item.numItemGroup,0) > 0 THEN WareHouseItems.vcBarCode ELSE Item.numBarCodeId END)
		ORDER BY
			vcWareHouse ASC,
			vcLocation ASC,
			vcItemName ASC
	END
	-- GROUP LIST
	ELSE IF @tintMode = 2
	BEGIN
		SELECT
			ISNULL(vcItemName,'') AS vcItemName,
			ISNULL((SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numDomainId=@numDomainID AND ItemImages.numItemCode=Item.numItemCode AND bitDefault=1 AND bitIsImage=1),'') AS vcImage,
			ISNULL(vcModelID,'') AS vcModelID,
			ISNULL(txtItemDesc,'') AS txtItemDesc,
			OpportunityBizDocs.numOppBizDocsId,
			(CASE WHEN ISNULL(Item.numItemGroup,0) > 0 THEN WareHouseItems.vcWHSKU ELSE Item.vcSKU END) AS vcSKU,
			CAST(numUnitHour AS INT) AS numUnitHour
		FROM
			OpportunityBizDocs
		INNER JOIN
			OpportunityBizDocItems
		ON
			OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
		INNER JOIN
			WareHouseItems
		ON
			OpportunityBizDocItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
		INNER JOIN
			Warehouses
		ON
			WareHouseItems.numWareHouseID = Warehouses.numWareHouseID
		INNER JOIN
			Item
		ON
			OpportunityBizDocItems.numItemCode = Item.numItemCode
		WHERE
			numOppBizDocID IN ( SELECT * FROM dbo.splitIds(@vcBizDocsIds,','))
		ORDER BY
			vcWareHouse ASC,
			vcLocation ASC,
			vcItemName ASC
	END
END
--Created by anoop jayaraj

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetPOItemsForFulfillment')
DROP PROCEDURE USP_GetPOItemsForFulfillment
GO
CREATE PROCEDURE [dbo].[USP_GetPOItemsForFulfillment]
@numDomainID as numeric(9),
@numUserCntID numeric(9)=0,                                                                                                               
@SortChar char(1)='0',                                                            
 @CurrentPage int,                                                              
 @PageSize int,                                                              
 @TotRecs int output,                                                              
 @columnName as Varchar(50),                                                              
 @columnSortOrder as Varchar(10),
 @Filter AS VARCHAR(30),
 @FilterBy AS int
as


--Create a Temporary table to hold data                                                              
Create table #tempTable ( ID INT IDENTITY PRIMARY KEY, 
    numoppitemtCode numeric(9),
    numOppID  numeric(9),
     vcPOppName varchar(200),
    vcItemName varchar(500),
    vcModelID varchar(200),
    numUnitHour numeric(9),
    numUnitHourReceived numeric(9),
    numOnHand numeric(9),
	numOnOrder  numeric(9),
	numAllocation  numeric(9),
	numBackOrder  numeric(9),
	vcWarehouse varchar(200),
	dtDueDate VARCHAR(25),bitSerialized BIT,numWarehouseItmsID NUMERIC(9),bitLotNo BIT,SerialLotNo VARCHAR(1000),
	fltExchangeRate float,numCurrencyID numeric(9),numDivisionID numeric(9),
	itemIncomeAccount numeric(9),itemInventoryAsset numeric(9),itemCoGs numeric(9),
	DropShip BIT,charItemType CHAR(1),ItemType VARCHAR(50),
	numProjectID numeric(9),numClassID numeric(9),numItemCode numeric(9),monPrice MONEY,bitPPVariance bit,Vendor varchar(max))
	
declare @strSql as varchar(8000)                                                              
set @strSql='Select numoppitemtCode,Opp.numOppID,vcPOppName,OI.vcItemName + '' - ''+ isnull(dbo.fn_GetAttributes(OI.numWarehouseItmsID,0),'''') as vcItemName ,OI.vcModelID,numUnitHour,isnull(numUnitHourReceived,0) as numUnitHourReceived,
isnull(numOnHand,0) as numOnHand,isnull(numOnOrder,0) as numOnOrder,isnull(numAllocation,0) as numAllocation,isnull(numBackOrder,0) as numBackOrder,vcWarehouse + '': '' + isnull(WL.vcLocation,'''') as  vcWarehouse,
[dbo].[FormatedDateFromDate](intPEstimatedCloseDate,'+convert(varchar(20),@numDomainID)+') AS dtDueDate,isnull(I.bitSerialized,0) as bitSerialized,OI.numWarehouseItmsID,isnull(I.bitLotNo,0) as bitLotNo
,SUBSTRING(
(SELECT '','' + vcSerialNo + CASE WHEN isnull(I.bitLotNo,0)=1 THEN '' ('' + CONVERT(VARCHAR(15),whi.numQty) + '')'' ELSE '''' end 
FROM OppWarehouseSerializedItem oppI JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID=whi.numWareHouseItmsDTLID where oppI.numOppID=OI.numOppId and oppI.numOppItemID=OI.numoppitemtCode 
ORDER BY vcSerialNo FOR XML PATH('''')),2,200000) AS SerialLotNo,
ISNULL(Opp.fltExchangeRate,0) AS fltExchangeRate,ISNULL(Opp.numCurrencyID,0) AS numCurrencyID,ISNULL(Opp.numDivisionID,0) AS numDivisionID,
isnull(I.numIncomeChartAcntId,0) as itemIncomeAccount,isnull(I.numAssetChartAcntId,0) as itemInventoryAsset,isnull(I.numCOGsChartAcntId,0) as itemCoGs,
isnull(OI.bitDropShip,0) as DropShip,I.charItemType,OI.vcType ItemType,ISNULL(OI.numProjectID, 0) numProjectID,ISNULL(OI.numClassID, 0) numClassID,
OI.numItemCode,OI.monPrice,isnull(Opp.bitPPVariance,0) as bitPPVariance,
ISNULL(com.vcCompanyName,''-'') as Vendor

from OpportunityMaster Opp
Join OpportunityItems OI On OI.numOppId=Opp.numOppId
Join Item I on I.numItemCode=OI.numItemCode
Left Join WareHouseItems WI on OI.numWarehouseItmsID=WI.numWareHouseItemID
Left Join Warehouses W on W.numWarehouseID=WI.numWarehouseID
LEFT join divisionmaster div on Opp.numDivisionId=div.numDivisionId 
LEFT join companyInfo com  on com.numCompanyid=div.numcompanyID
left join WarehouseLocation WL on WL.numWLocationID =WI.numWLocationID
where tintOppType=2 and tintOppstatus=1 and ISNULL(bitStockTransfer,0)=0 and tintShipped=0 and numUnitHour-isnull(numUnitHourReceived,0)>0  
AND I.[charItemType] IN (''P'',''N'',''S'') and (OI.bitDropShip=0 or OI.bitDropShip is null) and Opp.numDomainID='+convert(varchar(20),@numDomainID) 

--OI.[numWarehouseItmsID] IS NOT NULL
if @SortChar<>'0' set @strSql=@strSql + ' And Opp.vcPOppName like '''+@SortChar+'%'''                                                      

IF @FilterBy=1 --Item
	set	@strSql=@strSql + ' and OI.vcItemName like ''%' + @Filter + '%'''
ELSE IF @FilterBy=2 --Warehouse
	set	@strSql=@strSql + ' and W.vcWareHouse like ''%' + @Filter + '%'''
ELSE IF @FilterBy=3 --Purchase Order
	set	@strSql=@strSql + ' and Opp.vcPOppName like ''%' + @Filter + '%'''
ELSE IF @FilterBy=4 --Vendor
	set	@strSql=@strSql + ' and com.vcCompanyName LIKE ''%' + @Filter + '%'''		
ELSE IF @FilterBy=5 --BizDoc
	set	@strSql=@strSql + ' and Opp.numOppID in (SELECT numOppId FROM OpportunityBizDocs WHERE vcBizDocID like ''%' + @Filter + '%'')'

If @columnName = 'vcModelID' 
	set @columnName='OI.vcModelID'
								
set @strSql=@strSql + ' ORDER BY  ' + @columnName +' '+ @columnSortOrder

PRINT @strSql

insert into #tempTable(numoppitemtCode,
    numOppID ,
    vcPOppName,
    vcItemName,
    vcModelID,
    numUnitHour,
    numUnitHourReceived,
    numOnHand,
	numOnOrder,
	numAllocation,
	numBackOrder,
	vcWarehouse,
	dtDueDate,bitSerialized,numWarehouseItmsID,bitLotNo,SerialLotNo,fltExchangeRate,numCurrencyID,numDivisionID,
	itemIncomeAccount,itemInventoryAsset,itemCoGs,DropShip,charItemType,ItemType,numProjectID,numClassID,numItemCode,monPrice,bitPPVariance,Vendor)                                                              
exec (@strSql) 


declare @firstRec as integer                                                
declare @lastRec as integer                                                              
set @firstRec= (@CurrentPage-1) * @PageSize                                                              
set @lastRec= (@CurrentPage*@PageSize+1)                                           
set @TotRecs=(select count(*) from #tempTable) 


select *  from #tempTable where ID>@firstRec and ID < @lastRec

drop table #tempTable
/****** Object:  StoredProcedure [dbo].[usp_GetSalesFulfillmentOrder]    Script Date: 03/06/2009 00:39:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetSalesFulfillmentOrder')
DROP PROCEDURE USP_GetSalesFulfillmentOrder
GO
CREATE PROCEDURE [dbo].[USP_GetSalesFulfillmentOrder]
    (
      @numDomainId AS NUMERIC(9) = 0,
      @SortCol VARCHAR(50),
      @SortDirection VARCHAR(4),
      @CurrentPage INT,
      @PageSize INT,
      @TotRecs INT OUTPUT,
      @numUserCntID NUMERIC,
      @vcBizDocStatus VARCHAR(500),
      @vcBizDocType varchar(500),
      @numDivisionID as numeric(9)=0,
      @ClientTimeZoneOffset INT=0,
      @bitIncludeHistory bit=0,
      @vcOrderStatus VARCHAR(500),
      @vcOrderSource VARCHAR(1000),
	  @bitFulfilled BIT
     )
AS 
    BEGIN

        DECLARE @firstRec AS INTEGER                                                              
        DECLARE @lastRec AS INTEGER                                                     
        SET @firstRec = ( @CurrentPage - 1 ) * @PageSize                             
        SET @lastRec = ( @CurrentPage * @PageSize + 1 )                                                                    

	  IF LEN(ISNULL(@SortCol,''))=0
		SET  @SortCol = 'dtCreatedDate'
		
	  IF LEN(ISNULL(@SortDirection,''))=0	
		SET @SortDirection = 'desc'
      
        DECLARE @strSql NVARCHAR(4000)
        DECLARE @SELECT NVARCHAR(4000)
        DECLARE @FROM NVARCHAR(4000)
        DECLARE @WHERE NVARCHAR(4000)
		
		DECLARE @numShippingServiceItemID AS NUMERIC
		SELECT @numShippingServiceItemID=ISNULL(numShippingServiceItemID,0) FROM domain WHERE numDomainID=@numDomainID
		
	CREATE TABLE #temp (row INT,vcpoppname VARCHAR(100),numoppid NUMERIC,vcOrderStatus VARCHAR(100),
		numoppbizdocsid NUMERIC,numBizDocId NUMERIC,vcbizdocid VARCHAR(100),vcBizDocType VARCHAR(100),
		numbizdocstatus NUMERIC,vcbizdocstatus VARCHAR(100),dtFromDate VARCHAR(30),numDivisionID NUMERIC,
		vcCompanyName VARCHAR(100),tintCRMType TINYINT)
		
        SET @SELECT = 'WITH bizdocs
         AS (SELECT Row_number() OVER(ORDER BY ' + @SortCol + ' '
            + @SortDirection
            + ') AS row,
			om.vcpoppname,
			om.numoppid,dbo.fn_GetListItemName(om.[numstatus]) vcOrderStatus,
			obd.[numoppbizdocsid],OBD.numBizDocId,
			obd.[vcbizdocid],dbo.fn_GetListItemName(OBD.numBizDocId) AS vcBizDocType,
			obd.[numbizdocstatus],
			dbo.fn_GetListItemName(obd.[numbizdocstatus]) vcbizdocstatus,
			[dbo].[FormatedDateFromDate](dtFromDate,'+ CONVERT(VARCHAR(15), @numDomainId) + ') dtFromDate,
			om.numDivisionID,
			vcCompanyName,DM.tintCRMType'
			
		SET @FROM =' FROM opportunitymaster om INNER JOIN [opportunitybizdocs] obd ON om.[numoppid] = obd.[numoppid]
                     INNER JOIN DivisionMaster DM ON om.numDivisionId = DM.numDivisionID
					 Inner join CompanyInfo cmp on cmp.numCompanyID=DM.numCompanyID'
					
        SET @WHERE = ' WHERE om.tintopptype = 1 and om.tintOppStatus=1 AND ISNULL(OBD.bitFulfilled,0)=' + CAST(@bitFulfilled AS VARCHAR) + ' AND OBD.numBizDocId=296 AND om.numDomainID = '+ CONVERT(VARCHAR(15), @numDomainId)
		
		IF  LEN(ISNULL(@vcBizDocStatus,''))>0
		BEGIN
			IF @bitIncludeHistory=1
				SET @WHERE = @WHERE + ' and (select count(*) from OppFulfillmentBizDocsStatusHistory WHERE numOppId=om.numOppId and numOppBizDocsId=obd.numOppBizDocsId and isnull(numbizdocstatus,0) in (SELECT Items FROM dbo.Split(''' +@vcBizDocStatus + ''','','')))>0' 			
			ELSE	
				SET @WHERE = @WHERE + ' and obd.numbizdocstatus in (SELECT Items FROM dbo.Split(''' +@vcBizDocStatus + ''','',''))' 			
		END
		
		IF  LEN(ISNULL(@vcOrderStatus,''))>0
		BEGIN
				SET @WHERE = @WHERE + ' and isnull(om.numStatus,0) in (SELECT Items FROM dbo.Split(''' +@vcOrderStatus + ''','',''))' 			
		END
		
		IF LEN(ISNULL(@vcOrderSource,''))>0
		BEGIN
				SET @WHERE = @WHERE + ' and (' + @vcOrderSource +')' 			
		END
		
		IF  LEN(ISNULL(@vcBizDocType,''))>0
		BEGIN
			SET @WHERE = @WHERE + ' and isnull(OBD.numBizDocId,0) in (SELECT Items FROM dbo.Split(''' +@vcBizDocType + ''','',''))' 			
		END
		
		SET @WHERE = @WHERE + ' AND (om.numDivisionID ='+ CONVERT(VARCHAR(15), @numDivisionID) + ' OR '+ CONVERT(VARCHAR(15), @numDivisionID) + ' = 0) '
		
        SET @strSql = @SELECT + @FROM +@WHERE + ') insert into #temp SELECT * FROM [bizdocs] WHERE row > ' + CONVERT(VARCHAR(10), @firstRec) + ' and row <' + CONVERT(VARCHAR(10), @lastRec)
        
        PRINT @strSql ;
        EXEC ( @strSql ) ;
       
       SELECT OM.*,ISNULL((SELECT SUBSTRING((SELECT '$^$' + dbo.fn_GetListItemName(numBizDocStatus) +'#^#'+ dbo.fn_GetContactName(numUserCntID) +'#^#'+ dbo.FormatedDateTimeFromDate(DateAdd(minute,-@ClientTimeZoneOffset, dtCreatedDate),@numDomainId) + '#^#'+ isnull(DFCS.vcColorScheme,'NoForeColor')
	FROM OppFulfillmentBizDocsStatusHistory OFBS left join DycFieldColorScheme DFCS on OFBS.numBizDocStatus=DFCS.vcFieldValue AND DFCS.numDomainID=@numDomainId WHERE numOppId=om.numOppId and numOppBizDocsId=OM.numOppBizDocsId FOR XML PATH('')),4,2000)),'') as vcBizDocsStatusList,
	isnull((select max(numShippingReportId) from ShippingReport SR where /*SR.numOppID=om.numOppID and*/ SR.numOppBizDocId=OM.numoppbizdocsid AND SR.numDomainID = @numDomainID),0) AS numShippingReportId,
	ISNULL((SELECT max(SB.numShippingReportId) FROM [ShippingBox] SB JOIN dbo.ShippingReport SR ON SB.numShippingReportId = SR.numShippingReportId where /*SR.numOppID=om.numOppID and*/ SR.numOppBizDocId=OM.numoppbizdocsid AND SR.numDomainID = @numDomainID AND LEN(ISNULL(SB.vcTrackingNumber,''))>0),0) AS numShippingLabelReportId,
	CAST((Case WHEN ISNULL(OI.numoppitemtCode,0)>0 then 1 else 0 end) as bit) as IsShippingServiceItemID,ISNULL(vcItemDesc,'' ) AS vcItemDesc,ISNULL(monTotAmount,0) AS monTotAmount
	FROM #temp OM 
		LEFT JOIN (SELECT numOppId,numoppitemtCode,ISNULL(vcItemDesc, '') vcItemDesc,monTotAmount,
		Row_number() OVER(PARTITION BY numOppId ORDER BY numoppitemtCode) AS row 
		FROM OpportunityItems OI WHERE OI.numOppId in (SELECT numOppId FROM #temp) AND numItemCode =@numShippingServiceItemID) 
		OI ON OI.row=1 AND OM.numOppId = OI.numOppId
       
       
       SELECT opp.numOppId,OBD.numOppBizDocID,  Opp.vcitemname AS vcItemName,
				ISNULL((SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numDomainId=@numDomainID AND ItemImages.numItemCode=i.numItemCode AND bitDefault=1 AND bitIsImage=1),'') AS vcImage,
                        CASE WHEN charitemType = 'P' THEN 'Product'
                             WHEN charitemType = 'S' THEN 'Service'
                        END AS charitemType,
                        CASE WHEN charitemType = 'P' THEN 'Inventory'
	                         WHEN charitemType = 'N' THEN 'Non-Inventory'
                             WHEN charitemType = 'S' THEN 'Service'
                        END AS vcItemType, CASE WHEN ISNULL(numItemGroup,0) > 0 THEN WI.vcWHSKU
                             ELSE i.vcSKU
                        END vcSKU,  
                        dbo.USP_GetAttributes(OBD.numWarehouseItmsID,bitSerialized) AS vcAttributes,
                        ISNULL(W.vcWareHouse, '') AS Warehouse,WL.vcLocation,OBD.numUnitHour AS numUnitHourOrig,
                        Opp.bitDropShip AS DropShip,SUBSTRING(( SELECT  ' ,' + vcSerialNo
                                            + CASE WHEN ISNULL(I.bitLotNo, 0) = 1
                                                   THEN '('
                                                        + CONVERT(VARCHAR(15), oppI.numQty)
                                                        + ')'
                                                   ELSE ''
                                              END
                                    FROM    OppWarehouseSerializedItem oppI
                                            JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID = whi.numWareHouseItmsDTLID
                                    WHERE   oppI.numOppID = Opp.numOppId
                                            AND oppI.numOppItemID = Opp.numoppitemtCode
                                    ORDER BY vcSerialNo
                                  FOR
                                    XML PATH('')
                                  ), 3, 200000) AS SerialLotNo,ISNULL(bitSerialized,0) AS bitSerialized,ISNULL(bitLotNo,0) AS bitLotNo,Opp.numWarehouseItmsID,opp.numoppitemtCode
              FROM      OpportunityItems opp 
                        JOIN OpportunityBizDocItems OBD ON OBD.numOppItemID = opp.numoppitemtCode
                        join #temp OM on opp.numoppid=OM.numoppid and OM.numOppBizDocsId=OBD.numOppBizDocID
                        LEFT JOIN item i ON opp.numItemCode = i.numItemCode
                        LEFT JOIN dbo.WareHouseItems WI ON opp.numWarehouseItmsID = WI.numWareHouseItemID
                                                           AND WI.numDomainID = @numDomainId
                        LEFT JOIN dbo.Warehouses W ON WI.numDomainID = W.numDomainID
                                                      AND WI.numWareHouseID = W.numWareHouseID
						LEFT JOIN dbo.WarehouseLocation WL ON WL.numWLocationID= WI.numWLocationID
              
        
        DROP TABLE #temp 
        
        /*Get total count */
        SET @strSql =  ' (SELECT @TotRecs =COUNT(*) ' + @FROM + @WHERE + ')';
        
        PRINT @strSql ;
        EXEC sp_executesql 
        @query = @strSql, 
        @params = N'@TotRecs INT OUTPUT', 
        @TotRecs = @TotRecs OUTPUT 
        
    END
  
--created by anoop jayaraj
--	EXEC USP_GEtSFItemsForImporting 72,'76,318,538,758,772,1061,11409,11410,11628,11629,17089,18151,18193,18195,18196,18197',1
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getsfitemsforimporting')
DROP PROCEDURE usp_getsfitemsforimporting
GO
CREATE PROCEDURE USP_GEtSFItemsForImporting
    @numDomainID AS NUMERIC(9),
	@numBizDocId	AS BIGINT,
	@vcBizDocsIds		VARCHAR(MAX),
	@tintMode AS TINYINT 
AS
BEGIN
---------------------------------------------------------------------------------
    DECLARE  @strSql  AS VARCHAR(8000)
  DECLARE  @strFrom  AS VARCHAR(2000)

IF @tintMode=1 --PrintPickList
BEGIN
   SELECT
		ISNULL(vcItemName,'') AS vcItemName,
		ISNULL((SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numDomainId=187 AND bitDefault=1 AND bitIsImage=1),'') AS vcImage,
		ISNULL(vcModelID,'') AS vcModelID,
		ISNULL(txtItemDesc,'') AS txtItemDesc,
		ISNULL(vcWareHouse,'') AS vcWareHouse,
		ISNULL(vcLocation,'') AS vcLocation,
		(CASE WHEN ISNULL(Item.numItemGroup,0) > 0 THEN WareHouseItems.vcWHSKU ELSE Item.vcSKU END) AS vcSKU,
		(CASE WHEN ISNULL(Item.numItemGroup,0) > 0 THEN WareHouseItems.vcBarCode ELSE Item.numBarCodeId END) AS vcUPC,
		SUM(numUnitHour) AS numUnitHour
	FROM
		OpportunityBizDocItems
	INNER JOIN
		WareHouseItems
	ON
		OpportunityBizDocItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
	INNER JOIN
		Warehouses
	ON
		WareHouseItems.numWareHouseID = Warehouses.numWareHouseID
	INNER JOIN
		Item
	ON
		OpportunityBizDocItems.numItemCode = Item.numItemCode
	WHERE
		numOppBizDocID IN ( SELECT * FROM dbo.splitIds(@vcBizDocsIds,','))
	GROUP BY
		vcItemName,
		vcModelID,
		txtItemDesc,
		vcWareHouse,
		vcLocation,
		(CASE WHEN ISNULL(Item.numItemGroup,0) > 0 THEN WareHouseItems.vcWHSKU ELSE Item.vcSKU END),
		(CASE WHEN ISNULL(Item.numItemGroup,0) > 0 THEN WareHouseItems.vcBarCode ELSE Item.numBarCodeId END)
	ORDER BY
		vcWareHouse ASC,
		vcLocation ASC,
		vcItemName ASC
END
	
--		SELECT  DISTINCT I.vcItemName AS Item,
--						 I.[vcModelID] AS Model,
--						 0 AS [Qty To Pick],
--						 ISNULL(W.vcWareHouse, '') + ',' + ISNULL(WI.vcLocation, '') AS [Warehouse, Location],
--						 vcWHSKU AS [SKU] ,
--						 vcBarCode AS [UPC],
--						 vcWStreet AS [Street],
--						 vcWCity AS [City],
--						 vcWPinCode AS [PinCode],
--						 (SELECT vcState FROM State WHERE numStateID = W.numWState) AS [State],
--						 dbo.fn_GetListItemName(W.numWCountry) AS [Country],
--						 WI.numWareHouseItemID,
--						 SUBSTRING((SELECT ',' + vcSerialNo + CASE 
--																	WHEN isnull(I.bitLotNo,0)=1 
--																	THEN ' (' + CONVERT(VARCHAR(15),oppI.numQty) + ')' 
--																	ELSE '' 
--															  END 
--									FROM OppWarehouseSerializedItem oppI 
--									JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID = whi.numWareHouseItmsDTLID 
--									WHERE oppI.numOppID = OI.numOppId 
--									  AND oppI.numOppItemID = OI.numoppitemtCode 
--									ORDER BY vcSerialNo FOR XML PATH('')),2,200000) AS [Serial #]
--		FROM    OpportunityItems AS OI
--				INNER JOIN Item AS I ON OI.numItemCode = I.numItemCode
--				LEFT OUTER JOIN WareHouseItems AS WI ON I.numItemCode = WI.numItemID
--				LEFT OUTER JOIN Warehouses AS W ON WI.numWareHouseID = W.numWareHouseID
--				LEFT OUTER JOIN OpportunityBizDocItems AS BI ON I.numItemCode = BI.numItemCode
--																AND OI.numoppitemtCode = BI.numOppItemID
--																AND WI.numWareHouseItemID = BI.numWarehouseItmsID
--		WHERE   I.charItemType = 'P'
--				AND I.numDomainID = @numDomainID
--				AND OI.[numoppitemtCode] IN ( SELECT * FROM dbo.splitIds(@strOppItemsIDs, ',') )
--		GROUP BY I.[numItemCode],
--				 I.[vcItemName],
--				 BI.[numUnitHour],
--				 OI.[numWarehouseItmsID],
--				 W.[vcWareHouse],
--				 WI.[vcLocation],
--				 I.[vcModelID],
--				 WI.numWareHouseItemID,
--				 W.numWState,
--				 W.numWCountry,
--				 vcWareHouse,
--				 vcWStreet,
--				 vcWCity,
--				 vcWPinCode,
--				 vcWHSKU,
--				 vcBarCode,
--				 OI.numOppId,
--				 OI.numoppitemtCode,
--				 bitLotNo
--		ORDER BY I.[vcItemName]			
----------------------------------------------------------
ELSE IF @tintMode=2 --PrintPackingSlip
BEGIN
		  
		SELECT	
				ROW_NUMBER() OVER(ORDER BY OM.numDomainID ASC) AS SRNO,
				OM.numOppId,
				OM.numDomainID,
				OM.tintTaxOperator,
				I.numItemCode,
				I.vcModelID,
				OM.vcPOppName,
				cast((OBDI.monTotAmtBefDiscount - OBDI.monTotAMount) as varchar(20)) + Case When isnull(OM.fltDiscount,0)> 0 then  '(' + cast(OM.fltDiscount as varchar(10)) + '%)' else '' end AS DiscAmt,
				OBDI.dtRentalStartDate,
				OBDI.dtRentalReturnDate,
				OBDI.monShipCost,
				OBDI.vcShippingMethod,
				OBDI.dtDeliveryDate,
				numItemClassification,
				vcNotes,
				OBD.vcTrackingNo,
				I.vcManufacturer,
				--vcItemClassification,
				CASE 
					WHEN charItemType='P' 
					THEN 'Inventory Item' 
					WHEN charItemType='S' 
					THEN 'Service' 
					WHEN charItemType='A' 
					THEN 'Accessory' 
					WHEN charItemType='N' 
					THEN 'Non-Inventory Item' 
				END AS charItemType,charItemType AS [Type],
				OBDI.vcAttributes,
				OI.[numoppitemtCode] ,
				OI.vcItemName,
				OBDI.numUnitHour,
				OI.[numQtyShipped],
				OBDI.vcItemDesc AS txtItemDesc,
				vcUnitofMeasure AS numUOMId,
				monListPrice AS [Price],
				CONVERT(VARCHAR, CONVERT(DECIMAL(18, 4), OBDI.monTotAmount)) Amount,
				dbo.fn_GetListItemName(OBD.numShipVia) AS ShipVai,
				OBDI.monPrice,
				OBDI.monTotAmount,
				vcWareHouse,
				vcLocation,
				vcWStreet,
				vcWCity,
				vcWPinCode,
				(SELECT vcState FROM State WHERE numStateID = W.numWState) AS [vcState],
				dbo.fn_GetListItemName(W.numWCountry) AS [vcWCountry],							
				vcWHSKU,
				vcBarCode,
				SUBSTRING((SELECT ',' + vcSerialNo + CASE 
															WHEN isnull(I.bitLotNo,0)=1 
															THEN ' (' + CONVERT(VARCHAR(15),SERIALIZED_ITEM.numQty) + ')' 
															ELSE '' 
													 END 
							FROM OppWarehouseSerializedItem SERIALIZED_ITEM 
							JOIN WareHouseItmsDTL W_ITEM_DETAIL ON SERIALIZED_ITEM.numWareHouseItmsDTLID = W_ITEM_DETAIL.numWareHouseItmsDTLID 
							WHERE SERIALIZED_ITEM.numOppID = OI.numOppId 
							  AND SERIALIZED_ITEM.numOppItemID = OI.numoppitemtCode 
							  and SERIALIZED_ITEM.numOppBizDocsID=OBD.numOppBizDocsId
							ORDER BY vcSerialNo 
						  FOR XML PATH('')),2,200000) AS SerialLotNo,
				isnull(OBD.vcRefOrderNo,'') AS vcRefOrderNo,
				isnull(OM.bitBillingTerms,0) AS tintBillingTerms,
				isnull(intBillingDays,0) AS numBillingDays,
--				CASE 
--					WHEN ISNUMERIC(ISNULL(dbo.fn_GetListItemName(ISNULL(intBillingDays,0)),0)) = 1 
--					THEN ISNULL(dbo.fn_GetListItemName(ISNULL(intBillingDays,0)),0) 
--					ELSE 0 
--				END AS numBillingDaysName,
				CASE WHEN ISNUMERIC((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0))) = 1
					 THEN (SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0))
					 ELSE 0
				END AS numBillingDaysName,	 
				ISNULL(bitInterestType,0) AS tintInterestType,
				tintOPPType,
				dbo.fn_GetContactName(numApprovedBy) AS ApprovedBy,
				dtApprovedDate,
				OM.bintAccountClosingDate,
				tintShipToType,
				tintBillToType,
				tintshipped,
				dtShippedDate bintShippedDate,
				OM.numDivisionID,							
				ISNULL(numShipVia,0) AS numShipVia,
				ISNULL(vcTrackingURL,'') AS vcTrackingURL,
				CASE 
				    WHEN numShipVia IS NULL 
				    THEN '-'
				    ELSE dbo.fn_GetListItemName(numShipVia)
				END AS ShipVia,
				ISNULL(C.varCurrSymbol,'') varCurrSymbol,
				isnull(bitPartialFulfilment,0) bitPartialFulfilment,
				dbo.[fn_GetContactName](OM.[numRecOwner])  AS OrderRecOwner,
				dbo.[fn_GetContactName](DM.[numRecOwner]) AS AccountRecOwner,
				dbo.fn_GetContactName(OM.numAssignedTo) AS AssigneeName,
				ISNULL((SELECT vcEmail FROM dbo.AdditionalContactsInformation WHERE numContactID = OM.numAssignedTo),'') AS AssigneeEmail,
				ISNULL((SELECT numPhone FROM dbo.AdditionalContactsInformation WHERE numContactID = OM.numAssignedTo),'') AS AssigneePhone,
				dbo.fn_GetComapnyName(OM.numDivisionId) OrganizationName,
				DM.vcComPhone as OrganizationPhone,
				dbo.fn_GetContactName(OM.numContactID)  OrgContactName,
				dbo.getCompanyAddress(OM.numDivisionId,1,OM.numDomainId) CompanyBillingAddress,
				CASE 
					 WHEN ACI.numPhone<>'' 
					 THEN ACI.numPhone + CASE 
										    WHEN ACI.numPhoneExtension<>'' 
											THEN ' - ' + ACI.numPhoneExtension 
											ELSE '' 
										 END  
					 ELSE '' 
				END OrgContactPhone,
				ISNULL(ACI.vcEmail,'') AS OrgContactEmail,
				dbo.[fn_GetContactName](OM.[numRecOwner]) AS OnlyOrderRecOwner,
				(SELECT TOP 1 SD.vcSignatureFile FROM SignatureDetail SD 
												 JOIN OpportunityBizDocsDetails OBD ON SD.numSignID = OBD.numSignID 
												WHERE SD.numDomainID = @numDomainID 
												  AND OBD.numDomainID = @numDomainID 
												  AND OBD.numSignID IS not null 
				ORDER BY OBD.numBizDocsPaymentDetId DESC) AS vcSignatureFile,
				ISNULL(CMP.txtComments,'') AS vcOrganizationComments,
				OBD.vcRefOrderNo AS [P.O.No],
				OM.txtComments AS [Comments],
				D.vcBizDocImagePath  
	INTO #temp_Packing_List
	FROM OpportunityMaster AS OM
	join OpportunityBizDocs OBD on OM.numOppID=OBD.numOppID
	JOIN OpportunityBizDocItems OBDI on OBDI.numOppBizDocID=OBD.numOppBizDocsId
	JOIN OpportunityItems AS OI on OI.numoppitemtCode=OBDI.numOppItemID and OI.numoppID=OI.numOppID
	JOIN Item AS I ON OI.numItemCode = I.numItemCode AND I.numDomainID = @numDomainID
	LEFT OUTER JOIN WareHouseItems WI ON I.numItemCode = WI.numItemID AND WI.numWareHouseItemID = OBDI.numWarehouseItmsID
	LEFT OUTER JOIN Warehouses W ON WI.numWareHouseID = W.numWareHouseID 
	JOIN [DivisionMaster] DM ON DM.numDivisionID = OM.numDivisionID   
	JOIN CompanyInfo CMP ON DM.numCompanyId = CMP.numCompanyId and CMP.numDomainID = @numDomainID  
	JOIN dbo.AdditionalContactsInformation ACI ON ACI.numContactID = OM.numContactID
	JOIN Domain D ON D.numDomainID = OM.numDomainID
	LEFT OUTER JOIN [Currency] C ON C.numCurrencyID = OM.numCurrencyID
    WHERE OM.numDomainID = @numDomainID 
	  AND OBD.[numOppBizDocsId] IN ( SELECT * FROM dbo.splitIds(@vcBizDocsIds ,',')) ORDER BY OM.numOppId
	
	DECLARE @numFldID AS INT    
	DECLARE @intRowNum AS INT    	
	DECLARE @vcFldname AS VARCHAR(MAX)	
	
	SET @strSQL = ''
	SELECT TOP 1 @numFldID = numFormfieldID,
				 @vcFldname = Fld_label,
				 @intRowNum = (intRowNum+1) 
	FROM DynamicFormConfigurationDetails DTL                                                                                                
	JOIN CFW_Fld_Master FIELD_MASTER ON FIELD_MASTER.Fld_id = DTL.numFormFieldID                                                                                                 
	WHERE DTL.numFormID = 7 
	  AND FIELD_MASTER.Grp_id = 5 
	  AND DTL.numDomainID = @numDomainID 
	  AND numAuthGroupID = @numBizDocId
	  AND vcFieldType = 'C' 
	ORDER BY intRowNum

	/**** START: Added Sales Tax Column ****/

	DECLARE @strSQLUpdate AS VARCHAR(2000);
	
	EXEC ( 'ALTER TABLE #temp_Packing_List ADD [TotalTax] MONEY'  )


	DECLARE @i AS INT = 1
	DECLARE @Count AS INT 
	SELECT @Count = COUNT(*) FROM #temp_Packing_List

	DECLARE @numOppId AS BIGINT
	DECLARE @numOppItemCode AS BIGINT
	DECLARE @tintTaxOperator AS INT

	WHILE @i <= @Count
	BEGIN
		SET @strSQLUpdate=''

		SELECT 
			@numDomainID = numDomainID,
			@numOppId = numOppId,
			@tintTaxOperator = tintTaxOperator,
			@numOppItemCode = numoppitemtCode
		FROM
			#temp_Packing_List
		WHERE
			SRNO = @i


		IF @tintTaxOperator = 1 
			 SET @strSQLUpdate = @strSQLUpdate
				+ ',[TotalTax]= dbo.fn_CalBizDocTaxAmt('
				+ CONVERT(VARCHAR(20), @numDomainID) + ',0,'
				+ CONVERT(VARCHAR(20), @numOppId) + ','
				+ CONVERT(VARCHAR(20), @numOppItemCode) + ',1)*Amount/100'
		ELSE IF @tintTaxOperator = 2 -- remove sales tax
			SET @strSQLUpdate = @strSQLUpdate + ',[TotalTax]= 0'
		ELSE 
			 SET @strSQLUpdate = @strSQLUpdate
				+ ',[TotalTax]= dbo.fn_CalBizDocTaxAmt('
				+ CONVERT(VARCHAR(20), @numDomainID) + ',0,'
				+ CONVERT(VARCHAR(20), @numOppId) + ','
				+ CONVERT(VARCHAR(20), @numOppItemCode) + ',1)*Amount/100'

		IF @strSQLUpdate <> '' 
		BEGIN
			SET @strSQLUpdate = 'UPDATE #temp_Packing_List SET numItemCode=numItemCode' + @strSQLUpdate + ' WHERE numItemCode > 0 AND numOppId=' + CONVERT(VARCHAR(20), @numOppId) + ' AND numoppitemtCode=' + CONVERT(VARCHAR(20), @numOppItemCode)
			PRINT @strSQLUpdate
			EXEC (@strSQLUpdate)
		END

		SET @i = @i + 1
	END


	SET @strSQLUpdate = ''
	DECLARE @vcTaxName AS VARCHAR(100)
	DECLARE @numTaxItemID AS NUMERIC(9)
	SET @numTaxItemID = 0
	SELECT TOP 1 @vcTaxName = vcTaxName, @numTaxItemID = numTaxItemID FROM TaxItems WHERE numDomainID = @numDomainID

	WHILE @numTaxItemID > 0
	BEGIN

		EXEC ( 'ALTER TABLE #temp_Packing_List ADD [' + @vcTaxName + '] MONEY' )

		SET @strSQLUpdate = @strSQLUpdate + ',[' + @vcTaxName  + ']= dbo.fn_CalBizDocTaxAmt(numDomainID,'
						+ CONVERT(VARCHAR(20), @numTaxItemID) + ',numOppId,numoppitemtCode,1)*Amount/100'
           

		SELECT TOP 1 @vcTaxName = vcTaxName, @numTaxItemID = numTaxItemID FROM TaxItems WHERE numDomainID = @numDomainID AND numTaxItemID > @numTaxItemID

		IF @@rowcount = 0 
			SET @numTaxItemID = 0
	END


	IF @strSQLUpdate <> '' 
	BEGIN
		SET @strSQLUpdate = 'UPDATE #temp_Packing_List SET numItemCode=numItemCode' + @strSQLUpdate + ' WHERE numItemCode > 0' 
		PRINT @strSQLUpdate
		EXEC (@strSQLUpdate)
	END

	/**** END: Added Sales Tax Column ****/
	
	PRINT 'START'                                                                                          
	PRINT 'QUERY : ' + @strSQL
	PRINT @intRowNum
	WHILE @intRowNum > 0                                                                                  
	BEGIN    
			PRINT 'FROM LOOP'
			PRINT @vcFldname
			PRINT @numFldID

			EXEC('ALTER TABLE #temp_Packing_List add [' + @vcFldname + '] varchar(100)')
		
			SET @strSQL = 'UPDATE #temp_Packing_List SET [' + @vcFldname + '] = dbo.GetCustFldValueItem(' + CONVERT(VARCHAR(10),@numFldID) + ',numItemCode) 
						   WHERE numItemCode > 0'

			PRINT 'QUERY : ' + @strSQL
			EXEC (@strSQL)
			                                                                                 	                                                                                       
			SELECT TOP 1 @numFldID = numFormfieldID,
						 @vcFldname = Fld_label,
						 @intRowNum = (intRowNum + 1) 
			FROM DynamicFormConfigurationDetails DETAIL                                                                                                
			JOIN CFW_Fld_Master MASTER ON MASTER.Fld_id = DETAIL.numFormFieldID                                                                                                 
			WHERE DETAIL.numFormID = 7 
			  AND MASTER.Grp_id = 5 
			  AND DETAIL.numDomainID = @numDomainID 
			 AND numAuthGroupID = @numBizDocId                                                 
			  AND vcFieldType = 'C' 
			  AND intRowNum >= @intRowNum 
			ORDER BY intRowNum                                                                  
			           
			IF @@rowcount=0 SET @intRowNum=0                                                                                                                                                
		END	
	PRINT 'END'	
	SELECT * FROM #temp_Packing_List
END
END
/****** Object:  StoredProcedure [dbo].[USP_GetWareHouseItems]    Script Date: 07/26/2008 16:18:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec USP_GetWareHouseItems 6                            
--created by anoop jayaraj                              
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getwarehouseitems')
DROP PROCEDURE usp_getwarehouseitems
GO
CREATE PROCEDURE [dbo].[USP_GetWareHouseItems]                              
@numItemCode as numeric(9)=0,
@byteMode as tinyint=0                              
as                              
                              
                          
                              
declare @bitSerialize as bit                      
declare @str as nvarchar(max)                 
declare @str1 as nvarchar(max)               
declare @ColName as varchar(25)                      
set @str=''                       
                        
DECLARE @bitKitParent BIT
declare @numItemGroupID as numeric(9)                        
                        
select @numItemGroupID=numItemGroup,@bitSerialize=CASE WHEN bitSerialized=0 THEN bitLotNo ELSE bitSerialized END,
@bitKitParent = ( CASE WHEN ISNULL(bitKitParent,0) = 1
                               AND ISNULL(bitAssembly,0) = 1 THEN 0
                          WHEN ISNULL(bitKitParent,0) = 1 THEN 1
                          ELSE 0
                     END ) from Item where numItemCode=@numItemCode                        
if @bitSerialize=1 set @ColName='numWareHouseItmsDTLID,1'              
else set @ColName='numWareHouseItemID,0'              
              
--Create a Temporary table to hold data                                                            
create table #tempTable ( ID INT IDENTITY PRIMARY KEY,                                                                      
 numCusFlDItemID numeric(9)                                                         
 )                         
                        
insert into #tempTable                         
(numCusFlDItemID)                                                            
select distinct(numOppAccAttrID) from ItemGroupsDTL where numItemGroupID=@numItemGroupID and tintType=2                       
                          
                 
 declare @ID as numeric(9)                        
 declare @numCusFlDItemID as varchar(20)                        
 declare @fld_label as varchar(100),@fld_type as varchar(100)                        
 set @ID=0                        
 select top 1 @ID=ID,@numCusFlDItemID=numCusFlDItemID,@fld_label=fld_label,@fld_type=fld_type from #tempTable                         
 join CFW_Fld_Master on numCusFlDItemID=Fld_ID                        
                         
 while @ID>0                        
 begin                        
                          
   set @str=@str+',  dbo.GetCustFldItems('+@numCusFlDItemID+',9,'+@ColName+') as ['+ @fld_label+']'
	
	IF @byteMode=1                                        
		set @str=@str+',  dbo.GetCustFldItemsValue('+@numCusFlDItemID+',9,'+@ColName+','''+@fld_type+''') as ['+ @fld_label+'Value]'                                        
                          
   select top 1 @ID=ID,@numCusFlDItemID=numCusFlDItemID,@fld_label=fld_label from #tempTable                         
   join CFW_Fld_Master on numCusFlDItemID=Fld_ID and ID >@ID                        
   if @@rowcount=0 set @ID=0                        
                          
 end                        
                       
--      

DECLARE @KitOnHand AS NUMERIC(9);SET @KitOnHand=0

--IF @bitKitParent=1
	--SELECT @KitOnHand=ISNULL(dbo.fn_GetKitInventory(@numItemCode),0)          
  
set @str1='select '

IF @byteMode=1                                        
		set @str1 =@str1 +'I.numItemCode,'
	
set @str1 =@str1 + 'numWareHouseItemID,isnull(vcWarehouse,'''') + '': '' + isnull(WL.vcLocation,'''') vcWarehouse,W.numWareHouseID,
Case when @bitKitParent=1 then ISNULL(dbo.fn_GetKitInventoryByWarehouse(I.numItemCode, W.numWareHouseID),0) else isnull(numOnHand,0) end as [OnHand],
CAST((dbo.fn_UOMConversion(ISNULL(I.numBaseUnit,0),I.numItemCode,I.numDomainId,ISNULL(I.numPurchaseUnit,0)) * Case when @bitKitParent=1 THEN ISNULL(dbo.fn_GetKitInventoryByWarehouse(I.numItemCode, W.numWareHouseID),0) ELSE isnull(numOnHand,0) end) as numeric(18,2)) as PurchaseOnHand,
CAST((dbo.fn_UOMConversion(ISNULL(I.numBaseUnit,0),I.numItemCode,I.numDomainId,ISNULL(I.numSaleUnit,0)) * Case when @bitKitParent=1 THEN ISNULL(dbo.fn_GetKitInventoryByWarehouse(I.numItemCode, W.numWareHouseID),0) ELSE isnull(numOnHand,0) end) as numeric(18,2)) as SalesOnHand,
Case when @bitKitParent=1 THEN 0 ELSE isnull(numReorder,0) END as [Reorder] 
,Case when @bitKitParent=1 THEN 0 ELSE isnull(numOnOrder,0) END as [OnOrder]
,Case when @bitKitParent=1 THEN 0 ELSE isnull(numAllocation,0) END as [Allocation] 
,Case when @bitKitParent=1 THEN 0 ELSE isnull(numBackOrder,0) END as [BackOrder]
,0 as Op_Flag , isnull(WL.vcLocation,'''') Location,isnull(WL.numWLocationID,0) as numWLocationID,
round(isnull(monWListPrice,0),2) Price,isnull(vcWHSKU,'''') as SKU,isnull(vcBarCode,'''') as BarCode '+ case when @bitSerialize=1 then  ' ' else  @str end +'                   
,(SELECT CASE WHEN ISNULL(subI.bitKitParent,0) = 1 AND ISNULL((SELECT COUNT(*) FROM OpportunityItems 
															   WHERE OpportunityItems.numItemCode = WareHouseItems.numItemID
															   AND WareHouseItems.numWareHouseItemID = OpportunityItems.numWarehouseItmsID),0) = 0 THEN 1
			ELSE 0 
	    END 
FROM Item subI WHERE subI.numItemCode = WareHouseItems.numItemID) [IsDeleteKitWarehouse],@bitKitParent [bitKitParent],
(CASE WHEN (SELECT COUNT(*) FROM ItemDetails WHERE numChildItemID=WareHouseItems.numItemID AND numWareHouseItemId = WareHouseItems.numWareHouseItemID) > 0 THEN 1 ELSE 0 END) AS bitChildItemWarehouse
from WareHouseItems                           
join Warehouses W                             
on W.numWareHouseID=WareHouseItems.numWareHouseID 
left join WarehouseLocation WL on WL.numWLocationID =WareHouseItems.numWLocationID
join Item I on I.numItemCode=WareHouseItems.numItemID and WareHouseItems.numDomainId=I.numDomainId
where numItemID='+convert(varchar(15),@numItemCode)                  
   
print(@str1)           

EXECUTE sp_executeSQL @str1, N'@bitKitParent bit',@bitKitParent
                       
                        
set @str1='select numWareHouseItmsDTLID,WDTL.numWareHouseItemID, vcSerialNo,0 as Op_Flag,WDTL.vcComments as Comments,WDTL.numQty,WDTL.numQty as OldQty,W.vcWarehouse '+ case when @bitSerialize=1 then @str else '' end +'
,WDTL.dExpirationDate 
from WareHouseItmsDTL WDTL                             
join WareHouseItems WI                             
on WDTL.numWareHouseItemID=WI.numWareHouseItemID                              
join Warehouses W                             
on W.numWareHouseID=WI.numWareHouseID  
where ISNULL(numQty,0) > 0 and numItemID='+ convert(varchar(15),@numItemCode)                          
print @str1                       
exec (@str1)                       
                      
                      
select Fld_label,fld_id,fld_type,numlistid,vcURL from #tempTable                         
 join CFW_Fld_Master on numCusFlDItemID=Fld_ID                      
drop table #tempTable


-----------------For Kendo UI testting purpose
--create table #tempColumnConfig ( field varchar(100),                                                                      
-- title varchar(100) , format varchar(100)                                                      
-- )  
--
--insert into #tempColumnConfig
--	select 'vcWarehouse','WareHouse','' union all
--	select 'OnHand','On Hand','' union all
--	select 'Reorder','Re Order','' union all
--	select 'Allocation','Allocation','' union all
--	select 'BackOrder','Back Order','' union all
--	select 'Price','Price','{0:c}'
--
--select * from #tempColumnConfig
--
--drop table #tempColumnConfig

-----------------

--set @str1='select case when COUNT(*)=1 then MIN(numWareHouseItmsDTLID) else 0 end as numWareHouseItmsDTLID,MIN(WDTL.numWareHouseItemID) as numWareHouseItemID, vcSerialNo,0 as Op_Flag,WDTL.vcComments as Comments'+ case when @bitSerialize=1 then @str else '' end +',W.vcWarehouse,COUNT(*) Qty
--from WareHouseItmsDTL WDTL                             
--join WareHouseItems WI                             
--on WDTL.numWareHouseItemID=WI.numWareHouseItemID                              
--join Warehouses W                             
--on W.numWareHouseID=WI.numWareHouseID  
--where (tintStatus is null or tintStatus=0)  and  numItemID='+ convert(varchar(15),@numItemCode) +'
--GROUP BY vcSerialNo,WDTL.vcComments,W.vcWarehouse'
--                          
--print @str1                       
--exec (@str1)                       
              
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_InboxItems' ) 
    DROP PROCEDURE USP_InboxItems
GO
Create PROCEDURE [dbo].[USP_InboxItems]                                                                                              
@PageSize [int] ,                            
@CurrentPage [int],                            
@srch as varchar(100) = '',                            
--@vcSubjectFromTo as varchar(100) ='',
--@srchAttachmenttype as varchar(100) ='',                                   
@ToEmail as varchar (100)  ,        
@columnName as varchar(50) ,        
@columnSortOrder as varchar(4)  ,  
@numDomainId as numeric(9),  
@numUserCntId as numeric(9)  ,    
@numNodeId as numeric(9),  
--@chrSource as char(1),
@ClientTimeZoneOffset AS INT=0,
@tintUserRightType AS INT,
@EmailStatus AS NUMERIC(9)=0,
@srchFrom as varchar(100) = '', 
@srchTo as varchar(100) = '', 
@srchSubject as varchar(100) = '', 
@srchHasWords as varchar(100) = '', 
@srchHasAttachment as bit = 0, 
@srchIsAdvancedsrch as bit = 0,
@srchInNode as numeric(9)=0,
@FromDate AS DATE = NULL,
@ToDate AS DATE = NULL
as                                                      

--GET ENTERIES FROM PERTICULAR DOMAIN FROM VIEW_Email_Alert_Config
SELECT numDivisionID,numContactId,TotalBalanceDue,OpenSalesOppCount, OpenCaseCount, OpenProjectCount, UnreadEmailCount,OpenActionItemCount INTO #TEMP FROM VIEW_Email_Alert_Config WHERE numDomainId=@numDomainId

/************* START - GET ALERT CONFIGURATION **************/
DECLARE @bitOpenActionItem AS BIT = 1
DECLARE @bitOpencases AS BIT = 1
DECLARE @bitOpenProject AS BIT = 1
DECLARE @bitOpenSalesOpp AS BIT = 1
DECLARE @bitBalancedue AS BIT = 1
DECLARE @bitUnreadEmail AS BIT = 1
DECLARE @bitCampaign AS BIT = 1

SELECT  
	@bitOpenActionItem = ISNULL([bitOpenActionItem],1),
    @bitOpencases = ISNULL([bitOpenCases],1),
    @bitOpenProject = ISNULL([bitOpenProject],1),
    @bitOpenSalesOpp = ISNULL([bitOpenSalesOpp],1),
    @bitBalancedue = ISNULL([bitBalancedue],1),
    @bitUnreadEmail = ISNULL([bitUnreadEmail],1),
	@bitCampaign = ISNULL([bitCampaign],1)
FROM    
	AlertConfig
WHERE   
	AlertConfig.numDomainId = @numDomainId AND 
	AlertConfig.numContactId = @numUserCntId

/************* END - GET ALERT CONFIGURATION **************/
                                                      
 ---DECLARE @CRMType NUMERIC 
DECLARE @tintOrder AS TINYINT                                                      
DECLARE @vcFieldName AS VARCHAR(50)                                                      
DECLARE @vcListItemType AS VARCHAR(1)                                                 
DECLARE @vcAssociatedControlType VARCHAR(20)                                                      
DECLARE @numListID AS NUMERIC(9)                                                      
DECLARE @vcDbColumnName VARCHAR(20)                          
DECLARE @WhereCondition VARCHAR(2000)                           
DECLARE @vcLookBackTableName VARCHAR(2000)                    
DECLARE @bitCustom AS BIT
DECLARE @bitAllowEdit AS CHAR(1)                 
DECLARE @numFieldId AS NUMERIC  
DECLARE @bitAllowSorting AS CHAR(1)              
DECLARE @vcColumnName AS VARCHAR(500)  

DECLARE @strSql AS VARCHAR(5000)
DECLARE @column AS VARCHAR(50)                             
DECLARE @join AS VARCHAR(400) 
DECLARE @Nocolumns AS TINYINT               
DECLARE @lastRec AS INTEGER 
DECLARE @firstRec AS INTEGER                   
SET @join = ''           
SET @strSql = ''
--DECLARE @ClientTimeZoneOffset AS INT 
--DECLARE @numDomainId AS NUMERIC(18, 0)
SET @tintOrder = 0  
SET @WhereCondition = ''

SET @firstRec = ( @CurrentPage - 1 ) * @PageSize                                                              
SET @lastRec = ( @CurrentPage * @PageSize + 1 )   
SET @column = @columnName                
--DECLARE @join AS VARCHAR(400)                    
SET @join = ''             
IF @columnName LIKE 'Cust%' 
    BEGIN                  
        SET @join = ' left Join CFW_FLD_Values_Email CFW on CFW.RecId=DM.numEmailHstrID and CFW.fld_id= '
            + REPLACE(@columnName, 'Cust', '') + ' '                                                           
        SET @column = 'CFW.Fld_Value'
    END                                           
ELSE 
    IF @columnName LIKE 'DCust%' 
        BEGIN                  
            SET @join = ' left Join CFW_FLD_Values_Email CFW on CFW.RecId=DM.numEmailHstrID  and CFW.fld_id= '
                + REPLACE(@columnName, 'DCust', '')                                                                                                     
            SET @join = @join
                + ' left Join ListDetails LstCF on LstCF.numListItemID=CFW.Fld_Value  '                  
            SET @column = 'LstCF.vcData'                    
                  
        END            
    ELSE 
        BEGIN            
            DECLARE @lookbckTable AS VARCHAR(50)                
            SET @lookbckTable = ''                                                         
            SELECT  @lookbckTable = vcLookBackTableName
            FROM    View_DynamicDefaultColumns
            WHERE   numFormId = 44
                    AND vcDbColumnName = @columnName  and numDomainID=@numDOmainID               
            IF @lookbckTable <> '' 
                BEGIN                
                    IF @lookbckTable = 'EmailHistory' 
                        SET @column = 'ADC.' + @column                
                END                                                              
        END              
--DECLARE @strSql AS VARCHAR(5000)   
--SET @numNodeID = 0  
--SET @srchBody = ''
--SET @vcSubjectFromTo =''
--PRINT 'numnodeid' + CONVERT(VARCHAR, @numNodeId)                                 
SET @strSql = '
with FilterRows as (SELECT TOP('+ CONVERT(varchar,@CurrentPage) + ' * '+ CONVERT(varchar,@PageSize) + ') '                
SET @strSql = @strSql + 'ROW_NUMBER() OVER(ORDER BY EH.bintCreatedOn DESC ) AS RowNumber,COUNT(*) OVER(PARTITION BY NULL) AS TotalRowCount,EH.numEmailHstrID,isnull(EM.numContactId,0) as numContactId'
SET @strSql = @strSql + ' FROM [EmailHistory] EH JOIN EmailMaster EM on EM.numEMailId=(CASE numNodeID WHEN 4 THEN SUBSTRING(EH.vcTo, 1, CHARINDEX(''$^$'', EH.vcTo, 1) - 1) ELSE EH.numEmailId END)'
SET @strSql = @strSql + ' 
WHERE EH.numDomainID ='+ CONVERT(VARCHAR,@numDomainId) +' And [numUserCntId] = '+ CONVERT(VARCHAR,@numUserCntId)+' '
--SET @strSql = @strSql + 'Where [numDomainID] ='+ CONVERT(VARCHAR,@numDomainId) +'  And [numUserCntId] = '+ CONVERT(VARCHAR,@numUserCntId)+' AND numNodeID ='+ CONVERT(VARCHAR,@numNodeId) +''
SET @strSql = @strSql + '
And chrSource IN(''B'',''I'') '

--Simple Search for All Node
IF @srchIsAdvancedsrch=0 
BEGIN
if len(@srch)>0
	BEGIN
		SET @strSql = @strSql + '
			AND (EH.vcSubject LIKE ''%'' + '''+ @srch +''' + ''%'' or EH.vcBodyText LIKE ''%'' + '''+ @srch + ''' + ''%''
			or EH.vcFrom LIKE ''%'' + '''+ @srch + ''' + ''%'' or EH.vcTo LIKE ''%'' + '''+ @srch + ''' + ''%'')'
	END
ELSE
	BEGIN --Particular Node
		SET @strSql = @strSql + ' AND [numNodeID] = '+ CONVERT(VARCHAR,@numNodeID) +' '
	END
END
--Advanced Search for All Node or selected Node
Else IF @srchIsAdvancedsrch=1
BEGIN
declare @strCondition as varchar(2000);set @strCondition=''

if len(@srchSubject)>0
	SET @strCondition='EH.vcSubject LIKE ''%'' + '''+ @srchSubject +''' + ''%'''

if len(@srchFrom)>0
	begin
		if len(@strCondition)>0
			SET @strCondition=@strCondition + ' and '

		SET @strCondition = @strCondition + ' (EH.vcFrom LIKE ''%' + REPLACE(@srchFrom,',','%'' OR EH.vcFrom LIKE ''%') + '%'')'
	end

if len(@srchTo)>0
	begin
		if len(@strCondition)>0
			SET @strCondition=@strCondition + ' and '

		SET @strCondition = @strCondition + ' (EH.vcTo LIKE ''%' + REPLACE(@srchTo,',','%'' OR EH.vcTo LIKE ''%') + '%'')' 
	end

if len(@srchHasWords)>0
	begin
		if len(@strCondition)>0
			SET @strCondition=@strCondition + ' and '

		SET @strCondition = @strCondition + ' EH.vcBodyText LIKE ''%'' + '''+ @srchHasWords + ''' + ''%'''
	end

IF @srchHasAttachment=1
BEGIN
	if len(@strCondition)>0
			SET @strCondition=@strCondition + ' and '
	
	SET @strCondition = @strCondition + ' EH.bitHasAttachments =  '''+ CONVERT(VARCHAR,@srchHasAttachment) + ''''
END

IF (LEN(@FromDate) > 0 AND LEN(@ToDate) > 0 AND ISDATE(CAST(@FromDate AS VARCHAR(100))) = 1 AND ISDATE(CAST(@ToDate AS VARCHAR(100))) = 1)
BEGIN
	IF LEN(@strCondition)>0
		SET @strCondition=@strCondition + ' AND '

	SET @strCondition = @strCondition + ' EH.dtReceivedOn >= '''+ CONVERT(VARCHAR,@FromDate) + ''' AND EH.dtReceivedOn <= ''' + CONVERT(VARCHAR,@ToDate) + ''''
END

if len(@strCondition)>0
	BEGIN
		SET @strSql = @strSql +' and (' + @strCondition + ')'
		
		if @numNodeId>-1
			SET @strSql = @strSql + ' AND [numNodeID] = '+ CONVERT(VARCHAR,@numNodeId) +' '
	END
ELSE
	BEGIN --Particular Node
		SET @strSql = @strSql + ' AND [numNodeID] = '+ CONVERT(VARCHAR,@numNodeID) +' '
	END



--SET @strSql = @strSql + '
--AND (EH.vcSubject LIKE ''%'' + '''+ @srchSubject +''' + ''%'' and EH.vcFrom LIKE ''%'' + '''+ @srchFrom + ''' + ''%'' 
--and EH.vcTo LIKE ''%'' + '''+ @srchTo + ''' + ''%'' and EH.vcBodyText LIKE ''%'' + '''+ @srchHasWords + ''' + ''%'''
--
--IF @srchHasAttachment=1
--BEGIN
--SET @strSql = @strSql + ' and EH.bitHasAttachments =  '''+ CONVERT(VARCHAR,@srchHasAttachment) + ''''
--END

END


SET @strSql = @strSql + ' )'
--,FinalResult As
--( SELECT  TOP ('+CONVERT(VARCHAR,@PageSize)+ ') numEmailHstrID'
--SET @strSql = @strSql + ' 
--From FilterRows'
--SET @strSql = @strSql + '
--WHERE RowNumber > (('+ CONVERT(VARCHAR,@CurrentPage) + ' - 1) * '+ CONVERT(VARCHAR,@PageSize) +' ))'
--PRINT @strSql
--EXEC(@strSql)

select @Nocolumns=isnull(sum(TotalRow),0)  from(            
Select count(*) TotalRow from View_DynamicColumns where numFormId=44 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=0 
Union 
Select count(*) TotalRow from View_DynamicCustomColumns where numFormId=44 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=0) TotalRows

  CREATE TABLE #tempForm (tintOrder TINYINT,vcDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),
  vcListItemType char(3),numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,
  numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,bitIsAlphaNumeric BIT,bitIsNumeric BIT,
  bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),ListRelID numeric(9))


---Insert number of rows into temp table.
PRINT 'number OF columns :' + CONVERT(VARCHAR, @Nocolumns)
IF  @Nocolumns > 0 
    BEGIN    

INSERT INTO #tempForm
select tintRow+1 as tintOrder,vcDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
 vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName                                                 
,bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID 
 FROM View_DynamicColumns 
 where numFormId=44 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=0
  AND ISNULL(bitSettingField,0)=1 AND ISNULL(bitCustom,0)=0
                  
    UNION

     select tintRow+1 as tintOrder,vcDbColumnName,vcFieldName,                  
 vcAssociatedControlType,'' as vcListItemType,numListID,''                                                 
,bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID
 from View_DynamicCustomColumns
 where numFormId=44 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=0
 AND ISNULL(bitCustom,0)=1
 
 ORDER BY tintOrder ASC  
   
 END 
ELSE 
    BEGIN

 INSERT INTO #tempForm
select tintOrder,vcDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
 vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName                                                 
,bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID 
 FROM View_DynamicDefaultColumns
 where numFormId=44 and bitDefault=1 AND ISNULL(bitSettingField,0)=1 AND numDomainID=@numDomainID
order by tintOrder asc   

    END
      SET @strSql =@strSql + ' Select TotalRowCount, EH.numEmailHstrID ,EH.numListItemId,bitIsRead,CASE WHEN LEN(Cast(EH.vcBodyText AS VARCHAR(1000)))>150 THEN SUBSTRING(EH.vcBodyText,0,150) + ''...'' ELSE
		 EH.vcBodyText END as vcBodyText,EH.numEmailHstrID As [KeyId~numEmailHstrID~0~0~0~0~HiddenField],FR.numContactId AS [ContactId~numContactID~0~0~0~0~HiddenField] ,EH.bitIsRead As [IsRead~bitIsRead~0~0~0~0~Image],isnull(EH.IsReplied,0) As [~IsReplied~0~0~0~0~Image] '
 
Declare @ListRelID as numeric(9) 

   select top 1 @tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,                
  @vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,@numListID=numListID,@vcLookBackTableName=vcLookBackTableName                                               
,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,@ListRelID=ListRelID
  from  #tempForm order by tintOrder asc            

WHILE @tintOrder > 0                                                      
    BEGIN                                                      
        IF @bitCustom = 0  
            BEGIN            
                DECLARE @Prefix AS VARCHAR(5)                        
                IF @vcLookBackTableName = 'EmailHistory' 
                    SET @Prefix = 'EH.'                        
                SET @vcColumnName = @vcFieldName + '~' + @vcDbColumnName
                    + '~' + @bitAllowSorting + '~'
                    + CONVERT(VARCHAR(10), @numFieldId) + '~'
                    + @bitAllowEdit + '~' + CONVERT(VARCHAR(1), @bitCustom)
                    + '~' + @vcAssociatedControlType
                PRINT @vcColumnName
                
                IF @vcAssociatedControlType = 'SelectBox' 
                    BEGIN      
                        PRINT @vcListItemType                                                    
                        IF @vcListItemType = 'L' 
                            BEGIN    
                            ---PRINT 'columnName' + @vcColumnName
                                SET @strSql = @strSql + ',L'
                                    + CONVERT(VARCHAR(3), @tintOrder)
                                    + '.vcData' + ' [' + @vcColumnName + ']'                                                         
                                SET @WhereCondition = @WhereCondition
                                    + ' left Join ListDetails L'
                                    + CONVERT(VARCHAR(3), @tintOrder)
                                    + ' on L' + CONVERT(VARCHAR(3), @tintOrder)
                                    + '.numListItemID=EH.numListItemID'                                                           
                            END 
                           
                    END 
                  ELSE IF (@vcAssociatedControlType = 'TextBox'
                        OR @vcAssociatedControlType = 'TextArea'
                        OR @vcAssociatedControlType = 'Image')  AND @vcDbColumnName<>'AlertPanel' AND @vcDbColumnName<>'RecentCorrespondance'
                        BEGIN
							IF @numNodeId=4 and @vcDbColumnName='vcFrom'  
							BEGIN
								  SET @vcColumnName = 'To' + '~' + @vcDbColumnName
                    + '~' + @bitAllowSorting + '~'
                    + CONVERT(VARCHAR(10), @numFieldId) + '~'
                    + @bitAllowEdit + '~' + CONVERT(VARCHAR(1), @bitCustom)
                    + '~' + @vcAssociatedControlType

								 SET @strSql = @strSql + ',' + @Prefix  + 'vcTo' + ' [' + @vcColumnName + ']'   
							END 
							ELSE
							BEGIN
								SET @strSql = @strSql + ',' + @Prefix  + @vcDbColumnName + ' [' + @vcColumnName + ']'   
							END                        
                             PRINT @Prefix   
                        END 
				Else IF  @vcDbColumnName='AlertPanel'   
							BEGIN 
								SET @strSql = @strSql +  ', dbo.GetAlertDetail(EH.numEmailHstrID, ACI.numDivisionID, ACI.numECampaignID, FR.numContactId,' 
													  + CAST(@bitOpenActionItem AS VARCHAR(10)) + ',' 
													  + CAST(@bitOpencases AS VARCHAR(10)) + ',' 
													  + CAST(@bitOpenProject AS VARCHAR(10)) + ',' 
													  + CAST(@bitOpenSalesOpp AS VARCHAR(10)) + ',' 
													  + CAST(@bitBalancedue AS VARCHAR(10)) + ',' 
													  + CAST(@bitUnreadEmail AS VARCHAR(10)) + ',' 
													  + CAST(@bitCampaign AS VARCHAR(10)) + ',' +
													  'V1.TotalBalanceDue,V1.OpenSalesOppCount,V1.OpenCaseCount,V1.OpenProjectCount,V1.UnreadEmailCount,V2.OpenActionItemCount,V3.CampaignDTLCount) AS  [' + '' + @vcColumnName + '' + ']' 
							 END 
                 ELSE IF  @vcDbColumnName='RecentCorrespondance'
							BEGIN 
					            --SET @strSql = @strSql + ','+ '(SELECT COUNT(*) FROM dbo.Communication WHERE numContactId = FR.numContactId)' + ' AS  [' + '' + @vcColumnName + '' + ']'                                                       
					            SET @strSql = @strSql + ',0 AS  [' + '' + @vcColumnName + '' + ']'
                             END 
--				else if @vcAssociatedControlType='DateField'                                                  
--					begin            
--							set @strSql=@strSql+',case when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+') )= convert(varchar(11),getdate()) then ''<b><font color=red>Today</font></b>'''      
--							set @strSql=@strSql+'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+') )= convert(varchar(11),dateadd(day,-1,getdate())) then ''<b><font color=purple>YesterDay</font></b>'''      
--							set @strSql=@strSql+'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+') )= convert(varchar(11),dateadd(day,1,getdate())) then''<b><font color=orange>Tommorow</font></b>'' ' 
--							set @strSql=@strSql+'else dbo.FormatedDateFromDate(DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+'),'+convert(varchar(10),@numDomainId)+') end  ['+ @vcColumnName+']'
--				    end    
				else if @vcAssociatedControlType='DateField' AND @vcDbColumnName <> 'dtReceivedOn'                                     
					begin           
							set @strSql=@strSql+',case when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+') )= convert(varchar(11),getdate()) then ''<b><font color=red>Today</font></b>'''      
							set @strSql=@strSql+'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+') )= convert(varchar(11),dateadd(day,-1,getdate())) then ''<b><font color=purple>YesterDay</font></b>'''      
							set @strSql=@strSql+'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+') )= convert(varchar(11),dateadd(day,1,getdate())) then''<b><font color=orange>Tommorow</font></b>'' ' 
							set @strSql=@strSql+'else dbo.FormatedDateFromDate(DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+'),'+convert(varchar(10),@numDomainId)+') end  ['+ @vcColumnName+']'
				    end    
				else if @vcAssociatedControlType='DateField' AND @vcDbColumnName = 'dtReceivedOn'                                                 
					begin    
							set @strSql=@strSql+',case when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+') )= convert(varchar(11),getdate()) then ''<b><font color=red>Today</font></b>'''      
							set @strSql=@strSql+'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+') )= convert(varchar(11),dateadd(day,-1,getdate())) then ''<b><font color=purple>YesterDay</font></b>'''      
							set @strSql=@strSql+'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+') )= convert(varchar(11),dateadd(day,1,getdate())) then''<b><font color=orange>Tommorow</font></b>'' ' 
							set @strSql=@strSql+'else CONVERT(VARCHAR(20),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+'),100) end  ['+ @vcColumnName+']'
				    end   
			END 
else if @bitCustom = 1                
begin                
            
               SET @vcColumnName= @vcFieldName+'~'+ @vcDbColumnName+'~0~'+ convert(varchar(10),@numFieldId)+'~1~'+ convert(varchar(1),@bitCustom) +'~'+ @vcAssociatedControlType
       
   select @vcFieldName=FLd_label,@vcAssociatedControlType=fld_type,@vcDbColumnName='Cust'+Convert(varchar(10),Fld_Id)                               
    from CFW_Fld_Master                                                
   where    CFW_Fld_Master.Fld_Id = @numFieldId                 
                
                 
    if @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'               
   begin                
                   
    set @strSql= @strSql+',CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value  ['+ @vcColumnName +']'                   
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Cont CFW'+ convert(varchar(3),@tintOrder)+ '                 
    on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=FR.numContactId   '                                                         
   end      
    else if @vcAssociatedControlType = 'CheckBox'                 
   begin              
                 
    set @strSql= @strSql+',case when isnull(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,0)=0 then ''No'' when isnull(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,0)=1 then ''Yes'' end   ['+ @vcColumnName +']'              
   
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Cont CFW'+ convert(varchar(3),@tintOrder)+ '               
    on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=FR.numContactId   '                                                       
   end              
  else if @vcAssociatedControlType = 'DateField'              
   begin                
                   
    set @strSql= @strSql+',dbo.FormatedDateFromDate(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,'+convert(varchar(10),@numDomainId)+')  ['+ @vcColumnName +']'                   
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Cont CFW'+ convert(varchar(3),@tintOrder)+ '                 
    on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=FR.numContactId   '                                                         
   end                
   else if @vcAssociatedControlType = 'SelectBox'                    
   begin                
    set @vcDbColumnName = 'DCust'+Convert(varchar(10),@numFieldId)                
    set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName +']'                                                          
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Cont CFW'+ convert(varchar(3),@tintOrder)+ '                 
     on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=FR.numContactId   '                                                         
    set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value'                
   end                 
end     
                   --    SET @vcColumnName= @vcFieldName+'~'+ @vcDbColumnName+'~0~'+ convert(varchar(10),@numFieldId)+'~1~'+ convert(varchar(1),@bitCustom) +'~'+ @vcAssociatedControlType   
  select top 1 @tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,                
  @vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,@numListID=numListID,@vcLookBackTableName=vcLookBackTableName                                               
,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,@ListRelID=ListRelID
  from #tempForm WHERE tintOrder > @tintOrder-1 order by tintOrder asc            
 
           IF @@rowcount = 0 SET @tintOrder = 0 
		   PRINT @tintOrder 
    END  
                     
SELECT  * FROM    #tempForm

-------Change Row Color-------
Declare @vcCSOrigDbCOlumnName as varchar(50),@vcCSLookBackTableName as varchar(50),@vcCSAssociatedControlType as varchar(50)
SET @vcCSOrigDbCOlumnName=''
SET @vcCSLookBackTableName=''

Create table #tempColorScheme(vcOrigDbCOlumnName varchar(50),vcLookBackTableName varchar(50),vcFieldValue varchar(50),vcFieldValue1 varchar(50),vcColorScheme varchar(50),vcAssociatedControlType varchar(50))

insert into #tempColorScheme select DFM.vcOrigDbCOlumnName,DFM.vcLookBackTableName,DFCS.vcFieldValue,DFCS.vcFieldValue1,DFCS.vcColorScheme,DFFM.vcAssociatedControlType 
from DycFieldColorScheme DFCS join DycFormField_Mapping DFFM on DFCS.numFieldID=DFFM.numFieldID AND DFCS.numFormID=DFFM.numFormID
join DycFieldMaster DFM on DFM.numModuleID=DFFM.numModuleID and DFM.numFieldID=DFFM.numFieldID
where DFCS.numDomainID=@numDomainID and DFFM.numFormID=10 AND DFCS.numFormID=10 and isnull(DFFM.bitAllowGridColor,0)=1

IF(SELECT COUNT(*) FROM #tempColorScheme)>0
BEGIN
   select top 1 @vcCSOrigDbCOlumnName=vcOrigDbCOlumnName,@vcCSLookBackTableName=vcLookBackTableName,@vcCSAssociatedControlType=vcAssociatedControlType from #tempColorScheme
END   
---------------------------- 

IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
BEGIN
set @strSql=@strSql+',tCS.vcColorScheme'
END  

DECLARE @fltTotalSize FLOAT
SELECT @fltTotalSize=isnull(SUM(ISNULL(SpaceOccupied,0)),0) FROM dbo.View_InboxCount where numDomainID=@numDomainID and numUserCntID=@numUserCntID;

SET @strSql = @strSql + ', '+ CONVERT(VARCHAR(18),@fltTotalSize) +' AS TotalSize From EmailHistory EH Inner Join FilterRows FR on  FR.numEmailHstrID = EH.numEmailHstrID '
SET @strSql=@strSql + ' LEFT JOIN
	AdditionalContactsInformation ACI
ON 
	ACI.numContactId = FR.numContactId
CROSS APPLY
(
	SELECT
		SUM(TotalBalanceDue) TotalBalanceDue,
		SUM(OpenSalesOppCount) OpenSalesOppCount,
		SUM(OpenCaseCount) OpenCaseCount,
		SUM(OpenProjectCount) OpenProjectCount,
		SUM(UnreadEmailCount) UnreadEmailCount
	FROM
		#TEMP
	WHERE
		#TEMP.numDivisionId = ACI.numDivisionId
) AS V1
CROSS APPLY
(
	SELECT
		SUM(OpenActionItemCount) OpenActionItemCount
	FROM
		#TEMP
	WHERE
		#TEMP.numDivisionId = ACI.numDivisionId AND
		#TEMP.numContactId = FR.numContactId
) AS V2
CROSS APPLY
(
	SELECT 
		COUNT(*) AS CampaignDTLCount
	FROM 
		ConECampaignDTL 
	WHERE 
		numConECampID = (SELECT TOP 1 ISNULL(numConEmailCampID,0)  FROM ConECampaign WHERE numECampaignID = ACI.numECampaignID AND numContactID = ACI.numContactId ORDER BY numConEmailCampID DESC) AND 
		ISNULL(bitSend,0) = 0
) AS V3 '
SET @strSql = @strSql + @WhereCondition

IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
BEGIN
	 if @vcCSLookBackTableName = 'AdditionalContactsInformation'                  
		set @Prefix = 'ADC.'                  
     if @vcCSLookBackTableName = 'DivisionMaster'                  
		set @Prefix = 'DM.'
	 if @vcCSLookBackTableName = 'CompanyInfo'                  
		set @Prefix = 'CI.' 
	 if @vcCSLookBackTableName = 'AddressDetails'                  
		set @Prefix = 'AD.'   

set @strSql=@strSql+' left JOIN AdditionalContactsInformation ADC ON ADC.numcontactId=FR.numContactID                                                                             
 left JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID                                                                            
 left JOIN CompanyInfo CI ON DM.numCompanyID = CI.numCompanyId '
   
	IF @vcCSAssociatedControlType='DateField'
	BEGIN
			set @strSql=@strSql+' left join #tempColorScheme tCS on CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) >= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue as int),GETUTCDATE()),111)
			 and CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) <= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue1 as int),GETUTCDATE()),111)'
	END
	ELSE IF @vcCSAssociatedControlType='SelectBox' OR @vcCSAssociatedControlType='ListBox' OR @vcCSAssociatedControlType='CheckBox'
	BEGIN
		set @strSql=@strSql+' left join #tempColorScheme tCS on tCS.vcFieldValue=' + @Prefix + @vcCSOrigDbCOlumnName
	END
END

SET @strSql = @strSql + ' Where RowNumber >'  + CONVERT(VARCHAR(10), @firstRec) + ' and RowNumber <'
        + CONVERT(VARCHAR(10), @lastRec) 
IF @EmailStatus <> 0 
BEGIN
	SET @strSql = @strSql + ' AND EH.numListItemId = ' + CONVERT(VARCHAR(10), @EmailStatus)
END       
SET @strSql = @strSql + ' order by RowNumber'

PRINT @strSql 
EXEC(@strSql)   
DROP TABLE #tempForm          
DROP TABLE #TEMP
 
 /*
 declare @firstRec as integer                                                      
 declare @lastRec as integer                                                      
 set @firstRec= (@CurrentPage-1) * @PageSize                                                      
 set @lastRec= (@CurrentPage*@PageSize+1)   ;    
 
DECLARE @totalRows  INT
DECLARE @fltTotalSize FLOAT
SELECT @totalRows = RecordCount FROM dbo.View_InboxCount where numDomainID=@numDomainID and numUserCntID=@numUserCntID and numNodeId = @numNodeID ;
SELECT @fltTotalSize=SUM(ISNULL(SpaceOccupied,0)) FROM dbo.View_InboxCount where numDomainID=@numDomainID and numUserCntID=@numUserCntID;



-- Step 1: Get Only rows specific to user and domain 

WITH FilterRows
AS 
(
   SELECT 
   TOP (@CurrentPage * @PageSize)
        ROW_NUMBER() OVER(ORDER BY EH.bintCreatedOn DESC ) AS totrows,
        EH.numEmailHstrID
   FROM [EmailHistory] EH
		--LEFT JOIN EmailHstrAttchDtls A ON EH.numEmailHstrID = A.numEmailHstrID
   WHERE 
	   [numDomainID] = @numDomainID
	AND [numUserCntId] = @numUserCntId
	AND numNodeID = @numNodeId
	AND chrSource IN('B','I')
    AND (EH.vcBodyText LIKE '%' + @srchBody + '%' OR @srchBody = '')
	AND (EH.vcSubject LIKE '%' + @vcSubjectFromTo + '%' OR @vcSubjectFromTo ='')
    --AND (A.[vcAttachmentType] LIKE '%'+ @srchAttachmenttype + '%' OR @srchAttachmenttype='')
   
)
 --select seq1 + totrows1 -1 as TotRows2,* FROM [FilterRows]
,FinalResult
AS 
(
	SELECT  TOP (@PageSize) numEmailHstrID --,seq + totrows -1 as TotRows
	FROM FilterRows
	WHERE totrows > ((@CurrentPage - 1) * @PageSize)
)



-- select * FROM [UserEmail]
select  
		@totalRows TotRows,
		@fltTotalSize AS TotalSize,
		B.[numEmailHstrID],
        B.[numDomainID],
--        B.bintCreatedOn,
--        B.dtReceivedOn,
        B.numEmailHstrID,
        ISNULL(B.numListItemId,-1) AS numListItemId,
        ISNULL(vcSubject, '') AS vcSubject,
        CONVERT(VARCHAR(MAX), vcBody) AS vcBody,
        ISNULL(vcItemId, '') AS ItemId,
        ISNULL(vcChangeKey, '') AS ChangeKey,
        ISNULL(bitIsRead, 'False') AS IsRead,
        ISNULL(vcSize, 0) AS vcSize,
        ISNULL(bitHasAttachments, 'False') AS HasAttachments,
        ISNULL(B.tintType, 1) AS type,
        ISNULL(B.chrSource, 'B') AS chrSource,
        ISNULL(vcCategory, 'white') AS vcCategory,
        CASE B.[numNodeId] WHEN 4 THEN REPLACE(REPLACE(ISNULL(B.[vcTo],''),'<','') , '>','') ELSE B.vcFrom END FromName,
--        dbo.GetEmaillName(B.numEmailHstrID, 4) AS FromName,
--        dbo.GetEmaillName(B.numEmailHstrID, 1) AS ToName,
		CASE B.[numNodeId] WHEN 4 THEN 
			dbo.FormatedDateTimeFromDate(DATEADD(minute, -@ClientTimeZoneOffset,B.bintCreatedOn),B.numDomainId) 
		ELSE 
			dbo.FormatedDateTimeFromDate(B.dtReceivedOn,B.numDomainId)
		END dtReceivedOn,
        ISNULL(CONVERT(VARCHAR(5),B.[numNoofTimes]),'') NoOfTimeOpened
FROM [FinalResult] A INNER JOIN emailHistory B ON B.numEmailHstrID = A.numEmailHstrID;



--WITH UserEmail
--AS
--(
--			   SELECT
--						ROW_NUMBER() OVER ( ORDER BY X.bintCreatedOn DESC ) AS RowNumber,
--                        X.[numEmailHstrID]
--               FROM		[EmailHistory] X
--               WHERE    [numDomainID] = @numDomainID
--						AND [numUserCntId] = @numUserCntId
--						AND numNodeID = @numNodeId
--						AND X.chrSource IN('B','I')
--						
--)
--	SELECT 
--		COUNT(*) 
--   FROM UserEmail UM INNER JOIN [EmailHistory] EH  ON UM.numEmailHstrID =EH.numEmailHstrID
--		LEFT JOIN EmailHstrAttchDtls A ON EH.numEmailHstrID = A.numEmailHstrID
--   WHERE 
--		 (EH.vcBodyText LIKE '%' + @srchBody + '%' OR @srchBody = '')
--	AND (EH.vcSubject LIKE '%' + @vcSubjectFromTo + '%' OR @vcSubjectFromTo ='')
--    AND (A.[vcAttachmentType] LIKE '%'+ @srchAttachmenttype + '%' OR @srchAttachmenttype='')



--SELECT COUNT(*)
--FROM      View_Inbox I INNER JOIN [EmailHistory] EH ON EH.numEmailHstrID = I.numEmailHstrID
--			LEFT JOIN EmailHstrAttchDtls A ON EH.numEmailHstrID = A.numEmailHstrID
-- WHERE     I.numDomainId = @numDomainId
--                                AND I.numUserCntId = @numUserCntId
--                                AND I.numNodeId = @numNodeId
--                                AND I.chrSource IN('B','I')
--                                AND (EH.vcBodyText LIKE '%' + @srchBody + '%' OR @srchBody = '')
--								AND (I.vcSubjectFromTo LIKE '%' + @vcSubjectFromTo + '%' OR @vcSubjectFromTo ='')
--                                AND (A.[vcAttachmentType] LIKE '%'+ @srchAttachmenttype + '%' OR @srchAttachmenttype='')

RETURN ;
*/                                           
/* SELECT Y.[ID],
        Y.[numEmailHstrID],
        emailHistory.[numDomainID],
        emailHistory.bintCreatedOn,
        emailHistory.dtReceivedOn,
        emailHistory.numEmailHstrID,
        ISNULL(vcSubject, '') AS vcSubject,
        CONVERT(VARCHAR(MAX), vcBody) AS vcBody,
        ISNULL(vcItemId, '') AS ItemId,
        ISNULL(vcChangeKey, '') AS ChangeKey,
        ISNULL(bitIsRead, 'False') AS IsRead,
        ISNULL(vcSize, 0) AS vcSize,
        ISNULL(bitHasAttachments, 'False') AS HasAttachments,
        ISNULL(emailHistory.tintType, 1) AS type,
        ISNULL(emailHistory.chrSource, 'B') AS chrSource,
        ISNULL(vcCategory, 'white') AS vcCategory,
        dbo.GetEmaillName(Y.numEmailHstrID, 4) AS FromName,
        dbo.GetEmaillName(Y.numEmailHstrID, 1) AS ToName,
        dbo.FormatedDateFromDate(DATEADD(minute, -@ClientTimeZoneOffset,
                                         emailHistory.bintCreatedOn),
                                 emailHistory.numDomainId) AS bintCreatedOn,
        dbo.FormatedDateFromDate(emailHistory.dtReceivedOn,
                                 emailHistory.numDomainId) AS dtReceivedOn,
        ISNULL(CONVERT(VARCHAR(5),EmailHistory.[numNoofTimes]),'') NoOfTimeOpened
 FROM   ( SELECT   
				ROW_NUMBER() OVER ( ORDER BY X.bintCreatedOn DESC ) AS ID,
				X.[numEmailHstrID]
          FROM      ( SELECT    DISTINCT
                                EH.numEmailHstrID,
                                EH.[bintCreatedOn]
                      FROM      --View_Inbox I INNER JOIN 
                      [EmailHistory] EH --ON EH.numEmailHstrID = I.numEmailHstrID
								LEFT JOIN EmailHstrAttchDtls A ON EH.numEmailHstrID = A.numEmailHstrID
                      WHERE     EH.numDomainId = @numDomainId
                                AND EH.numUserCntId = @numUserCntId
                                AND EH.numNodeId = @numNodeId
                                AND EH.chrSource IN('B','I')
                                AND (EH.vcBodyText LIKE '%' + @srchBody + '%' OR @srchBody = '')
								AND (EH.vcSubject LIKE '%' + @vcSubjectFromTo + '%' OR @vcSubjectFromTo ='')
                                AND (A.[vcAttachmentType] LIKE '%'+ @srchAttachmenttype + '%' OR @srchAttachmenttype='')
                    ) X
          
        ) Y
        INNER JOIN emailHistory ON emailHistory.numEmailHstrID = Y.numEmailHstrID
--        LEFT JOIN EmailHstrAttchDtls ON emailHistory.numEmailHstrID = EmailHstrAttchDtls.numEmailHstrID
        AND Y.ID > @firstRec AND Y.ID < @lastRec
--UNION
--   SELECT   0 AS ID,
--			COUNT(*),
--            NULL,
--            NULL,
--            NULL,
--            NULL,
--            NULL,
--            NULL,
--            NULL,
--            NULL,
--            NULL,
--            NULL,
--            NULL,
----            NULL,
----            NULL,
----            NULL,
--            1,
--            NULL,
--            NULL,
--            NULL,
--            NULL,
--            NULL,
--            NULL,
--            ''
--FROM      View_Inbox I INNER JOIN [EmailHistory] EH ON EH.numEmailHstrID = I.numEmailHstrID
--			LEFT JOIN EmailHstrAttchDtls A ON EH.numEmailHstrID = A.numEmailHstrID
-- WHERE     I.numDomainId = @numDomainId
--                                AND I.numUserCntId = @numUserCntId
--                                AND I.numNodeId = @numNodeId
--                                AND I.chrSource IN('B','I')
--                                AND (EH.vcBodyText LIKE '%' + @srchBody + '%' OR @srchBody = '')
--								AND (I.vcSubjectFromTo LIKE '%' + @vcSubjectFromTo + '%' OR @vcSubjectFromTo ='')
--                                AND (A.[vcAttachmentType] LIKE '%'+ @srchAttachmenttype + '%' OR @srchAttachmenttype='')
--   ORDER BY ID

RETURN 
*/
/*
declare @strSql as varchar(8000)                    
   if @columnName = 'FromName'               
 begin        
 set @columnName =  'dbo.GetEmaillName(emailHistory.numEmailHstrID,4)'        
 end        
 if @columnName = 'FromEmail'        
 begin        
 set @columnName = 'dbo.GetEmaillAdd(emailHistory.numEmailHstrID,4)'        
 end        
  if @columnName = 'ToName'               
 begin        
 set @columnName =  'dbo.GetEmaillName(emailHistory.numEmailHstrID,1)'        
 end        
 if @columnName = 'ToEmail'        
 begin        
 set @columnName = 'dbo.GetEmaillAdd(emailHistory.numEmailHstrID,1)'        
 end 
                  
set @strSql='With tblSubscriber AS (SELECT  distinct(emailHistory.numEmailHstrID) ,            
ROW_NUMBER() OVER (ORDER BY '+@columnName+' '+@columnSortOrder+') AS  RowNumber             
from emailHistory                                
--left join EmailHstrAttchDtls on emailHistory.numEmailHstrID=EmailHstrAttchDtls.numEmailHstrID                     
--join EmailHStrToBCCAndCC on emailHistory.numEmailHstrID=EmailHStrToBCCAndCC.numEmailHstrID                     
--join EmailMaster on        EmailMaster.numEmailId=EmailHStrToBCCAndCC.numEmailId                     
where numDomainId='+convert(varchar(15),@numDomainId)+' and numUserCntId='+convert(varchar(15),@numuserCntId)+  
' and numNodeId='+convert(varchar(15),@numNodeId)  
    
 if @chrSource<> '0'   set @strSql=@strSql +' and (chrSource  = ''B'' or chrSource like ''%'+@chrSource+'%'')'   
           
 if @srchBody <> '' set @strSql=@strSql +' and (vcSubject  like ''%'+@srchBody+'%'' or  
  vcBodyText like ''%'+@srchBody+'%''            
  or emailHistory.numEmailHstrID in (select numEmailHstrId from EmailHStrToBCCAndCC where             
 (numemailid in (select numEmailid from emailmaster where vcemailid like ''%'+@srchBody+'%'')) or   
  vcName like ''%'+@srchBody+'%'') )'                
--if @ToEmail <> '' set @strSql=@strSql +'               
--      and (EmailMaster.vcEmailId like '''+@ToEmail+''' and EmailHStrToBCCAndCC.tintType<>4 )  '            
if @srchAttachmenttype <>'' set @strSql=@strSql +'              
  and  emailHistory.numEmailHstrID in  (select numEmailHstrId from EmailHstrAttchDtls                
  where EmailHstrAttchDtls.vcAttachmentType like ''%'+@srchAttachmenttype+'%'')'            */
            
            
/*Please Do not use GetEmaillAdd function as it recursively fetched email address for single mail which create too bad performance*/                 

/*set @strSql=@strSql +')                     
 select RowNumber,            
--dbo.GetEmaillAdd(emailHistory.numEmailHstrID,4) as FromEmail,                  
dbo.GetEmaillName(emailHistory.numEmailHstrID,4) as FromName,                             
--isnull(vcFromEmail,'''') as FromEmail,                                      
--dbo.GetEmaillAdd(emailHistory.numEmailHstrID,1) as ToEmail,                                 
  
--dbo.GetEmaillAdd(emailHistory.numEmailHstrID,2) as CCEmail,                                
--dbo.GetEmaillAdd(emailHistory.numEmailHstrID,3) as BCCEmail,                                     
emailHistory.numEmailHstrID,                            
isnull(vcSubject,'''') as vcSubject,                                      
convert(varchar(max),vcBody) as vcBody,                              
dbo.FormatedDateFromDate( DATEADD(minute,'+ CONVERT(VARCHAR(15),-@ClientTimeZoneOffset)+',bintCreatedOn),numDomainId) as bintCreatedOn,                              
isnull(vcItemId,'''') as ItemId,                              
isnull(vcChangeKey,'''') as ChangeKey,                              
isnull(bitIsRead,''False'') as IsRead,                  
isnull(vcSize,0) as vcSize,                              
isnull(bitHasAttachments,''False'') as HasAttachments,                             
  isnull(EmailHstrAttchDtls.vcFileName,'''') as AttachmentPath,                            
isnull(EmailHstrAttchDtls.vcAttachmentType,'''') as AttachmentType,                      
isnull(EmailHstrAttchDtls.vcAttachmentItemId,'''') as AttachmentItemId,                             
dbo.FormatedDateFromDate(dtReceivedOn,numDomainId) as dtReceivedOn,    
dbo.GetEmaillName(emailHistory.numEmailHstrID,1) as ToName,                          
isnull(emailHistory.tintType,1) as type,                              
isnull(chrSource,''B'') as chrSource,                          
isnull(vcCategory,''white'') as vcCategory                   
 from tblSubscriber T                  
 join emailHistory                   
 on emailHistory.numEmailHstrID=T.numEmailHstrID             
left join EmailHstrAttchDtls on emailHistory.numEmailHstrID=EmailHstrAttchDtls.numEmailHstrID             
where numDomainId='+convert(varchar(15),@numDomainId)+' and numUserCntId='+convert(varchar(15),@numuserCntId)+  
' and numNodeId='+convert(varchar(15),@numNodeId)+' and                
  RowNumber > '+convert(varchar(10),@firstRec)+' and RowNumber < '+convert(varchar(10),@lastRec)+'              
union                 
 select 0 as RowNumber,null,count(*),null,null,            
null,null,null,null,null,null,null,null,null,NULL,           
null,1,null,null from tblSubscriber  order by RowNumber'            
--print    @strSql             
exec (@strSql)
GO
*/               

--exec USP_INVENTORYREPORT @numDomainID=72,@numUserCntID=1,@numDateRange=19,@numBasedOn=11,@numItemType=21,@numItemClass=22,@numItemClassId=0,@DateCondition=' OppDate between ''01/01/2008'' and ''11/23/2008'''
-- select * from financialyear
-- exec USP_InventoryReport @numDomainID=72,@numUserCntID=17,@numDateRange=19,@numBasedOn=9,@numItemType=21,@numItemClass=22,@numItemClassId=0,@DateCondition=' oppDate between ''01/01/2009 '' And '' 01/18/2010''',@numWareHouseId=0
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_INVENTORYREPORT')
DROP PROCEDURE USP_INVENTORYREPORT
GO
CREATE PROCEDURE [dbo].[USP_INVENTORYREPORT]
(@numDomainID AS NUMERIC(9),
@numUserCntID AS NUMERIC(9),
@numDateRange AS NUMERIC(9),
@numBasedOn AS NUMERIC(9),
@numItemType AS NUMERIC(9),
@numItemClass AS NUMERIC(9),
@numItemClassId AS NUMERIC(9),
@DateCondition AS VARCHAR(100),
@numWareHouseId AS NUMERIC,
@dtFromDate AS DATE,
@dtToDate AS DATE,
@CurrentPage INT = 0,                                                            
@PageSize INT,
@TotRecs NUMERIC(18,0) OUTPUT)
AS 
BEGIN

DELETE FROM InventroyReportDTL WHERE numUserID=@numUserCntID;

INSERT INTO InventroyReportDTL SELECT @numDomainID,@numUserCntID,@numDateRange,0;
INSERT INTO InventroyReportDTL SELECT @numDomainID,@numUserCntID,@numBasedOn,0; 
INSERT INTO InventroyReportDTL SELECT @numDomainID,@numUserCntID,@numItemType,0;
INSERT INTO InventroyReportDTL SELECT @numDomainID,@numUserCntID,@numItemClass,@numItemClassId;

DECLARE @FilterItemType VARCHAR(MAX);
DECLARE @FilterBasedOn VARCHAR(MAX);
DECLARE @FilterDateRange VARCHAR(MAX);

DECLARE @strSql VARCHAR(MAX);
DECLARE @strGroup VARCHAR(MAX);
DECLARE @ItemClassID VARCHAR(100);
DECLARE @strSort VARCHAR(MAX);

SELECT @FilterItemType= vcCondition from InventoryReportMaster a inner join InventroyReportDTL b ON 
a.numReportID=b.numReportID and b.numUserID=@numUserCntID and a.numTypeID=1;

SELECT @FilterBasedOn= vcCondition from InventoryReportMaster a inner join InventroyReportDTL b ON 
a.numReportID=b.numReportID and b.numUserID=@numUserCntID and a.numTypeID=2;

SELECT @FilterDateRange = vcDesc from InventoryReportMaster a inner join InventroyReportDTL b ON
a.numReportID=b.numReportID and b.numUserID=@numUserCntID and a.numTypeID=3;

SELECT @ItemClassID = (CASE numItemClassification WHEN 0 THEN '' ELSE ' AND a.numItemClassification=' + CAST(numItemClassification AS VARCHAR(50))  END) FROM  InventroyReportDTL WHERE numUserID = @numUserCntID 

PRINT '@FilterItemType:' + @FilterItemType
PRINT '@FilterBasedOn:' + @FilterBasedOn
PRINT '@FilterDateRange:' + @FilterDateRange
PRINT '@ItemClassID: ' + @ItemClassID

CREATE TABLE #ItemWareHouse 
(RowID NUMERIC(18,0),
numItemCode NUMERIC(18),
vcItemName VARCHAR(2000),
vcModelID VARCHAR(150),
ItemClass VARCHAR(150),
ItemDesc VARCHAR(MAX),
numOnHand NUMERIC(18,2),
AVGCOST MONEY,
numAllocation NUMERIC(18,2),
Amount money,
SalesUnit numeric(18,2),
ReturnQty numeric(18,2),
ReturnPer numeric(18,2),
COGS money,
PROFIT money,
SALESRETURN money);

DECLARE @strItemSQL AS NVARCHAR(MAX)
CREATE TABLE #tmpItems(RowID NUMERIC(18,0),numItemCode NUMERIC(18,0))


SET @strItemSQL = ' SELECT ROW_NUMBER() OVER (ORDER BY [numItemCode]) AS [RowID] ,[numItemCode] FROM Item a WHERE a.[numDomainID] = ' + CONVERT(VARCHAR(10),@numDomainID) + ' 
AND 1=1 ' + @FilterItemType + ' ' + @ItemClassID

PRINT @strItemSQL
INSERT INTO [#tmpItems]( [RowID], [numItemCode] )
EXEC (@strItemSQL);

SET @strSql='SELECT DISTINCT RowID, A.numItemCode,A.vcItemName,a.vcModelID,A.numItemClass,isnull(txtItemDesc,'''') AS ItemDesc,
(CASE WHEN CONVERT(DATE,GETDATE()) = CONVERT(DATE, ''' + CONVERT(VARCHAR(50),@dtToDate) + ''') THEN SUM(ISNULL(c.[numOnHand],0))
	 ELSE
			(SELECT TOP 1 [WHIT].[numOnHand] 
					FROM [WareHouseItems_Tracking] WHIT 
					INNER JOIN WareHouseItems ON WHIT.numWareHouseItemID = WareHouseItems.numWareHouseItemID
					WHERE WareHouseItems.numWareHouseID =  (CASE ' + CAST(@numWareHouseId as varchar(10)) + '  WHEN 0 THEN WareHouseItems.numWareHouseID ELSE '  + cast(@numWareHouseId as varchar(10)) + ' END) 
					AND [WHIT].[dtCreatedDate] BETWEEN CONVERT(DATE, ''' + CONVERT(VARCHAR(50),@dtFromDate) + ''') And CONVERT(DATE, + ''' + CONVERT(VARCHAR(50),@dtToDate) + ''') 
					ORDER BY [WHIT].[numWareHouseItemTrackingID] DESC ) 
END) [numOnHand], 
(CASE WHEN CONVERT(DATE,GETDATE()) = CONVERT(DATE, ''' + CONVERT(VARCHAR(50),@dtToDate) + ''') THEN ISNULL(A.[monAverageCost],0)
	 ELSE
			(SELECT TOP 1 [WHIT].[monAverageCost] 
					FROM [WareHouseItems_Tracking] WHIT 
					INNER JOIN WareHouseItems ON WHIT.numWareHouseItemID = WareHouseItems.numWareHouseItemID
					WHERE WareHouseItems.numWareHouseID =  (CASE ' + CAST(@numWareHouseId as varchar(10)) + '  WHEN 0 THEN WareHouseItems.numWareHouseID ELSE '  + cast(@numWareHouseId as varchar(10)) + ' END) 
					AND [WHIT].[dtCreatedDate] BETWEEN CONVERT(DATE, ''' + CONVERT(VARCHAR(50),@dtFromDate) + ''') And CONVERT(DATE, + ''' + CONVERT(VARCHAR(50),@dtToDate) + ''')
					ORDER BY [WHIT].[numWareHouseItemTrackingID] DESC ) 
END) [AVGCOST],
(CASE WHEN CONVERT(DATE,GETDATE()) = CONVERT(DATE, ''' + CONVERT(VARCHAR(50),@dtFromDate) + ''') THEN SUM(ISNULL(c.[numAllocation],0))
	 ELSE
			(SELECT TOP 1 [WHIT].[numAllocation] 
					FROM [WareHouseItems_Tracking] WHIT 
					INNER JOIN WareHouseItems ON WHIT.numWareHouseItemID = WareHouseItems.numWareHouseItemID
					WHERE WareHouseItems.numWareHouseID =  (CASE ' + CAST(@numWareHouseId as varchar(10)) + '  WHEN 0 THEN WareHouseItems.numWareHouseID ELSE '  + cast(@numWareHouseId as varchar(10)) + ' END) 
					AND [WHIT].[dtCreatedDate] BETWEEN CONVERT(DATE, ''' + CONVERT(VARCHAR(50),@dtFromDate) + ''') And CONVERT(DATE, + ''' + CONVERT(VARCHAR(50),@dtToDate) + ''') 
					ORDER BY [WHIT].[numWareHouseItemTrackingID] DESC ) 
END) [numAllocation],
SUM(COGS) as Amount,
sum(SalesUnit) as SalesUnit,
sum(ReturnQty) as ReturnQty,
isnull((sum(ReturnQty)/NULLIF(sum(SalesUnit),0)*100),0) as ReturnPer,
SUM(COGS) AS COGS,
SUM(PROFIT) AS PROFIT,
SUM(SALESRETURN) AS SALESRETURN
FROM Item A
JOIN #tmpItems ON #tmpItems.numItemCode = A.numItemCode 
JOIN WareHouseItems C ON C.numItemID = A.numItemCode AND numWareHouseID = (CASE ' + CAST(@numWareHouseId as varchar(10)) + '  WHEN 0 THEN numWareHouseID ELSE '  + cast(@numWareHouseId as varchar(10)) + ' END) ' + @FilterItemType  +  @ItemClassID + '
LEFT JOIN 
	VIEW_INVENTORYOPPSALES B 
ON  
	b.numItemCode = A.numItemCode
	AND OppDate BETWEEN CONVERT(DATE, ''' + CONVERT(VARCHAR(50),@dtFromDate) + ''') And CONVERT(DATE, + ''' + CONVERT(VARCHAR(50),@dtToDate) + ''')
INNER JOIN 
	OpportunityBizDocItems
ON
	B.numOppBizDocItemID = OpportunityBizDocItems.numOppBizDocItemID
	AND OpportunityBizDocItems.numWarehouseItmsID = C.numWareHouseItemID
WHERE 1=1 AND A.[numDomainID] = ' + CONVERT(VARCHAR(10),@numDomainID)  

IF CONVERT(DATE,GETDATE()) <> CONVERT(DATE,@dtToDate)
	BEGIN
		SET @strsql = @strsql + ' AND CONVERT(DATE, bintModifiedDate) >= CONVERT(DATE, + ''' + CONVERT(VARCHAR(50),@dtFromDate) + ''') AND CONVERT(DATE, bintModifiedDate) <= CONVERT(DATE, + ''' + CONVERT(VARCHAR(50),@dtToDate) + ''')'
	END

DECLARE @firstRec AS INTEGER                                                            
DECLARE @lastRec AS INTEGER                                                            
SET @firstRec = ( @CurrentPage - 1 ) * @PageSize                                                            
SET @lastRec = ( @CurrentPage * @PageSize + 1 ) 

IF @CurrentPage > 0 
BEGIN
	SET @strsql = @strsql + ' AND RowID > ' + CONVERT(VARCHAR(10),@firstRec)+ ' AND RowID < '+ CONVERT(VARCHAR(10),@lastRec) + ' '
END

set @strGroup = ' GROUP BY A.numItemCode,A.vcItemName,a.vcModelID,A.numItemClass,ISNULL(txtItemDesc,''''),A.[monAverageCost], RowID '

SET @TotRecs = (SELECT COUNT(*) FROM [#tmpItems])
PRINT @TotRecs

--PRINT @DateCondition
--IF LEN(@DateCondition) > 0
--	SET @strsql = @strsql +' AND '+ @DateCondition + ' '+ @strGroup
--ELSE
--	SET @strsql = @strsql + ' ' + @strGroup + ';'

SET @strsql = @strsql + ' ' + @strGroup 
--SET @strsql = @strsql + ' ORDER BY ' + @FilterBasedOn + ' DESC '

PRINT @strSql
INSERT INTO #ItemWareHouse 
EXEC (@strSql);

--SELECT * FROM [#ItemWareHouse] AS IWH

--DECLARE @firstRec AS INTEGER                                                            
--DECLARE @lastRec AS INTEGER                                                            
--SET @firstRec = ( @CurrentPage - 1 ) * @PageSize                                                            
--SET @lastRec = ( @CurrentPage * @PageSize + 1 ) 

IF NOT EXISTS(SELECT * FROM #ItemWareHouse)
BEGIN
	SET @TotRecs = 0
END

SET @strSort = ' SELECT *, (ISNULL(numOnHand,0) + ISNULL(numAllocation,0)) [TotalInventory],  ((ISNULL(numOnHand,0) + ISNULL(numAllocation,0)) * AVGCOST) AS [InventoryValue] FROM #ItemWareHouse ORDER BY ' + @FilterBasedOn + ' DESC '

--WHERE 1=1 AND RowID > ' + CONVERT(VARCHAR(10),@firstRec)+ ' AND RowID < '+ CONVERT(VARCHAR(10),@lastRec) + ' 


--CREATE TABLE #InventoryReport 
--(RowID NUMERIC(18,0),
--numItemCode NUMERIC(18),
--vcItemName VARCHAR(2000),
--vcModelID VARCHAR(150),
--ItemClass VARCHAR(150),
--ItemDesc VARCHAR(MAX),
--numOnHand NUMERIC(18,2),
--AVGCOST MONEY,
--TotalInventory NUMERIC(18,0),
--InventoryValue MONEY,
--numAllocation NUMERIC(18,2));

PRINT @strSort
--INSERT INTO #InventoryReport 

EXEC (@strSort);

--SELECT * FROM #InventoryReport -- WHERE RowID > @firstRec AND RowID < @lastRec

--EXEC ('DROP TABLE #ItemWareHouse')
DROP TABLE #tmpItems
DROP TABLE #ItemWareHouse

END
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_Item_SearchForSalesOrder' )
    DROP PROCEDURE USP_Item_SearchForSalesOrder
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Sandeep Patel
-- Create date: 3 March 2014
-- Description:	Search items for sales order
-- =============================================
CREATE PROCEDURE USP_Item_SearchForSalesOrder
    @tintOppType AS TINYINT ,
    @numDomainID AS NUMERIC(9) ,
    @numDivisionID AS NUMERIC(9) = 0 ,
    @str AS VARCHAR(1000) ,
    @numUserCntID AS NUMERIC(9) ,
    @tintSearchOrderCustomerHistory AS TINYINT = 0 ,
    @numPageIndex AS INT ,
    @numPageSize AS INT ,
    @WarehouseID AS NUMERIC(18, 0) = NULL ,
    @TotalCount AS INT OUTPUT
AS
    BEGIN
  SET NOCOUNT ON;

DECLARE @strNewQuery NVARCHAR(MAX)
DECLARE @strSQL AS NVARCHAR(MAX)
SELECT  @str = REPLACE(@str, '''', '''''')

DECLARE @TableRowCount TABLE ( Value INT );
	
SELECT  *
INTO    #Temp1
FROM    ( SELECT    numFieldId ,
                    vcDbColumnName ,
                    ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                    tintRow AS tintOrder ,
                    0 AS Custom
          FROM      View_DynamicColumns
          WHERE     numFormId = 22
                    AND numUserCntID = @numUserCntID
                    AND numDomainID = @numDomainID
                    AND tintPageType = 1
                    AND bitCustom = 0
                    AND ISNULL(bitSettingField, 0) = 1
                    AND numRelCntType = 0
          UNION
          SELECT    numFieldId ,
                    vcFieldName ,
                    vcFieldName ,
                    tintRow AS tintOrder ,
                    1 AS Custom
          FROM      View_DynamicCustomColumns
          WHERE     Grp_id = 5
                    AND numFormId = 22
                    AND numUserCntID = @numUserCntID
                    AND numDomainID = @numDomainID
                    AND tintPageType = 1
                    AND bitCustom = 1
                    AND numRelCntType = 0
        ) X 
  
IF NOT EXISTS ( SELECT  *
                FROM    #Temp1 )
    BEGIN
        INSERT  INTO #Temp1
                SELECT  numFieldId ,
                        vcDbColumnName ,
                        ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                        tintorder ,
                        0
                FROM    View_DynamicDefaultColumns
                WHERE   numFormId = 22
                        AND bitDefault = 1
                        AND ISNULL(bitSettingField, 0) = 1
                        AND numDomainID = @numDomainID 
    END

CREATE TABLE #tempItemCode ( numItemCode NUMERIC(9) )
DECLARE @bitRemoveVendorPOValidation AS BIT
SELECT  
	@bitRemoveVendorPOValidation = ISNULL(bitRemoveVendorPOValidation,0)
FROM    
	domain
WHERE  
	numDomainID = @numDomainID

IF @tintOppType > 0
    BEGIN 
        IF CHARINDEX(',', @str) > 0
        BEGIN
			DECLARE @itemSearchText VARCHAR(100)
            DECLARE @vcAttribureSearch VARCHAR(MAX)
			DECLARE @vcAttribureSearchCondition VARCHAR(MAX)

			SET @itemSearchText = LTRIM(RTRIM(SUBSTRING(@str,0,CHARINDEX(',',@str))))

			--GENERATES ATTRIBUTE SEARCH CONDITION
			SET @vcAttribureSearch = SUBSTRING(@str,CHARINDEX(',',@str) + 1,LEN(@str))
	
			DECLARE @attributeSearchText VARCHAR(MAX)
			DECLARE @TempTable TABLE (vcValue VARCHAR(MAX))

			WHILE LEN(@vcAttribureSearch) > 0
			BEGIN
				SET @attributeSearchText = LEFT(@vcAttribureSearch, 
										ISNULL(NULLIF(CHARINDEX(',', @vcAttribureSearch) - 1, -1),
										LEN(@vcAttribureSearch)))
				SET @vcAttribureSearch = SUBSTRING(@vcAttribureSearch,
												ISNULL(NULLIF(CHARINDEX(',', @vcAttribureSearch), 0),
												LEN(@vcAttribureSearch)) + 1, LEN(@vcAttribureSearch))

				INSERT INTO @TempTable (vcValue) VALUES ( @attributeSearchText )
			END
		
			--REMOVES WHITE SPACES
			UPDATE
				@TempTable
			SET
				vcValue = LTRIM(RTRIM(vcValue))
	
			--CONVERTING TABLE TO COMMA SEPERATED STRING AFTER REMOVEING WHITE SPACES
			SELECT @vcAttribureSearch = COALESCE(@vcAttribureSearch,'') +  CONVERT(VARCHAR(MAX),vcValue) + ',' FROM @TempTable
	
			--REMOVE LAST COMMA FROM FINAL SEARCH STRING
			SET @vcAttribureSearch = LTRIM(RTRIM(@vcAttribureSearch))
			IF DATALENGTH(@vcAttribureSearch) > 0
				SET @vcAttribureSearch = LEFT(@vcAttribureSearch, LEN(@vcAttribureSearch) - 1)
	
			--START: GENERATES ATTRIBUTE SEARCH CONDITION
			SELECT 	@vcAttribureSearchCondition = '(TEMPMATRIX.Attributes LIKE ''%' + REPLACE(@vcAttribureSearch, ',', '%'' AND TEMPMATRIX.Attributes LIKE ''%') + '%'')' 

			--LOGIC FOR GENERATING FINAL SQL STRING
			IF @tintOppType = 1 OR ( @bitRemoveVendorPOValidation = 1 AND @tintOppType = 2)
                BEGIN      

				SET @strSQL = 'SELECT
									I.numItemCode,
									I.numVendorID,
									MIN(WHT.numWareHouseItemID) as numWareHouseItemID,
									dbo.fn_GetAttributes(WHT.numWareHouseItemId,I.bitSerialized) AS [Attributes]
								FROM
									dbo.Item I
								LEFT JOIN
									dbo.WareHouseItems WHT
								ON
									I.numItemCode = WHT.numItemID
								WHERE 
									((ISNULL(I.numItemGroup,0) > 0 AND I.vcItemName LIKE ''%' + @itemSearchText + '%'') OR (ISNULL(I.numItemGroup,0) = 0 AND I.vcItemName LIKE ''%' + @str + '%''))
									AND ISNULL(I.Isarchieve, 0) <> 1
									AND I.charItemType NOT IN ( ''A'' )
									AND I.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + '
									AND (WHT.numWareHouseID = ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0)'

				--- added Asset validation  by sojan
                IF @bitRemoveVendorPOValidation = 1
                    AND @tintOppType = 2
                    SET @strSQL = @strSQL
                        + ' and ISNULL(I.bitKitParent,0) = 0 AND ISNULL(I.bitAssembly,0) = 0 '

                IF @tintSearchOrderCustomerHistory = 1 AND @numDivisionID > 0
                BEGIN
                    SET @strSQL = @strSQL
                        + ' and I.numItemCode in (select distinct oppI.numItemCode from 
						OpportunityMaster oppM join OpportunityItems oppI on oppM.numOppId=oppI.numOppId where oppM.numDomainID='
                        + CONVERT(VARCHAR(20), @numDomainID)
                        + ' and oppM.numDivisionId='
                        + CONVERT(VARCHAR(20), @numDivisionId)
                        + ')'
                END

				SET @strSQL = @strSQL + ' GROUP BY
										I.numItemCode,
										I.numVendorID,
										dbo.fn_GetAttributes(numWareHouseItemId,I.bitSerialized)'

				SET @strSQL = 
				'
				SELECT    
					SRNO=ROW_NUMBER() OVER( ORDER BY  I.vcItemName),
					I.numItemCode,
					(SELECT TOP 1 vcPathForImage FROM ItemImages WHERE numItemCode = I.numItemCode) AS vcPathForImage ,
					(SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = I.numItemCode) AS vcPathForTImage ,
					ISNULL(vcItemName, '''') vcItemName ,
					CASE WHEN I.[charItemType] = ''P''
					THEN CONVERT(VARCHAR(100), ISNULL((SELECT TOP 1 ROUND(monWListPrice,2) FROM [WareHouseItems] WHERE [numItemID] = I.numItemCode), 0))
					ELSE CONVERT(VARCHAR(100), ROUND(monListPrice, 2))
					END AS monListPrice,
					ISNULL(vcSKU, '''') vcSKU ,
					ISNULL(numBarCodeId, 0) numBarCodeId ,
					ISNULL(vcModelID, '''') vcModelID ,
					ISNULL(txtItemDesc, '''') AS txtItemDesc ,
					ISNULL(C.vcCompanyName, '''') AS vcCompanyName,
					TEMPMATRIX.numWareHouseItemID,
					TEMPMATRIX.Attributes AS [vcAttributes]
				FROM  
					(' + @strSQL +') AS TEMPMATRIX
				INNER JOIN    
					Item I
				ON
					TEMPMATRIX.numItemCode = I.numItemCode
				LEFT JOIN 
					DivisionMaster D 
				ON 
					I.numVendorID = D.numDivisionID
				LEFT JOIN 
					CompanyInfo C 
				ON 
					C.numCompanyID = D.numCompanyID
				WHERE  
				  ((ISNULL(I.numItemGroup,0) = 0 AND I.vcItemName LIKE ''%' + @str + '%'') OR ' + @vcAttribureSearchCondition + ')'

				

                INSERT  INTO @TableRowCount
                        EXEC
                            ( 'SELECT COUNT(*) FROM ( ' + @strSQL
                                + ') as t2'
                                    );

                SELECT  @TotalCount = Value
                FROM    @TableRowCount;

                SET @strNewQuery = 'select * from (' + @strSQL
                    + ') as t where SRNO> '
                    + CAST(( ( @numPageIndex - 1 ) * @numPageSize ) AS VARCHAR(10))
                    + ' and SRNO < '
                    + CAST(( ( @numPageIndex * @numPageSize ) + 1 ) AS VARCHAR(10))
                    + ' order by  vcItemName'

				--PRINT @strNewQuery
                EXEC(@strNewQuery)
				END
			ELSE
				BEGIN

				SET @strSQL = 'SELECT
									I.numItemCode,
									I.numVendorID,
									MIN(WHT.numWareHouseItemID) as numWareHouseItemID,
									dbo.fn_GetAttributes(WHT.numWareHouseItemId,I.bitSerialized) AS [Attributes]
								FROM
									dbo.Item I
								INNER JOIN
									dbo.Vendor V
								ON
									V.numItemCode = I.numItemCode
								LEFT JOIN
									dbo.WareHouseItems WHT
								ON
									I.numItemCode = WHT.numItemID
								WHERE 
									((ISNULL(I.numItemGroup,0) > 0 AND I.vcItemName LIKE ''%' + @itemSearchText + '%'') OR (ISNULL(I.numItemGroup,0) = 0 AND I.vcItemName LIKE ''%' + @str + '%''))
									AND ISNULL(I.Isarchieve, 0) <> 1
									AND I.charItemType NOT IN ( ''A'' )
									AND I.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + '
									AND (WHT.numWareHouseID = ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0)'

				--- added Asset validation  by sojan
                IF @tintSearchOrderCustomerHistory = 1 AND @numDivisionID > 0
                BEGIN
                    SET @strSQL = @strSQL
                        + ' and I.numItemCode in (select distinct oppI.numItemCode from 
						OpportunityMaster oppM join OpportunityItems oppI on oppM.numOppId=oppI.numOppId where oppM.numDomainID='
                        + CONVERT(VARCHAR(20), @numDomainID)
                        + ' and oppM.numDivisionId='
                        + CONVERT(VARCHAR(20), @numDivisionId)
                        + ')'
                END

				SET @strSQL = @strSQL + ' GROUP BY
										I.numItemCode,
										I.numVendorID,
										dbo.fn_GetAttributes(numWareHouseItemId,I.bitSerialized)'

				SET @strSQL = 
				'
				SELECT    
					SRNO=ROW_NUMBER() OVER( ORDER BY  I.vcItemName),
					I.numItemCode,
					(SELECT TOP 1 vcPathForImage FROM ItemImages WHERE numItemCode = I.numItemCode) AS vcPathForImage ,
					(SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = I.numItemCode) AS vcPathForTImage ,
					ISNULL(vcItemName, '''') vcItemName ,
					CASE WHEN I.[charItemType] = ''P''
					THEN CONVERT(VARCHAR(100), ISNULL((SELECT TOP 1 ROUND(monWListPrice,2) FROM [WareHouseItems] WHERE [numItemID] = I.numItemCode), 0))
					ELSE CONVERT(VARCHAR(100), ROUND(monListPrice, 2))
					END AS monListPrice,
					ISNULL(vcSKU, '''') vcSKU ,
					ISNULL(numBarCodeId, 0) numBarCodeId ,
					ISNULL(vcModelID, '''') vcModelID ,
					ISNULL(txtItemDesc, '''') AS txtItemDesc ,
					ISNULL(C.vcCompanyName, '''') AS vcCompanyName,
					TEMPMATRIX.numWareHouseItemID,
					TEMPMATRIX.Attributes AS [vcAttributes]
				FROM  
					(' + @strSQL +') AS TEMPMATRIX
				INNER JOIN    
					Item I
				ON
					TEMPMATRIX.numItemCode = I.numItemCode
				LEFT JOIN 
					DivisionMaster D 
				ON 
					I.numVendorID = D.numDivisionID
				LEFT JOIN 
					CompanyInfo C 
				ON 
					C.numCompanyID = D.numCompanyID
				WHERE  
				 ((ISNULL(I.numItemGroup,0) = 0 AND I.vcItemName LIKE ''%' + @str + '%'') OR ' + @vcAttribureSearchCondition + ')'

                INSERT  INTO @TableRowCount
                        EXEC
                            ( 'SELECT COUNT(*) FROM ( ' + @strSQL
                                + ') as t2'
                                    );

                SELECT  @TotalCount = Value
                FROM    @TableRowCount;

                SET @strNewQuery = 'select * from (' + @strSQL
                    + ') as t where SRNO> '
                    + CAST(( ( @numPageIndex - 1 ) * @numPageSize ) AS VARCHAR(10))
                    + ' and SRNO < '
                    + CAST(( ( @numPageIndex * @numPageSize ) + 1 ) AS VARCHAR(10))
                    + ' order by  vcItemName'

				--PRINT @strNewQuery
                EXEC(@strNewQuery)
			END	
        END
        ELSE
        BEGIN
				DECLARE @vcWareHouseSearch VARCHAR(1000)
				DECLARE @vcVendorSearch VARCHAR(1000)
               

				SET @vcWareHouseSearch = ''
				SET @vcVendorSearch = ''

                DECLARE @tintOrder AS INT
                DECLARE @Fld_id AS NVARCHAR(20)
                DECLARE @Fld_Name AS VARCHAR(20)
                SET @strSQL = ''
                SELECT TOP 1
                        @tintOrder = tintOrder + 1 ,
                        @Fld_id = numFieldId ,
                        @Fld_Name = vcFieldName
                FROM    #Temp1
                WHERE   Custom = 1
                ORDER BY tintOrder
                WHILE @tintOrder > 0
                    BEGIN
                        SET @strSQL = @strSQL + ', dbo.GetCustFldValueItem('
                            + @Fld_id + ', I.numItemCode) as [' + @Fld_Name
                            + ']'

                        SELECT TOP 1
                                @tintOrder = tintOrder + 1 ,
                                @Fld_id = numFieldId ,
                                @Fld_Name = vcFieldName
                        FROM    #Temp1
                        WHERE   Custom = 1
                                AND tintOrder >= @tintOrder
                        ORDER BY tintOrder
                        IF @@rowcount = 0
                            SET @tintOrder = 0
                    END


				--Temp table for Item Search Configuration

                SELECT  *
                INTO    #tempSearch
                FROM    ( SELECT    numFieldId ,
                                    vcDbColumnName ,
                                    ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                                    tintRow AS tintOrder ,
                                    0 AS Custom
                          FROM      View_DynamicColumns
                          WHERE     numFormId = 22
                                    AND numUserCntID = @numUserCntID
                                    AND numDomainID = @numDomainID
                                    AND tintPageType = 1
                                    AND bitCustom = 0
                                    AND ISNULL(bitSettingField, 0) = 1
                                    AND numRelCntType = 1
                          UNION
                          SELECT    numFieldId ,
                                    vcFieldName ,
                                    vcFieldName ,
                                    tintRow AS tintOrder ,
                                    1 AS Custom
                          FROM      View_DynamicCustomColumns
                          WHERE     Grp_id = 5
                                    AND numFormId = 22
                                    AND numUserCntID = @numUserCntID
                                    AND numDomainID = @numDomainID
                                    AND tintPageType = 1
                                    AND bitCustom = 1
                                    AND numRelCntType = 1
                        ) X 
  
                IF NOT EXISTS ( SELECT  *
                                FROM    #tempSearch )
                    BEGIN
                        INSERT  INTO #tempSearch
                                SELECT  numFieldId ,
                                        vcDbColumnName ,
                                        ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                                        tintorder ,
                                        0
                                FROM    View_DynamicDefaultColumns
                                WHERE   numFormId = 22
                                        AND bitDefault = 1
                                        AND ISNULL(bitSettingField, 0) = 1
                                        AND numDomainID = @numDomainID 
                    END

				--Regular Search
                DECLARE @strSearch AS VARCHAR(8000) ,
                    @CustomSearch AS VARCHAR(4000) ,
                    @numFieldId AS NUMERIC(9)
                SET @strSearch = ''
                SELECT TOP 1
                        @tintOrder = tintOrder + 1 ,
                        @Fld_Name = vcDbColumnName ,
                        @numFieldId = numFieldId
                FROM    #tempSearch
                WHERE   Custom = 0
                ORDER BY tintOrder
                WHILE @tintOrder > -1
                    BEGIN
                        IF @Fld_Name = 'vcPartNo'
						BEGIN
                            INSERT  INTO #tempItemCode
                                    SELECT DISTINCT
                                            Item.numItemCode
                                    FROM    Vendor
                                            JOIN Item ON Item.numItemCode = Vendor.numItemCode
                                    WHERE   Item.numDomainID = @numDomainID
                                            AND Vendor.numDomainID = @numDomainID
                                            AND Vendor.vcPartNo IS NOT NULL
                                            AND LEN(Vendor.vcPartNo) > 0
                                            AND vcPartNo LIKE '%' + @str + '%'
							
							SET @vcVendorSearch = 'dbo.Vendor.vcPartNo LIKE ''%' + @str + '%'''
							
						END
                        ELSE IF @Fld_Name = 'vcBarCode'
						BEGIN
                            INSERT  INTO #tempItemCode
                                    SELECT DISTINCT
                                            Item.numItemCode
                                    FROM    Item
                                            JOIN WareHouseItems WI ON WI.numDomainID = @numDomainID
                                                            AND Item.numItemCode = WI.numItemID
                                            JOIN Warehouses W ON W.numDomainID = @numDomainID
                                                            AND W.numWareHouseID = WI.numWareHouseID
                                    WHERE   Item.numDomainID = @numDomainID
                                            AND ISNULL(Item.numItemGroup,
                                                        0) > 0
                                            AND WI.vcBarCode IS NOT NULL
                                            AND LEN(WI.vcBarCode) > 0
                                            AND WI.vcBarCode LIKE '%' + @str + '%'

							SET @vcWareHouseSearch = 'dbo.WareHouseItems.vcBarCode LIKE ''%' + @str + '%'''
						END
                        ELSE IF @Fld_Name = 'vcWHSKU'
						BEGIN
                            INSERT  INTO #tempItemCode
                                    SELECT DISTINCT
                                            Item.numItemCode
                                    FROM    Item
                                            JOIN WareHouseItems WI ON WI.numDomainID = @numDomainID
                                                        AND Item.numItemCode = WI.numItemID
                                            JOIN Warehouses W ON W.numDomainID = @numDomainID
                                                        AND W.numWareHouseID = WI.numWareHouseID
                                    WHERE   Item.numDomainID = @numDomainID
                                            AND ISNULL(Item.numItemGroup,
                                                        0) > 0
                                            AND WI.vcWHSKU IS NOT NULL
                                            AND LEN(WI.vcWHSKU) > 0
                                            AND WI.vcWHSKU LIKE '%'
                                            + @str + '%'

							IF LEN(@vcWareHouseSearch) > 0
								SET @vcWareHouseSearch = @vcWareHouseSearch + ' OR dbo.WareHouseItems.vcWHSKU LIKE ''%' + @str + '%'''
							ELSE
								SET @vcWareHouseSearch = 'dbo.WareHouseItems.vcWHSKU LIKE ''%' + @str + '%'''
						END
                        ELSE
                            SET @strSearch = @strSearch
                                + (CASE @Fld_Name WHEN 'numItemCode' THEN ' I.[' + @Fld_Name ELSE ' [' + @Fld_Name END)  + '] LIKE ''%' + @str
                                + '%'''

                        SELECT TOP 1
                                @tintOrder = tintOrder + 1 ,
                                @Fld_Name = vcDbColumnName ,
                                @numFieldId = numFieldId
                        FROM    #tempSearch
                        WHERE   tintOrder >= @tintOrder
                                AND Custom = 0
                        ORDER BY tintOrder
                        IF @@rowcount = 0
                            BEGIN
                                SET @tintOrder = -1
                            END
                        ELSE
                            IF @Fld_Name != 'vcPartNo'
                                AND @Fld_Name != 'vcBarCode'
                                AND @Fld_Name != 'vcWHSKU'
                                BEGIN
                                    SET @strSearch = @strSearch + ' or '
                                END
                    END

                IF ( SELECT COUNT(*)
                     FROM   #tempItemCode
                   ) > 0
                    SET @strSearch = @strSearch
                        + ' or  I.numItemCode in (select numItemCode from #tempItemCode)' 

				--Custom Search
                SET @CustomSearch = ''
                SELECT TOP 1
                        @tintOrder = tintOrder + 1 ,
                        @Fld_Name = vcDbColumnName ,
                        @numFieldId = numFieldId
                FROM    #tempSearch
                WHERE   Custom = 1
                ORDER BY tintOrder
                WHILE @tintOrder > -1
                    BEGIN
    
                        SET @CustomSearch = @CustomSearch
                            + CAST(@numFieldId AS VARCHAR(10)) 

                        SELECT TOP 1
                                @tintOrder = tintOrder + 1 ,
                                @Fld_Name = vcDbColumnName ,
                                @numFieldId = numFieldId
                        FROM    #tempSearch
                        WHERE   tintOrder >= @tintOrder
                                AND Custom = 1
                        ORDER BY tintOrder
                        IF @@rowcount = 0
                            BEGIN
                                SET @tintOrder = -1
                            END
                        ELSE
                            BEGIN
                                SET @CustomSearch = @CustomSearch + ' , '
                            END
                    END
	
                IF LEN(@CustomSearch) > 0
                    BEGIN
                        SET @CustomSearch = ' I.numItemCode in (SELECT RecId FROM dbo.CFW_FLD_Values_Item WHERE Fld_ID IN ('
                            + @CustomSearch + ') and Fld_Value like ''%'
                            + @str + '%'')'

                        IF LEN(@strSearch) > 0
                            SET @strSearch = @strSearch + ' OR '
                                + @CustomSearch
                        ELSE
                            SET @strSearch = @CustomSearch  
                    END


                IF @tintOppType = 1
                    OR ( @bitRemoveVendorPOValidation = 1
                         AND @tintOppType = 2
                       )
                    BEGIN      
                        SET @strSQL = 'select SRNO=ROW_NUMBER() OVER( ORDER BY  I.vcItemName), I.numItemCode,
									  (SELECT TOP 1 vcPathForImage from ItemImages WHERE numItemCode = I.numItemCode) as vcPathForImage,
									  (SELECT TOP 1 vcPathForTImage from ItemImages WHERE numItemCode = I.numItemCode) as vcPathForTImage,
									  isnull(vcItemName,'''') vcItemName,
									  CASE 
									  WHEN I.[charItemType]=''P''
									  THEN convert(varchar(100),ISNULL((SELECT TOP 1 ROUND(monWListPrice,2) FROM [WareHouseItems] WHERE [numItemID]=I.numItemCode),0)) 
									  ELSE 
										CONVERT(VARCHAR(100), ROUND(monListPrice, 2)) 
									  END AS monListPrice,
									  isnull(vcSKU,'''') vcSKU,
									  isnull(numBarCodeId,0) numBarCodeId,
									  isnull(vcModelID,'''') vcModelID,
									  isnull(txtItemDesc,'''') as txtItemDesc,
									  isnull(C.vcCompanyName,'''') as vcCompanyName,
									  WHT.numWareHouseItemID,
									  dbo.fn_GetAttributes(WHT.numWareHouseItemId,I.bitSerialized) AS [vcAttributes]'
									  + @strSQL
									  + ' from Item  I 
									  Left join DivisionMaster D              
									  on I.numVendorID=D.numDivisionID  
									  left join CompanyInfo C  
									  on C.numCompanyID=D.numCompanyID 
									  LEFT JOIN
											dbo.WareHouseItems WHT
									  ON
											WHT.numItemID = I.numItemCode AND
											WHT.numWareHouseItemID = (
																		SELECT 
																			TOP 1 numWareHouseItemID 
																		FROM 
																			dbo.WareHouseItems 
																		WHERE 
																			dbo.WareHouseItems.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' 
																			AND dbo.WareHouseItems.numItemID = I.numItemCode 
																			AND (dbo.WareHouseItems.numWareHouseID = ' +  CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20))  + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0)' +
																			CASE LEN(@vcWareHouseSearch)
																			WHEN 0 THEN ''
																			ELSE ' AND (' + @vcWareHouseSearch + ' OR 1=1)'
																			END
																			+
																	') 
										where ((I.charItemType <> ''P'') OR ((SELECT COUNT(*) FROM dbo.WareHouseItems WHERE numItemID = I.numItemCode AND numDomainID = '
									  + CONVERT(VARCHAR(20), @numDomainID)
									  + ' AND numWareHouseID =  ' +  CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20))  + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0) > 0)) AND ISNULL(I.Isarchieve,0) <> 1 And I.charItemType NOT IN(''A'') AND  I.numDomainID='
									  + CONVERT(VARCHAR(20), @numDomainID) + ' and ('
									  + @strSearch + ') '   
							      
						--- added Asset validation  by sojan
                        IF @bitRemoveVendorPOValidation = 1
                            AND @tintOppType = 2
                            SET @strSQL = @strSQL
                                + ' and ISNULL(I.bitKitParent,0) = 0 AND ISNULL(I.bitAssembly,0) = 0 '

                        IF @tintSearchOrderCustomerHistory = 1
                            AND @numDivisionID > 0
                            BEGIN
                                SET @strSQL = @strSQL
                                    + ' and I.numItemCode in (select distinct oppI.numItemCode from 
									OpportunityMaster oppM join OpportunityItems oppI on oppM.numOppId=oppI.numOppId where oppM.numDomainID='
                                    + CONVERT(VARCHAR(20), @numDomainID)
                                    + ' and oppM.numDivisionId='
                                    + CONVERT(VARCHAR(20), @numDivisionId)
                                    + ')'
                            END

                        INSERT  INTO @TableRowCount
                                EXEC
                                    ( 'SELECT COUNT(*) FROM ( ' + @strSQL
                                      + ') as t2'
                                    );
                        SELECT  @TotalCount = Value
                        FROM    @TableRowCount;

                        SET @strNewQuery = 'select * from (' + @strSQL
                            + ') as t where SRNO> '
                            + CAST(( ( @numPageIndex - 1 ) * @numPageSize ) AS VARCHAR(10))
                            + ' and SRNO < '
                            + CAST(( ( @numPageIndex * @numPageSize ) + 1 ) AS VARCHAR(10))
                            + ' order by  vcItemName'
                        
						--PRINT @strNewQuery
						EXEC(@strNewQuery)
                    END      
                ELSE
                    IF @tintOppType = 2
                        BEGIN      
                            SET @strSQL = 'SELECT SRNO=ROW_NUMBER() OVER( ORDER BY  I.vcItemName), I.numItemCode,
										  (SELECT TOP 1 vcPathForImage from ItemImages WHERE numItemCode = I.numItemCode) as vcPathForImage,
										  (SELECT TOP 1 vcPathForTImage from ItemImages WHERE numItemCode = I.numItemCode) as vcPathForTImage,
										  ISNULL(vcItemName,'''') vcItemName,
										  convert(varchar(200),round(monListPrice,2)) as monListPrice,
										  isnull(vcSKU,'''') vcSKU,
										  isnull(numBarCodeId,0) numBarCodeId,
										  isnull(vcModelID,'''') vcModelID,
										  isnull(txtItemDesc,'''') as txtItemDesc,
										  isnull(C.vcCompanyName,'''') as vcCompanyName,
										  WHT.numWareHouseItemID,
										  dbo.fn_GetAttributes(WHT.numWareHouseItemId,I.bitSerialized) AS [vcAttributes]'
										  + @strSQL
										  + '  from item I 
										  INNER JOIN
											dbo.Vendor V
										  ON
											V.numItemCode = I.numItemCode
										  Left join 
											DivisionMaster D              
										  ON 
											V.numVendorID=D.numDivisionID  
										  LEFT join 
											CompanyInfo C  
										  ON 
											C.numCompanyID=D.numCompanyID    
										  LEFT JOIN
											dbo.WareHouseItems WHT
										  ON
											WHT.numItemID = I.numItemCode AND
											WHT.numWareHouseItemID = (
																		SELECT 
																			TOP 1 numWareHouseItemID 
																		FROM 
																			dbo.WareHouseItems 
																		WHERE 
																			dbo.WareHouseItems.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' 
																			AND dbo.WareHouseItems.numItemID = I.numItemCode 
																			AND (dbo.WareHouseItems.numWareHouseID = ' +  CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20))  + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0)' +
																			CASE LEN(@vcWareHouseSearch)
																			WHEN 0 THEN ''
																			ELSE ' AND (' + @vcWareHouseSearch  + ' OR 1=1)' 
																			END
																			+
																	')
										       
										  WHERE (
													(I.charItemType <> ''P'') OR 
													((SELECT COUNT(*) FROM dbo.WareHouseItems WHERE numItemID = I.numItemCode AND numDomainID = '
										 + CONVERT(VARCHAR(20), @numDomainID)
										 + ' AND numWareHouseID =  ' +  CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20))  + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0) > 0)) AND ISNULL(I.Isarchieve,0) <> 1 and ISNULL(I.bitKitParent,0) = 0  AND ISNULL(I.bitAssembly,0) = 0 And I.charItemType NOT IN(''A'') AND V.numVendorID='
										 + CONVERT(VARCHAR(20), @numDivisionID)
										 + ' and ('+ CASE LEN(@vcVendorSearch)
																			WHEN 0 THEN ''
																			ELSE REPLACE(@vcVendorSearch,'dbo.Vendor','V') + ' OR '
																			END + @strSearch + ') ' 

							--- added Asset validation  by sojan
                            IF @tintSearchOrderCustomerHistory = 1
                                AND @numDivisionID > 0
                                BEGIN
                                    SET @strSQL = @strSQL
                                        + ' and I.numItemCode in (select distinct oppI.numItemCode from 
									OpportunityMaster oppM join OpportunityItems oppI on oppM.numOppId=oppI.numOppId where oppM.numDomainID='
                                        + CONVERT(VARCHAR(20), @numDomainID)
                                        + ' and oppM.numDivisionId='
                                        + CONVERT(VARCHAR(20), @numDivisionId)
                                        + ')'
                                END


                            INSERT  INTO @TableRowCount
                                    EXEC
                                        ( 'SELECT COUNT(*) FROM ( ' + @strSQL
                                          + ') as t2'
                                        );
                            SELECT  @TotalCount = Value
                            FROM    @TableRowCount;

                            SET @strNewQuery = 'select * from (' + @strSQL
                                + ') as t where SRNO> '
                                + CAST(( ( @numPageIndex - 1 ) * @numPageSize ) AS VARCHAR(10))
                                + ' and SRNO < '
                                + CAST(( ( @numPageIndex * @numPageSize ) + 1 ) AS VARCHAR(10))
                                + ' order by  vcItemName'
                            
							--PRINT @strNewQuery
							EXEC(@strNewQuery)
                        END
                    ELSE
                        SELECT  0  
            END
    END
ELSE
    SELECT  0  

SELECT  *
FROM    #Temp1
WHERE   vcDbColumnName NOT IN ( 'vcPartNo', 'vcBarCode', 'vcWHSKU' )ORDER BY tintOrder 
   


IF OBJECT_ID('dbo.Scores', 'U') IS NOT NULL
  DROP TABLE dbo.Scores

IF OBJECT_ID('tempdb..#Temp1') IS NOT NULL
BEGIN
    DROP TABLE #Temp1
END

IF OBJECT_ID('tempdb..#tempSearch') IS NOT NULL
BEGIN
    DROP TABLE #tempSearch
END

IF OBJECT_ID('tempdb..#tempItemCode') IS NOT NULL
BEGIN
    DROP TABLE #tempItemCode
END


    END
/****** Object:  StoredProcedure [dbo].[USP_LoadAttributes]    Script Date: 07/26/2008 16:19:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec USP_LoadAttributes 14,66,'834,837'    
--created by anoop jayaraj    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_loadattributes')
DROP PROCEDURE usp_loadattributes
GO
CREATE PROCEDURE [dbo].[USP_LoadAttributes]    
@numItemCode as numeric(9)=0,    
@numListID as numeric(9)=0,    
@strAtrr as varchar(1000)=''    
as    
    
declare @bitSerialize as bit    
declare @ItemGroup as numeric(9)    
declare @strSQL as varchar(1000)    
select @bitSerialize=bitSerialized,@ItemGroup=numItemgroup from item where numItemCode=@numItemCode    
    
    
if @strAtrr!=''    
begin    
 Declare @Cnt int    
 Set @Cnt = 1    
 declare @SplitOn char(1)    
 set @SplitOn=','    
 set @strSQL='SELECT recid FROM CFW_Fld_Values_Serialized_Items where  bitSerialized ='+ convert(varchar(1),@bitSerialize)    
 While (Charindex(@SplitOn,@strAtrr)>0)    
 Begin    
  if  @Cnt=1 set @strSQL=@strSQL+' and fld_value=''' + (ltrim(rtrim(Substring(@strAtrr,1,Charindex(@SplitOn,@strAtrr)-1))))+''''    
  else set @strSQL=@strSQL+' or fld_value=''' + (ltrim(rtrim(Substring(@strAtrr,1,Charindex(@SplitOn,@strAtrr)-1))))+''''    
  Set @strAtrr = Substring(@strAtrr,Charindex(@SplitOn,@strAtrr)+1,len(@strAtrr))    
  Set @Cnt = @Cnt + 1    
 End    
 if @Cnt=1 set @strSQL=@strSQL+' and fld_value=''' + @strAtrr + ''' GROUP BY recid HAVING count(*) > '+convert(varchar(10), @Cnt-1)    
 else set @strSQL=@strSQL+' or fld_value=''' + @strAtrr + ''' GROUP BY recid HAVING count(*) > '+convert(varchar(10), @Cnt-1)    
end    
    
    
if @bitSerialize=0 and @numItemCode>0    
begin    
 if @strAtrr=''    
  select numListItemID,vcData from listdetails where numlistitemid in(select distinct(fld_value) from WareHouseItems W    
  join CFW_Fld_Values_Serialized_Items CSI on W.numWareHouseItemID=CSI.RecId    
  join CFW_Fld_Master M on CSI.Fld_ID=M.Fld_ID                   
  join ItemGroupsDTL on CSI.Fld_ID=ItemGroupsDTL.numOppAccAttrID    
  where  M.numListID=@numListID and tintType=2 and CSI.bitSerialized=0 and numItemID=@numItemCode)    
 else    
 begin    
  set @strSQL ='select numListItemID,vcData from listdetails where numlistitemid in(select distinct(fld_value) from WareHouseItems W    
  join CFW_Fld_Values_Serialized_Items CSI on W.numWareHouseItemID=CSI.RecId    
  join CFW_Fld_Master M on CSI.Fld_ID=M.Fld_ID    
  join ItemGroupsDTL on CSI.Fld_ID=ItemGroupsDTL.numOppAccAttrID   
  where  M.numListID='+convert(varchar(20),@numListID)+' and tintType=2 and CSI.bitSerialized=0 and numItemID='+convert(varchar(20),@numItemCode)+' and fld_value!=''0'' and fld_value!=''''    
  and numWareHouseItemID in ('+@strSQL+'))'    
  exec (@strSQL)    
 end    
        
end    
else if @bitSerialize=1 and @numItemCode>0    
begin    
 if @strAtrr=''    
  select numListItemID,vcData from listdetails where numlistitemid in(select distinct(fld_value) from WareHouseItems W    
  join WareHouseItmsDTL WDTL on WDTL.numWareHouseItemID=W.numWareHouseItemID    
  join CFW_Fld_Values_Serialized_Items CSI on WDTL.numWareHouseItmsDTLID=CSI.RecId    
  join CFW_Fld_Master M on CSI.Fld_ID=M.Fld_ID    
  join ItemGroupsDTL on CSI.Fld_ID=ItemGroupsDTL.numOppAccAttrID     
  where  M.numListID=@numListID and tintType=2 and ISNULL(WDTL.numQty,0) > 0 and CSI.bitSerialized=1 and numItemID=@numItemCode     
  and fld_value!='0' and fld_value!='')    
 else    
 begin    
  set @strSQL ='select numListItemID,vcData from listdetails where numlistitemid in(select distinct(fld_value) from WareHouseItems W    
  join WareHouseItmsDTL WDTL on WDTL.numWareHouseItemID=W.numWareHouseItemID    
  join CFW_Fld_Values_Serialized_Items CSI on WDTL.numWareHouseItmsDTLID=CSI.RecId    
  join CFW_Fld_Master M on CSI.Fld_ID=M.Fld_ID    
  join ItemGroupsDTL on CSI.Fld_ID=ItemGroupsDTL.numOppAccAttrID   
  where  M.numListID='+convert(varchar(20),@numListID)+' and tintType=2 and ISNULL(WDTL.numQty,0) > 0 and CSI.bitSerialized=1 and numItemID='+convert(varchar(20),@numItemCode)+'     
  and fld_value!=''0'' and fld_value!='''' and WDTL.numWareHouseItmsDTLID in     
  ('+@strSQL+'))'    
  exec (@strSQL)    
 end    
end    
else select 0
GO
/****** Object:  StoredProcedure [dbo].[USP_LoadAttributesEcommerce]    Script Date: 07/26/2008 16:19:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec USP_LoadAttributes 14,66,'834,837'      
--created by anoop jayaraj      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_loadattributesecommerce')
DROP PROCEDURE usp_loadattributesecommerce
GO
CREATE PROCEDURE [dbo].[USP_LoadAttributesEcommerce]      
@numItemCode as numeric(9)=0,      
@numListID as numeric(9)=0,      
@strAtrr as varchar(1000)='',    
@numWareHouseID as numeric(9)=0      
as      
      
declare @bitSerialize as bit      
declare @ItemGroup as numeric(9)      
declare @strSQL as varchar(1000)      
select @bitSerialize=bitSerialized,@ItemGroup=numItemgroup from item where numItemCode=@numItemCode      
      
      
if @strAtrr!=''      
begin      
 Declare @Cnt int      
 Set @Cnt = 1      
 declare @SplitOn char(1)      
 set @SplitOn=','      
 set @strSQL='SELECT recid FROM CFW_Fld_Values_Serialized_Items where  bitSerialized ='+ convert(varchar(1),@bitSerialize)      
 While (Charindex(@SplitOn,@strAtrr)>0)      
 Begin      
  if  @Cnt=1 set @strSQL=@strSQL+' and fld_value=''' + (ltrim(rtrim(Substring(@strAtrr,1,Charindex(@SplitOn,@strAtrr)-1))))+''''      
  else set @strSQL=@strSQL+' or fld_value=''' + (ltrim(rtrim(Substring(@strAtrr,1,Charindex(@SplitOn,@strAtrr)-1))))+''''      
  Set @strAtrr = Substring(@strAtrr,Charindex(@SplitOn,@strAtrr)+1,len(@strAtrr))      
  Set @Cnt = @Cnt + 1      
 End      
 if @Cnt=1 set @strSQL=@strSQL+' and fld_value=''' + @strAtrr + ''' GROUP BY recid HAVING count(*) > '+convert(varchar(10), @Cnt-1)      
 else set @strSQL=@strSQL+' or fld_value=''' + @strAtrr + ''' GROUP BY recid HAVING count(*) > '+convert(varchar(10), @Cnt-1)      
end      
    
      
if @bitSerialize=0 and @numItemCode>0      
begin      
 if @strAtrr=''      
  select numListItemID,vcData from listdetails where numlistitemid in(select distinct(fld_value) from WareHouseItems W      
  join CFW_Fld_Values_Serialized_Items CSI on W.numWareHouseItemID=CSI.RecId      
  join CFW_Fld_Master M on CSI.Fld_ID=M.Fld_ID                        
  join ItemGroupsDTL on CSI.Fld_ID=ItemGroupsDTL.numOppAccAttrID      
  where  M.numListID=@numListID and tintType=2 and CSI.bitSerialized=0 and numItemID=@numItemCode and W.numWareHouseID=@numWareHouseID)      
 else      
 begin      
  set @strSQL ='select numListItemID,vcData from listdetails where numlistitemid in(select distinct(fld_value) from WareHouseItems W      
  join CFW_Fld_Values_Serialized_Items CSI on W.numWareHouseItemID=CSI.RecId      
  join CFW_Fld_Master M on CSI.Fld_ID=M.Fld_ID      
  join ItemGroupsDTL on CSI.Fld_ID=ItemGroupsDTL.numOppAccAttrID     
  where  M.numListID='+convert(varchar(20),@numListID)+' and tintType=2 and CSI.bitSerialized=0 and numItemID='+convert(varchar(20),@numItemCode)+' and W.numWareHouseID='+convert(varchar(20),@numWareHouseID)+' and fld_value!=''0'' and fld_value!=''''     
 
  and W.numWareHouseItemID in ('+@strSQL+'))' 
  
  Print @strSQL       
  exec (@strSQL)      
 end      
          
end      
else if @bitSerialize=1 and @numItemCode>0      
begin      
 if @strAtrr=''      
  select numListItemID,vcData from listdetails where numlistitemid in(select distinct(fld_value) from WareHouseItems W      
  join WareHouseItmsDTL WDTL on WDTL.numWareHouseItemID=W.numWareHouseItemID      
  join CFW_Fld_Values_Serialized_Items CSI on WDTL.numWareHouseItmsDTLID=CSI.RecId      
  join CFW_Fld_Master M on CSI.Fld_ID=M.Fld_ID      
join ItemGroupsDTL on CSI.Fld_ID=ItemGroupsDTL.numOppAccAttrID     
  where  M.numListID=@numListID and tintType=2 and ISNULL(WDTL.numQty,0) > 0 and CSI.bitSerialized=1 and numItemID=@numItemCode       
  and fld_value!='0' and fld_value!='' and W.numWareHouseID=@numWareHouseID)      
 else      
 begin      
  set @strSQL ='select numListItemID,vcData from listdetails where numlistitemid in(select distinct(fld_value) from WareHouseItems W      
  join WareHouseItmsDTL WDTL on WDTL.numWareHouseItemID=W.numWareHouseItemID      
  join CFW_Fld_Values_Serialized_Items CSI on WDTL.numWareHouseItmsDTLID=CSI.RecId      
  join CFW_Fld_Master M on CSI.Fld_ID=M.Fld_ID      
  join ItemGroupsDTL on CSI.Fld_ID=ItemGroupsDTL.numOppAccAttrID      
  where  M.numListID='+convert(varchar(20),@numListID)+' and tintType=2 and ISNULL(WDTL.numQty,0) > 0 and CSI.bitSerialized=1 and numItemID='+convert(varchar(20),@numItemCode)+'       
   and W.numWareHouseID='+convert(varchar(20),@numWareHouseID)+' and fld_value!=''0'' and fld_value!='''' and WDTL.numWareHouseItmsDTLID in       
  ('+@strSQL+'))'   
  exec (@strSQL)      
 end      
end      
else select 0
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageDefaultAccountsForDomain')
DROP PROCEDURE USP_ManageDefaultAccountsForDomain
GO
CREATE PROCEDURE USP_ManageDefaultAccountsForDomain
    @numDomainID NUMERIC(9),
    @str AS VARCHAR(8000)
AS 
    BEGIN
        DECLARE @hDocItem INT
        IF CONVERT(VARCHAR(10), @str) <> '' 
            BEGIN
                EXEC sp_xml_preparedocument @hDocItem OUTPUT, @str
            
                DELETE  FROM [AccountingCharges]
                WHERE   numDomainID = @numDomainID
            
                INSERT  INTO [AccountingCharges]
                        (
                          [numChargeTypeId],
                          [numAccountID],
                          [numDomainID]
                        )
                        SELECT  
								ISNULL((SELECT numChargeTypeId FROM [AccountingChargeTypes] WHERE chChargeCode= X.chChargeCode),0) AS numChargeTypeId,
                                X.numAccountID,
                                @numDomainID
                        FROM    ( SELECT    *
                                  FROM      OPENXML (@hDocItem, '/NewDataSet/Table1', 2)
                                            WITH ( chChargeCode CHAR(2), numAccountID NUMERIC(9) )
                                ) X
                                
                EXEC sp_xml_removedocument @hDocItem
            END
END

GO
/****** Object:  StoredProcedure [dbo].[USP_ManageItemsAndKits]    Script Date: 02/15/2010 21:44:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Anoop Jayaraj                                                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_manageitemsandkits')
DROP PROCEDURE usp_manageitemsandkits
GO
CREATE PROCEDURE [dbo].[USP_ManageItemsAndKits]                                                                
@numItemCode as numeric(9) output,                                                                
@vcItemName as varchar(300),                                                                
@txtItemDesc as varchar(1000),                                                                
@charItemType as char(1),                                                                
@monListPrice as money,                                                                
@numItemClassification as numeric(9),                                                                
@bitTaxable as bit,                                                                
@vcSKU as varchar(50),                                                                                                                                      
@bitKitParent as bit,                                                                                   
--@dtDateEntered as datetime,                                                                
@numDomainID as numeric(9),                                                                
@numUserCntID as numeric(9),                                                                                                  
@numVendorID as numeric(9),                                                            
@bitSerialized as bit,                                               
@strFieldList as text,                                            
@vcModelID as varchar(200)='' ,                                            
@numItemGroup as numeric(9)=0,                                      
@numCOGSChartAcntId as numeric(9)=0,                                      
@numAssetChartAcntId as numeric(9)=0,                                      
@numIncomeChartAcntId as numeric(9)=0,                            
@monAverageCost as money,                          
@monLabourCost as money,                  
@fltWeight as float,                  
@fltHeight as float,                  
@fltLength as float,                  
@fltWidth as float,                  
@bitFreeshipping as bit,                
@bitAllowBackOrder as bit,              
@UnitofMeasure as varchar(30)='',              
@strChildItems as text,            
@bitShowDeptItem as bit,              
@bitShowDeptItemDesc as bit,        
@bitCalAmtBasedonDepItems as bit,    
@bitAssembly as BIT,
@intWebApiId INT=null,
@vcApiItemId AS VARCHAR(50)=null,
@numBarCodeId as VARCHAR(25)=null,
@vcManufacturer as varchar(250)=NULL,
@numBaseUnit AS NUMERIC(18)=0,
@numPurchaseUnit AS NUMERIC(18)=0,
@numSaleUnit AS NUMERIC(18)=0,
@bitLotNo as BIT,
@IsArchieve AS BIT,
@numItemClass as numeric(9)=0,
@tintStandardProductIDType AS TINYINT=0,
@vcExportToAPI VARCHAR(50),
@numShipClass NUMERIC,
@ProcedureCallFlag SMALLINT =0,
@vcCategories AS VARCHAR(2000) = '',
@bitAllowDropShip AS BIT=0,
@bitArchiveItem AS BIT = 0,
@bitAsset AS BIT = 0,
@bitRental AS BIT = 0
as                                                                
declare @hDoc as INT

if @numCOGSChartAcntId=0 set @numCOGSChartAcntId=null                      
if  @numAssetChartAcntId=0 set @numAssetChartAcntId=null  
if  @numIncomeChartAcntId=0 set @numIncomeChartAcntId=null     

DECLARE @ParentSKU VARCHAR(50)                                        
DECLARE @ItemID AS NUMERIC(9)
DECLARE @cnt AS INT

--if @dtDateEntered = 'Jan  1 1753 12:00:00:000AM' set @dtDateEntered=null 

IF ISNULL(@charItemType,'') = '' OR ISNULL(@charItemType,'') = '0'  SET @charItemType = 'N'

IF @intWebApiId = 2 
    AND LEN(ISNULL(@vcApiItemId, '')) > 0
    AND ISNULL(@numItemGroup, 0) > 0 
    BEGIN      
    
        -- check wether this id already Mapped in ITemAPI 
        SELECT  @cnt = COUNT([numItemID])
        FROM    [ItemAPI]
        WHERE   [WebApiId] = @intWebApiId
                AND [numDomainId] = @numDomainID
                AND [vcAPIItemID] = @vcApiItemId
        IF @cnt > 0 
            BEGIN
                SELECT  @ItemID = [numItemID]
                FROM    [ItemAPI]
                WHERE   [WebApiId] = @intWebApiId
                        AND [numDomainId] = @numDomainID
                        AND [vcAPIItemID] = @vcApiItemId
        --Update Existing Product coming from API
                SET @numItemCode = @ItemID
            END
    
    END

ELSE 
    IF @intWebApiId > 1 --AND  @intWebApiId <> 2
        AND LEN(ISNULL(@vcSKU, '')) > 0 
        BEGIN
            SET @ParentSKU = @vcSKU
    
       -- check wether this id already exist in Domain
       
            SELECT  @cnt = COUNT([numItemCode])
            FROM    [Item]
            WHERE   [numDomainId] = @numDomainID
                    AND ( vcSKU = @vcSKU
                          OR @vcSKU IN ( SELECT vcWHSKU
                                         FROM   dbo.WareHouseItems
                                         WHERE  numItemID = Item.[numItemCode]
                                                AND vcWHSKU = @vcSKU )
                        )
            IF @cnt > 0 
                BEGIN
                    SELECT  @ItemID = [numItemCode],
                            @ParentSKU = vcSKU
                    FROM    [Item]
                    WHERE   [numDomainId] = @numDomainID
                            AND ( vcSKU = @vcSKU
                                  OR @vcSKU IN (
                                  SELECT    vcWHSKU
                                  FROM      dbo.WareHouseItems
                                  WHERE     numItemID = Item.[numItemCode]
                                            AND vcWHSKU = @vcSKU )
                                )
                    SET @numItemCode = @ItemID
                END
            ELSE 
                BEGIN
			
    -- check wether this id already Mapped in ITemAPI 
                    SELECT  @cnt = COUNT([numItemID])
                    FROM    [ItemAPI]
                    WHERE   [WebApiId] = @intWebApiId
                            AND [numDomainId] = @numDomainID
                            AND [vcAPIItemID] = @vcApiItemId
                    IF @cnt > 0 
                        BEGIN
                            SELECT  @ItemID = [numItemID]
                            FROM    [ItemAPI]
                            WHERE   [WebApiId] = @intWebApiId
                                    AND [numDomainId] = @numDomainID
                                    AND [vcAPIItemID] = @vcApiItemId
        --Update Existing Product coming from API
                            SET @numItemCode = @ItemID
                        END
     
                END
        END
                                                         
if @numItemCode=0 or  @numItemCode is null
begin                                                                 
 insert into Item (                                             
  vcItemName,                                             
  txtItemDesc,                                             
  charItemType,                                             
  monListPrice,                                             
  numItemClassification,                                             
  bitTaxable,                                             
  vcSKU,                                             
  bitKitParent,                                             
  --dtDateEntered,                                              
  numVendorID,                                             
  numDomainID,                                             
  numCreatedBy,                                             
  bintCreatedDate,                                             
  bintModifiedDate,                                             
 numModifiedBy,                                              
  bitSerialized,                                     
  vcModelID,                                            
  numItemGroup,                                          
  numCOGsChartAcntId,                        
  numAssetChartAcntId,                                      
  numIncomeChartAcntId,                            
  monAverageCost,                          
  monCampaignLabourCost,                  
  fltWeight,                  
  fltHeight,                  
  fltWidth,                  
  fltLength,                  
  bitFreeShipping,                
  bitAllowBackOrder,              
  vcUnitofMeasure,            
  bitShowDeptItem,            
  bitShowDeptItemDesc,        
  bitCalAmtBasedonDepItems,    
  bitAssembly,
  numBarCodeId,
  vcManufacturer,numBaseUnit,numPurchaseUnit,numSaleUnit,bitLotNo,IsArchieve,numItemClass,tintStandardProductIDType,vcExportToAPI,numShipClass,
  bitAllowDropShip,bitArchiveItem,
  bitAsset,bitRental
   )                                                              
   values                                                                
    (                                                                
  @vcItemName,                                             
  @txtItemDesc,                                  
  @charItemType,                                             
  @monListPrice,                                             
  @numItemClassification,                                             
  @bitTaxable,                                             
  @ParentSKU,                       
  @bitKitParent,                                             
  --@dtDateEntered,                                             
  @numVendorID,                                             
  @numDomainID,                                             
  @numUserCntID,                                           
  getutcdate(),                                             
  getutcdate(),                                             
  @numUserCntID,                                             
  @bitSerialized,                                       
  @vcModelID,                                            
  @numItemGroup,                                      
  @numCOGsChartAcntId,                                      
  @numAssetChartAcntId,                                      
  @numIncomeChartAcntId,                            
  @monAverageCost,                          
  @monLabourCost,                  
  @fltWeight,                  
  @fltHeight,                  
  @fltWidth,                  
  @fltLength,                  
  @bitFreeshipping,                
  @bitAllowBackOrder,              
  @UnitofMeasure,            
  @bitShowDeptItem,            
  @bitShowDeptItemDesc,        
  @bitCalAmtBasedonDepItems,    
  @bitAssembly,                                                          
   @numBarCodeId,
@vcManufacturer,@numBaseUnit,@numPurchaseUnit,@numSaleUnit,@bitLotNo,@IsArchieve,@numItemClass,@tintStandardProductIDType,@vcExportToAPI,@numShipClass,
@bitAllowDropShip,@bitArchiveItem ,@bitAsset,@bitRental)                                             
 
 set @numItemCode = SCOPE_IDENTITY()
  
 IF  @intWebApiId > 1 
   BEGIN
     -- insert new product
     --insert this id into linking table
     INSERT INTO [ItemAPI] (
	 	[WebApiId],
	 	[numDomainId],
	 	[numItemID],
	 	[vcAPIItemID],
	 	[numCreatedby],
	 	[dtCreated],
	 	[numModifiedby],
	 	[dtModified],vcSKU
	 ) VALUES     (@intWebApiId,
                 @numDomainID,
                 @numItemCode,
                 @vcApiItemId,
	 			 @numUserCntID,
	 			 GETUTCDATE(),
	 			 @numUserCntID,
	 			 GETUTCDATE(),@vcSKU
	 			 ) 
   
	--update lastUpdated Date                  
	--UPDATE [WebAPIDetail] SET [vcThirteenthFldValue] = GETUTCDATE() WHERE [WebApiId] =@intWebApiId AND [numDomainId] = @numDomainID
   END
 
 
end         
else if    @numItemCode>0  AND @intWebApiId > 0 
BEGIN 
	IF @ProcedureCallFlag =1 
		BEGIN
		DECLARE @ExportToAPIList AS VARCHAR(30)
		SELECT @ExportToAPIList = vcExportToAPI FROM dbo.Item WHERE numItemCode = @numItemCode
		IF @ExportToAPIList != ''
			BEGIN
			IF NOT EXISTS(select * from (SELECT * FROM dbo.Split(@ExportToAPIList ,','))as A where items= @vcExportToAPI ) 
				BEGIN
					SET @vcExportToAPI = @ExportToAPIList + ',' + @vcExportToAPI
				End
			ELSE
				BEGIN
					SET @vcExportToAPI = @ExportToAPIList 
				End
			END
		--update ExportToAPI String value
		UPDATE dbo.Item SET vcExportToAPI=@vcExportToAPI where numItemCode=@numItemCode  AND numDomainID=@numDomainID
					
			SELECT @cnt = COUNT([numItemID]) FROM [ItemAPI] WHERE  [WebApiId] = @intWebApiId 
																AND [numDomainId] = @numDomainID
																AND [vcAPIItemID] = @vcApiItemId
																AND numItemID = @numItemCode
			IF @cnt > 0
			BEGIN
				--update lastUpdated Date 
				UPDATE [ItemAPI] SET [dtModified] =GETUTCDATE() WHERE [WebApiId] =@intWebApiId AND [numDomainId] = @numDomainID
			END
			ELSE
				--Insert ItemAPI mapping
				BEGIN
					INSERT INTO [ItemAPI] (
						[WebApiId],
						[numDomainId],
						[numItemID],
						[vcAPIItemID],
						[numCreatedby],
						[dtCreated],
						[numModifiedby],
						[dtModified],vcSKU
					) VALUES     (@intWebApiId,
						@numDomainID,
						@numItemCode,
						@vcApiItemId,
						@numUserCntID,
						GETUTCDATE(),
						@numUserCntID,
						GETUTCDATE(),@vcSKU
					)	
				END

		END   
END
                                                       
else if    @numItemCode>0  AND @intWebApiId <= 0                                                           
begin

	DECLARE @OldGroupID NUMERIC 
	SELECT @OldGroupID = ISNULL(numItemGroup,0) FROM [Item] WHERE numitemcode = @numItemCode AND [numDomainId] = @numDomainID
	

	DECLARE @monOldAverageCost AS DECIMAL(18,4);SET @monOldAverageCost=0
	SELECT @monOldAverageCost=ISNULL(monAverageCost,0) FROM Item WHERE numItemCode =@numItemCode AND [numDomainId] = @numDomainID
	   
 update item set vcItemName=@vcItemName,                           
  txtItemDesc=@txtItemDesc,                                             
  charItemType=@charItemType,                                             
  monListPrice= @monListPrice,
  numItemClassification=@numItemClassification,                                             
  bitTaxable=@bitTaxable,                                             
  vcSKU = (CASE WHEN ISNULL(@ParentSKU,'') = '' THEN @vcSKU ELSE @ParentSKU END),                                             
  bitKitParent=@bitKitParent,                                             
  --dtDateEntered=@dtDateEntered,                                             
  numVendorID=@numVendorID,                                             
  numDomainID=@numDomainID,                                             
  bintModifiedDate=getutcdate(),                                             
  numModifiedBy=@numUserCntID,                                   
  bitSerialized=@bitSerialized,                                                         
  vcModelID=@vcModelID,                                            
  numItemGroup=@numItemGroup,                                      
  numCOGsChartAcntId=@numCOGsChartAcntId,                                      
  numAssetChartAcntId=@numAssetChartAcntId,                                      
  numIncomeChartAcntId=@numIncomeChartAcntId,                            
  --monAverageCost=@monAverageCost,                          
  monCampaignLabourCost=@monLabourCost,                
  fltWeight=@fltWeight,                  
  fltHeight=@fltHeight,                  
  fltWidth=@fltWidth,                  
  fltLength=@fltLength,                  
  bitFreeShipping=@bitFreeshipping,                
  bitAllowBackOrder=@bitAllowBackOrder,              
  vcUnitofMeasure=@UnitofMeasure,            
  bitShowDeptItem=@bitShowDeptItem,            
  bitShowDeptItemDesc=@bitShowDeptItemDesc,        
  bitCalAmtBasedonDepItems=@bitCalAmtBasedonDepItems,    
  bitAssembly=@bitAssembly ,
numBarCodeId=@numBarCodeId,
vcManufacturer=@vcManufacturer,numBaseUnit=@numBaseUnit,numPurchaseUnit=@numPurchaseUnit,numSaleUnit=@numSaleUnit,bitLotNo=@bitLotNo,
IsArchieve = @IsArchieve,numItemClass=@numItemClass,tintStandardProductIDType=@tintStandardProductIDType,vcExportToAPI=@vcExportToAPI,
numShipClass=@numShipClass,bitAllowDropShip=@bitAllowDropShip, bitArchiveItem = @bitArchiveItem,bitAsset=@bitAsset,bitRental=@bitRental
where numItemCode=@numItemCode  AND numDomainID=@numDomainID

IF NOT EXISTS(SELECT numItemCode FROM dbo.OpportunityItems WHERE numItemCode = @numItemCode)
	BEGIN
		IF (((SELECT ISNULL(SUM(numOnHand),0) FROM WareHouseItems WHERE numItemID = @numItemCode AND numDomainId=@numDomainID) = 0) AND ((SELECT ISNULL(monAverageCost,0) FROM Item  WHERE numItemCode = @numItemCode AND numDomainId=@numDomainID ) = 0))
		BEGIN
			UPDATE Item SET monAverageCost=@monAverageCost  where numItemCode=@numItemCode  AND numDomainID=@numDomainID	
		END
		--UPDATE Item SET monAverageCost=@monAverageCost  where numItemCode=@numItemCode  AND numDomainID=@numDomainID	
	END
	
 --If Average Cost changed then add in Tracking
	IF ISNULL(@monAverageCost,0) != ISNULL(@monOldAverageCost,0)
	BEGIN
		DECLARE @numWareHouseItemID NUMERIC(9);SET @numWareHouseItemID=0
		DECLARE @numTotal int;SET @numTotal=0
		DECLARE @i int;SET @i=0
		DECLARE @vcDescription AS VARCHAR(100);SET @vcDescription=''
		
	    SELECT @numTotal=COUNT(*) FROM WareHouseItems where numItemID=@numItemCode AND numDomainId=@numDomainId
	     
		WHILE @i < @numTotal
		BEGIN
			SELECT @numWareHouseItemID = min(numWareHouseItemID) FROM WareHouseItems 
				WHERE numItemID=@numItemCode AND numDomainId=@numDomainId and numWareHouseItemID > @numWareHouseItemID 
				
			IF @numWareHouseItemID>0
			BEGIN
				SET @vcDescription='Average Cost Changed Manually OLD : ' + CAST(ISNULL(@monOldAverageCost,0) AS VARCHAR(20))
				
				DECLARE @numDomain AS NUMERIC(18,0)
				SET @numDomain = @numDomainID
				EXEC dbo.USP_ManageWareHouseItems_Tracking
					@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)		
					@numReferenceID = @numItemCode, --  numeric(9, 0)
					@tintRefType = 1, --  tinyint
					@vcDescription = @vcDescription, --  varchar(100)
					@tintMode = 0, --  tinyint
					@numModifiedBy = @numUserCntID, --  numeric(9, 0)
					@numDomainID = @numDomain
		    END
		    
		    SET @i = @i + 1
		END
	END
 
 DECLARE @bitCartFreeShipping as  bit 
 SELECT @bitCartFreeShipping = bitFreeShipping FROM dbo.CartItems where numItemCode = @numItemCode  AND numDomainID = @numDomainID 
 
 IF @bitCartFreeShipping <> @bitFreeshipping
 BEGIN
    UPDATE dbo.CartItems SET bitFreeShipping = @bitFreeshipping WHERE numItemCode = @numItemCode  AND numDomainID=@numDomainID
 END 
 
 
 IF  @intWebApiId > 1 
   BEGIN
      SELECT @cnt = COUNT([numItemID])
    FROM   [ItemAPI]
    WHERE  [WebApiId] = @intWebApiId
           AND [numDomainId] = @numDomainID
           AND [vcAPIItemID] = @vcApiItemId
    IF @cnt > 0
      BEGIN
      --update lastUpdated Date 
      UPDATE [ItemAPI] SET [dtModified] =GETUTCDATE() WHERE [WebApiId] =@intWebApiId AND [numDomainId] = @numDomainID
      END
      ELSE
      --Insert ItemAPI mapping
      BEGIN
        INSERT INTO [ItemAPI] (
	 	[WebApiId],
	 	[numDomainId],
	 	[numItemID],
	 	[vcAPIItemID],
	 	[numCreatedby],
	 	[dtCreated],
	 	[numModifiedby],
	 	[dtModified],vcSKU
	 ) VALUES     (@intWebApiId,
                 @numDomainID,
                 @numItemCode,
                 @vcApiItemId,
	 			 @numUserCntID,
	 			 GETUTCDATE(),
	 			 @numUserCntID,
	 			 GETUTCDATE(),@vcSKU
	 			 ) 
      END
   
   END                                                                               
-- kishan It is necessary to add item into itemTax table . 
 IF	@bitTaxable = 1
	BEGIN
		IF NOT EXISTS(SELECT * FROM dbo.ItemTax WHERE numItemCode = @numItemCode AND numTaxItemID = 0)
			BEGIN
			    	INSERT INTO dbo.ItemTax (
						numItemCode,
						numTaxItemID,
						bitApplicable
					) VALUES ( 
						/* numItemCode - numeric(18, 0) */ @numItemCode,
						/* numTaxItemID - numeric(18, 0)*/ 0,
						/* bitApplicable - bit */ 1 ) 						
			END
	END	 
      
IF @charItemType='S' or @charItemType='N'                                                                       
BEGIN
	delete from WareHouseItmsDTL where numWareHouseItemID in (select numWareHouseItemID from WareHouseItems where numItemID=@numItemCode AND [WareHouseItems].[numDomainID] = @numDomainID)
	delete from WareHouseItems where numItemID=@numItemCode AND [WareHouseItems].[numDomainID] = @numDomainID
END                                      
   
    if @bitKitParent=1  OR @bitAssembly = 1              
    begin              
  declare @hDoc1 as int                                            
  EXEC sp_xml_preparedocument @hDoc1 OUTPUT, @strChildItems                                                                        
                                                                                                 
                                                                
   update ItemDetails set                                                                         
    numQtyItemsReq=X.QtyItemsReq,numWareHouseItemId=X.numWarehouseItmsID,numUOMId=X.numUOMId,vcItemDesc=X.vcItemDesc,sintOrder=X.sintOrder                                                                                          
     From (SELECT QtyItemsReq,ItemKitID,ChildItemID,numWarehouseItmsID,numUOMId,vcItemDesc,sintOrder                             
    FROM OPENXML(@hDoc1,'/NewDataSet/Table1',2)                                                                         
     with(ItemKitID numeric(9),                                                          
     ChildItemID numeric(9),                                                                        
     QtyItemsReq numeric(9),  
     numWarehouseItmsID numeric(9),numUOMId NUMERIC(9),vcItemDesc VARCHAR(1000),sintOrder int))X                                                                         
   where  numItemKitID=X.ItemKitID and numChildItemID=ChildItemID                
              
 end   
 ELSE
 BEGIN
 	DELETE FROM ItemDetails WHERE numItemKitID=@numItemCode
 END           
                                                               
end

declare  @rows as integer                                        
 EXEC sp_xml_preparedocument @hDoc OUTPUT, @strFieldList                                                                      
                                         
 SELECT @rows=count(*) FROM OPENXML (@hDoc,'/NewDataSet/CusFlds',2)                                                  
  WITH (Fld_ID numeric(9),Fld_Value varchar(100),RecId numeric(9))                                        
                                                                           
                                          
 if @rows>0                                        
 begin                                        
  Create table #tempTable ( ID INT IDENTITY PRIMARY KEY,                    
  Fld_ID numeric(9),                                        
  Fld_Value varchar(100),                                        
  RecId numeric(9),                                    
  bitSItems bit                                                                         
  )                                         
  insert into #tempTable (Fld_ID,Fld_Value,RecId,bitSItems)                                        
  SELECT * FROM OPENXML (@hDoc,'/NewDataSet/CusFlds',2)                                                  
  WITH (Fld_ID numeric(9),Fld_Value varchar(100),RecId numeric(9),bitSItems bit)                                           
                                         
  delete CFW_Fld_Values_Serialized_Items from CFW_Fld_Values_Serialized_Items C                                        
  inner join #tempTable T                          
  on   C.Fld_ID=T.Fld_ID and C.RecId=T.RecId and bitSerialized=bitSItems                                         
                                         
  drop table #tempTable                                        
                                                            
  insert into CFW_Fld_Values_Serialized_Items(Fld_ID,Fld_Value,RecId,bitSerialized)                                                  
  select Fld_ID,isnull(Fld_Value,'') as Fld_Value,RecId,bitSItems from(SELECT * FROM OPENXML (@hDoc,'/NewDataSet/CusFlds',2)                                                  
  WITH (Fld_ID numeric(9),Fld_Value varchar(100),RecId numeric(9),bitSItems bit)) X                                         
 end  

IF @numItemCode > 0
	BEGIN
			IF EXISTS(SELECT * FROM dbo.ItemCategory WHERE numItemID = @numItemCode)
			 BEGIN
					DELETE FROM dbo.ItemCategory WHERE numItemID = @numItemCode		
			 END 
	  IF @vcCategories <> ''	
		BEGIN
        INSERT INTO ItemCategory( numItemID , numCategoryID )  
        SELECT  @numItemCode AS numItemID,ID from dbo.SplitIDs(@vcCategories,',')	
		END
	END

 EXEC sp_xml_removedocument @hDoc              
                                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageOppItemSerialNo')
DROP PROCEDURE USP_ManageOppItemSerialNo
GO
CREATE PROCEDURE USP_ManageOppItemSerialNo
      @strItems TEXT
AS 
BEGIN

--Transaction is invoked from ADO.net (Purchase Fulfillment) Do not add transaction code here.
--BEGIN TRY
--BEGIN TRANSACTION

			DECLARE @hDoc AS INT                                            
			EXEC sp_xml_preparedocument @hDoc OUTPUT, @strItems
          
			SELECT X.*,ROW_NUMBER() OVER( order by X.numWareHouseItemID) AS ROWNUMBER
			INTO #TempTable
			FROM ( SELECT  vcSerialNo,numWareHouseItemID,numOppId,numOppItemId,numQty
						  FROM      OPENXML(@hDoc, '/NewDataSet/SerializedItems',2)
									WITH ( vcSerialNo varchar(50), numWareHouseItemID NUMERIC(9), numOppId NUMERIC(9),numOppItemId NUMERIC(9),numQty NUMERIC(9))
                ) X

			DECLARE @minROWNUMBER INT
			DECLARE @maxROWNUMBER INT
			DECLARE @numWareHouseItmsDTLID NUMERIC(9)
			DECLARE @numWareHouseItemID NUMERIC(9)

			SELECT  @minROWNUMBER = MIN(ROWNUMBER) , @maxROWNUMBER =MAX(ROWNUMBER) FROM #TempTable

			WHILE  @minROWNUMBER <= @maxROWNUMBER
				BEGIN
				   SELECT @numWareHouseItemID=numWareHouseItemID FROM #TempTable X WHERE X.ROWNUMBER=@minROWNUMBER
    
   					INSERT INTO WareHouseItmsDTL(numWareHouseItemID,vcSerialNo,numQty)  
		   				 SELECT  X.numWareHouseItemID,X.vcSerialNo,X.numQty FROM #TempTable X WHERE X.ROWNUMBER=@minROWNUMBER
		    
					SET @numWareHouseItmsDTLID=SCOPE_IDENTITY()
		
					INSERT INTO OppWarehouseSerializedItem(numWarehouseItmsDTLID,numOppId,numOppItemId,numWarehouseItmsID,numQty)  
		   				 SELECT  @numWareHouseItmsDTLID,X.numOppId,X.numOppItemId,X.numWareHouseItemID,X.numQty FROM #TempTable X WHERE X.ROWNUMBER=@minROWNUMBER

		   	 
					SELECT  @minROWNUMBER = MIN(ROWNUMBER) FROM  #TempTable WHERE  [ROWNUMBER] > @minROWNUMBER
				END	

			DROP TABLE #TempTable
			EXEC sp_xml_removedocument @hDoc
			
--COMMIT
--END TRY
--BEGIN CATCH
--  -- Whoops, there was an error
--  IF @@TRANCOUNT > 0
--     ROLLBACK

--  -- Raise an error with the details of the exception
--  DECLARE @ErrMsg nvarchar(4000), @ErrSeverity int
--  SELECT @ErrMsg = ERROR_MESSAGE(),
--         @ErrSeverity = ERROR_SEVERITY()

--  RAISERROR(@ErrMsg, @ErrSeverity, 1)
--END CATCH
END
                      
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ManageReturnBizDocs' ) 
    DROP PROCEDURE USP_ManageReturnBizDocs
GO

CREATE PROCEDURE [dbo].[USP_ManageReturnBizDocs]
    (
      @numReturnHeaderID NUMERIC(9) = 0,
      @tintReceiveType TINYINT,
      @numReturnStatus NUMERIC(9),
      @numDomainId NUMERIC(9),
      @numUserCntID NUMERIC(9),
      @numAccountID NUMERIC(9),
      @vcCheckNumber VARCHAR(50),
      @IsCreateRefundReceipt BIT,
      @numItemCode	NUMERIC(18,0)
    )
AS 
BEGIN
BEGIN TRY
BEGIN TRANSACTION
    DECLARE @tintType TINYINT,@tintReturnType TINYINT
	
    SELECT @tintReturnType=tintReturnType FROM ReturnHeader WHERE numReturnHeaderID = @numReturnHeaderID    
    
	PRINT @tintReturnType
	PRINT @tintReceiveType

    IF @tintReturnType=1 OR @tintReturnType=2 
    BEGIN
		DECLARE @i INT = 1
		DECLARE @COUNT AS INT 
		DECLARE @numTempOppID AS NUMERIC(18,0)
		DECLARE @numTempOppItemID AS NUMERIC(18,0)
		DECLARE @bitTempLotNo AS BIT
		DECLARE @numTempQty AS INT 
		DECLARE @numTempWareHouseItmsDTLID AS INT
			
		DECLARE @TempOppSerial TABLE
		(
			ID INT IDENTITY(1,1),
			numWarehouseItmsDTLID NUMERIC(18,0),
			numQty INT,
			bitLotNo NUMERIC(18,0),
			numOppId NUMERIC(18,0),
			numOppItemID NUMERIC(18,0)
		)

		INSERT INTO @TempOppSerial
		(
			numWarehouseItmsDTLID,
			numQty,
			bitLotNo,
			numOppId,
			numOppItemID
		)
		SELECT	
			OWSIReturn.numWarehouseItmsDTLID,
			OWSIReturn.numQty,
			I.bitLotNo,
			RH.numOppId,
			RI.numOppItemID
		FROM
			ReturnHeader RH
		INNER JOIN
			ReturnItems RI
		ON
			RH.numReturnHeaderID = RI.numReturnHeaderID
		INNER JOIN
			OppWarehouseSerializedItem OWSIReturn
		ON
			RH.numReturnHeaderID = OWSIReturn.numReturnHeaderID
			AND RI.numReturnItemID = OWSIReturn.numReturnItemID
		INNER JOIN
			OpportunityItems OI
		ON
			RI.numOppItemID = OI.numoppitemtCode
		INNER JOIN
			Item I
		ON
			OI.numItemCode = I.numItemCode
		WHERE
			RH.numReturnHeaderID = @numReturnHeaderID

		--SALES RETURN
		IF @tintReturnType = 1
		BEGIN
			-- MAKE SERIAL/LOT# NUUMBER AVAILABLE FOR USE AS ITEM IT IS RETURNED TO WAREHOUSE
			UPDATE WHIDL
				SET WHIDL.numQty = (CASE WHEN Item.bitLotNo = 1 THEN ISNULL(WHIDL.numQty,0) + ISNULL(OWSI.numQty,0) ELSE 1 END)
			FROM 
				WareHouseItmsDTL WHIDL
			INNER JOIN
				OppWarehouseSerializedItem OWSI
			ON
				WHIDL.numWareHouseItmsDTLID = OWSI.numWarehouseItmsDTLID
				AND WHIDL.numWareHouseItemID = OWSI.numWarehouseItmsID
			INNER JOIN
				WareHouseItems
			ON
				WHIDL.numWareHouseItemID = WareHouseItems.numWareHouseItemID
			INNER JOIN
				Item
			ON
				WareHouseItems.numItemID = Item.numItemCode
			WHERE
				OWSI.numReturnHeaderID=@numReturnHeaderID
		END
		ELSE IF @tintReturnType = 2
		BEGIN
			IF (SELECT
						COUNT(*)
				FROM
					WareHouseItmsDTL WHIDL
				INNER JOIN
					OppWarehouseSerializedItem OWSI
				ON
					WHIDL.numWareHouseItmsDTLID = OWSI.numWarehouseItmsDTLID
					AND WHIDL.numWareHouseItemID = OWSI.numWarehouseItmsID
				INNER JOIN
					WareHouseItems
				ON
					WHIDL.numWareHouseItemID = WareHouseItems.numWareHouseItemID
				INNER JOIN
					Item
				ON
					WareHouseItems.numItemID = Item.numItemCode
				WHERE
					numReturnHeaderID=@numReturnHeaderID
					AND 1 = (CASE 
								WHEN Item.bitLotNo = 1 AND ISNULL(WHIDL.numQty,0) < ISNULL(OWSI.numQty,0) THEN 1 
								WHEN Item.bitSerialized = 1 AND ISNULL(WHIDL.numQty,0) = 0 THEN 1 
								ELSE 0 
							END)
				) > 0
			BEGIN
				RAISERROR ('SERIAL/LOT#_USED', 16, 1 ) ;
			END
			ELSE
			BEGIN
				-- REMOVE SERIAL/LOT# NUUMBER FROM INVENTORY
				UPDATE WHIDL
					SET WHIDL.numQty = (CASE WHEN Item.bitLotNo = 1 THEN ISNULL(WHIDL.numQty,0) - ISNULL(OWSI.numQty,0) ELSE 0 END)
				FROM 
					WareHouseItmsDTL WHIDL
				INNER JOIN
					OppWarehouseSerializedItem OWSI
				ON
					WHIDL.numWareHouseItmsDTLID = OWSI.numWarehouseItmsDTLID
					AND WHIDL.numWareHouseItemID = OWSI.numWarehouseItmsID
				INNER JOIN
					WareHouseItems
				ON
					WHIDL.numWareHouseItemID = WareHouseItems.numWareHouseItemID
				INNER JOIN
					Item
				ON
					WareHouseItems.numItemID = Item.numItemCode
				WHERE
					numReturnHeaderID=@numReturnHeaderID
			END
		END

		--REMOVE ENTERIES SERIAL/LOT# ENTERIES FROM OppWarehouseSerializedItem TABLE WHICH ARE ASSOCIATED WITH ORDER BECAUSE NOW SERIALS ARE RETURNED TO/FROM WAREHOUSE
		SELECT @COUNT = COUNT(*) FROM @TempOppSerial

		WHILE @i <= @COUNT
		BEGIN
			SELECT
				@bitTempLotNo = bitLotNo,
				@numTempQty = numQty,
				@numTempOppID = numOppId,
				@numTempOppItemID = numOppItemID,
				@numTempWareHouseItmsDTLID = numWareHouseItmsDTLID
			FROM
				@TempOppSerial
			WHERE
				ID = @i

			-- LOT ITEM
			IF @bitTempLotNo = 1
			BEGIN
				-- IF RETURN QTY IS SAME AS ITEM ORDERED QTY DELETE ROW ELSE DECREASE QTY
				IF ISNULL((SELECT numQty FROM OppWarehouseSerializedItem WHERE numOppID=@numTempOppID AND numOppItemID=@numTempOppItemID AND numWarehouseItmsDTLID=@numTempWareHouseItmsDTLID),0) = @numTempQty
				BEGIN
					DELETE FROM OppWarehouseSerializedItem WHERE numOppID=@numTempOppID AND numOppItemID=@numTempOppItemID AND numWarehouseItmsDTLID=@numTempWareHouseItmsDTLID
				END
				ELSE
				BEGIN
					UPDATE 
						OppWarehouseSerializedItem
					SET 
						numQty = ISNULL(numQty,0) - ISNULL(@numTempQty,0)
					WHERE
						numOppID=@numTempOppID 
						AND numOppItemID=@numTempOppItemID 
						AND numWarehouseItmsDTLID=@numTempWareHouseItmsDTLID
				END
			END
			ELSE
			BEGIN
				DELETE FROM OppWarehouseSerializedItem WHERE numOppID=@numTempOppID AND numOppItemID=@numTempOppItemID AND numWarehouseItmsDTLID=@numTempWareHouseItmsDTLID
			END

			SET @i = @i + 1
		END

        EXEC usp_ManageRMAInventory @numReturnHeaderID,@numDomainId,@numUserCntID,1 
    END   
    
	SET @tintType=(CASE When @tintReturnType=1 AND @tintReceiveType=1 THEN 5 
				       When @tintReturnType=1 AND @tintReceiveType=2 THEN 3 
				       When @tintReturnType=2 AND @tintReceiveType=2 THEN 4
				       ELSE 0 
					END) 
		
	PRINT @tintType

	IF 	@tintType>0
	BEGIN
		EXEC dbo.USP_UpdateBizDocNameTemplate @tintType, @numDomainID,@numReturnHeaderID
    END    
    	
    DECLARE @numBizdocTempID AS NUMERIC(18, 0);
	SET @numBizdocTempID=0
        
    IF @tintReturnType=1 OR @tintReturnType=2 
    BEGIN
		IF @tintReceiveType = 2 AND @tintReturnType=1
		BEGIN
		SELECT TOP 1 @numBizdocTempID = numBizDocTempID FROM    BizDocTemplate
			WHERE   numOppType = 1 AND bitDefault = 1 AND bitEnabled = 1
					AND numDomainID = @numDomainID AND numBizDocID IN (SELECT TOP 1 numListItemID
					FROM    dbo.ListDetails WHERE   vcData = 'Sales Credit Memo' AND constFlag = 1 )
		END
		ELSE IF @tintReturnType=1 OR @tintReceiveType=1
        BEGIN 
            SELECT TOP 1 @numBizdocTempID = numBizdocTempID FROM BizDocTemplate
                WHERE   numOppType = 1 AND bitDefault = 1 AND bitEnabled = 1 AND numDomainID = @numDomainID
					AND numBizDocID = ( SELECT TOP 1 numListItemID 
                    FROM    dbo.ListDetails WHERE   vcData = 'Refund Receipt' AND constFlag = 1)
        END
        ELSE IF @tintReceiveType = 2 AND @tintReturnType=2
        BEGIN
                SELECT TOP 1 @numBizdocTempID = numBizDocTempID FROM    BizDocTemplate
                WHERE   numOppType = 2 AND bitDefault = 1 AND bitEnabled = 1
                        AND numDomainID = @numDomainID AND numBizDocID IN (SELECT TOP 1 numListItemID
                        FROM    dbo.ListDetails WHERE   vcData = 'Purchase Credit Memo' AND constFlag = 1 )
        END 
	END
		
	DECLARE @monAmount AS money,@monTotalTax  AS money,@monTotalDiscount AS  money  
		
	SET @tintType=CASE When @tintReturnType=1 AND @tintReceiveType=1 THEN 9 
				    When @tintReturnType=1 AND @tintReceiveType=2 THEN 7 
				    When @tintReturnType=1 AND @tintReceiveType=3 THEN 9 
				    When @tintReturnType=2 AND @tintReceiveType=2 THEN 8
				    When @tintReturnType=3 THEN 10
				    When @tintReturnType=4 THEN 9 
					END 
				       
	SELECT @monAmount=monAmount,@monTotalTax=monTotalTax,@monTotalDiscount=monTotalDiscount FROM dbo.GetReturnDealAmount(@numReturnHeaderID,@tintType)
		
    UPDATE  dbo.ReturnHeader
    SET     tintReceiveType = @tintReceiveType,
            numReturnStatus = @numReturnStatus,
            numModifiedBy = @numUserCntID,
            dtModifiedDate = GETUTCDATE(),
            numAccountID = @numAccountID,
            vcCheckNumber = @vcCheckNumber,
            IsCreateRefundReceipt = @IsCreateRefundReceipt,
            numBizdocTempID = CASE When @numBizdocTempID>0 THEN @numBizdocTempID ELSE numBizdocTempID END,
            monBizDocAmount= @monAmount + @monTotalTax - @monTotalDiscount,
            numItemCode = @numItemCode
    WHERE   numReturnHeaderID = @numReturnHeaderID
        
	DECLARE @numDepositIDRef AS NUMERIC(18,0)
	SELECT @numDepositIDRef = ISNULL(numDepositIDRef,0) FROM [dbo].[ReturnHeader] AS RH WHERE [RH].[numReturnHeaderID] = @numReturnHeaderID
	PRINT @numDepositIDRef

	IF @numDepositIDRef > 0
	BEGIN
		IF NOT EXISTS(SELECT * FROM [dbo].[DepositeDetails] AS DD WHERE [DD].[numDepositID] = @numDepositIDRef) AND @tintReceiveType = 1 AND @tintReturnType = 4
		BEGIN
			UPDATE dbo.DepositMaster SET [monAppliedAmount] = ISNULL((SELECT SUM([ReturnHeader].[monAmount]) FROM [dbo].[ReturnHeader] 
																	WHERE [ReturnHeader].[numDepositIDRef] = @numDepositIDRef),0)
											,[numReturnHeaderID] = @numReturnHeaderID	
											,[monRefundAmount] = [monDepositAmount] - ISNULL((SELECT SUM([ReturnHeader].[monAmount]) FROM [dbo].[ReturnHeader] WHERE [ReturnHeader].[numDepositIDRef] = @numDepositIDRef),0) 
			WHERE numDepositId=@numDepositIDRef

		END
		ELSE
		BEGIN
			UPDATE dbo.DepositMaster SET [monAppliedAmount] = @monAmount + ISNULL((SELECT SUM([DD].[monAmountPaid]) FROM [dbo].[DepositeDetails] AS DD WHERE [DD].[numDepositID] = @numDepositIDRef),0)
											,[monRefundAmount] = [monDepositAmount] - ISNULL((SELECT SUM([DD].[monAmountPaid]) FROM [dbo].[DepositeDetails] AS DD WHERE [DD].[numDepositID] = @numDepositIDRef),0)
			WHERE numDepositId=@numDepositIDRef						
		END
	END
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
END
GO
/****** Object:  StoredProcedure [USP_ManageReturnHeaderItems]    ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ManageReturnHeaderItems' ) 
    DROP PROCEDURE USP_ManageReturnHeaderItems
GO
CREATE PROCEDURE [USP_ManageReturnHeaderItems]
    (
      @numReturnHeaderID NUMERIC(18, 0) OUTPUT,
      @vcRMA VARCHAR(100),
      @vcBizDocName VARCHAR(100),
      @numDomainId NUMERIC(18, 0),
      @numUserCntID AS NUMERIC(9) = 0,
      @numDivisionId NUMERIC(18, 0),
      @numContactId NUMERIC(18, 0),
      @numOppId NUMERIC(18, 0),
      @tintReturnType TINYINT,
      @numReturnReason NUMERIC(18, 0),
      @numReturnStatus NUMERIC(18, 0),
      @monAmount MONEY,
      @monTotalTax MONEY,
      @monTotalDiscount MONEY,
      @tintReceiveType TINYINT,
      @vcComments TEXT,
      @strItems TEXT = '',
      @tintMode TINYINT = 0,
      @numBillAddressId NUMERIC(18, 0),
      @numShipAddressId NUMERIC(18, 0),
      @numDepositIDRef NUMERIC(18,0),
      @numBillPaymentIDRef NUMERIC(18,0),
	  @numParentID NUMERIC(18,0) = 0
    )
AS 
    BEGIN 
        DECLARE @hDocItem INT                                                                                                                                                                
 
        IF @numReturnHeaderID = 0 
            BEGIN             
				--Set Default Class If enable User Level Class Accountng 
				  DECLARE @numAccountClass AS NUMERIC(18);SET @numAccountClass=0
				  SELECT @numAccountClass=ISNULL(numDefaultClass,0) FROM dbo.UserMaster UM JOIN dbo.Domain D ON UM.numDomainID=D.numDomainId
				  WHERE D.numDomainId=@numDomainId AND UM.numUserDetailId=@numUserCntID AND ISNULL(D.IsEnableUserLevelClassTracking,0)=1
                   
                       INSERT  INTO [ReturnHeader]([vcRMA],[vcBizDocName],[numDomainId],[numDivisionId],[numContactId],
                                  [numOppId],[tintReturnType],[numReturnReason],[numReturnStatus],[monAmount],
                                  [monTotalTax],[monTotalDiscount],[tintReceiveType],[vcComments],
                                  [numCreatedBy],[dtCreatedDate],monBizDocAmount,monBizDocUsedAmount,numDepositIDRef,numBillPaymentIDRef,numAccountClass,numParentID,IsUnappliedPayment)
                                SELECT  @vcRMA,@vcBizDocName,@numDomainId,@numDivisionId,@numContactId,@numOppId,
                                        @tintReturnType,@numReturnReason,@numReturnStatus,@monAmount,
                                        @monTotalTax,@monTotalDiscount,@tintReceiveType,@vcComments,@numUserCntID,GETUTCDATE(),0,0,@numDepositIDRef,@numBillPaymentIDRef,@numAccountClass,
										@numParentID,
										(CASE WHEN (SELECT [DM].[tintDepositePage] FROM [dbo].[DepositMaster] AS DM 
													WHERE DM.[numDepositId] = @numDepositIDRef AND [DM].[numDomainId] = @numDomainId) = 2 THEN 1
											  ELSE 0
										END)
                    
                        SET @numReturnHeaderID = SCOPE_IDENTITY()                                        
                        SELECT  @numReturnHeaderID

                        --Update DepositMaster if Refund UnApplied Payment
                        IF @numDepositIDRef>0 AND @tintReturnType=4
                        BEGIN
							UPDATE dbo.DepositMaster SET monRefundAmount=@monAmount WHERE numDepositId=@numDepositIDRef
						END
						
						--Update BillPaymentHeader if Refund UnApplied Payment
						IF @numBillPaymentIDRef>0 AND @tintReturnType=4
						BEGIN
							UPDATE dbo.BillPaymentHeader SET monRefundAmount=@monAmount WHERE numBillPaymentID=@numBillPaymentIDRef
						END
						                        
      --                  --Update DepositMaster if Refund UnApplied Payment
      --                  IF @numDepositIDRef>0 AND @tintReturnType=4
      --                  BEGIN
						--	IF EXISTS(SELECT numDepositIDRef FROM [dbo].[ReturnHeader] AS RH WHERE [RH].numDepositIDRef=@numDepositIDRef)
						--	BEGIN
						--		UPDATE dbo.DepositMaster SET monRefundAmount = [monDepositAmount] - @monAmount + (ISNULL((SELECT SUM([ReturnHeader].[monAmount]) FROM [dbo].[ReturnHeader] WHERE [ReturnHeader].[numDepositIDRef] = @numDepositIDRef),0))
						--		--,[numReturnHeaderID] = @numReturnHeaderID 
						--		WHERE numDepositId=@numDepositIDRef
						--	END
						--	ELSE
						--	BEGIN
						--		UPDATE dbo.DepositMaster SET monRefundAmount = @monAmount
						--		--,[numReturnHeaderID] = @numReturnHeaderID 
						--		WHERE numDepositId=@numDepositIDRef
						--	END
						--END
						
						----Update BillPaymentHeader if Refund UnApplied Payment
						--IF @numBillPaymentIDRef>0 AND @tintReturnType=4
						--BEGIN
						--	UPDATE dbo.BillPaymentHeader SET monRefundAmount=@monAmount, [numReturnHeaderID] = @numReturnHeaderID WHERE numBillPaymentID=@numBillPaymentIDRef
						--END
						
                        DECLARE @tintType TINYINT 
                        SET @tintType=CASE When @tintReturnType=1 OR @tintReturnType=2 THEN 7 
												WHEN @tintReturnType=3 THEN 6
												WHEN @tintReturnType=4 THEN 5 END
												
                        EXEC dbo.USP_UpdateBizDocNameTemplate
								@tintType =  @tintType, --  tinyint
								@numDomainID = @numDomainID, --  numeric(18, 0)
								@RecordID = @numReturnHeaderID --  numeric(18, 0)

                        DECLARE @numRMATempID AS NUMERIC(18, 0)
                        DECLARE @numBizdocTempID AS NUMERIC(18, 0)
                        
                        --                            IF @tintReceiveType = 1 
--                            BEGIN 
--                                SELECT TOP 1 @numBizdocTempID = numBizdocTempID FROM BizDocTemplate
--                                WHERE   numOppType = 1 AND bitDefault = 1 AND bitEnabled = 1 AND numDomainID = @numDomainID
--                                        AND numBizDocID = ( SELECT TOP 1 numListItemID 
--                                        FROM    dbo.ListDetails WHERE   vcData = 'Refund Receipt' AND constFlag = 1)
--                            END
--							ELSE IF @tintReceiveType = 2 AND @tintReturnType=1
--                            BEGIN
--                                SELECT TOP 1 @numBizdocTempID = numBizDocTempID FROM    BizDocTemplate
--                                WHERE   numOppType = 1 AND bitDefault = 1 AND bitEnabled = 1
--                                        AND numDomainID = @numDomainID AND numBizDocID IN (SELECT TOP 1 numListItemID
--                                        FROM    dbo.ListDetails WHERE   vcData = 'Sales Credit Memo' AND constFlag = 1 )
--                            END
--                            ELSE IF @tintReceiveType = 2 AND @tintReturnType=2
--                            BEGIN
--                                SELECT TOP 1 @numBizdocTempID = numBizDocTempID FROM    BizDocTemplate
--                                WHERE   numOppType = 1 AND bitDefault = 1 AND bitEnabled = 1
--                                        AND numDomainID = @numDomainID AND numBizDocID IN (SELECT TOP 1 numListItemID
--                                        FROM    dbo.ListDetails WHERE   vcData = 'Purchase Credit Memo' AND constFlag = 1 )
--                            END

                        IF @tintReturnType=1 
                        BEGIN
							SELECT TOP 1 @numRMATempID = numBizDocTempID FROM BizDocTemplate
							WHERE   numOppType = 1 AND bitDefault = 1 AND bitEnabled = 1
                                AND numDomainID = @numDomainID AND numBizDocID IN ( SELECT TOP 1 numListItemID 
									    FROM   dbo.ListDetails WHERE  vcData = 'RMA' AND constFlag = 1 )							
						END
						ELSE IF @tintReturnType=2
                        BEGIN
							SELECT TOP 1 @numRMATempID = numBizDocTempID FROM BizDocTemplate
							WHERE   numOppType = 2 AND bitDefault = 1 AND bitEnabled = 1
                                AND numDomainID = @numDomainID AND numBizDocID IN ( SELECT TOP 1 numListItemID 
									    FROM   dbo.ListDetails WHERE  vcData = 'RMA' AND constFlag = 1 )	
						END			    
                        ELSE IF @tintReturnType=3
                        BEGIN
							SELECT TOP 1 @numRMATempID = numBizDocTempID FROM    BizDocTemplate
							WHERE   numOppType = 1 AND bitDefault = 1 AND bitEnabled = 1
                                AND numDomainID = @numDomainID AND numBizDocID IN ( SELECT TOP 1 numListItemID 
                                FROM   dbo.ListDetails WHERE  vcData = 'Credit Memo' AND constFlag = 1 )							
						END
						ELSE IF @tintReturnType=4
                        BEGIN
							SELECT TOP 1 @numRMATempID = numBizDocTempID FROM    BizDocTemplate
							WHERE   numOppType = 1 AND bitDefault = 1 AND bitEnabled = 1
                                AND numDomainID = @numDomainID AND numBizDocID IN ( SELECT TOP 1 numListItemID 
                                FROM   dbo.ListDetails WHERE  vcData = 'Refund Receipt' AND constFlag = 1 )							
                                
                            SET @numBizdocTempID=@numRMATempID    
						END
                                               
                        UPDATE  dbo.ReturnHeader
                        SET     numRMATempID = ISNULL(@numRMATempID,0),
                                numBizdocTempID = ISNULL(@numBizdocTempID,0)
                        WHERE   numReturnHeaderID = @numReturnHeaderID
			                
			                --Add/Update Address Details
        SET @numOppID = NULLIF(@numOppID, 0)
		            
        DECLARE @vcStreet VARCHAR(100),
            @vcCity VARCHAR(50),
            @vcPostalCode VARCHAR(15),
            @vcCompanyName VARCHAR(100),
            @vcAddressName VARCHAR(50)      
        DECLARE @numState NUMERIC(9),
            @numCountry NUMERIC(9),
            @numCompanyId NUMERIC(9) 
        DECLARE @bitIsPrimary BIT ;

        SELECT  @vcCompanyName = vcCompanyName,
                @numCompanyId = div.numCompanyID
        FROM    CompanyInfo Com
                JOIN divisionMaster Div ON div.numCompanyID = com.numCompanyID
        WHERE   div.numdivisionID = @numDivisionId

--Bill Address
        IF @numBillAddressId > 0 
            BEGIN
                SELECT  @vcStreet = ISNULL(vcStreet, ''),
                        @vcCity = ISNULL(vcCity, ''),
                        @vcPostalCode = ISNULL(vcPostalCode, ''),
                        @numState = ISNULL(numState, 0),
                        @numCountry = ISNULL(numCountry, 0),
                        @bitIsPrimary = bitIsPrimary,
                        @vcAddressName = vcAddressName
                FROM    dbo.AddressDetails
                WHERE   numDomainID = @numDomainID
                        AND numAddressID = @numBillAddressId
                EXEC dbo.USP_UpdateOppAddress @numOppID = @numOppID, --  numeric(9, 0)
                    @byteMode = 0, --  tinyint
                    @vcStreet = @vcStreet, --  varchar(100)
                    @vcCity = @vcCity, --  varchar(50)
                    @vcPostalCode = @vcPostalCode, --  varchar(15)
                    @numState = @numState, --  numeric(9, 0)
                    @numCountry = @numCountry, --  numeric(9, 0)
                    @vcCompanyName = @vcCompanyName, --  varchar(100)
                    @numCompanyId = 0, --  numeric(9, 0)
                    @vcAddressName = @vcAddressName,
                    @numReturnHeaderID = @numReturnHeaderID
            END
    
  --Ship Address
        IF @numShipAddressId > 0 
            BEGIN
                SELECT  @vcStreet = ISNULL(vcStreet, ''),
                        @vcCity = ISNULL(vcCity, ''),
                        @vcPostalCode = ISNULL(vcPostalCode, ''),
                        @numState = ISNULL(numState, 0),
                        @numCountry = ISNULL(numCountry, 0),
                        @bitIsPrimary = bitIsPrimary,
                        @vcAddressName = vcAddressName
                FROM    dbo.AddressDetails
                WHERE   numDomainID = @numDomainID
                        AND numAddressID = @numShipAddressId
 
                EXEC dbo.USP_UpdateOppAddress @numOppID = @numOppID, --  numeric(9, 0)
                    @byteMode = 1, --  tinyint
                    @vcStreet = @vcStreet, --  varchar(100)
                    @vcCity = @vcCity, --  varchar(50)
                    @vcPostalCode = @vcPostalCode, --  varchar(15)
                    @numState = @numState, --  numeric(9, 0)
                    @numCountry = @numCountry, --  numeric(9, 0)
                    @vcCompanyName = @vcCompanyName, --  varchar(100)
                    @numCompanyId = @numCompanyId, --  numeric(9, 0)
                    @vcAddressName = @vcAddressName,
                    @numReturnHeaderID = @numReturnHeaderID
            END
            
							IF @tintReturnType=1
							BEGIN    
								IF @numOppId>0
								BEGIN
									INSERT dbo.OpportunityMasterTaxItems (
											numReturnHeaderID,
											numTaxItemID,
											fltPercentage
										) SELECT @numReturnHeaderID,numTaxItemID,fltPercentage FROM OpportunityMasterTaxItems WHERE numOppID=@numOppID
								END
								ELSE
								BEGIN 
															--Insert Tax for Division                       
										INSERT dbo.OpportunityMasterTaxItems (
											numReturnHeaderID,
											numTaxItemID,
											fltPercentage
										) SELECT @numReturnHeaderID,TI.numTaxItemID,dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,TI.numTaxItemID,NULL,0,@numReturnHeaderID) FROM TaxItems TI JOIN DivisionTaxTypes DTT ON TI.numTaxItemID = DTT.numTaxItemID
										 WHERE DTT.numDivisionID=@numDivisionId AND DTT.bitApplicable=1
										   union 
										  select @numReturnHeaderID,0,dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,0,NULL,1,@numReturnHeaderID) 
										  FROM dbo.DivisionMaster WHERE bitNoTax=0 AND numDivisionID=@numDivisionID
								END			  
							END
          
          IF CONVERT(VARCHAR(10), @strItems) <> '' 
            BEGIN
                PRINT 1
                EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems                                                                                                             
                SELECT  *
                INTO    #temp
                FROM    OPENXML (@hDocItem, '/NewDataSet/Item',2) WITH ( numReturnItemID NUMERIC, numItemCode NUMERIC, numUnitHour NUMERIC, numUnitHourReceived NUMERIC, monPrice MONEY, monTotAmount MONEY, vcItemDesc VARCHAR(1000), numWareHouseItemID NUMERIC, vcModelID VARCHAR(200), vcManufacturer VARCHAR(250), numUOMId NUMERIC,numOppItemID NUMERIC )

                EXEC sp_xml_removedocument @hDocItem 
                
                        INSERT  INTO [ReturnItems]
                                (
                                  [numReturnHeaderID],
                                  [numItemCode],
                                  [numUnitHour],
                                  [numUnitHourReceived],
                                  [monPrice],
                                  [monTotAmount],
                                  [vcItemDesc],
                                  [numWareHouseItemID],
                                  [vcModelID],
                                  [vcManufacturer],
                                  [numUOMId],numOppItemID
                                )
                                SELECT  @numReturnHeaderID,
                                        numItemCode,
                                        numUnitHour,
                                        0,
                                        monPrice,
                                        monTotAmount,
                                        vcItemDesc,
                                        NULLIF(numWareHouseItemID, 0) numWareHouseItemID,
                                        vcModelID,
                                        vcManufacturer,
                                        numUOMId,numOppItemID
                                FROM    #temp
                                WHERE   numReturnItemID = 0
					
                     
                DROP TABLE #temp
   
						IF @tintReturnType=1
						BEGIN               
							IF @numOppId>0
								BEGIN
									INSERT INTO dbo.OpportunityItemsTaxItems (
										numReturnHeaderID,numReturnItemID,numTaxItemID
									)  SELECT @numReturnHeaderID,OI.numReturnItemID,IT.numTaxItemID 
									FROM dbo.ReturnItems OI JOIN dbo.OpportunityItemsTaxItems IT ON OI.numOppItemID=IT.numOppItemID 
									WHERE OI.numReturnHeaderID=@numReturnHeaderID AND IT.numOppId=@numOppId
								END
								ELSE
								BEGIN
										--Delete Tax for ReturnItems if item deleted 
						--DELETE FROM OpportunityItemsTaxItems WHERE numReturnHeaderID=@numReturnHeaderID AND numReturnItemID NOT IN (SELECT numReturnItemID FROM ReturnItems WHERE numReturnHeaderID=@numReturnHeaderID)

						--Insert Tax for ReturnItems
						INSERT INTO dbo.OpportunityItemsTaxItems (
							numReturnHeaderID,
							numReturnItemID,
							numTaxItemID
						) SELECT @numReturnHeaderID,OI.numReturnItemID,TI.numTaxItemID FROM dbo.ReturnItems OI JOIN dbo.ItemTax IT ON OI.numItemCode=IT.numItemCode JOIN
						TaxItems TI ON TI.numTaxItemID=IT.numTaxItemID WHERE OI.numReturnHeaderID=@numReturnHeaderID AND IT.bitApplicable=1  
						--AND OI.numReturnItemID NOT IN (SELECT numReturnItemID FROM OpportunityItemsTaxItems WHERE numReturnHeaderID=@numReturnHeaderID)
						UNION
						  select @numReturnHeaderID,OI.numReturnItemID,0 FROM dbo.ReturnItems OI JOIN dbo.Item I ON OI.numItemCode=I.numItemCode
						   WHERE OI.numReturnHeaderID=@numReturnHeaderID  AND I.bitTaxable=1
						--AND OI.numReturnItemID NOT IN (SELECT numReturnItemID FROM OpportunityItemsTaxItems WHERE numReturnHeaderID=@numReturnHeaderID)
							END
						END
            END 
            
            -- Update leads,prospects to Accounts
		EXEC dbo.USP_UpdateCRMTypeToAccounts @numDivisionID ,@numDomainID ,@numUserCntID
                          
            END    
        ELSE IF @numReturnHeaderID <> 0 
                BEGIN    
                   IF ISNULL(@tintMode, 0) = 0
                   BEGIN
				   			UPDATE  [ReturnHeader]
							SET     [vcRMA] = @vcRMA,
									[numReturnReason] = @numReturnReason,
									[numReturnStatus] = @numReturnStatus,
									[monAmount] = ISNULL(monAmount,0) + ISNULL(monTotalDiscount,0) - ISNULL(@monTotalDiscount,0),
									[monTotalDiscount] = @monTotalDiscount,
									[tintReceiveType] = @tintReceiveType,
									[vcComments] = @vcComments,
									[numModifiedBy] = @numUserCntID,
									[dtModifiedDate] = GETUTCDATE()
							WHERE   [numDomainId] = @numDomainId
									AND [numReturnHeaderID] = @numReturnHeaderID
				   END                 
                   ELSE IF ISNULL(@tintMode, 0) = 1 
                   BEGIN
                   	UPDATE  ReturnHeader
								SET     numReturnStatus = @numReturnStatus
								WHERE   numReturnHeaderID = @numReturnHeaderID
							
				   	IF CONVERT(VARCHAR(10), @strItems) <> '' 
					BEGIN
						EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems                                                                                                             
						SELECT  *
						INTO    #temp1
						FROM    OPENXML (@hDocItem, '/NewDataSet/Item',2) WITH ( numReturnItemID NUMERIC, numUnitHourReceived NUMERIC, numWareHouseItemID NUMERIC)
						EXEC sp_xml_removedocument @hDocItem 

								
								UPDATE  [ReturnItems]
								SET     [numUnitHourReceived] = X.numUnitHourReceived,
										[numWareHouseItemID] =CASE WHEN X.numWareHouseItemID=0 THEN NULL ELSE X.numWareHouseItemID END 
								FROM    #temp1 AS X
								WHERE   X.numReturnItemID = ReturnItems.numReturnItemID
										AND ReturnItems.numReturnHeaderID = @numReturnHeaderID
						DROP TABLE #temp1
				END 
			END
       END            
    END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageRMALotSerial')
DROP PROCEDURE USP_ManageRMALotSerial
GO
CREATE PROCEDURE [dbo].[USP_ManageRMALotSerial]  
    @numDomainId as numeric(9)=0,  
    @numReturnHeaderID as numeric(9)=0,  
    @numWareHouseItemID as numeric(9)=0,
    @numReturnItemID as numeric(9)=0,
    @tintMode AS TINYINT,
    @strItems as VARCHAR(1000)=''
AS  

	CREATE TABLE #tempLotSerial 
	(                                                                    
		ROWNUMBER INT IDENTITY(1,1),
		numWarehouseItemDTLID NUMERIC(18,0),
		numQty INT                                              
	)                       
  
	DECLARE @posComma INT, @strKeyVal VARCHAR(20)

	SET @strItems=RTRIM(@strItems)

	IF RIGHT(@strItems, 1) != ',' 
		SET @strItems = @strItems + ','

	SET @posComma=PATINDEX('%,%', @strItems)

	WHILE @posComma>1
	BEGIN
		SET @strKeyVal=LTRIM(RTRIM(SUBSTRING(@strItems, 1, @posComma-1)))
	
		DECLARE @posBStart INT,@posBEnd int, @strQty varchar(20),@strName varchar(20)

		SET @posBStart=PATINDEX('%(%', @strKeyVal)
		SET @posBEnd=PATINDEX('%)%', @strKeyVal)

		IF( @posBStart>1 AND @posBEnd>1)
		BEGIN
			SET @strName=LTRIM(RTRIM(SUBSTRING(@strKeyVal, 1, @posBStart-1)))
			SET	@strQty=LTRIM(RTRIM(SUBSTRING(@strKeyVal, @posBStart+1,LEN(@strKeyVal)-@posBStart-1)))
		END		
		ELSE
		BEGIN
			SET @strName=@strKeyVal
			SET	@strQty=1
		END
	  
		INSERT INTO #tempLotSerial 
		(
			numWarehouseItemDTLID,
			numQty
		)  
		VALUES
		(
			CAST(@strName AS NUMERIC(18,0)),
			CAST(@strQty AS INT)
		) 
	  
		 SET @strItems=SUBSTRING(@strItems, @posComma +1, LEN(@strItems)-@posComma)
		 SET @posComma=PATINDEX('%,%',@strItems)
	END

	DELETE FROM OppWarehouseSerializedItem WHERE numReturnHeaderID=@numReturnHeaderID AND numReturnItemID=@numReturnItemID

	DECLARE @minROWNUMBER INT
	DECLARE @maxROWNUMBER INT
	DECLARE @numWareHouseItmsDTLID NUMERIC(9)
	DECLARE @numQty AS NUMERIC(9)

	SELECT  @minROWNUMBER = MIN(ROWNUMBER) , @maxROWNUMBER =MAX(ROWNUMBER) FROM #tempLotSerial

	WHILE  @minROWNUMBER <= @maxROWNUMBER
	BEGIN
		SELECT  @numWareHouseItmsDTLID=numWarehouseItemDTLID, @numQty=numQty FROM #tempLotSerial X WHERE X.ROWNUMBER=@minROWNUMBER
    
		INSERT INTO OppWarehouseSerializedItem
		(
			numWarehouseItmsDTLID,
			numReturnHeaderID,
			numReturnItemID,
			numWarehouseItmsID,
			numQty
		)  
		SELECT  
			@numWareHouseItmsDTLID,
			@numReturnHeaderID,
			@numReturnItemID,
			@numWareHouseItemID,
			@numQty 
		
		
		SET @minROWNUMBER = @minROWNUMBER + 1
	END	
				
	DROP TABLE #tempLotSerial
GO
/****** Object:  StoredProcedure [dbo].[USP_OppBizDocs]    Script Date: 03/25/2009 15:23:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_oppbizdocs')
DROP PROCEDURE usp_oppbizdocs
GO
CREATE PROCEDURE [dbo].[USP_OppBizDocs]
(                        
 @byteMode as tinyint=null,                        
 @numOppId as numeric(9)=null,                        
 @numOppBizDocsId as numeric(9)=null,
 @ClientTimeZoneOffset AS INT=0,
 @numDomainID AS NUMERIC(18,0) = NULL,
 @numUserCntID AS NUMERIC(18,0) = NULL
)                        
as                        
                                          
if @byteMode= 1                        
begin 
BEGIN TRY
BEGIN TRANSACTION
DECLARE @numBizDocID AS INT
DECLARE @bitFulfilled AS BIT
Declare @tintOppType as tinyint

SELECT @tintOppType=tintOppType FROM OpportunityMaster WHERE  numOppId=@numOppId  and numDomainID= @numDomainID 
SELECT @numBizDocID = numBizDocId, @bitFulfilled=bitFulfilled FROM OpportunityBizDocs WHERE numOppBizDocsId = @numOppBizDocsId

--IF it's as sales order and BizDoc is of type of fulfillment and already fulfilled revert items to allocations
IF @tintOppType = 1 AND @numBizDocID = 296 AND @bitFulfilled = 1
BEGIN
	EXEC USP_OpportunityBizDocs_RevertFulFillment @numDomainID,@numUserCntID,@numOppId,@numOppBizDocsId
END


DELETE FROM [ProjectsOpportunities] WHERE numOppBizDocID=@numOppBizDocsId
--delete from OpportunityBizDocTaxItems where numOppBizDocID=@numOppBizDocsId

DELETE FROM [ShippingReportItems] WHERE [numShippingReportId] IN  (SELECT numShippingReportId from [ShippingReport] WHERE [numOppBizDocId] = @numOppBizDocsId)
DELETE FROM [ShippingBox] WHERE [numShippingReportId] IN  (SELECT numShippingReportId from [ShippingReport] WHERE [numOppBizDocId] = @numOppBizDocsId)
DELETE FROM [ShippingReport] WHERE [numOppBizDocId] = @numOppBizDocsId

 --Delete All entries for current bizdoc
EXEC [USP_DeleteEmbeddedCost] @numOppBizDocsId, 0, @numDomainID 
--DELETE FROM [OpportunityBizDocsPaymentDetails] WHERE [numBizDocsPaymentDetId] IN (SELECT  [numBizDocsPaymentDetId] FROM [OpportunityBizDocsDetails] WHERE [numBizDocsPaymentDetId] IN (SELECT [numBizDocsPaymentDetId] FROM [EmbeddedCost] WHERE [numOppBizDocID]=@numOppBizDocsId))
--DELETE FROM [EmbeddedCostItems] WHERE [numEmbeddedCostID] IN (SELECT [numEmbeddedCostID] FROM [EmbeddedCost] WHERE [numOppBizDocID] = @numOppBizDocsId)
--DELETE FROM [EmbeddedCost] WHERE [numOppBizDocID] = @numOppBizDocsId
--DELETE FROM [OpportunityBizDocsDetails] WHERE [numBizDocsPaymentDetId] IN (SELECT [numBizDocsPaymentDetId] FROM [EmbeddedCost] WHERE [numOppBizDocID]=@numOppBizDocsId)


delete  from OpportunityBizDocItems where   numOppBizDocID=  @numOppBizDocsId              

--Delete Deferred BizDoc Recurring Entries
delete from OpportunityRecurring where numOppBizDocID=@numOppBizDocsId and tintRecurringType=4

UPDATE dbo.OpportunityRecurring SET numOppBizDocID=NULL WHERE numOppBizDocID=@numOppBizDocsId

DELETE FROM [RecurringTransactionReport] WHERE [numRecTranBizDocID] = @numOppBizDocsId

--Credit Balance
Declare @monCreditAmount as money;Set @monCreditAmount=0
Declare @numDivisionID as numeric(9);Set @numDivisionID=0

Select @monCreditAmount=isnull(oppBD.monCreditAmount,0),@numDivisionID=Om.numDivisionID,
@numOppId=OM.numOppId 
from OpportunityBizDocs oppBD join OpportunityMaster Om on OM.numOppId=oppBD.numOppId WHERE  numOppBizDocsId = @numOppBizDocsId

delete from [CreditBalanceHistory] where numOppBizDocsId=@numOppBizDocsId

 
IF @tintOppType=1 --Sales
	update DivisionMaster set monSCreditBalance=isnull(monSCreditBalance,0) + isnull(@monCreditAmount,0) where numDivisionID=@numDivisionID and numDomainID=@numDomainID
else IF @tintOppType=2 --Purchase
	update DivisionMaster set monPCreditBalance=isnull(monPCreditBalance,0) + isnull(@monCreditAmount,0) where numDivisionID=@numDivisionID and numDomainID=@numDomainID


delete from DocumentWorkflow where numDocID=@numOppBizDocsId and cDocType='B'      

delete from BizDocAction where numBizActionId in(select numBizActionId from BizActionDetails where numOppBizDocsId =@numOppBizDocsId and btDocType=1) 
delete from BizActionDetails where numOppBizDocsId =@numOppBizDocsId and btDocType=1

delete from OpportunityBizDocs where numOppBizDocsId=@numOppBizDocsId                        

COMMIT TRANSACTION
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH             
end                        
                        
if @byteMode= 2                        
begin  

DECLARE @BaseCurrencySymbol nVARCHAR(3);SET @BaseCurrencySymbol='$'
SELECT @BaseCurrencySymbol = ISNULL(varCurrSymbol,'$') FROM dbo.Currency 
														   WHERE numCurrencyID IN ( SELECT numCurrencyID FROM dbo.Domain 
																					WHERE numDomainId=@numDomainId)
--PRINT @BaseCurrencySymbol

																					                      
select *, isnull(X.monPAmount,0)-X.monAmountPaid as BalDue from (select  opp.numOppBizDocsId,                        
 opp.numOppId,                        
 opp.numBizDocId,                        
 ISNULL((SELECT TOP 1 numOrientation FROM BizDocTemplate WHERE numBizDocTempID = Opp.numBizDocTempID),0) AS numOrientation, 
 ISNULL((SELECT TOP 1 bitKeepFooterBottom FROM BizDocTemplate WHERE numBizDocTempID = Opp.numBizDocTempID),0) AS bitKeepFooterBottom,                  
 oppmst.vcPOppName,                        
 mst.vcData as BizDoc,                        
 opp.numCreatedBy,                        
DATEADD(minute,-@ClientTimeZoneOffset,opp.dtCreatedDate) CreatedDate,
 opp.vcRefOrderNo,                       
 vcBizDocID,
 CASE WHEN LEN(ISNULL(vcBizDocName,''))=0 THEN vcBizDocID ELSE vcBizDocName END vcBizDocName,      
  [dbo].[GetDealAmount](@numOppId ,getutcdate(),opp.numOppBizDocsId ) as monPAmount,
  case  when tintOppType=1  then (case when (isnull(numAuthoritativeSales,0)=numBizDocId OR numBizDocId=296) then 'Authoritative' + Case when isnull(Opp.tintDeferred,0)=1 then ' (Deferred)' else '' end else 'Non-Authoritative' end ) 
  when tintOppType=2  then (case when isnull(numAuthoritativePurchase,0)=numBizDocId then 'Authoritative' + Case when isnull(Opp.tintDeferred,0)=1 then ' (Deferred)' else '' end else 'Non-Authoritative' end )  end as BizDocType,
  isnull(monAmountPaid,0) monAmountPaid, 
  ISNULL(C.varCurrSymbol,'') varCurrSymbol ,
  ISNULL(Opp.[bitAuthoritativeBizDocs],0) bitAuthoritativeBizDocs,
  ISNULL(oppmst.[tintshipped] ,0) tintshipped,isnull(opp.numShipVia,0) as numShipVia,oppmst.numDivisionId,isnull(Opp.tintDeferred,0) as tintDeferred,
  ISNULL(Opp.numBizDocTempID,0) AS numBizDocTempID,con.numContactid,isnull(con.vcEmail,'') AS vcEmail,ISNULL(Opp.numBizDocStatus,0) AS numBizDocStatus,
--  case when isnumeric(ISNULL(dbo.fn_GetListItemName(isnull(oppmst.intBillingDays,0)),0))=1 
--	   then ISNULL(dbo.fn_GetListItemName(isnull(oppmst.intBillingDays,0)),0) 
--	   else 0 
--  end AS numBillingDaysName,
  CASE WHEN ISNUMERIC((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(oppmst.intBillingDays,0))) = 1
 	   THEN (SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(oppmst.intBillingDays,0))
	   ELSE 0
  END AS numBillingDaysName,
  Opp.dtFromDate,
  opp.dtFromDate AS BillingDate,
  ISNULL(C.fltExchangeRate,1) fltExchangeRateCurrent,
  ISNULL(oppmst.fltExchangeRate,1) fltExchangeRateOfOrder,
  @BaseCurrencySymbol BaseCurrencySymbol,0 AS numReturnHeaderID,0 AS tintReturnType,ISNULL(Opp.fltExchangeRateBizDoc,ISNULL(oppmst.fltExchangeRate,1)) AS fltExchangeRateBizDoc,
  CASE WHEN (SELECT COUNT(*) FROM dbo.ShippingReport WHERE numOppBizDocId = opp.numOppBizDocsId AND numDomainID = @numDomainID) > 0 THEN 1 ELSE 0 END AS [IsShippingReportGenerated],
  --CASE WHEN (SELECT ISNULL(vcTrackingNo,'') FROM dbo.OpportunityBizDocs WHERE numOppBizDocsId = opp.numOppBizDocsId) <> '' THEN 1 ELSE 0 END AS [ISTrackingNumGenerated]
  CASE WHEN (SELECT COUNT(*) FROM dbo.ShippingBox 
			 WHERE numShippingReportId IN (SELECT numShippingReportId FROM dbo.ShippingReport WHERE numOppBizDocId = opp.numOppBizDocsId AND numDomainID = @numDomainID)
			 AND LEN(ISNULL(vcTrackingNumber,'')) > 0) > 0 THEN 1 ELSE 0 END AS [ISTrackingNumGenerated],
  ISNULL((SELECT MAX(numShippingReportId) from ShippingReport SR 
		  WHERE SR.numOppBizDocId = opp.numoppbizdocsid 
		  AND SR.numDomainID = oppmst.numDomainID),0) AS numShippingReportId,
  ISNULL((SELECT MAX(SB.numShippingReportId) FROM [ShippingBox] SB 
		  JOIN dbo.ShippingReport SR ON SB.numShippingReportId = SR.numShippingReportId 
		  where SR.numOppBizDocId = opp.numoppbizdocsid AND SR.numDomainID = oppmst.numDomainID 
		  AND LEN(ISNULL(SB.vcTrackingNumber,''))>0),0) AS numShippingLabelReportId			 
 ,ISNULL(STUFF(( SELECT  ',' +  CASE WHEN LEN(ISNULL(W.[vcWareHouse],'')  + ':' + ISNULL([WL].[vcLocation],'')) > 0 THEN ISNULL(W.[vcWareHouse],'')  + ':' + ISNULL([WL].[vcLocation],'') ELSE '' END
                FROM    [dbo].[WarehouseLocation] AS WL
				JOIN [dbo].[Warehouses] W ON [WL].[numWarehouseID] = [W].[numWareHouseID]
                WHERE  [WL].[numWLocationID] IN (
                        SELECT  WHI.[numWLocationID]
                        FROM    [dbo].[WareHouseItems] AS WHI
                                JOIN [dbo].[OpportunityItems] AS OI ON [WHI].[numWareHouseItemID] = [OI].[numWarehouseItmsID]
                        WHERE   [OI].[numOppId] = oppmst.[numOppId])
              FOR
                XML PATH('')
              ), 1, 1, ''),'') AS [vcLocation],
		(CASE WHEN ISNULL(RecurrenceConfiguration.numRecConfigID,0) = 0 THEN 0 ELSE 1 END) AS bitRecur,
		ISNULL(Opp.bitRecurred,0) AS bitRecurred,
		ISNULL(CMP.numCompanyType,0) AS numCompanyType,
		ISNULL(CMP.vcProfile,0) AS vcProfile
 FROM                         
 OpportunityBizDocs  opp                        
 left join ListDetails mst                        
 on mst.numListItemID=opp.numBizDocId                        
 join OpportunityMaster oppmst                        
 on oppmst.numOppId= opp.numOppId                                
 left join  AuthoritativeBizDocs  AB
 on AB.numDomainId=  oppmst.numDomainID             
 LEFT OUTER JOIN [Currency] C
  ON C.numCurrencyID = oppmst.numCurrencyID
  JOIN divisionmaster div ON div.numDivisionId=oppmst.numDivisionId
  JOIN CompanyInfo CMP ON CMP.numCompanyId = div.numCompanyID
  JOIN AdditionalContactsInformation con ON con.numdivisionId = div.numdivisionId AND con.numContactid = oppmst.numContactId
  LEFT JOIN
	RecurrenceConfiguration
  ON
	RecurrenceConfiguration.numOppBizDocID = opp.numOppBizDocsId
where opp.numOppId=@numOppId

UNION ALL

select  0 AS numOppBizDocsId,                        
 RH.numOppId,                        
 0 AS numBizDocId,                        
 1 AS numOrientation,  
 0 AS bitKeepFooterBottom,                   
 '' AS vcPOppName,                        
 'RMA' as BizDoc,                        
 RH.numCreatedBy,                        
DATEADD(minute,-@ClientTimeZoneOffset,RH.dtCreatedDate) CreatedDate,
 '' vcRefOrderNo,                       
 RH.vcRMA AS vcBizDocID,
 RH.vcRMA AS vcBizDocName,      
  ISNULL(monAmount,0) as monPAmount,
  'RMA' as BizDocType,
  0 AS monAmountPaid, 
  '' varCurrSymbol ,
  0 bitAuthoritativeBizDocs,
  0 tintshipped,0 as numShipVia,RH.numDivisionId,0 as tintDeferred,
  0 AS numBizDocTempID,con.numContactid,isnull(con.vcEmail,'') AS vcEmail,0 AS numBizDocStatus,
  0 AS numBillingDaysName,RH.dtCreatedDate AS dtFromDate,
  RH.dtCreatedDate AS BillingDate,
  1 fltExchangeRateCurrent,
  1 fltExchangeRateOfOrder,
  @BaseCurrencySymbol BaseCurrencySymbol,RH.numReturnHeaderID,RH.tintReturnType,1 AS fltExchangeRateBizDoc
  ,0 [IsShippingReportGenerated],0 [ISTrackingNumGenerated],
  0 AS numShippingReportId,
  0 AS numShippingLabelReportId			 
  ,ISNULL(STUFF(( SELECT  ',' +  CASE WHEN LEN(ISNULL(W.[vcWareHouse],'')  + ':' + ISNULL([WL].[vcLocation],'')) > 0 THEN ISNULL(W.[vcWareHouse],'')  + ':' + ISNULL([WL].[vcLocation],'') ELSE '' END
                FROM    [dbo].[WarehouseLocation] AS WL
				JOIN [dbo].[Warehouses] W ON [WL].[numWarehouseID] = [W].[numWareHouseID]
                WHERE  [WL].[numWLocationID] IN (
                        SELECT  WHI.[numWLocationID]
                        FROM    [dbo].[WareHouseItems] AS WHI
                                JOIN [dbo].[OpportunityItems] AS OI ON [WHI].[numWareHouseItemID] = [OI].[numWarehouseItmsID]
                        WHERE   [OI].[numOppId] = RH.[numOppId])
              FOR
                XML PATH('')
              ), 1, 1, ''),'') AS [vcLocation],
			  0 AS bitRecur,
			  0 AS bitRecurred,
			  ISNULL(CMP.numCompanyType,0) AS numCompanyType,
		ISNULL(CMP.vcProfile,0) AS vcProfile		 
from                         
 ReturnHeader RH                        
  JOIN divisionmaster div ON div.numDivisionId=RH.numDivisionId
  JOIN CompanyInfo CMP ON CMP.numCompanyId = div.numCompanyID
  LEFT JOIN AdditionalContactsInformation con ON con.numdivisionId = div.numdivisionId AND con.numContactid = RH.numContactId
where RH.numOppId=@numOppId
)X         
                        
END




/****** Object:  StoredProcedure [dbo].[USP_OPPDetails]    Script Date: 03/25/2009 15:10:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Created By Anoop Jayaraj
-- [dbo].[USP_OPPDetails] 1362,1,-333
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_oppdetails')
DROP PROCEDURE usp_oppdetails
GO
CREATE PROCEDURE [dbo].[USP_OPPDetails]
(
               @numOppID             AS NUMERIC(9)  = NULL,
               @numDomainID          AS NUMERIC(9)  = 0,
               @ClientTimeZoneOffset AS INT)
AS
	
  SELECT Opp.numoppid,
         numCampainID,
         ADC.vcFirstname + ' ' + ADC.vcLastName AS numContactId,
         isnull(ADC.vcEmail,'') AS vcEmail,
         Opp.txtComments,
         Opp.numDivisionID,
         tintOppType,
         ISNULL(dateadd(MINUTE,-@ClientTimeZoneOffset,bintAccountClosingDate),'') AS bintAccountClosingDate,
         Opp.numContactID AS ContactID,
         tintSource,
         C2.vcCompanyName + Case when isnull(D2.numCompanyDiff,0)>0 then  '  ' + dbo.fn_getlistitemname(D2.numCompanyDiff) + ':' + isnull(D2.vcCompanyDiff,'') else '' end as vcCompanyname,
         D2.tintCRMType,
         Opp.vcPoppName,
         intpEstimatedCloseDate,
         [dbo].[GetDealAmount](@numOppID,getutcdate(),0) AS monPAmount,
         lngPConclAnalysis,
         monPAmount AS OppAmount,
         numSalesOrPurType,
         Opp.tintActive,
         dbo.fn_GetContactName(Opp.numCreatedby)
           + ' '
           + CONVERT(VARCHAR(20),dateadd(MINUTE,-@ClientTimeZoneOffset,Opp.bintCreatedDate)) AS CreatedBy,
         dbo.fn_GetContactName(Opp.numModifiedBy)
           + ' '
           + CONVERT(VARCHAR(20),dateadd(MINUTE,-@ClientTimeZoneOffset,Opp.bintModifiedDate)) AS ModifiedBy,
         dbo.fn_GetContactName(Opp.numRecOwner) AS RecordOwner,
         Opp.numRecOwner,
         isnull(tintshipped,0) AS tintshipped,
         isnull(tintOppStatus,0) AS tintOppStatus,
         dbo.OpportunityLinkedItems(@numOppID) AS NoOfProjects,
         Opp.numAssignedTo,
         Opp.numAssignedBy,
         (SELECT COUNT(* )
          FROM   dbo.GenericDocuments
          WHERE  numRecID = @numOppID
                 AND vcDocumentSection = 'O') AS DocumentCount,
         isnull(OPR.numRecurringId,0) AS numRecurringId,
		 OPR.dtRecurringDate AS dtLastRecurringDate,
         D2.numTerID,
         Link.numOppID AS numParentOppID,
         Link.vcPoppName AS ParentOppName,
         Link.vcSource AS Source,
         Link.numParentProjectID,
         Link.vcProjectName,
         ISNULL(C.varCurrSymbol,'') varCurrSymbol,
         ISNULL(Opp.fltExchangeRate,0) AS [fltExchangeRate],
         ISNULL(C.chrCurrency,'') AS [chrCurrency],
         ISNULL(C.numCurrencyID,0) AS [numCurrencyID],
		 (Select Count(*) from OpportunityBizDocs 
		 where numOppId=@numOppID and bitAuthoritativeBizDocs=1) As AuthBizDocCount,
		 (SELECT COUNT(distinct numChildOppID) FROM [OpportunityLinking] OL WHERE OL.[numParentOppID] = Opp.numOppId) AS ChildCount,
		 (SELECT COUNT(*) FROM [RecurringTransactionReport] RTR WHERE RTR.[numRecTranSeedId] = Opp.numOppId)  AS RecurringChildCount
		 ,Opp.bintCreatedDate,
		 ISNULL(Opp.bitStockTransfer ,0) bitStockTransfer,	ISNULL(Opp.tintTaxOperator,0) AS tintTaxOperator, 
         isnull(Opp.intBillingDays,0) AS numBillingDays,isnull(Opp.bitInterestType,0) AS bitInterestType,
         isnull(opp.fltInterest,0) AS fltInterest,  isnull(Opp.fltDiscount,0) AS fltDiscount,
         isnull(Opp.bitDiscountType,0) AS bitDiscountType,ISNULL(Opp.bitBillingTerms,0) AS bitBillingTerms,ISNULL(OPP.numDiscountAcntType,0) AS numDiscountAcntType,
         (SELECT TOP 1 numCountry FROM fn_getOPPAddressDetails(Opp.numOppId,Opp.numDomainID,2)) AS [numShipCountry],
		 (SELECT TOP 1 numState FROM fn_getOPPAddressDetails(Opp.numOppId,Opp.numDomainID,2)) AS [numShipState],
		 (SELECT TOP 1 vcState FROM fn_getOPPAddressDetails(Opp.numOppId,Opp.numDomainID,2)) AS [vcShipState],
		 (SELECT TOP 1 vcCountry FROM fn_getOPPAddressDetails(Opp.numOppId,Opp.numDomainID,2)) AS [vcShipCountry],
		 (SELECT TOP 1 vcPostalCode FROM fn_getOPPAddressDetails(Opp.numOppId,Opp.numDomainID,2)) AS [vcPostalCode],
		 ISNULL(vcCouponCode,'') AS [vcCouponCode],ISNULL(Opp.bitPPVariance,0) AS bitPPVariance,
		 Opp.dtItemReceivedDate,ISNULL(bitUseShippersAccountNo,0) [bitUseShippersAccountNo], ISNULL(vcShippersAccountNo,'') [vcShippersAccountNo],
		 ISNULL(bitUseMarkupShippingRate,0) [bitUseMarkupShippingRate],
		 ISNULL(numMarkupShippingRate,0) [numMarkupShippingRate],
		 ISNULL(intUsedShippingCompany,0) [intUsedShippingCompany],ISNULL(Opp.[monLandedCostTotal],0) AS monLandedCostTotal,opp.[vcLanedCost],
		 ISNULL(Opp.vcRecurrenceType,'') AS vcRecurrenceType,
		 ISNULL(RecurrenceConfiguration.numRecConfigID,0) AS numRecConfigID,
		 (CASE WHEN ISNULL(Opp.vcRecurrenceType,'') = '' THEN 0 ELSE 1 END) AS bitRecur,
		 (CASE WHEN RecurrenceTransaction.numRecTranID IS NULL THEN 0 ELSE 1 END) AS bitRecurred,
		 dbo.FormatedDateFromDate(RecurrenceConfiguration.dtStartDate,@numDomainID) AS dtStartDate,
		 dbo.FormatedDateFromDate(RecurrenceConfiguration.dtEndDate,@numDomainID) AS dtEndDate,
		 RecurrenceConfiguration.vcFrequency AS vcFrequency,
		 ISNULL(RecurrenceConfiguration.bitDisabled,0) AS bitDisabled
  FROM   OpportunityMaster Opp 
         JOIN divisionMaster D2
           ON Opp.numDivisionID = D2.numDivisionID
         LEFT JOIN CompanyInfo C2
           ON C2.numCompanyID = D2.numCompanyID
         LEFT JOIN (SELECT TOP 1 OM.vcPoppName,
                                 OM.numOppID,
                                 numChildOppID,
                                 vcSource,
                                 numParentProjectID,
                                 vcProjectName
                    FROM   OpportunityLinking
                          LEFT JOIN OpportunityMaster OM
                             ON numOppID = numParentOppID
                          LEFT JOIN [ProjectsMaster]
							 ON numParentProjectID = numProId   
                    WHERE  numChildOppID = @numOppID) Link
           ON Link.numChildOppID = Opp.numOppID
         LEFT OUTER JOIN [Currency] C
					ON C.numCurrencyID = Opp.numCurrencyID
		 LEFT OUTER JOIN [OpportunityRecurring] OPR
					ON OPR.numOppId = Opp.numOppId
		 LEFT OUTER JOIN additionalContactsinformation ADC ON
		 ADC.numdivisionId = D2.numdivisionId AND  ADC.numContactId = Opp.numContactId	
		 LEFT JOIN RecurrenceConfiguration ON Opp.numOppId = RecurrenceConfiguration.numOppID AND RecurrenceConfiguration.numType = 1
		 LEFT JOIN RecurrenceTransaction ON Opp.numOppId = RecurrenceTransaction.numRecurrOppID
		WHERE  Opp.numOppId = @numOppID
         AND Opp.numDomainID = @numDomainID

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_oppdetailsdtlpl')
DROP PROCEDURE usp_oppdetailsdtlpl
GO
CREATE PROCEDURE [dbo].[USP_OPPDetailsDTLPL]
(
               @OpportunityId        AS NUMERIC(9)  = NULL,
               @numDomainID          AS NUMERIC(9),
               @ClientTimeZoneOffset AS INT)
AS
  SELECT Opp.numoppid,numCampainID,
--         (SELECT vcdata
--          FROM   listdetails
--          WHERE  numlistitemid = numCampainID) AS numCampainIDName,
		 CAMP.vcCampaignName AS [numCampainIDName],
         dbo.Fn_getcontactname(Opp.numContactId) numContactIdName,
		 numContactId,
         Opp.txtComments,
         Opp.numDivisionID,
         tintOppType,
         Dateadd(MINUTE,-@ClientTimeZoneOffset,bintAccountClosingDate) AS bintAccountClosingDate,
         dbo.fn_GetOpportunitySourceValue(ISNULL(Opp.tintSource,0),ISNULL(Opp.tintSourceType,0),Opp.numDomainID) AS tintSourceName,
          tintSource,
         Opp.VcPoppName,
         intpEstimatedCloseDate,
         [dbo].[getdealamount](@OpportunityId,Getutcdate(),0) AS CalAmount,
         --monPAmount,
         CONVERT(DECIMAL(10,2),Isnull(monPAmount,0)) AS monPAmount,
		lngPConclAnalysis,
         (SELECT vcdata
          FROM   listdetails
          WHERE  numlistitemid = lngPConclAnalysis) AS lngPConclAnalysisName,
         (SELECT vcdata
          FROM   listdetails
          WHERE  numlistitemid = numSalesOrPurType) AS numSalesOrPurTypeName,
		  numSalesOrPurType,
		 Opp.vcOppRefOrderNo,
		 Opp.vcMarketplaceOrderID,
         C2.vcCompanyName,
         D2.tintCRMType,
         Opp.tintActive,
         dbo.Fn_getcontactname(Opp.numCreatedby)
           + ' '
           + CONVERT(VARCHAR(20),Dateadd(MINUTE,-@ClientTimeZoneOffset,Opp.bintCreatedDate)) AS CreatedBy,
         dbo.Fn_getcontactname(Opp.numModifiedBy)
           + ' '
           + CONVERT(VARCHAR(20),Dateadd(MINUTE,-@ClientTimeZoneOffset,Opp.bintModifiedDate)) AS ModifiedBy,
         dbo.Fn_getcontactname(Opp.numRecOwner) AS RecordOwner,
         Opp.numRecOwner,
         Isnull(tintshipped,0) AS tintshipped,
         Isnull(tintOppStatus,0) AS tintOppStatus,
         [dbo].[GetOppStatus](Opp.numOppId) AS vcDealStatus,
         dbo.Opportunitylinkeditems(@OpportunityId) +
         (SELECT COUNT(*) FROM  dbo.Communication Comm JOIN Correspondence Corr ON Comm.numCommId=Corr.numCommID
			WHERE Comm.numDomainId=Opp.numDomainId AND Corr.numDomainID=Comm.numDomainID AND Corr.tintCorrType IN (1,2,3,4) AND Corr.numOpenRecordID=Opp.numOppId) AS NoOfProjects,
         (SELECT A.vcFirstName
                   + ' '
                   + A.vcLastName
          FROM   AdditionalContactsInformation A
          WHERE  A.numcontactid = opp.numAssignedTo) AS numAssignedToName,
		  opp.numAssignedTo,
         Opp.numAssignedBy,
         (SELECT COUNT(* )
          FROM   GenericDocuments
          WHERE  numRecID = @OpportunityId
                 AND vcDocumentSection = 'O') AS DocumentCount,
         CASE 
           WHEN tintOppStatus = 1 THEN 100
           ELSE isnull((SELECT SUM(tintPercentage)
                 FROM   OpportunityStageDetails
                 WHERE  numOppId = @OpportunityId
                        AND bitStageCompleted = 1),0)
         END AS TProgress,
         (SELECT TOP 1 Isnull(varRecurringTemplateName,'')
          FROM   RecurringTemplate
          WHERE  numRecurringId = OPR.numRecurringId) AS numRecurringIdName,
		 OPR.numRecurringId,
         Link.numOppID AS numParentOppID,
         Link.vcPoppName AS ParentOppName,
         Link.vcSource AS Source,
         ISNULL(C.varCurrSymbol,'') varCurrSymbol,
          ISNULL(Opp.fltExchangeRate,0) AS [fltExchangeRate],
           ISNULL(C.chrCurrency,'') AS [chrCurrency],
         ISNULL(C.numCurrencyID,0) AS [numCurrencyID],
         (SELECT COUNT(*) FROM [OpportunityLinking] OL WHERE OL.[numParentOppID] = Opp.numOppId) AS ChildCount,
         (SELECT COUNT(*) FROM [RecurringTransactionReport] RTR WHERE RTR.[numRecTranSeedId] = Opp.numOppId)  AS RecurringChildCount,
         (SELECT COUNT(*) FROM [ShippingReport] WHERE numOppBizDocId IN (SELECT numOppBizDocsId FROM   OpportunityBizDocs BD WHERE  BD.numOppId = @OpportunityId))AS ShippingReportCount,
         ISNULL(Opp.numStatus ,0) numStatus,
         (SELECT vcdata
          FROM   listdetails
          WHERE  numlistitemid = Opp.numStatus) AS numStatusName,
          (SELECT SUBSTRING(
	(SELECT ','+  A.vcFirstName+' ' +A.vcLastName + CASE WHEN ISNULL(SR.numContactType,0)>0 THEN '(' + dbo.fn_GetListItemName(SR.numContactType) + ')' ELSE '' END
	FROM ShareRecord SR JOIN UserMaster UM ON UM.numUserDetailId=SR.numAssignedTo 
							 JOIN AdditionalContactsInformation A ON UM.numUserDetailId=A.numContactID 
		 WHERE SR.numDomainID=Opp.numDomainID AND SR.numModuleID=3 AND SR.numRecordID=Opp.numOppId
				AND UM.numDomainID=Opp.numDomainID and UM.numDomainID=A.numDomainID FOR XML PATH('')),2,200000)) AS numShareWith,
		ISNULL((SELECT SUM(ISNULL(monDealAmount,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = @OpportunityId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0) AS monDealAmount,
		ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = @OpportunityId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0) AS monAmountPaid,
		ISNULL(Opp.tintSourceType,0) AS tintSourceType,
		ISNULL(Opp.tintTaxOperator,0) AS tintTaxOperator,  
             isnull(Opp.intBillingDays,0) AS numBillingDays,isnull(Opp.bitInterestType,0) AS bitInterestType,
             isnull(opp.fltInterest,0) AS fltInterest,  isnull(Opp.fltDiscount,0) AS fltDiscount,
             isnull(Opp.bitDiscountType,0) AS bitDiscountType,ISNULL(Opp.bitBillingTerms,0) AS bitBillingTerms,
             ISNULL(OPP.numDiscountAcntType,0) AS numDiscountAcntType,
              dbo.fn_getOPPAddress(Opp.numOppId,@numDomainID,1) AS BillingAddress,
            dbo.fn_getOPPAddress(Opp.numOppId,@numDomainID,2) AS ShippingAddress,
             ISNULL((SELECT SUBSTRING((SELECT '$^$' + ISNULL( (SELECT vcShipFieldValue FROM dbo.ShippingFieldValues WHERE numDomainID=Opp.numDomainID AND intShipFieldID IN ( SELECT intShipFieldID FROM dbo.ShippingFields WHERE numListItemID=OBD.numShipVia  AND vcFieldName='Tracking URL')),'') +'#^#'+ RTRIM(LTRIM(vcTrackingNo) )  FROM dbo.OpportunityBizDocs OBD WHERE OBD.numOppId=Opp.numOppID FOR XML PATH('')),4,200000)),'') AS  vcTrackingNo,
            ISNULL(numPercentageComplete,0) numPercentageComplete,
            dbo.GetListIemName(ISNULL(numPercentageComplete,0)) AS PercentageCompleteName,
            ISNULL(numBusinessProcessID,0) AS [numBusinessProcessID],
            CASE WHEN opp.tintOppType=1 THEN dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID) ELSE '' END AS vcInventoryStatus
            ,ISNULL(bitUseShippersAccountNo,0) [bitUseShippersAccountNo], ISNULL(vcShippersAccountNo,'') [vcShippersAccountNo] ,
			(SELECT SUM((ISNULL(monTotAmtBefDiscount, ISNULL(monTotAmount,0))- ISNULL(monTotAmount,0))) FROM OpportunityBizDocItems WHERE numOppBizDocID IN (SELECT numOppBizDocsId FROM OpportunityBizDocs WHERE numOppId = @OpportunityId AND bitAuthoritativeBizDocs = 1)) AS numTotalDiscount,
			ISNULL(Opp.vcRecurrenceType,'') AS vcRecurrenceType,
			ISNULL(Opp.bitRecurred,0) AS bitRecurred,
			(
				CASE 
				WHEN opp.tintOppType=1 AND opp.tintOppStatus = 1 
				THEN  
					dbo.GetOrderShipReceiveStatus(opp.numOppId,1,opp.tintShipped)
				ELSE 
						'' 
				END
			) AS vcOrderedShipped,
			(
				CASE 
				WHEN opp.tintOppType = 2 AND opp.tintOppStatus = 1 
				THEN  
					dbo.GetOrderShipReceiveStatus(opp.numOppId,2,opp.tintShipped)
				ELSE 
						'' 
				END
			) AS vcOrderedReceived
  FROM   OpportunityMaster Opp
         JOIN divisionMaster D2 ON Opp.numDivisionID = D2.numDivisionID
         JOIN CompanyInfo C2 ON C2.numCompanyID = D2.numCompanyID
         LEFT JOIN (SELECT TOP 1 vcPoppName,
                                 numOppID,
                                 numChildOppID,
                                 vcSource,numParentOppID,OpportunityMaster.numDivisionID AS numParentDivisionID 
                    FROM   OpportunityLinking
                          left JOIN OpportunityMaster
                             ON numOppID = numParentOppID
                    WHERE  numChildOppID = @OpportunityId) Link
           ON Link.numChildOppID = Opp.numOppID
           LEFT OUTER JOIN [Currency] C
					ON C.numCurrencyID = Opp.numCurrencyID
			LEFT OUTER JOIN [OpportunityRecurring] OPR
					ON OPR.numOppId = Opp.numOppId
					 LEFT JOIN AddressDetails AD --Billing add
				ON AD.numDomainID=D2.numDomainID AND AD.numRecordID=D2.numDivisionID AND AD.tintAddressOf = 2 AND AD.tintAddressType = 1 AND AD.bitIsPrimary=1
			 LEFT JOIN AddressDetails AD2--Shipping address
				ON AD2.numDomainID=D2.numDomainID AND AD2.numRecordID=D2.numDivisionID AND AD2.tintAddressOf = 2 AND AD2.tintAddressType = 2 AND AD2.bitIsPrimary=1	
		LEFT JOIN dbo.CampaignMaster CAMP ON CAMP.numCampaignID = Opp.numCampainID 
  WHERE  Opp.numOppId = @OpportunityId
         AND Opp.numDomainID = @numDomainID
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OPPGetINItems')
DROP PROCEDURE USP_OPPGetINItems
GO
-- =============================================  
-- Modified by: <Author,,Sachin Sadhu>  
-- Create date: <Create Date,,24thDec2013>  
-- Description: <Description,,add 3 fields:vcBizDocImagePath,vcBizDocFooter,vcPurBizDocFooter>  
-- =============================================  
CREATE PROCEDURE [dbo].[USP_OPPGetINItems]
(
          @byteMode        AS TINYINT  = NULL,
          @numOppId        AS NUMERIC(9)  = NULL,
          @numOppBizDocsId AS NUMERIC(9)  = NULL,
          @numDomainID     AS NUMERIC(9)  = 0,
          @numUserCntID    AS NUMERIC(9)  = 0,
          @ClientTimeZoneOffset INT=0)
AS
  IF @byteMode = 1
    BEGIN
      DECLARE  @BizDcocName  AS VARCHAR(100)
      DECLARE  @OppName  AS VARCHAR(100)
      DECLARE  @Contactid  AS VARCHAR(100)
      DECLARE  @shipAmount  AS MONEY
      DECLARE  @tintOppType  AS TINYINT
      DECLARE  @numlistitemid  AS VARCHAR(15)
      DECLARE  @RecOwner  AS VARCHAR(15)
      DECLARE  @MonAmount  AS VARCHAR(15)
      DECLARE  @OppBizDocID  AS VARCHAR(100)
      DECLARE  @bizdocOwner  AS VARCHAR(15)
      DECLARE  @tintBillType  AS TINYINT
      DECLARE  @tintShipType  AS TINYINT
      DECLARE  @numCustomerDivID AS NUMERIC 
      
      SELECT @RecOwner = numRecOwner,@MonAmount = CONVERT(VARCHAR(20),monPAmount),
             @Contactid = numContactId,@OppName = vcPOppName,
             @tintOppType = tintOppType,@tintBillType = tintBillToType,@tintShipType = tintShipToType
      FROM   OpportunityMaster WHERE  numOppId = @numOppId
      
      -- When Creating PO from SO and Bill type is Customer selected 
      SELECT @numCustomerDivID = ISNULL(numDivisionID,0) FROM dbo.OpportunityMaster WHERE 
			numOppID IN (SELECT  ISNULL(numParentOppID,0) FROM dbo.OpportunityLinking WHERE numChildOppID = @numOppId)
			
      --select @BizDcocName=vcBizDocID from OpportunityBizDocs where numOppBizDocsId=@numOppBizDocsId
      SELECT @BizDcocName = vcdata,
             @shipAmount = monShipCost,
             @numlistitemid = numlistitemid,
             @OppBizDocID = vcBizDocID,
             @bizdocOwner = OpportunityBizDocs.numCreatedBy
      FROM   listdetails JOIN OpportunityBizDocs ON numBizDocId = numlistitemid
      WHERE  numOppBizDocsId = @numOppBizDocsId
      
      SELECT @bizdocOwner AS BizDocOwner,
             @OppBizDocID AS BizDocID,
             @MonAmount AS Amount,
             isnull(@RecOwner,1) AS Owner,
             @BizDcocName AS BizDcocName,
             @numlistitemid AS BizDoc,
             @OppName AS OppName,
             VcCompanyName AS CompName,
             @shipAmount AS ShipAmount,
             dbo.fn_getOPPAddress(@numOppId,@numDomainID,1) AS BillAdd,
             dbo.fn_getOPPAddress(@numOppId,@numDomainID,2) AS ShipAdd,
             AD.VcStreet,
             AD.VcCity,
             isnull(dbo.fn_GetState(AD.numState),'') AS vcBilState,
             AD.vcPostalCode,
             isnull(dbo.fn_GetListItemName(AD.numCountry),
                    '') AS vcBillCountry,
             con.vcFirstName AS ConName,
             ISNULL(con.numPhone,'') numPhone,
             con.vcFax,
             isnull(con.vcEmail,'') AS vcEmail,
             div.numDivisionID,
             numContactid,
             isnull(vcFirstFldValue,'') vcFirstFldValue,
             isnull(vcSecndFldValue,'') vcSecndFldValue,
             isnull(bitTest,0) AS bitTest,
			dbo.fn_getOPPAddress(@numOppId,@numDomainID,1) AS BillToAddressName,
			dbo.fn_getOPPAddress(@numOppId,@numDomainID,1) AS ShipToAddressName,
             CASE 
               WHEN @tintBillType IS NULL THEN Com.vcCompanyName
             	-- When Sales order and bill to is set to customer 
               WHEN @tintBillType = 1 AND @tintOppType = 1 THEN Com.vcCompanyName
             -- When Create PO from SO and Bill to is set to Customer 
			   WHEN @tintBillType = 1 AND @tintOppType = 2 THEN dbo.fn_GetComapnyName(@numCustomerDivID)
               WHEN @tintBillType = 0 THEN (SELECT TOP 1 Com1.vcCompanyName
                                            FROM   companyinfo [Com1]
                                                   JOIN divisionmaster div1
                                                     ON com1.numCompanyID = div1.numCompanyID
                                                   JOIN Domain D1
                                                     ON D1.numDivisionID = div1.numDivisionID
                                                    JOIN dbo.AddressDetails AD1
														ON AD1.numDomainID = div1.numDomainID AND AD1.numRecordID = div1.numDivisionID AND tintAddressOf = 2 AND tintAddressType = 1 AND bitIsPrimary=1
                                            WHERE  D1.numDomainID = @numDomainID)
               WHEN @tintBillType = 2 THEN (SELECT vcBillCompanyName
                                            FROM   OpportunityAddress
                                            WHERE  numOppID = @numOppId)
				WHEN @tintBillType = 3 THEN  Com.vcCompanyName
              END AS BillToCompanyName,
             CASE 
               WHEN @tintShipType IS NULL THEN Com.vcCompanyName
               WHEN @tintShipType = 1 AND @tintOppType = 1 THEN Com.vcCompanyName
				-- When Create PO from SO and Ship to is set to Customer 
   			   WHEN @tintShipType = 1 AND @tintOppType = 2 THEN dbo.fn_GetComapnyName(@numCustomerDivID)
               WHEN @tintShipType = 0 THEN (SELECT TOP 1 Com1.vcCompanyName
                                            FROM   companyinfo [Com1]
                                                   JOIN divisionmaster div1
                                                     ON com1.numCompanyID = div1.numCompanyID
                                                   JOIN Domain D1
                                                     ON D1.numDivisionID = div1.numDivisionID
                                                    JOIN dbo.AddressDetails AD1
													 ON AD1.numDomainID = div1.numDomainID AND AD1.numRecordID = div1.numDivisionID AND tintAddressOf = 2 AND tintAddressType = 2 AND bitIsPrimary=1
                                            WHERE  D1.numDomainID = @numDomainID)
               WHEN @tintShipType = 2 THEN (SELECT vcShipCompanyName
                                            FROM   OpportunityAddress
                                            WHERE  numOppID = @numOppId)
				WHEN @tintShipType = 3 THEN (SELECT vcShipCompanyName
														FROM   OpportunityAddress
														WHERE  numOppID = @numOppId)
             END AS ShipToCompanyName
      FROM   companyinfo [Com] JOIN divisionmaster div ON com.numCompanyID = div.numCompanyID
             JOIN AdditionalContactsInformation con ON con.numdivisionId = div.numdivisionId
             JOIN Domain D ON D.numDomainID = div.numDomainID
             LEFT JOIN PaymentGatewayDTLID PGDTL ON PGDTL.intPaymentGateWay = D.intPaymentGateWay AND D.numDomainID = PGDTL.numDomainID
             LEFT JOIN AddressDetails AD --Billing add
				ON AD.numDomainID=DIV.numDomainID AND AD.numRecordID=div.numDivisionID AND AD.tintAddressOf = 2 AND AD.tintAddressType = 1 AND AD.bitIsPrimary=1
			 LEFT JOIN AddressDetails AD2--Shipping address
				ON AD2.numDomainID=DIV.numDomainID AND AD2.numRecordID=div.numDivisionID AND AD2.tintAddressOf = 2 AND AD2.tintAddressType = 2 AND AD2.bitIsPrimary=1	
      WHERE  numContactid = @Contactid AND Div.numDomainID = @numDomainID
    END
  IF @byteMode = 2
    BEGIN
      SELECT opp.vcBizDocID,
             Mst.fltDiscount AS decDiscount,
             isnull(opp.monAmountPaid,0) AS monAmountPaid,
             isnull(opp.vcComments,'') AS vcComments,
             CONVERT(VARCHAR(20),DATEADD(minute,-@ClientTimeZoneOffset,opp.dtCreatedDate)) dtCreatedDate,
             dbo.fn_GetContactName(opp.numCreatedby) AS numCreatedby,
             dbo.fn_GetContactName(opp.numModifiedBy) AS numModifiedBy,
             CONVERT(VARCHAR(20),DATEADD(minute,-@ClientTimeZoneOffset,opp.dtModifiedDate)) dtModifiedDate,
             CASE 
               WHEN isnull(opp.numViewedBy,0) = 0 THEN ''
               ELSE dbo.fn_GetContactName(opp.numViewedBy)
             END AS numViewedBy,
             CASE 
               WHEN opp.numViewedBy = 0 THEN NULL
               ELSE opp.dtViewedDate
             END AS dtViewedDate,
             isnull(Mst.bitBillingTerms,0) AS tintBillingTerms,
             isnull(Mst.intBillingDays,0) AS numBillingDays,
--			 case when isnumeric(ISNULL(dbo.fn_GetListItemName(isnull(Mst.intBillingDays,0)),0))=1 
--				  then ISNULL(dbo.fn_GetListItemName(isnull(Mst.intBillingDays,0)),0) 
--				  else 0 
--			 end AS numBillingDaysName,
--			 CASE WHEN ISNUMERIC((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(Mst.intBillingDays,0))) = 1
-- 				   THEN (SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(Mst.intBillingDays,0))
--				   ELSE 0
--			  END AS numBillingDaysName,
			 ISNULL(BTR.numNetDueInDays,0) AS numBillingDaysName,
			 BTR.vcTerms AS vcBillingTermsName,
             isnull(Mst.bitInterestType,0) AS tintInterestType,
             isnull(Mst.fltInterest,0) AS fltInterest,
             tintOPPType,
             Opp.numBizDocId,
             dbo.fn_GetContactName(numApprovedBy) AS ApprovedBy,
             dtApprovedDate,
             Mst.bintAccountClosingDate,
             tintShipToType,
             tintBillToType,
             tintshipped,
             dtShippedDate bintShippedDate,
             isnull(opp.monShipCost,0) AS monShipCost,
             (SELECT COUNT(* )
              FROM   DocumentWorkflow
              WHERE  numDocID = @numOppBizDocsId
                     AND numContactID = @numUserCntID
                     AND cDocType = 'B'
                     AND tintApprove = 0) AS AppReq,
             (SELECT COUNT(* )
              FROM   DocumentWorkflow
              WHERE  numDocID = @numOppBizDocsId
                     AND cDocType = 'B'
                     AND tintApprove = 1) AS Approved,
             (SELECT COUNT(* )
              FROM   DocumentWorkflow
              WHERE  numDocID = @numOppBizDocsId
                     AND cDocType = 'B'
                     AND tintApprove = 2) AS Declined,
             (SELECT COUNT(* )
              FROM   DocumentWorkflow
              WHERE  numDocID = @numOppBizDocsId
                     AND cDocType = 'B'
                     AND tintApprove = 0) AS Pending,
             ISNULL(BT.vcBizDocFooter,D.vcBizDocFooter) AS BizdocFooter,
             ISNULL(BT.vcBizDocImagePath,D.vcBizDocImagePath) As vcBizDocImagePath, 
             Mst.numDivisionID,
             ISNULL(BT.vcPurBizDocFooter,D.vcPurBizDocFooter) AS PurBizdocFooter, 
             isnull(Mst.fltDiscount,0) AS fltDiscount,
             isnull(Mst.bitDiscountType,0) AS bitDiscountType,
             isnull(Opp.numShipVia,0) AS numShipVia,
--             isnull(vcTrackingURL,'') AS vcTrackingURL,
			ISNULL( (SELECT vcShipFieldValue FROM dbo.ShippingFieldValues WHERE numDomainID=MSt.numDomainID AND intShipFieldID IN ( SELECT intShipFieldID FROM dbo.ShippingFields WHERE numListItemID=Opp.numShipVia  AND vcFieldName='Tracking URL')),'')		AS vcTrackingURL,
             isnull(Opp.numBizDocStatus,0) AS numBizDocStatus,
             CASE 
               WHEN Opp.numBizDocStatus IS NULL THEN '-'
               ELSE dbo.fn_GetListItemName(Opp.numBizDocStatus)
             END AS BizDocStatus,
             CASE 
               WHEN Opp.numShipVia IS NULL THEN '-'
               ELSE dbo.fn_GetListItemName(Opp.numShipVia)
             END AS ShipVia,
             ISNULL(C.varCurrSymbol,'') varCurrSymbol,
             (SELECT COUNT(*) FROM [ShippingReport] WHERE numOppBizDocId = opp.numOppBizDocsId )AS ShippingReportCount,
			 isnull(bitPartialFulfilment,0) bitPartialFulfilment,
			 ISNULL(Opp.vcBizDocName,'') vcBizDocName,
			 dbo.[fn_GetContactName](opp.[numModifiedBy])  + ', '
				+ CONVERT(VARCHAR(20),dateadd(MINUTE,-@ClientTimeZoneOffset,Opp.dtModifiedDate)) AS ModifiedBy,
			 dbo.[fn_GetContactName](Mst.[numRecOwner]) + ', ' +  dbo.[fn_GetContactName](Mst.[numAssignedTo]) AS OrderRecOwner,
			 dbo.[fn_GetContactName](DM.[numRecOwner]) + ', ' +  dbo.[fn_GetContactName](DM.[numAssignedTo]) AS AccountRecOwner,
			 Opp.dtFromDate,
			 Mst.tintTaxOperator,
			 dbo.GetAccountedEmbeddedCost(opp.numOppBizDocsID,1) AS monTotalEmbeddedCost,
			 dbo.GetAccountedEmbeddedCost(opp.numOppBizDocsID,2) AS monTotalAccountedCost,
			 ISNULL(BT.bitEnabled,0) bitEnabled,
			 ISNULL(BT.txtBizDocTemplate,'') txtBizDocTemplate, 
			 ISNULL(BT.txtCSS,'') txtCSS,
			 ISNULL(BT.numOrientation,0) numOrientation,
			 ISNULL(BT.bitKeepFooterBottom,0) bitKeepFooterBottom,
			 dbo.fn_GetContactName(Mst.numAssignedTo) AS AssigneeName,
			 ISNULL((SELECT vcEmail FROM dbo.AdditionalContactsInformation WHERE numContactID = Mst.numAssignedTo),'') AS AssigneeEmail,
			 ISNULL((SELECT numPhone FROM dbo.AdditionalContactsInformation WHERE numContactID = Mst.numAssignedTo),'') AS AssigneePhone,
			 dbo.fn_GetComapnyName(Mst.numDivisionId) OrganizationName,
			 dbo.fn_GetContactName(Mst.numContactID)  OrgContactName,

--CASE WHEN Mst.tintOppType = 1 THEN 
--dbo.getCompanyAddress((select ISNULL(DM.numDivisionID,0)  as numDivisionID from AdditionalContactsInformation AC INNER JOIN DivisionMaster DM 
--on AC.numDivisionID = DM.numDivisionID where numContactID = Mst.numCreatedBy),1,Mst.numDomainId)
--               ELSE dbo.getCompanyAddress(Mst.numDivisionId,1,Mst.numDomainId)
--             END AS CompanyBillingAddress1,

CASE WHEN Mst.tintOppType = 1 THEN 
dbo.getCompanyAddress((select ISNULL(AC.numDivisionID,0)  as numDivisionID from AdditionalContactsInformation AC
INNER JOIN Domain D ON D.numDomainID = AC.numDomainID where AC.numContactID = D.numAdminID AND D.numDomainID = Mst.numDomainId),1,Mst.numDomainId)
               ELSE dbo.getCompanyAddress(Mst.numDivisionId,1,Mst.numDomainId)
             END AS CompanyBillingAddress,

CASE WHEN Mst.tintOppType = 1 THEN 
dbo.getCompanyAddress((select ISNULL(AC.numDivisionID,0)  as numDivisionID from AdditionalContactsInformation AC
INNER JOIN Domain D ON D.numDomainID = AC.numDomainID where AC.numContactID = D.numAdminID AND D.numDomainID = Mst.numDomainId),1,Mst.numDomainId)
               ELSE dbo.getCompanyAddress(Mst.numDivisionId,2,Mst.numDomainId)
             END AS CompanyShippingAddress,
--
--			 dbo.getCompanyAddress(Mst.numDivisionId,1,Mst.numDomainId) CompanyBillingAddress,
--			 dbo.getCompanyAddress(Mst.numDivisionId,2,Mst.numDomainId) CompanyShippingAddress,
			 case when ACI.numPhone<>'' then ACI.numPhone +case when ACI.numPhoneExtension<>'' then ' - ' + ACI.numPhoneExtension else '' end  else '' END OrgContactPhone,
			 DM.vcComPhone as OrganizationPhone,
             ISNULL(ACI.vcEmail,'') AS OrgContactEmail,
 			 dbo.[fn_GetContactName](Mst.[numRecOwner]) AS OnlyOrderRecOwner,
 			 ISNULL(Opp.bitAuthoritativeBizDocs,0) bitAuthoritativeBizDocs,
			 ISNULL(Opp.tintDeferred,0) tintDeferred,isnull(Opp.monCreditAmount,0) AS monCreditAmount,
			 isnull(Opp.bitRentalBizDoc,0) as bitRentalBizDoc,isnull(BT.numBizDocTempID,0) as numBizDocTempID,isnull(BT.vcTemplateName,'') as vcTemplateName,
		     [dbo].[GetDealAmount](opp.numOppId ,getutcdate(),opp.numOppBizDocsId ) as monPAmount,
			 --(select top 1 SD.vcSignatureFile from SignatureDetail SD join OpportunityBizDocsDetails OBD on SD.numSignID=OBD.numSignID where SD.numDomainID = @numDomainID and OBD.numDomainID = @numDomainID and OBD.numSignID IS not null and OBD.numBizDocsId=opp.numOppBizDocsId order by OBD.numBizDocsPaymentDetId desc) as vcSignatureFile,
			 isnull(CMP.txtComments,'') as vcOrganizationComments,
			 isnull(Opp.vcTrackingNo,'') AS  vcTrackingNo,isnull(Opp.vcShippingMethod,'') AS  vcShippingMethod,
			 Opp.dtDeliveryDate,
			 CASE WHEN Opp.vcRefOrderNo IS NULL OR LEN(Opp.vcRefOrderNo)=0 THEN ISNULL(Mst.vcOppRefOrderNo,'') ELSE 
			  isnull(Opp.vcRefOrderNo,'') END AS vcRefOrderNo,ISNULL(Mst.numDiscountAcntType,0) AS numDiscountAcntType,
			 ISNULL(opp.numSequenceId,'') AS numSequenceId,
			 CASE WHEN ISNULL(Opp.numBizDocId,0)=296 THEN ISNULL((SELECT SUBSTRING((SELECT '$^$' + dbo.fn_GetListItemName(numBizDocStatus) +'#^#'+ dbo.fn_GetContactName(numUserCntID) +'#^#'+ dbo.FormatedDateTimeFromDate(DateAdd(minute, -@ClientTimeZoneOffset , dtCreatedDate),@numDomainId) + '#^#'+ isnull(DFCS.vcColorScheme,'NoForeColor')
					FROM OppFulfillmentBizDocsStatusHistory OFBS left join DycFieldColorScheme DFCS on OFBS.numBizDocStatus=DFCS.vcFieldValue AND DFCS.numDomainID=@numDomainID
					WHERE numOppId=opp.numOppId and numOppBizDocsId=opp.numOppBizDocsId FOR XML PATH('')),4,200000)),'') ELSE '' END AS vcBizDocsStatusList,ISNULL(Mst.numCurrencyID,0) AS numCurrencyID,
			ISNULL(Opp.fltExchangeRateBizDoc,Mst.fltExchangeRate) AS fltExchangeRateBizDoc,
			com1.vcCompanyName AS EmployerOrganizationName,div1.vcComPhone as EmployerOrganizationPhone,ISNULL(opp.bitAutoCreated,0) AS bitAutoCreated,
			ISNULL(RecurrenceConfiguration.numRecConfigID,0) AS numRecConfigID,
			(CASE WHEN RecurrenceConfiguration.numRecConfigID IS NULL THEN 0 ELSE 1 END) AS bitRecur,
			(CASE WHEN RecurrenceTransaction.numRecTranID IS NULL THEN 0 ELSE 1 END) AS bitRecurred,
			ISNULL(Mst.bitRecurred,0) AS bitOrderRecurred,
			dbo.FormatedDateFromDate(RecurrenceConfiguration.dtStartDate,@numDomainID) AS dtStartDate,
			RecurrenceConfiguration.vcFrequency AS vcFrequency,
			ISNULL(RecurrenceConfiguration.bitDisabled,0) AS bitDisabled,
			ISNULL(opp.bitFulfilled,0) AS bitFulfilled
		FROM   OpportunityBizDocs opp JOIN OpportunityMaster Mst ON Mst.numOppId = opp.numOppId
             JOIN Domain D ON D.numDomainID = Mst.numDomainID
             LEFT OUTER JOIN [Currency] C ON C.numCurrencyID = Mst.numCurrencyID
             LEFT JOIN [DivisionMaster] DM ON DM.numDivisionID = Mst.numDivisionID   
			 LEFT JOIN CompanyInfo CMP ON DM.numCompanyId = CMP.numCompanyId and CMP.numDomainID = @numDomainID  
             LEFT JOIN dbo.AdditionalContactsInformation ACI ON ACI.numContactID = Mst.numContactID
             LEFT JOIN [BizDocTemplate] BT ON BT.numDomainID = @numDomainID AND BT.numBizDocID = Opp.numBizDocID AND BT.numOppType = Mst.tintOppType AND BT.tintTemplateType=0 and BT.numBizDocTempID=opp.numBizDocTempID
             LEFT JOIN BillingTerms BTR ON BTR.numTermsID = ISNULL(Mst.intBillingDays,0)
             JOIN divisionmaster div1 ON D.numDivisionID = div1.numDivisionID
			 JOIN companyinfo [Com1] ON com1.numCompanyID = div1.numCompanyID
			 LEFT JOIN RecurrenceConfiguration ON Opp.numOppBizDocsId = RecurrenceConfiguration.numOppBizDocID
			 LEFT JOIN RecurrenceTransaction ON Opp.numOppBizDocsId = RecurrenceTransaction.numRecurrOppBizDocID
      WHERE  opp.numOppBizDocsId = @numOppBizDocsId
             AND Mst.numDomainID = @numDomainID
    END
/****** Object:  StoredProcedure [dbo].[USP_OPPGetINItemsForAuthorizativeAccounting]    Script Date: 07/26/2008 16:20:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by Siva                                                                                                            
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_oppgetinitemsforauthorizativeaccounting')
DROP PROCEDURE usp_oppgetinitemsforauthorizativeaccounting
GO
CREATE PROCEDURE  [dbo].[USP_OPPGetINItemsForAuthorizativeAccounting]                                                                                                                                                
(                                                                                                                                                
 @numOppId as numeric(9)=null,                                                                                                                                                
 @numOppBizDocsId as numeric(9)=null,                                                                
 @numDomainID as numeric(9)=0 ,                                                      
 @numUserCntID as numeric(9)=0                                                                                                                                               
)                                                                                                                                                
                                                                                                                                            
as                           
begin

declare @numCurrencyID as numeric(9)
declare @fltExchangeRate as float

declare @fltCurrentExchangeRate as float
declare @tintType as tinyint                                                          
declare @vcPOppName as VARCHAR(100)                                                          
DECLARE @bitPPVariance AS BIT
DECLARE @numDivisionID AS NUMERIC(9)

select  @numCurrencyID=numCurrencyID, @fltExchangeRate=fltExchangeRate,@tintType=tintOppType,@vcPOppName=vcPOppName,@bitPPVariance=ISNULL(bitPPVariance,0),@numDivisionID=numDivisionID from  OpportunityMaster
where numOppID=@numOppId 

DECLARE @vcBaseCurrency AS VARCHAR(100);SET  @vcBaseCurrency=''
DECLARE @bitAutolinkUnappliedPayment AS BIT 
SELECT @vcBaseCurrency=ISNULL(C.varCurrSymbol,''),@bitAutolinkUnappliedPayment=ISNULL(bitAutolinkUnappliedPayment,0) FROM  dbo.Domain D LEFT JOIN Currency C ON D.numCurrencyID=C.numCurrencyID 
WHERE D.numDomainID=@numDomainID

DECLARE @vcForeignCurrency AS VARCHAR(100) ;SET  @vcForeignCurrency=''
SELECT @vcForeignCurrency=ISNULL(C.varCurrSymbol,'') FROM  Currency C WHERE numCurrencyID=@numCurrencyID

DECLARE @fltExchangeRateBizDoc AS FLOAT 
declare @numBizDocId as numeric(9)                                                           

DECLARE @vcBizDocID AS VARCHAR(100)
select @numBizDocId=numBizDocId,@fltExchangeRateBizDoc=fltExchangeRateBizDoc,@vcBizDocID=vcBizDocID  from OpportunityBizDocs where numOppBizDocsId=@numOppBizDocsId                                                         

if @numCurrencyID>0 set @fltCurrentExchangeRate=dbo.GetExchangeRate(@numDomainID,@numCurrencyID)
else set @fltCurrentExchangeRate=@fltExchangeRate                                                                                                                                          

declare @strSQL as varchar(8000)                                                          
declare @strSQLCusFields varchar(1000)                                                          
declare @strSQLEmpFlds varchar(500)                                                          
set @strSQLCusFields=''                                                          
set @strSQLEmpFlds=''                                                          
declare @intRowNum as int                                                          
declare @numFldID as varchar(15)                                                          
declare @vcFldname as varchar(50)                                                                                                                                          
                                                                
if @tintType=1 set @tintType=7                                                                
else set @tintType=8                                                                                                                      
select top 1 @numFldID=numfieldID,@vcFldname=vcFieldName,@intRowNum=(tintRow+1) from View_DynamicCustomColumns                                       
where Grp_id=@tintType and numDomainID=@numDomainID and numAuthGroupID=@numBizDocId and bitCustom=1 order by tintRow                                                          
 while @intRowNum>0                                               
 begin                                                          
  set @strSQLCusFields=@strSQLCusFields+',  dbo.GetCustFldValueBizdoc('+@numFldID+','+convert(varchar(2),@tintType)+',opp.numoppitemtCode) as ['+ @vcFldname+']'                                                     
  set @strSQLEmpFlds=@strSQLEmpFlds+',''-'' as ['+ @vcFldname+']'         
                                                 
  select top 1 @numFldID=numfieldID,@vcFldname=vcFieldName,@intRowNum=(tintRow+1) from View_DynamicCustomColumns                                                               
  where Grp_id=@tintType and numDomainID=@numDomainID and numAuthGroupID=@numBizDocId                                                          
  and bitCustom=1 and tintRow>@intRowNum order by tintRow                                                          
                                                            
  if @@rowcount=0 set @intRowNum=0                                                          
                                                           
 end                                                           
                                                       
       
         
select I.[vcItemName] as Item,        
charitemType as type,        
OBI.vcitemdesc as [desc],        
OBI.numUnitHour as Unit,        
OBI.monPrice as Price,        
OBI.monTotAmount as Amount,(opp.monTotAmount/opp.numUnitHour)*OBI.numUnitHour AS  ItemTotalAmount,       
--case when isnull(bitTaxable,0) =0 then 0 when bitTaxable=1 then (select fltPercentage from OpportunityBizDocTaxItems where numOppBizDocID=@numOppBizDocsId and numTaxItemID=0)  end as Tax,       
isnull(monListPrice,0) as listPrice,convert(varchar,i.numItemCode) as ItemCode,numoppitemtCode,        
L.vcdata as vcItemClassification,case when bitTaxable=0 then 'No' else 'Yes' end as Taxable,monListPrice +' '+@strSQLCusFields,isnull(i.numIncomeChartAcntId,0) as itemIncomeAccount,isnull(i.numAssetChartAcntId,0) as itemInventoryAsset,'NI' as ItemType        
,isnull(i.numCOGsChartAcntId,0) as itemCoGs,isnull(i.monAverageCost,'0') as AverageCost ,isnull(OBI.bitDropShip,0) as  bitDropShip ,  (OBI.monTotAmtBefDiscount-OBI.monTotAmount) as DiscAmt,
NULLIF(Opp.numProjectID,0) numProjectID ,NULLIF(Opp.numClassID,0) numClassID,ISNULL(i.bitKitParent,0) AS bitKitParent,ISNULL(i.bitAssembly,0) AS bitAssembly
from  OpportunityItems opp
join  OpportunityBizDocItems OBI
on OBI.numOppItemID=Opp.numoppitemtCode       
left join item i on opp.numItemCode=i.numItemCode        
left join ListDetails L on i.numItemClassification=L.numListItemID        
where Opp.numOppId=@numOppId  and OBI.numOppBizDocID=@numOppBizDocsId
/*Commented by chintan, Reason: Now Adding Time will be based any Service item insted of 'Time' item
Below code commented While working on Project Mgmt Add Time Function.*/
--   union                                                       
-- select case when numcategory=1 then           
--case when isnull(te.numcontractid,0) = 0 then 'Time' else 'Time(Contract)' end            
--when numcategory=2 then           
--case when isnull(te.numcontractid,0) = 0 then 'Expense' else 'Expense(Contract)' end            
--end  as item,         
--case when numcategory=1 then 'Time' when numcategory=2 then 'Expense' end                    
-- as Type,                    
--convert(varchar(100),txtDesc) as [Desc],                    
----txtDesc as [Desc],                    
--convert(decimal(10,2),datediff(minute,te.dtfromdate,te.dttodate))/60 as unit,                    
--convert(varchar(100),monamount) as Price,                    
--case when isnull(te.numcontractid,0) = 0  
--then  
-- case when numCategory =1 then  
--   isnull(convert(decimal(10,2),datediff(minute,dtfromdate,dttodate))*monAmount/60,0)  
--   when numCategory =2 then  
--   isnull(monamount,0)  
--   end  
--else                
--  0  
--end as amount,                    
--0.00 as Tax,0 as listPrice,'' as ItemCode,numCategoryHDRID as numoppitemtCode,'' as vcItemClassification,                    
--'No' as Taxable,0 as monListPrice,0 as itemIncomeAccount,                    
--        
--0 as itemInventoryAsset,'TE' as ItemType        
--,0 as itemCoGs, 0 as AverageCost,0 as bitDropShip,0 as DiscAmt                   
--from timeandexpense TE                     
--left join contractmanagement cm on TE.numcontractid= cm.numcontractid                     
--where                     
--(numtype= 1 or numtype=2)  And      
--(numOppBizDocsId = @numOppBizDocsId  or numoppid = @numOppId)                                                         
--numHours + Case when numMins=15 then .25 when numMins=30 then .50 when numMins=45 then .75 when numMins=0 then .0 end as  unit,                                                
--convert(varchar(100),numRate) as Price,                                                          
--convert(varchar(100),numRate*numHours) + Case when numMins=15 then numRate*.25 when numMins=30 then numRate*.50 when numMins=45 then numRate*.75 when numMins=0 then .0 end  as amount,                     
                                                                                                                  
                                                         
  select  isnull(@numCurrencyID,0) as numCurrencyID, isnull(@fltExchangeRate,1) as fltExchangeRate,isnull(@fltCurrentExchangeRate,1) as CurrfltExchangeRate,isnull(@fltExchangeRateBizDoc,1) AS fltExchangeRateBizDoc ,@vcBaseCurrency AS vcBaseCurrency,@vcForeignCurrency AS vcForeignCurrency,ISNULL(@vcPOppName,'') AS vcPOppName,ISNULL(@vcBizDocID,'') AS vcBizDocID,ISNULL(@bitPPVariance,0) AS bitPPVariance,@numDivisionID AS numDivisionID,@bitAutolinkUnappliedPayment AS bitAutolinkUnappliedPayment                  
     
     
                                                  
End
GO
/****** Object:  StoredProcedure [dbo].[USP_OPPItemDetails]    Script Date: 09/01/2009 18:59:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- exec USP_OPPItemDetails @numItemCode=31455,@OpportunityId=12387,@numDomainId=110
--created by anooop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_oppitemdetails')
DROP PROCEDURE usp_oppitemdetails
GO
CREATE PROCEDURE [dbo].[USP_OPPItemDetails]
    (
      @numUserCntID NUMERIC(9) = 0,
      @OpportunityId NUMERIC(9) = 0,
      @numDomainId NUMERIC(9),
      @ClientTimeZoneOffset  INT
    )
AS 
    BEGIN
        IF @OpportunityId >0
            BEGIN
                DECLARE @DivisionID AS NUMERIC(9)
                DECLARE @TaxPercentage AS FLOAT
                DECLARE @tintOppType AS TINYINT
                DECLARE @fld_Name AS VARCHAR(500)
                DECLARE @strSQL AS VARCHAR(8000)
                DECLARE @fld_ID AS NUMERIC(9)
                DECLARE @numBillCountry AS NUMERIC(9)
                DECLARE @numBillState AS NUMERIC(9)

                
                SELECT  @DivisionID = numDivisionID,
                        @tintOppType = tintOpptype
                FROM    OpportunityMaster
                WHERE   numOppId = @OpportunityId


               -- Calculate Tax based on Country and state
             /*  SELECT  @numBillState = ISNULL(numState, 0),
						@numBillCountry = ISNULL(numCountry, 0)
			   FROM     dbo.AddressDetails
			   WHERE    numDomainID = @numDomainID
						AND numRecordID = @DivisionID
						AND tintAddressOf = 2
						AND tintAddressType = 1
						AND bitIsPrimary=1
                SET @TaxPercentage = 0
                IF @numBillCountry > 0
                    AND @numBillState > 0 
                    BEGIN
                        IF @numBillState > 0 
                            BEGIN
                                IF EXISTS ( SELECT  COUNT(*)
                                            FROM    TaxDetails
                                            WHERE   numCountryID = @numBillCountry
                                                    AND numStateID = @numBillState
                                                    AND numDomainID = @numDomainID ) 
                                    SELECT  @TaxPercentage = decTaxPercentage
                                    FROM    TaxDetails
                                    WHERE   numCountryID = @numBillCountry
                                            AND numStateID = @numBillState
                                            AND numDomainID = @numDomainID
                                ELSE 
                                    SELECT  @TaxPercentage = decTaxPercentage
                                    FROM    TaxDetails
                                    WHERE   numCountryID = @numBillCountry
                                            AND numStateID = 0
                                            AND numDomainID = @numDomainID
                            END
                    END
                ELSE 
                    IF @numBillCountry > 0 
                        SELECT  @TaxPercentage = decTaxPercentage
                        FROM    TaxDetails
                        WHERE   numCountryID = @numBillCountry
                                AND numStateID = 0
                                AND numDomainID = @numDomainID
               
               */
               

SET @strSQL = ''

--------------Make Dynamic Query--------------
SET @strSQL = @strSQL + '
Select 
Opp.numoppitemtCode,
ISNULL(Opp.bitItemPriceApprovalRequired,0) AS bitItemPriceApprovalRequired,
OM.tintOppType,
Opp.numItemCode,
I.charItemType,
ISNULL(I.bitAsset,0) as bitAsset,
ISNULL(I.bitRental,0) as bitRental,
Opp.numOppId,
Opp.bitWorkOrder,
Opp.fltDiscount,
ISNULL(( SELECT numReturnID FROM   Returns WHERE  numOppItemCode = Opp.numoppitemtCode AND numOppId = Opp.numOppID), 0) AS numReturnID,
dbo.fn_GetReturnItemQty(Opp.numoppitemtCode) AS ReturrnedQty,
[dbo].fn_GetReturnStatus(Opp.numoppitemtCode) AS ReturnStatus,
ISNULL(opp.numUOMId, 0) AS numUOM,
(SELECT vcUnitName FROM dbo.UOM WHERE numUOMId = opp.numUOMId AND ISNULL(bitEnabled,0) = 1)  AS  [vcBaseUOMName],
Opp.numUnitHour AS numOriginalUnitHour, 
isnull(M.vcPOppName,''Internal'') as Source,
numSourceID,
  CASE WHEN ISNULL(bitKitParent,0) = 1
                               AND ISNULL(bitAssembly,0) = 1 THEN 0
                          WHEN ISNULL(bitKitParent,0) = 1 THEN 1
                          ELSE 0
                     END as bitKitParent,
(CASE WHEN (I.charItemType = ''p'' AND ISNULL(bitDropShip, 0) = 0 AND ISNULL(bitAsset,0)=0)THEN dbo.CheckOrderItemInventoryStatus(Opp.numItemCode,Opp.numUnitHour,Opp.numoppitemtCode,isnull(Opp.numWarehouseItmsID,0),(CASE WHEN ISNULL(bitKitParent,0) = 1
                               AND ISNULL(bitAssembly,0) = 1 THEN 0
                          WHEN ISNULL(bitKitParent,0) = 1 THEN 1
                          ELSE 0
                     END)) ELSE 0 END) AS bitBackOrder,
(SELECT vcData FROM dbo.ListDetails WHERE 1=1 AND numListItemID = numShipClass ) AS [numShipClass],ISNULL(Opp.numQtyShipped,0) numQtyShipped,ISNULL(Opp.numUnitHourReceived,0) AS numUnitHourReceived,ISNULL(OM.fltExchangeRate,0) AS fltExchangeRate,ISNULL(OM.numCurrencyID,0) AS numCurrencyID,ISNULL(OM.numDivisionID,0) AS numDivisionID,
(CASE 
	WHEN OM.tintOppType = 1 AND OM.tintOppStatus = 1 AND UPPER(I.charItemType) = ''P'' AND bitDropShip = 0
	THEN (CASE WHEN OM.tintShipped = 1 THEN CAST(ISNULL(Opp.numUnitHour,0) AS VARCHAR) ELSE CAST(ISNULL(Opp.numQtyShipped,0) AS VARCHAR) END)
	WHEN OM.tintOppType = 2 AND OM.tintOppStatus = 1 AND UPPER(I.charItemType) = ''P'' AND bitDropShip = 0
	THEN (CASE WHEN OM.tintShipped = 1 THEN CAST(ISNULL(Opp.numUnitHour,0) AS VARCHAR) ELSE CAST(ISNULL(Opp.numUnitHourReceived,0) AS VARCHAR) END)
	ELSE '''' END) AS vcShippedReceived,
isnull(i.numIncomeChartAcntId,0) as itemIncomeAccount,isnull(i.numAssetChartAcntId,0) as itemInventoryAsset,isnull(i.numCOGsChartAcntId,0) as itemCoGs,
ISNULL(Opp.numRentalIN,0) as numRentalIN,
ISNULL(Opp.numRentalLost,0) as numRentalLost,
ISNULL(Opp.numRentalOut,0) as numRentalOut,
(SELECT vcData FROM dbo.ListDetails WHERE 1=1 AND numListItemID = numShipClass ) AS [numShipClass],ISNULL(bitFreeShipping,0) AS [IsFreeShipping],Opp.bitDiscountType,isnull(Opp.monLandedCost,0) as monLandedCost, '

--	IF @fld_Name = 'vcPathForTImage' 
		SET @strSQL = @strSQL + 'Opp.vcPathForTImage,'
--	ELSE IF @fld_Name = 'vcItemName' 
		SET @strSQL = @strSQL + 'Opp.vcItemName,'
--	ELSE IF @fld_Name = 'vcModelID,' 
		SET @strSQL = @strSQL + 'Opp.vcModelID,'
--	ELSE IF @fld_Name = 'vcWareHouse' 
		SET @strSQL = @strSQL + 'vcWarehouse,'
		SET @strSQL = @strSQL + 'WItems.numOnHand,isnull(WItems.numAllocation,0) as numAllocation,'
		SET @strSQL = @strSQL + 'WL.vcLocation,'
--	ELSE IF @fld_Name = 'ItemType' 
		SET @strSQL = @strSQL + 'Opp.vcType ItemType,Opp.vcType,'
--	ELSE IF @fld_Name = 'vcItemDesc' 
		SET @strSQL = @strSQL + 'ISNULL(Opp.vcItemDesc, '''') vcItemDesc,'
--	ELSE IF @fld_Name = 'Attributes' 
		SET @strSQL = @strSQL + 'Opp.vcAttributes,' --dbo.fn_GetAttributes(Opp.numWarehouseItmsID, I.bitSerialized) AS Attributes,
--	ELSE IF @fld_Name = 'bitDropShip' 
		SET @strSQL = @strSQL + 'ISNULL(bitDropShip, 0) as DropShip,CASE ISNULL(bitDropShip, 0) WHEN 1 THEN ''Yes'' ELSE '''' END AS bitDropShip,'
--	ELSE IF @fld_Name = 'numUnitHour' 
		SET @strSQL = @strSQL + 'CAST(( dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), Opp.numItemCode,om.numDomainId, ISNULL(opp.numUOMId, 0))* Opp.numUnitHour ) AS NUMERIC(18, 2)) AS numUnitHour,'
--	ELSE IF @fld_Name = 'monPrice' 
		SET @strSQL = @strSQL + 'Opp.monPrice,'
--	ELSE IF @fld_Name = 'monTotAmount' 
		SET @strSQL = @strSQL + 'Opp.monTotAmount,'
--	ELSE IF @fld_Name = 'vcSKU' 
		SET @strSQL = @strSQL + 'ISNULL(WItems.vcWHSKU,ISNULL(I.vcSKU,'''')) [vcSKU],'
--	ELSE IF @fld_Name = 'vcManufacturer' 
		SET @strSQL = @strSQL + 'Opp.vcManufacturer,'
--	ELSE IF @fld_Name = 'vcItemClassification' 
		SET @strSQL = @strSQL + ' dbo.[fn_GetListItemName](I.[numItemClassification]) numItemClassification,'
--	ELSE IF @fld_Name = 'numItemGroup' 
		SET @strSQL = @strSQL + '( SELECT    [vcItemGroup] FROM      [ItemGroups] WHERE     [numItemGroupID] = I.numItemGroup ) numItemGroup,'
--	ELSE IF @fld_Name = 'vcUOMName' 
		SET @strSQL = @strSQL + 'ISNULL(u.vcUnitName, '''') numUOMId,'
--	ELSE IF @fld_Name = 'fltWeight' 
		SET @strSQL = @strSQL + 'I.fltWeight,'
--	ELSE IF @fld_Name = 'fltHeight' 
		SET @strSQL = @strSQL + 'I.fltHeight,'
--	ELSE IF @fld_Name = 'fltWidth' 
		SET @strSQL = @strSQL + 'I.fltWidth,'
--	ELSE IF @fld_Name = 'fltLength' 
		SET @strSQL = @strSQL + 'I.fltLength,'
--	ELSE IF @fld_Name = 'numBarCodeId' 
		SET @strSQL = @strSQL + 'I.numBarCodeId,'
		SET @strSQL = @strSQL + 'I.IsArchieve,'
--	ELSE IF @fld_Name = 'vcWorkOrderStatus' 
		SET @strSQL = @strSQL + 'CASE WHEN ISNULL(Opp.bitWorkOrder, 0) = 1
				 THEN ( SELECT  dbo.GetListIemName(numWOStatus)
								+ CASE WHEN ISNULL(numWOStatus, 0) = 23184
									   THEN '' - ''
											+ CAST(ISNULL(dbo.fn_GetContactName(numAssignedTo),
														  '''') + '',''
											+ CONVERT(VARCHAR(20), DATEADD(minute, '+ CONVERT(VARCHAR(15),-@ClientTimeZoneOffset)  +', bintCompletedDate)) AS VARCHAR(100))
									   ELSE ''''
								  END
						FROM    WorkOrder
						WHERE   ISNULL(numOppId, 0) = Opp.numOppId
								AND numItemCode = Opp.numItemCode AND ISNULL(numParentWOID,0)=0
					  )
				 ELSE ''''
			END AS vcWorkOrderStatus,'
--	ELSE IF @fld_Name = 'DiscAmt' 
		SET @strSQL = @strSQL + 'CASE WHEN Opp.bitDiscountType = 0
				 THEN CAST(ISNULL(Opp.monTotAmtBefDiscount, Opp.monTotAmount)
					  - Opp.monTotAmount AS VARCHAR(10)) + '' (''
					  + CAST(Opp.fltDiscount AS VARCHAR(10)) + ''%)''
				 ELSE CAST(ISNULL(Opp.monTotAmtBefDiscount, Opp.monTotAmount)
					  - Opp.monTotAmount AS VARCHAR(10))
			END AS DiscAmt,'	
--	ELSE IF @fld_Name = 'numProjectID' 
		SET @strSQL = @strSQL + ' ISNULL(Opp.numProjectID, 0) numProjectID,
		 CASE WHEN ( SELECT TOP 1
								ISNULL(OBI.numOppBizDocItemID, 0)
						FROM    dbo.OpportunityBizDocs OBD
								INNER JOIN dbo.OpportunityBizDocItems OBI ON OBD.numOppBizDocsID = OBI.numOppBizDocID
						WHERE   OBI.numOppItemID = Opp.numoppitemtCode
								AND ISNULL(OBD.bitAuthoritativeBizDocs, 0) = 1
								and isnull(OBD.numOppID,0)=OM.numOppID
					  ) > 0 THEN 1
				 ELSE 0
			END AS bitItemAddedToAuthBizDoc,'	
--	ELSE IF @fld_Name = 'numClassID' 
		SET @strSQL = @strSQL + 'ISNULL(Opp.numClassID, 0) numClassID,'	
--	ELSE IF @fld_Name = 'vcProjectStageName' 
		SET @strSQL = @strSQL + ' ISNULL(Opp.numProjectStageID, 0) numProjectStageID,
				CASE WHEN OPP.numProjectStageID > 0
				 THEN dbo.fn_GetProjectStageName(OPP.numProjectId,
												 OPP.numProjectStageID)
				 WHEN OPP.numProjectStageID = -1 THEN ''T&E''
				 WHEN OPP.numProjectId > 0
				 THEN CASE WHEN OM.tintOppType = 1 THEN ''S.O.''
						   ELSE ''P.O.''
					  END
				 ELSE ''-''
			END AS vcProjectStageName,'	
	        
	
	

--------------Add custom fields to query----------------
SELECT TOP 1 @fld_Name= vcFieldName,@fld_ID = numFieldId FROM View_DynamicCustomColumns
WHERE 
grp_id = 5 AND numDomainID=@numDomainId
AND numFormId=26 AND bitCustom=1 AND numUserCntID = @numUserCntID ORDER BY numFieldId
WHILE @fld_ID > 0
BEGIN ------------------

	 PRINT @fld_Name
	
	       SET @strSQL = @strSQL + 'dbo.GetCustFldValueItem('
                                    + CONVERT(VARCHAR(15), @fld_ID)
                                    + ',Opp.numItemCode) as ''' + @fld_Name + ''','
       

SELECT TOP 1 @fld_Name= vcFieldName,@fld_ID = numFieldId FROM View_DynamicCustomColumns
WHERE 
grp_id = 5 AND numDomainID=@numDomainId
AND numFormId=26 AND bitCustom=1  AND numUserCntID = @numUserCntID AND numFieldId > @fld_ID ORDER BY numFieldId
 IF @@ROWCOUNT=0
 SET @fld_ID = 0
END ------------------


                                
--Purchase Opportunity ?
IF @tintOppType = 2 
    BEGIN
        SET @strSQL =  @strSQL + 'ISNULL((SELECT sum(numQty) FROM OppWarehouseSerializedItem WHERE numOppID=OM.numOppID AND numOppItemID=Opp.numoppitemtCode),0) AS SerialLotNo,
		  SUBSTRING(( SELECT  '', '' + vcSerialNo
                        + CASE WHEN ISNULL(I.bitLotNo, 0) = 1
                                THEN '' (''
                                    + CONVERT(VARCHAR(15), oppI.numQty)
                                    + '')''
                                ELSE ''''
                            END
                FROM    OppWarehouseSerializedItem oppI
                        JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID = whi.numWareHouseItmsDTLID
                WHERE   oppI.numOppID = Opp.numOppId
                        AND oppI.numOppItemID = Opp.numoppitemtCode
                ORDER BY vcSerialNo
                FOR
                XML PATH('''')
                ), 3, 200000) AS vcSerialLotNo,
		  CAST(Opp.numUnitHour AS NUMERIC(9, 0)) numQty,
		  (SELECT vcUnitName FROM dbo.UOM WHERE numUOMId = opp.numUOMId AND ISNULL(bitEnabled,0) = 1)  AS  [vcBaseUOMName],
		  I.bitSerialized,
		  ISNULL(I.bitLotNo, 0) AS bitLotNo,
		  isnull(Opp.numWarehouseItmsID,0) as numWarehouseItmsID,'
	END
ELSE
	BEGIN	
        SET @strSQL =  @strSQL + ' ISNULL((SELECT sum(numQty) FROM OppWarehouseSerializedItem WHERE numOppID=OM.numOppID AND numOppItemID=Opp.numoppitemtCode),0) AS SerialLotNo,
		   SUBSTRING(( SELECT  '', '' + vcSerialNo
                        + CASE WHEN ISNULL(I.bitLotNo, 0) = 1
                                THEN '' (''
                                    + CONVERT(VARCHAR(15), oppI.numQty)
                                    + '')''
                                ELSE ''''
                            END
                FROM    OppWarehouseSerializedItem oppI
                        JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID = whi.numWareHouseItmsDTLID
                WHERE   oppI.numOppID = Opp.numOppId
                        AND oppI.numOppItemID = Opp.numoppitemtCode
                ORDER BY vcSerialNo
                FOR
                XML PATH('''')
                ), 3, 200000) AS vcSerialLotNo,
		   CAST(Opp.numUnitHour AS NUMERIC(9, 0)) numQty,
		  I.bitSerialized,
		  ISNULL(I.bitLotNo, 0) AS bitLotNo,
		  isnull(Opp.numWarehouseItmsID,0) as numWarehouseItmsID,'
	END


        
 SET @strSQL = left(@strSQL,len(@strSQL)-1) + '   
        FROM    OpportunityItems Opp
        LEFT JOIN OpportunityMaster OM ON Opp.numOppID = oM.numOppID
        JOIN item I ON Opp.numItemCode = i.numItemcode
        LEFT JOIN WareHouseItems WItems ON WItems.numWareHouseItemID = Opp.numWarehouseItmsID
        LEFT JOIN Warehouses W ON W.numWarehouseID = WItems.numWarehouseID
        LEFT JOIN WarehouseLocation WL ON WL.numWLocationID = WItems.numWLocationID
        LEFT JOIN OpportunityMaster M ON Opp.numSourceID = M.numOppID
        LEFT JOIN UOM u ON u.numUOMId = opp.numUOMId
WHERE   om.numDomainId = '+ CONVERT(VARCHAR(10),@numDomainId) + '
        AND Opp.numOppId = '+ CONVERT(VARCHAR(15),@OpportunityId) +'
ORDER BY numoppitemtCode ASC '
  
PRINT @strSQL;
EXEC (@strSQL);
SELECT  vcSerialNo,
       vcComments AS Comments,
        numOppItemID AS numoppitemtCode,
        W.numWarehouseItmsDTLID,
        numWarehouseItmsID AS numWItmsID,
        dbo.Fn_getattributes(W.numWarehouseItmsDTLID,
                             1) Attributes
FROM    WareHouseItmsDTL W
        JOIN OppWarehouseSerializedItem O ON O.numWarehouseItmsDTLID = W.numWareHouseItmsDTLID
WHERE   numOppID = @OpportunityId


CREATE TABLE #tempForm (tintOrder TINYINT,vcDbColumnName nvarchar(50),vcOrigDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),
  vcListItemType varchar(3),numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,
  numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,bitIsAlphaNumeric BIT,bitIsNumeric BIT,
  bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),ListRelID numeric(9),intColumnWidth int,intVisible int,vcFieldDataType CHAR(1))


IF
	(
		SELECT 
			COUNT(*)
		FROM 
			View_DynamicColumns DC 
		WHERE 
			DC.numFormId=26 and DC.numDomainID=@numDomainID AND numUserCntID = @numUserCntID AND
			ISNULL(DC.bitSettingField,0)=1 AND ISNULL(DC.bitDeleted,0)=0 AND ISNULL(DC.bitCustom,0)=0) > 0
	OR
	(
		SELECT 
			COUNT(*)
		FROM 
			View_DynamicCustomColumns
		WHERE 
			numFormId=26 AND numUserCntID=@numUserCntID AND numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=0 AND ISNULL(bitCustom,0)=1
	) > 0
BEGIN	

INSERT INTO #tempForm
select isnull(DC.tintRow,0)+1 as tintOrder,DDF.vcDbColumnName,DDF.vcOrigDbColumnName,ISNULL(DDF.vcCultureFieldName,DDF.vcFieldName),                  
 DDF.vcAssociatedControlType,DDF.vcListItemType,DDF.numListID,DDF.vcLookBackTableName                                                 
,DDF.bitCustom,DDF.numFieldId,DDF.bitAllowSorting,DDF.bitInlineEdit,
DDF.bitIsRequired,DDF.bitIsEmail,DDF.bitIsAlphaNumeric,DDF.bitIsNumeric,DDF.bitIsLengthValidation,
DDF.intMaxLength,DDF.intMinLength,DDF.bitFieldMessage,DDF.vcFieldMessage vcFieldMessage,DDF.ListRelID,DDF.intColumnWidth,Case when DC.numFieldID is null then 0 else 1 end,DDF.vcFieldDataType
 FROM View_DynamicDefaultColumns DDF left join View_DynamicColumns DC on DC.numFormId=26 and DDF.numFieldId=DC.numFieldId and DC.numUserCntID=@numUserCntID and DC.numDomainID=@numDomainID and numRelCntType=0 AND tintPageType=1 
 where DDF.numFormId=26 and DDF.numDomainID=@numDomainID AND ISNULL(DDF.bitSettingField,0)=1 AND ISNULL(DDF.bitDeleted,0)=0 AND ISNULL(DDF.bitCustom,0)=0
       
 UNION
    
    select tintRow+1 as tintOrder,vcDbColumnName,vcFieldName,vcFieldName,                  
 vcAssociatedControlType,'' as vcListItemType,numListID,''                                                 
,bitCustom,numFieldId,bitAllowSorting,0 as bitAllowEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,''
 from View_DynamicCustomColumns
 where numFormId=26 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=0
 AND ISNULL(bitCustom,0)=1

END
ELSE
BEGIN
	--Check if Master Form Configuration is created by administrator if NoColumns=0
	DECLARE @IsMasterConfAvailable BIT = 0
	DECLARE @numUserGroup NUMERIC(18,0)

	SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

	IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = 26 AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
	BEGIN
		SET @IsMasterConfAvailable = 1
	END

	--If MasterConfiguration is available then load it otherwise load default columns
	IF @IsMasterConfAvailable = 1
	BEGIN
		INSERT INTO #tempForm
		SELECT 
			(intRowNum + 1) as tintOrder, vcDbColumnName, vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
			vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName,bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,
			bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth,1,vcFieldDataType
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=26 AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcFieldName,vcFieldName,                  
			 vcAssociatedControlType,'' as vcListItemType,numListID,''                                                 
			,bitCustom,numFieldId,bitAllowSorting,0 as bitAllowEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,''
		FROM 
			View_DynamicCustomColumnsMasterConfig
		WHERE 
			View_DynamicCustomColumnsMasterConfig.numFormId=26 AND 
			View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1
		ORDER BY 
			tintOrder ASC  
	END
END


select * from #tempForm order by tintOrder

drop table #tempForm

/*
                        SET @strSQL = 'select  Opp.numoppitemtCode,Opp.numOppId,
                               Opp.numItemCode,                                    
                               Opp.vcItemName,                                                                    
                               ISNULL(Opp.vcItemDesc,'''') as [Desc],                                                                    
                               Opp.vcType ItemType,                                   
                               Opp.vcPathForTImage,
                               Opp.vcModelID,
							    CAST((dbo.fn_UOMConversion(ISNULL(I.numBaseUnit,0),Opp.numItemCode,om.numDomainId,ISNULL(opp.numUOMId,0)) * Opp.numUnitHour) as numeric(18,2)) as numUnitHour,   
                               Opp.monPrice,                                                                    
                               Opp.monTotAmount,                                                     
							   ISNULL(Opp.vcItemDesc,'''') vcItemDesc,
							   Opp.vcManufacturer,
							   I.charItemType,
							   I.bitKitParent,
							   I.bitAssembly,
							   I.vcSKU,                                   
							   I.numBarCodeId,
							   dbo.[fn_GetListItemName](I.[numItemClassification]) vcClassification,
							   (SELECT [vcItemGroup] FROM [ItemGroups] WHERE [numItemGroupID] =I.numItemGroup) vcItemGroup,
							   ISNULL(I.vcUnitofMeasure,'''') vcUnitofMeasure,
							   I.fltWeight,I.fltHeight,I.fltWidth,I.fltLength,
							   WItems.numWarehouseID,
                               Opp.numWarehouseItmsID,isnull(Opp.numToWarehouseItemID,0) numToWarehouseItemID,bitDropShip DropShip, CASE ISNULL(bitDropShip,0) when 1 then ''Yes'' else '''' end as vcDropShip,
                               0 as Op_Flag,isnull(Opp.bitDiscountType,0) as bitDiscountType,isnull(Opp.fltDiscount,0) as fltDiscount,isnull(monTotAmtBefDiscount,0) as monTotAmtBefDiscount,                                    
                               vcWarehouse as Warehouse,I.bitSerialized,isnull(I.bitLotNo,0) as bitLotNo,vcAttributes, 
                               dbo.fn_GetReturnItemQty(Opp.numoppitemtCode) as ReturrnedQty,
                               [dbo].fn_GetReturnStatus(Opp.numoppitemtCode) as ReturnStatus,
							   isnull((SELECT numReturnID FROM Returns WHERE numOppItemCode=Opp.numoppitemtCode and numOppId=Opp.numOppID),0) as numReturnID,
                               isnull(v.monCost,0) as VendorCost,
                               bitTaxable,
                               case when bitTaxable =0 then 0 when bitTaxable=1 then '
                            + CONVERT(VARCHAR(15), @TaxPercentage)
                            + ' end as Tax,                                                
                                   isnull(M.vcPOppName,''Internal'') as Source,dbo.fn_GetAttributes(Opp.numWarehouseItmsID,I.bitSerialized) as Attributes  ,numSourceID '
                            + CASE WHEN @strSQL = '' THEN ''
                                   ELSE ',' + @strSQL
                              END
                            + ',isnull(opp.numUOMId,0) AS numUOM,ISNULL(u.vcUnitName,'''') vcUOMName,dbo.fn_UOMConversion(opp.numUOMId,Opp.numItemCode,om.numDomainId,null) AS UOMConversionFactor
                          ,case when Opp.bitDiscountType=0 then  cast(isnull(Opp.monTotAmtBefDiscount,Opp.monTotAmount)-Opp.monTotAmount AS VARCHAR(10)) +  '' ('' + CAST(Opp.fltDiscount AS VARCHAR(10)) +  ''%)'' 
                          ELSE cast(isnull(Opp.monTotAmtBefDiscount,Opp.monTotAmount)-Opp.monTotAmount AS VARCHAR(10)) end as DiscAmt
                           ,isnull(I.numVendorID,0) numVendorID,OM.tintOppType,cast(Opp.numUnitHour as numeric(9,0)) as numQty,SUBSTRING(
(SELECT '','' + vcSerialNo + CASE WHEN isnull(I.bitLotNo,0)=1 THEN '' ('' + CONVERT(VARCHAR(15),whi.numQty) + '')'' ELSE '''' end 
FROM OppWarehouseSerializedItem oppI JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID=whi.numWareHouseItmsDTLID where oppI.numOppID=Opp.numOppId and oppI.numOppItemID=Opp.numoppitemtCode 
ORDER BY vcSerialNo FOR XML PATH('''')),2,200000) AS SerialLotNo, ISNULL(Opp.bitWorkOrder,0) as bitWorkOrder,
CASE WHEN ISNULL(Opp.bitWorkOrder,0)=1 then (select dbo.GetListIemName(numWOStatus) + CASE WHEN isnull(numWOStatus,0)=23184 THEN
'' - '' + CAST(ISNULL(dbo.fn_GetContactName(numAssignedTo), '''')+'',''+convert(varchar(20),dateadd(minute,' + CONVERT(VARCHAR(15),-@ClientTimeZoneOffset) + ',bintCompletedDate)) AS VARCHAR(100)) ELSE '''' end
 from WorkOrder where isnull(numOppId,0)=Opp.numOppId and numItemCode=Opp.numItemCode) else '''' end as vcWorkOrderStatus,
 ISNULL(Opp.numVendorWareHouse,0) as numVendorWareHouse,ISNULL(Opp.numShipmentMethod,0) as numShipmentMethod,ISNULL(Opp.numSOVendorId,0) as numSOVendorId,Opp.numUnitHour as numOriginalUnitHour,
 isnull(Opp.numProjectID,0) numProjectID,isnull(Opp.numClassID,0) numClassID,isnull(Opp.numProjectStageID,0) numProjectStageID,Case When OPP.numProjectStageID>0 then dbo.fn_GetProjectStageName(OPP.numProjectId,OPP.numProjectStageID) when OPP.numProjectStageID=-1 then ''T&E'' When OPP.numProjectId>0 then case when OM.tintOppType=1 then ''S.O.'' else ''P.O.'' end else ''-'' end as vcProjectStageName,
 CASE WHEN (SELECT TOP 1 ISNULL(OBI.numOppBizDocItemID,0) FROM dbo.OpportunityBizDocs OBD INNER JOIN dbo.OpportunityBizDocItems OBI ON OBD.numOppBizDocsID = OBI.numOppBizDocID WHERE OBI.numOppItemID = Opp.numoppitemtCode AND ISNULL(OBD.bitAuthoritativeBizDocs,0)=1 ) > 0
      THEN 1 ELSE 0 END AS bitItemAddedToAuthBizDoc,isnull(I.numItemClass,0) as numItemClass
                              from OpportunityItems Opp                    
                               left join OpportunityMaster OM                                                            
                                  on Opp.numOppID=oM.numOppID                                                                     
                                 join item I                                                                    
                                  on Opp.numItemCode=i.numItemcode                                            
                                left join Vendor v 
									on v.numVendorID= I.numVendorID and I.numItemCode =v.numItemCode
                               left join  WareHouseItems  WItems                                        
                               on   WItems.numWareHouseItemID= Opp.numWarehouseItmsID                                            
                               left join  Warehouses  W                                            
                                  on   W.numWarehouseID= WItems.numWarehouseID                                         
                                  left join OpportunityMaster M                                                            
                                  on Opp.numSourceID=M.numOppID  
                                  LEFT JOIN  UOM u ON u.numUOMId = opp.numUOMId  
                                  where om.numDomainId ='
                            + CONVERT(VARCHAR(15), @numDomainId)
                            + ' and Opp.numOppId='+ CONVERT(VARCHAR(15), @OpportunityId) +  
                            + CASE WHEN @numItemCode > 0 THEN ' AND Opp.numOppItemTCode = ' + CONVERT(VARCHAR(15), @numItemCode)
                                   ELSE ''
                              END
                            +
                            ' order by numoppitemtCode ASC '
                        PRINT @strSQL
                        EXEC ( @strSQL
                            )
                        SELECT  vcSerialNo,
                                vcComments AS Comments,
                                numOppItemID AS numoppitemtCode,
                                W.numWarehouseItmsDTLID,
                                numWarehouseItmsID AS numWItmsID,
                                dbo.Fn_getattributes(W.numWarehouseItmsDTLID,
                                                     1) Attributes
                        FROM    WareHouseItmsDTL W
                                JOIN OppWarehouseSerializedItem O ON O.numWarehouseItmsDTLID = W.numWareHouseItmsDTLID
                        WHERE   numOppID = @OpportunityId
*/
      /*              END
                ELSE 
                    BEGIN
                        SELECT  @numDomainID = numDomainId
                        FROM    OpportunityMaster
                        WHERE   numOppID = @OpportunityId
                        SET @strSQL = ''
                        SELECT TOP 1
                                @fld_ID = Fld_ID,
                                @fld_Name = Fld_label 
                        FROM    CFW_Fld_Master
                        WHERE   grp_id = 5
                                AND numDomainID = @numDomainID
                        WHILE @fld_ID > 0
                            BEGIN
                                SET @strSQL =@strSQL + 'dbo.GetCustFldValueItem('
                                    + CONVERT(VARCHAR(15), @fld_ID)
                                    + ',Opp.numItemCode) as ''' + @fld_Name + ''''
                                SELECT TOP 1
                                        @fld_ID = Fld_ID,
                                        @fld_Name = Fld_label
                                FROM    CFW_Fld_Master
                                WHERE   grp_id = 5
                                        AND numDomainID = @numDomainID
                                        AND Fld_ID > @fld_ID
                                IF @@ROWCOUNT = 0 
                                    SET @fld_ID = 0
                                IF @fld_ID <> 0 
                                    SET @strSQL = @strSQL + ','
                            END
                        SET @strSQL = 'select  Opp.numoppitemtCode,Opp.numOppId,                                       
                                Opp.numItemCode,                                    
                               Opp.vcItemName,                                                                    
                               ISNULL(Opp.vcItemDesc,'''') as [Desc],                                                                    
                               Opp.vcType ItemType,                                   
                               Opp.vcPathForTImage,
                               Opp.vcModelID,
                              CAST((dbo.fn_UOMConversion(ISNULL(I.numBaseUnit,0),Opp.numItemCode,om.numDomainId,ISNULL(opp.numUOMId,0)) * Opp.numUnitHour) as numeric(18,2)) as numUnitHour,   
							   Opp.monPrice,                                                                    
                               Opp.monTotAmount,                                                     
							   ISNULL(Opp.vcItemDesc,'''') vcItemDesc,
							   Opp.vcManufacturer,
							   I.charItemType,
							   I.bitKitParent,
							   I.bitAssembly,
							   I.vcSKU,
							   I.numBarCodeId,
							   dbo.[fn_GetListItemName](I.[numItemClassification]) vcClassification,
							   (SELECT [vcItemGroup] FROM [ItemGroups] WHERE [numItemGroupID] =I.numItemGroup) vcItemGroup,
							   ISNULL(I.vcUnitofMeasure,'''') vcUnitofMeasure,
							   I.fltWeight,I.fltHeight,I.fltWidth,I.fltLength,
							   WItems.numWarehouseID,
                               Opp.numWarehouseItmsID,isnull(Opp.numToWarehouseItemID,0) numToWarehouseItemID,bitDropShip DropShip, CASE ISNULL(bitDropShip,0) when 1 then ''Yes'' else '''' end as vcDropShip,
                               0 as Op_Flag,isnull(Opp.bitDiscountType,0) bitDiscountType,isnull(Opp.fltDiscount,0) fltDiscount,isnull(monTotAmtBefDiscount,0) monTotAmtBefDiscount,                                           
                               vcWarehouse as Warehouse,I.bitSerialized,isnull(I.bitLotNo,0) as bitLotNo,vcAttributes,
                               dbo.fn_GetReturnItemQty(Opp.numoppitemtCode) as ReturrnedQty,
                               [dbo].fn_GetReturnStatus(Opp.numoppitemtCode) as ReturnStatus,
							   isnull((SELECT numReturnID FROM Returns WHERE numOppItemCode=Opp.numoppitemtCode and numOppId=Opp.numOppID),0) as numReturnID,
                               isnull(v.monCost,0) as VendorCost,
                               bitTaxable,
                               case when bitTaxable =0 then 0 when bitTaxable=1 then '
                            + CONVERT(VARCHAR(15), @TaxPercentage)
                            + ' end as Tax,                                                
                                   isnull(m.vcPOppName,''Internal'') as Source,dbo.fn_GetAttributes(Opp.numWarehouseItmsID,I.bitSerialized) as Attributes ,numSourceID '
                            + CASE WHEN @strSQL = '' THEN ''
                                   ELSE ',' + @strSQL
                              END
                            + ',isnull(opp.numUOMId,0) AS numUOM,ISNULL(u.vcUnitName,'''') vcUOMName,dbo.fn_UOMConversion(opp.numUOMId,Opp.numItemCode,om.numDomainId,null) AS UOMConversionFactor
                          ,case when Opp.bitDiscountType=0 then  cast(isnull(Opp.monTotAmtBefDiscount,Opp.monTotAmount)-Opp.monTotAmount AS VARCHAR(10)) +  '' ('' + CAST(Opp.fltDiscount AS VARCHAR(10)) +  ''%)'' 
                          ELSE cast(isnull(Opp.monTotAmtBefDiscount,Opp.monTotAmount)-Opp.monTotAmount AS VARCHAR(10)) end as DiscAmt
                         ,isnull(I.numVendorID,0) numVendorID,OM.tintOppType,cast(Opp.numUnitHour as numeric(9,0)) numQty,SUBSTRING(
(SELECT '','' + vcSerialNo + CASE WHEN isnull(I.bitLotNo,0)=1 THEN '' ('' + CONVERT(VARCHAR(15),whi.numQty) + '')'' ELSE '''' end 
FROM OppWarehouseSerializedItem oppI JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID=whi.numWareHouseItmsDTLID where oppI.numOppID=Opp.numOppId and oppI.numOppItemID=Opp.numoppitemtCode 
ORDER BY vcSerialNo FOR XML PATH('''')),2,200000) AS SerialLotNo, ISNULL(Opp.bitWorkOrder,0) as bitWorkOrder,
CASE WHEN ISNULL(Opp.bitWorkOrder,0)=1 then (select dbo.GetListIemName(numWOStatus) + CASE WHEN isnull(numWOStatus,0)=23184 THEN
'' - '' + CAST(ISNULL(dbo.fn_GetContactName(numAssignedTo), '''')+'',''+convert(varchar(20),dateadd(minute,'+ CONVERT(VARCHAR(15),-@ClientTimeZoneOffset) +',bintCompletedDate)) AS VARCHAR(100)) ELSE '''' end
 from WorkOrder where isnull(numOppId,0)=Opp.numOppId and numItemCode=Opp.numItemCode) else '''' end as vcWorkOrderStatus,
 ISNULL(Opp.numVendorWareHouse,0) as numVendorWareHouse,ISNULL(Opp.numShipmentMethod,0) as numShipmentMethod,ISNULL(Opp.numSOVendorId,0) as numSOVendorId,Opp.numUnitHour as numOriginalUnitHour,
 isnull(Opp.numProjectID,0) numProjectID,isnull(Opp.numClassID,0) numClassID,isnull(Opp.numProjectStageID,0) numProjectStageID,Case When OPP.numProjectStageID>0 then dbo.fn_GetProjectStageName(OPP.numProjectId,OPP.numProjectStageID) when OPP.numProjectStageID=-1 then ''T&E'' When OPP.numProjectId>0 then case when OM.tintOppType=1 then ''S.O.'' else ''P.O.'' end else ''-'' end as vcProjectStageName,
 CASE WHEN (SELECT TOP 1 ISNULL(OBI.numOppBizDocItemID,0) FROM dbo.OpportunityBizDocs OBD INNER JOIN dbo.OpportunityBizDocItems OBI ON OBD.numOppBizDocsID = OBI.numOppBizDocID WHERE OBI.numOppItemID = Opp.numoppitemtCode AND ISNULL(OBD.bitAuthoritativeBizDocs,0)=1 ) > 0
 THEN 1 ELSE 0 END AS bitItemAddedToAuthBizDoc,isnull(I.numItemClass,0) as numItemClass
              from OpportunityItems Opp                    
                               left join OpportunityMaster OM                                                            
                                  on Opp.numOppID=oM.numOppID                                    
                                 join item I                                                                    
                                  on Opp.numItemCode=i.numItemcode   
                                  left join Vendor v 
									on v.numVendorID= I.numVendorID and I.numItemCode =v.numItemCode
                                left join  WareHouseItems  WItems                                        
                                on   WItems.numWareHouseItemID= Opp.numWarehouseItmsID                                            
                                left join  Warehouses  W                                            
                                  on   W.numWarehouseID= WItems.numWarehouseID                                         
                                  left join OpportunityMaster M                                                            
                                  on Opp.numSourceID=M.numOppID                      
                                        LEFT JOIN  UOM u ON u.numUOMId = opp.numUOMId
                                  where om.numDomainId ='
                            + CONVERT(VARCHAR(15), @numDomainId)
                            + ' and Opp.numOppId='+ CONVERT(VARCHAR(15), @OpportunityId) +  
                            + CASE WHEN @numItemCode > 0 THEN ' AND Opp.numOppItemTCode = ' + CONVERT(VARCHAR(15), @numItemCode)
                                   ELSE ''
                              END
                            +
                            ' order by numoppitemtCode ASC '
                        PRINT @strSQL
                        EXEC ( @strSQL
                            )
                        SELECT  vcSerialNo,
                                vcComments AS Comments,
                                numOppItemID AS numoppitemtCode,
                                W.numWarehouseItmsDTLID,
                                numWarehouseItmsID AS numWItmsID,
                                dbo.Fn_getattributes(W.numWarehouseItmsDTLID,
                                                     1) Attributes
                        FROM    OppWarehouseSerializedItem O
                                LEFT JOIN WareHouseItmsDTL W ON O.numWarehouseItmsDTLID = W.numWareHouseItmsDTLID
                        WHERE   numOppID = @OpportunityId
                    END
            */
            END
            
    END
/****** Object:  StoredProcedure [dbo].[USP_OppManage]    Script Date: 07/26/2008 16:20:19 ******/
--2 changes
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OppManage')
DROP PROCEDURE usp_oppmanage
GO
CREATE PROCEDURE [dbo].[USP_OppManage] 
(                                                                          
  @numOppID as numeric=0 OUTPUT,                                                                          
  @numContactId numeric(9)=null,                                                                          
  @numDivisionId numeric(9)=null,                                                                          
  @tintSource numeric(9)=null,                                                                          
  @vcPOppName Varchar(100)='' OUTPUT,                                                                          
  @Comments varchar(1000)='',                                                                          
  @bitPublicFlag bit=0,                                                                          
  @numUserCntID numeric(9)=0,                                                                                 
  @monPAmount money =0,                                                 
  @numAssignedTo as numeric(9)=0,                                                                                                        
  @numDomainId numeric(9)=0,                                                                                                                                                                                   
  @strItems as text=null,                                                                          
  @strMilestone as text=null,                                                                          
  @dtEstimatedCloseDate datetime,                                                                          
  @CampaignID as numeric(9)=null,                                                                          
  @lngPConclAnalysis as numeric(9)=null,                                                                         
  @tintOppType as tinyint,                                                                                                                                             
  @tintActive as tinyint,                                                              
  @numSalesOrPurType as numeric(9),              
  @numRecurringId as numeric(9),
  @numCurrencyID as numeric(9)=0,
  @DealStatus as tinyint =0,-- 0=Open,1=Won,2=Lost
  @numStatus AS NUMERIC(9),
  @vcOppRefOrderNo VARCHAR(100),
  @numBillAddressId numeric(9)=0,
  @numShipAddressId numeric(9)=0,
  @bitStockTransfer BIT=0,
  @WebApiId INT = 0,
  @tintSourceType TINYINT=0,
  @bitDiscountType as bit,
  @fltDiscount  as float,
  @bitBillingTerms as bit,
  @intBillingDays as integer,
  @bitInterestType as bit,
  @fltInterest as float,
  @tintTaxOperator AS TINYINT,
  @numDiscountAcntType AS NUMERIC(9),
  @vcWebApiOrderNo VARCHAR(100)=NULL,
  @vcCouponCode		VARCHAR(100) = '',
  @vcMarketplaceOrderID VARCHAR(100)=NULL,
  @vcMarketplaceOrderDate  datetime=NULL,
  @vcMarketplaceOrderReportId VARCHAR(100)=NULL,
  @numPercentageComplete NUMERIC(9),
  @bitUseShippersAccountNo BIT = 0,
  @bitUseMarkupShippingRate BIT = 0,
  @numMarkupShippingRate NUMERIC(19,2) = 0,
  @intUsedShippingCompany INT = 0
)                                                                          
                                                                          
as  
BEGIN TRY
BEGIN TRANSACTION
 DECLARE @fltExchangeRate float                                 
 DECLARE @hDocItem int                                                                                                                            
 DECLARE @TotalAmount as money                                                                          
 DECLARE @tintOppStatus as tinyint               
 DECLARE @tintDefaultClassType NUMERIC
 DECLARE @numDefaultClassID NUMERIC;SET @numDefaultClassID=0
 
 --Get Default Item Class for particular user based on Domain settings  
 SELECT @tintDefaultClassType=isnull(tintDefaultClassType,0) FROM dbo.Domain  WHERE numDomainID = @numDomainID 

 IF @tintDefaultClassType=0
      SELECT @numDefaultClassID=NULLIF(numDefaultClass,0) FROM dbo.UserMaster  WHERE numUserDetailId =@numUserCntID AND numDomainID = @numDomainID 


--If new Oppertunity
if @numOppID = 0                                                                          
 begin     
  declare @intOppTcode as numeric(9)                           
  Select @intOppTcode=max(numOppId)from OpportunityMaster                
  set @intOppTcode =isnull(@intOppTcode,0) + 1                                                
  set @vcPOppName=@vcPOppName+ '-'+convert(varchar(4),year(getutcdate()))  +'-A'+ convert(varchar(10),@intOppTcode)                                  
  
  IF ISNULL(@numCurrencyID,0) = 0 
	BEGIN
	 SET @fltExchangeRate=1
	 SELECT @numCurrencyID= ISNULL(numCurrencyID,0) FROM dbo.Domain WHERE numDomainId=@numDomainId	
	END
  else set @fltExchangeRate=dbo.GetExchangeRate(@numDomainID,@numCurrencyID)
                                                                       
  --Set Default Class If enable User Level Class Accountng 
  DECLARE @numAccountClass AS NUMERIC(18);SET @numAccountClass=0
  SELECT @numAccountClass=ISNULL(numDefaultClass,0) FROM dbo.UserMaster UM JOIN dbo.Domain D ON UM.numDomainID=D.numDomainId
  WHERE D.numDomainId=@numDomainId AND UM.numUserDetailId=@numUserCntID AND ISNULL(D.IsEnableUserLevelClassTracking,0)=1
                                                                       
  insert into OpportunityMaster                                                                          
  (                                                                             
  numContactId,                                                                   
  numDivisionId,                                                                          
  txtComments,                                             
  numCampainID,                                                                          
  bitPublicFlag,                                                                          
  tintSource,
  tintSourceType,                                                             
  vcPOppName,                                                                          
  intPEstimatedCloseDate,                                                                          
  monPAmount,                                                                           
  numCreatedBy,                                                                          
  bintCreatedDate,                                                                           
  numModifiedBy,                                                                          
  bintModifiedDate,                                                                          
  numDomainId,                                                                                                             
  numRecOwner,                                                                          
  lngPConclAnalysis,                                                                         
  tintOppType,                                                              
  numSalesOrPurType,
  numCurrencyID,
  fltExchangeRate,
  numAssignedTo,
  numAssignedBy,
  [tintOppStatus],
  numStatus,
  vcOppRefOrderNo,
  --vcWebApiOrderNo,
  bitStockTransfer,bitBillingTerms,intBillingDays,bitInterestType,fltInterest,vcCouponCode,
  bitDiscountType,fltDiscount,bitPPVariance,vcMarketplaceOrderID,vcMarketplaceOrderReportId,
  numPercentageComplete,numAccountClass,tintTaxOperator,bitUseShippersAccountNo,
  bitUseMarkupShippingRate,
  numMarkupShippingRate,
  intUsedShippingCompany
  )                                                                          
  Values                                                                          
  (                                                                          
  @numContactId,                                                                          
  @numDivisionId,                                                                          
  @Comments,                           
  @CampaignID,                                                                          
  @bitPublicFlag,                     
  @tintSource,
  @tintSourceType,                         
  @vcPOppName,                                                                          
  @dtEstimatedCloseDate,                                                                          
  @monPAmount,                                                                                
  @numUserCntID,                                                                          
 CASE @tintSourceType WHEN 3 THEN @vcMarketplaceOrderDate ELSE GETUTCDATE() END,                                                                          
  @numUserCntID,                                                           
  getutcdate(),                                                                          
  @numDomainId,                                                                                   
  @numUserCntID,                                                                          
  @lngPConclAnalysis,                                                                                                                                                                      
  @tintOppType,                                                                                                                                      
  @numSalesOrPurType,              
  @numCurrencyID, 
  @fltExchangeRate,
  case when @numAssignedTo>0 then @numAssignedTo else null end,
  case when @numAssignedTo>0 then @numUserCntID else null   END,
  @DealStatus,
  @numStatus,
  @vcOppRefOrderNo,
 -- @vcWebApiOrderNo,
  @bitStockTransfer,@bitBillingTerms,@intBillingDays,@bitInterestType,@fltInterest,@vcCouponCode,
  @bitDiscountType,@fltDiscount,1,@vcMarketplaceOrderID,@vcMarketplaceOrderReportId,
  @numPercentageComplete,@numAccountClass,@tintTaxOperator,@bitUseShippersAccountNo,
  @bitUseMarkupShippingRate,
  @numMarkupShippingRate,
  @intUsedShippingCompany
    )                                                                                                                      
  set @numOppID=scope_identity()                                                
  
  --Update OppName as per Name Template
  EXEC dbo.USP_UpdateNameTemplateValue @tintOppType,@numDomainID,@numOppID

  --Map Custom Field	
  DECLARE @tintPageID AS TINYINT;
  SET @tintPageID=CASE WHEN @tintOppType=1 THEN 2 ELSE 6 END 
  	
  EXEC dbo.USP_AddParentChildCustomFieldMap
	@numDomainID = @numDomainID, --  numeric(18, 0)
	@numRecordID = @numOppID, --  numeric(18, 0)
	@numParentRecId = @numDivisionId, --  numeric(18, 0)
	@tintPageID = @tintPageID --  tinyint
 	
  
	IF ISNULL(@numStatus,0) > 0 AND @tintOppType=1 AND isnull(@DealStatus,0)=1
	   BEGIN
	   		EXEC USP_ManageOpportunityAutomationQueue
					@numOppQueueID = 0, -- numeric(18, 0)
					@numDomainID = @numDomainID, -- numeric(18, 0)
					@numOppId = @numOppID, -- numeric(18, 0)
					@numOppBizDocsId = 0, -- numeric(18, 0)
					@numOrderStatus = @numStatus, -- numeric(18, 0)
					@numUserCntID = @numUserCntID, -- numeric(18, 0)
					@tintProcessStatus = 1, -- tinyint
					@tintMode = 1 -- TINYINT
	   END
  ---- inserting Items                                                                          
   
                                                   
  if convert(varchar(10),@strItems) <>'' AND @numOppID>0
  begin                      
   EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems       
	
  insert into                       
   OpportunityItems                                                                          
   (numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,vcItemName,vcModelID,vcManufacturer,vcPathForTImage,monVendorCost,numUOMId,bitWorkOrder,
   numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,numClassID,numToWarehouseItemID,vcAttributes,monAvgCost,bitItemPriceApprovalRequired)
   select @numOppID,X.numItemCode,dbo.fn_UOMConversion(X.numUOM,numItemCode,@numDomainId,null) * x.numUnitHour,
   x.monPrice,x.monTotAmount,X.vcItemDesc,
   CASE X.numWarehouseItmsID 
   WHEN -1 
	THEN 
		CASE 
			WHEN ((SELECT COUNT(*) FROM Item WHERE numDomainID=@numDomainId AND vcSKU= X.vcSKU) > 0)
				THEN
					(SELECT TOP 1 
						numWareHouseItemID 
					FROM 
						WareHouseItems 
					WHERE 
						[numItemID] = X.numItemCode 
						AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId))
			ELSE
				(SELECT 
					[numWareHouseItemID] 
				FROM 
					[WareHouseItems] 
				WHERE 
					[numItemID] = X.numItemCode 
					AND ISNULL(WareHouseItems.vcWHSKU,'') = ISNULL(X.vcSKU,'')
					AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId)) 
		END
	WHEN 0 
	THEN 
		CASE 
		WHEN ((SELECT COUNT(*) FROM Item WHERE numDomainID=@numDomainId AND vcSKU=X.vcSKU) > 0)
			THEN
				(SELECT TOP 1 
					numWareHouseItemID 
				FROM 
					WareHouseItems 
				WHERE 
					[numItemID] = X.numItemCode 
					AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId))
		ELSE
			(SELECT 
				[numWareHouseItemID] 
			FROM 
				[WareHouseItems] 
			WHERE 
				[numItemID] = X.numItemCode 
				AND ISNULL(WareHouseItems.vcWHSKU,'') = ISNULL(X.vcSKU,'')
				AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId)) 
		END
	ELSE  
		X.numWarehouseItmsID 
	END AS numWarehouseItmsID,
	X.ItemType,X.DropShip,X.bitDiscountType,
   X.fltDiscount,X.monTotAmtBefDiscount,X.vcItemName,
   (SELECT vcModelID FROM item WHERE numItemCode = X.numItemCode),
   (SELECT vcManufacturer FROM item WHERE numItemCode = X.numItemCode),
   (SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = X.numItemCode AND BitDefault = 1 AND numdomainId = @numdomainId),
   (select isnull(VN.monCost,0) from Vendor VN, Item IT where IT.numItemCode=VN.numItemCode and  VN.numItemCode=X.numItemCode and  VN.numVendorID=IT.numVendorID),
   X.numUOM,X.bitWorkOrder,X.numVendorWareHouse,X.numShipmentMethod,X.numSOVendorId,X.numProjectID,X.numProjectStageID,Case When @tintDefaultClassType=0 then @numDefaultClassID else (SELECT numItemClass FROM item WHERE numItemCode = X.numItemCode) end,X.numToWarehouseItemID,X.Attributes,
   (SELECT ISNULL(monAverageCost,0) FROM Item I WHERE I.numDomainId=@numDomainId AND I.numItemCode=X.numItemCode),X.bitItemPriceApprovalRequired from(
   SELECT *FROM OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
   WITH                       
   (                                                                          
   numoppitemtCode numeric(9),                                     
   numItemCode numeric(9),                                                                          
   numUnitHour numeric(9,2),                                                                          
   monPrice money,                                                                       
   monTotAmount money,                                                                          
   vcItemDesc varchar(1000),                                    
   numWarehouseItmsID numeric(9),                          
   ItemType varchar(30),      
   DropShip bit,
   bitDiscountType bit,
   fltDiscount float,
   monTotAmtBefDiscount MONEY,
   vcItemName varchar(300),numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9) ,numSOVendorId numeric(9),numProjectID numeric(9),numProjectStageID numeric(9),numToWarehouseItemID NUMERIC(9),Attributes varchar(200),vcSKU VARCHAR(100),bitItemPriceApprovalRequired BIT
   ))X    
   ORDER BY 
   X.numoppitemtCode
    
    
   Update OpportunityItems                       
   set numItemCode=X.numItemCode,    
   numOppId=@numOppID,                     
   numUnitHour=dbo.fn_UOMConversion(X.numUOM,X.numItemCode,@numDomainId,null) * X.numUnitHour,                      
   monPrice=x.monPrice,                      
   monTotAmount=x.monTotAmount,                                  
   vcItemDesc=X.vcItemDesc,                      
   numWarehouseItmsID=X.numWarehouseItmsID,                 
   bitDropShip=X.DropShip,
   bitDiscountType=X.bitDiscountType,
   fltDiscount=X.fltDiscount,
   monTotAmtBefDiscount=X.monTotAmtBefDiscount,vcItemName=X.vcItemName,numUOMId=numUOM,bitWorkOrder=X.bitWorkOrder,
   numVendorWareHouse=X.numVendorWareHouse,numShipmentMethod=X.numShipmentMethod,numSOVendorId=X.numSOVendorId,numProjectId=X.numProjectID,numProjectStageId=X.numProjectStageID,numToWarehouseItemID = X.numToWarehouseItemID,
vcAttributes=X.Attributes,numClassID=(Case When @tintDefaultClassType=0 then @numDefaultClassID else (SELECT numItemClass FROM item WHERE numItemCode = X.numItemCode) END),bitItemPriceApprovalRequired = X.bitItemPriceApprovalRequired
--   ,vcModelID=(SELECT vcModelID FROM item WHERE numItemCode = X.numItemCode),vcManufacturer=(SELECT vcManufacturer FROM item WHERE numItemCode = X.numItemCode)
--  ,vcPathForTImage=(SELECT vcPathForTImage FROM item WHERE numItemCode = X.numItemCode),monVendorCost=(select isnull(VN.monCost,0) from Vendor VN, Item IT where IT.numItemCode=VN.numItemCode and  VN.numItemCode=X.numItemCode and  VN.numVendorID=IT.numVendorID)
   from (SELECT numoppitemtCode as numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,DropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,vcItemName,numUOM,bitWorkOrder,
   numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,numToWarehouseItemID,Attributes,bitItemPriceApprovalRequired
   FROM OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=2]',2)
   WITH  (                      
    numoppitemtCode numeric(9),                                     
    numItemCode numeric(9),                                                                          
    numUnitHour numeric(9,2),                                                                          
    monPrice money,                                         
    monTotAmount money,                                                                          
    vcItemDesc varchar(1000),                                    
    numWarehouseItmsID numeric(9),      
    DropShip bit,
	bitDiscountType bit,
    fltDiscount float,
    monTotAmtBefDiscount MONEY,vcItemName varchar(300),numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId numeric(9),numProjectID numeric(9),numProjectStageID numeric(9),numToWarehouseItemID NUMERIC(9),Attributes varchar(200),bitItemPriceApprovalRequired BIT
   ))X where numoppitemtCode=X.numOppItemID                                          
	
	
   -- Update UOM of opportunity items have not UOM set while exported from Marketplace (means tintSourceType = 3)
	IF ISNULL(@tintSourceType,0) = 3
	BEGIN
		--SELECT * FROM OpportunityMaster WHERE numOppId = 81654
		--SELECT * FROM OpportunityItems WHERE numOppId = 81654
		--SELECT * FROM Item WHERE numItemCode = 822078

		UPDATE OI SET numUOMID = ISNULL(I.numSaleUnit,0), 
					  numUnitHour = dbo.fn_UOMConversion(ISNULL(I.numSaleUnit,0),OI.numItemCode,@numDomainId,null) * OI.numUnitHour
		FROM OpportunityItems OI
		JOIN Item I ON OI.numItemCode = I.numItemCode
		WHERE numOppId = @numOppID
		AND I.numDomainID = @numDomainId

	END	
   --Update OpportunityItems
   --set 
   --FROM (SELECT numItemCode,vcItemName,[txtItemDesc],vcModelID FROM item WHERE numItemCode IN (SELECT [numItemCode] FROM OpportunityItems WHERE numOppID = @numOppID))X
   --WHERE OpportunityItems.[numItemCode] = X.numItemCode 
                                     
   insert into OppWarehouseSerializedItem                                                                          
   (numOppID,numWarehouseItmsDTLID,numOppItemID,numWarehouseItmsID)                      
   select @numOppID,X.numWarehouseItmsDTLID,X.numoppitemtCode,X.numWItmsID from(                                                                          
   SELECT * FROM OPENXML (@hDocItem,'/NewDataSet/SerialNo',2)                                                                          
   WITH                        
   (                                               
   numWarehouseItmsDTLID numeric(9),                      
   numoppitemtCode numeric(9),                      
   numWItmsID numeric(9)                                                     
   ))X                                    
               
                                 
   update OppWarehouseSerializedItem                       
   set numOppItemID=X.numoppitemtCode                                
   from                       
   (SELECT numWarehouseItmsDTLID as DTLID,O.numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/SerialNo',2)                                                                         
   WITH(numWarehouseItmsDTLID numeric(9),numoppitemtCode numeric(9),numWItmsID numeric(9))                                
   join OpportunityItems O on O.numWarehouseItmsID=numWItmsID and O.numOppID=@numOppID)X                 
   where numOppID=@numOppID and numWarehouseItmsDTLID=X.DTLID      
    
--   insert into OpportunityKitItems(numOppKitItem,numChildItem,intQuantity,numWareHouseItemId)    
--   SELECT * FROM OPENXML (@hDocItem,'/NewDataSet/ChildItems',2)                                                                          
--   WITH                        
--   (                                               
--   numoppitemtCode numeric(9),                      
--   numItemCode numeric(9),    
--   numQtyItemsReq integer,  
--   numWarehouseItmsID numeric(9)                                                       
--   )    
      
    
    
    
	EXEC USP_ManageSOWorkOrder @numDomainId,@numOppID,@strItems,@numUserCntID
    
                              
                          
   EXEC sp_xml_removedocument @hDocItem                                     
        end          
   set @TotalAmount=0                                                           
   select @TotalAmount=sum(monTotAmount)from OpportunityItems where numOppId=@numOppID                                                                          
  update OpportunityMaster set  monPamount=@TotalAmount                                                          
  where numOppId=@numOppID                                                 
                      
     
--Insert Tax for Division   

IF @tintOppType=1 OR (@tintOppType=2 and (select isnull(bitPurchaseTaxCredit,0) from Domain where numDomainId=@numDomainId)=1)
BEGIN                   
	INSERT dbo.OpportunityMasterTaxItems (
		numOppId,
		numTaxItemID,
		fltPercentage
	) SELECT @numOppID,TI.numTaxItemID,dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,TI.numTaxItemID,@numOppId,0,NULL) FROM TaxItems TI JOIN DivisionTaxTypes DTT ON TI.numTaxItemID = DTT.numTaxItemID
	 WHERE DTT.numDivisionID=@numDivisionId AND DTT.bitApplicable=1
	   union 
	  select @numOppID,0,dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,0,@numOppId,1,NULL) 
	  FROM dbo.DivisionMaster WHERE bitNoTax=0 AND numDivisionID=@numDivisionID
END
-- Archieve item based on Item's individual setting
--	UPDATE Item SET IsArchieve = CASE WHEN EXISTS(SELECT * FROM dbo.OpportunityItems WHERE OpportunityItems.numItemCode = I.numItemCode) AND ISNULL(I.bitArchiveItem,0) = 1 
--									  THEN 1 
--									  ELSE 0 
--									  END 
--	FROM Item I JOIN dbo.OpportunityItems OI ON I.numItemCode = OI.numItemCode
	
 END
 
 else                                                                                                                          
 BEGIN                  
	--Declaration
	 DECLARE @tempAssignedTo AS NUMERIC(9)                                                    
	 SET @tempAssignedTo = NULL 
	 
	 SELECT @tintOppStatus = tintOppStatus,@tempAssignedTo=isnull(numAssignedTo,0) FROM   OpportunityMaster WHERE  numOppID = @numOppID

	 IF @tempAssignedTo <> @numAssignedTo
	 BEGIN
		IF (SELECT COUNT(*) FROM BizDocComission WHERE ISNULL(numOppID,0) = @numOppID AND ISNULL(bitCommisionPaid,0) = 1) > 0
		BEGIN
			RAISERROR('COMMISSION_PAID_AGAINST_BIZDOC',16,1)
		END
	 END
 
	--Reverting back the warehouse items                  
	 IF @tintOppStatus = 1 
		BEGIN        
		PRINT 'inside revert'          
			EXEC USP_RevertDetailsOpp @numOppID,0,@numUserCntID                  
		END                  
	-- Update Master table
	
	   IF LEN(ISNULL(@vcOppRefOrderNo,''))>0
	   BEGIN
	   		UPDATE dbo.OpportunityBizDocs SET vcRefOrderNo=@vcOppRefOrderNo WHERE numOppId=@numOppID
	   END
		
	   IF EXISTS (SELECT 1 FROM dbo.OpportunityMaster WHERE numOppId = @numOppID AND tintOppType=1 AND isnull(tintOppStatus,0)=1 AND ISNULL(numStatus,0)!= @numStatus)
	   BEGIN
	   		EXEC USP_ManageOpportunityAutomationQueue
					@numOppQueueID = 0, -- numeric(18, 0)
					@numDomainID = @numDomainID, -- numeric(18, 0)
					@numOppId = @numOppID, -- numeric(18, 0)
					@numOppBizDocsId = 0, -- numeric(18, 0)
					@numOrderStatus = @numStatus, -- numeric(18, 0)
					@numUserCntID = @numUserCntID, -- numeric(18, 0)
					@tintProcessStatus = 1, -- tinyint
					@tintMode = 1 -- TINYINT
	   END 
						
	   UPDATE   OpportunityMaster
	   SET      vcPOppName = @vcPOppName,
				txtComments = @Comments,
				bitPublicFlag = @bitPublicFlag,
				numCampainID = @CampaignID,
				tintSource = @tintSource,
				tintSourceType=@tintSourceType,
				intPEstimatedCloseDate = @dtEstimatedCloseDate,
				numModifiedBy = @numUserCntID,
				bintModifiedDate = GETUTCDATE(),
				lngPConclAnalysis = @lngPConclAnalysis,
				monPAmount = @monPAmount,
				tintActive = @tintActive,
				numSalesOrPurType = @numSalesOrPurType,
				numStatus = @numStatus,
				tintOppStatus = @DealStatus,
				vcOppRefOrderNo=@vcOppRefOrderNo,
				--vcWebApiOrderNo = @vcWebApiOrderNo,
				bintOppToOrder=(Case when @tintOppStatus=0 and @DealStatus=1 then GETUTCDATE() else bintOppToOrder end),
				bitDiscountType=@bitDiscountType,fltDiscount=@fltDiscount,bitBillingTerms=@bitBillingTerms,
				intBillingDays=@intBillingDays,bitInterestType=@bitInterestType,fltInterest=@fltInterest,
				tintTaxOperator=@tintTaxOperator,numDiscountAcntType=@numDiscountAcntType,vcCouponCode = @vcCouponCode,
				numPercentageComplete =@numPercentageComplete,bitUseShippersAccountNo = @bitUseShippersAccountNo,
				bitUseMarkupShippingRate = @bitUseMarkupShippingRate,
				numMarkupShippingRate = @numMarkupShippingRate,
				intUsedShippingCompany = @intUsedShippingCompany
	   WHERE    numOppId = @numOppID   
	   
	---Updating if organization is assigned to someone                                                      
	   IF ( @tempAssignedTo <> @numAssignedTo AND @numAssignedTo <> '0' ) 
		BEGIN                                   
			UPDATE  OpportunityMaster SET     numAssignedTo = @numAssignedTo, numAssignedBy = @numUserCntID WHERE   numOppId = @numOppID                                                    
		END                                                     
	   ELSE 
	   IF ( @numAssignedTo = 0 ) 
		BEGIN                
			UPDATE  OpportunityMaster SET     numAssignedTo = 0, numAssignedBy = 0 WHERE   numOppId = @numOppID
		END                                                                      
		---- Updating Opp Items
		if convert(varchar(10),@strItems) <>'' AND @numOppID>0                                                                         
		begin
			   EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems                                      
			   --Delete Items
			   delete from OpportunityKitItems where numOppID=@numOppID and numOppItemID not in                       
			   (SELECT numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/Item',2)                                                                          
			   WITH  (numoppitemtCode numeric(9)))                      
                                        
--               ---- ADDED BY Manish Anjara : Jun 26,2013 - Archive item based on Item's individual setting
--               UPDATE Item SET IsArchieve = CASE WHEN EXISTS(SELECT * FROM dbo.OpportunityItems WHERE OpportunityItems.numItemCode = I.numItemCode) AND ISNULL(I.bitArchiveItem,0) = 1 
--												 THEN 1 
--												 ELSE 0 
--												 END 
--			   FROM Item I JOIN dbo.OpportunityItems OI ON I.numItemCode = OI.numItemCode
--					 	
--			   UPDATE Item SET IsArchieve = CASE WHEN ISNULL(I.bitArchiveItem,0) = 1 THEN 0 ELSE 1 END
--			   FROM Item I
--			   JOIN OpportunityItems OI ON I.numItemCode = OI.numItemCode
--			   WHERE numOppID = @numOppID and OI.numItemCode NOT IN (SELECT numItemCode FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) 
--																	 WITH (numItemCode numeric(9)))

			   delete from OpportunityItems where numOppID=@numOppID and numoppitemtCode not in                       
			   (SELECT numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/Item',2)                                                                          
			   WITH  (numoppitemtCode numeric(9)))   
	                                
			   insert into OpportunityItems                                                                          
			   (numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,[vcItemName],[vcModelID],[vcManufacturer],vcPathForTImage,monVendorCost,numUOMId,bitWorkOrder,numVendorWareHouse,numToWarehouseItemID,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,vcAttributes,numClassID,monAvgCost,bitItemPriceApprovalRequired)
			   select @numOppID,X.numItemCode,dbo.fn_UOMConversion(X.numUOM,numItemCode,@numDomainId,null) * x.numUnitHour,x.monPrice,x.monTotAmount,X.vcItemDesc,X.numWarehouseItmsID,X.ItemType,X.DropShip,X.bitDiscountType,X.fltDiscount,X.monTotAmtBefDiscount,X.vcItemName,(SELECT vcModelID FROM item WHERE numItemCode = X.numItemCode),(SELECT vcManufacturer FROM item WHERE numItemCode = X.numItemCode),(SELECT TOP 1 vcPathForTImage FROM dbo.ItemImages WHERE numItemCode = X.numItemCode AND BitDefault = 1 AND numDomainId = @numDomainId),(select isnull(VN.monCost,0) from Vendor VN, Item IT where IT.numItemCode=VN.numItemCode and  VN.numItemCode=X.numItemCode and  VN.numVendorID=IT.numVendorID),X.numUOM,X.bitWorkOrder,
			   X.numVendorWareHouse,X.numToWarehouseItemID,X.numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,Attributes,Case When @tintDefaultClassType=0 then @numDefaultClassID else (SELECT numItemClass FROM item WHERE numItemCode = X.numItemCode) END,
			   (SELECT ISNULL(monAverageCost,0) FROM Item I WHERE I.numDomainId=@numDomainId AND I.numItemCode=X.numItemCode), X.bitItemPriceApprovalRequired from(
			   SELECT *FROM OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
			   WITH  (                      
				numoppitemtCode numeric(9),                                     
				numItemCode numeric(9),                                                                          
				numUnitHour numeric(9,2),                                                                          
				monPrice money,                                                                       
				monTotAmount money,                                                                          
				vcItemDesc varchar(1000),                                    
				numWarehouseItmsID numeric(9),
				numToWarehouseItemID numeric(9),                   
				Op_Flag tinyint,                            
				ItemType varchar(30),      
				DropShip bit,
				bitDiscountType bit,
				fltDiscount float,
				monTotAmtBefDiscount MONEY,
				vcItemName VARCHAR(300),numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId numeric(9),numProjectID numeric(9),numProjectStageID numeric(9),Attributes varchar(200), bitItemPriceApprovalRequired BIT
				 ))X                                     
				--Update items                 
			   Update OpportunityItems                       
			   set numItemCode=X.numItemCode,    
			   numOppId=@numOppID,                       
			   numUnitHour=dbo.fn_UOMConversion(X.numUOM,X.numItemCode,@numDomainId,null) * x.numUnitHour,                      
			   monPrice=x.monPrice,                      
			   monTotAmount=x.monTotAmount,                                  
			   vcItemDesc=X.vcItemDesc,                      
			   numWarehouseItmsID=X.numWarehouseItmsID,
			   numToWarehouseItemID = X.numToWarehouseItemID,             
			   bitDropShip=X.DropShip,
			   bitDiscountType=X.bitDiscountType,
			   fltDiscount=X.fltDiscount,
			   monTotAmtBefDiscount=X.monTotAmtBefDiscount,vcItemName=X.vcItemName,numUOMId=numUOM,
			   numVendorWareHouse=X.numVendorWareHouse,numShipmentMethod=X.numShipmentMethod,numSOVendorId=X.numSOVendorId,numProjectId=X.numProjectID,numProjectStageId=X.numProjectStageID,vcAttributes=X.Attributes,bitItemPriceApprovalRequired=X.bitItemPriceApprovalRequired,bitWorkOrder=X.bitWorkOrder
			   from (SELECT numoppitemtCode as numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,numToWarehouseItemID,DropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,vcItemName,numUOM,numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,Attributes,bitItemPriceApprovalRequired,bitWorkOrder  FROM OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=2]',2)                                                                
			   WITH  (                      
				numoppitemtCode numeric(9),                                     
				numItemCode numeric(9),                                                                          
				numUnitHour numeric(9,2),                                                                          
				monPrice money,                                         
				monTotAmount money,                                                                          
				vcItemDesc varchar(1000),                                    
				numWarehouseItmsID numeric(9), 
				numToWarehouseItemID NUMERIC(18,0),     
				DropShip bit,
				bitDiscountType bit,
				fltDiscount float,
				monTotAmtBefDiscount MONEY,vcItemName varchar(300),numUOM NUMERIC(9),numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId numeric(9),numProjectID numeric(9),numProjectStageID numeric(9),Attributes varchar(200),bitItemPriceApprovalRequired BIT, bitWorkOrder BIT                                              
			   ))X where numoppitemtCode=X.numOppItemID                           
			                                    
			   EXEC USP_ManageSOWorkOrder @numDomainId,@numOppID,@strItems,@numUserCntID
			   		                                                                      
			   EXEC sp_xml_removedocument @hDocItem                                               
		end     

		--FOLLOWING DELETE STATEMENT IS COMMENTED BECUASE NOW WE ARE NOT ALLOWING USER TO EIDT SERIAL/LOT ITEM UNTILL USER REMOVED ASSIGNED SERIAL/LOT NUMBERS
	    --Delete previously added serial lot number added for opportunity
		--DELETE FROM OppWarehouseSerializedItem WHERE numOppID = @numOppID                                                         
END

----------------generic section will be called in both insert and update ---------------------------------
-- Archieve item based on Item's individual setting
--	UPDATE Item SET IsArchieve = CASE WHEN EXISTS(SELECT * FROM dbo.OpportunityItems WHERE OpportunityItems.numItemCode = I.numItemCode) AND ISNULL(I.bitArchiveItem,0) = 1 
--									  THEN 1 
--									  ELSE 0 
--									  END 
--	FROM Item I JOIN dbo.OpportunityItems OI ON I.numItemCode = OI.numItemCode
	
if convert(varchar(10),@strItems) <>'' AND @numOppID>0                                                                        
  begin 
EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems

--delete from OpportunityKitItems where numOppID=@numOppID and numOppItemID 
--	not in (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppID=@numOppID)                      
		
--Update Kit Items                 
Update OKI set numQtyItemsReq=numQtyItemsReq_Orig * OI.numUnitHour
			   FROM OpportunityKitItems OKI JOIN OpportunityItems OI ON OKI.numOppItemID=OI.numoppitemtCode and OKI.numOppId=OI.numOppId  
			   WHERE OI.numOppId=@numOppId 

--Insert Kit Items                 
INSERT into OpportunityKitItems                                                                          
		(numOppId,numOppItemID,numChildItemID,numWareHouseItemId,numQtyItemsReq,numQtyItemsReq_Orig,numUOMId,numQtyShipped)
  SELECT @numOppId,OI.numoppitemtCode,ID.numChildItemID,ID.numWareHouseItemId,ID.numQtyItemsReq * OI.numUnitHour,ID.numQtyItemsReq,ID.numUOMId,0
	 FROM OpportunityItems OI JOIN ItemDetails ID ON OI.numItemCode=ID.numItemKitID WHERE 
	OI.numOppId=@numOppId AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityKitItems OKI WHERE OKI.numOppId=@numOppID)
--	OI.numoppitemtCode NOT IN (SELECT numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=2]',2)                                                                
--			   WITH  (                      
--				numoppitemtCode numeric(9))X)
			   
EXEC sp_xml_removedocument @hDocItem  
END

/*Added by chintan - While creating SO from PO it wasn't puting Item Qty to Allocation. this segment was being called from USP_OppSaveMilestone which No longer in use after changes anoop did. */     
--Updating the warehouse items              
declare @tintShipped as tinyint               
select @tintOppStatus=tintOppStatus,@tintOppType=tintOppType,@tintShipped=tintShipped from OpportunityMaster where numOppID=@numOppID              
if @tintOppStatus=1               
	begin         
	PRINT 'inside USP_UpdatingInventoryonCloseDeal'
		exec USP_UpdatingInventoryonCloseDeal @numOppID,@numUserCntID
	end              

declare @tintCRMType as numeric(9)        
declare @AccountClosingDate as datetime        

select @numDivisionID=numDivisionID,@AccountClosingDate=bintAccountClosingDate from  OpportunityMaster where numOppID=@numOppID         
select @tintCRMType=tintCRMType from divisionmaster where numDivisionID =@numDivisionID 

--When deal is won                                         
if @DealStatus = 1 
begin           
	 if @AccountClosingDate is null                   
	  update OpportunityMaster set bintAccountClosingDate=getutcdate() where numOppID=@numOppID              
end         
--When Deal is Lost
else if @DealStatus = 2
begin         
	 select @AccountClosingDate=bintAccountClosingDate from  OpportunityMaster where numOppID=@numOppID         
	 if @AccountClosingDate is null                   
	  update OpportunityMaster set bintAccountClosingDate=getutcdate() where numOppID=@numOppID
end                    

/*Added by chintan -  Biz usually automatically promotes the record from Lead/Prospect to Account When a Sales Opp/Order is made against it*/  
-- Promote Lead to Prospect when Sales/Purchase Opp is created against it
if @tintCRMType=0 AND @DealStatus = 0 --Lead & Open Opp
begin        
	update divisionmaster set tintCRMType=1,bintLeadProm=getutcdate(),bintLeadPromBy=@numUserCntID,bintProsProm=getutcdate(),bintProsPromBy=@numUserCntID        
	where numDivisionID=@numDivisionID        
end        
-- Promote Lead to Account when Sales/Purchase Order is created against it
ELSE if @tintCRMType=0 AND @DealStatus = 1 --Lead & Order
begin        
	update divisionmaster set tintCRMType=2,bintLeadProm=getutcdate(),bintLeadPromBy=@numUserCntID,bintProsProm=getutcdate(),bintProsPromBy=@numUserCntID        
	where numDivisionID=@numDivisionID        
end
--Promote Prospect to Account
else if @tintCRMType=1 AND @DealStatus = 1 
begin        
	update divisionmaster set tintCRMType=2,bintProsProm=getutcdate(),bintProsPromBy=@numUserCntID        
	where numDivisionID=@numDivisionID        
end

--Add/Update Address Details
DECLARE @vcStreet  varchar(100),@vcCity  varchar(50),@vcPostalCode  varchar(15),@vcCompanyName  varchar(100),@vcAddressName VARCHAR(50)      
DECLARE @numState  numeric(9),@numCountry  numeric(9), @numCompanyId  numeric(9) 
DECLARE @bitIsPrimary BIT;

select  @vcCompanyName=vcCompanyName,@numCompanyId=div.numCompanyID from CompanyInfo Com                            
join divisionMaster Div                            
on div.numCompanyID=com.numCompanyID                            
where div.numdivisionID=@numDivisionId

--Bill Address
IF @numBillAddressId>0
BEGIN
 SELECT  @vcStreet=ISNULL(vcStreet,''),@vcCity=ISNULL(vcCity,''),@vcPostalCode=ISNULL(vcPostalCode,''),
		@numState=ISNULL(numState,0),@numCountry=ISNULL(numCountry,0),@bitIsPrimary=bitIsPrimary,
		@vcAddressName=vcAddressName
            FROM  dbo.AddressDetails WHERE   numDomainID = @numDomainID AND numAddressID = @numBillAddressId
  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 0, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = @vcCompanyName, --  varchar(100)
	@numCompanyId = 0, --  numeric(9, 0)
	@vcAddressName = @vcAddressName
END
  
  
  --Ship Address
IF @numShipAddressId>0
BEGIN
	SELECT  @vcStreet=ISNULL(vcStreet,''),@vcCity=ISNULL(vcCity,''),@vcPostalCode=ISNULL(vcPostalCode,''),
		@numState=ISNULL(numState,0),@numCountry=ISNULL(numCountry,0),@bitIsPrimary=bitIsPrimary,
		@vcAddressName=vcAddressName
            FROM  dbo.AddressDetails WHERE   numDomainID = @numDomainID AND numAddressID = @numShipAddressId
 



  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 1, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = @vcCompanyName, --  varchar(100)
	@numCompanyId =@numCompanyId, --  numeric(9, 0)
	@vcAddressName = @vcAddressName
END

--Delete Tax for Opportunity Items if item deleted 
DELETE FROM OpportunityItemsTaxItems WHERE numOppId=@numOppId AND numOppItemID NOT IN (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID)

--Insert Tax for Opportunity Items
INSERT INTO dbo.OpportunityItemsTaxItems (
	numOppId,
	numOppItemID,
	numTaxItemID
) SELECT @numOppId,OI.numoppitemtCode,TI.numTaxItemID FROM dbo.OpportunityItems OI JOIN dbo.ItemTax IT ON OI.numItemCode=IT.numItemCode JOIN
TaxItems TI ON TI.numTaxItemID=IT.numTaxItemID WHERE OI.numOppId=@numOppID AND IT.bitApplicable=1 AND 
OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
UNION
  select @numOppId,OI.numoppitemtCode,0 FROM dbo.OpportunityItems OI JOIN dbo.Item I ON OI.numItemCode=I.numItemCode
   WHERE OI.numOppId=@numOppID  AND I.bitTaxable=1 AND
OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)

  
 UPDATE OpportunityMaster SET monDealAmount=dbo.GetDealAmount(@numOppID,getdate(),0) WHERE numOppId=@numOppID
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SET @numOppID = 0

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityBizDocs_AutoFulFill')
DROP PROCEDURE USP_OpportunityBizDocs_AutoFulFill
GO
CREATE PROCEDURE [dbo].[USP_OpportunityBizDocs_AutoFulFill] 
	@numDomainID NUMERIC(18,0),
	@numUserCntID NUMERIC(18,0),
	@numOppId NUMERIC(18,0),
	@numOppBizDocID NUMERIC(18,0)
AS  
BEGIN 
BEGIN TRY
	DECLARE @vcItemType AS CHAR(1)
	DECLARE @bitDropShip AS BIT
	DECLARE @numOppItemID AS INT
	DECLARE @numWarehouseItemID AS INT
	DECLARE @numQty AS INT
	DECLARE @numQtyShipped AS INT
	DECLARE @numXmlQty AS INT
	DECLARE @vcSerialLot AS VARCHAR(MAX)
	DECLARE @numSerialCount AS INT
	DECLARE @bitSerial AS BIT
	DECLARE @bitLot AS BIT
	DECLARE @bitAssembly AS BIT
	DECLARE @bitKit AS BIT

	DECLARE @i AS INT = 1
	DECLARE @COUNT AS INT  
	DECLARE @numNewQtyShipped AS INT


	/* VERIFY BIZDOC IS EXISTS OR NOT */
	IF NOT EXISTS (SELECT numOppBizDocsId FROM OpportunityBizDocs WHERE numOppBizDocsId = @numOppBizDocID)
	BEGIN
		RAISERROR('FULFILLMENT_BIZDOC_IS_DELETED',16,1)
	END

	/* VERIFY WHETHER BIZDOC IS ALREADY FULLFILLED BY SOMEBODY ELSE */
	IF EXISTS (SELECT numOppBizDocsId FROM OpportunityBizDocs WHERE numOppBizDocsId = @numOppBizDocID AND bitFulfilled = 1) 
	BEGIN
		RAISERROR('FULFILLMENT_BIZDOC_ALREADY_FULFILLED',16,1)
	END

	IF EXISTS(SELECT numWOId FROm WorkOrder WHERE numOppId=@numOppId AND numWOStatus <> 23184) --WORK ORDER NOT YET COMPLETED
	BEGIN
		RAISERROR('WORK_ORDER_NOT_COMPLETED',16,1)
	END

	BEGIN TRANSACTION

		BEGIN /* REVERT SEIRAL/LOT# NUMBER ASSIGNED FROM PRODCUT&SERVICES TAB */
			UPDATE WHIDL
				SET WHIDL.numQty = (CASE WHEN Item.bitLotNo = 1 THEN ISNULL(WHIDL.numQty,0) + ISNULL(OWSI.numQty,0) ELSE 1 END)
			FROM 
				WareHouseItmsDTL WHIDL
			INNER JOIN
				OppWarehouseSerializedItem OWSI
			ON
				WHIDL.numWareHouseItmsDTLID = OWSI.numWarehouseItmsDTLID
				AND WHIDL.numWareHouseItemID = OWSI.numWarehouseItmsID
			INNER JOIN
				WareHouseItems
			ON
				WHIDL.numWareHouseItemID = WareHouseItems.numWareHouseItemID
			INNER JOIN
				Item
			ON
				WareHouseItems.numItemID = Item.numItemCode
			WHERE
				OWSI.numOppID=@numOppId
				AND OWSI.numOppBizDocsId IS NULL
		END

		BEGIN /* GET FULFILLMENT BIZDOC ITEMS */
			DECLARE @TempFinalTable TABLE
			(
				ID INT IDENTITY(1,1),
				numItemCode NUMERIC(18,0),
				vcItemType CHAR(1),
				bitSerial BIT,
				bitLot BIT,
				bitAssembly BIT,
				bitKit BIT,
				numOppItemID NUMERIC(18,0),
				numWarehouseItemID NUMERIC(18,0),
				numQtyShipped NUMERIC(18,0),
				numQty NUMERIC(18,0),
				bitDropShip BIT,
				vcSerialLot VARCHAR(MAX)
			)

			INSERT INTO @TempFinalTable
			(
				numItemCode,
				vcItemType,
				bitSerial,
				bitLot,
				bitAssembly,
				bitKit,
				numOppItemID,
				numWarehouseItemID,
				numQtyShipped,
				numQty,
				bitDropShip,
				vcSerialLot
			)
			SELECT
				OpportunityBizDocItems.numItemCode,
				ISNULL(Item.charItemType,''),
				ISNULL(Item.bitSerialized,0),
				ISNULL(Item.bitLotNo,0),
				ISNULL(Item.bitAssembly,0),
				ISNULL(Item.bitKitParent,0),
				ISNULL(OpportunityItems.numoppitemtcode,0),
				ISNULL(OpportunityBizDocItems.numWarehouseItmsID,0),
				ISNULL(OpportunityItems.numQtyShipped,0),
				ISNULL(OpportunityBizDocItems.numUnitHour,0),
				ISNULL(OpportunityItems.bitDropShip,0),
				''
			FROM
				OpportunityBizDocs
			INNER JOIN
				OpportunityBizDocItems
			ON
				OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
			INNER JOIN
				OpportunityItems
			ON
				OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtCode
			INNER JOIN
				Item
			ON
				OpportunityBizDocItems.numItemCode = Item.numItemCode
			WHERE
				numOppBizDocsId = @numOppBizDocID
				AND ISNULL(OpportunityItems.bitDropShip,0) = 0
		END

		BEGIN /* GET ALL ITEMS ADDED TO BIZDOC WITH ASSIGNED SERIAL/LOT# FOR SERIAL/LOT ITEM  */
			DECLARE @TempItemXML TABLE
			(
				numOppItemID NUMERIC(18,0),
				numQty INT,
				numWarehouseItemID NUMERIC(18,0),
				vcSerialLot VARCHAR(MAX)
			)

			INSERT INTO
				@TempItemXML
			SELECT
				OBI.numOppItemID,
				OBI.numUnitHour,
				OBI.numWarehouseItmsID,
				STUFF((SELECT 
							',' + WIDL.vcSerialNo + (CASE WHEN ISNULL(I.bitLotNo,0) = 1 THEN '(' + CAST(OWSI.numQty AS VARCHAR) + ')'  ELSE '' END)
						FROM 
							OppWarehouseSerializedItem OWSI
						INNER JOIN
							WareHouseItmsDTL WIDL
						ON
							OWSI.numWarehouseItmsDTLID = WIDL.numWareHouseItmsDTLID
						INNER JOIN
							WareHouseItems WI
						ON
							OWSI.numWarehouseItmsID = WI.numWareHouseItemID
						INNER JOIN
							Item I
						ON
							WI.numItemID = I.numItemCode
						WHERE 
							OWSI.numOppID = @numOppId
							AND OWSI.numOppItemID = OBI.numOppItemID
							AND ISNULL(OWSI.numOppBizDocsId,0) = 0 --THIS CONDITION IS REQUIRED BECAUSE IT IS AUTOFULFILLMENT
					for xml path('')
				),1,1,'') AS vcSerialLot
			FROM 
				OpportunityBizDocItems OBI
			WHERE
				OBI.numOppBizDocID = @numOppBizDocID
		END

		BEGIN /* VALIDATE ALL THE DETAILS BEFORE FULFILLING ITEMS */
		
			SELECT @COUNT=COUNT(*) FROM @TempFinalTable

			DECLARE @TempLot TABLE
			(
				vcLotNo VARCHAR(100),
				numQty INT 
			)

			WHILE @i <= @COUNT
			BEGIN
				SELECT
					@numOppItemID=numOppItemID,
					@numWarehouseItemID=numWarehouseItemID,
					@numQty=numQty,
					@bitSerial=bitSerial,
					@bitLot=bitLot 
				FROM 
					@TempFinalTable 
				WHERE 
					ID = @i

				--GET QUENTITY RECEIVED FROM XML PARAMETER
				SELECT 
					@numXmlQty = numQty, 
					@vcSerialLot=vcSerialLot, 
					@numSerialCount = (CASE WHEN (@bitSerial = 1 OR @bitLot = 1) THEN (SELECT COUNT(*) FROM dbo.SplitString(vcSerialLot,',')) ELSE 0 END)  
				FROM 
					@TempItemXML 
				WHERE 
					numOppItemID = @numOppItemID 

				--CHECK IF QUANTITY OF ITEM IN OpportunityBizDocItems TBALE IS SAME AS QUANTITY SUPPLIED FROM XML
				IF @numQty <> @numXmlQty
				BEGIN
					RAISERROR('QTY_MISMATCH',16,1)
				END

				--IF ITEM IS SERIAL ITEM CHEKC THAT VALID AND REQUIRED NUMBER OF SERIALS ARE PROVIDED BY USER
				IF @bitSerial = 1 
				BEGIN
					IF @numSerialCount <> @numQty
					BEGIN
						RAISERROR('REQUIRED_SERIALS_NOT_PROVIDED',16,1)
					END
					ELSE
					BEGIN
						IF (SELECT COUNT(*) FROM WareHouseItmsDTL WHERE numWareHouseItemID = @numWarehouseItemID AND numQty > 0 AND vcSerialNo IN (SELECT OutParam FROM dbo.SplitString(@vcSerialLot,','))) <> @numSerialCount
						BEGIN
							RAISERROR('INVALID_SERIAL_NUMBERS',16,1)
						END
					END
				END

				IF @bitLot = 1
				BEGIN
					-- DELETE DATA OF PREVIOUS ITERATION FROM TABLE
					DELETE FROM @TempLot

					--CONVERTS COMMA SEPERATED STRING TO LOTNo AND QTY 
					INSERT INTO
						@TempLot
					SELECT
						SUBSTRING(OutParam,0,PATINDEX('%(%', @vcSerialLot)),
						CAST(SUBSTRING(OutParam,(PATINDEX('%(%', @vcSerialLot) + 1),(PATINDEX('%)%', @vcSerialLot) - PATINDEX('%(%', @vcSerialLot) - 1)) AS INT)
					FROM
						dbo.SplitString(@vcSerialLot,',')

					--CHECKS WHETHER PROVIDED QTY OF LOT NO IS SAME IS REQUIRED QTY
					IF (SELECT SUM(numQty) FROM @TempLot) <> @numQty
					BEGIN
						RAISERROR('REQUIRED_LOTNO_NOT_PROVIDED',16,1)
					END
					ELSE
					BEGIN
						-- CHECKS WHETHER LOT NO EXISTS IN WareHouseItmsDTL
						IF (SELECT COUNT(*) FROM WareHouseItmsDTL WHERE numWareHouseItemID = @numWarehouseItemID AND vcSerialNo IN (SELECT vcLotNo FROM @TempLot)) <> (SELECT COUNT(*) FROM @TempLot)
						BEGIN
							RAISERROR('INVALID_LOT_NUMBERS',16,1)
						END
						ELSE
						BEGIN
							-- CHECKS WHETHER LOT NO HAVE ENOUGH QTY TO FULFILL ITEM QTY
							IF (SELECT 
									COUNT(*) 
								FROM 
									WareHouseItmsDTL 
								INNER JOIN
									@TempLot TEMPLOT
								ON
									WareHouseItmsDTL.vcSerialNo = TEMPLOT.vcLotNo
								WHERE 
									numWareHouseItemID = @numWarehouseItemID
									AND TEMPLOT.numQty > WareHouseItmsDTL.numQty) > 0
							BEGIN
								RAISERROR('SOME_LOTNO_DO_NOT_HAVE_ENOUGH_QTY',16,1)
							END
						END
					END
				END

		
				UPDATE @TempFinalTable SET vcSerialLot = @vcSerialLot WHERE ID=@i

				SET @i = @i + 1
			END
		END
	
		SET @i = 1
		SELECT @COUNT = COUNT(*) FROM @TempFinalTable
		DECLARE @vcDescription AS VARCHAR(300)
		DECLARE @numAllocation AS INT

		DECLARE @j INT = 0
		DECLARE @ChildCount AS INT
		DECLARE @vcChildDescription AS VARCHAR(300)
		DECLARE @numChildAllocation AS INT
		DECLARE @numOppChildItemID AS INT
		DECLARE @numChildItemCode AS INT
		DECLARE @numChildWarehouseItemID AS INT
		DECLARE @numChildQty AS INT
		DECLARE @numChildQtyShipped AS INT

		DECLARE @TempKitSubItems TABLE
		(
			ID INT IDENTITY(1,1),
			numOppChildItemID NUMERIC(18,0),
			numChildItemCode NUMERIC(18,0),
			numChildWarehouseItemID NUMERIC(18,0),
			numChildQty INT,
			numChildQtyShipped INT
		)

		WHILE @i <= @COUNT
		BEGIN
			SELECT 
				@vcItemType=vcItemType,
				@bitSerial=bitSerial,
				@bitLot=bitLot,
				@bitAssembly=bitAssembly,
				@bitKit=bitKit,
				@bitDropShip=bitDropShip,
				@numOppItemID=numOppItemID,
				@numQty=numQty,
				@numQtyShipped = numQtyShipped,
				@numWarehouseItemID=numWarehouseItemID,
				@vcSerialLot = vcSerialLot
			FROM 
				@TempFinalTable 
			WHERE 
				ID = @i

			--MAKE INVENTIRY CHANGES IF INVENTORY ITEM
			IF (UPPER(@vcItemType) = 'P' AND @bitDropShip = 0)
			BEGIN
				SET @vcDescription='SO Qty Shipped (Qty:' + CAST(@numQty AS VARCHAR(10)) + ' Shipped:' +  CAST(@numQtyShipped AS VARCHAR(10)) + ')'
						 
				IF @bitKit = 1
				BEGIN
					-- CLEAR DATA OF PREVIOUS ITERATION
					DELETE FROM @TempKitSubItems

					-- GET KIT SUB ITEMS DETAIL
					INSERT INTO @TempKitSubItems
					(
						numOppChildItemID,
						numChildItemCode,
						numChildWarehouseItemID,
						numChildQty,
						numChildQtyShipped
					)
					SELECT 
						ISNULL(numOppChildItemID,0),
						ISNULL(I.numItemCode,0),
						ISNULL(numWareHouseItemId,0),
						ISNULL(numQtyItemsReq,0),
						ISNULL(numQtyShipped,0)
					FROM 
						OpportunityKitItems OKI 
					JOIN 
						Item I 
					ON 
						OKI.numChildItemID=I.numItemCode    
					WHERE 
						charitemtype='P' 
						AND ISNULL(numWareHouseItemId,0) > 0 
						AND OKI.numOppID=@numOppID 
						AND OKI.numOppItemID=@numOppItemID 

					SET @j = 1
					SELECT @ChildCount=COUNT(*) FROM @TempKitSubItems

					--LOOP OVER ALL CHILD ITEMS AND RELESE ALLOCATION
					WHILE @j <= @ChildCount
					BEGIN
						SELECT
							@numOppChildItemID=numOppChildItemID,
							@numChildItemCode=numChildItemCode,
							@numChildWarehouseItemID=numChildWarehouseItemID,
							@numChildQty=numChildQty,
							@numChildQtyShipped=numChildQtyShipped
						FROM
							@TempKitSubItems
						WHERE 
							ID = @j

						SELECT @numChildAllocation=ISNULL(numAllocation,0) FROM WareHouseItems WHERE numWareHouseItemID=@numChildWarehouseItemID
						
						--REMOVE QTY FROM ALLOCATION
						SET @numChildAllocation = @numChildAllocation - @numChildQty
						IF @numChildAllocation >= 0
						BEGIN
							UPDATE  
								WareHouseItems
							SET     
								numAllocation = @numChildAllocation,
								dtModified = GETDATE() 
							WHERE   
								numWareHouseItemID = @numChildWarehouseItemID      	
							
							UPDATE  
								OpportunityKitItems
							SET 
								numQtyShipped = ISNULL(numQtyShipped,0) + ISNULL(@numChildQty,0)
							WHERE   
								numOppChildItemID = @numOppChildItemID 
								AND numOppId=@numOppId
								AND numOppItemID=@numOppItemID

							SET @vcChildDescription = 'SO KIT Qty Shipped (Qty:' + CAST(@numChildQty AS VARCHAR(10)) + ' Shipped:' +  CAST(@numChildQtyShipped AS VARCHAR(10)) + ')'

							EXEC dbo.USP_ManageWareHouseItems_Tracking
								@numWareHouseItemID = @numChildWarehouseItemID,
								@numReferenceID = @numOppId,
								@tintRefType = 3,
								@vcDescription = @vcChildDescription,
								@numModifiedBy = @numUserCntID,
								@numDomainID = @numDomainID	 
						END
						ELSE
						BEGIN
							RAISERROR ('NOTSUFFICIENTQTY_ALLOCATION',16,1);
						END

						SET @j = @j + 1
					END
				END
				ELSE
				BEGIN
					SELECT @numAllocation = ISNULL(numAllocation, 0) FROM WareHouseItems WHERE numWareHouseItemID = @numWareHouseItemID
				       
					--REMOVE QTY FROM ALLOCATION
					SET @numAllocation = @numAllocation - @numQty	
				       
					IF (@numAllocation >= 0 )
					BEGIN
						UPDATE  
							WareHouseItems
						SET     
							numAllocation = @numAllocation,
							dtModified = GETDATE() 
						WHERE   
							numWareHouseItemID = @numWareHouseItemID      	

						IF @bitSerial = 1
						BEGIN								
							DELETE FROM OppWarehouseSerializedItem WHERE numOppID=@numOppID AND numOppItemID=@numOppItemID AND numWarehouseItmsID=@numWarehouseItemID

							INSERT INTO OppWarehouseSerializedItem
							(
								numWarehouseItmsDTLID,
								numOppID,
								numOppItemID,
								numWarehouseItmsID,
								numQty,
								numOppBizDocsId	
							)
							SELECT
								(SELECT numWarehouseItmsDTLID FROM WareHouseItmsDTL WHERE numWareHouseItemID=@numWarehouseItemID AND vcSerialNo=OutParam),
								@numOppID,
								@numOppItemID,
								@numWarehouseItemID,
								1,
								@numOppBizDocID
							FROM
								dbo.SplitString(@vcSerialLot,',')


							UPDATE WareHouseItmsDTL SET numQty=0 WHERE numWareHouseItemID=@numWarehouseItemID AND vcSerialNo IN (SELECT OutParam FROM dbo.SplitString(@vcSerialLot,','))
						END

						IF @bitLot = 1
						BEGIN
							DELETE FROM @TempLot

							INSERT INTO
								@TempLot
							SELECT
								SUBSTRING(OutParam,0,PATINDEX('%(%', @vcSerialLot)),
								CAST(SUBSTRING(OutParam,(PATINDEX('%(%', @vcSerialLot) + 1),(PATINDEX('%)%', @vcSerialLot) - PATINDEX('%(%', @vcSerialLot) - 1)) AS INT)
							FROM
								dbo.SplitString(@vcSerialLot,',')

							DELETE FROM OppWarehouseSerializedItem WHERE numOppID=@numOppID AND numOppItemID=@numOppItemID AND numWarehouseItmsID=@numWarehouseItemID

							INSERT INTO OppWarehouseSerializedItem
							(
								numWarehouseItmsDTLID,
								numOppID,
								numOppItemID,
								numWarehouseItmsID,
								numQty,
								numOppBizDocsId	
							)
							SELECT
								(SELECT numWarehouseItmsDTLID FROM WareHouseItmsDTL WHERE numWareHouseItemID=@numWarehouseItemID AND vcSerialNo=TEMPLOT.vcLotNo),
								@numOppID,
								@numOppItemID,
								@numWarehouseItemID,
								TEMPLOT.numQty,
								@numOppBizDocID
							FROM
								@TempLot TEMPLOT


							UPDATE	TEMPWareHouseItmsDTL
								SET TEMPWareHouseItmsDTL.numQty = ISNULL(TEMPWareHouseItmsDTL.numQty,0) - ISNULL(TEMPLOT.numQty,0)
							FROM
								WareHouseItmsDTL TEMPWareHouseItmsDTL
							INNER JOIN
								@TempLot TEMPLOT
							ON
								TEMPWareHouseItmsDTL.vcSerialNo = TEMPLOT.vcLotNo
							WHERE
								TEMPWareHouseItmsDTL.numWareHouseItemID = @numWarehouseItemID
						END
					END
					ELSE
					BEGIN
						PRINT 'PARENT: ' + CAST(@numChildWarehouseItemID AS VARCHAR)
						RAISERROR ('NOTSUFFICIENTQTY_ALLOCATION',16,1);
					END
				END

				EXEC dbo.USP_ManageWareHouseItems_Tracking
							@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
							@numReferenceID = @numOppId, --  numeric(9, 0)
							@tintRefType = 3, --  tinyint
							@vcDescription = @vcDescription, --  varchar(100)
							@numModifiedBy = @numUserCntID,
							@numDomainID = @numDomainID	 
			END

			UPDATE  
				OpportunityItems
			SET 
				numQtyShipped = ISNULL(numQtyShipped,0) + ISNULL(@numQty,0)
			WHERE   
				numoppitemtCode = @numOppItemID 
				AND numOppId=@numOppId

			SET @i = @i + 1
		END

		UPDATE OpportunityBizDocs SET bitFulfilled = 1 WHERE numOppBizDocsId=@numOppBizDocID
	COMMIT TRANSACTION
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
END


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityBizDocs_FulFill')
DROP PROCEDURE USP_OpportunityBizDocs_FulFill
GO
CREATE PROCEDURE [dbo].[USP_OpportunityBizDocs_FulFill] 
	@numDomainID NUMERIC(18,0),
	@numUserCntID NUMERIC(18,0),
	@numOppId NUMERIC(18,0),
	@numOppBizDocID NUMERIC(18,0),
	@vcItems VARCHAR(MAX)
AS  
BEGIN 
BEGIN TRY
	DECLARE @vcItemType AS CHAR(1)
	DECLARE @bitDropShip AS BIT
	DECLARE @numOppItemID AS INT
	DECLARE @numWarehouseItemID AS INT
	DECLARE @numQty AS INT
	DECLARE @numQtyShipped AS INT
	DECLARE @numXmlQty AS INT
	DECLARE @vcSerialLot AS VARCHAR(MAX)
	DECLARE @numSerialCount AS INT
	DECLARE @bitSerial AS BIT
	DECLARE @bitLot AS BIT
	DECLARE @bitAssembly AS BIT
	DECLARE @bitKit AS BIT

	DECLARE @i AS INT = 1
	DECLARE @COUNT AS INT  
	DECLARE @numNewQtyShipped AS INT


	/* VERIFY BIZDOC IS EXISTS OR NOT */
	IF NOT EXISTS (SELECT numOppBizDocsId FROM OpportunityBizDocs WHERE numOppBizDocsId = @numOppBizDocID)
	BEGIN
		RAISERROR('FULFILLMENT_BIZDOC_IS_DELETED',16,1)
	END

	/* VERIFY WHETHER BIZDOC IS ALREADY FULLFILLED BY SOMEBODY ELSE */
	IF EXISTS (SELECT numOppBizDocsId FROM OpportunityBizDocs WHERE numOppBizDocsId = @numOppBizDocID AND bitFulfilled = 1) 
	BEGIN
		RAISERROR('FULFILLMENT_BIZDOC_ALREADY_FULFILLED',16,1)
	END

	IF EXISTS(SELECT numWOId FROm WorkOrder WHERE numOppId=@numOppId AND numWOStatus <> 23184) --WORK ORDER NOT YET COMPLETED
	BEGIN
		RAISERROR('WORK_ORDER_NOT_COMPLETED',16,1)
	END

	BEGIN TRANSACTION

		BEGIN /* REVERT SEIRAL/LOT# NUMBER ASSIGNED FROM PRODCUT&SERVICES TAB */
			UPDATE WHIDL
				SET WHIDL.numQty = (CASE WHEN Item.bitLotNo = 1 THEN ISNULL(WHIDL.numQty,0) + ISNULL(OWSI.numQty,0) ELSE 1 END)
			FROM 
				WareHouseItmsDTL WHIDL
			INNER JOIN
				OppWarehouseSerializedItem OWSI
			ON
				WHIDL.numWareHouseItmsDTLID = OWSI.numWarehouseItmsDTLID
				AND WHIDL.numWareHouseItemID = OWSI.numWarehouseItmsID
			INNER JOIN
				WareHouseItems
			ON
				WHIDL.numWareHouseItemID = WareHouseItems.numWareHouseItemID
			INNER JOIN
				Item
			ON
				WareHouseItems.numItemID = Item.numItemCode
			WHERE
				OWSI.numOppID=@numOppId
				AND OWSI.numOppBizDocsId IS NULL
		END

		BEGIN /* GET FULFILLMENT BIZDOC ITEMS */
			DECLARE @TempFinalTable TABLE
			(
				ID INT IDENTITY(1,1),
				numItemCode NUMERIC(18,0),
				vcItemType CHAR(1),
				bitSerial BIT,
				bitLot BIT,
				bitAssembly BIT,
				bitKit BIT,
				numOppItemID NUMERIC(18,0),
				numWarehouseItemID NUMERIC(18,0),
				numQtyShipped NUMERIC(18,0),
				numQty NUMERIC(18,0),
				bitDropShip BIT,
				vcSerialLot VARCHAR(MAX)
			)

			INSERT INTO @TempFinalTable
			(
				numItemCode,
				vcItemType,
				bitSerial,
				bitLot,
				bitAssembly,
				bitKit,
				numOppItemID,
				numWarehouseItemID,
				numQtyShipped,
				numQty,
				bitDropShip,
				vcSerialLot
			)
			SELECT
				OpportunityBizDocItems.numItemCode,
				ISNULL(Item.charItemType,''),
				ISNULL(Item.bitSerialized,0),
				ISNULL(Item.bitLotNo,0),
				ISNULL(Item.bitAssembly,0),
				ISNULL(Item.bitKitParent,0),
				ISNULL(OpportunityItems.numoppitemtcode,0),
				ISNULL(OpportunityBizDocItems.numWarehouseItmsID,0),
				ISNULL(OpportunityItems.numQtyShipped,0),
				ISNULL(OpportunityBizDocItems.numUnitHour,0),
				ISNULL(OpportunityItems.bitDropShip,0),
				''
			FROM
				OpportunityBizDocs
			INNER JOIN
				OpportunityBizDocItems
			ON
				OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
			INNER JOIN
				OpportunityItems
			ON
				OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtCode
			INNER JOIN
				Item
			ON
				OpportunityBizDocItems.numItemCode = Item.numItemCode
			WHERE
				numOppBizDocsId = @numOppBizDocID
				AND ISNULL(OpportunityItems.bitDropShip,0) = 0
		END

		BEGIN /* CONVERT vcItems XML to TABLE */
			DECLARE @TempItemXML TABLE
			(
				numOppItemID NUMERIC(18,0),
				numQty INT,
				numWarehouseItemID NUMERIC(18,0),
				vcSerialLot VARCHAR(MAX)
			)

			DECLARE @idoc int
			EXEC sp_xml_preparedocument @idoc OUTPUT, @vcItems;

			INSERT INTO
				@TempItemXML
			SELECT
				numOppItemID,
				numQty,
				numWarehouseItemID,
				vcSerialLot
			FROM 
				OPENXML (@idoc, '/Items/Item',2)
			WITH 
			(
				numOppItemID NUMERIC(18,0),
				numQty INT,
				numWarehouseItemID NUMERIC(18,0),
				vcSerialLot VARCHAR(MAX)
			);
		END

		BEGIN /* VALIDATE ALL THE DETAILS BEFORE FULFILLING ITEMS */
		
			SELECT @COUNT=COUNT(*) FROM @TempFinalTable

			DECLARE @TempLot TABLE
			(
				vcLotNo VARCHAR(100),
				numQty INT 
			)

			WHILE @i <= @COUNT
			BEGIN
				SELECT
					@numOppItemID=numOppItemID,
					@numWarehouseItemID=numWarehouseItemID,
					@numQty=numQty,
					@bitSerial=bitSerial,
					@bitLot=bitLot 
				FROM 
					@TempFinalTable 
				WHERE 
					ID = @i

				--GET QUENTITY RECEIVED FROM XML PARAMETER
				SELECT 
					@numXmlQty = numQty, 
					@vcSerialLot=vcSerialLot, 
					@numSerialCount = (CASE WHEN (@bitSerial = 1 OR @bitLot = 1) THEN (SELECT COUNT(*) FROM dbo.SplitString(vcSerialLot,',')) ELSE 0 END)  
				FROM 
					@TempItemXML 
				WHERE 
					numOppItemID = @numOppItemID 

				--CHECK IF QUANTITY OF ITEM IN OpportunityBizDocItems TBALE IS SAME AS QUANTITY SUPPLIED FROM XML
				IF @numQty <> @numXmlQty
				BEGIN
					RAISERROR('QTY_MISMATCH',16,1)
				END

				--IF ITEM IS SERIAL ITEM CHEKC THAT VALID AND REQUIRED NUMBER OF SERIALS ARE PROVIDED BY USER
				IF @bitSerial = 1 
				BEGIN
					IF @numSerialCount <> @numQty
					BEGIN
						RAISERROR('REQUIRED_SERIALS_NOT_PROVIDED',16,1)
					END
					ELSE
					BEGIN
						IF (SELECT COUNT(*) FROM WareHouseItmsDTL WHERE numWareHouseItemID = @numWarehouseItemID AND numQty > 0 AND vcSerialNo IN (SELECT OutParam FROM dbo.SplitString(@vcSerialLot,','))) <> @numSerialCount
						BEGIN
							RAISERROR('INVALID_SERIAL_NUMBERS',16,1)
						END
					END
				END

				IF @bitLot = 1
				BEGIN
					-- DELETE DATA OF PREVIOUS ITERATION FROM TABLE
					DELETE FROM @TempLot

					--CONVERTS COMMA SEPERATED STRING TO LOTNo AND QTY 
					INSERT INTO
						@TempLot
					SELECT
						SUBSTRING(OutParam,0,PATINDEX('%(%', @vcSerialLot)),
						CAST(SUBSTRING(OutParam,(PATINDEX('%(%', @vcSerialLot) + 1),(PATINDEX('%)%', @vcSerialLot) - PATINDEX('%(%', @vcSerialLot) - 1)) AS INT)
					FROM
						dbo.SplitString(@vcSerialLot,',')

					--CHECKS WHETHER PROVIDED QTY OF LOT NO IS SAME IS REQUIRED QTY
					IF (SELECT SUM(numQty) FROM @TempLot) <> @numQty
					BEGIN
						RAISERROR('REQUIRED_LOTNO_NOT_PROVIDED',16,1)
					END
					ELSE
					BEGIN
						-- CHECKS WHETHER LOT NO EXISTS IN WareHouseItmsDTL
						IF (SELECT COUNT(*) FROM WareHouseItmsDTL WHERE numWareHouseItemID = @numWarehouseItemID AND vcSerialNo IN (SELECT vcLotNo FROM @TempLot)) <> (SELECT COUNT(*) FROM @TempLot)
						BEGIN
							RAISERROR('INVALID_LOT_NUMBERS',16,1)
						END
						ELSE
						BEGIN
							-- CHECKS WHETHER LOT NO HAVE ENOUGH QTY TO FULFILL ITEM QTY
							IF (SELECT 
									COUNT(*) 
								FROM 
									WareHouseItmsDTL 
								INNER JOIN
									@TempLot TEMPLOT
								ON
									WareHouseItmsDTL.vcSerialNo = TEMPLOT.vcLotNo
								WHERE 
									numWareHouseItemID = @numWarehouseItemID
									AND TEMPLOT.numQty > WareHouseItmsDTL.numQty) > 0
							BEGIN
								RAISERROR('SOME_LOTNO_DO_NOT_HAVE_ENOUGH_QTY',16,1)
							END
						END
					END
				END

		
				UPDATE @TempFinalTable SET vcSerialLot = @vcSerialLot WHERE ID=@i

				SET @i = @i + 1
			END
		END
	
		SET @i = 1
		SELECT @COUNT = COUNT(*) FROM @TempFinalTable
		DECLARE @vcDescription AS VARCHAR(300)
		DECLARE @numAllocation AS INT

		DECLARE @j INT = 0
		DECLARE @ChildCount AS INT
		DECLARE @vcChildDescription AS VARCHAR(300)
		DECLARE @numChildAllocation AS INT
		DECLARE @numOppChildItemID AS INT
		DECLARE @numChildItemCode AS INT
		DECLARE @numChildWarehouseItemID AS INT
		DECLARE @numChildQty AS INT
		DECLARE @numChildQtyShipped AS INT

		DECLARE @TempKitSubItems TABLE
		(
			ID INT IDENTITY(1,1),
			numOppChildItemID NUMERIC(18,0),
			numChildItemCode NUMERIC(18,0),
			numChildWarehouseItemID NUMERIC(18,0),
			numChildQty INT,
			numChildQtyShipped INT
		)

		WHILE @i <= @COUNT
		BEGIN
			SELECT 
				@vcItemType=vcItemType,
				@bitSerial=bitSerial,
				@bitLot=bitLot,
				@bitAssembly=bitAssembly,
				@bitKit=bitKit,
				@bitDropShip=bitDropShip,
				@numOppItemID=numOppItemID,
				@numQty=numQty,
				@numQtyShipped = numQtyShipped,
				@numWarehouseItemID=numWarehouseItemID,
				@vcSerialLot = vcSerialLot
			FROM 
				@TempFinalTable 
			WHERE 
				ID = @i

			--MAKE INVENTIRY CHANGES IF INVENTORY ITEM
			IF (UPPER(@vcItemType) = 'P' AND @bitDropShip = 0)
			BEGIN
				SET @vcDescription='SO Qty Shipped (Qty:' + CAST(@numQty AS VARCHAR(10)) + ' Shipped:' +  CAST(@numQtyShipped AS VARCHAR(10)) + ')'
						 
				IF @bitKit = 1
				BEGIN
					-- CLEAR DATA OF PREVIOUS ITERATION
					DELETE FROM @TempKitSubItems

					-- GET KIT SUB ITEMS DETAIL
					INSERT INTO @TempKitSubItems
					(
						numOppChildItemID,
						numChildItemCode,
						numChildWarehouseItemID,
						numChildQty,
						numChildQtyShipped
					)
					SELECT 
						ISNULL(numOppChildItemID,0),
						ISNULL(I.numItemCode,0),
						ISNULL(numWareHouseItemId,0),
						ISNULL(numQtyItemsReq,0),
						ISNULL(numQtyShipped,0)
					FROM 
						OpportunityKitItems OKI 
					JOIN 
						Item I 
					ON 
						OKI.numChildItemID=I.numItemCode    
					WHERE 
						charitemtype='P' 
						AND ISNULL(numWareHouseItemId,0) > 0 
						AND OKI.numOppID=@numOppID 
						AND OKI.numOppItemID=@numOppItemID 

					SET @j = 1
					SELECT @ChildCount=COUNT(*) FROM @TempKitSubItems

					--LOOP OVER ALL CHILD ITEMS AND RELESE ALLOCATION
					WHILE @j <= @ChildCount
					BEGIN
						SELECT
							@numOppChildItemID=numOppChildItemID,
							@numChildItemCode=numChildItemCode,
							@numChildWarehouseItemID=numChildWarehouseItemID,
							@numChildQty=numChildQty,
							@numChildQtyShipped=numChildQtyShipped
						FROM
							@TempKitSubItems
						WHERE 
							ID = @j

						SELECT @numChildAllocation=ISNULL(numAllocation,0) FROM WareHouseItems WHERE numWareHouseItemID=@numChildWarehouseItemID
						
						--REMOVE QTY FROM ALLOCATION
						SET @numChildAllocation = @numChildAllocation - @numChildQty
						IF @numChildAllocation >= 0
						BEGIN
							UPDATE  
								WareHouseItems
							SET     
								numAllocation = @numChildAllocation,
								dtModified = GETDATE() 
							WHERE   
								numWareHouseItemID = @numChildWarehouseItemID      	
							
							UPDATE  
								OpportunityKitItems
							SET 
								numQtyShipped = ISNULL(numQtyShipped,0) + ISNULL(@numChildQty,0)
							WHERE   
								numOppChildItemID = @numOppChildItemID 
								AND numOppId=@numOppId
								AND numOppItemID=@numOppItemID

							SET @vcChildDescription = 'SO KIT Qty Shipped (Qty:' + CAST(@numChildQty AS VARCHAR(10)) + ' Shipped:' +  CAST(@numChildQtyShipped AS VARCHAR(10)) + ')'

							EXEC dbo.USP_ManageWareHouseItems_Tracking
								@numWareHouseItemID = @numChildWarehouseItemID,
								@numReferenceID = @numOppId,
								@tintRefType = 3,
								@vcDescription = @vcChildDescription,
								@numModifiedBy = @numUserCntID,
								@numDomainID = @numDomainID	 
						END
						ELSE
						BEGIN
							RAISERROR ('NOTSUFFICIENTQTY_ALLOCATION',16,1);
						END

						SET @j = @j + 1
					END
				END
				ELSE
				BEGIN
					SELECT @numAllocation = ISNULL(numAllocation, 0) FROM WareHouseItems WHERE numWareHouseItemID = @numWareHouseItemID
				       
					--REMOVE QTY FROM ALLOCATION
					SET @numAllocation = @numAllocation - @numQty	
				       
					IF (@numAllocation >= 0 )
					BEGIN
						UPDATE  
							WareHouseItems
						SET     
							numAllocation = @numAllocation,
							dtModified = GETDATE() 
						WHERE   
							numWareHouseItemID = @numWareHouseItemID      	

						IF @bitSerial = 1
						BEGIN								
							DELETE FROM OppWarehouseSerializedItem WHERE numOppID=@numOppID AND numOppItemID=@numOppItemID AND numWarehouseItmsID=@numWarehouseItemID

							INSERT INTO OppWarehouseSerializedItem
							(
								numWarehouseItmsDTLID,
								numOppID,
								numOppItemID,
								numWarehouseItmsID,
								numQty,
								numOppBizDocsId	
							)
							SELECT
								(SELECT numWarehouseItmsDTLID FROM WareHouseItmsDTL WHERE numWareHouseItemID=@numWarehouseItemID AND vcSerialNo=OutParam),
								@numOppID,
								@numOppItemID,
								@numWarehouseItemID,
								1,
								@numOppBizDocID
							FROM
								dbo.SplitString(@vcSerialLot,',')


							UPDATE WareHouseItmsDTL SET numQty=0 WHERE numWareHouseItemID=@numWarehouseItemID AND vcSerialNo IN (SELECT OutParam FROM dbo.SplitString(@vcSerialLot,','))
						END

						IF @bitLot = 1
						BEGIN
							DELETE FROM @TempLot

							INSERT INTO
								@TempLot
							SELECT
								SUBSTRING(OutParam,0,PATINDEX('%(%', @vcSerialLot)),
								CAST(SUBSTRING(OutParam,(PATINDEX('%(%', @vcSerialLot) + 1),(PATINDEX('%)%', @vcSerialLot) - PATINDEX('%(%', @vcSerialLot) - 1)) AS INT)
							FROM
								dbo.SplitString(@vcSerialLot,',')

							DELETE FROM OppWarehouseSerializedItem WHERE numOppID=@numOppID AND numOppItemID=@numOppItemID AND numWarehouseItmsID=@numWarehouseItemID

							INSERT INTO OppWarehouseSerializedItem
							(
								numWarehouseItmsDTLID,
								numOppID,
								numOppItemID,
								numWarehouseItmsID,
								numQty,
								numOppBizDocsId	
							)
							SELECT
								(SELECT numWarehouseItmsDTLID FROM WareHouseItmsDTL WHERE numWareHouseItemID=@numWarehouseItemID AND vcSerialNo=TEMPLOT.vcLotNo),
								@numOppID,
								@numOppItemID,
								@numWarehouseItemID,
								TEMPLOT.numQty,
								@numOppBizDocID
							FROM
								@TempLot TEMPLOT


							UPDATE	TEMPWareHouseItmsDTL
								SET TEMPWareHouseItmsDTL.numQty = ISNULL(TEMPWareHouseItmsDTL.numQty,0) - ISNULL(TEMPLOT.numQty,0)
							FROM
								WareHouseItmsDTL TEMPWareHouseItmsDTL
							INNER JOIN
								@TempLot TEMPLOT
							ON
								TEMPWareHouseItmsDTL.vcSerialNo = TEMPLOT.vcLotNo
							WHERE
								TEMPWareHouseItmsDTL.numWareHouseItemID = @numWarehouseItemID
						END
					END
					ELSE
					BEGIN
						PRINT 'PARENT: ' + CAST(@numChildWarehouseItemID AS VARCHAR)
						RAISERROR ('NOTSUFFICIENTQTY_ALLOCATION',16,1);
					END
				END

				EXEC dbo.USP_ManageWareHouseItems_Tracking
							@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
							@numReferenceID = @numOppId, --  numeric(9, 0)
							@tintRefType = 3, --  tinyint
							@vcDescription = @vcDescription, --  varchar(100)
							@numModifiedBy = @numUserCntID,
							@numDomainID = @numDomainID	 
			END

			UPDATE  
				OpportunityItems
			SET 
				numQtyShipped = ISNULL(numQtyShipped,0) + ISNULL(@numQty,0)
			WHERE   
				numoppitemtCode = @numOppItemID 
				AND numOppId=@numOppId

			SET @i = @i + 1
		END

		UPDATE OpportunityBizDocs SET bitFulfilled = 1 WHERE numOppBizDocsId=@numOppBizDocID
	COMMIT TRANSACTION
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
END

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityBizDocs_RevertFulFillment')
DROP PROCEDURE USP_OpportunityBizDocs_RevertFulFillment
GO
CREATE PROCEDURE [dbo].[USP_OpportunityBizDocs_RevertFulFillment] 
	@numDomainID NUMERIC(18,0),
	@numUserCntID NUMERIC(18,0),
	@numOppId NUMERIC(18,0),
	@numOppBizDocID NUMERIC(18,0)
AS  
BEGIN 
BEGIN TRY

	DECLARE @vcItemType AS CHAR(1)
	DECLARE @bitDropShip AS BIT
	DECLARE @bitSerial AS BIT
	DECLARE @bitLot AS BIT
	DECLARE @bitAssembly AS BIT
	DECLARE @bitKit AS BIT
	DECLARE @numOppItemID AS INT
	DECLARE @numWarehouseItemID AS INT
	DECLARE @numQty AS INT
	DECLARE @numQtyShipped AS INT
	DECLARE @vcDescription AS VARCHAR(300)
	DECLARE @numAllocation AS INT


	DECLARE @TempKitSubItems TABLE
	(
		ID INT IDENTITY(1,1),
		numOppChildItemID NUMERIC(18,0),
		numChildItemCode NUMERIC(18,0),
		numChildWarehouseItemID NUMERIC(18,0),
		numChildQty INT,
		numChildQtyShipped INT
	)

	DECLARE @j INT = 0
	DECLARE @ChildCount AS INT
	DECLARE @vcChildDescription AS VARCHAR(300)
	DECLARE @numChildAllocation AS INT
	DECLARE @numOppChildItemID AS INT
	DECLARE @numChildItemCode AS INT
	DECLARE @numChildWarehouseItemID AS INT
	DECLARE @numChildQty AS INT
	DECLARE @numChildQtyShipped AS INT

	DECLARE @TempFinalTable TABLE
	(
		ID INT IDENTITY(1,1),
		numItemCode NUMERIC(18,0),
		vcItemType CHAR(1),
		bitSerial BIT,
		bitLot BIT,
		bitAssembly BIT,
		bitKit BIT,
		numOppItemID NUMERIC(18,0),
		numWarehouseItemID NUMERIC(18,0),
		numQtyShipped NUMERIC(18,0),
		numQty NUMERIC(18,0),
		bitDropShip BIT,
		vcSerialLot VARCHAR(MAX)
	)

	INSERT INTO @TempFinalTable
	(
		numItemCode,
		vcItemType,
		bitSerial,
		bitLot,
		bitAssembly,
		bitKit,
		numOppItemID,
		numWarehouseItemID,
		numQtyShipped,
		numQty,
		bitDropShip,
		vcSerialLot
	)
	SELECT
		OpportunityBizDocItems.numItemCode,
		ISNULL(Item.charItemType,''),
		ISNULL(Item.bitSerialized,0),
		ISNULL(Item.bitLotNo,0),
		ISNULL(Item.bitAssembly,0),
		ISNULL(Item.bitKitParent,0),
		ISNULL(OpportunityItems.numoppitemtcode,0),
		ISNULL(OpportunityBizDocItems.numWarehouseItmsID,0),
		ISNULL(OpportunityItems.numQtyShipped,0),
		ISNULL(OpportunityBizDocItems.numUnitHour,0),
		ISNULL(OpportunityItems.bitDropShip,0),
		''
	FROM
		OpportunityBizDocs
	INNER JOIN
		OpportunityBizDocItems
	ON
		OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
	INNER JOIN
		OpportunityItems
	ON
		OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtCode
	INNER JOIN
		Item
	ON
		OpportunityBizDocItems.numItemCode = Item.numItemCode
	WHERE
		numOppBizDocsId = @numOppBizDocID
		AND ISNULL(OpportunityItems.bitDropShip,0) = 0

	BEGIN TRANSACTION
		DECLARE @i INT = 1
		DECLARE @COUNT INT = 0
		SELECT @COUNT = COUNT(*) FROM @TempFinalTable

		WHILE @i <= @COUNT
		BEGIN
			SELECT 
				@vcItemType=vcItemType,
				@bitSerial=bitSerial,
				@bitLot=bitLot,
				@bitAssembly=bitAssembly,
				@bitKit=bitKit,
				@bitDropShip=bitDropShip,
				@numOppItemID=numOppItemID,
				@numQty=numQty,
				@numQtyShipped = numQtyShipped,
				@numWarehouseItemID=numWarehouseItemID
			FROM 
				@TempFinalTable 
			WHERE 
				ID = @i

			IF @numQty > @numQtyShipped
			BEGIN
				RAISERROR ('INVALID_SHIPPED_QTY',16,1);
			END

			--REVERT QUANTITY TO ON ALLOCATION IF INVENTORY ITEM
			IF (UPPER(@vcItemType) = 'P' AND @bitDropShip = 0)
			BEGIN
				SET @vcDescription='SO Qty Shipped Return (Qty:' + CAST(@numQty AS VARCHAR(10)) + ' Shipped:' +  CAST(@numQtyShipped AS VARCHAR(10)) + ')'

				IF @bitKit = 1
				BEGIN
					-- CLEAR DATA OF PREVIOUS ITERATION
					DELETE FROM @TempKitSubItems

					-- GET KIT SUB ITEMS DETAIL
					INSERT INTO @TempKitSubItems
					(
						numOppChildItemID,
						numChildItemCode,
						numChildWarehouseItemID,
						numChildQty,
						numChildQtyShipped
					)
					SELECT 
						ISNULL(numOppChildItemID,0),
						ISNULL(I.numItemCode,0),
						ISNULL(OKI.numWareHouseItemId,0),
						ISNULL(OKI.numQtyItemsReq,0),
						ISNULL(OKI.numQtyShipped,0)
					FROM 
						OpportunityKitItems OKI 
					JOIN 
						Item I 
					ON 
						OKI.numChildItemID=I.numItemCode    
					WHERE 
						charitemtype='P' 
						AND ISNULL(numWareHouseItemId,0) > 0 
						AND OKI.numOppID=@numOppID 
						AND OKI.numOppItemID=@numOppItemID 

					SET @j = 1
					SELECT @ChildCount=COUNT(*) FROM @TempKitSubItems

					--LOOP OVER ALL CHILD ITEMS AND REVERT ALLOCATION
					WHILE @j <= @ChildCount
					BEGIN
						SELECT
							@numOppChildItemID=numOppChildItemID,
							@numChildItemCode=numChildItemCode,
							@numChildWarehouseItemID=numChildWarehouseItemID,
							@numChildQty=numChildQty,
							@numChildQtyShipped=numChildQtyShipped
						FROM
							@TempKitSubItems
						WHERE 
							ID = @j

						IF @numChildQty > @numChildQtyShipped
						BEGIN
							RAISERROR ('INVALID_KIT_SUB_ITEM_SHIPPED_QTY',16,1);
						END

						SELECT @numChildAllocation=ISNULL(numAllocation,0) FROM WareHouseItems WHERE numWareHouseItemID=@numChildWarehouseItemID

						--PLACE QTY BACK ON ALLOCATION
						SET @numChildAllocation = @numChildAllocation + @numChildQty
						
						-- UPDATE WAREHOUSE INVENTORY
						UPDATE  
							WareHouseItems
						SET     
							numAllocation = @numChildAllocation,
							dtModified = GETDATE() 
						WHERE   
							numWareHouseItemID = @numChildWarehouseItemID      	
						
						--DECREASE QTY SHIPPED OF ORDER ITEM	
						UPDATE  
							OpportunityKitItems
						SET 
							numQtyShipped = ISNULL(numQtyShipped,0) - ISNULL(@numChildQty,0)
						WHERE   
							numOppChildItemID = @numOppChildItemID 
							AND numOppId=@numOppId
							AND numOppItemID=@numOppItemID

						SET @vcChildDescription = 'SO KIT Qty Shipped Return (Qty:' + CAST(@numChildQty AS VARCHAR(10)) + ' Shipped:' +  CAST(@numChildQtyShipped AS VARCHAR(10)) + ')'

						-- CREATE WAREHOUSE INVENTORY TRACKING ENTRY
						EXEC dbo.USP_ManageWareHouseItems_Tracking
							@numWareHouseItemID = @numChildWarehouseItemID,
							@numReferenceID = @numOppId,
							@tintRefType = 3,
							@vcDescription = @vcChildDescription,
							@numModifiedBy = @numUserCntID,
							@numDomainID = @numDomainID	 


						SET @j = @j + 1
					END
				END
				ELSE
				BEGIN
					SELECT @numAllocation = ISNULL(numAllocation, 0) FROM WareHouseItems WHERE numWareHouseItemID = @numWareHouseItemID
				       
					--PLACE QTY BACK ON ALLOCATION
					SET @numAllocation = @numAllocation + @numQty	

					-- UPDATE WAREHOUSE INVENTORY
					UPDATE  
						WareHouseItems
					SET     
						numAllocation = @numAllocation
						,dtModified = GETDATE() 
					WHERE   
						numWareHouseItemID = @numWareHouseItemID      	

					IF @bitSerial = 1
					BEGIN 
						--INCREASE SERIAL NUMBER
						UPDATE 
							WareHouseItmsDTL 
						SET 
							numQty=1 
						WHERE 
							numWareHouseItmsDTLID IN (
								SELECT 
									ISNULL(OWSI.numWareHouseItmsDTLID ,0)
								FROM 
									OppWarehouseSerializedItem OWSI
								WHERE 
									OWSI.numOppID = @numOppID
									AND OWSI.numOppItemID =  @numOppItemID
									AND OWSI.numWarehouseItmsID = @numWarehouseItemID
									AND OWSI.numOppBizDocsId = @numOppBizDocID
							)

						-- DELETE SERIAL NUMBER ENTERIES ASSOCIATED WITH BIZDOC
						DELETE FROM OppWarehouseSerializedItem WHERE numOppID=@numOppID AND numOppItemID= @numOppItemID AND numWarehouseItmsID=@numWarehouseItemID AND numOppBizDocsId=@numOppBizDocID		
					END

					IF @bitLot = 1
					BEGIN
						-- INCREASE LOT NOT QTY
						UPDATE WHDL
							SET WHDL.numQty = ISNULL(WHDL.numQty,0) + ISNULL(OWSI.numQty,0)
						FROM
							WareHouseItmsDTL  WHDL
						INNER JOIN
							OppWarehouseSerializedItem OWSI
						ON
							WHDL.numWareHouseItmsDTLID = OWSI.numWarehouseItmsDTLID
							AND OWSI.numOppID = @numOppID
							AND OWSI.numOppItemID =  @numOppItemID
							AND OWSI.numWarehouseItmsID = @numWarehouseItemID
							AND OWSI.numOppBizDocsId = @numOppBizDocID

						-- DELETE SERIAL NUMBER ENTERIES ASSOCIATED WITH BIZDOC
						DELETE FROM OppWarehouseSerializedItem WHERE numOppID=@numOppID AND numOppItemID= @numOppItemID AND numWarehouseItmsID=@numWarehouseItemID AND numOppBizDocsId=@numOppBizDocID
					END
				END

				-- CREATE WAREHOUSE INVENTORY TRACKING ENTRY
				EXEC dbo.USP_ManageWareHouseItems_Tracking
							@numWareHouseItemID = @numWareHouseItemID,
							@numReferenceID = @numOppId,
							@tintRefType = 3,
							@vcDescription = @vcDescription,
							@numModifiedBy = @numUserCntID,
							@numDomainID = @numDomainID	 
				
			END

			--DECREASE QTY SHIPPED OF ORDER ITEM	
			UPDATE 
				OpportunityItems
			SET     
				numQtyShipped = (ISNULL(numQtyShipped,0) - @numQty)
			WHERE   
				numoppitemtCode = @numOppItemID 
				AND numOppId=@numOppId

			SET @i = @i + 1
		END
	COMMIT TRANSACTION 
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRAN

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
END
/****** Object:  StoredProcedure [dbo].[USP_OppSaveMilestone]    Script Date: 07/26/2008 16:20:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                                            
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_oppsavemilestone')
DROP PROCEDURE usp_oppsavemilestone
GO
CREATE PROCEDURE [dbo].[USP_OppSaveMilestone]                                              
(                                              
 @numOppID as numeric(9)=null,                                              
 @strMilestone as text='' ,                        
 @numUserCntID as numeric(9)=0                                           
)                                              
as                                              
declare @status as tinyint                                              
declare @hDoc3  int                                              
declare @divId as numeric(9)                                              
declare @ConId as numeric(9)                                              
declare @tintOpptype as tinyint                                              
declare @numItemCode as numeric(9)                                              
declare @numOppItemCode as numeric(9)                                              
declare @numUnit as numeric(9)                                              
declare @numQtyOnHand as numeric(9)                                          
                                          
                                            
                                            
if convert(varchar(10),@strMilestone) <>''                                           
 begin                                              
  EXEC sp_xml_preparedocument @hDoc3 OUTPUT, @strMilestone  


   delete from OpportunityStageDetails                                               
   where numOppStageID not in (SELECT StageID FROM OPENXML(@hDoc3,'/NewDataSet/Table',2) with(StageID numeric,Op_Flag numeric))                                             
      and numOppID=  @numOppID                                             
                                                
    update OpportunityStageDetails set                                               
     vcstageDetail=X.vcstageDetail,                                              
     numModifiedBy=X.numModifiedBy,                                    
     bintModifiedDate=GETUTCDATE(),
     bintDueDate=CONVERT(DATETIME,replace(X.bintDueDate,'T',' ')),
     bintStageComDate= CONVERT(DATETIME,REPLACE(X.bintStageComDate,'T',' ')),
     vcComments=X.vcComments,                                              
     numAssignTo=X.numAssignTo,                                              
     bitAlert=X.bitAlert,                                               
     bitStageCompleted=X.bitStageCompleted,numStage=X.numStage ,          
  vcStagePercentageDtl =x.vcStagePercentageDtl, numTemplateId =  x.numTemplateId,          
  tintPercentage=X.tintPercentage  ,      
 numEvent=X.numEvent,      
 numReminder=X.numReminder,      
 numET=X.numET,      
 numActivity =X.numActivity ,      
 numStartDate=X.numStartDate,      
 numStartTime=X.numStartTime,      
 numStartTimePeriod=X.numStartTimePeriod,      
 numEndTime=X.numEndTime,      
 numEndTimePeriod=X.numEndTimePeriod,      
 txtCom=X.txtCom     
 ,numChgStatus =x.numChgStatus ,bitChgStatus=x.bitChgStatus    
,bitClose =x.bitClose,numType =x.numType  ,numCommActId= x.numCommActId  
      From (SELECT *                                              
     FROM OPENXML(@hDoc3,'/NewDataSet/Table[StageID>0][Op_Flag=0]',2)                                  
      with(StageID numeric,                                              
      vcstageDetail varchar(100),                                              
      numModifiedBy numeric,                                   
      bintModifiedDate VARCHAR(23),                                              
      bintDueDate VARCHAR(23),
      bintStageComDate VARCHAR(23),
      vcComments varchar(1000),                                    
      numAssignTo numeric,                                              
      bitAlert bit,                                              
      bitStageCompleted bit,                                              
      Op_Flag numeric,                     
      numStage numeric,            
    vcStagePercentageDtl varchar(500),            
    numTemplateId numeric,          
  tintPercentage tinyint,      
  numEvent tinyint,      
  numReminder numeric,      
  numET numeric,      
  numActivity numeric ,      
  numStartDate tinyint,      
  numStartTime tinyint,      
  numStartTimePeriod tinyint ,      
  numEndTime tinyint ,      
  numEndTimePeriod tinyint,       
  txtCom varchar(1000) ,numChgStatus numeric(9),bitChgStatus bit ,bitClose bit,numType numeric(9),numCommActId numeric(9) ))X                                               
    where  numOppStageID=X.StageID                                              
                                                
                                                
   insert into OpportunityStageDetails                                              
    (numOppId,vcStageDetail,numStagePercentage,numDomainId,                                        
    numCreatedBy,bintCreatedDate,numModifiedBy,bintModifiedDate,                                              
    bintDueDate,bintStageComDate,vcComments,numAssignTo,bitAlert,                                              
    bitStageCompleted,numStage,vcStagePercentageDtl,numTemplateId,numEvent,numReminder,numET,numActivity ,numStartDate,numStartTime,numStartTimePeriod ,numEndTime ,      
numEndTimePeriod, txtCom ,numChgStatus ,bitChgStatus  ,bitClose,numType,numCommActId,tintPercentage )                                              
                                                 
   select @numOppID,                                              
    X.vcstageDetail ,                                              
    X.numStagePercentage ,                              
    X.numDomainId,                                              
    X.numCreatedBy ,                                              
    GETUTCDATE(),--replace(X.bintCreatedDate,'T',' ') ,                                              
	X.numModifiedBy ,                                              
    GETUTCDATE(),--replace(X.bintModifiedDate,'T',' ') ,                                              
    replace(X.bintDueDate,'T',' ') ,
    replace(X.bintStageComDate,'T',' ') ,
    X.vcComments ,                                              
    X.numAssignTo,                                    
    X.bitAlert ,                                              
    X.bitStageCompleted,                                        
    X.numStage,x.vcStagePercentageDtl,x.numTemplateId,X.numEvent,      
 X.numReminder,      
 X.numET,      
 X.numActivity ,      
 X.numStartDate,      
 X.numStartTime,      
 X.numStartTimePeriod ,      
 X.numEndTime ,      
 X.numEndTimePeriod,       
 X.txtCom  ,x.numChgStatus ,x.bitChgStatus  ,x.bitClose ,x.numType,x.numCommActId,x.tintPercentage from(SELECT *FROM OPENXML (@hDoc3,'/NewDataSet/Table[StageID=0][Op_Flag=0]',2)                                              
   WITH  (StageID numeric,                                              
    vcstageDetail varchar(100),                                              
    numStagePercentage numeric,                                              
    numDomainId numeric,                                              
    numCreatedBy numeric,                                              
    bintCreatedDate VARCHAR(23),                                              
    numModifiedBy numeric,                                              
    bintModifiedDate VARCHAR(23),                                              
    bintDueDate VARCHAR(23),
    bintStageComDate VARCHAR(23),                                              
    vcComments varchar(1000),                                              
    numAssignTo numeric,                                              
    bitAlert bit,                                              
    bitStageCompleted bit,                                              
    Op_Flag numeric,                                        
    numStage numeric ,            
 vcStagePercentageDtl varchar(500),            
    numTemplateId numeric ,      
 numEvent tinyint,      
 numReminder numeric,      
 numET numeric,      
 numActivity numeric ,      
 numStartDate tinyint,      
 numStartTime tinyint,      
 numStartTimePeriod tinyint ,      
 numEndTime tinyint ,      
 numEndTimePeriod tinyint,       
 txtCom varchar(1000)  ,numChgStatus numeric(9),bitChgStatus bit ,bitClose bit ,numType numeric(9),numCommActId numeric(9),tintPercentage tinyint
    ))X 

         
                                        
                                                
                                                
                                                
                                                
   EXEC sp_xml_removedocument @hDoc3                                              
                                              
   end                                          
         
                                            
declare @numDivisionID as numeric(9)        
declare @tintCRMType as numeric(9)        
declare @AccountClosingDate as datetime        
                                        
if exists(select * from OpportunityStageDetails where numOppId=@numOppID and   numStagePercentage=100 and bitStageCompleted=1)              
begin           
 select @numDivisionID=numDivisionID,@AccountClosingDate=bintAccountClosingDate from  OpportunityMaster where numOppID=@numOppID         
 select @tintCRMType=tintCRMType from divisionmaster where numDivisionID =@numDivisionID         
           
 if @AccountClosingDate is null                   
  update OpportunityMaster set tintOppStatus=1,bintAccountClosingDate=getutcdate() where numOppID=@numOppID              
    else         
  update OpportunityMaster set tintOppStatus=1 where numOppID=@numOppID         
 if @tintCRMType=0        
 begin        
  update divisionmaster set tintCRMType=2,bintLeadProm=getutcdate(),bintLeadPromBy=@numUserCntID,bintProsProm=getutcdate(),bintProsPromBy=@numUserCntID        
  where numDivisionID=@numDivisionID        
 end        
 else if @tintCRMType=1        
 begin        
  update divisionmaster set tintCRMType=2,bintProsProm=getutcdate(),bintProsPromBy=@numUserCntID        
  where numDivisionID=@numDivisionID        
 end        
        
end         
else if exists(select * from OpportunityStageDetails where numOppId=@numOppID and   numStagePercentage=0 and bitStageCompleted=1)              
begin         
 select @numDivisionID=numDivisionID,@AccountClosingDate=bintAccountClosingDate from  OpportunityMaster where numOppID=@numOppID         
 select @tintCRMType=tintCRMType from divisionmaster where numDivisionID =@numDivisionID         
 if @AccountClosingDate is null                   
  update OpportunityMaster set tintOppStatus=2,bintAccountClosingDate=getutcdate() where numOppID=@numOppID              
    else         
  update OpportunityMaster set tintOppStatus=2 where numOppID=@numOppID         
end                    
                            
 select numoppitemtCode from OpportunityItems where numOppid= @numOppID                      
              
              
--Updating the warehouse items              
  declare @tintOppStatus as tinyint               
  declare @tintShipped as tinyint               
  select @tintOppStatus=tintOppStatus,@tintOppType=tintOppType,@tintShipped=tintShipped from OpportunityMaster where numOppID=@numOppID              
  if @tintOppStatus=1               
        begin              
   exec USP_UpdatingInventoryonCloseDeal @numOppID              
                 
  end              

GO
/****** Object:  StoredProcedure [dbo].[USP_OppShippingorReceiving]    Script Date: 07/26/2008 16:20:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- created by anoop jayaraj            
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_oppshippingorreceiving')
DROP PROCEDURE usp_oppshippingorreceiving
GO
CREATE PROCEDURE [dbo].[USP_OppShippingorReceiving]            
@OppID as numeric(9),
@numUserCntID AS NUMERIC(9)          
as
BEGIN TRY
	
	DECLARE @numDomain AS NUMERIC(18,0)
	SELECT @numDomain = OM.numDomainID FROM [dbo].[OpportunityMaster] AS OM WHERE [OM].[numOppId] = @OppID
	PRINT @numDomain
	-- Added by Manish Anjara : 17th Sep,2014
	-- Check whether order is already closed or not. If closed then show a warning message
	IF (SELECT ISNULL(tintshipped,0) FROM [dbo].[OpportunityMaster] WHERE [OpportunityMaster].[numOppId] = @OppID AND [OpportunityMaster].[numDomainId] = @numDomain) = 1
	BEGIN
		RAISERROR('ORDER_CLOSED', 16, 1)
	END		

   BEGIN TRANSACTION 
			
			update OpportunityMaster set tintshipped=1,bintAccountClosingDate=GETUTCDATE(), bintClosedDate=GETUTCDATE() where  numOppId=@OppID 
			UPDATE [OpportunityBizDocs] SET [dtShippedDate] = GETUTCDATE() WHERE [numOppId]=@OppID
			
						
			declare @status as varchar(2)            
			declare @OppType as varchar(2)   
			declare @bitStockTransfer as BIT
			DECLARE @fltExchangeRate AS FLOAT 
         
			select @status=tintOppStatus,@OppType=tintOppType,@bitStockTransfer=ISNULL(bitStockTransfer,0),@fltExchangeRate=(CASE WHEN ISNULL(fltExchangeRate,0)=0 THEN 1 ELSE fltExchangeRate END) from OpportunityMaster
			where numOppId=@OppID
			
			-- Archive item based on Item's individual setting
			IF @OppType = 1
			BEGIN
				UPDATE Item SET IsArchieve = CASE WHEN EXISTS(SELECT OpportunityItems.numItemCode FROM dbo.OpportunityItems WHERE OpportunityItems.numItemCode = I.numItemCode) 
												  THEN 1
												  ELSE 0 
											 END 
				FROM Item I JOIN dbo.OpportunityItems OI ON I.numItemCode = OI.numItemCode  
				WHERE numOppId = @OppID 
				AND I.[numDomainID] = @numDomain

				AND ISNULL(I.bitArchiveItem,0) = 1 
			END
			----------------------------------------------------------------------------            
			--Updating Item Details            
			-----------------------------------------------------------------------            
            
			---Updating the inventory Items once the deal is won
			declare @numoppitemtCode as numeric
			declare @numUnits as numeric
			declare @numWarehouseItemID as numeric       
			declare @numToWarehouseItemID as numeric(9) --to be used with stock transfer
			declare @itemcode as numeric        
			declare @QtyShipped as NUMERIC
			declare @QtyReceived as numeric
			Declare @monPrice as money
			declare @Kit as bit
			declare @bitWorkOrder AS BIT
			declare @bitSerialized AS BIT
			declare @bitLotNo AS BIT
    
			 select top 1 @numoppitemtCode=numoppitemtCode,@itemcode=OI.numItemCode,@numUnits=ISNULL(numUnitHour,0),@qtyShipped=ISNULL([numQtyShipped],0),@qtyReceived=ISNULL([numUnitHourReceived],0),
			 @numWareHouseItemID=ISNULL(numWarehouseItmsID,0),@numToWarehouseItemID=numToWarehouseItemID,@monPrice=isnull(monPrice,0) * @fltExchangeRate,@Kit= (case when bitKitParent=1 and bitAssembly=1 then 0 when bitKitParent=1 then 1 else 0 end),
			 @bitWorkOrder = ISNULL(bitWorkOrder,0),@bitSerialized=ISNULL(bitSerialized,0),@bitLotNo=ISNULL(bitLotNo,0)
			 from OpportunityItems OI join Item I                                                
			 on OI.numItemCode=I.numItemCode and numOppId=@OppID and (bitDropShip=0 or bitDropShip is null)                                            
			 where (charitemtype='P' OR 1=(CASE WHEN @OppType=1 THEN 
							CASE WHEN ISNULL(I.bitKitParent,0) = 1 AND ISNULL(I.bitAssembly,0) = 1 THEN 0
								 WHEN ISNULL(I.bitKitParent,0) = 1 THEN 1
								 ELSE 0 END 
							ELSE 0 END))  
			 AND I.[numDomainID] = @numDomain	
							order by OI.numoppitemtCode      

			 while @numoppitemtCode>0                
			 begin   
 
--			  if @Kit = 1
--			  begin
--					exec USP_UpdateKitItems @numoppitemtCode,@numUnits,@OppType,2,@OppID
--			  end
             
             
				IF @bitStockTransfer = 1
				BEGIN
					--Make inventory changes as if sales order was closed , using OpportunityItems.numWarehouseItmsID
					EXEC usp_ManageInventory @itemcode,@numWareHouseItemID,0,1,@numUnits,@qtyShipped,@qtyReceived,@monPrice,2,@OppID,@numoppitemtCode,@numUserCntID,@bitWorkOrder
					--Make inventory changes as if purchase order was closed, using OpportunityItems.numToWarehouseItemID
					EXEC usp_ManageInventory @itemcode,@numToWarehouseItemID,0,2,@numUnits,@qtyShipped,@qtyReceived,@monPrice,2,@OppID,@numoppitemtCode,@numUserCntID,@bitWorkOrder
					If @bitSerialized = 1 OR @bitLotNo = 1
					BEGIN
						--Transfer Serial/Lot Numbers
						EXEC USP_WareHouseItmsDTL_TransferSerialLotNo @OppID,@numoppitemtCode,@numWareHouseItemID,@numToWarehouseItemID,@bitSerialized,@bitLotNo
					END
				END
				ELSE
				BEGIN
					EXEC usp_ManageInventory @itemcode,@numWareHouseItemID,0,@OppType,@numUnits,@qtyShipped,@qtyReceived,@monPrice,2,@OppID,@numoppitemtCode,@numUserCntID,@bitWorkOrder
				END
        
			  select top 1 @numoppitemtCode=numoppitemtCode,@itemcode=OI.numItemCode,@numUnits=ISNULL(numUnitHour,0),@qtyShipped=ISNULL([numQtyShipped],0),@qtyReceived=ISNULL([numUnitHourReceived],0),
			 @numWareHouseItemID=ISNULL(numWarehouseItmsID,0),@numToWarehouseItemID=numToWarehouseItemID,@monPrice=isnull(monPrice,0) * @fltExchangeRate,@Kit= (case when bitKitParent=1 and bitAssembly=1 then 0 when bitKitParent=1 then 1 else 0 end),
			 @bitWorkOrder = ISNULL(bitWorkOrder,0),@bitSerialized=ISNULL(bitSerialized,0),@bitLotNo=ISNULL(bitLotNo,0)
			  from OpportunityItems OI
			  join Item I
			  on OI.numItemCode=I.numItemCode and numOppId=@OppID                                              
			  where (charitemtype='P' OR 1=(CASE WHEN @OppType=1 THEN 
							CASE WHEN ISNULL(I.bitKitParent,0) = 1 AND ISNULL(I.bitAssembly,0) = 1 THEN 0
								 WHEN ISNULL(I.bitKitParent,0) = 1 THEN 1
								 ELSE 0 END 
							ELSE 0 END)) and OI.numoppitemtCode>@numoppitemtCode and (bitDropShip=0 or bitDropShip is null) 
			  AND I.numDomainID = @numDomain
			  ORDER by OI.numoppitemtCode                                                
			  if @@rowcount=0 set @numoppitemtCode=0     
  
			 END
 COMMIT
END TRY
BEGIN CATCH
  -- Whoops, there was an error
  IF @@TRANCOUNT > 0
     ROLLBACK

  -- Raise an error with the details of the exception
  DECLARE @ErrMsg nvarchar(4000), @ErrSeverity int
  SELECT @ErrMsg = ERROR_MESSAGE(),
         @ErrSeverity = ERROR_SEVERITY()

  RAISERROR(@ErrMsg, @ErrSeverity, 1)
END CATCH

GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OppWarehouseSerializedItem_Insert')
DROP PROCEDURE USP_OppWarehouseSerializedItem_Insert
GO
CREATE PROCEDURE [dbo].[USP_OppWarehouseSerializedItem_Insert]
	@numOppID AS NUMERIC(18,0),
	@numOppItemID AS NUMERIC(18,0),
	@numWarehouseItemID AS NUMERIC(18,0),
	@vcSelectedItems AS VARCHAR(MAX),
	@tintItemType AS TINYINT -- 1 = Serial, 2 = Lot
AS
BEGIN
SET NOCOUNT ON;
BEGIN TRY
BEGIN TRANSACTION	

	IF @tintItemType = 1 OR @tintItemType = 2
	BEGIN

		DECLARE @TEMPSERIALLOT TABLE
		(
			ID INT IDENTITY(1,1),
			numWareHouseItmsDTLID NUMERIC(18,0),
			numQty INT
		)

		UPDATE 
			WIDL
		SET
			WIDL.numQty = (CASE WHEN @tintItemType = 2 THEN (ISNULL(WIDL.numQty,0) + ISNULL(OWSI.numQty,0)) ELSE 1 END)
		FROM 
			WarehouseItmsDTL WIDL
		INNER JOIN
			OppWarehouseSerializedItem OWSI
		ON
			WIDL.numWareHouseItmsDTLID = OWSI.numWarehouseItmsDTLID
			AND WIDL.numWareHouseItemID = OWSI.numWarehouseItmsID
		WHERE 
			OWSI.numOppID=@numOppID 
			AND OWSI.numOppItemID=@numOppItemID 
			AND OWSI.numWarehouseItmsID=@numWarehouseItemID


		DELETE FROM OppWarehouseSerializedItem WHERE numOppID=@numOppID AND numOppItemID=@numOppItemID AND numWarehouseItmsID=@numWarehouseItemID

		IF LEN(@vcSelectedItems) > 0
		BEGIN
			INSERT INTO 
				@TEMPSERIALLOT
			SELECT 
				PARSENAME(items,2),
				PARSENAME(items,1) 
			FROM 
				dbo.Split(Replace(@vcSelectedItems, '-', '.'),',')


			DECLARE @i AS INT = 1
			DECLARE @COUNT AS INT = 0
			DECLARE @numWareHouseItmsDTLID NUMERIC(18,0)
			DECLARE @numQty INT

			SELECT @COUNT = COUNT(*) FROM @TEMPSERIALLOT

			WHILE @i <= @COUNT
			BEGIN
				SELECT @numWareHouseItmsDTLID=numWareHouseItmsDTLID,@numQty=numQty FROM @TEMPSERIALLOT WHERE ID = @i

				UPDATE WareHouseItmsDTL SET numQty = (CASE WHEN @tintItemType = 2 THEN (ISNULL(numQty,0) - ISNULL(@numQty,0)) ELSE 0 END) WHERE numWareHouseItmsDTLID=@numWareHouseItmsDTLID

				INSERT INTO OppWarehouseSerializedItem
				(
					numWarehouseItmsDTLID,
					numOppID,
					numOppItemID,
					numWarehouseItmsID,
					numQty
				)
				SELECT 
					@numWareHouseItmsDTLID,
					@numOppID,
					@numOppItemID,
					@numWarehouseItemID,
					@numQty 

				SET @i = @i + 1
			END
		END
	END
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
END







/****** Object:  StoredProcedure [dbo].[USP_ReOpenOppertunity]    Script Date: 07/26/2008 16:20:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReOpenOppertunity')
DROP PROCEDURE USP_ReOpenOppertunity
GO
CREATE PROCEDURE [dbo].[USP_ReOpenOppertunity]            
@OppID as numeric(9),
@numUserCntID AS NUMERIC(9),
@numDomainID AS NUMERIC(9)
          
as          
BEGIN TRY
		-- Added by Manish Anjara : 17th Sep,2014
	-- Check whether order is already opened or not. If already opened then show a warning message
	IF (SELECT ISNULL(tintshipped,0) FROM [dbo].[OpportunityMaster] WHERE [OpportunityMaster].[numOppId] = @OppID AND [OpportunityMaster].[numDomainId] = @numDomainID) = 0
	BEGIN
		RAISERROR('ORDER_OPENED', 16, 1)
	END

   BEGIN TRANSACTION       
			declare @status as varchar(2)            
			declare @OppType as varchar(2)  
			DECLARE @fltExchangeRate AS FLOAT 
			          
			select @status=tintOppStatus,@OppType=tintOppType,@fltExchangeRate=(CASE WHEN ISNULL(fltExchangeRate,0)=0 THEN 1 ELSE fltExchangeRate END) from OpportunityMaster            
			where numOppId=@OppID AND [numDomainId] = @numDomainID

			update OpportunityMaster set tintshipped=0,bintAccountClosingDate=NULL,bintClosedDate=NULL where  numOppId=@OppID  AND [numDomainId] = @numDomainID
			UPDATE [OpportunityBizDocs] SET [dtShippedDate] = NULL WHERE [numOppId]=@OppID
        			
			-- WE ARE REVERTING RECEIVED (PURCHASED) QTY ONLY BECAUSE USER HAS TO DELETE FULFILLMENT ORDER TO REVERSE SHIPPED (SALED) QTY
			IF @OppType = 2
			BEGIN
				--SET Received qty to 0	
				UPDATE OpportunityItems SET numUnitHourReceived=0,numQtyShipped=0 WHERE [numOppId]=@OppID
				UPDATE OpportunityKitItems SET numQtyShipped=0 WHERE [numOppId]=@OppID
			END
			
			-- UnArchive item based on Item's individual setting
			IF @OppType = 1
			BEGIN
				UPDATE Item SET IsArchieve = CASE WHEN EXISTS(SELECT OpportunityItems.numItemCode FROM dbo.OpportunityItems 
															  WHERE OpportunityItems.numItemCode = I.numItemCode
															  AND OpportunityItems.numoppitemtCode = OI.numoppitemtCode) 
												  THEN 0
												  ELSE 1 
											 END 
				FROM Item I JOIN dbo.OpportunityItems OI ON I.numItemCode = OI.numItemCode  
				WHERE numOppId = @OppID 
				AND I.[numDomainId] = @numDomainID
				AND ISNULL(I.bitArchiveItem,0) = 1 
			END     
			----------------------------------------------------------------------------            
			--Updating Item Details            
			-----------------------------------------------------------------------            
            
			---Updating the inventory Items once the deal is won              
			declare @numoppitemtCode as numeric            
			declare @numUnits as numeric              
			declare @numWarehouseItemID as numeric       
			declare @itemcode as numeric        
			declare @QtyShipped as NUMERIC
			declare @QtyReceived as numeric
			Declare @monPrice as money  
			declare @Kit as bit  
			declare @bitWorkOrder AS BIT
    
			 select top 1 @numoppitemtCode=numoppitemtCode,@itemcode=OI.numItemCode,@numUnits=ISNULL(numUnitHour,0),@qtyShipped=ISNULL([numQtyShipped],0),@qtyReceived=ISNULL([numUnitHourReceived],0),
			 @numWareHouseItemID=ISNULL(numWarehouseItmsID,0),@monPrice=isnull(monPrice,0) * @fltExchangeRate,@Kit= (case when bitKitParent=1 and bitAssembly=1 then 0 when bitKitParent=1 then 1 else 0 end),
			 @bitWorkOrder = bitWorkOrder
			 from OpportunityItems OI join Item I                                                
			 on OI.numItemCode=I.numItemCode and numOppId=@OppID and (bitDropShip=0 or bitDropShip is null)                                            
			 where (charitemtype='P' OR 1=(CASE WHEN @OppType=1 THEN 
							CASE WHEN ISNULL(I.bitKitParent,0) = 1 AND ISNULL(I.bitAssembly,0) = 1 THEN 0
								 WHEN ISNULL(I.bitKitParent,0) = 1 THEN 1
								 ELSE 0 END 
							ELSE 0 END))  
			 AND I.[numDomainId] = @numDomainID
			order by OI.numoppitemtCode      
            
			 while @numoppitemtCode>0                
			 begin   
 
				EXEC usp_ManageInventory @itemcode,@numWareHouseItemID,0,@OppType,@numUnits,@qtyShipped,@qtyReceived,@monPrice,3,@OppID,@numoppitemtCode,@numUserCntID,@bitWorkOrder
        
			  select top 1 @numoppitemtCode=numoppitemtCode,@itemcode=OI.numItemCode,@numUnits=ISNULL(numUnitHour,0),@qtyShipped=ISNULL([numQtyShipped],0),@qtyReceived=ISNULL([numUnitHourReceived],0),
			  @numWareHouseItemID=ISNULL(numWarehouseItmsID,0),@monPrice=isnull(monPrice,0) * @fltExchangeRate,@Kit= (case when bitKitParent=1 and bitAssembly=1 then 0 when bitKitParent=1 then 1 else 0 end) 
			  from OpportunityItems OI                                                
			  join Item I                                                
			  on OI.numItemCode=I.numItemCode and numOppId=@OppID                                              
			  where (charitemtype='P' OR 1=(CASE WHEN @OppType=1 THEN 
							CASE WHEN ISNULL(I.bitKitParent,0) = 1 AND ISNULL(I.bitAssembly,0) = 1 THEN 0
								 WHEN ISNULL(I.bitKitParent,0) = 1 THEN 1
								 ELSE 0 END 
							ELSE 0 END))
					and OI.numoppitemtCode>@numoppitemtCode and (bitDropShip=0 or bitDropShip is null) 
					AND I.[numDomainId] = @numDomainID
					order by OI.numoppitemtCode                                                
			  if @@rowcount=0 set @numoppitemtCode=0     
  
			 END
				

			IF @OppType=2
			BEGIN
				IF (SELECT
						COUNT(*)
					FROM
						OppWarehouseSerializedItem OWSI
					INNER JOIN
						WareHouseItmsDTL WHIDL
					ON
						OWSI.numWarehouseItmsDTLID = WHIDL.numWareHouseItmsDTLID 
						AND WHIDL.numWareHouseItemID = OWSI.numWarehouseItmsID 
					INNER JOIN
						WareHouseItems
					ON
						WHIDL.numWareHouseItemID = WareHouseItems.numWareHouseItemID
					INNER JOIN
						Item
					ON
						WareHouseItems.numItemID = Item.numItemCode
					WHERE
						OWSI.numOppID = @OppID
						AND 1 = (CASE 
									WHEN Item.bitLotNo = 1 AND ISNULL(WHIDL.numQty,0) < ISNULL(OWSI.numQty,0) THEN 1 
									WHEN Item.bitSerialized = 1 AND ISNULL(WHIDL.numQty,0) = 0 THEN 1 
									ELSE 0 
								END)
					) > 0
				BEGIN
					RAISERROR ('SERIAL/LOT#_USED', 16, 1 ) ;
				END
				ELSE
				BEGIN
					-- REMOVE SERIAL/LOT# NUMBER FROM INVENTORY BECAUSE PURCHASE ORDER IS DELETED
					UPDATE WHIDL
						SET WHIDL.numQty = (CASE WHEN Item.bitLotNo = 1 THEN ISNULL(WHIDL.numQty,0) - ISNULL(OWSI.numQty,0) ELSE 0 END)
					FROM 
						WareHouseItmsDTL WHIDL
					INNER JOIN
						OppWarehouseSerializedItem OWSI
					ON
						WHIDL.numWareHouseItmsDTLID = OWSI.numWarehouseItmsDTLID
						AND WHIDL.numWareHouseItemID = OWSI.numWarehouseItmsID
					INNER JOIN
						WareHouseItems
					ON
						WHIDL.numWareHouseItemID = WareHouseItems.numWareHouseItemID
					INNER JOIN
						Item
					ON
						WareHouseItems.numItemID = Item.numItemCode
					WHERE
						OWSI.numOppID = @OppID
				END
				
				DELETE FROM OppWarehouseSerializedItem WHERE numOppID=@OppID
			END
		
			
			IF @OppType=2
			BEGIn
			DELETE FROM dbo.General_Journal_Details WHERE numJournalId IN (SELECT numJournal_Id FROM dbo.General_Journal_Header WHERE numDomainID=@numDomainID AND numOppId=@OppID AND ISNULL(numOppBizDocsId,0)=0 
				AND isnull(numBillID,0)=0 AND isnull(numBillPaymentID,0)=0 AND isnull(numPayrollDetailID,0)=0
				AND isnull(numCheckHeaderID,0)=0 AND isnull(numReturnID,0)=0 
				AND isnull(numDepositId,0)=0)
			
			DELETE FROM General_Journal_Header WHERE numDomainID=@numDomainID AND numOppId=@OppID AND ISNULL(numOppBizDocsId,0)=0 
				AND isnull(numBillID,0)=0 AND isnull(numBillPaymentID,0)=0 AND isnull(numPayrollDetailID,0)=0
				AND isnull(numCheckHeaderID,0)=0 AND isnull(numReturnID,0)=0 
				AND isnull(numDepositId,0)=0
			END
COMMIT
END TRY
BEGIN CATCH
  -- Whoops, there was an error
  IF @@TRANCOUNT > 0
     ROLLBACK

  -- Raise an error with the details of the exception
  DECLARE @ErrMsg nvarchar(4000), @ErrSeverity int
  SELECT @ErrMsg = ERROR_MESSAGE(),
         @ErrSeverity = ERROR_SEVERITY()

  RAISERROR(@ErrMsg, @ErrSeverity, 1)
END CATCH


GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_SalesFulfillmentUpdateInventory')
DROP PROCEDURE USP_SalesFulfillmentUpdateInventory
GO
CREATE PROCEDURE  USP_SalesFulfillmentUpdateInventory
    @numDomainID NUMERIC(9),
    @numOppID NUMERIC(9),
    @numOppBizDocsId NUMERIC(9),
    @numUserCntID NUMERIC(9),
    @tintMode TINYINT --1:Release 2:Revert
  AS 
BEGIN
BEGIN TRY
BEGIN TRANSACTION      
		DECLARE @numDomain AS NUMERIC(9)
		SET @numDomain = @numDomainID
        DECLARE @onAllocation AS NUMERIC,@numWarehouseItemID AS NUMERIC ,@numOppItemID AS NUMERIC          
        DECLARE @numOldQtyShipped AS NUMERIC,@numNewQtyShipped AS NUMERIC,@numUnits NUMERIC       
        DECLARE @Kit AS BIT,@vcItemName AS VARCHAR(300)                   
        DECLARE @vcError VARCHAR(500) 
		DECLARE @description AS VARCHAR(100)
         
        DECLARE @minRowNumber NUMERIC(9),@maxRowNumber NUMERIC(9)
         
         DECLARE @minKitRowNumber NUMERIC(9),@maxKitRowNumber NUMERIC(9),@KitWareHouseItemID NUMERIC(9)
								DECLARE @KitonAllocation as numeric(9),@KitQtyShipped as numeric(9),@KiyQtyItemsReq_Orig as numeric(9),@KiyQtyItemsReq as numeric(9)  
									DECLARE @Kitdescription AS VARCHAR(100)
								DECLARE @KitNewQtyShipped AS NUMERIC(9),@KitOppChildItemID AS NUMERIC(9)
						
        SELECT OBDI.numOppItemID,ISNULL(OBDI.numWarehouseItmsID, 0) AS numWarehouseItemID,
        ISNULL(OI.numQtyShipped,0) AS numQtyShipped,OBDI.numUnitHour,I.vcItemName,(case when bitKitParent=1 and bitAssembly=1 then 
        0 when bitKitParent=1 then 1 else 0 end) AS KIT,ROW_NUMBER() OVER(ORDER BY OBDI.numOppItemID) AS RowNumber INTO #tempItems
        FROM  dbo.OpportunityBizDocs OBD JOIN dbo.OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId=OBDI.numOppBizDocID
			  JOIN OpportunityItems OI ON OBDI.numOppItemID=OI.numoppitemtCode join Item I on OI.numItemCode=I.numItemCode
        WHERE OBD.numOppID=@numOppID AND OBD.numOppBizDocsId=@numOppBizDocsId AND OI.numOppID=@numOppID 
        	 AND (I.charitemtype='P' OR 1=(CASE WHEN ISNULL(I.bitKitParent,0) = 1 AND ISNULL(I.bitAssembly,0) = 1 THEN 0
											WHEN ISNULL(I.bitKitParent,0) = 1 THEN 1 ELSE 0 END))
			 and (OI.bitDropShip=0 or OI.bitDropShip is null) AND ISNULL(OI.bitWorkOrder,0)=0                                   
							
							
        SELECT  @minRowNumber = MIN(RowNumber),@maxRowNumber = MAX(RowNumber) FROM #tempItems
        
        
        WHILE  @minRowNumber <= @maxRowNumber
			BEGIN
				SELECT @numWarehouseItemID=numWarehouseItemID,@numOppItemID=numOppItemID,
				   	   @numOldQtyShipped=ISNULL(numQtyShipped,0),@numUnits=numUnitHour,
					   @Kit=Kit FROM #tempItems WHERE RowNumber=@minRowNumber
			
			
				IF @tintMode=1 --1:Release
				BEGIN
					SET @numNewQtyShipped = @numUnits-@numOldQtyShipped;
					  		
					IF @numNewQtyShipped>0
					BEGIN
						 SET @description='SO Qty Shipped (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@numNewQtyShipped AS VARCHAR(10)) + ')'
						 
						 IF @Kit=1
						 BEGIN
								SELECT OKI.numOppChildItemID,OKI.numWareHouseItemId,numQtyItemsReq,numQtyItemsReq_Orig,numQtyShipped,ISNULL(numAllocation, 0) AS numAllocation,
									ROW_NUMBER() OVER(ORDER BY numOppChildItemID) AS RowNumber INTO #tempKits
									from OpportunityKitItems OKI join Item I on OKI.numChildItemID=I.numItemCode  
									JOIN WareHouseItems WI ON WI.numWareHouseItemID=OKI.numWareHouseItemID  
									where charitemtype='P' and OKI.numWareHouseItemId>0 and OKI.numWareHouseItemId is not null 
									AND numOppItemID=@numOppItemID 
					
							IF(SELECT COUNT(*) FROM #tempKits WHERE numAllocation - ((@numUnits * numQtyItemsReq_Orig) - numQtyShipped) < 0)>0
							BEGIN
								SET @vcError ='You do not have enough KIT inventory to support this shipment('+ @vcItemName +'). Modify your Qty-Shipped to an amount equal to or less than the Qty-On-Allocation and try again.' 
								
								RAISERROR ('NotSufficientQty_Allocation',16,1);
							END
							ELSE
							BEGIN
								
								SELECT  @minKitRowNumber = MIN(RowNumber),@maxKitRowNumber = MAX(RowNumber) FROM #tempKits

								WHILE  @minKitRowNumber <= @maxKitRowNumber
								BEGIN
									SELECT @KitOppChildItemID=numOppChildItemID,@KitWareHouseItemID=numWareHouseItemID,@KiyQtyItemsReq=numQtyItemsReq,
											@KitQtyShipped=numQtyShipped,@KiyQtyItemsReq_Orig=numQtyItemsReq_Orig,
											@KitonAllocation=numAllocation FROM #tempKits WHERE RowNumber=@minKitRowNumber
						
									SET @KitNewQtyShipped = (@numNewQtyShipped * @KiyQtyItemsReq_Orig) - @KitQtyShipped;
						
									SET @Kitdescription='SO KIT Qty Shipped (Qty:' + CAST(@KiyQtyItemsReq AS VARCHAR(10)) + ' Shipped:' +  CAST(@KitNewQtyShipped AS VARCHAR(10)) + ')'

									SET @KitonAllocation = @KitonAllocation - @KitNewQtyShipped
			        
									IF (@KitonAllocation >= 0 )
									BEGIN
										UPDATE  WareHouseItems
										SET     numAllocation = @KitonAllocation,dtModified = GETDATE() 
										WHERE   numWareHouseItemID = @KitWareHouseItemID      	
									
										UPDATE  [OpportunityKitItems]
										SET     [numQtyShipped] = @numUnits * @KiyQtyItemsReq_Orig 
										WHERE   [numOppChildItemID] = @KitOppChildItemID
						                
										EXEC dbo.USP_ManageWareHouseItems_Tracking
												@numWareHouseItemID = @KitWareHouseItemID, --  numeric(9, 0)
												@numReferenceID = @numOppId, --  numeric(9, 0)
												@tintRefType = 3, --  tinyint
												@vcDescription = @Kitdescription, --  varchar(100)
												@numModifiedBy = @numUserCntID,
												@numDomainID = @numDomain 
									END
			            
									SET @minKitRowNumber=@minKitRowNumber + 1			 		
								END				
						   
								UPDATE  [OpportunityItems]
								SET     [numQtyShipped] = @numUnits
								WHERE   [numoppitemtCode] = @numOppItemID
				                
								EXEC dbo.USP_ManageWareHouseItems_Tracking
										@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
										@numReferenceID = @numOppId, --  numeric(9, 0)
										@tintRefType = 3, --  tinyint
										@vcDescription = @description, --  varchar(100)
										@numModifiedBy = @numUserCntID,
										@numDomainID = @numDomain
						 END
						 END
						 ELSE
						 BEGIN
							 SELECT @onAllocation = ISNULL(numAllocation, 0) FROM    WareHouseItems WHERE   numWareHouseItemID = @numWareHouseItemID
				        
							 SET @onAllocation = @onAllocation - @numNewQtyShipped	
				        
							 IF (@onAllocation >= 0 )
							 BEGIN
								UPDATE  WareHouseItems
								SET     numAllocation = @onAllocation,dtModified = GETDATE() 
								WHERE   numWareHouseItemID = @numWareHouseItemID      	
							
								UPDATE  [OpportunityItems]
								SET     [numQtyShipped] = @numUnits
								WHERE   [numoppitemtCode] = @numOppItemID AND numOppId=@numOppId
				                
								EXEC dbo.USP_ManageWareHouseItems_Tracking
										@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
										@numReferenceID = @numOppId, --  numeric(9, 0)
										@tintRefType = 3, --  tinyint
										@vcDescription = @description, --  varchar(100)
										@numModifiedBy = @numUserCntID,
										@numDomainID = @numDomain 
							END
							ELSE
							BEGIN
								SET @vcError ='You do not have enough inventory to support this shipment('+ @vcItemName +'). Modify your Qty-Shipped to an amount equal to or less than the Qty-On-Allocation and try again.' 
								RAISERROR ('NotSufficientQty_Allocation',16,1);
							END
						 END
					END	 
				END
				ELSE IF @tintMode=2 --2:Revert
				BEGIN
						DECLARE @numAuthInvoice NUMERIC(9)
						Select @numAuthInvoice=isnull(numAuthoritativeSales,0) From dbo.AuthoritativeBizDocs Where AuthoritativeBizDocs.numDomainId=@numDomainId 
				
						IF EXISTS (SELECT numOppBizDocsId FROM OpportunityMaster OM JOIN OpportunityBizDocs OBD ON OM.numOppID=OBD.numOppID
							WHERE OM.numDomainId=@numDomainId AND OM.numOppID=@numOppID  AND OBD.numBizDocId IN (@numAuthInvoice))
						BEGIN
								RAISERROR ('AlreadyInvoice_DoNotReturn',16,1);
								RETURN -1
						END		
						
					IF @numOldQtyShipped>0
					BEGIN
						 SET @description='SO Qty Shipped Return (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped Return:' +  CAST(@numOldQtyShipped AS VARCHAR(10)) + ')'
					
						 IF @Kit=1
						 BEGIN
								SELECT OKI.numOppChildItemID,OKI.numWareHouseItemId,numQtyItemsReq,numQtyItemsReq_Orig,numQtyShipped,ISNULL(numAllocation, 0) AS numAllocation,
									ROW_NUMBER() OVER(ORDER BY numOppChildItemID) AS RowNumber INTO #tempKitsReturn
									from OpportunityKitItems OKI join Item I on OKI.numChildItemID=I.numItemCode  
									JOIN WareHouseItems WI ON WI.numWareHouseItemID=OKI.numWareHouseItemID  
									where charitemtype='P' and OKI.numWareHouseItemId>0 and OKI.numWareHouseItemId is not null 
									AND numOppItemID=@numOppItemID 
					
								SELECT  @minKitRowNumber = MIN(RowNumber),@maxKitRowNumber = MAX(RowNumber) FROM #tempKitsReturn

								WHILE  @minKitRowNumber <= @maxKitRowNumber
								BEGIN
									SELECT @KitOppChildItemID=numOppChildItemID,@KitWareHouseItemID=numWareHouseItemID,@KiyQtyItemsReq=numQtyItemsReq,
											@KitQtyShipped=numQtyShipped,@KiyQtyItemsReq_Orig=numQtyItemsReq_Orig,
											@KitonAllocation=numAllocation FROM #tempKits WHERE RowNumber=@minKitRowNumber
						
									SET @KitNewQtyShipped = (@numOldQtyShipped * @KiyQtyItemsReq_Orig);
						
									SET @Kitdescription='SO KIT Qty Shipped Return (Qty:' + CAST(@KiyQtyItemsReq AS VARCHAR(10)) + ' Shipped:' +  CAST(@KitNewQtyShipped AS VARCHAR(10)) + ')'

									SET @KitonAllocation = @KitonAllocation + @KitNewQtyShipped
			        
										UPDATE  WareHouseItems
										SET     numAllocation = @KitonAllocation,dtModified = GETDATE() 
										WHERE   numWareHouseItemID = @KitWareHouseItemID      	
									
										UPDATE  [OpportunityKitItems]
										SET     [numQtyShipped] = 0
										WHERE   [numOppChildItemID] = @KitOppChildItemID
						                
										EXEC dbo.USP_ManageWareHouseItems_Tracking
												@numWareHouseItemID = @KitWareHouseItemID, --  numeric(9, 0)
												@numReferenceID = @numOppId, --  numeric(9, 0)
												@tintRefType = 3, --  tinyint
												@vcDescription = @Kitdescription, --  varchar(100)
												@numModifiedBy = @numUserCntID,
												@numDomainID = @numDomain 
			            
									SET @minKitRowNumber=@minKitRowNumber + 1			 		
								END				
						   
								UPDATE  [OpportunityItems]
								SET     [numQtyShipped] = @numUnits
								WHERE   [numoppitemtCode] = @numOppItemID
				                
								EXEC dbo.USP_ManageWareHouseItems_Tracking
										@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
										@numReferenceID = @numOppId, --  numeric(9, 0)
										@tintRefType = 3, --  tinyint
										@vcDescription = @description, --  varchar(100)
										@numModifiedBy = @numUserCntID,
										@numDomainID = @numDomain 
						 END
						 ELSE
						 BEGIN
						 SELECT @onAllocation = ISNULL(numAllocation, 0) FROM    WareHouseItems WHERE   numWareHouseItemID = @numWareHouseItemID
				        
						 SET @onAllocation = @onAllocation + @numOldQtyShipped	
				        
							 IF (@onAllocation >= 0 )
							 BEGIN
								UPDATE  WareHouseItems
								SET     numAllocation = @onAllocation,dtModified = GETDATE() 
								WHERE   numWareHouseItemID = @numWareHouseItemID      	
							
								UPDATE  [OpportunityItems]
								SET     [numQtyShipped] = 0
								WHERE   [numoppitemtCode] = @numOppItemID AND numOppId=@numOppId
				                
								EXEC dbo.USP_ManageWareHouseItems_Tracking
										@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
										@numReferenceID = @numOppId, --  numeric(9, 0)
										@tintRefType = 3, --  tinyint
										@vcDescription = @description, --  varchar(100)
										@numModifiedBy = @numUserCntID,
										@numDomainID = @numDomain 
							 END
							END 			
					END
					ELSE
					BEGIN
						SET @vcError ='You have not shipped qty for ('+ @vcItemName +').' 
						RAISERROR ('NoQty_ForShipped',16,1);
					END
				END
				  		
				SET @minRowNumber=@minRowNumber + 1			 		
			END	

COMMIT
END TRY
BEGIN CATCH
  -- Whoops, there was an error
  IF @@TRANCOUNT > 0
     ROLLBACK

  -- Raise an error with the details of the exception
  DECLARE @ErrMsg nvarchar(4000), @ErrSeverity int
  SELECT @ErrMsg = ERROR_MESSAGE(),
         @ErrSeverity = ERROR_SEVERITY()

  RAISERROR(@ErrMsg, @ErrSeverity, 1)
END CATCH
END
GO
    
/****** Object:  StoredProcedure [dbo].[USP_ShoppingCart]    Script Date: 05/07/2009 22:09:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_shoppingcart')
DROP PROCEDURE usp_shoppingcart
GO
CREATE PROCEDURE [dbo].[USP_ShoppingCart]                            
@numDivID as numeric(18),                            
@numOppID as numeric(18)=null output ,                                              
@vcPOppName as varchar(200)='' output,  
@numContactId as numeric(18),                            
@numDomainId as numeric(18),
@vcSource VARCHAR(50)=NULL,
@numCampainID NUMERIC(9)=0,
@numCurrencyID as numeric(9),
@numBillAddressId numeric(9),
@numShipAddressId numeric(9),
@bitDiscountType BIT,
@fltDiscount FLOAT,
@numProId NUMERIC(9),
@numSiteID NUMERIC(9),
@tintOppStatus INT,
@monShipCost MONEY,
@tintSource AS TINYINT ,
@tintSourceType AS TINYINT ,                          
@numPaymentMethodId NUMERIC(9),
@txtComments varchar(1000)

as                            
declare @CRMType as integer               
declare @numRecOwner as integer                            
if @numOppID=  0                             
begin                            
select @CRMType=tintCRMType,@numRecOwner=numRecOwner from DivisionMaster where numDivisionID=@numDivID                            
if @CRMType= 0                             
begin                            
UPDATE DivisionMaster                               
   SET tintCRMType=1                              
   WHERE numDivisionID=@numDivID                              
                                                   
end                            
declare @TotalAmount as FLOAT
--declare @intOppTcode as numeric(9) 
--declare @numCurrencyID as numeric(9)
--declare @fltExchangeRate as float                          
--Select @intOppTcode=max(numOppId)from OpportunityMaster                              
--set @intOppTcode =isnull(@intOppTcode,0) + 1                              

DECLARE @numTemplateID NUMERIC
SELECT  @numTemplateID= numMirrorBizDocTemplateId FROM eCommercePaymentConfig WHERE  numSiteId=@numSiteID AND numPaymentMethodId = @numPaymentMethodId AND numDomainID = @numDomainID   

   Declare @fltExchangeRate float                                 
    
   IF @numCurrencyID=0 select @numCurrencyID=isnull(numCurrencyID,0) from Domain where numDomainID=@numDomainId                            
   if @numCurrencyID=0 set   @fltExchangeRate=1
  else set @fltExchangeRate=dbo.GetExchangeRate(@numDomainID,@numCurrencyID)                          
                            
  
  --Set Default Class If enable User Level Class Accountng 
  DECLARE @numAccountClass AS NUMERIC(18);SET @numAccountClass=0
  SELECT @numAccountClass=ISNULL(numDefaultClass,0) FROM dbo.UserMaster UM JOIN dbo.Domain D ON UM.numDomainID=D.numDomainId
  WHERE D.numDomainId=@numDomainId AND UM.numUserDetailId=@numContactId AND ISNULL(D.IsEnableUserLevelClassTracking,0)=1
                            
--set @vcPOppName=@vcPOppName+ '-'+convert(varchar(4),year(getutcdate()))  +'-A'+ convert(varchar(10),@intOppTcode)                              
  --SELECT tintSource , tintSourceType FROM dbo.OpportunityMaster
insert into OpportunityMaster                              
  (                              
  numContactId,                              
  numDivisionId,                              
  txtComments,                              
  numCampainID,                              
  bitPublicFlag,                              
  tintSource, 
  tintSourceType ,                               
  vcPOppName,                              
  monPAmount,                              
  numCreatedBy,                              
  bintCreatedDate,                               
  numModifiedBy,                              
  bintModifiedDate,                              
  numDomainId,                               
  tintOppType, 
  tintOppStatus,                   
  intPEstimatedCloseDate,              
  numRecOwner,
  bitOrder,
  numCurrencyID,
  fltExchangeRate,fltDiscountTotal,bitDiscountTypeTotal,
  monShipCost,
  numOppBizDocTempID,numAccountClass
  )                              
 Values                              
  (                              
  @numContactId,                              
  @numDivID,                              
  @txtComments,                              
  @numCampainID,--  0,
  0,                              
  @tintSource,   
  @tintSourceType,                           
  ISNULL(@vcPOppName,'SO'),                             
  0,                                
  @numContactId,                              
  getutcdate(),                              
  @numContactId,                              
  getutcdate(),        
  @numDomainId,                              
  1,             
  @tintOppStatus,       
  getutcdate(),                                 
  @numRecOwner ,
  1,
  @numCurrencyID,
  @fltExchangeRate,@fltDiscount,@bitDiscountType
  ,@monShipCost,
  @numTemplateID,@numAccountClass
  
  )                               
set @numOppID=scope_identity()                             
--Update OppName as per Name Template
EXEC dbo.USP_UpdateNameTemplateValue 1,@numDomainId,@numOppID
SELECT @vcPOppName = vcPOppName FROM OpportunityMaster WHERE numOppID= @numOppID
end
delete  from    OpportunityItems where   numOppId= @numOppID                               

--if convert(varchar(10),@strItems) <>''                
--begin               
-- DECLARE @hDocItem int                
-- EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems                
                
insert into OpportunityItems                                                        
  (numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,vcAttributes,[vcItemName],[vcModelID],[vcManufacturer],vcPathForTImage, monVendorCost,numUOMId,bitDiscountType,fltDiscount,monTotAmtBefDiscount)                                                        
  --kishan                                                       
 select @numOppID,X.numItemCode,x.numUnitHour * x.decUOMConversionFactor,x.monPrice/x.decUOMConversionFactor,x.monTotAmount,X.vcItemDesc, NULLIF(X.numWarehouseItmsID,0),X.vcItemType,X.vcAttributes,(SELECT vcItemName FROM item WHERE numItemCode = X.numItemCode),(SELECT vcModelID FROM item WHERE numItemCode = X.numItemCode),(SELECT vcManufacturer FROM item WHERE numItemCode = X.numItemCode),
		(SELECT TOP 1  vcPathForTImage FROM dbo.ItemImages WHERE numItemCode = X.numItemCode AND bitDefault = 1 AND numDomainId = @numDomainId),   (select isnull(VN.monCost,0) from Vendor VN, Item IT where IT.numItemCode=VN.numItemCode and  VN.numItemCode=X.numItemCode and  VN.numVendorID=IT.numVendorID), 
		numUOM,bitDiscountType,fltDiscount,monTotAmtBefDiscount 
FROM dbo.CartItems X WHERE numUserCntId =@numContactId

declare @tintShipped as tinyint      
DECLARE @tintOppType AS TINYINT
      
select @tintOppStatus=tintOppStatus,@tintOppType=tintOppType,@tintShipped=tintShipped from OpportunityMaster where numOppID=@numOppID              
if @tintOppStatus=1               
	begin         
	PRINT 'inside USP_UpdatingInventoryonCloseDeal'
		exec USP_UpdatingInventoryonCloseDeal @numOppID,@numContactId
	end              

/*Commented by Chintan..reason:tobe executed only when order is confirmed*/
--exec USP_UpdatingInventoryonCloseDeal @numOppID,@numContactId

-- SELECT * FROM OPENXML (@hDocItem,'/NewDataSet/Table',2)                                                        
-- WITH  (                                                        
--  numoppitemtCode numeric(9),                   
--  numItemCode numeric(9),                                                        
--  numUnitHour numeric(9),                                                        
--  monPrice money,                                                     
--  monTotAmount money,                                                        
--  vcItemDesc varchar(1000),                  
--  numWarehouseItmsID numeric(9),        
--  vcItemType varchar(30),    
--  vcAttrValues varchar(50) ,
--  numUOM NUMERIC(18,0),
--  decUOMConversionFactor DECIMAL(18,5),bitDiscountType BIT,fltDiscount FLOAT,monTotAmtBefDiscount MONEY,
--  vcShippingMethod VARCHAR(100),decShippingCharge MONEY,dtDeliveryDate datetime
--  ))X                
--                
                
-- EXEC sp_xml_removedocument @hDocItem                
                
--end
                             
                            
select @TotalAmount=sum(monTotAmount)from OpportunityItems where numOppId=@numOppID                            
 update OpportunityMaster set  monPamount=@TotalAmount - @fltDiscount                           
 where numOppId=@numOppID                           
                          
--set @vcPOppName=(select  ISNULL(vcPOppName,numOppId) from OpportunityMaster where numOppId=@numOppID )

IF(@vcSource IS NOT NULL)
BEGIN
	insert into OpportunityLinking ([numParentOppID],[numChildOppID],[vcSource],[numParentOppBizDocID],numPromotionId,numSiteID)  values(null,@numOppID,@vcSource,NULL,@numProId,@numSiteID);
END

--Add/Update Address Details
DECLARE @vcStreet  varchar(100),@vcCity  varchar(50),@vcPostalCode  varchar(15),@vcCompanyName  varchar(100),@vcAddressName VARCHAR(50)      
DECLARE @numState  numeric(9),@numCountry  numeric(9), @numCompanyId  numeric(9) 
DECLARE @bitIsPrimary BIT;

--Bill Address
IF @numBillAddressId>0
BEGIN
 SELECT  @vcStreet=ISNULL(vcStreet,''),@vcCity=ISNULL(vcCity,''),@vcPostalCode=ISNULL(vcPostalCode,''),
		@numState=ISNULL(numState,0),@numCountry=ISNULL(numCountry,0),@bitIsPrimary=bitIsPrimary,
		@vcAddressName=vcAddressName
            FROM  dbo.AddressDetails WHERE   numDomainID = @numDomainID AND numAddressID = @numBillAddressId 
            
  
  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 0, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = '', --  varchar(100)
	@numCompanyId = 0, --  numeric(9, 0)
	@vcAddressName =@vcAddressName
	
        
END
  
  
  --Ship Address
IF @numShipAddressId>0
BEGIN
   	SELECT  @vcStreet=ISNULL(vcStreet,''),@vcCity=ISNULL(vcCity,''),@vcPostalCode=ISNULL(vcPostalCode,''),
		@numState=ISNULL(numState,0),@numCountry=ISNULL(numCountry,0),@bitIsPrimary=bitIsPrimary,
		@vcAddressName=vcAddressName
            FROM  dbo.AddressDetails WHERE   numDomainID = @numDomainID AND numAddressID = @numShipAddressId 
  
  
	select  @vcCompanyName=vcCompanyName,@numCompanyId=div.numCompanyID from CompanyInfo Com                            
	join divisionMaster Div                            
	on div.numCompanyID=com.numCompanyID                            
	where div.numdivisionID=@numDivID

  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 1, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = @vcCompanyName, --  varchar(100)
	@numCompanyId =@numCompanyId, --  numeric(9, 0)
	@vcAddressName =@vcAddressName
	
   	
END

--Insert Tax for Division                       
INSERT dbo.OpportunityMasterTaxItems (
	numOppId,
	numTaxItemID,
	fltPercentage
) SELECT @numOppID,TI.numTaxItemID,dbo.fn_CalItemTaxAmt(@numDomainID,@numDivID,0,TI.numTaxItemID,@numOppId,0,NULL) FROM TaxItems TI JOIN DivisionTaxTypes DTT ON TI.numTaxItemID = DTT.numTaxItemID
 WHERE DTT.numDivisionID=@numDivID AND DTT.bitApplicable=1
   union 
  select @numOppID,0,dbo.fn_CalItemTaxAmt(@numDomainID,@numDivID,0,0,@numOppId,1,NULL) 
  FROM dbo.DivisionMaster WHERE bitNoTax=0 AND numDivisionID=@numDivID
  
  --Delete Tax for Opportunity Items if item deleted 
DELETE FROM OpportunityItemsTaxItems WHERE numOppId=@numOppId AND numOppItemID NOT IN (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID)

--Insert Tax for Opportunity Items
INSERT INTO dbo.OpportunityItemsTaxItems (
	numOppId,
	numOppItemID,
	numTaxItemID
) SELECT @numOppId,OI.numoppitemtCode,TI.numTaxItemID FROM dbo.OpportunityItems OI JOIN dbo.ItemTax IT ON OI.numItemCode=IT.numItemCode JOIN
TaxItems TI ON TI.numTaxItemID=IT.numTaxItemID WHERE OI.numOppId=@numOppID AND IT.bitApplicable=1 AND 
OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
UNION
  select @numOppId,OI.numoppitemtCode,0 FROM dbo.OpportunityItems OI JOIN dbo.Item I ON OI.numItemCode=I.numItemCode
   WHERE OI.numOppId=@numOppID  AND I.bitTaxable=1 AND
OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)

  
 

GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_UpdateInventoryAdjustments' ) 
    DROP PROCEDURE USP_UpdateInventoryAdjustments
GO
CREATE PROCEDURE USP_UpdateInventoryAdjustments
    @numDomainId NUMERIC,
    @numUserCntID NUMERIC,
    @strItems VARCHAR(8000),
    @dtAdjustmentDate AS DATETIME
AS 
BEGIN
    
DECLARE @hDoc INT    
EXEC sp_xml_preparedocument @hDoc OUTPUT, @strItems    
  SELECT * INTO #temp FROM OPENXML (@hDoc,'/NewDataSet/Item',2)    
	WITH ( numWareHouseItemID NUMERIC(9),numItemCode NUMERIC(9),
		intAdjust INT) 
		
  SELECT *,ROW_NUMBER() OVER(ORDER BY numItemCode) AS RowNo INTO #tempSerialLotNO FROM OPENXML (@hDoc,'/NewDataSet/SerialLotNo',2)    
	WITH (numItemCode NUMERIC(9),numWareHouseID NUMERIC(9),numWareHouseItemID NUMERIC(9),vcSerialNo VARCHAR(3000),byteMode TINYINT) 
		
EXEC sp_xml_removedocument @hDoc  

    
		    DECLARE @numWareHouseItemID AS NUMERIC(9)
			DECLARE @numItemCode NUMERIC(18)
			
			
		    SELECT @numWareHouseItemID = MIN(numWareHouseItemID) FROM #temp
			Declare @bitLotNo as bit;SET @bitLotNo=0  
			Declare @bitSerialized as bit;SET @bitSerialized=0  

				while @numWareHouseItemID>0    
				 begin
				  
				  select @bitLotNo=isnull(Item.bitLotNo,0),@bitSerialized=isnull(Item.bitSerialized,0)
					from item INNER JOIN #temp ON item.numItemCode=#temp.numItemCode where #temp.numWareHouseItemID=@numWareHouseItemID and numDomainID=@numDomainID

				  IF @bitLotNo=0 AND @bitSerialized=0
				  BEGIN		
						  UPDATE  WI SET numOnHand =numOnHand + #temp.intAdjust,dtModified=GETDATE()  FROM dbo.WareHouseItems WI INNER JOIN #temp ON WI.numWareHouseItemID = #temp.numWareHouseItemID
						  WHERE WI.numWareHouseItemID = @numWareHouseItemID AND WI.numDomainID=@numDomainId
						  
								 SELECT @numItemCode=numItemID FROM dbo.WareHouseItems WHERE numWareHouseItemID = @numWareHouseItemID AND numDomainID=@numDomainId
								
								DECLARE @numDomain AS NUMERIC(18,0)
								SET @numDomain = @numDomainID
								EXEC dbo.USP_ManageWareHouseItems_Tracking
								@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
								@numReferenceID = @numItemCode, --  numeric(9, 0)
								@tintRefType = 1, --  tinyint
								@vcDescription = 'Inventory Adjustment', --  varchar(100)
								@numModifiedBy = @numUserCntID,
								@dtRecordDate = @dtAdjustmentDate,
								@numDomainId = @numDomain
				  END	  
				    
				 SELECT TOP 1 @numWareHouseItemID = numWareHouseItemID FROM #temp WHERE numWareHouseItemID >@numWareHouseItemID 
				 order by numWareHouseItemID  
				    
				 if @@rowcount=0 set @numWareHouseItemID =0    
				 end    
    

    DROP TABLE #temp
    
    
    -------------Serial/Lot #s----------------
	DECLARE @minRowNo NUMERIC(18),@maxRowNo NUMERIC(18)
	SELECT @minRowNo = MIN(RowNo),@maxRowNo = Max(RowNo) FROM #tempSerialLotNO
		    
	DECLARE @numWareHouseID NUMERIC(9),@vcSerialNo VARCHAR(3000),@numWareHouseItmsDTLID AS NUMERIC(9),
			@vcComments VARCHAR(1000),@OldQty NUMERIC(9),@numQty NUMERIC(9),@byteMode TINYINT 		    
	DECLARE @posComma int, @strKeyVal varchar(20)
		    
	WHILE @minRowNo <= @maxRowNo
	BEGIN
		SELECT @numItemCode=numItemCode,@numWareHouseID=numWareHouseID,@numWareHouseItemID=numWareHouseItemID,
			@vcSerialNo=vcSerialNo,@byteMode=byteMode FROM #tempSerialLotNO WHERE RowNo=@minRowNo
		
		SET @posComma=0
		
		SET @vcSerialNo=RTRIM(@vcSerialNo)
		IF RIGHT(@vcSerialNo, 1)!=',' SET @vcSerialNo=@vcSerialNo+','

		SET @posComma=PatIndex('%,%', @vcSerialNo)
		WHILE @posComma>1
			BEGIN
				SET @strKeyVal=ltrim(rtrim(substring(@vcSerialNo, 1, @posComma-1)))
	
				DECLARE @posBStart INT,@posBEnd int, @strQty varchar(20),@strName varchar(20)

				SET @posBStart=PatIndex('%(%', @strKeyVal)
				SET @posBEnd=PatIndex('%)%', @strKeyVal)
				IF( @posBStart>1 AND @posBEnd>1)
				BEGIN
					SET @strName=ltrim(rtrim(substring(@strKeyVal, 1, @posBStart-1)))
					SET	@strQty=ltrim(rtrim(substring(@strKeyVal, @posBStart+1,len(@strKeyVal)-@posBStart-1)))
				END		
				ELSE
				BEGIN
					SET @strName=@strKeyVal
					SET	@strQty=1
				END
	  
			SET @numWareHouseItmsDTLID=0
			SET @OldQty=0
			SET @vcComments=''
			
			select top 1 @numWareHouseItmsDTLID=ISNULL(numWareHouseItmsDTLID,0),@OldQty=ISNULL(numQty,0),@vcComments=ISNULL(vcComments,'') 
					from WareHouseItmsDTL where numWareHouseItemID=@numWareHouseItemID and
					vcSerialNo=@strName AND ISNULL(numQty,0) > 0
			
			IF @byteMode=0 --Add
				SET @numQty=@OldQty + @strQty
			ELSE --Deduct
				SET @numQty=@OldQty - @strQty
			
			EXEC dbo.USP_AddUpdateWareHouseForItems
					@numItemCode = @numItemCode, --  numeric(9, 0)
					@numWareHouseID = @numWareHouseID, --  numeric(9, 0)
					@numWareHouseItemID =@numWareHouseItemID,
					@numDomainID = @numDomainId, --  numeric(9, 0)
					@vcSerialNo = @strName, --  varchar(100)
					@vcComments = @vcComments, -- varchar(1000)
					@numQty = @numQty, --  numeric(18, 0)
					@byteMode = 5, --  tinyint
					@numWareHouseItmsDTLID = @numWareHouseItmsDTLID, --  numeric(18, 0)
					@numUserCntID = @numUserCntID, --  numeric(9, 0)
					@dtAdjustmentDate = @dtAdjustmentDate
	  
			SET @vcSerialNo=substring(@vcSerialNo, @posComma +1, len(@vcSerialNo)-@posComma)
			SET @posComma=PatIndex('%,%',@vcSerialNo)
		END

		SET @minRowNo=@minRowNo+1
	END
	
	DROP TABLE #tempSerialLotNO	    
END


/****** Object:  StoredProcedure [dbo].[USP_UpdateOppSerializedItem_BizDoc_Comma]    Script Date: 07/26/2008 16:21:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_UpdateOppSerializedItem_BizDoc_Comma')
DROP PROCEDURE USP_UpdateOppSerializedItem_BizDoc_Comma
GO
CREATE PROCEDURE [dbo].[USP_UpdateOppSerializedItem_BizDoc_Comma]  
    @numOppItemTcode as numeric(9)=0,  
    @numOppID as numeric(9)=0,  
    @strItems as VARCHAR(1000)='',
    @numBizDocID as numeric(9)=0,
    @numWarehouseItmsID as numeric(9)=0
as  
-- STORE PROCEDURE LOGIC IS COMMENTED BECAUSE IT IS NOT USED NOW AND CODE IS NOT RELEVANT TO BIZ NOW.
--SELECT vcSerialNo,  isnull(numQty,0) 
--  - isnull((select sum(w.numQty) from OppWarehouseSerializedItem w INNER JOIN OpportunityMaster 
--  opp ON (w.numOppId=opp.numOppId AND opp.tintOppType=1) where isnull(opp.tintshipped,0)=0 and 
--  w.numWarehouseItmsDTLID=WareHouseItmsDTL.numWarehouseItmsDTLID),0)
--  
--  + isnull((select sum(w.numQty) from OppWarehouseSerializedItem w INNER JOIN OpportunityMaster 
--  opp ON (w.numOppId=opp.numOppId AND opp.tintOppType=1) where isnull(opp.tintshipped,0)=0 and 
--  w.numWarehouseItmsDTLID=WareHouseItmsDTL.numWarehouseItmsDTLID AND numOppBizDocsId=@numBizDocID),0)
--   as TotalQty,numWarehouseItmsDTLID
--INTO #tempTable
-- from   WareHouseItmsDTL   
--    where (tintStatus is null or tintStatus=0) and  numWareHouseItemID=@numWarehouseItmsID

--Create table #tempTable (                                                                    
-- vcSerialNo VARCHAR(100),TotalQty [numeric](18, 0),numWarehouseItmsDTLID [numeric](18, 0)                                             
-- )                       

--INSERT INTO #tempTable (vcSerialNo,numWarehouseItmsDTLID,TotalQty)  
--            SELECT vcSerialNo,WareHouseItmsDTL.numWareHouseItmsDTLID,
--  isnull(WareHouseItmsDTL.numQty,0) - isnull((select sum(w.numQty) from OppWarehouseSerializedItem w INNER JOIN OpportunityMaster opp ON (w.numOppId=opp.numOppId AND opp.tintOppType=1) where isnull(opp.tintshipped,0)=0 and w.numWarehouseItmsDTLID=WareHouseItmsDTL.numWarehouseItmsDTLID),0) + isnull(OppWarehouseSerializedItem.numQty,0) as TotalQty
-- from   OppWarehouseSerializedItem      
-- join WareHouseItmsDTL      
-- on WareHouseItmsDTL.numWareHouseItmsDTLID= OppWarehouseSerializedItem.numWarehouseItmsDTLID                           
-- where numOppID=@numOppID and  numWareHouseItemID=@numWarehouseItmsID and numOppBizDocsId=@numBizDocID ORDER BY vcSerialNo,TotalQty desc


--INSERT INTO #tempTable (vcSerialNo,numWarehouseItmsDTLID,TotalQty)  
--		 SELECT vcSerialNo,numWareHouseItmsDTLID,
--  isnull(numQty,0) - isnull((select sum(w.numQty) from OppWarehouseSerializedItem w INNER JOIN OpportunityMaster opp ON (w.numOppId=opp.numOppId AND opp.tintOppType=1) where isnull(opp.tintshipped,0)=0 and w.numWarehouseItmsDTLID=WareHouseItmsDTL.numWarehouseItmsDTLID),0) as TotalQty
-- from   WareHouseItmsDTL   
--    where (tintStatus is null or tintStatus=0) and  numWareHouseItemID=@numWarehouseItmsID  
--    and numWareHouseItmsDTLID not in(select numWarehouseItmsDTLID from OppWarehouseSerializedItem where numOppID=@numOppID and  numWarehouseItmsID=@numWarehouseItmsID and numOppBizDocsId=@numBizDocID) ORDER BY vcSerialNo,TotalQty desc

    
--Create table #tempLotSerial (                                                                    
-- vcSerialNo VARCHAR(100),UsedQty [numeric](18, 0),TotalQty [numeric](18, 0),numWarehouseItmsDTLID [numeric](18, 0)                                              
-- )                       

  
--DECLARE @posComma int, @strKeyVal varchar(20)

--SET @strItems=RTRIM(@strItems)
--IF RIGHT(@strItems, 1)!=',' SET @strItems=@strItems+','

--SET @posComma=PatIndex('%,%', @strItems)
--WHILE @posComma>1
--BEGIN
--	SET @strKeyVal=ltrim(rtrim(substring(@strItems, 1, @posComma-1)))
	
--	DECLARE @posBStart INT,@posBEnd int, @strQty varchar(20),@strName varchar(20)

--    SET @posBStart=PatIndex('%(%', @strKeyVal)
--    SET @posBEnd=PatIndex('%)%', @strKeyVal)
--	IF( @posBStart>1 AND @posBEnd>1)
--	BEGIN
--		    SET @strName=ltrim(rtrim(substring(@strKeyVal, 1, @posBStart-1)))
--			SET	@strQty=ltrim(rtrim(substring(@strKeyVal, @posBStart+1,len(@strKeyVal)-@posBStart-1)))
--	END		
			
--	ELSE
--	BEGIN
--			SET @strName=@strKeyVal
--			SET	@strQty=1
--	END
	  
--	PRINT  'Name->' + @strName + ':Qty->' + @strQty
	  
--	  DECLARE @TotalQty NUMERIC(9)
	  
--	 IF EXISTS(SELECT 1 FROM #tempTable WHERE vcSerialNo=@strName AND TotalQty>=@strQty)
--	 BEGIN
--	     INSERT INTO #tempLotSerial (vcSerialNo,UsedQty,TotalQty,numWarehouseItmsDTLID)  
--		 SELECT TOP 1 vcSerialNo,@strQty,TotalQty,numWarehouseItmsDTLID FROM #tempTable WHERE vcSerialNo=@strName AND TotalQty>=@strQty 
--	 END
	  
--	 SET @strItems=substring(@strItems, @posComma +1, len(@strItems)-@posComma)
--	 SET @posComma=PatIndex('%,%',@strItems)
	
--END

--update WareHouseItmsDTL set tintStatus=0 where numWareHouseItmsDTLID in 
--(select numWarehouseItmsDTLID from OppWarehouseSerializedItem where numOppItemID=@numOppItemTcode 
--AND numOppBizDocsId=@numBizDocID)

--delete from OppWarehouseSerializedItem where numOppItemID=@numOppItemTcode AND numOppBizDocsId=@numBizDocID

-- Insert into OppWarehouseSerializedItem(numWarehouseItmsDTLID,numOppID,numOppItemID,numWarehouseItmsID,numQty,numOppBizDocsId)                
--  (SELECT numWarehouseItmsDTLID,@numOppID,@numOppItemTcode,@numWarehouseItmsID,SUM(UsedQty),@numBizDocID                                            
--     FROM #tempLotSerial GROUP BY numWarehouseItmsDTLID)
      
--update WareHouseItmsDTL set tintStatus=1 where numWareHouseItmsDTLID in 
--(SELECT numWareHouseItmsDTLID FROM #tempLotSerial GROUP BY numWarehouseItmsDTLID having (SUM(TotalQty)-SUM(UsedQty)=0)
----SELECT numWareHouseItmsDTLID FROM #tempLotSerial WHERE (TotalQty-UsedQty)=0
--)


----SELECT * FROM #tempTable
----SELECT * FROM #tempLotSerial
--DROP TABLE #tempLotSerial
--DROP TABLE #tempTable

GO
/****** Object:  StoredProcedure [dbo].[USP_UpdateOppSerializedItems]    Script Date: 07/26/2008 16:21:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_updateoppserializeditems')
DROP PROCEDURE usp_updateoppserializeditems
GO
CREATE PROCEDURE [dbo].[USP_UpdateOppSerializedItems]  
@numOppItemTcode as numeric(9)=0,  
@numOppID as numeric(9)=0,  
@strItems as text='',
@OppType as tinyint  
as  
--THIS PROCEDURE IS NOT USED AND ITS IMPLEMETATION IS OLD AND NOT RELEVENT TO CURRENT STRUCTURE SO DO NOT USE IT
--declare @hDoc as int                
--EXEC sp_xml_preparedocument @hDoc OUTPUT, @strItems    
--if   @OppType=2
--begin             
                
--    update WareHouseItmsDTL set                                             
--     numWareHouseItemID=X.numWareHouseItemID,                                           
--     vcSerialNo=X.vcSerialNo                                                               
--      From (SELECT numWareHouseItmsDTLID as WareHouseItmsDTLID,numWareHouseItemID,vcSerialNo,Op_Flag                                           
--     FROM OPENXML(@hDoc,'/NewDataSet/SerializedItems[Op_Flag=2]',2)                                             
--      with(numWareHouseItmsDTLID numeric(9),                                            
--      numWareHouseItemID numeric(9),                                            
--      vcSerialNo varchar(100),                
--      Op_Flag tinyint))X                                             
--    where  numWareHouseItmsDTLID=X.WareHouseItmsDTLID                
                
                
-- delete from  WareHouseItmsDTL where numWareHouseItmsDTLID in (SELECT numWareHouseItmsDTLID                                            
--     FROM OPENXML(@hDoc,'/NewDataSet/SerializedItems[Op_Flag=3]',2)                                             
--      with(numWareHouseItmsDTLID numeric(9),                                            
--      numWareHouseItemID numeric(9),                                            
--      vcSerialNo varchar(100),                
--      Op_Flag tinyint))  
  
       
--  delete from  OppWarehouseSerializedItem where numWareHouseItmsDTLID in (SELECT numWareHouseItmsDTLID                                            
--     FROM OPENXML(@hDoc,'/NewDataSet/SerializedItems[Op_Flag=2]',2)                                             
--      with(numWareHouseItmsDTLID numeric(9),                                            
--      numWareHouseItemID numeric(9),                                            
--      vcSerialNo varchar(100),                
--      Op_Flag tinyint))  
--  delete from  OppWarehouseSerializedItem where numWareHouseItmsDTLID in (SELECT numWareHouseItmsDTLID                                            
--     FROM OPENXML(@hDoc,'/NewDataSet/SerializedItems[Op_Flag=3]',2)                                             
--      with(numWareHouseItmsDTLID numeric(9),                                            
--      numWareHouseItemID numeric(9),                                            
--      vcSerialNo varchar(100),                
--      Op_Flag tinyint))  
    
-- Insert into OppWarehouseSerializedItem(numWarehouseItmsDTLID,numOppID,numOppItemID,numWarehouseItmsID)                
--  (SELECT numWareHouseItmsDTLID,@numOppID,@numOppItemTcode,numWareHouseItemID                                            
--      FROM OPENXML(@hDoc,'/NewDataSet/SerializedItems[Op_Flag=2]',2)                                             
--      with(numWareHouseItmsDTLID numeric(9),                                            
--      numWareHouseItemID numeric(9),                      
--      vcSerialNo varchar(100),                
--      Op_Flag tinyint))   
  
  
  
--declare  @rows as integer            
            
--SELECT @rows=count(*) FROM OPENXML (@hDoc,'/NewDataSet/CusFlds',2)                      
-- WITH (Fld_ID numeric(9),Fld_Value varchar(100),RecId numeric(9))            
                                              
             
--if @rows>0            
--begin            
-- Create table #tempTable ( ID INT IDENTITY PRIMARY KEY,                                                          
-- Fld_ID numeric(9),            
-- Fld_Value varchar(100),            
-- RecId numeric(9),        
-- bitSItems bit                                             
-- )             
-- insert into #tempTable (Fld_ID,Fld_Value,RecId,bitSItems)            
-- SELECT * FROM OPENXML (@hDoc,'/NewDataSet/CusFlds',2)                      
-- WITH (Fld_ID numeric(9),Fld_Value varchar(100),RecId numeric(9),bitSItems bit)               
            
-- delete CFW_Fld_Values_Serialized_Items from CFW_Fld_Values_Serialized_Items C            
-- inner join #tempTable T            
-- on   C.Fld_ID=T.Fld_ID and C.RecId=T.RecId and bitSerialized=bitSItems             
            
-- drop table #tempTable            
                               
-- insert into CFW_Fld_Values_Serialized_Items(Fld_ID,Fld_Value,RecId,bitSerialized)                      
-- select Fld_ID,isnull(Fld_Value,'') as Fld_Value,RecId,bitSItems from(SELECT * FROM OPENXML (@hDoc,'/NewDataSet/CusFlds',2)                      
-- WITH (Fld_ID numeric(9),Fld_Value varchar(100),RecId numeric(9),bitSItems bit)) X             
--end   
--end
--else if @OppType=1
--begin

--update WareHouseItmsDTL set tintStatus=0 where numWareHouseItmsDTLID in (select numWarehouseItmsDTLID from OppWarehouseSerializedItem where numOppItemID=@numOppItemTcode)

--delete from OppWarehouseSerializedItem where numOppItemID=@numOppItemTcode

-- Insert into OppWarehouseSerializedItem(numWarehouseItmsDTLID,numOppID,numOppItemID,numWarehouseItmsID,numQty)                
--  (SELECT numWareHouseItmsDTLID,@numOppID,@numOppItemTcode,numWareHouseItemID,UsedQty                                            
--      FROM OPENXML(@hDoc,'/NewDataSet/SerializedItems',2)                                             
--      with(numWareHouseItmsDTLID numeric(9),                                            
--      numWareHouseItemID numeric(9),UsedQty NUMERIC(9)))
      
--update WareHouseItmsDTL set tintStatus=1 where numWareHouseItmsDTLID in 
--(SELECT numWareHouseItmsDTLID                                            
--      FROM OPENXML(@hDoc,'/NewDataSet/SerializedItems',2)                                             
--      with(numWareHouseItmsDTLID numeric(9),UsedQty NUMERIC(9),TotalQty NUMERIC(9)) WHERE (TotalQty-UsedQty)=0)

--end  
  
--EXEC sp_xml_removedocument @hDoc
GO
/****** Object:  StoredProcedure [dbo].[usp_updateoppserializeditems_BizDoc]    Script Date: 07/26/2008 16:21:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_updateoppserializeditems_BizDoc')
DROP PROCEDURE usp_updateoppserializeditems_BizDoc
GO
CREATE PROCEDURE [dbo].[usp_updateoppserializeditems_BizDoc]  
@numOppItemTcode as numeric(9)=0,  
@numOppID as numeric(9)=0,  
@strItems as text='',
@OppType as tinyint,
@numBizDocID as numeric(9)=0
 
as  
-- STORE PROCEDURE LOGIC IS COMMENTED BECAUSE IT IS NOT USED NOW AND CODE IS NOT RELEVANT TO BIZ NOW.
--declare @hDoc as int                
--EXEC sp_xml_preparedocument @hDoc OUTPUT, @strItems    


--update WareHouseItmsDTL set tintStatus=0 where numWareHouseItmsDTLID in (select numWarehouseItmsDTLID from OppWarehouseSerializedItem where numOppItemID=@numOppItemTcode AND numOppBizDocsId=@numBizDocID)

--delete from OppWarehouseSerializedItem where numOppItemID=@numOppItemTcode AND numOppBizDocsId=@numBizDocID

-- Insert into OppWarehouseSerializedItem(numWarehouseItmsDTLID,numOppID,numOppItemID,numWarehouseItmsID,numQty,numOppBizDocsId)                
--  (SELECT numWareHouseItmsDTLID,@numOppID,@numOppItemTcode,numWareHouseItemID,UsedQty,@numBizDocID                                            
--      FROM OPENXML(@hDoc,'/NewDataSet/SerializedItems',2)                                             
--      with(numWareHouseItmsDTLID numeric(9),                                            
--      numWareHouseItemID numeric(9),UsedQty NUMERIC(9)))
      
--update WareHouseItmsDTL set tintStatus=1 where numWareHouseItmsDTLID in 
--(SELECT numWareHouseItmsDTLID                                            
--      FROM OPENXML(@hDoc,'/NewDataSet/SerializedItems',2)                                             
--      with(numWareHouseItmsDTLID numeric(9),UsedQty NUMERIC(9),TotalQty NUMERIC(9)) WHERE (TotalQty-UsedQty)=0)


  
--EXEC sp_xml_removedocument @hDoc
GO

GO
/****** Object:  StoredProcedure [dbo].[USP_UpdatingInventory]    Script Date: 06/01/2009 23:48:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj       
--exec usp_updatinginventory 0,181193,1,0,1       
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_updatinginventory')
DROP PROCEDURE usp_updatinginventory
GO
CREATE PROCEDURE [dbo].[USP_UpdatingInventory]        
@byteMode as tinyint=0,  
@numWareHouseItemID as numeric(9)=0,  
@Units as INTEGER,
@numWOId AS NUMERIC(9)=0,
@numUserCntID AS NUMERIC(9)=0,
@numOppId AS NUMERIC(9)=0      
as      
BEGIN TRY
BEGIN TRANSACTION
  DECLARE @Description AS VARCHAR(200)
  DECLARE @numItemCode NUMERIC(18)
  DECLARE @numDomain AS NUMERIC(18,0)
  DECLARE @ParentWOID AS NUMERIC(18,0)

  SELECT @ParentWOID=numParentWOID FROM WorkOrder WHERE numWOId=@numWOId
  SELECT @numItemCode=numItemID,@numDomain = numDomainID from WareHouseItems where numWareHouseItemID = @numWareHouseItemID
  
if @byteMode=0  -- Aseeembly Item
begin  
	DECLARE @CurrentAverageCost MONEY
	DECLARE @TotalCurrentOnHand INT
	DECLARE @newAverageCost MONEY
	
	SELECT @CurrentAverageCost = monAverageCost FROM dbo.Item WHERE numItemCode =@numItemCode 
	SELECT @TotalCurrentOnHand = ISNULL((SUM(numOnHand) + SUM(numAllocation)),0) FROM dbo.WareHouseItems WHERE numItemID =@numItemCode 
	PRINT @CurrentAverageCost
	PRINT @TotalCurrentOnHand
	
	
	SELECT @newAverageCost = SUM((numQtyItemsReq * dbo.fn_UOMConversion(UOM.numUOMId,0,IDUOM.numDomainId,IDUOM.numUOMId)) * I.monAverageCost )
	FROM dbo.ItemDetails ID
	LEFT JOIN dbo.Item I ON I.numItemCode = ID.numChildItemID
	LEFT JOIN UOM ON UOM.numUOMId=i.numBaseUnit
	LEFT JOIN UOM IDUOM ON IDUOM.numUOMId=ID.numUOMId
	WHERE numItemKitID = @numItemCode
	PRINT @newAverageCost
	SET @newAverageCost = ((@CurrentAverageCost * @TotalCurrentOnHand)  + (@newAverageCost * @Units)) / (@TotalCurrentOnHand + @Units)
	
	PRINT @newAverageCost

	UPDATE item SET monAverageCost =@newAverageCost WHERE numItemCode = @numItemCode

	IF ISNULL(@numWOId,0) > 0
	BEGIN
		UPDATE WareHouseItems set numOnHand=ISNULL(numOnHand,0)+@Units,numOnOrder= CASE WHEN ISNULL(numOnOrder,0) < @Units THEN 0 ELSE ISNULL(numOnOrder,0)-@Units END,dtModified = GETDATE()  where numWareHouseItemID=@numWareHouseItemID  
	END
	ELSE
	BEGIN
		UPDATE WareHouseItems set numOnHand=ISNULL(numOnHand,0)+@Units,dtModified = GETDATE()  where numWareHouseItemID=@numWareHouseItemID  
	END
end  
else if @byteMode=1  --Aseembly Child item
begin  
	declare @onHand as numeric                                    
	declare @onOrder as numeric                                    
	declare @onBackOrder as numeric                                   
	declare @onAllocation as numeric    

	select                                     
		@onHand=isnull(numOnHand,0),                                    
		@onAllocation=isnull(numAllocation,0),                                    
		@onOrder=isnull(numOnOrder,0),                                    
		@onBackOrder=isnull(numBackOrder,0)                                     
	from 
		WareHouseItems 
	where 
		numWareHouseItemID=@numWareHouseItemID  


	IF ISNULL(@numWOId,0) > 0
	BEGIN
		--RELEASE QTY FROM ALLOCATION
		SET @onAllocation=@onAllocation-@Units
	END
	ELSE
	BEGIN
		--DECREASE QTY FROM ONHAND
		SET @onHand=@onHand-@Units
	END

	 --UPDATE INVENTORY
	UPDATE 
		WareHouseItems 
	SET      
		numOnHand=@onHand,
		numOnOrder=@onOrder,                         
		numAllocation=@onAllocation,
		numBackOrder=@onBackOrder,
		dtModified = GETDATE()                                    
	WHERE 
		numWareHouseItemID=@numWareHouseItemID   
END

IF @numWOId>0
BEGIN
    
	DECLARE @numReferenceID NUMERIC(18,0)
	DECLARE @tintRefType NUMERIC(18,0)

	IF ISNULL(@numOppId,0)>0 --FROM SALES ORDER 
	BEGIN
		IF @byteMode = 0 --Aseeembly Item
			SET @Description = 'SO-WO Work Order Completed (Qty: ' + CONVERT(varchar(10),@Units) + ')'
		ELSE IF @byteMode =1 --Aseembly Child item
			IF ISNULL(@ParentWOID,0) = 0
			BEGIN
				SET @Description = 'Items Used In SO-WO (Qty: ' + CONVERT(varchar(10),@Units) + ')'
			END
			ELSE
			BEGIN
				SET @Description = 'Items Used In SO-WO Work Order (Qty: ' + CONVERT(varchar(10),@Units) + ')'
			END

		SET @numReferenceID = @numOppId
		SET @tintRefType = 3
	END
	ELSE --FROM CREATE ASSEMBLY SCREEN FROM ITEM
	BEGIN
		IF @byteMode = 0 --Aseeembly Item
			SET @Description = 'Work Order Completed (Qty: ' + CONVERT(varchar(10),@Units) + ')'
		ELSE IF @byteMode =1 --Aseembly Child item
			SET @Description = 'Items Used In Work Order (Qty: ' + CONVERT(varchar(10),@Units) + ')'

		SET @numReferenceID = @numWOId
		SET @tintRefType=2
	END

	EXEC dbo.USP_ManageWareHouseItems_Tracking
				@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
				@numReferenceID = @numReferenceID, --  numeric(9, 0)
				@tintRefType = @tintRefType, --  tinyint
				@vcDescription = @Description, --  varchar(100)
				@numModifiedBy = @numUserCntID,
				@numDomainID = @numDomain
END
ELSE if @byteMode=0
BEGIN
DECLARE @desc AS VARCHAR(100)
SET @desc= 'Create Assembly:' + CONVERT(varchar(10),@Units) + ' Units';

  EXEC dbo.USP_ManageWareHouseItems_Tracking
				@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
				@numReferenceID = @numItemCode, --  numeric(9, 0)
				@tintRefType = 1, --  tinyint
				@vcDescription = @desc, --  varchar(100)
				@numModifiedBy = @numUserCntID,
				@numDomainID = @numDomain
END
ELSE if @byteMode=1
BEGIN
SET @Description = 'Item Used in Assembly (Qty: ' + CONVERT(varchar(10),@Units) + ')' 

EXEC dbo.USP_ManageWareHouseItems_Tracking
				@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
				@numReferenceID = @numItemCode, --  numeric(9, 0)
				@tintRefType = 1, --  tinyint
				@vcDescription = @Description, --  varchar(100)
				@numModifiedBy = @numUserCntID,
				@numDomainID = @numDomain
END 
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SET @numOppID = 0

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ValidateBizDocFulfillment')
DROP PROCEDURE USP_ValidateBizDocFulfillment
GO
CREATE PROCEDURE [dbo].[USP_ValidateBizDocFulfillment]
    (
	  @numDomainID NUMERIC(9),
      @numOppID NUMERIC(9),
      @numFromOppBizDocsId NUMERIC(9),
      @numBizDocId NUMERIC(9)
    )
AS 
    BEGIN
   
		DECLARE @numPackingSlipBizDocId NUMERIC(9),@numAuthInvoice NUMERIC(9),@numFulfillmentOrderBizDocId NUMERIC(9)
		
		SELECT @numPackingSlipBizDocId=numListItemID FROM ListDetails WHERE vcData='Packing Slip' AND constFlag=1 AND numListID=27
	    
		Select @numAuthInvoice=isnull(numAuthoritativeSales,0) From AuthoritativeBizDocs Where numDomainId=@numDomainId 
	    
	    SET @numFulfillmentOrderBizDocId=296
	    
	    IF @numFromOppBizDocsId>0
	    BEGIN
			PRINT '1'
	    END
	    ELSE
	    BEGIN
			IF @numFulfillmentOrderBizDocId=@numBizDocId
			BEGIN
				--Only a Sales Order can create a Fulfillment Order (Don�t allow an FO to be created from an Opportunity)
				IF EXISTS (SELECT numOppID FROM OpportunityMaster OM WHERE OM.numDomainId=@numDomainId AND OM.numOppID=@numOppID AND OM.tintOppstatus=0)
				BEGIN
					RAISERROR ('NOT_ALLOWED_FulfillmentOrder',16,1);
					RETURN -1
				END
			END	
		END
    END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_ValidateOppSerializLot')
DROP PROCEDURE usp_ValidateOppSerializLot
GO
CREATE PROCEDURE [dbo].[usp_ValidateOppSerializLot]
    (
      @numOppID NUMERIC(9) = 0
    )
AS 
    BEGIN
   
--    SELECT numoppitemtCode,opp.vcItemName,CASE WHEN SUM(oppI.numQty)=SUM(opp.numUnitHour) THEN 0 ELSE 1 END Error
--FROM OpportunityItems opp JOIN Item i ON i.numItemCode=opp.numItemCode left JOIN OppWarehouseSerializedItem oppI ON (opp.numoppitemtCode=oppI.numOppItemID AND oppI.numOppID=Opp.numOppId)
-- LEFT JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID=whi.numWareHouseItmsDTLID
--where opp.numOppId=@numOppID AND (ISNULL(i.bitSerialized,0)=1 OR ISNULL(i.bitLotNo,0)=1) GROUP BY numoppitemtCode,opp.vcItemName
DECLARE @numOppType TINYINT
DECLARE @numOppStatus TINYINT
DECLARE @bitStockTransfer BIT

SELECT @numOppType=tintOppType, @numOppStatus=tintOppStatus, @bitStockTransfer=bitStockTransfer FROM OpportunityMaster WHERE numOppID =  @numOppID

--Validate serial number in case of sales order and sotck transfer only
If (@numOppType = 1 AND @numOppStatus=1) OR @bitStockTransfer = 1
BEGIN
    SELECT numoppitemtCode,opp.vcItemName,CASE WHEN SUM(opp.numUnitHour)=
    (SELECT SUM(oppI.numQty) FROM OppWarehouseSerializedItem oppI JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID=whi.numWareHouseItmsDTLID WHERE opp.numoppitemtCode=oppI.numOppItemID AND oppI.numOppID=@numOppID) THEN 0 ELSE 1 END Error
FROM OpportunityItems opp JOIN Item i ON i.numItemCode=opp.numItemCode
where opp.numOppId=@numOppID AND (ISNULL(i.bitSerialized,0)=1 OR ISNULL(i.bitLotNo,0)=1) GROUP BY numoppitemtCode,opp.vcItemName
END
ELSE IF (@numOppType = 2 AND @numOppStatus=1)
	SELECT 
		numoppitemtCode,
		opp.vcItemName,
		CASE 
			WHEN SUM(opp.numUnitHour) = (SELECT SUM(oppI.numQty) FROM OppWarehouseSerializedItem oppI JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID=whi.numWareHouseItmsDTLID WHERE opp.numoppitemtCode=oppI.numOppItemID AND oppI.numOppID=@numOppID) 
			THEN 0 
			ELSE 1 
		END Error
	FROM 
		OpportunityItems opp 
	JOIN 
		Item i 
	ON 
		i.numItemCode=opp.numItemCode
	WHERE 
		opp.numOppId=@numOppID 
		AND (ISNULL(i.bitSerialized,0)=1 OR ISNULL(i.bitLotNo,0)=1)
	GROUP BY 
		numoppitemtCode,opp.vcItemName
ELSE
BEGIN
	 SELECT 
		numoppitemtCode,
		opp.vcItemName,
		0 AS Error
	FROM 
		OpportunityItems opp 
	JOIN 
		Item i 
	ON 
		i.numItemCode=opp.numItemCode
	WHERE 
		opp.numOppId=@numOppID AND 
		(ISNULL(i.bitSerialized,0)=1 OR ISNULL(i.bitLotNo,0)=1) 
	GROUP BY 
		numoppitemtCode,opp.vcItemName
END

END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_ValidateOppSerializLot_Name')
DROP PROCEDURE usp_ValidateOppSerializLot_Name
GO
CREATE PROCEDURE [dbo].[usp_ValidateOppSerializLot_Name]
    (
    @numOppItemTcode as numeric(9)=0,  
    @numOppID as numeric(9)=0,  
    @strItems as VARCHAR(1000)='',
    @numBizDocID as numeric(9)=0,
    @numWarehouseItmsID as numeric(9)=0,
    @bitSerialized as bit,
    @bitLotNo as bit
    )
AS 
    BEGIN
 
 -- THIS PROCEDURE IS COMMENTED BECAUSE IT IS NOT USED NOW AND ITS LOGIC IS NOT RELEVENT TO BIZ NOW. SO DO NOT USE IT.

-- SELECT vcSerialNo,  isnull(numQty,0) 
--  - isnull((select sum(w.numQty) from OppWarehouseSerializedItem w INNER JOIN OpportunityMaster 
--  opp ON (w.numOppId=opp.numOppId AND opp.tintOppType=1) where isnull(opp.tintshipped,0)=0 and 
--  w.numWarehouseItmsDTLID=WareHouseItmsDTL.numWarehouseItmsDTLID),0)
--  
--  + isnull((select sum(w.numQty) from OppWarehouseSerializedItem w INNER JOIN OpportunityMaster 
--  opp ON (w.numOppId=opp.numOppId AND opp.tintOppType=1) where isnull(opp.tintshipped,0)=0 and 
--  w.numWarehouseItmsDTLID=WareHouseItmsDTL.numWarehouseItmsDTLID AND numOppBizDocsId=@numBizDocID),0)
--   as TotalQty
--INTO #tempTable
-- from   WareHouseItmsDTL   
--    where (tintStatus is null or tintStatus=0) and  numWareHouseItemID=@numWarehouseItmsID

--Create table #tempTable (                                                                    
-- vcSerialNo VARCHAR(100),TotalQty [numeric](18, 0),numWarehouseItmsDTLID [numeric](18, 0)                                             
-- )                       

--INSERT INTO #tempTable (vcSerialNo,numWarehouseItmsDTLID,TotalQty)  
--            SELECT vcSerialNo,WareHouseItmsDTL.numWareHouseItmsDTLID,
--  isnull(WareHouseItmsDTL.numQty,0) - isnull((select sum(w.numQty) from OppWarehouseSerializedItem w INNER JOIN OpportunityMaster opp ON (w.numOppId=opp.numOppId AND opp.tintOppType=1) where isnull(opp.tintshipped,0)=0 and w.numWarehouseItmsDTLID=WareHouseItmsDTL.numWarehouseItmsDTLID),0) + isnull(OppWarehouseSerializedItem.numQty,0) as TotalQty
-- from   OppWarehouseSerializedItem      
-- join WareHouseItmsDTL      
-- on WareHouseItmsDTL.numWareHouseItmsDTLID= OppWarehouseSerializedItem.numWarehouseItmsDTLID                           
-- where numOppID=@numOppID and  numWareHouseItemID=@numWarehouseItmsID and numOppBizDocsId=@numBizDocID ORDER BY vcSerialNo,TotalQty desc


--INSERT INTO #tempTable (vcSerialNo,numWarehouseItmsDTLID,TotalQty)  
--		 SELECT vcSerialNo,numWareHouseItmsDTLID,
--  isnull(numQty,0) - isnull((select sum(w.numQty) from OppWarehouseSerializedItem w INNER JOIN OpportunityMaster opp ON (w.numOppId=opp.numOppId AND opp.tintOppType=1) where isnull(opp.tintshipped,0)=0 and w.numWarehouseItmsDTLID=WareHouseItmsDTL.numWarehouseItmsDTLID),0) as TotalQty
-- from   WareHouseItmsDTL   
--    where (tintStatus is null or tintStatus=0) and  numWareHouseItemID=@numWarehouseItmsID  
--    and numWareHouseItmsDTLID not in(select numWarehouseItmsDTLID from OppWarehouseSerializedItem where numOppID=@numOppID and  numWarehouseItmsID=@numWarehouseItmsID and numOppBizDocsId=@numBizDocID) ORDER BY vcSerialNo,TotalQty desc

--Create table #tempError (                                                                    
-- vcSerialNo VARCHAR(100),UsedQty [numeric](18, 0),AvailableQty [numeric](18, 0),vcError VARCHAR(100)                                             
-- )                       

  
--DECLARE @posComma int, @strKeyVal varchar(20)

--SET @strItems=RTRIM(@strItems)
--IF RIGHT(@strItems, 1)!=',' SET @strItems=@strItems+','

--SET @posComma=PatIndex('%,%', @strItems)
--WHILE @posComma>1
--BEGIN
--	SET @strKeyVal=ltrim(rtrim(substring(@strItems, 1, @posComma-1)))
	
--	DECLARE @posBStart INT,@posBEnd int, @strQty varchar(20),@strName varchar(20)

--    IF @bitLotNo=1 
--    BEGIN
--		SET @posBStart=PatIndex('%(%', @strKeyVal)
--		SET @posBEnd=PatIndex('%)%', @strKeyVal)
--		IF( @posBStart>1 AND @posBEnd>1)
--			BEGIN
--				SET @strName=ltrim(rtrim(substring(@strKeyVal, 1, @posBStart-1)))
--				SET	@strQty=ltrim(rtrim(substring(@strKeyVal, @posBStart+1,len(@strKeyVal)-@posBStart-1)))
--			END		
			
--		ELSE
--			BEGIN
--				SET @strName=@strKeyVal
--				SET	@strQty=1
--			END
--	END
--	ELSE
--	BEGIN
--			SET @strName=@strKeyVal
--			SET	@strQty=1
--	END  
	
--	PRINT  'Name->' + @strName + ':Qty->' + @strQty
	  
--	  DECLARE @AvailableQty NUMERIC(9)
	  
--	 IF NOT EXISTS(SELECT 1 FROM #tempTable WHERE vcSerialNo=@strName)
--		 INSERT INTO #tempError (vcSerialNo,UsedQty,AvailableQty,vcError) VALUES (@strName,0,0,'Serial / Lot # not available')
--	 ELSE
--	 BEGIN 
--	    SELECT TOP 1 @AvailableQty=TotalQty FROM #tempTable WHERE vcSerialNo=@strName 
	    
--	    IF  (@strQty>@AvailableQty)	 
--		 INSERT INTO #tempError (vcSerialNo,UsedQty,AvailableQty,vcError) VALUES (@strName,@strQty,@AvailableQty,'Used Serial / Lot # more than Available Serial / Lot #')
--	  END
	  
--	SET @strItems=substring(@strItems, @posComma +1, len(@strItems)-@posComma)
--	SET @posComma=PatIndex('%,%',@strItems)
	
--END
----SELECT * FROM #tempTable

--SELECT * FROM #tempError
--DROP TABLE #tempError
--DROP TABLE #tempTable

END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_ValidateSerializLot_Name')
DROP PROCEDURE usp_ValidateSerializLot_Name
GO
CREATE PROCEDURE [dbo].[usp_ValidateSerializLot_Name]
(
    @numItemCode as numeric(9)=0,
    @numWarehouseItmsID as numeric(9)=0,
    @strItems as VARCHAR(3000)='',
    @bitSerialized as bit,
    @bitLotNo as bit
)
AS 
    BEGIN
 
Create table #tempTable 
(                                                                    
	vcSerialNo VARCHAR(100),
	TotalQty [numeric](18,0),
	numWarehouseItmsDTLID [numeric](18, 0)                                             
)                       

INSERT INTO #tempTable 
(
	vcSerialNo,TotalQty,numWarehouseItmsDTLID
)  
SELECT 
	vcSerialNo,numWareHouseItmsDTLID,ISNULL(numQty,0) AS TotalQty
FROM   
	WareHouseItmsDTL   
WHERE 
	numWareHouseItemID=@numWarehouseItmsID  
ORDER BY 
	vcSerialNo,TotalQty desc

Create table #tempError 
(                                                                    
	vcSerialNo VARCHAR(100),
	UsedQty [numeric](18, 0),
	AvailableQty [numeric](18, 0),
	vcError VARCHAR(100)                                             
)                       

  
DECLARE @posComma int, @strKeyVal varchar(20)

SET @strItems=RTRIM(@strItems)
IF RIGHT(@strItems, 1)!=',' SET @strItems=@strItems+','

SET @posComma=PatIndex('%,%', @strItems)
WHILE @posComma>1
BEGIN
	SET @strKeyVal=ltrim(rtrim(substring(@strItems, 1, @posComma-1)))
	
	DECLARE @posBStart INT,@posBEnd int, @strQty varchar(20),@strName varchar(20)

    IF @bitLotNo=1 
    BEGIN
		SET @posBStart=PatIndex('%(%', @strKeyVal)
		SET @posBEnd=PatIndex('%)%', @strKeyVal)
		IF( @posBStart>1 AND @posBEnd>1)
			BEGIN
				SET @strName=ltrim(rtrim(substring(@strKeyVal, 1, @posBStart-1)))
				SET	@strQty=ltrim(rtrim(substring(@strKeyVal, @posBStart+1,len(@strKeyVal)-@posBStart-1)))
			END		
			
		ELSE
			BEGIN
				SET @strName=@strKeyVal
				SET	@strQty=1
			END
	END
	ELSE
	BEGIN
			SET @strName=@strKeyVal
			SET	@strQty=1
	END  
	
	PRINT  'Name->' + @strName + ':Qty->' + @strQty
	  
	  DECLARE @AvailableQty NUMERIC(9)
	  
	 IF NOT EXISTS(SELECT 1 FROM #tempTable WHERE vcSerialNo=@strName)
		 INSERT INTO #tempError (vcSerialNo,UsedQty,AvailableQty,vcError) VALUES (@strName,@strQty,0,'Serial / Lot # not available')
	 ELSE
	 BEGIN 
	    SELECT TOP 1 @AvailableQty=TotalQty FROM #tempTable WHERE vcSerialNo=@strName 
	    
	    IF  (@strQty>@AvailableQty)	 
		 INSERT INTO #tempError (vcSerialNo,UsedQty,AvailableQty,vcError) VALUES (@strName,@strQty,@AvailableQty,'Used Serial / Lot # more than Available Serial / Lot #')
	  END
	  
	SET @strItems=substring(@strItems, @posComma +1, len(@strItems)-@posComma)
	SET @posComma=PatIndex('%,%',@strItems)
	
END
--SELECT * FROM #tempTable

SELECT * FROM #tempError
DROP TABLE #tempError
DROP TABLE #tempTable

END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_VerifySerialLot')
DROP PROCEDURE USP_VerifySerialLot
GO
CREATE PROCEDURE [dbo].[USP_VerifySerialLot]  
	@numOppItemID NUMERIC(18,0),
	@numWarehouseItemID NUMERIC(18,0),
	@vcSerialLot VARCHAR(100),
	@bitLotNo BIT 
AS  
BEGIN 
	IF @bitLotNo = 1 --LOT ITEM
	BEGIN
		DECLARE @TempLot TABLE
		(
			vcLotNo VARCHAR(100),
			numQty INT 
		)

		DECLARE @vcLotNo AS VARCHAR(200) = ''
		DECLARE @numQty AS VARCHAR(200) = 0

		SELECT
			@vcLotNo = SUBSTRING(OutParam,0,PATINDEX('%(%', @vcSerialLot)),
			@numQty = CAST(SUBSTRING(OutParam,(PATINDEX('%(%', @vcSerialLot) + 1),(PATINDEX('%)%', @vcSerialLot) - PATINDEX('%(%', @vcSerialLot) - 1)) AS INT)
		FROM
			dbo.SplitString(@vcSerialLot,',')


		IF EXISTS (SELECT numWareHouseItmsDTLID FROM WareHouseItmsDTL WHERE numWareHouseItemID=@numWarehouseItemID AND vcSerialNo = @vcLotNo AND ISNULL(numQty,0) >= @numQty)
		BEGIN
			SELECT 1
		END
		ELSE
		BEGIN
			SELECT 0
		END
	END
	ELSE --SERIALIZED ITEM
	BEGIN
		IF EXISTS (SELECT numWareHouseItmsDTLID FROM WareHouseItmsDTL WHERE numWareHouseItemID=@numWarehouseItemID AND vcSerialNo = @vcSerialLot AND ISNULL(numQty,0) > 0)
		BEGIN
			SELECT 1
		END
		ELSE
		BEGIN
			SELECT 0
		END
	END
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
         
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_WareHouseItmsDTL_TransferSerialLotNo')
DROP PROCEDURE USP_WareHouseItmsDTL_TransferSerialLotNo
GO
CREATE PROCEDURE [dbo].[USP_WareHouseItmsDTL_TransferSerialLotNo]            
@numOppID AS NUMERIC(18,0),
@numOppItemID AS numeric(18,0),
@numFormWarehouseID AS NUMERIC(18,0),
@numToWarehouseID AS NUMERIC(18,0),
@bitSerialized AS BIT,
@bitLotNo AS BIT     
AS
BEGIN TRY
BEGIN TRANSACTION 
	
	IF @bitSerialized = 1
	BEGIN
		--TRANSFER SERIAL NUMBERS TO SELECTED WAREHOUSE
		UPDATE 
			WareHouseItmsDTL 
		SET 
			numWareHouseItemID=@numToWarehouseID,
			numQty=1
		WHERE 
			numWareHouseItmsDTLID IN 
			(
				SELECT 
					t1.numWarehouseItmsDTLID
				FROM 
					OppWarehouseSerializedItem t1
				WHERE 
					t1.numOppID=@numOppID AND
					t1.numOppItemID=@numOppItemID AND
					t1.numWarehouseItmsID = @numFormWarehouseID)
	END

	IF @bitLotNo = 1
	BEGIN
		DECLARE @TableLotNoSelected TABLE
		(
			ID INT IDENTITY(1,1),
			numWarehouseItmsDTLID NUMERIC(18,0),
			numOppID NUMERIC(18,0),
			numOppItemID NUMERIC(18,0),
			numWarehouseItmsID NUMERIC(18,0),
			numQty NUMERIC(18,0)
		)
		
		INSERT INTO 
			@TableLotNoSelected 
		SELECT 
			numWarehouseItmsDTLID,
			numOppID,
			numOppItemID,
			numWarehouseItmsID,
			numQty
		FROM 
			OppWarehouseSerializedItem 
		WHERE 
			numOppID=@numOppID 
			AND numOppItemID=@numOppItemID 
			AND numWarehouseItmsID = @numFormWarehouseID

		DECLARE @i INT = 1
		DECLARE @Count INT
		SELECT @Count=COUNT(*) FROM @TableLotNoSelected

		WHILE @i <= @Count
		BEGIN
			DECLARE @numWarehouseItmsDTLID AS NUMERIC(18,0)
			DECLARE @vcSerialNo AS VARCHAR(100)
			DECLARE @dExpiryDate AS DATETIME
			DECLARE @numQty AS INT

			SELECT @numQty=numQty,@numWarehouseItmsDTLID=numWarehouseItmsDTLID FROM @TableLotNoSelected WHERE ID= @i
			SELECT @vcSerialNo=vcSerialNo,@dExpiryDate=dExpirationDate FROM WareHouseItmsDTL WHERE numWareHouseItmsDTLID=@numWarehouseItmsDTLID

			--IF SAME LOT NUMBER IS ALREADY AVAILABLE IN DESTINATION WAREHOUSE THEN UPDATE QUANITY ELSE CREATE NEW ENTRY
			IF EXISTS (SELECT * FROM WareHouseItmsDTL WHERE numWareHouseItemID = @numToWarehouseID AND vcSerialNo=@vcSerialNo)
			BEGIN
				UPDATE
					WareHouseItmsDTL
				SET
					numQty = ISNULL(numQty,0) + ISNULL(@numQty,0)
				WHERE
					numWareHouseItemID = @numToWarehouseID AND vcSerialNo=@vcSerialNo
			END
			ELSE
			BEGIN
				INSERT INTO WareHouseItmsDTL
				(
					numWareHouseItemID,
					vcSerialNo,
					numQty,
					dExpirationDate
				)
				VALUES
				(
					@numToWarehouseID,
					@vcSerialNo,
					@numQty,
					@dExpiryDate
				)
			END

			SET @i = @i + 1
		END 
	END


	--UPDATE TRANSFER COMPLETES SO USERS WILL BE ABLE TO USE SERIAL/LOT NUMBR OTHERWISE USER WILL NOT BE ABLE TO SELECT SERIAL/LOT NUMBR
	UPDATE OppWarehouseSerializedItem SET bitTransferComplete=1 WHERE numOppID=@numOppID AND numOppItemID=@numOppItemID AND numWarehouseItmsID = @numFormWarehouseID

COMMIT
END TRY
BEGIN CATCH
  -- Whoops, there was an error
  IF @@TRANCOUNT > 0
     ROLLBACK

  -- Raise an error with the details of the exception
  DECLARE @ErrMsg nvarchar(4000), @ErrSeverity int
  SELECT @ErrMsg = ERROR_MESSAGE(),
         @ErrSeverity = ERROR_SEVERITY()

  RAISERROR(@ErrMsg, @ErrSeverity, 1)
END CATCH

GO
