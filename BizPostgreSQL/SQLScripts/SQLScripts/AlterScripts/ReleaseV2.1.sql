/******************************************************************
Project: Release 2.1 Date: 01.10.2013
Comments: 
*******************************************************************/

/*******************Kamal Script******************/
--/************************************************************************************************/
--/************************25_Sep_2013*******************************************************************/
--/************************************************************************************************/

--Add Item & WareHouse into Opportunity Items
INSERT INTO [dbo].[ReportModuleGroupFieldMappingMaster]([numReportModuleGroupID], [numReportFieldGroupID])
SELECT 10, 7 UNION ALL
SELECT 10, 11 


--Financial Transaction Search (frmAddAcc.aspx)
UPDATE PageMaster SET bitIsExportApplicable=1 WHERE numModuleID=9 AND numPageID=12


--numIncomeChartAcntId,numAssetChartAcntId,numCOGsChartAcntId
INSERT INTO [dbo].[ReportFieldGroupMappingMaster]([numReportFieldGroupID], [numFieldID], [vcFieldName], 
[vcAssociatedControlType], [vcFieldDataType], [bitAllowSorting], [bitAllowGrouping], [bitAllowAggregate], [bitAllowFiltering])
SELECT 7, numFieldId, vcFieldName, vcAssociatedControlType, vcFieldDataType, 0, 0, 0, 0 FROM DycFieldMaster 
WHERE numModuleID=4 AND numFieldId IN (270,271,272)



--Primary Vendor
INSERT INTO [dbo].[ReportFieldGroupMappingMaster]([numReportFieldGroupID], [numFieldID], [vcFieldName], 
[vcAssociatedControlType], [vcFieldDataType], [bitAllowSorting], [bitAllowGrouping], [bitAllowAggregate], [bitAllowFiltering])
SELECT 7, numFieldId, vcFieldName, vcAssociatedControlType, vcFieldDataType, 0, 0, 0, 0 FROM DycFieldMaster 
WHERE numModuleID=4 AND numFieldId IN (289,290,291,349)

--Vendor to Primary Vendor
UPDATE ReportFieldGroupMappingMaster SET vcFieldName='Primary Vendor' WHERE numReportFieldGroupID=7 AND numFieldID=349
 
 
ALTER TABLE dbo.ReportListMaster ADD
	bitHideSummaryDetail bit NULL 
	
	
	
--====Set bitAllowAggregate=0===

UPDATE ReportFieldGroupMappingMaster SET bitAllowAggregate=0 WHERE vcFieldDataType='V'	

--SELECT * FROM dbo.ReportFieldGroupMappingMaster RFGM JOIN dbo.DycFieldMaster DFM ON RFGM.numFieldID = DFM.numFieldId
-- WHERE bitAllowAggregate=1 AND ISNULL(DFM.vcListItemType,'')!=''

UPDATE RFGM SET bitAllowAggregate=0 FROM dbo.ReportFieldGroupMappingMaster RFGM JOIN dbo.DycFieldMaster DFM ON RFGM.numFieldID = DFM.numFieldId
 WHERE bitAllowAggregate=1 AND ISNULL(DFM.vcListItemType,'')!=''


--SELECT * FROM dbo.ReportFieldGroupMappingMaster RFGM JOIN dbo.DycFieldMaster DFM ON RFGM.numFieldID = DFM.numFieldId
-- WHERE bitAllowAggregate=1 AND ISNULL(RFGM.vcFieldDataType,'') IN ('D','Y')
 
 
 UPDATE RFGM SET bitAllowAggregate=0 FROM dbo.ReportFieldGroupMappingMaster RFGM JOIN dbo.DycFieldMaster DFM ON RFGM.numFieldID = DFM.numFieldId
 WHERE bitAllowAggregate=1 AND ISNULL(RFGM.vcFieldDataType,'') IN ('D','Y')

--Item ID,Opp Type,Project
 UPDATE RFGM SET bitAllowAggregate=0 FROM dbo.ReportFieldGroupMappingMaster RFGM 
 WHERE bitAllowAggregate=1 AND numFieldID IN (118,211,263)
