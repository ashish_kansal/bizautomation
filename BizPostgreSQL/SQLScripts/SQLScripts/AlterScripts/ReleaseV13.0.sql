/******************************************************************
Project: Release 13.0 Date: 02.DEC.2019
Comments: ALTER SCRIPTS
*******************************************************************/


/******************************************** SANDEEP *********************************************/

USE [Production.2014]
GO

/****** Object:  Table [dbo].[RecordOrganizationChangeHistory]    Script Date: 29-Nov-19 10:24:12 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[RecordOrganizationChangeHistory](
	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numDomainID] [numeric](18, 0) NOT NULL,
	[numOppID] [numeric](18, 0) NULL,
	[numProjectID] [numeric](18, 0) NULL,
	[numCaseID] [numeric](18, 0) NULL,
	[numOldDivisionID] [numeric](18, 0) NOT NULL,
	[numNewDivisionID] [numeric](18, 0) NOT NULL,
	[numModifiedBy] [numeric](18, 0) NOT NULL,
	[dtModified] [datetime] NOT NULL,
 CONSTRAINT [PK_RecordOrganizationChangeHistory] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO



---------------------------------------------

ALTER TABLE SalesOrderConfiguration ADD bitDisplayItemGridItemPromotion BIT
ALTER TABLE SalesOrderConfiguration ADD bitDisplayApplyPromotionCode BIT
ALTER TABLE SalesOrderConfiguration ADD bitDisplayCreateOrderStartNewButton BIT
ALTER TABLE SalesOrderConfiguration ADD bitDisplayCreateOpenUnpaidInvoiceButton BIT
ALTER TABLE SalesOrderConfiguration ADD bitDisplayPayButton BIT
ALTER TABLE SalesOrderConfiguration ADD bitDisplayPOSPay BIT

------------------------------------------------

DELETE FROM TabMaster WHERE numTabId IN (76,103) AND bitFixed=1

UPDATE TabMaster SET numTabName='Opportunities & Orders',Remarks='Opportunities & Orders' WHERE numTabId=1
--------------------------------------------------

UPDATE BizFormWizardModule SET vcModule='Projects, Cases & Activities' WHERE numBizFormModuleID=3
UPDATE BizFormWizardModule SET vcModule='Web to Lead Forms' WHERE numBizFormModuleID=7
UPDATE BizFormWizardModule SET vcModule='Organizations (Custom Relationships)' WHERE numBizFormModuleID=9
UPDATE BizFormWizardModule SET vcModule='Activities' WHERE numBizFormModuleID=11

----------------------------------------------------------

ALTER TABLE BizFormWizardModule ADD tintSortOrder INT

---------------------------------------------------------------------

UPDATE BizFormWizardModule SET tintSortOrder=1 WHERE numBizFormModuleID=1
UPDATE BizFormWizardModule SET tintSortOrder=2 WHERE numBizFormModuleID=2
UPDATE BizFormWizardModule SET tintSortOrder=3 WHERE numBizFormModuleID=9
UPDATE BizFormWizardModule SET tintSortOrder=4 WHERE numBizFormModuleID=3
UPDATE BizFormWizardModule SET tintSortOrder=5 WHERE numBizFormModuleID=11
UPDATE BizFormWizardModule SET tintSortOrder=6 WHERE numBizFormModuleID=4
UPDATE BizFormWizardModule SET tintSortOrder=7 WHERE numBizFormModuleID=5
UPDATE BizFormWizardModule SET tintSortOrder=8 WHERE numBizFormModuleID=6
UPDATE BizFormWizardModule SET tintSortOrder=9 WHERE numBizFormModuleID=7
UPDATE BizFormWizardModule SET tintSortOrder=10 WHERE numBizFormModuleID=8
UPDATE BizFormWizardModule SET tintSortOrder=11 WHERE numBizFormModuleID=10

----------------------------------------------------------------------

UPDATE DycFieldMaster SET vcListItemType='LI',numListID=18 WHERE numFieldId IN (7,116)
UPDATE DycFieldMaster SET vcListItemType='LI',numListID=18 WHERE vcOrigDbColumnName='numCampaignID' AND numListID=24


UPDATE DycFormField_Mapping SET vcFieldName='Campaign' WHERE numFieldId IN (SELECT ISNULL(numFieldID,0) FROM DycFieldMaster WHERE ISNULL(vcOrigDbColumnName,'')='vcHow')
UPDATE DycFieldMaster SET vcFieldName='Campaign' WHERE vcOrigDbColumnName='vcHow'


DELETE FROM BizFormWizardMasterConfiguration WHERE numFormID IN (SELECT numFormID FROM DycFormField_Mapping WHERE vcFieldName='Campaign' GROUP BY numFormID,vcFieldName HAVING COUNT(*) > 1) AND numFieldID IN (7,116)
DELETE FROM DycFormConfigurationDetails WHERE numFormID IN (SELECT numFormID FROM DycFormField_Mapping WHERE vcFieldName='Campaign' GROUP BY numFormID,vcFieldName HAVING COUNT(*) > 1) AND numFieldID IN (7,116)
DELETE FROM DycFormField_Mapping WHERE numFormID IN (SELECT numFormID FROM DycFormField_Mapping WHERE vcFieldName='Campaign' GROUP BY numFormID,vcFieldName HAVING COUNT(*) > 1) AND numFieldID IN (7,116)

UPDATE DynamicFormField_Validation SET vcNewFormFieldName='Campaign' WHERE numFormFieldId IN (SELECT ISNULL(numFieldID,0) FROM DycFieldMaster WHERE ISNULL(vcOrigDbColumnName,'')='vcHow')
UPDATE DynamicFormField_Validation SET vcNewFormFieldName='Campaign' WHERE numFormFieldId IN (SELECT ISNULL(numFieldID,0) FROM DycFieldMaster WHERE ISNULL(vcOrigDbColumnName,'')='vcHow')


DELETE FROM TreeNavigationAuthorization WHERE numPageNavID IN (SELECT numPageNavID FROM PageNavigationDTL WHERE numParentID=16)
DELETE FROM PageNavigationDTL WHERE numParentID=16
UPDATE PageNavigationDTL SET vcAddURL='',bitAddIsPopUp=0,vcNavURL='../Marketing/frmCampaignList.aspx' WHERE numPageNavID=16

--------------------------------


INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,tintROw,tintColumn,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
)
VALUES
(
	3,274,141,0,0,'Item Group','SelectBox',38,1,38,1,0,0,1,1,1
)

---------------------------------

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,[order],bitInResults,bitDeleted,bitDefault,bitImport,bitAllowFiltering,intSectionID
)
SELECT
	numModuleID,numFieldID,20,bitAllowEdit,bitInlineEdit,'Re-Order Point',vcAssociatedControlType,vcPropertyName,PopupFunctionName,[order],bitInResults,bitDeleted,bitDefault,bitImport,bitAllowFiltering,intSectionID
FROM 
	DycFormField_Mapping
WHERE 
	numFormID=48 AND numFieldID=200

----------------------------------

ALTER TABLE OpportunityMaster ADD numProjectID NUMERIC(18,0)

-----------------------------------

DECLARE @numFieldID NUMERIC(18,0)

INSERT INTO [dbo].[DycFieldMaster]
(
	[numModuleID],[numDomainID],[vcFieldName],[vcDbColumnName],[vcOrigDbColumnName],[vcPropertyName],[vcLookBackTableName],[vcFieldDataType],[vcFieldType],[vcAssociatedControlType],[vcToolTip],[vcListItemType],[numListID],[PopupFunctionName],[order],[tintRow],[tintColumn],[bitInResults],[bitDeleted],[bitAllowEdit],[bitDefault],[bitSettingField],[bitAddField],[bitDetailField],[bitAllowSorting],[bitWorkFlowField],[bitImport],[bitExport],[bitAllowFiltering],[bitInlineEdit],[bitRequired],[intColumnWidth],[intFieldMaxLength],[intWFCompare],[vcGroup],[vcWFCompareField]
)     
SELECT 
	[numModuleID],[numDomainID],[vcFieldName],[vcDbColumnName],[vcOrigDbColumnName],'ProjectID','OpportunityMaster',[vcFieldDataType],[vcFieldType],[vcAssociatedControlType],[vcToolTip],[vcListItemType],[numListID],[PopupFunctionName],[order],[tintRow],[tintColumn],[bitInResults],[bitDeleted],[bitAllowEdit],[bitDefault],[bitSettingField],[bitAddField],[bitDetailField],[bitAllowSorting],[bitWorkFlowField],[bitImport],[bitExport],[bitAllowFiltering],[bitInlineEdit],[bitRequired],[intColumnWidth],[intFieldMaxLength],[intWFCompare],[vcGroup],[vcWFCompareField] 
FROM 
	DycFieldMaster 
WHERE 
	vcDbColumnName='numProjectID' 
	AND vcLookBackTableName='OpportunityItems'

SET @numFieldID = SCOPE_IDENTITY()

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering
)
VALUES
(
	3,@numFieldID,39,1,1,'Project','SelectBox',1,0,0,1,0,1,1,1
)




/******************************************** PRASANT *********************************************/

ALTER TABLE BizFormWizardMasterConfiguration ADD numFormFieldGroupId NUMERIC(18,0) DEFAULT 0

----------------------------------

CREATE TABLE [dbo].[FormFieldGroupConfigurarion](
	[numFormFieldGroupId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numDomainId] NUMERIC(18,0) NOT NULL,
	[numFormId] [numeric](18, 0) NULL,
	[numGroupId] [numeric](18, 0) NULL,
	[vcGroupName] [varchar](500) NULL,
 CONSTRAINT [PK_FormFieldGroupConfigurarion] PRIMARY KEY CLUSTERED 
(
	[numFormFieldGroupId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO