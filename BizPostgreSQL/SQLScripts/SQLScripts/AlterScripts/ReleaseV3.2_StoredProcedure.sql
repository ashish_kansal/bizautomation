SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_AdvancedSearchFinancialSearch')
DROP PROCEDURE USP_AdvancedSearchFinancialSearch
GO
CREATE PROCEDURE [dbo].[USP_AdvancedSearchFinancialSearch] 
@WhereCondition as varchar(4000)='',                              
@ViewID as tinyint,                              
@numDomainID as numeric(9)=0,                              
@numUserCntID as numeric(9)=0,                              
@numGroupID as numeric(9)=0,                              
@CurrentPage int,                                                                    
@PageSize int,                                                                    
@TotRecs int output,                                                                                                             
@columnSortOrder as Varchar(10),                              
@ColumnName as varchar(20)='', 
@SortCharacter as char(1),                        
@SortColumnName as varchar(20)='',  
@LookTable as varchar(20)='' ,        
@GetAll as bit                            
as                               
DECLARE  @tintOrder  AS TINYINT
DECLARE  @vcFormFieldName  AS VARCHAR(50)
DECLARE  @vcListItemType  AS VARCHAR(3)
DECLARE  @vcListItemType1  AS VARCHAR(3)
DECLARE  @vcAssociatedControlType VARCHAR(10)
DECLARE  @numListID  AS NUMERIC(9)
DECLARE  @vcDbColumnName VARCHAR(20)
declare @ColumnSearch as varchar(10)
DECLARE  @vcLookBackTableName VARCHAR(50)
DECLARE  @WCondition VARCHAR(1000)

if (@SortCharacter<>'0' and @SortCharacter<>'') set @ColumnSearch=@SortCharacter   

CREATE TABLE #temptable (
  id           INT   IDENTITY   PRIMARY KEY,
  numJournalId NUMERIC(18),numTransactionId NUMERIC(18))

declare @strSql as varchar(8000)                                                              
 set  @strSql='Select GJD.numJournalId,GJD.numTransactionId                    
  FROM General_Journal_Header GJH
  JOIN General_Journal_Details GJD on GJH.numJournal_Id=GJD.numJournalId
   LEFT OUTER JOIN (DivisionMaster DM                                               
   JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId) ON GJD.numCustomerId = DM.numDivisionID    
  LEFT OUTER JOIN CheckHeader CH ON (GJH.numCheckHeaderID = CH.numCheckHeaderID or (GJH.numBillPaymentID=CH.numReferenceID and CH.tintReferenceType=8))' 

  if (@ColumnName<>'' and  @ColumnSearch<>'')                        
  begin                          
	select @vcListItemType=vcListItemType,@numListID=numListID from View_DynamicDefaultColumns where vcDbColumnName=@ColumnName and numFormID=1 and numDomainId=@numDomainId                         
    
    if @vcListItemType='LI'                               
    begin                      
		set @strSql= @strSql +' left Join ListDetails L1 on L1.numListItemID='+@ColumnName                              
    end                              
  end       
           
                      
  if (@SortColumnName<>'')                        
  begin                          
	select @vcListItemType1=vcListItemType,@numListID=numListID from View_DynamicDefaultColumns where vcDbColumnName=@SortColumnName and numFormID=1 and numDomainId=@numDomainId                         
    
    if @vcListItemType1='LI'                   
    begin     
		set @strSql= @strSql +' left Join ListDetails L2 on L2.numListItemID='+@SortColumnName                              
	END
  end    
                              
  set @strSql=@strSql+' where GJH.numDomainID  = '+convert(varchar(15),@numDomainID)+   @WhereCondition                                  
                 
if (@ColumnName<>'' and  @ColumnSearch<>'')                         
begin                          
  if @vcListItemType='LI'                               
      set @strSql= @strSql +' and L1.vcData like '''+@ColumnSearch +'%'''                             
    else  
		set @strSql= @strSql +' and '+@ColumnName+' like '''+@ColumnSearch  +'%'''                            
END
                          
   if (@SortColumnName<>'')                        
  begin                           
    if @vcListItemType1='LI'                               
		begin                                
		  set @strSql= @strSql +' order by L2.vcData '+@columnSortOrder                              
		end                              
    else  
       BEGIN     
			set @strSql= @strSql +' order by '+ @SortColumnName +' ' +@columnSortOrder                            
        END  
  END
  ELSE 
	  set @strSql= @strSql +' order by  GJH.datCreatedDate asc '
	  
	  
    PRINT @strSql

	INSERT INTO #temptable (numJournalId,numTransactionId)
	EXEC( @strSql)
	
	SET @strSql = ''


set @tintOrder=0;
set @WhereCondition ='';
set @strSql='select numDomainId,
                numAccountId,
                TransactionType,
                CompanyName,
                dbo.[FormatedDateFromDate](datEntry_Date,numDomainID) Date,
                varDescription,
                ISNULL(BizPayment,'''') + '' '' + ISNULL(CheqNo,'''') AS Narration,
                BizDocID,
                TranRef,
                TranDesc,
                CONVERT(VARCHAR(20),ISNULL(numDebitAmt,0)) numDebitAmt,
                CONVERT(VARCHAR(20),ISNULL(numCreditAmt,0)) numCreditAmt,
                vcAccountName,
                '''' balance,
                VG.numJournal_Id AS JournalId,
				numCheckHeaderID AS CheckId,
				numCashCreditCardId AS CashCreditCardId,
				VG.numTransactionId,
				numOppId,
				numOppBizDocsId,
				numDepositId,
				numCategoryHDRID,
				tintTEType,
				numCategory,
				dtFromDate,
				numUserCntID,bitReconcile,bitCleared,numBillID,numBillPaymentID,dbo.fn_GetListItemName(numClassID) as vcClass,
				VG.numLandedCostOppId '                              
                                                                           
  declare @firstRec as integer                                                                    
  declare @lastRec as integer                                                                    
 set @firstRec= (@CurrentPage-1) * @PageSize                      
 set @lastRec= (@CurrentPage*@PageSize+1)                                                                     
set @TotRecs=(select count(*) from #tempTable)                                                   
                                          
if @GetAll=0         
begin        
	set @strSql=@strSql+'FROM VIEW_GENERALLEDGER VG                       
	   '+@WhereCondition+' inner join #tempTable T on T.numJournalId=VG.numJournal_Id and T.numTransactionId=VG.numTransactionId                                                               
	  WHERE VG.numDomainID  = '+convert(varchar(15),@numDomainID) + ' AND ID > '+convert(varchar(10),@firstRec)+ ' and ID <'+ convert(varchar(10),@lastRec) + ' order by T.ID' 
end                           
 else                              
     set @strSql=@strSql+'FROM VIEW_GENERALLEDGER VG                      
  '+@WhereCondition+' join #tempTable T on T.numJournalId=VG.numJournal_Id and T.numTransactionId=VG.numTransactionId 
  WHERE VG.numDomainID  = '+convert(varchar(15),@numDomainID)            
      
print @strSql                                                      
exec(@strSql) 
drop table #tempTable

/****** Object:  StoredProcedure [dbo].[USP_AdvancedSearchOpp]    Script Date: 07/26/2008 16:14:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_advancedsearchopp')
DROP PROCEDURE usp_advancedsearchopp
GO
Create PROCEDURE [dbo].[USP_AdvancedSearchOpp]
@WhereCondition as varchar(4000)='',
@ViewID as tinyint,
@numDomainID as numeric(9)=0,
@numUserCntID as numeric(9)=0,
@numGroupID as numeric(9)=0,
@CurrentPage int,
@PageSize int,                                                                  
@TotRecs int output,                                                                                                           
@columnSortOrder as Varchar(10),    
@ColumnName as varchar(50)='',    
@SortCharacter as char(1),                
@SortColumnName as varchar(50)='',    
@LookTable as varchar(10)='',
@strMassUpdate as varchar(2000)=''
  
as   
  
	  
declare @tintOrder as tinyint                                  
declare @vcFormFieldName as varchar(50)                                  
declare @vcListItemType as varchar(3)                             
declare @vcListItemType1 as varchar(3)                                 
declare @vcAssociatedControlType varchar(10)                                  
declare @numListID AS numeric(9)                                  
declare @vcDbColumnName varchar(30)                                   
declare @ColumnSearch as varchar(10)
DECLARE @vcLookBackTableName VARCHAR(50)
DECLARE @bitIsSearchBizDoc BIT




IF CHARINDEX('OpportunityBizDocs', @WhereCondition) > 0
begin

	SET @bitIsSearchBizDoc = 1
	end
ELSE 
 BEGIN
	SET @bitIsSearchBizDoc = 0
	
 END
 --Added By Sachin Sadhu||Date:12thJune2014
  --Purpose:To add Functionaly :Search with Notes Field-OpportunityBizDocItems
  --Set Manually To fullfill "All" Selection in Search
 IF CHARINDEX('vcNotes', @WhereCondition) > 0
	BEGIN
			SET 	@bitIsSearchBizDoc = 1
	END
--End of script

if (@SortCharacter<>'0' and @SortCharacter<>'') set @ColumnSearch=@SortCharacter   
Create table #tempTable ( ID INT IDENTITY PRIMARY KEY,                                                                         
      numOppID varchar(15),
      numOppBizDocID varchar(15)
 )  
  
declare @strSql as varchar(8000)
 set  @strSql='select OppMas.numOppId '
 
IF @bitIsSearchBizDoc = 1 --Added to eliminate duplicate records 
	SET @strSql = @strSql + ' ,OpportunityBizDocs.numOppBizDocsId '
ELSE 
	SET @strSql = @strSql + ' ,0 numOppBizDocsId '

IF @SortColumnName ='CalAmount'
BEGIN
	ALTER TABLE #tempTable ADD CalAmount money ;
	SET @strSql = @strSql + ' ,[dbo].[getdealamount](OppMas.numOppId,Getutcdate(),0) as CalAmount'
END	

IF @SortColumnName ='monDealAmount'
BEGIN
	ALTER TABLE #tempTable ADD monDealAmount money ;

	SET @strSql = @strSql + ' ,ISNULL((SELECT SUM(ISNULL(BD.monDealAmount,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = OppMas.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0) as monDealAmount'
END	

IF @SortColumnName ='monAmountPaid'
BEGIN
	ALTER TABLE #tempTable ADD monAmountPaid money ;

	SET @strSql = @strSql + ' ,ISNULL((SELECT SUM(ISNULL(BD.monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = OppMas.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0) AS monAmountPaid'
END	

SET @strSql = @strSql + ' from   
OpportunityMaster OppMas   
Join AdditionalContactsinformation ADC on OppMas.numContactId = ADC.numContactId  
join DivisionMaster DM on Dm.numDivisionId = OppMas.numDivisionId  
JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId   
'  
IF @bitIsSearchBizDoc = 1 --Added to eliminate duplicate records 
BEGIN	
	IF CHARINDEX('vcNotes', @WhereCondition) > 0
		BEGIN
	  		SET @strSql = @strSql + ' left join OpportunityBizDocs on OppMas.numOppId = OpportunityBizDocs.numOppId left join OpportunityBizDocItems on OpportunityBizDocItems.numOppBizDocID=OpportunityBizDocs.numOppBizDocsId '
		END
	ELSE
		BEGIN
			SET @strSql = @strSql + ' left join OpportunityBizDocs on OppMas.numOppId = OpportunityBizDocs.numOppId '
		END


END
	
IF @SortColumnName ='numBillCountry' OR @SortColumnName ='numBillState'
     OR @ColumnName ='numBillCountry' OR @ColumnName ='numBillState'
BEGIN 	
	set @strSql= @strSql +' left Join AddressDetails AD1 on AD1.numRecordId= DM.numDivisionID and AD1.tintAddressOf=2 and AD1.tintAddressType=1 AND AD1.bitIsPrimary=1 and AD1.numDomainID= ADC.numDomainID '
END	
ELSE IF @SortColumnName ='numShipCountry' OR @SortColumnName ='numShipState'
     OR @ColumnName ='numShipCountry' OR @ColumnName ='numShipState'
BEGIN
	set @strSql= @strSql +' left Join AddressDetails AD2 on AD2.numRecordId= DM.numDivisionID and AD2.tintAddressOf=2 and AD2.tintAddressType=2 AND AD2.bitIsPrimary=1 and AD2.numDomainID= ADC.numDomainID '
END

   if (@SortColumnName<>'')                        
  begin                              
 select top 1 @vcListItemType1=vcListItemType from View_DynamicDefaultColumns where vcDbColumnName=@SortColumnName and numFormID=15 and numDomainId=@numDomainId
  if @vcListItemType1='LI'                       
  begin
			IF @SortColumnName ='numBillCountry'
			BEGIN
					set @strSql= @strSql +' left Join ListDetails L2 on L2.numListItemID=AD1.numCountry' 
			END
			ELSE IF @SortColumnName ='numShipCountry'
			BEGIN
					set @strSql= @strSql +' left Join ListDetails L2 on L2.numListItemID=AD2.numCountry' 
			END
		  ELSE
		  BEGIN                                    
				set @strSql= @strSql +' left Join ListDetails L2 on L2.numListItemID='+@SortColumnName                                  
		  END
  end  
 else if @vcListItemType1='S'                               
    begin           
		IF @SortColumnName ='numBillState'
		BEGIN
   			set @strSql= @strSql +' left join State S2 on S2.numStateID=AD1.numState'                            
		END
	    ELSE IF @SortColumnName ='numShipState'
	    BEGIN
   			set @strSql= @strSql +' left join State S2 on S2.numStateID=AD2.numState'                           
		END
    end                          
  end                              
  set @strSql=@strSql+' where OppMas.numDomainID  = '+convert(varchar(15),@numDomainID)+   @WhereCondition                                      
     
  
if (@ColumnName<>'' and  @ColumnSearch<>'')                             
begin                              
  if @vcListItemType='LI'                                   
    begin    
			IF @ColumnName ='numBillCountry'
			BEGIN
					set @strSql= @strSql +' left Join ListDetails L1 on L1.numListItemID=AD1.numCountry' 
			END
			ELSE IF @ColumnName ='numShipCountry'
			BEGIN
					set @strSql= @strSql +' left Join ListDetails L1 on L1.numListItemID=AD2.numCountry' 
			END
		  ELSE
		  BEGIN                                
				set @strSql= @strSql +' and L1.vcData like '''+@ColumnSearch +'%'''                                 
			END
		end                                  
 else if @vcListItemType='S'                               
    begin           
		IF @ColumnName ='numBillState'
		BEGIN
   			set @strSql= @strSql +' left join State S1 on S1.numStateID=AD1.numState'                            
		END
	    ELSE IF @ColumnName ='numShipState'
	    BEGIN
   			set @strSql= @strSql +' left join State S1 on S1.numStateID=AD2.numState'                           
		END
    end
    else set @strSql= @strSql +' and '+ 
				case when @ColumnName = 'numAssignedTo' then 'OppMas.'+@ColumnName 
				else @ColumnName end 
				+' like '''+@ColumnSearch  +'%'''                                
                              
end                              
   if (@SortColumnName<>'')                            
  begin     
                          
    if @vcListItemType1='LI'                                   
    begin        
      set @strSql= @strSql +' order by L2.vcData '+@columnSortOrder                                  
    end   
	else if @vcListItemType1='S'                               
		begin                                  
		  set @strSql= @strSql +' order by S2.vcState '+@columnSortOrder                              
		end                                
    ELSE
	BEGIN
		--SET @strSql  = REPLACE(@strSql,'distinct','')
		set @strSql= @strSql +' order by '+ @SortColumnName +' ' +@columnSortOrder
	END  
  end   
  
  
insert into #tempTable exec(@strSql)    
 print @strSql 
 print '===================================================='  
         
         --SELECT * FROM #tempTable               
set @strSql=''                                  
                                  
DECLARE @bitBillAddressJoinAdded AS BIT;
DECLARE @bitShipAddressJoinAdded AS BIT;
SET @bitBillAddressJoinAdded=0;
SET @bitShipAddressJoinAdded=0;
                      
set @tintOrder=0                                  
set @WhereCondition =''                               
set @strSql='select OppMas.numOppId '
IF @bitIsSearchBizDoc = 1 --Added to eliminate duplicate records 
BEGIN
	SET @strSql = @strSql + ' ,OpportunityBizDocs.numOppBizDocsId '
END

select top 1 @tintOrder=A.tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFormFieldName=vcFieldName,@vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,@numListID=numListID,@vcLookBackTableName=vcLookBackTableName
from AdvSerViewConf A                                  
join View_DynamicDefaultColumns D                                   
on D.numFieldId=A.numFormFieldId   and d.numformId = 15                               
where D.numDomainID = @numDomainID AND D.numFormID = 15 AND A.numFormID = 15  AND tintViewID=@ViewID and numGroupID=@numGroupID and A.numDomainID=@numDomainID 
order by A.tintOrder asc                                  

while @tintOrder>0                                  
begin                                  

--PRINT @vcDbColumnName
--print @vcAssociatedControlType

 if @vcAssociatedControlType='SelectBox'                                  
        begin                                  
                                                                              
    IF @vcDbColumnName = 'tintSource'
            BEGIN
              SET @strSql = @strSql
                                  + ',dbo.fn_GetOpportunitySourceValue(ISNULL(OppMas.tintSource,0),ISNULL(OppMas.tintSourceType,0),OppMas.numDomainID) '
                                  +' ['+ @vcFormFieldName+'~'+ @vcDbColumnName+']'
				
            END                                            
		  ELSE if @vcListItemType='LI'                                   
		  begin   
IF @numListID=40--Country
		  BEGIN
	  			set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcFormFieldName+'~'+ @vcDbColumnName+']'                              
			IF @vcDbColumnName ='numBillCountry'
			BEGIN
				IF @bitBillAddressJoinAdded=0 
				BEGIN
					set @WhereCondition= @WhereCondition +' left Join AddressDetails AD1 on AD1.numRecordId= DM.numDivisionID and AD1.tintAddressOf=2 and AD1.tintAddressType=1 AND AD1.bitIsPrimary=1 and AD1.numDomainID= ADC.numDomainID '
					
					SET @bitBillAddressJoinAdded=1;
				END
				set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=AD1.numCountry'
			END
			ELSE IF @vcDbColumnName ='numShipCountry'
			BEGIN
				IF @bitShipAddressJoinAdded=0 
				BEGIN
					set @WhereCondition= @WhereCondition +' left Join AddressDetails AD2 on AD2.numRecordId= DM.numDivisionID and AD2.tintAddressOf=2 and AD2.tintAddressType=2 AND AD2.bitIsPrimary=1 and AD2.numDomainID= ADC.numDomainID '
					
					SET @bitShipAddressJoinAdded=1;
				END
				set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=AD2.numCountry'
			END
		  END                        
		  ELSE
		  BEGIN                               
			set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcFormFieldName+'~'+ @vcDbColumnName+']'
				
			if @vcDbColumnName = 'numSalesOrPurType' AND @numListID=46
				set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=OppMas.numSalesOrPurType and OppMas.tintOppType = 2 '                                 		
			else if @vcDbColumnName = 'numSalesOrPurType' AND @numListID=45
				set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=OppMas.numSalesOrPurType and OppMas.tintOppType = 1 '                                 
			else
				set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName                                  
		  end                                  
            END                      
		end 
ELSE IF @vcAssociatedControlType='ListBox'   
BEGIN
	if @vcListItemType='S'                               
	  begin                         
		set @strSql=@strSql+',S'+ convert(varchar(3),@tintOrder)+'.vcState'+' ['+ @vcFormFieldName+'~'+ @vcDbColumnName+']'
		IF @vcDbColumnName ='numBillState'
		BEGIN
			IF @bitBillAddressJoinAdded=0 
			BEGIN
				set @WhereCondition= @WhereCondition +' left Join AddressDetails AD1 on AD1.numRecordId= DM.numDivisionID and AD1.tintAddressOf=2 and AD1.tintAddressType=1 AND AD1.bitIsPrimary=1 and AD1.numDomainID= ADC.numDomainID '
				SET @bitBillAddressJoinAdded=1;
			END
			set @WhereCondition= @WhereCondition +' left join State S'+ convert(varchar(3),@tintOrder)+ ' on S'+convert(varchar(3),@tintOrder)+ '.numStateID=AD1.numState'
		END
	    ELSE IF @vcDbColumnName ='numShipState'
	    BEGIN
			IF @bitShipAddressJoinAdded=0 
			BEGIN
				set @WhereCondition= @WhereCondition +' left Join AddressDetails AD2 on AD2.numRecordId= DM.numDivisionID and AD2.tintAddressOf=2 and AD2.tintAddressType=2 AND AD2.bitIsPrimary=1 and AD2.numDomainID= ADC.numDomainID '
				SET @bitShipAddressJoinAdded=1;
			END
			set @WhereCondition= @WhereCondition +' left join State S'+ convert(varchar(3),@tintOrder)+ ' on S'+convert(varchar(3),@tintOrder)+ '.numStateID=AD2.numState'
		END
	  end  
END  
else if @vcAssociatedControlType = 'CheckBox'           
   begin            
               
    set @strSql= @strSql+',case when isnull('+ @vcDbColumnName +',0)=0 then ''No'' when isnull('+ @vcDbColumnName +',0)=1 then ''Yes'' end   ['+ @vcFormFieldName+'~'+ @vcDbColumnName+']'              
 
   end                                  
 else 
		BEGIN
			IF @bitIsSearchBizDoc = 0
			BEGIN
				IF @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName != 'monDealAmount' AND @vcDbColumnName != 'monAmountPaid'
				  BEGIN
	  					SET @vcDbColumnName = '''''' --include OpportunityBizDocs tables column as blank string
				  END	
			END
	  
			set @strSql=@strSql+','+ 
				case  
					when @vcLookBackTableName = 'OpportunityMaster' AND @vcDbColumnName='monDealAmount' then 'OppMas.monDealAmount' 
					when @vcDbColumnName='numAge' then 'year(getutcdate())-year(bintDOB)'   
					when @vcDbColumnName='numDivisionID' then 'DM.numDivisionID' 
					when @vcDbColumnName='vcProgress' then  ' dbo.fn_OppTotalProgress(OppMas.numOppId)'
					when @vcDbColumnName='intPEstimatedCloseDate' or @vcDbColumnName='dtDateEntered' or @vcDbColumnName='bintShippedDate'  then  
							'dbo.FormatedDateFromDate('+@vcDbColumnName+','+convert(varchar(10),@numDomainId)+')'  
					WHEN @vcDbColumnName = 'CalAmount' THEN '[dbo].[getdealamount](OppMas.numOppId,Getutcdate(),0)'
					WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monDealAmount' THEN 'ISNULL((SELECT SUM(ISNULL(BD.monDealAmount,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = OppMas.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
					WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monAmountPaid' THEN 'ISNULL((SELECT SUM(ISNULL(BD.monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = OppMas.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
					else @vcDbColumnName end+' ['+ @vcFormFieldName+'~'+ @vcDbColumnName+']'	
		END
                   
                                          
 select top 1 @tintOrder=A.tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFormFieldName=vcFieldName,@vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,@numListID=numListID,@vcLookBackTableName=vcLookBackTableName
 from AdvSerViewConf A                                  
 join View_DynamicDefaultColumns D                                   
 on D.numFieldId=A.numFormFieldId  and d.numformId = 15    
 where D.numDomainID = @numDomainID AND D.numFormID = 15 AND A.numFormID = 15  AND tintViewID=@ViewID and numGroupID=@numGroupID and A.numDomainID=@numDomainID and A.tintOrder > @tintOrder-1 
 order by A.tintOrder asc                                  

 if @@rowcount=0 set @tintOrder=0                                  
end   
	declare @firstRec as integer                                                                        
	declare @lastRec as integer                                                                        
	set @firstRec= (@CurrentPage-1) * @PageSize                          
	set @lastRec= (@CurrentPage*@PageSize+1)                                                                         
	set @TotRecs=(select count(*) from #tempTable)   


DECLARE @from VARCHAR(8000) 
set @from = ' from OpportunityMaster OppMas   
Join AdditionalContactsinformation ADC on OppMas.numContactId = ADC.numContactId  
join DivisionMaster DM on Dm.numDivisionId = OppMas.numDivisionId  
JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId '

IF @bitIsSearchBizDoc = 1 --Added to eliminate duplicate records 
 --Added By Sachin Sadhu||Date:12thJune2014
  --Purpose:To add Functionaly :Search with Notes Field-OpportunityBizDocItems

	IF CHARINDEX('vcNotes', @WhereCondition) > 0
		BEGIN
	  		SET @from = @from + ' left join OpportunityBizDocs on OppMas.numOppId = OpportunityBizDocs.numOppId left join OpportunityBizDocItems on OpportunityBizDocItems.numOppBizDocID=OpportunityBizDocs.numOppBizDocsId '
		END
	ELSE
		BEGIN
			SET @from = @from + ' left join OpportunityBizDocs on OppMas.numOppId = OpportunityBizDocs.numOppId '
		END
	--End of Script

SET @strSql=@strSql+@from;

 
 
 if LEN(@strMassUpdate)>1
 begin     
	Declare @strReplace as varchar(2000)
    set @strReplace = case when @LookTable='OppMas' then 'OppMas.numOppID' ELSE '' end  + @from
   
    set @strMassUpdate=replace(@strMassUpdate,'@$replace',@strReplace)
    PRINT @strMassUpdate
   exec (@strMassUpdate)         
 end         




SET @strSql=@strSql+ @WhereCondition


	SET @strSql = @strSql +' join #tempTable T on T.numOppID=OppMas.numOppID 
 WHERE ID > '+convert(varchar(10),@firstRec)+ ' and ID <'+ convert(varchar(10),@lastRec)
 IF @bitIsSearchBizDoc = 1 
 BEGIN
 	SET @strSql = @strSql + '  and T.numOppBizDocID = OpportunityBizDocs.numOppBizDocsID '
 END
 
 SET @strSql = @strSql + ' order by ID'
print @strSql
exec(@strSql)                                                                     
drop table #tempTable

 



--Left Join OpportunityItems OppItems on OppItems.numOppId = OppMas.numOppId   
--left join Item on Item.numItemCode = OppItems.numItemCode  
--left join WareHouseItems on Item.numItemCode = WareHouseItems.numItemId  
--left Join WareHouses WareHouse on WareHouse.numWareHouseId =WareHouseItems.numWareHouseId  
--left Join WareHouseItmsDTL WareHouseItemDTL on WareHouseItemDTL.numWareHouseItemID = WareHouse.numWareHouseId
GO
--Created by anoop jayaraj                                                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_checkuseratlogin')
DROP PROCEDURE usp_checkuseratlogin
GO
CREATE PROCEDURE [dbo].[USP_CheckUserAtLogin]                                                              
(                                                                                    
@UserName as varchar(100)='',                                    
@vcPassword as varchar(100)=''                                                                           
)                                                              
as                                                              


/*check subscription validity */
IF EXISTS( SELECT *
FROM UserMaster U                              
 Join Domain D                              
 on D.numDomainID=U.numDomainID
 Join  Subscribers S                            
 on S.numTargetDomainID=D.numDomainID
 WHERE vcEmailID=@UserName and vcPassword=@vcPassword and bitActive=1 AND getutcdate() > dtSubEndDate)
BEGIN
	RAISERROR('SUB_RENEW',16,1)
	RETURN;
END



                                                                
 SELECT top 1 U.numUserID,numUserDetailId,isnull(vcEmailID,'') vcEmailID,isnull(numGroupID,0) numGroupID,bitActivateFlag,                              
 isnull(vcMailNickName,'') vcMailNickName,isnull(txtSignature,'') txtSignature,isnull(U.numDomainID,0) numDomainID,                              
 isnull(Div.numDivisionID,0) numDivisionID,isnull(vcCompanyName,'') vcCompanyName,isnull(E.bitExchangeIntegration,0) bitExchangeIntegration,                              
 isnull(E.bitAccessExchange,0) bitAccessExchange,isnull(E.vcExchPath,'') vcExchPath,isnull(E.vcExchDomain,'') vcExchDomain,                              
 isnull(D.vcExchUserName,'') vcExchUserName ,isnull(vcFirstname,'')+' '+isnull(vcLastName,'') as ContactName,                              
 Case when E.bitAccessExchange=0 then  E.vcExchPassword when E.bitAccessExchange=1 then D.vcExchPassword end as vcExchPassword,                          
 tintCustomPagingRows,                         
 vcDateFormat,                         
 numDefCountry,                     
 tintComposeWindow,
 dateadd(day,-sintStartDate,getutcdate()) as StartDate,                        
 dateadd(day,sintNoofDaysInterval,dateadd(day,-sintStartDate,getutcdate())) as EndDate,                        
 tintAssignToCriteria,                         
 bitIntmedPage,                         
 tintFiscalStartMonth,numAdminID,isnull(A.numTeam,0) numTeam ,                
 isnull(D.vcCurrency,'') as vcCurrency, 
 isnull(D.numCurrencyID,0) as numCurrencyID, 
 isnull(D.bitMultiCurrency,0) as bitMultiCurrency,              
 bitTrial,isnull(tintChrForComSearch,0) as tintChrForComSearch,  
isnull(tintChrForItemSearch,0) as tintChrForItemSearch,              
 case when bitSMTPServer= 1 then vcSMTPServer else '' end as vcSMTPServer  ,          
 case when bitSMTPServer= 1 then  isnull(numSMTPPort,0)  else 0 end as numSMTPPort      
,isnull(bitSMTPAuth,0) bitSMTPAuth    
,isnull([vcSmtpPassword],'') vcSmtpPassword    
,isnull(bitSMTPSSL,0) bitSMTPSSL   ,isnull(bitSMTPServer,0) bitSMTPServer,       
isnull(IM.bitImap,0) bitImapIntegration, isnull(charUnitSystem,'E')   UnitSystem, datediff(day,getutcdate(),dtSubEndDate) as NoOfDaysLeft,
ISNULL(bitCreateInvoice,0) bitCreateInvoice,
ISNULL([numDefaultSalesBizDocId],0) numDefaultSalesBizDocId,
ISNULL([numDefaultPurchaseBizDocId],0) numDefaultPurchaseBizDocId,
vcDomainName,
S.numSubscriberID,
D.[tintBaseTax],
ISNULL(D.[numShipCompany],0) numShipCompany,
ISNULL(D.[bitEmbeddedCost],0) bitEmbeddedCost,
(Select isnull(numAuthoritativeSales,0) From AuthoritativeBizDocs Where numDomainId=D.numDomainID) numAuthoritativeSales,
(Select isnull(numAuthoritativePurchase,0) From AuthoritativeBizDocs Where numDomainId=D.numDomainID) numAuthoritativePurchase,
ISNULL(D.[numDefaultSalesOppBizDocId],0) numDefaultSalesOppBizDocId,
ISNULL(tintDecimalPoints,2) tintDecimalPoints,
ISNULL(D.tintBillToForPO,0) tintBillToForPO, 
ISNULL(D.tintShipToForPO,0) tintShipToForPO,
ISNULL(D.tintOrganizationSearchCriteria,0) tintOrganizationSearchCriteria,
ISNULL(D.tintLogin,0) tintLogin,
ISNULL(bitDocumentRepositary,0) bitDocumentRepositary,ISNULL(D.tintComAppliesTo,0) tintComAppliesTo,
ISNULL(D.vcPortalName,'') vcPortalName,
(SELECT COUNT(*) FROM dbo.Subscribers WHERE getutcdate() between dtSubStartDate and dtSubEndDate and bitActive=1),
ISNULL(D.bitGtoBContact,0) bitGtoBContact,
ISNULL(D.bitBtoGContact,0) bitBtoGContact,
ISNULL(D.bitGtoBCalendar,0) bitGtoBCalendar,
ISNULL(D.bitBtoGCalendar,0) bitBtoGCalendar,
ISNULL(bitExpenseNonInventoryItem,0) bitExpenseNonInventoryItem,
ISNULL(D.bitInlineEdit,0) bitInlineEdit,
ISNULL(U.numDefaultClass,0) numDefaultClass,
ISNULL(D.bitRemoveVendorPOValidation,0) bitRemoveVendorPOValidation,
ISNULL(D.bitTrakDirtyForm,1) bitTrakDirtyForm,
isnull(D.tintBaseTaxOnArea,0) tintBaseTaxOnArea,
ISNULL(D.bitAmountPastDue,0) bitAmountPastDue,
CONVERT(DECIMAL(18,2),ISNULL(D.monAmountPastDue,0)) monAmountPastDue,
ISNULL(D.bitSearchOrderCustomerHistory,0) bitSearchOrderCustomerHistory,
isnull(U.tintTabEvent,0) as tintTabEvent,
isnull(D.bitRentalItem,0) as bitRentalItem,
isnull(D.numRentalItemClass,0) as numRentalItemClass,
isnull(D.numRentalHourlyUOM,0) as numRentalHourlyUOM,
isnull(D.numRentalDailyUOM,0) as numRentalDailyUOM,
isnull(D.tintRentalPriceBasedOn,0) as tintRentalPriceBasedOn,
isnull(D.tintCalendarTimeFormat,24) as tintCalendarTimeFormat,
isnull(D.tintDefaultClassType,0) as tintDefaultClassType,
ISNULL(U.numDefaultWarehouse,0) numDefaultWarehouse,
ISNULL(D.tintPriceBookDiscount,0) tintPriceBookDiscount,
ISNULL(D.bitAutoSerialNoAssign,0) bitAutoSerialNoAssign,
ISNULL(IsEnableClassTracking,0) AS [IsEnableClassTracking],
ISNULL(IsEnableProjectTracking,0) AS [IsEnableProjectTracking],
ISNULL(IsEnableCampaignTracking,0) AS [IsEnableCampaignTracking],
ISNULL(IsEnableResourceScheduling,0) AS [IsEnableResourceScheduling],
ISNULL(D.numSOBizDocStatus,0) AS numSOBizDocStatus,
ISNULL(D.bitAutolinkUnappliedPayment,0) AS bitAutolinkUnappliedPayment,
ISNULL(numShippingServiceItemID,0) AS [numShippingServiceItemID],
ISNULL(D.numDiscountServiceItemID,0) AS numDiscountServiceItemID,
ISNULL(D.tintCommissionType,1) AS tintCommissionType,
ISNULL(D.bitDiscountOnUnitPrice,0) AS bitDiscountOnUnitPrice,
ISNULL(D.intShippingImageWidth,0) AS intShippingImageWidth ,
ISNULL(D.intShippingImageHeight,0) AS intShippingImageHeight,
ISNULL(D.numTotalInsuredValue,0) AS numTotalInsuredValue,
ISNULL(D.numTotalCustomsValue,0) AS numTotalCustomsValue,
ISNULL(D.IsEnableUserLevelClassTracking,0) AS IsEnableUserLevelClassTracking,
ISNULL(D.bitUseBizdocAmount,1) AS [bitUseBizdocAmount],
ISNULL(D.bitDefaultRateType,1) AS [bitDefaultRateType],
ISNULL(D.bitIncludeTaxAndShippingInCommission,1) AS [bitIncludeTaxAndShippingInCommission],
ISNULL(D.[bitLandedCost],0) AS bitLandedCost,
ISNULL(D.vcLanedCostDefault,'') AS vcLanedCostDefault
/*,ISNULL(D.bitAllowPPVariance,0) AS bitAllowPPVariance*/
 FROM UserMaster U                              
 left join ExchangeUserDetails E                              
 on E.numUserID=U.numUserID                              
left join ImapUserDetails IM                              
 on IM.numUserCntID=U.numUserDetailID          
 Join Domain D                              
 on D.numDomainID=U.numDomainID                                           
 left join DivisionMaster Div                                            
 on D.numDivisionID=Div.numDivisionID                               
 left join CompanyInfo C                              
 on C.numCompanyID=Div.numDivisionID                               
 left join AdditionalContactsInformation A                              
 on  A.numContactID=U.numUserDetailId                            
 Join  Subscribers S                            
 on S.numTargetDomainID=D.numDomainID                             
 WHERE vcEmailID=@UserName and vcPassword=@vcPassword and bitActive IN (1,2) AND getutcdate() between dtSubStartDate and dtSubEndDate
             
select numTerritoryId from UserTerritory UT                              
join UserMaster U                              
on numUserDetailId =UT.numUserCntID                                         
where vcEmailID=@UserName and vcPassword=@vcPassword
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_CompanyInfo_Search' ) 
    DROP PROCEDURE USP_CompanyInfo_Search
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Sandeep Patel
-- Create date: 15 April 2014
-- Description:	Gets company info based on search fileds configured
-- =============================================
CREATE PROCEDURE USP_CompanyInfo_Search
	@numDomainID NUMERIC(18,0),
	@numUserCntID NUMERIC(18,0),
	@isStartWithSearch BIT = 0,
	@searchText VARCHAR(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

SELECT @searchText = REPLACE(@searchText,'''', '''''')

DECLARE @CustomerSelectFields AS VARCHAR(MAX)
DECLARE @CustomerSelectCustomFields AS VARCHAR(MAX)
DECLARE @ProspectsRights AS TINYINT                    
DECLARE @accountsRights AS TINYINT                   
DECLARE @leadsRights AS TINYINT 

SET @CustomerSelectFields = ''
SET @CustomerSelectCustomFields = ''
               
SET @ProspectsRights = ( SELECT TOP 1
                                MAX(GA.intViewAllowed) AS intViewAllowed
                         FROM   UserMaster UM ,
                                GroupAuthorization GA ,
                                PageMaster PM
                         WHERE  GA.numGroupID = UM.numGroupID
                                AND PM.numPageID = GA.numPageID
                                AND PM.numModuleID = GA.numModuleID
                                AND PM.numpageID = 2
                                AND PM.numModuleID = 3
                                AND UM.numUserID = ( SELECT numUserID
                                                     FROM   userMaster
                                                     WHERE  numUserDetailId = @numUserCntID
                                                   )
                         GROUP BY UM.numUserId ,
                                UM.vcUserName ,
                                PM.vcFileName ,
                                GA.numModuleID ,
                                GA.numPageID
                       )                        
                    
SET @accountsRights = ( SELECT TOP 1
                                MAX(GA.intViewAllowed) AS intViewAllowed
                        FROM    UserMaster UM ,
                                GroupAuthorization GA ,
                                PageMaster PM
                        WHERE   GA.numGroupID = UM.numGroupID
                                AND PM.numPageID = GA.numPageID
                                AND PM.numModuleID = GA.numModuleID
                                AND PM.numpageID = 2
                                AND PM.numModuleID = 4
                                AND UM.numUserID = ( SELECT numUserID
                                                     FROM   userMaster
                                                     WHERE  numUserDetailId = @numUserCntID
                                                   )
                        GROUP BY UM.numUserId ,
                                UM.vcUserName ,
                                PM.vcFileName ,
                                GA.numModuleID ,
                                GA.numPageID
                      )                
              
SET @leadsRights = ( SELECT TOP 1
                            MAX(GA.intViewAllowed) AS intViewAllowed
                     FROM   UserMaster UM ,
                            GroupAuthorization GA ,
                            PageMaster PM
                     WHERE  GA.numGroupID = UM.numGroupID
                            AND PM.numPageID = GA.numPageID
                            AND PM.numModuleID = GA.numModuleID
                            AND PM.numpageID = 2
                            AND PM.numModuleID = 2
                            AND UM.numUserID = ( SELECT numUserID
                                                 FROM   userMaster
                                                 WHERE  numUserDetailId = @numUserCntID
                                               )
                     GROUP BY UM.numUserId ,
                            UM.vcUserName ,
                            PM.vcFileName ,
                            GA.numModuleID ,
                            GA.numPageID
                   )                   
                    
-- START: Get organization display fields
SELECT * INTO #tempOrgDisplayFields FROM
(
	SELECT 
		numFieldId,
		vcDbColumnName,
		ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName,
		tintRow AS tintOrder,
		0 as Custom
    FROM 
		View_DynamicColumns
	WHERE 
		numFormId=96 AND numUserCntID=@numUserCntID AND numDomainID=@numDomainID AND 
		tintPageType=1 AND bitCustom=0  AND ISNULL(bitSettingField,0)=1 AND numRelCntType=0
	UNION   
	SELECT 
		numFieldId,
		vcFieldName,
		vcFieldName,
		tintRow AS tintOrder,
		1 as Custom
    FROM 
		View_DynamicCustomColumns   
	WHERE 
		Grp_id=1 AND numFormId=96 AND numUserCntID=@numUserCntID AND numDomainID=@numDomainID AND 
		tintPageType=1 AND bitCustom=1 AND numRelCntType=0
)X

IF EXISTS (SELECT * FROM #tempOrgDisplayFields)
BEGIN

	SELECT 
		@CustomerSelectFields = COALESCE (@CustomerSelectFields,'') + CAST(vcDbColumnName AS VARCHAR(100)) + ','	
	FROM 
		#tempOrgDisplayFields 
	WHERE
		Custom = 0
		
	-- Removes last , from string
	IF DATALENGTH(@CustomerSelectFields) > 0
		SET @CustomerSelectFields = LEFT(@CustomerSelectFields, LEN(@CustomerSelectFields) - 1)

	IF EXISTS(SELECT * FROM #tempOrgDisplayFields WHERE Custom = 1)
	BEGIN
		SELECT 
			@CustomerSelectCustomFields = COALESCE (@CustomerSelectCustomFields,'') + 'dbo.fn_GetCustFldOrganization(' + CAST(numFieldId AS VARCHAR(100)) + ',1,TEMP1.numCompanyID) AS [' + CAST (vcDbColumnName AS VARCHAR (100)) + '],'	
		FROM 
			#tempOrgDisplayFields 
		WHERE
			Custom = 1
	END

	-- Removes last , from string
	IF DATALENGTH(@CustomerSelectCustomFields) > 0
		SET @CustomerSelectCustomFields = LEFT(@CustomerSelectCustomFields, LEN(@CustomerSelectCustomFields) - 1)
END

-- END: Get organization display fields

-- START: Generate default select statement 
                    
DECLARE @strSQL AS VARCHAR(MAX)    

SET @strSQL = 'SELECT numDivisionID, numCompanyID, vcCompanyName' + 
	CASE @CustomerSelectFields
	WHEN '' THEN ''
	ELSE ',' + @CustomerSelectFields
	END
	 + 
	CASE @CustomerSelectCustomFields
	WHEN '' THEN ''
	ELSE ',' + @CustomerSelectCustomFields
	END
	 + ' FROM (SELECT 
	CMP.numDomainID,
	CMP.numCompanyID, 
	CMP.vcCompanyName, 
	DM.numDivisionID, 
	DM.vcDivisionName, 
	ISNULL((SELECT vcData FROM dbo.ListDetails WHERE numListID = 1 AND numDomainID = DM.numDomainID AND numListItemID = DM.numStatusID),'''') as numStatusID, 
	ISNULL((SELECT vcData FROM dbo.ListDetails WHERE numListID = 2 AND numDomainID = DM.numDomainID AND numListItemID = CMP.numCompanyRating),'''') as numCompanyRating, 
	ISNULL((SELECT vcData FROM dbo.ListDetails WHERE numListID = 3 AND numDomainID = DM.numDomainID AND numListItemID = CMP.numCompanyCredit),'''') as numCompanyCredit,
	ISNULL((SELECT vcData FROM dbo.ListDetails WHERE numListID = 4 AND numDomainID = DM.numDomainID AND numListItemID = CMP.numCompanyIndustry),'''') as numCompanyIndustry,
	ISNULL((SELECT vcData FROM dbo.ListDetails WHERE numListID = 5 AND numDomainID = DM.numDomainID AND numListItemID = CMP.numCompanyType),'''') as numCompanyType,
	ISNULL((SELECT vcData FROM dbo.ListDetails WHERE numListID = 6 AND numDomainID = DM.numDomainID AND numListItemID = CMP.numAnnualRevID),'''') as numAnnualRevID,
	ISNULL((SELECT vcData FROM dbo.ListDetails WHERE numListID = 7 AND numDomainID = DM.numDomainID AND numListItemID = CMP.numNoOfEmployeesID),'''') as numNoOfEmployeesID,
	vcComPhone,
	vcComFax,
	ISNULL((SELECT vcData FROM dbo.ListDetails WHERE numListID = 18 AND numDomainID = DM.numDomainID AND numListItemID = CMP.vcHow),'''') as vcHow,
	ISNULL((SELECT vcData FROM dbo.ListDetails WHERE numListID = 21 AND numDomainID = DM.numDomainID AND numListItemID = CMP.vcProfile),'''') as vcProfile,
	ISNULL((SELECT vcData FROM dbo.ListDetails WHERE numListID = 438 AND numDomainID = DM.numDomainID AND numListItemID = DM.numCompanyDiff),'''') as numCompanyDiff,
	DM.vcCompanyDiff,
	isnull(CMP.txtComments,'''') as txtComments, 
	isnull(CMP.vcWebSite,'''') vcWebSite, 
	isnull(AD1.vcStreet,'''') as vcBillStreet, 
	isnull(AD1.vcCity,'''') as vcBillCity, 
	isnull(dbo.fn_GetState(AD1.numState),'''') as numBillState,
	isnull(AD1.vcPostalCode,'''') as vcBillPostCode,
	isnull(dbo.fn_GetListName(AD1.numCountry,0),'''') as numBillCountry,
	isnull(AD2.vcStreet,'''') as vcShipStreet,
	isnull(AD2.vcCity,'''') as vcShipCity,
	isnull(dbo.fn_GetState(AD2.numState),'''')  as numShipState,
	isnull(AD2.vcPostalCode,'''') as vcShipPostCode, 
	isnull(dbo.fn_GetListName(AD2.numCountry,0),'''') as numShipCountry,
	ISNULL((SELECT vcData FROM dbo.ListDetails WHERE numListID = 78 AND numDomainID = DM.numDomainID AND numListItemID = DM.numTerID),'''') as numTerID
FROM  
(
SELECT
	CMP.numCompanyId
FROM
	dbo.CompanyInfo CMP
INNER JOIN
	dbo.DivisionMaster d
ON
	CMP.numCompanyId = d.numCompanyID
INNER JOIN
	AdditionalContactsInformation AC
ON 
	AC.numDivisionID=d.numDivisionID AND ISNULL(AC.bitPrimaryContact,0)=1
WHERE 
	CMP.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' AND d.tintCRMType<>0 and d.tintCRMType=1' +
	CASE @ProspectsRights WHEN 0 THEN ' and d.numRecOwner=0 ' ELSE '' END +
    CASE @ProspectsRights WHEN 1 THEN ' and d.numRecOwner='+convert(varchar(15),@numUserCntID) ELSE '' END +                        
    CASE @ProspectsRights WHEN 2 THEN ' and (d.numTerID in (select numTerritoryID  from UserTerritory where numUserCntID='+convert(varchar(15),@numUserCntID)+') or d.numTerID=0)' ELSE '' END +
'
UNION
SELECT
	CMP.numCompanyId
FROM
	dbo.CompanyInfo CMP
INNER JOIN
	dbo.DivisionMaster d
ON
	CMP.numCompanyId = d.numCompanyID
INNER JOIN
	AdditionalContactsInformation AC
ON 
	AC.numDivisionID=d.numDivisionID AND ISNULL(AC.bitPrimaryContact,0)=1
WHERE 
	CMP.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' AND d.tintCRMType<>0 and d.tintCRMType=2' +
	CASE @accountsRights WHEN 0 THEN ' and d.numRecOwner=0 ' ELSE '' END +
    CASE @accountsRights WHEN 1 THEN ' and d.numRecOwner='+convert(varchar(15),@numUserCntID) ELSE '' END +                        
    CASE @accountsRights WHEN 2 THEN ' and (d.numTerID in (select numTerritoryID  from UserTerritory where numUserCntID='+convert(varchar(15),@numUserCntID)+') or d.numTerID=0)' ELSE '' END +
'
UNION
SELECT
	CMP.numCompanyId
FROM
	dbo.CompanyInfo CMP
INNER JOIN
	dbo.DivisionMaster d
ON
	CMP.numCompanyId = d.numCompanyID
INNER JOIN
	AdditionalContactsInformation AC
ON 
	AC.numDivisionID=d.numDivisionID AND ISNULL(AC.bitPrimaryContact,0)=1
WHERE 
	CMP.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' AND d.tintCRMType=0' +
	CASE @leadsRights WHEN 0 THEN ' and d.numRecOwner=0 ' ELSE '' END +
    CASE @leadsRights WHEN 1 THEN ' and d.numRecOwner='+convert(varchar(15),@numUserCntID) ELSE '' END +                        
    CASE @leadsRights WHEN 2 THEN ' and (d.numTerID in (select numTerritoryID  from UserTerritory where numUserCntID='+convert(varchar(15),@numUserCntID)+') or d.numTerID=0)' ELSE '' END +
') AS TEMP2
INNER JOIN
	CompanyInfo CMP
ON
	TEMP2.numCompanyId =  CMP.numCompanyId                                                 
INNER JOIN 
	DivisionMaster DM    
ON 
	DM.numCompanyID=CMP.numCompanyID
LEFT JOIN 
	dbo.AddressDetails AD1 
ON 
    AD1.numAddressID =  (
							SELECT 
								MAX(numAddressID) 
							FROM 
								AddressDetails 
							WHERE 
								AddressDetails.numDomainID = DM.numDomainID AND 
								AddressDetails.numRecordID = DM.numDivisionID AND 
								AddressDetails.tintAddressOf = 2 AND 
								AddressDetails.tintAddressType = 1 AND 
								AddressDetails.bitIsPrimary = 1
						  )
LEFT JOIN 
	AddressDetails AD2 
ON 
    AD2.numAddressID = (
							SELECT 
								MAX(numAddressID) 
							FROM 
								AddressDetails 
							WHERE 
								AddressDetails.numDomainID = DM.numDomainID AND 
								AddressDetails.numRecordID = DM.numDivisionID AND 
								AddressDetails.tintAddressOf = 2 AND 
								AddressDetails.tintAddressType = 2 AND 
								AddressDetails.bitIsPrimary = 1
						)) AS TEMP1'
	
-- END: Generate default select statement 

                  
-- START: Generate user defined search fields condition

DECLARE @searchWhereCondition varchar(MAX)
DECLARE @singleValueSearchFields AS varchar(MAX)   
DECLARE @multiValueSearchFields AS varchar(MAX) 
DECLARE @customSearchFields AS varchar(MAX)    


SET @searchWhereCondition = ''
SET @singleValueSearchFields = ''   
SET @multiValueSearchFields = ''
SET @customSearchFields = ''
                

SELECT * INTO #tempOrgSearch FROM
(
	SELECT 
		numFieldId,
		vcDbColumnName,
		ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName,
		tintRow AS tintOrder,
		0 as Custom
    FROM 
		View_DynamicColumns
	WHERE 
		numFormId=97 AND numUserCntID=@numUserCntID AND numDomainID=@numDomainID AND 
		tintPageType=1 AND bitCustom=0  AND ISNULL(bitSettingField,0)=1 AND numRelCntType=0
	UNION   
	SELECT 
		numFieldId,
		vcFieldName,
		vcFieldName,
		tintRow AS tintOrder,
		1 as Custom
    FROM 
		View_DynamicCustomColumns   
	WHERE 
		Grp_id=1 AND numFormId=97 AND numUserCntID=@numUserCntID AND numDomainID=@numDomainID AND 
		tintPageType=1 AND bitCustom=1 AND numRelCntType=0
)Y

IF @searchText != ''
BEGIN
	IF (SELECT COUNT(*) FROM #tempOrgSearch) > 0 
	BEGIN
		-- START: Remove white spaces from search values
		DECLARE @finalSearchText VARCHAR(MAX)
		DECLARE @TempTable TABLE (vcValue VARCHAR(MAX))

		WHILE LEN(@searchText) > 0
		BEGIN
			SET @finalSearchText = LEFT(@searchText, 
									ISNULL(NULLIF(CHARINDEX(',', @searchText) - 1, -1),
									LEN(@searchText)))
			SET @searchText = SUBSTRING(@searchText,
										 ISNULL(NULLIF(CHARINDEX(',', @searchText), 0),
										 LEN(@searchText)) + 1, LEN(@searchText))

			INSERT INTO @TempTable (vcValue) VALUES ( @finalSearchText )
		END
		
		--Removes white spaces
		UPDATE
			@TempTable
		SET
			vcValue = LTRIM(RTRIM(vcValue))
	
		--Converting table to comma seperated string
		SELECT @searchText = COALESCE(@searchText,'') +  CONVERT(VARCHAR(MAX),vcValue) + ',' FROM @TempTable
	
		--Remove last comma from final search string
		IF DATALENGTH(@searchText) > 0
			SET @searchText = LEFT(@searchText, LEN(@searchText) - 1)
	
		-- END: Remove white spaces from search values
	
		--START: Gets search fields where there is one-one relation 
		SELECT 
			@singleValueSearchFields = COALESCE(@singleValueSearchFields,'') + '(' + CAST (vcDbColumnName AS VARCHAR (50)) + ' LIKE '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END + REPLACE(@searchText, ',', '%'' OR ' + vcDbColumnName + ' LIKE  ' + CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END) + '%'')' + ' OR '	
		FROM 
			#tempOrgSearch 
		WHERE 
			numFieldId NOT IN (51,52,96,148,127) AND
			Custom = 0
			
		
		-- Removes last OR from string
		IF DATALENGTH(@singleValueSearchFields) > 0
			SET @singleValueSearchFields = LEFT(@singleValueSearchFields, LEN(@singleValueSearchFields) - 3)
			
		--END: Gets search fields where there is one-one relation 
		
		
		--START: Gets search fields where there is one-many relation 
		-- 1. CONTACT FIRST NAME
		IF (SELECT COUNT(*) FROM #tempOrgSearch WHERE numFieldId = 51) > 0
			SET @multiValueSearchFields = '(SELECT COUNT(*) FROM AdditionalContactsInformation ACI WHERE ACI.numDivisionID=TEMP1.numDivisionID AND (ACI.vcFirstName LIKE '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END +  REPLACE(@searchText, ',', '%'' OR ACI.vcFirstName LIKE  '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END) + '%'')) > 0'	
		
		-- 2. CONTACT LAST NAME
		IF (SELECT COUNT(*) FROM #tempOrgSearch WHERE numFieldId = 52) > 0
		BEGIN
			IF @multiValueSearchFields = ''
				SET @multiValueSearchFields = '(SELECT COUNT(*) FROM AdditionalContactsInformation ACI WHERE ACI.numDivisionID=TEMP1.numDivisionID AND (ACI.vcLastName LIKE '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END +  REPLACE(@searchText, ',', '%'' OR ACI.vcLastName LIKE  '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END) + '%'')) > 0'
			ELSE	
				SET @multiValueSearchFields = @multiValueSearchFields + ' OR (SELECT COUNT(*) FROM AdditionalContactsInformation ACI WHERE ACI.numDivisionID=TEMP1.numDivisionID AND (ACI.vcLastName LIKE '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END +  REPLACE(@searchText, ',', '%'' OR ACI.vcLastName LIKE  '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END) + '%'')) > 0'
		END	
		
		-- 3. ORDER NAME
		IF (SELECT COUNT(*) FROM #tempOrgSearch WHERE numFieldId = 96) > 0
		BEGIN
			IF @multiValueSearchFields = ''
				SET @multiValueSearchFields = '(SELECT COUNT(*) FROM OpportunityMaster OM WHERE OM.numDivisionId = TEMP1.numDivisionID AND (OM.vcPOppName LIKE '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END +  REPLACE(@searchText, ',', '%'' OR OM.vcPOppName LIKE  '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END) + '%'')) > 0'
			ELSE	
				SET @multiValueSearchFields = @multiValueSearchFields + ' OR (SELECT COUNT(*) FROM OpportunityMaster OM WHERE OM.numDivisionId = TEMP1.numDivisionID AND (OM.vcPOppName LIKE '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END +  REPLACE(@searchText, ',', '%'' OR OM.vcPOppName LIKE  '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END) + '%'')) > 0'
		END	
		
		-- 4.PROJECT NAME
		IF (SELECT COUNT(*) FROM #tempOrgSearch WHERE numFieldId = 148) > 0
		BEGIN
			IF @multiValueSearchFields = ''
				SET @multiValueSearchFields = '(SELECT COUNT(*) FROM dbo.ProjectsMaster PM WHERE PM.numDivisionId = TEMP1.numDivisionID AND (PM.vcProjectName LIKE '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END +  REPLACE(@searchText, ',', '%'' OR PM.vcProjectName LIKE  '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END) + '%'')) > 0'
			ELSE	
				SET @multiValueSearchFields = @multiValueSearchFields + ' OR (SELECT COUNT(*) FROM dbo.ProjectsMaster PM WHERE PM.numDivisionId = TEMP1.numDivisionID AND (PM.vcProjectName LIKE '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END +  REPLACE(@searchText, ',', '%'' OR PM.vcProjectName LIKE  '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END) + '%'')) > 0'
		END	
		
		-- 5. CASE NAME
		IF (SELECT COUNT(*) FROM #tempOrgSearch WHERE numFieldId = 127) > 0
		BEGIN		
			IF @multiValueSearchFields = ''
				SET @multiValueSearchFields = '(SELECT COUNT(*) FROM dbo.Cases C WHERE C.numDivisionId = TEMP1.numDivisionID AND (C.textSubject LIKE '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END +  REPLACE(@searchText, ',', '%'' OR C.textSubject LIKE  '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END) + '%'')) > 0'
			ELSE	
				SET @multiValueSearchFields = @multiValueSearchFields + ' OR (SELECT COUNT(*) FROM dbo.Cases C WHERE C.numDivisionId = TEMP1.numDivisionID AND (C.textSubject LIKE '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END +  REPLACE(@searchText, ',', '%'' OR C.textSubject LIKE  '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END) + '%'')) > 0'
		END	
		
		--END: Gets search fields where there is one-many relation 
		
		--START: Get custom search fields
		
		IF EXISTS (SELECT * FROM #tempOrgSearch WHERE Custom = 1)
		BEGIN
			SELECT 
				@customSearchFields = COALESCE (@customSearchFields,'') + '( dbo.fn_GetCustFldOrganization(' + CAST(numFieldId AS VARCHAR(20)) + ',1,TEMP1.numCompanyID) LIKE '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END + REPLACE(@searchText, ',', '%'' OR dbo.fn_GetCustFldOrganization(' + CAST(numFieldId AS VARCHAR(20)) + ',1,TEMP1.numCompanyID) LIKE  ' + CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END) + '%'')' + ' OR '	
			FROM 
				#tempOrgSearch 
			WHERE 
				Custom = 1
		END
		
		-- Removes last OR from string
		IF DATALENGTH(@customSearchFields) > 0
			SET @customSearchFields = LEFT(@customSearchFields, LEN(@customSearchFields) - 3)
		
		--END: Get custom search fields
		
		IF DATALENGTH(@singleValueSearchFields) > 0
			SET @searchWhereCondition = @singleValueSearchFields
			
		IF DATALENGTH(@multiValueSearchFields) > 0
			IF DATALENGTH(@searchWhereCondition) > 0
				SET @searchWhereCondition = @searchWhereCondition + ' OR ' + @multiValueSearchFields
			ELSE
				SET @searchWhereCondition = @multiValueSearchFields
				
		IF DATALENGTH(@customSearchFields) > 0
			IF 	DATALENGTH(@searchWhereCondition) = 0
				SET @searchWhereCondition = @customSearchFields
			ELSE
				SET @searchWhereCondition = @searchWhereCondition + ' OR ' + @customSearchFields
			
	END
	ELSE
	BEGIN
		SET @searchWhereCondition = '(vcCompanyName LIKE '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END +  REPLACE(@searchText, ',', '%'' OR vcCompanyName LIKE  '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END) + '%'')'
	END
END

-- END: Generate user defined search fields condition

IF DATALENGTH(@searchWhereCondition) > 0
		SET @strSQL = @strSQL + ' WHERE ' + @searchWhereCondition 
    
 
SET @strSQL = @strSQL + ' ORDER BY vcCompanyname'    

--SELECT @strSQL
EXEC (@strSQL)

END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_createbizdocs')
DROP PROCEDURE usp_createbizdocs
GO
CREATE PROCEDURE [dbo].[USP_CreateBizDocs]                        
(                                              
 @numOppId as numeric(9)=null,                        
 @numBizDocId as numeric(9)=null,                        
 @numUserCntID as numeric(9)=null,                        
 @numOppBizDocsId as numeric(9)=NULL OUTPUT,
 @vcComments as varchar(1000),
 @bitPartialFulfillment as bit,
 @strBizDocItems as text='' ,
 --@bitDiscountType as bit,
 --@fltDiscount  as float,
 --@monShipCost as money,
 @numShipVia as numeric(9),
 @vcTrackingURL as varchar(1000),
 --@bitBillingTerms as bit,
 --@intBillingDays as integer,
 @dtFromDate AS DATETIME,
 --@bitInterestType as bit,
 --@fltInterest as float,
-- @numShipDoc as numeric(9),
 @numBizDocStatus as numeric(9) = 0,
 @bitRecurringBizDoc AS BIT = NULL,
 @numSequenceId AS VARCHAR(50) = '',
 --@tintTaxOperator AS tinyint,
 @tintDeferred as tinyint=0,
 @monCreditAmount as money= 0 OUTPUT,
 @ClientTimeZoneOffset as int=0,
 @monDealAmount as money= 0 OUTPUT,
 @bitRentalBizDoc as bit=0,
 @numBizDocTempID as numeric(9)=0,
 @vcTrackingNo AS VARCHAR(500),
 --@vcShippingMethod AS VARCHAR(100),
 @vcRefOrderNo AS VARCHAR(100),
 --@dtDeliveryDate AS DATETIME,
 @OMP_SalesTaxPercent AS FLOAT = 0, --(joseph) Added to Get Online Marketplace Sales Tax Percentage
 @numFromOppBizDocsId AS NUMERIC(9)=0,
 @bitTakeSequenceId AS BIT=0,
 @bitAllItems AS BIT=0,
 @bitNotValidateBizDocFulfillment AS BIT=0,
 @fltExchangeRateBizDoc AS FLOAT=0
)                        
as 

BEGIN TRY

declare @hDocItem as INTEGER
declare @numDivisionID as numeric(9)
DECLARE @numDomainID NUMERIC(9)
DECLARE @tintOppType AS TINYINT	
DECLARE @numFulfillmentOrderBizDocId NUMERIC(9);SET @numFulfillmentOrderBizDocId=296 
	    
IF ISNULL(@fltExchangeRateBizDoc,0)=0
	SELECT @fltExchangeRateBizDoc=fltExchangeRate FROM OpportunityMaster where numOppID=@numOppId
	
select @numDomainID=numDomainID,@numDivisionID=numDivisionID,@tintOppType=tintOppType from OpportunityMaster where numOppID=@numOppId

IF @numBizDocTempID=0
BEGIN
	select @numBizDocTempID=numBizDocTempID from BizDocTemplate where numBizDocId=@numBizDocId 
	and numDomainID=@numDomainID and numOpptype=@tintOppType 
	and tintTemplateType=0 and isnull(bitDefault,0)=1 and isnull(bitEnabled,0)=1
END

if @numOppBizDocsId=0
BEGIN
IF (
	(@bitPartialFulfillment = 1 AND NOT EXISTS (SELECT * FROM   OpportunityBizDocs WHERE  numOppID = @numOppId AND numBizDocId = @numBizDocId AND bitPartialFulfilment = 0))
     OR (NOT EXISTS (SELECT * FROM   OpportunityBizDocs WHERE  numOppID = @numOppId AND numBizDocId = @numBizDocId AND bitPartialFulfilment = 0)
         AND NOT EXISTS (SELECT * FROM   OpportunityBizDocs WHERE  numOppID = @numOppId AND numBizDocId = @numBizDocId AND bitPartialFulfilment = 1))
     OR (@bitRecurringBizDoc = 1) -- Added new flag to add unit qty in case of recurring bizdoc
     OR (@numFromOppBizDocsId >0)
    )
begin                      
		
		
		--DECLARE  @vcBizDocID  AS VARCHAR(100)
        --DECLARE  @numBizMax  AS NUMERIC(9)
        DECLARE @tintShipped AS TINYINT	
        DECLARE @dtShipped AS DATETIME
        DECLARE @bitAuthBizdoc AS BIT
        
        
		select @tintShipped=ISNULL(tintShipped,0),@numDomainID=numDomainID from OpportunityMaster where numOppId=@numOppId                        
		
		IF @tintOppType = 1 AND @bitNotValidateBizDocFulfillment=0
			EXEC USP_ValidateBizDocFulfillment @numDomainID,@numOppId,@numFromOppBizDocsId,@numBizDocId
		
--		Select @numBizMax=count(*) from  OpportunityBizDocs                        
--		IF @numBizMax > 0
--          BEGIN
--				SELECT @numBizMax = MAX(numOppBizDocsId) FROM   OpportunityBizDocs
--          END
--		SET @numBizMax = @numBizMax
--        SET @vcBizDocID = @vcBizDocID + '-BD-' + CONVERT(VARCHAR(10),@numBizMax)
        IF @tintShipped =1 
			SET @dtShipped = GETUTCDATE();
		ELSE
			SET @dtShipped = null;
		IF @tintOppType = 1 AND (SELECT numAuthoritativeSales FROM [AuthoritativeBizDocs] WHERE numDomainID = @numDomainID) = @numBizDocId
			SET @bitAuthBizdoc = 1
		ELSE if @tintOppType = 2 AND (SELECT numAuthoritativePurchase FROM [AuthoritativeBizDocs] WHERE numDomainID = @numDomainID) = @numBizDocId
			SET @bitAuthBizdoc = 1
		ELSE 
			SET @bitAuthBizdoc = 0
		
		Declare @dtCreatedDate as datetime;SET @dtCreatedDate=Getutcdate()
		IF @tintDeferred=1
		BEGIn
			SET @dtCreatedDate=DateAdd(minute, @ClientTimeZoneOffset,@dtFromDate)
		END
	
	
		IF @bitTakeSequenceId=1
		BEGIN
			--Sequence #
			CREATE TABLE #tempSequence (numSequenceId bigint )
			INSERT INTO #tempSequence EXEC [dbo].[USP_GetScalerValue] @numDomainID ,33,@numBizDocId,0
			SELECT @numSequenceId =ISNULL(numSequenceId,0) FROM #tempSequence
			DROP TABLE #tempSequence
			
			--BizDoc Template ID
			CREATE TABLE #tempBizDocTempID (numBizDocTempID NUMERIC(9) )
			INSERT INTO #tempBizDocTempID EXEC [dbo].[USP_GetBizDocTemplateList] @numDomainID,@numBizDocId,1,1
			SELECT @numBizDocTempID =ISNULL(numBizDocTempID,0) FROM #tempBizDocTempID
			DROP TABLE #tempBizDocTempID
		END	
			
			
		IF @numFromOppBizDocsId>0 --If Created from Sales Fulfillment Workflow
		BEGIN
			INSERT INTO OpportunityBizDocs
                   (numOppId,numBizDocId,numCreatedBy,dtCreatedDate,numModifiedBy,dtModifiedDate,vcComments,bitPartialFulfilment,
                    monShipCost,numShipVia,vcTrackingURL,dtFromDate,numBizDocStatus,[dtShippedDate],
                    [bitAuthoritativeBizDocs],tintDeferred,bitRentalBizDoc,numBizDocTempID,/*vcTrackingNo,vcShippingMethod,dtDeliveryDate,*/vcRefOrderNo,numSequenceId,numBizDocStatusOLD,bitAutoCreated)
            select @numOppId,@numBizDocId,@numUserCntID,Getutcdate(),@numUserCntID,Getutcdate(),@vcComments,@bitPartialFulfillment,
                    monShipCost,numShipVia,vcTrackingURL,dtFromDate,0,[dtShippedDate],
                    @bitAuthBizdoc,tintDeferred,bitRentalBizDoc,@numBizDocTempID,/*vcTrackingNo,vcShippingMethod,dtDeliveryDate,*/vcRefOrderNo,@numSequenceId,0,1
		    from OpportunityBizDocs OBD where  numOppId=@numOppId AND OBD.numOppBizDocsId=@numFromOppBizDocsId
		             
		END
		ELSE
		BEGIN
			INSERT INTO OpportunityBizDocs
                   (numOppId,numBizDocId,
                    --vcBizDocID,
                    numCreatedBy,
                    dtCreatedDate,
                    numModifiedBy,
                    dtModifiedDate,
                    vcComments,
                    bitPartialFulfilment,
                    --bitDiscountType,
                    --fltDiscount,
                    --monShipCost,
                    numShipVia,
                    vcTrackingURL,
                    --bitBillingTerms,
                    --intBillingDays,
                    dtFromDate,
                    --bitInterestType,
                    --fltInterest,
--                    numShipDoc,
                    numBizDocStatus,
                    --vcBizDocName,
                    [dtShippedDate],
                    [bitAuthoritativeBizDocs],tintDeferred,bitRentalBizDoc,numBizDocTempID,vcTrackingNo,
                    --vcShippingMethod,dtDeliveryDate,
                    vcRefOrderNo,numSequenceId,numBizDocStatusOLD,fltExchangeRateBizDoc,bitAutoCreated)
        VALUES     (@numOppId,
                    @numBizDocId,
                    --@vcBizDocID,
                    @numUserCntID,
                    @dtCreatedDate,
                    @numUserCntID,
                    Getutcdate(),
                    @vcComments,
                    @bitPartialFulfillment,
                    --@bitDiscountType,
                    --@fltDiscount,
                    --@monShipCost,
                    @numShipVia,
                    @vcTrackingURL,
                    --@bitBillingTerms,
                    --@intBillingDays,
                    @dtFromDate,
                    --@bitInterestType,
                    --@fltInterest,
--                    @numShipDoc,
                    @numBizDocStatus,--@numBizDocStatus,--@numBizDocStatus,
                    --@vcBizDocName,
                    @dtShipped,
                    ISNULL(@bitAuthBizdoc,0),@tintDeferred,@bitRentalBizDoc,@numBizDocTempID,@vcTrackingNo,
                    --@vcShippingMethod,@dtDeliveryDate,
                    @vcRefOrderNo,@numSequenceId,0,@fltExchangeRateBizDoc,0)
        END
        
		SET @numOppBizDocsId = @@IDENTITY
		
		--Deferred BizDocs : Create Recurring entry only for create Account Journal
		if @tintDeferred=1
		BEGIN
			exec USP_RecurringAddOpportunity @numOppId,@numOppBizDocsId,0,4,@dtCreatedDate,0,@numDomainID,0,0,100,''
		END

		-- Update name template set for bizdoc
		--DECLARE @tintType TINYINT
		--SELECT @tintType = CASE @tintOppType WHEN 1 THEN 3 WHEN 2 THEN 4 END;
		EXEC dbo.USP_UpdateBizDocNameTemplate @tintOppType,@numDomainID,@numOppBizDocsId

		IF @numFromOppBizDocsId>0
		BEGIN
			insert into                       
		   OpportunityBizDocItems                                                                          
  		   (numOppBizDocID,numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,vcAttributes,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,vcNotes,/*vcTrackingNo,*/bitEmbeddedCost,monEmbeddedCost,/*monShipCost,vcShippingMethod,dtDeliveryDate,*/dtRentalStartDate,dtRentalReturnDate,tintTrackingStatus)
		   select @numOppBizDocsId,OBDI.numOppItemID,OBDI.numItemCode,OBDI.numUnitHour,OBDI.monPrice,OBDI.monTotAmount,OBDI.vcItemDesc,OBDI.numWarehouseItmsID,OBDI.vcType,OBDI.vcAttributes,OBDI.bitDropShip,OBDI.bitDiscountType,OBDI.fltDiscount,OBDI.monTotAmtBefDiscount,OBDI.vcNotes,/*OBDI.vcTrackingNo,*/OBDI.bitEmbeddedCost,OBDI.monEmbeddedCost,/*OBDI.monShipCost,OBDI.vcShippingMethod,OBDI.dtDeliveryDate,*/OBDI.dtRentalStartDate,OBDI.dtRentalReturnDate,OBDI.tintTrackingStatus
		   from OpportunityBizDocs OBD JOIN OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId=OBDI.numOppBizDocID 
		   JOIN OpportunityItems OI ON OBDI.numOppItemID=OI.numoppitemtCode
		   JOIN Item I ON OBDI.numItemCode=I.numItemCode
		   where  OBD.numOppId=@numOppId AND OBD.numOppBizDocsId=@numFromOppBizDocsId
		   AND OI.numoppitemtCode NOT IN(SELECT OBDI.numOppItemID FROM OpportunityBizDocs OBD JOIN dbo.OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId=OBDI.numOppBizDocId WHERE numOppId=@numOppId AND numBizDocId=@numBizDocId)
		END
		ELSE IF /*@bitPartialFulfillment=1 or*/ (@bitPartialFulfillment=1 and DATALENGTH(@strBizDocItems)>2)
			BEGIN
				
			if DATALENGTH(@strBizDocItems)>2
			begin
				EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strBizDocItems 

					IF @bitRentalBizDoc=1
					BEGIN
						update OI set OI.monPrice=OBZ.monPrice,OI.monTotAmount=OBZ.monTotAmount,OI.monTotAmtBefDiscount=OBZ.monTotAmount
						from OpportunityItems OI
						Join (SELECT * FROM OPENXML (@hDocItem,'/NewDataSet/BizDocItems',2)                                                                          
							WITH ( OppItemID numeric(9),Quantity numeric(18,10),monPrice money,monTotAmount money)) OBZ
						on OBZ.OppItemID=OI.numoppitemtCode
						where OBZ.OppItemID=OI.numoppitemtCode
					END
	
					insert into                       
				   OpportunityBizDocItems                                                                          
				   (numOppBizDocID,numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,vcAttributes,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,vcNotes,/*vcTrackingNo,vcShippingMethod,monShipCost,dtDeliveryDate,*/dtRentalStartDate,dtRentalReturnDate)
				   
				   select @numOppBizDocsId,numoppitemtCode,numItemCode,Quantity,CASE WHEN @tintOppType=2 THEN OBZ.monPrice ELSE OI.monPrice END,CASE WHEN @tintOppType=2 THEN OBZ.monPrice * Quantity ELSE (monTotAmount/numUnitHour)*Quantity END,vcItemDesc,numWarehouseItmsID,vcType,vcAttributes,bitDropShip,bitDiscountType,
				   case when bitDiscountType=0 then fltDiscount when bitDiscountType=1 then (fltDiscount/numUnitHour)*Quantity else fltDiscount end ,CASE WHEN @tintOppType=2 THEN OBZ.monPrice * Quantity ELSE (monTotAmtBefDiscount/numUnitHour)*Quantity END,Notes,/*TrackingNo,vcShippingMethod,monShipCost,dtDeliveryDate,*/dtRentalStartDate,dtRentalReturnDate  from OpportunityItems OI
				   Join (SELECT * FROM OPENXML (@hDocItem,'/NewDataSet/BizDocItems',2)                                                                          
				   WITH                       
				   (OppItemID numeric(9),                               
				    Quantity numeric(18,10),
					Notes varchar(500),
					--TrackingNo varchar(500),
					--vcShippingMethod varchar(100),
					--monShipCost money,
					--dtDeliveryDate datetime
					monPrice MONEY,
					dtRentalStartDate datetime,dtRentalReturnDate datetime
					)) OBZ
				   on OBZ.OppItemID=OI.numoppitemtCode
				   where  numOppId=@numOppId AND ISNULL(OI.numUnitHour,0)>0 AND ISNULL(OI.numUnitHour,0)>0
				   
				EXEC sp_xml_removedocument @hDocItem 
			END

		end
		else
		BEGIN
		   	   insert into OpportunityBizDocItems                                                                          
  			   (numOppBizDocID,numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,vcAttributes,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount)
			   select @numOppBizDocsId,numoppitemtCode,numItemCode,CASE @bitRecurringBizDoc WHEN 1 THEN 1 ELSE numUnitHour END AS numUnitHour ,monPrice,CASE @bitRecurringBizDoc WHEN 1 THEN monPrice * 1 ELSE monTotAmount END AS monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,vcAttributes,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount from OpportunityItems OI
			   where  numOppId=@numOppId AND ISNULL(OI.numUnitHour,0)>0
			   AND numoppitemtCode NOT IN(SELECT OBDI.numOppItemID FROM OpportunityBizDocs OBD JOIN dbo.OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId=OBDI.numOppBizDocId WHERE numOppId=@numOppId AND numBizDocId=@numBizDocId)
		   
			IF @bitAuthBizdoc = 1 and @tintOppType = 1
				exec USP_AutoAssign_OppSerializedItem @numDomainID,@numOppID,@numOppBizDocsId
		end


               
--		insert into OpportunityBizDocTaxItems(numOppBizDocID, numTaxItemID, fltPercentage)
--		select @numOppBizDocsId,numTaxItemID,dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,numTaxItemID,@numOppId,0) from TaxItems
--        where numDomainID= @numDomainID
--        union 
--        select @numOppBizDocsId,0,dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,0,@numOppId,0)

		SET @numOppBizDocsID = @numOppBizDocsId
		--select @numOppBizDocsId
	end
	ELSE
	  BEGIN
  			SET @numOppBizDocsId=0
			SET @monDealAmount = 0
			SET @monCreditAmount = 0
			RAISERROR ('NOT_ALLOWED',16,1);
	  END
--	IF @tintTaxOperator = 3 -- Remove default Sales Tax percentage defined and Add Sales Tax percentage Used for Online Market Place
--		BEGIN
--				DELETE FROM [OpportunityBizDocTaxItems] WHERE [numOppBizDocID] = @numOppBizDocsId AND [numTaxItemID] = 0 
--				insert into OpportunityBizDocTaxItems(numOppBizDocID, numTaxItemID, fltPercentage)
--					select @numOppBizDocsId,0,@OMP_SalesTaxPercent
--		END	
end
else
BEGIN
	  UPDATE OpportunityBizDocs
	  SET    numModifiedBy = @numUserCntID,
			 dtModifiedDate = Getdate(),
			 vcComments = @vcComments,
			 --bitDiscountType = @bitDiscountType,
			 --fltDiscount = @fltDiscount,
			 --monShipCost = @monShipCost,
			 numShipVia = @numShipVia,
			 vcTrackingURL = @vcTrackingURL,
			 --bitBillingTerms = @bitBillingTerms,
			 --intBillingDays = @intBillingDays,
			 dtFromDate = @dtFromDate,
			 --bitInterestType = @bitInterestType,
			 --fltInterest = @fltInterest,
--			 numShipDoc = @numShipDoc,
			 numBizDocStatus = @numBizDocStatus,
			 numSequenceId=@numSequenceId,
			 --[tintTaxOperator]=@tintTaxOperator,
			 numBizDocTempID=@numBizDocTempID,
			 vcTrackingNo=@vcTrackingNo,
			 --vcShippingMethod=@vcShippingMethod,dtDeliveryDate=@dtDeliveryDate,
			 vcRefOrderNo=@vcRefOrderNo,
			 fltExchangeRateBizDoc=@fltExchangeRateBizDoc
			 --,numBizDocStatusOLD=ISNULL(numBizDocStatus,0)
	  WHERE  numOppBizDocsId = @numOppBizDocsId
	  --SELECT @numOppBizDocsId


   	IF DATALENGTH(@strBizDocItems)>2
			BEGIN

				EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strBizDocItems 

                    delete from OpportunityBizDocItems  
					where numOppItemID not in (SELECT OppItemID FROM OPENXML (@hDocItem,'/NewDataSet/BizDocItems',2)                                                                          
					WITH                       
				   ( OppItemID numeric(9)
					)) and numOppBizDocID=@numOppBizDocsId  
                  
                     
					IF @bitRentalBizDoc=1
					BEGIN
						update OI set OI.monPrice=OBZ.monPrice,OI.monTotAmount=OBZ.monTotAmount,OI.monTotAmtBefDiscount=OBZ.monTotAmount
						from OpportunityItems OI
						Join (SELECT * FROM OPENXML (@hDocItem,'/NewDataSet/BizDocItems',2)                                                                          
							WITH ( OppItemID numeric(9),Quantity numeric(18,10),monPrice money,monTotAmount money)) OBZ
						on OBZ.OppItemID=OI.numoppitemtCode
						where OBZ.OppItemID=OI.numoppitemtCode
					END

					update OBI set OBI.numUnitHour= Quantity,OBI.monPrice=CASE WHEN @tintOppType=2 THEN OBZ.monPrice ELSE OI.monPrice END,OBI.monTotAmount=CASE WHEN @tintOppType=2 THEN OBZ.monPrice * Quantity ELSE (OI.monTotAmount/OI.numUnitHour)*Quantity END,OBI.vcNotes= Notes,
					OBI.fltDiscount=case when OI.bitDiscountType=0 then OI.fltDiscount when OI.bitDiscountType=1 then (OI.fltDiscount/OI.numUnitHour)*Quantity else OI.fltDiscount end ,
					OBI.monTotAmtBefDiscount=(OI.monTotAmtBefDiscount/OI.numUnitHour)*Quantity ,                                                                    
					/*OBI.vcTrackingNo=TrackingNo,OBI.vcShippingMethod=OBZ.vcShippingMethod,OBI.monShipCost=OBZ.monShipCost,OBI.dtDeliveryDate=OBZ.dtDeliveryDate*/
				    OBI.dtRentalStartDate=OBZ.dtRentalStartDate,OBI.dtRentalReturnDate=OBZ.dtRentalReturnDate
					from OpportunityBizDocItems OBI
					join OpportunityItems OI
					on OBI.numOppItemID=OI.numoppitemtCode
					Join (SELECT * FROM OPENXML (@hDocItem,'/NewDataSet/BizDocItems',2)                                                                          
					WITH                       
				   ( OppItemID numeric(9),                                     
				   Quantity numeric(18,10),
					Notes varchar(500),
					--TrackingNo varchar(500),
					--vcShippingMethod varchar(100),
					--monShipCost money,
					--dtDeliveryDate datetime,
					monPrice MONEY,
					dtRentalStartDate datetime,dtRentalReturnDate datetime
					)) OBZ
				   on OBZ.OppItemID=OI.numoppitemtCode
				   where  OI.numOppId=@numOppId and OBI.numOppBizDocID=@numOppBizDocsId AND ISNULL(OI.numUnitHour,0)>0


                   insert into                       
				   OpportunityBizDocItems                                                                          
				   (numOppBizDocID,numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,vcAttributes,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,vcNotes/*,vcTrackingNo,vcShippingMethod,monShipCost,dtDeliveryDate*/)                      
				   select @numOppBizDocsId,numoppitemtCode,numItemCode,Quantity,CASE WHEN @tintOppType=2 THEN OBZ.monPrice ELSE OI.monPrice END,CASE WHEN @tintOppType=2 THEN OBZ.monPrice * Quantity ELSE (monTotAmount/numUnitHour)*Quantity END,vcItemDesc,numWarehouseItmsID,vcType,vcAttributes,bitDropShip,bitDiscountType,
				   case when bitDiscountType=0 then fltDiscount when bitDiscountType=1 then (fltDiscount/numUnitHour)*Quantity else fltDiscount end ,(monTotAmtBefDiscount/numUnitHour)*Quantity,Notes/*,TrackingNo,vcShippingMethod,monShipCost,dtDeliveryDate*/ from OpportunityItems OI
				   Join (SELECT * FROM OPENXML (@hDocItem,'/NewDataSet/BizDocItems',2)                                                                          
				   WITH                       
				   ( OppItemID numeric(9),                                     
				   Quantity numeric(18,10),
					Notes varchar(500),
					monPrice MONEY
					--TrackingNo varchar(500),
					--vcShippingMethod varchar(100),
					--monShipCost money,
					--dtDeliveryDate DATETIME
					)) OBZ
				   on OBZ.OppItemID=OI.numoppitemtCode and OI.numoppitemtCode not in (select numOppItemID from OpportunityBizDocItems where numOppBizDocID =@numOppBizDocsId)
				   where  numOppId=@numOppId AND ISNULL(OI.numUnitHour,0)>0
                 
				EXEC sp_xml_removedocument @hDocItem 
			END


		
		/*Note: by chintan
			numTaxItemID= 0 which stands for default sales tax
		*/
--		IF @tintTaxOperator = 1 -- Add Sales Tax Blindly which doesn't  depends on if company/item has sales tax enabled
--		BEGIN
--				DECLARE @SalesTax float
--				SELECT @SalesTax = dbo.fn_CalSalesTaxAmt(@numOppBizDocsId,@numDomainID);
--				IF @SalesTax > 0 
--				BEGIN
--					DELETE FROM [OpportunityBizDocTaxItems] WHERE [numOppBizDocID]=@numOppBizDocsId AND [numTaxItemID]=0
--					insert into OpportunityBizDocTaxItems(numOppBizDocID, numTaxItemID, fltPercentage)
--					select @numOppBizDocsId,0,@SalesTax
--				END
--				
--		END
--		ELSE IF @tintTaxOperator = 2 -- Remove Sales Tax Blindly which doesn't  depends on if company/item has sales tax enabled
--		BEGIN
--				DELETE FROM [OpportunityBizDocTaxItems] WHERE [numOppBizDocID] = @numOppBizDocsId AND [numTaxItemID] = 0 
--		END
END

IF @numOppBizDocsId>0
BEGIN
	select @monDealAmount=isnull(dbo.[GetDealAmount](@numOppId,GETUTCDATE(),@numOppBizDocsId),0)

	--Credit Balance
--	Declare @CAError as int;Set @CAError=0
--
--   	Declare @monOldCreditAmount as money;Set @monOldCreditAmount=0
--	Select @monOldCreditAmount=isnull(monCreditAmount,0) from OpportunityBizDocs WHERE  numOppBizDocsId = @numOppBizDocsId
--
--	if @monCreditAmount>0
--	BEGIN
--			IF (@monDealAmount + @monOldCreditAmount) >= @monCreditAmount
--			BEGIN
--				IF exists (select * from [CreditBalanceHistory] where  numOppBizDocsId=@numOppBizDocsId)
--						Update [CreditBalanceHistory] set [monAmount]=@monCreditAmount * -1 where numOppBizDocsId=@numOppBizDocsId
--				ELSE
--						 INSERT INTO [CreditBalanceHistory]
--							(numOppBizDocsId,[monAmount],[dtCreateDate],[dtCreatedBy],[numDomainId],numDivisionID)
--						VALUES (@numOppBizDocsId,@monCreditAmount * -1,GETDATE(),@numUserCntID,@numDomainId,@numDivisionID)
--			END	
--			ELSE
--			BEGIN	
--				SET @monCreditAmount=0
--				SET @CAError=-1
--			END
--	  END
--	  ELSE
--	  BEGIN
--			delete from [CreditBalanceHistory] where numOppBizDocsId=@numOppBizDocsId
--	  END
--	
--	IF @tintOppType=1 --Sales
--	  update DivisionMaster set monSCreditBalance=isnull(monSCreditBalance,0) + isnull(@monOldCreditAmount,0) - isnull(@monCreditAmount,0) where numDivisionID=@numDivisionID and numDomainID=@numDomainID
--    else IF @tintOppType=2 --Purchase
--	  update DivisionMaster set monPCreditBalance=isnull(monPCreditBalance,0) + isnull(@monOldCreditAmount,0) - isnull(@monCreditAmount,0) where numDivisionID=@numDivisionID and numDomainID=@numDomainID
--		

	  --Update OpportunityBizDocs set monCreditAmount=@monCreditAmount   WHERE  numOppBizDocsId = @numOppBizDocsId
	  Update OpportunityBizDocs set vcBizDocID=vcBizDocName + ISNULL(@numSequenceId,''),
			monDealAmount=@monDealAmount
			WHERE  numOppBizDocsId = @numOppBizDocsId

	 --SET @monCreditAmount =Case when @CAError=-1 then -1 else @monCreditAmount END
END
 
	 END TRY
BEGIN CATCH
    DECLARE @ErrorMessage NVARCHAR(4000);
    DECLARE @ErrorSeverity INT;
    DECLARE @ErrorState INT;

    SELECT 
        @ErrorMessage = ERROR_MESSAGE(),
        @ErrorSeverity = ERROR_SEVERITY(),
        @ErrorState = ERROR_STATE();

    -- Use RAISERROR inside the CATCH block to return error
    -- information about the original error that caused
    -- execution to jump to the CATCH block.
    RAISERROR (@ErrorMessage, -- Message text.
               @ErrorSeverity, -- Severity.
               @ErrorState -- State.
               );
END CATCH;
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_CreateTreeNavigationForDomain' ) 
    DROP PROCEDURE USP_CreateTreeNavigationForDomain
GO
-- exec USP_CreateTreeNavigationForDomain 1
CREATE PROCEDURE [dbo].[USP_CreateTreeNavigationForDomain] 
@numDomainID NUMERIC,
@numPageNavID numeric=0 -- Suppy this parameter when you want to give tree node permissoin to all domain and all user groups by default.
AS 
    SET NOCOUNT ON
    SET TRANSACTION ISOLATION LEVEL READ COMMITTED

    BEGIN
    
    IF ISNULL(@numPageNavID,0) = 0 
    BEGIN
    	
   
    
		DELETE FROM TreeNavigationAuthorization WHERE numDomainID = @numDomainID
        SELECT  *
        INTO    #tempData
        FROM    ( SELECT    T.numTabID,
                            CASE WHEN bitFixed = 1
                                 THEN CASE WHEN EXISTS ( SELECT numTabName
                                                         FROM   TabDefault
                                                         WHERE  numTabId = T.numTabId
                                                                AND tintTabType = T.tintTabType AND numDomainID = @numDomainID)
                                           THEN ( SELECT TOP 1
                                                            numTabName
                                                  FROM      TabDefault
                                                  WHERE     numTabId = T.numTabId
                                                            AND tintTabType = T.tintTabType AND numDomainID = @numDomainID
                                                )
                                           ELSE T.numTabName
                                      END
                                 ELSE T.numTabName
                            END numTabname,
                            vcURL,
                            bitFixed,
                            1 AS tintType,
                            T.vcImage,
                            D.numDomainID,
                            A.numGroupID
                  FROM      TabMaster T
                            CROSS JOIN Domain D --ON D.numDomainID = T.numDomainID
                            LEFT JOIN dbo.AuthenticationGroupMaster A ON A.numDomainID = D.numDomainId
                  WHERE     bitFixed = 1
                            AND D.numDomainId <> -255
                            AND t.tintTabType = 1
                            AND D.numDomainID = @numDomainID
                  UNION ALL
                  SELECT    -1 AS [numTabID],
                            'Administration' numTabname,
                            '' vcURL,
                            1 bitFixed,
                            1 AS tintType,
                            '' AS vcImage,
                            D.numDomainID,
                            A.numGroupID
                  FROM      Domain D
                            JOIN dbo.AuthenticationGroupMaster A ON A.numDomainID = D.numDomainId
                            WHERE D.numDomainID = @numDomainID
				  
				  UNION ALL
				  SELECT    -3 AS [numTabID],
							'Advanced Search' numTabname,
							'' vcURL,
							1 bitFixed,
							1 AS tintType,
							'' AS vcImage,
							D.numDomainID,
							A.numGroupID
				  FROM      Domain D
							JOIN dbo.AuthenticationGroupMaster A ON A.numDomainID = D.numDomainId	                             
                ) X 

        INSERT  INTO dbo.TreeNavigationAuthorization
                (
                  numGroupID,
                  numTabID,
                  numPageNavID,
                  bitVisible,
                  numDomainID,
                  tintType
                )
                SELECT  numGroupID,
                        PND.numTabID,
                        numPageNavID,
                        bitVisible,
                        numDomainID,
                        tintType
                FROM    dbo.PageNavigationDTL PND
                        JOIN #tempData TD ON TD.numTabID = PND.numTabID 
                        WHERE TD.numDomainID = @numDomainID
				
				UNION ALL 
				
				SELECT  numGroupID AS [numGroupID],
						1 AS [numTabID],
						ISNULL(LD.[numListItemID], 0) AS [numPageNavID],
						1 AS [bitVisible],
						@numDomainID AS [numDomainID],
						1 AS [tintType]
				FROM    dbo.ListDetails LD
				CROSS JOIN dbo.AuthenticationGroupMaster AG
				WHERE   numListID = 27
						AND ISNULL(constFlag, 0) = 1
						AND AG.numDomainID = @numDomainID
						AND numListItemID NOT IN (293,294,295,297,298,299)
--SELECT * FROM #tempData WHERE numGroupID = 1 ORDER BY numDomainID
        DROP TABLE #tempData        	
        
	END
	
	------------------------------------------
	IF ISNULL(@numPageNavID,0) > 0 
	BEGIN

		
		--Delete all existing tree permission from all domains for selected page
			DELETE FROM TreeNavigationAuthorization WHERE numPageNavID=@numPageNavID
		
		
			 SELECT    T.numTabID,
                            CASE WHEN bitFixed = 1
                                 THEN CASE WHEN EXISTS ( SELECT numTabName
                                                         FROM   TabDefault
                                                         WHERE  numTabId = T.numTabId
                                                                AND tintTabType = T.tintTabType AND numDomainID =D.numDomainID)
                                           THEN ( SELECT TOP 1
                                                            numTabName
                                                  FROM      TabDefault
                                                  WHERE     numTabId = T.numTabId
                                                            AND tintTabType = T.tintTabType AND numDomainID = D.numDomainID
                                                )
                                           ELSE T.numTabName
                                      END
                                 ELSE T.numTabName
                            END numTabname,
                            vcURL,
                            bitFixed,
                            1 AS tintType,
                            T.vcImage,
                            D.numDomainID,
                            A.numGroupID
				  INTO    #tempData1
                  FROM      TabMaster T
                            CROSS JOIN Domain D --ON D.numDomainID = T.numDomainID
                            LEFT JOIN dbo.AuthenticationGroupMaster A ON A.numDomainID = D.numDomainId
                  WHERE     bitFixed = 1
                            AND D.numDomainId > 0
                            AND t.tintTabType = 1
                            
                            

		
--			SELECT * FROM #tempData
		
                            
			INSERT  INTO dbo.TreeNavigationAuthorization
                (
                  numGroupID,
                  numTabID,
                  numPageNavID,
                  bitVisible,
                  numDomainID,
                  tintType
                )
                SELECT  numGroupID,
                        PND.numTabID,
                        numPageNavID,
                        bitVisible,
                        numDomainID,
                        tintType
                FROM    dbo.PageNavigationDTL PND
                        JOIN #tempData1 TD ON TD.numTabID = PND.numTabID 
                        WHERE PND.numPageNavID= @numPageNavID
			
              DROP TABLE #tempData1
                          
		SELECT * FROM dbo.TreeNavigationAuthorization WHERE numPageNavID=@numPageNavID
	END
	
        
    END    
   

 
GO
IF EXISTS(SELECT * FROM sysobjects SYSOBJ WHERE SYSOBJ.xtype='p'AND SYSOBJ.NAME ='USP_GET_DROPDOWN_DATA_FOR_IMPORT')
DROP PROCEDURE USP_GET_DROPDOWN_DATA_FOR_IMPORT
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- USP_GET_DROPDOWN_DATA_FOR_IMPORT 75,1
CREATE PROCEDURE USP_GET_DROPDOWN_DATA_FOR_IMPORT
(
	@intImportFileID    BIGINT,
	@numDomainID		BIGINT
)
AS 
BEGIN
	
	DECLARE @numFormID AS NUMERIC
	
	SELECT @numFormID = numMasterID  FROM dbo.Import_File_Master  WHERE intImportFileID=@intImportFileID
	
	
	SELECT ROW_NUMBER() OVER (ORDER BY intImportFileID) AS [ID],* 
			INTO #tempDropDownData FROM(
					SELECT  PARENT.intImportFileID,
							vcImportFileName,
							PARENT.numDomainID,
							DFFM.numFieldId [numFormFieldId],
							DFFM.vcAssociatedControlType,
							DYNAMIC_FIELD.vcDbColumnName,
							DFFM.vcFieldName [vcFormFieldName],
							FIELD_MAP.intMapColumnNo,
							DYNAMIC_FIELD.numListID,
							0 AS [bitCustomField]					
					FROM Import_File_Master PARENT
					INNER JOIN Import_File_Field_Mapping FIELD_MAP ON PARENT.intImportFileID = FIELD_MAP.intImportFileID
					--INNER JOIN dbo.DynamicFormFieldMaster DYNAMIC_FIELD ON FIELD_MAP.numFormFieldID = DYNAMIC_FIELD.numFormFieldId
					INNER JOIN dbo.DycFormField_Mapping DFFM ON FIELD_MAP.numFormFieldID = DFFM.numFieldId
					INNER JOIN dbo.DycFieldMaster DYNAMIC_FIELD ON DYNAMIC_FIELD.numFieldId = DFFM.numFieldId
					WHERE DFFM.vcAssociatedControlType ='SelectBox'
					  AND PARENT.intImportFileID = @intImportFileID
					  AND PARENT.numDomainID = @numDomainID   
					  AND DFFM.numFormID = @numFormID
					
					UNION ALL
					
						  SELECT    FIELD_MAP.intImportFileID AS [intImportFileID],
									cfm.Fld_label  AS [vcImportFileName],
									numDomainID AS [numDomainID],
									CFm.Fld_id AS [numFormFieldId],
									CASE WHEN CFM.Fld_type = 'Drop Down List Box'
										 THEN 'SelectBox'
										 ELSE CFM.Fld_type 
									END	 AS [vcAssociatedControlType],
									cfm.Fld_label AS [vcDbColumnName],
									cfm.Fld_label AS [vcFormFieldName],
									FIELD_MAP.intMapColumnNo  AS [intMapColumnNo],
									CFM.numlistid AS [numListID],
									1 AS [bitCustomField]
						  FROM      dbo.CFW_Fld_Master CFM
						  INNER JOIN Import_File_Field_Mapping FIELD_MAP ON CFM.Fld_id = FIELD_MAP.numFormFieldID		
						  WHERE FIELD_MAP.intImportFileID = @intImportFileID     
						    AND numDomainID = @numDomainID) TABLE1 WHERE  vcAssociatedControlType = 'SelectBox' 
                    
	SELECT * FROM #tempDropDownData   
	
	DECLARE @intRow AS INT
	SELECT @intRow = COUNT(*) FROM #tempDropDownData
	PRINT @intRow
	
	DECLARE @intCnt AS INT
	DECLARE @numListIDValue AS BIGINT
	DECLARE @numFormFieldIDValue AS BIGINT
	DECLARE @bitCustomField AS BIT
		
	DECLARE @strSQL AS NVARCHAR(MAX)
	DECLARE @vcTableNames AS VARCHAR(MAX) 
	DECLARE @strDBColumnName AS NVARCHAR(1000)
	
	SET @intCnt = 0
	SET @strSQL = '' 
	SET @vcTableNames = ''

	CREATE TABLE #tempGroups
	(
		[Key] NUMERIC(9),
		[Value] VARCHAR(100)
	) 	
	INSERT INTO #tempGroups([Key],[Value]) 
	SELECT 0 AS [Key],'Groups' AS [Value]
	UNION ALL
	SELECT 1 AS [Key],'Prospects' AS [Value]
	UNION ALL
	SELECT 1 AS [Key],'Prospect' AS [Value]
	UNION ALL
	SELECT 1 AS [Key],'Pros' AS [Value]
	UNION ALL
	SELECT 2 AS [Key],'Accounts' AS [Value]
	UNION ALL
	SELECT 2 AS [Key],'Account' AS [Value]
	UNION ALL
	SELECT 2 AS [Key],'Acc' AS [Value]

	--SELECT * FROM #tempGroups
	
	WHILE(@intCnt < @intRow   )
		BEGIN
			SET @intCnt = @intCnt + 1
				
			SELECT  
					@numListIDValue = numListID,
					@strDBColumnName = vcDbColumnName, 
					@numFormFieldIDValue = numFormFieldID,
					@bitCustomField = bitCustomField 	 
			FROM #tempDropDownData 
			WHERE ID = @intCnt
			  AND intImportFileID = @intImportFileID
			
			PRINT 'BIT'
			PRINT @bitCustomField
			PRINT @strDBColumnName
			SELECT @strSQL = CASE  
								WHEN @strDBColumnName = 'numItemGroup'
								THEN @strSQL + 'SELECT ITEM_GROUP.numItemGroupID AS [Key],ITEM_GROUP.vcItemGroup AS [Value] FROM dbo.ItemGroups ITEM_GROUP WHERE ITEM_GROUP.numDomainID = ' + CONVERT(VARCHAR,@numDomainID)
								
								WHEN @strDBColumnName = 'tintCRMType'
								THEN @strSQL + 'SELECT DISTINCT [Key],[Value] FROM #tempGroups '
								
								WHEN @strDBColumnName = 'vcLocation'
								THEN @strSQL + 'SELECT numWLocationID AS [Key],vcLocation AS [Value] FROM dbo.WarehouseLocation WHERE numDomainID = ' + CONVERT(VARCHAR,@numDomainID)
								
								WHEN @strDBColumnName = 'vcExportToAPI'
								THEN @strSQL + 'SELECT DISTINCT WebApiId AS [Key],vcProviderName AS [Value] FROM dbo.WebAPI '
								
								WHEN @strDBColumnName = 'numCategoryID'
								THEN @strSQL + 'SELECT numCategoryID AS [Key], vcCategoryName AS [Value] FROM dbo.Category WHERE numDomainID =' + CONVERT(VARCHAR,@numDomainID)
								
								WHEN @strDBColumnName = 'numGrpID'
								THEN @strSQL + 'SELECT GRP.numGrpID AS [Key],GRP.vcGrpName AS [Value] FROM dbo.Groups GRP ' 
											    			    
								WHEN @strDBColumnName = 'numWareHouseID' --OR @strDBColumnName = 'numWareHouseItemID'															  
								THEN @strSQL + 'SELECT WAREHOUSE.numWareHouseID AS [Key],WAREHOUSE.vcWareHouse AS [Value] FROM dbo.Warehouses WAREHOUSE WHERE WAREHOUSE.numDomainID = '+ CONVERT(VARCHAR,@numDomainID) 
								
								WHEN @strDBColumnName = 'numWareHouseItemID'
								THEN @strSQL + 'SELECT WI.numWareHouseItemID AS [Key],W.vcWareHouse AS [Value] FROM dbo.WareHouseItems WI INNER JOIN dbo.Warehouses W ON WI.numWareHouseID = W.numWareHouseID WHERE W.numDomainID = '+ CONVERT(VARCHAR,@numDomainID)
												
								WHEN @strDBColumnName = 'numChildItemID' OR @strDBColumnName = 'numItemKitID'
								THEN @strSQL + ' SELECT ITEM.numItemCode AS [Key],ITEM.vcItemName AS [Value] FROM dbo.Item ITEM WHERE ITEM.numDomainID = '+ CONVERT(VARCHAR,@numDomainID)
								
								WHEN (@strDBColumnName = 'numUOMId' OR @strDBColumnName = 'numPurchaseUnit' OR @strDBColumnName = 'numSaleUnit' OR @strDBColumnName = 'numBaseUnit') 
								THEN @strSQL + '  SELECT numUOMId AS [Key], vcUnitName AS [Value] FROM dbo.UOM
												  JOIN Domain d ON dbo.UOM.numDomainID = d.numDomainID 
												  WHERE dbo.UOM.numDomainId = ' + CONVERT(VARCHAR(10),@numDomainID) +' 
												  AND bitEnabled=1 
												  AND dbo.UOM.tintUnitType=(CASE WHEN ISNULL(d.charUnitSystem,''E'')=''E'' THEN 1 WHEN d.charUnitSystem=''M'' THEN 2 END) '	
								
								WHEN (@strDBColumnName = 'numIncomeChartAcntId' OR @strDBColumnName = 'numAssetChartAcntId' OR @strDBColumnName = 'numCOGsChartAcntId')
								THEN @strSQL + '  SELECT numAccountId AS [Key], vcAccountName AS [Value] FROM dbo.Chart_Of_Accounts WHERE numDomainID= ' + CONVERT(VARCHAR(10),@numDomainID)

								WHEN @strDBColumnName = 'numCampaignID'
								THEN @strSQL + '  SELECT numCampaignID AS [Key], vcCampaignName AS [Value] FROM  CampaignMaster WHERE CampaignMaster.numDomainID = ' + CONVERT(VARCHAR(10),@numDomainID)
								
								WHEN @strDBColumnName = 'numVendorID'
								THEN @strSQL + '  SELECT DM.numDivisionID AS [Key], CI.vcCompanyName AS [Value] FROM dbo.CompanyInfo CI INNER JOIN dbo.DivisionMaster DM ON DM.numCompanyID = CI.numCompanyId 
												  WHERE DM.numDomainID = ' + CONVERT(VARCHAR(10),@numDomainID)
								
								WHEN @strDBColumnName = 'numAssignedTo' OR @strDBColumnName = 'numRecOwner' 
								THEN @strSQL + '  SELECT A.numContactID AS [Key], A.vcFirstName+'' ''+A.vcLastName AS [Value] from UserMaster UM join AdditionalContactsInformation A  on UM.numUserDetailId=A.numContactID  where UM.numDomainID = A.numDomainID AND UM.numDomainID = ' + CONVERT(VARCHAR,@numDomainID)	
								
------								 SELECT A.numContactID,A.vcFirstName+' '+A.vcLastName as vcUserName from UserMaster UM         
------								 join AdditionalContactsInformation A        
------								 on UM.numUserDetailId=A.numContactID          
------								 where UM.numDomainID=@numDomainID and UM.numDomainID=A.numDomainID   
							
								WHEN @strDBColumnName = 'numTermsID'
								THEN @strSQL + 'SELECT DISTINCT numTermsID AS [Key],vcTerms AS [Value] FROM BillingTerms 
												WHERE numDomainID = ' + CONVERT(VARCHAR,@numDomainID) 
												
								WHEN @strDBColumnName = 'numShipState' OR @strDBColumnName = 'numBillState' OR @strDBColumnName = 'numState' OR @strDBColumnName = 'State'
								THEN @strSQL + '  SELECT numStateID AS [Key], vcState AS [Value] FROM State where numDomainID=' + CONVERT(VARCHAR,@numDomainID)
								
								WHEN @strDBColumnName = 'charItemType'
								THEN @strSQL + ' SELECT ''A'' AS [Key], ''asset'' AS [Value]
											     UNION 				
											     SELECT ''P'' AS [Key], ''inventory item'' AS [Value]
											     UNION 				
											     SELECT ''S'' AS [Key], ''service'' AS [Value]
											     UNION 				
											     SELECT ''N'' AS [Key], ''non-inventory item'' AS [Value]'	
								
								WHEN @strDBColumnName = 'tintRuleType'
								THEN @strSQL + ' SELECT 1 AS [Key], ''Deduct from List price'' AS [Value]
											     UNION 				
											     SELECT 2 AS [Key], ''Add to Primary Vendor Cost'' AS [Value]
											     UNION 				
											     SELECT 3 AS [Key], ''Named Price'' AS [Value]'	
											     
								WHEN @strDBColumnName = 'tintDiscountType'
								THEN @strSQL + ' SELECT 1 AS [Key], ''Percentage'' AS [Value]
											     UNION 				
											     SELECT 2 AS [Key], ''Flat Amount'' AS [Value]'												     
											     																		    
								ELSE 
									 @strSQL + ' SELECT LIST.numListItemID AS [Key],vcData AS [Value] FROM ListDetails LIST LEFT JOIN listorder LIST_ORDER ON LIST.numListItemID = LIST_ORDER.numListItemID AND LIST_ORDER.numDomainId = ' + CONVERT(VARCHAR,@numDomainID) + ' WHERE ( LIST.numDomainID = ' + CONVERT(VARCHAR,@numDomainID) + ' OR LIST.constFlag = 1) AND LIST.numListID = ' + CONVERT(VARCHAR,@numListIDValue) + ' AND  (LIST.numDomainID = ' + CONVERT(VARCHAR,@numDomainID) + ' OR constFlag = 1 )  ORDER BY intSortOrder '
						     END						
						     
			SET @vcTableNames = @vcTableNames + CONVERT(VARCHAR,@numFormFieldIDValue) + '_' + @strDBColumnName + ',' 
		END
	
	PRINT @vcTableNames
	PRINT @strSQL	
	
	SELECT @vcTableNames AS TableNames
	EXEC SP_EXECUTESQL @strSQL		
	
	DROP TABLE #tempDropDownData
	DROP TABLE #tempGroups
END
GO
/****** Object:  StoredProcedure [dbo].[USP_GET_DYNAMIC_IMPORT_FIELDS]  ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO

IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GET_DYNAMIC_IMPORT_FIELDS' ) 
    DROP PROCEDURE USP_GET_DYNAMIC_IMPORT_FIELDS
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- USP_GET_DYNAMIC_IMPORT_FIELDS 48,2,40,1
CREATE PROCEDURE [dbo].[USP_GET_DYNAMIC_IMPORT_FIELDS]
    (
      @numFormID BIGINT,
      @intMode INT,
      @numImportFileID NUMERIC = 0,
      @numDomainID NUMERIC = 0
    )
AS 
    BEGIN
	-- EXEC USP_GET_DYNAMIC_IMPORT_FIELDS 20,2,0,1  -> Item
	-- EXEC USP_GET_DYNAMIC_IMPORT_FIELDS 20,2,0,72  -> Item
	-- EXEC USP_GET_DYNAMIC_IMPORT_FIELDS 54,2,1,1  -> Item Images
	-- EXEC USP_GET_DYNAMIC_IMPORT_FIELDS 31,2,1,1  -> Item Assembly
	-- EXEC USP_GET_DYNAMIC_IMPORT_FIELDS 54,2,0,1  -> Item Vendor
	-- EXEC USP_GET_DYNAMIC_IMPORT_FIELDS 36,2,1,1  -> Organization 
	-- EXEC USP_GET_DYNAMIC_IMPORT_FIELDS 43,2,1,1  -> Organization Correspondence
	-- EXEC USP_GET_DYNAMIC_IMPORT_FIELDS 48,2,1,1  -> Item WareHouse
	-- EXEC USP_GET_DYNAMIC_IMPORT_FIELDS 98,2,1,1  -> Address Details
		
        IF @intMode = 1 -- TO BIND CATEGORY DROP DOWN
            BEGIN

                SELECT  20 AS [intImportMasterID],
                        'Item' AS [vcImportName]
                UNION ALL
                SELECT  48 AS [intImportMasterID],
                        'Item WareHouse' AS [vcImportName]
                UNION ALL
                SELECT  31 AS [intImportMasterID],
                        'Kit sub-items' AS [vcImportName]
                UNION ALL
                SELECT  54 AS [intImportMasterID],
                        'Item Images' AS [vcImportName]
                UNION ALL
                SELECT  55 AS [intImportMasterID],
                        'Item Vendor' AS [vcImportName]
                UNION ALL
                SELECT  65 AS [intImportMasterID],
                        'Item Web API Details' AS [vcImportName]
                UNION ALL
                SELECT  36 AS [intImportMasterID],
                        'Organization' AS [vcImportName]
                UNION ALL
                SELECT  43 AS [intImportMasterID],
                        'Organization Correspondence' AS [vcImportName]                        
                UNION ALL
                SELECT  98 AS [intImportMasterID],
                        'Address Details' AS [vcImportName]                        		
            END
	
        IF @intMode = 2 -- TO GET DETAILS OF DYNAMIC FORM FIELDS
            BEGIN
	
		
                SELECT DISTINCT
                        intSectionID AS [Id],
                        vcSectionName AS [Data]
                FROM    dbo.DycFormSectionDetail
                WHERE   numFormID = @numFormID			
		
		
		/*SELECT DISTINCT  DMAP.intSectionID AS [Id], 
						 DFSD.vcSectionName AS [Data]					 
		FROM dbo.DycFormField_Mapping DMAP 
		INNER JOIN dbo.DycFormSectionDetail DFSD ON DFSD.intSectionID = DMAP.intSectionID
											   AND DFSD.numFormID = DMAP.numFormID
		WHERE DMAP.numFormID = @numFormID */
		
                SELECT  ROW_NUMBER() OVER ( ORDER BY X.intSectionID, X.numFormFieldID ) AS SrNo,
                        X.*
                FROM    ( SELECT    ISNULL(( SELECT intImportTransactionID
                                             FROM   dbo.Import_File_Field_Mapping FFM
                                                    INNER JOIN Import_File_Master IFM ON IFM.intImportFileID = FFM.intImportFileID
                                                                                         AND IFM.intImportFileID = @numImportFileID
                                             WHERE  FFM.numFormFieldID = DCOL.numFieldID
                                           ), 0) AS [intImportFieldID],
                                    DCOL.numFieldID AS [numFormFieldID],
                                    '' AS [vcSectionNames],
                                    DCOL.vcFieldName AS [vcFormFieldName],
                                    DCOL.vcDbColumnName,
                                    DCOL.intSectionID AS [intSectionID],
                                    0 [IsCustomField],
                                    ISNULL(( SELECT intMapColumnNo
                                             FROM   dbo.Import_File_Field_Mapping FFM
                                             WHERE  1 = 1
                                                    AND FFM.numFormFieldID = DCOL.numFieldID
                                                    AND FFM.intImportFileID = @numImportFileID
                                           ), 0) intMapColumnNo,
                                    DCOL.vcPropertyName,
                                    CASE WHEN ISNULL(DCOL.bitRequired, 0) = 0
                                         THEN ''
                                         ELSE '*'
                                    END AS [vcReqString],
                                    DCOL.bitRequired AS [bitRequired],
                                    CAST(DCOL.numFieldID AS VARCHAR) + '~'
                                    + CAST(0 AS VARCHAR(10)) AS [keyFieldID],
                                    DCOL.tintOrder
                          FROM      dbo.View_DynamicDefaultColumns DCOL
                          WHERE     DCOL.numDomainID = @numDomainID
                                    AND DCOL.numFormID = @numFormID
                                    AND ISNULL(DCOL.bitImport, 0) = 1
                                    AND ISNULL(DCOL.bitDeleted, 0) = 0
--                          UNION ALL
--                          SELECT    ISNULL(( SELECT intImportTransactionID
--                                             FROM   dbo.Import_File_Field_Mapping FFM
--                                                    INNER JOIN Import_File_Master IFM ON IFM.intImportFileID = FFM.intImportFileID
--                                                                                         AND IFM.intImportFileID = @numImportFileID
--                                             WHERE  FFM.numFormFieldID = DCOL.numFieldID
--                                           ), 0) AS [intImportFieldID],
--                                    DCOL.numFieldID AS [numFormFieldID],
--                                    '' AS [vcSectionNames],
--                                    DCOL.vcFieldName AS [vcFormFieldName],
--                                    DCOL.vcDbColumnName,
--                                    DCOL.intSectionID AS [intSectionID],
--                                    0 [IsCustomField],
--                                    ISNULL(( SELECT intMapColumnNo
--                                             FROM   dbo.Import_File_Field_Mapping FFM
--                                             WHERE  1 = 1
--                                                    AND FFM.numFormFieldID = DCOL.numFieldID
--                                                    AND FFM.intImportFileID = @numImportFileID
--                                           ), 0) intMapColumnNo,
--                                    DCOL.vcPropertyName,
--                                    CASE WHEN ISNULL(DCOL.bitRequired, 0) = 0
--                                         THEN ''
--                                         ELSE '*'
--                                    END AS [vcReqString],
--                                    DCOL.bitRequired AS [bitRequired],
--                                    CAST(DCOL.numFieldID AS VARCHAR) + '~'
--                                    + CAST(0 AS VARCHAR(10)) AS [keyFieldID],
--                                    ISNULL(DCOL.tintOrder, 100) AS tintOrder
--                          FROM      dbo.View_DynamicDefaultColumns DCOL
--                          WHERE     DCOL.numDomainID = @numDomainID
--                                    AND DCOL.numFormID = @numFormID
--                                    AND ISNULL(DCOL.bitImport, 0) = 0
--                                    AND ISNULL(DCOL.bitDeleted, 0) = 0
                          UNION ALL
                          SELECT    -1,
                                    CFm.Fld_id,
                                    'CustomField',
                                    cfm.Fld_label,
                                    cfm.Fld_label,
                                    DFSD.intSectionId,
                                    1,
                                    ISNULL(( SELECT intMapColumnNo
                                             FROM   dbo.Import_File_Field_Mapping IIFM
                                                    LEFT JOIN dbo.DycFormField_Mapping DMAP ON IIFM.numFormFieldID = DMAP.numFieldID
                                             WHERE  IIFM.intImportFileID = @numImportFileID
                                                    AND IIFM.numFormFieldID = CFM.Fld_id
                                                    AND bitCustomField = 1
                                                    AND ISNULL(DMAP.bitImport, 0) = 1
                                           ), 0) intMapColumnNo,
                                    '' AS [vcPropertyName],
                                    '' AS [vcReqString],
                                    0 AS [bitRequired],
                                    CAST(CFm.Fld_id AS VARCHAR) + '~'
                                    + CAST(1 AS VARCHAR(10)) AS [keyFieldID],
                                    100 AS [tintOrder]
                          FROM      dbo.CFW_Fld_Master CFM
                                    LEFT JOIN dbo.CFW_Loc_Master LOC ON CFM.Grp_id = LOC.Loc_id
                                    LEFT JOIN dbo.DycFormSectionDetail DFSD ON DFSD.Loc_id = LOC.Loc_id
                          WHERE     Grp_id = DFSD.Loc_Id
                                    AND numDomainID = @numDomainID
                                    AND DFSD.numFormID = @numFormID
                        ) X
                ORDER BY X.tintOrder,
                        X.intSectionID        		
        
        
                SELECT  ROW_NUMBER() OVER ( ORDER BY X.intSectionID, X.numFormFieldID ) AS SrNo,
                        X.*
                FROM    ( SELECT    ISNULL(( SELECT intImportTransactionID
                                             FROM   dbo.Import_File_Field_Mapping FFM
                                                    INNER JOIN Import_File_Master IFM ON IFM.intImportFileID = FFM.intImportFileID
                                                                                         AND IFM.intImportFileID = @numImportFileID
                                             WHERE  FFM.numFormFieldID = DCOL.numFieldID
                                           ), 0) AS [intImportFieldID],
                                    DCOL.numFieldID AS [numFormFieldID],
                                    '' AS [vcSectionNames],
                                    DCOL.vcFieldName AS [vcFormFieldName],
                                    DCOL.vcDbColumnName,
                                    DCOL.intSectionID AS [intSectionID],
                                    0 [IsCustomField],
                                    ISNULL(( SELECT intMapColumnNo
                                             FROM   dbo.Import_File_Field_Mapping FFM
                                             WHERE  1 = 1
                                                    AND FFM.numFormFieldID = DCOL.numFieldID
                                                    AND FFM.intImportFileID = @numImportFileID
                                           ), 0) intMapColumnNo,
                                    DCOL.vcPropertyName,
                                    CASE WHEN ISNULL(DCOL.bitRequired, 0) = 0
                                         THEN ''
                                         ELSE '*'
                                    END AS [vcReqString],
                                    DCOL.bitRequired AS [bitRequired],
                                    CAST(DCOL.numFieldID AS VARCHAR) + '~'
                                    + CAST(0 AS VARCHAR(10)) AS [keyFieldID],
                                    ISNULL(DCOL.tintColumn, 0) AS SortOrder,
                                    ISNULL(DCOL.tintRow,0) AS tintRow
                          FROM      dbo.View_DynamicColumns DCOL
                          WHERE     DCOL.numDomainID = @numDomainID
                                    AND DCOL.numFormID = @numFormID
                                    AND ISNULL(DCOL.bitImport, 0) = 1
                                    AND ISNULL(tintPageType, 0) = 4
                          UNION ALL
                          SELECT    -1,
                                    CFm.Fld_id,
                                    'CustomField',
                                    cfm.Fld_label,
                                    cfm.Fld_label,
                                    DFSD.intSectionId,
                                    1,
                                    ISNULL(( SELECT intMapColumnNo
                                             FROM   dbo.Import_File_Field_Mapping IIFM
                                                    LEFT JOIN dbo.DycFormField_Mapping DMAP ON IIFM.numFormFieldID = DMAP.numFieldID
                                             WHERE  IIFM.intImportFileID = @numImportFileID
                                                    AND IIFM.numFormFieldID = CFM.Fld_id
                                                    AND bitCustomField = 1
                                                    AND ISNULL(DMAP.bitImport, 0) = 1
                                           ), 0) intMapColumnNo,
                                    '' AS [vcPropertyName],
                                    '' AS [vcReqString],
                                    0 AS [bitRequired],
                                    CAST(CFm.Fld_id AS VARCHAR) + '~'
                                    + CAST(1 AS VARCHAR(10)) AS [keyFieldID],
                                    DFCD.intColumnNum AS [SortOrder],
                                    ISNULL(DFCD.intRowNum,0) AS tintRow
                          FROM      dbo.CFW_Fld_Master CFM
                                    LEFT JOIN dbo.CFW_Loc_Master LOC ON CFM.Grp_id = LOC.Loc_id
                                    LEFT JOIN dbo.DycFormSectionDetail DFSD ON DFSD.Loc_id = LOC.Loc_id
                                    JOIN DycFormConfigurationDetails DFCD ON DFCD.numFieldID = CFM.Fld_Id
                                                                             AND DFCD.numFormID = @numFormID
                                                                             AND DFCD.bitCustom = 1
                          WHERE     Grp_id = DFSD.Loc_Id
                                    AND DFCD.numDomainID = @numDomainID
                                    AND DFSD.numFormID = @numFormID
                                    AND ( DFCD.tintPageType = 4
                                          OR DFCD.tintPageType IS NULL
                                        )
                                    
--                          SELECT    -1,
--                                    numFieldId,
--                                    'CustomField',
--                                    vcFieldName,
--                                    vcFieldName,
--                                    DFSD.intSectionId,
--                                    1,
--                                    ISNULL(( SELECT intMapColumnNo
--                                             FROM   dbo.Import_File_Field_Mapping IIFM
--                                                    LEFT JOIN dbo.DycFormField_Mapping DMAP ON IIFM.numFormFieldID = DMAP.numFieldID
--                                             WHERE  IIFM.intImportFileID = @numImportFileID
--                                                    AND IIFM.numFormFieldID = numFieldId
--                                                    AND bitCustomField = 1
--                                                    AND ISNULL(DMAP.bitImport, 0) = 1
--                                           ), 0) intMapColumnNo,
--                                    '' AS [vcPropertyName],
--                                    '' AS [vcReqString],
--                                    0 AS [bitRequired],
--                                    CAST(numFieldId AS VARCHAR) + '~'
--                                    + CAST(1 AS VARCHAR(10)) AS [keyFieldID]
--                          FROM      View_DynamicCustomColumns DCOL
--                                    LEFT JOIN dbo.DycFormSectionDetail DFSD ON Grp_id = DFSD.Loc_Id
--                                                                               AND DFSD.numFormID = @numFormID
--                          WHERE     1 = 1
--                                    AND numDomainID = numDomainID
--                                    AND DFSD.numFormID = @numFormID
--                                    AND tintPageType = 4
                          
                        ) X
                ORDER BY X.SortOrder
                        ,X.intSectionID 
            END	
    END

/****** Object:  StoredProcedure [dbo].[USP_GetAccountPayableAging_Details]    Script Date: 03/06/2009 00:30:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- exec [dbo].USP_GetAccountPayableAging_Details 1,7281,'90-'
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetAccountPayableAging_Details')
DROP PROCEDURE USP_GetAccountPayableAging_Details
GO
CREATE PROCEDURE [dbo].[USP_GetAccountPayableAging_Details](
               @numDomainId   AS NUMERIC(9)  = 0,
               @numDivisionID NUMERIC(9),
               @vcFlag        VARCHAR(20))
AS
  BEGIN
	DECLARE @strSqlJournal VARCHAR(8000);
	DECLARE @strSqlOrder VARCHAR(8000);
	Declare @strSqlCommission varchar(8000);
	Declare @strSqlBill varchar(8000);
	Declare @strSqlCheck varchar(8000);
	
    DECLARE  @AuthoritativePurchaseBizDocId  AS INTEGER
    SELECT  @AuthoritativePurchaseBizDocId= isnull(numAuthoritativePurchase,0)
    FROM   AuthoritativeBizDocs
    WHERE  numDomainId =  @numDomainId
       ------------------------------------------      
    DECLARE @baseCurrency AS NUMERIC
	SELECT @baseCurrency=numCurrencyID FROM dbo.Domain WHERE numDomainId=@numDomainId
	/*when any Order is placed in Foreign currency (other than base currency)then show it in purple color */
	------------------------------------------      

     
    --Get Master data
	 
	 SET @strSqlBill = 'SELECT  
						  Case When isnull(BH.bitLandedCost,0)=1 then  isnull(BH.numOppId,0) else 0 end AS numOppId,
						   ''Bill'' vcPOppName,
						   -2 AS [tintOppType],-- flag for bills 
						   0 numOppBizDocsId,
						   CASE WHEN len(BH.vcReference)=0 THEN '''' ELSE BH.vcReference END AS [vcBizDocID],
						   BH.monAmountDue as TotalAmount,
						   ISNULL(BH.monAmtPaid, 0) as AmountPaid,
					       ISNULL(BH.monAmountDue, 0) - ISNULL(BH.monAmtPaid, 0) as BalanceDue,
						   [dbo].[FormatedDateFromDate](BH.dtDueDate,BH.numDomainID) AS DueDate,
						   0 bitBillingTerms,
						   0 intBillingDays,
						   BH.dtCreatedDate dtCreatedDate,
						   '''' varCurrSymbol,
						   1 AS fltExchangeRate,
						   BH.dtDueDate AS dtFromDate,
						   0 as numBizDocsPaymentDetId,
						   1 AS CurrencyCount,0 as numComissionID,'''' as vcRefOrderNo,BH.numBillID,0 AS numCheckHeaderID
					FROM    
							BillHeader BH 
					WHERE   
							BH.numDomainID = '+ CONVERT(VARCHAR(15),@numDomainId) +'
							AND BH.numDivisionId = '+ CONVERT(VARCHAR(15),@numDivisionID) +'
							AND ISNULL(BH.monAmountDue, 0) - ISNULL(BH.monAmtPaid, 0) > 0 '
			 
		SET @strSqlCheck=' UNION
              SELECT  
						   0 AS numOppId,
						   ''Check'' vcPOppName,
						   -3 AS [tintOppType],-- flag for Checks
						   0 numOppBizDocsId,
						   '''' AS [vcBizDocID],
						   ISNULL(CD.monAmount, 0) as TotalAmount,
						   ISNULL(CD.monAmount, 0) as AmountPaid,
					       0 as BalanceDue,
						   [dbo].[FormatedDateFromDate](CH.dtCheckDate,CH.numDomainID) AS DueDate,
						   0 bitBillingTerms,
						   0 intBillingDays,
						   CH.dtCreatedDate dtCreatedDate,
						   '''' varCurrSymbol,
						   1 AS fltExchangeRate,
						   CH.dtCheckDate AS dtFromDate,
						   0 as numBizDocsPaymentDetId,
						   1 AS CurrencyCount,0 as numComissionID,'''' as vcRefOrderNo,0 AS numBillID,CH.numCheckHeaderID
					FROM   CheckHeader CH
						JOIN  dbo.CheckDetails CD ON CH.numCheckHeaderID=CD.numCheckHeaderID
                        INNER JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = CD.numChartAcntId
					WHERE CH.numDomainID = '+ CONVERT(VARCHAR(15),@numDomainId) +' AND CH.tintReferenceType=1
						AND COA.vcAccountCode LIKE ''01020102%''
                        AND CD.numCustomerId = '+ CONVERT(VARCHAR(15),@numDivisionID)
			SET @strSqlOrder=' UNION 
				SELECT 
               OM.[numOppId],
               OM.[vcPOppName],
               OM.[tintOppType],
               OB.[numOppBizDocsId],
               OB.[vcBizDocID],
               isnull(OB.monDealAmount * OM.fltExchangeRate,
                      0) TotalAmount,--OM.[monPAmount]
               (OB.[monAmountPaid] * OM.fltExchangeRate) AmountPaid,
               isnull(OB.monDealAmount * OM.fltExchangeRate
                        - OB.[monAmountPaid] * OM.fltExchangeRate,0)  BalanceDue,
               CASE ISNULL(OM.bitBillingTerms,0) 
                 --WHEN 1 THEN [dbo].[FormatedDateFromDate](DATEADD(dd,convert(int,ISNULL(dbo.fn_GetListItemName(isnull(OM.intBillingDays,0)),0)),OB.dtFromDate),
                 WHEN 1 THEN [dbo].[FormatedDateFromDate](DATEADD(dd,convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(OM.intBillingDays,0)), 0)),OB.dtFromDate),
                                                          '+ CONVERT(VARCHAR(15),@numDomainId) +')
                 WHEN 0 THEN [dbo].[FormatedDateFromDate](ob.[dtFromDate],'+ CONVERT(VARCHAR(15),@numDomainId) +')
               END AS DueDate,
               OM.bitBillingTerms,
               OM.intBillingDays,
               OB.dtCreatedDate,
               ISNULL(C.[varCurrSymbol],'''') varCurrSymbol,
               OM.fltExchangeRate,
               OB.dtFromDate,
			   0 AS numBizDocsPaymentDetId,
			   CASE WHEN '+ CONVERT(VARCHAR(15),@baseCurrency) +' <> OM.numCurrencyID THEN 2 ELSE 1 END AS CurrencyCount,0 as numComissionID,OB.vcRefOrderNo,0 as numBillID,0 AS numCheckHeaderID
        FROM   [OpportunityMaster] OM
               INNER JOIN [DivisionMaster] DM
                 ON OM.[numDivisionId] = DM.[numDivisionID]
               LEFT OUTER JOIN OpportunityBizDocs OB
                 ON OB.numOppId = OM.numOppID
               LEFT OUTER JOIN [Currency] C
                 ON OM.[numCurrencyID] = C.[numCurrencyID]
        WHERE  OM.[tintOppType] = 2
               AND OM.[tintOppStatus] = 1
               AND om.[numDomainId] = '+ CONVERT(VARCHAR(15),@numDomainId) +'
               AND OB.[numBizDocId] = '+ CONVERT(VARCHAR(15),ISNULL(@AuthoritativePurchaseBizDocId,0)) +' 
               AND DM.[numDivisionID] = '+ CONVERT(VARCHAR(15),@numDivisionID) +'
               AND OB.bitAuthoritativeBizDocs=1
               AND ISNULL(OB.tintDeferred,0) <> 1
               AND  isnull(OB.monDealAmount * OM.fltExchangeRate,0) > 0 
			   AND isnull(OB.monDealAmount * OM.fltExchangeRate
                        - OB.[monAmountPaid] * OM.fltExchangeRate,0) > 0 '
		
		SET @strSqlCommission=' UNION 
		 SELECT  OM.[numOppId],
               OM.[vcPOppName],
               OM.[tintOppType],
               OB.[numOppBizDocsId],
               OB.[vcBizDocID],
               BDC.numComissionAmount,
               0 AmountPaid,
               BDC.numComissionAmount BalanceDue,
               CASE ISNULL(OM.bitBillingTerms,0) 
                 --WHEN 1 THEN [dbo].[FormatedDateFromDate](DATEADD(dd,convert(int,ISNULL(dbo.fn_GetListItemName(isnull(OM.intBillingDays,0)),0)),OB.dtFromDate),
                 WHEN 1 THEN [dbo].[FormatedDateFromDate](DATEADD(dd,convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(OM.intBillingDays,0)), 0)),OB.dtFromDate),
                                                          '+ CONVERT(VARCHAR(15),@numDomainId) +')
                 WHEN 0 THEN [dbo].[FormatedDateFromDate](ob.[dtFromDate],'+ CONVERT(VARCHAR(15),@numDomainId) +')
               END AS DueDate,
               OM.bitBillingTerms,
               OM.intBillingDays,
               OB.dtCreatedDate,
               ISNULL(C.[varCurrSymbol],'''') varCurrSymbol,
               OM.fltExchangeRate,
               OB.dtFromDate,
			   0 AS numBizDocsPaymentDetId,
			   CASE WHEN '+ CONVERT(VARCHAR(15),@baseCurrency) +' <> OM.numCurrencyID THEN 2 ELSE 1 END AS CurrencyCount,BDC.numComissionID,OB.vcRefOrderNo,0 as numBillID,0 AS numCheckHeaderID
        FROM    BizDocComission BDC JOIN  OpportunityBizDocs OB
                ON BDC.numOppBizDocId = OB.numOppBizDocsId
				join OpportunityMaster OM on OM.numOppId=OB.numOppId
				JOIN Domain D on D.numDomainID=BDC.numDomainID
				  LEFT OUTER JOIN [Currency] C
                 ON OM.[numCurrencyID] = C.[numCurrencyID]
				LEFT JOIN OpportunityBizDocsDetails OBDD ON BDC.numBizDocsPaymentDetId=isnull(OBDD.numBizDocsPaymentDetId,0)
        WHERE   OM.numDomainID = '+ CONVERT(VARCHAR(15),@numDomainId) +' and 
				 BDC.numDomainID = '+ CONVERT(VARCHAR(15),@numDomainId) +' 
				--and BDC.bitCommisionPaid=0
				AND OB.bitAuthoritativeBizDocs = 1
				AND isnull(OBDD.bitIntegratedToAcnt,0) = 0
                AND D.numDivisionId = ' + CONVERT(VARCHAR(15),@numDivisionID) 
             
							
                
    --Show Impact of AR journal entries in report as well 
    	
SET @strSqlJournal = '    	
		UNION 
		 SELECT 
				GJH.numJournal_Id [numOppId],
				''Journal'' [vcPOppName],
				-1 [tintOppType],
				0 [numOppBizDocsId],
				'''' [vcBizDocID],
				CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt ELSE -1*GJD.numCreditAmt END TotalAmount,
				0 AmountPaid,
				1 BalanceDue,
				[dbo].[FormatedDateFromDate](GJH.datEntry_Date,'+ CONVERT(VARCHAR(15),@numDomainId) +') AS DueDate,
				0 bitBillingTerms,
				0 intBillingDays,
				GJH.datCreatedDate,
				ISNULL(C.[varCurrSymbol], '''') [varCurrSymbol],
				1 fltExchangeRate,
				GJH.datEntry_Date AS dtFromDate,
				0 numBizDocsPaymentDetId,
				1 AS CurrencyCount,0 as numComissionID,'''' as vcRefOrderNo,0 as numBillID,0 AS numCheckHeaderID
		 FROM   dbo.General_Journal_Header GJH
				INNER JOIN dbo.General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId
															  AND GJH.numDomainId = GJD.numDomainId
				INNER JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = GJD.numChartAcntId
				LEFT OUTER JOIN [Currency] C ON GJD.[numCurrencyID] = C.[numCurrencyID]
		 WHERE  GJH.numDomainId = '+ CONVERT(VARCHAR(15),@numDomainId) +'
				and GJD.numCustomerID =' + CONVERT(VARCHAR(15),@numDivisionID) +'
				AND COA.vcAccountCode LIKE ''01020102%''
				AND ISNULL(numOppId,0)=0 AND ISNULL(numOppBizDocsId,0)=0 
				AND ISNULL(GJH.numBillID,0)=0 AND ISNULL(GJH.numBillPaymentID,0)=0 AND ISNULL(GJH.numCheckHeaderID,0)=0 AND ISNULL(GJH.numReturnID,0)=0
				AND GJH.numJournal_Id NOT IN (
				--Exclude Bill and Bill payment entries
				SELECT GH.numJournal_Id FROM dbo.General_Journal_Header GH INNER JOIN dbo.OpportunityBizDocsDetails OBD ON GH.numBizDocsPaymentDetId = OBD.numBizDocsPaymentDetId 
				WHERE GH.numDomainId='+ CONVERT(VARCHAR(15),@numDomainId) +' and OBD.tintPaymentType IN (2,4,6) and GH.numBizDocsPaymentDetId>0
				) AND ISNULL(GJH.numBillID,0)=0 AND ISNULL(GJH.numBillPaymentID,0)=0 AND ISNULL(GJH.numCheckHeaderID,0)=0
				'
    
    
    IF (@vcFlag = '0+30')
      BEGIN
			SET @strSqlBill =@strSqlBill +' AND BH.dtDueDate BETWEEN dbo.GetUTCDateWithoutTime() AND dateadd(DAY,31,dbo.GetUTCDateWithoutTime()) '
                                               
            SET @strSqlCheck =@strSqlCheck +' AND CH.dtCheckDate BETWEEN dbo.GetUTCDateWithoutTime() AND dateadd(DAY,31,dbo.GetUTCDateWithoutTime()) '
                                               
            SET @strSqlOrder =@strSqlOrder +' AND 
               dateadd(DAY,CASE 
                                 --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                 WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                 ELSE 0
                               END,dtFromDate) BETWEEN dbo.GetUTCDateWithoutTime() AND dateadd(DAY,31,dbo.GetUTCDateWithoutTime())
               AND dbo.GetUTCDateWithoutTime() <= dateadd(DAY,CASE 
                                                 --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                 WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                 ELSE 0
                                               END,dtFromDate) '
                                                                                                                     
			SET @strSqlCommission =@strSqlCommission +' AND 
               dateadd(DAY,CASE 
                                 --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                 WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                 ELSE 0
                               END,dtFromDate) BETWEEN dbo.GetUTCDateWithoutTime() AND dateadd(DAY,31,dbo.GetUTCDateWithoutTime())
               AND dbo.GetUTCDateWithoutTime() <= dateadd(DAY,CASE 
                                                 --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                 WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                 ELSE 0
                                               END,dtFromDate) '
                                               
          SET @strSqlJournal =@strSqlJournal +' AND GJH.datEntry_Date BETWEEN dbo.GetUTCDateWithoutTime() AND dateadd(DAY,31,dbo.GetUTCDateWithoutTime())'
      END
    ELSE
      IF (@vcFlag = '30+60')
        BEGIN
			SET @strSqlBill =@strSqlBill +' AND BH.dtDueDate BETWEEN dateadd(DAY,31,dbo.GetUTCDateWithoutTime()) AND dateadd(DAY,61,dbo.GetUTCDateWithoutTime())'
                                               
            SET @strSqlCheck =@strSqlCheck +' AND CH.dtCheckDate BETWEEN dateadd(DAY,31,dbo.GetUTCDateWithoutTime()) AND dateadd(DAY,61,dbo.GetUTCDateWithoutTime())'
            
            SET @strSqlOrder =@strSqlOrder +' AND  
                 dateadd(DAY,CASE 
                                   --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                   WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                   ELSE 0
                                 END,dtFromDate) BETWEEN dateadd(DAY,31,dbo.GetUTCDateWithoutTime()) AND dateadd(DAY,61,dbo.GetUTCDateWithoutTime())
                 AND dbo.GetUTCDateWithoutTime() <= dateadd(DAY,CASE 
                                                   --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                   WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                   ELSE 0
                                                 END,dtFromDate)'
                                                                          
			SET @strSqlCommission =@strSqlCommission +' AND  
                 dateadd(DAY,CASE 
                                   --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                   WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                   ELSE 0
                                 END,dtFromDate) BETWEEN dateadd(DAY,31,dbo.GetUTCDateWithoutTime()) AND dateadd(DAY,61,dbo.GetUTCDateWithoutTime())
                 AND dbo.GetUTCDateWithoutTime() <= dateadd(DAY,CASE 
                                                   --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                   WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                   ELSE 0
                                                 END,dtFromDate)'
                                                 
           SET @strSqlJournal =@strSqlJournal +' AND GJH.datEntry_Date BETWEEN dateadd(DAY,31,dbo.GetUTCDateWithoutTime()) AND dateadd(DAY,61,dbo.GetUTCDateWithoutTime())'
        END
      ELSE
        IF (@vcFlag = '60+90')
          BEGIN
			SET @strSqlBill =@strSqlBill +' AND BH.dtDueDate BETWEEN dateadd(DAY,61,dbo.GetUTCDateWithoutTime()) AND dateadd(DAY,91,dbo.GetUTCDateWithoutTime())'
                                               
            SET @strSqlCheck =@strSqlCheck +' AND CH.dtCheckDate BETWEEN dateadd(DAY,61,dbo.GetUTCDateWithoutTime()) AND dateadd(DAY,91,dbo.GetUTCDateWithoutTime())'
               
            SET @strSqlOrder =@strSqlOrder +' AND  
                   dateadd(DAY,CASE 
                                     --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                     WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                     ELSE 0
                                   END,dtFromDate) BETWEEN dateadd(DAY,61,dbo.GetUTCDateWithoutTime()) AND dateadd(DAY,91,dbo.GetUTCDateWithoutTime())
                   AND dbo.GetUTCDateWithoutTime() <= dateadd(DAY,CASE 
                                                     --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                     WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                     ELSE 0
                                                   END,dtFromDate) '
                                                                            
			SET @strSqlCommission =@strSqlCommission +' AND  
                   dateadd(DAY,CASE 
                                     --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                     WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                     ELSE 0
                                   END,dtFromDate) BETWEEN dateadd(DAY,61,dbo.GetUTCDateWithoutTime()) AND dateadd(DAY,91,dbo.GetUTCDateWithoutTime())
                   AND dbo.GetUTCDateWithoutTime() <= dateadd(DAY,CASE 
                                                     --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                     WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                     ELSE 0
                                                   END,dtFromDate) '
                                                   
			SET @strSqlJournal =@strSqlJournal +' AND GJH.datEntry_Date BETWEEN dateadd(DAY,61,dbo.GetUTCDateWithoutTime()) AND dateadd(DAY,91,dbo.GetUTCDateWithoutTime())'
          END
        ELSE
          IF (@vcFlag = '90+')
            BEGIN
            SET @strSqlBill =@strSqlBill +' AND BH.dtDueDate > dateadd(DAY,91,dbo.GetUTCDateWithoutTime())'
                                               
            SET @strSqlCheck =@strSqlCheck +' AND CH.dtCheckDate > dateadd(DAY,91,dbo.GetUTCDateWithoutTime())'

			SET @strSqlOrder =@strSqlOrder +' AND  
                     dateadd(DAY,CASE 
                                       --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                       WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                       ELSE 0
                                     END,dtFromDate) > dateadd(DAY,91,dbo.GetUTCDateWithoutTime())
                     AND dbo.GetUTCDateWithoutTime() <= dateadd(DAY,CASE 
                                                       --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                       WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                       ELSE 0
                                                     END,dtFromDate)'                                     
                                                     
            SET @strSqlCommission =@strSqlCommission +' AND  
                     dateadd(DAY,CASE 
                                       --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                       WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                       ELSE 0
                                     END,dtFromDate) > dateadd(DAY,91,dbo.GetUTCDateWithoutTime())
                     AND dbo.GetUTCDateWithoutTime() <= dateadd(DAY,CASE 
                                                       --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                       WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                       ELSE 0
                                                     END,dtFromDate)'
                                                     
			SET @strSqlJournal =@strSqlJournal +' AND GJH.datEntry_Date > dateadd(DAY,91,dbo.GetUTCDateWithoutTime())'
            END
          ELSE
            IF (@vcFlag = '0-30')
              BEGIN
              SET @strSqlBill =@strSqlBill +' AND datediff(DAY,BH.dtDueDate,dbo.GetUTCDateWithoutTime()) BETWEEN 0 AND 30'
                                               
            SET @strSqlCheck =@strSqlCheck +' AND datediff(DAY,CH.dtCheckDate ,dbo.GetUTCDateWithoutTime()) BETWEEN 0 AND 30'
                    
                    SET @strSqlOrder =@strSqlOrder +' AND  
                       dbo.GetUTCDateWithoutTime() > dateadd(DAY,CASE 
                                                        --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                        WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                        ELSE 0
                                                      END,dtFromDate)
                       AND datediff(DAY,dateadd(DAY,CASE 
                                                      --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                      WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                      ELSE 0
                                                    END,dtFromDate),dbo.GetUTCDateWithoutTime()) BETWEEN 0 AND 30'
                                                    
                                                                     
					SET @strSqlCommission =@strSqlCommission +' AND  
                       dbo.GetUTCDateWithoutTime() > dateadd(DAY,CASE 
                                                        --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                        WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                        ELSE 0
                                                      END,dtFromDate)
                       AND datediff(DAY,dateadd(DAY,CASE 
                                                      --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                      WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                      ELSE 0
                                                    END,dtFromDate),dbo.GetUTCDateWithoutTime()) BETWEEN 0 AND 30'
                                                    
				SET @strSqlJournal =@strSqlJournal +' AND datediff(DAY,GJH.datEntry_Date,dbo.GetUTCDateWithoutTime()) BETWEEN 0 AND 30 '
              END
            ELSE
              IF (@vcFlag = '30-60')
                BEGIN
                SET @strSqlBill =@strSqlBill +' AND datediff(DAY,BH.dtDueDate,dbo.GetUTCDateWithoutTime()) BETWEEN 31 AND 60'
                                               
				SET @strSqlCheck =@strSqlCheck +' AND datediff(DAY,CH.dtCheckDate,dbo.GetUTCDateWithoutTime()) BETWEEN 31 AND 60'
                
                SET @strSqlOrder =@strSqlOrder +' AND  
                         dbo.GetUTCDateWithoutTime() > dateadd(DAY,CASE 
                                                          --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                          WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                          ELSE 0
                                                        END,dtFromDate)
                         AND datediff(DAY,dateadd(DAY,CASE 
                                                        --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                        WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                        ELSE 0
                                                      END,dtFromDate),dbo.GetUTCDateWithoutTime()) BETWEEN 31 AND 60'
                                                                           
						SET @strSqlCommission =@strSqlCommission +' AND  
                         dbo.GetUTCDateWithoutTime() > dateadd(DAY,CASE 
                                                          --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                          WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                          ELSE 0
                                                        END,dtFromDate)
                         AND datediff(DAY,dateadd(DAY,CASE 
                                                        --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                        WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                        ELSE 0
                                                      END,dtFromDate),dbo.GetUTCDateWithoutTime()) BETWEEN 31 AND 60'
                                                      
					SET @strSqlJournal =@strSqlJournal +' AND datediff(DAY,GJH.datEntry_Date,dbo.GetUTCDateWithoutTime()) BETWEEN 31 AND 60 '
                END
              ELSE
                IF (@vcFlag = '60-90')
                  BEGIN
                  SET @strSqlBill =@strSqlBill +' AND datediff(DAY,BH.dtDueDate,dbo.GetUTCDateWithoutTime()) BETWEEN 61 AND 90 '
                                               
	              SET @strSqlCheck =@strSqlCheck +' AND datediff(DAY,CH.dtCheckDate,dbo.GetUTCDateWithoutTime()) BETWEEN 61 AND 90 '
                        
                        SET @strSqlOrder =@strSqlOrder +' AND  
                           dbo.GetUTCDateWithoutTime() > dateadd(DAY,CASE 
                                                            --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                            WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                            ELSE 0
                                                          END,dtFromDate)
                           AND datediff(DAY,dateadd(DAY,CASE 
                                                          --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                          WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                          ELSE 0
                                                        END,dtFromDate),dbo.GetUTCDateWithoutTime()) BETWEEN 61 AND 90'
                                                                     
						SET @strSqlCommission =@strSqlCommission +' AND  
                           dbo.GetUTCDateWithoutTime() > dateadd(DAY,CASE 
                                                            --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                            WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                            ELSE 0
                                                          END,dtFromDate)
                           AND datediff(DAY,dateadd(DAY,CASE 
                                                          --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                          WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                          ELSE 0
                                                        END,dtFromDate),dbo.GetUTCDateWithoutTime()) BETWEEN 61 AND 90'
                                                        
					SET @strSqlJournal =@strSqlJournal +' AND datediff(DAY,GJH.datEntry_Date,dbo.GetUTCDateWithoutTime()) BETWEEN 61 AND 90 '
                  END
                ELSE
                  IF (@vcFlag = '90-')
                    BEGIN
                    SET @strSqlBill =@strSqlBill +' AND datediff(DAY,BH.dtDueDate,dbo.GetUTCDateWithoutTime()) > 90 '
                                               
            SET @strSqlCheck =@strSqlCheck +' AND datediff(DAY,CH.dtCheckDate,dbo.GetUTCDateWithoutTime()) > 90 '
                             
                         SET @strSqlOrder =@strSqlOrder +' AND  
                             dbo.GetUTCDateWithoutTime() > dateadd(DAY,CASE 
                                                              --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                              WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                              ELSE 0
                                                            END,dtFromDate)
                             AND datediff(DAY,dateadd(DAY,CASE 
                                                            --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                            WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                            ELSE 0
                                                          END,dtFromDate),dbo.GetUTCDateWithoutTime()) > 90'
                                                                  
						SET @strSqlCommission =@strSqlCommission +' AND  
                             dbo.GetUTCDateWithoutTime() > dateadd(DAY,CASE 
                                                              --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                              WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                              ELSE 0
                                                            END,dtFromDate)
                             AND datediff(DAY,dateadd(DAY,CASE 
                                                            --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                            WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                            ELSE 0
                                                          END,dtFromDate),dbo.GetUTCDateWithoutTime()) > 90'
                                                          
						SET @strSqlJournal =@strSqlJournal +' AND datediff(DAY,GJH.datEntry_Date,dbo.GetUTCDateWithoutTime()) > 90 '
                    END
                  ELSE
                    BEGIN
						SET @strSqlBill =@strSqlBill +''
						SET @strSqlCheck =@strSqlCheck +''
						SET @strSqlOrder =@strSqlOrder +''
						SET @strSqlCommission =@strSqlCommission +''
						SET @strSqlJournal =@strSqlJournal +''
                    END
                    
                    
	PRINT (@strSqlBill)
	PRINT (@strSqlCheck)
	PRINT (@strSqlOrder)
	PRINT (@strSqlCommission)
	PRINT (@strSqlJournal)
	
	EXEC(@strSqlBill + @strSqlCheck + @strSqlOrder + @strSqlCommission + @strSqlJournal)
  END
/****** Object:  StoredProcedure [dbo].[Usp_GetAddressDetailsForImport]    Script Date: 06/19/2014 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'Usp_GetAddressDetailsForImport' )
    DROP PROCEDURE Usp_GetAddressDetailsForImport
GO
CREATE PROCEDURE [dbo].[Usp_GetAddressDetailsForImport]
    (
      @numDomainID AS NUMERIC(9) ,
      @tintUpdateMode AS TINYINT  -- Pass 1 for Adding / updating Contact's address
								  -- Pass 2 for Adding / updating Organization's Address
    )
AS
    BEGIN    
        IF @tintUpdateMode = 2
            BEGIN
                SELECT DISTINCT
                        CI.[vcCompanyName] ,
                        'Organization' [AddressOf] ,
                        CASE AD.[tintAddressType]
                          WHEN 1 THEN 'BILL TO'
                          ELSE 'SHIP TO'
                        END AS [AddressType] ,
                        CASE AD.[bitIsPrimary]
                          WHEN 1 THEN 'YES'
                          ELSE 'NO'
                        END AS [Is Primary] ,
                        AD.[vcAddressName] AS [Address Name] ,
                        AD.[vcStreet] AS [Street] ,
                        AD.[vcCity] AS [City] ,
                        ( SELECT    [S].[vcState]
                          FROM      [dbo].[State] AS S
                          WHERE     S.[numStateID] = AD.[numState]
                        ) AS [State] ,
                        ( SELECT    [LD].[vcData]
                          FROM      [dbo].[ListDetails] AS LD
                          WHERE     [LD].[numListID] = 40
                                    AND [LD].[numListItemID] = AD.[numCountry]
                        ) AS [Country] ,
                        AD.[vcPostalCode] AS [Postal Code] ,
                        AD.[numAddressID] AS [AddressID] ,
                        DM.[numDivisionID] AS [RecordID]
                FROM    [dbo].[AddressDetails] AS AD
                        LEFT JOIN [dbo].[DivisionMaster] AS DM ON AD.numDomainID = DM.numDomainID
                                                              AND AD.[numRecordID] = DM.[numDivisionID]
                                                              AND Ad.[tintAddressOf] = 2
                        JOIN [dbo].[CompanyInfo] AS CI ON CI.[numCompanyId] = DM.[numCompanyID]
                WHERE   [AD].[numDomainID] = @numDomainID
                ORDER BY CI.[vcCompanyName] ,
                        AD.[numAddressID]

            END
        ELSE
            IF @tintUpdateMode = 1
                BEGIN
                    SELECT  ACI.[vcFirstName] AS [First Name],
                            ACI.[vcLastName] AS [Last Name],
                            'Contact' [AddressOf] ,
                            CASE AD.[tintAddressType]
                              WHEN 1 THEN 'BILL TO'
                              ELSE 'SHIP TO'
                            END AS [AddressType] ,
                            CASE AD.[bitIsPrimary]
                              WHEN 1 THEN 'YES'
                              ELSE 'NO'
                            END AS [IsPrimary] ,
                            AD.[vcAddressName] AS [Address Name] ,
                            AD.[vcStreet] AS [Street] ,
                            AD.[vcCity] AS [City] ,
                            ( SELECT    [S].[vcState]
                              FROM      [dbo].[State] AS S
                              WHERE     S.[numStateID] = AD.[numState]
                            ) AS [State] ,
                            ( SELECT    [LD].[vcData]
                              FROM      [dbo].[ListDetails] AS LD
                              WHERE     [LD].[numListID] = 40
                                        AND [LD].[numListItemID] = AD.[numCountry]
                            ) AS [Country] ,
                            AD.[vcPostalCode] AS [Postal Code] ,
                            AD.[numAddressID] AS [AddressID] ,
                            [ACI].[numContactId] AS [RecordID]
                    FROM    [dbo].[AddressDetails] AS AD
                            JOIN [dbo].[AdditionalContactsInformation] AS ACI ON [AD].[numRecordID] = ACI.[numContactId]
                                                              AND ACI.[numDomainID] = [AD].[numDomainID]
                                                              AND [AD].[tintAddressOf] = 1
                    WHERE   [AD].[numDomainID] = @numDomainID
                            AND ( ISNULL(ACI.[vcFirstName], '') <> ''
                                  OR ISNULL(ACI.[vcLastName], '') <> ''
                                )
                    ORDER BY ACI.[vcFirstName] ,
                            ACI.[vcLastName] ,
                            AD.[numAddressID]

                END
    END
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetBillPaymentDetails' ) 
    DROP PROCEDURE USP_GetBillPaymentDetails
GO
-- exec USP_GetBillPaymentDetails 0,1,0,0
CREATE PROCEDURE [dbo].[USP_GetBillPaymentDetails]
    @numBillPaymentID NUMERIC(18, 0) ,
    @numDomainID NUMERIC ,
    @ClientTimeZoneOffset INT,
    @numDivisionID NUMERIC=0,
    @numCurrencyID NUMERIC=0,
    @CurrentPage INT,
    @PageSize INT,
    @TotRecs INT OUTPUT,
    @numAccountClass NUMERIC(18,0)=0
AS 
    SET NOCOUNT ON
    SET TRANSACTION ISOLATION LEVEL READ COMMITTED
		DECLARE @numBaseCurrencyID NUMERIC
	    DECLARE @BaseCurrencySymbol nVARCHAR(3)
	    
        SELECT @numBaseCurrencyID = ISNULL(numCurrencyID,0) FROM dbo.Domain WHERE numDomainId=@numDomainId
        SELECT @BaseCurrencySymbol = ISNULL(varCurrSymbol,'') FROM dbo.Currency WHERE numCurrencyID = @numBaseCurrencyID
    
		DECLARE @firstRec AS INTEGER
		DECLARE @lastRec AS INTEGER
		
		SET @firstRec = ( @CurrentPage - 1 ) * @PageSize
		SET @lastRec = ( @CurrentPage * @PageSize + 1 )
		
				  
		IF @numBillPaymentID>0
		BEGIN
			SELECT @numDivisionID=numDivisionID FROM [dbo].[BillPaymentHeader] BPH WHERE BPH.numBillPaymentID=@numBillPaymentID
		END
		
	 SELECT BPH.numBillPaymentID,BPH.dtPaymentDate,BPH.numAccountID,BPH.numDivisionID,BPH.numReturnHeaderID,BPH.numPaymentMethod
	 ,ISNULL(BPH.monPaymentAmount,0) AS monPaymentAmount,ISNULL(BPH.monAppliedAmount,0) AS monAppliedAmount,
	 ISNULL(CH.numCheckHeaderID, 0) numCheckHeaderID,
	 ISNULL(CH.numCheckNo,0) AS numCheckNo,ISNULL(BPH.numReturnHeaderID,0) AS numReturnHeaderID,
	 BPH.numCurrencyID,ISNULL(NULLIF(BPH.fltExchangeRate,0),1) fltExchangeRateBillPayment
	,ISNULL(C.varCurrSymbol,'') varCurrSymbol
	,@BaseCurrencySymbol BaseCurrencySymbol,ISNULL(monRefundAmount,0) AS monRefundAmount,ISNULL(BPH.numAccountClass,0) AS numAccountClass
	  FROM  [dbo].[BillPaymentHeader] BPH
			LEFT JOIN dbo.CheckHeader CH ON CH.numReferenceID = BPH.numBillPaymentID AND CH.tintReferenceType = 8
			LEFT JOIN dbo.Currency C ON C.numCurrencyID = BPH.numCurrencyID
			WHERE BPH.numBillPaymentID=@numBillPaymentID
		
		--For Edit Bill Payment Get Details
		SELECT Row_number() OVER ( ORDER BY DueDate DESC ) AS row,* INTO #tempBillPayment FROM 
		(
			SELECT  Case When isnull(BH.bitLandedCost,0)=1 then  isnull(BH.numOppId,0) else OBD.numOppId end AS numoppid,
					  BPH.numDivisionID,BPD.[numOppBizDocsID],BPD.[numBillID],
					  CASE WHEN(BPD.numBillID>0) THEN  Case When isnull(BH.bitLandedCost,0)=1 THEN 'Laned Cost-' ELSE 'Bill-' END + CONVERT(VARCHAR(10),BH.numBillID)
					  else OBD.vcbizdocid END AS [Name],
					  CASE BPD.[tintBillType] WHEN 1 THEN OBD.monDealAmount WHEN 2 THEN BH.monAmountDue END monOriginalAmount,
					  CASE BPD.[tintBillType] WHEN 1 THEN ( OBD.monDealAmount - OBD.monAmountPaid + BPD.[monAmount]) WHEN 2 THEN BH.monAmountDue - BH.monAmtPaid + BPD.[monAmount] END monAmountDue,
					  CASE BPD.[tintBillType] WHEN 1 THEN '' WHEN 2 THEN BH.vcReference END Reference,
					  CASE BPD.[tintBillType] WHEN 1 THEN dbo.fn_GetComapnyName(OM.numDivisionId) WHEN 2 THEN dbo.fn_GetComapnyName(BH.numDivisionID) END vcCompanyName,
					  BPD.[tintBillType],
					  CASE BPD.[tintBillType]
						  WHEN 1
						  THEN CASE ISNULL(OM.bitBillingTerms, 0)
								 WHEN 1
								 --THEN [dbo].[FormatedDateFromDate](DATEADD(dd,CONVERT(INT, ISNULL(dbo.fn_GetListItemName(ISNULL(OM.intBillingDays,0)), 0)),OBD.dtFromDate),@numDomainId)
								 THEN [dbo].[FormatedDateFromDate](DATEADD(dd,CONVERT(INT, ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(OM.intBillingDays,0)), 0)),OBD.dtFromDate),@numDomainId)
								 WHEN 0
								 THEN [dbo].[FormatedDateFromDate](OBD.[dtFromDate],
																  @numDomainId)
							   END
						  WHEN 2
						  THEN [dbo].[FormatedDateFromDate](BH.dtDueDate,@numDomainID) END AS DueDate,
						  BPD.[monAmount] AS monAmountPaid,1 bitIsPaid
						  ,ISNULL(NULLIF(OM.fltExchangeRate,0),1) fltExchangeRateOfOrder
				FROM    [dbo].[BillPaymentHeader] BPH
						INNER JOIN dbo.BillPaymentDetails BPD ON BPH.numBillPaymentID = BPD.numBillPaymentID
						LEFT JOIN dbo.OpportunityBizDocs OBD ON BPD.numOppBizDocsID = OBD.numOppBizDocsId
						LEFT JOIN dbo.OpportunityMaster OM ON OBD.numOppId = OM.numOppId
						LEFT JOIN dbo.BillHeader BH ON BH.numBillID = BPD.numBillID
						LEFT JOIN dbo.CheckHeader CH ON CH.numReferenceID = BPH.numBillPaymentID
														AND CH.tintReferenceType = 8
						                                                 
				WHERE   BPH.[numBillPaymentID] = @numBillPaymentID
				AND BPH.numDomainID = @numDomainID
				AND (OM.numCurrencyID = @numCurrencyID OR @numCurrencyID = 0 OR ISNULL(BPD.numOppBizDocsID,0) =0)
	            AND (ISNULL(BPH.numAccountClass,0)=@numAccountClass OR @numAccountClass=0)
	            
				UNION
		--PO Bills For Add Bill Payment
				SELECT  opp.numoppid AS numoppid ,
						opp.numdivisionid AS numDivisionID ,
						OBD.numOppBizDocsId AS numOppBizDocsId ,
						0 AS numBillID ,
						OBD.vcbizdocid AS [name] ,
						OBD.monDealAmount AS monOriginalAmount ,
						( OBD.monDealAmount - OBD.monAmountPaid ) AS monAmountDue ,
	--                    '' AS memo ,
						OBD.vcRefOrderNo AS reference ,
	--                    c.numcompanyid AS numcompanyid ,
						c.vccompanyname AS vccompanyname ,
						1 AS [tintBillType] , -- 1 =PO Bill,2=Regular bill , 3= Bill against liability
						CASE ISNULL(Opp.bitBillingTerms, 0)
						  WHEN 1
						  --THEN [dbo].[FormatedDateFromDate](DATEADD(dd,CONVERT(INT, ISNULL(dbo.fn_GetListItemName(ISNULL(Opp.intBillingDays,0)), 0)),OBD.dtFromDate),@numDomainId)
						  THEN [dbo].[FormatedDateFromDate](DATEADD(dd,CONVERT(INT, ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0)),OBD.dtFromDate),@numDomainId)
						  WHEN 0
						  THEN [dbo].[FormatedDateFromDate](OBD.[dtFromDate],
															@numDomainId)
						END AS DueDate ,
						'' AS monAmountPaid,
						0 bitIsPaid
						,ISNULL(NULLIF(Opp.fltExchangeRate,0),1) fltExchangeRateOfOrder
				FROM    OpportunityBizDocs OBD
						INNER JOIN OpportunityMaster Opp ON OBD.numOppId = Opp.numOppId
						INNER JOIN AdditionalContactsInformation ADC ON Opp.numContactId = ADC.numContactId
						INNER JOIN DivisionMaster Div ON Opp.numDivisionId = Div.numDivisionID
														 AND ADC.numDivisionId = Div.numDivisionID
						INNER JOIN CompanyInfo C ON Div.numCompanyID = C.numCompanyId
				WHERE   Opp.numDomainId = @numDomainId
						AND OBD.bitAuthoritativeBizDocs = 1
						AND Opp.tintOppType = 2
						AND ( OBD.monDealAmount - OBD.monAmountPaid ) > 0
						AND (Opp.numDivisionId = @numDivisionID OR ISNULL(@numDivisionID,0)=0)
						AND OBD.numOppBizDocsID NOT IN (SELECT numOppBizDocsID FROM BillPaymentDetails WHERE numBillPaymentID = @numBillPaymentID)
						AND (Opp.numCurrencyID = @numCurrencyID OR @numCurrencyID = 0)
						AND (ISNULL(Opp.numAccountClass,0)=@numAccountClass OR @numAccountClass=0)
				 UNION       
				 --Regular Bills For Add Bill Payment
				SELECT  Case When isnull(BH.bitLandedCost,0)=1 then  isnull(BH.numOppId,0) else 0 end AS numoppid ,
						numDivisionID AS numDivisionID ,
						0 AS numOppBizDocsId ,
						BH.numBillID ,
						Case When isnull(BH.bitLandedCost,0)=1 THEN 'Laned Cost-' ELSE 'Bill-' END + CONVERT(VARCHAR(10),BH.numBillID) AS [name] ,
						BH.monAmountDue AS monOriginalAmount ,
						(BH.monAmountDue - BH.monAmtPaid) AS monAmountDue ,
	--                    '' AS memo ,
						BH.vcReference AS reference ,
	--                    c.numcompanyid AS numcompanyid ,
						dbo.fn_GetComapnyName(BH.numDivisionID) AS vccompanyname ,
						2 AS [tintBillType] , -- 1 =PO Bill,2=Regular bill , 3= Bill against liability
						[dbo].[FormatedDateFromDate](BH.dtDueDate,@numDomainId) AS DueDate ,
						'' AS monAmountPaid,
						0 bitIsPaid
						,1 fltExchangeRateOfOrder
				FROM    dbo.BillHeader BH
				WHERE   BH.numDomainId = @numDomainId
						AND BH.bitIsPaid = 0
						AND (BH.numDivisionId = @numDivisionID OR ISNULL(@numDivisionID,0)=0) 
						AND BH.numBillID NOT IN (SELECT numBillID FROM BillPaymentDetails WHERE numBillPaymentID = @numBillPaymentID)      
						AND  (@numCurrencyID = @numBaseCurrencyID OR @numCurrencyID = 0)
						AND (ISNULL(BH.numAccountClass,0)=@numAccountClass OR @numAccountClass=0)
		) [BillPayment]

		SELECT  @TotRecs = COUNT(*)  FROM #tempBillPayment	
			                                                 	
		SELECT * FROM #tempBillPayment	
		WHERE  row > @firstRec 
		   AND row < @lastRec ;				

        DROP TABLE #tempBillPayment	    


GO


/****** Object:  StoredProcedure [dbo].[USP_GetColumnConfiguration1]    Script Date: 07/26/2008 16:16:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_getcolumnconfiguration1' )
    DROP PROCEDURE usp_getcolumnconfiguration1
GO
CREATE PROCEDURE [dbo].[USP_GetColumnConfiguration1]
    @numDomainID NUMERIC(9) ,
    @numUserCntId NUMERIC(9) ,
    @FormId TINYINT ,
    @numtype NUMERIC(9) ,
    @numViewID NUMERIC(9) = 0
AS
    DECLARE @PageId AS TINYINT         
    IF @FormId = 10
        OR @FormId = 44
        SET @PageId = 4          
    ELSE
        IF @FormId = 11
            OR @FormId = 34
            OR @FormId = 35
            OR @FormId = 36
            OR @FormId = 96
            OR @FormId = 97
            SET @PageId = 1          
        ELSE
            IF @FormId = 12
                SET @PageId = 3          
            ELSE
                IF @FormId = 13
                    SET @PageId = 11       
                ELSE
                    IF ( @FormId = 14
                         AND ( @numtype = 1
                               OR @numtype = 3
                             )
                       )
                        OR @FormId = 33
                        OR @FormId = 38
                        OR @FormId = 39
                        SET @PageId = 2          
                    ELSE
                        IF @FormId = 14
                            AND ( @numtype = 2
                                  OR @numtype = 4
                                )
                            OR @FormId = 40
                            OR @FormId = 41
                            SET @PageId = 6  
                        ELSE
                            IF @FormId = 21
                                OR @FormId = 26
                                OR @FormId = 32
                                SET @PageId = 5   
                            ELSE
                                IF @FormId = 22
                                    OR @FormId = 30
                                    SET @PageId = 5    
                                ELSE
                                    IF @FormId = 76
                                        SET @PageId = 10 
                                    ELSE
                                        IF @FormId = 84
                                            OR @FormId = 85
                                            SET @PageId = 5
                     
    IF @PageId = 1
        OR @PageId = 4
        BEGIN          
            SELECT  CONVERT(VARCHAR(9), numFieldID) + '~0' AS numFieldID ,
                    ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                    0 AS Custom
            FROM    View_DynamicDefaultColumns
            WHERE   numFormID = @FormId
                    AND ISNULL(bitSettingField, 0) = 1
                    AND ISNULL(bitDeleted, 0) = 0
                    AND numDomainID = @numDomainID
            UNION
            SELECT  CONVERT(VARCHAR(9), fld_id) + '~1' AS numFieldID ,
                    fld_label AS vcFieldName ,
                    1 AS Custom
            FROM    CFW_Fld_Master
                    LEFT JOIN CFW_Fld_Dtl ON Fld_id = numFieldId
                                             AND numRelation = CASE
                                                              WHEN @numtype IN (
                                                              1, 2, 3 )
                                                              THEN numRelation
                                                              ELSE @numtype
                                                              END
                    LEFT JOIN CFw_Grp_Master ON subgrp = CFw_Grp_Master.Grp_id
            WHERE   CFW_Fld_Master.grp_id = @PageId
                    AND CFW_Fld_Master.numDomainID = @numDomainID
                    AND Fld_type <> 'Link'
            ORDER BY Custom ,
                    vcFieldName                                       
        END  
    ELSE
        IF @PageId = 10
            BEGIN          
                SELECT  CONVERT(VARCHAR(9), numFieldID) + '~0' AS numFieldID ,
                        ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                        0 AS Custom
                FROM    View_DynamicDefaultColumns
                WHERE   numFormID = @FormId
                        AND ISNULL(bitSettingField, 0) = 1
                        AND ISNULL(bitDeleted, 0) = 0
                        AND numDomainID = @numDomainID
                UNION
                SELECT  CONVERT(VARCHAR(9), fld_id) + '~1' AS numFieldID ,
                        fld_label AS vcFieldName ,
                        1 AS Custom
                FROM    CFW_Fld_Master
                        LEFT JOIN CFW_Fld_Dtl ON Fld_id = numFieldId
                                                 AND numRelation = CASE
                                                              WHEN @numtype IN (
                                                              1, 2, 3 )
                                                              THEN numRelation
                                                              ELSE @numtype
                                                              END
                        LEFT JOIN CFw_Grp_Master ON subgrp = CFw_Grp_Master.Grp_id
                WHERE   CFW_Fld_Master.grp_id = 5
                        AND CFW_Fld_Master.numDomainID = @numDomainID
                        AND Fld_Type = 'SelectBox'
                ORDER BY Custom ,
                        vcFieldName       
	                                     
            END 

        ELSE
            IF @PageId = 5
                AND ( @FormId = 84
                      OR @FormId = 85
                    )
                BEGIN          
                    SELECT  CONVERT(VARCHAR(9), numFieldID) + '~0' AS numFieldID ,
                            ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                            0 AS Custom ,
                            vcDbColumnName
                    FROM    View_DynamicDefaultColumns
                    WHERE   numFormID = @FormId
                            AND ISNULL(bitSettingField, 0) = 1
                            AND ISNULL(bitDeleted, 0) = 0
                            AND numDomainID = @numDomainID
                    UNION
                    SELECT  CONVERT(VARCHAR(9), fld_id) + '~1' AS numFieldID ,
                            fld_label AS vcFieldName ,
                            1 AS Custom ,
                            CONVERT(VARCHAR(10), fld_id) AS vcDbColumnName
                    FROM    CFW_Fld_Master
                            LEFT JOIN CFw_Grp_Master ON subgrp = CFw_Grp_Master.Grp_id
                    WHERE   CFW_Fld_Master.grp_id = @PageId
                            AND CFW_Fld_Master.numDomainID = @numDomainID
                            AND Fld_type = 'TextBox'
                    ORDER BY Custom ,
                            vcFieldName             
                END  
             
            ELSE
                BEGIN          
      
                    SELECT  CONVERT(VARCHAR(9), numFieldID) + '~0' AS numFieldID ,
                            ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                            0 AS Custom ,
                            vcDbColumnName
                    FROM    View_DynamicDefaultColumns
                    WHERE   numFormID = @FormId
                            AND ISNULL(bitSettingField, 0) = 1
                            AND ISNULL(bitDeleted, 0) = 0
                            AND numDomainID = @numDomainID
                    UNION
                    SELECT  CONVERT(VARCHAR(9), fld_id) + '~1' AS numFieldID ,
                            fld_label AS vcFieldName ,
                            1 AS Custom ,
                            CONVERT(VARCHAR(10), fld_id) AS vcDbColumnName
                    FROM    CFW_Fld_Master
                            LEFT JOIN CFw_Grp_Master ON subgrp = CFw_Grp_Master.Grp_id
                    WHERE   CFW_Fld_Master.grp_id = @PageId
                            AND CFW_Fld_Master.numDomainID = @numDomainID
                            AND Fld_type <> 'Link'
                    ORDER BY Custom ,
                            vcFieldName             
                END                      
                      
                   
    IF @PageId = 1
        OR @PageId = 4
        BEGIN
 
            IF ( @FormId = 96
                 OR @FormId = 97
               )
                SELECT  CONVERT(VARCHAR(9), numFieldID) + '~0' numFieldID ,
                        ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                        vcDbColumnName ,
                        0 AS Custom ,
                        tintRow AS tintOrder
                FROM    View_DynamicColumns
                WHERE   numFormId = @FormId
                        AND numUserCntID = @numUserCntID
                        AND numDomainID = @numDomainID
                        AND ISNULL(bitCustom, 0) = 0
                        AND tintPageType = 1
                        AND ISNULL(bitSettingField, 0) = 1
                        AND numRelCntType = @numtype
                        AND ISNULL(bitDeleted, 0) = 0
                UNION
                SELECT  CONVERT(VARCHAR(9), numFieldID) + '~1' AS numFieldID ,
                        vcFieldName ,
                        vcFieldName AS vcDbColumnName ,
                        1 AS Custom ,
                        tintRow AS tintOrder
                FROM    View_DynamicCustomColumns
                WHERE   numFormID = @FormId
                        AND numUserCntID = @numUserCntID
                        AND numDomainID = @numDomainID
                        AND grp_id = @PageId
                        AND numDomainID = @numDomainID
                        AND vcAssociatedControlType <> 'Link'
                        AND ISNULL(bitCustom, 0) = 1
                        AND tintPageType = 1
                        AND numRelCntType = @numtype
                ORDER BY tintOrder                
            ELSE
                SELECT  CONVERT(VARCHAR(9), numFieldID) + '~0' numFieldID ,
                        ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                        vcDbColumnName ,
                        0 AS Custom ,
                        tintRow AS tintOrder
                FROM    View_DynamicColumns
                WHERE   numFormId = @FormId
                        AND numUserCntID = @numUserCntID
                        AND numDomainID = @numDomainID
                        AND ISNULL(bitCustom, 0) = 0
                        AND tintPageType = 1
                        AND ISNULL(bitSettingField, 0) = 1
                        AND numRelCntType = @numtype
                        AND ISNULL(bitDeleted, 0) = 0
                UNION
                SELECT  CONVERT(VARCHAR(9), numFieldID) + '~1' AS numFieldID ,
                        vcFieldName ,
                        '' AS vcDbColumnName ,
                        1 AS Custom ,
                        tintRow AS tintOrder
                FROM    View_DynamicCustomColumns
                WHERE   numFormID = @FormId
                        AND numUserCntID = @numUserCntID
                        AND numDomainID = @numDomainID
                        AND grp_id = @PageId
                        AND numDomainID = @numDomainID
                        AND vcAssociatedControlType <> 'Link'
                        AND ISNULL(bitCustom, 0) = 1
                        AND tintPageType = 1
                        AND numRelCntType = @numtype
                ORDER BY tintOrder          
        END     
    ELSE
        IF @PageId = 10
            BEGIN
	
                SELECT  CONVERT(VARCHAR(9), numFieldID) + '~0' numFieldID ,
                        ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                        vcDbColumnName ,
                        0 AS Custom ,
                        tintRow AS tintOrder ,
                        numListID ,
                        numFieldID AS [numFieldID1]
                FROM    View_DynamicColumns
                WHERE   numFormId = @FormId
                        AND numUserCntID = @numUserCntID
                        AND numDomainID = @numDomainID
                        AND ISNULL(bitCustom, 0) = 0
                        AND tintPageType = 1
                        AND ISNULL(bitSettingField, 0) = 1
                        AND numRelCntType = @numtype
                        AND ISNULL(bitDeleted, 0) = 0
                UNION
                SELECT  CONVERT(VARCHAR(9), numFieldID) + '~1' AS numFieldID ,
                        vcFieldName ,
                        '' AS vcDbColumnName ,
                        1 AS Custom ,
                        tintRow AS tintOrder ,
                        numListID ,
                        numFieldID AS [numFieldID1]
                FROM    View_DynamicCustomColumns
                WHERE   numFormID = @FormId
                        AND numUserCntID = @numUserCntID
                        AND numDomainID = @numDomainID   
   --and grp_id=@PageId    
                        AND numDomainID = @numDomainID
                        AND vcAssociatedControlType = 'SelectBox'
                        AND ISNULL(bitCustom, 0) = 1
                        AND tintPageType = 1
                        AND numRelCntType = @numtype
                ORDER BY tintOrder    
--   
--    SELECT DISTINCT numFieldID as numFieldID,'Archived Items' AS vcFieldName,bitCustom as Custom 
--	   FROM DycFormConfigurationDetails
--	   WHERE 1=1
--	   AND numFormID = @FormId
--	   AND ISNULL(numFieldID,0)= -1
--	   AND numDomainID = @numDomainID	
            END	   
--ELSE IF @PageId = 12
--	BEGIN
--		SELECT  CONVERT(VARCHAR(9),numFieldID) + '~0' numFieldID,
--				ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName,
--				vcDbColumnName ,
--				0 AS Custom,
--				tintRow AS tintOrder,
--				vcAssociatedControlType,
--				numlistid,
--				vcLookBackTableName,
--				numFieldID AS FieldId,
--				bitAllowEdit                    
--		FROM View_DynamicColumns
--		WHERE numFormId = @FormId 
--		AND numUserCntID = @numUserCntID 
--		AND  numDomainID = @numDomainID  
--	    AND ISNULL(bitCustom,0) = 0 
--		AND tintPageType = 1 
--		AND ISNULL(bitSettingField,0) = 1 
--		AND ISNULL(numRelCntType,0) = @numtype 
--		AND ISNULL(bitDeleted,0) = 0
--	    AND  ISNULL(numViewID,0) = @numViewID   
--	   
--	    UNION
--	   
--	    SELECT  CONVERT(VARCHAR(9),numFieldID)+'~1' AS numFieldID ,
--				vcFieldName ,'' AS vcDbColumnName,
--				1 AS Custom,
--				tintRow AS tintOrder,
--				vcAssociatedControlType,
--				numlistid,'' AS vcLookBackTableName,
--				numFieldID AS FieldId,
--				0 AS bitAllowEdit           
--	    FROM View_DynamicCustomColumns          
--	    WHERE numFormID = @FormId 
--		AND numUserCntID = @numUserCntID 
--		--AND numDomainID=@numDomainID
--		AND grp_id = @PageId  
--		AND numDomainID = @numDomainID  
--		AND vcAssociatedControlType <> 'Link' 
--		AND ISNULL(bitCustom,0) = 1 
--		--AND tintPageType = 1  
--		---AND ISNULL(numRelCntType,0)= @numtype     
--		--AND  ISNULL(numViewID,0) = @numViewID
--	    ORDER BY tintOrder     
--
--	END 
        ELSE
            IF @PageId = 5
                AND ( @FormId = 84
                      OR @FormId = 85
                    )
                BEGIN

                    IF EXISTS ( SELECT  'col1'
                                FROM    DycFormConfigurationDetails
                                WHERE   numDomainID = @numDomainID
                                        AND numFormId = @FormID
                                        AND numFieldID IN ( 507, 508, 509, 510,
                                                            511, 512 ) )
                        BEGIN
                            SELECT  CONVERT(VARCHAR(9), numFieldID) + '~0' numFieldID ,
                                    ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                                    vcDbColumnName ,
                                    0 AS Custom ,
                                    tintRow AS tintOrder ,
                                    vcAssociatedControlType ,
                                    numlistid ,
                                    vcLookBackTableName ,
                                    numFieldID AS FieldId ,
                                    bitAllowEdit ,
                                    0 AS AZordering
                            FROM    View_DynamicColumns
                            WHERE   numFormId = @FormID
                                    AND numDomainID = @numDomainID
                                    AND ISNULL(bitCustom, 0) = 0   
		 --AND tintPageType = 1   
                                    AND ISNULL(bitSettingField, 0) = 1
                                    AND ISNULL(bitDeleted, 0) = 0
                            UNION
                            SELECT  CONVERT(VARCHAR(9), numFieldID) + '~1' AS numFieldID ,
                                    vcFieldName ,
                                    '' AS vcDbColumnName ,
                                    1 AS Custom ,
                                    tintRow AS tintOrder ,
                                    vcAssociatedControlType ,
                                    numlistid ,
                                    '' AS vcLookBackTableName ,
                                    numFieldID AS FieldId ,
                                    0 AS bitAllowEdit ,
                                    0 AS AZordering
                            FROM    View_DynamicCustomColumns
                            WHERE   numFormID = @FormID
                                    AND numDomainID = @numDomainID
                                    AND grp_id = @PageID
                                    AND vcAssociatedControlType = 'TextBox'
                                    AND ISNULL(bitCustom, 0) = 1
                            ORDER BY tintOrder 
                        END
                    ELSE
                        BEGIN
                            SELECT  CONVERT(VARCHAR(9), numFieldID) + '~0' numFieldID ,
                                    ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                                    vcDbColumnName ,
                                    0 AS Custom ,
                                    tintRow AS tintOrder ,
                                    vcAssociatedControlType ,
                                    numlistid ,
                                    vcLookBackTableName ,
                                    numFieldID AS FieldId ,
                                    bitAllowEdit ,
                                    0 AS AZordering
                            FROM    View_DynamicColumns
                            WHERE   numFormId = @FormID
                                    AND numDomainID = @numDomainID
                                    AND ISNULL(bitCustom, 0) = 0   
		 --AND tintPageType = 1   
                                    AND ISNULL(bitSettingField, 0) = 1
                                    AND ISNULL(bitDeleted, 0) = 0
                            UNION
                            SELECT  CONVERT(VARCHAR(9), numFieldID) + '~1' AS numFieldID ,
                                    vcFieldName ,
                                    '' AS vcDbColumnName ,
                                    1 AS Custom ,
                                    tintRow AS tintOrder ,
                                    vcAssociatedControlType ,
                                    numlistid ,
                                    '' AS vcLookBackTableName ,
                                    numFieldID AS FieldId ,
                                    0 AS bitAllowEdit ,
                                    0 AS AZordering
                            FROM    View_DynamicCustomColumns
                            WHERE   numFormID = @FormID
                                    AND numDomainID = @numDomainID
                                    AND grp_id = @PageID
                                    AND vcAssociatedControlType = 'TextBox'
                                    AND ISNULL(bitCustom, 0) = 1
                            UNION
                            SELECT  '507~0' AS numFieldID ,
                                    'Title:A-Z' AS vcFieldName ,
                                    'Title:A-Z' AS vcDbColumnName ,
                                    1 AS Custom ,
                                    0 AS tintOrder ,
                                    ' ' AS vcAssociatedControlType ,
                                    1 AS numlistid ,
                                    '' AS vcLookBackTableName ,
                                    507 AS FieldId ,
                                    0 AS bitAllowEdit ,
                                    1 AS IsFixed
                            UNION
                            SELECT  '508~0' AS numFieldID ,
                                    'Title:Z-A' AS vcFieldName ,
                                    'Title:Z-A' AS vcDbColumnName ,
                                    1 AS Custom ,
                                    0 AS tintOrder ,
                                    ' ' AS vcAssociatedControlType ,
                                    1 AS numlistid ,
                                    '' AS vcLookBackTableName ,
                                    508 AS FieldId ,
                                    0 AS bitAllowEdit ,
                                    1 AS IsFixed
                            UNION
                            SELECT  '509~0' AS numFieldID ,
                                    'Price:Low to High' AS vcFieldName ,
                                    'Price:Low to High' AS vcDbColumnName ,
                                    1 AS Custom ,
                                    0 AS tintOrder ,
                                    ' ' AS vcAssociatedControlType ,
                                    1 AS numlistid ,
                                    '' AS vcLookBackTableName ,
                                    509 AS FieldId ,
                                    0 AS bitAllowEdit ,
                                    1 AS IsFixed
                            UNION
                            SELECT  '510~0' AS numFieldID ,
                                    'Price:High to Low' AS vcFieldName ,
                                    'Price:High to Low' AS vcDbColumnName ,
                                    1 AS Custom ,
                                    0 AS tintOrder ,
                                    ' ' AS vcAssociatedControlType ,
                                    1 AS numlistid ,
                                    '' AS vcLookBackTableName ,
                                    510 AS FieldId ,
                                    0 AS bitAllowEdit ,
                                    1 AS IsFixed
                            UNION
                            SELECT  '511~0' AS numFieldID ,
                                    'New Arrival' AS vcFieldName ,
                                    'New Arrival' AS vcDbColumnName ,
                                    1 AS Custom ,
                                    0 AS tintOrder ,
                                    ' ' AS vcAssociatedControlType ,
                                    1 AS numlistid ,
                                    '' AS vcLookBackTableName ,
                                    511 AS FieldId ,
                                    0 AS bitAllowEdit ,
                                    1 AS IsFixed
                            UNION
                            SELECT  '512~0' AS numFieldID ,
                                    'Oldest' AS vcFieldName ,
                                    'Oldest' AS vcDbColumnName ,
                                    1 AS Custom ,
                                    0 AS tintOrder ,
                                    ' ' AS vcAssociatedControlType ,
                                    1 AS numlistid ,
                                    '' AS vcLookBackTableName ,
                                    512 AS FieldId ,
                                    0 AS bitAllowEdit ,
                                    1 AS IsFixed
                            ORDER BY tintOrder 

                        END



    

                END
            ELSE
                BEGIN  
 
                    SELECT  CONVERT(VARCHAR(9), numFieldID) + '~0' numFieldID ,
                            ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                            vcDbColumnName ,
                            0 AS Custom ,
                            tintRow AS tintOrder ,
                            vcAssociatedControlType ,
                            numlistid ,
                            vcLookBackTableName ,
                            numFieldID AS FieldId ,
                            bitAllowEdit
                    FROM    View_DynamicColumns
                    WHERE   numFormId = @FormId
                            AND numUserCntID = @numUserCntID
                            AND numDomainID = @numDomainID
                            AND ISNULL(bitCustom, 0) = 0
                            AND tintPageType = 1
                            AND ISNULL(bitSettingField, 0) = 1
                            AND ISNULL(numRelCntType, 0) = @numtype
                            AND ISNULL(bitDeleted, 0) = 0
                            AND ISNULL(numViewID, 0) = @numViewID
                    UNION
                    SELECT  CONVERT(VARCHAR(9), numFieldID) + '~1' AS numFieldID ,
                            vcFieldName ,
                            '' AS vcDbColumnName ,
                            1 AS Custom ,
                            tintRow AS tintOrder ,
                            vcAssociatedControlType ,
                            numlistid ,
                            '' AS vcLookBackTableName ,
                            numFieldID AS FieldId ,
                            0 AS bitAllowEdit
                    FROM    View_DynamicCustomColumns
                    WHERE   numFormID = @FormId
                            AND numUserCntID = @numUserCntID
                            AND numDomainID = @numDomainID
                            AND grp_id = @PageId
                            AND numDomainID = @numDomainID
                            AND vcAssociatedControlType <> 'Link'
                            AND ISNULL(bitCustom, 0) = 1
                            AND tintPageType = 1
                            AND ISNULL(numRelCntType, 0) = @numtype
                            AND ISNULL(numViewID, 0) = @numViewID
                    ORDER BY tintOrder          
                END
GO
/****** Object:  StoredProcedure [dbo].[USP_GetDealsList1]    Script Date: 05/07/2009 22:12:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj

--exec USP_GetDealsList1 
--@numUserCntID=85098,
--@numDomainID=110,
--@tintSalesUserRightType=3,
--@tintPurchaseUserRightType=3,
--@tintSortOrder=1,
--@SortChar=0,
--@FirstName='',
--@LastName='',
--@CustName='',
--@CurrentPage=1,
--@PageSize=50,
--@TotRecs=0,
--@columnName='vcCompanyName',
--@columnSortOrder='Asc',
--@numCompanyID=0,
--@bitPartner=0,
--@ClientTimeZoneOffset=-180,
--@tintShipped=0,
--@intOrder=0,
--@intType=1,
--@tintFilterBy=0


GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getdealslist1')
DROP PROCEDURE usp_getdealslist1
GO
CREATE PROCEDURE [dbo].[USP_GetDealsList1]
               @numUserCntID              NUMERIC(9)  = 0,
               @numDomainID               NUMERIC(9)  = 0,
               @tintSalesUserRightType    TINYINT  = 0,
               @tintPurchaseUserRightType TINYINT  = 0,
               @tintSortOrder             TINYINT  = 4,
               @SortChar                  CHAR(1)  = '0',
               @FirstName                 VARCHAR(100)  = '',
               @LastName                  VARCHAR(100)  = '',
               @CustName                  VARCHAR(100)  = '',
               @CurrentPage               INT,
               @PageSize                  INT,
               @TotRecs                   INT  OUTPUT,
               @columnName                AS VARCHAR(50),
               @columnSortOrder           AS VARCHAR(10),
               @numCompanyID              AS NUMERIC(9)  = 0,
               @bitPartner                AS BIT  = 0,
               @ClientTimeZoneOffset      AS INT,
               @tintShipped               AS TINYINT,
               @intOrder                  AS TINYINT,
               @intType                   AS TINYINT,
               @tintFilterBy              AS TINYINT  = 0,
               @numOrderStatus			  AS NUMERIC,
			   @vcRegularSearchCriteria varchar(1000)='',
			   @vcCustomSearchCriteria varchar(1000)='' 
AS
  DECLARE  @PageId  AS TINYINT
  DECLARE  @numFormId  AS INT 
  
  SET @PageId = 0
  IF @inttype = 1
  BEGIN
  	SET @PageId = 2
  	SET @inttype = 3
  	SET @numFormId=39
  END
  IF @inttype = 2
  BEGIN
  	SET @PageId = 6
    SET @inttype = 4
    SET @numFormId=41
  END
  --Create a Temporary table to hold data
  CREATE TABLE #tempTable (
    ID              INT   IDENTITY   PRIMARY KEY,
    numOppId        NUMERIC(9),intTotalProgress INT,
	vcInventoryStatus	VARCHAR(100))
    
 DECLARE @strShareRedordWith AS VARCHAR(300);
 SET @strShareRedordWith=' Opp.numOppId in (select SR.numRecordID FROM ShareRecord SR WHERE SR.numDomainID='+ convert(varchar(15),@numDomainID )+' AND SR.numModuleID=3 AND SR.numAssignedTo='+convert(varchar(15),@numUserCntId)+') '

  DECLARE  @strSql  AS VARCHAR(8000)
  SET @strSql = 'SELECT  Opp.numOppId,PP.intTotalProgress,RTRIM(LTRIM(REPLACE(SUBSTRING(dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID),CHARINDEX(''>'',dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID)) + 1, CHARINDEX(''<font>'',dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID))),''<font>'',''''))) AS [vcInventoryStatus]'
  
  DECLARE  @strOrderColumn  AS VARCHAR(100);SET @strOrderColumn=@columnName
--  
--   IF LEN(@columnName)>0
--   BEGIN
--	 IF CHARINDEX(@columnName,@strSql)=0
--	 BEGIN
--		SET @strSql= @strSql + ',' + @columnName
--	 	SET @strOrderColumn=@columnName
--	 END
--   END
   
  SET @strSql = @strSql + ' FROM OpportunityMaster Opp                                                               
                                 INNER JOIN AdditionalContactsInformation ADC ON Opp.numContactId = ADC.numContactId                                                               
                                 INNER JOIN DivisionMaster Div ON Opp.numDivisionId = Div.numDivisionID AND ADC.numDivisionId = Div.numDivisionID
								 INNER JOIN CompanyInfo cmp ON Div.numCompanyID = cmp.numCompanyId 
								 LEFT JOIN ProjectProgress PP On PP.numOppId = Opp.numOppId '
                                 --left JOIN OpportunityLinking OL on OL.numChildOppID=Opp.numOppId '
								 
  IF @bitPartner = 1
    SET @strSql = @strSql + ' left join OpportunityContact OppCont on OppCont.numOppId=Opp.numOppId and OppCont.bitPartner=1 and OppCont.numContactId=' + CONVERT(VARCHAR(15),@numUserCntID)
                    
   
   if @columnName like 'CFW.Cust%'             
	begin            
		SET @strSql = @strSql + ' left Join CFW_Fld_Values_Opp CFW on CFW.RecId=Opp.numOppid  and CFW.fld_id= '+replace(@columnName,'CFW.Cust','') 
		SET @strOrderColumn = 'CFW.Fld_Value'   
	end                                     
  ELSE if @columnName like 'DCust%'            
	begin            
	 SET @strSql = @strSql + ' left Join CFW_Fld_Values_Opp CFW on CFW.RecId=Opp.numOppid   and CFW.fld_id= '+replace(@columnName,'DCust','')                                                                                               
	 SET @strSql = @strSql + ' left Join ListDetails LstCF on LstCF.numListItemID=CFW.Fld_Value  '            
	 SET @strOrderColumn = 'LstCF.vcData'  
	end       

             
  IF @tintFilterBy = 1 --Partially Fulfilled Orders 
    SET @strSql = @strSql + ' left join AdditionalContactsInformation ADC1 on ADC1.numContactId=Opp.numRecOwner                                                               
                             LEFT OUTER JOIN [OpportunityBizDocs] OB ON OB.[numOppId] = Opp.[numOppId] 
                             WHERE Opp.numDomainID=' + CONVERT(VARCHAR(15),@numDomainID) + ' and Opp.tintOppstatus=1 and Opp.tintShipped=' + CONVERT(VARCHAR(15),@tintShipped)
  ELSE
    SET @strSql = @strSql + ' left join AdditionalContactsInformation ADC1 on ADC1.numContactId=Opp.numRecOwner                                                               
                             WHERE Opp.numDomainID=' + CONVERT(VARCHAR(15),@numDomainID) + ' and Opp.tintOppstatus=1 and Opp.tintShipped='+ CONVERT(VARCHAR(15),@tintShipped) 
            
                    
  IF @FirstName <> ''
    SET @strSql = @strSql + ' and ADC.vcFirstName  like ''' + @FirstName + '%'''
    
  IF @LastName <> '' 
    SET @strSql = @strSql + ' and ADC.vcLastName like ''' + @LastName + '%'''
  
  IF @CustName <> ''
    SET @strSql = @strSql + ' and cmp.vcCompanyName like ''' + @CustName + '%'''
  
  
  IF @intOrder <> 0
    BEGIN
      IF @intOrder = 1
        SET @strSql = @strSql + ' and Opp.bitOrder = 0 '
      IF @intOrder = 2
        SET @strSql = @strSql + ' and Opp.bitOrder = 1'
    END
    
  IF @numCompanyID <> 0
    SET @strSql = @strSql + ' And Div.numCompanyID ='+ CONVERT(VARCHAR(15),@numCompanyID)
    
  IF @SortChar <> '0'
    SET @strSql = @strSql + ' And Opp.vcPOppName like ''' + @SortChar + '%'''
  
  
  IF @tintSalesUserRightType = 0 OR @tintPurchaseUserRightType = 0
    SET @strSql = @strSql + ' AND opp.tintOppType =0'
  ELSE IF @tintSalesUserRightType = 1 OR @tintPurchaseUserRightType = 1
    SET @strSql = @strSql + ' AND (Opp.numRecOwner= ' + CONVERT(VARCHAR(15),@numUserCntID)
                    + ' or Opp.numAssignedTo= '+ CONVERT(VARCHAR(15),@numUserCntID)
                    --+ ' or  Opp.numOppId in (select distinct(numOppId) from OpportunityStageDetails where  numAssignTo ='+ CONVERT(VARCHAR(15),@numUserCntID)+ ')'
                    + CASE WHEN @bitPartner = 1 THEN ' or ( OppCont.bitPartner=1 and OppCont.numContactId='+ CONVERT(VARCHAR(15),@numUserCntID) + ')' ELSE '' END
                    + ' or ' + @strShareRedordWith +')'
  ELSE IF @tintSalesUserRightType = 2 OR @tintPurchaseUserRightType = 2
      SET @strSql = @strSql + ' AND (Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+ CONVERT(VARCHAR(15),@numUserCntID) + ' ) 
								or div.numTerID=0 or Opp.numAssignedTo= '+ CONVERT(VARCHAR(15),@numUserCntID)
								 --+ ' or  Opp.numOppId in (select distinct(numOppId) from OpportunityStageDetails where numAssignTo ='+ CONVERT(VARCHAR(15),@numUserCntID)+ ')' 
								+ ' or ' + @strShareRedordWith +')'
	
								
  IF @tintSortOrder <> '0'
    SET @strSql = @strSql + '  AND Opp.tintOppType= '+ CONVERT(VARCHAR(1),@tintSortOrder)
    
    
 IF @tintFilterBy = 1 --Partially Fulfilled Orders
    SET @strSql = @strSql + ' AND OB.bitPartialFulfilment = 1 AND OB.bitAuthoritativeBizDocs = 1 '
 ELSE IF @tintFilterBy = 2 --Fulfilled Orders
    SET @strSql = @strSql + ' AND Opp.[numOppId] IN (SELECT OM.numOppId FROM [OpportunityMaster] OM
                      			INNER JOIN [OpportunityBizDocs] OB ON OB.[numOppId] = OM.[numOppId]
                      			INNER JOIN [OpportunityItems] OI ON OM.[numOppId] = OI.[numOppId]
                      			INNER JOIN [OpportunityBizDocItems] BI ON BI.[numOppBizDocID] = OB.[numOppBizDocsId]
                      			Where OM.numDomainId=' + CONVERT(VARCHAR(15),@numDomainId) + 'AND OB.[bitAuthoritativeBizDocs] = 1 GROUP BY OM.[numOppId]
                      			HAVING   COUNT(OI.[numUnitHour]) = COUNT(BI.[numUnitHour])) '
 ELSE IF @tintFilterBy = 3 --Orders without Authoritative-BizDocs
    SET @strSql = @strSql + ' AND Opp.[numOppId] not IN (SELECT DISTINCT OM.[numOppId]
                      		  FROM [OpportunityMaster] OM JOIN [OpportunityBizDocs] OB ON OM.[numOppId] = OB.[numOppId]
                      		  Where OM.numDomainId=' + CONVERT(VARCHAR(15),@numDomainId) + ' AND OM.tintOppstatus=1 AND OM.tintShipped=' + CONVERT(VARCHAR(15),@tintShipped) + ' AND OM.tintOppType= '+ CONVERT(VARCHAR(1),@tintSortOrder) + ' AND isnull(OB.[bitAuthoritativeBizDocs],0)=1) '
--ELSE IF @tintFilterBy = 4 --Orders with child records
--    SET @strSql = @strSql
--                    + ' AND Opp.[numOppId] IN (SELECT numParentOppID FROM dbo.OpportunityLinking OL INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OL.numParentOppID WHERE OM.numDomainId='+ CONVERT(VARCHAR(15),@numDomainID) + ' )  '

 ELSE IF @tintFilterBy = 4 --Orders with items yet to be added to Invoices
    SET @strSql = @strSql + ' AND Opp.[numOppId] IN (SELECT distinct OPPM.numOppId FROM [OpportunityMaster] OPPM
                                  INNER JOIN [OpportunityItems] OPPI ON OPPM.[numOppId] = OPPI.[numOppId]
                                  Where OPPM.numDomainId=' + CONVERT(VARCHAR(15),@numDomainId) + ' 
								  and OPPI.numoppitemtCode not in (select numOppItemID from [OpportunityBizDocs] OB INNER JOIN [OpportunityBizDocItems] BI
                      			  ON BI.[numOppBizDocID] = OB.[numOppBizDocsId] where OB.[numOppId] = OPPM.[numOppId]))'      
 ELSE IF @tintFilterBy = 6 --Orders with deferred Income/Invoices
    SET @strSql = @strSql + ' AND Opp.[numOppId] IN (SELECT DISTINCT OM.[numOppId]
                      		 FROM [OpportunityMaster] OM LEFT OUTER JOIN [OpportunityBizDocs] OB ON OM.[numOppId] = OB.[numOppId]
                      		 Where OM.numDomainId=' + CONVERT(VARCHAR(15),@numDomainId) + ' AND OB.[bitAuthoritativeBizDocs] =1 and OB.tintDeferred=1)'

IF @numOrderStatus <>0 
	SET @strSql = @strSql + ' AND Opp.numStatus = ' + CONVERT(VARCHAR(15),@numOrderStatus);

IF CHARINDEX('vcInventoryStatus',@vcRegularSearchCriteria) > 0
	BEGIN
		IF CHARINDEX('vcInventoryStatus=1',@vcRegularSearchCriteria) > 0
			SET @strSql = @strSql + ' and ' + ' CHARINDEX(''Not Applicable'', dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID)) > 0 '
		IF CHARINDEX('vcInventoryStatus=2',@vcRegularSearchCriteria) > 0
			SET @strSql = @strSql + ' and ' + ' CHARINDEX(''Allocation Cleared'', dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID)) > 0 '
		IF CHARINDEX('vcInventoryStatus=3',@vcRegularSearchCriteria) > 0
			SET @strSql = @strSql + ' and ' + ' CHARINDEX(''Back Order'', dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID)) > 0 '
		IF CHARINDEX('vcInventoryStatus=4',@vcRegularSearchCriteria) > 0
			SET @strSql = @strSql + ' and ' + ' CHARINDEX(''Ready to Ship'', dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID)) > 0 '
		  
	END
ELSE IF @vcRegularSearchCriteria<>'' 
	BEGIN
		SET @strSql=@strSql+' and ' + @vcRegularSearchCriteria 
	END
	
IF @vcCustomSearchCriteria<>'' 
	set @strSql=@strSql+' and Opp.numOppid in (select distinct CFW.RecId from CFW_Fld_Values_Opp CFW where ' + @vcCustomSearchCriteria + ')'

 --SET @strSql = 'SELECT numOppId from (' + @strSql + ') a'

 IF LEN(@strOrderColumn)>0
	IF @strOrderColumn = 'OI.vcInventoryStatus'
		BEGIN
			SET @strSql =  @strSql + ' ORDER BY  RTRIM(LTRIM(REPLACE(SUBSTRING(dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID),CHARINDEX(''>'',dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID)) + 1, CHARINDEX(''<font>'',dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID))),''<font>'','''')))'  + ' ' + @columnSortOrder    
		END
	ELSE
		BEGIN
			SET @strSql =  @strSql + ' ORDER BY  ' + @strOrderColumn + ' ' + @columnSortOrder    
		END

	
  PRINT @strSql
  INSERT INTO #tempTable (numOppId,intTotalProgress,vcInventoryStatus) EXEC( @strSql)
  
  DECLARE  @firstRec  AS INTEGER
  DECLARE  @lastRec  AS INTEGER
  SET @firstRec = (@CurrentPage - 1) * @PageSize
  SET @lastRec = (@CurrentPage * @PageSize + 1)
  
  SET @TotRecs = (SELECT COUNT(* ) FROM #tempTable)
  
  DECLARE  @tintOrder  AS TINYINT;SET @tintOrder = 0
  DECLARE  @vcFieldName  AS VARCHAR(50)
  DECLARE  @vcListItemType  AS VARCHAR(3)
  DECLARE  @vcListItemType1  AS VARCHAR(1)
  DECLARE  @vcAssociatedControlType VARCHAR(30)
  DECLARE  @numListID  AS NUMERIC(9)
  DECLARE  @vcDbColumnName VARCHAR(40)
  DECLARE  @WhereCondition VARCHAR(2000);SET @WhereCondition = ''
  DECLARE  @vcLookBackTableName VARCHAR(2000)
  DECLARE  @bitCustom  AS BIT
  Declare @numFieldId as numeric  
  DECLARE @bitAllowSorting AS CHAR(1)   
  DECLARE @bitAllowEdit AS CHAR(1)                 
  DECLARE @vcColumnName AS VARCHAR(500)                  
  DECLARE  @Nocolumns  AS TINYINT;SET @Nocolumns = 0

  SELECT @Nocolumns=isnull(sum(TotalRow),0) from(            
	SELECT count(*) TotalRow from View_DynamicColumns where numFormId=@numFormId and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType = @numFormId
	UNION 
	SELECT count(*) TotalRow from View_DynamicCustomColumns where numFormId=@numFormId and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType = @numFormId) TotalRows


  CREATE TABLE #tempForm (tintOrder TINYINT,vcDbColumnName nvarchar(50),vcOrigDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),
  vcListItemType varchar(3),numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,
  numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,bitIsAlphaNumeric BIT,bitIsNumeric BIT,
  bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),ListRelID numeric(9),intColumnWidth int,bitAllowFiltering BIT,vcFieldDataType CHAR(1))
    
  SET @strSql = ''
  SET @strSql = 'select ADC.numContactId,DM.numDivisionID,isnull(DM.numTerID,0) numTerID,opp.numRecOwner as numRecOwner,DM.tintCRMType,opp.numOppId'

if @Nocolumns=0
BEGIN
INSERT INTO DycFormConfigurationDetails (numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth)
select @numFormId,numFieldId,0,Row_number() over(order by tintRow desc),@numDomainID,@numUserCntID,@numFormId,1,0,intColumnWidth
 FROM View_DynamicDefaultColumns
 where numFormId=@numFormId and bitDefault=1 AND ISNULL(bitSettingField,0)=1 and numDomainID=@numDomainID
order by tintOrder asc   

END

    
    INSERT INTO #tempForm
select tintRow+1 as tintOrder,vcDbColumnName,vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
 vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName                                                 
,bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType  
 FROM View_DynamicColumns 
 where numFormId=@numFormId and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 
  AND ISNULL(bitSettingField,0)=1 AND ISNULL(bitCustom,0)=0  AND numRelCntType = @numFormId
 
 UNION
    
     select tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName,                  
 vcAssociatedControlType,'' as vcListItemType,numListID,''                                                 
,bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,0,'' 
 from View_DynamicCustomColumns
 where numFormId=@numFormId and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 
 AND ISNULL(bitCustom,0)=1  AND numRelCntType = @numFormId
 
  order by tintOrder asc  



--  SET @DefaultNocolumns = @Nocolumns
Declare @ListRelID as numeric(9) 

  select top 1 @tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,                
  @vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,@numListID=numListID,@vcLookBackTableName=vcLookBackTableName                                               
,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,
@ListRelID=ListRelID
  from  #tempForm order by tintOrder asc            

  WHILE @tintOrder > 0
    BEGIN
      IF @bitCustom = 0
        BEGIN
          DECLARE  @Prefix  AS VARCHAR(5)
          IF @vcLookBackTableName = 'AdditionalContactsInformation'
            SET @Prefix = 'ADC.'
          IF @vcLookBackTableName = 'DivisionMaster'
            SET @Prefix = 'DM.'
          IF @vcLookBackTableName = 'OpportunityMaster'
            SET @PreFix = 'Opp.'
          IF @vcLookBackTableName = 'OpportunityRecurring'
            SET @PreFix = 'OPR.'
            
		 SET @vcColumnName= @vcDbColumnName+'~'+ convert(varchar(10),@numFieldId) +'~0'
            
          IF @vcAssociatedControlType = 'SelectBox'
            BEGIN
            IF @vcDbColumnName = 'tintSource'
            BEGIN
              SET @strSql = @strSql + ',ISNULL(dbo.fn_GetOpportunitySourceValue(ISNULL(Opp.tintSource,0),ISNULL(Opp.tintSourceType,0),Opp.numDomainID),''Internal Order'')' + ' [' + @vcColumnName + ']'
            END
            
			IF @vcDbColumnName = 'vcInventoryStatus'
            BEGIN
			SET @strSql = @strSql + ', ISNULL(dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID),'''') ' + ' [' + @vcColumnName + ']'
			END

            ELSE IF @vcDbColumnName = 'numCampainID'
            BEGIN
              SET @strSql = @strSql + ', (SELECT vcCampaignName FROM CampaignMaster WHERE numCampaignID= Opp.numCampainID) ' + ' [' + @vcColumnName + ']'
            END
            
            ELSE IF @vcListItemType = 'LI'
            BEGIN
                  SET @strSql = @strSql + ',L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData' + ' [' + @vcColumnName + ']'
                  SET @WhereCondition = @WhereCondition + ' left Join ListDetails L' + CONVERT(VARCHAR(3),@tintOrder) + ' on L' + CONVERT(VARCHAR(3),@tintOrder) + '.numListItemID=' + @vcDbColumnName
            END
            ELSE IF @vcListItemType = 'S'
            BEGIN
                    SET @strSql = @strSql + ',S' + CONVERT(VARCHAR(3),@tintOrder) + '.vcState' + ' [' + @vcColumnName + ']'
                    SET @WhereCondition = @WhereCondition + ' left join State S' + CONVERT(VARCHAR(3),@tintOrder) + ' on S' + CONVERT(VARCHAR(3),@tintOrder) + '.numStateID=' + @vcDbColumnName
            END
            ELSE IF @vcListItemType = 'BP'
            BEGIN
                    SET @strSql = @strSql + ',SPLM.Slp_Name' + ' [' + @vcColumnName + ']'
                    SET @WhereCondition = @WhereCondition + ' LEFT JOIN Sales_process_List_Master SPLM on SPLM.Slp_Id = OPP.numBusinessProcessID '
                  END
            ELSE IF @vcListItemType = 'PP'
            BEGIN
                              SET @strSql = @strSql + ',ISNULL(T.intTotalProgress,0)' + ' [' + @vcColumnName + ']'

--                SET @strSql = @strSql 
--                                + ',ISNULL(PP.intTotalProgress,0)'
--                                + ' [' +
--                                @vcColumnName + ']'
--                SET @WhereCondition = @WhereCondition
--                                        + ' LEFT JOIN ProjectProgress PP ON PP.numDomainID='+ convert(varchar(15),@numDomainID )+' and OPP.numOppID = PP.numOppID '
            END
            ELSE IF @vcListItemType = 'T'
                    BEGIN
                      SET @strSql = @strSql + ',L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData' + ' [' + @vcColumnName + ']'
                      SET @WhereCondition = @WhereCondition + ' left Join ListDetails L' + CONVERT(VARCHAR(3),@tintOrder) + ' on L' + CONVERT(VARCHAR(3),@tintOrder) + '.numListItemID=' + @vcDbColumnName
                    END
             ELSE IF @vcListItemType = 'U'
                      BEGIN
                        SET @strSql = @strSql + ',dbo.fn_GetContactName(' + @Prefix + @vcDbColumnName + ') [' + @vcColumnName + ']'
                      END
              ELSE IF @vcListItemType = 'WI'
				BEGIN
					SET @strSql = @strSql + ', Case when isnull(OL.numSiteID,0)>0 then Sites.vcSiteName else WebAPI.vcProviderName end [' + @vcColumnName + ']'
					set @WhereCondition= @WhereCondition +' left JOIN OpportunityLinking OL on OL.numChildOppID=Opp.numOppId left join WebAPI on WebAPI.WebApiId=isnull(OL.numWebApiId,0)
													left join Sites on isnull(OL.numSiteID,0)=Sites.numSIteID and Sites.numDomainID=' + convert(varchar(15),@numDomainID)                                                       
				END 
            END
          ELSE
            IF @vcAssociatedControlType = 'DateField'
              BEGIN
              if @Prefix ='OPR.'
				 BEGIN
				  	SET @strSql = @strSql + ', (SELECT TOP 1 dbo.FormatedDateFromDate( DateAdd(minute,+'+convert(varchar(15),-@ClientTimeZoneOffset)+', dtRecurringDate)'+ ','+ CONVERT(VARCHAR(10),@numDomainId) +') FROM  dbo.OpportunityRecurring WHERE numOppID=OPP.numOppID AND ISNULL(numOppBizDocID,0)=0 and dtRecurringDate > dbo.GetUTCDateWithoutTime()) AS  ['+ @vcColumnName + ']'
				 END
				 else
				 BEGIN
					SET @strSql = @strSql
                                + ',case when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','
                                + @Prefix
                                + @vcDbColumnName
                                + ') )= convert(varchar(11),getdate()) then ''<b><font color=red>Today</font></b>'''
					SET @strSql = @strSql
									+ 'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','
									+ @Prefix
									+ @vcDbColumnName
									+ ') )= convert(varchar(11),dateadd(day,-1,getdate())) then ''<b><font color=purple>YesterDay</font></b>'''
					SET @strSql = @strSql
									+ 'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','
									+ @Prefix
									+ @vcDbColumnName
									+ ') )= convert(varchar(11),dateadd(day,1,getdate())) then''<b><font color=orange>Tommorow</font></b>'' '
					SET @strSql = @strSql
									+ 'else dbo.FormatedDateFromDate('
									+ @Prefix
									+ @vcDbColumnName
									+ ','
									+ CONVERT(VARCHAR(10),@numDomainId)
									+ ') end  ['
									+ @vcColumnName + ']' 	
				 END
                
                                
					
						
              END
            ELSE
              IF @vcAssociatedControlType = 'TextBox'
                BEGIN
                  SET @strSql = @strSql
                                  + ','
                                  + CASE 
                                      WHEN @vcDbColumnName = 'numAge' THEN 'year(getutcdate())-year(bintDOB)'
                                      WHEN @vcDbColumnName = 'monPAmount' THEN 'isnull((SELECT varCurrSymbol FROM [Currency] WHERE numCurrencyID = Opp.[numCurrencyID]),'''') + '' ''+ CONVERT(VARCHAR,CONVERT(MONEY,monPAmount),1)'
                                      WHEN @vcDbColumnName = 'numDivisionID' THEN 'DM.numDivisionID'
                                      WHEN @vcDbColumnName = 'tintOppStatus' THEN 'CASE tintOppStatus WHEN 0 then ''Open'' WHEN 1 THEN ''Closed-Won'' WHEN 2 THEN ''Closed-Lost'' END '
                                      WHEN @vcDbColumnName = 'tintOppType' THEN 'CASE tintOppType WHEN 1 then ''Sales'' WHEN 2 THEN ''Purchase'' Else ''NA'' END '
                                      WHEN @vcDbColumnName = 'vcPOppName' THEN 'Opp.vcPOppName'
                                      ELSE @vcDbColumnName
                                    END
                                  + ' ['
                                  + @vcColumnName + ']'
                END
              ELSE
                IF @vcAssociatedControlType = 'TextArea'
                  BEGIN
						set @strSql=@strSql+','+  @Prefix + @vcDbColumnName +' ['+ @vcColumnName+']'  
                  END
                   else if  @vcAssociatedControlType='Label'                                              
begin  
 set @strSql=@strSql+','+ case                    
	WHEN @vcDbColumnName = 'CalAmount' THEN 'Opp.monDealAmount'--'[dbo].[getdealamount](Opp.numOppId,Getutcdate(),0)'
    WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monDealAmount' THEN 'ISNULL((SELECT SUM(ISNULL(monDealAmount,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
    WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monAmountPaid' THEN 'ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
    WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'vcBizDocsList' THEN 'ISNULL((SELECT SUBSTRING((SELECT ''$^$'' + CAST(numOppBizDocsId AS VARCHAR(18)) +''#^#''+ vcBizDocID
	FROM OpportunityBizDocs WHERE numOppId=Opp.numOppId FOR XML PATH('''')),4,200000)),'''')'
    else @vcDbColumnName END +' ['+ @vcColumnName+']'            
 END

                   	else if @vcAssociatedControlType='Popup' and @vcDbColumnName='numShareWith'                                                   
		begin      
			 set @strSql=@strSql+',(SELECT SUBSTRING(
	(SELECT '','' + A.vcFirstName+'' ''+A.vcLastName + CASE WHEN ISNULL(SR.numContactType,0)>0 THEN ''('' + dbo.fn_GetListItemName(SR.numContactType) + '')'' ELSE '''' END
	FROM ShareRecord SR JOIN UserMaster UM ON UM.numUserDetailId=SR.numAssignedTo 
							 JOIN AdditionalContactsInformation A ON UM.numUserDetailId=A.numContactID 
		 WHERE SR.numDomainID=Opp.numDomainID AND SR.numModuleID=3 AND SR.numRecordID=Opp.numOppId
				AND UM.numDomainID=Opp.numDomainID and UM.numDomainID=A.numDomainID FOR XML PATH('''')),2,200000)) ['+ @vcColumnName+']'                
		 end 
        END
      ELSE
        BEGIN
        
            SET @vcColumnName= @vcDbColumnName +'~'+ convert(varchar(10),@numFieldId)+'~1'

          IF @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'
            BEGIN
              SET @strSql = @strSql
                              + ',CFW'
                              + CONVERT(VARCHAR(3),@tintOrder)
                              + '.Fld_Value  ['
                              + @vcColumnName + ']'
              SET @WhereCondition = @WhereCondition
                                      + ' left Join CFW_FLD_Values_Opp CFW'
                                      + CONVERT(VARCHAR(3),@tintOrder)
                                      + '               
                                                                                                                            on CFW'
                                      + CONVERT(VARCHAR(3),@tintOrder)
                                      + '.Fld_Id='
                                      + CONVERT(VARCHAR(10),@numFieldId)
                                      + 'and CFW'
                                      + CONVERT(VARCHAR(3),@tintOrder)
                                      + '.RecId=Opp.numOppid   '
            END
          ELSE
            IF @vcAssociatedControlType = 'CheckBox' 
              BEGIN
                SET @strSql = @strSql
                                + ',case when isnull(CFW'
                                + CONVERT(VARCHAR(3),@tintOrder)
                                + '.Fld_Value,0)=0 then ''No'' when isnull(CFW'
                                + CONVERT(VARCHAR(3),@tintOrder)
                                + '.Fld_Value,0)=1 then ''Yes'' end   ['
                                + @vcColumnName + ']'
                SET @WhereCondition = @WhereCondition
                                        + ' left Join CFW_FLD_Values_Opp CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '           
                                                                                                                                  on CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.Fld_Id='
                                        + CONVERT(VARCHAR(10),@numFieldId)
                                        + 'and CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.RecId=Opp.numOppid   '
              END
            ELSE
              IF @vcAssociatedControlType = 'DateField'
                BEGIN
                  SET @strSql = @strSql
                                  + ',dbo.FormatedDateFromDate(CFW'
                                  + CONVERT(VARCHAR(3),@tintOrder)
                                  + '.Fld_Value,'
                                  + CONVERT(VARCHAR(10),@numDomainId)
                                  + ')  ['
                                  + @vcColumnName + ']'
                  SET @WhereCondition = @WhereCondition
                                          + ' left Join CFW_FLD_Values_Opp CFW'
                                          + CONVERT(VARCHAR(3),@tintOrder)
                                          + '               
                                                                                                                                        on CFW'
                                          + CONVERT(VARCHAR(3),@tintOrder)
                                          + '.Fld_Id='
                                          + CONVERT(VARCHAR(10),@numFieldId)
                                          + 'and CFW'
                                          + CONVERT(VARCHAR(3),@tintOrder)
                                          + '.RecId=Opp.numOppid    '
                END
              ELSE
                IF @vcAssociatedControlType = 'SelectBox'
                  BEGIN
                    SET @vcDbColumnName = 'DCust'
                                            + CONVERT(VARCHAR(10),@numFieldId)
                    SET @strSql = @strSql
                                    + ',L'
                                    + CONVERT(VARCHAR(3),@tintOrder)
                                    + '.vcData'
                                    + ' ['
                                    + @vcColumnName + ']'
                    SET @WhereCondition = @WhereCondition
                                            + ' left Join CFW_FLD_Values_Opp CFW'
                                            + CONVERT(VARCHAR(3),@tintOrder)
                                            + '               
                                                                                                                                               on CFW'
                                            + CONVERT(VARCHAR(3),@tintOrder)
                                            + '.Fld_Id='
                                            + CONVERT(VARCHAR(10),@numFieldId)
                                            + 'and CFW'
                                            + CONVERT(VARCHAR(3),@tintOrder)
                                            + '.RecId=Opp.numOppid     '
                    SET @WhereCondition = @WhereCondition
                                            + ' left Join ListDetails L'
                                            + CONVERT(VARCHAR(3),@tintOrder)
                                            + ' on L'
                                            + CONVERT(VARCHAR(3),@tintOrder)
                                            + '.numListItemID=CFW'
                                            + CONVERT(VARCHAR(3),@tintOrder)
                                            + '.Fld_Value'
                  END
        END
      
     
       select top 1 @tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,                
  @vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,@numListID=numListID,@vcLookBackTableName=vcLookBackTableName                                               
,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,
@ListRelID=ListRelID
  from #tempForm WHERE tintOrder > @tintOrder-1 order by tintOrder asc            
 
  if @@rowcount=0 set @tintOrder=0 
 END 
  
  -------Change Row Color-------
Declare @vcCSOrigDbCOlumnName as varchar(50),@vcCSLookBackTableName as varchar(50),@vcCSAssociatedControlType as varchar(50)
SET @vcCSOrigDbCOlumnName=''
SET @vcCSLookBackTableName=''

--select top 1 @vcCSOrigDbCOlumnName=DFM.vcOrigDbCOlumnName,@vcCSLookBackTableName=DFM.vcLookBackTableName from DycFieldColorScheme DFCS join DycFieldMaster DFM on 
--DFCS.numModuleID=DFM.numModuleID and DFCS.numFieldID=DFM.numFieldID
--where DFCS.numDomainID=@numDomainID and DFM.numModuleID=1

Create table #tempColorScheme(vcOrigDbCOlumnName varchar(50),vcLookBackTableName varchar(50),vcFieldValue varchar(50),vcFieldValue1 varchar(50),vcColorScheme varchar(50),vcAssociatedControlType varchar(50))

insert into #tempColorScheme select DFM.vcOrigDbCOlumnName,DFM.vcLookBackTableName,DFCS.vcFieldValue,DFCS.vcFieldValue1,DFCS.vcColorScheme,DFFM.vcAssociatedControlType 
from DycFieldColorScheme DFCS join DycFormField_Mapping DFFM on DFCS.numFieldID=DFFM.numFieldID
join DycFieldMaster DFM on DFM.numModuleID=DFFM.numModuleID and DFM.numFieldID=DFFM.numFieldID
where DFCS.numDomainID=@numDomainID and DFFM.numFormID=@numFormId AND DFCS.numFormID=@numFormId and isnull(DFFM.bitAllowGridColor,0)=1

IF(SELECT COUNT(*) FROM #tempColorScheme)>0
BEGIN
   select top 1 @vcCSOrigDbCOlumnName=vcOrigDbCOlumnName,@vcCSLookBackTableName=vcLookBackTableName,@vcCSAssociatedControlType=vcAssociatedControlType from #tempColorScheme
END   
----------------------------                       

IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
BEGIN
set @strSql=@strSql+',tCS.vcColorScheme'
END

  
  SET @strSql = @strSql + ' FROM OpportunityMaster Opp                                                               
                            INNER JOIN AdditionalContactsInformation ADC ON Opp.numContactId = ADC.numContactId                                                               
							INNER JOIN DivisionMaster DM ON Opp.numDivisionId = DM.numDivisionID AND ADC.numDivisionId = DM.numDivisionID                                                               
                            INNER JOIN CompanyInfo cmp ON DM.numCompanyID = cmp.numCompanyId '
  
  IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
BEGIN
	 if @vcCSLookBackTableName = 'AdditionalContactsInformation'                  
		set @Prefix = 'ADC.'                  
     if @vcCSLookBackTableName = 'DivisionMaster'                  
		set @Prefix = 'DM.'
	 if @vcCSLookBackTableName = 'CompanyInfo'                  
		set @Prefix = 'CMP.'   
	 if @vcCSLookBackTableName = 'OpportunityMaster'                  
		set @Prefix = 'Opp.'   
 
	IF @vcCSAssociatedControlType='DateField'
	BEGIN
			set @strSql=@strSql+' left join #tempColorScheme tCS on CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) >= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue as int),GETUTCDATE()),111)
			 and CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) <= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue1 as int),GETUTCDATE()),111)'
	END
	ELSE IF @vcCSAssociatedControlType='SelectBox' OR @vcCSAssociatedControlType='ListBox' OR @vcCSAssociatedControlType='CheckBox'
	BEGIN
		set @strSql=@strSql+' left join #tempColorScheme tCS on tCS.vcFieldValue=' + @Prefix + @vcCSOrigDbCOlumnName
	END
END
                          
if @columnName like 'CFW.Cust%'             
begin            
 SET @strSql = @strSql + ' left Join CFW_Fld_Values_Opp CFW on CFW.RecId=Opp.numOppid and CFW.fld_id= '+replace(@columnName,'CFW.Cust','') +' '                                                     
 set @columnName='CFW.Fld_Value'            
end 

SET @strSql = @strSql   + @WhereCondition + ' join (select ID,numOppId,intTotalProgress,vcInventoryStatus FROM  #tempTable )T on T.numOppId=Opp.numOppId                                              
                          WHERE ID > ' + CONVERT(VARCHAR(10),@firstRec) + ' and ID <' + CONVERT(VARCHAR(10),@lastRec)

IF (@tintFilterBy = 5) --Sort alphabetically
   SET @strSql = @strSql +' order by ID '
ELSE IF @columnName = 'OI.vcInventoryStatus'  
   SET @strSql =  @strSql + ' ORDER BY  T.vcInventoryStatus '  + ' ' + @columnSortOrder     
else  IF (@columnName = 'numAssignedBy' OR @columnName = 'numAssignedTo')
   SET @strSql = @strSql + ' ORDER BY  opp.' +  @columnName + ' ' + @columnSortOrder 
else  IF (@columnName = 'PP.intTotalProgress')
   SET @strSql = @strSql + ' ORDER BY  T.intTotalProgress ' + @columnSortOrder 
ELSE IF @columnName <> 'opp.bintCreatedDate' and @columnName <> 'vcPOppName' and @columnName <> 'bintCreatedDate' and @columnName <> 'vcCompanyName'
   SET @strSql = @strSql + ' ORDER BY  ' +  @columnName + ' '+ @columnSortOrder   
ELSE 
   SET @strSql = @strSql +' order by ID '
		                                             
print @strSql                      
              
exec (@strSql)  

SELECT * FROM #tempForm

DROP TABLE #tempForm

drop table #tempColorScheme

DROP TABLE #tempTable
--created by anoop jayaraj                                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getdomaindetails')
DROP PROCEDURE usp_getdomaindetails
GO
CREATE PROCEDURE [dbo].[USP_GetDomainDetails]                                          
@numDomainID as numeric(9)=0                                          
as                                          
select                                           
vcDomainName,                                           
isnull(vcDomainDesc,'') vcDomainDesc,                                           
isnull(bitExchangeIntegration,0) bitExchangeIntegration,                                           
isnull(bitAccessExchange,0) bitAccessExchange,                                           
isnull(vcExchUserName,'') vcExchUserName,                                           
isnull(vcExchPassword,'') vcExchPassword,                                           
isnull(vcExchPath,'') vcExchPath,                                           
isnull(vcExchDomain,'') vcExchDomain,                                         
tintCustomPagingRows,                                         
vcDateFormat,                                         
numDefCountry,                                         
tintComposeWindow,                                         
sintStartDate,                                         
sintNoofDaysInterval,                                         
tintAssignToCriteria,                                         
bitIntmedPage,                                         
tintFiscalStartMonth,                                    
--isnull(bitDeferredIncome,0) as bitDeferredIncome,                                  
isnull(tintPayPeriod,0) as tintPayPeriod,                                
isnull(numCurrencyID,0) as numCurrencyID,                            
isnull(vcCurrency,'') as vcCurrency,                      
isnull(charUnitSystem,'') as charUnitSystem ,                    
isnull(vcPortalLogo,'') as vcPortalLogo,                  
isnull(tintChrForComSearch,2) as tintChrForComSearch,                
isnull(tintChrForItemSearch,'') as tintChrForItemSearch,            
isnull(intPaymentGateWay,0) as intPaymentGateWay  ,        
 case when bitPSMTPServer= 1 then vcPSMTPServer else '' end as vcPSMTPServer  ,              
 case when bitPSMTPServer= 1 then  isnull(numPSMTPPort,0)  else 0 end as numPSMTPPort          
,isnull(bitPSMTPAuth,0) bitPSMTPAuth        
,isnull([vcPSmtpPassword],'')vcPSmtpPassword        
,isnull(bitPSMTPSSL,0) bitPSMTPSSL     
,isnull(bitPSMTPServer,0) bitPSMTPServer  ,isnull(vcPSMTPUserName,'') vcPSMTPUserName,      
--isnull(numDefaultCategory,0) numDefaultCategory,
isnull(numShipCompany,0)   numShipCompany,
isnull(bitAutoPopulateAddress,0) bitAutoPopulateAddress,
isnull(tintPoulateAddressTo,0) tintPoulateAddressTo,
isnull(bitMultiCompany,0) bitMultiCompany,
isnull(bitMultiCurrency,0) bitMultiCurrency,
ISNULL(bitCreateInvoice,0) bitCreateInvoice,
ISNULL([numDefaultSalesBizDocId],0) numDefaultSalesBizDocId,
ISNULL([bitSaveCreditCardInfo],0) bitSaveCreditCardInfo,
ISNULL([intLastViewedRecord],10) intLastViewedRecord,
ISNULL([tintCommissionType],1) tintCommissionType,
ISNULL([tintBaseTax],2) tintBaseTax,
ISNULL([numDefaultPurchaseBizDocId],0) numDefaultPurchaseBizDocId,
ISNULL(bitEmbeddedCost,0) bitEmbeddedCost,
ISNULL(numDefaultSalesOppBizDocId,0) numDefaultSalesOppBizDocId,
ISNULL(tintSessionTimeOut,1) tintSessionTimeOut,
ISNULL(tintDecimalPoints,2) tintDecimalPoints,
ISNULL(bitCustomizePortal,0) bitCustomizePortal,
ISNULL(tintBillToForPO,0) tintBillToForPO,
ISNULL(tintShipToForPO,0) tintShipToForPO,
ISNULL(tintOrganizationSearchCriteria,1) tintOrganizationSearchCriteria,
ISNULL(tintLogin,0) tintLogin,
lower(ISNULL(vcPortalName,'')) vcPortalName,
ISNULL(bitDocumentRepositary,0) bitDocumentRepositary,
D.numDomainID,ISNULL(D.tintComAppliesTo,0) tintComAppliesTo,
ISNULL(vcPSMTPDisplayName,'') vcPSMTPDisplayName,
ISNULL(bitGtoBContact,0) bitGtoBContact,
ISNULL(bitBtoGContact,0) bitBtoGContact,
ISNULL(bitGtoBCalendar,0) bitGtoBCalendar,
ISNULL(bitBtoGCalendar,0) bitBtoGCalendar,
ISNULL(bitExpenseNonInventoryItem,0) bitExpenseNonInventoryItem,
ISNULL(bitInlineEdit,0) bitInlineEdit,
ISNULL(bitRemoveVendorPOValidation,0) bitRemoveVendorPOValidation,
isnull(D.tintBaseTaxOnArea,0) tintBaseTaxOnArea,
ISNULL(D.bitAmountPastDue,0) bitAmountPastDue,
CONVERT(DECIMAL(18,2),ISNULL(D.monAmountPastDue,0)) monAmountPastDue,
ISNULL(bitSearchOrderCustomerHistory,0) bitSearchOrderCustomerHistory,
isnull(D.bitRentalItem,0) as bitRentalItem,
isnull(D.numRentalItemClass,0) as numRentalItemClass,
isnull(D.numRentalHourlyUOM,0) as numRentalHourlyUOM,
isnull(D.numRentalDailyUOM,0) as numRentalDailyUOM,
isnull(D.tintRentalPriceBasedOn,0) as tintRentalPriceBasedOn,
isnull(D.tintCalendarTimeFormat,24) as tintCalendarTimeFormat,
isnull(D.tintDefaultClassType,0) as tintDefaultClassType,
ISNULL(D.tintPriceBookDiscount,0) tintPriceBookDiscount,
ISNULL(D.bitAutoSerialNoAssign,0) as bitAutoSerialNoAssign,
ISNULL(D.bitIsShowBalance,0) AS [bitIsShowBalance],
ISNULL(numIncomeAccID,0) AS [numIncomeAccID],
ISNULL(numCOGSAccID,0) AS [numCOGSAccID],
ISNULL(numAssetAccID,0) AS [numAssetAccID],
ISNULL(IsEnableClassTracking,0) AS [IsEnableClassTracking],
ISNULL(IsEnableProjectTracking,0) AS [IsEnableProjectTracking],
ISNULL(IsEnableCampaignTracking,0) AS [IsEnableCampaignTracking],
ISNULL(IsEnableResourceScheduling,0) AS [IsEnableResourceScheduling],
ISNULL(numShippingServiceItemID,0) AS [numShippingServiceItemID],
ISNULL(D.numSOBizDocStatus,0) AS numSOBizDocStatus,
ISNULL(D.bitAutolinkUnappliedPayment,0) AS bitAutolinkUnappliedPayment,
ISNULL(D.numDiscountServiceItemID,0) AS numDiscountServiceItemID,
ISNULL(D.vcShipToPhoneNo,0) AS vcShipToPhoneNo,
ISNULL(D.vcGAUserEMail,'') AS vcGAUserEMail,
ISNULL(D.vcGAUserPassword,'') AS vcGAUserPassword,
ISNULL(D.vcGAUserProfileId,0) AS vcGAUserProfileId,
ISNULL(D.bitDiscountOnUnitPrice,0) AS bitDiscountOnUnitPrice,
ISNULL(D.intShippingImageWidth,0) AS intShippingImageWidth ,
ISNULL(D.intShippingImageHeight,0) AS intShippingImageHeight,
ISNULL(D.numTotalInsuredValue,0) AS numTotalInsuredValue,
ISNULL(D.numTotalCustomsValue,0) AS numTotalCustomsValue,
ISNULL(D.vcHideTabs,'') AS vcHideTabs,
ISNULL(D.bitUseBizdocAmount,0) AS bitUseBizdocAmount,
ISNULL(D.IsEnableUserLevelClassTracking,0) AS IsEnableUserLevelClassTracking,
ISNULL(D.bitDefaultRateType,0) AS bitDefaultRateType,
ISNULL(D.bitIncludeTaxAndShippingInCommission,0) AS bitIncludeTaxAndShippingInCommission,
/*,ISNULL(D.bitAllowPPVariance,0) AS bitAllowPPVariance*/
ISNULL(D.bitPurchaseTaxCredit,0) AS bitPurchaseTaxCredit,
ISNULL(D.bitLandedCost,0) AS bitLandedCost,
ISNULL(D.vcLanedCostDefault,'') AS vcLanedCostDefault
from Domain D  
left join eCommerceDTL eComm  
on eComm.numDomainID=D.numDomainID  
where (D.numDomainID=@numDomainID OR @numDomainID=-1)


GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetDropDownValue')
DROP PROCEDURE USP_GetDropDownValue
GO
Create PROCEDURE [dbo].[USP_GetDropDownValue]     
    @numDomainID numeric(18, 0),
    @numListID numeric(18, 0),
    @vcListItemType AS VARCHAR(5),
    @vcDbColumnName VARCHAR(50)
as                 

CREATE TABLE #temp(numID VARCHAR(500),vcData VARCHAR(max))

			 IF @vcListItemType='LI' OR @vcListItemType='T'                                                    
				BEGIN  
					INSERT INTO #temp                                                   
					SELECT Ld.numListItemID,vcData from ListDetails LD          
					left join listorder LO on Ld.numListItemID= LO.numListItemID  and lo.numDomainId = @numDomainID
					where (Ld.numDomainID=@numDomainID or Ld.constFlag=1) and Ld.numListID=@numListID order by intSortOrder
				end  
 			 ELSE IF @vcListItemType='C'                                                     
				begin    
		   		    INSERT INTO #temp                                                   
	  			    SELECT  numCampaignID,vcCampaignName + CASE bitIsOnline WHEN 1 THEN ' (Online)' ELSE ' (Offline)' END AS vcCampaignName
					FROM CampaignMaster WHERE numDomainID = @numDomainID
				end  
			 ELSE IF @vcListItemType='AG'                                                     
				begin      
		   		    INSERT INTO #temp                                                   
					SELECT numGrpID, vcGrpName FROM groups       
					ORDER BY vcGrpName                                               
				end   
			else if @vcListItemType='DC'                                                     
				begin    
		   		    INSERT INTO #temp                                                   
					select numECampaignID,vcECampName from ECampaign where numDomainID= @numDomainID                                                       
				end 
			else if @vcListItemType='U' OR @vcDbColumnName='numManagerID'
			    BEGIN
		   		     INSERT INTO #temp                                                   
			    	 SELECT A.numContactID,ISNULL(A.vcFirstName,'') +' '+ ISNULL(A.vcLastName,'') as vcUserName          
					 from UserMaster UM join AdditionalContactsInformation A        
					 on UM.numUserDetailId=A.numContactID          
					 where UM.numDomainID=@numDomainID and UM.numDomainID=A.numDomainID  ORDER BY A.vcFirstName,A.vcLastName 
				END
			else if @vcListItemType='S'
			    BEGIN
		   		     INSERT INTO #temp                                                   
			    	 SELECT S.numStateID,S.vcState          
					 from State S      
					 where S.numDomainID=@numDomainID ORDER BY S.vcState
				END
			ELSE IF @vcListItemType = 'OC' 
				BEGIN
					INSERT INTO #temp   
					SELECT DISTINCT C.numCurrencyID,C.vcCurrencyDesc FROM dbo.Currency C WHERE C.numDomainID=@numDomainID
				END
			ELSE IF @vcListItemType = 'UOM' 
				BEGIN
					INSERT INTO #temp   
					SELECT  numUOMId AS numItemID,vcUnitName AS vcItemName FROM UOM WHERE numDomainID = @numDomainID
				END	
			ELSE IF @vcListItemType = 'IG' 
				BEGIN
					INSERT INTO #temp   
					SELECT numItemGroupID,vcItemGroup FROM dbo.ItemGroups WHERE numDomainID=@numDomainID   	
				END
			ELSE IF @vcListItemType = 'SYS'
				BEGIN
					INSERT INTO #temp
					SELECT 0 AS numItemID,'Lead' AS vcItemName
					UNION ALL
					SELECT 1 AS numItemID,'Prospect' AS vcItemName
					UNION ALL
					SELECT 2 AS numItemID,'Account' AS vcItemName
				END
			ELSE IF @vcListItemType = 'PP' 
				BEGIN
					INSERT INTO #temp 
					SELECT 'P','Inventory Item'
					UNION 
					SELECT 'N','Non-Inventory Item'
					UNION 
					SELECT 'S','Service'
				END

				--<asp:ListItem Value="0" Selected="True">0</asp:ListItem>
    --                                <asp:ListItem Value="25" Selected="True">25%</asp:ListItem>
    --                                <asp:ListItem Value="50">50%</asp:ListItem>
    --                                <asp:ListItem Value="75">75%</asp:ListItem>
    --                                <asp:ListItem Value="100">100%</asp:ListItem>
				ELSE IF @vcListItemType = 'SP' --Progress Percentage
				BEGIN
					INSERT INTO #temp 
					SELECT '0','0%'
					UNION 
					SELECT '25','25%'
					UNION 
					SELECT '50','50%'
					UNION 
					SELECT '75','75%'
					UNION 
					SELECT '100','100%'
				END
				ELSE IF @vcListItemType = 'ST' --Stage Details
				BEGIN
					INSERT INTO #temp 
					SELECT vcStageName ,vcStageName  from StagePercentageDetails where numDomainId=@numDomainID and slp_id=@numListID  AND ISNULL(numProjectID,0)= 0 AND ISNULL(numOppID,0)= 0
				END
		    ELSE IF @vcDbColumnName='charSex'
				BEGIN
					 INSERT INTO #temp
					 SELECT 'M','Male' UNION
					 SELECT 'F','Female'
				 END
			ELSE IF @vcDbColumnName='tintOppStatus'
			  BEGIN
					 INSERT INTO #temp
					 SELECT 1,'Deal Won' UNION
					 SELECT 2,'Deal Lost'
			  END
			ELSE IF @vcDbColumnName='tintOppType'
			  BEGIN
					 INSERT INTO #temp
					 SELECT 1,'Sales Order' UNION
					 SELECT 2,'Purchase Order' UNION
					 SELECT 3,'Sales Opportunity' UNION
					 SELECT 4,'Purchase Opportunity' 
			  END
			 
--			Else                                                     
--				BEGIN                                                     
--					select @listName=vcData from ListDetails WHERE numListItemID=@numlistID
--				end 	

 INSERT INTO #temp  SELECT 0,'--None--'				
        
		SELECT * FROM #temp 
		DROP TABLE #temp	
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetGeneralLedger_Virtual')
DROP PROCEDURE USP_GetGeneralLedger_Virtual
GO
CREATE PROCEDURE [dbo].[USP_GetGeneralLedger_Virtual]    
(
      @numDomainID INT,
      @vcAccountId VARCHAR(4000),
      @dtFromDate DATETIME,
      @dtToDate DATETIME,
      @vcTranType VARCHAR(50) = '',
      @varDescription VARCHAR(50) = '',
      @CompanyName VARCHAR(50) = '',
      @vcBizPayment VARCHAR(50) = '',
      @vcCheqNo VARCHAR(50) = '',
      @vcBizDocID VARCHAR(50) = '',
      @vcTranRef VARCHAR(50) = '',
      @vcTranDesc VARCHAR(50) = '',
	  @numDivisionID NUMERIC(9)=0,
      @ClientTimeZoneOffset INT,  --Added by Chintan to enable calculation of date according to client machine
      @charReconFilter CHAR(1)='',
	  @tintMode AS TINYINT=0,
      @numItemID AS NUMERIC(9)=0,
      @CurrentPage INT=0,
	  @PageSize INT=0
    )
WITH Recompile -- Added by Chandan 26 Feb 2013 to avoid parameter sniffing
AS 
    BEGIN
    
--SET @dtFromDate = DateAdd(minute, @ClientTimeZoneOffset, @dtFromDate);
--SET @dtToDate = DateAdd(minute, @ClientTimeZoneOffset, @dtToDate); 
PRINT @dtFromDate
PRINT @dtToDate
	 
DECLARE @first_id NUMERIC, @startRow int
DECLARE @CurrRecord as INT

if @tintMode = 0
BEGIN
SELECT  COA1.numAccountId INTO #Temp1 /*,COA1.vcAccountCode ,COA2.vcAccountCode*/
		FROM    dbo.Chart_Of_Accounts COA1
		WHERE   COA1.numAccountId IN(SELECT  cast(ID AS NUMERIC(9)) FROM dbo.SplitIDs(@vcAccountId, ','))
				AND COA1.numDomainId = @numDomainID
	SELECT @vcAccountId = ISNULL( STUFF((SELECT ',' + CAST(numAccountID AS VARCHAR(10)) FROM #Temp1 FOR XML PATH('')),1, 1, '') , '') 
    
    	SELECT  @numDomainID AS numDomainID,* FROM dbo.fn_GetOpeningBalance(@vcAccountID,@numDomainID,@dtFromDate,@ClientTimeZoneOffset)
		--SET @dtFromDate = DateAdd(minute, @ClientTimeZoneOffset, @dtFromDate);
		--SET @dtToDate = DateAdd(minute, @ClientTimeZoneOffset, @dtToDate);

SELECT TOP 1
dbo.General_Journal_Details.numDomainId,
 dbo.Chart_Of_Accounts.numAccountId , 
dbo.General_Journal_Header.numJournal_Id,
dbo.[FormatedDateFromDate](dbo.General_Journal_Header.datEntry_Date,dbo.General_Journal_Details.numDomainID) Date,
dbo.General_Journal_Header.varDescription, 
dbo.CompanyInfo.vcCompanyName AS CompanyName,
dbo.General_Journal_Details.numTransactionId,

ISNULL(ISNULL(ISNULL(dbo.VIEW_BIZPAYMENT.Narration,dbo.General_Journal_Header.varDescription),'') + ' ' + 
(CASE WHEN isnull(dbo.General_Journal_Header.numReturnID, 0) <> 0 THEN ' Chek No: ' + isnull( CAST(CH.numCheckNo AS VARCHAR(50)),'No-Cheq') 
ELSE  dbo.VIEW_JOURNALCHEQ.Narration 
END),'') as Narration,

ISNULL(dbo.General_Journal_Details.vcReference,'') AS TranRef, 
ISNULL(dbo.General_Journal_Details.varDescription,'') AS TranDesc, 
CONVERT(VARCHAR(20),ISNULL(General_Journal_Details.numDebitAmt,0)) numDebitAmt,
CONVERT(VARCHAR(20),ISNULL(General_Journal_Details.numCreditAmt,0)) numCreditAmt,
dbo.Chart_Of_Accounts.vcAccountName,
'' as balance,
CASE 
WHEN isnull(dbo.General_Journal_Header.numCheckHeaderID, 0) <> 0 THEN + 'Checks' 
WHEN isnull(dbo.General_Journal_Header.numDepositId, 0) <> 0 AND DM.tintDepositePage = 1 THEN 'Deposit' 
WHEN isnull(dbo.General_Journal_Header.numDepositId, 0) <> 0 AND DM.tintDepositePage = 2 THEN 'Receved Pmt' 
WHEN isnull(dbo.General_Journal_Header.numOppId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numOppBizDocsId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numBizDocsPaymentDetId, 0) = 0 AND 
dbo.OpportunityMaster.tintOppType = 1 THEN 'BizDocs Invoice' 
WHEN isnull(dbo.General_Journal_Header.numOppId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numOppBizDocsId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numBizDocsPaymentDetId, 0) = 0 AND 
dbo.OpportunityMaster.tintOppType = 2 THEN 'BizDocs Purchase' 
WHEN isnull(dbo.General_Journal_Header.numCategoryHDRID, 0) <> 0 THEN 'Time And Expenses' 
WHEN ISNULL(General_Journal_Header.numBillId,0) <>0 AND isnull(BH.bitLandedCost,0)=1 then 'Landed Cost' 
WHEN isnull(dbo.General_Journal_Header.numBillID, 0) <> 0 THEN 'Bill'
WHEN isnull(dbo.General_Journal_Header.numBillPaymentID, 0) <> 0 THEN 'Pay Bill'
WHEN isnull(dbo.General_Journal_Header.numReturnID, 0) <> 0 THEN 
																CASE 
																	WHEN RH.tintReturnType=1 AND RH.tintReceiveType=1 THEN 'Sales Return Refund'
																	WHEN RH.tintReturnType=1 AND RH.tintReceiveType=2 THEN 'Sales Return Credit Memo'
																	WHEN RH.tintReturnType=2 AND RH.tintReceiveType=2 THEN 'Purchase Return Credit Memo'
																	WHEN RH.tintReturnType=3 AND RH.tintReceiveType=2 THEN 'Credit Memo'
																	WHEN RH.tintReturnType=4 AND RH.tintReceiveType=1 THEN 'Refund'
																END 
WHEN isnull(dbo.General_Journal_Header.numOppId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numOppBizDocsId, 0) = 0 THEN 'PO Fulfillment'
WHEN dbo.General_Journal_Header.numJournal_Id <> 0 THEN 'Journal' 
END AS TransactionType,

ISNULL(dbo.General_Journal_Details.bitCleared,0) AS bitCleared,
ISNULL(dbo.General_Journal_Details.bitReconcile,0) AS bitReconcile,
ISNULL(dbo.General_Journal_Details.numItemID,0) AS numItemID,
Case When isnull(BH.bitLandedCost,0)=1 then  isnull(BH.numOppId,0) else 0 end AS numLandedCostOppId
FROM dbo.General_Journal_Header INNER JOIN
dbo.General_Journal_Details ON dbo.General_Journal_Header.numJournal_Id = dbo.General_Journal_Details.numJournalId INNER JOIN
dbo.Chart_Of_Accounts ON dbo.General_Journal_Details.numChartAcntId = dbo.Chart_Of_Accounts.numAccountId LEFT OUTER JOIN
dbo.TimeAndExpense ON dbo.General_Journal_Header.numCategoryHDRID = dbo.TimeAndExpense.numCategoryHDRID 
LEFT OUTER JOIN dbo.DivisionMaster ON dbo.General_Journal_Details.numCustomerId = dbo.DivisionMaster.numDivisionID and 
dbo.General_Journal_Details.numDomainID = dbo.DivisionMaster.numDomainID
LEFT OUTER JOIN dbo.CompanyInfo ON dbo.DivisionMaster.numCompanyID = dbo.CompanyInfo.numCompanyId 
LEFT OUTER JOIN dbo.OpportunityMaster ON dbo.General_Journal_Header.numOppId = dbo.OpportunityMaster.numOppId LEFT OUTER JOIN
dbo.OpportunityBizDocs ON dbo.General_Journal_Header.numOppBizDocsId = dbo.OpportunityBizDocs.numOppBizDocsId LEFT OUTER JOIN
dbo.VIEW_JOURNALCHEQ ON (dbo.General_Journal_Header.numCheckHeaderID = dbo.VIEW_JOURNALCHEQ.numCheckHeaderID 
or (dbo.General_Journal_Header.numBillPaymentID=dbo.VIEW_JOURNALCHEQ.numReferenceID and dbo.VIEW_JOURNALCHEQ.tintReferenceType=8))LEFT OUTER JOIN
dbo.VIEW_BIZPAYMENT ON dbo.General_Journal_Header.numBizDocsPaymentDetId = dbo.VIEW_BIZPAYMENT.numBizDocsPaymentDetId LEFT OUTER JOIN
dbo.DepositMaster DM ON DM.numDepositId = dbo.General_Journal_Header.numDepositId  LEFT OUTER JOIN
dbo.ReturnHeader RH ON RH.numReturnHeaderID = dbo.General_Journal_Header.numReturnID LEFT OUTER JOIN
dbo.CheckHeader CH ON CH.numReferenceID=RH.numReturnHeaderID AND CH.tintReferenceType=10
LEFT OUTER JOIN BillHeader BH  ON ISNULL(General_Journal_Header.numBillId,0) = BH.numBillId

WHERE   dbo.General_Journal_Header.datEntry_Date BETWEEN  @dtFromDate AND @dtToDate -- Removed Tmezone bug id 1029 
AND dbo.General_Journal_Header.numDomainId = @numDomainID 
 AND (dbo.DivisionMaster.numDivisionID = @numDivisionID OR @numDivisionID = 0)
AND (bitCleared = CASE @charReconFilter WHEN 'C' THEN 1 ELSE 0 END  OR RTRIM(@charReconFilter)='A')
AND (bitReconcile = CASE @charReconFilter WHEN 'R' THEN 1 ELSE 0 END  OR RTRIM(@charReconFilter)='A') 
AND (dbo.Chart_Of_Accounts.numAccountId IN (SELECT numAccountId FROM #Temp1))
--ORDER BY dbo.General_Journal_Header.numEntryDateSortOrder asc
--ORDER BY dbo.Chart_Of_Accounts.numAccountId ASC,  dbo.General_Journal_Header.datEntry_Date ASC, dbo.General_Journal_Details.numTransactionId asc 
END
ELSE IF @tintMode = 1
BEGIN

SELECT  @numDomainID AS numDomainID,* FROM dbo.fn_GetOpeningBalance(@vcAccountID,@numDomainID,@dtFromDate,@ClientTimeZoneOffset)
--SET @dtFromDate = DateAdd(minute, @ClientTimeZoneOffset, @dtFromDate);
--SET @dtToDate = DateAdd(minute, @ClientTimeZoneOffset, @dtToDate);

SET @CurrRecord = ((@CurrentPage - 1) * @PageSize) + 1
PRINT @CurrRecord
SET ROWCOUNT @CurrRecord

SELECT @first_id = dbo.General_Journal_Details.numEntryDateSortOrder1
FROM dbo.General_Journal_Header 
INNER JOIN dbo.General_Journal_Details ON dbo.General_Journal_Header.numJournal_Id = dbo.General_Journal_Details.numJournalId   
INNER JOIN dbo.Chart_Of_Accounts ON dbo.General_Journal_Details.numChartAcntId = dbo.Chart_Of_Accounts.numAccountId 
LEFT OUTER JOIN dbo.TimeAndExpense ON dbo.General_Journal_Header.numCategoryHDRID = dbo.TimeAndExpense.numCategoryHDRID -- LEFT OUTER JOIN
WHERE dbo.General_Journal_Header.datEntry_Date BETWEEN  @dtFromDate AND @dtToDate -- Removed Tmezone bug id 1029 
AND dbo.General_Journal_Header.numDomainId = @numDomainID 
AND dbo.General_Journal_Details.numDomainId = @numDomainID 
AND dbo.Chart_Of_Accounts.numDomainId = @numDomainID 
AND (bitCleared = CASE @charReconFilter WHEN 'C' THEN 1 ELSE 0 END  OR RTRIM(@charReconFilter)='A')
AND (bitReconcile = CASE @charReconFilter WHEN 'R' THEN 1 ELSE 0 END  OR RTRIM(@charReconFilter)='A') 
--AND (dbo.Chart_Of_Accounts.numAccountId =@vcAccountId)
AND (dbo.Chart_Of_Accounts.numAccountId= @vcAccountId)
AND (General_Journal_Details.numItemID = @numItemID OR @numItemID = 0)
AND dbo.General_Journal_Details.numEntryDateSortOrder1 > 0
--ORDER BY dbo.Chart_Of_Accounts.numAccountId ASC, dbo.General_Journal_Details.numTransactionId asc 
ORDER BY dbo.General_Journal_Details.numEntryDateSortOrder1 asc

PRINT @first_id
PRINT ROWCOUNT_BIG()
SET ROWCOUNT @PageSize

SELECT dbo.General_Journal_Details.numDomainId, dbo.Chart_Of_Accounts.numAccountId , dbo.General_Journal_Header.numJournal_Id,
dbo.[FormatedDateFromDate](dbo.General_Journal_Header.datEntry_Date,dbo.General_Journal_Details.numDomainID) Date,
dbo.General_Journal_Header.varDescription, 
dbo.CompanyInfo.vcCompanyName AS CompanyName,
dbo.General_Journal_Details.numTransactionId,
ISNULL(ISNULL(ISNULL(dbo.VIEW_BIZPAYMENT.Narration,dbo.General_Journal_Header.varDescription),'') + ' ' + 
(CASE WHEN isnull(dbo.General_Journal_Header.numReturnID, 0) <> 0 THEN ' Chek No: ' + isnull( CAST(CH.numCheckNo AS VARCHAR(50)),'No-Cheq') 
ELSE  dbo.VIEW_JOURNALCHEQ.Narration 
END),'') as Narration,
ISNULL(dbo.General_Journal_Details.vcReference,'') AS TranRef, 
ISNULL(dbo.General_Journal_Details.varDescription,'') AS TranDesc, 
CONVERT(VARCHAR(20),ISNULL(General_Journal_Details.numDebitAmt,0)) numDebitAmt,
CONVERT(VARCHAR(20),ISNULL(General_Journal_Details.numCreditAmt,0)) numCreditAmt,
dbo.Chart_Of_Accounts.vcAccountName,
'' as balance,
CASE 
WHEN isnull(dbo.General_Journal_Header.numCheckHeaderID, 0) <> 0 THEN + 'Checks' 
WHEN isnull(dbo.General_Journal_Header.numDepositId, 0) <> 0 AND DM.tintDepositePage = 1 THEN 'Deposit' 
WHEN isnull(dbo.General_Journal_Header.numDepositId, 0) <> 0 AND DM.tintDepositePage = 2 THEN 'Receved Pmt' 
WHEN isnull(dbo.General_Journal_Header.numOppId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numOppBizDocsId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numBizDocsPaymentDetId, 0) = 0 AND 
dbo.OpportunityMaster.tintOppType = 1 THEN 'BizDocs Invoice' 
WHEN isnull(dbo.General_Journal_Header.numOppId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numOppBizDocsId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numBizDocsPaymentDetId, 0) = 0 AND 
dbo.OpportunityMaster.tintOppType = 2 THEN 'BizDocs Purchase' 
WHEN isnull(dbo.General_Journal_Header.numCategoryHDRID, 0) <> 0 THEN 'Time And Expenses' 
WHEN ISNULL(General_Journal_Header.numBillId,0) <>0 AND isnull(BH.bitLandedCost,0)=1 then 'Landed Cost' 
WHEN isnull(dbo.General_Journal_Header.numBillID, 0) <> 0 THEN 'Bill'
WHEN isnull(dbo.General_Journal_Header.numBillPaymentID, 0) <> 0 THEN 'Pay Bill'
WHEN isnull(dbo.General_Journal_Header.numReturnID, 0) <> 0 THEN 
																CASE 
																	WHEN RH.tintReturnType=1 AND RH.tintReceiveType=1 THEN 'Sales Return Refund'
																	WHEN RH.tintReturnType=1 AND RH.tintReceiveType=2 THEN 'Sales Return Credit Memo'
																	WHEN RH.tintReturnType=2 AND RH.tintReceiveType=2 THEN 'Purchase Return Credit Memo'
																	WHEN RH.tintReturnType=3 AND RH.tintReceiveType=2 THEN 'Credit Memo'
																	WHEN RH.tintReturnType=4 AND RH.tintReceiveType=1 THEN 'Refund'
																END 
WHEN isnull(dbo.General_Journal_Header.numOppId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numOppBizDocsId, 0) = 0 THEN 'PO Fulfillment'
WHEN dbo.General_Journal_Header.numJournal_Id <> 0 THEN 'Journal' 
END AS TransactionType,
ISNULL(dbo.General_Journal_Details.bitCleared,0) AS bitCleared,
ISNULL(dbo.General_Journal_Details.bitReconcile,0) AS bitReconcile,
ISNULL(dbo.General_Journal_Details.numItemID,0) AS numItemID,
dbo.General_Journal_Details.numEntryDateSortOrder1 AS numEntryDateSortOrder,
Case When isnull(BH.bitLandedCost,0)=1 then  isnull(BH.numOppId,0) else 0 end AS numLandedCostOppId
FROM dbo.General_Journal_Header INNER JOIN
dbo.General_Journal_Details ON dbo.General_Journal_Header.numJournal_Id = dbo.General_Journal_Details.numJournalId INNER JOIN
dbo.Chart_Of_Accounts ON dbo.General_Journal_Details.numChartAcntId = dbo.Chart_Of_Accounts.numAccountId LEFT OUTER JOIN
dbo.TimeAndExpense ON dbo.General_Journal_Header.numCategoryHDRID = dbo.TimeAndExpense.numCategoryHDRID 
LEFT OUTER JOIN dbo.DivisionMaster ON dbo.General_Journal_Details.numCustomerId = dbo.DivisionMaster.numDivisionID and 
dbo.General_Journal_Details.numDomainID = dbo.DivisionMaster.numDomainID
LEFT OUTER JOIN dbo.CompanyInfo ON dbo.DivisionMaster.numCompanyID = dbo.CompanyInfo.numCompanyId 
LEFT OUTER JOIN dbo.OpportunityMaster ON dbo.General_Journal_Header.numOppId = dbo.OpportunityMaster.numOppId LEFT OUTER JOIN
dbo.OpportunityBizDocs ON dbo.General_Journal_Header.numOppBizDocsId = dbo.OpportunityBizDocs.numOppBizDocsId LEFT OUTER JOIN
dbo.VIEW_JOURNALCHEQ ON (dbo.General_Journal_Header.numCheckHeaderID = dbo.VIEW_JOURNALCHEQ.numCheckHeaderID 
or (dbo.General_Journal_Header.numBillPaymentID=dbo.VIEW_JOURNALCHEQ.numReferenceID and dbo.VIEW_JOURNALCHEQ.tintReferenceType=8))LEFT OUTER JOIN
dbo.VIEW_BIZPAYMENT ON dbo.General_Journal_Header.numBizDocsPaymentDetId = dbo.VIEW_BIZPAYMENT.numBizDocsPaymentDetId LEFT OUTER JOIN
dbo.DepositMaster DM ON DM.numDepositId = dbo.General_Journal_Header.numDepositId  LEFT OUTER JOIN
dbo.ReturnHeader RH ON RH.numReturnHeaderID = dbo.General_Journal_Header.numReturnID LEFT OUTER JOIN
dbo.CheckHeader CH ON CH.numReferenceID=RH.numReturnHeaderID AND CH.tintReferenceType=10
LEFT OUTER JOIN BillHeader BH  ON ISNULL(General_Journal_Header.numBillId,0) = BH.numBillId

WHERE   dbo.General_Journal_Header.datEntry_Date BETWEEN  @dtFromDate AND @dtToDate -- Removed Tmezone bug id 1029 
AND dbo.General_Journal_Header.numDomainId = @numDomainID 
AND (dbo.DivisionMaster.numDivisionID = @numDivisionID OR @numDivisionID = 0)
AND (bitCleared = CASE @charReconFilter WHEN 'C' THEN 1 ELSE 0 END  OR RTRIM(@charReconFilter)='A')
AND (bitReconcile = CASE @charReconFilter WHEN 'R' THEN 1 ELSE 0 END  OR RTRIM(@charReconFilter)='A') 
--AND (dbo.Chart_Of_Accounts.numAccountId =@vcAccountId)
--AND (dbo.Chart_Of_Accounts. IN ( SELECT numAccountId FROM #Temp) OR  @tintMode=1)
AND (dbo.Chart_Of_Accounts.numAccountId= @vcAccountId)
AND (General_Journal_Details.numItemID = @numItemID OR @numItemID = 0)
   AND dbo.General_Journal_Details.numEntryDateSortOrder1  >= @first_id              
ORDER BY dbo.General_Journal_Details.numEntryDateSortOrder1 asc
--AND dbo.General_Journal_Details.numTransactionId >= @first_id
--ORDER BY dbo.Chart_Of_Accounts.numAccountId ASC,   dbo.General_Journal_Details.numTransactionId asc 
SET ROWCOUNT 0

END
	
IF object_id('TEMPDB.DBO.#Temp1') IS NOT NULL drop table #Temp1	
END
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetGLTransactionDetailById')
DROP PROCEDURE USP_GetGLTransactionDetailById
GO
CREATE PROCEDURE [dbo].[USP_GetGLTransactionDetailById]    
(
      @numDomainID INT,
	  @numTransactionID NUMERIC(9) 
)
WITH Recompile -- Added by Chandan 26 Feb 2013 to avoid parameter sniffing
AS 
    BEGIN

SELECT dbo.General_Journal_Details.numDomainId, dbo.Chart_Of_Accounts.numAccountId , dbo.General_Journal_Header.numJournal_Id as JournalId,
dbo.[FormatedDateFromDate](dbo.General_Journal_Header.datEntry_Date,dbo.General_Journal_Details.numDomainID) Date,
dbo.General_Journal_Header.varDescription, 
dbo.CompanyInfo.vcCompanyName AS CompanyName,
dbo.General_Journal_Details.numTransactionId,
ISNULL(dbo.General_Journal_Header.numOppBizDocsId, 0) AS numOppBizDocsId,
ISNULL(dbo.General_Journal_Header.numCheckHeaderID, 0) AS CheckId, 
ISNULL(dbo.General_Journal_Header.numCashCreditCardId, 0) AS CashCreditCardId,
ISNULL( dbo.General_Journal_Header.numOppId, 0) AS numOppId,
 ISNULL(dbo.General_Journal_Header.numDepositId, 0) AS numDepositId, 
ISNULL(dbo.General_Journal_Header.numCategoryHDRID, 0) AS numCategoryHDRID,

ISNULL(dbo.DivisionMaster.numDivisionID,0)numDivisionID,
ISNULL(dbo.General_Journal_Header.numBillID, 0) AS numBillID,
ISNULL(dbo.General_Journal_Header.numBillPaymentID,0) AS numBillPaymentID,
ISNULL(General_Journal_Details.numClassID,0) AS numClassID,
ISNULL(General_Journal_Details.numProjectID,0) AS numProjectID,
isnull(dbo.General_Journal_Header.numReturnID, 0) AS numReturnID,
ISNULL(dbo.General_Journal_Details.numCurrencyID,0) AS numCurrencyID,
ISNULL(ISNULL(dbo.VIEW_BIZPAYMENT.Narration,dbo.General_Journal_Header.varDescription) + ' ' + 
(CASE WHEN isnull(dbo.General_Journal_Header.numReturnID, 0) <> 0 THEN ' Chek No: ' + isnull( CAST(CH.numCheckNo AS VARCHAR(50)),'No-Cheq') 
ELSE  dbo.VIEW_JOURNALCHEQ.Narration 
END),'') as Narration,

dbo.General_Journal_Details.vcReference AS TranRef, 
dbo.General_Journal_Details.varDescription AS TranDesc, 
CONVERT(VARCHAR(20),ISNULL(General_Journal_Details.numDebitAmt,0)) numDebitAmt,
CONVERT(VARCHAR(20),ISNULL(General_Journal_Details.numCreditAmt,0)) numCreditAmt,
dbo.Chart_Of_Accounts.vcAccountName,
'' as balance,
CASE 
WHEN isnull(dbo.General_Journal_Header.numCheckHeaderID, 0) <> 0 THEN + 'Checks' 
WHEN isnull(dbo.General_Journal_Header.numDepositId, 0) <> 0 AND DM.tintDepositePage = 1 THEN 'Deposit' 
WHEN isnull(dbo.General_Journal_Header.numDepositId, 0) <> 0 AND DM.tintDepositePage = 2 THEN 'Receved Pmt' 
WHEN isnull(dbo.General_Journal_Header.numOppId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numOppBizDocsId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numBizDocsPaymentDetId, 0) = 0 AND 
dbo.OpportunityMaster.tintOppType = 1 THEN 'BizDocs Invoice' 
WHEN isnull(dbo.General_Journal_Header.numOppId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numOppBizDocsId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numBizDocsPaymentDetId, 0) = 0 AND 
dbo.OpportunityMaster.tintOppType = 2 THEN 'BizDocs Purchase' 
WHEN isnull(dbo.General_Journal_Header.numCategoryHDRID, 0) <> 0 THEN 'Time And Expenses' 
WHEN isnull(dbo.General_Journal_Header.numBillID, 0) <> 0 THEN 'Bill'
WHEN isnull(dbo.General_Journal_Header.numBillPaymentID, 0) <> 0 THEN 'Pay Bill'
WHEN isnull(dbo.General_Journal_Header.numReturnID, 0) <> 0 THEN 
																CASE 
																	WHEN RH.tintReturnType=1 AND RH.tintReceiveType=1 THEN 'Sales Return Refund'
																	WHEN RH.tintReturnType=1 AND RH.tintReceiveType=2 THEN 'Sales Return Credit Memo'
																	WHEN RH.tintReturnType=2 AND RH.tintReceiveType=2 THEN 'Purchase Return Credit Memo'
																	WHEN RH.tintReturnType=3 AND RH.tintReceiveType=2 THEN 'Credit Memo'
																	WHEN RH.tintReturnType=4 AND RH.tintReceiveType=1 THEN 'Refund'
																END 
WHEN isnull(dbo.General_Journal_Header.numOppId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numOppBizDocsId, 0) = 0 THEN 'PO Fulfillment'
WHEN dbo.General_Journal_Header.numJournal_Id <> 0 THEN 'Journal' 
END AS TransactionType,

ISNULL(dbo.General_Journal_Details.bitCleared,0) AS bitCleared,
ISNULL(dbo.General_Journal_Details.bitReconcile,0) AS bitReconcile,
Case When isnull(BH.bitLandedCost,0)=1 then  isnull(BH.numOppId,0) else 0 end AS numLandedCostOppId
FROM dbo.General_Journal_Header INNER JOIN
dbo.General_Journal_Details ON dbo.General_Journal_Header.numJournal_Id = dbo.General_Journal_Details.numJournalId INNER JOIN
dbo.Chart_Of_Accounts ON dbo.General_Journal_Details.numChartAcntId = dbo.Chart_Of_Accounts.numAccountId LEFT OUTER JOIN
dbo.TimeAndExpense ON dbo.General_Journal_Header.numCategoryHDRID = dbo.TimeAndExpense.numCategoryHDRID 
LEFT OUTER JOIN dbo.DivisionMaster ON dbo.General_Journal_Details.numCustomerId = dbo.DivisionMaster.numDivisionID and 
dbo.General_Journal_Details.numDomainID = dbo.DivisionMaster.numDomainID
LEFT OUTER JOIN dbo.CompanyInfo ON dbo.DivisionMaster.numCompanyID = dbo.CompanyInfo.numCompanyId 
LEFT OUTER JOIN dbo.OpportunityMaster ON dbo.General_Journal_Header.numOppId = dbo.OpportunityMaster.numOppId LEFT OUTER JOIN
dbo.OpportunityBizDocs ON dbo.General_Journal_Header.numOppBizDocsId = dbo.OpportunityBizDocs.numOppBizDocsId LEFT OUTER JOIN
dbo.VIEW_JOURNALCHEQ ON (dbo.General_Journal_Header.numCheckHeaderID = dbo.VIEW_JOURNALCHEQ.numCheckHeaderID 
or (dbo.General_Journal_Header.numBillPaymentID=dbo.VIEW_JOURNALCHEQ.numReferenceID and dbo.VIEW_JOURNALCHEQ.tintReferenceType=8))LEFT OUTER JOIN
dbo.VIEW_BIZPAYMENT ON dbo.General_Journal_Header.numBizDocsPaymentDetId = dbo.VIEW_BIZPAYMENT.numBizDocsPaymentDetId LEFT OUTER JOIN
dbo.DepositMaster DM ON DM.numDepositId = dbo.General_Journal_Header.numDepositId  LEFT OUTER JOIN
dbo.ReturnHeader RH ON RH.numReturnHeaderID = dbo.General_Journal_Header.numReturnID LEFT OUTER JOIN
dbo.CheckHeader CH ON CH.numReferenceID=RH.numReturnHeaderID AND CH.tintReferenceType=10
LEFT OUTER JOIN BillHeader BH  ON ISNULL(General_Journal_Header.numBillId,0) = BH.numBillId

WHERE dbo.General_Journal_Header.numDomainId = @numDomainID 
AND numTransactionId = @numTransactionID 

END
/****** Object:  StoredProcedure [dbo].[USP_GetInventoryStatus]    Script Date: 11/06/2014 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetInventoryStatus')
DROP PROCEDURE USP_GetInventoryStatus
GO
CREATE PROCEDURE USP_GetInventoryStatus
AS
SELECT 1 AS [vcValue],'Not Applicable' AS [vcInventoryStatus]
UNION
SELECT 2 AS [vcValue],'Allocation Cleared' AS [vcInventoryStatus]
UNION
SELECT 3 AS [vcValue],'Back Order' AS [vcInventoryStatus]
UNION
SELECT 4 AS [vcValue],'Ready to Ship' AS [vcInventoryStatus]
/****** Object:  StoredProcedure [dbo].[USP_GetMasterListItems]    Script Date: 07/26/2008 16:17:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--modified by anoop jayaraj        
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetMasterListItemsUsingListType')
DROP PROCEDURE USP_GetMasterListItemsUsingListType
GO
CREATE PROCEDURE [dbo].[USP_GetMasterListItemsUsingListType]          
@ListID as numeric(9)=0,        
@numDomainID as numeric(9)=0    ,
@numListType numeric(9,0)      
as          
          
SELECT Ld.numListItemID, isnull(vcRenamedListName,vcData) as vcData,ISNULL(ld.numListType,0) as numListType FROM listdetails Ld        
left join listorder LO 
on Ld.numListItemID= LO.numListItemID and Lo.numDomainId = @numDomainID 
WHERE Ld.numListID=@ListID and (constFlag=1 or Ld.numDomainID=@numDomainID )
and ISNULL(ld.numListType,0) in (0,@numListType)
order by ISNULL(intSortOrder,LD.sintOrder)

--select * from ListDetails


--SELECT Ld.numListItemID, vcData FROM listdetails Ld        
--left join listorder LO on Ld.numListItemID= LO.numListItemID and Lo.numDomainId = @numDomainID   
--WHERE Ld.numListID=@ListID and (constFlag=1 or Ld.numDomainID=@numDomainID)      
--order by intSortOrder
GO
        
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetOppBizDocsReceivePayment' ) 
    DROP PROCEDURE USP_GetOppBizDocsReceivePayment
GO

CREATE PROCEDURE [dbo].[USP_GetOppBizDocsReceivePayment]
    @numDomainID NUMERIC(18, 0) ,
	@numOppID NUMERIC(18,0),
	@numOppBizDocsID NUMERIC(18,0),
	@numBillID AS NUMERIC(18,0),
	@numReturnHeaderID AS NUMERIC(18,0),
    @tintClientTimeZoneOffset as INT
AS 
		DECLARE @tintOppType AS TINYINT

   IF @numBillID>0
   BEGIN
   		SELECT 1 AS tintRefType,BPH.numBillPaymentID AS numReferenceID,BPH.monPaymentAmount AS monPaymentTotal,BPD.monAmount AS monApplied,/*dbo.fn_GetComapnyName(BH.numDivisionID) AS vcCompanyName,*/
   			dbo.FormatedDateFromDate(BPD.dtAppliedDate,@numDomainId ) AS dtAppliedDate,dbo.FormatedDateFromDate(BPH.[dtPaymentDate],@numDomainId ) AS dtPaymentDate,
   			CASE WHEN ISNULL(numReturnHeaderID,0)>0 THEN 'Purchase Credit Applied' ELSE 'Bill Payment' END vcReference
   		FROM dbo.BillPaymentHeader BPH JOIN dbo.BillPaymentDetails BPD ON BPH.numBillPaymentID = BPD.numBillPaymentID
   		JOIN dbo.BillHeader BH ON BPD.numBillID=BH.numBillID
   		WHERE BH.numBillID=@numBillID ORDER BY BPD.dtAppliedDate
   		
   		SELECT ISNULL(monAmountDue,0) AS monAmountTotal,ISNULL(monAmtPaid,0) AS monAmtPaid FROM BillHeader 
   		WHERE numBillID=@numBillID
   END
   ELSE IF @numReturnHeaderID>0
   BEGIN
		DECLARE @tintReturnType AS TINYINT,@tintReceiveType AS TINYINT 
		
		SELECT @tintReturnType=tintReturnType,@tintReceiveType=ISNULL(tintReceiveType,0) FROM ReturnHeader WHERE numReturnHeaderID=@numReturnHeaderID
		
		IF (@tintReturnType=1 AND @tintReceiveType=2) OR @tintReturnType=3
		BEGIN
			SELECT 2 AS tintRefType,DM.numDepositID AS numReferenceID,DM.monDepositAmount AS monPaymentTotal,DD.monAmountPaid AS monApplied,/*dbo.fn_GetComapnyName(BPH.numDivisionID) AS vcCompanyName,*/
   			dbo.FormatedDateFromDate(DD.dtCreatedDate,@numDomainId ) AS dtAppliedDate,dbo.FormatedDateFromDate(DD.dtCreatedDate,@numDomainId ) AS dtPaymentDate,
   			OM.vcPOppName AS vcReference
   			FROM dbo.DepositMaster DM JOIN dbo.DepositeDetails DD ON DM.numDepositID = DD.numDepositID
   			JOIN dbo.OpportunityMaster OM ON DD.numOppID=OM.numOppID
   			WHERE DM.numReturnHeaderID=@numReturnHeaderID ORDER BY DD.dtCreatedDate	
   			
   			SELECT 2 AS tintRefType,ISNULL(monDepositAmount,0) AS monAmountTotal,ISNULL(monAppliedAmount,0) AS monAmtPaid FROM DepositMaster 
   			WHERE numReturnHeaderID=@numReturnHeaderID
		END
		ELSE IF @tintReturnType=2 AND @tintReceiveType=2
		BEGIN
			SELECT 1 AS tintRefType,BPH.numBillPaymentID AS numReferenceID,BPH.monPaymentAmount AS monPaymentTotal,BPD.monAmount AS monApplied,/*dbo.fn_GetComapnyName(BPH.numDivisionID) AS vcCompanyName,*/
   			dbo.FormatedDateFromDate(BPD.dtAppliedDate,@numDomainId ) AS dtAppliedDate,dbo.FormatedDateFromDate(BPH.[dtPaymentDate],@numDomainId ) AS dtPaymentDate,
   			CASE WHEN(BPD.numBillID>0) THEN  'Bill-' + CONVERT(VARCHAR(10),BH.numBillID) ELSE OM.vcPOppName END AS vcReference
   			FROM dbo.BillPaymentHeader BPH JOIN dbo.BillPaymentDetails BPD ON BPH.numBillPaymentID = BPD.numBillPaymentID
   			LEFT JOIN dbo.OpportunityBizDocs OBD ON BPD.numOppBizDocsID = OBD.numOppBizDocsId
            LEFT JOIN dbo.OpportunityMaster OM ON OBD.numOppId = OM.numOppId
            LEFT JOIN dbo.BillHeader BH ON BH.numBillID = BPD.numBillID
   			WHERE BPH.numReturnHeaderID=@numReturnHeaderID	ORDER BY dtAppliedDate
   			
   			SELECT 1 AS tintRefType,ISNULL(monPaymentAmount,0) AS monAmountTotal,ISNULL(monAppliedAmount,0) AS monAmtPaid FROM BillPaymentHeader 
   			WHERE numReturnHeaderID=@numReturnHeaderID
		END
   END
   ELSE IF @numOppID>0 AND @numOppBizDocsID>0
   BEGIN
		
		SELECT @tintOppType=tintOppType FROM OpportunityMaster WHERE numOppId=@numOppId
		
		IF @tintOppType=1
		BEGIN
		SELECT 2 AS tintRefType,DM.numDepositID AS numReferenceID,DM.monDepositAmount AS monPaymentTotal,DD.monAmountPaid AS monApplied,/*dbo.fn_GetComapnyName(BPH.numDivisionID) AS vcCompanyName,*/
   			dbo.FormatedDateFromDate(DD.dtCreatedDate,@numDomainId ) AS dtAppliedDate,dbo.FormatedDateFromDate(DD.dtCreatedDate,@numDomainId ) AS dtPaymentDate,
   			CASE WHEN tintDepositePage=3 THEN 'Credit Applied' ELSE 'Payment Received' END vcReference
   			FROM dbo.DepositMaster DM JOIN dbo.DepositeDetails DD ON DM.numDepositID = DD.numDepositID
   			WHERE DD.numOppID=@numOppID AND DD.numOppBizDocsID=@numOppBizDocsID ORDER BY DD.dtCreatedDate	
   		END
		ELSE
		BEGIN
			SELECT 1 AS tintRefType,BPH.numBillPaymentID AS numReferenceID,BPH.monPaymentAmount AS monPaymentTotal,BPD.monAmount AS monApplied,/*dbo.fn_GetComapnyName(BPH.numDivisionID) AS vcCompanyName,*/
   			dbo.FormatedDateFromDate(BPD.dtAppliedDate,@numDomainId ) AS dtAppliedDate,dbo.FormatedDateFromDate(BPH.[dtPaymentDate],@numDomainId ) AS dtPaymentDate,
   			CASE WHEN ISNULL(numReturnHeaderID,0)>0 THEN 'Purchase Credit Applied' ELSE 'Bill Payment' END + 
   			CASE WHEN ISNULL(CH.numCheckNo,0)>0 THEN ' (Check # : ' + CAST(CH.numCheckNo AS VARCHAR(18)) + ')'  ELSE '' END AS vcReference
   			FROM dbo.BillPaymentHeader BPH JOIN dbo.BillPaymentDetails BPD ON BPH.numBillPaymentID = BPD.numBillPaymentID
   			LEFT JOIN dbo.CheckHeader CH ON BPH.numBillPaymentID=CH.numReferenceID AND CH.tintReferenceType=8 AND CH.numDomainID=@numDomainID
   			WHERE BPD.numOppBizDocsID=@numOppBizDocsID ORDER BY BPD.dtAppliedDate
   		END
   		 
   		 SELECT tintOppType AS tintRefType,ISNULL(OBD.monDealAmount,0) AS monAmountTotal,ISNULL(OBD.monAmountPaid,0) AS monAmtPaid,numDivisionId 
   		 FROM OpportunityBizDocs OBD JOIN OpportunityMaster OM ON OBD.numOppID=OM.numOppID
   			WHERE OM.numOppID=@numOppID AND numOppBizDocsID=@numOppBizDocsID --AND ISNULL(OBD.bitAuthoritativeBizDocs,0)=1
	END	
	ELSE IF @numOppID>0 
    BEGIN
		SELECT @tintOppType=tintOppType FROM OpportunityMaster WHERE numOppId=@numOppId
		
		IF @tintOppType=1
		BEGIN
		SELECT 2 AS tintRefType,DM.numDepositID AS numReferenceID,DM.monDepositAmount AS monPaymentTotal,DD.monAmountPaid AS monApplied,/*dbo.fn_GetComapnyName(BPH.numDivisionID) AS vcCompanyName,*/
   			dbo.FormatedDateFromDate(DD.dtCreatedDate,@numDomainId ) AS dtAppliedDate,dbo.FormatedDateFromDate(DD.dtCreatedDate,@numDomainId ) AS dtPaymentDate,
   			CASE WHEN tintDepositePage=3 THEN 'Credit Applied' ELSE 'Payment Received' END vcReference
   			FROM dbo.DepositMaster DM JOIN dbo.DepositeDetails DD ON DM.numDepositID = DD.numDepositID
   			WHERE DD.numOppID=@numOppID ORDER BY DD.dtCreatedDate	
   		END
		ELSE
		BEGIN
			SELECT 1 AS tintRefType,BPH.numBillPaymentID AS numReferenceID,BPH.monPaymentAmount AS monPaymentTotal,BPD.monAmount AS monApplied,/*dbo.fn_GetComapnyName(BPH.numDivisionID) AS vcCompanyName,*/
   			dbo.FormatedDateFromDate(BPD.dtAppliedDate,@numDomainId ) AS dtAppliedDate,dbo.FormatedDateFromDate(BPH.[dtPaymentDate],@numDomainId ) AS dtPaymentDate,
   			CASE WHEN ISNULL(numReturnHeaderID,0)>0 THEN 'Purchase Credit Applied' ELSE 'Bill Payment' END + 
   			CASE WHEN ISNULL(CH.numCheckNo,0)>0 THEN ' (Check # : ' + CAST(CH.numCheckNo AS VARCHAR(18)) + ')'  ELSE '' END  AS vcReference
   			FROM dbo.BillPaymentHeader BPH JOIN dbo.BillPaymentDetails BPD ON BPH.numBillPaymentID = BPD.numBillPaymentID
   			LEFT JOIN dbo.CheckHeader CH ON BPH.numBillPaymentID=CH.numReferenceID AND CH.tintReferenceType=8 AND CH.numDomainID=@numDomainID
   			WHERE BPD.numOppBizDocsID IN (SELECT numOppBizDocsID FROM dbo.OpportunityBizDocs WHERE numOppID=@numOppID) ORDER BY BPD.dtAppliedDate
   		END
   		 
   		 SELECT tintOppType AS tintRefType,ISNULL(OBD.monDealAmount,0) AS monAmountTotal,ISNULL(OBD.monAmountPaid,0) AS monAmtPaid,numDivisionId 
   		 FROM OpportunityBizDocs OBD JOIN OpportunityMaster OM ON OBD.numOppID=OM.numOppID
   			WHERE OM.numOppID=@numOppID AND ISNULL(OBD.bitAuthoritativeBizDocs,0)=1
	END			
				
/****** Object:  StoredProcedure [dbo].[USP_GetPageElementAttributes]    Script Date: 08/08/2009 16:15:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--EXEC [USP_GetPageElementAttributes] @numSiteID = 3,@numElementID =4
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetOrderType')
DROP PROCEDURE USP_GetOrderType
GO
-- =============================================
-- Author:		<Author,,Sachin Sadhu>
-- Create date: <Create Date,, 18thJune2014>
-- Description:	<Description,,Get only Order type :Sales /or Purchase>
-- =============================================
Create PROCEDURE [dbo].[USP_GetOrderType]
	-- Add the parameters for the stored procedure here
	@numOppId numeric (18,0)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
    -- Insert statements for procedure here
	SELECT tintOppType from OpportunityMaster where numOppId=@numOppId
END

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetOrganizationTransaction')
DROP PROCEDURE USP_GetOrganizationTransaction
GO
CREATE PROCEDURE [dbo].[USP_GetOrganizationTransaction]
    (
      @numDivisionID NUMERIC(9)=0,
      @numDomainID NUMERIC,
      @CurrentPage INT=0,
	  @PageSize INT=0,
      @TotRecs INT=0  OUTPUT,
      @vcTransactionType AS VARCHAR(500),
      @dtFromDate DATETIME,
      @dtToDate DATETIME,
      @ClientTimeZoneOffset INT
    )
AS 
    BEGIN
		
		SELECT Items INTO #temp FROM dbo.Split(@vcTransactionType,',')

		CREATE TABLE #tempTransaction(intTransactionType INT,vcTransactionType VARCHAR(30),dtCreatedDate VARCHAR(30),numRecordID NUMERIC,
		vcRecordName VARCHAR(100),monTotalAmount MONEY,monPaidAmount MONEY,dtDueDate DATETIME,vcMemo VARCHAR(1000),vcDescription VARCHAR(500),
		vcStatus VARCHAR(100),numRecordID2 NUMERIC,numStatus NUMERIC, vcBizDocId VARCHAR(1000))
		 
		 --Sales Order
		 IF EXISTS (SELECT 1 FROM #temp WHERE Items=1)
		 BEGIN
		 	INSERT INTO #tempTransaction 
		 	SELECT 1,'Sales Order',dbo.FormatedDateFromDate(OM.bintCreatedDate,OM.numDomainId),OM.numOppId,
		 	OM.vcPOppName,
		 	[dbo].[getdealamount](OM.numOppId,Getutcdate(),0),
		 	ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = OM.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0),
		 	NULL,'','',dbo.GetListIemName(numStatus),0,numStatus,
		 	ISNULL(OM.vcOppRefOrderNo,'')
		 	 FROM OpportunityMaster OM where OM.numDomainID = @numDomainID AND OM.numDivisionId=@numDivisionId
                        AND OM.tintOppType = 1  AND OM.tintOppStatus =  1
                        AND ((dateadd(minute,-@ClientTimeZoneOffset,OM.bintCreatedDate) between @dtFromDate And @dtToDate))
		 END
		 
		 --Sales Order Invoices(BizDocs)
		 IF EXISTS (SELECT 1 FROM #temp WHERE Items=2)
		 BEGIN
		 	INSERT INTO #tempTransaction 
		 	SELECT 2,'SO Invoice',dbo.FormatedDateFromDate(OBD.dtCreatedDate,OM.numDomainId),OBD.numOppBizDocsId,
		 	OBD.vcBizDocID,[dbo].[getdealamount](OM.numOppId,Getutcdate(),OBD.numOppBizDocsId),
		 	ISNULL(OBD.monAmountPaid,0),
		 	NULL,'','',dbo.GetListIemName(OBD.numBizDocStatus),OM.numOppId,numBizDocStatus,
		 	ISNULL(vcRefOrderNo,'')
		 	 FROM OpportunityMaster OM INNER JOIN OpportunityBizDocs OBD ON OBD.numOppId = OM.numOppId 
		 	 where OM.numDomainID = @numDomainID AND OM.numDivisionId=@numDivisionId
                        AND OM.tintOppType = 1  AND OM.tintOppStatus =  1 AND OBD.[bitAuthoritativeBizDocs] =1
                        AND ((dateadd(minute,-@ClientTimeZoneOffset,OBD.dtCreatedDate) between @dtFromDate And @dtToDate))
        END
		 
		 --Sales Opportunity
		  IF EXISTS (SELECT 1 FROM #temp WHERE Items=3)
		 BEGIN
		 	INSERT INTO #tempTransaction 
		 	SELECT 3,'Sales Opportunity',dbo.FormatedDateFromDate(OM.bintCreatedDate,OM.numDomainId),OM.numOppId,
		 	OM.vcPOppName,[dbo].[getdealamount](OM.numOppId,Getutcdate(),0),
		 	ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = OM.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0),
		 	NULL,'','',dbo.GetListIemName(numStatus),0,numStatus,''
		 	 FROM OpportunityMaster OM where OM.numDomainID = @numDomainID AND OM.numDivisionId=@numDivisionId
                        AND OM.tintOppType = 1  AND OM.tintOppStatus =  0
                        AND ((dateadd(minute,-@ClientTimeZoneOffset,OM.bintCreatedDate) between @dtFromDate And @dtToDate))
		 END
		 
		 --Purchase Order
		  IF EXISTS (SELECT 1 FROM #temp WHERE Items=4)
		 BEGIN
		 	INSERT INTO #tempTransaction 
		 	SELECT 4,'Purchase Order',dbo.FormatedDateFromDate(OM.bintCreatedDate,OM.numDomainId),OM.numOppId,
		 	OM.vcPOppName,
		 	[dbo].[getdealamount](OM.numOppId,Getutcdate(),0),
		 	ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = OM.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0),
		 	NULL,'','',dbo.GetListIemName(numStatus),0,numStatus,
		 	ISNULL(OM.vcOppRefOrderNo,'')
		 	 FROM OpportunityMaster OM where OM.numDomainID = @numDomainID AND OM.numDivisionId=@numDivisionId
                        AND OM.tintOppType = 2  AND OM.tintOppStatus =  1
                        AND ((dateadd(minute,-@ClientTimeZoneOffset,OM.bintCreatedDate) between @dtFromDate And @dtToDate))
		 END
		 
		 --Purchase Order Bill(BizDocs)
		  IF EXISTS (SELECT 1 FROM #temp WHERE Items=5)
		 BEGIN
		 	INSERT INTO #tempTransaction 
		 	SELECT 5,'PO Bill',dbo.FormatedDateFromDate(OBD.dtCreatedDate,OM.numDomainId),OBD.numOppBizDocsId,
		 	OBD.vcBizDocID,[dbo].[getdealamount](OM.numOppId,Getutcdate(),OBD.numOppBizDocsId),
		 	ISNULL(OBD.monAmountPaid,0),
		 	NULL,'','',dbo.GetListIemName(OBD.numBizDocStatus),OM.numOppId,numBizDocStatus,
		 	ISNULL(vcRefOrderNo,'')
--		 	(SELECT SUBSTRING((SELECT vcBizDocID + ',' FROM OpportunityBizDocs 
--														   WHERE numOppId = OM.numOppId
--														   AND ISNULL(bitAuthoritativeBizDocs,0) = 1 
--										 FOR XML PATH('')), 0, 200000))
		 	 FROM OpportunityMaster OM INNER JOIN OpportunityBizDocs OBD ON OBD.numOppId = OM.numOppId 
		 	 where OM.numDomainID = @numDomainID AND OM.numDivisionId=@numDivisionId
                        AND OM.tintOppType = 2  AND OM.tintOppStatus =  1 AND OBD.[bitAuthoritativeBizDocs] =1
                        AND ((dateadd(minute,-@ClientTimeZoneOffset,OBD.dtCreatedDate) between @dtFromDate And @dtToDate))
		 END
		 
		 --Purchase Opportunity
		  IF EXISTS (SELECT 1 FROM #temp WHERE Items=6)
		 BEGIN
		 	INSERT INTO #tempTransaction 
		 	SELECT 6,'Purchase Opportunity',dbo.FormatedDateFromDate(OM.bintCreatedDate,OM.numDomainId),OM.numOppId,
		 	OM.vcPOppName,[dbo].[getdealamount](OM.numOppId,Getutcdate(),0),
		 	ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = OM.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0),
		 	NULL,'','',dbo.GetListIemName(numStatus),0,numStatus,''
		 	 FROM OpportunityMaster OM where OM.numDomainID = @numDomainID AND OM.numDivisionId=@numDivisionId
                        AND OM.tintOppType = 2  AND OM.tintOppStatus =  0
                        AND ((dateadd(minute,-@ClientTimeZoneOffset,OM.bintCreatedDate) between @dtFromDate And @dtToDate))
		 END
		 
		 --Sales Return
		  IF EXISTS (SELECT 1 FROM #temp WHERE Items=7)
		 BEGIN
		 	INSERT INTO #tempTransaction 
		 	SELECT 7,'Sales Return',dbo.FormatedDateFromDate(RH.dtCreatedDate,RH.numDomainId),RH.numReturnHeaderID,
		 	RH.vcRMA,ISNULL(RH.monAmount,0),
		 	CASE ISNULL(RH.tintReceiveType,0) WHEN 1 THEN ISNULL((SELECT ISNULL(SUM(CH.monAmount),0) FROM dbo.CheckHeader CH WHERE CH.numDomainID=1 AND CH.tintReferenceType=10 AND CH.numReferenceID=RH.numReturnHeaderID AND ISNULL(CH.bitIsPrint,0)=1),0)
		 		 	WHEN 2 THEN ISNULL((SELECT SUM(ISNULL(monAppliedAmount,0)) AS monAmtPaid FROM DepositMaster DM WHERE DM.numDomainID=RH.numDomainID AND DM.numReturnHeaderID=RH.numReturnHeaderID),0)
		 		 	ELSE 0 END,
		 	NULL,'','',dbo.GetListIemName(RH.numReturnStatus),0,RH.numReturnStatus,''
		 	 FROM ReturnHeader RH where RH.numDomainID = @numDomainID AND RH.numDivisionId=@numDivisionId
                        AND RH.tintReturnType = 1
                        AND ((dateadd(minute,-@ClientTimeZoneOffset,RH.dtCreatedDate) between @dtFromDate And @dtToDate))
		 END
		 
		 --Purchase Return
		  IF EXISTS (SELECT 1 FROM #temp WHERE Items=8)
		 BEGIN
		 	INSERT INTO #tempTransaction 
		 	SELECT 8,'Purchase Return',dbo.FormatedDateFromDate(RH.dtCreatedDate,RH.numDomainId),RH.numReturnHeaderID,
		 	RH.vcRMA,ISNULL(RH.monAmount,0),
		 	ISNULL((SELECT SUM(ISNULL(monAppliedAmount,0)) AS monAmtPaid FROM BillPaymentHeader BPH WHERE BPH.numDomainID=RH.numDomainID AND BPH.numReturnHeaderID=RH.numReturnHeaderID),0),
		 	NULL,'','',dbo.GetListIemName(RH.numReturnStatus),0,RH.numReturnStatus,''
		 	 FROM ReturnHeader RH where RH.numDomainID = @numDomainID AND RH.numDivisionId=@numDivisionId
                        AND RH.tintReturnType = 2
                        AND ((dateadd(minute,-@ClientTimeZoneOffset,RH.dtCreatedDate) between @dtFromDate And @dtToDate))

		 END
		 
		 --Credit Memo
		  IF EXISTS (SELECT 1 FROM #temp WHERE Items=9)
		 BEGIN
		 	INSERT INTO #tempTransaction 
		 	SELECT 9,'Credit Memo',dbo.FormatedDateFromDate(RH.dtCreatedDate,RH.numDomainId),RH.numReturnHeaderID,
		 	RH.vcRMA,ISNULL(RH.monAmount,0),
		 	ISNULL((SELECT SUM(ISNULL(monAppliedAmount,0)) AS monAmtPaid FROM DepositMaster DM WHERE DM.numDomainID=RH.numDomainID AND DM.numReturnHeaderID=RH.numReturnHeaderID),0),
		 	NULL,'','',dbo.GetListIemName(RH.numReturnStatus),0,RH.numReturnStatus,''
		 	 FROM ReturnHeader RH where RH.numDomainID = @numDomainID AND RH.numDivisionId=@numDivisionId
                        AND RH.tintReturnType = 3
                        AND ((dateadd(minute,-@ClientTimeZoneOffset,RH.dtCreatedDate) between @dtFromDate And @dtToDate))
		 END
		 
		 --Refund Receipt
		  IF EXISTS (SELECT 1 FROM #temp WHERE Items=10)
		 BEGIN
		 	INSERT INTO #tempTransaction 
		 	SELECT 10,'Refund Receipt',dbo.FormatedDateFromDate(RH.dtCreatedDate,RH.numDomainId),RH.numReturnHeaderID,
		 	RH.vcRMA,ISNULL(RH.monAmount,0),
		 	ISNULL((SELECT ISNULL(SUM(CH.monAmount),0) FROM dbo.CheckHeader CH WHERE CH.numDomainID=1 AND CH.tintReferenceType=10 AND CH.numReferenceID=RH.numReturnHeaderID AND ISNULL(CH.bitIsPrint,0)=1),0),
		 	NULL,'','',dbo.GetListIemName(RH.numReturnStatus),0,RH.numReturnStatus,''
		 	 FROM ReturnHeader RH where RH.numDomainID = @numDomainID AND RH.numDivisionId=@numDivisionId
                        AND RH.tintReturnType = 4
                        AND ((dateadd(minute,-@ClientTimeZoneOffset,RH.dtCreatedDate) between @dtFromDate And @dtToDate))
		 END
		 
		 --Bills
		  IF EXISTS (SELECT 1 FROM #temp WHERE Items=11)
		 BEGIN
		 	INSERT INTO #tempTransaction 
		 	SELECT 11,'Bills',dbo.FormatedDateFromDate(BH.dtCreatedDate,BH.numDomainId),BH.numBillID,
		 	'Bill-' + CAST(BH.numBillID AS VARCHAR(18)),ISNULL(BH.monAmountDue,0),
		 	ISNULL(BH.monAmtPaid,0),
		 	NULL,BH.vcMemo,BH.vcReference,'',0,0,''
		 	 FROM BillHeader BH where BH.numDomainID = @numDomainID AND BH.numDivisionId=@numDivisionId
                        AND ((dateadd(minute,-@ClientTimeZoneOffset,BH.dtCreatedDate) between @dtFromDate And @dtToDate))
						AND ISNULL(BH.[bitLandedCost],0) = 0

		 END
		 
		  --Bills Landed Cost
		  IF EXISTS (SELECT 1 FROM #temp WHERE Items=16)
		 BEGIN
		 	INSERT INTO #tempTransaction 
		 	SELECT 16,'Landed Cost',dbo.FormatedDateFromDate(BH.dtCreatedDate,BH.numDomainId),BH.[numOppId],
		 	'Landed Cost Bill-' + CAST(BH.numBillID AS VARCHAR(18)),ISNULL(BH.monAmountDue,0),
		 	ISNULL(BH.monAmtPaid,0),
		 	NULL,BH.vcMemo,BH.vcReference,'',0,0,''
		 	 FROM BillHeader BH where BH.numDomainID = @numDomainID AND BH.numDivisionId=@numDivisionId
                        AND ((dateadd(minute,-@ClientTimeZoneOffset,BH.dtCreatedDate) between @dtFromDate And @dtToDate))
						AND ISNULL(BH.[bitLandedCost],0) = 1
		 END

		 --UnApplied Bill Payments
		  IF EXISTS (SELECT 1 FROM #temp WHERE Items=12)
		 BEGIN
		 	INSERT INTO #tempTransaction 
		 	SELECT 12,'Unapplied Bill Payments',dbo.FormatedDateFromDate(BPH.dtCreateDate,BPH.numDomainId),BPH.numBillPaymentID,
		 	CASE WHEN ISNULL(numReturnHeaderID,0)>0 THEN 'Return Credit #' + CAST(numReturnHeaderID AS VARCHAR(20)) ELSE 'Unapplied Bill Payment' END
			,ISNULL(BPH.monPaymentAmount,0),
		 	ISNULL(BPH.monAppliedAmount,0),
		 	NULL,'','','',ISNULL(numReturnHeaderID,0),0,''
		 	 FROM BillPaymentHeader BPH where BPH.numDomainID = @numDomainID AND BPH.numDivisionId=@numDivisionId
                        AND ((dateadd(minute,-@ClientTimeZoneOffset,BPH.dtCreateDate) between @dtFromDate And @dtToDate))
                        AND (ISNULL(monPaymentAmount,0) - ISNULL(monAppliedAmount,0)) > 0
		 END
		 
		 --Checks
		  IF EXISTS (SELECT 1 FROM #temp WHERE Items=13)
		 BEGIN
		 	INSERT INTO #tempTransaction 
		 	SELECT 13,'Checks',dbo.FormatedDateFromDate(CH.dtCreatedDate,CH.numDomainId),CH.numCheckHeaderID,
		 	CASE CH.tintReferenceType WHEN 1 THEN 'Checks' 
				  WHEN 8 THEN 'Bill Payment'
				  WHEN 10 THEN 'RMA'
				  WHEN 11 THEN 'Payroll'
				  ELSE 'Check' END + CASE WHEN ISNULL(CH.numCheckNo,0) > 0 THEN ' - #' + CAST(CH.numCheckNo AS VARCHAR(18)) ELSE '' END 
			,ISNULL(CH.monAmount,0),
		 	CASE WHEN ISNULL(bitIsPrint,0)=1 THEN ISNULL(CH.monAmount,0) ELSE 0 END,
		 	NULL,CH.vcMemo,'','',0,0,''
		 	 FROM dbo.CheckHeader CH where CH.numDomainID = @numDomainID AND CH.numDivisionId=@numDivisionId
		 				--AND ch.tintReferenceType=1
                        AND ((dateadd(minute,-@ClientTimeZoneOffset,CH.dtCreatedDate) between @dtFromDate And @dtToDate))

		 END
		 
		 --Deposits
		  IF EXISTS (SELECT 1 FROM #temp WHERE Items=14)
		 BEGIN
		 	INSERT INTO #tempTransaction 
		 	SELECT 14,'Deposits',dbo.FormatedDateFromDate(DM.dtCreationDate,DM.numDomainId),DM.numDepositId,
		 	'Deposit',
		 	ISNULL(DM.monDepositAmount,0),
		 	ISNULL(DM.monAppliedAmount,0),
		 	NULL,DM.vcMemo,DM.vcReference,'',0,tintDepositePage,''
		 	 FROM DepositMaster DM where DM.numDomainID = @numDomainID AND DM.numDivisionId=@numDivisionId
                        AND ((dateadd(minute,-@ClientTimeZoneOffset,DM.dtCreationDate) between @dtFromDate And @dtToDate))
                        AND tintDepositePage IN (1,2)
		 END
		 
		 --Unapplied Payments
		  IF EXISTS (SELECT 1 FROM #temp WHERE Items=15)
		 BEGIN
			INSERT INTO #tempTransaction 
		 	SELECT 15,'Unapplied Payments',dbo.FormatedDateFromDate(DM.dtCreationDate,DM.numDomainId),DM.numDepositId,
		 	CASE WHEN tintDepositePage=3 THEN 'Credit Memo #' + CAST(numReturnHeaderID AS VARCHAR(20)) ELSE 'Unapplied Payment' END,
		 	ISNULL(DM.monDepositAmount,0),
		 	ISNULL(DM.monAppliedAmount,0),
		 	NULL,DM.vcMemo,DM.vcReference,'',ISNULL(numReturnHeaderID,0),tintDepositePage,''
		 	 FROM DepositMaster DM where DM.numDomainID = @numDomainID AND DM.numDivisionId=@numDivisionId
                        AND ((dateadd(minute,-@ClientTimeZoneOffset,DM.dtCreationDate) between @dtFromDate And @dtToDate))
                        AND (ISNULL(monDepositAmount,0) - ISNULL(monAppliedAmount,0)) > 0 AND tintDepositePage IN(2,3)
		 END
		 
		 DECLARE  @firstRec  AS INTEGER
		 DECLARE  @lastRec  AS INTEGER

         SET @firstRec = (@CurrentPage - 1) * @PageSize
		 SET @lastRec = (@CurrentPage * @PageSize + 1)
		 SET @TotRecs = (SELECT COUNT(*) FROM   #tempTransaction)
         
         SELECT * FROM 
         (SELECT *,ROW_NUMBER() OVER(ORDER BY dtCreatedDate) AS RowNumber FROM #tempTransaction) a
          WHERE RowNumber > @firstRec and RowNumber < @lastRec order by intTransactionType
          
		 DROP TABLE #tempTransaction
		 
		 DROP TABLE #temp
    END
/****** Object:  StoredProcedure [dbo].[USP_GetPageElementAttributes]    Script Date: 08/08/2009 16:15:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--EXEC [USP_GetPageElementAttributes] @numSiteID = 3,@numElementID =4
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetPageElementAttributes')
DROP PROCEDURE USP_GetPageElementAttributes
GO
CREATE PROCEDURE [dbo].[USP_GetPageElementAttributes]
               @numSiteID    NUMERIC(9),
               @numElementID NUMERIC(9),
			   @numPageID NUMERIC(9)
AS
  BEGIN
  
  --SELECT PA.[numAttributeID],
  --             PA.numElementID,
  --             PA.[vcAttributeName],
		--	   CASE	WHEN  @numElementID = (SELECT numElementID 
		--				  FROM [dbo].[PageElementMaster] 
		--				  WHERE [vcTagName] = '{#BreadCrumb#}')
		--			THEN (SELECT [vcPageName] 
		--				  FROM [dbo].[SitePages] 
		--				  WHERE [numPageID] = @numPageID)
		--	   WHEN ISNULL(PA.bitEditor,0)=1 THEN [vcHtml] 
		--	   ELSE ISNULL(vcAttributeValue,'')  
		--	   END vcAttributeValue,
		--	   PA.[vcControlType],
		--	   PA.[vcControlValues],ISNULL(PA.bitEditor,0) bitEditor
  --      FROM   [PageElementAttributes] PA
		--LEFT OUTER JOIN PageElementDetail PED ON PED.numAttributeID = PA.numAttributeID
  --      WHERE PA.[numElementID] = @numElementID
  --      AND numSiteID = @numSiteID 
        
        SELECT PA.[numAttributeID],
               PA.numElementID,
               PA.[vcAttributeName],
			   CASE	WHEN  @numElementID = (SELECT numElementID 
						  FROM [dbo].[PageElementMaster] 
						  WHERE [vcTagName] = '{#BreadCrumb#}')
					THEN (SELECT [vcPageName] 
						  FROM [dbo].[SitePages] 
						  WHERE [numPageID] = @numPageID)
			   WHEN ISNULL(PA.bitEditor,0)=1 THEN (SELECT [vcHtml]
                       FROM   [PageElementDetail]
                       WHERE  numSiteID = @numSiteID
                              AND numElementID = @numElementID
                              AND numAttributeID = PA.numAttributeID)
			   ELSE ISNULL((SELECT [vcAttributeValue]
                       FROM   [PageElementDetail]
                       WHERE  numSiteID = @numSiteID
                              AND numElementID = @numElementID
                              AND numAttributeID = PA.numAttributeID),'') 
			   END vcAttributeValue,
			   PA.[vcControlType],
			   PA.[vcControlValues],ISNULL(PA.bitEditor,0) bitEditor
        FROM   [PageElementAttributes] PA
        --           LEFT OUTER JOIN [PageElementDetail] PD
        --             ON PA.[numAttributeID] = PD.[numAttributeID]
        WHERE PA.[numElementID] = @numElementID
      END


set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

--created by anoop jayaraj   
--EXEC [USP_GetPageNavigation] @numModuleID=9,@numDomainID=1

         
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getpagenavigation')
DROP PROCEDURE usp_getpagenavigation
GO
CREATE PROCEDURE [dbo].[USP_GetPageNavigation]            
@numModuleID as numeric(9),      
@numDomainID as numeric(9),
@numGroupID NUMERIC(18, 0)        
as            
if @numModuleID = 14      
begin
           
select  PND.numPageNavID,
        numModuleID,
        numParentID,
        vcPageNavName,
        isnull(vcNavURL, '') as vcNavURL,
        vcImageURL,
        0 intSortOrder
from    PageNavigationDTL PND
        JOIN dbo.TreeNavigationAuthorization TNA ON PND.numPageNavID = TNA.numPageNavID  --AND PND.numTabID = TNA.numTabID       
 WHERE  1 = 1
        AND ( ISNULL(TNA.bitVisible, 0) = 1
              --OR ISNULL(numParentID, 0) = 0
              --OR ISNULL(TNA.bitVisible, 0) = 0
            )
        AND ISNULL(PND.bitVisible, 0) = 1
        AND numModuleID = 14
        AND TNA.numDomainID = @numDomainID
        AND numGroupID = @numGroupID
union
select  1111 numPageNavID,
        14 numModuleID,
        133 numParentID,
        'Regular Documents' as vcPageNavName,
        '../Documents/frmRegularDocList.aspx?Category=0',
        '../images/tf_note.gif' vcImageURL,
        1
union

--select numListItemId,9 numModuleID,1111 numParentID,vcData vcPageNavName,'../Documents/frmDocList.aspx?TabID=1&Category=' + cast(numListItemId as varchar(10)) vcImageURL ,'../images/tf_note.gif',-2 vcImageURL
-- from ListDetails where numListid=29 and constFlag=1 
--UNION
--
--select numListItemId,9 numModuleID,1111 numParentID,vcData vcPageNavName,'../Documents/frmDocList.aspx?TabID=1&Category=' + cast(numListItemId as varchar(10)) vcImageURL ,'../images/tf_note.gif',-2 vcImageURL
-- from ListDetails where numListid=29 and numDomainID=@numDomainID
-- 
SELECT  Ld.numListItemId,
        14 numModuleID,
        1111 numParentID,
        vcData vcPageNavName,
        '../Documents/frmRegularDocList.aspx?Category='
        + cast(Ld.numListItemId as varchar(10)) vcImageURL,
        '../images/tf_note.gif',
        ISNULL(intSortOrder, LD.sintOrder) SortOrder
FROM    listdetails Ld
        left join listorder LO on Ld.numListItemID = LO.numListItemID
                                  and Lo.numDomainId = @numDomainID
WHERE   Ld.numListID = 29
        and ( constFlag = 1
              or Ld.numDomainID = @numDomainID
            )
--order by ISNULL(intSortOrder,LD.sintOrder)
--union
--   
-- SELECT 0 numPageNavID,9 numModuleID,1111 numParentID, vcData vcPageNavName,'../Documents/frmDocList.aspx?TabID=1&Category='+convert(varchar(10), LD.numListItemID) vcNavURL      
-- ,'../images/Text.gif' vcImageURL,0      
-- FROM listdetails Ld   
-- left join listorder LO on Ld.numListItemID= LO.numListItemID  and lo.numDomainId = @numDomainId        
-- WHERE  LD.numListID=28 and (constFlag=1 or  LD.numDomainID=@numDomainID)    
union
SELECT  ld.numListItemID AS numPageNavID,
        14 numModuleID,
        133 numParentID,
        vcData vcPageNavName,
        '../Documents/frmDocList.aspx?Category='
        + convert(varchar(10), LD.numListItemID) vcNavURL,
        '../images/tf_note.gif' vcImageURL,
        -3
FROM    listdetails Ld
        left join listorder LO on Ld.numListItemID = LO.numListItemID
                                  and lo.numDomainId = @numDomainId
        inner join AuthoritativeBizDocs AB on ( LD.numListItemID = AB.numAuthoritativeSales )
WHERE   LD.numListID = 27
        and ( constFlag = 1
              or LD.numDomainID = @numDomainID
            )
        and AB.numDomainID = @numDomainID
union
SELECT  ld.numListItemID AS numPageNavID,
        14 numModuleID,
        133 numParentID,
        vcData vcPageNavName,
        '../Documents/frmDocList.aspx?Category='
        + convert(varchar(10), LD.numListItemID) vcNavURL,
        '../images/tf_note.gif' vcImageURL,
        -3
FROM    listdetails Ld
        left join listorder LO on Ld.numListItemID = LO.numListItemID
                                  and lo.numDomainId = @numDomainId
        inner join AuthoritativeBizDocs AB on ( ld.numListItemID = AB.numAuthoritativePurchase )
WHERE   LD.numListID = 27
        and ( constFlag = 1
              or LD.numDomainID = @numDomainID
            )
        and AB.numDomainID = @numDomainID
UNION
select  0 numPageNavID,
        14 numModuleID,
        LD.numListItemID AS numParentID,
        LT.vcData as vcPageNavName,
        '../Documents/frmDocList.aspx?Category='
        + convert(varchar(10), Ld.numListItemID) + '&Status='
        + convert(varchar(10), LT.numListItemID) vcNavURL,
        '../images/Text.gif' vcImageURL,
        0
from    ListDetails LT,
        listdetails ld
where   LT.numListID = 11
        and Lt.numDomainID = @numDomainID
        AND LD.numListID = 27
        AND ( ld.constFlag = 1
              or LD.numDomainID = @numDomainID
            )
        and ( LD.numListItemID in ( select  AB.numAuthoritativeSales
                                    from    AuthoritativeBizDocs AB
                                    where   AB.numDomainId = @numDomainID )
              or ld.numListItemID in ( select   AB.numAuthoritativePurchase
                                       from     AuthoritativeBizDocs AB
                                       where    AB.numDomainId = @numDomainID )
            )


--SELECT * FROM AuthoritativeBizDocs where numDomainId=72

 
--UNION
--	SELECT 0 numPageNavID,9 numModuleID,133 numParentID, 'BizDoc Approval Rule' as  vcPageNavName,'../Documents/frmBizDocAppRule.aspx' as vcNavURL      
-- ,'../images/tf_note.gif' vcImageURL,0 
--
--UNION
--	SELECT 0 numPageNavID,9 numModuleID,133 numParentID, 'Order Auto Rule' as  vcPageNavName,'../Documents/frmOrderAutoRules.aspx' as vcNavURL      
-- ,'../images/tf_note.gif' vcImageURL,1  
union
select  2222 numPageNavID,
        14 numModuleID,
        133 numParentID,
        'Other BizDocs' as vcPageNavName,
        '',
        '../images/tf_note.gif' vcImageURL,
        -1
union
SELECT  ld.numListItemID AS numPageNavID,
        14 numModuleID,
        2222 numParentID,
        vcData vcPageNavName,
        '../Documents/frmDocList.aspx?Category='
        + convert(varchar(10), LD.numListItemID) vcNavURL,
        '../images/Text.gif' vcImageURL,
        -1
FROM    listdetails Ld
where   ld.numListID = 27
        and ( constFlag = 1
              or LD.numDomainID = @numDomainID
            )
        and LD.numListItemID not in ( select    AB.numAuthoritativeSales
                                      from      AuthoritativeBizDocs AB
                                      where     AB.numDomainID = @numDomainID )
        and LD.numListItemID not in ( select    AB.numAuthoritativePurchase
                                      from      AuthoritativeBizDocs AB
                                      where     AB.numDomainID = @numDomainID )
union
SELECT  ls.numListItemID AS numPageNavID,
        14 numModuleID,
        ld.numListItemID as numParentID,
        ls.vcData vcPageNavName,
        '../Documents/frmDocList.aspx?Category='
        + convert(varchar(10), LD.numListItemID) vcNavURL,
        null vcImageURL,
        0
FROM    listdetails Ld,
        ( select    *
          from      ListDetails LS
          where     LS.numDomainID = @numDomainID
                    and LS.numListID = 11
        ) LS
where   ld.numListID = 27
        and ( ld.constFlag = 1
              or LD.numDomainID = @numDomainID
            )
        and LD.numListItemID not in ( select    AB.numAuthoritativeSales
                                      from      AuthoritativeBizDocs AB
                                      where     AB.numDomainID = @numDomainID
                                      union
                                      select    AB.numAuthoritativePurchase
                                      from      AuthoritativeBizDocs AB
                                      where     AB.numDomainID = @numDomainID )
order by numParentID,
        intSortOrder,
        numPageNavID

END
ELSE IF @numModuleID = 35 
BEGIN
	select PND.numPageNavID,numModuleID,numParentID,vcPageNavName,isnull(vcNavURL,'') as vcNavURL,vcImageURL ,0 sintOrder      
	from PageNavigationDTL PND
	JOIN dbo.TreeNavigationAuthorization TNA ON PND.numPageNavID = TNA.numPageNavID  --AND PND.numTabID = TNA.numTabID       
	WHERE  1 = 1
        AND ( ISNULL(TNA.bitVisible, 0) = 1
              --OR ISNULL(numParentID, 0) = 0
              --OR ISNULL(TNA.bitVisible, 0) = 0
            ) 
        AND ISNULL(PND.bitVisible, 0) = 1
	AND numModuleID=@numModuleID --AND bitVisible=1  
	AND TNA.numDomainID = @numDomainID
	AND numGroupID = @numGroupID
	    
	UNION 
	 SELECT numFRID,35,CASE numFinancialViewID WHEN 26770 THEN 96 /*Profit & Loss*/
	WHEN 26769 THEN 176 /*Income & Expense*/
	WHEN 26768 THEN 98 /*Cash Flow Statement*/
	WHEN 26767 THEN 97 /*Balance Sheet*/
	WHEN 26766 THEN 81 /*A/R & A/P Aging*/
	WHEN 26766 THEN 82 /*A/R & A/P Aging*/
	ELSE 0
	END 
	  ,vcReportName,CASE numFinancialViewID 
	WHEN 26770 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =96 AND numModuleID=35)  /*Profit & Loss*/
	WHEN 26769 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =176 AND numModuleID=35) /*Income & Expense*/
	WHEN 26768 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =98 AND numModuleID=35) /*Cash Flow Statement*/
	WHEN 26767 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =97 AND numModuleID=35) /*Balance Sheet*/
	WHEN 26766 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =81 AND numModuleID=35) /*A/R & A/P Aging*/
	WHEN 26766 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =82 AND numModuleID=35) /*A/R & A/P Aging*/
	ELSE ''
	END + 
	CASE CHARINDEX('?',(CASE numFinancialViewID 
	WHEN 26770 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =96 AND numModuleID=35)  /*Profit & Loss*/
	WHEN 26769 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =176 AND numModuleID=35) /*Income & Expense*/
	WHEN 26768 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =98 AND numModuleID=35) /*Cash Flow Statement*/
	WHEN 26767 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =97 AND numModuleID=35) /*Balance Sheet*/
	WHEN 26766 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =81 AND numModuleID=35) /*A/R & A/P Aging*/
	WHEN 26766 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =82 AND numModuleID=35) /*A/R & A/P Aging*/
	ELSE ''
	END)) 
	WHEN 0 THEN '?FRID='
	ELSE '&FRID='
	END
	 + CAST(numFRID AS VARCHAR)
	 'url',NULL,0 FROM dbo.FinancialReport WHERE numDomainID=@numDomainID
	order by numParentID,numPageNavID  
END
-------------- ADDED BY MANISH ANJARA ON : 6th April,2012
ELSE IF @numModuleID = -1 AND @numDomainID = -1 
BEGIN 
	SELECT PND.numPageNavID,numModuleID,numParentID,vcPageNavName,isnull(vcNavURL,'') as vcNavURL,vcImageURL ,0 sintOrder      
	FROM PageNavigationDTL PND
	JOIN dbo.TreeNavigationAuthorization TNA ON PND.numPageNavID = TNA.numPageNavID  --AND PND.numTabID = TNA.numTabID       
	WHERE  1 = 1
        AND ( ISNULL(TNA.bitVisible, 0) = 1
              --OR ISNULL(numParentID, 0) = 0
              --OR ISNULL(TNA.bitVisible, 0) = 0
            )
        AND ISNULL(PND.bitVisible, 0) = 1
	--AND TNA.numDomainID = @numDomainID
	AND numGroupID = @numGroupID
	--WHERE bitVisible=1  
	ORDER BY numParentID,numPageNavID 
END
--------------------------------------------------------- 
-------------- ADDED BY MANISH ANJARA ON : 12th Oct,2012
ELSE IF @numModuleID = 10 
BEGIN 
	DECLARE @numParentID AS NUMERIC(18,0)
	SELECT TOP 1 @numParentID = numParentID FROM dbo.PageNavigationDTL WHERE numModuleID = 10
	PRINT @numParentID

	SELECT * FROM (
	SELECT PND.numPageNavID,numModuleID,numParentID,vcPageNavName,isnull(vcNavURL,'') as vcNavURL,vcImageURL ,0 sintOrder      
	FROM PageNavigationDTL PND
	JOIN dbo.TreeNavigationAuthorization TNA ON PND.numPageNavID = TNA.numPageNavID  --AND PND.numTabID = TNA.numTabID       
	WHERE  1 = 1
        AND ( ISNULL(TNA.bitVisible, 0) = 1
              --OR ISNULL(numParentID, 0) = 0
              --OR ISNULL(TNA.bitVisible, 0) = 0
            )
        AND ISNULL(PND.bitVisible, 0) = 1
	AND numModuleID = @numModuleID
	AND TNA.numDomainID = @numDomainID
	AND numGroupID = @numGroupID
	
	UNION ALL

	SELECT  numListItemID AS numPageNavID,
		    (SELECT numModuleID FROM ModuleMaster WHERE vcModuleName = 'Opportunities') AS [numModuleID],
			@numParentID AS numParentID,
			ISNULL(vcData,'') + 's' [vcPageNavName],
			'../Opportunity/frmOpenBizDocs.aspx?BizDocName=' + vcData + '&BizDocID=' + CONVERT(VARCHAR(10),LD.numListItemID) AS [vcNavURL],
			'../images/Text.gif' AS [vcImageURL],
			1 AS [bitVisible] 
	FROM dbo.ListDetails LD
	JOIN dbo.TreeNavigationAuthorization TNA ON LD.numListItemID = TNA.numPageNavID  AND ISNULL(bitVisible,0) = 1  
	WHERE numListID = 27 
	AND ISNULL(TNA.[numDomainID], 0) = @numDomainID
	AND ISNULL(TNA.[numGroupID], 0) = @numGroupID) AS [FINAL]
	ORDER BY numParentID,FINAL.numPageNavID 
END
--------------------------------------------------------- 
--ELSE IF @numModuleID = 13
--	BEGIN
--		select PND.numPageNavID,
--        numModuleID,
--        numParentID,
--        vcPageNavName,
--        isnull(vcNavURL, '') as vcNavURL,
--        vcImageURL,
--        0 sintOrder
-- from   PageNavigationDTL PND
-- WHERE  1 = 1
--        AND ISNULL(PND.bitVisible, 0) = 1
--        AND numModuleID = @numModuleID
-- order by numParentID,
--        numPageNavID 
--	END
--	
--	ELSE IF @numModuleID = 9
--	BEGIN
--			select PND.numPageNavID,
--        numModuleID,
--        numParentID,
--        vcPageNavName,
--        isnull(vcNavURL, '') as vcNavURL,
--        vcImageURL,
--        0 sintOrder
-- from   PageNavigationDTL PND
-- WHERE  1 = 1
--        AND ISNULL(PND.bitVisible, 0) = 1
--        AND numModuleID = @numModuleID
-- order by numParentID,
--        numPageNavID 
--	END

ELSE IF (@numModuleID = 7 OR @numModuleID = 36)
BEGIN
  select    PND.numPageNavID,
            numModuleID,
            numParentID,
            vcPageNavName,
            isnull(vcNavURL, '') as vcNavURL,
            vcImageURL,
            0 sintOrder
  from      PageNavigationDTL PND
  WHERE     1 = 1
            AND ISNULL(PND.bitVisible, 0) = 1
            AND numModuleID = @numModuleID
  order by  numParentID,
            numPageNavID 
END	
else      
begin            
 select PND.numPageNavID,
        PND.numModuleID,
        numParentID,
        vcPageNavName,
        isnull(vcNavURL, '') as vcNavURL,
        vcImageURL,
        0 sintOrder
 from   PageNavigationDTL PND
        JOIN dbo.TreeNavigationAuthorization TNA ON PND.numPageNavID = TNA.numPageNavID  --AND PND.numTabID = TNA.numTabID       
 WHERE  1 = 1
        AND ( ISNULL(TNA.bitVisible, 0) = 1
              --OR ISNULL(numParentID, 0) = 0
              --OR ISNULL(TNA.bitVisible, 0) = 0
            )
        AND ISNULL(PND.bitVisible, 0) = 1
        AND PND.numModuleID = @numModuleID --AND bitVisible=1  
        AND numGroupID = @numGroupID
 order by numParentID,
        numPageNavID      
END

--select * from TreeNavigationAuthorization where numModuleID=13 
--select * from TreeNavigationAuthorization where numTabID=-1 AND numDomainID = 1 AND numGroupID = 1
--select * from TreeNavigationAuthorization where numListID=11 and numDomainId=72
--select * from AuthoritativeBizDocs where numDomainId=72











GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='Usp_GetShippingDetailsForOrder')
DROP PROCEDURE Usp_GetShippingDetailsForOrder
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- EXEC Usp_GetShippingDetailsForOrder 95379,170
CREATE PROCEDURE Usp_GetShippingDetailsForOrder
(
	@numOppID		NUMERIC(18),
	@numDomainID	NUMERIC(18)
)
AS 
BEGIN
	SELECT TOP 1 OM.numOppId,OBD.numBizDocId,OBD.numOppBizDocsId, SR.numShippingReportId, SR.numShippingCompany, 
	STUFF ((SELECT ',' + ISNULL(vcTrackingNumber,'') FROM dbo.ShippingBox WHERE numShippingReportId = SR.numShippingReportId FOR XML PATH('')), 1, 1, '') [vcTrackingNumber],
--	CONVERT(VARCHAR(20),(SELECT COUNT(numBoxID) FROM dbo.ShippingBox WHERE numShippingReportId = SR.numShippingReportId)) + ' (' + 
--	(
--		STUFF ((SELECT ', ' + CONVERT(VARCHAR(20),ISNULL(fltLength,0)) + 'x' +  CONVERT(VARCHAR(20),ISNULL(fltWidth,0)) + 'x' + CONVERT(VARCHAR(20),ISNULL(fltHeight,0)) FROM dbo.ShippingBox WHERE numShippingReportId = SR.numShippingReportId FOR XML PATH('')), 1, 1, '')	
--	) + ')' AS [vcBoxDimensions],
	CONVERT(VARCHAR(20),(SELECT COUNT(numBoxID) FROM dbo.ShippingBox WHERE numShippingReportId = SR.numShippingReportId)) AS [numBoxCount],
	(SELECT TOP 1 numBoxID FROM dbo.ShippingBox WHERE numShippingReportId = SR.numShippingReportId) AS [numBoxID],
	CASE WHEN (SELECT COUNT(*) FROM dbo.ShippingReport WHERE dbo.ShippingReport.numOppBizDocId = OBD.numOppBizDocsId AND numDomainID = @numDomainID) > 0 THEN 1 
		 ELSE 0 
	END AS [IsShippingReportGenerated],
	CASE WHEN (SELECT COUNT(*) FROM dbo.ShippingBox 
			   WHERE numShippingReportId IN (SELECT numShippingReportId FROM dbo.ShippingReport WHERE numOppBizDocId = OBD.numOppBizDocsId AND numDomainID = @numDomainID)
			   AND LEN(ISNULL(vcTrackingNumber,'')) > 0) > 0 THEN 1 
		 ELSE 0 
	END AS [ISTrackingNumGenerated],
	LD.vcData as BizDoc,
	CASE WHEN tintOppType = 1  THEN (CASE WHEN ISNULL(numAuthoritativeSales,0) = numBizDocId THEN 'Authoritative' + CASE WHEN ISNULL(OBD.tintDeferred,0) = 1 THEN ' (Deferred)' 
																														 ELSE '' 
																													END 
									 ELSE 'Non-Authoritative' 
									 END ) 
		 WHEN tintOppType = 2  THEN (CASE WHEN ISNULL(numAuthoritativePurchase,0) = numBizDocId THEN 'Authoritative' + CASE WHEN ISNULL(OBD.tintDeferred,0) = 1 THEN ' (Deferred)' 
																															ELSE '' 
																													   END 
										  ELSE 'Non-Authoritative' 
									 END ) 
	END AS BizDocType,
	ISNULL(vcBizDocID,'') [vcBizDocID],
	(
		SELECT SUM(fltTotalWeight) FROM dbo.ShippingBox SBOX WHERE SBOX.numShippingReportId = SR.numShippingReportId
		GROUP BY SBOX.numShippingReportId
	) AS [BoxWeight],
	(
		STUFF ((SELECT '~' + ISNULL(dbo.ShippingBox.vcBoxName,'') + '- Weight: ' + CONVERT(VARCHAR(20),ISNULL(fltTotalWeight,0)) + ' lbs, LxBxH: ' + CONVERT(VARCHAR(20),ISNULL(fltLength,0)) + 'x' +  CONVERT(VARCHAR(20),ISNULL(fltWidth,0)) + 'x' + CONVERT(VARCHAR(20),ISNULL(fltHeight,0)) FROM dbo.ShippingBox WHERE numShippingReportId = SR.numShippingReportId FOR XML PATH('')), 1, 1, '') 
	) AS [WeightPerBox],
	STUFF ((SELECT ', ' + vcBizDocID + '~' + CONVERT(VARCHAR(10), numOppBizDocsId) FROM dbo.OpportunityBizDocs OppBiz WHERE OppBiz.numOppId = OM.numOppId  FOR XML PATH('')), 1, 1, '') [vcBizDocs],
	ISNULL((SELECT MAX(SB.numShippingReportId) FROM [ShippingBox] SB 
		  WHERE SB.numShippingReportId = SR.numShippingReportId
		  AND SR.numOppBizDocId = OBD.numoppbizdocsid AND SR.numDomainID = OM.numDomainID 
		  AND LEN(ISNULL(SB.vcTrackingNumber,''))>0),0) AS numShippingLabelReportId
	FROM dbo.OpportunityBizDocs OBD
	JOIN dbo.OpportunityMaster OM ON OBD.numOppId = OM.numOppId
	JOIN dbo.ShippingReport SR ON OBD.numOppBizDocsId = SR.numOppBizDocId AND SR.numOppID = OM.numOppId
	LEFT JOIN ListDetails LD on LD.numListItemID = OBD.numBizDocId 
	LEFT JOIN AuthoritativeBizDocs AB on AB.numDomainId = OM.numDomainID  
	WHERE OM.numDomainId = @numDomainID
	AND OM.numOppId = @numOppID
	ORDER BY SR.numShippingReportId DESC
END

/****** Object:  StoredProcedure [dbo].[USP_GetShippingReport]    Script Date: 05/07/2009 17:34:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--SELECT * FROM ShippingReport
--SELECT * FROM ShippingReportItems
-- USP_GetShippingReport 1,60754,792
-- exec USP_GetShippingReport @numDomainID=72,@numOppBizDocId=5465,@ShippingReportId=4
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetShippingReport' ) 
    DROP PROCEDURE USP_GetShippingReport
GO
CREATE PROCEDURE [dbo].[USP_GetShippingReport]
    @numDomainID NUMERIC(9),
    @numOppBizDocId NUMERIC(9),
    @ShippingReportId NUMERIC(9)
AS 
    BEGIN
  
        SELECT  SRI.numItemCode,
                I.vcItemName,
                I.vcModelID,
                CASE WHEN LEN(BDI.vcItemDesc) > 100
                     THEN CONVERT(VARCHAR(100), BDI.vcItemDesc) + '..'
                     ELSE BDI.vcItemDesc
                END vcItemDesc,
                dbo.FormatedDateFromDate(SRI.dtDeliveryDate, @numDomainID) dtDeliveryDate,
                SRI.tintServiceType,
                ISNULL(I.fltWeight, 0) AS [fltTotalWeight],
                ISNULL(I.[fltHeight], 0) AS [fltHeight],
                ISNULL(I.[fltLength], 0) AS [fltLength],
                ISNULL(I.[fltWidth], 0) AS [fltWidth],
                ISNULL(SRI.monShippingRate,(SELECT monTotAmount FROM dbo.OpportunityItems 
											WHERE SR.numOppID = dbo.OpportunityItems.numOppId
											AND numItemCode = (SELECT numShippingServiceITemID FROM dbo.Domain WHERE numDomainId = @numDomainId))) [monShippingRate],
                SB.vcShippingLabelImage,
                SB.vcTrackingNumber,
                SRI.ShippingReportItemId,
                SRI.intNoOfBox,
                SR.[vcValue3],
                SR.[vcValue4],
                CASE WHEN ISNUMERIC(SR.vcFromState) = 1
                     THEN ( SELECT  vcStateCode
                            FROM    ShippingStateMaster
                            WHERE   vcStateName = ( SELECT TOP 1
                                                            vcState
                                                    FROM    State
                                                    WHERE   numStateID = SR.vcFromState
                                                  )
                                    AND [numShipCompany] = SR.[numShippingCompany]
                          )
                     ELSE ''
                END vcFromState,
                CASE WHEN ISNUMERIC(SR.vcFromCountry) = 1
                     THEN ( SELECT  vcCountryCode
                            FROM    ShippingCountryMaster
                            WHERE   vcCountryName = ( SELECT TOP 1
                                                                vcData
                                                      FROM      ListDetails
                                                      WHERE     numListItemID = SR.vcFromCountry
                                                    )
                                    AND [numShipCompany] = SR.[numShippingCompany]
                          )
                     ELSE ''
                END vcFromCountry,
                CASE WHEN ISNUMERIC(SR.vcToState) = 1
                     THEN ( SELECT  vcStateCode
                            FROM    ShippingStateMaster
                            WHERE   vcStateName = ( SELECT TOP 1
                                                            vcState
                                                    FROM    State
                                                    WHERE   numStateID = SR.vcToState
                                                  )
                                    AND [numShipCompany] = SR.[numShippingCompany]
                          )
                     ELSE ''
                END vcToState,
                CASE WHEN ISNUMERIC(SR.vcToCountry) = 1
                     THEN ( SELECT  vcCountryCode
                            FROM    ShippingCountryMaster
                            WHERE   vcCountryName = ( SELECT TOP 1
                                                                vcData
                                                      FROM      ListDetails
                                                      WHERE     numListItemID = SR.vcToCountry
                                                    )
                                    AND [numShipCompany] = SR.[numShippingCompany]
                          )
                     ELSE ''
                END vcToCountry,
                SR.vcToZip,
                SR.vcFromZip,
                SR.[numShippingCompany],
                SB.[numBoxID],
                SB.[vcBoxName],
                BDI.numOppBizDocItemID,
                SR.tintPayorType,
                ISNULL(SR.vcPayorAccountNo, '') vcPayorAccountNo,
                ISNULL(SR.numPayorCountry, 0) numPayorCountry,
                ( SELECT    vcCountryCode
                  FROM      ShippingCountryMaster
                  WHERE     vcCountryName = ( SELECT TOP 1
                                                        vcData
                                              FROM      ListDetails
                                              WHERE     numListItemID = SR.numPayorCountry
                                            )
                            AND [numShipCompany] = SR.[numShippingCompany]
                ) vcPayorCountryCode,
                SR.vcPayorZip,
                SR.vcFromCity,
                SR.vcFromAddressLine1,
                SR.vcFromAddressLine2,
                ISNULL(SR.vcFromCountry,0) [numFromCountry],
                ISNULL(SR.vcFromState,0) [numFromState],
                SR.vcToCity,
                SR.vcToAddressLine1,
                SR.vcToAddressLine2,
                ISNULL(SR.vcToCountry,0) [numToCountry],
                ISNULL(SR.vcToState,0) [numToState],
                SRI.intBoxQty,
                ISNULL(SB.numServiceTypeID, 0) AS [numServiceTypeID],
                ISNULL(SR.vcFromCompany, '') AS vcFromCompany,
                ISNULL(SR.vcFromName, '') AS vcFromName,
                ISNULL(SR.vcFromPhone, '') AS vcFromPhone,
                ISNULL(SR.vcToCompany, '') AS vcToCompany,
                ISNULL(SR.vcToName, '') AS vcToName,
                ISNULL(SR.vcToPhone, '') AS vcToPhone,
                ISNULL(SB.fltDimensionalWeight, 0) AS [fltDimensionalWeight],
                dbo.fn_GetContactName(SR.numCreatedBy) + ' : '
                + CONVERT(VARCHAR(20), SR.dtCreateDate) AS [CreatedDetails],
                CASE WHEN SR.numShippingCompany = 91 THEN 'Fedex'
                     WHEN SR.numShippingCompany = 88 THEN 'UPS'
                     WHEN SR.numShippingCompany = 90 THEN 'USPS'
                     ELSE 'Other'
                END AS [vcShipCompany],
                CAST(ROUND(( ( SELECT   SUM(fltTotalWeight)
                               FROM     dbo.ShippingReportItems
                               WHERE    numShippingReportId = SR.numShippingReportId
                             ) * intBoxQty ), 2) AS DECIMAL(9, 2)) AS [fltTotalRegularWeight],
                CAST(ROUND(( SELECT SUM(fltDimensionalWeight)
                             FROM   dbo.ShippingBox
                             WHERE  numShippingReportId = SR.numShippingReportId
                           ), 2) AS DECIMAL(9, 2)) AS [fltTotalDimensionalWeight],
                ISNULL(monTotAmount, 0) AS [monTotAmount],
                ISNULL(bitFromResidential,0) [bitFromResidential],
                ISNULL(bitToResidential,0) [bitToResidential],
                ISNULL(IsCOD,0) [IsCOD],
				ISNULL(IsDryIce,0) [IsDryIce],
				ISNULL(IsHoldSaturday,0) [IsHoldSaturday],
				ISNULL(IsHomeDelivery,0) [IsHomeDelivery],
				ISNULL(IsInsideDelevery,0) [IsInsideDelevery],
				ISNULL(IsInsidePickup,0) [IsInsidePickup],
				ISNULL(IsReturnShipment,0) [IsReturnShipment],
				ISNULL(IsSaturdayDelivery,0) [IsSaturdayDelivery],
				ISNULL(IsSaturdayPickup,0) [IsSaturdayPickup],
				ISNULL(IsAdditionalHandling,0) [IsAdditionalHandling],
				ISNULL(vcCODType,0) [vcCODType],
				numOppID,
				ISNULL(numCODAmount,0) [numCODAmount],
				ISNULL([SR].[numTotalInsuredValue],0) AS [numTotalInsuredValue],
				ISNULL([SR].[numTotalCustomsValue],0) AS [numTotalCustomsValue]
        FROM    ShippingReport AS SR
                INNER JOIN ShippingBox SB ON SB.numShippingReportId = SR.numShippingReportId
                INNER JOIN ShippingReportItems AS SRI ON SR.numShippingReportId = SRI.numShippingReportId
                                                         AND SRI.numBoxID = SB.numBoxID
                INNER JOIN OpportunityBizDocItems BDI ON SRI.numItemCode = BDI.numItemCode
                                                         AND SR.numOppBizDocId = BDI.numOppBizDocID
                INNER JOIN Item I ON BDI.numItemCode = I.numItemCode
        WHERE   SR.[numOppBizDocId] = @numOppBizDocId
                AND SR.numDomainId = @numDomainID
                AND SR.[numShippingReportId] = @ShippingReportId
--           AND (SB.bitIsMasterTrackingNo = 1 OR SB.bitIsMasterTrackingNo IS NULL)
           
        SELECT  vcBizDocID
        FROM    [OpportunityBizDocs]
        WHERE   [numOppBizDocsId] = @numOppBizDocId

    END
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_Item_SearchForSalesOrder' )
    DROP PROCEDURE USP_Item_SearchForSalesOrder
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Sandeep Patel
-- Create date: 3 March 2014
-- Description:	Search items for sales order
-- =============================================
CREATE PROCEDURE USP_Item_SearchForSalesOrder
    @tintOppType AS TINYINT ,
    @numDomainID AS NUMERIC(9) ,
    @numDivisionID AS NUMERIC(9) = 0 ,
    @str AS VARCHAR(1000) ,
    @numUserCntID AS NUMERIC(9) ,
    @tintSearchOrderCustomerHistory AS TINYINT = 0 ,
    @numPageIndex AS INT ,
    @numPageSize AS INT ,
    @WarehouseID AS NUMERIC(18, 0) = NULL ,
    @TotalCount AS INT OUTPUT
AS
    BEGIN
  SET NOCOUNT ON;

DECLARE @strNewQuery NVARCHAR(MAX)
DECLARE @strSQL AS NVARCHAR(MAX)
SELECT  @str = REPLACE(@str, '''', '''''')

DECLARE @TableRowCount TABLE ( Value INT );
	
SELECT  *
INTO    #Temp1
FROM    ( SELECT    numFieldId ,
                    vcDbColumnName ,
                    ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                    tintRow AS tintOrder ,
                    0 AS Custom
          FROM      View_DynamicColumns
          WHERE     numFormId = 22
                    AND numUserCntID = @numUserCntID
                    AND numDomainID = @numDomainID
                    AND tintPageType = 1
                    AND bitCustom = 0
                    AND ISNULL(bitSettingField, 0) = 1
                    AND numRelCntType = 0
          UNION
          SELECT    numFieldId ,
                    vcFieldName ,
                    vcFieldName ,
                    tintRow AS tintOrder ,
                    1 AS Custom
          FROM      View_DynamicCustomColumns
          WHERE     Grp_id = 5
                    AND numFormId = 22
                    AND numUserCntID = @numUserCntID
                    AND numDomainID = @numDomainID
                    AND tintPageType = 1
                    AND bitCustom = 1
                    AND numRelCntType = 0
        ) X 
  
IF NOT EXISTS ( SELECT  *
                FROM    #Temp1 )
    BEGIN
        INSERT  INTO #Temp1
                SELECT  numFieldId ,
                        vcDbColumnName ,
                        ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                        tintorder ,
                        0
                FROM    View_DynamicDefaultColumns
                WHERE   numFormId = 22
                        AND bitDefault = 1
                        AND ISNULL(bitSettingField, 0) = 1
                        AND numDomainID = @numDomainID 
    END

CREATE TABLE #tempItemCode ( numItemCode NUMERIC(9) )
DECLARE @bitRemoveVendorPOValidation AS BIT
SELECT  
	@bitRemoveVendorPOValidation = ISNULL(bitRemoveVendorPOValidation,0)
FROM    
	domain
WHERE  
	numDomainID = @numDomainID

IF @tintOppType > 0
    BEGIN 
        IF CHARINDEX(',', @str) > 0
        BEGIN
			DECLARE @itemSearchText VARCHAR(100)
            DECLARE @vcAttribureSearch VARCHAR(MAX)
			DECLARE @vcAttribureSearchCondition VARCHAR(MAX)

			SET @itemSearchText = LTRIM(RTRIM(SUBSTRING(@str,0,CHARINDEX(',',@str))))

			--GENERATES ATTRIBUTE SEARCH CONDITION
			SET @vcAttribureSearch = SUBSTRING(@str,CHARINDEX(',',@str) + 1,LEN(@str))
	
			DECLARE @attributeSearchText VARCHAR(MAX)
			DECLARE @TempTable TABLE (vcValue VARCHAR(MAX))

			WHILE LEN(@vcAttribureSearch) > 0
			BEGIN
				SET @attributeSearchText = LEFT(@vcAttribureSearch, 
										ISNULL(NULLIF(CHARINDEX(',', @vcAttribureSearch) - 1, -1),
										LEN(@vcAttribureSearch)))
				SET @vcAttribureSearch = SUBSTRING(@vcAttribureSearch,
												ISNULL(NULLIF(CHARINDEX(',', @vcAttribureSearch), 0),
												LEN(@vcAttribureSearch)) + 1, LEN(@vcAttribureSearch))

				INSERT INTO @TempTable (vcValue) VALUES ( @attributeSearchText )
			END
		
			--REMOVES WHITE SPACES
			UPDATE
				@TempTable
			SET
				vcValue = LTRIM(RTRIM(vcValue))
	
			--CONVERTING TABLE TO COMMA SEPERATED STRING AFTER REMOVEING WHITE SPACES
			SELECT @vcAttribureSearch = COALESCE(@vcAttribureSearch,'') +  CONVERT(VARCHAR(MAX),vcValue) + ',' FROM @TempTable
	
			--REMOVE LAST COMMA FROM FINAL SEARCH STRING
			SET @vcAttribureSearch = LTRIM(RTRIM(@vcAttribureSearch))
			IF DATALENGTH(@vcAttribureSearch) > 0
				SET @vcAttribureSearch = LEFT(@vcAttribureSearch, LEN(@vcAttribureSearch) - 1)
	
			--START: GENERATES ATTRIBUTE SEARCH CONDITION
			SELECT 	@vcAttribureSearchCondition = '(TEMPMATRIX.Attributes LIKE ''%' + REPLACE(@vcAttribureSearch, ',', '%'' AND TEMPMATRIX.Attributes LIKE ''%') + '%'')' 

			--LOGIC FOR GENERATING FINAL SQL STRING
			IF @tintOppType = 1 OR ( @bitRemoveVendorPOValidation = 1 AND @tintOppType = 2)
                BEGIN      

				SET @strSQL = 'SELECT
									I.numItemCode,
									I.numVendorID,
									MIN(WHT.numWareHouseItemID) as numWareHouseItemID,
									dbo.fn_GetAttributes(WHT.numWareHouseItemId,I.bitSerialized) AS [Attributes]
								FROM
									dbo.Item I
								LEFT JOIN
									dbo.WareHouseItems WHT
								ON
									I.numItemCode = WHT.numItemID
								WHERE 
									((ISNULL(I.numItemGroup,0) > 0 AND I.vcItemName LIKE ''%' + @itemSearchText + '%'') OR (ISNULL(I.numItemGroup,0) = 0 AND I.vcItemName LIKE ''%' + @str + '%''))
									AND ISNULL(I.Isarchieve, 0) <> 1
									AND I.charItemType NOT IN ( ''A'' )
									AND I.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + '
									AND (WHT.numWareHouseID = ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0)'

				--- added Asset validation  by sojan
                IF @bitRemoveVendorPOValidation = 1
                    AND @tintOppType = 2
                    SET @strSQL = @strSQL
                        + ' and ISNULL(I.bitKitParent,0) = 0 '

                IF @tintSearchOrderCustomerHistory = 1 AND @numDivisionID > 0
                BEGIN
                    SET @strSQL = @strSQL
                        + ' and I.numItemCode in (select distinct oppI.numItemCode from 
						OpportunityMaster oppM join OpportunityItems oppI on oppM.numOppId=oppI.numOppId where oppM.numDomainID='
                        + CONVERT(VARCHAR(20), @numDomainID)
                        + ' and oppM.numDivisionId='
                        + CONVERT(VARCHAR(20), @numDivisionId)
                        + ')'
                END

				SET @strSQL = @strSQL + ' GROUP BY
										I.numItemCode,
										I.numVendorID,
										dbo.fn_GetAttributes(numWareHouseItemId,I.bitSerialized)'

				SET @strSQL = 
				'
				SELECT    
					SRNO=ROW_NUMBER() OVER( ORDER BY  I.vcItemName),
					I.numItemCode,
					(SELECT TOP 1 vcPathForImage FROM ItemImages WHERE numItemCode = I.numItemCode) AS vcPathForImage ,
					(SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = I.numItemCode) AS vcPathForTImage ,
					ISNULL(vcItemName, '''') vcItemName ,
					CASE WHEN I.[charItemType] = ''P'' AND ISNULL(I.bitSerialized, 0) = 0  AND ISNULL(I.bitLotNo, 0) = 0
					THEN CONVERT(VARCHAR(100), ISNULL((SELECT TOP 1 ROUND(monWListPrice,2) FROM [WareHouseItems] WHERE [numItemID] = I.numItemCode), 0))
					ELSE CONVERT(VARCHAR(100), ROUND(monListPrice, 2))
					END AS monListPrice,
					ISNULL(vcSKU, '''') vcSKU ,
					ISNULL(numBarCodeId, 0) numBarCodeId ,
					ISNULL(vcModelID, '''') vcModelID ,
					ISNULL(txtItemDesc, '''') AS txtItemDesc ,
					ISNULL(C.vcCompanyName, '''') AS vcCompanyName,
					TEMPMATRIX.numWareHouseItemID,
					TEMPMATRIX.Attributes AS [vcAttributes]
				FROM  
					(' + @strSQL +') AS TEMPMATRIX
				INNER JOIN    
					Item I
				ON
					TEMPMATRIX.numItemCode = I.numItemCode
				LEFT JOIN 
					DivisionMaster D 
				ON 
					I.numVendorID = D.numDivisionID
				LEFT JOIN 
					CompanyInfo C 
				ON 
					C.numCompanyID = D.numCompanyID
				WHERE  
				  ((ISNULL(I.numItemGroup,0) = 0 AND I.vcItemName LIKE ''%' + @str + '%'') OR ' + @vcAttribureSearchCondition + ')'

				

                INSERT  INTO @TableRowCount
                        EXEC
                            ( 'SELECT COUNT(*) FROM ( ' + @strSQL
                                + ') as t2'
                                    );

                SELECT  @TotalCount = Value
                FROM    @TableRowCount;

                SET @strNewQuery = 'select * from (' + @strSQL
                    + ') as t where SRNO> '
                    + CAST(( ( @numPageIndex - 1 ) * @numPageSize ) AS VARCHAR(10))
                    + ' and SRNO < '
                    + CAST(( ( @numPageIndex * @numPageSize ) + 1 ) AS VARCHAR(10))
                    + ' order by  vcItemName'

				--PRINT @strNewQuery
                EXEC(@strNewQuery)
				END
			ELSE
				BEGIN

				SET @strSQL = 'SELECT
									I.numItemCode,
									I.numVendorID,
									MIN(WHT.numWareHouseItemID) as numWareHouseItemID,
									dbo.fn_GetAttributes(WHT.numWareHouseItemId,I.bitSerialized) AS [Attributes]
								FROM
									dbo.Item I
								INNER JOIN
									dbo.Vendor V
								ON
									V.numItemCode = I.numItemCode
								LEFT JOIN
									dbo.WareHouseItems WHT
								ON
									I.numItemCode = WHT.numItemID
								WHERE 
									((ISNULL(I.numItemGroup,0) > 0 AND I.vcItemName LIKE ''%' + @itemSearchText + '%'') OR (ISNULL(I.numItemGroup,0) = 0 AND I.vcItemName LIKE ''%' + @str + '%''))
									AND ISNULL(I.Isarchieve, 0) <> 1
									AND I.charItemType NOT IN ( ''A'' )
									AND I.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + '
									AND (WHT.numWareHouseID = ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0)'

				--- added Asset validation  by sojan
                IF @tintSearchOrderCustomerHistory = 1 AND @numDivisionID > 0
                BEGIN
                    SET @strSQL = @strSQL
                        + ' and I.numItemCode in (select distinct oppI.numItemCode from 
						OpportunityMaster oppM join OpportunityItems oppI on oppM.numOppId=oppI.numOppId where oppM.numDomainID='
                        + CONVERT(VARCHAR(20), @numDomainID)
                        + ' and oppM.numDivisionId='
                        + CONVERT(VARCHAR(20), @numDivisionId)
                        + ')'
                END

				SET @strSQL = @strSQL + ' GROUP BY
										I.numItemCode,
										I.numVendorID,
										dbo.fn_GetAttributes(numWareHouseItemId,I.bitSerialized)'

				SET @strSQL = 
				'
				SELECT    
					SRNO=ROW_NUMBER() OVER( ORDER BY  I.vcItemName),
					I.numItemCode,
					(SELECT TOP 1 vcPathForImage FROM ItemImages WHERE numItemCode = I.numItemCode) AS vcPathForImage ,
					(SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = I.numItemCode) AS vcPathForTImage ,
					ISNULL(vcItemName, '''') vcItemName ,
					CASE WHEN I.[charItemType] = ''P'' AND ISNULL(I.bitSerialized, 0) = 0  AND ISNULL(I.bitLotNo, 0) = 0
					THEN CONVERT(VARCHAR(100), ISNULL((SELECT TOP 1 ROUND(monWListPrice,2) FROM [WareHouseItems] WHERE [numItemID] = I.numItemCode), 0))
					ELSE CONVERT(VARCHAR(100), ROUND(monListPrice, 2))
					END AS monListPrice,
					ISNULL(vcSKU, '''') vcSKU ,
					ISNULL(numBarCodeId, 0) numBarCodeId ,
					ISNULL(vcModelID, '''') vcModelID ,
					ISNULL(txtItemDesc, '''') AS txtItemDesc ,
					ISNULL(C.vcCompanyName, '''') AS vcCompanyName,
					TEMPMATRIX.numWareHouseItemID,
					TEMPMATRIX.Attributes AS [vcAttributes]
				FROM  
					(' + @strSQL +') AS TEMPMATRIX
				INNER JOIN    
					Item I
				ON
					TEMPMATRIX.numItemCode = I.numItemCode
				LEFT JOIN 
					DivisionMaster D 
				ON 
					I.numVendorID = D.numDivisionID
				LEFT JOIN 
					CompanyInfo C 
				ON 
					C.numCompanyID = D.numCompanyID
				WHERE  
				 ((ISNULL(I.numItemGroup,0) = 0 AND I.vcItemName LIKE ''%' + @str + '%'') OR ' + @vcAttribureSearchCondition + ')'

                INSERT  INTO @TableRowCount
                        EXEC
                            ( 'SELECT COUNT(*) FROM ( ' + @strSQL
                                + ') as t2'
                                    );

                SELECT  @TotalCount = Value
                FROM    @TableRowCount;

                SET @strNewQuery = 'select * from (' + @strSQL
                    + ') as t where SRNO> '
                    + CAST(( ( @numPageIndex - 1 ) * @numPageSize ) AS VARCHAR(10))
                    + ' and SRNO < '
                    + CAST(( ( @numPageIndex * @numPageSize ) + 1 ) AS VARCHAR(10))
                    + ' order by  vcItemName'

				--PRINT @strNewQuery
                EXEC(@strNewQuery)
			END	
        END
        ELSE
        BEGIN
				DECLARE @vcWareHouseSearch VARCHAR(1000)
				DECLARE @vcVendorSearch VARCHAR(1000)
               

				SET @vcWareHouseSearch = ''
				SET @vcVendorSearch = ''

                DECLARE @tintOrder AS INT
                DECLARE @Fld_id AS NVARCHAR(20)
                DECLARE @Fld_Name AS VARCHAR(20)
                SET @strSQL = ''
                SELECT TOP 1
                        @tintOrder = tintOrder + 1 ,
                        @Fld_id = numFieldId ,
                        @Fld_Name = vcFieldName
                FROM    #Temp1
                WHERE   Custom = 1
                ORDER BY tintOrder
                WHILE @tintOrder > 0
                    BEGIN
                        SET @strSQL = @strSQL + ', dbo.GetCustFldValueItem('
                            + @Fld_id + ', I.numItemCode) as [' + @Fld_Name
                            + ']'

                        SELECT TOP 1
                                @tintOrder = tintOrder + 1 ,
                                @Fld_id = numFieldId ,
                                @Fld_Name = vcFieldName
                        FROM    #Temp1
                        WHERE   Custom = 1
                                AND tintOrder >= @tintOrder
                        ORDER BY tintOrder
                        IF @@rowcount = 0
                            SET @tintOrder = 0
                    END


				--Temp table for Item Search Configuration

                SELECT  *
                INTO    #tempSearch
                FROM    ( SELECT    numFieldId ,
                                    vcDbColumnName ,
                                    ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                                    tintRow AS tintOrder ,
                                    0 AS Custom
                          FROM      View_DynamicColumns
                          WHERE     numFormId = 22
                                    AND numUserCntID = @numUserCntID
                                    AND numDomainID = @numDomainID
                                    AND tintPageType = 1
                                    AND bitCustom = 0
                                    AND ISNULL(bitSettingField, 0) = 1
                                    AND numRelCntType = 1
                          UNION
                          SELECT    numFieldId ,
                                    vcFieldName ,
                                    vcFieldName ,
                                    tintRow AS tintOrder ,
                                    1 AS Custom
                          FROM      View_DynamicCustomColumns
                          WHERE     Grp_id = 5
                                    AND numFormId = 22
                                    AND numUserCntID = @numUserCntID
                                    AND numDomainID = @numDomainID
                                    AND tintPageType = 1
                                    AND bitCustom = 1
                                    AND numRelCntType = 1
                        ) X 
  
                IF NOT EXISTS ( SELECT  *
                                FROM    #tempSearch )
                    BEGIN
                        INSERT  INTO #tempSearch
                                SELECT  numFieldId ,
                                        vcDbColumnName ,
                                        ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                                        tintorder ,
                                        0
                                FROM    View_DynamicDefaultColumns
                                WHERE   numFormId = 22
                                        AND bitDefault = 1
                                        AND ISNULL(bitSettingField, 0) = 1
                                        AND numDomainID = @numDomainID 
                    END

				--Regular Search
                DECLARE @strSearch AS VARCHAR(8000) ,
                    @CustomSearch AS VARCHAR(4000) ,
                    @numFieldId AS NUMERIC(9)
                SET @strSearch = ''
                SELECT TOP 1
                        @tintOrder = tintOrder + 1 ,
                        @Fld_Name = vcDbColumnName ,
                        @numFieldId = numFieldId
                FROM    #tempSearch
                WHERE   Custom = 0
                ORDER BY tintOrder
                WHILE @tintOrder > -1
                    BEGIN
                        IF @Fld_Name = 'vcPartNo'
						BEGIN
                            INSERT  INTO #tempItemCode
                                    SELECT DISTINCT
                                            Item.numItemCode
                                    FROM    Vendor
                                            JOIN Item ON Item.numItemCode = Vendor.numItemCode
                                    WHERE   Item.numDomainID = @numDomainID
                                            AND Vendor.numDomainID = @numDomainID
                                            AND Vendor.vcPartNo IS NOT NULL
                                            AND LEN(Vendor.vcPartNo) > 0
                                            AND vcPartNo LIKE '%' + @str + '%'
							
							SET @vcVendorSearch = 'dbo.Vendor.vcPartNo LIKE ''%' + @str + '%'''
							
						END
                        ELSE IF @Fld_Name = 'vcBarCode'
						BEGIN
                            INSERT  INTO #tempItemCode
                                    SELECT DISTINCT
                                            Item.numItemCode
                                    FROM    Item
                                            JOIN WareHouseItems WI ON WI.numDomainID = @numDomainID
                                                            AND Item.numItemCode = WI.numItemID
                                            JOIN Warehouses W ON W.numDomainID = @numDomainID
                                                            AND W.numWareHouseID = WI.numWareHouseID
                                    WHERE   Item.numDomainID = @numDomainID
                                            AND ISNULL(Item.numItemGroup,
                                                        0) > 0
                                            AND WI.vcBarCode IS NOT NULL
                                            AND LEN(WI.vcBarCode) > 0
                                            AND WI.vcBarCode LIKE '%' + @str + '%'

							SET @vcWareHouseSearch = 'dbo.WareHouseItems.vcBarCode LIKE ''%' + @str + '%'''
						END
                        ELSE IF @Fld_Name = 'vcWHSKU'
						BEGIN
                            INSERT  INTO #tempItemCode
                                    SELECT DISTINCT
                                            Item.numItemCode
                                    FROM    Item
                                            JOIN WareHouseItems WI ON WI.numDomainID = @numDomainID
                                                        AND Item.numItemCode = WI.numItemID
                                            JOIN Warehouses W ON W.numDomainID = @numDomainID
                                                        AND W.numWareHouseID = WI.numWareHouseID
                                    WHERE   Item.numDomainID = @numDomainID
                                            AND ISNULL(Item.numItemGroup,
                                                        0) > 0
                                            AND WI.vcWHSKU IS NOT NULL
                                            AND LEN(WI.vcWHSKU) > 0
                                            AND WI.vcWHSKU LIKE '%'
                                            + @str + '%'

							IF LEN(@vcWareHouseSearch) > 0
								SET @vcWareHouseSearch = @vcWareHouseSearch + ' OR dbo.WareHouseItems.vcWHSKU LIKE ''%' + @str + '%'''
							ELSE
								SET @vcWareHouseSearch = 'dbo.WareHouseItems.vcWHSKU LIKE ''%' + @str + '%'''
						END
                        ELSE
                            SET @strSearch = @strSearch + ' ['
                                + @Fld_Name + '] LIKE ''%' + @str
                                + '%'''

                        SELECT TOP 1
                                @tintOrder = tintOrder + 1 ,
                                @Fld_Name = vcDbColumnName ,
                                @numFieldId = numFieldId
                        FROM    #tempSearch
                        WHERE   tintOrder >= @tintOrder
                                AND Custom = 0
                        ORDER BY tintOrder
                        IF @@rowcount = 0
                            BEGIN
                                SET @tintOrder = -1
                            END
                        ELSE
                            IF @Fld_Name != 'vcPartNo'
                                AND @Fld_Name != 'vcBarCode'
                                AND @Fld_Name != 'vcWHSKU'
                                BEGIN
                                    SET @strSearch = @strSearch + ' or '
                                END
                    END

                IF ( SELECT COUNT(*)
                     FROM   #tempItemCode
                   ) > 0
                    SET @strSearch = @strSearch
                        + ' or  I.numItemCode in (select numItemCode from #tempItemCode)' 

				--Custom Search
                SET @CustomSearch = ''
                SELECT TOP 1
                        @tintOrder = tintOrder + 1 ,
                        @Fld_Name = vcDbColumnName ,
                        @numFieldId = numFieldId
                FROM    #tempSearch
                WHERE   Custom = 1
                ORDER BY tintOrder
                WHILE @tintOrder > -1
                    BEGIN
    
                        SET @CustomSearch = @CustomSearch
                            + CAST(@numFieldId AS VARCHAR(10)) 

                        SELECT TOP 1
                                @tintOrder = tintOrder + 1 ,
                                @Fld_Name = vcDbColumnName ,
                                @numFieldId = numFieldId
                        FROM    #tempSearch
                        WHERE   tintOrder >= @tintOrder
                                AND Custom = 1
                        ORDER BY tintOrder
                        IF @@rowcount = 0
                            BEGIN
                                SET @tintOrder = -1
                            END
                        ELSE
                            BEGIN
                                SET @CustomSearch = @CustomSearch + ' , '
                            END
                    END
	
                IF LEN(@CustomSearch) > 0
                    BEGIN
                        SET @CustomSearch = ' I.numItemCode in (SELECT RecId FROM dbo.CFW_FLD_Values_Item WHERE Fld_ID IN ('
                            + @CustomSearch + ') and Fld_Value like ''%'
                            + @str + '%'')'

                        IF LEN(@strSearch) > 0
                            SET @strSearch = @strSearch + ' OR '
                                + @CustomSearch
                        ELSE
                            SET @strSearch = @CustomSearch  
                    END


                IF @tintOppType = 1
                    OR ( @bitRemoveVendorPOValidation = 1
                         AND @tintOppType = 2
                       )
                    BEGIN      
                        SET @strSQL = 'select SRNO=ROW_NUMBER() OVER( ORDER BY  I.vcItemName), I.numItemCode,
									  (SELECT TOP 1 vcPathForImage from ItemImages WHERE numItemCode = I.numItemCode) as vcPathForImage,
									  (SELECT TOP 1 vcPathForTImage from ItemImages WHERE numItemCode = I.numItemCode) as vcPathForTImage,
									  isnull(vcItemName,'''') vcItemName,
									  CASE 
									  WHEN I.[charItemType]=''P'' AND ISNULL(I.bitSerialized,0) = 0 AND  ISNULL(I.bitLotNo,0) = 0 
									  THEN convert(varchar(100),ISNULL((SELECT TOP 1 ROUND(monWListPrice,2) FROM [WareHouseItems] WHERE [numItemID]=I.numItemCode),0)) 
									  ELSE 
										CONVERT(VARCHAR(100), ROUND(monListPrice, 2)) 
									  END AS monListPrice,
									  isnull(vcSKU,'''') vcSKU,
									  isnull(numBarCodeId,0) numBarCodeId,
									  isnull(vcModelID,'''') vcModelID,
									  isnull(txtItemDesc,'''') as txtItemDesc,
									  isnull(C.vcCompanyName,'''') as vcCompanyName,
									  WHT.numWareHouseItemID,
									  dbo.fn_GetAttributes(WHT.numWareHouseItemId,I.bitSerialized) AS [vcAttributes]'
									  + @strSQL
									  + ' from Item  I 
									  Left join DivisionMaster D              
									  on I.numVendorID=D.numDivisionID  
									  left join CompanyInfo C  
									  on C.numCompanyID=D.numCompanyID 
									  LEFT JOIN
											dbo.WareHouseItems WHT
									  ON
											WHT.numItemID = I.numItemCode AND
											WHT.numWareHouseItemID = (
																		SELECT 
																			TOP 1 numWareHouseItemID 
																		FROM 
																			dbo.WareHouseItems 
																		WHERE 
																			dbo.WareHouseItems.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' 
																			AND dbo.WareHouseItems.numItemID = I.numItemCode 
																			AND (dbo.WareHouseItems.numWareHouseID = ' +  CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20))  + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0)' +
																			CASE LEN(@vcWareHouseSearch)
																			WHEN 0 THEN ''
																			ELSE ' AND (' + @vcWareHouseSearch + ' OR 1=1)'
																			END
																			+
																	') 
										where ((I.charItemType <> ''P'') OR ((SELECT COUNT(*) FROM dbo.WareHouseItems WHERE numItemID = I.numItemCode AND numDomainID = '
									  + CONVERT(VARCHAR(20), @numDomainID)
									  + ' AND numWareHouseID =  ' +  CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20))  + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0) > 0)) AND ISNULL(I.Isarchieve,0) <> 1 And I.charItemType NOT IN(''A'') AND  I.numDomainID='
									  + CONVERT(VARCHAR(20), @numDomainID) + ' and ('
									  + @strSearch + ') '   
							      
						--- added Asset validation  by sojan
                        IF @bitRemoveVendorPOValidation = 1
                            AND @tintOppType = 2
                            SET @strSQL = @strSQL
                                + ' and ISNULL(I.bitKitParent,0) = 0 '

                        IF @tintSearchOrderCustomerHistory = 1
                            AND @numDivisionID > 0
                            BEGIN
                                SET @strSQL = @strSQL
                                    + ' and I.numItemCode in (select distinct oppI.numItemCode from 
									OpportunityMaster oppM join OpportunityItems oppI on oppM.numOppId=oppI.numOppId where oppM.numDomainID='
                                    + CONVERT(VARCHAR(20), @numDomainID)
                                    + ' and oppM.numDivisionId='
                                    + CONVERT(VARCHAR(20), @numDivisionId)
                                    + ')'
                            END

                        INSERT  INTO @TableRowCount
                                EXEC
                                    ( 'SELECT COUNT(*) FROM ( ' + @strSQL
                                      + ') as t2'
                                    );
                        SELECT  @TotalCount = Value
                        FROM    @TableRowCount;

                        SET @strNewQuery = 'select * from (' + @strSQL
                            + ') as t where SRNO> '
                            + CAST(( ( @numPageIndex - 1 ) * @numPageSize ) AS VARCHAR(10))
                            + ' and SRNO < '
                            + CAST(( ( @numPageIndex * @numPageSize ) + 1 ) AS VARCHAR(10))
                            + ' order by  vcItemName'
                        
						--PRINT @strNewQuery
						EXEC(@strNewQuery)
                    END      
                ELSE
                    IF @tintOppType = 2
                        BEGIN      
                            SET @strSQL = 'SELECT SRNO=ROW_NUMBER() OVER( ORDER BY  I.vcItemName), I.numItemCode,
										  (SELECT TOP 1 vcPathForImage from ItemImages WHERE numItemCode = I.numItemCode) as vcPathForImage,
										  (SELECT TOP 1 vcPathForTImage from ItemImages WHERE numItemCode = I.numItemCode) as vcPathForTImage,
										  ISNULL(vcItemName,'''') vcItemName,
										  convert(varchar(200),round(monListPrice,2)) as monListPrice,
										  isnull(vcSKU,'''') vcSKU,
										  isnull(numBarCodeId,0) numBarCodeId,
										  isnull(vcModelID,'''') vcModelID,
										  isnull(txtItemDesc,'''') as txtItemDesc,
										  isnull(C.vcCompanyName,'''') as vcCompanyName,
										  WHT.numWareHouseItemID,
										  dbo.fn_GetAttributes(WHT.numWareHouseItemId,I.bitSerialized) AS [vcAttributes]'
										  + @strSQL
										  + '  from item I 
										  INNER JOIN
											dbo.Vendor V
										  ON
											V.numItemCode = I.numItemCode
										  Left join 
											DivisionMaster D              
										  ON 
											V.numVendorID=D.numDivisionID  
										  LEFT join 
											CompanyInfo C  
										  ON 
											C.numCompanyID=D.numCompanyID    
										  LEFT JOIN
											dbo.WareHouseItems WHT
										  ON
											WHT.numItemID = I.numItemCode AND
											WHT.numWareHouseItemID = (
																		SELECT 
																			TOP 1 numWareHouseItemID 
																		FROM 
																			dbo.WareHouseItems 
																		WHERE 
																			dbo.WareHouseItems.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' 
																			AND dbo.WareHouseItems.numItemID = I.numItemCode 
																			AND (dbo.WareHouseItems.numWareHouseID = ' +  CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20))  + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0)' +
																			CASE LEN(@vcWareHouseSearch)
																			WHEN 0 THEN ''
																			ELSE ' AND (' + @vcWareHouseSearch  + ' OR 1=1)' 
																			END
																			+
																	')
										       
										  WHERE (
													(I.charItemType <> ''P'') OR 
													((SELECT COUNT(*) FROM dbo.WareHouseItems WHERE numItemID = I.numItemCode AND numDomainID = '
										 + CONVERT(VARCHAR(20), @numDomainID)
										 + ' AND numWareHouseID =  ' +  CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20))  + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0) > 0)) AND ISNULL(I.Isarchieve,0) <> 1 and ISNULL(I.bitKitParent,0) = 0 And I.charItemType NOT IN(''A'') AND V.numVendorID='
										 + CONVERT(VARCHAR(20), @numDivisionID)
										 + ' and ('+ CASE LEN(@vcVendorSearch)
																			WHEN 0 THEN ''
																			ELSE REPLACE(@vcVendorSearch,'dbo.Vendor','V') + ' OR '
																			END + @strSearch + ') ' 

							--- added Asset validation  by sojan
                            IF @tintSearchOrderCustomerHistory = 1
                                AND @numDivisionID > 0
                                BEGIN
                                    SET @strSQL = @strSQL
                                        + ' and I.numItemCode in (select distinct oppI.numItemCode from 
									OpportunityMaster oppM join OpportunityItems oppI on oppM.numOppId=oppI.numOppId where oppM.numDomainID='
                                        + CONVERT(VARCHAR(20), @numDomainID)
                                        + ' and oppM.numDivisionId='
                                        + CONVERT(VARCHAR(20), @numDivisionId)
                                        + ')'
                                END


                            INSERT  INTO @TableRowCount
                                    EXEC
                                        ( 'SELECT COUNT(*) FROM ( ' + @strSQL
                                          + ') as t2'
                                        );
                            SELECT  @TotalCount = Value
                            FROM    @TableRowCount;

                            SET @strNewQuery = 'select * from (' + @strSQL
                                + ') as t where SRNO> '
                                + CAST(( ( @numPageIndex - 1 ) * @numPageSize ) AS VARCHAR(10))
                                + ' and SRNO < '
                                + CAST(( ( @numPageIndex * @numPageSize ) + 1 ) AS VARCHAR(10))
                                + ' order by  vcItemName'
                            
							--PRINT @strNewQuery
							EXEC(@strNewQuery)
                        END
                    ELSE
                        SELECT  0  
            END
    END
ELSE
    SELECT  0  

SELECT  *
FROM    #Temp1
WHERE   vcDbColumnName NOT IN ( 'vcPartNo', 'vcBarCode', 'vcWHSKU' )ORDER BY tintOrder 
   


IF OBJECT_ID('dbo.Scores', 'U') IS NOT NULL
  DROP TABLE dbo.Scores

IF OBJECT_ID('tempdb..#Temp1') IS NOT NULL
BEGIN
    DROP TABLE #Temp1
END

IF OBJECT_ID('tempdb..#tempSearch') IS NOT NULL
BEGIN
    DROP TABLE #tempSearch
END

IF OBJECT_ID('tempdb..#tempItemCode') IS NOT NULL
BEGIN
    DROP TABLE #tempItemCode
END


    END
/*
declare @p7 int
set @p7=0
exec USP_ItemList1 @ItemClassification=0,@KeyWord='',@IsKit=0,@SortChar='0',@CurrentPage=1,@PageSize=10,@TotRecs=@p7 
output,@columnName='vcItemName',@columnSortOrder='Asc',@numDomainID=1,@ItemType=' ',@bitSerialized=0,@numItemGroup=0,@bitAssembly=0,@numUserCntID=1,@Where='',@IsArchive=0
select @p7
*/

GO
/****** Object:  StoredProcedure [dbo].[USP_ItemList1]    Script Date: 05/07/2009 18:14:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Created By Anoop Jayaraj                                        
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_itemlist1' ) 
    DROP PROCEDURE usp_itemlist1
GO
CREATE PROCEDURE [dbo].[USP_ItemList1]
    @ItemClassification AS NUMERIC(9) = 0,
    @KeyWord AS VARCHAR(1000) = '',
    @IsKit AS TINYINT,
    @SortChar CHAR(1) = '0',
    @CurrentPage INT,
    @PageSize INT,
    @TotRecs INT OUTPUT, 
    @columnName AS VARCHAR(50),
    @columnSortOrder AS VARCHAR(10),
    @numDomainID AS NUMERIC(9) = 0,
    @ItemType AS CHAR(1),
    @bitSerialized AS BIT,
    @numItemGroup AS NUMERIC(9),
    @bitAssembly AS BIT,
    @numUserCntID AS NUMERIC(9),
    @Where	AS VARCHAR(MAX),
    @IsArchive AS BIT = 0,
    @byteMode AS TINYINT
AS 
	
	 DECLARE @Nocolumns AS TINYINT               
    SET @Nocolumns = 0                
 
    SELECT  @Nocolumns = ISNULL(SUM(TotalRow), 0)
    FROM    ( SELECT    COUNT(*) TotalRow
              FROM      View_DynamicColumns
              WHERE     numFormId = 21
                        AND numUserCntID = @numUserCntID
                        AND numDomainID = @numDomainID
                        AND tintPageType = 1 AND ISNULL(numRelCntType,0)=0
              UNION
              SELECT    COUNT(*) TotalRow
              FROM      View_DynamicCustomColumns
              WHERE     numFormId = 21
                        AND numUserCntID = @numUserCntID
                        AND numDomainID = @numDomainID
                        AND tintPageType = 1 AND ISNULL(numRelCntType,0)=0
            ) TotalRows
               
 
 if @Nocolumns=0
BEGIN
INSERT INTO DycFormConfigurationDetails (numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth,numViewID)
select 21,numFieldId,0,Row_number() over(order by tintRow desc),@numDomainID,@numUserCntID,0,1,0,intColumnWidth,0
 FROM    View_DynamicDefaultColumns
                    WHERE   numFormId = 21
                            AND bitDefault = 1
                            AND ISNULL(bitSettingField, 0) = 1
                            AND numDomainID = @numDomainID
                    ORDER BY tintOrder ASC 
END

    CREATE TABLE #tempForm
        (
          tintOrder TINYINT,
          vcDbColumnName NVARCHAR(50),
          vcFieldName NVARCHAR(50),
          vcAssociatedControlType NVARCHAR(50),
          vcListItemType CHAR(3),
          numListID NUMERIC(9),
          vcLookBackTableName VARCHAR(50),
          bitCustomField BIT,
          numFieldId NUMERIC,
          bitAllowSorting BIT,
          bitAllowEdit BIT,
          bitIsRequired BIT,
          bitIsEmail BIT,
          bitIsAlphaNumeric BIT,
          bitIsNumeric BIT,
          bitIsLengthValidation BIT,
          intMaxLength INT,
          intMinLength INT,
          bitFieldMessage BIT,
          vcFieldMessage VARCHAR(500),
          ListRelID NUMERIC(9)
        )


          

            INSERT  INTO #tempForm
                    SELECT  tintRow + 1 AS tintOrder,
                            vcDbColumnName,
                            ISNULL(vcCultureFieldName, vcFieldName),
                            vcAssociatedControlType,
                            vcListItemType,
                            numListID,
                            vcLookBackTableName,
                            bitCustom,
                            numFieldId,
                            bitAllowSorting,
                            bitAllowEdit,
                            bitIsRequired,
                            bitIsEmail,
                            bitIsAlphaNumeric,
                            bitIsNumeric,
                            bitIsLengthValidation,
                            intMaxLength,
                            intMinLength,
                            bitFieldMessage,
                            vcFieldMessage vcFieldMessage,
                            ListRelID
                    FROM    View_DynamicColumns
                    WHERE   numFormId = 21
                            AND numUserCntID = @numUserCntID
                            AND numDomainID = @numDomainID
                            AND tintPageType = 1
                            AND ISNULL(bitSettingField, 0) = 1
                            AND ISNULL(bitCustom, 0) = 0
                            AND ISNULL(numRelCntType,0)=0
                    UNION
                    SELECT  tintRow + 1 AS tintOrder,
                            vcDbColumnName,
                            vcFieldName,
                            vcAssociatedControlType,
                            '' AS vcListItemType,
                            numListID,
                            '',
                            bitCustom,
                            numFieldId,
                            bitAllowSorting,
                            bitAllowEdit,
                            bitIsRequired,
                            bitIsEmail,
                            bitIsAlphaNumeric,
                            bitIsNumeric,
                            bitIsLengthValidation,
                            intMaxLength,
                            intMinLength,
                            bitFieldMessage,
                            vcFieldMessage,
                            ListRelID
                    FROM    View_DynamicCustomColumns
                    WHERE   numFormId = 21
                            AND numUserCntID = @numUserCntID
                            AND numDomainID = @numDomainID
                            AND tintPageType = 1
                            AND ISNULL(bitCustom, 0) = 1
                            AND ISNULL(numRelCntType,0)=0
                    ORDER BY tintOrder ASC      
            
    
            
	IF @byteMode=0
	BEGIN
		
	IF @columnName = 'OnHand' 
        SET @columnName = 'numOnHand'
    ELSE IF @columnName = 'Backorder' 
        SET @columnName = 'numBackOrder'
    ELSE IF @columnName = 'OnOrder' 
        SET @columnName = 'numOnOrder'
    ELSE IF @columnName = 'OnAllocation' 
        SET @columnName = 'numAllocation'
    ELSE IF @columnName = 'Reorder' 
        SET @columnName = 'numReorder'
    ELSE IF @columnName = 'monStockValue' 
        SET @columnName = 'sum(numOnHand) * Isnull(monAverageCost,0)'
    ELSE IF @columnName = 'ItemType' 
        SET @columnName = 'charItemType'

  
    DECLARE @column AS VARCHAR(50)              
    SET @column = @columnName              
    DECLARE @join AS VARCHAR(400)                  
    SET @join = ''           
    IF @columnName LIKE '%Cust%' 
        BEGIN                
			DECLARE @CustomFieldType AS VARCHAR(10)
			DECLARE @fldId AS VARCHAR(10)
			
			SET @fldId = REPLACE(@columnName, 'Cust', '')
			SELECT TOP 1 @CustomFieldType = ISNULL(vcAssociatedControlType,'') FROM View_DynamicCustomColumns WHERE numFieldID = @fldId
            
            IF ISNULL(@CustomFieldType,'') = 'SelectBox' OR ISNULL(@CustomFieldType,'') = 'ListBox'
				BEGIN
					SET @join = @join + ' left Join CFW_FLD_Values_Item CFW on CFW.RecId=item.numItemCode and CFW.fld_id= ' + @fldId + ' ' 
					SET @join = @join + ' left Join ListDetails LstCF on LstCF.numListItemID = CFW.Fld_Value  '            
					SET @columnName = ' LstCF.vcData ' 	
				END
			ELSE
				BEGIN
					SET @join = ' left Join CFW_FLD_Values_Item CFW on CFW.RecId=item.numItemCode and CFW.fld_id= ' + @fldId + ' '                                                         
					SET @columnName = 'CFW.Fld_Value'                	
				END
        END                                                
           
                                      
    DECLARE @firstRec AS INTEGER                                        
    DECLARE @lastRec AS INTEGER                                        
    SET @firstRec = ( @CurrentPage - 1 ) * @PageSize                                        
    SET @lastRec = ( @CurrentPage * @PageSize + 1 )   

    DECLARE @bitLocation AS BIT  
    SELECT  @bitLocation = ( CASE WHEN COUNT(*) > 0 THEN 1
                                  ELSE 0
                             END )
    FROM    View_DynamicColumns
    WHERE   numFormId = 21
            AND numUserCntID = @numUserCntID
            AND numDomainID = @numDomainID
            AND tintPageType = 1
            AND bitCustom = 0
            AND ISNULL(bitSettingField, 0) = 1
            AND vcDbColumnName = 'vcWareHouse'
                                                                   
                                        
    DECLARE @strSql AS VARCHAR(8000)
    DECLARE @strWhere AS VARCHAR(8000)
    SET @strWhere = ' AND 1=1'
    
      SET @strSql = '
declare @numDomain numeric
set @numDomain = ' + CONVERT(VARCHAR(15), @numDomainID) + ';
    INSERT INTO #tempItemList select numItemCode'
  
--    SET @strSql = @strSql + ' from item                     
        
        
    IF @bitAssembly = '1' 
        SET @strWhere = @strWhere + ' and bitAssembly= ' + CONVERT(VARCHAR(2), @bitAssembly) + ''                                    
        
    ELSE 
        IF @IsKit = '1' 
            SET @strWhere = @strWhere + ' and (bitAssembly=0 or  bitAssembly is null) and bitKitParent= ' + CONVERT(VARCHAR(2), @IsKit) + ''               

    IF @ItemType = 'R' 
        SET @strWhere = @strWhere + ' and numItemClassification=(select numRentalItemClass from domain where numDomainId=' + CONVERT(VARCHAR(20), @numDomainID) + ')'            
    ELSE 
        IF @ItemType <> ''  
            SET @strWhere = @strWhere + ' and charItemType= ''' + @ItemType + ''''            

    IF @bitSerialized = 1 
        SET @strWhere = @strWhere + ' and (bitSerialized=1 or bitLotNo=1)'                                                   
        
    IF @SortChar <> '0' 
        SET @strWhere = @strWhere + ' and vcItemName like ''' + @SortChar + '%'''                                       
        
    IF @ItemClassification <> '0' 
        SET @strWhere = @strWhere + ' and numItemClassification=' + CONVERT(VARCHAR(15), @ItemClassification)    
    
    IF @IsArchive = 0 
        SET @strWhere = @strWhere + ' AND ISNULL(Item.IsArchieve,0) = 0'
        
    IF @KeyWord <> '' 
        BEGIN 
            IF CHARINDEX('vcCompanyName', @KeyWord) > 0 
                BEGIN
                    SET @strWhere = @strWhere + ' and item.numItemCode in (select Vendor.numItemCode from Vendor join 
											  divisionMaster div on div.numdivisionid=Vendor.numVendorid 
											  join companyInfo com  on com.numCompanyID=div.numCompanyID  
											  WHERE Vendor.numDomainID=' + CONVERT(VARCHAR(15), @numDomainID) + '  
											  and Vendor.numItemCode= item.numItemCode and com.' + @KeyWord + ')' 
											  
					SET @join = @join + ' LEFT JOIN divisionmaster div on numVendorid=div.numDivisionId '   		
                END
            ELSE 
                SET @strWhere = @strWhere + ' and ' + @KeyWord 
        END
        
    IF @numItemGroup > 0 
        SET @strWhere = @strWhere + ' and numItemGroup = ' + CONVERT(VARCHAR(20), @numItemGroup)    
               
    IF @numItemGroup = -1 
        SET @strWhere = @strWhere + ' and numItemGroup in (select numItemGroupID from ItemGroups where numDomainID=' + CONVERT(VARCHAR(20), @numDomainID) + ')'                                      
        
    SET @strWhere = @strWhere + ISNULL(@Where,'') + ' group by numItemCode '  
  
    IF @bitLocation = 1 
        SET @strWhere = @strWhere + '  ,vcWarehouse'
        
        SET @join = @join + ' LEFT JOIN WareHouseItems ON numItemID = numItemCode '
        SET @join = @join + ' LEFT JOIN Warehouses W ON W.numWareHouseID = WareHouseItems.numWareHouseID '
        
    IF @columnName <> 'sum(numOnHand) * Isnull(monAverageCost,0)' 
        BEGIN
            SET @strWhere = @strWhere + ',' + @columnName   
            
            IF CHARINDEX('LEFT JOIN WareHouseItems',@join) = 0
            BEGIN
				SET @join = @join + ' LEFT JOIN WareHouseItems ON numItemID = numItemCode '
			END	
        END
    
  
    SET @strSql = @strSql + ' FROM Item  ' + @join + '   
	LEFT JOIN ListDetails LD ON LD.numListItemID = Item.numShipClass
	LEFT JOIN ItemCategory IC ON IC.numItemID = Item.numItemCode
	WHERE Item.numDomainID= @numDomain
	AND ( WareHouseItems.numDomainID = ' + CONVERT(VARCHAR(15), @numDomainID) + ' OR WareHouseItems.numWareHouseItemID IS NULL) ' + @strWhere
    
    SET @strSql = @strSql + ' ORDER BY ' + @columnName + ' ' + @columnSortOrder 
 
	PRINT @strSql

	CREATE TABLE #tempItemList (RowNo INT IDENTITY(1,1) NOT NULL,numItemCode NUMERIC(18,0))
	 EXEC (@strSql)
	DELETE FROM #tempItemList WHERE RowNo NOT IN (SELECT MIN(RowNo) FROM #tempItemList GROUP BY numItemCode)

	CREATE TABLE #tempItemList1 (RunningCount INT IDENTITY(1,1) NOT NULL,numItemCode NUMERIC(18,0),TotalRowCount INT)
	INSERT INTO #tempItemList1 (numItemCode) SELECT numItemCode FROM #tempItemList
	UPDATE #tempItemList1 SET TotalRowCount=(SELECT COUNT(*) FROM #tempItemList1)
	
	SET @strSql = ' select                    
 min(RunningCount) as RunningCount,
 min(TotalRowCount) as TotalRowCount,I.numItemCode,I.vcItemName,I.txtItemDesc,I.charItemType,                                     
case when charItemType=''P'' then ''Inventory Item'' when charItemType=''N'' then ''Non Inventory Item'' when charItemType=''S'' then ''Service'' when charItemType=''A'' then ''Accessory'' end as ItemType,                                      
CASE WHEN bitKitParent = 1 THEN ISNULL(dbo.fn_GetKitInventory(I.numItemCode),0) ELSE isnull(sum(numOnHand),0) END  as numOnHand,                  
isnull(sum(numOnOrder),0) as numOnOrder,                                      
isnull(sum(numReorder),0) as numReorder,                  
isnull(sum(numBackOrder),0) as numBackOrder,                  
isnull(sum(numAllocation),0) as numAllocation,                  
vcCompanyName,  
CAST(ISNULL(monAverageCost,0) AS DECIMAL(10,2)) AS monAverageCost ,
CAST(ISNULL(monAverageCost,0) * SUM(numOnHand) AS DECIMAL(10,2)) AS monStockValue,
I.vcModelID,I.monListPrice,I.vcManufacturer,I.numBarCodeId,I.fltLength,I.fltWidth,I.fltHeight,I.fltWeight,I.vcSKU,LD.vcData AS numShipClass ' 

   IF @bitLocation = 1 
        SET @strSql = @strSql + '  ,vcWarehouse'  

SET @strSql = @strSql + ' INTO #tblItem FROM #tempItemList1 as tblItem JOIN ITEM I ON tblItem.numItemCode=I.numItemCode 
left join WareHouseItems on numItemID=I.numItemCode                                  
left join divisionmaster div  on I.numVendorid=div.numDivisionId                                    
left join companyInfo com on com.numCompanyid=div.numcompanyID   
left join Warehouses W  on W.numWareHouseID=WareHouseItems.numWareHouseID 
LEFT JOIN ListDetails LD ON LD.numListItemID = i.numShipClass
    where  RunningCount >' + CONVERT(VARCHAR(15), @firstRec)
        + ' and RunningCount < ' + CONVERT(VARCHAR(15), @lastRec)
          + ' group by I.numItemCode,vcItemName,txtItemDesc,charItemType,vcCompanyName,vcModelID,monListPrice,vcManufacturer,numBarCodeId,fltLength,fltWidth,fltHeight,fltWeight,monAverageCost,I.vcSKU,LD.vcData,bitKitParent '  
          
              IF @bitLocation = 1 
        SET @strSql = @strSql + '  ,vcWarehouse'  
        
        SET @strSql = @strSql + ' order by RunningCount '   
          
    DECLARE @tintOrder AS TINYINT                                                  
    DECLARE @vcFieldName AS VARCHAR(50)                                                  
    DECLARE @vcListItemType AS VARCHAR(3)                                             
    DECLARE @vcListItemType1 AS VARCHAR(1)                                                 
    DECLARE @vcAssociatedControlType VARCHAR(10)                                                  
    DECLARE @numListID AS NUMERIC(9)                                                  
    DECLARE @vcDbColumnName VARCHAR(20)                      
    DECLARE @WhereCondition VARCHAR(2000)                       
    DECLARE @vcLookBackTableName VARCHAR(2000)                
    DECLARE @bitCustom AS BIT                  
    DECLARE @numFieldId AS NUMERIC  
    DECLARE @bitAllowSorting AS CHAR(1)   
    DECLARE @bitAllowEdit AS CHAR(1)                   
                 
    SET @tintOrder = 0                                                  
    SET @WhereCondition = ''                 
                   
   
   
    SET @strSql = @strSql
        + ' select TotalRowCount,RunningCount,temp.numItemCode,vcItemName,txtItemDesc,charItemType,ItemType,numOnHand, LD.vcData AS numShipClass,                  
				   numOnOrder,numReorder,numBackOrder,numAllocation,vcCompanyName,vcModelID,monListPrice,vcManufacturer,numBarCodeId,fltLength,fltWidth,fltHeight,
				   fltWeight,(Select SUM(WO.numQtyItemsReq) from WorkOrder WO where WO.numItemCode=temp.numItemCode and WO.numWOStatus=0) as WorkOrder,
				   monAverageCost, monStockValue,vcSKU'     
  
    IF @bitLocation = 1 
        SET @strSql = @strSql + '  ,vcWarehouse'                                               

    DECLARE @ListRelID AS NUMERIC(9) 
  
    SELECT TOP 1
            @tintOrder = tintOrder + 1,
            @vcDbColumnName = vcDbColumnName,
            @vcFieldName = vcFieldName,
            @vcAssociatedControlType = vcAssociatedControlType,
            @vcListItemType = vcListItemType,
            @numListID = numListID,
            @vcLookBackTableName = vcLookBackTableName,
            @bitCustom = bitCustomField,
            @numFieldId = numFieldId,
            @bitAllowSorting = bitAllowSorting,
            @bitAllowEdit = bitAllowEdit,
            @ListRelID = ListRelID
    FROM    #tempForm --WHERE bitCustomField=1
    ORDER BY tintOrder ASC            

    WHILE @tintOrder > 0                                                  
        BEGIN                                                  
            IF @bitCustom = 0
               BEGIN
               PRINT @vcDbColumnName
			   		IF @vcDbColumnName = 'vcPathForTImage'
		   			BEGIN
					   	SET @strSql = @strSql + ',ISNULL(II.vcPathForTImage,'''') AS [vcPathForTImage]'                   
                        SET @WhereCondition = @WhereCondition + ' LEFT JOIN ItemImages II ON II.numItemCode = temp.numItemCode AND bitDefault = 1'
					END
			   END                              
			   
            ELSE IF @bitCustom = 1 
                BEGIN      
                  
                    SELECT  @vcFieldName = FLd_label,
                            @vcAssociatedControlType = fld_type,
                            @vcDbColumnName = 'Cust'
                            + CONVERT(VARCHAR(10), Fld_Id)
                    FROM    CFW_Fld_Master
                    WHERE   CFW_Fld_Master.Fld_Id = @numFieldId                 
     
              
--    print @vcAssociatedControlType                
                    IF @vcAssociatedControlType = 'TextBox'
                        OR @vcAssociatedControlType = 'TextArea' 
                        BEGIN                
                   
                            SET @strSql = @strSql + ',CFW'  + CONVERT(VARCHAR(3), @tintOrder)  + '.Fld_Value  [' + @vcFieldName + '~' + @vcDbColumnName + ']'                   
                            SET @WhereCondition = @WhereCondition + ' left Join CFW_FLD_Values_Item CFW' + CONVERT(VARCHAR(3), @tintOrder)
                                + '                 
    on CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
                                + CONVERT(VARCHAR(10), @numFieldId)
                                + 'and CFW' + CONVERT(VARCHAR(3), @tintOrder)
                                + '.RecId=temp.numItemCode   '                                                         
                        END   
                    ELSE 
                        IF @vcAssociatedControlType = 'CheckBox' 
                            BEGIN            
               
                                SET @strSql = @strSql
                                    + ',case when isnull(CFW'
                                    + CONVERT(VARCHAR(3), @tintOrder)
                                    + '.Fld_Value,0)=0 then ''No'' when isnull(CFW'
                                    + CONVERT(VARCHAR(3), @tintOrder)
                                    + '.Fld_Value,0)=1 then ''Yes'' end   ['
                                    + @vcFieldName + '~' + @vcDbColumnName
                                    + ']'              
 
                                SET @WhereCondition = @WhereCondition
                                    + ' left Join CFW_FLD_Values_Item CFW'
                                    + CONVERT(VARCHAR(3), @tintOrder)
                                    + '             
    on CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
                                    + CONVERT(VARCHAR(10), @numFieldId)
                                    + 'and CFW'
                                    + CONVERT(VARCHAR(3), @tintOrder)
                                    + '.RecId=temp.numItemCode   '                                                     
                            END                
                        ELSE 
                            IF @vcAssociatedControlType = 'DateField' 
                                BEGIN              
                   
                                    SET @strSql = @strSql
                                        + ',dbo.FormatedDateFromDate(CFW'
                                        + CONVERT(VARCHAR(3), @tintOrder)
                                        + '.Fld_Value,'
                                        + CONVERT(VARCHAR(10), @numDomainId)
                                        + ')  [' + @vcFieldName + '~'
                                        + @vcDbColumnName + ']'                   
                                    SET @WhereCondition = @WhereCondition
                                        + ' left Join CFW_FLD_Values_Item CFW'
                                        + CONVERT(VARCHAR(3), @tintOrder)
                                        + '                 
    on CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
                                        + CONVERT(VARCHAR(10), @numFieldId)
                                        + 'and CFW'
                                        + CONVERT(VARCHAR(3), @tintOrder)
                                        + '.RecId=temp.numItemCode   '                                                         
                                END                
                            ELSE 
                                IF @vcAssociatedControlType = 'SelectBox' 
                                    BEGIN                
                                        SET @vcDbColumnName = 'Cust'
                                            + CONVERT(VARCHAR(10), @numFieldId)                
                                        SET @strSql = @strSql + ',L'
                                            + CONVERT(VARCHAR(3), @tintOrder)
                                            + '.vcData' + ' [' + @vcFieldName
                                            + '~' + @vcDbColumnName + ']'                                                          
                                        SET @WhereCondition = @WhereCondition
                                            + ' left Join CFW_FLD_Values_Item CFW'
                                            + CONVERT(VARCHAR(3), @tintOrder)
                                            + '                 
     on CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
                                            + CONVERT(VARCHAR(10), @numFieldId)
                                            + 'and CFW'
                                            + CONVERT(VARCHAR(3), @tintOrder)
                                            + '.RecId=temp.numItemCode    '                                                         
                                        SET @WhereCondition = @WhereCondition
                                            + ' left Join ListDetails L'
                                            + CONVERT(VARCHAR(3), @tintOrder)
                                            + ' on L'
                                            + CONVERT(VARCHAR(3), @tintOrder)
                                            + '.numListItemID=CFW'
                                            + CONVERT(VARCHAR(3), @tintOrder)
                                            + '.Fld_Value'                
                                    END                 
                END          
  
            SELECT TOP 1
                    @tintOrder = tintOrder + 1,
                    @vcDbColumnName = vcDbColumnName,
                    @vcFieldName = vcFieldName,
                    @vcAssociatedControlType = vcAssociatedControlType,
                    @vcListItemType = vcListItemType,
                    @numListID = numListID,
                    @vcLookBackTableName = vcLookBackTableName,
                    @bitCustom = bitCustomField,
                    @numFieldId = numFieldId,
                    @bitAllowSorting = bitAllowSorting,
                    @bitAllowEdit = bitAllowEdit,
                    @ListRelID = ListRelID
            FROM    #tempForm
            WHERE   tintOrder > @tintOrder - 1 --AND bitCustomField=1
            ORDER BY tintOrder ASC            
 
            IF @@rowcount = 0 
                SET @tintOrder = 0 
            
        END                       
      
    PRINT @bitLocation
    SET @strSql = @strSql + ' from #tblItem temp
			LEFT JOIN ListDetails LD ON LD.vcData = temp.numShipClass AND LD.numListID=461 and LD.numDomainID = '  + CONVERT(VARCHAR(20), @numDomainID) + ' '
		+ @WhereCondition
        + ' where  RunningCount >' + CONVERT(VARCHAR(15), @firstRec)
        + ' and RunningCount < ' + CONVERT(VARCHAR(15), @lastRec)
        + ' order by RunningCount'   

    SET @strSql = REPLACE(@strSql, '|', ',') 
    PRINT @strSql
    EXEC ( @strSql)  

    
                              
 
DROP TABLE #tempItemList
DROP TABLE #tempItemList1

END

UPDATE  #tempForm
    SET     vcDbColumnName = CASE WHEN bitCustomField = 1
                                  THEN vcFieldName + '~' + vcDbColumnName
                                  ELSE vcDbColumnName END
                                  
   SELECT  * FROM #tempForm

    DROP TABLE #tempForm
/****** Object:  StoredProcedure [dbo].[USP_Journal_EntryListForBankReconciliation]    Script Date: 05/07/2009 22:01:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Chintan
-- EXEC USP_Journal_EntryListForBankReconciliation 2233,'','',0,1
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Journal_EntryListForBankReconciliation')
DROP PROCEDURE USP_Journal_EntryListForBankReconciliation
GO
CREATE PROCEDURE [dbo].[USP_Journal_EntryListForBankReconciliation]
               @numChartAcntId AS NUMERIC(9),
               @tintFlag       TINYINT=0,
               @numDomainId    AS NUMERIC(9),
               @bitDateHide AS BIT=0,
               @numReconcileID AS NUMERIC(9),
               @tintReconcile AS TINYINT=0 --0:not Reconcile 1:Reconsile for particular ReconID 2:All reconcile for particular ReconID and all non reconcile
AS
  BEGIN
  
  DECLARE @numCreditAmt NUMERIC(9)
  DECLARE @numDebitAmt NUMERIC(9)
  DECLARE @dtStatementDate AS DATETIME
  
  SELECT @dtStatementDate=dtStatementDate FROM BankReconcileMaster WHERE numReconcileID=@numReconcileID
  
  create table #TempBankRecon
  (numChartAcntId numeric(18),
  numTransactionId numeric(18),
  bitReconcile bit,
  EntryDate datetime,
  CompanyName varchar(500),
  Deposit money,
  Payment money,
  Memo  varchar(250),
  TransactionType varchar(100),
  JournalId numeric(18),
  numCheckHeaderID numeric(18),
  vcReference varchar(1000),
  tintOppType INTEGER,bitCleared BIT,numCashCreditCardId NUMERIC(18),numOppId NUMERIC(18),numOppBizDocsId NUMERIC(18),
  numDepositId NUMERIC(18),numCategoryHDRID NUMERIC(9),tintTEType TINYINT,numCategory NUMERIC(18),numUserCntID NUMERIC(18),dtFromDate DATETIME,
  numBillId NUMERIC(18),numBillPaymentID NUMERIC(18),numLandedCostOppId NUMERIC(18))
  
  IF (@tintFlag = 1)
	SET @numDebitAmt = 0
  ELSE 	IF (@tintFlag = 2)
	SET @numCreditAmt = 0 
  
  insert into #TempBankRecon
  
    SELECT GJD.numChartAcntId AS numChartAcntId,
		   GJD.numTransactionId,
		   ISNULL(GJD.bitReconcile,0) bitReconcile,	
           GJH.datEntry_Date AS EntryDate,
           CI.vcCompanyName AS CompanyName,
			(case when GJD.numCreditAmt=0 then GJD.numdebitAmt  end) as Deposit,        
			(case when GJD.numdebitAmt=0 then GJD.numCreditAmt end) as Payment,   
           --GJH.[datEntry_Date]  as EntryDate,
           GJD.varDescription as Memo,
            case when isnull(GJH.numCheckHeaderID,0) <> 0 THEN case (select isnull(OBD.numCheckNo,0) from OpportunityBizDocsPaymentDetails OBD where OBD.numBizDocsPaymentDetId=isnull(GJH.numBizDocsPaymentDetId,0))
					when 0 then 'Cash' else 'Checks' end 			
			when isnull(GJH.numCashCreditCardId,0) <> 0 And CCCD.bitMoneyOut=0 then 'Cash'
			when isnull(GJH.numCashCreditCardId,0) <> 0 And CCCD.bitMoneyOut=1 And CCCD.bitChargeBack=1  then 'Charge'
			when isnull(GJH.numOppId,0) <> 0 And isnull(GJH.numOppBizDocsId,0) <> 0 And isnull(GJH.numBizDocsPaymentDetId,0)=0 And Opp.tintOppType=1 then 'BizDocs Invoice'
			when isnull(GJH.numOppId,0) <> 0 And isnull(GJH.numOppBizDocsId,0) <> 0 And isnull(GJH.numBizDocsPaymentDetId,0)=0 And Opp.tintOppType=2 then 'BizDocs Purchase'
			when isnull(GJH.numDepositId,0) <> 0 then 'Deposit'
			when isnull(GJH.numBizDocsPaymentDetId,0)<>0 And Opp.tintOppType=1 then 'Receive Amt'
			when isnull(GJH.numBizDocsPaymentDetId,0)<>0 And Opp.tintOppType=2 then 'Vendor Amt'
			when isnull(GJH.numCategoryHDRID,0)<>0 then 'Time And Expenses'
			When ISNULL(GJH.numBillId,0) <>0 AND isnull(BH.bitLandedCost,0)=1 then 'Landed Cost' 
			When ISNULL(GJH.numBillId,0) <>0 then 'Bill' 
			When ISNULL(GJH.numBillPaymentID,0) <>0 then 'Pay Bill' 
			When GJH.numJournal_Id <>0 then 'Journal' End  as TransactionType,
			GJD.numJournalId AS JournalId,
--			case (select isnull(OBD.numCheckNo,0) from OpportunityBizDocsPaymentDetails OBD where OBD.numBizDocsPaymentDetId=isnull(GJH.numBizDocsPaymentDetId,0))
--			when 0 then null else dbo.fn_GetJournalCheqDetails(GJH.numCheckId,@numDomainId) end 
			isnull(GJH.numCheckHeaderID,0) as numCheckHeaderID,
			ISNULL((SELECT Narration FROM VIEW_BIZPAYMENT WHERE numDomainID=GJH.numDomainID AND numBizDocsPaymentDetId = isnull(GJH.numBizDocsPaymentDetId,0)),GJH.varDescription) vcReference,
			isnull(Opp.tintOppType,0),ISNULL(GJD.bitCleared,0) AS bitCleared,
			ISNULL(GJH.numCashCreditCardId,0) AS numCashCreditCardId,ISNULL(GJH.numOppId,0) AS numOppId,
			isnull(GJH.numOppBizDocsId,0) AS numOppBizDocsId,isnull(GJH.numDepositId,0) AS numDepositId,
			isnull(GJH.numCategoryHDRID,0) AS numCategoryHDRID,TE.tintTEType,TE.numCategory,TE.numUserCntID,TE.dtFromDate,ISNULL(GJH.numBillId,0) AS numBillID,ISNULL(GJH.numBillPaymentID,0) AS numBillPaymentID,
			Case When isnull(BH.bitLandedCost,0)=1 then  isnull(BH.numOppId,0) else 0 end AS numLandedCostOppId
    FROM   General_Journal_Header GJH
           INNER JOIN General_Journal_Details GJD
             ON GJH.numJournal_Id = GJD.numJournalId
           LEFT OUTER JOIN DivisionMaster AS DM
             ON GJD.numCustomerId = DM.numDivisionID
           LEFT OUTER JOIN CompanyInfo AS CI
             ON DM.numCompanyID = CI.numCompanyId
            Left outer join CashCreditCardDetails CCCD on GJH.numCashCreditCardId=CCCD.numCashCreditId        
			Left outer join (select * from OpportunityMaster WHERE tintOppType=1 ) Opp on GJH.numOppId=Opp.numOppId    
			LEFT OUTER JOIN TimeAndExpense TE ON GJH.numCategoryHDRID = TE.numCategoryHDRID
			LEFT OUTER JOIN BillHeader BH  ON ISNULL(GJH.numBillId,0) = BH.numBillId
    WHERE  
		   ((GJH.[datEntry_Date] <= @dtStatementDate AND @bitDateHide=1) OR @bitDateHide=0)
           AND GJD.numChartAcntId = @numChartAcntId
           AND GJD.numDomainId = @numDomainId
           AND (GJD.numCreditAmt <> @numCreditAmt OR @numCreditAmt IS null)
           AND (GJD.numDebitAmt <> @numDebitAmt OR @numDebitAmt IS null)
           AND 1=(CASE WHEN @tintReconcile=0 THEN CASE WHEN ISNULL(GJD.bitReconcile,0)=0 THEN 1 ELSE 0 END
					   WHEN @tintReconcile=1 THEN CASE WHEN ISNULL(GJD.numReconcileID,0)=@numReconcileID THEN 1 ELSE 0 END
					   WHEN @tintReconcile=2 THEN CASE WHEN  (ISNULL(GJD.bitReconcile,0)=1 AND ISNULL(GJD.numReconcileID,0)=@numReconcileID) OR ISNULL(GJD.bitReconcile,0)=0 THEN 1 ELSE 0 END END)
			ORDER BY GJH.datEntry_Date
           --and GJH.numCheckId is null
           --and GJH.numCheckId not in (select isnull(CD.numCheckId,0) from CheckDetails CD where CD.numDomainId=@numDomainId )
           --and CD.datCheckDate BETWEEN @dtDateFrom AND @dtToDate OR @dtDateFrom = '' OR @dtToDate = ''
   
   
   select * from #TempBankRecon /*where TransactionType  not in ('Checks')*/
--   union
--   --select * from #TempBankRecon where TransactionType   in ('Checks')
--   select numChartAcntId,0 as numTransactionId,bitReconcile ,EntryDate,CompanyName,SUM(isnull(Deposit,0)) as Deposit, SUM(isnull(Payment,0)) as Payment,
--   Memo,TransactionType,0 as JournalId,numCheckHeaderID,vcReference,tintOppType,bitCleared,numCashCreditCardId,numOppId,numOppBizDocsId,numDepositId,numCategoryHDRID,tintTEType,numCategory,numUserCntID,dtFromDate,numBillId,numBillPaymentID
--   from #TempBankRecon where TransactionType  in ('Checks')
--   group by numChartAcntId,bitReconcile,EntryDate,CompanyName,Memo,TransactionType,numCheckHeaderID,vcReference,tintOppType,bitCleared,numCashCreditCardId,numOppId,numOppBizDocsId,numDepositId,numCategoryHDRID,tintTEType,numCategory,numUserCntID,dtFromDate,numBillId,numBillPaymentID
--   ORDER BY EntryDate ASC
	
   drop table #TempBankRecon           
  END
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_LandedCostBillDetails' ) 
    DROP PROCEDURE USP_LandedCostBillDetails
GO
-- exec USP_GetBillPaymentDetails 3,1,0
CREATE PROCEDURE [dbo].[USP_LandedCostBillDetails]
    @numDomainID NUMERIC,
    @numOppID NUMERIC
AS 
    SET NOCOUNT ON
    SET TRANSACTION ISOLATION LEVEL READ COMMITTED
    
    SELECT  BH.[numBillID],
            BH.[numDivisionID],
            BH.[dtBillDate],
            BH.[numTermsID],
            BH.[dtDueDate],
            BH.[monAmountDue],
            BH.[monAmtPaid],
            BH.[bitIsPaid],
            BH.[vcMemo],
            BH.[vcReference],
			BD.[numBillDetailID],
            BD.[numExpenseAccountID],
            ISNULL(GJD.numTransactionId,0) AS numTransactionId_Bill,
            ISNULL(GJDD.numTransactionId,0) AS numTransactionId_BillDetail,
			BH.dtBillDate,(SELECT CAST([BillingTerms].[numTermsID] AS VARCHAR(20)) + '~' + CAST([BillingTerms].[numNetDueInDays] AS VARCHAR(20)) FROM BillingTerms WHERE numTermsID = BH.[numTermsID] AND numDomainId = BH.numDomainID)  AS [vcTermName],BH.dtDueDate,
			OM.[vcLanedCost]
    FROM    [dbo].[BillHeader] BH
			JOIN [dbo].[BillDetails] AS BD ON [BH].[numBillID] = [BD].[numBillID]
			JOIN [dbo].[OpportunityMaster] AS OM ON BH.[numOppId] = OM.[numOppId]
            LEFT OUTER JOIN General_Journal_Details GJD ON GJD.tintReferenceType=3 AND GJD.numReferenceID=BH.numBillID
           	LEFT OUTER JOIN General_Journal_Details GJDD ON GJDD.tintReferenceType=4 AND GJDD.numReferenceID=BD.numBillDetailID 
    WHERE  BH.numDomainID = @numDomainID AND BH.[numOppId] = @numOppID AND ISNULL(BH.[bitLandedCost],0) = 1
            
GO


GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_LandedCostExpenseAccountGet')
DROP PROCEDURE USP_LandedCostExpenseAccountGet
GO
CREATE PROC [dbo].[USP_LandedCostExpenseAccountGet] 
    @numDomainId NUMERIC(18, 0)
AS 
	SELECT [numLCExpenseId],LCEA. [numDomainId], LCEA.[vcDescription], LCEA.[numAccountId], COA.[vcAccountName]
	FROM   [dbo].[LandedCostExpenseAccount] LCEA JOIN [dbo].[Chart_Of_Accounts] AS COA
	ON [LCEA].[numAccountId] = [COA].[numAccountId] AND LCEA.[numDomainId] = COA.[numDomainId]
	WHERE  LCEA.numDomainId = @numDomainId


GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_LandedCostExpenseAccountSave' )
    DROP PROCEDURE [USP_LandedCostExpenseAccountSave]
GO
CREATE PROC [dbo].[USP_LandedCostExpenseAccountSave]
    @numDomainId NUMERIC(18, 0) ,
    @strItems VARCHAR(MAX)
AS
    BEGIN TRY
        BEGIN TRANSACTION 

        DECLARE @hDocItem INT
        IF CONVERT(VARCHAR(MAX), @strItems) <> ''
            BEGIN
                EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems

                SELECT  *
                INTO    #temp
                FROM    OPENXML (@hDocItem, '/NewDataSet/Table1', 2)
                         WITH (numLCExpenseId NUMERIC(18,0), vcDescription VARCHAR(50), numAccountId NUMERIC(18, 0)) X
     
                DELETE  FROM [dbo].[LandedCostExpenseAccount]
                WHERE   [numDomainId] = @numDomainId
                        AND numLCExpenseId NOT IN ( SELECT  numLCExpenseId
                                                    FROM    #temp
                                                    WHERE   numLCExpenseId > 0 )
	       
                UPDATE  LCEA
                SET     [vcDescription] = T.vcDescription ,
                        [numAccountId] = T.numAccountId
                FROM    [LandedCostExpenseAccount] LCEA
                        JOIN #temp T ON LCEA.numLCExpenseId = T.numLCExpenseId
                WHERE   T.numLCExpenseId > 0
                        AND LCEA.[numDomainId] = @numDomainId
			                   
                INSERT  INTO [dbo].[LandedCostExpenseAccount]
                        ( [numDomainId] ,
                          [vcDescription] ,
                          [numAccountId]
                        )
                        SELECT  @numDomainId ,
                                vcDescription ,
                                numAccountId
                        FROM    #temp
                        WHERE   numLCExpenseId = 0

                EXEC sp_xml_removedocument @hDocItem
            END

        COMMIT TRANSACTION

    END TRY
    BEGIN CATCH
        DECLARE @strMsg VARCHAR(200)
        SET @strMsg = ERROR_MESSAGE()
        IF ( @@TRANCOUNT > 0 )
            BEGIN
                RAISERROR ( @strMsg, 16, 1 );
                ROLLBACK TRAN
                RETURN 1
            END
    END CATCH
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_LandedCostOpportunityUpdate')
DROP PROCEDURE USP_LandedCostOpportunityUpdate
GO
CREATE PROC [dbo].[USP_LandedCostOpportunityUpdate] 
    @numDomainId NUMERIC(18, 0),
    @numOppId NUMERIC(18, 0),
	@vcLanedCost varchar(50)
AS 
	
		DECLARE @monLandedCostTotal money
		SELECT @monLandedCostTotal = SUM([BH].[monAmountDue]) FROM [dbo].[BillHeader] AS BH 
		WHERE [BH].[numDomainID]=@numDomainId AND [BH].[numOppId]=@numOppId AND [BH].[bitLandedCost] = 1

		--Update Total Landed Cost on Opp
		UPDATE [dbo].[OpportunityMaster] SET [monLandedCostTotal]=@monLandedCostTotal,
		[vcLanedCost]=@vcLanedCost
		WHERE [OpportunityMaster].[numDomainId]=@numDomainId AND [OpportunityMaster].[numOppId]=@numOppId

		--Calculate landed cost for each item and Update Landed Cost on Item
		SELECT [OI].[numoppitemtCode],
		CASE @vcLanedCost WHEN 'Amount' THEN [OI].[monTotAmount]
			WHEN 'Cubic Size' THEN ISNULL(I.[fltWidth],0) * ISNULL(I.[fltHeight],0) * ISNULL(I.[fltLength] ,0)
			WHEN 'Weight' THEN ISNULL(I.[fltWeight],0) END AS Total
		INTO #temp
		FROM [dbo].[OpportunityItems] AS OI JOIN [dbo].[Item] AS I ON OI.[numItemCode] = I.[numItemCode] 
		WHERE [OI].[numOppId] = @numOppId

		DECLARE @Total AS DECIMAL(18,2)
		SELECT @Total = SUM(Total) FROM [#temp] AS T

		UPDATE OI SET [monLandedCost] = CAST(ISNULL((@monLandedCostTotal * T.[Total]) / NULLIF(@Total,0),0) AS DECIMAL(18,2)) FROM [dbo].[OpportunityItems] AS OI
		JOIN #temp T ON OI.[numoppitemtCode] = T.[numoppitemtCode] WHERE OI.[numOppId] = @numOppId

		--Update Item Average Cost
		UPDATE I SET [monAverageCost] = ISNULL(I.[monAverageCost],0) + ISNULL(OI.[monLandedCost],0) - ISNULL(OI.monAvgLandedCost,0)
		FROM [dbo].[OpportunityItems] AS OI 
		JOIN [dbo].[Item] AS I ON OI.[numItemCode] = I.[numItemCode] WHERE OI.[numOppId] = @numOppId

		--Update monAvgLandedCost for tracking Purpose
		UPDATE OI SET monAvgLandedCost = OI.[monLandedCost] FROM [dbo].[OpportunityItems] AS OI WHERE OI.[numOppId] = @numOppId

		DROP TABLE #temp

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_LandedCostVendorsDelete')
DROP PROCEDURE USP_LandedCostVendorsDelete
GO
CREATE PROC [dbo].[USP_LandedCostVendorsDelete] 
    @numDomainId numeric(18, 0),
    @numLCVendorId numeric(18, 0)
AS 
	DELETE
	FROM   [dbo].[LandedCostVendors]
	WHERE  [numLCVendorId] = @numLCVendorId and numDomainId = @numDomainId
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_LandedCostVendorsGet')
DROP PROCEDURE USP_LandedCostVendorsGet
GO
CREATE PROC [dbo].[USP_LandedCostVendorsGet] 
    @numDomainId NUMERIC(18, 0)
AS 
	SELECT [numLCVendorId], LCV.[numDomainId], [numVendorId], CI.[vcCompanyName]
	FROM   [dbo].[LandedCostVendors] LCV JOIN [dbo].[DivisionMaster] AS DM
	ON lcv.[numVendorId] = DM.[numDivisionID]
	JOIN [dbo].[CompanyInfo] CI ON CI.[numCompanyId] = DM.[numCompanyID]
	WHERE LCV.numDomainId=@numDomainId


GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_LandedCostVendorsSave')
DROP PROCEDURE USP_LandedCostVendorsSave
GO
CREATE PROC [dbo].[USP_LandedCostVendorsSave] 
    @numDomainId numeric(18, 0),
    @numVendorId numeric(18, 0)
AS 

BEGIN TRY
	BEGIN TRANSACTION 

		IF EXISTS(SELECT 1 FROM [dbo].[LandedCostVendors] AS LCV WHERE [LCV].[numDomainId]=@numDomainId AND [LCV].[numVendorId]=@numVendorId)
		BEGIN
			RAISERROR('Vendor_Exists', 16, 1)
					return 1
		END
		ELSE
		BEGIN
			INSERT INTO [dbo].[LandedCostVendors] ([numDomainId], [numVendorId])
				SELECT @numDomainId, @numVendorId
		END

	COMMIT TRANSACTION

END TRY
BEGIN CATCH
 DECLARE @strMsg VARCHAR(200)
        SET @strMsg = ERROR_MESSAGE()
        IF ( @@TRANCOUNT > 0 ) 
            BEGIN
                RAISERROR ( @strMsg, 16, 1 ) ;
                ROLLBACK TRAN
                RETURN 1
            END
END CATCH
	
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ManageBillHeader' ) 
    DROP PROCEDURE USP_ManageBillHeader
GO

CREATE PROCEDURE USP_ManageBillHeader
    @numBillID [numeric](18, 0) OUTPUT,
    @numDivisionID [numeric](18, 0),
    @numAccountID [numeric](18, 0),
    @dtBillDate [datetime],
    @numTermsID [numeric](18, 0),
    @numDueAmount [numeric](18, 2),
    @dtDueDate [datetime],
    @vcMemo [varchar](1000),
    @vcReference [varchar](500),
    @numBillTotalAmount [numeric](18, 2),
    @bitIsPaid [bit],
    @numDomainID [numeric](18, 0),
    @strItems VARCHAR(MAX),
    @numUserCntID NUMERIC(18, 0),
	@numOppId NUMERIC(18,0)=0,
	@bitLandedCost BIT=0
AS 
    SET NOCOUNT ON  
    BEGIN TRY 
        BEGIN TRAN  
        --Validation of closed financial year
		EXEC dbo.USP_ValidateFinancialYearClosingDate @numDomainID,@dtBillDate
        
        IF EXISTS ( SELECT  [numBillID]
                    FROM    [dbo].[BillHeader]
                    WHERE   [numBillID] = @numBillID ) 
            BEGIN
                UPDATE  [dbo].[BillHeader]
                SET     numDivisionID = @numDivisionID,
                        numAccountID = @numAccountID,
                        dtBillDate = @dtBillDate,
                        numTermsID = @numTermsID,
                        monAmountDue = @numDueAmount,
                        dtDueDate = @dtDueDate,
                        vcMemo = @vcMemo,
                        vcReference = @vcReference,
                        --monAmtPaid = @numBillTotalAmount,
                        bitIsPaid = (CASE WHEN ISNULL(monAmtPaid,0)=@numDueAmount THEN 1
									ELSE 0 END),
                        numDomainID = @numDomainID,
                        dtModifiedDate = GETUTCDATE(),
                        numModifiedBy = @numUserCntID
                WHERE   [numBillID] = @numBillID
                        AND numDomainID = @numDomainID
            END
        ELSE 
            BEGIN
            
				--Set Default Class If enable User Level Class Accountng 
				  DECLARE @numAccountClass AS NUMERIC(18);SET @numAccountClass=0
				  SELECT @numAccountClass=ISNULL(numDefaultClass,0) FROM dbo.UserMaster UM JOIN dbo.Domain D ON UM.numDomainID=D.numDomainId
				  WHERE D.numDomainId=@numDomainId AND UM.numUserDetailId=@numUserCntID AND ISNULL(D.IsEnableUserLevelClassTracking,0)=1
				  
                INSERT  INTO [dbo].[BillHeader]
                        (
                          [numDivisionID],
                          [numAccountID],
                          [dtBillDate],
                          [numTermsID],
                          [monAmountDue],
                          [dtDueDate],
                          [vcMemo],
                          [vcReference],
                          --[monAmtPaid],
                          [bitIsPaid],
                          [numDomainID],
                          [numCreatedBy],
                          [dtCreatedDate],numAccountClass,[numOppId],[bitLandedCost] 
                        )
                VALUES  (
                          @numDivisionID,
                          @numAccountID,
                          @dtBillDate,
                          @numTermsID,
                          @numDueAmount,
                          @dtDueDate,
                          @vcMemo,
                          @vcReference,
                          --@numBillTotalAmount,
                          @bitIsPaid,
                          @numDomainID,
                          @numUserCntID,
                          GETUTCDATE(),@numAccountClass,@numOppId,@bitLandedCost
                        )
                SET @numBillID = SCOPE_IDENTITY()
            END
				
		PRINT @numBillID		
--        DELETE  FROM dbo.BillDetails
--        WHERE   numBillID = @numBillID 
				
        DECLARE @hDocItem INT
        IF CONVERT(VARCHAR(MAX), @strItems) <> '' 
            BEGIN
                EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems
                
                 DELETE FROM BillDetails WHERE numBillID = @numBillID 
					AND numBillDetailID NOT IN (SELECT X.numBillDetailID 
			 FROM OPENXML(@hDocItem,'/NewDataSet/Table1 [numBillDetailID>0]',2)                                                                                                              
			With(numBillDetailID numeric(9))X)
			                                                           
			--Update transactions
			Update BillDetails  Set numExpenseAccountID=X.numExpenseAccountID,monAmount=X.monAmount,                                                                                              
			vcDescription=X.vcDescription,numProjectID=X.numProjectID
			,numClassID=X.numClassID,numCampaignID=X.numCampaignID
			From (SELECT * FROM OPENXML(@hDocItem,'/NewDataSet/Table1 [numBillDetailID>0]',2)                                                                                                              
			With(                                                                                       
			numBillDetailID NUMERIC(18,0),[numExpenseAccountID] NUMERIC(18, 0), [monAmount] MONEY, [vcDescription] VARCHAR(1000), [numProjectID] NUMERIC(18, 0), [numClassID] NUMERIC(18, 0), [numCampaignID] NUMERIC(18, 0)                                                                                                 
			))X                                                                                                  
			Where BillDetails.numBillDetailID=X.numBillDetailID AND numBillID = @numBillID                                                                                             
			               
			
                INSERT  INTO [dbo].[BillDetails]
                        (
                          [numBillID],
                          [numExpenseAccountID],
                          [monAmount],
                          [vcDescription],
                          [numProjectID],
                          [numClassID],
                          [numCampaignID]
                        )
                        SELECT  @numBillID,
                                X.[numExpenseAccountID],
                                X.[monAmount],
                                X.[vcDescription],
                                X.[numProjectID],
                                X.[numClassID],
                                X.[numCampaignID]
                        FROM    ( SELECT    *
                                  FROM      OPENXML (@hDocItem, '/NewDataSet/Table1 [numBillDetailID=0]', 2)
                                            WITH (numBillDetailID NUMERIC(18,0), [numExpenseAccountID] NUMERIC(18, 0), [monAmount] MONEY, [vcDescription] VARCHAR(1000), [numProjectID] NUMERIC(18, 0), [numClassID] NUMERIC(18, 0), [numCampaignID] NUMERIC(18, 0) )
                                ) X
                EXEC sp_xml_removedocument @hDocItem
            END

		-- Update leads,prospects to Accounts
		EXEC dbo.USP_UpdateCRMTypeToAccounts @numDivisionID ,@numDomainID ,@numUserCntID
		
        COMMIT TRAN 
    END TRY 
    BEGIN CATCH		
        DECLARE @strMsg VARCHAR(200)
        SET @strMsg = ERROR_MESSAGE()
        IF ( @@TRANCOUNT > 0 ) 
            BEGIN
                RAISERROR ( @strMsg, 16, 1 ) ;
                ROLLBACK TRAN
                RETURN 1
            END
    END CATCH	
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ManageOpportunityAutomationQueue' ) 
    DROP PROCEDURE USP_ManageOpportunityAutomationQueue
GO
CREATE PROCEDURE USP_ManageOpportunityAutomationQueue
    @numOppQueueID numeric(18, 0)=0,
    @numDomainID numeric(18, 0)=0,
    @numOppId numeric(18, 0)=0,
    @numOppBizDocsId numeric(18, 0)=0,
    @numBizDocStatus numeric(18, 0)=0,
    @numOrderStatus NUMERIC(18,0)=0,
    @numUserCntID numeric(18, 0)=0,
    @tintProcessStatus TINYINT=1,
    @vcDescription varchar(1000)='',
    @numRuleID numeric(18, 0)=0,
    @bitSuccess BIT=0,
    @tintMode TINYINT,
    @numShippingReportID NUMERIC(18,0)=0
AS 
BEGIN
    IF @tintMode = 2
        BEGIN
			  SELECT TOP 25 OAQ.[numOppQueueID] ,
							OAQ.[numDomainID],
							OAQ.[numOppId],
							OAQ.[numOppBizDocsId],
							OAQ.[numBizDocStatus],
							OAQ.[numCreatedBy],
							OAQ.[dtCreatedDate],
							OAQ.[tintProcessStatus],
							OAQ.[numRuleID],
							OAQ.[numOrderStatus],
							CASE WHEN ISNULL(OAQ.[numShippingReportID],0) > 0 THEN OAQ.[numShippingReportID] 
								 ELSE (SELECT TOP 1 [ShippingReport].[numShippingReportId] FROM [dbo].[ShippingReport] WHERE [OBD].[numOppBizDocsId] = [ShippingReport].[numOppBizDocId] ORDER BY [ShippingReport].[numShippingReportId] DESC)
							END [numShippingReportID], 
							OBD.numBizDocId,OM.numDivisionId,OM.tintOppStatus,OM.tintshipped,OM.tintOppType FROM OpportunityAutomationQueue OAQ JOIN dbo.OpportunityMaster OM ON OAQ.numOppId=OM.numOppId
				 LEFT JOIN dbo.OpportunityBizDocs OBD ON OAQ.numOppId=OBD.numOppId AND OAQ.numOppBizDocsId=OBD.numOppBizDocsId
				 WHERE tintProcessStatus IN (1,2) ORDER BY numOppQueueID
			--SELECT TOP 1 @numOppQueueID=numOppQueueID FROM OpportunityAutomationQueue WHERE tintProcessStatus IN (1,2) ORDER BY numOppQueueID
			--UPDATE OpportunityAutomationQueue SET tintProcessStatus=2 WHERE numOppQueueID=@numOppQueueID
			
			--SELECT * FROM OpportunityAutomationQueue WHERE numOppQueueID=@numOppQueueID
        END
    ELSE IF @tintMode = 3
        BEGIN
			SELECT @numOrderStatus=ISNULL(numStatus,0) FROM OpportunityMaster WHERE numOppId=@numOppId
			
			IF ISNULL(@numOppBizDocsId,0)>0
			BEGIN
				SELECT @numBizDocStatus=ISNULL(numBizDocStatus,0) FROM OpportunityBizDocs WHERE numOppId=@numOppId AND numOppBizDocsId=@numOppBizDocsId
			END
			
			INSERT INTO dbo.OpportunityAutomationQueueExecution (
						numOppQueueID,bitSuccess,vcDescription,dtExecutionDate,numRuleID,numOppId,numOppBizDocsId,numOrderStatus,numBizDocStatus
					) VALUES ( @numOppQueueID,@bitSuccess,@vcDescription,GETUTCDATE(),@numRuleID,@numOppId,@numOppBizDocsId,@numOrderStatus,@numBizDocStatus)
        END    
	ELSE IF @tintMode = 1 
        BEGIN
			IF @numOppQueueID>0
			BEGIN
				UPDATE [dbo].[OpportunityAutomationQueue] SET [tintProcessStatus] = @tintProcessStatus WHERE  [numOppQueueID] = @numOppQueueID AND [numDomainID] = @numDomainID
			END
			ELSE
			BEGIN
				IF EXISTS (SELECT * FROM OpportunityAutomationQueue WHERE numDomainID=@numDomainID AND numOppId=@numOppId AND numOppBizDocsId=@numOppBizDocsId AND tintProcessStatus=1 AND ISNULL(numRuleID,0)=@numRuleID)
				BEGIN
					UPDATE OpportunityAutomationQueue SET tintProcessStatus=5 WHERE numDomainID=@numDomainID AND numOppId=@numOppId AND numOppBizDocsId=@numOppBizDocsId AND tintProcessStatus=1 AND ISNULL(numRuleID,0)=@numRuleID
				END 
				
				IF @numBizDocStatus>0 OR @numRuleID>0 OR @numOrderStatus>0
				BEGIN
					INSERT INTO [dbo].[OpportunityAutomationQueue] ([numDomainID], [numOppId], [numOppBizDocsId], [numBizDocStatus], [numCreatedBy], [dtCreatedDate], [tintProcessStatus],numRuleID,numOrderStatus,numShippingReportID)
						SELECT @numDomainID, @numOppId, @numOppBizDocsId, @numBizDocStatus, @numUserCntID, GETUTCDATE(), @tintProcessStatus,@numRuleID,@numOrderStatus,@numShippingReportID	
				END
			END
        END	
 END



/****** Object:  StoredProcedure [dbo].[USP_ManageShippingReport]    Script Date: 05/07/2009 17:31:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--SELECT * FROM [ShippingReport]
--SELECT * FROM [ShippingReportItems]
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageShippingReport')
DROP PROCEDURE USP_ManageShippingReport
GO
CREATE PROCEDURE [dbo].[USP_ManageShippingReport]
  @numDomainId NUMERIC(9),
    @numShippingReportId NUMERIC(9) = 0 OUTPUT,
    @numOppBizDocId NUMERIC(9),
    @numShippingCompany NUMERIC(9),
    @Value1 VARCHAR(100) = NULL,
    @Value2 VARCHAR(100) = NULL,
    @Value3 VARCHAR(100) = NULL,
    @Value4 VARCHAR(100) = NULL,
    @vcFromState VARCHAR(50) = '',
    @vcFromZip VARCHAR(50) = '',
    @vcFromCountry VARCHAR(50) = '',
    @vcToState VARCHAR(50) = '',
    @vcToZip VARCHAR(50) = '',
    @vcToCountry VARCHAR(50) = '',
    @numUserCntId NUMERIC(9),
    @strItems TEXT = NULL,
    @tintMode TINYINT=0,
	@tintPayorType TINYINT=0,
	@vcPayorAccountNo VARCHAR(20)='',
	@numPayorCountry NUMERIC(9),
	@vcPayorZip VARCHAR(50)='',
	@vcFromCity VARCHAR(50)='',
    @vcFromAddressLine1 VARCHAR(50)='',
    @vcFromAddressLine2 VARCHAR(50)='',
    @vcToCity VARCHAR(50)='',
    @vcToAddressLine1 VARCHAR(50)='',
    @vcToAddressLine2 VARCHAR(50)='',
    @vcFromName			VARCHAR(1000)='',
    @vcFromCompany		VARCHAR(1000)='',
    @vcFromPhone		VARCHAR(100)='',
    @bitFromResidential	BIT=0,
    @vcToName			VARCHAR(1000)='',
    @vcToCompany		VARCHAR(1000)='',
    @vcToPhone			VARCHAR(100)='',
    @bitToResidential	BIT=0,
    @IsCOD					BIT = 0,
	@IsDryIce				BIT = 0,
	@IsHoldSaturday			BIT = 0,
	@IsHomeDelivery			BIT = 0,
	@IsInsideDelevery		BIT = 0,
	@IsInsidePickup			BIT = 0,
	@IsReturnShipment		BIT = 0,
	@IsSaturdayDelivery		BIT = 0,
	@IsSaturdayPickup		BIT = 0,
	@numCODAmount			NUMERIC(18,0) = 0,
	@vcCODType				VARCHAR(50) = 'Any',
	@numTotalInsuredValue	NUMERIC(18,2) = 0,
	@IsAdditionalHandling	BIT,
	@IsLargePackage			BIT,
	@vcDeliveryConfirmation	VARCHAR(1000),
	@vcDescription			VARCHAR(MAX),
	@numOppId				NUMERIC(18,0),
	@numTotalCustomsValue	NUMERIC(18,2)
AS 
    BEGIN
  
        IF @numShippingReportId = 0 
            BEGIN
                INSERT  INTO [ShippingReport]
                        (
                          [numOppBizDocId],
                          [numShippingCompany],
                          vcValue1,
                          vcValue2,
                          vcValue3,
                          vcValue4,
                          vcFromState,
                          vcFromZip,
                          vcFromCountry,
                          vcToState,
                          vcToZip,
                          vcToCountry,
                          [dtCreateDate],
                          [numCreatedBy],
                          numDomainId,
						  tintPayorType,
						  vcPayorAccountNo,
						  numPayorCountry,
						  vcPayorZip,
						  vcFromCity ,
						  vcFromAddressLine1 ,
						  vcFromAddressLine2 ,
						  vcToCity ,
						  vcToAddressLine1 ,
						  vcToAddressLine2,
						  vcFromName,
						  vcFromCompany,
						  vcFromPhone,
						  bitFromResidential,
						  vcToName,
						  vcToCompany,
						  vcToPhone,
						  bitToResidential,
						  IsCOD,					
						  IsDryIce,				
						  IsHoldSaturday,			
						  IsHomeDelivery,			
						  IsInsideDelevery,		
						  IsInsidePickup,			
						  IsReturnShipment,		
						  IsSaturdayDelivery,		
						  IsSaturdayPickup,		
						  numCODAmount,			
						  vcCODType,				
						  numTotalInsuredValue,
						  IsAdditionalHandling,
						  IsLargePackage,
						  vcDeliveryConfirmation,
						  vcDescription,
						  numOppId,numTotalCustomsValue
                        )
                VALUES  (
                          @numOppBizDocId,
                          @numShippingCompany,
                          @value1,
                          @value2,
                          @value3,
                          @value4,
                          @vcFromState,
                          @vcFromZip,
                          @vcFromCountry,
                          @vcToState,
                          @vcToZip,
                          @vcToCountry,
                          GETUTCDATE(),
                          @numUserCntId,
                          @numDomainId,
						  @tintPayorType,
						  @vcPayorAccountNo,
						  @numPayorCountry,
						  @vcPayorZip,
						  @vcFromCity ,
						  @vcFromAddressLine1 ,
						  @vcFromAddressLine2 ,
						  @vcToCity ,
						  @vcToAddressLine1 ,
						  @vcToAddressLine2,
						  @vcFromName,
						  @vcFromCompany,
						  @vcFromPhone,
						  @bitFromResidential,
						  @vcToName,
						  @vcToCompany,
						  @vcToPhone,
						  @bitToResidential,
						  @IsCOD,					
						  @IsDryIce,				
						  @IsHoldSaturday,			
						  @IsHomeDelivery,			
						  @IsInsideDelevery,		
						  @IsInsidePickup,			
						  @IsReturnShipment,		
						  @IsSaturdayDelivery,		
						  @IsSaturdayPickup,		
						  @numCODAmount,			
						  @vcCODType,				
						  @numTotalInsuredValue,
						  @IsAdditionalHandling,
						  @IsLargePackage,
						  @vcDeliveryConfirmation,
						  @vcDescription,
						  @numOppId,@numTotalCustomsValue							  
                        )
                        
				SET @numShippingReportId = @@IDENTITY
				
                DECLARE @numBoxID NUMERIC 
--				IF ISNULL(@value2,0) >0 -- Service type i.e Priority overnight,Ground
--				BEGIN
					-- create shipping box and Put all items into single box
						INSERT INTO [ShippingBox] (
							[vcBoxName],
							[numShippingReportId],
							[dtCreateDate],
							[numCreatedBy]
						) VALUES ( 
							/* vcBoxName - varchar(20) */ 'Box1',
							/* numShippingReportId - numeric(18, 0) */ @numShippingReportId,
							/* dtCreateDate - datetime */ GETUTCDATE(),
							/* numCreatedBy - numeric(18, 0) */ @numUserCntId ) 
		                
						SET @numBoxID = @@IDENTITY
--				END
                
                
                IF (@strItems IS NOT NULL)
				BEGIN
				PRINT 'XML'
					DECLARE @hDocItem INT
					EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems
	  
					INSERT  INTO [ShippingReportItems]
							(
							  [numShippingReportId],
							  [numItemCode],
							  [tintServiceType],
							  [dtDeliveryDate],
							  [monShippingRate],
							  [fltTotalWeight],
							  [intNoOfBox],
							  [fltHeight],
							  [fltWidth],
							  [fltLength],
							  [dtCreateDate],
							  [numCreatedBy],
							  numBoxID,
							  [numOppBizDocItemID]
							)
							SELECT  @numShippingReportId,--X.[numShippingReportId],
									X.[numItemCode],
									X.[tintServiceType],
									X.[dtDeliveryDate],
									X.[monShippingRate],
									X.[fltTotalWeight],
									X.[intNoOfBox],
									X.[fltHeight],
									X.[fltWidth],
									X.[fltLength],
									GETUTCDATE(),--X.[dtCreateDate],
									@numUserCntId,
									@numBoxID,
									CASE @tintMode WHEN 0 THEN X.numOppBizDocItemID ELSE (SELECT TOP 1 numOppBizDocItemID FROM OpportunityBizDocItems OBI WHERE OBI.numItemCode = X.numItemCode) END AS numOppBizDocItemID
							FROM    ( SELECT    *
									  FROM      OPENXML (@hDocItem, '/NewDataSet/Items', 2)
												WITH ( --numShippingReportId  NUMERIC(18,0),
												numItemCode NUMERIC(18, 0), tintServiceType TINYINT, dtDeliveryDate DATETIME, monShippingRate MONEY, 
												fltTotalWeight FLOAT, intNoOfBox INT, fltHeight FLOAT, fltWidth FLOAT, fltLength FLOAT,numOppBizDocItemID NUMERIC)
									) X
					
					EXEC sp_xml_removedocument @hDocItem	
				END                                
                

            END
        ELSE 
            IF @numShippingReportId > 0 
                BEGIN
                    UPDATE  [ShippingReport]
                    SET     [numShippingCompany] = @numShippingCompany,
                            vcValue1 = @Value1,
                            vcValue2 = @Value2,
                            vcValue3 = @Value3,
                            vcValue4 = @Value4,
                            vcFromState = @vcFromState,
                            vcFromZip = @vcFromZip,
                            vcFromCountry = @vcFromCountry,
                            vcToState = @vcToState,
                            vcToZip = @vcToZip,
                            vcToCountry = @vcToCountry,
							tintPayorType = @tintPayorType,
						    vcPayorAccountNo=@vcPayorAccountNo,
						    numPayorCountry=@numPayorCountry,
						    vcPayorZip=@vcPayorZip,
							vcFromCity=@vcFromCity,
							vcFromAddressLine1=@vcFromAddressLine1,
							vcFromAddressLine2=@vcFromAddressLine2,
							vcToCity=@vcToCity,
							vcToAddressLine1=@vcToAddressLine1,
							vcToAddressLine2=@vcToAddressLine2,
							vcFromName = @vcFromName,
							vcFromCompany = @vcFromCompany,
							vcFromPhone = @vcFromPhone,
							bitFromResidential = @bitFromResidential,
							vcToName = @vcToName,
							vcToCompany = @vcToCompany,
							vcToPhone = @vcToPhone,
							bitToResidential = @bitToResidential,
						    IsCOD = @IsCOD ,					
						    IsDryIce = @IsDryIce,				
						    IsHoldSaturday  = @IsHoldSaturday,			
						    IsHomeDelivery = @IsHomeDelivery,			
						    IsInsideDelevery = @IsInsideDelevery,		
						    IsInsidePickup = @IsInsidePickup,			
						    IsReturnShipment = @IsReturnShipment,		
						    IsSaturdayDelivery = @IsSaturdayDelivery,		
						    IsSaturdayPickup = @IsSaturdayPickup,		
						    numCODAmount = @numCODAmount,			
						    vcCODType = @vcCODType,				
						    numTotalInsuredValue = @numTotalInsuredValue,
						    IsAdditionalHandling = @IsAdditionalHandling,
							IsLargePackage = @IsLargePackage,
							vcDeliveryConfirmation = @vcDeliveryConfirmation,
							vcDescription = @vcDescription,							
						    numOppId = @numOppId,
						    numTotalCustomsValue = @numTotalCustomsValue
                    WHERE   [numShippingReportId] = @numShippingReportId
                    
                    UPDATE [ShippingReportItems] 
                    SET [tintServiceType] = @Value2 
                    WHERE [numShippingReportId]=@numShippingReportId
                  
                END
    
       
    END

/****** Object:  StoredProcedure [dbo].[USP_OPPBizDocItems]    Script Date: 07/26/2008 16:20:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                                                                                                                                                        
-- exec USP_OPPBizDocItems @numOppId=12388,@numOppBizDocsId=11988,@numDomainID=110
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_oppbizdocitems' ) 
    DROP PROCEDURE usp_oppbizdocitems
GO
CREATE PROCEDURE [dbo].[USP_OPPBizDocItems]
    (
      @numOppId AS NUMERIC(9) = NULL,
      @numOppBizDocsId AS NUMERIC(9) = NULL,
      @numDomainID AS NUMERIC(9) = 0      
    )
AS 
    DECLARE @DivisionID AS NUMERIC(9)      
    DECLARE @tintTaxOperator AS TINYINT   
                                                                                                                                                                                                                                                                                                                                          
    SELECT  @DivisionID = numDivisionID, @tintTaxOperator = [tintTaxOperator]           
    FROM    OpportunityMaster
    WHERE   numOppId = @numOppId                                                                                                                                                                          
--Decimal points to show in bizdoc 
    DECLARE @DecimalPoint AS TINYINT
    SELECT  @DecimalPoint = tintDecimalPoints
    FROM    domain
    WHERE   numDomainID = @numDomainID
                                                                                          
    DECLARE @strSQL AS VARCHAR(MAX)                                                                                          
    DECLARE @strSQL1 AS VARCHAR(8000)                                       
    DECLARE @strSQLCusFields VARCHAR(1000)                      
    DECLARE @strSQLEmpFlds VARCHAR(500)                                                                         
    SET @strSQLCusFields = ''                                                                                          
    SET @strSQLEmpFlds = ''                                                                                          
    DECLARE @tintType AS TINYINT   
    DECLARE @tintOppType AS TINYINT                                                      
    DECLARE @intRowNum AS INT                                                  
    DECLARE @numFldID AS VARCHAR(15)                                          
    DECLARE @vcFldname AS VARCHAR(50)                                            
    DECLARE @numBizDocId AS NUMERIC(9)
    DECLARE @numBizDocTempID AS NUMERIC(9) ;
        SET @numBizDocTempID = 0
                                           
    SELECT  @numBizDocTempID = ISNULL(numBizDocTempID, 0),
            @numBizDocId = numBizDocId
    FROM    OpportunityBizDocs
    WHERE   numOppBizDocsId = @numOppBizDocsId
    SELECT  @tintType = tintOppType,
            @tintOppType = tintOppType
    FROM    OpportunityMaster
    WHERE   numOppID = @numOppId                                                                                          
                                                                                   
    IF @tintType = 1 
        SET @tintType = 7
    ELSE 
        SET @tintType = 8                                                                                      
                                                                                
                                                                                        
                                                                                          
 

    SELECT  *
    INTO    #Temp1
    FROM    ( SELECT   Opp.vcitemname AS vcItemName,
                        ISNULL(OBD.numOppBizDocItemID, 0) OppBizDocItemID,
                        CASE WHEN charitemType = 'P' THEN 'Product'
                             WHEN charitemType = 'S' THEN 'Service'
                        END AS charitemType,
                        CASE WHEN charitemType = 'P' THEN 'Inventory'
	                         WHEN charitemType = 'N' THEN 'Non-Inventory'
                             WHEN charitemType = 'S' THEN 'Service'
                        END AS vcItemType,
                        CASE WHEN bitShowDeptItemDesc = 0
                             THEN CONVERT(VARCHAR(2500), OBD.vcitemDesc)
                             WHEN bitShowDeptItemDesc = 1
                                  AND bitKitParent = 1
                                  AND bitAssembly = 0
                             THEN dbo.fn_PopulateKitDesc(opp.numOppId,
                                                         opp.numoppitemtCode)
                             WHEN bitShowDeptItemDesc = 1
                                  AND bitKitParent = 1
                                  AND bitAssembly = 1
                             THEN dbo.fn_PopulateAssemblyDesc(opp.numoppitemtCode, OBD.numUnitHour)
                             ELSE CONVERT(VARCHAR(2500), OBD.vcitemDesc)
                        END AS txtItemDesc,                                      
--vcitemdesc as [desc],                                      
                        dbo.fn_UOMConversion(ISNULL(i.numBaseUnit, 0),
                                             i.numItemCode, @numDomainID,
                                             ISNULL(opp.numUOMId, 0))
                        * OBD.numUnitHour AS numUnitHour,
                        CONVERT(DECIMAL(18, 4), dbo.fn_UOMConversion(ISNULL(opp.numUOMId, 0), i.numItemCode, @numDomainID, ISNULL(i.numBaseUnit, 0))
                        * OBD.monPrice) monPrice,
                        CONVERT(VARCHAR, CONVERT(DECIMAL(18, 4), OBD.monTotAmount)) Amount,
                        OBD.monTotAmount/*Fo calculating sum*/,
--                        CASE WHEN @tintOppType = 1
--                             THEN CASE WHEN bitTaxable = 0 THEN 0
--                                       WHEN bitTaxable = 1
--                                       THEN ( SELECT    fltPercentage
--                                              FROM      OpportunityBizDocTaxItems
--                                              WHERE     numOppBizDocID = @numOppBizDocsId
--                                                        AND numTaxItemID = 0
--                                            )
--                                  END
--                             ELSE 0
--                        END AS Tax,
                        ISNULL(CONVERT(VARCHAR, i.monListPrice), 0) AS monListPrice,
                        i.numItemCode AS ItemCode,
                        opp.numoppitemtCode,
                        L.vcdata AS numItemClassification,
                        CASE WHEN bitTaxable = 0 THEN 'No'
                             ELSE 'Yes'
                        END AS Taxable,
                        numIncomeChartAcntId AS itemIncomeAccount,
                        'NI' AS ItemType,
                        ISNULL(GJH.numJournal_Id, 0) AS numJournalId,
                        ( SELECT TOP 1
                                    ISNULL(GJD.numTransactionId, 0)
                          FROM      General_Journal_Details GJD
                          WHERE     GJH.numJournal_Id = GJD.numJournalId
                                    AND GJD.chBizDocItems = 'NI'
                                    AND GJD.numoppitemtCode = opp.numoppitemtCode
                        ) AS numTransactionId,
                        ISNULL(Opp.vcModelID, '') vcModelID,
                        dbo.USP_GetAttributes(OBD.numWarehouseItmsID,
                                              bitSerialized) AS vcAttributes,
                        ISNULL(vcPartNo, '') AS vcPartNo,
                        ( ISNULL(OBD.monTotAmtBefDiscount, OBD.monTotAmount)
                          - OBD.monTotAmount ) AS DiscAmt,
                        OBD.vcNotes,
                        OBD.vcTrackingNo,
                        OBD.vcShippingMethod,
                        OBD.monShipCost,
                        [dbo].[FormatedDateFromDate](OBD.dtDeliveryDate,
                                                     @numDomainID) dtDeliveryDate,
                        Opp.numWarehouseItmsID,
--ISNULL(I.vcUnitofMeasure,'Units') vcUnitofMeasure,
                        ISNULL(u.vcUnitName, '') numUOMId,
                        ISNULL(Opp.vcManufacturer, '') AS vcManufacturer,
                        ISNULL(V.monCost, 0) AS VendorCost,
                        Opp.vcPathForTImage,
                        i.[fltLength],
                        i.[fltWidth],
                        i.[fltHeight],
                        i.[fltWeight],
                        ISNULL(I.numVendorID, 0) numVendorID,
                        Opp.bitDropShip AS DropShip,
                        SUBSTRING(( SELECT  ' ,' + vcSerialNo
                                            + CASE WHEN ISNULL(I.bitLotNo, 0) = 1
                                                   THEN ' ('
                                                        + CONVERT(VARCHAR(15), oppI.numQty)
                                                        + ')'
                                                   ELSE ''
                                              END
                                    FROM    OppWarehouseSerializedItem oppI
                                            JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID = whi.numWareHouseItmsDTLID
                                    WHERE   oppI.numOppID = Opp.numOppId
                                            AND oppI.numOppItemID = Opp.numoppitemtCode
                                            AND 1=(CASE WHEN @tintOppType=1 then CASE WHEN oppI.numOppBizDocsID=OBD.numOppBizDocID THEN 1 ELSE 0 END
														ELSE 1 END)
                                    ORDER BY vcSerialNo
                                  FOR
                                    XML PATH('')
                                  ), 3, 200000) AS SerialLotNo,
                        ISNULL(opp.numUOMId, 0) AS numUOM,
                        ISNULL(u.vcUnitName, '') vcUOMName,
                        dbo.fn_UOMConversion(opp.numUOMId, Opp.numItemCode,
                                             @numDomainID, NULL) AS UOMConversionFactor,
                        ISNULL(Opp.numSOVendorId, 0) AS numSOVendorId,
                        [dbo].FormatedDateTimeFromDate(OBD.dtRentalStartDate,
                                                       @numDomainID) dtRentalStartDate,
                        [dbo].FormatedDateTimeFromDate(OBD.dtRentalReturnDate,
                                                       @numDomainID) dtRentalReturnDate,
-- Added for packing slip
                        ISNULL(W.vcWareHouse, '') AS Warehouse,
                        CASE WHEN charitemType = 'P' THEN WI.vcWHSKU
                             ELSE i.vcSKU
                        END vcSKU,
                        CASE WHEN charitemType = 'P' THEN WI.vcBarCode
                             ELSE i.numBarCodeId
                        END vcBarcode,
                        ISNULL(opp.numClassID, 0) AS numClassID,
                        ISNULL(opp.numProjectID, 0) AS numProjectID,
                        ISNULL(I.[numShipClass],0) AS numShipClass,OBD.numOppBizDocID,WL.vcLocation,ISNULL(bitSerialized,0) AS bitSerialized,ISNULL(bitLotNo,0) AS bitLotNo,
                        opp.numOppId,OBD.numUnitHour AS numUnitHourOrig,ISNULL(I.numItemCode,0) AS [numItemCode]						
              FROM      OpportunityItems opp
                        JOIN OpportunityBizDocItems OBD ON OBD.numOppItemID = opp.numoppitemtCode
                        LEFT JOIN item i ON opp.numItemCode = i.numItemCode
                        LEFT JOIN ListDetails L ON i.numItemClassification = L.numListItemID
                        LEFT JOIN Vendor V ON V.numVendorID = @DivisionID
                                              AND V.numItemCode = opp.numItemCode
                        LEFT JOIN General_Journal_Header GJH ON opp.numoppid = GJH.numOppId
                                                                AND GJH.numBizDocsPaymentDetId IS NULL
                                                                AND GJH.numOppBizDocsId = @numOppBizDocsId
                        LEFT JOIN UOM u ON u.numUOMId = opp.numUOMId                                    
-- left join General_Journal_Details GJD on GJH.numJournal_Id=GJD.numJournalId And GJD.chBizDocItems='NI' And GJD.numoppitemtCode=opp.numoppitemtCode                                       
                        LEFT JOIN dbo.WareHouseItems WI ON opp.numWarehouseItmsID = WI.numWareHouseItemID
                                                           AND WI.numDomainID = @numDomainID
                        LEFT JOIN dbo.Warehouses W ON WI.numDomainID = W.numDomainID
                                                      AND WI.numWareHouseID = W.numWareHouseID
                        LEFT JOIN dbo.WarehouseLocation WL ON WL.numWLocationID = WI.numWLocationID
              WHERE     opp.numOppId = @numOppId
                        AND ( bitShowDeptItem IS NULL
                              OR bitShowDeptItem = 0
                            )
                        AND OBD.numOppBizDocID = @numOppBizDocsId
       
--For Kit Items where following condition is true( Show dependant items as line items on BizDoc (instead of kit item name))
-- Commented by chintan, Reason: Bug #1829 
/*
union
select Opp.vcitemname as Item,                                      
case when i.charitemType='P' then 'Product' when i.charitemType='S' then 'Service' end as type,                                      
convert(varchar(500),OBD.vcitemDesc) as [Desc],                                      
--vcitemdesc as [desc],                                      
OBD.numUnitHour*intQuantity as Unit,
CONVERT(VARCHAR,CONVERT(DECIMAL(18, 4), OBD.monPrice)) Price,
CONVERT(VARCHAR,CONVERT(DECIMAL(18, 4), OBD.monTotAmount)) Amount,
OBD.monTotAmount/*Fo calculating sum*/,
case when @tintOppType=1 then case when i.bitTaxable =0 then 0 when i.bitTaxable=1 then (select fltPercentage from OpportunityBizDocTaxItems where numOppBizDocID=@numOppBizDocsId and numTaxItemID=0) end else 0 end  as Tax,                                    
isnull(convert(varchar,i.monListPrice),0) as monListPrice,i.numItemCode as ItemCode,opp.numoppitemtCode,                                      
L.vcdata as vcItemClassification,case when i.bitTaxable=0 then 'No' else 'Yes' end as Taxable,i.numIncomeChartAcntId as itemIncomeAccount,                                      
'NI' as ItemType                                      
,isnull(GJH.numJournal_Id,0) as numJournalId,(SELECT TOP 1 isnull(GJD.numTransactionId, 0) FROM General_Journal_Details GJD WHERE GJH.numJournal_Id = GJD.numJournalId And GJD.chBizDocItems = 'NI' And GJD.numoppitemtCode = opp.numoppitemtCode) as numTransactionId,isnull(Opp.vcModelID,'') vcModelID, dbo.USP_GetAttributes(OBD.numWarehouseItmsID,i.bitSerialized) as Attributes,isnull(vcPartNo,'') as Part 
,(isnull(OBD.monTotAmtBefDiscount,OBD.monTotAmount)-OBD.monTotAmount) as DiscAmt,OBD.vcNotes,OBD.vcTrackingNo,OBD.vcShippingMethod,OBD.monShipCost,[dbo].[FormatedDateFromDate](OBD.dtDeliveryDate,@numDomainID) dtDeliveryDate,Opp.numWarehouseItmsID,
--ISNULL(I.vcUnitofMeasure,'Units') vcUnitofMeasure,
ISNULL(u.vcUnitName,'') vcUnitofMeasure,
isnull(i2.vcManufacturer,'') as vcManufacturer,
isnull(V.monCost,0) as VendorCost,
Opp.vcPathForTImage,i.[fltLength],i.[fltWidth],i.[fltHeight],i.[fltWeight],isnull(I.numVendorID,0) numVendorID,Opp.bitDropShip AS DropShip,
SUBSTRING(
(SELECT ',' + vcSerialNo + CASE WHEN isnull(I.bitLotNo,0)=1 THEN ' (' + CONVERT(VARCHAR(15),oppI.numQty) + ')' ELSE '' end 
FROM OppWarehouseSerializedItem oppI JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID=whi.numWareHouseItmsDTLID where oppI.numOppID=Opp.numOppId and oppI.numOppItemID=Opp.numoppitemtCode 
ORDER BY vcSerialNo FOR XML PATH('')),2,200000) AS SerialLotNo,isnull(opp.numUOMId,0) AS numUOM,ISNULL(u.vcUnitName,'') vcUOMName,dbo.fn_UOMConversion(opp.numUOMId,Opp.numItemCode,@numDomainID,null) AS UOMConversionFactor,
ISNULL(Opp.numSOVendorId,0) as numSOVendorId,
[dbo].FormatedDateTimeFromDate(OBD.dtRentalStartDate,@numDomainID) dtRentalStartDate,
[dbo].FormatedDateTimeFromDate(OBD.dtRentalReturnDate,@numDomainID) dtRentalReturnDate
from OpportunityItems opp       
join OpportunityBizDocItems OBD on OBD.numOppItemID= opp.numoppitemtCode                                   
join  OpportunityKitItems OppKitItems      
on OppKitItems.numOppKitItem=opp.numoppitemtCode                                     
join item i on OppKitItems.numChildItem=i.numItemCode      
join item i2 on opp.numItemCode=i2.numItemCode                                      
left join ListDetails L on i.numItemClassification=L.numListItemID
left join Vendor V on V.numVendorID=@DivisionID and V.numItemCode=opp.numItemCode                                       
left join General_Journal_Header GJH on opp.numoppid=GJH.numOppId And GJH.numBizDocsPaymentDetId is null And GJH.numOppBizDocsId=@numOppBizDocsId                                      
-- left join General_Journal_Details GJD on GJH.numJournal_Id=GJD.numJournalId And GJD.chBizDocItems='NI' And GJD.numoppitemtCode=opp.numoppitemtCode
LEFT JOIN  UOM u ON u.numUOMId = opp.numUOMId                                    
where opp.numOppId=@numOppId and i2.bitShowDeptItem=1      and   OBD.numOppBizDocID=@numOppBizDocsId   */
--union
--select case when numcategory=1 then                             
--case when isnull(te.numcontractid,0) = 0 then 'Time' else 'Time(Contract)' end                              
--when numcategory=2 then                             
--case when isnull(te.numcontractid,0) = 0 then 'Expense' else 'Expense(Contract)' end                              
--end  as item,                           
--case when numcategory=1 then 'Time' when numcategory=2 then 'Expense' end                                      
-- as Type,                                      
--convert(varchar(100),txtDesc) as [Desc],                                      
----txtDesc as [Desc],                                      
--convert(decimal(18,2),datediff(minute,te.dtfromdate,te.dttodate))/60 as unit,                                      
--convert(varchar(100),monamount) as Price,                                      
--case when isnull(te.numcontractid,0) = 0                    
--then                    
-- case when numCategory =1  then                    
--   isnull(convert(decimal(18,2),datediff(minute,dtfromdate,dttodate))*monAmount/60,0)                    
--   when numCategory =2  then                    
--   isnull(monamount,0)                    
--                     
--   end                    
--else                                  
--  0                    
--end as amount,                                      
--0.00 as Tax,'' as listPrice,0 as ItemCode,0 as numoppitemtCode,'' as vcItemClassification,                                      
--'No' as Taxable,0 as itemIncomeAccount,'TE' as ItemType                                      
--,0 as numJournalId,0 as numTransactionId ,'' vcModelID,''as Attributes,'' as Part,0 as DiscAmt ,'' as vcNotes,'' as vcTrackingNo                                      
--from timeandexpense TE                            
--                                    
--where                                       
--(numtype= 1)   and   numDomainID=@numDomainID and                                    
--(numOppBizDocsId = @numOppBizDocsId  or numoppid = @numOppId)
            ) X


    --IF @DecimalPoint = 1 
    --    BEGIN
    --        UPDATE  #Temp1
    --        SET     monPrice = CONVERT(DECIMAL(18, 1), monPrice),
    --                Amount = CONVERT(DECIMAL(18, 1), Amount)
    --    END
    --IF @DecimalPoint = 2 
    --    BEGIN
    --        UPDATE  #Temp1
    --        SET     monPrice = CONVERT(DECIMAL(18, 2), monPrice),
    --                Amount = CONVERT(DECIMAL(18, 2), Amount)
    --    END
    --IF @DecimalPoint = 3 
    --    BEGIN
    --        UPDATE  #Temp1
    --        SET     monPrice = CONVERT(DECIMAL(18, 3), monPrice),
    --                Amount = CONVERT(DECIMAL(18, 3), Amount)
    --    END
    --IF @DecimalPoint = 4 
    --    BEGIN
    --        UPDATE  #Temp1
    --        SET     monPrice = CONVERT(DECIMAL(18, 4), monPrice),
    --                Amount = CONVERT(DECIMAL(18, 4), Amount)
    --    END

    DECLARE @strSQLUpdate AS VARCHAR(2000);SET @strSQLUpdate=''
   
    EXEC ( 'alter table #Temp1 add [TotalTax] money'
        )

    IF /*@tintOppType = 1 AND*/ @tintTaxOperator = 1 --add Sales tax
	--set @strSQLUpdate=@strSQLUpdate +',[TotalTax]= dbo.fn_CalSalesTaxAmt('+ convert(varchar(20),@numOppBizDocsId)+','+ convert(varchar(20),@numDomainID)+')*Amount/100'
        SET @strSQLUpdate = @strSQLUpdate
            + ',[TotalTax]= dbo.fn_CalBizDocTaxAmt('
            + CONVERT(VARCHAR(20), @numDomainID) + ',0,'
            + CONVERT(VARCHAR(20), @numOppId) +','+
            + 'numoppitemtCode,1)*Amount/100'
    ELSE IF /*@tintOppType = 1 AND*/ @tintTaxOperator = 2 -- remove sales tax
            SET @strSQLUpdate = @strSQLUpdate + ',[TotalTax]= 0'
    ELSE /*IF @tintOppType = 1*/ 
                SET @strSQLUpdate = @strSQLUpdate
                    + ',[TotalTax]= dbo.fn_CalBizDocTaxAmt('
                    + CONVERT(VARCHAR(20), @numDomainID) + ',0,'
                    + CONVERT(VARCHAR(20), @numOppId) +','+
                    + 'numoppitemtCode,1)*Amount/100'
    --ELSE 
    --            SET @strSQLUpdate = @strSQLUpdate + ',[TotalTax]= 0'


 DECLARE @vcTaxName AS VARCHAR(100)
    DECLARE @numTaxItemID AS NUMERIC(9)
    SET @numTaxItemID = 0
    SELECT TOP 1
            @vcTaxName = vcTaxName,
            @numTaxItemID = numTaxItemID
    FROM    TaxItems
    WHERE   numDomainID = @numDomainID


    WHILE @numTaxItemID > 0
        BEGIN

            EXEC ( 'alter table #Temp1 add [' + @vcTaxName + '] money'
                )

            /*IF @tintOppType = 1 */
                SET @strSQLUpdate = @strSQLUpdate + ',[' + @vcTaxName
                    + ']= dbo.fn_CalBizDocTaxAmt('
                    + CONVERT(VARCHAR(20), @numDomainID) + ','
                    + CONVERT(VARCHAR(20), @numTaxItemID) + ','
                    + CONVERT(VARCHAR(20), @numOppId) +','+
                    + 'numoppitemtCode,1)*Amount/100'
            --ELSE 
            --    SET @strSQLUpdate = @strSQLUpdate + ',[' + @vcTaxName + ']= 0'

            SELECT TOP 1
                    @vcTaxName = vcTaxName,
                    @numTaxItemID = numTaxItemID
            FROM    TaxItems
            WHERE   numDomainID = @numDomainID
                    AND numTaxItemID > @numTaxItemID

            IF @@rowcount = 0 
                SET @numTaxItemID = 0


        END

    IF @strSQLUpdate <> '' 
        BEGIN
            SET @strSQLUpdate = 'update #Temp1 set ItemCode=ItemCode'
                + @strSQLUpdate + ' where ItemCode>0'
            PRINT @strSQLUpdate
            EXEC ( @strSQLUpdate
                )
        END

    DECLARE @vcDbColumnName NVARCHAR(50)
    SELECT TOP 1
            @numFldID = numfieldID,
            @vcFldname = vcFieldName,
            @vcDbColumnName = vcDbColumnName,
            @intRowNum = ( tintRow + 1 )
    FROM    View_DynamicCustomColumns
    WHERE   numFormID = @tintType
            AND Grp_id = 5
            AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId
            AND bitCustom = 1
            AND ISNULL(numRelCntType, 0) = @numBizDocTempID
    ORDER BY tintRow                                                                                          
    WHILE @intRowNum > 0                                                                                  
        BEGIN    
            EXEC
                ( 'alter table #Temp1 add [' + @vcDbColumnName
                  + '] varchar(100)'
                )
	
            SET @strSQLUpdate = 'update #Temp1 set ItemCode=ItemCode,['
                + @vcDbColumnName + ']=dbo.GetCustFldValueItem(' + @numFldID
                + ',ItemCode) where ItemCode>0'
            EXEC ( @strSQLUpdate
                )
		                   
            SELECT TOP 1
                    @numFldID = numfieldID,
                    @vcFldname = vcFieldName,
                    @vcDbColumnName = vcDbColumnName,
                    @intRowNum = ( tintRow + 1 )
            FROM    View_DynamicCustomColumns
            WHERE   numFormID = @tintType
                    AND Grp_id = 5
                    AND numDomainID = @numDomainID
                    AND numAuthGroupID = @numBizDocId
                    AND bitCustom = 1
                    AND tintRow >= @intRowNum
                    AND ISNULL(numRelCntType, 0) = @numBizDocTempID
            ORDER BY tintRow                                                                  
		           
            IF @@rowcount = 0 
                SET @intRowNum = 0                                                                                          
        END


    SELECT  Row_NUmber() OVER ( ORDER BY numoppitemtCode ) RowNumber,
            *
    FROM    #Temp1
    ORDER BY numoppitemtCode ASC 

    DROP TABLE #Temp1

    SELECT  ISNULL(tintOppStatus, 0)
    FROM    OpportunityMaster
    WHERE   numOppID = @numOppId                                                                                                 


IF (SELECT COUNT(*) FROM View_DynamicColumns WHERE   numFormID = @tintType AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId AND vcFieldType = 'R'AND ISNULL(numRelCntType, 0) = @numBizDocTempID)=0
BEGIN            
	INSERT INTO DycFormConfigurationDetails (numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numAuthGroupID,numRelCntType)
	select numFormId,numFieldId,1,tintRow,@numDomainID,@numBizDocId,@numBizDocTempID
	 FROM View_DynamicDefaultColumns
	 where numFormId=@tintType and bitDefault=1 and numDomainID=@numDomainID order by tintRow ASC	
	 
	 
	 IF(SELECT COUNT(*) FROM DycFrmConfigBizDocsSumm Ds where numBizDocID=@numBizDocId and Ds.numFormID=@tintType 
	and isnull(Ds.numBizDocTempID,0)=@numBizDocTempID and Ds.numDomainID=@numDomainID)=0
	BEGIN
		INSERT INTO DycFrmConfigBizDocsSumm (numFormID,numBizDocID,numFieldID,numDomainID,numBizDocTempID)
		 SELECT @tintType,@numBizDocId,DFM.numFieldID,@numDomainID,@numBizDocTempID FROM
		 DycFormField_Mapping DFFM 
			JOIN DycFieldMaster DFM on DFFM.numModuleID=DFFM.numModuleID and DFFM.numFieldID=DFM.numFieldID
			where numFormID=16 and DFFM.numDomainID is null
	END

END      


    SELECT  vcFieldName AS vcFormFieldName,
            vcDbColumnName,
            vcFieldDataType,
            tintRow AS intRowNum
    FROM    View_DynamicColumns
    WHERE   numFormID = @tintType
            AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId
            AND vcFieldType = 'R'
            AND ISNULL(numRelCntType, 0) = @numBizDocTempID
    UNION
    SELECT  vcFieldName AS vcFormFieldName,
            vcDbColumnName,
            'C' vcFieldDataType,
            tintRow AS intRowNum
    FROM    View_DynamicCustomColumns
    WHERE   numFormID = @tintType
            AND Grp_id = 5
            AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId
            AND bitCustom = 1
            AND ISNULL(numRelCntType, 0) = @numBizDocTempID
    ORDER BY tintRow
GO
/****** Object:  StoredProcedure [dbo].[USP_OppBizDocs]    Script Date: 03/25/2009 15:23:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_oppbizdocs')
DROP PROCEDURE usp_oppbizdocs
GO
CREATE PROCEDURE [dbo].[USP_OppBizDocs]
(                        
 @byteMode as tinyint=null,                        
 @numOppId as numeric(9)=null,                        
 @numOppBizDocsId as numeric(9)=null,
 @ClientTimeZoneOffset AS INT=0
)                        
as                        
                        
DECLARE @numDomainID NUMERIC
                                          
if @byteMode= 1                        
begin 

BEGIN TRANSACTION
DELETE FROM [ProjectsOpportunities] WHERE numOppBizDocID=@numOppBizDocsId
--delete from OpportunityBizDocTaxItems where numOppBizDocID=@numOppBizDocsId

DELETE FROM [ShippingReportItems] WHERE [numShippingReportId] IN  (SELECT numShippingReportId from [ShippingReport] WHERE [numOppBizDocId] = @numOppBizDocsId)
DELETE FROM [ShippingBox] WHERE [numShippingReportId] IN  (SELECT numShippingReportId from [ShippingReport] WHERE [numOppBizDocId] = @numOppBizDocsId)
DELETE FROM [ShippingReport] WHERE [numOppBizDocId] = @numOppBizDocsId
SELECT @numDomainID = OM.numDOmainID FROM [OpportunityBizDocs] INNER JOIN [OpportunityMaster] OM ON [OpportunityBizDocs].[numOppId] = OM.[numOppId] WHERE [numOppBizDocsId]=@numOppBizDocsId
 --Delete All entries for current bizdoc
EXEC [USP_DeleteEmbeddedCost] @numOppBizDocsId, 0, @numDomainID 
--DELETE FROM [OpportunityBizDocsPaymentDetails] WHERE [numBizDocsPaymentDetId] IN (SELECT  [numBizDocsPaymentDetId] FROM [OpportunityBizDocsDetails] WHERE [numBizDocsPaymentDetId] IN (SELECT [numBizDocsPaymentDetId] FROM [EmbeddedCost] WHERE [numOppBizDocID]=@numOppBizDocsId))
--DELETE FROM [EmbeddedCostItems] WHERE [numEmbeddedCostID] IN (SELECT [numEmbeddedCostID] FROM [EmbeddedCost] WHERE [numOppBizDocID] = @numOppBizDocsId)
--DELETE FROM [EmbeddedCost] WHERE [numOppBizDocID] = @numOppBizDocsId
--DELETE FROM [OpportunityBizDocsDetails] WHERE [numBizDocsPaymentDetId] IN (SELECT [numBizDocsPaymentDetId] FROM [EmbeddedCost] WHERE [numOppBizDocID]=@numOppBizDocsId)


delete  from OpportunityBizDocItems where   numOppBizDocID=  @numOppBizDocsId              

--Delete Deferred BizDoc Recurring Entries
delete from OpportunityRecurring where numOppBizDocID=@numOppBizDocsId and tintRecurringType=4

UPDATE dbo.OpportunityRecurring SET numOppBizDocID=NULL WHERE numOppBizDocID=@numOppBizDocsId

DELETE FROM [RecurringTransactionReport] WHERE [numRecTranBizDocID] = @numOppBizDocsId

--Credit Balance
Declare @monCreditAmount as money;Set @monCreditAmount=0
Declare @numDivisionID as numeric(9);Set @numDivisionID=0

Select @monCreditAmount=isnull(oppBD.monCreditAmount,0),@numDivisionID=Om.numDivisionID,
@numOppId=OM.numOppId 
from OpportunityBizDocs oppBD join OpportunityMaster Om on OM.numOppId=oppBD.numOppId WHERE  numOppBizDocsId = @numOppBizDocsId

delete from [CreditBalanceHistory] where numOppBizDocsId=@numOppBizDocsId

Declare @tintOppType as tinyint
Select @tintOppType=tintOppType from OpportunityMaster WHERE  numOppId=@numOppId  and numDomainID= @numDomainID 
 
IF @tintOppType=1 --Sales
	update DivisionMaster set monSCreditBalance=isnull(monSCreditBalance,0) + isnull(@monCreditAmount,0) where numDivisionID=@numDivisionID and numDomainID=@numDomainID
else IF @tintOppType=2 --Purchase
	update DivisionMaster set monPCreditBalance=isnull(monPCreditBalance,0) + isnull(@monCreditAmount,0) where numDivisionID=@numDivisionID and numDomainID=@numDomainID


delete from DocumentWorkflow where numDocID=@numOppBizDocsId and cDocType='B'      

delete from BizDocAction where numBizActionId in(select numBizActionId from BizActionDetails where numOppBizDocsId =@numOppBizDocsId and btDocType=1) 
delete from BizActionDetails where numOppBizDocsId =@numOppBizDocsId and btDocType=1

delete from OpportunityBizDocs where numOppBizDocsId=@numOppBizDocsId                        


-- Rollback the transaction if there were any errors
IF @@ERROR <> 0
 BEGIN
    -- Rollback the transaction
    ROLLBACK
    RETURN
 END
 
 
COMMIT

                    
end                        
                        
if @byteMode= 2                        
begin  
SELECT @numDomainID = numDOmainID FROM [dbo].[OpportunityMaster] 
WHERE [numOppId] = @numOppId
--PRINT @numDomainID

DECLARE @BaseCurrencySymbol nVARCHAR(3);SET @BaseCurrencySymbol='$'
SELECT @BaseCurrencySymbol = ISNULL(varCurrSymbol,'$') FROM dbo.Currency 
														   WHERE numCurrencyID IN ( SELECT numCurrencyID FROM dbo.Domain 
																					WHERE numDomainId=@numDomainId)
--PRINT @BaseCurrencySymbol

																					                      
select *, isnull(X.monPAmount,0)-X.monAmountPaid as BalDue from (select  opp.numOppBizDocsId,                        
 opp.numOppId,                        
 opp.numBizDocId,                        
 oppmst.vcPOppName,                        
 mst.vcData as BizDoc,                        
 opp.numCreatedBy,                        
DATEADD(minute,-@ClientTimeZoneOffset,opp.dtCreatedDate) CreatedDate,
 opp.vcRefOrderNo,                       
 vcBizDocID,
 CASE WHEN LEN(ISNULL(vcBizDocName,''))=0 THEN vcBizDocID ELSE vcBizDocName END vcBizDocName,      
  [dbo].[GetDealAmount](@numOppId ,getutcdate(),opp.numOppBizDocsId ) as monPAmount,
  case  when tintOppType=1  then (case when isnull(numAuthoritativeSales,0)=numBizDocId then 'Authoritative' + Case when isnull(Opp.tintDeferred,0)=1 then ' (Deferred)' else '' end else 'Non-Authoritative' end ) 
  when tintOppType=2  then (case when isnull(numAuthoritativePurchase,0)=numBizDocId then 'Authoritative' + Case when isnull(Opp.tintDeferred,0)=1 then ' (Deferred)' else '' end else 'Non-Authoritative' end )  end as BizDocType,
  isnull(monAmountPaid,0) monAmountPaid, 
  ISNULL(C.varCurrSymbol,'') varCurrSymbol ,
  ISNULL(Opp.[bitAuthoritativeBizDocs],0) bitAuthoritativeBizDocs,
  ISNULL(oppmst.[tintshipped] ,0) tintshipped,isnull(opp.numShipVia,0) as numShipVia,oppmst.numDivisionId,isnull(Opp.tintDeferred,0) as tintDeferred,
  ISNULL(Opp.numBizDocTempID,0) AS numBizDocTempID,con.numContactid,isnull(con.vcEmail,'') AS vcEmail,ISNULL(Opp.numBizDocStatus,0) AS numBizDocStatus,
--  case when isnumeric(ISNULL(dbo.fn_GetListItemName(isnull(oppmst.intBillingDays,0)),0))=1 
--	   then ISNULL(dbo.fn_GetListItemName(isnull(oppmst.intBillingDays,0)),0) 
--	   else 0 
--  end AS numBillingDaysName,
  CASE WHEN ISNUMERIC((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(oppmst.intBillingDays,0))) = 1
 	   THEN (SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(oppmst.intBillingDays,0))
	   ELSE 0
  END AS numBillingDaysName,
  Opp.dtFromDate,
  opp.dtFromDate AS BillingDate,
  ISNULL(C.fltExchangeRate,1) fltExchangeRateCurrent,
  ISNULL(oppmst.fltExchangeRate,1) fltExchangeRateOfOrder,
  @BaseCurrencySymbol BaseCurrencySymbol,0 AS numReturnHeaderID,0 AS tintReturnType,ISNULL(Opp.fltExchangeRateBizDoc,ISNULL(oppmst.fltExchangeRate,1)) AS fltExchangeRateBizDoc,
  CASE WHEN (SELECT COUNT(*) FROM dbo.ShippingReport WHERE numOppBizDocId = opp.numOppBizDocsId AND numDomainID = @numDomainID) > 0 THEN 1 ELSE 0 END AS [IsShippingReportGenerated],
  --CASE WHEN (SELECT ISNULL(vcTrackingNo,'') FROM dbo.OpportunityBizDocs WHERE numOppBizDocsId = opp.numOppBizDocsId) <> '' THEN 1 ELSE 0 END AS [ISTrackingNumGenerated]
  CASE WHEN (SELECT COUNT(*) FROM dbo.ShippingBox 
			 WHERE numShippingReportId IN (SELECT numShippingReportId FROM dbo.ShippingReport WHERE numOppBizDocId = opp.numOppBizDocsId AND numDomainID = @numDomainID)
			 AND LEN(ISNULL(vcTrackingNumber,'')) > 0) > 0 THEN 1 ELSE 0 END AS [ISTrackingNumGenerated],
  ISNULL((SELECT MAX(numShippingReportId) from ShippingReport SR 
		  WHERE SR.numOppBizDocId = opp.numoppbizdocsid 
		  AND SR.numDomainID = oppmst.numDomainID),0) AS numShippingReportId,
  ISNULL((SELECT MAX(SB.numShippingReportId) FROM [ShippingBox] SB 
		  JOIN dbo.ShippingReport SR ON SB.numShippingReportId = SR.numShippingReportId 
		  where SR.numOppBizDocId = opp.numoppbizdocsid AND SR.numDomainID = oppmst.numDomainID 
		  AND LEN(ISNULL(SB.vcTrackingNumber,''))>0),0) AS numShippingLabelReportId
 ,ISNULL(STUFF(( SELECT  ',' +  CASE WHEN LEN(ISNULL(W.[vcWareHouse],'')  + ':' + ISNULL([WL].[vcLocation],'')) > 0 THEN ISNULL(W.[vcWareHouse],'')  + ':' + ISNULL([WL].[vcLocation],'') ELSE '' END
                FROM    [dbo].[WarehouseLocation] AS WL
				JOIN [dbo].[Warehouses] W ON [WL].[numWarehouseID] = [W].[numWareHouseID]
                WHERE  [WL].[numWLocationID] IN (
                        SELECT  WHI.[numWLocationID]
                        FROM    [dbo].[WareHouseItems] AS WHI
                                JOIN [dbo].[OpportunityItems] AS OI ON [WHI].[numWareHouseItemID] = [OI].[numWarehouseItmsID]
                        WHERE   [OI].[numOppId] = oppmst.[numOppId])
              FOR
                XML PATH('')
              ), 1, 1, ''),'') AS [vcLocation]
 FROM                         
 OpportunityBizDocs  opp                        
 left join ListDetails mst                        
 on mst.numListItemID=opp.numBizDocId                        
 join OpportunityMaster oppmst                        
 on oppmst.numOppId= opp.numOppId                                
 left join  AuthoritativeBizDocs  AB
 on AB.numDomainId=  oppmst.numDomainID             
 LEFT OUTER JOIN [Currency] C
  ON C.numCurrencyID = oppmst.numCurrencyID
  JOIN divisionmaster div ON div.numDivisionId=oppmst.numDivisionId
  JOIN AdditionalContactsInformation con ON con.numdivisionId = div.numdivisionId AND con.numContactid = oppmst.numContactId
where opp.numOppId=@numOppId

UNION ALL

select  0 AS numOppBizDocsId,                        
 RH.numOppId,                        
 0 AS numBizDocId,                        
 '' AS vcPOppName,                        
 'RMA' as BizDoc,                        
 RH.numCreatedBy,                        
DATEADD(minute,-@ClientTimeZoneOffset,RH.dtCreatedDate) CreatedDate,
 '' vcRefOrderNo,                       
 RH.vcRMA AS vcBizDocID,
 RH.vcRMA AS vcBizDocName,      
  ISNULL(monAmount,0) as monPAmount,
  'RMA' as BizDocType,
  0 AS monAmountPaid, 
  '' varCurrSymbol ,
  0 bitAuthoritativeBizDocs,
  0 tintshipped,0 as numShipVia,RH.numDivisionId,0 as tintDeferred,
  0 AS numBizDocTempID,con.numContactid,isnull(con.vcEmail,'') AS vcEmail,0 AS numBizDocStatus,
  0 AS numBillingDaysName,RH.dtCreatedDate AS dtFromDate,
  RH.dtCreatedDate AS BillingDate,
  1 fltExchangeRateCurrent,
  1 fltExchangeRateOfOrder,
  @BaseCurrencySymbol BaseCurrencySymbol,RH.numReturnHeaderID,RH.tintReturnType,1 AS fltExchangeRateBizDoc
  ,0 [IsShippingReportGenerated],0 [ISTrackingNumGenerated],
  0 AS numShippingReportId,
  0 AS numShippingLabelReportId
  ,ISNULL(STUFF(( SELECT  ',' +  CASE WHEN LEN(ISNULL(W.[vcWareHouse],'')  + ':' + ISNULL([WL].[vcLocation],'')) > 0 THEN ISNULL(W.[vcWareHouse],'')  + ':' + ISNULL([WL].[vcLocation],'') ELSE '' END
                FROM    [dbo].[WarehouseLocation] AS WL
				JOIN [dbo].[Warehouses] W ON [WL].[numWarehouseID] = [W].[numWareHouseID]
                WHERE  [WL].[numWLocationID] IN (
                        SELECT  WHI.[numWLocationID]
                        FROM    [dbo].[WareHouseItems] AS WHI
                                JOIN [dbo].[OpportunityItems] AS OI ON [WHI].[numWareHouseItemID] = [OI].[numWarehouseItmsID]
                        WHERE   [OI].[numOppId] = RH.[numOppId])
              FOR
                XML PATH('')
              ), 1, 1, ''),'') AS [vcLocation]			 
from                         
 ReturnHeader RH                        
  JOIN divisionmaster div ON div.numDivisionId=RH.numDivisionId
  LEFT JOIN AdditionalContactsInformation con ON con.numdivisionId = div.numdivisionId AND con.numContactid = RH.numContactId
where RH.numOppId=@numOppId
)X         
                        
END



/****** Object:  StoredProcedure [dbo].[USP_OPPDetails]    Script Date: 03/25/2009 15:10:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Created By Anoop Jayaraj
-- [dbo].[USP_OPPDetails] 1362,1,-333
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_oppdetails')
DROP PROCEDURE usp_oppdetails
GO
CREATE PROCEDURE [dbo].[USP_OPPDetails]
(
               @numOppID             AS NUMERIC(9)  = NULL,
               @numDomainID          AS NUMERIC(9)  = 0,
               @ClientTimeZoneOffset AS INT)
AS
	
  SELECT Opp.numoppid,
         numCampainID,
         ADC.vcFirstname + ' ' + ADC.vcLastName AS numContactId,
         isnull(ADC.vcEmail,'') AS vcEmail,
         Opp.txtComments,
         Opp.numDivisionID,
         tintOppType,
         ISNULL(dateadd(MINUTE,-@ClientTimeZoneOffset,bintAccountClosingDate),'') AS bintAccountClosingDate,
         Opp.numContactID AS ContactID,
         tintSource,
         C2.vcCompanyName + Case when isnull(D2.numCompanyDiff,0)>0 then  '  ' + dbo.fn_getlistitemname(D2.numCompanyDiff) + ':' + isnull(D2.vcCompanyDiff,'') else '' end as vcCompanyname,
         D2.tintCRMType,
         Opp.vcPoppName,
         intpEstimatedCloseDate,
         [dbo].[GetDealAmount](@numOppID,getutcdate(),0) AS monPAmount,
         lngPConclAnalysis,
         monPAmount AS OppAmount,
         numSalesOrPurType,
         Opp.tintActive,
         dbo.fn_GetContactName(Opp.numCreatedby)
           + ' '
           + CONVERT(VARCHAR(20),dateadd(MINUTE,-@ClientTimeZoneOffset,Opp.bintCreatedDate)) AS CreatedBy,
         dbo.fn_GetContactName(Opp.numModifiedBy)
           + ' '
           + CONVERT(VARCHAR(20),dateadd(MINUTE,-@ClientTimeZoneOffset,Opp.bintModifiedDate)) AS ModifiedBy,
         dbo.fn_GetContactName(Opp.numRecOwner) AS RecordOwner,
         Opp.numRecOwner,
         isnull(tintshipped,0) AS tintshipped,
         isnull(tintOppStatus,0) AS tintOppStatus,
         dbo.OpportunityLinkedItems(@numOppID) AS NoOfProjects,
         Opp.numAssignedTo,
         Opp.numAssignedBy,
         (SELECT COUNT(* )
          FROM   dbo.GenericDocuments
          WHERE  numRecID = @numOppID
                 AND vcDocumentSection = 'O') AS DocumentCount,
         isnull(OPR.numRecurringId,0) AS numRecurringId,
		 OPR.dtRecurringDate AS dtLastRecurringDate,
         D2.numTerID,
         Link.numOppID AS numParentOppID,
         Link.vcPoppName AS ParentOppName,
         Link.vcSource AS Source,
         Link.numParentProjectID,
         Link.vcProjectName,
         ISNULL(C.varCurrSymbol,'') varCurrSymbol,
         ISNULL(Opp.fltExchangeRate,0) AS [fltExchangeRate],
         ISNULL(C.chrCurrency,'') AS [chrCurrency],
         ISNULL(C.numCurrencyID,0) AS [numCurrencyID],
		 (Select Count(*) from OpportunityBizDocs 
		 where numOppId=@numOppID and bitAuthoritativeBizDocs=1) As AuthBizDocCount,
		 (SELECT COUNT(distinct numChildOppID) FROM [OpportunityLinking] OL WHERE OL.[numParentOppID] = Opp.numOppId) AS ChildCount,
		 (SELECT COUNT(*) FROM [RecurringTransactionReport] RTR WHERE RTR.[numRecTranSeedId] = Opp.numOppId)  AS RecurringChildCount
		 ,Opp.bintCreatedDate,
		 ISNULL(Opp.bitStockTransfer ,0) bitStockTransfer,	ISNULL(Opp.tintTaxOperator,0) AS tintTaxOperator, 
         isnull(Opp.intBillingDays,0) AS numBillingDays,isnull(Opp.bitInterestType,0) AS bitInterestType,
         isnull(opp.fltInterest,0) AS fltInterest,  isnull(Opp.fltDiscount,0) AS fltDiscount,
         isnull(Opp.bitDiscountType,0) AS bitDiscountType,ISNULL(Opp.bitBillingTerms,0) AS bitBillingTerms,ISNULL(OPP.numDiscountAcntType,0) AS numDiscountAcntType,
         (SELECT TOP 1 numCountry FROM fn_getOPPAddressDetails(Opp.numOppId,Opp.numDomainID,2)) AS [numShipCountry],
		 (SELECT TOP 1 numState FROM fn_getOPPAddressDetails(Opp.numOppId,Opp.numDomainID,2)) AS [numShipState],
		 (SELECT TOP 1 vcState FROM fn_getOPPAddressDetails(Opp.numOppId,Opp.numDomainID,2)) AS [vcShipState],
		 (SELECT TOP 1 vcCountry FROM fn_getOPPAddressDetails(Opp.numOppId,Opp.numDomainID,2)) AS [vcShipCountry],
		 (SELECT TOP 1 vcPostalCode FROM fn_getOPPAddressDetails(Opp.numOppId,Opp.numDomainID,2)) AS [vcPostalCode],
		 ISNULL(vcCouponCode,'') AS [vcCouponCode],ISNULL(Opp.bitPPVariance,0) AS bitPPVariance,
		 Opp.dtItemReceivedDate,ISNULL(bitUseShippersAccountNo,0) [bitUseShippersAccountNo], ISNULL(vcShippersAccountNo,'') [vcShippersAccountNo],
		 ISNULL(bitUseMarkupShippingRate,0) [bitUseMarkupShippingRate],
		 ISNULL(numMarkupShippingRate,0) [numMarkupShippingRate],
		 ISNULL(intUsedShippingCompany,0) [intUsedShippingCompany],ISNULL(Opp.[monLandedCostTotal],0) AS monLandedCostTotal,opp.[vcLanedCost]
  FROM   OpportunityMaster Opp 
         JOIN divisionMaster D2
           ON Opp.numDivisionID = D2.numDivisionID
         LEFT JOIN CompanyInfo C2
           ON C2.numCompanyID = D2.numCompanyID
         LEFT JOIN (SELECT TOP 1 OM.vcPoppName,
                                 OM.numOppID,
                                 numChildOppID,
                                 vcSource,
                                 numParentProjectID,
                                 vcProjectName
                    FROM   OpportunityLinking
                          LEFT JOIN OpportunityMaster OM
                             ON numOppID = numParentOppID
                          LEFT JOIN [ProjectsMaster]
							 ON numParentProjectID = numProId   
                    WHERE  numChildOppID = @numOppID) Link
           ON Link.numChildOppID = Opp.numOppID
         LEFT OUTER JOIN [Currency] C
					ON C.numCurrencyID = Opp.numCurrencyID
		 LEFT OUTER JOIN [OpportunityRecurring] OPR
					ON OPR.numOppId = Opp.numOppId
		 LEFT OUTER JOIN additionalContactsinformation ADC ON
		 ADC.numdivisionId = D2.numdivisionId AND  ADC.numContactId = Opp.numContactId		
  WHERE  Opp.numOppId = @numOppID
         AND Opp.numDomainID = @numDomainID
/****** Object:  StoredProcedure [dbo].[USP_OPPItemDetails]    Script Date: 09/01/2009 18:59:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- exec USP_OPPItemDetails @numItemCode=31455,@OpportunityId=12387,@numDomainId=110
--created by anooop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_oppitemdetails')
DROP PROCEDURE usp_oppitemdetails
GO
CREATE PROCEDURE [dbo].[USP_OPPItemDetails]
    (
      @numUserCntID NUMERIC(9) = 0,
      @OpportunityId NUMERIC(9) = 0,
      @numDomainId NUMERIC(9),
      @ClientTimeZoneOffset  INT
    )
AS 
    BEGIN
        IF @OpportunityId >0
            BEGIN
                DECLARE @DivisionID AS NUMERIC(9)
                DECLARE @TaxPercentage AS FLOAT
                DECLARE @tintOppType AS TINYINT
                DECLARE @fld_Name AS VARCHAR(500)
                DECLARE @strSQL AS VARCHAR(8000)
                DECLARE @fld_ID AS NUMERIC(9)
                DECLARE @numBillCountry AS NUMERIC(9)
                DECLARE @numBillState AS NUMERIC(9)
                
                SELECT  @DivisionID = numDivisionID,
                        @tintOppType = tintOpptype
                FROM    OpportunityMaster
                WHERE   numOppId = @OpportunityId
               -- Calculate Tax based on Country and state
             /*  SELECT  @numBillState = ISNULL(numState, 0),
						@numBillCountry = ISNULL(numCountry, 0)
			   FROM     dbo.AddressDetails
			   WHERE    numDomainID = @numDomainID
						AND numRecordID = @DivisionID
						AND tintAddressOf = 2
						AND tintAddressType = 1
						AND bitIsPrimary=1
                SET @TaxPercentage = 0
                IF @numBillCountry > 0
                    AND @numBillState > 0 
                    BEGIN
                        IF @numBillState > 0 
                            BEGIN
                                IF EXISTS ( SELECT  COUNT(*)
                                            FROM    TaxDetails
                                            WHERE   numCountryID = @numBillCountry
                                                    AND numStateID = @numBillState
                                                    AND numDomainID = @numDomainID ) 
                                    SELECT  @TaxPercentage = decTaxPercentage
                                    FROM    TaxDetails
                                    WHERE   numCountryID = @numBillCountry
                                            AND numStateID = @numBillState
                                            AND numDomainID = @numDomainID
                                ELSE 
                                    SELECT  @TaxPercentage = decTaxPercentage
                                    FROM    TaxDetails
                                    WHERE   numCountryID = @numBillCountry
                                            AND numStateID = 0
                                            AND numDomainID = @numDomainID
                            END
                    END
                ELSE 
                    IF @numBillCountry > 0 
                        SELECT  @TaxPercentage = decTaxPercentage
                        FROM    TaxDetails
                        WHERE   numCountryID = @numBillCountry
                                AND numStateID = 0
                                AND numDomainID = @numDomainID
               
               */
               

SET @strSQL = ''

--------------Make Dynamic Query--------------
SET @strSQL = @strSQL + '
Select 
Opp.numoppitemtCode,
OM.tintOppType,
Opp.numItemCode,
I.charItemType,
Opp.numOppId,
Opp.bitWorkOrder,
Opp.fltDiscount,
ISNULL(( SELECT numReturnID FROM   Returns WHERE  numOppItemCode = Opp.numoppitemtCode AND numOppId = Opp.numOppID), 0) AS numReturnID,
dbo.fn_GetReturnItemQty(Opp.numoppitemtCode) AS ReturrnedQty,
[dbo].fn_GetReturnStatus(Opp.numoppitemtCode) AS ReturnStatus,
ISNULL(opp.numUOMId, 0) AS numUOM,
(SELECT vcUnitName FROM dbo.UOM WHERE numUOMId = opp.numUOMId AND ISNULL(bitEnabled,0) = 1)  AS  [vcBaseUOMName],
Opp.numUnitHour AS numOriginalUnitHour, 
isnull(M.vcPOppName,''Internal'') as Source,
numSourceID,
  CASE WHEN ISNULL(bitKitParent,0) = 1
                               AND ISNULL(bitAssembly,0) = 1 THEN 0
                          WHEN ISNULL(bitKitParent,0) = 1 THEN 1
                          ELSE 0
                     END as bitKitParent,
(SELECT vcData FROM dbo.ListDetails WHERE 1=1 AND numListItemID = numShipClass ) AS [numShipClass],ISNULL(Opp.numQtyShipped,0) numQtyShipped,ISNULL(Opp.numUnitHourReceived,0) AS numUnitHourReceived,ISNULL(OM.fltExchangeRate,0) AS fltExchangeRate,ISNULL(OM.numCurrencyID,0) AS numCurrencyID,ISNULL(OM.numDivisionID,0) AS numDivisionID,
isnull(i.numIncomeChartAcntId,0) as itemIncomeAccount,isnull(i.numAssetChartAcntId,0) as itemInventoryAsset,isnull(i.numCOGsChartAcntId,0) as itemCoGs,
(SELECT vcData FROM dbo.ListDetails WHERE 1=1 AND numListItemID = numShipClass ) AS [numShipClass],ISNULL(bitFreeShipping,0) AS [IsFreeShipping],Opp.bitDiscountType,isnull(Opp.monLandedCost,0) as monLandedCost, 
'

--	IF @fld_Name = 'vcPathForTImage' 
		SET @strSQL = @strSQL + 'Opp.vcPathForTImage,'
--	ELSE IF @fld_Name = 'vcItemName' 
		SET @strSQL = @strSQL + 'Opp.vcItemName,'
--	ELSE IF @fld_Name = 'vcModelID,' 
		SET @strSQL = @strSQL + 'Opp.vcModelID,'
--	ELSE IF @fld_Name = 'vcWareHouse' 
		SET @strSQL = @strSQL + 'vcWarehouse,'
		SET @strSQL = @strSQL + 'WItems.numOnHand,isnull(WItems.numAllocation,0) as numAllocation,'
		SET @strSQL = @strSQL + 'WL.vcLocation,'
--	ELSE IF @fld_Name = 'ItemType' 
		SET @strSQL = @strSQL + 'Opp.vcType ItemType,Opp.vcType,'
--	ELSE IF @fld_Name = 'vcItemDesc' 
		SET @strSQL = @strSQL + 'ISNULL(Opp.vcItemDesc, '''') vcItemDesc,'
--	ELSE IF @fld_Name = 'Attributes' 
		SET @strSQL = @strSQL + 'Opp.vcAttributes,' --dbo.fn_GetAttributes(Opp.numWarehouseItmsID, I.bitSerialized) AS Attributes,
--	ELSE IF @fld_Name = 'bitDropShip' 
		SET @strSQL = @strSQL + 'ISNULL(bitDropShip, 0) as DropShip,CASE ISNULL(bitDropShip, 0) WHEN 1 THEN ''Yes'' ELSE '''' END AS bitDropShip,'
--	ELSE IF @fld_Name = 'numUnitHour' 
		SET @strSQL = @strSQL + 'CAST(( dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), Opp.numItemCode,om.numDomainId, ISNULL(opp.numUOMId, 0))* Opp.numUnitHour ) AS NUMERIC(18, 2)) AS numUnitHour,'
--	ELSE IF @fld_Name = 'monPrice' 
		SET @strSQL = @strSQL + 'Opp.monPrice,'
--	ELSE IF @fld_Name = 'monTotAmount' 
		SET @strSQL = @strSQL + 'Opp.monTotAmount,'
--	ELSE IF @fld_Name = 'vcSKU' 
		SET @strSQL = @strSQL + 'ISNULL(WItems.vcWHSKU,ISNULL(I.vcSKU,'''')) [vcSKU],'
--	ELSE IF @fld_Name = 'vcManufacturer' 
		SET @strSQL = @strSQL + 'Opp.vcManufacturer,'
--	ELSE IF @fld_Name = 'vcItemClassification' 
		SET @strSQL = @strSQL + ' dbo.[fn_GetListItemName](I.[numItemClassification]) numItemClassification,'
--	ELSE IF @fld_Name = 'numItemGroup' 
		SET @strSQL = @strSQL + '( SELECT    [vcItemGroup] FROM      [ItemGroups] WHERE     [numItemGroupID] = I.numItemGroup ) numItemGroup,'
--	ELSE IF @fld_Name = 'vcUOMName' 
		SET @strSQL = @strSQL + 'ISNULL(u.vcUnitName, '''') numUOMId,'
--	ELSE IF @fld_Name = 'fltWeight' 
		SET @strSQL = @strSQL + 'I.fltWeight,'
--	ELSE IF @fld_Name = 'fltHeight' 
		SET @strSQL = @strSQL + 'I.fltHeight,'
--	ELSE IF @fld_Name = 'fltWidth' 
		SET @strSQL = @strSQL + 'I.fltWidth,'
--	ELSE IF @fld_Name = 'fltLength' 
		SET @strSQL = @strSQL + 'I.fltLength,'
--	ELSE IF @fld_Name = 'numBarCodeId' 
		SET @strSQL = @strSQL + 'I.numBarCodeId,'
		SET @strSQL = @strSQL + 'I.IsArchieve,'
--	ELSE IF @fld_Name = 'vcWorkOrderStatus' 
		SET @strSQL = @strSQL + 'CASE WHEN ISNULL(Opp.bitWorkOrder, 0) = 1
				 THEN ( SELECT  dbo.GetListIemName(numWOStatus)
								+ CASE WHEN ISNULL(numWOStatus, 0) = 23184
									   THEN '' - ''
											+ CAST(ISNULL(dbo.fn_GetContactName(numAssignedTo),
														  '''') + '',''
											+ CONVERT(VARCHAR(20), DATEADD(minute, '+ CONVERT(VARCHAR(15),-@ClientTimeZoneOffset)  +', bintCompletedDate)) AS VARCHAR(100))
									   ELSE ''''
								  END
						FROM    WorkOrder
						WHERE   ISNULL(numOppId, 0) = Opp.numOppId
								AND numItemCode = Opp.numItemCode
					  )
				 ELSE ''''
			END AS vcWorkOrderStatus,'
--	ELSE IF @fld_Name = 'DiscAmt' 
		SET @strSQL = @strSQL + 'CASE WHEN Opp.bitDiscountType = 0
				 THEN CAST(ISNULL(Opp.monTotAmtBefDiscount, Opp.monTotAmount)
					  - Opp.monTotAmount AS VARCHAR(10)) + '' (''
					  + CAST(Opp.fltDiscount AS VARCHAR(10)) + ''%)''
				 ELSE CAST(ISNULL(Opp.monTotAmtBefDiscount, Opp.monTotAmount)
					  - Opp.monTotAmount AS VARCHAR(10))
			END AS DiscAmt,'	
--	ELSE IF @fld_Name = 'numProjectID' 
		SET @strSQL = @strSQL + ' ISNULL(Opp.numProjectID, 0) numProjectID,
		 CASE WHEN ( SELECT TOP 1
								ISNULL(OBI.numOppBizDocItemID, 0)
						FROM    dbo.OpportunityBizDocs OBD
								INNER JOIN dbo.OpportunityBizDocItems OBI ON OBD.numOppBizDocsID = OBI.numOppBizDocID
						WHERE   OBI.numOppItemID = Opp.numoppitemtCode
								AND ISNULL(OBD.bitAuthoritativeBizDocs, 0) = 1
								and isnull(OBD.numOppID,0)=OM.numOppID
					  ) > 0 THEN 1
				 ELSE 0
			END AS bitItemAddedToAuthBizDoc,'	
--	ELSE IF @fld_Name = 'numClassID' 
		SET @strSQL = @strSQL + 'ISNULL(Opp.numClassID, 0) numClassID,'	
--	ELSE IF @fld_Name = 'vcProjectStageName' 
		SET @strSQL = @strSQL + ' ISNULL(Opp.numProjectStageID, 0) numProjectStageID,
				CASE WHEN OPP.numProjectStageID > 0
				 THEN dbo.fn_GetProjectStageName(OPP.numProjectId,
												 OPP.numProjectStageID)
				 WHEN OPP.numProjectStageID = -1 THEN ''T&E''
				 WHEN OPP.numProjectId > 0
				 THEN CASE WHEN OM.tintOppType = 1 THEN ''S.O.''
						   ELSE ''P.O.''
					  END
				 ELSE ''-''
			END AS vcProjectStageName,'	
	        
	
	

--------------Add custom fields to query----------------
SELECT TOP 1 @fld_Name= vcFieldName,@fld_ID = numFieldId FROM View_DynamicCustomColumns
WHERE 
grp_id = 5 AND numDomainID=@numDomainId
AND numFormId=26 AND bitCustom=1 AND numUserCntID = @numUserCntID ORDER BY numFieldId
WHILE @fld_ID > 0
BEGIN ------------------

	 PRINT @fld_Name
	
	       SET @strSQL = @strSQL + 'dbo.GetCustFldValueItem('
                                    + CONVERT(VARCHAR(15), @fld_ID)
                                    + ',Opp.numItemCode) as ''' + @fld_Name + ''','
       

SELECT TOP 1 @fld_Name= vcFieldName,@fld_ID = numFieldId FROM View_DynamicCustomColumns
WHERE 
grp_id = 5 AND numDomainID=@numDomainId
AND numFormId=26 AND bitCustom=1  AND numUserCntID = @numUserCntID AND numFieldId > @fld_ID ORDER BY numFieldId
 IF @@ROWCOUNT=0
 SET @fld_ID = 0
END ------------------


                                
--Purchase Opportunity ?
IF @tintOppType = 2 
    BEGIN
        SET @strSQL =  @strSQL + 'SUBSTRING(( SELECT  '','' + vcSerialNo
            + CASE WHEN ISNULL(I.bitLotNo, 0) = 1
                   THEN '' ('' + CONVERT(VARCHAR(15), oppI.numQty)
                        + '')''
                   ELSE ''''
              END
			FROM    OppWarehouseSerializedItem oppI
					JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID = whi.numWareHouseItmsDTLID
			WHERE   oppI.numOppID = Opp.numOppId
					AND oppI.numOppItemID = Opp.numoppitemtCode
			ORDER BY vcSerialNo
		  FOR
			XML PATH('''')
		  ), 2, 200000) AS SerialLotNo,
		  CAST(Opp.numUnitHour AS NUMERIC(9, 0)) numQty,
		  (SELECT vcUnitName FROM dbo.UOM WHERE numUOMId = opp.numUOMId AND ISNULL(bitEnabled,0) = 1)  AS  [vcBaseUOMName],
		  I.bitSerialized,
		  ISNULL(I.bitLotNo, 0) AS bitLotNo,
		  isnull(Opp.numWarehouseItmsID,0) as numWarehouseItmsID,'
	END
ELSE
	BEGIN	
        SET @strSQL =  @strSQL + ' '''' AS SerialLotNo,
		   CAST(Opp.numUnitHour AS NUMERIC(9, 0)) numQty,
		  I.bitSerialized,
		  ISNULL(I.bitLotNo, 0) AS bitLotNo,
		  isnull(Opp.numWarehouseItmsID,0) as numWarehouseItmsID,'
	END


        
 SET @strSQL = left(@strSQL,len(@strSQL)-1) + '   
        FROM    OpportunityItems Opp
        LEFT JOIN OpportunityMaster OM ON Opp.numOppID = oM.numOppID
        JOIN item I ON Opp.numItemCode = i.numItemcode
        LEFT JOIN WareHouseItems WItems ON WItems.numWareHouseItemID = Opp.numWarehouseItmsID
        LEFT JOIN Warehouses W ON W.numWarehouseID = WItems.numWarehouseID
        LEFT JOIN WarehouseLocation WL ON WL.numWLocationID = WItems.numWLocationID
        LEFT JOIN OpportunityMaster M ON Opp.numSourceID = M.numOppID
        LEFT JOIN UOM u ON u.numUOMId = opp.numUOMId
WHERE   om.numDomainId = '+ CONVERT(VARCHAR(10),@numDomainId) + '
        AND Opp.numOppId = '+ CONVERT(VARCHAR(15),@OpportunityId) +'
ORDER BY numoppitemtCode ASC '
  
PRINT @strSQL;
EXEC (@strSQL);
SELECT  vcSerialNo,
       vcComments AS Comments,
        numOppItemID AS numoppitemtCode,
        W.numWarehouseItmsDTLID,
        numWarehouseItmsID AS numWItmsID,
        dbo.Fn_getattributes(W.numWarehouseItmsDTLID,
                             1) Attributes
FROM    WareHouseItmsDTL W
        JOIN OppWarehouseSerializedItem O ON O.numWarehouseItmsDTLID = W.numWareHouseItmsDTLID
WHERE   numOppID = @OpportunityId


CREATE TABLE #tempForm (tintOrder TINYINT,vcDbColumnName nvarchar(50),vcOrigDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),
  vcListItemType varchar(3),numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,
  numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,bitIsAlphaNumeric BIT,bitIsNumeric BIT,
  bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),ListRelID numeric(9),intColumnWidth int,intVisible int,vcFieldDataType CHAR(1))


INSERT INTO #tempForm
select isnull(DC.tintRow,0)+1 as tintOrder,DDF.vcDbColumnName,DDF.vcOrigDbColumnName,ISNULL(DDF.vcCultureFieldName,DDF.vcFieldName),                  
 DDF.vcAssociatedControlType,DDF.vcListItemType,DDF.numListID,DDF.vcLookBackTableName                                                 
,DDF.bitCustom,DDF.numFieldId,DDF.bitAllowSorting,DDF.bitInlineEdit,
DDF.bitIsRequired,DDF.bitIsEmail,DDF.bitIsAlphaNumeric,DDF.bitIsNumeric,DDF.bitIsLengthValidation,
DDF.intMaxLength,DDF.intMinLength,DDF.bitFieldMessage,DDF.vcFieldMessage vcFieldMessage,DDF.ListRelID,DDF.intColumnWidth,Case when DC.numFieldID is null then 0 else 1 end,DDF.vcFieldDataType
 FROM View_DynamicDefaultColumns DDF left join View_DynamicColumns DC on DC.numFormId=26 and DDF.numFieldId=DC.numFieldId and DC.numUserCntID=@numUserCntID and DC.numDomainID=@numDomainID and numRelCntType=0 AND tintPageType=1 
 where DDF.numFormId=26 and DDF.numDomainID=@numDomainID AND ISNULL(DDF.bitSettingField,0)=1 AND ISNULL(DDF.bitDeleted,0)=0 AND ISNULL(DDF.bitCustom,0)=0
       
 UNION
    
    select tintRow+1 as tintOrder,vcDbColumnName,vcFieldName,vcFieldName,                  
 vcAssociatedControlType,'' as vcListItemType,numListID,''                                                 
,bitCustom,numFieldId,bitAllowSorting,0 as bitAllowEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,''
 from View_DynamicCustomColumns
 where numFormId=26 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=0
 AND ISNULL(bitCustom,0)=1


select * from #tempForm order by tintOrder

drop table #tempForm

/*
                        SET @strSQL = 'select  Opp.numoppitemtCode,Opp.numOppId,
                               Opp.numItemCode,                                    
                               Opp.vcItemName,                                                                    
                               ISNULL(Opp.vcItemDesc,'''') as [Desc],                                                                    
                               Opp.vcType ItemType,                                   
                               Opp.vcPathForTImage,
                               Opp.vcModelID,
							    CAST((dbo.fn_UOMConversion(ISNULL(I.numBaseUnit,0),Opp.numItemCode,om.numDomainId,ISNULL(opp.numUOMId,0)) * Opp.numUnitHour) as numeric(18,2)) as numUnitHour,   
                               Opp.monPrice,                                                                    
                               Opp.monTotAmount,                                                     
							   ISNULL(Opp.vcItemDesc,'''') vcItemDesc,
							   Opp.vcManufacturer,
							   I.charItemType,
							   I.bitKitParent,
							   I.bitAssembly,
							   I.vcSKU,                                   
							   I.numBarCodeId,
							   dbo.[fn_GetListItemName](I.[numItemClassification]) vcClassification,
							   (SELECT [vcItemGroup] FROM [ItemGroups] WHERE [numItemGroupID] =I.numItemGroup) vcItemGroup,
							   ISNULL(I.vcUnitofMeasure,'''') vcUnitofMeasure,
							   I.fltWeight,I.fltHeight,I.fltWidth,I.fltLength,
							   WItems.numWarehouseID,
                               Opp.numWarehouseItmsID,isnull(Opp.numToWarehouseItemID,0) numToWarehouseItemID,bitDropShip DropShip, CASE ISNULL(bitDropShip,0) when 1 then ''Yes'' else '''' end as vcDropShip,
                               0 as Op_Flag,isnull(Opp.bitDiscountType,0) as bitDiscountType,isnull(Opp.fltDiscount,0) as fltDiscount,isnull(monTotAmtBefDiscount,0) as monTotAmtBefDiscount,                                    
                               vcWarehouse as Warehouse,I.bitSerialized,isnull(I.bitLotNo,0) as bitLotNo,vcAttributes, 
                               dbo.fn_GetReturnItemQty(Opp.numoppitemtCode) as ReturrnedQty,
                               [dbo].fn_GetReturnStatus(Opp.numoppitemtCode) as ReturnStatus,
							   isnull((SELECT numReturnID FROM Returns WHERE numOppItemCode=Opp.numoppitemtCode and numOppId=Opp.numOppID),0) as numReturnID,
                               isnull(v.monCost,0) as VendorCost,
                               bitTaxable,
                               case when bitTaxable =0 then 0 when bitTaxable=1 then '
                            + CONVERT(VARCHAR(15), @TaxPercentage)
                            + ' end as Tax,                                                
                                   isnull(M.vcPOppName,''Internal'') as Source,dbo.fn_GetAttributes(Opp.numWarehouseItmsID,I.bitSerialized) as Attributes  ,numSourceID '
                            + CASE WHEN @strSQL = '' THEN ''
                                   ELSE ',' + @strSQL
                              END
                            + ',isnull(opp.numUOMId,0) AS numUOM,ISNULL(u.vcUnitName,'''') vcUOMName,dbo.fn_UOMConversion(opp.numUOMId,Opp.numItemCode,om.numDomainId,null) AS UOMConversionFactor
                          ,case when Opp.bitDiscountType=0 then  cast(isnull(Opp.monTotAmtBefDiscount,Opp.monTotAmount)-Opp.monTotAmount AS VARCHAR(10)) +  '' ('' + CAST(Opp.fltDiscount AS VARCHAR(10)) +  ''%)'' 
                          ELSE cast(isnull(Opp.monTotAmtBefDiscount,Opp.monTotAmount)-Opp.monTotAmount AS VARCHAR(10)) end as DiscAmt
                           ,isnull(I.numVendorID,0) numVendorID,OM.tintOppType,cast(Opp.numUnitHour as numeric(9,0)) as numQty,SUBSTRING(
(SELECT '','' + vcSerialNo + CASE WHEN isnull(I.bitLotNo,0)=1 THEN '' ('' + CONVERT(VARCHAR(15),whi.numQty) + '')'' ELSE '''' end 
FROM OppWarehouseSerializedItem oppI JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID=whi.numWareHouseItmsDTLID where oppI.numOppID=Opp.numOppId and oppI.numOppItemID=Opp.numoppitemtCode 
ORDER BY vcSerialNo FOR XML PATH('''')),2,200000) AS SerialLotNo, ISNULL(Opp.bitWorkOrder,0) as bitWorkOrder,
CASE WHEN ISNULL(Opp.bitWorkOrder,0)=1 then (select dbo.GetListIemName(numWOStatus) + CASE WHEN isnull(numWOStatus,0)=23184 THEN
'' - '' + CAST(ISNULL(dbo.fn_GetContactName(numAssignedTo), '''')+'',''+convert(varchar(20),dateadd(minute,' + CONVERT(VARCHAR(15),-@ClientTimeZoneOffset) + ',bintCompletedDate)) AS VARCHAR(100)) ELSE '''' end
 from WorkOrder where isnull(numOppId,0)=Opp.numOppId and numItemCode=Opp.numItemCode) else '''' end as vcWorkOrderStatus,
 ISNULL(Opp.numVendorWareHouse,0) as numVendorWareHouse,ISNULL(Opp.numShipmentMethod,0) as numShipmentMethod,ISNULL(Opp.numSOVendorId,0) as numSOVendorId,Opp.numUnitHour as numOriginalUnitHour,
 isnull(Opp.numProjectID,0) numProjectID,isnull(Opp.numClassID,0) numClassID,isnull(Opp.numProjectStageID,0) numProjectStageID,Case When OPP.numProjectStageID>0 then dbo.fn_GetProjectStageName(OPP.numProjectId,OPP.numProjectStageID) when OPP.numProjectStageID=-1 then ''T&E'' When OPP.numProjectId>0 then case when OM.tintOppType=1 then ''S.O.'' else ''P.O.'' end else ''-'' end as vcProjectStageName,
 CASE WHEN (SELECT TOP 1 ISNULL(OBI.numOppBizDocItemID,0) FROM dbo.OpportunityBizDocs OBD INNER JOIN dbo.OpportunityBizDocItems OBI ON OBD.numOppBizDocsID = OBI.numOppBizDocID WHERE OBI.numOppItemID = Opp.numoppitemtCode AND ISNULL(OBD.bitAuthoritativeBizDocs,0)=1 ) > 0
      THEN 1 ELSE 0 END AS bitItemAddedToAuthBizDoc,isnull(I.numItemClass,0) as numItemClass
                              from OpportunityItems Opp                    
                               left join OpportunityMaster OM                                                            
                                  on Opp.numOppID=oM.numOppID                                                                     
                                 join item I                                                                    
                                  on Opp.numItemCode=i.numItemcode                                            
                                left join Vendor v 
									on v.numVendorID= I.numVendorID and I.numItemCode =v.numItemCode
                               left join  WareHouseItems  WItems                                        
                               on   WItems.numWareHouseItemID= Opp.numWarehouseItmsID                                            
                               left join  Warehouses  W                                            
                                  on   W.numWarehouseID= WItems.numWarehouseID                                         
                                  left join OpportunityMaster M                                                            
                                  on Opp.numSourceID=M.numOppID  
                                  LEFT JOIN  UOM u ON u.numUOMId = opp.numUOMId  
                                  where om.numDomainId ='
                            + CONVERT(VARCHAR(15), @numDomainId)
                            + ' and Opp.numOppId='+ CONVERT(VARCHAR(15), @OpportunityId) +  
                            + CASE WHEN @numItemCode > 0 THEN ' AND Opp.numOppItemTCode = ' + CONVERT(VARCHAR(15), @numItemCode)
                                   ELSE ''
                              END
                            +
                            ' order by numoppitemtCode ASC '
                        PRINT @strSQL
                        EXEC ( @strSQL
                            )
                        SELECT  vcSerialNo,
                                vcComments AS Comments,
                                numOppItemID AS numoppitemtCode,
                                W.numWarehouseItmsDTLID,
                                numWarehouseItmsID AS numWItmsID,
                                dbo.Fn_getattributes(W.numWarehouseItmsDTLID,
                                                     1) Attributes
                        FROM    WareHouseItmsDTL W
                                JOIN OppWarehouseSerializedItem O ON O.numWarehouseItmsDTLID = W.numWareHouseItmsDTLID
                        WHERE   numOppID = @OpportunityId
*/
      /*              END
                ELSE 
                    BEGIN
                        SELECT  @numDomainID = numDomainId
                        FROM    OpportunityMaster
                        WHERE   numOppID = @OpportunityId
                        SET @strSQL = ''
                        SELECT TOP 1
                                @fld_ID = Fld_ID,
                                @fld_Name = Fld_label 
                        FROM    CFW_Fld_Master
                        WHERE   grp_id = 5
                                AND numDomainID = @numDomainID
                        WHILE @fld_ID > 0
                            BEGIN
                                SET @strSQL =@strSQL + 'dbo.GetCustFldValueItem('
                                    + CONVERT(VARCHAR(15), @fld_ID)
                                    + ',Opp.numItemCode) as ''' + @fld_Name + ''''
                                SELECT TOP 1
                                        @fld_ID = Fld_ID,
                                        @fld_Name = Fld_label
                                FROM    CFW_Fld_Master
                                WHERE   grp_id = 5
                                        AND numDomainID = @numDomainID
                                        AND Fld_ID > @fld_ID
                                IF @@ROWCOUNT = 0 
                                    SET @fld_ID = 0
                                IF @fld_ID <> 0 
                                    SET @strSQL = @strSQL + ','
                            END
                        SET @strSQL = 'select  Opp.numoppitemtCode,Opp.numOppId,                                       
                                Opp.numItemCode,                                    
                               Opp.vcItemName,                                                                    
                               ISNULL(Opp.vcItemDesc,'''') as [Desc],                                                                    
                               Opp.vcType ItemType,                                   
                               Opp.vcPathForTImage,
                               Opp.vcModelID,
                              CAST((dbo.fn_UOMConversion(ISNULL(I.numBaseUnit,0),Opp.numItemCode,om.numDomainId,ISNULL(opp.numUOMId,0)) * Opp.numUnitHour) as numeric(18,2)) as numUnitHour,   
							   Opp.monPrice,                                                                    
                               Opp.monTotAmount,                                                     
							   ISNULL(Opp.vcItemDesc,'''') vcItemDesc,
							   Opp.vcManufacturer,
							   I.charItemType,
							   I.bitKitParent,
							   I.bitAssembly,
							   I.vcSKU,
							   I.numBarCodeId,
							   dbo.[fn_GetListItemName](I.[numItemClassification]) vcClassification,
							   (SELECT [vcItemGroup] FROM [ItemGroups] WHERE [numItemGroupID] =I.numItemGroup) vcItemGroup,
							   ISNULL(I.vcUnitofMeasure,'''') vcUnitofMeasure,
							   I.fltWeight,I.fltHeight,I.fltWidth,I.fltLength,
							   WItems.numWarehouseID,
                               Opp.numWarehouseItmsID,isnull(Opp.numToWarehouseItemID,0) numToWarehouseItemID,bitDropShip DropShip, CASE ISNULL(bitDropShip,0) when 1 then ''Yes'' else '''' end as vcDropShip,
                               0 as Op_Flag,isnull(Opp.bitDiscountType,0) bitDiscountType,isnull(Opp.fltDiscount,0) fltDiscount,isnull(monTotAmtBefDiscount,0) monTotAmtBefDiscount,                                           
                               vcWarehouse as Warehouse,I.bitSerialized,isnull(I.bitLotNo,0) as bitLotNo,vcAttributes,
                               dbo.fn_GetReturnItemQty(Opp.numoppitemtCode) as ReturrnedQty,
                               [dbo].fn_GetReturnStatus(Opp.numoppitemtCode) as ReturnStatus,
							   isnull((SELECT numReturnID FROM Returns WHERE numOppItemCode=Opp.numoppitemtCode and numOppId=Opp.numOppID),0) as numReturnID,
                               isnull(v.monCost,0) as VendorCost,
                               bitTaxable,
                               case when bitTaxable =0 then 0 when bitTaxable=1 then '
                            + CONVERT(VARCHAR(15), @TaxPercentage)
                            + ' end as Tax,                                                
                                   isnull(m.vcPOppName,''Internal'') as Source,dbo.fn_GetAttributes(Opp.numWarehouseItmsID,I.bitSerialized) as Attributes ,numSourceID '
                            + CASE WHEN @strSQL = '' THEN ''
                                   ELSE ',' + @strSQL
                              END
                            + ',isnull(opp.numUOMId,0) AS numUOM,ISNULL(u.vcUnitName,'''') vcUOMName,dbo.fn_UOMConversion(opp.numUOMId,Opp.numItemCode,om.numDomainId,null) AS UOMConversionFactor
                          ,case when Opp.bitDiscountType=0 then  cast(isnull(Opp.monTotAmtBefDiscount,Opp.monTotAmount)-Opp.monTotAmount AS VARCHAR(10)) +  '' ('' + CAST(Opp.fltDiscount AS VARCHAR(10)) +  ''%)'' 
                          ELSE cast(isnull(Opp.monTotAmtBefDiscount,Opp.monTotAmount)-Opp.monTotAmount AS VARCHAR(10)) end as DiscAmt
                         ,isnull(I.numVendorID,0) numVendorID,OM.tintOppType,cast(Opp.numUnitHour as numeric(9,0)) numQty,SUBSTRING(
(SELECT '','' + vcSerialNo + CASE WHEN isnull(I.bitLotNo,0)=1 THEN '' ('' + CONVERT(VARCHAR(15),whi.numQty) + '')'' ELSE '''' end 
FROM OppWarehouseSerializedItem oppI JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID=whi.numWareHouseItmsDTLID where oppI.numOppID=Opp.numOppId and oppI.numOppItemID=Opp.numoppitemtCode 
ORDER BY vcSerialNo FOR XML PATH('''')),2,200000) AS SerialLotNo, ISNULL(Opp.bitWorkOrder,0) as bitWorkOrder,
CASE WHEN ISNULL(Opp.bitWorkOrder,0)=1 then (select dbo.GetListIemName(numWOStatus) + CASE WHEN isnull(numWOStatus,0)=23184 THEN
'' - '' + CAST(ISNULL(dbo.fn_GetContactName(numAssignedTo), '''')+'',''+convert(varchar(20),dateadd(minute,'+ CONVERT(VARCHAR(15),-@ClientTimeZoneOffset) +',bintCompletedDate)) AS VARCHAR(100)) ELSE '''' end
 from WorkOrder where isnull(numOppId,0)=Opp.numOppId and numItemCode=Opp.numItemCode) else '''' end as vcWorkOrderStatus,
 ISNULL(Opp.numVendorWareHouse,0) as numVendorWareHouse,ISNULL(Opp.numShipmentMethod,0) as numShipmentMethod,ISNULL(Opp.numSOVendorId,0) as numSOVendorId,Opp.numUnitHour as numOriginalUnitHour,
 isnull(Opp.numProjectID,0) numProjectID,isnull(Opp.numClassID,0) numClassID,isnull(Opp.numProjectStageID,0) numProjectStageID,Case When OPP.numProjectStageID>0 then dbo.fn_GetProjectStageName(OPP.numProjectId,OPP.numProjectStageID) when OPP.numProjectStageID=-1 then ''T&E'' When OPP.numProjectId>0 then case when OM.tintOppType=1 then ''S.O.'' else ''P.O.'' end else ''-'' end as vcProjectStageName,
 CASE WHEN (SELECT TOP 1 ISNULL(OBI.numOppBizDocItemID,0) FROM dbo.OpportunityBizDocs OBD INNER JOIN dbo.OpportunityBizDocItems OBI ON OBD.numOppBizDocsID = OBI.numOppBizDocID WHERE OBI.numOppItemID = Opp.numoppitemtCode AND ISNULL(OBD.bitAuthoritativeBizDocs,0)=1 ) > 0
 THEN 1 ELSE 0 END AS bitItemAddedToAuthBizDoc,isnull(I.numItemClass,0) as numItemClass
              from OpportunityItems Opp                    
                               left join OpportunityMaster OM                                                            
                                  on Opp.numOppID=oM.numOppID                                    
                                 join item I                                                                    
                                  on Opp.numItemCode=i.numItemcode   
                                  left join Vendor v 
									on v.numVendorID= I.numVendorID and I.numItemCode =v.numItemCode
                                left join  WareHouseItems  WItems                                        
                                on   WItems.numWareHouseItemID= Opp.numWarehouseItmsID                                            
                                left join  Warehouses  W                                            
                                  on   W.numWarehouseID= WItems.numWarehouseID                                         
                                  left join OpportunityMaster M                                                            
                                  on Opp.numSourceID=M.numOppID                      
                                        LEFT JOIN  UOM u ON u.numUOMId = opp.numUOMId
                                  where om.numDomainId ='
                            + CONVERT(VARCHAR(15), @numDomainId)
                            + ' and Opp.numOppId='+ CONVERT(VARCHAR(15), @OpportunityId) +  
                            + CASE WHEN @numItemCode > 0 THEN ' AND Opp.numOppItemTCode = ' + CONVERT(VARCHAR(15), @numItemCode)
                                   ELSE ''
                              END
                            +
                            ' order by numoppitemtCode ASC '
                        PRINT @strSQL
                        EXEC ( @strSQL
                            )
                        SELECT  vcSerialNo,
                                vcComments AS Comments,
                                numOppItemID AS numoppitemtCode,
                                W.numWarehouseItmsDTLID,
                                numWarehouseItmsID AS numWItmsID,
                                dbo.Fn_getattributes(W.numWarehouseItmsDTLID,
                                                     1) Attributes
                        FROM    OppWarehouseSerializedItem O
                                LEFT JOIN WareHouseItmsDTL W ON O.numWarehouseItmsDTLID = W.numWareHouseItmsDTLID
                        WHERE   numOppID = @OpportunityId
                    END
            */
            END
            
    END
/****** Object:  StoredProcedure [dbo].[usp_SetUsersWithDomains]    Script Date: 07/26/2008 16:21:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_setuserswithdomains')
DROP PROCEDURE usp_setuserswithdomains
GO
CREATE PROCEDURE [dbo].[usp_SetUsersWithDomains]
 @numUserID NUMERIC(9),                                    
 @vcUserName VARCHAR(50),                                    
 @vcUserDesc VARCHAR(250),                              
 @numGroupId as numeric(9),                                           
 @numUserDetailID numeric ,                              
@numUserCntID as numeric(9),                              
@strTerritory as varchar(4000) ,                              
@numDomainID as numeric(9),
@strTeam as varchar(4000),
@vcEmail as varchar(100),
@vcPassword as varchar(100),          
@Active as BIT,
@numDefaultClass NUMERIC,
@numDefaultWarehouse as numeric(18)
AS
BEGIN

            
if @numUserID=0             
begin 

DECLARE @APIPublicKey varchar(20)
SELECT @APIPublicKey = dbo.GenerateBizAPIPublicKey() 
           
 insert into UserMaster(vcUserName,vcUserDesc,numGroupId,numUserDetailId,numModifiedBy,bintModifiedDate,
	vcEmailID,vcPassword,numDomainID,numCreatedBy,bintCreatedDate,bitActivateFlag,numDefaultClass,numDefaultWarehouse,vcBizAPIPublicKey,vcBizAPISecretKey,bitBizAPIAccessEnabled)
 values(@vcUserName,@vcUserDesc,@numGroupId,@numUserDetailID,@numUserCntID, getutcdate(),
	@vcEmail,@vcPassword,@numDomainID,@numUserCntID,getutcdate(),@Active,@numDefaultClass,@numDefaultWarehouse,@APIPublicKey,NEWID(),0)            
 set @numUserID=@@identity          
           
INSERT INTO BizAPIThrottlePolicy 
(
	[RateLimitKey],
	[bitIsIPAddress],
	[numPerSecond],
	[numPerMinute],
	[numPerHour],
	[numPerDay],
	[numPerWeek]
)
VALUES
(
	@APIPublicKey,
	0,
	3,
	60,
	1200,
	28800,
	200000
)
    
EXECUTE [dbo].[Resource_Add]   @vcUserName  ,@vcUserDesc  ,@vcEmail  ,1  ,@numDomainID  ,@numUserDetailID      
      
      
end            
else            
begin            
   UPDATE UserMaster SET vcUserName = @vcUserName,                                    
    vcUserDesc = @vcUserDesc,                              
    numGroupId =@numGroupId,                                    
    numUserDetailId = @numUserDetailID ,                              
    numModifiedBy= @numUserCntID ,                              
    bintModifiedDate= getutcdate(),                          
    vcEmailID=@vcEmail,            
    vcPassword=@vcPassword,          
    bitActivateFlag=@Active,
    numDefaultClass=@numDefaultClass,
    numDefaultWarehouse=@numDefaultWarehouse
    WHERE numUserID = @numUserID          
          
  
 if not exists (select * from resource where numUserCntId = @numUserDetailID and numdomainId = @numDomainId)  
 begin   
  EXECUTE [dbo].[Resource_Add]   @vcUserName  ,@vcUserDesc  ,@vcEmail  ,1  ,@numDomainID  ,@numUserDetailID     
 end  
  
-- if not exists(select * from ExchangeUserDetails where numUserID=@numUserID)          
-- begin          
--  insert into  ExchangeUserDetails(numUserID,bitExchangeIntegration,bitAccessExchange,vcExchPassword ,vcExchPath,vcExchDomain)            
--  values(@numUserID,@bitExchangeIntegration,@bitAccessExchange,@vcExchPassword,@vcExchPath,@vcExchDomain)          
-- end          
-- else          
-- begin          
--  update ExchangeUserDetails set           
--     bitExchangeIntegration=@bitExchangeIntegration,          
--     bitAccessExchange=@bitAccessExchange,          
--     vcExchPassword =@vcExchPassword,          
--     vcExchPath=@vcExchPath,          
--     vcExchDomain=@vcExchDomain          
--  where numUserID=@numUserID          
-- end      
end            
    
--if not exists (select * from [ImapUserDetails] where numDomainId = @numDomainId and numUserCntId= @numUserCntID  )    
-- begin    
--  INSERT INTO [ImapUserDetails]    
--           ( bitImap    
--     ,[vcImapServerUrl]    
--           ,[vcImapPassword]    
--           ,[bitSSl]    
--           ,[numPort]    
--           ,[numDomainId]    
--           ,[numUserCntId])    
--     VALUES    
--           (@bitImap    
--     ,@vcImapServerUrl    
--           ,@vcImapPassword    
--           ,@bitImapSsl    
--           ,@numImapSSLPort    
--           ,@numDomainID    
--           ,@numUserDetailID)    
-- end    
--else    
-- begin    
--  UPDATE [ImapUserDetails]    
--     SET [bitImap] = @bitImap    
--     ,[vcImapServerUrl] = @vcImapServerUrl    
--     ,[vcImapPassword] = @vcImapPassword    
--     ,[bitSSl] = @bitImapSsl    
--     ,[numPort] = @numImapSSLPort    
--     ,[numDomainId] =@numDomainID    
--     ,[numUserCntId] = @numUserDetailID    
--   WHERE [numUserCntId] = @numUserDetailID and numDomainId = @numDomainID    
-- End      
                       
                               
                              
                              
declare @separator_position as integer                              
declare @strPosition as varchar(1000)                 
                            
                              
delete from UserTerritory where numUserCntID = @numUserDetailID                              
and numDomainId=@numDomainID                              
          
                              
   while patindex('%,%' , @strTerritory) <> 0                              
    begin -- Begin for While Loop                              
      select @separator_position =  patindex('%,%' , @strTerritory)                              
    select @strPosition = left(@strTerritory, @separator_position - 1)                              
       select @strTerritory = stuff(@strTerritory, 1, @separator_position,'')                              
     insert into UserTerritory (numUserCntID,numTerritoryID,numDomainID)                              
     values (@numUserDetailID,@strPosition,@numDomainID)                              
                               
                                
   end                              
                                           
                              
                              
delete from UserTeams where numUserCntID = @numUserDetailID                              
and numDomainId=@numDomainID                              
                               
                              
   while patindex('%,%' , @strTeam) <> 0                              
    begin -- Begin for While Loop                              
      select @separator_position =  patindex('%,%' , @strTeam)                              
    select @strPosition = left(@strTeam, @separator_position - 1)                  
       select @strTeam = stuff(@strTeam, 1, @separator_position,'')                              
     insert into UserTeams (numUserCntID,numTeam,numDomainID)                              
     values (@numUserDetailID,@strPosition,@numDomainID)                              
                               
                                
   end                 
                
                
--delete from EmailUsers where numUserCntID = @numUserDetailID                                            
--                                       
--   while patindex('%,%' , @strEmail) <> 0                         
--    begin -- Begin for While Loop                              
--      select @separator_position =  patindex('%,%' , @strEmail)                              
--    select @strPosition = left(@strEmail, @separator_position - 1)                              
--       select @strEmail = stuff(@strEmail, 1, @separator_position,'')                              
--     insert into EmailUsers (numContactID,numUserCntID)                              
--     values (@strPosition,@numUserDetailID)                              
--   end                             
                                 
                              
delete from ForReportsByTeam where  numUserCntID = @numUserDetailID                              
and numDomainId=@numDomainID and numTeam not in (select numTeam from UserTeams where numUserCntID = @numUserCntID                              
and numDomainId=@numDomainID )                              
                              
                                  
END
GO
/****** Object:  StoredProcedure [dbo].[USP_ShoppingCart]    Script Date: 05/07/2009 22:09:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_shoppingcart')
DROP PROCEDURE usp_shoppingcart
GO
CREATE PROCEDURE [dbo].[USP_ShoppingCart]                            
@numDivID as numeric(18),                            
@numOppID as numeric(18)=null output ,                                              
@vcPOppName as varchar(200)='' output,  
@numContactId as numeric(18),                            
@numDomainId as numeric(18),
@vcSource VARCHAR(50)=NULL,
@numCampainID NUMERIC(9)=0,
@numCurrencyID as numeric(9),
@numBillAddressId numeric(9),
@numShipAddressId numeric(9),
@bitDiscountType BIT,
@fltDiscount FLOAT,
@numProId NUMERIC(9),
@numSiteID NUMERIC(9),
@tintOppStatus INT,
@monShipCost MONEY,
@tintSource AS TINYINT ,
@tintSourceType AS TINYINT ,                          
@numPaymentMethodId NUMERIC(9),
@txtComments varchar(1000)

as                            
declare @CRMType as integer               
declare @numRecOwner as integer                            
if @numOppID=  0                             
begin                            
select @CRMType=tintCRMType,@numRecOwner=numRecOwner from DivisionMaster where numDivisionID=@numDivID                            
if @CRMType= 0                             
begin                            
UPDATE DivisionMaster                               
   SET tintCRMType=1                              
   WHERE numDivisionID=@numDivID                              
                                                   
end                            
declare @TotalAmount as FLOAT
--declare @intOppTcode as numeric(9) 
--declare @numCurrencyID as numeric(9)
--declare @fltExchangeRate as float                          
--Select @intOppTcode=max(numOppId)from OpportunityMaster                              
--set @intOppTcode =isnull(@intOppTcode,0) + 1                              

DECLARE @numTemplateID NUMERIC
SELECT  @numTemplateID= numMirrorBizDocTemplateId FROM eCommercePaymentConfig WHERE  numSiteId=@numSiteID AND numPaymentMethodId = @numPaymentMethodId AND numDomainID = @numDomainID   

   Declare @fltExchangeRate float                                 
    
   IF @numCurrencyID=0 select @numCurrencyID=isnull(numCurrencyID,0) from Domain where numDomainID=@numDomainId                            
   if @numCurrencyID=0 set   @fltExchangeRate=1
  else set @fltExchangeRate=dbo.GetExchangeRate(@numDomainID,@numCurrencyID)                          
                            
  
  --Set Default Class If enable User Level Class Accountng 
  DECLARE @numAccountClass AS NUMERIC(18);SET @numAccountClass=0
  SELECT @numAccountClass=ISNULL(numDefaultClass,0) FROM dbo.UserMaster UM JOIN dbo.Domain D ON UM.numDomainID=D.numDomainId
  WHERE D.numDomainId=@numDomainId AND UM.numUserDetailId=@numContactId AND ISNULL(D.IsEnableUserLevelClassTracking,0)=1
                            
--set @vcPOppName=@vcPOppName+ '-'+convert(varchar(4),year(getutcdate()))  +'-A'+ convert(varchar(10),@intOppTcode)                              
  --SELECT tintSource , tintSourceType FROM dbo.OpportunityMaster
insert into OpportunityMaster                              
  (                              
  numContactId,                              
  numDivisionId,                              
  txtComments,                              
  numCampainID,                              
  bitPublicFlag,                              
  tintSource, 
  tintSourceType ,                               
  vcPOppName,                              
  monPAmount,                              
  numCreatedBy,                              
  bintCreatedDate,                               
  numModifiedBy,                              
  bintModifiedDate,                              
  numDomainId,                               
  tintOppType, 
  tintOppStatus,                   
  intPEstimatedCloseDate,              
  numRecOwner,
  bitOrder,
  numCurrencyID,
  fltExchangeRate,fltDiscountTotal,bitDiscountTypeTotal,
  monShipCost,
  numOppBizDocTempID,numAccountClass
  )                              
 Values                              
  (                              
  @numContactId,                              
  @numDivID,                              
  @txtComments,                              
  @numCampainID,--  0,
  0,                              
  @tintSource,   
  @tintSourceType,                           
  ISNULL(@vcPOppName,'SO'),                             
  0,                                
  @numContactId,                              
  getutcdate(),                              
  @numContactId,                              
  getutcdate(),        
  @numDomainId,                              
  1,             
  @tintOppStatus,       
  getutcdate(),                                 
  @numRecOwner ,
  1,
  @numCurrencyID,
  @fltExchangeRate,@fltDiscount,@bitDiscountType
  ,@monShipCost,
  @numTemplateID,@numAccountClass
  
  )                               
set @numOppID=scope_identity()                             
--Update OppName as per Name Template
EXEC dbo.USP_UpdateNameTemplateValue 1,@numDomainId,@numOppID
SELECT @vcPOppName = vcPOppName FROM OpportunityMaster WHERE numOppID= @numOppID
end
delete  from    OpportunityItems where   numOppId= @numOppID                               

--if convert(varchar(10),@strItems) <>''                
--begin               
-- DECLARE @hDocItem int                
-- EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems                
                
insert into OpportunityItems                                                        
  (numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,vcAttributes,[vcItemName],[vcModelID],[vcManufacturer],vcPathForTImage, monVendorCost,numUOMId,bitDiscountType,fltDiscount,monTotAmtBefDiscount)                                                        
  --kishan                                                       
 select @numOppID,X.numItemCode,x.numUnitHour * x.decUOMConversionFactor,x.monPrice/x.decUOMConversionFactor,x.monTotAmount,X.vcItemDesc, NULLIF(X.numWarehouseItmsID,0),X.vcItemType,X.vcAttributes,(SELECT vcItemName FROM item WHERE numItemCode = X.numItemCode),(SELECT vcModelID FROM item WHERE numItemCode = X.numItemCode),(SELECT vcManufacturer FROM item WHERE numItemCode = X.numItemCode),
		(SELECT TOP 1  vcPathForTImage FROM dbo.ItemImages WHERE numItemCode = X.numItemCode AND bitDefault = 1 AND numDomainId = @numDomainId),   (select isnull(VN.monCost,0) from Vendor VN, Item IT where IT.numItemCode=VN.numItemCode and  VN.numItemCode=X.numItemCode and  VN.numVendorID=IT.numVendorID), 
		numUOM,bitDiscountType,fltDiscount,monTotAmtBefDiscount 
FROM dbo.CartItems X WHERE numUserCntId =@numContactId

declare @tintShipped as tinyint      
DECLARE @tintOppType AS TINYINT
      
select @tintOppStatus=tintOppStatus,@tintOppType=tintOppType,@tintShipped=tintShipped from OpportunityMaster where numOppID=@numOppID              
if @tintOppStatus=1               
	begin         
	PRINT 'inside USP_UpdatingInventoryonCloseDeal'
		exec USP_UpdatingInventoryonCloseDeal @numOppID,@numContactId
	end              
            
if @tintOppType=1              
begin              
	if @tintOppStatus=1 Update WareHouseItmsDTL set tintStatus=1 where numWareHouseItmsDTLID in (select numWarehouseItmsDTLID from OppWarehouseSerializedItem where numOppID=@numOppID)
	if @tintShipped=1 Update WareHouseItmsDTL set tintStatus=2 where numWareHouseItmsDTLID in (select numWarehouseItmsDTLID from OppWarehouseSerializedItem where numOppID=@numOppID)              
END

/*Commented by Chintan..reason:tobe executed only when order is confirmed*/
--exec USP_UpdatingInventoryonCloseDeal @numOppID,@numContactId

-- SELECT * FROM OPENXML (@hDocItem,'/NewDataSet/Table',2)                                                        
-- WITH  (                                                        
--  numoppitemtCode numeric(9),                   
--  numItemCode numeric(9),                                                        
--  numUnitHour numeric(9),                                                        
--  monPrice money,                                                     
--  monTotAmount money,                                                        
--  vcItemDesc varchar(1000),                  
--  numWarehouseItmsID numeric(9),        
--  vcItemType varchar(30),    
--  vcAttrValues varchar(50) ,
--  numUOM NUMERIC(18,0),
--  decUOMConversionFactor DECIMAL(18,5),bitDiscountType BIT,fltDiscount FLOAT,monTotAmtBefDiscount MONEY,
--  vcShippingMethod VARCHAR(100),decShippingCharge MONEY,dtDeliveryDate datetime
--  ))X                
--                
                
-- EXEC sp_xml_removedocument @hDocItem                
                
--end
                             
                            
select @TotalAmount=sum(monTotAmount)from OpportunityItems where numOppId=@numOppID                            
 update OpportunityMaster set  monPamount=@TotalAmount - @fltDiscount                           
 where numOppId=@numOppID                           
                          
--set @vcPOppName=(select  ISNULL(vcPOppName,numOppId) from OpportunityMaster where numOppId=@numOppID )

IF(@vcSource IS NOT NULL)
BEGIN
	insert into OpportunityLinking ([numParentOppID],[numChildOppID],[vcSource],[numParentOppBizDocID],numPromotionId,numSiteID)  values(null,@numOppID,@vcSource,NULL,@numProId,@numSiteID);
END

--Add/Update Address Details
DECLARE @vcStreet  varchar(100),@vcCity  varchar(50),@vcPostalCode  varchar(15),@vcCompanyName  varchar(100),@vcAddressName VARCHAR(50)      
DECLARE @numState  numeric(9),@numCountry  numeric(9), @numCompanyId  numeric(9) 
DECLARE @bitIsPrimary BIT;

--Bill Address
IF @numBillAddressId>0
BEGIN
 SELECT  @vcStreet=ISNULL(vcStreet,''),@vcCity=ISNULL(vcCity,''),@vcPostalCode=ISNULL(vcPostalCode,''),
		@numState=ISNULL(numState,0),@numCountry=ISNULL(numCountry,0),@bitIsPrimary=bitIsPrimary,
		@vcAddressName=vcAddressName
            FROM  dbo.AddressDetails WHERE   numDomainID = @numDomainID AND numAddressID = @numBillAddressId 
            
  
  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 0, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = '', --  varchar(100)
	@numCompanyId = 0, --  numeric(9, 0)
	@vcAddressName =@vcAddressName
	
        
END
  
  
  --Ship Address
IF @numShipAddressId>0
BEGIN
   	SELECT  @vcStreet=ISNULL(vcStreet,''),@vcCity=ISNULL(vcCity,''),@vcPostalCode=ISNULL(vcPostalCode,''),
		@numState=ISNULL(numState,0),@numCountry=ISNULL(numCountry,0),@bitIsPrimary=bitIsPrimary,
		@vcAddressName=vcAddressName
            FROM  dbo.AddressDetails WHERE   numDomainID = @numDomainID AND numAddressID = @numShipAddressId 
  
  
	select  @vcCompanyName=vcCompanyName,@numCompanyId=div.numCompanyID from CompanyInfo Com                            
	join divisionMaster Div                            
	on div.numCompanyID=com.numCompanyID                            
	where div.numdivisionID=@numDivID

  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 1, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = @vcCompanyName, --  varchar(100)
	@numCompanyId =@numCompanyId, --  numeric(9, 0)
	@vcAddressName =@vcAddressName
	
   	
END

--Insert Tax for Division                       
INSERT dbo.OpportunityMasterTaxItems (
	numOppId,
	numTaxItemID,
	fltPercentage
) SELECT @numOppID,TI.numTaxItemID,dbo.fn_CalItemTaxAmt(@numDomainID,@numDivID,0,TI.numTaxItemID,@numOppId,0,NULL) FROM TaxItems TI JOIN DivisionTaxTypes DTT ON TI.numTaxItemID = DTT.numTaxItemID
 WHERE DTT.numDivisionID=@numDivID AND DTT.bitApplicable=1
   union 
  select @numOppID,0,dbo.fn_CalItemTaxAmt(@numDomainID,@numDivID,0,0,@numOppId,1,NULL) 
  FROM dbo.DivisionMaster WHERE bitNoTax=0 AND numDivisionID=@numDivID
  
  --Delete Tax for Opportunity Items if item deleted 
DELETE FROM OpportunityItemsTaxItems WHERE numOppId=@numOppId AND numOppItemID NOT IN (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID)

--Insert Tax for Opportunity Items
INSERT INTO dbo.OpportunityItemsTaxItems (
	numOppId,
	numOppItemID,
	numTaxItemID
) SELECT @numOppId,OI.numoppitemtCode,TI.numTaxItemID FROM dbo.OpportunityItems OI JOIN dbo.ItemTax IT ON OI.numItemCode=IT.numItemCode JOIN
TaxItems TI ON TI.numTaxItemID=IT.numTaxItemID WHERE OI.numOppId=@numOppID AND IT.bitApplicable=1 AND 
OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
UNION
  select @numOppId,OI.numoppitemtCode,0 FROM dbo.OpportunityItems OI JOIN dbo.Item I ON OI.numItemCode=I.numItemCode
   WHERE OI.numOppId=@numOppID  AND I.bitTaxable=1 AND
OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)

  
 
/****** Object:  StoredProcedure [dbo].[USP_UpdatedomainDetails]    Script Date: 05/07/2009 21:56:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                                      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_updatedomaindetails')
DROP PROCEDURE usp_updatedomaindetails
GO
CREATE PROCEDURE [dbo].[USP_UpdatedomainDetails]                                      
@numDomainID as numeric(9)=0,                                      
@vcDomainName as varchar(50)='',                                      
@vcDomainDesc as varchar(100)='',                                      
@bitExchangeIntegration as bit,                                      
@bitAccessExchange as bit,                                      
@vcExchUserName as varchar(100)='',                                      
@vcExchPassword as varchar(100)='',                                      
@vcExchPath as varchar(200)='',                                      
@vcExchDomain as varchar(100)='',                                    
@tintCustomPagingRows as tinyint,                                    
@vcDateFormat as varchar(30)='',                                    
@numDefCountry as numeric(9),                                    
@tintComposeWindow as tinyint,                                    
@sintStartDate as smallint,                                    
@sintNoofDaysInterval as smallint,                                    
@tintAssignToCriteria as tinyint,                                    
@bitIntmedPage as bit,                                    
@tintFiscalStartMonth as tinyint,                            
--@bitDeferredIncome as bit,                               
@tintPayPeriod as tinyint,                        
@numCurrencyID as numeric(9) ,                
@charUnitSystem as char(1)='',              
@vcPortalLogo as varchar(100)='',            
@tintChrForComSearch as tinyint  ,            
@intPaymentGateWay as int,    
@tintChrForItemSearch as tinyint,
@ShipCompany as numeric(9),
@bitAutoPopulateAddress as bit=null,
@tintPoulateAddressTo as tinyint=null,
@bitMultiCurrency as bit,
--@bitMultiCompany as BIT,
@bitCreateInvoice AS tinyint = null,
@numDefaultSalesBizDocID AS NUMERIC(9)=NULL,
@numDefaultPurchaseBizDocID AS NUMERIC(9)=NULL,
@bitSaveCreditCardInfo AS BIT = NULL,
@intLastViewedRecord AS INT=10,
@tintCommissionType AS TINYINT,
@tintBaseTax AS TINYINT,
@bitEmbeddedCost AS BIT,
@numDefaultSalesOppBizDocId NUMERIC=NULL,
@tintSessionTimeOut TINYINT=1,
@tintDecimalPoints TINYINT,
@bitCustomizePortal BIT=0,
@tintBillToForPO TINYINT,
@tintShipToForPO TINYINT,
@tintOrganizationSearchCriteria TINYINT,
@bitDocumentRepositary BIT=0,
@tintComAppliesTo TINYINT=0,
@bitGtoBContact BIT=0,
@bitBtoGContact BIT=0,
@bitGtoBCalendar BIT=0,
@bitBtoGCalendar BIT=0,
@bitExpenseNonInventoryItem BIT=0,
@bitInlineEdit BIT=0,
@bitRemoveVendorPOValidation BIT=0,
@tintBaseTaxOnArea tinyint=0,
@bitAmountPastDue BIT=0,
@monAmountPastDue money,
@bitSearchOrderCustomerHistory BIT=0,
@bitRentalItem as bit=0,
@numRentalItemClass as NUMERIC(9)=NULL,
@numRentalHourlyUOM as NUMERIC(9)=NULL,
@numRentalDailyUOM as NUMERIC(9)=NULL,
@tintRentalPriceBasedOn as tinyint=NULL,
@tintCalendarTimeFormat as tinyint=NULL,
@tintDefaultClassType as tinyint=NULL,
@tintPriceBookDiscount as tinyint=0,
@bitAutoSerialNoAssign as bit=0,
@bitIsShowBalance AS BIT = 0,
@numIncomeAccID NUMERIC(18, 0) = 0,
@numCOGSAccID NUMERIC(18, 0) = 0,
@numAssetAccID NUMERIC(18, 0) = 0,
@IsEnableClassTracking AS BIT = 0,
@IsEnableProjectTracking AS BIT = 0,
@IsEnableCampaignTracking AS BIT = 0,
@IsEnableResourceScheduling AS BIT = 0,
@ShippingServiceItem  AS NUMERIC(9)=0,
@numSOBizDocStatus AS NUMERIC(9)=0,
@bitAutolinkUnappliedPayment AS BIT=0,
@numDiscountServiceItemID AS NUMERIC(9)=0,
@vcShipToPhoneNumber as VARCHAR(50)='',
@vcGAUserEmailId as VARCHAR(50)='',
@vcGAUserPassword as VARCHAR(50)='',
@vcGAUserProfileId as VARCHAR(50)='',
@bitDiscountOnUnitPrice AS BIT=0,
@intShippingImageWidth INT = 0,
@intShippingImageHeight INT = 0,
@numTotalInsuredValue NUMERIC(10,2) = 0,
@numTotalCustomsValue NUMERIC(10,2) = 0,
@vcHideTabs VARCHAR(1000) = '',
@bitUseBizdocAmount BIT = 0,
@IsEnableUserLevelClassTracking BIT = 0,
@bitDefaultRateType BIT = 0,
@bitIncludeTaxAndShippingInCommission  BIT = 0,
/*,@bitAllowPPVariance AS BIT=0*/
@bitPurchaseTaxCredit BIT=0,
@bitLandedCost BIT=0,
@vcLanedCostDefault VARCHAR(10)=''
as                                      
                                      
 update Domain                                       
   set                                       
   vcDomainName=@vcDomainName,                                      
   vcDomainDesc=@vcDomainDesc,                                      
   bitExchangeIntegration=@bitExchangeIntegration,                                      
   bitAccessExchange=@bitAccessExchange,                                      
   vcExchUserName=@vcExchUserName,                                      
   vcExchPassword=@vcExchPassword,                                      
   vcExchPath=@vcExchPath,                                      
   vcExchDomain=@vcExchDomain,                                    
   tintCustomPagingRows =@tintCustomPagingRows,                                    
   vcDateFormat=@vcDateFormat,                                    
   numDefCountry =@numDefCountry,                                    
   tintComposeWindow=@tintComposeWindow,                                    
   sintStartDate =@sintStartDate,                                    
   sintNoofDaysInterval=@sintNoofDaysInterval,                                    
   tintAssignToCriteria=@tintAssignToCriteria,                                    
   bitIntmedPage=@bitIntmedPage,                                    
   tintFiscalStartMonth=@tintFiscalStartMonth,                            
--   bitDeferredIncome=@bitDeferredIncome,                          
   tintPayPeriod=@tintPayPeriod,
   vcCurrency=isnull((select varCurrSymbol from Currency Where numCurrencyID=@numCurrencyID  ),'')  ,                      
  numCurrencyID=@numCurrencyID,                
  charUnitSystem= @charUnitSystem,              
  vcPortalLogo=@vcPortalLogo ,            
  tintChrForComSearch=@tintChrForComSearch  ,      
  intPaymentGateWay=@intPaymentGateWay,
 tintChrForItemSearch=@tintChrForItemSearch,
 numShipCompany= @ShipCompany,
 bitAutoPopulateAddress = @bitAutoPopulateAddress,
 tintPoulateAddressTo =@tintPoulateAddressTo,
 --bitMultiCompany=@bitMultiCompany ,
 bitMultiCurrency=@bitMultiCurrency,
 bitCreateInvoice= @bitCreateInvoice,
 [numDefaultSalesBizDocId] =@numDefaultSalesBizDocId,
 bitSaveCreditCardInfo=@bitSaveCreditCardInfo,
 intLastViewedRecord=@intLastViewedRecord,
 [tintCommissionType]=@tintCommissionType,
 [tintBaseTax]=@tintBaseTax,
 [numDefaultPurchaseBizDocId]=@numDefaultPurchaseBizDocID,
 bitEmbeddedCost=@bitEmbeddedCost,
 numDefaultSalesOppBizDocId=@numDefaultSalesOppBizDocId,
 tintSessionTimeOut=@tintSessionTimeOut,
 tintDecimalPoints=@tintDecimalPoints,
 bitCustomizePortal=@bitCustomizePortal,
 tintBillToForPO = @tintBillToForPO,
 tintShipToForPO = @tintShipToForPO,
 tintOrganizationSearchCriteria=@tintOrganizationSearchCriteria,
-- vcPortalName=@vcPortalName,
 bitDocumentRepositary=@bitDocumentRepositary,tintComAppliesTo=@tintComAppliesTo,
 bitGtoBContact=@bitGtoBContact,
 bitBtoGContact=@bitBtoGContact,
 bitGtoBCalendar=@bitGtoBCalendar,
 bitBtoGCalendar=@bitBtoGCalendar,
 bitExpenseNonInventoryItem=@bitExpenseNonInventoryItem,
 bitInlineEdit=@bitInlineEdit,
 bitRemoveVendorPOValidation=@bitRemoveVendorPOValidation,
tintBaseTaxOnArea=@tintBaseTaxOnArea,
bitAmountPastDue = @bitAmountPastDue,
monAmountPastDue = @monAmountPastDue,
bitSearchOrderCustomerHistory=@bitSearchOrderCustomerHistory,
bitRentalItem=@bitRentalItem,
numRentalItemClass=@numRentalItemClass,
numRentalHourlyUOM=@numRentalHourlyUOM,
numRentalDailyUOM=@numRentalDailyUOM,
tintRentalPriceBasedOn=@tintRentalPriceBasedOn,
tintCalendarTimeFormat=@tintCalendarTimeFormat,
tintDefaultClassType=@tintDefaultClassType,
tintPriceBookDiscount=@tintPriceBookDiscount,
bitAutoSerialNoAssign=@bitAutoSerialNoAssign,
bitIsShowBalance = @bitIsShowBalance,
numIncomeAccID = @numIncomeAccID,
numCOGSAccID = @numCOGSAccID,
numAssetAccID = @numAssetAccID,
IsEnableClassTracking = @IsEnableClassTracking,
IsEnableProjectTracking = @IsEnableProjectTracking,
IsEnableCampaignTracking = @IsEnableCampaignTracking,
IsEnableResourceScheduling = @IsEnableResourceScheduling,
numShippingServiceItemID = @ShippingServiceItem,
numSOBizDocStatus=@numSOBizDocStatus,
bitAutolinkUnappliedPayment=@bitAutolinkUnappliedPayment,
numDiscountServiceItemID=@numDiscountServiceItemID,
vcShipToPhoneNo = @vcShipToPhoneNumber,
vcGAUserEMail = @vcGAUserEmailId,
vcGAUserPassword = @vcGAUserPassword,
vcGAUserProfileId = @vcGAUserProfileId,
bitDiscountOnUnitPrice=@bitDiscountOnUnitPrice,
intShippingImageWidth = @intShippingImageWidth ,
intShippingImageHeight = @intShippingImageHeight,
numTotalInsuredValue = @numTotalInsuredValue,
numTotalCustomsValue = @numTotalCustomsValue,
vcHideTabs = @vcHideTabs,
bitUseBizdocAmount = @bitUseBizdocAmount,
IsEnableUserLevelClassTracking = @IsEnableUserLevelClassTracking,
bitDefaultRateType = @bitDefaultRateType,
bitIncludeTaxAndShippingInCommission = @bitIncludeTaxAndShippingInCommission,
/*,bitAllowPPVariance=@bitAllowPPVariance*/
bitPurchaseTaxCredit=@bitPurchaseTaxCredit,
bitLandedCost=@bitLandedCost,
vcLanedCostDefault=@vcLanedCostDefault
 where numDomainId=@numDomainID

/****** Object:  StoredProcedure [dbo].[USP_UserList]    Script Date: 07/26/2008 16:21:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj    
--Modified By Sachin Sadhu
--Worked on AND-OR condition       
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_userlist')
DROP PROCEDURE usp_userlist
GO
CREATE PROCEDURE [dbo].[USP_UserList]                  
 @UserName as varchar(100)='',                  
 @tintGroupType as tinyint,                  
 @SortChar char(1)='0',                  
 @CurrentPage int,                  
 @PageSize int,                  
 @TotRecs int output,                  
 @columnName as Varchar(50),                  
 @columnSortOrder as Varchar(10),            
 @numDomainID as numeric(9),          
 @Status as char(1)                  
                  
as                  
                     
Create table #tempTable ( ID INT IDENTITY PRIMARY KEY,                   
      numUserID VARCHAR(15),                  
      vcEmailID varchar(100),                  
      [Name] VARCHAR(100),                  
      vcGroupName varchar(100),                  
 vcDomainName varchar(50),          
 Active varchar(10),
 numDefaultWarehouse numeric(18,0)              
 )                  
declare @strSql as varchar(8000)                  
set @strSql='SELECT numUserID, isnull(vcEmailID,''-'') as vcEmailID,                  
ISNULL(ADC.vcfirstname,'''') +'' ''+ ISNULL(ADC.vclastname,'''') as Name,                  
isnull(vcGroupName,''-'')as vcGroupName, vcDomainName, case when bitActivateFlag=1 then ''Active'' else ''Suspended'' end as bitActivateFlag, numDefaultWarehouse                          
   FROM UserMaster                    
   join Domain                      
   on UserMaster.numDomainID =  Domain.numDomainID                     
    join AdditionalContactsInformation ADC                    
   on ADC.numContactid=UserMaster.numUserDetailId                   
   left join AuthenticationGroupMaster GM                   
   on Gm.numGroupID= UserMaster.numGroupID              
    where  UserMaster.numDomainID='+convert(varchar(15),@numDomainID)+ ' and bitActivateFlag=' +@Status               
                  
if @SortChar<>'0' set @strSql=@strSql + ' And (vcUserName like '''+@SortChar+'%'' OR vcEmailID LIKE '''+@SortChar+'%'')'

if @UserName<>''  set @strSql=@strSql + ' And (vcUserName like '''+@UserName+'%'' OR vcEmailID LIKE '''+@UserName+'%'')'

if @columnName<>'' set @strSql=@strSql + ' ORDER BY ' + @columnName +' '+ @columnSortOrder                  
   print @strSql               
insert into #tempTable(                  
 numUserID,                  
      vcEmailID,                  
      [Name],                  
      vcGroupName,                  
 vcDomainName,Active,numDefaultWarehouse)                  
exec (@strSql)                   
                  
  declare @firstRec as integer                   
  declare @lastRec as integer                  
 set @firstRec= (@CurrentPage-1) * @PageSize                  
     set @lastRec= (@CurrentPage*@PageSize+1)                  
select * from #tempTable where ID > @firstRec and ID < @lastRec                  
set @TotRecs=(select count(*) from #tempTable)                  
drop table #tempTable            
          
          
select  vcCompanyName,ISNULL(intNoofUsersSubscribed,0) + ISNULL(intNoofPartialSubscribed,0)/2 + ISNULL(intNoofMinimalSubscribed,0)/10 AS intNoofUsersSubscribed,dtSubStartDate,dtSubEndDate,(select count(*) from UserMaster  join Domain                      
   on UserMaster.numDomainID =  Domain.numDomainID                     
    join AdditionalContactsInformation ADC                    
   on ADC.numContactid=UserMaster.numUserDetailId  where Domain.numDomainID=@numDomainID and bitActivateFlag=1) as ActiveSub          
from Subscribers S           
Join Domain D          
on D.numDomainID=S.numTargetDomainID          
join DivisionMaster Div          
on Div.numDivisionID=D.numDivisionID          
join CompanyInfo C          
on C.numCompanyID=Div.numCompanyID          
where D.numDomainID=@numDomainID
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_CheckDuplicateBizDocs' ) 
    DROP PROCEDURE USP_CheckDuplicateBizDocs
GO
-- =============================================
-- Author:		<Author,,Sachin Sadhu>
-- Create date: <Create Date,,10thJun2014>
-- Description:	<Description,,BizDoc details>
-- =============================================
CREATE PROCEDURE USP_CheckDuplicateBizDocs
	-- Add the parameters for the stored procedure here
 @numDomainId numeric(18,0) ,
 @numBizDocId numeric(18,0),
 @tintOppType int,
 @numBizDocTemplateID numeric(18,0),
 @numOppId numeric(18,0)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
SELECT 'col1' FROM OpportunityBizDocs as od INNER JOIN OpportunityMaster o on od.numOppId=o.numOppId  
			  WHERE o.numDomainId=@numDomainId AND
					  od.numBizDocId=@numBizDocId AND 
					  o.tintOppType=@tintOppType AND
					  od.numBizDocTempID=@numBizDocTemplateID
					  and o.numOppId=@numOppId
END


	
GO
IF EXISTS ( SELECT * FROM sysobjects WHERE xtype = 'V' AND NAME = 'VIEW_GENERALLEDGER' ) 
    DROP VIEW VIEW_GENERALLEDGER
GO
CREATE VIEW [dbo].[VIEW_GENERALLEDGER]
AS
SELECT     dbo.General_Journal_Header.numJournal_Id, dbo.General_Journal_Header.datEntry_Date, dbo.General_Journal_Header.varDescription, 
                      ISNULL(dbo.VIEW_BIZPAYMENT.Narration,dbo.General_Journal_Header.varDescription) AS BizPayment, dbo.General_Journal_Details.numTransactionId,
                     CASE WHEN isnull(dbo.General_Journal_Header.numReturnID, 0) <> 0 THEN ' Chek No: ' + isnull( CAST(CH.numCheckNo AS VARCHAR(50)),'No-Cheq') ELSE  dbo.VIEW_JOURNALCHEQ.Narration END AS CheqNo, 
                      dbo.General_Journal_Header.numDomainId, 'Biz Doc Id: ' + dbo.OpportunityBizDocs.vcBizDocID AS BizDocID, 
                      dbo.General_Journal_Header.numOppBizDocsId, dbo.General_Journal_Details.vcReference AS TranRef, 
                      dbo.General_Journal_Details.varDescription AS TranDesc, dbo.General_Journal_Details.numDebitAmt, dbo.General_Journal_Details.numCreditAmt, 
                      dbo.Chart_Of_Accounts.numAccountId, dbo.Chart_Of_Accounts.vcAccountName,
                      CASE WHEN isnull(dbo.General_Journal_Header.numCheckHeaderID, 0) <> 0 THEN + 'Checks' 
                           WHEN isnull(dbo.General_Journal_Header.numCashCreditCardId, 0) <> 0 AND dbo.CashCreditCardDetails.bitMoneyOut = 0 THEN 'Cash' 
                           WHEN isnull(dbo.General_Journal_Header.numCashCreditCardId, 0) <> 0 AND dbo.CashCreditCardDetails.bitMoneyOut = 1 AND dbo.CashCreditCardDetails.bitChargeBack = 1 THEN 'Charge' 
                           WHEN isnull(dbo.General_Journal_Header.numDepositId, 0) <> 0 AND DM.tintDepositePage = 1 THEN 'Deposit' 
						   WHEN isnull(dbo.General_Journal_Header.numDepositId, 0) <> 0 AND DM.tintDepositePage = 2 THEN 'Receved Pmt' 
						   --WHEN isnull(dbo.General_Journal_Header.numBizDocsPaymentDetId, 0) <> 0 AND dbo.OpportunityMaster.tintOppType = 1 THEN 'Receive Amt' 
						   --WHEN isnull(dbo.General_Journal_Header.numBizDocsPaymentDetId, 0) <> 0 AND dbo.OpportunityMaster.tintOppType = 2 THEN 'Vendor Amt' 
						      WHEN isnull(dbo.General_Journal_Header.numOppId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numOppBizDocsId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numBizDocsPaymentDetId, 0) = 0 AND 
									dbo.OpportunityMaster.tintOppType = 1 THEN 'BizDocs Invoice' 
						   WHEN isnull(dbo.General_Journal_Header.numOppId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numOppBizDocsId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numBizDocsPaymentDetId, 0) = 0 AND 
									dbo.OpportunityMaster.tintOppType = 2 THEN 'BizDocs Purchase' 
						   WHEN isnull(dbo.General_Journal_Header.numCategoryHDRID, 0) <> 0 THEN 'Time And Expenses' 
						   WHEN ISNULL(General_Journal_Header.numBillId,0) <>0 AND isnull(BH.bitLandedCost,0)=1 then 'Landed Cost' 
						   WHEN isnull(dbo.General_Journal_Header.numBillID, 0) <> 0 THEN 'Bill'
						   WHEN isnull(dbo.General_Journal_Header.numBillPaymentID, 0) <> 0 THEN 'Pay Bill'
						   WHEN isnull(dbo.General_Journal_Header.numReturnID, 0) <> 0 THEN 
									CASE WHEN RH.tintReturnType=1 AND RH.tintReceiveType=1 THEN 'Sales Return Refund'
									     WHEN RH.tintReturnType=1 AND RH.tintReceiveType=2 THEN 'Sales Return Credit Memo'
									     WHEN RH.tintReturnType=2 AND RH.tintReceiveType=2 THEN 'Purchase Return Credit Memo'
									     WHEN RH.tintReturnType=3 AND RH.tintReceiveType=2 THEN 'Credit Memo'
									     WHEN RH.tintReturnType=4 AND RH.tintReceiveType=1 THEN 'Refund' END 
						   WHEN isnull(dbo.General_Journal_Header.numOppId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numOppBizDocsId, 0) = 0 THEN 'PO Fulfillment'
						   WHEN dbo.General_Journal_Header.numJournal_Id <> 0 THEN 'Journal' END AS TransactionType,
						    dbo.CompanyInfo.vcCompanyName AS CompanyName, dbo.General_Journal_Header.numCheckHeaderID, 
                      dbo.General_Journal_Header.numCashCreditCardId, dbo.General_Journal_Header.numOppId, dbo.General_Journal_Header.numDepositId, 
                      dbo.General_Journal_Header.numCategoryHDRID, dbo.TimeAndExpense.tintTEType, dbo.TimeAndExpense.numCategory, 
                      dbo.TimeAndExpense.dtFromDate, dbo.TimeAndExpense.numUserCntID,
                      dbo.DivisionMaster.numDivisionID,ISNULL(dbo.General_Journal_Details.bitCleared,0) AS bitCleared,
                      ISNULL(dbo.General_Journal_Details.bitReconcile,0) AS bitReconcile,isnull(dbo.General_Journal_Header.numBillID, 0) AS numBillID,ISNULL(dbo.General_Journal_Header.numBillPaymentID,0) AS numBillPaymentID,
                      ISNULL(General_Journal_Details.numClassID,0) AS numClassID,ISNULL(General_Journal_Details.numProjectID,0) AS numProjectID,isnull(dbo.General_Journal_Header.numReturnID, 0) AS numReturnID,
                      General_Journal_Details.numCurrencyID,ISNULL(General_Journal_Details.numItemID,0) AS numItemID,
					  Case When isnull(BH.bitLandedCost,0)=1 then  isnull(BH.numOppId,0) else 0 end AS numLandedCostOppId
FROM         dbo.General_Journal_Header INNER JOIN
                      dbo.General_Journal_Details ON dbo.General_Journal_Header.numJournal_Id = dbo.General_Journal_Details.numJournalId INNER JOIN
                      dbo.Chart_Of_Accounts ON dbo.General_Journal_Details.numChartAcntId = dbo.Chart_Of_Accounts.numAccountId LEFT OUTER JOIN
                      dbo.TimeAndExpense ON dbo.General_Journal_Header.numCategoryHDRID = dbo.TimeAndExpense.numCategoryHDRID LEFT OUTER JOIN
                      dbo.DivisionMaster LEFT OUTER JOIN
                      dbo.CompanyInfo ON dbo.DivisionMaster.numCompanyID = dbo.CompanyInfo.numCompanyId ON 
                      dbo.General_Journal_Details.numCustomerId = dbo.DivisionMaster.numDivisionID LEFT OUTER JOIN
                      dbo.OpportunityMaster ON dbo.General_Journal_Header.numOppId = dbo.OpportunityMaster.numOppId LEFT OUTER JOIN
                      dbo.CashCreditCardDetails ON dbo.General_Journal_Header.numCashCreditCardId = dbo.CashCreditCardDetails.numCashCreditId LEFT OUTER JOIN
                      dbo.OpportunityBizDocs ON dbo.General_Journal_Header.numOppBizDocsId = dbo.OpportunityBizDocs.numOppBizDocsId LEFT OUTER JOIN
                      dbo.VIEW_JOURNALCHEQ ON (dbo.General_Journal_Header.numCheckHeaderID = dbo.VIEW_JOURNALCHEQ.numCheckHeaderID 
                      or (dbo.General_Journal_Header.numBillPaymentID=dbo.VIEW_JOURNALCHEQ.numReferenceID and dbo.VIEW_JOURNALCHEQ.tintReferenceType=8))LEFT OUTER JOIN
                      dbo.VIEW_BIZPAYMENT ON dbo.General_Journal_Header.numBizDocsPaymentDetId = dbo.VIEW_BIZPAYMENT.numBizDocsPaymentDetId LEFT OUTER JOIN
                      dbo.DepositMaster DM ON DM.numDepositId = dbo.General_Journal_Header.numDepositId  LEFT OUTER JOIN
                      dbo.ReturnHeader RH ON RH.numReturnHeaderID = dbo.General_Journal_Header.numReturnID LEFT OUTER JOIN
                      dbo.CheckHeader CH ON CH.numReferenceID=RH.numReturnHeaderID AND CH.tintReferenceType=10
					  LEFT OUTER JOIN BillHeader BH  ON ISNULL(General_Journal_Header.numBillId,0) = BH.numBillId

GO
