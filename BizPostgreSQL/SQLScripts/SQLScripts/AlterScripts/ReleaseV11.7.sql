/******************************************************************
Project: Release 11.7 Date: 10.APR.2019
Comments: ALTER SCRIPTS
*******************************************************************/

/******************************************** PRASANT *********************************************/

UPDATE DycFieldMaster SET vcPropertyName='vcCompactContactDetails' WHERE numFieldId=941

BEGIN TRY
BEGIN TRANSACTION

DECLARE @numFieldID AS NUMERIC(18,0)
DECLARE @numFormFieldID AS NUMERIC(18,0)


SELECT @numFieldID = 941
--Lead Form
INSERT INTO DynamicFormFieldMaster
(numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
 bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
VALUES
(34,'Contact','R','DateField','vcCompactContactDetails',0,0,'V','vcContactName',0,'AdditionalContactsInformation',0,7,1,'vcContactName',1,7,1,0,0,1)

SELECT @numFormFieldID = SCOPE_IDENTITY()

INSERT INTO DycFormField_Mapping
(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
VALUES
(2,@numFieldID,34,0,0,'Contact','DateField',NULL,7,1,7,1,0,0,1,0,1,1,1,@numFormFieldID)

--Prospect Form
INSERT INTO DynamicFormFieldMaster
(numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
 bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
VALUES
(35,'Contact','R','DateField','vcCompactContactDetails',0,0,'V','vcContactName',0,'AdditionalContactsInformation',0,7,1,'vcContactName',1,7,1,0,0,1)

SELECT @numFormFieldID = SCOPE_IDENTITY()

INSERT INTO DycFormField_Mapping
(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
VALUES
(3,@numFieldID,35,0,0,'Contact','DateField',NULL,7,1,7,1,0,0,1,0,1,1,1,@numFormFieldID)

--Contact Form
INSERT INTO DynamicFormFieldMaster
(numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
 bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
VALUES
(10,'Contact','R','TextBox','vcCompactContactDetails',0,0,'V','vcContactName',0,'AdditionalContactsInformation',0,7,1,'vcContactName',1,7,1,0,0,1)

SELECT @numFormFieldID = SCOPE_IDENTITY()

INSERT INTO DycFormField_Mapping
(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
VALUES
(32,@numFieldID,10,0,0,'Contact','TextBox',NULL,7,1,7,1,0,0,1,0,1,1,1,@numFormFieldID)

--Case Form
INSERT INTO DynamicFormFieldMaster
(numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
 bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
VALUES
(12,'Contact','R','TextBox','vcCompactContactDetails',0,0,'V','vcContactName',0,'AdditionalContactsInformation',0,7,1,'vcContactName',1,7,1,0,0,1)

SELECT @numFormFieldID = SCOPE_IDENTITY()

INSERT INTO DycFormField_Mapping
(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
VALUES
(7,@numFieldID,12,0,0,'Contact','TextBox',NULL,7,1,7,1,0,0,1,0,1,1,1,@numFormFieldID)


--Account Form
INSERT INTO DynamicFormFieldMaster
(numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
 bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
VALUES
(36,'Contact','R','DateField','vcCompactContactDetails',0,0,'V','vcContactName',0,'AdditionalContactsInformation',0,7,1,'vcContactName',1,7,1,0,0,1)

SELECT @numFormFieldID = SCOPE_IDENTITY()

INSERT INTO DycFormField_Mapping
(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
VALUES
(4,@numFieldID,36,0,0,'Contact','DateField',NULL,7,1,7,1,0,0,1,0,1,1,1,@numFormFieldID)


----Sales Opportunity
INSERT INTO DynamicFormFieldMaster
(numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
 bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
VALUES
(38,'Contact','R','DateField','vcCompactContactDetails',0,0,'V','vcContactName',0,'AdditionalContactsInformation',0,7,1,'vcContactName',1,7,1,0,0,1)

SELECT @numFormFieldID = SCOPE_IDENTITY()

INSERT INTO DycFormField_Mapping
(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
VALUES
(10,@numFieldID,38,0,0,'Contact','DateField',NULL,7,1,7,1,0,0,1,0,1,1,1,@numFormFieldID)

--Purchase Opportunity
INSERT INTO DynamicFormFieldMaster
(numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
 bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
VALUES
(40,'Contact','R','DateField','vcCompactContactDetails',0,0,'V','vcContactName',0,'AdditionalContactsInformation',0,7,1,'vcContactName',1,7,1,0,0,1)

SELECT @numFormFieldID = SCOPE_IDENTITY()

INSERT INTO DycFormField_Mapping
(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
VALUES
(10,@numFieldID,40,0,0,'Contact','DateField',NULL,7,1,7,1,0,0,1,0,1,1,1,@numFormFieldID)

----Sales Order
INSERT INTO DynamicFormFieldMaster
(numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
 bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
VALUES
(39,'Contact','R','DateField','vcCompactContactDetails',0,0,'V','vcContactName',0,'AdditionalContactsInformation',0,7,1,'vcContactName',1,7,1,0,0,1)

SELECT @numFormFieldID = SCOPE_IDENTITY()

INSERT INTO DycFormField_Mapping
(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
VALUES
(10,@numFieldID,39,0,0,'Contact','DateField',NULL,7,1,7,1,0,0,1,0,1,1,1,@numFormFieldID)

--Purchase Order
INSERT INTO DynamicFormFieldMaster
(numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
 bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
VALUES
(41,'Contact','R','DateField','vcCompactContactDetails',0,0,'V','vcContactName',0,'AdditionalContactsInformation',0,7,1,'vcContactName',1,7,1,0,0,1)

SELECT @numFormFieldID = SCOPE_IDENTITY()

INSERT INTO DycFormField_Mapping
(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
VALUES
(10,@numFieldID,41,0,0,'Contact','DateField',NULL,7,1,7,1,0,0,1,0,1,1,1,@numFormFieldID)

COMMIT
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		ERROR_MESSAGE(),
		ERROR_NUMBER(),
		ERROR_SEVERITY(),
		ERROR_STATE(),
		ERROR_LINE(),
		ISNULL(ERROR_PROCEDURE(), '-');
END CATCH




/******************************************** RAJIB *********************************************/


UPDATE ReportListMaster SET vcReportName='Sales vs Expenses', vcReportDescription='Sales vs Expenses' WHERE numReportID=1662
UPDATE ReportListMaster SET vcReportName='Lead Sources', vcReportDescription='Lead Sources' WHERE numReportID=1672
UPDATE ReportListMaster SET vcReportName='Sales Opportunities by Revenue', vcReportDescription='Sales Opportunities by Revenue' WHERE numReportID=1665
UPDATE ReportListMaster SET vcReportName='Employee Benefit to Company', vcReportDescription='Employee Benefit to Company' WHERE numReportID=1725
UPDATE ReportListMaster SET vcReportName='Employee Sales - 1', vcReportDescription='Employee Sales - 1' WHERE numReportID=1722
UPDATE ReportListMaster SET vcReportName='Employee Sales - 2', vcReportDescription='Employee Sales - 2' WHERE numReportID=1724
UPDATE ReportListMaster SET vcReportName='Opportunities', vcReportDescription='Opportunities' WHERE numReportID=1668
UPDATE ReportListMaster SET vcReportName='Revenue, Profit, & Expense Comparison KPI', vcReportDescription='Revenue, Profit, & Expense Comparison KPI' WHERE numReportID=1661
UPDATE ReportListMaster SET vcReportName='Top sources of Sales Orders', vcReportDescription='Top sources of Sales Orders' WHERE numReportID=1663
UPDATE ReportListMaster SET vcReportName='Top Customers', vcReportDescription='Top Customers' WHERE numReportID=1657
UPDATE ReportListMaster SET vcReportName='Sales Return / RMA Reasons', vcReportDescription='Sales Return / RMA Reasons' WHERE numReportID=1721
UPDATE ReportListMaster SET vcReportName='Item Sales Returns / RMAs vs Quantity Sold', vcReportDescription='Item Sales Returns / RMAs vs Quantity Sold' WHERE numReportID=1667
UPDATE ReportListMaster SET vcReportName='Items Sales', vcReportDescription='Items Sales' WHERE numReportID=1654
UPDATE ReportListMaster SET vcReportName='Employee Benefit to Company', vcReportDescription='Employee Benefit to Company' WHERE numReportID=1725

UPDATE ReportDashboard SET numReportID=1657 WHERE numReportID=1658
DELETE FROM ReportListMaster WHERE numReportID=1658

UPDATE ReportDashboard SET numReportID=1654 WHERE numReportID IN (1655,1664)
DELETE FROM ReportListMaster WHERE numReportID IN (1655,1664)

ALTER TABLE ReportDashboard ADD numRecordCount NUMERIC(18,0);
ALTER TABLE ReportDashboard ADD tintControlField NUMERIC(18,0);
ALTER TABLE ReportDashboard ADD vcDealAmount Varchar(250);
ALTER TABLE ReportDashboard ADD tintOppType NUMERIC(18,0);
ALTER TABLE ReportDashboard ADD lngPConclAnalysis VARCHAR(250);
ALTER TABLE ReportDashboard ADD bitTask VARCHAR(250);
ALTER TABLE ReportDashboard ADD tintTotalProgress NUMERIC(18,0);
ALTER TABLE ReportDashboard ADD tintMinNumber NUMERIC(18,0);
ALTER TABLE ReportDashboard ADD tintMaxNumber NUMERIC(18,0);
ALTER TABLE ReportDashboard ADD tintQtyToDisplay NUMERIC(18,0);
ALTER TABLE ReportDashboard DROP COLUMN vcTeams;
ALTER TABLE ReportDashboard DROP COLUMN vcEmploees;
ALTER TABLE UserMaster ADD vcDashboardTemplateIDs Varchar(250);
ALTER TABLE ReportDashboard ADD vcDueDate Varchar(250);
 

UPDATE ReportDashboard SET vcTimeLine='Last12Months' WHERE numReportID=1725
UPDATE ReportDashboard SET vcTimeLine='Last12Months' WHERE numReportID=1722
UPDATE ReportDashboard SET vcTimeLine='Last12Months' WHERE numReportID=1724
UPDATE ReportDashboard SET vcTimeLine='Last12Months' WHERE numReportID=1668
UPDATE ReportDashboard SET vcTimeLine='Last12Months' WHERE numReportID=1662


UPDATE ReportDashboard SET vcTimeLine='Last12Months' WHERE numReportID=1657
UPDATE ReportDashboard SET vcTimeLine='Last12Months' WHERE numReportID=1654
UPDATE ReportDashboard SET vcTimeLine='Last12Months' WHERE numReportID=1667
UPDATE ReportDashboard SET vcTimeLine='Last12Months' WHERE numReportID=1665
UPDATE ReportDashboard SET vcTimeLine='Last12Months' WHERE numReportID=1666

