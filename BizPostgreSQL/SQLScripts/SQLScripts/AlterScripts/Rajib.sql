-------------------------------------------------------------------------------------------------------------------------
------------------------------------------ PLECE NEW SCRIPTS ABOVE IT ---------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------

----09.02.2019 :
---- Changes on report table field name 
----SELECT * FROM ReportListMaster WHERE bitDefault=1 AND numDomainID=0 AND intDefaultReportID=13
--UPDATE ReportListMaster SET vcReportName='Sales vs Expenses', vcReportDescription='Sales vs Expenses' WHERE numReportID=70604
----Rename "Top 10 Lead Sources (New Leads)" to "Lead Sources".
--UPDATE ReportListMaster SET vcReportName='Lead Sources', vcReportDescription='Lead Sources' WHERE numReportID=70617

----Rename "Top 10 Lead Sources (New Leads)" to "Lead Sources".
--UPDATE ReportListMaster SET vcReportName='Sales Opportunities by Revenue', vcReportDescription='Sales Opportunities by Revenue' WHERE numReportID=70607

----Rename "Employee Benefit to Company" to "Employee Benefit to Company".
--UPDATE ReportListMaster SET vcReportName='Employee Benefit to Company', vcReportDescription='Employee Benefit to Company' WHERE numReportID=70630

----Rename "Employee Benefit to Company" to "Employee Benefit to Company".
--UPDATE ReportListMaster SET vcReportName='Employee Benefit to Company', vcReportDescription='Employee Benefit to Company' WHERE numReportID=70630

----Rename "Employee Sales Performance Panel 1" to "Employee Sales - 1".
--UPDATE ReportListMaster SET vcReportName='Employee Sales - 1', vcReportDescription='Employee Sales - 1' WHERE numReportID=70627

----Rename "Employee Sales Performance pt 1 (Last 12 months)" to "Employee Sales - 2".
--UPDATE ReportListMaster SET vcReportName='Employee Sales - 2', vcReportDescription='Employee Sales - 2' WHERE numReportID=70629

----Rename "Largest 10 Sales Opportunities past their due date" to "Opportunities".
--UPDATE ReportListMaster SET vcReportName='Opportunities', vcReportDescription='Opportunities' WHERE numReportID=70610

----Rename "Last month vs same period last month" to "Revenue, Profit, & Expense Comparison KPI".
--UPDATE ReportListMaster SET vcReportName='Revenue, Profit, & Expense Comparison KPI', vcReportDescription='Revenue, Profit, & Expense Comparison KPI' WHERE numReportID=70603

----Rename "Top sources of Sales Orders (Last 12 months)" to "Top sources of Sales Orders".
--UPDATE ReportListMaster SET vcReportName='Top sources of Sales Orders', vcReportDescription='Top sources of Sales Orders' WHERE numReportID=70605

----Rename "Top 10 Customer by profit margin" to "Top Customers".
--UPDATE ReportListMaster SET vcReportName='Top Customers', vcReportDescription='Top Customers' WHERE numReportID=70599

----Add a field "numRecordCount" in "ReportDashboard".
--ALTER TABLE ReportDashboard ADD numRecordCount NUMERIC(18,0);
----Add a field "tintControlField" in "ReportDashboard".
--ALTER TABLE ReportDashboard ADD tintControlField NUMERIC(18,0);
----Add a field "vcDealAmount" in "ReportDashboard".
--ALTER TABLE ReportDashboard ADD vcDealAmount Varchar(250);
----Add a field "tintOppType" in "ReportDashboard".
--ALTER TABLE ReportDashboard ADD tintOppType NUMERIC(18,0);

----Add a field "lngPConclAnalysis" in "ReportDashboard".
--ALTER TABLE ReportDashboard ADD lngPConclAnalysis VARCHAR(250);

----Add a field "bitTask" in "ReportDashboard".
--ALTER TABLE ReportDashboard ADD bitTask VARCHAR(250);

----Add a field "tintTotalProgress" in "ReportDashboard".
--ALTER TABLE ReportDashboard ADD tintTotalProgress NUMERIC(18,0);

----Add a field "tintMinNumber" in "ReportDashboard".
--ALTER TABLE ReportDashboard ADD tintMinNumber NUMERIC(18,0);
----Add a field "tintMaxNumber" in "ReportDashboard".
--ALTER TABLE ReportDashboard ADD tintMaxNumber NUMERIC(18,0);

----Add a field "tintQtyToDisplay" in "ReportDashboard".
--ALTER TABLE ReportDashboard ADD tintQtyToDisplay NUMERIC(18,0);
 
 
----Drop a field "vcTeams" in "ReportDashboard".
--ALTER TABLE ReportDashboard DROP COLUMN vcTeams;

----Drop a field "vcEmploees" in "ReportDashboard".
--ALTER TABLE ReportDashboard DROP COLUMN vcEmploees;

----Drop a field "vcEmploees" in "UserMaster".
--ALTER TABLE UserMaster ADD vcDashboardTemplateIDs Varchar(250);
----Drop a field "vcEmploees" in "UserMaster".
--ALTER TABLE ReportDashboard ADD vcDueDate Varchar(250);



----Rename "Top 10 Reasons for Sales Returns" to "Sales Return / RMA Reasons".
--UPDATE ReportListMaster SET vcReportName='Sales Return / RMA Reasons', vcReportDescription='Sales Return / RMA Reasons' WHERE numReportID=70626

----Rename "Top 10 items returned vs Qty Sold" to "Item Sales Returns / RMAs vs Quantity Sold".
--UPDATE ReportListMaster SET vcReportName='Item Sales Returns / RMAs vs Quantity Sold', vcReportDescription='Item Sales Returns / RMAs vs Quantity Sold' WHERE numReportID=70609

------Rename "Top 10 Items by profit amount" to "Items Sales".
----UPDATE ReportListMaster SET vcReportName='Top 10 Customer by profit amount', vcReportDescription='Top 10 Customer by profit amount' WHERE numReportID=70600

------Rename "Top 10 Items by profit margin" to "Items Sales".
----UPDATE ReportListMaster SET vcReportName='Top 10 Customer by profit margin', vcReportDescription='Top 10 Customer by profit margin' WHERE numReportID=70599

------Rename "Top 10 Items by profit margin" to "Items Sales".
----UPDATE ReportListMaster SET vcReportName='Top 10 Items by profit margin', vcReportDescription='Top 10 Items by profit margin' WHERE numReportID=70606

----Rename "Top 10 Items by profit amount (Last 12 months)" to "Items Sales".
--UPDATE ReportListMaster SET vcReportName='Items Sales', vcReportDescription='Items Sales' WHERE numReportID=70596

--UPDATE RD SET RD.numRecordCount=10 from ReportDashboard  RD 
--JOIN ReportListMaster RM ON RD.numReportID=RM.numReportID
--WHERE RD.numRecordCount IS NULL AND RM.intDefaultReportID in (5,8,9,16,18,23,28)

--UPDATE RD SET RD.vcTimeLine='Last12Months' from ReportDashboard  RD 
--JOIN ReportListMaster RM ON RD.numReportID=RM.numReportID
--WHERE RD.vcTimeLine IS NULL AND RM.intDefaultReportID in (5,8,9,10,13,16,19,23,29,31,32,34,35)

--UPDATE RD SET RD.vcGroupBy='Month' from ReportDashboard  RD 
--JOIN ReportListMaster RM ON RD.numReportID=RM.numReportID
--WHERE RD.vcGroupBy IS NULL AND RM.intDefaultReportID in (5,13,23,34,35)

--UPDATE RD SET RD.tintControlField=1 from ReportDashboard  RD 
--JOIN ReportListMaster RM ON RD.numReportID=RM.numReportID
--WHERE RD.tintControlField IS NULL AND RM.intDefaultReportID in (5,8,9)

--UPDATE RD SET RD.tintQtyToDisplay=5 from ReportDashboard  RD 
--JOIN ReportListMaster RM ON RD.numReportID=RM.numReportID
--WHERE RD.tintQtyToDisplay IS NULL AND RM.intDefaultReportID in (19)

--UPDATE RD SET RD.tintTotalProgress=50 from ReportDashboard  RD 
--JOIN ReportListMaster RM ON RD.numReportID=RM.numReportID
--WHERE RD.tintTotalProgress IS NULL AND RM.intDefaultReportID in (19)