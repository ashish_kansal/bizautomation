/******************************************************************
Project: Release 4.1 Date: 02.JAN.2015
Comments: 
*******************************************************************/


------------------ SANDEEP ---------------------

--TODO: Add EmailTracking and AmzonSESMaxPerSec in Web.Config of BizService
<add key="AmazonSESMaxPerSec" value="5"></add>
<add key="EmailTracking" value="http://portal.bizautomation.com/common/EmailTracking.ashx" />

--TODO: Change ECampaignWakeupTime from 1440 to 30 in Web.Config of BizService
<!--Value in Minits - Every Half Hour -->
<add key="ECampaignWakeupTime" value="30"/>


------------------------------------------------------------------------------------------------------

ALTER TABLE [dbo].[ECampaignDTLs] 
ADD tintWaitPeriod TINYINT

------------------------------------------------------------------------------------------------------

ALTER TABLE ConECampaignDTL ADD
bitEmailRead BIT,
dtExecutionDate DATETIME

------------------------------------------------------------------------------------------------------

ALTER TABLE ECampaign
ADD bitDeleted BIT

------------------------------------------------------------------------------------------------------

GO

/****** Object:  Table [dbo].[ConECampaignDTLLinks]    Script Date: 13-Dec-14 9:56:49 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ConECampaignDTLLinks](
	[numLinkID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numConECampaignDTLID] [numeric](18, 0) NOT NULL,
	[vcOriginalLink] [varchar](1000) NULL,
	[bitClicked] [bit] NULL,
 CONSTRAINT [PK_ConECampaignDTLLinks] PRIMARY KEY CLUSTERED 
(
	[numLinkID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING ON
GO

------------------------------------------------------------------------------------------------------

BEGIN TRANSACTION

DECLARE @numFieldID NUMERIC(18,0)

INSERT INTO DycFieldMaster
(
	numModuleID,
	vcFieldName,
	vcDbColumnName,
	vcOrigDbColumnName,
	vcPropertyName,
	vcLookBackTableName,
	vcFieldDataType,
	vcFieldType,
	vcAssociatedControlType,
	PopupFunctionName,
	numListID,
	bitInResults,
	bitDeleted,
	bitAllowEdit,
	bitDefault,
	bitSettingField,
	bitDetailField
)
VALUES
(
	2,
	'Next Drip / Audit',
	'vcCampaignAudit',
	'vcCampaignAudit',
	'vcCampaignAudit',
	'',
	'V',
	'R',
	'Popup',
	'openDrip',
	0,
	1,
	0,
	0,
	0,
	1,
	0
)

SET @numFieldID = SCOPE_IDENTITY()

INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (2,@numFieldID,Null,10,0,0,'Next Drip / Audit','Popup','','openDrip',1,1,2,Null,Null,Null,1,0,0,0,0,0,0,0,Null,Null,Null,0)

INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (2,@numFieldID,Null,34,0,0,'Next Drip / Audit','Popup','','openDrip',1,1,2,Null,Null,Null,1,0,0,0,0,0,0,0,Null,Null,Null,0)

COMMIT


------------------------------------------------------------------------------------------------------


--Replace following keys in appsettings in BACRMUI and BIZSERVICE
<add key="ImapLicense" value="MN800-C901FED5013E01A501FB53A6147F-B4F5" />
<add key="SMTPLicense" value="MN800-C901FED5013E01A501FB53A6147F-B4F5" />

/******************* ORDER CHANGES ******************/

ALTER TABLE dbo.SalesOrderConfiguration ADD
bitDisplaySaveNew BIT NULL DEFAULT(1),
bitDisplayAddToCart BIT NULL DEFAULT(1)

/******************* EMAIL CHANGES ******************/

--------------------------------------------------------------

ALTER TABLE InboxTreeSort ADD
numFixID NUMERIC(18,0) NULL,
bitSystem BIT NULL

--------------------------------------------------------------

ALTER TABLE EmailHistory ADD
bitInvisible BIT NULL,
numOldNodeID NUMERIC(18,0) NULL,
bitArchived BIT NULL

---------------------------------------------------------------

ALTER TABLE AlertConfig
ADD bitCampaign BIT NULL

---------------------------------------------------------------

UPDATE InboxTreeSort SET bitSystem=1 WHERE (numParentID IS NULL AND vcNodeName IS NULL) OR vcNodeName IN ('Inbox','Deleted Mails','Sent Mails','Calendar','Folders','Settings','Spam')

--------------------------------------------------------------

UPDATE InboxTreeSort SET numFixID = 1 WHERE vcNodeName = 'Inbox'
UPDATE InboxTreeSort SET numFixID = 2 WHERE vcNodeName = 'Deleted Mails'
UPDATE InboxTreeSort SET numFixID = 4 WHERE vcNodeName = 'Sent Mails'
UPDATE InboxTreeSort SET numFixID = 5 WHERE vcNodeName = 'Calendar'
UPDATE InboxTreeSort SET numFixID = 6 WHERE vcNodeName = 'Folders'
UPDATE InboxTreeSort SET numFixID = 190 WHERE vcNodeName = 'Settings'
UPDATE InboxTreeSort SET numFixID = 191 WHERE vcNodeName = 'Spam'

--------------------------------------------------------------

/** Change Label Sent Mails To Sent Messages **/
UPDATE InboxTree SET vcName = 'Sent Messages' WHERE vcName = 'Sent Mails'
UPDATE InboxTreeSort SET vcNodeName = 'Sent Messages' WHERE vcNodeName = 'Sent Mails'

--------------------------------------------------------------

/** Change Label Sent Mails To Sent Messages **/
UPDATE InboxTree SET vcName = 'Email Archive' WHERE vcName = 'Deleted Mails'
UPDATE InboxTreeSort SET vcNodeName = 'Email Archive' WHERE vcNodeName = 'Deleted Mails'

---------------------------------------------------------------

/** Change Label Folders To Custom Folders **/
UPDATE InboxTree SET vcName = 'Custom Folders' WHERE vcName = 'Folders'
UPDATE InboxTreeSort SET vcNodeName = 'Custom Folders' WHERE vcNodeName = 'Folders'

---------------------------------------------------------------

/** Remove Settings Node - Not required now **/
DELETE FROM InboxTreeSort WHERE numFixID = 190
DELETE FROM InboxTree WHERE numNodeID = 190

----------------------------------------------------------------

/** Remove Spam Node - Not required now **/
DELETE FROM InboxTreeSort WHERE numFixID = 191
DELETE FROM InboxTree WHERE numNodeID = 191

-----------------------------------------------------------------

--1.Deleted Mails
UPDATE t1
	SET numNodeId = ISNULL((SELECT numNodeId FROM InboxTreeSort WHERE numDomainID = t1.numDomainID AND numUserCntID = t1.numUserCntID AND ISNULL(numFixID,0) = 2),0)
FROM
	EmailHistory t1 
WHERE 
	t1.numNodeId = 2 AND
	t1.numNodeId <> (SELECT numNodeId FROM InboxTreeSort WHERE numDomainID = t1.numDomainID AND numUserCntID = t1.numUserCntID AND ISNULL(numFixID,0) = 2)

--2.Sent Mails
UPDATE t1
	SET numNodeId = ISNULL((SELECT numNodeId FROM InboxTreeSort WHERE numDomainID = t1.numDomainID AND numUserCntID = t1.numUserCntID AND ISNULL(numFixID,0) = 4),0)
FROM
	EmailHistory t1 
WHERE 
	t1.numNodeId = 4 AND
	t1.numNodeId <> (SELECT numNodeId FROM InboxTreeSort WHERE numDomainID = t1.numDomainID AND numUserCntID = t1.numUserCntID AND ISNULL(numFixID,0) = 4)

--3.Inbox Mails
UPDATE t1
	SET numNodeId = ISNULL((SELECT numNodeId FROM InboxTreeSort WHERE numDomainID = t1.numDomainID AND numUserCntID = t1.numUserCntID AND ISNULL(numFixID,0) = 1),0)
FROM
	EmailHistory t1 
WHERE 
	t1.numNodeId = 0 AND
	numNodeId <> (SELECT numNodeId FROM InboxTreeSort WHERE numDomainID = t1.numDomainID AND numUserCntID = t1.numUserCntID AND ISNULL(numFixID,0) = 1)


----------------------------------------------------------------------------------------------------------------

/****** Object:  Index [Division_Domain]    Script Date: 29-Dec-14 5:24:40 PM ******/
CREATE NONCLUSTERED INDEX [Division_Domain] ON [dbo].[ProjectsMaster]
(
	[numDivisionId] ASC,
	[numDomainId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

-----------------------------------------------------------------------------------------------------------------

/****** Object:  Index [Division_Domain]    Script Date: 29-Dec-14 5:25:47 PM ******/
CREATE NONCLUSTERED INDEX [Division_Domain] ON [dbo].[Communication]
(
	[numDivisionId] ASC,
	[numDomainID] ASC,
	[bitClosedFlag] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

-----------------------------------------------------------------------------------------------------------------

/****** Object:  Index [Domain_Division]    Script Date: 29-Dec-14 5:26:36 PM ******/
CREATE NONCLUSTERED INDEX [Domain_Division] ON [dbo].[OpportunityMaster]
(
	[numDivisionId] ASC,
	[numDomainId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

-----------------------------------------------------------------------------------------------------------------

/****** Object:  Index [Domain_Division_OppType_OppStatus]    Script Date: 29-Dec-14 5:27:00 PM ******/
CREATE NONCLUSTERED INDEX [Domain_Division_OppType_OppStatus] ON [dbo].[OpportunityMaster]
(
	[tintOppType] ASC,
	[numDivisionId] ASC,
	[numDomainId] ASC,
	[tintOppStatus] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

------------------------------------------------


------------------ Manish ---------------------

------/******************************************************************
------Project: BACRMUI   Date: 23.DEC.2014
------Comments: Add New Fields to Custom Report : RMA
------*******************************************************************/

BEGIN TRANSACTION

DECLARE @numMaxReportModuleID NUMERIC(18,0)
SELECT @numMaxReportModuleID = MAX([numReportModuleID]) + 1 FROM [dbo].[ReportModuleMaster]
PRINT @numMaxReportModuleID

SET IDENTITY_INSERT [dbo].[ReportModuleMaster] ON
INSERT INTO [dbo].[ReportModuleMaster] ([numReportModuleID] ,[vcModuleName], [bitActive] ) 
SELECT @numMaxReportModuleID, 'Returns', 1 
SET IDENTITY_INSERT [dbo].[ReportModuleMaster] OFF

SELECT * FROM [dbo].[ReportModuleMaster] AS RMM

--------------------------------------------
DECLARE @numReportModuleGroupID NUMERIC(18,0)
SELECT @numReportModuleGroupID = MAX([numReportModuleGroupID]) + 1 FROM [dbo].[ReportModuleGroupMaster]
PRINT @numReportModuleGroupID

SET IDENTITY_INSERT [dbo].[ReportModuleGroupMaster] ON
INSERT INTO [dbo].[ReportModuleGroupMaster]( [numReportModuleGroupID] ,[numReportModuleID] ,[vcGroupName] ,[bitActive])
SELECT @numReportModuleGroupID, @numMaxReportModuleID, 'Returns', 1
--SELECT @numReportModuleGroupID, 7, 'Returns', 1
SET IDENTITY_INSERT [dbo].[ReportModuleGroupMaster] OFF

SELECT * FROM [dbo].[ReportModuleGroupMaster] AS RMGM WHERE [RMGM].[bitActive] = 1
--------------------------------------------

DECLARE @numMaxReportFieldGroupID NUMERIC(18,0)
SELECT @numMaxReportFieldGroupID = MAX([numReportFieldGroupID]) + 1 FROM [dbo].[ReportFieldGroupMaster]
PRINT @numMaxReportFieldGroupID

SET IDENTITY_INSERT [dbo].[ReportFieldGroupMaster] ON
INSERT INTO [dbo].[ReportFieldGroupMaster] ( [numReportFieldGroupID],[vcFieldGroupName] ,[bitActive] ,[bitCustomFieldGroup] ,[numGroupID] ,[vcCustomTableName])
SELECT @numMaxReportFieldGroupID, 'Returns',1,NULL,NULL,NULL

SET IDENTITY_INSERT [dbo].[ReportFieldGroupMaster] OFF

--------------------------------------------
INSERT INTO [dbo].[ReportModuleGroupFieldMappingMaster]( [numReportModuleGroupID] ,[numReportFieldGroupID])
SELECT @numMaxReportFieldGroupID,[RMGFMM].[numReportFieldGroupID] FROM [dbo].[ReportModuleGroupFieldMappingMaster] AS RMGFMM WHERE [RMGFMM].[numReportModuleGroupID] IN (9)

--INSERT INTO [dbo].[ReportModuleGroupFieldMappingMaster]( [numReportModuleGroupID] ,[numReportFieldGroupID])
--SELECT 24,35 

SELECT * FROM [dbo].[ReportModuleGroupFieldMappingMaster] AS RMGFMM WHERE [RMGFMM].[numReportModuleGroupID] IN (@numMaxReportFieldGroupID)

--SELECT * FROM [dbo].[ReportModuleGroupFieldMappingMaster] WHERE [numReportModuleGroupID] IN (35)

SELECT * FROM [dbo].[ReturnHeader] AS RH


SET IDENTITY_INSERT [dbo].[DycFieldMaster] ON

DECLARE @numMaxFieldID NUMERIC(18,0)
SELECT @numMaxFieldID = MAX([DFM].[numFieldId]) + 1 FROM [dbo].[DycFieldMaster] AS DFM
PRINT @numMaxFieldID

INSERT INTO [dbo].[DycFieldMaster] 
( [numFieldId], [numModuleID] ,[numDomainID] ,[vcFieldName] ,[vcDbColumnName] ,[vcOrigDbColumnName] ,[vcPropertyName] ,[vcLookBackTableName] ,[vcFieldDataType] ,[vcFieldType] ,[vcAssociatedControlType] ,
[vcToolTip] ,[vcListItemType] ,[numListID] ,[PopupFunctionName] ,[order] ,[tintRow] ,[tintColumn] ,[bitInResults] ,[bitDeleted] ,[bitAllowEdit] ,[bitDefault] ,[bitSettingField] ,[bitAddField] ,
[bitDetailField] ,[bitAllowSorting] ,[bitWorkFlowField] ,[bitImport] ,[bitExport] ,[bitAllowFiltering] ,[bitInlineEdit] ,[bitRequired] ,[intColumnWidth] ,[intFieldMaxLength] ,[intWFCompare] ,
[vcGroup] ,[vcWFCompareField])
VALUES  ( @numMaxFieldID, -- numModuleID - numeric
		  39 , -- numModuleID - numeric
          NULL , -- numDomainID - numeric
          N'Return Date' , -- vcFieldName - nvarchar(50)
          N'dtCreatedDate' , -- vcDbColumnName - nvarchar(50)
          N'dtCreatedDate' , -- vcOrigDbColumnName - nvarchar(50)
          'CreatedDate' , -- vcPropertyName - varchar(100)
          N'ReturnHeader' , -- vcLookBackTableName - nvarchar(50)
          'V' , -- vcFieldDataType - char(1)
          'R' , -- vcFieldType - char(1)
          N'TextBox' , -- vcAssociatedControlType - nvarchar(50)
          NULL , -- vcToolTip - nvarchar(1000)
          '' , -- vcListItemType - varchar(3)
          0 , -- numListID - numeric
          NULL , -- PopupFunctionName - varchar(100)
          0 , -- order - tinyint
          NULL , -- tintRow - tinyint
          NULL, -- tintColumn - tinyint
          0 , -- bitInResults - bit
          0 , -- bitDeleted - bit
          0 , -- bitAllowEdit - bit
          0 , -- bitDefault - bit
          0 , -- bitSettingField - bit
          0 , -- bitAddField - bit
          0 , -- bitDetailField - bit
          1 , -- bitAllowSorting - bit
          0 , -- bitWorkFlowField - bit
          0 , -- bitImport - bit
          0 , -- bitExport - bit
          1 , -- bitAllowFiltering - bit
          0 , -- bitInlineEdit - bit
          0 , -- bitRequired - bit
          0 , -- intColumnWidth - int
          0 , -- intFieldMaxLength - int
          0 , -- intWFCompare - int
          NULL , -- vcGroup - varchar(100)
          NULL  -- vcWFCompareField - varchar(150)
        )

SET IDENTITY_INSERT [dbo].[DycFieldMaster] OFF

INSERT INTO [dbo].[ReportFieldGroupMappingMaster]([numReportFieldGroupID], [numFieldID], [vcFieldName], 
[vcAssociatedControlType], [vcFieldDataType], [bitAllowSorting], [bitAllowGrouping], [bitAllowAggregate], [bitAllowFiltering])
SELECT 35, numFieldId, vcFieldName,vcAssociatedControlType, 'D', 1, 0, 0, 0 FROM DycFieldMaster 
WHERE numModuleID=39 AND vcLookBackTableName='ReturnHeader' AND [DycFieldMaster].[numFieldId] = @numMaxFieldID

--INSERT INTO [dbo].[ReportFieldGroupMappingMaster]([numReportFieldGroupID], [numFieldID], [vcFieldName], 
--[vcAssociatedControlType], [vcFieldDataType], [bitAllowSorting], [bitAllowGrouping], [bitAllowAggregate], [bitAllowFiltering])
--SELECT 5, numFieldId, vcFieldName, vcDbColumnName, 'D', 1, 1, 0, 1 FROM DycFieldMaster 
--WHERE numModuleID=39 AND vcLookBackTableName='ReturnHeader' AND [DycFieldMaster].[numFieldId] = 10695
----------------------------
---------------------------------------------

SET IDENTITY_INSERT [dbo].[DycFieldMaster] ON

DECLARE @numMaxFieldID NUMERIC(18,0)
SELECT @numMaxFieldID = MAX([DFM].[numFieldId]) + 1 FROM [dbo].[DycFieldMaster] AS DFM
PRINT @numMaxFieldID

INSERT INTO [dbo].[DycFieldMaster] 
( [numFieldId], [numModuleID] ,[numDomainID] ,[vcFieldName] ,[vcDbColumnName] ,[vcOrigDbColumnName] ,[vcPropertyName] ,[vcLookBackTableName] ,[vcFieldDataType] ,[vcFieldType] ,[vcAssociatedControlType] ,
[vcToolTip] ,[vcListItemType] ,[numListID] ,[PopupFunctionName] ,[order] ,[tintRow] ,[tintColumn] ,[bitInResults] ,[bitDeleted] ,[bitAllowEdit] ,[bitDefault] ,[bitSettingField] ,[bitAddField] ,
[bitDetailField] ,[bitAllowSorting] ,[bitWorkFlowField] ,[bitImport] ,[bitExport] ,[bitAllowFiltering] ,[bitInlineEdit] ,[bitRequired] ,[intColumnWidth] ,[intFieldMaxLength] ,[intWFCompare] ,
[vcGroup] ,[vcWFCompareField])
VALUES  ( @numMaxFieldID, -- numModuleID - numeric
		  39 , -- numModuleID - numeric
          NULL , -- numDomainID - numeric
          N'RMA' , -- vcFieldName - nvarchar(50)
          N'vcRMA' , -- vcDbColumnName - nvarchar(50)
          N'vcRMA' , -- vcOrigDbColumnName - nvarchar(50)
          'RMA' , -- vcPropertyName - varchar(100)
          N'ReturnHeader' , -- vcLookBackTableName - nvarchar(50)
          'V' , -- vcFieldDataType - char(1)
          'R' , -- vcFieldType - char(1)
          N'TextBox' , -- vcAssociatedControlType - nvarchar(50)
          NULL , -- vcToolTip - nvarchar(1000)
          '' , -- vcListItemType - varchar(3)
          0 , -- numListID - numeric
          NULL , -- PopupFunctionName - varchar(100)
          0 , -- order - tinyint
          NULL , -- tintRow - tinyint
          NULL, -- tintColumn - tinyint
          0 , -- bitInResults - bit
          0 , -- bitDeleted - bit
          0 , -- bitAllowEdit - bit
          0 , -- bitDefault - bit
          0 , -- bitSettingField - bit
          0 , -- bitAddField - bit
          0 , -- bitDetailField - bit
          1 , -- bitAllowSorting - bit
          0 , -- bitWorkFlowField - bit
          0 , -- bitImport - bit
          0 , -- bitExport - bit
          1 , -- bitAllowFiltering - bit
          0 , -- bitInlineEdit - bit
          0 , -- bitRequired - bit
          0 , -- intColumnWidth - int
          0 , -- intFieldMaxLength - int
          0 , -- intWFCompare - int
          NULL , -- vcGroup - varchar(100)
          NULL  -- vcWFCompareField - varchar(150)
        )

SET IDENTITY_INSERT [dbo].[DycFieldMaster] OFF

INSERT INTO [dbo].[ReportFieldGroupMappingMaster]([numReportFieldGroupID], [numFieldID], [vcFieldName], 
[vcAssociatedControlType], [vcFieldDataType], [bitAllowSorting], [bitAllowGrouping], [bitAllowAggregate], [bitAllowFiltering])
SELECT 35, numFieldId, vcFieldName,vcAssociatedControlType, 'D', 1, 1, 0, 1 FROM DycFieldMaster 
WHERE numModuleID=39 AND vcLookBackTableName='ReturnHeader' AND [DycFieldMaster].[numFieldId] = @numMaxFieldID
-----------------------

SET IDENTITY_INSERT [dbo].[DycFieldMaster] ON

DECLARE @numMaxFieldID NUMERIC(18,0)
SELECT @numMaxFieldID = MAX([DFM].[numFieldId]) + 1 FROM [dbo].[DycFieldMaster] AS DFM
PRINT @numMaxFieldID

INSERT INTO [dbo].[DycFieldMaster] 
( [numFieldId], [numModuleID] ,[numDomainID] ,[vcFieldName] ,[vcDbColumnName] ,[vcOrigDbColumnName] ,[vcPropertyName] ,[vcLookBackTableName] ,[vcFieldDataType] ,[vcFieldType] ,[vcAssociatedControlType] ,
[vcToolTip] ,[vcListItemType] ,[numListID] ,[PopupFunctionName] ,[order] ,[tintRow] ,[tintColumn] ,[bitInResults] ,[bitDeleted] ,[bitAllowEdit] ,[bitDefault] ,[bitSettingField] ,[bitAddField] ,
[bitDetailField] ,[bitAllowSorting] ,[bitWorkFlowField] ,[bitImport] ,[bitExport] ,[bitAllowFiltering] ,[bitInlineEdit] ,[bitRequired] ,[intColumnWidth] ,[intFieldMaxLength] ,[intWFCompare] ,
[vcGroup] ,[vcWFCompareField])
VALUES  ( @numMaxFieldID, -- numModuleID - numeric
		  39 , -- numModuleID - numeric
          NULL , -- numDomainID - numeric
          N'Bizdoc Name' , -- vcFieldName - nvarchar(50)
          N'vcBizDocName' , -- vcDbColumnName - nvarchar(50)
          N'vcBizDocName' , -- vcOrigDbColumnName - nvarchar(50)
          'BizdocName' , -- vcPropertyName - varchar(100)
          N'ReturnHeader' , -- vcLookBackTableName - nvarchar(50)
          'V' , -- vcFieldDataType - char(1)
          'R' , -- vcFieldType - char(1)
          N'TextBox' , -- vcAssociatedControlType - nvarchar(50)
          NULL , -- vcToolTip - nvarchar(1000)
          '' , -- vcListItemType - varchar(3)
          0 , -- numListID - numeric
          NULL , -- PopupFunctionName - varchar(100)
          0 , -- order - tinyint
          NULL , -- tintRow - tinyint
          NULL, -- tintColumn - tinyint
          0 , -- bitInResults - bit
          0 , -- bitDeleted - bit
          0 , -- bitAllowEdit - bit
          0 , -- bitDefault - bit
          0 , -- bitSettingField - bit
          0 , -- bitAddField - bit
          0 , -- bitDetailField - bit
          1 , -- bitAllowSorting - bit
          0 , -- bitWorkFlowField - bit
          0 , -- bitImport - bit
          0 , -- bitExport - bit
          1 , -- bitAllowFiltering - bit
          0 , -- bitInlineEdit - bit
          0 , -- bitRequired - bit
          0 , -- intColumnWidth - int
          0 , -- intFieldMaxLength - int
          0 , -- intWFCompare - int
          NULL , -- vcGroup - varchar(100)
          NULL  -- vcWFCompareField - varchar(150)
        )

SET IDENTITY_INSERT [dbo].[DycFieldMaster] OFF

INSERT INTO [dbo].[ReportFieldGroupMappingMaster]([numReportFieldGroupID], [numFieldID], [vcFieldName], 
[vcAssociatedControlType], [vcFieldDataType], [bitAllowSorting], [bitAllowGrouping], [bitAllowAggregate], [bitAllowFiltering])
SELECT 35, numFieldId, vcFieldName,vcAssociatedControlType, 'D', 1, 1, 0, 1 FROM DycFieldMaster 
WHERE numModuleID=39 AND vcLookBackTableName='ReturnHeader' AND [DycFieldMaster].[numFieldId] = @numMaxFieldID

---------------------------------------

SET IDENTITY_INSERT [dbo].[DycFieldMaster] ON

DECLARE @numMaxFieldID NUMERIC(18,0)
SELECT @numMaxFieldID = MAX([DFM].[numFieldId]) + 1 FROM [dbo].[DycFieldMaster] AS DFM
PRINT @numMaxFieldID

INSERT INTO [dbo].[DycFieldMaster] 
( [numFieldId], [numModuleID] ,[numDomainID] ,[vcFieldName] ,[vcDbColumnName] ,[vcOrigDbColumnName] ,[vcPropertyName] ,[vcLookBackTableName] ,[vcFieldDataType] ,[vcFieldType] ,[vcAssociatedControlType] ,
[vcToolTip] ,[vcListItemType] ,[numListID] ,[PopupFunctionName] ,[order] ,[tintRow] ,[tintColumn] ,[bitInResults] ,[bitDeleted] ,[bitAllowEdit] ,[bitDefault] ,[bitSettingField] ,[bitAddField] ,
[bitDetailField] ,[bitAllowSorting] ,[bitWorkFlowField] ,[bitImport] ,[bitExport] ,[bitAllowFiltering] ,[bitInlineEdit] ,[bitRequired] ,[intColumnWidth] ,[intFieldMaxLength] ,[intWFCompare] ,
[vcGroup] ,[vcWFCompareField])
VALUES  ( @numMaxFieldID, -- numModuleID - numeric
		  39 , -- numModuleID - numeric
          NULL , -- numDomainID - numeric
          N'Return Amount' , -- vcFieldName - nvarchar(50)
          N'monAmount' , -- vcDbColumnName - nvarchar(50)
          N'monAmount' , -- vcOrigDbColumnName - nvarchar(50)
          'Amount' , -- vcPropertyName - varchar(100)
          N'ReturnHeader' , -- vcLookBackTableName - nvarchar(50)
          'N' , -- vcFieldDataType - char(1)
          'R' , -- vcFieldType - char(1)
          N'TextBox' , -- vcAssociatedControlType - nvarchar(50)
          NULL , -- vcToolTip - nvarchar(1000)
          '' , -- vcListItemType - varchar(3)
          0 , -- numListID - numeric
          NULL , -- PopupFunctionName - varchar(100)
          0 , -- order - tinyint
          NULL , -- tintRow - tinyint
          NULL, -- tintColumn - tinyint
          0 , -- bitInResults - bit
          0 , -- bitDeleted - bit
          0 , -- bitAllowEdit - bit
          0 , -- bitDefault - bit
          0 , -- bitSettingField - bit
          0 , -- bitAddField - bit
          0 , -- bitDetailField - bit
          1 , -- bitAllowSorting - bit
          0 , -- bitWorkFlowField - bit
          0 , -- bitImport - bit
          0 , -- bitExport - bit
          1 , -- bitAllowFiltering - bit
          0 , -- bitInlineEdit - bit
          0 , -- bitRequired - bit
          0 , -- intColumnWidth - int
          0 , -- intFieldMaxLength - int
          0 , -- intWFCompare - int
          NULL , -- vcGroup - varchar(100)
          NULL  -- vcWFCompareField - varchar(150)
        )

SET IDENTITY_INSERT [dbo].[DycFieldMaster] OFF

INSERT INTO [dbo].[ReportFieldGroupMappingMaster]([numReportFieldGroupID], [numFieldID], [vcFieldName], 
[vcAssociatedControlType], [vcFieldDataType], [bitAllowSorting], [bitAllowGrouping], [bitAllowAggregate], [bitAllowFiltering])
SELECT 35, numFieldId, vcFieldName,vcAssociatedControlType, 'D', 1, 1, 0, 1 FROM DycFieldMaster 
WHERE numModuleID=39 AND vcLookBackTableName='ReturnHeader' AND [DycFieldMaster].[numFieldId] = @numMaxFieldID

------------------------------

SET IDENTITY_INSERT [dbo].[DycFieldMaster] ON

DECLARE @numMaxFieldID NUMERIC(18,0)
SELECT @numMaxFieldID = MAX([DFM].[numFieldId]) + 1 FROM [dbo].[DycFieldMaster] AS DFM
PRINT @numMaxFieldID

INSERT INTO [dbo].[DycFieldMaster] 
( [numFieldId], [numModuleID] ,[numDomainID] ,[vcFieldName] ,[vcDbColumnName] ,[vcOrigDbColumnName] ,[vcPropertyName] ,[vcLookBackTableName] ,[vcFieldDataType] ,[vcFieldType] ,[vcAssociatedControlType] ,
[vcToolTip] ,[vcListItemType] ,[numListID] ,[PopupFunctionName] ,[order] ,[tintRow] ,[tintColumn] ,[bitInResults] ,[bitDeleted] ,[bitAllowEdit] ,[bitDefault] ,[bitSettingField] ,[bitAddField] ,
[bitDetailField] ,[bitAllowSorting] ,[bitWorkFlowField] ,[bitImport] ,[bitExport] ,[bitAllowFiltering] ,[bitInlineEdit] ,[bitRequired] ,[intColumnWidth] ,[intFieldMaxLength] ,[intWFCompare] ,
[vcGroup] ,[vcWFCompareField])
VALUES  ( @numMaxFieldID, -- numModuleID - numeric
		  39 , -- numModuleID - numeric
          NULL , -- numDomainID - numeric
          N'Comments' , -- vcFieldName - nvarchar(50)
          N'vcComments' , -- vcDbColumnName - nvarchar(50)
          N'vcComments' , -- vcOrigDbColumnName - nvarchar(50)
          'Comments' , -- vcPropertyName - varchar(100)
          N'ReturnHeader' , -- vcLookBackTableName - nvarchar(50)
          'V' , -- vcFieldDataType - char(1)
          'R' , -- vcFieldType - char(1)
          N'TextBox' , -- vcAssociatedControlType - nvarchar(50)
          NULL , -- vcToolTip - nvarchar(1000)
          '' , -- vcListItemType - varchar(3)
          0 , -- numListID - numeric
          NULL , -- PopupFunctionName - varchar(100)
          0 , -- order - tinyint
          NULL , -- tintRow - tinyint
          NULL, -- tintColumn - tinyint
          0 , -- bitInResults - bit
          0 , -- bitDeleted - bit
          0 , -- bitAllowEdit - bit
          0 , -- bitDefault - bit
          0 , -- bitSettingField - bit
          0 , -- bitAddField - bit
          0 , -- bitDetailField - bit
          1 , -- bitAllowSorting - bit
          0 , -- bitWorkFlowField - bit
          0 , -- bitImport - bit
          0 , -- bitExport - bit
          1 , -- bitAllowFiltering - bit
          0 , -- bitInlineEdit - bit
          0 , -- bitRequired - bit
          0 , -- intColumnWidth - int
          0 , -- intFieldMaxLength - int
          0 , -- intWFCompare - int
          NULL , -- vcGroup - varchar(100)
          NULL  -- vcWFCompareField - varchar(150)
        )

SET IDENTITY_INSERT [dbo].[DycFieldMaster] OFF

INSERT INTO [dbo].[ReportFieldGroupMappingMaster]([numReportFieldGroupID], [numFieldID], [vcFieldName], 
[vcAssociatedControlType], [vcFieldDataType], [bitAllowSorting], [bitAllowGrouping], [bitAllowAggregate], [bitAllowFiltering])
SELECT 35, numFieldId, vcFieldName,vcAssociatedControlType, 'D', 1, 1, 0, 1 FROM DycFieldMaster 
WHERE numModuleID=39 AND vcLookBackTableName='ReturnHeader' AND [DycFieldMaster].[numFieldId] = @numMaxFieldID

---------------------------------
ROLLBACK

------/******************************************************************
------Project: BACRMUI   Date: 9.DEC.2014
------Comments: Add New Fields to Similar Items for E-Commerece Related Item settings
------*******************************************************************/

ALTER TABLE [dbo].[SimilarItems] ADD bitPreUpSell BIT, bitPostUpSell BIT, vcUpSellDesc VARCHAR(1000)

UPDATE [dbo].[PageNavigationDTL] SET [vcPageNavName] = 'Commission Rules' WHERE [vcPageNavName]  = 'Commission Rule'
--=======================================================================


------/******************************************************************
------Project: BACRMUI   Date: 4.DEC.2014
------Comments: Add Item Custom Fields to Opportunity Items Module within custom reports
------*******************************************************************/

BEGIN TRANSACTION

INSERT INTO [dbo].[ReportFieldGroupMaster]( [vcFieldGroupName] ,[bitActive] ,[bitCustomFieldGroup] ,[numGroupID] ,[vcCustomTableName])
SELECT 'Items Custom Field',1,	1,	5,	'CFW_FLD_Values_Item'

INSERT INTO [dbo].[ReportModuleGroupFieldMappingMaster]( [numReportModuleGroupID] ,[numReportFieldGroupID])
SELECT 10,(SELECT MAX([numReportFieldGroupID]) FROM ReportFieldGroupMaster)

SELECT * FROM ReportModuleGroupFieldMappingMaster 
JOIN ReportFieldGroupMaster ON [ReportModuleGroupFieldMappingMaster].[numReportFieldGroupID] = [ReportFieldGroupMaster].numReportFieldGroupID
WHERE [ReportModuleGroupFieldMappingMaster].[numReportModuleGroupID] IN (10)

ROLLBACK
--=======================================================================


------/******************************************************************
------Project: BACRMUI   Date: 2.DEC.2014
------Comments: Add new columns to E-Commerce Details
------*******************************************************************/

ALTER TABLE [dbo].[eCommerceDTL] ADD bitPreSellUp BIT, bitPostSellUp BIT, dcPostSellDiscount DECIMAL

--=======================================================================



------------------------------------------------