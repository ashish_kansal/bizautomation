/******************************************************************
Project: Release 8.6 Date: 11.DECEMBER.2017
Comments: STORE PROCEDURES
*******************************************************************/


GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='CheckChildRecord')
DROP FUNCTION CheckChildRecord
GO
CREATE FUNCTION CheckChildRecord
(
	@numOppID AS NUMERIC(9)  = NULL,
    @numDomainID AS NUMERIC(9)  = 0
)
RETURNS NVARCHAR(MAX)
AS BEGIN
	
	DECLARE @ChildCount INT, @RecurringChildCount INT, @TotalCount INT

	SELECT @ChildCount = COUNT(distinct numChildOppID) 
		FROM [OpportunityLinking] OL 
			JOIN OpportunityMaster Opp  ON OL.[numParentOppID] = Opp.numOppId
		WHERE  
		Opp.numOppId = @numOppID
			AND 
			Opp.numDomainID = @numDomainID

	SELECT @RecurringChildCount = COUNT(*) 
		FROM [RecurringTransactionReport] RTR 
			JOIN OpportunityMaster Opp  ON RTR.[numRecTranSeedId] = Opp.numOppId
		WHERE  
		Opp.numOppId = @numOppID
			AND 
			Opp.numDomainID = @numDomainID

	SET @TotalCount = @ChildCount + @RecurringChildCount

	DECLARE @vcPoppName AS VARCHAR(MAX);	

	SELECT @vcPoppName = Opp.vcPoppName 
			FROM OpportunityMaster Opp         
			WHERE  
			Opp.numOppId = @numOppID
				AND 
				Opp.numDomainID = @numDomainID

	IF @TotalCount > 1
	BEGIN
		SET @vcPoppName =  CONCAT(@vcPoppName,' <a href=''#'' id=''anchChild'' onclick=''openChild(event,',@numOppID,')''><img src=''../images/GLReport.png'' style=''width: 20px; height: 20px;'' /></a>')
	END
	
    RETURN ISNULL(@vcPoppName,'')
END

GO


GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetCustFldValueOppItems')
DROP FUNCTION GetCustFldValueOppItems
GO
CREATE FUNCTION GetCustFldValueOppItems
(
 @numFldId NUMERIC(18,0),
 @numRecordId NUMERIC(18,0),
 @numItemCode NUMERIC(18,0)
)
RETURNS VARCHAR(MAX) 
AS
BEGIN
 DECLARE @vcValue AS VARCHAR(MAX) = ''
 DECLARE @fld_Type AS VARCHAR(50)
 DECLARE @numListID AS NUMERIC(18,0)

    SELECT 
  @numListID=numListID,
  @fld_Type=Fld_type 
 FROM 
  CFW_Fld_Master 
 WHERE 
  Fld_id=@numFldId
       
 IF EXISTS (SELECT Fld_Value FROM CFW_Fld_Values_OppItems WHERE Fld_ID = @numFldId  AND RecId = @numRecordId)
 BEGIN
  SELECT 
     @vcValue = ISNULL(Fld_Value,0) 
  FROM 
     CFW_Fld_Values_OppItems
  WHERE 
     Fld_ID = @numFldId 
     AND RecId = @numRecordId
 END
 ELSE
 BEGIN
  SELECT 
   @vcValue = ISNULL(Fld_Value,0) 
  FROM 
   CFW_Fld_Values_Item 
  WHERE 
   Fld_ID = @numFldId 
   AND RecId = @numItemCode
 END
    IF (@fld_Type='TextBox' OR @fld_Type='TextArea')
    BEGIN
  IF @vcValue='' SET @vcValue='-'
    END
    else if @fld_Type='SelectBox'
    BEGIN
  IF @vcValue='' 
   SET @vcValue='-'
  ELSE 
      SET @vcValue = dbo.GetListIemName(@vcValue)
 END 
    ELSE IF @fld_Type = 'CheckBox' 
 BEGIN
  SET @vcValue= (CASE WHEN @vcValue=0 THEN 'No' ELSE 'Yes' END)
 END
 ELSE IF @fld_type = 'CheckBoxList'
 BEGIN
  SELECT 
   @vcValue = STUFF((SELECT CONCAT(',', vcData) 
  FROM 
   ListDetails 
  WHERE 
   numlistitemid IN (SELECT ISNULL(Id,0) FROM dbo.SplitIDs(@vcValue,',')) FOR XML PATH('')), 1, 1, '')
 END

 IF @vcValue IS NULL SET @vcValue=''

 RETURN @vcValue
END


GO
/****** Object:  StoredProcedure [dbo].[USP_cfwGetFields]    Script Date: 07/26/2008 16:15:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_cfwgetfields')
DROP PROCEDURE usp_cfwgetfields
GO
CREATE PROCEDURE [dbo].[USP_cfwGetFields]                              
@PageId as tinyint,                              
@numRelation as numeric(9),                              
@numRecordId as numeric(9),                  
@numDomainID as numeric(9)                              
as            

                  
if @PageId= 1 
begin                              
select CFW_Fld_Master.fld_id,fld_type,fld_label,numlistid,numOrder,Rec.FldDTLID,isnull(Rec.Fld_Value,0) as Value,subgrp as TabId,Grp_Name as tabname,vcURL,CFW_Fld_Master.vcToolTip from CFW_Fld_Master join CFW_Fld_Dtl                              
on Fld_id=numFieldId                              
left join CFw_Grp_Master                               
on subgrp=CFw_Grp_Master.Grp_id 
left join dbo.GetCustomFieldDTLIDAndValues(@PageId,@numRecordId) Rec
on Rec.Fld_ID=CFW_Fld_Master.fld_id                             
where CFW_Fld_Master.grp_id=@PageId and numRelation=@numRelation and CFW_Fld_Master.numDomainID=@numDomainID                          
         
union        
select 0 as fld_id,'Frame' as fld_type,'' as fld_label,0 as numlistid,0,0,'' as Value,Grp_id as TabId,Grp_Name as tabname,vcURLF,Null as vcToolTip from CFw_Grp_Master        
where tintType=1 and loc_Id = @PageId   and numDomainID=@numDomainID           
order by subgrp,numOrder                             
end                              

---select Shared Custom fields and tab specific customfields -- where Pageid=1 stands for shared custom fields among accounts,leads,prospects
IF  @PageId= 12 or  @PageId= 13 or  @PageId= 14
BEGIN
	select CFW_Fld_Master.fld_id,fld_type,fld_label,numlistid,numOrder,Rec.FldDTLID,isnull(Rec.Fld_Value,0) as Value,subgrp as TabId,Grp_Name as tabname,vcURL,CFW_Fld_Master.vcToolTip from CFW_Fld_Master join CFW_Fld_Dtl                              
	on Fld_id=numFieldId                              
	left join CFw_Grp_Master                               
	on subgrp=CFw_Grp_Master.Grp_id 
	left join dbo.GetCustomFieldDTLIDAndValues(@PageId,@numRecordId) Rec
	on Rec.Fld_ID=CFW_Fld_Master.fld_id                             
	where CFW_Fld_Master.grp_id=@PageId and numRelation=@numRelation and CFW_Fld_Master.numDomainID=@numDomainID                          
	
	UNION 
	
	select CFW_Fld_Master.fld_id,fld_type,fld_label,numlistid,numOrder,Rec.FldDTLID,isnull(Rec.Fld_Value,0) as Value,subgrp as TabId,Grp_Name as tabname,vcURL,CFW_Fld_Master.vcToolTip from CFW_Fld_Master join CFW_Fld_Dtl                              
	on Fld_id=numFieldId                              
	left join CFw_Grp_Master                               
	on subgrp=CFw_Grp_Master.Grp_id 
	left join dbo.GetCustomFieldDTLIDAndValues(1,@numRecordId) Rec
	on Rec.Fld_ID=CFW_Fld_Master.fld_id                             
	where CFW_Fld_Master.grp_id=1 and numRelation=@numRelation and CFW_Fld_Master.numDomainID=@numDomainID
	         
	union        
	select 0 as fld_id,'Frame' as fld_type,'' as fld_label,0 as numlistid,0,0,'' as Value,Grp_id as TabId,Grp_Name as tabname,vcURLF,NUll as vcToolTip from CFw_Grp_Master        
	where tintType=1 and loc_Id = @PageId   and numDomainID=@numDomainID           
	order by subgrp,numOrder                             
	
END


                              
if @PageId= 4                              
begin       
---------------------Very Important----------------------    
-------Who wrote this part    
---------------------------------------------------------    
    
    
--declare @ContactId as numeric(9)                              
---select @ContactId=numContactId from cases where numCaseid=@numRecordId                              
--select @numRelation=numCompanyType from AdditionalContactsInformation AddC                              
---join DivisionMaster Div on AddC.numDivisionId=Div.numDivisionId                              
---join CompanyInfo Com on  Com.numCompanyId=Div.numCompanyId                              
---where AddC.numContactID=@ContactId                              
                              
select CFW_Fld_Master.fld_id,fld_type,fld_label,numlistid,numOrder,
Rec.FldDTLID,isnull(Rec.Fld_Value,0) as Value,
subgrp as TabId,Grp_Name as tabname,vcURL,CFW_Fld_Master.vcToolTip from CFW_Fld_Master join CFW_Fld_Dtl                              
on Fld_id=numFieldId                              
left join CFw_Grp_Master                               
on subgrp=CFw_Grp_Master.Grp_id  
left join dbo.GetCustomFieldDTLIDAndValues(@PageId,@numRecordId) Rec
on Rec.Fld_ID=CFW_Fld_Master.fld_id                             
where CFW_Fld_Master.grp_id=@PageId and numRelation=@numRelation and CFW_Fld_Master.numDomainID=@numDomainID                             
--select @numRelation=numContactType from AdditionalContactsInformation AddC                              
--join DivisionMaster Div on AddC.numDivisionId=Div.numDivisionId                              
--join CompanyInfo Com on  Com.numCompanyId=Div.numCompanyId                              
--where AddC.numContactID=@numRecordId                              
                              
union        
select 0 as fld_id,'Frame' as fld_type,'' as fld_label,0 as numlistid,0,0,'' as Value,
Grp_id as TabId,Grp_Name as tabname,vcURLF,NUll as vcToolTip from CFw_Grp_Master        
where tintType=1 and loc_Id = @PageId   and numDomainID=@numDomainID                              
   order by subgrp,numOrder                           
end                              
                              
if @PageId= 2 or  @PageId= 3 or @PageId= 5 or @PageId= 6 or @PageId= 7 or @PageId= 8     or @PageId= 11   or @PageId = 17                
begin                              
select CFW_Fld_Master.fld_id,fld_type,fld_label,numlistid,ISNULL(Rec.FldDTLID,0) FldDTLID,
CASE WHEN @PageId = 5 THEN (CASE WHEN ISNULL(Rec.Fld_Value, '') = '' AND fld_type = 'Checkbox' THEN '0' ELSE ISNULL(Rec.Fld_Value, 0) END) ELSE ISNULL(Rec.Fld_Value,0) END  AS Value,
subgrp as TabId,Grp_Name as tabname,vcURL,CFW_Fld_Master.vcToolTip  from CFW_Fld_Master                               
left join CFw_Grp_Master                               
on subgrp=CFw_Grp_Master.Grp_id  
left join dbo.GetCustomFieldDTLIDAndValues(@PageId,@numRecordId) Rec
on Rec.Fld_ID=CFW_Fld_Master.fld_id                             
where CFW_Fld_Master.grp_id=@PageId and CFW_Fld_Master.numDomainID=@numDomainID         
union        
select 0 as fld_id,'Frame' as fld_type,'' as fld_label,0 as numlistid,0 as FldDTLID,'0' as Value,Grp_id as TabId,Grp_Name as tabname,vcURLF,NULL as vcToolTip from CFw_Grp_Master        
where tintType=1 and loc_Id = @PageId  and numDomainID=@numDomainID      
end

select GRp_Id as TabID,Grp_Name AS TabName from CFw_Grp_Master where 
(Loc_ID=@PageId OR 1=(CASE WHEN @PageId IN (12,13,14) THEN CASE WHEN Loc_Id=1 THEN 1 ELSE 0 END ELSE 0 END))
 and numDomainID=@numDomainID
/*tintType stands for static tabs */ AND tintType<>2
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_cfwgetfieldsoppitems')
DROP PROCEDURE dbo.usp_cfwgetfieldsoppitems
GO
CREATE PROCEDURE [dbo].[usp_cfwgetfieldsoppitems]
 @numDomainID as numeric(18,0),                       
	@numOppItemId as numeric(18,0),                  
    @numItemCode AS NUMERIC(18,0)                        
AS
BEGIN
	IF EXISTS (SELECT * FROM CFW_Fld_Values_OppItems WHERE RecId=@numOppItemId)
	BEGIN
		SELECT 
			CFW_Fld_Master.fld_id
			,fld_type,fld_label
			,numlistid
			,ISNULL(CFW_Fld_Values_OppItems.FldDTLID,0) FldDTLID
			,(CASE WHEN (ISNULL(CFW_Fld_Values_OppItems.Fld_Value, '') = '' OR UPPER(ISNULL(CFW_Fld_Values_OppItems.Fld_Value, '')) = 'NO' OR UPPER(ISNULL(CFW_Fld_Values_OppItems.Fld_Value, '')) = 'FALSE') AND fld_type = 'Checkbox' THEN '0' ELSE ISNULL(CFW_Fld_Values_OppItems.Fld_Value, 0) END) AS Value
			,subgrp as TabId
			,Grp_Name as tabname
			,vcURL
			,CFW_Fld_Master.vcToolTip  
		FROM 
			CFW_Fld_Master                               
		LEFT JOIN 
			CFw_Grp_Master                               
		ON 
			subgrp=CFw_Grp_Master.Grp_id  
        LEFT JOIN
			CFW_Fld_Values_OppItems
		ON
			CFW_Fld_Master.Fld_id = CFW_Fld_Values_OppItems.Fld_ID
			AND CFW_Fld_Values_OppItems.RecId=@numOppItemId
		WHERE 
			CFW_Fld_Master.grp_id=5 
			and CFW_Fld_Master.numDomainID=@numDomainID 
	END
	ELSE
	BEGIN
		SELECT 
			CFW_Fld_Master.fld_id
			,fld_type,fld_label
			,numlistid
			,ISNULL(CFW_Fld_Values_Item.FldDTLID,0) FldDTLID
			,(CASE WHEN (ISNULL(CFW_Fld_Values_Item.Fld_Value, '') = '' OR UPPER(ISNULL(CFW_Fld_Values_Item.Fld_Value, '')) = 'NO' OR UPPER(ISNULL(CFW_Fld_Values_Item.Fld_Value, '')) = 'FALSE') AND fld_type = 'Checkbox' THEN '0' ELSE ISNULL(CFW_Fld_Values_Item.Fld_Value, 0) END) AS Value
			,subgrp as TabId
			,Grp_Name as tabname
			,vcURL
			,CFW_Fld_Master.vcToolTip  
		FROM 
			CFW_Fld_Master                               
		LEFT JOIN 
			CFw_Grp_Master                               
		ON 
			subgrp=CFw_Grp_Master.Grp_id  
        LEFT JOIN
			CFW_Fld_Values_Item
		ON
			CFW_Fld_Master.Fld_id = CFW_Fld_Values_Item.Fld_ID
			AND CFW_Fld_Values_Item.RecId=@numItemCode
		WHERE 
			CFW_Fld_Master.grp_id=5 
			and CFW_Fld_Master.numDomainID=@numDomainID 
	END
END
/****** Object:  StoredProcedure [dbo].[USP_cfwSaveCusfld]    Script Date: 07/26/2008 16:15:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_cfwsavecusfld')
DROP PROCEDURE usp_cfwsavecusfld
GO
CREATE PROCEDURE [dbo].[USP_cfwSaveCusfld]          
          
@RecordId as numeric(9)=null,          
@strDetails as text='',          
@PageId as tinyint=null          
AS 
BEGIN        
	IF CHARINDEX('<?xml',@strDetails) = 0
	BEGIN
		SET @strDetails = CONCAT('<?xml version="1.0" encoding="ISO-8859-1"?>',@strDetails)
	END

 
declare @hDoc int          
          
        
          
EXEC sp_xml_preparedocument @hDoc OUTPUT, @strDetails          
          
if @PageId=1          
begin          

Delete from CFW_FLD_Values where FldDTLID not in (SELECT FldDTLID FROM OPENXML (@hDoc,'/NewDataSet/Table[FldDTLID>0]',2)          
WITH (fld_id numeric(9),FldDTLID numeric(9),Value varchar(1000))) 
and  RecId= @RecordId      


insert into CFW_FLD_Values (Fld_ID,Fld_Value,RecId)          
select X.fld_id,X.Value,@RecordId  from(SELECT *FROM OPENXML (@hDoc,'/NewDataSet/Table[FldDTLID=0]',2)          
WITH (fld_id numeric(9),FldDTLID numeric(9),Value varchar(1000)))X 


update CFW_FLD_Values set Fld_Value=X.Value  from      
(SELECT * FROM OPENXML (@hDoc,'/NewDataSet/Table[FldDTLID>0]',2)          
WITH (fld_id numeric(9),FldDTLID numeric(9),Value varchar(1000)))X
where CFW_FLD_Values.FldDTLID=X.FldDTLID


--Author :Sachin Sadhu ||Date:13thOctober2014
--Purpose:To make Entry in WorkFLow queue(Identify rules based on Custom fields)

DECLARE @CFFields as table (  
  RowID INT IDENTITY(1, 1),
  fld_id numeric(9)
  )
INSERT INTO @CFFields(fld_id) 
SELECT Fld_ID from CFW_FLD_Values where RecId=@RecordId
--select X.fld_id  from(SELECT *FROM OPENXML (@hDoc,'/NewDataSet/Table[FldDTLID>0]',2)          
--WITH (fld_id numeric(9),FldDTLID numeric(9),Value varchar(1000)))X 

DECLARE @intCnt AS INT
DECLARE @intCntFields AS INT
Declare @numFieldID As numeric(9,0)
Declare @numDomainID as numeric(18,0)
Declare @numUserID as numeric(18,0)
SET @intCnt = 0
SET @intCntFields = (SELECT COUNT(*) FROM @CFFields)
SELECT @numDomainID=numDomainID,@numUserID=numCreatedBy from DivisionMaster where numDivisionID=@RecordId

IF @intCntFields > 0
	BEGIN
		WHILE(@intCnt < @intCntFields)
			BEGIN
			--print 'sahc'
				SET @intCnt = @intCnt + 1
				SELECT @numFieldID = fld_id FROM @CFFields WHERE RowID = @intCnt
				 --Added By :Sachin Sadhu ||Date:13thOct2014
				 --Purpose  :To manage Custom Fields Queue
							EXEC USP_CFW_FLD_Values_CT
									@numDomainID =@numDomainID,
									@numUserCntID =0,
									@numRecordID =@RecordId ,
									@numFormFieldId  =@numFieldID
			END 

	END
End       
-- action item details, hard coded flag to control delete behaviour of custom field bug id 635
if @PageId=128 
begin          

insert into CFW_FLD_Values (Fld_ID,Fld_Value,RecId)          
select X.fld_id,X.Value,@RecordId  from(SELECT *FROM OPENXML (@hDoc,'/NewDataSet/Table[FldDTLID=0]',2)          
WITH (fld_id numeric(9),FldDTLID numeric(9),Value varchar(1000)))X 


update CFW_FLD_Values set Fld_Value=X.Value  from      
(SELECT * FROM OPENXML (@hDoc,'/NewDataSet/Table[FldDTLID>0]',2)          
WITH (fld_id numeric(9),FldDTLID numeric(9),Value varchar(1000)))X
where CFW_FLD_Values.FldDTLID=X.FldDTLID

End        
 
          
if @PageId=4          
begin  

Delete from CFW_FLD_Values_Cont where FldDTLID not in (SELECT FldDTLID FROM OPENXML (@hDoc,'/NewDataSet/Table[FldDTLID>0]',2)          
WITH (fld_id numeric(9),FldDTLID numeric(9),Value varchar(1000))) 
and  RecId= @RecordId      


insert into CFW_FLD_Values_Cont (Fld_ID,Fld_Value,RecId)          
select X.fld_id,X.Value,@RecordId  from(SELECT *FROM OPENXML (@hDoc,'/NewDataSet/Table[FldDTLID=0]',2)          
WITH (fld_id numeric(9),FldDTLID numeric(9),Value varchar(1000)))X 


update CFW_FLD_Values_Cont set Fld_Value=X.Value  from      
(SELECT * FROM OPENXML (@hDoc,'/NewDataSet/Table[FldDTLID>0]',2)          
WITH (fld_id numeric(9),FldDTLID numeric(9),Value varchar(1000)))X
where CFW_FLD_Values_Cont.FldDTLID=X.FldDTLID

--        
--Delete from CFW_FLD_Values_Cont where RecId=@RecordId          
--insert into CFW_FLD_Values_Cont (Fld_ID,Fld_Value,RecId)          
--          
--select X.fld_id,X.Value,@RecordId  from(SELECT *FROM OPENXML (@hDoc,'/NewDataSet/Table[fld_id>0]',2)          
-- WITH (fld_id numeric(9),Value varchar(1000)))X          
    
	
--Author :Sachin Sadhu ||Date:13thOctober2014
--Purpose:To make Entry in WorkFLow queue(Identify rules based on Custom fields)

DECLARE @CFFieldsCon as table (  
  RowID INT IDENTITY(1, 1),
  fld_id numeric(9)
  )
INSERT INTO @CFFieldsCon(fld_id) 
SELECT Fld_ID from CFW_FLD_Values_Cont where RecId=@RecordId
--select X.fld_id  from(SELECT *FROM OPENXML (@hDoc,'/NewDataSet/Table[FldDTLID>0]',2)          
--WITH (fld_id numeric(9),FldDTLID numeric(9),Value varchar(1000)))X 

DECLARE @intCntCon AS INT
DECLARE @intCntFieldsCon AS INT
Declare @numFieldIDCon As numeric(9,0)
Declare @numDomainIDCon as numeric(18,0)
Declare @numUserIDCon as numeric(18,0)
SET @intCntCon = 0
SET @intCntFieldsCon = (SELECT COUNT(*) FROM @CFFieldsCon)
SELECT @numDomainIDCon=numDomainID,@numUserIDCon=numCreatedBy from AdditionalContactsInformation where numContactId=@RecordId

IF @intCntFieldsCon > 0
	BEGIN
		WHILE(@intCntCon < @intCntFieldsCon)
			BEGIN
			
				SET @intCntCon = @intCntCon + 1
				SELECT @numFieldIDCon = fld_id FROM @CFFieldsCon WHERE RowID = @intCntCon
				 --Added By :Sachin Sadhu ||Date:14thOct2014
				 --Purpose  :To manage Custom Fields Queue
							EXEC USP_CFW_FLD_Values_Cont_CT
									@numDomainID =@numDomainIDCon,
									@numUserCntID =@numUserIDCon,
									@numRecordID =@RecordId ,
									@numFormFieldId  =@numFieldIDCon
			END 

	END  
	   
end          
          
if @PageId=3          
BEGIN 
	DECLARE @TEMP TABLE
	(
		fld_id numeric(9),
		FldDTLID numeric(9),
		Value varchar(1000)
	)

	INSERT INTO @TEMP 
	(
		fld_id,
		FldDTLID,
		Value
	)
	SELECT
		fld_id,
		FldDTLID,
		Value
	FROM OPENXML 
		(@hDoc,'/NewDataSet/Table',2)          
	WITH 
		(fld_id NUMERIC(18,0),FldDTLID NUMERIC(18,0),Value VARCHAR(1000)) 

  
	DELETE FROM 
		CFW_FLD_Values_Case 
	WHERE 
		FldDTLID NOT IN (SELECT FldDTLID FROM @TEMP WHERE FldDTLID > 0) 
		AND RecId= @RecordId      
		AND Fld_ID NOT IN (SELECT ISNULL(numChildFieldID,0) FROM ParentChildCustomFieldMap WHERE tintChildModule=3)  


	INSERT INTO CFW_FLD_Values_Case 
	(
		Fld_ID,
		Fld_Value,
		RecId
	)          
	SELECT 
		fld_id,
		Value,
		@RecordId  
	FROM
		@TEMP
	WHERE
		FldDTLID=0
		AND fld_id NOT IN (SELECT ISNULL(numChildFieldID,0) FROM ParentChildCustomFieldMap WHERE tintChildModule=3)


	UPDATE 
		CFW_FLD_Values_Case 
	SET 
		Fld_Value = X.Value  
	FROM
	   @TEMP X
	WHERE
	   X.FldDTLID > 0
	   AND CFW_FLD_Values_Case.FldDTLID=X.FldDTLID
	   AND  CFW_FLD_Values_Case.Fld_ID NOT IN (SELECT ISNULL(numChildFieldID,0) FROM ParentChildCustomFieldMap WHERE tintChildModule=3)           
          
--Delete from CFW_FLD_Values_Case where RecId=@RecordId          
--insert into CFW_FLD_Values_Case (Fld_ID,Fld_Value,RecId)          
--          
--select X.fld_id,X.Value,@RecordId  from(SELECT *FROM OPENXML (@hDoc,'/NewDataSet/Table[fld_id>0]',2)          
-- WITH (fld_id numeric(9),Value varchar(1000)))X          
     
	 --Author :Sachin Sadhu ||Date:13thOctober2014
--Purpose:To make Entry in WorkFLow queue(Identify rules based on Custom fields)

DECLARE @CFFieldsCase as table (  
  RowID INT IDENTITY(1, 1),
  fld_id numeric(9)
  )
INSERT INTO @CFFieldsCase(fld_id) 
SELECT Fld_ID from CFW_FLD_Values_Case where RecId=@RecordId
--select X.fld_id  from(SELECT *FROM OPENXML (@hDoc,'/NewDataSet/Table[FldDTLID>0]',2)          
--WITH (fld_id numeric(9),FldDTLID numeric(9),Value varchar(1000)))X 

DECLARE @intCntCase AS INT
DECLARE @intCntFieldsCase AS INT
Declare @numFieldIDCase As numeric(9,0)
Declare @numDomainIDCase as numeric(18,0)
Declare @numUserIDCase as numeric(18,0)
SET @intCntCase = 0
SET @intCntFieldsCase = (SELECT COUNT(*) FROM @CFFieldsCase)
SELECT @numDomainIDCase=numDomainID,@numUserIDCase=numCreatedBy from Cases where numCaseId=@RecordId

IF @intCntFieldsCase > 0
	BEGIN
		WHILE(@intCntCase < @intCntFieldsCase)
			BEGIN
			--print 'sahc'
				SET @intCntCase = @intCntCase + 1
				SELECT @numFieldIDCase = fld_id FROM @CFFieldsCase WHERE RowID = @intCntCase
				 --Added By :Sachin Sadhu ||Date:13thOct2014
				 --Purpose  :To manage Custom Fields Queue
							EXEC USP_CFW_FLD_Values_Case_CT
									@numDomainID =@numDomainIDCase,
									@numUserCntID =@numUserIDCase,
									@numRecordID =@RecordId ,
									@numFormFieldId  =@numFieldIDCase
			END 

	END
	      
end          
        
if @PageId=5        
begin          


Delete from CFW_FLD_Values_Item where FldDTLID not in (SELECT FldDTLID FROM OPENXML (@hDoc,'/NewDataSet/Table[FldDTLID>0]',2)          
WITH (fld_id numeric(9),FldDTLID numeric(9),Value varchar(1000))) 
and  RecId= @RecordId      


insert into CFW_FLD_Values_Item (Fld_ID,Fld_Value,RecId)          
select X.fld_id,X.Value,@RecordId  from(SELECT *FROM OPENXML (@hDoc,'/NewDataSet/Table[FldDTLID=0]',2)          
WITH (fld_id numeric(9),FldDTLID numeric(9),Value varchar(1000)))X 


update CFW_FLD_Values_Item set Fld_Value=X.Value  from      
(SELECT * FROM OPENXML (@hDoc,'/NewDataSet/Table[FldDTLID>0]',2)          
WITH (fld_id numeric(9),FldDTLID numeric(9),Value varchar(1000)))X
where CFW_FLD_Values_Item.FldDTLID=X.FldDTLID  


--Delete from CFW_FLD_Values_Item where RecId=@RecordId          
--insert into CFW_FLD_Values_Item (Fld_ID,Fld_Value,RecId)          
--          
--select X.fld_id,X.Value,@RecordId  from(SELECT *FROM OPENXML (@hDoc,'/NewDataSet/Table[fld_id>0]',2)          
-- WITH (fld_id numeric(9),Value varchar(1000)))X          
          
end         
          
if @PageId=6 or @PageId=2      
begin       

Delete from CFW_Fld_Values_Opp where FldDTLID not in (SELECT FldDTLID FROM OPENXML (@hDoc,'/NewDataSet/Table[FldDTLID>0]',2)          
WITH (fld_id numeric(9),FldDTLID numeric(9),Value varchar(1000))) 
and  RecId= @RecordId      


insert into CFW_Fld_Values_Opp (Fld_ID,Fld_Value,RecId)          
select X.fld_id,X.Value,@RecordId  from(SELECT *FROM OPENXML (@hDoc,'/NewDataSet/Table[FldDTLID=0]',2)          
WITH (fld_id numeric(9),FldDTLID numeric(9),Value varchar(1000)))X 


update CFW_Fld_Values_Opp set Fld_Value=X.Value  from      
(SELECT * FROM OPENXML (@hDoc,'/NewDataSet/Table[FldDTLID>0]',2)          
WITH (fld_id numeric(9),FldDTLID numeric(9),Value varchar(1000)))X
where CFW_Fld_Values_Opp.FldDTLID=X.FldDTLID 
   
--Delete from CFW_Fld_Values_Opp where RecId=@RecordId          
--insert into CFW_Fld_Values_Opp (Fld_ID,Fld_Value,RecId)          
--          
--select X.fld_id,X.Value,@RecordId  from(SELECT *FROM OPENXML (@hDoc,'/NewDataSet/Table[fld_id>0]',2)          
-- WITH (fld_id numeric(9),Value varchar(1000)))X     
  
--Author :Sachin Sadhu ||Date:13thOctober2014
--Purpose:To make Entry in WorkFLow queue(Identify rules based on Custom fields)

DECLARE @CFFieldsOpp as table (  
  RowID INT IDENTITY(1, 1),
  fld_id numeric(9)
  )
INSERT INTO @CFFieldsOpp(fld_id) 
SELECT Fld_ID from CFW_Fld_Values_Opp where RecId=@RecordId
--select X.fld_id  from(SELECT *FROM OPENXML (@hDoc,'/NewDataSet/Table[FldDTLID>0]',2)          
--WITH (fld_id numeric(9),FldDTLID numeric(9),Value varchar(1000)))X 

DECLARE @intCntOpp AS INT
DECLARE @intCntFieldsOpp AS INT
Declare @numFieldIDOpp As numeric(9,0)
Declare @numDomainIDOpp as numeric(18,0)
Declare @numUserIDOpp as numeric(18,0)
SET @intCntOpp = 0
SET @intCntFieldsOpp = (SELECT COUNT(*) FROM @CFFieldsOpp)
SELECT @numDomainIDOpp=numDomainID,@numUserIDOpp=numCreatedBy from OpportunityMaster where numOppId=@RecordId

IF @intCntFieldsOpp > 0
	BEGIN
		WHILE(@intCntOpp < @intCntFieldsOpp)
			BEGIN
			--print 'sahc'
				SET @intCntOpp = @intCntOpp + 1
				SELECT @numFieldIDOpp = fld_id FROM @CFFieldsOpp WHERE RowID = @intCntOpp
				 --Added By :Sachin Sadhu ||Date:14thOct2014
				 --Purpose  :To manage Custom Fields Queue
							EXEC USP_CFW_Fld_Values_Opp_CT
									@numDomainID =@numDomainIDOpp,
									@numUserCntID =@numUserIDOpp,
									@numRecordID =@RecordId ,
									@numFormFieldId  =@numFieldIDOpp
			END 

	END

   
          
end      
    
if @PageId=7 or    @PageId=8    
begin

Delete from CFW_Fld_Values_Product where FldDTLID not in (SELECT FldDTLID FROM OPENXML (@hDoc,'/NewDataSet/Table[FldDTLID>0]',2)          
WITH (fld_id numeric(9),FldDTLID numeric(9),Value varchar(1000))) 
and  RecId= @RecordId      


insert into CFW_Fld_Values_Product (Fld_ID,Fld_Value,RecId)          
select X.fld_id,X.Value,@RecordId  from(SELECT *FROM OPENXML (@hDoc,'/NewDataSet/Table[FldDTLID=0]',2)          
WITH (fld_id numeric(9),FldDTLID numeric(9),Value varchar(1000)))X 


update CFW_Fld_Values_Product set Fld_Value=X.Value  from      
(SELECT * FROM OPENXML (@hDoc,'/NewDataSet/Table[FldDTLID>0]',2)          
WITH (fld_id numeric(9),FldDTLID numeric(9),Value varchar(1000)))X
where CFW_Fld_Values_Product.FldDTLID=X.FldDTLID
          
--Delete from CFW_Fld_Values_Product where RecId=@RecordId          
--insert into CFW_Fld_Values_Product (Fld_ID,Fld_Value,RecId)          
--          
--select X.fld_id,X.Value,@RecordId  from(SELECT *FROM OPENXML (@hDoc,'/NewDataSet/Table[fld_id>0]',2)          
-- WITH (fld_id numeric(9),Value varchar(1000)))X          
          
end     
          
if @PageId=11  
begin   

Delete from CFW_Fld_Values_Pro where FldDTLID not in (SELECT FldDTLID FROM OPENXML (@hDoc,'/NewDataSet/Table[FldDTLID>0]',2)          
WITH (fld_id numeric(9),FldDTLID numeric(9),Value varchar(1000))) 
and  RecId= @RecordId      


insert into CFW_Fld_Values_Pro (Fld_ID,Fld_Value,RecId)          
select X.fld_id,X.Value,@RecordId  from(SELECT *FROM OPENXML (@hDoc,'/NewDataSet/Table[FldDTLID=0]',2)          
WITH (fld_id numeric(9),FldDTLID numeric(9),Value varchar(1000)))X 


update CFW_Fld_Values_Pro set Fld_Value=X.Value  from      
(SELECT * FROM OPENXML (@hDoc,'/NewDataSet/Table[FldDTLID>0]',2)          
WITH (fld_id numeric(9),FldDTLID numeric(9),Value varchar(1000)))X
where CFW_Fld_Values_Pro.FldDTLID=X.FldDTLID
       
--Delete from CFW_Fld_Values_Pro where RecId=@RecordId          
--insert into CFW_Fld_Values_Pro (Fld_ID,Fld_Value,RecId)          
--          
--select X.fld_id,X.Value,@RecordId  from(SELECT *FROM OPENXML (@hDoc,'/NewDataSet/Table[fld_id>0]',2)          
-- WITH (fld_id numeric(9),Value varchar(1000)))X    


--Author :Sachin Sadhu ||Date:13thOctober2014
--Purpose:To make Entry in WorkFLow queue(Identify rules based on Custom fields)

DECLARE @CFFieldsPro as table (  
  RowID INT IDENTITY(1, 1),
  fld_id numeric(9)
  )
INSERT INTO @CFFieldsPro(fld_id) 
SELECT Fld_ID from CFW_FLD_Values_Pro where RecId=@RecordId
--select X.fld_id  from(SELECT *FROM OPENXML (@hDoc,'/NewDataSet/Table[FldDTLID>0]',2)          
--WITH (fld_id numeric(9),FldDTLID numeric(9),Value varchar(1000)))X 

DECLARE @intCntPro AS INT
DECLARE @intCntFieldsPro AS INT
Declare @numFieldIDPro As numeric(9,0)
Declare @numDomainIDPro as numeric(18,0)
Declare @numUserIDPro as numeric(18,0)
SET @intCntPro = 0
SET @intCntFieldsPro = (SELECT COUNT(*) FROM @CFFieldsPro)
SELECT @numDomainIDPro=numDomainID,@numUserIDPro=numCreatedBy from ProjectsMaster where numProId=@RecordId

IF @intCntFieldsPro > 0
	BEGIN
		WHILE(@intCntPro < @intCntFieldsPro)
			BEGIN
			--print 'sahc'
				SET @intCntPro = @intCntPro + 1
				SELECT @numFieldIDPro = fld_id FROM @CFFieldsPro WHERE RowID = @intCntPro
				 --Added By :Sachin Sadhu ||Date:14thOct2014
				 --Purpose  :To manage Custom Fields Queue
							EXEC USP_CFW_FLD_Values_Pro_CT
									@numDomainID =@numDomainIDPro,
									@numUserCntID =@numUserIDPro,
									@numRecordID =@RecordId ,
									@numFormFieldId  =@numFieldIDPro
			END 

	END      
          
end

--Added by Neelam Kapila || 12/01/2017 - Added condition to save custom items to CFW_Fld_Values_OppItems table
IF @PageId = 17        
BEGIN      
	DELETE FROM CFW_Fld_Values_OppItems 
	WHERE FldDTLID NOT IN (SELECT FldDTLID FROM OPENXML (@hDoc,'/NewDataSet/Table[FldDTLID>0]',2)          
	WITH (fld_id NUMERIC(9), FldDTLID NUMERIC(9), Value VARCHAR(1000))) 
	AND RecId = @RecordId    

	INSERT INTO CFW_Fld_Values_OppItems (Fld_ID,Fld_Value,RecId)          
	SELECT X.fld_id, X.Value, @RecordId  
	FROM (SELECT * FROM OPENXML (@hDoc,'/NewDataSet/Table[FldDTLID=0]',2)          
	WITH (fld_id NUMERIC(9), FldDTLID NUMERIC(9), Value VARCHAR(1000)))X 

	UPDATE CFW_Fld_Values_OppItems 
	SET Fld_Value=X.Value  
	FROM (SELECT * FROM OPENXML (@hDoc,'/NewDataSet/Table[FldDTLID>0]',2)          
	WITH (fld_id NUMERIC(9), FldDTLID NUMERIC(9), Value VARCHAR(1000)))X
	WHERE CFW_Fld_Values_OppItems.FldDTLID = X.FldDTLID  
END 
 
       
EXEC sp_xml_removedocument @hDoc
END
GO
/****** Object:  StoredProcedure [dbo].[USP_OPPCheckCanBeShipped]    Script Date: 07/26/2008 16:20:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON            
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CheckOrderedAndInvoicedOrBilledQty')
DROP PROCEDURE USP_CheckOrderedAndInvoicedOrBilledQty
GO
CREATE PROCEDURE [dbo].[USP_CheckOrderedAndInvoicedOrBilledQty]              
@numOppID AS NUMERIC(18,0)
AS             
BEGIN
	DECLARE @TMEP TABLE
	(
		ID INT,
		vcDescription VARCHAR(100),
		bitTrue BIT
	)

	INSERT INTO 
		@TMEP
	VALUES
		(1,'Fulfillment bizdoc is added but not yet fulfilled',0),
		(2,'Invoice/Bill and Ordered Qty is not same',0),
		(3,'Ordered & Fulfilled Qty is not same',0),
		(4,'Invoice is not generated against diferred income bizDocs.',0),
		(5,'Qty is not available to add in deferred bizdoc.',0)

	DECLARE @tintOppType AS TINYINT
	DECLARE @tintOppStatus AS TINYINT

	SELECT @tintOppType=tintOppType,@tintOppStatus=tintOppStatus FROM OpportunityMaster WHERe numOppID=@numOppID


	-- CHECK IF FULFILLMENT BIZDOC IS ADDED TO SALES ORDER BUT IT IS NOT FULFILLED FROM SALES FULFILLMENT SCREEN
	IF @tintOppType = 1 AND @tintOppStatus = 1  --SALES ORDER
	BEGIN
		IF (SELECT COUNT(*) FROM OpportunityBizDocs WHERE numOppId=@numOppID AND numBizDocId=296 AND ISNULL(bitFulFilled,0) = 0) > 0
		BEGIN
			UPDATE @TMEP SET bitTrue = 1 WHERE ID = 1
		END

		-- CHECK IF FULFILLMENT BIZDOC QTY OF ITEMS IS NOT SAME AS ORDERED QTY
		IF (SELECT 
				COUNT(*) 
			FROM 
			(
				SELECT
					OI.numoppitemtCode,
					ISNULL(OI.numUnitHour,0) AS OrderedQty,
					ISNULL(TempFulFilled.FulFilledQty,0) AS FulFilledQty
				FROM
					OpportunityItems OI
				INNER JOIN
					Item I
				ON
					OI.numItemCode = I.numItemCode
				OUTER APPLY
				(
					SELECT
						SUM(OpportunityBizDocItems.numUnitHour) AS FulFilledQty
					FROM
						OpportunityBizDocs
					INNER JOIN
						OpportunityBizDocItems 
					ON
						OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
					WHERE
						OpportunityBizDocs.numOppId = @numOppID
						AND ISNULL(OpportunityBizDocs.numBizDocId,0) = 296
						AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
				) AS TempFulFilled
				WHERE
					OI.numOppID = @numOppID
					AND UPPER(I.charItemType) = 'P'
					AND ISNULL(OI.bitDropShip,0) = 0
			) X
			WHERE
				X.OrderedQty <> X.FulFilledQty) > 0
		BEGIN
			UPDATE @TMEP SET bitTrue = 1 WHERE ID = 3
		END

		-- CHECK WHETHER INVOICES ARE GENERATED AGAINST DIFERRED INCOME BIZDOC
		IF (SELECT 
				COUNT(*)
			FROM  
			(
				SELECT 
					OBDI.numOppItemID,
					(ISNULL(SUM(OBDI.numUnitHour),0) - ISNULL(SUM(TEMPInvoiceAgainstDeferred.numUnitHour),0)) AS intQtyRemaining
				FROM
					OpportunityBizDocs OBD
				INNER JOIN
					OpportunityBizDocItems OBDI
				ON
					OBD.numOppBizDocsId = OBDI.numOppBizDocID
				CROSS APPLY
				(
					SELECT 
						SUM(OpportunityBizDocItems.numUnitHour) AS numUnitHour
					FROM
						OpportunityBizDocs
					INNER JOIN
						OpportunityBizDocItems
					ON
						OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
					WHERE
						numOppId=@numOppID
						AND ISNULL(numDeferredBizDocID,0) > 0
						AND OpportunityBizDocItems.numOppItemID = OBDI.numOppItemID

				) AS TEMPInvoiceAgainstDeferred
				WHERE
					numOppId=@numOppID 
					AND numBizDocId=304
				GROUP BY
					OBDI.numOppItemID
			) AS TEMP
			WHERE
				intQtyRemaining > 0) > 0
		BEGIN
			UPDATE @TMEP SET bitTrue = 1 WHERE ID = 4
		END

		-- CHECK WHETHER ITEM QTY LEFT TO ADD TO DIFERRED INCOME BIZDOC
		IF (SELECT 
				COUNT(*) 
			FROM 
			(
				SELECT
					OI.numoppitemtCode,
					ISNULL(OI.numUnitHour,0) AS OrderedQty,
					ISNULL(TempQtyLeftForDifferedIncome.intQty,0) AS intQty
				FROM
					OpportunityItems OI
				INNER JOIN
					Item I
				ON
					OI.numItemCode = I.numItemCode
				OUTER APPLY
				(
					SELECT
						SUM(OpportunityBizDocItems.numUnitHour) AS intQty
					FROM
						OpportunityBizDocs
					INNER JOIN
						OpportunityBizDocItems 
					ON
						OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
					WHERE
						OpportunityBizDocs.numOppId = @numOppID
						AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
						AND (ISNULL(OpportunityBizDocs.bitAuthoritativeBizDocs,0) = 1 OR OpportunityBizDocs.numBizDocID = 304)
				) AS TempQtyLeftForDifferedIncome
				WHERE
					OI.numOppID = @numOppID
			) X
			WHERE
				X.OrderedQty <> X.intQty) = 0
		BEGIN
			UPDATE @TMEP SET bitTrue = 1 WHERE ID = 5
		END
	END

	-- IF INVOICED/BILLED QTY OF ITEMS IS NOT SAME AS ORDERED QTY THEN ORDER CAN NOT BE SHIPPED/RECEIVED
	IF (SELECT 
			COUNT(*) 
		FROM 
		(
			SELECT
				OI.numoppitemtCode,
				ISNULL(OI.numUnitHour,0) AS OrderedQty,
				ISNULL(TempInvoice.InvoicedQty,0) AS InvoicedQty
			FROM
				OpportunityItems OI
			INNER JOIN
				Item I
			ON
				OI.numItemCode = I.numItemCode
			OUTER APPLY
			(
				SELECT
					SUM(OpportunityBizDocItems.numUnitHour) AS InvoicedQty
				FROM
					OpportunityBizDocs
				INNER JOIN
					OpportunityBizDocItems 
				ON
					OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
				WHERE
					OpportunityBizDocs.numOppId = @numOppID
					AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
					AND ISNULL(OpportunityBizDocs.bitAuthoritativeBizDocs,0) = 1
			) AS TempInvoice
			WHERE
				OI.numOppID = @numOppID
		) X
		WHERE
			X.OrderedQty <> X.InvoicedQty) > 0
	BEGIN
		UPDATE @TMEP SET bitTrue = 1 WHERE ID = 2
	END

	SELECT * FROM @TMEP

	--GET FULFILLMENT BIZDOCS WHICH ARE NOT FULFILLED YET
	SELECT numOppBizDocsId FROM OpportunityBizDocs WHERE OpportunityBizDocs.numOppId = @numOppID AND ISNULL(OpportunityBizDocs.numBizDocId,0) = 296 AND ISNULL(OpportunityBizDocs.bitFulFilled,0) = 0
END


/****** Object:  StoredProcedure [dbo].[USP_DeleteCartItem]    Script Date: 11/08/2011 17:28:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_DeleteCartItem' ) 
                    DROP PROCEDURE USP_DeleteCartItem
                    Go
CREATE PROCEDURE [dbo].[USP_DeleteCartItem]
    (
      @numUserCntId NUMERIC(18, 0),
      @numDomainId NUMERIC(18, 0),
      @vcCookieId VARCHAR(100),
      @numItemCode NUMERIC(9, 0) = 0,
      @bitDeleteAll BIT=0,
	  @numSiteID NUMERIC(18,0) = 0
    )
AS 
BEGIN
	IF @bitDeleteAll = 0
	BEGIN
		IF @numItemCode <> 0 
		BEGIN
			-- REMOVE DISCOUNT APPLIED TO ALL ITEMS FROM PROMOTION TRIGGERED OF ITEM TO BE DELETED
			UPDATE 
				CartItems
			SET 
				PromotionDesc='',PromotionID=0,fltDiscount=0,monTotAmount=(monPrice*numUnitHour),monTotAmtBefDiscount=(monPrice*numUnitHour),vcCoupon=NULL,bitParentPromotion=0
			WHERE
				PromotionID=(SELECT TOP 1 PromotionID FROM CartItems WHERE numDomainId = @numDomainId AND numUserCntId = @numUserCntId AND numItemCode = @numItemCode AND vcCookieId=@vcCookieId AND ISNULL(bitParentPromotion,0)=1)
				AND numDomainId = @numDomainId
				AND numUserCntId = @numUserCntId
				AND vcCookieId=@vcCookieId
				AND ISNULL(bitParentPromotion,0)=0

			--DELETE FROM CartItems WHERE numDomainId = @numDomainId AND numUserCntId = @numUserCntId AND numItemCode = @numItemCode	
			
			DELETE FROM CartItems 
			WHERE numDomainId = @numDomainId AND numUserCntId = @numUserCntId
				AND (numItemCode = @numItemCode 
					OR numItemCode IN (SELECT numItemCode FROM SimilarItems WHERE numParentItemCode = @numItemCode AND numDomainId = @numDomainId AND bitrequired = 1))	

			EXEC USP_ManageECommercePromotion @numUserCntId,@numDomainId,@vcCookieId,0,@numSiteID
		END
		ELSE
		BEGIN
			UPDATE 
				CartItems
			SET 
				PromotionDesc='',PromotionID=0,fltDiscount=0,monTotAmount=(monPrice*numUnitHour),monTotAmtBefDiscount=(monPrice*numUnitHour),vcCoupon=NULL,bitParentPromotion=0
			WHERE
				numDomainId = @numDomainId
				AND numUserCntId = @numUserCntId
				AND vcCookieId=@vcCookieId

			DELETE FROM CartItems 
			WHERE numDomainId = @numDomainId AND numUserCntId = 0 AND vcCookieId = @vcCookieId		
		END
	END
	ELSE
	BEGIN
		DELETE FROM CartItems WHERE numDomainId = @numDomainId AND numUserCntId = @numUserCntId	AND vcCookieId <> ''
	END
END

 



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DelPromotionOffer')
DROP PROCEDURE USP_DelPromotionOffer
GO
CREATE PROCEDURE [dbo].[USP_DelPromotionOffer]
	@numDomainID AS NUMERIC(18,0),
	@numProId AS NUMERIC(18,0)
AS
BEGIN
	DELETE FROM PromotionOfferOrganizations WHERE numProId = @numProId 
	DELETE FROM PromotionOfferItems WHERE numProId = @numProId AND tintRecordType IN (5,6)
	DELETE FROM PromotionOfferSites WHERE numPromotionID=@numProId
	DELETE FROM PromotionOffer WHERE numDomainId=@numDomainID AND numProId=@numProId		
END
GO


/****** Object:  StoredProcedure [dbo].[USP_GetCartItem]    Script Date: 11/08/2011 18:00:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetCartItem' ) 
                    DROP PROCEDURE USP_GetCartItem
                    Go
CREATE PROCEDURE [dbo].[USP_GetCartItem]
(
	@numUserCntId numeric(18,0),
	@numDomainId numeric(18,0),
	@vcCookieId varchar(100),
	@bitUserType BIT =0--if 0 then anonomyous user 1 for logi user
)
AS
	IF @numUserCntId <> 0
	BEGIN
		SELECT DISTINCT numCartId,
			   numParentItemCode,
			   numUserCntId,
			   [dbo].[CartItems].numDomainId,
			   vcCookieId,
			   numOppItemCode,
			   [dbo].[CartItems].numItemCode,
			   (SELECT TOP 1 numCategoryId  FROM dbo.ItemCategory WHERE numItemID =  CartItems.numItemCode AND [dbo].[CartItems].numDomainId = @numDomainId ) AS numCategoryId,
			   (SELECT vcCategoryName FROM [dbo].[Category] WHERE [Category].[numCategoryID]  = (SELECT TOP 1 numCategoryId  FROM dbo.ItemCategory WHERE numItemID =  CartItems.numItemCode AND [dbo].[CartItems].numDomainId = @numDomainId )) AS vcCategoryName,
			   numUnitHour,
			   monPrice,
			   monPrice AS [monListPrice],
			   numSourceId,
			   vcItemDesc,
			   vcItemDesc AS txtItemDesc,
			   [dbo].[CartItems].numWarehouseId,
			   [dbo].[CartItems].vcItemName,
			   vcWarehouse,
			   numWarehouseItmsID,
			   vcItemType,
			   vcAttributes,
			   vcAttrValues,
			   I.bitFreeShipping,
			   fltWeight as numWeight,
			   ISNULL((SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numDomainId=@numDomainId AND bitDefault=1 AND numItemCode=I.numItemCode),'') AS vcPathForTImage,
			   tintOpFlag,
			   bitDiscountType,
			   fltDiscount,
			   monTotAmtBefDiscount,
			   ItemURL,
			   numUOM,
			   vcUOMName,
			   decUOMConversionFactor,
			   numHeight,
			   numLength,
			   numWidth,
			   vcShippingMethod,
			   numServiceTypeId,
			   decShippingCharge,
			   numShippingCompany,
			   vcCoupon,
			   tintServicetype,CAST( dtDeliveryDate AS VARCHAR(30)) dtDeliveryDate,monTotAmount,
			   ISNULL(monTotAmtBefDiscount,0) - ISNULL(monTotAmount,0) AS monTotalDiscountAmount,
			   (SELECT vcModelID FROM dbo.Item WHERE Item.numItemCode =  CartItems.numItemCode) AS [vcModelID]
			   ,(SELECT TOP 1 vcPathForTImage FROM dbo.ItemImages II WHERE II.numItemCode = CartItems.numItemCode AND II.numDomainId= CartItems.numDomainId AND II.bitDefault=1) As vcPathForTImage
			   ,ISNULL(vcUpSellDesc,'') AS [txtUpSellItemDesc],ISNULL(bitPreUpSell,0) [bitPreUpSell], ISNULL(bitPostUpSell,0) [bitPostUpSell], ISNULL(vcUpSellDesc,'') [vcUpSellDesc]
			   ,ISNULL([I].[vcSKU],'') AS [vcSKU]
			   ,I.vcManufacturer
			   ,ISNULL(PromotionID,0) AS PromotionID
			   ,CAST(ISNULL(PromotionDesc,'') AS varchar) AS PromotionDesc
			   FROM CartItems 
			   LEFT JOIN [dbo].[SimilarItems] AS SI ON SI.[numItemCode] = [dbo].[CartItems].[numItemCode] AND SI.numParentItemCode = CartItems.numParentItem
			   LEFT JOIN [dbo].[Item] AS I ON I.[numItemCode] = [dbo].[CartItems].[numItemCode]
			   --LEFT JOIN [dbo].[WareHouseItems] AS WHI ON WHI.[numItemID] = I.[numItemCode]
			   WHERE [dbo].[CartItems].numDomainId = @numDomainId AND numUserCntId = @numUserCntId 	
				
	END
	ELSE
	BEGIN
		SELECT DISTINCT numCartId,
			   numParentItemCode,
			   numUserCntId,
			   [dbo].[CartItems].numDomainId,
			   vcCookieId,
			   numOppItemCode,
			   [dbo].[CartItems].numItemCode,
			   (SELECT TOP 1 numCategoryId  FROM dbo.ItemCategory WHERE numItemID =  CartItems.numItemCode AND CartItems.numDomainId =@numDomainId ) AS numCategoryId,
			   (SELECT vcCategoryName FROM [dbo].[Category] WHERE [Category].[numCategoryID]  = (SELECT TOP 1 numCategoryId  FROM dbo.ItemCategory WHERE numItemID =  CartItems.numItemCode AND [dbo].[CartItems].numDomainId = @numDomainId )) AS vcCategoryName,
			   numUnitHour,
			   monPrice,
			   monPrice AS [monListPrice],
			   numSourceId,
			   vcItemDesc,
			   vcItemDesc AS txtItemDesc,
			   [dbo].[CartItems].numWarehouseId,
			   [dbo].[CartItems].vcItemName,
			   vcWarehouse,
			   numWarehouseItmsID,
			   vcItemType,
			   vcAttributes,
			   vcAttrValues,
			   I.bitFreeShipping,
			   fltWeight as numWeight,
			   ISNULL((SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numDomainId=@numDomainId AND bitDefault=1 AND numItemCode=I.numItemCode),'') AS vcPathForTImage,
			   tintOpFlag,
			   bitDiscountType,
			   fltDiscount,
			   monTotAmtBefDiscount,
			   ItemURL,
			   numUOM,
			   vcUOMName,
			   decUOMConversionFactor,
			   numHeight,
			   numLength,
			   numWidth,
			   vcShippingMethod,
			   numServiceTypeId,
			   decShippingCharge,
			   numShippingCompany,
			   tintServicetype,
			   vcCoupon,
			   CAST( dtDeliveryDate AS VARCHAR(30)) dtDeliveryDate,
			   monTotAmount,
			   ISNULL(monTotAmtBefDiscount,0) - ISNULL(monTotAmount,0) AS monTotalDiscountAmount,
			   (SELECT vcModelID FROM dbo.Item WHERE Item.numItemCode =  CartItems.numItemCode) AS [vcModelID]
			   ,(SELECT TOP 1 vcPathForTImage FROM dbo.ItemImages II WHERE II.numItemCode = CartItems.numItemCode AND II.numDomainId= CartItems.numDomainId AND II.bitDefault=1) As vcPathForTImage
			   , ISNULL(vcUpSellDesc,'') AS [txtUpSellItemDesc],ISNULL(bitPreUpSell,0) [bitPreUpSell], ISNULL(bitPostUpSell,0) [bitPostUpSell], ISNULL(vcUpSellDesc,'') [vcUpSellDesc]
			   ,ISNULL([I].[vcSKU],'') AS [vcSKU]
			   ,I.vcManufacturer
			   ,ISNULL(PromotionID,0) AS PromotionID
			   ,CAST(ISNULL(PromotionDesc,'') AS VARCHAR) AS PromotionDesc
			   FROM CartItems
			   LEFT JOIN [dbo].[SimilarItems] AS SI ON SI.[numItemCode] = [dbo].[CartItems].[numItemCode] AND SI.numParentItemCode = CartItems.numParentItem
			   LEFT JOIN [dbo].[Item] AS I ON I.[numItemCode] = [dbo].[CartItems].[numItemCode]
			   --LEFT JOIN [dbo].[WareHouseItems] AS WHI ON WHI.[numItemID] = I.[numItemCode]
			   WHERE [dbo].[CartItems].numDomainId = @numDomainId AND numUserCntId = @numUserCntId AND vcCookieId = @vcCookieId	
				
	END
	
	

--exec USP_GetCartItem @numUserCntId=1,@numDomainId=1,@vcCookieId='fc3d604b-25fa-4c25-960c-1e317768113e',@bitUserType=NULL


GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetECommerceDetails')
DROP PROCEDURE USP_GetECommerceDetails
GO
CREATE PROCEDURE [dbo].[USP_GetECommerceDetails]      
@numDomainID as numeric(9)=0,
@numSiteID AS INTEGER = 0  
as
BEGIN
		IF ISNULL(@numSiteID,0) = 0
	BEGIN
		SELECT @numSiteID=numDefaultSiteID FROM Domain WHERE numDomainId=@numDomainID
	END                  
select                               
isnull(vcPaymentGateWay,'0') as vcPaymentGateWay,                
isnull(vcPGWManagerUId,'')as vcPGWManagerUId ,                
isnull(vcPGWManagerPassword,'') as vcPGWManagerPassword,              
isnull(bitShowInStock,0) as bitShowInStock,              
isnull(bitShowQOnHand,0) as bitShowQOnHand, 
isnull([numDefaultWareHouseID],0) numDefaultWareHouseID,
isnull(bitCheckCreditStatus,0) as bitCheckCreditStatus,
isnull([numRelationshipId],0) numRelationshipId,
isnull([numProfileId],0) numProfileId,
ISNULL([bitHidePriceBeforeLogin],0) AS bitHidePriceBeforeLogin,
ISNULL([numBizDocForCreditCard],0) AS numBizDocForCreditCard,
ISNULL([numBizDocForCreditTerm],0) AS numBizDocForCreditTerm,
ISNULL(bitAuthOnlyCreditCard,0) AS bitAuthOnlyCreditCard,
ISNULL(numAuthrizedOrderStatus,0) AS numAuthrizedOrderStatus,
ISNULL(bitSendEmail,1) AS bitSendEmail,
ISNULL(vcGoogleMerchantID , '') AS vcGoogleMerchantID,
ISNULL(vcGoogleMerchantKey,'') AS vcGoogleMerchantKey,
ISNULL(IsSandbox,0) AS IsSandbox,
ISNULL(numSiteID,0) AS numSiteID,
ISNULL(vcPaypalUserName,'') AS vcPaypalUserName,
ISNULL(vcPaypalPassword,'') AS vcPaypalPassword,
ISNULL(vcPaypalSignature,'') AS vcPaypalSignature,
ISNULL(IsPaypalSandbox,0) AS IsPaypalSandbox,
ISNULL(bitSkipStep2,0) AS [bitSkipStep2],
ISNULL(bitDisplayCategory,0) AS [bitDisplayCategory],
ISNULL(bitShowPriceUsingPriceLevel,0) AS [bitShowPriceUsingPriceLevel],
ISNULL(bitEnableSecSorting,0) AS [bitEnableSecSorting],
ISNULL(bitSortPriceMode,0) AS [bitSortPriceMode],
ISNULL(numPageSize,0) AS [numPageSize],
ISNULL(numPageVariant,0) AS [numPageVariant],
ISNULL(bitAutoSelectWarehouse,0) AS bitAutoSelectWarehouse,
ISNULL([bitPreSellUp],0) AS [bitPreSellUp],
ISNULL([bitPostSellUp],0) AS [bitPostSellUp],
ISNULL([dcPostSellDiscount],0) AS [dcPostSellDiscount],
ISNULL(numDefaultClass,0) numDefaultClass,
vcPreSellUp
,vcPostSellUp
,ISNULL(vcRedirectThankYouUrl,'') AS vcRedirectThankYouUrl
,ISNULL(tintPreLoginProceLevel,0) tintPreLoginProceLevel
,ISNULL(bitHideAddtoCart,0) bitHideAddtoCart
FROM eCommerceDTL 
WHERE numDomainID=@numDomainID
AND ([numSiteId] = @numSiteID OR ISNULL(@numSiteID,0) = 0)
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetPromotionOffer')
DROP PROCEDURE USP_GetPromotionOffer
GO
CREATE PROCEDURE [dbo].[USP_GetPromotionOffer]
    @numProId AS NUMERIC(9) = 0,
    @byteMode AS TINYINT = 0,
    @numDomainID AS NUMERIC(9) = 0
AS 
BEGIN
    IF @byteMode = 0 
    BEGIN    
		SELECT  
			[numProId]
			,[vcProName]
			,[numDomainId]
			,[dtValidFrom]
			,[dtValidTo]
			,[bitNeverExpires]
			,[bitApplyToInternalOrders]
			,[bitAppliesToSite]
			,[bitRequireCouponCode]
			,[txtCouponCode]
			,[tintUsageLimit]
			--,[bitFreeShiping]
			--,[monFreeShippingOrderAmount]
			--,[numFreeShippingCountry]
			,[bitDisplayPostUpSell]
			,[intPostSellDiscount]
			--,[bitFixShipping1]
			--,[monFixShipping1OrderAmount]
			--,[monFixShipping1Charge]
			--,[bitFixShipping2]
			--,[monFixShipping2OrderAmount]
			--,[monFixShipping2Charge]
			,[tintOfferTriggerValueType]
			,[fltOfferTriggerValue]
			,[tintOfferBasedOn]
			,[fltDiscountValue]
			,[tintDiscountType]
			,[tintDiscoutBaseOn]
			,[numCreatedBy]
			,[dtCreated]
			,[numModifiedBy]
			,[dtModified]
			,[tintCustomersBasedOn]
			,[tintOfferTriggerValueTypeRange]
			,[fltOfferTriggerValueRange]
        FROM 
			PromotionOffer
        WHERE 
			numProId = @numProId
    END    
    ELSE IF @byteMode = 1
    BEGIN    
        SELECT  
			numProId,
            vcProName,
			(CASE WHEN ISNULL(bitNeverExpires,0) = 1 THEN 'Promotion Never Expires' ELSE CONCAT(dbo.FormatedDateFromDate(dtValidFrom,@numDomainID),' to ',dbo.FormatedDateFromDate(dtValidTo,@numDomainID)) END) AS vcDateValidation,
			(CASE WHEN ISNULL(bitRequireCouponCode,0) = 1 THEN CONCAT(txtCouponCode,' (', ISNULL(intCouponCodeUsed,0),')') ELSE '' END) vcCouponCode
			--,CONCAT
			--(
			--	(CASE WHEN ISNULL(bitFixShipping1,0)=1 THEN CONCAT('Spend $',monFixShipping1OrderAmount,' and get $',monFixShipping1Charge,' shipping. ') ELSE '' END)
			--	,(CASE WHEN ISNULL(bitFixShipping2,0)=1  THEN CONCAT('Spend $',monFixShipping2OrderAmount,' and get $',monFixShipping2Charge,' shipping. ') ELSE '' END)
				--,(CASE WHEN bitFreeShiping=1 THEN CONCAT('Spend $',monFreeShippingOrderAmount,' and shipping is FREE if shipping country is ',ISNULL((SELECT vcData FROM ListDetails WHERE numListID=40 AND numListItemID=numFreeShippingCountry),''),'.') ELSE '' END) 
			--) AS vcFreeShippingTerms
			,(CASE WHEN ISNULL(bitApplyToInternalOrders,0) = 1 THEN 'Yes' ELSE 'No' END) AS vcInternalOrders
			,(CASE WHEN ISNULL(bitAppliesToSite,0)=1 THEN (SELECT Stuff((SELECT CONCAT(', ',Sites.vcSiteName) FROM PromotionOfferSites POS INNER JOIN Sites ON pos.numSiteID=Sites.numSiteID WHERE POS.numPromotionID=PO.numProId FOR XML PATH('')), 1, 2, '')) ELSE '' END) AS vcSites
			,CONCAT('Buy '
					,CASE WHEN tintOfferTriggerValueType=1 THEN CAST(fltOfferTriggerValue AS VARCHAR) ELSE CONCAT('$',fltOfferTriggerValue) END
					,CASE tintOfferBasedOn
							WHEN 1 THEN CONCAT(' of "',(SELECT Stuff((SELECT CONCAT(', ',Item.vcItemName) FROM PromotionOfferItems INNER JOIN Item ON PromotionOfferItems.numValue = Item.numItemCode WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 FOR XML PATH('')), 1, 2, '')), '"')
							WHEN 2 THEN CONCAT(' of items belonging to classification(s) "',(SELECT Stuff((SELECT CONCAT(', ',ListDetails.vcData) FROM PromotionOfferItems INNER JOIN ListDetails ON PromotionOfferItems.numValue = ListDetails.numListItemID WHERE numListID=36 AND numProId=PO.numProId AND tintRecordType=5 AND tintType=2 FOR XML PATH('')), 1, 2, '')), '"')
						END
					,' & get '
					, CASE tintDiscountType WHEN 1 THEN CONCAT(fltDiscountValue,'% off on') WHEN 2 THEN CONCAT('$',fltDiscountValue, ' off on') WHEN 3 THEN CONCAT(fltDiscountValue,' of') END
					, CASE 
						WHEN tintDiscoutBaseOn = 1 THEN 
							CONCAT
							(
								CONCAT(' "',(SELECT Stuff((SELECT CONCAT(', ',Item.vcItemName) FROM PromotionOfferItems INNER JOIN Item ON PromotionOfferItems.numValue = Item.numItemCode WHERE numProId=PO.numProId AND tintRecordType=6 AND tintType=1 FOR XML PATH('')), 1, 2, '')), '"')
								,(CASE WHEN PO.tintDiscountType = 3 THEN ' free' ELSE '' END)
								,'.'
							)
						WHEN tintDiscoutBaseOn = 2 THEN 
							CONCAT
							(
								CONCAT(' items belonging to classification(s) "',(SELECT Stuff((SELECT CONCAT(', ',ListDetails.vcData) FROM PromotionOfferItems INNER JOIN ListDetails ON PromotionOfferItems.numValue = ListDetails.numListItemID WHERE numListID=36 AND numProId=PO.numProId AND tintRecordType=6 AND tintType=2 FOR XML PATH('')), 1, 2, '')), '"')
								,(CASE WHEN PO.tintDiscountType = 3 THEN ' free' ELSE '' END)
								,'.'
							)
						WHEN tintDiscoutBaseOn = 3 THEN ' related items.'
						ELSE '' 
					END
				) AS vcPromotionRule,
				(CASE WHEN (SELECT COUNT(*) FROM OpportunityItems JOIN OpportunityMaster ON OpportunityItems.numOppId=OpportunityMaster.numOppId WHERE numDomainId=@numDomainID AND numPromotionID=PO.numProId) > 0 THEN 0 ELSE 1 END) AS bitCanEdit
				,bitEnabled, tintCustomersBasedOn, tintOfferTriggerValueTypeRange, fltOfferTriggerValueRange
        FROM  
			PromotionOffer PO
		WHERE 
			numDomainID = @numDomainID
		ORDER BY
			dtCreated DESC
    END
END      
  
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetSimilarItem')
DROP PROCEDURE USP_GetSimilarItem
GO
CREATE PROCEDURE USP_GetSimilarItem
@numDomainID NUMERIC(9),
@numParentItemCode NUMERIC(9),
@byteMode TINYINT,
@vcCookieId VARCHAR(MAX)='',
@numUserCntID NUMERIC(18)=0,
@numSiteID NUMERIC(18,0)=0,
@numOppID NUMERIC(18,0) = 0
AS 
BEGIN

If @byteMode=1
BEGIN
 select count(*) as Total from SimilarItems SI INNER JOIN Item I ON I.numItemCode = SI.numItemCode 
 WHERE SI.numDomainId = @numDomainID AND SI.numParentItemCode = @numParentItemCode
END
ELSE IF @byteMode=3
BEGIN
SELECT DISTINCT I.vcItemName AS vcItemName,(SELECT TOP 1 vcPathForTImage FROM dbo.ItemImages II WHERE II.numItemCode = I.numItemCode AND II.numDomainId= I.numDomainId AND II.bitDefault=1) As vcPathForTImage ,I.txtItemDesc AS txtItemDesc,  SI.* ,Category.vcCategoryName,
ISNULL(CASE WHEN I.[charItemType]='P' THEN CASE WHEN I.bitSerialized = 1 THEN 1.00000 * monListPrice ELSE 1.00000 * W.[monWListPrice] END ELSE 1.00000 * monListPrice END,0) monListPrice,
dbo.fn_UOMConversion(ISNULL(I.numSaleUnit,0),I.numItemCode,I.numDomainId,ISNULL(I.numBaseUnit,0)) as UOMConversionFactor,I.vcSKU,I.vcManufacturer
, ISNULL(vcUpSellDesc,'') AS [txtUpSellItemDesc],ISNULL(bitPreUpSell,0) [bitPreUpSell], ISNULL(bitPostUpSell,0) [bitPostUpSell], ISNULL(bitRequired,0) [bitRequired],ISNULL(vcUpSellDesc,'') [vcUpSellDesc]
,(SELECT TOP 1 CASE WHEN P.tintDiscountType=1  
				THEN (I.monListPrice*fltDiscountValue)/100
				WHEN P.tintDiscountType=2
				THEN fltDiscountValue
				WHEN P.tintDiscountType=3
				THEN fltDiscountValue*I.monListPrice
				ELSE 0 END
FROM PromotionOffer AS P 
LEFT JOIN CartItems AS C ON P.numProId=C.PromotionID
WHERE C.vcCookieId=@vcCookieId AND C.numUserCntId=@numUserCntID AND 
(I.numItemCode IN (SELECT PIS.numValue FROM PromotionOfferItems AS PIS WHERE PIS.numProId=P.numProId AND PIS.tintType=1 AND PIS.tintRecordType=6) OR
I.numItemClassification IN (SELECT PIS.numValue FROM PromotionOfferItems AS PIS WHERE PIS.numProId=P.numProId AND PIS.tintType=2 AND PIS.tintRecordType=6))
AND P.numDomainId=@numDomainID 
		AND ISNULL(bitEnabled,0)=1 
		AND ISNULL(bitAppliesToSite,0)=1 
		AND 1= (CASE WHEN P.tintOfferTriggerValueType=1 AND C.numUnitHour>=fltOfferTriggerValue THEN 1 WHEN P.tintOfferTriggerValueType=2 AND monTotAmount>=fltOfferTriggerValue THEN 1 ELSE 0 END)
		AND 1 = (CASE WHEN bitRequireCouponCode = 1 THEN CASE WHEN ISNULL(intCouponCodeUsed,0) < ISNULL(tintUsageLimit,0) OR ISNULL(tintUsageLimit,0) = 0  THEN 1 ELSE 0 END ELSE 1 END)
		AND 1 = (CASE WHEN bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN CAST(GETDATE() AS DATE)>=dtValidFrom AND CAST(GETDATE() AS DATE)<=dtValidTo THEN 1 ELSE 0 END) END)
) AS PromotionOffers
FROM 
Category INNER JOIN ItemCategory ON Category.numCategoryID = ItemCategory.numCategoryID INNER JOIN
                      Item I ON ItemCategory.numItemID = I.numItemCode INNER JOIN
                      SimilarItems SI ON I.numItemCode = SI.numItemCode LEFT  JOIN
                       WareHouseItems W ON I.numItemCode = W.numItemID
 WHERE SI.numDomainId = @numDomainID AND SI.numParentItemCode = @numParentItemCode
END
ELSE IF @byteMode=2
BEGIN
	SELECT 
		I.numItemCode,
		I.vcItemName AS vcItemName,
		(SELECT TOP 1 vcPathForTImage FROM dbo.ItemImages II WHERE II.numItemCode = I.numItemCode AND II.numDomainId= I.numDomainId AND II.bitDefault=1) As vcPathForTImage,
		I.txtItemDesc AS txtItemDesc, 
		dbo.fn_GetUOMName(I.numSaleUnit) AS vcUOMName,
		ISNULL(WarehouseItems.numWareHouseItemID,0) AS numWareHouseItemID,
		(CASE WHEN I.charItemType='P' THEN ISNULL(monWListPrice,0) ELSE I.monListPrice END) monListPrice
		,ISNULL(vcRelationship,'') vcRelationship
		,ISNULL(bitPreUpSell,0) [bitPreUpSell]
		,ISNULL(bitPostUpSell,0) [bitPostUpSell]
		,ISNULL(bitRequired,0) [bitRequired]
		,ISNULL(vcUpSellDesc,'') [vcUpSellDesc]
	FROM 
		SimilarItems SI 
	INNER JOIN 
		Item I 
	ON 
		I.numItemCode = SI.numItemCode
	OUTER APPLY
	(
		SELECT TOP 1
			ISNULL(numWareHouseItemID,0) AS numWareHouseItemID,
			monWListPrice
		FROM
			WarehouseItems
		WHERE
			WarehouseItems.numItemID = I.numItemCode
	) AS WarehouseItems
	WHERE 
		SI.numDomainId = @numDomainID AND SI.numParentItemCode = @numParentItemCode
END 

ELSE IF @byteMode=4 -- Pre up sell
BEGIN
DECLARE @numWarehouseID AS NUMERIC(18,0)
SELECT @numWarehouseID=ISNULL(numDefaultWareHouseID,0) FROM eCommerceDTL WHERE numSiteId=@numSiteID

SELECT DISTINCT I.vcItemName AS vcItemName,(SELECT TOP 1 vcPathForTImage FROM dbo.ItemImages II WHERE II.numItemCode = I.numItemCode AND II.numDomainId= I.numDomainId AND II.bitDefault=1) As vcPathForTImage ,I.txtItemDesc AS txtItemDesc,  SI.* ,Category.vcCategoryName,
ISNULL(CASE WHEN I.[charItemType]='P' THEN CASE WHEN I.bitSerialized = 1 THEN 1.00000 * monListPrice ELSE 1.00000 * W.[monWListPrice] END ELSE 1.00000 * monListPrice END,0) monListPrice,
dbo.fn_UOMConversion(ISNULL(I.numSaleUnit,0),I.numItemCode,I.numDomainId,ISNULL(I.numBaseUnit,0)) as UOMConversionFactor,I.vcSKU,I.vcManufacturer
, ISNULL(vcUpSellDesc,'') AS [txtUpSellItemDesc],ISNULL(bitPreUpSell,0) [bitPreUpSell], ISNULL(bitPostUpSell,0) [bitPostUpSell], ISNULL(bitRequired,0) [bitRequired],ISNULL(vcUpSellDesc,'') [vcUpSellDesc],
(SELECT TOP 1 CASE WHEN P.tintDiscountType=1  
				THEN (I.monListPrice*fltDiscountValue)/100
				WHEN P.tintDiscountType=2
				THEN fltDiscountValue
				WHEN P.tintDiscountType=3
				THEN fltDiscountValue*I.monListPrice
				ELSE 0 END
FROM PromotionOffer AS P 
LEFT JOIN CartItems AS C ON P.numProId=C.PromotionID
WHERE C.vcCookieId=@vcCookieId AND C.numUserCntId=@numUserCntID AND 
(I.numItemCode IN (SELECT PIS.numValue FROM PromotionOfferItems AS PIS WHERE PIS.numProId=P.numProId AND PIS.tintType=1 AND PIS.tintRecordType=6) OR
I.numItemClassification IN (SELECT PIS.numValue FROM PromotionOfferItems AS PIS WHERE PIS.numProId=P.numProId AND PIS.tintType=2 AND PIS.tintRecordType=6))
AND P.numDomainId=@numDomainID 
		AND ISNULL(bitEnabled,0)=1 
		AND ISNULL(bitAppliesToSite,0)=1 
		AND 1= (CASE WHEN P.tintOfferTriggerValueType=1 AND C.numUnitHour>=fltOfferTriggerValue THEN 1 WHEN P.tintOfferTriggerValueType=2 AND monTotAmount>=fltOfferTriggerValue THEN 1 ELSE 0 END)
		AND 1 = (CASE WHEN bitRequireCouponCode = 1 THEN CASE WHEN ISNULL(intCouponCodeUsed,0) < ISNULL(tintUsageLimit,0) OR ISNULL(tintUsageLimit,0) = 0  THEN 1 ELSE 0 END ELSE 1 END)
		AND 1 = (CASE WHEN bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN CAST(GETDATE() AS DATE)>=dtValidFrom AND CAST(GETDATE() AS DATE)<=dtValidTo THEN 1 ELSE 0 END) END)
) AS PromotionOffers
FROM 
Category INNER JOIN ItemCategory ON Category.numCategoryID = ItemCategory.numCategoryID INNER JOIN
                      Item I ON ItemCategory.numItemID = I.numItemCode INNER JOIN
                      SimilarItems SI ON I.numItemCode = SI.numItemCode 
                       OUTER APPLY
	(
		SELECT
			TOP 1 *
		FROM
			WareHouseItems
		WHERE
			 numItemID = I.numItemCode
			 AND (numWareHouseID = @numWarehouseID OR @numWarehouseID=0)
	) AS W
 --FROM SimilarItems SI INNER JOIN Item I ON I.numItemCode = SI.numItemCode  INNER JOIN dbo.Category cat ON cat.numCategoryID = I.num
 WHERE SI.numDomainId = @numDomainID AND SI.numParentItemCode = @numParentItemCode AND ISNULL(bitPreUpSell,0)=1
END 

ELSE IF @byteMode=5 -- Post up sell
BEGIN
	SELECT @numWarehouseID=ISNULL(numDefaultWareHouseID,0) FROM eCommerceDTL WHERE numSiteId=@numSiteID

	DECLARE @numPromotionID NUMERIC(18,0) = 0
	DECLARE @vcPromotionDescription VARCHAR(MAX) = ''
	DECLARE @intPostSellDiscount INT=0
	DECLARE @vcPromotionName VARCHAR(100) = ''
	
	SELECT TOP 1 
		@intPostSellDiscount=ISNULL(P.intPostSellDiscount,0)
		,@numPromotionID=C.numPromotionID
		,@vcPromotionDescription= CONCAT('Post sell Discount: ', OM.vcPOppName)
		,@vcPromotionName = P.vcProName 
	FROM 
		OpportunityItems AS C 
	INNER JOIN
		OpportunityMaster OM
	ON
		C.numOppId=OM.numOppId
	LEFT JOIN 
		PromotionOffer AS P 
	ON 
		P.numProId=C.numPromotionID 
	WHERE 
		OM.numDomainId=@numDomainID
		AND OM.numOppID = @numOppID
		AND ISNULL(OM.bitPostSellDiscountUsed,0) = 0
		AND ISNULL(C.numPromotionID,0) > 0
		AND ISNULL(P.bitDisplayPostUpSell,0)=1
		AND ISNULL(P.intPostSellDiscount,0) BETWEEN 1 AND 100

	SELECT DISTINCT 
		I.numItemCode
		,I.vcItemName AS vcItemName
		,(SELECT TOP 1 vcPathForTImage FROM dbo.ItemImages II WHERE II.numItemCode = I.numItemCode AND II.numDomainId= I.numDomainId AND II.bitDefault=1) As vcPathForTImage 
		,I.txtItemDesc AS txtItemDesc
		,I.vcModelID
		,Category.vcCategoryName
		,1 AS numUnitHour
		,ISNULL(CASE WHEN I.[charItemType]='P' THEN [monWListPrice] ELSE monListPrice END,0) monListPrice
		,ISNULL(CASE WHEN I.[charItemType]='P' THEN (monWListPrice-((monWListPrice*(CASE WHEN OI.numPromotionID = @numPromotionID THEN @intPostSellDiscount ELSE 0 END))/100)) ELSE (monListPrice-((monListPrice*(CASE WHEN OI.numPromotionID = @numPromotionID THEN @intPostSellDiscount ELSE 0 END))/100)) END,0) monOfferListPrice
		,ISNULL(W.numWarehouseItemID,0) AS numWarehouseItemID
		,(CASE WHEN OI.numPromotionID = @numPromotionID THEN @intPostSellDiscount ELSE 0 END) AS fltDiscount
		,0 AS bitDiscountType
		,(CASE WHEN OI.numPromotionID = @numPromotionID THEN @numPromotionID ELSE 0 END) AS numPromotionID
		,(CASE WHEN OI.numPromotionID = @numPromotionID THEN @vcPromotionDescription ELSE '' END) AS vcPromotionDescription
		,(CASE WHEN OI.numPromotionID = @numPromotionID THEN @vcPromotionName ELSE '' END) AS vcPromotionName
		,dbo.fn_UOMConversion(ISNULL(I.numSaleUnit,0)
		,I.numItemCode
		,I.numDomainId
		,ISNULL(I.numBaseUnit,0)) as UOMConversionFactor
		,I.vcSKU
		,I.vcManufacturer
		,ISNULL(vcUpSellDesc,'') AS [txtUpSellItemDesc]
		,ISNULL(bitPreUpSell,0) [bitPreUpSell]
		,ISNULL(bitPostUpSell,0) [bitPostUpSell]
		,ISNULL(bitRequired,0) [bitRequired]
		,ISNULL(vcUpSellDesc,'') [vcUpSellDesc]
	FROM 
		Category 
	INNER JOIN 
		ItemCategory 
	ON 
		Category.numCategoryID = ItemCategory.numCategoryID 
	INNER JOIN
        Item I 
	ON 
		ItemCategory.numItemID = I.numItemCode
	INNER JOIN
		OpportunityItems OI
	ON
		OI.numItemCode = I.numItemCode
	INNER JOIN 
		OpportunityMaster OM 
	ON 
		OI.numOppId=OM.numOppId	
	INNER JOIN
        SimilarItems SI 
	ON 
		SI.numParentItemCode=OI.numItemCode
		AND SI.bitPostUpSell = 1
	OUTER APPLY
	(
		SELECT
			TOP 1 *
		FROM
			WareHouseItems
		WHERE
			 numItemID = I.numItemCode
			 AND numWareHouseID = @numWarehouseID
	) AS W
	WHERE 
		SI.numDomainId = @numDomainID 
		AND OM.numDomainID=@numDomainID 
		AND OM.numOppId=@numOppID
		AND 1 = (CASE WHEN charItemType='P' THEN CASE WHEN ISNULL(W.numWareHouseItemID,0) > 0 THEN 1 ELSE 0 END ELSE 1 END)
		
		

	UPDATE OpportunityMaster SET bitPostSellDiscountUsed=1 WHERE numDomainId=@numDomainID AND numOppID = @numOppID
END 

ELSE IF @byteMode=6 -- Pre up sell for E-Commerce
BEGIN
	SELECT @numWarehouseID=ISNULL(numDefaultWareHouseID,0) FROM eCommerceDTL WHERE numSiteId=@numSiteID
	

	SELECT DISTINCT I.vcItemName AS vcItemName
	,(SELECT TOP 1 vcPathForTImage FROM dbo.ItemImages II WHERE II.numItemCode = I.numItemCode AND II.numDomainId= I.numDomainId AND II.bitDefault=1) As vcPathForTImage 
	,I.txtItemDesc AS txtItemDesc, SI.*, Category.vcCategoryName, ISNULL(W.numWareHouseItemID,0) AS numWareHouseItemID, ISNULL(vcRelationship,'') vcRelationship
	,ROUND(ISNULL(CASE WHEN I.[charItemType]='P' THEN CASE WHEN I.bitSerialized = 1 THEN 1.00000 * monListPrice ELSE 1.00000 * W.[monWListPrice] END ELSE 1.00000 * monListPrice END,0), 2) AS monListPrice
	,dbo.fn_UOMConversion(ISNULL(I.numSaleUnit,0), I.numItemCode, I.numDomainId, ISNULL(I.numBaseUnit,0)) as UOMConversionFactor, I.vcSKU, I.vcManufacturer
	,ISNULL(vcUpSellDesc,'') AS [txtUpSellItemDesc], ISNULL(bitPreUpSell,0) [bitPreUpSell], ISNULL(bitPostUpSell,0) [bitPostUpSell]
	,ISNULL(bitRequired,0) [bitRequired], ISNULL(vcUpSellDesc,'') [vcUpSellDesc], dbo.fn_GetUOMName(I.numSaleUnit) AS vcUOMName, ISNULL(numOnHand,0) AS numOnHand
	,(SELECT TOP 1 CASE WHEN P.tintDiscountType=1  
					THEN (I.monListPrice*fltDiscountValue)/100
					WHEN P.tintDiscountType=2
					THEN fltDiscountValue
					WHEN P.tintDiscountType=3
					THEN fltDiscountValue*I.monListPrice
					ELSE 0 END
	FROM PromotionOffer AS P 
	LEFT JOIN CartItems AS C ON P.numProId=C.PromotionID
	WHERE C.vcCookieId=@vcCookieId AND C.numUserCntId=@numUserCntID AND 
	(I.numItemCode IN (SELECT PIS.numValue FROM PromotionOfferItems AS PIS WHERE PIS.numProId=P.numProId AND PIS.tintType=1 AND PIS.tintRecordType=6) OR
	I.numItemClassification IN (SELECT PIS.numValue FROM PromotionOfferItems AS PIS WHERE PIS.numProId=P.numProId AND PIS.tintType=2 AND PIS.tintRecordType=6))
	AND P.numDomainId=@numDomainID 
			AND ISNULL(bitEnabled,0)=1 
			AND ISNULL(bitAppliesToSite,0)=1 
			AND 1= (CASE WHEN P.tintOfferTriggerValueType=1 AND C.numUnitHour>=fltOfferTriggerValue THEN 1 WHEN P.tintOfferTriggerValueType=2 AND monTotAmount>=fltOfferTriggerValue THEN 1 ELSE 0 END)
			AND 1 = (CASE WHEN bitRequireCouponCode = 1 THEN CASE WHEN ISNULL(intCouponCodeUsed,0) < ISNULL(tintUsageLimit,0) OR ISNULL(tintUsageLimit,0) = 0  THEN 1 ELSE 0 END ELSE 1 END)
			AND 1 = (CASE WHEN bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN CAST(GETDATE() AS DATE)>=dtValidFrom AND CAST(GETDATE() AS DATE)<=dtValidTo THEN 1 ELSE 0 END) END)
	) AS PromotionOffers,SI.numParentItemCode
	FROM SimilarItems SI --Category 
	INNER JOIN Item I ON I.numItemCode = SI.numItemCode
	LEFT JOIN ItemCategory ON ItemCategory.numItemID = I.numItemCode 
	LEFT JOIN Category ON Category.numCategoryID = ItemCategory.numCategoryID 
	--INNER JOIN ItemCategory ON Category.numCategoryID = ItemCategory.numCategoryID 
	--INNER JOIN Item I ON ItemCategory.numItemID = I.numItemCode 
	--INNER JOIN SimilarItems SI ON I.numItemCode = SI.numItemCode 
	OUTER APPLY
		(
			SELECT
				TOP 1 *
			FROM
				WareHouseItems
			WHERE
				 numItemID = I.numItemCode
				 AND (numWareHouseID = @numWarehouseID OR @numWarehouseID=0) 
		) AS W
	 WHERE SI.numDomainId = @numDomainID AND SI.numParentItemCode = @numParentItemCode AND ISNULL(bitPreUpSell,0)=1 
END 
ELSE IF @byteMode=7 -- Post up sell for E-Commerce
BEGIN
	SELECT @numWarehouseID=ISNULL(numDefaultWareHouseID,0) FROM eCommerceDTL WHERE numSiteId=@numSiteID

	SELECT DISTINCT I.vcItemName AS vcItemName
	,(SELECT TOP 1 vcPathForTImage FROM dbo.ItemImages II WHERE II.numItemCode = I.numItemCode AND II.numDomainId= I.numDomainId AND II.bitDefault=1) As vcPathForTImage 
	,I.txtItemDesc AS txtItemDesc, SI.*, Category.vcCategoryName, ISNULL(W.numWareHouseItemID,0) AS numWareHouseItemID, ISNULL(vcRelationship,'') vcRelationship
	,ROUND(ISNULL(CASE WHEN I.[charItemType]='P' THEN CASE WHEN I.bitSerialized = 1 THEN 1.00000 * monListPrice ELSE 1.00000 * W.[monWListPrice] END ELSE 1.00000 * monListPrice END,0), 2) AS monListPrice
	,dbo.fn_UOMConversion(ISNULL(I.numSaleUnit,0), I.numItemCode, I.numDomainId, ISNULL(I.numBaseUnit,0)) as UOMConversionFactor, I.vcSKU, I.vcManufacturer
	,ISNULL(vcUpSellDesc,'') AS [txtUpSellItemDesc], ISNULL(bitPreUpSell,0) [bitPreUpSell], ISNULL(bitPostUpSell,0) [bitPostUpSell]
	,ISNULL(bitRequired,0) [bitRequired], ISNULL(vcUpSellDesc,'') [vcUpSellDesc], dbo.fn_GetUOMName(I.numSaleUnit) AS vcUOMName, ISNULL(numOnHand,0) AS numOnHand
	,(SELECT TOP 1 CASE WHEN P.tintDiscountType=1  
					THEN (I.monListPrice*fltDiscountValue)/100
					WHEN P.tintDiscountType=2
					THEN fltDiscountValue
					WHEN P.tintDiscountType=3
					THEN fltDiscountValue*I.monListPrice
					ELSE 0 END
	FROM PromotionOffer AS P 
	LEFT JOIN CartItems AS C ON P.numProId=C.PromotionID
	WHERE C.vcCookieId=@vcCookieId AND C.numUserCntId=@numUserCntID AND 
	(I.numItemCode IN (SELECT PIS.numValue FROM PromotionOfferItems AS PIS WHERE PIS.numProId=P.numProId AND PIS.tintType=1 AND PIS.tintRecordType=6) OR
	I.numItemClassification IN (SELECT PIS.numValue FROM PromotionOfferItems AS PIS WHERE PIS.numProId=P.numProId AND PIS.tintType=2 AND PIS.tintRecordType=6))
	AND P.numDomainId=@numDomainID 
			AND ISNULL(bitEnabled,0)=1 
			AND ISNULL(bitAppliesToSite,0)=1 
			AND 1= (CASE WHEN P.tintOfferTriggerValueType=1 AND C.numUnitHour>=fltOfferTriggerValue THEN 1 WHEN P.tintOfferTriggerValueType=2 AND monTotAmount>=fltOfferTriggerValue THEN 1 ELSE 0 END)
			AND 1 = (CASE WHEN bitRequireCouponCode = 1 THEN CASE WHEN ISNULL(intCouponCodeUsed,0) < ISNULL(tintUsageLimit,0) OR ISNULL(tintUsageLimit,0) = 0  THEN 1 ELSE 0 END ELSE 1 END)
			AND 1 = (CASE WHEN bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN CAST(GETDATE() AS DATE)>=dtValidFrom AND CAST(GETDATE() AS DATE)<=dtValidTo THEN 1 ELSE 0 END) END)
	) AS PromotionOffers, PO.vcProName
	,CONCAT('Buy '
					,CASE WHEN tintOfferTriggerValueType=1 THEN CAST(fltOfferTriggerValue AS VARCHAR) ELSE CONCAT('$',fltOfferTriggerValue) END
					,CASE tintOfferBasedOn
							WHEN 1 THEN CONCAT(' of "',(SELECT Stuff((SELECT CONCAT(', ',Item.vcItemName) FROM PromotionOfferItems INNER JOIN Item ON PromotionOfferItems.numValue = Item.numItemCode WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 FOR XML PATH('')), 1, 2, '')), '"')
							WHEN 2 THEN CONCAT(' of items belonging to classification(s) "',(SELECT Stuff((SELECT CONCAT(', ',ListDetails.vcData) FROM PromotionOfferItems INNER JOIN ListDetails ON PromotionOfferItems.numValue = ListDetails.numListItemID WHERE numListID=36 AND numProId=PO.numProId AND tintRecordType=5 AND tintType=2 FOR XML PATH('')), 1, 2, '')), '"')
						END
					,' & get '
					, CASE tintDiscountType WHEN 1 THEN CONCAT(fltDiscountValue,'% off on') WHEN 2 THEN CONCAT('$',fltDiscountValue, ' off on') WHEN 3 THEN CONCAT(fltDiscountValue,' of') END
					, CASE 
						WHEN tintDiscoutBaseOn = 1 THEN 
							CONCAT
							(
								CONCAT(' "',(SELECT Stuff((SELECT CONCAT(', ',Item.vcItemName) FROM PromotionOfferItems INNER JOIN Item ON PromotionOfferItems.numValue = Item.numItemCode WHERE numProId=PO.numProId AND tintRecordType=6 AND tintType=1 FOR XML PATH('')), 1, 2, '')), '"')
								,(CASE WHEN PO.tintDiscountType = 3 THEN ' free' ELSE '' END)
								,'.'
							)
						WHEN tintDiscoutBaseOn = 2 THEN 
							CONCAT
							(
								CONCAT(' items belonging to classification(s) "',(SELECT Stuff((SELECT CONCAT(', ',ListDetails.vcData) FROM PromotionOfferItems INNER JOIN ListDetails ON PromotionOfferItems.numValue = ListDetails.numListItemID WHERE numListID=36 AND numProId=PO.numProId AND tintRecordType=6 AND tintType=2 FOR XML PATH('')), 1, 2, '')), '"')
								,(CASE WHEN PO.tintDiscountType = 3 THEN ' free' ELSE '' END)
								,'.'
							)
						WHEN tintDiscoutBaseOn = 3 THEN ' related items.'
						ELSE '' 
					END
				) AS vcPromotionRule, PO.numProId, SI.numParentItemCode 
	FROM SimilarItems SI --Category 
	INNER JOIN Item I ON I.numItemCode = SI.numItemCode
	LEFT JOIN ItemCategory ON ItemCategory.numItemID = I.numItemCode 
	LEFT JOIN Category ON Category.numCategoryID = ItemCategory.numCategoryID 
	LEFT JOIN PromotionOfferItems AS POI ON POI.numValue = I.numItemCode
	LEFT JOIN PromotionOffer PO on PO.numProId = POI.numProId 
	OUTER APPLY
		(
			SELECT
				TOP 1 *
			FROM
				WareHouseItems
			WHERE
				 numItemID = I.numItemCode
				 AND (numWareHouseID = @numWarehouseID OR @numWarehouseID=0) 
		) AS W
	 WHERE SI.numDomainId = @numDomainID AND SI.numParentItemCode = @numParentItemCode AND ISNULL(bitPostUpSell,0)=1 
END 

END


GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetSimilarRequiredItem')
-- exec USP_GetSimilarRequiredItem 72,4648
DROP PROCEDURE USP_GetSimilarRequiredItem 
GO
CREATE PROCEDURE USP_GetSimilarRequiredItem 
 @numDomainID NUMERIC(9),
 @numParentItemCode NUMERIC(9)
AS 
BEGIN
 
 --SELECT * FROM SimilarItems WHERE numParentItemCode = @numParentItemCode AND bitRequired = 1
 SELECT 
  I.numItemCode
  ,I.txtItemDesc
  ,I.numSaleUnit
  ,ISNULL(I.numContainer,0) AS numContainer
  ,ISNULL(I.bitAllowDropShip,0) AS bitAllowDropShip
  ,dbo.fn_GetUOMName(I.numSaleUnit) AS vcUOMName
  ,ISNULL(WarehouseItems.numWareHouseItemID,0) AS numWareHouseItemID
  ,I.charItemType
  ,dbo.fn_UOMConversion(I.numSaleUnit,I.numItemCode,@numDomainId,I.numBaseUnit) AS fltUOMConversionFactor
 FROM 
  SimilarItems SI 
 INNER JOIN 
  Item I 
 ON 
  I.numItemCode = SI.numItemCode
 OUTER APPLY
 (
  SELECT TOP 1
   numWareHouseItemID,
   monWListPrice
  FROM
   WarehouseItems
  WHERE
   WarehouseItems.numItemID = I.numItemCode
 ) AS WarehouseItems
 WHERE 
  SI.numDomainId = @numDomainID AND SI.numParentItemCode = @numParentItemCode AND bitRequired = 1
END

/****** Object:  StoredProcedure [dbo].[USP_InsertCartItem]    Script Date: 11/08/2011 17:53:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_InsertCartItem' ) 
                    DROP PROCEDURE USP_InsertCartItem
                    Go
CREATE  PROCEDURE [dbo].[USP_InsertCartItem]
(
    @numCartId AS NUMERIC OUTPUT,
    @numUserCntId NUMERIC(18, 0),
    @numDomainId NUMERIC(18, 0),
    @vcCookieId VARCHAR(100),
    @numOppItemCode NUMERIC(18, 0),
    @numItemCode NUMERIC(18, 0),
    @numUnitHour NUMERIC(18, 0),
    @monPrice MONEY,
    @numSourceId NUMERIC(18, 0),
    @vcItemDesc VARCHAR(2000),
    @numWarehouseId NUMERIC(18, 0),
    @vcItemName VARCHAR(200),
    @vcWarehouse VARCHAR(200),
    @numWarehouseItmsID NUMERIC(18, 0),
    @vcItemType VARCHAR(200),
    @vcAttributes VARCHAR(100),
    @vcAttrValues VARCHAR(100),
    @bitFreeShipping BIT,
    @numWeight NUMERIC(18, 2),
    @tintOpFlag TINYINT,
    @bitDiscountType BIT,
    @fltDiscount DECIMAL(18,2),
    @monTotAmtBefDiscount MONEY,
    @ItemURL VARCHAR(200),
    @numUOM NUMERIC(18, 0),
    @vcUOMName VARCHAR(200),
    @decUOMConversionFactor DECIMAL(18, 5),
    @numHeight NUMERIC(18, 0),
    @numLength NUMERIC(18, 0),
    @numWidth NUMERIC(18, 0),
    @vcShippingMethod VARCHAR(200),
    @numServiceTypeId NUMERIC(18, 0),
    @decShippingCharge DECIMAL(18, 2),
    @numShippingCompany NUMERIC(18, 0),
    @tintServicetype TINYINT,
    @dtDeliveryDate DATETIME,
    @monTotAmount money,
	@postselldiscount INT=0,
	@numSiteID NUMERIC(18,0) = 0,
	@numPromotionID NUMERIC(18,0) = 0,
	@vcPromotionDescription VARCHAR(MAX) = '',
	@numParentItemCode NUMERIC(18,0) = 0
)
AS 
BEGIN TRY
BEGIN TRANSACTION;
	
	INSERT INTO [dbo].[CartItems]
    (
        [numUserCntId],
        [numDomainId],
        [vcCookieId],
        [numOppItemCode],
        [numItemCode],
        [numUnitHour],
        [monPrice],
        [numSourceId],
        [vcItemDesc],
        [numWarehouseId],
        [vcItemName],
        [vcWarehouse],
        [numWarehouseItmsID],
        [vcItemType],
        [vcAttributes],
        [vcAttrValues],
        [bitFreeShipping],
        [numWeight],
        [tintOpFlag],
        [bitDiscountType],
        [fltDiscount],
        [monTotAmtBefDiscount],
        [ItemURL],
        [numUOM],
        [vcUOMName],
        [decUOMConversionFactor],
        [numHeight],
        [numLength],
        [numWidth],
        [vcShippingMethod],
        [numServiceTypeId],
        [decShippingCharge],
        [numShippingCompany],
        [tintServicetype],
        [monTotAmount],
		[PromotionID],
		[PromotionDesc],
		numParentItem
    )
	VALUES  
	(
        @numUserCntId,
        @numDomainId,
        @vcCookieId,
        @numOppItemCode,
        @numItemCode,
        @numUnitHour,
        @monPrice,
        @numSourceId,
        @vcItemDesc,
        @numWarehouseId,
        @vcItemName,
        @vcWarehouse,
        @numWarehouseItmsID,
        @vcItemType,
        @vcAttributes,
        @vcAttrValues,
        @bitFreeShipping,
        @numWeight,
        @tintOpFlag,
        @bitDiscountType,
        @fltDiscount,
        @monTotAmtBefDiscount,
        @ItemURL,
        @numUOM,
        @vcUOMName,
        @decUOMConversionFactor,
        @numHeight,
        @numLength,
        @numWidth,
        @vcShippingMethod,
        @numServiceTypeId,
        @decShippingCharge,
        @numShippingCompany,
        @tintServicetype,
        @monTotAmount,
		@numPromotionID,
		@vcPromotionDescription,
		@numParentItemCode
    )

	SET @numCartId = SCOPE_IDENTITY()

	DECLARE @numItemClassification As NUMERIC(18,0)

	SELECT 
		@numItemClassification=numItemClassification
	FROM
		Item
	WHERE
		numItemCode=@numItemCode


	IF(@postselldiscount=0)
	BEGIN

		/************************** First check if any promotion can be applied *********************************/
		DECLARE @TEMPPromotion TABLE
		(
			ID INT IDENTITY(1,1),
			numPromotionID NUMERIC(18,0),
			vcPromotionDescription VARCHAR(MAX)
		)

		INSERT INTO 
			@TEMPPromotion
		SELECT
			CI.PromotionID,
			CI.PromotionDesc
		FROM 
			CartItems CI
		INNER JOIN
			PromotionOffer PO
		ON
			CI.PromotionID = PO.numProId
		WHERE
			CI.numDomainId=@numDomainId
			AND CI.numUserCntId=@numUserCntId
			AND CI.vcCookieId=@vcCookieId
			AND ISNULL(CI.bitParentPromotion,0)=1
			AND ISNULL(CI.PromotionID,0) > 0
			AND ISNULL(bitEnabled,0)=1
			AND 1=(CASE PO.tintDiscoutBaseOn
					 WHEN 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems POI WHERE POI.numProId=PO.numProId AND POI.numValue=@numItemCode AND tintRecordType=6 AND tintType=1) > 0 THEN 1 ELSE 0 END)
					 WHEN 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems POI WHERE POI.numProId=PO.numProId AND POI.numValue=@numItemClassification AND tintRecordType=6 AND tintType=2) > 0 THEN 1 ELSE 0 END)
					 WHEN 3 THEN (CASE PO.tintOfferBasedOn 
									WHEN 1 THEN (CASE WHEN (SELECT COUNT(*) FROM SimilarItems WHERE numItemCode=@numItemCode AND numParentItemCode IN (SELECT numValue FROM PromotionOfferItems POI WHERE POI.numProId=PO.numProId AND tintRecordType=5 AND tintType=1)) > 0 THEN 1 ELSE 0 END)
									WHEN 2 THEN (CASE WHEN (SELECT COUNT(*) FROM SimilarItems WHERE numItemCode=@numItemCode AND numParentItemCode IN (SELECT numItemCode FROM Item WHERE numDomainID=@numDomainId AND numItemClassification IN (SELECT numValue FROM PromotionOfferItems POI WHERE POI.numProId=PO.numProId AND tintRecordType=5 AND tintType=2))) > 0 THEN 1 ELSE 0 END)
									ELSE 0
								END) 
					ELSE 0
				END)
		ORDER BY
			numCartId ASC

		DECLARE @fltDiscountValue FLOAT
		DECLARE @tintDiscountType INT
		DECLARE @tintDiscoutBaseOn INT
		DECLARE @IsPromotionApplied BIT = 0

		DECLARE @Count AS INT
		SELECT @Count=COUNT(*) FROM @TEMPPromotion

		IF @Count > 0
		BEGIN
			DECLARE @i AS INT = 1
		
			WHILE @i <= @Count
			BEGIN
				SELECT @numPromotionID=numPromotionID,@vcPromotionDescription=vcPromotionDescription FROM @TEMPPromotion WHERE ID=@i

				SELECT
					@tintDiscountType=tintDiscountType,
					@fltDiscountValue=fltDiscountValue
				FROM
					PromotionOffer 
				WHERE
					numProId=@numPromotionID
			
				IF @tintDiscountType=1 --Percentage
				BEGIN
					UPDATE 
						CartItems
					SET
						PromotionID=@numPromotionID,
						PromotionDesc=@vcPromotionDescription,
						bitParentPromotion=0,
						fltDiscount=@fltDiscountValue,
						bitDiscountType=0,
						monTotAmount=(monTotAmount-((monTotAmount*@fltDiscountValue)/100))
					WHERE
						numCartId=@numCartId

					SET @IsPromotionApplied = 1
					BREAK
				END
				ELSE IF @tintDiscountType=2 --Flat Amount
				BEGIN
					-- FIRST CHECK WHETHER DISCOUNT AMOUNT IS NOT ALREADY USED BY OTHER ITEMS
					DECLARE @usedDiscountAmount AS FLOAT
				
					SELECT
						@usedDiscountAmount = SUM(fltDiscount)
					FROM
						CartItems
					WHERE
						PromotionID=@numPromotionID

					IF @usedDiscountAmount < @fltDiscountValue
					BEGIN
						UPDATE 
							CartItems 
						SET 
							monTotAmount=(CASE WHEN (@fltDiscountValue - @usedDiscountAmount)  >= monTotAmount THEN 0 ELSE (monTotAmount-(@fltDiscountValue - @usedDiscountAmount)) END),
							PromotionID=@numPromotionID ,
							PromotionDesc=@vcPromotionDescription,
							bitDiscountType=1,
							fltDiscount=(CASE WHEN (@fltDiscountValue - @usedDiscountAmount) >= monTotAmount THEN monTotAmount ELSE (@fltDiscountValue - @usedDiscountAmount) END)
						WHERE 
							numCartId=@numCartId

						SET @IsPromotionApplied = 1
						BREAK
					END
				END
				ELSE IF @tintDiscountType=3 --Quantity
				BEGIN
					-- FIRST CHECK WHETHER DISCOUNT QTY IS NOT ALREADY USED BY OTHER ITEMS
					DECLARE @usedDiscountQty AS FLOAT

					SELECT
						@usedDiscountQty = SUM(fltDiscount/monPrice)
					FROM
						CartItems
					WHERE
						PromotionID=@numPromotionID

					IF @usedDiscountQty < @fltDiscountValue
					BEGIN
						UPDATE 
							CartItems 
						SET 
							monTotAmount=(CASE WHEN (@fltDiscountValue - @usedDiscountQty)  >= numUnitHour THEN 0 ELSE (monTotAmount-((@fltDiscountValue - @usedDiscountQty)*monPrice)) END),
							PromotionID=@numPromotionID ,
							PromotionDesc=@vcPromotionDescription,
							bitDiscountType=1,
							fltDiscount=(CASE WHEN (@fltDiscountValue - @usedDiscountQty) >= numUnitHour THEN (numUnitHour*monPrice) ELSE ((@fltDiscountValue - @usedDiscountQty) *monPrice) END) 
						WHERE 
							numCartId=@numCartId

						SET @IsPromotionApplied = 1
						BREAK
					END

				END

				SET @i = @i + 1
			END
		END


		IF @IsPromotionApplied = 0
		BEGIN
			/************************** Check if any promotion can be trigerred *********************************/
			EXEC USP_ECommerceTiggerPromotion @numDomainID,@numUserCntId,@vcCookieId,@numCartID,@numItemCode,@numItemClassification,@numUnitHour,@monPrice,@numSiteID
		
		END

	END


	SELECT @numCartId

 COMMIT TRANSACTION;
  END TRY
  BEGIN CATCH
    IF @@TRANCOUNT > 0
    ROLLBACK TRANSACTION;

    DECLARE @ErrorNumber INT = ERROR_NUMBER();
    DECLARE @ErrorLine INT = ERROR_LINE();
    DECLARE @ErrorMessage NVARCHAR(4000) = ERROR_MESSAGE();
    DECLARE @ErrorSeverity INT = ERROR_SEVERITY();
    DECLARE @ErrorState INT = ERROR_STATE();

    PRINT 'Actual error number: ' + CAST(@ErrorNumber AS VARCHAR(10));
    PRINT 'Actual line number: ' + CAST(@ErrorLine AS VARCHAR(10));

    RAISERROR(@ErrorMessage, @ErrorSeverity, @ErrorState);
  END CATCH
            

GO
/****** Object:  StoredProcedure [dbo].[usp_ManageAddlContInfo]    Script Date: 04/02/2009 00:17:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Modified By Anoop jayaraj                  
 GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_manageaddlcontinfo')
DROP PROCEDURE usp_manageaddlcontinfo
GO
CREATE PROCEDURE [dbo].[usp_ManageAddlContInfo]                                    
 @numcontactId numeric=0,                                    
 @numContactType numeric=0,                                    
 @vcDepartment numeric(9)=0,                                    
 @vcCategory numeric(9)=0,                                    
 @vcGivenName varchar(100)='',                                    
 @vcFirstName varchar(50)='',                                    
 @vcLastName varchar(50)='',                                    
 @numDivisionId numeric,                                    
 @numPhone varchar(15)='',                                    
 @numPhoneExtension varchar(7)='',                                    
 @numCell varchar(15)='',                                    
 @NumHomePhone varchar(15)='',                                    
 @vcFax varchar(15)='',                                    
 @vcEmail varchar(50)='',                                    
 @VcAsstFirstName varchar(50)='',                                    
 @vcAsstLastName varchar(50)='',                                    
 @numAsstPhone varchar(15)='',                                    
 @numAsstExtn varchar(6)='',                                    
 @vcAsstEmail varchar(50)='',                                                      
 @charSex char(1)='',                                    
 @bintDOB datetime,                                    
 @vcPosition numeric(9)=0,                                                     
 @txtNotes text='',                                                     
 @numUserCntID numeric,                                                                                              
 @numDomainID numeric=1,                                                   
 @vcPStreet varchar(100)='',                                    
 @vcPCity varchar(50)='',                                    
 @vcPPostalCode varchar(15)='',                                    
 @vcPState numeric(9)=0,                                    
 @vcPCountry numeric(9)=0,                  
 @numManagerID numeric=0,        
 @numTeam as numeric(9)=0,        
 @numEmpStatus as numeric(9)=0,
 @vcTitle AS VARCHAR(100)='',
 @bitPrimaryContact AS BIT=0,
 @bitOptOut AS BIT=0,
 @vcLinkedinId VARCHAR(30)=NULL,
 @vcLinkedinUrl VARCHAR(300)=NULL,
 @numECampaignID NUMERIC(18)=0,
 @vcPassword VARCHAR(50) = '',
 @vcTaxID VARCHAR(100) = ''
AS   
	If LEN(ISNULL(@vcFirstName,'')) = 0
	BEGIN
		SET @vcFirstName = '-'
	END

	If LEN(ISNULL(@vcLastName,'')) = 0
	BEGIN
		SET @vcLastName = '-'
	END

 SELECT @vcGivenName = @vcLastName + ', ' + @vcFirstName  + '.eml'                                 
   if @bintDOB ='Jan  1 1753 12:00AM' set  @bintDOB=null                      
IF @numContactId=0
     BEGIN   

IF @bitPrimaryContact = 1 
    BEGIN            
        DECLARE @numID AS NUMERIC(9)            
        DECLARE @bitPrimaryContactCheck AS BIT             
             
        SELECT TOP 1
                @numID = numContactID,
                @bitPrimaryContactCheck = ISNULL(bitPrimaryContact,0)
        FROM    AdditionalContactsInformation
        WHERE   numDivisionID = @numDivisionID 
                   
        IF @@rowcount = 0 
            SET @numID = 0             
        WHILE @numID > 0            
            BEGIN            
                IF @bitPrimaryContactCheck = 1 
                    UPDATE  AdditionalContactsInformation
                    SET     bitPrimaryContact = 0
                    WHERE   numContactID = @numID  
                              
                SELECT TOP 1
                        @numID = numContactID,
                        @bitPrimaryContactCheck = ISNULL(bitPrimaryContact,0)
                FROM    AdditionalContactsInformation
                WHERE   numDivisionID = @numDivisionID
                        AND numContactID > @numID    
                                
                IF @@rowcount = 0 
                    SET @numID = 0             
            END            
    END  
    ELSE IF @bitPrimaryContact = 0
    BEGIN
		IF (SELECT COUNT(*) FROM AdditionalContactsInformation WHERE numDivisionID = @numDivisionID AND numContactId!=@numContactId AND ISNULL(bitPrimaryContact,0)=1)=0
			SET @bitPrimaryContact = 1
    END                 
    
    
declare @bitAutoPopulateAddress bit 
declare @tintPoulateAddressTo tinyint 

select @bitAutoPopulateAddress= isnull(bitAutoPopulateAddress,'0'),@tintPoulateAddressTo=isnull(tintPoulateAddressTo,'0') from Domain where numDomainId = @numDomainId ;
if (@bitAutoPopulateAddress = 1)
	if(@tintPoulateAddressTo = 1) --if primary address is not specified then getting billing address    
	begin
		 
		  select                   
			@vcPStreet = AD1.vcStreet,
			@vcPCity= AD1.vcCity,
			@vcPState= AD1.numState,
			@vcPPostalCode= AD1.vcPostalCode,
			@vcPCountry= AD1.numCountry              
		  from divisionMaster  DM              
		   LEFT JOIN dbo.AddressDetails AD1 ON AD1.numDomainID=DM.numDomainID 
			AND AD1.numRecordID=DM.numDivisionID AND AD1.tintAddressOf=2 AND AD1.tintAddressType=1 AND AD1.bitIsPrimary=1
		  where numDivisionID=@numDivisionId    
	end  
	else if (@tintPoulateAddressTo = 2)-- Primary Address is Shipping Address
	begin
		  select                   
			@vcPStreet= AD2.vcStreet,
			@vcPCity=AD2.vcCity,
			@vcPState=AD2.numState,
			@vcPPostalCode=AD2.vcPostalCode,
			@vcPCountry=AD2.numCountry                   
		  from divisionMaster DM
		  LEFT JOIN dbo.AddressDetails AD2 ON AD2.numDomainID=DM.numDomainID 
		  AND AD2.numRecordID= DM.numDivisionID AND AD2.tintAddressOf=2 AND AD2.tintAddressType=2 AND AD2.bitIsPrimary=1
		  where numDivisionID=@numDivisionId    
	
	end                       
                       
 if (@numEmpStatus is null or @numEmpStatus=0) set @numEmpStatus=658        
 
    INSERT into AdditionalContactsInformation                                    
    (                  
   numContactType,                  
   vcDepartment,                  
   vcCategory,                  
   vcGivenName,                  
   vcFirstName,                  
   vcLastName,                   
   numDivisionId ,                  
   numPhone ,                  
   numPhoneExtension,                  
   numCell ,                  
   numHomePhone ,                          
   vcFax ,                  
   vcEmail ,                  
   VcAsstFirstName ,                  
   vcAsstLastName ,                  
   numAsstPhone ,                  
   numAsstExtn ,                  
   vcAsstEmail  ,                  
   charSex ,                  
   bintDOB ,                  
   vcPosition ,                  
   txtNotes ,                  
   numCreatedBy ,                                    
   bintCreatedDate,             
   numModifiedBy,            
   bintModifiedDate,                   
   numDomainID,                  
   numManagerID,                   
   numRecOwner,                  
   numTeam,                  
   numEmpStatus,
   vcTitle,bitPrimaryContact,bitOptOut,
   vcLinkedinId,vcLinkedinUrl,numECampaignID,vcTaxID           
   )                                    
    VALUES                                    
      (                  
   @numContactType,                  
   @vcDepartment,                  
   @vcCategory,                  
   @vcGivenName ,                  
   @vcFirstName ,                  
   @vcLastName,                   
   @numDivisionId ,                  
   @numPhone ,                                    
   @numPhoneExtension,                  
   @numCell ,                  
   @NumHomePhone ,                  
   @vcFax ,                  
   @vcEmail ,                  
   @VcAsstFirstName,                         
   @vcAsstLastName,                  
   @numAsstPhone,                  
   @numAsstExtn,                  
   @vcAsstEmail,                  
   @charSex,                  
   @bintDOB ,                  
   @vcPosition,                                   
   @txtNotes,                  
   @numUserCntID,                  
   getutcdate(),             
   @numUserCntID,                  
   getutcdate(),                   
   @numDomainID,                   
   @numManagerID,                   
   @numUserCntID,                   
   @numTeam,                  
   @numEmpStatus,
   @vcTitle,@bitPrimaryContact,@bitOptOut,
   @vcLinkedinId,@vcLinkedinUrl,@numECampaignID,@vcTaxID           
   )                                    
                     
 set @numcontactId= SCOPE_IDENTITY()
 
 SELECT @numcontactId             

     INSERT INTO dbo.AddressDetails (
	 	vcAddressName,
	 	vcStreet,
	 	vcCity,
	 	vcPostalCode,
	 	numState,
	 	numCountry,
	 	bitIsPrimary,
	 	tintAddressOf,
	 	tintAddressType,
	 	numRecordID,
	 	numDomainID
	 ) VALUES ('Primary', @vcPStreet,@vcPCity,@vcPPostalCode,@vcPState,@vcPCountry,1,1,0,@numcontactId,@numDomainID)
     
	 IF ISNULL(@numECampaignID,0) > 0 
	 BEGIN
		  --Manage ECampiagn When Campaign is changed from inline edit
		DECLARE @Date AS DATETIME 
		SELECT @Date = dbo.[GetUTCDateWithoutTime]()

 		EXEC [USP_ManageConEmailCampaign]
		@numConEmailCampID = 0, --  numeric(9, 0)
		@numContactID = @numcontactId, --  numeric(9, 0)
		@numECampaignID = @numECampaignID, --  numeric(9, 0)
		@intStartDate = @Date, --  datetime
		@bitEngaged = 1, --  bit
		@numUserCntID = @numUserCntID --  numeric(9, 0)     
	END                                         
                                                 
  END                                    
 ELSE if @numContactId>0                                                        
    BEGIN                                    
    UPDATE AdditionalContactsInformation SET                                    
      numContactType=@numContactType,                                    
      vcGivenName=@vcGivenName,                                    
      vcFirstName=@vcFirstName ,                                    
      vcLastName =@vcLastName ,                   
      numDivisionId =@numDivisionId ,                                    
      numPhone=@numPhone ,                                    
      numPhoneExtension=@numPhoneExtension,                                    
      numCell =@numCell ,                                    
      NumHomePhone =@NumHomePhone ,                                    
      vcFax=@vcFax ,                                    
      vcEmail=@vcEmail ,                                    
      VcAsstFirstName=@VcAsstFirstName,                                    
      vcAsstLastName=@vcAsstLastName,                                    
      numAsstPhone=@numAsstPhone,                                    
      numAsstExtn=@numAsstExtn,                                    
      vcAsstEmail=@vcAsstEmail,                                                      
      charSex=@charSex,                            
      bintDOB=@bintDOB ,                                    
      vcPosition=@vcPosition,                                                    
      txtNotes=@txtNotes,                                                     
      numModifiedBy=@numUserCntID,                                    
      bintModifiedDate=getutcdate(),                                    
      numManagerID=@numManagerID,
      bitPrimaryContact=@bitPrimaryContact                              
     WHERE numContactId=@numContactId   
                    
     Update dbo.AddressDetails set                   
       vcStreet=@vcPStreet,                                    
       vcCity =@vcPCity,                                    
       vcPostalCode=@vcPPostalCode,                                     
       numState=@vcPState,                                    
       numCountry=@vcPCountry
	  where numRecordID=@numContactId AND bitIsPrimary=1 AND tintAddressOf=1 AND tintAddressType=0
 
     SELECT @numcontactId   
                                                
  END    
  ------Check the Email id Exist into the Email Master Table

	IF @vcPassword IS NOT NULL AND  LEN(@vcPassword) > 0
	BEGIN
		IF EXISTS (SELECT numExtranetID FROM ExtarnetAccounts WHERE numDivisionID = @numDivisionID)
		BEGIN
			IF EXISTS (SELECT numExtranetDtlID FROM ExtranetAccountsDtl WHERE numContactID=@numContactID)
			BEGIN
				UPDATE
					ExtranetAccountsDtl
				SET 
					vcPassword=@vcPassword
					,tintAccessAllowed=1
				WHERE
					numContactID=@numContactID
			END
			ELSE
			BEGIN
				DECLARE @numExtranetID NUMERIC(18,0)
				SELECT @numExtranetID=numExtranetID FROM ExtarnetAccounts WHERE numDivisionID = @numDivisionID

				INSERT INTO ExtranetAccountsDtl (numExtranetID,numContactID,vcPassword,tintAccessAllowed,numDomainID)
				VALUES (@numExtranetID,@numContactID,@vcPassword,1,@numDomainID)
			END
		END
		ELSE
		BEGIN
			RAISERROR('Organization e-commerce access is not enabled',16,1)
		END
	END



DECLARE @numCount AS NUMERIC(18,0)
SET @numCount = 0
IF @vcEmail <> ''
BEGIN 
SELECT @numCount = COUNT(*) FROM dbo.EmailMaster WHERE vcEmailID =@vcEmail and numDomainId=@numDomainID
IF @numCount = 0 
BEGIN
	INSERT INTO EmailMaster(vcEmailId,vcName,numDomainId,numContactId)
	VALUES(@vcEmail,@vcFirstName,@numDomainID,@numcontactId)
END
else
begin
	update EmailMaster set numContactId=@numcontactId where vcEmailID =@vcEmail and numDomainId=@numDomainID
end
END 
--IF @vcAsstEmail <> ''
--BEGIN
--SELECT @numCount = COUNT(*) FROM dbo.EmailMaster WHERE vcEmailID =@vcAsstEmail
--IF @numCount = 0 
--BEGIN
--	INSERT INTO EmailMaster(vcEmailId,vcName,numDomainId,numContactId)
--	VALUES(@vcAsstEmail,@vcFirstName,@numDomainID,@numcontactId)
--END
--END
---------------------------------------------------------

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManagePromotionOffer')
DROP PROCEDURE USP_ManagePromotionOffer
GO
CREATE PROCEDURE [dbo].[USP_ManagePromotionOffer]
    @numProId AS NUMERIC(9) = 0,
    @vcProName AS VARCHAR(100),
    @numDomainId AS NUMERIC(18,0),
    @dtValidFrom AS DATE,
    @dtValidTo AS DATE,
	@bitNeverExpires BIT,
	@bitApplyToInternalOrders BIT,
	@bitAppliesToSite BIT,
	@bitRequireCouponCode BIT,
	@txtCouponCode VARCHAR(100),
	@tintUsageLimit TINYINT,
	--@bitFreeShiping BIT,
	--@monFreeShippingOrderAmount MONEY,
	--@numFreeShippingCountry NUMERIC(18,0),
	--@bitFixShipping1 BIT,
	--@monFixShipping1OrderAmount MONEY,
	--@monFixShipping1Charge MONEY,
	--@bitFixShipping2 BIT,
	--@monFixShipping2OrderAmount MONEY,
	--@monFixShipping2Charge MONEY,
	@bitDisplayPostUpSell BIT,
	@intPostSellDiscount INT,
	@tintOfferTriggerValueType TINYINT,
	@fltOfferTriggerValue INT,
	@tintOfferBasedOn TINYINT,
	@tintDiscountType TINYINT,
	@fltDiscountValue FLOAT,
	@tintDiscoutBaseOn TINYINT,
	@numUserCntID NUMERIC(18,0),
	@tintCustomersBasedOn TINYINT,
	@tintOfferTriggerValueTypeRange TINYINT,
	@fltOfferTriggerValueRange FLOAT
AS 
BEGIN
	IF @numProId = 0 
	BEGIN
		INSERT INTO PromotionOffer
        (
            vcProName
			,numDomainId
			, numCreatedBy
			,dtCreated
			,bitEnabled
        )
        VALUES  
		(
			@vcProName
           ,@numDomainId
           ,@numUserCntID
		   ,GETUTCDATE()
		   ,1
        )
            
		SELECT SCOPE_IDENTITY()
	END
	ELSE
	BEGIN
		IF ISNULL(@bitRequireCouponCode,0)=1 AND (SELECT COUNT(*) FROM PromotionOffer WHERE txtCouponCode=@txtCouponCode AND numDomainId=@numDomainId AND numProId<>@numProId)>0
		BEGIN
 			RAISERROR ( 'DUPLICATE_COUPON_CODE',16, 1 )
 			RETURN ;
		END

		IF ISNULL(@bitAppliesToSite,0)=1 AND ISNULL(@numProId,0) > 0 AND (SELECT COUNT(*) FROM PromotionOfferSites WHERE numPromotionID=@numProId) = 0
		BEGIN
			RAISERROR ( 'SELECT_SITES',16, 1 )
 			RETURN;
		END

		IF @tintOfferBasedOn <> 4
		BEGIN
			IF (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numProId AND tintType=ISNULL(@tintOfferBasedOn,0) AND tintRecordType=5) = 0
			BEGIN
				RAISERROR ( 'SELECT_ITEMS_FOR_PROMOTIONS',16, 1 )
 				RETURN;
			END
		END

		IF ISNULL(@tintDiscoutBaseOn,0) <> 3 AND (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numProId AND tintType=ISNULL(@tintDiscoutBaseOn,0) AND tintRecordType=6) = 0
		BEGIN
			RAISERROR ( 'SELECT_DISCOUNTED_ITEMS',16, 1 )
 			RETURN;
		END

		/*Check For Duplicate Customer-Item relationship 
			1- The customer is specific (level 1), and the item is specific (also level 1). If the user tries to create another rule that targets the customer by name, or that same item by name, validation would say �You�ve already created a promotion rule that targets this customer and item by name�*/
		IF @tintCustomersBasedOn = 1
		BEGIN
			IF ISNULL(@tintCustomersBasedOn,0) = 1 AND (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId = @numProId AND tintType = 1) = 0
			BEGIN
				RAISERROR ( 'SELECT_INDIVIDUAL_CUSTOMER',16, 1 )
 				RETURN;
			END
		END
			
			/* 2- The customer is targeted at the group level (level 2), and the item is specific (level 1). If the user tries to create another rule that targets this same Relationship / Profile combination and that same item in level 1 (L2 customer and L1 item both match), validation would say �You�ve already created a promotion rule that targets this relationship / profile and one or more item specifically�*/
		ELSE IF @tintCustomersBasedOn = 2
		BEGIN
			IF ISNULL(@tintCustomersBasedOn,0) = 2 AND (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId = @numProId AND tintType = 2) = 0
			BEGIN
				RAISERROR ( 'SELECT_RELATIONSHIP_PROFILE',16, 1 )
 				RETURN;
			END
		END

		ELSE IF @tintCustomersBasedOn = 0
		BEGIN
			RAISERROR ( 'SELECT_CUSTOMERS',16, 1 )
 			RETURN;
		END

		IF @tintCustomersBasedOn = 3
		BEGIN
			DELETE FROM PromotionOfferorganizations 
				WHERE tintType IN (1, 2) 
					AND numProId = @numProId
		END


		IF (
			SELECT
				COUNT(*)
			FROM
				PromotionOffer
			LEFT JOIN
				PromotionOfferOrganizations
			ON
				PromotionOffer.numProId = PromotionOfferOrganizations.numProId
				AND 1 = (CASE 
							WHEN @tintCustomersBasedOn=1 THEN (CASE WHEN PromotionOfferOrganizations.tintType=1 THEN 1 ELSE 0 END) 
							WHEN @tintCustomersBasedOn=2 THEN (CASE WHEN PromotionOfferOrganizations.tintType=2 THEN 1 ELSE 0 END) 
							ELSE 1
						END)
			LEFT JOIN
				PromotionOfferItems
			ON
				PromotionOffer.numProId = PromotionOfferItems.numProId
				AND 1 = (CASE 
							WHEN @tintOfferBasedOn=1 THEN (CASE WHEN PromotionOfferItems.tintType=1 THEN 1 ELSE 0 END) 
							WHEN @tintOfferBasedOn=2 THEN (CASE WHEN PromotionOfferItems.tintType=2 THEN 1 ELSE 0 END) 
							ELSE 1
						END)
				AND PromotionOfferItems.tintRecordType = 5
			LEFT JOIN
				PromotionOfferItems PromotionOfferItemsDiscount
			ON
				PromotionOffer.numProId = PromotionOfferItemsDiscount.numProId
				AND 1 = (CASE 
							WHEN @tintDiscoutBaseOn=1 THEN (CASE WHEN PromotionOfferItemsDiscount.tintType=1 THEN 1 ELSE 0 END) 
							WHEN @tintDiscoutBaseOn=2 THEN (CASE WHEN PromotionOfferItemsDiscount.tintType=2 THEN 1 ELSE 0 END) 
							ELSE 1
						END)
				AND PromotionOfferItemsDiscount.tintRecordType = 6
			WHERE
				PromotionOffer.numDomainId=@numDomainID
				AND PromotionOffer.numProId <> @numProId
				AND tintCustomersBasedOn = @tintCustomersBasedOn
				AND tintDiscoutBaseOn = @tintDiscoutBaseOn
				AND tintOfferBasedOn = @tintOfferBasedOn
				AND tintOfferTriggerValueType = @tintOfferTriggerValueType
				AND 1 = (CASE 
							WHEN tintOfferTriggerValueTypeRange = 1 --FIXED
							THEN
								(CASE 
									WHEN @tintOfferTriggerValueTypeRange = 1 THEN 
										(CASE WHEN fltOfferTriggerValue = @fltOfferTriggerValue THEN 1 ELSE 0 END)
									WHEN @tintOfferTriggerValueTypeRange = 2 THEN 
										(CASE WHEN fltOfferTriggerValue BETWEEN @fltOfferTriggerValue AND @fltOfferTriggerValueRange THEN 1 ELSE 0 END)
									ELSE 0 
								END)
							WHEN tintOfferTriggerValueTypeRange = 2 --BETWEEN
							THEN
								(CASE 
									WHEN @tintOfferTriggerValueTypeRange = 1 THEN 
										(CASE WHEN @fltOfferTriggerValue BETWEEN fltOfferTriggerValue AND fltOfferTriggerValueRange THEN 1 ELSE 0 END)
									WHEN @tintOfferTriggerValueTypeRange = 2 THEN 
										(CASE 
											WHEN (@fltOfferTriggerValue BETWEEN fltOfferTriggerValue AND fltOfferTriggerValueRange) OR (@fltOfferTriggerValueRange BETWEEN fltOfferTriggerValue AND fltOfferTriggerValueRange) THEN 1 ELSE 0 END)
									ELSE 0 
								END)
							ELSE 0
						END)
				AND 1 = (CASE 
							WHEN @tintCustomersBasedOn=1 AND @tintOfferBasedOn=1 AND @tintDiscoutBaseOn=1 THEN
								(CASE 
									WHEN 
										(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId=@numProId AND POOInner.tintType=1 AND POOInner.numDivisionID=PromotionOfferOrganizations.numDivisionID) > 0 
										AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=5 AND POIInner.tintType=1 AND POIInner.numValue=PromotionOfferItems.numValue) > 0 
										AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=6 AND POIInner.tintType=1 AND POIInner.numValue=PromotionOfferItemsDiscount.numValue) > 0 
									THEN 
										1 
									ELSE 
										0 
								END)
							WHEN @tintCustomersBasedOn=1 AND @tintOfferBasedOn=1 AND @tintDiscoutBaseOn=2 THEN
								(CASE 
									WHEN 
										(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId=@numProId AND POOInner.tintType=1 AND POOInner.numDivisionID=PromotionOfferOrganizations.numDivisionID) > 0 
										AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=5 AND POIInner.tintType=1 AND POIInner.numValue=PromotionOfferItems.numValue) > 0 
										AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=6 AND POIInner.tintType=2 AND POIInner.numValue=PromotionOfferItemsDiscount.numValue) > 0 
									THEN 
										1 
									ELSE 
										0 
								END)
							WHEN @tintCustomersBasedOn=1 AND @tintOfferBasedOn=1 AND @tintDiscoutBaseOn=3 THEN
								(CASE 
									WHEN 
										(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId=@numProId AND POOInner.tintType=1 AND POOInner.numDivisionID=PromotionOfferOrganizations.numDivisionID) > 0 
										AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=5 AND POIInner.tintType=1 AND POIInner.numValue=PromotionOfferItems.numValue) > 0 
									THEN 
										1 
									ELSE 
										0 
								END)
							WHEN @tintCustomersBasedOn=1 AND @tintOfferBasedOn=2  AND @tintDiscoutBaseOn=1 THEN
								(CASE 
									WHEN 
										(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId=@numProId AND POOInner.tintType=1 AND POOInner.numDivisionID=PromotionOfferOrganizations.numDivisionID) > 0 
										AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=5 AND POIInner.tintType=2 AND POIInner.numValue=PromotionOfferItems.numValue) > 0 
										AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=6 AND POIInner.tintType=1 AND POIInner.numValue=PromotionOfferItemsDiscount.numValue) > 0  
									THEN 
										1 
									ELSE 
										0 
								END)
							WHEN @tintCustomersBasedOn=1 AND @tintOfferBasedOn=2  AND @tintDiscoutBaseOn=2 THEN
								(CASE 
									WHEN 
										(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId=@numProId AND POOInner.tintType=1 AND POOInner.numDivisionID=PromotionOfferOrganizations.numDivisionID) > 0 
										AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=5 AND POIInner.tintType=2 AND POIInner.numValue=PromotionOfferItems.numValue) > 0 
										AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=6 AND POIInner.tintType=2 AND POIInner.numValue=PromotionOfferItemsDiscount.numValue) > 0  
									THEN 
										1 
									ELSE 
										0 
								END)
							WHEN @tintCustomersBasedOn=1 AND @tintOfferBasedOn=2 AND @tintDiscoutBaseOn=3 THEN
								(CASE 
									WHEN 
										(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId=@numProId AND POOInner.tintType=1 AND POOInner.numDivisionID=PromotionOfferOrganizations.numDivisionID) > 0 
										AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=5 AND POIInner.tintType=2 AND POIInner.numValue=PromotionOfferItems.numValue) > 0 
									THEN 
										1 
									ELSE 
										0 
								END)
							WHEN @tintCustomersBasedOn=1 AND @tintOfferBasedOn=4 AND @tintDiscoutBaseOn=1 THEN
								(CASE 
									WHEN 
										(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId=@numProId AND POOInner.tintType=1 AND POOInner.numDivisionID=PromotionOfferOrganizations.numDivisionID) > 0
										AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=6 AND POIInner.tintType=1 AND POIInner.numValue=PromotionOfferItemsDiscount.numValue) > 0
									THEN 
										1 
									ELSE 
										0 
								END) 
							WHEN @tintCustomersBasedOn=1 AND @tintOfferBasedOn=4 AND @tintDiscoutBaseOn=2 THEN
								(CASE 
									WHEN 
										(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId=@numProId AND POOInner.tintType=1 AND POOInner.numDivisionID=PromotionOfferOrganizations.numDivisionID) > 0
										AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=6 AND POIInner.tintType=2 AND POIInner.numValue=PromotionOfferItemsDiscount.numValue) > 0
									THEN 
										1 
									ELSE 
										0 
								END) 
							WHEN @tintCustomersBasedOn=1 AND @tintOfferBasedOn=4 AND @tintDiscoutBaseOn=3 THEN
								(CASE 
									WHEN 
										(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId=@numProId AND POOInner.tintType=1 AND POOInner.numDivisionID=PromotionOfferOrganizations.numDivisionID) > 0 
									THEN 
										1 
									ELSE 
										0 
								END) 
							WHEN @tintCustomersBasedOn=2 AND @tintOfferBasedOn=1 AND @tintDiscoutBaseOn=1 THEN
								(CASE 
									WHEN 
										(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId=@numProId AND POOInner.tintType=2 AND POOInner.numRelationship=PromotionOfferOrganizations.numRelationship AND POOInner.numProfile=PromotionOfferOrganizations.numProfile) > 0 
										AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=5 AND POIInner.tintType=1 AND POIInner.numValue=PromotionOfferItems.numValue) > 0 
										AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=6 AND POIInner.tintType=1 AND POIInner.numValue=PromotionOfferItemsDiscount.numValue) > 0
									THEN 
										1 
									ELSE 
										0 
								END)
							WHEN @tintCustomersBasedOn=2 AND @tintOfferBasedOn=1 AND @tintDiscoutBaseOn=2 THEN
								(CASE 
									WHEN 
										(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId=@numProId AND POOInner.tintType=2 AND POOInner.numRelationship=PromotionOfferOrganizations.numRelationship AND POOInner.numProfile=PromotionOfferOrganizations.numProfile) > 0 
										AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=5 AND POIInner.tintType=1 AND POIInner.numValue=PromotionOfferItems.numValue) > 0 
										AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=6 AND POIInner.tintType=2 AND POIInner.numValue=PromotionOfferItemsDiscount.numValue) > 0
									THEN 
										1 
									ELSE 
										0 
								END)
							WHEN @tintCustomersBasedOn=2 AND @tintOfferBasedOn=1 AND @tintDiscoutBaseOn=3 THEN
								(CASE 
									WHEN 
										(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId=@numProId AND POOInner.tintType=2 AND POOInner.numRelationship=PromotionOfferOrganizations.numRelationship AND POOInner.numProfile=PromotionOfferOrganizations.numProfile) > 0 
										AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=5 AND POIInner.tintType=1 AND POIInner.numValue=PromotionOfferItems.numValue) > 0 
									THEN 
										1 
									ELSE 
										0 
								END)
							WHEN @tintCustomersBasedOn=2 AND @tintOfferBasedOn=2 AND @tintDiscoutBaseOn=1 THEN
								(CASE 
									WHEN 
										(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId=@numProId AND POOInner.tintType=2 AND POOInner.numRelationship=PromotionOfferOrganizations.numRelationship AND POOInner.numProfile=PromotionOfferOrganizations.numProfile) > 0 
										AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=5 AND POIInner.tintType=2 AND POIInner.numValue=PromotionOfferItems.numValue) > 0
										AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=6 AND POIInner.tintType=1 AND POIInner.numValue=PromotionOfferItemsDiscount.numValue) > 0
									THEN 
										1 
									ELSE 
										0 
								END)
							WHEN @tintCustomersBasedOn=2 AND @tintOfferBasedOn=2 AND @tintDiscoutBaseOn=2 THEN
								(CASE 
									WHEN 
										(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId=@numProId AND POOInner.tintType=2 AND POOInner.numRelationship=PromotionOfferOrganizations.numRelationship AND POOInner.numProfile=PromotionOfferOrganizations.numProfile) > 0 
										AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=5 AND POIInner.tintType=2 AND POIInner.numValue=PromotionOfferItems.numValue) > 0 
										AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=6 AND POIInner.tintType=2 AND POIInner.numValue=PromotionOfferItemsDiscount.numValue) > 0
									THEN 
										1 
									ELSE 
										0 
								END)
							WHEN @tintCustomersBasedOn=2 AND @tintOfferBasedOn=2  AND @tintDiscoutBaseOn=3 THEN
								(CASE 
									WHEN 
										(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId=@numProId AND POOInner.tintType=2 AND POOInner.numRelationship=PromotionOfferOrganizations.numRelationship AND POOInner.numProfile=PromotionOfferOrganizations.numProfile) > 0 
										AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=5 AND POIInner.tintType=2 AND POIInner.numValue=PromotionOfferItems.numValue) > 0 
									THEN 
										1 
									ELSE 
										0 
								END)
							WHEN @tintCustomersBasedOn=2 AND @tintOfferBasedOn=4 AND @tintDiscoutBaseOn=1 THEN
								(CASE 
									WHEN 
										(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId=@numProId AND POOInner.tintType=2 AND POOInner.numRelationship=PromotionOfferOrganizations.numRelationship AND POOInner.numProfile=PromotionOfferOrganizations.numProfile) > 0 
										AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=6 AND POIInner.tintType=1 AND POIInner.numValue=PromotionOfferItemsDiscount.numValue) > 0
									THEN 
										1 
									ELSE 
										0 
								END)
							WHEN @tintCustomersBasedOn=2 AND @tintOfferBasedOn=4 AND @tintDiscoutBaseOn=2 THEN
								(CASE 
									WHEN 
										(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId=@numProId AND POOInner.tintType=2 AND POOInner.numRelationship=PromotionOfferOrganizations.numRelationship AND POOInner.numProfile=PromotionOfferOrganizations.numProfile) > 0
										AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=6 AND POIInner.tintType=2 AND POIInner.numValue=PromotionOfferItemsDiscount.numValue) > 0
									THEN 
										1 
									ELSE 
										0 
								END)
							WHEN @tintCustomersBasedOn=2 AND @tintOfferBasedOn=4 AND @tintDiscoutBaseOn=3 THEN
								(CASE 
									WHEN 
										(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId=@numProId AND POOInner.tintType=2 AND POOInner.numRelationship=PromotionOfferOrganizations.numRelationship AND POOInner.numProfile=PromotionOfferOrganizations.numProfile) > 0 
									THEN 
										1 
									ELSE 
										0 
								END)
							WHEN @tintCustomersBasedOn=3 AND @tintOfferBasedOn=1 AND @tintDiscoutBaseOn=1 THEN
								(CASE 
									WHEN 
										(SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=5 AND POIInner.tintType=1 AND POIInner.numValue=PromotionOfferItems.numValue) > 0
										AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=6 AND POIInner.tintType=1 AND POIInner.numValue=PromotionOfferItemsDiscount.numValue) > 0
									THEN 
										1 
									ELSE 
										0 
								END)
							WHEN @tintCustomersBasedOn=3 AND @tintOfferBasedOn=1 AND @tintDiscoutBaseOn=2 THEN
								(CASE 
									WHEN 
										(SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=5 AND POIInner.tintType=1 AND POIInner.numValue=PromotionOfferItems.numValue) > 0 
										AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=6 AND POIInner.tintType=2 AND POIInner.numValue=PromotionOfferItemsDiscount.numValue) > 0
									THEN 
										1 
									ELSE 
										0 
								END)
							WHEN @tintCustomersBasedOn=3 AND @tintOfferBasedOn=1 AND @tintDiscoutBaseOn=3 THEN
								(CASE 
									WHEN 
										(SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=5 AND POIInner.tintType=1 AND POIInner.numValue=PromotionOfferItems.numValue) > 0 
									THEN 
										1 
									ELSE 
										0 
								END)
							WHEN @tintCustomersBasedOn=3 AND @tintOfferBasedOn=2 AND @tintDiscoutBaseOn=1 THEN
								(CASE 
									WHEN 
										(SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=5 AND POIInner.tintType=2 AND POIInner.numValue=PromotionOfferItems.numValue) > 0
										AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=6 AND POIInner.tintType=1 AND POIInner.numValue=PromotionOfferItemsDiscount.numValue) > 0
									THEN 
										1 
									ELSE 
										0 
								END)
							WHEN @tintCustomersBasedOn=3 AND @tintOfferBasedOn=2 AND @tintDiscoutBaseOn=2 THEN
								(CASE 
									WHEN 
										(SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=5 AND POIInner.tintType=2 AND POIInner.numValue=PromotionOfferItems.numValue) > 0 
										AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=6 AND POIInner.tintType=2 AND POIInner.numValue=PromotionOfferItemsDiscount.numValue) > 0
									THEN 
										1 
									ELSE 
										0 
								END)
							WHEN @tintCustomersBasedOn=3 AND @tintOfferBasedOn=2 AND @tintDiscoutBaseOn=3 THEN
								(CASE 
									WHEN 
										(SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=5 AND POIInner.tintType=2 AND POIInner.numValue=PromotionOfferItems.numValue) > 0 
									THEN 
										1 
									ELSE 
										0 
								END)
							WHEN @tintCustomersBasedOn=3 AND @tintOfferBasedOn=4 AND @tintDiscoutBaseOn=1 THEN
								(CASE 
									WHEN 
										(SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=6 AND POIInner.tintType=1 AND POIInner.numValue=PromotionOfferItemsDiscount.numValue) > 0
									THEN 
										1 
									ELSE 
										0 
								END)
							WHEN @tintCustomersBasedOn=3 AND @tintOfferBasedOn=4 AND @tintDiscoutBaseOn=2 THEN
								(CASE 
									WHEN 
										(SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=6 AND POIInner.tintType=2 AND POIInner.numValue=PromotionOfferItemsDiscount.numValue) > 0
									THEN 
										1 
									ELSE 
										0 
								END)
							WHEN @tintCustomersBasedOn=3 AND @tintOfferBasedOn=4 AND @tintDiscoutBaseOn=3 THEN 3
						END)
			) > 0
		BEGIN
			DECLARE @ERRORMESSAGE AS VARCHAR(50)
			SET @ERRORMESSAGE = (CASE 
							WHEN @tintCustomersBasedOn=1 AND @tintOfferBasedOn=1 THEN 'ORGANIZATION-ITEM'
							WHEN @tintCustomersBasedOn=1 AND @tintOfferBasedOn=2 THEN 'ORGANIZATION-ITEMCLASSIFICATIONS'
							WHEN @tintCustomersBasedOn=1 AND @tintOfferBasedOn=4 THEN 'ORGANIZATION-ALLITEMS'
							WHEN @tintCustomersBasedOn=2 AND @tintOfferBasedOn=1 THEN 'RELATIONSHIPPROFILE-ITEMS'
							WHEN @tintCustomersBasedOn=2 AND @tintOfferBasedOn=2 THEN 'RELATIONSHIPPROFILE-ITEMCLASSIFICATIONS'
							WHEN @tintCustomersBasedOn=2 AND @tintOfferBasedOn=4 THEN 'RELATIONSHIPPROFILE-ALLITEMS'
							WHEN @tintCustomersBasedOn=3 AND @tintOfferBasedOn=1 THEN 'ALLORGANIZATIONS-ITEMS'
							WHEN @tintCustomersBasedOn=3 AND @tintOfferBasedOn=2 THEN 'ALLORGANIZATIONS-ITEMCLASSIFICATIONS'
							WHEN @tintCustomersBasedOn=3 AND @tintOfferBasedOn=4 THEN 'ALLORGANIZATIONS-ALLITEMS'
					END)
			RAISERROR(@ERRORMESSAGE,16,1)
			RETURN

		END


		IF ISNULL(@bitAppliesToSite,0) = 0
		BEGIN
			DELETE FROM PromotionOfferSites WHERE numPromotionID=@numProId
		END

		UPDATE 
			PromotionOffer
		SET 
			vcProName = @vcProName
			,numDomainId = @numDomainId
			,dtValidFrom = @dtValidFrom
			,dtValidTo = @dtValidTo
			,bitNeverExpires = @bitNeverExpires
			,bitApplyToInternalOrders = @bitApplyToInternalOrders
			,bitAppliesToSite = @bitAppliesToSite
			,bitRequireCouponCode = @bitRequireCouponCode
			,txtCouponCode = @txtCouponCode
			,tintUsageLimit = @tintUsageLimit
			--,bitFreeShiping = @bitFreeShiping
			--,monFreeShippingOrderAmount = @monFreeShippingOrderAmount
			--,numFreeShippingCountry = @numFreeShippingCountry
			--,bitFixShipping1 = @bitFixShipping1
			--,bitFixShipping2 = @bitFixShipping2
			--,monFixShipping1OrderAmount = @monFixShipping1OrderAmount
			--,monFixShipping2OrderAmount = @monFixShipping2OrderAmount
			--,monFixShipping1Charge = @monFixShipping1Charge
			--,monFixShipping2Charge = @monFixShipping2Charge
			,bitDisplayPostUpSell = @bitDisplayPostUpSell
			,intPostSellDiscount = @intPostSellDiscount
			,tintOfferTriggerValueType = @tintOfferTriggerValueType
			,fltOfferTriggerValue = @fltOfferTriggerValue
			,tintOfferBasedOn = @tintOfferBasedOn
			,tintDiscountType = @tintDiscountType
			,fltDiscountValue = @fltDiscountValue
			,tintDiscoutBaseOn = @tintDiscoutBaseOn
			,numModifiedBy = @numUserCntID
			,dtModified = GETUTCDATE()
			,tintCustomersBasedOn = @tintCustomersBasedOn
			,tintOfferTriggerValueTypeRange = @tintOfferTriggerValueTypeRange
			,fltOfferTriggerValueRange = @fltOfferTriggerValueRange
		WHERE 
			numProId=@numProId
       
        SELECT  @numProId
	END
END
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageSimilarItem')
DROP PROCEDURE USP_ManageSimilarItem
GO
CREATE PROCEDURE USP_ManageSimilarItem
	@numDomainID NUMERIC(9),
	@numParentItemCode NUMERIC(9),
	@numItemCode NUMERIC(9),
	@vcRelationship VARCHAR(500),
	@bitPreUpSell BIT,
	@bitPostUpSell BIT, 
	@bitRequired BIT,
	@vcUpSellDesc VARCHAR(1000),
	@byteMode TINYINT
AS 
BEGIN
	IF @byteMode = 0
	BEGIN
		IF NOT EXISTS( SELECT * FROM [SimilarItems] WHERE [numDomainID]=@numDomainID AND numItemCode = @numItemCode AND numParentItemCode = @numParentItemCode)
		BEGIN
			INSERT INTO [SimilarItems] (
				[numDomainID],
				[numItemCode],
				[numParentItemCode],
				[vcRelationship],
				[bitPreUpSell], 
				[bitPostUpSell], 
				[bitRequired],
				[vcUpSellDesc]
			) VALUES (  
				/* numDomainID - numeric(18, 0) */ @numDomainID,
				/* vcExportTables - varchar(100) */ @numItemCode,
				/* dtLastExportedOn - datetime */ @numParentItemCode,
				@vcRelationship,
				@bitPreUpSell,
				@bitPostUpSell, 
				@bitRequired,
				@vcUpSellDesc) 
		END
	END
	ELSE IF @byteMode = 1
	BEGIN
		UPDATE [SimilarItems]
		SET [bitPreUpSell] = @bitPreUpSell,
			[bitPostUpSell] = @bitPostUpSell, 
			[bitRequired] =	@bitRequired
		WHERE numParentItemCode = @numParentItemCode
			AND numItemCode = @numItemCode
			AND numDomainID = @numDomainID
	END
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageWorkFlowMaster')
DROP PROCEDURE USP_ManageWorkFlowMaster
GO
Create PROCEDURE [dbo].[USP_ManageWorkFlowMaster]                                        
	@numWFID numeric(18, 0) OUTPUT,
    @vcWFName varchar(200),
    @vcWFDescription varchar(500),   
    @numDomainID numeric(18, 0),
    @numUserCntID numeric(18, 0),
    @bitActive bit,
    @numFormID NUMERIC(18,0),
    @tintWFTriggerOn NUMERIC(18),
    @vcWFAction varchar(Max),
    @vcDateField VARCHAR(MAX),
    @intDays INT,
    @intActionOn INT,
	@bitCustom bit,
	@numFieldID numeric(18),
    @strText TEXT = ''
as                 
DECLARE @ErrorMessage NVARCHAR(4000);SET @ErrorMessage=''
DECLARE @ErrorSeverity INT;
DECLARE @ErrorState INT;
		
BEGIN TRANSACTION;

BEGIN TRY
	DECLARE @hDocItems INT                                                                                                                                                                
	EXEC sp_xml_preparedocument @hDocItems OUTPUT, @strText


	DECLARE @TEMP TABLE
	(
		tintActionType TINYINT,
		numBizDocTypeID NUMERIC(18,0)
	)
	INSERT INTO 
		@TEMP
	SELECT 
		tintActionType,
		numBizDocTypeID 
	FROM 
		OPENXML (@hDocItems, '/NewDataSet/WorkFlowActionList',2) 
	WITH 
		(
			tintActionType TINYINT, 
			numBizDocTypeID numeric(18,0)
		)

	IF (SELECT COUNT(*) FROM @TEMP WHERE tintActionType = 6 AND (numBizDocTypeID=287 OR numBizDocTypeID=29397)) > 0
		BEGIN
			--CHECK IF CREATE INVOICE OR CREATE PACKING SLIP RULE EXISTS IN MASS SALES FULLFILLMENT
			IF (
				SELECT 
					COUNT(*) 
				FROM 
					OpportunityAutomationRules 
				WHERE 
					numDomainID = @numDomainID AND 
					(numRuleID = 1 OR numRuleID = 2)
				) > 0
			BEGIN
				RAISERROR ('MASS SALES FULLFILLMENT RULE EXISTS',16,1);
				RETURN -1
			END
		END


	DECLARE @bitDiplicate BIT = 0

	-- FIRST CHECK IF WORKFLOW WITH SAME MODULE AND TRIGGER TYPE EXISTS
	IF EXISTS (SELECT numWFID FROM WorkFlowMaster WHERE numDomainID=@numDomainID AND numWFID <> @numWFID AND numFormID=@numFormID AND tintWFTriggerOn=@tintWFTriggerOn AND (SELECT COUNT(*) FROM WorkFlowConditionList WHERE WorkFlowConditionList.numWFID = WorkFlowMaster.numWFID) = (SELECT COUNT(*) FROM OPENXML (@hDocItems, '/NewDataSet/WorkFlowConditionList',2) WITH (numFieldID NUMERIC, bitCustom BIT, vcFilterValue VARCHAR(500), vcFilterOperator VARCHAR(10), vcFilterData VARCHAR(MAX))))
	BEGIN
		-- NOT CHECK IF CONDITIONS ARE ALSO SAME
		DECLARE @TEMPWF TABLE
		(
			ID INT IDENTITY(1,1)
			,numWFID NUMERIC(18,0)
		)
		INSERT INTO 
			@TEMPWF 
		SELECT 
			numWFID 
		FROM 
			WorkFlowMaster 
		WHERE 
			numDomainID=@numDomainID
			AND numWFID <> @numWFID
			AND numFormID=@numFormID 
			AND tintWFTriggerOn=@tintWFTriggerOn

		DECLARE @i AS INT = 1
		DECLARE @iCount AS INT
		DECLARE @numTempWFID NUMERIC(18,0)

		SELECT @iCount=COUNT(*) FROM @TEMPWF

		WHILE @i <= @iCount
		BEGIN
			SELECT @numTempWFID=numWFID FROM @TEMPWF WHERE ID=@i

			IF NOT EXISTS (
				SELECT 
					numFieldID,bitCustom,vcFilterValue,vcFilterOperator,vcFilterData 
				FROM 
				(
					SELECT 
						numFieldID
						,bitCustom
						,vcFilterValue
						,vcFilterOperator
						,Split.a.value('.', 'VARCHAR(100)') AS vcFilterData  
					FROM  
					(
						SELECT 
							numFieldID
							,bitCustom
							,vcFilterValue
							,vcFilterOperator
							,CAST ('<M>' + REPLACE(vcFilterData, ',', '</M><M>') + '</M>' AS XML) AS vcFilterData
						FROM 
							OPENXML (@hDocItems, '/NewDataSet/WorkFlowConditionList',2) 
						WITH 
							(numFieldID NUMERIC, bitCustom BIT, vcFilterValue VARCHAR(500), vcFilterOperator VARCHAR(10), vcFilterData VARCHAR(MAX))
					) AS A CROSS APPLY vcFilterData.nodes ('/M') AS Split(a)
				) T1
				EXCEPT
				SELECT 
					numFieldID,bitCustom,vcFilterValue,vcFilterOperator,vcFilterData 
				FROM 
				(
					SELECT 
						numFieldID
						,bitCustom
						,vcFilterValue
						,vcFilterOperator
						,Split.a.value('.', 'VARCHAR(100)') AS vcFilterData  
					FROM  
					(
						SELECT 
							numFieldID
							,bitCustom
							,vcFilterValue
							,vcFilterOperator
							,CAST ('<M>' + REPLACE(vcFilterData, ',', '</M><M>') + '</M>' AS XML) AS vcFilterData  
						FROM
							WorkFlowConditionList
						WHERE
							numWFID=@numTempWFID
					) AS A CROSS APPLY vcFilterData.nodes ('/M') AS Split(a)
				) T2
			)
			SELECT @bitDiplicate = 1 ELSE SELECT @bitDiplicate = 0


			IF NOT EXISTS (
				SELECT 
					numFieldID,bitCustom,vcFilterValue,vcFilterOperator,vcFilterData 
				FROM 
				(
					SELECT 
						numFieldID
						,bitCustom
						,vcFilterValue
						,vcFilterOperator
						,Split.a.value('.', 'VARCHAR(100)') AS vcFilterData  
					FROM  
					(
						SELECT 
							numFieldID
							,bitCustom
							,vcFilterValue
							,vcFilterOperator
							,CAST ('<M>' + REPLACE(vcFilterData, ',', '</M><M>') + '</M>' AS XML) AS vcFilterData  
						FROM
							WorkFlowConditionList
						WHERE
							numWFID=@numTempWFID
					) AS A CROSS APPLY vcFilterData.nodes ('/M') AS Split(a)
				) T2
				EXCEPT
				SELECT 
					numFieldID,bitCustom,vcFilterValue,vcFilterOperator,vcFilterData 
				FROM 
				(
					SELECT 
						numFieldID
						,bitCustom
						,vcFilterValue
						,vcFilterOperator
						,Split.a.value('.', 'VARCHAR(100)') AS vcFilterData  
					FROM  
					(
						SELECT 
							numFieldID
							,bitCustom
							,vcFilterValue
							,vcFilterOperator
							,CAST ('<M>' + REPLACE(vcFilterData, ',', '</M><M>') + '</M>' AS XML) AS vcFilterData
						FROM 
							OPENXML (@hDocItems, '/NewDataSet/WorkFlowConditionList',2) 
						WITH 
							(numFieldID NUMERIC, bitCustom BIT, vcFilterValue VARCHAR(500), vcFilterOperator VARCHAR(10), vcFilterData VARCHAR(MAX))
					) AS A CROSS APPLY vcFilterData.nodes ('/M') AS Split(a)
				) T1	
			)

			SELECT @bitDiplicate = 1 ELSE SELECT @bitDiplicate = 0

			IF @bitDiplicate = 1
			BEGIN
				BREAK
			END

			SET @i = @i + 1
		END
	END


	IF @bitDiplicate = 1
	BEGIN
		RAISERROR ('DUPLICATE_WORKFLOW',16,1);
		RETURN -1
	END

	IF @numWFID=0
	BEGIN
		INSERT INTO [dbo].[WorkFlowMaster] ([numDomainID], [vcWFName], [vcWFDescription], [numCreatedBy], [numModifiedBy], [dtCreatedDate], [dtModifiedDate], [bitActive], [numFormID], [tintWFTriggerOn],[vcWFAction],[vcDateField],[intDays],[intActionOn],[bitCustom],[numFieldID])
		SELECT @numDomainID, @vcWFName, @vcWFDescription, @numUserCntID, @numUserCntID, GETUTCDATE(), GETUTCDATE(), @bitActive, @numFormID, @tintWFTriggerOn,@vcWFAction,@vcDateField,@intDays,@intActionOn,@bitCustom,@numFieldID

		SET @numWFID=@@IDENTITY
		--start of my
		IF DATALENGTH(@strText)>2
		BEGIN                                                                                                         
			--Delete records into WorkFlowTriggerFieldList and Insert
			DELETE FROM WorkFlowTriggerFieldList WHERE  [numWFID] = @numWFID

			INSERT INTO dbo.WorkFlowTriggerFieldList (numWFID,numFieldID,bitCustom) 
				SELECT @numWFID,numFieldID,bitCustom
				  FROM OPENXML (@hDocItems, '/NewDataSet/WorkFlowTriggerFieldList',2) WITH ( numFieldID NUMERIC, bitCustom BIT )
	
	
			--Delete records into WorkFlowConditionList and Insert
			DELETE FROM WorkFlowConditionList WHERE  [numWFID] = @numWFID

			INSERT INTO dbo.WorkFlowConditionList (numWFID,numFieldID,bitCustom,vcFilterValue,vcFilterOperator,vcFilterANDOR,vcFilterData,intCompare) 
				SELECT @numWFID,numFieldID,bitCustom,vcFilterValue,vcFilterOperator,vcFilterANDOR,vcFilterData,intCompare
				  FROM OPENXML (@hDocItems, '/NewDataSet/WorkFlowConditionList',2) WITH ( numFieldID NUMERIC, bitCustom BIT, vcFilterValue VARCHAR(500), vcFilterOperator VARCHAR(10),vcFilterANDOR VARCHAR(10),vcFilterData VARCHAR(MAX),intCompare int)


			--Delete records into WorkFlowActionUpdateFields and Insert
			DELETE FROM WorkFlowActionUpdateFields WHERE numWFActionID IN(SELECT numWFActionID FROM WorkFlowActionList WHERE numWFID=@numWFID)

			--Delete records into WorkFlowActionList and Insert
			DELETE FROM WorkFlowActionList WHERE  [numWFID] = @numWFID

			INSERT INTO dbo.WorkFlowActionList (numWFID,tintActionType,numTemplateID,vcEmailToType,tintTicklerActionAssignedTo,vcAlertMessage,tintActionOrder,tintIsAttachment,vcApproval,tintSendMail,numBizDocTypeID,numBizDocTemplateID,vcSMSText,vcEmailSendTo,vcMailBody,vcMailSubject) 
				SELECT @numWFID,tintActionType,numTemplateID,vcEmailToType,tintTicklerActionAssignedTo,vcAlertMessage,tintActionOrder,tintIsAttachment,vcApproval ,tintSendMail,numBizDocTypeID,numBizDocTemplateID,vcSMSText,vcEmailSendTo,vcMailBody ,vcMailSubject
				  FROM OPENXML (@hDocItems, '/NewDataSet/WorkFlowActionList',2) WITH ( tintActionType TINYINT, numTemplateID NUMERIC,vcEmailToType VARCHAR(50),tintTicklerActionAssignedTo TINYINT,vcAlertMessage NTEXT,tintActionOrder TINYINT,tintIsAttachment TINYINT,vcApproval VARCHAR(100),tintSendMail TINYINT,numBizDocTypeID numeric(18,0),numBizDocTemplateID numeric(18,0),vcSMSText NTEXT,vcEmailSendTo varchar(500),vcMailBody ntext,vcMailSubject varchar(500))

			INSERT INTO dbo.WorkFlowActionUpdateFields (numWFActionID,tintModuleType,numFieldID,bitCustom,vcValue,vcData) 
				SELECT WFA.numWFActionID,tintModuleType,numFieldID,bitCustom,vcValue,vcData
				  FROM OPENXML (@hDocItems, '/NewDataSet/WorkFlowActionUpdateFields',2) WITH ( numWFActionID NUMERIC, tintModuleType NUMERIC,numFieldID NUMERIC,bitCustom BIT,vcValue VARCHAR(500),vcData VARCHAR(500) ) WFAUF
				  JOIN WorkFlowActionList WFA ON WFA.tintActionOrder=WFAUF.numWFActionID WHERE WFA.numWFID=@numWFID

			EXEC sp_xml_removedocument @hDocItems
		END	
		--end of my
	END
	ELSE
	BEGIN
	
		UPDATE [dbo].[WorkFlowMaster]
		SET    [vcWFName] = @vcWFName, [vcWFDescription] = @vcWFDescription, [numModifiedBy] = @numUserCntID, [dtModifiedDate] = GETUTCDATE(), [bitActive] = @bitActive, [numFormID] = @numFormID, [tintWFTriggerOn] = @tintWFTriggerOn,[vcWFAction]=@vcWFAction,[vcDateField]=@vcDateField,[intDays]=@intDays,[intActionOn]=@intActionOn,[bitCustom]=@bitCustom,[numFieldID]=@numFieldID
		WHERE  [numDomainID] = @numDomainID AND [numWFID] = @numWFID
	
		IF DATALENGTH(@strText)>2
		BEGIN
			DECLARE @hDocItem INT                                                                                                                                                                
	
			EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strText                                                                                                             
        
			--Delete records into WorkFlowTriggerFieldList and Insert
			DELETE FROM WorkFlowTriggerFieldList WHERE  [numWFID] = @numWFID

			INSERT INTO dbo.WorkFlowTriggerFieldList (numWFID,numFieldID,bitCustom) 
				SELECT @numWFID,numFieldID,bitCustom
				  FROM OPENXML (@hDocItem, '/NewDataSet/WorkFlowTriggerFieldList',2) WITH ( numFieldID NUMERIC, bitCustom BIT )
	
	
			--Delete records into WorkFlowConditionList and Insert
			DELETE FROM WorkFlowConditionList WHERE  [numWFID] = @numWFID

			INSERT INTO dbo.WorkFlowConditionList (numWFID,numFieldID,bitCustom,vcFilterValue,vcFilterOperator,vcFilterANDOR,vcFilterData,intCompare) 
				SELECT @numWFID,numFieldID,bitCustom,vcFilterValue,vcFilterOperator,vcFilterANDOR,vcFilterData,intCompare
				  FROM OPENXML (@hDocItem, '/NewDataSet/WorkFlowConditionList',2) WITH ( numFieldID NUMERIC, bitCustom BIT, vcFilterValue VARCHAR(500), vcFilterOperator VARCHAR(10),vcFilterANDOR VARCHAR(10),vcFilterData varchar(MAX),intCompare int)


			--Delete records into WorkFlowActionUpdateFields and Insert
			DELETE FROM WorkFlowActionUpdateFields WHERE numWFActionID IN(SELECT numWFActionID FROM WorkFlowActionList WHERE numWFID=@numWFID)

			--Delete records into WorkFlowActionList and Insert
			DELETE FROM WorkFlowActionList WHERE  [numWFID] = @numWFID

			INSERT INTO dbo.WorkFlowActionList (numWFID,tintActionType,numTemplateID,vcEmailToType,tintTicklerActionAssignedTo,vcAlertMessage,tintActionOrder,tintIsAttachment,vcApproval ,tintSendMail,numBizDocTypeID ,numBizDocTemplateID,vcSMSText,vcEmailSendTo,vcMailBody,vcMailSubject ) 
				SELECT @numWFID,tintActionType,numTemplateID,vcEmailToType,tintTicklerActionAssignedTo,vcAlertMessage,tintActionOrder,tintIsAttachment,vcApproval ,tintSendMail,numBizDocTypeID ,numBizDocTemplateID,vcSMSText ,vcEmailSendTo,vcMailBody,vcMailSubject
				  FROM OPENXML (@hDocItem, '/NewDataSet/WorkFlowActionList',2) WITH ( tintActionType TINYINT, numTemplateID NUMERIC,vcEmailToType VARCHAR(50),tintTicklerActionAssignedTo TINYINT,vcAlertMessage NTEXT,tintActionOrder TINYINT,tintIsAttachment TINYINT,vcApproval VARCHAR(100),tintSendMail TINYINT,numBizDocTypeID numeric(18,0),numBizDocTemplateID numeric(18,0),vcSMSText NTEXT ,vcEmailSendTo varchar(500),vcMailBody ntext,vcMailSubject varchar(500))

			INSERT INTO dbo.WorkFlowActionUpdateFields (numWFActionID,tintModuleType,numFieldID,bitCustom,vcValue,vcData) 
				SELECT WFA.numWFActionID,tintModuleType,numFieldID,bitCustom,vcValue,vcData
				  FROM OPENXML (@hDocItem, '/NewDataSet/WorkFlowActionUpdateFields',2) WITH ( numWFActionID NUMERIC, tintModuleType NUMERIC,numFieldID NUMERIC,bitCustom BIT,vcValue VARCHAR(500),vcData VARCHAR(500)) WFAUF
				  JOIN WorkFlowActionList WFA ON WFA.tintActionOrder=WFAUF.numWFActionID WHERE WFA.numWFID=@numWFID

			EXEC sp_xml_removedocument @hDocItem
		END	
	END

	IF EXISTS (SELECT * FROM SalesFulfillmentConfiguration WHERE numDomainID=@numDomainID AND bitActive=1)
	BEGIN
		DECLARE @numRule2OrderStatus NUMERIC(18,0)
		DECLARE @numRule3OrderStatus NUMERIC(18,0)
		DECLARE @numRule4OrderStatus NUMERIC(18,0)

		SELECT
			@numRule2OrderStatus= (CASE WHEN bitRule2IsActive = 1 THEN numRule2OrderStatus ELSE 0 END)
			,@numRule3OrderStatus= (CASE WHEN bitRule3IsActive = 1 THEN numRule3OrderStatus ELSE 0 END)
			,@numRule4OrderStatus= (CASE WHEN bitRule3IsActive = 1 THEN numRule4OrderStatus ELSE 0 END)
		FROM
			SalesFulfillmentConfiguration
		WHERE 
			numDomainID=@numDomainID 
			AND bitActive=1

		IF EXISTS (SELECT * FROM WorkFlowConditionList WHERE numFieldID=101 AND vcFilterValue IN (@numRule2OrderStatus,@numRule3OrderStatus,@numRule4OrderStatus))
		BEGIN
			RAISERROR ('MASS SALES FULLFILLMENT RULE EXISTS',16,1);
			RETURN -1
		END
	END

	COMMIT
END TRY
BEGIN CATCH

	SELECT @ErrorMessage = ERROR_MESSAGE(), @ErrorSeverity = ERROR_SEVERITY(), @ErrorState = ERROR_STATE();

	IF @@TRANCOUNT > 0
	ROLLBACK TRANSACTION;

	-- Use RAISERROR inside the CATCH block to return error
	-- information about the original error that caused
	-- execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState);
END CATCH;
/****** Object:  StoredProcedure [dbo].[USP_OPPItemDetails]    Script Date: 09/01/2009 18:59:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- exec USP_OPPItemDetails @numItemCode=31455,@OpportunityId=12387,@numDomainId=110
--created by anooop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_oppitemdetails')
DROP PROCEDURE usp_oppitemdetails
GO
CREATE PROCEDURE [dbo].[USP_OPPItemDetails]
    (
      @numUserCntID NUMERIC(9) = 0,
      @OpportunityId NUMERIC(9) = 0,
      @numDomainId NUMERIC(9),
      @ClientTimeZoneOffset  INT
    )
AS 
BEGIN
    IF @OpportunityId >0
    BEGIN
        DECLARE @DivisionID AS NUMERIC(9)
        DECLARE @TaxPercentage AS FLOAT
        DECLARE @tintOppType AS TINYINT
        DECLARE @fld_Name AS VARCHAR(500)
        DECLARE @strSQL AS VARCHAR(MAX)
        DECLARE @fld_ID AS NUMERIC(9)
        DECLARE @numBillCountry AS NUMERIC(9)
        DECLARE @numBillState AS NUMERIC(9)

                
        SELECT  @DivisionID = numDivisionID,
                @tintOppType = tintOpptype
        FROM    OpportunityMaster
        WHERE   numOppId = @OpportunityId
               

		CREATE TABLE #tempForm 
		(
			tintOrder TINYINT,vcDbColumnName nvarchar(50),vcOrigDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),
			vcListItemType varchar(3),numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,
			numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,bitIsAlphaNumeric BIT,bitIsNumeric BIT,
			bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),ListRelID numeric(9),intColumnWidth int,intVisible int
			,vcFieldDataType CHAR(1)
		)

		IF
			(
				SELECT 
					COUNT(*)
				FROM 
					View_DynamicColumns DC 
				WHERE 
					DC.numFormId=(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) and DC.numDomainID=@numDomainID AND numUserCntID = @numUserCntID AND
					ISNULL(DC.bitSettingField,0)=1 AND ISNULL(DC.bitDeleted,0)=0 AND ISNULL(DC.bitCustom,0)=0) > 0
			OR
			(
				SELECT 
					COUNT(*)
				FROM 
					View_DynamicCustomColumns
				WHERE 
					numFormId=(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) AND numUserCntID=@numUserCntID AND numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=0 AND ISNULL(bitCustom,0)=1
			) > 0
		BEGIN	

		INSERT INTO #tempForm
		select isnull(DC.tintRow,0)+1 as tintOrder,DDF.vcDbColumnName,DDF.vcOrigDbColumnName,ISNULL(DDF.vcCultureFieldName,DDF.vcFieldName),                  
		 DDF.vcAssociatedControlType,DDF.vcListItemType,DDF.numListID,DDF.vcLookBackTableName                                                 
		,DDF.bitCustom,DDF.numFieldId,DDF.bitAllowSorting,DDF.bitInlineEdit,
		DDF.bitIsRequired,DDF.bitIsEmail,DDF.bitIsAlphaNumeric,DDF.bitIsNumeric,DDF.bitIsLengthValidation,
		DDF.intMaxLength,DDF.intMinLength,DDF.bitFieldMessage,DDF.vcFieldMessage vcFieldMessage,DDF.ListRelID,DDF.intColumnWidth,Case when DC.numFieldID is null then 0 else 1 end,DDF.vcFieldDataType
		 FROM View_DynamicDefaultColumns DDF left join View_DynamicColumns DC on DC.numFormId=(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) and DDF.numFieldId=DC.numFieldId and DC.numUserCntID=@numUserCntID and DC.numDomainID=@numDomainID and numRelCntType=0 AND tintPageType=1 
		 where DDF.numFormId=(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) and DDF.numDomainID=@numDomainID AND ISNULL(DDF.bitSettingField,0)=1 AND ISNULL(DDF.bitDeleted,0)=0 AND ISNULL(DDF.bitCustom,0)=0
       
		 UNION
    
			select tintRow+1 as tintOrder,vcDbColumnName,vcFieldName,vcFieldName,                  
		 vcAssociatedControlType,'' as vcListItemType,numListID,''                                                 
		,bitCustom,numFieldId,bitAllowSorting,0 as bitAllowEdit,
		bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
		intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,''
		 from View_DynamicCustomColumns
		 where numFormId=(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=0
		 AND ISNULL(bitCustom,0)=1

		END
		ELSE
		BEGIN
			--Check if Master Form Configuration is created by administrator if NoColumns=0
			DECLARE @IsMasterConfAvailable BIT = 0
			DECLARE @numUserGroup NUMERIC(18,0)

			SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

			IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = (CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
			BEGIN
				SET @IsMasterConfAvailable = 1
			END

			--If MasterConfiguration is available then load it otherwise load default columns
			IF @IsMasterConfAvailable = 1
			BEGIN
				INSERT INTO #tempForm
				SELECT 
					(intRowNum + 1) as tintOrder, vcDbColumnName, vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
					vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName,bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,
					bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,
					bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth,1,vcFieldDataType
				FROM 
					View_DynamicColumnsMasterConfig
				WHERE
					View_DynamicColumnsMasterConfig.numFormId=(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) AND 
					View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
					View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
					View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
					ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
					ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
				UNION
				SELECT 
					tintRow+1 as tintOrder,vcDbColumnName,vcFieldName,vcFieldName,                  
					 vcAssociatedControlType,'' as vcListItemType,numListID,''                                                 
					,bitCustom,numFieldId,bitAllowSorting,0 as bitAllowEdit,
					bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
					intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,''
				FROM 
					View_DynamicCustomColumnsMasterConfig
				WHERE 
					View_DynamicCustomColumnsMasterConfig.numFormId=(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) AND 
					View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
					View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
					View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
					ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1
				ORDER BY 
					tintOrder ASC  
			END
		END


SET @strSQL = ''

DECLARE @avgCost int
SET @avgCost = ISNULL((select TOP 1 numCost from Domain where numDOmainId=@numDomainID),0)



--------------Make Dynamic Query--------------
SET @strSQL = @strSQL + '
Select 
Opp.numoppitemtCode,Opp.vcNotes,WItems.numBackOrder,CAST(ISNULL(Opp.numCost,0) AS MONEY) AS numCost,
ISNULL(Opp.bitItemPriceApprovalRequired,0) AS bitItemPriceApprovalRequired,
ISNULL(Opp.numSOVendorID,0) AS numVendorID,'+
CONCAT('ISNULL(Opp.monTotAmount,0) - (ISNULL(Opp.numUnitHour,0) * (CASE ',@avgCost,' WHEN 3 THEN ISNULL(V.monCost,0) / dbo.fn_UOMConversion(numPurchaseUnit,I.numItemCode,',@numDomainID,',numBaseUnit) WHEN 2 THEN (CASE WHEN ISNULL(Opp.numSOVendorID,0) > 0 THEN Opp.numCost ELSE ISNULL(V.monCost,0) / dbo.fn_UOMConversion(numPurchaseUnit,I.numItemCode,',@numDomainID,',numBaseUnit) END) ELSE monAverageCost END)) AS vcMargin,')
+'
OM.tintOppType,
Opp.numItemCode,
I.charItemType,
ISNULL(I.bitAsset,0) as bitAsset,
ISNULL(I.bitRental,0) as bitRental,
ISNULL(I.bitSerialized,0) bitSerialized,
ISNULL(I.bitLotNo,0) bitLotNo,
Opp.numOppId,
Opp.bitWorkOrder,
Opp.fltDiscount,
ISNULL(Opp.bitDiscountType,0) bitDiscountType,
dbo.fn_UOMConversion(Opp.numUOMId, Opp.numItemCode,' + CAST(@numDomainId AS VARCHAR) +', NULL) AS UOMConversionFactor,
ISNULL(Opp.bitWorkOrder,0) bitWorkOrder,
ISNULL(Opp.numUnitHourReceived,0) numUnitHourReceived,
ISNULL((SELECT COUNT(*) FROM OppWarehouseSerializedItem WHERE numOppId=Opp.numOppID AND numOppItemID=Opp.numoppitemtCode),0) AS numSerialNoAssigned,
CASE WHEN (SELECT COUNT(*) FROM OpportunityBizDocs OB JOIN dbo.OpportunityBizDocItems OBI ON ob.numOppBizDocsId=obi.numOppBizDocID
					AND (ob.bitAuthoritativeBizDocs=1 OR ob.numBizDocId=304) AND obi.numOppItemID=Opp.numoppitemtCode AND ob.numOppId=Opp.numOppId)>0 THEN 1 ELSE 0 END  AS bitIsAuthBizDoc,
(CASE WHEN (SELECT COUNT(*) FROM OpportunityBizDocs OB JOIN dbo.OpportunityBizDocItems OBI ON OB.numOppBizDocsId=OBI.numOppBizDocID WHERE OB.numOppId = Opp.numOppId AND OBI.numOppItemID=Opp.numoppitemtCode AND OB.numBizDocId=296) > 0 THEN 1 ELSE 0 END) AS bitAddedFulFillmentBizDoc,
ISNULL(numPromotionID,0) AS numPromotionID,
ISNULL(( SELECT numReturnID FROM   Returns WHERE  numOppItemCode = Opp.numoppitemtCode AND numOppId = Opp.numOppID), 0) AS numReturnID,
dbo.fn_GetReturnItemQty(Opp.numoppitemtCode) AS ReturrnedQty,
[dbo].fn_GetReturnStatus(Opp.numoppitemtCode) AS ReturnStatus,
ISNULL(opp.numUOMId, 0) AS numUOM,
(SELECT vcUnitName FROM dbo.UOM WHERE numUOMId = opp.numUOMId AND ISNULL(bitEnabled,0) = 1)  AS  [vcBaseUOMName],
Opp.numUnitHour AS numOriginalUnitHour, 
isnull(M.vcPOppName,''Internal'') as Source,
numSourceID,
  CASE WHEN ISNULL(bitKitParent,0) = 1
                               AND ISNULL(bitAssembly,0) = 1 THEN 0
                          WHEN ISNULL(bitKitParent,0) = 1 THEN 1
                          ELSE 0
                     END as bitKitParent,
(CASE WHEN OM.tintOppType = 1 AND OM.tintOppStatus = 1 THEN 
(CASE WHEN (I.charItemType = ''p'' AND ISNULL(bitDropShip, 0) = 0 AND ISNULL(bitAsset,0)=0)THEN dbo.CheckOrderItemInventoryStatus(Opp.numItemCode,Opp.numUnitHour,Opp.numoppitemtCode,Opp.numWarehouseItmsID,(CASE WHEN ISNULL(bitKitParent,0) = 1
                               AND ISNULL(bitAssembly,0) = 1 THEN 0
                          WHEN ISNULL(bitKitParent,0) = 1 THEN 1
                          ELSE 0
                     END)) ELSE 0 END) ELSE 0 END) AS bitBackOrder,
(SELECT vcData FROM dbo.ListDetails WHERE 1=1 AND numListItemID = numShipClass ) AS [numShipClass],ISNULL(Opp.numQtyShipped,0) numQtyShipped,ISNULL(Opp.numUnitHourReceived,0) AS numUnitHourReceived,ISNULL(OM.fltExchangeRate,0) AS fltExchangeRate,ISNULL(OM.numCurrencyID,0) AS numCurrencyID,ISNULL(OM.numDivisionID,0) AS numDivisionID,
(CASE 
	WHEN OM.tintOppType = 1 AND OM.tintOppStatus = 1 AND UPPER(I.charItemType) = ''P'' AND bitDropShip = 0
	THEN (CASE WHEN OM.tintShipped = 1 THEN CAST(ISNULL(Opp.numUnitHour,0) AS VARCHAR) ELSE CAST(ISNULL(Opp.numQtyShipped,0) AS VARCHAR) END)
	WHEN OM.tintOppType = 2 AND OM.tintOppStatus = 1 AND UPPER(I.charItemType) = ''P'' AND bitDropShip = 0
	THEN (CASE WHEN OM.tintShipped = 1 THEN CAST(ISNULL(Opp.numUnitHour,0) AS VARCHAR) ELSE CAST(ISNULL(Opp.numUnitHourReceived,0) AS VARCHAR) END)
	ELSE '''' END) AS vcShippedReceived,
isnull(i.numIncomeChartAcntId,0) as itemIncomeAccount,isnull(i.numAssetChartAcntId,0) as itemInventoryAsset,isnull(i.numCOGsChartAcntId,0) as itemCoGs,
ISNULL(Opp.numRentalIN,0) as numRentalIN,
ISNULL(Opp.numRentalLost,0) as numRentalLost,
ISNULL(Opp.numRentalOut,0) as numRentalOut,
(SELECT vcData FROM dbo.ListDetails WHERE 1=1 AND numListItemID = numShipClass ) AS [numShipClass],ISNULL(bitFreeShipping,0) AS [IsFreeShipping],Opp.bitDiscountType,isnull(Opp.monLandedCost,0) as monLandedCost, ISNULL(Opp.vcPromotionDetail,'''') AS vcPromotionDetail,
(CASE WHEN I.charItemType = ''p'' AND ISNULL(bitDropShip, 0) = 0 THEN dbo.GetOppItemReleaseDates(om.numDomainId,Opp.numOppId,Opp.numoppitemtCode,1) ELSE '''' END) AS vcItemReleaseDate,'''' AS numPurchasedQty, '

--	IF @fld_Name = 'vcPathForTImage' 
		SET @strSQL = @strSQL + 'Opp.vcPathForTImage,'
--	ELSE IF @fld_Name = 'vcItemName' 
		SET @strSQL = @strSQL + 'Opp.vcItemName,'
--	ELSE IF @fld_Name = 'vcModelID,' 
		SET @strSQL = @strSQL + 'Opp.vcModelID,'
--	ELSE IF @fld_Name = 'vcWareHouse' 
		SET @strSQL = @strSQL + 'vcWarehouse,WItems.numWarehouseID,'
		SET @strSQL = @strSQL + 'WItems.numOnHand,isnull(WItems.numAllocation,0) as numAllocation,'
		SET @strSQL = @strSQL + 'WL.vcLocation,'
--	ELSE IF @fld_Name = 'ItemType' 
		SET @strSQL = @strSQL + 'Opp.vcType ItemType,Opp.vcType,'
--	ELSE IF @fld_Name = 'vcItemDesc' 
		SET @strSQL = @strSQL + 'ISNULL(Opp.vcItemDesc, '''') vcItemDesc,'
--	ELSE IF @fld_Name = 'Attributes' 
		SET @strSQL = @strSQL + 'Opp.vcAttributes,' --dbo.fn_GetAttributes(Opp.numWarehouseItmsID, I.bitSerialized) AS Attributes,
--	ELSE IF @fld_Name = 'bitDropShip' 
		SET @strSQL = @strSQL + 'ISNULL(bitDropShip, 0) as DropShip,CASE ISNULL(bitDropShip, 0) WHEN 1 THEN ''Yes'' ELSE '''' END AS bitDropShip,'
--	ELSE IF @fld_Name = 'numUnitHour' 
		SET @strSQL = @strSQL + 'CAST(( dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), Opp.numItemCode,om.numDomainId, ISNULL(opp.numUOMId, 0))* Opp.numUnitHour ) AS NUMERIC(18, 2)) AS numUnitHour,'
--	ELSE IF @fld_Name = 'monPrice' 
		SET @strSQL = @strSQL + 'Opp.monPrice,'
--	ELSE IF @fld_Name = 'monPriceUOM' 
		SET @strSQL = @strSQL + 'CAST(( dbo.fn_UOMConversion(ISNULL(opp.numUOMId, 0), Opp.numItemCode,om.numDomainId, ISNULL(I.numBaseUnit, 0)) * Opp.monPrice) AS NUMERIC(18, 4)) AS monPriceUOM,'
--	ELSE IF @fld_Name = 'monTotAmount' 
		SET @strSQL = @strSQL + 'Opp.monTotAmount,Opp.monTotAmtBefDiscount,'
--	ELSE IF @fld_Name = 'vcSKU' 
		SET @strSQL = @strSQL + 'ISNULL(WItems.vcWHSKU,ISNULL(I.vcSKU,'''')) [vcSKU],'
--	ELSE IF @fld_Name = 'vcManufacturer' 
		SET @strSQL = @strSQL + 'Opp.vcManufacturer,'
--	ELSE IF @fld_Name = 'vcItemClassification' 
		SET @strSQL = @strSQL + ' dbo.[fn_GetListItemName](I.[numItemClassification]) numItemClassification,'
--	ELSE IF @fld_Name = 'numItemGroup' 
		SET @strSQL = @strSQL + '( SELECT    [vcItemGroup] FROM      [ItemGroups] WHERE     [numItemGroupID] = I.numItemGroup ) numItemGroup,'
--	ELSE IF @fld_Name = 'vcUOMName' 
		SET @strSQL = @strSQL + 'ISNULL(u.vcUnitName, '''') numUOMId,'
--	ELSE IF @fld_Name = 'fltWeight' 
		SET @strSQL = @strSQL + 'I.fltWeight,'
--	ELSE IF @fld_Name = 'fltHeight' 
		SET @strSQL = @strSQL + 'I.fltHeight,'
--	ELSE IF @fld_Name = 'fltWidth' 
		SET @strSQL = @strSQL + 'I.fltWidth,'
--	ELSE IF @fld_Name = 'fltLength' 
		SET @strSQL = @strSQL + 'I.fltLength,'
--	ELSE IF @fld_Name = 'numBarCodeId' 
		SET @strSQL = @strSQL + 'I.numBarCodeId,'
		SET @strSQL = @strSQL + 'I.IsArchieve,ISNULL(I.numContainer,0) AS numContainer,ISNULL(I.numNoItemIntoContainer,0) AS numContainerQty,'
--	ELSE IF @fld_Name = 'vcWorkOrderStatus' 
		SET @strSQL = @strSQL + 'CASE WHEN ISNULL(Opp.bitWorkOrder, 0) = 1
				 THEN ( SELECT  dbo.GetListIemName(numWOStatus)
								+ CASE WHEN ISNULL(numWOStatus, 0) = 23184
									   THEN '' - ''
											+ CAST(ISNULL(dbo.fn_GetContactName(numAssignedTo),
														  '''') + '',''
											+ CONVERT(VARCHAR(20), DATEADD(minute, '+ CONVERT(VARCHAR(15),-@ClientTimeZoneOffset)  +', bintCompletedDate)) AS VARCHAR(100))
									   ELSE ''''
								  END
						FROM    WorkOrder
						WHERE   ISNULL(numOppId, 0) = Opp.numOppId
								AND numItemCode = Opp.numItemCode AND ISNULL(numParentWOID,0)=0
					  )
				 ELSE ''''
			END AS vcWorkOrderStatus,'
--	ELSE IF @fld_Name = 'DiscAmt' 
		SET @strSQL = @strSQL + 'CASE WHEN Opp.bitDiscountType = 0
				 THEN REPLACE(STR(ISNULL(Opp.monTotAmtBefDiscount, Opp.monTotAmount)
					  - Opp.monTotAmount,20,4), '' '', '''') + '' (''
					  + CAST(FORMAT(Opp.fltDiscount,''0.00##'') AS VARCHAR(50)) + ''%)''
				 ELSE REPLACE(STR(ISNULL(Opp.monTotAmtBefDiscount, Opp.monTotAmount)
					  - Opp.monTotAmount,20,4), '' '', '''')
			END AS DiscAmt,'	
--	ELSE IF @fld_Name = 'numProjectID' 
		SET @strSQL = @strSQL + ' ISNULL(Opp.numProjectID, 0) numProjectID,
		 CASE WHEN ( SELECT TOP 1
								ISNULL(OBI.numOppBizDocItemID, 0)
						FROM    dbo.OpportunityBizDocs OBD
								INNER JOIN dbo.OpportunityBizDocItems OBI ON OBD.numOppBizDocsID = OBI.numOppBizDocID
						WHERE   OBI.numOppItemID = Opp.numoppitemtCode
								AND ISNULL(OBD.bitAuthoritativeBizDocs, 0) = 1
								and isnull(OBD.numOppID,0)=OM.numOppID
					  ) > 0 THEN 1
				 ELSE 0
			END AS bitItemAddedToAuthBizDoc,'	
--	ELSE IF @fld_Name = 'numClassID' 
		SET @strSQL = @strSQL + 'ISNULL(Opp.numClassID, 0) numClassID,'	
--	ELSE IF @fld_Name = 'vcProjectStageName' 
		SET @strSQL = @strSQL + ' ISNULL(Opp.numProjectStageID, 0) numProjectStageID,
				CASE WHEN OPP.numProjectStageID > 0
				 THEN dbo.fn_GetProjectStageName(OPP.numProjectId,
												 OPP.numProjectStageID)
				 WHEN OPP.numProjectStageID = -1 THEN ''T&E''
				 WHEN OPP.numProjectId > 0
				 THEN CASE WHEN OM.tintOppType = 1 THEN ''S.O.''
						   ELSE ''P.O.''
					  END
				 ELSE ''-''
			END AS vcProjectStageName,'	
	   
	   IF EXISTS (SELECT * FROM #tempForm WHERE vcOrigDbColumnName='vcInclusionDetails')
	   BEGIN
			SET @strSQL = @strSQL + 'dbo.GetOrderAssemblyKitInclusionDetails(OM.numOppID,Opp.numoppitemtCode,Opp.numUnitHour) AS vcInclusionDetails,'
	   END     
	
	-- (CASE WHEN @tintOppType=1 THEN 26 ELSE 129 END) --

--------------Add custom fields to query----------------
SELECT TOP 1 @fld_Name= vcFieldName,@fld_ID = numFieldId FROM View_DynamicCustomColumns
WHERE 
grp_id = 5 AND numDomainID=@numDomainId
AND numFormId=26 AND bitCustom=1 AND numUserCntID = @numUserCntID ORDER BY numFieldId
WHILE @fld_ID > 0
BEGIN ------------------

	 PRINT @fld_Name
	
	       SET @strSQL = @strSQL + 'dbo.GetCustFldValueOppItems('
                                    + CONVERT(VARCHAR(15), @fld_ID)
                                    + ',Opp.numoppitemtCode,Opp.numItemCode) as ''' + @fld_Name + ''','
       

SELECT TOP 1 @fld_Name= vcFieldName,@fld_ID = numFieldId FROM View_DynamicCustomColumns
WHERE 
grp_id = 5 AND numDomainID=@numDomainId
AND numFormId=26 AND bitCustom=1  AND numUserCntID = @numUserCntID AND numFieldId > @fld_ID ORDER BY numFieldId
 IF @@ROWCOUNT=0
 SET @fld_ID = 0
END ------------------


                                
--Purchase Opportunity ?
IF @tintOppType = 2 
    BEGIN
        SET @strSQL =  @strSQL + 'ISNULL((SELECT sum(numQty) FROM OppWarehouseSerializedItem WHERE numOppID=OM.numOppID AND numOppItemID=Opp.numoppitemtCode),0) AS SerialLotNo,
		  SUBSTRING(( SELECT  '', '' + vcSerialNo
                        + CASE WHEN ISNULL(I.bitLotNo, 0) = 1
                                THEN '' (''
                                    + CONVERT(VARCHAR(15), oppI.numQty)
                                    + '')''
                                ELSE ''''
                            END
                FROM    OppWarehouseSerializedItem oppI
                        JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID = whi.numWareHouseItmsDTLID
                WHERE   oppI.numOppID = Opp.numOppId
                        AND oppI.numOppItemID = Opp.numoppitemtCode
                ORDER BY vcSerialNo
                FOR
                XML PATH('''')
                ), 3, 200000) AS vcSerialLotNo,
		  CAST(Opp.numUnitHour AS FLOAT) numQty,
		  (SELECT vcUnitName FROM dbo.UOM WHERE numUOMId = opp.numUOMId AND ISNULL(bitEnabled,0) = 1)  AS  [vcBaseUOMName],
		  I.bitSerialized,
		  ISNULL(I.bitLotNo, 0) AS bitLotNo,
		  0 AS bitBarcodePrint,
		  Opp.numWarehouseItmsID as numWarehouseItmsID,'
	END
ELSE
	BEGIN	
        SET @strSQL =  @strSQL + ' ISNULL((SELECT sum(numQty) FROM OppWarehouseSerializedItem WHERE numOppID=OM.numOppID AND numOppItemID=Opp.numoppitemtCode),0) AS SerialLotNo,
		   SUBSTRING(( SELECT  '', '' + vcSerialNo
                        + CASE WHEN ISNULL(I.bitLotNo, 0) = 1
                                THEN '' (''
                                    + CONVERT(VARCHAR(15), oppI.numQty)
                                    + '')''
                                ELSE ''''
                            END
                FROM    OppWarehouseSerializedItem oppI
                        JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID = whi.numWareHouseItmsDTLID
                WHERE   oppI.numOppID = Opp.numOppId
                        AND oppI.numOppItemID = Opp.numoppitemtCode
                ORDER BY vcSerialNo
                FOR
                XML PATH('''')
                ), 3, 200000) AS vcSerialLotNo,
		   CAST(Opp.numUnitHour AS FLOAT) numQty,
		  I.bitSerialized,
		  ISNULL(I.bitLotNo, 0) AS bitLotNo,
		  Opp.numWarehouseItmsID as numWarehouseItmsID,'
	END


        
 SET @strSQL = left(@strSQL,len(@strSQL)-1) + '   
        FROM    OpportunityItems Opp
        LEFT JOIN OpportunityMaster OM ON Opp.numOppID = oM.numOppID
        JOIN item I ON Opp.numItemCode = i.numItemcode
        LEFT JOIN WareHouseItems WItems ON WItems.numWareHouseItemID = Opp.numWarehouseItmsID
        LEFT JOIN Warehouses W ON W.numWarehouseID = WItems.numWarehouseID
        LEFT JOIN WarehouseLocation WL ON WL.numWLocationID = WItems.numWLocationID
        LEFT JOIN OpportunityMaster M ON Opp.numSourceID = M.numOppID
        LEFT JOIN UOM u ON u.numUOMId = opp.numUOMId
		LEFT JOIN Vendor V ON I.numVendorID = V.numVendorID  AND V.numItemCode = I.numItemCode
WHERE   om.numDomainId = '+ CONVERT(VARCHAR(10),@numDomainId) + '
        AND Opp.numOppId = '+ CONVERT(VARCHAR(15),@OpportunityId) +'
ORDER BY opp.numSortOrder,numoppitemtCode ASC '
  
PRINT @strSQL;
EXEC (@strSQL);
SELECT  vcSerialNo,
       vcComments AS Comments,
        numOppItemID AS numoppitemtCode,
        W.numWarehouseItmsDTLID,
        numWarehouseItmsID AS numWItmsID,
        dbo.Fn_getattributes(W.numWarehouseItmsDTLID,
                             1) Attributes
FROM    WareHouseItmsDTL W
        JOIN OppWarehouseSerializedItem O ON O.numWarehouseItmsDTLID = W.numWareHouseItmsDTLID
WHERE   numOppID = @OpportunityId


select * from #tempForm order by tintOrder

drop table #tempForm

            END
            
    END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_SalesFulfillmentWorkflow')
DROP PROCEDURE dbo.USP_SalesFulfillmentWorkflow
GO
CREATE PROCEDURE [dbo].[USP_SalesFulfillmentWorkflow]
    (
	  @numDomainID NUMERIC(9),
      @numOppID NUMERIC(9),
      @numOppBizDocsId NUMERIC(9),
      @numUserCntID NUMERIC(9),
      @numBizDocStatus NUMERIC(9),
	  @numOppAuthBizDocsId as numeric(9)=0 OUTPUT,
	  @bitNotValidateBizDocFulfillment AS BIT
    )
AS 
    BEGIN
    DECLARE @ErrorMessage NVARCHAR(4000);SET @ErrorMessage=''
    DECLARE @ErrorSeverity INT;
    DECLARE @ErrorState INT;
    
BEGIN TRY		
		--DECLARE @numBizDocStatus AS NUMERIC(9);SET @numBizDocStatus=0
		DECLARE @numBizDocId AS NUMERIC(9);SET @numBizDocId=0
		DECLARE @numRuleID AS NUMERIC(9);SET @numRuleID=0
		DECLARE @numPackingSlipBizDocId NUMERIC(9),@numAuthInvoice NUMERIC(9)
		DECLARE @tintOppType NUMERIC(9);SET @tintOppType=0
	   
		SELECT /*@numBizDocStatus=ISNULL(OBD.numBizDocStatus,0),*/@tintOppType=tintOppType,@numBizDocId=OBD.numBizDocId FROM dbo.OpportunityMaster OM JOIN dbo.OpportunityBizDocs OBD ON OM.numOppID=OBD.numOppID
				WHERE OM.numDomainId=@numDomainId AND OM.numOppID=@numOppID AND OBD.numOppBizDocsId=@numOppBizDocsId 
				/*AND ISNULL(OBD.numBizDocStatus,0)!=0 AND ISNULL(OBD.numBizDocStatus,0)!=ISNULL(OBD.numBizDocStatusOLD,0)*/
		
		IF @tintOppType=1
		BEGIN
		IF @numBizDocStatus>0 AND @numBizDocId>0
		BEGIN
			IF @numBizDocId=296
			BEGIN
				SELECT @numRuleID=ISNULL(numRuleID,0) FROM dbo.OpportunityAutomationRules WHERE numDomainID=@numDomainID AND numBizDocStatus1=@numBizDocStatus AND numRuleID IN (1,2,3,4,5,6)
				
				IF @numRuleID=1 --create an invoice for all items within the fulfillment order
				BEGIN
					Select @numAuthInvoice=isnull(numAuthoritativeSales,0) From dbo.AuthoritativeBizDocs Where AuthoritativeBizDocs.numDomainId=@numDomainId 
					
					EXEC dbo.USP_CreateBizDocs
							@numOppId = @numOppID, --  numeric(9, 0)
							@numBizDocId = @numAuthInvoice, --  numeric(9, 0)
							@numUserCntID = @numUserCntID, --  numeric(9, 0)
							@numOppBizDocsId = @numOppAuthBizDocsId output,
							@vcComments = '', --  varchar(1000)
							@bitPartialFulfillment = 1, --  bit
							@strBizDocItems = '', --  text
							--@monShipCost = 0, --  money
							@numShipVia = 0, --  numeric(9, 0)
							@vcTrackingURL = '', --  varchar(1000)
							@dtFromDate = '2013-1-2 10:55:29.610', --  datetime
							@numBizDocStatus = 0, --  numeric(9, 0)
							@bitRecurringBizDoc = 0, --  bit
							@numSequenceId = '', --  varchar(50)
							@tintDeferred = 0, --  tinyint
							@monCreditAmount =0,
							@ClientTimeZoneOffset = 0, --  int
							@monDealAmount =0,
							@bitRentalBizDoc = 0, --  bit
							@numBizDocTempID = 0, --  numeric(9, 0)
							@vcTrackingNo = '', --  varchar(500)
							--@vcShippingMethod = '', --  varchar(100)
							@vcRefOrderNo = '', --  varchar(100)
							--@dtDeliveryDate = '2013-1-2 10:55:29.611', --  datetime
							@OMP_SalesTaxPercent = 0, --  float
							@numFromOppBizDocsId = @numOppBizDocsId, --  numeric(9, 0)
							@bitTakeSequenceId = 1,  --bit
							@bitNotValidateBizDocFulfillment=@bitNotValidateBizDocFulfillment
					
				END
				ELSE IF @numRuleID=2 --create a packing slip for its inventory items
				BEGIN
					SELECT @numPackingSlipBizDocId=numListItemID FROM dbo.ListDetails WHERE ListDetails.vcData='Packing Slip' AND ListDetails.constFlag=1 AND ListDetails.numListID=27
				
					EXEC dbo.USP_CreateBizDocs
								@numOppId = @numOppID, --  numeric(9, 0)
								@numBizDocId = @numPackingSlipBizDocId, --  numeric(9, 0)
								@numUserCntID = @numUserCntID, --  numeric(9, 0)
								@numOppBizDocsId =0,
								@vcComments = '', --  varchar(1000)
								@bitPartialFulfillment = 1, --  bit
								@strBizDocItems = '', --  text
								--@monShipCost = 0, --  money
								@numShipVia = 0, --  numeric(9, 0)
								@vcTrackingURL = '', --  varchar(1000)
								@dtFromDate = '2013-1-2 10:56:4.477', --  datetime
								@numBizDocStatus = 0, --  numeric(9, 0)
								@bitRecurringBizDoc = 0, --  bit
								@numSequenceId = '', --  varchar(50)
								@tintDeferred = 0, --  tinyint
								@monCreditAmount =0,
								@ClientTimeZoneOffset = 0, --  int
								@monDealAmount =0,
								@bitRentalBizDoc = 0, --  bit
								@numBizDocTempID = 0, --  numeric(9, 0)
								@vcTrackingNo = '', --  varchar(500)
								--@vcShippingMethod = '', --  varchar(100)
								@vcRefOrderNo = '', --  varchar(100)
								--@dtDeliveryDate = '2013-1-2 10:56:4.477', --  datetime
								@OMP_SalesTaxPercent = 0, --  float
								@numFromOppBizDocsId = @numOppBizDocsId, --  numeric(9, 0)
								@bitTakeSequenceId = 1,  --bit
								@bitNotValidateBizDocFulfillment=@bitNotValidateBizDocFulfillment
				END
				ELSE IF @numRuleID=3 --release its inventory items from allocation
				BEGIN
					BEGIN TRY		
						EXEC dbo.USP_SalesFulfillmentUpdateInventory
							@numDomainID = @numDomainID, --  numeric(9, 0)
							@numOppID = @numOppID, --  numeric(9, 0)
							@numOppBizDocsId = @numOppBizDocsId, --  numeric(9, 0)
							@numUserCntID = @numUserCntID,  --  numeric(9, 0) 
							@tintMode = 1 --  tinyint
					END TRY
					BEGIN CATCH
					SELECT @ErrorMessage = ERROR_MESSAGE(),
								       @ErrorSeverity = ERROR_SEVERITY(),
									   @ErrorState = ERROR_STATE();
									   
						IF @ErrorMessage='NotSufficientQty_Allocation'
						BEGIN
							SELECT @numBizDocStatus=ISNULL(numBizDocStatus1,0) FROM dbo.OpportunityAutomationRules 
								WHERE numDomainID=@numDomainID AND ISNULL(numRuleID,0)=6
							
							EXEC dbo.USP_UpdateSingleFieldValue
								@tintMode = 27, --  tinyint
								@numUpdateRecordID = @numOppBizDocsId, --  numeric(18, 0)
								@numUpdateValueID = @numBizDocStatus, --  numeric(18, 0)
								@vcText = '', --  text
								@numDomainID = @numDomainID --  numeric(18, 0)
								
								INSERT INTO dbo.OppFulfillmentBizDocsStatusHistory (
										numDomainId,numOppId,numOppBizDocsId,numBizDocStatus,numUserCntID,dtCreatedDate
										) VALUES ( 
										@numDomainID,@numOppID,@numOppBizDocsId,@numBizDocStatus,@numUserCntID,GETUTCDATE() ) 
						END	

						RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState);
						
					END CATCH;		
				END
				ELSE IF @numRuleID=4 --return its inventory items back to allocation
				BEGIN
					Select @numAuthInvoice=isnull(numAuthoritativeSales,0) From dbo.AuthoritativeBizDocs Where AuthoritativeBizDocs.numDomainId=@numDomainId 
				
						IF EXISTS (SELECT numOppBizDocsId FROM OpportunityMaster OM JOIN OpportunityBizDocs OBD ON OM.numOppID=OBD.numOppID
							WHERE OM.numDomainId=@numDomainId AND OM.numOppID=@numOppID  AND OBD.numBizDocId IN (@numAuthInvoice))
						BEGIN
								RAISERROR ('AlreadyInvoice_DoNotReturn',16,1);
								RETURN -1
						END		
						ELSE
						BEGIN
								EXEC dbo.USP_SalesFulfillmentUpdateInventory
							@numDomainID = @numDomainID, --  numeric(9, 0)
							@numOppID = @numOppID, --  numeric(9, 0)
							@numOppBizDocsId = @numOppBizDocsId, --  numeric(9, 0)
							@numUserCntID = @numUserCntID,  --  numeric(9, 0) 
							@tintMode = 2 --  tinyint
						END
				END
				ELSE IF @numRuleID=5 --Remove the Fulfillment Order from the Sales Fulfillment queue
				BEGIN
					PRINT 'Rule-5'
				
				END
			END
		END
		
		IF @numBizDocStatus>0
		BEGIN
				INSERT INTO dbo.OppFulfillmentBizDocsStatusHistory (
					numDomainId,
					numOppId,
					numOppBizDocsId,
					numBizDocStatus,
					numUserCntID,
					dtCreatedDate
				) VALUES ( 
					/* numDomainId - numeric(18, 0) */ @numDomainID,
					/* numOppId - numeric(18, 0) */ @numOppID,
					/* numOppBizDocsId - numeric(18, 0) */ @numOppBizDocsId,
					/* numBizDocStatus - numeric(18, 0) */ @numBizDocStatus,
					/* numUserCntID - numeric(18, 0) */ @numUserCntID,
					/* dtCreatedDate - datetime */ GETUTCDATE() )  
		 END			
	
	END
			 END TRY
BEGIN CATCH

    SELECT 
        @ErrorMessage = ERROR_MESSAGE(),
        @ErrorSeverity = ERROR_SEVERITY(),
        @ErrorState = ERROR_STATE();

    -- Use RAISERROR inside the CATCH block to return error
    -- information about the original error that caused
    -- execution to jump to the CATCH block.
    RAISERROR (@ErrorMessage, -- Message text.
               @ErrorSeverity, -- Severity.
               @ErrorState -- State.
               );
END CATCH;
		
    END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetCartItemPromotionDetail')
DROP PROCEDURE USP_GetCartItemPromotionDetail
GO
CREATE PROCEDURE [dbo].[USP_GetCartItemPromotionDetail]
	@numDomainID NUMERIC(18,0),
	@numItemCode NUMERIC(18,0),
	@numUOMID NUMERIC(18,0),
	@numShippingCountry AS NUMERIC(18,0),
	@cookieId VARCHAR(MAX)=NULL,
	@numUserCntId numeric=0,
	@vcSendCoupon VARCHAR(200)=0
AS
BEGIN
	DECLARE @vPromotionDesc VARCHAR(MAX)
	IF(@vcSendCoupon='0')
	BEGIN

	DECLARE @vcItemName AS VARCHAR(500)
	DECLARE @chrItemType AS CHAR(1)
	DECLARE @numItemGroup AS NUMERIC(18,0)
	DECLARE @ItemDesc AS VARCHAR(1000)
	DECLARE @numItemClassification AS NUMERIC(18,0)

	DECLARE @numWarehouseItemID NUMERIC(18,0)

	
	DECLARE @decmPrice DECIMAL(18,2)
	SET @decmPrice=(select ISNULL(SUM(monTotAmount),0) from CartItems where numDomainId=@numDomainID AND vcCookieId=@cookieId AND numUserCntId=@numUserCntId)

	DECLARE @numShippingPromotionID AS NUMERIC(18,0)=0
	DECLARE @numPromotionID AS NUMERIC(18,0)=0

	SET @numShippingPromotionID=(SELECT TOP 1 PromotionID from CartItems AS C 
	LEFT JOIN PromotionOffer AS P ON C.PromotionID=P.numProId
	LEFT JOIN ShippingPromotions SP ON C.numDomainId = SP.numDomainId
	where C.numDomainId=@numDomainID AND C.vcCookieId=@cookieId AND C.numUserCntId=@numUserCntId
	AND (ISNULL(SP.bitFixShipping1,0)=1 OR ISNULL(SP.bitFixShipping2,0)=1 OR ISNULL(SP.bitFreeShiping,0)=1)  AND ISNULL(C.bitParentPromotion,0)=1
	AND 1=(CASE WHEN P.tintOfferTriggerValueType=1 AND C.numUnitHour>=fltOfferTriggerValue THEN 1 WHEN P.tintOfferTriggerValueType=2 AND monTotAmount>=fltOfferTriggerValue THEN 1 ELSE 0 END))
	PRINT(@decmPrice)

	IF(ISNULL(@numPromotionID,0)=0)
	BEGIN
	-- GET ITEM DETAILS
	SELECT
		@vcItemName = vcItemName,
		@ItemDesc = Item.txtItemDesc,
		@chrItemType = charItemType,
		@numItemGroup = numItemGroup,
		@numItemClassification = numItemClassification,
		@numWarehouseItemID = (CASE WHEN ISNULL(@numWarehouseItemID,0) > 0 THEN @numWarehouseItemID ELSE WareHouseItems.numWareHouseItemID END)
	FROM
		Item
	OUTER APPLY
	(
		SELECT TOP 1 numWareHouseItemID,monWListPrice FROM WareHouseItems WHERE numItemID=@numItemCode AND (numWareHouseItemID = @numWarehouseItemID OR ISNULL(@numWarehouseItemID,0) = 0)
	) AS WareHouseItems
	WHERE
		Item.numItemCode = @numItemCode


	--IF @chrItemType = 'P' AND ISNULL(@numWarehouseItemID,0) = 0
	--BEGIN
	--	RAISERROR('WAREHOUSE_DOES_NOT_EXISTS',16,1)
	--END

	

	-- CHECK IF PROMOTION CAN BE TRIGGERED FROM THIS ITEM
	
	DECLARE @bitCoupon bit
	DECLARE @vcCouponCode VARCHAR(200)
	SELECT TOP 1
		@numPromotionID = numProId 
	FROM 
		PromotionOffer PO
	WHERE 
		numDomainId=@numDomainID 
		AND ISNULL(bitEnabled,0)=1
		AND ISNULL(bitAppliesToSite,0)=1 
		AND 1 = (CASE WHEN bitRequireCouponCode = 1 THEN CASE WHEN ISNULL(intCouponCodeUsed,0) < ISNULL(tintUsageLimit,0) OR ISNULL(tintUsageLimit,0) = 0  THEN 1 ELSE 0 END ELSE 1 END)
		AND 1 = (CASE WHEN bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN CAST(GETDATE() AS DATE)>=dtValidFrom AND CAST(GETDATE() AS DATE)<=dtValidTo THEN 1 ELSE 0 END) END)
		AND (1 = (CASE 
					WHEN tintOfferBasedOn = 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 AND numValue=@numItemCode) > 0 THEN 1 ELSE 0 END)
					WHEN tintOfferBasedOn = 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=2 AND numValue=@numItemClassification) > 0 THEN 1 ELSE 0 END)
					ELSE 0
				END)
			OR
			1 = (CASE 
					WHEN tintDiscoutBaseOn = 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=6 AND tintType=1 AND numValue=@numItemCode) > 0 THEN 1 ELSE 0 END)
					WHEN tintDiscoutBaseOn = 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=6 AND tintType=2 AND numValue=@numItemClassification) > 0 THEN 1 ELSE 0 END)
					WHEN tintDiscoutBaseOn = 3 AND tintOfferBasedOn = 1 THEN (CASE WHEN (SELECT COUNT(SimilarItems.numItemCode) FROM SimilarItems WHERE numParentItemCode IN (SELECT numValue FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 AND SimilarItems.numItemCode=@numItemCode))>0 THEN 1 ELSE 0 END)
					WHEN tintDiscoutBaseOn = 3 AND tintOfferBasedOn = 2 THEN (CASE WHEN (SELECT COUNT(SimilarItems.numItemCode) FROM SimilarItems WHERE SimilarItems.numItemCode IN(@numItemCode) AND numParentItemCode IN (SELECT numItemCode FROM Item WHERE numItemClassification IN (SELECT numValue FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=2)))>0 THEN 1 ELSE 0 END)
					ELSE 0
				END)
			)
	ORDER BY
		CASE 
			WHEN PO.txtCouponCode IN(SELECT vcCoupon FROM CartItems WHERE numUserCntId=@numUserCntId AND vcCookieId=@cookieId) THEN 1
			WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 1 THEN 2
			WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 1 THEN 3
			WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 2 THEN 4
			WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 2 THEN 5
			WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 3 THEN 6
			WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 3 THEN 7
		END

		END

	
	--IF ISNULL(@numPromotionID,0) > 0
	--BEGIN
		-- Get Top 1 Promotion Based on priority
		

		DECLARE @bitRequireCouponCode VARCHAR(200)

		DECLARE @tintOfferTriggerValueType INT
		DECLARE @fltOfferTriggerValue DECIMAL
		
		SET @vcCouponCode=(SELECT  STUFF((SELECT 
			','+txtCouponCode
		FROM 
			PromotionOffer PO
		WHERE 
			numDomainId=@numDomainID 
			AND ISNULL(bitRequireCouponCode,0)=1
			AND ISNULL(bitEnabled,0)=1
			AND ISNULL(bitAppliesToSite,0)=1 
			AND 1 = (CASE WHEN bitRequireCouponCode = 1 THEN CASE WHEN ISNULL(intCouponCodeUsed,0) < ISNULL(tintUsageLimit,0) OR ISNULL(tintUsageLimit,0) = 0  THEN 1 ELSE 0 END ELSE 1 END)
			AND 1 = (CASE WHEN bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN CAST(GETDATE() AS DATE)>=dtValidFrom AND CAST(GETDATE() AS DATE)<=dtValidTo THEN 1 ELSE 0 END) END)
			AND (1 = (CASE 
						WHEN tintOfferBasedOn = 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 AND numValue=@numItemCode) > 0 THEN 1 ELSE 0 END)
						WHEN tintOfferBasedOn = 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=2 AND numValue=@numItemClassification) > 0 THEN 1 ELSE 0 END)
						ELSE 0
					END)
				)
			AND (SELECT COUNT(numCartId) FROM CartItems WHERE vcCoupon=PO.txtCouponCode AND numDomainId=@numDomainID AND numUserCntId=@numUserCntId AND vcCookieId=@cookieId)=0
		ORDER BY
			CASE 
				WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 1 THEN 1
				WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 1 THEN 2
				WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 2 THEN 3
				WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 2 THEN 4
				WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 3 THEN 5
				WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 3 THEN 6
			END
		FOR
                XML PATH('')
              ), 1, 0, ''))
		SET @vcCouponCode= (SELECT SUBSTRING(@vcCouponCode,2,(LEN(@vcCouponCode))))

		

		IF(LEN(@vcCouponCode)>0)
		BEGIN
			SET @bitRequireCouponCode=1
		END

		SELECT @vPromotionDesc=( 
			CASE WHEN PO.bitRequireCouponCode=0 OR
			(PO.bitRequireCouponCode=1 AND (SELECT COUNT(*) FROM CartItems WHERE PromotionID=PO.numProId AND bitParentPromotion=1 AND vcCookieId=@cookieId AND numUserCntId=@numUserCntId)>0)
			
			THEN CONCAT('Buy '
					,CASE WHEN tintOfferTriggerValueType=1 THEN CAST(fltOfferTriggerValue AS VARCHAR) ELSE CONCAT('$',fltOfferTriggerValue) END
					,CASE tintOfferBasedOn
							WHEN 1 THEN CONCAT(' of "',(SELECT Stuff((SELECT CONCAT(', ',Item.vcItemName) FROM PromotionOfferItems INNER JOIN Item ON PromotionOfferItems.numValue = Item.numItemCode WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 FOR XML PATH('')), 1, 2, '')), '"')
							WHEN 2 THEN CONCAT(' of items belonging to classification(s) "',(SELECT Stuff((SELECT CONCAT(', ',ListDetails.vcData) FROM PromotionOfferItems INNER JOIN ListDetails ON PromotionOfferItems.numValue = ListDetails.numListItemID WHERE numListID=36 AND numProId=PO.numProId AND tintRecordType=5 AND tintType=2 FOR XML PATH('')), 1, 2, '')), '"')
						END
					,' & get '
					, CASE tintDiscountType WHEN 1 THEN CONCAT(fltDiscountValue,'% off on') WHEN 2 THEN CONCAT('$',fltDiscountValue, ' off on') WHEN 3 THEN CONCAT(fltDiscountValue,' of') END
					, CASE 
						WHEN tintDiscoutBaseOn = 1 THEN 
							CONCAT
							(
								CONCAT(' "',(SELECT Stuff((SELECT CONCAT(',',Item.vcItemName) FROM PromotionOfferItems INNER JOIN Item ON PromotionOfferItems.numValue = Item.numItemCode WHERE numProId=PO.numProId AND tintRecordType=6 AND tintType=1 FOR XML PATH('')), 1, 1, '')), '"')
								,(CASE WHEN PO.tintDiscountType = 3 THEN ' free' ELSE '' END)
								,'.'
							)
						WHEN tintDiscoutBaseOn = 2 THEN 
							CONCAT
							(
								CONCAT(' items belonging to classification(s) "',(SELECT Stuff((SELECT CONCAT(',',ListDetails.vcData) FROM PromotionOfferItems INNER JOIN ListDetails ON PromotionOfferItems.numValue = ListDetails.numListItemID WHERE numListID=36 AND numProId=PO.numProId AND tintRecordType=6 AND tintType=2 FOR XML PATH('')), 1, 1, '')), '"')
								,(CASE WHEN PO.tintDiscountType = 3 THEN ' free' ELSE '' END)
								,'.'
							)
						WHEN tintDiscoutBaseOn = 3 THEN ' related items.'
						ELSE '' 
					END
				)  ELSE '' END) 
				
		FROM
			PromotionOffer PO
		WHERE
			numDomainId=@numDomainID 
			AND numProId =@numPromotionID

		IF(LEN(@vcCouponCode)>0)
		BEGIN
			SET @bitRequireCouponCode=1
			SET @vPromotionDesc=''
		END


		IF (@numShippingPromotionID=0)
		BEGIN
			SET @numShippingPromotionID=@numPromotionID
		END
		
		DECLARE @vcShippingDescription VARCHAR(MAX)
		DECLARE @vcQualifiedShipping VARCHAR(MAX)


		SELECT @vcShippingDescription=(CONCAT
				(
					(CASE WHEN ISNULL(bitFixShipping1,0)=1 AND @decmPrice < monFixShipping1OrderAmount THEN CONCAT('Spend $',(monFixShipping1OrderAmount-@decmPrice), CASE WHEN @decmPrice>0 THEN ' more ' ELSE '' END+' and get $',monFixShipping1Charge,' shipping. ') ELSE '' END)
					,(CASE WHEN ISNULL(bitFixShipping2,0)=1 AND @decmPrice < monFixShipping2OrderAmount THEN CONCAT('Spend $',monFixShipping2OrderAmount-@decmPrice, CASE WHEN @decmPrice>0 THEN ' more ' ELSE '' END+' and get $',monFixShipping2Charge,' shipping. ') ELSE '' END)
					,(CASE WHEN (bitFreeShiping=1 AND numFreeShippingCountry=@numShippingCountry) AND @decmPrice<monFreeShippingOrderAmount THEN CONCAT('Spend $',monFreeShippingOrderAmount-@decmPrice, CASE WHEN @decmPrice>0 THEN ' more ' ELSE '' END+' and shipping is FREE! ') ELSE '' END) 
					
				)),
				@vcQualifiedShipping=((CASE WHEN ISNULL(bitFixShipping1,0)=1 AND @decmPrice > monFixShipping1OrderAmount AND 1=(CASE WHEN ISNULL(bitFixShipping2,0)=1 AND @decmPrice<monFixShipping2OrderAmount THEN 1 WHEN ISNULL(bitFixShipping2,0)=0 THEN 1 ELSE 0 END) THEN 'You have qualified for $'+CAST(monFixShipping1Charge AS varchar)+' shipping'
						   WHEN ISNULL(bitFixShipping2,0)=1 AND @decmPrice > monFixShipping2OrderAmount  AND 1=(CASE WHEN ISNULL(bitFreeShiping,0)=1 AND @decmPrice<monFreeShippingOrderAmount THEN 1 WHEN ISNULL(bitFreeShiping,0)=0 THEN 1 ELSE 0 END) THEN 'You have qualified for $'+CAST(monFixShipping2Charge AS varchar)+' shipping'
						   WHEN (ISNULL(bitFreeShiping,0)=1 AND numFreeShippingCountry=@numShippingCountry AND @decmPrice>monFreeShippingOrderAmount) THEN 'You have qualified for Free Shipping' END
					))
		FROM
			PromotionOffer PO
			LEFT JOIN ShippingPromotions SP ON PO.numDomainId = SP.numDomainId
		WHERE
			PO.numDomainId=@numDomainID 
			AND numProId = @numShippingPromotionID

		IF((SELECT COUNT(*) FROM CartItems WHERE numItemCode=@numItemCode AND numDomainId=@numDomainID AND numUserCntId=@numUserCntId AND vcCookieId=@cookieId AND PromotionID>0 AND @vcCouponCode<>'')>0)
		BEGIN
			SET @bitRequireCouponCode='APPLIED';
			SET @vcCouponCode=(SELECT TOP 1 vcCoupon FROM CartItems WHERE numItemCode=@numItemCode AND vcCookieId=@cookieId AND numUserCntId=@numUserCntId AND numDomainId=@numDomainID AND PromotionID>0 AND @vcCouponCode<>'')

			SELECT @vPromotionDesc=( 
				CONCAT('Buy '
						,CASE WHEN tintOfferTriggerValueType=1 THEN CAST(fltOfferTriggerValue AS VARCHAR) ELSE CONCAT('$',fltOfferTriggerValue) END
						,CASE tintOfferBasedOn
								WHEN 1 THEN CONCAT(' of "',(SELECT Stuff((SELECT CONCAT(', ',Item.vcItemName) FROM PromotionOfferItems INNER JOIN Item ON PromotionOfferItems.numValue = Item.numItemCode WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 FOR XML PATH('')), 1, 2, '')), '"')
								WHEN 2 THEN CONCAT(' of items belonging to classification(s) "',(SELECT Stuff((SELECT CONCAT(', ',ListDetails.vcData) FROM PromotionOfferItems INNER JOIN ListDetails ON PromotionOfferItems.numValue = ListDetails.numListItemID WHERE numListID=36 AND numProId=PO.numProId AND tintRecordType=5 AND tintType=2 FOR XML PATH('')), 1, 2, '')), '"')
							END
						,' & get '
						, CASE tintDiscountType WHEN 1 THEN CONCAT(fltDiscountValue,'% off on') WHEN 2 THEN CONCAT('$',fltDiscountValue, ' off on') WHEN 3 THEN CONCAT(fltDiscountValue,' of') END
						, CASE 
							WHEN tintDiscoutBaseOn = 1 THEN 
								CONCAT
								(
									CONCAT(' "',(SELECT Stuff((SELECT CONCAT(',',Item.vcItemName) FROM PromotionOfferItems INNER JOIN Item ON PromotionOfferItems.numValue = Item.numItemCode WHERE numProId=PO.numProId AND tintRecordType=6 AND tintType=1 FOR XML PATH('')), 1, 1, '')), '"')
									,(CASE WHEN PO.tintDiscountType = 3 THEN ' free' ELSE '' END)
									,'.'
								)
							WHEN tintDiscoutBaseOn = 2 THEN 
								CONCAT
								(
									CONCAT(' items belonging to classification(s) "',(SELECT Stuff((SELECT CONCAT(',',ListDetails.vcData) FROM PromotionOfferItems INNER JOIN ListDetails ON PromotionOfferItems.numValue = ListDetails.numListItemID WHERE numListID=36 AND numProId=PO.numProId AND tintRecordType=6 AND tintType=2 FOR XML PATH('')), 1, 1, '')), '"')
									,(CASE WHEN PO.tintDiscountType = 3 THEN ' free' ELSE '' END)
									,'.'
								)
							WHEN tintDiscoutBaseOn = 3 THEN ' related items.'
							ELSE '' 
						END
					))
				
			FROM
				PromotionOffer PO
			WHERE
				numDomainId=@numDomainID 
				AND txtCouponCode=@vcCouponCode
			PRINT @vPromotionDesc
		END

		SELECT @vPromotionDesc AS vcPromotionDescription,@vcShippingDescription AS vcShippingDescription,@vcQualifiedShipping AS vcQualifiedShipping,ISNULL(@bitRequireCouponCode,'False') AS bitRequireCouponCode,@vcCouponCode AS vcCouponCode,
		ISNULL(@tintOfferTriggerValueType,0) as tintOfferTriggerValueType,ISNULL(@fltOfferTriggerValue,0) as fltOfferTriggerValue

			PRINT @decmPrice
	END
	ELSE
	BEGIN
		SELECT 
			@bitRequireCouponCode=PO.bitRequireCouponCode,
			@tintOfferTriggerValueType=PO.tintOfferTriggerValueType,
			@fltOfferTriggerValue=PO.fltOfferTriggerValue

		FROM 
			PromotionOffer PO
		WHERE 
			numDomainId=@numDomainID 
			AND ISNULL(bitEnabled,0)=1
			AND ISNULL(bitAppliesToSite,0)=1 
			AND txtCouponCode=@vcSendCoupon

		SELECT @vPromotionDesc=( 
			CONCAT('Buy '
					,CASE WHEN tintOfferTriggerValueType=1 THEN CAST(fltOfferTriggerValue AS VARCHAR) ELSE CONCAT('$',fltOfferTriggerValue) END
					,CASE tintOfferBasedOn
							WHEN 1 THEN CONCAT(' of "',(SELECT Stuff((SELECT CONCAT(', ',Item.vcItemName) FROM PromotionOfferItems INNER JOIN Item ON PromotionOfferItems.numValue = Item.numItemCode WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 FOR XML PATH('')), 1, 2, '')), '"')
							WHEN 2 THEN CONCAT(' of items belonging to classification(s) "',(SELECT Stuff((SELECT CONCAT(', ',ListDetails.vcData) FROM PromotionOfferItems INNER JOIN ListDetails ON PromotionOfferItems.numValue = ListDetails.numListItemID WHERE numListID=36 AND numProId=PO.numProId AND tintRecordType=5 AND tintType=2 FOR XML PATH('')), 1, 2, '')), '"')
						END
					,' & get '
					, CASE tintDiscountType WHEN 1 THEN CONCAT(fltDiscountValue,'% off on') WHEN 2 THEN CONCAT('$',fltDiscountValue, ' off on') WHEN 3 THEN CONCAT(fltDiscountValue,' of') END
					, CASE 
						WHEN tintDiscoutBaseOn = 1 THEN 
							CONCAT
							(
								CONCAT(' "',(SELECT Stuff((SELECT CONCAT(',',Item.vcItemName) FROM PromotionOfferItems INNER JOIN Item ON PromotionOfferItems.numValue = Item.numItemCode WHERE numProId=PO.numProId AND tintRecordType=6 AND tintType=1 FOR XML PATH('')), 1, 1, '')), '"')
								,(CASE WHEN PO.tintDiscountType = 3 THEN ' free' ELSE '' END)
								,'.'
							)
						WHEN tintDiscoutBaseOn = 2 THEN 
							CONCAT
							(
								CONCAT(' items belonging to classification(s) "',(SELECT Stuff((SELECT CONCAT(',',ListDetails.vcData) FROM PromotionOfferItems INNER JOIN ListDetails ON PromotionOfferItems.numValue = ListDetails.numListItemID WHERE numListID=36 AND numProId=PO.numProId AND tintRecordType=6 AND tintType=2 FOR XML PATH('')), 1, 1, '')), '"')
								,(CASE WHEN PO.tintDiscountType = 3 THEN ' free' ELSE '' END)
								,'.'
							)
						WHEN tintDiscoutBaseOn = 3 THEN ' related items.'
						ELSE '' 
					END
				))
				
		FROM
			PromotionOffer PO
		WHERE
			numDomainId=@numDomainID 
			AND txtCouponCode=@vcSendCoupon

		SELECT @bitRequireCouponCode AS bitRequireCouponCode,@tintOfferTriggerValueType AS tintOfferTriggerValueType,@vPromotionDesc AS vPromotionDesc,
		@fltOfferTriggerValue AS fltOfferTriggerValue
	END
	--END
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetOrderSelectedItemDetail')
DROP PROCEDURE USP_GetOrderSelectedItemDetail
GO
CREATE PROCEDURE [dbo].[USP_GetOrderSelectedItemDetail]
	@numDomainID NUMERIC(18,0),
	@numDivisionID NUMERIC(18,0),
	@tintOppType AS TINYINT,
	@numItemCode NUMERIC(18,0),
	@numUOMID NUMERIC(18,0),
	@numWarehouseItemID AS NUMERIC(18,0),
	@numQty INT,
	@monPrice FLOAT,
	@numOppID NUMERIC(18,0),
	@numOppItemID NUMERIC(18,0),
	@vcSelectedKitChildItems VARCHAR(MAX),
	@numShippingCountry AS NUMERIC(18,0),
	@numWarehouseID AS NUMERIC(18,0),
	@vcCoupon AS VARCHAR(200)
AS
BEGIN
	DECLARE @vcItemName AS VARCHAR(500)
	DECLARE @bitDropship AS BIT
	DECLARE @bitMatrix AS BIT
	DECLARE @chrItemType AS CHAR(1)
	DECLARE @numItemGroup AS NUMERIC(18,0)
	DECLARE @numBaseUnit AS NUMERIC(18,0)
	DECLARE @numSaleUnit AS NUMERIC(18,0)
	DECLARE @fltUOMConversionFactor AS FLOAT
	DECLARE @numPurchaseUnit AS NUMERIC(18,0)
	DECLARE @bitAssemblyOrKit BIT
	DECLARE @bitAssembly AS BIT
	DECLARE @bitCalAmtBasedonDepItems AS BIT
	DECLARE @ItemDesc AS VARCHAR(1000)
	DECLARE @numItemClassification AS NUMERIC(18,0)

	DECLARE @monListPrice AS MONEY = 0.0000
	DECLARE @monVendorCost AS MONEY = 0.0000
	DECLARE @tintPricingBasedOn AS TINYINT = 0.0000

	DECLARE @tintRuleType AS TINYINT
	DECLARE @monPriceLevelPrice AS MONEY = 0.0000
	DECLARE @monPriceFinalPrice AS MONEY = 0.0000
	DECLARE @monPriceRulePrice AS MONEY = 0.0000
	DECLARE @monPriceRuleFinalPrice AS MONEY = 0.0000
	DECLARE @monLastPrice AS MONEY = 0.0000
	DECLARE @dtLastOrderDate VARCHAR(200) = ''

	DECLARE @tintDisountType TINYINT
	DECLARE @decDiscount AS FLOAT

	DECLARE @numVendorID AS NUMERIC(18,0)
	DECLARE @vcMinOrderQty AS VARCHAR(5) = '-'
	DECLARE @vcVendorNotes AS VARCHAR(300) = ''
	DECLARE @numRelationship NUMERIC(18,0)
	DECLARE @numProfile NUMERIC(18,0)
	DECLARE @tintPriceLevel AS TINYINT
	DECLARE @tintDefaultSalesPricing AS TINYINT
	DECLARE @tintPriceBookDiscount AS TINYINT

	IF ISNULL(@numDivisionID,0) > 0
	BEGIN
		SELECT @vcMinOrderQty=ISNULL(CAST(intMinQty AS VARCHAR),'-'),@vcVendorNotes=ISNULL(vcNotes,'') FROM Vendor WHERE numVendorID=@numDivisionID AND numItemCode=@numItemCode
	END

	SELECT @tintDefaultSalesPricing = numDefaultSalesPricing,@tintPriceBookDiscount=tintPriceBookDiscount FROM Domain WHERE numDomainId=@numDomainID

	IF ISNULL(@numWarehouseID,0) > 0
	BEGIN
		SELECT TOP 1 @numWarehouseItemID=numWareHouseItemID FROM WareHouseItems WHERE numItemID=@numItemCode AND numWareHouseID=@numWarehouseID ORDER BY numWareHouseItemID ASC
	END

	-- GET ORGANIZATION DETAL
	SELECT 
		@numRelationship=numCompanyType,
		@numProfile=vcProfile,
		@tintPriceLevel = ISNULL(D.tintPriceLevel,0)
	FROM 
		DivisionMaster D                  
	JOIN 
		CompanyInfo C 
	ON 
		C.numCompanyId=D.numCompanyID                  
	WHERE 
		numDivisionID =@numDivisionID       

	-- GET ITEM DETAILS
	SELECT
		@vcItemName = vcItemName,
		@bitDropship = bitAllowDropShip,
		@ItemDesc = Item.txtItemDesc,
		@bitMatrix = ISNULL(Item.bitMatrix,0),
		@chrItemType = charItemType,
		@numItemGroup = numItemGroup,
		@bitAssembly = ISNULL(bitAssembly,0),
		@numBaseUnit = ISNULL(numBaseUnit,0),
		@numSaleUnit = (CASE WHEN ISNULL(@numUOMID,0) > 0 THEN @numUOMID ELSE ISNULL(numSaleUnit,0) END),
		@numPurchaseUnit = (CASE WHEN ISNULL(@numUOMID,0) > 0 THEN @numUOMID ELSE ISNULL(numPurchaseUnit,0) END),
		@numItemClassification = numItemClassification,
		@bitAssemblyOrKit = (CASE WHEN ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0)=1 THEN 1 ELSE 0 END),
		@monListPrice = (CASE WHEN Item.charItemType='P' THEN ISNULL(WareHouseItems.monWListPrice,0) ELSE ISNULL(Item.monListPrice,0) END),
		@monVendorCost = ISNULL((SELECT monCost FROM Vendor WHERE numVendorID=Item.numVendorID AND Vendor.numItemCode=Item.numItemCode),0),
		@numVendorID = ISNULL(Item.numVendorID,0),
		@numWarehouseItemID = (CASE WHEN ISNULL(@numWarehouseItemID,0) > 0 THEN @numWarehouseItemID ELSE WareHouseItems.numWareHouseItemID END),
		@bitCalAmtBasedonDepItems = ISNULL(bitCalAmtBasedonDepItems,0)
	FROM
		Item
	OUTER APPLY
	(
		SELECT TOP 1 numWareHouseItemID,monWListPrice FROM WareHouseItems WHERE numItemID=@numItemCode AND (numWareHouseItemID = @numWarehouseItemID OR ISNULL(@numWarehouseItemID,0) = 0)
	) AS WareHouseItems
	WHERE
		Item.numItemCode = @numItemCode

	IF ISNULL(@numDivisionID,0) > 0 AND EXISTS (SELECT * FROM Vendor WHERE numItemCode=@numItemCode AND numVendorID=@numDivisionID)
	BEGIN
		SELECT @monVendorCost=ISNULL(monCost,0) FROM Vendor WHERE numVendorID=@numDivisionID AND numItemCode=@numItemCode
	END


	IF @chrItemType = 'P' AND ISNULL(@numWarehouseItemID,0) = 0
	BEGIN
		RAISERROR('WAREHOUSE_DOES_NOT_EXISTS',16,1)
	END

	SET @fltUOMConversionFactor = dbo.fn_UOMConversion((CASE WHEN  @tintOppType = 1 THEN @numSaleUnit ELSE @numPurchaseUnit END),@numItemCode,@numDomainId,@numBaseUnit)

	IF @tintOppType = 1
	BEGIN
		/*Use Calculated Price When item is Assembly/Kit,Calculate price based on sum of dependent items.*/
		IF @bitCalAmtBasedonDepItems = 1 
		BEGIN
			PRINT 'IN DEPENDED ITEM'

			CREATE TABLE #TEMPSelectedKitChilds
			(
				ChildKitItemID NUMERIC(18,0),
				ChildKitWarehouseItemID NUMERIC(18,0),
				ChildKitChildItemID NUMERIC(18,0),
				ChildKitChildWarehouseItemID NUMERIC(18,0)
			)

			IF ISNULL(@numOppItemID,0) > 0
			BEGIN
				INSERT INTO 
					#TEMPSelectedKitChilds
				SELECT 
					OKI.numChildItemID,
					OKI.numWareHouseItemId,
					OKCI.numItemID,
					OKCI.numWareHouseItemId
				FROM
					OpportunityKitItems OKI
				INNER JOIN
					OpportunityKitChildItems OKCI
				ON
					OKI.numOppChildItemID = OKCI.numOppChildItemID
				WHERE
					OKI.numOppId = @numOppID
					AND OKI.numOppItemID = @numOppItemID
			END
			ELSE IF ISNULL(@vcSelectedKitChildItems,'') <> ''
			BEGIN
				INSERT INTO 
					#TEMPSelectedKitChilds
				SELECT 
					SUBSTRING
					(
						SUBSTRING(OutParam,0,CHARINDEX('-',OutParam)),
						0,
						CHARINDEX('#', SUBSTRING(OutParam,0,CHARINDEX('-',OutParam)))
					) AS numChildKitID,
					SUBSTRING
					(
						SUBSTRING(OutParam,0,CHARINDEX('-',OutParam)),
						CHARINDEX('#', SUBSTRING(OutParam,0,CHARINDEX('-',OutParam))) + 1,
						LEN(SUBSTRING(OutParam,0,CHARINDEX('-',OutParam)))
					) AS numChildKitWarehouseItemID,
					SUBSTRING
					(
						SUBSTRING(OutParam,CHARINDEX('-',OutParam) + 1, LEN(OutParam)),
						0,
						CHARINDEX('#', SUBSTRING(OutParam,CHARINDEX('-',OutParam) + 1, LEN(OutParam)))
					) AS numChildKitID,
					SUBSTRING
					(
						SUBSTRING(OutParam,CHARINDEX('-',OutParam) + 1, LEN(OutParam)),
						CHARINDEX('#', SUBSTRING(OutParam,CHARINDEX('-',OutParam) + 1, LEN(OutParam))) + 1,
						LEN(SUBSTRING(OutParam,CHARINDEX('-',OutParam) + 1, LEN(OutParam)))
					) AS numChildKitWarehouseItemID
		
				FROM 
					dbo.SplitString(@vcSelectedKitChildItems,',')
			END

			;WITH CTE (numItemCode, vcItemName, bitKitParent, numWarehouseItemID,numQtyItemsReq,numUOMId) AS
			(
				SELECT
					ID.numChildItemID,
					I.vcItemName,
					ISNULL(I.bitKitParent,0),
					ISNULL(ID.numWarehouseItemId,0),
					CAST((ISNULL(ID.numQtyItemsReq,0) * ISNULL(dbo.fn_UOMConversion(ISNULL(ID.numUOMID, 0), I.numItemCode, @numDomainID, ISNULL(I.numBaseUnit, 0)),1)) AS FLOAT),
					ISNULL(numUOMId,0)
				FROM 
					[ItemDetails] ID
				INNER JOIN 
					[Item] I 
				ON 
					ID.[numChildItemID] = I.[numItemCode]
				WHERE   
					[numItemKitID] = @numItemCode
				UNION ALL
				SELECT
					ID.numChildItemID,
					I.vcItemName,
					ISNULL(I.bitKitParent,0),
					ISNULL(ID.numWarehouseItemId,0),
					CAST((ISNULL(Temp1.numQtyItemsReq,0) * ISNULL(ID.numQtyItemsReq,0) * ISNULL(dbo.fn_UOMConversion(ISNULL(ID.numUOMID, 0), I.numItemCode, @numDomainID, ISNULL(I.numBaseUnit, 0)),1)) AS FLOAT),
					ISNULL(ID.numUOMId,0)
				FROM 
					CTE As Temp1
				INNER JOIN
					[ItemDetails] ID
				ON
					ID.numItemKitID = Temp1.numItemCode
				INNER JOIN 
					[Item] I 
				ON 
					ID.[numChildItemID] = I.[numItemCode]
				INNER JOIN
					#TEMPSelectedKitChilds
				ON
					ISNULL(Temp1.bitKitParent,0) = 1
					AND ID.numChildItemID = #TEMPSelectedKitChilds.ChildKitChildItemID
					AND ID.numWareHouseItemId = #TEMPSelectedKitChilds.ChildKitChildWarehouseItemID
			)

			SELECT  
					@monListPrice = ISNULL(  SUM(ISNULL(CASE WHEN I.[charItemType]='P' 
					THEN WI.[monWListPrice] 
					ELSE monListPrice END,0) * ID.[numQtyItemsReq] )  ,0)
			FROM    CTE ID
					INNER JOIN [Item] I ON ID.numItemCode = I.[numItemCode]
					left join  WareHouseItems WI
					on WI.numItemID=I.numItemCode and WI.[numWareHouseItemID]=ID.numWarehouseItemID
			WHERE
				ISNULL(ID.bitKitParent,0) = 0

			DROP TABLE #TEMPSelectedKitChilds
		END

		IF @tintDefaultSalesPricing = 1 AND (SELECT COUNT(*) FROM PricingTable WHERE numItemCode=@numItemCode AND ISNULL(numPriceRuleID,0) = 0) > 0 -- PRICE LEVEL
		BEGIN
			SELECT
				@tintDisountType = tintDiscountType,
				@decDiscount = ISNULL(decDiscount,0),
				@tintRuleType = tintRuleType,
				@monPriceLevelPrice = (CASE tintRuleType WHEN 1 THEN @monListPrice WHEN 2 THEN @monVendorCost ELSE ISNULL(decDiscount,0) END),
				@monPriceFinalPrice = (CASE tintRuleType
										WHEN 1 -- Deduct From List Price
										THEN
											CASE tintDiscountType 
												WHEN 1 -- PERCENT
												THEN (CASE WHEN @monListPrice > 0 THEN @monListPrice - (@monListPrice * (ISNULL(decDiscount,0)/100)) ELSE 0 END)
												WHEN 2 -- FLAT AMOUNT
												THEN (CASE WHEN @monListPrice > 0 THEN (CASE WHEN (@monListPrice * @numQty * @fltUOMConversionFactor) - ISNULL(decDiscount,0) > 0 THEN ((@monListPrice * @numQty * @fltUOMConversionFactor) - ISNULL(decDiscount,0)) / (@numQty * @fltUOMConversionFactor) ELSE 0 END) ELSE 0 END)
												WHEN 3 -- NAMED PRICE
												THEN ISNULL(decDiscount,0)
											END
										WHEN 2 -- Add to primary vendor cost
										THEN
											CASE tintDiscountType 
												WHEN 1  -- PERCENT
												THEN @monVendorCost + (@monVendorCost * (ISNULL(decDiscount,0)/100))  * @fltUOMConversionFactor
												WHEN 2 -- FLAT AMOUNT
												THEN (CASE WHEN (@monVendorCost * @numQty * @fltUOMConversionFactor) + ISNULL(decDiscount,0) > 0 THEN ((@monVendorCost * @numQty * @fltUOMConversionFactor) + ISNULL(decDiscount,0)) / (@numQty * @fltUOMConversionFactor)  ELSE 0 END)
												WHEN 3 -- NAMED PRICE
												THEN ISNULL(decDiscount,0)
											END
										WHEN 3 -- Named price
										THEN
											ISNULL(decDiscount,0)
										END
									) 
			FROM
			(
				SELECT 
					ROW_NUMBER() OVER(PARTITION BY PricingTable.numItemCode ORDER BY numPricingID) Id,
					*
				FROM
					PricingTable
				WHERE 
					PricingTable.numItemCode = @numItemCode
					
			) TEMP
			WHERE
				numItemCode=@numItemCode AND ((tintRuleType = 3 AND Id=@tintPriceLevel) OR @numQty * @fltUOMConversionFactor >= intFromQty AND @numQty * @fltUOMConversionFactor <= intToQty)
		END
		ELSE IF @tintDefaultSalesPricing = 2 AND (SELECT COUNT(*) FROM PriceBookRules WHERE numDomainID=@numDomainID AND tintRuleFor=@tintOppType) > 0 -- PRICE RULE
		BEGIN
			DECLARE @numPriceRuleID NUMERIC(18,0)
			DECLARE @tintPriceRuleType AS TINYINT
			DECLARE @tintPricingMethod TINYINT
			DECLARE @tintPriceBookDiscountType TINYINT
			DECLARE @decPriceBookDiscount FLOAT
			DECLARE @intQntyItems INT
			DECLARE @decMaxDedPerAmt FLOAT

			SELECT 
				TOP 1
				@numPriceRuleID = numPricRuleID,
				@tintPricingMethod = tintPricingMethod,
				@tintPriceRuleType = p.tintRuleType,
				@tintPriceBookDiscountType = P.[tintDiscountType],
				@decPriceBookDiscount = P.[decDiscount],
				@intQntyItems = P.[intQntyItems],
				@decMaxDedPerAmt = P.[decMaxDedPerAmt]
			FROM   
				PriceBookRules P 
			LEFT JOIN 
				PriceBookRuleDTL PDTL 
			ON 
				P.numPricRuleID = PDTL.numRuleID
			LEFT JOIN 
				PriceBookRuleItems PBI 
			ON 
				P.numPricRuleID = PBI.numRuleID
			LEFT JOIN 
				[PriceBookPriorities] PP 
			ON 
				PP.[Step2Value] = P.[tintStep2] 
				AND PP.[Step3Value] = P.[tintStep3]
			WHERE  
				P.numDomainID=@numDomainID 
				AND tintRuleFor=@tintOppType
				AND (
						((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 1
						OR ((tintStep2 = 2 AND PBI.numValue = @numItemClassification) AND (tintStep3= 1 AND PDTL.numValue = @numDivisionID)) -- Priority 2
						OR ((tintStep2 = 3) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 3

						OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 4
						OR ((tintStep2 = 2 AND PBI.numValue = @numItemClassification) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 5
						OR ((tintStep2 = 3) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 6

						OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 3)) -- Priority 7
						OR ((tintStep2 = 2 AND PBI.numValue = @numItemClassification) AND (tintStep3 = 3)) -- Priority 8
						OR ((tintStep2 = 3) and (tintStep3 = 3)) -- Priority 9
					)
			ORDER BY 
				PP.Priority ASC


			IF ISNULL(@numPriceRuleID,0) > 0
			BEGIN
				IF @tintPricingMethod = 1 -- BASED ON PRICE TABLE
				BEGIN
					SELECT
						@tintDisountType = tintDiscountType,
						@decDiscount = ISNULL(decDiscount,0),
						@tintRuleType = tintRuleType,
						@monPriceRulePrice = (CASE tintRuleType WHEN 1 THEN @monListPrice WHEN 2 THEN @monVendorCost END) ,
						@monPriceRuleFinalPrice = (CASE tintRuleType
												WHEN 1 -- Deduct From List Price
												THEN
													CASE tintDiscountType 
														WHEN 1 -- PERCENT
														THEN (CASE WHEN @monListPrice > 0 THEN @monListPrice - (@monListPrice * (ISNULL(decDiscount,0)/100)) ELSE 0 END)
														WHEN 2 -- FLAT AMOUNT
														THEN (CASE WHEN @monListPrice > 0 THEN (CASE WHEN (@monListPrice * @numQty * @fltUOMConversionFactor) - ISNULL(decDiscount,0) > 0 THEN ((@monListPrice * @numQty * @fltUOMConversionFactor) - ISNULL(decDiscount,0)) / (@numQty * @fltUOMConversionFactor) ELSE 0 END) ELSE 0 END)
														WHEN 3 -- NAMED PRICE
														THEN ISNULL(decDiscount,0)
													END
												WHEN 2 -- Add to primary vendor cost
												THEN
													CASE tintDiscountType 
														WHEN 1  -- PERCENT
														THEN @monVendorCost + (@monVendorCost * (ISNULL(decDiscount,0)/100))
														WHEN 2 -- FLAT AMOUNT
														THEN (CASE WHEN (@monVendorCost * @numQty * @fltUOMConversionFactor) + ISNULL(decDiscount,0) > 0 THEN ((@monVendorCost * @numQty * @fltUOMConversionFactor) + ISNULL(decDiscount,0)) / (@numQty * @fltUOMConversionFactor)  ELSE 0 END)
														WHEN 3 -- NAMED PRICE
														THEN ISNULL(decDiscount,0)
													END
												WHEN 3 -- Named price
												THEN
													CASE 
													WHEN ISNULL(@tintPriceLevel,0) > 0
													THEN
														(SELECT 
															 ISNULL(decDiscount,0)
														FROM
														(
															SELECT 
																ROW_NUMBER() OVER(PARTITION BY PricingTable.numItemCode ORDER BY numPricingID) Id,
																decDiscount
															FROM 
																PricingTable 
															WHERE 
																PricingTable.numItemCode = @numItemCode 
																AND tintRuleType = 3 
														) TEMP
														WHERE
															Id = @tintPriceLevel)
													ELSE
														ISNULL(decDiscount,0)
													END
												END
											) 
					FROM
						PricingTable
					WHERE
						numPriceRuleID=@numPriceRuleID AND @numQty * @fltUOMConversionFactor >= intFromQty AND @numQty * @fltUOMConversionFactor <= intToQty
				END
				ELSE IF @tintPricingMethod = 2 -- BASED ON RULE
				BEGIN
					SET @tintRuleType = @tintPriceRuleType
					SET @monPriceRulePrice = (CASE @tintPriceRuleType WHEN 1 THEN @monListPrice WHEN 2 THEN @monVendorCost END) 

					IF (@tintPriceBookDiscountType = 1 ) -- Percentage 
					BEGIN
						SET @tintDisountType = @tintPriceBookDiscountType
						SET @decDiscount = @decDiscount * (@numQty/@intQntyItems);
						SET @decDiscount = @decDiscount * (CASE WHEN @tintPriceRuleType = 2 THEN @monVendorCost ELSE @monListPrice END) /100;
						SET @decMaxDedPerAmt = @decMaxDedPerAmt * (CASE WHEN @tintPriceRuleType = 2 THEN @monVendorCost ELSE @monListPrice END) /100;
					
						IF (@decDiscount > @decMaxDedPerAmt)
							SET @decDiscount = @decMaxDedPerAmt;
				
						IF ( @tintPriceRuleType = 1 )
								SET @monPriceRuleFinalPrice = (@monListPrice - @decDiscount);
						IF ( @tintPriceRuleType = 2 )
								SET @monPriceRuleFinalPrice = (@monVendorCost + @decDiscount);
					END
					IF ( @tintPriceBookDiscountType = 2 ) -- Flat discount 
					BEGIN
						SET @decDiscount = @decDiscount * ((@numQty * @fltUOMConversionFactor)/@intQntyItems);

						IF (@decDiscount > @decMaxDedPerAmt)
							SET @decDiscount = @decMaxDedPerAmt;
					
						IF ( @tintPriceRuleType = 1 )
							SET @monPriceRuleFinalPrice =  ((@monListPrice * @numQty * @fltUOMConversionFactor) - @decDiscount)/(@numQty * @fltUOMConversionFactor);
						IF ( @tintPriceRuleType = 2 )
							SET @monPriceRuleFinalPrice = ((@monVendorCost * @numQty * @fltUOMConversionFactor) + @decDiscount)/(@numQty * @fltUOMConversionFactor);
					END
				END
			END
		
		END
	
		-- GET Last Price
		DECLARE @LastDiscountType TINYINT
		DECLARE @LastDiscount FLOAT

		SELECT 
			TOP 1 
			@monLastPrice = monPrice
			,@dtLastOrderDate = dbo.FormatedDateFromDate(OpportunityMaster.bintCreatedDate,@numDomainID)
			,@LastDiscountType = ISNULL(OpportunityItems.bitDiscountType,1)
			,@LastDiscount = ISNULL(OpportunityItems.fltDiscount,0)
		FROM 
			OpportunityItems 
		JOIN 
			OpportunityMaster 
		ON 
			OpportunityItems.numOppId = OpportunityMaster.numOppId 
		WHERE 
			OpportunityMaster.numDomainId= @numDomainID
			AND tintOppType = @tintOppType
			AND tintOppStatus = 1
			AND numItemCode=@numItemCode
		ORDER BY
			OpportunityMaster.numOppId DESC

		IF @tintDefaultSalesPricing = 1 AND ISNULL(@monPriceFinalPrice,0) > 0
		BEGIN
			-- WE ARE NOT DISPLAYING DISCOUNT SEPERATELY IN PRICE LEVEL
			SET @tintPricingBasedOn = 1
			SET @monPrice = @monPriceFinalPrice
			SET @tintDisountType = 1
			SET @decDiscount = 0
		END
		ELSE IF @tintDefaultSalesPricing = 2 AND ISNULL(@monPriceRuleFinalPrice,0) > 0
		BEGIN
			SET @tintPricingBasedOn = 2
		
			IF @tintPriceBookDiscountType = 1 -- Display Disocunt Price As Unit Price
			BEGIN
				SET @monPrice = @monPriceRuleFinalPrice
				SET @tintDisountType = 1
				SET @decDiscount = 0
			END
			ELSE -- Display Unit Price And Disocunt Price Seperately
			BEGIN
				SET @monPrice = @monPriceRulePrice
			END
		
		END
		ELSE IF @tintDefaultSalesPricing = 3 AND ISNULL(@monLastPrice,0) > 0
		BEGIN
			SET @tintPricingBasedOn = 3
			SET @monPrice = @monLastPrice
			SET @tintDisountType = @LastDiscountType
			SET @decDiscount = @LastDiscount
		END
		ELSE
		BEGIN
			SET @tintPricingBasedOn = 0
			SET @tintDisountType = 1
			SET @decDiscount = 0
			SET @tintRuleType = 0

			IF @tintOppType = 1
				SET @monPrice = @monListPrice
			ELSE
				SET @monPrice = @monVendorCost
		END
	END
	ELSE IF @tintOppType = 2
	BEGIN
		SELECT 
			TOP 1
			@numPriceRuleID = numPricRuleID,
			@tintPricingMethod = tintPricingMethod,
			@tintPriceRuleType = p.tintRuleType,
			@tintPriceBookDiscountType = P.[tintDiscountType],
			@decPriceBookDiscount = P.[decDiscount],
			@intQntyItems = P.[intQntyItems],
			@decMaxDedPerAmt = P.[decMaxDedPerAmt]
		FROM   
			PriceBookRules P 
		LEFT JOIN 
			PriceBookRuleDTL PDTL 
		ON 
			P.numPricRuleID = PDTL.numRuleID
		LEFT JOIN 
			PriceBookRuleItems PBI 
		ON 
			P.numPricRuleID = PBI.numRuleID
		LEFT JOIN 
			[PriceBookPriorities] PP 
		ON 
			PP.[Step2Value] = P.[tintStep2] 
			AND PP.[Step3Value] = P.[tintStep3]
		WHERE  
			P.numDomainID=@numDomainID 
			AND tintRuleFor=@tintOppType
			AND (
					((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 1
					OR ((tintStep2 = 2 AND PBI.numValue = @numItemClassification) AND (tintStep3= 1 AND PDTL.numValue = @numDivisionID)) -- Priority 2
					OR ((tintStep2 = 3) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 3

					OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 4
					OR ((tintStep2 = 2 AND PBI.numValue = @numItemClassification) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 5
					OR ((tintStep2 = 3) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 6

					OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 3)) -- Priority 7
					OR ((tintStep2 = 2 AND PBI.numValue = @numItemClassification) AND (tintStep3 = 3)) -- Priority 8
					OR ((tintStep2 = 3) and (tintStep3 = 3)) -- Priority 9
				)
		ORDER BY 
			PP.Priority ASC


		IF ISNULL(@numPriceRuleID,0) > 0
		BEGIN
			IF @tintPricingMethod = 1 -- BASED ON PRICE TABLE
			BEGIN
				SELECT
					@tintDisountType = tintDiscountType,
					@decDiscount = ISNULL(decDiscount,0),
					@tintRuleType = tintRuleType,
					@monPriceRulePrice = (CASE tintRuleType WHEN 1 THEN @monListPrice WHEN 2 THEN @monVendorCost END) ,
					@monPriceRuleFinalPrice = (CASE tintRuleType
											WHEN 1 -- Deduct From List Price
											THEN
												CASE tintDiscountType 
													WHEN 1 -- PERCENT
													THEN (CASE WHEN @monListPrice > 0 THEN @monListPrice - (@monListPrice * (ISNULL(decDiscount,0)/100)) ELSE 0 END)
													WHEN 2 -- FLAT AMOUNT
													THEN (CASE WHEN @monListPrice > 0 THEN (CASE WHEN (@monListPrice * @numQty * @fltUOMConversionFactor) - ISNULL(decDiscount,0) > 0 THEN ((@monListPrice * @numQty * @fltUOMConversionFactor) - ISNULL(decDiscount,0)) / (@numQty * @fltUOMConversionFactor) ELSE 0 END) ELSE 0 END)
													WHEN 3 -- NAMED PRICE
													THEN ISNULL(decDiscount,0)
												END
											WHEN 2 -- Add to primary vendor cost
											THEN
												CASE tintDiscountType 
													WHEN 1  -- PERCENT
													THEN @monVendorCost + (@monVendorCost * (ISNULL(decDiscount,0)/100))
													WHEN 2 -- FLAT AMOUNT
													THEN (CASE WHEN (@monVendorCost * @numQty * @fltUOMConversionFactor) + ISNULL(decDiscount,0) > 0 THEN ((@monVendorCost * @numQty * @fltUOMConversionFactor) + ISNULL(decDiscount,0)) / (@numQty * @fltUOMConversionFactor)  ELSE 0 END)
													WHEN 3 -- NAMED PRICE
													THEN ISNULL(decDiscount,0)
												END
											WHEN 3 -- Named price
											THEN
												CASE 
												WHEN ISNULL(@tintPriceLevel,0) > 0
												THEN
													(SELECT 
															ISNULL(decDiscount,0)
													FROM
													(
														SELECT 
															ROW_NUMBER() OVER(PARTITION BY PricingTable.numItemCode ORDER BY numPricingID) Id,
															decDiscount
														FROM 
															PricingTable 
														WHERE 
															PricingTable.numItemCode = @numItemCode 
															AND tintRuleType = 3 
													) TEMP
													WHERE
														Id = @tintPriceLevel)
												ELSE
													ISNULL(decDiscount,0)
												END
											END
										) 
				FROM
					PricingTable
				WHERE
					numPriceRuleID=@numPriceRuleID AND @numQty * @fltUOMConversionFactor >= intFromQty AND @numQty * @fltUOMConversionFactor <= intToQty
			END
			ELSE IF @tintPricingMethod = 2 -- BASED ON RULE
			BEGIN
				SET @tintRuleType = @tintPriceRuleType
				SET @monPriceRulePrice = (CASE @tintPriceRuleType WHEN 1 THEN @monListPrice WHEN 2 THEN @monVendorCost END) 

				IF (@tintPriceBookDiscountType = 1 ) -- Percentage 
				BEGIN
					SET @tintDisountType = @tintPriceBookDiscountType
					SET @decDiscount = @decDiscount * (@numQty/@intQntyItems);
					SET @decDiscount = @decDiscount * (CASE WHEN @tintPriceRuleType = 2 THEN @monVendorCost ELSE @monListPrice END) /100;
					SET @decMaxDedPerAmt = @decMaxDedPerAmt * (CASE WHEN @tintPriceRuleType = 2 THEN @monVendorCost ELSE @monListPrice END) /100;
					
					IF (@decDiscount > @decMaxDedPerAmt)
						SET @decDiscount = @decMaxDedPerAmt;
				
					IF ( @tintPriceRuleType = 1 )
							SET @monPriceRuleFinalPrice = (@monListPrice - @decDiscount);
					IF ( @tintPriceRuleType = 2 )
							SET @monPriceRuleFinalPrice = (@monVendorCost + @decDiscount);
				END
				IF ( @tintPriceBookDiscountType = 2 ) -- Flat discount 
				BEGIN
					SET @decDiscount = @decDiscount * ((@numQty * @fltUOMConversionFactor)/@intQntyItems);

					IF (@decDiscount > @decMaxDedPerAmt)
						SET @decDiscount = @decMaxDedPerAmt;
					
					IF ( @tintPriceRuleType = 1 )
						SET @monPriceRuleFinalPrice =  ((@monListPrice * @numQty * @fltUOMConversionFactor) - @decDiscount)/(@numQty * @fltUOMConversionFactor);
					IF ( @tintPriceRuleType = 2 )
						SET @monPriceRuleFinalPrice = ((@monVendorCost * @numQty * @fltUOMConversionFactor) + @decDiscount)/(@numQty * @fltUOMConversionFactor);
				END
			END
		END

		If @monPriceRuleFinalPrice > 0
		BEGIN
			SET @monPrice = @monPriceRuleFinalPrice
		END
		ELSE
		BEGIN
			SET @monPrice = @monVendorCost
		END


		SET @monPriceLevelPrice = 0
		SET @monPriceFinalPrice = 0
		SET @monPriceRulePrice = 0
		SET @monPriceRuleFinalPrice = 0
		SET @monLastPrice = 0
		SET @dtLastOrderDate = NULL
		SET @tintDisountType = 1
		SET @decDiscount =0 
	END


	

	SELECT
		@vcItemName AS vcItemName,
		@bitDropship AS bitDropship,
		@chrItemType AS ItemType,
		@bitMatrix AS bitMatrix,
		@numItemClassification AS ItemClassification,
		@numItemGroup AS ItemGroup,
		@ItemDesc AS ItemDescription,
		@bitAssembly AS bitAssembly,
		case when ISNULL(@bitAssembly,0)=1 then dbo.fn_GetAssemblyPossibleWOQty(@numItemCode) else 0 end AS MaxWorkOrderQty,
		@tintPricingBasedOn AS tintPriceBaseOn,
		@monPrice AS monPrice,
		@fltUOMConversionFactor AS UOMConversionFactor,
		(CASE WHEN @tintOppType=1 THEN @monListPrice ELSE @monVendorCost END) AS monListPrice,
		@monVendorCost AS VendorCost,
		@numVendorID AS numVendorID,
		@tintRuleType AS tintRuleType,
		@monPriceLevelPrice AS monPriceLevel,
		@monPriceFinalPrice AS monPriceLevelFinalPrice,
		@monPriceRulePrice AS monPriceRule,
		@monPriceRuleFinalPrice AS monPriceRuleFinalPrice,
		@monLastPrice AS monLastPrice,
		@dtLastOrderDate AS LastOrderDate,
		@tintDisountType AS DiscountType,
		@decDiscount AS Discount,
		@numBaseUnit AS numBaseUnit,
		@numSaleUnit AS numSaleUnit,
		@numPurchaseUnit AS numPurchaseUnit,
		dbo.fn_GetUOMName(@numBaseUnit) AS vcBaseUOMName,
		dbo.fn_GetUOMName(@numSaleUnit) AS vcSaleUOMName,
		@numPurchaseUnit AS numPurchaseUnit,
		@numWarehouseItemID AS numWarehouseItemID,
		dbo.fn_GetItemTransitCount(@numItemcode,@numDomainID) AS OrdersInTransist,
		(SELECT COUNT(*) FROM SimilarItems SI INNER JOIN Item I ON I.numItemCode = SI.numItemCode WHERE SI.numDomainId = @numDomainID AND SI.numParentItemCode = @numItemCode) AS RelatedItemsCount,
		@tintPriceLevel AS tintPriceLevel,
		@vcMinOrderQty AS vcMinOrderQty,
		@vcVendorNotes AS vcVendorNotes


	-- CHECK IF PROMOTION CAN BE TRIGGERED FROM THIS ITEM
	DECLARE @numPromotionID AS NUMERIC(18,0)

	SELECT TOP 1
		@numPromotionID = numProId 
	FROM 
		PromotionOffer PO
	WHERE 
		numDomainId=@numDomainID 
		AND ISNULL(bitEnabled,0)=1
		AND ISNULL(bitApplyToInternalOrders,0)=1 
		AND 1 = (CASE WHEN bitRequireCouponCode = 1 THEN CASE WHEN ISNULL(intCouponCodeUsed,0) < ISNULL(tintUsageLimit,0) OR ISNULL(tintUsageLimit,0) = 0  THEN 1 ELSE 0 END ELSE 1 END)
		AND 1 = (CASE WHEN bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN CAST(GETDATE() AS DATE)>=dtValidFrom AND CAST(GETDATE() AS DATE)<=dtValidTo THEN 1 ELSE 0 END) END)
		AND 1 = (CASE 
					WHEN tintOfferBasedOn = 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 AND numValue=@numItemCode) > 0 THEN 1 ELSE 0 END)
					WHEN tintOfferBasedOn = 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=2 AND numValue=@numItemClassification) > 0 THEN 1 ELSE 0 END)
					ELSE 0
				END)
	ORDER BY
		CASE 
			WHEN ISNULL(PO.bitRequireCouponCode,0)=1 AND PO.txtCouponCode=@vcCoupon THEN 1
			WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 1 THEN 2
			WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 1 THEN 3
			WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 2 THEN 4
			WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 2 THEN 5
			WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 3 THEN 6
			WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 3 THEN 7
		END


	IF ISNULL(@numPromotionID,0) > 0
	BEGIN
		-- Get Top 1 Promotion Based on priority

		SELECT 
			numProId
			,vcProName
			,bitNeverExpires
			,bitRequireCouponCode
			,txtCouponCode
			,tintUsageLimit
			,intCouponCodeUsed
			,bitFreeShiping
			,monFreeShippingOrderAmount
			,numFreeShippingCountry
			,bitFixShipping1
			,monFixShipping1OrderAmount
			,monFixShipping1Charge
			,bitFixShipping2
			,monFixShipping2OrderAmount
			,monFixShipping2Charge
			,tintOfferTriggerValueType
			,fltOfferTriggerValue
			,tintOfferBasedOn
			,fltDiscountValue
			,tintDiscountType
			,tintDiscoutBaseOn
			,dbo.FormatedDateFromDate(dtValidTo,@numDomainID) AS dtExpire
			,(CASE tintOfferBasedOn
				WHEN 1 THEN (SELECT Stuff((SELECT CONCAT(',',numValue) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 FOR XML PATH('')), 1, 1, ''))
				WHEN 2 THEN (SELECT Stuff((SELECT CONCAT(',',numValue) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=2 FOR XML PATH('')), 1, 1, ''))
			END) As vcPromotionItems
			,(CASE tintDiscoutBaseOn
				WHEN 1 THEN (SELECT Stuff((SELECT CONCAT(',',numValue) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=6 AND tintType=1 FOR XML PATH('')), 1, 1, ''))
				WHEN 2 THEN (SELECT Stuff((SELECT CONCAT(',',numValue) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=6 AND tintType=2 FOR XML PATH('')), 1, 1, ''))
				WHEN 3 THEN (SELECT Stuff((SELECT CONCAT(',',SimilarItems.numItemCode) FROM SimilarItems WHERE 1 = (CASE 
																														WHEN tintOfferBasedOn=1 THEN CASE WHEN numParentItemCode IN (SELECT numValue FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1) THEN 1 ELSE 0 END
																														WHEN tintOfferBasedOn=2 THEN CASE WHEN numParentItemCode IN (SELECT numItemCode FROM Item WHERE numDomainID=@numDomainID AND numItemClassification IN (SELECT numValue FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=2)) THEN 1 ELSE 0 END
																														ELSE 0
																													END) FOR XML PATH('')), 1, 1, ''))
			END) As vcItems
			,CONCAT('Buy '
					,CASE WHEN tintOfferTriggerValueType=1 THEN CAST(fltOfferTriggerValue AS VARCHAR) ELSE CONCAT('$',fltOfferTriggerValue) END
					,CASE tintOfferBasedOn
							WHEN 1 THEN CONCAT(' of "',(SELECT Stuff((SELECT CONCAT(', ',Item.vcItemName) FROM PromotionOfferItems INNER JOIN Item ON PromotionOfferItems.numValue = Item.numItemCode WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 FOR XML PATH('')), 1, 2, '')), '"')
							WHEN 2 THEN CONCAT(' of items belonging to classification(s) "',(SELECT Stuff((SELECT CONCAT(', ',ListDetails.vcData) FROM PromotionOfferItems INNER JOIN ListDetails ON PromotionOfferItems.numValue = ListDetails.numListItemID WHERE numListID=36 AND numProId=PO.numProId AND tintRecordType=5 AND tintType=2 FOR XML PATH('')), 1, 2, '')), '"')
						END
					,' & get '
					, CASE tintDiscountType WHEN 1 THEN CONCAT(fltDiscountValue,'% off on') WHEN 2 THEN CONCAT('$',fltDiscountValue, ' off on') WHEN 3 THEN CONCAT(fltDiscountValue,' of') END
					, CASE 
						WHEN tintDiscoutBaseOn = 1 THEN 
							CONCAT
							(
								CONCAT(' "',(SELECT Stuff((SELECT CONCAT(',',Item.vcItemName) FROM PromotionOfferItems INNER JOIN Item ON PromotionOfferItems.numValue = Item.numItemCode WHERE numProId=PO.numProId AND tintRecordType=6 AND tintType=1 FOR XML PATH('')), 1, 1, '')), '"')
								,(CASE WHEN PO.tintDiscountType = 3 THEN ' free' ELSE '' END)
								,'.'
							)
						WHEN tintDiscoutBaseOn = 2 THEN 
							CONCAT
							(
								CONCAT(' items belonging to classification(s) "',(SELECT Stuff((SELECT CONCAT(',',ListDetails.vcData) FROM PromotionOfferItems INNER JOIN ListDetails ON PromotionOfferItems.numValue = ListDetails.numListItemID WHERE numListID=36 AND numProId=PO.numProId AND tintRecordType=6 AND tintType=2 FOR XML PATH('')), 1, 1, '')), '"')
								,(CASE WHEN PO.tintDiscountType = 3 THEN ' free' ELSE '' END)
								,'.'
							)
						WHEN tintDiscoutBaseOn = 3 THEN ' related items.'
						ELSE '' 
					END
				) AS vcPromotionDescription
				,CONCAT
				(
					(CASE WHEN ISNULL(bitFixShipping1,0)=1 THEN CONCAT('Spend $',monFixShipping1OrderAmount,' and get $',monFixShipping1Charge,' shipping. ') ELSE '' END)
					,(CASE WHEN ISNULL(bitFixShipping2,0)=1  THEN CONCAT('Spend $',monFixShipping2OrderAmount,' and get $',monFixShipping2Charge,' shipping. ') ELSE '' END)
					,(CASE WHEN (bitFreeShiping=1 AND numFreeShippingCountry=@numShippingCountry) THEN CONCAT('Spend $',monFreeShippingOrderAmount,' and shipping is FREE! ') ELSE '' END) 
				) AS vcShippingDescription,
				CASE 
					WHEN ISNULL(PO.bitRequireCouponCode,0)=1 AND PO.txtCouponCode=@vcCoupon THEN 1
					WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 1 THEN 2
					WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 1 THEN 3
					WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 2 THEN 4
					WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 2 THEN 5
					WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 3 THEN 6
					WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 3 THEN 7
				END AS tintPriority
		FROM
			PromotionOffer PO
			LEFT JOIN ShippingPromotions SP ON PO.numDomainId = SP.numDomainId
		WHERE
			PO.numDomainId=@numDomainID 
			AND numProId = @numPromotionID
	END
END 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetShippingCharge')
DROP PROCEDURE USP_GetShippingCharge
GO
CREATE PROCEDURE [dbo].[USP_GetShippingCharge]
	@numDomainID NUMERIC(18,0),
	@numShippingCountry AS NUMERIC(18,0),
	@cookieId VARCHAR(MAX)=NULL,
	@numUserCntId numeric=0,
	@monShippingAmount money OUTPUT
AS
BEGIN
	SET @monShippingAmount=-1
	DECLARE @numPromotionID AS NUMERIC(18,0)=0

	DECLARE @decmPrice DECIMAL(18,2)
	SET @decmPrice = (
						SELECT 
							ISNULL(SUM(monTotAmount),0) 
						FROM 
							CartItems 
						WHERE 
							numDomainId=@numDomainID 
							AND vcCookieId=@cookieId 
							AND numUserCntId=@numUserCntId
					)


	SET @numPromotionID= (
							SELECT TOP 1 
								PromotionID 
							FROM 
								CartItems AS C 
							LEFT JOIN 
								PromotionOffer AS P 
							ON 
								C.PromotionID=P.numProId
								LEFT JOIN ShippingPromotions SP ON C.numDomainId = SP.numDomainId
							WHERE
								C.numDomainId=@numDomainID 
								AND C.vcCookieId=@cookieId 
								AND C.numUserCntId=@numUserCntId
								AND (ISNULL(bitFixShipping1,0)=1 OR ISNULL(bitFixShipping2,0)=1 OR ISNULL(bitFreeShiping,0)=1) 
							ORDER BY 
								numCartId 
							)

	IF(@numPromotionID>0)
	BEGIN
		DECLARE @bitFreeShiping BIT
		DECLARE @monFreeShippingOrderAmount money
		DECLARE @numFreeShippingCountry money
		DECLARE @bitFixShipping1 BIT
		DECLARE @monFixShipping1OrderAmount money
		DECLARE @monFixShipping1Charge money
		DECLARE @bitFixShipping2 BIT
		DECLARE @monFixShipping2OrderAmount money
		DECLARE @monFixShipping2Charge money
		SELECT 
			@bitFreeShiping=bitFreeShiping
			,@monFreeShippingOrderAmount=monFreeShippingOrderAmount
			,@numFreeShippingCountry=numFreeShippingCountry
			,@bitFixShipping1=bitFixShipping1
			,@monFixShipping1OrderAmount=monFixShipping1OrderAmount
			,@monFixShipping1Charge=monFixShipping1Charge
			,@bitFixShipping2=bitFixShipping2
			,@monFixShipping2OrderAmount=monFixShipping2OrderAmount
			,@monFixShipping2Charge=monFixShipping2Charge
		FROM
			PromotionOffer PO
			LEFT JOIN ShippingPromotions SP ON PO.numDomainId = SP.numDomainId
		WHERE
			PO.numDomainId=@numDomainID 
			AND numProId = @numPromotionID

		IF(ISNULL(@bitFixShipping1,0)=1 AND @decmPrice > @monFixShipping1OrderAmount)
		BEGIN
			SET @monShippingAmount=@monFixShipping1Charge
		END

		IF(ISNULL(@bitFixShipping2,0)=1 AND @decmPrice > @monFixShipping2OrderAmount)
		BEGIN
			SET @monShippingAmount=@monFixShipping2Charge
		END

		IF((ISNULL(@bitFreeShiping,0)=1 AND @numFreeShippingCountry=@numShippingCountry AND @decmPrice>@monFreeShippingOrderAmount))
		BEGIN
			SET @monShippingAmount=0
		END
	END
	
	SELECT ISNULL(@monShippingAmount,0)
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityMaster_GetUsedPromotion')
DROP PROCEDURE USP_OpportunityMaster_GetUsedPromotion
GO
CREATE PROCEDURE [dbo].[USP_OpportunityMaster_GetUsedPromotion]
	@numDomainID NUMERIC(18,0),
	@numOppID NUMERIC(18,0)
AS
BEGIN
	DECLARE @numDivisionID AS NUMERIC(18,0)
	DECLARE @tintShipToAddressType AS TINYINT
	DECLARE @numShipToCountry AS NUMERIC(18,0)

	SELECT  
		@tintShipToAddressType = tintShipToType,
		@numDivisionID = numDivisionId
    FROM    
		OpportunityMaster
    WHERE   
		numOppId = @numOppID
               
    IF (@tintShipToAddressType IS NULL OR @tintShipToAddressType = 1) 
        SELECT  
			@numShipToCountry = AD.numCountry
        FROM    
			dbo.AddressDetails AD
        WHERE   
			AD.numRecordID = @numDivisionID
            AND AD.tintAddressOf = 2
            AND AD.tintAddressType = 2
            AND AD.bitIsPrimary = 1
    ELSE IF @tintShipToAddressType = 0 
        SELECT 
			@numShipToCountry = AD1.numCountry
        FROM    
			companyinfo Com1
        JOIN 
			divisionmaster div1 ON com1.numCompanyID = div1.numCompanyID
        JOIN 
			Domain D1 ON D1.numDivisionID = div1.numDivisionID
        JOIN 
			dbo.AddressDetails AD1 ON AD1.numDomainID = div1.numDomainID
            AND AD1.numRecordID = div1.numDivisionID
            AND tintAddressOf = 2
            AND tintAddressType = 2
            AND bitIsPrimary = 1
        WHERE 
			D1.numDomainID = @numDomainID
    ELSE IF @tintShipToAddressType = 2 
        SELECT 
			@numShipToCountry = numShipCountry
        FROM    
			OpportunityAddress
        WHERE   
			numOppID = @numOppID
    ELSE IF @tintShipToAddressType = 3 
        SELECT 
			@numShipToCountry = numShipCountry
        FROM    
			OpportunityAddress
        WHERE   
			numOppID = @numOppID
				
	SELECT 
		numProId
		,vcProName
		,bitNeverExpires
		,bitRequireCouponCode
		,txtCouponCode
		,tintUsageLimit
		,intCouponCodeUsed
		,bitFreeShiping
		,monFreeShippingOrderAmount
		,numFreeShippingCountry
		,bitFixShipping1
		,monFixShipping1OrderAmount
		,monFixShipping1Charge
		,bitFixShipping2
		,monFixShipping2OrderAmount
		,monFixShipping2Charge
		,tintOfferTriggerValueType
		,fltOfferTriggerValue
		,tintOfferBasedOn
		,fltDiscountValue
		,tintDiscountType
		,tintDiscoutBaseOn
		,dbo.FormatedDateFromDate(dtValidTo,@numDomainID) AS dtExpire
		,(CASE tintOfferBasedOn
			WHEN 1 THEN (SELECT Stuff((SELECT CONCAT(',',numValue) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 FOR XML PATH('')), 1, 1, ''))
			WHEN 2 THEN (SELECT Stuff((SELECT CONCAT(',',numValue) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=2 FOR XML PATH('')), 1, 1, ''))
		END) As vcPromotionItems
		,(CASE tintDiscoutBaseOn
			WHEN 1 THEN (SELECT Stuff((SELECT CONCAT(',',numValue) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=6 AND tintType=1 FOR XML PATH('')), 1, 1, ''))
			WHEN 2 THEN (SELECT Stuff((SELECT CONCAT(',',numValue) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=6 AND tintType=2 FOR XML PATH('')), 1, 1, ''))
			WHEN 3 THEN (SELECT Stuff((SELECT CONCAT(',',SimilarItems.numItemCode) FROM SimilarItems WHERE 1 = (CASE 
																													WHEN tintOfferBasedOn=1 THEN CASE WHEN numParentItemCode IN (SELECT numValue FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1) THEN 1 ELSE 0 END
																													WHEN tintOfferBasedOn=2 THEN CASE WHEN numParentItemCode IN (SELECT numItemCode FROM Item WHERE numDomainID=@numDomainID AND numItemClassification IN (SELECT numValue FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=2)) THEN 1 ELSE 0 END
																													ELSE 0
																												END) FOR XML PATH('')), 1, 1, ''))
		END) As vcItems
		,CONCAT('Buy '
				,CASE WHEN tintOfferTriggerValueType=1 THEN CAST(fltOfferTriggerValue AS VARCHAR) ELSE CONCAT('$',fltOfferTriggerValue) END
				,CASE tintOfferBasedOn
						WHEN 1 THEN CONCAT(' of "',(SELECT Stuff((SELECT CONCAT(', ',Item.vcItemName) FROM PromotionOfferItems INNER JOIN Item ON PromotionOfferItems.numValue = Item.numItemCode WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 FOR XML PATH('')), 1, 2, '')), '"')
						WHEN 2 THEN CONCAT(' of items belonging to classification(s) "',(SELECT Stuff((SELECT CONCAT(', ',ListDetails.vcData) FROM PromotionOfferItems INNER JOIN ListDetails ON PromotionOfferItems.numValue = ListDetails.numListItemID WHERE numListID=36 AND numProId=PO.numProId AND tintRecordType=5 AND tintType=2 FOR XML PATH('')), 1, 2, '')), '"')
					END
				,' & get '
				, CASE tintDiscountType WHEN 1 THEN CONCAT(fltDiscountValue,'% off on') WHEN 2 THEN CONCAT('$',fltDiscountValue, ' off on') WHEN 3 THEN CONCAT(fltDiscountValue,' of') END
				, CASE 
					WHEN tintDiscoutBaseOn = 1 THEN 
						CONCAT
						(
							CONCAT(' "',(SELECT Stuff((SELECT CONCAT(',',Item.vcItemName) FROM PromotionOfferItems INNER JOIN Item ON PromotionOfferItems.numValue = Item.numItemCode WHERE numProId=PO.numProId AND tintRecordType=6 AND tintType=1 FOR XML PATH('')), 1, 1, '')), '"')
							,(CASE WHEN PO.tintDiscountType = 3 THEN ' free' ELSE '' END)
							,'.'
						)
					WHEN tintDiscoutBaseOn = 2 THEN 
						CONCAT
						(
							CONCAT(' items belonging to classification(s) "',(SELECT Stuff((SELECT CONCAT(',',ListDetails.vcData) FROM PromotionOfferItems INNER JOIN ListDetails ON PromotionOfferItems.numValue = ListDetails.numListItemID WHERE numListID=36 AND numProId=PO.numProId AND tintRecordType=6 AND tintType=2 FOR XML PATH('')), 1, 1, '')), '"')
							,(CASE WHEN PO.tintDiscountType = 3 THEN ' free' ELSE '' END)
							,'.'
						)
					WHEN tintDiscoutBaseOn = 3 THEN ' related items.'
					ELSE '' 
				END
			) AS vcPromotionDescription
			,CONCAT
			(
				(CASE WHEN ISNULL(bitFixShipping1,0)=1 THEN CONCAT('$',monFixShipping1Charge,' shipping on order of $',monFixShipping1OrderAmount,' or more. ') ELSE '' END)
				,(CASE WHEN ISNULL(bitFixShipping2,0)=1  THEN CONCAT('$',monFixShipping2Charge,' shipping on order of $',monFixShipping2OrderAmount,' or more. ') ELSE '' END)
				,(CASE WHEN (bitFreeShiping=1 AND numFreeShippingCountry=@numShipToCountry) THEN CONCAT('Free shipping on order of $',monFreeShippingOrderAmount,'. ') ELSE '' END) 
			) AS vcShippingDescription,
			CASE 
				WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 1 THEN 1
				WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 1 THEN 2
				WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 2 THEN 3
				WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 2 THEN 4
				WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 3 THEN 5
				WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 3 THEN 6
			END AS tintPriority
	FROM
		PromotionOffer PO
		LEFT JOIN ShippingPromotions SP ON PO.numDomainId = SP.numDomainId
	WHERE
		PO.numDomainId=@numDomainID 
		AND numProId IN (SELECT Distinct numPromotionID FROM OpportunityItems WHERE numOppId=@numOppID)     

END
