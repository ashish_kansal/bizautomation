/******************************************************************
Project: Release 11.2 Date: 02.MAR.2019
Comments: ALTER SCRIPTS
*******************************************************************/

/******************************************** SANDEEP *********************************************/

ALTER TABLE eCommerceDTL ADD bitShowPromoDetailsLink BIT DEFAULT 1

------------------

UPDATE eCommerceDTL SET bitShowPromoDetailsLink=1

---------------------

INSERT INTO PageElementMaster
(
	numElementID,vcElementName,vcUserControlPath,vcTagName,bitCustomization,bitAdd,bitDelete,numSiteID
)
VALUES
(
	61,'ItemPromotionPopup','~/UserControls/ItemPromotionPopup.ascx','{#ItemPromotionPopup#}',1,0,0,0
)

INSERT INTO PageElementAttributes
(
	numElementID,vcAttributeName,vcControlType,bitEditor
)
VALUES
(
	61,'Html Customize','HtmlEditor',1
)

------------------------

ALTER TABLE CartItems ADD numPreferredPromotionID NUMERIC(18,0) DEFAULT 0
ALTER TABLE CartItems ADD bitPromotionTrigerred BIT DEFAULT 0
ALTER TABLE CartItems ADD monInsertPrice DECIMAL(20,5)
ALTER TABLE CartItems ADD fltInsertDiscount FLOAT
ALTER TABLE CartItems ADD bitInsertDiscountType BIT

--------------------------------

DROP PROCEDURE USP_ManageECommercePromotion

------------------------------

--POWERSHELL SCRIPT TO COPY NEW IMAGE ADDED TO BIZCART PROJECT TO ALL CURRENT WEBSITE FOLDER. CHANGE PATH ON PRODUCTION
Get-ChildItem | where-object {$_.Psiscontainer -eq "True"} | ForEach-Object { If ((Test-Path (Join-Path -Path $_.FullName -ChildPath 'Images'))) { Copy-Item -Path "D:\Biz2016\Vss\BizCart\Default\images\item-promotion.png" -Destination "$($_.FullName)\Images" }}