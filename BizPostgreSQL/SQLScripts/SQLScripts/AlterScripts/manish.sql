----ALTER DATABASE [Prod.2014] SET READ_COMMITTED_SNAPSHOT ON;

----------/******************************************************************
----------Project: BACRMUI   Date: 05.MAR.2015
----------Comments: Adding new field for default shipping service for an account

----------*******************************************************************/
----BEGIN TRANSACTION

----ALTER TABLE [dbo].[DivisionMaster] ADD numDefaultShippingServiceID NUMERIC(18,0)

----ROLLBACK
----------/******************************************************************
----------Project: BACRMUI   Date: 16.FEB.2015
----------Comments: Adding new Bizdoc "Customer Statement"

----------*******************************************************************/
----BEGIN TRANSACTION

----SELECT * FROM [ListDetails] WHERE [ListDetails].[numListItemID] = 40911
------40911
--------------------------------------DO NOT UPDATE THIS QUERY ON PRODUCTION. THIS IS ALREADY EXECUTED --------------------
----DECLARE @MaxListItemID AS NUMERIC(18,0)
----SELECT @MaxListItemID = 40911--  MAX([LD].[numListItemID]) + 1 FROM [dbo].[ListDetails] AS LD WHERE [LD].[numListID] = 27
----PRINT @MaxListItemID

----SET IDENTITY_INSERT [ListDetails] ON

---- INSERT INTO [dbo].[ListDetails]
----         ( [numListItemID],
----		   [numListID] ,
----           [vcData] ,
----           [numCreatedBY] ,
----           [bintCreatedDate] ,
----           [numModifiedBy] ,
----           [bintModifiedDate] ,
----           [bitDelete] ,
----           [numDomainID] ,
----           [constFlag] ,
----           [sintOrder] ,
----           [numListType]
----         )
---- VALUES  ( @MaxListItemID,
----		   27 , -- numListID - numeric
----           'Customer Statement' , -- vcData - varchar(50)
----           1 , -- numCreatedBY - numeric
----           GETDATE() , -- bintCreatedDate - datetime
----           1 , -- numModifiedBy - numeric
----           GETDATE() , -- bintModifiedDate - datetime
----           0 , -- bitDelete - bit
----           1 , -- numDomainID - numeric
----           1 , -- constFlag - bit
----           19 , -- sintOrder - smallint
----           NULL  -- numListType - numeric
----         )

----SET IDENTITY_INSERT [ListDetails] OFF

--------------------------------------DO NOT UPDATE THIS QUERY ON PRODUCTION. THIS IS ALREADY EXECUTED --------------------


----INSERT INTO [dbo].[BizDocTemplate]
----( [numDomainID] ,[numBizDocID] ,[numOppType] ,[txtBizDocTemplate] ,[txtCSS] ,[bitEnabled] ,[tintTemplateType] ,[vcTemplateName] ,[bitDefault] ,[txtNewBizDocTemplate] ,[vcBizDocImagePath] ,
---- [vcBizDocFooter] ,[vcPurBizDocFooter] ,[bitKeepFooterBottom] ,[numOrientation]
----        )
----VALUES  ( 1 , -- numDomainID - numeric
----          40911 , -- numBizDocID - numeric
----          1 , -- numOppType - numeric
----         '&lt;table width=&quot;100%&quot; border=&quot;0&quot; cellpadding=&quot;0&quot;&gt;
----    &lt;tbody&gt;
----        &lt;tr&gt;
----            &lt;td valign=&quot;top&quot; align=&quot;left&quot;&gt;#Logo#&lt;br /&gt;
----            &lt;/td&gt;
----            &lt;td align=&quot;right&quot; style=&quot;white-space: nowrap;&quot;&gt;&lt;/td&gt;
----            &lt;td align=&quot;right&quot; class=&quot;title&quot; style=&quot;white-space: nowrap;&quot;&gt;#BizDocTemplateName#
----            &lt;/td&gt;
----        &lt;/tr&gt;
----    &lt;/tbody&gt;
----&lt;/table&gt;
----&lt;br /&gt;
----&lt;table width=&quot;100%&quot; border=&quot;0&quot; cellpadding=&quot;0&quot;&gt;
----    &lt;tbody&gt;
----        &lt;tr&gt;
----            &lt;td align=&quot;right&quot; style=&quot;text-align: left;&quot;&gt;&lt;strong&gt;Bill To:&lt;/strong&gt;&lt;br /&gt;
----            &lt;/td&gt;
----            &lt;td align=&quot;right&quot; style=&quot;text-align: center;&quot;&gt;&lt;strong&gt;Statement Date:&lt;/strong&gt;&amp;nbsp;#StatementDate#&lt;br /&gt;
----            &lt;/td&gt;
----        &lt;/tr&gt;
----        &lt;tr&gt;
----            &lt;td align=&quot;right&quot; style=&quot;text-align: left;&quot;&gt;#Customer/VendorOrganizationName#&lt;br /&gt;
----            #Customer/VendorBillToStreet#&lt;br /&gt;
----            #Customer/VendorBillToCity#&lt;br /&gt;
----            #Customer/VendorBillToState#&amp;nbsp;#Customer/VendorBillToPostal#&lt;br /&gt;
----            #Customer/VendorBillToCountry#&lt;br /&gt;
----            &lt;br /&gt;
----            &lt;/td&gt;
----            &lt;td align=&quot;right&quot; style=&quot;text-align: center;&quot;&gt;&lt;span style=&quot;font-size: 24px;&quot;&gt;&lt;strong&gt;Balance Due:&lt;/strong&gt;&amp;nbsp;&amp;nbsp;#BalanceDue#&lt;/span&gt;&lt;/td&gt;
----        &lt;/tr&gt;
----    &lt;/tbody&gt;
----&lt;/table&gt;
----&lt;table width=&quot;75%&quot; border=&quot;0&quot; cellpadding=&quot;0&quot;&gt;
----    &lt;tbody&gt;
----        &lt;tr&gt;
----            &lt;td colspan=&quot;2&quot; style=&quot;text-align: center;&quot;&gt;&lt;strong&gt;&lt;span style=&quot;color: #00b050;&quot;&gt;Current Amt Due&lt;/span&gt;&lt;/strong&gt;&lt;/td&gt;
----            &lt;td colspan=&quot;2&quot; style=&quot;text-align: center;&quot;&gt;&lt;strong&gt;&lt;span style=&quot;color: #ff0000;&quot;&gt;Past Due&lt;/span&gt;&lt;/strong&gt;&lt;/td&gt;
----        &lt;/tr&gt;
----        &lt;tr&gt;
----            &lt;td style=&quot;text-align: right;&quot;&gt;Today-30:&lt;/td&gt;
----            &lt;td&gt;#Today-30(CurrentAmtDue)#&lt;br /&gt;
----            &lt;/td&gt;
----            &lt;td style=&quot;text-align: right;&quot;&gt;Today-30:&lt;/td&gt;
----            &lt;td&gt;#Today-30(PastDue)#&lt;/td&gt;
----        &lt;/tr&gt;
----        &lt;tr&gt;
----            &lt;td style=&quot;text-align: right;&quot;&gt;31-60:&lt;/td&gt;
----            &lt;td&gt;#31-60(CurrentAmtDue)#&lt;br /&gt;
----            &lt;/td&gt;
----            &lt;td style=&quot;text-align: right;&quot;&gt;31-60:&lt;/td&gt;
----            &lt;td&gt;#61-90(PastDue)#&lt;br /&gt;
----            &lt;/td&gt;
----        &lt;/tr&gt;
----        &lt;tr&gt;
----            &lt;td style=&quot;text-align: right;&quot;&gt;61-90:&lt;/td&gt;
----            &lt;td&gt;#61-90(CurrentAmtDue)#&lt;br /&gt;
----            &lt;/td&gt;
----            &lt;td style=&quot;text-align: right;&quot;&gt;61-90:&lt;/td&gt;
----            &lt;td&gt;#61-90(PastDue)#&lt;br /&gt;
----            &lt;/td&gt;
----        &lt;/tr&gt;
----        &lt;tr&gt;
----            &lt;td style=&quot;text-align: right;&quot;&gt;Over 91:&lt;/td&gt;
----            &lt;td&gt;#Over91(CurrentAmtDue)#&lt;br /&gt;
----            &lt;/td&gt;
----            &lt;td style=&quot;text-align: right;&quot;&gt;Over 91:&lt;/td&gt;
----            &lt;td&gt;#Over91(PastDue)#&lt;br /&gt;
----            &lt;/td&gt;
----        &lt;/tr&gt;
----    &lt;/tbody&gt;
----&lt;/table&gt;
----&lt;br /&gt;
----&lt;table width=&quot;100%&quot; border=&quot;0&quot; cellpadding=&quot;0&quot;&gt;
----    &lt;tbody&gt;
----        &lt;tr&gt;
----            &lt;td style=&quot;text-align: center;&quot;&gt;#Products#
----            &lt;/td&gt;
----        &lt;/tr&gt;
----    &lt;/tbody&gt;
----&lt;/table&gt;', -- txtBizDocTemplate - text
----          '.title{color: #696969; /*padding:10px;*/font: bold 30px Arial, Helvetica, sans-serif;}
----			.RowHeader{background-color: #dedede;color: #333;font-family: Arial;font-size: 8pt;}
----			.RowHeader.hyperlink{color: #333;}
----			.ItemHeader, .ItemStyle, .AltItemStyle{border-width: 1px;padding: 8px;font: normal 12px/17px arial;border-style: solid;border-color: #666666;background-color: #dedede;}
----			.ItemHeader{border-width: 1px;padding: 8px;border-style: solid;border-color: #666666;}
----			.ItemStyle td{border-width: 1px;padding: 8px;border-style: solid;border-color: #666666;background-color: #ffffff;}
----			.AltItemStyle{background-color: White;border-color: black;padding: 8px;}
----			.AltItemStyle td{padding: 8px;}
----			.ItemHeader td{border-width: 1px;padding: 8px;font-weight: bold;border-style: solid;border-color: #666666;background-color: #dedede;}
----			.ItemHeader td th{border-width: 1px;padding: 8px;border-style: solid;border-color: #666666;background-color: #dedede;}
----			#tblBizDocSumm{font: normal 12px verdana;color: #333;border-width: 1px;border-color: #666666;border-collapse: collapse;background-color: #dedede;}
----			#tblBizDocSumm td{border-width: 1px;padding: 8px;border-style: solid;border-color: #666666;}
----			.WordWrapSerialNo{width: 30%;word-break: break-all;}' , -- txtCSS - text
----          1 , -- bitEnabled - bit
----          0 , -- tintTemplateType - tinyint
----          'Customer Statement' , -- vcTemplateName - varchar(50)
----          1 , -- bitDefault - bit
----          '&lt;table width=&quot;100%&quot; border=&quot;0&quot; cellpadding=&quot;0&quot;&gt;
----    &lt;tbody&gt;
----        &lt;tr&gt;
----            &lt;td valign=&quot;top&quot; align=&quot;left&quot;&gt;#Logo#&lt;br /&gt;
----            &lt;/td&gt;
----            &lt;td align=&quot;right&quot; style=&quot;white-space: nowrap;&quot;&gt;&lt;/td&gt;
----            &lt;td align=&quot;right&quot; class=&quot;title&quot; style=&quot;white-space: nowrap;&quot;&gt;#BizDocTemplateName#
----            &lt;/td&gt;
----        &lt;/tr&gt;
----    &lt;/tbody&gt;
----&lt;/table&gt;
----&lt;br /&gt;
----&lt;table width=&quot;100%&quot; border=&quot;0&quot; cellpadding=&quot;0&quot;&gt;
----    &lt;tbody&gt;
----        &lt;tr&gt;
----            &lt;td align=&quot;right&quot; style=&quot;text-align: left;&quot;&gt;&lt;strong&gt;Bill To:&lt;/strong&gt;&lt;br /&gt;
----            &lt;/td&gt;
----            &lt;td align=&quot;right&quot; style=&quot;text-align: center;&quot;&gt;&lt;strong&gt;Statement Date:&lt;/strong&gt;&amp;nbsp;#StatementDate#&lt;br /&gt;
----            &lt;/td&gt;
----        &lt;/tr&gt;
----        &lt;tr&gt;
----            &lt;td align=&quot;right&quot; style=&quot;text-align: left;&quot;&gt;#Customer/VendorOrganizationName#&lt;br /&gt;
----            #Customer/VendorBillToStreet#&lt;br /&gt;
----            #Customer/VendorBillToCity#&lt;br /&gt;
----            #Customer/VendorBillToState#&amp;nbsp;#Customer/VendorBillToPostal#&lt;br /&gt;
----            #Customer/VendorBillToCountry#&lt;br /&gt;
----            &lt;br /&gt;
----            &lt;/td&gt;
----            &lt;td align=&quot;right&quot; style=&quot;text-align: center;&quot;&gt;&lt;span style=&quot;font-size: 24px;&quot;&gt;&lt;strong&gt;Balance Due:&lt;/strong&gt;&amp;nbsp;&amp;nbsp;#BalanceDue#&lt;/span&gt;&lt;/td&gt;
----        &lt;/tr&gt;
----    &lt;/tbody&gt;
----&lt;/table&gt;
----&lt;table width=&quot;75%&quot; border=&quot;0&quot; cellpadding=&quot;0&quot;&gt;
----    &lt;tbody&gt;
----        &lt;tr&gt;
----            &lt;td colspan=&quot;2&quot; style=&quot;text-align: center;&quot;&gt;&lt;strong&gt;&lt;span style=&quot;color: #00b050;&quot;&gt;Current Amt Due&lt;/span&gt;&lt;/strong&gt;&lt;/td&gt;
----            &lt;td colspan=&quot;2&quot; style=&quot;text-align: center;&quot;&gt;&lt;strong&gt;&lt;span style=&quot;color: #ff0000;&quot;&gt;Past Due&lt;/span&gt;&lt;/strong&gt;&lt;/td&gt;
----        &lt;/tr&gt;
----        &lt;tr&gt;
----            &lt;td style=&quot;text-align: right;&quot;&gt;Today-30:&lt;/td&gt;
----            &lt;td&gt;#Today-30(CurrentAmtDue)#&lt;br /&gt;
----            &lt;/td&gt;
----            &lt;td style=&quot;text-align: right;&quot;&gt;Today-30:&lt;/td&gt;
----            &lt;td&gt;#Today-30(PastDue)#&lt;/td&gt;
----        &lt;/tr&gt;
----        &lt;tr&gt;
----            &lt;td style=&quot;text-align: right;&quot;&gt;31-60:&lt;/td&gt;
----            &lt;td&gt;#31-60(CurrentAmtDue)#&lt;br /&gt;
----            &lt;/td&gt;
----            &lt;td style=&quot;text-align: right;&quot;&gt;31-60:&lt;/td&gt;
----            &lt;td&gt;#61-90(PastDue)#&lt;br /&gt;
----            &lt;/td&gt;
----        &lt;/tr&gt;
----        &lt;tr&gt;
----            &lt;td style=&quot;text-align: right;&quot;&gt;61-90:&lt;/td&gt;
----            &lt;td&gt;#61-90(CurrentAmtDue)#&lt;br /&gt;
----            &lt;/td&gt;
----            &lt;td style=&quot;text-align: right;&quot;&gt;61-90:&lt;/td&gt;
----            &lt;td&gt;#61-90(PastDue)#&lt;br /&gt;
----            &lt;/td&gt;
----        &lt;/tr&gt;
----        &lt;tr&gt;
----            &lt;td style=&quot;text-align: right;&quot;&gt;Over 91:&lt;/td&gt;
----            &lt;td&gt;#Over91(CurrentAmtDue)#&lt;br /&gt;
----            &lt;/td&gt;
----            &lt;td style=&quot;text-align: right;&quot;&gt;Over 91:&lt;/td&gt;
----            &lt;td&gt;#Over91(PastDue)#&lt;br /&gt;
----            &lt;/td&gt;
----        &lt;/tr&gt;
----    &lt;/tbody&gt;
----&lt;/table&gt;
----&lt;br /&gt;
----&lt;table width=&quot;100%&quot; border=&quot;0&quot; cellpadding=&quot;0&quot;&gt;
----    &lt;tbody&gt;
----        &lt;tr&gt;
----            &lt;td style=&quot;text-align: center;&quot;&gt;#Products#
----            &lt;/td&gt;
----        &lt;/tr&gt;
----    &lt;/tbody&gt;
----&lt;/table&gt;' , -- txtNewBizDocTemplate - text
----          NULL , -- vcBizDocImagePath - varchar(100)
----          NULL , -- vcBizDocFooter - varchar(100)
----          NULL , -- vcPurBizDocFooter - varchar(100)
----          0 , -- bitKeepFooterBottom - bit
----          1  -- numOrientation - int
----        )
----ROLLBACK


------ Inserting Bizdoc fields

---- --- GRID COLUMNS -----

------Invoice
----DECLARE @numMaxFieldId AS NUMERIC(18,0)
----SELECT @numMaxFieldId = MAX(numFieldID) + 1 FROM [dbo].[DycFieldMaster] AS DFM
----PRINT @numMaxFieldId

----SET IDENTITY_INSERT [dbo].[DycFieldMaster] ON

----INSERT INTO [dbo].[DycFieldMaster]
----        ( [numFieldId],
----		  [numModuleID] ,
----          [numDomainID] ,
----          [vcFieldName] ,
----          [vcDbColumnName] ,
----          [vcOrigDbColumnName] ,
----          [vcPropertyName] ,
----          [vcLookBackTableName] ,
----          [vcFieldDataType] ,
----          [vcFieldType] ,
----          [vcAssociatedControlType] ,
----          [vcToolTip] ,
----          [vcListItemType] ,
----          [numListID] ,
----          [PopupFunctionName] ,
----          [order] ,
----          [tintRow] ,
----          [tintColumn] ,
----          [bitInResults] ,
----          [bitDeleted] ,
----          [bitAllowEdit] ,
----          [bitDefault] ,
----          [bitSettingField] ,
----          [bitAddField] ,
----          [bitDetailField] ,
----          [bitAllowSorting] ,
----          [bitWorkFlowField] ,
----          [bitImport] ,
----          [bitExport] ,
----          [bitAllowFiltering] ,
----          [bitInlineEdit] ,
----          [bitRequired] ,
----          [intColumnWidth] ,
----          [intFieldMaxLength] ,
----          [intWFCompare] ,
----          [vcGroup] ,
----          [vcWFCompareField]
----        )
----VALUES  ( @numMaxFieldId , -- numFieldID - numeric
----		  4 , -- numModuleID - numeric
----          NULL , -- numDomainID - numeric
----          N'Invoice' , -- vcFieldName - nvarchar(50)
----          N'vcBizDocID' , -- vcDbColumnName - nvarchar(50)
----          N'vcBizDocID' , -- vcOrigDbColumnName - nvarchar(50)
----          'InvoiceID' , -- vcPropertyName - varchar(100)
----          N'CSGrid' , -- vcLookBackTableName - nvarchar(50)
----          'V' , -- vcFieldDataType - char(1)
----          'R' , -- vcFieldType - char(1)
----          N'TextBox' , -- vcAssociatedControlType - nvarchar(50)
----          N'InvoiceID' , -- vcToolTip - nvarchar(1000)
----          '' , -- vcListItemType - varchar(3)
----          0 , -- numListID - numeric
----          NULL , -- PopupFunctionName - varchar(100)
----          63 , -- order - tinyint
----          NULL , -- tintRow - tinyint
----          NULL , -- tintColumn - tinyint
----          1 , -- bitInResults - bit
----          0 , -- bitDeleted - bit
----          1 , -- bitAllowEdit - bit
----          1 , -- bitDefault - bit
----          1 , -- bitSettingField - bit
----          NULL , -- bitAddField - bit
----          NULL , -- bitDetailField - bit
----          1 , -- bitAllowSorting - bit
----          NULL , -- bitWorkFlowField - bit
----          NULL , -- bitImport - bit
----          NULL , -- bitExport - bit
----          1 , -- bitAllowFiltering - bit
----          NULL , -- bitInlineEdit - bit
----          NULL , -- bitRequired - bit
----          NULL , -- intColumnWidth - int
----          NULL , -- intFieldMaxLength - int
----          NULL , -- intWFCompare - int
----          NULL , -- vcGroup - varchar(100)
----          NULL  -- vcWFCompareField - varchar(150)
----        )

----SET IDENTITY_INSERT [dbo].[DycFieldMaster] OFF

----INSERT INTO [dbo].[DycFormField_Mapping]
----        ( [numModuleID] ,
----          [numFieldID] ,
----          [numDomainID] ,
----          [numFormID] ,
----          [bitAllowEdit] ,
----          [bitInlineEdit] ,
----          [vcFieldName] ,
----          [vcAssociatedControlType] ,
----          [vcPropertyName] ,
----          [PopupFunctionName] ,
----          [order] ,
----          [tintRow] ,
----          [tintColumn] ,
----          [bitInResults] ,
----          [bitDeleted] ,
----          [bitDefault] ,
----          [bitSettingField] ,
----          [bitAddField] ,
----          [bitDetailField] ,
----          [bitAllowSorting] ,
----          [bitWorkFlowField] ,
----          [bitImport] ,
----          [bitExport] ,
----          [bitAllowFiltering] ,
----          [bitRequired] ,
----          [numFormFieldID] ,
----          [intSectionID] ,
----          [bitAllowGridColor]
----        )
----VALUES  ( 4 , -- numModuleID - numeric
----          @numMaxFieldId , -- numFieldID - numeric
----          NULL , -- numDomainID - numeric
----          7 , -- numFormID - numeric
----          1 , -- bitAllowEdit - bit
----          1 , -- bitInlineEdit - bit
----          N'Invoice' , -- vcFieldName - nvarchar(50)
----          N'TextBox' , -- vcAssociatedControlType - nvarchar(50)
----          'InvoiceID' , -- vcPropertyName - varchar(100)
----          '' , -- PopupFunctionName - varchar(100)
----          0 , -- order - tinyint
----          1 , -- tintRow - tinyint
----          0 , -- tintColumn - tinyint
----          1 , -- bitInResults - bit
----          0 , -- bitDeleted - bit
----          1 , -- bitDefault - bit
----          NULL , -- bitSettingField - bit
----          NULL , -- bitAddField - bit
----          NULL , -- bitDetailField - bit
----          NULL , -- bitAllowSorting - bit
----          NULL , -- bitWorkFlowField - bit
----          NULL , -- bitImport - bit
----          NULL , -- bitExport - bit
----          NULL , -- bitAllowFiltering - bit
----          NULL , -- bitRequired - bit
----          NULL , -- numFormFieldID - numeric
----          NULL, -- intSectionID - int
----          NULL  -- bitAllowGridColor - bit
----        )

---------------------------------

------ Order ID
----DECLARE @numMaxFieldId AS NUMERIC(18,0)
----SELECT @numMaxFieldId = MAX(numFieldID) + 1 FROM [dbo].[DycFieldMaster] AS DFM
----PRINT @numMaxFieldId

----SET IDENTITY_INSERT [dbo].[DycFieldMaster] ON

----INSERT INTO [dbo].[DycFieldMaster]
----        ( [numFieldId],
----		  [numModuleID] ,
----          [numDomainID] ,
----          [vcFieldName] ,
----          [vcDbColumnName] ,
----          [vcOrigDbColumnName] ,
----          [vcPropertyName] ,
----          [vcLookBackTableName] ,
----          [vcFieldDataType] ,
----          [vcFieldType] ,
----          [vcAssociatedControlType] ,
----          [vcToolTip] ,
----          [vcListItemType] ,
----          [numListID] ,
----          [PopupFunctionName] ,
----          [order] ,
----          [tintRow] ,
----          [tintColumn] ,
----          [bitInResults] ,
----          [bitDeleted] ,
----          [bitAllowEdit] ,
----          [bitDefault] ,
----          [bitSettingField] ,
----          [bitAddField] ,
----          [bitDetailField] ,
----          [bitAllowSorting] ,
----          [bitWorkFlowField] ,
----          [bitImport] ,
----          [bitExport] ,
----          [bitAllowFiltering] ,
----          [bitInlineEdit] ,
----          [bitRequired] ,
----          [intColumnWidth] ,
----          [intFieldMaxLength] ,
----          [intWFCompare] ,
----          [vcGroup] ,
----          [vcWFCompareField]
----        )
----VALUES  ( @numMaxFieldId , -- numFieldID - numeric
----		  4 , -- numModuleID - numeric
----          NULL , -- numDomainID - numeric
----          N'Order ID' , -- vcFieldName - nvarchar(50)
----          N'vcPOppName' , -- vcDbColumnName - nvarchar(50)
----          N'vcPOppName' , -- vcOrigDbColumnName - nvarchar(50)
----          'OrderID' , -- vcPropertyName - varchar(100)
----          N'CSGrid' , -- vcLookBackTableName - nvarchar(50)
----          'V' , -- vcFieldDataType - char(1)
----          'R' , -- vcFieldType - char(1)
----          N'TextBox' , -- vcAssociatedControlType - nvarchar(50)
----          N'OrderID' , -- vcToolTip - nvarchar(1000)
----          '' , -- vcListItemType - varchar(3)
----          0 , -- numListID - numeric
----          NULL , -- PopupFunctionName - varchar(100)
----          63 , -- order - tinyint
----          NULL , -- tintRow - tinyint
----          NULL , -- tintColumn - tinyint
----          1 , -- bitInResults - bit
----          0 , -- bitDeleted - bit
----          1 , -- bitAllowEdit - bit
----          1 , -- bitDefault - bit
----          1 , -- bitSettingField - bit
----          NULL , -- bitAddField - bit
----          NULL , -- bitDetailField - bit
----          1 , -- bitAllowSorting - bit
----          NULL , -- bitWorkFlowField - bit
----          NULL , -- bitImport - bit
----          NULL , -- bitExport - bit
----          1 , -- bitAllowFiltering - bit
----          NULL , -- bitInlineEdit - bit
----          NULL , -- bitRequired - bit
----          NULL , -- intColumnWidth - int
----          NULL , -- intFieldMaxLength - int
----          NULL , -- intWFCompare - int
----          NULL , -- vcGroup - varchar(100)
----          NULL  -- vcWFCompareField - varchar(150)
----        )

----SET IDENTITY_INSERT [dbo].[DycFieldMaster] OFF

----INSERT INTO [dbo].[DycFormField_Mapping]
----        ( [numModuleID] ,
----          [numFieldID] ,
----          [numDomainID] ,
----          [numFormID] ,
----          [bitAllowEdit] ,
----          [bitInlineEdit] ,
----          [vcFieldName] ,
----          [vcAssociatedControlType] ,
----          [vcPropertyName] ,
----          [PopupFunctionName] ,
----          [order] ,
----          [tintRow] ,
----          [tintColumn] ,
----          [bitInResults] ,
----          [bitDeleted] ,
----          [bitDefault] ,
----          [bitSettingField] ,
----          [bitAddField] ,
----          [bitDetailField] ,
----          [bitAllowSorting] ,
----          [bitWorkFlowField] ,
----          [bitImport] ,
----          [bitExport] ,
----          [bitAllowFiltering] ,
----          [bitRequired] ,
----          [numFormFieldID] ,
----          [intSectionID] ,
----          [bitAllowGridColor]
----        )
----VALUES  ( 4 , -- numModuleID - numeric
----          @numMaxFieldId , -- numFieldID - numeric
----          NULL , -- numDomainID - numeric
----          7 , -- numFormID - numeric
----          1 , -- bitAllowEdit - bit
----          1 , -- bitInlineEdit - bit
----          N'Order ID' , -- vcFieldName - nvarchar(50)
----          N'TextBox' , -- vcAssociatedControlType - nvarchar(50)
----          'OrderID' , -- vcPropertyName - varchar(100)
----          '' , -- PopupFunctionName - varchar(100)
----          0 , -- order - tinyint
----          1 , -- tintRow - tinyint
----          0 , -- tintColumn - tinyint
----          1 , -- bitInResults - bit
----          0 , -- bitDeleted - bit
----          1 , -- bitDefault - bit
----          NULL , -- bitSettingField - bit
----          NULL , -- bitAddField - bit
----          NULL , -- bitDetailField - bit
----          NULL , -- bitAllowSorting - bit
----          NULL , -- bitWorkFlowField - bit
----          NULL , -- bitImport - bit
----          NULL , -- bitExport - bit
----          NULL , -- bitAllowFiltering - bit
----          NULL , -- bitRequired - bit
----          NULL , -- numFormFieldID - numeric
----          NULL, -- intSectionID - int
----          NULL  -- bitAllowGridColor - bit
----        )
-----------------------------

------ Billing Date
----DECLARE @numMaxFieldId AS NUMERIC(18,0)
----SELECT @numMaxFieldId = MAX(numFieldID) + 1 FROM [dbo].[DycFieldMaster] AS DFM
----PRINT @numMaxFieldId

----SET IDENTITY_INSERT [dbo].[DycFieldMaster] ON

----INSERT INTO [dbo].[DycFieldMaster]
----        ( [numFieldId],
----		  [numModuleID] ,
----          [numDomainID] ,
----          [vcFieldName] ,
----          [vcDbColumnName] ,
----          [vcOrigDbColumnName] ,
----          [vcPropertyName] ,
----          [vcLookBackTableName] ,
----          [vcFieldDataType] ,
----          [vcFieldType] ,
----          [vcAssociatedControlType] ,
----          [vcToolTip] ,
----          [vcListItemType] ,
----          [numListID] ,
----          [PopupFunctionName] ,
----          [order] ,
----          [tintRow] ,
----          [tintColumn] ,
----          [bitInResults] ,
----          [bitDeleted] ,
----          [bitAllowEdit] ,
----          [bitDefault] ,
----          [bitSettingField] ,
----          [bitAddField] ,
----          [bitDetailField] ,
----          [bitAllowSorting] ,
----          [bitWorkFlowField] ,
----          [bitImport] ,
----          [bitExport] ,
----          [bitAllowFiltering] ,
----          [bitInlineEdit] ,
----          [bitRequired] ,
----          [intColumnWidth] ,
----          [intFieldMaxLength] ,
----          [intWFCompare] ,
----          [vcGroup] ,
----          [vcWFCompareField]
----        )
----VALUES  ( @numMaxFieldId , -- numFieldID - numeric
----		  4 , -- numModuleID - numeric
----          NULL , -- numDomainID - numeric
----          N'Billing Date' , -- vcFieldName - nvarchar(50)
----          N'dtFromDate' , -- vcDbColumnName - nvarchar(50)
----          N'dtFromDate' , -- vcOrigDbColumnName - nvarchar(50)
----          'BillingDate' , -- vcPropertyName - varchar(100)
----          N'CSGrid' , -- vcLookBackTableName - nvarchar(50)
----          'V' , -- vcFieldDataType - char(1)
----          'R' , -- vcFieldType - char(1)
----          N'TextBox' , -- vcAssociatedControlType - nvarchar(50)
----          N'Billing Date' , -- vcToolTip - nvarchar(1000)
----          '' , -- vcListItemType - varchar(3)
----          0 , -- numListID - numeric
----          NULL , -- PopupFunctionName - varchar(100)
----          63 , -- order - tinyint
----          NULL , -- tintRow - tinyint
----          NULL , -- tintColumn - tinyint
----          1 , -- bitInResults - bit
----          0 , -- bitDeleted - bit
----          1 , -- bitAllowEdit - bit
----          1 , -- bitDefault - bit
----          1 , -- bitSettingField - bit
----          NULL , -- bitAddField - bit
----          NULL , -- bitDetailField - bit
----          1 , -- bitAllowSorting - bit
----          NULL , -- bitWorkFlowField - bit
----          NULL , -- bitImport - bit
----          NULL , -- bitExport - bit
----          1 , -- bitAllowFiltering - bit
----          NULL , -- bitInlineEdit - bit
----          NULL , -- bitRequired - bit
----          NULL , -- intColumnWidth - int
----          NULL , -- intFieldMaxLength - int
----          NULL , -- intWFCompare - int
----          NULL , -- vcGroup - varchar(100)
----          NULL  -- vcWFCompareField - varchar(150)
----        )

----SET IDENTITY_INSERT [dbo].[DycFieldMaster] OFF

----INSERT INTO [dbo].[DycFormField_Mapping]
----        ( [numModuleID] ,
----          [numFieldID] ,
----          [numDomainID] ,
----          [numFormID] ,
----          [bitAllowEdit] ,
----          [bitInlineEdit] ,
----          [vcFieldName] ,
----          [vcAssociatedControlType] ,
----          [vcPropertyName] ,
----          [PopupFunctionName] ,
----          [order] ,
----          [tintRow] ,
----          [tintColumn] ,
----          [bitInResults] ,
----          [bitDeleted] ,
----          [bitDefault] ,
----          [bitSettingField] ,
----          [bitAddField] ,
----          [bitDetailField] ,
----          [bitAllowSorting] ,
----          [bitWorkFlowField] ,
----          [bitImport] ,
----          [bitExport] ,
----          [bitAllowFiltering] ,
----          [bitRequired] ,
----          [numFormFieldID] ,
----          [intSectionID] ,
----          [bitAllowGridColor]
----        )
----VALUES  ( 4 , -- numModuleID - numeric
----          @numMaxFieldId , -- numFieldID - numeric
----          NULL , -- numDomainID - numeric
----          7 , -- numFormID - numeric
----          1 , -- bitAllowEdit - bit
----          1 , -- bitInlineEdit - bit
----          N'Billing Date' , -- vcFieldName - nvarchar(50)
----          N'TextBox' , -- vcAssociatedControlType - nvarchar(50)
----          'BillingDate' , -- vcPropertyName - varchar(100)
----          '' , -- PopupFunctionName - varchar(100)
----          0 , -- order - tinyint
----          1 , -- tintRow - tinyint
----          0 , -- tintColumn - tinyint
----          1 , -- bitInResults - bit
----          0 , -- bitDeleted - bit
----          1 , -- bitDefault - bit
----          NULL , -- bitSettingField - bit
----          NULL , -- bitAddField - bit
----          NULL , -- bitDetailField - bit
----          NULL , -- bitAllowSorting - bit
----          NULL , -- bitWorkFlowField - bit
----          NULL , -- bitImport - bit
----          NULL , -- bitExport - bit
----          NULL , -- bitAllowFiltering - bit
----          NULL , -- bitRequired - bit
----          NULL , -- numFormFieldID - numeric
----          NULL, -- intSectionID - int
----          NULL  -- bitAllowGridColor - bit
----        )

---------------------------------


------ Due Date
----DECLARE @numMaxFieldId AS NUMERIC(18,0)
----SELECT @numMaxFieldId = MAX(numFieldID) + 1 FROM [dbo].[DycFieldMaster] AS DFM
----PRINT @numMaxFieldId

----SET IDENTITY_INSERT [dbo].[DycFieldMaster] ON

----INSERT INTO [dbo].[DycFieldMaster]
----        ( [numFieldId],
----		  [numModuleID] ,
----          [numDomainID] ,
----          [vcFieldName] ,
----          [vcDbColumnName] ,
----          [vcOrigDbColumnName] ,
----          [vcPropertyName] ,
----          [vcLookBackTableName] ,
----          [vcFieldDataType] ,
----          [vcFieldType] ,
----          [vcAssociatedControlType] ,
----          [vcToolTip] ,
----          [vcListItemType] ,
----          [numListID] ,
----          [PopupFunctionName] ,
----          [order] ,
----          [tintRow] ,
----          [tintColumn] ,
----          [bitInResults] ,
----          [bitDeleted] ,
----          [bitAllowEdit] ,
----          [bitDefault] ,
----          [bitSettingField] ,
----          [bitAddField] ,
----          [bitDetailField] ,
----          [bitAllowSorting] ,
----          [bitWorkFlowField] ,
----          [bitImport] ,
----          [bitExport] ,
----          [bitAllowFiltering] ,
----          [bitInlineEdit] ,
----          [bitRequired] ,
----          [intColumnWidth] ,
----          [intFieldMaxLength] ,
----          [intWFCompare] ,
----          [vcGroup] ,
----          [vcWFCompareField]
----        )
----VALUES  ( @numMaxFieldId , -- numFieldID - numeric
----		  4 , -- numModuleID - numeric
----          NULL , -- numDomainID - numeric
----          N'Due Date' , -- vcFieldName - nvarchar(50)
----          N'DueDate' , -- vcDbColumnName - nvarchar(50)
----          N'DueDate' , -- vcOrigDbColumnName - nvarchar(50)
----          'DueDate' , -- vcPropertyName - varchar(100)
----          N'CSGrid' , -- vcLookBackTableName - nvarchar(50)
----          'V' , -- vcFieldDataType - char(1)
----          'R' , -- vcFieldType - char(1)
----          N'TextBox' , -- vcAssociatedControlType - nvarchar(50)
----          N'Due Date' , -- vcToolTip - nvarchar(1000)
----          '' , -- vcListItemType - varchar(3)
----          0 , -- numListID - numeric
----          NULL , -- PopupFunctionName - varchar(100)
----          63 , -- order - tinyint
----          NULL , -- tintRow - tinyint
----          NULL , -- tintColumn - tinyint
----          1 , -- bitInResults - bit
----          0 , -- bitDeleted - bit
----          1 , -- bitAllowEdit - bit
----          1 , -- bitDefault - bit
----          1 , -- bitSettingField - bit
----          NULL , -- bitAddField - bit
----          NULL , -- bitDetailField - bit
----          1 , -- bitAllowSorting - bit
----          NULL , -- bitWorkFlowField - bit
----          NULL , -- bitImport - bit
----          NULL , -- bitExport - bit
----          1 , -- bitAllowFiltering - bit
----          NULL , -- bitInlineEdit - bit
----          NULL , -- bitRequired - bit
----          NULL , -- intColumnWidth - int
----          NULL , -- intFieldMaxLength - int
----          NULL , -- intWFCompare - int
----          NULL , -- vcGroup - varchar(100)
----          NULL  -- vcWFCompareField - varchar(150)
----        )

----SET IDENTITY_INSERT [dbo].[DycFieldMaster] OFF

----INSERT INTO [dbo].[DycFormField_Mapping]
----        ( [numModuleID] ,
----          [numFieldID] ,
----          [numDomainID] ,
----          [numFormID] ,
----          [bitAllowEdit] ,
----          [bitInlineEdit] ,
----          [vcFieldName] ,
----          [vcAssociatedControlType] ,
----          [vcPropertyName] ,
----          [PopupFunctionName] ,
----          [order] ,
----          [tintRow] ,
----          [tintColumn] ,
----          [bitInResults] ,
----          [bitDeleted] ,
----          [bitDefault] ,
----          [bitSettingField] ,
----          [bitAddField] ,
----          [bitDetailField] ,
----          [bitAllowSorting] ,
----          [bitWorkFlowField] ,
----          [bitImport] ,
----          [bitExport] ,
----          [bitAllowFiltering] ,
----          [bitRequired] ,
----          [numFormFieldID] ,
----          [intSectionID] ,
----          [bitAllowGridColor]
----        )
----VALUES  ( 4 , -- numModuleID - numeric
----          @numMaxFieldId , -- numFieldID - numeric
----          NULL , -- numDomainID - numeric
----          7 , -- numFormID - numeric
----          1 , -- bitAllowEdit - bit
----          1 , -- bitInlineEdit - bit
----          N'Due Date' , -- vcFieldName - nvarchar(50)
----          N'TextBox' , -- vcAssociatedControlType - nvarchar(50)
----          'DueDate' , -- vcPropertyName - varchar(100)
----          '' , -- PopupFunctionName - varchar(100)
----          0 , -- order - tinyint
----          1 , -- tintRow - tinyint
----          0 , -- tintColumn - tinyint
----          1 , -- bitInResults - bit
----          0 , -- bitDeleted - bit
----          1 , -- bitDefault - bit
----          NULL , -- bitSettingField - bit
----          NULL , -- bitAddField - bit
----          NULL , -- bitDetailField - bit
----          NULL , -- bitAllowSorting - bit
----          NULL , -- bitWorkFlowField - bit
----          NULL , -- bitImport - bit
----          NULL , -- bitExport - bit
----          NULL , -- bitAllowFiltering - bit
----          NULL , -- bitRequired - bit
----          NULL , -- numFormFieldID - numeric
----          NULL, -- intSectionID - int
----          NULL  -- bitAllowGridColor - bit
----        )

---------------------------------


------ Total Invoice
----DECLARE @numMaxFieldId AS NUMERIC(18,0)
----SELECT @numMaxFieldId = MAX(numFieldID) + 1 FROM [dbo].[DycFieldMaster] AS DFM
----PRINT @numMaxFieldId

----SET IDENTITY_INSERT [dbo].[DycFieldMaster] ON

----INSERT INTO [dbo].[DycFieldMaster]
----        ( [numFieldId],
----		  [numModuleID] ,
----          [numDomainID] ,
----          [vcFieldName] ,
----          [vcDbColumnName] ,
----          [vcOrigDbColumnName] ,
----          [vcPropertyName] ,
----          [vcLookBackTableName] ,
----          [vcFieldDataType] ,
----          [vcFieldType] ,
----          [vcAssociatedControlType] ,
----          [vcToolTip] ,
----          [vcListItemType] ,
----          [numListID] ,
----          [PopupFunctionName] ,
----          [order] ,
----          [tintRow] ,
----          [tintColumn] ,
----          [bitInResults] ,
----          [bitDeleted] ,
----          [bitAllowEdit] ,
----          [bitDefault] ,
----          [bitSettingField] ,
----          [bitAddField] ,
----          [bitDetailField] ,
----          [bitAllowSorting] ,
----          [bitWorkFlowField] ,
----          [bitImport] ,
----          [bitExport] ,
----          [bitAllowFiltering] ,
----          [bitInlineEdit] ,
----          [bitRequired] ,
----          [intColumnWidth] ,
----          [intFieldMaxLength] ,
----          [intWFCompare] ,
----          [vcGroup] ,
----          [vcWFCompareField]
----        )
----VALUES  ( @numMaxFieldId , -- numFieldID - numeric
----		  4 , -- numModuleID - numeric
----          NULL , -- numDomainID - numeric
----          N'Total Invoice' , -- vcFieldName - nvarchar(50)
----          N'TotalAmount' , -- vcDbColumnName - nvarchar(50)
----          N'TotalAmount' , -- vcOrigDbColumnName - nvarchar(50)
----          'TotalInvoice' , -- vcPropertyName - varchar(100)
----          N'CSGrid' , -- vcLookBackTableName - nvarchar(50)
----          'N' , -- vcFieldDataType - char(1)
----          'R' , -- vcFieldType - char(1)
----          N'TextBox' , -- vcAssociatedControlType - nvarchar(50)
----          N'Total Invoice' , -- vcToolTip - nvarchar(1000)
----          '' , -- vcListItemType - varchar(3)
----          0 , -- numListID - numeric
----          NULL , -- PopupFunctionName - varchar(100)
----          63 , -- order - tinyint
----          NULL , -- tintRow - tinyint
----          NULL , -- tintColumn - tinyint
----          1 , -- bitInResults - bit
----          0 , -- bitDeleted - bit
----          1 , -- bitAllowEdit - bit
----          1 , -- bitDefault - bit
----          1 , -- bitSettingField - bit
----          NULL , -- bitAddField - bit
----          NULL , -- bitDetailField - bit
----          1 , -- bitAllowSorting - bit
----          NULL , -- bitWorkFlowField - bit
----          NULL , -- bitImport - bit
----          NULL , -- bitExport - bit
----          1 , -- bitAllowFiltering - bit
----          NULL , -- bitInlineEdit - bit
----          NULL , -- bitRequired - bit
----          NULL , -- intColumnWidth - int
----          NULL , -- intFieldMaxLength - int
----          NULL , -- intWFCompare - int
----          NULL , -- vcGroup - varchar(100)
----          NULL  -- vcWFCompareField - varchar(150)
----        )

----SET IDENTITY_INSERT [dbo].[DycFieldMaster] OFF

----INSERT INTO [dbo].[DycFormField_Mapping]
----        ( [numModuleID] ,
----          [numFieldID] ,
----          [numDomainID] ,
----          [numFormID] ,
----          [bitAllowEdit] ,
----          [bitInlineEdit] ,
----          [vcFieldName] ,
----          [vcAssociatedControlType] ,
----          [vcPropertyName] ,
----          [PopupFunctionName] ,
----          [order] ,
----          [tintRow] ,
----          [tintColumn] ,
----          [bitInResults] ,
----          [bitDeleted] ,
----          [bitDefault] ,
----          [bitSettingField] ,
----          [bitAddField] ,
----          [bitDetailField] ,
----          [bitAllowSorting] ,
----          [bitWorkFlowField] ,
----          [bitImport] ,
----          [bitExport] ,
----          [bitAllowFiltering] ,
----          [bitRequired] ,
----          [numFormFieldID] ,
----          [intSectionID] ,
----          [bitAllowGridColor]
----        )
----VALUES  ( 4 , -- numModuleID - numeric
----          @numMaxFieldId , -- numFieldID - numeric
----          NULL , -- numDomainID - numeric
----          7 , -- numFormID - numeric
----          1 , -- bitAllowEdit - bit
----          1 , -- bitInlineEdit - bit
----          N'Total Invoice' , -- vcFieldName - nvarchar(50)
----          N'TextBox' , -- vcAssociatedControlType - nvarchar(50)
----          'TotalInvoice' , -- vcPropertyName - varchar(100)
----          '' , -- PopupFunctionName - varchar(100)
----          0 , -- order - tinyint
----          1 , -- tintRow - tinyint
----          0 , -- tintColumn - tinyint
----          1 , -- bitInResults - bit
----          0 , -- bitDeleted - bit
----          1 , -- bitDefault - bit
----          NULL , -- bitSettingField - bit
----          NULL , -- bitAddField - bit
----          NULL , -- bitDetailField - bit
----          NULL , -- bitAllowSorting - bit
----          NULL , -- bitWorkFlowField - bit
----          NULL , -- bitImport - bit
----          NULL , -- bitExport - bit
----          NULL , -- bitAllowFiltering - bit
----          NULL , -- bitRequired - bit
----          NULL , -- numFormFieldID - numeric
----          NULL, -- intSectionID - int
----          NULL  -- bitAllowGridColor - bit
----        )

---------------------------------

------ Balance Due
----DECLARE @numMaxFieldId AS NUMERIC(18,0)
----SELECT @numMaxFieldId = MAX(numFieldID) + 1 FROM [dbo].[DycFieldMaster] AS DFM
----PRINT @numMaxFieldId

----SET IDENTITY_INSERT [dbo].[DycFieldMaster] ON

----INSERT INTO [dbo].[DycFieldMaster]
----        ( [numFieldId],
----		  [numModuleID] ,
----          [numDomainID] ,
----          [vcFieldName] ,
----          [vcDbColumnName] ,
----          [vcOrigDbColumnName] ,
----          [vcPropertyName] ,
----          [vcLookBackTableName] ,
----          [vcFieldDataType] ,
----          [vcFieldType] ,
----          [vcAssociatedControlType] ,
----          [vcToolTip] ,
----          [vcListItemType] ,
----          [numListID] ,
----          [PopupFunctionName] ,
----          [order] ,
----          [tintRow] ,
----          [tintColumn] ,
----          [bitInResults] ,
----          [bitDeleted] ,
----          [bitAllowEdit] ,
----          [bitDefault] ,
----          [bitSettingField] ,
----          [bitAddField] ,
----          [bitDetailField] ,
----          [bitAllowSorting] ,
----          [bitWorkFlowField] ,
----          [bitImport] ,
----          [bitExport] ,
----          [bitAllowFiltering] ,
----          [bitInlineEdit] ,
----          [bitRequired] ,
----          [intColumnWidth] ,
----          [intFieldMaxLength] ,
----          [intWFCompare] ,
----          [vcGroup] ,
----          [vcWFCompareField]
----        )
----VALUES  ( @numMaxFieldId , -- numFieldID - numeric
----		  4 , -- numModuleID - numeric
----          NULL , -- numDomainID - numeric
----          N'Balance Due' , -- vcFieldName - nvarchar(50)
----          N'BalanceDue' , -- vcDbColumnName - nvarchar(50)
----          N'BalanceDue' , -- vcOrigDbColumnName - nvarchar(50)
----          'BalanceDueGrid' , -- vcPropertyName - varchar(100)
----          N'CSGrid' , -- vcLookBackTableName - nvarchar(50)
----          'N' , -- vcFieldDataType - char(1)
----          'R' , -- vcFieldType - char(1)
----          N'TextBox' , -- vcAssociatedControlType - nvarchar(50)
----          N'Balance Due' , -- vcToolTip - nvarchar(1000)
----          '' , -- vcListItemType - varchar(3)
----          0 , -- numListID - numeric
----          NULL , -- PopupFunctionName - varchar(100)
----          63 , -- order - tinyint
----          NULL , -- tintRow - tinyint
----          NULL , -- tintColumn - tinyint
----          1 , -- bitInResults - bit
----          0 , -- bitDeleted - bit
----          1 , -- bitAllowEdit - bit
----          1 , -- bitDefault - bit
----          1 , -- bitSettingField - bit
----          NULL , -- bitAddField - bit
----          NULL , -- bitDetailField - bit
----          1 , -- bitAllowSorting - bit
----          NULL , -- bitWorkFlowField - bit
----          NULL , -- bitImport - bit
----          NULL , -- bitExport - bit
----          1 , -- bitAllowFiltering - bit
----          NULL , -- bitInlineEdit - bit
----          NULL , -- bitRequired - bit
----          NULL , -- intColumnWidth - int
----          NULL , -- intFieldMaxLength - int
----          NULL , -- intWFCompare - int
----          NULL , -- vcGroup - varchar(100)
----          NULL  -- vcWFCompareField - varchar(150)
----        )

----SET IDENTITY_INSERT [dbo].[DycFieldMaster] OFF

----INSERT INTO [dbo].[DycFormField_Mapping]
----        ( [numModuleID] ,
----          [numFieldID] ,
----          [numDomainID] ,
----          [numFormID] ,
----          [bitAllowEdit] ,
----          [bitInlineEdit] ,
----          [vcFieldName] ,
----          [vcAssociatedControlType] ,
----          [vcPropertyName] ,
----          [PopupFunctionName] ,
----          [order] ,
----          [tintRow] ,
----          [tintColumn] ,
----          [bitInResults] ,
----          [bitDeleted] ,
----          [bitDefault] ,
----          [bitSettingField] ,
----          [bitAddField] ,
----          [bitDetailField] ,
----          [bitAllowSorting] ,
----          [bitWorkFlowField] ,
----          [bitImport] ,
----          [bitExport] ,
----          [bitAllowFiltering] ,
----          [bitRequired] ,
----          [numFormFieldID] ,
----          [intSectionID] ,
----          [bitAllowGridColor]
----        )
----VALUES  ( 4 , -- numModuleID - numeric
----          @numMaxFieldId , -- numFieldID - numeric
----          NULL , -- numDomainID - numeric
----          7 , -- numFormID - numeric
----          1 , -- bitAllowEdit - bit
----          1 , -- bitInlineEdit - bit
----          N'Balance Due' , -- vcFieldName - nvarchar(50)
----          N'TextBox' , -- vcAssociatedControlType - nvarchar(50)
----          'BalanceDueGrid' , -- vcPropertyName - varchar(100)
----          '' , -- PopupFunctionName - varchar(100)
----          0 , -- order - tinyint
----          1 , -- tintRow - tinyint
----          0 , -- tintColumn - tinyint
----          1 , -- bitInResults - bit
----          0 , -- bitDeleted - bit
----          1 , -- bitDefault - bit
----          NULL , -- bitSettingField - bit
----          NULL , -- bitAddField - bit
----          NULL , -- bitDetailField - bit
----          NULL , -- bitAllowSorting - bit
----          NULL , -- bitWorkFlowField - bit
----          NULL , -- bitImport - bit
----          NULL , -- bitExport - bit
----          NULL , -- bitAllowFiltering - bit
----          NULL , -- bitRequired - bit
----          NULL , -- numFormFieldID - numeric
----          NULL, -- intSectionID - int
----          NULL  -- bitAllowGridColor - bit
----        )

---------------------------------

------ Interest
----DECLARE @numMaxFieldId AS NUMERIC(18,0)
----SELECT @numMaxFieldId = MAX(numFieldID) + 1 FROM [dbo].[DycFieldMaster] AS DFM
----PRINT @numMaxFieldId

----SET IDENTITY_INSERT [dbo].[DycFieldMaster] ON

----INSERT INTO [dbo].[DycFieldMaster]
----        ( [numFieldId],
----		  [numModuleID] ,
----          [numDomainID] ,
----          [vcFieldName] ,
----          [vcDbColumnName] ,
----          [vcOrigDbColumnName] ,
----          [vcPropertyName] ,
----          [vcLookBackTableName] ,
----          [vcFieldDataType] ,
----          [vcFieldType] ,
----          [vcAssociatedControlType] ,
----          [vcToolTip] ,
----          [vcListItemType] ,
----          [numListID] ,
----          [PopupFunctionName] ,
----          [order] ,
----          [tintRow] ,
----          [tintColumn] ,
----          [bitInResults] ,
----          [bitDeleted] ,
----          [bitAllowEdit] ,
----          [bitDefault] ,
----          [bitSettingField] ,
----          [bitAddField] ,
----          [bitDetailField] ,
----          [bitAllowSorting] ,
----          [bitWorkFlowField] ,
----          [bitImport] ,
----          [bitExport] ,
----          [bitAllowFiltering] ,
----          [bitInlineEdit] ,
----          [bitRequired] ,
----          [intColumnWidth] ,
----          [intFieldMaxLength] ,
----          [intWFCompare] ,
----          [vcGroup] ,
----          [vcWFCompareField]
----        )
----VALUES  ( @numMaxFieldId , -- numFieldID - numeric
----		  4 , -- numModuleID - numeric
----          NULL , -- numDomainID - numeric
----          N'Interest' , -- vcFieldName - nvarchar(50)
----          N'numInterest' , -- vcDbColumnName - nvarchar(50)
----          N'numInterest' , -- vcOrigDbColumnName - nvarchar(50)
----          'Interest' , -- vcPropertyName - varchar(100)
----          N'CSGrid' , -- vcLookBackTableName - nvarchar(50)
----          'N' , -- vcFieldDataType - char(1)
----          'R' , -- vcFieldType - char(1)
----          N'TextBox' , -- vcAssociatedControlType - nvarchar(50)
----          N'Interest' , -- vcToolTip - nvarchar(1000)
----          '' , -- vcListItemType - varchar(3)
----          0 , -- numListID - numeric
----          NULL , -- PopupFunctionName - varchar(100)
----          63 , -- order - tinyint
----          NULL , -- tintRow - tinyint
----          NULL , -- tintColumn - tinyint
----          1 , -- bitInResults - bit
----          0 , -- bitDeleted - bit
----          1 , -- bitAllowEdit - bit
----          1 , -- bitDefault - bit
----          1 , -- bitSettingField - bit
----          NULL , -- bitAddField - bit
----          NULL , -- bitDetailField - bit
----          1 , -- bitAllowSorting - bit
----          NULL , -- bitWorkFlowField - bit
----          NULL , -- bitImport - bit
----          NULL , -- bitExport - bit
----          1 , -- bitAllowFiltering - bit
----          NULL , -- bitInlineEdit - bit
----          NULL , -- bitRequired - bit
----          NULL , -- intColumnWidth - int
----          NULL , -- intFieldMaxLength - int
----          NULL , -- intWFCompare - int
----          NULL , -- vcGroup - varchar(100)
----          NULL  -- vcWFCompareField - varchar(150)
----        )

----SET IDENTITY_INSERT [dbo].[DycFieldMaster] OFF

----INSERT INTO [dbo].[DycFormField_Mapping]
----        ( [numModuleID] ,
----          [numFieldID] ,
----          [numDomainID] ,
----          [numFormID] ,
----          [bitAllowEdit] ,
----          [bitInlineEdit] ,
----          [vcFieldName] ,
----          [vcAssociatedControlType] ,
----          [vcPropertyName] ,
----          [PopupFunctionName] ,
----          [order] ,
----          [tintRow] ,
----          [tintColumn] ,
----          [bitInResults] ,
----          [bitDeleted] ,
----          [bitDefault] ,
----          [bitSettingField] ,
----          [bitAddField] ,
----          [bitDetailField] ,
----          [bitAllowSorting] ,
----          [bitWorkFlowField] ,
----          [bitImport] ,
----          [bitExport] ,
----          [bitAllowFiltering] ,
----          [bitRequired] ,
----          [numFormFieldID] ,
----          [intSectionID] ,
----          [bitAllowGridColor]
----        )
----VALUES  ( 4 , -- numModuleID - numeric
----          @numMaxFieldId , -- numFieldID - numeric
----          NULL , -- numDomainID - numeric
----          7 , -- numFormID - numeric
----          1 , -- bitAllowEdit - bit
----          1 , -- bitInlineEdit - bit
----          N'Interest' , -- vcFieldName - nvarchar(50)
----          N'TextBox' , -- vcAssociatedControlType - nvarchar(50)
----          'Interest' , -- vcPropertyName - varchar(100)
----          '' , -- PopupFunctionName - varchar(100)
----          0 , -- order - tinyint
----          1 , -- tintRow - tinyint
----          0 , -- tintColumn - tinyint
----          1 , -- bitInResults - bit
----          0 , -- bitDeleted - bit
----          1 , -- bitDefault - bit
----          NULL , -- bitSettingField - bit
----          NULL , -- bitAddField - bit
----          NULL , -- bitDetailField - bit
----          NULL , -- bitAllowSorting - bit
----          NULL , -- bitWorkFlowField - bit
----          NULL , -- bitImport - bit
----          NULL , -- bitExport - bit
----          NULL , -- bitAllowFiltering - bit
----          NULL , -- bitRequired - bit
----          NULL , -- numFormFieldID - numeric
----          NULL, -- intSectionID - int
----          NULL  -- bitAllowGridColor - bit
----        )

---------------------------------

------ Discount
----DECLARE @numMaxFieldId AS NUMERIC(18,0)
----SELECT @numMaxFieldId = MAX(numFieldID) + 1 FROM [dbo].[DycFieldMaster] AS DFM
----PRINT @numMaxFieldId

----SET IDENTITY_INSERT [dbo].[DycFieldMaster] ON

----INSERT INTO [dbo].[DycFieldMaster]
----        ( [numFieldId],
----		  [numModuleID] ,
----          [numDomainID] ,
----          [vcFieldName] ,
----          [vcDbColumnName] ,
----          [vcOrigDbColumnName] ,
----          [vcPropertyName] ,
----          [vcLookBackTableName] ,
----          [vcFieldDataType] ,
----          [vcFieldType] ,
----          [vcAssociatedControlType] ,
----          [vcToolTip] ,
----          [vcListItemType] ,
----          [numListID] ,
----          [PopupFunctionName] ,
----          [order] ,
----          [tintRow] ,
----          [tintColumn] ,
----          [bitInResults] ,
----          [bitDeleted] ,
----          [bitAllowEdit] ,
----          [bitDefault] ,
----          [bitSettingField] ,
----          [bitAddField] ,
----          [bitDetailField] ,
----          [bitAllowSorting] ,
----          [bitWorkFlowField] ,
----          [bitImport] ,
----          [bitExport] ,
----          [bitAllowFiltering] ,
----          [bitInlineEdit] ,
----          [bitRequired] ,
----          [intColumnWidth] ,
----          [intFieldMaxLength] ,
----          [intWFCompare] ,
----          [vcGroup] ,
----          [vcWFCompareField]
----        )
----VALUES  ( @numMaxFieldId , -- numFieldID - numeric
----		  4 , -- numModuleID - numeric
----          NULL , -- numDomainID - numeric
----          N'Discount' , -- vcFieldName - nvarchar(50)
----          N'numDiscount' , -- vcDbColumnName - nvarchar(50)
----          N'numDiscount' , -- vcOrigDbColumnName - nvarchar(50)
----          'Discount' , -- vcPropertyName - varchar(100)
----          N'CSGrid' , -- vcLookBackTableName - nvarchar(50)
----          'N' , -- vcFieldDataType - char(1)
----          'R' , -- vcFieldType - char(1)
----          N'TextBox' , -- vcAssociatedControlType - nvarchar(50)
----          N'Discount' , -- vcToolTip - nvarchar(1000)
----          '' , -- vcListItemType - varchar(3)
----          0 , -- numListID - numeric
----          NULL , -- PopupFunctionName - varchar(100)
----          63 , -- order - tinyint
----          NULL , -- tintRow - tinyint
----          NULL , -- tintColumn - tinyint
----          1 , -- bitInResults - bit
----          0 , -- bitDeleted - bit
----          1 , -- bitAllowEdit - bit
----          1 , -- bitDefault - bit
----          1 , -- bitSettingField - bit
----          NULL , -- bitAddField - bit
----          NULL , -- bitDetailField - bit
----          1 , -- bitAllowSorting - bit
----          NULL , -- bitWorkFlowField - bit
----          NULL , -- bitImport - bit
----          NULL , -- bitExport - bit
----          1 , -- bitAllowFiltering - bit
----          NULL , -- bitInlineEdit - bit
----          NULL , -- bitRequired - bit
----          NULL , -- intColumnWidth - int
----          NULL , -- intFieldMaxLength - int
----          NULL , -- intWFCompare - int
----          NULL , -- vcGroup - varchar(100)
----          NULL  -- vcWFCompareField - varchar(150)
----        )

----SET IDENTITY_INSERT [dbo].[DycFieldMaster] OFF

----INSERT INTO [dbo].[DycFormField_Mapping]
----        ( [numModuleID] ,
----          [numFieldID] ,
----          [numDomainID] ,
----          [numFormID] ,
----          [bitAllowEdit] ,
----          [bitInlineEdit] ,
----          [vcFieldName] ,
----          [vcAssociatedControlType] ,
----          [vcPropertyName] ,
----          [PopupFunctionName] ,
----          [order] ,
----          [tintRow] ,
----          [tintColumn] ,
----          [bitInResults] ,
----          [bitDeleted] ,
----          [bitDefault] ,
----          [bitSettingField] ,
----          [bitAddField] ,
----          [bitDetailField] ,
----          [bitAllowSorting] ,
----          [bitWorkFlowField] ,
----          [bitImport] ,
----          [bitExport] ,
----          [bitAllowFiltering] ,
----          [bitRequired] ,
----          [numFormFieldID] ,
----          [intSectionID] ,
----          [bitAllowGridColor]
----        )
----VALUES  ( 4 , -- numModuleID - numeric
----          @numMaxFieldId , -- numFieldID - numeric
----          NULL , -- numDomainID - numeric
----          7 , -- numFormID - numeric
----          1 , -- bitAllowEdit - bit
----          1 , -- bitInlineEdit - bit
----          N'Discount' , -- vcFieldName - nvarchar(50)
----          N'TextBox' , -- vcAssociatedControlType - nvarchar(50)
----          'Discount' , -- vcPropertyName - varchar(100)
----          '' , -- PopupFunctionName - varchar(100)
----          0 , -- order - tinyint
----          1 , -- tintRow - tinyint
----          0 , -- tintColumn - tinyint
----          1 , -- bitInResults - bit
----          0 , -- bitDeleted - bit
----          1 , -- bitDefault - bit
----          NULL , -- bitSettingField - bit
----          NULL , -- bitAddField - bit
----          NULL , -- bitDetailField - bit
----          NULL , -- bitAllowSorting - bit
----          NULL , -- bitWorkFlowField - bit
----          NULL , -- bitImport - bit
----          NULL , -- bitExport - bit
----          NULL , -- bitAllowFiltering - bit
----          NULL , -- bitRequired - bit
----          NULL , -- numFormFieldID - numeric
----          NULL, -- intSectionID - int
----          NULL  -- bitAllowGridColor - bit
----        )


---------------------------------

------ Status(Days)
----DECLARE @numMaxFieldId AS NUMERIC(18,0)
----SELECT @numMaxFieldId = MAX(numFieldID) + 1 FROM [dbo].[DycFieldMaster] AS DFM
----PRINT @numMaxFieldId

----SET IDENTITY_INSERT [dbo].[DycFieldMaster] ON

----INSERT INTO [dbo].[DycFieldMaster]
----        ( [numFieldId],
----		  [numModuleID] ,
----          [numDomainID] ,
----          [vcFieldName] ,
----          [vcDbColumnName] ,
----          [vcOrigDbColumnName] ,
----          [vcPropertyName] ,
----          [vcLookBackTableName] ,
----          [vcFieldDataType] ,
----          [vcFieldType] ,
----          [vcAssociatedControlType] ,
----          [vcToolTip] ,
----          [vcListItemType] ,
----          [numListID] ,
----          [PopupFunctionName] ,
----          [order] ,
----          [tintRow] ,
----          [tintColumn] ,
----          [bitInResults] ,
----          [bitDeleted] ,
----          [bitAllowEdit] ,
----          [bitDefault] ,
----          [bitSettingField] ,
----          [bitAddField] ,
----          [bitDetailField] ,
----          [bitAllowSorting] ,
----          [bitWorkFlowField] ,
----          [bitImport] ,
----          [bitExport] ,
----          [bitAllowFiltering] ,
----          [bitInlineEdit] ,
----          [bitRequired] ,
----          [intColumnWidth] ,
----          [intFieldMaxLength] ,
----          [intWFCompare] ,
----          [vcGroup] ,
----          [vcWFCompareField]
----        )
----VALUES  ( @numMaxFieldId , -- numFieldID - numeric
----		  4 , -- numModuleID - numeric
----          NULL , -- numDomainID - numeric
----          N'Status(Days)' , -- vcFieldName - nvarchar(50)
----          N'Status(DaysLate)' , -- vcDbColumnName - nvarchar(50)
----          N'Status(DaysLate)' , -- vcOrigDbColumnName - nvarchar(50)
----          'Status' , -- vcPropertyName - varchar(100)
----          N'CSGrid' , -- vcLookBackTableName - nvarchar(50)
----          'V' , -- vcFieldDataType - char(1)
----          'R' , -- vcFieldType - char(1)
----          N'TextBox' , -- vcAssociatedControlType - nvarchar(50)
----          N'Status(Days)' , -- vcToolTip - nvarchar(1000)
----          '' , -- vcListItemType - varchar(3)
----          0 , -- numListID - numeric
----          NULL , -- PopupFunctionName - varchar(100)
----          63 , -- order - tinyint
----          NULL , -- tintRow - tinyint
----          NULL , -- tintColumn - tinyint
----          1 , -- bitInResults - bit
----          0 , -- bitDeleted - bit
----          1 , -- bitAllowEdit - bit
----          1 , -- bitDefault - bit
----          1 , -- bitSettingField - bit
----          NULL , -- bitAddField - bit
----          NULL , -- bitDetailField - bit
----          1 , -- bitAllowSorting - bit
----          NULL , -- bitWorkFlowField - bit
----          NULL , -- bitImport - bit
----          NULL , -- bitExport - bit
----          1 , -- bitAllowFiltering - bit
----          NULL , -- bitInlineEdit - bit
----          NULL , -- bitRequired - bit
----          NULL , -- intColumnWidth - int
----          NULL , -- intFieldMaxLength - int
----          NULL , -- intWFCompare - int
----          NULL , -- vcGroup - varchar(100)
----          NULL  -- vcWFCompareField - varchar(150)
----        )

----SET IDENTITY_INSERT [dbo].[DycFieldMaster] OFF

----INSERT INTO [dbo].[DycFormField_Mapping]
----        ( [numModuleID] ,
----          [numFieldID] ,
----          [numDomainID] ,
----          [numFormID] ,
----          [bitAllowEdit] ,
----          [bitInlineEdit] ,
----          [vcFieldName] ,
----          [vcAssociatedControlType] ,
----          [vcPropertyName] ,
----          [PopupFunctionName] ,
----          [order] ,
----          [tintRow] ,
----          [tintColumn] ,
----          [bitInResults] ,
----          [bitDeleted] ,
----          [bitDefault] ,
----          [bitSettingField] ,
----          [bitAddField] ,
----          [bitDetailField] ,
----          [bitAllowSorting] ,
----          [bitWorkFlowField] ,
----          [bitImport] ,
----          [bitExport] ,
----          [bitAllowFiltering] ,
----          [bitRequired] ,
----          [numFormFieldID] ,
----          [intSectionID] ,
----          [bitAllowGridColor]
----        )
----VALUES  ( 4 , -- numModuleID - numeric
----          @numMaxFieldId , -- numFieldID - numeric
----          NULL , -- numDomainID - numeric
----          7 , -- numFormID - numeric
----          1 , -- bitAllowEdit - bit
----          1 , -- bitInlineEdit - bit
----          N'Status(Days)' , -- vcFieldName - nvarchar(50)
----          N'TextBox' , -- vcAssociatedControlType - nvarchar(50)
----          'Status' , -- vcPropertyName - varchar(100)
----          '' , -- PopupFunctionName - varchar(100)
----          0 , -- order - tinyint
----          1 , -- tintRow - tinyint
----          0 , -- tintColumn - tinyint
----          1 , -- bitInResults - bit
----          0 , -- bitDeleted - bit
----          1 , -- bitDefault - bit
----          NULL , -- bitSettingField - bit
----          NULL , -- bitAddField - bit
----          NULL , -- bitDetailField - bit
----          NULL , -- bitAllowSorting - bit
----          NULL , -- bitWorkFlowField - bit
----          NULL , -- bitImport - bit
----          NULL , -- bitExport - bit
----          NULL , -- bitAllowFiltering - bit
----          NULL , -- bitRequired - bit
----          NULL , -- numFormFieldID - numeric
----          NULL, -- intSectionID - int
----          NULL  -- bitAllowGridColor - bit
----        )


---------------------------------

------ Term Conditions
----DECLARE @numMaxFieldId AS NUMERIC(18,0)
----SELECT @numMaxFieldId = MAX(numFieldID) + 1 FROM [dbo].[DycFieldMaster] AS DFM
----PRINT @numMaxFieldId

----SET IDENTITY_INSERT [dbo].[DycFieldMaster] ON

----INSERT INTO [dbo].[DycFieldMaster]
----        ( [numFieldId],
----		  [numModuleID] ,
----          [numDomainID] ,
----          [vcFieldName] ,
----          [vcDbColumnName] ,
----          [vcOrigDbColumnName] ,
----          [vcPropertyName] ,
----          [vcLookBackTableName] ,
----          [vcFieldDataType] ,
----          [vcFieldType] ,
----          [vcAssociatedControlType] ,
----          [vcToolTip] ,
----          [vcListItemType] ,
----          [numListID] ,
----          [PopupFunctionName] ,
----          [order] ,
----          [tintRow] ,
----          [tintColumn] ,
----          [bitInResults] ,
----          [bitDeleted] ,
----          [bitAllowEdit] ,
----          [bitDefault] ,
----          [bitSettingField] ,
----          [bitAddField] ,
----          [bitDetailField] ,
----          [bitAllowSorting] ,
----          [bitWorkFlowField] ,
----          [bitImport] ,
----          [bitExport] ,
----          [bitAllowFiltering] ,
----          [bitInlineEdit] ,
----          [bitRequired] ,
----          [intColumnWidth] ,
----          [intFieldMaxLength] ,
----          [intWFCompare] ,
----          [vcGroup] ,
----          [vcWFCompareField]
----        )
----VALUES  ( @numMaxFieldId , -- numFieldID - numeric
----		  4 , -- numModuleID - numeric
----          NULL , -- numDomainID - numeric
----          N'Term Conditions' , -- vcFieldName - nvarchar(50)
----          N'vcTerms' , -- vcDbColumnName - nvarchar(50)
----          N'vcTerms' , -- vcOrigDbColumnName - nvarchar(50)
----          'Terms' , -- vcPropertyName - varchar(100)
----          N'' , -- vcLookBackTableName - nvarchar(50)
----          'V' , -- vcFieldDataType - char(1)
----          'R' , -- vcFieldType - char(1)
----          N'TextBox' , -- vcAssociatedControlType - nvarchar(50)
----          N'Term Conditions' , -- vcToolTip - nvarchar(1000)
----          '' , -- vcListItemType - varchar(3)
----          0 , -- numListID - numeric
----          NULL , -- PopupFunctionName - varchar(100)
----          63 , -- order - tinyint
----          NULL , -- tintRow - tinyint
----          NULL , -- tintColumn - tinyint
----          1 , -- bitInResults - bit
----          0 , -- bitDeleted - bit
----          1 , -- bitAllowEdit - bit
----          1 , -- bitDefault - bit
----          1 , -- bitSettingField - bit
----          NULL , -- bitAddField - bit
----          NULL , -- bitDetailField - bit
----          1 , -- bitAllowSorting - bit
----          NULL , -- bitWorkFlowField - bit
----          NULL , -- bitImport - bit
----          NULL , -- bitExport - bit
----          1 , -- bitAllowFiltering - bit
----          NULL , -- bitInlineEdit - bit
----          NULL , -- bitRequired - bit
----          NULL , -- intColumnWidth - int
----          NULL , -- intFieldMaxLength - int
----          NULL , -- intWFCompare - int
----          NULL , -- vcGroup - varchar(100)
----          NULL  -- vcWFCompareField - varchar(150)
----        )

----SET IDENTITY_INSERT [dbo].[DycFieldMaster] OFF

----INSERT INTO [dbo].[DycFormField_Mapping]
----        ( [numModuleID] ,
----          [numFieldID] ,
----          [numDomainID] ,
----          [numFormID] ,
----          [bitAllowEdit] ,
----          [bitInlineEdit] ,
----          [vcFieldName] ,
----          [vcAssociatedControlType] ,
----          [vcPropertyName] ,
----          [PopupFunctionName] ,
----          [order] ,
----          [tintRow] ,
----          [tintColumn] ,
----          [bitInResults] ,
----          [bitDeleted] ,
----          [bitDefault] ,
----          [bitSettingField] ,
----          [bitAddField] ,
----          [bitDetailField] ,
----          [bitAllowSorting] ,
----          [bitWorkFlowField] ,
----          [bitImport] ,
----          [bitExport] ,
----          [bitAllowFiltering] ,
----          [bitRequired] ,
----          [numFormFieldID] ,
----          [intSectionID] ,
----          [bitAllowGridColor]
----        )
----VALUES  ( 4 , -- numModuleID - numeric
----          @numMaxFieldId , -- numFieldID - numeric
----          NULL , -- numDomainID - numeric
----          7 , -- numFormID - numeric
----          1 , -- bitAllowEdit - bit
----          1 , -- bitInlineEdit - bit
----          N'Term Conditions' , -- vcFieldName - nvarchar(50)
----          N'TextBox' , -- vcAssociatedControlType - nvarchar(50)
----          'Terms' , -- vcPropertyName - varchar(100)
----          '' , -- PopupFunctionName - varchar(100)
----          0 , -- order - tinyint
----          1 , -- tintRow - tinyint
----          0 , -- tintColumn - tinyint
----          1 , -- bitInResults - bit
----          0 , -- bitDeleted - bit
----          1 , -- bitDefault - bit
----          NULL , -- bitSettingField - bit
----          NULL , -- bitAddField - bit
----          NULL , -- bitDetailField - bit
----          NULL , -- bitAllowSorting - bit
----          NULL , -- bitWorkFlowField - bit
----          NULL , -- bitImport - bit
----          NULL , -- bitExport - bit
----          NULL , -- bitAllowFiltering - bit
----          NULL , -- bitRequired - bit
----          NULL , -- numFormFieldID - numeric
----          NULL, -- intSectionID - int
----          NULL  -- bitAllowGridColor - bit
----        )

-------------------- Add Default Customer Statement Template for all domain ---------------------------------------

----BEGIN TRANSACTION
----DECLARE @numDomainID AS NUMERIC(18)
----SELECT  ROW_NUMBER() OVER ( ORDER BY numDomainID ) AS [RowID] ,
----        numDomainID
----INTO    #tempDomains
----FROM    dbo.Domain
----WHERE   (numDomainId > 0 AND [Domain].[numDomainId] <> 1)
----DECLARE @intCnt AS INT
----DECLARE @intCntDomain AS INT
----SET @intCnt = 0
----SET @intCntDomain = ( SELECT    COUNT(*)
----                      FROM      #tempDomains
----                    )

----IF @intCntDomain > 0
----    BEGIN
----        WHILE ( @intCnt < @intCntDomain )
----            BEGIN

----                SET @intCnt = @intCnt + 1
----                SELECT  @numDomainID = numDomainID
----                FROM    #tempDomains
----                WHERE   RowID = @intCnt
----                PRINT @numDomainID

----				--SELECT * FROM #tempDomains
----                INSERT  INTO [dbo].[BizDocTemplate]
----                        ( [numDomainID] ,
----                          [numBizDocID] ,
----                          [numOppType] ,
----                          [txtBizDocTemplate] ,
----                          [txtCSS] ,
----                          [bitEnabled] ,
----                          [tintTemplateType] ,
----                          [vcTemplateName] ,
----                          [bitDefault] ,
----                          [txtNewBizDocTemplate] ,
----                          [vcBizDocImagePath] ,
----                          [vcBizDocFooter] ,
----                          [vcPurBizDocFooter] ,
----                          [bitKeepFooterBottom] ,
----                          [numOrientation]
----                        )
----                VALUES  ( @numDomainID , -- numDomainID - numeric
----                          40911 , -- numBizDocID - numeric
----                          1 , -- numOppType - numeric
----                          '&lt;table width=&quot;100%&quot; border=&quot;0&quot; cellpadding=&quot;0&quot;&gt;
----    &lt;tbody&gt;
----        &lt;tr&gt;
----            &lt;td valign=&quot;top&quot; align=&quot;left&quot;&gt;#Logo#&lt;br /&gt;
----            &lt;/td&gt;
----            &lt;td align=&quot;right&quot; style=&quot;white-space: nowrap;&quot;&gt;&lt;/td&gt;
----            &lt;td align=&quot;right&quot; class=&quot;title&quot; style=&quot;white-space: nowrap;&quot;&gt;#BizDocTemplateName#
----            &lt;/td&gt;
----        &lt;/tr&gt;
----    &lt;/tbody&gt;
----&lt;/table&gt;
----&lt;br /&gt;
----&lt;table width=&quot;100%&quot; border=&quot;0&quot; cellpadding=&quot;0&quot;&gt;
----    &lt;tbody&gt;
----        &lt;tr&gt;
----            &lt;td align=&quot;right&quot; style=&quot;text-align: left;&quot;&gt;&lt;strong&gt;Bill To:&lt;/strong&gt;&lt;br /&gt;
----            &lt;/td&gt;
----            &lt;td align=&quot;right&quot; style=&quot;text-align: center;&quot;&gt;&lt;strong&gt;Statement Date:&lt;/strong&gt;&amp;nbsp;#StatementDate#&lt;br /&gt;
----            &lt;/td&gt;
----        &lt;/tr&gt;
----        &lt;tr&gt;
----            &lt;td align=&quot;right&quot; style=&quot;text-align: left;&quot;&gt;#Customer/VendorOrganizationName#&lt;br /&gt;
----            #Customer/VendorBillToStreet#&lt;br /&gt;
----            #Customer/VendorBillToCity#&lt;br /&gt;
----            #Customer/VendorBillToState#&amp;nbsp;#Customer/VendorBillToPostal#&lt;br /&gt;
----            #Customer/VendorBillToCountry#&lt;br /&gt;
----            &lt;br /&gt;
----            &lt;/td&gt;
----            &lt;td align=&quot;right&quot; style=&quot;text-align: center;&quot;&gt;&lt;span style=&quot;font-size: 24px;&quot;&gt;&lt;strong&gt;Balance Due:&lt;/strong&gt;&amp;nbsp;&amp;nbsp;#BalanceDue#&lt;/span&gt;&lt;/td&gt;
----        &lt;/tr&gt;
----    &lt;/tbody&gt;
----&lt;/table&gt;
----&lt;table width=&quot;75%&quot; border=&quot;0&quot; cellpadding=&quot;0&quot;&gt;
----    &lt;tbody&gt;
----        &lt;tr&gt;
----            &lt;td colspan=&quot;2&quot; style=&quot;text-align: center;&quot;&gt;&lt;strong&gt;&lt;span style=&quot;color: #00b050;&quot;&gt;Current Amt Due&lt;/span&gt;&lt;/strong&gt;&lt;/td&gt;
----            &lt;td colspan=&quot;2&quot; style=&quot;text-align: center;&quot;&gt;&lt;strong&gt;&lt;span style=&quot;color: #ff0000;&quot;&gt;Past Due&lt;/span&gt;&lt;/strong&gt;&lt;/td&gt;
----        &lt;/tr&gt;
----        &lt;tr&gt;
----            &lt;td style=&quot;text-align: right;&quot;&gt;Today-30:&lt;/td&gt;
----            &lt;td&gt;#Today-30(CurrentAmtDue)#&lt;br /&gt;
----            &lt;/td&gt;
----            &lt;td style=&quot;text-align: right;&quot;&gt;Today-30:&lt;/td&gt;
----            &lt;td&gt;#Today-30(PastDue)#&lt;/td&gt;
----        &lt;/tr&gt;
----        &lt;tr&gt;
----            &lt;td style=&quot;text-align: right;&quot;&gt;31-60:&lt;/td&gt;
----            &lt;td&gt;#31-60(CurrentAmtDue)#&lt;br /&gt;
----            &lt;/td&gt;
----            &lt;td style=&quot;text-align: right;&quot;&gt;31-60:&lt;/td&gt;
----            &lt;td&gt;#61-90(PastDue)#&lt;br /&gt;
----            &lt;/td&gt;
----        &lt;/tr&gt;
----        &lt;tr&gt;
----            &lt;td style=&quot;text-align: right;&quot;&gt;61-90:&lt;/td&gt;
----            &lt;td&gt;#61-90(CurrentAmtDue)#&lt;br /&gt;
----            &lt;/td&gt;
----            &lt;td style=&quot;text-align: right;&quot;&gt;61-90:&lt;/td&gt;
----            &lt;td&gt;#61-90(PastDue)#&lt;br /&gt;
----            &lt;/td&gt;
----        &lt;/tr&gt;
----        &lt;tr&gt;
----            &lt;td style=&quot;text-align: right;&quot;&gt;Over 91:&lt;/td&gt;
----            &lt;td&gt;#Over91(CurrentAmtDue)#&lt;br /&gt;
----            &lt;/td&gt;
----            &lt;td style=&quot;text-align: right;&quot;&gt;Over 91:&lt;/td&gt;
----            &lt;td&gt;#Over91(PastDue)#&lt;br /&gt;
----            &lt;/td&gt;
----        &lt;/tr&gt;
----    &lt;/tbody&gt;
----&lt;/table&gt;
----&lt;br /&gt;
----&lt;table width=&quot;100%&quot; border=&quot;0&quot; cellpadding=&quot;0&quot;&gt;
----    &lt;tbody&gt;
----        &lt;tr&gt;
----            &lt;td style=&quot;text-align: center;&quot;&gt;#Products#
----            &lt;/td&gt;
----        &lt;/tr&gt;
----    &lt;/tbody&gt;
----&lt;/table&gt;' , -- txtBizDocTemplate - text
----                          '.title{color: #696969; /*padding:10px;*/font: bold 30px Arial, Helvetica, sans-serif;}
----			.RowHeader{background-color: #dedede;color: #333;font-family: Arial;font-size: 8pt;}
----			.RowHeader.hyperlink{color: #333;}
----			.ItemHeader, .ItemStyle, .AltItemStyle{border-width: 1px;padding: 8px;font: normal 12px/17px arial;border-style: solid;border-color: #666666;background-color: #dedede;}
----			.ItemHeader{border-width: 1px;padding: 8px;border-style: solid;border-color: #666666;}
----			.ItemStyle td{border-width: 1px;padding: 8px;border-style: solid;border-color: #666666;background-color: #ffffff;}
----			.AltItemStyle{background-color: White;border-color: black;padding: 8px;}
----			.AltItemStyle td{padding: 8px;}
----			.ItemHeader td{border-width: 1px;padding: 8px;font-weight: bold;border-style: solid;border-color: #666666;background-color: #dedede;}
----			.ItemHeader td th{border-width: 1px;padding: 8px;border-style: solid;border-color: #666666;background-color: #dedede;}
----			#tblBizDocSumm{font: normal 12px verdana;color: #333;border-width: 1px;border-color: #666666;border-collapse: collapse;background-color: #dedede;}
----			#tblBizDocSumm td{border-width: 1px;padding: 8px;border-style: solid;border-color: #666666;}
----			.WordWrapSerialNo{width: 30%;word-break: break-all;}' , -- txtCSS - text
----                          1 , -- bitEnabled - bit
----                          0 , -- tintTemplateType - tinyint
----                          'Customer Statement' , -- vcTemplateName - varchar(50)
----                          1 , -- bitDefault - bit
----                          '&lt;table width=&quot;100%&quot; border=&quot;0&quot; cellpadding=&quot;0&quot;&gt;
----    &lt;tbody&gt;
----        &lt;tr&gt;
----            &lt;td valign=&quot;top&quot; align=&quot;left&quot;&gt;#Logo#&lt;br /&gt;
----            &lt;/td&gt;
----            &lt;td align=&quot;right&quot; style=&quot;white-space: nowrap;&quot;&gt;&lt;/td&gt;
----            &lt;td align=&quot;right&quot; class=&quot;title&quot; style=&quot;white-space: nowrap;&quot;&gt;#BizDocTemplateName#
----            &lt;/td&gt;
----        &lt;/tr&gt;
----    &lt;/tbody&gt;
----&lt;/table&gt;
----&lt;br /&gt;
----&lt;table width=&quot;100%&quot; border=&quot;0&quot; cellpadding=&quot;0&quot;&gt;
----    &lt;tbody&gt;
----        &lt;tr&gt;
----            &lt;td align=&quot;right&quot; style=&quot;text-align: left;&quot;&gt;&lt;strong&gt;Bill To:&lt;/strong&gt;&lt;br /&gt;
----            &lt;/td&gt;
----            &lt;td align=&quot;right&quot; style=&quot;text-align: center;&quot;&gt;&lt;strong&gt;Statement Date:&lt;/strong&gt;&amp;nbsp;#StatementDate#&lt;br /&gt;
----            &lt;/td&gt;
----        &lt;/tr&gt;
----        &lt;tr&gt;
----            &lt;td align=&quot;right&quot; style=&quot;text-align: left;&quot;&gt;#Customer/VendorOrganizationName#&lt;br /&gt;
----            #Customer/VendorBillToStreet#&lt;br /&gt;
----            #Customer/VendorBillToCity#&lt;br /&gt;
----            #Customer/VendorBillToState#&amp;nbsp;#Customer/VendorBillToPostal#&lt;br /&gt;
----            #Customer/VendorBillToCountry#&lt;br /&gt;
----            &lt;br /&gt;
----            &lt;/td&gt;
----            &lt;td align=&quot;right&quot; style=&quot;text-align: center;&quot;&gt;&lt;span style=&quot;font-size: 24px;&quot;&gt;&lt;strong&gt;Balance Due:&lt;/strong&gt;&amp;nbsp;&amp;nbsp;#BalanceDue#&lt;/span&gt;&lt;/td&gt;
----        &lt;/tr&gt;
----    &lt;/tbody&gt;
----&lt;/table&gt;
----&lt;table width=&quot;75%&quot; border=&quot;0&quot; cellpadding=&quot;0&quot;&gt;
----    &lt;tbody&gt;
----        &lt;tr&gt;
----            &lt;td colspan=&quot;2&quot; style=&quot;text-align: center;&quot;&gt;&lt;strong&gt;&lt;span style=&quot;color: #00b050;&quot;&gt;Current Amt Due&lt;/span&gt;&lt;/strong&gt;&lt;/td&gt;
----            &lt;td colspan=&quot;2&quot; style=&quot;text-align: center;&quot;&gt;&lt;strong&gt;&lt;span style=&quot;color: #ff0000;&quot;&gt;Past Due&lt;/span&gt;&lt;/strong&gt;&lt;/td&gt;
----        &lt;/tr&gt;
----        &lt;tr&gt;
----            &lt;td style=&quot;text-align: right;&quot;&gt;Today-30:&lt;/td&gt;
----            &lt;td&gt;#Today-30(CurrentAmtDue)#&lt;br /&gt;
----            &lt;/td&gt;
----            &lt;td style=&quot;text-align: right;&quot;&gt;Today-30:&lt;/td&gt;
----            &lt;td&gt;#Today-30(PastDue)#&lt;/td&gt;
----        &lt;/tr&gt;
----        &lt;tr&gt;
----            &lt;td style=&quot;text-align: right;&quot;&gt;31-60:&lt;/td&gt;
----            &lt;td&gt;#31-60(CurrentAmtDue)#&lt;br /&gt;
----            &lt;/td&gt;
----            &lt;td style=&quot;text-align: right;&quot;&gt;31-60:&lt;/td&gt;
----            &lt;td&gt;#61-90(PastDue)#&lt;br /&gt;
----            &lt;/td&gt;
----        &lt;/tr&gt;
----        &lt;tr&gt;
----            &lt;td style=&quot;text-align: right;&quot;&gt;61-90:&lt;/td&gt;
----            &lt;td&gt;#61-90(CurrentAmtDue)#&lt;br /&gt;
----            &lt;/td&gt;
----            &lt;td style=&quot;text-align: right;&quot;&gt;61-90:&lt;/td&gt;
----            &lt;td&gt;#61-90(PastDue)#&lt;br /&gt;
----            &lt;/td&gt;
----        &lt;/tr&gt;
----        &lt;tr&gt;
----            &lt;td style=&quot;text-align: right;&quot;&gt;Over 91:&lt;/td&gt;
----            &lt;td&gt;#Over91(CurrentAmtDue)#&lt;br /&gt;
----            &lt;/td&gt;
----            &lt;td style=&quot;text-align: right;&quot;&gt;Over 91:&lt;/td&gt;
----            &lt;td&gt;#Over91(PastDue)#&lt;br /&gt;
----            &lt;/td&gt;
----        &lt;/tr&gt;
----    &lt;/tbody&gt;
----&lt;/table&gt;
----&lt;br /&gt;
----&lt;table width=&quot;100%&quot; border=&quot;0&quot; cellpadding=&quot;0&quot;&gt;
----    &lt;tbody&gt;
----        &lt;tr&gt;
----            &lt;td style=&quot;text-align: center;&quot;&gt;#Products#
----            &lt;/td&gt;
----        &lt;/tr&gt;
----    &lt;/tbody&gt;
----&lt;/table&gt;' , -- txtNewBizDocTemplate - text
----                          NULL , -- vcBizDocImagePath - varchar(100)
----                          NULL , -- vcBizDocFooter - varchar(100)
----                          NULL , -- vcPurBizDocFooter - varchar(100)
----                          0 , -- bitKeepFooterBottom - bit
----                          1  -- numOrientation - int
----                        )

----            END
----    END
----DROP TABLE #tempDomains

----ROLLBACK

-------------- Added 2 fields for accounting audit facility ---------------------------

----ALTER TABLE Subscribers ADD bitEnabledAccountingAudit BIT, bitEnabledNotifyAdmin BIT
----UPDATE [dbo].[Subscribers] SET bitEnabledAccountingAudit = 1, bitEnabledNotifyAdmin = 1

----ROLLBACK

----------/******************************************************************
----------Project: BACRMUI   Date: 16.FEB.2015
----------Comments: Adding new Bizdoc "Customer Statement" / Adding New fields to Billing Terms

----------*******************************************************************/
----BEGIN TRANSACTION

----ALTER TABLE [dbo].[BillingTerms] 
----ADD numInterestPercentage NUMERIC(10,2), 
----	numInterestPercentageIfPaidAfterDays NUMERIC(10,2),  
----	numCreatedBy NUMERIC(18,0), 
----	dtCreatedDate DATETIME, 
----	numModifiedBy NUMERIC(18,0), 
----	dtModifiedDate DATETIME

----ROLLBACK


----<add key="ReportPageSize" value="100"></add>


/* Updated on last prod on 3rd Feb,2015 
------/******************************************************************
------Project: BACRMUI   Date: 31.JAN.2015
------Comments: Added new control for onepagecheckout
------*******************************************************************/

BEGIN TRANSACTION

SELECT * FROM [PageElementAttributes] WHERE [PageElementAttributes].[numElementID] = 35
UPDATE [PageElementAttributes] SET [bitEditor] = 1 WHERE [PageElementAttributes].[numElementID] = 35

SELECT * FROM dbo.PageElementMaster 
SELECT vcMergeField,vcMergeFieldValue,numModuleID,tintModuleType FROM EmailMergeFields WHERE [numModuleID] = 44

DECLARE @MaxElementID AS NUMERIC(18,0)
SELECT @MaxElementID = MAX([numElementID]) + 1 FROM [PageElementMaster] 
PRINT @MaxElementID

INSERT INTO dbo.PageElementMaster 
(numElementID,vcElementName,vcUserControlPath,vcTagName,bitCustomization,bitAdd,bitDelete)
SELECT @MaxElementID,'NewOnePageCheckout','~/UserControls/NewOnePageCheckout.ascx','{#NewOnePageCheckout#}',1,NULL,NULL
--------------------------------
INSERT INTO dbo.EmailMergeModule (numModuleID,vcModuleName,tintModuleType) 
SELECT @MaxElementID,'NewOnePageCheckout',1
--------------------------------
--SELECT * FROM dbo.EmailMergeFields
INSERT INTO dbo.EmailMergeFields (vcMergeField,vcMergeFieldValue,numModuleID,tintModuleType)
SELECT vcMergeField,vcMergeFieldValue,@MaxElementID,tintModuleType FROM EmailMergeFields WHERE [numModuleID] = 10
UNION ALL 
SELECT 'Sub Total','##SubTotalLabel##',@MaxElementID,1
UNION ALL 
SELECT 'Discount','##DiscountLabel##',@MaxElementID,1
UNION ALL 
SELECT 'Tax','##TaxLabel##',@MaxElementID,1
UNION ALL 
SELECT 'Show Shipping Amount With Method','##ShowShippingAmountWithMethod##',@MaxElementID,1
UNION ALL 
SELECT 'Shipping Method','##ShippingMethodDropdown##',@MaxElementID,1
UNION ALL 
SELECT 'Shipping Charge','##ShippingChargeLabel##',@MaxElementID,1
UNION ALL 
SELECT 'Total Amount','##TotalAmountLabel##',@MaxElementID,1
UNION ALL 
SELECT 'Total Amount','##TotalAmount##',@MaxElementID,1
UNION ALL 
SELECT 'Comments','##Comments##',@MaxElementID,1
UNION ALL 
SELECT 'View Past Orders','##ViewPastOrdersButton##',@MaxElementID,1
UNION ALL
SELECT 'Shipping FirstName','##ShippingFirstName##',@MaxElementID,1
UNION ALL
SELECT 'Shipping LastName','##ShippingLastName##',@MaxElementID,1
UNION ALL
SELECT 'CurrencySymbol','##CurrencySymbol##',@MaxElementID,1


--------------------------------
INSERT INTO dbo.PageElementDetail (numElementID,numAttributeID,vcAttributeValue,numSiteID,numDomainID,vcHtml)
SELECT @MaxElementID,96,'',82,170,''
--------------------------------
INSERT INTO dbo.PageElementAttributes (numElementID,vcAttributeName,vcControlType,vcControlValues,bitEditor) 
SELECT @MaxElementID,'Html Customize','HtmlEditor','',1
--------------------------------

SELECT * FROM [dbo].[EmailMergeFields] AS EMF WHERE [EMF].[numModuleID] = 44
UPDATE [dbo].[EmailMergeFields] SET [vcMergeField] = 'Billing FirstName', [vcMergeFieldValue] = '##BillingFirstName##' WHERE [numModuleID] = 44 AND [vcMergeField] = 'Customer FirstName'
UPDATE [dbo].[EmailMergeFields] SET [vcMergeField] = 'Billing LastName', [vcMergeFieldValue] = '##BillingLastName##' WHERE [numModuleID] = 44 AND [vcMergeField] = 'Customer LastName'

ROLLBACK


------/******************************************************************
------Project: BACRMUI   Date: 21.JAN.2015
------Comments: Make ItemID available to Custom Report module : Opportunity Bizdoc Items
------*******************************************************************/
BEGIN TRANSACTION

INSERT INTO [dbo].[ReportModuleGroupFieldMappingMaster]
        ( [numReportModuleGroupID] ,
          [numReportFieldGroupID]
        )
VALUES  ( 9 , -- numReportModuleGroupID - numeric
          7  -- numReportFieldGroupID - numeric
        )
ROLLBACK


------/******************************************************************
------Project: BizService   Date: 20.JAN.2015
------Comments: Add New Field to Organization Import
------*******************************************************************/


BEGIN TRANSACTION

DECLARE @numMaxFieldId AS NUMERIC(18,0)
SELECT @numMaxFieldId = MAX(numFieldID) + 1 FROM [dbo].[DycFieldMaster] AS DFM
PRINT @numMaxFieldId

SET IDENTITY_INSERT [dbo].[DycFieldMaster] ON

INSERT INTO [dbo].[DycFieldMaster]
        ( [numFieldId],
		  [numModuleID] ,
          [numDomainID] ,
          [vcFieldName] ,
          [vcDbColumnName] ,
          [vcOrigDbColumnName] ,
          [vcPropertyName] ,
          [vcLookBackTableName] ,
          [vcFieldDataType] ,
          [vcFieldType] ,
          [vcAssociatedControlType] ,
          [vcToolTip] ,
          [vcListItemType] ,
          [numListID] ,
          [PopupFunctionName] ,
          [order] ,
          [tintRow] ,
          [tintColumn] ,
          [bitInResults] ,
          [bitDeleted] ,
          [bitAllowEdit] ,
          [bitDefault] ,
          [bitSettingField] ,
          [bitAddField] ,
          [bitDetailField] ,
          [bitAllowSorting] ,
          [bitWorkFlowField] ,
          [bitImport] ,
          [bitExport] ,
          [bitAllowFiltering] ,
          [bitInlineEdit] ,
          [bitRequired] ,
          [intColumnWidth] ,
          [intFieldMaxLength] ,
          [intWFCompare] ,
          [vcGroup] ,
          [vcWFCompareField]
        )
VALUES  ( @numMaxFieldId , -- numFieldID - numeric
		  4 , -- numModuleID - numeric
          NULL , -- numDomainID - numeric
          N'Default Expense Account' , -- vcFieldName - nvarchar(50)
          N'numDefaultExpenseAccountID' , -- vcDbColumnName - nvarchar(50)
          N'numDefaultExpenseAccountID' , -- vcOrigDbColumnName - nvarchar(50)
          'DefaultExpenseAccountID' , -- vcPropertyName - varchar(100)
          N'DivisionMaster' , -- vcLookBackTableName - nvarchar(50)
          'N' , -- vcFieldDataType - char(1)
          'R' , -- vcFieldType - char(1)
          N'SelectBox' , -- vcAssociatedControlType - nvarchar(50)
          N'Default Expense Account' , -- vcToolTip - nvarchar(1000)
          '' , -- vcListItemType - varchar(3)
          0 , -- numListID - numeric
          NULL , -- PopupFunctionName - varchar(100)
          63 , -- order - tinyint
          0 , -- tintRow - tinyint
          1 , -- tintColumn - tinyint
          1 , -- bitInResults - bit
          0 , -- bitDeleted - bit
          1 , -- bitAllowEdit - bit
          0 , -- bitDefault - bit
          1 , -- bitSettingField - bit
          1 , -- bitAddField - bit
          1 , -- bitDetailField - bit
          1 , -- bitAllowSorting - bit
          1 , -- bitWorkFlowField - bit
          1 , -- bitImport - bit
          NULL , -- bitExport - bit
          1 , -- bitAllowFiltering - bit
          NULL , -- bitInlineEdit - bit
          NULL , -- bitRequired - bit
          NULL , -- intColumnWidth - int
          NULL , -- intFieldMaxLength - int
          NULL , -- intWFCompare - int
          'Org.Details Fields' , -- vcGroup - varchar(100)
          NULL  -- vcWFCompareField - varchar(150)
        )

SET IDENTITY_INSERT [dbo].[DycFieldMaster] OFF
INSERT INTO [dbo].[DycFormField_Mapping]
        ( [numModuleID] ,
          [numFieldID] ,
          [numDomainID] ,
          [numFormID] ,
          [bitAllowEdit] ,
          [bitInlineEdit] ,
          [vcFieldName] ,
          [vcAssociatedControlType] ,
          [vcPropertyName] ,
          [PopupFunctionName] ,
          [order] ,
          [tintRow] ,
          [tintColumn] ,
          [bitInResults] ,
          [bitDeleted] ,
          [bitDefault] ,
          [bitSettingField] ,
          [bitAddField] ,
          [bitDetailField] ,
          [bitAllowSorting] ,
          [bitWorkFlowField] ,
          [bitImport] ,
          [bitExport] ,
          [bitAllowFiltering] ,
          [bitRequired] ,
          [numFormFieldID] ,
          [intSectionID] ,
          [bitAllowGridColor]
        )
VALUES  ( 4 , -- numModuleID - numeric
          @numMaxFieldId , -- numFieldID - numeric
          NULL , -- numDomainID - numeric
          36 , -- numFormID - numeric
          0 , -- bitAllowEdit - bit
          0 , -- bitInlineEdit - bit
          N'Default Expense Account' , -- vcFieldName - nvarchar(50)
          N'SelectBox' , -- vcAssociatedControlType - nvarchar(50)
          'DefaultExpenseAccountID' , -- vcPropertyName - varchar(100)
          NULL , -- PopupFunctionName - varchar(100)
          63 , -- order - tinyint
          NULL , -- tintRow - tinyint
          NULL , -- tintColumn - tinyint
          NULL , -- bitInResults - bit
          NULL , -- bitDeleted - bit
          NULL , -- bitDefault - bit
          NULL , -- bitSettingField - bit
          NULL , -- bitAddField - bit
          NULL , -- bitDetailField - bit
          NULL , -- bitAllowSorting - bit
          NULL , -- bitWorkFlowField - bit
          1 , -- bitImport - bit
          NULL , -- bitExport - bit
          NULL , -- bitAllowFiltering - bit
          NULL , -- bitRequired - bit
          NULL , -- numFormFieldID - numeric
          1 , -- intSectionID - int
          NULL  -- bitAllowGridColor - bit
        )

SELECT * FROM [dbo].[DycFormField_Mapping] AS DFFM WHERE [DFFM].[numFormID] = 36
ROLLBACK

*/

/* ------------ UPDATED ON PRODUCTION ALREADY ON 2nd Jan,2015--------------------------------

------/******************************************************************
------Project: BACRMUI   Date: 23.DEC.2014
------Comments: Add New Fields to Custom Report : RMA
------*******************************************************************/

BEGIN TRANSACTION

INSERT INTO [dbo].[DycFormField_Mapping]
        ( [numModuleID] ,
          [numFieldID] ,
          [numDomainID] ,
          [numFormID] ,
          [bitAllowEdit] ,
          [bitInlineEdit] ,
          [vcFieldName] ,
          [vcAssociatedControlType] ,
          [vcPropertyName] ,
          [PopupFunctionName] ,
          [order] ,
          [tintRow] ,
          [tintColumn] ,
          [bitInResults] ,
          [bitDeleted] ,
          [bitDefault] ,
          [bitSettingField] ,
          [bitAddField] ,
          [bitDetailField] ,
          [bitAllowSorting] ,
          [bitWorkFlowField] ,
          [bitImport] ,
          [bitExport] ,
          [bitAllowFiltering] ,
          [bitRequired] ,
          [numFormFieldID] ,
          [intSectionID] ,
          [bitAllowGridColor]
        )
VALUES  ( 4 , -- numModuleID - numeric
          484 , -- numFieldID - numeric
          NULL , -- numDomainID - numeric
          20 , -- numFormID - numeric
          0 , -- bitAllowEdit - bit
          0 , -- bitInlineEdit - bit
          N'Allow DropShip' , -- vcFieldName - nvarchar(50)
          N'CheckBox' , -- vcAssociatedControlType - nvarchar(50)
          'AllowDropShip' , -- vcPropertyName - varchar(100)
          NULL , -- PopupFunctionName - varchar(100)
          64 , -- order - tinyint
          NULL , -- tintRow - tinyint
          NULL , -- tintColumn - tinyint
          NULL , -- bitInResults - bit
          NULL , -- bitDeleted - bit
          NULL , -- bitDefault - bit
          NULL , -- bitSettingField - bit
          NULL , -- bitAddField - bit
          NULL , -- bitDetailField - bit
          NULL , -- bitAllowSorting - bit
          NULL , -- bitWorkFlowField - bit
          1 , -- bitImport - bit
          NULL , -- bitExport - bit
          NULL , -- bitAllowFiltering - bit
          NULL , -- bitRequired - bit
          NULL , -- numFormFieldID - numeric
          1 , -- intSectionID - int
          NULL  -- bitAllowGridColor - bit
        )

SELECT * FROM [dbo].[DycFormField_Mapping] AS DFFM WHERE [DFFM].[numFormID] = 20


ALTER TABLE [dbo].[Category] ALTER COLUMN [vcDescription] VARCHAR(MAX)

DECLARE @numMaxReportModuleID NUMERIC(18,0)
SELECT @numMaxReportModuleID = MAX([numReportModuleID]) + 1 FROM [dbo].[ReportModuleMaster]
PRINT @numMaxReportModuleID

SET IDENTITY_INSERT [dbo].[ReportModuleMaster] ON
INSERT INTO [dbo].[ReportModuleMaster] ([numReportModuleID] ,[vcModuleName], [bitActive] ) 
SELECT @numMaxReportModuleID, 'Returns', 1 
SET IDENTITY_INSERT [dbo].[ReportModuleMaster] OFF

SELECT * FROM [dbo].[ReportModuleMaster] AS RMM

--------------------------------------------
DECLARE @numReportModuleGroupID NUMERIC(18,0)
SELECT @numReportModuleGroupID = MAX([numReportModuleGroupID]) + 1 FROM [dbo].[ReportModuleGroupMaster]
PRINT @numReportModuleGroupID

SET IDENTITY_INSERT [dbo].[ReportModuleGroupMaster] ON
INSERT INTO [dbo].[ReportModuleGroupMaster]( [numReportModuleGroupID] ,[numReportModuleID] ,[vcGroupName] ,[bitActive])
SELECT @numReportModuleGroupID, @numMaxReportModuleID, 'Returns', 1
--SELECT @numReportModuleGroupID, 7, 'Returns', 1
SET IDENTITY_INSERT [dbo].[ReportModuleGroupMaster] OFF

SELECT * FROM [dbo].[ReportModuleGroupMaster] AS RMGM WHERE [RMGM].[bitActive] = 1
--------------------------------------------

DECLARE @numMaxReportFieldGroupID NUMERIC(18,0)
SELECT @numMaxReportFieldGroupID = MAX([numReportFieldGroupID]) + 1 FROM [dbo].[ReportFieldGroupMaster]
PRINT @numMaxReportFieldGroupID

SET IDENTITY_INSERT [dbo].[ReportFieldGroupMaster] ON
INSERT INTO [dbo].[ReportFieldGroupMaster] ( [numReportFieldGroupID],[vcFieldGroupName] ,[bitActive] ,[bitCustomFieldGroup] ,[numGroupID] ,[vcCustomTableName])
SELECT @numMaxReportFieldGroupID, 'Returns',1,NULL,NULL,NULL

SET IDENTITY_INSERT [dbo].[ReportFieldGroupMaster] OFF

--------------------------------------------
INSERT INTO [dbo].[ReportModuleGroupFieldMappingMaster]( [numReportModuleGroupID] ,[numReportFieldGroupID])
SELECT @numMaxReportFieldGroupID,[RMGFMM].[numReportFieldGroupID] FROM [dbo].[ReportModuleGroupFieldMappingMaster] AS RMGFMM WHERE [RMGFMM].[numReportModuleGroupID] IN (9)

--INSERT INTO [dbo].[ReportModuleGroupFieldMappingMaster]( [numReportModuleGroupID] ,[numReportFieldGroupID])
--SELECT 24,35 

SELECT * FROM [dbo].[ReportModuleGroupFieldMappingMaster] AS RMGFMM WHERE [RMGFMM].[numReportModuleGroupID] IN (@numMaxReportFieldGroupID)

--SELECT * FROM [dbo].[ReportModuleGroupFieldMappingMaster] WHERE [numReportModuleGroupID] IN (35)

SELECT * FROM [dbo].[ReturnHeader] AS RH


SET IDENTITY_INSERT [dbo].[DycFieldMaster] ON

DECLARE @numMaxFieldID NUMERIC(18,0)
SELECT @numMaxFieldID = MAX([DFM].[numFieldId]) + 1 FROM [dbo].[DycFieldMaster] AS DFM
PRINT @numMaxFieldID

INSERT INTO [dbo].[DycFieldMaster] 
( [numFieldId], [numModuleID] ,[numDomainID] ,[vcFieldName] ,[vcDbColumnName] ,[vcOrigDbColumnName] ,[vcPropertyName] ,[vcLookBackTableName] ,[vcFieldDataType] ,[vcFieldType] ,[vcAssociatedControlType] ,
[vcToolTip] ,[vcListItemType] ,[numListID] ,[PopupFunctionName] ,[order] ,[tintRow] ,[tintColumn] ,[bitInResults] ,[bitDeleted] ,[bitAllowEdit] ,[bitDefault] ,[bitSettingField] ,[bitAddField] ,
[bitDetailField] ,[bitAllowSorting] ,[bitWorkFlowField] ,[bitImport] ,[bitExport] ,[bitAllowFiltering] ,[bitInlineEdit] ,[bitRequired] ,[intColumnWidth] ,[intFieldMaxLength] ,[intWFCompare] ,
[vcGroup] ,[vcWFCompareField])
VALUES  ( @numMaxFieldID, -- numModuleID - numeric
		  39 , -- numModuleID - numeric
          NULL , -- numDomainID - numeric
          N'Return Date' , -- vcFieldName - nvarchar(50)
          N'dtCreatedDate' , -- vcDbColumnName - nvarchar(50)
          N'dtCreatedDate' , -- vcOrigDbColumnName - nvarchar(50)
          'CreatedDate' , -- vcPropertyName - varchar(100)
          N'ReturnHeader' , -- vcLookBackTableName - nvarchar(50)
          'V' , -- vcFieldDataType - char(1)
          'R' , -- vcFieldType - char(1)
          N'TextBox' , -- vcAssociatedControlType - nvarchar(50)
          NULL , -- vcToolTip - nvarchar(1000)
          '' , -- vcListItemType - varchar(3)
          0 , -- numListID - numeric
          NULL , -- PopupFunctionName - varchar(100)
          0 , -- order - tinyint
          NULL , -- tintRow - tinyint
          NULL, -- tintColumn - tinyint
          0 , -- bitInResults - bit
          0 , -- bitDeleted - bit
          0 , -- bitAllowEdit - bit
          0 , -- bitDefault - bit
          0 , -- bitSettingField - bit
          0 , -- bitAddField - bit
          0 , -- bitDetailField - bit
          1 , -- bitAllowSorting - bit
          0 , -- bitWorkFlowField - bit
          0 , -- bitImport - bit
          0 , -- bitExport - bit
          1 , -- bitAllowFiltering - bit
          0 , -- bitInlineEdit - bit
          0 , -- bitRequired - bit
          0 , -- intColumnWidth - int
          0 , -- intFieldMaxLength - int
          0 , -- intWFCompare - int
          NULL , -- vcGroup - varchar(100)
          NULL  -- vcWFCompareField - varchar(150)
        )

SET IDENTITY_INSERT [dbo].[DycFieldMaster] OFF

INSERT INTO [dbo].[ReportFieldGroupMappingMaster]([numReportFieldGroupID], [numFieldID], [vcFieldName], 
[vcAssociatedControlType], [vcFieldDataType], [bitAllowSorting], [bitAllowGrouping], [bitAllowAggregate], [bitAllowFiltering])
SELECT 35, numFieldId, vcFieldName,vcAssociatedControlType, 'D', 1, 0, 0, 0 FROM DycFieldMaster 
WHERE numModuleID=39 AND vcLookBackTableName='ReturnHeader' AND [DycFieldMaster].[numFieldId] = @numMaxFieldID

--INSERT INTO [dbo].[ReportFieldGroupMappingMaster]([numReportFieldGroupID], [numFieldID], [vcFieldName], 
--[vcAssociatedControlType], [vcFieldDataType], [bitAllowSorting], [bitAllowGrouping], [bitAllowAggregate], [bitAllowFiltering])
--SELECT 5, numFieldId, vcFieldName, vcDbColumnName, 'D', 1, 1, 0, 1 FROM DycFieldMaster 
--WHERE numModuleID=39 AND vcLookBackTableName='ReturnHeader' AND [DycFieldMaster].[numFieldId] = 10695
----------------------------
---------------------------------------------

SET IDENTITY_INSERT [dbo].[DycFieldMaster] ON

DECLARE @numMaxFieldID NUMERIC(18,0)
SELECT @numMaxFieldID = MAX([DFM].[numFieldId]) + 1 FROM [dbo].[DycFieldMaster] AS DFM
PRINT @numMaxFieldID

INSERT INTO [dbo].[DycFieldMaster] 
( [numFieldId], [numModuleID] ,[numDomainID] ,[vcFieldName] ,[vcDbColumnName] ,[vcOrigDbColumnName] ,[vcPropertyName] ,[vcLookBackTableName] ,[vcFieldDataType] ,[vcFieldType] ,[vcAssociatedControlType] ,
[vcToolTip] ,[vcListItemType] ,[numListID] ,[PopupFunctionName] ,[order] ,[tintRow] ,[tintColumn] ,[bitInResults] ,[bitDeleted] ,[bitAllowEdit] ,[bitDefault] ,[bitSettingField] ,[bitAddField] ,
[bitDetailField] ,[bitAllowSorting] ,[bitWorkFlowField] ,[bitImport] ,[bitExport] ,[bitAllowFiltering] ,[bitInlineEdit] ,[bitRequired] ,[intColumnWidth] ,[intFieldMaxLength] ,[intWFCompare] ,
[vcGroup] ,[vcWFCompareField])
VALUES  ( @numMaxFieldID, -- numModuleID - numeric
		  39 , -- numModuleID - numeric
          NULL , -- numDomainID - numeric
          N'RMA' , -- vcFieldName - nvarchar(50)
          N'vcRMA' , -- vcDbColumnName - nvarchar(50)
          N'vcRMA' , -- vcOrigDbColumnName - nvarchar(50)
          'RMA' , -- vcPropertyName - varchar(100)
          N'ReturnHeader' , -- vcLookBackTableName - nvarchar(50)
          'V' , -- vcFieldDataType - char(1)
          'R' , -- vcFieldType - char(1)
          N'TextBox' , -- vcAssociatedControlType - nvarchar(50)
          NULL , -- vcToolTip - nvarchar(1000)
          '' , -- vcListItemType - varchar(3)
          0 , -- numListID - numeric
          NULL , -- PopupFunctionName - varchar(100)
          0 , -- order - tinyint
          NULL , -- tintRow - tinyint
          NULL, -- tintColumn - tinyint
          0 , -- bitInResults - bit
          0 , -- bitDeleted - bit
          0 , -- bitAllowEdit - bit
          0 , -- bitDefault - bit
          0 , -- bitSettingField - bit
          0 , -- bitAddField - bit
          0 , -- bitDetailField - bit
          1 , -- bitAllowSorting - bit
          0 , -- bitWorkFlowField - bit
          0 , -- bitImport - bit
          0 , -- bitExport - bit
          1 , -- bitAllowFiltering - bit
          0 , -- bitInlineEdit - bit
          0 , -- bitRequired - bit
          0 , -- intColumnWidth - int
          0 , -- intFieldMaxLength - int
          0 , -- intWFCompare - int
          NULL , -- vcGroup - varchar(100)
          NULL  -- vcWFCompareField - varchar(150)
        )

SET IDENTITY_INSERT [dbo].[DycFieldMaster] OFF

INSERT INTO [dbo].[ReportFieldGroupMappingMaster]([numReportFieldGroupID], [numFieldID], [vcFieldName], 
[vcAssociatedControlType], [vcFieldDataType], [bitAllowSorting], [bitAllowGrouping], [bitAllowAggregate], [bitAllowFiltering])
SELECT 35, numFieldId, vcFieldName,vcAssociatedControlType, 'D', 1, 1, 0, 1 FROM DycFieldMaster 
WHERE numModuleID=39 AND vcLookBackTableName='ReturnHeader' AND [DycFieldMaster].[numFieldId] = @numMaxFieldID
-----------------------

SET IDENTITY_INSERT [dbo].[DycFieldMaster] ON

DECLARE @numMaxFieldID NUMERIC(18,0)
SELECT @numMaxFieldID = MAX([DFM].[numFieldId]) + 1 FROM [dbo].[DycFieldMaster] AS DFM
PRINT @numMaxFieldID

INSERT INTO [dbo].[DycFieldMaster] 
( [numFieldId], [numModuleID] ,[numDomainID] ,[vcFieldName] ,[vcDbColumnName] ,[vcOrigDbColumnName] ,[vcPropertyName] ,[vcLookBackTableName] ,[vcFieldDataType] ,[vcFieldType] ,[vcAssociatedControlType] ,
[vcToolTip] ,[vcListItemType] ,[numListID] ,[PopupFunctionName] ,[order] ,[tintRow] ,[tintColumn] ,[bitInResults] ,[bitDeleted] ,[bitAllowEdit] ,[bitDefault] ,[bitSettingField] ,[bitAddField] ,
[bitDetailField] ,[bitAllowSorting] ,[bitWorkFlowField] ,[bitImport] ,[bitExport] ,[bitAllowFiltering] ,[bitInlineEdit] ,[bitRequired] ,[intColumnWidth] ,[intFieldMaxLength] ,[intWFCompare] ,
[vcGroup] ,[vcWFCompareField])
VALUES  ( @numMaxFieldID, -- numModuleID - numeric
		  39 , -- numModuleID - numeric
          NULL , -- numDomainID - numeric
          N'Bizdoc Name' , -- vcFieldName - nvarchar(50)
          N'vcBizDocName' , -- vcDbColumnName - nvarchar(50)
          N'vcBizDocName' , -- vcOrigDbColumnName - nvarchar(50)
          'BizdocName' , -- vcPropertyName - varchar(100)
          N'ReturnHeader' , -- vcLookBackTableName - nvarchar(50)
          'V' , -- vcFieldDataType - char(1)
          'R' , -- vcFieldType - char(1)
          N'TextBox' , -- vcAssociatedControlType - nvarchar(50)
          NULL , -- vcToolTip - nvarchar(1000)
          '' , -- vcListItemType - varchar(3)
          0 , -- numListID - numeric
          NULL , -- PopupFunctionName - varchar(100)
          0 , -- order - tinyint
          NULL , -- tintRow - tinyint
          NULL, -- tintColumn - tinyint
          0 , -- bitInResults - bit
          0 , -- bitDeleted - bit
          0 , -- bitAllowEdit - bit
          0 , -- bitDefault - bit
          0 , -- bitSettingField - bit
          0 , -- bitAddField - bit
          0 , -- bitDetailField - bit
          1 , -- bitAllowSorting - bit
          0 , -- bitWorkFlowField - bit
          0 , -- bitImport - bit
          0 , -- bitExport - bit
          1 , -- bitAllowFiltering - bit
          0 , -- bitInlineEdit - bit
          0 , -- bitRequired - bit
          0 , -- intColumnWidth - int
          0 , -- intFieldMaxLength - int
          0 , -- intWFCompare - int
          NULL , -- vcGroup - varchar(100)
          NULL  -- vcWFCompareField - varchar(150)
        )

SET IDENTITY_INSERT [dbo].[DycFieldMaster] OFF

INSERT INTO [dbo].[ReportFieldGroupMappingMaster]([numReportFieldGroupID], [numFieldID], [vcFieldName], 
[vcAssociatedControlType], [vcFieldDataType], [bitAllowSorting], [bitAllowGrouping], [bitAllowAggregate], [bitAllowFiltering])
SELECT 35, numFieldId, vcFieldName,vcAssociatedControlType, 'D', 1, 1, 0, 1 FROM DycFieldMaster 
WHERE numModuleID=39 AND vcLookBackTableName='ReturnHeader' AND [DycFieldMaster].[numFieldId] = @numMaxFieldID

---------------------------------------

SET IDENTITY_INSERT [dbo].[DycFieldMaster] ON

DECLARE @numMaxFieldID NUMERIC(18,0)
SELECT @numMaxFieldID = MAX([DFM].[numFieldId]) + 1 FROM [dbo].[DycFieldMaster] AS DFM
PRINT @numMaxFieldID

INSERT INTO [dbo].[DycFieldMaster] 
( [numFieldId], [numModuleID] ,[numDomainID] ,[vcFieldName] ,[vcDbColumnName] ,[vcOrigDbColumnName] ,[vcPropertyName] ,[vcLookBackTableName] ,[vcFieldDataType] ,[vcFieldType] ,[vcAssociatedControlType] ,
[vcToolTip] ,[vcListItemType] ,[numListID] ,[PopupFunctionName] ,[order] ,[tintRow] ,[tintColumn] ,[bitInResults] ,[bitDeleted] ,[bitAllowEdit] ,[bitDefault] ,[bitSettingField] ,[bitAddField] ,
[bitDetailField] ,[bitAllowSorting] ,[bitWorkFlowField] ,[bitImport] ,[bitExport] ,[bitAllowFiltering] ,[bitInlineEdit] ,[bitRequired] ,[intColumnWidth] ,[intFieldMaxLength] ,[intWFCompare] ,
[vcGroup] ,[vcWFCompareField])
VALUES  ( @numMaxFieldID, -- numModuleID - numeric
		  39 , -- numModuleID - numeric
          NULL , -- numDomainID - numeric
          N'Return Amount' , -- vcFieldName - nvarchar(50)
          N'monAmount' , -- vcDbColumnName - nvarchar(50)
          N'monAmount' , -- vcOrigDbColumnName - nvarchar(50)
          'Amount' , -- vcPropertyName - varchar(100)
          N'ReturnHeader' , -- vcLookBackTableName - nvarchar(50)
          'N' , -- vcFieldDataType - char(1)
          'R' , -- vcFieldType - char(1)
          N'TextBox' , -- vcAssociatedControlType - nvarchar(50)
          NULL , -- vcToolTip - nvarchar(1000)
          '' , -- vcListItemType - varchar(3)
          0 , -- numListID - numeric
          NULL , -- PopupFunctionName - varchar(100)
          0 , -- order - tinyint
          NULL , -- tintRow - tinyint
          NULL, -- tintColumn - tinyint
          0 , -- bitInResults - bit
          0 , -- bitDeleted - bit
          0 , -- bitAllowEdit - bit
          0 , -- bitDefault - bit
          0 , -- bitSettingField - bit
          0 , -- bitAddField - bit
          0 , -- bitDetailField - bit
          1 , -- bitAllowSorting - bit
          0 , -- bitWorkFlowField - bit
          0 , -- bitImport - bit
          0 , -- bitExport - bit
          1 , -- bitAllowFiltering - bit
          0 , -- bitInlineEdit - bit
          0 , -- bitRequired - bit
          0 , -- intColumnWidth - int
          0 , -- intFieldMaxLength - int
          0 , -- intWFCompare - int
          NULL , -- vcGroup - varchar(100)
          NULL  -- vcWFCompareField - varchar(150)
        )

SET IDENTITY_INSERT [dbo].[DycFieldMaster] OFF

INSERT INTO [dbo].[ReportFieldGroupMappingMaster]([numReportFieldGroupID], [numFieldID], [vcFieldName], 
[vcAssociatedControlType], [vcFieldDataType], [bitAllowSorting], [bitAllowGrouping], [bitAllowAggregate], [bitAllowFiltering])
SELECT 35, numFieldId, vcFieldName,vcAssociatedControlType, 'D', 1, 1, 0, 1 FROM DycFieldMaster 
WHERE numModuleID=39 AND vcLookBackTableName='ReturnHeader' AND [DycFieldMaster].[numFieldId] = @numMaxFieldID

------------------------------

SET IDENTITY_INSERT [dbo].[DycFieldMaster] ON

DECLARE @numMaxFieldID NUMERIC(18,0)
SELECT @numMaxFieldID = MAX([DFM].[numFieldId]) + 1 FROM [dbo].[DycFieldMaster] AS DFM
PRINT @numMaxFieldID

INSERT INTO [dbo].[DycFieldMaster] 
( [numFieldId], [numModuleID] ,[numDomainID] ,[vcFieldName] ,[vcDbColumnName] ,[vcOrigDbColumnName] ,[vcPropertyName] ,[vcLookBackTableName] ,[vcFieldDataType] ,[vcFieldType] ,[vcAssociatedControlType] ,
[vcToolTip] ,[vcListItemType] ,[numListID] ,[PopupFunctionName] ,[order] ,[tintRow] ,[tintColumn] ,[bitInResults] ,[bitDeleted] ,[bitAllowEdit] ,[bitDefault] ,[bitSettingField] ,[bitAddField] ,
[bitDetailField] ,[bitAllowSorting] ,[bitWorkFlowField] ,[bitImport] ,[bitExport] ,[bitAllowFiltering] ,[bitInlineEdit] ,[bitRequired] ,[intColumnWidth] ,[intFieldMaxLength] ,[intWFCompare] ,
[vcGroup] ,[vcWFCompareField])
VALUES  ( @numMaxFieldID, -- numModuleID - numeric
		  39 , -- numModuleID - numeric
          NULL , -- numDomainID - numeric
          N'Comments' , -- vcFieldName - nvarchar(50)
          N'vcComments' , -- vcDbColumnName - nvarchar(50)
          N'vcComments' , -- vcOrigDbColumnName - nvarchar(50)
          'Comments' , -- vcPropertyName - varchar(100)
          N'ReturnHeader' , -- vcLookBackTableName - nvarchar(50)
          'V' , -- vcFieldDataType - char(1)
          'R' , -- vcFieldType - char(1)
          N'TextBox' , -- vcAssociatedControlType - nvarchar(50)
          NULL , -- vcToolTip - nvarchar(1000)
          '' , -- vcListItemType - varchar(3)
          0 , -- numListID - numeric
          NULL , -- PopupFunctionName - varchar(100)
          0 , -- order - tinyint
          NULL , -- tintRow - tinyint
          NULL, -- tintColumn - tinyint
          0 , -- bitInResults - bit
          0 , -- bitDeleted - bit
          0 , -- bitAllowEdit - bit
          0 , -- bitDefault - bit
          0 , -- bitSettingField - bit
          0 , -- bitAddField - bit
          0 , -- bitDetailField - bit
          1 , -- bitAllowSorting - bit
          0 , -- bitWorkFlowField - bit
          0 , -- bitImport - bit
          0 , -- bitExport - bit
          1 , -- bitAllowFiltering - bit
          0 , -- bitInlineEdit - bit
          0 , -- bitRequired - bit
          0 , -- intColumnWidth - int
          0 , -- intFieldMaxLength - int
          0 , -- intWFCompare - int
          NULL , -- vcGroup - varchar(100)
          NULL  -- vcWFCompareField - varchar(150)
        )

SET IDENTITY_INSERT [dbo].[DycFieldMaster] OFF

INSERT INTO [dbo].[ReportFieldGroupMappingMaster]([numReportFieldGroupID], [numFieldID], [vcFieldName], 
[vcAssociatedControlType], [vcFieldDataType], [bitAllowSorting], [bitAllowGrouping], [bitAllowAggregate], [bitAllowFiltering])
SELECT 35, numFieldId, vcFieldName,vcAssociatedControlType, 'D', 1, 1, 0, 1 FROM DycFieldMaster 
WHERE numModuleID=39 AND vcLookBackTableName='ReturnHeader' AND [DycFieldMaster].[numFieldId] = @numMaxFieldID

---------------------------------
ROLLBACK

------/******************************************************************
------Project: BACRMUI   Date: 9.DEC.2014
------Comments: Add New Fields to Similar Items for E-Commerece Related Item settings
------*******************************************************************/

ALTER TABLE [dbo].[SimilarItems] ADD bitPreUpSell BIT, bitPostUpSell BIT, vcUpSellDesc VARCHAR(1000)

UPDATE [dbo].[PageNavigationDTL] SET [vcPageNavName] = 'Commission Rules' WHERE [vcPageNavName]  = 'Commission Rule'
--=======================================================================


------/******************************************************************
------Project: BACRMUI   Date: 4.DEC.2014
------Comments: Add Item Custom Fields to Opportunity Items Module within custom reports
------*******************************************************************/

BEGIN TRANSACTION

INSERT INTO [dbo].[ReportFieldGroupMaster]( [vcFieldGroupName] ,[bitActive] ,[bitCustomFieldGroup] ,[numGroupID] ,[vcCustomTableName])
SELECT 'Items Custom Field',1,	1,	5,	'CFW_FLD_Values_Item'

INSERT INTO [dbo].[ReportModuleGroupFieldMappingMaster]( [numReportModuleGroupID] ,[numReportFieldGroupID])
SELECT 10,(SELECT MAX([numReportFieldGroupID]) FROM ReportFieldGroupMaster)

SELECT * FROM ReportModuleGroupFieldMappingMaster 
JOIN ReportFieldGroupMaster ON [ReportModuleGroupFieldMappingMaster].[numReportFieldGroupID] = [ReportFieldGroupMaster].numReportFieldGroupID
WHERE [ReportModuleGroupFieldMappingMaster].[numReportModuleGroupID] IN (10)

ROLLBACK
--=======================================================================


------/******************************************************************
------Project: BACRMUI   Date: 2.DEC.2014
------Comments: Add new columns to E-Commerce Details
------*******************************************************************/

ALTER TABLE [dbo].[eCommerceDTL] ADD bitPreSellUp BIT, bitPostSellUp BIT, dcPostSellDiscount DECIMAL

--=======================================================================

*/

/* ------------ UPDATED ON PRODUCTION ALREADY ON 29 Nov,2014--------------------------------

------/******************************************************************
------Project: BACRMUI   Date: 22.Nov.2014
------Comments: Add new column to custom reports for Serial Number
------*******************************************************************/

INSERT INTO [dbo].[ReportFieldGroupMappingMaster]([numReportFieldGroupID], [numFieldID], [vcFieldName], 
[vcAssociatedControlType], [vcFieldDataType], [bitAllowSorting], [bitAllowGrouping], [bitAllowAggregate], [bitAllowFiltering])
SELECT 4, numFieldId, vcFieldName, vcDbColumnName, vcFieldDataType, 0, 0, 0, 0 FROM DycFieldMaster 
WHERE numModuleID=4 AND vcLookBackTableName='WareHouseItmsDTL' AND [DycFieldMaster].[numFieldId] = 320

--=======================================================================

*/

/* ------------ UPDATED ON PRODUCTION ALREADY ON 03 Nov,2014--------------------------------

------/******************************************************************
------Project: BACRMUI   Date: 1.Nov.2014
------Comments: Add new column to ReturnHeader
------*******************************************************************/

ALTER TABLE [dbo].[ReturnHeader] ADD IsUnappliedPayment BIT DEFAULT 0

UPDATE [dbo].[ReturnHeader] SET IsUnappliedPayment = 0

UPDATE [PageNavigationDTL] SET [vcNavURL] = '../Accounting/frmJournalDetailsReport.aspx' WHERE 
[vcNavURL] = '../Accounting/frmJournalEntryReport.aspx'

--=======================================================================

*/

/* ------------ UPDATED ON PRODUCTION ALREADY ON 17th OCT,2014--------------------------------

------/******************************************************************
------Project: BACRMUI   Date: 16.Oct.2014
------Comments: Add new column to ReturnHeader
------*******************************************************************/

ALTER TABLE [dbo].[ReturnHeader] ADD numParentID NUMERIC(18,0)

UPDATE [dbo].[ReturnHeader] SET numParentID = 0 
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.ReturnHeader ADD CONSTRAINT
	DF_ReturnHeader_numParentID DEFAULT 0 FOR numParentID
GO
ALTER TABLE dbo.ReturnHeader SET (LOCK_ESCALATION = TABLE)
GO
COMMIT


------/******************************************************************
------Project: BizCart   Date: 06.Oct.2014
------Comments: Added a new setting for displaying treeview as per need (TreeView-UL/LI Based Tree)
------*******************************************************************/

BEGIN TRANSACTION

INSERT INTO [dbo].[PageElementAttributes]
( [numElementID] ,[vcAttributeName] ,[vcControlType] ,[vcControlValues] ,[bitEditor])
SELECT 1, 'Populate UL LI Based Tree','CheckBox',NULL,NULL

/*
UPDATE [PageElementAttributes] SET [vcAttributeName] = 'Populate UL LI Based Tree' 
WHERE [PageElementAttributes].[vcAttributeName] = 'Populate ul li Based Tree' 
AND [numElementID] = 1 
AND [numAttributeID] = 10116
*/
ROLLBACK

------/******************************************************************
------Project: BACRMUI   Date: 30.Sep.2014
------Comments: Create a new report Credit Card Register & Journal Entries Report
------*******************************************************************/

BEGIN TRANSACTION
	DECLARE @numPageNavID numeric 
	SELECT @numPageNavID = MAX(numPageNavID) + 1  FROM dbo.PageNavigationDTL 
	INSERT INTO dbo.PageNavigationDTL
			( numPageNavID ,
			  numModuleID ,
			  numParentID ,
			  vcPageNavName ,
			  vcNavURL ,
			  vcImageURL ,
			  bitVisible ,
			  numTabID
			)
	SELECT @numPageNavID ,35,222,'Credit Card Register','../Accounting/frmAccountsReports.aspx?RType=9',NULL,1,45 
	EXEC USP_CreateTreeNavigationForDomain 0,@numPageNavID --Give permission to tree node.

	exec USP_GetPageNavigation @numModuleID=35,@numDomainID=169,@numGroupID=838
ROLLBACK


BEGIN TRANSACTION

	DECLARE @numPageNavID numeric 
	SELECT @numPageNavID = MAX(numPageNavID) + 1  FROM dbo.PageNavigationDTL 
	INSERT INTO dbo.PageNavigationDTL
			( numPageNavID ,
			  numModuleID ,
			  numParentID ,
			  vcPageNavName ,
			  vcNavURL ,
			  vcImageURL ,
			  bitVisible ,
			  numTabID
			)
	SELECT @numPageNavID ,35,93,'Journal Entry','../Accounting/frmJournalEntryReport.aspx',NULL,1,45 
	EXEC USP_CreateTreeNavigationForDomain 0,@numPageNavID --Give permission to tree node.

	exec USP_GetPageNavigation @numModuleID=35,@numDomainID=169,@numGroupID=838

ROLLBACK
*/
/* ------------ UPDATED ON PRODUCTION ALREADY ON 22nd Sep,2014--------------------------------
ALTER TABLE [dbo].[General_Journal_Header] ADD numAmountWith4Decimals MONEY
UPDATE [dbo].[General_Journal_Header] SET numAmountWith4Decimals = ISNULL([numAmount],0)
UPDATE [dbo].[General_Journal_Header] SET [numAmount] = ROUND(ISNULL([numAmount],0),2)

ALTER TABLE [dbo].[General_Journal_Details] ADD numDebitAmtWith4Decimals MONEY, numCreditAmtWith4Decimals MONEY
UPDATE [dbo].[General_Journal_Details] SET numDebitAmtWith4Decimals = ISNULL([numDebitAmt],0)
UPDATE [dbo].[General_Journal_Details] SET numCreditAmtWith4Decimals = ISNULL(numCreditAmt,0)

UPDATE [dbo].[General_Journal_Details] SET [numDebitAmt] = ROUND(ISNULL([numDebitAmt],0),2)
UPDATE [dbo].[General_Journal_Details] SET [numCreditAmt] = ROUND(ISNULL(numCreditAmt,0),2)

*/

/* ------------ UPDATED ON PRODUCTION ALREADY ON 2nd Sep,2014--------------------------------
------/******************************************************************
------Project: BACRMUI   Date: 2.Sep.2014
------Comments: Store master SequenceID to know modified data are there or not.
------*******************************************************************/
ALTER TABLE OpportunityBizDocs ADD numMasterBizdocSequenceID VARCHAR(50)
--------------------------------------------------------------------------

/*
Add this into Web.config for maximizing json length
<system.web.extensions>
            <scripting>
                <webServices>
                    <jsonSerialization maxJsonLength="50000000"/>
                </webServices>
            </scripting>
        </system.web.extensions>
*/

------/******************************************************************
------Project: BACRMUI   Date: 29.Aug.2014
------Comments: Copy Original 4 Decimal Deal Amount To a new Field
------*******************************************************************/
BEGIN TRANSACTION

ALTER TABLE [dbo].[OpportunityMaster] ADD monDealAmountWith4Decimals MONEY 
ALTER TABLE [dbo].[OpportunityBizDocs] ADD monDealAmountWith4Decimals MONEY 
ALTER TABLE [dbo].DepositMaster ADD monAppliedAmountWith4Decimals MONEY ,monDepositAmountWith4Decimals MONEY ,monRefundAmountWith4Decimals MONEY 
ALTER TABLE [dbo].DepositMaster ADD monAppliedAmountWith4Decimals MONEY ,monDepositAmountWith4Decimals MONEY ,monRefundAmountWith4Decimals MONEY 
ALTER TABLE [dbo].ReturnHeader ADD monAmountWith4Decimals MONEY ,
								   monBizDocAmountWith4Decimals MONEY ,
								   monBizDocUsedAmountWith4Decimals MONEY,
								   monTotalTaxWith4Decimals MONEY,
								   monTotalDiscountWith4Decimals MONEY 

UPDATE [dbo].[OpportunityMaster] SET [monDealAmountWith4Decimals] = ISNULL(monDealAmount,0)
UPDATE [dbo].[OpportunityBizDocs] SET [monDealAmountWith4Decimals] = ISNULL(monDealAmount,0)

UPDATE [dbo].[OpportunityMaster] SET monDealAmount = ROUND(ISNULL(monDealAmount,0),2)
UPDATE [dbo].[OpportunityBizDocs] SET monDealAmount = ROUND(ISNULL(monDealAmount,0),2)

UPDATE [dbo].DepositMaster SET monAppliedAmountWith4Decimals  = [monAppliedAmount],
								monDepositAmountWith4Decimals = [monDepositAmount],
								monRefundAmountWith4Decimals = [monRefundAmount]

UPDATE [dbo].DepositMaster SET [monAppliedAmount] = ROUND(ISNULL([monAppliedAmount],0),2),
								[monDepositAmount] = ROUND(ISNULL([monDepositAmount],0),2),
								[monRefundAmount] = ROUND(ISNULL([monRefundAmount],0),2)

UPDATE [dbo].[ReturnHeader] SET monAmountWith4Decimals  = monAmount,
								monBizDocAmountWith4Decimals = monBizDocAmount,
								monBizDocUsedAmountWith4Decimals = monBizDocUsedAmount,
								monTotalTaxWith4Decimals = monTotalTax,
								monTotalDiscountWith4Decimals = monTotalDiscount

UPDATE [dbo].[ReturnHeader] SET monAmount = ROUND(ISNULL(monAmount,0),2),
								monBizDocAmount = ROUND(ISNULL(monBizDocAmount,0),2),
								monBizDocUsedAmount = ROUND(ISNULL(monBizDocUsedAmount,0),2),
								monTotalTax = ROUND(ISNULL(monTotalTax,0),2),
								monTotalDiscount = ROUND(ISNULL(monTotalDiscount,0),2)
ROLLBACK

*/


/*
------------ UPDATED ON PRODUCTION ALREADY ON 21AUG,2014--------------------------------
------/******************************************************************
------Project: BACRMUI   Date: 06.08.2014
------Comments: Adding Endicia Account mode into Shipping for specific use.
------*******************************************************************/
BEGIN TRANSACTION

SET IDENTITY_INSERT [dbo].[ShippingFields] ON
INSERT INTO [dbo].[ShippingFields]( [intShipFieldID] ,[vcFieldName] ,[numListItemID] ,[vcToolTip])
SELECT 21,'Is Endicia',90,'Used for identifying whether configured account is Endicia or not'
UNION
SELECT 25,'Endicia UserID',90,'Endicia UserID'
UNION
SELECT 23,'Endicia Password',90,'Endicia Password'
UNION
SELECT 22,'Endicia Label Server URL',90,'Used for Endicia Label Server URL'
UNION
SELECT 24,'Endicia Tracking URL',90,'Used for Endicia Tracking URL'
SET IDENTITY_INSERT dbo.DycFieldMaster OFF


--SELECT * FROM [dbo].[ShippingFieldValues] AS SFV WHERE [SFV].[numDomainID] = 1 AND [SFV].[numListItemID] = 90

DECLARE @numDomainID AS NUMERIC(18)
SELECT ROW_NUMBER() OVER (ORDER BY numDomainID)AS [RowID], numDomainID  INTO #tempDomains FROM dbo.Domain WHERE numDomainId > 0
DECLARE @intCnt AS INT
DECLARE @intCntDomain AS INT
SET @intCnt = 0
SET @intCntDomain = (SELECT COUNT(*) FROM #tempDomains)

IF @intCntDomain > 0
BEGIN
WHILE(@intCnt < @intCntDomain)
BEGIN

SET @intCnt = @intCnt + 1
SELECT @numDomainID = numDomainID FROM #tempDomains WHERE RowID = @intCnt
PRINT @numDomainID

INSERT INTO [dbo].[ShippingFieldValues]( [intShipFieldID] ,[vcShipFieldValue] ,[numDomainID] ,[numListItemID])
SELECT 21,'1',@numDomainID,90
UNION
SELECT 25,[SFV].[vcShipFieldValue],[SFV].[numDomainID],[SFV].[numListItemID] FROM [dbo].[ShippingFieldValues] AS SFV 
WHERE [SFV].[numDomainID] = @numDomainID 
AND [SFV].[numListItemID] = 90
AND sfv.[intShipFieldID] = 15
UNION
SELECT 23,[SFV].[vcShipFieldValue],[SFV].[numDomainID],[SFV].[numListItemID] FROM [dbo].[ShippingFieldValues] AS SFV 
WHERE [SFV].[numDomainID] = @numDomainID 
AND [SFV].[numListItemID] = 90
AND sfv.[intShipFieldID] = 16
UNION
SELECT 22,[SFV].[vcShipFieldValue],[SFV].[numDomainID],[SFV].[numListItemID] FROM [dbo].[ShippingFieldValues] AS SFV 
WHERE [SFV].[numDomainID] = @numDomainID 
AND [SFV].[numListItemID] = 90
AND sfv.[intShipFieldID] = 17
UNION
SELECT 24,[SFV].[vcShipFieldValue],[SFV].[numDomainID],[SFV].[numListItemID] FROM [dbo].[ShippingFieldValues] AS SFV 
WHERE [SFV].[numDomainID] = @numDomainID 
AND [SFV].[numListItemID] = 90
AND sfv.[intShipFieldID] = 18

END 

SELECT * FROM [dbo].[ShippingFieldValues] AS SFV 

END
DROP TABLE #tempDomains

ROLLBACK

--------------------------------------------------------------------------

------/******************************************************************
------Project: BACRMUI   Date: 04.08.2014
------Comments: ACCOUNTING ISSUE TRACING SCRIPTS
------*******************************************************************/

GO
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[GenealEntryAudit](
	[numDomainID] [numeric](18, 0) NOT NULL,
	[numJournalID] [numeric](18, 0) NOT NULL,
	[numTransactionID] [numeric](18, 0) NOT NULL,
	[dtCreatedDate] [datetime] NOT NULL,
	[varDescription] [varchar](1000) NOT NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING ON
GO

-------------
*/

-------- UPDATED ON PRODUCTION ALREADY ON :  23th Jul,2014------------------------------------
/*
------/******************************************************************
------Project: BACRMUI   Date: 03.07.2014
------Comments: CHange the size of decDiscount column within PricingTable
------*******************************************************************/
BEGIN TRANSACTION

ALTER TABLE [dbo].[PricingTable] ALTER COLUMN [decDiscount] [decimal] (18, 4) NOT NULL

ROLLBACK

*/

-------- UPDATED ON PRODUCTION ALREADY ON :  28th Jun,2014------------------------------------
/*
------/******************************************************************
------Project: BACRMUI   Date: 26.06.2014
------Comments: Change in Name of field "Contact Record Owner"
------*******************************************************************/
BEGIN TRANSACTION
--SELECT * FROM [dbo].[DycFormField_Mapping] AS DFFM WHERE [DFFM].[vcFieldName] = 'Contact Record Owner' AND [DFFM].[numFormID] = 36
UPDATE [dbo].[DycFormField_Mapping] SET [vcFieldName] = 'Organization & Contact Record Owner'  WHERE [vcFieldName] = 'Contact Record Owner' AND [numFormID] = 36
UPDATE [dbo].[DynamicFormField_Validation] SET [vcNewFormFieldName] = 'Organization & Contact Record Owner'  WHERE [vcNewFormFieldName] = 'Contact Record Owner' AND [numFormID] = 36
--SELECT * FROM [dbo].[DycFormField_Mapping] AS DFFM WHERE [DFFM].[vcFieldName] = 'Organization & Contact Record Owner' AND [DFFM].[numFormID] = 36
ROLLBACK


------/******************************************************************
------Project: BACRMUI   Date: 18.06.2014
------Comments: Add new Import Category "Address Details"
------*******************************************************************/
BEGIN TRANSACTION

----------------------- RUN 1 ----------------
INSERT  INTO [dbo].[DynamicFormMaster]
        ( [numFormId] ,
          [vcFormName] ,
          [cCustomFieldsAssociated] ,
          [cAOIAssociated] ,
          [bitDeleted] ,
          [tintFlag] ,
          [bitWorkFlow] ,
          [vcLocationID] ,
          [bitAllowGridColor]
        )
VALUES  ( 98 , -- numFormId - numeric
          N'Update Address' , -- vcFormName - nvarchar(50)
          'N' , -- cCustomFieldsAssociated - char(1)
          'N' , -- cAOIAssociated - char(1)
          0 , -- bitDeleted - bit
          2 , -- tintFlag - tinyint
          NULL , -- bitWorkFlow - bit
          '' , -- vcLocationID - varchar(50)
          NULL  -- bitAllowGridColor - bit
        )

ROLLBACK

BEGIN TRANSACTION
----------------------- RUN 2 ----------------

SET IDENTITY_INSERT dbo.DycFieldMaster ON
INSERT INTO dbo.DycFieldMaster 
([numFieldId],numModuleID,numDomainID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,
vcToolTip,vcListItemType,numListID,PopupFunctionName,[order],tintRow,tintColumn,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,
bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,bitAllowFiltering,bitInlineEdit,bitRequired,intColumnWidth,intFieldMaxLength)
SELECT 529,2,NULL,'AddressID','numAddressID','numAddressID','AddressID','AddressDetails','N','R','TextBox',
NULL,'',0,NULL,32,NULL,NULL,1,0,0,0,0,NULL,
0,0,0,1,1,0,1,1,NULL,NULL
UNION
SELECT 530,2,NULL,'Address Name','vcAddressName','vcAddressName','AddressName','AddressDetails','V','R','TextBox',
NULL,'',0,NULL,32,NULL,NULL,1,0,0,0,0,NULL,
0,0,0,1,1,0,1,NULL,NULL,NULL
UNION
SELECT 531,2,NULL,'RecordID','numRecordID','numRecordID','RecordID','AddressDetails','N','R','TextBox',
NULL,'',0,NULL,32,NULL,NULL,1,0,0,0,0,NULL,
0,0,0,1,1,0,1,1,NULL,NULL
UNION
SELECT 532,2,NULL,'AddressType','tintAddressType','tintAddressType','AddressType','AddressDetails','N','R','TextBox',
NULL,'',0,NULL,32,NULL,NULL,1,0,0,0,0,NULL,
0,0,0,1,1,0,1,1,NULL,NULL
UNION
SELECT 533,2,NULL,'IsPrimary','bitIsPrimary','bitIsPrimary','IsPrimaryAddress','AddressDetails','Y','R','CheckBox',
NULL,'',0,NULL,32,NULL,NULL,1,0,0,0,0,NULL,
0,0,0,1,1,0,1,1,NULL,NULL
UNION
SELECT 534,2,NULL,'AddressOf','tintAddressOf','tintAddressOf','AddresOf','AddressDetails','N','R','TextBox',
NULL,'',0,NULL,32,NULL,NULL,1,0,0,0,0,NULL,
0,0,0,1,1,0,1,1,NULL,NULL

SET IDENTITY_INSERT dbo.DycFieldMaster OFF

-- SELECT * FROM  DycFieldMaster WHERE [DycFieldMaster].[numFieldId] IN (529,530)
ROLLBACK

---------------------------------------------------- RUN 3 ----------------------------------------------------
BEGIN TRANSACTION
-- SELECT * FROM  [dbo].[DycFormSectionDetail] AS DFSD
INSERT INTO dbo.DycFormSectionDetail(numFormID,intSectionID,vcSectionName,Loc_id) 
SELECT  98,1,'Address Details',0
ROLLBACK

---------------------------------------------------- RUN 4 ----------------------------------------------------
BEGIN TRANSACTION
INSERT INTO dbo.DycFormField_Mapping 
	(numModuleID,numFieldID,numDomainID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,[order],
	 tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,
	 bitAllowFiltering,bitRequired,numFormFieldID,intSectionID,bitAllowGridColor)
SELECT DISTINCT numModuleID,numFieldID,numDomainID,98,bitAllowEdit,bitInlineEdit,'Street',vcAssociatedControlType,'Street',PopupFunctionName,1,
	 tintRow,tintColumn,1,0,bitDefault,1,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,1,1,
	 0,bitRequired,numFormFieldID,1,bitAllowGridColor 
	 FROM dbo.DycFormField_Mapping 
	 WHERE [DycFormField_Mapping].[numFieldID] IN (82)
	 AND [DycFormField_Mapping].[numFormID] = 36
UNION 
SELECT DISTINCT numModuleID,numFieldID,numDomainID,98,bitAllowEdit,bitInlineEdit,'City',vcAssociatedControlType,'City',PopupFunctionName,1,
	 tintRow,tintColumn,1,0,bitDefault,1,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,1,1,
	 0,bitRequired,numFormFieldID,1,bitAllowGridColor 
	 FROM dbo.DycFormField_Mapping 
	 WHERE [DycFormField_Mapping].[numFieldID] IN (83)
	 AND [DycFormField_Mapping].[numFormID] = 36
UNION 
SELECT DISTINCT numModuleID,numFieldID,numDomainID,98,bitAllowEdit,bitInlineEdit,'State',vcAssociatedControlType,'State',PopupFunctionName,1,
	 tintRow,tintColumn,1,0,bitDefault,1,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,1,1,
	 0,bitRequired,numFormFieldID,1,bitAllowGridColor 
	 FROM dbo.DycFormField_Mapping 
	 WHERE [DycFormField_Mapping].[numFieldID] IN (84)
	 AND [DycFormField_Mapping].[numFormID] = 36
UNION 
SELECT DISTINCT numModuleID,numFieldID,numDomainID,98,bitAllowEdit,bitInlineEdit,'Country',vcAssociatedControlType,'Country',PopupFunctionName,1,
	 tintRow,tintColumn,1,0,bitDefault,1,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,1,1,
	 0,bitRequired,numFormFieldID,1,bitAllowGridColor 
	 FROM dbo.DycFormField_Mapping 
	 WHERE [DycFormField_Mapping].[numFieldID] IN (85)
	 AND [DycFormField_Mapping].[numFormID] = 36
UNION 
SELECT DISTINCT numModuleID,numFieldID,numDomainID,98,bitAllowEdit,bitInlineEdit,'Postal Code',vcAssociatedControlType,'PostalCode',PopupFunctionName,1,
	 tintRow,tintColumn,1,0,bitDefault,1,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,1,1,
	 0,bitRequired,numFormFieldID,1,bitAllowGridColor 
	 FROM dbo.DycFormField_Mapping 
	 WHERE [DycFormField_Mapping].[numFieldID] IN (86)
	 AND [DycFormField_Mapping].[numFormID] = 36
UNION 
SELECT DISTINCT numModuleID,numFieldID,numDomainID,98,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,1,
	 tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,1,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,1,1,
	 0,bitRequired,NULL,1,NULL 
	 FROM dbo.[DycFieldMaster] AS DFM 
	 WHERE DFM.[numFieldID] IN (529,530)
UNION
SELECT DISTINCT numModuleID,numFieldID,numDomainID,98,bitAllowEdit,bitInlineEdit,'RecordID',vcAssociatedControlType,'RecordID',PopupFunctionName,1,
	 tintRow,tintColumn,1,0,bitDefault,1,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,1,1,
	 0,bitRequired,NULL,1,NULL
	 FROM dbo.[DycFieldMaster] AS DFM 
	 WHERE DFM.[numFieldID] IN (531)
UNION
SELECT DISTINCT numModuleID,numFieldID,numDomainID,98,bitAllowEdit,bitInlineEdit,'AddressType',vcAssociatedControlType,'AddressType',PopupFunctionName,1,
	 tintRow,tintColumn,1,0,bitDefault,1,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,1,1,
	 0,bitRequired,NULL,1,NULL
	 FROM dbo.[DycFieldMaster] AS DFM 
	 WHERE DFM.[numFieldID] IN (532)
UNION
SELECT DISTINCT numModuleID,numFieldID,numDomainID,98,bitAllowEdit,bitInlineEdit,'IsPrimary',vcAssociatedControlType,'IsPrimaryAddress',PopupFunctionName,1,
	 tintRow,tintColumn,1,0,bitDefault,1,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,1,1,
	 0,bitRequired,NULL,1,NULL
	 FROM dbo.[DycFieldMaster] AS DFM 
	 WHERE DFM.[numFieldID] IN (533)
UNION
SELECT DISTINCT numModuleID,numFieldID,numDomainID,98,bitAllowEdit,bitInlineEdit,'AddressOf',vcAssociatedControlType,'AddresOf',PopupFunctionName,1,
	 tintRow,tintColumn,1,0,bitDefault,1,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,1,1,
	 0,bitRequired,NULL,1,NULL
	 FROM dbo.[DycFieldMaster] AS DFM 
	 WHERE DFM.[numFieldID] IN (534)


SELECT * FROM [dbo].[DycFormField_Mapping] AS DFFM WHERE [DFFM].[numFormID] = 98
ROLLBACK


------/******************************************************************
------Project: BACRMUI   Date: 11.06.2014
------Comments: Update Inventory Status to use for filtering
------*******************************************************************/
BEGIN TRANSACTION

UPDATE [dbo].[DycFieldMaster] SET [bitAllowFiltering] = 1, [bitAllowSorting] = 0 WHERE [vcFieldName] = 'Inventory Status' AND [vcDbColumnName] = 'vcInventoryStatus'
UPDATE [dbo].[DycFormField_Mapping] SET [bitAllowFiltering] = 1, [bitAllowSorting] = 0, [vcAssociatedControlType]= 'SelectBox' WHERE [vcFieldName] = 'Inventory Status' AND [vcPropertyName] = 'InventoryStatus'

ROLLBACK

*/

----------- UPDATED PROD UPDATE ON 01 Jun,2014
------/******************************************************************
------Project: BACRMUI   Date: 22.05.2014
------Comments: Add new order shipping details to OpportunityMaster
------*******************************************************************/

--BEGIN TRANSACTION

--ALTER TABLE dbo.OpportunityMaster ADD bitUseMarkupShippingRate BIT NULL,
--									  numMarkupShippingRate NUMERIC(19,2) NULL,
--									  intUsedShippingCompany INT NULL
									  	
--ROLLBACK

------/******************************************************************
------Project: BACRMUI   Date: 20.05.2014
------Comments: Contact Phone extension field should be 7 characters (currently it's 3 or 4).
------*******************************************************************/

--/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
--BEGIN TRANSACTION
--SET QUOTED_IDENTIFIER ON
--SET ARITHABORT ON
--SET NUMERIC_ROUNDABORT OFF
--SET CONCAT_NULL_YIELDS_NULL ON
--SET ANSI_NULLS ON
--SET ANSI_PADDING ON
--SET ANSI_WARNINGS ON
--COMMIT
--BEGIN TRANSACTION
--GO
--ALTER VIEW [dbo].[View_SelectEmailContact]  
--As
-- SELECT ADC.numContactID, C.vcCompanyName+'' AS CompanyName, ''+isnull(ld.vcData,'-') AS Company,                     
--               ADC.vcEmail AS vcEmail ,ADC.vcFirstName AS vcFirstName , ADC.vcLastName AS vcLastName ,adc.numDomainID AS numDomainID
--FROM dbo.AdditionalContactsInformation ADC INNER JOIN dbo.DivisionMaster DM 
--ON adc.numDivisionId = dm.numDivisionID INNER JOIN dbo.CompanyInfo C ON
--dm.numCompanyID = c.numCompanyId LEFT JOIN dbo.ListDetails ld ON ld.numListItemID=numCompanyType
----WHERE ADC.vcEmail NOT IN (SELECT vcEmailId FROM dbo.EmailMaster)

--UNION 
--SELECT 0 AS numContactId,'' AS  CompanyName,'' AS Company ,vcEmailId AS vcEmail ,vcName AS vcFirstName,'' AS vcLastName, numDomainID FROM dbo.emailmaster 
--WHERE vcEmailId NOT IN (SELECT                 
--               ADC.vcEmail  
--FROM dbo.AdditionalContactsInformation ADC INNER JOIN dbo.DivisionMaster DM 
--ON adc.numDivisionId = dm.numDivisionID INNER JOIN dbo.CompanyInfo C ON
--dm.numCompanyID = c.numCompanyId LEFT JOIN dbo.ListDetails ld ON ld.numListItemID=numCompanyType)
--/*
-- SELECT ROW_NUMBER() OVER (ORDER BY ADC.numDomainID) AS ID, ADC.numContactID, C.vcCompanyName+'' AS CompanyName,
----  ''+isnull(ld.vcData,'-') AS Company,                     
--               ADC.vcEmail AS vcEmail ,ADC.vcFirstName AS vcFirstName , ADC.vcLastName AS vcLastName ,adc.numDomainID AS numDomainID
--FROM dbo.AdditionalContactsInformation ADC INNER JOIN dbo.DivisionMaster DM 
--ON adc.numDivisionId = dm.numDivisionID INNER JOIN dbo.CompanyInfo C ON
--dm.numCompanyID = c.numCompanyId */
----LEFT JOIN dbo.ListDetails ld ON ld.numListItemID=numCompanyType
----WHERE ADC.vcEmail NOT IN (SELECT vcEmailId FROM dbo.EmailMaster)

----UNION 
----SELECT 0 AS numContactId,'' AS  CompanyName,'' AS Company ,vcEmailId AS vcEmail ,vcName AS vcFirstName,'' AS vcLastName, numDomainID FROM dbo.emailmaster 
----WHERE vcEmailId NOT IN (SELECT                 
----               ADC.vcEmail  
----FROM dbo.AdditionalContactsInformation ADC INNER JOIN dbo.DivisionMaster DM 
----ON adc.numDivisionId = dm.numDivisionID INNER JOIN dbo.CompanyInfo C ON
----dm.numCompanyID = c.numCompanyId LEFT JOIN dbo.ListDetails ld ON ld.numListItemID=numCompanyType)

--GO
--ALTER TABLE dbo.AdditionalContactsInformation
--	DROP CONSTRAINT DF_AdditionalContactsInformation_bitOptout
--GO
--ALTER TABLE dbo.AdditionalContactsInformation
--	DROP CONSTRAINT DF_AdditionalContactsInformation_numManagerID
--GO
--ALTER TABLE dbo.AdditionalContactsInformation
--	DROP CONSTRAINT DF_AdditionalContactsInformation_numEmpStatus
--GO
--CREATE TABLE dbo.Tmp_AdditionalContactsInformation
--	(
--	numContactId numeric(18, 0) NOT NULL IDENTITY (1, 1),
--	vcDepartment numeric(18, 0) NULL,
--	vcCategory numeric(18, 0) NULL,
--	vcGivenName varchar(100) NULL,
--	vcFirstName varchar(50) NULL,
--	vcLastName varchar(50) NULL,
--	numDivisionId numeric(18, 0) NULL,
--	numContactType numeric(18, 0) NULL,
--	numTeam numeric(18, 0) NULL,
--	numPhone varchar(15) NULL,
--	numPhoneExtension varchar(7) NULL,
--	numCell varchar(15) NULL,
--	numHomePhone varchar(15) NULL,
--	vcFax varchar(15) NULL,
--	vcEmail varchar(50) NULL,
--	VcAsstFirstName varchar(50) NULL,
--	vcAsstLastName varchar(50) NULL,
--	numAsstPhone varchar(15) NULL,
--	numAsstExtn varchar(6) NULL,
--	vcAsstEmail varchar(50) NULL,
--	charSex char(1) NULL,
--	bintDOB datetime NULL,
--	vcPosition numeric(18, 0) NULL,
--	txtNotes text NULL,
--	numCreatedBy numeric(18, 0) NULL,
--	bintCreatedDate datetime NULL,
--	numModifiedBy numeric(18, 0) NULL,
--	bintModifiedDate datetime NULL,
--	numDomainID numeric(18, 0) NOT NULL,
--	bitOptOut bit NULL,
--	numManagerID numeric(18, 0) NULL,
--	numRecOwner numeric(18, 0) NULL,
--	numEmpStatus bigint NULL,
--	vcTitle varchar(100) NULL,
--	vcAltEmail varchar(100) NULL,
--	vcItemId varchar(250) NULL,
--	vcChangeKey varchar(250) NULL,
--	numECampaignID numeric(18, 0) NULL,
--	bitPrimaryContact bit NULL
--	)  ON [PRIMARY]
--	 TEXTIMAGE_ON [PRIMARY]
--GO
--ALTER TABLE dbo.Tmp_AdditionalContactsInformation ADD CONSTRAINT
--	DF_AdditionalContactsInformation_bitOptout DEFAULT ((0)) FOR bitOptOut
--GO
--ALTER TABLE dbo.Tmp_AdditionalContactsInformation ADD CONSTRAINT
--	DF_AdditionalContactsInformation_numManagerID DEFAULT ((0)) FOR numManagerID
--GO
--ALTER TABLE dbo.Tmp_AdditionalContactsInformation ADD CONSTRAINT
--	DF_AdditionalContactsInformation_numEmpStatus DEFAULT ((2)) FOR numEmpStatus
--GO
--SET IDENTITY_INSERT dbo.Tmp_AdditionalContactsInformation ON
--GO
--IF EXISTS(SELECT * FROM dbo.AdditionalContactsInformation)
--	 EXEC('INSERT INTO dbo.Tmp_AdditionalContactsInformation (numContactId, vcDepartment, vcCategory, vcGivenName, vcFirstName, vcLastName, numDivisionId, numContactType, numTeam, numPhone, numPhoneExtension, numCell, numHomePhone, vcFax, vcEmail, VcAsstFirstName, vcAsstLastName, numAsstPhone, numAsstExtn, vcAsstEmail, charSex, bintDOB, vcPosition, txtNotes, numCreatedBy, bintCreatedDate, numModifiedBy, bintModifiedDate, numDomainID, bitOptOut, numManagerID, numRecOwner, numEmpStatus, vcTitle, vcAltEmail, vcItemId, vcChangeKey, numECampaignID, bitPrimaryContact)
--		SELECT numContactId, vcDepartment, vcCategory, vcGivenName, vcFirstName, vcLastName, numDivisionId, numContactType, numTeam, numPhone, numPhoneExtension, numCell, numHomePhone, vcFax, vcEmail, VcAsstFirstName, vcAsstLastName, numAsstPhone, numAsstExtn, vcAsstEmail, charSex, bintDOB, vcPosition, txtNotes, numCreatedBy, bintCreatedDate, numModifiedBy, bintModifiedDate, numDomainID, bitOptOut, numManagerID, numRecOwner, numEmpStatus, vcTitle, vcAltEmail, vcItemId, vcChangeKey, numECampaignID, bitPrimaryContact FROM dbo.AdditionalContactsInformation WITH (HOLDLOCK TABLOCKX)')
--GO
--SET IDENTITY_INSERT dbo.Tmp_AdditionalContactsInformation OFF
--GO
--ALTER TABLE dbo.CustomerCreditCardInfo
--	DROP CONSTRAINT FK_CustomerCreditCardInfo_AdditionalContactsInformation
--GO
--ALTER TABLE dbo.SiteSubscriberDetails
--	DROP CONSTRAINT FK_SiteSubscriberDetails_AdditionalContactsInformation
--GO
--DROP TABLE dbo.AdditionalContactsInformation
--GO
--EXECUTE sp_rename N'dbo.Tmp_AdditionalContactsInformation', N'AdditionalContactsInformation', 'OBJECT' 
--GO
--ALTER TABLE dbo.AdditionalContactsInformation ADD CONSTRAINT
--	PK_AdditionalContactsInformation PRIMARY KEY CLUSTERED 
--	(
--	numContactId
--	) WITH( PAD_INDEX = OFF, FILLFACTOR = 80, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

--GO
--CREATE NONCLUSTERED INDEX IX_AdditionalContactsInformation ON dbo.AdditionalContactsInformation
--	(
--	numDomainID
--	) WITH( PAD_INDEX = OFF, FILLFACTOR = 80, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
--GO
--COMMIT
--BEGIN TRANSACTION
--GO
--ALTER TABLE dbo.SiteSubscriberDetails ADD CONSTRAINT
--	FK_SiteSubscriberDetails_AdditionalContactsInformation FOREIGN KEY
--	(
--	numContactID
--	) REFERENCES dbo.AdditionalContactsInformation
--	(
--	numContactId
--	) ON UPDATE  NO ACTION 
--	 ON DELETE  NO ACTION 
	
--GO
--COMMIT
--BEGIN TRANSACTION
--GO
--ALTER TABLE dbo.CustomerCreditCardInfo ADD CONSTRAINT
--	FK_CustomerCreditCardInfo_AdditionalContactsInformation FOREIGN KEY
--	(
--	numContactId
--	) REFERENCES dbo.AdditionalContactsInformation
--	(
--	numContactId
--	) ON UPDATE  NO ACTION 
--	 ON DELETE  NO ACTION 
	
--GO
--COMMIT


------/******************************************************************
------Project: BACRMUI   Date: 20.05.2014
------Comments: Add "Default Expense" account to the Accounting section of the Organization. When selected, it would pre-select the expense account on new Bills.
------*******************************************************************/
--BEGIN TRANSACTION
--ALTER TABLE dbo.DivisionMaster ADD numDefaultExpenseAccountID NUMERIC(18,0) NULL

--UPDATE dbo.DivisionMaster SET numDefaultExpenseAccountID = 0 

--ROLLBACK

--/*
------/******************************************************************
------Project: Marketplace Service   Date: 23.04.2014
------Comments: Changes those needs to apply on Production Server for Install/Uninstall service
------*******************************************************************/



--Open Command Prompt and type commands as below :

--cd\
--cd\Windows\System32
--sc delete OnlineMarketplaceIntegratorService
--installutil /i "FilePath"

--*/

------/******************************************************************
------Project: BACRMUI   Date: 07.04.2014
------Comments: Add Field into Domain for Include/Exclude Tax & Shipping from Calculation of Sales Commission
------*******************************************************************/

--BEGIN TRANSACTION

--ALTER TABLE dbo.Domain ADD bitIncludeTaxAndShippingInCommission BIT

------------
--UPDATE dbo.Domain SET bitIncludeTaxAndShippingInCommission = 1 

---------------
--ALTER TABLE dbo.DivisionMaster ADD intShippingCompany INT

--ROLLBACK

------/******************************************************************
------Project: BACRMUI   Date: 14.Mar.2014
------Comments: Add new tab for Shipping/Packaging in Order/Opportunities
------*******************************************************************/

--BEGIN TRANSACTION

--ALTER TABLE dbo.DivisionMaster ADD vcShippersAccountNo VARCHAR(100)
--ALTER TABLE dbo.OpportunityMaster ADD bitUseShippersAccountNo BIT

--ROLLBACK

------/******************************************************************
------Project: BACRMUI   Date: 14.Mar.2014
------Comments: Add new tab for Shipping/Packaging in Order/Opportunities
------*******************************************************************/

--BEGIN TRANSACTION

--INSERT INTO CFW_Loc_Master
--                      (Loc_name, vcFieldType, vcCustomLookBackTableName)
--VALUES     ('Shipping & Packaging','O','CFW_Fld_Values_Opp')

--DECLARE @numDomainID AS NUMERIC(18)
--SELECT ROW_NUMBER() OVER (ORDER BY numDomainID)AS [RowID], numDomainID  INTO #tempDomains FROM dbo.Domain WHERE numDomainId > 0
--DECLARE @intCnt AS INT
--DECLARE @intCntDomain AS INT
--SET @intCnt = 0
--SET @intCntDomain = (SELECT COUNT(*) FROM #tempDomains)

--IF @intCntDomain > 0
--BEGIN
--WHILE(@intCnt < @intCntDomain)
--BEGIN

--SET @intCnt = @intCnt + 1
--SELECT @numDomainID = numDomainID FROM #tempDomains WHERE RowID = @intCnt
--PRINT @numDomainID
----SELECT * FROM #tempDomains
 
--exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=2,@TabName='Shipping & Packaging',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
--exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=6,@TabName='Shipping & Packaging',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
----DELETE FROM CFw_Grp_Master WHERE Grp_Name = 'Shipping & Packaging' AND numDomainID = @numDomainID
--END 

--END
--DROP TABLE #tempDomains


---- Add new column into domain for Default Rate Type setting
--ALTER TABLE Domain ADD bitDefaultRateType BIT NULL
 
--ROLLBACK

------/******************************************************************
------Project: Amazon Service   Date: 16.Feb.2014
------Comments: Add SKU for Matrix Item Support on Marketplace
------*******************************************************************/
--BEGIN TRANSACTION

--ALTER TABLE dbo.ItemAPI ADD vcSKU VARCHAR(100)

--ROLLBACK
----/******************************************************************
----Project: BizCart   Date: 12.Nov.2013
----Comments: Cart Item secondary sorting
----*******************************************************************/
----BEGIN TRANSACTION
----
-------SELECT * FROM DynamicFormMaster 
----INSERT dbo.DynamicFormMaster 
----(numFormId,vcFormName,cCustomFieldsAssociated,cAOIAssociated,bitDeleted,tintFlag,bitWorkFlow,vcLocationID,bitAllowGridColor )
----SELECT 84 ,'Secondary Sort Order For Cart Item', 'Y', 'N',0,0, NULL,'5', NULL 
----
----INSERT INTO dbo.DycFormField_Mapping 
----	(numModuleID,numFieldID,numDomainID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,[order],
----	 tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,
----	 bitAllowFiltering,bitRequired,numFormFieldID,intSectionID,bitAllowGridColor)
----SELECT numModuleID,numFieldID,numDomainID,84,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,1,
----	 tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,1,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,0,bitExport,
----	 1,bitRequired,numFormFieldID,intSectionID,bitAllowGridColor 
----	 FROM dbo.DycFormField_Mapping 
----	 WHERE numFormID = 20 
----	 AND vcFieldName IN ('Item Name','Model ID','Manufacturer')	
----
----UPDATE DynamicFormMaster SET vcFormName = 'Secondary Sort Order For Cart Item' WHERE vcFormName = 'Sort Order For Cart Item'
----
------ ADD new column for enabling Secondary sorting
----ALTER TABLE EcommerceDTL ADD bitEnableSecSorting BIT NULL
----
----SELECT * FROM DynamicFormMaster 
----INSERT dbo.DynamicFormMaster 
----(numFormId,vcFormName,cCustomFieldsAssociated,cAOIAssociated,bitDeleted,tintFlag,bitWorkFlow,vcLocationID,bitAllowGridColor )
----SELECT 85 ,'Priamry Sort Order For Cart Item', 'Y', 'N',0,0, NULL,'5', NULL 
----
----INSERT INTO dbo.DycFormField_Mapping 
----	(numModuleID,numFieldID,numDomainID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,[order],
----	 tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,
----	 bitAllowFiltering,bitRequired,numFormFieldID,intSectionID,bitAllowGridColor)
----SELECT numModuleID,numFieldID,numDomainID,85,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,1,
----	 tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,1,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,0,bitExport,
----	 1,bitRequired,numFormFieldID,intSectionID,bitAllowGridColor 
----	 FROM dbo.DycFormField_Mapping 
----	 WHERE numFormID = 20 
----	 AND vcFieldName IN ('Item Name','Model ID','Manufacturer')	
-------SELECT * FROM dbo.DycFormField_Mapping WHERE numFormID = 20 AND vcAssociatedControlType = 'TextBox'
-------DELETE FROM dbo.DycFormField_Mapping WHERE numFormID = 84
-------SELECT * FROM DycFieldMaster WHERE numFieldID IN (SELECT numFieldID FROM dbo.DycFormField_Mapping WHERE numFormID = 84)
------ SELECT * FROM EcommerceDTL
----
----ROLLBACK
----
----/******************************************************************
----Project: BACRM / PORTAL   Date: 28.Oct.2013
----Comments: Add field to Use Bizdoc amount into Total Insured Value & Total Customs Value
----*******************************************************************/
----BEGIN TRANSACTION
----ALTER TABLE Domain ADD bitUseBizdocAmount BIT
----ROLLBACK
----
----/******************************************************************
----Project: BACRM / PORTAL   Date: 26.Oct.2013
----Comments: Remove 2 forms from grid column config / Change in column configuration
----*******************************************************************/
----BEGIN TRANSACTION
----UPDATE DynamicFormMaster SET bitDeleted = 1 WHERE vcFormName IN ('Knowledge Base (Portal)','Documents (Portal)') 
----
----SELECT * FROM dycFormField_Mapping WHERE numFormID = 82  AND vcAssociatedControlType = 'Popup'
----UPDATE  dycFormField_Mapping SET bitSettingField = 0 WHERE numFormID = 82  AND vcAssociatedControlType = 'Popup'
----
----UPDATE  dycFormField_Mapping SET bitSettingField = 0 WHERE numFormID = 78 AND vcFieldName IN ('Active','Billing Address','Linked Items','Tracking No','Shipping Address','Contact','Deal Status','Documents','Recurring Template','Share With')
----
----UPDATE  dycFormField_Mapping SET bitSettingField = 0 WHERE numFormID = 79 AND vcFieldName IN ('Active','Billing Address','Linked Items','Tracking No','Shipping Address','Contact','Deal Status','Documents','Recurring Template','Share With')
----
----SELECT * FROM dycFormField_Mapping WHERE numFormID = 80  AND vcAssociatedControlType = 'Popup'
----UPDATE  dycFormField_Mapping SET bitSettingField = 0 WHERE numFormID = 80  AND vcAssociatedControlType = 'Popup'
----
----UPDATE  dycFormField_Mapping SET bitSettingField = 0 
----WHERE numFormID = 80 
---- AND vcFieldName IN ('Email','Phone','')
----ROLLBACK
----
----/******************************************************************
----Project: BACRM / PORTAL   Date: 17.Oct.2013
----Comments: Add New Field to Hide Tabs from Portal
----*******************************************************************/
----BEGIN TRANSACTION
---- ALTER TABLE DOmain ADD vcHideTabs VARCHAR(1000)
----ROLLBACK
----
----/******************************************************************
----Project: PORTAL   Date: 11.Oct.2013
----Comments: Added 3 new fields for Item List
----*******************************************************************/
----BEGIN TRANSACTION
----
----INSERT INTO dbo.EmailMergeFields (vcMergeField,vcMergeFieldValue,numModuleID,tintModuleType)
----SELECT 'ModelID','##ModelID##',2,1
----UNION 
----SELECT 'FirstPriceLevelValue','##FirstPriceLevelValue##',2,1
----UNION 
----SELECT 'FirstPriceLevelDiscount','##FirstPriceLevelDiscount##',2,1
----	
----ROLLBACK
----
----/******************************************************************
----Project: PORTAL   Date: 07.Oct.2013
----Comments: Removed Default PRM from AuthenticationGroupMaster
----*******************************************************************/
----BEGIN TRANSACTION
----DELETE FROM AuthenticationGroupMaster WHERE tintGroupType = 3
----ROLLBACK
----
----
----/******************************************************************
----Project: PORTAL   Date: 30.Sep.2013
----Comments: Portal Column settings, adding form ids, form fields 
----*******************************************************************/
----BEGIN TRANSACTION
----ALTER TABLE dbo.eCommerceDTL ADD bitShowPriceUsingPriceLevel BIT
----ROLLBACK
----
----/******************************************************************
----Project: PORTAL   Date: 30.Sep.2013
----Comments: Portal Column settings, adding form ids, form fields 
----*******************************************************************/
----BEGIN TRANSACTION
----
----SELECT * FROM DynamicFormMaster 
----INSERT dbo.DynamicFormMaster 
----(numFormId,vcFormName,cCustomFieldsAssociated,cAOIAssociated,bitDeleted,tintFlag,bitWorkFlow,vcLocationID,bitAllowGridColor )
----SELECT 78 ,'Sales Order (Portal)', 'Y', 'N',0,0, NULL,'5', NULL 
----UNION ALL
----SELECT 79 ,'Purchase Order (Portal)', 'Y', 'N',0,0, NULL,'5', NULL 
----UNION ALL
----SELECT 80 ,'Cases (Portal)', 'Y', 'N',0,0, NULL,'5', NULL 
----UNION ALL
----SELECT 81 ,'Knowledge Base (Portal)', 'Y', 'N',0,0, NULL,'5', NULL 
----UNION ALL
----SELECT 82 ,'Projects (Portal)', 'Y', 'N',0,0, NULL,'5', NULL 
----UNION ALL
----SELECT 83 ,'Documents (Portal)', 'Y', 'N',0,0, NULL,'5', NULL 
-------------------------------
----
----SELECT * FROM dbo.DycFormField_Mapping WHERE numFormID = 39  -- Sales Order
----SELECT * FROM dbo.DycFormField_Mapping WHERE numFormID = 41  -- Purchase Order
----SELECT * FROM dbo.DycFormField_Mapping WHERE numFormID = 72  -- Cases
------SELECT * FROM dbo.DycFormField_Mapping WHERE numFormID = 
----SELECT * FROM dbo.DycFormField_Mapping WHERE numFormID = 73  -- Projects
------SELECT * FROM dbo.DycFormField_Mapping WHERE numFormID = 
----
----INSERT INTO dbo.DycFormField_Mapping 
----	(numModuleID,numFieldID,numDomainID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,[order],
----	 tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,
----	 bitAllowFiltering,bitRequired,numFormFieldID,intSectionID,bitAllowGridColor)
----SELECT numModuleID,numFieldID,numDomainID,78,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,1,
----	 tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,1,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,0,bitExport,
----	 1,bitRequired,numFormFieldID,intSectionID,bitAllowGridColor 
----	 FROM dbo.DycFormField_Mapping WHERE numFormID = 39 AND bitSettingField = 1
----
----UNION ALL 
----
----SELECT numModuleID,numFieldID,numDomainID,79,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,1,
----	 tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,1,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,0,bitExport,
----	 1,bitRequired,numFormFieldID,intSectionID,bitAllowGridColor 
----	 FROM dbo.DycFormField_Mapping WHERE numFormID = 41 AND bitSettingField = 1
----
----UNION ALL 
----
----SELECT numModuleID,numFieldID,numDomainID,80,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,1,
----	 tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,1,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,0,bitExport,
----	 1,bitRequired,numFormFieldID,intSectionID,bitAllowGridColor 
----	 FROM dbo.DycFormField_Mapping WHERE numFormID = 72 AND vcFieldName <> 'Contact' --AND bitSettingField = 1
----
----UNION ALL 
----
----SELECT numModuleID,numFieldID,numDomainID,82,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,1,
----	 tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,1,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,0,bitExport,
----	 1,bitRequired,numFormFieldID,intSectionID,bitAllowGridColor 
----	 FROM dbo.DycFormField_Mapping WHERE numFormID = 73 --AND bitSettingField = 1
----	 	 
----ROLLBACK
----
----
----/******************************************************************
----Project: BACRMUI   Date: 20.Sep.2013
----Comments: Please go to Manage Authorization, and pull up the Opportunity/Orders module. In that list, 
----		  you will see "Sales Opportunity Details", which needs to be relabeld to "Sales Opportunity/Order Details" 
----		  there is also one called "Purchase Opportunity Details" and that one must be relabeled "Purchase Opportunity/Order Details" 
----*******************************************************************/
----BEGIN TRANSACTION
------SELECT * FROM PageMaster WHERE numModuleID = 10 AND vcPageDesc = 'Sales Opportunity Details'
------SELECT * FROM PageMaster WHERE numModuleID = 10 AND vcPageDesc = 'Purchase Opportunity Details'
----UPDATE PageMaster SET vcPageDesc = 'Sales Opportunity/Order Details' WHERE numModuleID = 10 AND vcPageDesc = 'Sales Opportunity Details'
----UPDATE PageMaster SET vcPageDesc = 'Purchase Opportunity/Order Details' WHERE numModuleID = 10 AND vcPageDesc = 'Purchase Opportunity Details'
----
----ROLLBACK
----
----/******************************************************************
----Project: BACRMUI   Date: 16.Sep.2013
----Comments: Add New Page Element
----*******************************************************************/
----
----BEGIN TRANSACTION
----
----SELECT * FROM dbo.PageElementMaster 
----INSERT INTO dbo.PageElementMaster 
----(numElementID,vcElementName,vcUserControlPath,vcTagName,bitCustomization,bitAdd,bitDelete)
----SELECT 43,'CartItemFilters','~/UserControls/CartItemFilter.ascx','{#CartItemFilter#}',1,NULL,NULL
------------------------------------
----INSERT INTO dbo.EmailMergeModule (numModuleID,vcModuleName,tintModuleType) 
----SELECT 43,'CartItemFilters',1
------------------------------------
----SELECT * FROM dbo.EmailMergeFields
----INSERT INTO dbo.EmailMergeFields (vcMergeField,vcMergeFieldValue,numModuleID,tintModuleType)
----SELECT 'FilterName','##FilterName##',43,1
----UNION 
----SELECT 'FilterControl','##FilterControl##',43,1
------------------------------------
----INSERT INTO dbo.PageElementDetail (numElementID,numAttributeID,vcAttributeValue,numSiteID,numDomainID,vcHtml)
----SELECT 43,96,'',91,172,''
------------------------------------
----INSERT INTO dbo.PageElementAttributes (numElementID,vcAttributeName,vcControlType,vcControlValues,bitEditor) 
----SELECT 43,'Html Customize','HtmlEditor','',1
------------------------------------
----
----ROLLBACK
----
----
----/******************************************************************
----Project: BACRMUI   Date: 10.Sep.2013
----Comments: Add New Payment Method
----*******************************************************************/
----BEGIN TRANSACTION
----
----GO
----/****** Object:  Table [dbo].[DycCartFilters]    Script Date: 09/12/2013 18:56:16 ******/
----SET ANSI_NULLS ON
----GO
----SET QUOTED_IDENTIFIER ON
----GO
----CREATE TABLE [dbo].[DycCartFilters](
----	[numFieldID] [numeric](18, 0) NOT NULL,
----	[numFormID] [numeric](18, 0) NOT NULL,
----	[numSiteID] [numeric](18, 0) NOT NULL,
----	[numDomainID] [numeric](18, 0) NULL,
----	[numFilterType] [int] NOT NULL,
----	[bitCustomField] [bit] NOT NULL,
----	[tintOrder] [tinyint] NULL,
---- CONSTRAINT [PK_DycCartFilters] PRIMARY KEY CLUSTERED 
----(
----	[numFieldID] ASC,
----	[numFormID] ASC,
----	[numSiteID] ASC
----)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
----) ON [PRIMARY]
----
----GO
----EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1 = Checkbox' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'DycCartFilters', @level2type=N'COLUMN', @level2name=N'numFilterType'
----
------------------------------
------SELECT * FROM DynamicFormMaster
----INSERT dbo.DynamicFormMaster 
----(numFormId,vcFormName,cCustomFieldsAssociated,cAOIAssociated,bitDeleted,tintFlag,bitWorkFlow,vcLocationID,bitAllowGridColor )
----SELECT 77 ,'Cart Item Filter Fields', 'Y', 'N',0,0, NULL,'5', NULL 
------------------------------
----
----INSERT INTO dbo.DycFormField_Mapping 
----	(numModuleID,numFieldID,numDomainID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,[order],
----	 tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,
----	 bitAllowFiltering,bitRequired,numFormFieldID,intSectionID,bitAllowGridColor)
----SELECT numModuleID,numFieldID,numDomainID,77,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,1,
----	 tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,1,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,0,bitExport,
----	 1,bitRequired,numFormFieldID,intSectionID,bitAllowGridColor 
----	 FROM dbo.DycFormField_Mapping WHERE numFormID = 20
----	 AND vcFieldName IN ('Item Classification','Item Group','Item Class','Item Category')
----
----ROLLBACK
----
----/******************************************************************
----Project: BACRMUI   Date: 10.Sep.2013
----Comments: Add New Payment Method
----*******************************************************************/
----BEGIN TRANSACTION
----
----SET IDENTITY_INSERT dbo.ListDetails ON
----
----INSERT INTO dbo.ListDetails 
----(numListItemID,numListID,vcData,numCreatedBY,bintCreatedDate,numModifiedBy,bintModifiedDate,bitDelete,numDomainID,constFlag,sintOrder)
----SELECT 84,31,	'Sales Inquiry',	1,	NULL,	NULL,	NULL,	0,	1,	1,	0
----
----SET IDENTITY_INSERT dbo.ListDetails OFF
----ROLLBACK
----
----
----/******************************************************************
----Project: BACRMUI   Date: 29.Aug.2013
----Comments: Add Ecommerce settings
----*******************************************************************/
----BEGIN TRANSACTION
----ALTER TABLE eCommerceDTL ADD bitSkipStep2 BIT,bitDisplayCategory BIT
----ROLLBACK
----
----/******************************************************************
----Project: BACRMUI   Date: 15.Aug.2013
----Comments: Add Shipping Label Image Width,Height fields in domain details
----*******************************************************************/
----BEGIN TRANSACTION
----ALTER TABLE Domain ADD intShippingImageWidth INT,intShippingImageHeight INT
----
----ALTER TABLE Domain ADD numTotalInsuredValue NUMERIC(10,2),numTotalCustomsValue NUMERIC(10,2)
----
----UPDATE dynamicFormMaster SET vcLocationID='5' WHERE numFormId=67
----ROLLBACK
----
----/******************************************************************
----Project: BACRMUI   Date: 14.Aug.2013
----Comments: Adding USPS Packages
----*******************************************************************/
----BEGIN TRANSACTION
----INSERT INTO dbo.CustomPackages 
----(numPackageTypeID,vcPackageName,fltWidth,fltHeight,fltLength,fltTotalWeight,numShippingCompanyID)
---- SELECT 0,'None',0,0,0,0,90
---- UNION ALL
---- SELECT 1,'Postcards',0,0,0,0,90
---- UNION ALL
---- SELECT 3,'Large Envelope',0,0,0,0,90
---- UNION ALL
---- SELECT 4,'Flat Rate Envelope',0,0,0,0,90
---- UNION ALL
---- SELECT 12,'Flat Rate Box',0,0,0,0,90
---- UNION ALL
---- SELECT 13,'Small Flat Rate Box',0,0,0,0,90
---- UNION ALL
---- SELECT 14,'Large Flat Rate Box',0,0,0,0,90
---- UNION ALL
---- SELECT 15,'Rectangular',0,0,0,0,90
---- UNION ALL
---- SELECT 16,'Non Rectangular',0,0,0,0,90
---- UNION ALL
---- SELECT 19,'Matter For The Blind',0,0,0,0,90
---- 
----ROLLBACK	
----
----/******************************************************************
----Project: BACRMUI   Date: 12.Aug.2013
----Comments: Add Customs Amount field in Shipping report
----*******************************************************************/
----BEGIN TRANSACTION
----ALTER TABLE ShippingReport ADD numTotalCustomsValue NUMERIC(18,2)
----ROLLBACK
----
----/******************************************************************
----Project: BACRMUI   Date: 8.Aug.2013
----Comments: Add Shipping States for Canada
----*******************************************************************/
----BEGIN TRANSACTION
----INSERT INTO dbo.ShippingStateMaster 
----(vcStateName,vcStateCode,numShipCompany) 
----SELECT 'Alberta','AB',91
----UNION ALL
----SELECT 'British Columbia','BC',91
----UNION ALL
----SELECT 'Manitoba','MB',91
----UNION ALL
----SELECT 'New Brunswick','NB',91
----UNION ALL
----SELECT 'New Foundland','NF',91
----UNION ALL
----SELECT 'Northwest Territories','NT',91
----UNION ALL
----SELECT 'Nova Scotia','NS',91
----UNION ALL
----SELECT 'Ontario','ON',91
----UNION ALL
----SELECT 'Prince Edward Island','PE',91
----UNION ALL
----SELECT 'Quebec','PQ',91
----UNION ALL
----SELECT 'Saskatchewan','SK',91
----UNION ALL
----SELECT 'Yukon Territories','YT',91
----UNION ALL
----SELECT 'Alberta','AB',90
----UNION ALL
----SELECT 'British Columbia','BC',90
----UNION ALL
----SELECT 'Manitoba','MB',90
----UNION ALL
----SELECT 'New Brunswick','NB',90
----UNION ALL
----SELECT 'New Foundland','NF',90
----UNION ALL
----SELECT 'Northwest Territories','NT',90
----UNION ALL
----SELECT 'Nova Scotia','NS',90
----UNION ALL
----SELECT 'Ontario','ON',90
----UNION ALL
----SELECT 'Prince Edward Island','PE',90
----UNION ALL
----SELECT 'Quebec','PQ',90
----UNION ALL
----SELECT 'Saskatchewan','SK',90
----UNION ALL
----SELECT 'Yukon Territories','YT',90
----UNION ALL
----SELECT 'Alberta','AB',88
----UNION ALL
----SELECT 'British Columbia','BC',88
----UNION ALL
----SELECT 'Manitoba','MB',88
----UNION ALL
----SELECT 'New Brunswick','NB',88
----UNION ALL
----SELECT 'New Foundland','NF',88
----UNION ALL
----SELECT 'Northwest Territories','NT',88
----UNION ALL
----SELECT 'Nova Scotia','NS',88
----UNION ALL
----SELECT 'Ontario','ON',88
----UNION ALL
----SELECT 'Prince Edward Island','PE',88
----UNION ALL
----SELECT 'Quebec','PQ',88
----UNION ALL
----SELECT 'Saskatchewan','SK',88
----UNION ALL
----SELECT 'Yukon Territories','YT',88
----
----ROLLBACK
----
----
----/******************************************************************
----Project: BACRMUI   Date: 7.Aug.2013
----Comments: Add Item Barcode Fields
----*******************************************************************/
----BEGIN TRANSACTION
----	INSERT dbo.DynamicFormMaster 
----	( numFormId,vcFormName,cCustomFieldsAssociated,cAOIAssociated,bitDeleted,tintFlag,bitWorkFlow,vcLocationID,bitAllowGridColor )
----	VALUES 	( /* numFormId - numeric(10, 0) */ 67 , /* vcFormName - nvarchar(50) */ N'Item Barcode Fields', /* cCustomFieldsAssociated - char(1) */ 'Y',
----	  /* cAOIAssociated - char(1) */ 'N', /* bitDeleted - bit */ 0, /* tintFlag - tinyint */ 0, /* bitWorkFlow - bit */ NULL, 
----	  /* vcLocationID - varchar(50) */ '5', /* bitAllowGridColor - bit */ NULL ) 
----	  
------- exec usp_getDynamicFormList @numDomainID=1,@tintFlag=0,@bitWorkFlow=0,@bitAllowGridColor=0
----		
----	INSERT INTO dbo.DycFormField_Mapping 
----	(numModuleID,numFieldID,numDomainID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,[order],
----	 tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,
----	 bitAllowFiltering,bitRequired,numFormFieldID,intSectionID,bitAllowGridColor)
----	 SELECT numModuleID,numFieldID,numDomainID,67,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,1,
----	 tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,1,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,0,bitExport,
----	 1,bitRequired,numFormFieldID,intSectionID,bitAllowGridColor 
----	 FROM dbo.DycFormField_Mapping WHERE numFormID = 29
----	 AND vcFieldName NOT IN ('Serial Number','')
----	 UNION
----	 SELECT numModuleID,numFieldID,numDomainID,67,bitAllowEdit,bitInlineEdit,'Item ID',vcAssociatedControlType,vcPropertyName,PopupFunctionName,1,
----	 tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,1,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,0,bitExport,
----	 1,bitRequired,numFormFieldID,intSectionID,bitAllowGridColor 
----	 FROM dbo.DycFormField_Mapping WHERE numFormID = 65 AND vcFieldName = 'Item Code'
----	 
----	 UPDATE dynamicFormMaster SET vcLocationID='5' WHERE numFormId=67
----	 --SELECT * FROM dbo.DycFormField_Mapping WHERE numFormID = 67
----	 --DELETE FROM dbo.DycFormField_Mapping WHERE numFormID = 67
----ROLLBACK
----
----/******************************************************************
----Project: BACRMUI   Date: 6.Aug.2013
----Comments: Add Item ID in Products & Services 
----*******************************************************************/
----BEGIN TRANSACTION
----
----INSERT INTO dbo.DycFormField_Mapping 
----(numModuleID,numFieldID,numDomainID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,[order],
---- tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,
---- bitAllowFiltering,bitRequired,numFormFieldID,intSectionID,bitAllowGridColor)
----SELECT numModuleID,numFieldID,numDomainID,26,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,[order],
----	   tintRow,tintColumn,bitInResults,0,bitDefault,1,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,
----	   bitAllowFiltering,bitRequired,numFormFieldID,intSectionID,bitAllowGridColor
----FROM dbo.DycFormField_Mapping WHERE numFormID = 20 AND vcFieldName = 'Item ID'	   
---- 
----exec USP_GetColumnConfiguration1 @numDomainID=1,@numUserCntId=1,@FormId=26,@numtype=0,@numViewID=0
----
----ROLLBACK 
----
----/******************************************************************
----Project: BACRMUI   Date: 18.Jul.2013
----Comments: Add Vendor Minimum Qty for Mass Update
----*******************************************************************/
----BEGIN TRANSACTION
----
------SELECT * FROM dbo.DycFormField_Mapping WHERE vcFieldName = 'Vendor Minimum Qty' AND numFormID = 27
------SELECT * FROM dbo.DycFormField_Mapping WHERE numFormID = 29 AND vcFieldName = 'Vendor'
----
----INSERT INTO dbo.DycFormField_Mapping 
----(numModuleID,numFieldID,numDomainID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,[order],
---- tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,
---- bitAllowFiltering,bitRequired,numFormFieldID,intSectionID,bitAllowGridColor)
----SELECT numModuleID,numFieldID,numDomainID,29,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,[order],
---- tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,
---- bitAllowFiltering,bitRequired,numFormFieldID,intSectionID,bitAllowGridColor
----FROM dbo.DycFormField_Mapping WHERE vcFieldName = 'Vendor Minimum Qty' AND numFormID = 27
---- 
----exec usp_GetUpdatableFieldList @numDomainID=1,@numFormID=29
----ROLLBACK 
----/******************************************************************
----Project: BACRMUI   Date: 12.Jul.2013
----Comments: Add Item Price Level for ItemList
----*******************************************************************/
----BEGIN TRANSACTION
----
----INSERT INTO dbo.DycFormField_Mapping 
----(numModuleID,numFieldID,numDomainID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,[order],
---- tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,
---- bitAllowFiltering,bitRequired,numFormFieldID,intSectionID,bitAllowGridColor)
----SELECT numModuleID,numFieldID,numDomainID,21,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,[order],
----	   tintRow,tintColumn,bitInResults,0,bitDefault,1,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,
----	   bitAllowFiltering,bitRequired,numFormFieldID,intSectionID,bitAllowGridColor
----FROM dbo.DycFormField_Mapping WHERE numFormID = 20 AND vcFieldName = 'Price Level'	   
---- 
----exec USP_GetColumnConfiguration1 @numDomainID=1,@numUserCntId=1,@FormId=21,@numtype=0,@numViewID=0
------ exec usp_getFormConfigFields @numDomainId=1,@numFormId=29,@numAuthGroupId=1,@numBizDocTemplateID=0
----
----ROLLBACK 
----
----/******************************************************************
----Project: BACRMUI   Date: 10.Jul.2013
----Comments: Add Documents into Case 
----*******************************************************************/
----BEGIN TRANSACTION
----
----INSERT INTO dbo.DycFieldMaster 
----(numModuleID,numDomainID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,
----vcToolTip,vcListItemType,numListID,PopupFunctionName,[order],tintRow,tintColumn,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,
----bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,bitAllowFiltering,bitInlineEdit,bitRequired,intColumnWidth,intFieldMaxLength) 
----SELECT 4,NULL,'Documents','DocumentCount','DocumentCount','DocCount','Cases','V','R','Popup',
----NULL,'',0,'OpenDocuments',	9,	3,	1,	1,	0,	0,	0,	0,	NULL,	
----1,	0,	1,	NULL,	NULL,	NULL,	0,	NULL,	NULL,	NULL
----
----DECLARE @numFieldID AS BIGINT
----SELECT @numFieldID = numFieldId FROM dbo.DycFieldMaster WHERE vcFieldName = 'Documents' AND vcDbColumnName = 'DocumentCount' AND vcLookBackTableName = 'Cases'
----
----INSERT INTO dbo.DycFormField_Mapping 
----(numModuleID,numFieldID,numDomainID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,[order],
---- tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,
---- bitAllowFiltering,bitRequired,numFormFieldID,intSectionID,bitAllowGridColor)
----SELECT 7,@numFieldID,NULL,12,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,[order],
----	   tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,1,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,
----	   bitAllowFiltering,bitRequired,0,1,NULL
----FROM dbo.DycFieldMaster WHERE vcFieldName = 'Documents' AND vcDbColumnName = 'DocumentCount' AND vcLookBackTableName = 'Cases'
----
----
----SELECT * FROM dbo.DycFieldMaster WHERE vcFieldName = 'Documents' AND vcDbColumnName = 'DocumentCount' AND vcLookBackTableName = 'Cases'
------DELETE FROM dbo.DycFieldMaster WHERE vcFieldName = 'Documents' AND vcDbColumnName = 'DocumentCount' AND vcLookBackTableName = 'Cases'
----
----SELECT * FROM dbo.DycFormField_Mapping WHERE vcFieldName = 'Documents' AND numFormID = 26
------DELETE FROM dbo.DycFormField_Mapping WHERE vcFieldName = 'Documents' AND numFormID = 26
----
----ROLLBACK 
----
----/******************************************************************
----Project: BACRMUI   Date: 5.Jul.2013
----Comments: Add Description for New Case in Administration->Field Management
----*******************************************************************/
----BEGIN TRANSACTION
----
----UPDATE dbo.DycFormField_Mapping SET bitAllowEdit = 1,bitInlineEdit = 1 ,bitAddField = 1, bitDetailField = 1,bitDefault = 1 WHERE numFormID = 12 AND  vcFieldName  = 'Description'
---- 
----exec usp_GetLayoutInfoDdl @numUserCntId=0,@intcoulmn=0,@numDomainID=1,@PageId=3,@numRelation=0,@numFormID=12,@tintPageType=2
----
----ROLLBACK 
----/******************************************************************
----Project: BACRMUI   Date: 2.Jul.2013
----Comments: Add AssignTo for New Case in Administration->Field Management
----*******************************************************************/
----BEGIN TRANSACTION
----
----UPDATE dbo.DycFormField_Mapping SET bitAddField = 1, bitDetailField = 1 WHERE numFormID = 12 AND  vcFieldName  = 'Assigned To'
---- 
----exec usp_GetLayoutInfoDdl @numUserCntId=0,@intcoulmn=0,@numDomainID=1,@PageId=3,@numRelation=0,@numFormID=12,@tintPageType=2
----
----ROLLBACK 
----/******************************************************************
----Project: BACRMUI   Date: 2.Jul.2013
----Comments: Add Item ID for Item Column search
----*******************************************************************/
----BEGIN TRANSACTION
----
----INSERT INTO dbo.DycFormField_Mapping 
----(numModuleID,numFieldID,numDomainID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,[order],
---- tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,
---- bitAllowFiltering,bitRequired,numFormFieldID,intSectionID,bitAllowGridColor)
----SELECT numModuleID,numFieldID,numDomainID,22,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,[order],
----	   tintRow,tintColumn,bitInResults,0,bitDefault,1,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,
----	   bitAllowFiltering,bitRequired,numFormFieldID,intSectionID,bitAllowGridColor
----FROM dbo.DycFormField_Mapping WHERE numFormID = 20 AND vcFieldName = 'Item ID'	   
---- 
----exec USP_GetColumnConfiguration1 @numDomainID=1,@numUserCntId=1,@FormId=22,@numtype=1,@numViewID=0
----
----ROLLBACK 
----
----/******************************************************************
----Project: BACRMUI   Date: 1.Jul.2013
----Comments: . In case details "Description" field is missing from the "Available Fields" list (and from initial list grid). Add it. 
----		    Also, "InternalComments" field should be re labeled to "Internal Comments". The space is needed
----*******************************************************************/
----BEGIN TRANSACTION
------SELECT * FROM dbo.DycFormField_Mapping WHERE numFormID = 12 AND vcFieldName = 'Description'
----
----UPDATE dbo.DycFormField_Mapping SET bitDetailField = 1
----WHERE numFormID = 12 AND vcFieldName = 'Description'
----
----UPDATE dbo.DycFormField_Mapping SET vcFieldName = 'Internal Comments'
----WHERE numFormID = 12 AND vcFieldName = 'InternalComments'
----
----exec usp_GetLayoutInfoDdl @numUserCntId=1,@intcoulmn=0,@numDomainID=1,@PageId=3,@numRelation=0,@numFormID=12,@tintPageType=3
----ROLLBACK
----
----/******************************************************************
----Project: BACRMUI   Date: 27.Jun.2013
----Comments: Add ItemID in Available field for Bizdocs
----*******************************************************************/
----BEGIN TRANSACTION
----INSERT INTO dbo.DycFormField_Mapping (
----	numModuleID,
----	numFieldID,
----	numDomainID,
----	numFormID,
----	bitAllowEdit,
----	bitInlineEdit,
----	vcFieldName,
----	vcAssociatedControlType,
----	vcPropertyName,
----	PopupFunctionName,
----	[order],
----	tintRow,
----	tintColumn,
----	bitInResults,
----	bitDeleted,
----	bitDefault,
----	bitSettingField,
----	bitAddField,
----	bitDetailField,
----	bitAllowSorting,
----	bitWorkFlowField,
----	bitImport,
----	bitExport,
----	bitAllowFiltering,
----	bitRequired,
----	numFormFieldID,
----	intSectionID,
----	bitAllowGridColor 
----)
----SELECT numModuleID,
----		numFieldID,
----		numDomainID,
----		7,
----		bitAllowEdit,
----		bitInlineEdit,
----		vcFieldName,
----		vcAssociatedControlType,
----		vcPropertyName,
----		PopupFunctionName,
----		[order],
----		tintRow,
----		tintColumn,
----		bitInResults,
----		bitDeleted,
----		bitDefault,
----		bitSettingField,
----		bitAddField,
----		bitDetailField,
----		bitAllowSorting,
----		bitWorkFlowField,
----		bitImport,
----		bitExport,
----		bitAllowFiltering,
----		bitRequired,
----		NULL,
----		NULL,
----		0 FROM dbo.DycFieldMaster  WHERE vcDbColumnName = 'numItemCode' AND numFieldId = 211
----UNION 
----SELECT numModuleID,
----		numFieldID,
----		numDomainID,
----		8,
----		bitAllowEdit,
----		bitInlineEdit,
----		vcFieldName,
----		vcAssociatedControlType,
----		vcPropertyName,
----		PopupFunctionName,
----		[order],
----		tintRow,
----		tintColumn,
----		bitInResults,
----		bitDeleted,
----		bitDefault,
----		bitSettingField,
----		bitAddField,
----		bitDetailField,
----		bitAllowSorting,
----		bitWorkFlowField,
----		bitImport,
----		bitExport,
----		bitAllowFiltering,
----		bitRequired,
----		NULL,
----		NULL,
----		0 FROM dbo.DycFieldMaster  WHERE vcDbColumnName = 'numItemCode' AND numFieldId = 211	 	
----ROLLBACK
----
----/******************************************************************
----Project: BACRMUI   Date: 26.Jun.2013
----Comments: Add Item Thumbnail Image for ItemList
----*******************************************************************/
----BEGIN TRANSACTION
----
----INSERT INTO dbo.DycFormField_Mapping 
----(numModuleID,numFieldID,numDomainID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,[order],
---- tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,
---- bitAllowFiltering,bitRequired,numFormFieldID,intSectionID,bitAllowGridColor)
----SELECT numModuleID,numFieldID,numDomainID,21,bitAllowEdit,bitInlineEdit,'Item Image',vcAssociatedControlType,vcPropertyName,PopupFunctionName,[order],
----	   tintRow,tintColumn,bitInResults,0,bitDefault,1,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,
----	   bitAllowFiltering,bitRequired,numFormFieldID,intSectionID,bitAllowGridColor
----FROM dbo.DycFormField_Mapping WHERE numFormID = 20 AND vcFieldName = 'Item Thumbnail Iamge'	   
---- 
----exec USP_GetColumnConfiguration1 @numDomainID=1,@numUserCntId=1,@FormId=21,@numtype=0,@numViewID=0
------ exec usp_getFormConfigFields @numDomainId=1,@numFormId=29,@numAuthGroupId=1,@numBizDocTemplateID=0
----
----ROLLBACK 
----
----/******************************************************************
----Project: BACRMUI   Date: 20.Jun.2013
----Comments: Add Received Qty in Products & Services for PO only
----*******************************************************************/
----BEGIN TRANSACTION
----
----INSERT INTO dbo.DycFieldMaster 
----(numModuleID,numDomainID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,
----vcToolTip,vcListItemType,numListID,PopupFunctionName,[order],tintRow,tintColumn,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,
----bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,bitAllowFiltering,bitInlineEdit,bitRequired,intColumnWidth,intFieldMaxLength) 
----SELECT 4,NULL,'Received','numUnitHourReceived','numUnitHourReceived','QtyReceived','OpportunityItems','N','R','TextBox',
----'Received Quantity','',0,NULL,	62,	0,	1,	1,	0,	1,	0,	1,	1,	
----1,	1,	1,	1,	NULL,	1,	NULL,	NULL,	NULL,	NULL
----
----DECLARE @numFieldID AS BIGINT
----SELECT @numFieldID = numFieldId FROM dbo.DycFieldMaster WHERE vcFieldName = 'Received' AND vcDbColumnName = 'numUnitHourReceived'
----INSERT INTO dbo.DycFormField_Mapping 
----(numModuleID,numFieldID,numDomainID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,[order],
---- tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,
---- bitAllowFiltering,bitRequired,numFormFieldID,intSectionID,bitAllowGridColor)
----SELECT 4,@numFieldID,NULL,26,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,[order],
----	   tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,1,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,
----	   bitAllowFiltering,bitRequired,0,1,NULL
----FROM dbo.DycFieldMaster WHERE vcFieldName = 'Received' AND vcDbColumnName = 'numUnitHourReceived'
---- 
----exec USP_GetColumnConfiguration1 @numDomainID=1,@numUserCntId=1,@FormId=26,@numtype=0,@numViewID=0
----
---------------------
----ALTER TABLE Item ADD bitArchiveItem BIT
----
----ROLLBACK 
----
----/******************************************************************
----Project: BACRMUI   Date: 19.Jun.2013
----Comments: Add Item Thumbnail Image for Products & Services
----*******************************************************************/
----BEGIN TRANSACTION
----
----INSERT INTO dbo.DycFormField_Mapping 
----(numModuleID,numFieldID,numDomainID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,[order],
---- tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,
---- bitAllowFiltering,bitRequired,numFormFieldID,intSectionID,bitAllowGridColor)
----SELECT numModuleID,numFieldID,numDomainID,26,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,[order],
----	   tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,1,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,
----	   bitAllowFiltering,bitRequired,numFormFieldID,intSectionID,bitAllowGridColor
----FROM dbo.DycFormField_Mapping WHERE numFormID = 20 AND vcFieldName = 'Item Thumbnail Image'	   
---- 
----exec USP_GetColumnConfiguration1 @numDomainID=1,@numUserCntId=1,@FormId=26,@numtype=0,@numViewID=0
------ exec usp_getFormConfigFields @numDomainId=1,@numFormId=29,@numAuthGroupId=1,@numBizDocTemplateID=0
----
----ROLLBACK 
----
----/******************************************************************
----Project: BACRMUI   Date: 18.Jun.2013
----Comments: Add Item Filters
----*******************************************************************/
----BEGIN TRANSACTION
----	INSERT dbo.DynamicFormMaster 
----	( numFormId,vcFormName,cCustomFieldsAssociated,cAOIAssociated,bitDeleted,tintFlag,bitWorkFlow,vcLocationID,bitAllowGridColor )
----	VALUES 
----	( /* numFormId - numeric(10, 0) */ 76 ,
----	  /* vcFormName - nvarchar(50) */ N'Item Filter',
----	  /* cCustomFieldsAssociated - char(1) */ 'Y',
----	  /* cAOIAssociated - char(1) */ 'N',
----	  /* bitDeleted - bit */ 0,
----	  /* tintFlag - tinyint */ 2,
----	  /* bitWorkFlow - bit */ NULL,
----	  /* vcLocationID - varchar(50) */ '',
----	  /* bitAllowGridColor - bit */ NULL ) 
----	  
----	--SELECT * FROM dbo.DynamicFormMaster WHERE numFormID > 66
----	--SELECT * FROM dbo.DycFormField_Mapping WHERE numFormID = 66  AND vcAssociatedControlType ='SelectBox'
----	--DELETE FROM dbo.DycFormField_Mapping WHERE numFormID = 66
----	
----	INSERT INTO dbo.DycFormField_Mapping 
----	(numModuleID,numFieldID,numDomainID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,[order],
----	 tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,
----	 bitAllowFiltering,bitRequired,numFormFieldID,intSectionID,bitAllowGridColor)
----	 SELECT numModuleID,numFieldID,numDomainID,76,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,1,
----	 tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,1,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,0,bitExport,
----	 1,bitRequired,numFormFieldID,intSectionID,bitAllowGridColor FROM dbo.DycFormField_Mapping WHERE vcFieldName = 'Item Classification' AND numFormID = 20
----	 UNION 
----	 SELECT numModuleID,numFieldID,numDomainID,76,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,2,
----	 tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,1,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,0,bitExport,
----	 1,bitRequired,numFormFieldID,intSectionID,bitAllowGridColor FROM dbo.DycFormField_Mapping WHERE vcFieldName = 'Item Category' AND numFormID = 20
----	 UNION 
----	 SELECT numModuleID,numFieldID,numDomainID,76,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,3,
----	 tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,1,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,0,bitExport,
----	 1,bitRequired,numFormFieldID,intSectionID,bitAllowGridColor FROM dbo.DycFormField_Mapping WHERE vcFieldName = 'Item Group' AND numFormID = 20
----	 UNION 
----	 SELECT numModuleID,numFieldID,numDomainID,76,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,4,
----	 tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,1,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,0,bitExport,
----	 1,bitRequired,numFormFieldID,intSectionID,bitAllowGridColor FROM dbo.DycFormField_Mapping WHERE vcFieldName = 'Shipping Class' AND numFormID = 20
----	 UNION 
----	 SELECT numModuleID,numFieldID,numDomainID,76,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,5,
----	 tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,1,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,0,bitExport,
----	 1,bitRequired,numFormFieldID,intSectionID,bitAllowGridColor FROM dbo.DycFormField_Mapping WHERE vcFieldName = 'Item Class' AND numFormID = 20
----	 	 
----	 --SELECT * FROM dbo.DycFormField_Mapping WHERE numFormID = @numMaxFormID 
----	 
----RollBACk
----
----/******************************************************************
----Project: BACRMUI   Date: 17.Jun.2013
----Comments: Add IsArchieve for Products & Services
----*******************************************************************/
----BEGIN TRANSACTION
----
----INSERT INTO dbo.DycFormField_Mapping 
----(numModuleID,numFieldID,numDomainID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,[order],
---- tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,
---- bitAllowFiltering,bitRequired,numFormFieldID,intSectionID,bitAllowGridColor)
----SELECT numModuleID,numFieldID,numDomainID,26,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,[order],
----	   tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,1,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,
----	   bitAllowFiltering,bitRequired,numFormFieldID,intSectionID,bitAllowGridColor
----FROM dbo.DycFormField_Mapping WHERE numFormID = 20 AND vcFieldName LIKE '%archi%'	   
---- 
----exec USP_GetColumnConfiguration1 @numDomainID=1,@numUserCntId=1,@FormId=26,@numtype=0,@numViewID=0
------ exec usp_getFormConfigFields @numDomainId=1,@numFormId=29,@numAuthGroupId=1,@numBizDocTemplateID=0
----
----ROLLBACK 
----
----
----/******************************************************************
----Project: BACRMUI   Date: 28.May.2013
----Comments: Add IsReplied to get status of mail whether it is replied or not.
----*******************************************************************/
----BEGIN TRANSACTION
----ALTER TABLE EmailHistory ADD IsReplied BIT
----UPDATE EmailHistory SET IsReplied = 0 
------SELECT IsReplied,* FROM dbo.EmailHistory
----ROLLBACK
----
----/******************************************************************
----Project: BACRMUI   Date: 23.May.2013
----Comments: To modify Campaign Name
----*******************************************************************/
----BEGIN TRANSACTION
------SELECT * FROM dbo.ShortCutBar WHERE vcLinkName = 'Online & Offline Campaigns'
----UPDATE dbo.ShortCutBar SET vcLinkName = 'Campaigns' WHERE vcLinkName = 'Online & Offline Campaigns'
----ROLLBACK
----
----
----/******************************************************************
----Project: BACRMUI   Date: 15.May.2013
----Comments: To Add Campaign into Sales Order
----*******************************************************************/
----BEGIN TRANSACTION
----UPDATE dbo.DycFieldMaster SET vcPropertyName = 'CampaignID', numListID = 0, vcListItemType = 'C' WHERE vcDbColumnName = 'numCampainID' AND vcLookBackTableName = 'OpportunityMaster'
----
----INSERT INTO dbo.DycFormField_Mapping 
----(numModuleID,numFieldID,numDomainID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,[order],tintRow,
---- tintColumn,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,bitAllowFiltering,
---- bitRequired,numFormFieldID,intSectionID,bitAllowGridColor )
---- SELECT 3,116,NULL,39,1,1,'Campaign','SelectBox',NULL,NULL,1,1,1,1,0,1,1,1,1,1,NULL,NULL,NULL,1,NULL,0,NULL,NULL
---- 
---- exec usp_GetTableInfoDefault @numUserCntID=1,@numRecordID=51916,@numDomainId=1,@charCoType='O',@pageId=2,@numRelCntType=0,@numFormID=39,@tintPageType=3
----
---- --SELECT * FROM dbo.DycFormField_Mapping	WHERE vcFieldName = 'Campaign' AND numFormID = 39
---- --DELETE FROM dbo.DycFormField_Mapping	WHERE vcFieldName = 'Campaign' AND numFormID = 39
----ROLLBACK
----
----
----/******************************************************************
----Project: BACRMUI   Date: 13.May.2013
----Comments: To Add Average Cost into Import
----*******************************************************************/
----BEGIN TRANSACTION
----UPDATE dbo.DycFieldMaster SET vcPropertyName = 'AverageCost' WHERE vcFieldName = 'Average Cost' AND vcDbColumnName ='monAverageCost'
----
----INSERT INTO dbo.DycFormField_Mapping 
----(numModuleID,numFieldID,numDomainID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,[order],tintRow,
---- tintColumn,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,bitAllowFiltering,
---- bitRequired,numFormFieldID,intSectionID,bitAllowGridColor )
---- SELECT 4,209,	NULL,	20,	0,	0,	'Average Cost',	'TextBox','AverageCost',NULL,NULL,NULL,NULL,1,0,0,NULL,	NULL,	NULL,	NULL,	NULL,	1,	NULL,	1,	NULL,	0,	1,	NULL
---- 
---- --SELECT * FROM dbo.DycFormField_Mapping	WHERE vcFieldName LIKE '%avera%' AND numFormID = 20
---- --DELETE FROM dbo.DycFormField_Mapping	WHERE vcFieldName LIKE '%avera%' AND numFormID = 20
----ROLLBACK
----
----/******************************************************************
----Project: BACRMUI   Date: 19.Apr.2013
----Comments: To Remove footer shipping amount total from bizdocs
----*******************************************************************/
----BEGIN TRANSACTION
----UPDATE dbo.DycFieldMaster SET bitDeleted = 0,bitInResults = 1 WHERE numFieldID IN (SELECT numFieldId FROM dbo.DycFieldMaster WHERE vcFieldName ='shipping amount' AND vcDbColumnName = 'ShippingAmount')
----UPDATE dbo.DycFormField_Mapping SET bitDeleted = 0,bitInResults = 1 WHERE numFieldID IN (SELECT numFieldId FROM dbo.DycFieldMaster WHERE vcFieldName ='shipping amount' AND vcDbColumnName = 'ShippingAmount')
----ROLLBACK
----/******************************************************************
----Project: BACRMUI   Date: 17.Apr.2013
----Comments: Add Time & Expense Module
----*******************************************************************/
----BEGIN TRANSACTION
----SET IDENTITY_INSERT dbo.ModuleMaster ON
----INSERT INTO dbo.ModuleMaster (numModuleID,vcModuleName,tintGroupType) VALUES ( 40,'Time & Expense',1 ) 
----SET IDENTITY_INSERT dbo.ModuleMaster OFF	
------exec usp_GetAllSystemModules @tintGroupType=1
----
----INSERT INTO dbo.PageMaster (numPageID,numModuleID,vcFileName,vcPageDesc,bitIsViewApplicable,bitIsAddApplicable,bitIsUpdateApplicable,bitIsDeleteApplicable,
----							bitIsExportApplicable,vcToolTip,bitDeleted) 
----VALUES ( 
----	/* numPageID - numeric(18, 0) */ 117,
----	/* numModuleID - numeric(18, 0) */ 40,
----	/* vcFileName - varchar(50) */ 'frmEmpCal.aspx',
----	/* vcPageDesc - varchar(100) */ 'Time & Expense',
----	/* bitIsViewApplicable - int */ 1,
----	/* bitIsAddApplicable - int */ 1,
----	/* bitIsUpdateApplicable - int */ 0,
----	/* bitIsDeleteApplicable - int */ 1,
----	/* bitIsExportApplicable - int */ 0,
----	/* vcToolTip - nvarchar(1000) */ N'Time & Expense',
----	/* bitDeleted - bit */ 0 ) 
----ROLLBACK
----
----/******************************************************************
----Project: BACRMUI   Date: 15.Apr.2013
----Comments: Solved issues arose while using terms
----*******************************************************************/
----BEGIN TRANSACTION
----
----SELECT * FROM dbo.BillingTerms WHERE (LTRIM(vcTerms) <> vcTerms OR RTRIM(vcTerms) <> vcTerms)
----
----UPDATE dbo.BillingTerms SET vcTerms = LTRIM(vcTerms) WHERE LTRIM(vcTerms) <> vcTerms
----UPDATE dbo.BillingTerms SET vcTerms = RTRIM(vcTerms) WHERE RTRIM(vcTerms) <> vcTerms
----
----SELECT * FROM dbo.BillingTerms WHERE (LTRIM(vcTerms) <> vcTerms OR RTRIM(vcTerms) <> vcTerms)
----
----ROLLBACK
----
----/******************************************************************
----Project: BACRMUI   Date: 02.Apr.2013
----Comments: Update Warehouse Location as per new change
----*******************************************************************/
----BEGIN TRANSACTION
----
----UPDATE DycFieldMaster SET vcAssociatedControlType = 'SelectBox',vcPropertyName = 'WarehouseLocationID' WHERE vcFieldName = 'Warehouse Location' AND vcLookBackTableName = 'WareHouseItems'
------SELECT * FROM dbo.DycFieldMaster WHERE vcFieldName LIKE '%Warehouse Location%'
----
----UPDATE DycFormField_Mapping SET vcAssociatedControlType = 'SelectBox',vcFieldName = 'Location',vcPropertyName = 'WarehouseLocationID' WHERE vcFieldName LIKE '%Location%' AND numFormID IN (20,48)
----
------SELECT * FROM dbo.DycFormField_Mapping WHERE vcFieldName LIKE '%Location%' AND numFormID IN (20,48)
----
----ROLLBACK
----/******************************************************************
----Project: BACRMUI   Date: 28.Mar.2013
----Comments: Add bitIsImage into ItemImages
----*******************************************************************/
----BEGIN TRANSACTION
----
----INSERT INTO dbo.DycFieldMaster 
----(numModuleID,numDomainID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcToolTip,
----vcListItemType,numListID,PopupFunctionName,[order],tintRow,tintColumn,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,
----bitAllowSorting,bitWorkFlowField,bitImport,bitExport,bitAllowFiltering,bitInlineEdit,bitRequired,intColumnWidth,intFieldMaxLength) 
----SELECT 4,NULL,'Is Image','bitIsImage','bitIsImage','IsImage','ItemImages','Y','R','CheckBox','Is Image','',0,NULL,	7,	0,	1,	1,	0,	1,	0,	1,	1,	1,	1,	1,	1,	NULL,	1,	NULL,	NULL,	NULL,	NULL
----
----DECLARE @numMaxField AS NUMERIC(10,0)
----SELECT @numMaxField = numFieldID FROM dbo.DycFieldMaster WHERE vcFieldName = 'Is Image' AND vcDbColumnName = 'bitIsImage' AND vcLookBackTableName = 'ItemImages'
----PRINT @numMaxField
----
----INSERT INTO dbo.DycFormField_Mapping
----(numModuleID,numFieldID,numDomainID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,[order],tintRow,
----tintColumn,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,bitAllowFiltering,
----bitRequired,numFormFieldID,intSectionID,bitAllowGridColor) 
----SELECT 4,@numMaxField,	NULL,	54,	0,	0,	'Is Image',	'Checkbox',	'IsImage',	NULL,	7,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	NULL,	1,	NULL
----
----SELECT * FROM dbo.DycFormField_Mapping 
----WHERE numFormID = 54 
----
----EXEC dbo.USP_GET_DYNAMIC_IMPORT_FIELDS
----	@numFormID = 54, --  bigint
----	@intMode = 2, --  int
----	@numImportFileID = 0, --  numeric(18, 0)
----	@numDomainID = 1 --  numeric(18, 0)
----
----ROLLBACK
----
----/******************************************************************
----Project: BACRMUI   Date: 25.Mar.2013
----Comments: Add Customer Statement Email Template
----*******************************************************************/
----BEGIN TRANSACTION
----INSERT INTO [GenericDocuments]
----           ([VcFileName]
----           ,[vcDocName]
----           ,[numDocCategory]
----           ,[cUrlType]
----           ,[vcFileType]
----           ,[numDocStatus]
----           ,[vcDocDesc]
----           ,[numDomainID]
----           ,[numCreatedBy]
----           ,[bintCreatedDate]
----           ,[numModifiedBy]
----           ,[bintModifiedDate]
----           ,[vcSubject]
----           ,[vcDocumentSection]
----           ,[numRecID]
----           ,[tintCheckOutStatus]
----           ,[intDocumentVersion]
----           ,[numLastCheckedOutBy]
----           ,[dtCheckOutDate]
----           ,[dtCheckInDate]
----           ,[tintDocumentType]
----           ,[numOldSpecificDocID]
----           ,[numModuleID])
----     SELECT '#SYS#CUSTOMER_STATEMENT'
----           ,'Customer Statement'
----           ,369
----           ,''
----           ,''
----           ,0
----           ,'<div style="color: #222222; font-family: arial, sans-serif; font-size: 12.800000190734863px; background-color: #ffffff;"><span style="color: #0000ff;">Dear ##ContactFirstName##&nbsp;##ContactLastName##, &nbsp; &nbsp;&nbsp;</span></div>
----<div style="color: #222222; font-family: arial, sans-serif; font-size: 12.800000190734863px; background-color: #ffffff;"><span style="color: #0000ff;"><br />
----</span></div>
----<div style="color: #222222; font-family: arial, sans-serif; font-size: 12.800000190734863px; background-color: #ffffff;"><span style="color: #0000ff;">We have attached with this email a list of all your transactions with us. You can write to us or call us if you need any assistance or clarifications. &nbsp; &nbsp;&nbsp;</span></div>
----<div style="color: #222222; font-family: arial, sans-serif; font-size: 12.800000190734863px; background-color: #ffffff;"><span style="color: #0000ff;"><br />
----</span></div>
----<div style="color: #222222; font-family: arial, sans-serif; font-size: 12.800000190734863px; background-color: #ffffff;"><span style="color: #0000ff;">Thanks for your business.</span></div>
----<div style="color: #222222; font-family: arial, sans-serif; font-size: 13.333333969116211px; background-color: #ffffff;"><br />
----</div>
----##Signature##'
----           ,1
----           ,1
----           ,'2013-03-25 11:32:38.690'
----           ,1
----           ,'2013-03-25 11:40:32.090'
----           ,'Statement of transactions with ##OrganizationName##'
----           ,NULL
----           ,0
----           ,NULL
----           ,NULL
----           ,NULL
----           ,NULL
----           ,NULL
----           ,1
----           ,NULL
----           ,1           
----
--------------------- Insert Template for All domain ------------------
----
----INSERT  INTO dbo.GenericDocuments
----        (
----          VcFileName,
----          vcDocName,
----          numDocCategory,
----          cUrlType,
----          vcFileType,
----          numDocStatus,
----          vcDocDesc,
----          numDomainID,
----          numCreatedBy,
----          bintCreatedDate,
----          numModifiedBy,
----          bintModifiedDate,
----          vcSubject,
----          vcDocumentSection,
----          numRecID,
----          tintCheckOutStatus,
----          intDocumentVersion,
----          numLastCheckedOutBy,
----          dtCheckOutDate,
----          dtCheckInDate,
----          tintDocumentType,
----          numOldSpecificDocID,
----          numModuleID
----        )
----        SELECT  VcFileName,
----                vcDocName,
----                numDocCategory,
----                cUrlType,
----                vcFileType,
----                numDocStatus,
----                vcDocDesc,
----                DOM.numDomainID,
----                numCreatedBy,
----                bintCreatedDate,
----                numModifiedBy,
----                bintModifiedDate,
----                vcSubject,
----                vcDocumentSection,
----                numRecID,
----                tintCheckOutStatus,
----                intDocumentVersion,
----                numLastCheckedOutBy,
----                dtCheckOutDate,
----                dtCheckInDate,
----                tintDocumentType,
----                numOldSpecificDocID,
----                numModuleID
----        FROM    dbo.GenericDocuments GD
----                CROSS JOIN dbo.Domain DOM
----        WHERE   VcFileName = '#SYS#CUSTOMER_STATEMENT'
----                AND DOM.numDomainID NOT IN (
----                SELECT  numDomainID
----                FROM    dbo.GenericDocuments
----                WHERE   VcFileName = '#SYS#CUSTOMER_STATEMENT' )
----
----SELECT  *
----FROM    dbo.GenericDocuments
----WHERE   VcFileName = '#SYS#CUSTOMER_STATEMENT'        
----           
----ROLLBACK
----
----
----
----/******************************************************************
----Project: BACRMUI   Date: 20.Mar.2013
----Comments: Add Price Level into Item
----*******************************************************************/
----BEGIN TRANSACTION
----
----INSERT INTO dbo.DycFieldMaster 
----(numModuleID,numDomainID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcToolTip,
----vcListItemType,numListID,PopupFunctionName,[order],tintRow,tintColumn,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,
----bitAllowSorting,bitWorkFlowField,bitImport,bitExport,bitAllowFiltering,bitInlineEdit,bitRequired,intColumnWidth,intFieldMaxLength) 
----SELECT 4,NULL,'Price Level','vcPriceLevelDetail','vcPriceLevelDetail','PriceLevelDetail','PricingTable','V','R','TextBox','Price Level','',0,NULL,	62,	0,	1,	1,	0,	1,	0,	1,	1,	1,	1,	1,	1,	NULL,	1,	NULL,	NULL,	NULL,	NULL
----UNION ALL
----SELECT 4,NULL,'Price Rule Type','tintRuleType','tintRuleType','PriceRuleType','PricingTable','N','R','SelectBox','Price Rule Type','',0,NULL,	62,	0,	1,	1,	0,	1,	0,	1,	1,	1,	1,	1,	1,	NULL,	1,	NULL,	NULL,	NULL,	NULL
----UNION ALL
----SELECT 4,NULL,'Price Level Discount Type','tintDiscountType','tintDiscountType','PriceDiscountType','PricingTable','N','R','SelectBox','Price Level Discount Type','',0,NULL,	62,	0,	1,	1,	0,	1,	0,	1,	1,	1,	1,	1,	1,	NULL,	1,	NULL,	NULL,	NULL,	NULL
----
----DECLARE @numField1 AS NUMERIC(10,0)
----DECLARE @numField2 AS NUMERIC(10,0)
----DECLARE @numField3 AS NUMERIC(10,0)
----SELECT @numField1 = numFieldID FROM dbo.DycFieldMaster 
----							   WHERE vcFieldName = 'Price Level' 
----							   AND vcDbColumnName = 'vcPriceLevelDetail' 
----							   AND vcLookBackTableName = 'PricingTable'
----SELECT @numField2 = numFieldID FROM dbo.DycFieldMaster 
----							   WHERE vcFieldName = 'Price Rule Type' 
----							   AND vcDbColumnName = 'tintRuleType' 
----							   AND vcLookBackTableName = 'PricingTable'
----SELECT @numField3 = numFieldID FROM dbo.DycFieldMaster 
----							   WHERE vcFieldName = 'Price Level Discount Type' 
----							   AND vcDbColumnName = 'tintDiscountType' 
----							   AND vcLookBackTableName = 'PricingTable'
----PRINT @numField1
----PRINT @numField2
----PRINT @numField3
----
----INSERT INTO dbo.DycFormField_Mapping
----(numModuleID,numFieldID,numDomainID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,[order],tintRow,
----tintColumn,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,bitAllowFiltering,
----bitRequired,numFormFieldID,intSectionID,bitAllowGridColor) 
----SELECT 4,@numField1,	NULL,	20,	0,	0,	'Price Level',	'TextBox',	'PriceLevelDetail',	NULL,	62,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	NULL,	1,	NULL
----UNION ALL
----SELECT 4,@numField2,	NULL,	20,	0,	0,	'Price Rule Type',	'SelectBox',	'PriceRuleType',	NULL,	62,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	NULL,	1,	NULL
----UNION ALL
----SELECT 4,@numField3,	NULL,	20,	0,	0,	'Price Level Discount Type',	'SelectBox',	'PriceDiscountType',	NULL,	62,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	NULL,	1,	NULL
----
----
----SELECT * FROM dbo.DycFormField_Mapping 
----WHERE numFormID = 20 AND vcFieldName IN ('Price Level','Price Rule Type','Price Level Discount Type')
----
----SELECT * FROM dbo.DycFieldMaster
----WHERE vcFieldName IN ('Price Level','Price Rule Type','Price Level Discount Type')
----
----
----EXEC dbo.USP_GET_DYNAMIC_IMPORT_FIELDS
----	@numFormID = 20, --  bigint
----	@intMode = 2, --  int
----	@numImportFileID = 0, --  numeric(18, 0)
----	@numDomainID = 1 --  numeric(18, 0)
----
----ROLLBACK
----
----/******************************************************************
----Project: BACRMUI   Date: 18.Mar.2013
----Comments: Add BillingDaysName
----*******************************************************************/
----BEGIN TRANSACTION
----
----INSERT INTO dbo.DycFieldMaster 
----(numModuleID,numDomainID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcToolTip,
----vcListItemType,numListID,PopupFunctionName,[order],tintRow,tintColumn,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,
----bitAllowSorting,bitWorkFlowField,bitImport,bitExport,bitAllowFiltering,bitInlineEdit,bitRequired,intColumnWidth,intFieldMaxLength) 
----SELECT 4,NULL,'Net Days','numTermsID','numTermsID','BillingDays','BillingTerms','N','R','SelectBox','Net Days','',0,NULL,	62,	0,	1,	1,	0,	1,	0,	1,	1,	1,	1,	1,	1,	NULL,	1,	NULL,	NULL,	NULL,	NULL
----
----DECLARE @numMaxField AS NUMERIC(10,0)
----SELECT @numMaxField = numFieldID FROM dbo.DycFieldMaster WHERE vcFieldName = 'Net Days' AND vcDbColumnName = 'numTermsID' AND vcLookBackTableName = 'BillingTerms'
----PRINT @numMaxField
----
----INSERT INTO dbo.DycFormField_Mapping
----(numModuleID,numFieldID,numDomainID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,[order],tintRow,
----tintColumn,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,bitAllowFiltering,
----bitRequired,numFormFieldID,intSectionID,bitAllowGridColor) 
----SELECT 4,@numMaxField,	NULL,	36,	0,	0,	'BillingDaysName',	'SelectBox',	'BillingDays',	NULL,	62,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	NULL,	1,	NULL
----
----SELECT * FROM dbo.DycFormField_Mapping 
----WHERE numFormID = 36 
----AND numFieldID = (SELECT numFieldId FROM dbo.DycFieldMaster WHERE vcFieldName = 'Net Days' AND vcLookBackTableName = 'DivisionMaster')
----
----DELETE FROM dbo.DycFormField_Mapping 
----WHERE numFormID = 36 
----AND numFieldID = (SELECT numFieldId FROM dbo.DycFieldMaster WHERE vcFieldName = 'Net Days' AND vcLookBackTableName = 'DivisionMaster')
----
----EXEC dbo.USP_GET_DYNAMIC_IMPORT_FIELDS
----	@numFormID = 36, --  bigint
----	@intMode = 2, --  int
----	@numImportFileID = 0, --  numeric(18, 0)
----	@numDomainID = 1 --  numeric(18, 0)
----
----ROLLBACK
----
----/******************************************************************
----Project: BACRMUI   Date: 05.Mar.2013
----Comments: Add column in ShippingBox
----*******************************************************************/
----BEGIN TRANSACTION
----
----ALTER TABLE dbo.ShippingBox ADD numShipCompany INT
----
-------ALTER TABLE OpportunityMaster ADD monTermDiscount NUMERIC(10,0)
----ROLLBACK
----
----
----/******************************************************************
----Project: BACRMUI   Date: 02.Mar.2013
----Comments: Add column in Billing Terms/OpportunityMaster
----*******************************************************************/
----BEGIN TRANSACTION
----
----ALTER TABLE dbo.BillingTerms ADD bitActive BIT
----UPDATE BillingTerms SET bitActive = 1
----
-------ALTER TABLE OpportunityMaster ADD monTermDiscount NUMERIC(10,0)
----ROLLBACK
----
----
----/******************************************************************
----Project: BACRMUI   Date: 20.Feb.2013
----Comments: To Migrate Billing Terms with domain
----*******************************************************************/
----BEGIN TRANSACTION
----
----DELETE FROM BillingTerms
----SET IDENTITY_INSERT dbo.BillingTerms ON
----INSERT INTO dbo.BillingTerms 
----(numTermsID,vcTerms,tintApplyTo,numNetDueInDays,numDiscount,numDiscountPaidInDays,numListItemID,numDomainID)
----SELECT numListItemID,'Net ' + vcData,1,vcData,0,0,numListItemID,ListDetails.numDomainID FROM dbo.ListDetails 
------CROSS JOIN dbo.Domain
----WHERE numListID = (SELECT numListID FROM dbo.ListMaster WHERE vcListName = 'Billing term net days')
------AND ListDetails.numDomainID =1
----ORDER BY ListDetails.numDomainID
----SELECT * FROM BillingTerms
----
----SET IDENTITY_INSERT dbo.BillingTerms OFF	
----ROLLBACK
----
----
----/******************************************************************
----Project: BACRMUI   Date: 20.Feb.2013
----Comments: Financial Term Changes
----*******************************************************************/
----BEGIN TRANSACTION
----
----GO
----/****** Object:  Table [dbo].[BillingTerms]    Script Date: 02/20/2013 15:50:29 ******/
----SET ANSI_NULLS ON
----GO
----SET QUOTED_IDENTIFIER ON
----GO
----SET ANSI_PADDING ON
----GO
----CREATE TABLE [dbo].[BillingTerms](
----	[numTermsID] [bigint] IDENTITY(1,1) NOT NULL,
----	[vcTerms] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
----	[tintApplyTo] [tinyint] NOT NULL,
----	[numNetDueInDays] [int] NOT NULL,
----	[numDiscount] [numeric](10, 0) NULL,
----	[numDiscountPaidInDays] [int] NULL,
----	[numListItemID] [numeric](18, 0) NULL,
----	[numDomainID] [numeric](18, 0) NOT NULL,
---- CONSTRAINT [PK_BillingTerms] PRIMARY KEY CLUSTERED 
----(
----	[numTermsID] ASC
----)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
----) ON [PRIMARY]
----
----GO
----SET ANSI_PADDING ON
----GO
----EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1 - Sales Order, 2 - Purchase Order' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'BillingTerms', @level2type=N'COLUMN', @level2name=N'tintApplyTo'
----
----ROLLBACK
----
----/******************************************************************
----Project: BACRMUI   Date: 19.Feb.2013
----Comments: BUG ID:267
----*******************************************************************/
----BEGIN TRANSACTION
----
------------ INSERT 'Relationship Type','Group' Fields in DycFormField_Mapping
----DECLARE @numRelationShipTypeFieldID AS NUMERIC(9)
----SELECT @numRelationShipTypeFieldID = MAX(numFieldID) FROM dbo.DycFieldMaster WHERE vcDbColumnName = 'tintCRMType' AND vcFieldName = 'Relationship Type'
----PRINT @numRelationShipTypeFieldID
----
----INSERT INTO dbo.DycFormField_Mapping 
----(numModuleID,numFieldID,numDomainID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,[order],tintRow,tintColumn,
----bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,bitAllowFiltering,bitRequired,
----numFormFieldID,intSectionID,bitAllowGridColor )
----SELECT  2,@numRelationShipTypeFieldID,NULL,1,1,0,'Relationship Type','SelectBox','CRMType','',0,0,0,	0,	0,	0,	1,	0,	0,	0,	0,	1,	0,	0,	0,	0,	1,	0
----
----UPDATE dbo.DycFieldMaster SET vcListItemType = 'SYS' WHERE vcFieldName = 'Relationship Type'
------SELECT * FROM View_DynamicDefaultColumns WHERE numDomainID = 1 and bitAllowEdit = 1 AND numformid =1
----
----ROLLBACK
----
----/******************************************************************
----Project: BACRMUI   Date: 15.Feb.2013
----Comments: Import Item,Warehouse Change
----*******************************************************************/
----BEGIN TRANSACTION
----
------SELECT * FROM dbo.DycFormField_Mapping WHERE numFormID = 48 AND vcFieldName IN ('Serial/Lot #s','Comments','Qty')
----UPDATE dbo.DycFormField_Mapping SET bitImport = 1 WHERE numFormID = 48 AND vcFieldName IN ('Serial/Lot #s','Comments','Qty')
----
----ROLLBACK
----
----/******************************************************************
----Project: BACRMUI   Date: 15.Feb.2013
----Comments: Import Item,Warehouse Change
----*******************************************************************/
----BEGIN TRANSACTION
----
----UPDATE dbo.DycFieldMaster SET vcLookBackTableName = 'Vendor' WHERE vcFieldName = 'Vendor' AND vcDbColumnName = 'numVendorID'
----
----ROLLBACK
----
----/******************************************************************
----Project: BACRMUI   Date: 13.Feb.2013
----Comments: Import Item,Warehouse Change
----*******************************************************************/
----BEGIN TRANSACTION
----
----INSERT INTO dbo.DycFormSectionDetail(numFormID,intSectionID,vcSectionName,Loc_id) 
----SELECT  20,3,'Item Warehouse Details',0
----UNION ALL
----SELECT  20,4,'Item Vendor Details',0
----
----INSERT INTO dbo.DycFormField_Mapping 
----(numModuleID,numFieldID,numDomainID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,[order],tintRow,tintColumn,
----bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,bitAllowFiltering,bitRequired,numFormFieldID,
----intSectionID,	bitAllowGridColor) 
----SELECT numModuleID,numFieldID,numDomainID,20,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,[order],tintRow,tintColumn,
----bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,bitAllowFiltering,bitRequired,numFormFieldID,
----3,	bitAllowGridColor
----FROM dbo.DycFormField_Mapping 
----WHERE numFormID = 48 
----AND ISNULL(bitImport,0) = 1
----AND vcFieldName NOT IN ('Item','Item Code')
----UNION ALL 
----SELECT numModuleID,numFieldID,numDomainID,20,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,[order],tintRow,tintColumn,
----bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,bitAllowFiltering,bitRequired,numFormFieldID,
----4,	bitAllowGridColor
----FROM dbo.DycFormField_Mapping 
----WHERE numFormID = 55
----AND ISNULL(bitImport,0) = 1
----AND vcFieldName NOT IN ('Item','Item ID')
----
----SELECT * FROM dbo.DycFormField_Mapping WHERE numFormID = 20 ORDER BY numFieldID
----exec USP_GET_DYNAMIC_IMPORT_FIELDS @numFormID=20,@intMode=2,@numImportFileID=0,@numDomainID=1
----EXEC dbo.USP_GetItemForUpdate @numDomainID = 1 --  numeric(9, 0)
----
----ROLLBACK
-------------------
----
----
----/******************************************************************
----Project: BACRMUI   Date: 11.Feb.2013
----Comments: Default Site Entry
----*******************************************************************/
----BEGIN TRANSACTION
----SET IDENTITY_INSERT dbo.Sites ON
----INSERT INTO dbo.Sites 
----(numSiteID,vcSiteName,vcDescription,vcHostName,numCreatedBy,dtCreateDate,numModifiedBy,dtModifiedDate,numDomainID,bitIsActive,vcLiveURL,numCurrencyID,bitOnePageCheckout,
---- tintRateType,bitIsMaintainScroll) 
---- VALUES ( 
----	-1,
----	/* vcSiteName - varchar(50) */ 'Default Generic Site',
----	/* vcDescription - varchar(1000) */ 'Do Not Delete',
----	/* vcHostName - varchar(500) */ '',
----	/* numCreatedBy - numeric(18, 0) */ 0,
----	/* dtCreateDate - datetime */ '2013-2-11 19:30:45.826',
----	/* numModifiedBy - numeric(18, 0) */ 0,
----	/* dtModifiedDate - datetime */ '2013-2-11 19:30:45.826',
----	/* numDomainID - numeric(18, 0) */ -1,
----	/* bitIsActive - bit */ 1,
----	/* vcLiveURL - varchar(100) */ '',
----	/* numCurrencyID - numeric(18, 0) */ 0,
----	/* bitOnePageCheckout - bit */ 0,
----	/* tintRateType - tinyint */ 0,
----	/* bitIsMaintainScroll - bit */ 0 ) 
----
----SET IDENTITY_INSERT dbo.Sites OFF	
----SELECT * FROM dbo.Sites WHERE vcSiteName = 'Default Generic Site'  
----ROLLBACK
----
-------------------
----/******************************************************************
----Project: BACRMUI   Date: 06.Feb.2013
----Comments: Sales Order Modification
----*******************************************************************/
----BEGIN TRANSACTION
----
----ALTER TABLE OpportunityMaster ADD vcCouponCode VARCHAR(100)
----
----ROLLBACK
------
------/******************************************************************
------Project: BACRMUI   Date: 04.Feb.2013
------Comments: Sales Order/Shipping Report Modification
------*******************************************************************/
------BEGIN TRANSACTION
------
----ALTER TABLE dbo.ShippingReport ADD numOppID NUMERIC(18,0)
------ALTER TABLE dbo.ShippingReportItems ADD numOppItemID NUMERIC(18,0)
------
------ROLLBACK
----
----
----/******************************************************************
----Project: BACRMUI   Date: 01.Feb.2013
----Comments: Shipping Report Modification
----*******************************************************************/
----BEGIN TRANSACTION
----
----ALTER TABLE dbo.ShippingBox ADD fltDimensionalWeight FLOAT
----
----ROLLBACK
----
----/******************************************************************
----Project: BACRMUI/Bizservice   Date: 29.01.2013
----Comments: Inserting new records for a Marketplace, Item Category into DynamicFormMaster,DycFormField_Mapping
----*******************************************************************/
----BEGIN TRANSACTION
----INSERT INTO dbo.DycFieldMaster 
----(numModuleID,numDomainID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,
----vcToolTip,vcListItemType,numListID,PopupFunctionName,[order],tintRow,tintColumn,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,
----bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,bitAllowFiltering,bitInlineEdit,bitRequired,intColumnWidth,intFieldMaxLength ) 
----SELECT 4,NULL,'Marketplace','vcExportToAPI','vcExportToAPI','ListOfAPI','Item','V','R','SelectBox',
----NULL,'',0,NULL,NULL,NULL,1,0,0,0,0,0,0,0,
----NULL,NULL,1,NULL,NULL,	NULL,	NULL,	NULL,NULL
----
----DECLARE @numFieldID AS NUMERIC(9)
----SELECT @numFieldID = numFieldID FROM dbo.DycFieldMaster WHERE vcFieldName = 'Marketplace' AND vcDbColumnName = 'vcExportToAPI'
----PRINT @numFieldID
----
------SELECT * FROM dbo.DycFieldMaster WHERE vcFieldName = 'Marketplace' AND vcDbColumnName = 'vcExportToAPI'
----
----INSERT INTO dbo.DycFormField_Mapping 
----(numModuleID,numFieldID,numDomainID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,[order],tintRow,tintColumn,
----bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,bitAllowFiltering,bitRequired,numFormFieldID,
----intSectionID,bitAllowGridColor) 
----SELECT MASTER.numModuleID,MASTER.numFieldID,MASTER.numDomainID,20,MASTER.bitAllowEdit,MASTER.bitInlineEdit,MASTER.vcFieldName,
----MASTER.vcAssociatedControlType,'ListOfCategories',MASTER.PopupFunctionName,MASTER.[order],1,1,
----1,0,0,0,0,0,0,0,1,0,0,0,0,1,NULL
----FROM dbo.DycFieldMaster MASTER 
----WHERE vcFieldName = 'Item Category' 
----AND vcDbColumnName = 'numCategoryID'
----
----UNION ALL
----
----SELECT MASTER.numModuleID,MASTER.numFieldID,MASTER.numDomainID,20,MASTER.bitAllowEdit,MASTER.bitInlineEdit,MASTER.vcFieldName,
----MASTER.vcAssociatedControlType,MASTER.vcPropertyName,MASTER.PopupFunctionName,MASTER.[order],1,1,
----1,0,0,0,0,0,0,0,1,0,0,0,0,1,NULL
----FROM dbo.DycFieldMaster MASTER 
----WHERE vcFieldName = 'Marketplace' AND vcDbColumnName = 'vcExportToAPI'
----
------SELECT * FROM dbo.DycFormField_Mapping WHERE numFormID = 20 AND bitImport = 1
----UPDATE DycFormField_Mapping SET vcPropertyName = 'ListOfCategories' WHERE numFormID = 20 AND vcFieldName = 'Item Category'
----ROLLBACK 
----
----
----/******************************************************************
----Project: BACRMUI/Bizservice   Date: 29.01.2013
----Comments: 
----*******************************************************************/
----BEGIN TRANSACTION
------SELECT * FROM dbo.DycFieldMaster WHERE vcFieldName = 'Item Detail ID' AND vcDbColumnName = 'numItemDetailID'
----UPDATE dbo.DycFieldMaster SET vcLookBackTableName = 'ItemDetails'
----WHERE vcFieldName = 'Item Detail ID' AND vcDbColumnName = 'numItemDetailID'
----
----UPDATE dbo.DycFieldMaster SET vcLookBackTableName = 'Item' WHERE vcDbColumnName = 'numItemCode' AND vcLookBackTableName = 'ItemImages' 
----
----ROLLBACK
----
----/******************************************************************
----Project: BACRMUI/Bizservice   Date: 26.01.2013
----Comments: Inserting new records for a web api detail into DynamicFormMaster,DycFormField_Mapping
----*******************************************************************/
----BEGIN TRANSACTION
----
----INSERT INTO dbo.DynamicFormMaster (
----	numFormId,
----	vcFormName,
----	cCustomFieldsAssociated,
----	cAOIAssociated,
----	bitDeleted,
----	tintFlag,
----	bitWorkFlow,
----	vcLocationID,
----	bitAllowGridColor
----) 
----
----SELECT 65,'Item Web Api Detail','N','N',0,1,NULL,NULL,NULL
----
----DECLARE @numFormID AS NUMERIC(10,0)
----SELECT @numFormID = numFormID FROM dbo.DynamicFormMaster WHERE vcFormName = 'Item Web Api Detail'
----PRINT @numFormID
----
----INSERT INTO dbo.DycFormField_Mapping 
----(numModuleID,numFieldID,numDomainID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,[order],tintRow,tintColumn,
----bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,bitAllowFiltering,bitRequired,numFormFieldID,
----intSectionID,bitAllowGridColor) 
----SELECT MAPPING.numModuleID,MASTER.numFieldID,MAPPING.numDomainID,@numFormID,MAPPING.bitAllowEdit,MAPPING.bitInlineEdit,MASTER.vcFieldName,
----MASTER.vcAssociatedControlType,MASTER.vcPropertyName,MASTER.PopupFunctionName,MASTER.[order],MASTER.tintRow,MASTER.tintColumn,
----1,0,0,0,0,0,0,0,1,0,0,0,MAPPING.numFormFieldID,MAPPING.intSectionID,MAPPING.bitAllowGridColor
----FROM dbo.DycFormField_Mapping MAPPING
----JOIN dbo.DycFieldMaster MASTER ON MASTER.numFieldId = MAPPING.numFieldID
----WHERE 1=1
----AND numFormID = 50
----AND MASTER.numFieldId NOT IN (406)
----
----UNION ALL
----
----SELECT MAPPING.numModuleID,MAPPING.numFieldID,MAPPING.numDomainID,@numFormID,0,MAPPING.bitInlineEdit,MAPPING.vcFieldName,
----MAPPING.vcAssociatedControlType,MAPPING.vcPropertyName,MAPPING.PopupFunctionName,1,MAPPING.tintRow,MAPPING.tintColumn,
----MAPPING.bitInResults,MAPPING.bitDeleted,0,0,0,0,0,0,1,0,0,0,NULL,1,NULL
----FROM dbo.DycFieldMaster MAPPING
----WHERE 1=1
----AND vcFieldName = 'Item Code' AND vcDbColumnName = 'numItemCode'
----
----SELECT * FROM dbo.DycFormField_Mapping WHERE numFormID = @numFormID
------SELECT * FROM dbo.DynamicFormMaster WHERE numFormID = 50
----
---- INSERT INTO dbo.DycFormSectionDetail 
---- (numFormID,intSectionID,vcSectionName,Loc_id) 
----SELECT 65,	1,	'More Item Details',0
----UNION ALL
----SELECT 65,	2,	'Amazon',0
----UNION ALL
----SELECT 65,	3,	'E-Bay',0
----UNION ALL
----SELECT 65,	4,	'Google',0
----UNION ALL
----SELECT 65,	5,	'Magento',0
----
----ROLLBACK 
----
----
----/******************************************************************
----Project: BACRMUI/Bizservice   Date: 25.01.2013
----Comments: General Function used for splitting large string
----*******************************************************************/
----BEGIN TRANSACTION
----
----CREATE TABLE Numbers
----(
----	Number INT NOT NULL,
----	CONSTRAINT PK_Numbers 
----		PRIMARY KEY CLUSTERED (Number)
----		WITH FILLFACTOR = 100
----)
----
----INSERT INTO Numbers
----SELECT
----	(a.Number * 256) + b.Number AS Number
----FROM 
----	(
----		SELECT number
----		FROM master..spt_values
----		WHERE 
----			type = 'P'
----			AND number <= 255
----	) a (Number),
----	(
----		SELECT number
----		FROM master..spt_values
----		WHERE 
----			type = 'P'
----			AND number <= 255
----	) b (Number)
----GO
----ROLLBACK
----
----
----/******************************************************************
----Project: Bizservice   Date: 23.01.2013
----Comments: Added Excel Row number into Import File Master to track records for starting interrupt
----*******************************************************************/
----BEGIN TRANSACTION
----
----ALTER TABLE Import_File_Master ADD numProcessedCSVRowNumber NUMERIC(9)
----
----ROLLBACK
----
----
----/******************************************************************
----Project: BizService   Date: 23.01.2013
----Comments: 
--------------------- BACRMUI : Add this keys to web.Config ------------------------
----*******************************************************************/
----
----<add key="MaxUploadFileSize" value="51200"></add> <!--File size in kb-->
----<add key="MaxFileRecords" value="10000"></add> <!--File Records count-->
----
------ Put this code block into <system.webServer> section in web.config file of BACRMUI (52428800 bytes = 50 MB)
----<security>
----    <requestFiltering>
----        <requestLimits maxAllowedContentLength="52428800" maxUrl="4096" />
----    </requestFiltering>
----</security>
---------------------------------------------------------------------------------------
----
----/******************************************************************
----Project: Bizservice   Date: 23.01.2013
----Comments: Update vcPropertyName for 'Bill To Country'
----*******************************************************************/
----BEGIN TRANSACTION
----
----UPDATE dbo.DycFormField_Mapping SET vcPropertyName = 'Country'
----WHERE vcFieldName = 'Bill To Country' AND numFormID = 36
----
----ROLLBACK
----
----
----
----/******************************************************************
----Project: Bizservice   Date: 21.01.2013
----Comments: Added 2 new fields into configuration
----*******************************************************************/
----BEGIN TRANSACTION
----
------------ INSERT '' Field in DycFieldMaster
----INSERT INTO dbo.DycFieldMaster 
----(numModuleID,numDomainID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcToolTip,
----vcListItemType,numListID,PopupFunctionName,[order],tintRow,tintColumn,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,
----bitAllowSorting,bitWorkFlowField,bitImport,bitExport,bitAllowFiltering,bitInlineEdit,bitRequired,intColumnWidth,intFieldMaxLength) 
----SELECT 2,	NULL,	'Relationship Type',	'tintCRMType',	'tintCRMType',	'CRMType',	'DivisionMaster',	'N',	'R',	'SelectBox',	NULL,		'',		0,	'',	0,	0,	0,	0,	0,	0,	0,	1,	0,	0,	0,	0,	0,	0,	0,	0,	0,0,0
----
------------ INSERT 'Relationship Type','Group' Fields in DycFormField_Mapping
----DECLARE @numRelationShipTypeFieldID AS NUMERIC(9)
----SELECT @numRelationShipTypeFieldID = MAX(numFieldID) FROM dbo.DycFieldMaster WHERE vcDbColumnName = 'tintCRMType' AND vcFieldName = 'Relationship Type'
----PRINT @numRelationShipTypeFieldID
----
----DECLARE @numGroupIDFieldID AS NUMERIC(9)
----SELECT @numGroupIDFieldID = MAX(numFieldID) FROM dbo.DycFieldMaster WHERE vcDbColumnName = 'numGrpID' AND vcFieldName = 'Group'
----PRINT @numGroupIDFieldID
----
----INSERT INTO dbo.DycFormField_Mapping 
----(numModuleID,numFieldID,numDomainID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,[order],tintRow,tintColumn,
----bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,bitAllowFiltering,bitRequired,
----numFormFieldID,intSectionID,bitAllowGridColor )
----SELECT  2,@numRelationShipTypeFieldID,NULL,36,0,0,'Relationship Type','SelectBox','CRMType','',0,0,0,	0,	0,	0,	1,	0,	0,	0,	0,	1,	0,	0,	0,	0,	1,	0
----UNION ALL
----SELECT 2,@numGroupIDFieldID,NULL,36,1,1,'Group','SelectBox','GroupID',NULL,17,20,2,	1,	0,	0,	1,	0,	0,	0,	0,	1,	NULL,	1,	0,	0,	1,	0
----
----
------SELECT * FROM dbo.DycFieldMaster WHERE vcFieldName LIKE '%Relationship Type%'
------SELECT * FROM DycFormField_Mapping WHERE vcFieldName IN ('Relationship Type') AND numFormID = 36
------SELECT * FROM DycFormField_Mapping WHERE vcFieldName IN ('Group') AND numFormID = 36
----
----ROLLBACK
----
----/******************************************************************
----Project: BACRMUI   Date: 18.01.2013
----Comments: TO Solve Duplicate Records in Import Configure Columns
----*******************************************************************/
----BEGIN TRANSACTION
----
----DELETE FROM [DycFormConfigurationDetails] WHERE tintPageType = 4 AND [numUserCntId] = 0
----AND [numFormId] IN (20,48,31,45,55,50,36)
----
----ROLLBACK
----
----/******************************************************************
----Project: BizService   Date: 18.01.2013
----Comments: 
--------------------- BizService : ADD THIS KEYS TO app.Config ------------------------
----*******************************************************************/
----
----<add key="WaitRecords" value="10"></add>
----<add key="WaitMiliSeconds" value="5000"></add> <!-- value in MiliSeconds-->
----<add key="IsDebugMode" value="True"></add>
---------------------------------------------------------------------------------------
----
----/******************************************************************
----Project: Time & Expense   Date: 10.01.2013
----Comments: Task 24 : Slide # 5 : Setup/Update Default AddressId For ProjectsMaster
----*******************************************************************/
----BEGIN TRANSACTION
----	BEGIN
----		CREATE TABLE #tmpDiv
----		(
----			ID [numeric] (18, 0) NOT NULL IDENTITY(1, 1),
----			numDivisionId [numeric] (18, 0) NOT NULL,
----			numDomainID [numeric] (18, 0) NOT NULL 
----		)
----		
----		INSERT INTO #tmpDiv ( numDivisionId,numDomainID ) 
----		SELECT DISTINCT numDivisionId,numDomainId FROM dbo.ProjectsMaster  WHERE ISNULL(numAddressID,0) = 0
----		SELECT * FROM #tmpDiv
----		
----		DECLARE @numDomainID  AS NUMERIC(18,0)
----		DECLARE @numDivisionId  AS NUMERIC(18,0)
----		DECLARE @intCnt AS INT
----		SELECT @intCnt = COUNT(*) FROM #tmpDiv
----		PRINT '#tmpDiv Table Row Count : ' + CONVERT(VARCHAR(10),@intCnt)
----		
----		IF @intCnt > 0
----			BEGIN
----				DECLARE @numPrimaryAddressID AS NUMERIC(18,0)
----				DECLARE @numAddressID AS NUMERIC(18,0)
----				DECLARE @intCount AS INT 
----				SET @intCount = 0
----				
----				WHILE(@intCount < @intCnt)
----				BEGIN
----					SET @intCount = @intCount + 1
----					PRINT '@intCount : ' + CONVERT(VARCHAR(10),@intCount)
----					
----					SELECT @numDivisionId = numDivisionID,
----					       @numDomainID = numDomainID
----					FROM #tmpDiv WHERE ID = @intCount
----					
----					PRINT '@numDivisionId : ' + CONVERT(VARCHAR(10),@numDivisionId)
----					PRINT '@numDomainID : ' + CONVERT(VARCHAR(10),@numDomainID)
----					
----					SELECT @numPrimaryAddressID = ISNULL(numAddressID,0) FROM dbo.AddressDetails 
----					WHERE numRecordID = @numDivisionId  
----					AND numDomainID = @numDomainID
----					AND ISNULL(bitIsPrimary,0) = 1
----					PRINT '@numPrimaryAddressID : ' + CONVERT(VARCHAR(10),@numPrimaryAddressID)
----					
----					INSERT INTO dbo.AddressDetails 
----					(vcAddressName,vcStreet,vcCity,vcPostalCode,numState,numCountry,bitIsPrimary,tintAddressOf,tintAddressType,numRecordID,numDomainID )
----					SELECT vcAddressName,vcStreet,vcCity,vcPostalCode,numState,numCountry,bitIsPrimary,tintAddressOf,tintAddressType,numRecordID ,numDomainID
----					FROM dbo.AddressDetails
----					WHERE numAddressID = @numPrimaryAddressID
----					AND numDomainID = @numDomainID
----					
----					--SELECT @numAddressID = numAddressID FROM dbo.AddressDetails WHERE numRecordID = @numProId AND ISNULL(bitIsPrimary,0) = 1  AND ISNULL(tintAddressOf,0) = 4
----					SET @numAddressID = @@Identity  
----					PRINT '@numAddressID : ' + CONVERT(VARCHAR(10),@numAddressID)
----					
----					UPDATE ProjectsMaster SET numAddressID = @numAddressID 	
----					WHERE numDivisionId = @numDivisionId  
----					AND numDomainID = @numDomainID 	
----					
----				END
----						
----			END
----		
----		SELECT * FROM dbo.AddressDetails WHERE numAddressID IN (SELECT numAddressID FROM ProjectsMaster)
------		SELECT * FROM ProjectsMaster WHERE ISNULL(numAddressID,0) > 0
------		SELECT * FROM ProjectsMaster WHERE numAddressID = 0
------		SELECT * FROM ProjectsMaster WHERE ISNULL(numAddressID,0) = 0
----
------		UPDATE ProjectsMaster SET numAddressID = NULL
----		DROP TABLE #tmpDiv
----	END 
----ROLLBACK
----
----/******************************************************************
----Project: Time & Expense   Date: 09.01.2013
----Comments: Task 24 : Slide # 5 
----*******************************************************************/
----BEGIN TRANSACTION
----
----GO
----DECLARE @v sql_variant 
----SET @v = N'Contact->1,Organization->2,Opportunity->3, Project ->4'
----EXECUTE sp_updateextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'AddressDetails', N'COLUMN', N'tintAddressOf'
----GO
----
----GO
----ALTER TABLE ProjectsMaster ADD [numAddressID] [numeric] (18, 0) NULL
----GO
----
----GO
----INSERT INTO dbo.DycFormField_Mapping 
----(numFieldID, numModuleID,numDomainID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,[order],tintRow,
----  tintColumn,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,bitAllowFiltering,
----  bitRequired,numFormFieldID,intSectionID,bitAllowGridColor ) 
----SELECT 428,3,NULL,13,0,NULL,'Shipping Address','Popup','ShippingAddress','openAddress',46,28,2,1,0,0,0,0,1,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL
------SELECT * FROM dbo.DycFormField_Mapping WHERE numFormID = 13
----GO
----ROLLBACK TRANSACTION
----
----/******************************************************************
----Project: Time & Expense   Date: 08.01.2013
----Comments: Task 24 : Slide # 5 
----*******************************************************************/
----BEGIN TRANSACTION
----GO
----INSERT INTO dbo.DycFormField_Mapping 
----(numFieldID, numModuleID,numDomainID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,[order],tintRow,
----  tintColumn,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,bitAllowFiltering,
----  bitRequired,numFormFieldID,intSectionID,bitAllowGridColor ) 
----SELECT  120,3,NULL,43,0,0,'Business Process','SelectBox',NULL,NULL,25,NULL,NULL,1,0,0,1,0,0,1,NULL,NULL,NULL,1,NULL,1845,NULL,1
----UNION ALL
----SELECT  121,3,NULL,43,0,0,'Total Progress','SelectBox',NULL,NULL,0,NULL,NULL,1,0,0,1,NULL,NULL,1,NULL,NULL,NULL,1,NULL,1511,NULL,NULL
----UNION ALL
----SELECT  96,3,NULL,43,0,0,'Sales Order Name','TextBox',NULL,NULL,6,NULL,NULL,1,0,0,1,NULL,NULL,1,NULL,NULL,NULL,1,NULL,1569,NULL,NULL
----
------SELECT * FROM dbo.DycFormField_Mapping WHERE numFormID =43
----GO
----ROLLBACK TRANSACTION
----
----
----/******************************************************************
----Project: Time & Expense   Date: 03.01.2013
----Comments: Task 24 : Slide # 1
----*******************************************************************/
----BEGIN TRANSACTION
----GO
------- ADD IsEnableResourceScheduling To Domain Details
----ALTER TABLE Domain ADD IsEnableResourceScheduling BIT NULL
----GO
----ROLLBACK TRANSACTION

INSERT INTO dbo.DycFormField_Mapping 
	(numModuleID,numFieldID,numDomainID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,[order],
	 tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,
	 bitAllowFiltering,bitRequired,numFormFieldID,intSectionID,bitAllowGridColor)
SELECT DISTINCT numModuleID,numFieldID,numDomainID,98,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,1,
	 tintRow,tintColumn,1,0,bitDefault,1,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,1,1,
	 0,bitRequired,NULL,1,NULL 
	 FROM [dbo].[DycFieldMaster]
	 WHERE [dbo].[DycFieldMaster].[numFieldID] IN (308)