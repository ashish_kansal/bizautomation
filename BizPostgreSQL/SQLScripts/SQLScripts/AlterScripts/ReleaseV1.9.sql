/******************************************************************
Project: Release 1.9 Date: 21.08.2013
Comments: 
*******************************************************************/

/*******************Kamal Script******************/
--/************************************************************************************************/
--/************************26_July_2013*******************************************************************/
--/************************************************************************************************/

ALTER TABLE dbo.Domain ADD
	bitDiscountOnUnitPrice bit NULL
	

--SELECT * FROM dbo.ReportFieldGroupMaster
INSERT INTO ReportFieldGroupMaster SELECT 'BizDocs Items Tax Field',1,NULL,NULL,NULL	

INSERT INTO ReportFieldGroupMaster SELECT 'BizDocs Tax Field',1,NULL,NULL,NULL	

--SELECT * FROM dbo.ReportModuleGroupMaster
--SELECT * FROM dbo.ReportModuleGroupFieldMappingMaster
INSERT INTO dbo.ReportModuleGroupFieldMappingMaster SELECT 9,25 

INSERT INTO dbo.ReportModuleGroupFieldMappingMaster SELECT 8,26 


--*******Change Opportunities to Transactions (Also SP:USP_ManageTabsInCuSFields)*********************************************************************
--exec USP_GetCustomFieldsTab @LocID=13,@numDomainID=169,@Type=-1,@numGroupID=1
--SELECT * FROM CFw_Grp_Master

--SELECT * FROM CFw_Grp_Master WHERE Loc_Id=13 AND grp_name='Opportunities'
UPDATE CFw_Grp_Master SET grp_name='Transactions' WHERE Loc_Id=13 AND grp_name='Opportunities'

--SELECT * FROM CFw_Grp_Master WHERE Loc_Id=12 AND grp_name='Opportunities'
UPDATE CFw_Grp_Master SET grp_name='Transactions' WHERE Loc_Id=12 AND grp_name='Opportunities'






CREATE TABLE [dbo].[ParentChildCustomFieldMap](
	[numParentChildFieldID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numDomainID] [numeric](18, 0) NULL,
	[tintParentModule] [tinyint] NULL,
	[numParentFieldID] [nchar](10) NULL,
	[tintChildModule] [tinyint] NULL,
	[numChildFieldID] [tinyint] NULL,
 CONSTRAINT [PK_ParentChildCustomFieldMap] PRIMARY KEY CLUSTERED 
(
	[numParentChildFieldID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]



INSERT INTO [dbo].[ReportFieldGroupMappingMaster]([numReportFieldGroupID], [numFieldID], [vcFieldName], [vcAssociatedControlType], [vcFieldDataType], [bitAllowSorting], [bitAllowGrouping], [bitAllowAggregate], [bitAllowFiltering])
SELECT 7, numFieldId, N'Item ID', N'TextBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster 
WHERE numModuleID=4 AND vcOrigDbColumnName='numItemCode' AND numFieldId=211 


INSERT INTO [dbo].[ReportFieldGroupMappingMaster]([numReportFieldGroupID], [numFieldID], [vcFieldName], [vcAssociatedControlType], [vcFieldDataType], [bitAllowSorting], [bitAllowGrouping], [bitAllowAggregate], [bitAllowFiltering])
SELECT 7, numFieldId, N'Description', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster 
WHERE numModuleID=4 AND vcOrigDbColumnName='txtItemDesc' AND numFieldId=191 


/*******************Manish Script******************/
/******************************************************************
Project: BACRMUI   Date: 15.Aug.2013
Comments: Add Shipping Label Image Width,Height fields in domain details
*******************************************************************/
BEGIN TRANSACTION
ALTER TABLE Domain ADD intShippingImageWidth INT,intShippingImageHeight INT

ALTER TABLE Domain ADD numTotalInsuredValue NUMERIC(10,2),numTotalCustomsValue NUMERIC(10,2)

UPDATE dynamicFormMaster SET vcLocationID='5' WHERE numFormId=67
ROLLBACK

/******************************************************************
Project: BACRMUI   Date: 14.Aug.2013
Comments: Adding USPS Packages
*******************************************************************/
BEGIN TRANSACTION
INSERT INTO dbo.CustomPackages 
(numPackageTypeID,vcPackageName,fltWidth,fltHeight,fltLength,fltTotalWeight,numShippingCompanyID)
 SELECT 0,'None',0,0,0,0,90
 UNION ALL
 SELECT 1,'Postcards',0,0,0,0,90
 UNION ALL
 SELECT 3,'Large Envelope',0,0,0,0,90
 UNION ALL
 SELECT 4,'Flat Rate Envelope',0,0,0,0,90
 UNION ALL
 SELECT 12,'Flat Rate Box',0,0,0,0,90
 UNION ALL
 SELECT 13,'Small Flat Rate Box',0,0,0,0,90
 UNION ALL
 SELECT 14,'Large Flat Rate Box',0,0,0,0,90
 UNION ALL
 SELECT 15,'Rectangular',0,0,0,0,90
 UNION ALL
 SELECT 16,'Non Rectangular',0,0,0,0,90
 UNION ALL
 SELECT 19,'Matter For The Blind',0,0,0,0,90
 
ROLLBACK	

/******************************************************************
Project: BACRMUI   Date: 12.Aug.2013
Comments: Add Customs Amount field in Shipping report
*******************************************************************/
BEGIN TRANSACTION
ALTER TABLE ShippingReport ADD numTotalCustomsValue NUMERIC(18,2)
ROLLBACK

/******************************************************************
Project: BACRMUI   Date: 8.Aug.2013
Comments: Add Shipping States for Canada
*******************************************************************/
BEGIN TRANSACTION
INSERT INTO dbo.ShippingStateMaster 
(vcStateName,vcStateCode,numShipCompany) 
SELECT 'Alberta','AB',91
UNION ALL
SELECT 'British Columbia','BC',91
UNION ALL
SELECT 'Manitoba','MB',91
UNION ALL
SELECT 'New Brunswick','NB',91
UNION ALL
SELECT 'New Foundland','NF',91
UNION ALL
SELECT 'Northwest Territories','NT',91
UNION ALL
SELECT 'Nova Scotia','NS',91
UNION ALL
SELECT 'Ontario','ON',91
UNION ALL
SELECT 'Prince Edward Island','PE',91
UNION ALL
SELECT 'Quebec','PQ',91
UNION ALL
SELECT 'Saskatchewan','SK',91
UNION ALL
SELECT 'Yukon Territories','YT',91
UNION ALL
SELECT 'Alberta','AB',90
UNION ALL
SELECT 'British Columbia','BC',90
UNION ALL
SELECT 'Manitoba','MB',90
UNION ALL
SELECT 'New Brunswick','NB',90
UNION ALL
SELECT 'New Foundland','NF',90
UNION ALL
SELECT 'Northwest Territories','NT',90
UNION ALL
SELECT 'Nova Scotia','NS',90
UNION ALL
SELECT 'Ontario','ON',90
UNION ALL
SELECT 'Prince Edward Island','PE',90
UNION ALL
SELECT 'Quebec','PQ',90
UNION ALL
SELECT 'Saskatchewan','SK',90
UNION ALL
SELECT 'Yukon Territories','YT',90
UNION ALL
SELECT 'Alberta','AB',88
UNION ALL
SELECT 'British Columbia','BC',88
UNION ALL
SELECT 'Manitoba','MB',88
UNION ALL
SELECT 'New Brunswick','NB',88
UNION ALL
SELECT 'New Foundland','NF',88
UNION ALL
SELECT 'Northwest Territories','NT',88
UNION ALL
SELECT 'Nova Scotia','NS',88
UNION ALL
SELECT 'Ontario','ON',88
UNION ALL
SELECT 'Prince Edward Island','PE',88
UNION ALL
SELECT 'Quebec','PQ',88
UNION ALL
SELECT 'Saskatchewan','SK',88
UNION ALL
SELECT 'Yukon Territories','YT',88

ROLLBACK


/******************************************************************
Project: BACRMUI   Date: 7.Aug.2013
Comments: Add Item Barcode Fields
*******************************************************************/
BEGIN TRANSACTION
	INSERT dbo.DynamicFormMaster 
	( numFormId,vcFormName,cCustomFieldsAssociated,cAOIAssociated,bitDeleted,tintFlag,bitWorkFlow,vcLocationID,bitAllowGridColor )
	VALUES 	( /* numFormId - numeric(10, 0) */ 67 , /* vcFormName - nvarchar(50) */ N'Item Barcode Fields', /* cCustomFieldsAssociated - char(1) */ 'Y',
	  /* cAOIAssociated - char(1) */ 'N', /* bitDeleted - bit */ 0, /* tintFlag - tinyint */ 0, /* bitWorkFlow - bit */ NULL, 
	  /* vcLocationID - varchar(50) */ '5', /* bitAllowGridColor - bit */ NULL ) 
	  
--- exec usp_getDynamicFormList @numDomainID=1,@tintFlag=0,@bitWorkFlow=0,@bitAllowGridColor=0
		
	INSERT INTO dbo.DycFormField_Mapping 
	(numModuleID,numFieldID,numDomainID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,[order],
	 tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,
	 bitAllowFiltering,bitRequired,numFormFieldID,intSectionID,bitAllowGridColor)
	 SELECT numModuleID,numFieldID,numDomainID,67,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,1,
	 tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,1,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,0,bitExport,
	 1,bitRequired,numFormFieldID,intSectionID,bitAllowGridColor 
	 FROM dbo.DycFormField_Mapping WHERE numFormID = 29
	 AND vcFieldName NOT IN ('Serial Number','')
	 UNION
	 SELECT numModuleID,numFieldID,numDomainID,67,bitAllowEdit,bitInlineEdit,'Item ID',vcAssociatedControlType,vcPropertyName,PopupFunctionName,1,
	 tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,1,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,0,bitExport,
	 1,bitRequired,numFormFieldID,intSectionID,bitAllowGridColor 
	 FROM dbo.DycFormField_Mapping WHERE numFormID = 65 AND vcFieldName = 'Item Code'
	 
	 UPDATE dynamicFormMaster SET vcLocationID='5' WHERE numFormId=67
	 --SELECT * FROM dbo.DycFormField_Mapping WHERE numFormID = 67
	 --DELETE FROM dbo.DycFormField_Mapping WHERE numFormID = 67
ROLLBACK

/******************************************************************
Project: BACRMUI   Date: 6.Aug.2013
Comments: Add Item ID in Products & Services 
*******************************************************************/
BEGIN TRANSACTION

INSERT INTO dbo.DycFormField_Mapping 
(numModuleID,numFieldID,numDomainID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,[order],
 tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,
 bitAllowFiltering,bitRequired,numFormFieldID,intSectionID,bitAllowGridColor)
SELECT numModuleID,numFieldID,numDomainID,26,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,[order],
	   tintRow,tintColumn,bitInResults,0,bitDefault,1,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,
	   bitAllowFiltering,bitRequired,numFormFieldID,intSectionID,bitAllowGridColor
FROM dbo.DycFormField_Mapping WHERE numFormID = 20 AND vcFieldName = 'Item ID'	   
 
exec USP_GetColumnConfiguration1 @numDomainID=1,@numUserCntId=1,@FormId=26,@numtype=0,@numViewID=0

ROLLBACK



    
<add key="PaypalServerUrl" value="https://api-3t.paypal.com/nvp" />
<add key="PaypalSandboxUrl" value="https://api-3t.sandbox.paypal.com/nvp" />
<add key="PaypalRedirectSandboxUrl" value="https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_express-checkout&amp;useraction=commit&amp;token=" />
<add key="PaypalRedirectServerUrl" value="https://www.paypal.com/cgi-bin/webscr?cmd=_express-checkout&amp;useraction=commit&amp;token=" />   
