/******************************************************************
Project: Release 5.6 Date: 28.May.2016
Comments: ALTER SCRIPTS
*******************************************************************/

/*************************** PRASANT ****************************/

======================================================================

ALTER TABLE Item
	ADD numNoItemIntoContainer NUMERIC(18,0) DEFAULT 0

======================================================================

ALTER TABLE Item
	ADD numContainer NUMERIC(18,0) DEFAULT 0

=====================================================================


BEGIN TRY
BEGIN TRANSACTION
	

	DECLARE @numMAXPageNavID INT
	SELECT @numMAXPageNavID = MAX(numPageNavID) FROM PageNavigationDTL


	INSERT INTO PageNavigationDTL
	(
		numPageNavID,numModuleID,numParentID,vcPageNavName,vcNavURL,bitVisible,bitAddIsPopUp,numTabID,intSortOrder,vcAddURL
	)
	VALUES
	(
		(@numMAXPageNavID + 1),37,173,'Containers','../Items/frmItemList.aspx?Page=Containers&ItemGroup=0',1,1,80,1,'~/Items/frmNewItem.aspx?FormID=86&ItemType=Containers'
	)

	INSERT INTO dbo.TreeNavigationAuthorization
	(
		numGroupID,
		numTabID,
		numPageNavID,
		bitVisible,
		numDomainID,
		tintType
	)
	SELECT  
		numGroupID,
		80,
		(@numMAXPageNavID + 1),
		1,
		numDomainID,
		1
	FROM    
		AuthenticationGroupMaster
	WHERE
		tintGroupType=1

COMMIT
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		ERROR_MESSAGE(),
		ERROR_NUMBER(),
		ERROR_SEVERITY(),
		ERROR_STATE(),
		ERROR_LINE(),
		ISNULL(ERROR_PROCEDURE(), '-');
END CATCH



======================================================================

ALTER TABLE Item
	ADD bitContainer BIT DEFAULT 0

=====================================================================


ALTER TABLE Domain
	ADD bitApprovalforOpportunity BIT

ALTER TABLE Domain 
	ADD intOpportunityApprovalProcess INT

ALTER TABLE OpportunityMaster
	ADD intPromotionApprovalStatus INT DEFAULT 0

ALTER TABLE OpportunityMaster
	ADD intChangePromotionStatus INT DEFAULT 0

====================================================================


BEGIN TRY
BEGIN TRANSACTION
	

	DECLARE @numMAXPageNavID INT
	SELECT @numMAXPageNavID = MAX(numPageNavID) FROM PageNavigationDTL


	INSERT INTO PageNavigationDTL
	(
		numPageNavID,numModuleID,numParentID,vcPageNavName,vcNavURL,bitVisible,numTabID,intSortOrder
	)
	VALUES
	(
		(@numMAXPageNavID + 1),1,0,'Activities','',1,36,1
	),
	(
		(@numMAXPageNavID + 2),1,(@numMAXPageNavID + 1),'Action Items & Meetings','../common/frmTicklerdisplay.aspx?SelectedIndex=0',1,36,1
	),
	(
		(@numMAXPageNavID + 3),1,(@numMAXPageNavID + 1),'Opportunities & Projects','../common/frmTicklerdisplay.aspx?SelectedIndex=1',1,36,1
	),
	(
		(@numMAXPageNavID + 4),1,(@numMAXPageNavID + 1),'Cases','../common/frmTicklerdisplay.aspx?SelectedIndex=2',1,36,1
	),
	(
		(@numMAXPageNavID + 5),1,(@numMAXPageNavID + 1),'Approval Requests','../common/frmTicklerdisplay.aspx?SelectedIndex=3',1,36,1
	)

	INSERT INTO dbo.TreeNavigationAuthorization
	(
		numGroupID,
		numTabID,
		numPageNavID,
		bitVisible,
		numDomainID,
		tintType
	)
	SELECT  
		numGroupID,
		36,
		(@numMAXPageNavID + 1),
		1,
		numDomainID,
		1
	FROM    
		AuthenticationGroupMaster
	WHERE
		tintGroupType=1

	INSERT INTO dbo.TreeNavigationAuthorization
	(
		numGroupID,
		numTabID,
		numPageNavID,
		bitVisible,
		numDomainID,
		tintType
	)
	SELECT  
		numGroupID,
		36,
		(@numMAXPageNavID + 2),
		1,
		numDomainID,
		1
	FROM    
		AuthenticationGroupMaster
	WHERE
		tintGroupType=1

	INSERT INTO dbo.TreeNavigationAuthorization
	(
		numGroupID,
		numTabID,
		numPageNavID,
		bitVisible,
		numDomainID,
		tintType
	)
	SELECT  
		numGroupID,
		36,
		(@numMAXPageNavID + 3),
		1,
		numDomainID,
		1
	FROM    
		AuthenticationGroupMaster
	WHERE
		tintGroupType=1

	INSERT INTO dbo.TreeNavigationAuthorization
	(
		numGroupID,
		numTabID,
		numPageNavID,
		bitVisible,
		numDomainID,
		tintType
	)
	SELECT  
		numGroupID,
		36,
		(@numMAXPageNavID + 4),
		1,
		numDomainID,
		1
	FROM    
		AuthenticationGroupMaster
	WHERE
		tintGroupType=1

	INSERT INTO dbo.TreeNavigationAuthorization
	(
		numGroupID,
		numTabID,
		numPageNavID,
		bitVisible,
		numDomainID,
		tintType
	)
	SELECT  
		numGroupID,
		36,
		(@numMAXPageNavID + 5),
		1,
		numDomainID,
		1
	FROM    
		AuthenticationGroupMaster
	WHERE
		tintGroupType=1

COMMIT
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		ERROR_MESSAGE(),
		ERROR_NUMBER(),
		ERROR_SEVERITY(),
		ERROR_STATE(),
		ERROR_LINE(),
		ISNULL(ERROR_PROCEDURE(), '-');
END CATCH


===================================================================


--------------------------------------------------------------


INSERT INTO BizFormWizardModule(vcModule) values('Web to Case')

---------------------------------

INSERT INTO DynamicFormMaster
			(numFormId, vcFormName, cCustomFieldsAssociated, cAOIAssociated, bitDeleted
			, tintFlag, bitWorkFlow, vcLocationID, bitAllowGridColor, numBizFormModuleID)
		VALUES
			(128,'Web to Case Form','Y','Y',0,0,NULL,NULL,NULL,10)

--------------------------------------

--INSERT INTO DynamicFormFieldMaster
--			(numFormID, vcFormFieldName, vcFieldType, vcAssociatedControlType, vcDbColumnName, vcListItemType, numListID, bitDeleted, vcFieldDataType, vcOrigDbColumnName, bitAllowEdit, vcLookBackTableName, bitDefault, [order], bitInResults)
--			VALUES
--			(128,'Email','R','EditBox','vcEmail','',0,0,'V','vcEmail',1,'AdditionalContactsInformation',0,NULL,1)

INSERT INTO DycFormField_Mapping
			(numModuleID, numFieldID, numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitAllowFiltering,numFormFieldID)
			VALUES
			(7,128,128,0,0,'Email','TextBox',1,0,0,1,2113)

INSERT INTO DycFormField_Mapping
			(numModuleID, numFieldID, numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitAllowFiltering,numFormFieldID)
			VALUES
			(7,127,128,0,0,'Subject','TextBox',1,0,0,1,1629)
			
INSERT INTO DycFormField_Mapping
			(numModuleID, numFieldID, numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitAllowFiltering,numFormFieldID)
			VALUES
			(7,129,128,0,0,'Priority','SelectBox',1,0,0,1,1631)

INSERT INTO DycFormField_Mapping
			(numModuleID, numFieldID, numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitAllowFiltering,numFormFieldID)
			VALUES
			(7,130,128,0,0,'Origin','SelectBox',1,0,0,1,1632)

INSERT INTO DycFormField_Mapping
			(numModuleID, numFieldID, numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitAllowFiltering,numFormFieldID)
			VALUES
			(7,145,128,0,0,'Description','TextArea',1,0,0,1,1643)

INSERT INTO DycFormField_Mapping
			(numModuleID, numFieldID, numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitAllowFiltering,numFormFieldID)
			VALUES
			(7,133,128,0,0,'Type','SelectBox',1,0,0,1,1635)
			


INSERT INTO DycFormField_Mapping
			(numModuleID, numFieldID, numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitAllowFiltering,numFormFieldID)
			VALUES
			(7,136,128,0,0,'Reason','SelectBox',1,0,0,1,1638)


-----------------------------------------------------------------------------------------------------

INSERT INTO AccountingChargeTypes (vcChageType, vcChargeAccountCode, chChargeCode)
VALUES('Reimbursable Expense Receivable','','RE')

ALTER TABLE TimeAndExpense ADD numServiceItemID numeric(18,0)

ALTER TABLE TimeAndExpense ADD numClassID numeric(18,0)

ALTER TABLE TimeAndExpense ADD numApprovalComplete numeric(18,0) DEFAULT 0

---------------------------------------------------------

ALTER TABLE DOMAIN ADD intTimeExpApprovalProcess INT DEFAULT 0
-----------------------------------------------------------

ALTER TABLE TimeAndExpense ADD numExpId numeric(18,0)

-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
	DECLARE @numPageNavID numeric 
	SELECT @numPageNavID = MAX(numPageNavID) + 1  FROM dbo.PageNavigationDTL 
	INSERT INTO dbo.PageNavigationDTL
			( numPageNavID ,
			  numModuleID ,
			  numParentID ,
			  vcPageNavName ,
			  vcNavURL ,
			  vcImageURL ,
			  bitVisible ,
			  numTabID
			)
	SELECT @numPageNavID ,13,66,'Approval Process','../admin/frmManageApprovalConfig.aspx',NULL,1,-1
	

	
	SELECT * INTO #temp FROM
    (
		SELECT 
			ROW_NUMBER() OVER(ORDER BY numDomainId DESC) AS Row, 
			numDomainId 
		FROM 
			Domain 
		WHERE 
			numDomainId <> -255
	) TABLE2
	
	DECLARE @RowCount INT
	SET @RowCount = (SELECT COUNT(Row) FROM #temp) 
	
	DECLARE @I INT
	DECLARE @numDomainId NUMERIC(18,0);
	
	SET @I = 1

	WHILE (@I <= @RowCount)
	BEGIN
			
		SELECT @numDomainId = numDomainID FROM #temp WHERE Row = @I
		
		DELETE FROM TreeNavigationAuthorization WHERE numPageNavID=@numPageNavID AND numDomainID = @numDomainId

		SELECT * INTO #tempData1 FROM
		(
		 SELECT    T.numTabID,
						CASE WHEN bitFixed = 1
							 THEN CASE WHEN EXISTS ( SELECT numTabName
													 FROM   TabDefault
													 WHERE  numTabId = T.numTabId
															AND tintTabType = T.tintTabType AND numDomainID =D.numDomainID)
									   THEN ( SELECT TOP 1
														numTabName
											  FROM      TabDefault
											  WHERE     numTabId = T.numTabId
														AND tintTabType = T.tintTabType AND numDomainID = D.numDomainID
											)
									   ELSE T.numTabName
								  END
							 ELSE T.numTabName
						END numTabname,
						vcURL,
						bitFixed,
						1 AS tintType,
						T.vcImage,
						D.numDomainID,
						A.numGroupID
			  FROM      TabMaster T
						CROSS JOIN Domain D --ON D.numDomainID = T.numDomainID
						LEFT JOIN AuthenticationGroupMaster A ON A.numDomainID = D.numDomainId
			  WHERE     bitFixed = 1
						AND D.numDomainId <> -255
						AND t.tintTabType = 1
	          
			  UNION ALL
			  SELECT    -1 AS [numTabID],
						'Administration' numTabname,
						'' vcURL,
						1 bitFixed,
						1 AS tintType,
						'' AS vcImage,
						D.numDomainID,
						A.numGroupID
			  FROM      Domain D
						JOIN AuthenticationGroupMaster A ON A.numDomainID = D.numDomainId
						WHERE D.numDomainID = @numDomainID
			  
			  UNION ALL
			  SELECT    -3 AS [numTabID],
						'Advanced Search' numTabname,
						'' vcURL,
						1 bitFixed,
						1 AS tintType,
						'' AS vcImage,
						D.numDomainID,
						A.numGroupID
			  FROM      Domain D
						JOIN AuthenticationGroupMaster A ON A.numDomainID = D.numDomainId          
			) TABLE2       		
	                    
		INSERT  INTO dbo.TreeNavigationAuthorization
			(
			  numGroupID,
			  numTabID,
			  numPageNavID,
			  bitVisible,
			  numDomainID,
			  tintType
			)
			SELECT  numGroupID,
					PND.numTabID,
					numPageNavID,
					bitVisible,
					numDomainID,
					tintType
			FROM    dbo.PageNavigationDTL PND
					JOIN #tempData1 TD ON TD.numTabID = PND.numTabID 
					WHERE PND.numPageNavID= @numPageNavID
			
			DROP TABLE #tempData1
			
			SET @I = @I  + 1
	END
	
	DROP TABLE #temp
