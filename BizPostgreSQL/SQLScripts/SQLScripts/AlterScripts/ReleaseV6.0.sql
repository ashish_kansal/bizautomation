/******************************************************************
Project: Release 6.0 Date: 10.SEP.2016
Comments: ALTER SCRIPTS
*******************************************************************/

/*************************** SANDEEP ****************************/

UPDATE DycFormField_Mapping SET bitAllowFiltering=1 WHERE numFormID=39 AND vcPropertyName='InventoryStatus'
ALTER TABLE OpportunityMaster ADD bitPostSellDiscountUsed BIT

-------------------------------------------------------------


USE [Production.2014]
GO

/****** Object:  Table [dbo].[OpportunityItemsReceievedLocation]    Script Date: 10-Sep-16 10:24:51 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[OpportunityItemsReceievedLocation](
	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numDomainID] [numeric](18, 0) NOT NULL,
	[numOppID] [numeric](18, 0) NOT NULL,
	[numOppItemID] [numeric](18, 0) NOT NULL,
	[numWarehouseItemID] [numeric](18, 0) NOT NULL,
	[numUnitReceieved] [float] NOT NULL,
 CONSTRAINT [PK_OpportunityItemsReceievedLocation] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[OpportunityItemsReceievedLocation]  WITH CHECK ADD  CONSTRAINT [FK_OpportunityItemsReceievedLocation_OpportunityItems] FOREIGN KEY([numOppItemID])
REFERENCES [dbo].[OpportunityItems] ([numoppitemtCode])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[OpportunityItemsReceievedLocation] CHECK CONSTRAINT [FK_OpportunityItemsReceievedLocation_OpportunityItems]
GO

------------------------------------------------------------------

USE [Production.2014]
GO

/****** Object:  Table [dbo].[OpportunityItemsReceievedLocationReturn]    Script Date: 10-Sep-16 10:25:22 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[OpportunityItemsReceievedLocationReturn](
	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numOIRLID] [numeric](18, 0) NOT NULL,
	[numReturnID] [numeric](18, 0) NOT NULL,
	[numReturnItemID] [numeric](18, 0) NOT NULL,
	[numReturnedQty] [float] NOT NULL,
 CONSTRAINT [PK_OpportunityItemsReceievedLocationReturn] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[OpportunityItemsReceievedLocationReturn]  WITH CHECK ADD  CONSTRAINT [FK_OpportunityItemsReceievedLocationReturn_OpportunityItemsReceievedLocation] FOREIGN KEY([numOIRLID])
REFERENCES [dbo].[OpportunityItemsReceievedLocation] ([ID])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[OpportunityItemsReceievedLocationReturn] CHECK CONSTRAINT [FK_OpportunityItemsReceievedLocationReturn_OpportunityItemsReceievedLocation]
GO
