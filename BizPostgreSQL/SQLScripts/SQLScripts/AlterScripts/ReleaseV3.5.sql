/******************************************************************
Project: Release 3.5 Date: 02.SEP.2014
Comments: 
*******************************************************************/

------------------- SACHIN ------------------

--New Script
ALTER TABLE dbo.DycFieldMaster ADD
	vcGroup  varchar(100) NULL

	ALTER TABLE dbo.DycFieldMaster ADD
	intWFCompare  int NULL

ALTER TABLE dbo.DycFieldMaster ADD
	vcWFCompareField  varchar(150) NULL


update DycFieldMaster set vcGroup='BizDoc Fields' where vcLookBackTableName='OpportunityBizDocs'

update DycFieldMaster set vcGroup='Order Fields' where vcLookBackTableName='OpportunityMaster'

update DycFieldMaster set intWFCompare=1 where vcOrigDbColumnName in ('monDealAmount','monAmountPaid') and numModuleID=3
 
update DycFieldMaster set vcWFCompareField='monPAmount' where vcOrigDbColumnName ='monDealAmount' and vcLookBackTableName='OpportunityMaster' and numModuleID=3 and intWFCompare=1

update DycFieldMaster set vcWFCompareField='monAmountPaid' where vcOrigDbColumnName ='monDealAmount' and vcLookBackTableName='OpportunityBizDocs' and numModuleID=3 and intWFCompare=1

update DycFieldMaster set vcWFCompareField='monDealAmount' where vcOrigDbColumnName ='monAmountPaid' and vcLookBackTableName='OpportunityBizDocs' and numModuleID=3 and intWFCompare=1

update DycFieldMaster set vcGroup='Org.Details Fields'  where vcGroup='Organization Fields'

update DycFieldMaster set vcGroup='Org.Details Fields'  where vcGroup!='Org. | Accounting Fields'  and vcLookBackTableName='DivisionMaster'
--end of new
update DycFieldMaster set vcGroup='Contact Fields' where vcLookBackTableName='AdditionalContactsInformation'
update DycFieldMaster set vcGroup='Action Items Fields' where vcLookBackTableName='Communication'
update DycFieldMaster set vcGroup='Cases Fields' where vcLookBackTableName='Cases'
update DycFieldMaster set vcGroup='Project Fields' where vcLookBackTableName='ProjectsMaster'
update DycFieldMaster set vcGroup='Busniess Process Fields' where vcLookBackTableName='StagePercentageDetails'
update DycFieldMaster set vcGroup='Project Fields' where vcLookBackTableName='ProjectProgress'



ALTER TABLE dbo.WorkFlowActionList ADD
	vcEmailSendTo varchar(500) NULL

	update dycfieldMaster set vcGroup='Organization Fields' where vcGroup='Order Details'

	UPdate DycFieldMaster set vcListItemType='CR' ,vcGroup='Org. | Accounting Fields' where vcDbColumnName='numCurrencyID' and vcListItemType='U' 
	
	ALTER TABLE dbo.WorkFlowActionList ADD
	vcMailSubject varchar(500) NULL

	ALTER TABLE dbo.WorkFlowActionList ADD
	vcMailBody ntext NULL

	ALTER TABLE dbo.WorkFlowconditionList ADD
	intCompare  int NULL


	
	ALTER TABLE dbo.DycFieldMaster ADD
	intWFCompare int NULL


update dycFieldMaster set intWFCompare=1 where numFieldID in (268,269)


exec USP_GetWorkFlowFormFieldMaster 1,68
exec USP_GetWorkFlowFormFieldMaster 1,70

select * from DycFieldMaster where  vcLookBackTableName ='CFW_FLD_Values'
update DycFieldMaster set vcGroup='Organization Details' where vcLookBackTableName='AddressDetails'
update DycFieldMaster set vcGroup='Organization Details' where vcLookBackTableName='CompanyInfo'
update DycFieldMaster set vcGroup='Org. | Accounting Fields' where numFieldId in (20547,20548)

update DycFieldMaster set vcGroup='Organization Fields' where numFieldId in (10530,30549)


select * from ModuleMaster

select * from PageNavigationDTL where vcPageNavName='Auto Routing Rules'

select * from PageNavigationDTL where vcPageNavName='Automation Rules'


Update PageNavigationDTL set numParentID=66,vcImageURL=NULL where numPageNavID=65 --'../images/RoundLink.gif'

select * from PageNavigationDTL where numPageNavID=65

select * from PageNavigationDTL where vcPageNavName='e-Mail Broadcast'
Update PageNavigationDTL set vcNavURL='../Marketing/frmEmailBroadcastConfigDetail.aspx' where numPageNavID=17 


select * from PageNavigationDTL where vcPageNavName='Packaging Automation'

Update PageNavigationDTL set vcNavURL='../Ecommerce/frmShippingLabelBatchProcessingList.aspx' where numPageNavID=199 

select * from PageNavigationDTL where vcPageNavName='Custom Box' --../Marketing/frmEmailBroadcastConfigDetail.aspx 203,201
select * from PageNavigationDTL where vcPageNavName='Packaging/Box Rules'
select * from PageNavigationDTL where vcPageNavName='Pay Bill WorkFlow'
Update PageNavigationDTL set bitVisible=0 where numPageNavID in (201,203,235)



	ALTER TABLE dbo.PageNavigationDTL ADD
	intSortOrder int NULL

select * from PageNavigationDTL where numParentID=73
select * from PageNavigationDTL where numPageNavID=120
Update PageNavigationDTL set vcPageNavName='Global Settings',intsortOrder=1 where numPageNavID=120 --Ecommerce Settings
Update PageNavigationDTL set intsortOrder=2 where numPageNavID=103 --Categories
Update PageNavigationDTL set vcPageNavName='Location Mapping',intsortOrder=3 where numPageNavID=119 --Warehouse Mapping
Update PageNavigationDTL set vcPageNavName='Shipping',intsortOrder=4 where numPageNavID=000 --Shipping Company(new-shipping)
Update PageNavigationDTL set vcPageNavName='Payment Gateways',intsortOrder=5 where numPageNavID=143 --Payment Gateways
Update PageNavigationDTL set vcPageNavName='Marketplaces',intsortOrder=6 where numPageNavID=150 --Market Places(ol-e-Commerce API)
Update PageNavigationDTL set vcPageNavName='Promotions',intsortOrder=7 where numPageNavID=187 --Promotions & Offers
Update PageNavigationDTL set vcPageNavName='Error Codes',intsortOrder=8 where numPageNavID=197 --Error Message 
Update PageNavigationDTL set vcPageNavName='Reviews & Ratings',intsortOrder=9 where numPageNavID=217 --Reviews & Ratings
Update PageNavigationDTL set vcPageNavName='Design',intsortOrder=10 where numPageNavID=157 --Design Cart

Update PageNavigationDTL set numParentID=236 where numPageNavID in (121,193) --under Shipping

Update PageNavigationDTL set vcPageNavName='Shipping Carrier Setup' where numPageNavID in (121)
Update PageNavigationDTL set vcPageNavName='Shipping Rules' where numPageNavID in (193)

--Ecommerce settings main
update PageNavigationDTL set vcNavURL='../Items/frmECommerceSettings.aspx',vcPageNavName='e-Commerce' where numPageNavID=73

--Global Settings
Update  PageNavigationDTL set vcPageNavName= 'Global Settings' where numPageNavID=79

Update PageNavigationDTL set intSortOrder=101 where numPageNavID=35 --AdminiStration
Update PageNavigationDTL set intSortOrder=102 where numPageNavID=57 --User Administration
Update PageNavigationDTL set intSortOrder=103 where numPageNavID=61 --Master List Admin
Update PageNavigationDTL set intSortOrder=104 where numPageNavID=64 --BizForms Wizard
Update PageNavigationDTL set intSortOrder=105 where numPageNavID=66 --Workflow Automation
Update PageNavigationDTL set intSortOrder=106 where numPageNavID=73 --Price & Cost Rules
Update PageNavigationDTL set intSortOrder=107 where numPageNavID=78 --e-Commerce
Update PageNavigationDTL set intSortOrder=108 where numPageNavID=101 --Recurring Template List
Update PageNavigationDTL set intSortOrder=109 where numPageNavID=109 --Field Management
Update PageNavigationDTL set intSortOrder=110 where numPageNavID=205 --Import Wizard


Update PageNavigationDTL set intSortOrder=2000 where numPageNavID=79 --AdminiStration
update PageNavigationDTL set vcImageURL='../images/Globe.png' where numPageNavID=79 



--Organization Details for ORder Module



INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength)
VALUES        (3,Null,'Annual Revenue','numAnnualRevID','numAnnualRevID',Null,'CompanyInfo','N','R','SelectBox',Null,'LI',6,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null)


--10542
--update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (3,30549,Null,70,1,0,'Annual Revenue','SelectBox','numAnnualRevID',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)





INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (3,Null,'Assigned By','numAssignedBy','numAssignedBy',Null,'DivisionMaster','N','R','SelectBox',Null,'U',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Organization Fields')


--10542
--update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (3,30550,Null,70,1,0,'Assigned By','SelectBox','numAssignedBy',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)




INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (3,Null,'Assigned To','numAssignedTo','numAssignedTo',Null,'DivisionMaster','N','R','SelectBox',Null,'U',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Organization Fields')


--10542
--update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (3,30551,Null,70,1,0,'Assigned To','SelectBox','numAssignedTo',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)


--exec USP_GetWorkFlowFormFieldMaster 1,68


INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (3,Null,'Campaign','numCampaignID','numCampaignID',Null,'DivisionMaster','N','R','SelectBox',Null,'C',24,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Organization Fields')

select * from DycFieldMaster
--10542
--update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (3,30555,Null,70,1,0,'Campaign','SelectBox','numCampaignID',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)

INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (3,Null,'Company Differentiation','numCompanyDiff','numCompanyDiff',Null,'DivisionMaster','N','R','SelectBox',Null,'LI',438,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Organization Fields')

select * from DycFieldMaster
--10542
--update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (3,30556,Null,70,1,0,'Company Differentiation','SelectBox','numCompanyDiff',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)


--Group
INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (3,Null,'Group','numGrpID','numGrpID',Null,'DivisionMaster','N','R','SelectBox',Null,'AG',38,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Organization Fields')

select * from DycFieldMaster
--10542
--update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (3,30557,Null,70,1,0,'Group','SelectBox','numGrpID',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)



INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (3,Null,'Follow-up Status','numFollowUpStatus','numFollowUpStatus',Null,'DivisionMaster','N','R','SelectBox',Null,'LI',30,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Organization Fields')

select * from DycFieldMaster
--10542
--update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (3,30558,Null,70,1,0,'Follow-up Status','SelectBox','numFollowUpStatus',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)



INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (3,Null,'Default Trading Currency','numCurrencyID','numCurrencyID',Null,'DivisionMaster','N','R','SelectBox',Null,'U',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Organization Fields')

select * from DycFieldMaster
--10542
--update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (3,30559,Null,70,1,0,'Default Trading Currency','SelectBox','numCurrencyID',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)



INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (3,Null,'Territory','numTerID','numTerID',Null,'DivisionMaster','N','R','SelectBox',Null,'T',78,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Organization Fields')

select * from DycFieldMaster
--10542
--update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (3,30560,Null,70,1,0,'Territory','SelectBox','numTerID',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)




INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (3,Null,'Organization Phone','vcComPhone','vcComPhone',Null,'DivisionMaster','V','R','TextBox',Null,'',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Organization Fields')

select * from DycFieldMaster
--10542
--update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (3,30561,Null,70,1,0,'Organization Phone','TextBox','vcComPhone',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)



INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (3,Null,'Organization Fax','vcComFax','vcComFax',Null,'DivisionMaster','V','R','TextBox',Null,'',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Organization Fields')

select * from DycFieldMaster
--10542
--update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (3,30563,Null,70,1,0,'Organization Fax','TextBox','vcComFax',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)




INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (3,Null,'Relationship Type','tintCRMType','tintCRMType',Null,'DivisionMaster','N','R','SelectBox',Null,'SYS',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Organization Fields')

select * from DycFieldMaster
--10542
--update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (3,30564,Null,70,1,0,'Relationship Type','SelectBox','tintCRMType',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)



INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (3,Null,'Organization Status','numStatusID','numStatusID',Null,'DivisionMaster','N','R','SelectBox',Null,'LI',1,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Organization Fields')

select * from DycFieldMaster
--10542
--update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (3,30565,Null,70,1,0,'Organization Status','SelectBox','numStatusID',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)



INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (3,Null,'Department Name','vcDepartment','vcDepartment',Null,'AdditionalContactsInformation','N','R','SelectBox',Null,'LI',19,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Organization Fields')

select * from DycFieldMaster
--10542
--update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (3,30566,Null,70,1,0,'Department Name','SelectBox','vcDepartment',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)



INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (3,Null,'Organization Rating','numCompanyRating','numCompanyRating',Null,'CompanyInfo','N','R','SelectBox',Null,'LI',2,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Organization Fields')

select * from DycFieldMaster
--10542
--update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (3,30568,Null,70,1,0,'Organization Rating','SelectBox','numCompanyRating',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)


INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (3,Null,'Lead Source','vcHow','vcHow',Null,'CompanyInfo','N','R','SelectBox',Null,'LI',18,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Organization Fields')

select * from DycFieldMaster
--10542
--update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (3,30569,Null,70,1,0,'Lead Source','SelectBox','vcHow',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)






INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (3,Null,'Net Days','numBillingDays','numBillingDays',Null,'DivisionMaster','N','R','SelectBox',Null,'BD',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org. | Accounting Fields')

select * from DycFieldMaster
--10542
--update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (3,30570,Null,70,1,0,'Net Days','SelectBox','numBillingDays',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)





INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (3,Null,'Assigned To','numAssignedTo','numAssignedTo',Null,'OpportunityMaster','N','R','SelectBox',Null,'U',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Order Fields')

INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (3,30571,Null,49,1,0,'Assigned To','SelectBox','numAssignedTo',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,0,Null,Null,Null,0)


INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (3,Null,'Assigned By','numAssignedBy','numAssignedBy',Null,'OpportunityMaster','N','R','SelectBox',Null,'U',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Order Fields')

INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (3,30572,Null,49,1,0,'Assigned By','SelectBox','numAssignedBy',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,0,Null,Null,Null,0)



INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (3,Null,'Conclusion Reason','lngPConclAnalysis','lngPConclAnalysis',Null,'OpportunityMaster','N','R','SelectBox',Null,'LI',12,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Order Fields')

INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (3,30573,Null,49,1,0,'Conclusion Reason','SelectBox','lngPConclAnalysis',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,0,Null,Null,Null,0)




INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (3,Null,'Opp Type','tintOppType','tintOppType',Null,'OpportunityMaster','N','R','SelectBox',Null,'',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Order Fields')

INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (3,30574,Null,49,1,0,'Opp Type','SelectBox','tintOppType',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,0,Null,Null,Null,0)



INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (3,Null,'Order Sub-total','monDealAmount','monDealAmount',Null,'OpportunityMaster','V','R','TextBox',Null,'',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Order Fields')

INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (3,30576,Null,49,1,0,'Order Sub-total','TextBox','monDealAmount',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,0,Null,Null,Null,0)



INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (3,Null,'Comments','txtComments','txtComments',Null,'OpportunityMaster','V','R','TextArea',Null,'',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Order Fields')

INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (3,30577,Null,49,1,0,'Comments','TextArea','txtComments',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,0,Null,Null,Null,0)




INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (3,Null,'Net Days','intBillingDays','intBillingDays',Null,'OpportunityMaster','V','R','SelectBox',Null,'BD',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Order Fields')

INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (3,30578,Null,49,1,0,'Net Days','SelectBox','intBillingDays',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,0,Null,Null,Null,0)


INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (3,Null,'Estimated close date','intpEstimatedCloseDate','intpEstimatedCloseDate',Null,'OpportunityMaster','V','R','DateField',Null,'',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Order Fields')

INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (3,30579,Null,49,1,0,'Estimated close date','DateField','intpEstimatedCloseDate',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,0,Null,Null,Null,0)

INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (3,Null,'Closing Date','bintClosedDate','bintClosedDate',Null,'OpportunityMaster','V','R','DateField',Null,'',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Order Fields')

INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (3,30581,Null,49,1,0,'Closing Date','DateField','bintClosedDate',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,0,Null,Null,Null,0)







--Organization Details for Projects

INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (5,Null,'Annual Revenue','numAnnualRevID','numAnnualRevID',Null,'CompanyInfo','N','R','SelectBox',Null,'LI',6,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')

select * from DycFieldMaster order by numFieldId desc

--10542
--update DycFieldMaster set vcGroup='Org.Details Fields' where numFieldId in (40549)

INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (5,40549,Null,73,1,0,'Annual Revenue','SelectBox','numAnnualRevID',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)





INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (5,Null,'Assigned By','numAssignedBy','numAssignedBy',Null,'DivisionMaster','N','R','SelectBox',Null,'U',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')


--10542
--update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (5,40550,Null,73,1,0,'Assigned By','SelectBox','numAssignedBy',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)


--select * from DycFieldMaster order by numFieldId desc


INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (5,Null,'Assigned To','numAssignedTo','numAssignedTo',Null,'DivisionMaster','N','R','SelectBox',Null,'U',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')


--10542
--update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (5,40551,Null,73,1,0,'Assigned To','SelectBox','numAssignedTo',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)



--exec USP_GetWorkFlowFormFieldMaster 1,68


INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (5,Null,'Campaign','numCampaignID','numCampaignID',Null,'DivisionMaster','N','R','SelectBox',Null,'C',24,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')

select * from DycFieldMaster
--10542
--update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (5,40552,Null,73,1,0,'Campaign','SelectBox','numCampaignID',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)


INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (5,Null,'Company Differentiation','numCompanyDiff','numCompanyDiff',Null,'DivisionMaster','N','R','SelectBox',Null,'LI',438,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')

select * from DycFieldMaster
--10542
--update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (5,40553,Null,70,1,0,'Company Differentiation','SelectBox','numCompanyDiff',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)


--Group
INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (5,Null,'Group','numGrpID','numGrpID',Null,'DivisionMaster','N','R','SelectBox',Null,'AG',38,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')

select * from DycFieldMaster
--10542
--update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (5,40554,Null,73,1,0,'Group','SelectBox','numGrpID',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)



INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (5,Null,'Follow-up Status','numFollowUpStatus','numFollowUpStatus',Null,'DivisionMaster','N','R','SelectBox',Null,'LI',30,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')

select * from DycFieldMaster order by numFieldId
--10542
--update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (5,40555,Null,73,1,0,'Follow-up Status','SelectBox','numFollowUpStatus',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)



INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (5,Null,'Default Trading Currency','numCurrencyID','numCurrencyID',Null,'DivisionMaster','N','R','SelectBox',Null,'U',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')

select * from DycFieldMaster
--10542
--update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (5,40556,Null,73,1,0,'Default Trading Currency','SelectBox','numCurrencyID',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)



INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (5,Null,'Territory','numTerID','numTerID',Null,'DivisionMaster','N','R','SelectBox',Null,'T',78,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')

select * from DycFieldMaster
--10542
--update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

--select * from DycFormField_Mapping order by numFieldID desc
--update DycFormField_Mapping set numModuleID=5 where numFieldID=40558

INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (5,40557,Null,73,1,0,'Territory','SelectBox','numTerID',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)




INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (5,Null,'Organization Phone','vcComPhone','vcComPhone',Null,'DivisionMaster','V','R','TextBox',Null,'',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')

select * from DycFieldMaster order by numFieldId desc
--10542
--update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (5,40558,Null,73,1,0,'Organization Phone','TextBox','vcComPhone',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)




INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (5,Null,'Organization Fax','vcComFax','vcComFax',Null,'DivisionMaster','V','R','TextBox',Null,'',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')

select * from DycFieldMaster
--10542
--update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (5,40559,Null,73,1,0,'Organization Fax','TextBox','vcComFax',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)




INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (5,Null,'Relationship Type','tintCRMType','tintCRMType',Null,'DivisionMaster','N','R','SelectBox',Null,'SYS',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')

select * from DycFieldMaster
--10542
--update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (5,40560,Null,73,1,0,'Relationship Type','SelectBox','tintCRMType',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)



INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (5,Null,'Organization Status','numStatusID','numStatusID',Null,'DivisionMaster','N','R','SelectBox',Null,'LI',1,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')




INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (5,40561,Null,73,1,0,'Organization Status','SelectBox','numStatusID',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)







INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (5,Null,'Department Name','vcDepartment','vcDepartment',Null,'AdditionalContactsInformation','N','R','SelectBox',Null,'LI',19,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')

select * from DycFieldMaster
--10542
--update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)
update DycFormField_Mapping set numFormID=73 where numFieldID=40562
INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (3,40562,Null,73,1,0,'Department Name','SelectBox','vcDepartment',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)



INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (5,Null,'Organization Rating','numCompanyRating','numCompanyRating',Null,'CompanyInfo','N','R','SelectBox',Null,'LI',2,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')

select * from DycFieldMaster
--10542
--update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (5,40563,Null,73,1,0,'Organization Rating','SelectBox','numCompanyRating',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)


INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (5,Null,'Lead Source','vcHow','vcHow',Null,'CompanyInfo','N','R','SelectBox',Null,'LI',18,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')

select * from DycFieldMaster
--10542
--update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (5,40564,Null,73,1,0,'Lead Source','SelectBox','vcHow',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)






INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (5,Null,'Net Days','numBillingDays','numBillingDays',Null,'DivisionMaster','N','R','SelectBox',Null,'BD',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org. | Accounting Fields')

select * from DycFieldMaster
--10542
--update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (5,40565,Null,73,1,0,'Net Days','SelectBox','numBillingDays',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)




--Organization Details for Cases

INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (7,Null,'Annual Revenue','numAnnualRevID','numAnnualRevID',Null,'CompanyInfo','N','R','SelectBox',Null,'LI',6,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')

select * from DycFieldMaster order by numFieldId desc

--10542
--update DycFieldMaster set vcGroup='Org.Details Fields' where numFieldId in (40549)

INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (7,40566,Null,72,1,0,'Annual Revenue','SelectBox','numAnnualRevID',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)





INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (7,Null,'Assigned By','numAssignedBy','numAssignedBy',Null,'DivisionMaster','N','R','SelectBox',Null,'U',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')


--10542
--update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (7,40567,Null,72,1,0,'Assigned By','SelectBox','numAssignedBy',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)


--select * from DycFieldMaster order by numFieldId desc


INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (7,Null,'Assigned To','numAssignedTo','numAssignedTo',Null,'DivisionMaster','N','R','SelectBox',Null,'U',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')


--10542
--update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (7,40568,Null,72,1,0,'Assigned To','SelectBox','numAssignedTo',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)



--exec USP_GetWorkFlowFormFieldMaster 1,68


INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (7,Null,'Campaign','numCampaignID','numCampaignID',Null,'DivisionMaster','N','R','SelectBox',Null,'C',24,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')

select * from DycFieldMaster
--10542
--update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (7,40569,Null,72,1,0,'Campaign','SelectBox','numCampaignID',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)


INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (7,Null,'Company Differentiation','numCompanyDiff','numCompanyDiff',Null,'DivisionMaster','N','R','SelectBox',Null,'LI',438,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')

select * from DycFieldMaster
--10542
--update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (7,40570,Null,72,1,0,'Company Differentiation','SelectBox','numCompanyDiff',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)


--Group
INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (7,Null,'Group','numGrpID','numGrpID',Null,'DivisionMaster','N','R','SelectBox',Null,'AG',38,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')

select * from DycFieldMaster
--10542
--update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (7,40571,Null,72,1,0,'Group','SelectBox','numGrpID',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)



INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (7,Null,'Follow-up Status','numFollowUpStatus','numFollowUpStatus',Null,'DivisionMaster','N','R','SelectBox',Null,'LI',30,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')

select * from DycFieldMaster order by numFieldId
--10542
--update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (7,40572,Null,72,1,0,'Follow-up Status','SelectBox','numFollowUpStatus',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)



INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (7,Null,'Default Trading Currency','numCurrencyID','numCurrencyID',Null,'DivisionMaster','N','R','SelectBox',Null,'U',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')

select * from DycFieldMaster
--10542
--update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (7,40573,Null,72,1,0,'Default Trading Currency','SelectBox','numCurrencyID',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)



INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (7,Null,'Territory','numTerID','numTerID',Null,'DivisionMaster','N','R','SelectBox',Null,'T',78,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')

select * from DycFieldMaster
--10542
--update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

--select * from DycFieldMaster order by numFieldID desc
--update DycFormField_Mapping set numModuleID=5 where numFieldID=40558

INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (7,40574,Null,72,1,0,'Territory','SelectBox','numTerID',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)




INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (7,Null,'Organization Phone','vcComPhone','vcComPhone',Null,'DivisionMaster','V','R','TextBox',Null,'',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')

select * from DycFieldMaster order by numFieldId desc
--10542
--update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (7,40575,Null,72,1,0,'Organization Phone','TextBox','vcComPhone',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)




INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (7,Null,'Organization Fax','vcComFax','vcComFax',Null,'DivisionMaster','V','R','TextBox',Null,'',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')

select * from DycFieldMaster
--10542
--update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (7,40576,Null,72,1,0,'Organization Fax','TextBox','vcComFax',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)




INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (7,Null,'Relationship Type','tintCRMType','tintCRMType',Null,'DivisionMaster','N','R','SelectBox',Null,'SYS',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')

select * from DycFieldMaster
--10542
--update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (7,40577,Null,72,1,0,'Relationship Type','SelectBox','tintCRMType',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)



INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (7,Null,'Organization Status','numStatusID','numStatusID',Null,'DivisionMaster','N','R','SelectBox',Null,'LI',1,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')




INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (7,40578,Null,72,1,0,'Organization Status','SelectBox','numStatusID',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)







INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (7,Null,'Department Name','vcDepartment','vcDepartment',Null,'AdditionalContactsInformation','N','R','SelectBox',Null,'LI',19,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')

select * from DycFieldMaster
--10542
--update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)
update DycFormField_Mapping set numFormID=73 where numFieldID=40562
INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (7,40579,Null,72,1,0,'Department Name','SelectBox','vcDepartment',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)



INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (7,Null,'Organization Rating','numCompanyRating','numCompanyRating',Null,'CompanyInfo','N','R','SelectBox',Null,'LI',2,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')

select * from DycFieldMaster
--10542
--update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (7,40580,Null,72,1,0,'Organization Rating','SelectBox','numCompanyRating',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)


INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (7,Null,'Lead Source','vcHow','vcHow',Null,'CompanyInfo','N','R','SelectBox',Null,'LI',18,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')

select * from DycFieldMaster
--10542
--update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (7,40581,Null,72,1,0,'Lead Source','SelectBox','vcHow',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)






INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (7,Null,'Net Days','numBillingDays','numBillingDays',Null,'DivisionMaster','N','R','SelectBox',Null,'BD',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org. | Accounting Fields')

select * from DycFieldMaster
--10542
--update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (7,40582,Null,72,1,0,'Net Days','SelectBox','numBillingDays',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)


--Organization Details for Tickker

INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (1,Null,'Annual Revenue','numAnnualRevID','numAnnualRevID',Null,'CompanyInfo','N','R','SelectBox',Null,'LI',6,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')




--10542
--update DycFieldMaster set vcGroup='Org.Details Fields' where numFieldId in (40549)
select * from DycFieldMaster  order by numFieldId desc
select * from DycFormField_Mapping  order by numFieldId desc

INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (1,(select MAX(numFieldID) from DycFieldMaster),Null,124,1,0,'Annual Revenue','SelectBox','numAnnualRevID',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)






INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (1,Null,'Assigned By','numAssignedBy','numAssignedBy',Null,'DivisionMaster','N','R','SelectBox',Null,'U',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')


--10542
--update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (1,(select MAX(numFieldID) from DycFieldMaster),Null,124,1,0,'Assigned By','SelectBox','numAssignedBy',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)


--select * from DycFieldMaster order by numFieldId desc


INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (1,Null,'Assigned To','numAssignedTo','numAssignedTo',Null,'DivisionMaster','N','R','SelectBox',Null,'U',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')


--10542
--update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (1,(select MAX(numFieldID) from DycFieldMaster),Null,124,1,0,'Assigned To','SelectBox','numAssignedTo',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)



--exec USP_GetWorkFlowFormFieldMaster 1,68


INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (1,Null,'Campaign','numCampaignID','numCampaignID',Null,'DivisionMaster','N','R','SelectBox',Null,'C',24,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')

select * from DycFieldMaster
--10542
--update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (1,(select MAX(numFieldID) from DycFieldMaster),Null,124,1,0,'Campaign','SelectBox','numCampaignID',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)


INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (1,Null,'Company Differentiation','numCompanyDiff','numCompanyDiff',Null,'DivisionMaster','N','R','SelectBox',Null,'LI',438,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')

select * from DycFieldMaster
--10542
--update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (1,(select MAX(numFieldID) from DycFieldMaster),Null,124,1,0,'Company Differentiation','SelectBox','numCompanyDiff',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)


--Group
INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (1,Null,'Group','numGrpID','numGrpID',Null,'DivisionMaster','N','R','SelectBox',Null,'AG',38,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')

select * from DycFieldMaster
--10542
--update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (1,(select MAX(numFieldID) from DycFieldMaster),Null,124,1,0,'Group','SelectBox','numGrpID',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)



INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (1,Null,'Follow-up Status','numFollowUpStatus','numFollowUpStatus',Null,'DivisionMaster','N','R','SelectBox',Null,'LI',30,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')

select * from DycFieldMaster order by numFieldId
--10542
--update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (1,(select MAX(numFieldID) from DycFieldMaster),Null,124,1,0,'Follow-up Status','SelectBox','numFollowUpStatus',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)



INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (1,Null,'Default Trading Currency','numCurrencyID','numCurrencyID',Null,'DivisionMaster','N','R','SelectBox',Null,'U',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')

select * from DycFieldMaster
--10542
--update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (1,(select MAX(numFieldID) from DycFieldMaster),Null,124,1,0,'Default Trading Currency','SelectBox','numCurrencyID',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)



INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (1,Null,'Territory','numTerID','numTerID',Null,'DivisionMaster','N','R','SelectBox',Null,'T',78,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')

select * from DycFieldMaster
--10542
--update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

--select * from DycFieldMaster order by numFieldID desc
--update DycFormField_Mapping set numModuleID=5 where numFieldID=40558

INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (1,(select MAX(numFieldID) from DycFieldMaster),Null,124,1,0,'Territory','SelectBox','numTerID',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)




INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (1,Null,'Organization Phone','vcComPhone','vcComPhone',Null,'DivisionMaster','V','R','TextBox',Null,'',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')

select * from DycFieldMaster order by numFieldId desc
--10542
--update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (1,(select MAX(numFieldID) from DycFieldMaster),Null,124,1,0,'Organization Phone','TextBox','vcComPhone',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)




INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (1,Null,'Organization Fax','vcComFax','vcComFax',Null,'DivisionMaster','V','R','TextBox',Null,'',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')

select * from DycFieldMaster
--10542
--update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (1,(select MAX(numFieldID) from DycFieldMaster),Null,124,1,0,'Organization Fax','TextBox','vcComFax',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)




INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (1,Null,'Relationship Type','tintCRMType','tintCRMType',Null,'DivisionMaster','N','R','SelectBox',Null,'SYS',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')

select * from DycFieldMaster
--10542
--update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (1,(select MAX(numFieldID) from DycFieldMaster),Null,124,1,0,'Relationship Type','SelectBox','tintCRMType',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)



INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (1,Null,'Organization Status','numStatusID','numStatusID',Null,'DivisionMaster','N','R','SelectBox',Null,'LI',1,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')




INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (1,(select MAX(numFieldID) from DycFieldMaster),Null,124,1,0,'Organization Status','SelectBox','numStatusID',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)







INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (1,Null,'Department Name','vcDepartment','vcDepartment',Null,'AdditionalContactsInformation','N','R','SelectBox',Null,'LI',19,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')

select * from DycFieldMaster
--10542
--update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)
update DycFormField_Mapping set numFormID=73 where numFieldID=40562
INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (1,(select MAX(numFieldID) from DycFieldMaster),Null,124,1,0,'Department Name','SelectBox','vcDepartment',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)



INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (1,Null,'Organization Rating','numCompanyRating','numCompanyRating',Null,'CompanyInfo','N','R','SelectBox',Null,'LI',2,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')

select * from DycFieldMaster
--10542
--update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (1,(select MAX(numFieldID) from DycFieldMaster),Null,124,1,0,'Organization Rating','SelectBox','numCompanyRating',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)


INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (1,Null,'Lead Source','vcHow','vcHow',Null,'CompanyInfo','N','R','SelectBox',Null,'LI',18,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')

select * from DycFieldMaster
--10542
--update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (1,(select MAX(numFieldID) from DycFieldMaster),Null,124,1,0,'Lead Source','SelectBox','vcHow',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)






INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (1,Null,'Net Days','numBillingDays','numBillingDays',Null,'DivisionMaster','N','R','SelectBox',Null,'BD',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org. | Accounting Fields')

select * from DycFieldMaster
--10542
--update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (1,(select MAX(numFieldID) from DycFieldMaster),Null,124,1,0,'Net Days','SelectBox','numBillingDays',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)




--stage Details for Projects

INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (5,Null,'Stage','vcStageName','vcStageName',Null,'StagePercentageDetails','V','R','SelectBox',Null,'SG',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Stage Details fields')



INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (5,(select MAX(numFieldID) from DycFieldMaster),Null,73,1,0,'Stage','SelectBox','vcStageName',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)



INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength)
VALUES        (5,Null,'Stage Progress','tinProgressPercentage','tinProgressPercentage',Null,'StagePercentageDetails','N','R','SelectBox',Null,'SP',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null)


--update DycFieldMaster set  vcGroup='Stage Details fields'  where numFieldId =40601

INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (5,(select MAX(numFieldID) from DycFieldMaster),Null,73,1,0,'Stage Progress','SelectBox','tinProgressPercentage',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)






INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (5,Null,'Assigned To','numAssignTo','numAssignTo',Null,'StagePercentageDetails','N','R','SelectBox',Null,'U',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Stage Details fields')

INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (5,(select MAX(numFieldID) from DycFieldMaster),Null,73,1,0,'Assigned To','SelectBox','numAssignTo',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)




--stage Details for Orders

INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (3,Null,'Stage','vcStageName','vcStageName',Null,'StagePercentageDetails','V','R','SelectBox',Null,'SG',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Stage Details fields')



INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (3,(select MAX(numFieldID) from DycFieldMaster),Null,70,1,0,'Stage','SelectBox','vcStageName',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)



INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (3,Null,'Stage Progress','tinProgressPercentage','tinProgressPercentage',Null,'StagePercentageDetails','N','R','SelectBox',Null,'SP',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Stage Details fields')


--update DycFieldMaster set  vcGroup='Stage Details fields'  where numFieldId =40601

INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (3,(select MAX(numFieldID) from DycFieldMaster),Null,70,1,0,'Stage Progress','SelectBox','tinProgressPercentage',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)






INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (3,Null,'Assigned To','numAssignTo','numAssignTo',Null,'StagePercentageDetails','N','R','SelectBox',Null,'U',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Stage Details fields')

INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (3,(select MAX(numFieldID) from DycFieldMaster),Null,70,1,0,'Assigned To','SelectBox','numAssignTo',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)





--cases for ORders
INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (7,Null,'Order Status','numStatus','numStatus',Null,'OpportunityMaster','N','R','SelectBox',Null,'LI',176,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Order Fields')

INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (7,(select MAX(numFieldID) from DycFieldMaster),Null,72,1,0,'Order Status','SelectBox','numStatus',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,0,Null,Null,Null,0)


INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (7,Null,'Deal Status','tintOppStatus','tintOppStatus',Null,'OpportunityMaster','N','R','SelectBox',Null,NULL,0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Order Fields')

INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (7,(select MAX(numFieldID) from DycFieldMaster),Null,72,1,0,'Deal Status','SelectBox','tintOppStatus',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,0,Null,Null,Null,0)




INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (7,Null,'Opp Type','tintOppType','tintOppType',Null,'OpportunityMaster','N','R','SelectBox',Null,'',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Order Fields')

INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (7,(select MAX(numFieldID) from DycFieldMaster),Null,72,1,0,'Opp Type','SelectBox','tintOppType',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,0,Null,Null,Null,0)



INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (7,Null,'Order Sub-total','monDealAmount','monDealAmount',Null,'OpportunityMaster','V','R','TextBox',Null,'',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Order Fields')

INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (7,(select MAX(numFieldID) from DycFieldMaster),Null,72,1,0,'Order Sub-total','TextBox','monDealAmount',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,0,Null,Null,Null,0)



INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (7,Null,'Assigned To','numAssignedTo','numAssignedTo',Null,'OpportunityMaster','N','R','SelectBox',Null,'U',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Order Fields')

INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (7,(select MAX(numFieldID) from DycFieldMaster),Null,72,1,0,'Assigned To','SelectBox','numAssignedTo',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,0,Null,Null,Null,0)


INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (7,Null,'Assigned By','numAssignedBy','numAssignedBy',Null,'OpportunityMaster','N','R','SelectBox',Null,'U',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Order Fields')

INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (7,(select MAX(numFieldID) from DycFieldMaster),Null,72,1,0,'Assigned By','SelectBox','numAssignedBy',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,0,Null,Null,Null,0)



INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (7,Null,'Comments','txtComments','txtComments',Null,'OpportunityMaster','V','R','TextArea',Null,'',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Order Fields')

INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (7,(select MAX(numFieldID) from DycFieldMaster),Null,72,1,0,'Comments','TextArea','txtComments',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,0,Null,Null,Null,0)




INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (7,Null,'Net Days','intBillingDays','intBillingDays',Null,'OpportunityMaster','V','R','SelectBox',Null,'BD',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Order Fields')

INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (7,(select MAX(numFieldID) from DycFieldMaster),Null,72,1,0,'Net Days','SelectBox','intBillingDays',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,0,Null,Null,Null,0)


INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (7,Null,'Estimated close date','intpEstimatedCloseDate','intpEstimatedCloseDate',Null,'OpportunityMaster','V','R','DateField',Null,'',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Order Fields')

INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (7,(select MAX(numFieldID) from DycFieldMaster),Null,72,1,0,'Estimated close date','DateField','intpEstimatedCloseDate',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,0,Null,Null,Null,0)

INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (7,Null,'Closing Date','bintClosedDate','bintClosedDate',Null,'OpportunityMaster','V','R','DateField',Null,'',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Order Fields')

INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (7,(select MAX(numFieldID) from DycFieldMaster),Null,72,1,0,'Closing Date','DateField','bintClosedDate',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,0,Null,Null,Null,0)






--Organization Details for BiZdocs

INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (3,Null,'Annual Revenue','numAnnualRevID','numAnnualRevID',Null,'CompanyInfo','N','R','SelectBox',Null,'LI',6,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')




--10542
--update DycFieldMaster set vcGroup='Org.Details Fields' where numFieldId in (40549)
select * from DycFieldMaster  order by numFieldId desc
select * from DycFormField_Mapping  order by numFieldId desc

INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (3,(select MAX(numFieldID) from DycFieldMaster),Null,49,1,0,'Annual Revenue','SelectBox','numAnnualRevID',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)






INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (3,Null,'Assigned By','numAssignedBy','numAssignedBy',Null,'DivisionMaster','N','R','SelectBox',Null,'U',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')


--10542
--update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (3,(select MAX(numFieldID) from DycFieldMaster),Null,49,1,0,'Assigned By','SelectBox','numAssignedBy',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)


--select * from DycFieldMaster order by numFieldId desc


INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (3,Null,'Assigned To','numAssignedTo','numAssignedTo',Null,'DivisionMaster','N','R','SelectBox',Null,'U',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')


--10542
--update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (3,(select MAX(numFieldID) from DycFieldMaster),Null,49,1,0,'Assigned To','SelectBox','numAssignedTo',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)



--exec USP_GetWorkFlowFormFieldMaster 1,68


INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (3,Null,'Campaign','numCampaignID','numCampaignID',Null,'DivisionMaster','N','R','SelectBox',Null,'C',24,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')

select * from DycFieldMaster
--10542
--update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (3,(select MAX(numFieldID) from DycFieldMaster),Null,49,1,0,'Campaign','SelectBox','numCampaignID',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)


INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (3,Null,'Company Differentiation','numCompanyDiff','numCompanyDiff',Null,'DivisionMaster','N','R','SelectBox',Null,'LI',438,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')

select * from DycFieldMaster
--10542
--update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (3,(select MAX(numFieldID) from DycFieldMaster),Null,49,1,0,'Company Differentiation','SelectBox','numCompanyDiff',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)


--Group
INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (3,Null,'Group','numGrpID','numGrpID',Null,'DivisionMaster','N','R','SelectBox',Null,'AG',38,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')

select * from DycFieldMaster
--10542
--update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (3,(select MAX(numFieldID) from DycFieldMaster),Null,49,1,0,'Group','SelectBox','numGrpID',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)



INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (3,Null,'Follow-up Status','numFollowUpStatus','numFollowUpStatus',Null,'DivisionMaster','N','R','SelectBox',Null,'LI',30,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')

select * from DycFieldMaster order by numFieldId
--10542
--update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (3,(select MAX(numFieldID) from DycFieldMaster),Null,49,1,0,'Follow-up Status','SelectBox','numFollowUpStatus',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)



INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (3,Null,'Default Trading Currency','numCurrencyID','numCurrencyID',Null,'DivisionMaster','N','R','SelectBox',Null,'U',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')

select * from DycFieldMaster
--10542
--update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (3,(select MAX(numFieldID) from DycFieldMaster),Null,49,1,0,'Default Trading Currency','SelectBox','numCurrencyID',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)



INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (3,Null,'Territory','numTerID','numTerID',Null,'DivisionMaster','N','R','SelectBox',Null,'T',78,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')

select * from DycFieldMaster
--10542
--update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

--select * from DycFieldMaster order by numFieldID desc
--update DycFormField_Mapping set numModuleID=5 where numFieldID=40558

INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (3,(select MAX(numFieldID) from DycFieldMaster),Null,49,1,0,'Territory','SelectBox','numTerID',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)




INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (3,Null,'Organization Phone','vcComPhone','vcComPhone',Null,'DivisionMaster','V','R','TextBox',Null,'',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')

select * from DycFieldMaster order by numFieldId desc
--10542
--update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (3,(select MAX(numFieldID) from DycFieldMaster),Null,49,1,0,'Organization Phone','TextBox','vcComPhone',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)




INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (3,Null,'Organization Fax','vcComFax','vcComFax',Null,'DivisionMaster','V','R','TextBox',Null,'',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')

select * from DycFieldMaster
--10542
--update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (3,(select MAX(numFieldID) from DycFieldMaster),Null,49,1,0,'Organization Fax','TextBox','vcComFax',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)




INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (3,Null,'Relationship Type','tintCRMType','tintCRMType',Null,'DivisionMaster','N','R','SelectBox',Null,'SYS',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')

select * from DycFieldMaster
--10542
select * from DycFormField_Mapping order by numFieldID desc
--update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (3,(select MAX(numFieldID) from DycFieldMaster),Null,49,1,0,'Relationship Type','SelectBox','tintCRMType',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)



INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (3,Null,'Organization Status','numStatusID','numStatusID',Null,'DivisionMaster','N','R','SelectBox',Null,'LI',1,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')




INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (3,(select MAX(numFieldID) from DycFieldMaster),Null,49,1,0,'Organization Status','SelectBox','numStatusID',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)







INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (3,Null,'Department Name','vcDepartment','vcDepartment',Null,'AdditionalContactsInformation','N','R','SelectBox',Null,'LI',19,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')

select * from DycFieldMaster
--10542
--update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)
--update DycFormField_Mapping set numFormID=73 where numFieldID=40562
INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (3,(select MAX(numFieldID) from DycFieldMaster),Null,49,1,0,'Department Name','SelectBox','vcDepartment',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)



INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (3,Null,'Organization Rating','numCompanyRating','numCompanyRating',Null,'CompanyInfo','N','R','SelectBox',Null,'LI',2,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')

select * from DycFormField_Mapping order by numFieldID desc
--10542
--update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (3,(select MAX(numFieldID) from DycFieldMaster),Null,49,1,0,'Organization Rating','SelectBox','numCompanyRating',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)


INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (3,Null,'Lead Source','vcHow','vcHow',Null,'CompanyInfo','N','R','SelectBox',Null,'LI',18,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')

select * from DycFieldMaster
--10542
--update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (3,(select MAX(numFieldID) from DycFieldMaster),Null,49,1,0,'Lead Source','SelectBox','vcHow',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)






INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (3,Null,'Net Days','numBillingDays','numBillingDays',Null,'DivisionMaster','N','R','SelectBox',Null,'BD',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org. | Accounting Fields')

select * from DycFieldMaster
--10542
--update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (3,(select MAX(numFieldID) from DycFieldMaster),Null,49,1,0,'Net Days','SelectBox','numBillingDays',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)


update DycFormField_Mapping set bitAllowEdit=1 where numFormID=68






INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (3,Null,'BizDoc Template','numBizDocTempID','numBizDocTempID',Null,'OpportunityBizDocs','N','R','SelectBox',Null,'BT',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'BizDoc Fields')

--10542
--update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (3,(select MAX(numFieldID) from DycFieldMaster),Null,49,1,0,'BizDoc Template','SelectBox','numBizDocTempID',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)





INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (3,Null,'BizDoc Template','numBizDocTempID','numBizDocTempID',Null,'OpportunityBizDocs','N','R','SelectBox',Null,'BT',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'BizDoc Fields')

--10542
--update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (3,(select MAX(numFieldID) from DycFieldMaster),Null,70,1,0,'BizDoc Template','SelectBox','numBizDocTempID',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)





INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (3,Null,'Credit Limit','numCompanyCredit','numCompanyCredit',Null,'CompanyInfo','N','R','SelectBox',Null,'LI',3,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org. | Accounting Fields')

--10542




INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (3,(select MAX(numFieldID) from DycFieldMaster),Null,49,1,0,'Credit Limit','SelectBox','numCompanyCredit',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)


--orders

--bitOnCreditHold


INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (3,Null,'On Credit Hold','bitOnCreditHold','bitOnCreditHold',Null,'DivisionMaster','Y','R','CheckBox',Null,'',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org. | Accounting Fields')

--10542

--update DycFieldMaster set vcFieldDataType='Y' where numFieldID=20548

--select * from DycFormField_Mapping where  numFieldID=20548
--update DycFormField_Mapping set vcAssociatedControlType='CheckBox' where  numFieldID=20548

INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (3,(select MAX(numFieldID) from DycFieldMaster),Null,49,1,0,'On Credit Hold','CheckBox','bitOnCreditHold',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)





INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (7,Null,'Credit Limit','numCompanyCredit','numCompanyCredit',Null,'CompanyInfo','N','R','SelectBox',Null,'LI',3,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org. | Accounting Fields')

--10542




INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (7,(select MAX(numFieldID) from DycFieldMaster),Null,72,1,0,'Credit Limit','SelectBox','numCompanyCredit',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)


--orders

--bitOnCreditHold


INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (7,Null,'On Credit Hold','bitOnCreditHold','bitOnCreditHold',Null,'DivisionMaster','Y','R','CheckBox',Null,'',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org. | Accounting Fields')

--10542

--update DycFieldMaster set vcFieldDataType='Y' where numFieldID=20548

--select * from DycFormField_Mapping where  numFieldID=20548
--update DycFormField_Mapping set vcAssociatedControlType='CheckBox' where  numFieldID=20548

INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (7,(select MAX(numFieldID) from DycFieldMaster),Null,72,1,0,'On Credit Hold','CheckBox','bitOnCreditHold',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)



INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (1,Null,'Credit Limit','numCompanyCredit','numCompanyCredit',Null,'CompanyInfo','N','R','SelectBox',Null,'LI',3,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org. | Accounting Fields')

--10542




INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (1,(select MAX(numFieldID) from DycFieldMaster),Null,124,1,0,'Credit Limit','SelectBox','numCompanyCredit',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)


--orders

--bitOnCreditHold


INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (1,Null,'On Credit Hold','bitOnCreditHold','bitOnCreditHold',Null,'DivisionMaster','Y','R','CheckBox',Null,'',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org. | Accounting Fields')

--10542

--update DycFieldMaster set vcFieldDataType='Y' where numFieldID=20548

--select * from DycFormField_Mapping where  numFieldID=20548
--update DycFormField_Mapping set vcAssociatedControlType='CheckBox' where  numFieldID=20548

INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (1,(select MAX(numFieldID) from DycFieldMaster),Null,124,1,0,'On Credit Hold','CheckBox','bitOnCreditHold',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)




INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (5,Null,'Credit Limit','numCompanyCredit','numCompanyCredit',Null,'CompanyInfo','N','R','SelectBox',Null,'LI',3,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org. | Accounting Fields')

--10542




INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (5,(select MAX(numFieldID) from DycFieldMaster),Null,73,1,0,'Credit Limit','SelectBox','numCompanyCredit',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)


--orders

--bitOnCreditHold


INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (5,Null,'On Credit Hold','bitOnCreditHold','bitOnCreditHold',Null,'DivisionMaster','Y','R','CheckBox',Null,'',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org. | Accounting Fields')

--10542

--update DycFieldMaster set vcFieldDataType='Y' where numFieldID=20548

--select * from DycFormField_Mapping where  numFieldID=20548
--update DycFormField_Mapping set vcAssociatedControlType='CheckBox' where  numFieldID=20548

INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (5,(select MAX(numFieldID) from DycFieldMaster),Null,73,1,0,'On Credit Hold','CheckBox','bitOnCreditHold',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)



INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (1,Null,'Credit Limit','numCompanyCredit','numCompanyCredit',Null,'CompanyInfo','N','R','SelectBox',Null,'LI',3,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org. | Accounting Fields')

--10542




INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (1,(select MAX(numFieldID) from DycFieldMaster),Null,68,1,0,'Credit Limit','SelectBox','numCompanyCredit',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)


--orders

--bitOnCreditHold


INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (1,Null,'On Credit Hold','bitOnCreditHold','bitOnCreditHold',Null,'DivisionMaster','Y','R','CheckBox',Null,'',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org. | Accounting Fields')

--10542

--update DycFieldMaster set vcFieldDataType='Y' where numFieldID=20548

--select * from DycFormField_Mapping where  numFieldID=20548
--update DycFormField_Mapping set vcAssociatedControlType='CheckBox' where  numFieldID=20548

INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (1,(select MAX(numFieldID) from DycFieldMaster),Null,68,1,0,'On Credit Hold','CheckBox','bitOnCreditHold',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)





--Adding Page to Admin Tree


--BEGIN TRANSACTION
	DECLARE @numPageNavID numeric 
	SELECT @numPageNavID = MAX(numPageNavID) + 1  FROM dbo.PageNavigationDTL 
	INSERT INTO dbo.PageNavigationDTL
			( numPageNavID ,
			  numModuleID ,
			  numParentID ,
			  vcPageNavName ,
			  vcNavURL ,
			  vcImageURL ,
			  bitVisible ,
			  numTabID
			)
	SELECT @numPageNavID ,13,66,'Sales Fulfillment Workflow','../opportunity/frmSalesFulfillmentWorkflowSettings.aspx',NULL,1,-1
	

	DECLARE CursorTreeNavigation CURSOR FOR
	SELECT numDomainId FROM Domain WHERE numDomainId <> -255
	
	OPEN CursorTreeNavigation;

	DECLARE @numDomainId NUMERIC(18,0);

	FETCH NEXT FROM CursorTreeNavigation INTO @numDomainId;
	WHILE (@@FETCH_STATUS = 0)
	BEGIN
	  
	   ---- Give permission for each domain to tree node
	   
   		DELETE FROM TreeNavigationAuthorization WHERE numPageNavID=@numPageNavID AND numDomainID = @numDomainId
	
		SELECT * INTO #tempData1 FROM
		(
		 SELECT    T.numTabID,
                        CASE WHEN bitFixed = 1
                             THEN CASE WHEN EXISTS ( SELECT numTabName
                                                     FROM   TabDefault
                                                     WHERE  numTabId = T.numTabId
                                                            AND tintTabType = T.tintTabType AND numDomainID =D.numDomainID)
                                       THEN ( SELECT TOP 1
                                                        numTabName
                                              FROM      TabDefault
                                              WHERE     numTabId = T.numTabId
                                                        AND tintTabType = T.tintTabType AND numDomainID = D.numDomainID
                                            )
                                       ELSE T.numTabName
                                  END
                             ELSE T.numTabName
                        END numTabname,
                        vcURL,
                        bitFixed,
                        1 AS tintType,
                        T.vcImage,
                        D.numDomainID,
                        A.numGroupID
              FROM      TabMaster T
                        CROSS JOIN Domain D --ON D.numDomainID = T.numDomainID
                        LEFT JOIN AuthenticationGroupMaster A ON A.numDomainID = D.numDomainId
              WHERE     bitFixed = 1
                        AND D.numDomainId <> -255
                        AND t.tintTabType = 1
              
              UNION ALL
              SELECT    -1 AS [numTabID],
                        'Administration' numTabname,
                        '' vcURL,
                        1 bitFixed,
                        1 AS tintType,
                        '' AS vcImage,
                        D.numDomainID,
                        A.numGroupID
              FROM      Domain D
                        JOIN AuthenticationGroupMaster A ON A.numDomainID = D.numDomainId
                        WHERE D.numDomainID = @numDomainID
			  
			  UNION ALL
			  SELECT    -3 AS [numTabID],
						'Advanced Search' numTabname,
						'' vcURL,
						1 bitFixed,
						1 AS tintType,
						'' AS vcImage,
						D.numDomainID,
						A.numGroupID
			  FROM      Domain D
						JOIN AuthenticationGroupMaster A ON A.numDomainID = D.numDomainId          
            ) TABLE2       		
                        
		INSERT  INTO dbo.TreeNavigationAuthorization
            (
              numGroupID,
              numTabID,
              numPageNavID,
              bitVisible,
              numDomainID,
              tintType
            )
            SELECT  numGroupID,
                    PND.numTabID,
                    numPageNavID,
                    bitVisible,
                    numDomainID,
                    tintType
            FROM    dbo.PageNavigationDTL PND
                    JOIN #tempData1 TD ON TD.numTabID = PND.numTabID 
                    WHERE PND.numPageNavID= @numPageNavID
		
          DROP TABLE #tempData1
	   
	   

		

		
	   FETCH NEXT FROM CursorTreeNavigation INTO @numDomainId;
	END;

	CLOSE CursorTreeNavigation;
	DEALLOCATE CursorTreeNavigation;


	--ROLLbaCk Transaction




	--PAy Bill


	
--BEGIN TRANSACTION
	DECLARE @numPageNavID numeric 
	SELECT @numPageNavID = MAX(numPageNavID) + 1  FROM dbo.PageNavigationDTL 
	INSERT INTO dbo.PageNavigationDTL
			( numPageNavID ,
			  numModuleID ,
			  numParentID ,
			  vcPageNavName ,
			  vcNavURL ,
			  vcImageURL ,
			  bitVisible ,
			  numTabID
			)
	SELECT @numPageNavID ,13,66,'Pay Bill Workflow','../opportunity/frmPurchaseWorkflowSettings.aspx',NULL,1,-1
	

	DECLARE CursorTreeNavigation CURSOR FOR
	SELECT numDomainId FROM Domain WHERE numDomainId <> -255
	
	OPEN CursorTreeNavigation;

	DECLARE @numDomainId NUMERIC(18,0);

	FETCH NEXT FROM CursorTreeNavigation INTO @numDomainId;
	WHILE (@@FETCH_STATUS = 0)
	BEGIN
	  
	   ---- Give permission for each domain to tree node
	   
   		DELETE FROM TreeNavigationAuthorization WHERE numPageNavID=@numPageNavID AND numDomainID = @numDomainId
	
		SELECT * INTO #tempData1 FROM
		(
		 SELECT    T.numTabID,
                        CASE WHEN bitFixed = 1
                             THEN CASE WHEN EXISTS ( SELECT numTabName
                                                     FROM   TabDefault
                                                     WHERE  numTabId = T.numTabId
                                                            AND tintTabType = T.tintTabType AND numDomainID =D.numDomainID)
                                       THEN ( SELECT TOP 1
                                                        numTabName
                                              FROM      TabDefault
                                              WHERE     numTabId = T.numTabId
                                                        AND tintTabType = T.tintTabType AND numDomainID = D.numDomainID
                                            )
                                       ELSE T.numTabName
                                  END
                             ELSE T.numTabName
                        END numTabname,
                        vcURL,
                        bitFixed,
                        1 AS tintType,
                        T.vcImage,
                        D.numDomainID,
                        A.numGroupID
              FROM      TabMaster T
                        CROSS JOIN Domain D --ON D.numDomainID = T.numDomainID
                        LEFT JOIN AuthenticationGroupMaster A ON A.numDomainID = D.numDomainId
              WHERE     bitFixed = 1
                        AND D.numDomainId <> -255
                        AND t.tintTabType = 1
              
              UNION ALL
              SELECT    -1 AS [numTabID],
                        'Administration' numTabname,
                        '' vcURL,
                        1 bitFixed,
                        1 AS tintType,
                        '' AS vcImage,
                        D.numDomainID,
                        A.numGroupID
              FROM      Domain D
                        JOIN AuthenticationGroupMaster A ON A.numDomainID = D.numDomainId
                        WHERE D.numDomainID = @numDomainID
			  
			  UNION ALL
			  SELECT    -3 AS [numTabID],
						'Advanced Search' numTabname,
						'' vcURL,
						1 bitFixed,
						1 AS tintType,
						'' AS vcImage,
						D.numDomainID,
						A.numGroupID
			  FROM      Domain D
						JOIN AuthenticationGroupMaster A ON A.numDomainID = D.numDomainId          
            ) TABLE2       		
                        
		INSERT  INTO dbo.TreeNavigationAuthorization
            (
              numGroupID,
              numTabID,
              numPageNavID,
              bitVisible,
              numDomainID,
              tintType
            )
            SELECT  numGroupID,
                    PND.numTabID,
                    numPageNavID,
                    bitVisible,
                    numDomainID,
                    tintType
            FROM    dbo.PageNavigationDTL PND
                    JOIN #tempData1 TD ON TD.numTabID = PND.numTabID 
                    WHERE PND.numPageNavID= @numPageNavID
		
          DROP TABLE #tempData1
	   
	   

		

		
	   FETCH NEXT FROM CursorTreeNavigation INTO @numDomainId;
	END;

	CLOSE CursorTreeNavigation;
	DEALLOCATE CursorTreeNavigation;


	--ROLLbaCk Transaction

	DECLARE @numPageNavID numeric 
	SELECT @numPageNavID = MAX(numPageNavID) + 1  FROM dbo.PageNavigationDTL 
	INSERT INTO dbo.PageNavigationDTL
			( numPageNavID ,
			  numModuleID ,
			  numParentID ,
			  vcPageNavName ,
			  vcNavURL ,
			  vcImageURL ,
			  bitVisible ,
			  numTabID,
			  intSortOrder
			)
	SELECT @numPageNavID ,13,73,'Shipping',NULL,NULL,1,-1,4
	

	DECLARE CursorTreeNavigation CURSOR FOR
	SELECT numDomainId FROM Domain WHERE numDomainId <> -255
	
	OPEN CursorTreeNavigation;

	DECLARE @numDomainId NUMERIC(18,0);

	FETCH NEXT FROM CursorTreeNavigation INTO @numDomainId;
	WHILE (@@FETCH_STATUS = 0)
	BEGIN
	  
	   ---- Give permission for each domain to tree node
	   
   		DELETE FROM TreeNavigationAuthorization WHERE numPageNavID=@numPageNavID AND numDomainID = @numDomainId
	
		SELECT * INTO #tempData1 FROM
		(
		 SELECT    T.numTabID,
                        CASE WHEN bitFixed = 1
                             THEN CASE WHEN EXISTS ( SELECT numTabName
                                                     FROM   TabDefault
                                                     WHERE  numTabId = T.numTabId
                                                            AND tintTabType = T.tintTabType AND numDomainID =D.numDomainID)
                                       THEN ( SELECT TOP 1
                                                        numTabName
                                              FROM      TabDefault
                                              WHERE     numTabId = T.numTabId
                                                        AND tintTabType = T.tintTabType AND numDomainID = D.numDomainID
                                            )
                                       ELSE T.numTabName
                                  END
                             ELSE T.numTabName
                        END numTabname,
                        vcURL,
                        bitFixed,
                        1 AS tintType,
                        T.vcImage,
                        D.numDomainID,
                        A.numGroupID
              FROM      TabMaster T
                        CROSS JOIN Domain D --ON D.numDomainID = T.numDomainID
                        LEFT JOIN AuthenticationGroupMaster A ON A.numDomainID = D.numDomainId
              WHERE     bitFixed = 1
                        AND D.numDomainId <> -255
                        AND t.tintTabType = 1
              
              UNION ALL
              SELECT    -1 AS [numTabID],
                        'Administration' numTabname,
                        '' vcURL,
                        1 bitFixed,
                        1 AS tintType,
                        '' AS vcImage,
                        D.numDomainID,
                        A.numGroupID
              FROM      Domain D
                        JOIN AuthenticationGroupMaster A ON A.numDomainID = D.numDomainId
                        WHERE D.numDomainID = @numDomainID
			  
			  UNION ALL
			  SELECT    -3 AS [numTabID],
						'Advanced Search' numTabname,
						'' vcURL,
						1 bitFixed,
						1 AS tintType,
						'' AS vcImage,
						D.numDomainID,
						A.numGroupID
			  FROM      Domain D
						JOIN AuthenticationGroupMaster A ON A.numDomainID = D.numDomainId          
            ) TABLE2       		
                        
		INSERT  INTO dbo.TreeNavigationAuthorization
            (
              numGroupID,
              numTabID,
              numPageNavID,
              bitVisible,
              numDomainID,
              tintType
            )
            SELECT  numGroupID,
                    PND.numTabID,
                    numPageNavID,
                    bitVisible,
                    numDomainID,
                    tintType
            FROM    dbo.PageNavigationDTL PND
                    JOIN #tempData1 TD ON TD.numTabID = PND.numTabID 
                    WHERE PND.numPageNavID= @numPageNavID
		
          DROP TABLE #tempData1
	   
	   

		

		
	   FETCH NEXT FROM CursorTreeNavigation INTO @numDomainId;
	END;

	CLOSE CursorTreeNavigation;
	DEALLOCATE CursorTreeNavigation;


	--ROLLbaCk Transaction

---------------------------------------------

------------------- SANDEEP ------------------
ALTER TABLE GroupAuthorization ADD
bitCustomRelationship bit NOT NULL DEFAULT(0)

-------------------------------------------------------------------------------------------------------------------

ALTER TABLE Domain ADD
numDefaultSalesPricing TINYINT 

-------------------------------------------------------------------------------------------------------------------

-- Remove Organization List Page
DELETE FROM PageMaster WHERE numModuleID = 32 AND numPageID = 1
DELETE FROM GroupAuthorization WHERE numModuleID = 32 AND numPageID = 1

-------------------------------------------------------------------------------------------------------------------

/****** Object:  Table [dbo].[TreeNodeOrder]    Script Date: 14-Aug-14 1:40:04 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[TreeNodeOrder](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[numTabID] [int] NULL,
	[numPageNavID] [int] NULL,
	[numDomainID] [int] NULL,
	[numGroupID] [int] NULL,
	[numListItemID] [int] NULL,
	[numParentID] [int] NULL,
	[numOrder] [int] NULL,
	[bitVisible] [int] NULL,
	[tintType] [int] NULL,
 CONSTRAINT [PK_TreeNodeOrder] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

--------------------------------------------------------------------------------------------------------------------

/****** Object:  Table [dbo].[TicklerListFilterConfiguration]    Script Date: 21-Aug-14 9:42:17 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[TicklerListFilterConfiguration](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[numDomainID] [numeric](18, 0) NOT NULL,
	[numUserCntID] [numeric](18, 0) NOT NULL,
	[vcSelectedEmployee] [varchar](500) NULL,
	[numCriteria] [int] NULL,
	[vcActionTypes] [varchar](500) NULL,
	[vcSelectedEmployeeOpp] [varchar](500) NULL,
	[vcSelectedEmployeeCases] [varchar](500) NULL,
	[vcAssignedStages] [varchar](500) NULL,
	[numCriteriaCases] [int] NULL,
 CONSTRAINT [PK_TicklerListFilterConfiguration] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

--------------------------------------------------------------------------------------------------------------------

ALTER TABLE dbo.SalesOrderConfiguration ADD
bitDisplayCouponDiscount BIT NOT NULL DEFAULT(1),
bitDisplayShippingCharges BIT NOT NULL DEFAULT(1),
bitDisplayDiscount BIT NOT NULL DEFAULT(1)

--------------------------------------------------------------------------------------------------------------------

BEGIN TRANSACTION

DECLARE @NewFieldID INTEGER

INSERT INTO dbo.DycFieldMaster 
(		
	numModuleID,
	numDomainID,
	vcFieldName,
	vcDbColumnName,
	vcOrigDbColumnName,
	vcPropertyName,
	vcLookBackTableName,
	vcFieldDataType,
	vcFieldType,
	vcAssociatedControlType ,
	bitInResults,
	bitDeleted,
	bitSettingField,
	numListID,
	bitAllowEdit,
	bitDefault,
	bitDetailField
)
VALUES  
( 
	3 , 
	NULL,
	N'Total Discount' ,
	N'numTotalDiscount' , 
	N'numTotalDiscount' , 
	'TotalDiscount' ,
	N'OpportunityMaster' ,
	'M' , 
	'R' , 
	N'Label' ,
	1,
	0,
	1,
	0,
	0,
	0,
	1
)

SELECT @NewFieldID = SCOPE_IDENTITY() 

IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 3 AND numFieldID = @NewFieldID AND numFormID = 39)
BEGIN
	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit],[bitDetailField]) VALUES (3,@NewFieldID,39,0,0,0,1)
END

ROLLBACK TRANSACTION

--------------------------------------------------------------------------------------------------------------------

---------------------------------------------

------------------- MANISH ------------------

------/******************************************************************
------Project: BACRMUI   Date: 2.Sep.2014
------Comments: Store master SequenceID to know modified data are there or not.
------*******************************************************************/
ALTER TABLE OpportunityBizDocs ADD numMasterBizdocSequenceID VARCHAR(50)
--------------------------------------------------------------------------

/*
Add this into Web.config for maximizing json length
<system.web.extensions>
            <scripting>
                <webServices>
                    <jsonSerialization maxJsonLength="50000000"/>
                </webServices>
            </scripting>
        </system.web.extensions>
*/

------/******************************************************************
------Project: BACRMUI   Date: 29.Aug.2014
------Comments: Copy Original 4 Decimal Deal Amount To a new Field
------*******************************************************************/
BEGIN TRANSACTION

ALTER TABLE [dbo].[OpportunityMaster] ADD monDealAmountWith4Decimals MONEY 
ALTER TABLE [dbo].[OpportunityBizDocs] ADD monDealAmountWith4Decimals MONEY 
ALTER TABLE [dbo].DepositMaster ADD monAppliedAmountWith4Decimals MONEY ,monDepositAmountWith4Decimals MONEY ,monRefundAmountWith4Decimals MONEY 
ALTER TABLE [dbo].DepositMaster ADD monAppliedAmountWith4Decimals MONEY ,monDepositAmountWith4Decimals MONEY ,monRefundAmountWith4Decimals MONEY 
ALTER TABLE [dbo].ReturnHeader ADD monAmountWith4Decimals MONEY ,
								   monBizDocAmountWith4Decimals MONEY ,
								   monBizDocUsedAmountWith4Decimals MONEY,
								   monTotalTaxWith4Decimals MONEY,
								   monTotalDiscountWith4Decimals MONEY 

UPDATE [dbo].[OpportunityMaster] SET [monDealAmountWith4Decimals] = ISNULL(monDealAmount,0)
UPDATE [dbo].[OpportunityBizDocs] SET [monDealAmountWith4Decimals] = ISNULL(monDealAmount,0)

UPDATE [dbo].[OpportunityMaster] SET monDealAmount = ROUND(ISNULL(monDealAmount,0),2),
UPDATE [dbo].[OpportunityBizDocs] SET monDealAmount = ROUND(ISNULL(monDealAmount,0),2)

UPDATE [dbo].DepositMaster SET monAppliedAmountWith4Decimals  = [monAppliedAmount],
								monDepositAmountWith4Decimals = [monDepositAmount],
								monRefundAmountWith4Decimals = [monRefundAmount]

UPDATE [dbo].DepositMaster SET [monAppliedAmount] = ROUND(ISNULL([monAppliedAmount],0),2),
								[monDepositAmount] = ROUND(ISNULL([monDepositAmount],0),2),
								[monRefundAmount] = ROUND(ISNULL([monRefundAmount],0),2)

UPDATE [dbo].[ReturnHeader] SET monAmountWith4Decimals  = monAmount,
								monBizDocAmountWith4Decimals = monBizDocAmount,
								monBizDocUsedAmountWith4Decimals = monBizDocUsedAmount,
								monTotalTaxWith4Decimals = monTotalTax,
								monTotalDiscountWith4Decimals = monTotalDiscount

UPDATE [dbo].[ReturnHeader] SET monAmount = ROUND(ISNULL(monAmount,0),2),
								monBizDocAmount = ROUND(ISNULL(monBizDocAmount,0),2),
								monBizDocUsedAmount = ROUND(ISNULL(monBizDocUsedAmount,0),2),
								monTotalTax = ROUND(ISNULL(monTotalTax,0),2),
								monTotalDiscount = ROUND(ISNULL(monTotalDiscount,0),2)
ROLLBACK
---------------------------------------------


