
/******************************************************************
Project: E-Banking Integration for Bank Of America    Date: 18/01/2013
Comments: 
*******************************************************************/
BEGIN TRANSACTION
GO
INSERT INTO [dbo].[BankMaster]
           ([tintBankType]
           ,[vcFIId]
           ,[vcFIName]
           ,[vcFIOrganization]
           ,[vcFIOFXServerUrl]
           ,[vcBankID]
           ,[vcOFXAccessKey])
     VALUES
           (2,'6812','Bank of America','HAN','https://ofx.bankofamerica.com/cgi-forte/fortecgi?servicename=ofx_2-3&pagename=ofx','','')
GO
ROLLBACK TRANSACTION

/******************************************************************
Project:    Date: 18-1-2013
Comments: Note : After adding Error message it requires to click get Error Message button on Maintanance Page .
*******************************************************************/
BEGIN TRANSACTION
INSERT INTO dbo.ErrorMaster (
	vcErrorCode,
	vcErrorDesc,
	tintApplication
) VALUES ( 
	/* vcErrorCode - varchar(6) */ 'ERR068',
	/* vcErrorDesc - nvarchar(max) */ N' Shipping information you provided does not seem valid.Please re enter details or contact site administrator.',
	/* tintApplication - tinyint */ 3 ) 
GO
ROLLBACK TRANSACTION

/******************************************************************
Project: BACRMUI   Date: 18.01.2013
Comments: TO Solve Duplicate Records in Import Configure Columns
*******************************************************************/
BEGIN TRANSACTION

DELETE * FROM [DycFormConfigurationDetails] WHERE tintPageType = 4 AND [numUserCntId] = 0
AND [numFormId] IN (20,48,31,45,55,50,36)

ROLLBACK

/******************************************************************
Project: BizService   Date: 18.01.2013
Comments: 
----------------- BizService : ADD THIS KEYS TO app.Config ------------------------
*******************************************************************/

<add key="WaitRecords" value="10"></add>
<add key="WaitMiliSeconds" value="5000"></add> <!-- value in MiliSeconds-->
<add key="IsDebugMode" value="True"></add>
-----------------------------------------------------------------------------------

/******************************************************************
Project: Bug fixing Date: 14.01.2013
Comments: 
*******************************************************************/
/*1 index was modified to include non-key columns*/
DROP INDEX [IX_General_Journal_Details] ON [dbo].[General_Journal_Details] 
CREATE NONCLUSTERED INDEX [IX_General_Journal_Details] ON [dbo].[General_Journal_Details] 
(
 [numTransactionId] ASC,
 [numDebitAmt] ASC,
 [numChartAcntId] ASC,
 [numCustomerId] ASC,
 [numJournalId] ASC
)
INCLUDE ( [vcReference],
[bitReconcile],
[bitCleared],
[varDescription],
[numCreditAmt])
/*2nd index was on a small table which was involved in multiple scans*/
CREATE NONCLUSTERED INDEX [IX_CheckHeader_numReferenceID] ON [dbo].[CheckHeader] 
(
 [numReferenceID] ASC,
 [tintReferenceType] ASC,
 [numCheckHeaderID] ASC
)
INCLUDE ( [vcMemo],
[monAmount],
[numCheckNo])

----------------------------------------------------From kamal----------------------------------------------
GO
/****** Object:  Table [dbo].[OppFulfillmentBizDocsStatusHistory]    Script Date: 01/13/2013 13:15:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OppFulfillmentBizDocsStatusHistory](
	[numOppBizDocStatus] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numDomainId] [numeric](18, 0) NULL,
	[numOppId] [numeric](18, 0) NULL,
	[numOppBizDocsId] [numeric](18, 0) NULL,
	[numBizDocStatus] [numeric](18, 0) NULL,
	[numUserCntID] [numeric](18, 0) NULL,
	[dtCreatedDate] [datetime] NULL,
 CONSTRAINT [PK_OppFulfillmentBizDocsStatusHistory] PRIMARY KEY CLUSTERED 
(
	[numOppBizDocStatus] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
