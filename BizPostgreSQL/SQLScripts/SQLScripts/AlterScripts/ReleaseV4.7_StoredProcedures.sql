/******************************************************************
Project: Release 4.7 Date: 29.Jun.2015
Comments: STORED PROCEDURES
*******************************************************************/

GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_CheckInventoryForLandedCost' ) 
    DROP PROCEDURE USP_CheckInventoryForLandedCost
GO
CREATE PROCEDURE [dbo].[USP_CheckInventoryForLandedCost]
	@numDomainID NUMERIC(18,0),
	@numOppID NUMERIC(18,0)
AS
BEGIN
	IF (SELECT COUNT(*) FROM OpportunityMaster WHERE numOppId=@numOppID) > 0
	BEGIN
		IF
		(
			SELECT
				COUNT(*)
			FROM
				OpportunityItems
			INNER JOIN
				Item 
			ON
				OpportunityItems.numItemCode = Item.numItemCode
			OUTER APPLY
			(
				SELECT
					ISNULL((SUM(numOnHand) + SUM(numAllocation)),0) AS Qty
				FROM
					WarehouseItems
				WHERE 
					numItemID = Item.numItemCode
			) AS TotalOnHand
			WHERE
				numOppId = @numOppID
				AND ISNULL(numWarehouseItmsID,0) > 0
				AND TotalOnHand.Qty < OpportunityItems.numUnitHour
		) > 0
		BEGIN
			SELECT 0
		END
		ELSE
		BEGIN
			SELECT 1
		END
	END
	ELSE
	BEGIN
		SELECT 0
	END
END
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetDataForIndexing' ) 
    DROP PROCEDURE USP_GetDataForIndexing
GO
-- exec USP_GetDataForIndexing @tintMode=0,@numDomainID=172,@numSiteID=90,@bitSchemaOnly=0,@tintAddUpdateMode=1,@bitDeleteAll=1
CREATE PROCEDURE USP_GetDataForIndexing
    @tintMode AS TINYINT = 0,
    @numDomainID AS NUMERIC,
    @numSiteID AS NUMERIC,
    @bitSchemaOnly AS BIT=0,
    @tintAddUpdateMode AS TINYINT, -- 1 = add , 2= update ,3=Deleted items
    @bitDeleteAll BIT=0 --Used for rebuilding all index from scratch
AS
BEGIN
    IF @tintMode = 0 -- items 
        BEGIN
        DECLARE @strSQL VARCHAR(4000)
        DECLARE @Fields VARCHAR(1000)
        DECLARE @numFldID VARCHAR(15)
        DECLARE @vcFldname VARCHAR(200)
        DECLARE @vcDBFldname VARCHAR(200)
        DECLARE @vcLookBackTableName VARCHAR(200)
        DECLARE @intRowNum INT
        DECLARE @strSQLUpdate VARCHAR(4000)
        DECLARE @Custom AS BIT
--SELECT * FROM View_DynamicColumns WHERE numFormid=30


--Select fields set for simple search from e-com settings as well fields from advance search
					SELECT  ROW_NUMBER() 
        OVER (ORDER BY vcFormFieldName) AS tintOrder,*
					INTO    #tempCustomField
					FROM    ( SELECT   distinct numFieldID as numFormFieldId,
										vcDbColumnName,
										vcFieldName as vcFormFieldName,
										0 AS Custom,
										vcLookBackTableName
							  FROM      View_DynamicColumns
							  WHERE     numFormId = 30
										AND numDomainID = @numDomainID
--										AND DFC.tintPageType = 1 --Commented so can agreegate all fields of simple and advance search
										AND isnull(numRelCntType,0) = 0
										AND isnull(bitCustom,0) = 0
										AND ISNULL(bitSettingField, 0) = 1
							  UNION
							  SELECT    numFieldID as numFormFieldId,
										vcFieldName,
										vcFieldName,
										1 AS Custom,
										'' vcLookBackTableName
							  FROM      View_DynamicCustomColumns
							  WHERE     Grp_id = 5
										AND numFormId = 30
										AND numDomainID = @numDomainID
										AND isnull(numRelCntType,0) = 0
--										AND isnull(DFC.bitCustom,0) = 1
--										AND ISNULL(DFC.tintPageType,0) = 1 --Commented so can agreegate all fields of simple and advance search

							) X
       
       IF @bitSchemaOnly =1
       BEGIN
			SELECT * FROM #tempCustomField
	   		RETURN
	   END
	   IF @bitDeleteAll = 1
	   BEGIN
	   		DELETE FROM LuceneItemsIndex WHERE numDomainID = @numDomainID AND numSiteID = @numSiteID
	   END
       
       --Create Fields query 
		 SET @Fields = '';
         SELECT TOP 1  @intRowNum = (tintOrder +1), @numFldID=numFormFieldId,@vcFldname=vcFormFieldName,@vcDBFldName=vcDbColumnName,@vcLookBackTableName=vcLookBackTableName
				FROM #tempCustomField WHERE Custom=0 ORDER BY tintOrder
				while @intRowNum>0                                                                                  
				 BEGIN
--					PRINT @numFldID
					IF(@vcDBFldName='numItemCode')
					set @vcDBFldName ='I.numItemCode'
				 
					SET @Fields = @Fields + ','  + @vcDBFldName + ' as ['  + @vcFldname + ']' 
					 

						SELECT TOP 1 @intRowNum = (tintOrder +1), @numFldID=numFormFieldId,@vcFldname=vcFormFieldName ,@vcDBFldName=vcDbColumnName,@vcLookBackTableName=vcLookBackTableName
						FROM #tempCustomField WHERE tintOrder >=@intRowNum AND Custom=0 ORDER BY tintOrder
							           
						if @@rowcount=0 set @intRowNum=0                                                                                          
				 END
       
--       PRINT @Fields
       
       
       SET @strSQL =  'SELECT  I.numItemCode AS numItemCode
			' + @Fields + '            
            INTO #DataForIndexing
            FROM    dbo.Item I 
            LEFT JOIN dbo.ItemExtendedDetails IED
            ON I.numItemCode = IED.numItemCode
            WHERE   I.numDomainID = ' + convert(varchar(10),@numDomainID) + '
                    AND I.numItemCode IN (
                    SELECT  numItemID
                    FROM    dbo.ItemCategory
                    WHERE ISNULL(I.IsArchieve,0) = 0 AND  numCategoryID IN ( SELECT   numCategoryID
                                               FROM     dbo.SiteCategories
                                               WHERE    numSiteID = ' + convert(varchar(10),@numSiteID) +' )) '
            
        IF @tintAddUpdateMode = 1 -- Add mode
			SET @strSQL = @strSQL +' and I.numItemCode Not in (SELECT numItemCode FROM LuceneItemsIndex WHERE numDomainID = '+ CONVERT(VARCHAR(10),@numDomainID) +  ' and numSiteID = ' + CONVERT(VARCHAR(10),@numSiteID) + ') '
		IF @tintAddUpdateMode = 2 -- Update mode
			SET @strSQL = @strSQL +' and bintModifiedDate BETWEEN  DATEADD( hour,-1,GETUTCDATE()) AND GETUTCDATE() 
			AND I.numItemCode in(SELECT I1.numItemCode FROM dbo.Item I1 INNER JOIN dbo.ItemCategory IC ON I1.numItemCode = IC.numItemID
			INNER JOIN dbo.SiteCategories SC ON IC.numCategoryID = SC.numCategoryID
			WHERE SC.numSiteID = ' + CONVERT(VARCHAR(10),@numSiteID) + ' AND I1.numDomainID= ' + CONVERT(VARCHAR(10),@numDomainID) + ')'
		IF @tintAddUpdateMode = 3 -- Delete mode
		BEGIN
			SET @strSQL = 'SELECT numItemCode FROM LuceneItemsIndex WHERE bitItemDeleted=1 and numDomainID = '+ CONVERT(VARCHAR(10),@numDomainID) +  ' and numSiteID = ' + CONVERT(VARCHAR(10),@numSiteID) 
			EXEC(@strSQL);
			RETURN
		END
			
       
     
       
       

				SET @strSQLUpdate=''
			--Add  custom Fields and data to table
				SELECT TOP 1  @intRowNum = (tintOrder +1), @numFldID=numFormFieldId,@vcFldname=vcFormFieldName
				FROM #tempCustomField WHERE Custom=1 ORDER BY tintOrder
				while @intRowNum>0                                                                                  
				 BEGIN
--				 PRINT @numFldID
					
				 			SET @strSQLUpdate = @strSQLUpdate + 'alter table #DataForIndexing add ['+@vcFldname+'_CustomField] varchar(500);'
							set @strSQLUpdate=@strSQLUpdate+ 'update #DataForIndexing set [' +@vcFldname + '_CustomField]=dbo.GetCustFldValueItem('+@numFldID+',numItemCode) where numItemCode>0'
							
					 

						SELECT TOP 1 @intRowNum = (tintOrder +1), @numFldID=numFormFieldId,@vcFldname=vcFormFieldName
						FROM #tempCustomField WHERE tintOrder >=@intRowNum AND Custom=1 ORDER BY tintOrder
							           
						if @@rowcount=0 set @intRowNum=0
				 END
				 SET @strSQLUpdate = @strSQLUpdate + ' SELECT * FROM #DataForIndexing'
				 
				 
				 IF @bitSchemaOnly=0
				 BEGIN
				 	SET @strSQLUpdate = @strSQLUpdate + ' INSERT INTO LuceneItemsIndex
				 	SELECT numItemCode,1,0,'+ CONVERT(VARCHAR(10), @numDomainID) +','+ CONVERT(VARCHAR(10), @numSiteID) +'  FROM #DataForIndexing where numItemCode not in (select numItemCode from LuceneItemsIndex where numDomainID ='+ CONVERT(VARCHAR(10), @numDomainID) +' and numSiteID = ' + CONVERT(VARCHAR(10),@numSiteID) + ' )'
				 END
				 
				 
				 SET @strSQLUpdate = @strSQLUpdate + ' DROP TABLE #DataForIndexing
												DROP TABLE #tempCustomField '
				 
				 SET @strSQL =@strSQL + @strSQLUpdate;
				 
				 
				 PRINT @strSQL
				 exec (@strSQL)
				 
				 
				 
				 
			
        END
        
	IF @tintMode= 1 
	BEGIN
		RETURN	
	END
	
	IF @tintMode= 2
	BEGIN
		RETURN	
	END
	
END
-- exec Usp_GetItemAttributesForAPI @numItemCode=822625,@numDomainID=170,@WebApiId = 5
/****** Subject:  StoredProcedure [dbo].[Usp_GetItemAttributesForAPI]    Script Date: 12th Jan,2015 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--created by Manish Anjara
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'Usp_GetItemAttributesForAPI' )
    DROP PROCEDURE Usp_GetItemAttributesForAPI
GO
CREATE PROCEDURE [dbo].[Usp_GetItemAttributesForAPI]
    @numItemCode AS NUMERIC(9) = 0 ,
    @numDomainID AS NUMERIC(9) ,
    @WebApiId AS INT
    --@byteMode AS TINYINT = 0
AS
    DECLARE @numDefaultWarehouseID AS NUMERIC(18)
    SELECT  @numDefaultWarehouseID = ISNULL(numwarehouseID, 0)
    FROM    [dbo].[WebAPIDetail] AS WAD
    WHERE   [WAD].[numDomainId] = @numDomainID
            AND [WAD].[WebApiId] = @WebApiId
    IF ISNULL(@numDefaultWarehouseID, 0) = 0
        BEGIN
            SELECT TOP 1
                    *
            FROM    [dbo].[WareHouseItems] AS WHI
            WHERE   [WHI].[numItemID] = @numItemCode
                    AND [WHI].[numDomainID] = @numDomainID
        END


    SELECT  DISTINCT
            FMST.fld_id ,
            Fld_label ,
            ISNULL(FMST.numlistid, 0) AS numlistid ,
            [LD].[numListItemID] ,
            [LD].[vcData]
    INTO    #tmpSpec
    FROM    CFW_Fld_Master FMST
            LEFT JOIN CFw_Grp_Master Tmst ON Tmst.Grp_id = FMST.subgrp
            JOIN CFW_Loc_Master Lmst ON Lmst.Loc_id = FMST.Grp_id
            JOIN [dbo].[ListMaster] AS LM ON [FMST].[numlistid] = LM.[numListID]
            JOIN [dbo].[ListDetails] AS LD ON LD.[numListID] = LM.[numListID]
                                              AND [FMST].[numlistid] = LD.[numListID]
            JOIN [dbo].[ItemGroupsDTL] AS IGD ON IGD.[numOppAccAttrID] = [FMST].[Fld_id]
            JOIN [dbo].[CFW_Fld_Values_Serialized_Items] AS CFVSI ON [CFVSI].[Fld_ID] = [FMST].[Fld_id]
                                                              AND CONVERT(NUMERIC, [CFVSI].[Fld_Value]) = [LD].[numListItemID]
                                                              AND [CFVSI].[RecId] IN (
                                                              SELECT
                                                              numWarehouseItemID
                                                              FROM
                                                              [dbo].[WareHouseItems]
                                                              WHERE
                                                              numwarehouseID = @numDefaultWarehouseID
                                                              AND numDomainID = @numDomainID
                                                              AND numItemID = @numItemCode )
    WHERE   FMST.numDomainID = @numDomainID
            AND Lmst.Loc_id = 9
    ORDER BY [fmst].[Fld_id]

	--SELECT * FROM [#tmpSpec] AS TS
    SELECT DISTINCT
            TS.fld_label [Name] ,
            STUFF(( SELECT  ',' + CAST([vcData] AS VARCHAR(200))
                    FROM    #tmpSpec
                    WHERE   [ts].[Fld_id] = #tmpSpec.[Fld_id]
                  FOR
                    XML PATH('')
                  ), 1, 1, '') [Value]
    FROM    [#tmpSpec] AS TS
    

    DECLARE @bitSerialize AS BIT                      
    DECLARE @str AS NVARCHAR(MAX)                 
    DECLARE @str1 AS NVARCHAR(MAX)               
    DECLARE @ColName AS VARCHAR(25)                      
    SET @str = ''                       
                        
    DECLARE @bitKitParent BIT
    DECLARE @numItemGroupID AS NUMERIC(9)                        
                        
    SELECT  @numItemGroupID = numItemGroup ,
            @bitSerialize = CASE WHEN bitSerialized = 0 THEN bitLotNo
                                 ELSE bitSerialized
                            END ,
            @bitKitParent = ( CASE WHEN ISNULL(bitKitParent, 0) = 1
                                        AND ISNULL(bitAssembly, 0) = 1 THEN 0
                                   WHEN ISNULL(bitKitParent, 0) = 1 THEN 1
                                   ELSE 0
                              END )
    FROM    Item
    WHERE   numItemCode = @numItemCode                        
    IF @bitSerialize = 1
        SET @ColName = 'numWareHouseItmsDTLID,1'              
    ELSE
        SET @ColName = 'numWareHouseItemID,0'              
              
--Create a Temporary table to hold data                                                            
    CREATE TABLE #tempTable
        (
          ID INT IDENTITY
                 PRIMARY KEY ,
          numCusFlDItemID NUMERIC(9)
        )                         
                        
    INSERT  INTO #tempTable
            ( numCusFlDItemID
            )
            SELECT DISTINCT
                    ( numOppAccAttrID )
            FROM    ItemGroupsDTL
            WHERE   numItemGroupID = @numItemGroupID
                    AND tintType = 2                       
                          
                 
    DECLARE @ID AS NUMERIC(9)                        
    DECLARE @numCusFlDItemID AS VARCHAR(20)                        
    DECLARE @fld_label AS VARCHAR(100) ,
        @fld_type AS VARCHAR(100)                        
    SET @ID = 0                        
    SELECT TOP 1
            @ID = ID ,
            @numCusFlDItemID = numCusFlDItemID ,
            @fld_label = fld_label ,
            @fld_type = fld_type
    FROM    #tempTable
            JOIN CFW_Fld_Master ON numCusFlDItemID = Fld_ID                        
                         
    WHILE @ID > 0
        BEGIN                        
                          
           -- SET @str = @str + ',  dbo.GetCustFldItems(' + @numCusFlDItemID + ',9,' + @ColName + ') as [' + @fld_label + ']'
	
            --IF @byteMode = 1
            SET @str = @str + ', dbo.GetCustFldItemsValue(' + @numCusFlDItemID + ',9,' + @ColName + ',''' + @fld_type + ''') as [' + @fld_label + '_c]'                                        
                          
            SELECT TOP 1
                    @ID = ID ,
                    @numCusFlDItemID = numCusFlDItemID ,
                    @fld_label = fld_label
            FROM    #tempTable
                    JOIN CFW_Fld_Master ON numCusFlDItemID = Fld_ID
                                           AND ID > @ID                        
            IF @@rowcount = 0
                SET @ID = 0                        
                          
        END                        
                       
--      

    DECLARE @KitOnHand AS NUMERIC(9);
    SET @KitOnHand = 0

    SET @str1 = 'select '

    SET @str1 = @str1 + 'I.numItemCode,'
	
    SET @str1 = @str1
        + 'numWareHouseItemID,isnull(vcWarehouse,'''') + '': '' + isnull(WL.vcLocation,'''') vcWarehouse,W.numWareHouseID,
Case when @bitKitParent=1 then ISNULL(dbo.fn_GetKitInventoryByWarehouse(I.numItemCode, W.numWareHouseID),0) else isnull(numOnHand,0) end as [OnHand],
CAST((dbo.fn_UOMConversion(ISNULL(I.numBaseUnit,0),I.numItemCode,I.numDomainId,ISNULL(I.numPurchaseUnit,0)) * Case when @bitKitParent=1 THEN ISNULL(dbo.fn_GetKitInventoryByWarehouse(I.numItemCode, W.numWareHouseID),0) ELSE isnull(numOnHand,0) end) as numeric(18,2)) as PurchaseOnHand,
CAST((dbo.fn_UOMConversion(ISNULL(I.numBaseUnit,0),I.numItemCode,I.numDomainId,ISNULL(I.numSaleUnit,0)) * Case when @bitKitParent=1 THEN ISNULL(dbo.fn_GetKitInventoryByWarehouse(I.numItemCode, W.numWareHouseID),0) ELSE isnull(numOnHand,0) end) as numeric(18,2)) as SalesOnHand,
Case when @bitKitParent=1 THEN 0 ELSE isnull(numReorder,0) END as [Reorder] 
,Case when @bitKitParent=1 THEN 0 ELSE isnull(numOnOrder,0) END as [OnOrder]
,Case when @bitKitParent=1 THEN 0 ELSE isnull(numAllocation,0) END as [Allocation] 
,Case when @bitKitParent=1 THEN 0 ELSE isnull(numBackOrder,0) END as [BackOrder]
,0 as Op_Flag , isnull(WL.vcLocation,'''') Location,isnull(WL.numWLocationID,0) as numWLocationID,
round(isnull(monWListPrice,0),2) Price,isnull(vcWHSKU,'''') as SKU,isnull(vcBarCode,'''') as BarCode '
        + CASE WHEN @bitSerialize = 1 THEN ' '
               ELSE @str
          END
        + '                   
,(SELECT CASE WHEN ISNULL(subI.bitKitParent,0) = 1 AND ISNULL((SELECT COUNT(*) FROM OpportunityItems 
															   WHERE OpportunityItems.numItemCode = WareHouseItems.numItemID
															   AND WareHouseItems.numWareHouseItemID = OpportunityItems.numWarehouseItmsID),0) = 0 THEN 1
			ELSE 0 
	    END 
FROM Item subI WHERE subI.numItemCode = WareHouseItems.numItemID) [IsDeleteKitWarehouse],@bitKitParent [bitKitParent]
from WareHouseItems                           
join Warehouses W                             
on W.numWareHouseID=WareHouseItems.numWareHouseID 
left join WarehouseLocation WL on WL.numWLocationID = WareHouseItems.numWLocationID
join Item I on I.numItemCode=WareHouseItems.numItemID and WareHouseItems.numDomainId=I.numDomainId
where numItemID=' + CONVERT(VARCHAR(15), @numItemCode)
        + ' AND WareHouseItems.numWarehouseItemID IN (
                                                              SELECT
                                                              numWarehouseItemID
                                                              FROM
                                                              [dbo].[WareHouseItems]
                                                              WHERE
                                                              numwarehouseID = '
        + CONVERT(VARCHAR(15), @numDefaultWarehouseID)
        + '
                                                              AND numDomainID = '
        + CONVERT(VARCHAR(15), @numDomainID)
        + '
                                                              AND numItemID = '
        + CONVERT(VARCHAR(15), @numItemCode) + ' ) '
   
    PRINT ( @str1 )           

    EXECUTE sp_executeSQL @str1, N'@bitKitParent bit', @bitKitParent
                       

GO

GO
/****** Object:  StoredProcedure [dbo].[USP_GetItems_API]    Script Date: 05/07/2009 17:44:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---created by Chintan prajapati
--EXEC USP_GetItems_API 1,1,'',''
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetItems_API')
DROP PROCEDURE USP_GetItems_API
GO
CREATE PROCEDURE [dbo].[USP_GetItems_API]
          @numDomainID AS NUMERIC(9),
          @WebAPIId    AS int,
          @CreatedDate AS VARCHAR(50) ='',
          @ModifiedDate AS VARCHAR(50)='' 
AS
IF @WebAPIId  = 5
BEGIN
	SELECT   numItemCode,
				   vcItemName,
					ISNULL((SELECT TOP 1([monWListPrice])
					 FROM   [WareHouseItems]
					 WHERE  numItemId = numItemCode
							AND [numWareHouseID] IN (SELECT ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail 
													 WHERE  [numDomainId] = I.[numDomainID] AND WebApiId = @WebAPIId)),
					I.monListPrice) AS monListPrice,
				   vcModelID,
				   ISNULL(fltWeight,0) intWeight,
				   ISNULL((SELECT TOP 1([numOnHand])
					 FROM   [WareHouseItems]
					 WHERE  numItemId = numItemCode
							AND [numWareHouseID] IN (SELECT ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail 
													 WHERE  [numDomainId] = I.[numDomainID] AND WebApiId = @WebAPIId)),
					0) AS QtyOnHand,
				   ISNULL(IA.vcAPIItemID,0) AS vcAPIItemID, ISNULL(IA.vcSKU, ISNULL(I.[vcSKU],'')) [vcAPISKU],I.numDomainID

		  FROM     Item I 
		  LEFT OUTER JOIN [ItemAPI] IA ON I.[numItemCode] = IA.[numItemID] AND IA.WebApiId = @WebAPIId 
		  WHERE   
			 I.numDomainID = @numDomainID 
			AND 1 = (CASE 
						WHEN @CreatedDate = '' THEN 1
						ELSE (CASE WHEN I.bintCreatedDate > @CreatedDate THEN 1	ELSE 0 END)
					END) 
			AND 1 = (CASE 
						WHEN @ModifiedDate = '' THEN 1
						ELSE (CASE WHEN I.bintModifiedDate > @ModifiedDate THEN 1 ELSE 0 END)
					END) 
			AND  LEN(vcItemName) >0
			AND I.charItemType ='P'
			AND vcExportToAPI IS NOT NULL
			AND @WebAPIId IN (SELECT * FROM dbo.Split(vcExportToAPI,','))	
END
ELSE
BEGIN
	-- GET DEFAULT WAREHOUSE SET FOR MARKET PLACE     
	DECLARE @numDefaultWarehouse AS NUMERIC(18,0)
	SELECT @numDefaultWarehouse=numWareHouseID FROM WebAPIDetail WHERE numDomainID=@numDomainID AND WebApiId=@WebAPIId

	DECLARE @TempItem TABLE
	(
		numDomainID NUMERIC(18,0),
		numItemCode NUMERIC(18,0),
		vcItemName VARCHAR(300),
		vcModelID VARCHAR(200),
		numItemGroup NUMERIC(18,0),
		fltWeight float,
		fltLength float,
		fltHeight float,
		fltWidth float,
		vcManufacturer VARCHAR(250),
		IsArchieve BIT,
		txtItemDesc VARCHAR(1000),
		vcSKU VARCHAR(50),
		numBarCodeId VARCHAR(50),
		bintCreatedDate DATETIME,
		bintModifiedDate DATETIME
	)

	INSERT INTO 
		@TempItem
	SELECT
		numDomainID,
		numItemCode,
		ISNULL(vcItemName,''),
		ISNULL(vcModelID,''),
		ISNULL(numItemGroup,0) numItemGroup,
		ISNULL(fltWeight,0) fltWeight,
		ISNULL(fltLength,0) fltLength,
		ISNULL(fltHeight,0) fltHeight,
		ISNULL(fltWidth,0) fltWidth,
		ISNULL(vcManufacturer,'') vcManufacturer,
		ISNULL(IsArchieve,0) IsArchieve,
		ISNULL(txtItemDesc,'') txtItemDesc,
		ISNULL(vcSKU,''),
		ISNULL(numBarCodeId,''),
		bintCreatedDate,
		bintModifiedDate
	FROM
		Item
	WHERE
		Item.numDomainID = @numDomainID
		AND  LEN(Item.vcItemName) > 0
		AND Item.charItemType ='P'
		AND Item.vcExportToAPI IS NOT NULL
		AND @WebAPIId IN (SELECT * FROM dbo.Split(vcExportToAPI,','))

	SELECT
		X.*
	FROM
	(
		SELECT
			Item.numDomainID,
			ISNULL(ItemAPI.vcAPIItemID,0) AS vcAPIItemID,
			Item.numItemCode,
			Item.vcItemName,
			Item.vcModelID,
			Item.numItemGroup,
			Item.fltWeight AS intWeight,
			Item.fltWeight,
			Item.fltLength,
			Item.fltHeight,
			Item.fltWidth,
			Item.vcManufacturer,
			Item.IsArchieve,
			Item.txtItemDesc,
			Item.vcSKU,
			Item.numBarCodeId,
			TempWarehouseItem.numOnHand AS QtyOnHand,
			TempWarehouseItem.monWListPrice AS monListPrice,
			(
				SELECT 
					numItemImageId, 
					vcPathForImage, 
					vcPathForTImage, 
					bitDefault,
					CASE 
						WHEN bitdefault = 1 THEN -1 
						ELSE isnull(intDisplayOrder,0) 
					END AS intDisplayOrder 
				FROM 
					ItemImages  
				WHERE 
					numItemCode=Item.numItemCode 
				ORDER BY 
					CASE WHEN bitdefault=1 THEN -1 ELSE ISNULL(intDisplayOrder,0) END  asc 
				FOR XML AUTO,ROOT('Images')
			) AS xmlItemImages,
			CAST(ISNULL(ItemExtendedDetails.txtDesc,'') AS VARCHAR(MAX)) AS vcExtendedDescToAPI
		FROM
			@TempItem Item
		LEFT JOIN
			ItemExtendedDetails
		ON
			ItemExtendedDetails.numItemCode = Item.numItemCode
		LEFT JOIN 
			ItemAPI 
		ON 
			ItemAPI.WebApiId = @WebAPIId 
			AND Item.numItemCode = ItemAPI.numItemID 
			AND Item.vcSKU = ItemAPI.vcSKU
		CROSS APPLY
		(
			SELECT
				ISNULL(SUM(numOnHand),0) AS numOnHand,
				ISNULL(MAX(monWListPrice),0) AS monWListPrice,
				MAX(dtModified) AS dtModified
			FROM
				WareHouseItems
			WHERE
				WareHouseItems.numItemID = Item.numItemCode
				AND WareHouseItems.numWareHouseID = @numDefaultWarehouse
			GROUP BY
				numWareHouseID
		) TempWarehouseItem
		WHERE
			numItemGroup = 0 --INVENTORY ITEM
			AND 1 = (CASE 
						WHEN @CreatedDate = '' THEN 1
						ELSE (CASE 
								WHEN Item.bintCreatedDate > @CreatedDate THEN 1
								ELSE 0
							  END)
					END) 
			AND 1 = (CASE 
						WHEN @ModifiedDate = '' THEN 1
						ELSE (CASE 
								WHEN Item.bintModifiedDate > @ModifiedDate THEN 1
								ELSE 0
							  END)
					END) 
		UNION
		SELECT
			Item.numDomainID,
			ISNULL(ItemAPI.vcAPIItemID,0) AS vcAPIItemID,
			Item.numItemCode,
			Item.vcItemName,
			Item.vcModelID,
			Item.numItemGroup,
			Item.fltWeight AS intWeight,
			Item.fltWeight,
			Item.fltLength,
			Item.fltHeight,
			Item.fltWidth,
			Item.vcManufacturer,
			Item.IsArchieve,
			Item.txtItemDesc,
			ISNULL(WareHouseItems.vcWHSKU,'') vcSKU,
			ISNULL(WareHouseItems.vcBarCode,'') numBarCodeId,
			WareHouseItems.numOnHand AS QtyOnHand,
			WareHouseItems.monWListPrice AS monListPrice,
			(
				SELECT 
					numItemImageId, 
					vcPathForImage, 
					vcPathForTImage, 
					bitDefault,
					CASE 
						WHEN bitdefault = 1 THEN -1 
						ELSE isnull(intDisplayOrder,0) 
					END AS intDisplayOrder 
				FROM 
					ItemImages  
				WHERE 
					numItemCode=Item.numItemCode 
				ORDER BY 
					CASE WHEN bitdefault=1 THEN -1 ELSE ISNULL(intDisplayOrder,0) END ASC 
				FOR XML AUTO,ROOT('Images')
			) AS xmlItemImages,
			CAST(ISNULL(ItemExtendedDetails.txtDesc,'') AS VARCHAR(MAX)) AS vcExtendedDescToAPI
		FROM
			@TempItem Item
		INNER JOIN
			WareHouseItems
		ON
			WareHouseItems.numItemID = Item.numItemCode
			AND WareHouseItems.numWareHouseID = @numDefaultWarehouse
		LEFT JOIN
			ItemExtendedDetails
		ON
			ItemExtendedDetails.numItemCode = Item.numItemCode
		LEFT JOIN 
			ItemAPI 
		ON 
			ItemAPI.WebApiId = @WebAPIId 
			AND Item.numItemCode = ItemAPI.numItemID 
			AND Item.vcSKU = ItemAPI.vcSKU
		WHERE
			numItemGroup > 0 --MATRIX ITEM
			AND 1 = (CASE 
						WHEN @CreatedDate = '' THEN 1
						ELSE (CASE 
								WHEN Item.bintCreatedDate > @CreatedDate THEN 1
								ELSE 0
							  END)
					END) 
			AND 1 = (CASE 
						WHEN @ModifiedDate = '' THEN 1
						ELSE (CASE 
								WHEN Item.bintModifiedDate > @ModifiedDate THEN 1
								ELSE 0
							  END)
					END) 
	) AS X
	ORDER BY
		X.numItemCode ASC
END



  
  

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetScalerValue')
DROP PROCEDURE USP_GetScalerValue
GO
CREATE PROCEDURE USP_GetScalerValue 
@numDomainID AS NUMERIC,--Will receive SiteID when mode =2,12,9
@tintMode AS TINYINT,
@vcInput AS VARCHAR(1000),
@numUserCntId AS NUMERIC(9)=0
AS
BEGIN
	IF @tintMode = 1
	BEGIN
		SELECT numJournal_Id FROM dbo.General_Journal_Header WHERE numDomainId=@numDomainID and numBizDocsPaymentDetId= CONVERT(NUMERIC, @vcInput)
	END
	IF @tintMode = 2 
	BEGIN
		SELECT COUNT(*) FROM dbo.Sites WHERE vcHostName=@vcInput AND numSiteID <> @numDomainID 
	END
	IF @tintMode = 3 
	BEGIN
		SELECT COUNT(*) FROM dbo.Domain WHERE vcPortalName=@vcInput AND numDomainId <> @numDomainID
	END
	IF @tintMode = 4 --Item Name
	BEGIN
		SELECT dbo.GetItemName(@vcInput)
	END
	IF @tintMode = 5 --Company Name
	BEGIN
		SELECT dbo.fn_GetComapnyName(@vcInput)
	END
	IF @tintMode = 6 --Vendor Cost
	BEGIN
		SELECT dbo.fn_GetVendorCost(@vcInput)
	END
	IF @tintMode = 7 -- tintRuleFor
	BEGIN
		SELECT tintRuleFor FROM PriceBookRules WHERE numPricRuleID=@vcInput AND numDomainId =@numDomainId
	End
	IF @tintMode = 8 -- monSCreditBalance
	BEGIN
		SELECT isnull(monSCreditBalance,0) as monCreditBalance FROM DivisionMaster WHERE numDivisionID=@vcInput AND numDomainId =@numDomainId
	End
    IF @tintMode = 9 -- Check Site Page Exists
	BEGIN
		if exists(SELECT [vcPageName] FROM   SitePages WHERE  [numSiteID] = @numDomainId AND LOWER([vcPageURL]) = LOWER(@vcInput))
			SELECT 1 as SitePageExists
		else
			SELECT 0 as SitePageExists
	END
	IF @tintMode = 10 -- Category Name
	BEGIN
		SELECT ISNULL(vcCategoryName,'NA') FROM dbo.Category WHERE numCategoryID=CAST(@vcInput AS NUMERIC(9)) AND numDomainId =@numDomainId
	End
	IF @tintMode = 11 -- monPCreditBalance
	BEGIN
		SELECT isnull(monPCreditBalance,0) as monCreditBalance FROM DivisionMaster WHERE numDivisionID=@vcInput AND numDomainId =@numDomainId
	END
	 IF @tintMode = 12 -- Get Style ID from filename
	BEGIN
		SELECT TOP 1 ISNULL([numCssID],0) FROM  dbo.StyleSheets WHERE  [numSiteID] = @numDomainId AND LOWER([styleFileName]) = LOWER(@vcInput)
	END
	IF @tintMode = 13 -- numItemClassification
	BEGIN
		SELECT ISNULL(numItemClassification,0) as numItemClassification FROM  Item WHERE  numDomainID = @numDomainId AND numItemCode = @vcInput
	END
	IF @tintMode = 14 -- numAssignedTo
	BEGIN
		SELECT ISNULL(numAssignedTo,0) as numAssignedTo FROM  DivisionMaster WHERE  numDomainID = @numDomainId AND numDivisionID = @vcInput
	END
	IF @tintMode = 15 -- Get Currency ID 
	BEGIN
		SELECT numCurrencyID FROM Currency WHERE numDomainID = @numDomainID AND chrCurrency = @vcInput
	END
	IF @tintMode = 16 -- Total Order of Items where Warehouse exists
	BEGIN
		SELECT count(*) as TotalRecords FROM OpportunityMaster OM join OpportunityItems OI on OM.numOppId=OI.numOppId
			 WHERE OI.numItemCode = @vcInput AND OM.numDomainID = @numDomainID and OI.numWarehouseItmsID>0
	END
	IF @tintMode = 17 -- Get numBizDocID
	BEGIN
		SELECT numBizDocTempID FROM dbo.BizDocTemplate 
								WHERE numDomainID = @numDomainID
								  AND tintTemplateType = 0
								  AND bitDefault = 1
								  AND bitEnabled = 1
								  AND numBizDocID = @vcInput
	END
	IF @tintMode = 18 -- Get Domain DivisionID
	BEGIN
		SELECT numDivisionID FROM Domain WHERE numDomainID = @numDomainID
	END
	IF @tintMode = 19
	BEGIN
		SELECT numItemID FROM ItemAPI WHERE numDomainID = @numDomainID AND  vcAPIItemID = @vcInput
	END
	IF @tintMode = 20
	BEGIN
		SELECT numItemCode FROM Item WHERE numDomainID = @numDomainID AND  vcSKU = @vcInput
	END
	IF @tintMode = 21
	BEGIN
		SELECT numStateId FROM dbo.State S inner JOIN webApiStateMapping SM
		ON S.vcState =  SM.vcStateName 
		AND S.numDomainId = @numDomainId 
		AND ( LOWER(SM.vcAmazonStateCode) = LOWER(@vcInput) OR LOWER(SM.vcStateName) = LOWER(@vcInput))
	END
	IF @tintMode = 22
	BEGIN
		SELECT LD.numListItemId FROM ListDetails LD INNER Join WebApiCountryMapping CM ON 
		LD.vcData = CM.vcCountryName WHERE LD.numListId = 40 AND LD.numDomainId = @numDomainId AND
		CM.vcAmazonCountryCode = @vcInput
	END
   IF @tintMode = 23
   BEGIN
       select isnull(txtSignature,'') from userMaster where numUserDetailId = convert(numeric , @vcInput) and numDomainId = @numDomainId 
   END
   
   IF @tintMode = 24
   BEGIN
         DECLARE @dtEmailStartDate AS varchar(20),
                 @dtEmailEndDate AS varchar(20) ,
                 @intNoOfEmail AS numeric
            
        select @dtEmailStartDate =convert(varchar(20) , dtEmailStartDate) ,@dtEmailEndDate =convert(varchar(20), dtEmailEndDate), @intNoOfEmail = intNoOfEmail from Subscribers where numTargetDomainID=@numDomainID
       
        IF  Convert(datetime, Convert(int, GetDate())) >=  @dtEmailStartDate AND Convert(datetime, Convert(int, GetDate())) <=  @dtEmailEndDate  
            BEGIN
		          select  (convert(varchar(10) , count(*)) + '-' +(select top 1 convert(varchar(10), intNoOfEmail) from Subscribers where numTargetDomainID = @numDomainID)) as EmailData
                  from broadcastDTLs BD inner join BroadCast B on B.numBroadCastID = BD.numBroadCastID 
                  where B.numDomainId = @numDomainId AND bintBroadCastDate between @dtEmailStartDate and @dtEmailEndDate and bitBroadcasted = 1 and tintSucessfull = 1     		
			END       
   END  
   IF @tintMode = 25
	BEGIN
		SELECT IsNull(vcMsg,'') AS vcMsg FROM dbo.fn_GetEmailAlert(@numDomainId ,@vcInput,@numUserCntId)
	END
	IF @tintMode = 26
	BEGIN
		SELECT COUNT(*) AS Total FROM dbo.Communication WHERE numDomainID = @numDomainID and numContactId = @vcInput
	END 
	IF @tintMode = 27 -- Check Order Closed
	BEGIN
		SELECT ISNULL(tintshipped,0) AS tintshipped FROM OpportunityMaster  
			 WHERE numOppId = convert(numeric , @vcInput) AND numDomainID = @numDomainID
	END
	IF @tintMode = 28 -- General Journal Header for Bank Recon.
	BEGIN
		SELECT ISNULL(numJournal_Id,0) AS numJournal_Id FROM General_Journal_Header  
			 WHERE numReconcileID = convert(numeric , @vcInput) AND numDomainID = @numDomainID
	END
	IF @tintMode = 29 -- General Journal Header for Write Check
	BEGIN
		SELECT ISNULL(numJournal_Id,0) AS numJournal_Id FROM General_Journal_Header  
			 WHERE numCheckHeaderID = convert(numeric , @vcInput) AND numDomainID = @numDomainID
	END
	
	IF @tintMode = 30 -- Get Units for sample item to be created
	BEGIN
	 SELECT ISNULL(u.numUOMId,0) FROM 
        UOM u INNER JOIN Domain d ON u.numDomainID=d.numDomainID
        WHERE u.numDomainID=@numDomainID 
        AND d.numDomainID=@numDomainID 
        AND u.tintUnitType=(CASE WHEN ISNULL(d.charUnitSystem,'E')='E' THEN 1 WHEN d.charUnitSystem='M' THEN 2 END)
        AND u.vcUnitName ='Units'
	END
	
	IF @tintMode = 31 -- Get Magento Attribute Set for Current Domain
	BEGIN
		SELECT vcEighthFldValue FROM dbo.WebAPIDetail WHERE numDomainId = @numDomainID  AND webApiId = @vcInput
	END
	
	IF @tintMode = 32 -- Get WareHouseItemID for ItemID is existis for Current Domain
	BEGIN
		 SELECT ISNULL((SELECT TOP 1 numWareHouseItemID FROM  dbo.WareHouseItems WHERE numDomainId = @numDomainID  AND numItemID = @vcInput),0)AS  numWareHouseItemID
	END
	
	IF @tintMode = 33
	BEGIN
	SELECT ISNULL(MAX(CAST(ISNULL(numSequenceId,0) AS BigInt)),0) + 1 FROM dbo.OpportunityMaster OM JOIN dbo.OpportunityBizDocs OBD ON OM.numOppId=OBD.numOppId
			 WHERE OM.numDomainId=@numDomainID AND OBD.numBizDocId=@vcInput
	END	
	
	IF @tintMode = 34
	BEGIN
	SELECT ISNULL(vcOppRefOrderNo,'') FROM dbo.OpportunityMaster OM 
			 WHERE OM.numDomainId=@numDomainID AND OM.numOppID=@vcInput
	END
	
	IF @tintMode = 35
	BEGIN 
	SELECT ISNULL(bitAutolinkUnappliedPayment,0) FROM dbo.Domain  WHERE numDomainId=@numDomainID 
	END		 
	
	IF @tintMode = 36
	BEGIN 
		SELECT ISNULL(numJournalId,0) FROM dbo.General_Journal_Details WHERE numDomainId=@numDomainID AND numChartAcntId=@vcInput AND chBizDocItems='OE'
	END	
	
	IF @tintMode = 37
	BEGIN 
		SELECT ISNULL(numShippingServiceItemID,0) FROM dbo.Domain WHERE numDomainId=@numDomainID
	END	
	IF @tintMode = 38
	BEGIN
		DECLARE @numItemCode AS NUMERIC(18,0)
		
		--FIRST CHECK FOR ITEM LEVEL SKU
		SELECT TOP 1 @numItemCode=numItemCode FROM Item WHERE numDomainID = @numDomainID AND  vcSKU = @vcInput

		--IF ITEM NOT AVAILABLE WITH SKU CHECK FOR MATRIX ITEM (WAREHOUSE LEVEL SKU)
		IF ISNULL(@numItemCode,0) = 0
		BEGIN
			SELECT TOP 1 @numItemCode=numItemID FROM WarehouseItems WHERE numDomainID = @numDomainID AND vcWHSKU = @vcInput
		END

		SELECT CONVERT(VARCHAR(50),numItemCode)+'~'+CharItemType as numItemCode FROM Item WHERE numDomainID = @numDomainID AND numItemCode=@numItemCode
	END	
	IF @tintMode = 39 -- Get WareHouseItemID for SKU is exists for Current Domain
	BEGIN
	IF CHARINDEX('~',@vcInput) > 0
		BEGIN
			DECLARE @numItemID NUMERIC(18,0)
			DECLARE @vcSKU VARCHAR(50)
			SELECT @numItemID = strItem FROM dbo.SplitWordWithPosition(@vcInput,'~') WHERE RowNumber = 1
			SELECT @vcSKU = strItem FROM dbo.SplitWordWithPosition(@vcInput,'~') WHERE RowNumber = 2

			
			SELECT   ISNULL(( SELECT TOP 1
                            numWareHouseItemID
                     FROM   dbo.WareHouseItems
                     WHERE  numDomainId = @numDomainID
                            AND vcWHSKU = @vcSKU
                            AND numItemID = @numItemID
                   ), 0) AS numWareHouseItemID
		END
	ELSE
		BEGIN
			SELECT   ISNULL(( SELECT TOP 1
                            numWareHouseItemID
                     FROM   dbo.WareHouseItems
                     WHERE  numDomainId = @numDomainID
                            AND numItemID = @vcInput
                   ), 0) AS numWareHouseItemID
		END
	END 
END
--exec USP_GetScalerValue @numDomainID=1,@tintMode=24,@vcInput='0'
 
GO
/****** Object:  StoredProcedure [dbo].[USP_ItemDetails]    Script Date: 02/07/2010 15:31:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Anoop Jayaraj                                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_itemdetails')
DROP PROCEDURE usp_itemdetails
GO
CREATE PROCEDURE [dbo].[USP_ItemDetails]                                                  
@numItemCode as numeric(9) ,          
@ClientTimeZoneOffset as int                                               
as                                                 
   
   DECLARE @bitItemIsUsedInOrder AS BIT;SET @bitItemIsUsedInOrder=0
   
   DECLARE @numDomainId AS NUMERIC
   SELECT @numDomainId=numDomainId FROM Item WHERE numItemCode=@numItemCode
   
   IF GETUTCDATE()<'2013-03-15 23:59:39'
		SET @bitItemIsUsedInOrder=0
   ELSE IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityItems OI WHERE OI.numItemCode =@numItemCode) >0
		SET @bitItemIsUsedInOrder=1
   ELSE IF EXISTS(SELECT numItemID FROM dbo.General_Journal_Details WHERE numItemID= @numItemCode and numDomainId=@numDomainId AND chBizDocItems='IA1'   )
		set @bitItemIsUsedInOrder =1
                                               
select I.numItemCode, vcItemName, ISNULL(txtItemDesc,'') txtItemDesc,CAST( ISNULL(IED.txtDesc,'')  AS VARCHAR(MAX) ) vcExtendedDescToAPI, charItemType, 
case 
when charItemType='P' AND ISNULL(bitAssembly,0) = 1 then 'Assembly'
when charItemType='P' AND ISNULL(bitKitParent,0) = 1 then 'Kit'
when charItemType='P' AND ISNULL(bitAsset,0) = 1 AND ISNULL(bitRental,0)=0 then 'Asset'
when charItemType='P' AND ISNULL(bitAsset,0) = 1 AND ISNULL(bitRental,0)=1 then 'Rental Asset'
when charItemType='P' AND ISNULL(bitSerialized,0) = 1 AND ISNULL(bitAsset,0) = 0  then 'Serialized'
when charItemType='P' AND ISNULL(bitSerialized,0) = 1 AND ISNULL(bitAsset,0) = 1 AND ISNULL(bitRental,0)=0 then 'Serialized Asset'
when charItemType='P' AND ISNULL(bitSerialized,0) = 1 AND ISNULL(bitAsset,0) = 1 AND ISNULL(bitRental,0)=1 then 'Serialized Rental Asset'
when charItemType='P' AND ISNULL(bitLotNo,0)=1 THEN 'Lot #'
else case when charItemType='P' then 'Inventory' else '' end
end as InventoryItemType,
dbo.fn_GetItemChildMembershipCount(@numDomainId,I.numItemCode) AS numChildMembershipCount,
case when ISNULL(bitAssembly,0)=1 then dbo.fn_GetAssemblyPossibleWOQty(I.numItemCode) else 0 end AS numWOQty,
monListPrice,                   
numItemClassification, isnull(bitTaxable,0) as bitTaxable, 
(CASE WHEN I.numItemGroup > 0 THEN ISNULL(W.[vcWHSKU],'') ELSE ISNULL(vcSKU,'') END) AS vcSKU, 
ISNULL(bitKitParent,0) as bitKitParent,--, dtDateEntered,                  
 numVendorID, I.numDomainID, numCreatedBy, bintCreatedDate, bintModifiedDate,                   
numModifiedBy,
(SELECT TOP 1 vcPathForImage FROM ItemImages WHERE numItemCode = @numItemCode AND bitDefault = 1 AND bitIsImage = 1 ) AS vcPathForImage ,
(SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = @numItemCode AND bitDefault = 1 AND bitIsImage = 1) AS  vcPathForTImage, isnull(bitSerialized,0) as bitSerialized, vcModelID,                   
(SELECT numItemImageId,vcPathForImage,vcPathForTImage,bitDefault,case when bitdefault=1 then -1 
else isnull(intDisplayOrder,0) end as intDisplayOrder 
FROM ItemImages  
WHERE numItemCode=@numItemCode order by case when bitdefault=1 then -1 else isnull(intDisplayOrder,0) end  asc 
FOR XML AUTO,ROOT('Images')) AS xmlItemImages,
(SELECT STUFF((SELECT ',' + CONVERT(VARCHAR(50) , numCategoryID)  FROM dbo.ItemCategory WHERE numItemID = I.numItemCode FOR XML PATH('')), 1, 1, '')) AS vcItemCategories ,
numItemGroup, numCOGsChartAcntId, numAssetChartAcntId, numIncomeChartAcntId, monAverageCost,                   
monCampaignLabourCost,dbo.fn_GetContactName(numCreatedBy)+' ,'+convert(varchar(20),DateAdd(minute, -@ClientTimeZoneOffset,bintCreatedDate )) as CreatedBy ,                                      
dbo.fn_GetContactName(numModifiedBy)+' ,'+convert(varchar(20),DateAdd(minute, -@ClientTimeZoneOffset,bintModifiedDate )) as ModifiedBy,                      
sum(numOnHand) as numOnHand,                      
sum(numOnOrder) as numOnOrder,                      
sum(numReorder)  as numReorder,                      
sum(numAllocation)  as numAllocation,                      
sum(numBackOrder)  as numBackOrder,                   
isnull(fltWeight,0) as fltWeight,                
isnull(fltHeight,0) as fltHeight,                
isnull(fltWidth,0) as fltWidth,                
isnull(fltLength,0) as fltLength,                
isnull(bitFreeShipping,0) as bitFreeShipping,              
isnull(bitAllowBackOrder,0) as bitAllowBackOrder,              
isnull(vcUnitofMeasure,'Units') as vcUnitofMeasure,      
isnull(bitShowDeptItem,0) bitShowDeptItem,      
isnull(bitShowDeptItemDesc,0) bitShowDeptItemDesc,  
isnull(bitCalAmtBasedonDepItems ,0) bitCalAmtBasedonDepItems,
isnull(bitAssembly ,0) bitAssembly ,
isnull(cast(numBarCodeId as varchar(50)),'') as  numBarCodeId ,
isnull(I.vcManufacturer,'') as vcManufacturer,
ISNULL(I.numBaseUnit,0) AS numBaseUnit,ISNULL(I.numPurchaseUnit,0) AS numPurchaseUnit,ISNULL(I.numSaleUnit,0) AS numSaleUnit,
dbo.fn_GetUOMName(ISNULL(I.numBaseUnit,0)) AS vcBaseUnit,dbo.fn_GetUOMName(ISNULL(I.numPurchaseUnit,0)) AS vcPurchaseUnit,dbo.fn_GetUOMName(ISNULL(I.numSaleUnit,0)) AS vcSaleUnit,
isnull(bitLotNo,0) as bitLotNo,
ISNULL(IsArchieve,0) AS IsArchieve,
case when charItemType='P' then 'Inventory Item' when charItemType='S' then 'Service' when charItemType='A' then 'Accessory' when charItemType='N' then 'Non-Inventory Item' end as ItemType,
isnull(I.numItemClass,0) as numItemClass,
ISNULL(tintStandardProductIDType,0) tintStandardProductIDType,
ISNULL(vcExportToAPI,'') vcExportToAPI,
ISNULL(numShipClass,0) numShipClass,ISNULL(I.bitAllowDropShip,0) AS bitAllowDropShip,
ISNULL(@bitItemIsUsedInOrder,0) AS bitItemIsUsedInOrder,
ISNULL((SELECT COUNT(*) FROM ItemDetails WHERE numChildItemID = I.numItemCode),0) AS intParentKitItemCount,
ISNULL(I.bitArchiveItem,0) AS [bitArchiveItem],
ISNULL(W.numWareHouseID,0) AS [numWareHouseID],
ISNULL(W.numWareHouseItemID,0) AS [numWareHouseItemID],
ISNULL((SELECT COUNT(WI.[numWareHouseItemID]) FROM [dbo].[WareHouseItems] WI WHERE WI.[numItemID] = I.numItemCode AND WI.[numItemID] = @numItemCode),0) AS [intTransCount],
ISNULL(I.bitAsset,0) AS [bitAsset],
ISNULL(I.bitRental,0) AS [bitRental],
ISNULL(W.vcBarCode,ISNULL(I.[numBarCodeId],'')) AS [vcBarCode]
FROM Item I       
left join  WareHouseItems W                  
on W.numItemID=I.numItemCode                
LEFT JOIN ItemExtendedDetails IED   ON I.numItemCode = IED.numItemCode               
WHERE I.numItemCode=@numItemCode  
GROUP BY I.numItemCode, vcItemName, txtItemDesc, charItemType, monListPrice,                   
numItemClassification, bitTaxable,vcSKU,[W].[vcWHSKU] , bitKitParent,numVendorID, I.numDomainID,               
numCreatedBy, bintCreatedDate, bintModifiedDate,                   
numModifiedBy,bitAllowBackOrder, bitSerialized, vcModelID,                   
numItemGroup, numCOGsChartAcntId, numAssetChartAcntId, numIncomeChartAcntId, monAverageCost,                   
monCampaignLabourCost,fltWeight,fltHeight,fltWidth,fltLength,bitFreeShipping,vcUnitofMeasure,bitShowDeptItemDesc,bitShowDeptItem,bitCalAmtBasedonDepItems,bitAssembly,numBarCodeId,I.vcManufacturer
,I.numBaseUnit,I.numPurchaseUnit,I.numSaleUnit,I.bitLotNo,I.IsArchieve,I.numItemClass,I.tintStandardProductIDType,I.vcExportToAPI,I.numShipClass,
CAST( ISNULL(IED.txtDesc,'')  AS VARCHAR(MAX) ),I.bitAllowDropShip,I.bitArchiveItem,I.bitAsset,I.bitRental,W.[numWarehouseID],W.numWareHouseItemID,W.[vcBarCode]


--exec USP_ItemDetails @numItemCode = 822625 ,@ClientTimeZoneOffset =330

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_LandedCostOpportunityUpdate')
DROP PROCEDURE USP_LandedCostOpportunityUpdate
GO
CREATE PROC [dbo].[USP_LandedCostOpportunityUpdate] 
    @numDomainId NUMERIC(18, 0),
    @numOppId NUMERIC(18, 0),
	@vcLanedCost varchar(50)
AS 
	
		DECLARE @monLandedCostTotal money
		SELECT @monLandedCostTotal = SUM([BH].[monAmountDue]) FROM [dbo].[BillHeader] AS BH 
		WHERE [BH].[numDomainID]=@numDomainId AND [BH].[numOppId]=@numOppId AND [BH].[bitLandedCost] = 1

		--Update Total Landed Cost on Opp
		UPDATE [dbo].[OpportunityMaster] SET [monLandedCostTotal]=@monLandedCostTotal,
		[vcLanedCost]=@vcLanedCost
		WHERE [OpportunityMaster].[numDomainId]=@numDomainId AND [OpportunityMaster].[numOppId]=@numOppId

		--Calculate landed cost for each item and Update Landed Cost on Item
		SELECT [OI].[numoppitemtCode],
		CASE @vcLanedCost WHEN 'Amount' THEN [OI].[monTotAmount]
			WHEN 'Cubic Size' THEN ISNULL(I.[fltWidth],0) * ISNULL(I.[fltHeight],0) * ISNULL(I.[fltLength] ,0)
			WHEN 'Weight' THEN ISNULL(I.[fltWeight],0) END AS Total
		INTO #temp
		FROM [dbo].[OpportunityItems] AS OI JOIN [dbo].[Item] AS I ON OI.[numItemCode] = I.[numItemCode] 
		WHERE [OI].[numOppId] = @numOppId

		DECLARE @Total AS DECIMAL(18,2)
		SELECT @Total = SUM(Total) FROM [#temp] AS T

		UPDATE OI SET [monLandedCost] = CAST(ISNULL((@monLandedCostTotal * T.[Total]) / NULLIF(@Total,0),0) AS DECIMAL(18,2)) FROM [dbo].[OpportunityItems] AS OI
		JOIN #temp T ON OI.[numoppitemtCode] = T.[numoppitemtCode] WHERE OI.[numOppId] = @numOppId


		

		--Update Item Average Cost
		UPDATE 
			I 
		SET 
			[monAverageCost] = CASE WHEN ISNULL(TotalOnHand.Qty,0) <= 0
								THEN 
									I.monAverageCost
								ELSE								
									((ISNULL(I.[monAverageCost],0) * Isnull(TotalOnHand.Qty,0)) + ISNULL(OI.[monLandedCost],0) - ISNULL(OI.monAvgLandedCost,0))/NULLIF(Isnull(TotalOnHand.Qty,0),0)
								END
		FROM 
			[dbo].[OpportunityItems] AS OI 
		JOIN 
			[dbo].[Item] AS I 
		OUTER APPLY
		(
			SELECT
				ISNULL((SUM(numOnHand) + SUM(numAllocation)),0) AS Qty
			FROM
				WarehouseItems
			WHERE 
				numItemID = I.numItemCode
		) AS TotalOnHand
		ON 
			OI.[numItemCode] = I.[numItemCode] WHERE OI.[numOppId] = @numOppId

		--Update monAvgLandedCost for tracking Purpose
		UPDATE OI SET monAvgLandedCost = OI.[monLandedCost] FROM [dbo].[OpportunityItems] AS OI WHERE OI.[numOppId] = @numOppId

		DROP TABLE #temp

GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_ManageInventory' ) 
    DROP PROCEDURE usp_ManageInventory
GO
CREATE PROCEDURE [dbo].[usp_ManageInventory]
    @itemcode AS NUMERIC(9) = 0,
    @numWareHouseItemID AS NUMERIC(9) = 0,
    @monAmount AS MONEY,
    @tintOpptype AS TINYINT,
    @numUnits AS NUMERIC,
    @QtyShipped AS NUMERIC,
    @QtyReceived AS NUMERIC,
    @monPrice AS MONEY,
    @tintFlag AS TINYINT,  --1: SO/PO insert/edit 2:SO/PO Close 3:SO/PO Re-Open 4:Sales Fulfillment Release 5:Sales Fulfillment Revert
    @numOppID as numeric(9)=0,
    @numoppitemtCode AS NUMERIC(9)=0,
    @numUserCntID AS NUMERIC(9)=0,
	@bitWorkOrder AS BIT = 0   
AS 
    DECLARE @onHand AS NUMERIC                                    
    DECLARE @onOrder AS NUMERIC 
    DECLARE @onBackOrder AS NUMERIC
    DECLARE @onAllocation AS NUMERIC
	DECLARE @onReOrder AS NUMERIC
    DECLARE @monAvgCost AS MONEY 
    DECLARE @bitKitParent BIT
	DECLARE @numOrigUnits AS NUMERIC		
	DECLARE @description AS VARCHAR(100)
	DECLARE @bitAsset as BIT
	DECLARE @numDomainID AS NUMERIC
	DECLARE @bitReOrderPoint AS BIT = 0
	DECLARE @numReOrderPointOrderStatus AS NUMERIC(18,0) = 0
--    DECLARE @OldOnHand AS NUMERIC    
--    DECLARE @NewAverageCost AS MONEY    
--    DECLARE @OldCost AS MONEY    
--    DECLARE @NewCost AS MONEY    
      
	SELECT @bitAsset=ISNULL(bitAsset,0),@numDomainID=numDomainID from Item where numItemCode=@itemcode--Added By Sachin
	SELECT @bitReOrderPoint=ISNULL(bitReOrderPoint,0),@numReOrderPointOrderStatus=ISNULL(numReOrderPointOrderStatus,0) FROM Domain WHERE numDomainID = @numDomainID
    
	SELECT  @monAvgCost = ISNULL(monAverageCost, 0),
            @bitKitParent = ( CASE WHEN ISNULL(bitKitParent,0) = 1
                               AND ISNULL(bitAssembly,0) = 1 THEN 0
                          WHEN ISNULL(bitKitParent,0) = 1 THEN 1
                          ELSE 0
                     END )
    FROM    Item
    WHERE   numitemcode = @itemcode                                   
    
    IF @numWareHouseItemID>0
    BEGIN                     
		SELECT  @onHand = ISNULL(numOnHand, 0),
				@onAllocation = ISNULL(numAllocation, 0),
				@onOrder = ISNULL(numOnOrder, 0),
				@onBackOrder = ISNULL(numBackOrder, 0),
				@onReOrder = ISNULL(numReorder,0)
		FROM    WareHouseItems
		WHERE   numWareHouseItemID = @numWareHouseItemID 
    END
   
   DECLARE @dtItemReceivedDate AS DATETIME;SET @dtItemReceivedDate=NULL
   
   DECLARE @TotalOnHand AS NUMERIC;SET @TotalOnHand=0  
   SELECT @TotalOnHand=ISNULL((SUM(numOnHand) + SUM(numAllocation)), 0) FROM dbo.WareHouseItems WHERE numItemID=@itemcode
					
   SET @numOrigUnits=@numUnits
            
    IF @tintFlag = 1  --1: SO/PO insert/edit 
        BEGIN                                   
            IF @tintOpptype = 1 
            BEGIN  
			--When change below code also change to USP_ManageWorkOrderStatus  

				/* When QtyShipped has been updated form sales fulfillment then it need to be deducted */
				SET @description='SO insert/edit (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'
				
				IF @bitKitParent = 1 
                BEGIN
                    EXEC USP_UpdatingInventoryForEmbeddedKits @numoppitemtCode,@numOrigUnits, 0,@numOppID,0,@numUserCntID
                END 
				ELSE IF @bitWorkOrder = 1
				BEGIN
					SET @description='SO-WO insert/edit (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'
					-- MANAGE INVENTOY OF ASSEMBLY ITEMS
					EXEC USP_ManageInventoryAssemblyWO @numUserCntID,@numOppID,@itemcode,@numWareHouseItemID,@QtyShipped,1
				END
                ELSE
				BEGIN       
					SET @numUnits = @numUnits - @QtyShipped
                                                        
                    IF @onHand >= @numUnits 
                    BEGIN                                    
                        SET @onHand = @onHand - @numUnits                            
                        SET @onAllocation = @onAllocation + @numUnits                                    
                    END                                    
                    ELSE IF @onHand < @numUnits 
                    BEGIN                                    
                        SET @onAllocation = @onAllocation + @onHand                                    
                        SET @onBackOrder = @onBackOrder + @numUnits
                            - @onHand                                    
                        SET @onHand = 0                                    
                    END    
			                                 

					IF @bitAsset=0--Not Asset
					BEGIN 
						UPDATE  WareHouseItems
						SET     numOnHand = @onHand,
								numAllocation = @onAllocation,
								numBackOrder = @onBackOrder,dtModified = GETDATE() 
						WHERE   numWareHouseItemID = @numWareHouseItemID  


						-- IF REORDER POINT IS ENABLED FROM GLOABAL SETTING THEN CREATE PURCHASE OPPORTUNITY
						If (@onHand + @onOrder <= @onReOrder) AND @bitReOrderPoint = 1 AND @onReOrder > 0
						BEGIN
							BEGIN TRY
								DECLARE @numNewOppID AS NUMERIC(18,0) = 0
								EXEC USP_OpportunityMaster_CreatePO @numNewOppID OUTPUT,@numDomainID,@numUserCntID,@itemcode,@onBackOrder,@numWareHouseItemID,0,0,@numReOrderPointOrderStatus
							END TRY
							BEGIN CATCH
								--WE ARE NOT THROWING ERROR
							END CATCH
						END         
					END
				END  
            END                       
            ELSE IF @tintOpptype = 2 
            BEGIN  
				SET @description='PO insert/edit (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Received:' +  CAST(@QtyReceived AS VARCHAR(10)) + ')'
		            
				/* When QtyReceived has been updated form purchase fulfillment then it need to be deducted */
                SET @numUnits = @numUnits - @QtyReceived   
                                             
                SET @onOrder = @onOrder + @numUnits 
		    
                UPDATE  
					WareHouseItems
                SET     
					numOnHand = @onHand,
                    numAllocation = @onAllocation,
                    numBackOrder = @onBackOrder,
                    numOnOrder = @onOrder,dtModified = GETDATE() 
                WHERE   
					numWareHouseItemID = @numWareHouseItemID   
            END                                                                                                                                         
        END
	 
    ELSE IF @tintFlag = 2 --2:SO/PO Close
            BEGIN
	 	
                PRINT '@OppType=' + CONVERT(VARCHAR(5), @tintOpptype)           
                IF @tintOpptype = 1 
                    BEGIN   
					SET @description='SO Close (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'
                    
                    IF @bitKitParent = 1 
                    BEGIN
                        EXEC USP_UpdatingInventoryForEmbeddedKits @numoppitemtCode,
                            @numOrigUnits, 2,@numOppID,0,@numUserCntID
                    END  
					ELSE IF @bitWorkOrder = 1
					BEGIN
						SET @description='SO-WO Close (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'
						EXEC USP_ManageInventoryAssemblyWO @numUserCntID,@numOppID,@itemcode,@numWareHouseItemID,@QtyShipped,2
					END
                    ELSE
                    BEGIN       
                    
    /* When QtyShipped has been updated form sales fulfillment then it need to be deducted */
							SET @numUnits = @numUnits - @QtyShipped
					  IF @bitAsset=0--Not Asset
							  BEGIN
									SET @onAllocation = @onAllocation - @numUnits 
							  END
					  ELSE--Asset
							  BEGIN
									 SET @onAllocation = @onAllocation  
							  END
											           
		    
							UPDATE  WareHouseItems
							SET     numOnHand = @onHand,
									numAllocation = @onAllocation,
									numBackOrder = @onBackOrder,dtModified = GETDATE() 
							WHERE   numWareHouseItemID = @numWareHouseItemID   
					   END
	
                    END                
                ELSE IF @tintOpptype = 2 
                        BEGIN
					SET @description='PO Close (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Received:' +  CAST(@QtyReceived AS VARCHAR(10)) + ')'
                        
        /* When QtyReceived has been updated form purchase fulfillment then it need to be deducted */
                            SET @numUnits = @numUnits - @QtyReceived
            
							IF EXISTS(SELECT 1 FROM dbo.OpportunityMaster WHERE numOppID=@numOppID AND ISNULL(bitStockTransfer,0)=0)
							BEGIN
								--Updating the Average Cost
								IF @TotalOnHand + @numUnits <= 0
								BEGIN
									  SET @monAvgCost = @monAvgCost --( ( @TotalOnHand * @monAvgCost ) + (@numUnits * @monPrice))
								END
								ELSE
								BEGIN
									  SET @monAvgCost = ( ( @TotalOnHand * @monAvgCost )
												+ (@numUnits * @monPrice))
								/ ( @TotalOnHand + @numUnits )
								END    
		                            
								UPDATE  item
								SET     monAverageCost = @monAvgCost
								WHERE   numItemCode = @itemcode
							END
		    
--                            SELECT  @OldOnHand = SUM(numonhand)
--                            FROM    WareHouseItems WHI
--                            WHERE   WHI.numitemid = @itemcode     
--                            SET @OldCost = @OldOnHand * @monAvgCost     
      
                            IF @onOrder >= @numUnits 
                                BEGIN            
                                    PRINT 'in @onOrder >= @numUnits '
                                    SET @onOrder = @onOrder - @numUnits            
                                    IF @onBackOrder >= @numUnits 
                                        BEGIN            
                                            PRINT 'in @onBackOrder >= @numUnits '
                                            PRINT @numUnits
                                            SET @onBackOrder = @onBackOrder
                                                - @numUnits            
                                            SET @onAllocation = @onAllocation
                                                + @numUnits            
                                        END            
                                    ELSE 
                                        BEGIN            
                                            SET @onAllocation = @onAllocation
                                                + @onBackOrder            
                                            SET @numUnits = @numUnits
                                                - @onBackOrder            
                                            SET @onBackOrder = 0            
                                            SET @onHand = @onHand + @numUnits            
                                        END         
                                END            
                            ELSE IF @onOrder < @numUnits 
                                    BEGIN            
                                        PRINT 'in @onOrder < @numUnits '
                                        SET @onHand = @onHand + @onOrder
                                        SET @onOrder = @numUnits - @onOrder
                                    END         
   -- To Find New Stock Details    
--    Print '@numUnits='+convert(varchar(5),@numUnits)    
--    Print '@monPrice='+Convert(varchar(5),@monPrice)    
--                            SET @NewCost = @numUnits * @monPrice    
    ---Set @NewAverageCost=(@OldCost+@NewCost)/@onHand    
    --Print '@NewAverageCost='+Convert(varchar(5),@NewAverageCost)    
                            UPDATE  WareHouseItems
                            SET     numOnHand = @onHand,
                                    numAllocation = @onAllocation,
                                    numBackOrder = @onBackOrder,
                                    numOnOrder = @onOrder,dtModified = GETDATE() 
                            WHERE   numWareHouseItemID = @numWareHouseItemID  
			                
						SELECT @dtItemReceivedDate=dtItemReceivedDate FROM OpportunityMaster WHERE numOppId=@numOppId
			                
                --ASk Carl for more Info
--                            UPDATE  Item
--                            SET     monAverageCost = @NewAverageCost
--                            WHERE   numItemCode = @itemcode         
                        END
            END

ELSE IF @tintFlag = 3 --3:SO/PO Re-Open
            BEGIN
	 	
                PRINT '@OppType=' + CONVERT(VARCHAR(5), @tintOpptype)           
                IF @tintOpptype = 1 
                    BEGIN         
					SET @description='SO Re-Open (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'
                    
                    IF @bitKitParent = 1 
                        BEGIN
                            EXEC USP_UpdatingInventoryForEmbeddedKits @numoppitemtCode,
                                @numOrigUnits, 3,@numOppID,0,@numUserCntID
                        END 
					ELSE IF @bitWorkOrder = 1
					BEGIN
						SET @description='SO-WO Re-Open (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'
						EXEC USP_ManageInventoryAssemblyWO @numUserCntID,@numOppID,@itemcode,@numWareHouseItemID,@QtyShipped,3
					END
                     ELSE
                     BEGIN
    /* When QtyShipped has been updated form sales fulfillment then it need to be deducted */
                        SET @numUnits = @numUnits - @QtyShipped
  
					 IF @bitAsset=0--Not Asset
							  BEGIN
									SET @onAllocation = @onAllocation + @numUnits
							  END
					  ELSE--Asset
							  BEGIN
									 SET @onAllocation = @onAllocation 
							  END
                                    
	    
                        UPDATE  WareHouseItems
                        SET     numOnHand = @onHand,
                                numAllocation = @onAllocation,
                                numBackOrder = @onBackOrder,dtModified = GETDATE() 
                        WHERE   numWareHouseItemID = @numWareHouseItemID
					  END
						               
                    END                
                ELSE IF @tintOpptype = 2 
                        BEGIN
					SET @description='PO Re-Open (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Received:' +  CAST(@QtyReceived AS VARCHAR(10)) + ')'
                        
        /* When QtyReceived has been updated form purchase fulfillment then it need to be deducted */
                            SET @numUnits = @numUnits - @QtyReceived
            
            
							--Updating the Average Cost
							IF EXISTS(SELECT 1 FROM dbo.OpportunityMaster WHERE numOppID=@numOppID AND ISNULL(bitStockTransfer,0)=0)
							BEGIN
								--IF @onHand + @onOrder - @numUnits <> 0 
								--BEGIN
								IF @TotalOnHand - @numUnits <= 0
								BEGIN
									SET @monAvgCost = 0
	--								SET @monAvgCost = ( ( @TotalOnHand * @monAvgCost )
	--                                                     - (@numUnits * @monPrice))
								END
								ELSE
								BEGIN
									SET @monAvgCost = ( ( @TotalOnHand * @monAvgCost )
														 - (@numUnits * @monPrice))
										/ ( @TotalOnHand - @numUnits )
								END        
								--END
							--ELSE 
								--SET @monAvgCost = 0
                            
                            
								 UPDATE  item
								 SET     monAverageCost = @monAvgCost
								 WHERE   numItemCode = @itemcode
							END

                            IF @onHand >= @numUnits 
                                BEGIN     
                                     SET @onHand = @onHand - @numUnits            
                                     SET @onOrder = @onOrder + @numUnits
                                END         
                          
						   UPDATE  WareHouseItems
                            SET     numOnHand = @onHand,
                                    numOnOrder = @onOrder,dtModified = GETDATE() 
                            WHERE   numWareHouseItemID = @numWareHouseItemID  
                            
                        END
            END
            
            
            
             IF @numWareHouseItemID>0
				 BEGIN
					DECLARE @tintRefType AS TINYINT
					
					IF @tintOpptype = 1 --SO
					  SET @tintRefType=3
					ELSE IF @tintOpptype = 2 --PO
					  SET @tintRefType=4
					  
						DECLARE @numDomain AS NUMERIC(18,0)
						SET @numDomain = (SELECT TOP 1 [numDomainID] FROM WareHouseItems WHERE [WareHouseItems].[numWareHouseItemID] = @numWareHouseItemID)

						EXEC dbo.USP_ManageWareHouseItems_Tracking
						@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
						@numReferenceID = @numOppID, --  numeric(9, 0)
						@tintRefType = @tintRefType, --  tinyint
						@vcDescription = @description, --  varchar(100)
						@numModifiedBy = @numUserCntID,
						@dtRecordDate =  @dtItemReceivedDate,
						@numDomainID = @numDomain
                  END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageInventoryAssemblyWO')
DROP PROCEDURE USP_ManageInventoryAssemblyWO
GO
CREATE PROCEDURE [dbo].[USP_ManageInventoryAssemblyWO]
	@numUserCntID AS NUMERIC(18,0),
	@numOppID AS NUMERIC(9),
	@numItemCode as numeric(9),
	@numWarehouseItemID AS NUMERIC(18,0),
	@QtyShipped AS NUMERIC(18,0),
	@tintMode AS TINYINT=0 --1: Work Order insert/edit 2:Work Order Close 3:Work Order Re-Open 4: Work Order Delete 5: Work Order Edit(Revert Before New Insert)
AS
BEGIN
	/*  
		WHEN WORK ORDER IS CREATED FOR ASSEMBY THEN BOTH SO AND WO PROCESS ARE CARRIED OUT SEPERATELY.
		FOR EXAMPLE: LETS SAY WE HAVE "ASSEMBY ITEM" WHICH REQUIRED 2 QTY OF "ASSEMBLY SUB ITEM" 
		CURRENT INVENTORY STATUS FOR PARENT ITEM: ONHAND:1 ONORDER:0 ONALLOCATION:0 ONBACKORDER:0
		CURRENT INVENTORY STATUS FOR CHILD ITEM: ONHAND:4 ONORDER:0 ONALLOCATION:0 ONBACKORDER:0

		-- WHEN WORK ORDER IS CREATED FOR 1 QTY:
		CURRENT INVENTORY STATUS FOR PARENT ITEM: ONHAND:0 ONORDER:1 ONALLOCATION:1 ONBACKORDER:0
		CURRENT INVENTORY STATUS FOR CHILD ITEM: ONHAND:2 ONORDER:0 ONALLOCATION:2 ONBACKORDER:0

		HERE PARENT ITEM HAVE BOTH SO AND WO IMPACT. SO ONHAND DECREASE AND ONALLOCATION IS INCREASED
		BUT ITS WORK ORDER SO WE HAVE TO INCERASE ONORDER ALSO BY QTY. CHILD ITEM HAVE ONLY WO IMPACT MEANS 
		ONHAND IS DECREASE AND ONALLOCATION IS INCREASED

		-- WHEN ASSEMBLY IS COMPLETED
		CURRENT INVENTORY STATUS FOR PARENT ITEM: ONHAND:1 ONORDER:0 ONALLOCATION:1 ONBACKORDER:0
		CURRENT INVENTORY STATUS FOR CHILD ITEM: ONHAND:2 ONORDER:0 ONALLOCATION:0 ONBACKORDER:0

		-- WHEN WORK ORDER IS CLOSED:
		CURRENT INVENTORY STATUS FOR PARENT ITEM: ONHAND:1 ONORDER:0 ONALLOCATION:0 ONBACKORDER:0
		CURRENT INVENTORY STATUS FOR CHILD ITEM: ONHAND:2 ONORDER:0 ONALLOCATION:0 ONBACKORDER:0
	*/
DECLARE @numWOID AS NUMERIC(18,0)
DECLARE @numParentWOID AS NUMERIC(18,0)
DECLARE	@numDomainID AS NUMERIC(18,0)
DECLARE @vcInstruction AS VARCHAR(1000)
DECLARE @bintCompliationDate DATETIME
DECLARE @numAssemblyQty NUMERIC(18,0) 
DECLARE @numWOStatus AS NUMERIC(18,0)
DECLARE @numWOAssignedTo AS NUMERIC(18,0)

SET XACT_ABORT ON;

BEGIN TRY 
BEGIN TRAN
	SELECT 
		@numWOID=numWOId,@numAssemblyQty=numQtyItemsReq,@numDomainID=numDomainID,
		@numWOStatus=ISNULL(numWOStatus,0),@vcInstruction=vcInstruction,
		@bintCompliationDate= bintCompliationDate, @numWOAssignedTo=numAssignedTo,@numParentWOID=ISNULL(numParentWOID,0)
	FROM 
		WorkOrder 
	WHERE 
		numOppId=@numOppID AND numItemCode=@numItemCode AND numWareHouseItemId=@numWarehouseItemID

	--IF WORK ORDER IS FOUND FOR ITEM AND QTY IS GREATER THEN 0 THEN PROCESS INVENTORY
	IF ISNULL(@numWOID,0) > 0 AND ISNULL(@numAssemblyQty,0) > 0
	BEGIN
		
		DECLARE @TEMPITEM AS TABLE
		(
			RowNo INT NOT NULL identity(1,1),
			numItemCode INT,
			numQty INT,
			numOrgQtyRequired INT,
			numWarehouseItemID INT,
			bitAssembly BIT
		)

		INSERT INTO @TEMPITEM SELECT numChildItemID,numQtyItemsReq,numQtyItemsReq_Orig,numWareHouseItemId,Item.bitAssembly FROM WorkOrderDetails INNER JOIN Item ON WorkOrderDetails.numChildItemID=Item.numItemCode WHERE numWOId = @numWOID AND ISNULL(numWareHouseItemId,0) > 0

		DECLARE @i AS INT = 1
		DECLARE @Count AS INT

		SELECT @Count = COUNT(RowNo) FROM @TEMPITEM

		--IF ASSEMBLY SUB ITEMS ARE AVAILABLE THEN UPDATE INVENTORY OF SUB ITEMS
		IF ISNULL(@Count,0) > 0
		BEGIN
			DECLARE @Description AS VARCHAR(500)
			DECLARE @numChildItemCode AS INT
			DECLARE @numChildItemQty AS INT
			DECLARE @numChildOrgQtyRequired AS INT
			DECLARE @bitChildIsAssembly AS BIT
			DECLARE @numQuantityToBuild AS INT
			DECLARE @numChildItemWarehouseItemID AS INT
			DECLARE @numOnHand AS INT
			DECLARE @numOnOrder AS INT
			DECLARE @numOnAllocation AS INT
			DECLARE @numOnBackOrder AS INT

			--THIS BLOCKS UPDATE INVENTORY LEVEL OF TOP LEVEL PARENT ONLY AND NOT CALLED FOR ITS RECURSIVE CHILD
			IF @numParentWOID = 0
			BEGIN
				--GET ASSEMBY ITEM INVENTORY
				SELECT
					@numOnHand = ISNULL(numOnHand, 0),
					@numOnAllocation = ISNULL(numAllocation, 0),
					@numOnOrder = ISNULL(numOnOrder, 0),
					@numOnBackOrder = ISNULL(numBackOrder, 0)
				FROM
					WareHouseItems
				WHERE
					numWareHouseItemID = @numWarehouseItemID

				IF @tintMode = 1 --WO INSERT
				BEGIN
					-- IF HAVE ENOUGH ITEM QTY IN WAREHOUSE THEN 
					-- DECREASE ONHAND BY QTY AND 
					-- INCREASE ONALLOCATION BY QTY
					-- ONORDER GOES UP BY ASSEMBLY QTY
					IF @numOnHand >= @numAssemblyQty 
					BEGIN      
						SET @numOnHand = @numOnHand - @numAssemblyQty
						SET @numOnOrder = @numOnOrder + @numAssemblyQty
						SET @numOnAllocation = @numOnAllocation + @numAssemblyQty

						UPDATE 
							WareHouseItems
						SET    
							numOnHand = @numOnHand,
							numOnOrder= @numOnOrder,
							numAllocation = @numOnAllocation,
							dtModified = GETDATE() 
						WHERE   
							numWareHouseItemID = @numWareHouseItemID                                                              
					END    
					-- IF DO NOT HAVE ENOUGH ITEM QTY IN WAREHOUSE THEN 
					-- INCREASE ONALLOCATION BY QTY WE HAVE IN WAREHOUSE AND
					-- REMAINING QTY AFTER ALLLOCATING FROM ONHAND WILL GO TO BACKORDER
					-- SET ONHAND TO 0 BECAUSE NOTHING LEFT NOW IN WAREHOUSE FOR ITEM 
					-- ONORDER GOES UP BY ASSEMBLY QTY				 
					ELSE IF @numOnHand < @numAssemblyQty 
					BEGIN      
						SET @numOnAllocation = @numOnAllocation + @numOnHand
						SET @numOnBackOrder = @numOnBackOrder + (@numAssemblyQty - @numOnHand)
						SET @numOnOrder = @numOnOrder + @numAssemblyQty
						SET @numOnHand = 0
 
						UPDATE 
							WareHouseItems
						SET    
							numAllocation = @numOnAllocation,
							numBackOrder = @numOnBackOrder,
							numOnHand = @numOnHand,		
							numOnOrder= @numOnOrder,				
							dtModified = GETDATE() 
						WHERE   
							numWareHouseItemID = @numWareHouseItemID                                     
					END 
				--UPDATE ONORDER QTY OF ASSEMBLY ITEM BY QTY
				END
				ELSE IF @tintMode = 2 --WO CLOSE
				BEGIN
					--REMOVE SHIPPED QTY FROM ORIGIAL QUANTITY
					SET @numAssemblyQty = ISNULL(@numAssemblyQty,0) - ISNULL(@QtyShipped,0)

					--RELASE ALLOCATION
					SET @numOnAllocation = @numOnAllocation - @numAssemblyQty 
											           
					UPDATE  
						WareHouseItems
					SET     
						numAllocation = @numOnAllocation,
						dtModified = GETDATE() 
					WHERE   
						numWareHouseItemID = @numWareHouseItemID   
				END
				ELSE IF @tintMode = 3 --WO REOPEN
				BEGIN
					--REMOVE SHIPPED QTY FROM ORIGIAL QUANTITY
					SET @numAssemblyQty = @numAssemblyQty - @QtyShipped
  
					--ADD ITEM TO ALLOCATION
					SET @numOnAllocation = @numOnAllocation + @numAssemblyQty
                                    
					UPDATE  
						WareHouseItems
					SET     
						numAllocation = @numOnAllocation,
						dtModified = GETDATE() 
					WHERE   
						numWareHouseItemID = @numWareHouseItemID
				END
				ELSE IF (@tintMode = 4 OR  @tintMode=5) --WO DELETE OR EDIT
				BEGIN
					-- CHECK WHETHER ANY QTY IS SHIPPED OR NOT
					IF @QtyShipped > 0
					BEGIN
						IF @tintMode=4
								SET @numAssemblyQty = @numAssemblyQty - @QtyShipped
						ELSE IF @tintmode=5
								SET @numOnAllocation = @numOnAllocation + @QtyShipped 
					END 
	
					-- IF BACKORDER QTY IS GREATER THEN QTY TO REVERT THEN
					-- DECREASE BACKORDER QTY BY QTY TO REVERT
					IF @numAssemblyQty < @numOnBackOrder 
					BEGIN                  
						SET @numOnBackOrder = @numOnBackOrder - @numAssemblyQty
					END 
					-- IF BACKORDER QTY IS LESS THEN OR EQUAL TO QTY TO REVERT THEN
					-- DECREASE QTY TO REVERT BY QTY OF BACK ORDER AND
					-- SET BACKORDER QTY TO 0
					ELSE IF @numAssemblyQty >= @numOnBackOrder 
					BEGIN
						SET @numAssemblyQty = @numAssemblyQty - @numOnBackOrder
						SET @numOnBackOrder = 0
                        
						--REMOVE ITEM FROM ALLOCATION 
						IF (@numOnAllocation - @numAssemblyQty) >= 0
							SET @numOnAllocation = @numOnAllocation - @numAssemblyQty
						
						--ADD QTY TO ONHAND
						SET @numOnHand = @numOnHand + @numAssemblyQty
					END

					--THIS IF BLOCK SHOULD BE LAST OTHERWISE IT WILL CREATE PROBLEM IN INVENTORY LEVEL
					--IF WORK ORDER IS NOT COMPLETED THEN REVERT 
					--QTY FROM ONORDER BECAUSE WE HAVE TO REVERT
					IF @numWOStatus <> 23184
					BEGIN
						--DECREASE ONORDER BY ASSEMBLY QTY
						SET @numOnOrder = @numOnOrder - @numAssemblyQty
					END
					ELSE
					BEGIN
						--DECREASE ONHAND BY ASSEMBLY QTY
						--BECAUSE WHEN WORK ORDER IS COMPLETED ONHAND QTY OF PARENT IS INCREASED
						SET @numOnHand = @numOnHand - @numAssemblyQty
					END

					UPDATE 
						WareHouseItems
					SET    
						numOnHand = @numOnHand,
						numAllocation = @numOnAllocation,
						numBackOrder = @numOnBackOrder,
						numOnOrder = @numOnOrder,
						dtModified = GETDATE() 
					WHERE   
						numWareHouseItemID = @numWareHouseItemID				
				END
			END

			--CHILD ITEM INVENTORY IS CHANGED ONLY WHEN SALES ORDER WITH WORK ORDER IS INSERTED OR DELETED
			--THE INVENTORY MANGMENT FOR DELETE ACTION IS HANDLED FROM STORE PROCEDURE USP_DeleteAssemblyWOAndInventory
			IF @tintMode = 1
			BEGIN
				
				WHILE @i <= @Count
				BEGIN
					DECLARE @numNewOppID AS NUMERIC(18,0) = 0
					DECLARE @QtyToBuild AS NUMERIC(18,0)
					DECLARE @numQtyShipped AS NUMERIC(18,0)

					--GET CHILD ITEM DETAIL FROM TEMP TABLE
					SELECT @numChildItemCode=numItemCode,@numChildItemWarehouseItemID=numWarehouseItemID,@numChildItemQty=numQty,@numChildOrgQtyRequired=numOrgQtyRequired,@bitChildIsAssembly=bitAssembly FROM @TEMPITEM WHERE RowNo = @i
					
					--GET CHILD ITEM CURRENT INVENTORY DETAIL
					SELECT  
						@numOnHand = ISNULL(numOnHand, 0),
						@numOnAllocation = ISNULL(numAllocation, 0),
						@numOnOrder = ISNULL(numOnOrder, 0),
						@numOnBackOrder = ISNULL(numBackOrder, 0)
					FROM    
						WareHouseItems
					WHERE   
						numWareHouseItemID = @numChildItemWarehouseItemID
					
					IF ISNULL(@numParentWOID,0) = 0
					BEGIN
						SET @Description='Items Allocated For SO-WO (Qty:' + CAST(@numChildItemQty AS VARCHAR(10)) + ' Shipped:' +  CAST((@QtyShipped * @numChildOrgQtyRequired) AS VARCHAR(10)) + ')'
					END
					ELSE
					BEGIN
						SET @Description='Items Allocated For SO-WO Work Order (Qty:' + CAST(@numChildItemQty AS VARCHAR(10)) + ' Shipped:' +  CAST((@QtyShipped * @numChildOrgQtyRequired) AS VARCHAR(10)) + ')'
					END
					
					SET @numChildItemQty = @numChildItemQty - (@QtyShipped * @numChildOrgQtyRequired)
						
					--IF CHILD ITEM IS ASSEMBLY THEN SAME LOGIC LIKE PARENT IS APPLIED
					IF ISNULL(@bitChildIsAssembly,0) = 1
					BEGIN
						-- IF HAVE ENOUGH ITEM QTY IN WAREHOUSE THEN 
						-- DECREASE ONHAND BY QTY AND 
						-- INCREASE ONALLOCATION BY QTY
						-- ONORDER GOES UP BY ASSEMBLY QTY
						IF @numOnHand >= @numChildItemQty 
						BEGIN      
							SET @numOnHand = @numOnHand - @numChildItemQty
							SET @numOnAllocation = @numOnAllocation + @numChildItemQty  
							
							--DONOT PLACE THIS LOGIC AT BOTTOM BECAUSE WORK ORDER ARE CREATED RECURSIVELY WHICH CREATE PROBLEM IN WAREHOUSE TRAKING ENTRIES
							--UPDATE WAREHOUSE INVENTORY AND WAREHOUSE HISTORY
							EXEC USP_UpdateInventoryAndTracking 
							@numOnHand=@numOnHand,
							@numOnAllocation=@numOnAllocation,
							@numOnBackOrder=@numOnBackOrder,
							@numOnOrder=@numOnOrder,
							@numWarehouseItemID=@numChildItemWarehouseItemID,
							@numReferenceID=@numOppID,
							@tintRefType=3,
							@numDomainID=@numDomainID,
							@numUserCntID=@numUserCntID,
							@Description=@Description                                                         
						END    
						-- IF DO NOT HAVE ENOUGH ITEM QTY IN WAREHOUSE THEN 
						-- INCREASE ONALLOCATION BY QTY WE HAVE IN WAREHOUSE AND
						-- REMAINING QTY AFTER ALLLOCATING FROM ONHAND WILL GO TO BACKORDER
						-- SET ONHAND TO 0 BECAUSE NOTHING LEFT NOW IN WAREHOUSE FOR ITEM 
						-- ONORDER GOES UP BY ASSEMBLY QTY				 
						ELSE IF @numOnHand < @numChildItemQty 
						BEGIN      
							SET @numOnAllocation = @numOnAllocation + @numOnHand
							SET @numOnBackOrder = @numOnBackOrder + (@numChildItemQty - @numOnHand)
							SET @QtyToBuild = (@numChildItemQty - @numOnHand)
							SET @numQtyShipped = (@QtyShipped * @numChildOrgQtyRequired)
							SET @numOnHand = 0   

							--DONOT PLACE THIS LOGIC AT BOTTOM BECAUSE WORK ORDER ARE CREATED RECURSIVELY WHICH CREATE PROBLEM IN WAREHOUSE TRAKING ENTRIES
							--UPDATE WAREHOUSE INVENTORY AND WAREHOUSE HISTORY
							EXEC USP_UpdateInventoryAndTracking 
							@numOnHand=@numOnHand,
							@numOnAllocation=@numOnAllocation,
							@numOnBackOrder=@numOnBackOrder,
							@numOnOrder=@numOnOrder,
							@numWarehouseItemID=@numChildItemWarehouseItemID,
							@numReferenceID=@numOppID,
							@tintRefType=3,
							@numDomainID=@numDomainID,
							@numUserCntID=@numUserCntID,
							@Description=@Description
								
							EXEC USP_WorkOrder_InsertRecursive @numOppID,@numChildItemCode,@QtyToBuild,@numChildItemWarehouseItemID,@numDomainID,@numUserCntID,@numQtyShipped,@numWOID,0
						END 
					END
					ELSE
					BEGIN
						-- IF HAVE ENOUGH ITEM QTY IN WAREHOUSE THEN 
						-- DECREASE ONHAND BY QTY AND 
						-- INCREASE ONALLOCATION BY QTY
						IF @numOnHand >= @numChildItemQty 
						BEGIN                                    
							SET @numOnHand = @numOnHand - @numChildItemQty                            
							SET @numOnAllocation = @numOnAllocation + @numChildItemQty 
							
							--DONOT PLACE THIS LOGIC AT BOTTOM BECAUSE WORK ORDER ARE CREATED RECURSIVELY WHICH CREATE PROBLEM IN WAREHOUSE TRAKING ENTRIES
							--UPDATE WAREHOUSE INVENTORY AND WAREHOUSE HISTORY
							EXEC USP_UpdateInventoryAndTracking 
							@numOnHand=@numOnHand,
							@numOnAllocation=@numOnAllocation,
							@numOnBackOrder=@numOnBackOrder,
							@numOnOrder=@numOnOrder,
							@numWarehouseItemID=@numChildItemWarehouseItemID,
							@numReferenceID=@numOppID,
							@tintRefType=3,
							@numDomainID=@numDomainID,
							@numUserCntID=@numUserCntID,
							@Description=@Description                                   
						END    
						-- IF DO NOT HAVE ENOUGH ITEM QTY IN WAREHOUSE THEN 
						-- INCREASE ONALLOCATION BY QTY WE HAVE IN WAREHOUSE AND
						-- REMAINING QTY AFTER ALLLOCATING FROM ONHAND WILL GO TO BACKORDER
						-- SET ONHAND TO 0 BECAUSE NOTHING LEFT NOW IN WAREHOUSE FOR ITEM 				 
						ELSE IF @numOnHand < @numChildItemQty 
						BEGIN     
								
							SET @numOnAllocation = @numOnAllocation + @numOnHand  
							SET @numOnBackOrder = @numOnBackOrder + (@numChildItemQty - @numOnHand) 
							SET @QtyToBuild = (@numChildItemQty - @numOnHand) 
							SET @numOnHand = 0 

							--DONOT PLACE THIS LOGIC AT BOTTOM BECAUSE WORK ORDER ARE CREATED RECURSIVELY WHICH CREATE PROBLEM IN WAREHOUSE TRAKING ENTRIES
							--UPDATE WAREHOUSE INVENTORY AND WAREHOUSE HISTORY
							EXEC USP_UpdateInventoryAndTracking 
							@numOnHand=@numOnHand,
							@numOnAllocation=@numOnAllocation,
							@numOnBackOrder=@numOnBackOrder,
							@numOnOrder=@numOnOrder,
							@numWarehouseItemID=@numChildItemWarehouseItemID,
							@numReferenceID=@numOppID,
							@tintRefType=3,
							@numDomainID=@numDomainID,
							@numUserCntID=@numUserCntID,
							@Description=@Description

							--CREATE PURCHASE ORDER FOR QUANTITY WHICH GOES ON BACKORDER
							EXEC USP_OpportunityMaster_CreatePO @numNewOppID OUTPUT,@numDomainID,@numUserCntID,@numChildItemCode,@QtyToBuild,@numChildItemWarehouseItemID,1,1,0
								
							UPDATE WorkOrderDetails SET numPOID= @numNewOppID WHERE numWOId=@numWOID AND numChildItemID=@numChildItemCode AND numWareHouseItemId=@numChildItemWarehouseItemID                                 
						END 
					END
 
					
					SET @i = @i + 1
				END
			END
		END
	END
COMMIT TRAN 
END TRY 
BEGIN CATCH		
    IF ( @@TRANCOUNT > 0 ) 
    BEGIN
        ROLLBACK TRAN
    END

	DECLARE @ErrorMessage NVARCHAR(4000);
    DECLARE @ErrorSeverity INT;
    DECLARE @ErrorState INT;

    SELECT @ErrorMessage = ERROR_MESSAGE(), @ErrorSeverity = ERROR_SEVERITY(), @ErrorState = ERROR_STATE();

    RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);
END CATCH	

SET XACT_ABORT OFF;
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageInventoryWorkOrder')
DROP PROCEDURE USP_ManageInventoryWorkOrder
GO
CREATE PROCEDURE [dbo].[USP_ManageInventoryWorkOrder]
@numWOId AS NUMERIC(18,0),
@numDomainID AS NUMERIC(18,0),
@numUserCntID AS NUMERIC(18,0)
AS
BEGIN
BEGIN TRY 
BEGIN TRAN
		
	DECLARE @TEMPITEM AS TABLE
	(
		RowNo INT NOT NULL identity(1,1),
		numItemCode INT,
		numQty INT,
		numOrgQtyRequired INT,
		numWarehouseItemID INT,
		bitAssembly BIT
	)

	INSERT INTO @TEMPITEM SELECT numChildItemID,numQtyItemsReq,numQtyItemsReq_Orig,numWareHouseItemId,Item.bitAssembly FROM WorkOrderDetails INNER JOIN Item ON WorkOrderDetails.numChildItemID=Item.numItemCode WHERE numWOId = @numWOID AND ISNULL(numWareHouseItemId,0) > 0

	DECLARE @i AS INT = 1
	DECLARE @Count AS INT

	SELECT @Count = COUNT(RowNo) FROM @TEMPITEM

	--IF ASSEMBLY SUB ITEMS ARE AVAILABLE THEN UPDATE INVENTORY OF SUB ITEMS
	IF ISNULL(@Count,0) > 0
	BEGIN
		DECLARE @Description AS VARCHAR(1000)
		DECLARE @numChildItemCode AS INT
		DECLARE @numChildItemQty AS INT
		DECLARE @numChildOrgQtyRequired AS INT
		DECLARE @bitChildIsAssembly AS BIT
		DECLARE @numQuantityToBuild AS INT
		DECLARE @numChildItemWarehouseItemID AS INT
		DECLARE @numOnHand AS INT
		DECLARE @numOnOrder AS INT
		DECLARE @numOnAllocation AS INT
		DECLARE @numOnBackOrder AS INT

		WHILE @i <= @Count
		BEGIN
			DECLARE @numNewOppID AS NUMERIC(18,0) = 0
			DECLARE @QtyToBuild AS NUMERIC(18,0)
			DECLARE @numQtyShipped AS NUMERIC(18,0)

			--GET CHILD ITEM DETAIL FROM TEMP TABLE
			SELECT @numChildItemCode=numItemCode,@numChildItemWarehouseItemID=numWarehouseItemID,@numChildItemQty=numQty,@numChildOrgQtyRequired=numOrgQtyRequired,@bitChildIsAssembly=bitAssembly FROM @TEMPITEM WHERE RowNo = @i
					
			--GET CHILD ITEM CURRENT INVENTORY DETAIL
			SELECT  
				@numOnHand = ISNULL(numOnHand, 0),
				@numOnAllocation = ISNULL(numAllocation, 0),
				@numOnOrder = ISNULL(numOnOrder, 0),
				@numOnBackOrder = ISNULL(numBackOrder, 0)
			FROM    
				WareHouseItems
			WHERE   
				numWareHouseItemID = @numChildItemWarehouseItemID	

			SET @Description='Items Allocated For Work Order (Qty:' + CAST(@numChildItemQty AS VARCHAR(10)) + ')'
						
			--IF CHILD ITEM IS ASSEMBLY THEN SAME LOGIC LIKE PARENT IS APPLIED
			IF ISNULL(@bitChildIsAssembly,0) = 1
			BEGIN
				-- IF HAVE ENOUGH ITEM QTY IN WAREHOUSE THEN 
				-- DECREASE ONHAND BY QTY AND 
				-- INCREASE ONALLOCATION BY QTY
				-- ONORDER GOES UP BY ASSEMBLY QTY
				IF @numOnHand >= @numChildItemQty 
				BEGIN      
					SET @numOnHand = @numOnHand - @numChildItemQty
					SET @numOnAllocation = @numOnAllocation + @numChildItemQty  
					
					--DONOT PLACE THIS LOGIC AT BOTTOM BECAUSE WORK ORDER ARE CREATED RECURSIVELY WHICH CREATE PROBLEM IN WAREHOUSE TRAKING ENTRIES
					--UPDATE WAREHOUSE INVENTORY AND WAREHOUSE HISTORY
					EXEC USP_UpdateInventoryAndTracking 
					@numOnHand=@numOnHand,
					@numOnAllocation=@numOnAllocation,
					@numOnBackOrder=@numOnBackOrder,
					@numOnOrder=@numOnOrder,
					@numWarehouseItemID=@numChildItemWarehouseItemID,
					@numReferenceID=@numWOId,
					@tintRefType=2,
					@numDomainID=@numDomainID,
					@numUserCntID=@numUserCntID,
					@Description=@Description                                                         
				END    
				-- IF DO NOT HAVE ENOUGH ITEM QTY IN WAREHOUSE THEN 
				-- INCREASE ONALLOCATION BY QTY WE HAVE IN WAREHOUSE AND
				-- REMAINING QTY AFTER ALLLOCATING FROM ONHAND WILL GO TO BACKORDER
				-- SET ONHAND TO 0 BECAUSE NOTHING LEFT NOW IN WAREHOUSE FOR ITEM 
				-- ONORDER GOES UP BY ASSEMBLY QTY				 
				ELSE IF @numOnHand < @numChildItemQty 
				BEGIN      
					SET @numOnAllocation = @numOnAllocation + @numOnHand
					SET @numOnBackOrder = @numOnBackOrder + (@numChildItemQty - @numOnHand)
					SET @QtyToBuild = (@numChildItemQty - @numOnHand)
					SET @numQtyShipped = 0
					SET @numOnHand = 0   
					
					--DONOT PLACE THIS LOGIC AT BOTTOM BECAUSE WORK ORDER ARE CREATED RECURSIVELY WHICH CREATE PROBLEM IN WAREHOUSE TRAKING ENTRIES
					--UPDATE WAREHOUSE INVENTORY AND WAREHOUSE HISTORY	
					EXEC USP_UpdateInventoryAndTracking 
					@numOnHand=@numOnHand,
					@numOnAllocation=@numOnAllocation,
					@numOnBackOrder=@numOnBackOrder,
					@numOnOrder=@numOnOrder,
					@numWarehouseItemID=@numChildItemWarehouseItemID,
					@numReferenceID=@numWOId,
					@tintRefType=2,
					@numDomainID=@numDomainID,
					@numUserCntID=@numUserCntID,
					@Description=@Description 

					EXEC USP_WorkOrder_InsertRecursive 0,@numChildItemCode,@QtyToBuild,@numChildItemWarehouseItemID,@numDomainID,@numUserCntID,@numQtyShipped,@numWOID,1
				END 
			END
			ELSE
			BEGIN
				-- IF HAVE ENOUGH ITEM QTY IN WAREHOUSE THEN 
				-- DECREASE ONHAND BY QTY AND 
				-- INCREASE ONALLOCATION BY QTY
				IF @numOnHand >= @numChildItemQty 
				BEGIN                                    
					SET @numOnHand = @numOnHand - @numChildItemQty                            
					SET @numOnAllocation = @numOnAllocation + @numChildItemQty        
					
					--DONOT PLACE THIS LOGIC AT BOTTOM BECAUSE WORK ORDER ARE CREATED RECURSIVELY WHICH CREATE PROBLEM IN WAREHOUSE TRAKING ENTRIES
					--UPDATE WAREHOUSE INVENTORY AND WAREHOUSE HISTORY
					EXEC USP_UpdateInventoryAndTracking 
					@numOnHand=@numOnHand,
					@numOnAllocation=@numOnAllocation,
					@numOnBackOrder=@numOnBackOrder,
					@numOnOrder=@numOnOrder,
					@numWarehouseItemID=@numChildItemWarehouseItemID,
					@numReferenceID=@numWOId,
					@tintRefType=2,
					@numDomainID=@numDomainID,
					@numUserCntID=@numUserCntID,
					@Description=@Description                             
				END    
				-- IF DO NOT HAVE ENOUGH ITEM QTY IN WAREHOUSE THEN 
				-- INCREASE ONALLOCATION BY QTY WE HAVE IN WAREHOUSE AND
				-- REMAINING QTY AFTER ALLLOCATING FROM ONHAND WILL GO TO BACKORDER
				-- SET ONHAND TO 0 BECAUSE NOTHING LEFT NOW IN WAREHOUSE FOR ITEM 				 
				ELSE IF @numOnHand < @numChildItemQty 
				BEGIN     
								
					SET @numOnAllocation = @numOnAllocation + @numOnHand  
					SET @numOnBackOrder = @numOnBackOrder + (@numChildItemQty - @numOnHand) 
					SET @QtyToBuild = (@numChildItemQty - @numOnHand) 
					SET @numOnHand = 0 

					--DONOT PLACE THIS LOGIC AT BOTTOM BECAUSE WORK ORDER ARE CREATED RECURSIVELY WHICH CREATE PROBLEM IN WAREHOUSE TRAKING ENTRIES
					--UPDATE WAREHOUSE INVENTORY AND WAREHOUSE HISTORY
					EXEC USP_UpdateInventoryAndTracking 
					@numOnHand=@numOnHand,
					@numOnAllocation=@numOnAllocation,
					@numOnBackOrder=@numOnBackOrder,
					@numOnOrder=@numOnOrder,
					@numWarehouseItemID=@numChildItemWarehouseItemID,
					@numReferenceID=@numWOId,
					@tintRefType=2,
					@numDomainID=@numDomainID,
					@numUserCntID=@numUserCntID,
					@Description=@Description 

					--CREATE PURCHASE ORDER FOR QUANTITY WHICH GOES ON BACKORDER
					EXEC USP_OpportunityMaster_CreatePO @numNewOppID OUTPUT,@numDomainID,@numUserCntID,@numChildItemCode,@QtyToBuild,@numChildItemWarehouseItemID,1,1,0
								
					UPDATE WorkOrderDetails SET numPOID= @numNewOppID WHERE numWOId=@numWOID AND numChildItemID=@numChildItemCode AND numWareHouseItemId=@numChildItemWarehouseItemID                                 
				END 
			END
					
			SET @i = @i + 1
		END
	END
COMMIT TRAN 
END TRY 
BEGIN CATCH		
    IF ( @@TRANCOUNT > 0 ) 
    BEGIN
        ROLLBACK TRAN
    END

	DECLARE @ErrorMessage NVARCHAR(4000);
    DECLARE @ErrorSeverity INT;
    DECLARE @ErrorState INT;

    SELECT @ErrorMessage = ERROR_MESSAGE(), @ErrorSeverity = ERROR_SEVERITY(), @ErrorState = ERROR_STATE();

    RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);
END CATCH	
END
GO
/****** Object:  StoredProcedure [dbo].[USP_ManageItemsAndKits]    Script Date: 02/15/2010 21:44:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Anoop Jayaraj                                                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_manageitemsandkits')
DROP PROCEDURE usp_manageitemsandkits
GO
CREATE PROCEDURE [dbo].[USP_ManageItemsAndKits]                                                                
@numItemCode as numeric(9) output,                                                                
@vcItemName as varchar(300),                                                                
@txtItemDesc as varchar(1000),                                                                
@charItemType as char(1),                                                                
@monListPrice as money,                                                                
@numItemClassification as numeric(9),                                                                
@bitTaxable as bit,                                                                
@vcSKU as varchar(50),                                                                                                                                      
@bitKitParent as bit,                                                                                   
--@dtDateEntered as datetime,                                                                
@numDomainID as numeric(9),                                                                
@numUserCntID as numeric(9),                                                                                                  
@numVendorID as numeric(9),                                                            
@bitSerialized as bit,                                               
@strFieldList as text,                                            
@vcModelID as varchar(200)='' ,                                            
@numItemGroup as numeric(9)=0,                                      
@numCOGSChartAcntId as numeric(9)=0,                                      
@numAssetChartAcntId as numeric(9)=0,                                      
@numIncomeChartAcntId as numeric(9)=0,                            
@monAverageCost as money,                          
@monLabourCost as money,                  
@fltWeight as float,                  
@fltHeight as float,                  
@fltLength as float,                  
@fltWidth as float,                  
@bitFreeshipping as bit,                
@bitAllowBackOrder as bit,              
@UnitofMeasure as varchar(30)='',              
@strChildItems as text,            
@bitShowDeptItem as bit,              
@bitShowDeptItemDesc as bit,        
@bitCalAmtBasedonDepItems as bit,    
@bitAssembly as BIT,
@intWebApiId INT=null,
@vcApiItemId AS VARCHAR(50)=null,
@numBarCodeId as VARCHAR(25)=null,
@vcManufacturer as varchar(250)=NULL,
@numBaseUnit AS NUMERIC(18)=0,
@numPurchaseUnit AS NUMERIC(18)=0,
@numSaleUnit AS NUMERIC(18)=0,
@bitLotNo as BIT,
@IsArchieve AS BIT,
@numItemClass as numeric(9)=0,
@tintStandardProductIDType AS TINYINT=0,
@vcExportToAPI VARCHAR(50),
@numShipClass NUMERIC,
@ProcedureCallFlag SMALLINT =0,
@vcCategories AS VARCHAR(2000) = '',
@bitAllowDropShip AS BIT=0,
@bitArchiveItem AS BIT = 0,
@bitAsset AS BIT = 0,
@bitRental AS BIT = 0
as                                                                
declare @hDoc as INT

if @numCOGSChartAcntId=0 set @numCOGSChartAcntId=null                      
if  @numAssetChartAcntId=0 set @numAssetChartAcntId=null  
if  @numIncomeChartAcntId=0 set @numIncomeChartAcntId=null     

DECLARE @ParentSKU VARCHAR(50)                                        
DECLARE @ItemID AS NUMERIC(9)
DECLARE @cnt AS INT

--if @dtDateEntered = 'Jan  1 1753 12:00:00:000AM' set @dtDateEntered=null 

IF ISNULL(@charItemType,'') = '' OR ISNULL(@charItemType,'') = '0'  SET @charItemType = 'N'

IF @intWebApiId = 2 
    AND LEN(ISNULL(@vcApiItemId, '')) > 0
    AND ISNULL(@numItemGroup, 0) > 0 
    BEGIN      
    
        -- check wether this id already Mapped in ITemAPI 
        SELECT  @cnt = COUNT([numItemID])
        FROM    [ItemAPI]
        WHERE   [WebApiId] = @intWebApiId
                AND [numDomainId] = @numDomainID
                AND [vcAPIItemID] = @vcApiItemId
        IF @cnt > 0 
            BEGIN
                SELECT  @ItemID = [numItemID]
                FROM    [ItemAPI]
                WHERE   [WebApiId] = @intWebApiId
                        AND [numDomainId] = @numDomainID
                        AND [vcAPIItemID] = @vcApiItemId
        --Update Existing Product coming from API
                SET @numItemCode = @ItemID
            END
    
    END

ELSE 
    IF @intWebApiId > 1 --AND  @intWebApiId <> 2
        AND LEN(ISNULL(@vcSKU, '')) > 0 
        BEGIN
            SET @ParentSKU = @vcSKU
            --DECLARE @ItemID AS NUMERIC(9)
            --DECLARE @cnt AS INT
    
       -- check wether this id already exist in Domain
       
            SELECT  @cnt = COUNT([numItemCode])
            FROM    [Item]
            WHERE   [numDomainId] = @numDomainID
                    AND ( vcSKU = @vcSKU
                          OR @vcSKU IN ( SELECT vcWHSKU
                                         FROM   dbo.WareHouseItems
                                         WHERE  numItemID = Item.[numItemCode]
                                                AND vcWHSKU = @vcSKU )
                        )
            IF @cnt > 0 
                BEGIN
                    SELECT  @ItemID = [numItemCode],
                            @ParentSKU = vcSKU
                    FROM    [Item]
                    WHERE   [numDomainId] = @numDomainID
                            AND ( vcSKU = @vcSKU
                                  OR @vcSKU IN (
                                  SELECT    vcWHSKU
                                  FROM      dbo.WareHouseItems
                                  WHERE     numItemID = Item.[numItemCode]
                                            AND vcWHSKU = @vcSKU )
                                )
                    SET @numItemCode = @ItemID
                END
            ELSE 
                BEGIN
			
    -- check wether this id already Mapped in ITemAPI 
                    SELECT  @cnt = COUNT([numItemID])
                    FROM    [ItemAPI]
                    WHERE   [WebApiId] = @intWebApiId
                            AND [numDomainId] = @numDomainID
                            AND [vcAPIItemID] = @vcApiItemId
                    IF @cnt > 0 
                        BEGIN
                            SELECT  @ItemID = [numItemID]
                            FROM    [ItemAPI]
                            WHERE   [WebApiId] = @intWebApiId
                                    AND [numDomainId] = @numDomainID
                                    AND [vcAPIItemID] = @vcApiItemId
        --Update Existing Product coming from API
                            SET @numItemCode = @ItemID
                        END
     
                END
        END
                                                         
if @numItemCode=0 or  @numItemCode is null
begin                                                                 
 insert into Item (                                             
  vcItemName,                                             
  txtItemDesc,                                             
  charItemType,                                             
  monListPrice,                                             
  numItemClassification,                                             
  bitTaxable,                                             
  vcSKU,                                             
  bitKitParent,                                             
  --dtDateEntered,                                              
  numVendorID,                                             
  numDomainID,                                             
  numCreatedBy,                                             
  bintCreatedDate,                                             
  bintModifiedDate,                                             
 numModifiedBy,                                              
  bitSerialized,                                     
  vcModelID,                                            
  numItemGroup,                                          
  numCOGsChartAcntId,                        
  numAssetChartAcntId,                                      
  numIncomeChartAcntId,                            
  monAverageCost,                          
  monCampaignLabourCost,                  
  fltWeight,                  
  fltHeight,                  
  fltWidth,                  
  fltLength,                  
  bitFreeShipping,                
  bitAllowBackOrder,              
  vcUnitofMeasure,            
  bitShowDeptItem,            
  bitShowDeptItemDesc,        
  bitCalAmtBasedonDepItems,    
  bitAssembly,
  numBarCodeId,
  vcManufacturer,numBaseUnit,numPurchaseUnit,numSaleUnit,bitLotNo,IsArchieve,numItemClass,tintStandardProductIDType,vcExportToAPI,numShipClass,
  bitAllowDropShip,bitArchiveItem,
  bitAsset,bitRental
   )                                                              
   values                                                                
    (                                                                
  @vcItemName,                                             
  @txtItemDesc,                                  
  @charItemType,                                             
  @monListPrice,                                             
  @numItemClassification,                                             
  @bitTaxable,                                             
  @ParentSKU,                       
  @bitKitParent,                                             
  --@dtDateEntered,                                             
  @numVendorID,                                             
  @numDomainID,                                             
  @numUserCntID,                                           
  getutcdate(),                                             
  getutcdate(),                                             
  @numUserCntID,                                             
  @bitSerialized,                                       
  @vcModelID,                                            
  @numItemGroup,                                      
  @numCOGsChartAcntId,                                      
  @numAssetChartAcntId,                                      
  @numIncomeChartAcntId,                            
  @monAverageCost,                          
  @monLabourCost,                  
  @fltWeight,                  
  @fltHeight,                  
  @fltWidth,                  
  @fltLength,                  
  @bitFreeshipping,                
  @bitAllowBackOrder,              
  @UnitofMeasure,            
  @bitShowDeptItem,            
  @bitShowDeptItemDesc,        
  @bitCalAmtBasedonDepItems,    
  @bitAssembly,                                                          
   @numBarCodeId,
@vcManufacturer,@numBaseUnit,@numPurchaseUnit,@numSaleUnit,@bitLotNo,@IsArchieve,@numItemClass,@tintStandardProductIDType,@vcExportToAPI,@numShipClass,
@bitAllowDropShip,@bitArchiveItem ,@bitAsset,@bitRental)                                             
 
 set @numItemCode = SCOPE_IDENTITY()
 
 --insert warehouse for inventory item
-- EXEC sp_xml_preparedocument @hDoc OUTPUT, @strFieldList      
-- insert into WareHouseItems (numItemID, numWareHouseID,numOnHand,monWListPrice,vcLocation,vcWHSKU,vcBarCode,numDomainID,numReorder)  
--	SELECT @numItemCode,numWareHouseID,OnHand,Price,Location,SKU,BarCode,@numDomainID,Reorder
--	FROM OPENXML(@hDoc,'/NewDataSet/WareHouse[Op_Flag=1]',2)                                                    
--    with(numWareHouseItemID numeric(9),                                                                        
--    numWareHouseID numeric(9),                    
--    Reorder numeric(9),                                                                       
--    OnHand numeric(9),                                            
--    Op_Flag tinyint,      
--    Price money,
--    Location Varchar(100),
--    SKU varchar(100),
--    BarCode varchar(100))
--    
--update WareHouseItems  set                                                                         
--   numItemID=@numItemCode,                                                                       
--   numWareHouseID=X.numWareHouseID,                                                              
--   numOnHand=X.OnHand,                     
--   numReorder=X.Reorder,      
--   monWListPrice=X.Price,
--   vcLocation=X.Location,
--   vcWHSKU=X.SKU,
--	vcBarCode=X.BarCode                              
--    From (SELECT numWareHouseItemID as WareHouseItemID,numWareHouseID,Reorder,OnHand,Op_Flag,Price,Location,SKU,BarCode                                                                        
--   FROM OPENXML(@hDoc,'/NewDataSet/WareHouse[Op_Flag=2]',2)                                                                         
--    with(numWareHouseItemID numeric(9),                                                                        
--    numWareHouseID numeric(9),                    
--    Reorder numeric(9),                                                                       
--    OnHand numeric(9),                                            
--    Op_Flag tinyint,      
--    Price money,
--    Location Varchar(100),
--    SKU varchar(100),
--    BarCode varchar(100)))X                                                                         
--  where  numWareHouseItemID=X.WareHouseItemID 
--    insert into ItemUOM (numItemCode, numUOMId,numDomainId)  
--	SELECT @numItemCode,numUOMId,@numDomainID
--	FROM OPENXML(@hDoc,'/NewDataSet/TableUOM',2)                                                    
--    with(numUOMId numeric(9))
  
 IF  @intWebApiId > 1 
   BEGIN
     -- insert new product
     --insert this id into linking table
     INSERT INTO [ItemAPI] (
	 	[WebApiId],
	 	[numDomainId],
	 	[numItemID],
	 	[vcAPIItemID],
	 	[numCreatedby],
	 	[dtCreated],
	 	[numModifiedby],
	 	[dtModified],vcSKU
	 ) VALUES     (@intWebApiId,
                 @numDomainID,
                 @numItemCode,
                 @vcApiItemId,
	 			 @numUserCntID,
	 			 GETUTCDATE(),
	 			 @numUserCntID,
	 			 GETUTCDATE(),@vcSKU
	 			 ) 
   
	--update lastUpdated Date                  
	--UPDATE [WebAPIDetail] SET [vcThirteenthFldValue] = GETUTCDATE() WHERE [WebApiId] =@intWebApiId AND [numDomainId] = @numDomainID
   END
 
 
end         
else if    @numItemCode>0  AND @intWebApiId > 0 
BEGIN 
	IF @ProcedureCallFlag =1 
		BEGIN
		DECLARE @ExportToAPIList AS VARCHAR(30)
		SELECT @ExportToAPIList = vcExportToAPI FROM dbo.Item WHERE numItemCode = @numItemCode
		IF @ExportToAPIList != ''
			BEGIN
			IF NOT EXISTS(select * from (SELECT * FROM dbo.Split(@ExportToAPIList ,','))as A where items= @vcExportToAPI ) 
				BEGIN
					SET @vcExportToAPI = @ExportToAPIList + ',' + @vcExportToAPI
				End
			ELSE
				BEGIN
					SET @vcExportToAPI = @ExportToAPIList 
				End
			END
		--update ExportToAPI String value
		UPDATE dbo.Item SET vcExportToAPI=@vcExportToAPI where numItemCode=@numItemCode  AND numDomainID=@numDomainID
					
			SELECT @cnt = COUNT([numItemID]) FROM [ItemAPI] WHERE  [WebApiId] = @intWebApiId 
																AND [numDomainId] = @numDomainID
																AND [vcAPIItemID] = @vcApiItemId
																AND numItemID = @numItemCode
			IF @cnt > 0
			BEGIN
				--update lastUpdated Date 
				UPDATE [ItemAPI] SET [dtModified] =GETUTCDATE() WHERE [WebApiId] =@intWebApiId AND [numDomainId] = @numDomainID
			END
			ELSE
				--Insert ItemAPI mapping
				BEGIN
					INSERT INTO [ItemAPI] (
						[WebApiId],
						[numDomainId],
						[numItemID],
						[vcAPIItemID],
						[numCreatedby],
						[dtCreated],
						[numModifiedby],
						[dtModified],vcSKU
					) VALUES     (@intWebApiId,
						@numDomainID,
						@numItemCode,
						@vcApiItemId,
						@numUserCntID,
						GETUTCDATE(),
						@numUserCntID,
						GETUTCDATE(),@vcSKU
					)	
				END

		END   
END
                                                       
else if    @numItemCode>0  AND @intWebApiId <= 0                                                           
begin

	DECLARE @OldGroupID NUMERIC 
	SELECT @OldGroupID = ISNULL(numItemGroup,0) FROM [Item] WHERE numitemcode = @numItemCode AND [numDomainId] = @numDomainID
	
--	--validate average cost, do not update average cost if item has sales/purchase orders
--	IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityItems OI WHERE OI.numItemCode =@numItemCode) >0
--	BEGIN
--		SELECT @monAverageCost = monAverageCost FROM dbo.Item WHERE numItemCode=@numItemCode
--	END

	DECLARE @monOldAverageCost AS DECIMAL(18,4);SET @monOldAverageCost=0
	SELECT @monOldAverageCost=ISNULL(monAverageCost,0) FROM Item WHERE numItemCode =@numItemCode AND [numDomainId] = @numDomainID
	
----	-- Remove below code block while you update production server
----	PRINT 'FROM ITEM UPDATE : Supplied VendorID =' + CONVERT(VARCHAR(10), @numVendorID)
----	IF ISNULL(@numVendorID,0) = 0
----	BEGIN
----		SELECT @numVendorID = ISNULL(numVendorID,0) FROM dbo.Item WHERE numItemCode = @numItemCode
----		--PRINT @numVendorID
----	END
----	PRINT 'FROM ITEM UPDATE : Modified VendorID =' + CONVERT(VARCHAR(10), @numVendorID)
----	--------------------------------------------------------              
 update item set vcItemName=@vcItemName,                           
  txtItemDesc=@txtItemDesc,                                             
  charItemType=@charItemType,                                             
  monListPrice= @monListPrice,
  numItemClassification=@numItemClassification,                                             
  bitTaxable=@bitTaxable,                                             
  vcSKU = (CASE WHEN ISNULL(@ParentSKU,'') = '' THEN @vcSKU ELSE @ParentSKU END),                                             
  bitKitParent=@bitKitParent,                                             
  --dtDateEntered=@dtDateEntered,                                             
  numVendorID=@numVendorID,                                             
  numDomainID=@numDomainID,                                             
  bintModifiedDate=getutcdate(),                                             
  numModifiedBy=@numUserCntID,                                   
  bitSerialized=@bitSerialized,                                                         
  vcModelID=@vcModelID,                                            
  numItemGroup=@numItemGroup,                                      
  numCOGsChartAcntId=@numCOGsChartAcntId,                                      
  numAssetChartAcntId=@numAssetChartAcntId,                                      
  numIncomeChartAcntId=@numIncomeChartAcntId,                            
  --monAverageCost=@monAverageCost,                          
  monCampaignLabourCost=@monLabourCost,                
  fltWeight=@fltWeight,                  
  fltHeight=@fltHeight,                  
  fltWidth=@fltWidth,                  
  fltLength=@fltLength,                  
  bitFreeShipping=@bitFreeshipping,                
  bitAllowBackOrder=@bitAllowBackOrder,              
  vcUnitofMeasure=@UnitofMeasure,            
  bitShowDeptItem=@bitShowDeptItem,            
  bitShowDeptItemDesc=@bitShowDeptItemDesc,        
  bitCalAmtBasedonDepItems=@bitCalAmtBasedonDepItems,    
  bitAssembly=@bitAssembly ,
numBarCodeId=@numBarCodeId,
vcManufacturer=@vcManufacturer,numBaseUnit=@numBaseUnit,numPurchaseUnit=@numPurchaseUnit,numSaleUnit=@numSaleUnit,bitLotNo=@bitLotNo,
IsArchieve = @IsArchieve,numItemClass=@numItemClass,tintStandardProductIDType=@tintStandardProductIDType,vcExportToAPI=@vcExportToAPI,
numShipClass=@numShipClass,bitAllowDropShip=@bitAllowDropShip, bitArchiveItem = @bitArchiveItem,bitAsset=@bitAsset,bitRental=@bitRental
where numItemCode=@numItemCode  AND numDomainID=@numDomainID

IF NOT EXISTS(SELECT numItemCode FROM dbo.OpportunityItems WHERE numItemCode = @numItemCode)
	BEGIN
		IF (((SELECT ISNULL(SUM(numOnHand),0) FROM WareHouseItems WHERE numItemID = @numItemCode AND numDomainId=@numDomainID) = 0) AND ((SELECT ISNULL(monAverageCost,0) FROM Item  WHERE numItemCode = @numItemCode AND numDomainId=@numDomainID ) = 0))
		BEGIN
			UPDATE Item SET monAverageCost=@monAverageCost  where numItemCode=@numItemCode  AND numDomainID=@numDomainID	
		END
		--UPDATE Item SET monAverageCost=@monAverageCost  where numItemCode=@numItemCode  AND numDomainID=@numDomainID	
	END
	
 --If Average Cost changed then add in Tracking
	IF ISNULL(@monAverageCost,0) != ISNULL(@monOldAverageCost,0)
	BEGIN
		DECLARE @numWareHouseItemID NUMERIC(9);SET @numWareHouseItemID=0
		DECLARE @numTotal int;SET @numTotal=0
		DECLARE @i int;SET @i=0
		DECLARE @vcDescription AS VARCHAR(100);SET @vcDescription=''
		
	    SELECT @numTotal=COUNT(*) FROM WareHouseItems where numItemID=@numItemCode AND numDomainId=@numDomainId
	     
		WHILE @i < @numTotal
		BEGIN
			SELECT @numWareHouseItemID = min(numWareHouseItemID) FROM WareHouseItems 
				WHERE numItemID=@numItemCode AND numDomainId=@numDomainId and numWareHouseItemID > @numWareHouseItemID 
				
			IF @numWareHouseItemID>0
			BEGIN
				SET @vcDescription='Average Cost Changed Manually OLD : ' + CAST(ISNULL(@monOldAverageCost,0) AS VARCHAR(20))
				
				DECLARE @numDomain AS NUMERIC(18,0)
				SET @numDomain = @numDomainID
				EXEC dbo.USP_ManageWareHouseItems_Tracking
					@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)		
					@numReferenceID = @numItemCode, --  numeric(9, 0)
					@tintRefType = 1, --  tinyint
					@vcDescription = @vcDescription, --  varchar(100)
					@tintMode = 0, --  tinyint
					@numModifiedBy = @numUserCntID, --  numeric(9, 0)
					@numDomainID = @numDomain
		    END
		    
		    SET @i = @i + 1
		END
	END
 
 DECLARE @bitCartFreeShipping as  bit 
 SELECT @bitCartFreeShipping = bitFreeShipping FROM dbo.CartItems where numItemCode = @numItemCode  AND numDomainID = @numDomainID 
 
 IF @bitCartFreeShipping <> @bitFreeshipping
 BEGIN
    UPDATE dbo.CartItems SET bitFreeShipping = @bitFreeshipping WHERE numItemCode = @numItemCode  AND numDomainID=@numDomainID
 END 
 
 
 IF  @intWebApiId > 1 
   BEGIN
      SELECT @cnt = COUNT([numItemID])
    FROM   [ItemAPI]
    WHERE  [WebApiId] = @intWebApiId
           AND [numDomainId] = @numDomainID
           AND [vcAPIItemID] = @vcApiItemId
    IF @cnt > 0
      BEGIN
      --update lastUpdated Date 
      UPDATE [ItemAPI] SET [dtModified] =GETUTCDATE() WHERE [WebApiId] =@intWebApiId AND [numDomainId] = @numDomainID
      END
      ELSE
      --Insert ItemAPI mapping
      BEGIN
        INSERT INTO [ItemAPI] (
	 	[WebApiId],
	 	[numDomainId],
	 	[numItemID],
	 	[vcAPIItemID],
	 	[numCreatedby],
	 	[dtCreated],
	 	[numModifiedby],
	 	[dtModified],vcSKU
	 ) VALUES     (@intWebApiId,
                 @numDomainID,
                 @numItemCode,
                 @vcApiItemId,
	 			 @numUserCntID,
	 			 GETUTCDATE(),
	 			 @numUserCntID,
	 			 GETUTCDATE(),@vcSKU
	 			 ) 
      END

--UPDATE [WebAPIDetail] SET [vcThirteenthFldValue] = GETUTCDATE() WHERE [WebApiId] =@intWebApiId AND [numDomainId] = @numDomainID
--UPDATE [ItemAPI] SET [dtModified] =GETUTCDATE() WHERE [WebApiId] =@intWebApiId AND [numDomainId] = @numDomainID
--  SELECT * FROM dbo.ItemCategory
--  Insert into ItemCategory(numItemID,numCategoryID)                                            
--  (SELECT numItemID,numCategoryID
--  FROM OPENXML(@hDoc,'/ItemCategories/',2)                                                                         
--    with(numWareHouseItmsDTLID numeric(9),                                                                        
--    numWareHouseItemID numeric(9),                                                  
--   ))
   
   
   END                                                                               
-- kishan It is necessary to add item into itemTax table . 
 IF	@bitTaxable = 1
	BEGIN
		IF NOT EXISTS(SELECT * FROM dbo.ItemTax WHERE numItemCode = @numItemCode AND numTaxItemID = 0)
			BEGIN
			    	INSERT INTO dbo.ItemTax (
						numItemCode,
						numTaxItemID,
						bitApplicable
					) VALUES ( 
						/* numItemCode - numeric(18, 0) */ @numItemCode,
						/* numTaxItemID - numeric(18, 0)*/ 0,
						/* bitApplicable - bit */ 1 ) 						
			END
	END


--  Insert into WareHouseItmsDTL(numWareHouseItemID,vcSerialNo,vcComments,numQty)                                            
--  (SELECT numWareHouseItemID,vcSerialNo,Comments,numQty                                                                        
--  FROM OPENXML(@hDoc,'/NewDataSet/SerializedItems[Op_Flag=1]',2)                                                                         
--    with(numWareHouseItmsDTLID numeric(9),                                                                        
--    numWareHouseItemID numeric(9),                                                  
--    vcSerialNo varchar(100),                                            
--    Op_Flag tinyint,          
--    Comments varchar(1000),numQty NUMERIC(9)))
--                                                                                
--  update WareHouseItmsDTL set                                                                         
--   numWareHouseItemID=X.numWareHouseItemID,                                                                       
--   vcSerialNo=X.vcSerialNo,          
--    vcComments=X.Comments,numQty=X.numQty                                                                                          
--    From (SELECT numWareHouseItmsDTLID as WareHouseItmsDTLID,numWareHouseItemID,vcSerialNo,Op_Flag,Comments,numQty                              
--   FROM OPENXML(@hDoc,'/NewDataSet/SerializedItems[Op_Flag=2]',2)                                                                         
--    with(numWareHouseItmsDTLID numeric(9),                                                          
--    numWareHouseItemID numeric(9),                                                                        
--    vcSerialNo varchar(100),                                            
--    Op_Flag tinyint,          
--    Comments varchar(1000),numQty NUMERIC(9)))X                                                                         
--  where  numWareHouseItmsDTLID=X.WareHouseItmsDTLID                                            
--                                             
--  delete from  WareHouseItmsDTL where numWareHouseItmsDTLID in (SELECT numWareHouseItmsDTLID                                                                        
--   FROM OPENXML(@hDoc,'/NewDataSet/SerializedItems[Op_Flag=3]',2)                                   
--    with(numWareHouseItmsDTLID numeric(9),                              
--    numWareHouseItemID numeric(9),                                                                        
--    vcSerialNo varchar(100),                                            
--  Op_Flag tinyint)) 


--  SELECT X.*,ROW_NUMBER() OVER( order by X.numQty) AS ROWNUMBER
--INTO #TempTableWareHouseItmsDTLID
--FROM ( SELECT  numWareHouseItmsDTLID,numWareHouseItemID,vcSerialNo,Op_Flag,Comments,numQty,OldQty
--                  FROM      OPENXML(@hDoc, '/NewDataSet/SerializedItems',2)
--                            WITH ( numWareHouseItmsDTLID NUMERIC(9), numWareHouseItemID NUMERIC(9), vcSerialNo VARCHAR(100), Op_Flag TINYINT, Comments VARCHAR(1000),OldQty NUMERIC(9),numQty NUMERIC(9) )
--                ) X


--DECLARE @minROWNUMBER INT
--DECLARE @maxROWNUMBER INT
--DECLARE @Diff INT,@Op_Flag AS int
--DECLARE @Diff1 INT
--DECLARE @numWareHouseItemID NUMERIC(9)
--DECLARE @numWareHouseItmsDTLID NUMERIC(9)
--
--SELECT  @minROWNUMBER = MIN(ROWNUMBER) , @maxROWNUMBER =MAX(ROWNUMBER) FROM #TempTableWareHouseItmsDTLID
--
--WHILE  @minROWNUMBER <= @maxROWNUMBER
--    BEGIN
--   	    SELECT @Diff = numQty - OldQty,@numWareHouseItemID=numWareHouseItemID,@Op_Flag=Op_Flag,@numWareHouseItmsDTLID=numWareHouseItmsDTLID FROM #TempTableWareHouseItmsDTLID WHERE ROWNUMBER=@minROWNUMBER
--   	    
--   	    IF @Op_Flag=2 
--   	    BEGIN
--		   	 UPDATE WareHouseItmsDTL SET  numWareHouseItemID = X.numWareHouseItemID,
--                vcSerialNo = X.vcSerialNo,vcComments = X.Comments,numQty=X.numQty 
--                FROM #TempTableWareHouseItmsDTLID X INNER JOIN WareHouseItmsDTL W
--                ON X.numWareHouseItmsDTLID=W.numWareHouseItmsDTLID WHERE X.ROWNUMBER=@minROWNUMBER
--		END 
--   	    
--   	    ELSE IF @Op_Flag=1 
--   	    BEGIN
--   	       	  INSERT INTO WareHouseItmsDTL(numWareHouseItemID,vcSerialNo,vcComments,numQty)  
--		   	   SELECT  X.numWareHouseItemID,X.vcSerialNo,X.Comments,X.numQty  FROM #TempTableWareHouseItmsDTLID X 
--		   	   WHERE X.ROWNUMBER=@minROWNUMBER
--		END
--   	    
--   	    ELSE IF @Op_Flag=3 
--   	    BEGIN
--   	       	  delete from  WareHouseItmsDTL where numWareHouseItmsDTLID=@numWareHouseItmsDTLID
--		END
--		
--   	    update WareHouseItems SET numOnHand=numOnHand + @Diff where numWareHouseItemID = @numWareHouseItemID AND (numOnHand + @Diff)>=0
--  
--        SELECT  @minROWNUMBER = MIN(ROWNUMBER) FROM  #TempTableWareHouseItmsDTLID WHERE  [ROWNUMBER] > @minROWNUMBER
--    END	 
      
IF @charItemType='S' or @charItemType='N'                                                                       
BEGIN
	delete from WareHouseItmsDTL where numWareHouseItemID in (select numWareHouseItemID from WareHouseItems where numItemID=@numItemCode AND [WareHouseItems].[numDomainID] = @numDomainID)
	delete from WareHouseItems where numItemID=@numItemCode AND [WareHouseItems].[numDomainID] = @numDomainID
END

--	insert into WareHouseItems (numItemID, numWareHouseID,numOnHand,monWListPrice,vcLocation,vcWHSKU,vcBarCode,numDomainID,numReorder)  
--	SELECT @numItemCode,numWareHouseID,OnHand,Price,Location,SKU,BarCode,@numDomainID,Reorder
--	FROM OPENXML(@hDoc,'/NewDataSet/WareHouse[Op_Flag=1]',2)                                                    
--    with(numWareHouseItemID numeric(9),                                                                        
--    numWareHouseID numeric(9),                    
--    Reorder numeric(9),                                                                       
--    OnHand numeric(9),                                            
--    Op_Flag tinyint,      
--    Price money,
--    Location Varchar(100),
--    SKU varchar(100),
--    BarCode varchar(100))
--SELECT @@IDENTITY	

--  update WareHouseItems  set                                                                         
--   numItemID=@numItemCode,                                                                       
--   numWareHouseID=X.numWareHouseID,                                                              
--   numOnHand=X.OnHand,                     
--   numReorder=X.Reorder,      
--   monWListPrice=X.Price,
--   vcLocation=X.Location,
--   vcWHSKU=X.SKU,
--	vcBarCode=X.BarCode                              
--    From (SELECT numWareHouseItemID as WareHouseItemID,numWareHouseID,Reorder,OnHand,Op_Flag,Price,Location,SKU,BarCode                                                                        
--   FROM OPENXML(@hDoc,'/NewDataSet/WareHouse[Op_Flag=2]',2)                                                                         
--    with(numWareHouseItemID numeric(9),                                                                        
--    numWareHouseID numeric(9),                    
--    Reorder numeric(9),                                                                       
--    OnHand numeric(9),                                            
--    Op_Flag tinyint,      
--    Price money,
--    Location Varchar(100),
--    SKU varchar(100),
--    BarCode varchar(100)))X                                                                         
--  where  numWareHouseItemID=X.WareHouseItemID                                   
                                   
                                   
--    delete from  WareHouseItmsDTL where numWareHouseItemID in (SELECT numWareHouseItemID                                                                 
--   FROM OPENXML(@hDoc,'/NewDataSet/WareHouse[Op_Flag=3]',2)                                                                         
--    with(numWareHouseItemID numeric(9),                                            
--    Op_Flag tinyint))                                             
	
	/*Enforce valiation for child records*/
--	IF exists (SELECT * FROM [OpportunityMaster] OM INNER JOIN [OpportunityItems] OI ON OM.[numOppId] = OI.[numOppId] WHERE OM.[numDomainId]=@numDomainID AND [numWarehouseItmsID] IN (SELECT numWareHouseItemID FROM OPENXML(@hDoc,'/NewDataSet/WareHouse[Op_Flag=3]',2)
--    with(numWareHouseItemID numeric(9),
--    Op_Flag TINYINT)) )
--	BEGIN
--		raiserror('CHILD_WAREHOUSE',16,1);
--		RETURN ;
--	END

--  delete from  WareHouseItems where numWareHouseItemID in (SELECT numWareHouseItemID FROM OPENXML(@hDoc,'/NewDataSet/WareHouse[Op_Flag=3]',2)
--    with(numWareHouseItemID numeric(9),                                                                 
--    numWareHouseID numeric(9),                                                                        
--    OnHand numeric(9),                                            
--    Op_Flag tinyint))                                  
    /*commented by chintan, Obsolete, Reason: While inserting new Row numWareHouseItemID will be 0,1,2 incrementally. */
--  delete from  WareHouseItems where numItemID=@numItemCode and  numWareHouseItemID not in (SELECT numWareHouseItemID                                                                    
--   FROM OPENXML(@hDoc,'/NewDataSet/WareHouse',2)                                                                         
--    with(numWareHouseItemID numeric(9),                                    
--    numWareHouseID numeric(9),                                                                        
--    OnHand numeric(9),                                            
--    Op_Flag tinyint))                                        

/*If Item is Matrix item and changed to non matrix item then remove matrix attributes associated with- by chintan*/
--IF @OldGroupID>0 AND @numItemGroup = 0 
--BEGIN
--	DELETE FROM [CFW_Fld_Values_Serialized_Items] WHERE [bitSerialized] =0 AND 
--	[RecId] IN ( SELECT numWareHouseItemID FROM  OPENXML(@hDoc,'/NewDataSet/WareHouse',2) with(numWareHouseItemID numeric(9)) )
--END
                                         
                                             
                                                       
                                             
                                             
-- delete from WareHouseItmsDTL where numWareHouseItemID not in (select numWareHouseItemID from WareHouseItems)                                            
 
-- if @bitSerialized=0 AND @bitLotNo=0 delete from WareHouseItmsDTL where  numWareHouseItemID  in (select numWareHouseItemID from WareHouseItems where numItemID=@numItemCode)                                             
    
--    DELETE FROM  ItemUOM WHERE numItemCode=@numItemCode AND numDomainId=@numDomainId
    
--    insert into ItemUOM (numItemCode, numUOMId,numDomainId)  
--	SELECT @numItemCode,numUOMId,@numDomainID
--	FROM OPENXML(@hDoc,'/NewDataSet/TableUOM',2)                                                    
--    with(numUOMId numeric(9))                                        
   
    if @bitKitParent=1  OR @bitAssembly = 1              
    begin              
  declare @hDoc1 as int                                            
  EXEC sp_xml_preparedocument @hDoc1 OUTPUT, @strChildItems                                                                        
                                                                                                 
                                                                
   update ItemDetails set                                                                         
    numQtyItemsReq=X.QtyItemsReq,numWareHouseItemId=X.numWarehouseItmsID,numUOMId=X.numUOMId,vcItemDesc=X.vcItemDesc,sintOrder=X.sintOrder                                                                                          
     From (SELECT QtyItemsReq,ItemKitID,ChildItemID,numWarehouseItmsID,numUOMId,vcItemDesc,sintOrder                             
    FROM OPENXML(@hDoc1,'/NewDataSet/Table1',2)                                                                         
     with(ItemKitID numeric(9),                                                          
     ChildItemID numeric(9),                                                                        
     QtyItemsReq numeric(9),  
     numWarehouseItmsID numeric(9),numUOMId NUMERIC(9),vcItemDesc VARCHAR(1000),sintOrder int))X                                                                         
   where  numItemKitID=X.ItemKitID and numChildItemID=ChildItemID                
              
 end   
 ELSE
 BEGIN
 	DELETE FROM ItemDetails WHERE numItemKitID=@numItemCode
 END           
                                                               
end

declare  @rows as integer                                        
 EXEC sp_xml_preparedocument @hDoc OUTPUT, @strFieldList         
 
 --THIS CODE IS COMMENTED BECAUSE THERE IS NO LIST PRICE TEXTBOX IN MANAGE INVENTORY SCREEN FOR SERIALIZED ITEM NOW
 --FROM WHERE USER CAN SET LIST PRICE
 --IF @bitSerialized = 1
 --BEGIN
	
	--UPDATE Item SET [monListPrice]  = (
	--SELECT WHI.[monWListPrice] FROM [dbo].[WareHouseItems] AS WHI WHERE [WHI].[numItemID] = @numItemCode
	--AND [WHI].[numDomainID] = @numDomainID
	--AND [WHI].[numWareHouseID] = (SELECT TOP 1 [WareHouseItems].[numWareHouseID] FROM [dbo].[WareHouseItems] WHERE [WareHouseItems].[numItemID] = [WHI].[numItemID])
	--)
	--WHERE [Item].[numItemCode] = @numItemCode AND [Item].[numDomainID] = @numDomainID

 --END                                                               
                                         
 SELECT @rows=count(*) FROM OPENXML (@hDoc,'/NewDataSet/CusFlds',2)                                                  
  WITH (Fld_ID numeric(9),Fld_Value varchar(100),RecId numeric(9))                                        
                                                                           
                                          
 if @rows>0                                        
 begin                                        
  Create table #tempTable ( ID INT IDENTITY PRIMARY KEY,                    
  Fld_ID numeric(9),                                        
  Fld_Value varchar(100),                                        
  RecId numeric(9),                                    
  bitSItems bit                                                                         
  )                                         
  insert into #tempTable (Fld_ID,Fld_Value,RecId,bitSItems)                                        
  SELECT * FROM OPENXML (@hDoc,'/NewDataSet/CusFlds',2)                                                  
  WITH (Fld_ID numeric(9),Fld_Value varchar(100),RecId numeric(9),bitSItems bit)                                           
                                         
  delete CFW_Fld_Values_Serialized_Items from CFW_Fld_Values_Serialized_Items C                                        
  inner join #tempTable T                          
  on   C.Fld_ID=T.Fld_ID and C.RecId=T.RecId and bitSerialized=bitSItems                                         
                                         
  drop table #tempTable                                        
                                                            
  insert into CFW_Fld_Values_Serialized_Items(Fld_ID,Fld_Value,RecId,bitSerialized)                                                  
  select Fld_ID,isnull(Fld_Value,'') as Fld_Value,RecId,bitSItems from(SELECT * FROM OPENXML (@hDoc,'/NewDataSet/CusFlds',2)                                                  
  WITH (Fld_ID numeric(9),Fld_Value varchar(100),RecId numeric(9),bitSItems bit)) X                                         
 end  

IF @numItemCode > 0
	BEGIN
			IF EXISTS(SELECT * FROM dbo.ItemCategory WHERE numItemID = @numItemCode)
			 BEGIN
					DELETE FROM dbo.ItemCategory WHERE numItemID = @numItemCode		
			 END 
	  IF @vcCategories <> ''	
		BEGIN
        INSERT INTO ItemCategory( numItemID , numCategoryID )  
        SELECT  @numItemCode AS numItemID,ID from dbo.SplitIDs(@vcCategories,',')	
		END
	END

 EXEC sp_xml_removedocument @hDoc              

--CASE WHEN @bitSerialized = 1 THEN (SELECT * FROM [dbo].[WareHouseItems] AS WI WHERE [WI].[numItemID] = @numItemCode AND [WI].[numWareHouseID] = )
--					 ELSE @monListPrice 
--				END,                                             
/****** Object:  StoredProcedure [dbo].[USP_OppManage]    Script Date: 07/26/2008 16:20:19 ******/
--2 changes
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OppManage')
DROP PROCEDURE usp_oppmanage
GO
CREATE PROCEDURE [dbo].[USP_OppManage] 
(                                                                          
  @numOppID as numeric=0 OUTPUT,                                                                          
  @numContactId numeric(9)=null,                                                                          
  @numDivisionId numeric(9)=null,                                                                          
  @tintSource numeric(9)=null,                                                                          
  @vcPOppName Varchar(100)='' OUTPUT,                                                                          
  @Comments varchar(1000)='',                                                                          
  @bitPublicFlag bit=0,                                                                          
  @numUserCntID numeric(9)=0,                                                                                 
  @monPAmount money =0,                                                 
  @numAssignedTo as numeric(9)=0,                                                                                                        
  @numDomainId numeric(9)=0,                                                                                                                                                                                   
  @strItems as text=null,                                                                          
  @strMilestone as text=null,                                                                          
  @dtEstimatedCloseDate datetime,                                                                          
  @CampaignID as numeric(9)=null,                                                                          
  @lngPConclAnalysis as numeric(9)=null,                                                                         
  @tintOppType as tinyint,                                                                                                                                             
  @tintActive as tinyint,                                                              
  @numSalesOrPurType as numeric(9),              
  @numRecurringId as numeric(9),
  @numCurrencyID as numeric(9)=0,
  @DealStatus as tinyint =0,-- 0=Open,1=Won,2=Lost
  @numStatus AS NUMERIC(9),
  @vcOppRefOrderNo VARCHAR(100),
  @numBillAddressId numeric(9)=0,
  @numShipAddressId numeric(9)=0,
  @bitStockTransfer BIT=0,
  @WebApiId INT = 0,
  @tintSourceType TINYINT=0,
  @bitDiscountType as bit,
  @fltDiscount  as float,
  @bitBillingTerms as bit,
  @intBillingDays as integer,
  @bitInterestType as bit,
  @fltInterest as float,
  @tintTaxOperator AS TINYINT,
  @numDiscountAcntType AS NUMERIC(9),
  @vcWebApiOrderNo VARCHAR(100)=NULL,
  @vcCouponCode		VARCHAR(100) = '',
  @vcMarketplaceOrderID VARCHAR(100)=NULL,
  @vcMarketplaceOrderDate  datetime=NULL,
  @vcMarketplaceOrderReportId VARCHAR(100)=NULL,
  @numPercentageComplete NUMERIC(9),
  @bitUseShippersAccountNo BIT = 0,
  @bitUseMarkupShippingRate BIT = 0,
  @numMarkupShippingRate NUMERIC(19,2) = 0,
  @intUsedShippingCompany INT = 0
)                                                                          
                                                                          
as  
BEGIN TRY
BEGIN TRANSACTION
 DECLARE @fltExchangeRate float                                 
 DECLARE @hDocItem int                                                                                                                            
 DECLARE @TotalAmount as money                                                                          
 DECLARE @tintOppStatus as tinyint               
 DECLARE @tintDefaultClassType NUMERIC
 DECLARE @numDefaultClassID NUMERIC;SET @numDefaultClassID=0
 
 --Get Default Item Class for particular user based on Domain settings  
 SELECT @tintDefaultClassType=isnull(tintDefaultClassType,0) FROM dbo.Domain  WHERE numDomainID = @numDomainID 

 IF @tintDefaultClassType=0
      SELECT @numDefaultClassID=NULLIF(numDefaultClass,0) FROM dbo.UserMaster  WHERE numUserDetailId =@numUserCntID AND numDomainID = @numDomainID 


--If new Oppertunity
if @numOppID = 0                                                                          
 begin     
  declare @intOppTcode as numeric(9)                           
  Select @intOppTcode=max(numOppId)from OpportunityMaster                
  set @intOppTcode =isnull(@intOppTcode,0) + 1                                                
  set @vcPOppName=@vcPOppName+ '-'+convert(varchar(4),year(getutcdate()))  +'-A'+ convert(varchar(10),@intOppTcode)                                  
  
  IF ISNULL(@numCurrencyID,0) = 0 
	BEGIN
	 SET @fltExchangeRate=1
	 SELECT @numCurrencyID= ISNULL(numCurrencyID,0) FROM dbo.Domain WHERE numDomainId=@numDomainId	
	END
  else set @fltExchangeRate=dbo.GetExchangeRate(@numDomainID,@numCurrencyID)
                                                                       
  --Set Default Class If enable User Level Class Accountng 
  DECLARE @numAccountClass AS NUMERIC(18);SET @numAccountClass=0
  SELECT @numAccountClass=ISNULL(numDefaultClass,0) FROM dbo.UserMaster UM JOIN dbo.Domain D ON UM.numDomainID=D.numDomainId
  WHERE D.numDomainId=@numDomainId AND UM.numUserDetailId=@numUserCntID AND ISNULL(D.IsEnableUserLevelClassTracking,0)=1
                                                                       
  insert into OpportunityMaster                                                                          
  (                                                                             
  numContactId,                                                                   
  numDivisionId,                                                                          
  txtComments,                                             
  numCampainID,                                                                          
  bitPublicFlag,                                                                          
  tintSource,
  tintSourceType,                                                             
  vcPOppName,                                                                          
  intPEstimatedCloseDate,                                                                          
  monPAmount,                                                                           
  numCreatedBy,                                                                          
  bintCreatedDate,                                                                           
  numModifiedBy,                                                                          
  bintModifiedDate,                                                                          
  numDomainId,                                                                                                             
  numRecOwner,                                                                          
  lngPConclAnalysis,                                                                         
  tintOppType,                                                              
  numSalesOrPurType,
  numCurrencyID,
  fltExchangeRate,
  numAssignedTo,
  numAssignedBy,
  [tintOppStatus],
  numStatus,
  vcOppRefOrderNo,
  --vcWebApiOrderNo,
  bitStockTransfer,bitBillingTerms,intBillingDays,bitInterestType,fltInterest,vcCouponCode,
  bitDiscountType,fltDiscount,bitPPVariance,vcMarketplaceOrderID,vcMarketplaceOrderReportId,
  numPercentageComplete,numAccountClass,tintTaxOperator,bitUseShippersAccountNo,
  bitUseMarkupShippingRate,
  numMarkupShippingRate,
  intUsedShippingCompany
  )                                                                          
  Values                                                                          
  (                                                                          
  @numContactId,                                                                          
  @numDivisionId,                                                                          
  @Comments,                           
  @CampaignID,                                                                          
  @bitPublicFlag,                     
  @tintSource,
  @tintSourceType,                         
  @vcPOppName,                                                                          
  @dtEstimatedCloseDate,                                                                          
  @monPAmount,                                                                                
  @numUserCntID,                                                                          
 CASE @tintSourceType WHEN 3 THEN @vcMarketplaceOrderDate ELSE GETUTCDATE() END,                                                                          
  @numUserCntID,                                                           
  getutcdate(),                                                                          
  @numDomainId,                                                                                   
  @numUserCntID,                                                                          
  @lngPConclAnalysis,                                                                                                                                                                      
  @tintOppType,                                                                                                                                      
  @numSalesOrPurType,              
  @numCurrencyID, 
  @fltExchangeRate,
  case when @numAssignedTo>0 then @numAssignedTo else null end,
  case when @numAssignedTo>0 then @numUserCntID else null   END,
  @DealStatus,
  @numStatus,
  @vcOppRefOrderNo,
 -- @vcWebApiOrderNo,
  @bitStockTransfer,@bitBillingTerms,@intBillingDays,@bitInterestType,@fltInterest,@vcCouponCode,
  @bitDiscountType,@fltDiscount,1,@vcMarketplaceOrderID,@vcMarketplaceOrderReportId,
  @numPercentageComplete,@numAccountClass,@tintTaxOperator,@bitUseShippersAccountNo,
  @bitUseMarkupShippingRate,
  @numMarkupShippingRate,
  @intUsedShippingCompany
    )                                                                                                                      
  set @numOppID=scope_identity()                                                
  
  --Update OppName as per Name Template
  EXEC dbo.USP_UpdateNameTemplateValue @tintOppType,@numDomainID,@numOppID

  --Map Custom Field	
  DECLARE @tintPageID AS TINYINT;
  SET @tintPageID=CASE WHEN @tintOppType=1 THEN 2 ELSE 6 END 
  	
  EXEC dbo.USP_AddParentChildCustomFieldMap
	@numDomainID = @numDomainID, --  numeric(18, 0)
	@numRecordID = @numOppID, --  numeric(18, 0)
	@numParentRecId = @numDivisionId, --  numeric(18, 0)
	@tintPageID = @tintPageID --  tinyint
 	
  
	IF ISNULL(@numStatus,0) > 0 AND @tintOppType=1 AND isnull(@DealStatus,0)=1
	   BEGIN
	   		EXEC USP_ManageOpportunityAutomationQueue
					@numOppQueueID = 0, -- numeric(18, 0)
					@numDomainID = @numDomainID, -- numeric(18, 0)
					@numOppId = @numOppID, -- numeric(18, 0)
					@numOppBizDocsId = 0, -- numeric(18, 0)
					@numOrderStatus = @numStatus, -- numeric(18, 0)
					@numUserCntID = @numUserCntID, -- numeric(18, 0)
					@tintProcessStatus = 1, -- tinyint
					@tintMode = 1 -- TINYINT
	   END
  ---- inserting Items                                                                          
   
                                                   
  if convert(varchar(10),@strItems) <>'' AND @numOppID>0
  begin                      
   EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems       
	
  insert into                       
   OpportunityItems                                                                          
   (numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,vcItemName,vcModelID,vcManufacturer,vcPathForTImage,monVendorCost,numUOMId,bitWorkOrder,
   numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,numClassID,numToWarehouseItemID,vcAttributes,monAvgCost,bitItemPriceApprovalRequired)
   select @numOppID,X.numItemCode,dbo.fn_UOMConversion(X.numUOM,numItemCode,@numDomainId,null) * x.numUnitHour,
   x.monPrice,x.monTotAmount,X.vcItemDesc,
   CASE X.numWarehouseItmsID 
   WHEN -1 
	THEN 
		CASE 
			WHEN ((SELECT COUNT(*) FROM Item WHERE numDomainID=@numDomainId AND vcSKU= X.vcSKU) > 0)
				THEN
					(SELECT TOP 1 
						numWareHouseItemID 
					FROM 
						WareHouseItems 
					WHERE 
						[numItemID] = X.numItemCode 
						AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId))
			ELSE
				(SELECT 
					[numWareHouseItemID] 
				FROM 
					[WareHouseItems] 
				WHERE 
					[numItemID] = X.numItemCode 
					AND ISNULL(WareHouseItems.vcWHSKU,'') = ISNULL(X.vcSKU,'')
					AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId)) 
		END
	WHEN 0 
	THEN 
		CASE 
		WHEN ((SELECT COUNT(*) FROM Item WHERE numDomainID=@numDomainId AND vcSKU=X.vcSKU) > 0)
			THEN
				(SELECT TOP 1 
					numWareHouseItemID 
				FROM 
					WareHouseItems 
				WHERE 
					[numItemID] = X.numItemCode 
					AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId))
		ELSE
			(SELECT 
				[numWareHouseItemID] 
			FROM 
				[WareHouseItems] 
			WHERE 
				[numItemID] = X.numItemCode 
				AND ISNULL(WareHouseItems.vcWHSKU,'') = ISNULL(X.vcSKU,'')
				AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId)) 
		END
	ELSE  
		X.numWarehouseItmsID 
	END AS numWarehouseItmsID,
	X.ItemType,X.DropShip,X.bitDiscountType,
   X.fltDiscount,X.monTotAmtBefDiscount,X.vcItemName,
   (SELECT vcModelID FROM item WHERE numItemCode = X.numItemCode),
   (SELECT vcManufacturer FROM item WHERE numItemCode = X.numItemCode),
   (SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = X.numItemCode AND BitDefault = 1 AND numdomainId = @numdomainId),
   (select isnull(VN.monCost,0) from Vendor VN, Item IT where IT.numItemCode=VN.numItemCode and  VN.numItemCode=X.numItemCode and  VN.numVendorID=IT.numVendorID),
   X.numUOM,X.bitWorkOrder,X.numVendorWareHouse,X.numShipmentMethod,X.numSOVendorId,X.numProjectID,X.numProjectStageID,Case When @tintDefaultClassType=0 then @numDefaultClassID else (SELECT numItemClass FROM item WHERE numItemCode = X.numItemCode) end,X.numToWarehouseItemID,X.Attributes,
   (SELECT ISNULL(monAverageCost,0) FROM Item I WHERE I.numDomainId=@numDomainId AND I.numItemCode=X.numItemCode),X.bitItemPriceApprovalRequired from(
   SELECT *FROM OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
   WITH                       
   (                                                                          
   numoppitemtCode numeric(9),                                     
   numItemCode numeric(9),                                                                          
   numUnitHour numeric(9,2),                                                                          
   monPrice money,                                                                       
   monTotAmount money,                                                                          
   vcItemDesc varchar(1000),                                    
   numWarehouseItmsID numeric(9),                          
   ItemType varchar(30),      
   DropShip bit,
   bitDiscountType bit,
   fltDiscount float,
   monTotAmtBefDiscount MONEY,
   vcItemName varchar(300),numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9) ,numSOVendorId numeric(9),numProjectID numeric(9),numProjectStageID numeric(9),numToWarehouseItemID NUMERIC(9),Attributes varchar(200),vcSKU VARCHAR(100),bitItemPriceApprovalRequired BIT
   ))X    
   ORDER BY 
   X.numoppitemtCode
    
    
   Update OpportunityItems                       
   set numItemCode=X.numItemCode,    
   numOppId=@numOppID,                     
   numUnitHour=dbo.fn_UOMConversion(X.numUOM,X.numItemCode,@numDomainId,null) * X.numUnitHour,                      
   monPrice=x.monPrice,                      
   monTotAmount=x.monTotAmount,                                  
   vcItemDesc=X.vcItemDesc,                      
   numWarehouseItmsID=X.numWarehouseItmsID,                 
   bitDropShip=X.DropShip,
   bitDiscountType=X.bitDiscountType,
   fltDiscount=X.fltDiscount,
   monTotAmtBefDiscount=X.monTotAmtBefDiscount,vcItemName=X.vcItemName,numUOMId=numUOM,bitWorkOrder=X.bitWorkOrder,
   numVendorWareHouse=X.numVendorWareHouse,numShipmentMethod=X.numShipmentMethod,numSOVendorId=X.numSOVendorId,numProjectId=X.numProjectID,numProjectStageId=X.numProjectStageID,numToWarehouseItemID = X.numToWarehouseItemID,
vcAttributes=X.Attributes,numClassID=(Case When @tintDefaultClassType=0 then @numDefaultClassID else (SELECT numItemClass FROM item WHERE numItemCode = X.numItemCode) END),bitItemPriceApprovalRequired = X.bitItemPriceApprovalRequired
--   ,vcModelID=(SELECT vcModelID FROM item WHERE numItemCode = X.numItemCode),vcManufacturer=(SELECT vcManufacturer FROM item WHERE numItemCode = X.numItemCode)
--  ,vcPathForTImage=(SELECT vcPathForTImage FROM item WHERE numItemCode = X.numItemCode),monVendorCost=(select isnull(VN.monCost,0) from Vendor VN, Item IT where IT.numItemCode=VN.numItemCode and  VN.numItemCode=X.numItemCode and  VN.numVendorID=IT.numVendorID)
   from (SELECT numoppitemtCode as numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,DropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,vcItemName,numUOM,bitWorkOrder,
   numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,numToWarehouseItemID,Attributes,bitItemPriceApprovalRequired
   FROM OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=2]',2)
   WITH  (                      
    numoppitemtCode numeric(9),                                     
    numItemCode numeric(9),                                                                          
    numUnitHour numeric(9,2),                                                                          
    monPrice money,                                         
    monTotAmount money,                                                                          
    vcItemDesc varchar(1000),                                    
    numWarehouseItmsID numeric(9),      
    DropShip bit,
	bitDiscountType bit,
    fltDiscount float,
    monTotAmtBefDiscount MONEY,vcItemName varchar(300),numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId numeric(9),numProjectID numeric(9),numProjectStageID numeric(9),numToWarehouseItemID NUMERIC(9),Attributes varchar(200),bitItemPriceApprovalRequired BIT
   ))X where numoppitemtCode=X.numOppItemID                                          
	
	
   -- Update UOM of opportunity items have not UOM set while exported from Marketplace (means tintSourceType = 3)
	IF ISNULL(@tintSourceType,0) = 3
	BEGIN
		--SELECT * FROM OpportunityMaster WHERE numOppId = 81654
		--SELECT * FROM OpportunityItems WHERE numOppId = 81654
		--SELECT * FROM Item WHERE numItemCode = 822078

		UPDATE OI SET numUOMID = ISNULL(I.numSaleUnit,0), 
					  numUnitHour = dbo.fn_UOMConversion(ISNULL(I.numSaleUnit,0),OI.numItemCode,@numDomainId,null) * OI.numUnitHour
		FROM OpportunityItems OI
		JOIN Item I ON OI.numItemCode = I.numItemCode
		WHERE numOppId = @numOppID
		AND I.numDomainID = @numDomainId

	END	
   --Update OpportunityItems
   --set 
   --FROM (SELECT numItemCode,vcItemName,[txtItemDesc],vcModelID FROM item WHERE numItemCode IN (SELECT [numItemCode] FROM OpportunityItems WHERE numOppID = @numOppID))X
   --WHERE OpportunityItems.[numItemCode] = X.numItemCode 
                                     
   insert into OppWarehouseSerializedItem                                                                          
   (numOppID,numWarehouseItmsDTLID,numOppItemID,numWarehouseItmsID)                      
   select @numOppID,X.numWarehouseItmsDTLID,X.numoppitemtCode,X.numWItmsID from(                                                                          
   SELECT * FROM OPENXML (@hDocItem,'/NewDataSet/SerialNo',2)                                                                          
   WITH                        
   (                                               
   numWarehouseItmsDTLID numeric(9),                      
   numoppitemtCode numeric(9),                      
   numWItmsID numeric(9)                                                     
   ))X                                    
               
                                 
   update OppWarehouseSerializedItem                       
   set numOppItemID=X.numoppitemtCode                                
   from                       
   (SELECT numWarehouseItmsDTLID as DTLID,O.numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/SerialNo',2)                                                                         
   WITH(numWarehouseItmsDTLID numeric(9),numoppitemtCode numeric(9),numWItmsID numeric(9))                                
   join OpportunityItems O on O.numWarehouseItmsID=numWItmsID and O.numOppID=@numOppID)X                 
   where numOppID=@numOppID and numWarehouseItmsDTLID=X.DTLID      
    
--   insert into OpportunityKitItems(numOppKitItem,numChildItem,intQuantity,numWareHouseItemId)    
--   SELECT * FROM OPENXML (@hDocItem,'/NewDataSet/ChildItems',2)                                                                          
--   WITH                        
--   (                                               
--   numoppitemtCode numeric(9),                      
--   numItemCode numeric(9),    
--   numQtyItemsReq integer,  
--   numWarehouseItmsID numeric(9)                                                       
--   )    
      
    
    
    
	EXEC USP_ManageSOWorkOrder @numDomainId,@numOppID,@strItems,@numUserCntID
    
                              
                          
   EXEC sp_xml_removedocument @hDocItem                                     
        end          
   set @TotalAmount=0                                                           
   select @TotalAmount=sum(monTotAmount)from OpportunityItems where numOppId=@numOppID                                                                          
  update OpportunityMaster set  monPamount=@TotalAmount                                                          
  where numOppId=@numOppID                                                 
                      
     
--Insert Tax for Division   

IF @tintOppType=1 OR (@tintOppType=2 and (select isnull(bitPurchaseTaxCredit,0) from Domain where numDomainId=@numDomainId)=1)
BEGIN                   
	INSERT dbo.OpportunityMasterTaxItems (
		numOppId,
		numTaxItemID,
		fltPercentage
	) SELECT @numOppID,TI.numTaxItemID,dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,TI.numTaxItemID,@numOppId,0,NULL) FROM TaxItems TI JOIN DivisionTaxTypes DTT ON TI.numTaxItemID = DTT.numTaxItemID
	 WHERE DTT.numDivisionID=@numDivisionId AND DTT.bitApplicable=1
	   union 
	  select @numOppID,0,dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,0,@numOppId,1,NULL) 
	  FROM dbo.DivisionMaster WHERE bitNoTax=0 AND numDivisionID=@numDivisionID
END
-- Archieve item based on Item's individual setting
--	UPDATE Item SET IsArchieve = CASE WHEN EXISTS(SELECT * FROM dbo.OpportunityItems WHERE OpportunityItems.numItemCode = I.numItemCode) AND ISNULL(I.bitArchiveItem,0) = 1 
--									  THEN 1 
--									  ELSE 0 
--									  END 
--	FROM Item I JOIN dbo.OpportunityItems OI ON I.numItemCode = OI.numItemCode
	
 END
 
 else                                                                                                                          
 BEGIN                  
	--Declaration
	 DECLARE @tempAssignedTo AS NUMERIC(9)                                                    
	 SET @tempAssignedTo = NULL 
	 
	 SELECT @tintOppStatus = tintOppStatus,@tempAssignedTo=isnull(numAssignedTo,0) FROM   OpportunityMaster WHERE  numOppID = @numOppID

	 IF @tempAssignedTo <> @numAssignedTo
	 BEGIN
		IF (SELECT COUNT(*) FROM BizDocComission WHERE ISNULL(numOppID,0) = @numOppID AND ISNULL(bitCommisionPaid,0) = 1) > 0
		BEGIN
			RAISERROR('COMMISSION_PAID_AGAINST_BIZDOC',16,1)
		END
	 END
 
	--Reverting back the warehouse items                  
	 IF @tintOppStatus = 1 
		BEGIN        
		PRINT 'inside revert'          
			EXEC USP_RevertDetailsOpp @numOppID,0,@numUserCntID                  
		END                  
	-- Update Master table
	
	   IF LEN(ISNULL(@vcOppRefOrderNo,''))>0
	   BEGIN
	   		UPDATE dbo.OpportunityBizDocs SET vcRefOrderNo=@vcOppRefOrderNo WHERE numOppId=@numOppID
	   END
		
	   IF EXISTS (SELECT 1 FROM dbo.OpportunityMaster WHERE numOppId = @numOppID AND tintOppType=1 AND isnull(tintOppStatus,0)=1 AND ISNULL(numStatus,0)!= @numStatus)
	   BEGIN
	   		EXEC USP_ManageOpportunityAutomationQueue
					@numOppQueueID = 0, -- numeric(18, 0)
					@numDomainID = @numDomainID, -- numeric(18, 0)
					@numOppId = @numOppID, -- numeric(18, 0)
					@numOppBizDocsId = 0, -- numeric(18, 0)
					@numOrderStatus = @numStatus, -- numeric(18, 0)
					@numUserCntID = @numUserCntID, -- numeric(18, 0)
					@tintProcessStatus = 1, -- tinyint
					@tintMode = 1 -- TINYINT
	   END 
						
	   UPDATE   OpportunityMaster
	   SET      vcPOppName = @vcPOppName,
				txtComments = @Comments,
				bitPublicFlag = @bitPublicFlag,
				numCampainID = @CampaignID,
				tintSource = @tintSource,
				tintSourceType=@tintSourceType,
				intPEstimatedCloseDate = @dtEstimatedCloseDate,
				numModifiedBy = @numUserCntID,
				bintModifiedDate = GETUTCDATE(),
				lngPConclAnalysis = @lngPConclAnalysis,
				monPAmount = @monPAmount,
				tintActive = @tintActive,
				numSalesOrPurType = @numSalesOrPurType,
				numStatus = @numStatus,
				tintOppStatus = @DealStatus,
				vcOppRefOrderNo=@vcOppRefOrderNo,
				--vcWebApiOrderNo = @vcWebApiOrderNo,
				bintOppToOrder=(Case when @tintOppStatus=0 and @DealStatus=1 then GETUTCDATE() else bintOppToOrder end),
				bitDiscountType=@bitDiscountType,fltDiscount=@fltDiscount,bitBillingTerms=@bitBillingTerms,
				intBillingDays=@intBillingDays,bitInterestType=@bitInterestType,fltInterest=@fltInterest,
				tintTaxOperator=@tintTaxOperator,numDiscountAcntType=@numDiscountAcntType,vcCouponCode = @vcCouponCode,
				numPercentageComplete =@numPercentageComplete,bitUseShippersAccountNo = @bitUseShippersAccountNo,
				bitUseMarkupShippingRate = @bitUseMarkupShippingRate,
				numMarkupShippingRate = @numMarkupShippingRate,
				intUsedShippingCompany = @intUsedShippingCompany
	   WHERE    numOppId = @numOppID   
	   
	---Updating if organization is assigned to someone                                                      
	   IF ( @tempAssignedTo <> @numAssignedTo AND @numAssignedTo <> '0' ) 
		BEGIN                                   
			UPDATE  OpportunityMaster SET     numAssignedTo = @numAssignedTo, numAssignedBy = @numUserCntID WHERE   numOppId = @numOppID                                                    
		END                                                     
	   ELSE 
	   IF ( @numAssignedTo = 0 ) 
		BEGIN                
			UPDATE  OpportunityMaster SET     numAssignedTo = 0, numAssignedBy = 0 WHERE   numOppId = @numOppID
		END                                                                      
		---- Updating Opp Items
		if convert(varchar(10),@strItems) <>'' AND @numOppID>0                                                                         
		begin
			   EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems                                      
			   --Delete Items
			   delete from OpportunityKitItems where numOppID=@numOppID and numOppItemID not in                       
			   (SELECT numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/Item',2)                                                                          
			   WITH  (numoppitemtCode numeric(9)))                      
                                        
--               ---- ADDED BY Manish Anjara : Jun 26,2013 - Archive item based on Item's individual setting
--               UPDATE Item SET IsArchieve = CASE WHEN EXISTS(SELECT * FROM dbo.OpportunityItems WHERE OpportunityItems.numItemCode = I.numItemCode) AND ISNULL(I.bitArchiveItem,0) = 1 
--												 THEN 1 
--												 ELSE 0 
--												 END 
--			   FROM Item I JOIN dbo.OpportunityItems OI ON I.numItemCode = OI.numItemCode
--					 	
--			   UPDATE Item SET IsArchieve = CASE WHEN ISNULL(I.bitArchiveItem,0) = 1 THEN 0 ELSE 1 END
--			   FROM Item I
--			   JOIN OpportunityItems OI ON I.numItemCode = OI.numItemCode
--			   WHERE numOppID = @numOppID and OI.numItemCode NOT IN (SELECT numItemCode FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) 
--																	 WITH (numItemCode numeric(9)))

			   delete from OpportunityItems where numOppID=@numOppID and numoppitemtCode not in                       
			   (SELECT numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/Item',2)                                                                          
			   WITH  (numoppitemtCode numeric(9)))   
	                                
			   insert into OpportunityItems                                                                          
			   (numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,[vcItemName],[vcModelID],[vcManufacturer],vcPathForTImage,monVendorCost,numUOMId,bitWorkOrder,numVendorWareHouse,numToWarehouseItemID,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,vcAttributes,numClassID,monAvgCost,bitItemPriceApprovalRequired)
			   select @numOppID,X.numItemCode,dbo.fn_UOMConversion(X.numUOM,numItemCode,@numDomainId,null) * x.numUnitHour,x.monPrice,x.monTotAmount,X.vcItemDesc,X.numWarehouseItmsID,X.ItemType,X.DropShip,X.bitDiscountType,X.fltDiscount,X.monTotAmtBefDiscount,X.vcItemName,(SELECT vcModelID FROM item WHERE numItemCode = X.numItemCode),(SELECT vcManufacturer FROM item WHERE numItemCode = X.numItemCode),(SELECT TOP 1 vcPathForTImage FROM dbo.ItemImages WHERE numItemCode = X.numItemCode AND BitDefault = 1 AND numDomainId = @numDomainId),(select isnull(VN.monCost,0) from Vendor VN, Item IT where IT.numItemCode=VN.numItemCode and  VN.numItemCode=X.numItemCode and  VN.numVendorID=IT.numVendorID),X.numUOM,X.bitWorkOrder,
			   X.numVendorWareHouse,X.numToWarehouseItemID,X.numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,Attributes,Case When @tintDefaultClassType=0 then @numDefaultClassID else (SELECT numItemClass FROM item WHERE numItemCode = X.numItemCode) END,
			   (SELECT ISNULL(monAverageCost,0) FROM Item I WHERE I.numDomainId=@numDomainId AND I.numItemCode=X.numItemCode), X.bitItemPriceApprovalRequired from(
			   SELECT *FROM OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
			   WITH  (                      
				numoppitemtCode numeric(9),                                     
				numItemCode numeric(9),                                                                          
				numUnitHour numeric(9,2),                                                                          
				monPrice money,                                                                       
				monTotAmount money,                                                                          
				vcItemDesc varchar(1000),                                    
				numWarehouseItmsID numeric(9),
				numToWarehouseItemID numeric(9),                   
				Op_Flag tinyint,                            
				ItemType varchar(30),      
				DropShip bit,
				bitDiscountType bit,
				fltDiscount float,
				monTotAmtBefDiscount MONEY,
				vcItemName VARCHAR(300),numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId numeric(9),numProjectID numeric(9),numProjectStageID numeric(9),Attributes varchar(200), bitItemPriceApprovalRequired BIT
				 ))X                                     
				--Update items                 
			   Update OpportunityItems                       
			   set numItemCode=X.numItemCode,    
			   numOppId=@numOppID,                       
			   numUnitHour=dbo.fn_UOMConversion(X.numUOM,X.numItemCode,@numDomainId,null) * x.numUnitHour,                      
			   monPrice=x.monPrice,                      
			   monTotAmount=x.monTotAmount,                                  
			   vcItemDesc=X.vcItemDesc,                      
			   numWarehouseItmsID=X.numWarehouseItmsID,
			   numToWarehouseItemID = X.numToWarehouseItemID,             
			   bitDropShip=X.DropShip,
			   bitDiscountType=X.bitDiscountType,
			   fltDiscount=X.fltDiscount,
			   monTotAmtBefDiscount=X.monTotAmtBefDiscount,vcItemName=X.vcItemName,numUOMId=numUOM,
			   numVendorWareHouse=X.numVendorWareHouse,numShipmentMethod=X.numShipmentMethod,numSOVendorId=X.numSOVendorId,numProjectId=X.numProjectID,numProjectStageId=X.numProjectStageID,vcAttributes=X.Attributes,bitItemPriceApprovalRequired=X.bitItemPriceApprovalRequired,bitWorkOrder=X.bitWorkOrder
			   from (SELECT numoppitemtCode as numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,numToWarehouseItemID,DropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,vcItemName,numUOM,numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,Attributes,bitItemPriceApprovalRequired,bitWorkOrder  FROM OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=2]',2)                                                                
			   WITH  (                      
				numoppitemtCode numeric(9),                                     
				numItemCode numeric(9),                                                                          
				numUnitHour numeric(9,2),                                                                          
				monPrice money,                                         
				monTotAmount money,                                                                          
				vcItemDesc varchar(1000),                                    
				numWarehouseItmsID numeric(9), 
				numToWarehouseItemID NUMERIC(18,0),     
				DropShip bit,
				bitDiscountType bit,
				fltDiscount float,
				monTotAmtBefDiscount MONEY,vcItemName varchar(300),numUOM NUMERIC(9),numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId numeric(9),numProjectID numeric(9),numProjectStageID numeric(9),Attributes varchar(200),bitItemPriceApprovalRequired BIT, bitWorkOrder BIT                                              
			   ))X where numoppitemtCode=X.numOppItemID                           
			                                    
			   EXEC USP_ManageSOWorkOrder @numDomainId,@numOppID,@strItems,@numUserCntID
			   		                                                                      
			   EXEC sp_xml_removedocument @hDocItem                                               
		end     

		--FOLLOWING DELETE STATEMENT IS COMMENTED BECUASE NOW WE ARE NOT ALLOWING USER TO EIDT SERIAL/LOT ITEM UNTILL USER REMOVED ASSIGNED SERIAL/LOT NUMBERS
	    --Delete previously added serial lot number added for opportunity
		--DELETE FROM OppWarehouseSerializedItem WHERE numOppID = @numOppID                                                         
END

----------------generic section will be called in both insert and update ---------------------------------
-- Archieve item based on Item's individual setting
--	UPDATE Item SET IsArchieve = CASE WHEN EXISTS(SELECT * FROM dbo.OpportunityItems WHERE OpportunityItems.numItemCode = I.numItemCode) AND ISNULL(I.bitArchiveItem,0) = 1 
--									  THEN 1 
--									  ELSE 0 
--									  END 
--	FROM Item I JOIN dbo.OpportunityItems OI ON I.numItemCode = OI.numItemCode
	
if convert(varchar(10),@strItems) <>'' AND @numOppID>0                                                                        
  begin 
EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems

--delete from OpportunityKitItems where numOppID=@numOppID and numOppItemID 
--	not in (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppID=@numOppID)                      
		
--Update Kit Items                 
Update OKI set numQtyItemsReq=numQtyItemsReq_Orig * OI.numUnitHour
			   FROM OpportunityKitItems OKI JOIN OpportunityItems OI ON OKI.numOppItemID=OI.numoppitemtCode and OKI.numOppId=OI.numOppId  
			   WHERE OI.numOppId=@numOppId 

--Insert Kit Items                 
INSERT into OpportunityKitItems                                                                          
		(numOppId,numOppItemID,numChildItemID,numWareHouseItemId,numQtyItemsReq,numQtyItemsReq_Orig,numUOMId,numQtyShipped)
  SELECT @numOppId,OI.numoppitemtCode,ID.numChildItemID,ID.numWareHouseItemId,ID.numQtyItemsReq * OI.numUnitHour,ID.numQtyItemsReq,ID.numUOMId,0
	 FROM OpportunityItems OI JOIN ItemDetails ID ON OI.numItemCode=ID.numItemKitID WHERE 
	OI.numOppId=@numOppId AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityKitItems OKI WHERE OKI.numOppId=@numOppID)
--	OI.numoppitemtCode NOT IN (SELECT numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=2]',2)                                                                
--			   WITH  (                      
--				numoppitemtCode numeric(9))X)
			   
EXEC sp_xml_removedocument @hDocItem  
END

/*Added by chintan - While creating SO from PO it wasn't puting Item Qty to Allocation. this segment was being called from USP_OppSaveMilestone which No longer in use after changes anoop did. */     
--Updating the warehouse items              
declare @tintShipped as tinyint               
select @tintOppStatus=tintOppStatus,@tintOppType=tintOppType,@tintShipped=tintShipped from OpportunityMaster where numOppID=@numOppID              
if @tintOppStatus=1               
	begin         
	PRINT 'inside USP_UpdatingInventoryonCloseDeal'
		exec USP_UpdatingInventoryonCloseDeal @numOppID,@numUserCntID
	end              
            
if @tintOppType=1              
begin              
	if @tintOppStatus=1 Update WareHouseItmsDTL set tintStatus=1 where numWareHouseItmsDTLID in (select numWarehouseItmsDTLID from OppWarehouseSerializedItem where numOppID=@numOppID)
	if @tintShipped=1 Update WareHouseItmsDTL set tintStatus=2 where numWareHouseItmsDTLID in (select numWarehouseItmsDTLID from OppWarehouseSerializedItem where numOppID=@numOppID)              
END


declare @tintCRMType as numeric(9)        
declare @AccountClosingDate as datetime        

select @numDivisionID=numDivisionID,@AccountClosingDate=bintAccountClosingDate from  OpportunityMaster where numOppID=@numOppID         
select @tintCRMType=tintCRMType from divisionmaster where numDivisionID =@numDivisionID 

--When deal is won                                         
if @DealStatus = 1 
begin           
	 if @AccountClosingDate is null                   
	  update OpportunityMaster set bintAccountClosingDate=getutcdate() where numOppID=@numOppID              
end         
--When Deal is Lost
else if @DealStatus = 2
begin         
	 select @AccountClosingDate=bintAccountClosingDate from  OpportunityMaster where numOppID=@numOppID         
	 if @AccountClosingDate is null                   
	  update OpportunityMaster set bintAccountClosingDate=getutcdate() where numOppID=@numOppID
end                    

/*Added by chintan -  Biz usually automatically promotes the record from Lead/Prospect to Account When a Sales Opp/Order is made against it*/  
-- Promote Lead to Prospect when Sales/Purchase Opp is created against it
if @tintCRMType=0 AND @DealStatus = 0 --Lead & Open Opp
begin        
	update divisionmaster set tintCRMType=1,bintLeadProm=getutcdate(),bintLeadPromBy=@numUserCntID,bintProsProm=getutcdate(),bintProsPromBy=@numUserCntID        
	where numDivisionID=@numDivisionID        
end        
-- Promote Lead to Account when Sales/Purchase Order is created against it
ELSE if @tintCRMType=0 AND @DealStatus = 1 --Lead & Order
begin        
	update divisionmaster set tintCRMType=2,bintLeadProm=getutcdate(),bintLeadPromBy=@numUserCntID,bintProsProm=getutcdate(),bintProsPromBy=@numUserCntID        
	where numDivisionID=@numDivisionID        
end
--Promote Prospect to Account
else if @tintCRMType=1 AND @DealStatus = 1 
begin        
	update divisionmaster set tintCRMType=2,bintProsProm=getutcdate(),bintProsPromBy=@numUserCntID        
	where numDivisionID=@numDivisionID        
end

--Add/Update Address Details
DECLARE @vcStreet  varchar(100),@vcCity  varchar(50),@vcPostalCode  varchar(15),@vcCompanyName  varchar(100),@vcAddressName VARCHAR(50)      
DECLARE @numState  numeric(9),@numCountry  numeric(9), @numCompanyId  numeric(9) 
DECLARE @bitIsPrimary BIT;

select  @vcCompanyName=vcCompanyName,@numCompanyId=div.numCompanyID from CompanyInfo Com                            
join divisionMaster Div                            
on div.numCompanyID=com.numCompanyID                            
where div.numdivisionID=@numDivisionId

--Bill Address
IF @numBillAddressId>0
BEGIN
 SELECT  @vcStreet=ISNULL(vcStreet,''),@vcCity=ISNULL(vcCity,''),@vcPostalCode=ISNULL(vcPostalCode,''),
		@numState=ISNULL(numState,0),@numCountry=ISNULL(numCountry,0),@bitIsPrimary=bitIsPrimary,
		@vcAddressName=vcAddressName
            FROM  dbo.AddressDetails WHERE   numDomainID = @numDomainID AND numAddressID = @numBillAddressId
  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 0, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = @vcCompanyName, --  varchar(100)
	@numCompanyId = 0, --  numeric(9, 0)
	@vcAddressName = @vcAddressName
END
  
  
  --Ship Address
IF @numShipAddressId>0
BEGIN
	SELECT  @vcStreet=ISNULL(vcStreet,''),@vcCity=ISNULL(vcCity,''),@vcPostalCode=ISNULL(vcPostalCode,''),
		@numState=ISNULL(numState,0),@numCountry=ISNULL(numCountry,0),@bitIsPrimary=bitIsPrimary,
		@vcAddressName=vcAddressName
            FROM  dbo.AddressDetails WHERE   numDomainID = @numDomainID AND numAddressID = @numShipAddressId
 



  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 1, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = @vcCompanyName, --  varchar(100)
	@numCompanyId =@numCompanyId, --  numeric(9, 0)
	@vcAddressName = @vcAddressName
END

--Delete Tax for Opportunity Items if item deleted 
DELETE FROM OpportunityItemsTaxItems WHERE numOppId=@numOppId AND numOppItemID NOT IN (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID)

--Insert Tax for Opportunity Items
INSERT INTO dbo.OpportunityItemsTaxItems (
	numOppId,
	numOppItemID,
	numTaxItemID
) SELECT @numOppId,OI.numoppitemtCode,TI.numTaxItemID FROM dbo.OpportunityItems OI JOIN dbo.ItemTax IT ON OI.numItemCode=IT.numItemCode JOIN
TaxItems TI ON TI.numTaxItemID=IT.numTaxItemID WHERE OI.numOppId=@numOppID AND IT.bitApplicable=1 AND 
OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
UNION
  select @numOppId,OI.numoppitemtCode,0 FROM dbo.OpportunityItems OI JOIN dbo.Item I ON OI.numItemCode=I.numItemCode
   WHERE OI.numOppId=@numOppID  AND I.bitTaxable=1 AND
OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)

  
 UPDATE OpportunityMaster SET monDealAmount=dbo.GetDealAmount(@numOppID,getdate(),0) WHERE numOppId=@numOppID
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SET @numOppID = 0

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityMaster_CreatePO')
DROP PROCEDURE dbo.USP_OpportunityMaster_CreatePO
GO
CREATE PROCEDURE [dbo].[USP_OpportunityMaster_CreatePO]
(
	@numOppID NUMERIC(18,0) OUTPUT,
	@numDomainID NUMERIC(18,0),
	@numUserCntID NUMERIC(18,0),
	@numItemCode NUMERIC(18,0),
	@numUnitHour NUMERIC(18,0),
	@numWarehouseItemID NUMERIC(18,0),
	@bitFromWorkOrder BIT,
	@tintOppStatus TINYINT,
	@numOrderStatus AS NUMERIC(18,0) = 0
)
AS 
BEGIN
    DECLARE @ErrorMessage NVARCHAR(4000);SET @ErrorMessage=''
    DECLARE @ErrorSeverity INT;
    DECLARE @ErrorState INT;
    
	BEGIN TRY
	BEGIN TRAN
		DECLARE @tintMinOrderQty AS INTEGER
		DECLARE @numUnitPrice MONEY
		DECLARE @numContactID NUMERIC(18,0)
	    DECLARE @numDivisionID NUMERIC(18,0)
		DECLARE @numVendorID NUMERIC(18,0)
		DECLARE @fltExchangeRate float    
		DECLARE @numCurrencyID NUMERIC(18,0)
		DECLARE @vcPOppName VARCHAR(1000) = ''                         
		DECLARE @hDocItem int                                                                                                                            
		DECLARE @TotalAmount as money                                                                                     
		DECLARE @tintDefaultClassType NUMERIC
		DECLARE @numDefaultClassID NUMERIC;SET @numDefaultClassID=0

		SELECT @numDivisionID = numVendorID FROM Item WHERE numDomainID=@numDomainID AND numItemCode=@numItemCode

		IF ISNULL(@numDivisionID,0) > 0
		BEGIN
			SELECT @numContactID = numContactId FROM AdditionalContactsInformation WHERE numDivisionId = @numDivisionID AND bitPrimaryContact = 1

			IF @numContactID IS NULL
			BEGIN
				SELECT TOP 1 @numContactID = numContactId FROM AdditionalContactsInformation WHERE numDivisionId = @numDivisionID
			END

			SELECT @tintMinOrderQty=ISNULL(intMinQty,0) FROm Vendor WHERE numVendorID = @numDivisionID AND numItemCode = @numItemCode AND numDomainID=@numDomainID
			SET @numUnitHour = @numUnitHour + @tintMinOrderQty
			SET @numUnitHour = IIF(@numUnitHour = 0, 1, @numUnitHour)

			SELECT @numUnitPrice = dbo.fn_FindVendorCost(@numItemCOde,@numDivisionID,@numDomainID,@numUnitHour)

			--Get Default Item Class for particular user based on Domain settings  
			SELECT @tintDefaultClassType=isnull(tintDefaultClassType,0) FROM dbo.Domain  WHERE numDomainID = @numDomainID 

			IF @tintDefaultClassType=0
				SELECT @numDefaultClassID=NULLIF(numDefaultClass,0) FROM dbo.UserMaster  WHERE numUserDetailId =@numUserCntID AND numDomainID = @numDomainID 
		
			DECLARE @intOppTcode AS NUMERIC(9)
			SELECT @intOppTcode=max(numOppId) FROM OpportunityMaster                
			SET @intOppTcode =isnull(@intOppTcode,0) + 1                                                
			SET @vcPOppName= @vcPOppName + '-' + convert(varchar(4),year(getutcdate()))  + '-A' + convert(varchar(10),@intOppTcode)                                  
  
			SET @fltExchangeRate=1
			SELECT @numCurrencyID= ISNULL(numCurrencyID,0) FROM dbo.Domain WHERE numDomainId=@numDomainId                                                                       
  
			--Set Default Class If enable User Level Class Accountng 
			DECLARE @numAccountClass AS NUMERIC(18);
			SET @numAccountClass=0
		
			SELECT 
				@numAccountClass=ISNULL(numDefaultClass,0) 
			FROM 
				dbo.UserMaster UM 
			JOIN 
				dbo.Domain D 
			ON 
				UM.numDomainID=D.numDomainId
			WHERE 
				D.numDomainId=@numDomainId 
				AND UM.numUserDetailId=@numUserCntID 
				AND ISNULL(D.IsEnableUserLevelClassTracking,0) = 1
                                                                       
			INSERT INTO OpportunityMaster                                                                          
			(                                                                             
				numContactId,numDivisionId,bitPublicFlag,tintSource,
				tintSourceType,vcPOppName,intPEstimatedCloseDate,numCreatedBy,bintCreatedDate,                                                                           
				numDomainId,numRecOwner,lngPConclAnalysis,tintOppType,                                                              
				numSalesOrPurType,numCurrencyID,fltExchangeRate,[tintOppStatus],numStatus,
				vcOppRefOrderNo,bitStockTransfer,bitBillingTerms,intBillingDays,bitInterestType,fltInterest,vcCouponCode,
				bitDiscountType,fltDiscount,bitPPVariance,numAccountClass,tintTaxOperator,bitUseShippersAccountNo,bitUseMarkupShippingRate,
				numMarkupShippingRate,intUsedShippingCompany,bitFromWorkOrder
			)                                                                          
			Values                                                                          
			(                                                                          
				@numContactId,@numDivisionId,0,0,1,@vcPOppName,DATEADD(D,1,GETUTCDATE()),@numUserCntID,GETUTCDATE(),
				@numDomainId,@numUserCntID,0,2,0,@numCurrencyID,@fltExchangeRate,@tintOppStatus,
				@numOrderStatus,NULL,0,0,0,0,0,'',0,0,1,@numAccountClass,0,0,0,0.00,0,@bitFromWorkOrder
			)                                                                                                                      
  
			set @numOppID=scope_identity()                                                
  
			--UPDATE OPPNAME AS PER NAME TEMPLATE
			EXEC dbo.USP_UpdateNameTemplateValue 2,@numDomainID,@numOppID

			EXEC dbo.USP_AddParentChildCustomFieldMap
			@numDomainID = @numDomainID,
			@numRecordID = @numOppID,
			@numParentRecId = @numDivisionId,
			@tintPageID = 6
 	
			--INSERTING ITEMS                                       
			IF @numOppID>0
			BEGIN                      

				INSERT INTO OpportunityItems                                                                          
				(
					numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,
					vcType,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,vcItemName,vcModelID,
					vcManufacturer,vcPathForTImage,monVendorCost,numUOMId,bitWorkOrder,numVendorWareHouse,
					numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,numClassID,numToWarehouseItemID,
					vcAttributes,monAvgCost,bitItemPriceApprovalRequired
				)
				SELECT 
					@numOppID,@numItemCode,@numUnitHour,@numUnitPrice,(@numUnitPrice * @numUnitHour),Item.txtItemDesc,@numWarehouseItemID,
					'',0,0,0,(@numUnitPrice * @numUnitHour),Item.vcItemName,Item.vcModelID,Item.vcManufacturer,
					(SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = Item.numItemCode AND BitDefault = 1 AND numdomainId = @numdomainId),
					(select isnull(VN.monCost,0) from Vendor VN, Item IT where IT.numItemCode=VN.numItemCode and  VN.numItemCode=Item.numItemCode and VN.numVendorID=IT.numVendorID),
					Item.numBaseUnit,0,NULL,NULL,NULL,NULL,NULL,
					Case When @tintDefaultClassType=0 then @numDefaultClassID else (SELECT numItemClass FROM item WHERE numItemCode = Item.numItemCode) end,
					NULL,'', (SELECT ISNULL(monAverageCost,0) FROM Item I WHERE I.numDomainId=@numDomainId AND I.numItemCode=Item.numItemCode),0
				FROM
					Item
				WHERE
					numItemCode = @numItemCode
			END          
	
			SET @TotalAmount=0                                                           
			SELECT @TotalAmount=sum(monTotAmount) FROM OpportunityItems WHERE numOppId=@numOppID                                                                          
			UPDATE OpportunityMaster SET monPamount=@TotalAmount WHERE numOppId=@numOppID                                                 
                      
			--Insert Tax for Division   
			IF (select isnull(bitPurchaseTaxCredit,0) from Domain where numDomainId=@numDomainId)=1
			BEGIN 
				INSERT dbo.OpportunityMasterTaxItems 
				(
					numOppId,
					numTaxItemID,
					fltPercentage
				) 
				SELECT 
					@numOppID,
					TI.numTaxItemID,
					dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,TI.numTaxItemID,@numOppId,0,NULL) 
				FROM 
					TaxItems TI 
				JOIN 
					DivisionTaxTypes DTT ON TI.numTaxItemID = DTT.numTaxItemID
				WHERE 
					DTT.numDivisionID=@numDivisionId AND DTT.bitApplicable=1
				UNION 
				SELECT
					@numOppID,
					0,
					dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,0,@numOppId,1,NULL) 
				FROM 
					dbo.DivisionMaster 
				WHERE 
					bitNoTax=0 AND 
					numDivisionID=@numDivisionID
			END

			/*Added by chintan - While creating SO from PO it wasn't puting Item Qty to Allocation. this segment was being called from USP_OppSaveMilestone which No longer in use after changes anoop did. */     
			--Updating the warehouse items              
			DECLARE @tintShipped AS TINYINT               
			DECLARE @tintOppType AS TINYINT
			DECLARE @DealStatus AS TINYINT
			SELECT 
				@tintOppStatus=tintOppStatus,
				@tintOppType=tintOppType,
				@tintShipped=tintShipped
			FROM 
				OpportunityMaster 
			WHERE 
				numOppID=@numOppID              
		
			IF @tintOppStatus=1               
			BEGIN         
				EXEC USP_UpdatingInventoryonCloseDeal @numOppID,@numUserCntID
			END

			DECLARE @tintCRMType as numeric(9)        
			DECLARE @AccountClosingDate as datetime        

			select @numDivisionID=numDivisionID,@AccountClosingDate=bintAccountClosingDate from  OpportunityMaster where numOppID=@numOppID         
			select @tintCRMType=tintCRMType from divisionmaster where numDivisionID =@numDivisionID 

			IF @AccountClosingDate is null                   
			UPDATE OpportunityMaster set bintAccountClosingDate=getutcdate() where numOppID=@numOppID              
               
			/*Added by chintan -  Biz usually automatically promotes the record from Lead/Prospect to Account When a Sales Opp/Order is made against it*/  
			-- Promote Lead to Prospect when Sales/Purchase Opp is created against it
			IF @tintCRMType=0 AND @tintOppStatus = 0 --Lead & Open Opp
			BEGIN
				UPDATE divisionmaster set tintCRMType=1,bintLeadProm=getutcdate(),bintLeadPromBy=@numUserCntID,bintProsProm=getutcdate(),bintProsPromBy=@numUserCntID        
				WHERE numDivisionID=@numDivisionID        
			END        
			-- Promote Lead to Account when Sales/Purchase Order is created against it
			ELSE if @tintCRMType=0 AND @tintOppStatus = 1 --Lead & Order
			BEGIN 
				UPDATE divisionmaster set tintCRMType=2,bintLeadProm=getutcdate(),bintLeadPromBy=@numUserCntID,bintProsProm=getutcdate(),bintProsPromBy=@numUserCntID        
				WHERE numDivisionID=@numDivisionID        
			END
			--Promote Prospect to Account
			ELSE IF @tintCRMType=1 AND @tintOppStatus = 1 
			BEGIN        
				update divisionmaster set tintCRMType=2,bintProsProm=getutcdate(),bintProsPromBy=@numUserCntID        
				where numDivisionID=@numDivisionID        
			END

			--Add/Update Address Details
			DECLARE @vcStreet  varchar(100),@vcCity  varchar(50),@vcPostalCode  varchar(15),@vcCompanyName  varchar(100),@vcAddressName VARCHAR(50)      
			DECLARE @numState  numeric(9),@numCountry  numeric(9), @numCompanyId  numeric(9) 
			DECLARE @bitIsPrimary BIT;

			SELECT  @vcCompanyName=vcCompanyName,@numCompanyId=div.numCompanyID FROM CompanyInfo Com                            
			JOIN divisionMaster Div                            
			ON div.numCompanyID=com.numCompanyID                            
			WHERE div.numdivisionID=@numDivisionId

			--INSERT TAX FOR OPPORTUNITY ITEMS
			INSERT INTO dbo.OpportunityItemsTaxItems 
			(
				numOppId,
				numOppItemID,
				numTaxItemID
			) 
			SELECT 
				@numOppId,OI.numoppitemtCode,TI.numTaxItemID 
			FROM 
				dbo.OpportunityItems OI 
			JOIN dbo.ItemTax IT ON OI.numItemCode=IT.numItemCode 
			JOIN TaxItems TI ON TI.numTaxItemID=IT.numTaxItemID 
			WHERE 
				OI.numOppId=@numOppID 
				AND IT.bitApplicable=1 
				AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
			UNION
			SELECT 
				@numOppId,OI.numoppitemtCode,0 
			FROM 
				dbo.OpportunityItems OI 
			JOIN dbo.Item I ON OI.numItemCode=I.numItemCode
			WHERE 
				OI.numOppId=@numOppID  
				AND I.bitTaxable=1 
				AND	OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)

			UPDATE OpportunityMaster SET monDealAmount=dbo.GetDealAmount(@numOppID,getdate(),0) WHERE numOppId=@numOppID
		END
	COMMIT TRAN 
	END TRY
	BEGIN CATCH
		IF ( @@TRANCOUNT > 0 ) 
		BEGIN
			ROLLBACK TRAN
		END

		SELECT @ErrorMessage = ERROR_MESSAGE(),@ErrorSeverity = ERROR_SEVERITY(),@ErrorState = ERROR_STATE()
		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState);
	END CATCH;
END

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_SimpleSearch')
DROP PROCEDURE USP_SimpleSearch
GO
CREATE PROCEDURE [dbo].[USP_SimpleSearch]                             
	@numDomainID NUMERIC (18,0),
	@numUserCntID NUMERIC(18,0),
	@searchText VARCHAR(100),
	@searchType VARCHAR(1),
	@searchCriteria VARCHAR(1),
	@orderType VARCHAR(1),
	@bizDocType AS INT ,
	@numSkip AS INT                                                                   
AS                          
	DECLARE @strSQL AS VARCHAR(MAX) = ''
	
	IF @searchType = '1' -- Organization
	BEGIN
		IF @searchCriteria = '1' --Organizations
		BEGIN
			EXEC USP_CompanyInfo_Search @numDomainID=@numDomainID,@numUserCntID=@numUserCntID,@isStartWithSearch=0,@searchText=@searchText
		END
		ELSE
		BEGIN
			--GET FIELDS CONFIGURED FOR ORGANIZATION SEARCH
			SELECT * INTO #tempOrgSearch FROM
			(
				SELECT 
					numFieldId,
					vcDbColumnName,
					ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName,
					vcLookBackTableName,
					tintRow AS tintOrder,
					0 as Custom
				FROM 
					View_DynamicColumns
				WHERE 
					numFormId=97 AND numUserCntID=@numUserCntID AND numDomainID=@numDomainID AND 
					tintPageType=1 AND bitCustom=0  AND ISNULL(bitSettingField,0)=1 AND numRelCntType=0
				UNION   
				SELECT 
					numFieldId,
					vcFieldName,
					vcFieldName,
					'CFW_FLD_Values',
					tintRow AS tintOrder,
					1 as Custom
				FROM 
					View_DynamicCustomColumns   
				WHERE 
					Grp_id=1 AND numFormId=97 AND numUserCntID=@numUserCntID AND numDomainID=@numDomainID AND 
					tintPageType=1 AND bitCustom=1 AND numRelCntType=0
			)Y
			
			DECLARE @searchSQL AS VARCHAR(MAX) = ''

			IF (SELECT COUNT(*) FROM #tempOrgSearch) > 0
			BEGIN
				SELECT  
					@searchSQL = STUFF(
										(SELECT ' OR ' + + CONCAT((CASE WHEN vcLookBackTableName = 'AddressDetails' THEN '' ELSE CONCAT(vcLookBackTableName,'.') END),IIF(Custom = 1,'Fld_Value',vcDbColumnName)) + ' LIKE ''%' + @searchText + '%''' FROM #tempOrgSearch FOR XML PATH(''))
										, 1
										, 4
										, ''
										)
			END
			ELSE
			BEGIN
				SET @searchSQL = 'CompanyInfo.vcCompanyName LIKE ''%' + @searchText + '%'''
			END

			CREATE TABLE #TEMPOrganization
			(
				numDivisionID NUMERIC(18,0),
				vcCompanyName VARCHAR(500)
			)


			SET @strSQL = 'INSERT INTO 
								#TEMPOrganization
							SELECT
								DivisionMaster.numDivisionID
								,CompanyInfo.vcCompanyName
							FROM
								DivisionMaster 
							INNER JOIN
								CompanyInfo 
							ON
								CompanyInfo.numCompanyId = DivisionMaster.numCompanyID
							LEFT JOIN
								AdditionalContactsInformation
							ON
								AdditionalContactsInformation.numDivisionId = DivisionMaster.numDivisionID
							LEFT JOIN
								OpportunityMaster
							ON
								OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
							LEFT JOIN
								Cases
							ON
								Cases.numDivisionID = DivisionMaster.numDivisionID
							LEFT JOIN
								ProjectsMaster
							ON
								ProjectsMaster.numDivisionId = DivisionMaster.numDivisionID
							OUTER APPLY 
								(
									SELECT
										isnull(vcStreet,'''') as vcBillStreet, 
										isnull(vcCity,'''') as vcBillCity, 
										isnull(dbo.fn_GetState(numState),'''') as numBillState,
										isnull(vcPostalCode,'''') as vcBillPostCode,
										isnull(dbo.fn_GetListName(numCountry,0),'''')  as numBillCountry
									FROM
										dbo.AddressDetails 
									WHERE 
										numDomainID = ' + CAST(@numDomainID AS VARCHAR) + '
										AND numRecordID=DivisionMaster.numDivisionID 
										AND tintAddressOf=2 
										AND tintAddressType=1 
								)  AD1
							OUTER APPLY
								(
									SELECT
										isnull(vcStreet,'''') as vcShipStreet,
										isnull(vcCity,'''') as vcShipCity,
										isnull(dbo.fn_GetState(numState),'''')  as numShipState,
										isnull(vcPostalCode,'''') as vcShipPostCode, 
										isnull(dbo.fn_GetListName(numCountry,0),'''') numShipCountry
									FROM
										dbo.AddressDetails 
									WHERE 
										numDomainID = ' + CAST(@numDomainID AS VARCHAR) + '
										AND numRecordID= DivisionMaster.numDivisionID 
										AND tintAddressOf=2 
										AND tintAddressType=2	
								) AD2	
							LEFT JOIN
								CFW_FLD_Values
							ON
								CFW_FLD_Values.RecId = DivisionMaster.numDivisionID
								AND CFW_FLD_Values.Fld_ID IN (SELECT numFieldID FROM #tempOrgSearch)
							WHERE
								DivisionMaster.numDomainID = ' + CAST(@numDomainID AS VARCHAR) + '
								AND (' + @searchSQL + ')
							GROUP BY
								DivisionMaster.numDivisionID
								,CompanyInfo.vcCompanyName'

			PRINT @strSQL
			EXEC (@strSQL)
			IF @searchCriteria = '2' --Items
			BEGIN
				DECLARE @TEMPORGITEM TABLE
				(
					numOppId NUMERIC(18,0),
					CreatedDate DATETIME,
					vcItemName VARCHAR(500),
					vcPOppName VARCHAR(200),
					vcOppType VARCHAR(50),
					numUnitHour INT,
					monPrice NUMERIC(18,2),
					vcCompanyName  VARCHAR(200),
					tintType TINYINT
				)

				INSERT INTO @TEMPORGITEM
				(
					numOppId,
					CreatedDate,
					vcItemName,
					vcPOppName,
					vcOppType,
					numUnitHour,
					monPrice,
					vcCompanyName,
					tintType
				)
				SELECT
					numOppId,
					bintCreatedDate,
					vcItemName,
					vcPOppName,
					vcOppType,
					CAST(numUnitHour AS INT) numUnitHour,
					CAST(monPrice AS decimal(18,2)) monPrice,
					vcCompanyName,
					tintType
				FROM
				(
					SELECT
						OpportunityMaster.numOppId,
						OpportunityMaster.bintCreatedDate,
						CONCAT(Item.vcItemName,',',Item.vcModelID) AS vcItemName,
						OpportunityMaster.vcPOppName,
						CASE 
							WHEN tintOppType = 1 THEN 'Sales ' + (CASE WHEN tintOppStatus = 1 THEN 'Order' ELSE 'Opportunity' END)
							WHEN tintOppType = 2 THEN 'Purchase ' + (CASE WHEN tintOppStatus = 1 THEN 'Order' ELSE 'Opportunity' END)
						END vcOppType,
						OpportunityItems.numUnitHour,
						OpportunityItems.monPrice,
						TEMP.vcCompanyName,
						1 AS tintType
					FROM
						OpportunityMaster
					INNER JOIN
						#TEMPOrganization TEMP
					ON
						OpportunityMaster.numDivisionId = TEMP.numDivisionID
					INNER JOIN
						OpportunityItems
					ON
						OpportunityMaster.numOppId = OpportunityItems.numOppId
					INNER JOIN
						Item
					ON
						OpportunityItems.numItemCode = Item.numItemCode
					WHERE
						OpportunityMaster.numDomainId = @numDomainID
					UNION
					SELECT
						ReturnHeader.numReturnHeaderID AS numOppID,
						ReturnHeader.dtCreatedDate AS bintCreatedDate,
						CONCAT(Item.vcItemName,',',Item.vcModelID) AS vcItemName,
						ReturnHeader.vcRMA AS vcPOppName,
						CASE 
							WHEN tintReturnType = 1 THEN 'Sales Return'
							WHEN tintReturnType = 2 THEN 'Purchase Return'
						END vcOppType,
						ReturnItems.numUnitHour,
						ReturnItems.monPrice,
						TEMP.vcCompanyName,
						2 AS tintType
					FROM
						ReturnHeader
					INNER JOIN
						#TEMPOrganization TEMP
					ON
						ReturnHeader.numDivisionId = TEMP.numDivisionID
					INNER JOIN	
						ReturnItems
					ON
						ReturnHeader.numReturnHeaderID = ReturnItems.numReturnHeaderID
					INNER JOIN
						Item
					ON
						Item.numItemCode = ReturnItems.numItemCode
					WHERE
						ReturnHeader.numDomainId = @numDomainID AND tintReturnType IN (1,2)
				) TEMPFinal

				SELECT  
					ISNULL(numOppId,0) AS numOppId,
					ISNULL(dbo.FormatedDateFromDate(CreatedDate,@numDomainID),'') bintCreatedDate,
					ISNULL(vcItemName,'') vcItemName,
					ISNULL(vcPOppName,'') vcPOppName,
					ISNULL(vcOppType,'') vcOppType,
					ISNULL(numUnitHour,0)numUnitHour,
					ISNULL(monPrice,0.00) monPrice,
					ISNULL(vcCompanyName,'') vcCompanyName,
					ISNULL(tintType,'') tintType
				FROM 
					@TEMPORGITEM 
				ORDER BY 
					CreatedDate DESC 
				OFFSET @numSkip ROWS FETCH NEXT 10 ROWS ONLY

				SELECT COUNT(numOppId) AS TotalItems FROM @TEMPORGITEM 
			END
			ELSE IF @searchCriteria = '3' -- Opp/Orders
			BEGIN			
				DECLARE @TEMPORGORDER TABLE
				(
					numOppId NUMERIC(18,0),
					CreatedDate DATETIME,
					vcPOppName VARCHAR(200),
					vcOppType VARCHAR(50),
					vcCompanyName  VARCHAR(200),
					tintType TINYINT
				)

				INSERT INTO 
					@TEMPORGORDER
				SELECT
					numOppId,
					bintCreatedDate,
					vcPOppName,
					vcOppType,
					vcCompanyName,
					tintType
				FROM
				(
					SELECT
						OpportunityMaster.numOppId,
						OpportunityMaster.vcPOppName,
						CASE 
							WHEN tintOppType = 1 THEN 'Sales ' + (CASE WHEN tintOppStatus = 1 THEN 'Order' ELSE 'Opportunity' END)
							WHEN tintOppType = 2 THEN 'Purchase ' + (CASE WHEN tintOppStatus = 1 THEN 'Order' ELSE 'Opportunity' END)
						END vcOppType,
						TEMP.vcCompanyName,
						bintCreatedDate,
						1 AS tintType
					FROM
						OpportunityMaster
					INNER JOIN
						#TEMPOrganization TEMP
					ON
						OpportunityMaster.numDivisionId = TEMP.numDivisionID
					WHERE
						numDomainId = @numDomainID
						AND tintOppType <> 0
					UNION 
					SELECT
						ReturnHeader.numReturnHeaderID AS numOppID,
						ReturnHeader.vcRMA AS vcPOppName,
						CASE 
							WHEN tintReturnType = 1 THEN 'Sales Return'
							WHEN tintReturnType = 2 THEN 'Purchase Return'
						END vcOppType,
						TEMP.vcCompanyName,
						dtCreatedDate AS bintCreatedDate,
						2 AS tintType
					FROM
						ReturnHeader
					INNER JOIN
						#TEMPOrganization TEMP
					ON
						ReturnHeader.numDivisionId = TEMP.numDivisionID
					WHERE
						numDomainId = @numDomainID AND tintReturnType IN (1,2)
				) TEMPFinal

				SELECT
					ISNULL(numOppId,0) numOppId,
					ISNULL(dbo.FormatedDateFromDate(CreatedDate,@numDomainID),'') bintCreatedDate,
					ISNULL(vcPOppName,'') vcPOppName,
					ISNULL(vcOppType,'') vcOppType,
					ISNULL(vcCompanyName,'') vcCompanyName,
					ISNULL(tintType,0) tintType
				FROM
					@TEMPORGORDER
				ORDER BY
					CreatedDate DESC
				OFFSET @numSkip ROWS FETCH NEXT 10 ROWS ONLY	
				
				SELECT COUNT(*) AS TotalRows FROM @TEMPORGORDER			
			END
			ELSE IF @searchCriteria = '4' -- BizDocs
			BEGIN
				DECLARE @TEMPORGBizDoc TABLE
				(
					numOppBizDocsId NUMERIC(18,0),
					CreatedDate DATETIME,
					vcBizDocID VARCHAR(200),
					vcBizDocType VARCHAR(100),
					vcCompanyName  VARCHAR(200)
				)

				INSERT INTO
					@TEMPORGBizDoc
				SELECT 
					numOppBizDocsId,
					OpportunityBizDocs.dtCreatedDate,
					vcBizDocID,
					ListDetails.vcData AS vcBizDocType,
					TEMP.vcCompanyName
				FROM 
					OpportunityBizDocs
				INNER JOIN
					OpportunityMaster
				ON
					OpportunityBizDocs.numOppId = OpportunityMaster.numOppId
				INNER JOIN
					#TEMPOrganization TEMP
				ON
					OpportunityMaster.numDivisionId = TEMP.numDivisionID
				INNER JOIN
					ListDetails
				ON
					ListDetails.numListID = 27
					AND OpportunityBizDocs.numBizDocId = ListDetails.numListItemID
				WHERE
					OpportunityMaster.numDomainId = @numDomainID

				SELECT
					ISNULL(numOppBizDocsId,0) numOppBizDocsId,
					ISNULL(dbo.FormatedDateFromDate(CreatedDate,@numDomainID),'') AS dtCreatedDate,
					ISNULL(vcBizDocID,'') vcBizDocID,
					ISNULL(vcBizDocType,'') vcBizDocType,
					ISNULL(vcCompanyName,'') vcCompanyName
				FROM
					@TEMPORGBizDoc
				ORDER BY
					CreatedDate DESC
				OFFSET @numSkip ROWS FETCH NEXT 10 ROWS ONLY
				
				SELECT COUNT(*) AS TotalRows FROM @TEMPORGBizDoc					
			END

			DROP TABLE #tempOrgSearch
			DROP TABLE #TEMPOrganization
		END
	END
	ELSE IF @searchType = '2' -- Item
	BEGIN
		CREATE TABLE #TempSearchedItem
		(
			numItemCode NUMERIC(18,0), 
			vcItemName VARCHAR(200), 
			vcPathForTImage VARCHAR(200), 
			vcCompanyName VARCHAR(200), 
			monListPrice NUMERIC(18,0), 
			txtItemDesc TEXT, 
			vcModelID VARCHAR(100), 
			numBarCodeId VARCHAR(100), 
			vcBarCode VARCHAR(100), 
			vcSKU VARCHAR(100),
			vcWHSKU VARCHAR(100), 
			vcPartNo VARCHAR(100),
			numWarehouseItemID NUMERIC(18,0), 
			vcAttributes VARCHAR(500)
		)

		DECLARE @strItemSearch AS VARCHAR(1000) = ''
		DECLARE @strAttributeSearch AS VARCHAR(1000) = ''
		DECLARE @strSearch AS VARCHAR(1000)

		IF CHARINDEX(',',@searchText) > 0
		BEGIN
			SET @strItemSearch = LTRIM(RTRIM(SUBSTRING(@searchText,0,CHARINDEX(',',@searchText))))
			SET @strAttributeSearch = SUBSTRING(@searchText,CHARINDEX(',',@searchText) + 1,LEN(@searchText)) 

			IF LEN(@strAttributeSearch) > 0
			BEGIN
				SET @strAttributeSearch = 'TEMPAttributes.vcAttributes LIKE ''%' + REPLACE(@strAttributeSearch,',','%'' AND TEMPAttributes.vcAttributes LIKE ''%') + '%'''
			END
			ELSE
			BEGIN
				SET @strAttributeSearch = ''
			END
		END
		ELSE
		BEGIN
			SET @strItemSearch = @searchText
		END

		DECLARE @vcCustomDisplayField AS VARCHAR(4000) = ''
		SELECT * INTO #TempItemCustomDisplayFields FROM
		(
			SELECT 
				numFieldId,
				vcFieldName
			FROM 
				View_DynamicCustomColumns   
			WHERE 
				Grp_id=5 AND numFormId=22 AND numUserCntID=@numUserCntID AND numDomainID=@numDomainID AND 
				tintPageType=1 AND bitCustom=1 AND numRelCntType=0
		)TD

		IF (SELECT COUNT(*) FROM #TempItemCustomDisplayFields) > 0
		BEGIN
			SELECT @vcCustomDisplayField = STUFF(
									(SELECT ', ' +  'ISNULL(dbo.GetCustFldValueItem(' + CAST(numFieldId AS VARCHAR) + ',numItemCode),'''') AS [' + vcFieldName + ']' FROM #TempItemCustomDisplayFields FOR XML PATH(''))
									, 1
									, 0
									, ''
								)
		END

		SELECT * INTO #TempItemSearchFields FROM
		(
			SELECT 
				numFieldId,
				vcDbColumnName,
				ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName,
				vcLookBackTableName,
				tintRow AS tintOrder,
				0 as Custom
			FROM 
				View_DynamicColumns
			WHERE 
				numFormId=22 AND numUserCntID=@numUserCntID AND numDomainID=@numDomainID AND 
				tintPageType=1 AND bitCustom=0  AND ISNULL(bitSettingField,0)=1 AND numRelCntType=1
				AND vcDbColumnName <> 'vcAttributes'
			UNION   
			SELECT 
				numFieldId,
				vcFieldName,
				vcFieldName,
				'CFW_FLD_Values',
				tintRow AS tintOrder,
				1 as Custom
			FROM 
				View_DynamicCustomColumns   
			WHERE 
				Grp_id=5 AND numFormId=22 AND numUserCntID=@numUserCntID AND numDomainID=@numDomainID AND 
				tintPageType=1 AND bitCustom=1 AND numRelCntType=1
		)Y

		IF (SELECT COUNT(*) FROM #TempItemSearchFields) > 0
		BEGIN
			SELECT  @strSearch = STUFF(
									(SELECT ' OR ' +  IIF(Custom = 1,'dbo.GetCustFldValueItem('+ CAST(numFieldId AS VARCHAR) +',numItemCode)',vcDbColumnName) + ' LIKE ''%' + @strItemSearch + '%''' FROM #TempItemSearchFields FOR XML PATH(''))
									, 1
									, 3
									, ''
								)
		END
		ELSE
		BEGIN
			SELECT @strSearch = 'vcItemName LIKE ''%' + @strItemSearch + '%'''
		END

		SET @strSQL = 'SELECT
							numItemCode, vcItemName, vcPathForTImage, vcCompanyName, MIN(monListPrice) monListPrice, txtItemDesc, 
							vcModelID, numBarCodeId, MIN(vcBarCode) vcBarCode, vcSKU, MIN(vcWHSKU) vcWHSKU, vcPartNo,
							bitSerialized, numItemGroup, Isarchieve, charItemType, 0 AS numWarehouseItemID, '''' vcAttributes
						FROM
						(
						SELECT
							Item.numItemCode,
							ISNULL(vcItemName,'''') AS vcItemName,
							ISNULL((SELECT TOP 1 vcPathForTImage from ItemImages WHERE numItemCode = Item.numItemCode),'''') as vcPathForTImage,
							ISNULL(vcCompanyName,'''') AS vcCompanyName,
							ISNULL(monListPrice,''0.00'') AS monListPrice,
							ISNULL(txtItemDesc,'''') AS txtItemDesc,
							ISNULL(vcModelID,'''') AS vcModelID,
							ISNULL(numBarCodeId,'''') AS numBarCodeId,
							ISNULL(vcBarCode,'''') AS vcBarCode,
							ISNULL(vcWHSKU,'''') AS vcWHSKU,
							ISNULL(vcSKU,'''') AS vcSKU,
							ISNULL(vcPartNo,'''') AS vcPartNo,
							ISNULL(bitSerialized,0) AS bitSerialized,
							ISNULL(numItemGroup,0) AS numItemGroup,
							ISNULL(Isarchieve,0) AS Isarchieve,
							ISNULL(charItemType,'''') AS charItemType
						FROM
							Item
						LEFT JOIN
							DivisionMaster 
						ON
							Item.numVendorID = DivisionMaster.numDivisionID
						LEFT JOIN
							CompanyInfo
						ON
							DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
						LEFT JOIN
							Vendor
						ON
							Item.numVendorID = Vendor.numVendorID AND
							Vendor.numItemCode = Item.numItemCode
						LEFT JOIN
							WareHouseItems
						ON
							Item.numItemCode = WareHouseItems.numItemID
						WHERE 
							Item.numDomainID = ' + CAST(@numDomainID AS VARCHAR) + ')V WHERE ' + @strSearch  + '
						GROUP BY
							numItemCode, vcItemName, vcPathForTImage, vcCompanyName, txtItemDesc, 
							vcModelID, numBarCodeId, vcSKU, bitSerialized, numItemGroup, Isarchieve, charItemType, vcPartNo'
	
		IF LEN(@strAttributeSearch) > 0
		BEGIN
			SET @strSQL = 'SELECT
								numItemCode, vcItemName, vcPathForTImage, vcCompanyName, TEMPAttributes.monWListPrice AS monListPrice, txtItemDesc, 
								vcModelID, numBarCodeId, TEMPAttributes.vcBarCode, vcSKU, TEMPAttributes.vcWHSKU, vcPartNo, TEMPAttributes.numWareHouseItemID,
								TEMPAttributes.vcAttributes
							FROM
								(' + @strSQL + ') TEMP
							OUTER APPLY
							(
								SELECT 
									WHI.numWareHouseItemID,
									WHI.vcBarCode,
									WHI.vcWHSKU,
									WHI.monWListPrice,
									STUFF(
												(SELECT 
												'', '' +  vcAttribute
												FROM  
												(
												SELECT
													CONCAT(	CFM.Fld_label, '' : '',
													(CASE 
														WHEN CFM.Fld_type = ''SelectBox''
														THEN 
															(select vcData from ListDetails where numListID= CFM.numlistid AND numListItemID=CFVSI.Fld_Value)
														ELSE
															CFVSI.Fld_Value
													END)) vcAttribute
												FROM
													CFW_Fld_Values_Serialized_Items CFVSI
												INNER JOIN
													CFW_Fld_Master CFM
												ON
													CFM.Fld_id = CFVSI.Fld_ID
												WHERE
													RecId = WHI.numWareHouseItemID
													AND bitSerialized = TEMP.bitSerialized
												) TEMP
												FOR XML PATH(''''))
											, 1
											, 1
											, ''''
											) AS vcAttributes
								FROM
									dbo.WareHouseItems WHI
								WHERE 
									WHI.numItemID = TEMP.numItemCode
									AND ISNULL(TEMP.numItemGroup,0) > 0
									AND ISNULL(TEMP.Isarchieve, 0) <> 1
									AND TEMP.charItemType NOT IN (''A'')
							) AS TEMPAttributes
							WHERE
								' + @strAttributeSearch
		END

		EXEC ('INSERT INTO #TempSearchedItem SELECT numItemCode, vcItemName, vcPathForTImage, vcCompanyName, monListPrice, txtItemDesc, vcModelID, numBarCodeId, vcBarCode, vcSKU, vcWHSKU, vcPartNo, numWarehouseItemID, vcAttributes FROM (' + @strSQL + ')TEMPFinal')

		IF @searchCriteria = '1' --Items
		BEGIN
			SET @strSQL = 'SELECT * ' + @vcCustomDisplayField + ' FROM #TempSearchedItem ORDER BY numItemCode OFFSET ' + CAST(@numSkip AS VARCHAR(100)) + ' ROWS FETCH NEXT 10 ROWS ONLY'
			EXEC (@strSQL)
			SELECT COUNT(*) AS TotalRows FROM #TempSearchedItem
		END
		ELSE IF @searchCriteria = '3' -- Opp/Orders
		BEGIN
			DECLARE @TEMPItemOrder TABLE
			(
				numOppId NUMERIC(18,0),
				CreatedDate DATETIME,
				vcPOppName VARCHAR(200),
				vcOppType VARCHAR(50),
				vcCompanyName  VARCHAR(200),
				tintType TINYINT
			)

			INSERT INTO 
				@TEMPItemOrder
			SELECT
				numOppId,
				bintCreatedDate,
				vcPOppName,
				vcOppType,
				vcCompanyName,
				tintType
			FROM
				(
					SELECT
						OpportunityMaster.numOppId,
						OpportunityMaster.vcPOppName,
						CASE 
							WHEN tintOppType = 1 THEN 'Sales ' + (CASE WHEN tintOppStatus = 1 THEN 'Order' ELSE 'Opportunity' END)
							WHEN tintOppType = 2 THEN 'Purchase ' + (CASE WHEN tintOppStatus = 1 THEN 'Order' ELSE 'Opportunity' END)
						END vcOppType,
						CompanyInfo.vcCompanyName,
						OpportunityMaster.bintCreatedDate,
						1 AS tintType
					FROM 
						OpportunityMaster
					INNER JOIN
						(
							SELECT
								numOppID
							FROM 
								OpportunityItems OI
							INNER JOIN
								#TempSearchedItem
							ON
								OI.numItemCode = #TempSearchedItem.numItemCode
								AND (OI.numWarehouseItmsID = #TempSearchedItem.numWarehouseItemID OR ISNULL(#TempSearchedItem.numWarehouseItemID,0) = 0)
							GROUP BY
								numOppID
						) TempOppItems
					ON
						OpportunityMaster.numOppId = TempOppItems.numOppId
					INNER JOIN
						DivisionMaster 
					ON
						OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
					INNER JOIN
						CompanyInfo
					ON
						DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
					WHERE
						OpportunityMaster.numDomainId =@numDomainID
						AND tintOppType <> 0
						AND 1 = (
								CASE @orderType
								 WHEN 0 THEN 1
								 WHEN 1 THEN (CASE WHEN (tintOppType = 1 AND tintOppStatus=1) THEN 1 ELSE 0 END)
								 WHEN 2 THEN (CASE WHEN (tintOppType = 2 AND tintOppStatus=1) THEN 1 ELSE 0 END)
								 WHEN 3 THEN (CASE WHEN (tintOppType = 1 AND ISNULL(tintOppStatus,0)=0) THEN 1 ELSE 0 END)
								 WHEN 4 THEN (CASE WHEN (tintOppType = 2 AND ISNULL(tintOppStatus,0)=0) THEN 1 ELSE 0 END)
								 WHEN 5 THEN 0
								 WHEN 6 THEN 0
								END
							)
					UNION
					SELECT
						ReturnHeader.numReturnHeaderID AS numOppID,
						ReturnHeader.vcRMA AS vcPOppName,
						CASE 
							WHEN tintReturnType = 1 THEN 'Sales Return'
							WHEN tintReturnType = 2 THEN 'Purchase Return'
						END vcOppType,
						CompanyInfo.vcCompanyName,
						ReturnHeader.dtCreatedDate AS bintCreatedDate,
						2 AS tintType
					FROM
						ReturnHeader
					INNER JOIN
						(
							SELECT
								numReturnHeaderID
							FROM 
								ReturnItems RI
							INNER JOIN
								#TempSearchedItem
							ON
								RI.numItemCode = #TempSearchedItem.numItemCode
								AND (RI.numWareHouseItemID = #TempSearchedItem.numWarehouseItemID OR ISNULL(#TempSearchedItem.numWarehouseItemID,0) = 0)
							GROUP BY
								numReturnHeaderID
						) TempReturnItems
					ON
						ReturnHeader.numReturnHeaderID = TempReturnItems.numReturnHeaderID
					INNER JOIN
						DivisionMaster 
					ON
						ReturnHeader.numDivisionId = DivisionMaster.numDivisionID
					INNER JOIN
						CompanyInfo
					ON
						DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
					WHERE
						ReturnHeader.numDomainId = @numDomainID 
						AND 1 = (
								CASE @orderType
								 WHEN 0 THEN (CASE WHEN tintReturnType = 1 OR tintReturnType = 2 THEN 1 ELSE 0 END)
								 WHEN 1 THEN 0
								 WHEN 2 THEN 0
								 WHEN 3 THEN 0
								 WHEN 4 THEN 0
								 WHEN 5 THEN (CASE WHEN tintReturnType = 1 THEN 1 ELSE 0 END)
								 WHEN 6 THEN (CASE WHEN tintReturnType = 2 THEN 1 ELSE 0 END)
								END
							)
				) TEMPFinal

			SELECT 
				ISNULL(numOppId,0) numOppId,
				ISNULL(dbo.FormatedDateFromDate(CreatedDate,@numDomainID),'') bintCreatedDate,
				ISNULL(vcPOppName,'') vcPOppName,
				ISNULL(vcOppType,'') vcOppType,
				ISNULL(vcCompanyName,'') vcCompanyName,
				ISNULL(tintType,0) tintType
			FROM
				@TEMPItemOrder
			ORDER BY
				CreatedDate DESC
			OFFSET @numSkip ROWS FETCH NEXT 10 ROWS ONLY	
				
			SELECT COUNT(*) AS TotalRows FROM @TEMPItemOrder	
		END
		ELSE IF @searchCriteria = '4' -- BizDocs
		BEGIN
			DECLARE @TEMPItemBizDoc TABLE
			(
				numOppBizDocsId NUMERIC(18,0),
				CreatedDate DATETIME,
				vcBizDocID VARCHAR(200),
				vcBizDocType VARCHAR(100),
				vcCompanyName  VARCHAR(200)
			)

			INSERT INTO
				@TEMPItemBizDoc
			SELECT 
				numOppBizDocsId,
				OpportunityBizDocs.dtCreatedDate,
				vcBizDocID,
				ListDetails.vcData AS vcBizDocType,
				CompanyInfo.vcCompanyName
			FROM 
				OpportunityBizDocs
			INNER JOIN
				(
					SELECT
						numOppBizDocID
					FROM 
						OpportunityBizDocItems OBI
					INNER JOIN
						#TempSearchedItem
					ON
						OBI.numItemCode = #TempSearchedItem.numItemCode
						AND (OBI.numWarehouseItmsID = #TempSearchedItem.numWarehouseItemID OR ISNULL(#TempSearchedItem.numWarehouseItemID,0) = 0)
					GROUP BY
						numOppBizDocID
				) TempOppBizDocItems
			ON
				OpportunityBizDocs.numOppBizDocsId = TempOppBizDocItems.numOppBizDocID
			INNER JOIN
				OpportunityMaster
			ON
				OpportunityBizDocs.numOppId = OpportunityMaster.numOppId
			INNER JOIN
				DivisionMaster
			ON
				OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
			INNER JOIN
				CompanyInfo
			ON
				DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
			INNER JOIN
				ListDetails
			ON
				ListDetails.numListID = 27
				AND OpportunityBizDocs.numBizDocId = ListDetails.numListItemID
			WHERE
				OpportunityMaster.numDomainId = @numDomainID
				AND (OpportunityBizDocs.numBizDocID = @bizDocType OR ISNULL(@bizDocType,0) = 0)

			SELECT
				ISNULL(numOppBizDocsId,0) numOppBizDocsId,
				ISNULL(dbo.FormatedDateFromDate(CreatedDate,@numDomainID),'') AS dtCreatedDate,
				ISNULL(vcBizDocID,'') vcBizDocID,
				ISNULL(vcBizDocType,'') vcBizDocType,
				ISNULL(vcCompanyName,'') vcCompanyName
			FROM
				@TEMPItemBizDoc
			ORDER BY
				CreatedDate DESC
			OFFSET @numSkip ROWS FETCH NEXT 10 ROWS ONLY
				
			SELECT COUNT(*) AS TotalRows FROM @TEMPItemBizDoc
		END

		
		DROP TABLE #TempItemSearchFields
		DROP TABLE #TempItemCustomDisplayFields
		DROP TABLE #TempSearchedItem
	END
	ELSE IF @searchType = '3' -- Opps/Orders
	BEGIN
		DECLARE @TEMPORDER TABLE
		(
			numOppId NUMERIC(18,0),
			CreatedDate DATETIME,
			vcPOppName VARCHAR(200),
			vcOppType VARCHAR(50),
			vcCompanyName  VARCHAR(200),
			tintType TINYINT
		)

		INSERT INTO 
			@TEMPORDER
		SELECT
			numOppId,
			bintCreatedDate,
			vcPOppName,
			vcOppType,
			vcCompanyName,
			tintType
		FROM
			(
				SELECT
					OpportunityMaster.numOppId,
					OpportunityMaster.vcPOppName,
					CASE 
						WHEN tintOppType = 1 THEN 'Sales ' + (CASE WHEN tintOppStatus = 1 THEN 'Order' ELSE 'Opportunity' END)
						WHEN tintOppType = 2 THEN 'Purchase ' + (CASE WHEN tintOppStatus = 1 THEN 'Order' ELSE 'Opportunity' END)
					END vcOppType,
					CompanyInfo.vcCompanyName,
					OpportunityMaster.bintCreatedDate,
					1 AS tintType
				FROM 
					OpportunityMaster
				INNER JOIN
					DivisionMaster 
				ON
					OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
				INNER JOIN
					CompanyInfo
				ON
					DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
				WHERE
					OpportunityMaster.numDomainId =@numDomainID
					AND tintOppType <> 0
					AND (vcPOppName LIKE N'%' + @searchText + '%' OR numOppId LIKE N'%' + @searchText + '%')
					AND 1 = (
							CASE @orderType
							 WHEN 0 THEN 1
							 WHEN 1 THEN (CASE WHEN (tintOppType = 1 AND tintOppStatus=1) THEN 1 ELSE 0 END)
							 WHEN 2 THEN (CASE WHEN (tintOppType = 2 AND tintOppStatus=1) THEN 1 ELSE 0 END)
							 WHEN 3 THEN (CASE WHEN (tintOppType = 1 AND ISNULL(tintOppStatus,0)=0) THEN 1 ELSE 0 END)
							 WHEN 4 THEN (CASE WHEN (tintOppType = 2 AND ISNULL(tintOppStatus,0)=0) THEN 1 ELSE 0 END)
							 WHEN 5 THEN 0
							 WHEN 6 THEN 0
							END
						)
				UNION
				SELECT
					ReturnHeader.numReturnHeaderID AS numOppID,
					ReturnHeader.vcRMA AS vcPOppName,
					CASE 
						WHEN tintReturnType = 1 THEN 'Sales Return'
						WHEN tintReturnType = 2 THEN 'Purchase Return'
					END vcOppType,
					CompanyInfo.vcCompanyName,
					ReturnHeader.dtCreatedDate AS bintCreatedDate,
					2 AS tintType
				FROM
					ReturnHeader
				INNER JOIN
					DivisionMaster 
				ON
					ReturnHeader.numDivisionId = DivisionMaster.numDivisionID
				INNER JOIN
					CompanyInfo
				ON
					DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
				WHERE
					ReturnHeader.numDomainId = @numDomainID 
					AND (vcRMA LIKE N'%' + @searchText + '%' OR ReturnHeader.numReturnHeaderID LIKE N'%' + @searchText + '%')
					AND 1 = (
							CASE @orderType
							 WHEN 0 THEN (CASE WHEN tintReturnType = 1 OR tintReturnType = 2 THEN 1 ELSE 0 END)
							 WHEN 1 THEN 0
							 WHEN 2 THEN 0
							 WHEN 3 THEN 0
							 WHEN 4 THEN 0
							 WHEN 5 THEN (CASE WHEN tintReturnType = 1 THEN 1 ELSE 0 END)
							 WHEN 6 THEN (CASE WHEN tintReturnType = 2 THEN 1 ELSE 0 END)
							END
						)
				) TEMPFinal

		SELECT 
			ISNULL(numOppId,0) numOppId,
			ISNULL(dbo.FormatedDateFromDate(CreatedDate,@numDomainID),'') bintCreatedDate,
			ISNULL(vcPOppName,'') vcPOppName,
			ISNULL(vcOppType,'') vcOppType,
			ISNULL(vcCompanyName,'') vcCompanyName,
			ISNULL(tintType,0) tintType
		FROM
			@TEMPORDER
		ORDER BY
			CreatedDate DESC
		OFFSET @numSkip ROWS FETCH NEXT 10 ROWS ONLY	
				
		SELECT COUNT(*) AS TotalRows FROM @TEMPORDER		
	END
	ELSE IF @searchType = '4' -- BizDocs
	BEGIN
		DECLARE @TEMPBizDoc TABLE
		(
			numOppBizDocsId NUMERIC(18,0),
			CreatedDate DATETIME,
			vcBizDocID VARCHAR(200),
			vcBizDocType VARCHAR(100),
			vcCompanyName  VARCHAR(200)
		)

		INSERT INTO
			@TEMPBizDoc
		SELECT 
			numOppBizDocsId,
			OpportunityBizDocs.dtCreatedDate,
			vcBizDocID,
			ListDetails.vcData AS vcBizDocType,
			CompanyInfo.vcCompanyName
		FROM 
			OpportunityBizDocs
		INNER JOIN
			OpportunityMaster
		ON
			OpportunityBizDocs.numOppId = OpportunityMaster.numOppId
		INNER JOIN
			DivisionMaster
		ON
			OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
		INNER JOIN
			CompanyInfo
		ON
			DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
		INNER JOIN
			ListDetails
		ON
			ListDetails.numListID = 27
			AND OpportunityBizDocs.numBizDocId = ListDetails.numListItemID
		WHERE
			OpportunityMaster.numDomainId = @numDomainID
			AND vcBizDocID LIKE N'%' + @searchText + '%'
			AND (OpportunityBizDocs.numBizDocID = @bizDocType OR ISNULL(@bizDocType,0) = 0)

		SELECT
			ISNULL(numOppBizDocsId,0) numOppBizDocsId,
			ISNULL(dbo.FormatedDateFromDate(CreatedDate,@numDomainID),'') AS dtCreatedDate,
			ISNULL(vcBizDocID,'') vcBizDocID,
			ISNULL(vcBizDocType,'') vcBizDocType,
			ISNULL(vcCompanyName,'') vcCompanyName
		FROM
			@TEMPBizDoc
		ORDER BY
			CreatedDate DESC
		OFFSET @numSkip ROWS FETCH NEXT 10 ROWS ONLY
				
		SELECT COUNT(*) AS TotalRows FROM @TEMPBizDoc
	END
	ELSE IF @searchType = '5' -- Contact
	BEGIN
		DECLARE @TableContact TABLE
		(
			numContactId NUMERIC(18,0),
			vcFirstName VARCHAR(100),
			vcLastname VARCHAR(100),
			vcFullName VARCHAR(200),
			vcEmail VARCHAR(100),
			numPhone VARCHAR(100),
			numPhoneExtension VARCHAR(100),
			vcCompanyName VARCHAR(200),
			bitPrimaryContact VARCHAR(5)
		)

		INSERT INTO
			@TableContact
		SELECT 
			ADC.numContactId,
			ISNULL(vcFirstName,'') AS vcFirstName,
			ISNULL(vcLastName,'') AS vcLastName,
			ISNULL(vcFirstName,'') + ' ' + ISNULL(vcLastName,'') AS vcFullName,
			ISNULL(vcEmail,'') AS vcEmail,
			ISNULL(ADC.numPhone,'') AS numPhone,
			ISNULL(ADC.numPhoneExtension,'') AS numPhoneExtension,
			ISNULL(CI.vcCompanyName,'') AS vcCompanyName,
			(CASE WHEN ISNULL(bitPrimaryContact,0) = 1 THEN 'Yes' ELSE 'No' END) AS bitPrimaryContact
		FROM 
			AdditionalContactsInformation ADC
		INNER JOIN
			DivisionMaster DM
		ON
			ADC.numDivisionId = DM.numDivisionID
		INNER JOIN
			CompanyInfo CI
		ON
			DM.numCompanyID = CI.numCompanyId
		WHERE 
			ADC.numDomainID = @numDomainID AND
			(vcFirstName LIKE '%' + @searchText + '%'
			OR vcLastName LIKE '%' + @searchText + '%'
			OR vcEmail LIKE '%' + @searchText + '%'
			OR numPhone LIKE '%' + @searchText + '%')

		SELECT 
			* 
		FROM 
			@TableContact	
		ORDER BY 
			vcCompanyName ASC, bitPrimaryContact DESC
		OFFSET 
			@numSkip 
		ROWS FETCH NEXT 
			10 
		ROWS ONLY

		SELECT COUNT(*) FROM @TableContact
	END
GO
--Created by Anoop Jayaraj

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_UpdatePOItemsForFulfillment')
DROP PROCEDURE USP_UpdatePOItemsForFulfillment
GO
CREATE PROCEDURE  USP_UpdatePOItemsForFulfillment
    @numQtyReceived NUMERIC(9),
    @numOppItemID NUMERIC(9),
    @vcError VARCHAR(200) = '' OUTPUT,
    @numUserCntID AS NUMERIC(9),
    @dtItemReceivedDate AS DATETIME 
AS 

--Transaction is invoked from ADO.net (Purchase Fulfillment) Do not add transaction code here.
-- BEGIN TRY
-- BEGIN TRANSACTION
			DECLARE @numDomain AS NUMERIC(18,0)
			DECLARE @onAllocation AS NUMERIC          
			DECLARE @numWarehouseItemID AS NUMERIC       
			DECLARE @numOldQtyReceived AS NUMERIC       
			DECLARE @numNewQtyReceived AS NUMERIC
			DECLARE @onHand AS NUMERIC         
			DECLARE @onOrder AS NUMERIC            
			DECLARE @onBackOrder AS NUMERIC              
			DECLARE @bitStockTransfer BIT
			DECLARE @numToWarehouseItemID NUMERIC
			DECLARE @numOppId NUMERIC
			DECLARE @numUnits NUMERIC
			DECLARE @monPrice AS MONEY 
			DECLARE @numItemCode NUMERIC 
		
			SELECT  @numWarehouseItemID = ISNULL([numWarehouseItmsID], 0),
					@numOldQtyReceived = ISNULL([numUnitHourReceived], 0),
					@bitStockTransfer = ISNULL(OM.bitStockTransfer,0),
					@numToWarehouseItemID= ISNULL(numToWarehouseItemID,0),
					@numOppId=OM.numOppId,
					@numUnits=OI.numUnitHour,
					@monPrice=isnull(monPrice,0) * (CASE WHEN ISNULL(OM.fltExchangeRate,0)=0 THEN 1 ELSE OM.fltExchangeRate END),
					@numItemCode=OI.numItemCode,
					@numDomain = OM.[numDomainId]
			FROM    [OpportunityItems] OI INNER JOIN dbo.OpportunityMaster OM
					ON OI.numOppId = OM.numOppId
			WHERE   [numoppitemtCode] = @numOppItemID
    
    
    
			IF @bitStockTransfer = 1 --added by chintan
			BEGIN
			--ship item from FROM warehouse
				declare @p3 varchar(500)
				set @p3=''
				exec USP_UpdateQtyShipped @numQtyReceived,@numOppItemID,@numUserCntID,@vcError=@p3 OUTPUT
				IF LEN(@p3)>0 
				BEGIN
					RAISERROR ( @p3,16, 1 )
					RETURN ;

				END
		
			-- Receive item from To Warehouse
			SET @numWarehouseItemID=@numToWarehouseItemID
    
		   END  
    
			DECLARE @numTotalQuantityReceived NUMERIC(18,0)
			SET @numTotalQuantityReceived = @numQtyReceived + @numOldQtyReceived ;
			SET @numNewQtyReceived = @numQtyReceived;
			DECLARE @description AS VARCHAR(100)
			SET @description='PO Qty Received (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Received:' +  CAST(@numTotalQuantityReceived AS VARCHAR(10)) + ')'
			
		--    PRINT @numNewQtyReceived
			IF @numNewQtyReceived <= 0 
				RETURN 
  
					SELECT  @onHand = ISNULL(numOnHand, 0),
					@onAllocation = ISNULL(numAllocation, 0),
					@onOrder = ISNULL(numOnOrder, 0),
					@onBackOrder = ISNULL(numBackOrder, 0)
			FROM    WareHouseItems
			WHERE   numWareHouseItemID = @numWareHouseItemID
			
			           DECLARE @TotalOnHand AS NUMERIC;SET @TotalOnHand=0  
			           SELECT @TotalOnHand=ISNULL((SUM(numOnHand) + SUM(numAllocation)), 0) FROM dbo.WareHouseItems WHERE numItemID=@numItemCode 
						
			           --Updating the Average Cost
			           DECLARE @monAvgCost AS MONEY 
			           SELECT  @monAvgCost = ISNULL(monAverageCost, 0) FROM    Item WHERE   numitemcode = @numItemCode  
    
  						SET @monAvgCost = ( ( @TotalOnHand * @monAvgCost )
                                             + (@numNewQtyReceived * @monPrice))
                            / ( @TotalOnHand + @numNewQtyReceived )
                            
                        UPDATE  item
                        SET     monAverageCost = @monAvgCost
                        WHERE   numItemCode = @numItemCode
    
			
  
		--	SELECT @onHand onHand,
		--            @onAllocation onAllocation,
		--            @onBackOrder onBackOrder,
		--            @onOrder onOrder
      
			IF @onOrder >= @numNewQtyReceived 
				BEGIN    
				PRINT '1 case'        
					SET @onOrder = @onOrder - @numNewQtyReceived             
					IF @onBackOrder >= @numNewQtyReceived 
						BEGIN            
							SET @onBackOrder = @onBackOrder - @numNewQtyReceived             
							SET @onAllocation = @onAllocation + @numNewQtyReceived             
						END            
					ELSE 
						BEGIN            
							SET @onAllocation = @onAllocation + @onBackOrder            
							SET @numNewQtyReceived = @numNewQtyReceived - @onBackOrder            
							SET @onBackOrder = 0            
							SET @onHand = @onHand + @numNewQtyReceived             
						END         
				END            
			ELSE IF @onOrder < @numNewQtyReceived 
				BEGIN            
				PRINT '2 case'        
					SET @onHand = @onHand + @onOrder
					SET @onOrder = @numNewQtyReceived - @onOrder
				END   

				SELECT @onHand onHand,
					@onAllocation onAllocation,
					@onBackOrder onBackOrder,
					@onOrder onOrder
                
			UPDATE  [OpportunityItems]
			SET     numUnitHourReceived = @numTotalQuantityReceived
			WHERE   [numoppitemtCode] = @numOppItemID

			UPDATE  WareHouseItems
			SET     numOnHand = @onHand,
					numAllocation = @onAllocation,
					numBackOrder = @onBackOrder,
					numOnOrder = @onOrder,dtModified = GETDATE() 
			WHERE   numWareHouseItemID = @numWareHouseItemID
    
  
	    UPDATE dbo.OpportunityMaster SET dtItemReceivedDate=@dtItemReceivedDate WHERE numOppId=@numOppId
		
		EXEC dbo.USP_ManageWareHouseItems_Tracking
			@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
			@numReferenceID = @numOppId, --  numeric(9, 0)
			@tintRefType = 4, --  tinyint
			@vcDescription = @description, --  varchar(100)
			@numModifiedBy = @numUserCntID,
			@dtRecordDate =  @dtItemReceivedDate,
			@numDomainID = @numDomain
          
    
		--    BEGIN
		--        DECLARE @vcOppName VARCHAR(200)
		--        SELECT TOP 1
		--                @vcOppName = ISNULL([vcPOppName], '')
		--        FROM    [OpportunityMaster]
		--        WHERE   [numOppId] IN ( SELECT  [numOppId]
		--                                FROM    [OpportunityItems]
		--                                WHERE   [numoppitemtCode] = @numOppItemID )
		--        SET @vcError = 'There is not enough Qty on Allocation to save quantity Received for ' + @vcOppName
		--        RETURN
		--    END
		--     
		
--COMMIT
--END TRY
--BEGIN CATCH
--   --Whoops, there was an error
--  IF @@TRANCOUNT > 0
--     ROLLBACK

--  -- Raise an error with the details of the exception
--  DECLARE @ErrMsg nvarchar(4000), @ErrSeverity int
--  SELECT @ErrMsg = ERROR_MESSAGE(),
--         @ErrSeverity = ERROR_SEVERITY()

--  RAISERROR(@ErrMsg, @ErrSeverity, 1)
--END CATCH
GO

GO
/****** Object:  StoredProcedure [dbo].[USP_UpdatingInventory]    Script Date: 06/01/2009 23:48:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj       
--exec usp_updatinginventory 0,181193,1,0,1       
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_updatinginventory')
DROP PROCEDURE usp_updatinginventory
GO
CREATE PROCEDURE [dbo].[USP_UpdatingInventory]        
@byteMode as tinyint=0,  
@numWareHouseItemID as numeric(9)=0,  
@Units as INTEGER,
@numWOId AS NUMERIC(9)=0,
@numUserCntID AS NUMERIC(9)=0,
@numOppId AS NUMERIC(9)=0      
as      
BEGIN TRY
BEGIN TRANSACTION
  DECLARE @Description AS VARCHAR(200)
  DECLARE @numItemCode NUMERIC(18)
  DECLARE @numDomain AS NUMERIC(18,0)
  DECLARE @ParentWOID AS NUMERIC(18,0)

  SELECT @ParentWOID=numParentWOID FROM WorkOrder WHERE numWOId=@numWOId
  SELECT @numItemCode=numItemID,@numDomain = numDomainID from WareHouseItems where numWareHouseItemID = @numWareHouseItemID
  
if @byteMode=0  -- Aseeembly Item
begin  
	DECLARE @CurrentAverageCost MONEY
	DECLARE @TotalCurrentOnHand INT
	DECLARE @newAverageCost MONEY
	
	SELECT @CurrentAverageCost = monAverageCost FROM dbo.Item WHERE numItemCode =@numItemCode 
	SELECT @TotalCurrentOnHand = ISNULL((SUM(numOnHand) + SUM(numAllocation)),0) FROM dbo.WareHouseItems WHERE numItemID =@numItemCode 
	PRINT @CurrentAverageCost
	PRINT @TotalCurrentOnHand
	
	
	SELECT @newAverageCost = SUM((numQtyItemsReq * dbo.fn_UOMConversion(UOM.numUOMId,0,IDUOM.numDomainId,IDUOM.numUOMId)) * I.monAverageCost )
	FROM dbo.ItemDetails ID
	LEFT JOIN dbo.Item I ON I.numItemCode = ID.numChildItemID
	LEFT JOIN UOM ON UOM.numUOMId=i.numBaseUnit
	LEFT JOIN UOM IDUOM ON IDUOM.numUOMId=ID.numUOMId
	WHERE numItemKitID = @numItemCode
	PRINT @newAverageCost
	SET @newAverageCost = ((@CurrentAverageCost * @TotalCurrentOnHand)  + (@newAverageCost * @Units)) / (@TotalCurrentOnHand + @Units)
	
	PRINT @newAverageCost

	UPDATE item SET monAverageCost =@newAverageCost WHERE numItemCode = @numItemCode

	IF ISNULL(@numWOId,0) > 0
	BEGIN
		UPDATE WareHouseItems set numOnHand=ISNULL(numOnHand,0)+@Units,numOnOrder= CASE WHEN ISNULL(numOnOrder,0) < @Units THEN 0 ELSE ISNULL(numOnOrder,0)-@Units END,dtModified = GETDATE()  where numWareHouseItemID=@numWareHouseItemID  
	END
	ELSE
	BEGIN
		UPDATE WareHouseItems set numOnHand=ISNULL(numOnHand,0)+@Units,dtModified = GETDATE()  where numWareHouseItemID=@numWareHouseItemID  
	END
end  
else if @byteMode=1  --Aseembly Child item
begin  
	declare @onHand as numeric                                    
	declare @onOrder as numeric                                    
	declare @onBackOrder as numeric                                   
	declare @onAllocation as numeric    

	select                                     
		@onHand=isnull(numOnHand,0),                                    
		@onAllocation=isnull(numAllocation,0),                                    
		@onOrder=isnull(numOnOrder,0),                                    
		@onBackOrder=isnull(numBackOrder,0)                                     
	from 
		WareHouseItems 
	where 
		numWareHouseItemID=@numWareHouseItemID  


	IF ISNULL(@numWOId,0) > 0
	BEGIN
		--RELEASE QTY FROM ALLOCATION
		SET @onAllocation=@onAllocation-@Units
	END
	ELSE
	BEGIN
		--DECREASE QTY FROM ONHAND
		SET @onHand=@onHand-@Units
	END

	 --UPDATE INVENTORY
	UPDATE 
		WareHouseItems 
	SET      
		numOnHand=@onHand,
		numOnOrder=@onOrder,                         
		numAllocation=@onAllocation,
		numBackOrder=@onBackOrder,
		dtModified = GETDATE()                                    
	WHERE 
		numWareHouseItemID=@numWareHouseItemID   
END

IF @numWOId>0
BEGIN
    
	DECLARE @numReferenceID NUMERIC(18,0)
	DECLARE @tintRefType NUMERIC(18,0)

	IF ISNULL(@numOppId,0)>0 --FROM SALES ORDER 
	BEGIN
		IF @byteMode = 0 --Aseeembly Item
			SET @Description = 'SO-WO Work Order Completed (Qty: ' + CONVERT(varchar(10),@Units) + ')'
		ELSE IF @byteMode =1 --Aseembly Child item
			IF ISNULL(@ParentWOID,0) = 0
			BEGIN
				SET @Description = 'Items Used In SO-WO (Qty: ' + CONVERT(varchar(10),@Units) + ')'
			END
			ELSE
			BEGIN
				SET @Description = 'Items Used In SO-WO Work Order (Qty: ' + CONVERT(varchar(10),@Units) + ')'
			END

		SET @numReferenceID = @numOppId
		SET @tintRefType = 3
	END
	ELSE --FROM CREATE ASSEMBLY SCREEN FROM ITEM
	BEGIN
		IF @byteMode = 0 --Aseeembly Item
			SET @Description = 'Work Order Completed (Qty: ' + CONVERT(varchar(10),@Units) + ')'
		ELSE IF @byteMode =1 --Aseembly Child item
			SET @Description = 'Items Used In Work Order (Qty: ' + CONVERT(varchar(10),@Units) + ')'

		SET @numReferenceID = @numWOId
		SET @tintRefType=2
	END

	EXEC dbo.USP_ManageWareHouseItems_Tracking
				@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
				@numReferenceID = @numReferenceID, --  numeric(9, 0)
				@tintRefType = @tintRefType, --  tinyint
				@vcDescription = @Description, --  varchar(100)
				@numModifiedBy = @numUserCntID,
				@numDomainID = @numDomain
END
ELSE if @byteMode=0
BEGIN
DECLARE @desc AS VARCHAR(100)
SET @desc= 'Create Assembly:' + CONVERT(varchar(10),@Units) + ' Units';

  EXEC dbo.USP_ManageWareHouseItems_Tracking
				@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
				@numReferenceID = @numItemCode, --  numeric(9, 0)
				@tintRefType = 1, --  tinyint
				@vcDescription = @desc, --  varchar(100)
				@numModifiedBy = @numUserCntID,
				@numDomainID = @numDomain
END
ELSE if @byteMode=1
BEGIN
SET @Description = 'Item Used in Assembly (Qty: ' + CONVERT(varchar(10),@Units) + ')' 

EXEC dbo.USP_ManageWareHouseItems_Tracking
				@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
				@numReferenceID = @numItemCode, --  numeric(9, 0)
				@tintRefType = 1, --  tinyint
				@vcDescription = @Description, --  varchar(100)
				@numModifiedBy = @numUserCntID,
				@numDomainID = @numDomain
END 
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SET @numOppID = 0

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_UpdatingInventoryForEmbeddedKits')
DROP PROCEDURE USP_UpdatingInventoryForEmbeddedKits
GO
CREATE PROCEDURE [dbo].[USP_UpdatingInventoryForEmbeddedKits]    
	@numOppItemID as numeric(9),
	@Units as integer,
	@Mode as TINYINT,
	@numOppID AS NUMERIC(9),
	@tintMode AS TINYINT=0, -- 0:Add/Edit 1:Delete
	@numUserCntID AS NUMERIC(9)
as                            


--WITH CTE(numItemKitID,numItemCode,numWareHouseItemId,numQtyItemsReq,numOppChildItemID,
--charItemType,StageLevel,numBaseUnit)
--AS
--(
--select convert(NUMERIC(18,0),0),numItemCode,isnull(Dtl.numWareHouseItemId,0) as numWareHouseItemId
--,convert(NUMERIC(18,0),DTL.numQtyItemsReq * @Units) AS numQtyItemsReq,0 as numOppChildItemID,charItemType,1,ISNULL(numBaseUnit,0)
--from item                                
--INNER join ItemDetails Dtl on numChildItemID=numItemCode
--where  numItemKitID=@numKitId
--
--UNION ALL
--
--select Dtl.numItemKitID,i.numItemCode,isnull(Dtl.numWareHouseItemId,0) as numWareHouseItemId,
--convert(NUMERIC(18,0),DTL.numQtyItemsReq * c.numQtyItemsReq) AS numQtyItemsReq,0 as numOppChildItemID,i.charItemType,c.StageLevel + 1,ISNULL(i.numBaseUnit,0)
--from item i                               
--INNER JOIN ItemDetails Dtl on Dtl.numChildItemID=i.numItemCode
--INNER JOIN CTE c ON Dtl.numItemKitID = c.numItemCode
--where Dtl.numChildItemID!=@numKitId
--)
--
--SELECT * ,ROW_NUMBER() OVER(ORDER BY StageLevel) AS RowNumber INTO #tempKits FROM CTE c where charitemtype='P' and numWareHouseItemId>0 and numWareHouseItemId is not null 
DECLARE @numDomain AS NUMERIC(18,0)
DECLARE @bitReOrderPoint BIT = 0
DECLARE @tintOppType AS TINYINT = 0
DECLARE @numReOrderPointOrderStatus AS NUMERIC(18,0) = 0

SELECT @numDomain = numDomainID, @tintOppType=tintOppType FROM [dbo].[OpportunityMaster] AS OM WHERE [OM].[numOppId] = @numOppID
SELECT @bitReOrderPoint=ISNULL(bitReOrderPoint,0),@numReOrderPointOrderStatus=ISNULL(numReOrderPointOrderStatus,0) FROM Domain WHERE numDomainId = @numDomain

SELECT I.numItemCode,numWareHouseItemId,numQtyItemsReq,numQtyShipped,ROW_NUMBER() OVER(ORDER BY numOppChildItemID) AS RowNumber INTO #tempKits
from OpportunityKitItems OKI join Item I on OKI.numChildItemID=I.numItemCode    
	where charitemtype='P' and numWareHouseItemId>0 and numWareHouseItemId is not null 
			AND OKI.numOppID=@numOppID AND OKI.numOppItemID=@numOppItemID 
  
  --SELECT * FROM #tempKits
  
DECLARE @minRowNumber NUMERIC(9),@maxRowNumber NUMERIC(9),@numWareHouseItemID NUMERIC(9),@numItemCode NUMERIC(18,0)
DECLARE @onHand as numeric(9),@onAllocation as numeric(9),@onOrder as numeric(9),@onBackOrder as numeric(9),@onReOrder AS NUMERIC(18,0)
DECLARE @numUnits as INTEGER,@QtyShipped AS NUMERIC(9)

SELECT  @minRowNumber = MIN(RowNumber),@maxRowNumber = MAX(RowNumber) FROM #tempKits

WHILE  @minRowNumber <= @maxRowNumber
    BEGIN
    
    SELECT @numItemCode=numItemCode,@numWareHouseItemID=numWareHouseItemID,@numUnits=numQtyItemsReq,@QtyShipped=numQtyShipped FROM #tempKits WHERE RowNumber=@minRowNumber
    
    DECLARE @description AS VARCHAR(100)
	IF @Mode=0
			SET @description='SO KIT insert/edit (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'
	ELSE IF @Mode=1
			SET @description='SO KIT Deleted (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'
	ELSE IF @Mode=2
			SET @description='SO KIT Close (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'
	ELSE IF @Mode=3
			SET @description='SO KIT Re-Open (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'
			
    	SELECT @onHand=isnull(numOnHand,0),@onAllocation=isnull(numAllocation,0),                                    
				   @onOrder=isnull(numOnOrder,0),@onBackOrder=isnull(numBackOrder,0),
				   @onReOrder = ISNULL(numReorder,0)                                     
					from WareHouseItems where numWareHouseItemID=@numWareHouseItemID 
					
    	IF @Mode=0 --insert/edit
    	BEGIN
    		SET @numUnits = @numUnits - @QtyShipped
    	
		    IF @onHand>=@numUnits                                    
			BEGIN                                    
				SET @onHand=@onHand-@numUnits                            
				SET @onAllocation=@onAllocation+@numUnits                                    
			END                                    
			ELSE IF @onHand<@numUnits                                    
			BEGIN                                    
				SET @onAllocation=@onAllocation+@onHand                                    
				SET @onBackOrder=@onBackOrder+@numUnits-@onHand                                    
				SET @onHand=0                                    
			END  
			
			-- IF REORDER POINT IS ENABLED FROM GLOABAL SETTING THEN CREATE PURCHASE OPPORTUNITY
			If @tintOppType = 1 AND (@onHand + @onOrder <= @onReOrder) AND @onReOrder > 0 AND @bitReOrderPoint = 1
			BEGIN
				BEGIN TRY
					DECLARE @numNewOppID AS NUMERIC(18,0) = 0
					EXEC USP_OpportunityMaster_CreatePO @numNewOppID OUTPUT,@numDomain,@numUserCntID,@numItemCode,@onBackOrder,@numWareHouseItemID,0,0,@numReOrderPointOrderStatus
				END TRY
				BEGIN CATCH
					--WE ARE NOT TROWING ERROR
				END CATCH
			END                       
    	END
        
        ELSE IF @Mode=1 --Revert
			BEGIN
					IF @QtyShipped>0
						BEGIN
							IF @tintMode=0
								SET @numUnits = @numUnits - @QtyShipped
							ELSE IF @tintmode=1
								SET @onAllocation = @onAllocation + @QtyShipped 
						END 
								                    
                    IF @numUnits >= @onBackOrder 
                        BEGIN
                            SET @numUnits = @numUnits - @onBackOrder
                            SET @onBackOrder = 0
                            
                            IF (@onAllocation - @numUnits >= 0)
								SET @onAllocation = @onAllocation - @numUnits
                            SET @onHand = @onHand + @numUnits                                            
                        END                                            
                    ELSE 
                        IF @numUnits < @onBackOrder 
                            BEGIN                  
								IF (@onBackOrder - @numUnits >0)
									SET @onBackOrder = @onBackOrder - @numUnits
									
--								IF @tintmode=1
--									SET @onAllocation = @onAllocation + (@numUnits - @numQtyShipped)
                            END 
			 END
       
		ELSE IF @Mode=2 --Close
			BEGIN
				  SET @numUnits = @numUnits - @QtyShipped
				  
                  SET @onAllocation = @onAllocation - @numUnits            
			 END
		ELSE IF @Mode=3 --Re-Open
			BEGIN
				  SET @numUnits = @numUnits - @QtyShipped
			
                  SET @onAllocation = @onAllocation + @numUnits            
			 END

       update WareHouseItems set numOnHand=@onHand,numAllocation=@onAllocation,                                    
					numBackOrder=@onBackOrder,dtModified = GETDATE()  where numWareHouseItemID=@numWareHouseItemID 
	

		
		EXEC dbo.USP_ManageWareHouseItems_Tracking
	@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
	@numReferenceID = @numOppID, --  numeric(9, 0)
	@tintRefType = 3, --  tinyint
	@vcDescription = @description, --  varchar(100)
	@numModifiedBy = @numUserCntID,
	@numDomainID = @numDomain
	
	SET @minRowNumber=@minRowNumber + 1			 		
     --SELECT  @minRowNumber = MIN(RowNumber) FROM #tempKits WHERE  [RowNumber] > @minRowNumber
    END	
  
  
  DROP TABLE #tempKits

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetItemsInventory_API')
DROP PROCEDURE USP_GetItemsInventory_API
GO
CREATE PROCEDURE [dbo].[USP_GetItemsInventory_API]
          @numDomainID AS NUMERIC(9),
          @WebAPIId    AS int,
          @ModifiedDate AS VARCHAR(50)='' 
AS

		-- GET DEFAULT WAREHOUSE SET FOR MARKET PLACE     
		DECLARE @numDefaultWarehouse AS NUMERIC(18,0)
		SELECT @numDefaultWarehouse=numWareHouseID FROM WebAPIDetail WHERE numDomainID=@numDomainID AND WebApiId=@WebAPIId

		DECLARE @TempItem TABLE
		(
			numDomainID NUMERIC(18,0),
			numItemCode NUMERIC(18,0),
			vcItemName VARCHAR(300),
			vcModelID VARCHAR(200),
			numItemGroup NUMERIC(18,0),
			fltWeight float,
			fltLength float,
			fltHeight float,
			fltWidth float,
			vcManufacturer VARCHAR(250),
			IsArchieve BIT,
			txtItemDesc VARCHAR(1000),
			vcSKU VARCHAR(50),
			numBarCodeId VARCHAR(50),
			bintCreatedDate DATETIME,
			bintModifiedDate DATETIME
		)

		INSERT INTO 
			@TempItem
		SELECT
			numDomainID,
			numItemCode,
			ISNULL(vcItemName,''),
			ISNULL(vcModelID,''),
			ISNULL(numItemGroup,0) numItemGroup,
			ISNULL(fltWeight,0) fltWeight,
			ISNULL(fltLength,0) fltLength,
			ISNULL(fltHeight,0) fltHeight,
			ISNULL(fltWidth,0) fltWidth,
			ISNULL(vcManufacturer,'') vcManufacturer,
			ISNULL(IsArchieve,0) IsArchieve,
			ISNULL(txtItemDesc,'') txtItemDesc,
			ISNULL(vcSKU,''),
			ISNULL(numBarCodeId,''),
			bintCreatedDate,
			bintModifiedDate
		FROM
			Item
		WHERE
			Item.numDomainID = @numDomainID
			AND  LEN(Item.vcItemName) > 0
			AND Item.charItemType ='P'
			AND Item.vcExportToAPI IS NOT NULL
			AND @WebAPIId IN (SELECT * FROM dbo.Split(vcExportToAPI,','))

		SELECT
			X.*
		FROM
		(
			SELECT
				Item.numDomainID,
				ISNULL(ItemAPI.vcAPIItemID,0) AS vcAPIItemID,
				Item.numItemCode,
				Item.vcItemName,
				Item.vcModelID,
				Item.numItemGroup,
				Item.fltWeight AS intWeight,
				Item.fltWeight,
				Item.fltLength,
				Item.fltHeight,
				Item.fltWidth,
				Item.vcManufacturer,
				Item.IsArchieve,
				Item.txtItemDesc,
				Item.vcSKU,
				Item.numBarCodeId,
				TempWarehouseItem.numOnHand AS QtyOnHand,
				TempWarehouseItem.monWListPrice AS monListPrice,
				(
					SELECT 
						numItemImageId, 
						vcPathForImage, 
						vcPathForTImage, 
						bitDefault,
						CASE 
							WHEN bitdefault = 1 THEN -1 
							ELSE isnull(intDisplayOrder,0) 
						END AS intDisplayOrder 
					FROM 
						ItemImages  
					WHERE 
						numItemCode=Item.numItemCode 
					ORDER BY 
						CASE WHEN bitdefault=1 THEN -1 ELSE ISNULL(intDisplayOrder,0) END  asc 
					FOR XML AUTO,ROOT('Images')
				) AS xmlItemImages,
				CAST(ISNULL(ItemExtendedDetails.txtDesc,'') AS VARCHAR(MAX)) AS vcExtendedDescToAPI
			FROM
				@TempItem Item
			LEFT JOIN
				ItemExtendedDetails
			ON
				ItemExtendedDetails.numItemCode = Item.numItemCode
			LEFT JOIN 
				ItemAPI 
			ON 
				ItemAPI.WebApiId = @WebAPIId 
				AND Item.numItemCode = ItemAPI.numItemID 
				AND Item.vcSKU = ItemAPI.vcSKU
			CROSS APPLY
			(
				SELECT
					ISNULL(SUM(numOnHand),0) AS numOnHand,
					ISNULL(MAX(monWListPrice),0) AS monWListPrice,
					MAX(dtModified) AS dtModified
				FROM
					WareHouseItems
				WHERE
					WareHouseItems.numItemID = Item.numItemCode
					AND WareHouseItems.numWareHouseID = @numDefaultWarehouse
				GROUP BY
					numWareHouseItemID
			) TempWarehouseItem
			WHERE
				numItemGroup = 0 --INVENTORY ITEM
				AND 1 = (CASE 
							WHEN @ModifiedDate = '' THEN 1
							ELSE (CASE 
									WHEN TempWarehouseItem.dtModified > @ModifiedDate THEN 1
									ELSE 0
								  END)
						END) 
			UNION
			SELECT
				Item.numDomainID,
				ISNULL(ItemAPI.vcAPIItemID,0) AS vcAPIItemID,
				Item.numItemCode,
				Item.vcItemName,
				Item.vcModelID,
				Item.numItemGroup,
				Item.fltWeight AS intWeight,
				Item.fltWeight,
				Item.fltLength,
				Item.fltHeight,
				Item.fltWidth,
				Item.vcManufacturer,
				Item.IsArchieve,
				Item.txtItemDesc,
				ISNULL(WareHouseItems.vcWHSKU,'') vcSKU,
				ISNULL(WareHouseItems.vcBarCode,'') numBarCodeId,
				WareHouseItems.numOnHand AS QtyOnHand,
				WareHouseItems.monWListPrice AS monListPrice,
				(
					SELECT 
						numItemImageId, 
						vcPathForImage, 
						vcPathForTImage, 
						bitDefault,
						CASE 
							WHEN bitdefault = 1 THEN -1 
							ELSE isnull(intDisplayOrder,0) 
						END AS intDisplayOrder 
					FROM 
						ItemImages  
					WHERE 
						numItemCode=Item.numItemCode 
					ORDER BY 
						CASE WHEN bitdefault=1 THEN -1 ELSE ISNULL(intDisplayOrder,0) END ASC 
					FOR XML AUTO,ROOT('Images')
				) AS xmlItemImages,
				CAST(ISNULL(ItemExtendedDetails.txtDesc,'') AS VARCHAR(MAX)) AS vcExtendedDescToAPI
			FROM
				@TempItem Item
			INNER JOIN
				WareHouseItems
			ON
				WareHouseItems.numItemID = Item.numItemCode
				AND WareHouseItems.numWareHouseID = @numDefaultWarehouse
			LEFT JOIN
				ItemExtendedDetails
			ON
				ItemExtendedDetails.numItemCode = Item.numItemCode
			LEFT JOIN 
				ItemAPI 
			ON 
				ItemAPI.WebApiId = @WebAPIId 
				AND Item.numItemCode = ItemAPI.numItemID 
				AND Item.vcSKU = ItemAPI.vcSKU
			WHERE
				numItemGroup > 0 --MATRIX ITEM
				AND 1 = (CASE 
							WHEN @ModifiedDate = '' THEN 1
							ELSE (CASE 
									WHEN WareHouseItems.dtModified > @ModifiedDate THEN 1
									ELSE 0
								  END)
						END) 
		) AS X
		ORDER BY
			X.numItemCode ASC
