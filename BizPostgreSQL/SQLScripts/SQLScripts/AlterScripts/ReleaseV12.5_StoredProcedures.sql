/******************************************************************
Project: Release 12.5 Date: 03.SEP.2019
Comments: STORE PROCEDURES
*******************************************************************/

     
/****** Object:  UserDefinedFunction [dbo].[fn_GetOpeningBalance]    Script Date: 10/05/2009 17:46:28 ******/

GO

GO
--DROP FUNCTION [dbo].[fn_GetOpeningBalance]
-- SELECT * FROM dbo.fn_GetOpeningBalance(',74,666,658,77,263,668,1156,1162,1166,1167,1170,1172,1174',72,'2009-01-01 00:00:00:000')
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='TF'AND NAME ='fn_getopeningbalance')
DROP FUNCTION fn_getopeningbalance
GO
CREATE FUNCTION [dbo].[fn_GetOpeningBalance]
(@vcChartAcntId as varchar(500),
@numDomainId as numeric(9),
@dtFromDate as DATETIME,
@ClientTimeZoneOffset Int  --Added by Chintan to enable calculation of date according to client machine
)                                 
RETURNS 

@COAOpeningBalance table
(
	numAccountId int,
	vcAccountName varchar(100),
	numParntAcntTypeID int,
	vcAccountDescription varchar(100),
	vcAccountCode varchar(50),
	dtAsOnDate datetime,
	mnOpeningBalance DECIMAL(20,5)
)                               
AS                                  
BEGIN          
SET @dtFromDate = DateAdd(minute, -@ClientTimeZoneOffset, @dtFromDate);

	INSERT INTO 
		@COAOpeningBalance
	SELECT 
		COA.numAccountId
		,vcAccountName
		,numParntAcntTypeID
		,vcAccountDescription
		,vcAccountCode
		,@dtFromDate
		,(SELECT 
			ISNULL(SUM(GJD.numDebitAmt),0) - ISNULL(SUM(GJD.numCreditAmt),0)
		FROM
			General_Journal_Details GJD 
		JOIN 
			Domain D 
		ON 
			D.numDOmainID = GJD.numDomainID 
		AND 
			COA.numDomainId = D.numDOmainID
		INNER JOIN
			General_Journal_Header GJH 
		ON 
			GJD.numJournalId =GJH.numJournal_Id 
			AND GJD.numDomainId=COA.numDomainId 
			AND GJD.numChartAcntId=COA.numAccountId 
			AND GJH.numDomainID = D.numDomainId 
			AND GJH.datEntry_Date <= @dtFromDate)
	FROM 
		Chart_of_Accounts COA
	WHERE 
		COA.numDomainId=@numDomainId 
		AND COA.numAccountId in (select CAST(ID AS NUMERIC(9)) from SplitIDs(@vcChartAcntId,','));

	RETURN 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CalculateCommission')
DROP PROCEDURE USP_CalculateCommission
GO
CREATE PROCEDURE [dbo].[USP_CalculateCommission]
	-- Add the parameters for the stored procedure here
	 @numComPayPeriodID AS NUMERIC(18,0),
	 @numDomainID AS NUMERIC(18,0),
	 @ClientTimeZoneOffset INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
SET NOCOUNT ON;

BEGIN TRY
BEGIN TRANSACTION
	DECLARE @dtPayStart DATE
	DECLARE @dtPayEnd DATE

	DECLARE @TempBizCommission TABLE
	(
		numUserCntID NUMERIC(18,0),
		numOppID NUMERIC(18,0),
		numOppBizDocID NUMERIC(18,0),
		numOppBizDocItemID NUMERIC(18,0),
		monCommission DECIMAL(20,5),
		numComRuleID NUMERIC(18,0),
		tintComType TINYINT,
		tintComBasedOn TINYINT,
		decCommission FLOAT,
		tintAssignTo TINYINT,
		numOppItemID NUMERIC(18,0),
		monInvoiceSubTotal DECIMAL(20,5),
		monOrderSubTotal DECIMAL(20,5)
	)

	DECLARE @TABLEPAID TABLE
	(
		ID INT IDENTITY(1,1),
		numDivisionID NUMERIC(18,0),
		numRelationship NUMERIC(18,0),
		numProfile NUMERIC(18,0),
		numAssignedTo NUMERIC(18,0),
		numRecordOwner NUMERIC(18,0),
		numItemCode NUMERIC(18,0),
		numItemClassification NUMERIC(18,0),
		numOppID NUMERIC(18,0),
		numOppItemID NUMERIC(18,0),
		numOppBizDocID NUMERIC(18,0),
		numOppBizDocItemID NUMERIC(18,0),
		numUnitHour FLOAT,
		monTotAmount DECIMAL(20,5),
		monInvoiceSubTotal DECIMAL(20,5),
		monOrderSubTotal DECIMAL(20,5),
		monVendorCost DECIMAL(20,5),
		monAvgCost DECIMAL(20,5),
		numPartner NUMERIC(18,0)
	)

	SELECT 
		@dtPayStart=dtStart
		,@dtPayEnd=dtEnd 
	FROM 
		CommissionPayPeriod 
	WHERE 
		numComPayPeriodID = @numComPayPeriodID

	--GET COMMISSION GLOBAL SETTINGS
	DECLARE @numDiscountServiceItemID NUMERIC(18,0)
	DECLARE @numShippingServiceItemID NUMERIC(18,0)
	DECLARE @tintCommissionType TINYINT
	DECLARE @bitCommissionBasedOn BIT
	DECLARE @tintCommissionBasedOn TINYINT
	DECLARE @bitIncludeTaxAndShippingInCommission BIT

	SELECT 
		@numDiscountServiceItemID=numDiscountServiceItemID, 
		@numShippingServiceItemID = numShippingServiceItemID, 
		@tintCommissionType=tintCommissionType, 
		@bitIncludeTaxAndShippingInCommission=bitIncludeTaxAndShippingInCommission
		,@tintCommissionBasedOn=ISNULL(tintCommissionBasedOn,1)
		,@bitCommissionBasedOn=ISNULL(bitCommissionBasedOn,0)
	FROM 
		Domain 
	WHERE 
		numDomainID=@numDomainID

	DELETE 
		BC 
	FROM 
		BizDocComission BC
	LEFT JOIN
		OpportunityMaster OM
	ON
		BC.numOppID = OM.numOppId
	WHERE 
		BC.numDomainId=@numDomainID
		AND ISNULL(BC.numOppID,0) > 0
		AND ISNULL(BC.bitCommisionPaid,0)=0
		AND OM.numOppId IS NULL

	DELETE 
		BC 
	FROM 
		BizDocComission BC
	LEFT JOIN
		OpportunityItems OI
	ON
		BC.numOppItemID = OI.numoppitemtCode
	WHERE 
		BC.numDomainId=@numDomainID 
		AND ISNULL(BC.numOppItemID,0) > 0
		AND ISNULL(BC.bitCommisionPaid,0)=0
		AND OI.numoppitemtCode IS NULL

	DELETE 
		BC 
	FROM 
		BizDocComission BC
	LEFT JOIN
		OpportunityBizDocs OBD
	ON
		BC.numOppBizDocId = OBD.numOppBizDocsId
	WHERE 
		BC.numDomainId=@numDomainID
		AND ISNULL(BC.numOppBizDocId,0) > 0
		AND ISNULL(BC.bitCommisionPaid,0)=0
		AND OBD.numOppBizDocsId IS NULL

	DELETE 
		BC 
	FROM 
		BizDocComission BC
	LEFT JOIN
		OpportunityBizDocItems OBDI
	ON
		BC.numOppBizDocItemID = OBDI.numOppBizDocItemID
	WHERE 
		BC.numDomainId=@numDomainID 
		AND ISNULL(BC.numOppBizDocItemID,0) > 0
		AND ISNULL(BC.bitCommisionPaid,0)=0
		AND OBDI.numOppBizDocItemID IS NULL

	-- DELETE UNPAID COMMISSION ENTRIES BETWEEN PAY PERIOD DATE RANGE	
	IF @tintCommissionType = 3 --Sales Order Sub-Total Amount
	BEGIN
		DELETE FROM 
			BizDocComission 
		WHERE 
			numDomainId=@numDomainID 
			AND ISNULL(bitCommisionPaid,0)=0 
			AND numOppItemID IN (SELECT 
									OI.numoppitemtCode
								FROM 
									OpportunityMaster OM
								INNER JOIN
									OpportunityItems OI
								ON
									OM.numOppId = OI.numOppId
								WHERE
									OM.numDomainId = @numDomainID 
									AND OM.tintOppType = 1
									AND OM.tintOppStatus = 1
									AND CAST(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,OM.bintCreatedDate) AS DATE) BETWEEN @dtPayStart AND @dtPayEnd)

		INSERT INTO 
			@TABLEPAID
		SELECT 
			OM.numDivisionID,
			ISNULL(CompanyInfo.numCompanyType,0),
			ISNULL(CompanyInfo.vcProfile,0),
			OM.numAssignedTo,
			OM.numRecOwner,
			OI.numItemCode,
			Item.numItemClassification,
			OM.numOppID,
			OI.numoppitemtCode,
			0,
			0,
			ISNULL(OI.numUnitHour,0),
			ISNULL(OI.monTotAmount,0),
			0,
			ISNULL(OI.monTotAmount,0),
			(ISNULL(OI.monVendorCost,0) * dbo.fn_UOMConversion(Item.numPurchaseUnit,Item.numItemCode,@numDomainID,Item.numBaseUnit) * ISNULL(OI.numUnitHour,0)),
			(ISNULL(OI.monAvgCost,0) * ISNULL(OI.numUnitHour,0)),
			OM.numPartner
		FROM 
			OpportunityMaster OM
		INNER JOIN
			DivisionMaster 
		ON
			OM.numDivisionID = DivisionMaster.numDivisionID
		INNER JOIN
			CompanyInfo
		ON
			DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
		INNER JOIN
			OpportunityItems OI 
		ON
			OM.numOppId = OI.numOppId
		INNER JOIN
			Item
		ON
			OI.numItemCode = Item.numItemCode
		LEFT JOIN
			BizDocComission
		ON
			OM.numOppID = BizDocComission.numOppID
			AND OI.numoppitemtCode = BizDocComission.numOppItemID
			AND ISNULL(bitCommisionPaid,0) = 1
		WHERE
			OM.numDomainId = @numDomainID 
			AND OM.tintOppType = 1
			AND OM.tintOppStatus = 1
			AND CAST(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,OM.bintCreatedDate) AS DATE) BETWEEN @dtPayStart AND @dtPayEnd
			AND BizDocComission.numComissionID IS NULL
			AND OI.numItemCode NOT IN ( @numDiscountServiceItemID )  /*DO NOT INCLUDE DISCOUNT ITEM*/
			AND 1 = (
						CASE 
							WHEN OI.numItemCode = @numShippingServiceItemID 
							THEN 
								CASE 
									WHEN ISNULL(@bitIncludeTaxAndShippingInCommission,0) = 1
									THEN	
										1
									ELSE	
										0
								END
							ELSE 
								1 
						END
					)/*CONDITION TO INCLUDE/EXCLUDE INCLUDE SHIPPING ITEM */
	END
	ELSE IF @tintCommissionType = 2 --Invoice Sub-Total Amount (Paid or Unpaid)
	BEGIN
		DELETE FROM 
			BizDocComission 
		WHERE 
			numDomainId=@numDomainID
			AND ISNULL(numOppBizDocID,0) = 0
			AND ISNULL(bitCommisionPaid,0)=0 
			AND numOppItemID IN (SELECT 
									OI.numoppitemtCode
								FROM 
									OpportunityMaster OM
								INNER JOIN
									OpportunityItems OI
								ON
									OM.numOppId = OI.numOppId
								WHERE
									OM.numDomainId = @numDomainID 
									AND OM.tintOppType = 1
									AND OM.tintOppStatus = 1
									AND CAST(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,OM.bintCreatedDate) AS DATE) BETWEEN @dtPayStart AND @dtPayEnd)

		DELETE FROM 
			BizDocComission 
		WHERE 
			numDomainId=@numDomainID 
			AND ISNULL(bitCommisionPaid,0)=0 
			AND numOppBizDocItemID IN (SELECT 
											ISNULL(numOppBizDocItemID,0)
										FROM 
											OpportunityBizDocs OppBiz
										INNER JOIN
											OpportunityMaster OppM
										ON
											OppBiz.numOppID = OppM.numOppID
										INNER JOIN
											OpportunityBizDocItems OppBizItems
										ON
											OppBiz.numOppBizDocsId = OppBizItems.numOppBizDocID
										OUTER APPLY
										(
											SELECT 
												MAX(DM.dtDepositDate) dtDepositDate
											FROM 
												DepositMaster DM 
											JOIN 
												dbo.DepositeDetails DD
											ON 
												DM.numDepositId=DD.numDepositID 
											WHERE 
												DM.tintDepositePage=2 
												AND DM.numDomainId=@numDomainId
												AND DD.numOppID=OppBiz.numOppID 
												AND DD.numOppBizDocsID =OppBiz.numOppBizDocsID
										) TempDepositMaster
										WHERE
											OppM.numDomainId = @numDomainID 
											AND OppM.tintOppType = 1
											AND OppM.tintOppStatus = 1
											AND OppBiz.bitAuthoritativeBizDocs = 1
											AND ((TempDepositMaster.dtDepositDate IS NOT NULL AND (CAST(TempDepositMaster.dtDepositDate AS DATE) BETWEEN @dtPayStart AND @dtPayEnd)) OR CAST(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,OppBiz.dtCreatedDate) AS DATE) BETWEEN @dtPayStart AND @dtPayEnd))
		INSERT INTO 
			@TABLEPAID
		SELECT 
			OM.numDivisionID,
			ISNULL(CompanyInfo.numCompanyType,0),
			ISNULL(CompanyInfo.vcProfile,0),
			OM.numAssignedTo,
			OM.numRecOwner,
			OppBizItems.numItemCode,
			Item.numItemClassification,
			OM.numOppID,
			OppMItems.numoppitemtCode,
			OppBizItems.numOppBizDocID,
			OppBizItems.numOppBizDocItemID,
			ISNULL(OppBizItems.numUnitHour,0),
			ISNULL(OppBizItems.monTotAmount,0),
			ISNULL(OppBizItems.monTotAmount,0),
			ISNULL(OppMItems.monTotAmount,0),
			(ISNULL(OppMItems.monVendorCost,0) * dbo.fn_UOMConversion(Item.numPurchaseUnit,Item.numItemCode,@numDomainID,Item.numBaseUnit) * ISNULL(OppBizItems.numUnitHour,0)),
			(ISNULL(OppMItems.monAvgCost,0) * ISNULL(OppBizItems.numUnitHour,0)),
			OM.numPartner
		FROM 
			OpportunityMaster OM
		INNER JOIN	
			OpportunityBizDocs OBD
		ON
			OM.numOppId = OBD.numOppId
		INNER JOIN
			DivisionMaster 
		ON
			OM.numDivisionID = DivisionMaster.numDivisionID
		INNER JOIN
			CompanyInfo
		ON
			DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
		INNER JOIN
			OpportunityBizDocItems OppBizItems
		ON
			OBD.numOppBizDocsId = OppBizItems.numOppBizDocID
		INNER JOIN
			OpportunityItems OppMItems 
		ON
			OppBizItems.numOppItemID = OppMItems.numoppitemtCode
		INNER JOIN
			Item
		ON
			OppBizItems.numItemCode = Item.numItemCode
		LEFT JOIN
			BizDocComission BDC1
		ON
			OM.numOppID = BDC1.numOppID
			AND OppMItems.numoppitemtCode = BDC1.numOppItemID
			AND ISNULL(bitCommisionPaid,0) = 1
		LEFT JOIN
			BizDocComission BDC2
		ON
			OM.numOppID = BDC2.numOppID
			AND OppMItems.numoppitemtCode = BDC2.numOppItemID
			AND ISNULL(BDC2.bitCommisionPaid,0) = 1
			AND OppBizItems.numOppBizDocID = BDC2.numOppBizDocId 
			AND OppBizItems.numOppBizDocItemID =  BDC2.numOppBizDocItemID
		WHERE
			OM.numDomainId = @numDomainID 
			AND OM.tintOppType = 1
			AND OM.tintOppStatus = 1
			AND OBD.bitAuthoritativeBizDocs = 1
			AND CAST(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,OBD.dtCreatedDate) AS DATE) BETWEEN @dtPayStart AND @dtPayEnd
			AND OppBizItems.numItemCode NOT IN ( @numDiscountServiceItemID )  /*DO NOT INCLUDE DISCOUNT ITEM*/
			AND BDC1.numComissionID IS NULL
			AND BDC2.numComissionID IS NULL
			AND 1 = (
						CASE 
							WHEN OppBizItems.numItemCode = @numShippingServiceItemID 
							THEN 
								CASE 
									WHEN ISNULL(@bitIncludeTaxAndShippingInCommission,0) = 1
									THEN	
										1
									ELSE	
										0
								END
							ELSE 
								1 
						END
					)/*CONDITION TO INCLUDE/EXCLUDE INCLUDE SHIPPING ITEM */
	END
	ELSE
	BEGIN
		DELETE FROM 
			BizDocComission 
		WHERE 
			numDomainId=@numDomainID
			AND ISNULL(numOppBizDocID,0) = 0
			AND ISNULL(bitCommisionPaid,0)=0 
			AND numOppItemID IN (SELECT 
									OI.numoppitemtCode
								FROM 
									OpportunityMaster OM
								INNER JOIN
									OpportunityItems OI
								ON
									OM.numOppId = OI.numOppId
								WHERE
									OM.numDomainId = @numDomainID 
									AND OM.tintOppType = 1
									AND OM.tintOppStatus = 1
									AND CAST(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,OM.bintCreatedDate) AS DATE) BETWEEN @dtPayStart AND @dtPayEnd)

		DELETE FROM 
			BizDocComission 
		WHERE 
			numDomainId=@numDomainID 
			AND ISNULL(bitCommisionPaid,0)=0 
			AND numOppBizDocItemID IN (SELECT 
											ISNULL(numOppBizDocItemID,0)
										FROM 
											OpportunityBizDocs OppBiz
										INNER JOIN
											OpportunityMaster OppM
										ON
											OppBiz.numOppID = OppM.numOppID
										INNER JOIN
											OpportunityBizDocItems OppBizItems
										ON
											OppBiz.numOppBizDocsId = OppBizItems.numOppBizDocID
										OUTER APPLY
										(
											SELECT 
												MAX(DM.dtDepositDate) dtDepositDate
											FROM 
												DepositMaster DM 
											JOIN 
												dbo.DepositeDetails DD
											ON 
												DM.numDepositId=DD.numDepositID 
											WHERE 
												DM.tintDepositePage=2 
												AND DM.numDomainId=@numDomainId
												AND DD.numOppID=OppBiz.numOppID 
												AND DD.numOppBizDocsID =OppBiz.numOppBizDocsID
										) TempDepositMaster
										WHERE
											OppM.numDomainId = @numDomainID 
											AND OppM.tintOppType = 1
											AND OppM.tintOppStatus = 1
											AND OppBiz.bitAuthoritativeBizDocs = 1
											AND ((TempDepositMaster.dtDepositDate IS NOT NULL AND (CAST(TempDepositMaster.dtDepositDate AS DATE) BETWEEN @dtPayStart AND @dtPayEnd)) OR CAST(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,OppBiz.dtCreatedDate) AS DATE) BETWEEN @dtPayStart AND @dtPayEnd))

		INSERT INTO 
			@TABLEPAID
		SELECT 
			OppM.numDivisionID,
			ISNULL(CompanyInfo.numCompanyType,0),
			ISNULL(CompanyInfo.vcProfile,0),
			OppM.numAssignedTo,
			OppM.numRecOwner,
			OppBizItems.numItemCode,
			Item.numItemClassification,
			OppM.numOppID,
			OppMItems.numoppitemtCode,
			OppBizItems.numOppBizDocID,
			OppBizItems.numOppBizDocItemID,
			ISNULL(OppBizItems.numUnitHour,0),
			ISNULL(OppBizItems.monTotAmount,0),
			ISNULL(OppBizItems.monTotAmount,0),
			ISNULL(OppMItems.monTotAmount,0),
			(ISNULL(OppMItems.monVendorCost,0) * dbo.fn_UOMConversion(Item.numPurchaseUnit,Item.numItemCode,@numDomainID,Item.numBaseUnit) * ISNULL(OppBizItems.numUnitHour,0)),
			(ISNULL(OppMItems.monAvgCost,0) * ISNULL(OppBizItems.numUnitHour,0)),
			OppM.numPartner
		FROM 
			OpportunityBizDocs OppBiz
		INNER JOIN
			OpportunityMaster OppM
		ON
			OppBiz.numOppID = OppM.numOppID
		INNER JOIN
			DivisionMaster 
		ON
			OppM.numDivisionID = DivisionMaster.numDivisionID
		INNER JOIN
			CompanyInfo
		ON
			DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
		INNER JOIN
			OpportunityBizDocItems OppBizItems
		ON
			OppBiz.numOppBizDocsId = OppBizItems.numOppBizDocID
		INNER JOIN
			OpportunityItems OppMItems 
		ON
			OppBizItems.numOppItemID = OppMItems.numoppitemtCode
		INNER JOIN
			Item
		ON
			OppBizItems.numItemCode = Item.numItemCode
		LEFT JOIN
			BizDocComission BDC1
		ON
			OppM.numOppID = BDC1.numOppID
			AND OppMItems.numoppitemtCode = BDC1.numOppItemID
			AND ISNULL(bitCommisionPaid,0) = 1
		LEFT JOIN
			BizDocComission BDC2
		ON
			OppM.numOppID = BDC2.numOppID
			AND OppMItems.numoppitemtCode = BDC2.numOppItemID
			AND ISNULL(BDC2.bitCommisionPaid,0) = 1
			AND OppBizItems.numOppBizDocID = BDC2.numOppBizDocId 
			AND OppBizItems.numOppBizDocItemID =  BDC2.numOppBizDocItemID
		OUTER APPLY
		(
			SELECT 
				MAX(DM.dtDepositDate) dtDepositDate
			FROM 
				DepositMaster DM 
			JOIN 
				dbo.DepositeDetails DD
			ON 
				DM.numDepositId=DD.numDepositID 
			WHERE 
				DM.tintDepositePage=2 
				AND DM.numDomainId=@numDomainId
				AND DD.numOppID=OppBiz.numOppID 
				AND DD.numOppBizDocsID =OppBiz.numOppBizDocsID
		) TempDepositMaster
		WHERE
			OppM.numDomainId = @numDomainID 
			AND OppM.tintOppType = 1
			AND OppM.tintOppStatus = 1
			AND OppBiz.bitAuthoritativeBizDocs = 1
			AND BDC1.numComissionID IS NULL
			AND BDC2.numComissionID IS NULL
			AND ISNULL(OppBiz.monAmountPaid,0) >= OppBiz.monDealAmount
			AND (TempDepositMaster.dtDepositDate IS NOT NULL AND (CAST(TempDepositMaster.dtDepositDate AS DATE) BETWEEN @dtPayStart AND @dtPayEnd))
			AND OppBizItems.numItemCode NOT IN ( @numDiscountServiceItemID )  /*DO NOT INCLUDE DISCOUNT ITEM*/
			AND 1 = (
						CASE 
							WHEN OppBizItems.numItemCode = @numShippingServiceItemID 
							THEN 
								CASE 
									WHEN ISNULL(@bitIncludeTaxAndShippingInCommission,0) = 1
									THEN	
										1
									ELSE	
										0
								END
							ELSE 
								1 
						END
					)/*CONDITION TO INCLUDE/EXCLUDE INCLUDE SHIPPING ITEM */
	END	

	--GET COMMISSION RULE OF DOMAIN
	DECLARE @TempCommissionRule TABLE
	(
		ID INT IDENTITY(1,1),
		numComRuleID NUMERIC(18,0),
		tintComBasedOn TINYINT,
		tintComType TINYINT,
		tintComAppliesTo TINYINT,
		tintComOrgType TINYINT,
		tintAssignTo TINYINT
	)

	INSERT INTO
		@TempCommissionRule
	SELECT
		CR.numComRuleID,
		CR.tintComBasedOn,
		CR.tintComType,
		CR.tintComAppliesTo,
		CR.tintComOrgType,
		CR.tintAssignTo
	FROM
		CommissionRules CR
	LEFT JOIN
		PriceBookPriorities PBP
	ON	
		CR.tintComAppliesTo = PBP.Step2Value 
		AND CR.tintComOrgType = PBP.Step3Value
	WHERE
		CR.numDomainID = @numDomainID
		AND ISNULL(PBP.[Priority],0) > 0
	ORDER BY
		ISNULL(PBP.[Priority],0) ASC

	-- LOOP ALL COMMISSION RULES
	DECLARE @i INT = 1
	DECLARE @COUNT INT = 0

	SELECT @COUNT=COUNT(*) FROM @TempCommissionRule

	DECLARE @TEMPITEM TABLE
	(
		ID INT,
		UniqueID INT,
		numUserCntID NUMERIC(18,0),
		numOppID NUMERIC(18,0),
		numOppBizDocID NUMERIC(18,0),
		numOppBizDocItemID NUMERIC(18,0),
		numItemCode NUMERIC(18,0),
		numUnitHour FLOAT,
		monTotAmount DECIMAL(20,5),
		monVendorCost DECIMAL(20,5),
		monAvgCost DECIMAL(20,5),
		numOppItemID NUMERIC(18,0),
		monInvoiceSubTotal DECIMAL(20,5),
		monOrderSubTotal DECIMAL(20,5)
	)

	WHILE @i <= @COUNT
	BEGIN
		DECLARE @numComRuleID NUMERIC(18,0)
		DECLARE @tintComBasedOn TINYINT
		DECLARE @tintComAppliesTo TINYINT
		DECLARE @tintComOrgType TINYINT
		DECLARE @tintComType TINYINT
		DECLARE @tintAssignTo TINYINT
		DECLARE @numTotalAmount AS DECIMAL(20,5)
		DECLARE @numTotalUnit AS FLOAT

		--CLEAR PREVIOUS VALUES FROM TEMP TABLE
		DELETE FROM @TEMPITEM

		--FETCH COMMISSION RULE
		SELECT @numComRuleID=numComRuleID,@tintComBasedOn=tintComBasedOn,@tintComType=tintComType,@tintComAppliesTo=tintComAppliesTo,@tintComOrgType=tintComOrgType,@tintAssignTo=tintAssignTo FROM @TempCommissionRule WHERE ID=@i

		--FIND ALL BIZ DOC ITEMS WHERE COMMISSION IS NOT PAID AND CURRENT RULE ITEMS, ORGANIZATION AND ASSINGED TO FILTER CAN BE APPLIED
		INSERT INTO
			@TEMPITEM
		SELECT
			ROW_NUMBER() OVER (ORDER BY ID),
			ID,
			(CASE @tintAssignTo WHEN 1 THEN numAssignedTo WHEN 2 THEN numRecordOwner ELSE numPartner END),
			numOppID,
			numOppBizDocID,
			numOppBizDocItemID,
			numItemCode,
			numUnitHour,
			monTotAmount,
			monVendorCost,
			monAvgCost,
			numOppItemID,
			monInvoiceSubTotal,
			monOrderSubTotal
		FROM
			@TABLEPAID
		WHERE
			1 = (CASE @tintComAppliesTo
					--SPECIFIC ITEMS
					WHEN 1 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleItems WHERE numComRuleID = @numComRuleID AND ISNULL(numValue,0) = numItemCode) > 0 THEN 1 ELSE 0 END
					-- ITEM WITH SPECIFIC CLASSIFICATIONS
					WHEN 2 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleItems WHERE numComRuleID = @numComRuleID AND ISNULL(numValue,0) = numItemClassification) > 0 THEN 1 ELSE 0 END
					-- ALL ITEMS
					ELSE 1
				END)
			AND 1 = (CASE @tintComOrgType
						-- SPECIFIC ORGANIZATION
						WHEN 1 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleOrganization WHERE numComRuleID = @numComRuleID AND ISNULL(numValue,0) = numDivisionID) > 0 THEN 1 ELSE 0 END
						-- ORGANIZATION WITH SPECIFIC RELATIONSHIP AND PROFILE
						WHEN 2 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleOrganization WHERE numComRuleID = @numComRuleID AND ISNULL(numValue,0) = numRelationship AND ISNULL(numProfile,0) = numProfile) > 0 THEN 1 ELSE 0 END
						-- ALL ORGANIZATIONS
						ELSE 1
					END)
			AND 1 = (CASE @tintAssignTo 
						-- ORDER ASSIGNED TO
						WHEN 1 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleContacts WHERE numComRuleID = @numComRuleID AND numValue=numAssignedTo) > 0 THEN 1 ELSE 0 END
						-- ORDER OWNER
						WHEN 2 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleContacts WHERE numComRuleID = @numComRuleID AND numValue=numRecordOwner) > 0 THEN 1 ELSE 0 END
						-- Order Partner
						WHEN 3 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleContacts WHERE numComRuleID = @numComRuleID AND numValue=numPartner) > 0 THEN 1 ELSE 0 END
						-- NO OPTION SELECTED IN RULE
						ELSE 0
					END)


		--CALCULATE COMMISSION AND STORE IN TEMPARORY TABLE
		INSERT INTO @TempBizCommission 
		(
			numUserCntID,
			numOppID,
			numOppBizDocID,
			numOppBizDocItemID,
			monCommission,
			numComRuleID,
			tintComType,
			tintComBasedOn,
			decCommission,
			tintAssignTo,
			numOppItemID,
			monInvoiceSubTotal,
			monOrderSubTotal
		)
		SELECT
			numUserCntID,
			numOppID,
			numOppBizDocID,
			numOppBizDocItemID,
			CASE @tintComType 
				WHEN 1 --PERCENT
					THEN 
						CASE 
							WHEN ISNULL(@bitCommissionBasedOn,0) = 1
							THEN
								CASE @tintCommissionBasedOn
									--ITEM GROSS PROFIT (VENDOR COST)
									WHEN 1 THEN (ISNULL(monTotAmount,0) - ISNULL(monVendorCost,0)) * (T2.decCommission / CAST(100 AS FLOAT))
									--ITEM GROSS PROFIT (AVERAGE COST)
									WHEN 2 THEN(ISNULL(monTotAmount,0) - ISNULL(monAvgCost,0)) * (T2.decCommission / CAST(100 AS FLOAT))
								END
							ELSE
								monTotAmount * (T2.decCommission / CAST(100 AS FLOAT))
						END
				ELSE  --FLAT
					T2.decCommission
			END,
			@numComRuleID,
			@tintComType,
			@tintComBasedOn,
			T2.decCommission,
			@tintAssignTo,
			numOppItemID,
			monInvoiceSubTotal,
			monOrderSubTotal
		FROM 
			@TEMPITEM AS T1
		CROSS APPLY
		(
			SELECT TOP 1 
				ISNULL(decCommission,0) AS decCommission
			FROM 
				CommissionRuleDtl 
			WHERE 
				numComRuleID=@numComRuleID 
				AND ISNULL(decCommission,0) > 0
				AND 1 = (CASE 
						WHEN @tintComBasedOn = 1 --BASED ON AMOUNT SOLD 
						THEN (CASE WHEN (T1.monTotAmount BETWEEN intFrom AND intTo) THEN 1 ELSE 0 END)
						WHEN @tintComBasedOn = 2 --BASED ON UNITS SOLD
						THEN (CASE WHEN (T1.numUnitHour BETWEEN intFrom AND intTo) THEN 1 ELSE 0 END)
						ELSE 0
						END)
		) AS T2
		WHERE
			(CASE 
				WHEN ISNULL(@bitCommissionBasedOn,0) = 1
				THEN
					CASE @tintCommissionBasedOn
						--ITEM GROSS PROFIT (VENDOR COST)
						WHEN 1 THEN (ISNULL(monTotAmount,0) - ISNULL(monVendorCost,0)) * (T2.decCommission / CAST(100 AS FLOAT))
						--ITEM GROSS PROFIT (AVERAGE COST)
						WHEN 2 THEN(ISNULL(monTotAmount,0) - ISNULL(monAvgCost,0)) * (T2.decCommission / CAST(100 AS FLOAT))
					END
				ELSE
					monTotAmount * (T2.decCommission / CAST(100 AS FLOAT))
			END) > 0 
			AND (
					SELECT 
						COUNT(*) 
					FROM 
						@TempBizCommission TempBDC
					WHERE 
						TempBDC.numUserCntID=T1.numUserCntID 
						AND TempBDC.numOppID=T1.numOppID 
						AND TempBDC.numOppBizDocID = T1.numOppBizDocID 
						AND TempBDC.numOppBizDocItemID = T1.numOppBizDocItemID
						AND TempBDC.numOppItemID = T1.numOppItemID 
						AND tintAssignTo = @tintAssignTo
				) = 0

		SET @i = @i + 1

		SET @numComRuleID = 0
		SET @tintComBasedOn = 0
		SET @tintComAppliesTo = 0
		SET @tintComOrgType = 0
		SET @tintComType = 0
		SET @tintAssignTo = 0
		SET @numTotalAmount = 0
		SET @numTotalUnit = 0
	END

	--INSERT CALCULATED ENTERIES IN BIZDOCCOMMISSION TABLE
	INSERT INTO BizDocComission
	(	
		numDomainID,
		numUserCntID,
		numOppBizDocId,
		numComissionAmount,
		numOppBizDocItemID,
		numComRuleID,
		tintComType,
		tintComBasedOn,
		decCommission,
		numOppID,
		bitFullPaidBiz,
		numComPayPeriodID,
		tintAssignTo,
		numOppItemID,
		monInvoiceSubTotal,
		monOrderSubTotal,
		bitDomainCommissionBasedOn,
		tintDomainCommissionBasedOn
	)
	SELECT
		@numDomainID,
		numUserCntID,
		numOppBizDocID,
		monCommission,
		numOppBizDocItemID,
		numComRuleID,
		tintComType,
		tintComBasedOn,
		decCommission,
		numOppID,
		1,
		@numComPayPeriodID,
		tintAssignTo,
		numOppItemID,
		monInvoiceSubTotal,
		monOrderSubTotal,
		@bitCommissionBasedOn,
		@tintCommissionBasedOn
	FROM
		@TempBizCommission
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CalculateCommissionOverPayment')
DROP PROCEDURE USP_CalculateCommissionOverPayment
GO
CREATE PROCEDURE [dbo].[USP_CalculateCommissionOverPayment]
	 @numDomainID AS NUMERIC(18,0)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
SET NOCOUNT ON;
	DECLARE @dtPayStart DATE
	DECLARE @dtPayEnd DATE

	DECLARE @TABLEPAID TABLE
	(
		ID INT IDENTITY(1,1),
		numComissionID NUMERIC(18,0),
		numComRuleID NUMERIC(18,0),
		tintComType TINYINT,
		tintComBasedOn TINYINT,
		bitDomainCommissionBasedOn BIT,
		tintDomainCommissionBasedOn TINYINT,
		numUnitHour FLOAT,
		monVendorCost DECIMAL(20,5),
		monAvgCost DECIMAL(20,5),
		monTotAmount DECIMAL(20,5),
		decCommission DECIMAL(18,2),
		monCommission DECIMAL(20,5)
	)

	DECLARE @tintCommissionType TINYINT

	SELECT 
		@tintCommissionType=tintCommissionType
	FROM 
		Domain 
	WHERE 
		numDomainID=@numDomainID

	DELETE 
		BDCPD
	FROM 
		BizDocComissionPaymentDifference BDCPD 
	INNER JOIN 
		BizDocComission BC 
	ON 
		BDCPD.numComissionID = BC.numComissionID 
	WHERE 
		BC.numDomainID=@numDomainID 
		AND ISNULL(bitDifferencePaid,0) = 0

	INSERT INTO BizDocComissionPaymentDifference
	(
		numComissionID
		,tintReason
		,vcReason
		,monDifference
		,bitDifferencePaid
	)
	SELECT
		BC.numComissionID
		,1
		,'Sales Order deleted'
		,ISNULL(numComissionAmount,0)  * -1
		,0
	FROM 
		BizDocComission BC
	LEFT JOIN
		BizDocComissionPaymentDifference BCPD
	ON
		BC.numComissionID = BCPD.numComissionID
		AND BCPD.tintReason = 1
	LEFT JOIN
		OpportunityMaster OM
	ON
		BC.numOppID = OM.numOppId
	WHERE 
		BC.numDomainId=@numDomainID 
		AND ISNULL(BC.numOppID,0) > 0
		AND ISNULL(BC.bitCommisionPaid,0)=1
		AND ISNULL(BC.bitNewImplementation,0) = 1
		AND OM.numOppId IS NULL
		AND BCPD.ID IS NULL 

	INSERT INTO BizDocComissionPaymentDifference
	(
		numComissionID
		,tintReason
		,vcReason
		,monDifference
		,bitDifferencePaid
	)
	SELECT
		BC.numComissionID
		,1
		,'Sales Order line item deleted'
		,ISNULL(numComissionAmount,0)  * -1
		,0 
	FROM 
		BizDocComission BC
	LEFT JOIN
		BizDocComissionPaymentDifference BCPD
	ON
		BC.numComissionID = BCPD.numComissionID
		AND BCPD.tintReason = 1
	LEFT JOIN
		OpportunityItems OI
	ON
		BC.numOppItemID = OI.numoppitemtCode
	WHERE 
		BC.numDomainId=@numDomainID 
		AND ISNULL(BC.numOppItemID,0) > 0
		AND ISNULL(BC.bitCommisionPaid,0)=1
		AND ISNULL(BC.bitNewImplementation,0) = 1
		AND OI.numoppitemtCode IS NULL
		AND BCPD.ID IS NULL 

	INSERT INTO BizDocComissionPaymentDifference
	(
		numComissionID
		,tintReason
		,vcReason
		,monDifference
		,bitDifferencePaid
	)
	SELECT
		BC.numComissionID
		,1
		,'Invoice deleted'
		,ISNULL(numComissionAmount,0)  * -1
		,0 
	FROM 
		BizDocComission BC
	LEFT JOIN
		BizDocComissionPaymentDifference BCPD
	ON
		BC.numComissionID = BCPD.numComissionID
		AND BCPD.tintReason = 1
	LEFT JOIN
		OpportunityBizDocs OBD
	ON
		BC.numOppBizDocId = OBD.numOppBizDocsId
	WHERE 
		BC.numDomainId=@numDomainID 
		AND ISNULL(BC.numOppBizDocId,0) > 0
		AND ISNULL(BC.bitCommisionPaid,0)=1
		AND ISNULL(BC.bitNewImplementation,0) = 1
		AND OBD.numOppBizDocsId IS NULL
		AND BCPD.ID IS NULL

	INSERT INTO BizDocComissionPaymentDifference
	(
		numComissionID
		,tintReason
		,vcReason
		,monDifference
		,bitDifferencePaid
	)
	SELECT
		BC.numComissionID
		,1
		,'Invoice line item deleted'
		,ISNULL(numComissionAmount,0)  * -1
		,0 
	FROM 
		BizDocComission BC
	LEFT JOIN
		BizDocComissionPaymentDifference BCPD
	ON
		BC.numComissionID = BCPD.numComissionID
		AND BCPD.tintReason = 1
	LEFT JOIN
		OpportunityBizDocItems OBDI
	ON
		BC.numOppBizDocItemID = OBDI.numOppBizDocItemID
	WHERE 
		BC.numDomainId=@numDomainID 
		AND ISNULL(BC.numOppBizDocItemID,0) > 0
		AND ISNULL(BC.bitCommisionPaid,0)=1
		AND ISNULL(BC.bitNewImplementation,0) = 1
		AND OBDI.numOppBizDocItemID IS NULL
		AND BCPD.ID IS NULL

	-- DELETE UNPAID COMMISSION ENTRIES BETWEEN PAY PERIOD DATE RANGE	
	IF @tintCommissionType = 3 --Sales Order Sub-Total Amount
	BEGIN
		INSERT INTO @TABLEPAID
		(
			numComissionID,
			numComRuleID,
			tintComType,
			tintComBasedOn,
			bitDomainCommissionBasedOn,
			tintDomainCommissionBasedOn,
			numUnitHour,
			monVendorCost,
			monAvgCost,
			monTotAmount,
			decCommission,
			monCommission
		)
		SELECT 
			BC.numComissionID,
			BC.numComRuleID,
			BC.tintComType,
			BC.tintComBasedOn,
			BC.bitDomainCommissionBasedOn,
			BC.tintDomainCommissionBasedOn,
			ISNULL(OI.numUnitHour,0),
			(ISNULL(OI.monVendorCost,0) * dbo.fn_UOMConversion(Item.numPurchaseUnit,Item.numItemCode,@numDomainID,Item.numBaseUnit) * ISNULL(OI.numUnitHour,0)),
			(ISNULL(OI.monAvgCost,0) * ISNULL(OI.numUnitHour,0)),
			ISNULL(OI.monTotAmount,0),
			ISNULL(decCommission,0),
			ISNULL(numComissionAmount,0)
		FROM 
			BizDocComission BC
		INNER JOIN
			OpportunityMaster OM
		ON
			BC.numOppID = OM.numOppId
		INNER JOIN
			DivisionMaster 
		ON
			OM.numDivisionID = DivisionMaster.numDivisionID
		INNER JOIN
			CompanyInfo
		ON
			DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
		INNER JOIN
			OpportunityItems OI 
		ON
			BC.numOppItemID = OI.numoppitemtCode
		INNER JOIN
			Item
		ON
			OI.numItemCode = Item.numItemCode
		WHERE
			BC.numDomainId=@numDomainID
			AND ISNULL(BC.bitCommisionPaid,0)=1
			AND ISNULL(BC.bitNewImplementation,0) = 1
	END
	ELSE IF @tintCommissionType = 2 --Invoice Sub-Total Amount (Paid or Unpaid)
	BEGIN
		INSERT INTO @TABLEPAID
		(
			numComissionID,
			numComRuleID,
			tintComType,
			tintComBasedOn,
			bitDomainCommissionBasedOn,
			tintDomainCommissionBasedOn,
			numUnitHour,
			monVendorCost,
			monAvgCost,
			monTotAmount,
			decCommission,
			monCommission
		)
		SELECT 
			BC.numComissionID,
			BC.numComRuleID,
			BC.tintComType,
			BC.tintComBasedOn,
			BC.bitDomainCommissionBasedOn,
			BC.tintDomainCommissionBasedOn,
			ISNULL(OppBizItems.numUnitHour,0),
			(ISNULL(OppMItems.monVendorCost,0) * dbo.fn_UOMConversion(Item.numPurchaseUnit,Item.numItemCode,@numDomainID,Item.numBaseUnit) * ISNULL(OppBizItems.numUnitHour,0)),
			(ISNULL(OppMItems.monAvgCost,0) * ISNULL(OppBizItems.numUnitHour,0)),
			ISNULL(OppBizItems.monTotAmount,0),
			ISNULL(decCommission,0),
			ISNULL(numComissionAmount,0)
		FROM 
			BizDocComission BC
		INNER JOIN
			OpportunityMaster OM
		ON
			BC.numOppID = OM.numOppId
		INNER JOIN	
			OpportunityBizDocs OBD
		ON
			OM.numOppId = OBD.numOppId
			AND BC.numOppBizDocId=OBD.numOppBizDocsId
		INNER JOIN
			DivisionMaster 
		ON
			OM.numDivisionID = DivisionMaster.numDivisionID
		INNER JOIN
			CompanyInfo
		ON
			DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
		INNER JOIN
			OpportunityBizDocItems OppBizItems
		ON
			BC.numOppBizDocItemID=OppBizItems.numOppBizDocItemID
		INNER JOIN
			OpportunityItems OppMItems 
		ON
			BC.numOppItemID=OppMItems.numoppitemtCode
		INNER JOIN
			Item
		ON
			OppBizItems.numItemCode = Item.numItemCode
		WHERE
			BC.numDomainId=@numDomainID
			AND ISNULL(BC.bitCommisionPaid,0) = 1
			AND ISNULL(BC.bitNewImplementation,0) = 1
	END
	ELSE
	BEGIN
		INSERT INTO @TABLEPAID
		(
			numComissionID,
			numComRuleID,
			tintComType,
			tintComBasedOn,
			bitDomainCommissionBasedOn,
			tintDomainCommissionBasedOn,
			numUnitHour,
			monVendorCost,
			monAvgCost,
			monTotAmount,
			decCommission,
			monCommission
		)
		SELECT 
			BC.numComissionID,
			BC.numComRuleID,
			BC.tintComType,
			BC.tintComBasedOn,
			BC.bitDomainCommissionBasedOn,
			BC.tintDomainCommissionBasedOn,
			ISNULL(OppBizItems.numUnitHour,0),
			(ISNULL(OppMItems.monVendorCost,0) * dbo.fn_UOMConversion(Item.numPurchaseUnit,Item.numItemCode,@numDomainID,Item.numBaseUnit) * ISNULL(OppBizItems.numUnitHour,0)),
			(ISNULL(OppMItems.monAvgCost,0) * ISNULL(OppBizItems.numUnitHour,0)),
			ISNULL(OppBizItems.monTotAmount,0),
			ISNULL(decCommission,0),
			ISNULL(numComissionAmount,0)
		FROM 
			BizDocComission BC
		INNER JOIN
			OpportunityMaster OppM
		ON
			BC.numOppID = OppM.numOppId
		INNER JOIN
			OpportunityBizDocs OppBiz
		ON
			OppM.numOppId = OppBiz.numOppId
			AND BC.numOppBizDocId=OppBiz.numOppBizDocsId
		INNER JOIN
			DivisionMaster 
		ON
			OppM.numDivisionID = DivisionMaster.numDivisionID
		INNER JOIN
			CompanyInfo
		ON
			DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
		INNER JOIN
			OpportunityBizDocItems OppBizItems
		ON
			BC.numOppBizDocItemID=OppBizItems.numOppBizDocItemID
		INNER JOIN
			OpportunityItems OppMItems 
		ON
			BC.numOppItemID=OppMItems.numoppitemtCode
		INNER JOIN
			Item
		ON
			OppBizItems.numItemCode = Item.numItemCode
		WHERE
			BC.numDomainId=@numDomainID
			AND ISNULL(BC.bitCommisionPaid,0) = 1
			AND ISNULL(BC.bitNewImplementation,0) = 1
	END	

	INSERT INTO BizDocComissionPaymentDifference
	(
		numComissionID
		,tintReason
		,vcReason
		,monDifference
		,bitDifferencePaid
	)
	SELECT
		numComissionID
		,tintReason
		,vcReason
		,monDifference
		,bitDifferencePaid
	FROM
	(SELECT
		T1.numComissionID
		,2 tintReason
		,'Change in qty/amount' vcReason
		,ISNULL(CASE tintComType 
					WHEN 1 --PERCENT
						THEN 
							CASE 
								WHEN ISNULL(bitDomainCommissionBasedOn,0) = 1
								THEN
									CASE tintDomainCommissionBasedOn
										--ITEM GROSS PROFIT (VENDOR COST)
										WHEN 1 THEN (ISNULL(monTotAmount,0) - ISNULL(monVendorCost,0)) * (T1.decCommission / CAST(100 AS FLOAT))
										--ITEM GROSS PROFIT (AVERAGE COST)
										WHEN 2 THEN(ISNULL(monTotAmount,0) - ISNULL(monAvgCost,0)) * (T1.decCommission / CAST(100 AS FLOAT))
									END
								ELSE
									monTotAmount * (T1.decCommission / CAST(100 AS FLOAT))
							END
					ELSE  --FLAT
						T1.decCommission
				END,0) - (ISNULL(monCommission,0) + ISNULL((SELECT SUM(monDifference) FROM BizDocComissionPaymentDifference WHERE numComissionID=T1.numComissionID),0)) monDifference
		,0 bitDifferencePaid
	FROM 
		@TABLEPAID AS T1
	WHERE
		ISNULL(CASE tintComType 
					WHEN 1 --PERCENT
						THEN 
							CASE 
								WHEN ISNULL(bitDomainCommissionBasedOn,0) = 1
								THEN
									CASE tintDomainCommissionBasedOn
										--ITEM GROSS PROFIT (VENDOR COST)
										WHEN 1 THEN (ISNULL(monTotAmount,0) - ISNULL(monVendorCost,0)) * (T1.decCommission / CAST(100 AS FLOAT))
										--ITEM GROSS PROFIT (AVERAGE COST)
										WHEN 2 THEN(ISNULL(monTotAmount,0) - ISNULL(monAvgCost,0)) * (T1.decCommission / CAST(100 AS FLOAT))
									END
								ELSE
									monTotAmount * (T1.decCommission / CAST(100 AS FLOAT))
							END
					ELSE  --FLAT
						T1.decCommission
				END,0) <> (ISNULL(monCommission,0) + ISNULL((SELECT SUM(monDifference) FROM BizDocComissionPaymentDifference WHERE numComissionID=T1.numComissionID),0))
	) TEMP
	WHERE
		CAST(TEMP.monDifference AS DECIMAL(20,4)) <> 0
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CalculateCommissionSalesReturn')
DROP PROCEDURE USP_CalculateCommissionSalesReturn
GO
CREATE PROCEDURE [dbo].[USP_CalculateCommissionSalesReturn]
	 @numComPayPeriodID AS NUMERIC(18,0),
	 @numDomainID AS NUMERIC(18,0),
	 @ClientTimeZoneOffset INT
AS
BEGIN
	DECLARE @TempCommissionSalesReturn TABLE
	(
		numComPayPeriodID NUMERIC(18,0)
		,numComRuleID NUMERIC(18,0)
		,numUserCntID NUMERIC(18,0)
		,tintAssignTo TINYINT
		,numReturnHeaderID NUMERIC(18,0)
		,numReturnItemID NUMERIC(18,0)
		,tintComType TINYINT
		,tintComBasedOn TINYINT
		,decCommission FLOAT
		,monCommission DECIMAL(20,5)
	)

	DECLARE @dtPayStart DATE
	DECLARE @dtPayEnd DATE

	SELECT 
		@dtPayStart=dtStart
		,@dtPayEnd=dtEnd 
	FROM 
		CommissionPayPeriod 
	WHERE 
		numComPayPeriodID = @numComPayPeriodID

	DELETE FROM 
		SalesReturnCommission 
	WHERE 
		numDomainId=@numDomainID 
		AND ISNULL(bitCommissionReversed,0)=0 
		AND numReturnItemID IN (SELECT 
									ReturnHeader.numReturnHeaderID
								FROM 
									ReturnHeader
								INNER JOIN
									ReturnItems
								ON
									ReturnHeader.numReturnHeaderID = ReturnItems.numReturnHeaderID
								CROSS APPLY
								(
									SELECT 
										MAX(datEntry_Date) dtDepositDate
									FROM 
										General_Journal_Header 
									WHERE 
										numDomainId=@numDomainId
										AND numReturnID=ReturnHeader.numReturnHeaderID
								) TempDepositMaster
								WHERE
									ReturnHeader.numDomainId = @numDomainID 
									AND ReturnHeader.numReturnStatus = 303
									AND CAST(DATEADD(MINUTE,-@ClientTimeZoneOffset,TempDepositMaster.dtDepositDate) AS DATE) BETWEEN @dtPayStart AND @dtPayEnd)

	INSERT INTO 
		@TempCommissionSalesReturn
	SELECT 
		@numComPayPeriodID
		,BizDocComission.numComRuleID
		,BizDocComission.numUserCntID
		,BizDocComission.tintAssignTo
		,ReturnHeader.numReturnHeaderID
		,ReturnItems.numReturnItemID
		,BizDocComission.tintComType
		,BizDocComission.tintComBasedOn
		,BizDocComission.decCommission
		,CASE tintComType 
			WHEN 1 --PERCENT
				THEN 
					CASE 
						WHEN ISNULL(bitDomainCommissionBasedOn,0) = 1
						THEN
							CASE tintDomainCommissionBasedOn
								--ITEM GROSS PROFIT (VENDOR COST)
								WHEN 1 THEN (ISNULL(ReturnItems.monTotAmount,0) - ISNULL(OpportunityItems.monVendorCost,0)) * (BizDocComission.decCommission / CAST(100 AS FLOAT))
								--ITEM GROSS PROFIT (AVERAGE COST)
								WHEN 2 THEN(ISNULL(ReturnItems.monTotAmount,0) - ISNULL(OpportunityItems.monAvgCost,0)) * (BizDocComission.decCommission / CAST(100 AS FLOAT))
							END
						ELSE
							ISNULL(ReturnItems.monTotAmount,0) * (BizDocComission.decCommission / CAST(100 AS FLOAT))
					END
			ELSE  --FLAT
				BizDocComission.decCommission
		END monCommissionReversed
	FROM
		ReturnHeader
	INNER JOIN
		ReturnItems
	ON
		ReturnHeader.numReturnHeaderID = ReturnItems.numReturnHeaderID
	INNER JOIN
		BizDocComission
	ON
		ReturnItems.numOppItemID = BizDocComission.numOppItemID
	INNER JOIN
		OpportunityItems 
	ON
		ReturnItems.numOppItemID = OpportunityItems.numoppitemtCode
	CROSS APPLY
	(
		SELECT 
			MAX(datEntry_Date) dtDepositDate
		FROM 
			General_Journal_Header 
		WHERE 
			numDomainId=@numDomainId
			AND numReturnID=ReturnHeader.numReturnHeaderID
	) TempDepositMaster
	WHERE
		ReturnHeader.numDomainId = @numDomainID 
		AND ReturnHeader.numReturnStatus = 303
		AND CAST(DATEADD(MINUTE,-@ClientTimeZoneOffset,TempDepositMaster.dtDepositDate) AS DATE) BETWEEN @dtPayStart AND @dtPayEnd
	
	-- UPDATE FLAT COMMMISSION AMOUNT 
	UPDATE
		T1
	SET
		T1.monCommission= decCommission/(SELECT COUNT(*) FROM @TempCommissionSalesReturn Tinner WHERE Tinner.numUserCntID=T1.numUserCntID AND Tinner.tintComType=2)
	FROM
		@TempCommissionSalesReturn T1
	WHERE
		tintComType=2

	INSERT INTO SalesReturnCommission
	(
		numDomainID
		,numComPayPeriodID
		,numComRuleID
		,numUserCntID
		,tintAssignTo
		,numReturnHeaderID
		,numReturnItemID
		,tintComType
		,tintComBasedOn
		,decCommission
		,monCommissionReversed
		,bitCommissionReversed
	)
	SELECT
		@numDomainID
		,numComPayPeriodID
		,numComRuleID
		,numUserCntID
		,tintAssignTo
		,numReturnHeaderID
		,numReturnItemID
		,tintComType
		,tintComBasedOn
		,decCommission
		,monCommission
		,0
	FROM
		@TempCommissionSalesReturn T1
	WHERE
		(SELECT 
			COUNT(*) 
		FROM 
			SalesReturnCommission SRC
		WHERE 
			SRC.numUserCntID=T1.numUserCntID 
			AND SRC.numReturnHeaderID=T1.numReturnHeaderID 
			AND SRC.numReturnItemID = T1.numReturnItemID 
			AND SRC.tintAssignTo = T1.tintAssignTo
		) = 0

END
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetEmailMergeData' ) 
    DROP PROCEDURE USP_GetEmailMergeData 
GO
CREATE PROCEDURE USP_GetEmailMergeData
    @numModuleID NUMERIC,
    @vcRecordIDs VARCHAR(8000) = '', -- Seperated by comma
    @numDomainID NUMERIC,
    @tintMode TINYINT = 0,
    @ClientTimeZoneOffset Int,
	@numOppBizDocId NUMERIC = 0
AS 
BEGIN
	
    IF (@numModuleID = 1  or @numModuleID = 0)
        BEGIN
            SELECT  ACI.numContactId,
                    [dbo].[GetListIemName](ACI.vcDepartment) ContactDepartment,
                    [dbo].[GetListIemName](ACI.vcCategory) ContactCategory,
                    
                    ACI.vcGivenName,
                    ACI.vcFirstName ContactFirstName,
                    ACI.vcLastName ContactLastName,
                    ACI.numDivisionId,
                    dbo.GetListIemName(ACI.numContactType) ContactType,
                    [dbo].[GetListIemName](ACI.numTeam) ContactTeam,
                    ACI.numPhone ContactPhone,
                    ACI.numPhoneExtension ContactPhoneExt,
                    ACI.numCell ContactCell,
                    ACI.numHomePhone ContactHomePhone,
                    ACI.vcFax ContactFax,
                    ACI.vcEmail ContactEmail,
                    ACI.VcAsstFirstName AssistantFirstName,
                    ACI.vcAsstLastName AssistantLastName,
                    ACI.numAsstPhone ContactAssistantPhone,
                    ACI.numAsstExtn ContactAssistantPhoneExt,
                    ACI.vcAsstEmail ContactAssistantEmail,
                    CASE WHEN ACI.charSex = 'M' THEN 'Male'
                         WHEN ACI.charSex = 'F' THEN 'Female'
                         ELSE '-'
                    END AS ContactGender,
                    ACI.bintDOB,
                    dbo.GetAge(ACI.bintDOB, GETUTCDATE()) AS ContactAge,
                    [dbo].[GetListIemName](ACI.vcPosition) ContactPosition,
                    ACI.txtNotes,
                    ACI.numCreatedBy,
                    ACI.bintCreatedDate,
                    ACI.numModifiedBy,
                    ACI.bintModifiedDate,
                    ACI.numDomainID,
                    ACI.bitOptOut,
                    dbo.fn_GetContactName(ACI.numManagerId) ContactManager,
                    ACI.numRecOwner,
                    [dbo].[GetListIemName](ACI.numEmpStatus) ContactStatus,
                    ACI.vcTitle ContactTitle,
                    ACI.vcAltEmail,
                    ACI.vcItemId,
                    ACI.vcChangeKey,
                    ISNULL(( SELECT TOP 1 vcECampName
                             FROM   [ECampaign]
                             WHERE  numECampaignID = ACI.numECampaignID
                           ), '') ContactDripCampaign,
                    dbo.getContactAddress(ACI.numContactID) AS ContactPrimaryAddress,
                    C.numCompanyId,
                    C.vcCompanyName OrganizationName,
                    C.numCompanyType,
                    C.numCompanyRating,
                    C.numCompanyIndustry,
                    C.numCompanyCredit,
                    C.txtComments,
                    C.vcWebSite,
                    C.vcWebLabel1,
                    C.vcWebLink1,
                    C.vcWebLabel2,
                    C.vcWebLink2,
                    C.vcWebLabel3,
                    C.vcWebLink3,
                    C.vcWeblabel4,
                    C.vcWebLink4,
                    C.numAnnualRevID,
                    dbo.GetListIemName(C.numNoOfEmployeesId) OrgNoOfEmployee,
                    C.vcHow,
                    C.numCreatedBy,
                    C.bintCreatedDate,
                    C.numModifiedBy,
                    C.bintModifiedDate,
                    C.bitPublicFlag,
                    C.numDomainID,
                    
                    DM.numDivisionID,
                    DM.numCompanyID,
                    DM.vcDivisionName,
                    DM.numGrpId,
                    DM.numFollowUpStatus,
                    DM.bitPublicFlag,
                    DM.numCreatedBy,
                    DM.bintCreatedDate,
                    DM.numModifiedBy,
                    DM.bintModifiedDate,
                    DM.tintCRMType,
                    DM.numDomainID,
                    DM.bitLeadBoxFlg,
                    DM.numTerID,
                    DM.numStatusID,
                    DM.bintLeadProm,
                    DM.bintLeadPromBy,
                    DM.bintProsProm,
                    DM.bintProsPromBy,
                    DM.numRecOwner,
                    DM.decTaxPercentage,
                    DM.tintBillingTerms,
                    DM.numBillingDays,
                    DM.tintInterestType,
                    DM.fltInterest,
                    DM.vcComPhone,
                    DM.vcComFax,
                    DM.numCampaignID,
                    DM.numAssignedBy,
                    DM.numAssignedTo,
                    DM.bitNoTax,
                    --vcCompanyName + ' ,<br>' + ISNULL(AD2.vcStreet,'') + ' <br>'
                    --+ ISNULL(AD2.VcCity,'') + ' ,' + ISNULL(dbo.fn_GetState(AD2.numState),
                    --                             '') + ' ' + ISNULL(AD2.vcPostalCode,'')
                    --+ ' <br>' + ISNULL(dbo.fn_GetListItemName(AD2.numCountry),
                    --                   '') AS OrgShippingAddress ,	
									   
					(SELECT TOP 1 vcCreditCardNo FROM CustomerCreditCardInfo CC
						LEFT JOIN ListDetails ON ListDetails.numListItemID=CC.numCardTypeID
					WHERE numContactId=ACI.numContactId AND bitIsDefault = 1 
					ORDER BY CC.bitIsDefault DESC) AS PrimaryCreditCardNo,												   		
                  
				   (select TOP 1 U.txtSignature from UserMaster U where U.numUserDetailId=ACI.numRecOwner) as [Signature],
				   (CASE WHEN @numDomainID=1 THEN ISNULL((SELECT TOP 1 LD.vcData FROM CFW_FLD_Values CFV INNER JOIN ListDetails LD ON CFV.Fld_Value=LD.numListItemID WHERE CFV.RecId=DM.numDivisionID AND Fld_ID=12882),'') ELSE '' END) Cust12882
            FROM    dbo.CompanyInfo C
                    INNER JOIN dbo.DivisionMaster DM ON DM.numCompanyId = C.numCompanyId
                    INNER JOIN dbo.AdditionalContactsInformation ACI ON ACI.numDivisionId = DM.numDivisionID
                    LEFT JOIN dbo.AddressDetails AD2 ON AD2.numDomainID=DM.numDomainID 
					 AND AD2.numRecordID= DM.numDivisionID AND AD2.tintAddressOf=2 AND AD2.tintAddressType=2 AND AD2.bitIsPrimary=1
					 ---- Added by Priya(9 Feb 2018)----
					LEFT JOIN dbo.AddressDetails AD1 ON AD1.numDomainID=DM.numDomainID 
					AND AD1.numRecordID= DM.numDivisionID AND AD1.tintAddressOf=2 AND AD1.tintAddressType=1 AND AD1.bitIsPrimary=1
					LEFT JOIN dbo.BillingTerms BT ON  DM.numBillingDays = BT.numTermsID
								AND ISNULL(BT.bitActive,0) = 1
				
            WHERE   DM.numDomainId = @numDomainID
                    AND numContactId IN (
                    SELECT  Id
                    FROM    dbo.SplitIDs(@vcRecordIDs, ',') )
        END
    IF @numModuleID = 2 
        BEGIN
             SELECT 
                    dbo.fn_getContactName(OM.numAssignedTo) OpportunityAssigneeName,
					(ISNULL(C.varCurrSymbol,'') + ' ' + (convert(varchar(30), ([dbo].[getdealamount](OM.numOppId,Getutcdate(),0)), 1) )) AS OppOrderSubTotal,
					vcPOppName OpportunityName,
					dbo.Fn_getcontactname(OM.numContactId) OppOrderContact,					
					ISNULL(OM.vcOppRefOrderNo,'') AS OppOrderCustomerPO,
					(ISNULL(C.varCurrSymbol,'') + ' ' + (convert(varchar(30),ISNULL((SELECT SUM(ISNULL(monDealAmount,0)) FROM OpportunityBizDocs BD WHERE  BD.numOppId = OM.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0), 1) )) AS OppOrderInvoiceGrandtotal,
					(ISNULL(C.varCurrSymbol,'') + ' ' + (convert(varchar(30), ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM OpportunityBizDocs BD WHERE  BD.numOppId = OM.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0), 1) )) AS OppOrderTotalAmountPaid,
					ISNULL((SELECT TOP 1 vcAddressName FROM fn_getOPPAddressDetails(OM.numOppId,OM.numDomainID,1)),0) AS OppOrderBillingAddressName,
					ISNULL((SELECT TOP 1 vcStreet FROM fn_getOPPAddressDetails(OM.numOppId,OM.numDomainID,1)),0) AS OppOrderBillingStreet,
					ISNULL((SELECT TOP 1 vcCity FROM fn_getOPPAddressDetails(OM.numOppId,OM.numDomainID,1)),0) AS OppOrderBillingCity,
					ISNULL((SELECT TOP 1 vcState FROM fn_getOPPAddressDetails(OM.numOppId,OM.numDomainID,1)),0) AS OppOrderBillingState,
					ISNULL((SELECT TOP 1 vcPostalCode FROM fn_getOPPAddressDetails(OM.numOppId,OM.numDomainID,1)),0) AS OppOrderBillingPostal,
					ISNULL((SELECT TOP 1 vcCountry FROM fn_getOPPAddressDetails(OM.numOppId,OM.numDomainID,1)),0) AS OppOrderBillingCountry,
					ISNULL((SELECT TOP 1 vcAddressName FROM fn_getOPPAddressDetails(OM.numOppId,OM.numDomainID,2)),'') AS OppOrderShippingAddressName,
					ISNULL((SELECT TOP 1 vcStreet FROM fn_getOPPAddressDetails(OM.numOppId,OM.numDomainID,2)),'') AS OppOrderShippingStreet,
					ISNULL((SELECT TOP 1 vcCity FROM fn_getOPPAddressDetails(OM.numOppId,OM.numDomainID,2)),0) AS OppOrderShippingCity,
					ISNULL((SELECT TOP 1 vcState FROM fn_getOPPAddressDetails(OM.numOppId,OM.numDomainID,2)),0) AS OppOrderShippingState,
					ISNULL((SELECT TOP 1 vcPostalCode FROM fn_getOPPAddressDetails(OM.numOppId,OM.numDomainID,2)),0) AS OppOrderShippingPostal,
					ISNULL((SELECT TOP 1 vcCountry FROM fn_getOPPAddressDetails(OM.numOppId,OM.numDomainID,2)),0) AS OppOrderShippingCountry,
					(CASE WHEN ISNULL(OM.intUsedShippingCompany,0) = 0 THEN '' 
					ELSE ISNULL((SELECT vcData FROM ListDetails WHERE numListID = 82 AND numListItemID=ISNULL(OM.intUsedShippingCompany,0)),'') END) AS OppOrderShipVia,
					ISNULL((SELECT vcShipmentService FROM ShippingService WHERE (numDomainID=@numDomainID OR ISNULL(numDomainID,0)=0) AND numShippingServiceID = OM.numShippingService),'') AS OppOrderShippingService,
					ISNULL(OM.txtComments, '') AS OppOrderComments,
					ISNULL(OM.dtReleaseDate,'') AS OppOrderReleaseDate,
					ISNULL(OM.intpEstimatedCloseDate,'') AS OppOrderEstimatedCloseDate,
					dbo.fn_GetOpportunitySourceValue(ISNULL(OM.tintSource,0),ISNULL(OM.tintSourceType,0),OM.numDomainID) AS OppOrderSource,
					--ISNULL((SELECT SUBSTRING((SELECT '$^$' + ISNULL( (SELECT vcShipFieldValue FROM dbo.ShippingFieldValues WHERE numDomainID=OM.numDomainID AND intShipFieldID IN ( SELECT intShipFieldID FROM dbo.ShippingFields WHERE numListItemID=OBD.numShipVia  AND vcFieldName='Tracking URL')),'') +'#^#'+ RTRIM(LTRIM(vcTrackingNo) )  FROM dbo.OpportunityBizDocs OBD WHERE OBD.numOppId=OM.numOppID FOR XML PATH('')),4,200000)),'') AS OppOrderTrackingNo,				

					 (SELECT  
						SUBSTRING(
									(SELECT '$^$' + 
									(SELECT vcShipFieldValue FROM dbo.ShippingFieldValues 
										WHERE numDomainID=OM.numDomainID AND intShipFieldID IN ( SELECT intShipFieldID FROM dbo.ShippingFields WHERE numListItemID=OBD.numShipVia  AND vcFieldName='Tracking URL')
									) +'#^#'+ RTRIM(LTRIM(vcTrackingNo))						
									FROM dbo.OpportunityBizDocs OBD WHERE OBD.numOppId= OM.numOppID and ISNULL(OBD.numShipVia,0) <> 0
						FOR XML PATH('')),4,200000)
						
					 ) AS  OppOrderTrackingNo,					
					
					(SELECT U.txtSignature FROM UserMaster U where U.numUserDetailId=OM.numContactId) as [Signature],

					--(SELECT TOP 1 vcCreditCardNo FROM CustomerCreditCardInfo CC
					--	INNER JOIN [AdditionalContactsInformation] ADC ON CC.[numContactId] = ADC.[numContactId]
					--	LEFT JOIN ListDetails ON ListDetails.numListItemID=CC.numCardTypeID
					--	WHERE CC.numContactId=OM.numContactId AND bitIsDefault = 1 
					--	ORDER BY CC.bitIsDefault DESC
					--) AS PrimaryCreditCardNo

					(CASE WHEN @numOppBizDocId > 0 AND EXISTS (SELECT numTransHistoryID FROM TransactionHistory TH WHERE TH.numOppBizDocsID = @numOppBizDocId AND TH.numOppID = OM.numOppId) THEN (SELECT vcCreditCardNo FROM TransactionHistory TH WHERE TH.numOppBizDocsID = @numOppBizDocId AND TH.numOppID = OM.numOppId)
							ELSE ISNULL((SELECT TOP 1 vcCreditCardNo FROM TransactionHistory TH WHERE TH.numOppID = OM.numOppId),'')
						END
					) AS PrimaryCreditCardNo,
					
					ISNULL(CMP.vcCompanyName , '') AS OppOrderAccountName

            FROM    dbo.OpportunityMaster OM
					LEFT OUTER JOIN [Currency] C
					ON C.numCurrencyID = OM.numCurrencyID
					LEFT JOIN DivisionMaster DM
					ON  OM.numDivisionId = DM.numDivisionID
					LEFT JOIN CompanyInfo CMP
					ON DM.numCompanyID = CMP.numCompanyId					
            WHERE   OM.numDomainID = @numDomainID
                   AND OM.numOppId IN (
                    SELECT  Id
                    FROM    dbo.SplitIDs(@vcRecordIDs, ',') )

        END
    IF @numModuleID = 4 
        BEGIN        
            SELECT  vcProjectID ProjectID,
                    vcProjectName ProjectName,
                    dbo.fn_getContactName(numAssignedTo) ProjectAssigneeName,
                    dbo.FormatedDateFromDate(intDueDate, numDomainId) ProjectDueDate
            FROM    dbo.ProjectsMaster
            WHERE   numDomainID = @numDomainID
                    AND numProID IN (
                    SELECT  Id
                    FROM    dbo.SplitIDs(@vcRecordIDs, ',') )
        END 
    IF @numModuleID = 5 
        BEGIN        
            SELECT  vcCaseNumber CaseNo,
                    textSubject AS CaseSubject,
                    dbo.fn_getContactName(numAssignedTo) CaseAssigneeName,
                    dbo.FormatedDateFromDate(intTargetResolveDate, numDomainID) CaseResoveDate
            FROM    dbo.Cases
            WHERE   numDomainID = @numDomainID
                    AND numCaseId IN (
                    SELECT  Id
                    FROM    dbo.SplitIDs(@vcRecordIDs, ',') )
        END 
    IF @numModuleID = 6 
        BEGIN        
            SELECT  textDetails TicklerComment,
                    dbo.GetListIemName(bitTask) TicklerType,
                    dbo.fn_getContactName(numAssign) TicklerAssigneeName,
                    dbo.FormatedDateFromDate(dtStartTime, C.numDomainID) TicklerDueDate,
                    dbo.GetListIemName(numStatus) TicklerPriority,
                    dbo.FormatedDateTimeFromDate(DateAdd(minute, -@ClientTimeZoneOffset,dtStartTime ), C.numDomainID) TicklerStartTime,
                    dbo.FormatedDateTimeFromDate(DateAdd(minute, -@ClientTimeZoneOffset,dtEndTime ), C.numDomainID) TicklerEndTime,
                    dbo.GetListIemName(bitTask) + ' - ' + dbo.GetListIemName(numStatus) AS TicklerTitle,
                    dbo.fn_getContactName(C.numCreatedBy) TicklerCreatedBy,
					CMP.vcCompanyName OrganizationName
                   
                    
            FROM    dbo.Communication C
			LEFT JOIN DivisionMaster DM
			ON C.numDivisionId = DM.numDivisionID
			LEFT JOIN CompanyInfo CMP
			ON DM.numCompanyId = CMP.numCompanyId
            WHERE   C.numDomainID = @numDomainID
                    AND numCommId IN (
                    SELECT  Id
                    FROM    dbo.SplitIDs(@vcRecordIDs, ',') )
        END 
    IF @numModuleID = 8 
        BEGIN
            SELECT  ACI.vcFirstName ContactFirstName,
                    ACI.vcLastName ContactLastName,
					C.vcCompanyName OrganizationName,
					dbo.GetListIemName(OB.numBizDocId) BizDoc,
                   OB.monDealAmount BizDocAmount,
                    OB.monAmountPaid BizDocAmountPaid,
                    ( OB.monDealAmount - OB.monAmountPaid ) BizDocBalanceDue,
                    dbo.GetListIemName(ISNULL(OB.numBizDocStatus, 0)) AS BizDocStatus,
                    C.vcCompanyName OrderOrganizationName,
                    ACI.vcFirstName OrderContactFirstName,
                    ACI.vcLastName OrderContactLastName,
                    ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(om.intBillingDays,0)), 0)  AS BizDocBillingTermsNetDays,
                    dbo.FormatedDateFromDate(OB.dtFromDate, OM.numDomainID) AS BizDocBillingTermsFromDate,
                    CASE ISNULL(OM.bitInterestType, 0)
                      WHEN 1 THEN '+   OB.vcBizDocID BizDocID,
                  '
                      ELSE '-'
                    END + CAST(ISNULL(OM.fltInterest, 0) AS VARCHAR(10))
                    + ' %' AS BizDocBillingTermsInterest,
                    OM.vcPOppName OrderName,
                    CASE WHEN   isnull(OM.bitBillingTerms ,0) <> 0		
                          THEN 'Net ' + 
                                CASE WHEN 	                                   
                                      ISNUMERIC(ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))=1 	
                                THEN 	
                                     CONVERT(VARCHAR(20) ,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0)) 
                                     ELSE
                                     CONVERT(VARCHAR(20) ,0) 
                                     END + ',' +
                                     CASE WHEN  tintInterestType = 0 THEN '-' ELSE '+' + CONVERT(VARCHAR(20) , OM.fltInterest)   + ' %' END ELSE '-' 
                           END         
                                     AS BizDocBillingTerms,

						  DATEADD(DAY,+ CASE WHEN ISNUMERIC(ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))=1 
											 THEN ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0)
											 ELSE 0 
										END,OB.dtCreatedDate) AS BizDocDueDate,										
					ACI.vcFirstName ContactFirstName,
                    ACI.vcLastName ContactLastName,
					ACI.vcEmail ContactEmail,OB.numOppBizDocsID,
					isnull(OB.vcTrackingNo,'') AS  vcTrackingNo,
					ISNULL( (SELECT vcShipFieldValue FROM dbo.ShippingFieldValues WHERE numDomainID=OM.numDomainID AND intShipFieldID IN ( SELECT intShipFieldID FROM dbo.ShippingFields WHERE numListItemID=OB.numShipVia  AND vcFieldName='Tracking URL')),'') AS vcTrackingURL,
					'' As BizDocTemplateFromGlobalSettings

					--OM.vcPOppName OpportunityName,
     --               dbo.fn_getContactName(OM.numAssignedTo) OpportunityAssigneeName
            FROM    dbo.OpportunityBizDocs OB
                    INNER JOIN dbo.OpportunityMaster OM ON OM.numOppID = OB.numOppID
                    INNER JOIN dbo.DivisionMaster DM ON DM.numDivisionID = OM.numDivisionID
                    INNER JOIN dbo.AdditionalContactsInformation ACI ON ACI.numContactID = OM.numContactID
                    INNER JOIN dbo.CompanyInfo C ON C.numCompanyID = DM.numCompanyID
            WHERE   OM.numDomainID = @numDomainID
                    AND OB.numOppBizDocsID IN (
                    SELECT  Id
                    FROM    dbo.SplitIDs(@vcRecordIDs, ',') )
			
        END 	
        IF @numModuleID = 9 
        BEGIN
            SELECT  
                   ISNULL(TempBizDoc.vcBizDocID,'') BizDocID,
                   ISNULL(TempBizDoc.monDealAmount,0) BizDocAmount,
                   ISNULL(TempBizDoc.monAmountPaid,0) BizDocAmountPaid,
                   ISNULL(TempBizDoc.BizDocBalanceDue,0) BizDocBalanceDue,
				   ISNULL(TempBizDoc.BizDocStatus,'') BizDocStatus,
                    C.vcCompanyName OrderOrganizationName,
                    ACI.vcFirstName OrderContactFirstName,
                    ACI.vcLastName OrderContactLastName,
                    --ISNULL(dbo.fn_GetListItemName(ISNULL(om.intBillingDays, 0)),0) AS BizDocBillingTermsNetDays,
                    ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(om.intBillingDays,0)), 0) AS BizDocBillingTermsNetDays,
                   -- dbo.FormatedDateFromDate(OB.dtFromDate, OM.numDomainID) AS BizDocBillingTermsFromDate,
                    CASE ISNULL(OM.bitInterestType, 0)
                      WHEN 1 THEN '+'
                      ELSE '-'
                    END + CAST(ISNULL(OM.fltInterest, 0) AS VARCHAR(10))
                    + ' %' AS BizDocBillingTermsInterest,
                    OM.vcPOppName OrderName,
                    CASE WHEN   isnull(OM.bitBillingTerms ,0) <> 0		
                          THEN 'Net ' + 
                                CASE WHEN 		
                                      --ISNUMERIC(ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))=1
                                      ISNUMERIC(ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))=1  		
                                THEN 	
                                     --CONVERT(VARCHAR(20) ,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0)) 
                                     CONVERT(VARCHAR(20) ,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0)) 
                                     ELSE
                                     CONVERT(VARCHAR(20) ,0) 
                                     END + ',' +
                                     CASE WHEN  tintInterestType = 0 THEN '-' ELSE '+' + CONVERT(VARCHAR(20) , OM.fltInterest)   + ' %' END ELSE '-' 
                           END         
                                     AS BizDocBillingTerms       		
                                    ,
                        --  DATEADD(DAY,+ CASE WHEN ISNUMERIC(ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))=1 THEN ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0) ELSE 0 END,OB.dtCreatedDate) AS BizDocDueDate,
					ACI.vcFirstName ContactFirstName,
                    ACI.vcLastName ContactLastName,
					ACI.vcEmail ContactEmail
            FROM    dbo.OpportunityMaster OM 
                    INNER JOIN dbo.DivisionMaster DM ON DM.numDivisionID = OM.numDivisionID
                    INNER JOIN dbo.AdditionalContactsInformation ACI ON ACI.numContactID = OM.numContactID
                    INNER JOIN dbo.CompanyInfo C ON C.numCompanyID = DM.numCompanyID
					OUTER APPLY
					(
						SELECT TOP 1
							vcBizDocID,
							monDealAmount,
							monAmountPaid,
							(monDealAmount - monAmountPaid) AS BizDocBalanceDue,
							dbo.GetListIemName(ISNULL(numBizDocStatus, 0)) AS BizDocStatus
						FROM
							OpportunityBizDocs 
						WHERE 
							numOppId=OM.numOppID ANd ISNULL(bitAuthoritativeBizDocs,0) = 1
					) AS TempBizDoc
            WHERE   OM.numDomainID = @numDomainID
                    AND OM.numOppID IN (
                    SELECT  Id
                    FROM    dbo.SplitIDs(@vcRecordIDs, ',') )
			
        END 
        
        IF @numModuleID = 11 
        BEGIN
            SELECT  ACI.numContactId,
                    [dbo].[GetListIemName](ACI.vcDepartment) ContactDepartment,
                    [dbo].[GetListIemName](ACI.vcCategory) ContactCategory,
                    
                    ACI.vcGivenName,
                    ACI.vcFirstName ContactFirstName,
                    ACI.vcLastName ContactLastName,
                    ACI.numDivisionId,
                    dbo.GetListIemName(ACI.numContactType) ContactType,
                    [dbo].[GetListIemName](ACI.numTeam) ContactTeam,
                    ACI.numPhone ContactPhone,
                    ACI.numPhoneExtension ContactPhoneExt,
                    ACI.numCell ContactCell,
                    ACI.numHomePhone ContactHomePhone,
                    ACI.vcFax ContactFax,
                    ACI.vcEmail ContactEmail,
                    ACI.VcAsstFirstName AssistantFirstName,
                    ACI.vcAsstLastName AssistantLastName,
                    ACI.numAsstPhone ContactAssistantPhone,
                    ACI.numAsstExtn ContactAssistantPhoneExt,
                    ACI.vcAsstEmail ContactAssistantEmail,
                    CASE WHEN ACI.charSex = 'M' THEN 'Male'
                         WHEN ACI.charSex = 'F' THEN 'Female'
                         ELSE '-'
                    END AS ContactGender,
                    ACI.bintDOB,
                    dbo.GetAge(ACI.bintDOB, GETUTCDATE()) AS ContactAge,
                    [dbo].[GetListIemName](ACI.vcPosition) ContactPosition,
                    ACI.txtNotes,
                    ACI.numCreatedBy,
                    ACI.bintCreatedDate,
                    ACI.numModifiedBy,
                    ACI.bintModifiedDate,
                    ACI.numDomainID,
                    ACI.bitOptOut,
                    dbo.fn_GetContactName(ACI.numManagerId) ContactManager,
                    ACI.numRecOwner,
                    [dbo].[GetListIemName](ACI.numEmpStatus) ContactStatus,
                    ACI.vcTitle ContactTitle,
                    ACI.vcAltEmail,
                    ACI.vcItemId,
                    ACI.vcChangeKey,
                    ISNULL(( SELECT vcECampName
                             FROM   [ECampaign]
                             WHERE  numECampaignID = ACI.numECampaignID
                           ), '') ContactDripCampaign,
                    dbo.getContactAddress(ACI.numContactID) AS ContactPrimaryAddress,
                    C.numCompanyId,
                    C.vcCompanyName OrganizationName,
                    C.numCompanyType,
                    C.numCompanyRating,
                    C.numCompanyIndustry,
                    C.numCompanyCredit,
                    C.txtComments,
                    C.vcWebSite,
                    C.vcWebLabel1,
                    C.vcWebLink1,
                    C.vcWebLabel2,
                    C.vcWebLink2,
                    C.vcWebLabel3,
                    C.vcWebLink3,
                    C.vcWeblabel4,
                    C.vcWebLink4,
                    C.numAnnualRevID,
                    dbo.GetListIemName(C.numNoOfEmployeesId) OrgNoOfEmployee,
                    C.vcHow,
                    C.vcProfile,
                    C.numCreatedBy,
                    C.bintCreatedDate,
                    C.numModifiedBy,
                    C.bintModifiedDate,
                    C.bitPublicFlag,
                    C.numDomainID,
                    
                    DM.numDivisionID,
                    DM.numCompanyID,
                    DM.vcDivisionName,
                    DM.numGrpId,
                    DM.numFollowUpStatus,
                    DM.bitPublicFlag,
                    DM.numCreatedBy,
                    DM.bintCreatedDate,
                    DM.numModifiedBy,
                    DM.bintModifiedDate,
                    DM.tintCRMType,
                    DM.numDomainID,
                    DM.bitLeadBoxFlg,
                    DM.numTerID,
                    DM.numStatusID,
                    DM.bintLeadProm,
                    DM.bintLeadPromBy,
                    DM.bintProsProm,
                    DM.bintProsPromBy,
                    DM.numRecOwner,
                    DM.decTaxPercentage,
                    DM.tintBillingTerms,
                    DM.numBillingDays,
                    DM.tintInterestType,
                    DM.fltInterest,
                    DM.vcComPhone,
                    DM.vcComFax,
                    DM.numCampaignID,
                    DM.numAssignedBy,
                    DM.numAssignedTo,
                    DM.bitNoTax,
                    vcCompanyName + ' ,<br>' + ISNULL(AD2.vcStreet,'') + ' <br>'
                    + ISNULL(AD2.VcCity,'') + ' ,' + ISNULL(dbo.fn_GetState(AD2.numState),
                                                 '') + ' ' + ISNULL(AD2.vcPostalCode,'')
                    + ' <br>' + ISNULL(dbo.fn_GetListItemName(AD2.numCountry),
                                       '') AS OrgShippingAddress 
            FROM    dbo.CompanyInfo C
                    INNER JOIN dbo.DivisionMaster DM ON DM.numCompanyId = C.numCompanyId
                    INNER JOIN dbo.AdditionalContactsInformation ACI ON ACI.numDivisionId = DM.numDivisionID
                    LEFT JOIN dbo.AddressDetails AD2 ON AD2.numDomainID=DM.numDomainID 
					 AND AD2.numRecordID= DM.numDivisionID AND AD2.tintAddressOf=1 AND AD2.tintAddressType=2 AND AD2.bitIsPrimary=1
            WHERE   DM.numDomainId = @numDomainID
                    AND numContactId IN (
                    SELECT  Id
                    FROM    dbo.SplitIDs(@vcRecordIDs, ',') )
        END

		 IF @numModuleID = 45
        BEGIN        
            SELECT  C.vcCompanyName OrganizationName,
					(select vcdata from listdetails where numListItemID = C.vcProfile) AS OrgProfile, 		   
					(select A.vcFirstName+' '+A.vcLastName                       
					 from AdditionalContactsInformation A                  
					 join DivisionMaster D                        
					 on D.numDivisionID=A.numDivisionID
					 where A.numContactID=DM.numAssignedTo) AS OrgAssignto,	
					 ISNULL(AD1.vcAddressName,'') AS OrgBillingName,
					 ISNULL(AD1.vcStreet,'') AS OrgBillingStreet,
					 ISNULL(AD1.VcCity,'') AS OrgBillingCity,
					 ISNULL(dbo.fn_GetState(AD1.numState),'') AS OrgBillingState,
					 ISNULL(AD1.vcPostalCode,'') AS OrgBillingPostal,
					 ISNULL(dbo.fn_GetListItemName(AD1.numCountry),'') AS OrgBillingCountry,
					 ISNULL(AD2.vcAddressName,'') AS OrgShippingName,
					 ISNULL(AD2.vcStreet,'') AS OrgShippingStreet,
					 ISNULL(AD2.VcCity,'') AS OrgShippingCity,
					 ISNULL(dbo.fn_GetState(AD2.numState),'') AS OrgShippingState,
					 ISNULL(AD2.vcPostalCode,'') AS OrgShippingPostal,
					 ISNULL(dbo.fn_GetListItemName(AD2.numCountry),'') AS OrgShippingCountry,
					 (select vcdata from listdetails where numListItemID = C.numCompanyCredit) AS  OrgCreditLimit,
					 (select (CAST(BT.vcTerms AS VARCHAR(100) ) + ' (' + CAST(BT.numNetDueInDays AS VARCHAR(10)) + ',' + CAST(BT.numDiscount AS VARCHAR(10)) + ',' +  CAST(BT.numDiscountPaidInDays AS VARCHAR(10)) + ')' )) AS OrgNetTerms,
					 (select vcdata from listdetails where numListItemID = DM.numDefaultPaymentMethod) AS  OrgPaymentMethod,
					 (select vcdata from listdetails where numListItemID = DM.intShippingCompany) AS  OrgPreferredShipVia,
					 ISNULL((SELECT vcShipmentService FROM ShippingService WHERE (numDomainID=@numDomainID OR ISNULL(numDomainID,0)=0) AND numShippingServiceID = DM.numDefaultShippingServiceID),'') AS OrgPreferredParcelShippingService,
					(SELECT TOP 1 vcCreditCardNo FROM CustomerCreditCardInfo CC
						LEFT JOIN ListDetails ON ListDetails.numListItemID=CC.numCardTypeID
						WHERE numContactId=ACI.numContactId AND bitIsDefault = 1
						ORDER BY CC.bitIsDefault DESC
					) AS PrimaryCreditCardNo,	
			
					ACI.vcFirstName ContactFirstName,
                    ACI.vcLastName ContactLastName,
					ACI.vcEmail ContactEmail,
					ACI.numPhone ContactPhone,
					ACI.numPhoneExtension ContactPhoneExt,
					ACI.numCell ContactCellPhone,					
					 ISNULL((SELECT vcPassword FROM ExtranetAccountsDtl WHERE numContactId=ACI.numContactId),'') AS ContactEcommercepassword,					
					(select U.txtSignature from UserMaster U where U.numUserDetailId=ACI.numRecOwner) AS [Signature],
					dbo.GetListIemName(C.numNoOfEmployeesId) OrgNoOfEmployee,
					(CASE WHEN @numDomainID=1 THEN ISNULL((SELECT TOP 1 LD.vcData FROM CFW_FLD_Values CFV INNER JOIN ListDetails LD ON CFV.Fld_Value=LD.numListItemID WHERE CFV.RecId=DM.numDivisionID AND Fld_ID=12882),'') ELSE '' END) Cust12882
            FROM    dbo.CompanyInfo C
                    INNER JOIN dbo.DivisionMaster DM ON DM.numCompanyId = C.numCompanyId
                    INNER JOIN dbo.AdditionalContactsInformation ACI ON ACI.numDivisionId = DM.numDivisionID
					LEFT JOIN dbo.AddressDetails AD2 ON AD2.numDomainID=DM.numDomainID 
					AND AD2.numRecordID= DM.numDivisionID AND AD2.tintAddressOf=2 AND AD2.tintAddressType=2 AND AD2.bitIsPrimary=1
					 ---- Added by Priya(9 Feb 2018)----
					LEFT JOIN dbo.AddressDetails AD1 ON AD1.numDomainID=DM.numDomainID 
					AND AD1.numRecordID= DM.numDivisionID AND AD1.tintAddressOf=2 AND AD1.tintAddressType=1 AND AD1.bitIsPrimary=1
					LEFT JOIN dbo.BillingTerms BT ON  DM.numBillingDays = BT.numTermsID
								AND ISNULL(BT.bitActive,0) = 1
            WHERE   DM.numDomainId = @numDomainID
                    AND numContactId IN (
                    SELECT  Id
                    FROM    dbo.SplitIDs(@vcRecordIDs, ',') )
        END 
        	
--select * from dbo.OpportunityBizDocs	
--    SELECT TOP 1 bitInterestType,
--            *
--    FROM    dbo.OpportunityBizDocs ORDER BY numoppbizdocsid DESC 
END
--exec USP_GetEmailMergeData @numModuleID=6,@vcRecordIDs='18463',@numDomainID=1,@tintMode=0,@ClientTimeZoneOffset=0
--exec USP_GetEmailMergeData @numModuleID=1,@vcRecordIDs='1',@numDomainID=1,@tintMode=0,@ClientTimeZoneOffset=0
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetGeneralLedger_Virtual')
DROP PROCEDURE USP_GetGeneralLedger_Virtual
GO
CREATE PROCEDURE [dbo].[USP_GetGeneralLedger_Virtual]    
(
      @numDomainID INT,
      @vcAccountId VARCHAR(4000),
      @dtFromDate DATETIME,
      @dtToDate DATETIME,
      @vcTranType VARCHAR(50) = '',
      @varDescription VARCHAR(50) = '',
      @CompanyName VARCHAR(50) = '',
      @vcBizPayment VARCHAR(50) = '',
      @vcCheqNo VARCHAR(50) = '',
      @vcBizDocID VARCHAR(50) = '',
      @vcTranRef VARCHAR(50) = '',
      @vcTranDesc VARCHAR(50) = '',
	  @numDivisionID NUMERIC(9)=0,
      @ClientTimeZoneOffset INT,  --Added by Chintan to enable calculation of date according to client machine
      @charReconFilter CHAR(1)='',
	  @tintMode AS TINYINT=0,
      @numItemID AS NUMERIC(9)=0,
      @CurrentPage INT=0,
	  @PageSize INT=0,
	  @numAccountClass NUMERIC(18,0) = 0,
	  @tintFilterTransactionType TINYINT = 0,
	  @vcFiterName VARCHAR(300) = '',
	  @vcFiterDescription VARCHAR(300) = '',
	  @vcFiterSplit VARCHAR(300) = '',
	  @vcFiterAmount VARCHAR(300) = '',
	  @vcFiterNarration VARCHAR(300) = ''
)
WITH Recompile -- Added by Chandan 26 Feb 2013 to avoid parameter sniffing
AS 
    BEGIN

--IF @IsReport = 1
--BEGIN
--	DECLARE @vcAccountIDs AS VARCHAR(MAX)
--	SELECT @vcAccountIDs = COALESCE(@vcAccountIDs + ',', '') + CAST([COA].[numAccountId] AS VARCHAR(20))
--	FROM [dbo].[Chart_Of_Accounts] AS COA 
--	WHERE [COA].[numDomainId] = @numDomainID
--	PRINT @vcAccountIDs

--	SET @vcAccountId= @vcAccountIDs
--END    

DECLARE @dtFinFromDate DATETIME;
SET @dtFinFromDate = ( SELECT   dtPeriodFrom
                       FROM     FINANCIALYEAR
                       WHERE    dtPeriodFrom <= @dtFromDate
                                AND dtPeriodTo >= @dtFromDate
                                AND numDomainId = @numDomainId
                     );

PRINT @dtFromDate
PRINT @dtToDate

DECLARE @dtFinYearFromJournal DATETIME ;
DECLARE @dtFinYearFrom DATETIME ;
SELECT @dtFinYearFromJournal = MIN(datEntry_Date) FROM General_Journal_Header WHERE numDomainID=@numDomainId 
SET @dtFinYearFrom = ( SELECT   dtPeriodFrom
                        FROM     FINANCIALYEAR
                        WHERE    dtPeriodFrom <= @dtFromDate
                                AND dtPeriodTo >= @dtFromDate
                                AND numDomainId = @numDomainId
                        ) ;

--SELECT * INTO #view_journal FROM view_journal WHERE numDomainID=@numDomainId 

DECLARE @numMaxFinYearID NUMERIC
DECLARE @Month INT
DECLARE @year  INT
SELECT @numMaxFinYearID = MAX(numFinYearId) FROM dbo.FinancialYear WHERE numDomainId=@numDomainId
SELECT @Month = DATEPART(MONTH,dtPeriodFrom),@year =DATEPART(year,dtPeriodFrom) FROM dbo.FinancialYear WHERE numDomainId=@numDomainId AND numFinYearId=@numMaxFinYearID
PRINT @Month
PRINT @year
		 
DECLARE @startRow int
DECLARE @CurrRecord as INT

if @tintMode = 0
BEGIN
SELECT  COA1.numAccountId INTO #Temp1 /*,COA1.vcAccountCode ,COA2.vcAccountCode*/
		FROM    dbo.Chart_Of_Accounts COA1
		WHERE   COA1.numAccountId IN(SELECT  cast(ID AS NUMERIC(9)) FROM dbo.SplitIDs(@vcAccountId, ','))
				AND COA1.numDomainId = @numDomainID
	SELECT @vcAccountId = ISNULL( STUFF((SELECT ',' + CAST(numAccountID AS VARCHAR(10)) FROM #Temp1 FOR XML PATH('')),1, 1, '') , '') 
    
    	--SELECT  @numDomainID AS numDomainID,* FROM dbo.fn_GetOpeningBalance(@vcAccountID,@numDomainID,@dtFromDate,@ClientTimeZoneOffset)
		SELECT  
			@numDomainID AS numDomainID
			,numAccountId
			,vcAccountName
			,numParntAcntTypeID
			,vcAccountDescription
			,vcAccountCode
			,dtAsOnDate
			,CASE 
				WHEN ([Table1].[vcAccountCode] LIKE '0103%' OR [Table1].[vcAccountCode] LIKE '0104%' OR [Table1].[vcAccountCode] LIKE '0106%') AND (@year + @Month) = (DATEPART(YEAR,@dtFromDate) + DATEPART(MONTH,@dtFromDate)) THEN 0 
				ELSE isnull(mnOpeningBalance,0)
			END  [mnOpeningBalance]
		FROM 
		(
			SELECT 
				COA.numAccountId
				,vcAccountName
				,numParntAcntTypeID
				,vcAccountDescription
				,vcAccountCode
				,@dtFromDate dtAsOnDate
				,CASE  
					WHEN (@year + @Month) > (DATEPART(YEAR,@dtFromDate) + DATEPART(MONTH,@dtFromDate)) 
					THEN (SELECT 
							ISNULL(SUM(GJD.numDebitAmt),0) - ISNULL(SUM(GJD.numCreditAmt),0)
						FROM
							General_Journal_Header GJH 
						INNER JOIN
							General_Journal_Details GJD 
						ON 
							GJH.numJournal_Id = GJD.numJournalId
						WHERE
							GJH.numDomainId=@numDomainID
							AND GJH.datEntry_Date BETWEEN (CASE WHEN COA.[vcAccountCode] LIKE '0103%' OR COA.[vcAccountCode] LIKE '0104%' OR COA.[vcAccountCode] LIKE '0106%' 
																	THEN @dtFinYearFrom 
																	ELSE @dtFinYearFromJournal 
																END ) AND  DATEADD(Minute,-1,@dtFromDate)
							AND GJD.numChartAcntId=COA.numAccountId 
							AND (GJD.numClassID = @numAccountClass OR ISNULL(@numAccountClass,0) = 0)) 
					WHEN (@year + @Month) < (DATEPART(YEAR,@dtFromDate) + DATEPART(MONTH,@dtFromDate))
					THEN (SELECT 
							ISNULL(SUM(GJD.numDebitAmt),0) - ISNULL(SUM(GJD.numCreditAmt),0)
						FROM
							General_Journal_Header GJH 
						INNER JOIN
							General_Journal_Details GJD 
						ON 
							GJH.numJournal_Id = GJD.numJournalId
						WHERE
							GJH.numDomainId=@numDomainID
							AND GJH.datEntry_Date BETWEEN (CASE WHEN COA.[vcAccountCode] LIKE '0103%' OR COA.[vcAccountCode] LIKE '0104%' OR COA.[vcAccountCode] LIKE '0106%' 
													THEN @dtFinYearFrom 
													ELSE @dtFinYearFromJournal 
											   END ) AND  DATEADD(Minute,1,@dtFromDate)
							AND GJD.numChartAcntId=COA.numAccountId 
							AND (GJD.numClassID = @numAccountClass OR ISNULL(@numAccountClass,0) = 0)) 
					ELSE
						(SELECT 
							ISNULL(SUM(GJD.numDebitAmt),0) - ISNULL(SUM(GJD.numCreditAmt),0)
						FROM
							General_Journal_Header GJH 
						INNER JOIN
							General_Journal_Details GJD 
						ON 
							GJH.numJournal_Id = GJD.numJournalId
						WHERE
							GJH.numDomainId=@numDomainID
							AND GJH.datEntry_Date <= DateAdd(MINUTE,-@ClientTimeZoneOffset, @dtFromDate)
							AND GJD.numChartAcntId=COA.numAccountId 
							AND (GJD.numClassID = @numAccountClass OR ISNULL(@numAccountClass,0) = 0))
				END AS mnOpeningBalance
			FROM 
				Chart_of_Accounts COA
			WHERE 
				COA.numDomainId=@numDomainId 
				AND COA.numAccountId in (select CAST(ID AS NUMERIC(9)) from SplitIDs(@vcAccountID,','))
		) Table1
		

		--SET @dtFromDate = DateAdd(minute, @ClientTimeZoneOffset, @dtFromDate);
		--SET @dtToDate = DateAdd(minute, @ClientTimeZoneOffset, @dtToDate);

SELECT TOP 1
dbo.General_Journal_Details.numDomainId,
 dbo.Chart_Of_Accounts.numAccountId , 
dbo.General_Journal_Header.numJournal_Id,
dbo.[FormatedDateFromDate](dbo.General_Journal_Header.datEntry_Date,dbo.General_Journal_Details.numDomainID) Date,
dbo.General_Journal_Header.varDescription, 
dbo.CompanyInfo.vcCompanyName AS CompanyName,
dbo.General_Journal_Details.numTransactionId,

ISNULL(ISNULL(ISNULL(dbo.VIEW_BIZPAYMENT.Narration,dbo.General_Journal_Header.varDescription),'') + ' ' + 
(CASE WHEN isnull(dbo.General_Journal_Header.numReturnID, 0) <> 0 THEN ' Chek No: ' + isnull( CAST(CH.numCheckNo AS VARCHAR(50)),'No-Cheq') 
ELSE  dbo.VIEW_JOURNALCHEQ.Narration 
END),'') as Narration,

ISNULL(dbo.General_Journal_Details.vcReference,'') AS TranRef, 
ISNULL(dbo.General_Journal_Details.varDescription,'') AS TranDesc, 
CONVERT(VARCHAR(20),ISNULL(General_Journal_Details.numDebitAmt,0)) numDebitAmt,
CONVERT(VARCHAR(20),ISNULL(General_Journal_Details.numCreditAmt,0)) numCreditAmt,
dbo.Chart_Of_Accounts.vcAccountName,
'' as balance,
CASE 
WHEN isnull(dbo.General_Journal_Header.numCheckHeaderID, 0) <> 0 THEN + 'Checks' 
WHEN isnull(dbo.General_Journal_Header.numDepositId, 0) <> 0 AND DM.tintDepositePage = 1 THEN 'Deposit' 
WHEN isnull(dbo.General_Journal_Header.numDepositId, 0) <> 0 AND DM.tintDepositePage = 2 THEN 'Receved Pmt' 
WHEN isnull(dbo.General_Journal_Header.numOppId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numOppBizDocsId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numBizDocsPaymentDetId, 0) = 0 AND 
dbo.OpportunityMaster.tintOppType = 1 THEN ISNULL(OpportunityBizDocs.vcBizDocID,'BizDocs Invoice')
WHEN isnull(dbo.General_Journal_Header.numOppId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numOppBizDocsId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numBizDocsPaymentDetId, 0) = 0 AND 
dbo.OpportunityMaster.tintOppType = 2 THEN ISNULL(OpportunityBizDocs.vcBizDocID,'BizDocs Purchase')
WHEN isnull(dbo.General_Journal_Header.numCategoryHDRID, 0) <> 0 THEN 'Time And Expenses' 
WHEN ISNULL(General_Journal_Header.numBillId,0) <>0 AND isnull(BH.bitLandedCost,0)=1 then 'Landed Cost' 
WHEN isnull(dbo.General_Journal_Header.numBillID, 0) <> 0 THEN 'Bill'
WHEN isnull(dbo.General_Journal_Header.numBillPaymentID, 0) <> 0 THEN 'Pay Bill'
WHEN isnull(dbo.General_Journal_Header.numReturnID, 0) <> 0 THEN 
																CASE 
																	WHEN RH.tintReturnType=1 AND RH.tintReceiveType=1 THEN 'Sales Return Refund'
																	WHEN RH.tintReturnType=1 AND RH.tintReceiveType=2 THEN 'Sales Return Credit Memo'
																	WHEN RH.tintReturnType=2 AND RH.tintReceiveType=2 THEN 'Purchase Return Credit Memo'
																	WHEN RH.tintReturnType=3 AND RH.tintReceiveType=2 THEN 'Credit Memo'
																	WHEN RH.tintReturnType=4 AND RH.tintReceiveType=1 THEN 'Refund Against Credit Memo'
																END 
WHEN isnull(dbo.General_Journal_Header.numOppId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numOppBizDocsId, 0) = 0 THEN 'PO Fulfillment'
WHEN dbo.General_Journal_Header.numJournal_Id <> 0 THEN 'Journal' 
END AS TransactionType,

ISNULL(dbo.General_Journal_Details.bitCleared,0) AS bitCleared,
ISNULL(dbo.General_Journal_Details.bitReconcile,0) AS bitReconcile,
ISNULL(dbo.General_Journal_Details.numItemID,0) AS numItemID,
Case When isnull(BH.bitLandedCost,0)=1 then  isnull(BH.numOppId,0) else 0 end AS numLandedCostOppId,
STUFF((SELECT ', ' + CAST([COA].[vcAccountName] AS VARCHAR(4000))
			FROM [dbo].[General_Journal_Details] AS GJD JOIN [dbo].[Chart_Of_Accounts] AS COA ON GJD.[numChartAcntId] = COA.[numAccountId]
			WHERE [GJD].[numJournalId] = dbo.General_Journal_Header.numJournal_Id 
			AND [COA].[numDomainId] = General_Journal_Header.[numDomainId]
			AND [COA].[numAccountId] <> dbo.Chart_Of_Accounts.numAccountId FOR XML PATH('')) ,1,2, '') AS [vcSplitAccountName]
FROM dbo.General_Journal_Header INNER JOIN
dbo.General_Journal_Details ON dbo.General_Journal_Header.numJournal_Id = dbo.General_Journal_Details.numJournalId INNER JOIN
dbo.Chart_Of_Accounts ON dbo.General_Journal_Details.numChartAcntId = dbo.Chart_Of_Accounts.numAccountId LEFT OUTER JOIN
dbo.TimeAndExpense ON dbo.General_Journal_Header.numCategoryHDRID = dbo.TimeAndExpense.numCategoryHDRID 
LEFT OUTER JOIN dbo.DivisionMaster ON dbo.General_Journal_Details.numCustomerId = dbo.DivisionMaster.numDivisionID and 
dbo.General_Journal_Details.numDomainID = dbo.DivisionMaster.numDomainID
LEFT OUTER JOIN dbo.CompanyInfo ON dbo.DivisionMaster.numCompanyID = dbo.CompanyInfo.numCompanyId 
LEFT OUTER JOIN dbo.OpportunityMaster ON dbo.General_Journal_Header.numOppId = dbo.OpportunityMaster.numOppId LEFT OUTER JOIN
dbo.OpportunityBizDocs ON dbo.General_Journal_Header.numOppBizDocsId = dbo.OpportunityBizDocs.numOppBizDocsId LEFT OUTER JOIN
dbo.VIEW_JOURNALCHEQ ON (dbo.General_Journal_Header.numCheckHeaderID = dbo.VIEW_JOURNALCHEQ.numCheckHeaderID 
or (dbo.General_Journal_Header.numBillPaymentID=dbo.VIEW_JOURNALCHEQ.numReferenceID and dbo.VIEW_JOURNALCHEQ.tintReferenceType=8))LEFT OUTER JOIN
dbo.VIEW_BIZPAYMENT ON dbo.General_Journal_Header.numBizDocsPaymentDetId = dbo.VIEW_BIZPAYMENT.numBizDocsPaymentDetId LEFT OUTER JOIN
dbo.DepositMaster DM ON DM.numDepositId = dbo.General_Journal_Header.numDepositId  LEFT OUTER JOIN
dbo.ReturnHeader RH ON RH.numReturnHeaderID = dbo.General_Journal_Header.numReturnID LEFT OUTER JOIN
dbo.CheckHeader CH ON CH.numReferenceID=RH.numReturnHeaderID AND CH.tintReferenceType=10
LEFT OUTER JOIN BillHeader BH  ON ISNULL(General_Journal_Header.numBillId,0) = BH.numBillId

WHERE   dbo.General_Journal_Header.datEntry_Date BETWEEN  @dtFromDate AND @dtToDate -- Removed Tmezone bug id 1029 
AND dbo.General_Journal_Header.numDomainId = @numDomainID 
 AND (dbo.DivisionMaster.numDivisionID = @numDivisionID OR @numDivisionID = 0)
AND (bitCleared = CASE @charReconFilter WHEN 'C' THEN 1 ELSE 0 END  OR RTRIM(@charReconFilter)='A')
AND (bitReconcile = CASE @charReconFilter WHEN 'R' THEN 1 ELSE 0 END  OR RTRIM(@charReconFilter)='A') 
AND (dbo.Chart_Of_Accounts.numAccountId IN (SELECT numAccountId FROM #Temp1))
AND (@tintFilterTransactionType = 0 OR 1 = (CASE @tintFilterTransactionType
												WHEN 1 THEN (CASE WHEN ISNULL(dbo.General_Journal_Header.numBillID, 0) <> 0 THEN 1 ELSE 0 END)
												WHEN 2 THEN (CASE WHEN isnull(dbo.General_Journal_Header.numOppId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numOppBizDocsId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numBizDocsPaymentDetId, 0) = 0 AND dbo.OpportunityMaster.tintOppType = 1 THEN 1 ELSE 0 END)
												WHEN 3 THEN (CASE WHEN isnull(dbo.General_Journal_Header.numOppId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numOppBizDocsId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numBizDocsPaymentDetId, 0) = 0 AND dbo.OpportunityMaster.tintOppType = 2 THEN 1 ELSE 0 END)
												WHEN 4 THEN (CASE WHEN isnull(dbo.General_Journal_Header.numCheckHeaderID, 0) <> 0 THEN 1 ELSE 0 END)
												WHEN 5 THEN (CASE WHEN isnull(dbo.General_Journal_Header.numReturnID, 0) <> 0 AND RH.tintReturnType=3 AND RH.tintReceiveType=2 THEN 1 ELSE 0 END)
												WHEN 6 THEN (CASE WHEN isnull(dbo.General_Journal_Header.numDepositId, 0) <> 0 AND DM.tintDepositePage = 1 THEN 1 ELSE 0 END)
												WHEN 7 THEN (CASE WHEN dbo.General_Journal_Header.numJournal_Id <> 0 THEN 1 ELSE 0 END)
												WHEN 8 THEN (CASE WHEN ISNULL(General_Journal_Header.numBillId,0) <>0 AND isnull(BH.bitLandedCost,0)=1 THEN 1 ELSE 0 END)
												WHEN 9 THEN (CASE WHEN isnull(dbo.General_Journal_Header.numBillPaymentID, 0) <> 0 THEN 1 ELSE 0 END)
												WHEN 10 THEN (CASE WHEN isnull(dbo.General_Journal_Header.numOppId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numOppBizDocsId, 0) = 0 THEN 1 ELSE 0 END)
												WHEN 11 THEN (CASE WHEN isnull(dbo.General_Journal_Header.numReturnID, 0) <> 0 AND RH.tintReturnType=2 AND RH.tintReceiveType=2 THEN 1 ELSE 0 END)
												WHEN 12 THEN (CASE WHEN isnull(dbo.General_Journal_Header.numDepositId, 0) <> 0 AND DM.tintDepositePage = 2 THEN 1 ELSE 0 END)
												WHEN 13 THEN (CASE WHEN isnull(dbo.General_Journal_Header.numReturnID, 0) <> 0 AND RH.tintReturnType=4 AND RH.tintReceiveType=1 THEN 1 ELSE 0 END)
												WHEN 14 THEN (CASE WHEN isnull(dbo.General_Journal_Header.numReturnID, 0) <> 0 AND RH.tintReturnType=1 AND RH.tintReceiveType=2 THEN 1 ELSE 0 END)
												WHEN 15 THEN (CASE WHEN isnull(dbo.General_Journal_Header.numReturnID, 0) <> 0 AND RH.tintReturnType=1 AND RH.tintReceiveType=1 THEN 1 ELSE 0 END)
												WHEN 16 THEN (CASE WHEN isnull(dbo.General_Journal_Header.numCategoryHDRID, 0) <> 0 THEN 1 ELSE 0 END)
												ELSE 0
											END))
AND (ISNULL(@vcFiterName,'') = '' OR  dbo.CompanyInfo.vcCompanyName LIKE CONCAT('%',@vcFiterName,'%'))
AND (ISNULL(@vcFiterDescription,'') = '' OR  dbo.General_Journal_Details.varDescription LIKE CONCAT('%',@vcFiterDescription,'%'))
AND (ISNULL(@vcFiterSplit,'') = '' OR  STUFF((SELECT ', ' + CAST([COA].[vcAccountName] AS VARCHAR(4000))
										FROM [dbo].[General_Journal_Details] AS GJD JOIN [dbo].[Chart_Of_Accounts] AS COA ON GJD.[numChartAcntId] = COA.[numAccountId]
										WHERE [GJD].[numJournalId] = dbo.General_Journal_Header.numJournal_Id 
										AND [COA].[numDomainId] = General_Journal_Header.[numDomainId]
										AND [COA].[numAccountId] <> dbo.Chart_Of_Accounts.numAccountId FOR XML PATH('')) ,1,2, '') LIKE CONCAT('%',@vcFiterSplit,'%'))
AND (ISNULL(@vcFiterAmount,'') = '' OR  CONVERT(VARCHAR(20),ISNULL(General_Journal_Details.numDebitAmt,0)) = @vcFiterAmount)
AND (ISNULL(@vcFiterNarration,'') = '' OR  (ISNULL(ISNULL(ISNULL(dbo.VIEW_BIZPAYMENT.Narration,dbo.General_Journal_Header.varDescription),'') + ' ' + 
											(CASE WHEN isnull(dbo.General_Journal_Header.numReturnID, 0) <> 0 THEN ' Chek No: ' + isnull( CAST(CH.numCheckNo AS VARCHAR(50)),'No-Cheq') 
											ELSE  dbo.VIEW_JOURNALCHEQ.Narration 
											END),'') LIKE CONCAT('%',@vcFiterNarration,'%') OR dbo.General_Journal_Details.vcReference LIKE CONCAT('%',@vcFiterNarration,'%')))
          
--ORDER BY dbo.General_Journal_Header.numEntryDateSortOrder asc
--ORDER BY dbo.Chart_Of_Accounts.numAccountId ASC,  dbo.General_Journal_Header.datEntry_Date ASC, dbo.General_Journal_Details.numTransactionId asc 
END
ELSE IF @tintMode = 1
BEGIN

	SELECT  
		@numDomainID AS numDomainID
		,numAccountId
		,vcAccountName
		,numParntAcntTypeID
		,vcAccountDescription
		,vcAccountCode
		,dtAsOnDate
		,CASE WHEN ([Table1].[vcAccountCode] LIKE '0103%' OR [Table1].[vcAccountCode] LIKE '0104%' OR [Table1].[vcAccountCode] LIKE '0106%') AND (@year + @Month) = (DATEPART(YEAR,@dtFromDate) + DATEPART(MONTH,@dtFromDate)) THEN 0 
			 WHEN (@year + @Month) > (DATEPART(YEAR,@dtFromDate) + DATEPART(MONTH,@dtFromDate)) THEN isnull(t1.[OPENING],0)
			 WHEN (@year + @Month) < (DATEPART(YEAR,@dtFromDate) + DATEPART(MONTH,@dtFromDate)) THEN isnull(t2.[OPENING],0)
			 ELSE isnull(mnOpeningBalance,0)
		END  [mnOpeningBalance]
		,(SELECT 
				ISNULL(SUM(GJD.numDebitAmt),0) - ISNULL(SUM(GJD.numCreditAmt),0)
			FROM
				General_Journal_Details GJD 
			JOIN 
				Domain D 
			ON 
				D.[numDomainId] = GJD.numDomainID AND D.[numDomainId] = @numDomainID
			INNER JOIN 
				General_Journal_Header GJH 
			ON 
				GJD.numJournalId = GJH.numJournal_Id 
				AND GJD.numDomainId = @numDomainID 
				AND GJD.numChartAcntId = [Table1].numAccountId 
				AND GJH.numDomainID = D.[numDomainId] 
				AND GJH.datEntry_Date <= @dtFinFromDate) 
	FROM 
		dbo.fn_GetOpeningBalance(@vcAccountID,@numDomainID,@dtFromDate,@ClientTimeZoneOffset) [Table1]
	OUTER APPLY 
	(SELECT 
			SUM(Debit - Credit) AS OPENING
		FROM
			view_journal VJ
        WHERE 
			VJ.numDomainId = @numDomainId
            AND VJ.numAccountID = [Table1].numAccountID
			AND (numAccountClass = @numAccountClass OR ISNULL(@numAccountClass,0) = 0)
            AND datEntry_Date BETWEEN (CASE 
										WHEN [Table1].[vcAccountCode] LIKE '0103%' OR [Table1].[vcAccountCode] LIKE '0104%' OR [Table1].[vcAccountCode] LIKE '0106%' 
										THEN @dtFinYearFrom 
										ELSE @dtFinYearFromJournal 
									END ) AND  DATEADD(Minute,-1,@dtFromDate)
	) AS t1
	OUTER APPLY 
	(SELECT  
			SUM(Debit - Credit) AS OPENING
		FROM 
			view_journal VJ
        WHERE 
			VJ.numDomainId = @numDomainId
            AND VJ.numAccountID = [Table1].numAccountID
			AND (numAccountClass = @numAccountClass OR ISNULL(@numAccountClass,0) = 0)
            AND datEntry_Date BETWEEN (CASE 
										WHEN [Table1].[vcAccountCode] LIKE '0103%' OR [Table1].[vcAccountCode] LIKE '0104%' OR [Table1].[vcAccountCode] LIKE '0106%' 
											THEN @dtFinYearFrom 
											ELSE @dtFinYearFromJournal 
										END ) AND  DATEADD(Minute,1,@dtFromDate)
	) AS t2


	SELECT 
		dbo.General_Journal_Details.numDomainId
		,dbo.Chart_Of_Accounts.numAccountId
		,dbo.General_Journal_Header.numJournal_Id
		,dbo.[FormatedDateFromDate](dbo.General_Journal_Header.datEntry_Date,dbo.General_Journal_Details.numDomainID) Date
		,dbo.General_Journal_Header.varDescription
		,dbo.CompanyInfo.vcCompanyName AS CompanyName
		,dbo.General_Journal_Details.numTransactionId
		,ISNULL(ISNULL(ISNULL(dbo.VIEW_BIZPAYMENT.Narration,dbo.General_Journal_Header.varDescription),'') + ' ' + 
		(CASE WHEN isnull(dbo.General_Journal_Header.numReturnID, 0) <> 0 THEN ' Chek No: ' + isnull( CAST(CH.numCheckNo AS VARCHAR(50)),'No-Cheq') 
		ELSE  dbo.VIEW_JOURNALCHEQ.Narration 
		END),'') as Narration
		,ISNULL(dbo.General_Journal_Details.vcReference,'') AS TranRef
		,ISNULL(dbo.General_Journal_Details.varDescription,'') AS TranDesc
		,CONVERT(VARCHAR(20),ISNULL(General_Journal_Details.numDebitAmt,0)) numDebitAmt
		,CONVERT(VARCHAR(20),ISNULL(General_Journal_Details.numCreditAmt,0)) numCreditAmt
		,dbo.Chart_Of_Accounts.vcAccountName
		,'' as balance
		,CASE 
			WHEN isnull(dbo.General_Journal_Header.numCheckHeaderID, 0) <> 0 THEN + 'Checks' 
			WHEN isnull(dbo.General_Journal_Header.numDepositId, 0) <> 0 AND DM.tintDepositePage = 1 THEN 'Deposit' 
			WHEN isnull(dbo.General_Journal_Header.numDepositId, 0) <> 0 AND DM.tintDepositePage = 2 THEN 'Receved Pmt' 
			WHEN isnull(dbo.General_Journal_Header.numOppId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numOppBizDocsId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numBizDocsPaymentDetId, 0) = 0 AND dbo.OpportunityMaster.tintOppType = 1 THEN ISNULL(OpportunityBizDocs.vcBizDocID,'BizDocs Invoice')
			WHEN isnull(dbo.General_Journal_Header.numOppId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numOppBizDocsId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numBizDocsPaymentDetId, 0) = 0 AND dbo.OpportunityMaster.tintOppType = 2 THEN ISNULL(OpportunityBizDocs.vcBizDocID,'BizDocs Purchase')
			WHEN isnull(dbo.General_Journal_Header.numCategoryHDRID, 0) <> 0 THEN 'Time And Expenses' 
			WHEN ISNULL(General_Journal_Header.numBillId,0) <>0 AND isnull(BH.bitLandedCost,0)=1 then 'Landed Cost' 
			WHEN isnull(dbo.General_Journal_Header.numBillID, 0) <> 0 THEN 'Bill'
			WHEN isnull(dbo.General_Journal_Header.numBillPaymentID, 0) <> 0 THEN 'Pay Bill'
			WHEN isnull(dbo.General_Journal_Header.numReturnID, 0) <> 0 THEN 
																			CASE 
																				WHEN RH.tintReturnType=1 AND RH.tintReceiveType=1 THEN 'Sales Return Refund'
																				WHEN RH.tintReturnType=1 AND RH.tintReceiveType=2 THEN 'Sales Return Credit Memo'
																				WHEN RH.tintReturnType=2 AND RH.tintReceiveType=2 THEN 'Purchase Return Credit Memo'
																				WHEN RH.tintReturnType=3 AND RH.tintReceiveType=2 THEN 'Credit Memo'
																				WHEN RH.tintReturnType=4 AND RH.tintReceiveType=1 THEN 'Refund Against Credit Memo'
																			END 
			WHEN isnull(dbo.General_Journal_Header.numOppId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numOppBizDocsId, 0) = 0 THEN 'PO Fulfillment'
			WHEN dbo.General_Journal_Header.numJournal_Id <> 0 THEN 'Journal' 
		END AS TransactionType,
		ISNULL(dbo.General_Journal_Details.bitCleared,0) AS bitCleared,
		ISNULL(dbo.General_Journal_Details.bitReconcile,0) AS bitReconcile,
		ISNULL(dbo.General_Journal_Details.numItemID,0) AS numItemID,
		dbo.General_Journal_Details.numEntryDateSortOrder1 AS numEntryDateSortOrder,
		Case When isnull(BH.bitLandedCost,0)=1 then  isnull(BH.numOppId,0) else 0 end AS numLandedCostOppId,
		STUFF((SELECT ', ' + CAST([COA].[vcAccountName] AS VARCHAR(4000))
					FROM [dbo].[General_Journal_Details] AS GJD JOIN [dbo].[Chart_Of_Accounts] AS COA ON GJD.[numChartAcntId] = COA.[numAccountId]
					WHERE [GJD].[numJournalId] = dbo.General_Journal_Header.numJournal_Id 
					AND [COA].[numDomainId] = General_Journal_Header.[numDomainId]
					AND [COA].[numAccountId] <> dbo.Chart_Of_Accounts.numAccountId FOR XML PATH('')) ,1,2, '') AS [vcSplitAccountName]
FROM dbo.General_Journal_Header INNER JOIN
dbo.General_Journal_Details ON dbo.General_Journal_Header.numJournal_Id = dbo.General_Journal_Details.numJournalId INNER JOIN
dbo.Chart_Of_Accounts ON dbo.General_Journal_Details.numChartAcntId = dbo.Chart_Of_Accounts.numAccountId LEFT OUTER JOIN
dbo.TimeAndExpense ON dbo.General_Journal_Header.numCategoryHDRID = dbo.TimeAndExpense.numCategoryHDRID 
LEFT OUTER JOIN dbo.DivisionMaster ON dbo.General_Journal_Details.numCustomerId = dbo.DivisionMaster.numDivisionID and 
dbo.General_Journal_Details.numDomainID = dbo.DivisionMaster.numDomainID
LEFT OUTER JOIN dbo.CompanyInfo ON dbo.DivisionMaster.numCompanyID = dbo.CompanyInfo.numCompanyId 
LEFT OUTER JOIN dbo.OpportunityMaster ON dbo.General_Journal_Header.numOppId = dbo.OpportunityMaster.numOppId LEFT OUTER JOIN
dbo.OpportunityBizDocs ON dbo.General_Journal_Header.numOppBizDocsId = dbo.OpportunityBizDocs.numOppBizDocsId LEFT OUTER JOIN
dbo.VIEW_JOURNALCHEQ ON (dbo.General_Journal_Header.numCheckHeaderID = dbo.VIEW_JOURNALCHEQ.numCheckHeaderID 
or (dbo.General_Journal_Header.numBillPaymentID=dbo.VIEW_JOURNALCHEQ.numReferenceID and dbo.VIEW_JOURNALCHEQ.tintReferenceType=8))LEFT OUTER JOIN
dbo.VIEW_BIZPAYMENT ON dbo.General_Journal_Header.numBizDocsPaymentDetId = dbo.VIEW_BIZPAYMENT.numBizDocsPaymentDetId LEFT OUTER JOIN
dbo.DepositMaster DM ON DM.numDepositId = dbo.General_Journal_Header.numDepositId  LEFT OUTER JOIN
dbo.ReturnHeader RH ON RH.numReturnHeaderID = dbo.General_Journal_Header.numReturnID LEFT OUTER JOIN
dbo.CheckHeader CH ON CH.numReferenceID=RH.numReturnHeaderID AND CH.tintReferenceType=10
LEFT OUTER JOIN BillHeader BH  ON ISNULL(General_Journal_Header.numBillId,0) = BH.numBillId

WHERE   dbo.General_Journal_Header.datEntry_Date BETWEEN  @dtFromDate AND @dtToDate -- Removed Tmezone bug id 1029 
AND dbo.General_Journal_Header.numDomainId = @numDomainID 
AND (dbo.DivisionMaster.numDivisionID = @numDivisionID OR @numDivisionID = 0)
AND (bitCleared = CASE @charReconFilter WHEN 'C' THEN 1 ELSE 0 END  OR RTRIM(@charReconFilter)='A')
AND (bitReconcile = CASE @charReconFilter WHEN 'R' THEN 1 ELSE 0 END  OR RTRIM(@charReconFilter)='A') 
AND (dbo.Chart_Of_Accounts.numAccountId= @vcAccountId)
AND (General_Journal_Details.numItemID = @numItemID OR @numItemID = 0)   
AND (@tintFilterTransactionType = 0 OR 1 = (CASE @tintFilterTransactionType
												WHEN 1 THEN (CASE WHEN ISNULL(dbo.General_Journal_Header.numBillID, 0) <> 0 THEN 1 ELSE 0 END)
												WHEN 2 THEN (CASE WHEN isnull(dbo.General_Journal_Header.numOppId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numOppBizDocsId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numBizDocsPaymentDetId, 0) = 0 AND dbo.OpportunityMaster.tintOppType = 1 THEN 1 ELSE 0 END)
												WHEN 3 THEN (CASE WHEN isnull(dbo.General_Journal_Header.numOppId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numOppBizDocsId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numBizDocsPaymentDetId, 0) = 0 AND dbo.OpportunityMaster.tintOppType = 2 THEN 1 ELSE 0 END)
												WHEN 4 THEN (CASE WHEN isnull(dbo.General_Journal_Header.numCheckHeaderID, 0) <> 0 THEN 1 ELSE 0 END)
												WHEN 5 THEN (CASE WHEN isnull(dbo.General_Journal_Header.numReturnID, 0) <> 0 AND RH.tintReturnType=3 AND RH.tintReceiveType=2 THEN 1 ELSE 0 END)
												WHEN 6 THEN (CASE WHEN isnull(dbo.General_Journal_Header.numDepositId, 0) <> 0 AND DM.tintDepositePage = 1 THEN 1 ELSE 0 END)
												WHEN 7 THEN (CASE WHEN dbo.General_Journal_Header.numJournal_Id <> 0 THEN 1 ELSE 0 END)
												WHEN 8 THEN (CASE WHEN ISNULL(General_Journal_Header.numBillId,0) <>0 AND isnull(BH.bitLandedCost,0)=1 THEN 1 ELSE 0 END)
												WHEN 9 THEN (CASE WHEN isnull(dbo.General_Journal_Header.numBillPaymentID, 0) <> 0 THEN 1 ELSE 0 END)
												WHEN 10 THEN (CASE WHEN isnull(dbo.General_Journal_Header.numOppId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numOppBizDocsId, 0) = 0 THEN 1 ELSE 0 END)
												WHEN 11 THEN (CASE WHEN isnull(dbo.General_Journal_Header.numReturnID, 0) <> 0 AND RH.tintReturnType=2 AND RH.tintReceiveType=2 THEN 1 ELSE 0 END)
												WHEN 12 THEN (CASE WHEN isnull(dbo.General_Journal_Header.numDepositId, 0) <> 0 AND DM.tintDepositePage = 2 THEN 1 ELSE 0 END)
												WHEN 13 THEN (CASE WHEN isnull(dbo.General_Journal_Header.numReturnID, 0) <> 0 AND RH.tintReturnType=4 AND RH.tintReceiveType=1 THEN 1 ELSE 0 END)
												WHEN 14 THEN (CASE WHEN isnull(dbo.General_Journal_Header.numReturnID, 0) <> 0 AND RH.tintReturnType=1 AND RH.tintReceiveType=2 THEN 1 ELSE 0 END)
												WHEN 15 THEN (CASE WHEN isnull(dbo.General_Journal_Header.numReturnID, 0) <> 0 AND RH.tintReturnType=1 AND RH.tintReceiveType=1 THEN 1 ELSE 0 END)
												WHEN 16 THEN (CASE WHEN isnull(dbo.General_Journal_Header.numCategoryHDRID, 0) <> 0 THEN 1 ELSE 0 END)
												ELSE 0
											END))
AND (ISNULL(@vcFiterName,'') = '' OR  dbo.CompanyInfo.vcCompanyName LIKE CONCAT('%',@vcFiterName,'%'))
AND (ISNULL(@vcFiterDescription,'') = '' OR  dbo.General_Journal_Details.varDescription LIKE CONCAT('%',@vcFiterDescription,'%'))
AND (ISNULL(@vcFiterSplit,'') = '' OR  STUFF((SELECT ', ' + CAST([COA].[vcAccountName] AS VARCHAR(4000))
										FROM [dbo].[General_Journal_Details] AS GJD JOIN [dbo].[Chart_Of_Accounts] AS COA ON GJD.[numChartAcntId] = COA.[numAccountId]
										WHERE [GJD].[numJournalId] = dbo.General_Journal_Header.numJournal_Id 
										AND [COA].[numDomainId] = General_Journal_Header.[numDomainId]
										AND [COA].[numAccountId] <> dbo.Chart_Of_Accounts.numAccountId FOR XML PATH('')) ,1,2, '') LIKE CONCAT('%',@vcFiterSplit,'%'))
AND (ISNULL(@vcFiterAmount,'') = '' OR  CONVERT(VARCHAR(20),ISNULL(General_Journal_Details.numDebitAmt,0)) = @vcFiterAmount)
AND (ISNULL(@vcFiterNarration,'') = '' OR  (ISNULL(ISNULL(ISNULL(dbo.VIEW_BIZPAYMENT.Narration,dbo.General_Journal_Header.varDescription),'') + ' ' + 
											(CASE WHEN isnull(dbo.General_Journal_Header.numReturnID, 0) <> 0 THEN ' Chek No: ' + isnull( CAST(CH.numCheckNo AS VARCHAR(50)),'No-Cheq') 
											ELSE  dbo.VIEW_JOURNALCHEQ.Narration 
											END),'') LIKE CONCAT('%',@vcFiterNarration,'%') OR dbo.General_Journal_Details.vcReference LIKE CONCAT('%',@vcFiterNarration,'%')))
          
ORDER BY dbo.General_Journal_Details.numEntryDateSortOrder1 asc
OFFSET (@CurrentPage - 1) * @PageSize ROWS FETCH NEXT @PageSize ROWS ONLY

END
	
IF object_id('TEMPDB.DBO.#Temp1') IS NOT NULL drop table #Temp1	
END
/****** Object:  StoredProcedure [USP_ManageReturnHeaderItems]    ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ManageReturnHeaderItems' ) 
    DROP PROCEDURE USP_ManageReturnHeaderItems
GO
CREATE PROCEDURE [USP_ManageReturnHeaderItems]
    (
      @numReturnHeaderID NUMERIC(18, 0) OUTPUT,
      @vcRMA VARCHAR(100),
      @vcBizDocName VARCHAR(100),
      @numDomainId NUMERIC(18, 0),
      @numUserCntID AS NUMERIC(9) = 0,
      @numDivisionId NUMERIC(18, 0),
      @numContactId NUMERIC(18, 0),
      @numOppId NUMERIC(18, 0),
      @tintReturnType TINYINT,
      @numReturnReason NUMERIC(18, 0),
      @numReturnStatus NUMERIC(18, 0),
      @monAmount DECIMAL(20,5),
      @monTotalTax DECIMAL(20,5),
      @monTotalDiscount DECIMAL(20,5),
      @tintReceiveType TINYINT,
      @vcComments TEXT,
      @strItems TEXT = '',
      @tintMode TINYINT = 0,
      @numBillAddressId NUMERIC(18, 0),
      @numShipAddressId NUMERIC(18, 0),
      @numDepositIDRef NUMERIC(18,0),
      @numBillPaymentIDRef NUMERIC(18,0),
	  @numParentID NUMERIC(18,0) = 0,
	  @numReferenceSalesOrder NUMERIC(18,0) = 0,
	  @numOppBizDocsId NUMERIC(18,0) = 0
    )
AS 
BEGIN 
BEGIN TRY
BEGIN TRANSACTION
    DECLARE @hDocItem INT                                                                                                                                                                
 
    IF @numReturnHeaderID = 0 
    BEGIN             
		-- GET ACCOUNT CLASS IF ENABLED
		DECLARE @numAccountClass AS NUMERIC(18) = 0

		DECLARE @tintDefaultClassType AS INT = 0
		SELECT @tintDefaultClassType = ISNULL(tintDefaultClassType,0) FROM Domain WHERE numDomainId = @numDomainID 

		IF @tintDefaultClassType = 1 --USER
		BEGIN
			SELECT 
				@numAccountClass=ISNULL(numDefaultClass,0) 
			FROM 
				dbo.UserMaster UM 
			JOIN 
				dbo.Domain D 
			ON 
				UM.numDomainID=D.numDomainId
			WHERE 
				D.numDomainId=@numDomainId 
				AND UM.numUserDetailId=@numUserCntID
		END
		ELSE IF @tintDefaultClassType = 2 --COMPANY
		BEGIN
			SELECT 
				@numAccountClass=ISNULL(numAccountClassID,0) 
			FROM 
				dbo.DivisionMaster DM 
			WHERE 
				DM.numDomainId=@numDomainId 
				AND DM.numDivisionID=@numDivisionID
		END
		ELSE
		BEGIN
			SET @numAccountClass = 0
		END
                   
        INSERT  INTO [ReturnHeader]
		(
			[vcRMA],[vcBizDocName],[numDomainId],[numDivisionId],[numContactId],
            [numOppId],[tintReturnType],[numReturnReason],[numReturnStatus],[monAmount],
            [monTotalTax],[monTotalDiscount],[tintReceiveType],[vcComments],
            [numCreatedBy],[dtCreatedDate],monBizDocAmount,monBizDocUsedAmount,numDepositIDRef,
			numBillPaymentIDRef,numAccountClass,numParentID,IsUnappliedPayment,numReferenceSalesOrder,numOppBizDocID
		)
        SELECT  
			@vcRMA,@vcBizDocName,@numDomainId,@numDivisionId,@numContactId,@numOppId,
            @tintReturnType,@numReturnReason,@numReturnStatus,@monAmount,@monTotalTax,
			@monTotalDiscount,@tintReceiveType,@vcComments,@numUserCntID,GETUTCDATE(),0,0,
			@numDepositIDRef,@numBillPaymentIDRef,@numAccountClass,@numParentID,
			(CASE WHEN (SELECT [DM].[tintDepositePage] FROM [dbo].[DepositMaster] AS DM 
						WHERE DM.[numDepositId] = @numDepositIDRef AND [DM].[numDomainId] = @numDomainId) = 2 THEN 1
					ELSE 0
			END),@numReferenceSalesOrder,@numOppBizDocsId
                    
        SET @numReturnHeaderID = SCOPE_IDENTITY()                                        
        SELECT  @numReturnHeaderID

		--Update DepositMaster if Refund UnApplied Payment
		IF @numDepositIDRef>0 AND @tintReturnType=4
		BEGIN
			UPDATE dbo.DepositMaster SET monRefundAmount=@monAmount WHERE numDepositId=@numDepositIDRef
		END
						
		--Update BillPaymentHeader if Refund UnApplied Payment
		IF @numBillPaymentIDRef>0 AND @tintReturnType=4
		BEGIN
			UPDATE dbo.BillPaymentHeader SET monRefundAmount=@monAmount WHERE numBillPaymentID=@numBillPaymentIDRef
		END
						
        DECLARE @tintType TINYINT 
        SET @tintType=CASE When @tintReturnType=1 OR @tintReturnType=2 THEN 7 
								WHEN @tintReturnType=3 THEN 6
								WHEN @tintReturnType=4 THEN 5 END
												
        EXEC dbo.USP_UpdateBizDocNameTemplate
				@tintType =  @tintType, --  tinyint
				@numDomainID = @numDomainID, --  numeric(18, 0)
				@RecordID = @numReturnHeaderID --  numeric(18, 0)

        DECLARE @numRMATempID AS NUMERIC(18, 0)
        DECLARE @numBizdocTempID AS NUMERIC(18, 0)

        IF @tintReturnType=1 
        BEGIN
			SELECT TOP 1 @numRMATempID = numBizDocTempID FROM BizDocTemplate
			WHERE   numOppType = 1 AND bitDefault = 1 AND bitEnabled = 1
                AND numDomainID = @numDomainID AND numBizDocID IN ( SELECT TOP 1 numListItemID 
						FROM   dbo.ListDetails WHERE  vcData = 'RMA' AND constFlag = 1 )							
		END
		ELSE IF @tintReturnType=2
        BEGIN
			SELECT TOP 1 @numRMATempID = numBizDocTempID FROM BizDocTemplate
			WHERE   numOppType = 2 AND bitDefault = 1 AND bitEnabled = 1
                AND numDomainID = @numDomainID AND numBizDocID IN ( SELECT TOP 1 numListItemID 
						FROM   dbo.ListDetails WHERE  vcData = 'RMA' AND constFlag = 1 )	
		END			    
        ELSE IF @tintReturnType=3
        BEGIN
			SELECT TOP 1 @numRMATempID = numBizDocTempID FROM    BizDocTemplate
			WHERE   numOppType = 1 AND bitDefault = 1 AND bitEnabled = 1
                AND numDomainID = @numDomainID AND numBizDocID IN ( SELECT TOP 1 numListItemID 
                FROM   dbo.ListDetails WHERE  vcData = 'Credit Memo' AND constFlag = 1 )							
		END
		ELSE IF @tintReturnType=4
        BEGIN
			SELECT TOP 1 @numRMATempID = numBizDocTempID FROM    BizDocTemplate
			WHERE   numOppType = 1 AND bitDefault = 1 AND bitEnabled = 1
                AND numDomainID = @numDomainID AND numBizDocID IN ( SELECT TOP 1 numListItemID 
                FROM   dbo.ListDetails WHERE  vcData = 'Refund Receipt' AND constFlag = 1 )							
                                
            SET @numBizdocTempID=@numRMATempID    
		END
                                               
        UPDATE  dbo.ReturnHeader
        SET     numRMATempID = ISNULL(@numRMATempID,0),
                numBizdocTempID = ISNULL(@numBizdocTempID,0)
        WHERE   numReturnHeaderID = @numReturnHeaderID
			                
		--Add/Update Address Details
        SET @numOppID = NULLIF(@numOppID, 0)
		            
        DECLARE @vcStreet VARCHAR(100),
            @vcCity VARCHAR(50),
            @vcPostalCode VARCHAR(15),
            @vcCompanyName VARCHAR(100),
            @vcAddressName VARCHAR(50),
			@vcAltContact VARCHAR(200)     
        DECLARE @numState NUMERIC(9),
            @numCountry NUMERIC(9),
            @numCompanyId NUMERIC(9), @numContact NUMERIC(18,0) 
        DECLARE @bitIsPrimary BIT, @bitAltContact BIT;

        SELECT  @vcCompanyName = vcCompanyName,
                @numCompanyId = div.numCompanyID
        FROM    CompanyInfo Com
                JOIN divisionMaster Div ON div.numCompanyID = com.numCompanyID
        WHERE   div.numdivisionID = @numDivisionId

		--Bill Address
        IF @numBillAddressId > 0 
        BEGIN
            SELECT  @vcStreet = ISNULL(vcStreet, ''),
                    @vcCity = ISNULL(vcCity, ''),
                    @vcPostalCode = ISNULL(vcPostalCode, ''),
                    @numState = ISNULL(numState, 0),
                    @numCountry = ISNULL(numCountry, 0),
                    @bitIsPrimary = bitIsPrimary,
                    @vcAddressName = vcAddressName,
					@numContact=numContact,
					@bitAltContact=bitAltContact,
					@vcAltContact=vcAltContact
            FROM    dbo.AddressDetails
            WHERE   numDomainID = @numDomainID
                    AND numAddressID = @numBillAddressId

            EXEC dbo.USP_UpdateOppAddress @numOppID = @numOppID, --  numeric(9, 0)
                @byteMode = 0, --  tinyint
                @vcStreet = @vcStreet, --  varchar(100)
                @vcCity = @vcCity, --  varchar(50)
                @vcPostalCode = @vcPostalCode, --  varchar(15)
                @numState = @numState, --  numeric(9, 0)
                @numCountry = @numCountry, --  numeric(9, 0)
                @vcCompanyName = @vcCompanyName, --  varchar(100)
                @numCompanyId = 0, --  numeric(9, 0)
                @vcAddressName = @vcAddressName,
                @numReturnHeaderID = @numReturnHeaderID,
				@bitCalledFromProcedure=1,
				@numContact = @numContact,
				@bitAltContact = @bitAltContact,
				@vcAltContact = @vcAltContact
        END
    
		--Ship Address
        IF @numShipAddressId > 0 
        BEGIN
            SELECT  @vcStreet = ISNULL(vcStreet, ''),
                    @vcCity = ISNULL(vcCity, ''),
                    @vcPostalCode = ISNULL(vcPostalCode, ''),
                    @numState = ISNULL(numState, 0),
                    @numCountry = ISNULL(numCountry, 0),
                    @bitIsPrimary = bitIsPrimary,
                    @vcAddressName = vcAddressName,
					@numContact=numContact,
					@bitAltContact=bitAltContact,
					@vcAltContact=vcAltContact
            FROM    dbo.AddressDetails
            WHERE   numDomainID = @numDomainID
                    AND numAddressID = @numShipAddressId
 
            EXEC dbo.USP_UpdateOppAddress @numOppID = @numOppID, --  numeric(9, 0)
                @byteMode = 1, --  tinyint
                @vcStreet = @vcStreet, --  varchar(100)
                @vcCity = @vcCity, --  varchar(50)
                @vcPostalCode = @vcPostalCode, --  varchar(15)
                @numState = @numState, --  numeric(9, 0)
                @numCountry = @numCountry, --  numeric(9, 0)
                @vcCompanyName = @vcCompanyName, --  varchar(100)
                @numCompanyId = @numCompanyId, --  numeric(9, 0)
                @vcAddressName = @vcAddressName,
                @numReturnHeaderID = @numReturnHeaderID,
				@bitCalledFromProcedure=1,
				@numContact = @numContact,
				@bitAltContact = @bitAltContact,
				@vcAltContact = @vcAltContact
        END
           
		   
		-- IMPORTANT: KEEP THIS BELOW ADDRESS UPDATE LOGIC 
		IF @tintReturnType=1 OR (@tintReturnType=2 AND (SELECT ISNULL(bitPurchaseTaxCredit,0) FROM Domain WHERE numDomainId=@numDomainId) = 1) 
		BEGIN    
			IF @numOppId > 0
			BEGIN
				INSERT dbo.OpportunityMasterTaxItems 
				(
					numReturnHeaderID,
					numTaxItemID,
					fltPercentage,
					tintTaxType,
					numTaxID
				) 
				SELECT 
					@numReturnHeaderID,
					numTaxItemID,
					fltPercentage,
					tintTaxType,
					numTaxID
				FROM 
					OpportunityMasterTaxItems 
				WHERE 
					numOppID=@numOppID
			END
			ELSE
			BEGIN 
				--Insert Tax for Division                       
				INSERT dbo.OpportunityMasterTaxItems (
					numReturnHeaderID,
					numTaxItemID,
					fltPercentage,
					tintTaxType,
					numTaxID
				) 
				SELECT 
					@numReturnHeaderID,
					TI.numTaxItemID,
					TEMPTax.decTaxValue,
					TEMPTax.tintTaxType,
					0
				FROM 
					TaxItems TI 
				JOIN 
					DivisionTaxTypes DTT 
				ON 
					TI.numTaxItemID = DTT.numTaxItemID
				CROSS APPLY
				(
					SELECT
						decTaxValue,
						tintTaxType,
						numTaxID
					FROM
						dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,TI.numTaxItemID,NULL,0,@numReturnHeaderID)   
				) AS TEMPTax
				WHERE 
					DTT.numDivisionID=@numDivisionId AND DTT.bitApplicable=1
				UNION 
				SELECT 
					@numReturnHeaderID,
					0,
					TEMPTax.decTaxValue,
					TEMPTax.tintTaxType,
					0
				FROM 
					dbo.DivisionMaster 
				CROSS APPLY
				(
					SELECT
						decTaxValue,
						tintTaxType,
						numTaxID
					FROM
						dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,0,NULL,1,@numReturnHeaderID)  
				) AS TEMPTax
				WHERE 
					bitNoTax=0 
					AND numDivisionID=@numDivisionID
				UNION 
				SELECT
					@numReturnHeaderID
					,1
					,decTaxValue
					,tintTaxType
					,numTaxID
				FROM
					dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,1,NULL,1,@numReturnHeaderID)	
			END			  
		END
          
        IF CONVERT(VARCHAR(10), @strItems) <> '' 
        BEGIN
            EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems
			                                                                                                        
            SELECT  
				*
            INTO 
				#temp
            FROM 
				OPENXML (@hDocItem, '/NewDataSet/Item',2) 
			WITH 
			( 
				numReturnItemID NUMERIC, numItemCode NUMERIC, numUnitHour FLOAT, numUnitHourReceived FLOAT, monPrice DECIMAL(30,16), 
				monTotAmount DECIMAL(20,5), vcItemDesc VARCHAR(1000), numWareHouseItemID NUMERIC, vcModelID VARCHAR(200), 
				vcManufacturer VARCHAR(250), numUOMId NUMERIC,numOppItemID NUMERIC, bitDiscountType BIT, fltDiscount FLOAT, bitMarkupDiscount BIT, numOppBizDocItemID NUMERIC(18,0)
			)

            EXEC sp_xml_removedocument @hDocItem 
                
            INSERT  INTO [ReturnItems]
			(
				[numReturnHeaderID],
				[numItemCode],
				[numUnitHour],
				[numUnitHourReceived],
				[monPrice],
				[monTotAmount],
				[vcItemDesc],
				[numWareHouseItemID],
				[vcModelID],
				[vcManufacturer],
				[numUOMId],numOppItemID,numOppBizDocItemID,
				bitDiscountType,fltDiscount
				,bitMarkupDiscount
			)
			SELECT  
				@numReturnHeaderID,
				numItemCode,
				numUnitHour,
				0,
				monPrice,
				monTotAmount,
				vcItemDesc,
				NULLIF(numWareHouseItemID, 0) numWareHouseItemID,
				vcModelID,
				vcManufacturer,
				numUOMId,numOppItemID,numOppBizDocItemID,
				bitDiscountType,
				fltDiscount
				,bitMarkupDiscount
			FROM 
				#temp
			WHERE 
				numReturnItemID = 0
		
			UPDATE
				ReturnItems
			SET
				monAverageCost = (CASE 
									WHEN @numOppId > 0 
									THEN 
										(CASE 
											WHEN @tintReturnType=2 
											THEN
												ISNULL((SELECT monPrice FROM OpportunityItems WHERE numOppId=@numOppId AND numoppitemtCode=ReturnItems.numOppItemID),0)
											ELSE
												ISNULL((SELECT monAvgCost FROM OpportunityItems WHERE numOppId=@numOppId AND numoppitemtCode=ReturnItems.numOppItemID),0)
										END)
									ELSE
										ISNULL((SELECT monAverageCost FROM Item WHERE numItemCode=ReturnItems.numItemCode),0)
								END),
				monVendorCost = (CASE 
									WHEN @numOppId > 0 
									THEN ISNULL((SELECT monVendorCost FROM OpportunityItems WHERE numOppId=@numOppId AND numoppitemtCode=ReturnItems.numOppItemID),0)
									ELSE
										ISNULL((SELECT Vendor.monCost FROM Item INNER JOIN Vendor ON Item.numVendorID=Vendor.numVendorID AND Vendor.numItemCode=Item.numItemCode WHERE Item.numItemCode=ReturnItems.numItemCode),0)
								END)
			WHERE
				numReturnHeaderID=@numReturnHeaderID
			 
            DROP TABLE #temp
   
			IF @tintReturnType=1 OR (@tintReturnType=2 AND (SELECT ISNULL(bitPurchaseTaxCredit,0) FROM Domain WHERE numDomainId=@numDomainId) = 1) 
			BEGIN               
				IF @numOppId>0
				BEGIN
					INSERT INTO dbo.OpportunityItemsTaxItems 
					(
						numReturnHeaderID,numReturnItemID,numTaxItemID,numTaxID
					)  
					SELECT 
						@numReturnHeaderID,OI.numReturnItemID,IT.numTaxItemID,IT.numTaxID
					FROM 
						dbo.ReturnItems OI 
					JOIN 
						dbo.OpportunityItemsTaxItems IT 
					ON 
						OI.numOppItemID=IT.numOppItemID 
					WHERE 
						OI.numReturnHeaderID=@numReturnHeaderID 
						AND IT.numOppId=@numOppId
				END
				ELSE
				BEGIN
					--Insert Tax for ReturnItems
					INSERT INTO dbo.OpportunityItemsTaxItems 
					(
						numReturnHeaderID,
						numReturnItemID,
						numTaxItemID,
						numTaxID
					) 
					SELECT 
						@numReturnHeaderID,
						OI.numReturnItemID,
						IT.numTaxItemID,
						(CASE WHEN IT.numTaxItemID = 1 THEN numTaxID ELSE 0 END)
					FROM 
						dbo.ReturnItems OI 
					JOIN 
						dbo.ItemTax IT 
					ON 
						OI.numItemCode=IT.numItemCode 
					WHERE 
						OI.numReturnHeaderID=@numReturnHeaderID 
						AND IT.bitApplicable=1  
				END
			END
        END 
            
            -- Update leads,prospects to Accounts
		EXEC dbo.USP_UpdateCRMTypeToAccounts @numDivisionID ,@numDomainID ,@numUserCntID
                          
    END    
    ELSE IF @numReturnHeaderID <> 0 
    BEGIN    
        IF ISNULL(@tintMode, 0) = 0
        BEGIN
			UPDATE
				[ReturnHeader]
			SET 
				[vcRMA] = @vcRMA,
				[numReturnReason] = @numReturnReason,
				[numReturnStatus] = @numReturnStatus,
				[monAmount] = ISNULL(monAmount,0) + ISNULL(monTotalDiscount,0) - ISNULL(@monTotalDiscount,0),
				[monTotalDiscount] = @monTotalDiscount,
				[tintReceiveType] = @tintReceiveType,
				[vcComments] = @vcComments,
				[numModifiedBy] = @numUserCntID,
				[dtModifiedDate] = GETUTCDATE()
			WHERE 
				[numDomainId] = @numDomainId
				AND [numReturnHeaderID] = @numReturnHeaderID
		END                 
        ELSE IF ISNULL(@tintMode, 0) = 1 
        BEGIN
            UPDATE  
				ReturnHeader
			SET 
				numReturnStatus = @numReturnStatus
			WHERE 
				numReturnHeaderID = @numReturnHeaderID
							
			IF CONVERT(VARCHAR(10), @strItems) <> '' 
			BEGIN
				EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems
				                                                                                                    
				SELECT 
					*
				INTO 
					#temp1
				FROM OPENXML 
					(@hDocItem, '/NewDataSet/Item',2) 
				WITH 
				( 
					numReturnItemID NUMERIC, 
					numUnitHourReceived FLOAT, 
					numWareHouseItemID NUMERIC
				)
				
				EXEC sp_xml_removedocument @hDocItem 

				UPDATE  
					[ReturnItems]
				SET 
					[numUnitHourReceived] = X.numUnitHourReceived,
					[numWareHouseItemID] =CASE WHEN X.numWareHouseItemID=0 THEN NULL ELSE X.numWareHouseItemID END 
				FROM 
					#temp1 AS X
				WHERE 
					X.numReturnItemID = ReturnItems.numReturnItemID
								AND ReturnItems.numReturnHeaderID = @numReturnHeaderID
				
				DROP TABLE #temp1

				IF EXISTS (SELECT
								RI.numReturnItemID
							FROM
								ReturnItems RI
							INNER JOIN
								WareHouseItems WI
							ON
								RI.numWareHouseItemID = WI.numWareHouseItemID
							INNER JOIN
								OpportunityItems OI
							ON
								RI.numOppItemID = OI.numoppitemtCode
							INNER JOIN
								OpportunityKitItems OKI
							ON
								OI.numoppitemtCode = OKI.numOppItemID
								AND ISNULL(OKi.numWareHouseItemId,0) > 0
							INNER JOIN
								Item I
							ON
								OKI.numChildItemID = I.numItemCode
							WHERE
								RI.numReturnHeaderID=@numReturnHeaderID
								AND ISNULL(I.bitKitParent,0) = 0
								AND 1 = (CASE WHEN (SELECT COUNT(WIInner.numWareHouseItemID) FROM WareHouseItems WIInner WHERE WIInner.numItemID = OKI.numChildItemID AND  WIInner.numWareHouseID=WI.numWareHouseID AND ISNULL(WIInner.numWLocationID,0)=ISNULL(WI.numWLocationID,0)) > 0 THEN 0 ELSE 1 END))
				BEGIN
					RAISERROR('KIT_ITEM_WAREHOUSE_NOT_AVAILABLE_IN_ALL_CHILD_ITEMS',16,1)
				END

				IF EXISTS (SELECT
								RI.numReturnItemID
							FROM
								ReturnItems RI
							INNER JOIN
								WareHouseItems WI
							ON
								RI.numWareHouseItemID = WI.numWareHouseItemID
							INNER JOIN
								OpportunityItems OI
							ON
								RI.numOppItemID = OI.numoppitemtCode
							INNER JOIN
								OpportunityKitChildItems OKCI
							ON
								OI.numoppitemtCode = OKCI.numOppItemID
								AND ISNULL(OKCI.numWareHouseItemId,0) > 0
							INNER JOIN
								Item I
							ON
								OKCI.numItemID = I.numItemCode
							WHERE
								RI.numReturnHeaderID=@numReturnHeaderID
								AND ISNULL(I.bitKitParent,0) = 0
								AND 1 = (CASE WHEN (SELECT COUNT(WIInner.numWareHouseItemID) FROM WareHouseItems WIInner WHERE WIInner.numItemID = OKCI.numItemID AND  WIInner.numWareHouseID=WI.numWareHouseID AND ISNULL(WIInner.numWLocationID,0)=ISNULL(WI.numWLocationID,0)) > 0 THEN 0 ELSE 1 END))
				BEGIN
					RAISERROR('KIT_ITEM_WAREHOUSE_NOT_AVAILABLE_IN_ALL_CHILD_ITEMS',16,1)
				END
			END 
		END
    END   
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SET @numReturnHeaderID = 0

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH         
END
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_ManageRMAInventory' ) 
    DROP PROCEDURE usp_ManageRMAInventory
GO
CREATE PROCEDURE [dbo].[usp_ManageRMAInventory]
    @numReturnHeaderID AS NUMERIC(9) = 0,
    @numDomainId NUMERIC(9),
    @numUserCntID AS NUMERIC(9)=0,
    @tintFlag AS TINYINT  --1: Receive 2: Revert
AS 
  BEGIN

	DECLARE @numOppItemID NUMERIC(18,0)
  	DECLARE @tintReturnType AS TINYINT
	DECLARE @tintReceiveType AS TINYINT
  	DECLARE @numReturnItemID AS NUMERIC
	DECLARE @itemcode AS NUMERIC
	DECLARE @numUnits as FLOAT
	DECLARE @numWareHouseItemID as numeric
	DECLARE @numWLocationID AS NUMERIC(18,0)
	DECLARE @monAmount as DECIMAL(20,5) 
	DECLARE @onHand AS FLOAT
	DECLARE @onOrder AS FLOAT
	DECLARE @onBackOrder AS FLOAT
	DECLARE @onAllocation AS FLOAT
	DECLARE @TotalOnHand FLOAT
	DECLARE @monItemAverageCost DECIMAL(20,5)
	DECLARE @monReturnAverageCost DECIMAL(20,5)
	DECLARE @monNewAverageCost DECIMAL(20,5)
	DECLARE @description AS VARCHAR(100)
	DECLARE @bitKitParent BIT
	DECLARE @bitKitChild BIT
    
  	SELECT 
		@tintReturnType=tintReturnType,
		@tintReceiveType=tintReceiveType
	FROM 
		dbo.ReturnHeader 
	WHERE 
		numReturnHeaderID=@numReturnHeaderID

	DECLARE @TEMPReturnItems TABLE
	(
		ID INT IDENTITY(1,1)
		,numReturnItemID NUMERIC(18,0)
		,numItemCode NUMERIC(18,0)
		,numUnitHourReceived FLOAT
		,numWareHouseItemID NUMERIC(18,0)
		,monTotAmount DECIMAL(20,5)
		,numOppItemID NUMERIC(18,0)
		,monAverageCost DECIMAL(20,5)
		,monReturnAverageCost DECIMAL(20,5)
		,bitKitParent BIT
		,bitKitChild BIT
	)

	INSERT INTO @TEMPReturnItems
	(
		numReturnItemID
		,numItemCode
		,numUnitHourReceived
		,numWareHouseItemID
		,monTotAmount
		,numOppItemID
		,monAverageCost
		,monReturnAverageCost
		,bitKitParent
		,bitKitChild
	)
  	SELECT
		numReturnItemID,
		RI.numItemCode,
		RI.numUnitHourReceived,
  		ISNULL(numWareHouseItemID,0),
		RI.monTotAmount,
		RI.numOppItemID
		,ISNULL(I.monAverageCost,0)
		,ISNULL(OBDI.monAverageCost,RI.monAverageCost)
		,ISNULL(I.bitKitParent,0)
		,0
	FROM 
		ReturnItems RI 
	JOIN 
		Item I 
	ON 
		RI.numItemCode=I.numItemCode
	LEFT JOIN
		OpportunityBizDocItems OBDI
	ON	
		RI.numOppBizDocItemID = OBDI.numOppBizDocItemID               
	WHERE 
		numReturnHeaderID=@numReturnHeaderID 
		AND (charitemtype='P' OR 1=(CASE 
									WHEN @tintReturnType=1 THEN 
										CASE 
											WHEN ISNULL(I.bitKitParent,0) = 1 AND ISNULL(I.bitAssembly,0) = 1 THEN 0
											WHEN ISNULL(I.bitKitParent,0) = 1 THEN 1
											ELSE 0 
										END 
									ELSE 
										0 
									END)) 
	ORDER BY 
		RI.numReturnItemID

	-- INSERT KIT CHILD ITEMS
	INSERT INTO @TEMPReturnItems
	(
		numReturnItemID
		,numItemCode
		,numUnitHourReceived
		,numWareHouseItemID
		,monTotAmount
		,numOppItemID
		,monAverageCost
		,monReturnAverageCost
		,bitKitParent
		,bitKitChild
	)
  	SELECT
		numReturnItemID,
		RI.numItemCode,
		RI.numUnitHourReceived * ISNULL(OKI.numQtyItemsReq_Orig,0),
  		ISNULL((SELECT TOP 1 WIInner.numWareHouseItemID FROM WareHouseItems WIInner WHERE WIInner.numItemID=I.numItemCode AND WIInner.numWareHouseID = WI.numWareHouseID AND ISNULL(WIInner.numWLocationID,0) = ISNULL(WI.numWLocationID,0)),0),
		RI.monTotAmount,
		RI.numOppItemID
		,ISNULL(I.monAverageCost,0)
		,ISNULL(OKI.monAvgCost,0)
		,ISNULL(I.bitKitParent,0)
		,1
	FROM
		ReturnItems RI
	INNER JOIN
		WareHouseItems WI
	ON
		RI.numWareHouseItemID = WI.numWareHouseItemID
	INNER JOIN
		OpportunityKitItems OKI
	ON
		RI.numOppItemID = OKI.numOppItemID
	INNER JOIN
		Item I
	ON
		OKI.numChildItemID = I.numItemCode
	WHERE
		RI.numReturnHeaderID=@numReturnHeaderID 
		AND ISNULL(RI.numOppItemID,0) > 0
		AND ISNULL(OKI.numWareHouseItemId,0) > 0

	INSERT INTO @TEMPReturnItems
	(
		numReturnItemID
		,numItemCode
		,numUnitHourReceived
		,numWareHouseItemID
		,monTotAmount
		,numOppItemID
		,monAverageCost
		,monReturnAverageCost
		,bitKitParent
		,bitKitChild
	)
	SELECT
		numReturnItemID,
		RI.numItemCode,
		RI.numUnitHourReceived * ISNULL(OKI.numQtyItemsReq_Orig,0) * ISNULL(OKCI.numQtyItemsReq_Orig,0),
  		ISNULL((SELECT TOP 1 WIInner.numWareHouseItemID FROM WareHouseItems WIInner WHERE WIInner.numItemID=I.numItemCode AND WIInner.numWareHouseID = WI.numWareHouseID AND ISNULL(WIInner.numWLocationID,0) = ISNULL(WI.numWLocationID,0)),0),
		RI.monTotAmount,
		RI.numOppItemID
		,ISNULL(I.monAverageCost,0)
		,ISNULL(OKCI.monAvgCost,0)
		,ISNULL(I.bitKitParent,0)
		,1
	FROM
		ReturnItems RI
	INNER JOIN
		WareHouseItems WI
	ON
		RI.numWareHouseItemID = WI.numWareHouseItemID
	INNER JOIN
		OpportunityKitChildItems OKCI
	ON
		RI.numOppItemID = OKCI.numOppItemID
	INNER JOIN
		OpportunityKitItems OKI
	ON
		OKCI.numOppChildItemID = OKI.numOppChildItemID
	INNER JOIN
		Item I
	ON
		OKCI.numItemID = I.numItemCode
	WHERE
		RI.numReturnHeaderID=@numReturnHeaderID 
		AND ISNULL(RI.numOppItemID,0) > 0
		AND ISNULL(OKCI.numWareHouseItemId,0) > 0

	DECLARE @k INT = 1
	DECLARE @kCount INT 

	SELECT @kCount = COUNT(*) FROM @TEMPReturnItems

	WHILE @k <= @kCount
	BEGIN
		SELECT TOP 1 
			@numReturnItemID=numReturnItemID,
			@itemcode=numItemCode,
			@numUnits=numUnitHourReceived,
  			@numWareHouseItemID=ISNULL(numWareHouseItemID,0),
			@monAmount=monTotAmount,
			@numOppItemID=numOppItemID
			,@monItemAverageCost=ISNULL(monAverageCost,0)
			,@monReturnAverageCost=ISNULL(monReturnAverageCost,0)
			,@bitKitParent=ISNULL(bitKitParent,0)
			,@bitKitChild=ISNULL(bitKitChild,0)
		FROM 
			@TEMPReturnItems
		WHERE
			ID = @k
		
		IF ISNULL(@bitKitParent,0) = 0
		BEGIN
			IF @numWareHouseItemID > 0
			BEGIN
				SELECT @TotalOnHand=SUM(ISNULL(numOnHand,0)) + SUM(ISNULL(numAllocation,0)) FROM dbo.WareHouseItems WHERE numItemID=@itemcode
		             
				SELECT  
					@onHand = ISNULL(numOnHand, 0),
					@onAllocation = ISNULL(numAllocation, 0),
					@onOrder = ISNULL(numOnOrder, 0),
					@onBackOrder = ISNULL(numBackOrder, 0),
					@numWLocationID=ISNULL(numWLocationID,0)
				FROM 
					WareHouseItems
				WHERE 
					numWareHouseItemID = @numWareHouseItemID 
			
				--Receive : SalesReturn 
				IF (@tintFlag=1 AND @tintReturnType=1)
				BEGIN
					SET @description=CONCAT('Sales Return Receive',(CASE WHEN ISNULL(@bitKitChild,0) = 1 THEN ' Kit Child ' ELSE ' ' END),'(Qty:',@numUnits,')')
										
					IF @onBackOrder >= @numUnits 
					BEGIN            
						SET @onBackOrder = @onBackOrder - @numUnits            
						SET @onAllocation = @onAllocation + @numUnits            
					END            
					ELSE 
					BEGIN            
						SET @onAllocation = @onAllocation + @onBackOrder            
						SET @numUnits = @numUnits - @onBackOrder            
						SET @onBackOrder = 0            
						SET @onHand = @onHand + @numUnits            
					END      
				
					IF @TotalOnHand + @numUnits > 0
					BEGIN
						SET @monNewAverageCost = ((@TotalOnHand * @monItemAverageCost) + (@numUnits * @monReturnAverageCost)) / (@TotalOnHand + @numUnits)
					END
					ELSE
					BEGIN
						SET @monNewAverageCost = @monItemAverageCost
					END
		                            
					UPDATE 
						Item
					SET 
						monAverageCost = (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE @monNewAverageCost END)
					WHERE 
						numItemCode = @itemcode   
				END
				--Revert : SalesReturn
				ELSE IF (@tintFlag=2 AND @tintReturnType=1)
				BEGIN
					SET @description=CONCAT('Sales Return Delete',(CASE WHEN ISNULL(@bitKitChild,0) = 1 THEN ' Kit Child ' ELSE ' ' END),'(Qty:',@numUnits,')')
					
					IF @onHand  - @numUnits >= 0
					BEGIN						
						SET @onHand = @onHand - @numUnits
					END
					ELSE IF (@onHand + @onAllocation)  - @numUnits >= 0
					BEGIN						
						SET @numUnits = @numUnits - @onHand
						SET @onHand = 0 
                            
						SET @onBackOrder = @onBackOrder + @numUnits
						SET @onAllocation = @onAllocation - @numUnits  
					END	
					ELSE IF  (@onHand + @onBackOrder + @onAllocation) - @numUnits >= 0
					BEGIN
						SET @numUnits = @numUnits - @onHand
						SET @onHand = 0 
                            
						SET @numUnits = @numUnits - @onAllocation  
						SET @onAllocation = 0 
                            
						SET @onBackOrder = @onBackOrder + @numUnits
					END

					IF @TotalOnHand - @numUnits > 0
					BEGIN
						SET @monNewAverageCost = ((@TotalOnHand * @monItemAverageCost) - (@numUnits * @monReturnAverageCost)) / (@TotalOnHand - @numUnits)
					END
					ELSE
					BEGIN
						SET @monNewAverageCost = @monItemAverageCost
					END
				                    
					UPDATE 
						Item
					SET 
						monAverageCost = (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE @monNewAverageCost END)
					WHERE 
						numItemCode = @itemcode
				END
				--Revert : PurchaseReturn
				ELSE IF (@tintFlag=2 AND @tintReturnType=2) 
				BEGIN
					DECLARE @numQtyReturnRemainingToDelete FLOAT
					SET @numQtyReturnRemainingToDelete = @numUnits
				
					IF EXISTS (SELECT ID FROM OpportunityItemsReceievedLocationReturn WHERE numReturnID=@numReturnHeaderID AND numReturnItemID = @numReturnItemID)
					BEGIN
						-- DELETE RETURN QTY
						DECLARE @TEMPReturn TABLE
						(
							ID INT,
							numWarehouseItemID NUMERIC(18,0),
							numReturnedQty FLOAT
						)

						INSERT INTO @TEMPReturn
						(
							ID,
							numWarehouseItemID,
							numReturnedQty
						)
						SELECT
							ROW_NUMBER() OVER(ORDER BY numWarehouseItemID)
							,numWarehouseItemID
							,SUM(numReturnedQty)
						FROM
							OpportunityItemsReceievedLocationReturn OITLR
						INNER JOIN
							OpportunityItemsReceievedLocation OIRL
						ON
							OITLR.numOIRLID = OIRL.ID
						WHERE
							numReturnID=@numReturnHeaderID
							AND numReturnItemID = @numReturnItemID
						GROUP BY
							numWarehouseItemID

						DECLARE @i AS INT = 1
						DECLARE @iCOUNT AS INT
						DECLARE @numQtyReturned FLOAT
						DECLARE @numTempReturnWarehouseItemID NUMERIC(18,0)

						SELECT @iCOUNT = COUNT(*) FROM @TEMPReturn
							
						WHILE @i <= @iCOUNT
																																			BEGIN
						SELECT 
							@numTempReturnWarehouseItemID=numWarehouseItemID,
							@numQtyReturned=numReturnedQty
						FROM 
							@TEMPReturn
						WHERE
							ID = @i
						
						-- INCREASE THE OnHand Of Destination Location
						UPDATE
							WareHouseItems
						SET
							numBackOrder = (CASE WHEN numBackOrder >= @numQtyReturned THEN ISNULL(numBackOrder,0) - @numQtyReturned ELSE 0 END),         
							numAllocation = (CASE WHEN numBackOrder >= @numQtyReturned THEN ISNULL(numAllocation,0) + @numQtyReturned ELSE ISNULL(numAllocation,0) + ISNULL(numBackOrder,0) END),
							numOnHand = (CASE WHEN numBackOrder >= @numQtyReturned THEN numOnHand ELSE ISNULL(numOnHand,0) + @numQtyReturned - ISNULL(numBackOrder,0) END),
							dtModified = GETDATE()
						WHERE
							numWareHouseItemID=@numTempReturnWarehouseItemID

						SET @numQtyReturnRemainingToDelete = ISNULL(@numQtyReturnRemainingToDelete,0) - ISNULL(@numQtyReturned,0)

						DECLARE @descriptionInner VARCHAR(MAX)=CONCAT('Purchase Return Delete (Qty:',@numQtyReturned,')')

						EXEC dbo.USP_ManageWareHouseItems_Tracking
							@numWareHouseItemID = @numTempReturnWarehouseItemID, --  numeric(9, 0)
							@numReferenceID = @numReturnHeaderID, --  numeric(9, 0)
							@tintRefType = 5, --  tinyint
							@vcDescription = @descriptionInner, --  varchar(100)
							@numModifiedBy = @numUserCntID,
							@numDomainId = @numDomainId

						SET @i = @i + 1
					END

						DELETE FROM OpportunityItemsReceievedLocationReturn WHERE numReturnID=@numReturnHeaderID AND numReturnItemID = @numReturnItemID
					END

					IF ISNULL(@numQtyReturnRemainingToDelete,0) > 0
					BEGIN
						SET @description=CONCAT('Purchase Return Delete',(CASE WHEN ISNULL(@bitKitChild,0) = 1 THEN ' Kit Child ' ELSE ' ' END),'(Qty:',@numQtyReturnRemainingToDelete,')')

						IF @onBackOrder >= @numQtyReturnRemainingToDelete 
						BEGIN            
							SET @onBackOrder = @onBackOrder - @numQtyReturnRemainingToDelete            
							SET @onAllocation = @onAllocation + @numQtyReturnRemainingToDelete            
						END            
						ELSE 
						BEGIN            
							SET @onAllocation = @onAllocation + @onBackOrder            
							SET @numQtyReturnRemainingToDelete = @numQtyReturnRemainingToDelete - @onBackOrder            
							SET @onBackOrder = 0            
							SET @onHand = @onHand + @numQtyReturnRemainingToDelete            
						END
					END

					IF @TotalOnHand + @numUnits > 0
					BEGIN
						SET @monNewAverageCost = ((@TotalOnHand * @monItemAverageCost) + (@numUnits * @monReturnAverageCost)) / (@TotalOnHand + @numUnits)
					END
					ELSE
					BEGIN
						SET @monNewAverageCost = @monItemAverageCost
					END
				               
					UPDATE 
						Item
					SET 
						monAverageCost = (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE @monNewAverageCost END)
					WHERE 
						numItemCode = @itemcode
				END
				--Receive : PurchaseReturn 
				ELSE IF (@tintFlag=1 AND @tintReturnType=2) 
				BEGIN
					DECLARE @numRemainingQtyToReturn AS FLOAT = @numUnits

					-- FIRST TRY TO RETURN QTY FROM OTHER WAREHOUSES WHERE PURCHASE ORDER IS RECIEVED
					IF EXISTS (SELECT ID FROM OpportunityItemsReceievedLocation OIRL WHERE numOppItemID=@numOppItemID AND OIRL.numUnitReceieved - ISNULL((SELECT SUM(numReturnedQty) FROM OpportunityItemsReceievedLocationReturn WHERE numOIRLID = OIRL.ID),0) >= 0)
					BEGIN
						DECLARE @TEMP TABLE
						(
							ID INT,
							numOIRLID NUMERIC(18,0),
							numWarehouseItemID NUMERIC(18,0),
							numUnitReceieved FLOAT,
							numReturnedQty FLOAT
						)

						INSERT INTO 
							@TEMP
						SELECT 
							ROW_NUMBER() OVER(ORDER BY OIRL.ID),
							OIRL.ID,
							OIRL.numWarehouseItemID,
							OIRL.numUnitReceieved,
							ISNULL((SELECT SUM(numReturnedQty) FROM OpportunityItemsReceievedLocationReturn WHERE numOIRLID = OIRL.ID),0)
						FROM
							OpportunityItemsReceievedLocation OIRL
						WHERE 
							numOppItemID=@numOppItemID
							AND (OIRL.numUnitReceieved - ISNULL((SELECT SUM(numReturnedQty) FROM OpportunityItemsReceievedLocationReturn WHERE numOIRLID = OIRL.ID),0)) > 0
						ORDER BY
							(OIRL.numUnitReceieved - ISNULL((SELECT SUM(numReturnedQty) FROM OpportunityItemsReceievedLocationReturn WHERE numOIRLID = OIRL.ID),0)) DESC

						DECLARE @numTempOnHand AS FLOAT
						DECLARE @numTempOIRLID NUMERIC(18,0)
						DECLARE @numTempWarehouseItemID	NUMERIC(18,0)
						DECLARE @numRemaingUnits FLOAT

						DECLARE @j AS INT = 1
						DECLARE @jCOUNT AS INT

						SELECT @jCOUNT = COUNT(*) FROM @TEMP
							
						WHILE @j <= @jCOUNT AND @numRemainingQtyToReturn > 0
																																																																																							BEGIN
						SELECT 
							@numTempOIRLID=numOIRLID,
							@numTempWarehouseItemID=T1.numWarehouseItemID,
							@numTempOnHand = ISNULL(WI.numOnHand,0),
							@numRemaingUnits=ISNULL(numUnitReceieved,0)-ISNULL(numReturnedQty,0)
						FROM
							@TEMP T1
						INNER JOIN
							WareHouseItems WI
						ON
							T1.numWarehouseItemID=WI.numWareHouseItemID
						WHERE
							ID = @j

						IF @numTempOnHand >= @numRemaingUnits
						BEGIN
							UPDATE  
								WareHouseItems
							SET 
								numOnHand = ISNULL(numOnHand,0) - (CASE WHEN @numRemaingUnits >= @numRemainingQtyToReturn THEN @numRemainingQtyToReturn ELSE @numRemaingUnits END),
								dtModified = GETDATE() 
							WHERE 
								numWareHouseItemID = @numTempWarehouseItemID 

							INSERT INTO OpportunityItemsReceievedLocationReturn
							(
								numOIRLID
								,[numReturnID]
								,[numReturnItemID]
								,[numReturnedQty]
							)
							VALUES
							(
								@numTempOIRLID
								,@numReturnHeaderID
								,@numReturnItemID
								,(CASE WHEN @numRemaingUnits >= @numRemainingQtyToReturn THEN @numRemainingQtyToReturn ELSE @numRemaingUnits END)
							)

							SET @descriptionInner = CONCAT('Purchase Return Receive (Qty:',(CASE WHEN @numRemaingUnits >= @numRemainingQtyToReturn THEN @numRemainingQtyToReturn ELSE @numRemaingUnits END),')')

							EXEC dbo.USP_ManageWareHouseItems_Tracking
								@numWareHouseItemID = @numTempWarehouseItemID, --  numeric(9, 0)
								@numReferenceID = @numReturnHeaderID, --  numeric(9, 0)
								@tintRefType = 5, --  tinyint
								@vcDescription = @descriptionInner, --  varchar(100)
								@numModifiedBy = @numUserCntID,
								@numDomainId = @numDomainId

							SET @numRemainingQtyToReturn = @numRemainingQtyToReturn - (CASE WHEN @numRemaingUnits >= @numRemainingQtyToReturn THEN @numRemainingQtyToReturn ELSE @numRemaingUnits END)
						END
						ELSE IF @numTempOnHand > 0
						BEGIN
							UPDATE  
								WareHouseItems
							SET 
								numOnHand = numOnHand - (CASE WHEN @numTempOnHand >= @numRemainingQtyToReturn THEN @numRemainingQtyToReturn ELSE @numTempOnHand END),
								dtModified = GETDATE() 
							WHERE 
								numWareHouseItemID = @numTempWarehouseItemID 

							INSERT INTO OpportunityItemsReceievedLocationReturn
							(
								numOIRLID
								,[numReturnID]
								,[numReturnItemID]
								,[numReturnedQty]
							)
							VALUES
							(
								@numTempOIRLID
								,@numReturnHeaderID
								,@numReturnItemID
								,(CASE WHEN @numTempOnHand >= @numRemainingQtyToReturn THEN @numRemainingQtyToReturn ELSE @numTempOnHand END)
							)

							SET @descriptionInner =CONCAT('Purchase Return Receive (Qty:',(CASE WHEN @numTempOnHand >= @numRemainingQtyToReturn THEN @numRemainingQtyToReturn ELSE @numTempOnHand END),')')

							EXEC dbo.USP_ManageWareHouseItems_Tracking
								@numWareHouseItemID = @numTempWarehouseItemID, --  numeric(9, 0)
								@numReferenceID = @numReturnHeaderID, --  numeric(9, 0)
								@tintRefType = 5, --  tinyint
								@vcDescription = @descriptionInner, --  varchar(100)
								@numModifiedBy = @numUserCntID,
								@numDomainId = @numDomainId

							SET @numRemainingQtyToReturn = @numRemainingQtyToReturn - (CASE WHEN @numTempOnHand >= @numRemainingQtyToReturn THEN @numRemainingQtyToReturn ELSE @numTempOnHand END)
						END

						SET @j = @j + 1
					END
					END

					IF @numRemainingQtyToReturn > 0
																BEGIN
					SET @description=CONCAT('Purchase Return Receive',(CASE WHEN ISNULL(@bitKitChild,0) = 1 THEN ' Kit Child ' ELSE ' ' END),'(Qty:',@numRemainingQtyToReturn,')')

					IF @onHand  - @numRemainingQtyToReturn >= 0
					BEGIN						
						SET @onHand = @onHand - @numRemainingQtyToReturn
					END
					ELSE
					BEGIN
						RAISERROR ( 'PurchaseReturn_Qty', 16, 1 ) ;
						RETURN ;
					END
				END

					IF @TotalOnHand - @numUnits > 0
							BEGIN
					SET @monNewAverageCost = ((@TotalOnHand * @monItemAverageCost) - (@numUnits * @monReturnAverageCost)) / (@TotalOnHand - @numUnits)
		        END
					ELSE
					BEGIN
						SET @monNewAverageCost = @monItemAverageCost
					END
				              
					UPDATE 
						Item
					SET 
						monAverageCost = (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE @monNewAverageCost END)
					WHERE 
						numItemCode = @itemcode
				END 

				IF @tintReturnType=1 OR (@tintReturnType=2 AND @numWareHouseItemID > 0 AND (ISNULL(@numRemainingQtyToReturn,0) > 0 OR ISNULL(@numQtyReturnRemainingToDelete,0) > 0))
				BEGIN
					UPDATE  
						WareHouseItems
					SET 
						numOnHand = @onHand,
						numAllocation = @onAllocation,
						numBackOrder = @onBackOrder,
						numOnOrder = @onOrder,
						dtModified = GETDATE() 
					WHERE 
						numWareHouseItemID = @numWareHouseItemID 

					DECLARE @tintRefType AS TINYINT;SET @tintRefType=5
					  
					EXEC dbo.USP_ManageWareHouseItems_Tracking
					@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
					@numReferenceID = @numReturnHeaderID, --  numeric(9, 0)
					@tintRefType = @tintRefType, --  tinyint
					@vcDescription = @description, --  varchar(100)
					@numModifiedBy = @numUserCntID,
					@numDomainId = @numDomainId
				END         
			END
			ELSE
			BEGIN
				RAISERROR('MISSING_WAREHOUSE',16,1)
			END
		END

		SET @k = @k + 1
	END
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MassSalesFulfillment_GetRecords')
DROP PROCEDURE dbo.USP_MassSalesFulfillment_GetRecords
GO

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_MassSalesFulfillment_GetRecords]
(
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@ClientTimeZoneOffset INT
	,@numViewID NUMERIC(18,0)
	,@numWarehouseID NUMERIC(18,0)
	,@bitGroupByOrder BIT
	,@bitRemoveBO BIT
	,@numShippingZone NUMERIC(18,0)
	,@bitIncludeSearch BIT
	,@vcOrderStauts VARCHAR(MAX)
	,@tintFilterBy TINYINT
	,@vcFilterValue VARCHAR(MAX)
	,@vcCustomSearchValue VARCHAR(MAX)
	,@numBatchID NUMERIC(18,0)
	,@tintPackingViewMode TINYINT
	,@tintPrintBizDocViewMode TINYINT
	,@tintPendingCloseFilter TINYINT
	,@numPageIndex INT
	,@vcSortColumn VARCHAR(100) OUTPUT
	,@vcSortOrder VARCHAR(4) OUTPUT
	,@numTotalRecords NUMERIC(18,0) OUTPUT
)
AS 
BEGIN
	IF @numViewID = 4 OR @numViewID = 5 OR @numViewID = 6 OR (@numViewID=2 AND @tintPackingViewMode=4)
	BEGIN
		SET @bitGroupByOrder = 1
	END

	SET @vcCustomSearchValue = REPLACE(@vcCustomSearchValue,'OpportunityMaster.dtItemReceivedDate','(SELECT TOP 1 OMInner.dtItemReceivedDate FROM OpportunityMaster OMInner INNER JOIN OpportunityItems OIInner ON OMInner.numOppID=OIInner.numOppID WHERE OMInner.tintOppType=2 AND OMInner.tintOppStatus=1 AND OIInner.numItemCode=OpportunityItems.numItemCode ORDER BY OMInner.dtItemReceivedDate DESC)')

	DECLARE @TempFieldsLeft TABLE
	(
		numPKID INT IDENTITY(1,1)
		,ID VARCHAR(50)
		,numFieldID NUMERIC(18,0)
		,vcFieldName VARCHAR(100)
		,vcOrigDbColumnName VARCHAR(100)
		,vcListItemType VARCHAR(10)
		,numListID NUMERIC(18,0)
		,bitAllowSorting BIT
		,bitAllowFiltering BIT
		,vcAssociatedControlType VARCHAR(20)
		,vcLookBackTableName VARCHAR(100)
		,bitCustomField BIT
		,intRowNum INT
		,intColumnWidth FLOAT
		,bitIsRequired BIT
		,bitIsEmail BIT
		,bitIsAlphaNumeric BIT
		,bitIsNumeric BIT
		,bitIsLengthValidation BIT
		,intMaxLength INT
		,intMinLength INT
		,bitFieldMessage BIT
		,vcFieldMessage VARCHAR(MAX)
		,Grp_id INT
	)

	CREATE TABLE #TEMPResult
	(
		numOppID NUMERIC(18,0)
		,numDivisionID NUMERIC(18,0)
		,numTerID NUMERIC(18,0)
		,numOppItemID NUMERIC(18,0)
		,numOppBizDocID NUMERIC(18,0)
		,bintCreatedDate VARCHAR(50)
		,numItemCode NUMERIC(18,0)
		,vcCompanyName VARCHAR(300)
		,vcPoppName VARCHAR(300)
		,numAssignedTo VARCHAR(100)
		,numRecOwner VARCHAR(100)
		,vcBizDocID VARCHAR(300)
		,numStatus VARCHAR(300)
		,txtComments VARCHAR(MAX)
		,numBarCodeId VARCHAR(50)
		,vcLocation VARCHAR(100)
		,vcItemName VARCHAR(300)
		,vcItemDesc VARCHAR(2000)
		,numQtyShipped FLOAT
		,numUnitHour FLOAT
		,monAmountPaid DECIMAL(20,5)
		,monAmountToPay DECIMAL(20,5)
		,numItemClassification VARCHAR(100)
		,vcSKU VARCHAR(50)
		,charItemType VARCHAR(50)
		,vcAttributes VARCHAR(300)
		,vcNotes VARCHAR(MAX)
		,vcItemReleaseDate DATE
		,dtItemReceivedDate VARCHAR(50)
		,intUsedShippingCompany VARCHAR(300)
		,intShippingCompany VARCHAR(300)
		,numPreferredShipVia NUMERIC(18,0)
		,numPreferredShipService NUMERIC(18,0)
		,vcInvoiced FLOAT
		,vcInclusionDetails VARCHAR(MAX)
		,bitPaidInFull BIT
		,numRemainingQty FLOAT
		,numAge VARCHAR(50)
		,dtAnticipatedDelivery  DATE
		,vcShipStatus VARCHAR(MAX)
		,dtExpectedDate VARCHAR(20)
		,dtExpectedDateOrig DATE
		,numQtyPicked FLOAT
		,numQtyPacked FLOAT
		,vcPaymentStatus VARCHAR(MAX)
		,numShipRate VARCHAR(40)
	)

	IF @bitGroupByOrder = 1
	BEGIN
		IF @vcSortColumn NOT IN ('CompanyInfo.vcCompanyName','OpportunityBizDocs.monAmountPaid','OpportunityMaster.bintCreatedDate','OpportunityMaster.bitPaidInFull','OpportunityMaster.numAge','OpportunityMaster.numStatus','OpportunityMaster.vcPoppName','OpportunityMaster.dtItemReceivedDate','OpportunityMaster.numAssignedTo','OpportunityMaster.numRecOwner')
		BEGIN
			SET @vcSortColumn = 'OpportunityMaster.bintCreatedDate'
			SET @vcSortOrder = 'DESC'
		END
	END
	ELSE 
	BEGIN
		IF @vcSortColumn NOT IN ('CompanyInfo.vcCompanyName'
								,'Item.charItemType'
								,'Item.numBarCodeId'
								,'Item.numItemClassification'
								,'Item.vcSKU'
								,'OpportunityBizDocs.monAmountPaid'
								,'OpportunityMaster.dtExpectedDate'
								,'OpportunityItems.numQtyPacked'
								,'OpportunityItems.numQtyPicked'
								,'OpportunityItems.numQtyShipped'
								,'OpportunityItems.numRemainingQty'
								,'OpportunityItems.numUnitHour'
								,'OpportunityItems.vcInvoiced'
								,'OpportunityItems.vcItemName'
								,'OpportunityItems.vcItemReleaseDate'
								,'OpportunityMaster.bintCreatedDate'
								,'OpportunityMaster.bitPaidInFull'
								,'OpportunityMaster.numAge'
								,'OpportunityMaster.numStatus'
								,'OpportunityMaster.vcPoppName'
								,'WareHouseItems.vcLocation'
								,'OpportunityMaster.dtItemReceivedDate')
		BEGIN
			SET @vcSortColumn = 'OpportunityMaster.bintCreatedDate'
			SET @vcSortOrder = 'DESC'
		END
	END

	DECLARE @tintPackingMode TINYINT
	DECLARE @tintInvoicingType TINYINT
	DECLARE @tintCommitAllocation TINYINT
	DECLARE @numShippingServiceItemID NUMERIC(18,0)
	DECLARE @numDefaultSalesShippingDoc NUMERIC(18,0)
	SELECT 
		@tintCommitAllocation=ISNULL(tintCommitAllocation,1) 
		,@numShippingServiceItemID=ISNULL(numShippingServiceItemID,0)
		,@numDefaultSalesShippingDoc=ISNULL(numDefaultSalesShippingDoc,0)
	FROM 
		Domain 
	WHERE 
		numDomainId=@numDomainID 

	IF @numViewID = 2 AND @tintPackingViewMode=3 AND @numDefaultSalesShippingDoc = 0
	BEGIN
		RAISERROR('DEFAULT_SHIPPING_BIZDOC_NOT_CONFIGURED',16,1)
		RETURN
	END

	SELECT
		@tintPackingMode=tintPackingMode
		,@tintInvoicingType=tintInvoicingType
	FROM
		MassSalesFulfillmentConfiguration
	WHERE
		numDomainID = @numDomainID 
		AND numUserCntID = @numUserCntID

	PRINT CONCAT('1-',CONVERT( VARCHAR(24), GETDATE(), 121))

	INSERT INTO @TempFieldsLeft
	(
		ID
		,numFieldID
		,vcFieldName
		,vcOrigDbColumnName
		,vcListItemType
		,numListID
		,bitAllowSorting
		,bitAllowFiltering
		,vcAssociatedControlType
		,vcLookBackTableName
		,bitCustomField
		,intRowNum
		,intColumnWidth
		,bitIsRequired
		,bitIsEmail
		,bitIsAlphaNumeric
		,bitIsNumeric
		,bitIsLengthValidation
		,intMaxLength
		,intMinLength
		,bitFieldMessage
		,vcFieldMessage
		,Grp_id
	)
	SELECT
		CONCAT(141,'~',DFM.numFieldID,'~',0)
		,DFM.numFieldID
		,(CASE WHEN @numViewID=6 AND DFM.numFieldID=248 THEN 'BizDoc' ELSE DFFM.vcFieldName END)
		,DFM.vcOrigDbColumnName
		,ISNULL(DFM.vcListItemType,'')
		,ISNULL(DFM.numListID,0)
		,DFFM.bitAllowSorting
		,DFFM.bitAllowFiltering
		,DFM.vcAssociatedControlType
		,DFM.vcLookBackTableName
		,0
		,DFCD.intRowNum
		,ISNULL(DFCD.intColumnWidth,0)
		,0
		,0
		,0
		,0
		,0
		,0
		,0
		,0
		,''
		,0
	FROM
		DycFormConfigurationDetails DFCD
	INNER JOIN
		DycFormField_Mapping DFFM
	ON
		DFCD.numFormId = DFFM.numFormID
		AND DFCD.numFieldId = DFFM.numFieldID
	INNER JOIN
		DycFieldMaster DFM
	ON
		DFFM.numFieldID = DFM.numFieldId
	WHERE 
		DFCD.numDomainId=@numDomainID 
		AND DFCD.numUserCntID=@numUserCntID 
		AND DFCD.numFormId=141 
		AND DFCD.numViewId=@numViewID
		AND ISNULL(DFFM.bitDeleted,0) = 0
		AND ISNULL(DFFM.bitSettingField,0) = 1
		AND ISNULL(DFCD.bitCustom,0) = 0
	UNION
	SELECT
		CONCAT(141,'~',CFM.Fld_id,'~',0)
		,CFM.Fld_id
		,CFM.Fld_label
		,CONCAT('CUST',CFM.Fld_id)
		,''
		,ISNULL(CFM.numListID,0)
		,0
		,0
		,CFM.Fld_type
		,''
		,1
		,DFCD.intRowNum
		,ISNULL(DFCD.intColumnWidth,0)
		,ISNULL(bitIsRequired,0)
		,ISNULL(bitIsEmail,0)
		,ISNULL(bitIsAlphaNumeric,0)
		,ISNULL(bitIsNumeric,0)
		,ISNULL(bitIsLengthValidation,0)
		,ISNULL(intMaxLength,0)
		,ISNULL(intMinLength,0)
		,ISNULL(bitFieldMessage,0)
		,ISNULL(vcFieldMessage,'')
		,CFM.Grp_id
	FROM
		DycFormConfigurationDetails DFCD
	INNER JOIN
		CFW_Fld_Master CFM
	ON
		DFCD.numFieldId = CFM.Fld_id
	LEFT JOIN
		CFW_Validation CV
	ON
		CFM.Fld_id = CV.numFieldID
	WHERE
		DFCD.numDomainId=@numDomainID 
		AND DFCD.numUserCntID=@numUserCntID 
		AND DFCD.numFormId=141 
		AND DFCD.numViewId=@numViewID
		AND ISNULL(DFCD.bitCustom,0) = 1

	PRINT CONCAT('2-',CONVERT( VARCHAR(24), GETDATE(), 121))

	IF (SELECT COUNT(*) FROM @TempFieldsLeft) = 0
	BEGIN
		INSERT INTO @TempFieldsLeft
		(
			ID
			,numFieldID
			,vcFieldName
			,vcOrigDbColumnName
			,vcListItemType
			,numListID
			,bitAllowSorting
			,bitAllowFiltering
			,vcAssociatedControlType
			,vcLookBackTableName
			,bitCustomField
			,intRowNum
			,intColumnWidth
		)
		SELECT
			CONCAT(141,'~',DFM.numFieldID,'~',0)
			,DFM.numFieldID
			,DFFM.vcFieldName
			,DFM.vcOrigDbColumnName
			,ISNULL(DFM.vcListItemType,'')
			,ISNULL(DFM.numListID,0)
			,DFFM.bitAllowSorting
			,DFFM.bitAllowFiltering
			,DFM.vcAssociatedControlType
			,DFM.vcLookBackTableName
			,0
			,DFFM.[order]
			,100
		FROM
			DycFormField_Mapping DFFM
		INNER JOIN
			DycFieldMaster DFM
		ON
			DFFM.numFieldID = DFM.numFieldId
		WHERE 
			DFFM.numFormId=141 
			AND ISNULL(DFFM.bitDeleted,0) = 0
			AND ISNULL(DFFM.bitSettingField,0) = 1
			AND ISNULL(DFFM.bitDefault,0) = 1
	END

	DECLARE @j INT = 0
	DECLARE @jCount INT
	DECLARE @numFieldId NUMERIC(18,0)
	DECLARE @vcFieldName AS VARCHAR(50)                                                                                                
	DECLARE @vcAssociatedControlType VARCHAR(10)  
	DECLARE @vcDbColumnName VARCHAR(100)
	DECLARE @Grp_id INT                                          
	--DECLARE @numListID AS NUMERIC(9)       
	DECLARE @bitCustomField BIT       
	SET @jCount = (SELECT COUNT(*) FROM @TempFieldsLeft)
	DECLARE @vcCustomFields VARCHAR(MAX) = ''
	DECLARE @vcCustomWhere VARCHAR(MAX) = ''

	WHILE @j <= @jCount
	BEGIN
		SELECT
			@numFieldId=numFieldID,
			@vcFieldName = vcFieldName,
			@vcAssociatedControlType = vcAssociatedControlType,
			@vcDbColumnName = CONCAT('Cust',numFieldID),
			@bitCustomField=ISNULL(bitCustomField,0),
			@Grp_id = ISNULL(Grp_id,0)
		FROM 
			@TempFieldsLeft
		WHERE 
			numPKID = @j     
		
		
			
		IF @bitCustomField = 1
		BEGIN         
			IF @Grp_id = 5
			BEGIN
				SET @vcCustomFields = CONCAT(@vcCustomFields,CONCAT(',dbo.GetCustFldValueOppItems(',@numFieldId,',T1.numOppItemID,T1.numItemCode)'),' [',@vcDbColumnName,']')
			END
			ELSE
			BEGIN
				IF @vcAssociatedControlType = 'TextBox' OR @vcAssociatedControlType = 'TextArea' 
				BEGIN
					SET @vcCustomFields = CONCAT(@vcCustomFields,',CFW',@numFieldId,'.Fld_Value  [',@vcDbColumnName,']')
             
					SET @vcCustomWhere = CONCAT(@vcCustomWhere,' left Join CFW_FLD_Values_Opp CFW',@numFieldId
									,' ON CFW' , @numFieldId , '.Fld_Id='
									,@numFieldId
									, ' and CFW' , @numFieldId
									, '.RecId=T1.numOppID')                                                         
				END   
				ELSE IF @vcAssociatedControlType = 'CheckBox' 
				BEGIN
					SET @vcCustomFields =CONCAT( @vcCustomFields
						, ',case when isnull(CFW'
						, @numFieldId
						, '.Fld_Value,0)=0 then 0 when isnull(CFW'
						, @numFieldId
						, '.Fld_Value,0)=1 then 1 end   ['
						,  @vcDbColumnName
						, ']')               
 
					SET @vcCustomWhere = CONCAT(@vcCustomWhere
						, ' left Join CFW_FLD_Values_Opp CFW'
						, @numFieldId
						, ' ON CFW',@numFieldId,'.Fld_Id='
						, @numFieldId
						, ' and CFW'
						, @numFieldId
						, '.RecId=T1.numOppID   ')                                                    
				END                
				ELSE IF @vcAssociatedControlType = 'DateField' 
				BEGIN
					SET @vcCustomFields = CONCAT(@vcCustomFields
						, ',dbo.FormatedDateFromDate(CFW'
						, @numFieldId
						, '.Fld_Value,'
						, @numDomainId
						, ')  [',@vcDbColumnName ,']' )  
					                  
					SET @vcCustomWhere = CONCAT(@vcCustomWhere
						, ' left Join CFW_FLD_Values_Opp CFW'
						, @numFieldId
						, ' on CFW', @numFieldId, '.Fld_Id='
						, @numFieldId
						, ' and CFW'
						, @numFieldId
						, '.RecId=T1.numOppID   ' )                                                        
				END                
				ELSE IF @vcAssociatedControlType = 'SelectBox' 
				BEGIN
					SET @vcCustomFields = CONCAT(@vcCustomFields,',L',@numFieldId,'.vcData',' [',@vcDbColumnName,']')
					                                                    
					SET @vcCustomWhere = CONCAT(@vcCustomWhere
						, ' left Join CFW_FLD_Values_Opp CFW'
						, @numFieldId
						, ' on CFW',@numFieldId ,'.Fld_Id='
						, @numFieldId
						, ' and CFW'
						, @numFieldId
						, '.RecId=T1.numOppID    ')     
					                                                    
					SET @vcCustomWhere = CONCAT(@vcCustomWhere
						, ' left Join ListDetails L'
						, @numFieldId
						, ' on L'
						, @numFieldId
						, '.numListItemID=CFW'
						, @numFieldId
						, '.Fld_Value' )              
				END
				ELSE
				BEGIN
					SET @vcCustomFields = CONCAT(@vcCustomFields,CONCAT(',dbo.GetCustFldValueOpp(',@numFieldId,',T1.numOppID)'),' [',@vcDbColumnName,']')
				END 
			END
			
		END

		SET @j = @j + 1
	END
	
	PRINT CONCAT('3-',CONVERT( VARCHAR(24), GETDATE(), 121))

	CREATE TABLE #TEMPMSRecords
	(
		numOppID NUMERIC(18,0)
		,numOppItemID NUMERIC(18,0)
		,numOppBizDocID NUMERIC(18,0)
	)

	DECLARE @vcSQL NVARCHAR(MAX)
	DECLARE @vcSQLFinal NVARCHAR(MAX)
	
	SET @vcSQL = CONCAT('INSERT INTO #TEMPMSRecords
						(
							numOppID
							,numOppItemID
							,numOppBizDocID
						)
						SELECT
							OpportunityMaster.numOppId
							,',(CASE WHEN @bitGroupByOrder = 1 THEN '0' ELSE 'OpportunityItems.numoppitemtCode' END),'
							,',(CASE 
									WHEN @numViewID = 4 OR @numViewID = 6 OR (@numViewID=2 AND @tintPackingViewMode=4)
									THEN 'OpportunityBizDocs.numOppBizDocsId'  
									ELSE '0' 
								END),'
						FROM
							OpportunityMaster
						INNER JOIN
							DivisionMaster
						ON
							OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
						INNER JOIN
							CompanyInfo
						ON
							DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
						INNER JOIN 
							OpportunityItems
						ON
							OpportunityMaster.numOppId = OpportunityItems.numOppId
						INNER JOIN
							Item
						ON
							OpportunityItems.numItemCode = Item.numItemCode
						LEFT JOIN
							WareHouseItems
						ON
							OpportunityItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
						',
						(CASE 
							WHEN ISNULL(@numBatchID,0) > 0 
							THEN 
								' INNER JOIN 
										MassSalesFulfillmentBatchOrders
								ON
									OpportunityMaster.numOppId = MassSalesFulfillmentBatchOrders.numOppID
									AND (OpportunityItems.numOppItemtCode = MassSalesFulfillmentBatchOrders.numOppItemID OR ISNULL(MassSalesFulfillmentBatchOrders.numOppItemID,0) = 0)'
							ELSE 
								'' 
						END)	
						,
						(CASE 
							WHEN @numViewID = 4 
							THEN
								'INNER JOIN 
									OpportunityBizDocs
								ON
									OpportunityMaster.numOppId = OpportunityBizDocs.numOppId
									AND OpportunityBizDocs.numBizDocID=287'
							WHEN @numViewID = 6
							THEN
								CONCAT('INNER JOIN 
											OpportunityBizDocs
										ON
											OpportunityMaster.numOppId = OpportunityBizDocs.numOppId
											AND OpportunityBizDocs.numBizDocID=',(CASE @tintPrintBizDocViewMode WHEN 2 THEN 29397 WHEN 3 THEN 296 WHEN 4 THEN 287 ELSE 55206 END))
							WHEN @numViewID = 2 AND @tintPackingViewMode = 4
							THEN 
								'INNER JOIN 
									OpportunityBizDocs
								ON
									OpportunityMaster.numOppId = OpportunityBizDocs.numOppId
									AND OpportunityBizDocs.numBizDocID=@numDefaultSalesShippingDoc'
							ELSE ''

						END)
						,'
						WHERE
							OpportunityMaster.numDomainId=@numDomainID
							AND OpportunityMaster.tintOppType = 1
							AND OpportunityMaster.tintOppStatus = 1
							AND ISNULL(OpportunityMaster.tintshipped,0) = 0
							AND 1 = (CASE 
										WHEN @numViewID IN (1,2) AND ISNULL(@bitRemoveBO,0) = 1
										THEN (CASE 
												WHEN (Item.charItemType = ''p'' AND ISNULL(OpportunityItems.bitDropShip, 0) = 0 AND ISNULL(Item.bitAsset,0)=0)
												THEN 
													(CASE WHEN dbo.CheckOrderItemInventoryStatus(OpportunityItems.numItemCode,(CASE 
				WHEN @numViewID = 1
				THEN ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyPicked,0)
				WHEN @numViewID = 2 AND @tintPackingViewMode = 2
				THEN (CASE WHEN ISNULL(@tintPackingMode,1) = 2 THEN ISNULL(OpportunityItems.numQtyPicked,0) - ISNULL(OpportunityItems.numQtyShipped,0) ELSE ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyShipped,0) END)
				WHEN @numViewID = 2 AND @tintPackingViewMode = 3
				THEN (CASE WHEN ISNULL(@tintPackingMode,1) = 2 THEN ISNULL(OpportunityItems.numQtyPicked,0) ELSE ISNULL(OpportunityItems.numUnitHour,0) END) - ISNULL((SELECT 
																																											SUM(OpportunityBizDocItems.numUnitHour)
																																										FROM
																																											OpportunityBizDocs
																																										INNER JOIN
																																											OpportunityBizDocItems
																																										ON
																																											OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																																										WHERE
																																											OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
																																											AND OpportunityBizDocs.numBizDocId=@numDefaultSalesShippingDoc
					 																																					AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0)
				
				ELSE OpportunityItems.numUnitHour
			END),OpportunityItems.numoppitemtCode,OpportunityItems.numWarehouseItmsID,(CASE WHEN ISNULL(Item.bitKitParent,0) = 1 THEN 1 ELSE 0 END)) = 1 THEN 0 ELSE 1 END)
												ELSE 1
											END) 
										ELSE 1
									END)		
							AND 1 = (CASE WHEN LEN(ISNULL(@vcOrderStauts,'''')) > 0 THEN (CASE WHEN OpportunityMaster.numStatus ',(CASE WHEN ISNULL(@bitIncludeSearch,0) = 0 THEN 'NOT IN' ELSE 'IN' END),' (SELECT Id FROM dbo.SplitIDs(@vcOrderStauts,'','')) THEN 1 ELSE 0 END) ELSE 1 END)
							AND 1 = (CASE 
										WHEN LEN(ISNULL(@vcFilterValue,'''')) > 0
										THEN
											CASE 
												WHEN @tintFilterBy=1 --Item Classification
												THEN (CASE WHEN Item.numItemClassification ',(CASE WHEN ISNULL(@bitIncludeSearch,0) = 0 THEN 'NOT IN' ELSE 'IN' END),' (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,'','')) THEN 1 ELSE 0 END)
												ELSE 1
											END
										ELSE 1
									END)
							AND 1 = (CASE
										WHEN @numViewID = 1 THEN (CASE WHEN ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyPicked,0) > 0 THEN 1 ELSE 0 END)
										WHEN @numViewID = 2 THEN (CASE 
																		WHEN @tintPackingViewMode = 1 
																		THEN (CASE 
																				WHEN ISNULL((SELECT COUNT(*) FROM OpportunityItems OIInner WHERE OIInner.numOppID=OpportunityMaster.numOppId AND OIInner.numItemCode=ISNULL(@numShippingServiceItemID,0)),0) = 0 
																				THEN 1 
																				ELSE 0 
																			END)
																		WHEN @tintPackingViewMode = 2
																		THEN (CASE 
																				WHEN ',(CASE WHEN ISNULL(@tintPackingMode,1) = 2 THEN 'ISNULL(OpportunityItems.numQtyPicked,0)' ELSE 'ISNULL(OpportunityItems.numUnitHour,0)' END),' - ISNULL(OpportunityItems.numQtyShipped,0) > 0
																				THEN 1 
																				ELSE 0 
																			END)
																		WHEN @tintPackingViewMode = 3
																		THEN (CASE 
																				WHEN ',(CASE WHEN ISNULL(@tintPackingMode,1) = 2 THEN 'ISNULL(OpportunityItems.numQtyPicked,0)' ELSE 'ISNULL(OpportunityItems.numUnitHour,0)' END),' - ISNULL((SELECT 
																																																													SUM(OpportunityBizDocItems.numUnitHour)
																																																												FROM
																																																													OpportunityBizDocs
																																																												INNER JOIN
																																																													OpportunityBizDocItems
																																																												ON
																																																													OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																																																												WHERE
																																																													OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
																																																													AND OpportunityBizDocs.numBizDocId=@numDefaultSalesShippingDoc
																																																													AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0) > 0
																				THEN 1 
																				ELSE 0 
																			END)',
																			(CASE 
																				WHEN @numViewID = 2 AND @tintPackingViewMode = 4
																				THEN 'WHEN @tintPackingViewMode = 4
																						THEN (CASE 
																								WHEN ISNULL((SELECT COUNT(*) FROM ShippingBox WHERE ISNULL(vcShippingLabelImage,'''') <> '''' AND ISNULL(vcTrackingNumber,'''') <> '''' AND numShippingReportId IN (SELECT numShippingReportId FROM ShippingReport WHERE ShippingReport.numOppID=OpportunityMaster.numOppId AND ShippingReport.numOppBizDocId=OpportunityBizDocs.numOppBizDocsId)),0) = 0 
																								THEN 1 
																								ELSE 0 
																							END)'
																				ELSE ''
																			END)
																			,'
																		ELSE 0
																	END)
										WHEN @numViewID = 3
										THEN (CASE 
												WHEN (CASE 
														WHEN ISNULL(@tintInvoicingType,0)=1 
														THEN ISNULL(OpportunityItems.numQtyShipped,0) 
														ELSE ISNULL(OpportunityItems.numUnitHour,0) 
													END) - ISNULL((SELECT 
																		SUM(OpportunityBizDocItems.numUnitHour)
																	FROM
																		OpportunityBizDocs
																	INNER JOIN
																		OpportunityBizDocItems
																	ON
																		OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																	WHERE
																		OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
																		AND OpportunityBizDocs.numBizDocId=287
																		AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0) > 0 
												THEN 1
												ELSE 0 
											END)
										WHEN @numViewID = 4 THEN 1
										WHEN @numViewID = 5 THEN 1
										WHEN @numViewID = 6 THEN 1
									END)
							AND 1 = (CASE
										WHEN @numViewID = 1 THEN (CASE WHEN ISNULL(WareHouseItems.numWareHouseItemID,0) > 0 AND ISNULL(OpportunityItems.bitDropship,0) = 0 THEN 1 ELSE 0 END)
										WHEN @numViewID = 2 AND @tintPackingViewMode <> 2 THEN (CASE WHEN ISNULL(WareHouseItems.numWareHouseItemID,0) > 0 AND ISNULL(OpportunityItems.bitDropship,0) = 0 THEN 1 ELSE 0 END)
										WHEN @numViewID = 2 AND @tintPackingViewMode = 2 THEN 1
										WHEN @numViewID = 3 THEN 1
										WHEN @numViewID = 4 THEN 1
										WHEN @numViewID = 5 THEN 1
										WHEN @numViewID = 6 THEN 1
										ELSE 0
									END)',
							(CASE 
								WHEN @numViewID = 5 AND ISNULL(@tintPendingCloseFilter,0) = 2
								THEN ' AND (SELECT 
												COUNT(*) 
											FROM 
											(
												SELECT
													OI.numoppitemtCode,
													ISNULL(OI.numUnitHour,0) AS OrderedQty,
													ISNULL(TempFulFilled.FulFilledQty,0) AS FulFilledQty
												FROM
													OpportunityItems OI
												INNER JOIN
													Item I
												ON
													OI.numItemCode = I.numItemCode
												OUTER APPLY
												(
													SELECT
														SUM(OpportunityBizDocItems.numUnitHour) AS FulFilledQty
													FROM
														OpportunityBizDocs
													INNER JOIN
														OpportunityBizDocItems 
													ON
														OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
													WHERE
														OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
														AND ISNULL(OpportunityBizDocs.numBizDocId,0) = 296
														AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
												) AS TempFulFilled
												WHERE
													OI.numOppID = OpportunityMaster.numOppID
													AND ISNULL(OI.numUnitHour,0) > 0
													AND ISNULL(OI.bitDropShip,0) = 0
											) X
											WHERE
												X.OrderedQty <> X.FulFilledQty) = 0
										AND (SELECT 
												COUNT(*) 
											FROM 
											(
												SELECT
													OI.numoppitemtCode,
													ISNULL(OI.numUnitHour,0) AS OrderedQty,
													ISNULL(TempInvoice.InvoicedQty,0) AS InvoicedQty
												FROM
													OpportunityItems OI
												INNER JOIN
													Item I
												ON
													OI.numItemCode = I.numItemCode
												OUTER APPLY
												(
													SELECT
														SUM(OpportunityBizDocItems.numUnitHour) AS InvoicedQty
													FROM
														OpportunityBizDocs
													INNER JOIN
														OpportunityBizDocItems 
													ON
														OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
													WHERE
														OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
														AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
														AND ISNULL(OpportunityBizDocs.bitAuthoritativeBizDocs,0) = 1
												) AS TempInvoice
												WHERE
													OI.numOppID = OpportunityMaster.numOppID
													AND ISNULL(OI.numUnitHour,0) > 0
											) X
											WHERE
												X.OrderedQty > X.InvoicedQty) = 0'
								ELSE '' 
							END)
							,(CASE WHEN @numViewID = 4 THEN 'AND 1 = (CASE WHEN ISNULL(OpportunityBizDocs.monDealAmount,0) - ISNULL(OpportunityBizDocs.monAmountPaid,0) > 0 THEN 1 ELSE 0 END)' ELSE '' END)
							,' AND 1 = (CASE WHEN @numViewID = 2 AND @tintPackingViewMode = 3 THEN (CASE WHEN ISNULL(Item.bitContainer,0)=1 THEN 0 ELSE 1 END) ELSE 1 END)
								AND 1 = (CASE WHEN ISNULL(@numWarehouseID,0) > 0 AND ISNULL(OpportunityItems.numWarehouseItmsID,0) > 0 THEN (CASE WHEN @numWarehouseID=WareHouseItems.numWarehouseID THEN 1 ELSE 0 END) ELSE 1 END)'
								,(CASE WHEN ISNULL(@numShippingZone,0) > 0 THEN ' AND 1 = (CASE 
																				WHEN ISNULL((SELECT TOP 1 numState FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainID,2)),0) > 0 
																				THEN
																					CASE 
																						WHEN (SELECT COUNT(*) FROM dbo.State WHERE numDomainID=@numDomainId AND numStateID =  (SELECT TOP 1 numState FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainID,2)) AND numShippingZone=@numShippingZone) > 0 
																						THEN 1
																						ELSE 0
																					END
																				ELSE 0 
																			END)' ELSE '' END),
								@vcCustomSearchValue,(CASE WHEN @bitGroupByOrder = 1 THEN CONCAT('GROUP BY OpportunityMaster.numOppId',(CASE WHEN @numViewID = 4 OR (@numViewID=2 AND @tintPackingViewMode=4) THEN ',OpportunityBizDocs.numOppBizDocsId,OpportunityBizDocs.monAmountPaid' WHEN @numViewID = 6 THEN ',OpportunityBizDocs.numOppBizDocsId'  ELSE '' END)) ELSE '' END));

	EXEC sp_executesql @vcSQL, N'@numDomainID NUMERIC(18,0), @vcOrderStauts VARCHAR(MAX), @vcFilterValue VARCHAR(MAX), @tintFilterBy TINYINT, @numViewID NUMERIC(18,0), @tintPackingViewMode TINYINT, @numShippingServiceItemID NUMERIC(18,0), @numWarehouseID NUMERIC(18,0), @numShippingZone NUMERIC(18,0), @numDefaultSalesShippingDoc NUMERIC(18,0), @tintInvoicingType TINYINT, @tintPackingMode TINYINT, @bitRemoveBO BIT', @numDomainID, @vcOrderStauts, @vcFilterValue, @tintFilterBy, @numViewID, @tintPackingViewMode, @numShippingServiceItemID, @numWarehouseID, @numShippingZone, @numDefaultSalesShippingDoc, @tintInvoicingType, @tintPackingMode, @bitRemoveBO;

	PRINT CONCAT('4-',CONVERT( VARCHAR(24), GETDATE(), 121))

	SET @numTotalRecords = (SELECT COUNT(*) FROM #TEMPMSRecords)
	
	PRINT CONCAT('5-',CONVERT( VARCHAR(24), GETDATE(), 121))

	IF @bitGroupByOrder = 1
	BEGIN
		
		SET @vcSQLFinal = CONCAT('

		INSERT INTO #TEMPResult
		(
			numOppID
			,numDivisionID
			,numTerID
			,numOppItemID
			,numOppBizDocID
			,bintCreatedDate
			,vcCompanyName
			,vcPoppName
			,numAssignedTo
			,numRecOwner
			,vcBizDocID
			,numStatus
			,txtComments
			,monAmountPaid
			,monAmountToPay
			,intUsedShippingCompany
			,intShippingCompany
			,numPreferredShipVia
			,numPreferredShipService
			,bitPaidInFull
			,numAge
			,dtAnticipatedDelivery
			,vcShipStatus
			,dtExpectedDate
			,dtExpectedDateOrig
			,vcPaymentStatus
			,numShipRate
		)
		SELECT
			OpportunityMaster.numOppId
			,OpportunityMaster.numDivisionID
			,ISNULL(DivisionMaster.numTerID,0) numTerID
			,0
			,TEMPOrder.numOppBizDocID
			,dbo.FormatedDateFromDate(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,OpportunityMaster.bintCreatedDate),@numDomainID)
			,ISNULL(CompanyInfo.vcCompanyName,'''')
			,ISNULL(OpportunityMaster.vcPoppName,'''')
			,dbo.fn_GetContactName(OpportunityMaster.numAssignedTo)
			,dbo.fn_GetContactName(OpportunityMaster.numRecOwner)
			,(CASE WHEN @numViewID = 4 OR @numViewID = 6 THEN ISNULL(OpportunityBizDocs.vcBizDocID,''-'') ELSE '''' END)
			,dbo.GetListIemName(OpportunityMaster.numStatus)
			,ISNULL(OpportunityMaster.txtComments,'''')
			,ISNULL(TempPaid.monAmountPaid,0)
			,ISNULL(OpportunityBizDocs.monDealAmount,0) - ISNULL(OpportunityBizDocs.monAmountPaid,0)
			,CONCAT(ISNULL(LDOpp.vcData,''-''),'', '',ISNULL(SSOpp.vcShipmentService,''-'')) 
			,CONCAT(ISNULL(LDDiv.vcData,''-''),'', '',ISNULL(SSDiv.vcShipmentService,''-''))
			,ISNULL(DivisionMaster.intShippingCompany,0)
			,ISNULL(DivisionMaster.numDefaultShippingServiceID,0)
			,(CASE WHEN ISNULL(TempPaid.monAmountPaid,0) >= ISNULL(OpportunityMaster.monDealAmount,0) THEN 1 ELSE 0 END)
			,CONCAT(''<span style="color:'',(CASE
						WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) = 0 AND (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) < 4  
						THEN ''#00ff00''
						WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) = 0 AND ((DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) >= 4 AND (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) < 8)
						THEN ''#00b050''
						WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) = 0 AND ((DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) >= 8 AND (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) < 12)
						THEN ''#3399ff''
						WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) = 0 AND ((DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) >= 12 AND (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) < 24)
						THEN ''#ff9900''
						WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) BETWEEN 1 AND 2  
						THEN ''#9b3596''
						ELSE ''#ff0000''
					END),''">'',CONCAT(DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24,''d '',DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24,''h''),''</span>'')
			,'''' dtAnticipatedDelivery
			,'''' vcShipStatus
			,dbo.FormatedDateFromDate(OpportunityMaster.dtExpectedDate,@numDomainID)
			,OpportunityMaster.dtExpectedDate
			,'''' vcPaymentStatus
			,ISNULL(CAST((SELECT SUM(monTotAmount) FROM OpportunityItems OIInner WHERE OIInner.numOppID=OpportunityMaster.numOppId AND OIInner.numItemCode=@numShippingServiceItemID) AS VARCHAR),'''')
		FROM
			#TEMPMSRecords TEMPOrder
		INNER JOIN
			OpportunityMaster
		ON
			TEMPOrder.numOppID = OpportunityMaster.numOppId
		INNER JOIN
			DivisionMaster
		ON
			OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
		INNER JOIN
			CompanyInfo
		ON
			DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
		LEFT JOIN
			OpportunityBizDocs
		ON
			OpportunityMaster.numOppId = OpportunityBizDocs.numOppId
			AND OpportunityBizDocs.numOppBizDocsId = ISNULL(TEMPOrder.numOppBizDocID,0)
		LEFT JOIN
			ListDetails  LDOpp
		ON
			LDOpp.numListID = 82
			AND (LDOpp.numDomainID = @numDomainID OR LDOpp.constFlag=1)
			AND ISNULL(OpportunityMaster.intUsedShippingCompany,0) = LDOpp.numListItemID
		LEFT JOIN
			ShippingService AS SSOpp
		ON
			(SSOpp.numDomainID=@numDomainID OR ISNULL(SSOpp.numDomainID,0)=0) 
			AND OpportunityMaster.numShippingService = SSOpp.numShippingServiceID
		LEFT JOIN
			ListDetails  LDDiv
		ON
			LDDiv.numListID = 82
			AND (LDDiv.numDomainID = @numDomainID OR LDDiv.constFlag=1)
			AND ISNULL(DivisionMaster.intShippingCompany,0) = LDDiv.numListItemID
		LEFT JOIN
			ShippingService AS SSDiv
		ON
			(SSDiv.numDomainID=@numDomainID OR ISNULL(SSDiv.numDomainID,0)=0) 
			AND ISNULL(DivisionMaster.numDefaultShippingServiceID,0) = SSDiv.numShippingServiceID
		OUTER APPLY
		(
			SELECT 
				SUM(OpportunityBizDocs.monAmountPaid) monAmountPaid
			FROM 
				OpportunityBizDocs 
			WHERE 
				OpportunityBizDocs.numOppId=OpportunityMaster.numOppId 
				AND OpportunityBizDocs.numBizDocId=287
		) TempPaid
		ORDER BY ',
		(CASE @vcSortOrder
			WHEN 'CompanyInfo.vcCompanyName' THEN 'CompanyInfo.vcCompanyName '
			WHEN 'OpportunityMaster.numStatus' THEN 'dbo.GetListIemName(OpportunityMaster.numStatus)'
			WHEN 'OpportunityMaster.vcPoppName' THEN 'OpportunityMaster.vcPoppName'
			WHEN 'OpportunityMaster.bintCreatedDate' THEN 'OpportunityMaster.bintCreatedDate'
			WHEN 'OpportunityMaster.numAssignedTo' THEN 'dbo.fn_GetContactName(OpportunityMaster.numAssignedTo)'
			WHEN 'OpportunityMaster.numRecOwner' THEN 'dbo.fn_GetContactName(OpportunityMaster.numRecOwner)'
			WHEN 'OpportunityMaster.numAge' THEN 'OpportunityMaster.bintCreatedDate'
			WHEN 'OpportunityBizDocs.monAmountPaid' THEN 'TempPaid.monAmountPaid'
			WHEN 'OpportunityMaster.bitPaidInFull' THEN '(CASE WHEN ISNULL(TempPaid.monAmountPaid,0) >= ISNULL(OpportunityMaster.monDealAmount,0) THEN 1 ELSE 0 END)'
			ELSE 'OpportunityMaster.bintCreatedDate'
		END),' ',(CASE WHEN ISNULL(@vcSortOrder,'') <> '' THEN @vcSortOrder ELSE 'DESC' END),' OFFSET (@numPageIndex-1) * 200 ROWS FETCH NEXT 200 ROWS ONLY')

		EXEC sp_executesql @vcSQLFinal, N'@numDomainID NUMERIC(18,0), @vcOrderStauts VARCHAR(MAX), @vcFilterValue VARCHAR(MAX), @tintFilterBy TINYINT, @numViewID NUMERIC(18,0), @tintPackingViewMode TINYINT, @numShippingServiceItemID NUMERIC(18,0), @numWarehouseID NUMERIC(18,0), @numShippingZone NUMERIC(18,0), @numDefaultSalesShippingDoc NUMERIC(18,0), @tintInvoicingType TINYINT, @ClientTimeZoneOffset INT, @tintPackingMode TINYINT, @numPageIndex INT', @numDomainID, @vcOrderStauts, @vcFilterValue, @tintFilterBy, @numViewID, @tintPackingViewMode, @numShippingServiceItemID, @numWarehouseID, @numShippingZone, @numDefaultSalesShippingDoc, @tintInvoicingType, @ClientTimeZoneOffset, @tintPackingMode, @numPageIndex;
	END
	ELSE
	BEGIN
		SET @vcSQLFinal = CONCAT('

		INSERT INTO #TEMPResult
		(
			numOppID
			,numDivisionID
			,numTerID
			,numOppItemID
			,numOppBizDocID
			,bintCreatedDate
			,numItemCode
			,vcCompanyName
			,vcPoppName
			,numAssignedTo
			,numRecOwner
			,vcBizDocID
			,numStatus
			,txtComments
			,numBarCodeId
			,vcLocation
			,vcItemName
			,vcItemDesc
			,numQtyShipped
			,numUnitHour
			,monAmountPaid
			,numItemClassification
			,vcSKU
			,charItemType
			,vcAttributes
			,vcNotes
			,vcItemReleaseDate
			,dtItemReceivedDate
			,intUsedShippingCompany
			,intShippingCompany
			,numPreferredShipVia
			,numPreferredShipService
			,vcInvoiced
			,vcInclusionDetails
			,bitPaidInFull
			,numAge
			,dtAnticipatedDelivery
			,vcShipStatus
			,numRemainingQty
			,dtExpectedDate
			,dtExpectedDateOrig
			,numQtyPicked
			,numQtyPacked
			,vcPaymentStatus
			,numShipRate
		)
		SELECT
			OpportunityMaster.numOppId
			,OpportunityMaster.numDivisionID
			,ISNULL(DivisionMaster.numTerID,0) numTerID
			,OpportunityItems.numoppitemtCode
			,0
			,dbo.FormatedDateFromDate(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,OpportunityMaster.bintCreatedDate),@numDomainID)
			,Item.numItemCode
			,ISNULL(CompanyInfo.vcCompanyName,'''')
			,ISNULL(OpportunityMaster.vcPoppName,'''')
			,dbo.fn_GetContactName(OpportunityMaster.numAssignedTo)
			,dbo.fn_GetContactName(OpportunityMaster.numRecOwner)
			,''''
			,dbo.GetListIemName(OpportunityMaster.numStatus)
			,ISNULL(OpportunityMaster.txtComments,'''')
			,ISNULL(Item.numBarCodeId,'''')
			,ISNULL(WarehouseLocation.vcLocation,'''')
			,ISNULL(Item.vcItemName,'''')
			,ISNULL(OpportunityItems.vcItemDesc,'''')
			,ISNULL(OpportunityItems.numQtyShipped,0)
			,ISNULL(OpportunityItems.numUnitHour,0)
			,ISNULL(TempPaid.monAmountPaid,0)
			,dbo.GetListIemName(Item.numItemClassification)
			,ISNULL(Item.vcSKU,'''')
			,(CASE 
				WHEN Item.charItemType=''P''  THEN ''Inventory Item''
				WHEN Item.charItemType=''N'' THEN ''Non Inventory Item'' 
				WHEN Item.charItemType=''S'' THEN ''Service'' 
				WHEN Item.charItemType=''A'' THEN ''Accessory'' 
			END)
			,ISNULL(OpportunityItems.vcAttributes,'''')
			,ISNULL(OpportunityItems.vcNotes,'''')
			,dbo.FormatedDateFromDate(OpportunityItems.ItemReleaseDate,@numDomainID)
			,dbo.FormatedDateFromDate((SELECT TOP 1 OMInner.dtItemReceivedDate FROM OpportunityMaster OMInner INNER JOIN OpportunityItems OIInner ON OMInner.numOppID=OIInner.numOppID WHERE OMInner.tintOppType=2 AND OMInner.tintOppStatus=1 AND OIInner.numItemCode=OpportunityItems.numItemCode ORDER BY OMInner.dtItemReceivedDate DESC),@numDomainID)
			,CONCAT(ISNULL(LDOpp.vcData,''-''),'', '',ISNULL(SSOpp.vcShipmentService,''-'')) 
			,CONCAT(ISNULL(LDDiv.vcData,''-''),'', '',ISNULL(SSDiv.vcShipmentService,''-''))
			,ISNULL(DivisionMaster.intShippingCompany,0)
			,ISNULL(DivisionMaster.numDefaultShippingServiceID,0)
			,ISNULL((SELECT SUM(numUnitHour) FROM OpportunityBizDocs OBD INNER JOIN OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId=OBDI.numOppBizDocID  WHERE OBD.numOppId=OpportunityMaster.numOppId AND OBD.numBizDocId=287 AND OBDI.numOppItemID=OpportunityItems.numoppitemtCode),0)
			,ISNULL(OpportunityItems.vcInclusionDetail,'''')
			,(CASE WHEN ISNULL(TempPaid.monAmountPaid,0) >= ISNULL(OpportunityMaster.monDealAmount,0) THEN 1 ELSE 0 END)
			,CONCAT(''<span style="color:'',(CASE
						WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) = 0 AND (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) < 4  
						THEN ''#00ff00''
						WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) = 0 AND ((DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) >= 4 AND (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) < 8)
						THEN ''#00b050''
						WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) = 0 AND ((DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) >= 8 AND (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) < 12)
						THEN ''#3399ff''
						WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) = 0 AND ((DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) >= 12 AND (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) < 24)
						THEN ''#ff9900''
						WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) BETWEEN 1 AND 2  
						THEN ''#9b3596''
						ELSE ''#ff0000''
					END),''">'',CONCAT(DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24,''d '',DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24,''h''),''</span>'')
			,'''' dtAnticipatedDelivery
			,'''' vcShipStatus
			,(CASE 
				WHEN @numViewID = 1
				THEN ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyPicked,0)
				WHEN @numViewID = 2 AND @tintPackingViewMode = 2
				THEN (CASE WHEN ISNULL(@tintPackingMode,1) = 2 THEN ISNULL(OpportunityItems.numQtyPicked,0) - ISNULL(OpportunityItems.numQtyShipped,0) ELSE ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyShipped,0) END)
				WHEN @numViewID = 2 AND @tintPackingViewMode = 3
				THEN (CASE WHEN ISNULL(@tintPackingMode,1) = 2 THEN ISNULL(OpportunityItems.numQtyPicked,0) ELSE ISNULL(OpportunityItems.numUnitHour,0) END) - ISNULL((SELECT 
																																											SUM(OpportunityBizDocItems.numUnitHour)
																																										FROM
																																											OpportunityBizDocs
																																										INNER JOIN
																																											OpportunityBizDocItems
																																										ON
																																											OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																																										WHERE
																																											OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
																																											AND OpportunityBizDocs.numBizDocId=@numDefaultSalesShippingDoc
					 																																					AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0)
				WHEN @numViewID = 3
				THEN (CASE 
						WHEN ISNULL(@tintInvoicingType,0)=1 
						THEN ISNULL(OpportunityItems.numQtyShipped,0) 
						ELSE ISNULL(OpportunityItems.numUnitHour,0) 
					END) - ISNULL((SELECT 
										SUM(OpportunityBizDocItems.numUnitHour)
									FROM
										OpportunityBizDocs
									INNER JOIN
										OpportunityBizDocItems
									ON
										OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
									WHERE
										OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
										AND OpportunityBizDocs.numBizDocId=287
										AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0)
				ELSE 0
			END) numRemainingQty
			,dbo.FormatedDateFromDate(OpportunityMaster.dtExpectedDate,@numDomainID)
			,OpportunityMaster.dtExpectedDate
			,ISNULL(OpportunityItems.numQtyPicked,0)
			,ISNULL(TempInvoiced.numInvoicedQty,0)
			,'''' vcPaymentStatus
			,ISNULL(CAST((SELECT SUM(monTotAmount) FROM OpportunityItems OIInner WHERE OIInner.numOppID=OpportunityMaster.numOppId AND OIInner.numItemCode=@numShippingServiceItemID) AS VARCHAR),'''')
		FROM
			#TEMPMSRecords TEMPOrder
		INNER JOIN
			OpportunityMaster
		ON
			TEMPOrder.numOppID = OpportunityMaster.numOppId
		INNER JOIN
			DivisionMaster
		ON
			OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
		INNER JOIN
			CompanyInfo
		ON
			DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
		INNER JOIN 
			OpportunityItems
		ON
			OpportunityMaster.numOppId = OpportunityItems.numOppId
			AND TEMPOrder.numOppItemID = OpportunityItems.numoppitemtCode
		INNER JOIN
			Item
		ON
			OpportunityItems.numItemCode = Item.numItemCode
		LEFT JOIN
			ListDetails  LDOpp
		ON
			LDOpp.numListID = 82
			AND (LDOpp.numDomainID = @numDomainID OR LDOpp.constFlag=1)
			AND ISNULL(OpportunityMaster.intUsedShippingCompany,0) = LDOpp.numListItemID
		LEFT JOIN
			ShippingService AS SSOpp
		ON
			(SSOpp.numDomainID=@numDomainID OR ISNULL(SSOpp.numDomainID,0)=0) 
			AND OpportunityMaster.numShippingService = SSOpp.numShippingServiceID
		LEFT JOIN
			ListDetails  LDDiv
		ON
			LDDiv.numListID = 82
			AND (LDDiv.numDomainID = @numDomainID OR LDDiv.constFlag=1)
			AND ISNULL(DivisionMaster.intShippingCompany,0) = LDDiv.numListItemID
		LEFT JOIN
			ShippingService AS SSDiv
		ON
			(SSDiv.numDomainID=@numDomainID OR ISNULL(SSDiv.numDomainID,0)=0) 
			AND ISNULL(DivisionMaster.numDefaultShippingServiceID,0) = SSDiv.numShippingServiceID
		LEFT JOIN
			WareHouseItems
		ON
			OpportunityItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
		LEFT JOIN
			WarehouseLocation
		ON
			WareHouseItems.numWLocationID = WarehouseLocation.numWLocationID
		OUTER APPLY
		(
			SELECT 
				SUM(OpportunityBizDocItems.numUnitHour) numInvoicedQty
			FROM 
				OpportunityBizDocs
			INNER JOIN
				OpportunityBizDocItems
			ON
				OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
			WHERE 
				OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
				AND OpportunityBizDocs.numBizDocId=@numDefaultSalesShippingDoc
				AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode
		) TempInvoiced
		OUTER APPLY
		(
			SELECT 
				SUM(OpportunityBizDocs.monAmountPaid) monAmountPaid
			FROM 
				OpportunityBizDocs 
			WHERE 
				OpportunityBizDocs.numOppId=OpportunityMaster.numOppId 
				AND OpportunityBizDocs.numBizDocId=287
		) TempPaid
		ORDER BY ',
		(CASE @vcSortColumn
			WHEN 'CompanyInfo.vcCompanyName' THEN 'CompanyInfo.vcCompanyName' 
			WHEN 'Item.charItemType' THEN '(CASE 
												WHEN Item.charItemType=''P''  THEN ''Inventory Item''
												WHEN Item.charItemType=''N'' THEN ''Non Inventory Item'' 
												WHEN Item.charItemType=''S'' THEN ''Service''
												WHEN Item.charItemType=''A'' THEN ''Accessory''
											END)'
			WHEN 'Item.numBarCodeId' THEN 'Item.numBarCodeId'
			WHEN 'Item.numItemClassification' THEN 'dbo.GetListIemName(Item.numItemClassification)'
			WHEN 'Item.vcSKU' THEN 'Item.vcSKU'
			WHEN 'OpportunityItems.vcItemName' THEN 'Item.vcItemName'
			WHEN 'OpportunityMaster.numStatus' THEN 'dbo.GetListIemName(OpportunityMaster.numStatus)'
			WHEN 'OpportunityMaster.vcPoppName' THEN 'OpportunityMaster.vcPoppName'
			WHEN 'OpportunityMaster.numAssignedTo' THEN 'dbo.fn_GetContactName(OpportunityMaster.numAssignedTo)'
			WHEN 'OpportunityMaster.numRecOwner' THEN 'dbo.fn_GetContactName(OpportunityMaster.numRecOwner)'
			WHEN 'WareHouseItems.vcLocation' THEN 'WarehouseLocation.vcLocation'
			WHEN 'OpportunityBizDocs.monAmountPaid' THEN 'TempPaid.monAmountPaid'
			WHEN 'OpportunityItems.numQtyPacked' THEN 'ISNULL((SELECT SUM(numUnitHour) FROM OpportunityBizDocs OBD INNER JOIN OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId=OBDI.numOppBizDocID  WHERE OBD.numOppId=OpportunityMaster.numOppId AND OBD.numBizDocId=29397 AND OBDI.numOppItemID=OpportunityItems.numoppitemtCode),0)'
			WHEN 'OpportunityItems.numQtyPicked' THEN 'OpportunityItems.numQtyPicked'
			WHEN 'OpportunityItems.numQtyShipped' THEN 'OpportunityItems.numQtyShipped'
			WHEN 'OpportunityItems.numRemainingQty' THEN '(CASE 
																WHEN @numViewID = 1
																THEN ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyPicked,0)
																WHEN @numViewID = 2
																THEN ISNULL(OpportunityItems.numUnitHour,0) - ISNULL((SELECT SUM(numUnitHour) FROM OpportunityBizDocs OBD INNER JOIN OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId=OBDI.numOppBizDocID  WHERE OBD.numOppId=OpportunityMaster.numOppId AND OBD.numBizDocId=29397 AND OBDI.numOppItemID=OpportunityItems.numoppitemtCode),0)
																ELSE 0
															END)'
			WHEN 'OpportunityItems.numUnitHour' THEN 'OpportunityItems.numUnitHour'
			WHEN 'OpportunityItems.vcInvoiced' THEN 'ISNULL((SELECT SUM(numUnitHour) FROM OpportunityBizDocs OBD INNER JOIN OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId=OBDI.numOppBizDocID  WHERE OBD.numOppId=OpportunityMaster.numOppId AND OBD.numBizDocId=287 AND OBDI.numOppItemID=OpportunityItems.numoppitemtCode),0)'
			WHEN 'OpportunityMaster.dtExpectedDate' THEN 'OpportunityMaster.dtExpectedDate'
			WHEN 'OpportunityItems.vcItemReleaseDate' THEN 'OpportunityItems.ItemReleaseDate'
			WHEN 'OpportunityMaster.bintCreatedDate' THEN 'OpportunityMaster.bintCreatedDate'
			WHEN 'OpportunityMaster.numAge' THEN 'OpportunityMaster.bintCreatedDate'
			WHEN 'OpportunityMaster.bitPaidInFull' THEN '(CASE WHEN ISNULL(TempPaid.monAmountPaid,0) >= ISNULL(OpportunityMaster.monDealAmount,0) THEN 1 ELSE 0 END)'
			WHEN 'OpportunityMaster.dtItemReceivedDate' THEN '(SELECT TOP 1 OMInner.dtItemReceivedDate FROM OpportunityMaster OMInner INNER JOIN OpportunityItems OIInner ON OMInner.numOppID=OIInner.numOppID WHERE OMInner.tintOppType=2 AND OMInner.tintOppStatus=1 AND OIInner.numItemCode=OpportunityItems.numItemCode ORDER BY OMInner.dtItemReceivedDate)'
			ELSE 'OpportunityMaster.bintCreatedDate'
		END),' ',(CASE WHEN ISNULL(@vcSortOrder,'') <> '' THEN @vcSortOrder ELSE 'ASC' END),' OFFSET (@numPageIndex-1) * 200 ROWS FETCH NEXT 200 ROWS ONLY')

		EXEC sp_executesql @vcSQLFinal, N'@numDomainID NUMERIC(18,0), @vcOrderStauts VARCHAR(MAX), @vcFilterValue VARCHAR(MAX), @tintFilterBy TINYINT, @numViewID NUMERIC(18,0), @tintPackingViewMode TINYINT, @numShippingServiceItemID NUMERIC(18,0), @numWarehouseID NUMERIC(18,0), @numShippingZone NUMERIC(18,0), @numDefaultSalesShippingDoc NUMERIC(18,0), @tintInvoicingType TINYINT, @ClientTimeZoneOffset INT, @tintPackingMode TINYINT, @numPageIndex INT', @numDomainID, @vcOrderStauts, @vcFilterValue, @tintFilterBy, @numViewID, @tintPackingViewMode, @numShippingServiceItemID, @numWarehouseID, @numShippingZone, @numDefaultSalesShippingDoc, @tintInvoicingType, @ClientTimeZoneOffset, @tintPackingMode, @numPageIndex;
	END

	PRINT CONCAT('6-',CONVERT( VARCHAR(24), GETDATE(), 121))

	SET @vcSQLFinal = CONCAT('SELECT T1.*',@vcCustomFields,' FROM #TEMPResult T1',@vcCustomWhere)

	PRINT CAST(@vcSQLFinal AS NTEXT)

	EXEC sp_executesql @vcSQLFinal

	PRINT CONCAT('7-',CONVERT( VARCHAR(24), GETDATE(), 121))

	SELECT * FROM @TempFieldsLeft ORDER BY intRowNum

	PRINT CONCAT('8-',CONVERT( VARCHAR(24), GETDATE(), 121))

	IF OBJECT_ID('tempdb..#TEMPMSRecords') IS NOT NULL DROP TABLE #TEMPMSRecords
	IF OBJECT_ID('tempdb..#TEMPResult') IS NOT NULL DROP TABLE #TEMPResult
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportQueryExecute')
DROP PROCEDURE USP_ReportQueryExecute
GO
CREATE PROCEDURE [dbo].[USP_ReportQueryExecute]     
    @numDomainID NUMERIC(18,0),
    @numUserCntID NUMERIC(18,0),
	@numReportID NUMERIC(18,0),
    @ClientTimeZoneOffset INT, 
    @textQuery NTEXT,
	@numCurrentPage NUMERIC(18,0)
AS                 
BEGIN
	DECLARE @tintReportType INT
	SELECT @tintReportType=ISNULL(tintReportType,0) FROM ReportListMaster WHERE numReportID=@numReportID

	DECLARE @avgCost int
	SET @avgCost =(select TOP 1 numCost from Domain where numDOmainId=@numDomainID)

	IF CHARINDEX('AND WareHouseItems.vcLocation',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'AND WareHouseItems.vcLocation','AND WareHouseItems.numWLocationID')
	END

	IF CHARINDEX('ReturnItems.monPrice',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'ReturnItems.monPrice','CAST(ReturnItems.monPrice AS NUMERIC(18,4))')
	END

	IF CHARINDEX('OpportunityItems.monPrice',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'OpportunityItems.monPrice','CAST(OpportunityItems.monPrice AS NUMERIC(18,4))')
	END

	IF CHARINDEX('isnull(category.numCategoryID,0)',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'isnull(category.numCategoryID,0)' ,'(SELECT ISNULL(STUFF((SELECT '','' + CONVERT(VARCHAR(50) , Category.vcCategoryName) FROM ItemCategory LEFT JOIN Category ON ItemCategory.numCategoryID = Category.numCategoryID WHERE ItemCategory.numItemID = Item.numItemCode FOR XML PATH('''')), 1, 1, ''''),''''))')
	END

	IF CHARINDEX('isnull(OpportunityMaster.monProfit,''0'')',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'isnull(OpportunityMaster.monProfit,''0'')' ,CONCAT('CAST(ISNULL(OpportunityItems.monTotAmount,0) - (ISNULL(OpportunityItems.numUnitHour,0) * (CASE ',ISNULL(@avgCost,1),' WHEN 3 THEN ISNULL(OpportunityItems.monVendorCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,@numDomainID,Item.numPurchaseUnit) WHEN 2 THEN ISNULL(OpportunityItems.numCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,@numDomainID,OpportunityItems.numUOMId) ELSE ISNULL(OpportunityItems.monAvgCost,0) END)) AS DECIMAL(20,5))'))
	END

	-- THIS ONE SHOULD BE AFTER ABOVE ONE
	IF CHARINDEX('OpportunityMaster.monProfit',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'OpportunityMaster.monProfit' ,CONCAT('CAST(ISNULL(OpportunityItems.monTotAmount,0) - (ISNULL(OpportunityItems.numUnitHour,0) * (CASE ',ISNULL(@avgCost,1),' WHEN 3 THEN ISNULL(OpportunityItems.monVendorCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,@numDomainID,Item.numPurchaseUnit) WHEN 2 THEN ISNULL(OpportunityItems.numCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,@numDomainID,OpportunityItems.numUOMId) ELSE ISNULL(OpportunityItems.monAvgCost,0) END)) AS DECIMAL(20,5))'))
	END

	IF CHARINDEX('isnull(OpportunityBizDocItems.monProfit,''0'')',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'isnull(OpportunityBizDocItems.monProfit,''0'')' ,CONCAT('CAST(ISNULL(OpportunityBizDocItems.monTotAmount,0) - (ISNULL(OpportunityBizDocItems.numUnitHour,0) * (CASE ',ISNULL(@avgCost,1),' WHEN 3 THEN ISNULL(OpportunityItems.monVendorCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,@numDomainID,Item.numPurchaseUnit) WHEN 2 THEN ISNULL(OpportunityItems.numCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,@numDomainID,OpportunityItems.numUOMId) ELSE ISNULL(OpportunityBizDocItems.monAverageCost,0) END)) AS DECIMAL(20,5))'))
	END

	-- THIS ONE SHOULD BE AFTER ABOVE ONE
	IF CHARINDEX('OpportunityBizDocItems.monProfit',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'OpportunityBizDocItems.monProfit' ,CONCAT('CAST(ISNULL(OpportunityBizDocItems.monTotAmount,0) - (ISNULL(OpportunityBizDocItems.numUnitHour,0) * (CASE ',ISNULL(@avgCost,1),' WHEN 3 THEN ISNULL(OpportunityItems.monVendorCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,@numDomainID,Item.numPurchaseUnit) WHEN 2 THEN ISNULL(OpportunityItems.numCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,@numDomainID,OpportunityItems.numUOMId) ELSE ISNULL(OpportunityBizDocItems.monAverageCost,0) END)) AS DECIMAL(20,5))'))
	END

	IF CHARINDEX('ISNULL(PT.numItemCode,0)=Item.numItemCode',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'ISNULL(PT.numItemCode,0)=Item.numItemCode' ,'Item.numItemCode=PT.numItemCode')
	END

	IF CHARINDEX('isnull(OpportunityMaster.numProfitPercent,0)',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'isnull(OpportunityMaster.numProfitPercent,0)' ,CONCAT('CAST((((ISNULL(OpportunityItems.monTotAmount,0)/ISNULL(NULLIF(OpportunityItems.numUnitHour,0),1)) - (CASE ',ISNULL(@avgCost,1),' WHEN 3 THEN ISNULL(OpportunityItems.monVendorCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,@numDomainID,Item.numPurchaseUnit) WHEN 2 THEN ISNULL(OpportunityItems.numCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,@numDomainID,OpportunityItems.numUOMId) ELSE ISNULL(OpportunityItems.monAvgCost,0) END)) / (CASE WHEN (ISNULL(OpportunityItems.monTotAmount,0)/ISNULL(NULLIF(OpportunityItems.numUnitHour,0),1))=0 then 1 ELSE (ISNULL(OpportunityItems.monTotAmount,0)/ISNULL(NULLIF(OpportunityItems.numUnitHour,0),1)) end) * 100) AS NUMERIC(18,2))'))
	END
	
	-- THIS ONE SHOULD BE AFTER ABOVE ONE
	IF CHARINDEX('OpportunityMaster.numProfitPercent',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'OpportunityMaster.numProfitPercent' ,CONCAT('CAST((((ISNULL(OpportunityItems.monTotAmount,0)/ISNULL(NULLIF(OpportunityItems.numUnitHour,0),1)) - (CASE ',ISNULL(@avgCost,1),' WHEN 3 THEN ISNULL(OpportunityItems.monVendorCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,@numDomainID,Item.numPurchaseUnit) WHEN 2 THEN ISNULL(OpportunityItems.numCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,@numDomainID,OpportunityItems.numUOMId) ELSE ISNULL(OpportunityItems.monAvgCost,0) END)) / (CASE WHEN (ISNULL(OpportunityItems.monTotAmount,0)/ISNULL(NULLIF(OpportunityItems.numUnitHour,0),1))=0 then 1 ELSE (ISNULL(OpportunityItems.monTotAmount,0)/ISNULL(NULLIF(OpportunityItems.numUnitHour,0),1)) end) * 100) AS NUMERIC(18,2))'))
	END

	IF CHARINDEX('isnull(OpportunityBizDocItems.numProfitPercent,0)',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'isnull(OpportunityBizDocItems.numProfitPercent,0)' ,CONCAT('CAST((((ISNULL(OpportunityBizDocItems.monTotAmount,0)/ISNULL(NULLIF(OpportunityBizDocItems.numUnitHour,0),1)) - (CASE ',ISNULL(@avgCost,1),' WHEN 3 THEN ISNULL(OpportunityItems.monVendorCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,@numDomainID,Item.numPurchaseUnit) WHEN 2 THEN ISNULL(OpportunityItems.numCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,@numDomainID,OpportunityItems.numUOMId) ELSE ISNULL(OpportunityBizDocItems.monAverageCost,0) END)) / (CASE WHEN (ISNULL(OpportunityBizDocItems.monTotAmount,0)/ISNULL(NULLIF(OpportunityBizDocItems.numUnitHour,0),1))=0 then 1 ELSE (ISNULL(OpportunityBizDocItems.monTotAmount,0)/ISNULL(NULLIF(OpportunityBizDocItems.numUnitHour,0),1)) end) * 100) AS NUMERIC(18,2))'))
	END
	
	-- THIS ONE SHOULD BE AFTER ABOVE ONE
	IF CHARINDEX('OpportunityBizDocItems.numProfitPercent',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'OpportunityBizDocItems.numProfitPercent' ,CONCAT('CAST((((ISNULL(OpportunityBizDocItems.monTotAmount,0)/ISNULL(NULLIF(OpportunityBizDocItems.numUnitHour,0),1)) - (CASE ',ISNULL(@avgCost,1),' WHEN 3 THEN ISNULL(OpportunityItems.monVendorCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,@numDomainID,Item.numPurchaseUnit) WHEN 2 THEN ISNULL(OpportunityItems.numCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,@numDomainID,OpportunityItems.numUOMId) ELSE ISNULL(OpportunityBizDocItems.monAverageCost,0) END)) / (CASE WHEN (ISNULL(OpportunityBizDocItems.monTotAmount,0)/ISNULL(NULLIF(OpportunityBizDocItems.numUnitHour,0),1))=0 then 1 ELSE (ISNULL(OpportunityBizDocItems.monTotAmount,0)/ISNULL(NULLIF(OpportunityBizDocItems.numUnitHour,0),1)) end) * 100) AS NUMERIC(18,2))'))
	END

	IF ISNULL(@numCurrentPage,0) > 0 
		AND ISNULL(@tintReportType,0) <> 3 
		AND ISNULL(@tintReportType,0) <> 4 
		AND ISNULL(@tintReportType,0) <> 5 
		AND CHARINDEX('Select top',CAST(@textQuery AS VARCHAR(MAX))) <> 1
		AND (SELECT COUNT(*) FROM ReportSummaryGroupList WHERE numReportID=@numReportID) = 0
	BEGIN
		SET @textQuery = CONCAT(CAST(@textQuery AS VARCHAR(MAX)),' OFFSET ',(@numCurrentPage-1) * 100 ,' ROWS FETCH NEXT 100 ROWS ONLY OPTION (OPTIMIZE FOR (@numDomainID UNKNOWN));')
	END
	ELSE
	BEGIN
		SET @textQuery = CONCAT(CAST(@textQuery AS VARCHAR(MAX)),' OPTION (OPTIMIZE FOR (@numDomainID UNKNOWN));')
	END
	 
	 print CAST(@textQuery AS ntext)
	EXECUTE sp_executesql @textQuery,N'@numDomainID numeric(18, 0),@numUserCntID numeric(18,0),@ClientTimeZoneOffset int',@numDomainID=@numDomainID,@numUserCntID=@numUserCntID,@ClientTimeZoneOffset=@ClientTimeZoneOffset
END


