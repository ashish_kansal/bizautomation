/******************************************************************
Project: Release 1.5 Date: 27.04.2013
Comments: 
*******************************************************************/

/*******************Kamal Script******************/

--SELECT * FROM CFw_Grp_Master WHERE Loc_Id=13 AND grp_name='Orders'
UPDATE CFw_Grp_Master SET grp_name='Transactions' WHERE Loc_Id=13 AND grp_name='Orders'

--SELECT * FROM CFw_Grp_Master WHERE Loc_Id=12 AND grp_name='Orders'
UPDATE CFw_Grp_Master SET grp_name='Transactions' WHERE Loc_Id=12 AND grp_name='Orders'


ALTER TABLE BizDocComission
ALTER COLUMN decCommission DECIMAL(18,2)

ALTER TABLE CommissionRuleDtl
ALTER COLUMN intFrom DECIMAL(18,2)

ALTER TABLE CommissionRuleDtl
ALTER COLUMN intTo DECIMAL(18,2)


DECLARE @numPageID NUMERIC 
SELECT @numPageID = MAX(numPageID) + 1 FROM dbo.PageMaster WHERE numModuleID=10
INSERT INTO PageMaster VALUES (@numPageID,10,'frmOpportunities.aspx','Re-open deal',1,0,0,0,0,NULL,NULL)


INSERT INTO dbo.GroupAuthorization
        ( numGroupID ,numModuleID ,numPageID ,intExportAllowed ,intPrintAllowed ,
          intViewAllowed ,intAddAllowed ,intUpdateAllowed ,intDeleteAllowed ,numDomainID
        )

 SELECT   AGM.numGroupID ,PM.numModuleID ,PM.numPageID ,PM.bitIsExportApplicable  AS intExportAllowed ,
                 0 AS intPrintAllowed ,PM.bitIsViewApplicable  AS  intViewAllowed ,PM.bitIsAddApplicable  AS  intAddAllowed ,
                PM.bitIsUpdateApplicable  AS  intUpdateAllowed ,PM.bitIsDeleteApplicable  AS  intDeleteAllowed,AGM.numDomainID
FROM dbo.AuthenticationGroupMaster AGM CROSS JOIN dbo.PageMaster PM 
WHERE numPageID = 30 AND numModuleID=10



/*******************Chintan Script******************/


/******************************************************************
Project: Release 1.5 Date: 16.03.2013
Comments: includes location management, Sales Opp Percentage completed and other bug fixes
*******************************************************************/

BEGIN TRANSACTION

GO
/****** Object:  Table [dbo].[WarehouseLocation]    Script Date: 04/16/2013 13:02:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[WarehouseLocation](
	[numWLocationID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numWarehouseID] [numeric](18, 0) NOT NULL,
	[vcAisle] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[vcRack] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[vcShelf] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[vcBin] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[vcLocation] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[bitDefault] [bit] NOT NULL CONSTRAINT [DF_WarehouseLocation_bitDefault]  DEFAULT ((0)),
	[bitSystem] [bit] NOT NULL CONSTRAINT [DF_WarehouseLocation_bitSystem]  DEFAULT ((0)),
	[intQty] [int] NULL,
	[numDomainID] [numeric](18, 0) NOT NULL,
 CONSTRAINT [PK_WarehouseLocation] PRIMARY KEY CLUSTERED 
(
	[numWLocationID] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING ON

--Migrate existing warehouse location data
INSERT INTO dbo.WarehouseLocation
        ( numWarehouseID ,
          vcAisle ,
          vcRack ,
          vcShelf ,
          vcBin ,
          vcLocation ,
          numDomainID
        )
SELECT  numWareHouseID ,
      LTRIM(RTRIM(ISNULL( NULLIF( ISNULL( vcLocation,''),'0') ,''))) ,
        '',
        '',
        '',
        '',
        numDomainID FROM dbo.WareHouseItems WHERE LEN(RTRIM(LTRIM(ISNULL( NULLIF( ISNULL( vcLocation,''),'0') ,''))) )>0
        
-- Update vcLocation 
UPDATE dbo.WarehouseLocation SET vcLocation = 
RTRIM(LTRIM(ISNULL(vcAisle,''))) + '.'
+ RTRIM(LTRIM(ISNULL(vcRack,''))) + '.'
+ RTRIM(LTRIM(ISNULL(vcShelf,''))) + '.'
+ RTRIM(LTRIM(ISNULL(vcBin,''))) 

--Remove trailing dot ...
UPDATE  dbo.WarehouseLocation SET vcLocation = REVERSE(SUBSTRING(REVERSE(vcLocation), PATINDEX('%[^.]%',REVERSE(vcLocation)), LEN(vcLocation) - PATINDEX('%[^.]%', REVERSE(vcLocation)) + 1))

UPDATE dbo.WarehouseLocation SET bitDefault = 0 
UPDATE dbo.WarehouseLocation SET bitSystem = 0 

--SELECT * FROM dbo.PageMaster 
--
--SELECT * FROM dbo.PageNavigationDTL WHERE vcPageNavName LIKE '%Group%'
--SELECT * FROM dbo.ModuleMaster
--SELECT * FROM dbo.WarehouseLocation
--SELECT * FROM dbo.WarehouseLocationTransactions
--SELECT * FROM dbo.WareHouseItems
--SELECT * FROM dbo.WareHouseItmsDTL

BEGIN TRANSACTION
	DECLARE @numPageNavID numeric 
	SELECT @numPageNavID = MAX(numPageNavID) + 1  FROM dbo.PageNavigationDTL 
	INSERT INTO dbo.PageNavigationDTL
			( numPageNavID ,
			  numModuleID ,
			  numParentID ,
			  vcPageNavName ,
			  vcNavURL ,
			  vcImageURL ,
			  bitVisible ,
			  numTabID
			)
	SELECT TOP 1 @numPageNavID ,37,173,'Warehouse Location','../Items/frmWarehouseLocation.aspx',NULL,1,80 FROM dbo.PageNavigationDTL 
	EXEC USP_CreateTreeNavigationForDomain 0,@numPageNavID --Give permission to tree node.
END TRANSACTION


Begin TRANSACTION

DECLARE @numMaxPageID NUMERIC
SELECT @numMaxPageID =  MAX(numPageID) + 1  FROM dbo.PageMaster
INSERT INTO dbo.PageMaster
        ( numPageID ,
          numModuleID ,
          vcFileName ,
          vcPageDesc ,
          bitIsViewApplicable ,
          bitIsAddApplicable ,
          bitIsUpdateApplicable ,
          bitIsDeleteApplicable ,
          bitIsExportApplicable ,
          vcToolTip ,
          bitDeleted
        )
SELECT @numMaxPageID,37,'frmWarehouseLocation.aspx','Warehouse Locations',1,1,0,1,1,'',0 
INSERT INTO dbo.GroupAuthorization
        ( numGroupID ,
          numModuleID ,
          numPageID ,
          intExportAllowed ,
          intPrintAllowed ,
          intViewAllowed ,
          intAddAllowed ,
          intUpdateAllowed ,
          intDeleteAllowed ,
          numDomainID
        )

 SELECT   
                AGM.numGroupID ,
                PM.numModuleID ,
                PM.numPageID ,
                PM.bitIsExportApplicable  AS intExportAllowed ,
                 0 AS intPrintAllowed ,
                PM.bitIsViewApplicable  AS  intViewAllowed ,
                PM.bitIsAddApplicable  AS  intAddAllowed ,
                PM.bitIsUpdateApplicable  AS  intUpdateAllowed ,
                PM.bitIsDeleteApplicable  AS  intDeleteAllowed,
                AGM.numDomainID
                /*,
				PM.vcPageDesc,
				(SELECT MM.vcModuleName FROM dbo.ModuleMaster MM WHERE numModuleID=PM.numModuleID) AS vcModuleName*/
FROM dbo.AuthenticationGroupMaster AGM CROSS JOIN dbo.PageMaster PM 
WHERE numPageID = @numMaxPageID



SELECT * FROM  GroupAuthorization WHERE numPageID=@numMaxPageID 


-------------------
-- Create default template for  all domain
FrmMaintanance.aspx -> click EXECUTE button 



ROLLBACK TRANSACTION

--------------------------------------------------------------------
/*updated on production on 20-03-2013*/
--Get category whose parent category does not exist
  SELECT numCategoryID,vcCategoryName,numDepCategory FROM dbo.Category WHERE ISNULL(numDepCategory,0) >0  AND ISNULL(numDepCategory,0) NOT IN (SELECT numCategoryID FROM dbo.Category) 
-- update such categories 
  UPDATE dbo.Category SET numDepCategory = NULL WHERE numCategoryID IN (
	SELECT numCategoryID FROM dbo.Category WHERE ISNULL(numDepCategory,0) >0  AND ISNULL(numDepCategory,0) NOT IN (SELECT numCategoryID FROM dbo.Category)   
  )
	
/* Item search result Export- updated on production on 21-03-2013*/	
UPDATE dbo.PageMaster SET bitIsExportApplicable=1 WHERE numModuleID=9 AND numPageID=11


ALTER TABLE dbo.WareHouseItems ADD
	numWLocationID numeric(18, 0) NULL
	
	

SET IDENTITY_INSERT dbo.ListMaster ON 
INSERT INTO dbo.ListMaster
        ( numListID,vcListName ,
          numCreatedBy ,
          bintCreatedDate ,
          numModifiedBy ,
          bintModifiedDate ,
          bitDeleted ,
          bitFixed ,
          numDomainID ,
          bitFlag ,
          vcDataType ,
          numModuleID
        )
VALUES  ( 50,'Percentage Complete' , -- vcListName - varchar(50)
          1 , -- numCreatedBy - numeric
          '2013-04-15 10:13:42' , -- bintCreatedDate - datetime
          NULL , -- numModifiedBy - numeric
          '2013-04-15 10:13:42' , -- bintModifiedDate - datetime
          0, -- bitDeleted - bit
          1 , -- bitFixed - bit
          1 , -- numDomainID - numeric
          1 , -- bitFlag - bit
          'numeric' , -- vcDataType - varchar(15)
          2  -- numModuleID - numeric
        )
SET IDENTITY_INSERT  dbo.ListMaster OFF  


INSERT INTO dbo.ListDetails 
        ( numListID ,
          vcData ,
          numCreatedBY ,
          bintCreatedDate ,
          numModifiedBy ,
          bintModifiedDate ,
          bitDelete ,
          numDomainID ,
          constFlag ,
          sintOrder
        )
       SELECT 50,Id,1,GETDATE(),1,GETDATE(),0,1,1,2 FROM dbo.SplitIDs('2, 5,10,15,20,25,30,40,50,60,70,80,85,90,95,100',',')   
	
	
ALTER TABLE dbo.OpportunityMaster
ADD numPercentageComplete numeric(9)

SET IDENTITY_INSERT dbo.DycFieldMaster ON 
INSERT INTO dbo.DycFieldMaster
        ( numFieldID,numModuleID ,
          numDomainID ,
          vcFieldName ,
          vcDbColumnName ,
          vcOrigDbColumnName ,
          vcPropertyName ,
          vcLookBackTableName ,
          vcFieldDataType ,
          vcFieldType ,
          vcAssociatedControlType ,
          vcToolTip ,
          vcListItemType ,
          numListID ,
          PopupFunctionName ,
          [order] ,
          tintRow ,
          tintColumn ,
          bitInResults ,
          bitDeleted ,
          bitAllowEdit ,
          bitDefault ,
          bitSettingField ,
          bitAddField ,
          bitDetailField ,
          bitAllowSorting ,
          bitWorkFlowField ,
          bitImport ,
          bitExport ,
          bitAllowFiltering ,
          bitInlineEdit ,
          bitRequired ,
          intColumnWidth ,
          intFieldMaxLength
        )
VALUES  ( 465,3 , -- numModuleID - numeric
          NULL , -- numDomainID - numeric
          N'Percentage Complete' , -- vcFieldName - nvarchar(50)
          N'numPercentageComplete' , -- vcDbColumnName - nvarchar(50)
          N'numPercentageComplete' , -- vcOrigDbColumnName - nvarchar(50)
          'PercentageComplete' , -- vcPropertyName - varchar(100)
          N'OpportunityMaster' , -- vcLookBackTableName - nvarchar(50)
          'N' , -- vcFieldDataType - char(1)
          'N' , -- vcFieldType - char(1)
          N'SelectBox' , -- vcAssociatedControlType - nvarchar(50)
          Null , -- vcToolTip - nvarchar(1000)
          'LI' , -- vcListItemType - varchar(3)
          50, -- numListID - numeric
          NULL , -- PopupFunctionName - varchar(100)
          7 , -- order - tinyint
          15 , -- tintRow - tinyint
          1 , -- tintColumn - tinyint
          1 , -- bitInResults - bit
          0 , -- bitDeleted - bit
          1 , -- bitAllowEdit - bit
          0, -- bitDefault - bit
          1, -- bitSettingField - bit
          0, -- bitAddField - bit
          1 , -- bitDetailField - bit
          1 , -- bitAllowSorting - bit
          1 , -- bitWorkFlowField - bit
          null , -- bitImport - bit
          NULL , -- bitExport - bit
          1 , -- bitAllowFiltering - bit
          NULL , -- bitInlineEdit - bit
          NULL , -- bitRequired - bit
          NULL , -- intColumnWidth - int
          NULL  -- intFieldMaxLength - int
        )
SET IDENTITY_INSERT  dbo.DycFieldMaster OFF 

INSERT INTO dbo.DycFormField_Mapping
        ( numModuleID ,
          numFieldID ,
          numDomainID ,
          numFormID ,
          bitAllowEdit ,
          bitInlineEdit ,
          vcFieldName ,
          vcAssociatedControlType ,
          vcPropertyName ,
          PopupFunctionName ,
          [order] ,
          tintRow ,
          tintColumn ,
          bitInResults ,
          bitDeleted ,
          bitDefault ,
          bitSettingField ,
          bitAddField ,
          bitDetailField ,
          bitAllowSorting ,
          bitWorkFlowField ,
          bitImport ,
          bitExport ,
          bitAllowFiltering ,
          bitRequired ,
          numFormFieldID ,
          intSectionID ,
          bitAllowGridColor
        )
VALUES  ( 3 , -- numModuleID - numeric
          465 , -- numFieldID - numeric
          NULL , -- numDomainID - numeric
          38 , -- numFormID - numeric
          1 , -- bitAllowEdit - bit
          1 , -- bitInlineEdit - bit
          N'Percentage Complete' , -- vcFieldName - nvarchar(50)
          N'SelectBox' , -- vcAssociatedControlType - nvarchar(50)
          'PercentageComplete' , -- vcPropertyName - varchar(100)
          null , -- PopupFunctionName - varchar(100)
          0 , -- order - tinyint
         NULL , -- tintRow - tinyint
          NULL , -- tintColumn - tinyint
          1 , -- bitInResults - bit
          0, -- bitDeleted - bit
          0 , -- bitDefault - bit
          1 , -- bitSettingField - bit
          0 , -- bitAddField - bit
          1, -- bitDetailField - bit
          1, -- bitAllowSorting - bit
          1, -- bitWorkFlowField - bit
          NULL , -- bitImport - bit
          NULL , -- bitExport - bit
          1 , -- bitAllowFiltering - bit
          NULL , -- bitRequired - bit
          NULL , -- numFormFieldID - numeric
          NULL , -- intSectionID - int
          1  -- bitAllowGridColor - bit
        )
        
        


ROLLBACK TRANSACTION


/*******************Manish Script******************/

/******************************************************************
Project: BACRMUI   Date: 19.Apr.2013
Comments: To Remove footer shipping amount total from bizdocs
*******************************************************************/
BEGIN TRANSACTION
UPDATE dbo.DycFieldMaster SET bitDeleted = 0,bitInResults = 1 WHERE numFieldID IN (SELECT numFieldId FROM dbo.DycFieldMaster WHERE vcFieldName ='shipping amount' AND vcDbColumnName = 'ShippingAmount')
UPDATE dbo.DycFormField_Mapping SET bitDeleted = 0,bitInResults = 1 WHERE numFieldID IN (SELECT numFieldId FROM dbo.DycFieldMaster WHERE vcFieldName ='shipping amount' AND vcDbColumnName = 'ShippingAmount')
ROLLBACK
/******************************************************************
Project: BACRMUI   Date: 17.Apr.2013
Comments: Add Time & Expense Module
*******************************************************************/
BEGIN TRANSACTION
SET IDENTITY_INSERT dbo.ModuleMaster ON
INSERT INTO dbo.ModuleMaster (numModuleID,vcModuleName,tintGroupType) VALUES ( 40,'Time & Expense',1 ) 
SET IDENTITY_INSERT dbo.ModuleMaster OFF	
--exec usp_GetAllSystemModules @tintGroupType=1

INSERT INTO dbo.PageMaster (numPageID,numModuleID,vcFileName,vcPageDesc,bitIsViewApplicable,bitIsAddApplicable,bitIsUpdateApplicable,bitIsDeleteApplicable,
							bitIsExportApplicable,vcToolTip,bitDeleted) 
VALUES ( 
	/* numPageID - numeric(18, 0) */ 117,
	/* numModuleID - numeric(18, 0) */ 40,
	/* vcFileName - varchar(50) */ 'frmEmpCal.aspx',
	/* vcPageDesc - varchar(100) */ 'Time & Expense',
	/* bitIsViewApplicable - int */ 1,
	/* bitIsAddApplicable - int */ 1,
	/* bitIsUpdateApplicable - int */ 0,
	/* bitIsDeleteApplicable - int */ 1,
	/* bitIsExportApplicable - int */ 0,
	/* vcToolTip - nvarchar(1000) */ N'Time & Expense',
	/* bitDeleted - bit */ 0 ) 
ROLLBACK

/******************************************************************
Project: BACRMUI   Date: 15.Apr.2013
Comments: Solved issues arose while using terms
*******************************************************************/
BEGIN TRANSACTION

SELECT * FROM dbo.BillingTerms WHERE (LTRIM(vcTerms) <> vcTerms OR RTRIM(vcTerms) <> vcTerms)

UPDATE dbo.BillingTerms SET vcTerms = LTRIM(vcTerms) WHERE LTRIM(vcTerms) <> vcTerms
UPDATE dbo.BillingTerms SET vcTerms = RTRIM(vcTerms) WHERE RTRIM(vcTerms) <> vcTerms

SELECT * FROM dbo.BillingTerms WHERE (LTRIM(vcTerms) <> vcTerms OR RTRIM(vcTerms) <> vcTerms)

ROLLBACK

/******************************************************************
Project: BACRMUI   Date: 02.Apr.2013
Comments: Update Warehouse Location as per new change
*******************************************************************/
BEGIN TRANSACTION

UPDATE DycFieldMaster SET vcAssociatedControlType = 'SelectBox',vcPropertyName = 'WarehouseLocationID' WHERE vcFieldName = 'Warehouse Location' AND vcLookBackTableName = 'WareHouseItems'
--SELECT * FROM dbo.DycFieldMaster WHERE vcFieldName LIKE '%Warehouse Location%'

UPDATE DycFormField_Mapping SET vcAssociatedControlType = 'SelectBox',vcFieldName = 'Location',vcPropertyName = 'WarehouseLocationID' WHERE vcFieldName LIKE '%Location%' AND numFormID IN (20,48)

--SELECT * FROM dbo.DycFormField_Mapping WHERE vcFieldName LIKE '%Location%' AND numFormID IN (20,48)

ROLLBACK
/******************************************************************
Project: BACRMUI   Date: 28.Mar.2013
Comments: Add bitIsImage into ItemImages
*******************************************************************/
BEGIN TRANSACTION

INSERT INTO dbo.DycFieldMaster 
(numModuleID,numDomainID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcToolTip,
vcListItemType,numListID,PopupFunctionName,[order],tintRow,tintColumn,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,
bitAllowSorting,bitWorkFlowField,bitImport,bitExport,bitAllowFiltering,bitInlineEdit,bitRequired,intColumnWidth,intFieldMaxLength) 
SELECT 4,NULL,'Is Image','bitIsImage','bitIsImage','IsImage','ItemImages','Y','R','CheckBox','Is Image','',0,NULL,	7,	0,	1,	1,	0,	1,	0,	1,	1,	1,	1,	1,	1,	NULL,	1,	NULL,	NULL,	NULL,	NULL

DECLARE @numMaxField AS NUMERIC(10,0)
SELECT @numMaxField = numFieldID FROM dbo.DycFieldMaster WHERE vcFieldName = 'Is Image' AND vcDbColumnName = 'bitIsImage' AND vcLookBackTableName = 'ItemImages'
PRINT @numMaxField

INSERT INTO dbo.DycFormField_Mapping
(numModuleID,numFieldID,numDomainID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,[order],tintRow,
tintColumn,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,bitAllowFiltering,
bitRequired,numFormFieldID,intSectionID,bitAllowGridColor) 
SELECT 4,@numMaxField,	NULL,	54,	0,	0,	'Is Image',	'Checkbox',	'IsImage',	NULL,	7,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	NULL,	1,	NULL

SELECT * FROM dbo.DycFormField_Mapping 
WHERE numFormID = 54 

EXEC dbo.USP_GET_DYNAMIC_IMPORT_FIELDS
	@numFormID = 54, --  bigint
	@intMode = 2, --  int
	@numImportFileID = 0, --  numeric(18, 0)
	@numDomainID = 1 --  numeric(18, 0)

ROLLBACK

/******************************************************************
Project: BACRMUI   Date: 25.Mar.2013
Comments: Add Customer Statement Email Template
*******************************************************************/
BEGIN TRANSACTION
INSERT INTO [GenericDocuments]
           ([VcFileName]
           ,[vcDocName]
           ,[numDocCategory]
           ,[cUrlType]
           ,[vcFileType]
           ,[numDocStatus]
           ,[vcDocDesc]
           ,[numDomainID]
           ,[numCreatedBy]
           ,[bintCreatedDate]
           ,[numModifiedBy]
           ,[bintModifiedDate]
           ,[vcSubject]
           ,[vcDocumentSection]
           ,[numRecID]
           ,[tintCheckOutStatus]
           ,[intDocumentVersion]
           ,[numLastCheckedOutBy]
           ,[dtCheckOutDate]
           ,[dtCheckInDate]
           ,[tintDocumentType]
           ,[numOldSpecificDocID]
           ,[numModuleID])
     SELECT '#SYS#CUSTOMER_STATEMENT'
           ,'Customer Statement'
           ,369
           ,''
           ,''
           ,0
           ,'<div style="color: #222222; font-family: arial, sans-serif; font-size: 12.800000190734863px; background-color: #ffffff;"><span style="color: #0000ff;">Dear ##ContactFirstName##&nbsp;##ContactLastName##, &nbsp; &nbsp;&nbsp;</span></div>
<div style="color: #222222; font-family: arial, sans-serif; font-size: 12.800000190734863px; background-color: #ffffff;"><span style="color: #0000ff;"><br />
</span></div>
<div style="color: #222222; font-family: arial, sans-serif; font-size: 12.800000190734863px; background-color: #ffffff;"><span style="color: #0000ff;">We have attached with this email a list of all your transactions with us. You can write to us or call us if you need any assistance or clarifications. &nbsp; &nbsp;&nbsp;</span></div>
<div style="color: #222222; font-family: arial, sans-serif; font-size: 12.800000190734863px; background-color: #ffffff;"><span style="color: #0000ff;"><br />
</span></div>
<div style="color: #222222; font-family: arial, sans-serif; font-size: 12.800000190734863px; background-color: #ffffff;"><span style="color: #0000ff;">Thanks for your business.</span></div>
<div style="color: #222222; font-family: arial, sans-serif; font-size: 13.333333969116211px; background-color: #ffffff;"><br />
</div>
##Signature##'
           ,1
           ,1
           ,'2013-03-25 11:32:38.690'
           ,1
           ,'2013-03-25 11:40:32.090'
           ,'Statement of transactions with ##OrganizationName##'
           ,NULL
           ,0
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,1
           ,NULL
           ,1           

----------------- Insert Template for All domain ------------------

INSERT  INTO dbo.GenericDocuments
        (
          VcFileName,
          vcDocName,
          numDocCategory,
          cUrlType,
          vcFileType,
          numDocStatus,
          vcDocDesc,
          numDomainID,
          numCreatedBy,
          bintCreatedDate,
          numModifiedBy,
          bintModifiedDate,
          vcSubject,
          vcDocumentSection,
          numRecID,
          tintCheckOutStatus,
          intDocumentVersion,
          numLastCheckedOutBy,
          dtCheckOutDate,
          dtCheckInDate,
          tintDocumentType,
          numOldSpecificDocID,
          numModuleID
        )
        SELECT  VcFileName,
                vcDocName,
                numDocCategory,
                cUrlType,
                vcFileType,
                numDocStatus,
                vcDocDesc,
                DOM.numDomainID,
                numCreatedBy,
                bintCreatedDate,
                numModifiedBy,
                bintModifiedDate,
                vcSubject,
                vcDocumentSection,
                numRecID,
                tintCheckOutStatus,
                intDocumentVersion,
                numLastCheckedOutBy,
                dtCheckOutDate,
                dtCheckInDate,
                tintDocumentType,
                numOldSpecificDocID,
                numModuleID
        FROM    dbo.GenericDocuments GD
                CROSS JOIN dbo.Domain DOM
        WHERE   VcFileName = '#SYS#CUSTOMER_STATEMENT'
                AND DOM.numDomainID NOT IN (
                SELECT  numDomainID
                FROM    dbo.GenericDocuments
                WHERE   VcFileName = '#SYS#CUSTOMER_STATEMENT' )

SELECT  *
FROM    dbo.GenericDocuments
WHERE   VcFileName = '#SYS#CUSTOMER_STATEMENT'        
           
ROLLBACK



/******************************************************************
Project: BACRMUI   Date: 20.Mar.2013
Comments: Add Price Level into Item
*******************************************************************/
BEGIN TRANSACTION

INSERT INTO dbo.DycFieldMaster 
(numModuleID,numDomainID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcToolTip,
vcListItemType,numListID,PopupFunctionName,[order],tintRow,tintColumn,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,
bitAllowSorting,bitWorkFlowField,bitImport,bitExport,bitAllowFiltering,bitInlineEdit,bitRequired,intColumnWidth,intFieldMaxLength) 
SELECT 4,NULL,'Price Level','vcPriceLevelDetail','vcPriceLevelDetail','PriceLevelDetail','PricingTable','V','R','TextBox','Price Level','',0,NULL,	62,	0,	1,	1,	0,	1,	0,	1,	1,	1,	1,	1,	1,	NULL,	1,	NULL,	NULL,	NULL,	NULL
UNION ALL
SELECT 4,NULL,'Price Rule Type','tintRuleType','tintRuleType','PriceRuleType','PricingTable','N','R','SelectBox','Price Rule Type','',0,NULL,	62,	0,	1,	1,	0,	1,	0,	1,	1,	1,	1,	1,	1,	NULL,	1,	NULL,	NULL,	NULL,	NULL
UNION ALL
SELECT 4,NULL,'Price Level Discount Type','tintDiscountType','tintDiscountType','PriceDiscountType','PricingTable','N','R','SelectBox','Price Level Discount Type','',0,NULL,	62,	0,	1,	1,	0,	1,	0,	1,	1,	1,	1,	1,	1,	NULL,	1,	NULL,	NULL,	NULL,	NULL

DECLARE @numField1 AS NUMERIC(10,0)
DECLARE @numField2 AS NUMERIC(10,0)
DECLARE @numField3 AS NUMERIC(10,0)
SELECT @numField1 = numFieldID FROM dbo.DycFieldMaster 
							   WHERE vcFieldName = 'Price Level' 
							   AND vcDbColumnName = 'vcPriceLevelDetail' 
							   AND vcLookBackTableName = 'PricingTable'
SELECT @numField2 = numFieldID FROM dbo.DycFieldMaster 
							   WHERE vcFieldName = 'Price Rule Type' 
							   AND vcDbColumnName = 'tintRuleType' 
							   AND vcLookBackTableName = 'PricingTable'
SELECT @numField3 = numFieldID FROM dbo.DycFieldMaster 
							   WHERE vcFieldName = 'Price Level Discount Type' 
							   AND vcDbColumnName = 'tintDiscountType' 
							   AND vcLookBackTableName = 'PricingTable'
PRINT @numField1
PRINT @numField2
PRINT @numField3

INSERT INTO dbo.DycFormField_Mapping
(numModuleID,numFieldID,numDomainID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,[order],tintRow,
tintColumn,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,bitAllowFiltering,
bitRequired,numFormFieldID,intSectionID,bitAllowGridColor) 
SELECT 4,@numField1,	NULL,	20,	0,	0,	'Price Level',	'TextBox',	'PriceLevelDetail',	NULL,	62,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	NULL,	1,	NULL
UNION ALL
SELECT 4,@numField2,	NULL,	20,	0,	0,	'Price Rule Type',	'SelectBox',	'PriceRuleType',	NULL,	62,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	NULL,	1,	NULL
UNION ALL
SELECT 4,@numField3,	NULL,	20,	0,	0,	'Price Level Discount Type',	'SelectBox',	'PriceDiscountType',	NULL,	62,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	NULL,	1,	NULL


SELECT * FROM dbo.DycFormField_Mapping 
WHERE numFormID = 20 AND vcFieldName IN ('Price Level','Price Rule Type','Price Level Discount Type')

SELECT * FROM dbo.DycFieldMaster
WHERE vcFieldName IN ('Price Level','Price Rule Type','Price Level Discount Type')


EXEC dbo.USP_GET_DYNAMIC_IMPORT_FIELDS
	@numFormID = 20, --  bigint
	@intMode = 2, --  int
	@numImportFileID = 0, --  numeric(18, 0)
	@numDomainID = 1 --  numeric(18, 0)

ROLLBACK



/*******************Joseph Script******************/


/***********************************************************************************************
Project: Marketplace Order Id in Simple and Advanced Search  Date: 04/April/2013
Comments: 
************************************************************************************************/

BEGIN TRANSACTION
GO
INSERT INTO dbo.DycFormField_Mapping (
 numModuleID,numFieldID,numFormID,vcFieldName,vcAssociatedControlType,
 bitAllowFiltering,
 intSectionID
 
) 
SELECT numModuleID,numFieldID,6,vcFieldName,vcAssociatedControlType,
 bitAllowFiltering,1 FROM DycFieldMaster WHERE vcDbColumnName='vcMarketplaceOrderID'


INSERT INTO dbo.DycFormField_Mapping (
 numModuleID,numFieldID,numFormID,vcFieldName,vcAssociatedControlType,
 bitAllowFiltering,
 intSectionID
 
) 
SELECT numModuleID,numFieldID,15,vcFieldName,vcAssociatedControlType,
 bitAllowFiltering,1 FROM DycFieldMaster WHERE vcDbColumnName='vcMarketplaceOrderID'
GO
ROLLBACK TRANSACTION


/******************************************************************
Project:GL Performance tunning  Date: 04/April/2013
Comments: 
*******************************************************************/

BEGIN TRANSACTION
GO

ALTER TABLE General_Journal_Header ADD
numEntryDateSortOrder NUMERIC NULL

UPDATE dbo.General_Journal_Header SET dbo.General_Journal_Header.numEntryDateSortOrder = CONVERT(numeric,
RIGHT('00000'+ CONVERT(VARCHAR,DATEPART(year, dbo.General_Journal_Header.datEntry_Date)),4)+
RIGHT('00000'+ CONVERT(VARCHAR,DATEPART(month, dbo.General_Journal_Header.datEntry_Date)),2)+
RIGHT('00000'+ CONVERT(VARCHAR,DATEPART(day, dbo.General_Journal_Header.datEntry_Date)),2)+
RIGHT('00000'+ CONVERT(VARCHAR,DATEPART(hour, dbo.General_Journal_Header.datEntry_Date)),2)+
RIGHT('00000'+ CONVERT(VARCHAR,DATEPART(minute, dbo.General_Journal_Header.datEntry_Date)),2)+
RIGHT('00000'+ CONVERT(VARCHAR,DATEPART(second, dbo.General_Journal_Header.datEntry_Date)),2)+
RIGHT('00000'+ CONVERT(VARCHAR,DATEPART(millisecond, dbo.General_Journal_Header.datEntry_Date)),3))
-- where dbo.General_Journal_Header.numDomainId = 163
GO

----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
DECLARE @RowsToProcess int
DECLARE @CurrentRow int
 
DECLARE @DuplicateVersions TABLE
(
    row_id int NOT NULL PRIMARY KEY IDENTITY(1,1),
    duplicate_count int,
    numEntryDateSortOrder NUMERIC
)
 
INSERT INTO @DuplicateVersions 
            (   
                duplicate_count,
                numEntryDateSortOrder
            )
         SELECT COUNT(*) AS duplicate_count,
               GJH.numEntryDateSortOrder
           FROM General_Journal_Header GJH
           --where GJH.numDomainId = 163
       GROUP BY GJH.numEntryDateSortOrder
         HAVING COUNT(*) > 1
       ORDER BY duplicate_count DESC

SELECT @RowsToProcess = COUNT(*) FROM @DuplicateVersions
SET @CurrentRow = 1
 
WHILE @CurrentRow <= @RowsToProcess
BEGIN   

Select GJH1.numJournal_ID,GJH1.numEntryDateSortOrder,IDENTITY( int,1,1 ) as NewSort INTO #temp123 from General_Journal_Header GJH1
   INNER JOIN @DuplicateVersions AS DuplicateVersions 
ON GJH1.numEntryDateSortOrder = DuplicateVersions.numEntryDateSortOrder 
WHERE DuplicateVersions.row_id = @CurrentRow

UPDATE  GJH2
SET GJH2.numEntryDateSortOrder = (Temp1.numEntryDateSortOrder + Temp1.NewSort)  from General_Journal_Header GJH2
   INNER JOIN #temp123 AS Temp1  
ON GJH2.numJournal_ID = Temp1.numJournal_ID

DROP TABLE #temp123

SET @CurrentRow = @CurrentRow + 1      
END      

----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


CREATE NONCLUSTERED INDEX [NC_OpportunityBizDocs_numOppId] ON [dbo].[OpportunityBizDocs] 
(
 [numOppId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]


----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

ROLLBACK TRANSACTION


/******************************************************************
Project: Allow Filtering for Marketplace Order ID field  Date: 04/April/2013
Comments: 
*******************************************************************/

BEGIN TRANSACTION
GO

update DycFieldMaster set bitAllowFiltering = 1,vcAssociatedControlType = 'TextBox' where vcFieldName = 'Marketplace Order ID'

update DycFormField_Mapping set bitAllowFiltering = 1,vcAssociatedControlType = 'TextBox' where vcFieldName = 'Marketplace Order ID'

GO
ROLLBACK TRANSACTION
