
/******************************************************************
Project: Release 3.9 Date: 29.NOV.2014
Comments: STORED PROCEDURES
*******************************************************************/


GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_CalOppItemTotalTaxAmt')
DROP FUNCTION fn_CalOppItemTotalTaxAmt
GO
CREATE FUNCTION [dbo].[fn_CalOppItemTotalTaxAmt](
@numDomainID as numeric(9),    
@numTaxItemID as numeric(9),    
@numOppID as numeric(9), 
@numOppBizDocID as numeric(9)    
) returns money

as
begin

declare @numOppItemID as numeric(9) 
DECLARE @numOppBizDocItemID AS NUMERIC(9) 
declare @numItemCode as numeric(9)  
declare @ItemAmount as money    
declare @TotalTaxAmt as money    
set @TotalTaxAmt=0    

DECLARE @tintTaxOperator AS TINYINT;SET @tintTaxOperator=0
SELECT @tintTaxOperator=ISNULL([tintTaxOperator],0) FROM [OpportunityMaster] WHERE numOppID=@numOppID

DECLARE @fltPercentage FLOAT
SET @fltPercentage=0
   
SELECT @fltPercentage=fltPercentage FROM OpportunityMasterTaxItems WHERE numOppId=@numOppID and numTaxItemID=@numTaxItemID    


IF @numOppBizDocID=0
BEGIN
			--- Skip tax applicable checking if tintTaxOperator =1 (Forcibly apply sales tax )
			IF @numTaxItemID = 0 and @tintTaxOperator = 1
			BEGIN
				select @TotalTaxAmt = isnull(SUM(monTotAmount),0)*@fltPercentage/100  from OpportunityItems
				where numOppID=@numOppID  
				  
				RETURN ISNULL(@TotalTaxAmt,0)
			END	
			ELSE IF @numTaxItemID = 0 and @tintTaxOperator = 2
			BEGIN
				RETURN  ISNULL(@TotalTaxAmt,0)
			END
			ELSE if (ISNULL(@fltPercentage,0)>0 AND 
					(SELECT [DivisionMaster].[bitNoTax] FROM [dbo].[DivisionMaster] 
					 WHERE [DivisionMaster].[numDomainID] = @numDomainID 
					  AND  [DivisionMaster].[numDivisionID] = (SELECT numdivisionID FROM [dbo].[OpportunityMaster] 
															   WHERE [OpportunityMaster].[numOppId] = @numOppID) ) = 0)
				begin  
					SET @numOppItemID=0
					
				 select top 1 @numOppItemID=numoppitemtCode,@ItemAmount=monTotAmount from OpportunityItems
				 where numOppID=@numOppID   
				 order by numoppitemtCode 
				   
				    
				 while @numOppItemID>0    
				 begin
				  IF (select COUNT(*) from OpportunityItemsTaxItems where numOppItemId=@numOppItemID and numTaxItemID=@numTaxItemID)>0
				  BEGIN
						set @TotalTaxAmt=@TotalTaxAmt+@fltPercentage *@ItemAmount/100    
				  END    
				    
				 select top 1 @numOppItemID=numoppitemtCode,@ItemAmount=monTotAmount from OpportunityItems 
				 where numOppID=@numOppID  and numoppitemtCode>@numOppItemID   
				 order by numoppitemtCode     
				    
				 if @@rowcount=0 set @numOppItemID=0    
				 end    
			end    
			else    
				set @TotalTaxAmt=0  
END

ELSE --IF BIZDoc
BEGIN
			--- Skip tax applicable checking if tintTaxOperator =1 (Forcibly apply sales tax )
			IF @numTaxItemID = 0 and @tintTaxOperator = 1
			BEGIN
				select @TotalTaxAmt = isnull(SUM(monTotAmount),0)*@fltPercentage/100  from OpportunityBizDocItems
				where numOppBizDocID=@numOppBizDocID    
				  
				RETURN ISNULL(@TotalTaxAmt,0)
			END	
			ELSE IF @numTaxItemID = 0 and @tintTaxOperator = 2
			BEGIN
				RETURN  ISNULL(@TotalTaxAmt,0)
			END
			ELSE if (ISNULL(@fltPercentage,0)>0 AND 
					(SELECT [DivisionMaster].[bitNoTax] FROM [dbo].[DivisionMaster] 
					 WHERE [DivisionMaster].[numDomainID] = @numDomainID 
					  AND  [DivisionMaster].[numDivisionID] = (SELECT numdivisionID FROM [dbo].[OpportunityMaster] 
															   WHERE [OpportunityMaster].[numOppId] = @numOppID) ) = 0)
				begin  
					SET @numOppBizDocItemID=0
					
				 select top 1 @numOppBizDocItemID=numOppBizDocItemID,@numOppItemID=numOppItemID,@numItemCode=numItemCode,@ItemAmount=monTotAmount from OpportunityBizDocItems
				 where numOppBizDocID=@numOppBizDocID    
				 order by numOppBizDocItemID 
				   
				    
				 while @numOppBizDocItemID>0    
				 begin
				  IF (select COUNT(*) from OpportunityItemsTaxItems where numOppItemId=@numOppItemID and numTaxItemID=@numTaxItemID)>0
				  BEGIN
						set @TotalTaxAmt=@TotalTaxAmt+@fltPercentage *@ItemAmount/100    
				  END    
				    
				 select top 1 @numOppBizDocItemID=numOppBizDocItemID,@numOppItemID=numOppItemID,@numItemCode=numItemCode,@ItemAmount=monTotAmount from OpportunityBizDocItems 
				 where numOppBizDocID=@numOppBizDocID  and numOppBizDocItemID>@numOppBizDocItemID   
				 order by numOppBizDocItemID     
				    
				 if @@rowcount=0 set @numOppBizDocItemID=0    
				 end    
			end    
			else    
				set @TotalTaxAmt=0  
END

return @TotalTaxAmt
end


GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_BroadCastDtls_GetAllByBroadcastID' ) 
    DROP PROCEDURE USP_BroadCastDtls_GetAllByBroadcastID
GO

CREATE PROCEDURE USP_BroadCastDtls_GetAllByBroadcastID  
(  
 @numDomainID NUMERIC(18,0),
 @numBroadCastID NUMERIC(18,0)
)  
AS  
BEGIN  
-- SET NOCOUNT ON added to prevent extra result sets from  
-- interfering with SELECT statements.  
SET NOCOUNT ON;  

	SELECT  
		BroadCastDtls.numBroadCastID,
		BroadCastDtls.numBroadCastDtlId,
		ACI.numContactId,
        [dbo].[GetListIemName](ACI.vcDepartment) ContactDepartment,
        [dbo].[GetListIemName](ACI.vcCategory) ContactCategory,
                    
        ACI.vcGivenName,
        ACI.vcFirstName ContactFirstName,
        ACI.vcLastName ContactLastName,
        ACI.numDivisionId,
        dbo.GetListIemName(ACI.numContactType) ContactType,
        [dbo].[GetListIemName](ACI.numTeam) ContactTeam,
        ACI.numPhone ContactPhone,
        ACI.numPhoneExtension ContactPhoneExt,
        ACI.numCell ContactCell,
        ACI.numHomePhone ContactHomePhone,
        ACI.vcFax ContactFax,
        ACI.vcEmail ContactEmail,
        ACI.VcAsstFirstName AssistantFirstName,
        ACI.vcAsstLastName AssistantLastName,
        ACI.numAsstPhone ContactAssistantPhone,
        ACI.numAsstExtn ContactAssistantPhoneExt,
        ACI.vcAsstEmail ContactAssistantEmail,
        CASE WHEN ACI.charSex = 'M' THEN 'Male'
                WHEN ACI.charSex = 'F' THEN 'Female'
                ELSE '-'
        END AS ContactGender,
        ACI.bintDOB,
        dbo.GetAge(ACI.bintDOB, GETUTCDATE()) AS ContactAge,
        [dbo].[GetListIemName](ACI.vcPosition) ContactPosition,
        ACI.txtNotes,
        ACI.numCreatedBy,
        ACI.bintCreatedDate,
        ACI.numModifiedBy,
        ACI.bintModifiedDate,
        ACI.numDomainID,
        ACI.bitOptOut,
        dbo.fn_GetContactName(ACI.numManagerId) ContactManager,
        ACI.numRecOwner,
        [dbo].[GetListIemName](ACI.numEmpStatus) ContactStatus,
        ACI.vcTitle ContactTitle,
        ACI.vcAltEmail,
        ACI.vcItemId,
        ACI.vcChangeKey,
        ISNULL(( SELECT vcECampName
                    FROM   [ECampaign]
                    WHERE  numECampaignID = ACI.numECampaignID
                ), '') ContactDripCampaign,
        dbo.getContactAddress(ACI.numContactID) AS ContactPrimaryAddress,
        C.numCompanyId,
        C.vcCompanyName OrganizationName,
        C.numCompanyType,
        C.numCompanyRating,
        C.numCompanyIndustry,
        C.numCompanyCredit,
        C.txtComments,
        C.vcWebSite,
        C.vcWebLabel1,
        C.vcWebLink1,
        C.vcWebLabel2,
        C.vcWebLink2,
        C.vcWebLabel3,
        C.vcWebLink3,
        C.vcWeblabel4,
        C.vcWebLink4,
        C.numAnnualRevID,
        dbo.GetListIemName(C.numNoOfEmployeesId) OrgNoOfEmployee,
        C.vcHow,
        C.vcProfile,
        C.numCreatedBy,
        C.bintCreatedDate,
        C.numModifiedBy,
        C.bintModifiedDate,
        C.bitPublicFlag,
        C.numDomainID,
                    
        DM.numDivisionID,
        DM.numCompanyID,
        DM.vcDivisionName,
        DM.numGrpId,
        DM.numFollowUpStatus,
        DM.bitPublicFlag,
        DM.numCreatedBy,
        DM.bintCreatedDate,
        DM.numModifiedBy,
        DM.bintModifiedDate,
        DM.tintCRMType,
        DM.numDomainID,
        DM.bitLeadBoxFlg,
        DM.numTerID,
        DM.numStatusID,
        DM.bintLeadProm,
        DM.bintLeadPromBy,
        DM.bintProsProm,
        DM.bintProsPromBy,
        DM.numRecOwner,
        DM.decTaxPercentage,
        DM.tintBillingTerms,
        DM.numBillingDays,
        DM.tintInterestType,
        DM.fltInterest,
        DM.vcComPhone,
        DM.vcComFax,
        DM.numCampaignID,
        DM.numAssignedBy,
        DM.numAssignedTo,
        DM.bitNoTax,
        vcCompanyName + ' ,<br>' + ISNULL(AD2.vcStreet,'') + ' <br>'
        + ISNULL(AD2.VcCity,'') + ' ,' + ISNULL(dbo.fn_GetState(AD2.numState),
                                        '') + ' ' + ISNULL(AD2.vcPostalCode,'')
        + ' <br>' + ISNULL(dbo.fn_GetListItemName(AD2.numCountry),
                            '') AS OrgShippingAddress ,								   
		(select U.txtSignature from UserMaster U where U.numUserDetailId=ACI.numRecOwner) as [Signature]
    FROM    
		BroadCastDtls
	INNER JOIN 
		dbo.AdditionalContactsInformation ACI 
	ON
		ACI.numContactId = BroadCastDtls.numContactID
	INNER JOIN
		dbo.DivisionMaster DM 
	ON
		ACI.numDivisionId = DM.numDivisionID
	INNER JOIN
		dbo.CompanyInfo C
	ON 
		DM.numCompanyId = C.numCompanyId
    LEFT JOIN 
		dbo.AddressDetails AD2 
	ON 
		AD2.numDomainID=DM.numDomainID 
		AND AD2.numRecordID= DM.numDivisionID 
		AND AD2.tintAddressOf=2 
		AND AD2.tintAddressType=2 
		AND AD2.bitIsPrimary=1
    WHERE   
		DM.numDomainId = @numDomainID AND
		BroadCastDtls.numBroadCastID = @numBroadCastID AND
		ACI.bitOptOut = 0

END  


GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_BroadCastDtls_UpdateSentStatus' ) 
    DROP PROCEDURE USP_BroadCastDtls_UpdateSentStatus
GO

CREATE PROCEDURE USP_BroadCastDtls_UpdateSentStatus  
(  
 @numBroadCastDtlID NUMERIC(18,0)
)  
AS  
BEGIN  
-- SET NOCOUNT ON added to prevent extra result sets from  
-- interfering with SELECT statements.  
SET NOCOUNT ON;  

UPDATE BroadCastDtls SET tintSucessfull = 1 WHERE numBroadCastDtlId = @numBroadCastDtlID
UPDATE Broadcast SET numTotalSussessfull = numTotalSussessfull + 1, bitBroadcasted=1 WHERE numBroadCastId = ISNULL((SELECT numBroadCastID FROM BroadCastDtls WHERE numBroadCastDtlId = @numBroadCastDtlID),0)

END  


GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_BroadcastErrorLog_Insert' ) 
    DROP PROCEDURE USP_BroadcastErrorLog_Insert
GO

CREATE PROCEDURE USP_BroadcastErrorLog_Insert  
(  
 @numBroadCastDtlID NUMERIC(18,0),  
 @vcType NVARCHAR(100),  
 @vcMessage NVARCHAR(MAX),
 @vcStackStrace NVARCHAR(MAX)
)  
AS  
BEGIN  
-- SET NOCOUNT ON added to prevent extra result sets from  
-- interfering with SELECT statements.  
SET NOCOUNT ON;  

INSERT INTO BroadcastErrorLog
(
	numBroadcastDtlID,
	vcType,
	vcMessage,
	vcStackStrace
)
VALUES
(
	@numBroadCastDtlID,
	@vcType,
	@vcMessage,
	@vcStackStrace
) 

END  

--modified by anoop jayaraj              
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_deleteoppurtunity')
DROP PROCEDURE usp_deleteoppurtunity
GO
CREATE PROCEDURE [dbo].[USP_DeleteOppurtunity]
 @numOppId numeric(9) ,        
 @numDomainID as numeric(9),
 @numUserCntID AS NUMERIC(9)                       
AS                          
             
       
if exists(select * from OpportunityMaster where numOppID=@numOppId and numDomainID=@numDomainID)         
BEGIN 

SET XACT_ABORT ON;

BEGIN TRY 
BEGIN TRAN

	IF (SELECT COUNT(*) FROM RecurrenceConfiguration WHERE numOppID = @numOppId AND numDomainID=@numDomainID AND ISNULL(bitMarkForDelete,0) = 0) > 0
	BEGIN
		RAISERROR ( 'RECURRING ORDER OR BIZDOC', 16, 1 )
		RETURN
	END
	ELSE IF (SELECT COUNT(*) FROM RecurrenceTransaction INNER JOIN RecurrenceConfiguration ON RecurrenceTransaction.numRecConfigID = RecurrenceConfiguration.numRecConfigID WHERE RecurrenceTransaction.numRecurrOppID = @numOppId AND ISNULL(RecurrenceConfiguration.bitDisabled,0) = 0) > 0
	BEGIN	
		RAISERROR ( 'RECURRED ORDER', 16, 1 ) ;
		RETURN ;
	END

      IF EXISTS ( SELECT  *
                FROM    dbo.CaseOpportunities
                WHERE   numOppID = @numOppId
                        AND numDomainID = @numDomainID )         
        BEGIN
            RAISERROR ( 'CASE DEPENDANT', 16, 1 ) ;
            RETURN ;
        END	
	 IF EXISTS ( SELECT  *
			FROM    dbo.ReturnHeader
			WHERE   numOppId = @numOppId
					AND numDomainId= @numDomainID )         
	BEGIN
		RAISERROR ( 'RETURN_EXIST', 16, 1 ) ;
		RETURN ;
	END	

      
      
      

DECLARE @tintError TINYINT;SET @tintError=0

EXECUTE USP_CheckCanbeDeleteOppertunity @numOppId,@tintError OUTPUT
  
  IF @tintError=1
  BEGIN
  	 RAISERROR ( 'OppItems DEPENDANT', 16, 1 ) ;
     RETURN ;
  END
  
  EXEC dbo.USP_ValidateFinancialYearClosingDate
	@numDomainID = @numDomainID, --  numeric(18, 0)
	@tintRecordType = 2, --  tinyint
	@numRecordID = @numOppId --  numeric(18, 0)
	
  --Credit Balance
   Declare @monCreditAmount as money;Set @monCreditAmount=0
   Declare @monCreditBalance as money;Set @monCreditBalance=0
   Declare @numDivisionID as numeric(9);Set @numDivisionID=0
   Declare @tintOppType as tinyint

   Select @numDivisionID=numDivisionID,@tintOppType=tintOppType from OpportunityMaster WHERE  numOppId=@numOppId  and numDomainID= @numDomainID 
   
    IF @tintOppType=1 --Sales
      Select @monCreditBalance=isnull(monSCreditBalance,0) from DivisionMaster WHERE  numDivisionID=@numDivisionID  and numDomainID= @numDomainID 
    else IF @tintOppType=2 --Purchase
      Select @monCreditBalance=isnull(monPCreditBalance,0) from DivisionMaster WHERE  numDivisionID=@numDivisionID  and numDomainID= @numDomainID 
   
   Select @monCreditAmount=sum(monAmount) from CreditBalanceHistory where numReturnID in (select numReturnID from [Returns] WHERE [numOppId] = @numOppId)
	
	IF ( @monCreditAmount > @monCreditBalance )         
        BEGIN
            RAISERROR ( 'CreditBalance DEPENDANT', 16, 1 ) ;
            RETURN ;
        END	
        
  declare @tintOppStatus as tinyint    
 declare @tintShipped as tinyint                
  select @tintOppStatus=tintOppStatus,@tintShipped=tintShipped from OpportunityMaster where numOppID=@numOppId                
  
  IF exists(SELECT * FROM WorkOrder WHERE ISNULL(numOppID,0)=@numOppId and numDomainID=@numDomainID)
  BEGIN
        --If Work Order is completed then set bitWorkOrder=0 for particular Opp Items
  	  	UPDATE OpportunityItems SET bitWorkOrder=0 WHERE ISNULL(bitWorkOrder,0)=1 AND numOppID=@numOppId
  	  	AND numItemCode IN (SELECT numItemCode FROM WorkOrder WHERE ISNULL(numOppID,0)=@numOppId and numDomainID=@numDomainID AND ISNULL(numWOStatus,0)=23184)
  	  	
  	  	DELETE FROM WorkOrderDetails WHERE numWOId IN (SELECT numWOId FROM WorkOrder WHERE ISNULL(numOppID,0)=@numOppId and numDomainID=@numDomainID)
  	  	DELETE FROM WorkOrder WHERE ISNULL(numOppID,0)=@numOppId and numDomainID=@numDomainID
  END

    DECLARE @isOrderItemsAvailable AS INT = 0

	SELECT 
		@isOrderItemsAvailable = COUNT(*) 
	FROM    
		OpportunityItems OI
	JOIN 
		dbo.OpportunityMaster OM ON OI.numOppId = OM.numOppId
	JOIN 
		Item I ON OI.numItemCode = I.numItemCode
	WHERE   
		(charitemtype='P' OR 1=(CASE WHEN tintOppType=1 THEN 
								CASE WHEN ISNULL(I.bitKitParent,0) = 1 AND ISNULL(I.bitAssembly,0) = 1 THEN 0
									 WHEN ISNULL(I.bitKitParent,0) = 1 THEN 1
									 ELSE 0 END 
								ELSE 0 END)) AND OI.numOppId = @numOppId
							   AND ( bitDropShip = 0
									 OR bitDropShip IS NULL
								   )  AND ISNULL(OI.bitWorkOrder,0)=0 

  if (@tintOppStatus=1 and @tintShipped=0) OR  @tintShipped=1               
  begin    
    exec USP_RevertDetailsOpp @numOppId,1,@numUserCntID                
  end                          
                        
  DELETE FROM [OpportunitySalesTemplate] WHERE [numOppId] =@numOppId 
  delete OpportunityLinking where   numChildOppID=  @numOppId or   numParentOppID=    @numOppId          
  delete RECENTITEMS where numRecordID =  @numOppId and chrRecordType='O'             
                          
   if @tintShipped=1
    BEGIN
		update WD set tintStatus=0,numQty=WD.numQty + WS.numQty FROM WareHouseItmsDTL WD INNER JOIN OppWarehouseSerializedItem WS ON WD.numWareHouseItmsDTLID=WS.numWarehouseItmsDTLID where  WS.numOppID =  @numOppId
    END      
    ELSE
	BEGIN
		update WareHouseItmsDTL set tintStatus=0 where numWareHouseItmsDTLID in (select numWarehouseItmsDTLID from OppWarehouseSerializedItem where numOppID =  @numOppId)
	END
                       
  delete from OppWarehouseSerializedItem where numOppID=  @numOppId                  
                  
                        
  delete from OpportunityItemLinking where numNewOppID=@numOppId                      
                         
  delete from OpportunityAddress  where numOppID=@numOppId


  DELETE FROM OpportunityBizDocKitItems WHERE [numOppBizDocItemID] IN 
  (SELECT [numOppBizDocItemID] FROM [OpportunityBizDocItems] WHERE [numOppBizDocID] IN 
	(select numOppBizDocsId from OpportunityBizDocs where numOppId=@numOppId))
	
  delete from OpportunityBizDocItems where numOppBizDocID in                        
  (select numOppBizDocsId from OpportunityBizDocs where numOppId=@numOppId)

--  delete from OpportunityBizDocTaxItems where numOppBizDocID in                        
--  (select numOppBizDocsId from OpportunityBizDocs where numOppId=@numOppId)
--  

  delete from OpportunityItemsTaxItems WHERE numOppID=@numOppID
  delete from OpportunityMasterTaxItems WHERE numOppID=@numOppID
  
  delete from OpportunityBizDocDtl where numBizDocID in                        
  (select numOppBizDocsId from OpportunityBizDocs where numOppId=@numOppId)                        
           
   DELETE FROM OpportunityBizDocsPaymentDetails WHERE [numBizDocsPaymentDetId] IN 
	 (SELECT [numBizDocsPaymentDetId] FROM [OpportunityBizDocsDetails] where numBizDocsId in                        
		(select numOppBizDocsId from OpportunityBizDocs where numOppId=@numOppId)) 
               
  delete from OpportunityBizDocsDetails where numBizDocsId in                        
  (select numOppBizDocsId from OpportunityBizDocs where numOppId=@numOppId)  

DELETE FROM [ShippingReportItems] WHERE [numShippingReportId] IN  (SELECT numShippingReportId from [ShippingReport] WHERE [numOppBizDocId] IN (select numOppBizDocsId from OpportunityBizDocs where numOppId=@numOppId))
DELETE FROM [ShippingBox] WHERE [numShippingReportId] IN  (SELECT numShippingReportId from [ShippingReport] WHERE [numOppBizDocId] IN (select numOppBizDocsId from OpportunityBizDocs where numOppId=@numOppId))
DELETE FROM [ShippingReport] WHERE [numOppBizDocId] IN (select numOppBizDocsId from OpportunityBizDocs where numOppId=@numOppId)

delete from DocumentWorkflow where numDocID in 
(select numOppBizDocsId from OpportunityBizDocs where numOppId=@numOppId) and cDocType='B'      

delete from BizDocAction where numBizActionId in(select numBizActionId from BizActionDetails where numOppBizDocsId in
(select numOppBizDocsId from OpportunityBizDocs where numOppId=@numOppId) and btDocType=1)

delete from BizActionDetails where numOppBizDocsId in
(select numOppBizDocsId from OpportunityBizDocs where numOppId=@numOppId) and btDocType=1

  delete from OpportunityBizDocs where numOppId=@numOppId                        
                          
  delete from OpportunityContact where numOppId=@numOppId                        
                          
  delete from OpportunityDependency where numOpportunityId=@numOppId                        
                          
  delete from TimeAndExpense where numOppId=@numOppId   and numDomainID= @numDomainID                     
  
  --Credit Balance
  delete from CreditBalanceHistory where numReturnID in (select numReturnID from [Returns] WHERE [numOppId] = @numOppId)

    IF @tintOppType=1 --Sales
		update DivisionMaster set monSCreditBalance=isnull(monSCreditBalance,0) - isnull(@monCreditAmount,0) where numDivisionID=@numDivisionID and numDomainID=@numDomainID
    else IF @tintOppType=2 --Purchase
		update DivisionMaster set monPCreditBalance=isnull(monPCreditBalance,0) - isnull(@monCreditAmount,0) where numDivisionID=@numDivisionID and numDomainID=@numDomainID
  

  DELETE FROM [Returns] WHERE [numOppId] = @numOppId
                         
  delete from OpportunityKitItems where numOppId=@numOppId                        
                          
  delete from OpportunityItems where numOppId=@numOppId                        
                          
  DELETE FROM dbo.ProjectProgress WHERE numOppId =@numOppId AND numDomainId=@numDomainID
                          
  delete from OpportunityStageDetails where numOppId=@numOppId
                          
  delete from OpportunitySubStageDetails where numOppId=@numOppId
                                               
  DELETE FROM [OpportunityRecurring] WHERE [numOppId] = @numOppId
  
  DELETE FROM [OpportunityMasterAPI] WHERE [numOppId] = @numOppId
  
  DELETE FROM [WebApiOrderDetails] WHERE [numOppId] = @numOppId
  
  DELETE FROM [WebApiOppItemDetails] WHERE [numOppId] = @numOppId
  
  /*
  added by chintan: reason: when bizdoc is deleted and journal entries are not deleted in exceptional case, 
  it throws Foreign key reference error when deleting order.
  */
  DELETE FROM dbo.General_Journal_Details WHERE numJournalId IN (SELECT numJournal_Id FROM dbo.General_Journal_Header WHERE numOppId=@numOppId AND numDomainId=@numDomainID)
  DELETE FROM dbo.General_Journal_Header WHERE numOppId=@numOppId AND numDomainId= @numDomainID
 
  IF ISNULL(@tintOppStatus,0)=0 OR ISNULL(@isOrderItemsAvailable,0) = 0 OR (SELECT COUNT(*) FROM WareHouseItems_Tracking WHERE numDomainID=@numDomainID AND numReferenceID=@numOppId AND tintRefType IN (3,4) AND vcDescription LIKE '%Deleted%' AND CAST(dtCreatedDate AS DATE) = CAST(GETUTCDATE() AS DATE)) > 0
	delete from OpportunityMaster where numOppId=@numOppId and numDomainID= @numDomainID             
  ELSE
	RAISERROR ( 'INVENTORY IM-BALANCE', 16, 1 ) ;


COMMIT TRAN 

    END TRY 
    BEGIN CATCH		
        DECLARE @strMsg VARCHAR(200)
        SET @strMsg = ERROR_MESSAGE()
        IF ( @@TRANCOUNT > 0 ) 
            BEGIN
             RAISERROR ( @strMsg, 16, 1 ) ;
                ROLLBACK TRAN
                RETURN 1
            END
    END CATCH	

SET XACT_ABORT OFF;
END 
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_EmailBroadcastConfiguration_GetByDomain' ) 
    DROP PROCEDURE USP_EmailBroadcastConfiguration_GetByDomain
GO

CREATE PROCEDURE USP_EmailBroadcastConfiguration_GetByDomain  
(  
 @numDomainID NUMERIC   
)  
AS  
BEGIN  
-- SET NOCOUNT ON added to prevent extra result sets from  
-- interfering with SELECT statements.  
SET NOCOUNT ON;  

SELECT * FROM EmailBroadcastConfiguration WHERE numDomainId = @numDomainID

END  
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_EmailBroadcastConfiguration_Save' ) 
    DROP PROCEDURE USP_EmailBroadcastConfiguration_Save
GO

CREATE PROCEDURE USP_EmailBroadcastConfiguration_Save  
(  
 @numDomainID NUMERIC,  
 @vcAWSAccessKey VARCHAR(100),  
 @vcAWSSecretKey VARCHAR(100),
 @vcAWSDomain VARCHAR(200),
 @vcFrom VARCHAR(100)
   
)  
AS  
BEGIN  
-- SET NOCOUNT ON added to prevent extra result sets from  
-- interfering with SELECT statements.  
SET NOCOUNT ON;  

IF EXISTS (SELECT * FROM EmailBroadcastConfiguration WHERE numDomainId = @numDomainID)
BEGIN
	UPDATE
		EmailBroadcastConfiguration
	SET
		vcAWSAccessKey=@vcAWSAccessKey,
		vcAWSSecretKey=@vcAWSSecretKey,
		vcAWSDomain=@vcAWSDomain,
		vcFrom= @vcFrom
	WHERE
		numDomainId = @numDomainID
END
ELSE
BEGIN
	INSERT INTO EmailBroadcastConfiguration
	(
		numDomainId,
		vcFrom,
		vcAWSDomain,
		vcAWSAccessKey,
		vcAWSSecretKey
	)
	VALUES
	(
		@numDomainID,
		@vcFrom,
		@vcAWSDomain,
		@vcAWSAccessKey,
		@vcAWSSecretKey
	)
END

END  

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetBizDocsByOrderType')
DROP PROCEDURE USP_GetBizDocsByOrderType
GO
CREATE PROCEDURE [dbo].[USP_GetBizDocsByOrderType]
	@ListID AS NUMERIC(9)=0,                
	@numDomainID AS NUMERIC(9)=0, 
	@tintOppType AS TINYINT,
	@tintOppStatus AS TINYINT
AS
BEGIN
	SET NOCOUNT ON;

	Declare @numAuthoritativeId AS NUMERIC(9) 

	If @tintOppType=1           
	 SELECT @numAuthoritativeId=numAuthoritativeSales FROM AuthoritativeBizDocs WHERE numDomainId=@numDomainID           
	Else IF @tintOppType=2
	 SELECT @numAuthoritativeId=numAuthoritativePurchase FROM AuthoritativeBizDocs WHERE numDomainId=@numDomainID            
 

	IF @tintOppStatus=1         
	BEGIN        
		IF ((SELECT COUNT(*) FROM BizDocFilter WHERE numDomainID=@numDomainID)=0)
		BEGIN
			SELECT 
				numListItemID,
				case when @numAuthoritativeId=numListItemID then  vcData +' - Authoritative' Else vcData   End as  vcData 
			FROM 
				listdetails               
			WHERE 
				numListID=@ListID AND 
				(constFlag=1 OR numDomainID=@numDomainID)  
		END
		ELSE
		BEGIN
			SELECT 
				Ld.numListItemID,
				case when @numAuthoritativeId=Ld.numListItemID then  isnull(vcRenamedListName,vcData) +' - Authoritative' Else isnull(vcRenamedListName,vcData)   End as  vcData 
			FROM 
				BizDocFilter BDF 
			INNER JOIN 
				listdetails Ld 
			ON 
				BDF.numBizDoc=Ld.numListItemID
			LEFT JOIN 
				listorder LO 
			ON 
				Ld.numListItemID= LO.numListItemID AND 
				Lo.numDomainId = @numDomainID
			WHERE 
				Ld.numListID=@ListID AND 
				BDF.tintBizocType=@tintOppType AND 
				BDF.numDomainID=@numDomainID AND 
				(constFlag=1 OR Ld.numDomainID=@numDomainID)            
		END        
	END               
	Else        
	Begin        
		IF ((SELECT COUNT(*) FROM BizDocFilter WHERE numDomainID=@numDomainID)=0)
		BEGIN
			SELECT 
				numListItemID,
				vcData as  vcData 
			FROM 
				listdetails               
			WHERE 
				numListID=@ListID and 
				(constFlag=1 or numDomainID=@numDomainID) And 
				numListItemID<>@numAuthoritativeId          
		END
		ELSE
		BEGIN
			SELECT 
				Ld.numListItemID, 
				isnull(vcRenamedListName,vcData) as vcData 
			FROM 
				BizDocFilter BDF 
			INNER JOIN 
				listdetails Ld 
			ON 
				BDF.numBizDoc=Ld.numListItemID
			LEFT JOIN 
				listorder LO 
			ON 
				Ld.numListItemID= LO.numListItemID AND 
				Lo.numDomainId = @numDomainID
			WHERE 
				Ld.numListID=@ListID AND 
				BDF.tintBizocType=@tintOppType AND 
				BDF.numDomainID=@numDomainID AND 
				(constFlag=1 or Ld.numDomainID=@numDomainID) AND 
				Ld.numListItemID<>@numAuthoritativeId AND 
				Ld.numListItemID NOT IN (296)
		  END    
	END        
END







-- CREATED BY MANISH ANJARA : 28th OCT, 2014
/*
EXEC USP_GetGenralJournalDetailsReport @numDomainId = 169, 
    @dtFromDate = '2014-01-01 00:00:00', @dtToDate = '2014-01-01 23:59:59',
    @CurrentPage = 1,@PageSize = 100

*/

GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetGenralJournalDetailsReport' )
    DROP PROCEDURE USP_GetGenralJournalDetailsReport
GO
CREATE PROCEDURE [dbo].[USP_GetGenralJournalDetailsReport]
    (
      @numDomainID NUMERIC(18, 0) ,
      @dtFromDate DATETIME ,
      @dtToDate DATETIME ,
      @CurrentPage INT = 0 ,
      @PageSize INT = 0 ,
      @tintMode AS TINYINT = 0
    )
AS
    BEGIN
	
        --SELECT TOP 10
        --        *
        --FROM    [dbo].[General_Journal_Details] AS GJD
        --ORDER BY [GJD].[numJournalId] DESC

        DECLARE @first_id NUMERIC
        DECLARE @startRow INT

        DECLARE @CurrRecord AS INT
        IF @tintMode = 0
            BEGIN

                SELECT TOP 1
                        [GJH].[numJournal_Id] ,
                        [GJD].[numTransactionId] ,
                        dbo.[FormatedDateFromDate]([GJH].[datEntry_Date],
                                                   [GJD].numDomainID) [Date] ,
                        COA.[numAccountId] ,
                        ISNULL([COA].vcAccountName, '') AS [vcAccountName] ,
                        ISNULL([GJD].[numDebitAmt], 0) [numDebitAmt] ,
                        ISNULL([GJD].[numCreditAmt], 0) [numCreditAmt]
                FROM    [dbo].[General_Journal_Header] AS GJH
                        JOIN [dbo].[General_Journal_Details] AS GJD ON [GJD].[numJournalId] = [GJH].[numJournal_Id]
                                                              AND [GJD].[numDomainId] = [GJH].[numDomainId]
                        JOIN [dbo].[Chart_Of_Accounts] AS COA ON [GJD].[numChartAcntId] = [COA].[numAccountId]
                                                              AND [GJH].[numDomainId] = [COA].[numDomainId]
                WHERE   [GJH].[numDomainId] = @numDomainID
                        AND [GJH].[datEntry_Date] BETWEEN @dtFromDate
                                                  AND     @dtToDate
						AND [GJD].[tintReferenceType] = 5
                ORDER BY [GJD].numEntryDateSortOrder1 ASC
            END
        ELSE
            IF @tintMode = 1
                BEGIN
                    SET @CurrRecord = ( ( @CurrentPage - 1 ) * @PageSize ) + 1
                    PRINT @CurrRecord
                    SET ROWCOUNT @CurrRecord

                    SELECT  @first_id = [GJD].numEntryDateSortOrder1
                    FROM    [dbo].[General_Journal_Header] AS GJH
                            JOIN [dbo].[General_Journal_Details] AS GJD ON [GJD].[numJournalId] = [GJH].[numJournal_Id]
                                                              AND [GJD].[numDomainId] = [GJH].[numDomainId]
                            JOIN [dbo].[Chart_Of_Accounts] AS COA ON [GJD].[numChartAcntId] = [COA].[numAccountId]
                                                              AND [GJH].[numDomainId] = [COA].[numDomainId]
                    WHERE   [GJH].[numDomainId] = @numDomainID
                            AND [GJH].[datEntry_Date] BETWEEN @dtFromDate
                                                      AND     @dtToDate
							AND [GJD].[tintReferenceType] = 5
                    ORDER BY [GJD].numEntryDateSortOrder1 ASC

                    PRINT @first_id
                    PRINT ROWCOUNT_BIG()
                    SET ROWCOUNT @PageSize

                    SELECT  [GJD].[numJournalId] ,
                            [GJD].[numTransactionId] ,
                            dbo.[FormatedDateFromDate]([GJH].[datEntry_Date],
                                                       [GJD].numDomainID) [Date] ,
                            COA.[numAccountId] ,
                            ISNULL([COA].vcAccountName, '') AS [vcAccountName] ,
                            ISNULL([GJD].[numDebitAmt], 0) [numDebitAmt] ,
                            ISNULL([GJD].[numCreditAmt], 0) [numCreditAmt]
                    FROM    [dbo].[General_Journal_Header] AS GJH
                            JOIN [dbo].[General_Journal_Details] AS GJD ON [GJD].[numJournalId] = [GJH].[numJournal_Id]
                                                              AND [GJD].[numDomainId] = [GJH].[numDomainId]
                            JOIN [dbo].[Chart_Of_Accounts] AS COA ON [GJD].[numChartAcntId] = [COA].[numAccountId]
                                                              AND [GJH].[numDomainId] = [COA].[numDomainId]
                    WHERE   [GJH].[numDomainId] = @numDomainID
                            AND [GJH].[datEntry_Date] BETWEEN @dtFromDate
                                                      AND     @dtToDate
							AND [GJD].[tintReferenceType] = 5
                            AND [GJD].numEntryDateSortOrder1 >= @first_id
                    ORDER BY [GJH].[datEntry_Date],[GJD].[numJournalId] ASC
                
                    SET ROWCOUNT 0

                END
            ELSE
                IF @tintMode = 3
                    BEGIN
                        SELECT  dbo.[FormatedDateFromDate]([GJH].[datEntry_Date],
                                                           [GJD].numDomainID) [Date] ,
                                COA.[numAccountId] AS [AccountID],
                                CASE WHEN ISNULL([GJD].[numDebitAmt], 0) = 0 THEN ISNULL([COA].vcAccountName, '') ELSE '' END AS [Reference] ,
								CASE WHEN ISNULL([GJD].[numCreditAmt], 0) = 0 THEN ISNULL([COA].vcAccountName, '')  ELSE '' END AS [TransDesc] ,
                                ISNULL([GJD].[numDebitAmt], 0) [Debit] ,
                                ISNULL([GJD].[numCreditAmt], 0) [Credit]
                        FROM    [dbo].[General_Journal_Header] AS GJH
                                JOIN [dbo].[General_Journal_Details] AS GJD ON [GJD].[numJournalId] = [GJH].[numJournal_Id]
                                                              AND [GJD].[numDomainId] = [GJH].[numDomainId]
                                JOIN [dbo].[Chart_Of_Accounts] AS COA ON [GJD].[numChartAcntId] = [COA].[numAccountId]
                                                              AND [GJH].[numDomainId] = [COA].[numDomainId]
                        WHERE   [GJH].[numDomainId] = @numDomainID
                                AND [GJH].[datEntry_Date] BETWEEN @dtFromDate
                                                          AND @dtToDate
								AND [GJD].[tintReferenceType] = 5
                        ORDER BY [GJD].numEntryDateSortOrder1 ASC
                    END
    END
/****** Object:  StoredProcedure [dbo].[USP_GetOppItemsForAccounting]    Script Date: 25/11/2014 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by Manish                             
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetOppItemsForAccounting' )
    DROP PROCEDURE USP_GetOppItemsForAccounting
GO
-- EXEC USP_GetOppItemsForAccounting 111925,169,1
CREATE PROCEDURE [dbo].[USP_GetOppItemsForAccounting]
    (
      @numOppId AS NUMERIC(9) = NULL ,
      @numDomainID AS NUMERIC(9) = 0 ,
      @numUserCntID AS NUMERIC(9) = 0                                                                                                                                               
    )
AS
    BEGIN

        DECLARE @numCurrencyID AS NUMERIC(9)
        DECLARE @fltExchangeRate AS FLOAT

        DECLARE @fltCurrentExchangeRate AS FLOAT
        DECLARE @tintType AS TINYINT                                                          
        DECLARE @vcPOppName AS VARCHAR(100)                                                          
        DECLARE @bitPPVariance AS BIT
        DECLARE @numDivisionID AS NUMERIC(9)

        SELECT  @numCurrencyID = numCurrencyID ,
                @fltExchangeRate = fltExchangeRate ,
                @tintType = tintOppType ,
                @vcPOppName = vcPOppName ,
                @bitPPVariance = ISNULL(bitPPVariance, 0) ,
                @numDivisionID = numDivisionID
        FROM    OpportunityMaster
        WHERE   numOppID = @numOppId 

        DECLARE @vcBaseCurrency AS VARCHAR(100);
            SET @vcBaseCurrency = ''
        DECLARE @bitAutolinkUnappliedPayment AS BIT 
        SELECT  @vcBaseCurrency = ISNULL(C.varCurrSymbol, '') ,
                @bitAutolinkUnappliedPayment = ISNULL(bitAutolinkUnappliedPayment,
                                                      0)
        FROM    dbo.Domain D
                LEFT JOIN Currency C ON D.numCurrencyID = C.numCurrencyID
        WHERE   D.numDomainID = @numDomainID

        DECLARE @vcForeignCurrency AS VARCHAR(100);
            SET @vcForeignCurrency = ''
        SELECT  @vcForeignCurrency = ISNULL(C.varCurrSymbol, '')
        FROM    Currency C
        WHERE   numCurrencyID = @numCurrencyID

        DECLARE @fltExchangeRateBizDoc AS FLOAT 
        --DECLARE @numBizDocId AS NUMERIC(9)                                                           

        --DECLARE @vcBizDocID AS VARCHAR(100)
        --SELECT  @numBizDocId = numBizDocId ,
        --        @fltExchangeRateBizDoc = fltExchangeRateBizDoc ,
        --        @vcBizDocID = vcBizDocID
        --FROM    OpportunityBizDocs
        --WHERE   numOppBizDocsId = @numOppBizDocsId                                                         

        IF @numCurrencyID > 0
            SET @fltCurrentExchangeRate = dbo.GetExchangeRate(@numDomainID,
                                                              @numCurrencyID)
        ELSE
            SET @fltCurrentExchangeRate = @fltExchangeRate                                                                                                                                          

        DECLARE @strSQL AS VARCHAR(8000)                                                          
        DECLARE @strSQLCusFields VARCHAR(1000)                                                          
        DECLARE @strSQLEmpFlds VARCHAR(500)                                                          
        SET @strSQLCusFields = ''                                                          
        SET @strSQLEmpFlds = ''                                                          
        DECLARE @intRowNum AS INT                                                          
        DECLARE @numFldID AS VARCHAR(15)                                                          
        DECLARE @vcFldname AS VARCHAR(50)                                                                                                                                          
                                                                
        IF @tintType = 1
            SET @tintType = 7                                                                
        ELSE
            SET @tintType = 8                                                                                                                      
        SELECT TOP 1
                @numFldID = numfieldID ,
                @vcFldname = vcFieldName ,
                @intRowNum = ( tintRow + 1 )
        FROM    View_DynamicCustomColumns
        WHERE   Grp_id = @tintType
                AND numDomainID = @numDomainID
                --AND numAuthGroupID = @numBizDocId
                AND bitCustom = 1
        ORDER BY tintRow                                                          
        WHILE @intRowNum > 0
            BEGIN                                                          
                SET @strSQLCusFields = @strSQLCusFields
                    + ',  dbo.GetCustFldValueBizdoc(' + @numFldID + ','
                    + CONVERT(VARCHAR(2), @tintType)
                    + ',opp.numoppitemtCode) as [' + @vcFldname + ']'                                                     
                SET @strSQLEmpFlds = @strSQLEmpFlds + ',''-'' as ['
                    + @vcFldname + ']'         
                                                 
                SELECT TOP 1
                        @numFldID = numfieldID ,
                        @vcFldname = vcFieldName ,
                        @intRowNum = ( tintRow + 1 )
                FROM    View_DynamicCustomColumns
                WHERE   Grp_id = @tintType
                        AND numDomainID = @numDomainID
                        --AND numAuthGroupID = @numBizDocId
                        AND bitCustom = 1
                        AND tintRow > @intRowNum
                ORDER BY tintRow                                                          
                                                            
                IF @@rowcount = 0
                    SET @intRowNum = 0                                                          
                                                           
            END                                                           
                                                       
       
         
        SELECT  I.[vcItemName] AS Item ,
                charitemType AS type ,
                opp.vcitemdesc AS [desc] ,
                opp.numUnitHour AS Unit ,
                opp.monPrice AS Price ,
                opp.monTotAmount AS Amount ,
                ( opp.monTotAmount / opp.numUnitHour ) * opp.numUnitHour AS ItemTotalAmount ,       
--case when isnull(bitTaxable,0) =0 then 0 when bitTaxable=1 then (select fltPercentage from OpportunityBizDocTaxItems where numOppBizDocID=@numOppBizDocsId and numTaxItemID=0)  end as Tax,       
                ISNULL(monListPrice, 0) AS listPrice ,
                ISNULL(i.numItemCode,0) AS ItemCode ,
                numoppitemtCode ,
                L.vcdata AS vcItemClassification ,
                CASE WHEN bitTaxable = 0 THEN 'No'
                     ELSE 'Yes'
                END AS Taxable ,
                monListPrice + ' ' + @strSQLCusFields ,
                ISNULL(i.numIncomeChartAcntId, 0) AS itemIncomeAccount ,
                ISNULL(i.numAssetChartAcntId, 0) AS itemInventoryAsset ,
                'NI' AS ItemType ,
                ISNULL(i.numCOGsChartAcntId, 0) AS itemCoGs ,
                ISNULL(i.monAverageCost, '0') AS AverageCost ,
                ISNULL(opp.bitDropShip, 0) AS bitDropShip ,
                ( opp.monTotAmtBefDiscount - opp.monTotAmount ) AS DiscAmt ,
                NULLIF(Opp.numProjectID, 0) numProjectID ,
                NULLIF(Opp.numClassID, 0) numClassID ,
                ISNULL(i.bitKitParent, 0) AS bitKitParent ,
                ISNULL(i.bitAssembly, 0) AS bitAssembly,
				ISNULL([opp].[numUnitHour],0) [numUnitHour],
				ISNULL(opp.[numUnitHourReceived],0) [numUnitHourReceived],
				ISNULL(opp.[numUnitHourReceived],0) [numUnitReceived],
				ISNULL(opp.[monPrice],0) [monPrice],
				ISNULL([OM].[dtItemReceivedDate],GETDATE()) AS [datEntry_Date],
				[OM].[vcPOppName],
				ISNULL([OM].[fltExchangeRate],0) [fltExchangeRate],[i].charItemType,
				ISNULL(opp.[bitDropShip],0) [DropShip],
				ISNULL([i].[numAssetChartAcntId],0) [ItemInventoryAsset],
				ISNULL([i].[numIncomeChartAcntId],0) [ItemInventoryAsset],
				ISNULL([i].[numCOGsChartAcntId],0) [ItemCoGs],
				I.[vcItemName] [vcItemName],
				ISNULL([OM].[numDivisionId],0) [numDivisionId],
				ISNULL([opp].[numoppitemtCode],0) AS [numoppitemtCode],
				ISNULL([OM].[numCurrencyID],0) [numCurrencyID],
				ISNULL([opp].[numClassID],0) [numClassID],
				ISNULL([opp].[numProjectID],0) AS [numProjectID],
				I.[numItemCode]
        FROM    OpportunityItems opp
				JOIN [dbo].[OpportunityMaster] AS OM ON opp.[numOppId] = OM.[numOppId]
                --JOIN OpportunityBizDocItems OBI ON OBI.numOppItemID = Opp.numoppitemtCode
                LEFT JOIN item i ON opp.numItemCode = i.numItemCode
                LEFT JOIN ListDetails L ON i.numItemClassification = L.numListItemID
        WHERE   Opp.numOppId = @numOppId
                --AND OBI.numOppBizDocID = @numOppBizDocsId                                                                                                                  
                                                         
        SELECT  ISNULL(@numCurrencyID, 0) AS numCurrencyID ,
                ISNULL(@fltExchangeRate, 1) AS fltExchangeRate ,
                ISNULL(@fltCurrentExchangeRate, 1) AS CurrfltExchangeRate ,
                ISNULL(@fltExchangeRateBizDoc, 1) AS fltExchangeRateBizDoc ,
                @vcBaseCurrency AS vcBaseCurrency ,
                @vcForeignCurrency AS vcForeignCurrency ,
                ISNULL(@vcPOppName, '') AS vcPOppName ,
                --ISNULL(@vcBizDocID, '') AS vcBizDocID ,
                ISNULL(@bitPPVariance, 0) AS bitPPVariance ,
                @numDivisionID AS numDivisionID ,
                @bitAutolinkUnappliedPayment AS bitAutolinkUnappliedPayment                  
     
     
                                                  
    END
GO

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetOppSerialLotNumber')
DROP PROCEDURE USP_GetOppSerialLotNumber
GO
CREATE PROCEDURE [dbo].[USP_GetOppSerialLotNumber]
	@numOppID AS NUMERIC(18,0),
	@numOppItemID AS NUMERIC(18,0),
	@numWarehouseItemID AS NUMERIC(18,0)
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @numDomainID AS NUMERIC(18,0)
	SELECT @numDomainID= numDomainId FROM OpportunityMaster WHERE numOppId = @numOppID


	-- Available Serail Lot Numbers
	SELECT
		ROW_NUMBER() OVER(ORDER BY dExpirationDate DESC) AS OrderNo,
		numWareHouseItmsDTLID,
		vcSerialNo,
		numQty,
		dbo.FormatedDateFromDate(dExpirationDate,@numDomainID) AS dExpirationDate
	FROM
	(
		SELECT
			
			numWareHouseItmsDTLID,
			vcSerialNo,
			numQty - isnull((select sum(w.numQty) from OppWarehouseSerializedItem w INNER JOIN OpportunityMaster opp ON w.numOppId=opp.numOppId AND opp.tintOppType=1 where isnull(opp.tintshipped,0)=0 and w.numWarehouseItmsDTLID=WareHouseItmsDTL.numWarehouseItmsDTLID),0) AS numQty,
			dExpirationDate
		FROM
			WareHouseItmsDTL
		WHERE
			numWareHouseItemID = @numWarehouseItemID AND
			ISNULL(numQty,0) > 0 
		
	) Temp
	WHERE
		Temp.numQty > 0
	ORDER BY
		dExpirationDate DESC

    -- Selected Serail Lot Numbers
	SELECT
		ROW_NUMBER() OVER(ORDER BY WareHouseItmsDTL.dExpirationDate DESC) AS OrderNo,
		WareHouseItmsDTL.numWareHouseItmsDTLID,
		WareHouseItmsDTL.vcSerialNo,
		dbo.FormatedDateFromDate(WareHouseItmsDTL.dExpirationDate,@numDomainID) AS dExpirationDate,
		OppWarehouseSerializedItem.numQty
	FROM
		OppWarehouseSerializedItem
	JOIN
		WareHouseItmsDTL
	ON
		OppWarehouseSerializedItem.numWarehouseItmsDTLID = WareHouseItmsDTL.numWareHouseItmsDTLID
	WHERE
		numOppID = @numOppID AND
		numOppItemID = @numOppItemID AND
		numWareHouseItemID = @numWarehouseItemID
	ORDER BY
		WareHouseItmsDTL.dExpirationDate DESC

END



GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetOrderWithoutJournals')
DROP PROCEDURE USP_GetOrderWithoutJournals
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- EXEC USP_GetOrderWithoutJournals 
CREATE PROCEDURE USP_GetOrderWithoutJournals
AS 
BEGIN
	SELECT DISTINCT --TOP 1 
	[OM].[numOppId],[OM].[numDomainId], [OM].[numDivisionId] ,OM.[vcPOppName], OM.[dtItemReceivedDate],OM.monDealAmount, [OM].[tintOppType],[OM].[numCreatedBy]
	FROM [dbo].[OpportunityMaster] AS OM 
	JOIN [dbo].[OpportunityItems] AS OI ON OM.[numOppId] = OI.[numOppId]
	WHERE [OM].[tintshipped] = 0
	--AND OM.[numDomainId] = 1
	--AND OM.[dtItemReceivedDate] >= '2013-09-30 00:00:59' 
	AND [OI].[numUnitHour] = OI.[numUnitHourReceived]
	AND [OM].[numOppId] NOT IN
	(
	SELECT ISNULL(numOppID,0) FROM [dbo].[General_Journal_Header] AS GJH 
	WHERE [GJH].[numDomainId] = OM.[numDomainId]
	AND ISNULL([GJH].[numOppBizDocsId],0) = 0
	)
END
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetOrganizationTransaction')
DROP PROCEDURE USP_GetOrganizationTransaction
GO
CREATE PROCEDURE [dbo].[USP_GetOrganizationTransaction]
    (
      @numDivisionID NUMERIC(9)=0,
      @numDomainID NUMERIC,
      @CurrentPage INT=0,
	  @PageSize INT=0,
      @TotRecs INT=0  OUTPUT,
      @vcTransactionType AS VARCHAR(500),
      @dtFromDate DATETIME,
      @dtToDate DATETIME,
      @ClientTimeZoneOffset INT
    )
AS 
    BEGIN
		
		SELECT Items INTO #temp FROM dbo.Split(@vcTransactionType,',')

		CREATE TABLE #tempTransaction(intTransactionType INT,vcTransactionType VARCHAR(30),dtCreatedDate DATETIME,numRecordID NUMERIC,
		vcRecordName VARCHAR(100),monTotalAmount MONEY,monPaidAmount MONEY,dtDueDate DATETIME,vcMemo VARCHAR(1000),vcDescription VARCHAR(500),
		vcStatus VARCHAR(100),numRecordID2 NUMERIC,numStatus NUMERIC, vcBizDocId VARCHAR(1000))
		 
		 --Sales Order
		 IF EXISTS (SELECT 1 FROM #temp WHERE Items=1)
		 BEGIN
		 	INSERT INTO #tempTransaction 
		 	SELECT 1,'Sales Order',dbo.FormatedDateFromDate(OM.bintCreatedDate,OM.numDomainId),OM.numOppId,
		 	OM.vcPOppName,
		 	[dbo].[getdealamount](OM.numOppId,Getutcdate(),0),
		 	ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = OM.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0),
		 	NULL,'','',dbo.GetListIemName(numStatus),0,numStatus,
		 	ISNULL(OM.vcOppRefOrderNo,'')
		 	 FROM OpportunityMaster OM where OM.numDomainID = @numDomainID AND OM.numDivisionId=@numDivisionId
                        AND OM.tintOppType = 1  AND OM.tintOppStatus =  1
                        AND ((dateadd(minute,-@ClientTimeZoneOffset,OM.bintCreatedDate) between @dtFromDate And @dtToDate))
		 END
		 
		 --Sales Order Invoices(BizDocs)
		 IF EXISTS (SELECT 1 FROM #temp WHERE Items=2)
		 BEGIN
		 	INSERT INTO #tempTransaction 
		 	SELECT 2,'SO Invoice',dbo.FormatedDateFromDate(OBD.dtCreatedDate,OM.numDomainId),OBD.numOppBizDocsId,
		 	OBD.vcBizDocID,[dbo].[getdealamount](OM.numOppId,Getutcdate(),OBD.numOppBizDocsId),
		 	ISNULL(OBD.monAmountPaid,0),
		 	NULL,'','',dbo.GetListIemName(OBD.numBizDocStatus),OM.numOppId,numBizDocStatus,
		 	ISNULL(vcRefOrderNo,'')
		 	 FROM OpportunityMaster OM INNER JOIN OpportunityBizDocs OBD ON OBD.numOppId = OM.numOppId 
		 	 where OM.numDomainID = @numDomainID AND OM.numDivisionId=@numDivisionId
                        AND OM.tintOppType = 1  AND OM.tintOppStatus =  1 AND OBD.[bitAuthoritativeBizDocs] =1
                        AND ((dateadd(minute,-@ClientTimeZoneOffset,OBD.dtCreatedDate) between @dtFromDate And @dtToDate))
        END
		 
		 --Sales Opportunity
		  IF EXISTS (SELECT 1 FROM #temp WHERE Items=3)
		 BEGIN
		 	INSERT INTO #tempTransaction 
		 	SELECT 3,'Sales Opportunity',dbo.FormatedDateFromDate(OM.bintCreatedDate,OM.numDomainId),OM.numOppId,
		 	OM.vcPOppName,[dbo].[getdealamount](OM.numOppId,Getutcdate(),0),
		 	ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = OM.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0),
		 	NULL,'','',dbo.GetListIemName(numStatus),0,numStatus,''
		 	 FROM OpportunityMaster OM where OM.numDomainID = @numDomainID AND OM.numDivisionId=@numDivisionId
                        AND OM.tintOppType = 1  AND OM.tintOppStatus =  0
                        AND ((dateadd(minute,-@ClientTimeZoneOffset,OM.bintCreatedDate) between @dtFromDate And @dtToDate))
		 END
		 
		 --Purchase Order
		  IF EXISTS (SELECT 1 FROM #temp WHERE Items=4)
		 BEGIN
		 	INSERT INTO #tempTransaction 
		 	SELECT 4,'Purchase Order',dbo.FormatedDateFromDate(OM.bintCreatedDate,OM.numDomainId),OM.numOppId,
		 	OM.vcPOppName,
		 	[dbo].[getdealamount](OM.numOppId,Getutcdate(),0),
		 	ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = OM.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0),
		 	NULL,'','',dbo.GetListIemName(numStatus),0,numStatus,
		 	ISNULL(OM.vcOppRefOrderNo,'')
		 	 FROM OpportunityMaster OM where OM.numDomainID = @numDomainID AND OM.numDivisionId=@numDivisionId
                        AND OM.tintOppType = 2  AND OM.tintOppStatus =  1
                        AND ((dateadd(minute,-@ClientTimeZoneOffset,OM.bintCreatedDate) between @dtFromDate And @dtToDate))
		 END
		 
		 --Purchase Order Bill(BizDocs)
		  IF EXISTS (SELECT 1 FROM #temp WHERE Items=5)
		 BEGIN
		 	INSERT INTO #tempTransaction 
		 	SELECT 5,'PO Bill',dbo.FormatedDateFromDate(OBD.dtCreatedDate,OM.numDomainId),OBD.numOppBizDocsId,
		 	OBD.vcBizDocID,[dbo].[getdealamount](OM.numOppId,Getutcdate(),OBD.numOppBizDocsId),
		 	ISNULL(OBD.monAmountPaid,0),
		 	NULL,'','',dbo.GetListIemName(OBD.numBizDocStatus),OM.numOppId,numBizDocStatus,
		 	ISNULL(vcRefOrderNo,'')
--		 	(SELECT SUBSTRING((SELECT vcBizDocID + ',' FROM OpportunityBizDocs 
--														   WHERE numOppId = OM.numOppId
--														   AND ISNULL(bitAuthoritativeBizDocs,0) = 1 
--										 FOR XML PATH('')), 0, 200000))
		 	 FROM OpportunityMaster OM INNER JOIN OpportunityBizDocs OBD ON OBD.numOppId = OM.numOppId 
		 	 where OM.numDomainID = @numDomainID AND OM.numDivisionId=@numDivisionId
                        AND OM.tintOppType = 2  AND OM.tintOppStatus =  1 AND OBD.[bitAuthoritativeBizDocs] =1
                        AND ((dateadd(minute,-@ClientTimeZoneOffset,OBD.dtCreatedDate) between @dtFromDate And @dtToDate))
		 END
		 
		 --Purchase Opportunity
		  IF EXISTS (SELECT 1 FROM #temp WHERE Items=6)
		 BEGIN
		 	INSERT INTO #tempTransaction 
		 	SELECT 6,'Purchase Opportunity',dbo.FormatedDateFromDate(OM.bintCreatedDate,OM.numDomainId),OM.numOppId,
		 	OM.vcPOppName,[dbo].[getdealamount](OM.numOppId,Getutcdate(),0),
		 	ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = OM.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0),
		 	NULL,'','',dbo.GetListIemName(numStatus),0,numStatus,''
		 	 FROM OpportunityMaster OM where OM.numDomainID = @numDomainID AND OM.numDivisionId=@numDivisionId
                        AND OM.tintOppType = 2  AND OM.tintOppStatus =  0
                        AND ((dateadd(minute,-@ClientTimeZoneOffset,OM.bintCreatedDate) between @dtFromDate And @dtToDate))
		 END
		 
		 --Sales Return
		  IF EXISTS (SELECT 1 FROM #temp WHERE Items=7)
		 BEGIN
		 	INSERT INTO #tempTransaction 
		 	SELECT 7,'Sales Return',dbo.FormatedDateFromDate(RH.dtCreatedDate,RH.numDomainId),RH.numReturnHeaderID,
		 	RH.vcRMA,ISNULL(RH.monAmount,0),
		 	CASE ISNULL(RH.tintReceiveType,0) WHEN 1 THEN ISNULL((SELECT ISNULL(SUM(CH.monAmount),0) FROM dbo.CheckHeader CH WHERE CH.numDomainID=1 AND CH.tintReferenceType=10 AND CH.numReferenceID=RH.numReturnHeaderID AND ISNULL(CH.bitIsPrint,0)=1),0)
		 		 	WHEN 2 THEN ISNULL((SELECT SUM(ISNULL(monAppliedAmount,0)) AS monAmtPaid FROM DepositMaster DM WHERE DM.numDomainID=RH.numDomainID AND DM.numReturnHeaderID=RH.numReturnHeaderID),0)
		 		 	ELSE 0 END,
		 	NULL,'','',dbo.GetListIemName(RH.numReturnStatus),0,RH.numReturnStatus,''
		 	 FROM ReturnHeader RH where RH.numDomainID = @numDomainID AND RH.numDivisionId=@numDivisionId
                        AND RH.tintReturnType = 1
                        AND ((dateadd(minute,-@ClientTimeZoneOffset,RH.dtCreatedDate) between @dtFromDate And @dtToDate))
		 END
		 
		 --Purchase Return
		  IF EXISTS (SELECT 1 FROM #temp WHERE Items=8)
		 BEGIN
		 	INSERT INTO #tempTransaction 
		 	SELECT 8,'Purchase Return',dbo.FormatedDateFromDate(RH.dtCreatedDate,RH.numDomainId),RH.numReturnHeaderID,
		 	RH.vcRMA,ISNULL(RH.monAmount,0),
		 	ISNULL((SELECT SUM(ISNULL(monAppliedAmount,0)) AS monAmtPaid FROM BillPaymentHeader BPH WHERE BPH.numDomainID=RH.numDomainID AND BPH.numReturnHeaderID=RH.numReturnHeaderID),0),
		 	NULL,'','',dbo.GetListIemName(RH.numReturnStatus),0,RH.numReturnStatus,''
		 	 FROM ReturnHeader RH where RH.numDomainID = @numDomainID AND RH.numDivisionId=@numDivisionId
                        AND RH.tintReturnType = 2
                        AND ((dateadd(minute,-@ClientTimeZoneOffset,RH.dtCreatedDate) between @dtFromDate And @dtToDate))

		 END
		 
		 --Credit Memo
		  IF EXISTS (SELECT 1 FROM #temp WHERE Items=9)
		 BEGIN
		 	INSERT INTO #tempTransaction 
		 	SELECT 9,'Credit Memo',dbo.FormatedDateFromDate(RH.dtCreatedDate,RH.numDomainId),RH.numReturnHeaderID,
		 	RH.vcRMA,ISNULL(RH.monAmount,0),
		 	ISNULL((SELECT SUM(ISNULL(monAppliedAmount,0)) AS monAmtPaid FROM DepositMaster DM WHERE DM.numDomainID=RH.numDomainID AND DM.numReturnHeaderID=RH.numReturnHeaderID),0),
		 	NULL,'','',dbo.GetListIemName(RH.numReturnStatus),0,RH.numReturnStatus,''
		 	 FROM ReturnHeader RH where RH.numDomainID = @numDomainID AND RH.numDivisionId=@numDivisionId
                        AND RH.tintReturnType = 3
                        AND ((dateadd(minute,-@ClientTimeZoneOffset,RH.dtCreatedDate) between @dtFromDate And @dtToDate))
		 END
		 
		 --Refund Receipt
		  IF EXISTS (SELECT 1 FROM #temp WHERE Items=10)
		 BEGIN
		 	INSERT INTO #tempTransaction 
		 	SELECT 10,'Refund Receipt',dbo.FormatedDateFromDate(RH.dtCreatedDate,RH.numDomainId),RH.numReturnHeaderID,
		 	RH.vcRMA,ISNULL(RH.monAmount,0),
		 	ISNULL((SELECT ISNULL(SUM(CH.monAmount),0) FROM dbo.CheckHeader CH WHERE CH.numDomainID=1 AND CH.tintReferenceType=10 AND CH.numReferenceID=RH.numReturnHeaderID AND ISNULL(CH.bitIsPrint,0)=1),0),
		 	NULL,'','',dbo.GetListIemName(RH.numReturnStatus),0,RH.numReturnStatus,''
		 	 FROM ReturnHeader RH where RH.numDomainID = @numDomainID AND RH.numDivisionId=@numDivisionId
                        AND RH.tintReturnType = 4
                        AND ((dateadd(minute,-@ClientTimeZoneOffset,RH.dtCreatedDate) between @dtFromDate And @dtToDate))
		 END
		 
		 --Bills
		  IF EXISTS (SELECT 1 FROM #temp WHERE Items=11)
		 BEGIN
		 	INSERT INTO #tempTransaction 
		 	SELECT 11,'Bills',dbo.FormatedDateFromDate(BH.dtCreatedDate,BH.numDomainId),BH.numBillID,
		 	'Bill-' + CAST(BH.numBillID AS VARCHAR(18)),ISNULL(BH.monAmountDue,0),
		 	ISNULL(BH.monAmtPaid,0),
		 	NULL,BH.vcMemo,BH.vcReference,'',0,0,''
		 	 FROM BillHeader BH where BH.numDomainID = @numDomainID AND BH.numDivisionId=@numDivisionId
                        AND ((dateadd(minute,-@ClientTimeZoneOffset,BH.dtCreatedDate) between @dtFromDate And @dtToDate))
						AND ISNULL(BH.[bitLandedCost],0) = 0

		 END
		 
		  --Bills Landed Cost
		  IF EXISTS (SELECT 1 FROM #temp WHERE Items=16)
		 BEGIN
		 	INSERT INTO #tempTransaction 
		 	SELECT 16,'Landed Cost',dbo.FormatedDateFromDate(BH.dtCreatedDate,BH.numDomainId),BH.[numOppId],
		 	'Landed Cost Bill-' + CAST(BH.numBillID AS VARCHAR(18)),ISNULL(BH.monAmountDue,0),
		 	ISNULL(BH.monAmtPaid,0),
		 	NULL,BH.vcMemo,BH.vcReference,'',0,0,''
		 	 FROM BillHeader BH where BH.numDomainID = @numDomainID AND BH.numDivisionId=@numDivisionId
                        AND ((dateadd(minute,-@ClientTimeZoneOffset,BH.dtCreatedDate) between @dtFromDate And @dtToDate))
						AND ISNULL(BH.[bitLandedCost],0) = 1
		 END

		 --UnApplied Bill Payments
		  IF EXISTS (SELECT 1 FROM #temp WHERE Items=12)
		 BEGIN
		 	INSERT INTO #tempTransaction 
		 	SELECT 12,'Unapplied Bill Payments',dbo.FormatedDateFromDate(BPH.dtCreateDate,BPH.numDomainId),BPH.numBillPaymentID,
		 	CASE WHEN ISNULL(numReturnHeaderID,0)>0 THEN 'Return Credit #' + CAST(numReturnHeaderID AS VARCHAR(20)) ELSE 'Unapplied Bill Payment' END
			,ISNULL(BPH.monPaymentAmount,0),
		 	ISNULL(BPH.monAppliedAmount,0),
		 	NULL,'','','',ISNULL(numReturnHeaderID,0),0,''
		 	 FROM BillPaymentHeader BPH where BPH.numDomainID = @numDomainID AND BPH.numDivisionId=@numDivisionId
                        AND ((dateadd(minute,-@ClientTimeZoneOffset,BPH.dtCreateDate) between @dtFromDate And @dtToDate))
                        AND (ISNULL(monPaymentAmount,0) - ISNULL(monAppliedAmount,0)) > 0
		 END
		 
		 --Checks
		  IF EXISTS (SELECT 1 FROM #temp WHERE Items=13)
		 BEGIN
		 	INSERT INTO #tempTransaction 
		 	SELECT 13,'Checks',dbo.FormatedDateFromDate(CH.dtCreatedDate,CH.numDomainId),CH.numCheckHeaderID,
		 	CASE CH.tintReferenceType WHEN 1 THEN 'Checks' 
				  WHEN 8 THEN 'Bill Payment'
				  WHEN 10 THEN 'RMA'
				  WHEN 11 THEN 'Payroll'
				  ELSE 'Check' END + CASE WHEN ISNULL(CH.numCheckNo,0) > 0 THEN ' - #' + CAST(CH.numCheckNo AS VARCHAR(18)) ELSE '' END 
			,ISNULL(CH.monAmount,0),
		 	CASE WHEN ISNULL(bitIsPrint,0)=1 THEN ISNULL(CH.monAmount,0) ELSE 0 END,
		 	NULL,CH.vcMemo,'','',0,0,''
		 	 FROM dbo.CheckHeader CH where CH.numDomainID = @numDomainID AND CH.numDivisionId=@numDivisionId
		 				--AND ch.tintReferenceType=1
                        AND ((dateadd(minute,-@ClientTimeZoneOffset,CH.dtCreatedDate) between @dtFromDate And @dtToDate))

		 END
		 
		 --Deposits
		  IF EXISTS (SELECT 1 FROM #temp WHERE Items=14)
		 BEGIN
		 	INSERT INTO #tempTransaction 
		 	SELECT 14,'Deposits',dbo.FormatedDateFromDate(DM.dtCreationDate,DM.numDomainId),DM.numDepositId,
		 	'Deposit',
		 	ISNULL(DM.monDepositAmount,0),
		 	ISNULL(DM.monAppliedAmount,0),
		 	NULL,DM.vcMemo,DM.vcReference,'',0,tintDepositePage,''
		 	 FROM DepositMaster DM where DM.numDomainID = @numDomainID AND DM.numDivisionId=@numDivisionId
                        AND ((dateadd(minute,-@ClientTimeZoneOffset,DM.dtCreationDate) between @dtFromDate And @dtToDate))
                        AND tintDepositePage IN (1,2)
		 END
		 
		 --Unapplied Payments
		  IF EXISTS (SELECT 1 FROM #temp WHERE Items=15)
		 BEGIN
			INSERT INTO #tempTransaction 
		 	SELECT 15,'Unapplied Payments',dbo.FormatedDateFromDate(DM.dtCreationDate,DM.numDomainId),DM.numDepositId,
		 	CASE WHEN tintDepositePage=3 THEN 'Credit Memo #' + CAST(numReturnHeaderID AS VARCHAR(20)) ELSE 'Unapplied Payment' END,
		 	ISNULL(DM.monDepositAmount,0),
		 	ISNULL(DM.monAppliedAmount,0),
		 	NULL,DM.vcMemo,DM.vcReference,'',ISNULL(numReturnHeaderID,0),tintDepositePage,''
		 	 FROM DepositMaster DM where DM.numDomainID = @numDomainID AND DM.numDivisionId=@numDivisionId
                        AND ((dateadd(minute,-@ClientTimeZoneOffset,DM.dtCreationDate) between @dtFromDate And @dtToDate))
                        AND (ISNULL(monDepositAmount,0) - ISNULL(monAppliedAmount,0)) > 0 AND tintDepositePage IN(2,3)
		 END
		 
		 DECLARE  @firstRec  AS INTEGER
		 DECLARE  @lastRec  AS INTEGER

         SET @firstRec = (@CurrentPage - 1) * @PageSize
		 SET @lastRec = (@CurrentPage * @PageSize + 1)
		 SET @TotRecs = (SELECT COUNT(*) FROM   #tempTransaction)
         
         SELECT * FROM 
         (SELECT intTransactionType ,vcTransactionType ,dbo.FormatedDateFromDate(dtCreatedDate,@numDomainID) AS [dtCreatedDate],numRecordID ,
				vcRecordName,monTotalAmount,monPaidAmount,dtDueDate E,vcMemo,vcDescription,
				vcStatus ,numRecordID2 ,numStatus, vcBizDocId ,
				ROW_NUMBER() OVER(ORDER BY dtCreatedDate) AS RowNumber FROM #tempTransaction) a
          WHERE RowNumber > @firstRec and RowNumber < @lastRec order BY  dtCreatedDate,intTransactionType
          
		 DROP TABLE #tempTransaction
		 
		 DROP TABLE #temp
    END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetReportModuleGroupFieldMaster')
DROP PROCEDURE USP_GetReportModuleGroupFieldMaster
GO
CREATE PROCEDURE [dbo].[USP_GetReportModuleGroupFieldMaster]     
    @numDomainID numeric(18, 0),
	@numReportID numeric(18, 0)
as                 

--Get numReportModuleGroupID
DECLARE @numReportModuleGroupID AS NUMERIC(18,0);SET @numReportModuleGroupID=0
SELECT @numReportModuleGroupID=numReportModuleGroupID FROM dbo.ReportListMaster WHERE numDomainID=@numDomainID AND numReportID=@numReportID


IF @numReportModuleGroupID>0
BEGIN
CREATE TABLE #tempField(numReportFieldGroupID NUMERIC,vcFieldGroupName VARCHAR(100),numFieldID NUMERIC,
vcFieldName NVARCHAR(50),vcDbColumnName NVARCHAR(50),vcOrigDbColumnName NVARCHAR(50),vcFieldDataType CHAR(1),
vcAssociatedControlType NVARCHAR(50),vcListItemType VARCHAR(3),numListID NUMERIC,bitCustom BIT,
bitAllowSorting BIT,bitAllowGrouping BIT,bitAllowAggregate BIT,bitAllowFiltering BIT,vcLookBackTableName NVARCHAR(50))

--Regular Fields
INSERT INTO #tempField
	SELECT RFGM.numReportFieldGroupID,RFGM.vcFieldGroupName,RGMM.numFieldID,RGMM.vcFieldName,
		   DFM.vcDbColumnName,DFM.vcOrigDbColumnName,RGMM.vcFieldDataType,RGMM.vcAssociatedControlType,
		   ISNULL(DFM.vcListItemType,''),ISNULL(DFM.numListID,0),CAST(0 AS BIT) AS bitCustom,RGMM.bitAllowSorting,RGMM.bitAllowGrouping,
		   RGMM.bitAllowAggregate,RGMM.bitAllowFiltering,DFM.vcLookBackTableName
		FROM ReportFieldGroupMaster RFGM 
			JOIN ReportModuleGroupFieldMappingMaster RFMM ON RFGM.numReportFieldGroupID=RFMM.numReportFieldGroupID
			JOIN ReportFieldGroupMappingMaster RGMM ON RGMM.numReportFieldGroupID=RFGM.numReportFieldGroupID
			JOIN dbo.DycFieldMaster DFM ON DFM.numFieldId=RGMM.numFieldID
		WHERE RFMM.numReportModuleGroupID=@numReportModuleGroupID AND RFGM.bitActive=1
			AND ISNULL(RFGM.bitCustomFieldGroup,0)=0


IF @numReportModuleGroupID=9
BEGIN
INSERT INTO #tempField
SELECT 25 AS numReportFieldGroupID,'BizDocs Items Tax Field' AS vcFieldGroupName,TI.numTaxItemID,TI.vcTaxName,
		   vcTaxName AS vcDbColumnName,vcTaxName AS vcOrigDbColumnName,'M' AS vcFieldDataType,'TextBox' AS vcAssociatedControlType,
		   '' AS vcListItemType,0 AS numListID,CAST(0 AS BIT) AS bitCustom,0 AS bitAllowSorting,0 AS bitAllowGrouping,
		   1 AS bitAllowAggregate,1 AS bitAllowFiltering,'TaxItems' AS vcLookBackTableName
from TaxItems TI 
where TI.numDomainID =@numDomainID
UNION
SELECT 25 AS numReportFieldGroupID,'BizDocs Items Tax Field' AS vcFieldGroupName,0 AS numTaxItemID,'Sales Tax' AS vcTaxName,
		   'Sales Tax' AS vcDbColumnName,'Sales Tax' AS vcOrigDbColumnName,'M' AS vcFieldDataType,'TextBox' AS vcAssociatedControlType,
		   '' AS vcListItemType,0 AS numListID,CAST(0 AS BIT) AS bitCustom,0 AS bitAllowSorting,0 AS bitAllowGrouping,
		   1 AS bitAllowAggregate,1 AS bitAllowFiltering,'TaxItems' AS vcLookBackTableName
END

IF @numReportModuleGroupID=8 OR @numReportModuleGroupID=9
BEGIN
INSERT INTO #tempField
SELECT 26 AS numReportFieldGroupID,'BizDocs Tax Field' AS vcFieldGroupName,TI.numTaxItemID,'Total ' + TI.vcTaxName,
		   'Total ' + vcTaxName AS vcDbColumnName,'Total ' +vcTaxName AS vcOrigDbColumnName,'M' AS vcFieldDataType,'TextBox' AS vcAssociatedControlType,
		   '' AS vcListItemType,0 AS numListID,CAST(0 AS BIT) AS bitCustom,0 AS bitAllowSorting,0 AS bitAllowGrouping,
		   1 AS bitAllowAggregate,1 AS bitAllowFiltering,'TaxItems' AS vcLookBackTableName
from TaxItems TI 
where TI.numDomainID =@numDomainID
UNION
SELECT 26 AS numReportFieldGroupID,'BizDocs Tax Field' AS vcFieldGroupName,0 AS numTaxItemID,'Total Sales Tax' AS vcTaxName,
		   'Total Sales Tax' AS vcDbColumnName,'Total Sales Tax' AS vcOrigDbColumnName,'M' AS vcFieldDataType,'TextBox' AS vcAssociatedControlType,
		   '' AS vcListItemType,0 AS numListID,CAST(0 AS BIT) AS bitCustom,0 AS bitAllowSorting,0 AS bitAllowGrouping,
		   1 AS bitAllowAggregate,1 AS bitAllowFiltering,'TaxItems' AS vcLookBackTableName
UNION
SELECT RFGM.numReportFieldGroupID,RFGM.vcFieldGroupName,ISNULL(CFM.Fld_Id,0) as numFieldID,CFM.Fld_label as vcFieldName,
	   '' as vcDbColumnName,'' AS vcOrigDbColumnName,CASE CFM.Fld_type WHEN 'SelectBox' THEN 'V' WHEN 'DateField' THEN 'D' ELSE 'V' END AS vcFieldDataType,
	   CFM.Fld_type AS vcAssociatedControlType,CASE WHEN CFM.Fld_type='SelectBox' THEN 'LI' ELSE '' END AS vcListItemType,isnull(CFM.numListID,0) as numListID,
	   CAST(1 AS BIT) AS bitCustom,1 AS bitAllowSorting,CASE WHEN CFM.Fld_type='SelectBox' THEN 1 ELSE 0 END AS bitAllowGrouping,
	   0 AS bitAllowAggregate,1 AS bitAllowFiltering,RFGM.vcCustomTableName AS vcLookBackTableName
FROM ReportFieldGroupMaster RFGM 
	 JOIN ReportModuleGroupFieldMappingMaster RFMM ON RFGM.numReportFieldGroupID=RFMM.numReportFieldGroupID
	 JOIN CFW_Fld_Master CFM ON CFM.Grp_id=RFGM.numGroupID 
	 LEFT JOIN CFw_Grp_Master CGM ON CFM.subgrp=CGM.Grp_id 
	 --JOIN CFW_Loc_Master L on CFM.GRP_ID=L.Loc_Id
	 WHERE RFMM.numReportModuleGroupID IN (18,19) AND RFGM.bitActive=1
			AND ISNULL(RFGM.bitCustomFieldGroup,0)=1 AND ISNULL(RFGM.numGroupID,0)>0
			AND CFM.numDomainID=@numDomainID  
END

IF @numReportModuleGroupID=19 OR @numReportModuleGroupID=23
BEGIN
INSERT INTO #tempField
SELECT RFGM.numReportFieldGroupID,RFGM.vcFieldGroupName,ISNULL(CFM.Fld_Id,0) as numFieldID,CFM.Fld_label as vcFieldName,
	   '' as vcDbColumnName,'' AS vcOrigDbColumnName,CASE CFM.Fld_type WHEN 'SelectBox' THEN 'V' WHEN 'DateField' THEN 'D' ELSE 'V' END AS vcFieldDataType,
	   CFM.Fld_type AS vcAssociatedControlType,CASE WHEN CFM.Fld_type='SelectBox' THEN 'LI' ELSE '' END AS vcListItemType,isnull(CFM.numListID,0) as numListID,
	   CAST(1 AS BIT) AS bitCustom,1 AS bitAllowSorting,CASE WHEN CFM.Fld_type='SelectBox' THEN 1 ELSE 0 END AS bitAllowGrouping,
	   0 AS bitAllowAggregate,1 AS bitAllowFiltering,RFGM.vcCustomTableName AS vcLookBackTableName
FROM ReportFieldGroupMaster RFGM 
	 JOIN ReportModuleGroupFieldMappingMaster RFMM ON RFGM.numReportFieldGroupID=RFMM.numReportFieldGroupID
	 JOIN CFW_Fld_Master CFM ON CFM.Grp_id=RFGM.numGroupID 
	 LEFT JOIN CFw_Grp_Master CGM ON CFM.subgrp=CGM.Grp_id 
	 --JOIN CFW_Loc_Master L on CFM.GRP_ID=L.Loc_Id
	 WHERE RFMM.numReportModuleGroupID IN (18,19) AND RFGM.bitActive=1
			AND ISNULL(RFGM.bitCustomFieldGroup,0)=1 AND ISNULL(RFGM.numGroupID,0)>0
			AND CFM.numDomainID=@numDomainID  
END

--Custom Fields			
INSERT INTO #tempField
SELECT RFGM.numReportFieldGroupID,RFGM.vcFieldGroupName,ISNULL(CFM.Fld_Id,0) as numFieldID,CFM.Fld_label as vcFieldName,
	   '' as vcDbColumnName,'' AS vcOrigDbColumnName,CASE CFM.Fld_type WHEN 'SelectBox' THEN 'V' WHEN 'DateField' THEN 'D' ELSE 'V' END AS vcFieldDataType,
	   CFM.Fld_type AS vcAssociatedControlType,CASE WHEN CFM.Fld_type='SelectBox' THEN 'LI' ELSE '' END AS vcListItemType,isnull(CFM.numListID,0) as numListID,
	   CAST(1 AS BIT) AS bitCustom,1 AS bitAllowSorting,CASE WHEN CFM.Fld_type='SelectBox' THEN 1 ELSE 0 END AS bitAllowGrouping,
	   0 AS bitAllowAggregate,1 AS bitAllowFiltering,RFGM.vcCustomTableName AS vcLookBackTableName
FROM ReportFieldGroupMaster RFGM 
	 JOIN ReportModuleGroupFieldMappingMaster RFMM ON RFGM.numReportFieldGroupID=RFMM.numReportFieldGroupID
	 JOIN CFW_Fld_Master CFM ON CFM.Grp_id=RFGM.numGroupID 
	 LEFT JOIN CFw_Grp_Master CGM ON CFM.subgrp=CGM.Grp_id 
	 --JOIN CFW_Loc_Master L on CFM.GRP_ID=L.Loc_Id
	 WHERE RFMM.numReportModuleGroupID=@numReportModuleGroupID AND RFGM.bitActive=1
			AND ISNULL(RFGM.bitCustomFieldGroup,0)=1 AND ISNULL(RFGM.numGroupID,0)>0
			AND CFM.numDomainID=@numDomainID  

SELECT * FROM #tempField ORDER BY numReportFieldGroupID

DROP TABLE #tempField
		
END

GO
/****** Object:  StoredProcedure [dbo].[USP_GetSites]    Script Date: 08/08/2009 12:42:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetSites')
DROP PROCEDURE USP_GetSites
GO
CREATE PROCEDURE [dbo].[USP_GetSites]
          @numSiteID   NUMERIC(9)  = 0,
          @numDomainID NUMERIC(9)
AS
  SELECT [numSiteID],
         [vcSiteName],
         [vcDescription],
         [vcHostName],
         [numDomainID],
         bitIsActive,
		 CASE WHEN [bitIsActive] = 1 THEN 'Inactivate'
		 ELSE 'Activate'
		 END AS IsActive,
		 ISNULL(vcLiveURL,'') vcLiveURL,
		 CASE LEN(ISNULL(vcLiveURL,'')) WHEN 0 THEN  ISNULL(vcHostName ,'') +'.bizautomation.com' ELSE vcLiveURL END vcPreviewURL,ISNULL(numCurrencyID,0) AS numCurrencyID,
		 ISNULL(bitOnePageCheckout,0) bitOnePageCheckout,
		 ISNULL(tintRateType,0) AS tintRateType,
		(SELECT STUFF((SELECT ',' + CONVERT(VARCHAR(50) , numCategoryID)  FROM dbo.SiteCategories WHERE numSiteID = @numSiteID FOR XML PATH('')), 1, 1, '')) AS vcItemCategories,
		bitHtml5,
		vcMetaTags,
		bitSSLRedirect
  FROM   Sites
  WHERE  ([numSiteID] = @numSiteID
           OR @numSiteID = 0)
         AND [numDomainID] = @numDomainID


/****** Object:  StoredProcedure [dbo].[USP_GetWareHouseItems]    Script Date: 07/26/2008 16:18:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec USP_GetWareHouseItems 6                            
--created by anoop jayaraj                              
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getwarehouseitems')
DROP PROCEDURE usp_getwarehouseitems
GO
CREATE PROCEDURE [dbo].[USP_GetWareHouseItems]                              
@numItemCode as numeric(9)=0,
@byteMode as tinyint=0                              
as                              
                              
                          
                              
declare @bitSerialize as bit                      
declare @str as nvarchar(max)                 
declare @str1 as nvarchar(max)               
declare @ColName as varchar(25)                      
set @str=''                       
                        
DECLARE @bitKitParent BIT
declare @numItemGroupID as numeric(9)                        
                        
select @numItemGroupID=numItemGroup,@bitSerialize=CASE WHEN bitSerialized=0 THEN bitLotNo ELSE bitSerialized END,
@bitKitParent = ( CASE WHEN ISNULL(bitKitParent,0) = 1
                               AND ISNULL(bitAssembly,0) = 1 THEN 0
                          WHEN ISNULL(bitKitParent,0) = 1 THEN 1
                          ELSE 0
                     END ) from Item where numItemCode=@numItemCode                        
if @bitSerialize=1 set @ColName='numWareHouseItmsDTLID,1'              
else set @ColName='numWareHouseItemID,0'              
              
--Create a Temporary table to hold data                                                            
create table #tempTable ( ID INT IDENTITY PRIMARY KEY,                                                                      
 numCusFlDItemID numeric(9)                                                         
 )                         
                        
insert into #tempTable                         
(numCusFlDItemID)                                                            
select distinct(numOppAccAttrID) from ItemGroupsDTL where numItemGroupID=@numItemGroupID and tintType=2                       
                          
                 
 declare @ID as numeric(9)                        
 declare @numCusFlDItemID as varchar(20)                        
 declare @fld_label as varchar(100),@fld_type as varchar(100)                        
 set @ID=0                        
 select top 1 @ID=ID,@numCusFlDItemID=numCusFlDItemID,@fld_label=fld_label,@fld_type=fld_type from #tempTable                         
 join CFW_Fld_Master on numCusFlDItemID=Fld_ID                        
                         
 while @ID>0                        
 begin                        
                          
   set @str=@str+',  dbo.GetCustFldItems('+@numCusFlDItemID+',9,'+@ColName+') as ['+ @fld_label+']'
	
	IF @byteMode=1                                        
		set @str=@str+',  dbo.GetCustFldItemsValue('+@numCusFlDItemID+',9,'+@ColName+','''+@fld_type+''') as ['+ @fld_label+'Value]'                                        
                          
   select top 1 @ID=ID,@numCusFlDItemID=numCusFlDItemID,@fld_label=fld_label from #tempTable                         
   join CFW_Fld_Master on numCusFlDItemID=Fld_ID and ID >@ID                        
   if @@rowcount=0 set @ID=0                        
                          
 end                        
                       
--      

DECLARE @KitOnHand AS NUMERIC(9);SET @KitOnHand=0

--IF @bitKitParent=1
	--SELECT @KitOnHand=ISNULL(dbo.fn_GetKitInventory(@numItemCode),0)          
  
set @str1='select '

IF @byteMode=1                                        
		set @str1 =@str1 +'I.numItemCode,'
	
set @str1 =@str1 + 'numWareHouseItemID,isnull(vcWarehouse,'''') + '': '' + isnull(WL.vcLocation,'''') vcWarehouse,W.numWareHouseID,
Case when @bitKitParent=1 then ISNULL(dbo.fn_GetKitInventoryByWarehouse(I.numItemCode, W.numWareHouseID),0) else isnull(numOnHand,0) end as [OnHand],
CAST((dbo.fn_UOMConversion(ISNULL(I.numBaseUnit,0),I.numItemCode,I.numDomainId,ISNULL(I.numPurchaseUnit,0)) * Case when @bitKitParent=1 THEN ISNULL(dbo.fn_GetKitInventoryByWarehouse(I.numItemCode, W.numWareHouseID),0) ELSE isnull(numOnHand,0) end) as numeric(18,2)) as PurchaseOnHand,
CAST((dbo.fn_UOMConversion(ISNULL(I.numBaseUnit,0),I.numItemCode,I.numDomainId,ISNULL(I.numSaleUnit,0)) * Case when @bitKitParent=1 THEN ISNULL(dbo.fn_GetKitInventoryByWarehouse(I.numItemCode, W.numWareHouseID),0) ELSE isnull(numOnHand,0) end) as numeric(18,2)) as SalesOnHand,
Case when @bitKitParent=1 THEN 0 ELSE isnull(numReorder,0) END as [Reorder] 
,Case when @bitKitParent=1 THEN 0 ELSE isnull(numOnOrder,0) END as [OnOrder]
,Case when @bitKitParent=1 THEN 0 ELSE isnull(numAllocation,0) END as [Allocation] 
,Case when @bitKitParent=1 THEN 0 ELSE isnull(numBackOrder,0) END as [BackOrder]
,0 as Op_Flag , isnull(WL.vcLocation,'''') Location,isnull(WL.numWLocationID,0) as numWLocationID,
round(isnull(monWListPrice,0),2) Price,isnull(vcWHSKU,'''') as SKU,isnull(vcBarCode,'''') as BarCode '+ case when @bitSerialize=1 then  ' ' else  @str end +'                   
,(SELECT CASE WHEN ISNULL(subI.bitKitParent,0) = 1 AND ISNULL((SELECT COUNT(*) FROM OpportunityItems 
															   WHERE OpportunityItems.numItemCode = WareHouseItems.numItemID
															   AND WareHouseItems.numWareHouseItemID = OpportunityItems.numWarehouseItmsID),0) = 0 THEN 1
			ELSE 0 
	    END 
FROM Item subI WHERE subI.numItemCode = WareHouseItems.numItemID) [IsDeleteKitWarehouse],@bitKitParent [bitKitParent]
from WareHouseItems                           
join Warehouses W                             
on W.numWareHouseID=WareHouseItems.numWareHouseID 
left join WarehouseLocation WL on WL.numWLocationID =WareHouseItems.numWLocationID
join Item I on I.numItemCode=WareHouseItems.numItemID and WareHouseItems.numDomainId=I.numDomainId
where numItemID='+convert(varchar(15),@numItemCode)                  
   
print(@str1)           

EXECUTE sp_executeSQL @str1, N'@bitKitParent bit',@bitKitParent
                       
                        
set @str1='select numWareHouseItmsDTLID,WDTL.numWareHouseItemID, vcSerialNo,0 as Op_Flag,WDTL.vcComments as Comments,WDTL.numQty,WDTL.numQty as OldQty,W.vcWarehouse '+ case when @bitSerialize=1 then @str else '' end +'
,WDTL.dExpirationDate 
from WareHouseItmsDTL WDTL                             
join WareHouseItems WI                             
on WDTL.numWareHouseItemID=WI.numWareHouseItemID                              
join Warehouses W                             
on W.numWareHouseID=WI.numWareHouseID  
where (tintStatus is null or tintStatus=0)  and  numItemID='+ convert(varchar(15),@numItemCode)                          
print @str1                       
exec (@str1)                       
                      
                      
select Fld_label,fld_id,fld_type,numlistid,vcURL from #tempTable                         
 join CFW_Fld_Master on numCusFlDItemID=Fld_ID                      
drop table #tempTable


-----------------For Kendo UI testting purpose
--create table #tempColumnConfig ( field varchar(100),                                                                      
-- title varchar(100) , format varchar(100)                                                      
-- )  
--
--insert into #tempColumnConfig
--	select 'vcWarehouse','WareHouse','' union all
--	select 'OnHand','On Hand','' union all
--	select 'Reorder','Re Order','' union all
--	select 'Allocation','Allocation','' union all
--	select 'BackOrder','Back Order','' union all
--	select 'Price','Price','{0:c}'
--
--select * from #tempColumnConfig
--
--drop table #tempColumnConfig

-----------------

--set @str1='select case when COUNT(*)=1 then MIN(numWareHouseItmsDTLID) else 0 end as numWareHouseItmsDTLID,MIN(WDTL.numWareHouseItemID) as numWareHouseItemID, vcSerialNo,0 as Op_Flag,WDTL.vcComments as Comments'+ case when @bitSerialize=1 then @str else '' end +',W.vcWarehouse,COUNT(*) Qty
--from WareHouseItmsDTL WDTL                             
--join WareHouseItems WI                             
--on WDTL.numWareHouseItemID=WI.numWareHouseItemID                              
--join Warehouses W                             
--on W.numWareHouseID=WI.numWareHouseID  
--where (tintStatus is null or tintStatus=0)  and  numItemID='+ convert(varchar(15),@numItemCode) +'
--GROUP BY vcSerialNo,WDTL.vcComments,W.vcWarehouse'
--                          
--print @str1                       
--exec (@str1)                       
              
GO
--SELECT * FROM Category
--- Alterd By Chintan Prajapati
-- EXEC dbo.USP_ItemsForECommerce 9,60,1,1,15,0,''
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ItemsForECommerce' ) 
    DROP PROCEDURE USP_ItemsForECommerce
GO
CREATE PROCEDURE [dbo].[USP_ItemsForECommerce]
    @numSiteID AS NUMERIC(9) = 0,
    @numCategoryID AS NUMERIC(18) = 0,
    @numWareHouseID AS NUMERIC(9) = 0,
    @SortBy NVARCHAR(MAX) = 0,
    @CurrentPage INT,
    @PageSize INT,
    @TotRecs INT = 0 OUTPUT,
    @SearchText AS NVARCHAR(MAX) = '',--Comma seperated item ids
    @FilterQuery AS NVARCHAR(MAX) = '',----Comma seperated Attributes Ids
    @FilterRegularCondition AS NVARCHAR(MAX),
    --@FilterCustomCondition AS NVARCHAR(MAX),
    @FilterCustomFields AS NVARCHAR(MAX),
    @FilterCustomWhere AS NVARCHAR(MAX)
   
AS 
    BEGIN
		
		DECLARE @numDomainID AS NUMERIC(9)
		SELECT  @numDomainID = numDomainID
        FROM    [Sites]
        WHERE   numSiteID = @numSiteID
        
        DECLARE @tintDisplayCategory AS TINYINT
		SELECT @tintDisplayCategory = ISNULL(bitDisplayCategory,0) FROM dbo.eCommerceDTL WHERE numSiteID = @numSiteId
		PRINT @tintDisplayCategory 
		
		CREATE TABLE #tmpItemCat(numCategoryID NUMERIC(18,0))
		CREATE TABLE #tmpItemCode(numItemCode NUMERIC(18,0))
		IF LEN(@FilterCustomFields) > 0 AND LEN(@FilterCustomWhere) > 0
		BEGIN
--			DECLARE @strCustomSql AS NVARCHAR(MAX)
--			SET @strCustomSql = 'INSERT INTO #tmpItemCode(numItemCode)
--								 SELECT DISTINCT t2.RecId FROM CFW_Fld_Master t1 
--								 JOIN CFW_FLD_Values_Item AS t2 ON t1.Fld_ID = t2.Fld_ID 
--								 WHERE t1.Grp_id = 5 
--								 AND t1.numDomainID = (SELECT numDomainID FROM dbo.Sites WHERE numSiteID = ' + CONVERT(VARCHAR(10),@numSiteID) + ') 
--								 AND t1.fld_type <> ''Link'' AND ' + @FilterCustomCondition
			
			DECLARE @FilterCustomFieldIds AS NVARCHAR(MAX)
			SET @FilterCustomFieldIds = Replace(@FilterCustomFields,'[','')
			SET @FilterCustomFieldIds = Replace(@FilterCustomFieldIds,']','')
			
			DECLARE @strCustomSql AS NVARCHAR(MAX)
			SET @strCustomSql = 'INSERT INTO #tmpItemCode(numItemCode)
								 SELECT DISTINCT T.RecId FROM 
															(
																SELECT * FROM 
																	(
																		SELECT  t2.RecId, t1.fld_id, t2.Fld_Value FROM CFW_Fld_Master t1 
																		JOIN CFW_FLD_Values_Item AS t2 ON t1.Fld_ID = t2.Fld_ID 
																		WHERE t1.Grp_id = 5 
																		AND t1.numDomainID = ' + CONVERT(VARCHAR(10),@numDomainID) + '
																		AND t1.fld_type <> ''Link'' 
																		AND t1.fld_id IN (' + @FilterCustomFieldIds + ')
																	) p  PIVOT (MAX([Fld_Value]) FOR fld_id IN (' + @FilterCustomFields + ')) AS pvt 
															) T  WHERE 1=1 AND ' + @FilterCustomWhere
								 
			PRINT @strCustomSql
			EXEC SP_EXECUTESQL @strCustomSql								 					 
		END
		
        DECLARE @strSQL NVARCHAR(MAX)
        DECLARE @firstRec AS INTEGER
        DECLARE @lastRec AS INTEGER
        DECLARE @Where NVARCHAR(MAX)
        DECLARE @SortString NVARCHAR(MAX)
        DECLARE @searchPositionColumn NVARCHAR(MAX)
        DECLARE @row AS INTEGER
        DECLARE @data AS VARCHAR(100)
        DECLARE @dynamicFilterQuery NVARCHAR(MAX)
        DECLARE @checkFldType VARCHAR(100)
        DECLARE @count NUMERIC(9,0)
        DECLARE @whereNumItemCode NVARCHAR(MAX)
        DECLARE @filterByNumItemCode VARCHAR(100)
        
		SET @SortString = ''
        set @searchPositionColumn = ''
        SET @strSQL = ''
		SET @whereNumItemCode = ''
		SET @dynamicFilterQuery = ''
		 
        SET @firstRec = ( @CurrentPage - 1 ) * @PageSize
        SET @lastRec = ( @CurrentPage * @PageSize + 1 )
                
        DECLARE @bitAutoSelectWarehouse AS BIT
		SELECT @bitAutoSelectWarehouse = ISNULL([ECD].[bitAutoSelectWarehouse],0) FROM [dbo].[eCommerceDTL] AS ECD WHERE [ECD].[numSiteId] = @numSiteID
		AND [ECD].[numDomainId] = @numDomainID

		DECLARE @vcWarehouseIDs AS VARCHAR(1000)
        IF (@numWareHouseID = 0 OR @bitAutoSelectWarehouse = 1)
            BEGIN				
				IF @bitAutoSelectWarehouse = 1
					BEGIN
						SELECT @vcWarehouseIDs = ISNULL( STUFF((SELECT DISTINCT ',' + CAST(numWarehouseID AS VARCHAR(10)) FROM [dbo].[Warehouses] WHERE [Warehouses].[numDomainID] = @numDomainID FOR XML PATH('')),1, 1, '') , '') 
					END
				
				ELSE
					BEGIN
						SELECT  @numWareHouseID = ISNULL(numDefaultWareHouseID,0)
						FROM    [eCommerceDTL]
						WHERE   [numDomainID] = @numDomainID	

						SET @vcWarehouseIDs = @numWareHouseID
					END
            END
		ELSE
		BEGIN
			SET @vcWarehouseIDs = @numWareHouseID
		END

		PRINT @vcWarehouseIDs

		--PRIAMRY SORTING USING CART DROPDOWN
		DECLARE @Join AS NVARCHAR(MAX)
		DECLARE @SortFields AS NVARCHAR(MAX)
		DECLARE @vcSort AS VARCHAR(MAX)

		SET @Join = ''
		SET @SortFields = ''
		SET @vcSort = ''

		IF @SortBy = ''
			BEGIN
				SET @SortString = ' vcItemName Asc '
			END
		ELSE IF CHARINDEX('~0',@SortBy) > 0 OR CHARINDEX('~1',@SortBy) > 0
			BEGIN
				DECLARE @fldID AS NUMERIC(18,0)
				DECLARE @RowNumber AS VARCHAR(MAX)
				
				SELECT * INTO #fldValues FROM dbo.SplitIDsWithPosition(@SortBy,'~')
				SELECT @RowNumber = RowNumber, @fldID = id FROM #fldValues WHERE RowNumber = 1
				SELECT @vcSort = CASE WHEN Id = 0 THEN 'ASC' ELSE 'DESC' END FROM #fldValues WHERE RowNumber = 2
				
				SET @Join = @Join + ' LEFT JOIN CFW_FLD_Values_Item CFVI' + @RowNumber + ' ON I.numItemCode = CFVI' + @RowNumber + '.RecID AND CFVI' + @RowNumber + '.fld_Id = ' + CONVERT(VARCHAR(10),@fldID)
				---SET @SortString = 'CFVI' + @RowNumber + '.fld_Value' 
				SET @SortString = '[fld_Value' + @RowNumber + '] ' --+ @vcSort
				SET @SortFields = ', CFVI' + @RowNumber + '.fld_Value [fld_Value' + @RowNumber + ']'
			END
		ELSE
			BEGIN
				--SET @SortString = @SortBy	
				
				IF CHARINDEX('monListPrice',@SortBy) > 0
				BEGIN
					DECLARE @bitSortPriceMode AS BIT
					SELECT @bitSortPriceMode = ISNULL(bitSortPriceMode,0) FROM dbo.eCommerceDTL WHERE numSiteId = @numSiteID
					PRINT @bitSortPriceMode
					IF @bitSortPriceMode = 1
					BEGIN
						SET @SortString = ' '
					END
					ELSE
					BEGIN
						SET @SortString = @SortBy	
					END
				END
				ELSE
				BEGIN
					SET @SortString = @SortBy	
				END
				
			END	

		--SECONDARY SORTING USING DEFAULT SETTINGS FROM ECOMMERCE
		SELECT ROW_NUMBER() OVER (ORDER BY Custom,vcFieldName) AS [RowID],* INTO #tempSort FROM (
		SELECT  CONVERT(VARCHAR(9),numFieldID)+'~0' AS numFieldID,
				ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName,
				0 AS Custom,
				vcDbColumnName 
		FROM View_DynamicColumns
		WHERE numFormID = 84 
		AND ISNULL(bitSettingField,0) = 1 
		AND ISNULL(bitDeleted,0)=0 
		AND ISNULL(bitCustom,0) = 0
		AND numDomainID = @numDomainID
					       
		UNION     

		SELECT  CONVERT(VARCHAR(9),numFieldID)+'~1' AS numFieldID ,
				vcFieldName ,
				1 AS Custom,
				'' AS vcDbColumnName
		FROM View_DynamicCustomColumns          
		WHERE numFormID = 84
		AND numDomainID = @numDomainID
		AND grp_id = 5 
		AND vcAssociatedControlType = 'TextBox' 
		AND ISNULL(bitCustom,0) = 1 
		AND tintPageType=1  
		) TABLE1 ORDER BY Custom,vcFieldName
		
		--SELECT * FROM #tempSort
		DECLARE @DefaultSort AS NVARCHAR(MAX)
		DECLARE @DefaultSortFields AS NVARCHAR(MAX)
		DECLARE @DefaultSortJoin AS NVARCHAR(MAX)
		DECLARE @intCnt AS INT
		DECLARE @intTotalCnt AS INT	
		DECLARE @strColumnName AS VARCHAR(100)
		DECLARE @bitCustom AS BIT
		DECLARE @FieldID AS VARCHAR(100)				

		SET @DefaultSort = ''
		SET @DefaultSortFields = ''
		SET @DefaultSortJoin = ''		
		SET @intCnt = 0
		SET @intTotalCnt = ( SELECT COUNT(*) FROM #tempSort )
		SET @strColumnName = ''
		
		IF (SELECT ISNULL(bitEnableSecSorting,0) FROM EcommerceDtl WHERE numSiteID = @numSiteID ) = 1
		BEGIN
			CREATE TABLE #fldDefaultValues(RowNumber INT,Id VARCHAR(1000))

			WHILE(@intCnt < @intTotalCnt)
			BEGIN
				SET @intCnt = @intCnt + 1
				SELECT @FieldID = numFieldID, @strColumnName = vcDbColumnName, @bitCustom = Custom FROM #tempSort WHERE RowID = @intCnt
				
				--PRINT @FieldID
				--PRINT @strColumnName
				--PRINT @bitCustom

				IF @bitCustom = 0
					BEGIN
						SET @DefaultSort =  @strColumnName + ',' + @DefaultSort 
					END

				ELSE
					BEGIN
						DECLARE @fldID1 AS NUMERIC(18,0)
						DECLARE @RowNumber1 AS VARCHAR(MAX)
						DECLARE @vcSort1 AS VARCHAR(10)
						DECLARE @vcSortBy AS VARCHAR(1000)
						SET @vcSortBy = CONVERT(VARCHAR(100),@FieldID) + '~' + '0'
						--PRINT @vcSortBy

						INSERT INTO #fldDefaultValues(RowNumber,Id) SELECT RowNumber, id FROM dbo.SplitIDsWithPosition(@vcSortBy,'~')
						SELECT @RowNumber1 = RowNumber + 100 + @intCnt, @fldID1 = id FROM #fldDefaultValues WHERE RowNumber = 1
						SELECT @vcSort1 = 'ASC' 
						
						SET @DefaultSortJoin = @DefaultSortJoin + ' LEFT JOIN CFW_FLD_Values_Item CFVI' + @RowNumber1 + ' ON I.numItemCode = CFVI' + @RowNumber1 + '.RecID AND CFVI' + @RowNumber1 + '.fld_Id = ' + CONVERT(VARCHAR(10),@fldID1)
						SET @DefaultSort = '[fld_Value' + @RowNumber1 + '] ' + ',' + @DefaultSort 
						SET @DefaultSortFields = @DefaultSortFields + ', CFVI' + @RowNumber1 + '.fld_Value [fld_Value' + @RowNumber1 + ']'
					END

			END
			IF LEN(@DefaultSort) > 0
			BEGIN
				SET  @DefaultSort = LEFT(@DefaultSort,LEN(@DefaultSort) - 1)
				SET @SortString = @SortString + ',' 
			END
			--PRINT @SortString
			--PRINT @DefaultSort
			--PRINT @DefaultSortFields
		END
--        IF @SortBy = 1 
--            SET @SortString = ' vcItemName Asc '
--        ELSE IF @SortBy = 2 
--            SET @SortString = ' vcItemName Desc '
--        ELSE IF @SortBy = 3 
--            SET @SortString = ' monListPrice Asc '
--        ELSE IF @SortBy = 4 
--            SET @SortString = ' monListPrice Desc '
--        ELSE IF @SortBy = 5 
--            SET @SortString = ' bintCreatedDate Desc '
--        ELSE IF @SortBy = 6 
--            SET @SortString = ' bintCreatedDate ASC '	
--        ELSE IF @SortBy = 7 --sort by relevance, invokes only when search
--            BEGIN
--                 IF LEN(@SearchText) > 0 
--		            BEGIN
--						SET @SortString = ' SearchOrder ASC ';
--					    set @searchPositionColumn = ' ,SearchItems.RowNumber AS SearchOrder ' 						
--					END
--			END
--        ELSE 
--            SET @SortString = ' vcItemName Asc '
		
                                		
        IF LEN(ISNULL(@SearchText,'')) > 0
			BEGIN
				--PRINT 1
				IF CHARINDEX('SearchOrder',@SortString) > 0
				BEGIN
					SET @searchPositionColumn = ' ,SearchItems.RowNumber AS SearchOrder ' 
				END

				SET @Where = ' INNER JOIN dbo.SplitIDsWithPosition('''+ @SearchText +''','','') AS SearchItems 
                        ON SearchItems.Id = I.numItemCode  and SC.numSiteID = ' + CONVERT(VARCHAR(15), @numSiteID) + ' WHERE ISNULL(IsArchieve,0) = 0'
				END
		ELSE
			BEGIN
				--PRINT 2
				SET @Where = ' WHERE 1=1 '
							+ ' and SC.numSiteID = ' + CONVERT(VARCHAR(15), @numSiteID)
							+ ' AND ISNULL(IsArchieve,0) = 0'
							
				IF @tintDisplayCategory = 1
				BEGIN
					
					;WITH CTE AS(
					SELECT C.numCategoryID,C.tintLevel,C.numDepCategory,CASE WHEN tintLevel=1 THEN 1 ELSE 0 END AS bitChild 
						FROM Category C WHERE (numCategoryID = @numCategoryID OR ISNULL(@numCategoryID,0) = 0) AND numDomainID = @numDomainID
					UNION ALL
					SELECT  C.numCategoryID,C.tintLevel,C.numDepCategory,CTE.bitChild FROM Category C JOIN CTE ON 
					C.numDepCategory=CTE.numCategoryID AND CTE.bitChild=1 AND numDomainID = @numDomainID)
					
					INSERT INTO #tmpItemCat(numCategoryID)												 
					SELECT DISTINCT C.numCategoryID FROM CTE C JOIN dbo.ItemCategory IC ON C.numCategoryID = IC.numCategoryID 
														WHERE numItemID IN ( 
																						
													 						SELECT numItemID FROM WareHouseItems WI
																			INNER JOIN dbo.Item I ON I.numItemCode = WI.numItemID AND I.numDomainID = @numDomainID
																			GROUP BY numItemID 
																			HAVING SUM(WI.numOnHand) > 0 
																			UNION ALL
													 						SELECT numItemID FROM WareHouseItems WI
																			INNER JOIN dbo.Item I ON I.numItemCode = WI.numItemID AND I.numDomainID = @numDomainID
																			GROUP BY numItemID 
																			HAVING SUM(WI.numAllocation) > 0
																		 )		
					SELECT numItemID INTO #tmpOnHandItems FROM 
					(													 					
						SELECT numItemID FROM WareHouseItems WI 
						INNER JOIN dbo.Item I ON I.numItemCode = WI.numItemID AND I.numDomainID = @numDomainID
						GROUP BY numItemID 
						HAVING SUM(WI.numOnHand) > 0
						UNION ALL
						SELECT numItemID FROM WareHouseItems WI 
						INNER JOIN dbo.Item I ON I.numItemCode = WI.numItemID AND I.numDomainID = @numDomainID
						GROUP BY numItemID 
						HAVING SUM(WI.numAllocation) > 0				
					) TABLE1
				END
			END
		
			--SELECT * FROM  #tmpOnHandItems
			--SELECT * FROM  #tmpItemCat
			
----		IF (SELECT COUNT(*) FROM #tmpItemCat) = 0 AND ISNULL(@numCategoryID,0) = 0
----		BEGIN
----			
----			INSERT INTO #tmpItemCat(numCategoryID)
----			SELECT DISTINCT C.numCategoryID FROM Category C
----			JOIN dbo.ItemCategory IC ON C.numCategoryID = IC.numCategoryID 
----							WHERE C.numDepCategory = (SELECT TOP 1 numCategoryID FROM dbo.Category 
----													  WHERE numDomainID = @numDomainID
----													  AND tintLevel = 1)
----							AND tintLevel <> 1
----							AND numDomainID = @numDomainID
----							AND numItemID IN (
----											 	SELECT numItemID FROM WareHouseItems WI
----												INNER JOIN dbo.Item I ON I.numItemCode = WI.numItemID
----												GROUP BY numItemID 
----												HAVING SUM(WI.numOnHand) > 0
----											 )
----			--SELECT * FROM #tmpItemCat								 
----		END
----		ELSE
----		BEGIN
----			INSERT INTO #tmpItemCat(numCategoryID)VALUES(@numCategoryID)
----		END

		       SET @strSQL = @strSQL + ' WITH Items AS 
	                                     ( 
	                                     SELECT   
                                               I.numItemCode,
                                               ROW_NUMBER() OVER ( Partition by I.numItemCode Order by I.numItemCode) RowID,
                                               vcItemName,
                                               I.bintCreatedDate,
                                               vcManufacturer,
                                               C.vcCategoryName,
                                               ISNULL(txtItemDesc,'''') txtItemDesc,
                                               ISNULL(CASE  WHEN I.[charItemType] = ''P''
															THEN CASE WHEN I.bitSerialized = 1
																	  THEN ( UOM * monListPrice )
																	  ELSE ( UOM * ISNULL(W1.[monWListPrice], 0) )
																 END
															ELSE ( UOM * monListPrice )
													  END, 0) monListPrice,
                                               vcSKU,
                                               fltHeight,
                                               fltWidth,
                                               fltLength,
                                               IM.vcPathForImage,
                                               IM.vcPathForTImage,
                                               vcModelID,
                                               fltWeight,
                                               bitFreeShipping,
                                               UOM AS UOMConversionFactor,
												( CASE WHEN charItemType <> ''S'' AND charItemType <> ''N''
													   THEN ( CASE WHEN bitShowInStock = 1
																   THEN ( CASE WHEN ( ISNULL(W.numOnHand,0)<=0) THEN ''<font color=red>Out Of Stock</font>''
																			   WHEN bitAllowBackOrder = 1 THEN ''In Stock''
																			   ELSE ''In Stock'' 
																		  END )
																   ELSE ''''
															  END )
													   ELSE ''''
												  END ) AS InStock, C.vcDescription as CategoryDesc, ISNULL(W1.numWareHouseItemID,0) [numWareHouseItemID] ' 
								               +	@searchPositionColumn + @SortFields + @DefaultSortFields + '
								
                                         FROM      
                                                      Item AS I
                                         INNER JOIN   ItemCategory IC   ON   I.numItemCode = IC.numItemID
                                         LEFT JOIN    ItemImages IM     ON   I.numItemCode = IM.numItemCode AND IM.bitdefault=1 AND IM.bitIsImage = 1
                                         INNER JOIN   Category C        ON   IC.numCategoryID = C.numCategoryID
                                         INNER JOIN   SiteCategories SC ON   IC.numCategoryID = SC.numCategoryID
                                         INNER JOIN   eCommerceDTL E    ON   E.numDomainID = I.numDomainID ' + @Join + ' ' + @DefaultSortJoin + ' 
                                         CROSS APPLY (SELECT SUM(numOnHand) numOnHand FROM WareHouseItems W 
													  WHERE  W.numItemID = I.numItemCode
                                                      AND numWareHouseID IN (SELECT [OutParam] FROM dbo.[SplitString](''' + @vcWarehouseIDs + ''','',''))
                                                      AND W.numDomainID = ' + CONVERT(VARCHAR(10),@numDomainID) + ') AS W  
										 CROSS APPLY (SELECT TOP 1 numWareHouseItemID,monWListPrice FROM WareHouseItems W 
													  WHERE  W.numItemID = I.numItemCode 
													  AND W.numDomainID = ' + CONVERT(VARCHAR(10),@numDomainID) + '
													  AND numWareHouseID IN (SELECT [OutParam] FROM dbo.[SplitString](''' + @vcWarehouseIDs + ''','',''))) AS W1
										 CROSS APPLY (SELECT SUM(numAllocation) numAllocation FROM WareHouseItems W2 
													  WHERE  W2.numItemID = I.numItemCode
                                                      AND numWareHouseID IN (SELECT [OutParam] FROM dbo.[SplitString](''' + @vcWarehouseIDs + ''','',''))
                                                      AND W2.numDomainID = ' + CONVERT(VARCHAR(10),@numDomainID) + ') AS W2  			  
										 CROSS APPLY (SELECT dbo.fn_UOMConversion(ISNULL(I.numSaleUnit, 0),I.numItemCode, I.numDomainId,ISNULL(I.numBaseUnit, 0)) AS UOM) AS UOM '

			IF LEN(@FilterCustomFields) > 0 AND LEN(@FilterCustomWhere) > 0
			BEGIN
				SET @strSQL = @strSQL + ' INNER JOIN #tmpItemCode TP ON TP.numItemCode = I.numItemCode ' 
			END
			
			IF (SELECT COUNT(*) FROM #tmpItemCat) > 0
			BEGIN
				SET @strSQL = @strSQL + ' INNER JOIN #tmpItemCat TC ON TC.numCategoryID = IC.numCategoryID '
				SET @strSQL = @strSQL + ' LEFT JOIN #tmpOnHandItems TOIC ON TOIC.numItemID = I.numItemCode '
			END
			ELSE IF @numCategoryID>0
			BEGIN
				SET @Where = REPLACE(@Where,'WHERE',' WHERE IC.numCategoryID = ' + CONVERT(VARCHAR(10),@numCategoryID) + ' AND ')
			END 
			
			IF @tintDisplayCategory = 1
			BEGIN
				SET @Where = @Where + ' AND (ISNULL(W.numOnHand, 0) > 0 OR ISNULL(W2.numAllocation, 0) > 0) '
			END
			
			--PRINT @Where
			SET @strSQL = @strSQL + @Where
			
			IF LEN(@FilterRegularCondition) > 0
			BEGIN
				SET @strSQL = @strSQL + @FilterRegularCondition
			END                                         
			
---PRINT @SortString + ',' + @DefaultSort + ' ' + @vcSort 
	
            IF LEN(@whereNumItemCode) > 0
                                        BEGIN
								              SET @strSQL = @strSQL + @filterByNumItemCode	
								    	END
            SET @strSQL = @strSQL +     '   )
                                        , ItemSorted AS (SELECT  ROW_NUMBER() OVER ( ORDER BY  ' + @SortString + @DefaultSort + ' ' + @vcSort + ' ) AS Rownumber,* FROM  Items WHERE RowID = 1)'
            
            DECLARE @fldList AS NVARCHAR(MAX)
			SELECT @fldList=COALESCE(@fldList + ',','') + QUOTENAME(REPLACE(Fld_label,' ','') + '_C') FROM dbo.CFW_Fld_Master WHERE numDomainID=@numDomainID AND Grp_id=5
            
            SET @strSQL = @strSQL +' SELECT  Rownumber,numItemCode,txtItemDesc,vcManufacturer,monListPrice,vcItemName,vcCategoryName,vcPathForImage,vcpathForTImage,vcSKU,fltHeight,fltWidth,fltLength,vcModelID,fltWeight,bitFreeShipping,UOMConversionFactor,InStock,CategoryDesc,numWareHouseItemID
                                     INTO #tempItem FROM ItemSorted ' 
--                                        + 'WHERE Rownumber > ' + CONVERT(VARCHAR(10), @firstRec) + '
--                                        AND   Rownumber < ' + CONVERT(VARCHAR(10), @lastRec) + '
--                                        order by Rownumber;' 
            
            SET @strSQL = @strSQL + ' SELECT * INTO #tmpPagedItems FROM #tempItem WHERE Rownumber > ' + CONVERT(VARCHAR(10), @firstRec) + '
                                      AND   Rownumber < ' + CONVERT(VARCHAR(10), @lastRec) + '
                                      order by Rownumber '
                                                                  
            IF LEN(ISNULL(@fldList,''))>0
            BEGIN
				PRINT 'flds1'
				SET @strSQL = @strSQL +'SELECT  Rownumber,numItemCode,txtItemDesc,vcManufacturer,monListPrice,vcItemName,vcCategoryName,vcPathForImage,vcpathForTImage,vcSKU,fltHeight,fltWidth,fltLength,vcModelID,fltWeight,bitFreeShipping,UOMConversionFactor,InStock,CategoryDesc,numWareHouseItemID,' + @fldList + '
                                        FROM #tmpPagedItems I left join (
			SELECT  t2.RecId, t1.Fld_label + ''_C'' as Fld_label, 
				CASE t1.fld_type WHEN ''SelectBox'' THEN isnull(dbo.fn_GetListItemName(t2.Fld_Value),'''')
								 WHEN ''CheckBox'' THEN isnull(dbo.fn_getCustomDataValue(t2.Fld_Value,9),'''')
								 WHEN ''DateField'' THEN dbo.FormatedDateFromDate(t2.Fld_Value,' + CAST(@numDomainId AS VARCHAR(18)) +')
								 ELSE Replace(ISNULL(t2.Fld_Value,''0''),''$'',''$$'') end AS Fld_Value FROM CFW_Fld_Master t1 
			JOIN CFW_FLD_Values_Item AS t2 ON t1.Fld_ID = t2.Fld_ID where t1.Grp_id = 5 and t1.numDomainID=' + CAST(@numDomainId AS VARCHAR(18)) +' AND t1.fld_type <> ''Link''
			AND t2.RecId IN (SELECT numItemCode FROM #tmpPagedItems )
			) p PIVOT (MAX([Fld_Value]) FOR Fld_label IN (' + @fldList + ')) AS pvt on I.numItemCode=pvt.RecId order by Rownumber;' 	
			END 
			ELSE
			BEGIN
				PRINT 'flds2'
				SET @strSQL = @strSQL +' SELECT  Rownumber,numItemCode,txtItemDesc,vcManufacturer,monListPrice,vcItemName,vcCategoryName,vcPathForImage,vcpathForTImage,vcSKU,fltHeight,fltWidth,fltLength,vcModelID,fltWeight,bitFreeShipping,UOMConversionFactor,InStock,CategoryDesc,numWareHouseItemID
                                         FROM  #tmpPagedItems order by Rownumber;'
			END               
                                        
            
--            PRINT(@strSQL)        
--            EXEC ( @strSQL) ; 
			                           
            SET @strSQL = @strSQL + ' SELECT @TotRecs = COUNT(*) FROM #tempItem '                            
--             SET @strSQL = 'SELECT @TotRecs = COUNT(*) 
--                           FROM Item AS I
--                           INNER JOIN   ItemCategory IC ON I.numItemCode = IC.numItemID
--                           INNER JOIN   Category C ON IC.numCategoryID = C.numCategoryID
--                           INNER JOIN   SiteCategories SC ON IC.numCategoryID = SC.numCategoryID ' --+ @Where
--             
--			 IF LEN(@FilterCustomFields) > 0 AND LEN(@FilterCustomWhere) > 0
--			 BEGIN
--				 SET @strSQL = @strSQL + ' INNER JOIN #tmpItemCode TP ON TP.numItemCode = I.numItemCode ' 
--			 END
--			
--			 IF (SELECT COUNT(*) FROM #tmpItemCat) > 0
--			 BEGIN
--				SET @strSQL = @strSQL + ' INNER JOIN #tmpItemCat TC ON TC.numCategoryID = IC.numCategoryID '
--				SET @strSQL = @strSQL + ' INNER JOIN #tmpOnHandItems TOIC ON TOIC.numItemID = I.numItemCode '
--			 END
--			 ELSE IF @numCategoryID>0
--			 BEGIN
--				SET @Where = REPLACE(@Where,'WHERE',' WHERE IC.numCategoryID = ' + CONVERT(VARCHAR(10),@numCategoryID) + ' AND ')
--			 END 
--			
--			 SET @strSQL = @strSQL + @Where  
--			 
--			 IF LEN(@FilterRegularCondition) > 0
--			 BEGIN
--				SET @strSQL = @strSQL + @FilterRegularCondition
--			 END 
			
		--			--SELECT * FROM  #tempAvailableFields
		--SELECT * FROM  #tmpItemCode
		--SELECT * FROM  #tmpItemCat
		----SELECT * FROM  #fldValues
		--SELECT * FROM  #tempSort
		--SELECT * FROM  #fldDefaultValues
		--SELECT * FROM  #tmpOnHandItems
		----SELECT * FROM  #tmpPagedItems

			 PRINT  LEN(@strSQL)
			 DECLARE @tmpSQL AS VARCHAR(MAX)
			 SET @tmpSQL = @strSQL
			 PRINT  @tmpSQL
			 
             EXEC sp_executeSQL @strSQL, N'@TotRecs INT OUTPUT', @TotRecs OUTPUT
             
             
        DECLARE @tintOrder AS TINYINT                                                  
		DECLARE @vcFormFieldName AS VARCHAR(50)                                                  
		DECLARE @vcListItemType AS VARCHAR(1)                                             
		DECLARE @vcAssociatedControlType VARCHAR(10)                                                  
		DECLARE @numListID AS NUMERIC(9)                                                  
		DECLARE @WhereCondition VARCHAR(MAX)                       
		DECLARE @numFormFieldId AS NUMERIC  
		DECLARE @vcFieldType CHAR(1)
		                  
		SET @tintOrder=0                                                  
		SET @WhereCondition =''                 
                   
              
		CREATE TABLE #tempAvailableFields(numFormFieldId  NUMERIC(9),vcFormFieldName NVARCHAR(50),
		vcFieldType CHAR(1),vcAssociatedControlType NVARCHAR(50),numListID NUMERIC(18),
		vcListItemType CHAR(1),intRowNum INT,vcItemValue VARCHAR(3000))
		   
		INSERT INTO #tempAvailableFields (numFormFieldId,vcFormFieldName,vcFieldType,vcAssociatedControlType
				,numListID,vcListItemType,intRowNum)                         
		SELECT  Fld_id,REPLACE(Fld_label,' ','') AS vcFormFieldName,
			   CASE GRP_ID WHEN 4 then 'C' WHEN 1 THEN 'D' ELSE 'C' END AS vcFieldType,
				fld_type as vcAssociatedControlType,
				ISNULL(C.numListID, 0) numListID,
				CASE WHEN C.numListID > 0 THEN 'L'
					 ELSE ''
				END vcListItemType,ROW_NUMBER() OVER (ORDER BY Fld_id ASC) AS intRowNum
		FROM    CFW_Fld_Master C LEFT JOIN CFW_Validation V ON V.numFieldID = C.Fld_id
		WHERE   C.numDomainID = @numDomainId
				AND GRP_ID IN (5)

		  
		SELECT * FROM #tempAvailableFields
		
		/*
		DROP TABLE #tempAvailableFields
		DROP TABLE #tmpItemCode
		DROP TABLE #tmpItemCat
		*/	

		IF OBJECT_ID('tempdb..#tempAvailableFields') IS NOT NULL
		DROP TABLE #tempAvailableFields
		
		IF OBJECT_ID('tempdb..#tmpItemCode') IS NOT NULL
		DROP TABLE #tmpItemCode
		
		IF OBJECT_ID('tempdb..#tmpItemCat') IS NOT NULL
		DROP TABLE #tmpItemCat
		
		IF OBJECT_ID('tempdb..#fldValues') IS NOT NULL
		DROP TABLE #fldValues
		
		IF OBJECT_ID('tempdb..#tempSort') IS NOT NULL
		DROP TABLE #tempSort
		
		IF OBJECT_ID('tempdb..#fldDefaultValues') IS NOT NULL
		DROP TABLE #fldDefaultValues
		
		IF OBJECT_ID('tempdb..#tmpOnHandItems') IS NOT NULL
		DROP TABLE #tmpOnHandItems

		IF OBJECT_ID('tempdb..#tmpPagedItems') IS NOT NULL
		DROP TABLE #tmpPagedItems
    END


GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageItemSerialNo')
DROP PROCEDURE USP_ManageItemSerialNo
GO
CREATE PROCEDURE USP_ManageItemSerialNo
    @numDomainID NUMERIC(9),
    @strFieldList TEXT,
    @numUserCntID AS NUMERIC(9)=0
AS 
    BEGIN
        DECLARE @hDoc AS INT                                            
        EXEC sp_xml_preparedocument @hDoc OUTPUT, @strFieldList
           
        
--                     SELECT * FROM WareHouseItmsDTL
--        INSERT  INTO WareHouseItmsDTL
--                (
--                  numWareHouseItemID,
--                  vcSerialNo,
--                  vcComments
--                )
--                ( SELECT    numWareHouseItemID,
--                            vcSerialNo,
--                            Comments
--                  FROM      OPENXML(@hDoc, '/NewDataSet/SerializedItems[Op_Flag=1]',2)
--                            WITH ( numWareHouseItmsDTLID NUMERIC(9), numWareHouseItemID NUMERIC(9), vcSerialNo VARCHAR(100), Op_Flag TINYINT, Comments VARCHAR(1000) )
--                )             
                                                                                
--        UPDATE  WareHouseItmsDTL
--        SET     numWareHouseItemID = X.numWareHouseItemID,
--                vcSerialNo = X.vcSerialNo,
--                vcComments = X.Comments
--        FROM    ( SELECT    numWareHouseItmsDTLID AS WareHouseItmsDTLID,
--                            numWareHouseItemID,
--                            vcSerialNo,
--                            Op_Flag,
--                            Comments
--                  FROM      OPENXML(@hDoc, '/NewDataSet/SerializedItems[Op_Flag=0][numWareHouseItmsDTLID!=0]',2)
--                            WITH ( numWareHouseItmsDTLID NUMERIC(9), numWareHouseItemID NUMERIC(9), vcSerialNo VARCHAR(100), Op_Flag TINYINT, Comments VARCHAR(1000) )
--                ) X
--        WHERE   numWareHouseItmsDTLID = X.WareHouseItmsDTLID                                            
     
     SELECT X.*,ROW_NUMBER() OVER( order by X.NewQty) AS ROWNUMBER
INTO #TempTable
FROM ( SELECT  numWareHouseItmsDTLID,numWareHouseItemID,vcSerialNo,Op_Flag,Comments,NewQty,OldQty, (CASE WHEN CAST(dExpirationDate AS DATE) = cast('1753-1-1' as date) THEN NULL ELSE dExpirationDate END) AS dExpirationDate
                  FROM      OPENXML(@hDoc, '/NewDataSet/SerializedItems',2)
                            WITH ( numWareHouseItmsDTLID NUMERIC(9), numWareHouseItemID NUMERIC(9), vcSerialNo VARCHAR(100), Op_Flag TINYINT, Comments VARCHAR(1000),OldQty NUMERIC(9),NewQty NUMERIC(9),dExpirationDate DATETIME)
                ) X


DECLARE @minROWNUMBER INT
DECLARE @maxROWNUMBER INT
DECLARE @Diff INT,@Op_Flag AS int
DECLARE @Diff1 INT
DECLARE @numWareHouseItemID NUMERIC(9)

SELECT  @minROWNUMBER = MIN(ROWNUMBER) , @maxROWNUMBER =MAX(ROWNUMBER) FROM #TempTable

DECLARE @vcDescription VARCHAR(100),@numItemCode NUMERIC(18)

WHILE  @minROWNUMBER <= @maxROWNUMBER
    BEGIN
   	    SELECT @Diff = NewQty - OldQty,@numWareHouseItemID=numWareHouseItemID,@Op_Flag=Op_Flag FROM #TempTable WHERE ROWNUMBER=@minROWNUMBER
   	    
   	    IF @Op_Flag=0 
   	    BEGIN
		   	 UPDATE WareHouseItmsDTL SET  numWareHouseItemID = X.numWareHouseItemID,
                vcSerialNo = X.vcSerialNo,vcComments = X.Comments,numQty=X.NewQty,dExpirationDate=X.dExpirationDate 
                FROM #TempTable X INNER JOIN WareHouseItmsDTL W
                ON X.numWareHouseItmsDTLID=W.numWareHouseItmsDTLID WHERE X.ROWNUMBER=@minROWNUMBER
                
                SET @vcDescription='UPDATE Lot/Serial#'
		END 
   	    
   	    ELSE IF @Op_Flag=1 
   	    BEGIN
   	       	  INSERT INTO WareHouseItmsDTL(numWareHouseItemID,vcSerialNo,vcComments,numQty,dExpirationDate)  
		   	   SELECT  X.numWareHouseItemID,X.vcSerialNo,X.Comments,X.NewQty,X.dExpirationDate  FROM #TempTable X 
		   	   WHERE X.ROWNUMBER=@minROWNUMBER
		   	   
		   	   SET @vcDescription='INSERT Lot/Serial#'
		END
   	    
   	    update WareHouseItems SET numOnHand=numOnHand + @Diff,dtModified = GETDATE()  where numWareHouseItemID = @numWareHouseItemID AND (numOnHand + @Diff)>=0
  
  
  SELECT @numItemCode=numItemID from WareHouseItems where numWareHouseItemID = @numWareHouseItemID AND [numDomainID] = @numDomainID

  DECLARE @numDomain AS NUMERIC(18,0)
  SET @numDomain = @numDomainID
  EXEC dbo.USP_ManageWareHouseItems_Tracking
	@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
	@numReferenceID = @numItemCode, --  numeric(9, 0)
	@tintRefType = 1, --  tinyint
	@vcDescription = @vcDescription, --  varchar(100)
	@numModifiedBy = @numUserCntID,
	@numDomainID = @numDomain 

        SELECT  @minROWNUMBER = MIN(ROWNUMBER) FROM  #TempTable WHERE  [ROWNUMBER] > @minROWNUMBER
    END	           
     
       
   
--        SELECT X.*,ROW_NUMBER() OVER( order by X.NewQty) AS ROWNUMBER
--INTO #TempTable
--FROM ( SELECT  numWareHouseItmsDTLID,vcOLDSerialNo,numWareHouseItemID,vcSerialNo,Op_Flag,Comments,NewQty,OldQty
--                  FROM      OPENXML(@hDoc, '/NewDataSet/SerializedItems',2)
--                            WITH ( numWareHouseItmsDTLID NUMERIC(9), vcOLDSerialNo varchar(50), numWareHouseItemID NUMERIC(9), vcSerialNo VARCHAR(100), Op_Flag TINYINT, Comments VARCHAR(1000),OldQty NUMERIC(9),NewQty NUMERIC(9) )
--                ) X
--
--
--DECLARE @minROWNUMBER INT
--DECLARE @maxROWNUMBER INT
--DECLARE @Diff INT
--DECLARE @Diff1 INT
--DECLARE @numWareHouseItemID NUMERIC(9)
--
--SELECT  @minROWNUMBER = MIN(ROWNUMBER) , @maxROWNUMBER =MAX(ROWNUMBER) FROM #TempTable
--
--WHILE  @minROWNUMBER <= @maxROWNUMBER
--    BEGIN
--   	    SELECT @Diff = NewQty - OldQty,@numWareHouseItemID=numWareHouseItemID FROM #TempTable WHERE ROWNUMBER=@minROWNUMBER
--   	    
--   	    IF @Diff=0 
--   	    BEGIN
--		   	 UPDATE  WareHouseItmsDTL SET  numWareHouseItemID = X.numWareHouseItemID,
--                vcSerialNo = X.vcSerialNo,vcComments = X.Comments FROM #TempTable X INNER JOIN WareHouseItmsDTL W
--                ON X.vcOLDSerialNo=W.vcSerialNo WHERE X.ROWNUMBER=@minROWNUMBER
--		END 
--   	    
--   	    ELSE IF @Diff>0 
--   	    BEGIN
--   	    SET @Diff1=@Diff
--   		    WHILE @Diff1>0	
--   		    Begin
--		   	  INSERT INTO WareHouseItmsDTL(numWareHouseItemID,vcSerialNo,vcComments)  
--		   	   SELECT  X.numWareHouseItemID,X.vcSerialNo,X.Comments FROM #TempTable X WHERE X.ROWNUMBER=@minROWNUMBER
--		   	   
--		   	    UPDATE  WareHouseItmsDTL SET  numWareHouseItemID = X.numWareHouseItemID,
--                vcSerialNo = X.vcSerialNo,vcComments = X.Comments FROM #TempTable X INNER JOIN WareHouseItmsDTL W
--                ON X.vcOLDSerialNo=W.vcSerialNo WHERE X.ROWNUMBER=@minROWNUMBER
--                
--		   	  SET @Diff1=@Diff1-1 
--		   	END
--		END
--   	    
--   	    ELSE IF @Diff<0 
--   	    BEGIN
--   	      DECLARE @sql VARCHAR(255) 
--          SET @sql = 'DELETE FROM WareHouseItmsDTL WHERE numWareHouseItmsDTLID IN (SELECT TOP ' + CONVERT(VARCHAR(10), @Diff * -1) + ' W.numWareHouseItmsDTLID FROM
--		   	#TempTable X INNER JOIN WareHouseItmsDTL W
--                ON X.vcOLDSerialNo=W.vcSerialNo WHERE X.ROWNUMBER=' + CONVERT(VARCHAR(9), @minROWNUMBER) + ')' 
--          EXEC(@sql) 
--          --PRINT(@sql)
--    	END
--   	        
--   	    update WareHouseItems SET numOnHand=numOnHand + @Diff where numWareHouseItemID = @numWareHouseItemID AND (numOnHand + @Diff)>=0
--  
--        SELECT  @minROWNUMBER = MIN(ROWNUMBER) FROM  #TempTable WHERE  [ROWNUMBER] > @minROWNUMBER
--    END	

DROP TABLE #TempTable
                
--   update WareHouseItems  set                                                                         
--   numOnHand=numOnHand + X.Total
--    From ((SELECT COUNT(*) Total,numWareHouseItemID AS WareHouseItemID                                                                  
--   FROM OPENXML(@hDoc,'/NewDataSet/SerializedItems[Op_Flag=1]',2)                                                                         
--    with(numWareHouseItemID numeric(9)) GROUP BY numWareHouseItemID)                   
--    )X                                                                         
--  where numWareHouseItemID = X.WareHouseItemID                      
                                             
--        DELETE  FROM WareHouseItmsDTL
--        WHERE   numWareHouseItmsDTLID IN (
--                SELECT  numWareHouseItmsDTLID
--                FROM    OPENXML(@hDoc, '/NewDataSet/SerializedItems[Op_Flag=3]',2)
--                        WITH ( numWareHouseItmsDTLID NUMERIC(9), numWareHouseItemID NUMERIC(9), vcSerialNo VARCHAR(100), Op_Flag TINYINT ) )                                                                          
-- 	
        EXEC sp_xml_removedocument @hDoc
    END
                      

GO
/****** Object:  StoredProcedure [dbo].[USP_ManageSites]    Script Date: 08/08/2009 14:59:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageSites')
DROP PROCEDURE USP_ManageSites
GO
CREATE PROCEDURE [dbo].[USP_ManageSites]
	@numSiteID NUMERIC(9) = 0 output,
	@vcSiteName varchar(50) = NULL,
	@vcDescription varchar(1000) = NULL,
	@numUserCntID NUMERIC(9)= NULL,
	@numDomainID NUMERIC(9) = NULL,
	@vcHostName VARCHAR(500)= NULL,
	@bitIsActive BIT,
	@vcLiveURL VARCHAR(100)= NULL,
	@numCurrencyID NUMERIC(9) = 0,
	@bitOnePageCheckout BIT =0 ,
	@tintRateType TINYINT = 0 ,
	@vcCategories VARCHAR(2000),
	@Mode INT = 0,
	@bitHtml5 BIT,
	@vcMetaTags TEXT,
	@bitSSLRedirect BIT
AS

IF LEN(@vcLiveURL)>1
BEGIN
	UPDATE Sites SET 
		vcLiveURL=@vcLiveURL
	WHERE [numSiteID] = @numSiteID
	
	RETURN
END


IF @numSiteID = 0 AND (NOT EXISTS(SELECT [vcSiteName] from Sites Where ([vcSiteName] = @vcSiteName OR vcHostName=@vcHostName) )) 
AND (NOT EXISTS(SELECT [vcPortalName] from dbo.Domain Where [vcPortalName] = @vcHostName))
BEGIN
	INSERT INTO Sites 
	(
		[vcSiteName],
		[vcDescription],
		vcHostName,
		[numCreatedBy],
		[dtCreateDate],
		[numModifiedBy],
		[dtModifiedDate],
		[numDomainID],
		[bitIsActive],numCurrencyID,bitOnePageCheckout,
		[tintRateType],
		[bitHtml5],
		[vcMetaTags],
		[bitSSLRedirect]
	)
	VALUES 
	(
		@vcSiteName,
		@vcDescription,
		@vcHostName,
		@numUserCntID,
		GETUTCDATE(),
		@numUserCntID,
		GETUTCDATE(),
		@numDomainID,
		@bitIsActive,@numCurrencyID,@bitOnePageCheckout,
		@tintRateType,
		@bitHtml5,
		@vcMetaTags,
		@bitSSLRedirect
	)
	
	SELECT @numSiteID = SCOPE_IDENTITY();
END
ELSE IF @numSiteID >0 AND (NOT EXISTS(SELECT [vcSiteName] from Sites Where ([vcSiteName] = @vcSiteName OR vcHostName=@vcHostName )AND numSiteID<> @numSiteID))
AND (NOT EXISTS(SELECT [vcPortalName] from dbo.Domain Where [vcPortalName] = @vcHostName))
BEGIN
  IF @Mode = 0
	 BEGIN
			UPDATE Sites SET 
			[vcSiteName] = @vcSiteName,
			[vcDescription] = @vcDescription,
			vcHostName		= @vcHostName,
			[numModifiedBy] = @numUserCntID,
			[dtModifiedDate] = GETUTCDATE(),
			[bitIsActive] = @bitIsActive,numCurrencyID=@numCurrencyID,
			bitOnePageCheckout=@bitOnePageCheckout,
			tintRateType = @tintRateType,
			bitHtml5 = @bitHtml5,
			vcMetaTags = @vcMetaTags,
			bitSSLRedirect=@bitSSLRedirect
			WHERE [numSiteID] = @numSiteID 	
	 END
ELSE IF @Mode = 1
IF	@numSiteID > 0
	BEGIN
          IF EXISTS(SELECT * FROM dbo.SiteCategories WHERE numSiteID = @numSiteID)
			 BEGIN
					DELETE FROM dbo.SiteCategories WHERE numSiteID = @numSiteID 			 	
			 END 

		INSERT INTO dbo.SiteCategories 
		(
			numSiteID,
			numCategoryID
		)  
		SELECT  @numSiteID AS numSiteID,  ID  from dbo.SplitIDs(@vcCategories,',')
	END
END
ELSE
	SET @numSiteID = 0;

/****** Object:  StoredProcedure [dbo].[USP_OPPBizDocItems]    Script Date: 07/26/2008 16:20:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                                                                                                                                                        
-- exec USP_OPPBizDocItems @numOppId=12388,@numOppBizDocsId=11988,@numDomainID=110
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_oppbizdocitems' ) 
    DROP PROCEDURE usp_oppbizdocitems
GO
CREATE PROCEDURE [dbo].[USP_OPPBizDocItems]
    (
      @numOppId AS NUMERIC(9) = NULL,
      @numOppBizDocsId AS NUMERIC(9) = NULL,
      @numDomainID AS NUMERIC(9) = 0      
    )
AS 
    DECLARE @DivisionID AS NUMERIC(9)      
    DECLARE @tintTaxOperator AS TINYINT   
                                                                                                                                                                                                                                                                                                                                          
    SELECT  @DivisionID = numDivisionID, @tintTaxOperator = [tintTaxOperator]           
    FROM    OpportunityMaster
    WHERE   numOppId = @numOppId                                                                                                                                                                          
--Decimal points to show in bizdoc 
    DECLARE @DecimalPoint AS TINYINT
    SELECT  @DecimalPoint = tintDecimalPoints
    FROM    domain
    WHERE   numDomainID = @numDomainID
                                                                                          
    DECLARE @strSQL AS VARCHAR(MAX)                                                                                          
    DECLARE @strSQL1 AS VARCHAR(8000)                                       
    DECLARE @strSQLCusFields VARCHAR(1000)                      
    DECLARE @strSQLEmpFlds VARCHAR(500)                                                                         
    SET @strSQLCusFields = ''                                                                                          
    SET @strSQLEmpFlds = ''                                                                                          
    DECLARE @tintType AS TINYINT   
    DECLARE @tintOppType AS TINYINT                                                      
    DECLARE @intRowNum AS INT                                                  
    DECLARE @numFldID AS VARCHAR(15)                                          
    DECLARE @vcFldname AS VARCHAR(50)                                            
    DECLARE @numBizDocId AS NUMERIC(9)
    DECLARE @numBizDocTempID AS NUMERIC(9) ;
        SET @numBizDocTempID = 0
                                           
    SELECT  @numBizDocTempID = ISNULL(numBizDocTempID, 0),
            @numBizDocId = numBizDocId
    FROM    OpportunityBizDocs
    WHERE   numOppBizDocsId = @numOppBizDocsId
    SELECT  @tintType = tintOppType,
            @tintOppType = tintOppType
    FROM    OpportunityMaster
    WHERE   numOppID = @numOppId                                                                                          
                                                                                   
    IF @tintType = 1 
        SET @tintType = 7
    ELSE 
        SET @tintType = 8                                                                                      
                                                                                
                                                                                        
                                                                                          
 

    SELECT  *
    INTO    #Temp1
    FROM    ( SELECT   Opp.vcitemname AS vcItemName,
                        ISNULL(OBD.numOppBizDocItemID, 0) OppBizDocItemID,
                        CASE WHEN charitemType = 'P' THEN 'Product'
                             WHEN charitemType = 'S' THEN 'Service'
                        END AS charitemType,
                        CASE WHEN charitemType = 'P' THEN 'Inventory'
	                         WHEN charitemType = 'N' THEN 'Non-Inventory'
                             WHEN charitemType = 'S' THEN 'Service'
                        END AS vcItemType,
                        CASE WHEN bitShowDeptItemDesc = 0
                             THEN CONVERT(VARCHAR(2500), OBD.vcitemDesc)
                             WHEN bitShowDeptItemDesc = 1
                                  AND bitKitParent = 1
                                  AND bitAssembly = 0
                             THEN dbo.fn_PopulateKitDesc(opp.numOppId,
                                                         opp.numoppitemtCode)
                             WHEN bitShowDeptItemDesc = 1
                                  AND bitKitParent = 1
                                  AND bitAssembly = 1
                             THEN dbo.fn_PopulateAssemblyDesc(opp.numoppitemtCode, OBD.numUnitHour)
                             ELSE CONVERT(VARCHAR(2500), OBD.vcitemDesc)
                        END AS txtItemDesc,                                      
--vcitemdesc as [desc],                                      
                        dbo.fn_UOMConversion(ISNULL(i.numBaseUnit, 0),
                                             i.numItemCode, @numDomainID,
                                             ISNULL(opp.numUOMId, 0))
                        * OBD.numUnitHour AS numUnitHour,
                        CONVERT(DECIMAL(18, 4), (dbo.fn_UOMConversion(ISNULL(opp.numUOMId, 0), i.numItemCode, @numDomainID, ISNULL(i.numBaseUnit, 0)) * OBD.monPrice)) monPrice,
                        CONVERT(VARCHAR, CONVERT(DECIMAL(18, 4), OBD.monTotAmount)) Amount,
                        OBD.monTotAmount/*Fo calculating sum*/,
--                        CASE WHEN @tintOppType = 1
--                             THEN CASE WHEN bitTaxable = 0 THEN 0
--                                       WHEN bitTaxable = 1
--                                       THEN ( SELECT    fltPercentage
--                                              FROM      OpportunityBizDocTaxItems
--                                              WHERE     numOppBizDocID = @numOppBizDocsId
--                                                        AND numTaxItemID = 0
--                                            )
--                                  END
--                             ELSE 0
--                        END AS Tax,
                        ISNULL(CONVERT(VARCHAR, i.monListPrice), 0) AS monListPrice,
                        i.numItemCode AS ItemCode,
                        opp.numoppitemtCode,
                        L.vcdata AS numItemClassification,
                        CASE WHEN bitTaxable = 0 THEN 'No'
                             ELSE 'Yes'
                        END AS Taxable,
                        numIncomeChartAcntId AS itemIncomeAccount,
                        'NI' AS ItemType,
                        ISNULL(GJH.numJournal_Id, 0) AS numJournalId,
                        ( SELECT TOP 1
                                    ISNULL(GJD.numTransactionId, 0)
                          FROM      General_Journal_Details GJD
                          WHERE     GJH.numJournal_Id = GJD.numJournalId
                                    AND GJD.chBizDocItems = 'NI'
                                    AND GJD.numoppitemtCode = opp.numoppitemtCode
                        ) AS numTransactionId,
                        ISNULL(Opp.vcModelID, '') vcModelID,
                        dbo.USP_GetAttributes(OBD.numWarehouseItmsID,
                                              bitSerialized) AS vcAttributes,
                        ISNULL(vcPartNo, '') AS vcPartNo,
                        ( ISNULL(OBD.monTotAmtBefDiscount, OBD.monTotAmount)
                          - OBD.monTotAmount ) AS DiscAmt,
						CONVERT(DECIMAL(18, 4),ISNULL(OBD.monTotAmount,0) / OBD.numUnitHour) As monUnitSalePrice,
                        OBD.vcNotes,
                        OBD.vcTrackingNo,
                        OBD.vcShippingMethod,
                        OBD.monShipCost,
                        [dbo].[FormatedDateFromDate](OBD.dtDeliveryDate,
                                                     @numDomainID) dtDeliveryDate,
                        Opp.numWarehouseItmsID,
--ISNULL(I.vcUnitofMeasure,'Units') vcUnitofMeasure,
                        ISNULL(u.vcUnitName, '') numUOMId,
                        ISNULL(Opp.vcManufacturer, '') AS vcManufacturer,
                        ISNULL(V.monCost, 0) AS VendorCost,
                        Opp.vcPathForTImage,
                        i.[fltLength],
                        i.[fltWidth],
                        i.[fltHeight],
                        i.[fltWeight],
                        ISNULL(I.numVendorID, 0) numVendorID,
                        Opp.bitDropShip AS DropShip,
                        SUBSTRING(( SELECT  ' ,' + vcSerialNo
                                            + CASE WHEN ISNULL(I.bitLotNo, 0) = 1
                                                   THEN ' ('
                                                        + CONVERT(VARCHAR(15), oppI.numQty)
                                                        + ')'
                                                   ELSE ''
                                              END
                                    FROM    OppWarehouseSerializedItem oppI
                                            JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID = whi.numWareHouseItmsDTLID
                                    WHERE   oppI.numOppID = Opp.numOppId
                                            AND oppI.numOppItemID = Opp.numoppitemtCode
              --                              AND 1=(CASE WHEN @tintOppType=1 then CASE WHEN oppI.numOppBizDocsID=OBD.numOppBizDocID THEN 1 ELSE 0 END
														--ELSE 1 END)
                                    ORDER BY vcSerialNo
                                  FOR
                                    XML PATH('')
                                  ), 3, 200000) AS SerialLotNo,
                        ISNULL(opp.numUOMId, 0) AS numUOM,
                        ISNULL(u.vcUnitName, '') vcUOMName,
                        dbo.fn_UOMConversion(opp.numUOMId, Opp.numItemCode,
                                             @numDomainID, NULL) AS UOMConversionFactor,
                        ISNULL(Opp.numSOVendorId, 0) AS numSOVendorId,
                        [dbo].FormatedDateTimeFromDate(OBD.dtRentalStartDate,
                                                       @numDomainID) dtRentalStartDate,
                        [dbo].FormatedDateTimeFromDate(OBD.dtRentalReturnDate,
                                                       @numDomainID) dtRentalReturnDate,
-- Added for packing slip
                        ISNULL(W.vcWareHouse, '') AS Warehouse,
                        CASE WHEN charitemType = 'P' THEN WI.vcWHSKU
                             ELSE i.vcSKU
                        END vcSKU,
                        CASE WHEN charitemType = 'P' THEN WI.vcBarCode
                             ELSE i.numBarCodeId
                        END vcBarcode,
                        ISNULL(opp.numClassID, 0) AS numClassID,
                        ISNULL(opp.numProjectID, 0) AS numProjectID,
                        ISNULL(I.[numShipClass],0) AS numShipClass,OBD.numOppBizDocID,WL.vcLocation,ISNULL(bitSerialized,0) AS bitSerialized,ISNULL(bitLotNo,0) AS bitLotNo,
                        opp.numOppId,OBD.numUnitHour AS numUnitHourOrig,ISNULL(I.numItemCode,0) AS [numItemCode]						
              FROM      OpportunityItems opp
                        JOIN OpportunityBizDocItems OBD ON OBD.numOppItemID = opp.numoppitemtCode
                        LEFT JOIN item i ON opp.numItemCode = i.numItemCode
                        LEFT JOIN ListDetails L ON i.numItemClassification = L.numListItemID
                        LEFT JOIN Vendor V ON V.numVendorID = @DivisionID
                                              AND V.numItemCode = opp.numItemCode
                        LEFT JOIN General_Journal_Header GJH ON opp.numoppid = GJH.numOppId
                                                                AND GJH.numBizDocsPaymentDetId IS NULL
                                                                AND GJH.numOppBizDocsId = @numOppBizDocsId
                        LEFT JOIN UOM u ON u.numUOMId = opp.numUOMId                                    
-- left join General_Journal_Details GJD on GJH.numJournal_Id=GJD.numJournalId And GJD.chBizDocItems='NI' And GJD.numoppitemtCode=opp.numoppitemtCode                                       
                        LEFT JOIN dbo.WareHouseItems WI ON opp.numWarehouseItmsID = WI.numWareHouseItemID
                                                           AND WI.numDomainID = @numDomainID
                        LEFT JOIN dbo.Warehouses W ON WI.numDomainID = W.numDomainID
                                                      AND WI.numWareHouseID = W.numWareHouseID
                        LEFT JOIN dbo.WarehouseLocation WL ON WL.numWLocationID = WI.numWLocationID
              WHERE     opp.numOppId = @numOppId
                        AND ( bitShowDeptItem IS NULL
                              OR bitShowDeptItem = 0
                            )
                        AND OBD.numOppBizDocID = @numOppBizDocsId
       
--For Kit Items where following condition is true( Show dependant items as line items on BizDoc (instead of kit item name))
-- Commented by chintan, Reason: Bug #1829 
/*
union
select Opp.vcitemname as Item,                                      
case when i.charitemType='P' then 'Product' when i.charitemType='S' then 'Service' end as type,                                      
convert(varchar(500),OBD.vcitemDesc) as [Desc],                                      
--vcitemdesc as [desc],                                      
OBD.numUnitHour*intQuantity as Unit,
CONVERT(VARCHAR,CONVERT(DECIMAL(18, 4), OBD.monPrice)) Price,
CONVERT(VARCHAR,CONVERT(DECIMAL(18, 4), OBD.monTotAmount)) Amount,
OBD.monTotAmount/*Fo calculating sum*/,
case when @tintOppType=1 then case when i.bitTaxable =0 then 0 when i.bitTaxable=1 then (select fltPercentage from OpportunityBizDocTaxItems where numOppBizDocID=@numOppBizDocsId and numTaxItemID=0) end else 0 end  as Tax,                                    
isnull(convert(varchar,i.monListPrice),0) as monListPrice,i.numItemCode as ItemCode,opp.numoppitemtCode,                                      
L.vcdata as vcItemClassification,case when i.bitTaxable=0 then 'No' else 'Yes' end as Taxable,i.numIncomeChartAcntId as itemIncomeAccount,                                      
'NI' as ItemType                                      
,isnull(GJH.numJournal_Id,0) as numJournalId,(SELECT TOP 1 isnull(GJD.numTransactionId, 0) FROM General_Journal_Details GJD WHERE GJH.numJournal_Id = GJD.numJournalId And GJD.chBizDocItems = 'NI' And GJD.numoppitemtCode = opp.numoppitemtCode) as numTransactionId,isnull(Opp.vcModelID,'') vcModelID, dbo.USP_GetAttributes(OBD.numWarehouseItmsID,i.bitSerialized) as Attributes,isnull(vcPartNo,'') as Part 
,(isnull(OBD.monTotAmtBefDiscount,OBD.monTotAmount)-OBD.monTotAmount) as DiscAmt,OBD.vcNotes,OBD.vcTrackingNo,OBD.vcShippingMethod,OBD.monShipCost,[dbo].[FormatedDateFromDate](OBD.dtDeliveryDate,@numDomainID) dtDeliveryDate,Opp.numWarehouseItmsID,
--ISNULL(I.vcUnitofMeasure,'Units') vcUnitofMeasure,
ISNULL(u.vcUnitName,'') vcUnitofMeasure,
isnull(i2.vcManufacturer,'') as vcManufacturer,
isnull(V.monCost,0) as VendorCost,
Opp.vcPathForTImage,i.[fltLength],i.[fltWidth],i.[fltHeight],i.[fltWeight],isnull(I.numVendorID,0) numVendorID,Opp.bitDropShip AS DropShip,
SUBSTRING(
(SELECT ',' + vcSerialNo + CASE WHEN isnull(I.bitLotNo,0)=1 THEN ' (' + CONVERT(VARCHAR(15),oppI.numQty) + ')' ELSE '' end 
FROM OppWarehouseSerializedItem oppI JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID=whi.numWareHouseItmsDTLID where oppI.numOppID=Opp.numOppId and oppI.numOppItemID=Opp.numoppitemtCode 
ORDER BY vcSerialNo FOR XML PATH('')),2,200000) AS SerialLotNo,isnull(opp.numUOMId,0) AS numUOM,ISNULL(u.vcUnitName,'') vcUOMName,dbo.fn_UOMConversion(opp.numUOMId,Opp.numItemCode,@numDomainID,null) AS UOMConversionFactor,
ISNULL(Opp.numSOVendorId,0) as numSOVendorId,
[dbo].FormatedDateTimeFromDate(OBD.dtRentalStartDate,@numDomainID) dtRentalStartDate,
[dbo].FormatedDateTimeFromDate(OBD.dtRentalReturnDate,@numDomainID) dtRentalReturnDate
from OpportunityItems opp       
join OpportunityBizDocItems OBD on OBD.numOppItemID= opp.numoppitemtCode                                   
join  OpportunityKitItems OppKitItems      
on OppKitItems.numOppKitItem=opp.numoppitemtCode                                     
join item i on OppKitItems.numChildItem=i.numItemCode      
join item i2 on opp.numItemCode=i2.numItemCode                                      
left join ListDetails L on i.numItemClassification=L.numListItemID
left join Vendor V on V.numVendorID=@DivisionID and V.numItemCode=opp.numItemCode                                       
left join General_Journal_Header GJH on opp.numoppid=GJH.numOppId And GJH.numBizDocsPaymentDetId is null And GJH.numOppBizDocsId=@numOppBizDocsId                                      
-- left join General_Journal_Details GJD on GJH.numJournal_Id=GJD.numJournalId And GJD.chBizDocItems='NI' And GJD.numoppitemtCode=opp.numoppitemtCode
LEFT JOIN  UOM u ON u.numUOMId = opp.numUOMId                                    
where opp.numOppId=@numOppId and i2.bitShowDeptItem=1      and   OBD.numOppBizDocID=@numOppBizDocsId   */
--union
--select case when numcategory=1 then                             
--case when isnull(te.numcontractid,0) = 0 then 'Time' else 'Time(Contract)' end                              
--when numcategory=2 then                             
--case when isnull(te.numcontractid,0) = 0 then 'Expense' else 'Expense(Contract)' end                              
--end  as item,                           
--case when numcategory=1 then 'Time' when numcategory=2 then 'Expense' end                                      
-- as Type,                                      
--convert(varchar(100),txtDesc) as [Desc],                                      
----txtDesc as [Desc],                                      
--convert(decimal(18,2),datediff(minute,te.dtfromdate,te.dttodate))/60 as unit,                                      
--convert(varchar(100),monamount) as Price,                                      
--case when isnull(te.numcontractid,0) = 0                    
--then                    
-- case when numCategory =1  then                    
--   isnull(convert(decimal(18,2),datediff(minute,dtfromdate,dttodate))*monAmount/60,0)                    
--   when numCategory =2  then                    
--   isnull(monamount,0)                    
--                     
--   end                    
--else                                  
--  0                    
--end as amount,                                      
--0.00 as Tax,'' as listPrice,0 as ItemCode,0 as numoppitemtCode,'' as vcItemClassification,                                      
--'No' as Taxable,0 as itemIncomeAccount,'TE' as ItemType                                      
--,0 as numJournalId,0 as numTransactionId ,'' vcModelID,''as Attributes,'' as Part,0 as DiscAmt ,'' as vcNotes,'' as vcTrackingNo                                      
--from timeandexpense TE                            
--                                    
--where                                       
--(numtype= 1)   and   numDomainID=@numDomainID and                                    
--(numOppBizDocsId = @numOppBizDocsId  or numoppid = @numOppId)
            ) X


    --IF @DecimalPoint = 1 
    --    BEGIN
    --        UPDATE  #Temp1
    --        SET     monPrice = CONVERT(DECIMAL(18, 1), monPrice),
    --                Amount = CONVERT(DECIMAL(18, 1), Amount)
    --    END
    --IF @DecimalPoint = 2 
    --    BEGIN
    --        UPDATE  #Temp1
    --        SET     monPrice = CONVERT(DECIMAL(18, 2), monPrice),
    --                Amount = CONVERT(DECIMAL(18, 2), Amount)
    --    END
    --IF @DecimalPoint = 3 
    --    BEGIN
    --        UPDATE  #Temp1
    --        SET     monPrice = CONVERT(DECIMAL(18, 3), monPrice),
    --                Amount = CONVERT(DECIMAL(18, 3), Amount)
    --    END
    --IF @DecimalPoint = 4 
    --    BEGIN
    --        UPDATE  #Temp1
    --        SET     monPrice = CONVERT(DECIMAL(18, 4), monPrice),
    --                Amount = CONVERT(DECIMAL(18, 4), Amount)
    --    END

    DECLARE @strSQLUpdate AS VARCHAR(2000);SET @strSQLUpdate=''
   
    EXEC ( 'alter table #Temp1 add [TotalTax] money'
        )

    IF /*@tintOppType = 1 AND*/ @tintTaxOperator = 1 --add Sales tax
	--set @strSQLUpdate=@strSQLUpdate +',[TotalTax]= dbo.fn_CalSalesTaxAmt('+ convert(varchar(20),@numOppBizDocsId)+','+ convert(varchar(20),@numDomainID)+')*Amount/100'
        SET @strSQLUpdate = @strSQLUpdate
            + ',[TotalTax]= dbo.fn_CalBizDocTaxAmt('
            + CONVERT(VARCHAR(20), @numDomainID) + ',0,'
            + CONVERT(VARCHAR(20), @numOppId) +','+
            + 'numoppitemtCode,1)*Amount/100'
    ELSE IF /*@tintOppType = 1 AND*/ @tintTaxOperator = 2 -- remove sales tax
            SET @strSQLUpdate = @strSQLUpdate + ',[TotalTax]= 0'
    ELSE /*IF @tintOppType = 1*/ 
                SET @strSQLUpdate = @strSQLUpdate
                    + ',[TotalTax]= dbo.fn_CalBizDocTaxAmt('
                    + CONVERT(VARCHAR(20), @numDomainID) + ',0,'
                    + CONVERT(VARCHAR(20), @numOppId) +','+
                    + 'numoppitemtCode,1)*Amount/100'
    --ELSE 
    --            SET @strSQLUpdate = @strSQLUpdate + ',[TotalTax]= 0'


 DECLARE @vcTaxName AS VARCHAR(100)
    DECLARE @numTaxItemID AS NUMERIC(9)
    SET @numTaxItemID = 0
    SELECT TOP 1
            @vcTaxName = vcTaxName,
            @numTaxItemID = numTaxItemID
    FROM    TaxItems
    WHERE   numDomainID = @numDomainID


    WHILE @numTaxItemID > 0
        BEGIN

            EXEC ( 'alter table #Temp1 add [' + @vcTaxName + '] money'
                )

            /*IF @tintOppType = 1 */
                SET @strSQLUpdate = @strSQLUpdate + ',[' + @vcTaxName
                    + ']= dbo.fn_CalBizDocTaxAmt('
                    + CONVERT(VARCHAR(20), @numDomainID) + ','
                    + CONVERT(VARCHAR(20), @numTaxItemID) + ','
                    + CONVERT(VARCHAR(20), @numOppId) +','+
                    + 'numoppitemtCode,1)*Amount/100'
            --ELSE 
            --    SET @strSQLUpdate = @strSQLUpdate + ',[' + @vcTaxName + ']= 0'

            SELECT TOP 1
                    @vcTaxName = vcTaxName,
                    @numTaxItemID = numTaxItemID
            FROM    TaxItems
            WHERE   numDomainID = @numDomainID
                    AND numTaxItemID > @numTaxItemID

            IF @@rowcount = 0 
                SET @numTaxItemID = 0


        END

    IF @strSQLUpdate <> '' 
        BEGIN
            SET @strSQLUpdate = 'update #Temp1 set ItemCode=ItemCode'
                + @strSQLUpdate + ' where ItemCode>0'
            PRINT @strSQLUpdate
            EXEC ( @strSQLUpdate
                )
        END

    DECLARE @vcDbColumnName NVARCHAR(50)
    SELECT TOP 1
            @numFldID = numfieldID,
            @vcFldname = vcFieldName,
            @vcDbColumnName = vcDbColumnName,
            @intRowNum = ( tintRow + 1 )
    FROM    View_DynamicCustomColumns
    WHERE   numFormID = @tintType
            AND Grp_id = 5
            AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId
            AND bitCustom = 1
            AND ISNULL(numRelCntType, 0) = @numBizDocTempID
    ORDER BY tintRow                                                                                          
    WHILE @intRowNum > 0                                                                                  
        BEGIN    
            EXEC
                ( 'alter table #Temp1 add [' + @vcDbColumnName
                  + '] varchar(100)'
                )
	
            SET @strSQLUpdate = 'update #Temp1 set ItemCode=ItemCode,['
                + @vcDbColumnName + ']=dbo.GetCustFldValueItem(' + @numFldID
                + ',ItemCode) where ItemCode>0'
            EXEC ( @strSQLUpdate
                )
		                   
            SELECT TOP 1
                    @numFldID = numfieldID,
                    @vcFldname = vcFieldName,
                    @vcDbColumnName = vcDbColumnName,
                    @intRowNum = ( tintRow + 1 )
            FROM    View_DynamicCustomColumns
            WHERE   numFormID = @tintType
                    AND Grp_id = 5
                    AND numDomainID = @numDomainID
                    AND numAuthGroupID = @numBizDocId
                    AND bitCustom = 1
                    AND tintRow >= @intRowNum
                    AND ISNULL(numRelCntType, 0) = @numBizDocTempID
            ORDER BY tintRow                                                                  
		           
            IF @@rowcount = 0 
                SET @intRowNum = 0                                                                                          
        END


    SELECT  Row_NUmber() OVER ( ORDER BY numoppitemtCode ) RowNumber,
            *
    FROM    #Temp1
    ORDER BY numoppitemtCode ASC 

    DROP TABLE #Temp1

    SELECT  ISNULL(tintOppStatus, 0)
    FROM    OpportunityMaster
    WHERE   numOppID = @numOppId                                                                                                 


IF (SELECT COUNT(*) FROM View_DynamicColumns WHERE   numFormID = @tintType AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId AND vcFieldType = 'R'AND ISNULL(numRelCntType, 0) = @numBizDocTempID)=0
BEGIN            
	INSERT INTO DycFormConfigurationDetails (numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numAuthGroupID,numRelCntType)
	select numFormId,numFieldId,1,tintRow,@numDomainID,@numBizDocId,@numBizDocTempID
	 FROM View_DynamicDefaultColumns
	 where numFormId=@tintType and bitDefault=1 and numDomainID=@numDomainID order by tintRow ASC	
	 
	 
	 IF(SELECT COUNT(*) FROM DycFrmConfigBizDocsSumm Ds where numBizDocID=@numBizDocId and Ds.numFormID=@tintType 
	and isnull(Ds.numBizDocTempID,0)=@numBizDocTempID and Ds.numDomainID=@numDomainID)=0
	BEGIN
		INSERT INTO DycFrmConfigBizDocsSumm (numFormID,numBizDocID,numFieldID,numDomainID,numBizDocTempID)
		 SELECT @tintType,@numBizDocId,DFM.numFieldID,@numDomainID,@numBizDocTempID FROM
		 DycFormField_Mapping DFFM 
			JOIN DycFieldMaster DFM on DFFM.numModuleID=DFFM.numModuleID and DFFM.numFieldID=DFM.numFieldID
			where numFormID=16 and DFFM.numDomainID is null
	END

END      


    SELECT  vcFieldName AS vcFormFieldName,
            vcDbColumnName,
            vcFieldDataType,
            tintRow AS intRowNum
    FROM    View_DynamicColumns
    WHERE   numFormID = @tintType
            AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId
            AND vcFieldType = 'R'
            AND ISNULL(numRelCntType, 0) = @numBizDocTempID
    UNION
    SELECT  vcFieldName AS vcFormFieldName,
            vcDbColumnName,
            'C' vcFieldDataType,
            tintRow AS intRowNum
    FROM    View_DynamicCustomColumns
    WHERE   numFormID = @tintType
            AND Grp_id = 5
            AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId
            AND bitCustom = 1
            AND ISNULL(numRelCntType, 0) = @numBizDocTempID
    ORDER BY tintRow
GO
/****** Object:  StoredProcedure [dbo].[USP_OPPDetails]    Script Date: 03/25/2009 15:10:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Created By Anoop Jayaraj
-- [dbo].[USP_OPPDetails] 1362,1,-333
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_oppdetails')
DROP PROCEDURE usp_oppdetails
GO
CREATE PROCEDURE [dbo].[USP_OPPDetails]
(
               @numOppID             AS NUMERIC(9)  = NULL,
               @numDomainID          AS NUMERIC(9)  = 0,
               @ClientTimeZoneOffset AS INT)
AS
	
  SELECT Opp.numoppid,
         numCampainID,
         ADC.vcFirstname + ' ' + ADC.vcLastName AS numContactId,
         isnull(ADC.vcEmail,'') AS vcEmail,
         Opp.txtComments,
         Opp.numDivisionID,
         tintOppType,
         ISNULL(dateadd(MINUTE,-@ClientTimeZoneOffset,bintAccountClosingDate),'') AS bintAccountClosingDate,
         Opp.numContactID AS ContactID,
         tintSource,
         C2.vcCompanyName + Case when isnull(D2.numCompanyDiff,0)>0 then  '  ' + dbo.fn_getlistitemname(D2.numCompanyDiff) + ':' + isnull(D2.vcCompanyDiff,'') else '' end as vcCompanyname,
         D2.tintCRMType,
         Opp.vcPoppName,
         intpEstimatedCloseDate,
         [dbo].[GetDealAmount](@numOppID,getutcdate(),0) AS monPAmount,
         lngPConclAnalysis,
         monPAmount AS OppAmount,
         numSalesOrPurType,
         Opp.tintActive,
         dbo.fn_GetContactName(Opp.numCreatedby)
           + ' '
           + CONVERT(VARCHAR(20),dateadd(MINUTE,-@ClientTimeZoneOffset,Opp.bintCreatedDate)) AS CreatedBy,
         dbo.fn_GetContactName(Opp.numModifiedBy)
           + ' '
           + CONVERT(VARCHAR(20),dateadd(MINUTE,-@ClientTimeZoneOffset,Opp.bintModifiedDate)) AS ModifiedBy,
         dbo.fn_GetContactName(Opp.numRecOwner) AS RecordOwner,
         Opp.numRecOwner,
         isnull(tintshipped,0) AS tintshipped,
         isnull(tintOppStatus,0) AS tintOppStatus,
         dbo.OpportunityLinkedItems(@numOppID) AS NoOfProjects,
         Opp.numAssignedTo,
         Opp.numAssignedBy,
         (SELECT COUNT(* )
          FROM   dbo.GenericDocuments
          WHERE  numRecID = @numOppID
                 AND vcDocumentSection = 'O') AS DocumentCount,
         isnull(OPR.numRecurringId,0) AS numRecurringId,
		 OPR.dtRecurringDate AS dtLastRecurringDate,
         D2.numTerID,
         Link.numOppID AS numParentOppID,
         Link.vcPoppName AS ParentOppName,
         Link.vcSource AS Source,
         Link.numParentProjectID,
         Link.vcProjectName,
         ISNULL(C.varCurrSymbol,'') varCurrSymbol,
         ISNULL(Opp.fltExchangeRate,0) AS [fltExchangeRate],
         ISNULL(C.chrCurrency,'') AS [chrCurrency],
         ISNULL(C.numCurrencyID,0) AS [numCurrencyID],
		 (Select Count(*) from OpportunityBizDocs 
		 where numOppId=@numOppID and bitAuthoritativeBizDocs=1) As AuthBizDocCount,
		 (SELECT COUNT(distinct numChildOppID) FROM [OpportunityLinking] OL WHERE OL.[numParentOppID] = Opp.numOppId) AS ChildCount,
		 (SELECT COUNT(*) FROM [RecurringTransactionReport] RTR WHERE RTR.[numRecTranSeedId] = Opp.numOppId)  AS RecurringChildCount
		 ,Opp.bintCreatedDate,
		 ISNULL(Opp.bitStockTransfer ,0) bitStockTransfer,	ISNULL(Opp.tintTaxOperator,0) AS tintTaxOperator, 
         isnull(Opp.intBillingDays,0) AS numBillingDays,isnull(Opp.bitInterestType,0) AS bitInterestType,
         isnull(opp.fltInterest,0) AS fltInterest,  isnull(Opp.fltDiscount,0) AS fltDiscount,
         isnull(Opp.bitDiscountType,0) AS bitDiscountType,ISNULL(Opp.bitBillingTerms,0) AS bitBillingTerms,ISNULL(OPP.numDiscountAcntType,0) AS numDiscountAcntType,
         (SELECT TOP 1 numCountry FROM fn_getOPPAddressDetails(Opp.numOppId,Opp.numDomainID,2)) AS [numShipCountry],
		 (SELECT TOP 1 numState FROM fn_getOPPAddressDetails(Opp.numOppId,Opp.numDomainID,2)) AS [numShipState],
		 (SELECT TOP 1 vcState FROM fn_getOPPAddressDetails(Opp.numOppId,Opp.numDomainID,2)) AS [vcShipState],
		 (SELECT TOP 1 vcCountry FROM fn_getOPPAddressDetails(Opp.numOppId,Opp.numDomainID,2)) AS [vcShipCountry],
		 (SELECT TOP 1 vcPostalCode FROM fn_getOPPAddressDetails(Opp.numOppId,Opp.numDomainID,2)) AS [vcPostalCode],
		 ISNULL(vcCouponCode,'') AS [vcCouponCode],ISNULL(Opp.bitPPVariance,0) AS bitPPVariance,
		 Opp.dtItemReceivedDate,ISNULL(bitUseShippersAccountNo,0) [bitUseShippersAccountNo], ISNULL(vcShippersAccountNo,'') [vcShippersAccountNo],
		 ISNULL(bitUseMarkupShippingRate,0) [bitUseMarkupShippingRate],
		 ISNULL(numMarkupShippingRate,0) [numMarkupShippingRate],
		 ISNULL(SR.[vcValue2],intUsedShippingCompany) [intUsedShippingCompany],
		 ISNULL(Opp.[monLandedCostTotal],0) AS monLandedCostTotal,opp.[vcLanedCost],
		 ISNULL(Opp.vcRecurrenceType,'') AS vcRecurrenceType,
		 ISNULL(RecurrenceConfiguration.numRecConfigID,0) AS numRecConfigID,
		 (CASE WHEN ISNULL(Opp.vcRecurrenceType,'') = '' THEN 0 ELSE 1 END) AS bitRecur,
		 (CASE WHEN RecurrenceTransaction.numRecTranID IS NULL THEN 0 ELSE 1 END) AS bitRecurred,
		 dbo.FormatedDateFromDate(RecurrenceConfiguration.dtStartDate,@numDomainID) AS dtStartDate,
		 dbo.FormatedDateFromDate(RecurrenceConfiguration.dtEndDate,@numDomainID) AS dtEndDate,
		 RecurrenceConfiguration.vcFrequency AS vcFrequency,
		 ISNULL(RecurrenceConfiguration.bitDisabled,0) AS bitDisabled
  FROM   OpportunityMaster Opp 
         JOIN divisionMaster D2
           ON Opp.numDivisionID = D2.numDivisionID
         LEFT JOIN CompanyInfo C2
           ON C2.numCompanyID = D2.numCompanyID
         LEFT JOIN (SELECT TOP 1 OM.vcPoppName,
                                 OM.numOppID,
                                 numChildOppID,
                                 vcSource,
                                 numParentProjectID,
                                 vcProjectName
                    FROM   OpportunityLinking
                          LEFT JOIN OpportunityMaster OM
                             ON numOppID = numParentOppID
                          LEFT JOIN [ProjectsMaster]
							 ON numParentProjectID = numProId   
                    WHERE  numChildOppID = @numOppID) Link
           ON Link.numChildOppID = Opp.numOppID
         LEFT OUTER JOIN [Currency] C
					ON C.numCurrencyID = Opp.numCurrencyID
		 LEFT OUTER JOIN [OpportunityRecurring] OPR
					ON OPR.numOppId = Opp.numOppId
		 LEFT OUTER JOIN additionalContactsinformation ADC ON
		 ADC.numdivisionId = D2.numdivisionId AND  ADC.numContactId = Opp.numContactId	
		 LEFT JOIN RecurrenceConfiguration ON Opp.numOppId = RecurrenceConfiguration.numOppID AND RecurrenceConfiguration.numType = 1
		 LEFT JOIN RecurrenceTransaction ON Opp.numOppId = RecurrenceTransaction.numRecurrOppID
		 LEFT JOIN [dbo].[ShippingReport] AS SR ON SR.[numOppID] = [Opp].[numOppId] AND SR.[numDomainID] = [Opp].[numDomainId]
		WHERE  Opp.numOppId = @numOppID
         AND Opp.numDomainID = @numDomainID
/****** Object:  StoredProcedure [dbo].[USP_OPPItemDetails]    Script Date: 09/01/2009 18:59:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- exec USP_OPPItemDetails @numItemCode=31455,@OpportunityId=12387,@numDomainId=110
--created by anooop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_oppitemdetails')
DROP PROCEDURE usp_oppitemdetails
GO
CREATE PROCEDURE [dbo].[USP_OPPItemDetails]
    (
      @numUserCntID NUMERIC(9) = 0,
      @OpportunityId NUMERIC(9) = 0,
      @numDomainId NUMERIC(9),
      @ClientTimeZoneOffset  INT
    )
AS 
    BEGIN
        IF @OpportunityId >0
            BEGIN
                DECLARE @DivisionID AS NUMERIC(9)
                DECLARE @TaxPercentage AS FLOAT
                DECLARE @tintOppType AS TINYINT
                DECLARE @fld_Name AS VARCHAR(500)
                DECLARE @strSQL AS VARCHAR(8000)
                DECLARE @fld_ID AS NUMERIC(9)
                DECLARE @numBillCountry AS NUMERIC(9)
                DECLARE @numBillState AS NUMERIC(9)

                
                SELECT  @DivisionID = numDivisionID,
                        @tintOppType = tintOpptype
                FROM    OpportunityMaster
                WHERE   numOppId = @OpportunityId


               -- Calculate Tax based on Country and state
             /*  SELECT  @numBillState = ISNULL(numState, 0),
						@numBillCountry = ISNULL(numCountry, 0)
			   FROM     dbo.AddressDetails
			   WHERE    numDomainID = @numDomainID
						AND numRecordID = @DivisionID
						AND tintAddressOf = 2
						AND tintAddressType = 1
						AND bitIsPrimary=1
                SET @TaxPercentage = 0
                IF @numBillCountry > 0
                    AND @numBillState > 0 
                    BEGIN
                        IF @numBillState > 0 
                            BEGIN
                                IF EXISTS ( SELECT  COUNT(*)
                                            FROM    TaxDetails
                                            WHERE   numCountryID = @numBillCountry
                                                    AND numStateID = @numBillState
                                                    AND numDomainID = @numDomainID ) 
                                    SELECT  @TaxPercentage = decTaxPercentage
                                    FROM    TaxDetails
                                    WHERE   numCountryID = @numBillCountry
                                            AND numStateID = @numBillState
                                            AND numDomainID = @numDomainID
                                ELSE 
                                    SELECT  @TaxPercentage = decTaxPercentage
                                    FROM    TaxDetails
                                    WHERE   numCountryID = @numBillCountry
                                            AND numStateID = 0
                                            AND numDomainID = @numDomainID
                            END
                    END
                ELSE 
                    IF @numBillCountry > 0 
                        SELECT  @TaxPercentage = decTaxPercentage
                        FROM    TaxDetails
                        WHERE   numCountryID = @numBillCountry
                                AND numStateID = 0
                                AND numDomainID = @numDomainID
               
               */
               

SET @strSQL = ''

--------------Make Dynamic Query--------------
SET @strSQL = @strSQL + '
Select 
Opp.numoppitemtCode,
ISNULL(Opp.bitItemPriceApprovalRequired,0) AS bitItemPriceApprovalRequired,
OM.tintOppType,
Opp.numItemCode,
I.charItemType,
ISNULL(I.bitAsset,0) as bitAsset,
ISNULL(I.bitRental,0) as bitRental,
Opp.numOppId,
Opp.bitWorkOrder,
Opp.fltDiscount,
ISNULL(( SELECT numReturnID FROM   Returns WHERE  numOppItemCode = Opp.numoppitemtCode AND numOppId = Opp.numOppID), 0) AS numReturnID,
dbo.fn_GetReturnItemQty(Opp.numoppitemtCode) AS ReturrnedQty,
[dbo].fn_GetReturnStatus(Opp.numoppitemtCode) AS ReturnStatus,
ISNULL(opp.numUOMId, 0) AS numUOM,
(SELECT vcUnitName FROM dbo.UOM WHERE numUOMId = opp.numUOMId AND ISNULL(bitEnabled,0) = 1)  AS  [vcBaseUOMName],
Opp.numUnitHour AS numOriginalUnitHour, 
isnull(M.vcPOppName,''Internal'') as Source,
numSourceID,
  CASE WHEN ISNULL(bitKitParent,0) = 1
                               AND ISNULL(bitAssembly,0) = 1 THEN 0
                          WHEN ISNULL(bitKitParent,0) = 1 THEN 1
                          ELSE 0
                     END as bitKitParent,
(CASE WHEN (I.charItemType = ''p'' AND ISNULL(bitDropShip, 0) = 0 AND bitAsset=0)THEN dbo.CheckOrderItemInventoryStatus(Opp.numItemCode,Opp.numUnitHour,Opp.numoppitemtCode,isnull(Opp.numWarehouseItmsID,0),(CASE WHEN ISNULL(bitKitParent,0) = 1
                               AND ISNULL(bitAssembly,0) = 1 THEN 0
                          WHEN ISNULL(bitKitParent,0) = 1 THEN 1
                          ELSE 0
                     END)) ELSE 0 END) AS bitBackOrder,
(SELECT vcData FROM dbo.ListDetails WHERE 1=1 AND numListItemID = numShipClass ) AS [numShipClass],ISNULL(Opp.numQtyShipped,0) numQtyShipped,ISNULL(Opp.numUnitHourReceived,0) AS numUnitHourReceived,ISNULL(OM.fltExchangeRate,0) AS fltExchangeRate,ISNULL(OM.numCurrencyID,0) AS numCurrencyID,ISNULL(OM.numDivisionID,0) AS numDivisionID,
isnull(i.numIncomeChartAcntId,0) as itemIncomeAccount,isnull(i.numAssetChartAcntId,0) as itemInventoryAsset,isnull(i.numCOGsChartAcntId,0) as itemCoGs,
ISNULL(Opp.numRentalIN,0) as numRentalIN,
ISNULL(Opp.numRentalLost,0) as numRentalLost,
ISNULL(Opp.numRentalOut,0) as numRentalOut,
(SELECT vcData FROM dbo.ListDetails WHERE 1=1 AND numListItemID = numShipClass ) AS [numShipClass],ISNULL(bitFreeShipping,0) AS [IsFreeShipping],Opp.bitDiscountType,isnull(Opp.monLandedCost,0) as monLandedCost, '

--	IF @fld_Name = 'vcPathForTImage' 
		SET @strSQL = @strSQL + 'Opp.vcPathForTImage,'
--	ELSE IF @fld_Name = 'vcItemName' 
		SET @strSQL = @strSQL + 'Opp.vcItemName,'
--	ELSE IF @fld_Name = 'vcModelID,' 
		SET @strSQL = @strSQL + 'Opp.vcModelID,'
--	ELSE IF @fld_Name = 'vcWareHouse' 
		SET @strSQL = @strSQL + 'vcWarehouse,'
		SET @strSQL = @strSQL + 'WItems.numOnHand,isnull(WItems.numAllocation,0) as numAllocation,'
		SET @strSQL = @strSQL + 'WL.vcLocation,'
--	ELSE IF @fld_Name = 'ItemType' 
		SET @strSQL = @strSQL + 'Opp.vcType ItemType,Opp.vcType,'
--	ELSE IF @fld_Name = 'vcItemDesc' 
		SET @strSQL = @strSQL + 'ISNULL(Opp.vcItemDesc, '''') vcItemDesc,'
--	ELSE IF @fld_Name = 'Attributes' 
		SET @strSQL = @strSQL + 'Opp.vcAttributes,' --dbo.fn_GetAttributes(Opp.numWarehouseItmsID, I.bitSerialized) AS Attributes,
--	ELSE IF @fld_Name = 'bitDropShip' 
		SET @strSQL = @strSQL + 'ISNULL(bitDropShip, 0) as DropShip,CASE ISNULL(bitDropShip, 0) WHEN 1 THEN ''Yes'' ELSE '''' END AS bitDropShip,'
--	ELSE IF @fld_Name = 'numUnitHour' 
		SET @strSQL = @strSQL + 'CAST(( dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), Opp.numItemCode,om.numDomainId, ISNULL(opp.numUOMId, 0))* Opp.numUnitHour ) AS NUMERIC(18, 2)) AS numUnitHour,'
--	ELSE IF @fld_Name = 'monPrice' 
		SET @strSQL = @strSQL + 'Opp.monPrice,'
--	ELSE IF @fld_Name = 'monTotAmount' 
		SET @strSQL = @strSQL + 'Opp.monTotAmount,'
--	ELSE IF @fld_Name = 'vcSKU' 
		SET @strSQL = @strSQL + 'ISNULL(WItems.vcWHSKU,ISNULL(I.vcSKU,'''')) [vcSKU],'
--	ELSE IF @fld_Name = 'vcManufacturer' 
		SET @strSQL = @strSQL + 'Opp.vcManufacturer,'
--	ELSE IF @fld_Name = 'vcItemClassification' 
		SET @strSQL = @strSQL + ' dbo.[fn_GetListItemName](I.[numItemClassification]) numItemClassification,'
--	ELSE IF @fld_Name = 'numItemGroup' 
		SET @strSQL = @strSQL + '( SELECT    [vcItemGroup] FROM      [ItemGroups] WHERE     [numItemGroupID] = I.numItemGroup ) numItemGroup,'
--	ELSE IF @fld_Name = 'vcUOMName' 
		SET @strSQL = @strSQL + 'ISNULL(u.vcUnitName, '''') numUOMId,'
--	ELSE IF @fld_Name = 'fltWeight' 
		SET @strSQL = @strSQL + 'I.fltWeight,'
--	ELSE IF @fld_Name = 'fltHeight' 
		SET @strSQL = @strSQL + 'I.fltHeight,'
--	ELSE IF @fld_Name = 'fltWidth' 
		SET @strSQL = @strSQL + 'I.fltWidth,'
--	ELSE IF @fld_Name = 'fltLength' 
		SET @strSQL = @strSQL + 'I.fltLength,'
--	ELSE IF @fld_Name = 'numBarCodeId' 
		SET @strSQL = @strSQL + 'I.numBarCodeId,'
		SET @strSQL = @strSQL + 'I.IsArchieve,'
--	ELSE IF @fld_Name = 'vcWorkOrderStatus' 
		SET @strSQL = @strSQL + 'CASE WHEN ISNULL(Opp.bitWorkOrder, 0) = 1
				 THEN ( SELECT  dbo.GetListIemName(numWOStatus)
								+ CASE WHEN ISNULL(numWOStatus, 0) = 23184
									   THEN '' - ''
											+ CAST(ISNULL(dbo.fn_GetContactName(numAssignedTo),
														  '''') + '',''
											+ CONVERT(VARCHAR(20), DATEADD(minute, '+ CONVERT(VARCHAR(15),-@ClientTimeZoneOffset)  +', bintCompletedDate)) AS VARCHAR(100))
									   ELSE ''''
								  END
						FROM    WorkOrder
						WHERE   ISNULL(numOppId, 0) = Opp.numOppId
								AND numItemCode = Opp.numItemCode
					  )
				 ELSE ''''
			END AS vcWorkOrderStatus,'
--	ELSE IF @fld_Name = 'DiscAmt' 
		SET @strSQL = @strSQL + 'CASE WHEN Opp.bitDiscountType = 0
				 THEN CAST(ISNULL(Opp.monTotAmtBefDiscount, Opp.monTotAmount)
					  - Opp.monTotAmount AS VARCHAR(10)) + '' (''
					  + CAST(Opp.fltDiscount AS VARCHAR(10)) + ''%)''
				 ELSE CAST(ISNULL(Opp.monTotAmtBefDiscount, Opp.monTotAmount)
					  - Opp.monTotAmount AS VARCHAR(10))
			END AS DiscAmt,'	
--	ELSE IF @fld_Name = 'numProjectID' 
		SET @strSQL = @strSQL + ' ISNULL(Opp.numProjectID, 0) numProjectID,
		 CASE WHEN ( SELECT TOP 1
								ISNULL(OBI.numOppBizDocItemID, 0)
						FROM    dbo.OpportunityBizDocs OBD
								INNER JOIN dbo.OpportunityBizDocItems OBI ON OBD.numOppBizDocsID = OBI.numOppBizDocID
						WHERE   OBI.numOppItemID = Opp.numoppitemtCode
								AND ISNULL(OBD.bitAuthoritativeBizDocs, 0) = 1
								and isnull(OBD.numOppID,0)=OM.numOppID
					  ) > 0 THEN 1
				 ELSE 0
			END AS bitItemAddedToAuthBizDoc,'	
--	ELSE IF @fld_Name = 'numClassID' 
		SET @strSQL = @strSQL + 'ISNULL(Opp.numClassID, 0) numClassID,'	
--	ELSE IF @fld_Name = 'vcProjectStageName' 
		SET @strSQL = @strSQL + ' ISNULL(Opp.numProjectStageID, 0) numProjectStageID,
				CASE WHEN OPP.numProjectStageID > 0
				 THEN dbo.fn_GetProjectStageName(OPP.numProjectId,
												 OPP.numProjectStageID)
				 WHEN OPP.numProjectStageID = -1 THEN ''T&E''
				 WHEN OPP.numProjectId > 0
				 THEN CASE WHEN OM.tintOppType = 1 THEN ''S.O.''
						   ELSE ''P.O.''
					  END
				 ELSE ''-''
			END AS vcProjectStageName,'	
	        
	
	

--------------Add custom fields to query----------------
SELECT TOP 1 @fld_Name= vcFieldName,@fld_ID = numFieldId FROM View_DynamicCustomColumns
WHERE 
grp_id = 5 AND numDomainID=@numDomainId
AND numFormId=26 AND bitCustom=1 AND numUserCntID = @numUserCntID ORDER BY numFieldId
WHILE @fld_ID > 0
BEGIN ------------------

	 PRINT @fld_Name
	
	       SET @strSQL = @strSQL + 'dbo.GetCustFldValueItem('
                                    + CONVERT(VARCHAR(15), @fld_ID)
                                    + ',Opp.numItemCode) as ''' + @fld_Name + ''','
       

SELECT TOP 1 @fld_Name= vcFieldName,@fld_ID = numFieldId FROM View_DynamicCustomColumns
WHERE 
grp_id = 5 AND numDomainID=@numDomainId
AND numFormId=26 AND bitCustom=1  AND numUserCntID = @numUserCntID AND numFieldId > @fld_ID ORDER BY numFieldId
 IF @@ROWCOUNT=0
 SET @fld_ID = 0
END ------------------


                                
--Purchase Opportunity ?
IF @tintOppType = 2 
    BEGIN
        SET @strSQL =  @strSQL + 'ISNULL((SELECT sum(numQty) FROM OppWarehouseSerializedItem WHERE numOppID=OM.numOppID AND numOppItemID=Opp.numoppitemtCode),0) AS SerialLotNo,
		  CAST(Opp.numUnitHour AS NUMERIC(9, 0)) numQty,
		  (SELECT vcUnitName FROM dbo.UOM WHERE numUOMId = opp.numUOMId AND ISNULL(bitEnabled,0) = 1)  AS  [vcBaseUOMName],
		  I.bitSerialized,
		  ISNULL(I.bitLotNo, 0) AS bitLotNo,
		  isnull(Opp.numWarehouseItmsID,0) as numWarehouseItmsID,'
	END
ELSE
	BEGIN	
        SET @strSQL =  @strSQL + ' ISNULL((SELECT sum(numQty) FROM OppWarehouseSerializedItem WHERE numOppID=OM.numOppID AND numOppItemID=Opp.numoppitemtCode),0) AS SerialLotNo,
		   CAST(Opp.numUnitHour AS NUMERIC(9, 0)) numQty,
		  I.bitSerialized,
		  ISNULL(I.bitLotNo, 0) AS bitLotNo,
		  isnull(Opp.numWarehouseItmsID,0) as numWarehouseItmsID,'
	END


        
 SET @strSQL = left(@strSQL,len(@strSQL)-1) + '   
        FROM    OpportunityItems Opp
        LEFT JOIN OpportunityMaster OM ON Opp.numOppID = oM.numOppID
        JOIN item I ON Opp.numItemCode = i.numItemcode
        LEFT JOIN WareHouseItems WItems ON WItems.numWareHouseItemID = Opp.numWarehouseItmsID
        LEFT JOIN Warehouses W ON W.numWarehouseID = WItems.numWarehouseID
        LEFT JOIN WarehouseLocation WL ON WL.numWLocationID = WItems.numWLocationID
        LEFT JOIN OpportunityMaster M ON Opp.numSourceID = M.numOppID
        LEFT JOIN UOM u ON u.numUOMId = opp.numUOMId
WHERE   om.numDomainId = '+ CONVERT(VARCHAR(10),@numDomainId) + '
        AND Opp.numOppId = '+ CONVERT(VARCHAR(15),@OpportunityId) +'
ORDER BY numoppitemtCode ASC '
  
PRINT @strSQL;
EXEC (@strSQL);
SELECT  vcSerialNo,
       vcComments AS Comments,
        numOppItemID AS numoppitemtCode,
        W.numWarehouseItmsDTLID,
        numWarehouseItmsID AS numWItmsID,
        dbo.Fn_getattributes(W.numWarehouseItmsDTLID,
                             1) Attributes
FROM    WareHouseItmsDTL W
        JOIN OppWarehouseSerializedItem O ON O.numWarehouseItmsDTLID = W.numWareHouseItmsDTLID
WHERE   numOppID = @OpportunityId


CREATE TABLE #tempForm (tintOrder TINYINT,vcDbColumnName nvarchar(50),vcOrigDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),
  vcListItemType varchar(3),numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,
  numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,bitIsAlphaNumeric BIT,bitIsNumeric BIT,
  bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),ListRelID numeric(9),intColumnWidth int,intVisible int,vcFieldDataType CHAR(1))


IF
	(
		SELECT 
			COUNT(*)
		FROM 
			View_DynamicColumns DC 
		WHERE 
			DC.numFormId=26 and DC.numDomainID=@numDomainID AND numUserCntID = @numUserCntID AND
			ISNULL(DC.bitSettingField,0)=1 AND ISNULL(DC.bitDeleted,0)=0 AND ISNULL(DC.bitCustom,0)=0) > 0
	OR
	(
		SELECT 
			COUNT(*)
		FROM 
			View_DynamicCustomColumns
		WHERE 
			numFormId=26 AND numUserCntID=@numUserCntID AND numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=0 AND ISNULL(bitCustom,0)=1
	) > 0
BEGIN	

INSERT INTO #tempForm
select isnull(DC.tintRow,0)+1 as tintOrder,DDF.vcDbColumnName,DDF.vcOrigDbColumnName,ISNULL(DDF.vcCultureFieldName,DDF.vcFieldName),                  
 DDF.vcAssociatedControlType,DDF.vcListItemType,DDF.numListID,DDF.vcLookBackTableName                                                 
,DDF.bitCustom,DDF.numFieldId,DDF.bitAllowSorting,DDF.bitInlineEdit,
DDF.bitIsRequired,DDF.bitIsEmail,DDF.bitIsAlphaNumeric,DDF.bitIsNumeric,DDF.bitIsLengthValidation,
DDF.intMaxLength,DDF.intMinLength,DDF.bitFieldMessage,DDF.vcFieldMessage vcFieldMessage,DDF.ListRelID,DDF.intColumnWidth,Case when DC.numFieldID is null then 0 else 1 end,DDF.vcFieldDataType
 FROM View_DynamicDefaultColumns DDF left join View_DynamicColumns DC on DC.numFormId=26 and DDF.numFieldId=DC.numFieldId and DC.numUserCntID=@numUserCntID and DC.numDomainID=@numDomainID and numRelCntType=0 AND tintPageType=1 
 where DDF.numFormId=26 and DDF.numDomainID=@numDomainID AND ISNULL(DDF.bitSettingField,0)=1 AND ISNULL(DDF.bitDeleted,0)=0 AND ISNULL(DDF.bitCustom,0)=0
       
 UNION
    
    select tintRow+1 as tintOrder,vcDbColumnName,vcFieldName,vcFieldName,                  
 vcAssociatedControlType,'' as vcListItemType,numListID,''                                                 
,bitCustom,numFieldId,bitAllowSorting,0 as bitAllowEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,''
 from View_DynamicCustomColumns
 where numFormId=26 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=0
 AND ISNULL(bitCustom,0)=1

END
ELSE
BEGIN
	--Check if Master Form Configuration is created by administrator if NoColumns=0
	DECLARE @IsMasterConfAvailable BIT = 0
	DECLARE @numUserGroup NUMERIC(18,0)

	SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

	IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = 26 AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
	BEGIN
		SET @IsMasterConfAvailable = 1
	END

	--If MasterConfiguration is available then load it otherwise load default columns
	IF @IsMasterConfAvailable = 1
	BEGIN
		INSERT INTO #tempForm
		SELECT 
			(intRowNum + 1) as tintOrder, vcDbColumnName, vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
			vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName,bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,
			bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth,1,vcFieldDataType
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=26 AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcFieldName,vcFieldName,                  
			 vcAssociatedControlType,'' as vcListItemType,numListID,''                                                 
			,bitCustom,numFieldId,bitAllowSorting,0 as bitAllowEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,''
		FROM 
			View_DynamicCustomColumnsMasterConfig
		WHERE 
			View_DynamicCustomColumnsMasterConfig.numFormId=26 AND 
			View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1
		ORDER BY 
			tintOrder ASC  
	END
END


select * from #tempForm order by tintOrder

drop table #tempForm

/*
                        SET @strSQL = 'select  Opp.numoppitemtCode,Opp.numOppId,
                               Opp.numItemCode,                                    
                               Opp.vcItemName,                                                                    
                               ISNULL(Opp.vcItemDesc,'''') as [Desc],                                                                    
                               Opp.vcType ItemType,                                   
                               Opp.vcPathForTImage,
                               Opp.vcModelID,
							    CAST((dbo.fn_UOMConversion(ISNULL(I.numBaseUnit,0),Opp.numItemCode,om.numDomainId,ISNULL(opp.numUOMId,0)) * Opp.numUnitHour) as numeric(18,2)) as numUnitHour,   
                               Opp.monPrice,                                                                    
                               Opp.monTotAmount,                                                     
							   ISNULL(Opp.vcItemDesc,'''') vcItemDesc,
							   Opp.vcManufacturer,
							   I.charItemType,
							   I.bitKitParent,
							   I.bitAssembly,
							   I.vcSKU,                                   
							   I.numBarCodeId,
							   dbo.[fn_GetListItemName](I.[numItemClassification]) vcClassification,
							   (SELECT [vcItemGroup] FROM [ItemGroups] WHERE [numItemGroupID] =I.numItemGroup) vcItemGroup,
							   ISNULL(I.vcUnitofMeasure,'''') vcUnitofMeasure,
							   I.fltWeight,I.fltHeight,I.fltWidth,I.fltLength,
							   WItems.numWarehouseID,
                               Opp.numWarehouseItmsID,isnull(Opp.numToWarehouseItemID,0) numToWarehouseItemID,bitDropShip DropShip, CASE ISNULL(bitDropShip,0) when 1 then ''Yes'' else '''' end as vcDropShip,
                               0 as Op_Flag,isnull(Opp.bitDiscountType,0) as bitDiscountType,isnull(Opp.fltDiscount,0) as fltDiscount,isnull(monTotAmtBefDiscount,0) as monTotAmtBefDiscount,                                    
                               vcWarehouse as Warehouse,I.bitSerialized,isnull(I.bitLotNo,0) as bitLotNo,vcAttributes, 
                               dbo.fn_GetReturnItemQty(Opp.numoppitemtCode) as ReturrnedQty,
                               [dbo].fn_GetReturnStatus(Opp.numoppitemtCode) as ReturnStatus,
							   isnull((SELECT numReturnID FROM Returns WHERE numOppItemCode=Opp.numoppitemtCode and numOppId=Opp.numOppID),0) as numReturnID,
                               isnull(v.monCost,0) as VendorCost,
                               bitTaxable,
                               case when bitTaxable =0 then 0 when bitTaxable=1 then '
                            + CONVERT(VARCHAR(15), @TaxPercentage)
                            + ' end as Tax,                                                
                                   isnull(M.vcPOppName,''Internal'') as Source,dbo.fn_GetAttributes(Opp.numWarehouseItmsID,I.bitSerialized) as Attributes  ,numSourceID '
                            + CASE WHEN @strSQL = '' THEN ''
                                   ELSE ',' + @strSQL
                              END
                            + ',isnull(opp.numUOMId,0) AS numUOM,ISNULL(u.vcUnitName,'''') vcUOMName,dbo.fn_UOMConversion(opp.numUOMId,Opp.numItemCode,om.numDomainId,null) AS UOMConversionFactor
                          ,case when Opp.bitDiscountType=0 then  cast(isnull(Opp.monTotAmtBefDiscount,Opp.monTotAmount)-Opp.monTotAmount AS VARCHAR(10)) +  '' ('' + CAST(Opp.fltDiscount AS VARCHAR(10)) +  ''%)'' 
                          ELSE cast(isnull(Opp.monTotAmtBefDiscount,Opp.monTotAmount)-Opp.monTotAmount AS VARCHAR(10)) end as DiscAmt
                           ,isnull(I.numVendorID,0) numVendorID,OM.tintOppType,cast(Opp.numUnitHour as numeric(9,0)) as numQty,SUBSTRING(
(SELECT '','' + vcSerialNo + CASE WHEN isnull(I.bitLotNo,0)=1 THEN '' ('' + CONVERT(VARCHAR(15),whi.numQty) + '')'' ELSE '''' end 
FROM OppWarehouseSerializedItem oppI JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID=whi.numWareHouseItmsDTLID where oppI.numOppID=Opp.numOppId and oppI.numOppItemID=Opp.numoppitemtCode 
ORDER BY vcSerialNo FOR XML PATH('''')),2,200000) AS SerialLotNo, ISNULL(Opp.bitWorkOrder,0) as bitWorkOrder,
CASE WHEN ISNULL(Opp.bitWorkOrder,0)=1 then (select dbo.GetListIemName(numWOStatus) + CASE WHEN isnull(numWOStatus,0)=23184 THEN
'' - '' + CAST(ISNULL(dbo.fn_GetContactName(numAssignedTo), '''')+'',''+convert(varchar(20),dateadd(minute,' + CONVERT(VARCHAR(15),-@ClientTimeZoneOffset) + ',bintCompletedDate)) AS VARCHAR(100)) ELSE '''' end
 from WorkOrder where isnull(numOppId,0)=Opp.numOppId and numItemCode=Opp.numItemCode) else '''' end as vcWorkOrderStatus,
 ISNULL(Opp.numVendorWareHouse,0) as numVendorWareHouse,ISNULL(Opp.numShipmentMethod,0) as numShipmentMethod,ISNULL(Opp.numSOVendorId,0) as numSOVendorId,Opp.numUnitHour as numOriginalUnitHour,
 isnull(Opp.numProjectID,0) numProjectID,isnull(Opp.numClassID,0) numClassID,isnull(Opp.numProjectStageID,0) numProjectStageID,Case When OPP.numProjectStageID>0 then dbo.fn_GetProjectStageName(OPP.numProjectId,OPP.numProjectStageID) when OPP.numProjectStageID=-1 then ''T&E'' When OPP.numProjectId>0 then case when OM.tintOppType=1 then ''S.O.'' else ''P.O.'' end else ''-'' end as vcProjectStageName,
 CASE WHEN (SELECT TOP 1 ISNULL(OBI.numOppBizDocItemID,0) FROM dbo.OpportunityBizDocs OBD INNER JOIN dbo.OpportunityBizDocItems OBI ON OBD.numOppBizDocsID = OBI.numOppBizDocID WHERE OBI.numOppItemID = Opp.numoppitemtCode AND ISNULL(OBD.bitAuthoritativeBizDocs,0)=1 ) > 0
      THEN 1 ELSE 0 END AS bitItemAddedToAuthBizDoc,isnull(I.numItemClass,0) as numItemClass
                              from OpportunityItems Opp                    
                               left join OpportunityMaster OM                                                            
                                  on Opp.numOppID=oM.numOppID                                                                     
                                 join item I                                                                    
                                  on Opp.numItemCode=i.numItemcode                                            
                                left join Vendor v 
									on v.numVendorID= I.numVendorID and I.numItemCode =v.numItemCode
                               left join  WareHouseItems  WItems                                        
                               on   WItems.numWareHouseItemID= Opp.numWarehouseItmsID                                            
                               left join  Warehouses  W                                            
                                  on   W.numWarehouseID= WItems.numWarehouseID                                         
                                  left join OpportunityMaster M                                                            
                                  on Opp.numSourceID=M.numOppID  
                                  LEFT JOIN  UOM u ON u.numUOMId = opp.numUOMId  
                                  where om.numDomainId ='
                            + CONVERT(VARCHAR(15), @numDomainId)
                            + ' and Opp.numOppId='+ CONVERT(VARCHAR(15), @OpportunityId) +  
                            + CASE WHEN @numItemCode > 0 THEN ' AND Opp.numOppItemTCode = ' + CONVERT(VARCHAR(15), @numItemCode)
                                   ELSE ''
                              END
                            +
                            ' order by numoppitemtCode ASC '
                        PRINT @strSQL
                        EXEC ( @strSQL
                            )
                        SELECT  vcSerialNo,
                                vcComments AS Comments,
                                numOppItemID AS numoppitemtCode,
                                W.numWarehouseItmsDTLID,
                                numWarehouseItmsID AS numWItmsID,
                                dbo.Fn_getattributes(W.numWarehouseItmsDTLID,
                                                     1) Attributes
                        FROM    WareHouseItmsDTL W
                                JOIN OppWarehouseSerializedItem O ON O.numWarehouseItmsDTLID = W.numWareHouseItmsDTLID
                        WHERE   numOppID = @OpportunityId
*/
      /*              END
                ELSE 
                    BEGIN
                        SELECT  @numDomainID = numDomainId
                        FROM    OpportunityMaster
                        WHERE   numOppID = @OpportunityId
                        SET @strSQL = ''
                        SELECT TOP 1
                                @fld_ID = Fld_ID,
                                @fld_Name = Fld_label 
                        FROM    CFW_Fld_Master
                        WHERE   grp_id = 5
                                AND numDomainID = @numDomainID
                        WHILE @fld_ID > 0
                            BEGIN
                                SET @strSQL =@strSQL + 'dbo.GetCustFldValueItem('
                                    + CONVERT(VARCHAR(15), @fld_ID)
                                    + ',Opp.numItemCode) as ''' + @fld_Name + ''''
                                SELECT TOP 1
                                        @fld_ID = Fld_ID,
                                        @fld_Name = Fld_label
                                FROM    CFW_Fld_Master
                                WHERE   grp_id = 5
                                        AND numDomainID = @numDomainID
                                        AND Fld_ID > @fld_ID
                                IF @@ROWCOUNT = 0 
                                    SET @fld_ID = 0
                                IF @fld_ID <> 0 
                                    SET @strSQL = @strSQL + ','
                            END
                        SET @strSQL = 'select  Opp.numoppitemtCode,Opp.numOppId,                                       
                                Opp.numItemCode,                                    
                               Opp.vcItemName,                                                                    
                               ISNULL(Opp.vcItemDesc,'''') as [Desc],                                                                    
                               Opp.vcType ItemType,                                   
                               Opp.vcPathForTImage,
                               Opp.vcModelID,
                              CAST((dbo.fn_UOMConversion(ISNULL(I.numBaseUnit,0),Opp.numItemCode,om.numDomainId,ISNULL(opp.numUOMId,0)) * Opp.numUnitHour) as numeric(18,2)) as numUnitHour,   
							   Opp.monPrice,                                                                    
                               Opp.monTotAmount,                                                     
							   ISNULL(Opp.vcItemDesc,'''') vcItemDesc,
							   Opp.vcManufacturer,
							   I.charItemType,
							   I.bitKitParent,
							   I.bitAssembly,
							   I.vcSKU,
							   I.numBarCodeId,
							   dbo.[fn_GetListItemName](I.[numItemClassification]) vcClassification,
							   (SELECT [vcItemGroup] FROM [ItemGroups] WHERE [numItemGroupID] =I.numItemGroup) vcItemGroup,
							   ISNULL(I.vcUnitofMeasure,'''') vcUnitofMeasure,
							   I.fltWeight,I.fltHeight,I.fltWidth,I.fltLength,
							   WItems.numWarehouseID,
                               Opp.numWarehouseItmsID,isnull(Opp.numToWarehouseItemID,0) numToWarehouseItemID,bitDropShip DropShip, CASE ISNULL(bitDropShip,0) when 1 then ''Yes'' else '''' end as vcDropShip,
                               0 as Op_Flag,isnull(Opp.bitDiscountType,0) bitDiscountType,isnull(Opp.fltDiscount,0) fltDiscount,isnull(monTotAmtBefDiscount,0) monTotAmtBefDiscount,                                           
                               vcWarehouse as Warehouse,I.bitSerialized,isnull(I.bitLotNo,0) as bitLotNo,vcAttributes,
                               dbo.fn_GetReturnItemQty(Opp.numoppitemtCode) as ReturrnedQty,
                               [dbo].fn_GetReturnStatus(Opp.numoppitemtCode) as ReturnStatus,
							   isnull((SELECT numReturnID FROM Returns WHERE numOppItemCode=Opp.numoppitemtCode and numOppId=Opp.numOppID),0) as numReturnID,
                               isnull(v.monCost,0) as VendorCost,
                               bitTaxable,
                               case when bitTaxable =0 then 0 when bitTaxable=1 then '
                            + CONVERT(VARCHAR(15), @TaxPercentage)
                            + ' end as Tax,                                                
                                   isnull(m.vcPOppName,''Internal'') as Source,dbo.fn_GetAttributes(Opp.numWarehouseItmsID,I.bitSerialized) as Attributes ,numSourceID '
                            + CASE WHEN @strSQL = '' THEN ''
                                   ELSE ',' + @strSQL
                              END
                            + ',isnull(opp.numUOMId,0) AS numUOM,ISNULL(u.vcUnitName,'''') vcUOMName,dbo.fn_UOMConversion(opp.numUOMId,Opp.numItemCode,om.numDomainId,null) AS UOMConversionFactor
                          ,case when Opp.bitDiscountType=0 then  cast(isnull(Opp.monTotAmtBefDiscount,Opp.monTotAmount)-Opp.monTotAmount AS VARCHAR(10)) +  '' ('' + CAST(Opp.fltDiscount AS VARCHAR(10)) +  ''%)'' 
                          ELSE cast(isnull(Opp.monTotAmtBefDiscount,Opp.monTotAmount)-Opp.monTotAmount AS VARCHAR(10)) end as DiscAmt
                         ,isnull(I.numVendorID,0) numVendorID,OM.tintOppType,cast(Opp.numUnitHour as numeric(9,0)) numQty,SUBSTRING(
(SELECT '','' + vcSerialNo + CASE WHEN isnull(I.bitLotNo,0)=1 THEN '' ('' + CONVERT(VARCHAR(15),whi.numQty) + '')'' ELSE '''' end 
FROM OppWarehouseSerializedItem oppI JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID=whi.numWareHouseItmsDTLID where oppI.numOppID=Opp.numOppId and oppI.numOppItemID=Opp.numoppitemtCode 
ORDER BY vcSerialNo FOR XML PATH('''')),2,200000) AS SerialLotNo, ISNULL(Opp.bitWorkOrder,0) as bitWorkOrder,
CASE WHEN ISNULL(Opp.bitWorkOrder,0)=1 then (select dbo.GetListIemName(numWOStatus) + CASE WHEN isnull(numWOStatus,0)=23184 THEN
'' - '' + CAST(ISNULL(dbo.fn_GetContactName(numAssignedTo), '''')+'',''+convert(varchar(20),dateadd(minute,'+ CONVERT(VARCHAR(15),-@ClientTimeZoneOffset) +',bintCompletedDate)) AS VARCHAR(100)) ELSE '''' end
 from WorkOrder where isnull(numOppId,0)=Opp.numOppId and numItemCode=Opp.numItemCode) else '''' end as vcWorkOrderStatus,
 ISNULL(Opp.numVendorWareHouse,0) as numVendorWareHouse,ISNULL(Opp.numShipmentMethod,0) as numShipmentMethod,ISNULL(Opp.numSOVendorId,0) as numSOVendorId,Opp.numUnitHour as numOriginalUnitHour,
 isnull(Opp.numProjectID,0) numProjectID,isnull(Opp.numClassID,0) numClassID,isnull(Opp.numProjectStageID,0) numProjectStageID,Case When OPP.numProjectStageID>0 then dbo.fn_GetProjectStageName(OPP.numProjectId,OPP.numProjectStageID) when OPP.numProjectStageID=-1 then ''T&E'' When OPP.numProjectId>0 then case when OM.tintOppType=1 then ''S.O.'' else ''P.O.'' end else ''-'' end as vcProjectStageName,
 CASE WHEN (SELECT TOP 1 ISNULL(OBI.numOppBizDocItemID,0) FROM dbo.OpportunityBizDocs OBD INNER JOIN dbo.OpportunityBizDocItems OBI ON OBD.numOppBizDocsID = OBI.numOppBizDocID WHERE OBI.numOppItemID = Opp.numoppitemtCode AND ISNULL(OBD.bitAuthoritativeBizDocs,0)=1 ) > 0
 THEN 1 ELSE 0 END AS bitItemAddedToAuthBizDoc,isnull(I.numItemClass,0) as numItemClass
              from OpportunityItems Opp                    
                               left join OpportunityMaster OM                                                            
                                  on Opp.numOppID=oM.numOppID                                    
                                 join item I                                                                    
                                  on Opp.numItemCode=i.numItemcode   
                                  left join Vendor v 
									on v.numVendorID= I.numVendorID and I.numItemCode =v.numItemCode
                                left join  WareHouseItems  WItems                                        
                                on   WItems.numWareHouseItemID= Opp.numWarehouseItmsID                                            
                                left join  Warehouses  W                                            
                                  on   W.numWarehouseID= WItems.numWarehouseID                                         
                                  left join OpportunityMaster M                                                            
                                  on Opp.numSourceID=M.numOppID                      
                                        LEFT JOIN  UOM u ON u.numUOMId = opp.numUOMId
                                  where om.numDomainId ='
                            + CONVERT(VARCHAR(15), @numDomainId)
                            + ' and Opp.numOppId='+ CONVERT(VARCHAR(15), @OpportunityId) +  
                            + CASE WHEN @numItemCode > 0 THEN ' AND Opp.numOppItemTCode = ' + CONVERT(VARCHAR(15), @numItemCode)
                                   ELSE ''
                              END
                            +
                            ' order by numoppitemtCode ASC '
                        PRINT @strSQL
                        EXEC ( @strSQL
                            )
                        SELECT  vcSerialNo,
                                vcComments AS Comments,
                                numOppItemID AS numoppitemtCode,
                                W.numWarehouseItmsDTLID,
                                numWarehouseItmsID AS numWItmsID,
                                dbo.Fn_getattributes(W.numWarehouseItmsDTLID,
                                                     1) Attributes
                        FROM    OppWarehouseSerializedItem O
                                LEFT JOIN WareHouseItmsDTL W ON O.numWarehouseItmsDTLID = W.numWareHouseItmsDTLID
                        WHERE   numOppID = @OpportunityId
                    END
            */
            END
            
    END
/****** Object:  StoredProcedure [dbo].[USP_OppManage]    Script Date: 07/26/2008 16:20:19 ******/
--2 changes
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OppManage')
DROP PROCEDURE usp_oppmanage
GO
CREATE PROCEDURE [dbo].[USP_OppManage] 
(                                                                          
  @numOppID as numeric=0 OUTPUT,                                                                          
  @numContactId numeric(9)=null,                                                                          
  @numDivisionId numeric(9)=null,                                                                          
  @tintSource numeric(9)=null,                                                                          
  @vcPOppName Varchar(100)='' OUTPUT,                                                                          
  @Comments varchar(1000)='',                                                                          
  @bitPublicFlag bit=0,                                                                          
  @numUserCntID numeric(9)=0,                                                                                 
  @monPAmount money =0,                                                 
  @numAssignedTo as numeric(9)=0,                                                                                                        
  @numDomainId numeric(9)=0,                                                                                                                                                                                   
  @strItems as text=null,                                                                          
  @strMilestone as text=null,                                                                          
  @dtEstimatedCloseDate datetime,                                                                          
  @CampaignID as numeric(9)=null,                                                                          
  @lngPConclAnalysis as numeric(9)=null,                                                                         
  @tintOppType as tinyint,                                                                                                                                             
  @tintActive as tinyint,                                                              
  @numSalesOrPurType as numeric(9),              
  @numRecurringId as numeric(9),
  @numCurrencyID as numeric(9)=0,
  @DealStatus as tinyint =0,-- 0=Open,1=Won,2=Lost
  @numStatus AS NUMERIC(9),
  @vcOppRefOrderNo VARCHAR(100),
  @numBillAddressId numeric(9)=0,
  @numShipAddressId numeric(9)=0,
  @bitStockTransfer BIT=0,
  @WebApiId INT = 0,
  @tintSourceType TINYINT=0,
  @bitDiscountType as bit,
  @fltDiscount  as float,
  @bitBillingTerms as bit,
  @intBillingDays as integer,
  @bitInterestType as bit,
  @fltInterest as float,
  @tintTaxOperator AS TINYINT,
  @numDiscountAcntType AS NUMERIC(9),
  @vcWebApiOrderNo VARCHAR(100)=NULL,
  @vcCouponCode		VARCHAR(100) = '',
  @vcMarketplaceOrderID VARCHAR(100)=NULL,
  @vcMarketplaceOrderDate  datetime=NULL,
  @vcMarketplaceOrderReportId VARCHAR(100)=NULL,
  @numPercentageComplete NUMERIC(9),
  @bitUseShippersAccountNo BIT = 0,
  @bitUseMarkupShippingRate BIT = 0,
  @numMarkupShippingRate NUMERIC(19,2) = 0,
  @intUsedShippingCompany INT = 0
)                                                                          
                                                                          
as  

 DECLARE @fltExchangeRate float                                 
 DECLARE @hDocItem int                                                                                                                            
 DECLARE @TotalAmount as money                                                                          
 DECLARE @tintOppStatus as tinyint               
 DECLARE @tintDefaultClassType NUMERIC
 DECLARE @numDefaultClassID NUMERIC;SET @numDefaultClassID=0
 
 --Get Default Item Class for particular user based on Domain settings  
 SELECT @tintDefaultClassType=isnull(tintDefaultClassType,0) FROM dbo.Domain  WHERE numDomainID = @numDomainID 

 IF @tintDefaultClassType=0
      SELECT @numDefaultClassID=NULLIF(numDefaultClass,0) FROM dbo.UserMaster  WHERE numUserDetailId =@numUserCntID AND numDomainID = @numDomainID 


--If new Oppertunity
if @numOppID = 0                                                                          
 begin     
  declare @intOppTcode as numeric(9)                           
  Select @intOppTcode=max(numOppId)from OpportunityMaster                
  set @intOppTcode =isnull(@intOppTcode,0) + 1                                                
  set @vcPOppName=@vcPOppName+ '-'+convert(varchar(4),year(getutcdate()))  +'-A'+ convert(varchar(10),@intOppTcode)                                  
  
  IF ISNULL(@numCurrencyID,0) = 0 
	BEGIN
	 SET @fltExchangeRate=1
	 SELECT @numCurrencyID= ISNULL(numCurrencyID,0) FROM dbo.Domain WHERE numDomainId=@numDomainId	
	END
  else set @fltExchangeRate=dbo.GetExchangeRate(@numDomainID,@numCurrencyID)
                                                                       
  --Set Default Class If enable User Level Class Accountng 
  DECLARE @numAccountClass AS NUMERIC(18);SET @numAccountClass=0
  SELECT @numAccountClass=ISNULL(numDefaultClass,0) FROM dbo.UserMaster UM JOIN dbo.Domain D ON UM.numDomainID=D.numDomainId
  WHERE D.numDomainId=@numDomainId AND UM.numUserDetailId=@numUserCntID AND ISNULL(D.IsEnableUserLevelClassTracking,0)=1
                                                                       
  insert into OpportunityMaster                                                                          
  (                                                                             
  numContactId,                                                                   
  numDivisionId,                                                                          
  txtComments,                                             
  numCampainID,                                                                          
  bitPublicFlag,                                                                          
  tintSource,
  tintSourceType,                                                             
  vcPOppName,                                                                          
  intPEstimatedCloseDate,                                                                          
  monPAmount,                                                                           
  numCreatedBy,                                                                          
  bintCreatedDate,                                                                           
  numModifiedBy,                                                                          
  bintModifiedDate,                                                                          
  numDomainId,                                                                                                             
  numRecOwner,                                                                          
  lngPConclAnalysis,                                                                         
  tintOppType,                                                              
  numSalesOrPurType,
  numCurrencyID,
  fltExchangeRate,
  numAssignedTo,
  numAssignedBy,
  [tintOppStatus],
  numStatus,
  vcOppRefOrderNo,
  --vcWebApiOrderNo,
  bitStockTransfer,bitBillingTerms,intBillingDays,bitInterestType,fltInterest,vcCouponCode,
  bitDiscountType,fltDiscount,bitPPVariance,vcMarketplaceOrderID,vcMarketplaceOrderReportId,
  numPercentageComplete,numAccountClass,tintTaxOperator,bitUseShippersAccountNo,
  bitUseMarkupShippingRate,
  numMarkupShippingRate,
  intUsedShippingCompany
  )                                                                          
  Values                                                                          
  (                                                                          
  @numContactId,                                                                          
  @numDivisionId,                                                                          
  @Comments,                           
  @CampaignID,                                                                          
  @bitPublicFlag,                     
  @tintSource,
  @tintSourceType,                         
  @vcPOppName,                                                                          
  @dtEstimatedCloseDate,                                                                          
  @monPAmount,                                                                                
  @numUserCntID,                                                                          
 CASE @tintSourceType WHEN 3 THEN @vcMarketplaceOrderDate ELSE GETUTCDATE() END,                                                                          
  @numUserCntID,                                                           
  getutcdate(),                                                                          
  @numDomainId,                                                                                   
  @numUserCntID,                                                                          
  @lngPConclAnalysis,                                                                                                                                                                      
  @tintOppType,                                                                                                                                      
  @numSalesOrPurType,              
  @numCurrencyID, 
  @fltExchangeRate,
  case when @numAssignedTo>0 then @numAssignedTo else null end,
  case when @numAssignedTo>0 then @numUserCntID else null   END,
  @DealStatus,
  @numStatus,
  @vcOppRefOrderNo,
 -- @vcWebApiOrderNo,
  @bitStockTransfer,@bitBillingTerms,@intBillingDays,@bitInterestType,@fltInterest,@vcCouponCode,
  @bitDiscountType,@fltDiscount,1,@vcMarketplaceOrderID,@vcMarketplaceOrderReportId,
  @numPercentageComplete,@numAccountClass,@tintTaxOperator,@bitUseShippersAccountNo,
  @bitUseMarkupShippingRate,
  @numMarkupShippingRate,
  @intUsedShippingCompany
    )                                                                                                                      
  set @numOppID=scope_identity()                                                
  
  --Update OppName as per Name Template
  EXEC dbo.USP_UpdateNameTemplateValue @tintOppType,@numDomainID,@numOppID

  --Map Custom Field	
  DECLARE @tintPageID AS TINYINT;
  SET @tintPageID=CASE WHEN @tintOppType=1 THEN 2 ELSE 6 END 
  	
  EXEC dbo.USP_AddParentChildCustomFieldMap
	@numDomainID = @numDomainID, --  numeric(18, 0)
	@numRecordID = @numOppID, --  numeric(18, 0)
	@numParentRecId = @numDivisionId, --  numeric(18, 0)
	@tintPageID = @tintPageID --  tinyint
 	
  
	IF ISNULL(@numStatus,0) > 0 AND @tintOppType=1 AND isnull(@DealStatus,0)=1
	   BEGIN
	   		EXEC USP_ManageOpportunityAutomationQueue
					@numOppQueueID = 0, -- numeric(18, 0)
					@numDomainID = @numDomainID, -- numeric(18, 0)
					@numOppId = @numOppID, -- numeric(18, 0)
					@numOppBizDocsId = 0, -- numeric(18, 0)
					@numOrderStatus = @numStatus, -- numeric(18, 0)
					@numUserCntID = @numUserCntID, -- numeric(18, 0)
					@tintProcessStatus = 1, -- tinyint
					@tintMode = 1 -- TINYINT
	   END
  ---- inserting Items                                                                          
   
                                                   
  if convert(varchar(10),@strItems) <>'' AND @numOppID>0
  begin                      
   EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems       
	
  insert into                       
   OpportunityItems                                                                          
   (numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,vcItemName,vcModelID,vcManufacturer,vcPathForTImage,monVendorCost,numUOMId,bitWorkOrder,
   numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,numClassID,numToWarehouseItemID,vcAttributes,monAvgCost,bitItemPriceApprovalRequired)
   select @numOppID,X.numItemCode,dbo.fn_UOMConversion(X.numUOM,numItemCode,@numDomainId,null) * x.numUnitHour,
   x.monPrice,x.monTotAmount,X.vcItemDesc,
   CASE X.numWarehouseItmsID WHEN -1 
							 THEN (SELECT [numWareHouseItemID] FROM [WareHouseItems] 
								   WHERE [numItemID] = X.numItemCode 
								   AND ISNULL(WareHouseItems.vcWHSKU,'') = ISNULL(X.vcSKU,'')
								   AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail 
														  WHERE numDomainID =@numDomainId 
														  AND WebApiId = @WebApiId))  
							 WHEN 0 
							 THEN (SELECT TOP 1 [numWareHouseItemID] FROM [WareHouseItems] 
								   WHERE [numItemID] = X.numItemCode
								   AND ISNULL(WareHouseItems.vcWHSKU,'') = ISNULL(X.vcSKU,'')) 
							 ELSE  X.numWarehouseItmsID 
	END AS numWarehouseItmsID,
	X.ItemType,X.DropShip,X.bitDiscountType,
   X.fltDiscount,X.monTotAmtBefDiscount,X.vcItemName,
   (SELECT vcModelID FROM item WHERE numItemCode = X.numItemCode),
   (SELECT vcManufacturer FROM item WHERE numItemCode = X.numItemCode),
   (SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = X.numItemCode AND BitDefault = 1 AND numdomainId = @numdomainId),
   (select isnull(VN.monCost,0) from Vendor VN, Item IT where IT.numItemCode=VN.numItemCode and  VN.numItemCode=X.numItemCode and  VN.numVendorID=IT.numVendorID),
   X.numUOM,X.bitWorkOrder,X.numVendorWareHouse,X.numShipmentMethod,X.numSOVendorId,X.numProjectID,X.numProjectStageID,Case When @tintDefaultClassType=0 then @numDefaultClassID else (SELECT numItemClass FROM item WHERE numItemCode = X.numItemCode) end,X.numToWarehouseItemID,X.Attributes,
   (SELECT ISNULL(monAverageCost,0) FROM Item I WHERE I.numDomainId=@numDomainId AND I.numItemCode=X.numItemCode),X.bitItemPriceApprovalRequired from(
   SELECT *FROM OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
   WITH                       
   (                                                                          
   numoppitemtCode numeric(9),                                     
   numItemCode numeric(9),                                                                          
   numUnitHour numeric(9,2),                                                                          
   monPrice money,                                                                       
   monTotAmount money,                                                                          
   vcItemDesc varchar(1000),                                    
   numWarehouseItmsID numeric(9),                          
   ItemType varchar(30),      
   DropShip bit,
   bitDiscountType bit,
   fltDiscount float,
   monTotAmtBefDiscount MONEY,
   vcItemName varchar(300),numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9) ,numSOVendorId numeric(9),numProjectID numeric(9),numProjectStageID numeric(9),numToWarehouseItemID NUMERIC(9),Attributes varchar(200),vcSKU VARCHAR(100),bitItemPriceApprovalRequired BIT
   ))X    
    
    
    
   Update OpportunityItems                       
   set numItemCode=X.numItemCode,    
   numOppId=@numOppID,                     
   numUnitHour=dbo.fn_UOMConversion(X.numUOM,X.numItemCode,@numDomainId,null) * X.numUnitHour,                      
   monPrice=x.monPrice,                      
   monTotAmount=x.monTotAmount,                                  
   vcItemDesc=X.vcItemDesc,                      
   numWarehouseItmsID=X.numWarehouseItmsID,                 
   bitDropShip=X.DropShip,
   bitDiscountType=X.bitDiscountType,
   fltDiscount=X.fltDiscount,
   monTotAmtBefDiscount=X.monTotAmtBefDiscount,vcItemName=X.vcItemName,numUOMId=numUOM,bitWorkOrder=X.bitWorkOrder,
   numVendorWareHouse=X.numVendorWareHouse,numShipmentMethod=X.numShipmentMethod,numSOVendorId=X.numSOVendorId,numProjectId=X.numProjectID,numProjectStageId=X.numProjectStageID,numToWarehouseItemID = X.numToWarehouseItemID,
vcAttributes=X.Attributes,numClassID=(Case When @tintDefaultClassType=0 then @numDefaultClassID else (SELECT numItemClass FROM item WHERE numItemCode = X.numItemCode) END),bitItemPriceApprovalRequired = X.bitItemPriceApprovalRequired
--   ,vcModelID=(SELECT vcModelID FROM item WHERE numItemCode = X.numItemCode),vcManufacturer=(SELECT vcManufacturer FROM item WHERE numItemCode = X.numItemCode)
--  ,vcPathForTImage=(SELECT vcPathForTImage FROM item WHERE numItemCode = X.numItemCode),monVendorCost=(select isnull(VN.monCost,0) from Vendor VN, Item IT where IT.numItemCode=VN.numItemCode and  VN.numItemCode=X.numItemCode and  VN.numVendorID=IT.numVendorID)
   from (SELECT numoppitemtCode as numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,DropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,vcItemName,numUOM,bitWorkOrder,
   numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,numToWarehouseItemID,Attributes,bitItemPriceApprovalRequired
   FROM OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=2]',2)
   WITH  (                      
    numoppitemtCode numeric(9),                                     
    numItemCode numeric(9),                                                                          
    numUnitHour numeric(9,2),                                                                          
    monPrice money,                                         
    monTotAmount money,                                                                          
    vcItemDesc varchar(1000),                                    
    numWarehouseItmsID numeric(9),      
    DropShip bit,
	bitDiscountType bit,
    fltDiscount float,
    monTotAmtBefDiscount MONEY,vcItemName varchar(300),numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId numeric(9),numProjectID numeric(9),numProjectStageID numeric(9),numToWarehouseItemID NUMERIC(9),Attributes varchar(200),bitItemPriceApprovalRequired BIT
   ))X where numoppitemtCode=X.numOppItemID                                          
	
	
   -- Update UOM of opportunity items have not UOM set while exported from Marketplace (means tintSourceType = 3)
	IF ISNULL(@tintSourceType,0) = 3
	BEGIN
		--SELECT * FROM OpportunityMaster WHERE numOppId = 81654
		--SELECT * FROM OpportunityItems WHERE numOppId = 81654
		--SELECT * FROM Item WHERE numItemCode = 822078

		UPDATE OI SET numUOMID = ISNULL(I.numSaleUnit,0), 
					  numUnitHour = dbo.fn_UOMConversion(ISNULL(I.numSaleUnit,0),OI.numItemCode,@numDomainId,null) * OI.numUnitHour
		FROM OpportunityItems OI
		JOIN Item I ON OI.numItemCode = I.numItemCode
		WHERE numOppId = @numOppID
		AND I.numDomainID = @numDomainId

	END	
   --Update OpportunityItems
   --set 
   --FROM (SELECT numItemCode,vcItemName,[txtItemDesc],vcModelID FROM item WHERE numItemCode IN (SELECT [numItemCode] FROM OpportunityItems WHERE numOppID = @numOppID))X
   --WHERE OpportunityItems.[numItemCode] = X.numItemCode 
                                     
   insert into OppWarehouseSerializedItem                                                                          
   (numOppID,numWarehouseItmsDTLID,numOppItemID,numWarehouseItmsID)                      
   select @numOppID,X.numWarehouseItmsDTLID,X.numoppitemtCode,X.numWItmsID from(                                                                          
   SELECT * FROM OPENXML (@hDocItem,'/NewDataSet/SerialNo',2)                                                                          
   WITH                        
   (                                               
   numWarehouseItmsDTLID numeric(9),                      
   numoppitemtCode numeric(9),                      
   numWItmsID numeric(9)                                                     
   ))X                                    
               
                                 
   update OppWarehouseSerializedItem                       
   set numOppItemID=X.numoppitemtCode                                
   from                       
   (SELECT numWarehouseItmsDTLID as DTLID,O.numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/SerialNo',2)                                                                         
   WITH(numWarehouseItmsDTLID numeric(9),numoppitemtCode numeric(9),numWItmsID numeric(9))                                
   join OpportunityItems O on O.numWarehouseItmsID=numWItmsID and O.numOppID=@numOppID)X                 
   where numOppID=@numOppID and numWarehouseItmsDTLID=X.DTLID      
    
--   insert into OpportunityKitItems(numOppKitItem,numChildItem,intQuantity,numWareHouseItemId)    
--   SELECT * FROM OPENXML (@hDocItem,'/NewDataSet/ChildItems',2)                                                                          
--   WITH                        
--   (                                               
--   numoppitemtCode numeric(9),                      
--   numItemCode numeric(9),    
--   numQtyItemsReq integer,  
--   numWarehouseItmsID numeric(9)                                                       
--   )    
      
    
    
    
	EXEC USP_ManageSOWorkOrder @numDomainId,@numOppID,@strItems,@numUserCntID
    
                              
                          
   EXEC sp_xml_removedocument @hDocItem                                     
        end          
   set @TotalAmount=0                                                           
   select @TotalAmount=sum(monTotAmount)from OpportunityItems where numOppId=@numOppID                                                                          
  update OpportunityMaster set  monPamount=@TotalAmount                                                          
  where numOppId=@numOppID                                                 
                      
     
--Insert Tax for Division   

IF @tintOppType=1 OR (@tintOppType=2 and (select isnull(bitPurchaseTaxCredit,0) from Domain where numDomainId=@numDomainId)=1)
BEGIN                   
	INSERT dbo.OpportunityMasterTaxItems (
		numOppId,
		numTaxItemID,
		fltPercentage
	) SELECT @numOppID,TI.numTaxItemID,dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,TI.numTaxItemID,@numOppId,0,NULL) FROM TaxItems TI JOIN DivisionTaxTypes DTT ON TI.numTaxItemID = DTT.numTaxItemID
	 WHERE DTT.numDivisionID=@numDivisionId AND DTT.bitApplicable=1
	   union 
	  select @numOppID,0,dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,0,@numOppId,1,NULL) 
	  FROM dbo.DivisionMaster WHERE bitNoTax=0 AND numDivisionID=@numDivisionID
END
-- Archieve item based on Item's individual setting
--	UPDATE Item SET IsArchieve = CASE WHEN EXISTS(SELECT * FROM dbo.OpportunityItems WHERE OpportunityItems.numItemCode = I.numItemCode) AND ISNULL(I.bitArchiveItem,0) = 1 
--									  THEN 1 
--									  ELSE 0 
--									  END 
--	FROM Item I JOIN dbo.OpportunityItems OI ON I.numItemCode = OI.numItemCode
	
 END
 
 else                                                                                                                          
 BEGIN                  
	--Declaration
	 DECLARE @tempAssignedTo AS NUMERIC(9)                                                    
	 SET @tempAssignedTo = NULL 
	 
	 SELECT @tintOppStatus = tintOppStatus,@tempAssignedTo=isnull(numAssignedTo,0) FROM   OpportunityMaster WHERE  numOppID = @numOppID
 
	--Reverting back the warehouse items                  
	 IF @tintOppStatus = 1 
		BEGIN        
		PRINT 'inside revert'          
			EXEC USP_RevertDetailsOpp @numOppID,0,@numUserCntID                  
		END                  
	-- Update Master table
	
	   IF LEN(ISNULL(@vcOppRefOrderNo,''))>0
	   BEGIN
	   		UPDATE dbo.OpportunityBizDocs SET vcRefOrderNo=@vcOppRefOrderNo WHERE numOppId=@numOppID
	   END
		
	   IF EXISTS (SELECT 1 FROM dbo.OpportunityMaster WHERE numOppId = @numOppID AND tintOppType=1 AND isnull(tintOppStatus,0)=1 AND ISNULL(numStatus,0)!= @numStatus)
	   BEGIN
	   		EXEC USP_ManageOpportunityAutomationQueue
					@numOppQueueID = 0, -- numeric(18, 0)
					@numDomainID = @numDomainID, -- numeric(18, 0)
					@numOppId = @numOppID, -- numeric(18, 0)
					@numOppBizDocsId = 0, -- numeric(18, 0)
					@numOrderStatus = @numStatus, -- numeric(18, 0)
					@numUserCntID = @numUserCntID, -- numeric(18, 0)
					@tintProcessStatus = 1, -- tinyint
					@tintMode = 1 -- TINYINT
	   END 
						
	   UPDATE   OpportunityMaster
	   SET      vcPOppName = @vcPOppName,
				txtComments = @Comments,
				bitPublicFlag = @bitPublicFlag,
				numCampainID = @CampaignID,
				tintSource = @tintSource,
				tintSourceType=@tintSourceType,
				intPEstimatedCloseDate = @dtEstimatedCloseDate,
				numModifiedBy = @numUserCntID,
				bintModifiedDate = GETUTCDATE(),
				lngPConclAnalysis = @lngPConclAnalysis,
				monPAmount = @monPAmount,
				tintActive = @tintActive,
				numSalesOrPurType = @numSalesOrPurType,
				numStatus = @numStatus,
				tintOppStatus = @DealStatus,
				vcOppRefOrderNo=@vcOppRefOrderNo,
				--vcWebApiOrderNo = @vcWebApiOrderNo,
				bintOppToOrder=(Case when @tintOppStatus=0 and @DealStatus=1 then GETUTCDATE() else bintOppToOrder end),
				bitDiscountType=@bitDiscountType,fltDiscount=@fltDiscount,bitBillingTerms=@bitBillingTerms,
				intBillingDays=@intBillingDays,bitInterestType=@bitInterestType,fltInterest=@fltInterest,
				tintTaxOperator=@tintTaxOperator,numDiscountAcntType=@numDiscountAcntType,vcCouponCode = @vcCouponCode,
				numPercentageComplete =@numPercentageComplete,bitUseShippersAccountNo = @bitUseShippersAccountNo,
				bitUseMarkupShippingRate = @bitUseMarkupShippingRate,
				numMarkupShippingRate = @numMarkupShippingRate,
				intUsedShippingCompany = @intUsedShippingCompany
	   WHERE    numOppId = @numOppID   
	   
	---Updating if organization is assigned to someone                                                      
	   IF ( @tempAssignedTo <> @numAssignedTo AND @numAssignedTo <> '0' ) 
		BEGIN                                   
			UPDATE  OpportunityMaster SET     numAssignedTo = @numAssignedTo, numAssignedBy = @numUserCntID WHERE   numOppId = @numOppID                                                    
		END                                                     
	   ELSE 
	   IF ( @numAssignedTo = 0 ) 
		BEGIN                
			UPDATE  OpportunityMaster SET     numAssignedTo = 0, numAssignedBy = 0 WHERE   numOppId = @numOppID
		END                                                                      
		---- Updating Opp Items
		if convert(varchar(10),@strItems) <>'' AND @numOppID>0                                                                         
		begin
			   EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems                                      
			   --Delete Items
			   delete from OpportunityKitItems where numOppID=@numOppID and numOppItemID not in                       
			   (SELECT numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/Item',2)                                                                          
			   WITH  (numoppitemtCode numeric(9)))                      
                                        
--               ---- ADDED BY Manish Anjara : Jun 26,2013 - Archive item based on Item's individual setting
--               UPDATE Item SET IsArchieve = CASE WHEN EXISTS(SELECT * FROM dbo.OpportunityItems WHERE OpportunityItems.numItemCode = I.numItemCode) AND ISNULL(I.bitArchiveItem,0) = 1 
--												 THEN 1 
--												 ELSE 0 
--												 END 
--			   FROM Item I JOIN dbo.OpportunityItems OI ON I.numItemCode = OI.numItemCode
--					 	
--			   UPDATE Item SET IsArchieve = CASE WHEN ISNULL(I.bitArchiveItem,0) = 1 THEN 0 ELSE 1 END
--			   FROM Item I
--			   JOIN OpportunityItems OI ON I.numItemCode = OI.numItemCode
--			   WHERE numOppID = @numOppID and OI.numItemCode NOT IN (SELECT numItemCode FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) 
--																	 WITH (numItemCode numeric(9)))

			   delete from OpportunityItems where numOppID=@numOppID and numoppitemtCode not in                       
			   (SELECT numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/Item',2)                                                                          
			   WITH  (numoppitemtCode numeric(9)))   
	                                
			   insert into OpportunityItems                                                                          
			   (numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,[vcItemName],[vcModelID],[vcManufacturer],vcPathForTImage,monVendorCost,numUOMId,bitWorkOrder,numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,vcAttributes,numClassID,monAvgCost,bitItemPriceApprovalRequired)
			   select @numOppID,X.numItemCode,dbo.fn_UOMConversion(X.numUOM,numItemCode,@numDomainId,null) * x.numUnitHour,x.monPrice,x.monTotAmount,X.vcItemDesc,X.numWarehouseItmsID,X.ItemType,X.DropShip,X.bitDiscountType,X.fltDiscount,X.monTotAmtBefDiscount,X.vcItemName,(SELECT vcModelID FROM item WHERE numItemCode = X.numItemCode),(SELECT vcManufacturer FROM item WHERE numItemCode = X.numItemCode),(SELECT TOP 1 vcPathForTImage FROM dbo.ItemImages WHERE numItemCode = X.numItemCode AND BitDefault = 1 AND numDomainId = @numDomainId),(select isnull(VN.monCost,0) from Vendor VN, Item IT where IT.numItemCode=VN.numItemCode and  VN.numItemCode=X.numItemCode and  VN.numVendorID=IT.numVendorID),X.numUOM,X.bitWorkOrder,
			   X.numVendorWareHouse,X.numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,Attributes,Case When @tintDefaultClassType=0 then @numDefaultClassID else (SELECT numItemClass FROM item WHERE numItemCode = X.numItemCode) END,
			   (SELECT ISNULL(monAverageCost,0) FROM Item I WHERE I.numDomainId=@numDomainId AND I.numItemCode=X.numItemCode), X.bitItemPriceApprovalRequired from(
			   SELECT *FROM OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
			   WITH  (                      
				numoppitemtCode numeric(9),                                     
				numItemCode numeric(9),                                                                          
				numUnitHour numeric(9,2),                                                                          
				monPrice money,                                                                       
				monTotAmount money,                                                                          
				vcItemDesc varchar(1000),                                    
				numWarehouseItmsID numeric(9),                              
				Op_Flag tinyint,                            
				ItemType varchar(30),      
				DropShip bit,
				bitDiscountType bit,
				fltDiscount float,
				monTotAmtBefDiscount MONEY,
				vcItemName VARCHAR(300),numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId numeric(9),numProjectID numeric(9),numProjectStageID numeric(9),Attributes varchar(200), bitItemPriceApprovalRequired BIT
				 ))X                                     
				--Update items                 
			   Update OpportunityItems                       
			   set numItemCode=X.numItemCode,    
			   numOppId=@numOppID,                       
			   numUnitHour=dbo.fn_UOMConversion(X.numUOM,X.numItemCode,@numDomainId,null) * x.numUnitHour,                      
			   monPrice=x.monPrice,                      
			   monTotAmount=x.monTotAmount,                                  
			   vcItemDesc=X.vcItemDesc,                      
			   numWarehouseItmsID=X.numWarehouseItmsID,                 
			   bitDropShip=X.DropShip,
			   bitDiscountType=X.bitDiscountType,
			   fltDiscount=X.fltDiscount,
			   monTotAmtBefDiscount=X.monTotAmtBefDiscount,vcItemName=X.vcItemName,numUOMId=numUOM,
			   numVendorWareHouse=X.numVendorWareHouse,numShipmentMethod=X.numShipmentMethod,numSOVendorId=X.numSOVendorId,numProjectId=X.numProjectID,numProjectStageId=X.numProjectStageID,vcAttributes=X.Attributes,bitItemPriceApprovalRequired=X.bitItemPriceApprovalRequired
			   from (SELECT numoppitemtCode as numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,DropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,vcItemName,numUOM,numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,Attributes,bitItemPriceApprovalRequired  FROM OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=2]',2)                                                                
			   WITH  (                      
				numoppitemtCode numeric(9),                                     
				numItemCode numeric(9),                                                                          
				numUnitHour numeric(9,2),                                                                          
				monPrice money,                                         
				monTotAmount money,                                                                          
				vcItemDesc varchar(1000),                                    
				numWarehouseItmsID numeric(9),      
				DropShip bit,
				bitDiscountType bit,
				fltDiscount float,
				monTotAmtBefDiscount MONEY,vcItemName varchar(300),numUOM NUMERIC(9),numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId numeric(9),numProjectID numeric(9),numProjectStageID numeric(9),Attributes varchar(200),bitItemPriceApprovalRequired BIT                                              
			   ))X where numoppitemtCode=X.numOppItemID                           
			                                    
			   EXEC USP_ManageSOWorkOrder @numDomainId,@numOppID,@strItems,@numUserCntID
			   		                                                                      
			   EXEC sp_xml_removedocument @hDocItem                                               
		end     
	    --Delete previously added serial lot number added for opportunity
		DELETE FROM OppWarehouseSerializedItem WHERE numOppID = @numOppID                                                         
END

----------------generic section will be called in both insert and update ---------------------------------
-- Archieve item based on Item's individual setting
--	UPDATE Item SET IsArchieve = CASE WHEN EXISTS(SELECT * FROM dbo.OpportunityItems WHERE OpportunityItems.numItemCode = I.numItemCode) AND ISNULL(I.bitArchiveItem,0) = 1 
--									  THEN 1 
--									  ELSE 0 
--									  END 
--	FROM Item I JOIN dbo.OpportunityItems OI ON I.numItemCode = OI.numItemCode
	
if convert(varchar(10),@strItems) <>'' AND @numOppID>0                                                                        
  begin 
EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems

--delete from OpportunityKitItems where numOppID=@numOppID and numOppItemID 
--	not in (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppID=@numOppID)                      
		
--Update Kit Items                 
Update OKI set numQtyItemsReq=numQtyItemsReq_Orig * OI.numUnitHour
			   FROM OpportunityKitItems OKI JOIN OpportunityItems OI ON OKI.numOppItemID=OI.numoppitemtCode and OKI.numOppId=OI.numOppId  
			   WHERE OI.numOppId=@numOppId 

--Insert Kit Items                 
INSERT into OpportunityKitItems                                                                          
		(numOppId,numOppItemID,numChildItemID,numWareHouseItemId,numQtyItemsReq,numQtyItemsReq_Orig,numUOMId,numQtyShipped)
  SELECT @numOppId,OI.numoppitemtCode,ID.numChildItemID,ID.numWareHouseItemId,ID.numQtyItemsReq * OI.numUnitHour,ID.numQtyItemsReq,ID.numUOMId,0
	 FROM OpportunityItems OI JOIN ItemDetails ID ON OI.numItemCode=ID.numItemKitID WHERE 
	OI.numOppId=@numOppId AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityKitItems OKI WHERE OKI.numOppId=@numOppID)
--	OI.numoppitemtCode NOT IN (SELECT numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=2]',2)                                                                
--			   WITH  (                      
--				numoppitemtCode numeric(9))X)
			   
EXEC sp_xml_removedocument @hDocItem  
END

/*Added by chintan - While creating SO from PO it wasn't puting Item Qty to Allocation. this segment was being called from USP_OppSaveMilestone which No longer in use after changes anoop did. */     
--Updating the warehouse items              
declare @tintShipped as tinyint               
select @tintOppStatus=tintOppStatus,@tintOppType=tintOppType,@tintShipped=tintShipped from OpportunityMaster where numOppID=@numOppID              
if @tintOppStatus=1               
	begin         
	PRINT 'inside USP_UpdatingInventoryonCloseDeal'
		exec USP_UpdatingInventoryonCloseDeal @numOppID,@numUserCntID
	end              
            
if @tintOppType=1              
begin              
	if @tintOppStatus=1 Update WareHouseItmsDTL set tintStatus=1 where numWareHouseItmsDTLID in (select numWarehouseItmsDTLID from OppWarehouseSerializedItem where numOppID=@numOppID)
	if @tintShipped=1 Update WareHouseItmsDTL set tintStatus=2 where numWareHouseItmsDTLID in (select numWarehouseItmsDTLID from OppWarehouseSerializedItem where numOppID=@numOppID)              
END


declare @tintCRMType as numeric(9)        
declare @AccountClosingDate as datetime        

select @numDivisionID=numDivisionID,@AccountClosingDate=bintAccountClosingDate from  OpportunityMaster where numOppID=@numOppID         
select @tintCRMType=tintCRMType from divisionmaster where numDivisionID =@numDivisionID 

--When deal is won                                         
if @DealStatus = 1 
begin           
	 if @AccountClosingDate is null                   
	  update OpportunityMaster set bintAccountClosingDate=getutcdate() where numOppID=@numOppID              
end         
--When Deal is Lost
else if @DealStatus = 2
begin         
	 select @AccountClosingDate=bintAccountClosingDate from  OpportunityMaster where numOppID=@numOppID         
	 if @AccountClosingDate is null                   
	  update OpportunityMaster set bintAccountClosingDate=getutcdate() where numOppID=@numOppID
end                    

/*Added by chintan -  Biz usually automatically promotes the record from Lead/Prospect to Account When a Sales Opp/Order is made against it*/  
-- Promote Lead to Prospect when Sales/Purchase Opp is created against it
if @tintCRMType=0 AND @DealStatus = 0 --Lead & Open Opp
begin        
	update divisionmaster set tintCRMType=1,bintLeadProm=getutcdate(),bintLeadPromBy=@numUserCntID,bintProsProm=getutcdate(),bintProsPromBy=@numUserCntID        
	where numDivisionID=@numDivisionID        
end        
-- Promote Lead to Account when Sales/Purchase Order is created against it
ELSE if @tintCRMType=0 AND @DealStatus = 1 --Lead & Order
begin        
	update divisionmaster set tintCRMType=2,bintLeadProm=getutcdate(),bintLeadPromBy=@numUserCntID,bintProsProm=getutcdate(),bintProsPromBy=@numUserCntID        
	where numDivisionID=@numDivisionID        
end
--Promote Prospect to Account
else if @tintCRMType=1 AND @DealStatus = 1 
begin        
	update divisionmaster set tintCRMType=2,bintProsProm=getutcdate(),bintProsPromBy=@numUserCntID        
	where numDivisionID=@numDivisionID        
end

--Add/Update Address Details
DECLARE @vcStreet  varchar(100),@vcCity  varchar(50),@vcPostalCode  varchar(15),@vcCompanyName  varchar(100),@vcAddressName VARCHAR(50)      
DECLARE @numState  numeric(9),@numCountry  numeric(9), @numCompanyId  numeric(9) 
DECLARE @bitIsPrimary BIT;

select  @vcCompanyName=vcCompanyName,@numCompanyId=div.numCompanyID from CompanyInfo Com                            
join divisionMaster Div                            
on div.numCompanyID=com.numCompanyID                            
where div.numdivisionID=@numDivisionId

--Bill Address
IF @numBillAddressId>0
BEGIN
 SELECT  @vcStreet=ISNULL(vcStreet,''),@vcCity=ISNULL(vcCity,''),@vcPostalCode=ISNULL(vcPostalCode,''),
		@numState=ISNULL(numState,0),@numCountry=ISNULL(numCountry,0),@bitIsPrimary=bitIsPrimary,
		@vcAddressName=vcAddressName
            FROM  dbo.AddressDetails WHERE   numDomainID = @numDomainID AND numAddressID = @numBillAddressId
  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 0, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = @vcCompanyName, --  varchar(100)
	@numCompanyId = 0, --  numeric(9, 0)
	@vcAddressName = @vcAddressName
END
  
  
  --Ship Address
IF @numShipAddressId>0
BEGIN
	SELECT  @vcStreet=ISNULL(vcStreet,''),@vcCity=ISNULL(vcCity,''),@vcPostalCode=ISNULL(vcPostalCode,''),
		@numState=ISNULL(numState,0),@numCountry=ISNULL(numCountry,0),@bitIsPrimary=bitIsPrimary,
		@vcAddressName=vcAddressName
            FROM  dbo.AddressDetails WHERE   numDomainID = @numDomainID AND numAddressID = @numShipAddressId
 



  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 1, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = @vcCompanyName, --  varchar(100)
	@numCompanyId =@numCompanyId, --  numeric(9, 0)
	@vcAddressName = @vcAddressName
END

--Delete Tax for Opportunity Items if item deleted 
DELETE FROM OpportunityItemsTaxItems WHERE numOppId=@numOppId AND numOppItemID NOT IN (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID)

--Insert Tax for Opportunity Items
INSERT INTO dbo.OpportunityItemsTaxItems (
	numOppId,
	numOppItemID,
	numTaxItemID
) SELECT @numOppId,OI.numoppitemtCode,TI.numTaxItemID FROM dbo.OpportunityItems OI JOIN dbo.ItemTax IT ON OI.numItemCode=IT.numItemCode JOIN
TaxItems TI ON TI.numTaxItemID=IT.numTaxItemID WHERE OI.numOppId=@numOppID AND IT.bitApplicable=1 AND 
OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
UNION
  select @numOppId,OI.numoppitemtCode,0 FROM dbo.OpportunityItems OI JOIN dbo.Item I ON OI.numItemCode=I.numItemCode
   WHERE OI.numOppId=@numOppID  AND I.bitTaxable=1 AND
OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)

  
 UPDATE OpportunityMaster SET monDealAmount=dbo.GetDealAmount(@numOppID,getdate(),0) WHERE numOppId=@numOppID
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OppWarehouseSerializedItem_Insert')
DROP PROCEDURE USP_OppWarehouseSerializedItem_Insert
GO
CREATE PROCEDURE [dbo].[USP_OppWarehouseSerializedItem_Insert]
	@numOppID AS NUMERIC(18,0),
	@numOppItemID AS NUMERIC(18,0),
	@numWarehouseItemID AS NUMERIC(18,0),
	@vcSelectedItems AS VARCHAR(MAX)
AS
BEGIN
	SET NOCOUNT ON;
	
	DELETE FROM OppWarehouseSerializedItem WHERE numOppID=@numOppID AND numOppItemID=@numOppItemID AND numWarehouseItmsID=@numWarehouseItemID

	INSERT INTO OppWarehouseSerializedItem
	(
		numWarehouseItmsDTLID,
		numOppID,
		numOppItemID,
		numWarehouseItmsID,
		numQty
	)
	SELECT 
		ParseName(items,2),
		@numOppID,
		@numOppItemID,
		@numWarehouseItemID,
		ParseName(items,1) 
	FROM 
		dbo.Split(Replace(@vcSelectedItems, '-', '.'),',')

END







SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_SitePages_GetByTemplateID')
DROP PROCEDURE dbo.USP_SitePages_GetByTemplateID
GO
CREATE PROCEDURE [dbo].[USP_SitePages_GetByTemplateID]
	@numDomainID NUMERIC (18,0),
	@numTemplateID  NUMERIC (18,0)
AS 
BEGIN

	DECLARE @vcTemplateCode VARCHAR(500)
	SELECT @vcTemplateCode='{template:' + REPLACE(LOWER([vcTemplateName]),' ','_') + '}' FROM SiteTemplates WHERE numTemplateID = @numTemplateID AND numDomainID = @numDomainID


	SELECT 
		* 
	FROM 
		SitePages 
	WHERE 
		numTemplateID IN (
							SELECT 
								numTemplateID 
							FROM 
								SiteTemplates 
							WHERE 
								numDomainID = 187 AND 
								CONTAINS(txtTemplateHTML,@vcTemplateCode)
							)
	
END



/****** Object:  StoredProcedure [dbo].[usp_TicklerOpportuity]    Script Date: 07/26/2008 16:21:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                              
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_tickleropportuity')
DROP PROCEDURE usp_tickleropportuity
GO
CREATE PROCEDURE [dbo].[usp_TicklerOpportuity]                                      
@numUserCntID as numeric(9)=null,                                      
@numDomainID as numeric(9)=null,                                      
@startDate as datetime,                                      
@endDate as datetime                                     
as                                       
  
DECLARE @vcSelectedEmployess VARCHAR(500)
DECLARE @vcAssignedStages VARCHAR(500)
DECLARE @vcActionTypes VARCHAR(100)
DECLARE @strSql VARCHAR(8000) = ''

SELECT 
	@vcSelectedEmployess = vcSelectedEmployeeOpp,
	@vcAssignedStages = vcAssignedStages
FROM 
	TicklerListFilterConfiguration
WHERE
	numDomainID = @numDomainID AND
	numUserCntID = @numUserCntID


SELECT   
		 Split.a.value('.', 'VARCHAR(100)') AS Data  
	INTO 
		#TEMP
	FROM  
	(
		SELECT CAST ('<M>' + REPLACE(ISNULL(@vcAssignedStages,''),',','</M><M>') + '</M>' AS XML) AS Data  
	) AS A CROSS APPLY Data.nodes ('/M') AS Split(a); 

IF ISNULL(LEN(@vcAssignedStages),0) = 0 OR (SELECT COUNT(*) FROM #TEMP WHERE Data = 1) > 0 --Sales Opportunity
BEGIN
	SET @strSql = 'select 
						SPD.numStageDetailsId,
						SPD.vcStageName,
						SPD.tinProgressPercentage as Tprogress,
						SPD.dtEndDate as CloseDate,
						opp.numOppId as ID,vcPOppName,
						''Sales Opportunity'' AS Type,
						Div.numTerId                                      
					FROM 
						OpportunityMaster opp 
					JOIN 
						StagePercentageDetails SPD 
					ON 
						opp.numOppID=SPD.numOppID 
					JOIN 
						DivisionMaster div 
					ON 
						div.numDivisionID=opp.numDivisionID  
					WHERE 
						opp.tintOppType= 1 AND 
						opp.tintOppStatus = 0 AND
						opp.numDomainID=' + CAST(@numDomainID AS VARCHAR(10))
						 + ' AND (SPD.numAssignTo IN (' + CASE WHEN LEN(@vcSelectedEmployess) > 0 THEN @vcSelectedEmployess ELSE Cast(@numUserCntID as varchar(10)) END +')  or SPD.numCreatedBy IN (' + CASE WHEN LEN(@vcSelectedEmployess) > 0 THEN @vcSelectedEmployess ELSE Cast(@numUserCntID as varchar(10)) END +' ))'
						 + ' AND (SPD.dtEndDate  >= '''+ Cast(@startDate as varchar(30)) +''' AND SPD.dtEndDate  <= ''' + CAST(@endDate AS VARCHAR(30)) + ''')'
END

IF ISNULL(LEN(@vcAssignedStages),0) = 0 OR (SELECT COUNT(*) FROM #TEMP WHERE Data = 2) > 0 --Purchase Opportunity
BEGIN
	SET @strSql =  @strSql + CASE WHEN LEN(@strSql) > 0 THEN ' UNION ' ELSE '' END + 
					'select 
						SPD.numStageDetailsId,
						SPD.vcStageName,
						SPD.tinProgressPercentage as Tprogress,
						SPD.dtEndDate as CloseDate,
						opp.numOppId as ID,vcPOppName,
						''Purchase Opportunity'' AS Type,
						Div.numTerId                                      
					FROM 
						OpportunityMaster opp 
					JOIN 
						StagePercentageDetails SPD 
					ON 
						opp.numOppID=SPD.numOppID 
					JOIN 
						DivisionMaster div 
					ON 
						div.numDivisionID=opp.numDivisionID  
					WHERE 
						opp.tintOppType= 2 AND 
						opp.tintOppStatus = 0 AND
						opp.numDomainID=' + CAST(@numDomainID AS VARCHAR(10))
						 + ' AND (SPD.numAssignTo IN (' + CASE WHEN LEN(@vcSelectedEmployess) > 0 THEN @vcSelectedEmployess ELSE Cast(@numUserCntID as varchar(10)) END +')  or SPD.numCreatedBy IN (' + CASE WHEN LEN(@vcSelectedEmployess) > 0 THEN @vcSelectedEmployess ELSE Cast(@numUserCntID as varchar(10)) END +' ))'
						 + ' AND (SPD.dtEndDate  >= '''+ Cast(@startDate as varchar(30)) +''' AND SPD.dtEndDate  <= ''' + CAST(@endDate AS VARCHAR(30)) + ''')'
END
	
IF ISNULL(LEN(@vcAssignedStages),0) = 0 OR (SELECT COUNT(*) FROM #TEMP WHERE Data = 3) > 0 --Sales Order
BEGIN
	SET @strSql = @strSql + CASE WHEN LEN(@strSql) > 0 THEN ' UNION ' ELSE '' END +
					'select 
						SPD.numStageDetailsId,
						SPD.vcStageName,
						SPD.tinProgressPercentage as Tprogress,
						SPD.dtEndDate as CloseDate,
						opp.numOppId as ID,vcPOppName,
						''Sales Order'' AS Type,
						Div.numTerId                                      
					FROM 
						OpportunityMaster opp 
					JOIN 
						StagePercentageDetails SPD 
					ON 
						opp.numOppID=SPD.numOppID 
					JOIN 
						DivisionMaster div 
					ON 
						div.numDivisionID=opp.numDivisionID  
					WHERE 
						opp.tintOppType= 1 AND 
						opp.tintOppStatus = 1 AND
						opp.numDomainID=' + CAST(@numDomainID AS VARCHAR(10))
						 + ' AND (SPD.numAssignTo IN (' + CASE WHEN LEN(@vcSelectedEmployess) > 0 THEN @vcSelectedEmployess ELSE Cast(@numUserCntID as varchar(10)) END +')  or SPD.numCreatedBy IN (' + CASE WHEN LEN(@vcSelectedEmployess) > 0 THEN @vcSelectedEmployess ELSE Cast(@numUserCntID as varchar(10)) END +' ))'
						 + ' AND (SPD.dtEndDate  >= '''+ Cast(@startDate as varchar(30)) +''' AND SPD.dtEndDate  <= ''' + CAST(@endDate AS VARCHAR(30)) + ''')'
END

IF ISNULL(LEN(@vcAssignedStages),0) = 0 OR (SELECT COUNT(*) FROM #TEMP WHERE Data = 4) > 0 --Purchase Order
BEGIN
	SET @strSql = @strSql + CASE WHEN LEN(@strSql) > 0 THEN ' UNION ' ELSE '' END +
					'select 
						SPD.numStageDetailsId,
						SPD.vcStageName,
						SPD.tinProgressPercentage as Tprogress,
						SPD.dtEndDate as CloseDate,
						opp.numOppId as ID,vcPOppName,
						''Purchase Order'' AS Type,
						Div.numTerId                                      
					FROM 
						OpportunityMaster opp 
					JOIN 
						StagePercentageDetails SPD 
					ON 
						opp.numOppID=SPD.numOppID 
					JOIN 
						DivisionMaster div 
					ON 
						div.numDivisionID=opp.numDivisionID  
					WHERE 
						opp.tintOppType= 2 AND 
						opp.tintOppStatus = 1 AND
						opp.numDomainID=' + CAST(@numDomainID AS VARCHAR(10))
						 + ' AND (SPD.numAssignTo IN (' + CASE WHEN LEN(@vcSelectedEmployess) > 0 THEN @vcSelectedEmployess ELSE Cast(@numUserCntID as varchar(10)) END +')  or SPD.numCreatedBy IN (' + CASE WHEN LEN(@vcSelectedEmployess) > 0 THEN @vcSelectedEmployess ELSE Cast(@numUserCntID as varchar(10)) END +' ))'
						 + ' AND (SPD.dtEndDate  >= '''+ Cast(@startDate as varchar(30)) +''' AND SPD.dtEndDate  <= ''' + CAST(@endDate AS VARCHAR(30)) + ''')'
END

IF ISNULL(LEN(@vcAssignedStages),0) = 0 OR (SELECT COUNT(*) FROM #TEMP WHERE Data = 5) > 0 --Project
BEGIN
	SET @strSql = @strSql + CASE WHEN LEN(@strSql) > 0 THEN ' UNION ' ELSE '' END +                    
							'SELECT 
								SPD.numStageDetailsId,
								SPD.vcStageName,
								SPD.tinProgressPercentage as Tprogress,
								SPD.dtEndDate as CloseDate,
								Pro.numProId as ID,
								vcProjectName as vcPOppName,
								''Project'' as Type,Div.numTerId  
							FROM 
								ProjectsMaster Pro 
							JOIN 
								StagePercentageDetails SPD 
							ON 
								Pro.numProId=SPD.numProjectID
							JOIN 
								DivisionMaster div 
							ON 
								div.numDivisionID=Pro.numDivisionID   
							WHERE 
								Pro.numDomainID=' + CAST(@numDomainID AS VARCHAR(10))
							+ ' AND (SPD.numAssignTo IN (' + CASE WHEN LEN(@vcSelectedEmployess) > 0 THEN @vcSelectedEmployess ELSE Cast(@numUserCntID as varchar(10)) END +')  or SPD.numCreatedBy IN (' + CASE WHEN LEN(@vcSelectedEmployess) > 0 THEN @vcSelectedEmployess ELSE Cast(@numUserCntID as varchar(10)) END +' ))'
							+ ' AND (SPD.dtEndDate  >= '''+ Cast(@startDate as varchar(30)) +''' and SPD.dtEndDate  <= ''' + CAST(@endDate AS VARCHAR(30)) + ''') ORDER BY dtEndDate Desc'                   
END


Print @strSql
EXEC(@strSql)   

DROP TABLE #TEMP                           
GO
--Created by Anoop Jayaraj

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_UpdatePOItemsForFulfillment')
DROP PROCEDURE USP_UpdatePOItemsForFulfillment
GO
CREATE PROCEDURE  USP_UpdatePOItemsForFulfillment
    @numQtyReceived NUMERIC(9),
    @numOppItemID NUMERIC(9),
    @vcError VARCHAR(200) = '' OUTPUT,
    @numUserCntID AS NUMERIC(9),
    @dtItemReceivedDate AS DATETIME 
AS 

--Transaction is invoked from ADO.net (Purchase Fulfillment) Do not add transaction code here.
-- BEGIN TRY
-- BEGIN TRANSACTION
			DECLARE @numDomain AS NUMERIC(18,0)
			DECLARE @onAllocation AS NUMERIC          
			DECLARE @numWarehouseItemID AS NUMERIC       
			DECLARE @numOldQtyReceived AS NUMERIC       
			DECLARE @numNewQtyReceived AS NUMERIC
			DECLARE @onHand AS NUMERIC         
			DECLARE @onOrder AS NUMERIC            
			DECLARE @onBackOrder AS NUMERIC              
			DECLARE @bitStockTransfer BIT
			DECLARE @numToWarehouseItemID NUMERIC
			DECLARE @numOppId NUMERIC
			DECLARE @numUnits NUMERIC
			DECLARE @monPrice AS MONEY 
			DECLARE @numItemCode NUMERIC 
		
			SELECT  @numWarehouseItemID = ISNULL([numWarehouseItmsID], 0),
					@numOldQtyReceived = ISNULL([numUnitHourReceived], 0),
					@bitStockTransfer = ISNULL(OM.bitStockTransfer,0),
					@numToWarehouseItemID= ISNULL(numToWarehouseItemID,0),
					@numOppId=OM.numOppId,
					@numUnits=OI.numUnitHour,
					@monPrice=isnull(monPrice,0) * (CASE WHEN ISNULL(OM.fltExchangeRate,0)=0 THEN 1 ELSE OM.fltExchangeRate END),
					@numItemCode=OI.numItemCode,
					@numDomain = OM.[numDomainId]
			FROM    [OpportunityItems] OI INNER JOIN dbo.OpportunityMaster OM
					ON OI.numOppId = OM.numOppId
			WHERE   [numoppitemtCode] = @numOppItemID
    
    
    
			IF @bitStockTransfer = 1 --added by chintan
			BEGIN
			--ship item from FROM warehouse
				declare @p3 varchar(500)
				set @p3=''
				exec USP_UpdateQtyShipped @numQtyReceived,@numOppItemID,@numUserCntID,@vcError=@p3 OUTPUT
				IF LEN(@p3)>0 
				BEGIN
					RAISERROR ( @p3,16, 1 )
					RETURN ;

				END
		
			-- Receive item from To Warehouse
			SET @numWarehouseItemID=@numToWarehouseItemID
    
		   END  
    
			SET @numNewQtyReceived = @numQtyReceived + @numOldQtyReceived ;
			DECLARE @description AS VARCHAR(100)
			SET @description='PO Qty Received (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Received:' +  CAST(@numNewQtyReceived AS VARCHAR(10)) + ')'
			
		--    PRINT @numNewQtyReceived
			IF @numNewQtyReceived <= 0 
				RETURN 
  
					SELECT  @onHand = ISNULL(numOnHand, 0),
					@onAllocation = ISNULL(numAllocation, 0),
					@onOrder = ISNULL(numOnOrder, 0),
					@onBackOrder = ISNULL(numBackOrder, 0)
			FROM    WareHouseItems
			WHERE   numWareHouseItemID = @numWareHouseItemID
			
			           DECLARE @TotalOnHand AS NUMERIC;SET @TotalOnHand=0  
			           SELECT @TotalOnHand=SUM(ISNULL(numOnHand, 0)) FROM dbo.WareHouseItems WHERE numItemID=@numItemCode 
						
			           --Updating the Average Cost
			           DECLARE @monAvgCost AS MONEY 
			           SELECT  @monAvgCost = ISNULL(monAverageCost, 0) FROM    Item WHERE   numitemcode = @numItemCode  
    
  						SET @monAvgCost = ( ( @TotalOnHand * @monAvgCost )
                                             + (@numNewQtyReceived * @monPrice))
                            / ( @TotalOnHand + @numNewQtyReceived )
                            
                        UPDATE  item
                        SET     monAverageCost = @monAvgCost
                        WHERE   numItemCode = @numItemCode
    
			
  
		--	SELECT @onHand onHand,
		--            @onAllocation onAllocation,
		--            @onBackOrder onBackOrder,
		--            @onOrder onOrder
      
			IF @onOrder >= @numNewQtyReceived 
				BEGIN    
				PRINT '1 case'        
					SET @onOrder = @onOrder - @numNewQtyReceived             
					IF @onBackOrder >= @numNewQtyReceived 
						BEGIN            
							SET @onBackOrder = @onBackOrder - @numNewQtyReceived             
							SET @onAllocation = @onAllocation + @numNewQtyReceived             
						END            
					ELSE 
						BEGIN            
							SET @onAllocation = @onAllocation + @onBackOrder            
							SET @numNewQtyReceived = @numNewQtyReceived - @onBackOrder            
							SET @onBackOrder = 0            
							SET @onHand = @onHand + @numNewQtyReceived             
						END         
				END            
			ELSE IF @onOrder < @numNewQtyReceived 
				BEGIN            
				PRINT '2 case'        
					SET @onHand = @onHand + @onOrder
					SET @onOrder = @numNewQtyReceived - @onOrder
				END   

				SELECT @onHand onHand,
					@onAllocation onAllocation,
					@onBackOrder onBackOrder,
					@onOrder onOrder
                
			UPDATE  [OpportunityItems]
			SET     numUnitHourReceived = @numNewQtyReceived
			WHERE   [numoppitemtCode] = @numOppItemID

			UPDATE  WareHouseItems
			SET     numOnHand = @onHand,
					numAllocation = @onAllocation,
					numBackOrder = @onBackOrder,
					numOnOrder = @onOrder,dtModified = GETDATE() 
			WHERE   numWareHouseItemID = @numWareHouseItemID
    
  
	    UPDATE dbo.OpportunityMaster SET dtItemReceivedDate=@dtItemReceivedDate WHERE numOppId=@numOppId
		
		EXEC dbo.USP_ManageWareHouseItems_Tracking
			@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
			@numReferenceID = @numOppId, --  numeric(9, 0)
			@tintRefType = 4, --  tinyint
			@vcDescription = @description, --  varchar(100)
			@numModifiedBy = @numUserCntID,
			@dtRecordDate =  @dtItemReceivedDate,
			@numDomainID = @numDomain
          
    
		--    BEGIN
		--        DECLARE @vcOppName VARCHAR(200)
		--        SELECT TOP 1
		--                @vcOppName = ISNULL([vcPOppName], '')
		--        FROM    [OpportunityMaster]
		--        WHERE   [numOppId] IN ( SELECT  [numOppId]
		--                                FROM    [OpportunityItems]
		--                                WHERE   [numoppitemtCode] = @numOppItemID )
		--        SET @vcError = 'There is not enough Qty on Allocation to save quantity Received for ' + @vcOppName
		--        RETURN
		--    END
		--     
		
--COMMIT
--END TRY
--BEGIN CATCH
--   --Whoops, there was an error
--  IF @@TRANCOUNT > 0
--     ROLLBACK

--  -- Raise an error with the details of the exception
--  DECLARE @ErrMsg nvarchar(4000), @ErrSeverity int
--  SELECT @ErrMsg = ERROR_MESSAGE(),
--         @ErrSeverity = ERROR_SEVERITY()

--  RAISERROR(@ErrMsg, @ErrSeverity, 1)
--END CATCH
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_UpdateSingleFieldValue' ) 
    DROP PROCEDURE USP_UpdateSingleFieldValue
GO

CREATE PROCEDURE USP_UpdateSingleFieldValue
	@tintMode AS TINYINT,
	@numUpdateRecordID NUMERIC,
	@numUpdateValueID NUMERIC=0,
	@vcText AS TEXT,
	@numDomainID AS NUMERIC= 0 
AS 
BEGIN
	 -- For 26 and 37
     DECLARE @Status AS NUMERIC(9,0)
     DECLARE @SiteID       AS NUMERIC(9,0)
     DECLARE @OrderStatus  AS NUMERIC(9,0)
	 DECLARE @BizdocStatus AS NUMERIC(9,0)  
	 DECLARE @numDomain AS NUMERIC(18,0)
	 SET @numDomain = @numDomainID 

	IF @tintMode=1
	BEGIN
		UPDATE  dbo.OpportunityBizDocsDetails
		SET     bitAmountCaptured = @numUpdateValueID
		WHERE   numBizDocsPaymentDetId = @numUpdateRecordID
	END
--	IF @tintMode = 2 
--		BEGIN
--			UPDATE  dbo.OpportunityMaster
--			SET     numStatus = @numUpdateValueID
--			WHERE   numOppId = @numUpdateRecordID
--		END
	IF @tintMode = 3
		BEGIN
			 UPDATE AdditionalContactsInformation SET txtNotes=ISNULL(CAST(txtNotes AS VARCHAR(8000)),'') + ' ' +  ISNULL(CAST(@vcText AS VARCHAR(8000)),'') WHERE numContactId=@numUpdateRecordID
		END
	IF @tintMode = 4
		BEGIN
			 UPDATE dbo.OpportunityBizDocsDetails SET numBillPaymentJournalID=@numUpdateValueID WHERE numBizDocsPaymentDetId=@numUpdateRecordID
		END
		
	IF @tintMode = 5
		BEGIN
			 UPDATE dbo.BizDocComission SET bitCommisionPaid=@numUpdateValueID WHERE numOppBizDocID=@numUpdateRecordID
		END
		
	IF @tintMode = 6
		BEGIN
			 UPDATE dbo.OpportunityRecurring SET numOppBizDocID=@numUpdateValueID WHERE numOppRecID=@numUpdateRecordID
		END
		
	IF @tintMode = 7
		BEGIN
			 UPDATE dbo.OpportunityRecurring SET dtRecurringDate=CAST( CAST( @vcText AS VARCHAR) AS DATETIME) WHERE numOppRecID=@numUpdateRecordID
		END
		
	IF @tintMode = 8
		BEGIN
			 UPDATE dbo.OpportunityRecurring SET fltBreakupPercentage=CAST( CAST( @vcText AS VARCHAR) AS float) WHERE numOppRecID=@numUpdateRecordID
		END
		/*Commented by chintan.. this is moved to USP_ShoppingCart SP*/
--	IF @tintMode = 9
--		BEGIN
--			UPDATE  dbo.OpportunityMaster
--			SET     txtComments = CAST(@vcText AS VARCHAR(1000))
--			WHERE   numOppId = @numUpdateRecordID
--		END    
--	IF @tintMode = 10
--		BEGIN
--			UPDATE  dbo.OpportunityMaster
--			SET     vcRefOrderNo = CAST(@vcText AS VARCHAR(100))
--			WHERE   numOppId = @numUpdateRecordID
--		END    
	IF @tintMode = 11
		BEGIN
			DELETE FROM dbo.LuceneItemsIndex WHERE numItemCode=@numUpdateRecordID
		END    
	IF @tintMode = 12
		BEGIN
			UPDATE dbo.Domain SET vcPortalName = CAST(@vcText AS VARCHAR(50))
			WHERE numDomainId=@numUpdateRecordID
		END 
	IF @tintMode = 13
		BEGIN
			UPDATE dbo.WareHouseItems SET numOnHand =numOnHand + CAST(CAST(@vcText AS VARCHAR(50)) AS numeric(18,0)),dtModified = GETDATE() 
			WHERE numWareHouseItemID = @numUpdateRecordID
			
			DECLARE @numItemCode NUMERIC(18)

			SELECT @numItemCode=numItemID from WareHouseItems where numWareHouseItemID = @numUpdateRecordID

			EXEC dbo.USP_ManageWareHouseItems_Tracking
				@numWareHouseItemID = @numUpdateRecordID, --  numeric(9, 0)
				@numReferenceID = @numItemCode, --  numeric(9, 0)
				@tintRefType = 1, --  tinyint
				@vcDescription = 'Inventory Adjustment', --  varchar(100)
				@numModifiedBy = @numUpdateValueID,
				@numDomainID = @numDomain
			 
		END
	IF @tintMode = 14
		BEGIN
			UPDATE dbo.Item SET monAverageCost=CAST(CAST(@vcText AS VARCHAR(50)) AS numeric(18,4))
			WHERE numItemCode = @numUpdateRecordID
		END
		IF @tintMode = 15
		BEGIN
			UPDATE dbo.EmailHistory  SET numListItemId = @numUpdateValueID  WHERE numEmailHstrID=@numUpdateRecordID
		END
		IF @tintMode = 16
		BEGIN
			UPDATE dbo.EmailHistory SET bitIsRead=@numUpdateValueID WHERE numEmailHstrID IN ( SELECT Id FROM dbo.SplitIDs(CAST(@vcText AS VARCHAR(200)) ,','))
		END
		IF @tintMode = 17
		BEGIN
			UPDATE CartItems SET numUserCntId = @numUpdateValueID WHERE vcCookieId =CAST(@vcText AS VARCHAR(MAX)) AND numUserCntId = 0
		END
		IF @tintMode = 18
		BEGIN
			UPDATE OpportunityBizDocs SET tintDeferred = 0 WHERE numOppBizDocsId=@numUpdateRecordID
		END
		IF @tintMode = 19
		BEGIN
			UPDATE dbo.OpportunityMaster SET vcPOppName=CAST(@vcText AS VARCHAR(100)) WHERE numOppId=@numUpdateRecordID
		END
	    IF @tintMode = 20
		BEGIN
			UPDATE UserMaster SET tintTabEvent=@numUpdateValueID WHERE numUserId=@numUpdateRecordID
		END
		IF @tintMode = 21
		BEGIN
			UPDATE OpportunityBizDocs SET numBizDocTempID = @numUpdateValueID WHERE numOppBizDocsId=@numUpdateRecordID
		END
		IF @tintMode = 22
		BEGIN
			UPDATE OpportunityBizDocItems SET tintTrackingStatus = 1 WHERE numOppBizDocItemID =@numUpdateRecordID
		END
		IF	 @tintMode = 23
		BEGIN
			UPDATE item SET bintModifiedDate = GETUTCDATE() WHERE numItemCode=@numUpdateRecordID			
		END
		IF @tintMode = 24
		BEGIN
			UPDATE dbo.OpportunityMaster SET numStatus=@numUpdateValueID WHERE numOppId in (select Items from dbo.split(@vcText,','))
		END
		
		IF @tintMode = 25
		BEGIN
			UPDATE dbo.WebAPIDetail SET vcEighthFldValue = @vcText WHERE WebApiId = @numUpdateRecordID AND numDomainId = @numDomainID
		END
		IF @tintMode = 26
		BEGIN
			
		     SELECT  @Status = ISNULL(numStatus ,0)  FROM dbo.OpportunityMaster WHERE numOppId = @numUpdateRecordID
		     
		     IF @Status = 0
		      BEGIN
				SELECT @SiteID  = CONVERT(NUMERIC(9,0),tintSource)   FROM	dbo.OpportunityMaster WHERE numOppId = @numUpdateRecordID
			    SELECT @OrderStatus = ISNULL(numOrderStatus,0) ,@BizdocStatus = ISNULL(numBizDocStatus,0)  FROM dbo.eCommercePaymentConfig WHERE numSiteId = @SiteID AND numPaymentMethodId = 31488
		       
			    UPDATE dbo.OpportunityMaster SET numStatus = ISNULL(@OrderStatus,0) WHERE numOppId = @numUpdateRecordID AND numDomainId = @numDomainID 
			    Update OpportunityBizDocs set monAmountPaid = ISNULL(monDealAmount,0),numBizDocStatus = @BizdocStatus  WHERE  numOppId = @numUpdateRecordID and bitAuthoritativeBizDocs =1
			  END   
		END
		IF @tintMode = 27
		BEGIN
			UPDATE OpportunityBizDocs SET numBizDocStatusOLD=numBizDocStatus,numBizDocStatus = @numUpdateValueID WHERE numOppBizDocsId=@numUpdateRecordID 
		END
	    IF @tintMode = 28
		BEGIN
			UPDATE General_Journal_Details SET bitReconcile=(CASE WHEN CAST(@vcText AS VARCHAR(5))='R' THEN 1 ELSE 0 END),
			bitCleared=(CASE WHEN CAST(@vcText AS VARCHAR(5))='C' THEN 1 ELSE 0 END),numReconcileID=0
			WHERE numDomainId=@numDomainID AND numTransactionId=@numUpdateRecordID 
		END
		
		IF @tintMode = 29
		BEGIN
			UPDATE dbo.ReturnHeader SET numRMATempID = @numUpdateValueID WHERE numReturnHeaderID = @numUpdateRecordID 
		END
		
		IF @tintMode = 30
		BEGIN
			UPDATE dbo.ReturnHeader SET numBizdocTempID = @numUpdateValueID WHERE numReturnHeaderID = @numUpdateRecordID 
		END
		
		IF @tintMode = 31
		BEGIN
			UPDATE dbo.OpportunityMaster SET numOppBizDocTempID = @numUpdateValueID WHERE numOppId = @numUpdateRecordID 
		END
		/*Commented by chintan.. this is moved to USP_ShoppingCart SP*/
--		IF @tintMode = 32
--		BEGIN
--			DECLARE @numBizDocId AS NUMERIC(9,0)
--			SELECT @numBizDocId = numBizDocId FROM eCommercePaymentConfig WHERE  numPaymentMethodId = @numUpdateValueId AND numDomainID = @numDomainID   
--			UPDATE dbo.OpportunityMaster SET numOppBizDocTempID = @numBizDocId WHERE numOppId = @numUpdateRecordID 
--		END
		
		IF @tintMode = 33
		BEGIN
			UPDATE dbo.ReturnHeader SET numBizdocTempID = @numUpdateValueID,numRMATempID = @numUpdateValueID WHERE numReturnHeaderID = @numUpdateRecordID 
		END
		
			
		IF @tintMode = 34
		BEGIN
			UPDATE dbo.WebAPIDetail SET vcFourteenthFldValue = @numUpdateValueID WHERE WebApiId = @numUpdateRecordID AND numDomainId = @numDomainID
		END
		
		IF @tintMode = 35
		BEGIN
			UPDATE OpportunityMasterTaxItems SET fltPercentage = CAST(CAST(@vcText AS VARCHAR(10)) AS FLOAT)
			WHERE numOppId = @numUpdateRecordID AND numTaxItemID = 0
		END
		
		IF @tintMode = 36
		BEGIN
			UPDATE dbo.Import_File_Master SET numProcessedCSVRowNumber = @numUpdateValueID
			WHERE intImportFileID = @numUpdateRecordID AND numDomainID = @numDomainID
		END
		IF @tintMode = 37
		BEGIN
		     SELECT  @Status = ISNULL(numStatus ,0)  FROM dbo.OpportunityMaster WHERE numOppId = @numUpdateRecordID
		     
		     IF @Status = 0
		      BEGIN
			    SELECT @SiteID  = CONVERT(NUMERIC(9,0),tintSource)   FROM	dbo.OpportunityMaster WHERE numOppId = @numUpdateRecordID
			    SELECT @OrderStatus = ISNULL(numFailedOrderStatus,0)    FROM dbo.eCommercePaymentConfig WHERE numSiteId = @SiteID AND numPaymentMethodId = 31488
		        
			    UPDATE dbo.OpportunityMaster SET numStatus = ISNULL(@OrderStatus,0) WHERE numOppId = @numUpdateRecordID AND numDomainId = @numDomainID 
			    
			  END   
		END
		IF @tintMode = 38
		BEGIN
			UPDATE [WebAPIDetail] SET    vcNinthFldValue = CAST(CAST(@vcText AS VARCHAR(30)) AS DATETIME)
			WHERE  [WebApiId] =  @numUpdateRecordID AND [numDomainId] = @numDomainId
		END
		
		IF @tintMode = 39
		BEGIN
			UPDATE [WebAPIDetail] SET    vcSixteenthFldValue = CAST(CAST(@vcText AS VARCHAR(30)) AS DATETIME)
			WHERE  [WebApiId] =  @numUpdateRecordID AND [numDomainId] = @numDomainId
		END

		IF @tintMode = 40
		BEGIN
			UPDATE dbo.WebAPIDetail SET bitEnableAPI = @numUpdateValueID WHERE WebApiId = @numUpdateRecordID AND numDomainId = @numDomainID
		END

		IF @tintMode = 41
		BEGIN
			UPDATE dbo.domain SET numShippingServiceItemID = @numUpdateValueID WHERE numDomainId = @numDomainID
		END
		
		IF @tintMode = 42
		BEGIN
			UPDATE dbo.domain SET numDiscountServiceItemID = @numUpdateValueID WHERE numDomainId = @numDomainID
		END
		
		IF @tintMode = 43
		BEGIN
			UPDATE dbo.Cases SET numStatus = @numUpdateValueID WHERE numCaseId = @numUpdateRecordID AND numDomainId = @numDomainID
		END
		
		IF @tintMode = 44
		BEGIN
			UPDATE dbo.UserMaster SET numDefaultClass = @numUpdateValueID WHERE numUserId = @numUpdateRecordID AND numDomainId = @numDomainID
		END

		IF @tintMode = 45
		BEGIN
		UPDATE [dbo].[EmailHistory] SET [numUid] = @numUpdateValueID WHERE [EmailHistory].[numEmailHstrID] = @numUpdateRecordID  AND numDomainId = @numDomainID
			--UPDATE dbo.UserMaster SET numDefaultClass = @numUpdateValueID WHERE numUserId = @numUpdateRecordID AND numDomainId = @numDomainID
		END
		
    SELECT @@ROWCOUNT
END
--Exec  USP_UpdateSingleFieldValue 26,52205,0,'',156
--UPDATE dbo.OpportunityMaster SET numStatus = 0 where numOppId = 52185
--SELECT numStatus FROM dbo.OpportunityMaster where numOppId = 52203
--SELECT * FROM dbo.OpportunityMaster 
--SELECT * FROM dbo.OpportunityBizDocs where numOppId = 52203
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_ValidateOppSerializLot')
DROP PROCEDURE usp_ValidateOppSerializLot
GO
CREATE PROCEDURE [dbo].[usp_ValidateOppSerializLot]
    (
      @numOppID NUMERIC(9) = 0
    )
AS 
    BEGIN
   
--    SELECT numoppitemtCode,opp.vcItemName,CASE WHEN SUM(oppI.numQty)=SUM(opp.numUnitHour) THEN 0 ELSE 1 END Error
--FROM OpportunityItems opp JOIN Item i ON i.numItemCode=opp.numItemCode left JOIN OppWarehouseSerializedItem oppI ON (opp.numoppitemtCode=oppI.numOppItemID AND oppI.numOppID=Opp.numOppId)
-- LEFT JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID=whi.numWareHouseItmsDTLID
--where opp.numOppId=@numOppID AND (ISNULL(i.bitSerialized,0)=1 OR ISNULL(i.bitLotNo,0)=1) GROUP BY numoppitemtCode,opp.vcItemName
DECLARE @numOppType TINYINT
DECLARE @numOppStatus TINYINT
DECLARE @bitStockTransfer BIT

SELECT @numOppType=tintOppType, @numOppStatus=tintOppStatus, @bitStockTransfer=bitStockTransfer FROM OpportunityMaster WHERE numOppID =  @numOppID

--Validate serial number in case of sales order and sotck transfer only
If (@numOppType = 1 AND @numOppStatus=1) OR @bitStockTransfer = 1
BEGIN
    SELECT numoppitemtCode,opp.vcItemName,CASE WHEN SUM(opp.numUnitHour)=
    (SELECT SUM(oppI.numQty) FROM OppWarehouseSerializedItem oppI JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID=whi.numWareHouseItmsDTLID WHERE opp.numoppitemtCode=oppI.numOppItemID AND oppI.numOppID=@numOppID) THEN 0 ELSE 1 END Error
FROM OpportunityItems opp JOIN Item i ON i.numItemCode=opp.numItemCode
where opp.numOppId=@numOppID AND (ISNULL(i.bitSerialized,0)=1 OR ISNULL(i.bitLotNo,0)=1) GROUP BY numoppitemtCode,opp.vcItemName
END
ELSE
BEGIN
	 SELECT 
		numoppitemtCode,
		opp.vcItemName,
		0 AS Error
	FROM 
		OpportunityItems opp 
	JOIN 
		Item i 
	ON 
		i.numItemCode=opp.numItemCode
	WHERE 
		opp.numOppId=@numOppID AND 
		(ISNULL(i.bitSerialized,0)=1 OR ISNULL(i.bitLotNo,0)=1) 
	GROUP BY 
		numoppitemtCode,opp.vcItemName
END

END
/*

declare @p5 int
set @p5=0
exec USP_WorkFlowMaster_History @numDomainID=183,@numFormID=70,@CurrentPage=1,@PageSize=20,@TotRecs=@p5 output,@SortChar='0',@columnName='dtCreatedDate',@columnSortOrder='Desc',@SearchStr=''
select @p5

*/
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_WorkFlowMaster_History')
DROP PROCEDURE USP_WorkFlowMaster_History
GO
  
 
-- =============================================      
-- Author:  <Author,,Sachin Sadhu>      
-- Create date: <Create Date,,3rdApril2014>      
-- Description: <Description,,To maintain Work rule execution history>      
-- =============================================      
Create PROCEDURE [dbo].[USP_WorkFlowMaster_History]       
 -- Add the parameters for the stored procedure here      
@numDomainID numeric(18,0),      
@numFormID INT  ,    
@CurrentPage int,                                                              
@PageSize int,                                                              
@TotRecs int output,           
@SortChar char(1)='0' ,                                                             
@columnName as Varchar(50),                                                              
@columnSortOrder as Varchar(50)  ,      
@SearchStr  as Varchar(50)         
AS      
BEGIN      
 -- SET NOCOUNT ON added to prevent extra result sets from      
 -- interfering with SELECT statements.      
 SET NOCOUNT ON;      
      
    -- Insert statements for procedure here      
    Create table #tempTable ( ID INT IDENTITY PRIMARY KEY,                                                               
        numWFID NUMERIC(18,0) ,    
        numFormID NUMERIC(18,0),    
        vcWFName VARCHAR(1000),      
        vcPOppName VARCHAR(500),      
        vcWFDescription VARCHAR(max),      
        numRecordID NUMERIC(18,0),      
        tintProcessStatus int,      
        bitSuccess int,      
        vcDescription VARCHAR(max),      
        vcFormName VARCHAR(100),      
        dtExecutionDate DATETIME,    
        numOppId NUMERIC(18,0),      
        STATUS VARCHAR(50),    
        TriggeredOn VARCHAR(1000) ,  
        vcUserName VARCHAR(1000)   ,
		dtCreatedDate     DATETIME                                                   
 )           
declare @strSql as varchar(MAX)                                                        
          
set  @strSql='  SELECT       
      
   m.numWFID,      
   m.numFormID,    
   m.vcWFName,      
   ISNULL(o.vcPOppName,'''') as vcPOppName,      
   m.vcWFDescription,      
   q.numRecordID,      
   q.tintProcessStatus,      
   e.bitSuccess,      
  ISNULL( e.vcDescription,''Waiting'') as  vcDescription,      
   DFM.vcFormName,      
   e.dtExecutionDate,    
   o.numOppId,     
   CASE WHEN q.tintProcessStatus=1 THEN ''Pending Execution'' WHEN q.tintProcessStatus= 2 THEN ''In Progress''  WHEN q.tintProcessStatus=3 THEN ''Success'' WHEN q.tintProcessStatus=4 then ''Failed'' WHEN q.tintProcessStatus=6 then ''Condition Not Matched'' else ''No WF Found'' END AS STATUS,      
   CASE WHEN (m.tintWFTriggerOn=1 AND m.intDays=0)THEN ''Create'' WHEN m.tintWFTriggerOn=2 THEN ''Edit'' WHEN m.tintWFTriggerOn=3 THEN ''Create or Edit'' WHEN m.tintWFTriggerOn=4 THEN ''Fields Update'' WHEN m.tintWFTriggerOn=5 THEN ''Delete'' WHEN m.intDays>0 THEN     
''Date Field'' ELSE ''NA'' end AS TriggeredOn,  
 dbo.fn_GetContactName(ISNULL(m.numCreatedBy,0)) AS vcUserName   ,
 q.dtCreatedDate   
  FROM dbo.WorkFlowMaster as m       
  INNER JOIN dbo.WorkFlowQueue AS q ON m.numWFID=q.numWFID       
  LEFT JOIN dbo.WorkFlowQueueExecution AS e ON e.numWFQueueID=q.numWFQueueID      
  LEFT JOIN dbo.OpportunityMaster o ON q.numRecordID =o.numOppId      
  JOIN DynamicFormMaster DFM ON m.numFormID=DFM.numFormID AND DFM.tintFlag=3    WHERE m.numDomainID='+ convert(varchar(15),@numDomainID)       
          
if @SortChar<>'0' set @strSql=@strSql + ' And m.vcWFName like '''+@SortChar+'%'''           
      
if @numFormID <> 0 set @strSql=@strSql + ' And m.numFormID = '+ convert(varchar(15),@numFormID) +''         
      
if @SearchStr<>'' set @strSql=@strSql + ' And (m.vcWFName like ''%'+@SearchStr+'%'' or       
m.vcWFDescription like ''%'+@SearchStr+'%'') '       
          
set  @strSql=@strSql +'ORDER BY ' + @columnName +' '+ @columnSortOrder          
     
insert into #tempTable( numWFID ,    
            numFormID ,    
            vcWFName ,    
            vcPOppName ,    
            vcWFDescription ,    
            numRecordID ,    
            tintProcessStatus ,    
            bitSuccess ,    
            vcDescription ,    
            vcFormName ,    
            dtExecutionDate ,    
            numOppId ,    
            STATUS ,    
            TriggeredOn,vcUserName,dtCreatedDate) exec(@strSql)          
          
 declare @firstRec as integer                                                              
 declare @lastRec as integer                                                   
 set @firstRec= (@CurrentPage-1) * @PageSize                                                              
 set @lastRec= (@CurrentPage*@PageSize+1)                                                               
      
 SELECT  @TotRecs = COUNT(*)  FROM #tempTable       
       
  SELECT       
   ID AS RowNo,*    
   From #tempTable T        
   WHERE ID > @firstRec and ID < @lastRec  order by ID      
       
       
--   SELECT       
--   ROW_NUMBER() OVER(ORDER BY m.numWFID ASC) AS RowNo,    
--   m.numWFID,      
--   m.vcWFName,      
--   o.vcPOppName,      
--   m.vcWFDescription,      
--   q.numRecordID,      
--   q.tintProcessStatus,      
--   e.bitSuccess,      
--   e.vcDescription ,      
--   DFM.vcFormName,      
--   e.dtExecutionDate,    
--   o.numOppId,      
--   CASE WHEN q.tintProcessStatus=1 THEN 'Pending Execution' WHEN q.tintProcessStatus= 2 THEN 'In Progress' WHEN q.tintProcessStatus=3 THEN 'Success' WHEN q.tintProcessStatus=4 then 'Failed' else 'No WF Found' END AS STATUS,      
--   CASE WHEN (m.tintWFTriggerOn=1 AND m.intDays=0)THEN 'Create' WHEN m.tintWFTriggerOn=2 THEN 'Edit' WHEN m.tintWFTriggerOn=3 THEN 'Create or Edit' WHEN m.tintWFTriggerOn=4 THEN 'Fields Update' WHEN m.tintWFTriggerOn=5 THEN 'Delete' WHEN m.intDays>0 TH
  
--E--N     
--'Date Field' ELSE 'NA' end AS TriggeredOn      
--  FROM dbo.WorkFlowMaster as m       
--  INNER JOIN dbo.WorkFlowQueue AS q ON m.numWFID=q.numWFID       
--  LEFT JOIN dbo.WorkFlowQueueExecution AS e ON e.numWFQueueID=q.numWFQueueID      
--  INNER JOIN dbo.OpportunityMaster o ON q.numRecordID =o.numOppId      
--  JOIN DynamicFormMaster DFM ON m.numFormID=DFM.numFormID AND DFM.tintFlag=3  join #tempTable T on T.numWFID=m.numWFID      
--    
--   WHERE ID > @firstRec and ID < @lastRec AND m.numDomainID=@numDomainID order by ID      
         
   DROP TABLE #tempTable      
        
        
        
        
        
        
        
        
        
        
     
    
      
      
END 
