/******************************************************************
Project: Release 13.2 Date: 24.MAR.2020
Comments: ALTER SCRIPTS
*******************************************************************/


/******************************************** SANDEEP *********************************************/


ALTER TABLE OpportunityItems ADD numWOQty FLOAT

-------------------------------------------------------------

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField
)
VALUES
(
	3,296,144,0,0,'Status','Label','Status',1,0,0,1,0,1
)


--------------------------------------------------------------

ALTER TABLE WorkOrder ADD numBuildPlan NUMERIC(18,0) DEFAULT 201

ALTER TABLE WorkOrder ADD numQtyBuilt FLOAT DEFAULT 0

--------------------------------------------------------------

UPDATE WorkOrder SET numBuildPlan = 201

--------------------------------------------------------------

DECLARE @numFieldID NUMERIC(18,0)

INSERT INTO DycFieldMaster
(
	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering
)
VALUES
(
	3,'Build Plan','numBuildPlan','numBuildPlan','BuildPlan','WorkOrder','N','R','SelectBox','LI',53,1,0,1,0,1,1,1,1,1
)

SET @numFieldID = SCOPE_IDENTITY()

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField
)
VALUES
(
	3,@numFieldID,144,1,1,'Build Plan','SelectBox',1,0,0,1,1,1
)

--------------------------------------------------------------

UPDATE DycFieldMaster SET vcPropertyName='Quantity' WHERE numFieldId=299

--------------------------------------------------------------

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField
)
VALUES
(
	3,299,144,1,1,'Quantity','TextBox',1,0,1,1,1,1
)

--------------------------------------------------------------

USE [Production.2014]
GO

/****** Object:  Table [dbo].[WorkOrderPickedItems]    Script Date: 24-Mar-20 1:36:12 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[WorkOrderPickedItems](
	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numWODetailId] [numeric](18, 0) NOT NULL,
	[numPickedQty] [float] NOT NULL,
	[numFromWarehouseItemID] [numeric](18, 0) NOT NULL,
	[numToWarehouseItemID] [numeric](18, 0) NOT NULL,
	[dtCreatedDate] [datetime] NOT NULL,
	[numCreatedBy] [numeric](18, 0) NOT NULL,
 CONSTRAINT [PK_WorkOrderPickedItems] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[WorkOrderPickedItems]  WITH CHECK ADD  CONSTRAINT [FK_WorkOrderDetails_WorkOrderPickedItems] FOREIGN KEY([numWODetailId])
REFERENCES [dbo].[WorkOrderDetails] ([numWODetailId])
GO

ALTER TABLE [dbo].[WorkOrderPickedItems] CHECK CONSTRAINT [FK_WorkOrderDetails_WorkOrderPickedItems]
GO

--------------------------------------------------------------

-- DO NOT EXECUTE ON LIVE ALREADY EXECUTED
SET IDENTITY_INSERT ListMaster ON

INSERT INTO ListMaster
(
	numListID,vcListName,numCreatedBy,bintCreatedDate,numModifiedBy,bintModifiedDate,bitDeleted,bitFixed,numDomainID,bitFlag,numModuleID
)
VALUES
(
	53,'Build Plan',1,GETUTCDATE(),1,GETUTCDATE(),0,1,1,0,16
)

SET IDENTITY_INSERT ListMaster OFF

------------------------------------------------------------
-- DO NOT EXECUTE ON LIVE ALREADY EXECUTED
SET IDENTITY_INSERT ListDetails ON

INSERT INTO ListDetails
(
	numListItemID,numListID,vcData,numCreatedBY,bitDelete,numDomainID,constFlag,sintOrder
)
VALUES
(
	201,53,'Based on whatever is picked ',1,0,1,1,1
),
(
	202,53,'Only after full qty has been picked',1,0,1,1,2
)

SET IDENTITY_INSERT ListDetails OFF



/******************************************** PRASHANT *********************************************/


DELETE FROM PageNavigationDTL WHERE numParentID IN(SELECT numPageNavID FROM PageNavigationDTL WHERE vcPageNavName='Activities')


UPDATE TabMaster SET vcURL='common/frmTicklerdisplay.aspx?SelectedIndex=0' WHERE numTabName='Activities'
UPDATE TabMaster SET vcAddURL='common/frmAddActivity.aspx',bitAddIsPopUp=1 WHERE numTabName='Activities'
ALTER TABLE OpportunityMaster ADD bitReqPOApproved BIT DEFAULT 0

ALTER TABLE Domain ADD bitDisplayCustomField BIT DEFAULT 0
ALTER TABLE Domain ADD bitFollowupAnytime BIT DEFAULT 0
ALTER TABLE Domain ADD bitpartycalendarTitle BIT DEFAULT 0
ALTER TABLE Domain ADD bitpartycalendarLocation BIT DEFAULT 0
ALTER TABLE Domain ADD bitpartycalendarDescription BIT DEFAULT 0

ALTER TABLE Domain ADD bitREQPOApproval BIT DEFAULT 0
ALTER TABLE Domain ADD bitARInvoiceDue BIT DEFAULT 0
ALTER TABLE Domain ADD bitAPBillsDue BIT DEFAULT 0
ALTER TABLE Domain ADD bitItemsToPickPackShip BIT DEFAULT 0
ALTER TABLE Domain ADD bitItemsToInvoice BIT DEFAULT 0
ALTER TABLE Domain ADD bitSalesOrderToClose BIT DEFAULT 0
ALTER TABLE Domain ADD bitItemsToPutAway BIT DEFAULT 0
ALTER TABLE Domain ADD bitItemsToBill BIT DEFAULT 0
ALTER TABLE Domain ADD bitPosToClose BIT DEFAULT 0
ALTER TABLE Domain ADD bitPOToClose BIT DEFAULT 0
ALTER TABLE Domain ADD bitBOMSToPick BIT DEFAULT 0

ALTER TABLE Domain ADD vchREQPOApprovalEmp VARCHAR(500) DEFAULT NULL
ALTER TABLE Domain ADD vchARInvoiceDue VARCHAR(500) DEFAULT NULL
ALTER TABLE Domain ADD vchAPBillsDue VARCHAR(500) DEFAULT NULL
ALTER TABLE Domain ADD vchItemsToPickPackShip VARCHAR(500) DEFAULT NULL
ALTER TABLE Domain ADD vchItemsToInvoice VARCHAR(500) DEFAULT NULL
ALTER TABLE Domain ADD vchPriceMarginApproval VARCHAR(500) DEFAULT NULL
ALTER TABLE Domain ADD vchSalesOrderToClose VARCHAR(500) DEFAULT NULL
ALTER TABLE Domain ADD vchItemsToPutAway VARCHAR(500) DEFAULT NULL
ALTER TABLE Domain ADD vchItemsToBill VARCHAR(500) DEFAULT NULL
ALTER TABLE Domain ADD vchPosToClose VARCHAR(500) DEFAULT NULL
ALTER TABLE Domain ADD vchPOToClose VARCHAR(500) DEFAULT NULL
ALTER TABLE Domain ADD vchBOMSToPick VARCHAR(500) DEFAULT NULL

ALTER TABLE Domain ADD decReqPOMinValue DECIMAL(18,2) DEFAULT 0

BEGIN TRY
BEGIN TRANSACTION

DECLARE @numFieldID AS NUMERIC(18,0)
DECLARE @numFormFieldID AS NUMERIC(18,0)

INSERT INTO DycFieldMaster
(numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,[order],tintRow,tintColumn,
bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numListID,PopupFunctionName)
VALUES
(7,'Solutions','SoultionName','SoultionName','SoultionName','Cases','V','R','Popup',46,1,1,1,0,0,0,1,0,1,1,1,0,'openSolutionPopup')

SELECT @numFieldID = SCOPE_IDENTITY()

INSERT INTO DynamicFormFieldMaster
(numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
 bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
VALUES
(12,'Solutions','R','Popup','SoultionName',0,0,'V','SoultionName',0,'Cases',0,7,1,'SoultionName',1,7,1,0,1,0)

SELECT @numFormFieldID = SCOPE_IDENTITY()

INSERT INTO DycFormField_Mapping
(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
VALUES
(7,@numFieldID,12,0,0,'Solutions','Popup','SoultionName',7,1,7,1,0,0,1,0,1,0,1,@numFormFieldID)

COMMIT
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		ERROR_MESSAGE(),
		ERROR_NUMBER(),
		ERROR_SEVERITY(),
		ERROR_STATE(),
		ERROR_LINE(),
		ISNULL(ERROR_PROCEDURE(), '-');
END CATCH


ALTER TABLE DycFormConfigurationDetails ADD bitDefaultByAdmin BIT
ALTER TABLE DycFormConfigurationDetails ADD numFormFieldGroupId NUMERIC(18,0)

ALTER TABLE CaseContacts ADD bitSubscribedEmailAlert BIT DEFAULT 1
ALTER TABLE ProjectsContacts ADD bitSubscribedEmailAlert BIT DEFAULT 1
ALTER TABLE OpportunityContact ADD bitSubscribedEmailAlert BIT DEFAULT 1

ALTER TABLE Cases ADD vcSolutionShortMessage VARCHAR(250) 