/******************************************************************
Project: Release 10.3 Date: 08.OCT.2018
Comments: ALTER SCRIPTS
*******************************************************************/

/******************************************** SANDEEP *********************************************/


ALTER TABLE OpportunityBizDocItems ADD monAverageCost DECIMAL(20,5) DEFAULT NULL


USE [Production.2014]
GO

/****** Object:  Table [dbo].[OpportunityBizDocKitItems]    Script Date: 02-Oct-18 4:43:53 PM ******/
DROP TABLE [dbo].[OpportunityBizDocKitItems]
GO

/****** Object:  Table [dbo].[OpportunityBizDocKitItems]    Script Date: 02-Oct-18 4:43:53 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[OpportunityBizDocKitItems](
	[numOppBizDocKitItemID] NUMERIC(18,0) NOT NULL IDENTITY(1,1) PRIMARY KEY,
	[numOppBizDocID] NUMERIC(18,0) NOT NULL,
	[numOppBizDocItemID] [numeric](18, 0) NOT NULL,
	[numOppChildItemID] [numeric](18, 0) NOT NULL,
	[numChildItemID] [numeric](18, 0) NOT NULL,
	[monAverageCost] DECIMAL(20,5) NULL
) ON [PRIMARY]

GO

------------------


/****** Object:  Table [dbo].[OpportunityBizDocKitItems]    Script Date: 02-Oct-18 4:43:53 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[OpportunityBizDocKitChildItems](
	[numOppBizDocKitChildItemID] NUMERIC(18,0) NOT NULL IDENTITY(1,1) PRIMARY KEY,
	[numOppBizDocID] NUMERIC(18,0) NOT NULL,
	[numOppBizDocItemID] [numeric](18, 0) NOT NULL,
	[numOppBizDocKitItemID] NUMERIC(18,0) NOT NULL,
	[numOppKitChildItemID] NUMERIC(18,0) NOT NULL,
	[numChildItemID] [numeric](18, 0) NOT NULL,
	[monAverageCost] DECIMAL(20,5) NULL
) ON [PRIMARY]

GO

ALTER TABLE ReturnItems ADD numOppBizDocItemID NUMERIC(18,0)


USP_OPPGetINItemsForAuthorizativeAccounting
USP_OpportunityBizDocKitItemsForAuthorizativeAccounting
USP_General_Journal_Details_CheckIfSalesClearingEntryExists
USP_ProjectOpp
USP_OpportunityBizDocs_GetItemsToReturn
USP_ManageReturnHeaderItems
USP_GetHeaderReturnDetails

usp_ManageRMAInventory - Pending
====================================================

ALTER TABLE Item ADD tintKitAssemblyPriceBasedOn TINYINT
ALTER TABLE Item Add fltReorderQty FLOAT

UPDATE Item SET tintKitAssemblyPriceBasedOn=1 WHERE ISNULL(bitCalAmtBasedonDepItems,0)=1

UPDATE ListDetails SET vcData='Build Completed' WHERE numListID=302 AND constFlag=1 AND vcData='Completed'


UPDATE WorkOrder SET numWOStatus=0 WHERE numWOStatus IN (40722,40723)
DELETE FROM ListDetails WHERE numListId=302 AND numListItemID IN (40722,40723)


EXEC sp_rename 'ItemDetails.numWareHouseItemId', 'numWareHouseItemId1', 'COLUMN'

BEGIN TRY
BEGIN TRANSACTION

	INSERT INTO PageMaster 
	(
		numPageID,numModuleID,vcFileName,vcPageDesc,bitIsViewApplicable,bitIsAddApplicable,bitIsUpdateApplicable,bitIsDeleteApplicable,bitIsExportApplicable
	)
	VALUES
	(
		35,10,'frmManageWorkOrder.aspx','Start Purchase Order(s) in Work Orders',1,0,0,0,0
	)
	SELECT 
		* 
	INTO 
		#temp 
	FROM
	(
		SELECT 
			ROW_NUMBER() OVER(ORDER BY numDomainId DESC) AS Row, 
			numDomainId 
		FROM 
			Domain 
		WHERE 
			numDomainId <> -255
	) TABLE2
	
	DECLARE @RowCount INT
	SET @RowCount = (SELECT COUNT(Row) FROM #temp) 
	
	DECLARE @I INT
	DECLARE @numDomainId NUMERIC(18,0)
	
	SET @I = 1

	WHILE (@I <= @RowCount)
	BEGIN
			
		SELECT @numDomainId = numDomainID FROM #temp WHERE Row = @I
	
		INSERT INTO dbo.GroupAuthorization
			(numDomainID,numModuleID,numPageID,numGroupID,intExportAllowed,intPrintAllowed,intViewAllowed,intAddAllowed,intUpdateAllowed,intDeleteAllowed)
		SELECT
			@numDomainId,10,35,numGroupID,0,0,3,0,0,0
		FROM
			AuthenticationGroupMaster
		WHERE
			numDomainID = @numDomainId AND
			tintGroupType = 1

		SET @i = @i + 1
	END
COMMIT
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		ERROR_MESSAGE(),
		ERROR_NUMBER(),
		ERROR_SEVERITY(),
		ERROR_STATE(),
		ERROR_LINE(),
		ISNULL(ERROR_PROCEDURE(), '-');
END CATCH


-- CHECK FIELD IDS ON LIVE
UPDATE DycFormField_Mapping SET bitDefault=1 WHERE numFormID=139 AND numFieldID IN (189,349,900,50904,50905,50911)



BEGIN TRY
BEGIN TRANSACTION

DECLARE @numFieldID NUMERIC(18,0)

INSERT INTO DycFieldMaster
(
	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering
)
VALUES
(
	4,'Adjusted Value ($)','monAdjustedValue','monAdjustedValue','AdjustedValue','WareHouseItems','N','R','Label',0,1,0,0,0,0,0,0,1,1
)

SET @numFieldID = SCOPE_IDENTITY()


INSERT INTO ReportFieldGroupMappingMaster
(
	numReportFieldGroupID,numFieldID,vcFieldName,vcAssociatedControlType,vcFieldDataType,bitAllowSorting,bitAllowGrouping,bitAllowAggregate,bitAllowFiltering
)
VALUES
(
	11,@numFieldID,'Adjusted Value ($)','TextBox','M',1,1,1,1
)

COMMIT
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		ERROR_MESSAGE(),
		ERROR_NUMBER(),
		ERROR_SEVERITY(),
		ERROR_STATE(),
		ERROR_LINE(),
		ISNULL(ERROR_PROCEDURE(), '-');
END CATCH

ALTER TABLE Category ADD bitVisible BIT DEFAULT 1
UPDATE Category SET bitVisible=1


DECLARE @numFieldID NUMERIC(18,0)
SELECT @numFieldID=numFieldID FROM DycFieldMaster WHERE vcDbColumnName='vcItemRequiredDate' AND vcOrigDbColumnName='vcItemRequiredDate'
UPDATE DycFieldMaster SET vcDbColumnName='ItemRequiredDate',vcOrigDbColumnName='ItemRequiredDate' WHERE numFieldId=@numFieldID

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering
)
VALUES
(
	3,@numFieldID,139,0,0,'Required Date','DateField',10,1,0,1,1,0,0,1,1
)


/******************************************** PRIYA *********************************************/

---------- Avaliable , On-Hand label change
---------------  
--select * from DycFieldMaster Where vcFieldName like '%on hand%'

UPDATE DycFieldMaster
SET vcFieldName = 'Available'
WHERE numFieldId = 195
------------------

--FormId = 7, 26, 27, 129
--select * from DycFormField_Mapping WHERE vcFieldName = 'on-hand' AND  numFieldId = 195

Update DycFormField_Mapping
SET vcFieldName = 'Available'
WHERE vcFieldName = 'on-hand' AND  numFieldId = 195
-----------------------------

--FormId = 15, 29, 33, 67, 125
--select * from DycFormField_Mapping WHERE vcFieldName = 'onhand' AND  numFieldId = 195

Update DycFormField_Mapping
SET vcFieldName = 'Available'
WHERE vcFieldName = 'onhand' AND  numFieldId = 195
------------------------------------

-- FormId = 21, 25, 48, 74, 135

--select * from DycFormField_Mapping WHERE vcFieldName = 'on hand' AND  numFieldId = 195

Update DycFormField_Mapping
SET vcFieldName = 'Available'
WHERE vcFieldName = 'on hand' AND  numFieldId = 195
-----------------------------------------


UPDATE DycFieldMaster
SET vcFieldName = 'On Hand'
WHERE numFieldId = 900 --OR numFieldId = 80833 

--------------------------------------------------
-- FormId = 139

--select * from DycFormField_Mapping WHERE vcFieldName = 'Available' AND  numFieldId = 900

UPDATE DycFormField_Mapping
SET vcFieldName = 'On Hand'
WHERE numFieldId = 900 
------------------------------------------------------
-- Formid = 7
 
select * from DycFormField_Mapping WHERE vcFieldName = 'Available' AND  numFieldId = 80833

UPDATE DycFormField_Mapping
SET vcFieldName = 'On Hand'
WHERE numFieldId = 80833 

--------------Next Tasks For Priya.PPT , 2nd Slide--------------

ALTER TABLE OpportunityMaster
ADD bitDropShipAddress BIT
---------------------Task List Item Category in Custom reports--------------

INSERT INTO ReportFieldGroupMappingMaster
(
	numReportFieldGroupID
	,numFieldID
	,vcFieldName
	,vcAssociatedControlType
	,vcFieldDataType
	,bitAllowSorting
	,bitAllowGrouping
	,bitAllowAggregate
	,bitAllowFiltering
)
	VALUES
(
	7
	,311
	,'Item Category'
	,'SelectBox'
	,'N'
	,1
	,1
	,0
	,1
)

-----------------------------------Task list SO comments editable -----------
UPDATE DycFormField_Mapping
SET bitInlineEdit = 1
WHERE numFieldID = 646 AND numFormID = 23

--------------------------------Exhibit Markup Changes----------------------------
ALTER TABLE Domain
ADD tintMarkupDiscountOption tinyint,
 tintMarkupDiscountValue tinyint
 
ALTER TABLE OpportunityItems
ADD bitMarkupDiscount bit NULL

ALTER table ReturnItems
ADD bitMarkupDiscount BIT NULL

------------------------------Exhibit Markup Changes----------------------------

----------------------------Date Field WorkFlow changes --------------------
USE [Production.2014]
GO

CREATE TABLE [dbo].[AdditionalContactsInformation_TempDateFields](
	[numContactId] [numeric](18, 0) NOT NULL,
	[numDomainID] [numeric](18, 0) NULL,
	[bintCreatedDate] [datetime] NULL,
	[bintDOB] [datetime] NULL
 CONSTRAINT [PK_AdditionalContactsInformation_TempDateFields] PRIMARY KEY CLUSTERED 
(
	[numContactId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]

GO

--------------------------------------------------------------------------------------------

GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Cases_TempDateFields](
	[numCaseId] [numeric](18, 0) NOT NULL,
	[numDomainID] [numeric](18, 0) NULL,
	[bintCreatedDate] [datetime] NULL,
	[intTargetResolveDate] [datetime] NULL
 CONSTRAINT [PK_Cases_TempDateFields] PRIMARY KEY CLUSTERED 
(
	[numCaseId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]

GO

---------------------------------------------------------------------------------------------------
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Communication_TempDateFields](
	[numCommId] [numeric](18, 0) NOT NULL,
	[numDomainID] [numeric](18, 0) NULL,
	[dtStartTime] [datetime] NULL
 CONSTRAINT [PK_Communication_TempDateFields] PRIMARY KEY CLUSTERED 
(
	[numCommId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]

GO

--------------------------------------------------------------------------------------------------------------

GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[DivisionMaster_TempDateFields](
	[numDivisionID] [numeric](18, 0) NOT NULL,
	[numDomainID] [numeric](18, 0) NULL,
	[bintCreatedDate] [datetime] NULL
 CONSTRAINT [PK_DivisionMaster_TempDateFields] PRIMARY KEY CLUSTERED 
(
	[numDivisionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]

GO

--------------------------------------------------------------------

GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[OpportunityMaster_TempDateFields](
	[numOppId] [numeric](18, 0) NOT NULL,
	[numDomainID] [numeric](18, 0) NULL,
	[bintCreatedDate] [datetime] NULL,
	[bintClosedDate] [datetime] NULL,
	[intPEstimatedCloseDate] [datetime] NULL,
	[dtReleaseDate] [datetime] NULL
 CONSTRAINT [PK_OpportunityMaster_TempDateFields] PRIMARY KEY CLUSTERED 
(
	[numOppId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]

GO

-----------------------------------------------------------

GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ProjectsMaster_TempDateFields](
	[numProId] [numeric](18, 0) NOT NULL,
	[numDomainID] [numeric](18, 0) NULL,
	[bintCreatedDate] [datetime] NULL,
	[bintProClosingDate] [datetime] NULL,
	[intDueDate] [datetime] NULL
 CONSTRAINT [PK_ProjectsMaster_TempDateFields] PRIMARY KEY CLUSTERED 
(
	[numProId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]

GO

-----------------------------------------------------------

GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[StagePercentageDetails_TempDateFields](
	[numStageDetailsId] [numeric](18, 0) NOT NULL,
	[numDomainID] [numeric](18, 0) NULL,
	[dtStartDate] [datetime] NULL,
	[dtEndDate] [datetime] NULL
 CONSTRAINT [PK_StagePercentageDetails_TempDateFields] PRIMARY KEY CLUSTERED 
(
	[numStageDetailsId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]

GO
-----------------------------------------------------------

GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[WorkFlowARAgingExecutionHistory](
	[numWFDateFieldID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numDomainID] [numeric](18, 0) NOT NULL,
	[dtCreatedDate] [datetime] NOT NULL,
	[numRecordID] [numeric](18, 0) NOT NULL,
	[numFormID] [numeric](18, 0) NOT NULL,
	[numWFID] [numeric](18, 0) NULL,
 CONSTRAINT [PK_WorkFlowARAgingExecutionHistory] PRIMARY KEY CLUSTERED 
(
	[numWFDateFieldID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]

GO
----------------------------------------------------------------
GO

GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[WorkFlowDateFieldExecutionHistory](
	[numWFDateFieldID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numDomainID] [numeric](18, 0) NOT NULL,
	[dtCreatedDate] [datetime] NOT NULL,
	[numRecordID] [numeric](18, 0) NOT NULL,
	[numFormID] [numeric](18, 0) NOT NULL,
	[numWFID] [numeric](18, 0) NULL,
 CONSTRAINT [PK_WorkFlowDateFieldExecutionHistory] PRIMARY KEY CLUSTERED 
(
	[numWFDateFieldID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]

GO
---------------------------------------------------------------------
INSERT INTO DynamicFormMaster
(
	numFormId
	,vcFormName
	,cCustomFieldsAssociated
	,cAOIAssociated
	,bitDeleted
	,tintFlag
	,bitWorkFlow
	,vcLocationID
	,bitAllowGridColor
	,numBizFormModuleID
)
VALUES
(
	138
	,'A/R (Aging)'
	,'Y'
	,'N'
	,0
	,3
	,0
	,''
	,0
	,null
)
------------------------------------------------------------------------------------------
INSERT INTO dbo.DycFormField_Mapping (
	numModuleID,numFieldID,numDomainID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,
	PopupFunctionName,[order],tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,
	bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,bitAllowFiltering,bitRequired,numFormFieldID,intSectionID,bitAllowGridColor)
SELECT numModuleID,numFieldID,numDomainID,138,1,0,vcFieldName,vcAssociatedControlType,vcPropertyName,
	PopupFunctionName,[order],tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,0,0,
	0,0,1,0,0,0,bitRequired,numFormFieldID,intSectionID,0
	 FROM dbo.DycFormField_Mapping WHERE numFormID=68 AND numFieldID = 678 AND vcFieldName = 'On Credit Hold'

----------------------------------------------------------------------------------------------------
INSERT INTO dbo.DycFormField_Mapping (
	numModuleID,numFieldID,numDomainID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,
	PopupFunctionName,[order],tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,
	bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,bitAllowFiltering,bitRequired,numFormFieldID,intSectionID,bitAllowGridColor)
SELECT numModuleID,numFieldID,numDomainID,70,1,0,vcFieldName,vcAssociatedControlType,vcPropertyName,
	PopupFunctionName,[order],tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,0,0,
	0,0,1,0,0,0,bitRequired,numFormFieldID,intSectionID,0
	 FROM dbo.DycFormField_Mapping WHERE numFormID=39 AND numFieldID = 40730 AND vcFieldName = 'Release Date'

---------------------------------------

Update DycFormField_Mapping
SET bitDetailField = 1
WHERE (numFormID = 35 OR numFormID = 36) AND (numFieldID = 51 OR numFieldID = 52 OR numFieldID = 53 OR numFieldID = 59 OR numFieldID = 60)