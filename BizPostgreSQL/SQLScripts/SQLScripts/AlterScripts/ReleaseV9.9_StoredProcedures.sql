/******************************************************************
Project: Release 9.9 Date: 02.JUL.2018
Comments: STORE PROCEDURES
*******************************************************************/

GO
IF EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[AfterInsertOpportunityBizDocs]'))
	DROP TRIGGER [dbo].[AfterInsertOpportunityBizDocs]
GO

CREATE TRIGGER [dbo].[AfterInsertOpportunityBizDocs]
ON [dbo].[OpportunityBizDocs]
AFTER INSERT
AS
  DECLARE  @numOppBizDocsId NUMERIC(9)
  DECLARE  @numOppId NUMERIC(9)
  DECLARE @numBizDocId NUMERIC(9)
  DECLARE @numBizDocStatus NUMERIC(9)
  DECLARE @numDomainID NUMERIC(9)
  DECLARE @vcBizDocID VARCHAR(250)
BEGIN

	INSERT INTO ElasticSearchModifiedRecords 
	(
		vcModule
		,numRecordID
		,vcAction
		,numDomainID
	)
	SELECT 
		'BizDoc'
		,numOppBizDocsId
		,'Insert'
		,OpportunityMaster.numDomainID 
	FROM 
		INSERTED 
	INNER JOIN 
		OpportunityMaster 
	ON 
		INSERTED.numOppId = OpportunityMaster.numOppId

	SELECT 
		@numOppBizDocsId = numOppBizDocsId
		,@numOppId = numOppId
	FROM 
		INSERTED
  
	INSERT INTO SalesFulfillmentQueue
	(
		numDomainID
		,numOppID
		,numUserCntID
		,numOrderStatus
		,dtDate
		,bitExecuted
		,intNoOfTimesTried
	)
	SELECT 
		OpportunityMaster.numDomainID
		,INSERTED.numOppId
		,INSERTED.numCreatedBy
		,-1
		,GETUTCDATE()
		,0
		,0
	FROM 
		INSERTED 
	INNER JOIN 
		OpportunityMaster 
	ON 
		INSERTED.numOppId = OpportunityMaster.numOppId
	LEFT JOIN 
		SalesFulfillmentQueue
	ON
		INSERTED.numOppId=SalesFulfillmentQueue.numOppID
		AND SalesFulfillmentQueue.numOrderStatus = -1
		AND ISNULL(SalesFulfillmentQueue.bitExecuted,0) = 0
	WHERE
		INSERTED.numBizDocId IN (287,296)
		AND SalesFulfillmentQueue.numSFQID IS NULL

	UPDATE 
		[OpportunityBizDocs]
	SET
		monDealAmount = dbo.[GetDealAmount](@numOppId,GETUTCDATE(),@numOppBizDocsId)
	WHERE 
		[numOppBizDocsId] = @numOppBizDocsId
  
SET NOCOUNT ON;
declare BizActionCursor CURSOR FOR
select numBizDocId,numBizDocStatus,numOppId,numOppBizDocsId,vcBizDocID from Inserted

OPEN BizActionCursor



FETCH   NEXT FROM BizActionCursor 
into @numBizDocId,@numBizDocStatus,@numOppId,@numOppBizDocsId,@vcBizDocID;
WHILE @@FETCH_STATUS = 0
   BEGIN
	
	select @numDomainID=numDomainID from OpportunityMaster where numOppId=@numOppId;

	insert into Communication

	select 972,numEmployeeId,ACI.numDivisionid,'BizDoc No: ' + @vcBizDocID + ' Created With Status: ' + dbo.GetListIemName(@numBizDocStatus)  ,0,0,0,numActionTypeId,numEmployeeId,
		0,0,0,numEmployeeId,getutcdate(),numEmployeeId,getutcdate(),@numDomainID,0,Null,0,getutcdate(),getutcdate(),numEmployeeId,1,0,0,0,0,0,0,numActionTypeId,0,0,null,null from BizDocStatusApprove BS 
		inner join AdditionalContactsInformation ACI 
		on ACI.numContactId=BS.numEmployeeID and 
		BS.numBizDocTypeId=@numBizDocId and 
		BS.numBizDocStatusID=@numBizDocStatus and 
		ACI.numDomainID=BS.numDomainID and 
		BS.numDomainID=@numDomainID

	FETCH  NEXT  FROM BizActionCursor 
		into @numBizDocId,@numBizDocStatus,@numOppId,@numOppBizDocsId,@vcBizDocID;
END;

CLOSE BizActionCursor;

END
GO
IF EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[AfterUpdateOpportunityBizDocs]'))
	DROP TRIGGER [dbo].[AfterUpdateOpportunityBizDocs]
GO

CREATE TRIGGER [dbo].[AfterUpdateOpportunityBizDocs]
ON [dbo].[OpportunityBizDocs]
After UPDATE 
AS
  DECLARE  @numOppBizDocsId NUMERIC(9)
  DECLARE  @numOppId NUMERIC(9)
	DECLARE @numBizDocId NUMERIC(9)
  DECLARE @numBizDocStatus NUMERIC(9)
  DECLARE @numDomainID NUMERIC(9)
  DECLARE @vcBizDocID VARCHAR(250)

  INSERT INTO ElasticSearchModifiedRecords (vcModule,numRecordID, vcAction, numDomainID) SELECT 'BizDoc',numOppBizDocsId,'Update',OpportunityMaster.numDomainID FROM INSERTED INNER JOIN OpportunityMaster ON INSERTED.numOppId = OpportunityMaster.numOppId

	INSERT INTO SalesFulfillmentQueue
	(
		numDomainID
		,numOppID
		,numUserCntID
		,numOrderStatus
		,dtDate
		,bitExecuted
		,intNoOfTimesTried
	)
	SELECT 
		OpportunityMaster.numDomainID
		,INSERTED.numOppId
		,INSERTED.numModifiedBy
		,-1
		,GETUTCDATE()
		,0
		,0
	FROM 
		INSERTED 
	INNER JOIN 
		OpportunityMaster 
	ON 
		INSERTED.numOppId = OpportunityMaster.numOppId
	LEFT JOIN 
		SalesFulfillmentQueue
	ON
		INSERTED.numOppId=SalesFulfillmentQueue.numOppID
		AND SalesFulfillmentQueue.numOrderStatus = -1
		AND ISNULL(SalesFulfillmentQueue.bitExecuted,0) = 0
	WHERE
		INSERTED.numBizDocId IN (287,296)
		AND SalesFulfillmentQueue.numSFQID IS NULL


--IF UPDATE(fltDiscount) OR update(bitDiscountType) OR update(monAmountPaid) OR update(bitPartialFulfilment) OR update(monShipCost) OR update(numShipVia) OR update(bitBillingTerms) OR update(intBillingDays) OR update(bitInterestType) OR update(fltInterest)   ---(COLUMNS_UPDATED() & 1332740320) > 0
  IF update(monAmountPaid) OR update(bitPartialFulfilment) OR update(monShipCost) OR update(numShipVia)    ---(COLUMNS_UPDATED() & 1332740320) > 0
    BEGIN
      SELECT @numOppBizDocsId = numOppBizDocsId,
             @numOppId = numOppId
      FROM   deleted
      UPDATE [OpportunityBizDocs]
      SET    monDealAmount = dbo.[GetDealAmount](@numOppId,GETUTCDATE(),@numOppBizDocsId)
      WHERE  [numOppBizDocsId] = @numOppBizDocsId
--      PRINT @numOppBizDocsId
    END

SET NOCOUNT ON;

if UPDATE(numBizDocStatus)
BEGIN
	declare BizActionCursor CURSOR FOR
	select numBizDocId,numBizDocStatus,numOppId,numOppBizDocsId,vcBizDocID from Inserted

OPEN BizActionCursor



FETCH   NEXT FROM BizActionCursor 
into @numBizDocId,@numBizDocStatus,@numOppId,@numOppBizDocsId,@vcBizDocID;
WHILE @@FETCH_STATUS = 0
   BEGIN
	
	select @numDomainID=numDomainID from OpportunityMaster where numOppId=@numOppId;

	insert into Communication

	select 972,numEmployeeId,ACI.numDivisionid,'BizDoc No: ' + @vcBizDocID + ' Status Into: ' + dbo.GetListIemName(@numBizDocStatus)  ,0,0,0,numActionTypeId,numEmployeeId,
		0,0,0,numEmployeeId,getutcdate(),numEmployeeId,getutcdate(),@numDomainID,0,Null,0,getutcdate(),getutcdate(),numEmployeeId,1,0,0,0,0,0,0,numActionTypeId,0,0,null,null from BizDocStatusApprove BS 
		inner join AdditionalContactsInformation ACI 
		on ACI.numContactId=BS.numEmployeeID and 
		BS.numBizDocTypeId=@numBizDocId and 
		BS.numBizDocStatusID=@numBizDocStatus and 
		ACI.numDomainID=BS.numDomainID and 
		BS.numDomainID=@numDomainID

	FETCH  NEXT  FROM BizActionCursor 
		into @numBizDocId,@numBizDocStatus,@numOppId,@numOppBizDocsId,@vcBizDocID;
END;

	CLOSE BizActionCursor;
END
    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='CheckAssemblyBuildStatus')
DROP FUNCTION CheckAssemblyBuildStatus
GO
CREATE FUNCTION [dbo].[CheckAssemblyBuildStatus] 
(
	@numItemKitID NUMERIC(18,0)
	,@numQty NUMERIC(18,0)
	,@bitSalesOrder BIT
	,@tintCommitAllocation TINYINT
)
RETURNS BIT
AS
BEGIN
    DECLARE @IsReadyToBuild BIT = 1

	DECLARE @Temp TABLE
	(
		numItemCode NUMERIC(18,0),
		numQtyRequired INT,
		numOnAllocation FLOAT,
		bitReadyToBuild BIT
	)

	INSERT INTO 
		@Temp
	SELECT
		numChildItemID,
		CAST(ISNULL(numQtyItemsReq * @numQty,0) AS INT),
		WareHouseItems.numAllocation,
		CASE 
			WHEN @bitSalesOrder=1 AND @tintCommitAllocation=2 
			THEN (CASE WHEN ISNULL(numQtyItemsReq * @numQty,0) > WareHouseItems.numOnHand THEN 0 ELSE 1 END)
			ELSE (CASE WHEN ISNULL(numQtyItemsReq * @numQty,0) > WareHouseItems.numAllocation THEN 0 ELSE 1 END) 
		END AS bitReadyToBuild 
	FROM
		ItemDetails
	INNER JOIN
		WareHouseItems
	ON
		ItemDetails.numWareHouseItemId = WareHouseItems.numWareHouseItemID
	WHERE
		numItemKitID = @numItemKitID

	IF (SELECT COUNT(*) FROM @Temp WHERE  bitReadyToBuild = 0) > 0
	BEGIN
		SET @IsReadyToBuild = 0
	END

    RETURN @IsReadyToBuild
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetOrderItemAvailableQtyToShip')
DROP FUNCTION GetOrderItemAvailableQtyToShip
GO
CREATE FUNCTION [dbo].[GetOrderItemAvailableQtyToShip] 
(	
	@numOppItemCode AS NUMERIC(18,0),
	@numWarehouseItemID NUMERIC(18,0),
	@bitKitParent BIT,
	@tintCommitAllocation TINYINT,
	@bitAllocateInventoryOnPickList BIT
)
RETURNS FLOAT
AS
BEGIN
	DECLARE @numQuantity AS FLOAT = 0
    DECLARE @numAvailableQtyToShip FLOAT = 0
	DECLARE @numAllocation FLOAT
	DECLARE @numQtyShipped FLOAT = 0

	SELECT @numQuantity=ISNULL(numUnitHour,0),@numQtyShipped = ISNULL(numQtyShipped,0) FROM OpportunityItems WHERE numoppitemtCode = @numOppItemCode

	IF @bitKitParent = 1
	BEGIN
		DECLARE @TEMPTABLE TABLE
		(
			numItemCode NUMERIC(18,0),
			numParentItemID NUMERIC(18,0),
			numOppChildItemID NUMERIC(18,0),
			vcItemName VARCHAR(300),
			bitKitParent BIT,
			numWarehouseItmsID NUMERIC(18,0),
			numQtyItemsReq FLOAT,
			numQtyItemReq_Orig FLOAT,
			numOnHand FLOAT,
			numAllocation FLOAT,
			numAvailableQtyToShip INT
		)

		INSERT INTO 
			@TEMPTABLE
		SELECT
			OKI.numChildItemID,
			0,
			OKI.numOppChildItemID,
			I.vcItemName,
			I.bitKitParent,
			OKI.numWareHouseItemId,
			OKI.numQtyItemsReq_Orig * (ISNULL(OI.numUnitHour,0) - ISNULL(OI.numQtyShipped,0)),
			OKI.numQtyItemsReq_Orig,
			ISNULL(WI.numOnHand,0),
			ISNULL(WI.numAllocation,0),
			0
		FROM
			OpportunityKitItems OKI
		INNER JOIN
			WareHouseItems WI
		ON
			OKI.numWareHouseItemId = WI.numWareHouseItemID
		INNER JOIN
			OpportunityItems OI
		ON
			OKI.numOppItemID = OI.numoppitemtCode
		INNER JOIN
			Item I
		ON
			OKI.numChildItemID = I.numItemCode
		WHERE
			OI.numoppitemtCode = @numOppItemCode  

		INSERT INTO
			@TEMPTABLE
		SELECT
			OKCI.numItemID,
			t1.numItemCode,
			t1.numOppChildItemID,
			I.vcItemName,
			I.bitKitParent,
			OKCI.numWareHouseItemId,
			OKCI.numQtyItemsReq_Orig * t1.numQtyItemsReq,
			OKCI.numQtyItemsReq_Orig,
			ISNULL(WI.numOnHand,0),
			ISNULL(WI.numAllocation,0),
			0
		FROM
			OpportunityKitChildItems OKCI
		INNER JOIN
			WareHouseItems WI
		ON
			OKCI.numWareHouseItemId = WI.numWareHouseItemID
		INNER JOIN
			@TEMPTABLE t1
		ON
			OKCI.numOppChildItemID = t1.numOppChildItemID
		INNER JOIN
			Item I
		ON
			OKCI.numItemID = I.numItemCode
		
		-- FIRST GET SUB KIT BACK ORDER NUMBER
		IF @tintCommitAllocation=2 AND ISNULL(@bitAllocateInventoryOnPickList,1)=0
		BEGIN
			UPDATE 
				@TEMPTABLE 
			SET 
				numAvailableQtyToShip = (CASE 
											WHEN ISNULL(numOnHand,0) = 0 THEN 0
											WHEN  numOnHand>= numQtyItemsReq THEN (numQtyItemsReq/numQtyItemReq_Orig)
											ELSE FLOOR(ISNULL(numOnHand,0)/numQtyItemReq_Orig)
										END)
			WHERE
				bitKitParent = 0
		END
		ELSE
		BEGIN
			UPDATE 
				@TEMPTABLE 
			SET 
				numAvailableQtyToShip = (CASE 
											WHEN ISNULL(numAllocation,0) = 0 THEN 0
											WHEN  numAllocation>= numQtyItemsReq THEN (numQtyItemsReq/numQtyItemReq_Orig)
											ELSE FLOOR(ISNULL(numAllocation,0)/numQtyItemReq_Orig)
										END)
			WHERE
				bitKitParent = 0
		END

		
		-- NOW USE SUB KIT BACK ORDER NUMBER TO GENERATE PARENT KIT BACK ORDER NUMBER
		UPDATE 
			t2
		SET 
			t2.numAvailableQtyToShip = CEILING((SELECT MAX(numAvailableQtyToShip) FROM @TEMPTABLE t1 WHERE t1.numParentItemID = t2.numItemCode) / t2.numQtyItemReq_Orig)
		FROM
			@TEMPTABLE t2
		WHERE
			bitKitParent = 1

		SELECT @numAvailableQtyToShip = MIN(numAvailableQtyToShip) FROM @TEMPTABLE WHERE ISNULL(numParentItemID,0) = 0
	END
	ELSE
	BEGIN
		SELECT
			@numAllocation = numAllocation
		FROM
			WareHouseItems 
		WHERE
			numWareHouseItemID = @numWarehouseItemID

		IF (@numQuantity - @numQtyShipped) >= @numAllocation
            SET @numAvailableQtyToShip = @numAllocation
		ELSE
			SET @numAvailableQtyToShip = (@numQuantity - @numQtyShipped)
	END

	RETURN @numAvailableQtyToShip
END
GO
/****** Object:  StoredProcedure [dbo].[usp_CompanyDivision]    Script Date: 07/26/2008 16:15:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_companydivision')
DROP PROCEDURE usp_companydivision
GO
CREATE PROCEDURE [dbo].[usp_CompanyDivision]                                         
 @numDivisionID  numeric=0,                                           
 @numCompanyID  numeric=0,                                           
 @vcDivisionName  varchar (100)='',                                                                
 @numGrpId   numeric=0,                                                                       
 @numTerID   numeric=0,                                          
 @bitPublicFlag  bit=0,                                          
 @tintCRMType  tinyint=0,                                          
 @numUserCntID  numeric=0,                                                                                                                                     
 @numDomainID  numeric=0,                                          
 @bitLeadBoxFlg  bit=0,  --Added this because when called form leadbox this value will be 1 else 0                                          
 @numStatusID  numeric=0,                                        
 @numCampaignID numeric=0,                            
 @numFollowUpStatus numeric(9)=0,                                                           
 @tintBillingTerms as tinyint,                                          
 @numBillingDays as numeric(9),                                         
 @tintInterestType as tinyint,                                          
 @fltInterest as float,                          
 @vcComPhone as varchar(50),                
 @vcComFax as varchar(50),        
 @numAssignedTo as numeric(9)=0,
 @bitNoTax as BIT,
 @bitSelectiveUpdate BIT=0,
@numCompanyDiff as numeric(9)=0,
@vcCompanyDiff as varchar(100)='',
@bitActiveInActive as bit=1,
@numCurrencyID AS numeric(9)=0,
@vcShipperAccountNo	VARCHAR(100),
@intShippingCompany INT = 0,
@numDefaultExpenseAccountID NUMERIC(18,0) = 0,
@numAccountClass NUMERIC(18,0) = 0,
@tintPriceLevel INT = 0,
@vcPartnerCode VARCHAR(200)=null,
@numPartenerContactId NUMERIC(18,0)=0,
@numPartenerSourceId NUMERIC(18,0)=0,
@bitEmailToCase BIT,
@bitShippingLabelRequired BIT,
@tintInbound850PickItem INT = 0
AS                                                                       
BEGIN                                          
                                         
                                         
IF @numDivisionId is null OR @numDivisionId=0                                          
BEGIN                                                                       
	INSERT INTO DivisionMaster                      
	(
		numCompanyID
		,vcDivisionName
		,numGrpId
		,numFollowUpStatus
		,bitPublicFlag
		,numCreatedBy
		,bintCreatedDate
		,numModifiedBy
		,bintModifiedDate
		,tintCRMType
		,numDomainID
		,bitLeadBoxFlg
		,numTerID
		,numStatusID
		,bintLeadProm
		,bintLeadPromBy
		,bintProsProm
		,bintProsPromBy
		,numRecOwner
		,tintBillingTerms
		,numBillingDays
		,tintInterestType
		,fltInterest
		,vcComPhone
		,vcComFax
		,numCampaignID
		,bitNoTax
		,numAssignedTo
		,numAssignedBy
		,numCompanyDiff
		,vcCompanyDiff
		,numCurrencyID
		,vcShippersAccountNo
		,intShippingCompany
		,numDefaultExpenseAccountID
		,numAccountClassID
		,tintPriceLevel
		,bitEmailToCase
		,bitShippingLabelRequired
		,tintInbound850PickItem
	)                     
	VALUES
	(
		@numCompanyID
		,@vcDivisionName
		,@numGrpId
		,0
		,@bitPublicFlag
		,@numUserCntID
		,GETUTCDATE()
		,@numUserCntID
		,GETUTCDATE()
		,@tintCRMType
		,@numDomainID
		,@bitLeadBoxFlg
		,@numTerID
		,@numStatusID
		,NULL
		,NULL
		,NULL
		,NULL
		,@numUserCntID
		,0
		,0
		,0
		,0
		,@vcComPhone
		,@vcComFax
		,@numCampaignID
		,@bitNoTax
		,@numAssignedTo
		,@numUserCntID
		,@numCompanyDiff
		,@vcCompanyDiff
		,@numCurrencyID
		,@vcShipperAccountNo
		,@intShippingCompany
		,@numDefaultExpenseAccountID
		,ISNULL(@numAccountClass,0)
		,ISNULL(@tintPriceLevel,0)
		,ISNULL(@bitEmailToCase,0)
		,@bitShippingLabelRequired
		,@tintInbound850PickItem
	)                                       
                                    
	--Return the ID auto generated by the above INSERT.                                          
	SELECT @numDivisionId = SCOPE_IDENTITY()
	
	DECLARE @numGroupID NUMERIC       
	SELECT TOP 1 @numGroupID=numGroupID FROM dbo.AuthenticationGroupMaster WHERE numDomainID=@numDomainID AND tintGroupType=2
	
	IF @numGroupID IS NULL 
	BEGIN
		SET @numGroupID = 0          
    END
	                                
	INSERT INTO ExtarnetAccounts 
	(
		numCompanyID,numDivisionID,numGroupID,numDomainID
	)                                  
    VALUES
	(
		@numCompanyID,@numDivisionId,@numGroupID,@numDomainID
	)                                  
                                            
    SELECT @numDivisionId                                 
 END                                          
 ELSE                                         
 BEGIN
	IF @bitSelectiveUpdate = 0 
	BEGIN
			declare @numFollow as varchar(10)                          
			declare @binAdded as varchar(20)                          
			declare @PreFollow as varchar(10)                          
			declare @RowCount as varchar(2)                          
			set @PreFollow=(select count(*)  numFollowUpStatus from FollowUpHistory where numDivisionID= @numDivisionID and numDomainId= @numDomainID)                          
			set @RowCount=@@rowcount                          
			select   @numFollow=numFollowUpStatus,   @binAdded=bintModifiedDate from divisionmaster where numDivisionID=@numDivisionID and numDomainId= @numDomainID                    
			if @numFollow <>'0' and @numFollow <> @numFollowUpStatus                          
			begin                          
			select top 1  numFollowUpStatus from FollowUpHistory where numDivisionID= @numDivisionID and numDomainId= @numDomainID order by numFollowUpStatusID desc                          
			                    
			if @PreFollow<>0                          
			begin                          
			                   
			if @numFollow <> (select top 1  numFollowUpStatus from FollowUpHistory where numDivisionID= @numDivisionID and numDomainId= @numDomainID order by numFollowUpStatusID desc)                          
			begin                          
			                      
				  insert into FollowUpHistory(numFollowUpstatus,numDivisionID,bintAddedDate,numDomainId)                          
						 values(@numFollow,@numDivisionID,@binAdded,@numDomainID)                          
				end                          
			end                          
			else                          
			begin                          
			                     
			insert into FollowUpHistory(numFollowUpstatus,numDivisionID,bintAddedDate,numDomainId)                          
						 values(@numFollow,@numDivisionID,@binAdded,@numDomainID)                          
			end                          
			                         
			end                          
			              
			                                      
			UPDATE DivisionMaster SET  numCompanyID = @numCompanyID ,vcDivisionName = @vcDivisionName,                                      
			numGrpId = @numGrpId,                                                           
			numFollowUpStatus =@numFollowUpStatus,                                                                 
			numTerID = @numTerID,                                       
			bitPublicFlag =@bitPublicFlag,                                            
			numModifiedBy = @numUserCntID,                                            
			bintModifiedDate = getutcdate(),                                         
			tintCRMType = @tintCRMType,                                          
			numStatusID = @numStatusID,                                                                            
			tintBillingTerms = @tintBillingTerms,                                          
			numBillingDays = @numBillingDays,                                         
			tintInterestType = @tintInterestType,                                           
			fltInterest =@fltInterest,                
			vcComPhone=@vcComPhone,                
			vcComFax=@vcComFax,            
			numCampaignID=@numCampaignID,
			bitNoTax=@bitNoTax,numCompanyDiff=@numCompanyDiff,vcCompanyDiff=@vcCompanyDiff,bitActiveInActive=@bitActiveInActive,numCurrencyID=@numCurrencyID,
			vcShippersAccountNo = @vcShipperAccountNo,
			intShippingCompany = @intShippingCompany,
			numDefaultExpenseAccountID = @numDefaultExpenseAccountID,
			numAccountClassID=ISNULL(@numAccountClass,0),
			tintPriceLevel=ISNULL(@tintPriceLevel,0),
			numPartenerSource=@numPartenerSourceId,
			numPartenerContact=@numPartenerContactId,
			bitEmailToCase=ISNULL(@bitEmailToCase,0),
			bitShippingLabelRequired=@bitShippingLabelRequired,
			tintInbound850PickItem = @tintInbound850PickItem
			WHERE numDivisionID = @numDivisionID   and numDomainId= @numDomainID     
			    
			
			---Updating if organization is assigned to someone            
			declare @tempAssignedTo as numeric(9)          
			set @tempAssignedTo=null           
			select @tempAssignedTo=isnull(numAssignedTo,0) from DivisionMaster where numDivisionID = @numDivisionID and numDomainId= @numDomainID           
			print @tempAssignedTo          
			if (@tempAssignedTo<>@numAssignedTo and  @numAssignedTo<>'0')          
			begin            
			update DivisionMaster set numAssignedTo=@numAssignedTo ,numAssignedBy=@numUserCntID where numDivisionID = @numDivisionID  and numDomainId= @numDomainID           
			end           
			else if  (@numAssignedTo =0)          
			begin          
			update DivisionMaster set numAssignedTo=0 ,numAssignedBy=0 where numDivisionID = @numDivisionID  and numDomainId= @numDomainID        
			end                                            
			
			DECLARE @staus INT=@numDivisionID
			IF(LEN(@vcPartnerCode)>0)
			BEGIN
				IF(SELECT COUNT(numDivisionID) FROM DivisionMaster WHERE numDivisionID <> @numDivisionID AND vcPartnerCode=@vcPartnerCode and numDomainId= @numDomainID)>0
				BEGIN
					 SET @staus=-1
				END   
				ELSE
				BEGIN
					UPDATE DivisionMaster SET vcPartnerCode=@vcPartnerCode WHERE numDivisionID = @numDivisionID
					SET @staus=@numDivisionID
				END                   
			              
			END				  
			                   
			SELECT @staus
 
 
 
		END                     
END
	
	IF @bitSelectiveUpdate = 1
	BEGIN
		DECLARE @sql VARCHAR(1000)
		SET @sql = 'update DivisionMaster Set '

		IF(LEN(@vcComPhone)>0)
		BEGIN
			SET @sql =@sql + ' vcComPhone=''' + @vcComPhone + ''', '
		END
		IF(LEN(@vcComFax)>0)
		BEGIN
			SET @sql =@sql + ' vcComFax=''' + @vcComFax + ''', '
		END
		
		IF(@numUserCntID>0)
		BEGIN
			SET @sql =@sql + ' numModifiedBy='+ CONVERT(VARCHAR(10),@numUserCntID) +', '
		END
	
		SET @sql=SUBSTRING(@sql,0,LEN(@sql)) + ' '
		SET @sql =@sql + 'where numDivisionID= '+CONVERT(VARCHAR(10),@numDivisionID)

		PRINT @sql
		EXEC(@sql)
	END
	
	

end
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_createbizdocs')
DROP PROCEDURE usp_createbizdocs
GO
CREATE PROCEDURE [dbo].[USP_CreateBizDocs]                        
(                                              
 @numOppId as numeric(9)=null,                        
 @numBizDocId as numeric(9)=null,                        
 @numUserCntID as numeric(9)=null,                        
 @numOppBizDocsId as numeric(9)=NULL OUTPUT,
 @vcComments as varchar(1000),
 @bitPartialFulfillment as bit,
 @strBizDocItems as text='' ,
 @numShipVia as numeric(9),
 @vcTrackingURL as varchar(1000),
 @dtFromDate AS DATETIME,
 @numBizDocStatus as numeric(9) = 0,
 @bitRecurringBizDoc AS BIT = NULL,
 @numSequenceId AS VARCHAR(50) = '',
 @tintDeferred as tinyint=0,
 @monCreditAmount as DECIMAL(20,5)= 0 OUTPUT,
 @ClientTimeZoneOffset as int=0,
 @monDealAmount as DECIMAL(20,5)= 0 OUTPUT,
 @bitRentalBizDoc as bit=0,
 @numBizDocTempID as numeric(9)=0,
 @vcTrackingNo AS VARCHAR(500),
 @vcRefOrderNo AS VARCHAR(100),
 @OMP_SalesTaxPercent AS FLOAT = 0, --(joseph) Added to Get Online Marketplace Sales Tax Percentage
 @numFromOppBizDocsId AS NUMERIC(9)=0,
 @bitTakeSequenceId AS BIT=0,
 @bitAllItems AS BIT=0,
 @bitNotValidateBizDocFulfillment AS BIT=0,
 @fltExchangeRateBizDoc AS FLOAT=0,
 @bitRecur BIT = 0,
 @dtStartDate DATE = NULL,
 @dtEndDate DATE = NULL,
 @numFrequency SMALLINT = 0,
 @vcFrequency VARCHAR(20) = '',
 @numRecConfigID NUMERIC(18,0) = 0,
 @bitDisable bit = 0,
 @bitInvoiceForDeferred BIT = 0,
 @numSourceBizDocId Numeric(18,0)=0,
 @bitShippingBizDoc BIT=0,
 @vcVendorInvoiceName  VARCHAR(100) = NULL
)                        
AS 
BEGIN TRY
	DECLARE @hDocItem as INTEGER
	DECLARE @numDivisionID AS NUMERIC(9)
	DECLARE @numDomainID NUMERIC(9)
	DECLARE @tintOppType AS TINYINT	
	DECLARE @numFulfillmentOrderBizDocId NUMERIC(9);
	DECLARE @bitShippingGenerated BIT=0;

	SELECT 
		@numDomainID=numDomainID,
		@numDivisionID=numDivisionID,
		@tintOppType=tintOppType 
	FROM 
		OpportunityMaster 
	WHERE 
		numOppID=@numOppId

	DECLARE @tintCommitAllocation AS TINYINT
	DECLARE @bitAllocateInventoryOnPickList AS BIT

	SELECT @tintCommitAllocation=ISNULL(tintCommitAllocation,1) FROM Domain WHERE numDomainId=@numDomainID
	SELECT @bitAllocateInventoryOnPickList=ISNULL(bitAllocateInventoryOnPickList,0) FROM DivisionMaster WHERE numDivisionID=@numDivisionID

	IF ISNULL(@numFromOppBizDocsId,0) > 0
	BEGIN
		IF (SELECT 
				COUNT(*) 
			FROM 
			(
				SELECT
					OBDI.numOppItemID
					,OI.numUnitHour AS OrderedQty
					,OBDI.numUnitHour AS FromBizDocQty
					,OtherSameBizDocType.BizDocQty
				FROM
					OpportunityBizDocs OBD
				INNER JOIN
					OpportunityBizDocItems OBDI
				ON
					OBD.numOppBizDocsId = OBDI.numOppBizDocID
				INNER JOIN
					OpportunityItems OI
				ON
					OI.numoppitemtCode = OBDI.numOppItemID
				OUTER APPLY
				(
					SELECT
						SUM(OpportunityBizDocItems.numUnitHour) BizDocQty
					FROM
						OpportunityBizDocs
					INNER JOIN
						OpportunityBizDocItems
					ON
						OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
					WHERE
						OpportunityBizDocs.numOppId = OBD.numOppId
						AND OpportunityBizDocItems.numOppItemID=OBDI.numOppItemID
						AND OpportunityBizDocs.numBizDocId = @numBizDocId
				) AS OtherSameBizDocType
				WHERE
					OBD.numOppBizDocsId = @numFromOppBizDocsId
			) X
			WHERE
				ISNULL(X.FromBizDocQty,0) > (ISNULL(X.OrderedQty,0) - ISNULL(X.BizDocQty,0))) > 0
		BEGIN
			-- IF SOMEONE HAS ALREADY CREATED OTHE SAME BIZDOC TYPE AND ENOUGHT QTY IS NOT LEFT FOR ALL ITEMS OF BIZDOC THAN BIZDOC CAN NOT BE CREATED AGAINST VENDOR INVOICE
			RAISERROR('BIZDOC_ALREADY_GENERATED_AGAINST_SOURCE',16,1)
		END
	END

	SET @numFulfillmentOrderBizDocId=296 
	    
	IF ISNULL(@fltExchangeRateBizDoc,0)=0
		SELECT @fltExchangeRateBizDoc=fltExchangeRate FROM OpportunityMaster where numOppID=@numOppId
	
	IF(@bitShippingBizDoc=1)
	BEGIN
		SET @bitShippingGenerated=1
	END
	ELSE
	BEGIN
	IF((SELECT COUNT(numBizDocId) FROM OpportunityBizDocs WHERE bitShippingGenerated=1 AND numOppId=@numOppId)=0)
	BEGIN
		IF((SELECT TOP 1 CAST(numDefaultSalesShippingDoc AS int) FROM Domain WHERE numDomainId=@numDomainID)=@numBizDocId)
		BEGIN
			SET @bitShippingGenerated=1
		END
	END
	END

	IF @numBizDocTempID=0
	BEGIN
		SELECT 
			@numBizDocTempID=numBizDocTempID 
		FROM 
			BizDocTemplate 
		WHERE 
			numBizDocId=@numBizDocId 
			AND numDomainID=@numDomainID and numOpptype=@tintOppType 
			AND tintTemplateType=0 
			AND isnull(bitDefault,0)=1 
			AND isnull(bitEnabled,0)=1
	END

	IF @numOppBizDocsId > 0 AND @numBizDocId = 29397 AND @tintCommitAllocation=2 AND @bitAllocateInventoryOnPickList=1
	BEGIN
		-- Revert Allocation
		EXEC USP_RevertAllocationPickList @numDomainID,@numOppID,@numOppBizDocsId,@numUserCntID
	END

	IF @numOppBizDocsId = 0 AND @numBizDocId = 296 AND @tintCommitAllocation=2 AND @bitAllocateInventoryOnPickList=1
	BEGIN
		-- Revert Allocation
		IF ISNULL(@numFromOppBizDocsId,0) = 0 AND ISNULL(@numSourceBizDocId,0) = 0
		BEGIN
			RAISERROR('FULFILLMENT_BIZDOC_REQUIRED_PICKLIST_FOR_ALLOCATE_ON_PICKLIST_SETTING',16,1)
		END
		ELSE IF ISNULL(@numFromOppBizDocsId,0) > 0 AND ISNULL(@numSourceBizDocId,0) = 0
		BEGIN
			SET @numSourceBizDocId = @numFromOppBizDocsId
		END
	END

	IF @numOppBizDocsId=0
	BEGIN
		IF (
			(@bitPartialFulfillment = 1 AND NOT EXISTS (SELECT * FROM OpportunityBizDocs WHERE numOppID = @numOppId AND numBizDocId = @numBizDocId AND bitPartialFulfilment = 0))
			 OR (NOT EXISTS (SELECT * FROM OpportunityBizDocs WHERE numOppID = @numOppId AND numBizDocId = @numBizDocId AND bitPartialFulfilment = 0)
				 AND NOT EXISTS (SELECT * FROM OpportunityBizDocs WHERE numOppID = @numOppId AND numBizDocId = @numBizDocId AND bitPartialFulfilment = 1))
			 OR (@bitRecurringBizDoc = 1) -- Added new flag to add unit qty in case of recurring bizdoc
			 OR (@numFromOppBizDocsId > 0)
			)
		BEGIN
			DECLARE @tintShipped AS TINYINT	
			DECLARE @dtShipped AS DATETIME
			DECLARE @bitAuthBizdoc AS BIT
        
			SELECT 
				@tintShipped=ISNULL(tintShipped,0),
				@numDomainID=numDomainID 
			FROM 
				OpportunityMaster 
			WHERE numOppId=@numOppId                        
		
			IF @tintOppType = 1 AND @bitNotValidateBizDocFulfillment=0
				EXEC USP_ValidateBizDocFulfillment @numDomainID,@numOppId,@numFromOppBizDocsId,@numBizDocId
		

			IF @tintShipped =1 
				SET @dtShipped = GETUTCDATE();
			ELSE
				SET @dtShipped = null;
			IF @tintOppType = 1 AND (SELECT numAuthoritativeSales FROM [AuthoritativeBizDocs] WHERE numDomainID = @numDomainID) = @numBizDocId
				SET @bitAuthBizdoc = 1
			ELSE if @tintOppType = 2 AND (SELECT numAuthoritativePurchase FROM [AuthoritativeBizDocs] WHERE numDomainID = @numDomainID) = @numBizDocId
				SET @bitAuthBizdoc = 1
			ELSE 
				SET @bitAuthBizdoc = 0
		
			Declare @dtCreatedDate as datetime;SET @dtCreatedDate=Getutcdate()
			IF @tintDeferred=1
			BEGIn
				SET @dtCreatedDate=DateAdd(minute, @ClientTimeZoneOffset,@dtFromDate)
			END
	
			IF @bitTakeSequenceId=1
			BEGIN
				--Sequence #
				CREATE TABLE #tempSequence (numSequenceId bigint )
				INSERT INTO #tempSequence EXEC [dbo].[USP_GetScalerValue] @numDomainID ,33,@numBizDocId,0
				SELECT @numSequenceId =ISNULL(numSequenceId,0) FROM #tempSequence
				DROP TABLE #tempSequence
			
				--BizDoc Template ID
				CREATE TABLE #tempBizDocTempID (numBizDocTempID NUMERIC(9) )
				INSERT INTO #tempBizDocTempID EXEC [dbo].[USP_GetBizDocTemplateList] @numDomainID,@numBizDocId,1,1
				SELECT @numBizDocTempID =ISNULL(numBizDocTempID,0) FROM #tempBizDocTempID
				DROP TABLE #tempBizDocTempID
			END	
			
			--To insert Tracking Id of previous record into the new record
			IF @vcTrackingNo IS NULL OR @vcTrackingNo = ''
			BEGIN
				SELECT TOP 1 @vcTrackingNo = vcTrackingNo FROM OpportunityBizDocs WHERE numOppId = @numOppId ORDER BY numOppBizDocsId DESC 
			END
			INSERT INTO OpportunityBizDocs
			(
				numOppId
				,numBizDocId
				,numCreatedBy
				,dtCreatedDate
				,numModifiedBy
				,dtModifiedDate
				,vcComments
				,bitPartialFulfilment
				,numShipVia
				,vcTrackingURL
				,dtFromDate
				,numBizDocStatus
				,[dtShippedDate]
				,[bitAuthoritativeBizDocs]
				,tintDeferred
				,bitRentalBizDoc
				,numBizDocTempID
				,vcTrackingNo
				,vcRefOrderNo
				,numSequenceId
				,numBizDocStatusOLD
				,fltExchangeRateBizDoc
				,bitAutoCreated
				,[numMasterBizdocSequenceID]
				,bitShippingGenerated
				,numSourceBizDocId
				,vcVendorInvoice
			)
			VALUES     
			(
				@numOppId
				,@numBizDocId
				,@numUserCntID
				,@dtCreatedDate
				,@numUserCntID
				,GETUTCDATE()
				,@vcComments
				,@bitPartialFulfillment
				,@numShipVia
				,@vcTrackingURL
				,@dtFromDate
				,@numBizDocStatus
				,@dtShipped
				,ISNULL(@bitAuthBizdoc,0)
				,@tintDeferred
				,@bitRentalBizDoc
				,@numBizDocTempID
				,@vcTrackingNo
				,@vcRefOrderNo
				,@numSequenceId
				,0
				,@fltExchangeRateBizDoc
				,0
				,@numSequenceId
				,@bitShippingGenerated
				,@numSourceBizDocId
				,@vcVendorInvoiceName
			)
			
        
			SET @numOppBizDocsId = SCOPE_IDENTITY()
		
			--Added By:Sachin Sadhu||Date:27thAug2014
			--Purpose :MAke entry in WFA queue 
			IF EXISTS (SELECT 1 FROM dbo.OpportunityMaster WHERE numOppId = @numOppID AND tintOppType=1 AND isnull(tintOppStatus,0)=1 )
			BEGIN
				EXEC 
				USP_ManageOpportunityAutomationQueue			
				@numOppQueueID = 0, -- numeric(18, 0)							
				@numDomainID = @numDomainID, -- numeric(18, 0)							
				@numOppId = @numOppID, -- numeric(18, 0)							
				@numOppBizDocsId = @numOppBizDocsId, -- numeric(18, 0)							
				@numOrderStatus = 0, -- numeric(18, 0)					
				@numUserCntID = @numUserCntID, -- numeric(18, 0)					
				@tintProcessStatus = 1, -- tinyint						
				@tintMode = 1 -- TINYINT
			END 
		   --end of script

			--Deferred BizDocs : Create Recurring entry only for create Account Journal
			if @tintDeferred=1
			BEGIN
				exec USP_RecurringAddOpportunity @numOppId,@numOppBizDocsId,0,4,@dtCreatedDate,0,@numDomainID,0,0,100,''
			END

			-- Update name template set for bizdoc
			--DECLARE @tintType TINYINT
			--SELECT @tintType = CASE @tintOppType WHEN 1 THEN 3 WHEN 2 THEN 4 END;
			EXEC dbo.USP_UpdateBizDocNameTemplate @tintOppType,@numDomainID,@numOppBizDocsId

			IF @numFromOppBizDocsId>0
			BEGIN
				INSERT INTO OpportunityBizDocItems
  				(
					numOppBizDocID
					,numOppItemID
					,numItemCode
					,numUnitHour
					,monPrice
					,monTotAmount
					,vcItemDesc
					,numWarehouseItmsID
					,vcType
					,vcAttributes
					,bitDropShip
					,bitDiscountType
					,fltDiscount
					,monTotAmtBefDiscount
					,vcNotes
					,bitEmbeddedCost
					,monEmbeddedCost
					,dtRentalStartDate
					,dtRentalReturnDate
					,tintTrackingStatus
				)
				SELECT 
					@numOppBizDocsId
					,OBDI.numOppItemID
					,OBDI.numItemCode
					,OBDI.numUnitHour
					,OBDI.monPrice
					,OBDI.monTotAmount
					,OBDI.vcItemDesc
					,OBDI.numWarehouseItmsID
					,OBDI.vcType
					,OBDI.vcAttributes
					,OBDI.bitDropShip
					,OBDI.bitDiscountType
					,OBDI.fltDiscount
					,OBDI.monTotAmtBefDiscount
					,OBDI.vcNotes
					,OBDI.bitEmbeddedCost
					,OBDI.monEmbeddedCost
					,OBDI.dtRentalStartDate
					,OBDI.dtRentalReturnDate
					,OBDI.tintTrackingStatus
				FROM 
					OpportunityBizDocs OBD 
				JOIN OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId=OBDI.numOppBizDocID 
				JOIN OpportunityItems OI ON OBDI.numOppItemID=OI.numoppitemtCode
				JOIN Item I ON OBDI.numItemCode=I.numItemCode
				WHERE 
					OBD.numOppId=@numOppId 
					AND OBD.numOppBizDocsId=@numFromOppBizDocsId
			END
			ELSE IF (@bitPartialFulfillment=1 and DATALENGTH(@strBizDocItems)>2)
			BEGIN
				IF DATALENGTH(@strBizDocItems)>2
				BEGIN
					EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strBizDocItems 

						IF @bitRentalBizDoc=1
						BEGIN
							update OI set OI.monPrice=OBZ.monPrice,OI.monTotAmount=OBZ.monTotAmount,OI.monTotAmtBefDiscount=OBZ.monTotAmount
							from OpportunityItems OI
							Join (SELECT * FROM OPENXML (@hDocItem,'/NewDataSet/BizDocItems',2)                                                                          
								WITH ( OppItemID numeric(9),Quantity FLOAT,monPrice DECIMAL(20,5),monTotAmount DECIMAL(20,5))) OBZ
							on OBZ.OppItemID=OI.numoppitemtCode
							where OBZ.OppItemID=OI.numoppitemtCode
						END
	
						insert into                       
					   OpportunityBizDocItems                                                                          
					   (numOppBizDocID,numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,vcAttributes,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,vcNotes,/*vcTrackingNo,vcShippingMethod,monShipCost,dtDeliveryDate,*/dtRentalStartDate,dtRentalReturnDate)
				   
					   select @numOppBizDocsId,numoppitemtCode,numItemCode,Quantity,CASE WHEN @tintOppType=2 THEN OBZ.monPrice ELSE OI.monPrice END,CASE WHEN @tintOppType=2 THEN OBZ.monPrice * Quantity ELSE (monTotAmount/numUnitHour)*Quantity END,vcItemDesc,numWarehouseItmsID,vcType,vcAttributes,bitDropShip,bitDiscountType,
					   case when bitDiscountType=0 then fltDiscount when bitDiscountType=1 then (fltDiscount/numUnitHour)*Quantity else fltDiscount end ,CASE WHEN @tintOppType=2 THEN OBZ.monPrice * Quantity ELSE (monTotAmtBefDiscount/numUnitHour)*Quantity END,Notes,/*TrackingNo,vcShippingMethod,monShipCost,dtDeliveryDate,*/dtRentalStartDate,dtRentalReturnDate  from OpportunityItems OI
					   Join (SELECT * FROM OPENXML (@hDocItem,'/NewDataSet/BizDocItems',2)                                                                          
					   WITH                       
					   (OppItemID numeric(9),                               
						Quantity FLOAT,
						Notes varchar(500),
						monPrice DECIMAL(20,5),
						dtRentalStartDate datetime,dtRentalReturnDate datetime
						)) OBZ
					   on OBZ.OppItemID=OI.numoppitemtCode
					   where  numOppId=@numOppId AND ISNULL(OI.numUnitHour,0)>0 AND ISNULL(OI.numUnitHour,0)>0
				   
					EXEC sp_xml_removedocument @hDocItem 
				END
			END
			ELSE
			BEGIN
				IF @numBizDocId <> 304 AND @bitAuthBizdoc = 1 AND @bitInvoiceForDeferred = 1
				BEGIN
					DECLARE @numTop1DeferredBizDocID AS NUMERIC(18,0)

					SELECT TOP 1
						@numTop1DeferredBizDocID = 	OpportunityBizDocs.numOppBizDocsId
					FROM 
						OpportunityBizDocs 
					WHERE 
						numOppId=@numOppId 
						AND numOppBizDocsId NOT IN (SELECT ISNULL(numDeferredBizDocID,0) FROM OpportunityBizDocs WHERE numOppId=@numOppId)
						AND OpportunityBizDocs.numBizDocId = 304 -- 304 IS DEFERRED INVOICE BIZDOC
					ORDER BY
						numOppBizDocsId

					INSERT INTO OpportunityBizDocItems
  					(
						numOppBizDocID,
						numOppItemID,
						numItemCode,
						numUnitHour,
						monPrice,
						monTotAmount,
						vcItemDesc,
						numWarehouseItmsID,
						vcType,
						vcAttributes,
						bitDropShip,
						bitDiscountType,
						fltDiscount,
						monTotAmtBefDiscount,
						vcNotes
					)
					SELECT 
						@numOppBizDocsId,
						numoppitemtCode,
						OI.numItemCode,
						(CASE @bitRecurringBizDoc WHEN 1 THEN 1 ELSE ISNULL(TempBizDoc.numUnitHour,0) END) AS numUnitHour,
						OI.monPrice,
						(CASE @bitRecurringBizDoc 
							WHEN 1 THEN monPrice * 1 
							ELSE (OI.monTotAmount/OI.numUnitHour) * ISNULL(TempBizDoc.numUnitHour,0)
						END) AS monTotAmount,
						vcItemDesc,
						numWarehouseItmsID,
						vcType,vcAttributes,
						bitDropShip,
						bitDiscountType,
						(CASE 
							WHEN bitDiscountType=0 THEN fltDiscount 
							WHEN bitDiscountType=1 THEN ((fltDiscount/OI.numUnitHour) * ISNULL(TempBizDoc.numUnitHour,0))
							ELSE fltDiscount 
						END),
						(monTotAmtBefDiscount/OI.numUnitHour) * ISNULL(TempBizDoc.numUnitHour,0),
						OI.vcNotes
					FROM 
						OpportunityItems OI
					JOIN 
						[dbo].[Item] AS I 
					ON 
						I.[numItemCode] = OI.[numItemCode]
					INNER JOIN
						(
							SELECT 
								OpportunityBizDocItems.numOppItemID,
								OpportunityBizDocItems.numUnitHour
							FROM 
								OpportunityBizDocs 
							JOIN 
								OpportunityBizDocItems 
							ON 
								OpportunityBizDocs.numOppBizDocsId  = OpportunityBizDocItems.numOppBizDocId
							WHERE 
								numOppId=@numOppId 
								AND OpportunityBizDocs.numOppBizDocsId = @numTop1DeferredBizDocID
						) TempBizDoc
					ON
						TempBizDoc.numOppItemID = OI.numoppitemtCode
					WHERE  
						numOppId=@numOppId 
						AND ISNULL(OI.numUnitHour,0) > 0
						AND (@bitRecurringBizDoc = 1 OR ISNULL(TempBizDoc.numUnitHour,0) > 0)


					UPDATE OpportunityBizDocs SET numDeferredBizDocID = @numTop1DeferredBizDocID WHERE numOppBizDocsId = @numOppBizDocsId
				END
				ELSE
				BEGIN
		   			INSERT INTO OpportunityBizDocItems                                                                          
  					(
						numOppBizDocID,
						numOppItemID,
						numItemCode,
						numUnitHour,
						monPrice,
						monTotAmount,
						vcItemDesc,
						numWarehouseItmsID,
						vcType,
						vcAttributes,
						bitDropShip,
						bitDiscountType,
						fltDiscount,
						monTotAmtBefDiscount,
						vcNotes
					)
					SELECT 
						@numOppBizDocsId,
						numoppitemtCode,
						OI.numItemCode,
						(CASE @bitRecurringBizDoc 
							WHEN 1 
							THEN 1 
							ELSE (OI.numUnitHour - ISNULL(TempBizDoc.numUnitHour,0)) 
						END) AS numUnitHour,
						OI.monPrice,
						(CASE @bitRecurringBizDoc 
							WHEN 1 THEN monPrice * 1 
							ELSE (OI.monTotAmount/OI.numUnitHour) * (OI.numUnitHour - ISNULL(TempBizDoc.numUnitHour,0))
						END) AS monTotAmount,
						vcItemDesc,
						numWarehouseItmsID,
						vcType,vcAttributes,
						bitDropShip,
						bitDiscountType,
						(CASE 
							WHEN bitDiscountType=0 THEN fltDiscount 
							WHEN bitDiscountType=1 THEN ((fltDiscount/OI.numUnitHour) * (OI.numUnitHour - ISNULL(TempBizDoc.numUnitHour,0)))
							ELSE fltDiscount 
						END),
						(monTotAmtBefDiscount/OI.numUnitHour) * (OI.numUnitHour - ISNULL(TempBizDoc.numUnitHour,0)),
						OI.vcNotes
					FROM 
						OpportunityItems OI
					JOIN 
						[dbo].[Item] AS I 
					ON 
						I.[numItemCode] = OI.[numItemCode]
					OUTER APPLY
						(
							SELECT 
								SUM(OBDI.numUnitHour) AS numUnitHour
							FROM 
								OpportunityBizDocs OBD 
							JOIN 
								dbo.OpportunityBizDocItems OBDI 
							ON 
								OBDI.numOppBizDocId  = OBD.numOppBizDocsId
								AND OBDI.numOppItemID = OI.numoppitemtCode
							WHERE 
								numOppId=@numOppId 
								AND 1 = (CASE 
											WHEN @bitAuthBizdoc = 1 
											THEN (CASE WHEN (numBizDocId = 304 OR (numBizDocId = @numBizDocId AND ISNULL(numDeferredBizDocID,0) = 0)) THEN 1 ELSE 0 END)
											WHEN @numBizDocId = 304 --Deferred Income
											THEN (CASE WHEN (numBizDocId = @numBizDocId OR ISNULL(OBD.bitAuthoritativeBizDocs,0) = 1) THEN 1 ELSE 0 END)
											ELSE (CASE WHEN numBizDocId = @numBizDocId THEN 1 ELSE 0 END) 
										END)
						) TempBizDoc
					WHERE  
						numOppId=@numOppId 
						AND ISNULL(OI.numUnitHour,0) > 0
						AND (@bitRecurringBizDoc = 1 OR (ISNULL(OI.numUnitHour,0) - ISNULL(TempBizDoc.numUnitHour,0)) > 0)
				END
			END

			SET @numOppBizDocsID = @numOppBizDocsId
			--select @numOppBizDocsId
		END
		ELSE
		BEGIN
  			SET @numOppBizDocsId=0
			SET @monDealAmount = 0
			SET @monCreditAmount = 0
			RAISERROR ('NOT_ALLOWED',16,1);
		END
	END
	ELSE
	BEGIN
		UPDATE 
			OpportunityBizDocs
		SET    
			numModifiedBy = @numUserCntID,
			dtModifiedDate = GETUTCDATE(),
			vcComments = @vcComments,
			numShipVia = @numShipVia,
			vcTrackingURL = @vcTrackingURL,
			dtFromDate = @dtFromDate,
			numBizDocStatus = @numBizDocStatus,
			numSequenceId=@numSequenceId,
			numBizDocTempID=@numBizDocTempID,
			vcTrackingNo=@vcTrackingNo,
			vcRefOrderNo=@vcRefOrderNo,
			fltExchangeRateBizDoc=@fltExchangeRateBizDoc,
			bitShippingGenerated=@bitShippingGenerated,
			vcVendorInvoice=@vcVendorInvoiceName
		WHERE  
			numOppBizDocsId = @numOppBizDocsId

		--Added By:Sachin Sadhu||Date:27thAug2014
		--Purpose :MAke entry in WFA queue 
		IF EXISTS (SELECT 1 FROM dbo.OpportunityMaster WHERE numOppId = @numOppID AND tintOppType=1 AND isnull(tintOppStatus,0)=1 )
		BEGIN
			EXEC 
			USP_ManageOpportunityAutomationQueue			
			@numOppQueueID = 0, -- numeric(18, 0)							
			@numDomainID = @numDomainID, -- numeric(18, 0)							
			@numOppId = @numOppID, -- numeric(18, 0)							
			@numOppBizDocsId = @numOppBizDocsId, -- numeric(18, 0)							
			@numOrderStatus = 0, -- numeric(18, 0)					
			@numUserCntID = @numUserCntID, -- numeric(18, 0)					
			@tintProcessStatus = 1, -- tinyint						
			@tintMode = 1 -- TINYINT
		END 
		--end of code

   		IF DATALENGTH(@strBizDocItems)>2
		BEGIN
			EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strBizDocItems 

            DELETE FROM 
				OpportunityBizDocItems  
			WHERE 
				numOppItemID NOT IN 
				(
					SELECT 
						OppItemID 
					FROM OPENXML 
						(@hDocItem,'/NewDataSet/BizDocItems',2)                                                                          
					WITH                       
					   (OppItemID numeric(9))
				)
				AND numOppBizDocID=@numOppBizDocsId  
                  
                     
			IF @bitRentalBizDoc=1
			BEGIN
				update OI set OI.monPrice=OBZ.monPrice,OI.monTotAmount=OBZ.monTotAmount,OI.monTotAmtBefDiscount=OBZ.monTotAmount
				from OpportunityItems OI
				Join (SELECT * FROM OPENXML (@hDocItem,'/NewDataSet/BizDocItems',2)                                                                          
					WITH ( OppItemID numeric(9),Quantity FLOAT,monPrice DECIMAL(20,5),monTotAmount DECIMAL(20,5))) OBZ
				on OBZ.OppItemID=OI.numoppitemtCode
				where OBZ.OppItemID=OI.numoppitemtCode
			END

			update OBI set OBI.numUnitHour= Quantity,OBI.monPrice=CASE WHEN @tintOppType=2 THEN OBZ.monPrice ELSE OI.monPrice END,OBI.monTotAmount=CASE WHEN @tintOppType=2 THEN OBZ.monPrice * Quantity ELSE (OI.monTotAmount/OI.numUnitHour)*Quantity END,OBI.vcNotes= Notes,
			OBI.fltDiscount=case when OI.bitDiscountType=0 then OI.fltDiscount when OI.bitDiscountType=1 then (OI.fltDiscount/OI.numUnitHour)*Quantity else OI.fltDiscount end ,
			OBI.monTotAmtBefDiscount=(OI.monTotAmtBefDiscount/OI.numUnitHour)*Quantity ,                                                                    
			/*OBI.vcTrackingNo=TrackingNo,OBI.vcShippingMethod=OBZ.vcShippingMethod,OBI.monShipCost=OBZ.monShipCost,OBI.dtDeliveryDate=OBZ.dtDeliveryDate*/
			OBI.dtRentalStartDate=OBZ.dtRentalStartDate,OBI.dtRentalReturnDate=OBZ.dtRentalReturnDate
			from OpportunityBizDocItems OBI
			join OpportunityItems OI
			on OBI.numOppItemID=OI.numoppitemtCode
			Join (SELECT * FROM OPENXML (@hDocItem,'/NewDataSet/BizDocItems',2)                                                                          
			WITH                       
			( OppItemID numeric(9),                                     
			Quantity FLOAT,
			Notes varchar(500),
			monPrice DECIMAL(20,5),
			dtRentalStartDate datetime,dtRentalReturnDate datetime
			)) OBZ
			on OBZ.OppItemID=OI.numoppitemtCode
			where  OI.numOppId=@numOppId and OBI.numOppBizDocID=@numOppBizDocsId AND ISNULL(OI.numUnitHour,0)>0


            insert into                       
			OpportunityBizDocItems                                                                          
			(numOppBizDocID,numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,vcAttributes,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,vcNotes/*,vcTrackingNo,vcShippingMethod,monShipCost,dtDeliveryDate*/)                      
			select @numOppBizDocsId,numoppitemtCode,numItemCode,Quantity,CASE WHEN @tintOppType=2 THEN OBZ.monPrice ELSE OI.monPrice END,CASE WHEN @tintOppType=2 THEN OBZ.monPrice * Quantity ELSE (monTotAmount/numUnitHour)*Quantity END,vcItemDesc,numWarehouseItmsID,vcType,vcAttributes,bitDropShip,bitDiscountType,
			case when bitDiscountType=0 then fltDiscount when bitDiscountType=1 then (fltDiscount/numUnitHour)*Quantity else fltDiscount end ,(monTotAmtBefDiscount/numUnitHour)*Quantity,Notes/*,TrackingNo,vcShippingMethod,monShipCost,dtDeliveryDate*/ from OpportunityItems OI
			Join (SELECT * FROM OPENXML (@hDocItem,'/NewDataSet/BizDocItems',2)                                                                          
			WITH                       
			( OppItemID numeric(9),                                     
			Quantity FLOAT,
			Notes varchar(500),
			monPrice DECIMAL(20,5)
			--TrackingNo varchar(500),
			--vcShippingMethod varchar(100),
			--monShipCost DECIMAL(20,5),
			--dtDeliveryDate DATETIME
			)) OBZ
			on OBZ.OppItemID=OI.numoppitemtCode and OI.numoppitemtCode not in (select numOppItemID from OpportunityBizDocItems where numOppBizDocID =@numOppBizDocsId)
			where  numOppId=@numOppId AND ISNULL(OI.numUnitHour,0)>0
                 
			EXEC sp_xml_removedocument @hDocItem 
		END
	END

	IF @numOppBizDocsId>0
	BEGIN
		SELECT @monDealAmount=isnull(dbo.[GetDealAmount](@numOppId,GETUTCDATE(),@numOppBizDocsId),0)

		IF @numBizDocId = 297 OR @numBizDocId = 299
		BEGIN
			IF NOT EXISTS(SELECT * FROM dbo.NameTemplate WHERE numDomainID=@numDomainID AND numRecordID=@numBizDocId AND tintModuleID=2)
			BEGIN
				INSERT INTO NameTemplate
				(numDomainId,tintModuleID,numRecordID,vcNameTemplate,numSequenceId,numMinLength)
				SELECT 
					@numDomainId,2,@numBizDocId,UPPER(SUBSTRING(dbo.GetListIemName(@numBizDocId),0,4)) + '-',1,4

				SET @numSequenceId = 1
			END
			ELSE
			BEGIN
				SELECT @numSequenceId=numSequenceId FROM NameTemplate WHERE numDomainID=@numDomainID AND numRecordID=@numBizDocId AND tintModuleID=2
				UPDATE NameTemplate SET numSequenceId=ISNULL(numSequenceId,0) + 1 WHERE numDomainID=@numDomainID AND numRecordID=@numBizDocId AND tintModuleID=2
			END
		END

		UPDATE 
			OpportunityBizDocs 
		SET 
			vcBizDocID=vcBizDocName + ISNULL(@numSequenceId,''),
			numSequenceId = CASE WHEN (numBizDocId = 297 OR numBizDocId = 299) THEN @numSequenceId ELSE numSequenceId END,
			monDealAmount=@monDealAmount
		WHERE  
			numOppBizDocsId = @numOppBizDocsId
	END

	-- 1 if recurrence is enabled for authorative bizdoc
	IF ISNULL(@bitRecur,0) = 1
	BEGIN
		-- First Check if receurrece is already created for sales order because user should not 
		-- be able to create recurrence on both order and invoice
		IF (SELECT COUNT(*) FROM RecurrenceConfiguration WHERE numOppID = @numOppId AND numType=1) > 0 
		BEGIN
			RAISERROR ('RECURRENCE_ALREADY_CREATED_FOR_ORDER',16,1);
		END
		--User can not create recurrence on bizdoc where all items are added to bizdoc
		ELSE IF (SELECT dbo.CheckAllItemQuantityAddedToBizDoc(@numOppId,@numOppBizDocsID)) = 1
		BEGIN
			RAISERROR ('RECURRENCE_NOT_ALLOWED_ALL_ITEM_QUANTITY_ADDED_TO_INVOICE',16,1);
		END
		ELSE
		BEGIN

			EXEC USP_RecurrenceConfiguration_Insert
				@numDomainID = @numDomainId,
				@numUserCntID = @numUserCntId,
				@numOppID = @numOppID,
				@numOppBizDocID = @numOppBizDocsId,
				@dtStartDate = @dtStartDate,
				@dtEndDate =NULL,
				@numType = 2,
				@vcType = 'Invoice',
				@vcFrequency = @vcFrequency,
				@numFrequency = @numFrequency

			UPDATE OpportunityMaster SET vcRecurrenceType = 'Invoice' WHERE numOppId = @numOppID
		END
	END
	ELSE IF ISNULL(@numRecConfigID,0) > 0 AND ISNULL(@bitDisable,0)= 1
	BEGIN
		UPDATE
			RecurrenceConfiguration
		SET
			bitDisabled = 1,
			numDisabledBy = @numUserCntId,
			dtDisabledDate = GETDATE()
		WHERE	
			numRecConfigID = @numRecConfigID
	END 
 
	IF @numOppBizDocsId > 0 AND @numBizDocId = 29397 AND @tintCommitAllocation=2 AND @bitAllocateInventoryOnPickList=1
	BEGIN
		EXEC USP_PickListManageSOWorkOrder @numDomainID,@numUserCntID,@numOppID,@numOppBizDocsId
		-- Allocate Inventory
		EXEC USP_ManageInventoryPickList @numDomainID,@numOppID,@numOppBizDocsId,@numUserCntID
	END
END TRY
BEGIN CATCH
    DECLARE @ErrorMessage NVARCHAR(4000);
    DECLARE @ErrorSeverity INT;
    DECLARE @ErrorState INT;

    SELECT 
        @ErrorMessage = ERROR_MESSAGE(),
        @ErrorSeverity = ERROR_SEVERITY(),
        @ErrorState = ERROR_STATE();

    -- Use RAISERROR inside the CATCH block to return error
    -- information about the original error that caused
    -- execution to jump to the CATCH block.
    RAISERROR (@ErrorMessage, -- Message text.
               @ErrorSeverity, -- Severity.
               @ErrorState -- State.
               );
END CATCH;
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DeleteAssemblyWOAndInventory')
DROP PROCEDURE USP_DeleteAssemblyWOAndInventory
GO
CREATE PROCEDURE [dbo].[USP_DeleteAssemblyWOAndInventory]
	@numDomainID AS NUMERIC(18,0),
	@numUserCntID AS NUMERIC(18,0),
	@numWOID AS NUMERIC(18,0),
	@numOppID AS NUMERIC(18,0),
	@tintMode TINYINT --1: Order/Item, 2: Pick List
AS
BEGIN
BEGIN TRY 
BEGIN TRANSACTION
	DECLARE @numChildWOID AS NUMERIC(18,0)
	DECLARE @WorkOrder TABLE
	(
		ID INT IDENTITY(1,1),
		numWOID NUMERIC(18,0)
	)

	--GET ALL WORK ORDER CREATED FOR ORDER ASSEMBLY ITEM IN BOTTOM TO TOP ORDER
	;WITH CTE (numLevel,numWOID) AS
	(
		SELECT 
			1,
			numWOId
		FROM
			WorkOrder
		WHERE
			numParentWOID = @numWOID
			AND numWOStatus <> 23184
		UNION ALL
		SELECT 
			c.numLevel + 1,
			WorkOrder.numWOId		
		FROM
			WorkOrder
		INNER JOIN
			CTE c
		ON
			WorkOrder.numParentWOID = c.numWOID AND
			WorkOrder.numWOStatus <> 23184
	)

	INSERT INTO @WorkOrder SELECT numWOID FROM CTE ORDER BY numLevel DESC

	DECLARE @i AS INT = 1
	DECLARE @COUNT AS INT
	SELECT @COUNT=COUNT(*) FROM @WorkOrder

	--DELETE EACH WORK ORDER CREATED RECURSIVELY
	WHILE @i <= @COUNT
	BEGIN
		SELECT @numChildWOID=numWOID FROM @WorkOrder WHERE ID=@i

		EXEC USP_DeleteWorkOrder @numWOId=@numChildWOID,@numUserCntID=@numUserCntID,@numDomainID=@numDomainID,@bitOrderDelete=1,@tintMode=@tintMode

		SET @i = @i + 1 
	END

	--REVERT INVENTORY OF WORK ORDER ITEMS
	DECLARE @TEMP TABLE
	(
		ID INT IDENTITY(1,1),
		numWarehouseItemID NUMERIC(18,0),
		numQtyItemsReq FLOAT
	)

	INSERT INTO @TEMP SELECT numWareHouseItemId,numQtyItemsReq FROM WorkOrderDetails WHERE numWOId=@numWOId AND ISNULL(numWareHouseItemId,0) > 0


	SET @i = 1
	SET @COUNT = 0
	SELECT @COUNT = COUNT(*) FROM @TEMP

	DECLARE @numWareHouseItemID NUMERIC(18,0)
	DECLARE @numQtyItemsReq FLOAT
	DECLARE @Description VARCHAR(2000)
	DECLARE @onHand FLOAT
	DECLARE @onOrder FLOAT
	DECLARE @onAllocation FLOAT
	DECLARE @onBackOrder FLOAT

	WHILE @i <= @COUNT
	BEGIN
		SELECT @numWareHouseItemID=ISNULL(numWarehouseItemID,0),@numQtyItemsReq=ISNULL(numQtyItemsReq,0) FROM @TEMP WHERE ID = @i

		IF @tintMode = 2
		BEGIN
			SET @Description = 'SO-WO Pick List Deleted (Qty:' + CAST(@numQtyItemsReq AS VARCHAR(20)) + ')'
		END
		ELSE
		BEGIN
			SET @Description = 'Items Allocation Reverted SO-WO Deleted (Qty:' + CAST(@numQtyItemsReq AS VARCHAR(20)) + ')'
		END
		--GET CURRENT INEVENTORY STATUS
		SELECT
			@onHand = ISNULL(numOnHand,0),
			@onOrder = ISNULL(numOnOrder,0),
			@onAllocation = ISNULL(numAllocation,0),
			@onBackOrder = ISNULL(numBackOrder,0)
		FROM
			WareHouseItems
		WHERE
			numWareHouseItemID = @numWareHouseItemID

		--CHANGE INVENTORY
		IF @numQtyItemsReq >= @onBackOrder 		BEGIN			SET @numQtyItemsReq = @numQtyItemsReq - @onBackOrder			SET @onBackOrder = 0                            			IF (@onAllocation - @numQtyItemsReq >= 0)					SET @onAllocation = @onAllocation - @numQtyItemsReq									SET @onHand = @onHand + @numQtyItemsReq                                                          		END                                            		ELSE IF @numQtyItemsReq < @onBackOrder 		BEGIN			SET @onBackOrder = @onBackOrder - @numQtyItemsReq		END

		--UPDATE INVENTORY AND WAREHOUSE TRACKING
		EXEC USP_UpdateInventoryAndTracking 
		@numOnHand=@onHand,
		@numOnAllocation=@onAllocation,
		@numOnBackOrder=@onBackOrder,
		@numOnOrder=@onOrder,
		@numWarehouseItemID=@numWareHouseItemID,
		@numReferenceID=@numOppID,
		@tintRefType=3,
		@numDomainID=@numDomainID,
		@numUserCntID=@numUserCntID,
		@Description=@Description 

		SET @i = @i + 1
	END


	DELETE FROM WorkOrderDetails WHERE numWOId=@numWOID 
	DELETE FROM WorkOrder WHERE numWOId=@numWOID AND [numDomainID] = @numDomainID
COMMIT TRANSACTION
END TRY 
BEGIN CATCH		
    IF ( @@TRANCOUNT > 0 ) 
    BEGIN
        ROLLBACK TRANSACTION
    END

	DECLARE @ErrorMessage NVARCHAR(4000);
    DECLARE @ErrorSeverity INT;
    DECLARE @ErrorState INT;

    SELECT @ErrorMessage = ERROR_MESSAGE(), @ErrorSeverity = ERROR_SEVERITY(), @ErrorState = ERROR_STATE();

    RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);
END CATCH	
END
--modified by anoop jayaraj              
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_deleteoppurtunity')
DROP PROCEDURE usp_deleteoppurtunity
GO
CREATE PROCEDURE [dbo].[USP_DeleteOppurtunity]
 @numOppId numeric(9) ,        
 @numDomainID as numeric(9),
 @numUserCntID AS NUMERIC(9)                       
AS                          
             
       
if exists(select * from OpportunityMaster where numOppID=@numOppId and numDomainID=@numDomainID)         
BEGIN 
	DECLARE @tintCommitAllocation AS TINYINT
	DECLARE @bitAllocateInventoryOnPickList AS BIT

	SELECT @tintCommitAllocation=ISNULL(tintCommitAllocation,1) FROM Domain WHERE numDomainId=@numDomainID
	SELECT @bitAllocateInventoryOnPickList=ISNULL(bitAllocateInventoryOnPickList,0) FROM OpportunityMaster INNER JOIN DivisionMaster ON OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID WHERE numOppID=@numOppID

SET XACT_ABORT ON;

BEGIN TRY 
BEGIN TRAN

	IF (SELECT COUNT(*) FROM MassSalesOrderQueue WHERE numOppID = @numOppId AND ISNULL(bitExecuted,0) = 0) > 0
	BEGIN
		RAISERROR ( 'MASS SALES ORDER', 16, 1 )
		RETURN
	END
	IF (SELECT COUNT(*) FROM RecurrenceConfiguration WHERE numOppID = @numOppId AND numDomainID=@numDomainID AND ISNULL(bitMarkForDelete,0) = 0) > 0
	BEGIN
		RAISERROR ( 'RECURRING ORDER OR BIZDOC', 16, 1 )
		RETURN
	END
	ELSE IF (SELECT COUNT(*) FROM RecurrenceTransaction INNER JOIN RecurrenceConfiguration ON RecurrenceTransaction.numRecConfigID = RecurrenceConfiguration.numRecConfigID WHERE RecurrenceTransaction.numRecurrOppID = @numOppId AND ISNULL(RecurrenceConfiguration.bitDisabled,0) = 0) > 0
	BEGIN	
		RAISERROR ( 'RECURRED ORDER', 16, 1 ) ;
		RETURN ;
	END
	ELSE IF (SELECT COUNT(*) FROM OpportunityBizdocs WHERE numOppId=@numOppId AND numBizDocId=296 AND ISNULL(bitFulfilled,0) = 1) > 0
	BEGIN
		RAISERROR ( 'FULFILLED_ITEMS', 16, 1 ) ;
		RETURN ;
	END

      IF EXISTS ( SELECT  *
                FROM    dbo.CaseOpportunities
                WHERE   numOppID = @numOppId
                        AND numDomainID = @numDomainID )         
        BEGIN
            RAISERROR ( 'CASE DEPENDANT', 16, 1 ) ;
            RETURN ;
        END	
	 IF EXISTS ( SELECT  *
			FROM    dbo.ReturnHeader
			WHERE   numOppId = @numOppId
					AND numDomainId= @numDomainID )         
	BEGIN
		RAISERROR ( 'RETURN_EXIST', 16, 1 ) ;
		RETURN ;
	END	

      
      
      

DECLARE @tintError TINYINT;SET @tintError=0

EXECUTE USP_CheckCanbeDeleteOppertunity @numOppId,@tintError OUTPUT
  
  IF @tintError=1
  BEGIN
  	 RAISERROR ( 'OppItems DEPENDANT', 16, 1 ) ;
     RETURN ;
  END
  
  EXEC dbo.USP_ValidateFinancialYearClosingDate
	@numDomainID = @numDomainID, --  numeric(18, 0)
	@tintRecordType = 2, --  tinyint
	@numRecordID = @numOppId --  numeric(18, 0)
	
  --Credit Balance
   Declare @monCreditAmount as DECIMAL(20,5);Set @monCreditAmount=0
   Declare @monCreditBalance as DECIMAL(20,5);Set @monCreditBalance=0
   Declare @numDivisionID as numeric(9);Set @numDivisionID=0
   Declare @tintOppType as tinyint

   Select @numDivisionID=numDivisionID,@tintOppType=tintOppType from OpportunityMaster WHERE  numOppId=@numOppId  and numDomainID= @numDomainID 
   
    IF @tintOppType=1 --Sales
      Select @monCreditBalance=isnull(monSCreditBalance,0) from DivisionMaster WHERE  numDivisionID=@numDivisionID  and numDomainID= @numDomainID 
    else IF @tintOppType=2 --Purchase
		UPDATE OpportunityMaster SET numReleaseStatus = 1 WHERE numDomainId=@numDomainID AND numOppId IN (SELECT DISTINCT numOppId FROM OpportunityItemsReleaseDates WHERE  numPurchasedOrderID = @numOppId)
		UPDATE OpportunityItemsReleaseDates SET tintStatus = 1, numPurchasedOrderID=0 WHERE numPurchasedOrderID = @numOppId


      Select @monCreditBalance=isnull(monPCreditBalance,0) from DivisionMaster WHERE  numDivisionID=@numDivisionID  and numDomainID= @numDomainID 
   
   Select @monCreditAmount=sum(monAmount) from CreditBalanceHistory where numReturnID in (select numReturnID from [Returns] WHERE [numOppId] = @numOppId)
	
	IF ( @monCreditAmount > @monCreditBalance )         
        BEGIN
            RAISERROR ( 'CreditBalance DEPENDANT', 16, 1 ) ;
            RETURN ;
        END	
        
  declare @tintOppStatus as tinyint    
 declare @tintShipped as tinyint                
  select @tintOppStatus=tintOppStatus,@tintShipped=tintShipped from OpportunityMaster where numOppID=@numOppId                
  
  DECLARE @isOrderItemsAvailable AS INT = 0

	SELECT 
		@isOrderItemsAvailable = COUNT(*) 
	FROM    
		OpportunityItems OI
	JOIN 
		dbo.OpportunityMaster OM ON OI.numOppId = OM.numOppId
	JOIN 
		Item I ON OI.numItemCode = I.numItemCode
	WHERE   
		(charitemtype='P' OR 1=(CASE WHEN tintOppType=1 THEN 
								CASE WHEN ISNULL(I.bitKitParent,0) = 1 AND ISNULL(I.bitAssembly,0) = 1 THEN 0
									 WHEN ISNULL(I.bitKitParent,0) = 1 THEN 1
									 ELSE 0 END 
								ELSE 0 END)) AND OI.numOppId = @numOppId
							   AND ( bitDropShip = 0
									 OR bitDropShip IS NULL
								   ) 

  if ((@tintOppStatus=1 and @tintShipped=0) OR  @tintShipped=1) AND @tintCommitAllocation <> 2            
  begin    
    exec USP_RevertDetailsOpp @numOppId,1,@numUserCntID                
  end                                 
                        
  DELETE FROM [OpportunitySalesTemplate] WHERE [numOppId] =@numOppId 
  delete OpportunityLinking where   numChildOppID=  @numOppId or   numParentOppID=    @numOppId          
  delete RECENTITEMS where numRecordID =  @numOppId and chrRecordType='O'             
                

	IF @tintOppType = 1 AND @tintOppStatus = 1 -- SALES ORDER
	BEGIN
		-- MAKE SERIAL/LOT# NUUMBER AVAILABLE FOR USE AS SALES ORDER IS DELETED
		UPDATE WHIDL
			SET WHIDL.numQty = (CASE WHEN Item.bitLotNo = 1 THEN ISNULL(WHIDL.numQty,0) + ISNULL(OWSI.numQty,0) ELSE 1 END)
		FROM 
			WareHouseItmsDTL WHIDL
		INNER JOIN
			OppWarehouseSerializedItem OWSI
		ON
			WHIDL.numWareHouseItmsDTLID = OWSI.numWarehouseItmsDTLID
			AND WHIDL.numWareHouseItemID = OWSI.numWarehouseItmsID
		INNER JOIN
			WareHouseItems
		ON
			WHIDL.numWareHouseItemID = WareHouseItems.numWareHouseItemID
		INNER JOIN
			Item
		ON
			WareHouseItems.numItemID = Item.numItemCode
		WHERE
			OWSI.numOppID = @numOppId
	END
	ELSE IF @tintOppType = 2 AND @tintOppStatus = 1 -- PURCHASE ORDER
	BEGIN
		IF (
			SELECT 
				COUNT(*)
			FROM 
				OppWarehouseSerializedItem OWSI 
			INNER JOIN 
				OpportunityMaster OM 
			ON 
				OWSI.numOppID=OM.numOppId 
				AND tintOppType=1 
			WHERE 
				OWSI.numWarehouseItmsDTLID IN (SELECT numWarehouseItmsDTLID FROM OppWarehouseSerializedItem OWSIInner WHERE OWSIInner.numOppID = @numOppID)
			) > 0
		BEGIN
			RAISERROR ('SERIAL/LOT#_USED', 16, 1 ) ;
		END
		ELSE
		BEGIN
			-- REMOVE SERIAL/LOT# NUMBER FROM INVENTORY BECAUSE PURCHASE ORDER IS DELETED
			UPDATE WHIDL
				SET WHIDL.numQty = (CASE WHEN Item.bitLotNo = 1 THEN ISNULL(WHIDL.numQty,0) - ISNULL(OWSI.numQty,0) ELSE 0 END)
			FROM 
				WareHouseItmsDTL WHIDL
			INNER JOIN
				OppWarehouseSerializedItem OWSI
			ON
				WHIDL.numWareHouseItmsDTLID = OWSI.numWarehouseItmsDTLID
				AND WHIDL.numWareHouseItemID = OWSI.numWarehouseItmsID
			INNER JOIN
				WareHouseItems
			ON
				WHIDL.numWareHouseItemID = WareHouseItems.numWareHouseItemID
			INNER JOIN
				Item
			ON
				WareHouseItems.numItemID = Item.numItemCode
			WHERE
				OWSI.numOppID = @numOppId
		END
	END
									        
  delete from OppWarehouseSerializedItem where numOppID=  @numOppId                  
                  
                        
  delete from OpportunityItemLinking where numNewOppID=@numOppId                      
                         
  delete from OpportunityAddress  where numOppID=@numOppId


  DELETE FROM OpportunityBizDocKitItems WHERE [numOppBizDocItemID] IN 
  (SELECT [numOppBizDocItemID] FROM [OpportunityBizDocItems] WHERE [numOppBizDocID] IN 
	(select numOppBizDocsId from OpportunityBizDocs where numOppId=@numOppId))
	
  delete from OpportunityBizDocItems where numOppBizDocID in                        
  (select numOppBizDocsId from OpportunityBizDocs where numOppId=@numOppId)

  delete from OpportunityItemsTaxItems WHERE numOppID=@numOppID
  delete from OpportunityMasterTaxItems WHERE numOppID=@numOppID
  
  delete from OpportunityBizDocDtl where numBizDocID in                        
  (select numOppBizDocsId from OpportunityBizDocs where numOppId=@numOppId)                        
           
   DELETE FROM OpportunityBizDocsPaymentDetails WHERE [numBizDocsPaymentDetId] IN 
	 (SELECT [numBizDocsPaymentDetId] FROM [OpportunityBizDocsDetails] where numBizDocsId in                        
		(select numOppBizDocsId from OpportunityBizDocs where numOppId=@numOppId)) 
               
  delete from OpportunityBizDocsDetails where numBizDocsId in                        
  (select numOppBizDocsId from OpportunityBizDocs where numOppId=@numOppId)  

DELETE FROM [ShippingReportItems] WHERE [numShippingReportId] IN  (SELECT numShippingReportId from [ShippingReport] WHERE [numOppBizDocId] IN (select numOppBizDocsId from OpportunityBizDocs where numOppId=@numOppId))
DELETE FROM [ShippingBox] WHERE [numShippingReportId] IN  (SELECT numShippingReportId from [ShippingReport] WHERE [numOppBizDocId] IN (select numOppBizDocsId from OpportunityBizDocs where numOppId=@numOppId))
DELETE FROM [ShippingReport] WHERE [numOppBizDocId] IN (select numOppBizDocsId from OpportunityBizDocs where numOppId=@numOppId)

delete from DocumentWorkflow where numDocID in 
(select numOppBizDocsId from OpportunityBizDocs where numOppId=@numOppId) and cDocType='B'      

delete from BizDocAction where numBizActionId in(select numBizActionId from BizActionDetails where numOppBizDocsId in
(select numOppBizDocsId from OpportunityBizDocs where numOppId=@numOppId) and btDocType=1)

delete from BizActionDetails where numOppBizDocsId in
(select numOppBizDocsId from OpportunityBizDocs where numOppId=@numOppId) and btDocType=1

  delete from OpportunityBizDocs where numOppId=@numOppId                        
                          
  delete from OpportunityContact where numOppId=@numOppId                        
                          
  delete from OpportunityDependency where numOpportunityId=@numOppId                        
                          
  delete from TimeAndExpense where numOppId=@numOppId   and numDomainID= @numDomainID                     
  
  --Credit Balance
  delete from CreditBalanceHistory where numReturnID in (select numReturnID from [Returns] WHERE [numOppId] = @numOppId)

    IF @tintOppType=1 --Sales
		update DivisionMaster set monSCreditBalance=isnull(monSCreditBalance,0) - isnull(@monCreditAmount,0) where numDivisionID=@numDivisionID and numDomainID=@numDomainID
    else IF @tintOppType=2 --Purchase
		update DivisionMaster set monPCreditBalance=isnull(monPCreditBalance,0) - isnull(@monCreditAmount,0) where numDivisionID=@numDivisionID and numDomainID=@numDomainID
  
	UPDATE
		PO
	SET 
		intCouponCodeUsed = ISNULL(intCouponCodeUsed,0) - (CASE WHEN ISNULL(T1.intUsed,0) > 0 THEN 1 ELSE 0 END)
	FROM
		PromotionOffer PO
	INNER JOIN
		(
			SELECT
				numPromotionID,
				COUNT(*) AS intUsed
			FROM
				OpportunityMaster
			INNER JOIN	
				OpportunityItems
			ON
				OpportunityMaster.numOppId=OpportunityItems.numOppId
			WHERE
				OpportunityMaster.numOppId=@numOppId
			GROUP BY
				numPromotionID
		) AS T1
	ON
		PO.numProId = T1.numPromotionID
	WHERE
		numDomainId=@numDomainId
		AND bitRequireCouponCode = 1

  DELETE FROM [Returns] WHERE [numOppId] = @numOppId
                         
  delete from OpportunityKitItems where numOppId=@numOppId                        
                          
  delete from OpportunityItems where numOppId=@numOppId                        
                          
  DELETE FROM dbo.ProjectProgress WHERE numOppId =@numOppId AND numDomainId=@numDomainID
                          
  delete from OpportunityStageDetails where numOppId=@numOppId
                          
  delete from OpportunitySubStageDetails where numOppId=@numOppId
                                               
  DELETE FROM [OpportunityRecurring] WHERE [numOppId] = @numOppId
  
  DELETE FROM [OpportunityMasterAPI] WHERE [numOppId] = @numOppId
  
  DELETE FROM [WebApiOrderDetails] WHERE [numOppId] = @numOppId
  
  DELETE FROM [WebApiOppItemDetails] WHERE [numOppId] = @numOppId
  
  /*
  added by chintan: reason: when bizdoc is deleted and journal entries are not deleted in exceptional case, 
  it throws Foreign key reference error when deleting order.
  */
  DELETE FROM dbo.General_Journal_Details WHERE numJournalId IN (SELECT numJournal_Id FROM dbo.General_Journal_Header WHERE numOppId=@numOppId AND numDomainId=@numDomainID)
  DELETE FROM dbo.General_Journal_Header WHERE numOppId=@numOppId AND numDomainId= @numDomainID
 
	IF @tintCommitAllocation = 2
	BEGIN
		DELETE FROM OpportunityMaster WHERE numOppId=@numOppId AND numDomainID= @numDomainID
	END
	ELSE
	BEGIN
		IF ISNULL(@tintOppStatus,0)=0 OR ISNULL(@isOrderItemsAvailable,0) = 0 OR (SELECT COUNT(*) FROM WareHouseItems_Tracking WHERE numDomainID=@numDomainID AND numReferenceID=@numOppId AND tintRefType IN (3,4) AND vcDescription LIKE '%Deleted%' AND CAST(dtCreatedDate AS DATE) = CAST(GETUTCDATE() AS DATE)) > 0
			DELETE FROM OpportunityMaster WHERE numOppId=@numOppId and numDomainID= @numDomainID             
		ELSE
			RAISERROR ('INVENTORY IM-BALANCE', 16, 1);
	END
COMMIT TRAN 

    END TRY 
    BEGIN CATCH		
        DECLARE @strMsg VARCHAR(200)
        SET @strMsg = ERROR_MESSAGE()
        IF ( @@TRANCOUNT > 0 ) 
            BEGIN
             RAISERROR ( @strMsg, 16, 1 ) ;
                ROLLBACK TRAN
                RETURN 1
            END
    END CATCH	

SET XACT_ABORT OFF;
END 

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DeleteWorkOrder')
DROP PROCEDURE USP_DeleteWorkOrder
GO
CREATE PROCEDURE [dbo].[USP_DeleteWorkOrder]
@numWOId AS NUMERIC(18,0),
@numUserCntID AS NUMERIC(18,0),
@numDomainID AS NUMERIC(18,0),
@bitOrderDelete AS BIT,
@tintMode TINYINT --1: Order/Item, 2: Pick List
AS
BEGIN
BEGIN TRY
BEGIN TRANSACTION

	DECLARE @numWareHouseItemID AS NUMERIC(18,0),@numQtyItemsReq AS FLOAT,@numOppId AS NUMERIC(18,0),@numItemCode AS NUMERIC(18,0),@numWOStatus AS NUMERIC(18,0)
	DECLARE @onHand FLOAT
	DECLARE @onAllocation FLOAT
	DECLARE @onOrder FLOAT
	DECLARE @onBackOrder FLOAT
	DECLARE @Description VARCHAR(1000)

	DECLARE @numReferenceID AS NUMERIC(18,0)
	DECLARE @tintRefType TINYINT

	SELECT @numOppId=ISNULL(numOppId,0) FROM WorkOrder WHERE numWOId=@numWOId AND [WorkOrder].[numDomainID] = @numDomainID

	IF ISNULL(@numOppId,0) > 0
	BEGIN
		SET @numReferenceID = @numOppId
		SET @tintRefType = 3
	END
	ELSE
	BEGIN
		SET @numReferenceID = @numWOId
		SET @tintRefType = 2
	END

	--IF WORK ORDER IS CREATED FROM SALES ORDER AND WORK ORDER IS DELETED NOT WHOLE ORDER THEN EXECUTE FOLLOWING IF CODE
	IF @numOppId>0 AND ISNULL(@bitOrderDelete,0) = 0
	BEGIN
			UPDATE opp SET bitWorkOrder=0 FROM OpportunityItems opp JOIN WorkOrder wo 
			ON opp.numItemCode=wo.numItemCode AND ISNULL(wo.numOppId,0)=opp.numOppId WHERE wo.numWOId=@numWOId
			AND WO.[numDomainID] = @numDomainID
	END  


	SELECT 
		@numWareHouseItemID=numWareHouseItemID,
		@numQtyItemsReq=numQtyItemsReq,
		@numOppId=ISNULL(numOppId,0),
		@numItemCode=numItemCode,
		@numWOStatus=numWOStatus
	FROM 
		WorkOrder 
	WHERE 
		numWOId=@numWOId
		AND [WorkOrder].[numDomainID] = @numDomainID

	--IF NOT COMPLETED THEN DELETE
	IF @numWOStatus <> 23184
	BEGIN
		IF @numOppId>0
		BEGIN
			IF @tintMode = 2
			BEGIN
				SET @Description = 'SO-WO Pick List Deleted (Qty:' + CAST(@numQtyItemsReq AS VARCHAR(10)) + ')'
			END
			ELSE
			BEGIN
				SET @Description = 'SO-WO Work Order Deleted (Qty:' + CAST(@numQtyItemsReq AS VARCHAR(10)) + ')'
			END
		END
		ELSE
		BEGIN
			SET @Description = 'Work Order Deleted (Qty:' + CAST(@numQtyItemsReq AS VARCHAR(10)) + ')'
		END

		--GET CURRENT INEVENTORY STATUS
		SELECT
			@onHand = ISNULL(numOnHand,0),
			@onOrder = ISNULL(numOnOrder,0),
			@onAllocation = ISNULL(numAllocation,0),
			@onBackOrder = ISNULL(numBackOrder,0)
		FROM
			WareHouseItems
		WHERE
			numWareHouseItemID = @numWareHouseItemID

		--CHANGE INVENTORY
		IF @onOrder < @numQtyItemsReq
			SET @onOrder = 0
		ELSE
			SET @onOrder = @onOrder - @numQtyItemsReq

		EXEC USP_UpdateInventoryAndTracking 
		@numOnHand=@onHand,
		@numOnAllocation=@onAllocation,
		@numOnBackOrder=@onBackOrder,
		@numOnOrder=@onOrder,
		@numWarehouseItemID=@numWareHouseItemID,
		@numReferenceID=@numReferenceID,
		@tintRefType=@tintRefType,
		@numDomainID=@numDomainID,
		@numUserCntID=@numUserCntID,
		@Description=@Description 


		DECLARE @TEMP TABLE
		(
			ID INT IDENTITY(1,1),
			numWarehouseItemID NUMERIC(18,0),
			numQtyItemsReq FLOAT
		)

		INSERT INTO @TEMP SELECT numWareHouseItemId,numQtyItemsReq FROM WorkOrderDetails WHERE numWOId=@numWOId AND ISNULL(numWareHouseItemId,0) > 0

		DECLARE @i INT = 1
		DECLARE @COUNT INT

		SELECT @COUNT = COUNT(*) FROM @TEMP

		WHILE @i <= @COUNT
		BEGIN
			SELECT @numWareHouseItemID=ISNULL(numWarehouseItemID,0),@numQtyItemsReq=ISNULL(numQtyItemsReq,0) FROM @TEMP WHERE ID = @i

			IF @numOppId>0
			BEGIN
				IF @tintMode = 2
				BEGIN
					SET @Description = 'Items Allocation Reverted O-WO Pick List Deleted (Qty:' + CAST(@numQtyItemsReq AS VARCHAR(10)) + ')'
				END
				ELSE
				BEGIN
					SET @Description = 'Items Allocation Reverted SO-WO Work Order Deleted (Qty:' + CAST(@numQtyItemsReq AS VARCHAR(10)) + ')'
				END
			END
			ELSE
			BEGIN
				SET @Description = 'Items Allocation Reverted Work Order Deleted (Qty:' + CAST(@numQtyItemsReq AS VARCHAR(10)) + ')'
			END

			--GET CURRENT INEVENTORY STATUS
			SELECT
				@onHand = ISNULL(numOnHand,0),
				@onOrder = ISNULL(numOnOrder,0),
				@onAllocation = ISNULL(numAllocation,0),
				@onBackOrder = ISNULL(numBackOrder,0)
			FROM
				WareHouseItems
			WHERE
				numWareHouseItemID = @numWareHouseItemID

			--CHANGE INVENTORY
			IF @numQtyItemsReq >= @onBackOrder 			BEGIN				SET @numQtyItemsReq = @numQtyItemsReq - @onBackOrder				SET @onBackOrder = 0                            				IF (@onAllocation - @numQtyItemsReq >= 0)						SET @onAllocation = @onAllocation - @numQtyItemsReq										SET @onHand = @onHand + @numQtyItemsReq                                                          			END                                            			ELSE IF @numQtyItemsReq < @onBackOrder 			BEGIN				SET @onBackOrder = @onBackOrder - @numQtyItemsReq			END

			--UPDATE INVENTORY AND WAREHOUSE TRACKING
			EXEC USP_UpdateInventoryAndTracking 
			@numOnHand=@onHand,
			@numOnAllocation=@onAllocation,
			@numOnBackOrder=@onBackOrder,
			@numOnOrder=@onOrder,
			@numWarehouseItemID=@numWareHouseItemID,
			@numReferenceID=@numReferenceID,
			@tintRefType=@tintRefType,
			@numDomainID=@numDomainID,
			@numUserCntID=@numUserCntID,
			@Description=@Description 

			SET @i = @i + 1
		END

		DELETE FROM WorkOrderDetails WHERE numWOId=@numWOId 

		DELETE FROM WorkOrder WHERE numWOId=@numWOId AND [numDomainID] = @numDomainID
	END
COMMIT TRANSACTION
END TRY
BEGIN CATCH
  -- Whoops, there was an error
  IF @@TRANCOUNT > 0
     ROLLBACK TRANSACTION

  -- Raise an error with the details of the exception
  DECLARE @ErrMsg nvarchar(4000), @ErrSeverity int
  SELECT @ErrMsg = ERROR_MESSAGE(),
         @ErrSeverity = ERROR_SEVERITY()

  RAISERROR(@ErrMsg, @ErrSeverity, 1)
END CATCH
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DepositMaster_UpdateTransactionHistoryID')
DROP PROCEDURE dbo.USP_DepositMaster_UpdateTransactionHistoryID
GO
CREATE PROCEDURE [dbo].[USP_DepositMaster_UpdateTransactionHistoryID]
(
	@numDepositId NUMERIC(18,0),
    @numTransHistoryID NUMERIC(18,0)
)
AS
BEGIN
	UPDATE DepositMaster SET numTransHistoryID=@numTransHistoryID WHERE numDepositId=@numDepositId
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DivisionMaster_GetDefaultSettingByFieldName')
DROP PROCEDURE USP_DivisionMaster_GetDefaultSettingByFieldName
GO
CREATE PROCEDURE [dbo].[USP_DivisionMaster_GetDefaultSettingByFieldName]
(
    @numDivisionID AS NUMERIC(18,0)
	,@vcDbColumnName VARCHAR(300)
)
AS 
BEGIN
	IF @vcDbColumnName = 'bitAllocateInventoryOnPickList'
	BEGIN
		SELECT ISNULL(bitAllocateInventoryOnPickList,0) AS bitAllocateInventoryOnPickList FROM DivisionMaster WHERE numDivisionID=@numDivisionID
	END
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Domain_GetColumnValue')
DROP PROCEDURE USP_Domain_GetColumnValue
GO
CREATE PROCEDURE [dbo].[USP_Domain_GetColumnValue]
(
    @numDomainID AS NUMERIC(18,0),
	@vcDbColumnName AS VARCHAR(300)
)
AS 
BEGIN
	IF @vcDbColumnName = 'tintCommitAllocation'
	BEGIN
		SELECT ISNULL(tintCommitAllocation,1) AS tintCommitAllocation FROM Domain WHERE numDomainId=@numDomainID
	END
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DomainSFTPDetail_Generate')
DROP PROCEDURE dbo.USP_DomainSFTPDetail_Generate
GO
CREATE PROCEDURE [dbo].[USP_DomainSFTPDetail_Generate]
	@numDomainID NUMERIC(18,0)
AS 
BEGIN
	IF NOT EXISTS (SELECT ID FROM DomainSFTPDetail WHERE numDomainID=@numDomainID)
	BEGIN
		BEGIN TRY
		BEGIN TRANSACTION
			DECLARE @TP1Username VARCHAr(20)
			DECLARE @TP1Password VARCHAr(20)

			;WITH CTETP1Username(n) AS (SELECT 1 UNION ALL SELECT n+1 FROM CTETP1Username WHERE n<68)
			SELECT 
				@TP1Username = LEFT((SELECT 
										'' + SUBSTRING('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz+-!@#$',n,1)
									FROM 
										CTETP1Username 
									ORDER BY 
										NEWID() FOR XML PATH('')),10)

			;WITH CTETP1Password(n) AS (SELECT 1 UNION ALL SELECT n+1 FROM CTETP1Password WHERE n<68)
			SELECT 
				@TP1Password = LEFT((SELECT 
										'' + SUBSTRING('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz+-!@#$',n,1)
									FROM 
										CTETP1Password 
									ORDER BY 
										NEWID() FOR XML PATH('')),10)

			
			INSERT INTO DomainSFTPDetail (numDomainID,vcUsername,vcPassword,tintType) VALUES (@numDomainID,@TP1Username,@TP1Password,1) --3PL
		COMMIT
		END TRY
		BEGIN CATCH
			DECLARE @ErrorMessage NVARCHAR(4000)
			DECLARE @ErrorNumber INT
			DECLARE @ErrorSeverity INT
			DECLARE @ErrorState INT
			DECLARE @ErrorLine INT
			DECLARE @ErrorProcedure NVARCHAR(200)

			IF @@TRANCOUNT > 0
				ROLLBACK TRANSACTION;

			SELECT 
				@ErrorMessage = ERROR_MESSAGE(),
				@ErrorNumber = ERROR_NUMBER(),
				@ErrorSeverity = ERROR_SEVERITY(),
				@ErrorState = ERROR_STATE(),
				@ErrorLine = ERROR_LINE(),
				@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

			RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);

		END CATCH
	END
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_EDIQueueLog_Insert')
DROP PROCEDURE dbo.USP_EDIQueueLog_Insert
GO
CREATE PROCEDURE [dbo].[USP_EDIQueueLog_Insert]
(
	@numEDIQueueID NUMERIC(18,0),
	@EDIType INT,
	@numDomainID NUMERIC(18,0),
    @numOppID NUMERIC(18,0),
	@vcLog NTEXT,
	@bitSuccess BIT,
	@vcExceptionMessage NTEXT
)
AS 
BEGIN
	INSERT INTO EDIQueueLog
	(
		numEDIQueueID
		,numDomainID
		,numOppID
		,vcLog
		,bitSuccess
		,vcExceptionMessage
		,dtDate
	)
	VALUES
	(
		@numEDIQueueID
		,@numDomainID
		,@numOppID
		,@vcLog
		,@bitSuccess
		,@vcExceptionMessage
		,GETUTCDATE()
	)

	IF ISNULL(@numEDIQueueID,0) > 0
	BEGIN
		UPDATE 
			EDIQueue
		SET
			bitExecuted=1
			,bitSuccess=@bitSuccess
			,dtExecutionDate=GETUTCDATE()
		WHERE
			ID=@numEDIQueueID
	END

	DECLARE @tintOppType AS TINYINT
	DECLARE @tintOppStatus AS TINYINT
	IF @EDIType = 940 
	BEGIN
		IF ISNULL(@bitSuccess,0) = 0
		BEGIN
			SELECT @tintOppStatus=tintOppStatus,@tintOppType=tintOppType from OpportunityMaster where numOppID=@numOppID
			UPDATE OpportunityMaster SET numStatus=15446,bintModifiedDate=GETUTCDATE(),tintEDIStatus=8 WHERE numOppId=@numOppID --15446: Shipment Request (940) Failed

			IF @tintOppType=1 AND @tintOppStatus=1
			BEGIN
				EXEC USP_SalesFulfillmentQueue_Save @numDomainID,0,@numOppID,15446
			END

		 	 IF EXISTS (SELECT 1 FROM dbo.OpportunityMaster WHERE numOppId = @numOppID AND tintOppType=1 AND isnull(tintOppStatus,0)=1 AND ISNULL(numStatus,0)!= 15446)
			 BEGIN
	   				EXEC USP_ManageOpportunityAutomationQueue
							@numOppQueueID = 0, -- numeric(18, 0)
							@numDomainID = @numDomainID, -- numeric(18, 0)
							@numOppId = @numOppID, -- numeric(18, 0)
							@numOppBizDocsId = 0, -- numeric(18, 0)
							@numOrderStatus = 15446, -- numeric(18, 0)
							@numUserCntID = 0, -- numeric(18, 0)
							@tintProcessStatus = 1, -- tinyint
							@tintMode = 1 -- TINYINT
			END 
		END
		ELSE
		BEGIN
			IF NOT EXISTS (SELECT ID FROM EDIHistory WHERE numOppID=@numOppID AND tintEDIStatus=3)
			BEGIN
				UPDATE OpportunityMaster SET tintEDIStatus=3,bintModifiedDate=GETUTCDATE() WHERE numOppId=@numOppID --3: 940 Sent
			
				INSERT INTO EDIHistory
				(
					numDomainID,numOppID,tintEDIStatus,dtDate
				)
				VALUES
				(
					@numDomainID,@numOppID,3,GETUTCDATE()
				)
			END
			
		END
	END
	ELSE IF @EDIType = 940997 -- 940 Acknowledge
	BEGIN
		IF NOT EXISTS (SELECT ID FROM EDIHistory WHERE numOppID=@numOppID AND tintEDIStatus=4)
		BEGIN
			UPDATE OpportunityMaster SET tintEDIStatus=4,bintModifiedDate=GETUTCDATE() WHERE numOppId=@numOppID --3: 940 Acknowledged
			
			INSERT INTO EDIHistory
			(
				numDomainID,numOppID,tintEDIStatus,dtDate
			)
			VALUES
			(
				@numDomainID,@numOppID,4,GETUTCDATE()
			)
		END
	END
	ELSE IF @EDIType = 8565 AND ISNULL(@bitSuccess,0) = 1 -- 856 Received from 3PL
	BEGIN
		IF NOT EXISTS (SELECT ID FROM EDIHistory WHERE numOppID=@numOppID AND tintEDIStatus=5)
		BEGIN
			UPDATE OpportunityMaster SET tintEDIStatus=5,bintModifiedDate=GETUTCDATE() WHERE numOppId=@numOppID --5: 856 Received
			
			INSERT INTO EDIHistory
			(
				numDomainID,numOppID,tintEDIStatus,dtDate
			)
			VALUES
			(
				@numDomainID,@numOppID,5,GETUTCDATE()
			)
		END
	END
	ELSE IF @EDIType = 856997 AND ISNULL(@bitSuccess,0) = 1 -- 856 Acknowledged
	BEGIN
		IF NOT EXISTS (SELECT ID FROM EDIHistory WHERE numOppID=@numOppID AND tintEDIStatus=6)
		BEGIN
			UPDATE OpportunityMaster SET tintEDIStatus=6,bintModifiedDate=GETUTCDATE() WHERE numOppId=@numOppID --6: 856 Acknowledged
			
			INSERT INTO EDIHistory
			(
				numDomainID,numOppID,tintEDIStatus,dtDate
			)
			VALUES
			(
				@numDomainID,@numOppID,6,GETUTCDATE()
			)
		END
	END
	ELSE IF @EDIType = 8566 -- 856 Sent to EDI
	BEGIN
		IF ISNULL(@bitSuccess,0) = 0
		BEGIN
			SELECT @tintOppStatus=tintOppStatus,@tintOppType=tintOppType from OpportunityMaster where numOppID=@numOppID
			UPDATE OpportunityMaster SET numStatus=15448,bintModifiedDate=GETUTCDATE(),tintEDIStatus=9 WHERE numOppId=@numOppID --15448: Send 856 & 810 Failed

			IF @tintOppType=1 AND @tintOppStatus=1
			BEGIN
				EXEC USP_SalesFulfillmentQueue_Save @numDomainID,0,@numOppID,15448
			END

		 	 IF EXISTS (SELECT 1 FROM dbo.OpportunityMaster WHERE numOppId = @numOppID AND tintOppType=1 AND isnull(tintOppStatus,0)=1 AND ISNULL(numStatus,0) != 15448)
			 BEGIN
	   				EXEC USP_ManageOpportunityAutomationQueue
							@numOppQueueID = 0, -- numeric(18, 0)
							@numDomainID = @numDomainID, -- numeric(18, 0)
							@numOppId = @numOppID, -- numeric(18, 0)
							@numOppBizDocsId = 0, -- numeric(18, 0)
							@numOrderStatus = 15448, -- numeric(18, 0)
							@numUserCntID = 0, -- numeric(18, 0)
							@tintProcessStatus = 1, -- tinyint
							@tintMode = 1 -- TINYINT
			END 
		END
		ELSE
		BEGIN
			IF NOT EXISTS (SELECT ID FROM EDIHistory WHERE numOppID=@numOppID AND tintEDIStatus=7)
			BEGIN
				UPDATE OpportunityMaster SET tintEDIStatus=7,bintModifiedDate=GETUTCDATE() WHERE numOppId=@numOppID --6: 856 Sent
			
				INSERT INTO EDIHistory
				(
					numDomainID,numOppID,tintEDIStatus,dtDate
				)
				VALUES
				(
					@numDomainID,@numOppID,7,GETUTCDATE()
				)
			END
		END
	END
	
END
GO
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ElasticSearch_GetActionItems')
DROP PROCEDURE dbo.USP_ElasticSearch_GetActionItems
GO
CREATE PROCEDURE [dbo].[USP_ElasticSearch_GetActionItems]
	@numDomainID NUMERIC(18,0)
	,@vcActionItemIds VARCHAR(MAX)
AS
BEGIN
	SELECT 
		numCommId AS id
		,'actionitem' AS module
		,ISNULL(Communication.numCreatedBy,0) numRecOwner
		,ISNULL(Communication.numAssign,0) numAssignedTo
		,ISNULL(DivisionMaster.numTerID,0) numTerID
		,CONCAT('<b style="color:#9900cc">Action Item', CASE WHEN LEN(ListDetails.vcData) > 0 THEN CONCAT(' (',ListDetails.vcData,')') ELSE '' END ,':</b> ',vcCompanyName,', ', ISNULL(Communication.textDetails,'')) AS displaytext
		,ISNULL(Communication.textDetails,'') AS [text]
		,CONCAT('/admin/actionitemdetails.aspx?CommId=',numCommId) AS url
		,ISNULL(vcCompanyName,'') AS Search_vcCompanyName
		,ISNULL(AdditionalContactsInformation.vcFirstName,'') AS Search_vcFirstName 
		,ISNULL(AdditionalContactsInformation.vcLastName,'') AS Search_vcLastName
		,ISNULL(ListDetails.vcData,'') AS Search_vcActionItemType
		,ISNULL(Communication.textDetails,'') AS Search_textDetails
	FROM 
		Communication
	INNER JOIN
		DivisionMaster
	ON
		Communication.numDivisionId = DivisionMaster.numDivisionID
	INNER JOIN
		CompanyInfo
	ON
		DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
	LEFT JOIN
		AdditionalContactsInformation
	ON
		Communication.numContactId = AdditionalContactsInformation.numContactId
	LEFT JOIN
		ListDetails
	ON
		ListDetails.numListID=73
		AND Communication.bitTask = ListDetails.numListItemID
	WHERE 
		Communication.numDomainID=@numDomainID
		AND ISNULL(bitClosedFlag,0)=0
		AND (@vcActionItemIds IS NULL OR Communication.numCommId IN (SELECT Id FROM dbo.SplitIDs(ISNULL(@vcActionItemIds,'0'),',')))

	IF @vcActionItemIds IS NULL
	BEGIN
		DELETE FROM ElasticSearchModifiedRecords WHERE numDomainID=@numDomainID AND vcModule='ActionItem'
	END
END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ElasticSearchModifiedRecords_Get')
DROP PROCEDURE dbo.USP_ElasticSearchModifiedRecords_Get
GO
CREATE PROCEDURE [dbo].[USP_ElasticSearchModifiedRecords_Get]

AS 
BEGIN
	DELETE FROM ElasticSearchModifiedRecords WHERE numDomainID NOT IN (SELECT numDomainId FROM Domain WHERE bitElasticSearch=1)

	DECLARE @TEMP TABLE
	(
		ID NUMERIC(18,0)
		,numDomainID NUMERIC(18,0)
		,vcModule VARCHAR(15)
		,numRecordID NUMERIC(18,0)
		,vcAction VARCHAR(10)
	)
	INSERT INTO @TEMP (ID,numDomainID,vcModule,numRecordID,vcAction) SELECT TOP 8000 ID,numDomainID,vcModule,numRecordID,vcAction FROM ElasticSearchModifiedRecords ORDER BY ID
	DELETE FROM ElasticSearchModifiedRecords WHERE ID IN (SELECT ID FROM @TEMP)

	SELECT * FROM (SELECT MIN(ID) AS ID,T1.numDomainID,vcModule,numRecordID,vcAction FROM @TEMP T1 INNER JOIN Domain ON T1.numDomainID=Domain.numDomainId WHERE ISNULL(Domain.bitElasticSearch,0)=1 AND vcAction='Insert' AND T1.numDomainID > 0 GROUP BY T1.numDomainID,vcModule,numRecordID,vcAction) TEMP ORDER BY ID
	SELECT * FROM (SELECT MIN(ID) AS ID,T1.numDomainID,vcModule,numRecordID,vcAction FROM @TEMP T1 INNER JOIN Domain ON T1.numDomainID=Domain.numDomainId WHERE ISNULL(Domain.bitElasticSearch,0)=1 AND vcAction='Update' AND T1.numDomainID > 0 GROUP BY T1.numDomainID,vcModule,numRecordID,vcAction) TEMP ORDER BY ID
	SELECT * FROM (SELECT MIN(ID) AS ID,T1.numDomainID,vcModule,numRecordID,vcAction FROM @TEMP T1 INNER JOIN Domain ON T1.numDomainID=Domain.numDomainId WHERE ISNULL(Domain.bitElasticSearch,0)=1 AND vcAction='Delete' AND T1.numDomainID > 0 GROUP BY T1.numDomainID,vcModule,numRecordID,vcAction) TEMP ORDER BY ID
END
/****** Object:  StoredProcedure [dbo].[usp_GetCompanyInfo]    Script Date: 07/26/2008 16:16:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--modified by anoop jayaraj                                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcompanyinfo')
DROP PROCEDURE usp_getcompanyinfo
GO
CREATE PROCEDURE [dbo].[usp_GetCompanyInfo]                                                                                                   
 @numDivisionID numeric,        
 @numDomainID as numeric(9)   ,        
@ClientTimeZoneOffset  int           
    --                                                         
AS                                                          
BEGIN                                                          
 SELECT CMP.numCompanyID, CMP.vcCompanyName,DM.numStatusID as numCompanyStatus,                                                          
  CMP.numCompanyRating, CMP.numCompanyType, DM.bitPublicFlag, DM.numDivisionID,                                                           
  DM. vcDivisionName, 
  ISNULL(AD1.numAddressID,0) as numBillAddressID,
  isnull(AD1.vcStreet,'') as vcBillStreet, 
  isnull(AD1.vcCity,'') as vcBillCity, 
  isnull(dbo.fn_GetState(AD1.numState),'') as vcBilState,
  isnull(AD1.vcPostalCode,'') as vcBillPostCode,
  isnull(dbo.fn_GetListName(AD1.numCountry,0),'')  as vcBillCountry,
  ISNULL(AD2.numAddressID,0) as numShipAddressID,
  isnull(AD2.vcStreet,'') as vcShipStreet,
  isnull(AD2.vcCity,'') as vcShipCity,
  isnull(dbo.fn_GetState(AD2.numState),'')  as vcShipState,
  isnull(AD2.vcPostalCode,'') as vcShipPostCode, 
  AD2.numCountry vcShipCountry,
  numFollowUpStatus,
  CMP.vcWebLabel1, CMP.vcWebLink1, CMP.vcWebLabel2, CMP.vcWebLink2,                                                           
  CMP.vcWebLabel3, CMP.vcWebLink3, CMP.vcWeblabel4, CMP.vcWebLink4,  
   tintBillingTerms,numBillingDays,
   --ISNULL(dbo.fn_GetListItemName(isnull(numBillingDays,0)),0) AS numBillingDaysName,
   (SELECT numNetDueInDays FROM dbo.BillingTerms WHERE numTermsID = numBillingDays AND numDomainID = DM.numDomainID) AS numBillingDaysName,
   tintInterestType,fltInterest,                                                          
   DM.bitPublicFlag, ISNULL(DM.numTerID,0) numTerID,                                                           
  CMP.numCompanyIndustry, CMP.numCompanyCredit,                                                           
  isnull(CMP.txtComments,'') as txtComments, isnull(CMP.vcWebSite,'') vcWebSite, CMP.numAnnualRevID, CMP.numNoOfEmployeesID, dbo.fn_GetListItemName(CMP.numNoOfEmployeesID) as NoofEmp,                                                         
  CMP.vcProfile, DM.numCreatedBy, ISNULL(DM.numRecOwner,0) AS numRecOwner, dbo.fn_GetContactName(DM.numRecOwner) as RecOwner,       
dbo.fn_GetContactName(DM.numCreatedBy)+' ' +convert(varchar(20),dateadd(minute,-@ClientTimeZoneOffset,DM.bintCreatedDate)) as vcCreatedBy,                                                    
 dbo.fn_GetContactName(DM.numModifiedBy)+' ' +convert(varchar(20),dateadd(minute,-@ClientTimeZoneOffset,DM.bintModifiedDate)) as vcModifiedBy,vcComPhone,vcComFax,                                                 
  DM.numGrpID, CMP.vcHow,                                                          
  DM.tintCRMType,isnull(DM.bitNoTax,0) bitNoTax ,                                                      
   numCampaignID,numAssignedBy,numAssignedTo,            
(select  count(*) from dbo.GenericDocuments where numRecID= @numDivisionID and  vcDocumentSection='A' AND tintDocumentType=2) as DocumentCount ,          
(SELECT count(*)from CompanyAssociations where numDivisionID=@numDivisionID and bitDeleted=0 ) as AssociateCountFrom,          
(SELECT count(*)from CompanyAssociations where numAssociateFromDivisionID=@numDivisionID and bitDeleted=0 ) as AssociateCountTo,
 ISNULL(DM.numCurrencyID,0) AS numCurrencyID,ISNULL(DM.numDefaultPaymentMethod,0) AS numDefaultPaymentMethod,ISNULL(DM.numDefaultCreditCard,0) AS numDefaultCreditCard,ISNULL(DM.bitOnCreditHold,0) AS bitOnCreditHold,
 ISNULL(vcShippersAccountNo,'') AS [vcShippersAccountNo],
 ISNULL(intShippingCompany,0) AS [intShippingCompany],ISNULL(DM.bitEmailToCase,0) AS bitEmailToCase,
 ISNULL(numDefaultExpenseAccountID,0) AS [numDefaultExpenseAccountID],
 ISNULL(numDefaultShippingServiceID,0) AS [numDefaultShippingServiceID],
 ISNULL(numAccountClassID,0) As numAccountClassID,
 ISNULL(tintPriceLevel,0) AS tintPriceLevel,DM.vcPartnerCode as vcPartnerCode,
 bitSaveCreditCardInfo,ISNULL(bitShippingLabelRequired,0) bitShippingLabelRequired,
 ISNULL(DM.tintInbound850PickItem,0) AS tintInbound850PickItem,
 ISNULL(bitAllocateInventoryOnPickList,0) bitAllocateInventoryOnPickList
 FROM  CompanyInfo CMP                                                          
 join DivisionMaster DM    
	on DM.numCompanyID=CMP.numCompanyID
 LEFT JOIN dbo.AddressDetails AD1 ON AD1.numDomainID=DM.numDomainID 
   AND AD1.numRecordID=DM.numDivisionID AND AD1.tintAddressOf=2 AND AD1.tintAddressType=1 AND AD1.bitIsPrimary=1
   LEFT JOIN dbo.AddressDetails AD2 ON AD1.numDomainID=DM.numDomainID 
   AND AD2.numRecordID= DM.numDivisionID AND AD2.tintAddressOf=2 AND AD2.tintAddressType=2 AND AD2.bitIsPrimary=1
   left join Domain on Domain.numDomainId=DM.numDomainID
where                                            
   DM.numDivisionID=@numDivisionID   and DM.numDomainID=@numDomainID                                                                        
END
GO
--created by anoop jayaraj                                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getdomaindetails')
DROP PROCEDURE usp_getdomaindetails
GO
CREATE PROCEDURE [dbo].[USP_GetDomainDetails]                                          
@numDomainID as numeric(9)=0                                          
as           

DECLARE @listIds VARCHAR(MAX)
SELECT @listIds = COALESCE(@listIds+',' ,'') + CAST(numUserID AS VARCHAR(100)) FROM UnitPriceApprover WHERE numDomainID = @numDomainID
   
DECLARE @canChangeDiferredIncomeSelection AS BIT = 1

IF EXISTS(SELECT 
			OB.numOppBizDocsId 
		FROM 
			OpportunityBizDocs OB 
		INNER JOIN 
			OpportunityMaster OM 
		ON 
			OB.numOppId=OM.numOppId 
		WHERE 
			numBizDocId=304 
			AND OM.numDomainId=@numDomainID)
BEGIN
	SET @canChangeDiferredIncomeSelection = 0
END   
   
                               
select                                           
vcDomainName,                                           
isnull(vcDomainDesc,'') vcDomainDesc,                                           
isnull(bitExchangeIntegration,0) bitExchangeIntegration,                                           
isnull(bitAccessExchange,0) bitAccessExchange,                                           
isnull(vcExchUserName,'') vcExchUserName,                                           
isnull(vcExchPassword,'') vcExchPassword,                                           
isnull(vcExchPath,'') vcExchPath,                                           
isnull(vcExchDomain,'') vcExchDomain,                                         
tintCustomPagingRows,                                         
vcDateFormat,                                         
numDefCountry,                                         
tintComposeWindow,                                         
sintStartDate,                                         
sintNoofDaysInterval,                                         
tintAssignToCriteria,                                         
bitIntmedPage,
ISNULL(D.numCost,0) as numCost,                                         
tintFiscalStartMonth,                                    
--isnull(bitDeferredIncome,0) as bitDeferredIncome,                                  
isnull(tintPayPeriod,0) as tintPayPeriod,                                
isnull(numCurrencyID,0) as numCurrencyID,                            
isnull(vcCurrency,'') as vcCurrency,                      
isnull(charUnitSystem,'') as charUnitSystem ,                    
isnull(vcPortalLogo,'') as vcPortalLogo,                  
isnull(tintChrForComSearch,2) as tintChrForComSearch,                
isnull(tintChrForItemSearch,'') as tintChrForItemSearch,            
isnull(intPaymentGateWay,0) as intPaymentGateWay  ,        
 case when bitPSMTPServer= 1 then vcPSMTPServer else '' end as vcPSMTPServer  ,              
 case when bitPSMTPServer= 1 then  isnull(numPSMTPPort,0)  else 0 end as numPSMTPPort          
,isnull(bitPSMTPAuth,0) bitPSMTPAuth        
,isnull([vcPSmtpPassword],'')vcPSmtpPassword        
,isnull(bitPSMTPSSL,0) bitPSMTPSSL     
,isnull(bitPSMTPServer,0) bitPSMTPServer  ,isnull(vcPSMTPUserName,'') vcPSMTPUserName,      
--isnull(numDefaultCategory,0) numDefaultCategory,
isnull(numShipCompany,0)   numShipCompany,
isnull(bitAutoPopulateAddress,0) bitAutoPopulateAddress,
isnull(tintPoulateAddressTo,0) tintPoulateAddressTo,
isnull(bitMultiCompany,0) bitMultiCompany,
isnull(bitMultiCurrency,0) bitMultiCurrency,
ISNULL(bitCreateInvoice,0) bitCreateInvoice,
ISNULL([numDefaultSalesBizDocId],0) numDefaultSalesBizDocId,
ISNULL([bitSaveCreditCardInfo],0) bitSaveCreditCardInfo,
ISNULL([intLastViewedRecord],10) intLastViewedRecord,
ISNULL([tintCommissionType],1) tintCommissionType,
ISNULL([tintBaseTax],2) tintBaseTax,
ISNULL([numDefaultPurchaseBizDocId],0) numDefaultPurchaseBizDocId,
ISNULL(bitEmbeddedCost,0) bitEmbeddedCost,
ISNULL(numDefaultSalesOppBizDocId,0) numDefaultSalesOppBizDocId,
ISNULL(tintSessionTimeOut,1) tintSessionTimeOut,
ISNULL(tintDecimalPoints,2) tintDecimalPoints,
ISNULL(bitCustomizePortal,0) bitCustomizePortal,
ISNULL(tintBillToForPO,0) tintBillToForPO,
ISNULL(tintShipToForPO,0) tintShipToForPO,
ISNULL(tintOrganizationSearchCriteria,1) tintOrganizationSearchCriteria,
ISNULL(tintLogin,0) tintLogin,
lower(ISNULL(vcPortalName,'')) vcPortalName,
ISNULL(bitDocumentRepositary,0) bitDocumentRepositary,
D.numDomainID,
ISNULL(vcPSMTPDisplayName,'') vcPSMTPDisplayName,
ISNULL(bitGtoBContact,0) bitGtoBContact,
ISNULL(bitBtoGContact,0) bitBtoGContact,
ISNULL(bitGtoBCalendar,0) bitGtoBCalendar,
ISNULL(bitBtoGCalendar,0) bitBtoGCalendar,
ISNULL(bitExpenseNonInventoryItem,0) bitExpenseNonInventoryItem,
ISNULL(bitInlineEdit,0) bitInlineEdit,
ISNULL(bitRemoveVendorPOValidation,0) bitRemoveVendorPOValidation,
isnull(D.tintBaseTaxOnArea,0) tintBaseTaxOnArea,
ISNULL(D.bitAmountPastDue,0) bitAmountPastDue,
CONVERT(DECIMAL(18,2),ISNULL(D.monAmountPastDue,0)) monAmountPastDue,
ISNULL(bitSearchOrderCustomerHistory,0) bitSearchOrderCustomerHistory,
isnull(D.bitRentalItem,0) as bitRentalItem,
isnull(D.numRentalItemClass,0) as numRentalItemClass,
isnull(D.numRentalHourlyUOM,0) as numRentalHourlyUOM,
isnull(D.numRentalDailyUOM,0) as numRentalDailyUOM,
isnull(D.tintRentalPriceBasedOn,0) as tintRentalPriceBasedOn,
isnull(D.tintCalendarTimeFormat,24) as tintCalendarTimeFormat,
isnull(D.tintDefaultClassType,0) as tintDefaultClassType,
ISNULL(D.tintPriceBookDiscount,0) tintPriceBookDiscount,
ISNULL(D.bitAutoSerialNoAssign,0) as bitAutoSerialNoAssign,
ISNULL(D.bitIsShowBalance,0) AS [bitIsShowBalance],
ISNULL(numIncomeAccID,0) AS [numIncomeAccID],
ISNULL(numCOGSAccID,0) AS [numCOGSAccID],
ISNULL(numAssetAccID,0) AS [numAssetAccID],
ISNULL(IsEnableProjectTracking,0) AS [IsEnableProjectTracking],
ISNULL(IsEnableCampaignTracking,0) AS [IsEnableCampaignTracking],
ISNULL(IsEnableResourceScheduling,0) AS [IsEnableResourceScheduling],
ISNULL(numShippingServiceItemID,0) AS [numShippingServiceItemID],
ISNULL(D.numSOBizDocStatus,0) AS numSOBizDocStatus,
ISNULL(D.bitAutolinkUnappliedPayment,0) AS bitAutolinkUnappliedPayment,
ISNULL(D.numDiscountServiceItemID,0) AS numDiscountServiceItemID,
ISNULL(D.vcShipToPhoneNo,0) AS vcShipToPhoneNo,
ISNULL(D.vcGAUserEMail,'') AS vcGAUserEMail,
ISNULL(D.vcGAUserPassword,'') AS vcGAUserPassword,
ISNULL(D.vcGAUserProfileId,0) AS vcGAUserProfileId,
ISNULL(D.bitDiscountOnUnitPrice,0) AS bitDiscountOnUnitPrice,
ISNULL(D.intShippingImageWidth,0) AS intShippingImageWidth ,
ISNULL(D.intShippingImageHeight,0) AS intShippingImageHeight,
ISNULL(D.numTotalInsuredValue,0) AS numTotalInsuredValue,
ISNULL(D.numTotalCustomsValue,0) AS numTotalCustomsValue,
ISNULL(D.vcHideTabs,'') AS vcHideTabs,
ISNULL(D.bitUseBizdocAmount,0) AS bitUseBizdocAmount,
ISNULL(D.bitDefaultRateType,0) AS bitDefaultRateType,
ISNULL(D.bitIncludeTaxAndShippingInCommission,0) AS bitIncludeTaxAndShippingInCommission,
/*,ISNULL(D.bitAllowPPVariance,0) AS bitAllowPPVariance*/
ISNULL(D.bitPurchaseTaxCredit,0) AS bitPurchaseTaxCredit,
ISNULL(D.bitLandedCost,0) AS bitLandedCost,
ISNULL(D.vcLanedCostDefault,'') AS vcLanedCostDefault,
ISNULL(D.bitMinUnitPriceRule,0) AS bitMinUnitPriceRule,
ISNULL(AP.numAbovePercent,0) AS numAbovePercent,
ISNULL(AP.numBelowPercent,0) AS numBelowPercent,
ISNULL(AP.numBelowPriceField,0) AS numBelowPriceField,
@listIds AS vcUnitPriceApprover,
ISNULL(D.numDefaultSalesPricing,2) AS numDefaultSalesPricing,
ISNULL(D.bitReOrderPoint,0) AS bitReOrderPoint,
ISNULL(D.numReOrderPointOrderStatus,0) AS numReOrderPointOrderStatus,
ISNULL(D.numPODropShipBizDoc,0) AS numPODropShipBizDoc,
ISNULL(D.numPODropShipBizDocTemplate,0) AS numPODropShipBizDocTemplate,
ISNULL(D.IsEnableDeferredIncome,0) AS IsEnableDeferredIncome,
ISNULL(D.bitApprovalforTImeExpense,0) AS bitApprovalforTImeExpense,
ISNULL(D.intTimeExpApprovalProcess,0) AS intTimeExpApprovalProcess,
ISNULL(D.bitApprovalforOpportunity,0) AS bitApprovalforOpportunity,
ISNULL(D.intOpportunityApprovalProcess,0) AS intOpportunityApprovalProcess,
@canChangeDiferredIncomeSelection AS canChangeDiferredIncomeSelection,
ISNULL(CAST(D.numDefaultSalesShippingDoc AS int),0) AS numDefaultSalesShippingDoc,
ISNULL(CAST(D.numDefaultPurchaseShippingDoc AS int),0) AS numDefaultPurchaseShippingDoc,
ISNULL(D.bitchkOverRideAssignto,0) AS bitchkOverRideAssignto,
ISNULL(D.vcPrinterIPAddress,'') vcPrinterIPAddress,
ISNULL(D.vcPrinterPort,'') vcPrinterPort,
ISNULL(AP.bitCostApproval,0) bitCostApproval
,ISNULL(AP.bitListPriceApproval,0) bitListPriceApproval
,ISNULL(AP.bitMarginPriceViolated,0) bitMarginPriceViolated
,ISNULL(vcSalesOrderTabs,'Sales Orders') AS vcSalesOrderTabs
,ISNULL(vcSalesQuotesTabs,'Sales Quotes') AS vcSalesQuotesTabs
,ISNULL(vcItemPurchaseHistoryTabs,'Item Purchase History') AS vcItemPurchaseHistoryTabs
,ISNULL(vcItemsFrequentlyPurchasedTabs,'Items Frequently Purchased') AS vcItemsFrequentlyPurchasedTabs
,ISNULL(vcOpenCasesTabs,'Open Cases') AS vcOpenCasesTabs
,ISNULL(vcOpenRMATabs,'Open RMAs') AS vcOpenRMATabs
,ISNULL(bitSalesOrderTabs,0) AS bitSalesOrderTabs
,ISNULL(bitSalesQuotesTabs,0) AS bitSalesQuotesTabs
,ISNULL(bitItemPurchaseHistoryTabs,0) AS bitItemPurchaseHistoryTabs
,ISNULL(bitItemsFrequentlyPurchasedTabs,0) AS bitItemsFrequentlyPurchasedTabs
,ISNULL(bitOpenCasesTabs,0) AS bitOpenCasesTabs
,ISNULL(bitOpenRMATabs,0) AS bitOpenRMATabs
,ISNULL(bitSupportTabs,0) AS bitSupportTabs
,ISNULL(vcSupportTabs,'Support') AS vcSupportTabs
,ISNULL(D.numDefaultSiteID,0) AS numDefaultSiteID
,ISNULL(tintOppStautsForAutoPOBackOrder,0) AS tintOppStautsForAutoPOBackOrder
,ISNULL(tintUnitsRecommendationForAutoPOBackOrder,1) AS tintUnitsRecommendationForAutoPOBackOrder
,ISNULL(bitRemoveGlobalLocation,0) AS bitRemoveGlobalLocation
,ISNULL(numAuthorizePercentage,0) AS numAuthorizePercentage
,ISNULL(bitEDI,0) AS bitEDI
,ISNULL(bit3PL,0) AS bit3PL
,ISNULL(numListItemID,0) AS numListItemID
,ISNULL(bitAllowDuplicateLineItems,0) bitAllowDuplicateLineItems
,ISNULL(tintCommitAllocation,1) tintCommitAllocation
,ISNULL(tintInvoicing,1) tintInvoicing
from Domain D  
LEFT JOIN eCommerceDTL eComm  on eComm.numDomainID=D.numDomainID  
LEFT JOIN ApprovalProcessItemsClassification AP ON AP.numDomainID = D.numDomainID
where (D.numDomainID=@numDomainID OR @numDomainID=-1)


--created by anoop jayaraj

-- exec USP_GetOppItemBizDocs @numBizDocID=644,@numDomainID=169,@numOppID=476291,@numOppBizDocID=0,@bitRentalBizDoc=0
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetOppItemBizDocs' ) 
    DROP PROCEDURE USP_GetOppItemBizDocs
GO
CREATE PROCEDURE USP_GetOppItemBizDocs
    @numBizDocID AS NUMERIC(9),
    @numDomainID AS NUMERIC(9),
    @numOppID AS NUMERIC(9),
    @numOppBizDocID AS NUMERIC(9),
    @bitRentalBizDoc AS BIT = 0
AS 
BEGIN

	DECLARE @tintCommitAllocation AS TINYINT
	DECLARE @bitAllocateInventoryOnPickList AS BIT

	SELECT @tintCommitAllocation=ISNULL(tintCommitAllocation,1) FROM Domain WHERE numDomainId=@numDomainID
	SELECT @bitAllocateInventoryOnPickList=ISNULL(bitAllocateInventoryOnPickList,0) FROM OpportunityMaster INNER JOIN DivisionMaster ON OpportunityMaster.numDivisionId=DivisionMaster.numDivisionID WHERE numOppId=@numOppID

	DECLARE @numSalesAuthoritativeBizDocID AS INT
	SELECT @numSalesAuthoritativeBizDocID = numAuthoritativeSales FROM [AuthoritativeBizDocs] WHERE numDomainID = @numDomainID


    IF @numOppBizDocID = 0 
        IF @bitRentalBizDoc = 1 
        BEGIN
            SELECT  *
            FROM    ( SELECT    OI.numoppitemtCode,
                                OI.numItemCode,
                                OI.vcItemName,
                                OI.vcModelID,
                                OI.numUnitHour AS QtyOrdered,
                                (OI.numUnitHour - ISNULL(SUM(OBI.numUnitHour), 0)) AS QtytoFulFilled,
                                (OI.numUnitHour - ISNULL(SUM(OBI.numUnitHour), 0)) AS QtytoFulFill,
                                ISNULL(numAllocation, 0) AS numAllocation,
                                ISNULL(numBackOrder, 0) AS numBackOrder,
                                CASE WHEN OBI.vcNotes IS NULL THEN OI.vcNotes ELSE OBI.vcNotes END AS vcNotes,
                                '' AS vcTrackingNo,
                                '' vcShippingMethod,
                                0 monShipCost,
                                GETDATE() dtDeliveryDate,
                                CONVERT(BIT, 1) AS Included,
                                ISNULL(OI.[numQtyShipped], 0) numQtyShipped,
                                ISNULL(OI.[numUnitHourReceived], 0) numQtyReceived,
                                I.[charItemType],
                                SUBSTRING(( SELECT  ',' + vcSerialNo
                                                    + CASE WHEN ISNULL(I.bitLotNo, 0) = 1 THEN ' (' + CONVERT(VARCHAR(15), oppI.numQty) + ')'
                                                            ELSE ''
                                                        END
                                            FROM    OppWarehouseSerializedItem oppI
                                                    JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID = whi.numWareHouseItmsDTLID
                                            WHERE   oppI.numOppID = @numOppID
                                                    AND oppI.numOppItemID = OI.numoppitemtCode
                                                    AND oppI.numOppBizDocsId = @numOppBizDocID
                                            ORDER BY vcSerialNo
                                            FOR
                                            XML PATH('')
                                            ), 2, 200000) AS SerialLotNo,
                                I.bitSerialized,
                                ISNULL(I.bitLotNo, 0) AS bitLotNo,
                                OI.numWarehouseItmsID,
                                ISNULL(I.numItemClassification, 0) AS numItemClassification,
                                I.vcSKU,
                                dbo.fn_GetAttributes(OI.numWarehouseItmsID,
                                                        I.bitSerialized) AS Attributes,
                                OM.bintCreatedDate AS dtRentalStartDate,
                                GETDATE() AS dtRentalReturnDate,
                                ISNULL(OI.numUOMId, 0) AS numUOM,
                                OI.monPrice AS monOppItemPrice,
                                ISNULL(OBI.monPrice, OI.monPrice) AS monOppBizDocPrice,
								OI.monTotAmount AS monOppBizDocAmount,
                                ISNULL(OI.vcItemDesc, '') AS [vcItemDesc],
								ISNULL(OI.bitDropShip,0) AS bitDropShip
                        FROM      OpportunityItems OI
                                LEFT JOIN OpportunityMaster OM ON OI.numOppID = oM.numOppID
                                LEFT JOIN OpportunityBizDocs OB ON OB.numOppId = OI.numOppId
                                                                    AND OB.numBizDocId = @numBizDocID
                                LEFT JOIN OpportunityBizDocItems OBI ON OB.numOppBizDocsId = OBI.numOppBizDocId
                                                                        AND OBI.numOppItemID = OI.numoppitemtCode
                                LEFT JOIN WareHouseItems WI ON WI.numWareHouseItemID = OI.numWarehouseItmsID
                                INNER JOIN Item I ON I.[numItemCode] = OI.[numItemCode]
                        WHERE     OI.numOppId = @numOppID
                                AND OI.numoppitemtCode NOT IN (
                                SELECT  OBDI.numOppItemID
                                FROM    OpportunityBizDocs OBD
                                        JOIN OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId = OBDI.numOppBizDocId
                                WHERE   OBD.numOppId = @numOppID )
                        GROUP BY  OI.numoppitemtCode,
                                OI.numItemCode,
                                OI.vcItemName,
                                OI.vcModelID,
								OI.vcNotes,
								OBI.vcNotes,
                                OI.numUnitHour,
                                numAllocation,
                                numBackOrder,
                                OI.numQtyShipped,
                                OI.[numUnitHourReceived],
                                I.[charItemType],
                                I.bitSerialized,
                                I.bitLotNo,
                                OI.numWarehouseItmsID,
                                I.numItemClassification,
                                I.vcSKU,
                                OI.numWarehouseItmsID,
                                OBI.dtRentalStartDate,
                                OBI.dtRentalReturnDate,
                                OI.numUOMId,
                                OM.bintCreatedDate,
                                OI.monPrice,
								OI.monTotAmount,
                                OBI.monPrice,
                                OI.vcItemDesc,OI.bitDropShip
                    ) X
            WHERE   X.QtytoFulFill > 0
        END
        ELSE 
        BEGIN
            SELECT  *
            FROM    ( SELECT    OI.numoppitemtCode,
                                OI.numItemCode,
                                OI.vcItemName,
                                OI.vcModelID,
                                OI.numUnitHour AS QtyOrdered,
								CASE 
									WHEN @numBizDocId = 296 
									THEN
										(CASE 
											WHEN (I.charItemType = 'p' AND ISNULL(OI.bitDropShip, 0) = 0 AND ISNULL(bitAsset,0)=0)
											THEN 
												(CASE WHEN (OI.numUnitHour - ISNULL(SUM(OBI.numUnitHour),0)) <= dbo.GetOrderItemAvailableQtyToShip(OI.numoppitemtCode,OI.numWarehouseItmsID,ISNULL(I.bitKitParent,0),@tintCommitAllocation,@bitAllocateInventoryOnPickList) THEN (OI.numUnitHour - ISNULL(SUM(OBI.numUnitHour),0)) ELSE dbo.GetOrderItemAvailableQtyToShip(OI.numoppitemtCode,OI.numWarehouseItmsID,ISNULL(I.bitKitParent,0),@tintCommitAllocation,@bitAllocateInventoryOnPickList) END)
											ELSE (OI.numUnitHour - ISNULL(SUM(OBI.numUnitHour),0))
										END)
									ELSE
										(OI.numUnitHour - ISNULL(SUM(OBI.numUnitHour),0))
								END AS QtytoFulFilled,
                                CASE 
									WHEN @numBizDocId = 296 
									THEN
										(CASE 
											WHEN (I.charItemType = 'p' AND ISNULL(OI.bitDropShip, 0) = 0 AND ISNULL(bitAsset,0)=0)
											THEN 
												(CASE WHEN (OI.numUnitHour - ISNULL(SUM(OBI.numUnitHour),0)) <= dbo.GetOrderItemAvailableQtyToShip(OI.numoppitemtCode,OI.numWarehouseItmsID,ISNULL(I.bitKitParent,0),@tintCommitAllocation,@bitAllocateInventoryOnPickList) THEN (OI.numUnitHour - ISNULL(SUM(OBI.numUnitHour),0)) ELSE dbo.GetOrderItemAvailableQtyToShip(OI.numoppitemtCode,OI.numWarehouseItmsID,ISNULL(I.bitKitParent,0),@tintCommitAllocation,@bitAllocateInventoryOnPickList) END)
											ELSE (OI.numUnitHour - ISNULL(SUM(OBI.numUnitHour),0))
										END)
									ELSE
										(OI.numUnitHour - ISNULL(SUM(OBI.numUnitHour),0))
								END AS QtytoFulFill,
                                ISNULL(numAllocation, 0) AS numAllocation,
                                ISNULL(numBackOrder, 0) AS numBackOrder,
                                CASE WHEN OBI.vcNotes IS NULL THEN OI.vcNotes ELSE OBI.vcNotes END AS vcNotes,
                                '' AS vcTrackingNo,
                                '' vcShippingMethod,
                                0 monShipCost,
                                GETDATE() dtDeliveryDate,
                                CONVERT(BIT, 1) AS Included,
                                ISNULL(OI.[numQtyShipped], 0) numQtyShipped,
                                ISNULL(OI.[numUnitHourReceived], 0) numQtyReceived,
                                I.[charItemType],
                                SUBSTRING(( SELECT  ',' + vcSerialNo
                                                    + CASE WHEN ISNULL(I.bitLotNo, 0) = 1 THEN ' (' + CONVERT(VARCHAR(15), oppI.numQty) + ')'
                                                            ELSE ''
                                                        END
                                            FROM    OppWarehouseSerializedItem oppI
                                                    JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID = whi.numWareHouseItmsDTLID
                                            WHERE   oppI.numOppID = @numOppID
                                                    AND oppI.numOppItemID = OI.numoppitemtCode
                                                    AND oppI.numOppBizDocsId = @numOppBizDocID
                                            ORDER BY vcSerialNo
                                            FOR
                                            XML PATH('')
                                            ), 2, 200000) AS SerialLotNo,
                                I.bitSerialized,
                                ISNULL(I.bitLotNo, 0) AS bitLotNo,
                                OI.numWarehouseItmsID,
                                ISNULL(I.numItemClassification, 0) AS numItemClassification,
                                I.vcSKU,
                                dbo.fn_GetAttributes(OI.numWarehouseItmsID,
                                                        I.bitSerialized) AS Attributes,
                                OBI.dtRentalStartDate,
                                OBI.dtRentalReturnDate,
                                ISNULL(OI.numUOMId, 0) AS numUOM,
                                OI.monPrice AS monOppItemPrice,
                                ISNULL(OBI.monPrice, OI.monPrice) AS monOppBizDocPrice,
								OI.monTotAmount AS monOppBizDocAmount,
                                ISNULL(OI.vcItemDesc, '') AS [vcItemDesc],
								ISNULL(OI.bitDropShip,0) AS bitDropShip
                        FROM      OpportunityItems OI
                                LEFT JOIN OpportunityMaster OM ON OI.numOppID = oM.numOppID 
                                LEFT JOIN 
									OpportunityBizDocItems OBI 
								ON 
									OBI.numOppItemID = OI.numoppitemtCode 
                                    AND OBI.numOppBizDocId IN ( SELECT  
																	numOppBizDocsId
                                                                FROM    
																	OpportunityBizDocs OB
                                                                WHERE   
																	OB.numOppId = OI.numOppId
                                                                    AND 1 = (CASE 
																				WHEN @numBizDocId = @numSalesAuthoritativeBizDocID --Invoice OR Deferred Income
																				THEN 
																					(CASE WHEN numBizDocId = 304 OR (ISNULL(OB.bitAuthoritativeBizDocs,0) = 1 AND ISNULL(OB.numDeferredBizDocID,0) = 0) THEN 1 ELSE 0 END)
																				WHEN  @numBizDocId = 304
																				THEN (CASE WHEN ISNULL(OB.bitAuthoritativeBizDocs,0) = 1 THEN 1 ELSE 0 END)
																				ELSE 
																					(CASE WHEN numBizDocId = @numBizDocId THEN 1 ELSE 0 END) 
																				END) 
																)
                                LEFT JOIN WareHouseItems WI ON WI.numWareHouseItemID = OI.numWarehouseItmsID
                                INNER JOIN Item I ON I.[numItemCode] = OI.[numItemCode]
                        WHERE     OI.numOppId = @numOppID
                        GROUP BY  OI.numoppitemtCode,
                                OI.numItemCode,
                                OI.vcItemName,
                                OI.vcModelID,
								OI.vcNotes,
								OBI.vcNotes,
                                OI.numUnitHour,
                                numAllocation,
                                numBackOrder,
                                OI.numQtyShipped,
                                OI.[numUnitHourReceived],
                                I.[charItemType],
                                I.bitSerialized,
                                I.bitLotNo,
								I.bitAsset,
								I.bitKitParent,
                                OI.numWarehouseItmsID,
                                I.numItemClassification,
                                I.vcSKU,
                                OI.numWarehouseItmsID,
                                OBI.dtRentalStartDate,
                                OBI.dtRentalReturnDate,
                                OI.numUOMId,
                                OI.monPrice,
								OI.monTotAmount,
                                OBI.monPrice,
                                OI.vcItemDesc,OI.bitDropShip
                    ) X
            WHERE   X.QtytoFulFill > 0
        END
    ELSE 
        IF @bitRentalBizDoc = 1 
        BEGIN
            SELECT  OI.numoppitemtCode,
                    OI.numItemCode,
                    OI.vcItemName,
                    OI.vcModelID,
                    OI.numUnitHour AS QtyOrdered,
                    OBI.numUnitHour AS QtytoFulFilled,
                    ( OI.numUnitHour - ISNULL(SUM(OBI.numUnitHour), 0) ) AS QtytoFulFill,
                    ISNULL(numAllocation, 0) AS numAllocation,
                    ISNULL(numBackOrder, 0) AS numBackOrder,
                    CASE WHEN OBI.vcNotes IS NULL THEN OI.vcNotes ELSE OBI.vcNotes END AS vcNotes,
                    ISNULL(OBI.vcTrackingNo, '') AS vcTrackingNo,
                    OBI.vcShippingMethod,
                    OBI.monShipCost,
                    OBI.dtDeliveryDate,
                    CONVERT(BIT, 1) AS Included,
                    ISNULL(OI.[numQtyShipped], 0) numQtyShipped,
                    ISNULL(OI.[numUnitHourReceived], 0) numQtyReceived,
                    I.[charItemType],
                    SUBSTRING(( SELECT  ',' + vcSerialNo
                                        + CASE WHEN ISNULL(I.bitLotNo, 0) = 1
                                                THEN ' ('
                                                    + CONVERT(VARCHAR(15), oppI.numQty)
                                                    + ')'
                                                ELSE ''
                                            END
                                FROM    OppWarehouseSerializedItem oppI
                                        JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID = whi.numWareHouseItmsDTLID
                                WHERE   oppI.numOppID = @numOppID
                                        AND oppI.numOppItemID = OI.numoppitemtCode
                                        AND oppI.numOppBizDocsId = @numOppBizDocID
                                ORDER BY vcSerialNo
                                FOR
                                XML PATH('')
                                ), 2, 200000) AS SerialLotNo,
                    I.bitSerialized,
                    ISNULL(I.bitLotNo, 0) AS bitLotNo,
                    OI.numWarehouseItmsID,
                    ISNULL(I.numItemClassification, 0) AS numItemClassification,
                    I.vcSKU,
                    dbo.fn_GetAttributes(OI.numWarehouseItmsID,
                                            I.bitSerialized) AS Attributes,
                    OBI.dtRentalStartDate,
                    OBI.dtRentalReturnDate,
                    ISNULL(OI.numUOMId, 0) AS numUOM,
                    OI.monPrice AS monOppItemPrice,
                    ISNULL(OBI.monPrice, OI.monPrice) AS monOppBizDocPrice,
					OI.monTotAmount AS monOppBizDocAmount,
                    ISNULL(OI.vcItemDesc, '') AS [vcItemDesc],
					ISNULL(OI.bitDropShip,0) AS bitDropShip
            FROM    OpportunityItems OI
                    LEFT JOIN OpportunityMaster OM ON OI.numOppID = oM.numOppID
                    JOIN OpportunityBizDocs OB ON OB.numOppId = OI.numOppId
                    JOIN OpportunityBizDocItems OBI ON OB.numOppBizDocsId = OBI.numOppBizDocId
                                                        AND OBI.numOppItemID = OI.numoppitemtCode
                    LEFT JOIN WareHouseItems WI ON WI.numWareHouseItemID = OI.numWarehouseItmsID
                    INNER JOIN Item I ON I.[numItemCode] = OI.[numItemCode]
            WHERE   OI.numOppId = @numOppID
                    AND OB.numOppBizDocsId = @numOppBizDocID
            GROUP BY OI.numoppitemtCode,
                    OI.numItemCode,
                    OI.vcItemName,
                    OI.vcModelID,
					OI.vcNotes,
                    OI.numUnitHour,
                    numAllocation,
                    numBackOrder,
                    OBI.vcNotes,
                    OBI.vcTrackingNo,
                    OBI.vcShippingMethod,
                    OBI.monShipCost,
                    OBI.dtDeliveryDate,
                    OBI.numUnitHour,
                    OI.[numQtyShipped],
                    OI.[numUnitHourReceived],
                    I.[charItemType],
                    I.bitSerialized,
                    I.bitLotNo,
                    OI.numWarehouseItmsID,
                    I.numItemClassification,
                    I.vcSKU,
                    OI.numWarehouseItmsID,
                    OBI.dtRentalStartDate,
                    OBI.dtRentalReturnDate,
                    OI.numUOMId,
                    OI.monPrice,
					OI.monTotAmount,
                    OBI.monPrice,
                    OI.vcItemDesc,OI.bitDropShip
        END
        ELSE 
        BEGIN
            SELECT  OI.numoppitemtCode,
                    OI.numItemCode,
                    OI.vcItemName,
                    OI.vcModelID,
                    OI.numUnitHour AS QtyOrdered,
                    OBI.numUnitHour AS QtytoFulFilled,
                    (OI.numUnitHour - ISNULL(SUM(OBI.numUnitHour), 0) - ISNULL(TEMPDeferredIncomeItem.numUnitHour,0)) AS QtytoFulFill,
                    ISNULL(numAllocation, 0) AS numAllocation,
                    ISNULL(numBackOrder, 0) AS numBackOrder,
                    CASE WHEN OBI.vcNotes IS NULL THEN OI.vcNotes ELSE OBI.vcNotes END AS vcNotes,
                    ISNULL(OBI.vcTrackingNo, '') AS vcTrackingNo,
                    OBI.vcShippingMethod,
                    OBI.monShipCost,
                    OBI.dtDeliveryDate,
                    CONVERT(BIT, 1) AS Included,
                    ISNULL(OI.[numQtyShipped], 0) numQtyShipped,
                    ISNULL(OI.[numUnitHourReceived], 0) numQtyReceived,
                    I.[charItemType],
                    SUBSTRING(( SELECT  ',' + vcSerialNo
                                        + CASE WHEN ISNULL(I.bitLotNo, 0) = 1
                                                THEN ' ('
                                                    + CONVERT(VARCHAR(15), oppI.numQty)
                                                    + ')'
                                                ELSE ''
                                            END
                                FROM    OppWarehouseSerializedItem oppI
                                        JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID = whi.numWareHouseItmsDTLID
                                WHERE   oppI.numOppID = @numOppID
                                        AND oppI.numOppItemID = OI.numoppitemtCode
                                        AND oppI.numOppBizDocsId = @numOppBizDocID
                                ORDER BY vcSerialNo
                                FOR
                                XML PATH('')
                                ), 2, 200000) AS SerialLotNo,
                    I.bitSerialized,
                    ISNULL(I.bitLotNo, 0) AS bitLotNo,
                    OI.numWarehouseItmsID,
                    ISNULL(I.numItemClassification, 0) AS numItemClassification,
                    I.vcSKU,
                    dbo.fn_GetAttributes(OI.numWarehouseItmsID,
                                            I.bitSerialized) AS Attributes,
                    OBI.dtRentalStartDate,
                    OBI.dtRentalReturnDate,
                    ISNULL(OI.numUOMId, 0) AS numUOM,
                    OI.monPrice AS monOppItemPrice,
                    ISNULL(OBI.monPrice, OI.monPrice) AS monOppBizDocPrice,
					OI.monTotAmount AS monOppBizDocAmount,
                    ISNULL(OI.vcItemDesc, '') AS [vcItemDesc],
					ISNULL(OI.bitDropShip,0) AS bitDropShip
            FROM    OpportunityItems OI
                    LEFT JOIN OpportunityMaster OM ON OI.numOppID = oM.numOppID
                    JOIN OpportunityBizDocs OB ON OB.numOppId = OI.numOppId
                    JOIN OpportunityBizDocItems OBI ON OB.numOppBizDocsId = OBI.numOppBizDocId
                                                        AND OBI.numOppItemID = OI.numoppitemtCode
                    LEFT JOIN WareHouseItems WI ON WI.numWareHouseItemID = OI.numWarehouseItmsID
                    INNER JOIN Item I ON I.[numItemCode] = OI.[numItemCode]
					OUTER APPLY
					(
						SELECT
							SUM(OpportunityBizDocItems.numUnitHour) AS numUnitHour
						FROM 
							OpportunityBizDocs
						INNER JOIN
							OpportunityBizDocItems 
						ON
							OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
						WHERE
							numOppId = @numOppID
							AND numOppBizDocID <> @numOppBizDocID
							AND	1 = (CASE 
									WHEN @numBizDocID = 304 --DEFERRED BIZDOC OR INVOICE
									THEN (CASE WHEN (numBizDocId = @numSalesAuthoritativeBizDocID OR numBizDocId=304) THEN 1 ELSE 0 END) -- WHEN DEFERRED BIZDOC(304) IS BEING ADDED SUBTRACT ITEMS ADDED TO INVOICE AND OTHER DEFERRED BIZDOC IN QtytoFulFill
									ELSE (CASE WHEN numBizDocId = @numBizDocID THEN 1 ELSE 0 END)
									END) 
							AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
					) TEMPDeferredIncomeItem
            WHERE   OI.numOppId = @numOppID
                    AND OB.numOppBizDocsId = @numOppBizDocID
            GROUP BY OI.numoppitemtCode,
                    OI.numItemCode,
                    OI.vcItemName,
                    OI.vcModelID,
					OI.vcNotes,
                    OI.numUnitHour,
                    numAllocation,
                    numBackOrder,
                    OBI.vcNotes,
                    OBI.vcTrackingNo,
                    OBI.vcShippingMethod,
                    OBI.monShipCost,
                    OBI.dtDeliveryDate,
                    OBI.numUnitHour,
                    OI.[numQtyShipped],
                    OI.[numUnitHourReceived],
                    I.[charItemType],
                    I.bitSerialized,
                    I.bitLotNo,
                    OI.numWarehouseItmsID,
                    I.numItemClassification,
                    I.vcSKU,
                    OI.numWarehouseItmsID,
                    OBI.dtRentalStartDate,
                    OBI.dtRentalReturnDate,
                    OI.numUOMId,
                    OI.monPrice,
					OI.monTotAmount,
                    OBI.monPrice,
                    OI.vcItemDesc,OI.bitDropShip,
					TEMPDeferredIncomeItem.numUnitHour
            UNION
            SELECT  *
            FROM    ( SELECT    OI.numoppitemtCode,
                                OI.numItemCode,
                                OI.vcItemName,
                                OI.vcModelID,
                                OI.numUnitHour AS QtyOrdered,
                                ( OI.numUnitHour
                                    - ISNULL(SUM(OBI.numUnitHour), 0) ) AS QtytoFulFilled,
                                ( OI.numUnitHour
                                    - ISNULL(SUM(OBI.numUnitHour), 0) ) AS QtytoFulFill,
                                ISNULL(numAllocation, 0) AS numAllocation,
                                ISNULL(numBackOrder, 0) AS numBackOrder,
                                CASE WHEN OBI.vcNotes IS NULL THEN OI.vcNotes ELSE OBI.vcNotes END AS vcNotes,
                                '' AS vcTrackingNo,
                                '' vcShippingMethod,
                                1 AS monShipCost,
                                NULL dtDeliveryDate,
                                CONVERT(BIT, 0) AS Included,
                                ISNULL(OI.[numQtyShipped], 0) numQtyShipped,
                                ISNULL(OI.[numUnitHourReceived], 0) numQtyReceived,
                                I.[charItemType],
                                SUBSTRING(( SELECT  ',' + vcSerialNo
                                                    + CASE WHEN ISNULL(I.bitLotNo, 0) = 1 THEN ' (' + CONVERT(VARCHAR(15), oppI.numQty) + ')'
                                                            ELSE ''
                                                        END
                                            FROM    OppWarehouseSerializedItem oppI
                                                    JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID = whi.numWareHouseItmsDTLID
                                            WHERE   oppI.numOppID = @numOppID
                                                    AND oppI.numOppItemID = OI.numoppitemtCode
                                                    AND oppI.numOppBizDocsId = @numOppBizDocID
                                            ORDER BY vcSerialNo
                                            FOR
                                            XML PATH('')
                                            ), 2, 200000) AS SerialLotNo,
                                I.bitSerialized,
                                ISNULL(I.bitLotNo, 0) AS bitLotNo,
                                OI.numWarehouseItmsID,
                                ISNULL(I.numItemClassification, 0) AS numItemClassification,
                                I.vcSKU,
                                dbo.fn_GetAttributes(OI.numWarehouseItmsID,
                                                        I.bitSerialized) AS Attributes,
                                OBI.dtRentalStartDate,
                                OBI.dtRentalReturnDate,
                                ISNULL(OI.numUOMId, 0) AS numUOM,
                                OI.monPrice AS monOppItemPrice,
                                ISNULL(OBI.monPrice, OI.monPrice) AS monOppBizDocPrice,
								OI.monTotAmount AS monOppBizDocAmount,
                                ISNULL(OI.vcItemDesc, '') AS [vcItemDesc],
								ISNULL(OI.bitDropShip,0) AS bitDropShip
                        FROM      OpportunityItems OI
                                LEFT JOIN OpportunityMaster OM ON OI.numOppID = oM.numOppID
                                LEFT JOIN OpportunityBizDocs OB ON OB.numOppId = OI.numOppId
                                                                    AND OB.numBizDocId = @numBizDocID
                                LEFT JOIN OpportunityBizDocItems OBI ON OB.numOppBizDocsId = OBI.numOppBizDocId
                                                                        AND OBI.numOppItemID = OI.numoppitemtCode
                                LEFT JOIN WareHouseItems WI ON WI.numWareHouseItemID = OI.numWarehouseItmsID
                                INNER JOIN Item I ON I.[numItemCode] = OI.[numItemCode]
                        WHERE     OI.numOppId = @numOppID
                                AND OI.numoppitemtCode NOT IN (
                                    SELECT  numOppItemID
                                    FROM    OpportunityBizDocItems OBI2
                                            JOIN OpportunityBizDocs OB2 ON OBI2.numOppBizDocID = OB2.numOppBizDocsId
                                    WHERE   numOppID = @numOppID
                                            AND numBizDocId = @numBizDocID )
                        GROUP BY  OI.numoppitemtCode,
                                OI.numItemCode,
                                OI.vcItemName,
                                OI.vcModelID,
								OI.vcNotes,
								OBI.vcNotes,
                                OI.numUnitHour,
                                numAllocation,
                                numBackOrder,
                                OBI.numUnitHour,
                                OI.[numQtyShipped],
                                OI.[numUnitHourReceived],
                                I.[charItemType],
                                I.bitSerialized,
                                I.bitLotNo,
                                OI.numWarehouseItmsID,
                                I.numItemClassification,
                                I.vcSKU,
                                OI.numWarehouseItmsID,
                                OBI.dtRentalStartDate,
                                OBI.dtRentalReturnDate,
                                OI.numUOMId,
                                OI.monPrice,
								OI.monTotAmount,
                                OBI.monPrice,
                                OI.vcItemDesc,OI.bitDropShip
                    ) X
            WHERE   X.QtytoFulFill > 0
        END
END


GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetOppItemsForPickPackShip' ) 
    DROP PROCEDURE USP_GetOppItemsForPickPackShip
GO
CREATE PROCEDURE USP_GetOppItemsForPickPackShip
    @numDomainID AS NUMERIC(18,0),
    @numOppID AS NUMERIC(18,0),
	@numBizDocID AS NUMERIC(18,0)
AS 
BEGIN
	IF @numBizDocID = 29397 -- Packing Slip
	BEGIN
		SELECT    
			OI.numoppitemtCode,
			OI.numUnitHour AS QtyOrdered,
			OI.numUnitHour - ISNULL(SUM(OBIPackingSlip.numUnitHour),0) AS QtytoFulFillOriginal,
			(OI.numUnitHour - (CASE WHEN ISNULL(OBDFulfillment.numUnitHour,0) > 0 THEN ISNULL(OBDFulfillment.numUnitHour,0) ELSE ISNULL(SUM(OBIPackingSlip.numUnitHour),0) END)) AS QtytoFulFill
		FROM 
			OpportunityMaster OM
		INNER JOIN
			OpportunityItems OI
		ON
			OM.numOppId = OI.numOppId
		LEFT JOIN
			OpportunityBizDocs OBDPackingSlip
		ON
			OM.numOppId = OBDPackingSlip.numOppId
			AND OBDPackingSlip.numBizDocId = @numBizDocID
		LEFT JOIN
			OpportunityBizDocItems OBIPackingSlip
		ON
			OBDPackingSlip.numOppBizDocsId = OBIPackingSlip.numOppBizDocID
			AND OBIPackingSlip.numOppItemID = OI.numoppitemtCode
		OUTER APPLY
		(
			SELECT
				SUM(numUnitHour) AS numUnitHour
			FROM
				OpportunityBizDocs
			LEFT JOIN
				OpportunityBizDocItems 
			ON
				OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
				AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
			WHERE
				OpportunityBizDocs.numOppId = OM.numOppId
				AND OpportunityBizDocs.numBizDocId = 296
		) OBDFulfillment
		WHERE     
			OI.numOppId = @numOppID
		GROUP BY
			OI.numoppitemtCode
			,OI.numUnitHour
			,OBDFulfillment.numUnitHour
	END
	ELSE IF @numBizDocID = 296 -- Fulfillment
	BEGIN
		DECLARE @tintCommitAllocation AS TINYINT
		DECLARE @bitAllocateInventoryOnPickList AS BIT

		SELECT @tintCommitAllocation=ISNULL(tintCommitAllocation,1) FROM Domain WHERE numDomainId=@numDomainID
		SELECT @bitAllocateInventoryOnPickList=ISNULL(bitAllocateInventoryOnPickList,0) FROM OpportunityMaster INNER JOIN DivisionMaster ON OpportunityMaster.numDivisionId=DivisionMaster.numDivisionID WHERE numOppId=@numOppID

		SELECT    
			OI.numoppitemtCode,
			OI.numUnitHour AS QtyOrdered,
			CASE 
				WHEN @numBizDocId = 296 
				THEN
					(CASE 
						WHEN (I.charItemType = 'p' AND ISNULL(OI.bitDropShip, 0) = 0 AND ISNULL(bitAsset,0)=0)
						THEN 
							(CASE 
								WHEN (OI.numUnitHour - ISNULL(SUM(OBI.numUnitHour),0)) <= dbo.GetOrderItemAvailableQtyToShip(OI.numoppitemtCode,OI.numWarehouseItmsID,ISNULL(I.bitKitParent,0),@tintCommitAllocation,@bitAllocateInventoryOnPickList) 
								THEN (OI.numUnitHour - ISNULL(SUM(OBI.numUnitHour),0)) 
								ELSE dbo.GetOrderItemAvailableQtyToShip(OI.numoppitemtCode,OI.numWarehouseItmsID,ISNULL(I.bitKitParent,0),@tintCommitAllocation,@bitAllocateInventoryOnPickList) 
							END)
						ELSE (OI.numUnitHour - ISNULL(SUM(OBI.numUnitHour),0))
					END)
				ELSE
					(OI.numUnitHour - ISNULL(SUM(OBI.numUnitHour),0))
			END AS QtytoFulFillOriginal,
			(OI.numUnitHour - ISNULL(SUM(OBI.numUnitHour),0)) QtyToFulfillWithoutAllocationCheck,
			(CASE 
				WHEN ISNULL(SUM(OBI.numUnitHour),0) >= ISNULL(OBDPackingSlip.numUnitHour,0) 
				THEN (CASE 
						WHEN (I.charItemType = 'p' AND ISNULL(OI.bitDropShip, 0) = 0 AND ISNULL(bitAsset,0)=0)
						THEN 
							(CASE 
								WHEN (OI.numUnitHour - ISNULL(SUM(OBI.numUnitHour),0)) <= dbo.GetOrderItemAvailableQtyToShip(OI.numoppitemtCode,OI.numWarehouseItmsID,ISNULL(I.bitKitParent,0),@tintCommitAllocation,@bitAllocateInventoryOnPickList) 
								THEN (OI.numUnitHour - ISNULL(SUM(OBI.numUnitHour),0)) 
								ELSE dbo.GetOrderItemAvailableQtyToShip(OI.numoppitemtCode,OI.numWarehouseItmsID,ISNULL(I.bitKitParent,0),@tintCommitAllocation,@bitAllocateInventoryOnPickList)
							END)
						ELSE (OI.numUnitHour - ISNULL(SUM(OBI.numUnitHour),0))
					END) 
				ELSE (CASE 
						WHEN (I.charItemType = 'p' AND ISNULL(OI.bitDropShip, 0) = 0 AND ISNULL(bitAsset,0)=0)
						THEN 
							(CASE 
								WHEN (ISNULL(OBDPackingSlip.numUnitHour,0) - ISNULL(SUM(OBI.numUnitHour),0)) <= dbo.GetOrderItemAvailableQtyToShip(OI.numoppitemtCode,OI.numWarehouseItmsID,ISNULL(I.bitKitParent,0),@tintCommitAllocation,@bitAllocateInventoryOnPickList)
								THEN (ISNULL(OBDPackingSlip.numUnitHour,0) - ISNULL(SUM(OBI.numUnitHour),0)) 
								ELSE dbo.GetOrderItemAvailableQtyToShip(OI.numoppitemtCode,OI.numWarehouseItmsID,ISNULL(I.bitKitParent,0),@tintCommitAllocation,@bitAllocateInventoryOnPickList)
							END)
						ELSE ISNULL(OBDPackingSlip.numUnitHour,0) - ISNULL(SUM(OBI.numUnitHour),0)
					END) 
			END) AS QtytoFulFill
		FROM 
			OpportunityMaster OM
		INNER JOIN
			OpportunityItems OI
		ON
			OM.numOppId = OI.numOppId
		INNER JOIN
			Item I
		ON
			OI.numItemCode = I.numItemCode
		LEFT JOIN
			OpportunityBizDocs OBD
		ON
			OM.numOppId = OBD.numOppId
			AND OBD.numBizDocId = @numBizDocID
		LEFT JOIN
			OpportunityBizDocItems OBI
		ON
			OBD.numOppBizDocsId = OBI.numOppBizDocID
			AND OBI.numOppItemID = OI.numoppitemtCode
		OUTER APPLY
		(
			SELECT
				SUM(numUnitHour) AS numUnitHour
			FROM
				OpportunityBizDocs
			LEFT JOIN
				OpportunityBizDocItems 
			ON
				OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
				AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
			WHERE
				OpportunityBizDocs.numOppId = OM.numOppId
				AND OpportunityBizDocs.numBizDocId = 29397
		) OBDPackingSlip
		WHERE     
			OI.numOppId = @numOppID
		GROUP BY
			OI.numoppitemtCode
			,OI.numUnitHour
			,OI.numWarehouseItmsID
			,OI.bitDropShip
			,I.charItemType
			,I.bitAsset
			,I.bitKitParent
			,OBDPackingSlip.numUnitHour
	END
	ELSE IF @numBizDocID = 287 -- Invoice
	BEGIN
		SELECT    
			OI.numoppitemtCode,
			OI.numUnitHour AS QtyOrdered,
			OI.numUnitHour - ISNULL(SUM(OBI.numUnitHour),0) AS QtytoFulFillOriginal,
			(CASE 
				WHEN ISNULL(SUM(OBI.numUnitHour),0) >= ISNULL(OBDFulfillment.numUnitHour,0) 
				THEN OI.numUnitHour - ISNULL(SUM(OBI.numUnitHour),0) 
				ELSE ISNULL(OBDFulfillment.numUnitHour,0) - ISNULL(SUM(OBI.numUnitHour),0) 
			END) AS QtytoFulFill
		FROM 
			OpportunityMaster OM
		INNER JOIN
			OpportunityItems OI
		ON
			OM.numOppId = OI.numOppId
		LEFT JOIN
			OpportunityBizDocs OBD
		ON
			OM.numOppId = OBD.numOppId
			AND OBD.numBizDocId = @numBizDocID
		LEFT JOIN
			OpportunityBizDocItems OBI
		ON
			OBD.numOppBizDocsId = OBI.numOppBizDocID
			AND OBI.numOppItemID = OI.numoppitemtCode
		OUTER APPLY
		(
			SELECT
				SUM(numUnitHour) AS numUnitHour
			FROM
				OpportunityBizDocs
			LEFT JOIN
				OpportunityBizDocItems 
			ON
				OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
				AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
			WHERE
				OpportunityBizDocs.numOppId = OM.numOppId
				AND OpportunityBizDocs.numBizDocId = 296
		) OBDFulfillment
		WHERE     
			OI.numOppId = @numOppID
		GROUP BY
			OI.numoppitemtCode
			,OI.numUnitHour
			,OBDFulfillment.numUnitHour
	END
END
GO
--Created by anoop jayaraj

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetPOItemsForFulfillment')
DROP PROCEDURE USP_GetPOItemsForFulfillment
GO
CREATE PROCEDURE [dbo].[USP_GetPOItemsForFulfillment]
	@numDomainID as numeric(9),
	@numUserCntID numeric(9)=0,                                                                                                               
	@SortChar char(1)='0',                                                            
	@CurrentPage int,                                                              
	@PageSize int,                                                              
	@TotRecs int output,                                                              
	@columnName as Varchar(50),                                                              
	@columnSortOrder as Varchar(10),
	@numRecordID NUMERIC(18,0),
	@FilterBy TINYINT,
	@Filter VARCHAR(300),
	@ClientTimeZoneOffset INT,
	@numVendorInvoiceBizDocID NUMERIC(18,0)
AS
BEGIN
	DECLARE @firstRec AS INTEGER                                                              
	DECLARE @lastRec AS INTEGER                                                     
	SET @firstRec = (@CurrentPage - 1) * @PageSize                             
	SET @lastRec = (@CurrentPage * @PageSize + 1 ) 

	IF LEN(ISNULL(@columnName,''))=0
		SET @columnName = 'Opp.numOppID'
		
	IF LEN(ISNULL(@columnSortOrder,''))=0	
		SET @columnSortOrder = 'DESC'

	CREATE TABLE #tempForm 
	(
		ID INT IDENTITY(1,1), tintOrder TINYINT,vcDbColumnName nvarchar(50),vcOrigDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),vcListItemType varchar(3),
		numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,
		bitIsAlphaNumeric BIT,bitIsNumeric BIT,bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),
		ListRelID numeric(9),intColumnWidth int,bitAllowFiltering BIT,vcFieldDataType CHAR(1)
	)

	-- FIRST CHECK IF GRID COLUMNS ARE CONFIGURED
	DECLARE  @Nocolumns  AS TINYINT = 0
	DECLARE @numFormId INT = 135

	SELECT 
		@Nocolumns=ISNULL(SUM(TotalRow),0) 
	FROM
	(            
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1
		UNION 
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicCustomColumns 
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
	) TotalRows

	-- IF GRID COLUMNS ARE NOT CONFIGURES THEN CHECK IF MASTER FORM CONFIGURATION IS CREATED BY ADMINISTRATOR
	DECLARE @IsMasterConfAvailable BIT = 0
	DECLARE @numUserGroup NUMERIC(18,0)

	IF @Nocolumns=0
	BEGIN
		SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

		IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = @numFormId AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
		BEGIN
			SET @IsMasterConfAvailable = 1
		END
	END

	--IF MASTERCONFIGURATION IS AVAILABLE THEN LOAD IT OTHERWISE LOAD DEFAULT COLUMNS
	IF @IsMasterConfAvailable = 1
	BEGIN
		INSERT INTO DycFormConfigurationDetails 
		(
			numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
		)
		SELECT 
			@numFormId,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,0,1,0,intColumnWidth
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
		UNION
		SELECT 
			@numFormId,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,0,1,1,intColumnWidth
		FROM 
			View_DynamicCustomColumnsMasterConfig
		WHERE 
			View_DynamicCustomColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1


		INSERT INTO 
			#tempForm
		SELECT 
			(intRowNum + 1) as tintOrder, vcDbColumnName,	vcOrigDbColumnName,	ISNULL(vcCultureFieldName,vcFieldName), vcAssociatedControlType,
			vcListItemType, numListID, vcLookBackTableName,	bitCustom, numFieldId, bitAllowSorting,	bitInlineEdit, bitIsRequired,
			bitIsEmail,	bitIsAlphaNumeric, bitIsNumeric, bitIsLengthValidation,	intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,
			ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType 
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName, vcAssociatedControlType,'' as vcListItemType,numListID,'',
			bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,''
		FROM 
			View_DynamicCustomColumnsMasterConfig
		WHERE 
			View_DynamicCustomColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1
		ORDER BY 
			tintOrder ASC  
	END
	ELSE                                        
	BEGIN
		IF @Nocolumns=0
		BEGIN
			INSERT INTO DycFormConfigurationDetails 
			(
				numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
			)
			SELECT 
				@numFormId,numFieldId,0,Row_number() over(order by tintRow desc),@numDomainID,@numUserCntID,0,1,0,intColumnWidth
			FROM 
				View_DynamicDefaultColumns
			WHERE 
				numFormId=@numFormId 
				AND bitDefault=1 
				AND ISNULL(bitSettingField,0)=1 
				AND numDomainID=@numDomainID
			ORDER BY 
				tintOrder asc   
		END

		INSERT INTO 
			#tempForm
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
			vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName,bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,bitFieldMessage,
			vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType  
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND ISNULL(bitSettingField,0)=1 
			AND ISNULL(bitCustom,0)=0  
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName,vcAssociatedControlType,'' as vcListItemType,numListID,'',bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,'' 
		FROM 
			View_DynamicCustomColumns
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND ISNULL(bitCustom,0)=1
		ORDER BY 
			tintOrder asc  
	END

	DECLARE @strColumns VARCHAR(MAX) = ''
    DECLARE @FROM NVARCHAR(MAX) = ''
    DECLARE @WHERE NVARCHAR(MAX) = ''

	SET @FROM =' FROM 
					OpportunityMaster Opp
				INNER JOIN DivisionMaster DM ON Opp.numDivisionId = DM.numDivisionID
				INNER JOIN CompanyInfo cmp ON cmp.numCompanyID=DM.numCompanyID
				INNER JOIN OpportunityItems OI On OI.numOppId=Opp.numOppId
				INNER JOIN Item I on I.numItemCode=OI.numItemCode
				LEFT JOIN WareHouseItems WI on OI.numWarehouseItmsID=WI.numWareHouseItemID
				LEFT JOIN Warehouses W on W.numWarehouseID=WI.numWarehouseID
				LEFT JOIN WarehouseLocation WL on WL.numWLocationID =WI.numWLocationID'

	SET @WHERE = CONCAT(' WHERE 
							tintOppType=2 
							AND tintOppstatus=1
							AND tintShipped=0 
							AND ',(CASE WHEN ISNULL(@numVendorInvoiceBizDocID,0) > 0 THEN 'OBDI.numUnitHour - ISNULL(OBDI.numVendorInvoiceUnitReceived,0) > 0' ELSE 'OI.numUnitHour - ISNULL(OI.numUnitHourReceived,0) > 0' END),'
							AND I.[charItemType] IN (''P'',''N'',''S'') 
							AND (OI.bitDropShip=0 or OI.bitDropShip IS NULL) 
							AND Opp.numDomainID=',@numDomainID)
	
	IF ISNULL(@numRecordID,0) > 0
	BEGIN
		IF @FilterBy=1 --Item
			set	@WHERE=@WHERE + CONCAT(' AND I.numItemCode = ',@numRecordID)
		ELSE IF @FilterBy=2 --Warehouse
			set	@WHERE=@WHERE + CONCAT(' AND WI.numWarehouseID = ',@numRecordID)
		ELSE IF @FilterBy=3 --Purchase Order
			set	@WHERE=@WHERE + CONCAT(' AND Opp.numOppID = ',@numRecordID)
		ELSE IF @FilterBy=4 --Vendor
			set	@WHERE=@WHERE + CONCAT(' AND DM.numDivisionID = ',@numRecordID)	
		ELSE IF @FilterBy=5 --BizDoc
			set	@WHERE=@WHERE + CONCAT(' AND Opp.numOppID in (SELECT numOppId FROM OpportunityBizDocs WHERE numOppBizDocsId = ',@numRecordID,')')
		ELSE IF @FilterBy=6 --SKU
			set	@WHERE=@WHERE + CONCAT(' AND I.numItemCode = ',@numRecordID)
	END
	ELSE IF LEN(ISNULL(@Filter,'')) > 0
	BEGIN
		IF @FilterBy=1 --Item
			set	@WHERE=@WHERE + ' and OI.vcItemName like ''%' + @Filter + '%'''
		ELSE IF @FilterBy=2 --Warehouse
			set	@WHERE=@WHERE + ' and W.vcWareHouse like ''%' + @Filter + '%'''
		ELSE IF @FilterBy=3 --Purchase Order
			set	@WHERE=@WHERE + ' and Opp.vcPOppName like ''%' + @Filter + '%'''
		ELSE IF @FilterBy=4 --Vendor
			set	@WHERE=@WHERE + ' and cmp.vcCompanyName LIKE ''%' + @Filter + '%'''		
		ELSE IF @FilterBy=5 --BizDoc
			set	@WHERE=@WHERE + ' and Opp.numOppID in (SELECT numOppId FROM OpportunityBizDocs WHERE vcBizDocID like ''%' + @Filter + '%'')'
		ELSE IF @FilterBy=6 --SKU
			set	@WHERE=@WHERE + ' and  I.vcSKU LIKE ''%' + @Filter + '%'' OR WI.vcWHSKU LIKE ''%' + @Filter + '%'')'
	END

	IF ISNULL(@numVendorInvoiceBizDocID,0) > 0
	BEGIN
		SET @FROM = @FROM + ' INNER JOIN OpportunityBizDocs OBD ON OBD.numOppID=Opp.numOppID INNER JOIN OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId = OBDI.numOppBizDocID AND OBDI.numOppItemID=OI.numoppitemtCode'
		SET	@WHERE=@WHERE + CONCAT(' AND OBD.numOppBizDocsId = ',@numVendorInvoiceBizDocID)
	END


	DECLARE @strSql NVARCHAR(MAX)
    DECLARE @SELECT NVARCHAR(MAX) = CONCAT('WITH POItems AS 
									(SELECT
										Row_number() OVER(ORDER BY ',@columnName,' ',@columnSortOrder,') AS row 
										,Opp.numDomainID
										,Opp.numOppID
										,ISNULL(Opp.bitStockTransfer,0) bitStockTransfer
										,OI.numoppitemtCode
										,OI.numWarehouseItmsID
										,WI.numWLocationID
										,Opp.numRecOwner
										,DM.tintCRMType
										,ISNULL(DM.numTerID,0) numTerID
										,DM.numDivisionID
										,',(CASE WHEN ISNULL(@numVendorInvoiceBizDocID,0) > 0 THEN 'ISNULL(OBDI.numUnitHour,0)' ELSE 'ISNULL(OI.numUnitHour,0)' END), ' numUnitHour
										,',(CASE WHEN ISNULL(@numVendorInvoiceBizDocID,0) > 0 THEN 'ISNULL(OBDI.numVendorInvoiceUnitReceived,0)' ELSE 'ISNULL(OI.numUnitHourReceived,0)' END), ' numUnitHourReceived
										,ISNULL(Opp.fltExchangeRate,0) AS fltExchangeRate
										,ISNULL(Opp.numCurrencyID,0) AS numCurrencyID
										,ISNULL(I.numIncomeChartAcntId,0) AS itemIncomeAccount
										,ISNULL(I.numAssetChartAcntId,0) AS itemInventoryAsset
										,ISNULL(I.numCOGsChartAcntId,0) AS itemCoGs
										,ISNULL(OI.bitDropShip,0) AS DropShip
										,I.charItemType
										,OI.vcType ItemType
										,ISNULL(OI.numProjectID, 0) numProjectID
										,ISNULL(OI.numClassID, 0) numClassID
										,OI.numItemCode
										,OI.monPrice
										,CONCAT(I.vcItemName,CASE WHEN OI.vcItemName <> I.vcItemName THEN CONCAT('' ('',''Changed in order to: '',OI.vcItemName,'')'') ELSE '''' END) AS vcItemName
										,vcPOppName
										,ISNULL(Opp.bitPPVariance,0) AS bitPPVariance
										,ISNULL(I.bitLotNo,0) as bitLotNo
										,ISNULL(I.bitSerialized,0) AS bitSerialized
										,cmp.vcCompanyName')

	DECLARE @tintOrder  AS TINYINT;SET @tintOrder = 0
	DECLARE @vcFieldName  AS VARCHAR(50)
	DECLARE @vcListItemType  AS VARCHAR(3)
	DECLARE @vcListItemType1  AS VARCHAR(1)
	DECLARE @vcAssociatedControlType VARCHAR(30)
	DECLARE @numListID  AS NUMERIC(9)
	DECLARE @vcDbColumnName VARCHAR(40)
	DECLARE @vcLookBackTableName VARCHAR(2000)
	DECLARE @bitCustom  AS BIT
	Declare @numFieldId as numeric  
	DECLARE @bitAllowSorting AS CHAR(1)   
	DECLARE @bitAllowEdit AS CHAR(1)                 
	DECLARE @vcColumnName AS VARCHAR(500)
	DECLARE @ListRelID AS NUMERIC(9) 

	DECLARE @i AS INT = 1
	DECLARE @iCount AS INT 
	SELECT @iCount = COUNT(*) FROM #tempForm

	WHILE @i <= @iCount
	BEGIN
		SELECT 
			@tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,@vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,
			@numListID=numListID,@vcLookBackTableName=vcLookBackTableName ,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,
			@ListRelID=ListRelID
		FROM 
			#tempForm 
		WHERE
			ID=@i

		IF @bitCustom = 0
		BEGIN
			DECLARE  @Prefix  AS VARCHAR(5)

			IF @vcLookBackTableName = 'AdditionalContactsInformation'
				SET @Prefix = 'ADC.'
			IF @vcLookBackTableName = 'DivisionMaster'
				SET @Prefix = 'DM.'
			IF @vcLookBackTableName = 'OpportunityMaster'
				SET @PreFix = 'Opp.'
			IF @vcLookBackTableName = 'OpportunityItems'
				SET @PreFix = 'OI.'
			IF @vcLookBackTableName = 'Item'
				SET @PreFix = 'I.'
			IF @vcLookBackTableName = 'WareHouseItems'
				SET @PreFix = 'WI.'
			IF @vcLookBackTableName = 'Warehouses'
				SET @PreFix = 'W.'
			IF @vcLookBackTableName = 'WarehouseLocation'
				SET @PreFix = 'WL.'
			IF @vcLookBackTableName = 'CompanyInfo'
				SET @PreFix = 'cmp.'

			SET @vcColumnName= @vcDbColumnName+'~'+ convert(varchar(10),@numFieldId) +'~0'

			IF @vcAssociatedControlType = 'SelectBox'
			BEGIN
				IF @vcDbColumnName = 'numPartner'
				BEGIN
					SET @strColumns=@strColumns+ ' ,(SELECT D.vcPartnerCode+''-''+C.vcCompanyName FROM DivisionMaster AS D LEFT JOIN CompanyInfo AS C ON D.numCompanyID=C.numCompanyID WHERE D.numDivisionID=Opp.numPartner) '+' ['+ @vcColumnName+']'    
					SET @strColumns=@strColumns+ ' ,(SELECT D.vcPartnerCode+''-''+C.vcCompanyName FROM DivisionMaster AS D LEFT JOIN CompanyInfo AS C ON D.numCompanyID=C.numCompanyID WHERE D.numDivisionID=Opp.numPartner) AS vcPartner' 
				END
				ELSE IF @vcDbColumnName = 'tintSource'
				BEGIN
					SET @strColumns = @strColumns + ',ISNULL(dbo.fn_GetOpportunitySourceValue(ISNULL(Opp.tintSource,0),ISNULL(Opp.tintSourceType,0),Opp.numDomainID),''Internal Order'')' + ' [' + @vcColumnName + ']'
				END
				ELSE IF @vcDbColumnName = 'numContactID'
				BEGIN
					SET @strColumns = @strColumns + ',dbo.fn_GetContactName(' + @Prefix + @vcDbColumnName + ') [' + @vcColumnName + ']'
				END
				ELSE IF @vcDbColumnName = 'vcLocation'
				BEGIN
					SET @strColumns = @strColumns + ',CONCAT(W.vcWarehouse,CASE WHEN LEN(ISNULL(WL.vcLocation,'''')) > 0 THEN CONCAT('' ('',WL.vcLocation,'')'') ELSE '''' END) [' + @vcColumnName + ']'
				END
				ELSE IF @vcListItemType = 'LI'
				BEGIN
					SET @strColumns = @strColumns + ',L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData' + ' [' + @vcColumnName + ']'
					SET @FROM = @FROM + ' LEFT JOIN ListDetails L' + CONVERT(VARCHAR(3),@tintOrder) + ' on L' + CONVERT(VARCHAR(3),@tintOrder) + '.numListItemID=' + @vcDbColumnName
				END
				ELSE IF @vcListItemType = 'U'
				BEGIN
					SET @strColumns = @strColumns + ',dbo.fn_GetContactName(' + @Prefix + @vcDbColumnName + ') [' + @vcColumnName + ']'
				END
				ELSE IF @vcListItemType = 'SGT'
				BEGIN
					SET @strColumns = @strColumns + ',(CASE ' + @Prefix + @vcDbColumnName + ' 
														WHEN 0 THEN ''Service Default''
														WHEN 1 THEN ''Adult Signature Required''
														WHEN 2 THEN ''Direct Signature''
														WHEN 3 THEN ''InDirect Signature''
														WHEN 4 THEN ''No Signature Required''
														ELSE ''''
														END) [' + @vcColumnName + ']'
				END
				ELSE IF @vcListItemType = 'PSS'
				BEGIN
					SET @strColumns = @strColumns + ',(CASE ' + @Prefix + @vcDbColumnName + ' 
														WHEN 10 THEN ''FedEx Priority Overnight''
														WHEN 11 THEN ''FedEx Standard Overnight''
														WHEN 12 THEN ''FedEx Overnight''
														WHEN 13 THEN ''FedEx 2nd Day''
														WHEN 14 THEN ''FedEx Express Saver''
														WHEN 15 THEN ''FedEx Ground''
														WHEN 16 THEN ''FedEx Ground Home Delivery''
														WHEN 17 THEN ''FedEx 1 Day Freight''
														WHEN 18 THEN ''FedEx 2 Day Freight''
														WHEN 19 THEN ''FedEx 3 Day Freight''
														WHEN 20 THEN ''FedEx International Priority''
														WHEN 21 THEN ''FedEx International Priority Distribution''
														WHEN 22 THEN ''FedEx International Economy''
														WHEN 23 THEN ''FedEx International Economy Distribution''
														WHEN 24 THEN ''FedEx International First''
														WHEN 25 THEN ''FedEx International Priority Freight''
														WHEN 26 THEN ''FedEx International Economy Freight''
														WHEN 27 THEN ''FedEx International Distribution Freight''
														WHEN 28 THEN ''FedEx Europe International Priority''
														WHEN 40 THEN ''UPS Next Day Air''
														WHEN 42 THEN ''UPS 2nd Day Air''
														WHEN 43 THEN ''UPS Ground''
														WHEN 48 THEN ''UPS 3Day Select''
														WHEN 49 THEN ''UPS Next Day Air Saver''
														WHEN 50 THEN ''UPS Saver''
														WHEN 51 THEN ''UPS Next Day Air Early A.M.''
														WHEN 55 THEN ''UPS 2nd Day Air AM''
														WHEN 70 THEN ''USPS Express''
														WHEN 71 THEN ''USPS First Class''
														WHEN 72 THEN ''USPS Priority''
														WHEN 73 THEN ''USPS Parcel Post''
														WHEN 74 THEN ''USPS Bound Printed Matter''
														WHEN 75 THEN ''USPS Media''
														WHEN 76 THEN ''USPS Library''
														ELSE ''''
														END) [' + @vcColumnName + ']'
						
				END
            END
			ELSE IF @vcAssociatedControlType = 'DateField'
			BEGIN
				IF @vcDbColumnName = 'dtReleaseDate'
				BEGIN
					SET @strColumns = @strColumns
                                + ',case when convert(varchar(11),'
                                + @Prefix
                                + @vcDbColumnName
                                + ')= convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getdate())) then ''<b><font color=red>Today</font></b>'''
					SET @strColumns = @strColumns
									+ 'when convert(varchar(11),'
									+ @Prefix
									+ @vcDbColumnName
									+ ')= convert(varchar(11),dateadd(day,-1,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getdate()))) then ''<b><font color=purple>Yesterday</font></b>'''
					SET @strColumns = @strColumns
									+ 'when convert(varchar(11),'
									+ @Prefix
									+ @vcDbColumnName
									+ ')= convert(varchar(11),dateadd(day,1,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getdate()))) then''<b><font color=orange>Tommorow</font></b>'' '
					SET @strColumns = @strColumns
									+ 'else dbo.FormatedDateFromDate('
									+ @Prefix
									+ @vcDbColumnName
									+ ','
									+ CONVERT(VARCHAR(10),@numDomainId)
									+ ') end  ['
									+ @vcColumnName + ']'
				END
				ELSE
				BEGIN
					SET @strColumns = @strColumns
                                + ',case when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','
                                + @Prefix
                                + @vcDbColumnName
                                + ') )= convert(varchar(11),getdate()) then ''<b><font color=red>Today</font></b>'''
					SET @strColumns = @strColumns
									+ 'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','
									+ @Prefix
									+ @vcDbColumnName
									+ ') )= convert(varchar(11),dateadd(day,-1,getdate())) then ''<b><font color=purple>Yesterday</font></b>'''
					SET @strColumns = @strColumns
									+ 'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','
									+ @Prefix
									+ @vcDbColumnName
									+ ') )= convert(varchar(11),dateadd(day,1,getdate())) then''<b><font color=orange>Tommorow</font></b>'' '
					SET @strColumns = @strColumns
									+ 'else dbo.FormatedDateFromDate('
									+ @Prefix
									+ @vcDbColumnName
									+ ','
									+ CONVERT(VARCHAR(10),@numDomainId)
									+ ') end  ['
									+ @vcColumnName + ']'
				 END	
            END
            ELSE IF @vcAssociatedControlType = 'TextBox' OR @vcAssociatedControlType = 'TextArea'
            BEGIN
				IF @vcDbColumnName = 'numShipState'
				BEGIN
					SET @strColumns = @strColumns + ',dbo.fn_getOPPAddressState(Opp.numOppId,Opp.numDomainID,2)' + ' [' + @vcColumnName + ']'
				END
				ELSE IF @vcDbColumnName = 'numQtyToShipReceive'
				BEGIN
					IF ISNULL(@numVendorInvoiceBizDocID,0) > 0
					BEGIN
						SET @strColumns = @strColumns + ',ISNULL(OBDI.numUnitHour,0) - ISNULL(OBDI.numVendorInvoiceUnitReceived,0)' + ' [' + @vcColumnName + ']'
					END
					ELSE
					BEGIN
						SET @strColumns = @strColumns + ',ISNULL(OI.numUnitHour,0) - ISNULL(OI.numUnitHourReceived,0)' + ' [' + @vcColumnName + ']'
					END
				END
				ELSE IF @vcDbColumnName = 'vcPathForTImage'
		   		BEGIN
					SET @strColumns = @strColumns + ',ISNULL(II.vcPathForTImage,'''') AS ' + ' ['+ @vcColumnName+']'                  
					SET @FROM = @FROM + ' LEFT JOIN ItemImages II ON II.numItemCode = I.numItemCode AND II.bitDefault = 1'
				END
				ELSE IF @vcDbColumnName = 'SerialLotNo'
				BEGIN
					SET @strColumns = @strColumns + ',SUBSTRING((SELECT '','' + vcSerialNo + CASE WHEN isnull(I.bitLotNo,0)=1 THEN '' ('' + CONVERT(VARCHAR(15),whi.numQty) + '')'' ELSE '''' end 
							FROM OppWarehouseSerializedItem oppI JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID=whi.numWareHouseItmsDTLID where oppI.numOppID=OI.numOppId and oppI.numOppItemID=OI.numoppitemtCode 
							ORDER BY vcSerialNo FOR XML PATH('''')),2,200000)  AS ' + ' ['+ @vcColumnName+']' 
				END
				ELSE IF @vcDbColumnName = 'numUnitHour'
				BEGIN
					IF ISNULL(@numVendorInvoiceBizDocID,0) > 0
					BEGIN
						SET @strColumns = @strColumns + ',ISNULL(OBDI.numUnitHour,0) [' + @vcColumnName + ']'
					END
					ELSE
					BEGIN
						SET @strColumns = @strColumns + ',ISNULL(' + @Prefix + @vcDbColumnName + ',0) [' + @vcColumnName + ']'
					END
				END
				ELSE IF @vcDbColumnName = 'numUnitHourReceived'
				BEGIN
					IF ISNULL(@numVendorInvoiceBizDocID,0) > 0
					BEGIN
						SET @strColumns = @strColumns + ',ISNULL(OBDI.numVendorInvoiceUnitReceived,0) [' + @vcColumnName + ']'
					END
					ELSE
					BEGIN
						SET @strColumns = @strColumns + ',ISNULL(' + @Prefix + @vcDbColumnName + ',0) [' + @vcColumnName + ']'
					END
				END
				ELSE IF @vcDbColumnName = 'vcSKU'
				BEGIN
					SET @strColumns = @strColumns + ',ISNULL(OI.vcSKU,ISNULL(I.vcSKU,'''')) [' + @vcColumnName + ']'
				END
				ELSE IF @vcDbColumnName = 'vcItemName'
				BEGIN
					SET @strColumns = @strColumns + ',CONCAT(I.vcItemName,CASE WHEN OI.vcItemName <> I.vcItemName THEN CONCAT('' ('',''Changed in order to: '',OI.vcItemName,'')'') ELSE '''' END)  [' + @vcColumnName + ']'
				END
				ELSE
				BEGIN
					SET @strColumns = @strColumns + ',' + @Prefix + @vcDbColumnName + ' [' + @vcColumnName + ']'
				END
            END
            ELSE IF  @vcAssociatedControlType='Label'                                              
			BEGIN
				IF @vcDbColumnName = 'numRemainingQty'
				BEGIN
					IF ISNULL(@numVendorInvoiceBizDocID,0) > 0
					BEGIN
						SET @strColumns = @strColumns + ',ISNULL(OBDI.numUnitHour,0) - ISNULL(OBDI.numVendorInvoiceUnitReceived,0)' + ' [' + @vcColumnName + ']'
					END
					ELSE
					BEGIN
						SET @strColumns = @strColumns + ',ISNULL(OI.numUnitHour,0) - ISNULL(OI.numUnitHourReceived,0)' + ' [' + @vcColumnName + ']'
					END
				END
				ELSE
				BEGIN
					SET @strColumns=@strColumns + ',' + @Prefix + @vcDbColumnName +' ['+ @vcColumnName+']'        
				END
			END
        END
		ELSE
        BEGIN
			SET @vcColumnName= @vcDbColumnName +'~'+ convert(varchar(10),@numFieldId)+'~1'

			IF @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'
            BEGIN
              SET @strColumns = @strColumns
                              + ',CFW'
                              + CONVERT(VARCHAR(3),@tintOrder)
                              + '.Fld_Value  ['
                              + @vcColumnName + ']'
              SET @FROM = @FROM
                                      + ' left Join CFW_FLD_Values_Opp CFW'
                                      + CONVERT(VARCHAR(3),@tintOrder)
                                      + ' on CFW'
                                      + CONVERT(VARCHAR(3),@tintOrder)
                                      + '.Fld_Id='
                                      + CONVERT(VARCHAR(10),@numFieldId)
                                      + 'and CFW'
                                      + CONVERT(VARCHAR(3),@tintOrder)
                                      + '.RecId=Opp.numOppid   '
			END
			ELSE
            IF @vcAssociatedControlType = 'CheckBox' 
            BEGIN
                SET @strColumns = @strColumns
                                + ',case when isnull(CFW'
                                + CONVERT(VARCHAR(3),@tintOrder)
                                + '.Fld_Value,0)=0 then 0 when isnull(CFW'
                                + CONVERT(VARCHAR(3),@tintOrder)
                                + '.Fld_Value,0)=1 then 1 end   ['
                                + @vcColumnName + ']'
                SET @FROM = @FROM
                                        + ' left Join CFW_FLD_Values_Opp CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + ' on CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.Fld_Id='
                                        + CONVERT(VARCHAR(10),@numFieldId)
                                        + 'and CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.RecId=Opp.numOppid   '
            END
            ELSE
            IF @vcAssociatedControlType = 'DateField'
            BEGIN
                  SET @strColumns = @strColumns
                                  + ',dbo.FormatedDateFromDate(CFW'
                                  + CONVERT(VARCHAR(3),@tintOrder)
                                  + '.Fld_Value,'
                                  + CONVERT(VARCHAR(10),@numDomainId)
                                  + ')  ['
                                  + @vcColumnName + ']'
                  SET @FROM = @FROM
                                          + ' left Join CFW_FLD_Values_Opp CFW'
                                          + CONVERT(VARCHAR(3),@tintOrder)
                                          + ' on CFW'
                                          + CONVERT(VARCHAR(3),@tintOrder)
                                          + '.Fld_Id='
                                          + CONVERT(VARCHAR(10),@numFieldId)
                                          + 'and CFW'
                                          + CONVERT(VARCHAR(3),@tintOrder)
                                          + '.RecId=Opp.numOppid    '
            END
            ELSE
            IF @vcAssociatedControlType = 'SelectBox'
            BEGIN
                SET @vcDbColumnName = 'DCust'
                                            + CONVERT(VARCHAR(10),@numFieldId)
                SET @strColumns = @strColumns
                                + ',L'
                                + CONVERT(VARCHAR(3),@tintOrder)
                                + '.vcData'
                                + ' ['
                                + @vcColumnName + ']'

                SET @FROM = @FROM
                                        + ' left Join CFW_FLD_Values_Opp CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + ' on CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.Fld_Id='
                                        + CONVERT(VARCHAR(10),@numFieldId)
                                        + 'and CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.RecId=Opp.numOppid '

                SET @FROM = @FROM
                                        + ' left Join ListDetails L'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + ' on L'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.numListItemID=CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.Fld_Value'
            END
			ELSE
			BEGIN
				SET @strColumns = @strColumns + CONCAT(',dbo.GetCustFldValueOpp(',@numFieldId,',opp.numOppID)') + ' [' + @vcColumnName + ']'
			END
		END

		SET @i = @i + 1
	END

	SET @strSql = @SELECT + @strColumns + @FROM + @WHERE + ') SELECT * INTO #tempTable FROM [POItems] WHERE row > ' + CONVERT(VARCHAR(10), @firstRec) + ' and row <' + CONVERT(VARCHAR(10), @lastRec)

	PRINT CAST(@strSql AS NTEXT)
	SET @strSql = @strSql + '; (SELECT @TotRecs =COUNT(*) ' + @FROM + @WHERE + ')' + '; SELECT * FROM #tempTable; DROP TABLE #tempTable;'

	EXEC sp_executesql 
        @query = @strSql, 
        @params = N'@TotRecs INT OUTPUT', 
        @TotRecs = @TotRecs OUTPUT 

	SELECT * FROM #tempForm
	DROP TABLE #tempForm
END
GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetReceivePayment' ) 
    DROP PROCEDURE USP_GetReceivePayment
GO
CREATE PROCEDURE [dbo].USP_GetReceivePayment
    @numDomainId AS NUMERIC(9) = 0 ,
    @numDepositID NUMERIC = 0,
    @numDivisionID NUMERIC(9) = 0,
    @numCurrencyID numeric(9),
    @numAccountClass NUMERIC(18,0)=0,
	@numCurrentPage INT=1
AS 
BEGIN
	DECLARE @numPageSize INT = 40

		
	DECLARE @numDiscountItemID AS NUMERIC(18,0)
	DECLARE @numShippingServiceItemID AS NUMERIC(18,0)

	SELECT @numDiscountItemID = ISNULL(numDiscountServiceItemID,0),@numShippingServiceItemID=ISNULL(numShippingServiceItemID,0) FROM dbo.Domain WHERE numDomainID = @numDomainID
	PRINT @numDiscountItemID
			
	DECLARE @BaseCurrencySymbol 
	nVARCHAR(3);SET @BaseCurrencySymbol='$'
	SELECT @BaseCurrencySymbol = ISNULL(varCurrSymbol,'$') FROM dbo.Currency WHERE numCurrencyID IN ( SELECT numCurrencyID FROM dbo.Domain WHERE numDomainId=@numDomainId)
    
	SELECT [T].[numDivisionID],T.bitChild,dbo.[fn_GetComapnyName](T.[numDivisionID]) AS vcCompanyName,AD2.[vcCity]
	,isnull(dbo.fn_GetState(AD2.numState),'') AS vcState
	INTO #tempOrg
	FROM (SELECT @numDivisionID AS numDivisionID,cast(0 AS BIT) AS bitChild
	UNION
	SELECT [CA].[numDivisionID],CAST(1 AS BIT) AS bitChild
	FROM [dbo].[CompanyAssociations] AS CA 
	WHERE [CA].[numDomainID]=@numDomainID AND  ca.[numAssociateFromDivisionID]=@numDivisionID AND CA.[bitChildOrg]=1) AS T
	LEFT JOIN dbo.AddressDetails AD2 ON AD2.numDomainID=@numDomainID 
	AND AD2.numRecordID= T.numDivisionID AND AD2.tintAddressOf=2 AND AD2.tintAddressType=2 AND AD2.bitIsPrimary=1

	DECLARE @TEMP TABLE
	(
		ID INT IDENTITY(1,1)
		,numOppBizDocID NUMERIC(18,0)
	)

	INSERT INTO @TEMP
	(
		numOppBizDocID
	)
	SELECT 
		numOppBizDocsID
	FROM
	(
		SELECT
			OpportunityBizdocs.numOppBizDocsID
			,T.bitChild
			,OpportunityBizdocs.dtCreatedDate
		FROM    
			OpportunityMaster
		INNER JOIN 
			[#tempOrg] AS T 
		ON 
			OpportunityMaster.[numDivisionId] = T.[numDivisionID]
		INNER JOIN 
			OpportunityBizdocs
		ON
			OpportunityMaster.[numoppid] = OpportunityBizdocs.[numoppid]
		INNER JOIN
			DepositeDetails 
		ON 
			DepositeDetails.numOppBizDocsID = OpportunityBizdocs.numOppBizDocsID
		WHERE   
			OpportunityBizdocs.[bitauthoritativebizdocs] = 1
			AND OpportunityMaster.tintOppType = 1
			AND OpportunityMaster.numDomainID = @numDomainID
			AND DepositeDetails.numDepositID = @numDepositID
			AND (ISNULL(OpportunityMaster.numAccountClass,0)=@numAccountClass OR @numAccountClass=0)
		UNION
		SELECT
			numOppBizDocsID
			,T.bitChild
			,OpportunityBizdocs.dtCreatedDate
		FROM 
			OpportunityMaster
		INNER JOIN
			OpportunityBizdocs 
		ON 
			OpportunityMaster.[numoppid] = OpportunityBizdocs.[numoppid]
		INNER JOIN 
			[#tempOrg] AS T 
		ON 
			OpportunityMaster.[numDivisionId] = T.[numDivisionID]
		WHERE   
			OpportunityBizdocs.[bitauthoritativebizdocs] = 1
			AND OpportunityMaster.tintOppType = 1
			AND OpportunityMaster.numDomainID = @numDomainID
			AND (@numDepositID= 0 OR (@numDepositID > 0 AND T.bitChild = 0))
			AND OpportunityBizdocs.monDealAmount - OpportunityBizdocs.monAmountPaid > 0
			AND (SELECT COUNT(*) FROM DepositeDetails WHERE numOppID=OpportunityMaster.numOppId AND numOppBizDocsId=OpportunityBizdocs.numOppBizDocsID AND numDepositID=@numDepositID) = 0
			AND OpportunityBizdocs.numOppBizDocsId NOT IN ( SELECT numDocID FROM dbo.DocumentWorkflow D WHERE D.cDocType='B' AND ISNULL(tintApprove,0) = 0 /*discard invoice which are Pending to approve */ )
			AND (OpportunityMaster.numCurrencyID = @numCurrencyID OR  @numCurrencyID = 0 )
			AND (ISNULL(OpportunityMaster.numAccountClass,0)=@numAccountClass OR @numAccountClass=0)
	) TEMP
	ORDER BY 
		bitChild,dtCreatedDate DESC


	DECLARE @TEMPFinal TABLE
	(
		numOppBizDocID NUMERIC(18,0)
	)

	INSERT INTO @TEMPFinal SELECT numOppBizDocID FROM @TEMP ORDER BY ID OFFSET (@numCurrentPage - 1) * @numPageSize ROWS FETCH NEXT @numPageSize ROWS ONLY


	SELECT  
		om.vcPOppName ,
		om.numoppid ,
		OBD.[numoppbizdocsid] ,
		OBD.[vcbizdocid] ,
		OBD.[numbizdocstatus] ,
		OBD.[vccomments] ,
		OBD.monAmountPaid AS monAmountPaid ,
		OBD.monDealAmount ,
		OBD.monCreditAmount monCreditAmount ,
		ISNULL(C.varCurrSymbol, '') varCurrSymbol ,
		[dbo].[FormatedDateFromDate](dtFromDate, @numDomainID) dtFromDate ,
		dtFromDate AS [dtOrigFromDate],
		CASE ISNULL(om.bitBillingTerms, 0)
			WHEN 1
			--THEN [dbo].[FormatedDateFromDate](DATEADD(dd,CONVERT(INT, ISNULL(dbo.fn_GetListItemName(ISNULL(om.intBillingDays,0)), 0)), dtFromDate), 1)
			THEN [dbo].[FormatedDateFromDate](DATEADD(dd,CONVERT(INT, ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(om.intBillingDays,0)), 0)), dtFromDate), @numDomainID)
			WHEN 0
			THEN [dbo].[FormatedDateFromDate](OBD.[dtFromDate], @numDomainID)
		END AS dtDueDate ,
		om.numDivisionID ,
                
		(OBD.monDealAmount - ISNULL(OBD.monCreditAmount, 0) - ISNULL( OBD.monAmountPaid , 0) + ISNULL(DD.monAmountPaid,0)  ) AS baldue ,
		T.vcCompanyName ,T.vcCity,T.vcState,T.bitChild,
		DD.numDepositID ,
		DD.monAmountPaid monAmountPaidInDeposite ,
		ISNULL(DD.numDepositeDetailID,0) numDepositeDetailID,
		obd.dtCreatedDate,
		OM.numContactId,obd.vcRefOrderNo
		,OM.numCurrencyID
		,ISNULL(NULLIF(C.fltExchangeRate,0),1) fltExchangeRateCurrent
		,ISNULL(NULLIF(OM.fltExchangeRate,0),1) fltExchangeRateOfOrder
		,@BaseCurrencySymbol BaseCurrencySymbol,om.vcPOppName,
		CASE ISNULL(om.bitBillingTerms, 0)
			WHEN 1 THEN (SELECT numDiscount FROM dbo.BillingTerms WHERE om.intBillingDays = numTermsID AND numDomainID = om.numDomainID)
			ELSE 0
		END AS [numDiscount],
		CASE ISNULL(om.bitBillingTerms, 0)
			WHEN 1 THEN (SELECT numDiscountPaidInDays FROM dbo.BillingTerms WHERE om.intBillingDays = numTermsID AND numDomainID = om.numDomainID)
			ELSE 0
		END AS [numDiscountPaidInDays],
		CASE ISNULL(om.bitBillingTerms, 0)
			WHEN 1 THEN (SELECT numNetDueInDays FROM dbo.BillingTerms WHERE om.intBillingDays = numTermsID AND numDomainID = om.numDomainID)
			ELSE 0
		END AS [numNetDueInDays],
		CASE WHEN (SELECT COUNT(*) FROM dbo.OpportunityBizDocItems OBDI
					JOIN dbo.OpportunityItems OI ON OI.numoppitemtCode = OBDI.numOppItemID
					WHERE 1=1 AND numOppId = om.[numoppid] AND numOppBizDocID = obd.numOppBizDocsId 
					AND OBDI.numItemCode = @numDiscountItemID
					AND OI.vcItemDesc LIKE '%Term Discount%') > 0 THEN 1 ELSE 0 END AS [IsDiscountItemAdded],
		(SELECT ISNULL(SUM(monTotAmount),0) FROM OpportunityBizDocItems WHERE numOppBizDocID=OBD.numOppBizDocsID AND numItemCode = @numShippingServiceItemID) AS monShippingCharge
		--(SELECT numDiscount FROM dbo.BillingTerms WHERE om.intBillingDays = numTermsID AND numDomainID = om.numDomainID)	
	FROM    opportunitymaster om
			INNER JOIN [opportunitybizdocs] obd ON om.[numoppid] = OBD.[numoppid]
			INNER JOIN @TEMPFinal T1 ON obd.numOppBizDocsId = T1.numOppBizDocID
			INNER JOIN [#tempOrg] AS T ON OM.[numDivisionId] = T.[numDivisionID]
			LEFT OUTER JOIN [ListDetails] ld ON ld.[numListItemID] = OBD.[numbizdocstatus]
			LEFT OUTER JOIN [Currency] C ON C.numCurrencyID = om.numCurrencyID
			JOIN DepositeDetails DD ON DD.numOppBizDocsID = OBD.numOppBizDocsID
	WHERE   
		OBD.[bitauthoritativebizdocs] = 1
		AND om.tintOppType = 1
		AND om.numDomainID = @numDomainID
		AND DD.numDepositID = @numDepositID
		AND (ISNULL(OM.numAccountClass,0)=@numAccountClass OR @numAccountClass=0)
	UNION
	SELECT  om.vcPOppName ,
			om.numoppid ,
			OBD.[numoppbizdocsid] ,
			OBD.[vcbizdocid] ,
			OBD.[numbizdocstatus] ,
			OBD.[vccomments] ,
			OBD.monAmountPaid AS monAmountPaid ,
			OBD.monDealAmount ,
			OBD.monCreditAmount monCreditAmount ,
			ISNULL(C.varCurrSymbol, '') varCurrSymbol ,
			[dbo].[FormatedDateFromDate](dtFromDate, @numDomainID) dtFromDate ,
			dtFromDate AS [dtOrigFromDate],
			CASE ISNULL(om.bitBillingTerms, 0)
				WHEN 1
				--THEN [dbo].[FormatedDateFromDate](DATEADD(dd,CONVERT(INT, ISNULL(dbo.fn_GetListItemName(ISNULL(om.intBillingDays,0)), 0)),dtFromDate), @numDomainID)
				THEN [dbo].[FormatedDateFromDate](DATEADD(dd,CONVERT(INT, ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(om.intBillingDays,0)), 0)),dtFromDate), @numDomainID)
				WHEN 0
				THEN [dbo].[FormatedDateFromDate](OBD.[dtFromDate], @numDomainID)
			END AS dtDueDate ,
			om.numDivisionID ,
			(OBD.monDealAmount - ISNULL(monCreditAmount, 0) - ISNULL( OBD.monAmountPaid , 0) ) AS baldue ,
			T.vcCompanyName ,T.vcCity,T.vcState,T.bitChild,
			0 numDepositID ,
			0 monAmountPaidInDeposite ,
			0 numDepositeDetailID,
			dtCreatedDate,
			OM.numContactId,obd.vcRefOrderNo
			,OM.numCurrencyID
			,ISNULL(NULLIF(C.fltExchangeRate,0),1) fltExchangeRateCurrent
			,ISNULL(NULLIF(OM.fltExchangeRate,0),1) fltExchangeRateOfOrder
			,@BaseCurrencySymbol BaseCurrencySymbol,om.vcPOppName,
			CASE ISNULL(om.bitBillingTerms, 0)
				WHEN 1 THEN (SELECT numDiscount FROM dbo.BillingTerms WHERE om.intBillingDays = numTermsID AND numDomainID = om.numDomainID)
				ELSE 0
			END AS [numDiscount],
			CASE ISNULL(om.bitBillingTerms, 0)
				WHEN 1 THEN (SELECT numDiscountPaidInDays FROM dbo.BillingTerms WHERE om.intBillingDays = numTermsID AND numDomainID = om.numDomainID)
				ELSE 0
			END AS [numDiscountPaidInDays],
			CASE ISNULL(om.bitBillingTerms, 0)
				WHEN 1 THEN (SELECT numNetDueInDays FROM dbo.BillingTerms WHERE om.intBillingDays = numTermsID AND numDomainID = om.numDomainID)
				ELSE 0
			END AS [numNetDueInDays],
			CASE WHEN (SELECT COUNT(*) FROM dbo.OpportunityBizDocItems OBDI
						JOIN dbo.OpportunityItems OI ON OI.numoppitemtCode = OBDI.numOppItemID
						WHERE 1=1 AND numOppId = om.[numoppid] AND numOppBizDocID = obd.numOppBizDocsId 
						AND OBDI.numItemCode = @numDiscountItemID
						AND OI.vcItemDesc LIKE '%Term Discount%') > 0 THEN 1 ELSE 0 END AS [IsDiscountItemAdded],
			(SELECT ISNULL(SUM(monTotAmount),0) FROM OpportunityBizDocItems WHERE numOppBizDocID=OBD.numOppBizDocsID AND numItemCode = @numShippingServiceItemID) AS monShippingCharge
	FROM    opportunitymaster om
			INNER JOIN [opportunitybizdocs] obd ON om.[numoppid] = OBD.[numoppid]
			INNER JOIN @TEMPFinal T1 ON obd.numOppBizDocsId = T1.numOppBizDocID
			INNER JOIN [#tempOrg] AS T ON OM.[numDivisionId] = T.[numDivisionID]
			LEFT OUTER JOIN [ListDetails] ld ON ld.[numListItemID] = OBD.[numbizdocstatus]
			LEFT OUTER JOIN [Currency] C ON C.numCurrencyID = om.numCurrencyID
	WHERE   
		OBD.[bitauthoritativebizdocs] = 1
		AND om.tintOppType = 1
		AND om.numDomainID = @numDomainID
		AND (@numDepositID= 0 OR (@numDepositID > 0 AND T.bitChild = 0))
		AND OBD.monDealAmount - OBD.monAmountPaid > 0
		AND (SELECT COUNT(*) FROM DepositeDetails WHERE numOppID=OM.numOppId AND numOppBizDocsId=OBD.numOppBizDocsID AND numDepositID=@numDepositID) = 0
		AND OBD.numOppBizDocsId NOT IN ( SELECT numDocID FROM dbo.DocumentWorkflow D WHERE D.cDocType='B' AND ISNULL(tintApprove,0) = 0 /*discard invoice which are Pending to approve */ )
		AND (OM.numCurrencyID = @numCurrencyID OR  @numCurrencyID = 0 )
		AND (ISNULL(OM.numAccountClass,0)=@numAccountClass OR @numAccountClass=0)      
	ORDER BY 
		T.bitChild,dtCreatedDate DESC
	
	SELECT COUNT(*) AS TotalRecords,@numPageSize AS PageSize FROM @TEMP

	DROP TABLE #tempOrg
END


/****** Object:  StoredProcedure [dbo].[usp_GetSalesFulfillmentOrder]    Script Date: 03/06/2009 00:39:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetSalesFulfillmentOrder')
DROP PROCEDURE USP_GetSalesFulfillmentOrder
GO
CREATE PROCEDURE [dbo].[USP_GetSalesFulfillmentOrder]
(
    @numDomainId AS NUMERIC(9) = 0,
    @SortCol VARCHAR(50),
    @SortDirection VARCHAR(4),
    @CurrentPage INT,
    @PageSize INT,
    @TotRecs INT OUTPUT,
    @numUserCntID NUMERIC,
    @vcBizDocStatus VARCHAR(500),
    @vcBizDocType varchar(500),
    @numDivisionID as numeric(9)=0,
    @ClientTimeZoneOffset INT=0,
    @bitIncludeHistory bit=0,
    @vcOrderStatus VARCHAR(500),
    @vcOrderSource VARCHAR(1000),
	@numOppID AS NUMERIC(18,0) = 0,
	@vcShippingService AS VARCHAR(MAX) = '',
	@numShippingZone NUMERIC(18,0)=0,
	@vcRegularSearchCriteria VARCHAR(MAX) = '',
	@vcCustomSearchCriteria VARCHAr(MAX) = ''
)
AS 
BEGIN

	DECLARE @firstRec AS INTEGER                                                              
	DECLARE @lastRec AS INTEGER                                                     
	SET @firstRec = ( @CurrentPage - 1 ) * @PageSize                             
	SET @lastRec = ( @CurrentPage * @PageSize + 1 )                                                                    

	IF LEN(ISNULL(@SortCol,''))=0 OR CHARINDEX('OBD.',@SortCol) > 0
		SET @SortCol = 'Opp.bintCreatedDate'
		
	IF LEN(ISNULL(@SortDirection,''))=0	
		SET @SortDirection = 'DESC'

	CREATE TABLE #tempForm 
	(
		ID INT IDENTITY(1,1), tintOrder TINYINT,vcDbColumnName nvarchar(50),vcOrigDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),vcListItemType varchar(3),
		numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,
		bitIsAlphaNumeric BIT,bitIsNumeric BIT,bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),
		ListRelID numeric(9),intColumnWidth int,bitAllowFiltering BIT,vcFieldDataType CHAR(1)
	)

	-- FIRST CHECK IF GRID COLUMNS ARE CONFIGURED
	DECLARE  @Nocolumns  AS TINYINT = 0
	DECLARE @numFormId INT = 23

	SELECT 
		@Nocolumns=ISNULL(SUM(TotalRow),0) 
	FROM
	(            
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1
		UNION 
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicCustomColumns 
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
	) TotalRows

	-- IF GRID COLUMNS ARE NOT CONFIGURES THEN CHECK IF MASTER FORM CONFIGURATION IS CREATED BY ADMINISTRATOR
	DECLARE @IsMasterConfAvailable BIT = 0
	DECLARE @numUserGroup NUMERIC(18,0)

	IF @Nocolumns=0
	BEGIN
		SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

		IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = @numFormId AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
		BEGIN
			SET @IsMasterConfAvailable = 1
		END
	END

	--IF MASTERCONFIGURATION IS AVAILABLE THEN LOAD IT OTHERWISE LOAD DEFAULT COLUMNS
	IF @IsMasterConfAvailable = 1
	BEGIN
		INSERT INTO DycFormConfigurationDetails 
		(
			numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
		)
		SELECT 
			@numFormId,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,0,1,0,intColumnWidth
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
		UNION
		SELECT 
			@numFormId,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,0,1,1,intColumnWidth
		FROM 
			View_DynamicCustomColumnsMasterConfig
		WHERE 
			View_DynamicCustomColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1


		INSERT INTO 
			#tempForm
		SELECT 
			(intRowNum + 1) as tintOrder, vcDbColumnName,	vcOrigDbColumnName,	ISNULL(vcCultureFieldName,vcFieldName), vcAssociatedControlType,
			vcListItemType, numListID, vcLookBackTableName,	bitCustom, numFieldId, bitAllowSorting,	bitInlineEdit, bitIsRequired,
			bitIsEmail,	bitIsAlphaNumeric, bitIsNumeric, bitIsLengthValidation,	intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,
			ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType 
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName, vcAssociatedControlType,'' as vcListItemType,numListID,'',
			bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,''
		FROM 
			View_DynamicCustomColumnsMasterConfig
		WHERE 
			View_DynamicCustomColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1
		ORDER BY 
			tintOrder ASC  
	END
	ELSE                                        
	BEGIN
		IF @Nocolumns=0
		BEGIN
			INSERT INTO DycFormConfigurationDetails 
			(
				numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
			)
			SELECT 
				@numFormId,numFieldId,0,Row_number() over(order by tintRow desc),@numDomainID,@numUserCntID,0,1,0,intColumnWidth
			FROM 
				View_DynamicDefaultColumns
			WHERE 
				numFormId=@numFormId 
				AND bitDefault=1 
				AND ISNULL(bitSettingField,0)=1 
				AND numDomainID=@numDomainID
			ORDER BY 
				tintOrder asc   
		END

		INSERT INTO 
			#tempForm
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
			vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName,bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,bitFieldMessage,
			vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType  
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND ISNULL(bitSettingField,0)=1 
			AND ISNULL(bitCustom,0)=0  
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName,vcAssociatedControlType,'' as vcListItemType,numListID,'',bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,'' 
		FROM 
			View_DynamicCustomColumns
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND ISNULL(bitCustom,0)=1
		ORDER BY 
			tintOrder asc  
	END

	DECLARE @strColumns VARCHAR(MAX) = ''
    DECLARE @FROM NVARCHAR(MAX) = ''
    DECLARE @WHERE NVARCHAR(MAX) = ''

	SET @FROM =' FROM 
						OpportunityMaster Opp
					INNER JOIN 
						DivisionMaster DM 
					ON 
						Opp.numDivisionId = DM.numDivisionID
					INNER JOIN 
						CompanyInfo cmp 
					ON 
						cmp.numCompanyID=DM.numCompanyID
					LEFT JOIN
						AdditionalContactsInformation ADC
					ON
						Opp.numContactID=ADC.numContactID
					LEFT JOIN
						DivisionMasterShippingConfiguration DMSC
					ON
						DM.numDivisionID=DMSC.numDivisionID'
					
	SET @WHERE = CONCAT(' WHERE 
							(Opp.numOppID = ',@numOppID,' OR ',@numOppID,' = 0) 
							AND Opp.tintopptype = 1 
							AND Opp.tintOppStatus=1 
							AND (CHARINDEX(''Ready to Ship'', dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID)) > 0 OR CHARINDEX(''Not Applicable'', dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID)) > 0)
							AND Opp.numDomainID = ',@numDomainId)

	IF @SortCol like 'CFW.Cust%'             
	BEGIN            
		SET @FROM = @FROM + ' LEFT JOIN CFW_Fld_Values_Opp CFW on CFW.RecId=Opp.numOppid and CFW.fld_id= '+ REPLACE(@SortCol,'CFW.Cust','') +' '                                                     
		SET @SortCol='CFW.Fld_Value'            
	END 
      
    DECLARE @strSql NVARCHAR(MAX)
    DECLARE @SELECT NVARCHAR(MAX) = 'WITH bizdocs AS 
									(SELECT
										Opp.numOppID
										,Opp.numRecOwner
										,Opp.vcpOppName
										,DM.tintCRMType
										,ISNULL(DM.numTerID,0) numTerID
										,DM.numDivisionID
										,ISNULL(Opp.intUsedShippingCompany,ISNULL(DM.intShippingCompany,90)) AS numShipVia
										,Opp.monDealAmount
										,(CASE WHEN (SELECT COUNT(*) FROM SalesFulfillmentQueue WHERE numDomainID = Opp.numDomainId AND numOppId=Opp.numOppId AND ISNULL(bitExecuted,0)=0) > 0 THEN 1 ELSE 0 END) as bitPendingExecution
										,ISNULL(Opp.monDealAmount,0) AS monDealAmount1'

	IF CHARINDEX('OBD.monAmountPaid',@vcRegularSearchCriteria) > 0
	BEGIN
		IF CHARINDEX('OBD.monAmountPaid like ''%0%''',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'OBD.monAmountPaid like ''%0%''',' 1 = (SELECT ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM  OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)) ')
		IF CHARINDEX('OBD.monAmountPaid like ''%1%''',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'OBD.monAmountPaid like ''%1%''','  1 = (SELECT (CASE WHEN (SELECT (CASE WHEN (SUM(ISNULL(monDealAmount,0)) = SUM(ISNULL(monAmountPaid,0)) AND SUM(ISNULL(monAmountPaid,0)) <> 0) THEN 1 ELSE 0 END) FROM OpportunityBizDocs BD WHERE BD.numOppId =  Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0) = 1) > 0 THEN 1 ELSE 0 END))  ')
		IF CHARINDEX('OBD.monAmountPaid like ''%2%''',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'OBD.monAmountPaid like ''%2%''',' 1 = (SELECT CASE WHEN (SELECT (CASE WHEN ((SUM(ISNULL(monDealAmount,0)) - SUM(ISNULL(monAmountPaid,0))) > 0 AND SUM(ISNULL(monAmountPaid,0)) <> 0) THEN 1 ELSE 0 END) FROM OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0) = 1) > 0 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('OBD.monAmountPaid like ''%3%''',@vcRegularSearchCriteria) > 0								   
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'OBD.monAmountPaid like ''%3%''',' 1 = (SELECT CASE WHEN (SELECT (CASE WHEN (SUM(ISNULL(monAmountPaid,0)) = 0) THEN 1 ELSE 0 END) FROM OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0) = 1) > 0 THEN 1 ELSE 0 END) ')
	END
	
	IF CHARINDEX('OI.vcInventoryStatus',@vcRegularSearchCriteria) > 0
	BEGIN
		IF CHARINDEX('OI.vcInventoryStatus=1',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'OI.vcInventoryStatus=1',' CHARINDEX(''Not Applicable'', dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID)) > 0 ')
		IF CHARINDEX('OI.vcInventoryStatus=2',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'OI.vcInventoryStatus=2',' CHARINDEX(''Allocation Cleared'', dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID)) > 0 ')
		IF CHARINDEX('OI.vcInventoryStatus=3',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'OI.vcInventoryStatus=3',' CHARINDEX(''Back Order'', dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID)) > 0 ')
		IF CHARINDEX('OI.vcInventoryStatus=4',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'OI.vcInventoryStatus=4',' CHARINDEX(''Ready to Ship'', dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID)) > 0 ')
	END

	IF CHARINDEX('dbo.fn_getOPPAddressState(Opp.numOppId,Opp.numDomainID,2)',@vcRegularSearchCriteria) > 0
	BEGIN
		SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'dbo.fn_getOPPAddressState(Opp.numOppId,Opp.numDomainID,2)','(CASE 
																																		WHEN Opp.tintShipToType IS NULL OR Opp.tintShipToType = 1
																																		THEN
																																			(SELECT  
																																				ISNULL(dbo.fn_GetState(AD.numState),'''')
																																			FROM 
																																				AddressDetails AD 
																																			WHERE 
																																				AD.numDomainID=Opp.numDomainID AND AD.numRecordID=Opp.numDivisionID 
																																				AND AD.tintAddressOf = 2 AND AD.tintAddressType = 2 AND AD.bitIsPrimary=1)
																																		WHEN Opp.tintShipToType = 0
																																		THEN
																																			(SELECT 
																																				ISNULL(dbo.fn_GetState(AD.numState),'''')
																																			FROM 
																																				CompanyInfo [Com1] 
																																			JOIN 
																																				DivisionMaster div1 
																																			ON 
																																				com1.numCompanyID = div1.numCompanyID
																																			JOIN Domain D1 ON D1.numDivisionID = div1.numDivisionID
																																			JOIN dbo.AddressDetails AD ON AD.numDomainID = div1.numDomainID AND AD.numRecordID = div1.numDivisionID AND tintAddressOf = 2 AND tintAddressType = 2 AND bitIsPrimary=1
																																			WHERE  D1.numDomainID = Opp.numDomainID)
																																		WHEN Opp.tintShipToType = 2 OR Opp.tintShipToType = 3
																																		THEN
																																			(SELECT
																																				ISNULL(dbo.fn_GetState(numShipState),'''')
																																			FROM 
																																				OpportunityAddress 
																																			WHERE 
																																				numOppID = Opp.numOppId)
																																		ELSE ''''
																																	END)')
	END
	

	IF @vcRegularSearchCriteria<>'' 
	BEGIN
		SET @WHERE = @WHERE + ' AND ' + @vcRegularSearchCriteria 
	END

	IF @vcCustomSearchCriteria<>'' 
	BEGIN
		SET @WHERE = @WHERE +' AND ' +  @vcCustomSearchCriteria
	END

	DECLARE @tintOrder  AS TINYINT;SET @tintOrder = 0
	DECLARE @vcFieldName  AS VARCHAR(50)
	DECLARE @vcListItemType  AS VARCHAR(3)
	DECLARE @vcListItemType1  AS VARCHAR(1)
	DECLARE @vcAssociatedControlType VARCHAR(30)
	DECLARE @numListID  AS NUMERIC(9)
	DECLARE @vcDbColumnName VARCHAR(40)
	DECLARE @vcLookBackTableName VARCHAR(2000)
	DECLARE @bitCustom  AS BIT
	Declare @numFieldId as numeric  
	DECLARE @bitAllowSorting AS CHAR(1)   
	DECLARE @bitAllowEdit AS CHAR(1)                 
	DECLARE @vcColumnName AS VARCHAR(500)
	DECLARE @ListRelID AS NUMERIC(9) 

	DECLARE @i AS INT = 1
	DECLARE @iCount AS INT 
	SELECT @iCount = COUNT(*) FROM #tempForm

	WHILE @i <= @iCount
	BEGIN
		SELECT 
			@tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,@vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,
			@numListID=numListID,@vcLookBackTableName=vcLookBackTableName ,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,
			@ListRelID=ListRelID
		FROM 
			#tempForm 
		WHERE
			ID=@i

		IF @bitCustom = 0
		BEGIN
			DECLARE  @Prefix  AS VARCHAR(5)

			IF @vcLookBackTableName = 'AdditionalContactsInformation'
				SET @Prefix = 'ADC.'
			IF @vcLookBackTableName = 'DivisionMaster'
				SET @Prefix = 'DM.'
			IF @vcLookBackTableName = 'OpportunityMaster'
				SET @PreFix = 'Opp.'
			IF @vcLookBackTableName = 'CompanyInfo'
				SET @PreFix = 'cmp.'
			IF @vcLookBackTableName = 'DivisionMasterShippingConfiguration'
				SET @PreFix = 'DMSC.'

			SET @vcColumnName= @vcDbColumnName+'~'+ convert(varchar(10),@numFieldId) +'~0'

			IF @vcAssociatedControlType = 'SelectBox'
			BEGIN
				IF @vcDbColumnName = 'numPartner'
				BEGIN
					SET @strColumns=@strColumns+ ' ,(SELECT D.vcPartnerCode+''-''+C.vcCompanyName FROM DivisionMaster AS D LEFT JOIN CompanyInfo AS C ON D.numCompanyID=C.numCompanyID WHERE D.numDivisionID=Opp.numPartner) '+' ['+ @vcColumnName+']'    
					SET @strColumns=@strColumns+ ' ,(SELECT D.vcPartnerCode+''-''+C.vcCompanyName FROM DivisionMaster AS D LEFT JOIN CompanyInfo AS C ON D.numCompanyID=C.numCompanyID WHERE D.numDivisionID=Opp.numPartner) AS vcPartner' 
				END
				ELSE IF @vcDbColumnName = 'tintSource'
				BEGIN
					SET @strColumns = @strColumns + ',ISNULL(dbo.fn_GetOpportunitySourceValue(ISNULL(Opp.tintSource,0),ISNULL(Opp.tintSourceType,0),Opp.numDomainID),''Internal Order'')' + ' [' + @vcColumnName + ']'
				END
				ELSE IF @vcDbColumnName = 'numContactID'
				BEGIN
					SET @strColumns = @strColumns + ',dbo.fn_GetContactName(' + @Prefix + @vcDbColumnName + ') [' + @vcColumnName + ']'
				END
				ELSE IF @vcListItemType = 'LI'
				BEGIN
					SET @strColumns = @strColumns + ',L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData' + ' [' + @vcColumnName + ']'
					SET @FROM = @FROM + ' LEFT JOIN ListDetails L' + CONVERT(VARCHAR(3),@tintOrder) + ' on L' + CONVERT(VARCHAR(3),@tintOrder) + '.numListItemID=' + @vcDbColumnName
				END
				ELSE IF @vcListItemType = 'U'
				BEGIN
					SET @strColumns = @strColumns + ',dbo.fn_GetContactName(' + @Prefix + @vcDbColumnName + ') [' + @vcColumnName + ']'
				END
				ELSE IF @vcListItemType = 'SGT'
				BEGIN
					SET @strColumns = @strColumns + ',(CASE ' + @Prefix + @vcDbColumnName + ' 
														WHEN 0 THEN ''Service Default''
														WHEN 1 THEN ''Adult Signature Required''
														WHEN 2 THEN ''Direct Signature''
														WHEN 3 THEN ''InDirect Signature''
														WHEN 4 THEN ''No Signature Required''
														ELSE ''''
														END) [' + @vcColumnName + ']'
				END
				ELSE IF @vcListItemType = 'PSS'
				BEGIN
					SET @strColumns = @strColumns + ',(CASE ' + @Prefix + @vcDbColumnName + ' 
														WHEN 10 THEN ''FedEx Priority Overnight''
														WHEN 11 THEN ''FedEx Standard Overnight''
														WHEN 12 THEN ''FedEx Overnight''
														WHEN 13 THEN ''FedEx 2nd Day''
														WHEN 14 THEN ''FedEx Express Saver''
														WHEN 15 THEN ''FedEx Ground''
														WHEN 16 THEN ''FedEx Ground Home Delivery''
														WHEN 17 THEN ''FedEx 1 Day Freight''
														WHEN 18 THEN ''FedEx 2 Day Freight''
														WHEN 19 THEN ''FedEx 3 Day Freight''
														WHEN 20 THEN ''FedEx International Priority''
														WHEN 21 THEN ''FedEx International Priority Distribution''
														WHEN 22 THEN ''FedEx International Economy''
														WHEN 23 THEN ''FedEx International Economy Distribution''
														WHEN 24 THEN ''FedEx International First''
														WHEN 25 THEN ''FedEx International Priority Freight''
														WHEN 26 THEN ''FedEx International Economy Freight''
														WHEN 27 THEN ''FedEx International Distribution Freight''
														WHEN 28 THEN ''Europe International Priority''
														WHEN 40 THEN ''UPS Next Day Air''
														WHEN 42 THEN ''UPS 2nd Day Air''
														WHEN 43 THEN ''UPS Ground''
														WHEN 48 THEN ''UPS 3Day Select''
														WHEN 49 THEN ''UPS Next Day Air Saver''
														WHEN 50 THEN ''UPS Saver''
														WHEN 51 THEN ''UPS Next Day Air Early A.M.''
														WHEN 55 THEN ''UPS 2nd Day Air AM''
														WHEN 70 THEN ''USPS Express''
														WHEN 71 THEN ''USPS First Class''
														WHEN 72 THEN ''USPS Priority''
														WHEN 73 THEN ''USPS Parcel Post''
														WHEN 74 THEN ''USPS Bound Printed Matter''
														WHEN 75 THEN ''USPS Media''
														WHEN 76 THEN ''USPS Library''
														ELSE ''''
														END) [' + @vcColumnName + ']'
						
				END
            END
			ELSE IF @vcAssociatedControlType = 'DateField'
			BEGIN
				IF @vcDbColumnName = 'dtReleaseDate'
				BEGIN
					SET @strColumns = @strColumns
                                + ',case when convert(varchar(11),'
                                + @Prefix
                                + @vcDbColumnName
                                + ')= convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getdate())) then ''<b><font color=red>Today</font></b>'''
					SET @strColumns = @strColumns
									+ 'when convert(varchar(11),'
									+ @Prefix
									+ @vcDbColumnName
									+ ')= convert(varchar(11),dateadd(day,-1,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getdate()))) then ''<b><font color=purple>Yesterday</font></b>'''
					SET @strColumns = @strColumns
									+ 'when convert(varchar(11),'
									+ @Prefix
									+ @vcDbColumnName
									+ ')= convert(varchar(11),dateadd(day,1,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getdate()))) then''<b><font color=orange>Tommorow</font></b>'' '
					SET @strColumns = @strColumns
									+ 'else dbo.FormatedDateFromDate('
									+ @Prefix
									+ @vcDbColumnName
									+ ','
									+ CONVERT(VARCHAR(10),@numDomainId)
									+ ') end  ['
									+ @vcColumnName + ']'
				END
				ELSE
				BEGIN
					SET @strColumns = @strColumns
                                + ',case when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','
                                + @Prefix
                                + @vcDbColumnName
                                + ') )= convert(varchar(11),getdate()) then ''<b><font color=red>Today</font></b>'''
					SET @strColumns = @strColumns
									+ 'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','
									+ @Prefix
									+ @vcDbColumnName
									+ ') )= convert(varchar(11),dateadd(day,-1,getdate())) then ''<b><font color=purple>Yesterday</font></b>'''
					SET @strColumns = @strColumns
									+ 'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','
									+ @Prefix
									+ @vcDbColumnName
									+ ') )= convert(varchar(11),dateadd(day,1,getdate())) then''<b><font color=orange>Tommorow</font></b>'' '
					SET @strColumns = @strColumns
									+ 'else dbo.FormatedDateFromDate('
									+ @Prefix
									+ @vcDbColumnName
									+ ','
									+ CONVERT(VARCHAR(10),@numDomainId)
									+ ') end  ['
									+ @vcColumnName + ']'
				 END	
            END
            ELSE IF @vcAssociatedControlType = 'TextBox' OR @vcAssociatedControlType = 'TextArea'
            BEGIN
				IF @vcDbColumnName = 'vcCompanyName'
				BEGIN
					SET @strColumns = @strColumns + ',' + @Prefix + @vcDbColumnName + ' [' + @vcColumnName + ']'
					SET @strColumns = @strColumns + ',dbo.fn_GetListItemName(Opp.[numstatus]) vcOrderStatus'
				END
				ELSE IF @vcDbColumnName = 'numShipState'
				BEGIN
					SET @strColumns = @strColumns + ',dbo.fn_getOPPAddressState(Opp.numOppId,Opp.numDomainID,2)' + ' [' + @vcColumnName + ']'
				END
				ELSE
				BEGIN
					SET @strColumns = @strColumns + ',' + @Prefix + @vcDbColumnName + ' [' + @vcColumnName + ']'
				END
            END
            ELSE IF  @vcAssociatedControlType='Label'                                              
			BEGIN
				SET @strColumns=@strColumns + ',' + CASE   
				WHEN @vcDbColumnName = 'vcFulfillmentStatus' THEN 'ISNULL(STUFF((SELECT CONCAT(''<br />'',vcMessage) FROM SalesFulfillmentLog WHERE numDomainID = Opp.numDomainId AND numOppID=Opp.numOppId AND ISNULL(bitSuccess,0) = 1 FOR XML PATH('''')) ,1,12,''''),'''')'                 
				WHEN @vcDbColumnName = 'vcPricedBoxedTracked' THEN 'STUFF((SELECT CONCAT('', '',OpportunityBizDocs.numOppID,''~'',numOppBizDocsId,''~'',ISNULL(numShippingReportId,0),''~'',(CASE WHEN (SELECT COUNT(*) FROM ShippingBox WHERE numShippingReportId=ShippingReport.numShippingReportId AND LEN(ISNULL(vcShippingLabelImage,'''')) > 0) > 0 THEN 1 ELSE 0 END)) vcText FROM OpportunityBizDocs LEFT JOIN ShippingReport ON ShippingReport.numOppBizDocId=OpportunityBizDocs.numOppBizDocsId WHERE OpportunityBizDocs.numBizDocId IN (SELECT ISNULL(numDefaultSalesShippingDoc,0) FROM Domain WHERE numDomainID=Opp.numDomainID) AND OpportunityBizDocs.numOppId=Opp.numOppID AND OpportunityBizDocs.bitShippingGenerated=1 FOR XML PATH(''''), TYPE).value(''.'',''NVARCHAR(MAX)''),1,2,'' '')'
				WHEN @vcDbColumnName = 'CalAmount' THEN 'Opp.monDealAmount'--'[dbo].[getdealamount](Opp.numOppId,Getutcdate(),0)'
				WHEN @vcDbColumnName = 'vcInventoryStatus' THEN 'ISNULL(dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID),'''')'
				WHEN @vcDbColumnName = 'vcOrderedShipped' THEN 'dbo.GetOrderShipReceiveStatus(Opp.numOppId,1,Opp.tintshipped)'
				WHEN @vcDbColumnName = 'vcOrderedReceived' THEN 'dbo.GetOrderShipReceiveStatus(Opp.numOppId,2,Opp.tintshipped)'
				WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monDealAmount' THEN 'ISNULL((SELECT SUM(ISNULL(monDealAmount,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
				WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monAmountPaid' THEN 'ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
				ELSE @Prefix + @vcDbColumnName END +' ['+ @vcColumnName+']'        
			END
        END
		ELSE
        BEGIN
			SET @vcColumnName= @vcDbColumnName +'~'+ convert(varchar(10),@numFieldId)+'~1'

			IF @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'
            BEGIN
              SET @strColumns = @strColumns
                              + ',CFW'
                              + CONVERT(VARCHAR(3),@tintOrder)
                              + '.Fld_Value  ['
                              + @vcColumnName + ']'
              SET @FROM = @FROM
                                      + ' left Join CFW_FLD_Values_Opp CFW'
                                      + CONVERT(VARCHAR(3),@tintOrder)
                                      + ' on CFW'
                                      + CONVERT(VARCHAR(3),@tintOrder)
                                      + '.Fld_Id='
                                      + CONVERT(VARCHAR(10),@numFieldId)
                                      + 'and CFW'
                                      + CONVERT(VARCHAR(3),@tintOrder)
                                      + '.RecId=Opp.numOppid   '
			END
			ELSE
            IF @vcAssociatedControlType = 'CheckBox' 
            BEGIN
                SET @strColumns = @strColumns
                                + ',case when isnull(CFW'
                                + CONVERT(VARCHAR(3),@tintOrder)
                                + '.Fld_Value,0)=0 then 0 when isnull(CFW'
                                + CONVERT(VARCHAR(3),@tintOrder)
                                + '.Fld_Value,0)=1 then 1 end   ['
                                + @vcColumnName + ']'
                SET @FROM = @FROM
                                        + ' left Join CFW_FLD_Values_Opp CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + ' on CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.Fld_Id='
                                        + CONVERT(VARCHAR(10),@numFieldId)
                                        + 'and CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.RecId=Opp.numOppid   '
            END
            ELSE
            IF @vcAssociatedControlType = 'DateField'
            BEGIN
                  SET @strColumns = @strColumns
                                  + ',dbo.FormatedDateFromDate(CFW'
                                  + CONVERT(VARCHAR(3),@tintOrder)
                                  + '.Fld_Value,'
                                  + CONVERT(VARCHAR(10),@numDomainId)
                                  + ')  ['
                                  + @vcColumnName + ']'
                  SET @FROM = @FROM
                                          + ' left Join CFW_FLD_Values_Opp CFW'
                                          + CONVERT(VARCHAR(3),@tintOrder)
                                          + ' on CFW'
                                          + CONVERT(VARCHAR(3),@tintOrder)
                                          + '.Fld_Id='
                                          + CONVERT(VARCHAR(10),@numFieldId)
                                          + 'and CFW'
                                          + CONVERT(VARCHAR(3),@tintOrder)
                                          + '.RecId=Opp.numOppid    '
            END
            ELSE
            IF @vcAssociatedControlType = 'SelectBox'
            BEGIN
                SET @vcDbColumnName = 'DCust'
                                            + CONVERT(VARCHAR(10),@numFieldId)
                SET @strColumns = @strColumns
                                + ',L'
                                + CONVERT(VARCHAR(3),@tintOrder)
                                + '.vcData'
                                + ' ['
                                + @vcColumnName + ']'

                SET @FROM = @FROM
                                        + ' left Join CFW_FLD_Values_Opp CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + ' on CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.Fld_Id='
                                        + CONVERT(VARCHAR(10),@numFieldId)
                                        + 'and CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.RecId=Opp.numOppid '

                SET @FROM = @FROM
                                        + ' left Join ListDetails L'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + ' on L'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.numListItemID=CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.Fld_Value'
            END
			ELSE
			BEGIN
				SET @strColumns = @strColumns + CONCAT(',dbo.GetCustFldValueOpp(',@numFieldId,',opp.numOppID)') + ' [' + @vcColumnName + ']'
			END
		END

		SET @i = @i + 1
	END

		
		DECLARE @numShippingServiceItemID AS NUMERIC
		SELECT @numShippingServiceItemID=ISNULL(numShippingServiceItemID,0) FROM domain WHERE numDomainID=@numDomainID
		
		IF  LEN(ISNULL(@vcOrderStatus,''))>0
		BEGIN
			SET @WHERE = @WHERE + ' and isnull(Opp.numStatus,0) in (SELECT Items FROM dbo.Split(''' +@vcOrderStatus + ''','',''))' 			
		END

		IF LEN(ISNULL(@vcShippingService,''))>0
		BEGIN
			SET @WHERE = @WHERE + ' and isnull(Opp.numShippingService,0) in (SELECT Items FROM dbo.Split(''' +@vcShippingService + ''','',''))'
		END

		IF ISNULL(@numShippingZone,0) > 0
		BEGIN
			SET @WHERE = @WHERE + ' AND 1 = (CASE 
												WHEN ISNULL((SELECT TOP 1 numState FROM fn_getOPPAddressDetails(Opp.numOppId,Opp.numDomainID,2)),0) > 0 
												THEN
													CASE 
														WHEN (SELECT COUNT(*) FROM dbo.State WHERE numDomainID=' + CAST(@numDomainId AS VARCHAR) + ' AND numStateID =  (SELECT TOP 1 numState FROM fn_getOPPAddressDetails(Opp.numOppId,Opp.numDomainID,2)) AND numShippingZone=' + CAST(@numShippingZone as VARCHAR) + ' ) > 0 
														THEN 1
														ELSE 0
													END
												ELSE 0 
											END)'
		END
		
		IF LEN(ISNULL(@vcOrderSource,''))>0
		BEGIN
				SET @WHERE = @WHERE + ' and (' + @vcOrderSource +')' 			
		END
		
		SET @WHERE = @WHERE + ' AND (Opp.numDivisionID ='+ CONVERT(VARCHAR(15), @numDivisionID) + ' OR '+ CONVERT(VARCHAR(15), @numDivisionID) + ' = 0) '
		
        SET @strSql = CONCAT(@SELECT,@strColumns,@FROM,@WHERE,' ORDER BY ',@SortCol,' ',@SortDirection,' OFFSET ',((@CurrentPage - 1) * @PageSize),' ROWS FETCH NEXT ',@PageSize, ' ROWS ONLY) SELECT * INTO #temp FROM [bizdocs];')
        
		SET @strSql = @strSql + CONCAT('SELECT 
											OM.*
											,CAST((Case WHEN ISNULL(OI.numoppitemtCode,0)>0 then 1 else 0 end) as bit) as IsShippingServiceItemID
										FROM #temp OM 
											LEFT JOIN (SELECT numOppId,numoppitemtCode,ISNULL(vcItemDesc, '''') vcItemDesc,monTotAmount,
											Row_number() OVER(PARTITION BY numOppId ORDER BY numoppitemtCode) AS row 
											FROM OpportunityItems OI WHERE OI.numOppId in (SELECT numOppId FROM #temp) AND numItemCode=',@numShippingServiceItemID,') 
											OI ON OI.row=1 AND OM.numOppId = OI.numOppId; 
											
											SELECT 
											opp.numOppId
											,Opp.vcitemname AS vcItemName
											,ISNULL((SELECT TOP 1 dbo.FormatedDateFromDate(dtReleaseDate,',@numDomainId,') FROM OpportunityItemsReleaseDates WHERE numDomainID=',@numDomainId,' AND numOppID=Opp.numOppId AND numOppItemID=Opp.numoppitemtCode),'''')  AS vcItemReleaseDate
											,ISNULL((SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numDomainId=',@numDomainId,' AND ItemImages.numItemCode=i.numItemCode AND bitDefault=1 AND bitIsImage=1),'''') AS vcImage
											,CASE 
												WHEN charitemType = ''P'' THEN ''Product''
												WHEN charitemType = ''S'' THEN ''Service''
											END AS charitemType
											,CASE WHEN charitemType = ''P'' THEN ''Inventory''
												WHEN charitemType = ''N'' THEN ''Non-Inventory''
												WHEN charitemType = ''S'' THEN ''Service''
											END AS vcItemType
											,CASE WHEN ISNULL(numItemGroup,0) > 0 THEN WI.vcWHSKU ELSE i.vcSKU END vcSKU
											,dbo.USP_GetAttributes(opp.numWarehouseItmsID,bitSerialized) AS vcAttributes
											,ISNULL(W.vcWareHouse, '''') AS Warehouse,WL.vcLocation,opp.numUnitHour AS numUnitHourOrig
											,Opp.bitDropShip AS DropShip
											,SUBSTRING((SELECT  
															'' ,'' + vcSerialNo
																				+ CASE WHEN ISNULL(I.bitLotNo, 0) = 1
																					   THEN ''(''
																							+ CONVERT(VARCHAR(15), oppI.numQty)
																							+ '')''
																					   ELSE ''''
																				  END
														FROM    OppWarehouseSerializedItem oppI
																JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID = whi.numWareHouseItmsDTLID
														WHERE   oppI.numOppID = Opp.numOppId
																AND oppI.numOppItemID = Opp.numoppitemtCode
														ORDER BY vcSerialNo
														FOR
														XML PATH('''')
														), 3, 200000) AS SerialLotNo
											,ISNULL(bitSerialized,0) AS bitSerialized
											,ISNULL(bitLotNo,0) AS bitLotNo
											,Opp.numWarehouseItmsID
											,opp.numoppitemtCode
										FROM 
											OpportunityItems opp 
										JOIN 
											#temp OM 
										ON 
											opp.numoppid=OM.numoppid 
										LEFT JOIN 
											Item i 
										ON 
											opp.numItemCode = i.numItemCode
										INNER JOIN 
											dbo.WareHouseItems WI 
										ON 
											opp.numWarehouseItmsID = WI.numWareHouseItemID
											AND WI.numDomainID = ',@numDomainId,'
										INNER JOIN 
											dbo.Warehouses W 
										ON 
											WI.numDomainID = W.numDomainID
											AND WI.numWareHouseID = W.numWareHouseID
										LEFT JOIN 
											dbo.WarehouseLocation WL 
											ON WL.numWLocationID= WI.numWLocationID;
			
										DROP TABLE #temp;')

        PRINT CAST(@strSql AS NTEXT);
        EXEC ( @strSql ) ;
       
      
        
        /*Get total count */
        SET @strSql =  ' (SELECT @TotRecs =COUNT(*) ' + @FROM + @WHERE + ')';
        
        PRINT @strSql ;
        EXEC sp_executesql 
        @query = @strSql, 
        @params = N'@TotRecs INT OUTPUT', 
        @TotRecs = @TotRecs OUTPUT 
        
		SELECT * FROM #tempForm
		DROP TABLE #tempForm
    END
  
/****** Object:  StoredProcedure [dbo].[USP_GetWorkOrder]    Script Date: 07/26/2008 16:16:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetWorkOrder')
DROP PROCEDURE USP_GetWorkOrder
GO
CREATE PROCEDURE [dbo].[USP_GetWorkOrder]                             
@numDomainID as numeric(9),
@ClientTimeZoneOffset  INT,
@numWOStatus as numeric(9),
@SortExpression AS VARCHAR(50),
@CurrentPage int,                                                                            
@PageSize INT,
@numUserCntID AS NUMERIC(9),
@vcWOId  AS VARCHAR(4000)                                                                       
AS                            
BEGIN
	SET @vcWOId=ISNULL(@vcWOId,'')

	DECLARE @firstRec AS INTEGER
	DECLARE @lastRec AS INTEGER
	SET @firstRec= ((@CurrentPage-1) * @PageSize) + 1                                                                           
	SET @lastRec= (@CurrentPage * @PageSize)
	
	DECLARE @tintCommitAllocation AS TINYINT
	SELECT @tintCommitAllocation=ISNULL(tintCommitAllocation,1) FROM Domain WHERE numDomainId=@numDomainID   
    
	DECLARE @TEMP TABLE
	(
		ItemLevel INT, numParentWOID NUMERIC(18,0), numWOID NUMERIC(18,0), ID VARCHAR(1000), numItemCode NUMERIC(18,0),
		vcItemName VARCHAR(500), numQty FLOAT, numWarehouseItemID NUMERIC(18,0), vcWarehouse VARCHAR(200), numOnHand FLOAT,
		numOnOrder FLOAT, numAllocation FLOAT, numBackOrder FLOAT, vcInstruction VARCHAR(2000),
		numAssignedTo NUMERIC(18,0), vcAssignedTo VARCHAR(500), numCreatedBy NUMERIC(18,0), vcCreated VARCHAR(500),
		bintCompliationDate VARCHAR(50), numWOStatus NUMERIC(18,0), numOppID NUMERIC(18,0), vcOppName VARCHAR(1000),
		numPOID NUMERIC(18,0), vcPOName VARCHAR(1000), bitWorkOrder BIT, bitReadyToBuild BIT
	)

	DECLARE @TEMPWORKORDER TABLE
	(
		ItemLevel INT, RowNumber INT, numParentWOID NUMERIC(18,0), numWOID NUMERIC(18,0), ID VARCHAR(1000), numItemCode NUMERIC(18,0),
		numOppID NUMERIC(18,0), numWarehouseItemID NUMERIC(18,0), numQty FLOAT, numWOStatus NUMERIC(18,0), vcInstruction VARCHAR(2000),
		numAssignedTo NUMERIC(18,0), bintCreatedDate DATETIME, bintCompliationDate DATETIME, numCreatedBy NUMERIC(18,0),  
		bitWorkOrder BIT, bitReadyToBuild BIT
	) 

	INSERT INTO 
		@TEMPWORKORDER
	SELECT 
		1 AS ItemLevel,
		ROW_NUMBER() OVER(ORDER BY numWOId) AS RowNumber,
		CAST(0 AS NUMERIC(18,0)) AS numParentId,
		numWOId,
		CAST(CONCAT('#',ISNULL(numWOId,'0'),'#') AS VARCHAR(1000)) AS ID,
		numItemCode,
		numOppId,
		numWareHouseItemId,
		CAST(numQtyItemsReq AS FLOAT),
		numWOStatus,
		vcInstruction,
		numAssignedTo,
		bintCreatedDate,
		bintCompliationDate,
		numCreatedBy,
		1 AS bitWorkOrder,
		dbo.CheckAssemblyBuildStatus(numItemCode,numQtyItemsReq,(CASE WHEN ISNULL(numOppId,0) > 0 THEN 1 ELSE 0 END),@tintCommitAllocation) AS bitReadyToBuild
	FROM 
		WorkOrder
	WHERE  
		numDomainID=@numDomainID 
		AND 1=(CASE WHEN ISNULL(@numWOStatus,0) = 0 THEN CASE WHEN WorkOrder.numWOStatus <> 23184 THEN 1 ELSE 0 END ELSE CASE WHEN WorkOrder.numWOStatus = @numWOStatus THEN 1 ELSE 0 END END)
		AND 1=(Case when @numUserCntID>0 then case when isnull(numAssignedTo,0)=@numUserCntID then 1 else 0 end else 1 end)
		AND 1=(Case when len(@vcWOId)>0 then Case when numWOId in (SELECT * from dbo.Split(@vcWOId,',')) then 1 else 0 end else 1 end)
		AND (ISNULL(numParentWOID,0) = 0 OR numParentWOID NOT IN (SELECT numWOId FROM WorkOrder WHERE numDomainID=@numDomainID))
	ORDER BY
		bintCreatedDate DESC,
		numWOId ASC

	DECLARE @TOTALROWCOUNT AS NUMERIC(18,0)

	SELECT @TOTALROWCOUNT=COUNT(*) FROM @TEMPWORKORDER


	;WITH CTEWorkOrder (ItemLevel,numParentWOID,numWOID,ID,numItemKitID,numQtyItemsReq,numWarehouseItemID,vcInstruction,numAssignedTo,
						bintCreatedDate,numCreatedBy,bintCompliationDate,numWOStatus,numOppID,bitWorkOrder,bitReadyToBuild) AS
	(
		SELECT
			ItemLevel,
			numParentWOID,
			numWOId,
			ID,
			numItemCode,
			numQty,
			numWareHouseItemId,
			vcInstruction,
			numAssignedTo,
			bintCreatedDate,
			numCreatedBy,
			bintCompliationDate,
			numWOStatus,
			numOppId,
			1 AS bitWorkOrder,
			bitReadyToBuild
		FROM
			@TEMPWORKORDER t
		WHERE
			t.RowNumber BETWEEN @firstRec AND @lastRec
		UNION ALL
		SELECT 
			c.ItemLevel+2,
			c.numWOID,
			WorkOrder.numWOId,
			CAST(CONCAT(c.ID,'-','#',ISNULL(WorkOrder.numWOId,'0'),'#') AS VARCHAR(1000)),
			WorkOrder.numItemCode,
			CAST(WorkOrder.numQtyItemsReq AS FLOAT),
			WorkOrder.numWareHouseItemId,
			CAST(WorkOrder.vcInstruction AS VARCHAR(2000)),
			WorkOrder.numAssignedTo,
			WorkOrder.bintCreatedDate,
			WorkOrder.numCreatedBy,
			WorkOrder.bintCompliationDate,
			WorkOrder.numWOStatus,
			WorkOrder.numOppId,
			1 AS bitWorkOrder,
			dbo.CheckAssemblyBuildStatus(numItemCode,WorkOrder.numQtyItemsReq,(CASE WHEN ISNULL(WorkOrder.numOppId,0) > 0 THEN 1 ELSE 0 END),@tintCommitAllocation) AS bitReadyToBuild
		FROM 
			WorkOrder
		INNER JOIN 
			CTEWorkOrder c 
		ON 
			WorkOrder.numParentWOID = c.numWOID
	)

	INSERT 
		@TEMP
	SELECT
		ItemLevel,
		numParentWOID,
		numWOID,
		ID,
		numItemCode,
		vcItemName,
		CTEWorkOrder.numQtyItemsReq,
		CTEWorkOrder.numWarehouseItemID,
		Warehouses.vcWareHouse,
		WareHouseItems.numOnHand,
		WareHouseItems.numOnOrder,
		WareHouseItems.numAllocation,
		WareHouseItems.numBackOrder,
		vcInstruction,
		CTEWorkOrder.numAssignedTo,
		dbo.fn_GetContactName(isnull(CTEWorkOrder.numAssignedTo,0)) AS vcAssignedTo,
		CTEWorkOrder.numCreatedBy,
		CAST(ISNULL(dbo.fn_GetContactName(CTEWorkOrder.numCreatedBy), '&nbsp;&nbsp;-') + ',' + convert(varchar(20),dateadd(minute,-@ClientTimeZoneOffset,CTEWorkOrder.bintCreatedDate)) AS VARCHAR(500)) ,
		convert(varchar(20),dateadd(minute,-@ClientTimeZoneOffset,bintCompliationDate)) AS bintCompliationDate,
		numWOStatus,
		CTEWorkOrder.numOppID,
		OpportunityMaster.vcPOppName,
		NULL,
		NULL,
		bitWorkOrder,
		bitReadyToBuild
	FROM 
		CTEWorkOrder
	INNER JOIN 
		Item
	ON
		CTEWorkOrder.numItemKitID = Item.numItemCode
	LEFT JOIN
		WareHouseItems
	ON
		CTEWorkOrder.numWarehouseItemID = WareHouseItems.numWareHouseItemID
	LEFT JOIN
		Warehouses
	ON
		WareHouseItems.numWareHouseID = Warehouses.numWareHouseID
	LEFT JOIN 
		OpportunityMaster
	ON
		CTEWorkOrder.numOppID = OpportunityMaster.numOppId

	INSERT INTO 
		@TEMP
	SELECT
		t1.ItemLevel + 1,
		t1.numWOID,
		NULL,
		CONCAT(t1.ID,'-#0#'),
		WorkOrderDetails.numChildItemID,
		Item.vcItemName,
		WorkOrderDetails.numQtyItemsReq,
		WorkOrderDetails.numWarehouseItemID,
		Warehouses.vcWareHouse,
		WareHouseItems.numOnHand,
		WareHouseItems.numOnOrder,
		WareHouseItems.numAllocation,
		WareHouseItems.numBackOrder,
		vcInstruction,
		NULL,
		NULL,
		NULL,
		NULL,
		convert(varchar(20),dateadd(minute,-@ClientTimeZoneOffset,bintCompliationDate)) AS bintCompliationDate,
		numWOStatus,
		NULL,
		NULL,
		OpportunityMaster.numOppId,
		OpportunityMaster.vcPOppName,
		0 AS bitWorkOrder,
		0 AS bitReadyToBuild
	FROM
		WorkOrderDetails
	INNER JOIN
		@TEMP t1
	ON
		WorkOrderDetails.numWOId = t1.numWOID
	INNER JOIN 
		Item
	ON
		WorkOrderDetails.numChildItemID = Item.numItemCode
	LEFT JOIN
		WareHouseItems
	ON
		WorkOrderDetails.numWarehouseItemID = WareHouseItems.numWareHouseItemID
	LEFT JOIN
		Warehouses
	ON
		WareHouseItems.numWareHouseID = Warehouses.numWareHouseID
	LEFT JOIN
		OpportunityMaster 
	ON
		WorkOrderDetails.numPOID = OpportunityMaster.numOppId

	SELECT * FROM @TEMP ORDER BY numParentWOID,ItemLevel

	SELECT @TOTALROWCOUNT AS TotalRowCount 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_InsertCommunication]    Script Date: 07/26/2008 16:19:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Modified By Anoop jayaraj                 
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_insertcommunication')
DROP PROCEDURE usp_insertcommunication
GO
CREATE PROCEDURE [dbo].[usp_InsertCommunication]                                                
@numCommId numeric=0,                            
@bitTask numeric,                                        
@numContactId numeric,                            
@numDivisionId numeric,                            
@txtDetails text,                            
@numOppId numeric=0,                            
@numAssigned numeric=0,                                                
@numUserCntID numeric,                                                                                                                                               
@numDomainId numeric,                                                
@bitClosed tinyint,                                                
@vcCalendarName varchar(100)='',                                                                                                     
@dtStartTime datetime,                 
@dtEndtime datetime,                                                                             
@numActivity as numeric(9),                                              
@numStatus as numeric(9),                                              
@intSnoozeMins as int,                                              
@tintSnoozeStatus as tinyint,                                            
@intRemainderMins as int,                                            
@tintRemStaus as tinyint,                                  
@ClientTimeZoneOffset Int,                  
@bitOutLook tinyint,            
@bitSendEmailTemplate bit,            
@bitAlert bit,            
@numEmailTemplate numeric(9)=0,            
@tintHours tinyint=0,          
@CaseID  numeric=0 ,        
@CaseTimeId numeric =0,    
@CaseExpId numeric = 0  ,                     
@ActivityId numeric = 0 ,
@bitFollowUpAnyTime BIT,
@strAttendee TEXT,
@numLinkedOrganization NUMERIC(18,0) = 0,
@numLinkedContact NUMERIC(18,0) = 0
--                                                
AS                       
--	IF @numDivisionId = 0                         
--	BEGIN
--		RAISERROR('COMPANY_NOT_FOUND',16,1)
--		RETURN
--	END
--  
--	IF @numContactId = 0 
--	BEGIN
--		RAISERROR('CONTACT_NOT_FOUND',16,1)
--		RETURN
--	END           
    
	declare @CheckRecord as bit   
	set @CheckRecord = 1                

  if not exists (select * from communication  WHERE numcommid=@numCommId   )                                                
	set @CheckRecord = 0                                                

  IF @numCommId = 0    or @CheckRecord = 0                                            

  BEGIN
  
	IF ISNULL(@numDivisionId,0) = 0                         
		BEGIN
			IF ISNULL(@numContactId,0) > 0
				BEGIN
					SELECT @numDivisionId = numDivisionId FROM AdditionalContactsInformation WHERE numContactId = @numContactId
					
					IF ISNULL(@numDivisionId,0) = 0 
					BEGIN
						RAISERROR('COMPANY_NOT_FOUND_FOR_CONTACT' ,16,1)
						RETURN
					END                   
				END
			ELSE
				BEGIN
					RAISERROR('COMPANY_NOT_FOUND',16,1)
					RETURN	
				END
		END
  
	IF ISNULL(@numContactId,0) = 0 
		BEGIN
			RAISERROR('CONTACT_NOT_FOUND',16,1)
			RETURN
		END   
		                   
   IF @numAssigned=0  SET @numAssigned=@numUserCntID                                                
                                             
   Insert into communication                  
   (                
   bitTask,                
   numContactId,                
   numDivisionId,                
   textDetails,                
   intSnoozeMins,                
   intRemainderMins,                
   numStatus,                
   numActivity,                
   numAssign,                
   tintSnoozeStatus,                
   tintRemStatus,                
   numOppId,                
   numCreatedby,                
   dtCreatedDate,                
   numModifiedBy,                
   dtModifiedDate,                
   numDomainID,                
   bitClosedFlag,                
   vcCalendarName,                
   bitOutlook,                
   dtStartTime,                
   dtEndTime,              
   numAssignedBy,            
   bitSendEmailTemp,            
   numEmailTemplate,            
   tintHours,            
   bitAlert,          
CaseId,        
CaseTimeId,                                                                                 
CaseExpId,  
numActivityId ,bitFollowUpAnyTime            
   )                 
 values                
   (                                              
   @bitTask,                
   @numcontactId,                                              
   @numDivisionId,                
   ISNULL(@txtdetails,''),                
   @intSnoozeMins,                
   @intRemainderMins,                
   @numStatus,                
   @numActivity,                                            
   @numAssigned,                
   @tintSnoozeStatus,                
   @tintRemStaus,                                             
   @numoppid,                
   @numUserCntID,                
   getutcdate(),                
   @numUserCntID,                                              
   getutcdate(),                
   @numDomainId,                
   @bitClosed,                
   @vcCalendarName,                
   @bitOutLook,                                              
   DateAdd(minute, @ClientTimeZoneOffset, @dtStartTime),                
   DateAdd(minute, @ClientTimeZoneOffset,@dtEndtime),              
   @numUserCntID,            
   @bitSendEmailTemplate,            
   @numEmailTemplate,            
   @tintHours,            
   @bitAlert,          
@CaseID,        
@CaseTimeId,                                                                                 
@CaseExpId,  
@ActivityId ,@bitFollowUpAnyTime 
 )                                                             
   set @numCommId= SCOPE_IDENTITY() 
   
	IF ISNULL(@numLinkedOrganization,0) > 0
	BEGIN
		INSERT INTO CommunicationLinkedOrganization VALUES (@numCommId,@numLinkedOrganization,@numLinkedContact)
	END
                                                    
  END                                                
 ELSE                    
  BEGIN                
    ---Updating if Action Item is assigned to someone                      
  declare @tempAssignedTo as numeric(9)                    
  set @tempAssignedTo=null                     
  select @tempAssignedTo=isnull(numAssign,0) from communication where numcommid=@numCommId                     
print @tempAssignedTo                    
  if (@tempAssignedTo<>@numAssigned and  @numAssigned<>'0')                    
  begin                      
    update communication set numAssignedBy=@numUserCntID where numcommid=@numCommId              
  end                     
  else if  (@numAssigned =0)                    
  begin                    
   update communication set numAssignedBy=0 where numcommid=@numCommId                  
  end                
              
              
                                              
    if @vcCalendarName=''                                                 
    begin                                                
      UPDATE communication SET                 
    bittask=@bittask,                                                                              
      textdetails=@txtdetails,                                               
      intSnoozeMins=@intSnoozeMins,                                               
      numStatus=@numStatus,                                              
      numActivity=@numActivity,                                             
      tintSnoozeStatus=@tintSnoozeStatus,                                            
      intRemainderMins= @intRemainderMins,                                            
      tintRemStatus= @tintRemStaus,                                              
      numassign=@numAssigned,                                                
      nummodifiedby=@numUserCntID,                                                
      dtModifiedDate=getutcdate(),                                                
      bitClosedFlag=@bitClosed,                                                        
      bitOutLook=@bitOutLook,                
    dtStartTime=DateAdd(minute, @ClientTimeZoneOffset, @dtStartTime),                
    dtEndTime=DateAdd(minute, @ClientTimeZoneOffset,@dtEndtime),            
    bitSendEmailTemp=@bitSendEmailTemplate,            
    numEmailTemplate=@numEmailTemplate,            
    tintHours=@tintHours,            
    bitAlert=@bitAlert,       
CaseID=@CaseID,        
CaseTimeId=@CaseTimeId,                                                                                 
CaseExpId=@CaseExpId  ,  
numActivityId=@ActivityId ,bitFollowUpAnyTime=@bitFollowUpAnyTime,
dtEventClosedDate=Case When isnull(@bitClosed,0)=1 then getutcdate() else null end                                 
      WHERE numcommid=@numCommId                                                
    END                                                
    ELSE                                                
    BEGIN                                                
       UPDATE communication SET                 
    bittask=@bittask,                                                                              
      textdetails=@txtdetails,                                               
      intSnoozeMins=@intSnoozeMins,                                
      numStatus=@numStatus,                                              
      numActivity=@numActivity,                                             
      tintSnoozeStatus=@tintSnoozeStatus,                                            
      intRemainderMins= @intRemainderMins,                                            
      tintRemStatus= @tintRemStaus,        
      numassign=@numAssigned,                                                
      nummodifiedby=@numUserCntID,                                                
      dtModifiedDate=getutcdate(),                                                
      bitClosedFlag=@bitClosed,                                                        
      bitOutLook=@bitOutLook,                
    dtStartTime=DateAdd(minute, @ClientTimeZoneOffset, @dtStartTime),                
    dtEndTime=DateAdd(minute, @ClientTimeZoneOffset,@dtEndtime),                                                 
     vcCalendarName=@vcCalendarName,            
      bitSendEmailTemp=@bitSendEmailTemplate,            
    numEmailTemplate=@numEmailTemplate,            
    tintHours=@tintHours,            
    bitAlert=@bitAlert ,      
CaseID=@CaseID,        
CaseTimeId=@CaseTimeId,                                                                                 
CaseExpId=@CaseExpId  ,  
numActivityId=@ActivityId  ,bitFollowUpAnyTime=@bitFollowUpAnyTime,
dtEventClosedDate=Case When isnull(@bitClosed,0)=1 then getutcdate() else null end 
   WHERE numcommid=@numCommId                                                
   END  
   
	IF ISNULL(@numLinkedOrganization,0) > 0
	BEGIN
		IF EXISTS (SELECT numCLOID FROM CommunicationLinkedOrganization WHERE numCommID=@numCommId)
		BEGIN
			UPDATE CommunicationLinkedOrganization SET numDivisionID=@numLinkedOrganization,numContactID=@numLinkedContact WHERE numCommID=@numCommId
		END
		ELSE
		BEGIN
			INSERT INTO CommunicationLinkedOrganization VALUES (@numCommId,@numLinkedOrganization,@numLinkedContact)
		END
	END
	ELSE
	BEGIN
		DELETE FROM CommunicationLinkedOrganization WHERE numCommID=@numCommId
	END
                                                 
END 

 DECLARE @hDocItem int                                                                                                                            
  IF convert(varchar(10),@strAttendee) <>''                                                                          
  BEGIN                      
      EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strAttendee  
      
      SELECT numContactID,ActivityID INTO #tempAttendees FROM OPENXML (@hDocItem,'/NewDataSet/AttendeeTable',2)                                                                          
			   WITH  (numContactID NUMERIC(9),ActivityID NUMERIC(9))
      
      delete from CommunicationAttendees where numCommId=@numCommId and numContactId not in                       
			   (SELECT numContactID from #tempAttendees)   
	
		 SELECT * FROM #tempAttendees
		 
	  INSERT INTO CommunicationAttendees (numCommId,numContactId,ActivityID)  
		  SELECT @numCommId,numContactId,ActivityID FROM #tempAttendees WHERE numContactId NOT IN(SELECT numContactId
		  FROM CommunicationAttendees WHERE numCommId=@numCommId)  
		  
	DROP TABLE #tempAttendees	   
  END
   
select @numCommId
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_Item_GetAll_BIZAPI' ) 
    DROP PROCEDURE USP_Item_GetAll_BIZAPI
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Sandeep Patel
-- Create date: 9 April 2014
-- Description:	Gets list of items domain wise
-- =============================================
CREATE PROCEDURE USP_Item_GetAll_BIZAPI
	@numDomainID NUMERIC(18,0) = NULL,
	@numPageIndex AS INT = 0,
    @numPageSize AS INT = 0,
    @TotalRecords INT OUTPUT,
	@dtCreatedAfter DateTime = NULL,
	@dtModifiedAfter DateTime = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT 
		@TotalRecords = COUNT(*) 
	FROM 
		Item 
	WHERE 
		Item.numDomainID = @numDomainID
		AND (@dtCreatedAfter IS NULL OR Item.bintCreatedDate > @dtCreatedAfter)
		AND (@dtModifiedAfter IS NULL OR Item.bintModifiedDate > @dtModifiedAfter OR (SELECT COUNT(*) FROM WareHouseItems WHERE numItemID=Item.numItemCode AND DATEADD(hh, DATEDIFF(hh, GETDATE(), GETUTCDATE()), dtModified) > @dtModifiedAfter) > 0)

	SELECT
		ROW_NUMBER() OVER (ORDER BY Item.numItemCode asc) AS RowNumber,
		Item.numItemCode,
		Item.vcItemName,
		(SELECT TOP 1 vcPathForImage FROM ItemImages WHERE numItemCode = Item.numItemCode AND ISNULL(bitDefault,0) = 1) AS vcPathForImage,
		(SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = Item.numItemCode AND ISNULL(bitDefault,0) = 1) AS vcPathForTImage,
		Item.txtItemDesc,
		Item.charItemType,
		CASE 
			WHEN Item.charItemType='P' THEN 'Inventory Item' 
			WHEN Item.charItemType='N' THEN 'Non Inventory Item' 
			WHEN Item.charItemType='S' THEN 'Service' 
			WHEN Item.charItemType='A' THEN 'Accessory' 
		END AS ItemType,
		Item.vcSKU,
		Item.vcModelID,
		Item.numBarCodeId,
		Item.vcManufacturer,
		Item.vcUnitofMeasure,
		Item.monListPrice,
		(CASE WHEN ISNULL(Item.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(Item.monAverageCost,0.00) END) AS monAverageCost,
		Item.monCampaignLabourCost,
		Item.numItemGroup,
		Item.numItemClass,
		Item.numShipClass,
		Item.numItemClassification,
		Item.fltWidth,
		Item.fltWeight,
		Item.fltHeight,
		Item.fltLength,
		Item.numBaseUnit,
		Item.numSaleUnit,
		Item.numPurchaseUnit,
		Item.bitKitParent,
		Item.bitLotNo,
		Item.bitAssembly,
		Item.bitSerialized,
		Item.bitTaxable,
		Item.bitAllowBackOrder,
		Item.bitFreeShipping,
		Item.numCOGsChartAcntId,
		Item.numAssetChartAcntId,
		Item.numIncomeChartAcntId,
		Item.numVendorID,
		WI.numOnHand,                      
		WI.numOnOrder,                      
		WI.numReorder,                      
		WI.numAllocation,                      
		WI.numBackOrder,
		bintCreatedDate,
		bintModifiedDate,
		STUFF((SELECT CONCAT(',',numCategoryID) FROM ItemCategory WHERE numItemID=Item.numItemCode GROUP BY numCategoryID FOR XML PATH('')),1,1,'') AS vcCategories
	INTO
		#Temp
	FROM
		Item
	OUTER APPLY
	(
		SELECT
			SUM(numOnHand) as numOnHand,                      
			SUM(numOnOrder) as numOnOrder,                      
			SUM(numReorder)  as numReorder,                      
			SUM(numAllocation)  as numAllocation,                      
			SUM(numBackOrder)  as numBackOrder
		FROM 
			WarehouseItems
		WHERE
			numItemID=Item.numItemCode
	) WI
	WHERE 
		Item.numDomainID = @numDomainID
		AND (@dtCreatedAfter IS NULL OR Item.bintCreatedDate > @dtCreatedAfter)
		AND (@dtModifiedAfter IS NULL OR Item.bintModifiedDate > @dtModifiedAfter OR (SELECT COUNT(*) FROM WareHouseItems WHERE numItemID=Item.numItemCode AND DATEADD(hh, DATEDIFF(hh, GETDATE(), GETUTCDATE()), dtModified) > @dtModifiedAfter) > 0)
	ORDER BY
		Item.numItemCode
	OFFSET (CASE WHEN ISNULL(@numPageIndex,0) > 0 THEN (@numPageIndex - 1) ELSE 0 END) * @numPageSize ROWS FETCH NEXT (CASE WHEN ISNULL(@numPageSize,0) > 0 THEN @numPageSize ELSE 999999999 END) ROWS ONLY

	SELECT * FROM #Temp

	--GET CHILD ITEMS
	SELECT
		ItemDetails.numItemKitID,
		ItemDetails.numChildItemID,
		ItemDetails.numQtyItemsReq,
		WareHouseItems.numWareHouseItemID,
		Warehouses.numWareHouseID,
		(SELECT vcUnitName FROM UOM WHERE numUOMId = ItemDetails.numUOMId) AS vcUnitName
	FROM
		ItemDetails
	LEFT JOIN  
		WareHouseItems 
	ON
		ItemDetails.numWareHouseItemId = WareHouseItems.numWareHouseItemID
	LEFT JOIN
		Warehouses
	ON
		WareHouseItems.numWareHouseID = Warehouses.numWareHouseID
	WHERE
		ItemDetails.numItemKitID IN (SELECT numItemCode FROM #Temp)

	--GET CUSTOM FIELDS
	SELECT 
		I.numItemCode,
		TEMPCustFields.Fld_id AS Id,
		TEMPCustFields.Fld_label AS Name,
		TEMPCustFields.Fld_type AS [Type],
		TEMPCustFields.Fld_Value AS ValueId,
		CASE 
			WHEN TEMPCustFields.Fld_type='TextBox' or TEMPCustFields.Fld_type='TextArea' THEN
				(CASE WHEN TEMPCustFields.Fld_Value='0' OR TEMPCustFields.Fld_Value='' THEN '-' ELSE TEMPCustFields.Fld_Value END)
			WHEN TEMPCustFields.Fld_type='SelectBox' THEN
				(CASE WHEN TEMPCustFields.Fld_Value='' THEN '-' ELSE dbo.GetListIemName(TEMPCustFields.Fld_Value) END)
			WHEN TEMPCustFields.Fld_type='CheckBox' THEN
				(CASE WHEN TEMPCustFields.Fld_Value=0 THEN 'No' ELSE 'Yes' END)
			ELSE
				''
		END AS Value
	FROM 
		Item I
	OUTER APPLY
	(
		SELECT
			CFW_Fld_Master.Fld_id,
			CFW_Fld_Master.Fld_label,
			CFW_Fld_Master.Fld_type,
			CFW_FLD_Values_Item.Fld_Value
		FROM
			CFW_Fld_Master
		LEFT JOIN
			CFW_FLD_Values_Item 
		ON
			CFW_FLD_Values_Item.Fld_ID = CFW_Fld_Master.Fld_id
			AND I.numItemCode = CFW_FLD_Values_Item.RecId
		WHERE 
			numDomainID = @numDomainID
			AND Grp_id = 5
	) TEMPCustFields
	WHERE 
		I.numDomainID = @numDomainID
		AND I.numItemCode IN (SELECT numItemCode FROM #Temp)

	DROp TABLE #Temp
END
GO


GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_Item_Search_BIZAPI' ) 
    DROP PROCEDURE USP_Item_Search_BIZAPI
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Sandeep Patel
-- Create date: 9 April 2014
-- Description:	Search list of items domain wise
-- =============================================
CREATE PROCEDURE USP_Item_Search_BIZAPI
	@numDomainID NUMERIC(18,0) = NULL,
	@SearchText VARCHAR(100),
	@numPageIndex AS INT = 0,
    @numPageSize AS INT = 0,
    @TotalRecords INT OUTPUT,
	@dtCreatedAfter DateTime = NULL,
	@dtModifiedAfter DateTime = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT 
		@TotalRecords = COUNT(*) 
	FROM 
		Item 
	WHERE 
		Item.numDomainID = @numDomainID 
		AND	Item.vcItemName LIKE '%'+@SearchText+'%'
		AND (@dtCreatedAfter IS NULL OR Item.bintCreatedDate > @dtCreatedAfter)
		AND (@dtModifiedAfter IS NULL OR Item.bintModifiedDate > @dtModifiedAfter OR (SELECT COUNT(*) FROM WareHouseItems WHERE numItemID=Item.numItemCode AND DATEADD(hh, DATEDIFF(hh, GETDATE(), GETUTCDATE()), dtModified) > @dtModifiedAfter) > 0)

	SELECT
		*
	INTO
		#Temp
    FROM
	(SELECT
		ROW_NUMBER() OVER (ORDER BY Item.vcItemName asc) AS RowNumber,
		Item.numItemCode,
		Item.vcItemName,
		(SELECT TOP 1 vcPathForImage FROM ItemImages WHERE numItemCode = Item.numItemCode AND ISNULL(bitDefault,0) = 1) AS vcPathForImage,
		(SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = Item.numItemCode AND ISNULL(bitDefault,0) = 1) AS vcPathForTImage,
		Item.txtItemDesc,
		Item.charItemType,
		CASE 
			WHEN Item.charItemType='P' THEN 'Inventory Item' 
			WHEN Item.charItemType='N' THEN 'Non Inventory Item' 
			WHEN Item.charItemType='S' THEN 'Service' 
			WHEN Item.charItemType='A' THEN 'Accessory' 
		END AS ItemType,
		Item.vcSKU,
		Item.vcModelID,
		Item.numBarCodeId,
		Item.vcManufacturer,
		Item.vcUnitofMeasure,
		Item.monListPrice,
		(CASE WHEN ISNULL(Item.bitVirtualInventory,0) = 1 THEN 0 ELSE Item.monAverageCost END) monAverageCost,
		Item.monCampaignLabourCost,
		Item.numItemGroup,
		Item.numItemClass,
		Item.numShipClass,
		Item.numItemClassification,
		Item.fltWidth,
		Item.fltWeight,
		Item.fltHeight,
		Item.fltLength,
		Item.numBaseUnit,
		Item.numSaleUnit,
		Item.numPurchaseUnit,
		Item.bitKitParent,
		Item.bitLotNo,
		Item.bitAssembly,
		Item.bitSerialized,
		Item.bitTaxable,
		Item.bitAllowBackOrder,
		Item.bitFreeShipping,
		Item.numCOGsChartAcntId,
		Item.numAssetChartAcntId,
		Item.numIncomeChartAcntId,
		Item.numVendorID,
		SUM(numOnHand) as numOnHand,                      
		SUM(numOnOrder) as numOnOrder,                      
		SUM(numReorder)  as numReorder,                      
		SUM(numAllocation)  as numAllocation,                      
		SUM(numBackOrder)  as numBackOrder,
		bintCreatedDate,
		bintModifiedDate,
		STUFF((SELECT CONCAT(',',numCategoryID) FROM ItemCategory WHERE numItemID=Item.numItemCode GROUP BY numCategoryID FOR XML PATH('')),1,1,'') AS vcCategories
	FROM
		Item
	LEFT JOIN  
		WareHouseItems W                  
	ON 
		W.numItemID=Item.numItemCode
	WHERE 
		Item.numDomainID = @numDomainID AND
		Item.vcItemName LIKE '%'+@SearchText+'%'
		AND (@dtCreatedAfter IS NULL OR Item.bintCreatedDate > @dtCreatedAfter)
		AND (@dtModifiedAfter IS NULL OR Item.bintModifiedDate > @dtModifiedAfter OR (SELECT COUNT(*) FROM WareHouseItems WHERE numItemID=Item.numItemCode AND DATEADD(hh, DATEDIFF(hh, GETDATE(), GETUTCDATE()), dtModified) > @dtModifiedAfter) > 0)
	GROUP BY
		Item.numItemCode,
		Item.vcItemName,
		Item.txtItemDesc,
		Item.charItemType,
		Item.vcSKU,
		Item.vcModelID,
		Item.numBarCodeId,
		Item.vcManufacturer,
		Item.vcUnitofMeasure,
		Item.monListPrice,
		Item.monAverageCost,
		Item.bitVirtualInventory,
		Item.monCampaignLabourCost,
		Item.numItemGroup,
		Item.numItemClass,
		Item.numShipClass,
		Item.numItemClassification,
		Item.fltWidth,
		Item.fltWeight,
		Item.fltHeight,
		Item.fltLength,
		Item.numBaseUnit,
		Item.numSaleUnit,
		Item.numPurchaseUnit,
		Item.bitKitParent,
		Item.bitLotNo,
		Item.bitAssembly,
		Item.bitSerialized,
		Item.bitTaxable,
		Item.bitAllowBackOrder,
		Item.bitFreeShipping,
		Item.numCOGsChartAcntId,
		Item.numAssetChartAcntId,
		Item.numIncomeChartAcntId,
		Item.numVendorID,
		Item.bintCreatedDate,
		Item.bintModifiedDate) AS I
	WHERE 
		(@numPageIndex = 0 OR @numPageSize = 0) OR
		(RowNumber > ((@numPageIndex - 1) * @numPageSize) and RowNumber < ((@numPageIndex * @numPageSize) + 1))

	SELECT * FROM #Temp

	--GET CHILD ITEMS
	SELECT
		ItemDetails.numItemKitID,
		ItemDetails.numChildItemID,
		ItemDetails.numQtyItemsReq,
		WareHouseItems.numWareHouseItemID,
		Warehouses.numWareHouseID,
		(SELECT vcUnitName FROM UOM WHERE numUOMId = ItemDetails.numUOMId) AS vcUnitName
	FROM
		ItemDetails
	LEFT JOIN  
		WareHouseItems 
	ON
		ItemDetails.numWareHouseItemId = WareHouseItems.numWareHouseItemID
	LEFT JOIN
		Warehouses
	ON
		WareHouseItems.numWareHouseID = Warehouses.numWareHouseID
	WHERE
		ItemDetails.numItemKitID IN (SELECT numItemCode FROM #Temp)

	--GET CUSTOM FIELDS
	SELECT 
		I.numItemCode,
		TEMPCustFields.Fld_id AS Id,
		TEMPCustFields.Fld_label AS Name,
		TEMPCustFields.Fld_type AS [Type],
		TEMPCustFields.Fld_Value AS ValueId,
		CASE 
			WHEN TEMPCustFields.Fld_type='TextBox' or TEMPCustFields.Fld_type='TextArea' THEN
				(CASE WHEN TEMPCustFields.Fld_Value='0' OR TEMPCustFields.Fld_Value='' THEN '-' ELSE TEMPCustFields.Fld_Value END)
			WHEN TEMPCustFields.Fld_type='SelectBox' THEN
				(CASE WHEN TEMPCustFields.Fld_Value='' THEN '-' ELSE dbo.GetListIemName(TEMPCustFields.Fld_Value) END)
			WHEN TEMPCustFields.Fld_type='CheckBox' THEN
				(CASE WHEN TEMPCustFields.Fld_Value=0 THEN 'No' ELSE 'Yes' END)
			ELSE
				''
		END AS Value
	FROM 
		Item I
	OUTER APPLY
	(
		SELECT
			CFW_Fld_Master.Fld_id,
			CFW_Fld_Master.Fld_label,
			CFW_Fld_Master.Fld_type,
			CFW_FLD_Values_Item.Fld_Value
		FROM
			CFW_Fld_Master
		LEFT JOIN
			CFW_FLD_Values_Item 
		ON
			CFW_FLD_Values_Item.Fld_ID = CFW_Fld_Master.Fld_id
			AND I.numItemCode = CFW_FLD_Values_Item.RecId
		WHERE 
			numDomainID = @numDomainID
			AND Grp_id = 5
	) TEMPCustFields
	WHERE 
		I.numDomainID = @numDomainID
		AND I.numItemCode IN (SELECT numItemCode FROM #Temp)

	DROp TABLE #Temp
END
GO
GO
/****** Object:  StoredProcedure [dbo].[USP_ItemDetails]    Script Date: 02/07/2010 15:31:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Anoop Jayaraj                                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_itemdetails')
DROP PROCEDURE usp_itemdetails
GO
CREATE PROCEDURE [dbo].[USP_ItemDetails]                                                  
@numItemCode as numeric(9) ,          
@ClientTimeZoneOffset as int,
@numDivisionId as numeric(18)                                               
AS
BEGIN 
   DECLARE @bitItemIsUsedInOrder AS BIT=0
   DECLARE @bitOppOrderExists AS BIT = 0
   
   DECLARE @numCompanyID NUMERIC(18,0)=0
   IF ISNULL(@numDivisionId,0) > 0
   BEGIN
		SELECT @numCompanyID=numCompanyID FROM DivisionMaster WHERE numDivisionID=@numDivisionId
   END

   DECLARE @numDomainId AS NUMERIC
   DECLARE @numItemGroup AS NUMERIC(18,0)
   SELECT @numDomainId=numDomainId,@numItemGroup=numItemGroup FROM Item WHERE numItemCode=@numItemCode
   
	IF GETUTCDATE()<'2013-03-15 23:59:39'
		SET @bitItemIsUsedInOrder=0
	ELSE IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityItems OI WHERE OI.numItemCode =@numItemCode) > 0
	BEGIN
		SET @bitItemIsUsedInOrder=1
		SET @bitOppOrderExists = 1
	END
	ELSE IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityKitItems OI WHERE OI.numChildItemID =@numItemCode) > 0
	BEGIN
		SET @bitItemIsUsedInOrder=1
		SET @bitOppOrderExists = 1
	END
	ELSE IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityKitChildItems OI WHERE OI.numItemID =@numItemCode) > 0
	BEGIN
		SET @bitItemIsUsedInOrder=1
		SET @bitOppOrderExists = 1
	END
	ELSE IF EXISTS(SELECT numItemID FROM dbo.General_Journal_Details WHERE numItemID= @numItemCode and numDomainId=@numDomainId AND chBizDocItems='IA1'   )
		set @bitItemIsUsedInOrder =1
                                               
	SELECT 
		I.numItemCode, vcItemName, ISNULL(txtItemDesc,'') txtItemDesc,CAST(ISNULL(IED.txtDesc,'')  AS VARCHAR(MAX)) vcExtendedDescToAPI, charItemType, 
		CASE 
			WHEN charItemType='P' AND ISNULL(bitAssembly,0) = 1 then 'Assembly'
			WHEN charItemType='P' AND ISNULL(bitKitParent,0) = 1 then 'Kit'
			WHEN charItemType='P' AND ISNULL(bitAsset,0) = 1 AND ISNULL(bitRental,0)=0 then 'Asset'
			WHEN charItemType='P' AND ISNULL(bitAsset,0) = 1 AND ISNULL(bitRental,0)=1 then 'Rental Asset'
			WHEN charItemType='P' AND ISNULL(bitSerialized,0) = 1 AND ISNULL(bitAsset,0) = 0  then 'Serialized'
			WHEN charItemType='P' AND ISNULL(bitSerialized,0) = 1 AND ISNULL(bitAsset,0) = 1 AND ISNULL(bitRental,0)=0 then 'Serialized Asset'
			WHEN charItemType='P' AND ISNULL(bitSerialized,0) = 1 AND ISNULL(bitAsset,0) = 1 AND ISNULL(bitRental,0)=1 then 'Serialized Rental Asset'
			WHEN charItemType='P' AND ISNULL(bitLotNo,0)=1 THEN 'Lot #'
			ELSE CASE WHEN charItemType='P' THEN 'Inventory' ELSE '' END
		END AS InventoryItemType
		,dbo.fn_GetItemChildMembershipCount(@numDomainId,I.numItemCode) AS numChildMembershipCount
		,CASE WHEN ISNULL(bitAssembly,0) = 1 THEN dbo.fn_GetAssemblyPossibleWOQty(I.numItemCode) ELSE 0 END AS numWOQty
		,CASE WHEN charItemType='P' THEN ISNULL((SELECT TOP 1 monWListPrice FROM WarehouseItems WHERE numDomainID=@numDomainID AND numItemID=@numItemCode AND ISNULL(monWListPrice,0) > 0),0) ELSE monListPrice END monListPrice,                   
		numItemClassification, isnull(bitTaxable,0) as bitTaxable, ISNULL(vcSKU,'') AS vcItemSKU, 
		(CASE WHEN I.numItemGroup > 0 THEN ISNULL(W.[vcWHSKU],'') ELSE ISNULL(vcSKU,'') END) AS vcSKU, 
		ISNULL(bitKitParent,0) as bitKitParent, V.numVendorID, V.monCost, I.numDomainID, I.numCreatedBy, I.bintCreatedDate, I.bintModifiedDate,I.numModifiedBy,
		(SELECT TOP 1 vcPathForImage FROM ItemImages WHERE numItemCode = @numItemCode AND bitDefault = 1 AND bitIsImage = 1 ) AS vcPathForImage ,
		(SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = @numItemCode AND bitDefault = 1 AND bitIsImage = 1) AS  vcPathForTImage
		,ISNULL(bitSerialized,0) as bitSerialized, vcModelID,                   
		(SELECT 
			numItemImageId
			,vcPathForImage
			,vcPathForTImage
			,bitDefault
			,CASE WHEN bitdefault=1 THEN -1 ELSE ISNULL(intDisplayOrder,0) END AS intDisplayOrder 
		FROM 
			ItemImages  
		WHERE 
			numItemCode=@numItemCode 
		ORDER BY 
			CASE WHEN bitdefault=1 THEN -1 ELSE isnull(intDisplayOrder,0) END ASC 
		FOR XML AUTO,ROOT('Images')) AS xmlItemImages,
		(SELECT STUFF((SELECT ',' + CONVERT(VARCHAR(50) , numCategoryID)  FROM dbo.ItemCategory WHERE numItemID = I.numItemCode FOR XML PATH('')), 1, 1, '')) AS vcItemCategories ,
		numItemGroup, numCOGsChartAcntId, numAssetChartAcntId, numIncomeChartAcntId, (CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE monAverageCost END) AS monAverageCost,                   
		monCampaignLabourCost,dbo.fn_GetContactName(I.numCreatedBy)+' ,'+convert(varchar(20),DateAdd(minute, -@ClientTimeZoneOffset,I.bintCreatedDate )) as CreatedBy ,                                      
		dbo.fn_GetContactName(I.numModifiedBy)+' ,'+convert(varchar(20),DateAdd(minute, -@ClientTimeZoneOffset,I.bintModifiedDate )) as ModifiedBy,                      
		sum(numOnHand) as numOnHand,                      
		sum(numOnOrder) as numOnOrder,                      
		sum(numReorder)  as numReorder,                      
		sum(numAllocation)  as numAllocation,                      
		sum(numBackOrder)  as numBackOrder,                   
		isnull(fltWeight,0) as fltWeight,                
		isnull(fltHeight,0) as fltHeight,                
		isnull(fltWidth,0) as fltWidth,                
		isnull(fltLength,0) as fltLength,                
		isnull(bitFreeShipping,0) as bitFreeShipping,              
		isnull(bitAllowBackOrder,0) as bitAllowBackOrder,              
		isnull(vcUnitofMeasure,'Units') as vcUnitofMeasure,      
		isnull(bitShowDeptItem,0) bitShowDeptItem,      
		isnull(bitShowDeptItemDesc,0) bitShowDeptItemDesc,  
		isnull(bitCalAmtBasedonDepItems ,0) bitCalAmtBasedonDepItems,
		isnull(bitAssembly ,0) bitAssembly ,
		isnull(cast(numBarCodeId as varchar(50)),'') as  numBarCodeId ,
		(CASE WHEN ISNULL(I.numManufacturer,0) > 0 THEN ISNULL((SELECT vcCompanyName FROM DivisionMaster INNER JOIN CompanyInfo ON DivisionMaster.numCompanyID=CompanyInfo.numCompanyId WHERE numDivisionID=I.numManufacturer),'')  ELSE ISNULL(I.vcManufacturer,'') END) AS vcManufacturer,
		ISNULL(I.numManufacturer,0) numManufacturer,
		ISNULL(I.numBaseUnit,0) AS numBaseUnit,ISNULL(I.numPurchaseUnit,0) AS numPurchaseUnit,ISNULL(I.numSaleUnit,0) AS numSaleUnit,
		dbo.fn_GetUOMName(ISNULL(I.numBaseUnit,0)) AS vcBaseUnit,dbo.fn_GetUOMName(ISNULL(I.numPurchaseUnit,0)) AS vcPurchaseUnit,dbo.fn_GetUOMName(ISNULL(I.numSaleUnit,0)) AS vcSaleUnit,
		isnull(bitLotNo,0) as bitLotNo,
		ISNULL(IsArchieve,0) AS IsArchieve,
		case when charItemType='P' then 'Inventory Item' when charItemType='S' then 'Service' when charItemType='A' then 'Accessory' when charItemType='N' then 'Non-Inventory Item' end as ItemType,
		isnull(I.numItemClass,0) as numItemClass,
		ISNULL(tintStandardProductIDType,0) tintStandardProductIDType,
		ISNULL(vcExportToAPI,'') vcExportToAPI,
		ISNULL(numShipClass,0) numShipClass,ISNULL(I.bitAllowDropShip,0) AS bitAllowDropShip,
		ISNULL(@bitItemIsUsedInOrder,0) AS bitItemIsUsedInOrder,
		ISNULL((SELECT COUNT(*) FROM ItemDetails WHERE numChildItemID = I.numItemCode),0) AS intParentKitItemCount,
		ISNULL(I.bitArchiveItem,0) AS [bitArchiveItem],
		ISNULL(W.numWareHouseID,0) AS [numWareHouseID],
		ISNULL(W.numWareHouseItemID,0) AS [numWareHouseItemID],
		ISNULL((SELECT COUNT(WI.[numWareHouseItemID]) FROM [dbo].[WareHouseItems] WI WHERE WI.[numItemID] = I.numItemCode AND WI.[numItemID] = @numItemCode),0) AS [intTransCount],
		ISNULL(I.bitAsset,0) AS [bitAsset],
		ISNULL(I.bitRental,0) AS [bitRental],
		ISNULL(W.vcBarCode,ISNULL(I.[numBarCodeId],'')) AS [vcBarCode],
		ISNULL((SELECT COUNT(*) FROM ItemUOMConversion WHERE numItemCode=I.numItemCode),0) AS tintUOMConvCount,
		ISNULL(I.bitVirtualInventory,0) bitVirtualInventory ,
		ISNULL(I.bitContainer,0) bitContainer
		,ISNULL(I.numContainer,0) numContainer
		,ISNULL(I.numNoItemIntoContainer,0) numNoItemIntoContainer
		,ISNULL(I.bitMatrix,0) AS bitMatrix
		,ISNULL(@bitOppOrderExists,0) AS bitOppOrderExists
		,CPN.CustomerPartNo
		,ISNULL(I.vcASIN,'') AS vcASIN 
	FROM 
		Item I       
	left join  
		WareHouseItems W                  
	on 
		W.numItemID=I.numItemCode                
	LEFT JOIN 
		ItemExtendedDetails IED 
	ON 
		I.numItemCode = IED.numItemCode
	LEFT JOIN 
		Vendor V 
	ON 
		I.numVendorID = V.numVendorID AND I.numItemCode = V.numItemCode  
	LEFT JOIN 
		CustomerPartNumber CPN 
	ON 
		I.numItemCode=CPN.numItemCode 
		AND CPN.numCompanyID=@numCompanyID
	WHERE 
		I.numItemCode=@numItemCode  
	GROUP BY 
		I.numItemCode, vcItemName, txtItemDesc, charItemType, monListPrice,                   
		numItemClassification, bitTaxable,vcSKU,[W].[vcWHSKU] , bitKitParent,V.numVendorID, V.monCost, I.numDomainID,               
		I.numCreatedBy, I.bintCreatedDate, I.bintModifiedDate,numContainer,   numNoItemIntoContainer,                
		I.numModifiedBy,bitAllowBackOrder, bitSerialized, vcModelID,                   
		numItemGroup, numCOGsChartAcntId, numAssetChartAcntId, numIncomeChartAcntId, monAverageCost, I.bitVirtualInventory,                   
		monCampaignLabourCost,fltWeight,fltHeight,fltWidth,fltLength,bitFreeShipping,vcUnitofMeasure,bitShowDeptItemDesc,bitShowDeptItem,bitCalAmtBasedonDepItems,bitAssembly,numBarCodeId,I.vcManufacturer
		,I.numBaseUnit,I.numPurchaseUnit,I.numSaleUnit,I.bitContainer,I.bitLotNo,I.IsArchieve,I.numItemClass,I.tintStandardProductIDType,I.vcExportToAPI,I.numShipClass,
		CAST( ISNULL(IED.txtDesc,'')  AS VARCHAR(MAX) ),I.bitAllowDropShip,I.bitArchiveItem,I.bitAsset,I.bitRental,W.[numWarehouseID],W.numWareHouseItemID,W.[vcBarCode],I.bitMatrix,I.numManufacturer,CPN.CustomerPartNo,I.vcASIN
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageInventoryAssemblyWO')
DROP PROCEDURE USP_ManageInventoryAssemblyWO
GO
CREATE PROCEDURE [dbo].[USP_ManageInventoryAssemblyWO]
	@numUserCntID AS NUMERIC(18,0),
	@numOppID AS NUMERIC(18,0),
	@numOppItemID AS NUMERIC(18,0),
	@numItemCode AS NUMERIC(18,0),
	@numWarehouseItemID AS NUMERIC(18,0),
	@QtyShipped AS FLOAT,
	@tintMode AS TINYINT=0 --1: Work Order insert/edit 2:Work Order Close 3:Work Order Re-Open 4: Work Order Delete 5: Work Order Edit(Revert Before New Insert)
AS
BEGIN
	/*  
		WHEN WORK ORDER IS CREATED FOR ASSEMBY THEN BOTH SO AND WO PROCESS ARE CARRIED OUT SEPERATELY.
		FOR EXAMPLE: LETS SAY WE HAVE "ASSEMBY ITEM" WHICH REQUIRED 2 QTY OF "ASSEMBLY SUB ITEM" 
		CURRENT INVENTORY STATUS FOR PARENT ITEM: ONHAND:1 ONORDER:0 ONALLOCATION:0 ONBACKORDER:0
		CURRENT INVENTORY STATUS FOR CHILD ITEM: ONHAND:4 ONORDER:0 ONALLOCATION:0 ONBACKORDER:0

		-- WHEN WORK ORDER IS CREATED FOR 1 QTY:
		CURRENT INVENTORY STATUS FOR PARENT ITEM: ONHAND:0 ONORDER:1 ONALLOCATION:1 ONBACKORDER:0
		CURRENT INVENTORY STATUS FOR CHILD ITEM: ONHAND:2 ONORDER:0 ONALLOCATION:2 ONBACKORDER:0

		HERE PARENT ITEM HAVE BOTH SO AND WO IMPACT. SO ONHAND DECREASE AND ONALLOCATION IS INCREASED
		BUT ITS WORK ORDER SO WE HAVE TO INCERASE ONORDER ALSO BY QTY. CHILD ITEM HAVE ONLY WO IMPACT MEANS 
		ONHAND IS DECREASE AND ONALLOCATION IS INCREASED

		-- WHEN ASSEMBLY IS COMPLETED
		CURRENT INVENTORY STATUS FOR PARENT ITEM: ONHAND:1 ONORDER:0 ONALLOCATION:1 ONBACKORDER:0
		CURRENT INVENTORY STATUS FOR CHILD ITEM: ONHAND:2 ONORDER:0 ONALLOCATION:0 ONBACKORDER:0

		-- WHEN WORK ORDER IS CLOSED:
		CURRENT INVENTORY STATUS FOR PARENT ITEM: ONHAND:1 ONORDER:0 ONALLOCATION:0 ONBACKORDER:0
		CURRENT INVENTORY STATUS FOR CHILD ITEM: ONHAND:2 ONORDER:0 ONALLOCATION:0 ONBACKORDER:0
	*/
DECLARE @numWOID AS NUMERIC(18,0)
DECLARE @numParentWOID AS NUMERIC(18,0)
DECLARE	@numDomainID AS NUMERIC(18,0)
DECLARE @vcInstruction AS VARCHAR(1000)
DECLARE @bintCompliationDate DATETIME
DECLARE @numAssemblyQty FLOAT 
DECLARE @numWOStatus AS NUMERIC(18,0)
DECLARE @numWOAssignedTo AS NUMERIC(18,0)

SET XACT_ABORT ON;

BEGIN TRY 
BEGIN TRAN
	SELECT 
		@numWOID=numWOId,@numAssemblyQty=numQtyItemsReq,@numDomainID=numDomainID,
		@numWOStatus=ISNULL(numWOStatus,0),@vcInstruction=vcInstruction,
		@bintCompliationDate= bintCompliationDate, @numWOAssignedTo=numAssignedTo,@numParentWOID=ISNULL(numParentWOID,0)
	FROM 
		WorkOrder 
	WHERE 
		numOppId=@numOppID AND numOppItemID=@numOppItemID AND numItemCode=@numItemCode AND numWareHouseItemId=@numWarehouseItemID

	--IF WORK ORDER IS FOUND FOR ITEM AND QTY IS GREATER THEN 0 THEN PROCESS INVENTORY
	IF ISNULL(@numWOID,0) > 0 AND ISNULL(@numAssemblyQty,0) > 0
	BEGIN
		
		DECLARE @TEMPITEM AS TABLE
		(
			RowNo INT NOT NULL identity(1,1),
			numItemCode INT,
			numQty FLOAT,
			numOrgQtyRequired FLOAT,
			numWarehouseItemID INT,
			bitAssembly BIT
		)

		INSERT INTO 
			@TEMPITEM 
		SELECT 
			numChildItemID,
			numQtyItemsReq,
			numQtyItemsReq_Orig,
			numWareHouseItemId,
			Item.bitAssembly 
		FROM 
			WorkOrderDetails 
		INNER JOIN 
			Item 
		ON 
			WorkOrderDetails.numChildItemID=Item.numItemCode 
		WHERE 
			numWOId = @numWOID 
			AND ISNULL(numWareHouseItemId,0) > 0

		DECLARE @i AS INT = 1
		DECLARE @Count AS INT

		SELECT @Count = COUNT(RowNo) FROM @TEMPITEM

		--IF ASSEMBLY SUB ITEMS ARE AVAILABLE THEN UPDATE INVENTORY OF SUB ITEMS
		IF ISNULL(@Count,0) > 0
		BEGIN
			DECLARE @Description AS VARCHAR(500)
			DECLARE @numChildItemCode AS INT
			DECLARE @numChildItemQty AS FLOAT
			DECLARE @numChildOrgQtyRequired AS FLOAT
			DECLARE @bitChildIsAssembly AS BIT
			DECLARE @numQuantityToBuild AS FLOAT
			DECLARE @numChildItemWarehouseItemID AS INT
			DECLARE @numOnHand AS FLOAT
			DECLARE @numOnOrder AS FLOAT
			DECLARE @numOnAllocation AS FLOAT
			DECLARE @numOnBackOrder AS FLOAT

			--THIS BLOCKS UPDATE INVENTORY LEVEL OF TOP LEVEL PARENT ONLY AND NOT CALLED FOR ITS RECURSIVE CHILD
			IF @numParentWOID = 0
			BEGIN
				--GET ASSEMBY ITEM INVENTORY
				SELECT
					@numOnHand = ISNULL(numOnHand, 0),
					@numOnAllocation = ISNULL(numAllocation, 0),
					@numOnOrder = ISNULL(numOnOrder, 0),
					@numOnBackOrder = ISNULL(numBackOrder, 0)
				FROM
					WareHouseItems
				WHERE
					numWareHouseItemID = @numWarehouseItemID

				IF @tintMode = 1 --WO INSERT/EDIT
				BEGIN
					-- IF HAVE ENOUGH ITEM QTY IN WAREHOUSE THEN 
					-- DECREASE ONHAND BY QTY AND 
					-- INCREASE ONALLOCATION BY QTY
					-- ONORDER GOES UP BY ASSEMBLY QTY
					IF @numOnHand >= @numAssemblyQty 
					BEGIN      
						SET @numOnHand = @numOnHand - @numAssemblyQty
						SET @numOnOrder = @numOnOrder + @numAssemblyQty
						SET @numOnAllocation = @numOnAllocation + @numAssemblyQty

						UPDATE 
							WareHouseItems
						SET    
							numOnHand = @numOnHand,
							numOnOrder= @numOnOrder,
							numAllocation = @numOnAllocation,
							dtModified = GETDATE() 
						WHERE   
							numWareHouseItemID = @numWareHouseItemID                                                              
					END    
					-- IF DO NOT HAVE ENOUGH ITEM QTY IN WAREHOUSE THEN 
					-- INCREASE ONALLOCATION BY QTY WE HAVE IN WAREHOUSE AND
					-- REMAINING QTY AFTER ALLLOCATING FROM ONHAND WILL GO TO BACKORDER
					-- SET ONHAND TO 0 BECAUSE NOTHING LEFT NOW IN WAREHOUSE FOR ITEM 
					-- ONORDER GOES UP BY ASSEMBLY QTY				 
					ELSE IF @numOnHand < @numAssemblyQty 
					BEGIN      
						SET @numOnAllocation = @numOnAllocation + @numOnHand
						SET @numOnBackOrder = @numOnBackOrder + (@numAssemblyQty - @numOnHand)
						SET @numOnOrder = @numOnOrder + @numAssemblyQty
						SET @numOnHand = 0
 
						UPDATE 
							WareHouseItems
						SET    
							numAllocation = @numOnAllocation,
							numBackOrder = @numOnBackOrder,
							numOnHand = @numOnHand,		
							numOnOrder= @numOnOrder,				
							dtModified = GETDATE() 
						WHERE   
							numWareHouseItemID = @numWareHouseItemID                                     
					END 
				--UPDATE ONORDER QTY OF ASSEMBLY ITEM BY QTY
				END
				ELSE IF @tintMode = 2 --WO CLOSE
				BEGIN
					--REMOVE SHIPPED QTY FROM ORIGIAL QUANTITY
					SET @numAssemblyQty = ISNULL(@numAssemblyQty,0) - ISNULL(@QtyShipped,0)

					--RELASE ALLOCATION
					SET @numOnAllocation = @numOnAllocation - @numAssemblyQty 
											           
					UPDATE  
						WareHouseItems
					SET     
						numAllocation = @numOnAllocation,
						dtModified = GETDATE() 
					WHERE   
						numWareHouseItemID = @numWareHouseItemID   
				END
				ELSE IF @tintMode = 3 --WO REOPEN
				BEGIN
					--REMOVE SHIPPED QTY FROM ORIGIAL QUANTITY
					SET @numAssemblyQty = @numAssemblyQty - @QtyShipped
  
					--ADD ITEM TO ALLOCATION
					SET @numOnAllocation = @numOnAllocation + @numAssemblyQty
                                    
					UPDATE  
						WareHouseItems
					SET     
						numAllocation = @numOnAllocation,
						dtModified = GETDATE() 
					WHERE   
						numWareHouseItemID = @numWareHouseItemID
				END
				ELSE IF @tintMode = 4 --WO DELETE
				BEGIN
					-- CHECK WHETHER ANY QTY IS SHIPPED OR NOT
					IF @QtyShipped > 0
					BEGIN
						SET @numAssemblyQty = @numAssemblyQty - @QtyShipped
						--SET @numOnAllocation = @numOnAllocation + @QtyShipped 
					END 
	
					-- IF BACKORDER QTY IS GREATER THEN QTY TO REVERT THEN
					-- DECREASE BACKORDER QTY BY QTY TO REVERT
					IF @numAssemblyQty < @numOnBackOrder 
					BEGIN                  
						SET @numOnBackOrder = @numOnBackOrder - @numAssemblyQty
					END 
					-- IF BACKORDER QTY IS LESS THEN OR EQUAL TO QTY TO REVERT THEN
					-- DECREASE QTY TO REVERT BY QTY OF BACK ORDER AND
					-- SET BACKORDER QTY TO 0
					ELSE IF @numAssemblyQty >= @numOnBackOrder 
					BEGIN
						SET @numAssemblyQty = @numAssemblyQty - @numOnBackOrder
						SET @numOnBackOrder = 0
                        
						--REMOVE ITEM FROM ALLOCATION 
						IF (@numOnAllocation - @numAssemblyQty) >= 0
							SET @numOnAllocation = @numOnAllocation - @numAssemblyQty
						
						--ADD QTY TO ONHAND
						SET @numOnHand = @numOnHand + @numAssemblyQty
					END

					--THIS IF BLOCK SHOULD BE LAST OTHERWISE IT WILL CREATE PROBLEM IN INVENTORY LEVEL
					--IF WORK ORDER IS NOT COMPLETED THEN REVERT 
					--QTY FROM ONORDER BECAUSE WE HAVE TO REVERT
					IF @numWOStatus <> 23184
					BEGIN
						--DECREASE ONORDER BY ASSEMBLY QTY
						SET @numOnOrder = @numOnOrder - @numAssemblyQty
					END
					ELSE
					BEGIN
						--DECREASE ONHAND BY ASSEMBLY QTY
						--BECAUSE WHEN WORK ORDER IS COMPLETED ONHAND QTY OF PARENT IS INCREASED
						SET @numOnHand = @numOnHand - @numAssemblyQty
					END

					UPDATE 
						WareHouseItems
					SET    
						numOnHand = @numOnHand,
						numAllocation = @numOnAllocation,
						numBackOrder = @numOnBackOrder,
						numOnOrder = @numOnOrder,
						dtModified = GETDATE() 
					WHERE   
						numWareHouseItemID = @numWareHouseItemID				
				END
			END

			--CHILD ITEM INVENTORY IS CHANGED ONLY WHEN SALES ORDER WITH WORK ORDER IS INSERTED, EDITED (NON COMPLETE WORK ORDER) OR DELETED
			--THE INVENTORY MANGMENT FOR DELETE ACTION IS HANDLED FROM STORE PROCEDURE USP_DeleteAssemblyWOAndInventory
			IF @tintMode = 1 
			BEGIN
				
				WHILE @i <= @Count
				BEGIN
					DECLARE @numNewOppID AS NUMERIC(18,0) = 0
					DECLARE @QtyToBuild AS FLOAT
					DECLARE @numQtyShipped AS FLOAT

					--GET CHILD ITEM DETAIL FROM TEMP TABLE
					SELECT @numChildItemCode=numItemCode,@numChildItemWarehouseItemID=numWarehouseItemID,@numChildItemQty=numQty,@numChildOrgQtyRequired=numOrgQtyRequired,@bitChildIsAssembly=bitAssembly FROM @TEMPITEM WHERE RowNo = @i
					
					--GET CHILD ITEM CURRENT INVENTORY DETAIL
					SELECT  
						@numOnHand = ISNULL(numOnHand, 0),
						@numOnAllocation = ISNULL(numAllocation, 0),
						@numOnOrder = ISNULL(numOnOrder, 0),
						@numOnBackOrder = ISNULL(numBackOrder, 0)
					FROM    
						WareHouseItems
					WHERE   
						numWareHouseItemID = @numChildItemWarehouseItemID
					
					IF ISNULL(@numParentWOID,0) = 0
					BEGIN
						SET @Description='Items Allocated For SO-WO (Qty:' + CAST(@numChildItemQty AS VARCHAR(10)) + ' Shipped:' +  CAST((@QtyShipped * @numChildOrgQtyRequired) AS VARCHAR(10)) + ')'
					END
					ELSE
					BEGIN
						SET @Description='Items Allocated For SO-WO Work Order (Qty:' + CAST(@numChildItemQty AS VARCHAR(10)) + ' Shipped:' +  CAST((@QtyShipped * @numChildOrgQtyRequired) AS VARCHAR(10)) + ')'
					END
					
					SET @numChildItemQty = @numChildItemQty - (@QtyShipped * @numChildOrgQtyRequired)
						
					--IF CHILD ITEM IS ASSEMBLY THEN SAME LOGIC LIKE PARENT IS APPLIED
					IF ISNULL(@bitChildIsAssembly,0) = 1
					BEGIN
						-- IF HAVE ENOUGH ITEM QTY IN WAREHOUSE THEN 
						-- DECREASE ONHAND BY QTY AND 
						-- INCREASE ONALLOCATION BY QTY
						-- ONORDER GOES UP BY ASSEMBLY QTY
						IF @numOnHand >= @numChildItemQty 
						BEGIN      
							SET @numOnHand = @numOnHand - @numChildItemQty
							SET @numOnAllocation = @numOnAllocation + @numChildItemQty  
							
							--DONOT PLACE THIS LOGIC AT BOTTOM BECAUSE WORK ORDER ARE CREATED RECURSIVELY WHICH CREATE PROBLEM IN WAREHOUSE TRAKING ENTRIES
							--UPDATE WAREHOUSE INVENTORY AND WAREHOUSE HISTORY
							EXEC USP_UpdateInventoryAndTracking 
							@numOnHand=@numOnHand,
							@numOnAllocation=@numOnAllocation,
							@numOnBackOrder=@numOnBackOrder,
							@numOnOrder=@numOnOrder,
							@numWarehouseItemID=@numChildItemWarehouseItemID,
							@numReferenceID=@numOppID,
							@tintRefType=3,
							@numDomainID=@numDomainID,
							@numUserCntID=@numUserCntID,
							@Description=@Description                                                         
						END    
						-- IF DO NOT HAVE ENOUGH ITEM QTY IN WAREHOUSE THEN 
						-- INCREASE ONALLOCATION BY QTY WE HAVE IN WAREHOUSE AND
						-- REMAINING QTY AFTER ALLLOCATING FROM ONHAND WILL GO TO BACKORDER
						-- SET ONHAND TO 0 BECAUSE NOTHING LEFT NOW IN WAREHOUSE FOR ITEM 
						-- ONORDER GOES UP BY ASSEMBLY QTY				 
						ELSE IF @numOnHand < @numChildItemQty 
						BEGIN      
							SET @numOnAllocation = @numOnAllocation + @numOnHand
							SET @numOnBackOrder = @numOnBackOrder + (@numChildItemQty - @numOnHand)
							SET @QtyToBuild = (@numChildItemQty - @numOnHand)
							SET @numQtyShipped = (@QtyShipped * @numChildOrgQtyRequired)
							SET @numOnHand = 0   

							--DONOT PLACE THIS LOGIC AT BOTTOM BECAUSE WORK ORDER ARE CREATED RECURSIVELY WHICH CREATE PROBLEM IN WAREHOUSE TRAKING ENTRIES
							--UPDATE WAREHOUSE INVENTORY AND WAREHOUSE HISTORY
							EXEC USP_UpdateInventoryAndTracking 
							@numOnHand=@numOnHand,
							@numOnAllocation=@numOnAllocation,
							@numOnBackOrder=@numOnBackOrder,
							@numOnOrder=@numOnOrder,
							@numWarehouseItemID=@numChildItemWarehouseItemID,
							@numReferenceID=@numOppID,
							@tintRefType=3,
							@numDomainID=@numDomainID,
							@numUserCntID=@numUserCntID,
							@Description=@Description
								
							EXEC USP_WorkOrder_InsertRecursive @numOppID,@numOppItemID,@numChildItemCode,@QtyToBuild,@numChildItemWarehouseItemID,@numDomainID,@numUserCntID,@numQtyShipped,@numWOID,0
						END 
					END
					ELSE
					BEGIN
						-- IF HAVE ENOUGH ITEM QTY IN WAREHOUSE THEN 
						-- DECREASE ONHAND BY QTY AND 
						-- INCREASE ONALLOCATION BY QTY
						IF @numOnHand >= @numChildItemQty 
						BEGIN                                    
							SET @numOnHand = @numOnHand - @numChildItemQty                            
							SET @numOnAllocation = @numOnAllocation + @numChildItemQty 
							
							--DONOT PLACE THIS LOGIC AT BOTTOM BECAUSE WORK ORDER ARE CREATED RECURSIVELY WHICH CREATE PROBLEM IN WAREHOUSE TRAKING ENTRIES
							--UPDATE WAREHOUSE INVENTORY AND WAREHOUSE HISTORY
							EXEC USP_UpdateInventoryAndTracking 
							@numOnHand=@numOnHand,
							@numOnAllocation=@numOnAllocation,
							@numOnBackOrder=@numOnBackOrder,
							@numOnOrder=@numOnOrder,
							@numWarehouseItemID=@numChildItemWarehouseItemID,
							@numReferenceID=@numOppID,
							@tintRefType=3,
							@numDomainID=@numDomainID,
							@numUserCntID=@numUserCntID,
							@Description=@Description                                   
						END    
						-- IF DO NOT HAVE ENOUGH ITEM QTY IN WAREHOUSE THEN 
						-- INCREASE ONALLOCATION BY QTY WE HAVE IN WAREHOUSE AND
						-- REMAINING QTY AFTER ALLLOCATING FROM ONHAND WILL GO TO BACKORDER
						-- SET ONHAND TO 0 BECAUSE NOTHING LEFT NOW IN WAREHOUSE FOR ITEM 				 
						ELSE IF @numOnHand < @numChildItemQty 
						BEGIN     
								
							SET @numOnAllocation = @numOnAllocation + @numOnHand  
							SET @numOnBackOrder = @numOnBackOrder + (@numChildItemQty - @numOnHand) 
							SET @QtyToBuild = (@numChildItemQty - @numOnHand) 
							SET @numOnHand = 0 

							--DONOT PLACE THIS LOGIC AT BOTTOM BECAUSE WORK ORDER ARE CREATED RECURSIVELY WHICH CREATE PROBLEM IN WAREHOUSE TRAKING ENTRIES
							--UPDATE WAREHOUSE INVENTORY AND WAREHOUSE HISTORY
							EXEC USP_UpdateInventoryAndTracking 
							@numOnHand=@numOnHand,
							@numOnAllocation=@numOnAllocation,
							@numOnBackOrder=@numOnBackOrder,
							@numOnOrder=@numOnOrder,
							@numWarehouseItemID=@numChildItemWarehouseItemID,
							@numReferenceID=@numOppID,
							@tintRefType=3,
							@numDomainID=@numDomainID,
							@numUserCntID=@numUserCntID,
							@Description=@Description

							--CREATE PURCHASE ORDER FOR QUANTITY WHICH GOES ON BACKORDER
							EXEC USP_OpportunityMaster_CreatePO @numNewOppID OUTPUT,@numDomainID,@numUserCntID,@numChildItemCode,@QtyToBuild,@numChildItemWarehouseItemID,1,1,0
								
							UPDATE WorkOrderDetails SET numPOID= @numNewOppID WHERE numWOId=@numWOID AND numChildItemID=@numChildItemCode AND numWareHouseItemId=@numChildItemWarehouseItemID                                 
						END 
					END
 
					
					SET @i = @i + 1
				END
			END
		END

		UPDATE WorkOrder SET bitInventoryImpacted=1 WHERE numWOId=@numWOID
	END
COMMIT TRAN 
END TRY 
BEGIN CATCH		
    IF ( @@TRANCOUNT > 0 ) 
    BEGIN
        ROLLBACK TRAN
    END

	DECLARE @ErrorMessage NVARCHAR(4000);
    DECLARE @ErrorSeverity INT;
    DECLARE @ErrorState INT;

    SELECT @ErrorMessage = ERROR_MESSAGE(), @ErrorSeverity = ERROR_SEVERITY(), @ErrorState = ERROR_STATE();

    RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);
END CATCH	

SET XACT_ABORT OFF;
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO   
/* deducts Qty from allocation and Adds back to onhand */
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageInventoryPickList')
DROP PROCEDURE USP_ManageInventoryPickList
GO
CREATE PROCEDURE [dbo].[USP_ManageInventoryPickList]
	@numDomainID NUMERIC(18,0)
	,@numOppID NUMERIC(18,0)
	,@numOppBizDocID NUMERIC(18,0)
	,@numUserCntID AS NUMERIC(18,0)
AS
BEGIN
	IF ISNULL(@numUserCntID,0) = 0
	BEGIN
		RAISERROR('INVALID_USERID',16,1)
		RETURN
	END

	DECLARE @numItemCode NUMERIC(18,0)
	DECLARE @numOppItemID NUMERIC(18,0)
	DECLARE @bitWorkorder AS BIT
	DECLARE @numUnitHour FLOAT
	DECLARE @numWareHouseItemID NUMERIC(18,0)
	DECLARE @onHand AS FLOAT                                    
    DECLARE @onOrder AS FLOAT 
    DECLARE @onBackOrder AS FLOAT
    DECLARE @onAllocation AS FLOAT
	DECLARE @onReOrder AS FLOAT
    DECLARE @monAvgCost AS DECIMAL(20,5) 
    DECLARE @bitKitParent BIT
	DECLARE @numOrigUnits AS NUMERIC		
	DECLARE @description AS VARCHAR(100)
	DECLARE @bitAsset as BIT
	DECLARE @dtItemReceivedDate AS DATETIME=NULL

	DECLARE @j INT = 0
	DECLARE @ChildCount AS INT
	DECLARE @vcChildDescription AS VARCHAR(300)
	DECLARE @numChildAllocation AS FLOAT
	DECLARE @numChildBackOrder AS FLOAT
	DECLARE @numChildOnHand AS FLOAT
	DECLARE @numOppChildItemID AS INT
	DECLARE @numChildItemCode AS INT
	DECLARE @bitChildIsKit AS BIT
	DECLARE @numChildWarehouseItemID AS INT
	DECLARE @numChildQty AS FLOAT
	DECLARE @numChildQtyShipped AS FLOAT

	DECLARE @TempKitSubItems TABLE
	(
		ID INT,
		numOppChildItemID NUMERIC(18,0),
		numChildItemCode NUMERIC(18,0),
		bitChildIsKit BIT,
		numChildWarehouseItemID NUMERIC(18,0),
		numChildQty FLOAT,
		numChildQtyShipped FLOAT
	)

	DECLARE @k AS INT = 1
	DECLARE @CountKitChildItems AS INT
	DECLARE @vcKitChildDescription AS VARCHAR(300)
	DECLARE @numKitChildAllocation AS FLOAT
	DECLARE @numKitChildBackOrder AS FLOAT
	DECLARE @numKitChildOnHand AS FLOAT
	DECLARE @numOppKitChildItemID AS NUMERIC(18,0)
	DECLARE @numKitChildItemCode AS NUMERIC(18,0)
	DECLARE @numKitChildWarehouseItemID AS NUMERIC(18,0)
	DECLARE @numKitChildQty AS FLOAT
	DECLARE @numKitChildQtyShipped AS FLOAT

	DECLARE @TempKitChildKitSubItems TABLE
	(
		ID INT,
		numOppKitChildItemID NUMERIC(18,0),
		numKitChildItemCode NUMERIC(18,0),
		numKitChildWarehouseItemID NUMERIC(18,0),
		numKitChildQty FLOAT,
		numKitChildQtyShipped FLOAT
	)

	DECLARE @TempPickListItems TABLE
	(
		ID INT IDENTITY(1,1)
		,numOppItemID NUMERIC(18,0)
		,numItemCode NUMERIC(18,0)
		,numUnitHour FLOAT
		,numWarehouseItemID NUMERIC(18,0)
		,bitWorkorder BIT
		,bitAsset BIT
		,bitKitParent BIT
		,monAvgCost DECIMAL(20,5)
	)

	INSERT INTO @TempPickListItems
	(
		numOppItemID
		,numItemCode
		,numUnitHour
		,numWarehouseItemID
		,bitWorkorder
		,bitAsset
		,bitKitParent
		,monAvgCost
	)
	SELECT
		OI.numoppitemtCode
		,I.numItemCode
		,OBDI.numUnitHour
		,OI.numWarehouseItmsID
		,ISNULL(OI.bitWorkorder,0)
		,ISNULL(I.bitAsset,0)
		,ISNULL(I.bitKitParent,0)
		,(CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(monAverageCost, 0) END)
	FROM
		OpportunityBizDocItems OBDI
	INNER JOIN
		OpportunityItems OI
	ON
		OBDI.numOppItemID = OI.numoppitemtCode
	INNER JOIN 
		WareHouseItems WI 
	ON 
		OI.numWarehouseItmsID=WI.numWareHouseItemID
    INNER JOIN 
		Item I 
	ON 
		OI.numItemCode = I.numItemCode
    WHERE  
		charitemtype='P'
		AND OBDI.numOppBizDocID=@numOppBizDocID
        AND ISNULL(OI.bitDropShip,0) = 0
		AND ISNULL(OBDI.numUnitHour,0) > 0 
    ORDER BY 
		OI.numoppitemtCode

	DECLARE @i AS INT = 1
	DECLARE @iCount AS INT

	SELECT @iCount=COUNT(*) FROM @TempPickListItems

	WHILE @i <= @iCount
	BEGIN
		SELECT
			@numOppItemID=numOppItemID
			,@numItemCode = numItemCode
			,@numUnitHour = numUnitHour
			,@numWareHouseItemID=numWarehouseItemID
			,@bitKitParent=bitKitParent
			,@bitWorkOrder=bitWorkorder
			,@bitAsset=bitAsset
			,@monAvgCost=monAvgCost
		FROM
			@TempPickListItems
		WHERE
			ID=@i

		SELECT  
			@onHand = ISNULL(numOnHand, 0),
			@onAllocation = ISNULL(numAllocation, 0),
			@onOrder = ISNULL(numOnOrder, 0),
			@onBackOrder = ISNULL(numBackOrder, 0),
			@onReOrder = ISNULL(numReorder,0)
		FROM 
			WareHouseItems
		WHERE
			 numWareHouseItemID = @numWareHouseItemID

		/* When QtyShipped has been updated form sales fulfillment then it need to be deducted */
		SET @description='SO Pick List insert/edit (Qty:' + CAST(@numUnitHour AS VARCHAR(10)) + ' Shipped:0)'
				
		IF @bitKitParent = 1 
		BEGIN
			-- CLEAR DATA OF PREVIOUS ITERATION
			DELETE FROM @TempKitSubItems

			-- GET KIT SUB ITEMS DETAIL
			INSERT INTO @TempKitSubItems
			(
				ID,
				numOppChildItemID,
				numChildItemCode,
				bitChildIsKit,
				numChildWarehouseItemID,
				numChildQty,
				numChildQtyShipped
			)
			SELECT 
				ROW_NUMBER() OVER(ORDER BY numOppChildItemID),
				ISNULL(numOppChildItemID,0),
				ISNULL(I.numItemCode,0),
				ISNULL(I.bitKitParent,0),
				ISNULL(numWareHouseItemId,0),
				((ISNULL(numQtyItemsReq_Orig,0) * ISNULL(dbo.fn_UOMConversion(OKI.numUOMId,OKI.numChildItemID,@numDomainID,I.numBaseUnit),1)) * @numUnitHour),
				ISNULL(numQtyShipped,0)
			FROM 
				OpportunityKitItems OKI 
			JOIN 
				Item I 
			ON 
				OKI.numChildItemID=I.numItemCode    
			WHERE 
				charitemtype='P' 
				AND ISNULL(numWareHouseItemId,0) > 0 
				AND OKI.numOppID=@numOppID 
				AND OKI.numOppItemID=@numOppItemID 

			SET @j = 1
			SELECT @ChildCount=COUNT(*) FROM @TempKitSubItems

			--LOOP OVER ALL CHILD ITEMS AND RELESE ALLOCATION
			WHILE @j <= @ChildCount
			BEGIN
				SELECT
					@numOppChildItemID=numOppChildItemID,
					@numChildItemCode=numChildItemCode,
					@bitChildIsKit=bitChildIsKit,
					@numChildWarehouseItemID=numChildWarehouseItemID,
					@numChildQty=numChildQty,
					@numChildQtyShipped=numChildQtyShipped
				FROM
					@TempKitSubItems
				WHERE 
					ID = @j

				SELECT @numChildOnHand=ISNULL(numOnHand,0) FROM WareHouseItems WHERE numWareHouseItemID=@numChildWarehouseItemID

				-- IF CHILD ITEM IS KIT THEN LOOK OUT FOR ITS CHILD ITEMS WHICH ARE STORED IN OpportunityKitChildItems TABLE
				IF @bitChildIsKit = 1
				BEGIN
					-- IF KIT IS ADDED WITHIN KIT THEN THIS SUB KIT HAS ITS OWN ITEM CONFIGURATION WHICH ID STORED IN OpportunityKitChildItems
					IF EXISTS (SELECT numOppKitChildItemID FROM OpportunityKitChildItems WHERE numOppID=@numOppId AND numOppItemID=@numOppItemID AND numOppChildItemID=@numOppChildItemID)
					BEGIN
						-- CLEAR DATA OF PREVIOUS ITERATION
						DELETE FROM @TempKitChildKitSubItems

						-- GET SUB KIT SUB ITEMS DETAIL
						INSERT INTO @TempKitChildKitSubItems
						(
							ID,
							numOppKitChildItemID,
							numKitChildItemCode,
							numKitChildWarehouseItemID,
							numKitChildQty,
							numKitChildQtyShipped
						)
						SELECT 
							ROW_NUMBER() OVER(ORDER BY numOppKitChildItemID),
							ISNULL(numOppKitChildItemID,0),
							ISNULL(OKCI.numItemID,0),
							ISNULL(OKCI.numWareHouseItemId,0),
							((ISNULL(OKCI.numQtyItemsReq_Orig,0) * ISNULL(dbo.fn_UOMConversion(OKCI.numUOMId,OKCI.numItemID,@numDomainID,I.numBaseUnit),1)) * @numChildQty),
							ISNULL(numQtyShipped,0)
						FROM 
							OpportunityKitChildItems OKCI 
						JOIN 
							Item I 
						ON 
							OKCI.numItemID=I.numItemCode    
						WHERE 
							charitemtype='P' 
							AND ISNULL(numWareHouseItemId,0) > 0 
							AND OKCI.numOppID=@numOppID 
							AND OKCI.numOppItemID=@numOppItemID 
							AND OKCI.numOppChildItemID = @numOppChildItemID

						SET @k = 1
						SELECT @CountKitChildItems=COUNT(*) FROM @TempKitChildKitSubItems

						WHILE @k <= @CountKitChildItems
						BEGIN
							SELECT
								@numOppKitChildItemID=numOppKitChildItemID,
								@numKitChildItemCode=numKitChildItemCode,
								@numKitChildWarehouseItemID=numKitChildWarehouseItemID,
								@numKitChildQty=numKitChildQty,
								@numKitChildQtyShipped=numKitChildQtyShipped
							FROM
								@TempKitChildKitSubItems
							WHERE 
								ID = @k

							UPDATE
								WarehouseItems
							SET 
								numOnHand = (CASE WHEN ISNULL(numOnHand,0) >= @numKitChildQty THEN ISNULL(numOnHand,0) - @numKitChildQty ELSE 0 END)
								,numAllocation = (CASE WHEN ISNULL(numOnHand,0) >= @numKitChildQty THEN ISNULL(numAllocation,0) + @numKitChildQty ELSE ISNULL(numAllocation,0) + ISNULL(numOnHand,0) END)
								,numBackOrder = (CASE WHEN ISNULL(numOnHand,0) >= @numKitChildQty THEN ISNULL(numBackOrder,0) ELSE ISNULL(numBackOrder,0) + (@numKitChildQty - ISNULL(numOnHand,0)) END)
								,dtModified = GETDATE()
							WHERE
								numWareHouseItemID=@numKitChildWarehouseItemID

							SET @vcKitChildDescription = 'SO Pick List Child Kit insert/edit (Qty:' + CAST(@numKitChildQty AS VARCHAR(10)) + ' Shipped:0)'

							EXEC dbo.USP_ManageWareHouseItems_Tracking
								@numWareHouseItemID = @numKitChildWarehouseItemID,
								@numReferenceID = @numOppId,
								@tintRefType = 3,
								@vcDescription = @vcKitChildDescription,
								@numModifiedBy = @numUserCntID,
								@numDomainID = @numDomainID	 

							SET @k = @k + 1
						END
					END

					SET @description='SO Pick List Kit insert/edit (Qty:' + CAST(@numChildQty AS VARCHAR(10)) + ' Shipped:0)'

					EXEC dbo.USP_ManageWareHouseItems_Tracking
						@numWareHouseItemID = @numChildWarehouseItemID, --  numeric(9, 0)
						@numReferenceID = @numOppID, --  numeric(9, 0)
						@tintRefType = 3, --  tinyint
						@vcDescription = @description, --  varchar(100)
						@numModifiedBy = @numUserCntID,
						@dtRecordDate =  @dtItemReceivedDate,
						@numDomainID = @numDomainID
				END
				ELSE
				BEGIN
					UPDATE
						WarehouseItems
					SET 
						numOnHand = (CASE WHEN ISNULL(numOnHand,0) >= @numChildQty THEN ISNULL(numOnHand,0) - @numChildQty ELSE 0 END)
						,numAllocation = (CASE WHEN ISNULL(numOnHand,0) >= @numChildQty THEN ISNULL(numAllocation,0) + @numChildQty ELSE ISNULL(numAllocation,0) + ISNULL(numOnHand,0) END)
						,numBackOrder = (CASE WHEN ISNULL(numOnHand,0) >= @numChildQty THEN ISNULL(numBackOrder,0) ELSE ISNULL(numBackOrder,0) + (@numChildQty - ISNULL(numOnHand,0)) END)
						,dtModified = GETDATE()
					WHERE
						numWareHouseItemID=@numChildWarehouseItemID

					SET @vcChildDescription = 'SO Pick List Kit insert/edit (Qty:' + CAST(@numChildQty AS VARCHAR(10)) + ' Shipped:0)'

					EXEC dbo.USP_ManageWareHouseItems_Tracking
						@numWareHouseItemID = @numChildWarehouseItemID,
						@numReferenceID = @numOppId,
						@tintRefType = 3,
						@vcDescription = @vcChildDescription,
						@numModifiedBy = @numUserCntID,
						@numDomainID = @numDomainID
				END

				SET @j = @j + 1
			END
		END 
		ELSE IF @bitWorkOrder = 1
		BEGIN
			IF EXISTS (SELECT numWOID FROM WorkOrder WHERE numOppID=@numOppID AND numOppItemID=@numOppItemID AND numItemCode=@numItemCode AND numWareHouseItemId=@numWareHouseItemID AND numWOStatus <> 23184)
			BEGIN
				SET @description='SO-WO Pick List insert/edit (Qty:' + CAST(@numUnitHour AS VARCHAR(10)) + ' Shipped:0)'
				-- MANAGE INVENTOY OF ASSEMBLY ITEMS
				EXEC USP_ManageInventoryAssemblyWO @numUserCntID,@numOppID,@numOppItemID,@numItemCode,@numWareHouseItemID,0,1
			END
			ELSE
			BEGIN
				SET @description='SO-WO Pick List insert/edit (Qty:' + CAST(@numUnitHour AS VARCHAR(10)) + ' Shipped:0)'
                                      
				IF @onHand >= @numUnitHour 
				BEGIN                                    
					SET @onHand = @onHand - @numUnitHour                            
					SET @onAllocation = @onAllocation + @numUnitHour                                    
				END                                    
				ELSE IF @onHand < @numUnitHour 
				BEGIN                                    
					SET @onAllocation = @onAllocation + @onHand                                    
					SET @onBackOrder = @onBackOrder + @numUnitHour - @onHand                                    
					SET @onHand = 0                                    
				END    
			                                 
				IF @bitAsset=0--Not Asset
				BEGIN 
					UPDATE  WareHouseItems
					SET     numOnHand = @onHand,
							numAllocation = @onAllocation,
							numBackOrder = @onBackOrder,dtModified = GETDATE() 
					WHERE   numWareHouseItemID = @numWareHouseItemID          
				END
			END
		END
		ELSE
		BEGIN
			IF @onHand >= @numUnitHour 
			BEGIN                                    
				SET @onHand = @onHand - @numUnitHour                            
				SET @onAllocation = @onAllocation + @numUnitHour                                    
			END                                    
			ELSE IF @onHand < @numUnitHour 
			BEGIN                                    
				SET @onAllocation = @onAllocation + @onHand                                    
				SET @onBackOrder = @onBackOrder + @numUnitHour - @onHand                                    
				SET @onHand = 0                                    
			END    
			                                 
			IF @bitAsset=0--Not Asset
			BEGIN 
				UPDATE
					WareHouseItems
				SET
					numOnHand = @onHand
					,numAllocation = @onAllocation
					,numBackOrder = @onBackOrder
					,dtModified = GETDATE() 
				WHERE 
					numWareHouseItemID = @numWareHouseItemID          
			END
		END  
            
		IF @numWareHouseItemID>0 AND @description <> 'DO NOT ADD WAREHOUSE TRACKING'
		BEGIN
			EXEC dbo.USP_ManageWareHouseItems_Tracking
				@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
				@numReferenceID = @numOppID, --  numeric(9, 0)
				@tintRefType = 3, --  tinyint
				@vcDescription = @description, --  varchar(100)
				@numModifiedBy = @numUserCntID,
				@dtRecordDate =  @dtItemReceivedDate,
				@numDomainID = @numDomainID
		END

		SET @i = @i + 1
	END
END
GO
GO
/****** Object:  StoredProcedure [dbo].[USP_ManageItemsAndKits]    Script Date: 02/15/2010 21:44:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Anoop Jayaraj                                                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_manageitemsandkits')
DROP PROCEDURE usp_manageitemsandkits
GO
CREATE PROCEDURE [dbo].[USP_ManageItemsAndKits]                                                                
@numItemCode as numeric(9) output,                                                                
@vcItemName as varchar(300),                                                                
@txtItemDesc as varchar(1000),                                                                
@charItemType as char(1),                                                                
@monListPrice as DECIMAL(20,5),                                                                
@numItemClassification as numeric(9),                                                                
@bitTaxable as bit,                                                                
@vcSKU as varchar(50),                                                                                                                                      
@bitKitParent as bit,                                                                                   
--@dtDateEntered as datetime,                                                                
@numDomainID as numeric(9),                                                                
@numUserCntID as numeric(9),                                                                                                  
@numVendorID as numeric(9),                                                            
@bitSerialized as bit,                                               
@strFieldList as text,                                            
@vcModelID as varchar(200)='' ,                                            
@numItemGroup as numeric(9)=0,                                      
@numCOGSChartAcntId as numeric(9)=0,                                      
@numAssetChartAcntId as numeric(9)=0,                                      
@numIncomeChartAcntId as numeric(9)=0,                            
@monAverageCost as DECIMAL(20,5),                          
@monLabourCost as DECIMAL(20,5),                  
@fltWeight as float,                  
@fltHeight as float,                  
@fltLength as float,                  
@fltWidth as float,                  
@bitFreeshipping as bit,                
@bitAllowBackOrder as bit,              
@UnitofMeasure as varchar(30)='',              
@strChildItems as text,            
@bitShowDeptItem as bit,              
@bitShowDeptItemDesc as bit,        
@bitCalAmtBasedonDepItems as bit,    
@bitAssembly as BIT,
@intWebApiId INT=null,
@vcApiItemId AS VARCHAR(50)=null,
@numBarCodeId as VARCHAR(25)=null,
@vcManufacturer as varchar(250)=NULL,
@numBaseUnit AS NUMERIC(18)=0,
@numPurchaseUnit AS NUMERIC(18)=0,
@numSaleUnit AS NUMERIC(18)=0,
@bitLotNo as BIT,
@IsArchieve AS BIT,
@numItemClass as numeric(9)=0,
@tintStandardProductIDType AS TINYINT=0,
@vcExportToAPI VARCHAR(50),
@numShipClass NUMERIC,
@ProcedureCallFlag SMALLINT =0,
@vcCategories AS VARCHAR(2000) = '',
@bitAllowDropShip AS BIT=0,
@bitArchiveItem AS BIT = 0,
@bitAsset AS BIT = 0,
@bitRental AS BIT = 0,
@numCategoryProfileID AS NUMERIC(18,0) = 0,
@bitVirtualInventory BIT,
@bitContainer BIT,
@numContainer NUMERIC(18,0)=0,
@numNoItemIntoContainer NUMERIC(18,0),
@bitMatrix BIT = 0 ,
@vcItemAttributes VARCHAR(2000) = '',
@numManufacturer NUMERIC(18,0),
@vcASIN as varchar(50)
AS
BEGIN     
BEGIN TRY
BEGIN TRANSACTION

	SET @vcManufacturer = CASE WHEN ISNULL(@numManufacturer,0) > 0 THEN ISNULL((SELECT vcCompanyName FROM DivisionMaster INNER JOIN CompanyInfo ON DivisionMaster.numCompanyID=CompanyInfo.numCompanyId WHERE numDivisionID=@numManufacturer),'')  ELSE ISNULL(@vcManufacturer,'') END

	DECLARE @bitAttributesChanged AS BIT  = 1
	DECLARE @bitOppOrderExists AS BIT = 0

	IF ISNULL(@numItemCode,0) > 0
	BEGIN
		IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityItems OI WHERE OI.numItemCode =@numItemCode) > 0
		BEGIN
			SET @bitOppOrderExists = 1
		END
		ELSE IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityKitItems OI WHERE OI.numChildItemID =@numItemCode) > 0
		BEGIN
			SET @bitOppOrderExists = 1
		END
		ELSE IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityKitChildItems OI WHERE OI.numItemID =@numItemCode) > 0
		BEGIN
			SET @bitOppOrderExists = 1
		END
	END
	ELSE 
		BEGIN
		   IF (ISNULL(@vcItemName,'') = '')
			   BEGIN
				   RAISERROR('ITEM_NAME_NOT_SELECTED',16,1)
				   RETURN
			   END
           ELSE IF (ISNULL(@charItemType,'') = '0')
			   BEGIN
				   RAISERROR('ITEM_TYPE_NOT_SELECTED',16,1)
				   RETURN
			   END
           ELSE
		   BEGIN 
		      If (@charItemType = 'P')
			  BEGIN
				IF ((SELECT COUNT(*) FROM Chart_Of_Accounts WHERE numDomainId=@numDomainID AND numAccountId=@numAssetChartAcntId) = 0)
				BEGIN
					RAISERROR('INVALID_ASSET_ACCOUNT',16,1)
					RETURN
				END
				
			  END

				-- This is common check for CharItemType P and Other
				IF ((SELECT COUNT(*) FROM Chart_Of_Accounts WHERE numDomainId=@numDomainID AND numAccountId=@numCOGSChartAcntId) = 0)
				BEGIN
					RAISERROR('INVALID_COGS_ACCOUNT',16,1)
					RETURN
				END

				IF ((SELECT COUNT(*) FROM Chart_Of_Accounts WHERE numDomainId=@numDomainID AND numAccountId=@numIncomeChartAcntId) = 0)
				BEGIN
					RAISERROR('INVALID_INCOME_ACCOUNT',16,1)
					RETURN
				END
		   END
		END
	--INSERT CUSTOM FIELDS BASE ON WAREHOUSEITEMS
	DECLARE @hDoc AS INT     
	                                                                        	
	DECLARE @TempTable TABLE 
	(
		ID INT IDENTITY(1,1),
		Fld_ID NUMERIC(18,0),
		Fld_Value NUMERIC(18,0)
	)   

	IF ISNULL(@bitMatrix,0) = 1 AND ISNULL(@numItemGroup,0) = 0
	BEGIN
		RAISERROR('ITEM_GROUP_NOT_SELECTED',16,1)
		RETURN
	END
	ELSE IF ISNULL(@bitMatrix,0) = 1 AND LEN(ISNULL(@vcItemAttributes,'')) = 0 
	BEGIN
		RAISERROR('ITEM_ATTRIBUTES_NOT_SELECTED',16,1)
		RETURN
	END
	ELSE IF ISNULL(@bitMatrix,0) = 1 
	BEGIN
		DECLARE @bitDuplicate AS BIT = 0
	
		IF DATALENGTH(ISNULL(@vcItemAttributes,''))>2
		BEGIN
			EXEC sp_xml_preparedocument @hDoc OUTPUT, @vcItemAttributes
	                                      
			INSERT INTO @TempTable 
			(
				Fld_ID,
				Fld_Value
			)    
			SELECT
				*          
			FROM 
				OPENXML (@hDoc,'/NewDataSet/CusFlds',2)                                                  
			WITH 
				(Fld_ID NUMERIC(18,0),Fld_Value NUMERIC(18,0)) 
			ORDER BY 
				Fld_ID       

			DECLARE @strAttribute VARCHAR(500) = ''
			DECLARE @i AS INT = 1
			DECLARE @COUNT AS INT 
			DECLARE @numFldID AS NUMERIC(18,0)
			DECLARE @numFldValue AS NUMERIC(18,0)
			SELECT @COUNT = COUNT(*) FROM @TempTable

			WHILE @i <= @COUNT
			BEGIN
				SELECT @numFldID=Fld_ID,@numFldValue=Fld_Value FROM @TempTable WHERE ID=@i

				IF ISNUMERIC(@numFldValue)=1
				BEGIN
					SELECT @strAttribute=@strAttribute+Fld_label FROM CFW_Fld_Master WHERE Fld_id=@numFldID AND Grp_id = 9
                
					IF LEN(@strAttribute) > 0
					BEGIN
						SELECT @strAttribute=@strAttribute+':'+ vcData +',' FROM ListDetails WHERE numListItemID=@numFldValue
					END
				END

				SET @i = @i + 1
			END

			SET @strAttribute = SUBSTRING(@strAttribute,0,LEN(@strAttribute))

			IF
				(SELECT
					COUNT(*)
				FROM
					Item
				INNER JOIN
					ItemAttributes
				ON
					Item.numItemCode = ItemAttributes.numItemCode
					AND ItemAttributes.numDomainID = @numDomainID                     
				WHERE
					Item.numItemCode <> @numItemCode
					AND Item.vcItemName = @vcItemName
					AND Item.numItemGroup = @numItemGroup
					AND Item.numDomainID = @numDomainID
					AND dbo.fn_GetItemAttributes(@numDomainID,Item.numItemCode) = @strAttribute
				) > 0
			BEGIN
				RAISERROR('ITEM_WITH_ATTRIBUTES_ALREADY_EXISTS',16,1)
				RETURN
			END

			If ISNULL(@numItemCode,0) > 0 AND dbo.fn_GetItemAttributes(@numDomainID,@numItemCode) = @strAttribute
			BEGIN
				SET @bitAttributesChanged = 0
			END

			EXEC sp_xml_removedocument @hDoc 
		END	
	END
	ELSE IF ISNULL(@bitMatrix,0) = 0
	BEGIN
		-- REMOVE ATTRIBUTES CONFIGURATION OF ITEM AND WAREHOUSE
		DELETE FROM ItemAttributes WHERE numDomainID=@numDomainID AND numItemCode=@numItemCode
	END 
	
	IF ISNULL(@numItemGroup,0) > 0 AND ISNULL(@bitMatrix,0) = 1
	BEGIN
		IF (SELECT COUNT(*) FROM ItemGroupsDTL WHERE numItemGroupID = @numItemGroup AND tintType=2) = 0
		BEGIN
			RAISERROR('ITEM_GROUP_NOT_CONFIGURED',16,1)
			RETURN
		END
		ELSE IF EXISTS (SELECT 
							CFM.Fld_id
						FROM 
							CFW_Fld_Master CFM
						LEFT JOIN
							ListDetails LD
						ON
							CFM.numlistid = LD.numListID
						WHERE
							CFM.numDomainID = @numDomainID
							AND CFM.Fld_id IN (SELECT numOppAccAttrID FROM ItemGroupsDTL WHERE numItemGroupID=@numItemGroup AND tintType = 2)
						GROUP BY
							CFM.Fld_id HAVING COUNT(LD.numListItemID) = 0)
		BEGIN
			RAISERROR('ITEM_GROUP_NOT_CONFIGURED',16,1)
			RETURN
		END
	END

	IF @numCOGSChartAcntId=0 SET @numCOGSChartAcntId=NULL
	IF @numAssetChartAcntId=0 SET @numAssetChartAcntId=NULL  
	IF @numIncomeChartAcntId=0 SET @numIncomeChartAcntId=NULL     

	DECLARE @ParentSKU VARCHAR(50) = @vcSKU                        
	DECLARE @ItemID AS NUMERIC(9)
	DECLARE @cnt AS INT

	IF ISNULL(@charItemType,'') = '' OR ISNULL(@charItemType,'') = '0'  
		SET @charItemType = 'N'

	IF @intWebApiId = 2 AND LEN(ISNULL(@vcApiItemId, '')) > 0 AND ISNULL(@numItemGroup, 0) > 0 
    BEGIN 
        -- check wether this id already Mapped in ITemAPI 
        SELECT 
			@cnt = COUNT([numItemID])
        FROM 
			[ItemAPI]
        WHERE 
			[WebApiId] = @intWebApiId
            AND [numDomainId] = @numDomainID
            AND [vcAPIItemID] = @vcApiItemId

        IF @cnt > 0 
        BEGIN
            SELECT 
				@ItemID = [numItemID]
            FROM 
				[ItemAPI]
            WHERE 
				[WebApiId] = @intWebApiId
                AND [numDomainId] = @numDomainID
                AND [vcAPIItemID] = @vcApiItemId
        
			--Update Existing Product coming from API
            SET @numItemCode = @ItemID
        END
    END
	ELSE IF @intWebApiId > 1 AND LEN(ISNULL(@vcSKU, '')) > 0 
    BEGIN
        SET @ParentSKU = @vcSKU
		 
        SELECT 
			@cnt = COUNT([numItemCode])
        FROM 
			[Item]
        WHERE 
			[numDomainId] = @numDomainID
            AND (vcSKU = @vcSKU OR @vcSKU IN (SELECT vcWHSKU FROM dbo.WareHouseItems WHERE numItemID = Item.[numItemCode] AND vcWHSKU = @vcSKU))
            
		IF @cnt > 0 
        BEGIN
            SELECT 
				@ItemID = [numItemCode],
                @ParentSKU = vcSKU
            FROM 
				[Item]
            WHERE 
				[numDomainId] = @numDomainID
                AND (vcSKU = @vcSKU OR @vcSKU IN (SELECT vcWHSKU FROM dbo.WareHouseItems WHERE numItemID = Item.[numItemCode] AND vcWHSKU = @vcSKU))

            SET @numItemCode = @ItemID
        END
		ELSE 
		BEGIN
			-- check wether this id already Mapped in ITemAPI 
			SELECT 
				@cnt = COUNT([numItemID])
			FROM 
				[ItemAPI]
			WHERE 
				[WebApiId] = @intWebApiId
				AND [numDomainId] = @numDomainID
				AND [vcAPIItemID] = @vcApiItemId
                    
			IF @cnt > 0 
			BEGIN
				SELECT 
					@ItemID = [numItemID]
				FROM 
					[ItemAPI]
				WHERE 
					[WebApiId] = @intWebApiId
					AND [numDomainId] = @numDomainID
					AND [vcAPIItemID] = @vcApiItemId
        
				--Update Existing Product coming from API
				SET @numItemCode = @ItemID
			END
		END
    END
                                                         
	IF @numItemCode=0 OR @numItemCode IS NULL
	BEGIN
		INSERT INTO Item 
		(                                             
			vcItemName,txtItemDesc,charItemType,monListPrice,numItemClassification,bitTaxable,vcSKU,bitKitParent,
			numVendorID,numDomainID,numCreatedBy,bintCreatedDate,bintModifiedDate,numModifiedBy,bitSerialized,
			vcModelID,numItemGroup,numCOGsChartAcntId,numAssetChartAcntId,numIncomeChartAcntId,monAverageCost,                          
			monCampaignLabourCost,fltWeight,fltHeight,fltWidth,fltLength,bitFreeShipping,bitAllowBackOrder,vcUnitofMeasure,
			bitShowDeptItem,bitShowDeptItemDesc,bitCalAmtBasedonDepItems,bitAssembly,numBarCodeId,vcManufacturer,numBaseUnit,
			numPurchaseUnit,numSaleUnit,bitLotNo,IsArchieve,numItemClass,tintStandardProductIDType,vcExportToAPI,numShipClass,
			bitAllowDropShip,bitArchiveItem,bitAsset,bitRental,bitVirtualInventory,bitContainer,numContainer,numNoItemIntoContainer,bitMatrix,numManufacturer,vcASIN
		)                                                              
		VALUES                                                                
		(                                                                
			@vcItemName,@txtItemDesc,@charItemType,@monListPrice,@numItemClassification,@bitTaxable,@ParentSKU,@bitKitParent,
			@numVendorID,@numDomainID,@numUserCntID,getutcdate(),getutcdate(),@numUserCntID,@bitSerialized,@vcModelID,@numItemGroup,                                      
			@numCOGsChartAcntId,@numAssetChartAcntId,@numIncomeChartAcntId,@monAverageCost,@monLabourCost,@fltWeight,@fltHeight,@fltWidth,                  
			@fltLength,@bitFreeshipping,@bitAllowBackOrder,@UnitofMeasure,@bitShowDeptItem,@bitShowDeptItemDesc,@bitCalAmtBasedonDepItems,    
			@bitAssembly,@numBarCodeId,@vcManufacturer,@numBaseUnit,@numPurchaseUnit,@numSaleUnit,@bitLotNo,@IsArchieve,@numItemClass,
			@tintStandardProductIDType,@vcExportToAPI,@numShipClass,@bitAllowDropShip,@bitArchiveItem ,@bitAsset,@bitRental,
			@bitVirtualInventory,@bitContainer,@numContainer,@numNoItemIntoContainer,@bitMatrix,@numManufacturer,@vcASIN
		)                                             
 
		SET @numItemCode = SCOPE_IDENTITY()
  
		IF  @intWebApiId > 1 
		BEGIN
			 -- insert new product
			 --insert this id into linking table
			 INSERT INTO [ItemAPI] (
	 			[WebApiId],
	 			[numDomainId],
	 			[numItemID],
	 			[vcAPIItemID],
	 			[numCreatedby],
	 			[dtCreated],
	 			[numModifiedby],
	 			[dtModified],vcSKU
			 ) VALUES     
			 (
				@intWebApiId,
				@numDomainID,
				@numItemCode,
				@vcApiItemId,
	 			@numUserCntID,
	 			GETUTCDATE(),
	 			@numUserCntID,
	 			GETUTCDATE(),@vcSKU
	 		) 
		END
 
		IF @charItemType = 'P' AND 1 = (CASE WHEN ISNULL(@numItemGroup,0) > 0 THEN (CASE WHEN ISNULL(@bitMatrix,0)=1 THEN 1 ELSE 0 END) ELSE 1 END)
		BEGIN
			
			DECLARE @TEMPWarehouse TABLE
			(
				ID INT IDENTITY(1,1)
				,numWarehouseID NUMERIC(18,0)
			)
		
			INSERT INTO @TEMPWarehouse (numWarehouseID) SELECT numWareHouseID FROM Warehouses WHERE numDomainID=@numDomainID

			DECLARE @j AS INT = 1
			DECLARE @jCount AS INT
			DECLARE @numTempWarehouseID NUMERIC(18,0)
			SELECT @jCount = COUNT(*) FROM @TEMPWarehouse

			WHILE @j <= @jCount
			BEGIN
				SELECT @numTempWarehouseID=numWarehouseID FROM @TEMPWarehouse WHERE ID=@j

				EXEC USP_WarehouseItems_Save @numDomainID,
											@numUserCntID,
											0,
											@numTempWarehouseID,
											0,
											@numItemCode,
											'',
											@monListPrice,
											0,
											0,
											'',
											'',
											'',
											'',
											0
 
				SET @j = @j + 1
			END
		END 
	END         
	ELSE IF @numItemCode>0 AND @intWebApiId > 0 
	BEGIN 
		IF @ProcedureCallFlag = 1 
		BEGIN
			DECLARE @ExportToAPIList AS VARCHAR(30)
			SELECT @ExportToAPIList = vcExportToAPI FROM dbo.Item WHERE numItemCode = @numItemCode

			IF @ExportToAPIList != ''
			BEGIN
				IF NOT EXISTS(select * from (SELECT * FROM dbo.Split(@ExportToAPIList ,','))as A where items= @vcExportToAPI ) 
				BEGIN
					SET @vcExportToAPI = @ExportToAPIList + ',' + @vcExportToAPI
				End
				ELSE
				BEGIN
					SET @vcExportToAPI = @ExportToAPIList 
				End
			END

			--update ExportToAPI String value
			UPDATE dbo.Item SET vcExportToAPI=@vcExportToAPI where numItemCode=@numItemCode  AND numDomainID=@numDomainID
					
			SELECT 
				@cnt = COUNT([numItemID]) 
			FROM 
				[ItemAPI] 
			WHERE  
				[WebApiId] = @intWebApiId 
				AND [numDomainId] = @numDomainID
				AND [vcAPIItemID] = @vcApiItemId
				AND numItemID = @numItemCode

			IF @cnt > 0
			BEGIN
				--update lastUpdated Date 
				UPDATE [ItemAPI] SET [dtModified] =GETUTCDATE() WHERE [WebApiId] =@intWebApiId AND [numDomainId] = @numDomainID
			END
			ELSE
			--Insert ItemAPI mapping
			BEGIN
				INSERT INTO [ItemAPI] 
				(
					[WebApiId],
					[numDomainId],
					[numItemID],
					[vcAPIItemID],
					[numCreatedby],
					[dtCreated],
					[numModifiedby],
					[dtModified],vcSKU
				) VALUES 
				(
					@intWebApiId,
					@numDomainID,
					@numItemCode,
					@vcApiItemId,
					@numUserCntID,
					GETUTCDATE(),
					@numUserCntID,
					GETUTCDATE(),@vcSKU
				)	
			END
		END   
	END                                            
	ELSE IF @numItemCode > 0 AND @intWebApiId <= 0                                                           
	BEGIN
		DECLARE @OldGroupID NUMERIC 
		SELECT @OldGroupID = ISNULL(numItemGroup,0) FROM [Item] WHERE numitemcode = @numItemCode AND [numDomainId] = @numDomainID
	
		DECLARE @monOldAverageCost AS DECIMAL(18,4);SET @monOldAverageCost=0
		SELECT @monOldAverageCost=(CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(monAverageCost,0) END) FROM Item WHERE numItemCode =@numItemCode AND [numDomainId] = @numDomainID
	   
		UPDATE 
			Item 
		SET 
			vcItemName=@vcItemName,txtItemDesc=@txtItemDesc,charItemType=@charItemType,monListPrice= @monListPrice,numItemClassification=@numItemClassification,                                             
			bitTaxable=@bitTaxable,vcSKU = (CASE WHEN ISNULL(@ParentSKU,'') = '' THEN @vcSKU ELSE @ParentSKU END),bitKitParent=@bitKitParent,                                             
			numVendorID=@numVendorID,numDomainID=@numDomainID,bintModifiedDate=getutcdate(),numModifiedBy=@numUserCntID,bitSerialized=@bitSerialized,                                                         
			vcModelID=@vcModelID,numItemGroup=@numItemGroup,numCOGsChartAcntId=@numCOGsChartAcntId,numAssetChartAcntId=@numAssetChartAcntId,                                      
			numIncomeChartAcntId=@numIncomeChartAcntId,monCampaignLabourCost=@monLabourCost,fltWeight=@fltWeight,fltHeight=@fltHeight,fltWidth=@fltWidth,                  
			fltLength=@fltLength,bitFreeShipping=@bitFreeshipping,bitAllowBackOrder=@bitAllowBackOrder,vcUnitofMeasure=@UnitofMeasure,bitShowDeptItem=@bitShowDeptItem,            
			bitShowDeptItemDesc=@bitShowDeptItemDesc,bitCalAmtBasedonDepItems=@bitCalAmtBasedonDepItems,bitAssembly=@bitAssembly,numBarCodeId=@numBarCodeId,
			vcManufacturer=@vcManufacturer,numBaseUnit=@numBaseUnit,numPurchaseUnit=@numPurchaseUnit,numSaleUnit=@numSaleUnit,bitLotNo=@bitLotNo,
			IsArchieve = @IsArchieve,numItemClass=@numItemClass,tintStandardProductIDType=@tintStandardProductIDType,vcExportToAPI=@vcExportToAPI,
			numShipClass=@numShipClass,bitAllowDropShip=@bitAllowDropShip, bitArchiveItem = @bitArchiveItem,bitAsset=@bitAsset,bitRental=@bitRental,
			bitVirtualInventory = @bitVirtualInventory,numContainer=@numContainer,numNoItemIntoContainer=@numNoItemIntoContainer,bitMatrix=@bitMatrix,numManufacturer=@numManufacturer,
			vcASIN = @vcASIN
		WHERE 
			numItemCode=@numItemCode 
			AND numDomainID=@numDomainID

		IF NOT EXISTS(SELECT numItemCode FROM dbo.OpportunityItems WHERE numItemCode = @numItemCode)
		BEGIN
			IF (((SELECT ISNULL(SUM(numOnHand),0) FROM WareHouseItems WHERE numItemID = @numItemCode AND numDomainId=@numDomainID) = 0) AND ((SELECT ISNULL(bitVirtualInventory,0) FROM Item  WHERE numItemCode = @numItemCode AND numDomainId=@numDomainID) = 0))
			BEGIN
				UPDATE Item SET monAverageCost=@monAverageCost  where numItemCode=@numItemCode  AND numDomainID=@numDomainID	
			END
		END
	
		--If Average Cost changed then add in Tracking
		IF ISNULL(@monAverageCost,0) != ISNULL(@monOldAverageCost,0)
		BEGIN
			DECLARE @numWareHouseItemID NUMERIC(9);SET @numWareHouseItemID=0
			DECLARE @numTotal int;SET @numTotal=0
			SET @i=0
			DECLARE @vcDescription AS VARCHAR(100);SET @vcDescription=''
		
			SELECT @numTotal=COUNT(*) FROM WareHouseItems where numItemID=@numItemCode AND numDomainId=@numDomainId
	     
			WHILE @i < @numTotal
			BEGIN
				SELECT @numWareHouseItemID = min(numWareHouseItemID) FROM WareHouseItems 
					WHERE numItemID=@numItemCode AND numDomainId=@numDomainId and numWareHouseItemID > @numWareHouseItemID 
				
				IF @numWareHouseItemID>0
				BEGIN
					SET @vcDescription='Average Cost Changed Manually OLD : ' + CAST(ISNULL(@monOldAverageCost,0) AS VARCHAR(20))
				
					DECLARE @numDomain AS NUMERIC(18,0)
					SET @numDomain = @numDomainID
					EXEC dbo.USP_ManageWareHouseItems_Tracking
						@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)		
						@numReferenceID = @numItemCode, --  numeric(9, 0)
						@tintRefType = 1, --  tinyint
						@vcDescription = @vcDescription, --  varchar(100)
						@tintMode = 0, --  tinyint
						@numModifiedBy = @numUserCntID, --  numeric(9, 0)
						@numDomainID = @numDomain
				END
		    
				SET @i = @i + 1
			END
		END
 
		DECLARE @bitCartFreeShipping as  bit 
		SELECT @bitCartFreeShipping = bitFreeShipping FROM dbo.CartItems where numItemCode = @numItemCode  AND numDomainID = @numDomainID 
 
		IF @bitCartFreeShipping <> @bitFreeshipping
		BEGIN
			UPDATE dbo.CartItems SET bitFreeShipping = @bitFreeshipping WHERE numItemCode = @numItemCode  AND numDomainID=@numDomainID
		END 
 
		IF @intWebApiId > 1 
		BEGIN
			SELECT 
				@cnt = COUNT([numItemID])
			FROM 
				[ItemAPI]
			WHERE 
				[WebApiId] = @intWebApiId
				AND [numDomainId] = @numDomainID
				AND [vcAPIItemID] = @vcApiItemId

			IF @cnt > 0
			BEGIN
				--update lastUpdated Date 
				UPDATE [ItemAPI] SET [dtModified] =GETUTCDATE() WHERE [WebApiId] =@intWebApiId AND [numDomainId] = @numDomainID
			END
			ELSE --Insert ItemAPI mapping
			BEGIN
				INSERT INTO [ItemAPI] 
				(
	 				[WebApiId],
	 				[numDomainId],
	 				[numItemID],
	 				[vcAPIItemID],
	 				[numCreatedby],
	 				[dtCreated],
	 				[numModifiedby],
	 				[dtModified],vcSKU
				 )
				 VALUES 
				 (
					@intWebApiId,
					@numDomainID,
					@numItemCode,
					@vcApiItemId,
	 				@numUserCntID,
	 				GETUTCDATE(),
	 				@numUserCntID,
	 				GETUTCDATE(),@vcSKU
	 			) 
			END
		END   
		                                                                            
		-- kishan It is necessary to add item into itemTax table . 
		IF	@bitTaxable = 1
		BEGIN
			IF NOT EXISTS(SELECT * FROM dbo.ItemTax WHERE numItemCode = @numItemCode AND numTaxItemID = 0)
			BEGIN
			    INSERT INTO dbo.ItemTax 
				(
					numItemCode,
					numTaxItemID,
					bitApplicable
				)
				VALUES 
				( 
					@numItemCode,
					0,
					1 
				) 						
			END
		END	 
      
		IF @charItemType='S' or @charItemType='N'                                                                       
		BEGIN
			DELETE FROM WareHouseItmsDTL where numWareHouseItemID in (select numWareHouseItemID from WareHouseItems where numItemID=@numItemCode AND [WareHouseItems].[numDomainID] = @numDomainID)
			DELETE FROM WareHouseItems where numItemID=@numItemCode AND [WareHouseItems].[numDomainID] = @numDomainID
		END                                      
   
		IF @bitKitParent=1 OR @bitAssembly = 1              
		BEGIN              
			DECLARE @hDoc1 AS INT                                            
			EXEC sp_xml_preparedocument @hDoc1 OUTPUT, @strChildItems                                                                        
                                             
			UPDATE 
				ItemDetails 
			SET
				numQtyItemsReq=X.QtyItemsReq
				,numWareHouseItemId=X.numWarehouseItmsID
				,numUOMId=X.numUOMId
				,vcItemDesc=X.vcItemDesc
				,sintOrder=X.sintOrder                                                                                          
			From 
			(
				SELECT 
					QtyItemsReq,ItemKitID,ChildItemID,numWarehouseItmsID,numUOMId,vcItemDesc,sintOrder, numItemDetailID As ItemDetailID                             
				FROM 
					OPENXML(@hDoc1,'/NewDataSet/Table1',2)                                                                         
				WITH
				(
					ItemKitID NUMERIC(9),                                                          
					ChildItemID NUMERIC(9),                                                                        
					QtyItemsReq DECIMAL(30, 16),
					numWarehouseItmsID NUMERIC(9),
					numUOMId NUMERIC(9),
					vcItemDesc VARCHAR(1000),
					sintOrder int,
					numItemDetailID NUMERIC(18,0)
				)
			)X                                                                         
			WHERE 
				numItemKitID=X.ItemKitID 
				AND numChildItemID=ChildItemID 
				AND numItemDetailID = X.ItemDetailID

			EXEC sp_xml_removedocument @hDoc1
		 END   
		 ELSE
		 BEGIN
 			DELETE FROM ItemDetails WHERE numItemKitID=@numItemCode
		 END                                                
	END

	IF ISNULL(@numItemGroup,0) = 0 OR ISNULL(@bitMatrix,0) = 1
	BEGIN
		UPDATE WareHouseItems SET monWListPrice = @monListPrice, vcWHSKU=@vcSKU, vcBarCode=@numBarCodeId WHERE numDomainID = @numDomainID AND numItemID = @numItemCode
	END

	IF @numItemCode > 0
	BEGIN
		DELETE 
			IC
		FROM 
			dbo.ItemCategory IC
		INNER JOIN 
			Category 
		ON 
			IC.numCategoryID = Category.numCategoryID
		WHERE 
			numItemID = @numItemCode		
			AND numCategoryProfileID = @numCategoryProfileID

		IF @vcCategories <> ''	
		BEGIN
			INSERT INTO ItemCategory( numItemID , numCategoryID )  SELECT  @numItemCode AS numItemID,ID from dbo.SplitIDs(@vcCategories,',')	
		END
	END

	


	-- SAVE MATRIX ITEM ATTRIBUTES
	IF @numItemCode > 0 AND ISNULL(@bitAttributesChanged,0)=1 AND ISNULL(@bitMatrix,0) = 1 AND LEN(ISNULL(@vcItemAttributes,'')) > 0 AND ISNULL(@bitOppOrderExists,0) <> 1 AND (SELECT COUNT(*) FROM @TempTable) > 0
	BEGIN
		-- DELETE PREVIOUS ITEM ATTRIBUTES
		DELETE FROM ItemAttributes WHERE numDomainID=@numDomainID AND numItemCode=@numItemCode

		-- INSERT NEW ITEM ATTRIBUTES
		INSERT INTO ItemAttributes
		(
			numDomainID
			,numItemCode
			,FLD_ID
			,FLD_Value
		)
		SELECT
			@numDomainID
			,@numItemCode
			,Fld_ID
			,Fld_Value
		FROM
			@TempTable

		IF (SELECT COUNT(*) FROM WarehouseItems WHERE numDomainID=@numDomainID AND numItemID=@numItemCode) > 0
		BEGIN

			-- DELETE PREVIOUS WAREHOUSE ATTRIBUTES
			DELETE FROM CFW_Fld_Values_Serialized_Items WHERE RecId IN (SELECT ISNULL(numWarehouseItemID,0) FROM WarehouseItems WHERE numDomainID=@numDomainID AND numItemID=@numItemCode)

			-- INSERT NEW WAREHOUSE ATTRIBUTES
			INSERT INTO CFW_Fld_Values_Serialized_Items
			(
				Fld_ID,
				Fld_Value,
				RecId,
				bitSerialized
			)                                                  
			SELECT 
				Fld_ID,
				ISNULL(CAST(Fld_Value AS VARCHAR),'') AS Fld_Value,
				TEMPWarehouse.numWarehouseItemID,
				0 
			FROM 
				@TempTable  
			OUTER APPLY
			(
				SELECT
					numWarehouseItemID
				FROM 
					WarehouseItems 
				WHERE 
					numDomainID=@numDomainID 
					AND numItemID=@numItemCode
			) AS TEMPWarehouse
		END
	END	  
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH        
END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageSOWorkOrder')
DROP PROCEDURE USP_ManageSOWorkOrder
GO
CREATE PROCEDURE [dbo].[USP_ManageSOWorkOrder]
@numDomainID AS numeric(18,0)=0,
@numOppID AS numeric(18,0)=0,
@strItems AS TEXT,
@numUserCntID AS numeric(9)=0
AS
BEGIN
	DECLARE @hDocItem int                                                                                                                            

	IF (SELECT COUNT(*) FROM WorkOrder WHERE numOppId=@numOppId)=0
	BEGIN
		EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems  
  
		DECLARE @numItemCode AS NUMERIC(18,0),@numUnitHour AS FLOAT,@numWarehouseItmsID AS NUMERIC(18,0),@vcItemDesc AS VARCHAR(1000)
  
		DECLARE @numWOId AS NUMERIC(18,0),@vcInstruction AS VARCHAR(1000),@bintCompliationDate AS DATETIME,@numWOAssignedTo NUMERIC(18,0)

		DECLARE @numoppitemtCode NUMERIC(18,0)
  
		DECLARE WOCursor CURSOR FOR 
		SELECT 
			numoppitemtCode
			,numItemCode
			,numUnitHour
			,numWarehouseItmsID
			,vcItemDesc
			,vcInstruction
			,bintCompliationDate
			,numWOAssignedTo
		FROM 
			OpportunityItems                                                                         
		WHERE 
			numOppID=@numOppID
			AND ISNULL(bitWorkOrder,0)=1
			

		OPEN WOCursor;

		FETCH NEXT FROM WOCursor INTO @numoppitemtCode,@numItemCode, @numUnitHour,@numWarehouseItmsID,@vcItemDesc,@vcInstruction,@bintCompliationDate,@numWOAssignedTo

		WHILE @@FETCH_STATUS = 0
		BEGIN
			INSERT INTO WorkOrder
			(
				numItemCode
				,numQtyItemsReq
				,numWareHouseItemId
				,numCreatedBy
				,bintCreatedDate
				,numDomainID
				,numWOStatus
				,numOppId
				,vcItemDesc
				,vcInstruction
				,bintCompliationDate
				,numAssignedTo
				,numOppItemID
			)
			VALUES
			(
				@numItemCode
				,@numUnitHour
				,@numWarehouseItmsID
				,@numUserCntID
				,GETUTCDATE()
				,@numDomainID
				,0
				,@numOppID
				,@vcItemDesc
				,@vcInstruction
				,@bintCompliationDate
				,@numWOAssignedTo
				,@numoppitemtCode
			)
	
			SET @numWOId = SCOPE_IDENTITY()
	

			IF @numWOAssignedTo>0
			BEGIN
				DECLARE @numDivisionId AS NUMERIC(9) ,@vcItemName AS VARCHAR(300)
	
				SELECT 
					@vcItemName=ISNULL(vcItemName,'') 
				FROM 
					Item 
				WHERE 
					numItemCode=@numItemCode

				SET @vcItemName= 'Work Order for Item : ' + @vcItemName

				SELECT @numDivisionId=numDivisionId FROM AdditionalContactsInformation WHERE numContactId=@numWOAssignedTo

				EXEC dbo.usp_InsertCommunication
					@numCommId = 0, --  numeric(18, 0)
					@bitTask = 972, --  numeric(18, 0)
					@numContactId = @numUserCntID, --  numeric(18, 0)
					@numDivisionId = @numDivisionId, --  numeric(18, 0)
					@txtDetails = @vcItemName, --  text
					@numOppId = 0, --  numeric(18, 0)
					@numAssigned = @numWOAssignedTo, --  numeric(18, 0)
					@numUserCntID = @numUserCntID, --  numeric(18, 0)
					@numDomainId = @numDomainID, --  numeric(18, 0)
					@bitClosed = 0, --  tinyint
					@vcCalendarName = '', --  varchar(100)
					@dtStartTime = @bintCompliationDate, --  datetime
					@dtEndtime = @bintCompliationDate, --  datetime
					@numActivity = 0, --  numeric(9, 0)
					@numStatus = 0, --  numeric(9, 0)
					@intSnoozeMins = 0, --  int
					@tintSnoozeStatus = 0, --  tinyint
					@intRemainderMins = 0, --  int
					@tintRemStaus = 0, --  tinyint
					@ClientTimeZoneOffset = 0, --  int
					@bitOutLook = 0, --  tinyint
					@bitSendEmailTemplate = 0, --  bit
					@bitAlert = 0, --  bit
					@numEmailTemplate = 0, --  numeric(9, 0)
					@tintHours = 0, --  tinyint
					@CaseID = 0, --  numeric(18, 0)
					@CaseTimeId = 0, --  numeric(18, 0)
					@CaseExpId = 0, --  numeric(18, 0)
					@ActivityId = 0, --  numeric(18, 0)
					@bitFollowUpAnyTime = 0, --  bit
					@strAttendee=''
			END

			INSERT INTO [WorkOrderDetails] 
			(
				numWOId,numItemKitID,numChildItemID,numQtyItemsReq,numWareHouseItemId,vcItemDesc,sintOrder,numQtyItemsReq_Orig,numUOMId
			)
			SELECT 
				@numWOId,numItemKitID,numItemCode,
				CAST(((DTL.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(Dtl.numUOMID,item.numItemCode,item.numDomainID,item.numBaseUnit),1)) * @numUnitHour)AS FLOAT),
				isnull(Dtl.numWareHouseItemId,0),
				ISNULL(Dtl.vcItemDesc,txtItemDesc),
				ISNULL(sintOrder,0),
				DTL.numQtyItemsReq,
				Dtl.numUOMId 
			FROM 
				item                                
			INNER JOIN 
				ItemDetails Dtl 
			ON 
				numChildItemID=numItemCode
			WHERE 
				numItemKitID=@numItemCode
	
			FETCH NEXT FROM WOCursor INTO @numoppitemtCode,@numItemCode, @numUnitHour,@numWarehouseItmsID,@vcItemDesc,@vcInstruction,@bintCompliationDate,@numWOAssignedTo
		END

		CLOSE WOCursor;
		DEALLOCATE WOCursor;

		EXEC sp_xml_removedocument @hDocItem     
	END 
END
/****** Object:  StoredProcedure [dbo].[USP_OppBizDocs]    Script Date: 03/25/2009 15:23:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_oppbizdocs')
DROP PROCEDURE usp_oppbizdocs
GO
CREATE PROCEDURE [dbo].[USP_OppBizDocs]
(                        
 @byteMode as tinyint=null,                        
 @numOppId as numeric(9)=null,                        
 @numOppBizDocsId as numeric(9)=null,
 @ClientTimeZoneOffset AS INT=0,
 @numDomainID AS NUMERIC(18,0) = NULL,
 @numUserCntID AS NUMERIC(18,0) = NULL
)                        
as      

DECLARE @ParentBizDoc AS NUMERIC(9)=0
DECLARE @lngJournalId AS NUMERIC(18,0)
DECLARE @numDivisionID NUMERIC(18,0)
SELECT @numDivisionID=numDivisionId FROM OpportunityMaster WHERE numOppId=@numOppId
    
SELECT 
	ISNULL(CompanyInfo.numCompanyType,0) AS numCompanyType,
	ISNULL(CompanyInfo.vcProfile,0) AS vcProfile,
	ISNULL(OpportunityMaster.numAccountClass,0) AS numAccountClass
FROM 
	OpportunityMaster 
JOIN 
	DivisionMaster 
ON 
	DivisionMaster.numDivisionId = OpportunityMaster.numDivisionId
JOIN 
	CompanyInfo
ON 
	CompanyInfo.numCompanyId = DivisionMaster.numCompanyID
WHERE 
	numOppId = @numOppId
		                                  
IF @byteMode= 1                        
BEGIN 
BEGIN TRY
BEGIN TRANSACTION
	DECLARE @numBizDocID AS INT
	DECLARE @bitFulfilled AS BIT
	Declare @tintOppType as tinyint
	DECLARE @tintShipped AS BIT
	DECLARE @bitAuthoritativeBizDoc AS BIT

	SELECT @tintOppType=tintOppType,@tintShipped=ISNULL(tintshipped,0) FROM OpportunityMaster WHERE  numOppId=@numOppId  and numDomainID= @numDomainID 
	SELECT @numBizDocID = numBizDocId, @bitFulfilled=bitFulfilled, @bitAuthoritativeBizDoc=ISNULL(bitAuthoritativeBizDocs,0) FROM OpportunityBizDocs WHERE numOppBizDocsId = @numOppBizDocsId

	IF ISNULL(@tintShipped,0) = 1 AND (@numBizDocID=296 OR ISNULL(@bitAuthoritativeBizDoc,0)=1)
	BEGIN
		RAISERROR('NOT_ALLOWED_TO_DELETE_AUTHORITATIVE_BIZDOC_AFTER_ORDER_IS_CLOSED',16,1)
		RETURN
	END

	DECLARE @tintCommitAllocation AS TINYINT
	DECLARE @bitAllocateInventoryOnPickList AS BIT

	SELECT @tintCommitAllocation=ISNULL(tintCommitAllocation,1) FROM Domain WHERE numDomainId=@numDomainID
	SELECT @bitAllocateInventoryOnPickList=ISNULL(bitAllocateInventoryOnPickList,0) FROM DivisionMaster WHERE numDivisionID=@numDivisionID

	IF ISNULL(@tintCommitAllocation,0)=2 AND ISNULL(@bitAllocateInventoryOnPickList,0) = 1 AND @numBizDocID=29397 AND (SELECT COUNT(*) FROM OpportunityBizDocs WHERE numOppId=@numOppId AND numBizDocId=296 AND numSourceBizDocId=@numOppBizDocsId) > 0
	BEGIN
		RAISERROR('NOT_ALLOWED_TO_DELETE_PICK_LIST_AFTER_FULFILLMENT_BIZDOC_IS_GENERATED_AGAINST_IT',16,1)
		RETURN
	END

	IF ISNULL(@tintCommitAllocation,0)=2 AND ISNULL(@bitAllocateInventoryOnPickList,0) = 1 AND @numBizDocID=29397
	BEGIN
		-- Revert Allocation
		EXEC USP_RevertAllocationPickList @numDomainID,@numOppID,@numOppBizDocsId,@numUserCntID
	END

	SET @ParentBizDoc=(SELECT TOP 1 numOppBizDocsId FROM OpportunityBizDocs WHERE numSourceBizDocId= @numOppBizDocsId)  
	   
	IF @ParentBizDoc>0
	BEGIN	
		SET @lngJournalId=(Select TOP 1 GJH.numJournal_Id as numJournalId from General_Journal_Header GJH Where GJH.numOppId=@numOppId And GJH.numOppBizDocsId=@ParentBizDoc)
	
		IF(@lngJournalId>0)
		BEGIN
			EXEC USP_DeleteJournalDetails @numDomainID=@numDomainID,@numJournalID=@lngJournalId,@numBillPaymentID=0,@numDepositID=0,@numCheckHeaderID=0,@numBillID=0,@numCategoryHDRID=0,@numReturnID=0,@numUserCntID=0
		END

		EXEC DeleteComissionDetails @numDomainId=@numDomainID,@numOppBizDocID=@ParentBizDoc 

		EXEC USP_OppBizDocs @byteMode=@byteMode,@numOppId=@numOppId,@numOppBizDocsId=@ParentBizDoc,@ClientTimeZoneOffset=@ClientTimeZoneOffset,@numDomainID=@numDomainID,@numUserCntID=@numUserCntID
	END  









--IF it's as sales order and BizDoc is of type of fulfillment and already fulfilled revert items to allocations
IF @tintOppType = 1 AND @numBizDocID = 296 AND @bitFulfilled = 1
BEGIN
	EXEC USP_OpportunityBizDocs_RevertFulFillment @numDomainID,@numUserCntID,@numOppId,@numOppBizDocsId
END


DELETE FROM [ProjectsOpportunities] WHERE numOppBizDocID=@numOppBizDocsId

DELETE FROM [ShippingReportItems] WHERE [numShippingReportId] IN  (SELECT numShippingReportId from [ShippingReport] WHERE [numOppBizDocId] = @numOppBizDocsId)
DELETE FROM [ShippingBox] WHERE [numShippingReportId] IN  (SELECT numShippingReportId from [ShippingReport] WHERE [numOppBizDocId] = @numOppBizDocsId)
DELETE FROM [ShippingReport] WHERE [numOppBizDocId] = @numOppBizDocsId

 --Delete All entries for current bizdoc
EXEC [USP_DeleteEmbeddedCost] @numOppBizDocsId, 0, @numDomainID 
--DELETE FROM [OpportunityBizDocsPaymentDetails] WHERE [numBizDocsPaymentDetId] IN (SELECT  [numBizDocsPaymentDetId] FROM [OpportunityBizDocsDetails] WHERE [numBizDocsPaymentDetId] IN (SELECT [numBizDocsPaymentDetId] FROM [EmbeddedCost] WHERE [numOppBizDocID]=@numOppBizDocsId))
--DELETE FROM [EmbeddedCostItems] WHERE [numEmbeddedCostID] IN (SELECT [numEmbeddedCostID] FROM [EmbeddedCost] WHERE [numOppBizDocID] = @numOppBizDocsId)
--DELETE FROM [EmbeddedCost] WHERE [numOppBizDocID] = @numOppBizDocsId
--DELETE FROM [OpportunityBizDocsDetails] WHERE [numBizDocsPaymentDetId] IN (SELECT [numBizDocsPaymentDetId] FROM [EmbeddedCost] WHERE [numOppBizDocID]=@numOppBizDocsId)


delete  from OpportunityBizDocItems where   numOppBizDocID=  @numOppBizDocsId              

--Delete Deferred BizDoc Recurring Entries
delete from OpportunityRecurring where numOppBizDocID=@numOppBizDocsId and tintRecurringType=4

UPDATE dbo.OpportunityRecurring SET numOppBizDocID=NULL WHERE numOppBizDocID=@numOppBizDocsId

DELETE FROM [RecurringTransactionReport] WHERE [numRecTranBizDocID] = @numOppBizDocsId

--Credit Balance
Declare @monCreditAmount as DECIMAL(20,5);Set @monCreditAmount=0
Set @numDivisionID=0

Select @monCreditAmount=isnull(oppBD.monCreditAmount,0),@numDivisionID=Om.numDivisionID,
@numOppId=OM.numOppId 
from OpportunityBizDocs oppBD join OpportunityMaster Om on OM.numOppId=oppBD.numOppId WHERE  numOppBizDocsId = @numOppBizDocsId

delete from [CreditBalanceHistory] where numOppBizDocsId=@numOppBizDocsId

 
IF @tintOppType=1 --Sales
	update DivisionMaster set monSCreditBalance=isnull(monSCreditBalance,0) + isnull(@monCreditAmount,0) where numDivisionID=@numDivisionID and numDomainID=@numDomainID
else IF @tintOppType=2 --Purchase
	update DivisionMaster set monPCreditBalance=isnull(monPCreditBalance,0) + isnull(@monCreditAmount,0) where numDivisionID=@numDivisionID and numDomainID=@numDomainID


delete from DocumentWorkflow where numDocID=@numOppBizDocsId and cDocType='B'      

delete from BizDocAction where numBizActionId in(select numBizActionId from BizActionDetails where numOppBizDocsId =@numOppBizDocsId and btDocType=1) 
delete from BizActionDetails where numOppBizDocsId =@numOppBizDocsId and btDocType=1


  DELETE FROM dbo.General_Journal_Details WHERE numJournalId IN (SELECT numJournal_Id FROM dbo.General_Journal_Header WHERE numOppId=@numOppId AND numOppBizDocsId=@numOppBizDocsId AND numDomainId=@numDomainID)
  DELETE FROM dbo.General_Journal_Header WHERE numOppId=@numOppId AND numOppBizDocsId=@numOppBizDocsId AND numDomainId= @numDomainID


delete from OpportunityBizDocs where numOppBizDocsId=@numOppBizDocsId                        

COMMIT TRANSACTION
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH             
end                        
                        
if @byteMode= 2                        
begin  

DECLARE @BaseCurrencySymbol nVARCHAR(3);SET @BaseCurrencySymbol='$'
SELECT @BaseCurrencySymbol = ISNULL(varCurrSymbol,'$') FROM dbo.Currency 
														   WHERE numCurrencyID IN ( SELECT numCurrencyID FROM dbo.Domain 
																					WHERE numDomainId=@numDomainId)
--PRINT @BaseCurrencySymbol

																					                      
select *, isnull(X.monPAmount,0)-X.monAmountPaid as BalDue from (select  opp.numOppBizDocsId,                        
 opp.numOppId,                        
 opp.numBizDocId,                        
 ISNULL((SELECT TOP 1 numOrientation FROM BizDocTemplate WHERE numBizDocTempID = Opp.numBizDocTempID),0) AS numOrientation, 
 ISNULL((SELECT TOP 1 vcTemplateName FROM BizDocTemplate WHERE numBizDocTempID = Opp.numBizDocTempID),0) AS vcTemplateName, 
 ISNULL((SELECT TOP 1 bitKeepFooterBottom FROM BizDocTemplate WHERE numBizDocTempID = Opp.numBizDocTempID),0) AS bitKeepFooterBottom,                  
 oppmst.vcPOppName,                        
 ISNULL(ListDetailsName.vcName,mst.vcData) as BizDoc,                        
 opp.numCreatedBy,                        
DATEADD(minute,-@ClientTimeZoneOffset,opp.dtCreatedDate) CreatedDate,
 opp.vcRefOrderNo,                       
 vcBizDocID,
 CASE WHEN LEN(ISNULL(vcBizDocName,''))=0 THEN vcBizDocID ELSE vcBizDocName END vcBizDocName,      
  [dbo].[GetDealAmount](@numOppId ,getutcdate(),opp.numOppBizDocsId ) as monPAmount,
  CASE 
	WHEN tintOppType=1  
	THEN (CASE 
			WHEN (ISNULL(numAuthoritativeSales,0)=numBizDocId OR numBizDocId=296) 
			THEN 'Authoritative' + CASE WHEN ISNULL(Opp.tintDeferred,0)=1 THEN ' (Deferred)' ELSE '' END 
			WHEN opp.numBizDocId =  304 THEN 'Deferred-Authoritative'
			ELSE 'Non-Authoritative' END ) 
  when tintOppType=2  then (case when isnull(numAuthoritativePurchase,0)=numBizDocId then 'Authoritative' + Case when isnull(Opp.tintDeferred,0)=1 then ' (Deferred)' else '' end else 'Non-Authoritative' end )  end as BizDocType,
  isnull(monAmountPaid,0) monAmountPaid, 
  ISNULL(C.varCurrSymbol,'') varCurrSymbol ,
  ISNULL(Opp.[bitAuthoritativeBizDocs],0) bitAuthoritativeBizDocs,
  ISNULL(oppmst.[tintshipped] ,0) tintshipped,isnull(opp.numShipVia,0) as numShipVia,oppmst.numDivisionId,isnull(Opp.tintDeferred,0) as tintDeferred,
  ISNULL(Opp.numBizDocTempID,0) AS numBizDocTempID,con.numContactid,isnull(con.vcEmail,'') AS vcEmail,ISNULL(Opp.numBizDocStatus,0) AS numBizDocStatus,
--  case when isnumeric(ISNULL(dbo.fn_GetListItemName(isnull(oppmst.intBillingDays,0)),0))=1 
--	   then ISNULL(dbo.fn_GetListItemName(isnull(oppmst.intBillingDays,0)),0) 
--	   else 0 
--  end AS numBillingDaysName,
  CASE WHEN ISNUMERIC((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(oppmst.intBillingDays,0))) = 1
 	   THEN (SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(oppmst.intBillingDays,0))
	   ELSE 0
  END AS numBillingDaysName,
  Opp.dtFromDate,
  opp.dtFromDate AS BillingDate,
  ISNULL(C.fltExchangeRate,1) fltExchangeRateCurrent,
  ISNULL(oppmst.fltExchangeRate,1) fltExchangeRateOfOrder,
  @BaseCurrencySymbol BaseCurrencySymbol,0 AS numReturnHeaderID,0 AS tintReturnType,ISNULL(Opp.fltExchangeRateBizDoc,ISNULL(oppmst.fltExchangeRate,1)) AS fltExchangeRateBizDoc,
  CASE WHEN (SELECT COUNT(*) FROM dbo.ShippingReport WHERE numOppBizDocId = opp.numOppBizDocsId AND numDomainID = @numDomainID) > 0 THEN 1 ELSE 0 END AS [IsShippingReportGenerated],
  --CASE WHEN (SELECT ISNULL(vcTrackingNo,'') FROM dbo.OpportunityBizDocs WHERE numOppBizDocsId = opp.numOppBizDocsId) <> '' THEN 1 ELSE 0 END AS [ISTrackingNumGenerated]
  CASE WHEN (SELECT COUNT(*) FROM dbo.ShippingBox 
			 WHERE numShippingReportId IN (SELECT numShippingReportId FROM dbo.ShippingReport WHERE numOppBizDocId = opp.numOppBizDocsId AND numDomainID = @numDomainID)
			 AND LEN(ISNULL(vcTrackingNumber,'')) > 0) > 0 THEN 1 ELSE 0 END AS [ISTrackingNumGenerated],
  ISNULL((SELECT MAX(numShippingReportId) from ShippingReport SR 
		  WHERE SR.numOppBizDocId = opp.numoppbizdocsid 
		  AND SR.numDomainID = oppmst.numDomainID),0) AS numShippingReportId,
  ISNULL((SELECT MAX(SB.numShippingReportId) FROM [ShippingBox] SB 
		  JOIN dbo.ShippingReport SR ON SB.numShippingReportId = SR.numShippingReportId 
		  where SR.numOppBizDocId = opp.numoppbizdocsid AND SR.numDomainID = oppmst.numDomainID 
		  AND LEN(ISNULL(SB.vcTrackingNumber,''))>0),0) AS numShippingLabelReportId			 
 ,ISNULL(STUFF(( SELECT  ',' +  CASE WHEN LEN(ISNULL(W.[vcWareHouse],'')  + ':' + ISNULL([WL].[vcLocation],'')) > 0 THEN ISNULL(W.[vcWareHouse],'')  + ':' + ISNULL([WL].[vcLocation],'') ELSE '' END
                FROM    [dbo].[WarehouseLocation] AS WL
				JOIN [dbo].[Warehouses] W ON [WL].[numWarehouseID] = [W].[numWareHouseID]
                WHERE  [WL].[numWLocationID] IN (
                        SELECT  WHI.[numWLocationID]
                        FROM    [dbo].[WareHouseItems] AS WHI
                                JOIN [dbo].[OpportunityItems] AS OI ON [WHI].[numWareHouseItemID] = [OI].[numWarehouseItmsID]
                        WHERE   [OI].[numOppId] = oppmst.[numOppId])
              FOR
                XML PATH('')
              ), 1, 1, ''),'') AS [vcLocation],
		(CASE WHEN ISNULL(RecurrenceConfiguration.numRecConfigID,0) = 0 THEN 0 ELSE 1 END) AS bitRecur,
		ISNULL(Opp.bitRecurred,0) AS bitRecurred,
		ISNULL((SELECT COUNT(*) FROM OpportunityBizDocs WHERE numOppID=@numOppID AND ISNULL(numDeferredBizDocID,0) = ISNULL(opp.numOppBizDocsId,0)),0) AS intInvoiceForDiferredBizDoc,
		ISNULL(Opp.bitShippingGenerated,0) AS bitShippingGenerated,
		ISNULL(oppmst.intUsedShippingCompany,0) as intUsedShippingCompany,
		numSourceBizDocId,
		ISNULL((SELECT TOP 1 vcBizDocID FROM OpportunityBizDocs WHERE numOppBizDOcsId=opp.numSourceBizDocId),'NA') AS vcSourceBizDocID,
		ISNULL(oppmst.numShipmentMethod,0) numShipmentMethod,
		ISNULL(oppmst.numShippingService,0) numShippingService,
		(CASE WHEN (SELECT COUNT(*) FROM OpportunityBizDocs OBInner WHERE OBInner.numOppID=@numOppID AND OBInner.numBizDocId=296 AND OBInner.numSourceBizDocId=opp.numOppBizDocsId AND opp.numBizDocId=29397) > 0 THEN 1 ELSE 0 END) AS IsFulfillmentGeneratedOnPickList
 FROM                         
 OpportunityBizDocs  opp                        
 left join ListDetails mst                        
 on mst.numListItemID=opp.numBizDocId  
 LEFT JOIN ListDetailsName
 ON ListDetailsName.numListItemID=opp.numBizDocId 
 AND ListDetailsName.numDomainID=@numDomainID
 join OpportunityMaster oppmst                        
 on oppmst.numOppId= opp.numOppId                                
 left join  AuthoritativeBizDocs  AB
 on AB.numDomainId=  oppmst.numDomainID             
 LEFT OUTER JOIN [Currency] C
  ON C.numCurrencyID = oppmst.numCurrencyID
  LEFT JOIN AdditionalContactsInformation con ON con.numContactid = oppmst.numContactId
  LEFT JOIN
	RecurrenceConfiguration
  ON
	RecurrenceConfiguration.numOppBizDocID = opp.numOppBizDocsId
where opp.numOppId=@numOppId

UNION ALL

select  0 AS numOppBizDocsId,                        
 RH.numOppId,                        
 0 AS numBizDocId,                        
 1 AS numOrientation,  
 '' AS vcTemplateName,
 0 AS bitKeepFooterBottom,                   
 '' AS vcPOppName,                        
 'RMA' as BizDoc,                        
 RH.numCreatedBy,                        
DATEADD(minute,-@ClientTimeZoneOffset,RH.dtCreatedDate) CreatedDate,
 '' vcRefOrderNo,                       
 RH.vcRMA AS vcBizDocID,
 RH.vcRMA AS vcBizDocName,      
  ISNULL(monAmount,0) as monPAmount,
  'RMA' as BizDocType,
  0 AS monAmountPaid, 
  '' varCurrSymbol ,
  0 bitAuthoritativeBizDocs,
  0 tintshipped,0 as numShipVia,RH.numDivisionId,0 as tintDeferred,
  0 AS numBizDocTempID,con.numContactid,isnull(con.vcEmail,'') AS vcEmail,0 AS numBizDocStatus,
  0 AS numBillingDaysName,RH.dtCreatedDate AS dtFromDate,
  RH.dtCreatedDate AS BillingDate,
  1 fltExchangeRateCurrent,
  1 fltExchangeRateOfOrder,
  @BaseCurrencySymbol BaseCurrencySymbol,RH.numReturnHeaderID,RH.tintReturnType,1 AS fltExchangeRateBizDoc
  ,0 [IsShippingReportGenerated],0 [ISTrackingNumGenerated],
  0 AS numShippingReportId,
  0 AS numShippingLabelReportId			 
  ,ISNULL(STUFF(( SELECT  ',' +  CASE WHEN LEN(ISNULL(W.[vcWareHouse],'')  + ':' + ISNULL([WL].[vcLocation],'')) > 0 THEN ISNULL(W.[vcWareHouse],'')  + ':' + ISNULL([WL].[vcLocation],'') ELSE '' END
                FROM    [dbo].[WarehouseLocation] AS WL
				JOIN [dbo].[Warehouses] W ON [WL].[numWarehouseID] = [W].[numWareHouseID]
                WHERE  [WL].[numWLocationID] IN (
                        SELECT  WHI.[numWLocationID]
                        FROM    [dbo].[WareHouseItems] AS WHI
                                JOIN [dbo].[OpportunityItems] AS OI ON [WHI].[numWareHouseItemID] = [OI].[numWarehouseItmsID]
                        WHERE   [OI].[numOppId] = RH.[numOppId])
              FOR
                XML PATH('')
              ), 1, 1, ''),'') AS [vcLocation],
			  0 AS bitRecur,
			  0 AS bitRecurred,
		0 AS intInvoiceForDiferredBizDoc ,
		0 AS bitShippingGenerated,
		0 as intUsedShippingCompany,
		0 AS numSourceBizDocId,
		'NA' AS vcSourceBizDocID,
		0 AS numShipmentMethod,
		0 AS numShippingService
		,0 AS IsFulfillmentGeneratedOnPickList
from                         
 ReturnHeader RH                        
 LEFT JOIN AdditionalContactsInformation con ON con.numContactid = RH.numContactId
  
where RH.numOppId=@numOppId
)X ORDER BY CreatedDate ASC       
                        
END
GO
/****** Object:  StoredProcedure [dbo].[USP_OppManage]    Script Date: 07/26/2008 16:20:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OppManage')
DROP PROCEDURE usp_oppmanage
GO
CREATE PROCEDURE [dbo].[USP_OppManage] 
(                                                                          
  @numOppID as numeric=0 OUTPUT,                                                                          
  @numContactId numeric(9)=null,                                                                          
  @numDivisionId numeric(9)=null,                                                                          
  @tintSource numeric(9)=null,                                                                          
  @vcPOppName Varchar(100)='' OUTPUT,                                                                          
  @Comments varchar(1000)='',                                                                          
  @bitPublicFlag bit=0,                                                                          
  @numUserCntID numeric(9)=0,                                                                                 
  @monPAmount DECIMAL(20,5) =0,                                                 
  @numAssignedTo as numeric(9)=0,                                                                                                        
  @numDomainId numeric(9)=0,                                                                                                                                                                                   
  @strItems as text=null,                                                                          
  @strMilestone as text=null,                                                                          
  @dtEstimatedCloseDate datetime,                                                                          
  @CampaignID as numeric(9)=null,                                                                          
  @lngPConclAnalysis as numeric(9)=null,                                                                         
  @tintOppType as tinyint,                                                                                                                                             
  @tintActive as tinyint,                                                              
  @numSalesOrPurType as numeric(9),              
  @numRecurringId as numeric(9),
  @numCurrencyID as numeric(9)=0,
  @DealStatus as tinyint =0,-- 0=Open,1=Won,2=Lost
  @numStatus AS NUMERIC(18,0),
  @vcOppRefOrderNo VARCHAR(100),
  @numBillAddressId numeric(9)=0,
  @numShipAddressId numeric(9)=0,
  @bitStockTransfer BIT=0,
  @WebApiId INT = 0,
  @tintSourceType TINYINT=0,
  @bitDiscountType as bit,
  @fltDiscount  as float,
  @bitBillingTerms as bit,
  @intBillingDays as integer,
  @bitInterestType as bit,
  @fltInterest as float,
  @tintTaxOperator AS TINYINT,
  @numDiscountAcntType AS NUMERIC(9),
  @vcWebApiOrderNo VARCHAR(100)=NULL,
  @vcCouponCode		VARCHAR(100) = '',
  @vcMarketplaceOrderID VARCHAR(100)=NULL,
  @vcMarketplaceOrderDate  datetime=NULL,
  @vcMarketplaceOrderReportId VARCHAR(100)=NULL,
  @numPercentageComplete NUMERIC(9),
  @bitUseShippersAccountNo BIT = 0,
  @bitUseMarkupShippingRate BIT = 0,
  @numMarkupShippingRate NUMERIC(19,2) = 0,
  @intUsedShippingCompany INT = 0,
  @numShipmentMethod NUMERIC(18,0)=0,
  @dtReleaseDate DATE = NULL,
  @numPartner NUMERIC(18,0)=0,
  @tintClickBtn INT=0,
  @numPartenerContactId NUMERIC(18,0)=0,
  @numAccountClass NUMERIC(18,0) = 0,
  @numWillCallWarehouseID NUMERIC(18,0) = 0,
  @numVendorAddressID NUMERIC(18,0) = 0,
  @numShipFromWarehouse NUMERIC(18,0) = 0,
  @numShippingService NUMERIC(18,0) = 0,
  @ClientTimeZoneOffset INT = 0
  -- USE PARAMETER WITH OPTIONAL VALUE BECAUSE PROCEDURE IS CALLED FROM OTHER PROCEDURE WHICH WILL NOT PASS NEW PARAMETER VALUE
)                                                                          
                                                                          
as  
BEGIN TRY
BEGIN TRANSACTION
	DECLARE @fltExchangeRate float                                 
	DECLARE @hDocItem int                                                                                                                            
	DECLARE @TotalAmount as DECIMAL(20,5)                                                                          
	DECLARE @tintOppStatus as tinyint
	DECLARE @bitNewOrder AS BIT = (CASE WHEN ISNULL(@numOppID,0) = 0 THEN 1 ELSE 0 END)
	
	IF ISNULL(@numContactId,0) = 0
	BEGIN
		RAISERROR('CONCAT_REQUIRED',16,1)
		RETURN
	END  
	
	IF ISNULL(@tintOppType,0) = 0
	BEGIN
		RAISERROR('OPP_TYPE_CAN_NOT_BE_0',16,1)
		RETURN
	END             

	IF ISNULL(@numOppID,0) > 0 AND EXISTS (SELECT numOppId FROM OpportunityMaster WHERE numDomainId=@numDomainId AND numOppId=@numOppID AND ISNULL(tintshipped,0) = 1) 
	BEGIN	 
		RAISERROR('OPP/ORDER_IS_CLOSED',16,1)
		RETURN
	END

	DECLARE @dtItemRelease AS DATE
	SET @dtItemRelease = DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())


	IF CONVERT(VARCHAR(10),@strItems) <> ''
	BEGIN
		EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems

		--VALIDATE PROMOTION OF COUPON CODE
		DECLARE @TEMP TABLE
		(
			ID INT IDENTITY(1,1),
			numOppItemID NUMERIC(18,0),
			numPromotionID NUMERIC(18,0)
		)

		INSERT INTO @TEMP
		(
			numOppItemID,
			numPromotionID
		)
		SELECT
			numoppitemtCode,
			numPromotionID
		FROM
			OPENXML (@hDocItem,'/NewDataSet/Item',2)
		WITH
		(
			numoppitemtCode NUMERIC(18,0),
			numPromotionID NUMERIC(18,0)
		)

		IF (SELECT
				COUNT(*)
			FROM
				PromotionOffer PO
			WHERE
				numDomainId=@numDomainId
				AND bitRequireCouponCode = 1
				AND ISNULL(bitEnabled,0) = 1
				AND ISNULL(tintUsageLimit,0) > 0
				AND (intCouponCodeUsed 
					- (CASE WHEN ISNULL((SELECT COUNT(*) FROM OpportunityItems WHERE numOppID=ISNULL(@numOppID,0) AND numPromotionID=PO.numProId),0) > 0 THEN 1 ELSE 0 END) 
					+ (CASE WHEN ISNULL((SELECT COUNT(*) FROM @TEMP WHERE numPromotionID=PO.numProId),0) > 0 THEN 1 ELSE 0 END)) > tintUsageLimit) > 0
		BEGIN
			RAISERROR('USAGE_OF_COUPON_EXCEED',16,1)
			RETURN
		END
		ELSE
		BEGIN
			UPDATE
				PO
			SET 
				intCouponCodeUsed = ISNULL(intCouponCodeUsed,0) 
									- (CASE WHEN ISNULL((SELECT COUNT(*) FROM OpportunityItems WHERE numOppID=ISNULL(@numOppID,0) AND numPromotionID=PO.numProId),0) > 0 THEN 1 ELSE 0 END) 
									+ (CASE WHEN ISNULL(T1.intUsed,0) > 0 THEN 1 ELSE 0 END) 
			FROM
				PromotionOffer PO
			INNER JOIN
				(
					SELECT
						numPromotionID,
						COUNT(*) AS intUsed
					FROM
						@TEMP
					GROUP BY
						numPromotionID
				) AS T1
			ON
				PO.numProId = T1.numPromotionID
			WHERE
				numDomainId=@numDomainId
				AND bitRequireCouponCode = 1
				AND ISNULL(bitEnabled,0) = 1
		END
	END

	DECLARE @numCompany AS NUMERIC(18)
	SELECT @numCompany = numCompanyID FROM DivisionMaster WHERE numDivisionID = @numDivisionId

	DECLARE @tintCommitAllocation AS TINYINT
	DECLARE @bitAllocateInventoryOnPickList AS BIT

	SELECT @tintCommitAllocation=ISNULL(tintCommitAllocation,1) FROM Domain WHERE numDomainId=@numDomainId
	SELECT @bitAllocateInventoryOnPickList=ISNULL(bitAllocateInventoryOnPickList,0) FROM DivisionMaster WHERE numDivisionID=@numDivisionId

	--IF NEW OPPERTUNITY
	IF @numOppID = 0                                                                          
	BEGIN
		DECLARE @bitIsInitalSalesOrder BIT
		IF(@DealStatus=1 AND @tintOppType=1)
		BEGIN
			SET @bitIsInitalSalesOrder=1
		END

		DECLARE @intOppTcode AS NUMERIC(9)  
  
		SELECT @intOppTcode = MAX(numOppId) FROM OpportunityMaster                

		SET @intOppTcode = ISNULL(@intOppTcode,0) + 1                                                
		
		SET @vcPOppName = @vcPOppName + '-'+ CONVERT(varchar(4),YEAR(GETUTCDATE()))  +'-A'+ CONVERT(varchar(10),@intOppTcode)                                  
  
		IF ISNULL(@numCurrencyID,0) = 0 
		BEGIN
			SET @fltExchangeRate=1
			SELECT @numCurrencyID= ISNULL(numCurrencyID,0) FROM dbo.Domain WHERE numDomainId=@numDomainId	
		END
		ELSE
		BEGIN 
			SET @fltExchangeRate = dbo.GetExchangeRate(@numDomainID,@numCurrencyID)
        END  
		                                                        
		IF ISNULL(@numAccountClass,0) = 0
		BEGIN                                                  
		DECLARE @tintDefaultClassType AS INT = 0
		SELECT @tintDefaultClassType = ISNULL(tintDefaultClassType,0) FROM Domain WHERE numDomainId = @numDomainID 

		IF @tintDefaultClassType = 1 --USER
		BEGIN
			SELECT 
				@numAccountClass=ISNULL(numDefaultClass,0) 
			FROM 
				dbo.UserMaster UM 
			JOIN 
				dbo.Domain D 
			ON 
				UM.numDomainID=D.numDomainId
			WHERE 
				D.numDomainId=@numDomainId 
				AND UM.numUserDetailId=@numUserCntID
		END
		ELSE IF @tintDefaultClassType = 2 --COMPANY
		BEGIN
			SELECT 
				@numAccountClass=ISNULL(numAccountClassID,0) 
			FROM 
				dbo.DivisionMaster DM 
			WHERE 
				DM.numDomainId=@numDomainId 
				AND DM.numDivisionID=@numDivisionID
		END
		ELSE
		BEGIN
			SET @numAccountClass = 0
		END
		END

        IF(@DealStatus=1 AND @tintOppType=1 AND @tintClickBtn=1)
		BEGIN
			SET @DealStatus=0
		END         
		--PRINT ('Deal Staus ' +@DealStatus)                                                      
		INSERT INTO OpportunityMaster
		(                                                                             
			numContactId,numDivisionId,txtComments,numCampainID,bitPublicFlag,tintSource,tintSourceType,vcPOppName,                                                                          
			intPEstimatedCloseDate,monPAmount,numCreatedBy,bintCreatedDate,numModifiedBy,bintModifiedDate,numDomainId,                                                                                                             
			numRecOwner,lngPConclAnalysis,tintOppType,numSalesOrPurType,numCurrencyID,fltExchangeRate,numAssignedTo,
			numAssignedBy,[tintOppStatus],numStatus,vcOppRefOrderNo,bitStockTransfer,bitBillingTerms,intBillingDays,
			bitInterestType,fltInterest,vcCouponCode,bitDiscountType,fltDiscount,bitPPVariance,vcMarketplaceOrderID,
			vcMarketplaceOrderReportId,numPercentageComplete,numAccountClass,tintTaxOperator,bitUseShippersAccountNo,
			bitUseMarkupShippingRate,numMarkupShippingRate,intUsedShippingCompany,numShipmentMethod,numShippingService,numPartner,numPartenerContact,dtReleaseDate,bitIsInitalSalesOrder,numVendorAddressID,numShipFromWarehouse
		)                                                                          
		Values                                                                          
		(                                                                          
			@numContactId,@numDivisionId,ISNULL(@Comments,''),@CampaignID,@bitPublicFlag,@tintSource,@tintSourceType,@vcPOppName,                                                                          
			@dtEstimatedCloseDate,@monPAmount,@numUserCntID,(CASE @tintSourceType WHEN 3 THEN @vcMarketplaceOrderDate ELSE GETUTCDATE() END),                                                                          
			@numUserCntID,GETUTCDATE(),@numDomainId,@numUserCntID,@lngPConclAnalysis,@tintOppType,@numSalesOrPurType,@numCurrencyID, 
			@fltExchangeRate,(CASE WHEN @numAssignedTo > 0 THEN @numAssignedTo ELSE NULL END),(CASE WHEN @numAssignedTo>0 THEN @numUserCntID ELSE NULL END),
			@DealStatus,@numStatus,@vcOppRefOrderNo,@bitStockTransfer,@bitBillingTerms,@intBillingDays,@bitInterestType,@fltInterest,
			@vcCouponCode,@bitDiscountType,@fltDiscount,1,@vcMarketplaceOrderID,@vcMarketplaceOrderReportId,@numPercentageComplete,
			@numAccountClass,@tintTaxOperator,@bitUseShippersAccountNo,@bitUseMarkupShippingRate,@numMarkupShippingRate,@intUsedShippingCompany,@numShipmentMethod,@numShippingService,@numPartner,@numPartenerContactId,@dtReleaseDate,@bitIsInitalSalesOrder,@numVendorAddressID,@numShipFromWarehouse
		)                                                                                                                      
		
		SET @numOppID=(SELECT SCOPE_IDENTITY())                
                            
		--UPDATE OPPNAME AS PER NAME TEMPLATE
		EXEC dbo.USP_UpdateNameTemplateValue @tintOppType,@numDomainID,@numOppID

		--MAP CUSTOM FIELD	
		DECLARE @tintPageID AS TINYINT;
		SET @tintPageID = CASE WHEN @tintOppType=1 THEN 2 ELSE 6 END 
  	
		EXEC dbo.USP_AddParentChildCustomFieldMap
				@numDomainID = @numDomainID, --  numeric(18, 0)
				@numRecordID = @numOppID, --  numeric(18, 0)
				@numParentRecId = @numDivisionId, --  numeric(18, 0)
				@tintPageID = @tintPageID --  tinyint
 	
		IF ISNULL(@numStatus,0) > 0 AND @tintOppType=1 AND isnull(@DealStatus,0)=1
		BEGIN
	   		EXEC USP_ManageOpportunityAutomationQueue
					@numOppQueueID = 0, -- numeric(18, 0)
					@numDomainID = @numDomainID, -- numeric(18, 0)
					@numOppId = @numOppID, -- numeric(18, 0)
					@numOppBizDocsId = 0, -- numeric(18, 0)
					@numOrderStatus = @numStatus, -- numeric(18, 0)
					@numUserCntID = @numUserCntID, -- numeric(18, 0)
					@tintProcessStatus = 1, -- tinyint
					@tintMode = 1 -- TINYINT
		END
  
		-- INSERTING ITEMS                                                                                                                    
		IF CONVERT(VARCHAR(10),@strItems) <> '' AND @numOppID > 0
		BEGIN
			EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems       
	
			INSERT INTO OpportunityItems
			(
				numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,bitDropShip,bitDiscountType,fltDiscount,
				monTotAmtBefDiscount,vcItemName,vcModelID,vcManufacturer,vcPathForTImage,monVendorCost,numUOMId,bitWorkOrder,numVendorWareHouse,
				numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,numClassID,numToWarehouseItemID,vcAttributes,vcAttrValues,monAvgCost,bitItemPriceApprovalRequired,
				numPromotionID,bitPromotionTriggered,vcPromotionDetail,numSortOrder,numCost,vcNotes,vcInstruction,bintCompliationDate,numWOAssignedTo,numShipToAddressID,ItemReleaseDate
			)
			SELECT 
				@numOppID,X.numItemCode,dbo.fn_UOMConversion(X.numUOM,numItemCode,@numDomainId,null) * x.numUnitHour,x.monPrice,x.monTotAmount,X.vcItemDesc,
				(CASE X.numWarehouseItmsID 
				WHEN -1 
				THEN 
					CASE 
						WHEN ((SELECT COUNT(*) FROM Item WHERE numDomainID=@numDomainId AND vcSKU= X.vcSKU) > 0)
							THEN
								(SELECT TOP 1 
									numWareHouseItemID 
								FROM 
									WareHouseItems 
								WHERE 
									[numItemID] = X.numItemCode 
									AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId))
						ELSE
							(SELECT 
								[numWareHouseItemID] 
							FROM 
								[WareHouseItems] 
							WHERE 
								[numItemID] = X.numItemCode 
								AND ISNULL(WareHouseItems.vcWHSKU,'') = ISNULL(X.vcSKU,'')
								AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId)) 
					END
				WHEN 0 
				THEN 
					CASE 
					WHEN ((SELECT COUNT(*) FROM Item WHERE numDomainID=@numDomainId AND vcSKU=X.vcSKU) > 0)
						THEN
							(SELECT TOP 1 
								numWareHouseItemID 
							FROM 
								WareHouseItems 
							WHERE 
								[numItemID] = X.numItemCode 
								AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId))
					ELSE
						(SELECT 
							[numWareHouseItemID] 
						FROM 
							[WareHouseItems] 
						WHERE 
							[numItemID] = X.numItemCode 
							AND ISNULL(WareHouseItems.vcWHSKU,'') = ISNULL(X.vcSKU,'')
							AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId)) 
					END
				ELSE  
					X.numWarehouseItmsID 
				END) AS numWarehouseItmsID,
				X.ItemType,X.DropShip,X.bitDiscountType,X.fltDiscount,X.monTotAmtBefDiscount,X.vcItemName,(SELECT vcModelID FROM item WHERE numItemCode = X.numItemCode),
				(SELECT vcManufacturer FROM item WHERE numItemCode = X.numItemCode),(SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = X.numItemCode AND BitDefault = 1 AND numdomainId = @numdomainId),
				(SELECT isnull(VN.monCost,0) FROM Vendor VN INNER JOIN Item IT ON IT.numItemCode=VN.numItemCode AND VN.numVendorID=IT.numVendorID WHERE  VN.numItemCode=X.numItemCode),
				X.numUOM,X.bitWorkOrder,X.numVendorWareHouse,X.numShipmentMethod,X.numSOVendorId,X.numProjectID,X.numProjectStageID,
				0,
				X.numToWarehouseItemID,X.Attributes,X.AttributeIDs,(SELECT (CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(I.monAverageCost,0) END) FROM Item I WHERE I.numDomainId=@numDomainId AND I.numItemCode=X.numItemCode),
				X.bitItemPriceApprovalRequired,X.numPromotionID,X.bitPromotionTriggered,X.vcPromotionDetail,X.numSortOrder,ISNULL(X.numCost,0),ISNULL(X.vcVendorNotes,'')
				,X.vcInstruction,X.bintCompliationDate,X.numWOAssignedTo,X.numShipToAddressID,ISNULL(ItemReleaseDate,@dtItemRelease)
			FROM
				(
					SELECT 
						*
					FROM 
						OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
					WITH                       
					(                                                                          
						numoppitemtCode NUMERIC(9),numItemCode NUMERIC(9), numUnitHour FLOAT, monPrice DECIMAL(30,16), monTotAmount DECIMAL(20,5),vcItemDesc VARCHAR(1000),                                    
						numWarehouseItmsID NUMERIC(9), ItemType VARCHAR(30), DropShip BIT, bitDiscountType BIT,	fltDiscount DECIMAL(30,16), monTotAmtBefDiscount DECIMAL(20,5),
						vcItemName VARCHAR(300),numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId NUMERIC(9),
						numProjectID numeric(9),numProjectStageID numeric(9),numToWarehouseItemID NUMERIC(9),Attributes varchar(500),AttributeIDs VARCHAR(500),vcSKU VARCHAR(100),bitItemPriceApprovalRequired BIT,
						numPromotionID NUMERIC(18,0),bitPromotionTriggered BIT,vcPromotionDetail VARCHAR(2000),numSortOrder NUMERIC(18,0),numCost DECIMAL(20,5),vcVendorNotes VARCHAR(1000),
						vcInstruction VARCHAR(1000),bintCompliationDate DATETIME,numWOAssignedTo NUMERIC(18,0),numShipToAddressID  NUMERIC(18,0),ItemReleaseDate DATETIME
					)
				)X    
			ORDER BY 
				X.numoppitemtCode
    
			-- UPDATE EXISTING CUSTOMER PART NO
			UPDATE
				CPN
			SET
				CPN.CustomerPartNo = ISNULL(TEMPCPN.CustomerPartNo,'')
			FROM
				CustomerPartNumber CPN
			INNER JOIN
			(
				SELECT 
					numItemCode
					,MIN(CustomerPartNo) AS CustomerPartNo
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
				WITH                       
				(                                                                          
					CustomerPartNo VARCHAR(300), numItemCode NUMERIC(18,0)
				)
				GROUP BY
					numItemCode
			) AS TEMPCPN
			ON
				CPN.numCompanyId = @numCompany
				AND CPN.numItemCode = TEMPCPN.numItemCode
			WHERE
				CPN.numDomainID=@numDomainID
				AND CPN.numCompanyId = @numCompany
				AND LEN(ISNULL(TEMPCPN.CustomerPartNo,'')) > 0
				AND ISNULL(TEMPCPN.CustomerPartNo,'') <> '0'
			
			-- INSERT NEW CUSTOMER PART NO
			INSERT INTO CustomerPartNumber
			(
				numCompanyId, numDomainID, CustomerPartNo, numItemCode 
			)
			SELECT 
				@numCompany, @numDomainID, TEMPCPN.CustomerPartNo,TEMPCPN.numItemCode
			FROM
			(
				SELECT 
					numItemCode
					,MIN(CustomerPartNo) AS CustomerPartNo
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
				WITH                       
				(                                                                          
					CustomerPartNo VARCHAR(300), numItemCode NUMERIC(18,0)
				)
				GROUP BY
					numItemCode
			) TEMPCPN 
			LEFT JOIN
				CustomerPartNumber CPN
			ON
				CPN.numCompanyId = @numCompany
				AND CPN.numItemCode = TEMPCPN.numItemCode
			WHERE
				CPN.CustomerPartNoID IS NULL
				AND LEN(ISNULL(TEMPCPN.CustomerPartNo,'')) > 0
				AND ISNULL(TEMPCPN.CustomerPartNo,'') <> '0'

			UPDATE 
				OpportunityItems                       
			SET 
				numItemCode=X.numItemCode,numOppId=@numOppID,numUnitHour=dbo.fn_UOMConversion(X.numUOM,X.numItemCode,@numDomainId,null) * X.numUnitHour,                      
				monPrice=x.monPrice,monTotAmount=x.monTotAmount,vcItemDesc=X.vcItemDesc,numWarehouseItmsID=X.numWarehouseItmsID,bitDropShip=X.DropShip,
				bitDiscountType=X.bitDiscountType,fltDiscount=X.fltDiscount,monTotAmtBefDiscount=X.monTotAmtBefDiscount,vcItemName=X.vcItemName,numUOMId=numUOM,
				bitWorkOrder=X.bitWorkOrder,numVendorWareHouse=X.numVendorWareHouse,numShipmentMethod=X.numShipmentMethod,numSOVendorId=X.numSOVendorId,
				numProjectId=X.numProjectID,numProjectStageId=X.numProjectStageID,numToWarehouseItemID = X.numToWarehouseItemID,vcAttributes=X.Attributes,
				numClassID=0,vcModelID=X.vcModelID,
				bitItemPriceApprovalRequired = X.bitItemPriceApprovalRequired,vcAttrValues=X.AttributeIDs,
				numPromotionID=X.numPromotionID,bitPromotionTriggered=X.bitPromotionTriggered,vcPromotionDetail=X.vcPromotionDetail,numSortOrder=X.numSortOrder,numCost=X.numCost
				,vcNotes=X.vcVendorNotes,vcInstruction=X.vcInstruction,bintCompliationDate=X.bintCompliationDate,numWOAssignedTo=X.numWOAssignedTo
			FROM 
			(
				SELECT 
					numoppitemtCode as numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,vcModelID,numWarehouseItmsID,DropShip,bitDiscountType,fltDiscount,
					monTotAmtBefDiscount,vcItemName,numUOM,bitWorkOrder,numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,numToWarehouseItemID,
					Attributes,bitItemPriceApprovalRequired,AttributeIDs,numPromotionID,bitPromotionTriggered,vcPromotionDetail,numSortOrder,ISNULL(numCost,0) numCost
					,ISNULL(vcVendorNotes,'') vcVendorNotes,vcInstruction,bintCompliationDate,numWOAssignedTo
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=2]',2)
				WITH  
					(                      
						numoppitemtCode NUMERIC(9),numItemCode NUMERIC(9),numUnitHour FLOAT,monPrice DECIMAL(30,16),monTotAmount DECIMAL(20,5),vcItemDesc VARCHAR(1000), vcModelID VARCHAR(300),
						numWarehouseItmsID NUMERIC(9),DropShip BIT,bitDiscountType BIT,fltDiscount DECIMAL(30,16),monTotAmtBefDiscount DECIMAL(20,5),vcItemName VARCHAR(300),
						numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId NUMERIC(9),numProjectID NUMERIC(9),
						numProjectStageID numeric(9),numToWarehouseItemID NUMERIC(9),Attributes varchar(500),bitItemPriceApprovalRequired BIT,AttributeIDs VARCHAR(500),
						numPromotionID NUMERIC(18,0),bitPromotionTriggered BIT,vcPromotionDetail VARCHAR(2000),numSortOrder NUMERIC(18,0),numCost DECIMAL(20,5),vcVendorNotes VARCHAR(1000)
						,vcInstruction VARCHAR(1000),bintCompliationDate DATETIME,numWOAssignedTo NUMERIC(18,0)
					)
			)X 
			WHERE 
				numoppitemtCode=X.numOppItemID AND numOppID=@numOppID
				
			--INSERT KIT ITEMS                 
			INSERT INTO OpportunityKitItems
			(
				numOppId,
				numOppItemID,
				numChildItemID,
				numWareHouseItemId,
				numQtyItemsReq,
				numQtyItemsReq_Orig,
				numUOMId,
				numQtyShipped,
				monAvgCost
			)
			SELECT
				@numOppId,
				OI.numoppitemtCode,
				ID.numChildItemID,
				ID.numWareHouseItemId,
				(ID.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(ID.numUOMId,ID.numChildItemID,@numDomainID,I.numBaseUnit),1)) * OI.numUnitHour,
				ID.numQtyItemsReq,
				ID.numUOMId,
				0,
				ISNULL(I.monAverageCost,0)
			FROM 
				OpportunityItems OI 
			JOIN 
				ItemDetails ID 
			ON 
				OI.numItemCode=ID.numItemKitID 
			JOIN
				Item I
			ON
				ID.numChildItemID = I.numItemCode
			WHERE 
				OI.numOppId=@numOppId 

			IF (SELECT COUNT(*) FROM OPENXML (@hDocItem,'/NewDataSet/Item[bitHasKitAsChild=''true'']',2)) > 0
			BEGIN
				--INSERT KIT ITEM HAS ANOTHER KIT AS CHILD THEN SAVE CONFIGURATION OF KIT
				DECLARE @TempKitConfiguration TABLE
				(
					numItemCode NUMERIC(18,0),
					numWarehouseItemID NUMERIC(18,0),
					KitChildItems VARCHAR(MAX)
				)

				INSERT INTO 
					@TempKitConfiguration
				SELECT
					numItemCode,
					numWarehouseItmsID,
					KitChildItems
				FROM
					OPENXML (@hDocItem,'/NewDataSet/Item[bitHasKitAsChild=''true'']',2)
				WITH                       
				(
					numItemCode NUMERIC(9),numWarehouseItmsID NUMERIC(9), KitChildItems VARCHAR(MAX)
				) 

				INSERT INTO OpportunityKitChildItems
				(
					numOppID,
					numOppItemID,
					numOppChildItemID,
					numItemID,
					numWareHouseItemId,
					numQtyItemsReq,
					numQtyItemsReq_Orig,
					numUOMId,
					numQtyShipped,
					monAvgCost
				)
				SELECT 
					@numOppID,
					OKI.numOppItemID,
					OKI.numOppChildItemID,
					ItemDetails.numChildItemID,
					ItemDetails.numWareHouseItemId,
					(ItemDetails.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(ItemDetails.numUOMId,ItemDetails.numChildItemID,@numDomainID,Item.numBaseUnit),1)) * OKI.numQtyItemsReq,
					ItemDetails.numQtyItemsReq,
					ItemDetails.numUOMId,
					0,
					ISNULL(Item.monAverageCost,0)
				FROM
					(
						SELECT 
							t1.numItemCode,
							t1.numWarehouseItemID,
							SUBSTRING(
										SUBSTRING(items,0,CHARINDEX('-',items)),
										0,
										CHARINDEX('#',SUBSTRING(items,0,CHARINDEX('-',items)))
									  ) AS numKitItemID,
							SUBSTRING(
										SUBSTRING(items,0,CHARINDEX('-',items)),
										CHARINDEX('#',SUBSTRING(items,0,CHARINDEX('-',items))) + 1, 
										LEN(SUBSTRING(items,0,CHARINDEX('-',items))) - CHARINDEX('#',SUBSTRING(items,0,CHARINDEX('-',items)))
									  ) AS numKitWarehouseItemID,
							SUBSTRING(
										SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)), 
										0, 
										CHARINDEX('#',SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)))
									  ) As numKitChildItemID,
							SUBSTRING(
										SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)), 
										CHARINDEX('#',SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items))) + 1, 
										LEN(SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items))) - CHARINDEX('#',SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)))
									  ) As numKitChildItemWarehouseItemID
						FROM 
							@TempKitConfiguration t1
						CROSS APPLY
						(
							SELECT
								*
							FROM 
								dbo.Split((SELECT KitChildItems FROM @TempKitConfiguration WHERE numItemCode=t1.numItemCode AND numWarehouseItemID=t1.numWarehouseItemID),',') 
						) as t2
					) TempChildItems
				INNER JOIN
					OpportunityKitItems OKI
				ON
					OKI.numOppId=@numOppId
					AND OKI.numChildItemID=TempChildItems.numKitItemID
					AND OKI.numWarehouseItemId=TempChildItems.numKitWarehouseItemID
				INNER JOIN
					OpportunityItems OI
				ON
					OI.numItemCode = TempChildItems.numItemCode
					AND OI.numWarehouseItmsID = TempChildItems.numWarehouseItemID
					AND OI.numoppitemtcode = OKI.numOppItemID
				INNER JOIN
					ItemDetails
				ON
					TempChildItems.numKitItemID = ItemDetails.numItemKitID
					AND TempChildItems.numKitChildItemID = ItemDetails.numChildItemID
					AND (TempChildItems.numKitChildItemWarehouseItemID = ItemDetails.numWareHouseItemId  OR ISNULL(TempChildItems.numKitChildItemWarehouseItemID,0) = 0)
				INNER JOIN
					Item 
				ON
					ItemDetails.numChildItemID = Item.numItemCode
			END

			-- UPDATE UOM OF OPPORTUNITY ITEMS HAVE NOT UOM SET WHILE EXPORTED FROM MARKETPLACE (MEANS TINTSOURCETYPE = 3)
			IF ISNULL(@tintSourceType,0) = 3
			BEGIN
				UPDATE 
					OI 
				SET 
					numUOMID = ISNULL(I.numSaleUnit,0), 
					numUnitHour = dbo.fn_UOMConversion(ISNULL(I.numSaleUnit,0),OI.numItemCode,@numDomainId,null) * OI.numUnitHour
				FROM 
					OpportunityItems OI
				JOIN 
					Item I 
				ON 
					OI.numItemCode = I.numItemCode
				WHERE 
					numOppId = @numOppID
					AND I.numDomainID = @numDomainId
			END	
                                     
			INSERT INTO OppWarehouseSerializedItem
			(
				numOppID,
				numWarehouseItmsDTLID,
				numOppItemID,
				numWarehouseItmsID
			)                      
			SELECT 
				@numOppID,
				X.numWarehouseItmsDTLID,
				X.numoppitemtCode,
				X.numWItmsID 
			FROM
			(                                                                          
				SELECT
					* 
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/SerialNo',2)                                                                          
				WITH                        
				(                                               
				numWarehouseItmsDTLID numeric(9),                      
				numoppitemtCode numeric(9),                      
				numWItmsID numeric(9)                                                     
				)
			)X                                    
                                   
			UPDATE 
				OppWarehouseSerializedItem                       
			SET 
				numOppItemID=X.numoppitemtCode                                
			FROM                       
			(
				SELECT 
					numWarehouseItmsDTLID AS DTLID,
					O.numoppitemtCode 
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/SerialNo',2)                                                                         
				WITH
				(
					numWarehouseItmsDTLID NUMERIC(9),numoppitemtCode NUMERIC(9),numWItmsID NUMERIC(9)
				)                                
				JOIN 
					OpportunityItems O 
				ON 
					O.numWarehouseItmsID=numWItmsID and O.numOppID=@numOppID
			)X                 
			WHERE 
				numOppID=@numOppID 
				AND numWarehouseItmsDTLID=X.DTLID      
    
			IF NOT (@tintCommitAllocation=2 AND @bitAllocateInventoryOnPickList=1)
			BEGIN
				EXEC USP_ManageSOWorkOrder @numDomainId,@numOppID,@strItems,@numUserCntID
			END
                     
			EXEC sp_xml_removedocument @hDocItem                                     
		END          
	END
	ELSE                                                                                                                          
	BEGIN

		DECLARE @tempAssignedTo AS NUMERIC(9) = NULL                                                    
		SELECT 
			@tintOppStatus = tintOppStatus,
			@tempAssignedTo=ISNULL(numAssignedTo,0) 
		FROM   
			OpportunityMaster 
		WHERE  
			numOppID = @numOppID

		IF @tempAssignedTo <> @numAssignedTo
		BEGIN
			IF (SELECT COUNT(*) FROM BizDocComission WHERE ISNULL(numOppID,0) = @numOppID AND ISNULL(bitCommisionPaid,0) = 1) > 0
			BEGIN
				RAISERROR('COMMISSION_PAID_AGAINST_BIZDOC',16,1)
			END
		END
 
		--REVERTING BACK THE WAREHOUSE ITEMS                  
		IF @tintOppStatus = 1 AND @tintCommitAllocation=1
		BEGIN               
			EXEC USP_RevertDetailsOpp @numOppID,0,@numUserCntID                  
		END                  

		IF LEN(ISNULL(@vcOppRefOrderNo,''))>0
		BEGIN
			UPDATE dbo.OpportunityBizDocs SET vcRefOrderNo=@vcOppRefOrderNo WHERE numOppId=@numOppID
		END
		
		IF EXISTS (SELECT 1 FROM dbo.OpportunityMaster WHERE numOppId = @numOppID AND tintOppType=1 AND isnull(tintOppStatus,0)=1 AND ISNULL(numStatus,0)!= @numStatus)
		BEGIN
			EXEC USP_ManageOpportunityAutomationQueue
					@numOppQueueID = 0, -- numeric(18, 0)
					@numDomainID = @numDomainID, -- numeric(18, 0)
					@numOppId = @numOppID, -- numeric(18, 0)
					@numOppBizDocsId = 0, -- numeric(18, 0)
					@numOrderStatus = @numStatus, -- numeric(18, 0)
					@numUserCntID = @numUserCntID, -- numeric(18, 0)
					@tintProcessStatus = 1, -- tinyint
					@tintMode = 1 -- TINYINT
		END 
						
		UPDATE   
			OpportunityMaster
		SET      
			vcPOppName = @vcPOppName,txtComments = ISNULL(@Comments,''),bitPublicFlag = @bitPublicFlag,numCampainID = @CampaignID,tintSource = @tintSource,tintSourceType=@tintSourceType,
			intPEstimatedCloseDate = @dtEstimatedCloseDate,numModifiedBy = @numUserCntID,bintModifiedDate = GETUTCDATE(),lngPConclAnalysis = @lngPConclAnalysis,monPAmount = @monPAmount,
			tintActive = @tintActive,numSalesOrPurType = @numSalesOrPurType,numStatus = @numStatus,vcOppRefOrderNo=@vcOppRefOrderNo,
			bintOppToOrder=(CASE WHEN @tintOppStatus=0 AND @DealStatus=1 THEN GETUTCDATE() ELSE bintOppToOrder END),bitDiscountType=@bitDiscountType,fltDiscount=@fltDiscount,
			bitBillingTerms=@bitBillingTerms,intBillingDays=@intBillingDays,bitInterestType=@bitInterestType,fltInterest=@fltInterest,tintTaxOperator=@tintTaxOperator,
			numDiscountAcntType=@numDiscountAcntType,vcCouponCode = @vcCouponCode,numPercentageComplete =@numPercentageComplete,bitUseShippersAccountNo = @bitUseShippersAccountNo,
			intUsedShippingCompany = @intUsedShippingCompany, numContactID=@numContactId,numShipmentMethod=@numShipmentMethod,numShippingService=@numShippingService,numPartner=@numPartner,numPartenerContact=@numPartenerContactId
		WHERE
			numOppId = @numOppID   
	   
		-- UPDATING IF ORGANIZATION IS ASSIGNED TO SOMEONE                                                      
		IF ( @tempAssignedTo <> @numAssignedTo AND @numAssignedTo <> '0' ) 
		BEGIN
			UPDATE
				OpportunityMaster 
			SET	
				numAssignedTo = @numAssignedTo, 
				numAssignedBy = @numUserCntID 
			WHERE 
				numOppId = @numOppID                                                    
		END                                                     
		ELSE IF(@numAssignedTo = 0) 
		BEGIN
			UPDATE 
				OpportunityMaster 
			SET 
				numAssignedTo = 0, 
				numAssignedBy = 0 
			WHERE
				numOppId = @numOppID
		END  
			                                                                    
		-- UPDATING OPP ITEMS
		IF CONVERT(VARCHAR(10),@strItems) <> '' AND @numOppID > 0                                                                         
		BEGIN
			EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems                                      
			   
			IF EXISTS 
			(
				SELECT 
					OI.numoppitemtCode
				FROM 
					OpportunityItems OI
				WHERE 
					numOppID=@numOppID 
					AND OI.numoppitemtCode NOT IN (SELECT numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) WITH (numoppitemtCode NUMERIC(9)))
					AND ISNULL((SELECT COUNT(*) FROM OppWarehouseSerializedItem WHERE numOppId=@numOppID AND numOppItemID=OI.numoppitemtCode),0) > 0
			)
			BEGIN
				RAISERROR('SERIAL/LOT#_ASSIGNED_TO_ITEM_DELETED',16,1)
			END

			--DELETE CHILD ITEMS OF KIT WITHIN KIT
			DELETE FROM 
				OpportunityKitChildItems 
			WHERE 
				numOppID=@numOppID AND 
				numOppItemID NOT IN (SELECT ISNULL(numoppitemtCode,0) FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) WITH (numoppitemtCode NUMERIC(9)))   

			--DELETE ITEMS
			DELETE FROM 
				OpportunityKitItems 
			WHERE 
				numOppID=@numOppID AND 
				numOppItemID NOT IN (SELECT ISNULL(numoppitemtCode,0) FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) WITH (numoppitemtCode NUMERIC(9)))                      
			                            
			DELETE FROM 
				OpportunityItems 
			WHERE 
				numOppID=@numOppID 
				AND numoppitemtCode NOT IN (SELECT numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) WITH (numoppitemtCode NUMERIC(9)))   
	          
			-- DELETE CORRESPONSING TIME AND EXPENSE ENTRIES OF DELETED ITEMS
			DELETE FROM
				TimeAndExpense
			WHERE
				numDomainID=@numDomainId
				AND numOppId=@numOppID
				AND numOppItemID NOT IN (SELECT numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) WITH (numoppitemtCode NUMERIC(9))) 

			--INSERT NEW ADDED ITEMS                        
			INSERT INTO OpportunityItems
			(
				numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,
				[vcItemName],[vcModelID],[vcManufacturer],vcPathForTImage,monVendorCost,numUOMId,bitWorkOrder,numVendorWareHouse,numToWarehouseItemID,numShipmentMethod,
				numSOVendorId,numProjectID,numProjectStageID,vcAttributes,numClassID,monAvgCost,bitItemPriceApprovalRequired,vcAttrValues,numPromotionID,bitPromotionTriggered
				,vcPromotionDetail,numSortOrder,numCost,vcNotes,vcInstruction,bintCompliationDate,numWOAssignedTo,ItemReleaseDate
			)
			SELECT 
				@numOppID,X.numItemCode,dbo.fn_UOMConversion(X.numUOM,numItemCode,@numDomainId,null) * x.numUnitHour,x.monPrice,x.monTotAmount,X.vcItemDesc,X.numWarehouseItmsID,
				X.ItemType,X.DropShip,X.bitDiscountType,X.fltDiscount,X.monTotAmtBefDiscount,X.vcItemName,(SELECT vcModelID FROM item WHERE numItemCode = X.numItemCode),
				(SELECT vcManufacturer FROM item WHERE numItemCode = X.numItemCode),
				(SELECT TOP 1 vcPathForTImage FROM dbo.ItemImages WHERE numItemCode = X.numItemCode AND BitDefault = 1 AND numDomainId = @numDomainId),
				(select isnull(VN.monCost,0) from Vendor VN INNER JOIN Item IT ON IT.numItemCode=VN.numItemCode and VN.numVendorID=IT.numVendorID where VN.numItemCode=X.numItemCode),
				X.numUOM,X.bitWorkOrder,X.numVendorWareHouse,X.numToWarehouseItemID,X.numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,Attributes,
				0,
				(SELECT (CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(I.monAverageCost,0) END) FROM Item I WHERE I.numDomainId=@numDomainId AND I.numItemCode=X.numItemCode), X.bitItemPriceApprovalRequired, X.AttributeIDs,
				X.numPromotionID,X.bitPromotionTriggered,X.vcPromotionDetail,X.numSortOrder,ISNULL(X.numCost,0),ISNULL(vcVendorNotes,'')
				,X.vcInstruction,X.bintCompliationDate,X.numWOAssignedTo,@dtItemRelease
			FROM
			(
				SELECT 
					*
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
				WITH  
				(                      
					numoppitemtCode numeric(9),numItemCode numeric(9),numUnitHour FLOAT,monPrice DECIMAL(30,16),monTotAmount DECIMAL(20,5),vcItemDesc varchar(1000),                                    
					numWarehouseItmsID numeric(9),numToWarehouseItemID numeric(9),Op_Flag tinyint,ItemType varchar(30),DropShip bit,bitDiscountType bit,
					fltDiscount DECIMAL(30,16),monTotAmtBefDiscount DECIMAL(20,5),vcItemName VARCHAR(300),numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),
					numShipmentMethod NUMERIC(9),numSOVendorId numeric(9),numProjectID numeric(9),numProjectStageID numeric(9),Attributes varchar(500), 
					bitItemPriceApprovalRequired BIT,AttributeIDs VARCHAR(500),numPromotionID NUMERIC(18,0),bitPromotionTriggered BIT,vcPromotionDetail VARCHAR(2000)
					,numSortOrder NUMERIC(18,0),numCost DECIMAL(20,5),vcVendorNotes VARCHAR(1000),vcInstruction VARCHAR(1000),bintCompliationDate DATETIME,numWOAssignedTo NUMERIC(18,0)
				)
			)X 
			
			-- UPDATE EXISTING CUSTOMER PART NO
			UPDATE
				CPN
			SET
				CPN.CustomerPartNo = ISNULL(TEMPCPN.CustomerPartNo,'')
			FROM
				CustomerPartNumber CPN
			INNER JOIN
			(
				SELECT 
					numItemCode
					,MIN(CustomerPartNo) AS CustomerPartNo
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item',2)                                                                          
				WITH                       
				(                                                                          
					CustomerPartNo VARCHAR(300), numItemCode NUMERIC(18,0)
				)
				GROUP BY
					numItemCode
			) AS TEMPCPN
			ON
				CPN.numCompanyId = @numCompany
				AND CPN.numItemCode = TEMPCPN.numItemCode
			WHERE
				CPN.numDomainID=@numDomainID
				AND CPN.numCompanyId = @numCompany
				AND LEN(ISNULL(TEMPCPN.CustomerPartNo,'')) > 0
				AND ISNULL(TEMPCPN.CustomerPartNo,'') <> '0'
			
			-- INSERT NEW CUSTOMER PART NO
			INSERT INTO CustomerPartNumber
			(
				numCompanyId, numDomainID, CustomerPartNo, numItemCode 
			)
			SELECT 
				@numCompany, @numDomainID, TEMPCPN.CustomerPartNo,TEMPCPN.numItemCode
			FROM
			(
				SELECT 
					numItemCode
					,MIN(CustomerPartNo) AS CustomerPartNo
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item',2)                                                                          
				WITH                       
				(                                                                          
					CustomerPartNo VARCHAR(300), numItemCode NUMERIC(18,0)
				)
				GROUP BY
					numItemCode
			) TEMPCPN 
			LEFT JOIN
				CustomerPartNumber CPN
			ON
				CPN.numCompanyId = @numCompany
				AND CPN.numItemCode = TEMPCPN.numItemCode
			WHERE
				CPN.CustomerPartNoID IS NULL
				AND LEN(ISNULL(TEMPCPN.CustomerPartNo,'')) > 0
				AND ISNULL(TEMPCPN.CustomerPartNo,'') <> '0'
			
			-- UPDATE ITEMS
			UPDATE 
				OpportunityItems                       
			SET 
				numItemCode=X.numItemCode,numOppId=@numOppID,numUnitHour=dbo.fn_UOMConversion(X.numUOM,X.numItemCode,@numDomainId,null) * x.numUnitHour,                      
				monPrice=x.monPrice,monTotAmount=x.monTotAmount,vcItemDesc=X.vcItemDesc,vcModelID=X.vcModelID,numWarehouseItmsID=X.numWarehouseItmsID,numToWarehouseItemID = X.numToWarehouseItemID,             
				bitDropShip=X.DropShip,bitDiscountType=X.bitDiscountType,fltDiscount=X.fltDiscount,monTotAmtBefDiscount=X.monTotAmtBefDiscount,vcItemName=X.vcItemName,
				numUOMId=numUOM,numVendorWareHouse=X.numVendorWareHouse,numShipmentMethod=X.numShipmentMethod,numSOVendorId=X.numSOVendorId,numProjectId=X.numProjectID,
				numProjectStageId=X.numProjectStageID,vcAttributes=X.Attributes,bitItemPriceApprovalRequired=X.bitItemPriceApprovalRequired,bitWorkOrder=X.bitWorkOrder,vcAttrValues=AttributeIDs,
				numPromotionID=X.numPromotionID,bitPromotionTriggered=X.bitPromotionTriggered,vcPromotionDetail=X.vcPromotionDetail,numSortOrder=X.numSortOrder,numCost=X.numCost,vcNotes=vcVendorNotes
			FROM 
			(
				SELECT 
					numoppitemtCode as numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,vcModelID,numWarehouseItmsID,numToWarehouseItemID,DropShip,bitDiscountType,
					fltDiscount,monTotAmtBefDiscount,vcItemName,numUOM,numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,Attributes,
					bitItemPriceApprovalRequired,bitWorkOrder,AttributeIDs,numPromotionID,bitPromotionTriggered,vcPromotionDetail,numSortOrder,ISNULL(numCost,0) AS numCost,ISNULL(vcVendorNotes,'') vcVendorNotes
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=2]',2)                                                                
				WITH  
				(                      
					numoppitemtCode NUMERIC(9),numItemCode NUMERIC(9),numUnitHour FLOAT,monPrice DECIMAL(30,16),monTotAmount DECIMAL(20,5),vcItemDesc VARCHAR(1000),vcModelID VARCHAR(300), numWarehouseItmsID NUMERIC(9), 
					numToWarehouseItemID NUMERIC(18,0),DropShip BIT,bitDiscountType BIT,fltDiscount DECIMAL(30,16),monTotAmtBefDiscount DECIMAL(20,5),vcItemName VARCHAR(300),numUOM NUMERIC(9),
					numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId NUMERIC(9),numProjectID NUMERIC(9),numProjectStageID NUMERIC(9),Attributes VARCHAR(500),
					bitItemPriceApprovalRequired BIT, bitWorkOrder BIT,AttributeIDs VARCHAR(500),numPromotionID NUMERIC(18,0),bitPromotionTriggered BIT,vcPromotionDetail VARCHAR(2000),numSortOrder NUMERIC(18,0),numCost DECIMAL(20,5),vcVendorNotes VARCHAR(1000)                           
				)
			)X 
			WHERE 
				numoppitemtCode=X.numOppItemID AND numOppId=@numOppID

			--INSERT NEW ADDED KIT ITEMS                 
			INSERT INTO OpportunityKitItems                                                                          
			(
				numOppId,
				numOppItemID,
				numChildItemID,
				numWareHouseItemId,
				numQtyItemsReq,
				numQtyItemsReq_Orig,
				numUOMId,
				numQtyShipped,
				monAvgCost
			)
			SELECT
				@numOppId,
				OI.numoppitemtCode,
				ID.numChildItemID,
				ID.numWareHouseItemId,
				ID.numQtyItemsReq * OI.numUnitHour,
				ID.numQtyItemsReq,
				ID.numUOMId,
				0,
				ISNULL(I.monAverageCost,0)
			FROM 
				OpportunityItems OI 
			JOIN 
				ItemDetails ID 
			ON 
				OI.numItemCode=ID.numItemKitID
			INNER JOIN
				Item I
			ON
				ID.numChildItemID = I.numItemCode
			WHERE 
				OI.numOppId=@numOppId 
				AND OI.numoppitemtCode NOT IN (SELECT ISNULL(numOppItemID,0) FROM OpportunityKitItems OKI WHERE OKI.numOppId=@numOppID)  
				
			IF (SELECT COUNT(*) FROM OPENXML (@hDocItem,'/NewDataSet/Item[bitHasKitAsChild=''true'']',2)) > 0
			BEGIN
				--INSERT KIT ITEM HAS ANOTHER KIT AS CHILD THEN SAVE CONFIGURATION OF KIT
				DELETE FROM @TempKitConfiguration

				INSERT INTO 
					@TempKitConfiguration
				SELECT
					numItemCode,
					numWarehouseItmsID,
					KitChildItems
				FROM
					OPENXML (@hDocItem,'/NewDataSet/Item[bitHasKitAsChild=''true'']',2)
				WITH                       
				(
					numItemCode NUMERIC(9),numWarehouseItmsID NUMERIC(9), KitChildItems VARCHAR(MAX)
				) 

				INSERT INTO OpportunityKitChildItems
				(
					numOppID,
					numOppItemID,
					numOppChildItemID,
					numItemID,
					numWareHouseItemId,
					numQtyItemsReq,
					numQtyItemsReq_Orig,
					numUOMId,
					numQtyShipped,
					monAvgCost
				)
				SELECT 
					@numOppID,
					OKI.numOppItemID,
					OKI.numOppChildItemID,
					ItemDetails.numChildItemID,
					ItemDetails.numWareHouseItemId,
					ItemDetails.numQtyItemsReq * OKI.numQtyItemsReq,
					ItemDetails.numQtyItemsReq,
					ItemDetails.numUOMId,
					0,
					ISNULL(Item.monAverageCost,0)
				FROM
					(
						SELECT 
							t1.numItemCode,
							t1.numWarehouseItemID,
							SUBSTRING(
										SUBSTRING(items,0,CHARINDEX('-',items)),
										0,
										CHARINDEX('#',SUBSTRING(items,0,CHARINDEX('-',items)))
									  ) AS numKitItemID,
							SUBSTRING(
										SUBSTRING(items,0,CHARINDEX('-',items)),
										CHARINDEX('#',SUBSTRING(items,0,CHARINDEX('-',items))) + 1, 
										LEN(SUBSTRING(items,0,CHARINDEX('-',items))) - CHARINDEX('#',SUBSTRING(items,0,CHARINDEX('-',items)))
									  ) AS numKitWarehouseItemID,
							SUBSTRING(
										SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)), 
										0, 
										CHARINDEX('#',SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)))
									  ) As numKitChildItemID,
							SUBSTRING(
										SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)), 
										CHARINDEX('#',SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items))) + 1, 
										LEN(SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items))) - CHARINDEX('#',SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)))
									  ) As numKitChildItemWarehouseItemID
						FROM 
							@TempKitConfiguration t1
						CROSS APPLY
						(
							SELECT
								*
							FROM 
								dbo.Split((SELECT KitChildItems FROM @TempKitConfiguration WHERE numItemCode=t1.numItemCode AND numWarehouseItemID=t1.numWarehouseItemID),',') 
						) as t2
					) TempChildItems
				INNER JOIN
					OpportunityKitItems OKI
				ON
					OKI.numOppId=@numOppId
					AND OKI.numChildItemID=TempChildItems.numKitItemID
					AND OKI.numWarehouseItemId=TempChildItems.numKitWarehouseItemID
				INNER JOIN
					OpportunityItems OI
				ON
					OI.numItemCode = TempChildItems.numItemCode
					AND OI.numWarehouseItmsID = TempChildItems.numWarehouseItemID
					AND OI.numoppitemtcode = OKI.numOppItemID
				INNER JOIN
					ItemDetails
				ON
					TempChildItems.numKitItemID = ItemDetails.numItemKitID
					AND TempChildItems.numKitChildItemID = ItemDetails.numChildItemID
					AND (TempChildItems.numKitChildItemWarehouseItemID = ItemDetails.numWareHouseItemId  OR ISNULL(TempChildItems.numKitChildItemWarehouseItemID,0) = 0)
				INNER JOIN
					Item 
				ON
					ItemDetails.numChildItemID = Item.numItemCode
			END                                  
			
			--UPDATE KIT ITEMS                 
			UPDATE 
				OKI 
			SET 
				numQtyItemsReq = numQtyItemsReq_Orig * OI.numUnitHour
			FROM 
				OpportunityKitItems OKI 
			JOIN 
				OpportunityItems OI 
			ON 
				OKI.numOppItemID=OI.numoppitemtCode 
				AND OKI.numOppId=OI.numOppId  
			WHERE 
				OI.numOppId=@numOppId 

			-- UPDATE CHILD ITEMS OF KIT WITHIN KIT
			UPDATE
				OKCI
			SET
				OKCI.numQtyItemsReq = (OKI.numQtyItemsReq * OKCI.numQtyItemsReq_Orig)
			FROM
				OpportunityKitChildItems OKCI
			INNER JOIN
				OpportunityKitItems OKI
			ON
				OKCI.numOppID = @numOppId
				AND OKCI.numOppItemID = OKI.numOppItemID
				AND OKCI.numOppChildItemID = OKI.numOppChildItemID                     
			                                    
			IF NOT (@tintCommitAllocation=2 AND @bitAllocateInventoryOnPickList=1)
			BEGIN
				IF @tintCommitAllocation = 2
				BEGIN
					DELETE FROM WorkOrderDetails WHERE numWOId IN (SELECT numWOId FROM WorkOrder WHERE numOppId=@numOppID AND numWOStatus <> 23184)
					DELETE FROM WorkOrder WHERE numOppId=@numOppID AND numWOStatus <> 23184
				END

				EXEC USP_ManageSOWorkOrder @numDomainId,@numOppID,@strItems,@numUserCntID
			END
			   		                                                                      
			EXEC sp_xml_removedocument @hDocItem                                               
		END     

		IF @tintOppType=1 AND @tintOppStatus=1 --Sales Order
		BEGIN
			IF EXISTS (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID AND ISNULL(numWarehouseItmsID,0) > 0 AND ISNULL(bitDropShip,0) = 0 AND (ISNULL(numUnitHour,0) - ISNULL(numQtyShipped,0)) < 0)
			BEGIN
				RAISERROR('EDITED_ITEMS_QTY_IS_LESS_THEN_SHIPPED_QTY',16,1)
				RETURN
			END

			IF EXISTS (SELECT numoppitemtCode FROM OpportunityItems INNER JOIN WorkOrder ON OpportunityItems.numOppID=WorkOrder.numOppID AND OpportunityItems.numoppitemtCode=WorkOrder.numOppItemID AND ISNULL(WorkOrder.numParentWOID,0)=0 AND WorkOrder.numWOStatus=23184 AND ISNULL(OpportunityItems.numUnitHour,0) > ISNULL(WorkOrder.numQtyItemsReq,0) WHERE OpportunityItems.numOppID=@numOppID)
			BEGIN
				RAISERROR('EDITED_ITEMS_QTY_CAN_NOT_BE_GREATER_THEN_COMPLETED_WORK_ORDER',16,1)
				RETURN
			END
		END
		ELSE IF @tintOppType=2 AND @tintOppStatus=1 --Purchase Order
		BEGIN
			IF EXISTS (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID AND ISNULL(numWarehouseItmsID,0) > 0 AND ISNULL(bitDropShip,0) = 0 AND (ISNULL(numUnitHour,0) - ISNULL(numUnitHourReceived,0)) < 0)
			BEGIN
				RAISERROR('EDITED_ITEMS_QTY_IS_LESS_THEN_RECEIVED_QTY',16,1)
				RETURN
			END
		END

		--FOLLOWING DELETE STATEMENT IS COMMENTED BECUASE NOW WE ARE NOT ALLOWING USER TO EIDT SERIAL/LOT ITEM UNTILL USER REMOVED ASSIGNED SERIAL/LOT NUMBERS
	    --Delete previously added serial lot number added for opportunity
		--DELETE FROM OppWarehouseSerializedItem WHERE numOppID = @numOppID                                                         
	END

	IF(SELECT 
			COUNT(*) 
		FROM 
			OpportunityBizDocItems OBDI 
		INNER JOIN 
			OpportunityBizDocs OBD 
		ON 
			OBDI.numOppBizDocID=OBD.numOppBizDocsID 
		WHERE 
			OBD.numOppId=@numOppID AND OBDI.numOppItemID NOT IN (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID)) > 0
	BEGIN
		RAISERROR('ITEM_USED_IN_BIZDOC',16,1)
		RETURN
	END

	IF @tintOppType=1 AND @tintCommitAllocation=2 AND @bitAllocateInventoryOnPickList=1
	BEGIN
		IF(SELECT 
				COUNT(*)
			FROM
			(
				SELECT
					numOppItemID
					,SUM(numUnitHour) PackingSlipUnits
				FROM 
					OpportunityBizDocItems OBDI 
				INNER JOIN 
					OpportunityBizDocs OBD 
				ON 
					OBDI.numOppBizDocID=OBD.numOppBizDocsID 
				WHERE 
					OBD.numOppId=@numOppID
					AND OBD.numBizDocId=29397 --Picking Slip
				GROUP BY
					numOppItemID
			) AS TEMP
			INNER JOIN
				OpportunityItems OI
			ON
				TEMP.numOppItemID = OI.numoppitemtCode
			WHERE
				OI.numUnitHour < PackingSlipUnits
			) > 0
		BEGIN
			RAISERROR('ORDERED_QUANTITY_CAN_NOT_BE_LESS_THEN_PACKING_SLIP_QTY',16,1)
			RETURN
		END
	END
	
	-- Set item release date to today
	If @tintOppType = 1
	BEGIN
		INSERT INTO OpportunityItemsReleaseDates
		(
			numDomainID
			,numOppID
			,numOppItemID
			,dtReleaseDate
		)
		SELECT
			@numDomainId
			,OI.numOppId
			,OI.numoppitemtCode
			,ISNULL(OI.ItemReleaseDate,@dtItemRelease)
		FROM
			OpportunityItems OI
		LEFT JOIN
			OpportunityItemsReleaseDates OIRD
		ON
			OI.numOppId=OIRD.numOppID
			AND OI.numoppitemtCode = OIRD.numOppItemID
		WHERE
			OI.numOppId=@numOppID
			AND OIRD.numID IS NULL
	END



	SET @TotalAmount=0                                                           
	SELECT @TotalAmount = SUM(monTotAmount) FROM OpportunityItems WHERE numOppId=@numOppID                                                                          
  
	UPDATE OpportunityMaster SET monPamount=@TotalAmount WHERE numOppId=@numOppID

/*Added by chintan - While creating SO from PO it wasn't puting Item Qty to Allocation. this segment was being called from USP_OppSaveMilestone which No longer in use after changes anoop did. */     
--Updating the warehouse items              
            
			
			DECLARE @bitViolatePrice BIT
			DECLARE @intPendingApprovePending INT

			SET @bitIsInitalSalesOrder=(SELECT bitIsInitalSalesOrder FROM OpportunityMaster WHERE numOppId=@numOppID AND numDomainId=@numDomainID)
			--SET @bitViolatePrice=(SELECT bitMarginPriceViolated FROM Domain WHERE numDomainId=@numDomainID)
			IF EXISTS (SELECT numApprovalRuleID FROM ApprovalProcessItemsClassification WHERE numDomainId=@numDomainID AND ISNULL(bitMarginPriceViolated,0) = 1)
			BEGIN
				SET @bitViolatePrice = 1
			END
			
			IF(@bitViolatePrice=1 AND @bitIsInitalSalesOrder=1 AND @tintClickBtn = 1)
			BEGIN
				UPDATE OpportunityMaster SET tintOppStatus=@DealStatus WHERE numOppId=@numOppID
			END
			ELSE
			BEGIN
				UPDATE OpportunityMaster SET tintOppStatus = @DealStatus WHERE numOppId=@numOppID
				DECLARE @tintShipped AS TINYINT               
				SELECT @tintOppStatus=tintOppStatus,@tintOppType=tintOppType,@tintShipped=tintShipped from OpportunityMaster where numOppID=@numOppID               
				
				IF @tintOppStatus=1 AND @tintCommitAllocation=1             
				BEGIN         
					EXEC USP_UpdatingInventoryonCloseDeal @numOppID,@numUserCntID
				END  
				declare @tintCRMType as numeric(9)        
				declare @AccountClosingDate as datetime        

				select @numDivisionID=numDivisionID,@AccountClosingDate=bintAccountClosingDate from OpportunityMaster where numOppID=@numOppID         
				select @tintCRMType=tintCRMType from divisionmaster where numDivisionID =@numDivisionID 

				--When deal is won                                         
				IF @DealStatus = 1 
				BEGIN
					IF @AccountClosingDate IS NULL                   
					BEGIN
						UPDATE OpportunityMaster SET bintAccountClosingDate=GETUTCDATE() WHERE numOppID=@numOppID              
					END

					-- Promote Lead to Account when Sales/Purchase Order is created against it
					IF @tintCRMType=0 --Lead & Order
					BEGIN
						UPDATE 
							divisionmaster 
						SET 
							tintCRMType=2
							,bintLeadProm=getutcdate()
							,bintLeadPromBy=@numUserCntID
							,bintProsProm=getutcdate()
							,bintProsPromBy=@numUserCntID        
						WHERE 
							numDivisionID=@numDivisionID        
					END
					--Promote Prospect to Account
					ELSE IF @tintCRMType=1 
					BEGIN
						UPDATE 
							divisionmaster 
						SET 
							tintCRMType=2
							,bintProsProm=getutcdate()
							,bintProsPromBy=@numUserCntID        
						WHERE 
							numDivisionID=@numDivisionID        
					END
				END         
				--When Deal is Lost
				ELSE IF @DealStatus = 2
				BEGIN
					SELECT @AccountClosingDate=bintAccountClosingDate FROM OpportunityMaster WHERE numOppID=@numOppID         
					
					IF @AccountClosingDate IS NULL                   
					BEGIN
						UPDATE OpportunityMaster SET bintAccountClosingDate=GETUTCDATE() WHERE numOppID=@numOppID
					END
				END 
			END

			/*Added by chintan -  Biz usually automatically promotes the record from Lead/Prospect to Account When a Sales Opp/Order is made against it*/  
			-- Promote Lead to Prospect when Sales/Purchase Opp is created against it
			if @tintCRMType=0 AND @DealStatus = 0 --Lead & Open Opp
			begin        
				update divisionmaster set tintCRMType=1,bintLeadProm=getutcdate(),bintLeadPromBy=@numUserCntID,bintProsProm=getutcdate(),bintProsPromBy=@numUserCntID        
				where numDivisionID=@numDivisionID        
			end        

--Add/Update Address Details
DECLARE @vcStreet  varchar(100),@vcCity  varchar(50),@vcPostalCode  varchar(15),@vcCompanyName  varchar(100),@vcAddressName VARCHAR(50), @vcAltContact VARCHAR(200)      
DECLARE @numState  numeric(9),@numCountry  numeric(9), @numCompanyId  numeric(9), @numContact NUMERIC(18,0)
DECLARE @bitIsPrimary BIT, @bitAltContact BIT;
DECLARE @tintAddressOf TINYINT

select  @vcCompanyName=vcCompanyName,@numCompanyId=div.numCompanyID from CompanyInfo Com                            
join divisionMaster Div                            
on div.numCompanyID=com.numCompanyID                            
where div.numdivisionID=@numDivisionId

--Bill Address
IF @numBillAddressId>0
BEGIN
	SELECT 
		@vcStreet=ISNULL(vcStreet,'')
		,@vcCity=ISNULL(vcCity,'')
		,@vcPostalCode=ISNULL(vcPostalCode,'')
		,@numState=ISNULL(numState,0)
		,@numCountry=ISNULL(numCountry,0)
		,@bitIsPrimary=bitIsPrimary
		,@vcAddressName=vcAddressName
		,@tintAddressOf=ISNULL(tintAddressOf,0)
		,@numContact=numContact
		,@bitAltContact=bitAltContact
		,@vcAltContact=vcAltContact
    FROM 
		dbo.AddressDetails 
	WHERE 
		numDomainID = @numDomainID 
		AND numAddressID = @numBillAddressId

	IF @tintAddressOf = 2 AND @tintOppType = 2
	BEGIN
		SELECT
			@vcCompanyName = ISNULL(vcCompanyName,'')
			,@numCompanyId = ISNULL(CompanyInfo.numCompanyId,0)
		FROM
			AddressDetails
		INNER JOIN
			DivisionMaster 
		ON
			AddressDetails.numRecordID = DivisionMaster.numDivisionID
		INNER JOIN
			CompanyInfo
		ON
			DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
		WHERE 
			AddressDetails.numDomainID = @numDomainID 
			AND numAddressID = @numBillAddressId
	END

  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 0, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = @vcCompanyName, --  varchar(100)
	@numCompanyId = 0, --  numeric(9, 0)
	@vcAddressName = @vcAddressName,
	@bitCalledFromProcedure = 1,
	@numContact = @numContact,
	@bitAltContact = @bitAltContact,
	@vcAltContact = @vcAltContact
END
  
  
--Ship Address
IF @intUsedShippingCompany = 92 -- WILL CALL (PICK UP FROM Warehouse)
BEGIN
	IF EXISTS (SELECT numWareHouseID FROM Warehouses WHERE Warehouses.numDomainID = @numDomainID AND numWareHouseID = @numWillCallWarehouseID AND ISNULL(numAddressID,0) > 0)
	BEGIN
		SELECT  
			@vcStreet=ISNULL(vcStreet,'')
			,@vcCity=ISNULL(vcCity,'')
			,@vcPostalCode=ISNULL(AddressDetails.vcPostalCode,'')
			,@numState=ISNULL(numState,0)
			,@numCountry=ISNULL(numCountry,0)
			,@vcAddressName=vcAddressName
		FROM 
			dbo.Warehouses 
		INNER JOIN
			AddressDetails
		ON
			Warehouses.numAddressID = AddressDetails.numAddressID
		WHERE 
			Warehouses.numDomainID = @numDomainID 
			AND numWareHouseID = @numWillCallWarehouseID
	END
	ELSE
	BEGIN
	SELECT  
		@vcStreet=ISNULL(vcWStreet,'')
		,@vcCity=ISNULL(vcWCity,'')
		,@vcPostalCode=ISNULL(vcWPinCode,'')
		,@numState=ISNULL(numWState,0)
		,@numCountry=ISNULL(numWCountry,0)
		,@vcAddressName=''
	FROM 
		dbo.Warehouses 
	WHERE 
		numDomainID = @numDomainID 
		AND numWareHouseID = @numWillCallWarehouseID
	END

	UPDATE OpportunityMaster SET tintShipToType=2 WHERE numOppId=@numOppID
 
  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 1, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = '', --  varchar(100)
	@numCompanyId =0, --  numeric(9, 0)
	@vcAddressName = @vcAddressName,
	@bitCalledFromProcedure = 1,
	@numContact = 0,
	@bitAltContact = 0,
	@vcAltContact = ''
END
ELSE IF @numShipAddressId>0
BEGIN
	SELECT
		@vcStreet=ISNULL(vcStreet,'')
		,@vcCity=ISNULL(vcCity,'')
		,@vcPostalCode=ISNULL(vcPostalCode,'')
		,@numState=ISNULL(numState,0)
		,@numCountry=ISNULL(numCountry,0)
		,@bitIsPrimary=bitIsPrimary
		,@vcAddressName=vcAddressName
		,@tintAddressOf=ISNULL(tintAddressOf,0)
		,@numContact=numContact
		,@bitAltContact=bitAltContact
		,@vcAltContact=vcAltContact
    FROM  
		dbo.AddressDetails 
	WHERE 
		numDomainID = @numDomainID 
		AND numAddressID = @numShipAddressId
 

	IF @tintAddressOf = 2 AND @tintOppType = 2
	BEGIN
		SELECT
			@vcCompanyName = ISNULL(vcCompanyName,'')
			,@numCompanyId = ISNULL(CompanyInfo.numCompanyId,0)
		FROM
			AddressDetails
		INNER JOIN
			DivisionMaster 
		ON
			AddressDetails.numRecordID = DivisionMaster.numDivisionID
		INNER JOIN
			CompanyInfo
		ON
			DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
		WHERE 
			AddressDetails.numDomainID = @numDomainID 
			AND numAddressID = @numShipAddressId
	END


  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 1, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = @vcCompanyName, --  varchar(100)
	@numCompanyId =@numCompanyId, --  numeric(9, 0)
	@vcAddressName = @vcAddressName,
	@bitCalledFromProcedure = 1,
	@numContact = @numContact,
	@bitAltContact = @bitAltContact,
	@vcAltContact = @vcAltContact
END

-- IMPORTANT:KEEP THIS BELOW ADDRESS UPDATE LOGIC
-- GET TAX APPLICABLE TO DIVISION WHEN ORDER IS CREATED AFTER THAT NOT NEED TO CHANGE IT
IF @bitNewOrder = 1
BEGIN
	--INSERT TAX FOR DIVISION   
	IF @tintOppType=1 OR (@tintOppType=2 and (select isnull(bitPurchaseTaxCredit,0) from Domain where numDomainId=@numDomainId)=1)
	BEGIN
		INSERT INTO dbo.OpportunityMasterTaxItems 
		(
			numOppId,
			numTaxItemID,
			fltPercentage,
			tintTaxType,
			numTaxID
		) 
		SELECT 
			@numOppID,
			TI.numTaxItemID,
			TEMPTax.decTaxValue,
			TEMPTax.tintTaxType,
			0
		FROM 
			TaxItems TI 
		JOIN 
			DivisionTaxTypes DTT 
		ON 
			TI.numTaxItemID = DTT.numTaxItemID
		CROSS APPLY
		(
			SELECT
				decTaxValue,
				tintTaxType
			FROM
				dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,TI.numTaxItemID,@numOppId,0,NULL)
		) AS TEMPTax
		WHERE 
			DTT.numDivisionID=@numDivisionId 
			AND DTT.bitApplicable=1
		UNION 
		SELECT 
			@numOppID,
			0,
			TEMPTax.decTaxValue,
			TEMPTax.tintTaxType,
			0
		FROM 
			dbo.DivisionMaster 
		CROSS APPLY
		(
			SELECT
				decTaxValue,
				tintTaxType
			FROM
				dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,0,@numOppId,1,NULL)
		) AS TEMPTax
		WHERE 
			bitNoTax=0 
			AND numDivisionID=@numDivisionID	
		UNION 
		SELECT
			@numOppId
			,1
			,decTaxValue
			,tintTaxType
			,numTaxID
		FROM
			dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,1,@numOppId,1,NULL)		
	END	
END

--INSERT TAX FOR DIVISION   
IF @tintOppType=1 OR (@tintOppType=2 and (select isnull(bitPurchaseTaxCredit,0) from Domain where numDomainId=@numDomainId)=1)
BEGIN
--Delete Tax for Opportunity Items if item deleted 
DELETE FROM OpportunityItemsTaxItems WHERE numOppId=@numOppId AND numOppItemID NOT IN (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID)

--Insert Tax for Opportunity Items
INSERT INTO dbo.OpportunityItemsTaxItems 
(
	numOppId,
	numOppItemID,
	numTaxItemID,
	numTaxID
) 
SELECT 
	@numOppId,
	OI.numoppitemtCode,
	TI.numTaxItemID,
	0
FROM 
	dbo.OpportunityItems OI 
JOIN 
	dbo.ItemTax IT 
ON 
	OI.numItemCode=IT.numItemCode 
JOIN
	TaxItems TI 
ON 
	TI.numTaxItemID = IT.numTaxItemID 
WHERE 
	OI.numOppId=@numOppID 
	AND IT.bitApplicable=1 
	AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
UNION
SELECT 
	@numOppId,
	OI.numoppitemtCode,
	0,
	0
FROM 
	dbo.OpportunityItems OI 
JOIN 
	dbo.Item I 
ON 
	OI.numItemCode=I.numItemCode
WHERE 
	OI.numOppId=@numOppID 
	AND I.bitTaxable=1 
	AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
UNION
SELECT
	@numOppId,
	OI.numoppitemtCode,
	1,
	TD.numTaxID
FROM
	dbo.OpportunityItems OI 
INNER JOIN
	ItemTax IT
ON
	IT.numItemCode = OI.numItemCode
INNER JOIN
	TaxDetails TD
ON
	TD.numTaxID = IT.numTaxID
	AND TD.numDomainId = @numDomainId
WHERE
	OI.numOppId = @numOppID
	AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)

END
  
 UPDATE OpportunityMaster SET monDealAmount=dbo.GetDealAmount(@numOppID,getdate(),0) WHERE numOppId=@numOppID

	IF @tintOppType=1 AND @tintOppStatus=1 
	BEGIN
		EXEC USP_SalesFulfillmentQueue_Save @numDomainID,@numUserCntID,@numOppID,@numStatus
	END
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SET @numOppID = 0

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Opportunity_BulkSales')
DROP PROCEDURE USP_Opportunity_BulkSales
GO
CREATE PROCEDURE [dbo].[USP_Opportunity_BulkSales]  
  
AS  
BEGIN
	DECLARE @TEMP TABLE
	(
		ID INT,
		numMSOQID NUMERIC(18,0),
		numDivisionID NUMERIC(18,0),
		numContactID NUMERIC(18,0)
	)

	DECLARE @TEMPMasterOpp TABLE
	(
		ID INT IDENTITY(1,1),
		numOppID NUMERIC(18,0)
	)

	DECLARE @TEMPOppItems TABLE
	(
		ID INT
		,numoppitemtCode NUMERIC(18,0)
		,numItemCode NUMERIC(18,0)
		,numUnitHour FLOAT
		,monPrice DECIMAL(30,16)
		,monTotAmount DECIMAL(20,5)
		,vcItemDesc VARCHAR(1000)
		,numWarehouseItmsID NUMERIC(18,0)
		,ItemType VARCHAR(30)
		,DropShip BIT
		,bitDiscountType BIT
		,fltDiscount DECIMAL(30,16)
		,monTotAmtBefDiscount DECIMAL(20,5)
		,vcItemName VARCHAR(300)
		,numUOM NUMERIC(18,0)
		,bitWorkOrder BIT
		,numVendorWareHouse NUMERIC(18,0)
		,numShipmentMethod NUMERIC(18,0)
		,numSOVendorId NUMERIC(18,0)
		,numProjectID numeric(18,0)
		,numProjectStageID numeric(18,0)
		,numToWarehouseItemID NUMERIC(18,0)
		,Attributes varchar(500)
		,AttributeIDs VARCHAR(500)
		,vcSKU VARCHAR(100)
		,bitItemPriceApprovalRequired BIT
		,numPromotionID NUMERIC(18,0)
		,bitPromotionTriggered BIT
		,vcPromotionDetail VARCHAR(2000)
		,numSortOrder NUMERIC(18,0)
	)


	-- FIRST GET UNIQUE OPPID THAT NEEDS TO BE RE CREATED WHICH ARE NOT EXECUTED
	INSERT INTO 
		@TEMPMasterOpp
	SELECT
		DISTINCT numOppId
	FROM
		dbo.MassSalesOrderQueue
	WHERE
		ISNULL(bitExecuted,0) = 0
	
	DECLARE @i AS INT = 1
	DECLARE @iCount AS INT
	SELECT @iCount = COUNT(*) FROM @TEMPMasterOpp

	DECLARE @tempOppID AS INT
	DECLARE @numNewOppID AS NUMERIC(18,0)
	DECLARE @numDomainID AS NUMERIC(18,0)
	DECLARE @numUserCntID AS NUMERIC(18,0)
	DECLARE @tintSource AS NUMERIC(9)
	DECLARE @txtComments AS VARCHAR(1000)
	DECLARE @bitPublicFlag AS BIT
	DECLARE @monPAmount AS DECIMAL(20,5)
	DECLARE @numAssignedTo AS NUMERIC(18,0)                                                                                                                                                                              
	DECLARE @strItems AS VARCHAR(MAX)
	DECLARE @dtEstimatedCloseDate DATETIME                                                            
	DECLARE @lngPConclAnalysis AS NUMERIC(18,0)
	DECLARE @numCurrencyID AS NUMERIC(18,0)
	DECLARE @numOrderStatus NUMERIC(18,0)

	DECLARE @bitBillingTerms AS BIT
	DECLARE @intBillingDays as integer
	DECLARE @bitInterestType as bit
	DECLARE @fltInterest as float
	DECLARE @intShippingCompany AS NUMERIC(18,0)
	DECLARE @numDefaultShippingServiceID AS NUMERIC(18,0)
	DECLARE @vcCouponCode VARCHAR(100)
	DECLARE @numShipFromWarehouse NUMERIC(18,0)
	DECLARE @numWillCallWarehouseID NUMERIC(18,0)
	DECLARE @dtReleaseDate DATE

	WHILE @i <= @iCount
	BEGIN
		SELECT @tempOppID=numOppID FROM @TEMPMasterOpp WHERE ID = @i

		IF EXISTS (SELECT * FROM OpportunityMaster WHERE numOppId=@tempOppID)
		BEGIN

			SELECT 
				@numDomainID=numDomainId
				,@numUserCntID=numCreatedBy
				,@tintSource=tintSource
				,@txtComments=txtComments
				,@bitPublicFlag=bitPublicFlag
				,@dtEstimatedCloseDate=GETUTCDATE()
				,@lngPConclAnalysis=lngPConclAnalysis
				,@numCurrencyID=numCurrencyID
				,@vcCouponCode=vcCouponCode
				,@numAssignedTo=numAssignedTo
				,@numOrderStatus=numStatus
				,@numShipFromWarehouse=ISNULL(numShipFromWarehouse,0)
			FROM
				OpportunityMaster
			WHERE
				numOppId=@tempOppID

			IF ISNULL(@numShipFromWarehouse,0) > 0
			BEGIN
				SET @numWillCallWarehouseID = @numShipFromWarehouse
			END
			ELSE
			BEGIN
				SET @numWillCallWarehouseID = ISNULL((SELECT TOP 1 numWarehouseItmsID FROM OpportunityItems WHERE numOppId=@tempOppID AND ISNULL(numWarehouseItmsID,0) > 0),0)
			END


			-- DELETE ENTRIES OF PREVIOUS LOOP
			DELETE FROM @TEMPOppItems

			INSERT INTO 
				@TEMPOppItems
			SELECT
				ROW_NUMBER() OVER(ORDER BY numoppitemtCode)
				,numoppitemtCode
				,numItemCode
				,numUnitHour
				,monPrice
				,monTotAmount
				,vcItemDesc
				,numWarehouseItmsID
				,vcType
				,bitDropShip
				,bitDiscountType
				,fltDiscount
				,monTotAmtBefDiscount
				,vcItemName
				,numUOMId
				,bitWorkOrder
				,numVendorWareHouse
				,numShipmentMethod
				,numSOVendorId
				,0 AS numProjectID
				,0 AS numProjectStageID
				,numToWarehouseItemID
				,vcAttributes AS Attributes
				,vcAttrValues AS AttributeIDs
				,vcSKU
				,bitItemPriceApprovalRequired
				,numPromotionID
				,bitPromotionTriggered
				,vcPromotionDetail
				,numSortOrder
			FROM 
				OpportunityItems
			WHERE
				numOppId = @tempOppID

			DECLARE @k AS INT = 1
			DECLARE @numTempOppItemID NUMERIC(18,0)
			DECLARE @kCount AS INT
			SELECT @kCount=COUNT(*) FROM @TEMPOppItems
	
			--TODO CREATE ITEMS
			SET @strItems = '<?xml version="1.0" encoding="iso-8859-1" ?><NewDataSet>'

			WHILE @k <= @kCount
			BEGIN
				SELECT 
					@numTempOppItemID = numoppitemtCode
					,@strItems = CONCAT(@strItems
										,'<Item><Op_Flag>1</Op_Flag>'
										,'<numoppitemtCode>',ID,'</numoppitemtCode>'
										,'<numItemCode>',numItemCode,'</numItemCode>'
										,'<numUnitHour>',numUnitHour,'</numUnitHour>'
										,'<monPrice>',monPrice,'</monPrice>'
										,'<monTotAmount>',monTotAmount,'</monTotAmount>'
										,'<vcItemDesc>',vcItemDesc,'</vcItemDesc>'
										,'<numWarehouseItmsID>',ISNULL(numWarehouseItmsID,0),'</numWarehouseItmsID>'
										,'<ItemType>',ItemType,'</ItemType>'
										,'<DropShip>',ISNULL(DropShip,0),'</DropShip>'
										,'<bitDiscountType>',ISNULL(bitDiscountType,0),'</bitDiscountType>'
										,'<fltDiscount>',ISNULL(fltDiscount,0),'</fltDiscount>'
										,'<monTotAmtBefDiscount>',monTotAmtBefDiscount,'</monTotAmtBefDiscount>'
										,'<vcItemName>',vcItemName,'</vcItemName>'
										,'<numUOM>',ISNULL(numUOM,0),'</numUOM>'
										,'<bitWorkOrder>',ISNULL(bitWorkOrder,0),'</bitWorkOrder>'
										,'<numVendorWareHouse>',ISNULL(numVendorWareHouse,0),'</numVendorWareHouse>'
										,'<numShipmentMethod>',ISNULL(numShipmentMethod,0),'</numShipmentMethod>'
										,'<numSOVendorId>',ISNULL(numSOVendorId,0),'</numSOVendorId>'
										,'<numProjectID>',ISNULL(numProjectID,0),'</numProjectID>'
										,'<numProjectStageID>',ISNULL(numProjectStageID,0),'</numProjectStageID>'
										,'<numToWarehouseItemID>',ISNULL(numToWarehouseItemID,0),'</numToWarehouseItemID>'
										,'<Attributes>',Attributes,'</Attributes>'
										,'<AttributeIDs>',AttributeIDs,'</AttributeIDs>'
										,'<vcSKU>',vcSKU,'</vcSKU>'
										,'<bitItemPriceApprovalRequired>',ISNULL(bitItemPriceApprovalRequired,0),'</bitItemPriceApprovalRequired>'
										,'<numPromotionID>',ISNULL(numPromotionID,0),'</numPromotionID>'
										,'<bitPromotionTriggered>',ISNULL(bitPromotionTriggered,0),'</bitPromotionTriggered>'
										,'<vcPromotionDetail>',vcPromotionDetail,'</vcPromotionDetail>'
										,'<numSortOrder>',ISNULL(numSortOrder,0),'</numSortOrder>')
									
				FROM 
					@TEMPOppItems 
				WHERE 
					ID=@k

			
				IF (SELECT COUNT(*) FROM OpportunityKitChildItems WHERE numOppID = @tempOppID AND numOppItemID=@numTempOppItemID) > 0
				BEGIN
					SET @strItems = CONCAT(@strItems,'<bitHasKitAsChild>true</bitHasKitAsChild>')
					SET @strItems = CONCAT(@strItems,'<KitChildItems>',
											STUFF((SELECT 
											',' + CONCAT(OKI.numChildItemID,'#',OKI.numWareHouseItemId,'-',OKCI.numItemID,'#',OKCI.numWareHouseItemId)
											FROM
												OpportunityKitChildItems OKCI
											INNER JOIN
												OpportunityKitItems OKI
											ON
												OKCI.numOppChildItemID = OKI.numOppChildItemID
											WHERE
												OKCI.numOppID=@tempOppID
												AND OKCI.numOppItemID=@numTempOppItemID
																		 FOR XML PATH('')), 
																		1, 1, ''),'</KitChildItems>')
				END
				ELSE
				BEGIN
					SET @strItems = CONCAT(@strItems,'<bitHasKitAsChild>false</bitHasKitAsChild>')
					SET @strItems = CONCAT(@strItems,'<KitChildItems />')
				END
			

				SET @strItems = CONCAT(@strItems,'</Item>')

				SET @k = @k + 1
			END

			SET @strItems = CONCAT(@strItems,'</NewDataSet>')
		
			-- DELETE ENTRIES OF PREVIOUS LOOP
			DELETE FROM @TEMP

			-- GET ALL ORGANIZATION FOR WHICH ORDER NEEDS TO BE CREATED
			INSERT INTO @TEMP
			(
				ID,
				numMSOQID,
				numDivisionID,
				numContactID
			)
			SELECT 
				ROW_NUMBER() OVER(ORDER BY numMSOQID),
				numMSOQID,
				numDivisionId,
				numContactId
			FROM
				MassSalesOrderQueue
			WHERE
				numOppId = @tempOppID
				AND ISNULL(bitExecuted,0) = 0

			DECLARE @j AS INT = 1
			DECLARE @jCount AS INT
			DECLARE @numTempMSOQID NUMERIC(18,0)
			DECLARE @numTempDivisionID NUMERIC(18,0)
			DECLARE @numTempContactID NUMERIC(18,0)
			SELECT @jCount = COUNT(*) FROM @TEMP

			WHILE @j <= @jCount
			BEGIN
			
				SELECT 
					@numTempMSOQID=numMSOQID
					,@numTempDivisionID=numDivisionID
					,@numTempContactID=numContactID
				FROM 
					@TEMP 
				WHERE 
					ID = @j

				SELECT 
					@bitBillingTerms=(CASE WHEN ISNULL(tintBillingTerms,0) = 1 THEN 1 ELSE 0 END) 
					,@intBillingDays = ISNULL(numBillingDays,0)
					,@bitInterestType= (CASE WHEN ISNULL(tintInterestType,0) = 1 THEN 1 ELSE 0 END)
					,@fltInterest=ISNULL(fltInterest ,0)
					,@dtReleaseDate=CAST(GETUTCDATE() AS DATE)
					,@numAssignedTo=ISNULL(numAssignedTo,0)
					,@intShippingCompany=ISNULL(intShippingCompany,0)
					,@numDefaultShippingServiceID=ISNULL(numDefaultShippingServiceID,0)
				FROM 
					DivisionMaster
				WHERE 
					numDomainID=@numDomainID AND numDivisionID=@numTempDivisionID

				BEGIN TRY
					SET @numNewOppID = 0

					EXEC USP_OppManage 
					@numNewOppID OUTPUT
					,@numTempContactID
					,@numTempDivisionID
					,@tintSource
					,''
					,@txtComments
					,@bitPublicFlag
					,@numUserCntID
					,0
					,@numAssignedTo
					,@numDomainID
					,@strItems
					,''
					,@dtEstimatedCloseDate
					,0
					,@lngPConclAnalysis
					,1
					,0
					,0
					,0
					,@numCurrencyID
					,1
					,@numOrderStatus
					,NULL
					,0
					,0
					,0
					,0
					,1
					,0
					,0
					,@bitBillingTerms
					,@intBillingDays
					,@bitInterestType
					,@fltInterest
					,0
					,0
					,NULL
					,@vcCouponCode
					,NULL
					,NULL
					,NULL
					,NULL
					,0
					,0
					,0
					,@intShippingCompany
					,0
					,@dtReleaseDate
					,0
					,0
					,0
					,0
					,@numWillCallWarehouseID
					,0
					,@numShipFromWarehouse
					,@numDefaultShippingServiceID
					,0

					-- UPDATE STATUS TO EXECUTED
					UPDATE 
						MassSalesOrderQueue
					SET
						bitExecuted = 1
					WHERE
						numMSOQID = @numTempMSOQID


					-- MAKE ENTRY INTO MASS SALES ORDER LOG
					INSERT INTO MassSalesOrderLog
					(
						numMSOQID
						,bitSuccess
						,numCreatedOrderID
					)
					VALUES
					(
						@numTempMSOQID
						,1
						,@numNewOppID
					)


					INSERT INTO CFW_Fld_Values_Opp
					(
						Fld_ID
						,Fld_Value
						,RecId
						,bintModifiedDate
						,numModifiedBy
					)
					SELECT
						Fld_ID
						,Fld_Value
						,@numNewOppID
						,GETUTCDATE()
						,numModifiedBy
					FROM
						CFW_Fld_Values_Opp
					WHERE
						RecId=@tempOppID
				END TRY
				BEGIN CATCH
					SELECT ERROR_MESSAGE(),ERROR_LINE()
						
					-- UPDATE STATUS TO EXECUTED
					UPDATE 
						MassSalesOrderQueue
					SET
						bitExecuted = 1
					WHERE
						numMSOQID = @numTempMSOQID


					-- MAKE ENTRY INTO MASS SALES ORDER LOG
					INSERT INTO MassSalesOrderLog
					(
						numMSOQID
						,bitSuccess
						,vcError
					)
					VALUES
					(
						@numTempMSOQID
						,0
						,ERROR_MESSAGE()
					)
				END CATCH

				SET @j = @j + 1
			END
		END

		SET @i = @i + 1
	END
END
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityBizDocItems_ChangePackingSlipQty')
DROP PROCEDURE USP_OpportunityBizDocItems_ChangePackingSlipQty
GO
CREATE PROCEDURE [dbo].[USP_OpportunityBizDocItems_ChangePackingSlipQty]                        
(              
	@numDomainID NUMERIC(18,0)                                
	,@numUserCntID NUMERIC(18,0)
	,@numOppId NUMERIC(18,0)
	,@numOppBizDocsId NUMERIC(18,0)
)
AS 
BEGIN
	-- CODE LEVEL TRANSACTION
	IF EXISTS (SELECT numOppBizDocsId FROM OpportunityBizDocs WHERE numOppId=@numOppId AND numBizDocId=296 AND numSourceBizDocId=@numOppBizDocsId)
	BEGIN
		IF EXISTS (SELECT 
					OBDI.numOppItemID
				FROM 
					OpportunityBizDocs OBD
				INNER JOIN
					OpportunityBizDocItems OBDI
				ON
					OBDI.numOppBizDocID=OBD.numOppBizDocsId
				INNER JOIN 
					OpportunityBizDocs OBDFulfillment 
				ON  
					OBDFulfillment.numSourceBizDocId = OBD.numOppBizDocsId
				INNER JOIN 
					OpportunityBizDocItems OBDIFulfillment 
				ON  
					OBDIFulfillment.numOppBizDocID = OBDFulfillment.numOppBizDocsId
					AND OBDIFulfillment.numOppItemID = OBDI.numOppItemID
				WHERE 
					OBD.numOppId=@numOppId
					AND OBD.numOppBizDocsId=@numOppBizDocsId
				GROUP BY
					OBDI.numOppItemID
					,OBDI.numUnitHour
				HAVING
					OBDI.numUnitHour > ISNULL(SUM(OBDIFulfillment.numUnitHour),0))
		BEGIN
			DECLARE @tintCommitAllocation AS TINYINT
			DECLARE @bitAllocateInventoryOnPickList AS BIT

			SELECT @tintCommitAllocation=ISNULL(tintCommitAllocation,1) FROM Domain WHERE numDomainId=@numDomainID
			SELECT @bitAllocateInventoryOnPickList=ISNULL(bitAllocateInventoryOnPickList,0) FROM OpportunityMaster INNER JOIN DivisionMaster ON OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID WHERE numOppID=@numOppID

			IF @numOppBizDocsId > 0 AND @tintCommitAllocation=2 AND @bitAllocateInventoryOnPickList=1
			BEGIN
				-- Revert Allocation
				EXEC USP_RevertAllocationPickList @numDomainID,@numOppID,@numOppBizDocsId,@numUserCntID
			END

			UPDATE 
				OpportunityBizDocs
			SET    
				numModifiedBy = @numUserCntID,
				dtModifiedDate = GETUTCDATE()
			WHERE  
				numOppBizDocsId = @numOppBizDocsId

			IF EXISTS (SELECT 1 FROM dbo.OpportunityMaster WHERE numOppId = @numOppID AND tintOppType=1 AND isnull(tintOppStatus,0)=1 )
			BEGIN
				EXEC 
				USP_ManageOpportunityAutomationQueue			
				@numOppQueueID = 0, -- numeric(18, 0)							
				@numDomainID = @numDomainID, -- numeric(18, 0)							
				@numOppId = @numOppID, -- numeric(18, 0)							
				@numOppBizDocsId = @numOppBizDocsId, -- numeric(18, 0)							
				@numOrderStatus = 0, -- numeric(18, 0)					
				@numUserCntID = @numUserCntID, -- numeric(18, 0)					
				@tintProcessStatus = 1, -- tinyint						
				@tintMode = 1 -- TINYINT
			END 

			DECLARE @TEMP TABLE
			(
				numOppItemID NUMERIC(18,0)
				,numNewUnitHour FLOAT
			)

			INSERT INTO @TEMP
			(
				numOppItemID
				,numNewUnitHour
			)
			SELECT
				OBDI.numOppItemID
				,SUM(OBDIFulfillment.numUnitHour)
			FROM 
				OpportunityBizDocs OBD
			INNER JOIN
				OpportunityBizDocItems OBDI
			ON
				OBDI.numOppBizDocID=OBD.numOppBizDocsId
			INNER JOIN 
				OpportunityBizDocs OBDFulfillment 
			ON  
				OBDFulfillment.numSourceBizDocId = OBD.numOppBizDocsId
			INNER JOIN 
				OpportunityBizDocItems OBDIFulfillment 
			ON  
				OBDIFulfillment.numOppBizDocID = OBDFulfillment.numOppBizDocsId
				AND OBDIFulfillment.numOppItemID = OBDI.numOppItemID
			WHERE 
				OBD.numOppId=@numOppId
				AND OBD.numOppBizDocsId=@numOppBizDocsId
			GROUP BY
				OBDI.numOppItemID

			UPDATE 
				OBI
			SET 
				OBI.numUnitHour = T1.numNewUnitHour
				,OBI.monPrice = OI.monPrice
				,OBI.monTotAmount = (OI.monTotAmount/OI.numUnitHour) * T1.numNewUnitHour
				,OBI.fltDiscount = (CASE WHEN OI.bitDiscountType=0 THEN OI.fltDiscount WHEN OI.bitDiscountType=1 THEN (OI.fltDiscount/OI.numUnitHour) * T1.numNewUnitHour ELSE OI.fltDiscount END)
				,OBI.monTotAmtBefDiscount = (OI.monTotAmtBefDiscount/OI.numUnitHour) * T1.numNewUnitHour 
			FROM 
				OpportunityBizDocItems OBI
			JOIN 
				OpportunityItems OI
			ON 
				OBI.numOppItemID=OI.numoppitemtCode
			INNER JOIN
				@TEMP T1
			ON
				OBI.numOppItemID = T1.numOppItemID
			WHERE 
				OI.numOppId=@numOppId 
				AND OBI.numOppBizDocID=@numOppBizDocsId
				AND OBI.numUnitHour > T1.numNewUnitHour

			UPDATE 
				OBI
			SET 
				OBI.numUnitHour = 0
				,OBI.monPrice = OI.monPrice
				,OBI.monTotAmount = 0
				,OBI.fltDiscount = 0
				,OBI.monTotAmtBefDiscount = 0 
			FROM 
				OpportunityBizDocItems OBI
			JOIN 
				OpportunityItems OI
			ON 
				OBI.numOppItemID=OI.numoppitemtCode
			WHERE 
				OI.numOppId=@numOppId 
				AND OBI.numOppBizDocID=@numOppBizDocsId
				AND OBI.numOppItemID NOT IN (SELECT numOppItemID FROM @TEMP)


			DECLARE @monDealAmount as DECIMAL(20,5)
			SELECT @monDealAmount=isnull(dbo.[GetDealAmount](@numOppId,GETUTCDATE(),@numOppBizDocsId),0)

			UPDATE 
				OpportunityBizDocs 
			SET 
				monDealAmount=@monDealAmount
			WHERE  
				numOppBizDocsId = @numOppBizDocsId

			--IF @numOppBizDocsId > 0 AND @tintCommitAllocation=2 AND @bitAllocateInventoryOnPickList=1
			--BEGIN
			--	EXEC USP_PickListManageSOWorkOrder @numDomainID,@numUserCntID,@numOppID,@numOppBizDocsId
			--	-- Allocate Inventory
			--	EXEC USP_ManageInventoryPickList @numDomainID,@numOppID,@numOppBizDocsId,@numUserCntID
			--END
		END
	END
END
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityBizDocs_AutoFulFill')
DROP PROCEDURE USP_OpportunityBizDocs_AutoFulFill
GO
CREATE PROCEDURE [dbo].[USP_OpportunityBizDocs_AutoFulFill] 
	@numDomainID NUMERIC(18,0),
	@numUserCntID NUMERIC(18,0),
	@numOppId NUMERIC(18,0),
	@numOppBizDocID NUMERIC(18,0)
AS  
BEGIN 
BEGIN TRY
	DECLARE @vcItemType AS CHAR(1)
	DECLARE @bitDropShip AS BIT
	DECLARE @numOppItemID AS INT
	DECLARE @numWarehouseItemID AS INT
	DECLARE @numQty AS FLOAT
	DECLARE @numQtyShipped AS FLOAT
	DECLARE @numXmlQty AS FLOAT
	DECLARE @vcSerialLot AS VARCHAR(MAX)
	DECLARE @numSerialCount AS INT
	DECLARE @bitSerial AS BIT
	DECLARE @bitLot AS BIT
	DECLARE @bitAssembly AS BIT
	DECLARE @bitKit AS BIT
	DECLARE @tintCommitAllocation AS TINYINT
	DECLARE @bitAllocateInventoryOnPickList AS BIT

	DECLARE @i AS INT = 1
	DECLARE @COUNT AS INT  
	DECLARE @numNewQtyShipped AS FLOAT

	/* VERIFY BIZDOC IS EXISTS OR NOT */
	IF NOT EXISTS (SELECT numOppBizDocsId FROM OpportunityBizDocs WHERE numOppBizDocsId = @numOppBizDocID)
	BEGIN
		RAISERROR('FULFILLMENT_BIZDOC_IS_DELETED',16,1)
	END

	/* VERIFY WHETHER BIZDOC IS ALREADY FULLFILLED BY SOMEBODY ELSE */
	IF EXISTS (SELECT numOppBizDocsId FROM OpportunityBizDocs WHERE numOppBizDocsId = @numOppBizDocID AND bitFulfilled = 1) 
	BEGIN
		RAISERROR('FULFILLMENT_BIZDOC_ALREADY_FULFILLED',16,1)
	END

	IF EXISTS(SELECT numWOId FROm WorkOrder WHERE numOppId=@numOppId AND numWOStatus <> 23184) --WORK ORDER NOT YET COMPLETED
	BEGIN
		RAISERROR('WORK_ORDER_NOT_COMPLETED',16,1)
	END

	SELECT @tintCommitAllocation=ISNULL(tintCommitAllocation,1) FROM Domain WHERE numDomainId=@numDomainID
	SELECT @bitAllocateInventoryOnPickList=ISNULL(bitAllocateInventoryOnPickList,0) FROM OpportunityMaster INNER JOIN DivisionMaster ON OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID WHERE numOppID=@numOppID

	BEGIN TRANSACTION

		-- CHANGE BIZDIC QTY TO AVAIALLBE QTY TO SHIP
		UPDATE
			OBI
		SET
			OBI.numUnitHour = (CASE 
								WHEN OM.tintOppType = 1 AND OM.tintOppStatus = 1 
								THEN 
									(CASE 
										WHEN (I.charItemType = 'p' AND ISNULL(OBI.bitDropShip, 0) = 0 AND ISNULL(bitAsset,0)=0)
										THEN 
											(CASE WHEN OBI.numUnitHour <= dbo.GetOrderItemAvailableQtyToShip(OBI.numOppItemID,OBI.numWarehouseItmsID,ISNULL(I.bitKitParent,0),@tintCommitAllocation,@bitAllocateInventoryOnPickList) THEN OBI.numUnitHour ELSE dbo.GetOrderItemAvailableQtyToShip(OBI.numOppItemID,OBI.numWarehouseItmsID,ISNULL(I.bitKitParent,0),@tintCommitAllocation,@bitAllocateInventoryOnPickList) END)
										ELSE OBI.numUnitHour
									END) 
								ELSE OBI.numUnitHour
							END)
		FROM
			OpportunityBizDocItems OBI
		INNER JOIN
			OpportunityItems OI
		ON
			OBI.numOppItemID=OI.numOppItemtcode
		INNER JOIN
			OpportunityMaster OM
		ON
			OI.numOppId=OM.numOppId
		INNER JOIN
			WareHouseItems WI
		ON
			OI.numWarehouseItmsID = WI.numWareHouseItemID
		INNER JOIN
			Item I
		ON
			OBI.numItemCode = I.numItemCode
		WHERE
			numOppBizDocID=@numOppBizDocID

		--CHANGE TOTAL AMOUNT
		UPDATE
			OBI
		SET
			OBI.monTotAmount = (OI.monTotAmount/OI.numUnitHour) * ISNULL(OBI.numUnitHour,0),
			OBI.monTotAmtBefDiscount = (OI.monTotAmtBefDiscount/OI.numUnitHour) * ISNULL(OBI.numUnitHour,0)
		FROM
			OpportunityBizDocItems OBI
		INNER JOIN
			OpportunityItems OI
		ON
			OBI.numOppItemID=OI.numOppItemtcode
		INNER JOIN
			OpportunityMaster OM
		ON
			OI.numOppId=OM.numOppId
		INNER JOIN
			WareHouseItems WI
		ON
			OBI.numWarehouseItmsID = WI.numWareHouseItemID
		INNER JOIN
			Item I
		ON
			OBI.numItemCode = I.numItemCode
		WHERE
			numOppBizDocID=@numOppBizDocID

		BEGIN /* REVERT SEIRAL/LOT# NUMBER ASSIGNED FROM PRODCUT&SERVICES TAB */
			UPDATE WHIDL
				SET WHIDL.numQty = (CASE WHEN Item.bitLotNo = 1 THEN ISNULL(WHIDL.numQty,0) + ISNULL(OWSI.numQty,0) ELSE 1 END)
			FROM 
				WareHouseItmsDTL WHIDL
			INNER JOIN
				OppWarehouseSerializedItem OWSI
			ON
				WHIDL.numWareHouseItmsDTLID = OWSI.numWarehouseItmsDTLID
				AND WHIDL.numWareHouseItemID = OWSI.numWarehouseItmsID
			INNER JOIN
				WareHouseItems
			ON
				WHIDL.numWareHouseItemID = WareHouseItems.numWareHouseItemID
			INNER JOIN
				Item
			ON
				WareHouseItems.numItemID = Item.numItemCode
			WHERE
				OWSI.numOppID=@numOppId
				AND OWSI.numOppBizDocsId IS NULL
		END

		BEGIN /* GET FULFILLMENT BIZDOC ITEMS */
			DECLARE @TempFinalTable TABLE
			(
				ID INT IDENTITY(1,1),
				numItemCode NUMERIC(18,0),
				vcItemType CHAR(1),
				bitSerial BIT,
				bitLot BIT,
				bitAssembly BIT,
				bitKit BIT,
				numOppItemID NUMERIC(18,0),
				numWarehouseItemID NUMERIC(18,0),
				numQtyShipped FLOAT,
				numQty FLOAT,
				bitDropShip BIT,
				vcSerialLot VARCHAR(MAX)
			)

			INSERT INTO @TempFinalTable
			(
				numItemCode,
				vcItemType,
				bitSerial,
				bitLot,
				bitAssembly,
				bitKit,
				numOppItemID,
				numWarehouseItemID,
				numQtyShipped,
				numQty,
				bitDropShip,
				vcSerialLot
			)
			SELECT
				OpportunityBizDocItems.numItemCode,
				ISNULL(Item.charItemType,''),
				ISNULL(Item.bitSerialized,0),
				ISNULL(Item.bitLotNo,0),
				ISNULL(Item.bitAssembly,0),
				ISNULL(Item.bitKitParent,0),
				ISNULL(OpportunityItems.numoppitemtcode,0),
				ISNULL(OpportunityBizDocItems.numWarehouseItmsID,0),
				ISNULL(OpportunityItems.numQtyShipped,0),
				ISNULL(OpportunityBizDocItems.numUnitHour,0),
				ISNULL(OpportunityItems.bitDropShip,0),
				''
			FROM
				OpportunityBizDocs
			INNER JOIN
				OpportunityBizDocItems
			ON
				OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
			INNER JOIN
				OpportunityItems
			ON
				OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtCode
			INNER JOIN
				Item
			ON
				OpportunityBizDocItems.numItemCode = Item.numItemCode
			WHERE
				numOppBizDocsId = @numOppBizDocID
		END

		BEGIN /* GET ALL ITEMS ADDED TO BIZDOC WITH ASSIGNED SERIAL/LOT# FOR SERIAL/LOT ITEM  */
			DECLARE @TempItemXML TABLE
			(
				numOppItemID NUMERIC(18,0),
				numQty FLOAT,
				numWarehouseItemID NUMERIC(18,0),
				vcSerialLot VARCHAR(MAX)
			)

			INSERT INTO
				@TempItemXML
			SELECT
				OBI.numOppItemID,
				OBI.numUnitHour,
				OBI.numWarehouseItmsID,
				STUFF((SELECT 
							',' + WIDL.vcSerialNo + (CASE WHEN ISNULL(I.bitLotNo,0) = 1 THEN '(' + CAST(OWSI.numQty AS VARCHAR) + ')'  ELSE '' END)
						FROM 
							OppWarehouseSerializedItem OWSI
						INNER JOIN
							WareHouseItmsDTL WIDL
						ON
							OWSI.numWarehouseItmsDTLID = WIDL.numWareHouseItmsDTLID
						INNER JOIN
							WareHouseItems WI
						ON
							OWSI.numWarehouseItmsID = WI.numWareHouseItemID
						INNER JOIN
							Item I
						ON
							WI.numItemID = I.numItemCode
						WHERE 
							OWSI.numOppID = @numOppId
							AND OWSI.numOppItemID = OBI.numOppItemID
							AND ISNULL(OWSI.numOppBizDocsId,0) = 0 --THIS CONDITION IS REQUIRED BECAUSE IT IS AUTOFULFILLMENT
					for xml path('')
				),1,1,'') AS vcSerialLot
			FROM 
				OpportunityBizDocItems OBI
			WHERE
				OBI.numOppBizDocID = @numOppBizDocID
		END

		BEGIN /* VALIDATE ALL THE DETAILS BEFORE FULFILLING ITEMS */
		
			SELECT @COUNT=COUNT(*) FROM @TempFinalTable

			DECLARE @TempLot TABLE
			(
				vcLotNo VARCHAR(100),
				numQty FLOAT 
			)

			WHILE @i <= @COUNT
			BEGIN
				SELECT
					@numOppItemID=numOppItemID,
					@numWarehouseItemID=numWarehouseItemID,
					@numQty=numQty,
					@bitSerial=bitSerial,
					@bitLot=bitLot 
				FROM 
					@TempFinalTable 
				WHERE 
					ID = @i

				--GET QUENTITY RECEIVED FROM XML PARAMETER
				SELECT 
					@numXmlQty = numQty, 
					@vcSerialLot=vcSerialLot, 
					@numSerialCount = (CASE WHEN (@bitSerial = 1 OR @bitLot = 1) THEN (SELECT COUNT(*) FROM dbo.SplitString(vcSerialLot,',')) ELSE 0 END)  
				FROM 
					@TempItemXML 
				WHERE 
					numOppItemID = @numOppItemID 

				--CHECK IF QUANTITY OF ITEM IN OpportunityBizDocItems TBALE IS SAME AS QUANTITY SUPPLIED FROM XML
				IF @numQty <> @numXmlQty
				BEGIN
					RAISERROR('QTY_MISMATCH',16,1)
				END

				--IF ITEM IS SERIAL ITEM CHEKC THAT VALID AND REQUIRED NUMBER OF SERIALS ARE PROVIDED BY USER
				IF @bitSerial = 1 
				BEGIN
					IF @numSerialCount <> @numQty
					BEGIN
						RAISERROR('REQUIRED_SERIALS_NOT_PROVIDED',16,1)
					END
					ELSE
					BEGIN
						IF (SELECT COUNT(*) FROM WareHouseItmsDTL WHERE numWareHouseItemID = @numWarehouseItemID AND numQty > 0 AND vcSerialNo IN (SELECT OutParam FROM dbo.SplitString(@vcSerialLot,','))) <> @numSerialCount
						BEGIN
							RAISERROR('INVALID_SERIAL_NUMBERS',16,1)
						END
					END
				END

				IF @bitLot = 1
				BEGIN
					-- DELETE DATA OF PREVIOUS ITERATION FROM TABLE
					DELETE FROM @TempLot

					--CONVERTS COMMA SEPERATED STRING TO LOTNo AND QTY 
					INSERT INTO
						@TempLot
					SELECT
						SUBSTRING(OutParam,0,PATINDEX('%(%', OutParam)),
						CAST(SUBSTRING(OutParam,(PATINDEX('%(%', OutParam) + 1),(PATINDEX('%)%', OutParam) - PATINDEX('%(%', OutParam) - 1)) AS INT)
					FROM
						dbo.SplitString(@vcSerialLot,',')

					--CHECKS WHETHER PROVIDED QTY OF LOT NO IS SAME IS REQUIRED QTY
					IF (SELECT SUM(numQty) FROM @TempLot) <> @numQty
					BEGIN
						RAISERROR('REQUIRED_LOTNO_NOT_PROVIDED',16,1)
					END
					ELSE
					BEGIN
						-- CHECKS WHETHER LOT NO EXISTS IN WareHouseItmsDTL
						IF (SELECT COUNT(*) FROM WareHouseItmsDTL WHERE numWareHouseItemID = @numWarehouseItemID AND vcSerialNo IN (SELECT vcLotNo FROM @TempLot)) <> (SELECT COUNT(*) FROM @TempLot)
						BEGIN
							RAISERROR('INVALID_LOT_NUMBERS',16,1)
						END
						ELSE
						BEGIN
							-- CHECKS WHETHER LOT NO HAVE ENOUGH QTY TO FULFILL ITEM QTY
							IF (SELECT 
									COUNT(*) 
								FROM 
									WareHouseItmsDTL 
								INNER JOIN
									@TempLot TEMPLOT
								ON
									WareHouseItmsDTL.vcSerialNo = TEMPLOT.vcLotNo
								WHERE 
									numWareHouseItemID = @numWarehouseItemID
									AND TEMPLOT.numQty > WareHouseItmsDTL.numQty) > 0
							BEGIN
								RAISERROR('SOME_LOTNO_DO_NOT_HAVE_ENOUGH_QTY',16,1)
							END
						END
					END
				END

		
				UPDATE @TempFinalTable SET vcSerialLot = @vcSerialLot WHERE ID=@i

				SET @i = @i + 1
			END
		END
	
		SET @i = 1
		SELECT @COUNT = COUNT(*) FROM @TempFinalTable
		DECLARE @vcDescription AS VARCHAR(300)
		DECLARE @numAllocation AS FLOAT
		DECLARE @numOnHand AS FLOAT

		DECLARE @j INT = 0
		DECLARE @ChildCount AS INT
		DECLARE @vcChildDescription AS VARCHAR(300)
		DECLARE @numChildAllocation AS FLOAT
		DECLARE @numChildOnHand AS FLOAT
		DECLARE @numOppChildItemID AS INT
		DECLARE @numChildItemCode AS INT
		DECLARE @bitChildIsKit AS BIT
		DECLARE @numChildWarehouseItemID AS INT
		DECLARE @numChildQty AS FLOAT
		DECLARE @numChildQtyShipped AS FLOAT

		DECLARE @TempKitSubItems TABLE
		(
			ID INT,
			numOppChildItemID NUMERIC(18,0),
			numChildItemCode NUMERIC(18,0),
			bitChildIsKit BIT,
			numChildWarehouseItemID NUMERIC(18,0),
			numChildQty FLOAT,
			numChildQtyShipped FLOAT
		)

		DECLARE @k AS INT = 1
		DECLARE @CountKitChildItems AS INT
		DECLARE @vcKitChildDescription AS VARCHAR(300)
		DECLARE @numKitChildAllocation AS FLOAT
		DECLARE @numKitChildOnHand AS FLOAT
		DECLARE @numOppKitChildItemID AS NUMERIC(18,0)
		DECLARE @numKitChildItemCode AS NUMERIC(18,0)
		DECLARE @numKitChildWarehouseItemID AS NUMERIC(18,0)
		DECLARE @numKitChildQty AS FLOAT
		DECLARE @numKitChildQtyShipped AS FLOAT

		DECLARE @TempKitChildKitSubItems TABLE
		(
			ID INT,
			numOppKitChildItemID NUMERIC(18,0),
			numKitChildItemCode NUMERIC(18,0),
			numKitChildWarehouseItemID NUMERIC(18,0),
			numKitChildQty FLOAT,
			numKitChildQtyShipped FLOAT
		)

		WHILE @i <= @COUNT
		BEGIN
			SELECT 
				@vcItemType=vcItemType,
				@bitSerial=bitSerial,
				@bitLot=bitLot,
				@bitAssembly=bitAssembly,
				@bitKit=bitKit,
				@bitDropShip=bitDropShip,
				@numOppItemID=numOppItemID,
				@numQty=numQty,
				@numQtyShipped = numQtyShipped,
				@numWarehouseItemID=numWarehouseItemID,
				@vcSerialLot = vcSerialLot
			FROM 
				@TempFinalTable 
			WHERE 
				ID = @i

			--MAKE INVENTIRY CHANGES IF INVENTORY ITEM
			IF (UPPER(@vcItemType) = 'P' AND @bitDropShip = 0)
			BEGIN
				SET @vcDescription='SO Qty Shipped (Qty:' + CAST(@numQty AS VARCHAR(10)) + ' Shipped:' +  CAST(@numQtyShipped AS VARCHAR(10)) + ')'
						 
				IF @bitKit = 1
				BEGIN
					-- CLEAR DATA OF PREVIOUS ITERATION
					DELETE FROM @TempKitSubItems

					-- GET KIT SUB ITEMS DETAIL
					INSERT INTO @TempKitSubItems
					(
						ID,
						numOppChildItemID,
						numChildItemCode,
						bitChildIsKit,
						numChildWarehouseItemID,
						numChildQty,
						numChildQtyShipped
					)
					SELECT 
						ROW_NUMBER() OVER(ORDER BY numOppChildItemID),
						ISNULL(numOppChildItemID,0),
						ISNULL(I.numItemCode,0),
						ISNULL(I.bitKitParent,0),
						ISNULL(numWareHouseItemId,0),
						((ISNULL(numQtyItemsReq_Orig,0) * ISNULL(dbo.fn_UOMConversion(OKI.numUOMId,OKI.numChildItemID,@numDomainID,I.numBaseUnit),1)) * @numQty),
						ISNULL(numQtyShipped,0)
					FROM 
						OpportunityKitItems OKI 
					JOIN 
						Item I 
					ON 
						OKI.numChildItemID=I.numItemCode    
					WHERE 
						charitemtype='P' 
						AND ISNULL(numWareHouseItemId,0) > 0 
						AND OKI.numOppID=@numOppID 
						AND OKI.numOppItemID=@numOppItemID 

					SET @j = 1
					SELECT @ChildCount=COUNT(*) FROM @TempKitSubItems

					--LOOP OVER ALL CHILD ITEMS AND RELESE ALLOCATION
					WHILE @j <= @ChildCount
					BEGIN
						SELECT
							@numOppChildItemID=numOppChildItemID,
							@numChildItemCode=numChildItemCode,
							@bitChildIsKit=bitChildIsKit,
							@numChildWarehouseItemID=numChildWarehouseItemID,
							@numChildQty=numChildQty,
							@numChildQtyShipped=numChildQtyShipped
						FROM
							@TempKitSubItems
						WHERE 
							ID = @j

						SELECT @numChildAllocation=ISNULL(numAllocation,0),@numChildOnHand=ISNULL(numOnHand,0) FROM WareHouseItems WHERE numWareHouseItemID=@numChildWarehouseItemID

						-- IF CHILD ITEM IS KIT THEN LOOK OUT FOR ITS CHILD ITEMS WHICH ARE STORED IN OpportunityKitChildItems TABLE
						IF @bitChildIsKit = 1
						BEGIN
							-- IF KIT IS ADDED WITHIN KIT THEN THIS SUB KIT HAS ITS OWN ITEM CONFIGURATION WHICH ID STORED IN OpportunityKitChildItems
							IF EXISTS (SELECT numOppKitChildItemID FROM OpportunityKitChildItems WHERE numOppID=@numOppId AND numOppItemID=@numOppItemID AND numOppChildItemID=@numOppChildItemID)
							BEGIN
								-- CLEAR DATA OF PREVIOUS ITERATION
								DELETE FROM @TempKitChildKitSubItems

								-- GET SUB KIT SUB ITEMS DETAIL
								INSERT INTO @TempKitChildKitSubItems
								(
									ID,
									numOppKitChildItemID,
									numKitChildItemCode,
									numKitChildWarehouseItemID,
									numKitChildQty,
									numKitChildQtyShipped
								)
								SELECT 
									ROW_NUMBER() OVER(ORDER BY numOppKitChildItemID),
									ISNULL(numOppKitChildItemID,0),
									ISNULL(OKCI.numItemID,0),
									ISNULL(OKCI.numWareHouseItemId,0),
									((ISNULL(OKCI.numQtyItemsReq_Orig,0) * ISNULL(dbo.fn_UOMConversion(OKCI.numUOMId,OKCI.numItemID,@numDomainID,I.numBaseUnit),1)) * @numChildQty),
									ISNULL(numQtyShipped,0)
								FROM 
									OpportunityKitChildItems OKCI 
								JOIN 
									Item I 
								ON 
									OKCI.numItemID=I.numItemCode    
								WHERE 
									charitemtype='P' 
									AND ISNULL(numWareHouseItemId,0) > 0 
									AND OKCI.numOppID=@numOppID 
									AND OKCI.numOppItemID=@numOppItemID 
									AND OKCI.numOppChildItemID = @numOppChildItemID

								SET @k = 1
								SELECT @CountKitChildItems=COUNT(*) FROM @TempKitChildKitSubItems

								WHILE @k <= @CountKitChildItems
								BEGIN
									SELECT
										@numOppKitChildItemID=numOppKitChildItemID,
										@numKitChildItemCode=numKitChildItemCode,
										@numKitChildWarehouseItemID=numKitChildWarehouseItemID,
										@numKitChildQty=numKitChildQty,
										@numKitChildQtyShipped=numKitChildQtyShipped
									FROM
										@TempKitChildKitSubItems
									WHERE 
										ID = @k

									SELECT @numKitChildAllocation=ISNULL(numAllocation,0),@numKitChildOnHand=ISNULL(numOnHand,0) FROM WareHouseItems WHERE numWareHouseItemID=@numKitChildWarehouseItemID

									IF @tintCommitAllocation = 1 OR (@tintCommitAllocation = 2 AND @bitAllocateInventoryOnPickList=1)--When order is created OR Allocation on pick list add
									BEGIN
										--REMOVE QTY FROM ALLOCATION
										SET @numKitChildAllocation = @numKitChildAllocation - @numKitChildQty

										IF @numKitChildAllocation >= 0
										BEGIN
											UPDATE  
												WareHouseItems
											SET     
												numAllocation = @numKitChildAllocation,
												dtModified = GETDATE() 
											WHERE   
												numWareHouseItemID = @numKitChildWarehouseItemID
										END
										ELSE
										BEGIN
											RAISERROR ('NOTSUFFICIENTQTY_ALLOCATION',16,1);
										END
									END
									ELSE  -- When added fulfillment order
									BEGIN
										--REMOVE QTY FROM ON HAND
										SET @numKitChildOnHand = @numKitChildOnHand - @numKitChildQty

										IF @numKitChildOnHand >= 0
										BEGIN
											UPDATE  
												WareHouseItems
											SET     
												numOnHand = @numKitChildOnHand,
												dtModified = GETDATE() 
											WHERE   
												numWareHouseItemID = @numKitChildWarehouseItemID
										END
										ELSE
										BEGIN
											RAISERROR ('NOTSUFFICIENTQTY_ONHAND',16,1);
										END
									END

									UPDATE  
										OpportunityKitChildItems
									SET 
										numQtyShipped = ISNULL(numQtyShipped,0) + ISNULL(@numKitChildQty,0)
									WHERE   
										numOppKitChildItemID = @numOppKitChildItemID
										AND numOppChildItemID = @numOppChildItemID 
										AND numOppId=@numOppId
										AND numOppItemID=@numOppItemID

									SET @vcKitChildDescription = 'SO CHILD KIT Qty Shipped (Qty:' + CAST(@numKitChildQty AS VARCHAR(10)) + ' Shipped:' +  CAST(@numKitChildQtyShipped AS VARCHAR(10)) + ')'

									EXEC dbo.USP_ManageWareHouseItems_Tracking
										@numWareHouseItemID = @numKitChildWarehouseItemID,
										@numReferenceID = @numOppId,
										@tintRefType = 3,
										@vcDescription = @vcKitChildDescription,
										@numModifiedBy = @numUserCntID,
										@numDomainID = @numDomainID	 

									SET @k = @k + 1
								END
							END 

							UPDATE  
								OpportunityKitItems
							SET 
								numQtyShipped = ISNULL(numQtyShipped,0) + ISNULL(@numChildQty,0)
							WHERE   
								numOppChildItemID = @numOppChildItemID 
								AND numOppId=@numOppId
								AND numOppItemID=@numOppItemID

							SET @vcChildDescription = 'SO KIT Qty Shipped (Qty:' + CAST(@numChildQty AS VARCHAR(10)) + ' Shipped:' +  CAST(@numChildQtyShipped AS VARCHAR(10)) + ')'

							EXEC dbo.USP_ManageWareHouseItems_Tracking
								@numWareHouseItemID = @numChildWarehouseItemID,
								@numReferenceID = @numOppId,
								@tintRefType = 3,
								@vcDescription = @vcChildDescription,
								@numModifiedBy = @numUserCntID,
								@numDomainID = @numDomainID	
						END
						ELSE
						BEGIN
							IF @tintCommitAllocation = 1 OR (@tintCommitAllocation = 2 AND @bitAllocateInventoryOnPickList=1)--When order is created OR Allocation on pick list add
							BEGIN
								--REMOVE QTY FROM ALLOCATION
								SET @numChildAllocation = @numChildAllocation - @numChildQty

								IF @numChildAllocation >= 0
								BEGIN
									UPDATE  
										WareHouseItems
									SET     
										numAllocation = @numChildAllocation,
										dtModified = GETDATE() 
									WHERE   
										numWareHouseItemID = @numChildWarehouseItemID 
								END
								ELSE
								BEGIN
									RAISERROR ('NOTSUFFICIENTQTY_ALLOCATION',16,1);
								END
							END
							ELSE -- When added fulfillment order
							BEGIN
								--REMOVE QTY FROM ONHAND
								SET @numChildOnHand = @numChildOnHand - @numChildQty

								IF @numChildOnHand >= 0
								BEGIN
									UPDATE  
										WareHouseItems
									SET     
										numOnHand = @numChildOnHand,
										dtModified = GETDATE() 
									WHERE   
										numWareHouseItemID = @numChildWarehouseItemID 
								END
								ELSE
								BEGIN
									RAISERROR ('NOTSUFFICIENTQTY_ONHAND',16,1);
								END
							END     	
							
							UPDATE  
								OpportunityKitItems
							SET 
								numQtyShipped = ISNULL(numQtyShipped,0) + ISNULL(@numChildQty,0)
							WHERE   
								numOppChildItemID = @numOppChildItemID 
								AND numOppId=@numOppId
								AND numOppItemID=@numOppItemID

							SET @vcChildDescription = 'SO KIT Qty Shipped (Qty:' + CAST(@numChildQty AS VARCHAR(10)) + ' Shipped:' +  CAST(@numChildQtyShipped AS VARCHAR(10)) + ')'

							EXEC dbo.USP_ManageWareHouseItems_Tracking
								@numWareHouseItemID = @numChildWarehouseItemID,
								@numReferenceID = @numOppId,
								@tintRefType = 3,
								@vcDescription = @vcChildDescription,
								@numModifiedBy = @numUserCntID,
								@numDomainID = @numDomainID
						END

						SET @j = @j + 1
					END
				END
				ELSE
				BEGIN
					SELECT @numAllocation = ISNULL(numAllocation,0),@numOnHand=ISNULL(numOnHand,0) FROM WareHouseItems WHERE numWareHouseItemID = @numWareHouseItemID
				       
					IF @tintCommitAllocation = 1 OR (@tintCommitAllocation = 2 AND @bitAllocateInventoryOnPickList=1)--When order is created OR Allocation on pick list add
					BEGIN
						--REMOVE QTY FROM ALLOCATION
						SET @numAllocation = @numAllocation - @numQty

						IF (@numAllocation >= 0 )
						BEGIN
							UPDATE  
								WareHouseItems
							SET     
								numAllocation = @numAllocation,
								dtModified = GETDATE() 
							WHERE   
								numWareHouseItemID = @numWareHouseItemID
						END
						ELSE
						BEGIN
							RAISERROR ('NOTSUFFICIENTQTY_ALLOCATION',16,1);
						END
					END
					ELSE 
					BEGIN
						--REMOVE QTY FROM ON HAND
						SET @numOnHand = @numOnHand - @numQty

						IF (@numOnHand >= 0 )
						BEGIN
							UPDATE  
								WareHouseItems
							SET     
								numOnHand = @numOnHand,
								dtModified = GETDATE() 
							WHERE   
								numWareHouseItemID = @numWareHouseItemID
						END
						ELSE
						BEGIN
							RAISERROR ('NOTSUFFICIENTQTY_ONHAND',16,1);
						END
					END
						
					IF @bitSerial = 1
					BEGIN
						DELETE FROM OppWarehouseSerializedItem WHERE numOppID=@numOppID AND numOppItemID=@numOppItemID AND numWarehouseItmsID=@numWarehouseItemID

						INSERT INTO OppWarehouseSerializedItem
						(
							numWarehouseItmsDTLID,
							numOppID,
							numOppItemID,
							numWarehouseItmsID,
							numQty,
							numOppBizDocsId	
						)
						SELECT
							(SELECT TOP 1 numWarehouseItmsDTLID FROM WareHouseItmsDTL WHERE numWareHouseItemID=@numWarehouseItemID AND vcSerialNo=OutParam AND  numQty = 1),
							@numOppID,
							@numOppItemID,
							@numWarehouseItemID,
							1,
							@numOppBizDocID
						FROM
							dbo.SplitString(@vcSerialLot,',')


						UPDATE WareHouseItmsDTL SET numQty=0 WHERE numWareHouseItemID=@numWarehouseItemID AND vcSerialNo IN (SELECT OutParam FROM dbo.SplitString(@vcSerialLot,','))
					END

					IF @bitLot = 1
					BEGIN
						DELETE FROM @TempLot

						INSERT INTO
							@TempLot
						SELECT
							SUBSTRING(OutParam,0,PATINDEX('%(%', OutParam)),
							CAST(SUBSTRING(OutParam,(PATINDEX('%(%', OutParam) + 1),(PATINDEX('%)%', OutParam) - PATINDEX('%(%', OutParam) - 1)) AS INT)
						FROM
							dbo.SplitString(@vcSerialLot,',')

						DELETE FROM OppWarehouseSerializedItem WHERE numOppID=@numOppID AND numOppItemID=@numOppItemID AND numWarehouseItmsID=@numWarehouseItemID

						INSERT INTO OppWarehouseSerializedItem
						(
							numWarehouseItmsDTLID,
							numOppID,
							numOppItemID,
							numWarehouseItmsID,
							numQty,
							numOppBizDocsId	
						)
						SELECT
							(SELECT TOP 1 numWarehouseItmsDTLID FROM WareHouseItmsDTL WHERE numWareHouseItemID=@numWarehouseItemID AND vcSerialNo=TEMPLOT.vcLotNo AND numQty >= TEMPLOT.numQty),
							@numOppID,
							@numOppItemID,
							@numWarehouseItemID,
							TEMPLOT.numQty,
							@numOppBizDocID
						FROM
							@TempLot TEMPLOT


						UPDATE	TEMPWareHouseItmsDTL
							SET TEMPWareHouseItmsDTL.numQty = ISNULL(TEMPWareHouseItmsDTL.numQty,0) - ISNULL(TEMPLOT.numQty,0)
						FROM
							WareHouseItmsDTL TEMPWareHouseItmsDTL
						INNER JOIN
							@TempLot TEMPLOT
						ON
							TEMPWareHouseItmsDTL.vcSerialNo = TEMPLOT.vcLotNo
						WHERE
							TEMPWareHouseItmsDTL.numWareHouseItemID = @numWarehouseItemID
					END
				END

				EXEC dbo.USP_ManageWareHouseItems_Tracking
							@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
							@numReferenceID = @numOppId, --  numeric(9, 0)
							@tintRefType = 3, --  tinyint
							@vcDescription = @vcDescription, --  varchar(100)
							@numModifiedBy = @numUserCntID,
							@numDomainID = @numDomainID	 
			END

			UPDATE  
				OpportunityItems
			SET 
				numQtyShipped = ISNULL(numQtyShipped,0) + ISNULL(@numQty,0)
			WHERE   
				numoppitemtCode = @numOppItemID 
				AND numOppId=@numOppId

			SET @i = @i + 1
		END

		UPDATE OpportunityBizDocs SET bitFulfilled = 1,dtShippedDate=GETUTCDATE() WHERE numOppBizDocsId=@numOppBizDocID
	COMMIT TRANSACTION
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityBizDocs_GetPickListWithNoFulfillment')
DROP PROCEDURE USP_OpportunityBizDocs_GetPickListWithNoFulfillment
GO
CREATE PROCEDURE [dbo].[USP_OpportunityBizDocs_GetPickListWithNoFulfillment]
(
    @numOppID AS NUMERIC(18,0)
)
AS 
BEGIN
	SELECT
		OBDPickList.numOppBizDocsId
		,OBDPickList.vcBizDocID
	FROM
		OpportunityBizDocs OBDPickList
	LEFT JOIN
		OpportunityBizDocs OBDFilfillment
	ON
		OBDFilfillment.numOppId=@numOppID
		AND OBDFilfillment.numBizDocId=296 --Fulfillment BizDoc
		AND OBDFilfillment.numSourceBizDocId = OBDPickList.numOppBizDocsId
	WHERE
		OBDPickList.numOppId=@numOppID
		AND OBDPickList.numBizDocId=29397
		AND OBDFilfillment.numOppBizDocsId IS NULL
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityBizDocs_RevertFulFillment')
DROP PROCEDURE USP_OpportunityBizDocs_RevertFulFillment
GO
CREATE PROCEDURE [dbo].[USP_OpportunityBizDocs_RevertFulFillment] 
	@numDomainID NUMERIC(18,0),
	@numUserCntID NUMERIC(18,0),
	@numOppId NUMERIC(18,0),
	@numOppBizDocID NUMERIC(18,0)
AS  
BEGIN 
BEGIN TRY
	DECLARE @tintCommitAllocation AS TINYINT
	DECLARE @bitAllocateInventoryOnPickList AS BIT

	SELECT @tintCommitAllocation=ISNULL(tintCommitAllocation,1) FROM Domain WHERE numDomainId=@numDomainID
	SELECT @bitAllocateInventoryOnPickList=ISNULL(bitAllocateInventoryOnPickList,0) FROM OpportunityMaster INNER JOIN DivisionMaster ON OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID WHERE numOppID=@numOppID

	DECLARE @vcItemType AS CHAR(1)
	DECLARE @bitDropShip AS BIT
	DECLARE @bitSerial AS BIT
	DECLARE @bitLot AS BIT
	DECLARE @bitAssembly AS BIT
	DECLARE @bitKit AS BIT
	DECLARE @numOppItemID AS INT
	DECLARE @numWarehouseItemID AS INT
	DECLARE @numQty AS FLOAT
	DECLARE @numQtyShipped AS FLOAT
	DECLARE @vcDescription AS VARCHAR(300)


	DECLARE @j INT = 0
	DECLARE @ChildCount AS INT
	DECLARE @vcChildDescription AS VARCHAR(300)
	DECLARE @numOppChildItemID AS INT
	DECLARE @numChildItemCode AS INT
	DECLARE @bitChildIsKit AS BIT
	DECLARE @numChildWarehouseItemID AS INT
	DECLARE @numChildQty AS FLOAT
	DECLARE @numChildQtyShipped AS FLOAT

	DECLARE @TempKitSubItems TABLE
	(
		ID INT,
		numOppChildItemID NUMERIC(18,0),
		numChildItemCode NUMERIC(18,0),
		bitChildIsKit BIT,
		numChildWarehouseItemID NUMERIC(18,0),
		numChildQty FLOAT,
		numChildQtyShipped FLOAT
	)

	DECLARE @k AS INT = 1
	DECLARE @CountKitChildItems AS INT
	DECLARE @vcKitChildDescription AS VARCHAR(300)
	DECLARE @numOppKitChildItemID AS NUMERIC(18,0)
	DECLARE @numKitChildItemCode AS NUMERIC(18,0)
	DECLARE @numKitChildWarehouseItemID AS NUMERIC(18,0)
	DECLARE @numKitChildQty AS FLOAT
	DECLARE @numKitChildQtyShipped AS FLOAT

	DECLARE @TempKitChildKitSubItems TABLE
	(
		ID INT,
		numOppKitChildItemID NUMERIC(18,0),
		numKitChildItemCode NUMERIC(18,0),
		numKitChildWarehouseItemID NUMERIC(18,0),
		numKitChildQty FLOAT,
		numKitChildQtyShipped FLOAT
	)

	DECLARE @TempFinalTable TABLE
	(
		ID INT IDENTITY(1,1),
		numItemCode NUMERIC(18,0),
		vcItemType CHAR(1),
		bitSerial BIT,
		bitLot BIT,
		bitAssembly BIT,
		bitKit BIT,
		numOppItemID NUMERIC(18,0),
		numWarehouseItemID NUMERIC(18,0),
		numQtyShipped FLOAT,
		numQty FLOAT,
		bitDropShip BIT,
		vcSerialLot VARCHAR(MAX)
	)

	INSERT INTO @TempFinalTable
	(
		numItemCode,
		vcItemType,
		bitSerial,
		bitLot,
		bitAssembly,
		bitKit,
		numOppItemID,
		numWarehouseItemID,
		numQtyShipped,
		numQty,
		bitDropShip,
		vcSerialLot
	)
	SELECT
		OpportunityBizDocItems.numItemCode,
		ISNULL(Item.charItemType,''),
		ISNULL(Item.bitSerialized,0),
		ISNULL(Item.bitLotNo,0),
		ISNULL(Item.bitAssembly,0),
		ISNULL(Item.bitKitParent,0),
		ISNULL(OpportunityItems.numoppitemtcode,0),
		ISNULL(OpportunityBizDocItems.numWarehouseItmsID,0),
		ISNULL(OpportunityItems.numQtyShipped,0),
		ISNULL(OpportunityBizDocItems.numUnitHour,0),
		ISNULL(OpportunityItems.bitDropShip,0),
		''
	FROM
		OpportunityBizDocs
	INNER JOIN
		OpportunityBizDocItems
	ON
		OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
	INNER JOIN
		OpportunityItems
	ON
		OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtCode
	INNER JOIN
		Item
	ON
		OpportunityBizDocItems.numItemCode = Item.numItemCode
	WHERE
		numOppBizDocsId = @numOppBizDocID

	BEGIN TRANSACTION
		DECLARE @i INT = 1
		DECLARE @COUNT INT = 0
		SELECT @COUNT = COUNT(*) FROM @TempFinalTable

		WHILE @i <= @COUNT
		BEGIN
			SELECT 
				@vcItemType=vcItemType,
				@bitSerial=bitSerial,
				@bitLot=bitLot,
				@bitAssembly=bitAssembly,
				@bitKit=bitKit,
				@bitDropShip=bitDropShip,
				@numOppItemID=numOppItemID,
				@numQty=numQty,
				@numQtyShipped = numQtyShipped,
				@numWarehouseItemID=numWarehouseItemID
			FROM 
				@TempFinalTable 
			WHERE 
				ID = @i

			IF @numQty > @numQtyShipped
			BEGIN
				RAISERROR ('INVALID_SHIPPED_QTY',16,1);
			END

			--REVERT QUANTITY TO ON ALLOCATION IF INVENTORY ITEM
			IF (UPPER(@vcItemType) = 'P' AND @bitDropShip = 0)
			BEGIN
				SET @vcDescription='SO Qty Shipped Return (Qty:' + CAST(@numQty AS VARCHAR(10)) + ' Shipped:' +  CAST(@numQtyShipped AS VARCHAR(10)) + ')'
				
				IF @bitKit = 1
				BEGIN
					-- CLEAR DATA OF PREVIOUS ITERATION
					DELETE FROM @TempKitSubItems

					-- GET KIT SUB ITEMS DETAIL
					INSERT INTO @TempKitSubItems
					(
						ID,
						numOppChildItemID,
						numChildItemCode,
						bitChildIsKit,
						numChildWarehouseItemID,
						numChildQty,
						numChildQtyShipped
					)
					SELECT 
						ROW_NUMBER() OVER(ORDER BY numOppChildItemID),
						ISNULL(numOppChildItemID,0),
						ISNULL(I.numItemCode,0),
						ISNULL(I.bitKitParent,0),
						ISNULL(OKI.numWareHouseItemId,0),
						((ISNULL(OKI.numQtyItemsReq_Orig,0) * ISNULL(dbo.fn_UOMConversion(OKI.numUOMId,OKI.numChildItemID,@numDomainID,I.numBaseUnit),1)) * @numQty),
						ISNULL(OKI.numQtyShipped,0)
					FROM 
						OpportunityKitItems OKI 
					JOIN 
						Item I 
					ON 
						OKI.numChildItemID=I.numItemCode    
					WHERE 
						charitemtype='P' 
						AND ISNULL(numWareHouseItemId,0) > 0 
						AND OKI.numOppID=@numOppID 
						AND OKI.numOppItemID=@numOppItemID 

					SET @j = 1
					SELECT @ChildCount=COUNT(*) FROM @TempKitSubItems

					--LOOP OVER ALL CHILD ITEMS AND REVERT ALLOCATION
					WHILE @j <= @ChildCount
					BEGIN
					
						SELECT
							@numOppChildItemID=numOppChildItemID,
							@numChildItemCode=numChildItemCode,
							@bitChildIsKit=bitChildIsKit,
							@numChildWarehouseItemID=numChildWarehouseItemID,
							@numChildQty=numChildQty,
							@numChildQtyShipped=numChildQtyShipped
						FROM
							@TempKitSubItems
						WHERE 
							ID = @j

						IF @numChildQty > @numChildQtyShipped
						BEGIN
							RAISERROR ('INVALID_KIT_SUB_ITEM_SHIPPED_QTY',16,1);
						END

						IF @bitChildIsKit = 1
						BEGIN
							-- IF KIT IS ADDED WITHIN KIT THEN THIS SUB KIT HAS ITS OWN ITEM CONFIGURATION WHICH ID STORED IN OpportunityKitChildItems
							IF EXISTS (SELECT numOppKitChildItemID FROM OpportunityKitChildItems WHERE numOppID=@numOppId AND numOppItemID=@numOppItemID AND numOppChildItemID=@numOppChildItemID)
							BEGIN
								-- CLEAR DATA OF PREVIOUS ITERATION
								DELETE FROM @TempKitChildKitSubItems

								-- GET SUB KIT SUB ITEMS DETAIL
								INSERT INTO @TempKitChildKitSubItems
								(
									ID,
									numOppKitChildItemID,
									numKitChildItemCode,
									numKitChildWarehouseItemID,
									numKitChildQty,
									numKitChildQtyShipped
								)
								SELECT 
									ROW_NUMBER() OVER(ORDER BY numOppKitChildItemID),
									ISNULL(numOppKitChildItemID,0),
									ISNULL(OKCI.numItemID,0),
									ISNULL(OKCI.numWareHouseItemId,0),
									((ISNULL(OKCI.numQtyItemsReq_Orig,0) * ISNULL(dbo.fn_UOMConversion(OKCI.numUOMId,OKCI.numItemID,@numDomainID,I.numBaseUnit),1)) * @numChildQty),
									ISNULL(numQtyShipped,0)
								FROM 
									OpportunityKitChildItems OKCI 
								JOIN 
									Item I 
								ON 
									OKCI.numItemID=I.numItemCode    
								WHERE 
									charitemtype='P' 
									AND ISNULL(numWareHouseItemId,0) > 0 
									AND OKCI.numOppID=@numOppID 
									AND OKCI.numOppItemID=@numOppItemID 
									AND OKCI.numOppChildItemID = @numOppChildItemID

								SET @k = 1
								SELECT @CountKitChildItems=COUNT(*) FROM @TempKitChildKitSubItems


								--LOOP OVER ALL CHILD ITEMS AND REVERT ALLOCATION
								WHILE @k <= @CountKitChildItems
								BEGIN
									SELECT
										@numOppKitChildItemID=numOppKitChildItemID,
										@numKitChildItemCode=numKitChildItemCode,
										@numKitChildWarehouseItemID=numKitChildWarehouseItemID,
										@numKitChildQty=numKitChildQty,
										@numKitChildQtyShipped=numKitChildQtyShipped
									FROM
										@TempKitChildKitSubItems
									WHERE 
										ID = @k

									IF @numKitChildQty > @numKitChildQtyShipped
									BEGIN
										RAISERROR ('INVALID_KIT_SUB_ITEM_SHIPPED_QTY',16,1);
									END

									IF @tintCommitAllocation = 1 OR (@tintCommitAllocation = 2 AND @bitAllocateInventoryOnPickList=1)--When order is created OR Allocation on pick list add
									BEGIN						
										--PLACE QTY BACK ON ALLOCATION
										UPDATE  
											WareHouseItems
										SET     
											numAllocation = ISNULL(numAllocation,0) + @numKitChildQty,
											dtModified = GETDATE() 
										WHERE   
											numWareHouseItemID = @numKitChildWarehouseItemID
									END
									ELSE -- When added fulfillment order
									BEGIN
										--PLACE QTY BACK ON ONHAND
										UPDATE  
											WareHouseItems
										SET     
											numOnHand = ISNULL(numOnHand,0) + @numKitChildQty,
											dtModified = GETDATE() 
										WHERE   
											numWareHouseItemID = @numKitChildWarehouseItemID
									END

									--DECREASE QTY SHIPPED OF ORDER ITEM	
									UPDATE  
										OpportunityKitChildItems
									SET 
										numQtyShipped = ISNULL(numQtyShipped,0) - ISNULL(@numKitChildQty,0)
									WHERE   
										numOppKitChildItemID = @numOppKitChildItemID
										AND numOppChildItemID = @numOppChildItemID 
										AND numOppId=@numOppId
										AND numOppItemID=@numOppItemID

									SET @vcKitChildDescription = 'SO CHILD KIT Qty Shipped Return (Qty:' + CAST(@numKitChildQty AS VARCHAR(10)) + ' Shipped:' +  CAST(@numKitChildQtyShipped AS VARCHAR(10)) + ')'

									-- CREATE WAREHOUSE INVENTORY TRACKING ENTRY
									EXEC dbo.USP_ManageWareHouseItems_Tracking
										@numWareHouseItemID = @numKitChildWarehouseItemID,
										@numReferenceID = @numOppId,
										@tintRefType = 3,
										@vcDescription = @vcKitChildDescription,
										@numModifiedBy = @numUserCntID,
										@numDomainID = @numDomainID	 
									
									SET @k = @k + 1
								END
							END

							--DECREASE QTY SHIPPED OF ORDER ITEM	
							UPDATE  
								OpportunityKitItems
							SET 
								numQtyShipped = ISNULL(numQtyShipped,0) - ISNULL(@numChildQty,0)
							WHERE   
								numOppChildItemID = @numOppChildItemID 
								AND numOppId=@numOppId
								AND numOppItemID=@numOppItemID

							SET @vcChildDescription = 'SO KIT Qty Shipped Return (Qty:' + CAST(@numChildQty AS VARCHAR(10)) + ' Shipped:' +  CAST(@numChildQtyShipped AS VARCHAR(10)) + ')'

							-- CREATE WAREHOUSE INVENTORY TRACKING ENTRY
							EXEC dbo.USP_ManageWareHouseItems_Tracking
								@numWareHouseItemID = @numChildWarehouseItemID,
								@numReferenceID = @numOppId,
								@tintRefType = 3,
								@vcDescription = @vcChildDescription,
								@numModifiedBy = @numUserCntID,
								@numDomainID = @numDomainID	 
						END
						ELSE
						BEGIN
							IF @tintCommitAllocation = 1 OR (@tintCommitAllocation = 2 AND @bitAllocateInventoryOnPickList=1)--When order is created OR Allocation on pick list add
							BEGIN
								--PLACE QTY BACK ON ALLOCATION
								UPDATE  
									WareHouseItems
								SET     
									numAllocation = ISNULL(numAllocation,0) + @numChildQty,
									dtModified = GETDATE() 
								WHERE   
									numWareHouseItemID = @numChildWarehouseItemID
							END
							ELSE  -- When added fulfillment order
							BEGIN
								--PLACE QTY BACK ON ONHAND
								UPDATE  
									WareHouseItems
								SET     
									numOnHand = ISNULL(numOnHand,0) + @numChildQty,
									dtModified = GETDATE() 
								WHERE   
									numWareHouseItemID = @numChildWarehouseItemID
							END
							      	
						
							--DECREASE QTY SHIPPED OF ORDER ITEM	
							UPDATE  
								OpportunityKitItems
							SET 
								numQtyShipped = ISNULL(numQtyShipped,0) - ISNULL(@numChildQty,0)
							WHERE   
								numOppChildItemID = @numOppChildItemID 
								AND numOppId=@numOppId
								AND numOppItemID=@numOppItemID

							SET @vcChildDescription = 'SO KIT Qty Shipped Return (Qty:' + CAST(@numChildQty AS VARCHAR(10)) + ' Shipped:' +  CAST(@numChildQtyShipped AS VARCHAR(10)) + ')'

							-- CREATE WAREHOUSE INVENTORY TRACKING ENTRY
							EXEC dbo.USP_ManageWareHouseItems_Tracking
								@numWareHouseItemID = @numChildWarehouseItemID,
								@numReferenceID = @numOppId,
								@tintRefType = 3,
								@vcDescription = @vcChildDescription,
								@numModifiedBy = @numUserCntID,
								@numDomainID = @numDomainID	
						END

						SET @j = @j + 1
					END
				END
				ELSE
				BEGIN
					IF @tintCommitAllocation = 1 OR (@tintCommitAllocation = 2 AND @bitAllocateInventoryOnPickList=1)--When order is created OR Allocation on pick list add
					BEGIN
						--PLACE QTY BACK ON ALLOCATION
						UPDATE  
							WareHouseItems
						SET     
							numAllocation = ISNULL(numAllocation,0) + @numQty
							,dtModified = GETDATE() 
						WHERE   
							numWareHouseItemID = @numWareHouseItemID
					END
					ELSE -- When added fulfillment order
					BEGIN
						-- ADD QUANTITY BACK ON ONHAND
						UPDATE  
							WareHouseItems
						SET     
							numOnHand = ISNULL(numOnHand,0) + @numQty
							,dtModified = GETDATE() 
						WHERE   
							numWareHouseItemID = @numWareHouseItemID
					END

					IF @bitSerial = 1 OR @bitLot = 1
					BEGIN 
						UPDATE OppWarehouseSerializedItem SET numOppBizDocsId=NULL WHERE numOppID=@numOppID AND numOppItemID= @numOppItemID AND numWarehouseItmsID=@numWarehouseItemID AND numOppBizDocsId=@numOppBizDocID
					END
				END

				-- CREATE WAREHOUSE INVENTORY TRACKING ENTRY
				EXEC dbo.USP_ManageWareHouseItems_Tracking
							@numWareHouseItemID = @numWareHouseItemID,
							@numReferenceID = @numOppId,
							@tintRefType = 3,
							@vcDescription = @vcDescription,
							@numModifiedBy = @numUserCntID,
							@numDomainID = @numDomainID	 
				
			END

			--DECREASE QTY SHIPPED OF ORDER ITEM	
			UPDATE 
				OpportunityItems
			SET     
				numQtyShipped = (ISNULL(numQtyShipped,0) - @numQty)
			WHERE   
				numoppitemtCode = @numOppItemID 
				AND numOppId=@numOppId

			SET @i = @i + 1
		END
	COMMIT TRANSACTION 
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRAN

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityMaster_GetDataForEDI856')
DROP PROCEDURE dbo.USP_OpportunityMaster_GetDataForEDI856
GO
CREATE PROCEDURE [dbo].[USP_OpportunityMaster_GetDataForEDI856]
	@numDomainID NUMERIC(18,0)
	,@numOppID NUMERIC(18,0)
AS 
BEGIN
	DECLARE @TableShippingReport TABLE
	(
		numShippingReportId NUMERIC(18,0)
	)

	DECLARE @TableShippingBox TABLE
	(
		numBoxID NUMERIC(18,0)
	)

	INSERT INTO @TableShippingReport
	(
		numShippingReportId
	)
	SELECT
		numShippingReportId
	FROM
		ShippingReport
	WHERE
		ShippingReport.numDomainID=@numDomainID
		AND ShippingReport.numOppID=@numOppID
		AND ISNULL(ShippingReport.bitSendToTP2,0) = 0
		AND 1 = (CASE WHEN (SELECT COUNT(*) FROM ShippingBox WHERE numShippingReportId=ShippingReport.numShippingReportId AND vcShippingLabelImage IS NOT NULL AND vcTrackingNumber IS NOT NULL) > 0 THEN 1 ELSE 0 END)

	INSERT INTO @TableShippingBox
	(
		numBoxID
	)
	SELECT 
		numBoxID
	FROM 
		ShippingBox 
	WHERE 
		numShippingReportId IN (SELECT numShippingReportId FROM @TableShippingReport)

	SELECT
		numShippingReportId
		,CONVERT(CHAR(8), ShippingReport.dtCreateDate, 112) vcShipDate
		,ISNULL(ShippingReport.vcToName,'') vcToName
		,ISNULL(ShippingReport.vcToAddressLine1,'') vcToAddressLine1
		,ISNULL(ShippingReport.vcToAddressLine2,'') vcToAddressLine2
		,ISNULL(ShippingReport.vcToCity,'') vcToCity
		,ISNULL(dbo.fn_GetState(ShippingReport.vcToState),'') vcToState
		,ISNULL(ShippingReport.vcToZip,'') vcToZip
		,ISNULL(dbo.fn_GetListName(ShippingReport.vcToCountry,0),'') vcToCountry
		,ISNULL(ShippingReport.vcFromName,'') vcFromName
		,ISNULL(ShippingReport.vcFromAddressLine1,'') vcFromAddressLine1
		,ISNULL(ShippingReport.vcFromAddressLine2,'') vcFromAddressLine2
		,ISNULL(ShippingReport.vcFromCity,'') vcFromCity
		,ISNULL(dbo.fn_GetState(ShippingReport.vcFromState),'') vcFromState
		,ISNULL(ShippingReport.vcFromZip,'') vcFromZip
		,ISNULL(dbo.fn_GetListName(ShippingReport.vcFromCountry,0),'') vcFromCountry
		,(CASE numShippingCompany WHEN 88 THEN 'UPS' WHEN 90 THEN 'USPS' WHEN 91 THEN 'FedEx' ELSE '' END) vcShipVia
		,CAST(ROUND(((SELECT SUM(ISNULL(fltTotalWeight,0) * ISNULL(intBoxQty,0)) FROM dbo.ShippingReportItems WHERE numShippingReportId = ShippingReport.numShippingReportId)),2) AS DECIMAL(9, 2)) AS fltTotalRegularWeight
		,CAST(ROUND((SELECT SUM(ISNULL(fltDimensionalWeight,0)) FROM dbo.ShippingBox WHERE numShippingReportId = ShippingReport.numShippingReportId), 2) AS DECIMAL(9, 2)) AS fltTotalDimensionalWeight
		,ISNULL(vcCustomerPO#,'') vcCustomerPO#
	FROM
		ShippingReport
	INNER JOIN
		OpportunityMaster
	ON
		ShippingReport.numOppID = OpportunityMaster.numOppId
	WHERE
		numShippingReportId IN (SELECT numShippingReportId FROM @TableShippingReport)


	SELECT 
		numBoxID
		,numShippingReportId
		,fltHeight
		,fltLength
		,fltWidth
		,fltTotalWeight
		,BoxItems.Qty
	FROM 
		ShippingBox 
	OUTER APPLY
	(
		SELECT 
			SUM(ISNULL(ShippingReportItems.intBoxQty,0) * dbo.fn_UOMConversion(Item.numBaseUnit,OpportunityItems.numItemCode,@numDomainID,OpportunityItems.numUOMId)) AS Qty
		FROM 
			ShippingReportItems
		INNER JOIN
			OpportunityBizDocItems 
		ON
			ShippingReportItems.numOppBizDocItemID = OpportunityBizDocItems.numOppBizDocItemID
		INNER JOIN
			OpportunityItems
		ON
			OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtCode
		INNER JOIN
			Item
		ON
			OpportunityItems.numItemCode = Item.numItemCode
		WHERE 
			numBoxID=ShippingBox.numBoxID
	) BoxItems
	WHERE 
		numBoxID IN (SELECT numBoxID FROM @TableShippingBox)
		
	SELECT 
		numBoxID
		,CASE 
			WHEN ISNULL(Item.numItemGroup,0) > 0 AND ISNULL(Item.bitMatrix,0) = 0
			THEN 
				(CASE WHEN LEN(ISNULL(WareHouseItems.vcWHSKU,'')) > 0 THEN WareHouseItems.vcWHSKU ELSE ISNULL(Item.vcSKU,'') END)
			ELSE
				(CASE WHEN LEN(ISNULL(OpportunityItems.vcSKU,'')) > 0 THEN OpportunityItems.vcSKU ELSE ISNULL(Item.vcSKU,'') END)
		END vcSKU
		,CASE 
			WHEN ISNULL(Item.numItemGroup,0) > 0 AND ISNULL(Item.bitMatrix,0) = 0
			THEN 
				(CASE WHEN LEN(ISNULL(WareHouseItems.vcBarCode,'')) > 0 THEN WareHouseItems.vcBarCode ELSE ISNULL(Item.numBarCodeId,'') END)
			ELSE
				ISNULL(Item.numBarCodeId,'')
		END vcUPC
		,CAST((dbo.fn_UOMConversion(ISNULL(Item.numBaseUnit,0),OpportunityItems.numItemCode,@numDomainID, ISNULL(OpportunityItems.numUOMId, 0))* OpportunityItems.numUnitHour) AS NUMERIC(18, 2)) AS numOrderedUnits
		,ISNULL(UOM.vcUnitName,'Units') vcUOM
		,(CASE 
			WHEN charItemType='P' THEN 'Inventory Item'
			WHEN charItemType='S' THEN 'Service' 
			WHEN charItemType='A' THEN 'Accessory' 
			WHEN charItemType='N' THEN 'Non-Inventory Item' 
		END) AS vcItemType
		,CAST((dbo.fn_UOMConversion(ISNULL(Item.numBaseUnit,0),OpportunityItems.numItemCode,@numDomainID, ISNULL(OpportunityItems.numUOMId, 0))* ISNULL(ShippingReportItems.intBoxQty,0)) AS NUMERIC(18, 2)) AS numShippedUnits
		,OpportunityItems.numoppitemtCode 
	FROM 
		ShippingReportItems
	INNER JOIN
		OpportunityBizDocItems 
	ON
		ShippingReportItems.numOppBizDocItemID = OpportunityBizDocItems.numOppBizDocItemID
	INNER JOIN
		OpportunityItems
	ON
		OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtCode
	INNER JOIN
		Item
	ON
		OpportunityItems.numItemCode = Item.numItemCode
	LEFT JOIN
		WareHouseItems
	ON
		OpportunityItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
	LEFT JOIN 
		UOM 
	ON
		OpportunityItems.numUOMId = UOM.numUOMId
	WHERE 
		numBoxID IN (SELECT numBoxID FROM @TableShippingBox)

END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO


IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityMaster_GetDataForEDI940')
DROP PROCEDURE dbo.USP_OpportunityMaster_GetDataForEDI940
GO
CREATE PROCEDURE [dbo].[USP_OpportunityMaster_GetDataForEDI940]
	@numDomainID NUMERIC(18,0)
	,@numOppID NUMERIC(18,0)
AS 
BEGIN
	SELECT
		OpportunityMaster.numOppID
		,OpportunityMaster.vcPOppName
		,CompanyInfo.vcCompanyName
		,Domain.vcDomainName
		,OpportunityMaster.numDomainID
		,OpportunityMaster.numDivisionID
		,CONCAT(OpportunityMaster.numDomainID,'-',OpportunityMaster.numDivisionID) AS vcDomainDivisionID
		,ISNULL(OpportunityMaster.txtComments,'') vcComments
		,ISNULL(AdditionalContactsInformation.vcFirstName,'') vcFirstName
		,ISNULL(AdditionalContactsInformation.vcLastName,'') vcLastName
		,ISNULL(AdditionalContactsInformation.numCell,'') vcPhone
		,ISNULL(OpportunityMaster.vcOppRefOrderNo,'') vcCustomerPO#
		,ISNULL((SELECT TOP 1 vcStreet FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainID,1)),'') AS [vcBillStreet]
		,ISNULL((SELECT TOP 1 vcCity FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainID,1)),'') AS [vcBillCity]
		,ISNULL((SELECT TOP 1 vcState FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainID,1)),'') AS [vcBillState]
		,ISNULL((SELECT TOP 1 vcCountry FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainID,1)),'') AS [vcBillCountry]
		,ISNULL((SELECT TOP 1 vcPostalCode FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainID,1)),'') AS [vcBillPostalCode]
		,ISNULL((SELECT TOP 1 vcStreet FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainID,2)),'') AS [vcShipStreet]
		,ISNULL((SELECT TOP 1 vcCity FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainID,2)),'') AS [vcShipCity]
		,ISNULL((SELECT TOP 1 vcState FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainID,2)),'') AS [vcShipState]
		,ISNULL((SELECT TOP 1 vcCountry FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainID,2)),'') AS [vcShipCountry]
		,ISNULL((SELECT TOP 1 vcPostalCode FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainID,2)),'') AS [vcShipPostalCode]
		,ISNULL((SELECT TOP 1 vcDescription FROM DivisionMasterShippingConfiguration WHERE DivisionMasterShippingConfiguration.numDivisionId=DivisionMaster.numDivisionID),'') vcShippingInstruction
		,(CASE WHEN ISNULL(DivisionMaster.bitShippingLabelRequired,0) = 1 THEN 'Yes' ELSE 'No' END) bitShippingLabelRequired
	FROM
		OpportunityMaster
	INNER JOIN
		Domain
	ON
		OpportunityMaster.numDomainID = Domain.numDomainId
	INNER JOIN
		DivisionMaster
	ON
		OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
	INNER JOIN
		CompanyInfo
	ON
		DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
	LEFT JOIN
		AdditionalContactsInformation
	ON
		OpportunityMaster.numContactId = AdditionalContactsInformation.numContactId
	WHERE
		OpportunityMaster.numDomainId=@numDomainID
		AND OpportunityMaster.numOppId = @numOppID


	SELECT
		Item.vcItemName
		,CASE 
			WHEN ISNULL(Item.numItemGroup,0) > 0 AND ISNULL(Item.bitMatrix,0) = 0
			THEN 
				(CASE WHEN LEN(ISNULL(WareHouseItems.vcWHSKU,'')) > 0 THEN WareHouseItems.vcWHSKU ELSE ISNULL(Item.vcSKU,'') END)
			ELSE
				(CASE WHEN LEN(ISNULL(OpportunityItems.vcSKU,'')) > 0 THEN OpportunityItems.vcSKU ELSE ISNULL(Item.vcSKU,'') END)
		END vcSKU
		,CAST((dbo.fn_UOMConversion(ISNULL(Item.numBaseUnit, 0),OpportunityItems.numItemCode,@numDomainID, ISNULL(OpportunityItems.numUOMId, 0))* OpportunityItems.numUnitHour) AS NUMERIC(18, 2)) AS numUnits
		,ISNULL(UOM.vcUnitName,'Units') vcUOM
		,(CASE 
			WHEN charItemType='P' THEN 'Inventory Item'
			WHEN charItemType='S' THEN 'Service' 
			WHEN charItemType='A' THEN 'Accessory' 
			WHEN charItemType='N' THEN 'Non-Inventory Item' 
		END) AS vcItemType
		,ISNULL(OpportunityItems.vcNotes,'') vcNotes
		,OpportunityItems.numoppitemtCode AS 'BizOppItemID'
	FROM
		OpportunityItems
	INNER JOIN
		Item
	ON
		OpportunityItems.numItemCode = Item.numItemCode
	LEFT JOIN
		WareHouseItems
	ON
		OpportunityItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
	LEFT JOIN 
		UOM 
	ON
		OpportunityItems.numUOMId = UOM.numUOMId
	WHERE
		numOppId = @numOppID
		AND ISNULL(OpportunityItems.bitDropShip,0) = 0
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityMaster_GetDomainID')
DROP PROCEDURE dbo.USP_OpportunityMaster_GetDomainID
GO
CREATE PROCEDURE [dbo].[USP_OpportunityMaster_GetDomainID]
	@numOppID NUMERIC(18,0)
AS 
BEGIN
	SELECT
		numDomainID
	FROM
		OpportunityMaster
	WHERE
		numOppId=@numOppID
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_PickListManageSOWorkOrder')
DROP PROCEDURE USP_PickListManageSOWorkOrder
GO
CREATE PROCEDURE [dbo].[USP_PickListManageSOWorkOrder]
	@numDomainID AS numeric(18,0),
	@numUserCntID AS NUMERIC(18,0),
	@numOppID AS NUMERIC(18,0),
	@numOppBizDocID AS NUMERIC(18,0)
AS
BEGIN
	DECLARE @numItemCode AS NUMERIC(18,0),@numUnitHour AS FLOAT,@numWarehouseItmsID AS NUMERIC(18,0),@vcItemDesc AS VARCHAR(1000)
	DECLARE @numWOId AS NUMERIC(18,0),@vcInstruction AS VARCHAR(1000),@bintCompliationDate AS DATETIME, @numWOAssignedTo NUMERIC(18,0)
	DECLARE @numoppitemtCode NUMERIC(18,0)
  
	DECLARE @TEMP TABLE
	(
		ID INT IDENTITY(1,1)
		,numoppitemtCode NUMERIC(18,0)
		,numItemCode NUMERIC(18,0)
		,numUnitHour FLOAT
		,numWarehouseItmsID NUMERIC(18,0)
		,vcItemDesc VARCHAR(1000)
		,vcInstruction VARCHAR(1000)
		,bintCompliationDate DATETIME
		,numWOAssignedTo NUMERIC(18,0)
	)

	INSERT INTO @TEMP
	(
		numoppitemtCode
		,numItemCode
		,numUnitHour
		,numWarehouseItmsID
		,vcItemDesc
		,vcInstruction
		,bintCompliationDate
		,numWOAssignedTo
	)
	SELECT 
		numoppitemtCode
		,OpportunityItems.numItemCode
		,OpportunityBizDocItems.numUnitHour
		,OpportunityItems.numWarehouseItmsID
		,OpportunityItems.vcItemDesc
		,vcInstruction
		,bintCompliationDate
		,numWOAssignedTo
	FROM
		OpportunityBizDocItems
	INNER JOIN 
		OpportunityItems             
	ON
		OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtCode                                                  
	WHERE 
		numOppID=@numOppID
		AND numOppBizDocID = @numOppBizDocID
		AND ISNULL(bitWorkOrder,0)=1
		AND ISNULL(OpportunityBizDocItems.numUnitHour,0) > 0

	DECLARE @i INT = 1
	DECLARE @iCount AS INT

	SELECT @iCount=COUNT(*) FROM @TEMP

	WHILE @i <= @iCount
	BEGIN
		SELECT 
			@numItemCode=numItemCode
			,@numUnitHour=numUnitHour
			,@numWarehouseItmsID=numWarehouseItmsID
			,@vcItemDesc=vcItemDesc
			,@vcInstruction=vcInstruction
			,@bintCompliationDate=bintCompliationDate
			,@numWOAssignedTo=numWOAssignedTo
			,@numoppitemtCode=numoppitemtCode
		FROM 
			@TEMP                                                                         
		WHERE 
			ID=@i
				
		INSERT INTO WorkOrder
		(
			numItemCode
			,numQtyItemsReq
			,numWareHouseItemId
			,numCreatedBy
			,bintCreatedDate
			,numDomainID
			,numWOStatus
			,numOppId
			,vcItemDesc
			,vcInstruction
			,bintCompliationDate
			,numAssignedTo
			,numOppItemID
		)
		VALUES
		(
			@numItemCode
			,@numUnitHour
			,@numWarehouseItmsID
			,@numUserCntID
			,GETUTCDATE()
			,@numDomainID
			,0
			,@numOppID
			,@vcItemDesc
			,@vcInstruction
			,@bintCompliationDate
			,@numWOAssignedTo
			,@numoppitemtCode
		)
	
		SET @numWOId = SCOPE_IDENTITY()

		INSERT INTO [WorkOrderDetails] 
		(
			numWOId,numItemKitID,numChildItemID,numQtyItemsReq,numWareHouseItemId,vcItemDesc,sintOrder,numQtyItemsReq_Orig,numUOMId
		)
		SELECT 
			@numWOId,numItemKitID,numItemCode,
			CAST(((DTL.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(Dtl.numUOMID,item.numItemCode,item.numDomainID,item.numBaseUnit),1)) * @numUnitHour)AS FLOAT),
			isnull(Dtl.numWareHouseItemId,0),
			ISNULL(Dtl.vcItemDesc,txtItemDesc),
			ISNULL(sintOrder,0),
			DTL.numQtyItemsReq,
			Dtl.numUOMId 
		FROM 
			item                                
		INNER JOIN 
			ItemDetails Dtl 
		ON 
			numChildItemID=numItemCode
		WHERE 
			numItemKitID=@numItemCode

		SET @i = @i + 1
	END	
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO   
/* deducts Qty from allocation and Adds back to onhand */
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_RevertAllocationPickList')
DROP PROCEDURE USP_RevertAllocationPickList
GO
CREATE PROCEDURE [dbo].[USP_RevertAllocationPickList]
	@numDomainID NUMERIC(18,0)
	,@numOppID NUMERIC(18,0)
	,@numOppBizDocID NUMERIC(18,0)
	,@numUserCntID AS NUMERIC(18,0)
AS
BEGIN
	DECLARE @OppType AS VARCHAR(2)              
    DECLARE @itemcode AS NUMERIC       
    DECLARE @numWareHouseItemID AS NUMERIC(18,0)
	DECLARE @numWLocationID NUMERIC(18,0)                     
    DECLARE @numUnits AS FLOAT                                              
    DECLARE @onHand AS FLOAT                                            
    DECLARE @onOrder AS FLOAT                                            
    DECLARE @onBackOrder AS FLOAT                                              
    DECLARE @onAllocation AS FLOAT
    DECLARE @numQtyShipped AS FLOAT
    DECLARE @numUnitHourReceived AS FLOAT
	DECLARE @numDeletedReceievedQty AS FLOAT
    DECLARE @numoppitemtCode AS NUMERIC(9) 
    DECLARE @monAmount AS DECIMAL(20,5) 
    DECLARE @monAvgCost AS DECIMAL(20,5)   
    DECLARE @Kit AS BIT                                        
    DECLARE @bitKitParent BIT		
    DECLARE @description AS VARCHAR(100)
	DECLARE @bitWorkOrder AS BIT

	Declare @numRentalIN as FLOAT
	Declare @numRentalOut as FLOAT
	Declare @numRentalLost as FLOAT
	DECLARE @bitAsset as BIT

	DECLARE @j INT = 0
	DECLARE @ChildCount AS INT
	DECLARE @vcChildDescription AS VARCHAR(300)
	DECLARE @numChildAllocation AS FLOAT
	DECLARE @numChildBackOrder AS FLOAT
	DECLARE @numChildOnHand AS FLOAT
	DECLARE @numOppChildItemID AS INT
	DECLARE @numChildItemCode AS INT
	DECLARE @bitChildIsKit AS BIT
	DECLARE @numChildWarehouseItemID AS INT
	DECLARE @numChildQty AS FLOAT
	DECLARE @numChildQtyShipped AS FLOAT

	DECLARE @TempKitSubItems TABLE
	(
		ID INT,
		numOppChildItemID NUMERIC(18,0),
		numChildItemCode NUMERIC(18,0),
		bitChildIsKit BIT,
		numChildWarehouseItemID NUMERIC(18,0),
		numChildQty FLOAT,
		numChildQtyShipped FLOAT
	)

	DECLARE @k AS INT = 1
	DECLARE @CountKitChildItems AS INT
	DECLARE @vcKitChildDescription AS VARCHAR(300)
	DECLARE @numKitChildAllocation AS FLOAT
	DECLARE @numKitChildBackOrder AS FLOAT
	DECLARE @numKitChildOnHand AS FLOAT
	DECLARE @numOppKitChildItemID AS NUMERIC(18,0)
	DECLARE @numKitChildItemCode AS NUMERIC(18,0)
	DECLARE @numKitChildWarehouseItemID AS NUMERIC(18,0)
	DECLARE @numKitChildQty AS FLOAT
	DECLARE @numKitChildQtyShipped AS FLOAT

	DECLARE @TempKitChildKitSubItems TABLE
	(
		ID INT,
		numOppKitChildItemID NUMERIC(18,0),
		numKitChildItemCode NUMERIC(18,0),
		numKitChildWarehouseItemID NUMERIC(18,0),
		numKitChildQty FLOAT,
		numKitChildQtyShipped FLOAT
	)

	DECLARE @numFulfillmentBizDocFromPickList AS NUMERIC(18,0)
	SELECT @numFulfillmentBizDocFromPickList=numOppBizDocsId FROM OpportunityBizDocs WHERE numOppId=@numOppID AND numBizDocId=296 AND numSourceBizDocId=@numOppBizDocID
				
    SELECT TOP 1
            @numoppitemtCode = numoppitemtCode,
            @itemcode = OI.numItemCode,
            @numUnits = ISNULL(OBDI.numUnitHour,0),
            @numWareHouseItemID = ISNULL(OI.numWarehouseItmsID,0),
			@numWLocationID = ISNULL(numWLocationID,0),
            @Kit = ( CASE WHEN bitKitParent = 1
                               AND bitAssembly = 1 THEN 0
                          WHEN bitKitParent = 1 THEN 1
                          ELSE 0
                     END ),
            @monAmount = ISNULL(OBDI.monTotAmount,0) * (CASE WHEN ISNULL(fltExchangeRate,0)=0 THEN 1 ELSE fltExchangeRate END),
            @monAvgCost = ISNULL(monAverageCost,0),
            @numQtyShipped = ISNULL(OBDIFulfillment.numUnitHour,0),
            @numUnitHourReceived = ISNULL(numUnitHourReceived,0),
			@numDeletedReceievedQty = ISNULL(numDeletedReceievedQty,0),
            @bitKitParent=ISNULL(bitKitParent,0),
            @OppType = tintOppType,
		    @numRentalIN=ISNULL(oi.numRentalIN,0),
			@numRentalOut=Isnull(oi.numRentalOut,0),
			@numRentalLost=Isnull(oi.numRentalLost,0),
			@bitAsset =ISNULL(I.bitAsset,0),
			@bitWorkOrder = ISNULL(OI.bitWorkOrder,0)
    FROM  
		OpportunityBizDocItems OBDI
	LEFT JOIN
		OpportunityBizDocItems OBDIFulfillment
	ON
		OBDI.numOppItemID=OBDIFulfillment.numOppItemID
		AND OBDIFulfillment.numOppBizDocID = @numFulfillmentBizDocFromPickList
	INNER JOIN
		OpportunityItems OI
	ON
		OBDI.numOppItemID = OI.numoppitemtCode
	LEFT JOIN 
		WareHouseItems WI 
	ON 
		OBDI.numWarehouseItmsID=WI.numWareHouseItemID
	JOIN 
		dbo.OpportunityMaster OM 
	ON 
		OI.numOppId = OM.numOppId
    JOIN 
		Item I 
	ON 
		OI.numItemCode = I.numItemCode
    WHERE  
		(charitemtype='P' OR 1=(CASE
									WHEN tintOppType=1 
									THEN CASE WHEN ISNULL(I.bitKitParent,0) = 1 AND ISNULL(I.bitAssembly,0) = 1 THEN 0 WHEN ISNULL(I.bitKitParent,0) = 1 THEN 1 ELSE 0 END 
									ELSE 0 
								END)) 
		AND OBDI.numOppBizDocID=@numOppBizDocID
        AND ISNULL(OI.bitDropShip,0) = 0
		AND ISNULL(OBDI.numUnitHour,0) > 0 
    ORDER BY 
		OI.numoppitemtCode

	
    WHILE @numoppitemtCode > 0                                  
    BEGIN    
        IF @numWareHouseItemID>0
        BEGIN
			SELECT  
				@onHand = ISNULL(numOnHand, 0),
				@onAllocation = ISNULL(numAllocation, 0),
				@onOrder = ISNULL(numOnOrder, 0),
				@onBackOrder = ISNULL(numBackOrder, 0)
			FROM 
				WareHouseItems
			WHERE 
				numWareHouseItemID = @numWareHouseItemID                                               
        END
            
		SET @description='SO Pick List Deleted (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@numQtyShipped AS VARCHAR(10)) + ')'
  
        IF @Kit = 1 AND @numUnits <> @numQtyShipped
		BEGIN
			-- CLEAR DATA OF PREVIOUS ITERATION
			DELETE FROM @TempKitSubItems

			-- GET KIT SUB ITEMS DETAIL
			INSERT INTO @TempKitSubItems
			(
				ID,
				numOppChildItemID,
				numChildItemCode,
				bitChildIsKit,
				numChildWarehouseItemID,
				numChildQty,
				numChildQtyShipped
			)
			SELECT 
				ROW_NUMBER() OVER(ORDER BY numOppChildItemID),
				ISNULL(numOppChildItemID,0),
				ISNULL(I.numItemCode,0),
				ISNULL(I.bitKitParent,0),
				ISNULL(numWareHouseItemId,0),
				((ISNULL(numQtyItemsReq_Orig,0) * ISNULL(dbo.fn_UOMConversion(OKI.numUOMId,OKI.numChildItemID,@numDomainID,I.numBaseUnit),1)) * @numUnits),
				((ISNULL(numQtyItemsReq_Orig,0) * ISNULL(dbo.fn_UOMConversion(OKI.numUOMId,OKI.numChildItemID,@numDomainID,I.numBaseUnit),1)) * @numQtyShipped)
			FROM 
				OpportunityKitItems OKI 
			JOIN 
				Item I 
			ON 
				OKI.numChildItemID=I.numItemCode    
			WHERE 
				charitemtype='P' 
				AND ISNULL(numWareHouseItemId,0) > 0 
				AND OKI.numOppID=@numOppID 
				AND OKI.numOppItemID=@numoppitemtCode 

			SET @j = 1
			SELECT @ChildCount=COUNT(*) FROM @TempKitSubItems

			--LOOP OVER ALL CHILD ITEMS AND RELESE ALLOCATION
			WHILE @j <= @ChildCount
			BEGIN
				SELECT
					@numOppChildItemID=numOppChildItemID,
					@numChildItemCode=numChildItemCode,
					@bitChildIsKit=bitChildIsKit,
					@numChildWarehouseItemID=numChildWarehouseItemID,
					@numChildQty=numChildQty,
					@numChildQtyShipped=numChildQtyShipped
				FROM
					@TempKitSubItems
				WHERE 
					ID = @j

				SELECT @numChildOnHand=ISNULL(numOnHand,0) FROM WareHouseItems WHERE numWareHouseItemID=@numChildWarehouseItemID

				-- IF CHILD ITEM IS KIT THEN LOOK OUT FOR ITS CHILD ITEMS WHICH ARE STORED IN OpportunityKitChildItems TABLE
				IF @bitChildIsKit = 1
				BEGIN
					-- IF KIT IS ADDED WITHIN KIT THEN THIS SUB KIT HAS ITS OWN ITEM CONFIGURATION WHICH ID STORED IN OpportunityKitChildItems
					IF EXISTS (SELECT numOppKitChildItemID FROM OpportunityKitChildItems WHERE numOppID=@numOppId AND numOppItemID=@numoppitemtCode AND numOppChildItemID=@numOppChildItemID)
					BEGIN
						-- CLEAR DATA OF PREVIOUS ITERATION
						DELETE FROM @TempKitChildKitSubItems

						-- GET SUB KIT SUB ITEMS DETAIL
						INSERT INTO @TempKitChildKitSubItems
						(
							ID,
							numOppKitChildItemID,
							numKitChildItemCode,
							numKitChildWarehouseItemID,
							numKitChildQty,
							numKitChildQtyShipped
						)
						SELECT 
							ROW_NUMBER() OVER(ORDER BY numOppKitChildItemID),
							ISNULL(numOppKitChildItemID,0),
							ISNULL(OKCI.numItemID,0),
							ISNULL(OKCI.numWareHouseItemId,0),
							((ISNULL(OKCI.numQtyItemsReq_Orig,0) * ISNULL(dbo.fn_UOMConversion(OKCI.numUOMId,OKCI.numItemID,@numDomainID,I.numBaseUnit),1)) * @numChildQty),
							((ISNULL(OKCI.numQtyItemsReq_Orig,0) * ISNULL(dbo.fn_UOMConversion(OKCI.numUOMId,OKCI.numItemID,@numDomainID,I.numBaseUnit),1)) * @numChildQtyShipped)
						FROM 
							OpportunityKitChildItems OKCI 
						JOIN 
							Item I 
						ON 
							OKCI.numItemID=I.numItemCode    
						WHERE 
							charitemtype='P' 
							AND ISNULL(numWareHouseItemId,0) > 0 
							AND OKCI.numOppID=@numOppID 
							AND OKCI.numOppItemID=@numoppitemtCode 
							AND OKCI.numOppChildItemID = @numOppChildItemID

						SET @k = 1
						SELECT @CountKitChildItems=COUNT(*) FROM @TempKitChildKitSubItems

						WHILE @k <= @CountKitChildItems
						BEGIN
							SELECT
								@numOppKitChildItemID=numOppKitChildItemID,
								@numKitChildItemCode=numKitChildItemCode,
								@numKitChildWarehouseItemID=numKitChildWarehouseItemID,
								@numKitChildQty=numKitChildQty,
								@numKitChildQtyShipped=numKitChildQtyShipped
							FROM
								@TempKitChildKitSubItems
							WHERE 
								ID = @k

							SELECT @numKitChildAllocation=ISNULL(numAllocation,0),@numKitChildBackOrder = ISNULL(numBackOrder,0) FROM WareHouseItems WHERE numWareHouseItemID=@numKitChildWarehouseItemID

							SET @vcKitChildDescription = 'SO Pick List Child Kit Deleted (Qty:' + CAST(@numKitChildQty AS VARCHAR(10)) + ' Shipped:' +  CAST(@numKitChildQtyShipped AS VARCHAR(10)) + ')'

							IF @numKitChildQtyShipped > 0
							BEGIN
								SET @numKitChildQty = @numKitChildQty - @numKitChildQtyShipped
							END

							IF @numKitChildAllocation + @numKitChildBackOrder >= @numKitChildQty
							BEGIN
								UPDATE
									WarehouseItems
								SET 
									numOnHand = ISNULL(numOnHand,0) + (CASE WHEN ISNULL(numBackOrder,0) >= @numKitChildQty THEN 0 ELSE (@numKitChildQty - ISNULL(numBackOrder,0)) END)
									,numAllocation = (CASE WHEN ISNULL(numBackOrder,0) >= @numKitChildQty THEN ISNULL(numAllocation,0) ELSE ISNULL(numAllocation,0) - (@numKitChildQty - ISNULL(numBackOrder,0)) END)
									,numBackOrder = (CASE WHEN ISNULL(numBackOrder,0) >= @numKitChildQty THEN ISNULL(numBackOrder,0) - @numKitChildQty ELSE 0 END)
									,dtModified = GETDATE()
								WHERE
									numWareHouseItemID=@numKitChildWarehouseItemID
							END
							ELSE
							BEGIN
								RAISERROR ('INVALID_INVENTORY',16,1);
							END

							EXEC dbo.USP_ManageWareHouseItems_Tracking
								@numWareHouseItemID = @numKitChildWarehouseItemID,
								@numReferenceID = @numOppId,
								@tintRefType = 3,
								@vcDescription = @vcKitChildDescription,
								@numModifiedBy = @numUserCntID,
								@numDomainID = @numDomainID	 

							SET @k = @k + 1
						END

						SET @vcChildDescription = 'SO Pick List KIT Deleted (Qty:' + CAST(@numChildQty AS VARCHAR(10)) + ' Shipped:' +  CAST(@numChildQtyShipped AS VARCHAR(10)) + ')'

						EXEC dbo.USP_ManageWareHouseItems_Tracking
							@numWareHouseItemID = @numChildWarehouseItemID,
							@numReferenceID = @numOppId,
							@tintRefType = 3,
							@vcDescription = @vcChildDescription,
							@numModifiedBy = @numUserCntID,
							@numDomainID = @numDomainID	 
					END
				END
				ELSE
				BEGIN
					SELECT @numChildAllocation=ISNULL(numAllocation,0),@numChildBackOrder=ISNULL(numBackOrder,0) FROM WareHouseItems WHERE numWareHouseItemID=@numChildWarehouseItemID

					SET @vcChildDescription = 'SO Pick List Kit Deleted (Qty:' + CAST(@numChildQty AS VARCHAR(10)) + ' Shipped:' +  CAST(@numChildQtyShipped AS VARCHAR(10)) + ')'

					IF @numChildQtyShipped > 0
					BEGIN
						SET @numChildQty = @numChildQty - @numChildQtyShipped
					END

					IF @numChildAllocation + @numChildBackOrder >= @numChildQty
					BEGIN
						UPDATE
							WarehouseItems
						SET 
							numOnHand = ISNULL(numOnHand,0) + (CASE WHEN ISNULL(numBackOrder,0) >= @numChildQty THEN 0 ELSE (@numChildQty - ISNULL(numBackOrder,0)) END)
							,numAllocation = (CASE WHEN ISNULL(numBackOrder,0) >= @numChildQty THEN ISNULL(numAllocation,0) ELSE ISNULL(numAllocation,0) - (@numChildQty - ISNULL(numBackOrder,0)) END)
							,numBackOrder = (CASE WHEN ISNULL(numBackOrder,0) >= @numChildQty THEN ISNULL(numBackOrder,0) - @numChildQty ELSE 0 END)
							,dtModified = GETDATE() 
						WHERE
							numWareHouseItemID=@numChildWarehouseItemID
					END
					ELSE
					BEGIN
						RAISERROR ('INVALID_INVENTORY',16,1);
					END

					EXEC dbo.USP_ManageWareHouseItems_Tracking
						@numWareHouseItemID = @numChildWarehouseItemID,
						@numReferenceID = @numOppId,
						@tintRefType = 3,
						@vcDescription = @vcChildDescription,
						@numModifiedBy = @numUserCntID,
						@numDomainID = @numDomainID
				END

				SET @j = @j + 1
			END
		END
		ELSE IF @bitWorkOrder = 1 AND @numUnits <> @numQtyShipped
		BEGIN
			DECLARE @numWOID AS NUMERIC(18,0)
			DECLARE @numWOStatus AS NUMERIC(18,0)
			SELECT @numWOID=numWOId,@numWOStatus=numWOStatus FROM WorkOrder WHERE numOppId=@numOppId AND numOppItemID=@numoppitemtCode AND numItemCode=@itemcode AND numWareHouseItemId=@numWareHouseItemID

			SET @description='SO-WO Pick List Deleted (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@numQtyShipped AS VARCHAR(10)) + ')'

			IF @numQtyShipped > 0
			BEGIN
				SET @numUnits = @numUnits - @numQtyShipped
			END 

			--IF WORK ORDER IS NOT COMPLETED THAN REMOVE ON ORDER QUANTITY
			IF @numWOStatus <> 23184
			BEGIN
				IF @onOrder >= @numUnits
					SET @onOrder = @onOrder - @numUnits
				ELSE
					SET @onOrder = 0
			END

			-- IF BACKORDER QTY IS GREATER THEN QTY TO REVERT THEN
			-- DECREASE BACKORDER QTY BY QTY TO REVERT
			IF @numUnits < @onBackOrder 
			BEGIN
				SET @onBackOrder = @onBackOrder - @numUnits
			END 
			-- IF BACKORDER QTY IS LESS THEN OR EQUAL TO QTY TO REVERT THEN
			-- DECREASE QTY TO REVERT BY QTY OF BACK ORDER AND
			-- SET BACKORDER QTY TO 0
			ELSE IF @numUnits >= @onBackOrder 
			BEGIN
				SET @numUnits = @numUnits - @onBackOrder
				SET @onBackOrder = 0
                        
				--REMOVE ITEM FROM ALLOCATION 
				IF (@onAllocation - @numUnits) >= 0
					SET @onAllocation = @onAllocation - @numUnits
						
				--ADD QTY TO ONHAND
				SET @onHand = @onHand + @numUnits
			END

			UPDATE  
				WareHouseItems
			SET 
				numOnHand = @onHand ,
				numAllocation = @onAllocation,
				numBackOrder = @onBackOrder,
				numOnOrder = @onOrder,
				dtModified = GETDATE()
			WHERE 
				numWareHouseItemID = @numWareHouseItemID   

			--IF WORK ORDER IS NOT COMPLETED THAN DELETE WORK ORDER AND ITS CHILDS WORK ORDER
			IF @numWOStatus <> 23184
			BEGIN
				EXEC USP_DeleteAssemblyWOAndInventory @numDomainID,@numUserCntID,@numWOID,@numOppID,2
			END
		END	
		ELSE IF @numUnits <> @numQtyShipped
		BEGIN
			IF @numQtyShipped>0
			BEGIN
				SET @numUnits = @numUnits - @numQtyShipped
			END 

			IF @onAllocation + @onBackOrder >= @numUnits
			BEGIN
				UPDATE
					WarehouseItems
				SET 
					numOnHand = ISNULL(numOnHand,0) + (CASE WHEN ISNULL(numBackOrder,0) >= @numUnits THEN 0 ELSE (@numUnits - ISNULL(numBackOrder,0)) END)
					,numAllocation = (CASE WHEN ISNULL(numBackOrder,0) >= @numUnits THEN ISNULL(numAllocation,0) ELSE ISNULL(numAllocation,0) - (@numUnits - ISNULL(numBackOrder,0)) END)
					,numBackOrder = (CASE WHEN ISNULL(numBackOrder,0) >= @numUnits THEN ISNULL(numBackOrder,0) - @numUnits ELSE 0 END)
					,dtModified = GETDATE() 
				WHERE
					numWareHouseItemID=@numWareHouseItemID
			END
			ELSE
			BEGIN
				PRINT @numUnits
				PRINT @onAllocation
				PRINT @onBackOrder
				PRINT @numWareHouseItemID
				RAISERROR ('INVALID_INVENTORY',16,1);
			END          
		END
			
		IF @numUnits <> @numQtyShipped
		BEGIN
			IF @numWareHouseItemID>0 AND @description <> 'DO NOT ADD WAREHOUSE TRACKING'
			BEGIN 
					EXEC dbo.USP_ManageWareHouseItems_Tracking
					@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
					@numReferenceID = @numOppId, --  numeric(9, 0)
					@tintRefType = 3, --  tinyint
					@vcDescription = @description, --  varchar(100)
					@numModifiedBy = @numUserCntID,
					@numDomainID = @numDomainID
			END
		END

		
                                                                
		SELECT TOP 1
				@numoppitemtCode = numoppitemtCode,
				@itemcode = OI.numItemCode,
				@numUnits = ISNULL(OBDI.numUnitHour,0),
				@numWareHouseItemID = ISNULL(OI.numWarehouseItmsID,0),
				@numWLocationID = ISNULL(numWLocationID,0),
				@Kit = ( CASE WHEN bitKitParent = 1
								   AND bitAssembly = 1 THEN 0
							  WHEN bitKitParent = 1 THEN 1
							  ELSE 0
						 END ),
				@monAmount = ISNULL(OBDI.monTotAmount,0) * (CASE WHEN ISNULL(fltExchangeRate,0)=0 THEN 1 ELSE fltExchangeRate END),
				@monAvgCost = ISNULL(monAverageCost,0),
				@numQtyShipped = ISNULL(OBDIFulfillment.numUnitHour,0),
				@numUnitHourReceived = ISNULL(numUnitHourReceived,0),
				@numDeletedReceievedQty = ISNULL(numDeletedReceievedQty,0),
				@bitKitParent=ISNULL(bitKitParent,0),
				@OppType = tintOppType,
				@numRentalIN=ISNULL(oi.numRentalIN,0),
				@numRentalOut=Isnull(oi.numRentalOut,0),
				@numRentalLost=Isnull(oi.numRentalLost,0),
				@bitAsset =ISNULL(I.bitAsset,0),
				@bitWorkOrder = ISNULL(OI.bitWorkOrder,0)
		FROM  
			OpportunityBizDocItems OBDI
		LEFT JOIN
			OpportunityBizDocItems OBDIFulfillment
		ON
			OBDI.numOppItemID=OBDIFulfillment.numOppItemID
			AND OBDIFulfillment.numOppBizDocID = @numFulfillmentBizDocFromPickList
		INNER JOIN
			OpportunityItems OI
		ON
			OBDI.numOppItemID = OI.numoppitemtCode
		LEFT JOIN 
			WareHouseItems WI 
		ON 
			OBDI.numWarehouseItmsID=WI.numWareHouseItemID
		JOIN 
			dbo.OpportunityMaster OM 
		ON 
			OI.numOppId = OM.numOppId
		JOIN 
			Item I 
		ON 
			OI.numItemCode = I.numItemCode
		WHERE  
			(charitemtype='P' OR 1=(CASE
										WHEN tintOppType=1 
										THEN CASE WHEN ISNULL(I.bitKitParent,0) = 1 AND ISNULL(I.bitAssembly,0) = 1 THEN 0 WHEN ISNULL(I.bitKitParent,0) = 1 THEN 1 ELSE 0 END 
										ELSE 0 
									END)) 
			AND OBDI.numOppBizDocID=@numOppBizDocID
			AND ISNULL(OI.bitDropShip,0) = 0
			AND ISNULL(OBDI.numUnitHour,0) > 0 
			AND OI.numoppitemtCode > @numoppitemtCode
		ORDER BY 
			OI.numoppitemtCode
		                                           
		IF @@rowcount = 0 
			SET @numoppitemtCode = 0
	END
END
GO
/****** Object:  StoredProcedure [dbo].[USP_RevertDetailsOpp]    Script Date: 07/26/2008 16:20:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj      
/* deducts Qty from allocation and Adds back to onhand */
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_RevertDetailsOpp')
DROP PROCEDURE USP_RevertDetailsOpp
GO
CREATE PROCEDURE [dbo].[USP_RevertDetailsOpp] 
@numOppId NUMERIC,
@tintMode AS TINYINT=0, -- 0:Add/Edit, 1:Delete, 2: Demoted to opportunity
@numUserCntID AS NUMERIC(9)
AS ---reverting back to previous state if deal is being edited 
	DECLARE @numDomain AS NUMERIC(18,0)
	DECLARE @tintCommitAllocation AS TINYINT
	SELECT @numDomain = OM.numDomainID, @tintCommitAllocation=ISNULL(tintCommitAllocation,1)  FROM [dbo].[OpportunityMaster] AS OM INNER JOIN Domain D ON OM.numDomainId = D.numDomainId WHERE [OM].[numOppId] = @numOppID
    
	DECLARE @OppType AS VARCHAR(2)              
    DECLARE @itemcode AS NUMERIC       
    DECLARE @numWareHouseItemID AS NUMERIC
	DECLARE @numWLocationID NUMERIC(18,0)                     
    DECLARE @numToWarehouseItemID AS NUMERIC 
    DECLARE @numUnits AS FLOAT                                              
    DECLARE @onHand AS FLOAT                                            
    DECLARE @onOrder AS FLOAT                                            
    DECLARE @onBackOrder AS FLOAT                                              
    DECLARE @onAllocation AS FLOAT
    DECLARE @numQtyShipped AS FLOAT
    DECLARE @numUnitHourReceived AS FLOAT
	DECLARE @numDeletedReceievedQty AS FLOAT
    DECLARE @numoppitemtCode AS NUMERIC(9) 
    DECLARE @monAmount AS DECIMAL(20,5) 
    DECLARE @monAvgCost AS DECIMAL(20,5)   
    DECLARE @Kit AS BIT                                        
    DECLARE @bitKitParent BIT
    DECLARE @bitStockTransfer BIT 
    DECLARE @numOrigUnits AS FLOAT			
    DECLARE @description AS VARCHAR(100)
	DECLARE @bitWorkOrder AS BIT
	--Added by :Sachin Sadhu||Date:18thSept2014
	--For Rental/Asset Project
	Declare @numRentalIN as FLOAT
	Declare @numRentalOut as FLOAT
	Declare @numRentalLost as FLOAT
	DECLARE @bitAsset as BIT
	--end sachin

    						
    SELECT TOP 1
            @numoppitemtCode = numoppitemtCode,
            @itemcode = OI.numItemCode,
            @numUnits = ISNULL(numUnitHour,0),
            @numWareHouseItemID = ISNULL(numWarehouseItmsID,0),
			@numWLocationID = ISNULL(numWLocationID,0),
            @Kit = ( CASE WHEN bitKitParent = 1
                               AND bitAssembly = 1 THEN 0
                          WHEN bitKitParent = 1 THEN 1
                          ELSE 0
                     END ),
            @monAmount = ISNULL(monTotAmount,0) * (CASE WHEN ISNULL(fltExchangeRate,0)=0 THEN 1 ELSE fltExchangeRate END),
            @monAvgCost = ISNULL(monAverageCost,0),
            @numQtyShipped = ISNULL(numQtyShipped,0),
            @numUnitHourReceived = ISNULL(numUnitHourReceived,0),
			@numDeletedReceievedQty = ISNULL(numDeletedReceievedQty,0),
            @bitKitParent=ISNULL(bitKitParent,0),
            @numToWarehouseItemID =OI.numToWarehouseItemID,
            @bitStockTransfer = ISNULL(OM.bitStockTransfer,0),
            @OppType = tintOppType,
		    @numRentalIN=ISNULL(oi.numRentalIN,0),
			@numRentalOut=Isnull(oi.numRentalOut,0),
			@numRentalLost=Isnull(oi.numRentalLost,0),
			@bitAsset =ISNULL(I.bitAsset,0),
			@bitWorkOrder = ISNULL(OI.bitWorkOrder,0)
    FROM    OpportunityItems OI
			LEFT JOIN WareHouseItems WI ON OI.numWarehouseItmsID=WI.numWareHouseItemID
			JOIN dbo.OpportunityMaster OM ON OI.numOppId = OM.numOppId
            JOIN Item I ON OI.numItemCode = I.numItemCode
    WHERE   (charitemtype='P' OR 1=(CASE WHEN tintOppType=1 THEN 
							CASE WHEN ISNULL(I.bitKitParent,0) = 1 AND ISNULL(I.bitAssembly,0) = 1 THEN 0
								 WHEN ISNULL(I.bitKitParent,0) = 1 THEN 1
								 ELSE 0 END 
							ELSE 0 END)) AND OI.numOppId = @numOppId
                           AND ( bitDropShip = 0
                                 OR bitDropShip IS NULL
                               ) 
    ORDER BY OI.numoppitemtCode

	
    WHILE @numoppitemtCode > 0                                  
    BEGIN    
        SET @numOrigUnits=@numUnits
            
        IF @bitStockTransfer=1
        BEGIN
			SET @OppType = 1
		END
                 
        IF @numWareHouseItemID>0
        BEGIN                                
			SELECT  @onHand = ISNULL(numOnHand, 0),
					@onAllocation = ISNULL(numAllocation, 0),
					@onOrder = ISNULL(numOnOrder, 0),
					@onBackOrder = ISNULL(numBackOrder, 0)
			FROM    WareHouseItems
			WHERE   numWareHouseItemID = @numWareHouseItemID                                               
        END
            
        IF @OppType = 1 AND @tintCommitAllocation=1
        BEGIN
			IF @tintMode = 2
			BEGIN
				SET @description='SO DEMOTED TO OPPORTUNITY (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@numQtyShipped AS VARCHAR(10)) + ')'
			END
			ELSE
			BEGIN
				SET @description='SO Deleted (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@numQtyShipped AS VARCHAR(10)) + ')'
			END
                
            IF @Kit = 1
			BEGIN
				exec USP_UpdatingInventoryForEmbeddedKits @numoppitemtCode,@numOrigUnits,1,@numOppID,@tintMode,@numUserCntID
			END
			ELSE IF @bitWorkOrder = 1
			BEGIN
				DECLARE @numWOID AS NUMERIC(18,0)
				DECLARE @numWOStatus AS NUMERIC(18,0)
				SELECT @numWOID=numWOId,@numWOStatus=numWOStatus FROM WorkOrder WHERE numOppId=@numOppId AND numOppItemID=@numoppitemtCode AND numItemCode=@itemcode AND numWareHouseItemId=@numWareHouseItemID

				IF  @tintMode=2
				BEGIN
					SET @description='SO-WO Deal Lost (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@numQtyShipped AS VARCHAR(10)) + ')'
				END
				ELSE
				BEGIN
					SET @description='SO-WO Deleted (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@numQtyShipped AS VARCHAR(10)) + ')'
				END


				IF @numQtyShipped>0
				BEGIN
					IF @tintMode=0
						SET @numUnits = @numUnits - @numQtyShipped
					ELSE IF @tintmode=1 OR @tintMode=2
						SET @onAllocation = @onAllocation + @numQtyShipped 
				END 


				--IF WORK ORDER IS NOT COMPLETED THAN REMOVE ON ORDER QUANTITY
				IF @numWOStatus <> 23184
				BEGIN
					IF @onOrder >= @numUnits
						SET @onOrder = @onOrder - @numUnits
					ELSE
						SET @onOrder = 0
				END

				-- IF BACKORDER QTY IS GREATER THEN QTY TO REVERT THEN
				-- DECREASE BACKORDER QTY BY QTY TO REVERT
				IF @numUnits < @onBackOrder 
				BEGIN                  
					SET @onBackOrder = @onBackOrder - @numUnits
				END 
				-- IF BACKORDER QTY IS LESS THEN OR EQUAL TO QTY TO REVERT THEN
				-- DECREASE QTY TO REVERT BY QTY OF BACK ORDER AND
				-- SET BACKORDER QTY TO 0
				ELSE IF @numUnits >= @onBackOrder 
				BEGIN
					SET @numUnits = @numUnits - @onBackOrder
					SET @onBackOrder = 0
                        
					--REMOVE ITEM FROM ALLOCATION 
					IF (@onAllocation - @numUnits) >= 0
						SET @onAllocation = @onAllocation - @numUnits
						
					--ADD QTY TO ONHAND
					SET @onHand = @onHand + @numUnits
				END

				UPDATE  
					WareHouseItems
				SET 
					numOnHand = @onHand ,
					numAllocation = @onAllocation,
					numBackOrder = @onBackOrder,
					numOnOrder = @onOrder,
					dtModified = GETDATE()
				WHERE 
					numWareHouseItemID = @numWareHouseItemID   

				--IF WORK ORDER IS NOT COMPLETED THAN DELETE WORK ORDER AND ITS CHILDS WORK ORDER
				IF @numWOStatus <> 23184
				BEGIN
					EXEC USP_DeleteAssemblyWOAndInventory  @numDomain,@numUserCntID,@numWOID,@numOppID,1
				END
			END	
			ELSE
			BEGIN
				IF @numQtyShipped>0
				BEGIN
					IF @tintMode=0
						SET @numUnits = @numUnits - @numQtyShipped
					ELSE IF @tintmode=1 OR @tintMode=2
						SET @onAllocation = @onAllocation + @numQtyShipped 
				END 
								                    
                IF @numUnits >= @onBackOrder 
                BEGIN
                    SET @numUnits = @numUnits - @onBackOrder
                    SET @onBackOrder = 0
                            
                    IF (@onAllocation - @numUnits >= 0)
						SET @onAllocation = @onAllocation - @numUnits

					IF @bitAsset=1--Not Asset
					BEGIN
						SET @onHand = @onHand +@numRentalIN+@numRentalLost+@numRentalOut     
					END
					ELSE
					BEGIN
						SET @onHand = @onHand + @numUnits     
					END                                         
                END                                            
                ELSE IF @numUnits < @onBackOrder 
                BEGIN
					IF (@onBackOrder - @numUnits >0)
						SET @onBackOrder = @onBackOrder - @numUnits
                END 
                 	
				UPDATE
					WareHouseItems
				SET 
					numOnHand = @onHand ,
					numAllocation = @onAllocation,
					numBackOrder = @onBackOrder,
					dtModified = GETDATE()
				WHERE 
					numWareHouseItemID = @numWareHouseItemID              
			END
				

			IF @numWareHouseItemID>0 AND @description <> 'DO NOT ADD WAREHOUSE TRACKING'
			BEGIN 
					EXEC dbo.USP_ManageWareHouseItems_Tracking
					@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
					@numReferenceID = @numOppId, --  numeric(9, 0)
					@tintRefType = 3, --  tinyint
					@vcDescription = @description, --  varchar(100)
					@numModifiedBy = @numUserCntID,
					@numDomainID = @numDomain 
			END		
		END      
          
        IF @bitStockTransfer=1
        BEGIN
			SET @numWareHouseItemID = @numToWarehouseItemID;
			SET @OppType = 2
			SET @numUnits = @numOrigUnits

			SELECT
				@onHand = ISNULL(numOnHand, 0),
                @onAllocation = ISNULL(numAllocation, 0),
                @onOrder = ISNULL(numOnOrder, 0),
                @onBackOrder = ISNULL(numBackOrder, 0)
			FROM 
				WareHouseItems
			WHERE
				numWareHouseItemID = @numWareHouseItemID   
		END
          
        IF @OppType = 2 
        BEGIN 
			IF @tintMode = 2
			BEGIN
				SET @description='PO DEMOTED TO OPPORTUNITY (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Received:' +  CAST(@numUnitHourReceived AS VARCHAR(10)) + ' Deleted:' + CAST(@numDeletedReceievedQty AS VARCHAR(10)) + ')'
			END
			ELSE
			BEGIN
				SET @description='PO Deleted (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Received:' +  CAST(@numUnitHourReceived AS VARCHAR(10)) + ' Deleted:' + CAST(@numDeletedReceievedQty AS VARCHAR(10)) + ')'
			END

			IF @numWLocationID = -1 AND ISNULL(@bitStockTransfer,0)=0
			BEGIN
				IF @tintmode = 1 OR @tintMode=2
				BEGIN
					DECLARE @TEMPReceievedItems TABLE
					(
						ID INT,
						numOIRLID NUMERIC(18,0),
						numWarehouseItemID NUMERIC(18,0),
						numUnitReceieved FLOAT,
						numDeletedReceievedQty FLOAT
					)

					DELETE FROM @TEMPReceievedItems

					INSERT INTO
						@TEMPReceievedItems
					SELECT
						ROW_NUMBER() OVER(ORDER BY ID),
						ID,
						numWarehouseItemID,
						numUnitReceieved,
						ISNULL(numDeletedReceievedQty,0)
					FROM
						OpportunityItemsReceievedLocation 
					WHERE
						numDomainID=@numDomain
						AND numOppID=@numOppId
						AND numOppItemID=@numoppitemtCode

					DECLARE @i AS INT = 1
					DECLARE @COUNT AS INT
					DECLARE @numTempOIRLID NUMERIC(18,0)
					DECLARE @numTempOnHand FLOAT
					DECLARE @numTempWarehouseItemID NUMERIC(18,0)
					DECLARE @numTempUnitReceieved FLOAT
					DECLARE @numTempDeletedReceievedQty FLOAT

					SELECT @COUNT=COUNT(*) FROM @TEMPReceievedItems

					WHILE @i <= @COUNT
					BEGIN
						SELECT 
							@numTempOIRLID=TRI.numOIRLID,
							@numTempOnHand= ISNULL(numOnHand,0),
							@numTempWarehouseItemID=ISNULL(TRI.numWarehouseItemID,0),
							@numTempUnitReceieved=ISNULL(numUnitReceieved,0),
							@numTempDeletedReceievedQty=ISNULL(numDeletedReceievedQty,0)
						FROM 
							@TEMPReceievedItems TRI
						INNER JOIN
							WareHouseItems WI
						ON
							TRI.numWarehouseItemID=WI.numWareHouseItemID
						WHERE 
							ID=@i

						IF @numTempOnHand >= (@numTempUnitReceieved - @numTempDeletedReceievedQty)
						BEGIN
							UPDATE
								WareHouseItems
							SET
								numOnHand = ISNULL(numOnHand,0) - (@numTempUnitReceieved - @numTempDeletedReceievedQty),
								dtModified = GETDATE()
							WHERE
								numWareHouseItemID=@numTempWarehouseItemID
						END
						ELSE
						BEGIN
							RAISERROR('INSUFFICIENT_ONHAND_QTY',16,1)
							RETURN
						END

						IF @tintMode = 2
						BEGIN
							SET @description='PO DEMOTED TO OPPORTUNITY (Total Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Deleted Qty: ' + CAST((@numTempUnitReceieved - @numTempDeletedReceievedQty) AS VARCHAR(10)) + ' Received:' +  CAST(@numUnitHourReceived AS VARCHAR(10)) + ')'
						END
						ELSE
						BEGIN
							SET @description='PO Deleted (Total Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Deleted Qty: ' + CAST((@numTempUnitReceieved - @numTempDeletedReceievedQty) AS VARCHAR(10)) + ' Received:' +  CAST(@numUnitHourReceived AS VARCHAR(10)) + ')'
						END

						EXEC dbo.USP_ManageWareHouseItems_Tracking
							@numWareHouseItemID = @numTempWarehouseItemID,
							@numReferenceID = @numOppId,
							@tintRefType = 4,
							@vcDescription = @description,
							@numModifiedBy = @numUserCntID,
							@numDomainID = @numDomain

						DELETE FROM OpportunityItemsReceievedLocation WHERE ID=@numTempOIRLID

						SET @i = @i + 1
					END
				END

				SET @numUnits = @numUnits - @numUnitHourReceived

				IF (@onOrder - @numUnits)>=0
				BEGIN
					UPDATE
						WareHouseItems
					SET 
						numOnOrder = ISNULL(numOnOrder,0) - @numUnits,
						dtModified = GETDATE()
					WHERE 
						numWareHouseItemID = @numWareHouseItemID

					EXEC dbo.USP_ManageWareHouseItems_Tracking
						@numWareHouseItemID = @numWareHouseItemID,
						@numReferenceID = @numOppId,
						@tintRefType = 4,
						@vcDescription = @description,
						@numModifiedBy = @numUserCntID,
						@numDomainID = @numDomain
				END
				ELSE
				BEGIN
					RAISERROR('INSUFFICIENT_ONORDER_QTY',16,1)
					RETURN
				END
			END
			ELSE
			BEGIN
				--Partial Fulfillment
				IF (@tintmode=1 OR @tintMode=2) and  @onHand >= (@numUnitHourReceived - @numDeletedReceievedQty)
				BEGIN
					SET @onHand= @onHand - (@numUnitHourReceived - @numDeletedReceievedQty)
				END
						
				SET @numUnits = @numUnits - @numUnitHourReceived

				IF (@onOrder - @numUnits)>=0
				BEGIN
					--Causing Negative Inventory Bug ID:494
					SET @onOrder = @onOrder - @numUnits	
				END
				ELSE IF (@onHand + @onOrder) - @numUnits >= 0
				BEGIN						
					SET @onHand = @onHand - (@numUnits-@onOrder)
					SET @onOrder = 0
				END
				ELSE IF  (@onHand + @onOrder + @onAllocation) - @numUnits >= 0
				BEGIN
					Declare @numDiff numeric
	
					SET @numDiff = @numUnits - @onOrder
					SET @onOrder = 0

					SET @numDiff = @numDiff - @onHand
					SET @onHand = 0

					SET @onAllocation = @onAllocation - @numDiff
					SET @onBackOrder = @onBackOrder + @numDiff
				END
					    
				UPDATE
					WareHouseItems
				SET 
					numOnHand = @onHand,
					numAllocation = @onAllocation,
					numBackOrder = @onBackOrder,
					numOnOrder = @onOrder,
					dtModified = GETDATE()
				WHERE 
					numWareHouseItemID = @numWareHouseItemID   
                        
						

				EXEC dbo.USP_ManageWareHouseItems_Tracking
				@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
				@numReferenceID = @numOppId, --  numeric(9, 0)
				@tintRefType = 4, --  tinyint
				@vcDescription = @description, --  varchar(100)
				@numModifiedBy = @numUserCntID,
				@numDomainID = @numDomain
			END                                        
        END   
                                                                
        SELECT TOP 1
                @numoppitemtCode = numoppitemtCode,
                @itemcode = OI.numItemCode,
                @numUnits = numUnitHour,
                @numWareHouseItemID = ISNULL(numWarehouseItmsID,0),
				@numWLocationID = ISNULL(numWLocationID,0),
                @Kit = ( CASE WHEN bitKitParent = 1
                                    AND bitAssembly = 1 THEN 0
                                WHEN bitKitParent = 1 THEN 1
                                ELSE 0
                            END ),
                @monAmount = ISNULL(monTotAmount,0)  * (CASE WHEN ISNULL(fltExchangeRate,0)=0 THEN 1 ELSE fltExchangeRate END),
                @monAvgCost = monAverageCost,
                @numQtyShipped = ISNULL(numQtyShipped,0),
				@numUnitHourReceived = ISNULL(numUnitHourReceived,0),
				@numDeletedReceievedQty = ISNULL(numDeletedReceievedQty,0),
				@bitKitParent=ISNULL(bitKitParent,0),
				@numToWarehouseItemID =OI.numToWarehouseItemID,
				@bitStockTransfer = ISNULL(OM.bitStockTransfer,0),
				@OppType = tintOppType,
				@numRentalIN=ISNULL(oi.numRentalIN,0),
				@numRentalOut=Isnull(oi.numRentalOut,0),
				@numRentalLost=Isnull(oi.numRentalLost,0),
				@bitAsset =ISNULL(I.bitAsset,0),
				@bitWorkOrder = ISNULL(OI.bitWorkOrder,0)
        FROM    OpportunityItems OI
				LEFT JOIN WareHouseItems WI ON OI.numWarehouseItmsID=WI.numWareHouseItemID
				JOIN dbo.OpportunityMaster OM ON OI.numOppId = OM.numOppId
                JOIN Item I ON OI.numItemCode = I.numItemCode
                                   
        WHERE   (charitemtype='P' OR 1=(CASE WHEN tintOppType=1 THEN 
						CASE WHEN ISNULL(I.bitKitParent,0) = 1 AND ISNULL(I.bitAssembly,0) = 1 THEN 0
								WHEN ISNULL(I.bitKitParent,0) = 1 THEN 1
								ELSE 0 END 
						ELSE 0 END))  
						AND OI.numOppId = @numOppId 
                AND OI.numoppitemtCode > @numoppitemtCode
                AND ( bitDropShip = 0
                        OR bitDropShip IS NULL
                    )
        ORDER BY OI.numoppitemtCode                                              
        IF @@rowcount = 0 
            SET @numoppitemtCode = 0      
    END

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_SalesFulfillmentQueue_Get')
DROP PROCEDURE USP_SalesFulfillmentQueue_Get
GO
CREATE PROCEDURE [dbo].[USP_SalesFulfillmentQueue_Get]  
	
AS
BEGIN
	DELETE FROM SalesFulfillmentQueue WHERE numOppID NOT IN (SELECT numOppID FROM OpportunityMaster)


	-- FIRST DELETE ENTRIES FROM QUEUE WHICH ARE NOT VALID BECAUSE MASS FULFILLMENT RULE MAY BE CHANGED
	DELETE FROM 
		SalesFulfillmentQueue
	WHERE
		ISNULL(bitExecuted,0) = 0
		AND numSFQID NOT IN (
								SELECT 
									SFQ.numSFQID 
								FROM 
									SalesFulfillmentQueue SFQ
								INNER JOIN
									SalesFulfillmentConfiguration SFC
								ON
									SFQ.numDomainID = SFC.numDomainID
								WHERE
									(SFC.bitRule1IsActive = 1 AND SFC.tintRule1Type = 2 AND SFQ.numOrderStatus=SFC.numRule1OrderStatus)
									OR (SFC.bitRule2IsActive = 1 AND SFQ.numOrderStatus=SFC.numRule2OrderStatus)
									OR (SFC.bitRule3IsActive = 1 AND SFQ.numOrderStatus=SFC.numRule3OrderStatus)
									OR SFQ.numOrderStatus=-1
							)
	
	-- GET TOP 25 entries
	SELECT TOP 25 
		SFQ.numSFQID,
		SFQ.numDomainID,
		SFQ.numUserCntID,
		SFQ.numOppID,
		OM.numDivisionId,
		OM.numContactID,
		D.numCurrencyID,
		D.bitMinUnitPriceRule,
		OM.tintOppStatus,
		OM.tintshipped,
		D.numDefaultSalesShippingDoc,
		D.IsEnableDeferredIncome,
		D.bitAutolinkUnappliedPayment,
		ISNULL(OM.intUsedShippingCompany,ISNULL(D.numShipCompany,91)) numShipCompany,
		CASE 
			WHEN (SFC.bitRule1IsActive = 1 AND SFC.tintRule1Type = 2 AND SFQ.numOrderStatus=SFC.numRule1OrderStatus) THEN 1
			WHEN (SFC.bitRule2IsActive = 1 AND SFQ.numOrderStatus=SFC.numRule2OrderStatus) THEN 2
			WHEN (SFC.bitRule3IsActive = 1 AND SFQ.numOrderStatus=SFC.numRule3OrderStatus) THEN 3
			WHEN SFQ.numOrderStatus=-1 THEN 4
			ELSE 0
		END	AS tintRule
	FROM 
		SalesFulfillmentQueue SFQ
	INNER JOIN
		SalesFulfillmentConfiguration SFC
	ON
		SFQ.numDomainID = SFC.numDomainID
	INNER JOIN
		OpportunityMaster OM
	ON
		SFQ.numOppID = OM.numOppId
	INNER JOIN
		Domain D
	ON
		SFQ.numDomainID = D.numDomainID
	WHERE
		ISNULL(SFQ.bitExecuted,0) = 0
		AND ((SFC.bitRule1IsActive = 1 AND SFC.tintRule1Type = 2 AND SFQ.numOrderStatus=SFC.numRule1OrderStatus)
		OR (SFC.bitRule2IsActive = 1 AND SFQ.numOrderStatus=SFC.numRule2OrderStatus)
		OR (SFC.bitRule3IsActive = 1 AND SFQ.numOrderStatus=SFC.numRule3OrderStatus)
		OR SFQ.numOrderStatus=-1)
	
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_SalesFulfillmentQueue_Save')
DROP PROCEDURE USP_SalesFulfillmentQueue_Save
GO
CREATE PROCEDURE [dbo].[USP_SalesFulfillmentQueue_Save]      
	@numDomainId NUMERIC(18,0),
	@numUserCntId NUMERIC(18,0),
	@numOppID NUMERIC(18,0),
	@numStatus NUMERIC(18,0)
AS
BEGIN
	IF (
			SELECT 
				COUNT(*) 
			FROM 
				SalesFulfillmentConfiguration 
			WHERE 
				numDomainID=@numDomainId 
				AND (ISNULL(bitActive,0)=1 OR @numStatus=-1)
				AND 1 = (CASE 
							WHEN ISNULL(bitRule1IsActive,0)=1 AND tintRule1Type=2 AND numRule1OrderStatus=@numStatus THEN 1
							WHEN ISNULL(bitRule2IsActive,0)=1 AND numRule2OrderStatus=@numStatus THEN 1
							WHEN ISNULL(bitRule3IsActive,0)=1 AND numRule3OrderStatus=@numStatus THEN 1
							WHEN @numStatus=-1 THEN 1
							ELSE 0
						END)
		) > 0
	BEGIN
		IF (@numStatus <> -1 OR NOT EXISTS (SELECT numSFQID FROM SalesFulfillmentQueue WHERE numDomainID=@numDomainId AND numOppID=@numOppID AND numOrderStatus=-1 AND ISNULL(bitExecuted,0)=0))
		BEGIN
			INSERT INTO SalesFulfillmentQueue
			( 
				numDomainID
				,numUserCntID
				,numOppID
				,numOrderStatus
				,dtDate
				,bitExecuted
				,intNoOfTimesTried
			)
			VALUES
			(
				@numDomainId
				,@numUserCntId
				,@numOppID
				,@numStatus
				,GETUTCDATE()
				,0
				,0
			)
		END
	END
END
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_UpdateBillingTerms')
	DROP PROCEDURE USP_UpdateBillingTerms
GO

CREATE PROCEDURE [dbo].[USP_UpdateBillingTerms]
(
	 @numDomainID  numeric=0,                                          
     @numDivisionID  numeric=0, 
	 @numCompanyID numeric=0,
	 @numCompanyCredit numeric=0,                                   
	 @vcDivisionName  varchar (100)='',
	 @numUserCntID  numeric=0,                                                                                                                                     
	 @tintCRMType  tinyint=0,                                          
	 @tintBillingTerms as tinyint,                                          
	 @numBillingDays as numeric(9),                                         
	 @tintInterestType as tinyint,                                          
	 @fltInterest as float,                          
	 @bitNoTax as BIT,
	 @numCurrencyID AS numeric(9)=0,
	 @numDefaultPaymentMethod AS numeric(9)=0,         
	 @numDefaultCreditCard AS numeric(9)=0,         
	 @bitOnCreditHold AS bit=0,
	 @vcShipperAccountNo VARCHAR(100) = '',
	 @intShippingCompany INT = 0,
	 @bitEmailToCase BIT=0,
	 @numDefaultExpenseAccountID NUMERIC(18,0) = 0,
	 @numDefaultShippingServiceID NUMERIC(18,0),
	 @numAccountClass NUMERIC(18,0) = 0,
	 @tintPriceLevel INT = 0,
	 @bitShippingLabelRequired BIT,
	 @tintInbound850PickItem INT = 0,
	 @bitAllocateInventoryOnPickList BIT = 0
)
AS 
	BEGIN
	DECLARE @bitAllocateInventoryOnPickListOld AS TINYINT
	SELECT @bitAllocateInventoryOnPickListOld=ISNULL(bitAllocateInventoryOnPickList,0) FROM DivisionMaster WHERE numDivisionID=@numDivisionID


	IF @bitAllocateInventoryOnPickList <> @bitAllocateInventoryOnPickListOld AND (SELECT COUNT(*) FROM OpportunityMaster WHERE numDomainId=@numDomainID AND tintOppType=1 AND tintOppStatus=1 AND ISNULL(tintshipped,0)=0) > 0
	BEGIN
		RAISERROR('NOT_ALLOWED_CHANGE_ALLOCATE_INVENTORY_FROM_PACKING_SLIP_OPEN_SALES_ORDER',16,1)
		RETURN
	END

		UPDATE DivisionMaster 
		SET numCompanyID = @numCompanyID,
			vcDivisionName = @vcDivisionName,                                      
			numModifiedBy = @numUserCntID,                                            
			bintModifiedDate = GETUTCDATE(),                                         
			tintCRMType = @tintCRMType,                                          
			tintBillingTerms = @tintBillingTerms,                                          
			numBillingDays = @numBillingDays,                                         
			tintInterestType = @tintInterestType,                                           
			fltInterest =@fltInterest,                
			bitNoTax=@bitNoTax,
			numCurrencyID=@numCurrencyID,
			numDefaultPaymentMethod=@numDefaultPaymentMethod,                                                  
			numDefaultCreditCard=@numDefaultCreditCard,                                                 
			bitOnCreditHold=@bitOnCreditHold,
			vcShippersAccountNo = @vcShipperAccountNo,
			intShippingCompany = @intShippingCompany,
			numDefaultExpenseAccountID = @numDefaultExpenseAccountID,
			numDefaultShippingServiceID = @numDefaultShippingServiceID,
			numAccountClassID = ISNULL(@numAccountClass,0),
			tintPriceLevel = ISNULL(@tintPriceLevel,0),
			bitShippingLabelRequired=@bitShippingLabelRequired,
			tintInbound850PickItem = ISNULL(@tintInbound850PickItem,0),
			bitAllocateInventoryOnPickList=ISNULL(@bitAllocateInventoryOnPickList,0)
		WHERE numDivisionID = @numDivisionID AND numDomainId= @numDomainID     

		UPDATE CompanyInfo
		SET numCompanyCredit = @numCompanyCredit,               
			numModifiedBy = @numUserCntID,                
			bintModifiedDate = GETUTCDATE()               
		WHERE                 
			numCompanyId=@numCompanyId and numDomainID=@numDomainID    

	END
/****** Object:  StoredProcedure [dbo].[USP_UpdatedomainDetails]    Script Date: 05/07/2009 21:56:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                                      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_updatedomaindetails')
DROP PROCEDURE usp_updatedomaindetails
GO
CREATE PROCEDURE [dbo].[USP_UpdatedomainDetails]                                      
@numDomainID as numeric(9)=0,                                      
@vcDomainName as varchar(50)='',                                      
@vcDomainDesc as varchar(100)='',                                      
@bitExchangeIntegration as bit,                                      
@bitAccessExchange as bit,                                      
@vcExchUserName as varchar(100)='',                                      
@vcExchPassword as varchar(100)='',                                      
@vcExchPath as varchar(200)='',                                      
@vcExchDomain as varchar(100)='',                                    
@tintCustomPagingRows as tinyint,                                    
@vcDateFormat as varchar(30)='',                                    
@numDefCountry as numeric(9),                                    
@tintComposeWindow as tinyint,                                    
@sintStartDate as smallint,                                    
@sintNoofDaysInterval as smallint,                                    
@tintAssignToCriteria as tinyint,                                    
@bitIntmedPage as bit,                                    
@tintFiscalStartMonth as tinyint,                                      
@tintPayPeriod as tinyint,                        
@numCurrencyID as numeric(9) ,                
@charUnitSystem as char(1)='',              
@vcPortalLogo as varchar(100)='',            
@tintChrForComSearch as tinyint  ,            
@intPaymentGateWay as int, -- DO NOT UPDATE VALUE FROM THIS PARAMETER
@tintChrForItemSearch as tinyint,
@ShipCompany as numeric(9),
@bitMultiCurrency as bit,
@bitCreateInvoice AS tinyint = null,
@numDefaultSalesBizDocID AS NUMERIC(9)=NULL,
@numDefaultPurchaseBizDocID AS NUMERIC(9)=NULL,
@bitSaveCreditCardInfo AS BIT = NULL,
@intLastViewedRecord AS INT=10,
@tintCommissionType AS TINYINT,
@tintBaseTax AS TINYINT,
@bitEmbeddedCost AS BIT,
@numDefaultSalesOppBizDocId NUMERIC=NULL,
@tintSessionTimeOut TINYINT=1,
@tintDecimalPoints TINYINT,
@bitCustomizePortal BIT=0,
@tintBillToForPO TINYINT,
@tintShipToForPO TINYINT,
@tintOrganizationSearchCriteria TINYINT,
@bitDocumentRepositary BIT=0,
@tintComAppliesTo TINYINT=0,
@bitGtoBContact BIT=0,
@bitBtoGContact BIT=0,
@bitGtoBCalendar BIT=0,
@bitBtoGCalendar BIT=0,
@bitExpenseNonInventoryItem BIT=0,
@bitInlineEdit BIT=0,
@bitRemoveVendorPOValidation BIT=0,
@tintBaseTaxOnArea tinyint=0,
@bitAmountPastDue BIT=0,
@monAmountPastDue DECIMAL(20,5),
@bitSearchOrderCustomerHistory BIT=0,
@bitRentalItem as bit=0,
@numRentalItemClass as NUMERIC(9)=NULL,
@numRentalHourlyUOM as NUMERIC(9)=NULL,
@numRentalDailyUOM as NUMERIC(9)=NULL,
@tintRentalPriceBasedOn as tinyint=NULL,
@tintCalendarTimeFormat as tinyint=NULL,
@tintDefaultClassType as tinyint=NULL,
@tintPriceBookDiscount as tinyint=0,
@bitAutoSerialNoAssign as bit=0,
@bitIsShowBalance AS BIT = 0,
@numIncomeAccID NUMERIC(18, 0) = 0,
@numCOGSAccID NUMERIC(18, 0) = 0,
@numAssetAccID NUMERIC(18, 0) = 0,
@IsEnableProjectTracking AS BIT = 0,
@IsEnableCampaignTracking AS BIT = 0,
@IsEnableResourceScheduling AS BIT = 0,
@ShippingServiceItem  AS NUMERIC(9)=0,
@numSOBizDocStatus AS NUMERIC(9)=0,
@bitAutolinkUnappliedPayment AS BIT=0,
@numDiscountServiceItemID AS NUMERIC(9)=0,
@vcShipToPhoneNumber as VARCHAR(50)='',
@vcGAUserEmailId as VARCHAR(50)='',
@vcGAUserPassword as VARCHAR(50)='',
@vcGAUserProfileId as VARCHAR(50)='',
@bitDiscountOnUnitPrice AS BIT=0,
@intShippingImageWidth INT = 0,
@intShippingImageHeight INT = 0,
@numTotalInsuredValue NUMERIC(10,2) = 0,
@numTotalCustomsValue NUMERIC(10,2) = 0,
@vcHideTabs VARCHAR(1000) = '',
@bitUseBizdocAmount BIT = 0,
@bitDefaultRateType BIT = 0,
@bitIncludeTaxAndShippingInCommission  BIT = 0,
@bitPurchaseTaxCredit BIT=0,
@bitLandedCost BIT=0,
@vcLanedCostDefault VARCHAR(10)='',
@bitMinUnitPriceRule BIT = 0,
@numAbovePercent AS NUMERIC(18,2),
@numBelowPercent AS NUMERIC(18,2),
@numBelowPriceField AS NUMERIC(18,0),
@vcUnitPriceApprover AS VARCHAR(100),
@numDefaultSalesPricing AS TINYINT,
@bitReOrderPoint AS BIT = 0,
@numReOrderPointOrderStatus AS NUMERIC(18,0) = 0,
@numPODropShipBizDoc AS NUMERIC(18,0) = 0,
@numPODropShipBizDocTemplate AS NUMERIC(18,0) = 0,
@IsEnableDeferredIncome AS BIT = 0,
@bitApprovalforTImeExpense AS BIT=0,
@numCost AS Numeric(9,0)=0,
@intTimeExpApprovalProcess int=0,
@bitApprovalforOpportunity AS BIT=0,
@intOpportunityApprovalProcess int=0,
@numDefaultSalesShippingDoc NUMERIC(18,0)=0,
@numDefaultPurchaseShippingDoc NUMERIC(18,0)=0,
@bitchkOverRideAssignto AS BIT=0,
@vcPrinterIPAddress VARCHAR(15) = '',
@vcPrinterPort VARCHAR(5) = '',
@numDefaultSiteID NUMERIC(18,0) = 0,
@vcSalesOrderTabs VARCHAR(200)='',
@vcSalesQuotesTabs VARCHAR(200)='',
@vcItemPurchaseHistoryTabs VARCHAR(200)='',
@vcItemsFrequentlyPurchasedTabs VARCHAR(200)='',
@vcOpenCasesTabs VARCHAR(200)='',
@vcOpenRMATabs VARCHAR(200)='',
@bitSalesOrderTabs BIT=0,
@bitSalesQuotesTabs BIT=0,
@bitItemPurchaseHistoryTabs BIT=0,
@bitItemsFrequentlyPurchasedTabs BIT=0,
@bitOpenCasesTabs BIT=0,
@bitOpenRMATabs BIT=0,
@bitRemoveGlobalLocation BIT = 0,
@bitSupportTabs BIT=0,
@vcSupportTabs VARCHAR(200)='',
@numAuthorizePercentage NUMERIC(18,0)=0,
@bitEDI BIT = 0,
@bit3PL BIT = 0,
@bitAllowDuplicateLineItems BIT = 0,
@tintCommitAllocation TINYINT = 1,
@tintInvoicing TINYINT = 1
as                                      
BEGIN
BEGIN TRY
BEGIN TRANSACTION

	IF ISNULL(@bitMinUnitPriceRule,0) = 1 AND ((SELECT COUNT(*) FROM UnitPriceApprover WHERE numDomainID=@numDomainID) = 0 OR ISNULL(@bitApprovalforOpportunity,0)=0 OR ISNULL(@intOpportunityApprovalProcess,0) = 0)
	BEGIN
		RAISERROR('INCOMPLETE_MIN_UNIT_PRICE_CONFIGURATION',16,1)
		RETURN
	END

	If ISNULL(@bitApprovalforTImeExpense,0) = 1 AND (SELECT
														COUNT(*) 
													FROM
														UserMaster
													LEFT JOIN
														ApprovalConfig
													ON
														ApprovalConfig.numUserId = UserMaster.numUserDetailId
													WHERE
														UserMaster.numDomainID=@numDomainID
														AND numModule=1
														AND ISNULL(numGroupID,0) > 0
														AND ISNULL(bitActivateFlag,0) = 1
														AND ISNULL(numLevel1Authority,0) = 0
														AND ISNULL(numLevel2Authority,0) = 0
														AND ISNULL(numLevel3Authority,0) = 0
														AND ISNULL(numLevel4Authority,0) = 0
														AND ISNULL(numLevel5Authority,0) = 0) > 0
	BEGIN
		RAISERROR('INCOMPLETE_TIMEEXPENSE_APPROVAL',16,1)
		RETURN
	END

	DECLARE @tintCommitAllocationOld AS TINYINT
	SELECT @tintCommitAllocationOld=ISNULL(tintCommitAllocation,1) FROM Domain WHERE numDomainID=@numDomainID

	IF @tintCommitAllocation <> @tintCommitAllocationOld AND (SELECT COUNT(*) FROM OpportunityMaster WHERE numDomainId=@numDomainID AND tintOppType=1 AND tintOppStatus=1 AND ISNULL(tintshipped,0)=0) > 0
	BEGIN
		RAISERROR('NOT_ALLOWED_CHANGE_COMMIT_ALLOCATION_OPEN_SALES_ORDER',16,1)
		RETURN
	END
                                  
 update Domain                                       
   set                                       
   vcDomainName=@vcDomainName,                                      
   vcDomainDesc=@vcDomainDesc,                                      
   bitExchangeIntegration=@bitExchangeIntegration,                                      
   bitAccessExchange=@bitAccessExchange,                                      
   vcExchUserName=@vcExchUserName,                                      
   vcExchPassword=@vcExchPassword,                                      
   vcExchPath=@vcExchPath,                                      
   vcExchDomain=@vcExchDomain,                                    
   tintCustomPagingRows =@tintCustomPagingRows,                                    
   vcDateFormat=@vcDateFormat,                                    
   numDefCountry =@numDefCountry,                                    
   tintComposeWindow=@tintComposeWindow,                                    
   sintStartDate =@sintStartDate,                                    
   sintNoofDaysInterval=@sintNoofDaysInterval,                                    
   tintAssignToCriteria=@tintAssignToCriteria,                                    
   bitIntmedPage=@bitIntmedPage,                                    
   tintFiscalStartMonth=@tintFiscalStartMonth,                            
--   bitDeferredIncome=@bitDeferredIncome,                          
   tintPayPeriod=@tintPayPeriod,
   vcCurrency=isnull((select varCurrSymbol from Currency Where numCurrencyID=@numCurrencyID  ),'')  ,                      
  numCurrencyID=@numCurrencyID,                
  charUnitSystem= @charUnitSystem,              
  vcPortalLogo=@vcPortalLogo ,            
  tintChrForComSearch=@tintChrForComSearch  ,      
 tintChrForItemSearch=@tintChrForItemSearch,
 numShipCompany= @ShipCompany,
 bitMultiCurrency=@bitMultiCurrency,
 bitCreateInvoice= @bitCreateInvoice,
 [numDefaultSalesBizDocId] =@numDefaultSalesBizDocId,
 bitSaveCreditCardInfo=@bitSaveCreditCardInfo,
 intLastViewedRecord=@intLastViewedRecord,
 [tintCommissionType]=@tintCommissionType,
 [tintBaseTax]=@tintBaseTax,
 [numDefaultPurchaseBizDocId]=@numDefaultPurchaseBizDocID,
 bitEmbeddedCost=@bitEmbeddedCost,
 numDefaultSalesOppBizDocId=@numDefaultSalesOppBizDocId,
 tintSessionTimeOut=@tintSessionTimeOut,
 tintDecimalPoints=@tintDecimalPoints,
 bitCustomizePortal=@bitCustomizePortal,
 tintBillToForPO = @tintBillToForPO,
 tintShipToForPO = @tintShipToForPO,
 tintOrganizationSearchCriteria=@tintOrganizationSearchCriteria,
-- vcPortalName=@vcPortalName,
 bitDocumentRepositary=@bitDocumentRepositary,
 --tintComAppliesTo=@tintComAppliesTo,THIS FIELD IS REMOVED FROM GLOBAL SETTINGS
 bitGtoBContact=@bitGtoBContact,
 bitBtoGContact=@bitBtoGContact,
 bitGtoBCalendar=@bitGtoBCalendar,
 bitBtoGCalendar=@bitBtoGCalendar,
 bitExpenseNonInventoryItem=@bitExpenseNonInventoryItem,
 bitInlineEdit=@bitInlineEdit,
 bitRemoveVendorPOValidation=@bitRemoveVendorPOValidation,
tintBaseTaxOnArea=@tintBaseTaxOnArea,
bitAmountPastDue = @bitAmountPastDue,
monAmountPastDue = @monAmountPastDue,
bitSearchOrderCustomerHistory=@bitSearchOrderCustomerHistory,
bitRentalItem=@bitRentalItem,
numRentalItemClass=@numRentalItemClass,
numRentalHourlyUOM=@numRentalHourlyUOM,
numRentalDailyUOM=@numRentalDailyUOM,
tintRentalPriceBasedOn=@tintRentalPriceBasedOn,
tintCalendarTimeFormat=@tintCalendarTimeFormat,
tintDefaultClassType=@tintDefaultClassType,
tintPriceBookDiscount=@tintPriceBookDiscount,
bitAutoSerialNoAssign=@bitAutoSerialNoAssign,
bitIsShowBalance = @bitIsShowBalance,
numIncomeAccID = @numIncomeAccID,
numCOGSAccID = @numCOGSAccID,
numAssetAccID = @numAssetAccID,
IsEnableProjectTracking = @IsEnableProjectTracking,
IsEnableCampaignTracking = @IsEnableCampaignTracking,
IsEnableResourceScheduling = @IsEnableResourceScheduling,
numShippingServiceItemID = @ShippingServiceItem,
numSOBizDocStatus=@numSOBizDocStatus,
bitAutolinkUnappliedPayment=@bitAutolinkUnappliedPayment,
numDiscountServiceItemID=@numDiscountServiceItemID,
vcShipToPhoneNo = @vcShipToPhoneNumber,
vcGAUserEMail = @vcGAUserEmailId,
vcGAUserPassword = @vcGAUserPassword,
vcGAUserProfileId = @vcGAUserProfileId,
bitDiscountOnUnitPrice=@bitDiscountOnUnitPrice,
intShippingImageWidth = @intShippingImageWidth ,
intShippingImageHeight = @intShippingImageHeight,
numTotalInsuredValue = @numTotalInsuredValue,
numTotalCustomsValue = @numTotalCustomsValue,
vcHideTabs = @vcHideTabs,
bitUseBizdocAmount = @bitUseBizdocAmount,
bitDefaultRateType = @bitDefaultRateType,
bitIncludeTaxAndShippingInCommission = @bitIncludeTaxAndShippingInCommission,
/*,bitAllowPPVariance=@bitAllowPPVariance*/
bitPurchaseTaxCredit=@bitPurchaseTaxCredit,
bitLandedCost=@bitLandedCost,
vcLanedCostDefault=@vcLanedCostDefault,
bitMinUnitPriceRule = @bitMinUnitPriceRule,
numDefaultSalesPricing = @numDefaultSalesPricing,
numPODropShipBizDoc=@numPODropShipBizDoc,
numPODropShipBizDocTemplate=@numPODropShipBizDocTemplate,
IsEnableDeferredIncome=@IsEnableDeferredIncome,
bitApprovalforTImeExpense=@bitApprovalforTImeExpense,
numCost= @numCost,intTimeExpApprovalProcess=@intTimeExpApprovalProcess,
bitApprovalforOpportunity=@bitApprovalforOpportunity,intOpportunityApprovalProcess=@intOpportunityApprovalProcess,
 numDefaultPurchaseShippingDoc=@numDefaultPurchaseShippingDoc,numDefaultSalesShippingDoc=@numDefaultSalesShippingDoc,
 bitchkOverRideAssignto=@bitchkOverRideAssignto,
 vcPrinterIPAddress=@vcPrinterIPAddress,
 vcPrinterPort=@vcPrinterPort,
 numDefaultSiteID=ISNULL(@numDefaultSiteID,0),
  vcSalesOrderTabs=@vcSalesOrderTabs,
 vcSalesQuotesTabs=@vcSalesQuotesTabs,
 vcItemPurchaseHistoryTabs=@vcItemPurchaseHistoryTabs,
 vcItemsFrequentlyPurchasedTabs=@vcItemsFrequentlyPurchasedTabs,
 vcOpenCasesTabs=@vcOpenCasesTabs,
 vcOpenRMATabs=@vcOpenRMATabs,
 bitSalesOrderTabs=@bitSalesOrderTabs,
 bitSalesQuotesTabs=@bitSalesQuotesTabs,
 bitItemPurchaseHistoryTabs=@bitItemPurchaseHistoryTabs,
 bitItemsFrequentlyPurchasedTabs=@bitItemsFrequentlyPurchasedTabs,
 bitOpenCasesTabs=@bitOpenCasesTabs,
 bitOpenRMATabs=@bitOpenRMATabs,
 bitRemoveGlobalLocation=@bitRemoveGlobalLocation,
  bitSupportTabs=@bitSupportTabs,
 vcSupportTabs=@vcSupportTabs,
 numAuthorizePercentage=@numAuthorizePercentage,
 bitEDI=ISNULL(@bitEDI,0),
 bit3PL=ISNULL(@bit3PL,0),
 bitAllowDuplicateLineItems=ISNULL(@bitAllowDuplicateLineItems,0),
 tintCommitAllocation=ISNULL(@tintCommitAllocation,1),
 tintInvoicing = ISNULL(@tintInvoicing,1)
 where numDomainId=@numDomainID
COMMIT

 IF ISNULL(@bitRemoveGlobalLocation,0) = 1
 BEGIN
	DECLARE @TEMPGlobalWarehouse TABLE
	(
		ID INT IDENTITY(1,1)
		,numItemCode NUMERIC(18,0)
		,numWarehouseID NUMERIC(18,0)
		,numWarehouseItemID NUMERIC(28,0)
	)

	INSERT INTO @TEMPGlobalWarehouse
	(
		numItemCode
		,numWarehouseID
		,numWarehouseItemID
	)
	SELECT
		numItemID
		,numWareHouseID
		,numWareHouseItemID
	FROM
		WareHouseItems
	WHERE
		numDomainID=@numDomainID
		AND numWLocationID = -1
		AND ISNULL(numOnHand,0) = 0
		AND ISNULL(numOnOrder,0)=0
		AND ISNULL(numAllocation,0)=0
		AND ISNULL(numBackOrder,0)=0


	DECLARE @i AS INT = 1
	DECLARE @iCount AS INT
	DECLARE @numTempItemCode AS NUMERIC(18,0)
	DECLARE @numTempWareHouseID AS NUMERIC(18,0)
	DECLARE @numTempWareHouseItemID AS NUMERIC(18,0)
	SELECT @iCount=COUNT(*) FROM @TEMPGlobalWarehouse

	WHILE @i <= @iCount
	BEGIN
		SELECT @numTempItemCode=numItemCode,@numTempWareHouseID=numWarehouseID,@numTempWareHouseItemID=numWarehouseItemID FROM @TEMPGlobalWarehouse WHERE ID=@i

		BEGIN TRY
			EXEC USP_AddUpdateWareHouseForItems @numTempItemCode,  
												@numTempWareHouseID,
												@numTempWareHouseItemID,
												'',
												0,
												0,
												0,
												'',
												'',
												@numDomainID,
												'',
												'',
												'',
												0,
												3,
												0,
												0,
												NULL,
												0
		END TRY
		BEGIN CATCH

		END CATCH

		SET @i = @i + 1
	END
 END

END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH

END

GO
/****** Object:  StoredProcedure [dbo].[USP_UpdatingInventory]    Script Date: 06/01/2009 23:48:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj       
--exec usp_updatinginventory 0,181193,1,0,1       
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_updatinginventory')
DROP PROCEDURE usp_updatinginventory
GO
CREATE PROCEDURE [dbo].[USP_UpdatingInventory]        
	@byteMode AS TINYINT=0,  
	@numWareHouseItemID AS NUMERIC(18,0)=0,  
	@Units AS FLOAT,
	@numWOId AS NUMERIC(9)=0,
	@numUserCntID AS NUMERIC(9)=0,
	@numOppId AS NUMERIC(9)=0,
	@numAssembledItemID AS NUMERIC(18,0) = 0,
	@fltQtyRequiredForSingleBuild AS FLOAT = 0   
AS      
BEGIN TRY
BEGIN TRANSACTION
	DECLARE @Description AS VARCHAR(200)
	DECLARE @numItemCode NUMERIC(18)
	DECLARE @numDomain AS NUMERIC(18,0)
	DECLARE @ParentWOID AS NUMERIC(18,0)
	DECLARE @numDivisionID NUMERIC(18,0)

	DECLARE @tintCommitAllocation AS TINYINT
	DECLARE @bitAllocateInventoryOnPickList AS BIT

	SELECT @ParentWOID=numParentWOID FROM WorkOrder WHERE numWOId=@numWOId
	SELECT @numItemCode=numItemID,@numDomain = numDomainID from WareHouseItems where numWareHouseItemID = @numWareHouseItemID

	IF ISNULL(@numOppId,0) > 0
	BEGIN
		SELECT @tintCommitAllocation=ISNULL(tintCommitAllocation,1) FROM Domain WHERE numDomainId=@numDomain
		SELECT @bitAllocateInventoryOnPickList=ISNULL(bitAllocateInventoryOnPickList,0) FROM OpportunityMaster INNER JOIN DivisionMaster ON OpportunityMaster.numDivisionId=DivisionMaster.numDivisionID WHERE numOppId=@numOppId
	END
  
	IF @byteMode=0  -- Aseeembly Item
	BEGIN  
		DECLARE @CurrentAverageCost DECIMAL(20,5)
		DECLARE @TotalCurrentOnHand FLOAT
		DECLARE @newAverageCost DECIMAL(20,5)
	
		SELECT @CurrentAverageCost = (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE monAverageCost END) FROM dbo.Item WHERE numItemCode =@numItemCode 
		SELECT @TotalCurrentOnHand = ISNULL((SUM(numOnHand) + SUM(numAllocation)),0) FROM dbo.WareHouseItems WHERE numItemID =@numItemCode 
		PRINT @CurrentAverageCost
		PRINT @TotalCurrentOnHand

		IF @numWOId > 0
		BEGIN
			SELECT 
				@newAverageCost = SUM((numQtyItemsReq_Orig * dbo.fn_UOMConversion(WOD.numUOMID,I.numItemCode,I.numDomainId,I.numBaseUnit)) * I.monAverageCost )
			FROM 
				WorkOrderDetails WOD
			LEFT JOIN 
				dbo.Item I 
			ON 
				I.numItemCode = WOD.numChildItemID
			WHERE 
				WOD.numWOId = @numWOId
				AND I.charItemType = 'P'

			UPDATE WorkOrder SET monAverageCost=@newAverageCost WHERE numWOId=@numWOId
		END
		ELSE
		BEGIN
			SELECT 
				@newAverageCost = SUM((numQtyItemsReq * dbo.fn_UOMConversion(ID.numUOMID,I.numItemCode,I.numDomainId,I.numBaseUnit)) * I.monAverageCost )
			FROM 
				dbo.ItemDetails ID
			LEFT JOIN 
				dbo.Item I 
			ON 
				I.numItemCode = ID.numChildItemID
			WHERE 
				numItemKitID = @numItemCode
				AND I.charItemType = 'P'

			UPDATE AssembledItem SET monAverageCost=@newAverageCost WHERE ID=@numAssembledItemID
		END

		SET @newAverageCost = ((@CurrentAverageCost * @TotalCurrentOnHand)  + (@newAverageCost * @Units)) / (@TotalCurrentOnHand + @Units)
	
		UPDATE item SET monAverageCost = (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE @newAverageCost END) WHERE numItemCode = @numItemCode

		IF ISNULL(@numWOId,0) > 0
		BEGIN
			UPDATE 
				WareHouseItems 
			SET 
				numOnHand = ISNULL(numOnHand,0) + @Units
				,numOnOrder = (CASE WHEN @tintCommitAllocation=2 AND @bitAllocateInventoryOnPickList=0 AND @numOppId > 0 THEN numOnOrder ELSE (CASE WHEN ISNULL(numOnOrder,0) < @Units THEN 0 ELSE ISNULL(numOnOrder,0) - @Units END) END) 
				,dtModified = GETDATE()  
			WHERE 
				numWareHouseItemID=@numWareHouseItemID  
		END
		ELSE
		BEGIN
			UPDATE 
				WareHouseItems 
			SET 
				numOnHand = ISNULL(numOnHand,0) + @Units
				,dtModified = GETDATE() 
			WHERE 
				numWareHouseItemID=@numWareHouseItemID  
		END
	END  
	ELSE IF @byteMode=1  --Aseembly Child item
	BEGIN  
		DECLARE @onHand as FLOAT                                    
		DECLARE @onOrder as FLOAT                                    
		DECLARE @onBackOrder as FLOAT                                   
		DECLARE @onAllocation as FLOAT    
		DECLARE @monAverageCost AS DECIMAL(20,5)


		SELECT @monAverageCost= (CASE WHEN ISNULL(bitVirtualInventory,0)=1 THEN 0 ELSE ISNULL(monAverageCost,0) END) FROM Item WHERE  numItemCode=@numItemCode

		IF @numWOId > 0
		BEGIN
			UPDATE
				WorkOrderDetails
			SET
				monAverageCost=@monAverageCost
			WHERE
				numWOId=@numWOId
				AND numChildItemID=@numItemCode
				AND numWareHouseItemId=@numWareHouseItemID
		END
		ELSE
		BEGIN
			-- KEEP HISTRIY OF AT WHICH PRICE CHILD ITEM IS USED IN ASSEMBLY BUILDING
			-- USEFUL IN CALCULATING AVERAGECOST WHEN DISASSEMBLING ITEM
			INSERT INTO [dbo].[AssembledItemChilds]
			(
				numAssembledItemID,
				numItemCode,
				numQuantity,
				numWarehouseItemID,
				monAverageCost,
				fltQtyRequiredForSingleBuild
			)
			VALUES
			(
				@numAssembledItemID,
				@numItemCode,
				@Units,
				@numWareHouseItemID,
				@monAverageCost,
				@fltQtyRequiredForSingleBuild
			)
		END

		select                                     
			@onHand=isnull(numOnHand,0),                                    
			@onAllocation=isnull(numAllocation,0),                                    
			@onOrder=isnull(numOnOrder,0),                                    
			@onBackOrder=isnull(numBackOrder,0)                                     
		from 
			WareHouseItems 
		where 
			numWareHouseItemID=@numWareHouseItemID  


		IF ISNULL(@numWOId,0) > 0
		BEGIN
			IF @tintCommitAllocation=2 AND @bitAllocateInventoryOnPickList=0 AND @numOppId > 0 -- Allocation When Fulfillment Order (Packing Slip) is added
			BEGIN
				IF @onHand >= @Units
				BEGIN
					--RELEASE QTY FROM ON HAND
					SET @onHand=@onHand-@Units
				END
				ELSE
				BEGIN
					RAISERROR('NOTSUFFICIENTQTY_ONHAND',16,1)
				END
			END
			ELSE
			BEGIN
				IF @onAllocation >= @Units
				BEGIN
					--RELEASE QTY FROM ALLOCATION
					SET @onAllocation=@onAllocation-@Units
				END
				ELSE
				BEGIN
					RAISERROR('NOTSUFFICIENTQTY_ALLOCATION',16,1)
				END
			END
		END
		ELSE
		BEGIN
			--DECREASE QTY FROM ONHAND
			SET @onHand=@onHand-@Units
		END

		 --UPDATE INVENTORY
		UPDATE 
			WareHouseItems 
		SET      
			numOnHand=@onHand,
			numOnOrder=@onOrder,                         
			numAllocation=@onAllocation,
			numBackOrder=@onBackOrder,
			dtModified = GETDATE()                                    
		WHERE 
			numWareHouseItemID=@numWareHouseItemID   
	END

	IF @numWOId>0
	BEGIN
    
		DECLARE @numReferenceID NUMERIC(18,0)
		DECLARE @tintRefType NUMERIC(18,0)

		IF ISNULL(@numOppId,0)>0 --FROM SALES ORDER 
		BEGIN
			IF @byteMode = 0 --Aseeembly Item
				SET @Description = 'SO-WO Work Order Completed (Qty: ' + CONVERT(varchar(10),@Units) + ')'
			ELSE IF @byteMode =1 --Aseembly Child item
				IF ISNULL(@ParentWOID,0) = 0
				BEGIN
					SET @Description = 'Items Used In SO-WO (Qty: ' + CONVERT(varchar(10),@Units) + ')'
				END
				ELSE
				BEGIN
					SET @Description = 'Items Used In SO-WO Work Order (Qty: ' + CONVERT(varchar(10),@Units) + ')'
				END

			SET @numReferenceID = @numOppId
			SET @tintRefType = 3
		END
		ELSE --FROM CREATE ASSEMBLY SCREEN FROM ITEM
		BEGIN
			IF @byteMode = 0 --Aseeembly Item
				SET @Description = 'Work Order Completed (Qty: ' + CONVERT(varchar(10),@Units) + ')'
			ELSE IF @byteMode =1 --Aseembly Child item
				SET @Description = 'Items Used In Work Order (Qty: ' + CONVERT(varchar(10),@Units) + ')'

			SET @numReferenceID = @numWOId
			SET @tintRefType=2
		END

		EXEC dbo.USP_ManageWareHouseItems_Tracking
					@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
					@numReferenceID = @numReferenceID, --  numeric(9, 0)
					@tintRefType = @tintRefType, --  tinyint
					@vcDescription = @Description, --  varchar(100)
					@numModifiedBy = @numUserCntID,
					@numDomainID = @numDomain
	END
	ELSE if @byteMode=0
	BEGIN
	DECLARE @desc AS VARCHAR(100)
	SET @desc= 'Create Assembly:' + CONVERT(varchar(10),@Units) + ' Units';

	  EXEC dbo.USP_ManageWareHouseItems_Tracking
					@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
					@numReferenceID = @numItemCode, --  numeric(9, 0)
					@tintRefType = 1, --  tinyint
					@vcDescription = @desc, --  varchar(100)
					@numModifiedBy = @numUserCntID,
					@numDomainID = @numDomain
	END
	ELSE if @byteMode=1
	BEGIN
	SET @Description = 'Item Used in Assembly (Qty: ' + CONVERT(varchar(10),@Units) + ')' 

	EXEC dbo.USP_ManageWareHouseItems_Tracking
					@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
					@numReferenceID = @numItemCode, --  numeric(9, 0)
					@tintRefType = 1, --  tinyint
					@vcDescription = @Description, --  varchar(100)
					@numModifiedBy = @numUserCntID,
					@numDomainID = @numDomain
	END 
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SET @numOppID = 0

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_ValidateeWorkOrderInventory')
DROP PROCEDURE usp_ValidateeWorkOrderInventory
GO
CREATE PROCEDURE [dbo].[usp_ValidateeWorkOrderInventory]
    (
      @numWOId as numeric(9)=0,
	  @numWOStatus as numeric(9)
    )
AS 
BEGIN
   
	IF @numWOStatus=23184 
	BEGIN
		DECLARE @numOppID NUMERIC(18,0)
		DECLARE @tintCommitAllocation TINYINT
		SELECT @tintCommitAllocation=ISNULL(tintCommitAllocation,1),@numOppID=ISNULL(numOppId,0) FROM WorkOrder INNER JOIN Domain ON WorkOrder.numDomainID=Domain.numDomainID WHERE numWOId=@numWOId

		SELECT 
			WD.numQtyItemsReq
			,i.vcItemName
			,isnull(WHI.numOnHand,0) numOnHand
			,isnull(WHI.numAllocation,0) numAllocation 
		FROM 
			WorkOrderDetails WD 
		JOIN 
			Item I 
		ON
			WD.numChildItemID=i.numItemCode
		JOIN 
			WareHouseItems WHI 
		ON 
			WHI.numWareHouseItemID=WD.numWareHouseItemID  
		WHERE 
			numWOId=@numWOId 
			AND i.charItemType IN ('P') 
			AND 1 = (CASE 
						WHEN @numOppID > 0 AND @tintCommitAllocation = 2 
						THEN (CASE WHEN WD.numQtyItemsReq > ISNULL(WHI.numOnHand,0) THEN 1 ELSE 0 END) 
						ELSE (CASE WHEN WD.numQtyItemsReq > ISNULL(WHI.numAllocation,0) THEN 1 ELSE 0 END) 
					END)
	END

END


GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetItemAttributesEcommerce')
DROP PROCEDURE dbo.[USP_GetItemAttributesEcommerce]
GO
CREATE PROCEDURE [dbo].[USP_GetItemAttributesEcommerce]
(
	@numDomainID NUMERIC(9),
	@numWareHouseID AS NUMERIC(9) = 0,
    @FilterQuery AS VARCHAR(2000) = '',
    @numCategoryID AS NUMERIC(18) = 0
)
AS 
BEGIN
	SELECT DISTINCT
		CFW.Fld_id 
		,CFW.Fld_type 
		,CFW.Fld_label 
		,CFW.numlistid
		,CFW.bitAutoComplete
	FROM 
		CFW_Fld_Master CFW  
	JOIN 
		ItemGroupsDTL    
	ON 
		numOppAccAttrID=Fld_id    
	WHERE
		numDomainID=@numDomainID
		AND Grp_id=9
		AND tintType=2 
		AND numItemGroupID IN (SELECT DISTINCT numItemGroup FROM ItemCategory INNER JOIN Item ON ItemCategory.numItemID=Item.numItemCode  WHERE numCategoryID=@numCategoryID)
	ORDER BY 
		bitAutoComplete
END
