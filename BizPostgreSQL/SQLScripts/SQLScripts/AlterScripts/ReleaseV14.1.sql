/******************************************************************
Project: Release 14.1 Date: 14.SEP.2020
Comments: ALTER SCRIPTS
*******************************************************************/

/******************************************** SANDEEP *********************************************/

UPDATE GenericDocuments SET vcDocDesc='Hi ##CustomerContactName##<br /> Here is a summary of what we did today.<br /> <br /> <strong><span style="font-size: 16px; color: #3366ff;">Work Description:</span></strong> ##TaskTitle## ##TaskNotes## <strong><span style="font-size: 16px; color: #3366ff;">Duration &amp; Schedule:</span></strong> ##TaskDuration##,##TaskStartEndTime##, on ##TaskDate##<br /> <strong><span style="font-size: 16px; color: #3366ff;">Balance Remaining:</span></strong> ##TimeContractBalance##' WHERE VcFileName='#SYS#EMAIL_ALERT:TIMECONTRACT_USED'

ALTER TABLE BillDetails ADD numOppID NUMERIC(18,0)
ALTER TABLE BillDetails ADD numOppItemID NUMERIC(18,0)
-----------------------------

USE [Production.2014]
GO

/****** Object:  Table [dbo].[StagePercentageDetailsTaskNotes]    Script Date: 09-Sep-20 2:51:16 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[StagePercentageDetailsTaskNotes](
	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numTaskID] [numeric](18, 0) NOT NULL,
	[vcNotes] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_StagePercentageDetailsTaskNotes] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

ALTER TABLE [dbo].[StagePercentageDetailsTaskNotes]  WITH CHECK ADD  CONSTRAINT [FK_StagePercentageDetailsTaskNotes_StagePercentageDetailsTask] FOREIGN KEY([numTaskID])
REFERENCES [dbo].[StagePercentageDetailsTask] ([numTaskId])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[StagePercentageDetailsTaskNotes] CHECK CONSTRAINT [FK_StagePercentageDetailsTaskNotes_StagePercentageDetailsTask]
GO