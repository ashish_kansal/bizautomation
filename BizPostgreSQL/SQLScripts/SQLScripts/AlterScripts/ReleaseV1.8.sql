/******************************************************************
Project: Release 1.8 Date: 23.07.2013
Comments: 
*******************************************************************/

/*******************Kamal Script******************/
--/************************************************************************************************/
--/************************11_July_2013*******************************************************************/
--/************************************************************************************************/

ALTER TABLE dbo.OpportunityItems ADD
	monAvgCost money NULL
	
	
UPDATE OI SET OI.monAvgCost=I.monAverageCost FROM dbo.OpportunityItems OI JOIN Item I 
	      ON OI.numItemCode=I.numItemCode WHERE I.charItemType='P'
	      
-------******Opportunity to Opportunity/Order******------------
--SELECT * FROM EmailMergeModule
UPDATE EmailMergeModule SET vcModuleName='Opportunity/Order' WHERE numModuleID=2 	      


-------******Add Tracking Number to Email Template******------------
--SELECT EMF.* FROM   EmailMergeFields EMF
--        INNER JOIN dbo.EmailMergeModule EMM ON EMF.numModuleID = EMM.numModuleID
--WHERE   EMF.numModuleID = 2 AND EMM.tintModuleType=0 AND EMM.tintModuleType=ISNULL(EMF.tintModuleType,0)

--exec USP_GetMergeFieldsByModuleId @numModuleID=2,@tintMode=0,@tintModuleType=0

INSERT INTO dbo.EmailMergeFields (
	vcMergeField,
	vcMergeFieldValue,
	numModuleID,
	tintModuleType
) VALUES ( 
	/* vcMergeField - varchar(100) */ 'Tracking Numbers',
	/* vcMergeFieldValue - varchar(2000) */ '##TrackingNo##',
	/* numModuleID - numeric(18, 0) */ 2,
	/* tintModuleType - tinyint */ 0 ) 
	
--SELECT * FROM dbo.DycFieldMaster WHERE vcDbColumnName='numManagerID'
UPDATE DycFieldMaster SET vcListItemType='U' WHERE vcDbColumnName='numManagerID'
	
---------------BizDoc Template---------------------------------------------------------

--#OrganizationComments# -> #Customer/VendorOrganizationComments#--#OrganizationContactName# -> #Customer/VendorOrganizationContactName#--#OrganizationContactEmail# -> #Customer/VendorOrganizationContactEmail#--#OrganizationContactPhone# -> #Customer/VendorOrganizationContactPhone#--#OrganizationName# -> #Customer/VendorOrganizationName#--#OrganizationPhone# -> #Customer/VendorOrganizationPhone#----#OrganizationBillingAddress# -> SO : #EmployerBillToAddress#       PO : #Customer/VendorBillToAddress#--#BillTo# -> SO : #Customer/VendorBillToAddress#       PO : #EmployerBillToAddress#--#ChangeBillToAddress(Bill To)# -> SO : #Customer/VendorChangeBillToHeader       PO : #EmployerChangeBillToHeader--#ShipTo# -> SO : #Customer/VendorShipToAddress#       PO : #EmployerShipToAddress#--#ChangeShipToAddress(Ship To)# -> SO : #Customer/VendorChangeShipToHeader       PO : #EmployerChangeShipToHeader--#BillToAddressName# -> SO : #Customer/VendorBillToAddressName#       PO : #EmployerBillToAddressName#--#ShipToAddressName# -> SO : #Customer/VendorShipToAddressName#       PO : #EmployerShipToAddressName#--#BillToCompanyName# -> SO : #Customer/VendorBillToCompanyName#       PO : #EmployerBillToCompanyName#--#ShipToCompanyName# -> SO : #Customer/VendorShipToCompanyName#       PO : #EmployerShipToCompanyName#

ALTER TABLE BizDocTemplate ADD
txtNewBizDocTemplate TEXT NULL


--UPDATE BizDocTemplate SET txtBizDocTemplate= ISNULL(txtNewBizDocTemplate,'')
UPDATE BizDocTemplate SET txtNewBizDocTemplate = txtBizDocTemplate

------------------------------------------------------------------------

UPDATE BizDocTemplate SET txtBizDocTemplate = REPLACE(CAST(txtBizDocTemplate AS NVARCHAR(MAX)),
'#OrganizationComments#','#Customer/VendorOrganizationComments#')
WHERE txtBizDocTemplate LIKE '%#OrganizationComments#%' --AND numOppType = 1

UPDATE BizDocTemplate SET txtBizDocTemplate = REPLACE(CAST(txtBizDocTemplate AS NVARCHAR(MAX)),
'#OrganizationContactName#','#Customer/VendorOrganizationContactName#')
WHERE txtBizDocTemplate LIKE '%#OrganizationContactName#%' --AND numOppType = 1

UPDATE BizDocTemplate SET txtBizDocTemplate = REPLACE(CAST(txtBizDocTemplate AS NVARCHAR(MAX)),
'#OrganizationContactEmail#','#Customer/VendorOrganizationContactEmail#')
WHERE txtBizDocTemplate LIKE '%#OrganizationContactEmail#%' --AND numOppType = 1

UPDATE BizDocTemplate SET txtBizDocTemplate = REPLACE(CAST(txtBizDocTemplate AS NVARCHAR(MAX)),
'#OrganizationContactPhone#','#Customer/VendorOrganizationContactPhone#')
WHERE txtBizDocTemplate LIKE '%#OrganizationContactPhone#%' --AND numOppType = 1

UPDATE BizDocTemplate SET txtBizDocTemplate = REPLACE(CAST(txtBizDocTemplate AS NVARCHAR(MAX)),
'#OrganizationName#','#Customer/VendorOrganizationName#')
WHERE txtBizDocTemplate LIKE '%#OrganizationName#%' --AND numOppType = 1

UPDATE BizDocTemplate SET txtBizDocTemplate = REPLACE(CAST(txtBizDocTemplate AS NVARCHAR(MAX)),
'#OrganizationPhone#','#Customer/VendorOrganizationPhone#')
WHERE txtBizDocTemplate LIKE '%#OrganizationPhone#%' --AND numOppType = 1

------------------------------------------------------------------------

UPDATE BizDocTemplate SET txtBizDocTemplate = REPLACE(CAST(txtBizDocTemplate AS NVARCHAR(MAX)),
'#BillTo#','#Customer/VendorBillToAddress#')
WHERE txtBizDocTemplate LIKE '%#BillTo#%' AND numOppType = 1

UPDATE BizDocTemplate SET txtBizDocTemplate = REPLACE(CAST(txtBizDocTemplate AS NVARCHAR(MAX)),
'#ShipTo#','#Customer/VendorShipToAddress#')
WHERE txtBizDocTemplate LIKE '%#ShipTo#%' AND numOppType = 1

UPDATE BizDocTemplate SET txtBizDocTemplate = REPLACE(CAST(txtBizDocTemplate AS NVARCHAR(MAX)),
'#ChangeBillToAddress','#Customer/VendorChangeBillToHeader')
WHERE txtBizDocTemplate LIKE '%#ChangeBillToAddress%' AND numOppType = 1

UPDATE BizDocTemplate SET txtBizDocTemplate = REPLACE(CAST(txtBizDocTemplate AS NVARCHAR(MAX)),
'#ChangeShipToAddress','#Customer/VendorChangeShipToHeader')
WHERE txtBizDocTemplate LIKE '%#ChangeShipToAddress%' AND numOppType = 1

UPDATE BizDocTemplate SET txtBizDocTemplate = REPLACE(CAST(txtBizDocTemplate AS NVARCHAR(MAX)),
'#BillToAddressName#','#Customer/VendorBillToAddressName#')
WHERE txtBizDocTemplate LIKE '%#BillToAddressName#%' AND numOppType = 1

UPDATE BizDocTemplate SET txtBizDocTemplate = REPLACE(CAST(txtBizDocTemplate AS NVARCHAR(MAX)),
'#ShipToAddressName#','#Customer/VendorShipToAddressName#')
WHERE txtBizDocTemplate LIKE '%#ShipToAddressName#%' AND numOppType = 1

UPDATE BizDocTemplate SET txtBizDocTemplate = REPLACE(CAST(txtBizDocTemplate AS NVARCHAR(MAX)),
'#BillToCompanyName#','#Customer/VendorBillToCompanyName#')
WHERE txtBizDocTemplate LIKE '%#BillToCompanyName#%' AND numOppType = 1

UPDATE BizDocTemplate SET txtBizDocTemplate = REPLACE(CAST(txtBizDocTemplate AS NVARCHAR(MAX)),
'#ShipToCompanyName#','#Customer/VendorShipToCompanyName#')
WHERE txtBizDocTemplate LIKE '%#ShipToCompanyName#%' AND numOppType = 1


UPDATE BizDocTemplate SET txtBizDocTemplate = REPLACE(CAST(txtBizDocTemplate AS NVARCHAR(MAX)),
'#OrganizationBillingAddress#','#EmployerBillToAddress#')
WHERE txtBizDocTemplate LIKE '%#OrganizationBillingAddress#%' AND numOppType = 1

----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

UPDATE BizDocTemplate SET txtBizDocTemplate = REPLACE(CAST(txtBizDocTemplate AS NVARCHAR(MAX)),
'#BillTo#','#EmployerBillToAddress#')
WHERE txtBizDocTemplate LIKE '%#BillTo#%' AND numOppType = 2

UPDATE BizDocTemplate SET txtBizDocTemplate = REPLACE(CAST(txtBizDocTemplate AS NVARCHAR(MAX)),
'#ShipTo#','#EmployerShipToAddress#')
WHERE txtBizDocTemplate LIKE '%#ShipTo#%' AND numOppType = 2

UPDATE BizDocTemplate SET txtBizDocTemplate = REPLACE(CAST(txtBizDocTemplate AS NVARCHAR(MAX)),
'#ChangeBillToAddress','#EmployerChangeBillToHeader')
WHERE txtBizDocTemplate LIKE '%#ChangeBillToAddress%' AND numOppType = 2

UPDATE BizDocTemplate SET txtBizDocTemplate = REPLACE(CAST(txtBizDocTemplate AS NVARCHAR(MAX)),
'#ChangeShipToAddress','#EmployerChangeShipToHeader')
WHERE txtBizDocTemplate LIKE '%#ChangeShipToAddress%' AND numOppType = 2

UPDATE BizDocTemplate SET txtBizDocTemplate = REPLACE(CAST(txtBizDocTemplate AS NVARCHAR(MAX)),
'#BillToAddressName#','#EmployerBillToAddressName#')
WHERE txtBizDocTemplate LIKE '%#BillToAddressName#%' AND numOppType = 2

UPDATE BizDocTemplate SET txtBizDocTemplate = REPLACE(CAST(txtBizDocTemplate AS NVARCHAR(MAX)),
'#ShipToAddressName#','#EmployerShipToAddressName#')
WHERE txtBizDocTemplate LIKE '%#ShipToAddressName#%' AND numOppType = 2

UPDATE BizDocTemplate SET txtBizDocTemplate = REPLACE(CAST(txtBizDocTemplate AS NVARCHAR(MAX)),
'#BillToCompanyName#','#EmployerBillToCompanyName#')
WHERE txtBizDocTemplate LIKE '%#BillToCompanyName#%' AND numOppType = 2

UPDATE BizDocTemplate SET txtBizDocTemplate = REPLACE(CAST(txtBizDocTemplate AS NVARCHAR(MAX)),
'#ShipToCompanyName#','#EmployerShipToCompanyName#')
WHERE txtBizDocTemplate LIKE '%#ShipToCompanyName#%' AND numOppType = 2


UPDATE BizDocTemplate SET txtBizDocTemplate = REPLACE(CAST(txtBizDocTemplate AS NVARCHAR(MAX)),
'#OrganizationBillingAddress#','#Customer/VendorBillToAddress#')
WHERE txtBizDocTemplate LIKE '%#OrganizationBillingAddress#%' AND numOppType = 2


/*******************Manish Script******************/
/******************************************************************
Project: BACRMUI   Date: 18.Jul.2013
Comments: Add Vendor Minimum Qty for Mass Update
*******************************************************************/
BEGIN TRANSACTION

--SELECT * FROM dbo.DycFormField_Mapping WHERE vcFieldName = 'Vendor Minimum Qty' AND numFormID = 27
--SELECT * FROM dbo.DycFormField_Mapping WHERE numFormID = 29 AND vcFieldName = 'Vendor'

INSERT INTO dbo.DycFormField_Mapping 
(numModuleID,numFieldID,numDomainID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,[order],
 tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,
 bitAllowFiltering,bitRequired,numFormFieldID,intSectionID,bitAllowGridColor)
SELECT numModuleID,numFieldID,numDomainID,29,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,[order],
 tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,
 bitAllowFiltering,bitRequired,numFormFieldID,intSectionID,bitAllowGridColor
FROM dbo.DycFormField_Mapping WHERE vcFieldName = 'Vendor Minimum Qty' AND numFormID = 27
 
exec usp_GetUpdatableFieldList @numDomainID=1,@numFormID=29
ROLLBACK 
/******************************************************************
Project: BACRMUI   Date: 12.Jul.2013
Comments: Add Item Price Level for ItemList
*******************************************************************/
BEGIN TRANSACTION

INSERT INTO dbo.DycFormField_Mapping 
(numModuleID,numFieldID,numDomainID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,[order],
 tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,
 bitAllowFiltering,bitRequired,numFormFieldID,intSectionID,bitAllowGridColor)
SELECT numModuleID,numFieldID,numDomainID,21,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,[order],
	   tintRow,tintColumn,bitInResults,0,bitDefault,1,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,
	   bitAllowFiltering,bitRequired,numFormFieldID,intSectionID,bitAllowGridColor
FROM dbo.DycFormField_Mapping WHERE numFormID = 20 AND vcFieldName = 'Price Level'	   
 
exec USP_GetColumnConfiguration1 @numDomainID=1,@numUserCntId=1,@FormId=21,@numtype=0,@numViewID=0
-- exec usp_getFormConfigFields @numDomainId=1,@numFormId=29,@numAuthGroupId=1,@numBizDocTemplateID=0

ROLLBACK 

/******************************************************************
Project: BACRMUI   Date: 10.Jul.2013
Comments: Add Documents into Case 
*******************************************************************/
BEGIN TRANSACTION

INSERT INTO dbo.DycFieldMaster 
(numModuleID,numDomainID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,
vcToolTip,vcListItemType,numListID,PopupFunctionName,[order],tintRow,tintColumn,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,
bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,bitAllowFiltering,bitInlineEdit,bitRequired,intColumnWidth,intFieldMaxLength) 
SELECT 4,NULL,'Documents','DocumentCount','DocumentCount','DocCount','Cases','V','R','Popup',
NULL,'',0,'OpenDocuments',	9,	3,	1,	1,	0,	0,	0,	0,	NULL,	
1,	0,	1,	NULL,	NULL,	NULL,	0,	NULL,	NULL,	NULL

DECLARE @numFieldID AS BIGINT
SELECT @numFieldID = numFieldId FROM dbo.DycFieldMaster WHERE vcFieldName = 'Documents' AND vcDbColumnName = 'DocumentCount' AND vcLookBackTableName = 'Cases'

INSERT INTO dbo.DycFormField_Mapping 
(numModuleID,numFieldID,numDomainID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,[order],
 tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,
 bitAllowFiltering,bitRequired,numFormFieldID,intSectionID,bitAllowGridColor)
SELECT 7,@numFieldID,NULL,12,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,[order],
	   tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,1,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,
	   bitAllowFiltering,bitRequired,0,1,NULL
FROM dbo.DycFieldMaster WHERE vcFieldName = 'Documents' AND vcDbColumnName = 'DocumentCount' AND vcLookBackTableName = 'Cases'


SELECT * FROM dbo.DycFieldMaster WHERE vcFieldName = 'Documents' AND vcDbColumnName = 'DocumentCount' AND vcLookBackTableName = 'Cases'
--DELETE FROM dbo.DycFieldMaster WHERE vcFieldName = 'Documents' AND vcDbColumnName = 'DocumentCount' AND vcLookBackTableName = 'Cases'

SELECT * FROM dbo.DycFormField_Mapping WHERE vcFieldName = 'Documents' AND numFormID = 26
--DELETE FROM dbo.DycFormField_Mapping WHERE vcFieldName = 'Documents' AND numFormID = 26

ROLLBACK 

/******************************************************************
Project: BACRMUI   Date: 5.Jul.2013
Comments: Add Description for New Case in Administration->Field Management
*******************************************************************/
BEGIN TRANSACTION

UPDATE dbo.DycFormField_Mapping SET bitAllowEdit = 1,bitInlineEdit = 1 ,bitAddField = 1, bitDetailField = 1,bitDefault = 1 WHERE numFormID = 12 AND  vcFieldName  = 'Description'
 
exec usp_GetLayoutInfoDdl @numUserCntId=0,@intcoulmn=0,@numDomainID=1,@PageId=3,@numRelation=0,@numFormID=12,@tintPageType=2

ROLLBACK 



/*******************Joseph Script******************/

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_CartItems
	(
	numCartId numeric(18, 0) NOT NULL IDENTITY (1, 1) NOT FOR REPLICATION,
	numUserCntId numeric(18, 0) NOT NULL,
	numDomainId numeric(18, 0) NOT NULL,
	vcCookieId varchar(100) NOT NULL,
	numOppItemCode numeric(18, 0) NULL,
	numItemCode numeric(18, 0) NULL,
	numUnitHour numeric(18, 0) NULL,
	monPrice money NULL,
	numSourceId numeric(18, 0) NULL,
	vcItemDesc varchar(2000) NULL,
	numWarehouseId numeric(18, 0) NULL,
	vcItemName varchar(200) NULL,
	vcWarehouse varchar(200) NULL,
	numWarehouseItmsID numeric(18, 0) NULL,
	vcItemType varchar(200) NULL,
	vcAttributes varchar(100) NULL,
	vcAttrValues varchar(100) NULL,
	bitFreeShipping bit NULL,
	numWeight numeric(18, 2) NULL,
	tintOpFlag tinyint NULL,
	bitDiscountType bit NULL,
	fltDiscount decimal(18, 2) NULL,
	monTotAmtBefDiscount money NULL,
	ItemURL varchar(200) NULL,
	numUOM numeric(18, 0) NULL,
	vcUOMName varchar(200) NULL,
	decUOMConversionFactor decimal(18, 0) NULL,
	numHeight numeric(18, 0) NULL,
	numLength numeric(18, 0) NULL,
	numWidth numeric(18, 0) NULL,
	vcShippingMethod varchar(200) NULL,
	numServiceTypeId numeric(18, 0) NULL,
	decShippingCharge money NULL,
	numShippingCompany numeric(18, 0) NULL,
	tintServicetype tinyint NULL,
	dtDeliveryDate datetime NULL,
	monTotAmount money NULL
	)  ON [PRIMARY]
GO
SET IDENTITY_INSERT dbo.Tmp_CartItems ON
GO
IF EXISTS(SELECT * FROM dbo.CartItems)
	 EXEC('INSERT INTO dbo.Tmp_CartItems (numCartId, numUserCntId, numDomainId, vcCookieId, numOppItemCode, numItemCode, numUnitHour, monPrice, numSourceId, vcItemDesc, numWarehouseId, vcItemName, vcWarehouse, numWarehouseItmsID, vcItemType, vcAttributes, vcAttrValues, bitFreeShipping, numWeight, tintOpFlag, bitDiscountType, fltDiscount, monTotAmtBefDiscount, ItemURL, numUOM, vcUOMName, decUOMConversionFactor, numHeight, numLength, numWidth, vcShippingMethod, numServiceTypeId, decShippingCharge, numShippingCompany, tintServicetype, dtDeliveryDate, monTotAmount)
		SELECT numCartId, numUserCntId, numDomainId, vcCookieId, numOppItemCode, numItemCode, numUnitHour, monPrice, numSourceId, vcItemDesc, numWarehouseId, vcItemName, vcWarehouse, numWarehouseItmsID, vcItemType, vcAttributes, vcAttrValues, bitFreeShipping, numWeight, tintOpFlag, bitDiscountType, fltDiscount, monTotAmtBefDiscount, ItemURL, numUOM, vcUOMName, decUOMConversionFactor, numHeight, numLength, numWidth, vcShippingMethod, numServiceTypeId, decShippingCharge, numShippingCompany, tintServicetype, dtDeliveryDate, monTotAmount FROM dbo.CartItems WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_CartItems OFF
GO
DROP TABLE dbo.CartItems
GO
EXECUTE sp_rename N'dbo.Tmp_CartItems', N'CartItems', 'OBJECT' 
GO
ALTER TABLE dbo.CartItems ADD CONSTRAINT
	PK_CartDetail PRIMARY KEY CLUSTERED 
	(
	numCartId,
	numUserCntId,
	numDomainId,
	vcCookieId
	) WITH( PAD_INDEX = OFF, FILLFACTOR = 80, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
COMMIT

CREATE TABLE [dbo].[WorkflowAutomation](
	[numAutomationID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numRuleID] [numeric](18, 0) NOT NULL,
	[numBizDocTypeId] [numeric](18, 0) NULL,
	[numBizDocTemplate] [numeric](18, 0) NULL,
	[numBizDocStatus1] [numeric](18, 0) NULL,
	[numBizDocStatus2] [numeric](18, 0) NULL,
	[numOppSource] [numeric](18, 0) NULL,
	[bitUpdatedTrackingNo] [bit] NULL,
	[tintOppType] [numeric](18, 0) NULL,
	[numOrderStatus] [numeric](18, 0) NULL,
	[numDomainID] [numeric](18, 0) NULL,
	[numUserCntID] [numeric](18, 0) NULL,
	[numEmailTemplate] [numeric](18, 0) NULL,
	[numBizDocCreatedFrom] [numeric](18, 0) NULL,
	[numOpenRecievePayment] [bit] NULL,
	[numCreditCardOption] [int] NULL,
	[dtCreated] [datetime] NOT NULL CONSTRAINT [DF_WorkflowAutomation_dtCreated]  DEFAULT (getdate()),
	[dtModified] [datetime] NULL,
	[bitIsActive] [bit] NULL CONSTRAINT [DF_Table_1_bitIsAvtive]  DEFAULT ((1)),
 CONSTRAINT [PK_WorkflowAutomation] PRIMARY KEY CLUSTERED 
(
	[numAutomationID] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

INSERT INTO ListDetails(numListID,vcData,numCreatedBY, bintCreatedDate,bitDelete,numDomainId, constFlag, sintOrder) 
values(31,'Paypal',1,GETDATE(),0,1,1,0)

INSERT INTO PaymentGateway(vcGateWayName, vcFirstFldName, vcSecndFldName, vcThirdFldName, vcToolTip)
VALUES ('PayPal','Username','Password','Signature','Used to receive Paypal Payment from Shopping cart')

ALTER TABLE eCommerceDTL ADD
vcPaypalUserName VARCHAR(50) NULL


ALTER TABLE eCommerceDTL ADD
vcPaypalPassword VARCHAR(50) NULL


ALTER TABLE eCommerceDTL ADD
vcPaypalSignature VARCHAR(500) NULL


ALTER TABLE eCommerceDTL ADD
IsPaypalSandbox BIT NULL


INSERT INTO dbo.ErrorMaster (
	vcErrorCode,
	vcErrorDesc,
	tintApplication
) VALUES ( 
	/* vcErrorCode - varchar(6) */ 'ERR073',
	/* vcErrorDesc - nvarchar(max) */ N'Paypal Merchant Configuration not found. Please contact the Merchant',
	/* tintApplication - tinyint */ 3 )  

INSERT INTO dbo.ErrorMaster (
	vcErrorCode,
	vcErrorDesc,
	tintApplication
) VALUES ( 
	/* vcErrorCode - varchar(6) */ 'ERR074',
	/* vcErrorDesc - nvarchar(max) */ N'Invalid Paypal User Email Or Invalid Merchant Paypal Account Configuration. Please contact the Merchant',
	/* tintApplication - tinyint */ 3 )  
INSERT INTO dbo.ErrorMaster (
	vcErrorCode,
	vcErrorDesc,
	tintApplication
) VALUES ( 
	/* vcErrorCode - varchar(6) */ 'ERR075',
	/* vcErrorDesc - nvarchar(max) */ N'Sorry!., There is a problem in redirecting to Paypal..',
	/* tintApplication - tinyint */ 3 )  
INSERT INTO dbo.ErrorMaster (
	vcErrorCode,
	vcErrorDesc,
	tintApplication
) VALUES ( 
	/* vcErrorCode - varchar(6) */ 'ERR076',
	/* vcErrorDesc - nvarchar(max) */ N'Sorry!., Unable to process your Paypal Payment..Paypal Payment Failure.',
	/* tintApplication - tinyint */ 3 )  



<add key="PaypalServerUrl" value="https://www.paypal.com/cgi-bin/webscr" />
  <add key="PaypalSandboxUrl" value="https://api-3t.sandbox.paypal.com/nvp" />
  <add key="PaypalRedirectSandboxUrl" value="https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_express-checkout&amp;token=" />
  <add key="PaypalRedirectServerUrl" value="https://www.paypal.com/cgi-bin/webscr?cmd=_express-checkout&amp;token=" />