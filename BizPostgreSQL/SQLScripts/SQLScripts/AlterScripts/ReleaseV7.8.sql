/******************************************************************
Project: Release 7.8 Date: 27.JULY.2017
Comments: ALTER SCRIPTS
*******************************************************************/

/******************   SANDEEP  *****************/

BEGIN TRY
BEGIN TRANSACTION

DECLARE @numFormFieldID NUMERIC(18,0)

INSERT INTO DynamicFormFieldMaster
(
	numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,vcListItemType,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,bitDefault,bitInResults
)
VALUES
(
	29,'Item Category','R','Label','numCategoryID','',0,0,'N','numCategoryID',1,'category',0,1
)

SET @numFormFieldID = SCOPE_IDENTITY()

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitAllowFiltering,numFormFieldID,intSectionID
)
VALUES
(
	4,311,29,1,1,'Item Category','Label',1,0,0,1,@numFormFieldID,1
)


COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH