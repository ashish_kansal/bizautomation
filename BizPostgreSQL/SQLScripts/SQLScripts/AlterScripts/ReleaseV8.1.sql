/******************************************************************
Project: Release 8.1 Date: 02.OCTOBER.2017
Comments: ALTER SCRIPTS
*******************************************************************/

/******************   SANDEEP  *****************/
USE [Production.2014]
GO

/****** Object:  Table [dbo].[DivisionMasterPromotionHistory]    Script Date: 02-Oct-17 1:43:49 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[DivisionMasterPromotionHistory](
	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numDomainID] [numeric](18, 0) NOT NULL,
	[numDivisionID] [numeric](18, 0) NOT NULL,
	[tintPreviousCRMType] [tinyint] NOT NULL,
	[tintNewCRMType] [tinyint] NOT NULL,
	[dtPromotedBy] [numeric](18, 0) NOT NULL,
	[dtPromotionDate] [datetime] NOT NULL,
 CONSTRAINT [PK_DivisionMasterPromotionHistory] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[DivisionMasterPromotionHistory]  WITH CHECK ADD  CONSTRAINT [FK_DivisionMasterPromotionHistory_DivisionMaster] FOREIGN KEY([numDivisionID])
REFERENCES [dbo].[DivisionMaster] ([numDivisionID])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[DivisionMasterPromotionHistory] CHECK CONSTRAINT [FK_DivisionMasterPromotionHistory_DivisionMaster]
GO


---------------------------------------------------------


ALTER TABLE ParentChildCustomFieldMap ADD bitCustomField BIT
UPDATE ParentChildCustomFieldMap SET bitCustomField=1

-------------------------------------------------------------------------------------------------------------------------------

BEGIN TRY
BEGIN TRANSACTION

DECLARE @numFieldID NUMERIC(18,0)

INSERT INTO DycFieldMaster
(
	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,[order],tintRow,tintColumn,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitAllowFiltering,vcGroup
)
VALUES
(
	1,'Price Level','tintPriceLevel','tintPriceLevel','PriceLevel','DivisionMaster','N','R','SelectBox','',0,50,1,1,1,0,1,0,0,0,0,1,1,1,'Org.Details Fields'
)

SET @numFieldID = SCOPE_IDENTITY()


INSERT INTO ReportFieldGroupMappingMaster
(
	numReportFieldGroupID,numFieldID,vcFieldName,vcAssociatedControlType,vcFieldDataType,bitAllowSorting,bitAllowGrouping,bitAllowAggregate,bitAllowFiltering
)
VALUES
(
	1,@numFieldID,'Price Level','SelectBox','N',1,1,1,1
)

COMMIT
END TRY
BEGIN CATCH
 IF @@TRANCOUNT > 0
  ROLLBACK TRANSACTION;

 SELECT 
  ERROR_MESSAGE(),
  ERROR_NUMBER(),
  ERROR_SEVERITY(),
  ERROR_STATE(),
  ERROR_LINE(),
  ISNULL(ERROR_PROCEDURE(), '-');
END CATCH

----------------------------------------------------------------------------------------------------------------------------

ALTER TABLE ReturnHeader ADD numReferenceSalesOrder NUMERIC(18,0)
ALTER TABLE ReturnItems ADD monVendorCost MONEY
ALTER TABLE ReturnItems ADD monAverageCost MONEY
ALTER TABLE AdditionalContactsInformation ADD vcTaxID VARCHAR(100)

----------------------------------------------------------------------------------------------------------------------------

BEGIN TRY
BEGIN TRANSACTION

	 --add new values for slide 1
	 DECLARE @numFieldID AS NUMERIC(18,0)
	 DECLARE @numFormFieldID AS NUMERIC(18,0)

	 INSERT INTO DycFieldMaster
	 (numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,[order],tintRow,tintColumn,
	 bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numListID)
	 VALUES
	 (2,'Tax ID','vcTaxID','vcTaxID','TaxID','AdditionalContactsInformation','V','R','TextBox',48,1,1,1,0,1,0,1,1,1,1,1,0)

	 SELECT @numFieldID = SCOPE_IDENTITY()

	 INSERT INTO DynamicFormFieldMaster
	 (numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
	  bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
	 VALUES
	 (10,'Tax ID','R','TextBox','vcCustomerPO#',0,0,'V','vcCustomerPO#',1,'AdditionalContactsInformation',0,48,1,'TaxID',47,1,1,1,1,1)

	 SELECT @numFormFieldID = SCOPE_IDENTITY()

	 INSERT INTO DycFormField_Mapping
	 (numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
	 bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
	 VALUES
	 (2,@numFieldID,10,1,1,'Tax ID','TextBox','TaxID',48,1,1,1,0,0,1,1,1,1,1,@numFormFieldID)
COMMIT
END TRY
BEGIN CATCH
 IF @@TRANCOUNT > 0
  ROLLBACK TRANSACTION;

 SELECT 
  ERROR_MESSAGE(),
  ERROR_NUMBER(),
  ERROR_SEVERITY(),
  ERROR_STATE(),
  ERROR_LINE(),
  ISNULL(ERROR_PROCEDURE(), '-');
END CATCH

=============================

BEGIN TRY
BEGIN TRANSACTION

	 --add new values for slide 1
	 DECLARE @numFieldID AS NUMERIC(18,0)
	 DECLARE @numFormFieldID AS NUMERIC(18,0)

	 INSERT INTO DycFieldMaster
	 (numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,[order],tintRow,tintColumn,
	 bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numListID)
	 VALUES
	 (3,'Serial/Lot #s','SerialLotNo','SerialLotNo','SerialLotNo','OpportunityItems','V','R','HyperLink',48,1,1,1,0,1,0,1,0,0,0,0,0)

	 SELECT @numFieldID = SCOPE_IDENTITY()

	 INSERT INTO DynamicFormFieldMaster
	 (numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
	  bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
	 VALUES
	 (26,'Serial/Lot #s','R','HyperLink','SerialLotNo',0,0,'V','SerialLotNo',1,'OpportunityItems',0,48,1,'SerialLotNo',47,1,1,0,0,0)

	 SELECT @numFormFieldID = SCOPE_IDENTITY()

	 INSERT INTO DycFormField_Mapping
	 (numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
	 bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
	 VALUES
	 (3,@numFieldID,26,1,1,'Serial/Lot #s','HyperLink','SerialLotNo',48,1,1,1,0,0,1,0,0,0,0,@numFormFieldID)

	 INSERT INTO DynamicFormFieldMaster
	 (numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
	  bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
	 VALUES
	 (129,'Serial/Lot #s','R','HyperLink','SerialLotNo',0,0,'V','SerialLotNo',1,'OpportunityItems',0,48,1,'SerialLotNo',47,1,1,0,0,0)

	 SELECT @numFormFieldID = SCOPE_IDENTITY()

	 INSERT INTO DycFormField_Mapping
	 (numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
	 bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
	 VALUES
	 (3,@numFieldID,129,1,1,'Serial/Lot #s','HyperLink','SerialLotNo',48,1,1,1,0,0,1,0,0,0,0,@numFormFieldID)
COMMIT
END TRY
BEGIN CATCH
 IF @@TRANCOUNT > 0
  ROLLBACK TRANSACTION;

 SELECT 
  ERROR_MESSAGE(),
  ERROR_NUMBER(),
  ERROR_SEVERITY(),
  ERROR_STATE(),
  ERROR_LINE(),
  ISNULL(ERROR_PROCEDURE(), '-');
END CATCH

--------------------------------------------------------------------------------------------------------------

UPDATE PageNavigationDTL SET bitVisible=0 WHERE vcPageNavName='Predefined Reports'
UPDATE PageNavigationDTL SET bitVisible=0 WHERE vcPageNavName='Commission Item Reports'
UPDATE PageNavigationDTL SET bitVisible=0 WHERE vcPageNavName='Commission Contacts Item Reports'
UPDATE PageNavigationDTL SET bitVisible=0 WHERE vcPageNavName='Dashboard' AND numTabID=6
UPDATE PageNavigationDTL SET vcPageNavName='Reports & Dashboards',vcImageURL='../images/CustomReports.png' WHERE vcPageNavName='Custom Reports'
UPDATE PageNavigationDTL SET vcImageURL='../images/MyReports.png' WHERE vcPageNavName='My Reports'
UPDATE ModuleMaster SET vcModuleName='Reports & Dashboards' WHERE numModuleID=24
--------------------------------------------------------------------
ALTER TABLE ReportListMaster ADD bitDefault BIT
ALTER TABLE ReportListMaster ADD intDefaultReportID INT
--------------------------------------------------------------------
DELETE FROM GroupAuthorization WHERE numModuleID=24 AND numPageID IN (SELECT numPageID FROM PageMaster WHERE numModuleID=24)
DELETE FROM PageMaster WHERE numModuleID=24
-----------------------------------------------------------------------------------------------------------
INSERT INTO PageMaster 
(
	numPageID,numModuleID,vcFileName,vcPageDesc,bitIsViewApplicable,bitIsAddApplicable,bitIsUpdateApplicable,bitIsDeleteApplicable,bitIsExportApplicable
)
VALUES
(
	1,24,'frmNewDashBoard.aspx','Ability to add new dashboard templates (from dashboard)',1,0,0,0,0
),
(
	2,24,'frmCustomReportList.aspx','Ability to create new custom reports',1,0,0,0,0
),
(
	3,24,'frmCustomReportList.aspx','Ability to set reports allowed by permission group',1,0,0,0,0
),
(
	4,24,'frmCustomReportList.aspx','Ability to build KPI reports',1,0,0,0,0
),
(
	5,24,'frmCustomReportList.aspx','Ability to Delete Dashboard templates',1,0,0,0,0
),
(
	6,24,'frmNewDashBoard.aspx','Ability to Update Dashboard templates',1,0,0,0,0
)
-------------------------------------------------------------------------------------------------------------------------------------
BEGIN TRY
BEGIN TRANSACTION
	SELECT 
		* 
	INTO 
		#temp 
	FROM
	(
		SELECT 
			ROW_NUMBER() OVER(ORDER BY numDomainId DESC) AS Row, 
			numDomainId 
		FROM 
			Domain 
		WHERE 
			numDomainId <> -255
	) TABLE2
	
	DECLARE @RowCount INT
	SET @RowCount = (SELECT COUNT(Row) FROM #temp) 
	
	DECLARE @I INT
	DECLARE @numDomainId NUMERIC(18,0)
	
	SET @I = 1

	WHILE (@I <= @RowCount)
	BEGIN
			
		SELECT @numDomainId = numDomainID FROM #temp WHERE Row = @I
	
		INSERT INTO dbo.GroupAuthorization
			(numDomainID,numModuleID,numPageID,numGroupID,intExportAllowed,intPrintAllowed,intViewAllowed,intAddAllowed,intUpdateAllowed,intDeleteAllowed)
		SELECT
			@numDomainId,24,1,numGroupID,0,0,3,0,0,0
		FROM
			AuthenticationGroupMaster
		WHERE
			numDomainID = @numDomainId AND
			tintGroupType = 1
		UNION
		SELECT
			@numDomainId,24,2,numGroupID,0,0,3,0,0,0
		FROM
			AuthenticationGroupMaster
		WHERE
			numDomainID = @numDomainId AND
			tintGroupType = 1
		UNION
		SELECT
			@numDomainId,24,3,numGroupID,0,0,3,0,0,0
		FROM
			AuthenticationGroupMaster
		WHERE
			numDomainID = @numDomainId AND
			tintGroupType = 1
		UNION
		SELECT
			@numDomainId,24,4,numGroupID,0,0,3,0,0,0
		FROM
			AuthenticationGroupMaster
		WHERE
			numDomainID = @numDomainId AND
			tintGroupType = 1
		UNION
		SELECT
			@numDomainId,24,5,numGroupID,0,0,3,0,0,0
		FROM
			AuthenticationGroupMaster
		WHERE
			numDomainID = @numDomainId AND
			tintGroupType = 1
		UNION
		SELECT
			@numDomainId,24,6,numGroupID,0,0,3,0,0,0
		FROM
			AuthenticationGroupMaster
		WHERE
			numDomainID = @numDomainId AND
			tintGroupType = 1

		SET @I = @I  + 1
	END
	
	DROP TABLE #temp
COMMIT
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		ERROR_MESSAGE(),
		ERROR_NUMBER(),
		ERROR_SEVERITY(),
		ERROR_STATE(),
		ERROR_LINE(),
		ISNULL(ERROR_PROCEDURE(), '-');
END CATCH
----------------------------------------------------------------------------------------------------------------
USE [Production.2014]
GO

/****** Object:  Table [dbo].[DashboardTemplate]    Script Date: 06-Sep-17 4:06:29 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DashboardTemplate](
	[numTemplateID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numDomainID] [numeric](18, 0) NOT NULL,
	[vcTemplateName] [varchar](300) NOT NULL,
	[dtCreatedDate] [datetime] NOT NULL,
	[numCreatedBy] [numeric](18, 0) NOT NULL,
	[dtModifiedDate] [datetime] NULL,
	[numModifiedBy] [numeric](18, 0) NULL,
 CONSTRAINT [PK_DashboardTemplate] PRIMARY KEY CLUSTERED 
(
	[numTemplateID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING ON
GO
-----------------------------------------------------------------------------------------------------------
USE [Production.2014]
GO

/****** Object:  Table [dbo].[DashboardTemplateReports]    Script Date: 06-Sep-17 4:06:22 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DashboardTemplateReports](
	[numDTRID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numDomainID] [numeric](18, 0) NOT NULL,
	[numDashboardTemplateID] [numeric](18, 0) NOT NULL,
	[numReportID] [numeric](18, 0) NOT NULL,
	[tintReportType] [tinyint] NOT NULL,
	[tintChartType] [tinyint] NOT NULL,
	[vcHeaderText] [varchar](100) NULL,
	[vcFooterText] [varchar](100) NULL,
	[tintRow] [tinyint] NOT NULL,
	[tintColumn] [tinyint] NOT NULL,
	[vcXAxis] [varchar](50) NULL,
	[vcYAxis] [varchar](50) NULL,
	[vcAggType] [varchar](50) NULL,
	[tintReportCategory] [tinyint] NULL,
	[intHeight] [int] NULL,
	[intWidth] [int] NULL,
 CONSTRAINT [PK_DashboardTemplateReports] PRIMARY KEY CLUSTERED 
(
	[numDTRID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING ON
GO

ALTER TABLE [dbo].[DashboardTemplateReports]  WITH CHECK ADD  CONSTRAINT [FK_DashboardTemplateReports_DashboardTemplate] FOREIGN KEY([numDashboardTemplateID])
REFERENCES [dbo].[DashboardTemplate] ([numTemplateID])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[DashboardTemplateReports] CHECK CONSTRAINT [FK_DashboardTemplateReports_DashboardTemplate]
GO

-----------------------------------------------------------------------------------------------------------
ALTER TABLE ReportDashboard ADD numDashboardTemplateID NUMERIC(18,0)
ALTER TABLE ReportDashboard ADD bitNewAdded BIT
-----------------------------------------------------------------------------------------------------------
ALTER TABLE UserMaster ADD numDashboardTemplateID NUMERIC(18,0)
-----------------------------------------------------------------------------------------------------------
INSERT INTO DashboardTemplate 
(
	numDomainID
	,vcTemplateName
	,dtCreatedDate
	,numCreatedBy
)
SELECT 
	numDomainID
	,CONCAT('Default_Template_',numUserCntID)
	,GETUTCDATE()
	,numUserCntID 
FROM 
	ReportDashboard 
GROUP BY 
	numDomainID,numUserCntID

INSERT INTO DashboardTemplate 
(
	numDomainID
	,vcTemplateName
	,dtCreatedDate
	,numCreatedBy
)
SELECT
	numDomainId
	,'Default Template'
	,GETUTCDATE()
	,numAdminID
FROM
	Domain
WHERE
	numDomainId <> -255
	AND numDomainId NOT IN (SELECT numDomainID FROM DashboardTemplate)
-----------------------------------------------------------------------------------------------------------
UPDATE
	ReportDashboard
SET
	numDashboardTemplateID = (CASE WHEN EXISTS (SELECT numTemplateID FROM DashboardTemplate WHERE numDomainID=ReportDashboard.numDomainID AND CHARINDEX(CONCAT('_',ReportDashboard.numUserCntID,'_'),CONCAT(vcTemplateName,'_')) > 0) THEN (SELECT numTemplateID FROM DashboardTemplate WHERE numDomainID=ReportDashboard.numDomainID AND CHARINDEX(CONCAT('_',ReportDashboard.numUserCntID,'_'),CONCAT(vcTemplateName,'_')) > 0) ELSE (SELECT numTemplateID FROM DashboardTemplate WHERE numDomainID=ReportDashboard.numDomainID) END)
-----------------------------------------------------------------------------------------------------------
UPDATE
	UserMaster
SET
	numDashboardTemplateID = (CASE WHEN EXISTS (SELECT numTemplateID FROM DashboardTemplate WHERE numDomainID=UserMaster.numDomainID AND CHARINDEX(CONCAT('_',UserMaster.numUserDetailId,'_'),CONCAT(vcTemplateName,'_')) > 0) THEN (SELECT numTemplateID FROM DashboardTemplate WHERE numDomainID=UserMaster.numDomainID AND CHARINDEX(CONCAT('_',UserMaster.numUserDetailId,'_'),CONCAT(vcTemplateName,'_')) > 0) ELSE (SELECT TOP 1 numTemplateID FROM DashboardTemplate WHERE numDomainID=UserMaster.numDomainID) END)
-----------------------------------------------------------------------------------------------------------
INSERT INTO [dbo].[DashboardTemplateReports]
(
	[numDomainID]
    ,[numDashboardTemplateID]
    ,[numReportID]
    ,[tintReportType]
    ,[tintChartType]
    ,[vcHeaderText]
    ,[vcFooterText]
    ,[tintRow]
    ,[tintColumn]
    ,[vcXAxis]
    ,[vcYAxis]
    ,[vcAggType]
    ,[tintReportCategory]
    ,[intHeight]
    ,[intWidth]
)
SELECT
	[numDomainID]
    ,[numDashboardTemplateID]
    ,[numReportID]
    ,[tintReportType]
    ,[tintChartType]
    ,[vcHeaderText]
    ,[vcFooterText]
    ,[tintRow]
    ,[tintColumn]
    ,[vcXAxis]
    ,[vcYAxis]
    ,[vcAggType]
    ,[tintReportCategory]
    ,[intHeight]
    ,[intWidth]
FROM
	ReportDashboard
----------------------------------------------------------------------------------------------------------
INSERT INTO ReportListMaster
(
	vcReportName,vcReportDescription,numDomainID,tintReportType,textQuery,bitDefault,intDefaultReportID
)
VALUES
(
	'A/R','Account Receivable',0,1,'EXEC USP_ReportListMaster_ARPreBuildReport @numDomainID',1,1
)
,
(
	'A/P','Account Payable',0,1,'EXEC USP_ReportListMaster_APPreBuildReport @numDomainID',1,2
),
(
	'Accounting Reports','Accounting Reports',0,1,'',1,4
),
(
	'Money in the Bank','Money in the Bank',0,1,'EXEC USP_ReportListMaster_BankPreBuildReport @numDomainID',1,3
),
(
	'Top 10 Items by profit amount (Last 12 months)','Top 10 Items by profit amount (Last 12 months)',0,2,'EXEC USP_ReportListMaster_Top10ItemByProfitPreBuildReport @numDomainID',1,5
),
(
	'Top 10 Items by revenue sold (Last 12 months)','Top 10 Items by revenue sold (Last 12 months)',0,2,'EXEC USP_ReportListMaster_Top10ItemByRevenuePreBuildReport @numDomainID',1,6
),
(
	'Profit margin by item classification (Last 12 months)','Profit margin by item classification (Last 12 months)',0,2,'EXEC USP_ReportListMaster_ItemClassificationProfitPreBuildReport @numDomainID',1,7
),
(
	'Top 10 Customers by profit margin','Top 10 Customers by profit margin',0,1,'EXEC USP_ReportListMaster_Top10CustomerByProfitPercentPreBuildReport @numDomainID',1,8
),
(
	'Top 10 Customers by profit amount','Top 10 Customers by profit amount',0,1,'EXEC USP_ReportListMaster_Top10CustomerByProfitAmountPreBuildReport @numDomainID',1,9
),
(
	'Action Items & Meetings due today','Action Items & Meetings due today',0,1,'EXEC USP_ReportListMaster_ActionItemPreBuildReport @numDomainID,@numUserCntID,@ClientTimeZoneOffset',1,10
),
(
	'YTD vs same period last year','YTD vs same period last year',0,1,'EXEC USP_ReportListMaster_RPEYTDAndSamePeriodLastYear @numDomainID',1,11
),
(
	'Last month vs same period last month','Last month vs same period last month',0,1,'EXEC USP_ReportListMaster_RPELastMonthAndSamePeriodLastYear @numDomainID',1,12
),
(
	'Sales vs Expenses (Last 12 months)','Sales vs Expenses (Last 12 months)',0,2,'EXEC USP_ReportListMaster_SalesVsExpenseLast12Months @numDomainID',1,13
),
(
	'Top sources of Sales Orders (Last 12 months)','Top sources of Sales Orders (Last 12 months)',0,1,'EXEC USP_ReportListMaster_TopSourceOfSalesOrder @numDomainID',1,14
),
(
	'Top 10 items by profit margin','Top 10 items by profit margin',0,1,'EXEC USP_ReportListMaster_Top10ItemByProfitMargin @numDomainID',1,15
),
(
	'Top 10 Sales Opportunities by Revenue (Last 12 months)','Top 10 Sales Opportunities by Revenue (Last 12 months)',0,1,'EXEC USP_ReportListMaster_Top10SalesOpportunityByRevenue @numDomainID',1,16
),
(
	'Top 10 Sales Opportunities by total Progress','Top 10 Sales Opportunities by total Progress',0,1,'EXEC USP_ReportListMaster_Top10SalesOpportunityByTotalProgress @numDomainID',1,17
),
(
	'Top 10 items returned vs Qty Sold','Top 10 items returned vs Qty Sold',0,1,'EXEC USP_ReportListMaster_Top10ItemsByReturnedVsSold @numDomainID',1,18
),
(
	'Largest 10 Sales Opportunities past their due date','Largest 10 Sales Opportunities past their due date',0,1,'EXEC USP_ReportListMaster_Top10SalesOpportunityByPastDue @numDomainID,@ClientTimeZoneOffset',1,19
),
(
	'Top 10 Campaigns by ROI','Top 10 Campaigns by ROI',0,1,'EXEC USP_ReportListMaster_Top10CampaignsByROI @numDomainID',1,20
),
(
	'Market Fragmentation � Top 10 Customers, as a portion total sales','Market Fragmentation � Top 10 Customers, as a portion total sales',0,2,'EXEC USP_ReportListMaster_TopCustomersByPortionOfTotalSales @numDomainID',1,21
),
(
	'Scorecard','Scorecard',0,1,'EXEC USP_ReportListMaster_PrebuildScoreCard @numDomainID',1,22
),
(
	'Top 10 Lead Sources (New Leads)','Top 10 Lead Sources (New Leads)',0,1,'EXEC USP_ReportListMaster_Top10LeadSource @numDomainID',1,23
),
(
	'Last 10 email messages from people I do business with','Last 10 email messages from people I do business with',0,1,'EXEC USP_ReportListMaster_Top10LeadSource @numDomainID',1,24
),
(
	'Reminders','Reminders',0,1,'EXEC USP_ReportListMaster_RemindersPreBuildReport @numDomainID,@ClientTimeZoneOffset',1,25
)