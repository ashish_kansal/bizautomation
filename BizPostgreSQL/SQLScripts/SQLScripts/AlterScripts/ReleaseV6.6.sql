/******************************************************************
Project: Release 6.6 Date: 9.JAN.2017
Comments: ALTER SCRIPTS
*******************************************************************/

/************************** SANDEEP ************************/

ALTER TABLE Domain DROP COLUMN numAbovePriceField
ALTER TABLE Domain DROP COLUMN numOrderStatusBeforeApproval
ALTER TABLE Domain DROP COLUMN numOrderStatusAfterApproval
ALTER TABLE Domain ADD bitCostApproval BIT
ALTER TABLE Domain ADD bitListPriceApproval BIT

UPDATE Domain SET bitCostApproval=1,bitListPriceApproval=1,bitMarginPriceViolated=1




BEGIN TRY
BEGIN TRANSACTION

	DECLARE @LocID NUMERIC(18,0)

	INSERT INTO CFW_Loc_Master
	(
		Loc_name
		,vcFieldType
		,vcCustomLookBackTableName
	)
	VALUES
	(
		'Activities'
		,'A'
		,''
	)

	SELECT @LocID = SCOPE_IDENTITY()


	SELECT 
		* 
	INTO 
		#temp 
	FROM
	(
		SELECT 
			ROW_NUMBER() OVER(ORDER BY numDomainId DESC) AS Row, 
			numDomainId 
		FROM 
			Domain 
		WHERE 
			numDomainId <> -255
	) TABLE2
	
	DECLARE @RowCount INT
	SET @RowCount = (SELECT COUNT(Row) FROM #temp) 
	
	DECLARE @I INT
	DECLARE @numDomainId NUMERIC(18,0)
	
	SET @I = 1

	WHILE (@I <= @RowCount)
	BEGIN
			
		SELECT @numDomainId = numDomainID FROM #temp WHERE Row = @I

		INSERT INTO CFw_Grp_Master
		(
			Grp_Name
			,Loc_Id
			,numDomainID
			,tintType
			,vcURLF
		)
		VALUES
		('Action Items & Meetings',@LocID,@numDomainId,2,'')


		INSERT INTO GroupTabDetails
		(
			numGroupId,numTabId,numRelationShip,bitallowed,numOrder,tintType
		)
		SELECT
			numGroupID,SCOPE_IDENTITY(),0,1,1,1
		FROM
			AuthenticationGroupMaster
		WHERE
			numDomainID = @numDomainId AND
			tintGroupType = 1

		INSERT INTO CFw_Grp_Master
		(
			Grp_Name
			,Loc_Id
			,numDomainID
			,tintType
			,vcURLF
		)
		VALUES
		('Opportunities & Projects',@LocID,@numDomainId,2,'')


		INSERT INTO GroupTabDetails
		(
			numGroupId,numTabId,numRelationShip,bitallowed,numOrder,tintType
		)
		SELECT
			numGroupID,SCOPE_IDENTITY(),0,1,2,1
		FROM
			AuthenticationGroupMaster
		WHERE
			numDomainID = @numDomainId AND
			tintGroupType = 1

		INSERT INTO CFw_Grp_Master
		(
			Grp_Name
			,Loc_Id
			,numDomainID
			,tintType
			,vcURLF
		)
		VALUES
		('Cases',@LocID,@numDomainId,2,'')


		INSERT INTO GroupTabDetails
		(
			numGroupId,numTabId,numRelationShip,bitallowed,numOrder,tintType
		)
		SELECT
			numGroupID,SCOPE_IDENTITY(),0,1,3,1
		FROM
			AuthenticationGroupMaster
		WHERE
			numDomainID = @numDomainId AND
			tintGroupType = 1

		INSERT INTO CFw_Grp_Master
		(
			Grp_Name
			,Loc_Id
			,numDomainID
			,tintType
			,vcURLF
		)
		VALUES
		('Approval Requests',@LocID,@numDomainId,2,'')


		INSERT INTO GroupTabDetails
		(
			numGroupId,numTabId,numRelationShip,bitallowed,numOrder,tintType
		)
		SELECT
			numGroupID,SCOPE_IDENTITY(),0,1,4,1
		FROM
			AuthenticationGroupMaster
		WHERE
			numDomainID = @numDomainId AND
			tintGroupType = 1

		SET @I = @I  + 1
	END
	
	DROP TABLE #temp
COMMIT
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		ERROR_MESSAGE(),
		ERROR_NUMBER(),
		ERROR_SEVERITY(),
		ERROR_STATE(),
		ERROR_LINE(),
		ISNULL(ERROR_PROCEDURE(), '-');
END CATCH