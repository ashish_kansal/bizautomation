/******************************************************************
Project: Release 6.3 Date: 10.NOV.2016
Comments: ALTER SCRIPTS
*******************************************************************/

/************************** SANDEEP ************************/

ALTER TABLE OpportunityMaster ADD
numReleaseStatus TINYINT

==============================

BEGIN TRY
BEGIN TRANSACTION

	DECLARE @numFieldID AS NUMERIC(18,0)
	DECLARE @numFormFieldID AS NUMERIC(18,0)

	INSERT INTO DycFieldMaster 
	(numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcPropertyName,numListID,[order],bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,vcGroup)
	VALUES
	(3,'Release Date','dtReleaseDate','dtReleaseDate','OpportunityMaster','V','R','DateField','ReleaseDate',0,1,1,0,1,0,1,1,1,1,1,'Order Fields')

	SELECT @numFieldID = SCOPE_IDENTITY()

	INSERT INTO DynamicFormFieldMaster
	(numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,vcListItemType,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,bitDefault,bitInResults)
	VALUES
	(38,'Release Date','R','DateField','dtReleaseDate','',0,0,'V','dtReleaseDate',1,'OpportunityMaster',0,1)

	SELECT @numFormFieldID = SCOPE_IDENTITY()

	INSERT INTO DycFormField_Mapping
	(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,[order],bitInResults,bitDefault,bitDeleted,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,numFormFieldID)
	VALUES
	(3,@numFieldID,38,1,1,'Release Date','DateField',47,1,0,0,1,1,1,1,1,@numFormFieldID)


	INSERT INTO DynamicFormFieldMaster
	(numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,vcListItemType,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,bitDefault,bitInResults)
	VALUES
	(39,'Release Date','R','DateField','dtReleaseDate','',0,0,'V','dtReleaseDate',1,'OpportunityMaster',0,1)

	SELECT @numFormFieldID = SCOPE_IDENTITY()

	INSERT INTO DycFormField_Mapping
	(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,[order],bitInResults,bitDefault,bitDeleted,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,numFormFieldID)
	VALUES
	(3,@numFieldID,39,1,1,'Release Date','DateField',47,1,0,0,1,1,1,1,1,@numFormFieldID)
COMMIT
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		ERROR_MESSAGE(),
		ERROR_NUMBER(),
		ERROR_SEVERITY(),
		ERROR_STATE(),
		ERROR_LINE(),
		ISNULL(ERROR_PROCEDURE(), '-');
END CATCH

===============================================================================

BEGIN TRY
BEGIN TRANSACTION

	DECLARE @numFieldID AS NUMERIC(18,0)
	DECLARE @numFormFieldID AS NUMERIC(18,0)

	INSERT INTO DycFieldMaster 
	(numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcPropertyName,vcListItemType,numListID,[order],bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,vcGroup)
	VALUES
	(3,'Release Status','numReleaseStatus','numReleaseStatus','OpportunityMaster','N','R','SelectBox','ReleaseStatus','',0,1,1,0,1,0,1,1,1,1,1,'Order Fields')

	SELECT @numFieldID = SCOPE_IDENTITY()

	INSERT INTO DynamicFormFieldMaster
	(numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,vcListItemType,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,bitDefault,bitInResults)
	VALUES
	(38,'Release Status','R','SelectBox','numReleaseStatus','',0,0,'N','numReleaseStatus',1,'OpportunityMaster',0,1)

	SELECT @numFormFieldID = SCOPE_IDENTITY()

	INSERT INTO DycFormField_Mapping
	(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,[order],bitInResults,bitDefault,bitDeleted,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,numFormFieldID)
	VALUES
	(3,@numFieldID,38,1,1,'Release Status','SelectBox',47,1,0,0,1,1,1,1,1,@numFormFieldID)


	INSERT INTO DynamicFormFieldMaster
	(numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,vcListItemType,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,bitDefault,bitInResults)
	VALUES
	(39,'Release Status','R','SelectBox','numReleaseStatus','',0,0,'N','numReleaseStatus',1,'OpportunityMaster',0,1)

	SELECT @numFormFieldID = SCOPE_IDENTITY()

	INSERT INTO DycFormField_Mapping
	(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,[order],bitInResults,bitDefault,bitDeleted,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,numFormFieldID)
	VALUES
	(3,@numFieldID,39,1,1,'Release Status','SelectBox',47,1,0,0,1,1,1,1,1,@numFormFieldID)
COMMIT
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		ERROR_MESSAGE(),
		ERROR_NUMBER(),
		ERROR_SEVERITY(),
		ERROR_STATE(),
		ERROR_LINE(),
		ISNULL(ERROR_PROCEDURE(), '-');
END CATCH

============================================================================================================================

UPDATE DynamicFormMaster SET vcFormName = CONCAT(vcFormName,' (Sales)') WHERE numFormId=26

=====================================================================

INSERT INTO DynamicFormMaster
(numFormId,vcFormName,cCustomFieldsAssociated,cAOIAssociated,bitDeleted,tintFlag)
VALUES
(129,'Products & services Setting (Purchase)','Y','N',0,1)

==================================================================

INSERT INTO DycFormField_Mapping 
(
	[numModuleID]
    ,[numFieldID]
    ,[numDomainID]
    ,[numFormID]
    ,[bitAllowEdit]
    ,[bitInlineEdit]
    ,[vcFieldName]
    ,[vcAssociatedControlType]
    ,[vcPropertyName]
    ,[PopupFunctionName]
    ,[order]
    ,[tintRow]
    ,[tintColumn]
    ,[bitInResults]
    ,[bitDeleted]
    ,[bitDefault]
    ,[bitSettingField]
    ,[bitAddField]
    ,[bitDetailField]
    ,[bitAllowSorting]
    ,[bitWorkFlowField]
    ,[bitImport]
    ,[bitExport]
    ,[bitAllowFiltering]
    ,[bitRequired]
    ,[numFormFieldID]
    ,[intSectionID]
    ,[bitAllowGridColor]
)
SELECT 
	[numModuleID]
    ,[numFieldID]
    ,[numDomainID]
    ,129
    ,[bitAllowEdit]
    ,[bitInlineEdit]
    ,[vcFieldName]
    ,[vcAssociatedControlType]
    ,[vcPropertyName]
    ,[PopupFunctionName]
    ,[order]
    ,[tintRow]
    ,[tintColumn]
    ,[bitInResults]
    ,[bitDeleted]
    ,[bitDefault]
    ,[bitSettingField]
    ,[bitAddField]
    ,[bitDetailField]
    ,[bitAllowSorting]
    ,[bitWorkFlowField]
    ,[bitImport]
    ,[bitExport]
    ,[bitAllowFiltering]
    ,[bitRequired]
    ,NULL
    ,[intSectionID]
    ,[bitAllowGridColor]
FROM 
	DycFormField_Mapping 
WHERE 
	numFormID=26

=============================================

INSERT INTO DycFormConfigurationDetails
(
	[numFormId]
    ,[numFieldId]
    ,[numViewId]
    ,[intColumnNum]
    ,[intRowNum]
    ,[boolAOIField]
    ,[numAuthGroupID]
    ,[numDomainId]
    ,[numSurId]
    ,[numUserCntID]
    ,[numRelCntType]
    ,[tintPageType]
    ,[bitCustom]
    ,[intColumnWidth]
)
SELECT
	129
    ,[numFieldId]
    ,[numViewId]
    ,[intColumnNum]
    ,[intRowNum]
    ,[boolAOIField]
    ,[numAuthGroupID]
    ,[numDomainId]
    ,[numSurId]
    ,[numUserCntID]
    ,[numRelCntType]
    ,[tintPageType]
    ,[bitCustom]
    ,[intColumnWidth]
FROM
	DycFormConfigurationDetails
WHERE
	numFormId=26

=================================================================

INSERT INTO DynamicFormConfigurationDetails
(
	[numFormId]
    ,[numFormFieldId]
    ,[vcDbColumnName]
    ,[vcAssociatedControlType]
    ,[vcFieldType]
    ,[intColumnNum]
    ,[intRowNum]
    ,[boolRequired]
    ,[numListID]
    ,[boolAOIField]
    ,[numAuthGroupID]
    ,[numDomainId]
    ,[numSurId]
    ,[numUserCntID]
    ,[numRelCntType]
    ,[tintPageType]
    ,[bitCustom]
)
SELECT
	129
    ,[numFormFieldId]
    ,[vcDbColumnName]
    ,[vcAssociatedControlType]
    ,[vcFieldType]
    ,[intColumnNum]
    ,[intRowNum]
    ,[boolRequired]
    ,[numListID]
    ,[boolAOIField]
    ,[numAuthGroupID]
    ,[numDomainId]
    ,[numSurId]
    ,[numUserCntID]
    ,[numRelCntType]
    ,[tintPageType]
    ,[bitCustom]
FROM
	DynamicFormConfigurationDetails
WHERE
	numFormId=26

===============================================================================

BEGIN TRY
BEGIN TRANSACTION

	DECLARE @numFieldID AS NUMERIC(18,0)
	DECLARE @numFormFieldID AS NUMERIC(18,0)

	INSERT INTO DycFieldMaster 
	(numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcPropertyName,numListID,[order],bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,vcGroup)
	VALUES
	(3,'Item Release Date','vcItemReleaseDate','vcItemReleaseDate','OpportunityItems','V','R','Label','ItemReleaseDate',0,1,1,0,0,0,1,1,1,1,0,'')

	SELECT @numFieldID = SCOPE_IDENTITY()

	INSERT INTO DynamicFormFieldMaster
	(numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,vcListItemType,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,bitDefault,bitInResults)
	VALUES
	(26,'Item Release Date','R','Label','vcItemReleaseDate','',0,0,'V','vcItemReleaseDate',0,'OpportunityItems',0,1)

	SELECT @numFormFieldID = SCOPE_IDENTITY()

	INSERT INTO DycFormField_Mapping
	(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,[order],bitInResults,bitDefault,bitDeleted,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,numFormFieldID)
	VALUES
	(3,@numFieldID,26,1,1,'Item Release Date','Label',47,1,0,0,1,1,1,1,0,@numFormFieldID)
COMMIT
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		ERROR_MESSAGE(),
		ERROR_NUMBER(),
		ERROR_SEVERITY(),
		ERROR_STATE(),
		ERROR_LINE(),
		ISNULL(ERROR_PROCEDURE(), '-');
END CATCH


=================================================================================================================================


BEGIN TRY
BEGIN TRANSACTION

	DECLARE @numFieldID AS NUMERIC(18,0)
	DECLARE @numFormFieldID AS NUMERIC(18,0)

	INSERT INTO DycFieldMaster 
	(numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcPropertyName,numListID,[order],bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,vcGroup)
	VALUES
	(3,'Purchased','numPurchasedQty','numPurchasedQty','OpportunityItems','V','R','Label','numPurchasedQty',0,1,1,0,0,0,1,1,1,1,0,'')

	SELECT @numFieldID = SCOPE_IDENTITY()

	INSERT INTO DynamicFormFieldMaster
	(numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,vcListItemType,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,bitDefault,bitInResults)
	VALUES
	(26,'Purchased','R','Label','numPurchasedQty','',0,0,'V','numPurchasedQty',0,'OpportunityItems',0,1)

	SELECT @numFormFieldID = SCOPE_IDENTITY()

	INSERT INTO DycFormField_Mapping
	(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,[order],bitInResults,bitDefault,bitDeleted,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,numFormFieldID)
	VALUES
	(3,@numFieldID,26,1,1,'Purchased','Label',47,1,0,0,1,1,1,1,0,@numFormFieldID)
COMMIT
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		ERROR_MESSAGE(),
		ERROR_NUMBER(),
		ERROR_SEVERITY(),
		ERROR_STATE(),
		ERROR_LINE(),
		ISNULL(ERROR_PROCEDURE(), '-');
END CATCH

====================================================================================================

USE [Production.2014]
GO

/****** Object:  Table [dbo].[OpportunityItemsReleaseDates]    Script Date: 13-Jul-16 3:15:02 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[OpportunityItemsReleaseDates](
	[numID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numDomainID] [numeric](18, 0) NOT NULL,
	[numOppID] [numeric](18, 0) NOT NULL,
	[numOppItemID] [numeric](18, 0) NOT NULL,
	[dtReleaseDate] [date] NOT NULL,
	[numQty] [numeric](18, 0) NOT NULL,
	[tintStatus] [tinyint] NOT NULL,
	[numPurchasedOrderID] [numeric](18, 0) NULL
 CONSTRAINT [PK_OpportunityItemsReleaseDates] PRIMARY KEY CLUSTERED 
(
	[numID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[OpportunityItemsReleaseDates]  WITH CHECK ADD  CONSTRAINT [FK_OpportunityItemsReleaseDates_OpportunityItems] FOREIGN KEY([numOppItemID])
REFERENCES [dbo].[OpportunityItems] ([numoppitemtCode])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[OpportunityItemsReleaseDates] CHECK CONSTRAINT [FK_OpportunityItemsReleaseDates_OpportunityItems]
GO

ALTER TABLE [dbo].[OpportunityItemsReleaseDates]  WITH CHECK ADD  CONSTRAINT [FK_OpportunityItemsReleaseDates_OpportunityMaster] FOREIGN KEY([numOppID])
REFERENCES [dbo].[OpportunityMaster] ([numOppId])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[OpportunityItemsReleaseDates] CHECK CONSTRAINT [FK_OpportunityItemsReleaseDates_OpportunityMaster]
GO


=============================================================================================================================

USE [Production.2014]
GO

/****** Object:  Index [NCIX_OpportunityItemsReleaseDates]    Script Date: 13-Jul-16 3:16:12 PM ******/
CREATE NONCLUSTERED INDEX [NCIX_OpportunityItemsReleaseDates] ON [dbo].[OpportunityItemsReleaseDates]
(
	[numDomainID] ASC,
	[numOppID] ASC,
	[numOppItemID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

=================================================================================================================

  ALTER TABLE DemandForecastDays ADD
  numFromDays INT NULL

================================================

  UPDATE DemandForecastDays SET vcDays='7 days out',numFromDays=0 WHERE numDFDaysID=1
  UPDATE DemandForecastDays SET vcDays='15 days out',numFromDays=8,numDays=15 WHERE numDFDaysID=2
  UPDATE DemandForecastDays SET vcDays='30 days out',numFromDays=16 WHERE numDFDaysID=3
  UPDATE DemandForecastDays SET vcDays='60 days out',numFromDays=31 WHERE numDFDaysID=4
  UPDATE DemandForecastDays SET vcDays='90 days out',numFromDays=61 WHERE numDFDaysID=5
  INSERT INTO DemandForecastDays (vcDays,numDays,numFromDays) VALUES ('180 days out',180,91)

=========================================================

ALTER TABLE DemandForecast DROP COLUMN bitLastWeek

======================================================

DELETE FROM DemandForecastAnalysisPattern WHERE bitNextWeekLastYear=1

=============================================================================

ALTER TABLE DemandForecastAnalysisPattern DROP COLUMN bitNextWeekLastYear

=============================================================================

sp_RENAME 'DemandForecast.bitIncludeSalesOpp' , 'bitIncludeOpenReleaseDates', 'COLUMN'

================================================================================

UPDATE DemandForecast SET bitIncludeOpenReleaseDates = 0

=================================================================================

INSERT INTO WareHouseItems
(
	numItemID
	,numWareHouseID
	,numDomainID
	,numOnHand
	,numOnOrder
	,numReorder
	,numAllocation
	,numBackOrder
	,monWListPrice
	,vcLocation
	,vcWHSKU
	,vcBarCode
	,dtModified
	,numWLocationID
	,bitDefault
)
SELECT
	DISTINCT
	WIMain.numItemID
	,WIMain.numWareHouseID
	,WIMain.numDomainID
	,0
	,0
	,0
	,0
	,0
	,0
	,''
	,''
	,''
	,GETUTCDATE()
	,-1
	,0
FROM
	WareHouseItems WIMain
INNER JOIN
	Item I
ON
	WIMain.numItemID = I.numItemCode
LEFT JOIN
	WareHouseItems WIGlobal
ON
	WIMain.numItemID = WIGlobal.numItemID
	AND WIGlobal.numWLocationID = -1
WHERE
	WIGlobal.numWareHouseItemID IS NULL
GROUP BY
	WIMain.numItemID
	,I.vcItemName
	,WIMain.numDomainID
	,WIMain.numWareHouseID
ORDER BY
	WIMain.numItemID
	,WIMain.numWareHouseID

=======================================================================


DECLARE @numPageID INT
SELECT @numPageID=MAX(numPageID) FROM PageMaster

INSERT INTO PageMaster 
(
	numPageID
	,numModuleID
	,vcFileName
	,vcPageDesc
	,bitIsViewApplicable
	,bitIsAddApplicable
	,bitIsUpdateApplicable
	,bitIsDeleteApplicable
	,bitIsExportApplicable
)
VALUES
(
	@numPageID + 1
	,2
	,'frmLeadList.aspx'
	,'Web Leads'
	,1
	,0
	,0
	,1
	,0
),
(
	@numPageID + 2
	,2
	,'frmLeadList.aspx'
	,'Public Leads'
	,1
	,0
	,0
	,1
	,0
)

====================================

UPDATE PageMaster SET vcPageDesc='My Leads List' WHERE numPageID=2 AND numModuleID=2


============================

ALTER TABLE SalesFulfillmentLog ADD bitSuccess BIT


/*************** Prasant ************************/


BEGIN TRY
BEGIN TRANSACTION

DECLARE @numFieldID AS NUMERIC(18,0)
DECLARE @numFormFieldID AS NUMERIC(18,0)

INSERT INTO DycFieldMaster
(numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,[order],tintRow,tintColumn,
bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numListID)
VALUES
(3,'Partner Source','numPartenerSource','numPartenerSource','numPartenerSource','OpportunityMaster','V','R','SelectBox',46,1,1,1,1,0,0,1,1,1,1,1,0)

SELECT @numFieldID = SCOPE_IDENTITY()

INSERT INTO DynamicFormFieldMaster
(numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
 bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
VALUES
(38,'Partner Source','R','SelectBox','numPartenerSource',0,0,'V','numPartenerSource',1,'OpportunityMaster',0,7,1,'numPartenerSource',1,7,1,1,1,0)

SELECT @numFormFieldID = SCOPE_IDENTITY()

INSERT INTO DycFormField_Mapping
(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
VALUES
(3,@numFieldID,38,1,1,'Partner Source','SelectBox','numPartenerSource',7,1,7,1,0,0,1,1,1,0,1,@numFormFieldID)



COMMIT
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		ERROR_MESSAGE(),
		ERROR_NUMBER(),
		ERROR_SEVERITY(),
		ERROR_STATE(),
		ERROR_LINE(),
		ISNULL(ERROR_PROCEDURE(), '-');
END CATCH

===============================================================

BEGIN TRY
BEGIN TRANSACTION

DECLARE @numFieldID AS NUMERIC(18,0)
DECLARE @numFormFieldID AS NUMERIC(18,0)

INSERT INTO DycFieldMaster
(numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,[order],tintRow,tintColumn,
bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numListID)
VALUES
(3,'Partner Contact','numPartenerContact','numPartenerContact','numPartenerContact','OpportunityMaster','V','R','SelectBox',46,1,1,1,1,0,0,1,1,1,1,1,0)

SELECT @numFieldID = SCOPE_IDENTITY()

INSERT INTO DynamicFormFieldMaster
(numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
 bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
VALUES
(38,'Partner Contact','R','SelectBox','numPartenerContact',0,0,'V','numPartenerContact',1,'OpportunityMaster',0,7,1,'numPartenerContact',1,7,1,1,1,0)

SELECT @numFormFieldID = SCOPE_IDENTITY()

INSERT INTO DycFormField_Mapping
(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
VALUES
(3,@numFieldID,38,1,1,'Partner Contact','SelectBox','numPartenerContact',7,1,7,1,0,0,1,1,1,0,1,@numFormFieldID)



COMMIT
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		ERROR_MESSAGE(),
		ERROR_NUMBER(),
		ERROR_SEVERITY(),
		ERROR_STATE(),
		ERROR_LINE(),
		ISNULL(ERROR_PROCEDURE(), '-');
END CATCH

=======================================================
-----Sales Order


BEGIN TRY
BEGIN TRANSACTION

DECLARE @numFieldID AS NUMERIC(18,0)
DECLARE @numFormFieldID AS NUMERIC(18,0)

INSERT INTO DycFieldMaster
(numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,[order],tintRow,tintColumn,
bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numListID)
VALUES
(3,'Partner Source','numPartenerSource','numPartenerSource','numPartenerSource','DivisionMaster','V','R','SelectBox',46,1,1,1,1,0,0,1,1,1,1,1,0)

SELECT @numFieldID = SCOPE_IDENTITY()

INSERT INTO DynamicFormFieldMaster
(numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
 bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
VALUES
(34,'Partner Source','R','SelectBox','numPartenerSource',0,0,'V','numPartenerSource',1,'DivisionMaster',0,7,1,'numPartenerSource',1,7,1,1,1,0)

SELECT @numFormFieldID = SCOPE_IDENTITY()

INSERT INTO DycFormField_Mapping
(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
VALUES
(3,@numFieldID,34,1,1,'Partner Source','SelectBox','numPartenerSource',7,1,7,1,0,0,1,1,1,0,1,@numFormFieldID)



COMMIT
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		ERROR_MESSAGE(),
		ERROR_NUMBER(),
		ERROR_SEVERITY(),
		ERROR_STATE(),
		ERROR_LINE(),
		ISNULL(ERROR_PROCEDURE(), '-');
END CATCH

=======================================================================================

BEGIN TRY
BEGIN TRANSACTION

DECLARE @numFieldID AS NUMERIC(18,0)
DECLARE @numFormFieldID AS NUMERIC(18,0)

INSERT INTO DycFieldMaster
(numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,[order],tintRow,tintColumn,
bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numListID)
VALUES
(3,'Partner Contact','numPartenerContact','numPartenerContact','numPartenerContact','OpportunityMaster','V','R','SelectBox',46,1,1,1,1,0,0,1,1,1,1,1,0)

SELECT @numFieldID = SCOPE_IDENTITY()

INSERT INTO DynamicFormFieldMaster
(numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
 bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
VALUES
(39,'Partner Contact','R','SelectBox','numPartenerContact',0,0,'V','numPartenerContact',1,'OpportunityMaster',0,7,1,'numPartenerContact',1,7,1,1,1,0)

SELECT @numFormFieldID = SCOPE_IDENTITY()

INSERT INTO DycFormField_Mapping
(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
VALUES
(3,@numFieldID,39,1,1,'Partner Contact','SelectBox','numPartenerContact',7,1,7,1,0,0,1,1,1,0,1,@numFormFieldID)



COMMIT
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		ERROR_MESSAGE(),
		ERROR_NUMBER(),
		ERROR_SEVERITY(),
		ERROR_STATE(),
		ERROR_LINE(),
		ISNULL(ERROR_PROCEDURE(), '-');
END CATCH

=========================================================================================

BEGIN TRY
BEGIN TRANSACTION

DECLARE @numFieldID AS NUMERIC(18,0)
DECLARE @numFormFieldID AS NUMERIC(18,0)

INSERT INTO DycFieldMaster
(numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,[order],tintRow,tintColumn,
bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numListID)
VALUES
(3,'Partner Contact','numPartenerContact','numPartenerContact','numPartenerContact','DivisionMaster','V','R','SelectBox',46,1,1,1,1,0,0,1,1,1,1,1,0)

SELECT @numFieldID = SCOPE_IDENTITY()

INSERT INTO DynamicFormFieldMaster
(numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
 bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
VALUES
(34,'Partner Contact','R','SelectBox','numPartenerContact',0,0,'V','numPartenerContact',1,'DivisionMaster',0,7,1,'numPartenerContact',1,7,1,1,1,0)

SELECT @numFormFieldID = SCOPE_IDENTITY()

INSERT INTO DycFormField_Mapping
(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
VALUES
(3,@numFieldID,34,1,1,'Partner Contact','SelectBox','numPartenerContact',7,1,7,1,0,0,1,1,1,0,1,@numFormFieldID)



COMMIT
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		ERROR_MESSAGE(),
		ERROR_NUMBER(),
		ERROR_SEVERITY(),
		ERROR_STATE(),
		ERROR_LINE(),
		ISNULL(ERROR_PROCEDURE(), '-');
END CATCH

===============================================================================================

BEGIN TRY
BEGIN TRANSACTION

DECLARE @numFieldID AS NUMERIC(18,0)
DECLARE @numFormFieldID AS NUMERIC(18,0)

INSERT INTO DycFieldMaster
(numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,[order],tintRow,tintColumn,
bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numListID)
VALUES
(3,'Partner Contact','numPartenerContact','numPartenerContact','numPartenerContact','OpportunityMaster','V','R','SelectBox',46,1,1,1,1,0,0,1,0,1,1,1,0)

SELECT @numFieldID = SCOPE_IDENTITY()

INSERT INTO DynamicFormFieldMaster
(numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
 bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
VALUES
(39,'Partner Contact','R','SelectBox','numPartenerContact',0,0,'V','numPartenerContact',1,'OpportunityMaster',0,7,1,'numPartenerContact',1,7,1,0,1,0)

SELECT @numFormFieldID = SCOPE_IDENTITY()

INSERT INTO DycFormField_Mapping
(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
VALUES
(3,@numFieldID,39,1,1,'Partner Contact','SelectBox','numPartenerContact',7,1,7,1,0,0,1,0,1,0,1,@numFormFieldID)

COMMIT
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		ERROR_MESSAGE(),
		ERROR_NUMBER(),
		ERROR_SEVERITY(),
		ERROR_STATE(),
		ERROR_LINE(),
		ISNULL(ERROR_PROCEDURE(), '-');
END CATCH

=================================================

ALTER TABLE DivisionMaster ADD numPartenerSource NUMERIC(18,0) DEFAULT 0
ALTER TABLE DivisionMaster ADD numPartenerContact NUMERIC(18,0) DEFAULT 0

ALTER TABLE OpportunityMaster ADD numPartenerContact NUMERIC(18,0) DEFAULT 0