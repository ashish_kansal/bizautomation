/******************************************************************
Project: Release 3.6 Date: 22.SEP.2014
Comments: STORED PROCEDURES
*******************************************************************/
---------------- VIEWS
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='v'AND NAME ='View_DynamicColumnsMasterConfig')
DROP VIEW View_DynamicColumnsMasterConfig
GO
-- select * from Dim_OrderDate order by monthkey desc
CREATE VIEW [dbo].[View_DynamicColumnsMasterConfig]
AS  
	SELECT 
	  DFM.numFieldId, 
	  DFM.numModuleID, 
	  ISNULL(DFV.vcNewFormFieldName, 
	  ISNULL(DFFM.vcFieldName, DFM.vcFieldName)) AS vcFieldName, 
      DFG.vcNewFieldName AS vcCultureFieldName, 
	  DFG.numCultureID, 
	  DFM.vcDbColumnName, 
	  ISNULL(DFFM.vcPropertyName, DFM.vcPropertyName) AS vcPropertyName, 
	  DFM.vcLookBackTableName, 
	  DFM.vcFieldDataType, 
	  DFM.vcFieldType, 
	  ISNULL(DFFM.vcAssociatedControlType, DFM.vcAssociatedControlType) AS vcAssociatedControlType, 
	  ISNULL(DFV.vcToolTip, DFM.vcToolTip) AS vcToolTip, 
	  DFG.vcToolTip AS vcCultureToolTip, 
	  ISNULL(DFM.vcListItemType, '') AS vcListItemType, 
	  DFM.numListID, 
	  ISNULL(DFFM.PopupFunctionName, DFM.PopupFunctionName) AS PopupFunctionName, 
	  ISNULL(DFFM.[order] + 1, DFM.[order] + 1) AS tintOrder, 
	  ISNULL(DFC.intRowNum, DFM.tintRow) AS tintRow, 
	  ISNULL(DFC.intColumnNum, DFM.tintColumn) AS tintColumn, 
      ISNULL(DFFM.bitInResults, ISNULL(DFM.bitInResults, 0)) AS bitInResults, ISNULL(DFFM.bitDeleted, ISNULL(DFM.bitDeleted, 0)) AS bitDeleted, 
      ISNULL(DFFM.bitAllowEdit, ISNULL(DFM.bitAllowEdit, 0)) AS bitAllowEdit, ISNULL(DFFM.bitDefault, ISNULL(DFM.bitDefault, 0)) AS bitDefault, 
      ISNULL(DFFM.bitSettingField, ISNULL(DFM.bitSettingField, 0)) AS bitSettingField, ISNULL(DFFM.bitAddField, ISNULL(DFM.bitAddField, 0)) AS bitAddField, 
      ISNULL(DFFM.bitDetailField, ISNULL(DFM.bitDetailField, 0)) AS bitDetailField, ISNULL(DFFM.bitAllowSorting, ISNULL(DFM.bitAllowSorting, 0)) AS bitAllowSorting, 
      ISNULL(DFFM.bitImport, ISNULL(DFM.bitImport, 0)) AS bitImport, ISNULL(DFFM.bitExport, ISNULL(DFM.bitExport, 0)) AS bitExport, ISNULL(DFFM.bitAllowFiltering, 
      ISNULL(DFM.bitAllowFiltering, 0)) AS bitAllowFiltering, 
	  ISNULL(DFFM.bitInlineEdit, 
	  ISNULL(DFM.bitInlineEdit, 0)) AS bitInlineEdit, 
	  ISNULL(DFFM.bitRequired, 
      ISNULL(DFM.bitRequired, 0)) AS bitRequired, 
	  DFM.intFieldMaxLength, 
	  ISNULL(DFV.bitIsRequired, 0) AS bitIsRequired, 
	  ISNULL(DFV.bitIsEmail, 0) AS bitIsEmail, 
      ISNULL(DFV.bitIsAlphaNumeric, 0) AS bitIsAlphaNumeric, 
	  ISNULL(DFV.bitIsNumeric, 0) AS bitIsNumeric, 
	  ISNULL(DFV.bitIsLengthValidation, 0) AS bitIsLengthValidation, 
	  DFV.intMaxLength, 
	  DFV.intMinLength, 
	  ISNULL(DFV.bitFieldMessage, 0) AS bitFieldMessage, 
	  ISNULL(DFV.vcFieldMessage, '') AS vcFieldMessage, 
	  CASE 
		WHEN DFM.vcAssociatedControlType = 'SelectBox' 
		THEN isnull ((SELECT numPrimaryListID FROM FieldRelationship WHERE numSecondaryListID = DFM.numListID AND numDomainID = DFC.numDomainID), 0) 
		ELSE 0 
	  END AS ListRelID, 
	  DFC.numFormId, 
	  DFC.intRowNum,
	  DFC.intColumnNum,
	  --DFC.numUserCntID, 
      DFC.numDomainId, 
	  DFC.tintPageType, 
	  DFC.numRelCntType, 
	  ISNULL(DFC.bitCustom, 0) AS bitCustom, 
	  --DFC.numViewId, 
	  DFC.numGroupID AS numAuthGroupID, 
	  DFC.bitGridConfiguration,
      --ISNULL(DFC.boolAOIField, 0) AS boolAOIField, 
	  ISNULL(DFFM.intSectionID, 0) AS intSectionID, 
	  DFM.vcOrigDbColumnName, 
	  ISNULL(DFFM.bitWorkFlowField, ISNULL(DFM.bitWorkFlowField, 0)) AS bitWorkFlowField, 
	  ISNULL(DFM.intColumnWidth, 0) AS intColumnWidth, 
      ISNULL(DFFM.bitAllowGridColor, 0) AS bitAllowGridColor
FROM            
	dbo.BizFormWizardMasterConfiguration AS DFC 
INNER JOIN
    dbo.DycFormField_Mapping AS DFFM 
ON 
	DFC.numFormId = DFFM.numFormID AND 
	DFC.numFieldId = DFFM.numFieldID 
INNER JOIN
    dbo.DycFieldMaster AS DFM 
ON 
	DFFM.numModuleID = DFFM.numModuleID AND 
	DFC.numFieldId = DFM.numFieldId 
LEFT OUTER JOIN
    dbo.DynamicFormField_Validation AS DFV 
ON 
	DFV.numDomainId = DFC.numDomainId AND 
	DFFM.numFormID = DFV.numFormId AND 
    DFM.numFieldId = DFV.numFormFieldId 
LEFT OUTER JOIN
    dbo.DycField_Globalization AS DFG 
ON 
	DFG.numDomainID = DFC.numDomainId AND 
	DFM.numFieldId = DFG.numFieldID AND 
    DFM.numModuleID = DFG.numModuleID
WHERE        
	(ISNULL(DFC.bitCustom, 0) = 0)
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='v'AND NAME ='View_DynamicCustomColumnsMasterConfig')
DROP VIEW View_DynamicCustomColumnsMasterConfig
GO
-- select * from Dim_OrderDate order by monthkey desc
CREATE VIEW [dbo].[View_DynamicCustomColumnsMasterConfig]
AS  
	SELECT        
		DFC.intRowNum + 1 AS tintOrder, 
		'Cust' + CONVERT(varchar(10), CFM.Fld_id) AS vcDbColumnName, 
		CFM.Fld_label AS vcFieldName, 
		CFM.Fld_type AS vcAssociatedControlType, 
		ISNULL(CFM.numlistid, 0) AS numListID, 
		DFC.numFieldId, 1 AS bitAllowSorting, 
		1 AS bitAllowEdit, 1 AS bitInlineEdit, 
		ISNULL(DFC.intRowNum, 0) AS tintRow, 
		ISNULL(DFC.intColumnNum, 0) AS tintColumn, 
		ISNULL(V.bitIsRequired, 0) AS bitIsRequired, 
		ISNULL(V.bitIsEmail, 0) AS bitIsEmail, 
		ISNULL(V.bitIsAlphaNumeric, 0) AS bitIsAlphaNumeric, 
		ISNULL(V.bitIsNumeric, 0) AS bitIsNumeric, 
		ISNULL(V.bitIsLengthValidation, 0) AS bitIsLengthValidation, 
		V.intMaxLength, 
		V.intMinLength, 
		ISNULL(V.bitFieldMessage, 0) AS bitFieldMessage, 
		ISNULL(V.vcFieldMessage, '') AS vcFieldMessage, 
		CASE 
		WHEN fld_type = 'SelectBox' 
		THEN isnull((SELECT numPrimaryListID FROM FieldRelationship WHERE numSecondaryListID = CFM.numListID AND numDomainID = DFC.numDomainID), 0) 
		ELSE 0 
		END AS ListRelID, 
		DFC.numFormId, 
		DFC.intRowNum,
	    DFC.intColumnNum,
		--DFC.numUserCntID, 
		DFC.numDomainId, 
		DFC.tintPageType, 
		DFC.numRelCntType, 
		DFC.bitCustom, 
		--DFC.numViewId, 
		DFC.numGroupID AS numAuthGroupID, 
		DFC.bitGridConfiguration,
		CFM.Grp_id, 
		CFM.subgrp, 
		CFM.vcURL, 
		CFM.vcToolTip, 
		--ISNULL(DFC.boolAOIField, 0) AS boolAOIField, 
		ISNULL(L.vcFieldType, 'C') AS vcFieldType, 
		0 AS intColumnWidth
	FROM            
		dbo.BizFormWizardMasterConfiguration AS DFC 
	INNER JOIN
		dbo.CFW_Fld_Master AS CFM 
	ON 
		DFC.numFieldId = CFM.Fld_id 
	LEFT OUTER JOIN
		dbo.CFw_Grp_Master AS CGM 
	ON 
		CFM.subgrp = CGM.Grp_id 
	LEFT OUTER JOIN
		dbo.CFW_Validation AS V 
	ON 
		V.numFieldID = CFM.Fld_id 
	INNER JOIN
		dbo.CFW_Loc_Master AS L 
	ON 
		CFM.Grp_id = L.Loc_id
	WHERE        
		(ISNULL(DFC.bitCustom, 0) = 1)
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'V '
                    AND NAME = 'View_DynamicDefaultColumns' ) 
    DROP VIEW View_DynamicDefaultColumns
GO
CREATE VIEW [dbo].[View_DynamicDefaultColumns]
AS  SELECT  DFM.numFieldID,
            DFM.numModuleID,
            ISNULL(DFV.vcNewFormFieldName,
                   ISNULL(DFFM.vcFieldName, DFM.vcFieldName)) AS vcFieldName,
            DFG.vcNewFieldName AS vcCultureFieldName,
            DFG.numCultureID,
            DFM.vcDbColumnName,
            ISNULL(DFFM.vcPropertyName, DFM.vcPropertyName) AS vcPropertyName,
            DFM.vcLookBackTableName,
            DFM.vcFieldDataType,
            DFM.vcFieldType,
            ISNULL(DFFM.vcAssociatedControlType, DFM.vcAssociatedControlType) AS vcAssociatedControlType,
            ISNULL(DFV.vcToolTip, DFM.vcToolTip) AS vcToolTip,
            DFG.vcToolTip AS vcCultureToolTip,
            ISNULL(DFM.vcListItemType, '') AS vcListItemType,
            DFM.numListID,
            ISNULL(DFFM.PopupFunctionName, DFM.PopupFunctionName) AS PopupFunctionName,
            ISNULL(DFFM.[Order] + 1, DFM.[Order] + 1) AS tintOrder,
            ISNULL(DFFM.tintRow, ISNULL(DFM.tintRow, 0)) AS tintRow,
            ISNULL(DFFM.tintColumn, ISNULL(DFM.tintColumn, 0)) AS tintColumn,
            ISNULL(DFFM.bitInResults, ISNULL(DFM.bitInResults, 0)) AS bitInResults,
            ISNULL(DFFM.bitDeleted, ISNULL(DFM.bitDeleted, 0)) AS bitDeleted,
            ISNULL(DFFM.bitAllowEdit, ISNULL(DFM.bitAllowEdit, 0)) AS bitAllowEdit,
            ISNULL(DFFM.bitDefault, ISNULL(DFM.bitDefault, 0)) AS bitDefault,
            ISNULL(DFFM.bitSettingField, 0) AS bitSettingField,
            ISNULL(DFFM.bitAddField, 0) AS bitAddField,
            ISNULL(DFFM.bitDetailField, 0) AS bitDetailField,
            ISNULL(DFFM.bitAllowSorting, ISNULL(DFM.bitAllowSorting, 0)) AS bitAllowSorting,
            ISNULL(DFFM.bitImport, ISNULL(DFM.bitImport, 0)) AS bitImport,
            ISNULL(DFFM.bitExport, ISNULL(DFM.bitExport, 0)) AS bitExport,
            ISNULL(DFFM.bitAllowFiltering, ISNULL(DFM.bitAllowFiltering, 0)) AS bitAllowFiltering,
            ISNULL(DFFM.bitInlineEdit, ISNULL(DFM.bitInlineEdit, 0)) AS bitInlineEdit,
            ISNULL(DFFM.bitRequired, ISNULL(DFM.bitRequired, 0)) AS bitRequired,
            DFM.intFieldMaxLength,
            ISNULL(DFV.bitIsRequired, 0) bitIsRequired,
            ISNULL(DFV.bitIsEmail, 0) bitIsEmail,
            ISNULL(DFV.bitIsAlphaNumeric, 0) bitIsAlphaNumeric,
            ISNULL(DFV.bitIsNumeric, 0) bitIsNumeric,
            ISNULL(DFV.bitIsLengthValidation, 0) bitIsLengthValidation,
            DFV.intMaxLength,
            DFV.intMinLength,
            ISNULL(DFV.bitFieldMessage, 0) bitFieldMessage,
            ISNULL(DFV.vcFieldMessage, '') vcFieldMessage,
            CASE WHEN DFM.vcAssociatedControlType = 'SelectBox'
                 THEN ISNULL(( SELECT   numPrimaryListID
                               FROM     FieldRelationship
                               WHERE    numSecondaryListID = DFM.numListID
                                        AND numDomainID = D.numDomainID
                             ), 0)
                 ELSE 0
            END AS ListRelID,
            DFFM.numFormId,
            D.numDomainID,
            0 AS bitCustom,
            ISNULL(DFFM.intSectionID, 0) AS intSectionID,
            vcOrigDbColumnName,
            ISNULL(DFFM.bitWorkFlowField, ISNULL(DFM.bitWorkFlowField, 0)) AS bitWorkFlowField,
            ISNULL(DFM.intColumnWidth, 0) AS intColumnWidth,
            ISNULL(DFFM.bitAllowGridColor, 0) AS bitAllowGridColor,
			DFM.vcGroup, 
            ISNULL(DFM.intWFCompare,0) as  intWFCompare,
			DFM.vcWFCompareField


    FROM    DycFormField_Mapping DFFM
            JOIN DycFieldMaster DFM ON DFFM.numModuleID = DFFM.numModuleID
                                       AND DFFM.numFieldID = DFM.numFieldID
            JOIN Domain D ON ( DFM.numDomainID IS NULL
                               OR DFM.numDomainID = D.numDomainID
                             )
            LEFT JOIN DynamicFormField_Validation DFV ON DFV.numDomainID = D.numDomainID
                                                         AND DFFM.numFormID = DFV.numFormID
                                                         AND DFM.numFieldID = DFV.numFormFieldID
            LEFT JOIN DycField_Globalization DFG ON DFG.numDomainID = D.numDomainID
                                                    AND DFM.numFieldID = DFG.numFieldID
                                                    AND DFM.numModuleID = DFG.numModuleID
--where DFM.bitDefault=1
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- created By Anoop Jayaraj  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE  xtype = 'V' AND NAME ='View_ShippingBox')
DROP VIEW View_ShippingBox
GO
CREATE VIEW View_ShippingBox
AS 

    SELECT  SB.[numBoxID],
			SRI.ShippingReportItemId,
            SB.[vcBoxName],
            SB.[numShippingReportId],
            SB.[fltTotalWeight],
            SB.[fltHeight],
            SB.[fltWidth],
            SB.[fltLength],
			SB.dtDeliveryDate,
			ISNULL(SRI.monShippingRate,ISNULL(SB.monShippingRate,0)) [monShippingRate],
			SB.vcShippingLabelImage,
            SB.vcTrackingNumber,
            SRI.[numOppBizDocItemID],
            OBI.[numItemCode],
			SRI.[fltTotalWeight] fltTotalWeightItem,
            SRI.[fltHeight] fltHeightItem,
            SRI.[fltWidth] fltWidthItem,
            SRI.[fltLength] fltLengthItem,
			OI.numOppId,
			OBI.numOppBizDocID,
            OI.[numoppitemtCode],
--            I.[fltHeight],
--            I.[fltWidth],
--            I.[fltLength],
            I.[fltWeight],
            ISNULL(OI.vcItemName,'') AS [vcItemName],
            OI.[vcModelID],
            ISNULL(OBI.[vcItemDesc],I.[vcItemName]) [vcItemDesc],
			SR.numDomainId,
			SR.tintPayorType,
			ISNULL(SR.vcPayorAccountNo,'') vcPayorAccountNo,
			ISNULL(SR.vcPayorZip,'') vcPayorZip,
			ISNULL(SR.numPayorCountry,0) numPayorCountry,
			ISNULL(SR.vcValue2,0) AS tintServiceType,
			ISNULL((CASE WHEN ISNULL(SB.numPackageTypeID,0) = 0 THEN ISNULL(CP.numPackageTypeID,0) ELSE ISNULL(SB.numPackageTypeID,0) END) ,0) AS [numPackageTypeID],
			CP.vcPackageName,
			ISNULL(SRI.intBoxQty,0) AS [intBoxQty],
			ISNULL(SB.numServiceTypeID,0) AS [numServiceTypeID],
			ISNULL(fltDimensionalWeight,0) AS [fltDimensionalWeight],
			CAST(ROUND(((SELECT SUM(fltTotalWeight) FROM dbo.ShippingReportItems WHERE numShippingReportId = SR.numShippingReportId) * SRI.intBoxQty), 2) AS DECIMAL(9,2)) AS [fltTotalRegularWeight],
			ISNULL(SB.numShipCompany,0) AS [numShipCompany],
			CASE WHEN ISNULL(OI.numUOMId,0) = 0 AND ISNULL(I.[numSaleUnit],0) > 0
			THEN 
				(SELECT ISNULL(vcUnitName,'') FROM dbo.UOM 
				WHERE dbo.UOM.numUOMId = I.[numSaleUnit]
				AND dbo.UOM.numDomainId = SR.numDomainId) 
			WHEN ISNULL(OI.numUOMId,0) > 0 
			THEN
				(SELECT ISNULL(vcUnitName,'') FROM dbo.UOM 
				WHERE dbo.UOM.numUOMId = OI.numUOMId 
				AND dbo.UOM.numDomainId = SR.numDomainId) 
			ELSE
				(
					(CASE WHEN D.[charUnitSystem] = 'E' 
						  THEN (SELECT TOP 1 [U].[vcUnitName] FROM [dbo].[UOM] AS U WHERE [U].[vcUnitName] LIKE 'Unit%' AND [U].[numDomainId] = D.[numDomainId] AND [U].[tintUnitType] = 1) 
						  ELSE (SELECT TOP 1 [U].[vcUnitName] FROM [dbo].[UOM] AS U WHERE [U].[vcUnitName] LIKE 'Unit%' AND [U].[numDomainId] = D.[numDomainId] AND [U].[tintUnitType] = 2) 
					END )
				)
			END AS [vcUnitName],
			 CONVERT(VARCHAR, CONVERT(DECIMAL(18, 4), dbo.fn_UOMConversion(ISNULL(OI.numUOMId, 0), I.numItemCode,SR.numDomainId, ISNULL(I.numBaseUnit, 0)) * OBI.monPrice)) monUnitPrice
    FROM    [ShippingBox] SB
            LEFT JOIN [ShippingReportItems] SRI ON SB.[numBoxID] = SRI.[numBoxID]
			LEFT JOIN dbo.ShippingReport SR ON SRI.numShippingReportId = SR.numShippingReportId
            LEFT JOIN [OpportunityBizDocItems] OBI ON SRI.[numOppBizDocItemID] = OBI.[numOppBizDocItemID]
            LEFT JOIN [OpportunityItems] OI ON OBI.[numOppItemID] = OI.[numoppitemtCode]
            LEFT JOIN dbo.CustomPackages CP ON CP.numCustomPackageID = SB.numPackageTypeID 
											AND (ISNULL(CP.numShippingCompanyID,0) = SB.numShipCompany OR ISNULL(CP.numShippingCompanyID,0) = 0)
            LEFT JOIN [Item] I ON OBI.[numItemCode] = I.[numItemCode]	
			JOIN [dbo].[Domain] AS D ON I.[numDomainID] = D.[numDomainId]
GO            

----------------------- FUNCTIONS

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='CheckIfPriceRuleApplicableToItem')
DROP FUNCTION CheckIfPriceRuleApplicableToItem
GO
CREATE FUNCTION [dbo].[CheckIfPriceRuleApplicableToItem](@numRuleID NUMERIC(18,0), @numItemID NUMERIC(18,0), @numDivisionID NUMERIC(18,0))  
RETURNS BIT  
AS  
BEGIN  

DECLARE @isRuleApplicable BIT = 0;

DECLARE @isStep2RuleApplicable BIT = 0;
DECLARE @isStep3RuleApplicable BIT = 0;

DECLARE @tempStep2 INT;
DECLARE @tempStep3 INT;

SELECT @tempStep2=tintStep2, @tempStep3=tintStep3  FROM PriceBookRules WHERE numPricRuleID = @numRuleID

/* Check if rule is applied to given item */
IF @tempStep2 = 1 --Apply to items individually
BEGIN
	IF (SELECT COUNT(*) FROM PriceBookRuleItems WHERE numRuleID = @numRuleID AND numValue = @numItemID) > 0
		SET @isStep2RuleApplicable = 1
END
ELSE IF @tempStep2 = 2 --Apply to items with the selected item classifications
BEGIN
	IF (SELECT COUNT(*) FROM PriceBookRuleItems WHERE numRuleID = @numRuleID AND numValue = (SELECT numItemClassification FROM Item WHERE numItemCode = @numItemID)) > 0
		SET @isStep2RuleApplicable = 1
END
ELSE IF @tempStep2 = 3 -- All Items
BEGIN
	SET @isStep2RuleApplicable = 1
END

/* If step 2 rule is not applicable then there is no need to go for check in step 3 */
IF @isStep2RuleApplicable = 1
BEGIN
	IF @tempStep3 = 1 --Apply to customers individually
	BEGIN
		IF (SELECT COUNT(*) FROM PriceBookRuleDTL WHERE numRuleID = @numRuleID AND numValue = @numDivisionID) > 0
			SET @isStep3RuleApplicable = 1
	END
	ELSE IF @tempStep3 = 2 --Apply to customers with the following Relationships & Profiles
	BEGIN
		IF (
			SELECT 
				COUNT(*) 
			FROM 
				PriceBookRuleDTL 
			WHERE 
				numRuleID = @numRuleID AND 
				numValue = (
								SELECT 
									ISNULL(numCompanyType,0) 
								FROM 
									CompanyInfo 
								WHERE 
									numCompanyId = (SELECT numCompanyID FROM DivisionMaster WHERE numDivisionID = @numDivisionID)
							) AND 
				numProfile = (
								SELECT 
									ISNULL(vcProfile,0) 
								FROM 
									CompanyInfo 
								WHERE 
									numCompanyId = (SELECT numCompanyID FROM DivisionMaster WHERE numDivisionID = @numDivisionID))
			) > 0
			SET @isStep3RuleApplicable = 1
	END
	ELSE IF @tempStep3 = 3 --All Customers
	BEGIN
		SET @isStep3RuleApplicable = 1
	END
END

IF @isStep2RuleApplicable = 1 AND @isStep3RuleApplicable = 1
BEGIN
	SET @isRuleApplicable = 1
END  

return @isRuleApplicable
END
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='CheckOrderInventoryStatus')
DROP FUNCTION CheckOrderInventoryStatus
GO
CREATE FUNCTION [dbo].[CheckOrderInventoryStatus] 
 (
      @numOppID AS NUMERIC(9),
      @numDomainID AS NUMERIC(9)
    )
RETURNS NVARCHAR(MAX)
AS BEGIN

	DECLARE @vcInventoryStatus AS NVARCHAR(MAX);SET @vcInventoryStatus='<font color="#000000">Not Applicable<font>';
	
	IF EXISTS(SELECT tintshipped FROM dbo.OpportunityMaster WHERE numDomainId=@numDomainID 
			  AND numOppId=@numOppID AND ISNULL(tintshipped,0)=1)
	BEGIN
		SET @vcInventoryStatus='<font color="#008000">Allocation Cleared<font>';
	END
	ELSE
	BEGIN
		DECLARE @ItemCount AS INT;SET @ItemCount=0
		DECLARE @BackOrderItemCount AS INT;SET @BackOrderItemCount=0

		
		SELECT 
			@ItemCount = ISNULL(COUNT(*),0),
			@BackOrderItemCount = ISNULL(SUM(CAST(bitBackOrder AS INT)),0)
		FROM
		(SELECT
			(CASE 
				WHEN ISNULL(bitKitParent,0) = 1 AND ISNULL(bitAssembly,0) = 1 
				THEN 0
                WHEN ISNULL(bitKitParent,0) = 1 THEN 1
                ELSE 0
             END) as bitKitParent,
			IIF(dbo.CheckOrderItemInventoryStatus(Opp.numItemCode,Opp.numUnitHour,Opp.numoppitemtCode,isnull(Opp.numWarehouseItmsID,0),bitKitParent) > 0,1,0) AS bitBackOrder
		FROM dbo.OpportunityItems Opp
			JOIN item I ON Opp.numItemCode = i.numItemcode
			JOIN WareHouseItems WItems ON Opp.numItemCode = WItems.numItemID 
			AND WItems.numWareHouseItemID = Opp.numWarehouseItmsID
			WHERE numOppId=@numOppID AND I.numDomainID=@numDomainID AND charItemType='P'
			and (bitDropShip=0 or bitDropShip is null)) AS TEMP    
		
	    
		IF @ItemCount>0 
		BEGIN
			IF @BackOrderItemCount>0
				SET @vcInventoryStatus = '<font color="#FF0000">Back Order (' + CAST(@BackOrderItemCount AS VARCHAR(18)) + '/' + CAST(@ItemCount AS VARCHAR(18)) +')<font>'
			ELSE
				SET @vcInventoryStatus = '<font color="#800080">Ready to Ship<font>';			
		END 
	END
    
    RETURN ISNULL(@vcInventoryStatus,'')
END

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='CheckOrderItemInventoryStatus')
DROP FUNCTION CheckOrderItemInventoryStatus
GO
CREATE FUNCTION [dbo].[CheckOrderItemInventoryStatus] 
(
	@numItemCode NUMERIC(18,0) = NULL,
	@numQuantity AS INT = 0,
	@numOppItemCode AS NUMERIC(18,0),
	@numWarehouseItemID NUMERIC(18,0),
	@bitKitParent BIT 
)
RETURNS INT
AS
BEGIN

    DECLARE @bitBackOrder INT = 0
	DECLARE @numAllocation INT
	DECLARE @numQtyShipped INT = 0

	SELECT @numQtyShipped = ISNULL(numQtyShipped,0) FROM OpportunityItems WHERE numoppitemtCode = @numOppItemCode

	IF @bitKitParent = 1
	BEGIN
		DECLARE @numBackOrder INT

		WITH CTE(numItemKitID, numItemCode, numWarehouseItmsID, numQtyItemsReq, numCalculatedQty, numAllocation, numBackOrder )
		AS ( SELECT   
					CONVERT(NUMERIC(18, 0), 0),
					numItemCode,
					ISNULL(Dtl.numWareHouseItemId, 0) AS numWarehouseItmsID,
					DTL.numQtyItemsReq,
					CAST(DTL.numQtyItemsReq AS NUMERIC(9, 0)) AS numCalculatedQty,
					WareHouseItems.numAllocation,
					(
						CASE 
						WHEN (DTL.numQtyItemsReq * (@numQuantity - @numQtyShipped)) > WareHouseItems.numAllocation THEN 
							CAST((((DTL.numQtyItemsReq * (@numQuantity - @numQtyShipped))  - WareHouseItems.numAllocation)/DTL.numQtyItemsReq) AS DECIMAL)
						ELSE
						    CAST(0 AS DECIMAL)
						END
					) AS numBackOrder
			FROM     item
					INNER JOIN ItemDetails Dtl ON numChildItemID = numItemCode
					INNER JOIN WareHouseItems ON WareHouseItems.numWareHouseItemID = Dtl.numWareHouseItemId
			WHERE    numItemKitID = @numItemCode
			UNION ALL
			SELECT   Dtl.numItemKitID,
					i.numItemCode,
					ISNULL(Dtl.numWareHouseItemId, 0) AS numWarehouseItmsID,
					DTL.numQtyItemsReq,
					CAST(( DTL.numQtyItemsReq * c.numCalculatedQty ) AS NUMERIC(9, 0)) AS numCalculatedQty,
					WareHouseItems.numAllocation,
					(
						CASE 
						WHEN (CAST(( DTL.numQtyItemsReq * c.numCalculatedQty ) AS NUMERIC(9, 0)) * (@numQuantity - @numQtyShipped)) > WareHouseItems.numAllocation THEN 
							CAST((((CAST(( DTL.numQtyItemsReq * c.numCalculatedQty ) AS NUMERIC(9, 0)) * (@numQuantity - @numQtyShipped)) - WareHouseItems.numAllocation)/DTL.numQtyItemsReq) AS DECIMAL)
						ELSE
						    CAST(0 AS DECIMAL)
						END
					) AS numBackOrder
			FROM     item i
					INNER JOIN ItemDetails Dtl ON Dtl.numChildItemID = i.numItemCode
					INNER JOIN WareHouseItems ON WareHouseItems.numWareHouseItemID = Dtl.numWareHouseItemId
					INNER JOIN CTE c ON Dtl.numItemKitID = c.numItemCode
			WHERE    Dtl.numChildItemID != @numItemCode
		)
		SELECT @numBackOrder = MAX(numBackOrder) FROM CTE
		
		IF @numBackOrder > 0
			SET @bitBackOrder = @numBackOrder
		ELSE
			SET @bitBackOrder = 0
	END
	ELSE
	BEGIN
		SELECT
			@numAllocation = numAllocation
		FROM
			WareHouseItems 
		WHERE
			numWareHouseItemID = @numWarehouseItemID

		IF (@numQuantity - @numQtyShipped) > @numAllocation
            SET @bitBackOrder = (@numQuantity - @numQtyShipped) - @numAllocation
		ELSE
			SET @bitBackOrder = 0
	END

	RETURN @bitBackOrder

END
GO
     
/****** Object:  UserDefinedFunction [dbo].[fn_GetOpeningBalance]    Script Date: 10/05/2009 17:46:28 ******/

GO

GO
--DROP FUNCTION [dbo].[fn_GetOpeningBalance]
-- SELECT * FROM dbo.fn_GetOpeningBalance(',74,666,658,77,263,668,1156,1162,1166,1167,1170,1172,1174',72,'2009-01-01 00:00:00:000')
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='TF'AND NAME ='fn_getopeningbalance')
DROP FUNCTION fn_getopeningbalance
GO
CREATE FUNCTION [dbo].[fn_GetOpeningBalance]
(@vcChartAcntId as varchar(500),
@numDomainId as numeric(9),
@dtFromDate as DATETIME,
@ClientTimeZoneOffset Int  --Added by Chintan to enable calculation of date according to client machine
)                                 
RETURNS 

@COAOpeningBalance table
(
	numAccountId int,
	vcAccountName varchar(100),
	numParntAcntTypeID int,
	vcAccountDescription varchar(100),
	vcAccountCode varchar(50),
	dtAsOnDate datetime,
	mnOpeningBalance money
)                               
As                                  
Begin          

DECLARE @numFinYear INT;
DECLARE @dtFinFromDate datetime;

set @numFinYear= (SELECT numFinYearId FROM FINANCIALYEAR WHERE dtPeriodFrom <= @dtFromDate AND  
dtPeriodTo >=@dtFromDate AND numDomainId= @numDomainId);

set @dtFinFromDate= (SELECT dtPeriodFrom  FROM FINANCIALYEAR WHERE dtPeriodFrom <= @dtFromDate AND  
dtPeriodTo >=@dtFromDate AND numDomainId= @numDomainId);


SET @dtFromDate = DateAdd(minute, -@ClientTimeZoneOffset, @dtFromDate);

INSERT INTO @COAOpeningBalance

SELECT  COA.numAccountId,vcAccountName,numParntAcntTypeID,vcAccountDescription,vcAccountCode,
 @dtFromDate,

--isnull((SELECT TOP 1 isnull(monOpening,0) from CHARTACCOUNTOPENING CAO WHERE
--numFinYearId=@numFinYear and numDomainID=@numDomainId and
--CAO.numAccountId=COA.numAccountId),0) 
--+
--CASE WHEN  COA.dtOpeningDate<= @dtFromDate THEN COA.numOriginalOpeningBal 
--ELSE 0 END
--+ 
(SELECT ISNULL(SUM(GJD.numDebitAmt),0) - ISNULL(SUM(GJD.numCreditAmt),0)
FROM
General_Journal_Details GJD 
JOIN Domain D ON D.numDOmainID = GJD.numDomainID AND COA.numDomainId = D.numDOmainID
INNER JOIN General_Journal_Header GJH ON GJD.numJournalId =GJH.numJournal_Id AND 
GJD.numDomainId=COA.numDomainId AND 
GJD.numChartAcntId=COA.numAccountId AND 
GJH.numDomainID = D.numDomainId AND 
--datEntry_Date BETWEEN (CASE WHEN COA.[vcAccountCode] LIKE '0103%' OR 
--								 COA.[vcAccountCode] LIKE '0104%' OR 
--								 COA.[vcAccountCode] LIKE '0106%' OR
--								 COA.[vcAccountCode] LIKE '0105%' 
--							THEN @dtFinFromDate 
--							ELSE @dtFromDate 
--					   END ) AND  DATEADD(Minute,-1,@dtFromDate))
GJH.datEntry_Date <= @dtFromDate /*BETWEEN @dtFinFromDate AND @dtFromDate*/ ) /*DATEADD(DAY,-1,@dtFromDate) removed as Date we are passing is 12AM which is 1 second more, compare to opening balance of pervious date */
FROM Chart_of_Accounts COA
WHERE COA.numDomainId=@numDomainId AND
	COA.numAccountId in (select CAST(ID AS NUMERIC(9)) from SplitIDs(@vcChartAcntId,','));

RETURN 

END
GO
--select dbo.GetDealAmount(1133,getdate(),1610)
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='getdealamount')
DROP FUNCTION getdealamount
GO
CREATE FUNCTION [dbo].[GetDealAmount]
(@numOppID numeric,@dt Datetime,@numOppBizDocsId numeric=0)    
returns money    
as    
begin   
  
  
declare @DivisionID as numeric(9)                         
                                  
declare @TaxPercentage as float                    
  
declare @shippingAmt as money    
declare @TotalTaxAmt as money     
declare @decDisc as float 
declare @bitDiscType as bit ;set @bitDiscType=0     
declare @decInterest as float    
    
set @decInterest=0    
set @decDisc=0    
declare @bitBillingTerms as bit     
declare @intBillingDays as integer    
declare @bitInterestType as bit    
declare @fltInterest as float    
declare @dtFromDate as varchar(20)    
declare @strDate as datetime    
declare @TotalAmt as money    
declare @TotalAmtAfterDisc as money    
declare @fltDiscount as float  
declare @tintOppStatus as tinyint  
declare @tintshipped as tinyint 
declare @dtshipped as datetime 
declare @numBillCountry as numeric(9)
declare @numBillState as numeric(9)
declare @numDomainID as numeric(9)
declare @tintOppType tinyint
declare @bitTaxApplicable BIT
set @TotalAmt=0


--declare @monCreditAmount as money;set @monCreditAmount=0    
DECLARE @tintTaxOperator AS TINYINT;SET @tintTaxOperator=0

select    
@DivisionID=numDivisionID,@shippingAmt=0,    
@bitBillingTerms=0,    
@intBillingDays=0,@bitInterestType=0,    
@fltInterest=0,@fltDiscount=0,
@tintOppStatus=tintOppStatus ,@tintshipped=tintshipped,@numDomainID=numDomainID,@tintOppType=tintOpptype,
@tintTaxOperator=ISNULL([tintTaxOperator],0)
from OpportunityMaster where numOppId=@numOppId          
   

IF @numOppBizDocsId=0
BEGIN
	select @TotalAmt=isnull(sum(X.amount),0)    
			from (SELECT monTotAmount as Amount from OpportunityItems opp                   
					join item i on opp.numItemCode=i.numItemCode where numOppId=@numOppId  
			 union ALL --timeandexpense (expense)  
			  SELECT monAmount as amount from timeandexpense   
			   where (numOppId=@numOppId or numOppBizDocsId in (select numOppBizDocsId from OpportunityBizDocs where numOppId=@numOppId ) )  
			   and numCategory = 2 and numtype=1  
			)x  
END
ELSE
BEGIN
SELECT @bitBillingTerms=isnull(bitBillingTerms,0),    
	@intBillingDays=isnull(intBillingDays,0),@bitInterestType=isnull(bitInterestType,0),    
	@fltInterest=isnull(fltInterest,0),@fltDiscount=isnull(fltDiscount,0),
	@bitDiscType=isnull(bitDiscountType,0)
FROM dbo.OpportunityMaster WHERE numOppId=@numOppID

	select    
	@shippingAmt=isnull(monShipCost,0),    
	@dtshipped=dtShippedDate,
--	@monCreditAmount=isnull(monCreditAmount,0),
	@dtFromDate=dtFromDate
	from OpportunityBizDocs where numOppBizDocsId=@numOppBizDocsId

select @TotalAmt=isnull(sum(X.amount),0)
from (SELECT OBD.monTotAmount as Amount from OpportunityItems opp                   
		join OpportunityBizDocItems OBD on OBD.numOppItemID=Opp.numoppitemtCode    
		where numOppId=@numOppId  and OBD.numOppBizDocID=@numOppBizDocsId
 
	union ALL  --timeandexpense (expense)  
	 select monAmount as amount from timeandexpense  where (numOppId=@numOppId or numOppBizDocsId =@numOppBizDocsId )  
		and numCategory = 2 and numtype=1  
)x  

END

	SET @TotalTaxAmt=isnull(dbo.[GetDealTaxAmount](@numOppId,@numOppBizDocsId,@numDOmainId),0)


	--IF @tintOppType=1 OR (@tintOppType=2 and (select isnull(bitPurchaseTaxCredit,0) from Domain where numDomainId=@numDomainId)=1)
	--BEGIN
	--	SET @TotalTaxAmt=0
		
	--	 DECLARE @numTaxItemID AS NUMERIC(9)
		
	--	 select top 1 @numTaxItemID=numTaxItemID from OpportunityMasterTaxItems
	--  	 where numOppId=@numOppId  order by numTaxItemID 
 
	--	 while @numTaxItemID>-1    
	--	 begin
	--		SELECT @TotalTaxAmt= @TotalTaxAmt+ ISNULL(dbo.fn_CalOppItemTotalTaxAmt(@numDomainID,@numTaxItemID,@numOppID,@numOppBizDocsId),0)
    
	--		select top 1 @numTaxItemID=numTaxItemID from OpportunityMasterTaxItems 
	--		where numOppId=@numOppId  and numTaxItemID>@numTaxItemID  order by numTaxItemID     
    
	--		if @@rowcount=0 set @numTaxItemID=-1    
	--	end   
	--END    
	--ELSE IF @tintOppType=2
	--BEGIN
	--	 SET @TotalTaxAmt=0
	--END

set @TotalAmtAfterDisc= @TotalAmt 
if @tintOppStatus=1 
begin  
   
    if @tintshipped=1 set @dt=isnull(@dtshipped,@dt)
	if  @bitBillingTerms=1    
	begin    
		--set @strDate = DateAdd(Day, convert(int,ISNULL(dbo.fn_GetListItemName(isnull(@intBillingDays,0)),0)),@dtFromDate)    
		set @strDate = DateAdd(Day, convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(@intBillingDays,0)),0)),@dtFromDate)    
		--(SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(oppmst.intBillingDays,0))
	     
		if @bitInterestType=0     
		begin    
			if @strDate>@dt    
			begin    
				set @decDisc=0    
			end    
			else    
			begin    
				set @decDisc=@fltInterest    
	       
			end    
		end    
		ELSE    
		begin    
			if @strDate>@dt    
			begin    
				set @decInterest=0    
			end    
			else    
			begin    
				set @decInterest=@fltInterest    
			end    
		end    
	end    
	 
	if @decDisc>0  
	begin      
          if @bitDiscType=0 set @TotalAmtAfterDisc=@TotalAmt-((@TotalAmt*@fltDiscount/100)+((@TotalAmt*(100-@fltDiscount)/100)*@decDisc/100))+@shippingAmt+@TotalTaxAmt  
          else if @bitDiscType=1  set @TotalAmtAfterDisc=@TotalAmt-(@fltDiscount+(@TotalAmt-@fltDiscount)*@decDisc/100)+@shippingAmt+@TotalTaxAmt  
	end    
	else  
	begin 
		if @bitDiscType=0 	set @TotalAmtAfterDisc=@TotalAmt+(@decInterest*(@TotalAmt*(100-@fltDiscount)/100)/100)-(@TotalAmt*@fltDiscount/100)+@shippingAmt+@TotalTaxAmt    
		else if @bitDiscType=1 	set @TotalAmtAfterDisc=@TotalAmt+(@decInterest*(@TotalAmt-@fltDiscount)/100)-(@fltDiscount)+@shippingAmt+@TotalTaxAmt    
	 end  
end 
else
begin
	if @bitDiscType=0 	set @TotalAmtAfterDisc=@TotalAmt+@shippingAmt+@TotalTaxAmt -(@TotalAmt*@fltDiscount/100)
	else if @bitDiscType=1 	set @TotalAmtAfterDisc=@TotalAmt+@shippingAmt+@TotalTaxAmt -@fltDiscount
end

 --SET @TotalAmtAfterDisc = @TotalAmtAfterDisc - isnull(@monCreditAmount,0)
SET @TotalAmtAfterDisc = ROUND(@TotalAmtAfterDisc,2)

return CAST(@TotalAmtAfterDisc AS MONEY) -- Set Accuracy of Two precision
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetDealTaxAmount')
DROP FUNCTION GetDealTaxAmount
GO
CREATE FUNCTION [dbo].[GetDealTaxAmount]
(@numOppID numeric,@numOppBizDocsId numeric=0/*,@tintOppType tinyint*/,@numDomainId numeric)    
returns money    
as    
begin   
  
  
  declare @TotalTaxAmt as money     
	SET @TotalTaxAmt=0

	--IF @tintOppType=1 OR (@tintOppType=2 and (select isnull(bitPurchaseTaxCredit,0) from Domain where numDomainId=@numDomainId)=1)
	--BEGIN
		SET @TotalTaxAmt=0
		
		 DECLARE @numTaxItemID AS NUMERIC(9)
		
		 select top 1 @numTaxItemID=numTaxItemID from OpportunityMasterTaxItems
	  	 where numOppId=@numOppId  order by numTaxItemID 
 
		 while @numTaxItemID>-1    
		 begin
			SELECT @TotalTaxAmt= @TotalTaxAmt+ ISNULL(dbo.fn_CalOppItemTotalTaxAmt(@numDomainID,@numTaxItemID,@numOppID,@numOppBizDocsId),0)
    
			select top 1 @numTaxItemID=numTaxItemID from OpportunityMasterTaxItems 
			where numOppId=@numOppId  and numTaxItemID>@numTaxItemID  order by numTaxItemID     
    
			if @@rowcount=0 set @numTaxItemID=-1    
		end   
	--END    
  
return CAST(@TotalTaxAmt AS money) -- Set Accuracy of Two precision
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='TF'AND NAME ='GetDFHistoricalSalesOppData')
DROP FUNCTION GetDFHistoricalSalesOppData
GO
CREATE FUNCTION [dbo].[GetDFHistoricalSalesOppData] 
 (
	  @numDFID AS NUMERIC(18,0),
      @numDomainID AS NUMERIC(9),
	  @dtFromDate AS DATETIME,
	  @dtToDate AS DATETIME
    )
RETURNS  @TEMP TABLE 
(
	numItemCode NUMERIC(18,0), 
	numWarehouseItmsID NUMERIC(18,0),
	numQtySold INTEGER
)
AS BEGIN 
		DECLARE @TEMPOPP TABLE
		(
			numOppID NUMERIC(18,0),
			numPercentageComplete NUMERIC(18,0),
			numItemCode NUMERIC(18,0),
			numWarehouseItmsID NUMERIC(18,0),
			numUnitHour INTEGER,
			charItemType CHAR(1),
			bitKitParent BIT,
			bitAssembly BIT
		)

	
		DECLARE @ITEMTABLE TABLE
		(
			numItemCode NUMERIC(18,0), 
			numWarehouseItmsID NUMERIC(18,0),
			numQtySold INTEGER
		)

		--GET Sales Opportunity(tintOppType = 1 AND tintOppStatus = 0) Created Between Dates
		INSERT INTO	
			@TEMPOPP
		SELECT
			OpportunityMaster.numOppId,
			(ISNULL((SELECT CAST(ISNULL(vcData,0) AS DECIMAL) FROM ListDetails WHERE numListItemID = OpportunityMaster.numPercentageComplete),0)/100),
			Item.numItemCode,
			OpportunityItems.numWarehouseItmsID,
			OpportunityItems.numUnitHour,
			Item.charItemType,
			Item.bitKitParent,
			Item.bitAssembly
		FROM
			OpportunityMaster
		LEFT JOIN
			OpportunityItems
		ON
			OpportunityMaster.numOppId = OpportunityItems.numOppId
		INNER JOIN
			Item
		ON
			OpportunityItems.numItemCode = Item.numItemCode
		LEFT JOIN
			WareHouseItems
		ON
			OpportunityItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
		WHERE
			OpportunityMaster.numDomainId = @numDomainID AND
			Item.charItemType = 'P' AND
			CONVERT(DATE,OpportunityMaster.bintCreatedDate) BETWEEN CONVERT(DATE,@dtFromDate) AND CONVERT(DATE,@dtToDate) AND
			OpportunityMaster.tintOppType = 1 AND 
			OpportunityMaster.tintOppStatus = 0 AND
			ISNULL(OpportunityMaster.numPercentageComplete,0) > 0 AND
			(
				(SELECT COUNT(*) FROM DemandForecastWarehouse WHERE numDFID = @numDFID) = 0 OR
				WareHouseItems.numWareHouseID IN (SELECT numWareHouseID FROM DemandForecastWarehouse WHERE numDFID = @numDFID)
			)
				
		--Get total quantity of each inventory or assembly item sold in Sales Opportunity(tintOppType = 1 AND tintOppStatus = 0)
		INSERT INTO 
			@ITEMTABLE
		SELECT
			TEMP.numItemCode,
			TEMP.numWarehouseItmsID,
			SUM(TEMP.QuantitySold) AS numQtySold
		FROM
			(SELECT
				t1.numOppId,
				t1.numItemCode,
				t1.numWarehouseItmsID, 
				CEILING(SUM(t1.numUnitHour) * ISNULL(t1.numPercentageComplete,0)) AS QuantitySold
			FROM
				@TEMPOPP AS t1
			WHERE
				(t1.bitKitParent = 0 OR (t1.bitKitParent = 1 AND t1.bitAssembly = 1)) 
			GROUP BY
				t1.numOppId,
				t1.numItemCode,
				t1.numWarehouseItmsID,
				t1.numPercentageComplete
			) AS TEMP
		GROUP BY
			TEMP.numItemCode,
			TEMP.numWarehouseItmsID


		--Get total quantity of each child inventory item based on quantity of kit item sold in Sales Opportunity(tintOppType = 1 AND tintOppStatus = 0)
		INSERT INTO 
			@ITEMTABLE
		SELECT
			TEMP.numItemCode,
			TEMP.numWarehouseItmsID,
			SUM(TEMP.QuantitySold) AS numQtySold
		FROM
			(SELECT
				t1.numOppId,
				t1.numPercentageComplete,
				KitChildItemsTable.numItemCode,
				KitChildItemsTable.numWarehouseItmsID, 
				CEILING((SUM(t1.numUnitHour) * ISNULL(t1.numPercentageComplete,0)) * ISNULL(SUM(KitChildItemsTable.numQtyItemsReq),0)) AS QuantitySold
			FROM
				@TEMPOPP AS t1
			CROSS APPLY
			(
				SELECT  
						numItemCode,
						ISNULL(Dtl.numWareHouseItemId, 0) AS numWarehouseItmsID,
						DTL.numQtyItemsReq
				FROM    
					Item
				INNER JOIN 
					ItemDetails Dtl 
				ON 
					numChildItemID = numItemCode
				INNER JOIN 
					WareHouseItems 
				ON 
					WareHouseItems.numWareHouseItemID = Dtl.numWareHouseItemId
				WHERE    
					numItemKitID = t1.numItemCode AND
					Item.charItemType = 'P'
			) AS KitChildItemsTable
			WHERE
				(t1.bitKitParent = 1 AND t1.bitAssembly = 0) 
			GROUP BY
				t1.numOppId,
				t1.numPercentageComplete,
				KitChildItemsTable.numItemCode,
				KitChildItemsTable.numWarehouseItmsID
			) AS TEMP
		GROUP BY
			TEMP.numItemCode,
			TEMP.numWarehouseItmsID

		INSERT INTO 
			@TEMP
		SELECT
			numItemCode, 
			numWarehouseItmsID,
			SUM(numQtySold)
		FROM	
			@ITEMTABLE
		GROUP BY
			numItemCode, 
			numWarehouseItmsID

		RETURN
END
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='TF'AND NAME ='GetDFHistoricalSalesOrderData')
DROP FUNCTION GetDFHistoricalSalesOrderData
GO
CREATE FUNCTION [dbo].[GetDFHistoricalSalesOrderData] 
 (
	  @numDFID AS NUMERIC(18,0),
      @numDomainID AS NUMERIC(9),
	  @dtFromDate AS DATETIME,
	  @dtToDate AS DATETIME
    )
RETURNS  @TEMP TABLE 
(
	numItemCode NUMERIC(18,0), 
	numWarehouseItmsID NUMERIC(18,0),
	numQtySold INTEGER
)
AS BEGIN 
	
		DECLARE @TEMPORDER TABLE
		(
			numOppID NUMERIC(18,0),
			numItemCode NUMERIC(18,0),
			numWarehouseItmsID NUMERIC(18,0),
			numUnitHour INTEGER,
			charItemType CHAR(1),
			bitKitParent BIT,
			bitAssembly BIT
		)

		DECLARE @ITEMTABLE TABLE
		(
			numItemCode NUMERIC(18,0), 
			numWarehouseItmsID NUMERIC(18,0),
			numQtySold INTEGER
		)


		INSERT INTO	
			@TEMPORDER
		SELECT
			OpportunityMaster.numOppId,			
			Item.numItemCode,
			OpportunityItems.numWarehouseItmsID,
			OpportunityItems.numUnitHour,
			Item.charItemType,
			Item.bitKitParent,
			Item.bitAssembly
		FROM
			OpportunityMaster
		LEFT JOIN
			OpportunityItems
		ON
			OpportunityMaster.numOppId = OpportunityItems.numOppId
		INNER JOIN
			Item
		ON
			OpportunityItems.numItemCode = Item.numItemCode
		LEFT JOIN
			WareHouseItems
		ON
			OpportunityItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
		WHERE
			OpportunityMaster.numDomainId = @numDomainID AND
			Item.charItemType = 'P' AND
			CONVERT(DATE,OpportunityMaster.bintCreatedDate) BETWEEN CONVERT(DATE,@dtFromDate) AND CONVERT(DATE,@dtToDate) AND
			OpportunityMaster.tintOppType = 1 AND 
			OpportunityMaster.tintOppStatus = 1 AND
			(
				(SELECT COUNT(*) FROM DemandForecastWarehouse WHERE numDFID = @numDFID) = 0 OR
				WareHouseItems.numWareHouseID IN (SELECT numWareHouseID FROM DemandForecastWarehouse WHERE numDFID = @numDFID)
			)

		-- Get total quantity of inventoty or assembly item in Sales Order(tintOppType = 1 AND tintOppStatus = 1)
		INSERT INTO
			@ITEMTABLE
		SELECT
			t1.numItemCode, 
			t1.numWarehouseItmsID,
			SUM(t1.numUnitHour) AS numQtySold
		FROM
			@TEMPORDER AS t1
		WHERE
			(t1.bitKitParent = 0 OR (t1.bitKitParent = 1 AND t1.bitAssembly = 1))
		GROUP BY
			t1.numItemCode,
			t1.numWarehouseItmsID


		-- Get sum of total child inventory items quantity based on kit quantity sold in Sales Order(tintOppType = 1 AND tintOppStatus = 1)
		INSERT INTO
			@ITEMTABLE
		SELECT
			KitChildItemsTable.numItemCode, 
			KitChildItemsTable.numWarehouseItmsID,
			SUM(ISNULL(t1.numUnitHour,0) * ISNULL(KitChildItemsTable.numQtyItemsReq,0)) AS numQtySold
		FROM
			@TEMPORDER AS t1
		CROSS APPLY
			(
				SELECT  
						numItemCode,
						ISNULL(Dtl.numWareHouseItemId, 0) AS numWarehouseItmsID,
						DTL.numQtyItemsReq
				FROM    
					Item
				INNER JOIN 
					ItemDetails Dtl 
				ON 
					numChildItemID = numItemCode
				INNER JOIN 
					WareHouseItems 
				ON 
					WareHouseItems.numWareHouseItemID = Dtl.numWareHouseItemId
				WHERE    
					numItemKitID = t1.numItemCode AND
					Item.charItemType = 'P'
			) AS KitChildItemsTable
		WHERE
			(t1.bitKitParent = 1 AND t1.bitAssembly = 0)
		GROUP BY
			KitChildItemsTable.numItemCode, 
			KitChildItemsTable.numWarehouseItmsID


		INSERT INTO 
			@TEMP
		SELECT
			numItemCode, 
			numWarehouseItmsID,
			SUM(numQtySold)
		FROM	
			@ITEMTABLE
		GROUP BY
			numItemCode, 
			numWarehouseItmsID

		RETURN
END

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='TF'AND NAME ='GetItemPurchaseVendorPrice')
DROP FUNCTION GetItemPurchaseVendorPrice
GO
CREATE FUNCTION [dbo].[GetItemPurchaseVendorPrice] 
(
      @numDivisionID AS NUMERIC(18,0),
	  @numItemCode AS NUMERIC(18,0),
	  @numDomainID AS NUMERIC(18,0), 
	  @numUnits AS INTEGER
)
RETURNS @TEMP TABLE (numDivisionID NUMERIC(9),intMinQty INT, monPrice MONEY, intLeadTimeDays INT)
AS BEGIN
	DECLARE @numMinimumQty INT = 0
	DECLARE @intLeadTimeDays INT = 0
	DECLARE @monListPrice MONEY = 0
	DECLARE @numRelationship INT = 0
	DECLARE @numProfile AS NUMERIC(9)


	IF ISNULL(@numDivisionID,0) = 0
	BEGIN
		-- CHECK IF PRIMARY VENDOR EXISTS
		SELECT 
			@numDivisionID=V.numVendorID, 
			@numMinimumQty=V.intMinQty,
			@intLeadTimeDays = ISNULL(V.intLeadTimeDays,0)
		FROM 
			Item I 
		INNER JOIN 
			[Vendor] V 
		ON 
			V.numVendorID=I.numVendorID AND 
			V.numItemCode=I.numItemCode 
		WHERE 
			I.numItemCode = @numItemCode AND
			I.numDomainID = @numDomainID 


		-- IF PRIMARY VENDOR IS NOT AVAILABLE THEN SELECT ANY VENDOR AVAILABLE
		IF ISNULL(@numDivisionID,0) = 0
		BEGIN
			SELECT TOP 1
				@numDivisionID=V.numVendorID,
				@numMinimumQty=V.intMinQty,
				@intLeadTimeDays = ISNULL(V.intLeadTimeDays,0)
			FROM 
				[Vendor] V 
			WHERE
				V.[numItemCode]=@numItemCode AND V.[numDomainID]=@numDomainID 
		END
	END

	-- IF ANY VENDOR IS AVAILABLE THEN GO FOR GETTING PRICE
	IF ISNULL(@numDivisionID,0) > 0
	BEGIN
		SELECT 
			@numRelationship=numCompanyType,
			@numProfile=vcProfile 
		FROM 
			DivisionMaster D                  
		JOIN 
			CompanyInfo C 
		ON 
			C.numCompanyId=D.numCompanyID                  
		WHERE 
			numDivisionID =@numDivisionID   

		SELECT 
			@monListPrice=ISNULL([monCost],0),
			@numMinimumQty=V.intMinQty,
			@intLeadTimeDays = ISNULL(V.intLeadTimeDays,0)
		FROM 
			[Vendor] V 
		INNER JOIN 
			dbo.Item I 
		ON 
			V.numItemCode = I.numItemCode
		WHERE 
			V.[numItemCode]=@numItemCode AND 
			V.[numDomainID]=@numDomainID AND 
			V.numVendorID=@numDivisionID
	
		--CHECK IF PRICE RULE IS APPLICABLE
		SELECT
			@monListPrice = (CASE 
							WHEN dbo.GetPriceBasedOnPriceBook(P.[numPricRuleID],@monListPrice,@numUnits,I.numItemCode) > 0 
							THEN dbo.GetPriceBasedOnPriceBook(P.[numPricRuleID],@monListPrice,@numUnits,I.numItemCode) 
							ELSE @monListPrice
							END) 			
		FROM    
			Item I 
		LEFT JOIN PriceBookRules P ON I.numDomainID = P.numDomainID
		LEFT JOIN PriceBookRuleDTL PDTL ON P.numPricRuleID = PDTL.numRuleID
		LEFT JOIN PriceBookRuleItems PBI ON P.numPricRuleID = PBI.numRuleID
		LEFT JOIN [PriceBookPriorities] PP ON PP.[Step2Value] = P.[tintStep2] AND PP.[Step3Value] = P.[tintStep3]
		WHERE   
			numItemCode = @numItemCode AND 
			tintRuleFor=2 AND 
			(
				((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 1
				OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND (tintStep3= 1 AND PDTL.numValue = @numDivisionID)) -- Priority 2
				OR ((tintStep2 = 3) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 3

				OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 4
				OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 5
				OR ((tintStep2 = 3) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 6

				OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 3)) -- Priority 7
				OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND (tintStep3 = 3)) -- Priority 8
				OR ((tintStep2 = 3) and (tintStep3 = 3)) -- Priority 9
			)
		ORDER BY PP.Priority ASC
	END


	INSERT INTO @TEMP (numDivisionID, intMinQty, monPrice, intLeadTimeDays) VALUES (@numDivisionID, @numMinimumQty, @monListPrice, @intLeadTimeDays)

	RETURN
END

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='TF'AND NAME ='GetItemQuantityToOrder')
DROP FUNCTION GetItemQuantityToOrder
GO
CREATE FUNCTION [dbo].[GetItemQuantityToOrder] 
 (
      @numItemCode AS NUMERIC(9),
	  @numWarehouseItemID AS NUMERIC(9),
      @numDomainID AS NUMERIC(9),
	  @numAnalysisDays AS FLOAT,
	  @numForecastDays AS FLOAT,
	  @numTotalQtySold AS FLOAT,
	  @numVendorID AS NUMERIC(18,0)
    )
RETURNS @TEMP TABLE 
(
	numItemCode NUMERIC(9), 
	numOnHand NUMERIC(18,0),  
	numOnOrder NUMERIC(18,0), 
	numAllocation NUMERIC(18,0),
	numBackOrder NUMERIC(18,0), 
	numReOrder NUMERIC(18,0),
	numQtyToOrder INT, 
	dtOrderDate DATETIME, 
	vcOrderDate VARCHAR(50),
	numDivisionID NUMERIC(18,0), 
	monListPrice MONEY,
	intMinQty INT,
	intLeadTimeDays INT
)
AS BEGIN
	DECLARE @numQtySoldPerDay AS FLOAT = 0
	DECLARE @numRequiredForecastQuantity AS FLOAT = 0
	DECLARE @numQtyToOrder AS FLOAT = 0
	DECLARE @numOnHand AS FLOAT = 0
	DECLARE @numOnOrder AS FLOAT = 0
	DECLARE @numAllocation AS FLOAT = 0
	DECLARE @numBackOrder AS FLOAT = 0
	DECLARE @numReOrder AS FLOAT = 0
	DECLARE @dtOrderDate DATETIME
	DECLARE @numDivisionID NUMERIC(18,0) = 0
	DECLARE @monListPrice MONEY = 0
	DECLARE @numMinimumQty INT = 0
	DECLARE @intLeadTimeDays INT = 0

	-- Get Back Order Quantity of Item
	SELECT 
		@numOnHand = ISNULL(numOnHand,0),
		@numOnOrder = ISNULL(numOnOrder,0),
		@numAllocation = ISNULL(numAllocation,0),
		@numBackOrder = ISNULL(numBackOrder,0),
		@numReOrder = ISNULL(numReorder,0)
	FROM
		WareHouseItems
	WHERE
		numItemID = @numItemCode AND
		numWarehouseItemID = @numWarehouseItemID

	-- Select Item Vendor Detail
	SELECT
		@numDivisionID = numDivisionID,
		@monListPrice = monPrice,
		@numMinimumQty = intMinQty,
		@intLeadTimeDays = intLeadTimeDays
	FROM
		dbo.GetItemPurchaseVendorPrice(@numVendorID,@numItemCode,@numDomainID,@numQtyToOrder)

	

	--If total quantity sold for selected period is greater then 0 then process further
	IF @numTotalQtySold > 0
	BEGIN
		SET @numQtySoldPerDay = @numTotalQtySold/@numAnalysisDays

		--If average quantity sold per day is greater then 0 then process further
		IF @numQtySoldPerDay > 0
		BEGIN
			SET @numRequiredForecastQuantity = @numQtySoldPerDay * ISNULL(@numForecastDays,0)

			-- If item already have some back order quantity then we have to add it to required quantity to purchase
			SET @numRequiredForecastQuantity = @numRequiredForecastQuantity + ISNULL(@numBackOrder,0)

			SET @numQtyToOrder = ISNULL(CEILING(@numRequiredForecastQuantity),0)


			--If item has enought quantity for required days then no need to 
			-- then there is no need to make order
			IF (@numOnHand + @numOnOrder) > @numQtyToOrder
			BEGIN
				SET @numQtyToOrder = 0
			END
			ELSE
			BEGIN
				-- Else get required quantity ot order after removing items on hand
				SET @numQtyToOrder = @numQtyToOrder - (@numOnHand + @numOnOrder)
			END
		END
	END

	IF ISNULL(@numQtyToOrder,0) > 0
	BEGIN
		DECLARE @numDays AS INT = 0

		-- Find number of days after which order needs to be placed
		-- Lets on hand = 60, itemes sold per days = 10 
		-- then number of days  = (60/10) - @numLaedTimeDays
		SET @numDays = FLOOR((@numOnHand + @numOnOrder)/@numQtySoldPerDay) - @intLeadTimeDays

		IF @numDays < 0
			SET @numDays = 0

		SET @dtOrderDate = DATEADD(d,@numDays,GETDATE())

		IF DATENAME(dw,@dtOrderDate) = 'Saturday'
		BEGIN
			SET @dtOrderDate = DATEADD(d,-1,@dtOrderDate)

			IF CAST(@dtOrderDate AS DATE) < CAST(GETDATE() AS DATE)
				SET @dtOrderDate = GETDATE()
		END
		ELSE IF DATENAME(dw,@dtOrderDate) = 'Sunday'
		BEGIN
			SET @dtOrderDate = DATEADD(d,-2,@dtOrderDate)

			IF CAST(@dtOrderDate AS DATE) < CAST(GETDATE() AS DATE)
				SET @dtOrderDate = GETDATE()
		END
		
		-- If order date goes more than required forecast date select date with number of forecast date
		IF CAST(@dtOrderDate AS DATE) > CAST(DATEADD(d,@numForecastDays,GETDATE()) AS DATE) 
		BEGIN
			SET @dtOrderDate = DATEADD(d,@numForecastDays,GETDATE())
		END
	END

	

	INSERT INTO @TEMP 
		(
			numItemCode, 
			numOnHand, 
			numOnOrder, 
			numAllocation, 
			numBackOrder, 
			numReOrder, 
			numQtyToOrder, 
			dtOrderDate, 
			vcOrderDate, 
			numDivisionID, 
			monListPrice, 
			intMinQty, 
			intLeadTimeDays
		) 
	VALUES 
		(
			@numItemCode,
			@numOnHand,
			@numOnOrder,
			@numAllocation,
			@numBackOrder, 
			@numReOrder, 
			CEILING(@numQtyToOrder),
			@dtOrderDate,
			(
				CASE WHEN CONVERT(DATE,@dtOrderDate) = CONVERT(DATE,GETDATE()) THEN
				'<b><font color=red>Today</font></b>'
				WHEN CONVERT(DATE,@dtOrderDate) = CONVERT(DATE,DATEADD(d,1,GETDATE())) THEN
				'<b><font color=purple>YesterDay</font></b>'
				ELSE
					dbo.FormatedDateFromDate(@dtOrderDate,@numDomainID)
				END
			),
			@numDivisionID, 
			@monListPrice, 
			@numMinimumQty, 
			@intLeadTimeDays
		)

	RETURN
END
-- SELECT dbo.GetPriceBasedOnPriceBook(366,100,1)
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='getpricebasedonpricebook')
DROP FUNCTION getpricebasedonpricebook
GO
CREATE FUNCTION GetPriceBasedOnPriceBook
--Below function applies Price Rule on given Price an Qty and returns calculated value
    (
      @numRuleID NUMERIC,
      @monPrice MONEY,
      @units INT,
      @ItemCode numeric
    )
RETURNS MONEY
AS BEGIN
    DECLARE @PriceBookPrice MONEY
    DECLARE @tintRuleType TINYINT
    DECLARE @tintPricingMethod TINYINT
    DECLARE @tintDiscountType TINYINT
    DECLARE @decDiscount DECIMAL(9,2)
    DECLARE @intQntyItems INT
    DECLARE @decMaxDedPerAmt DECIMAL(9,2)
--    DECLARE @intOperator INT
    
    SET @PriceBookPrice = 0
    SELECT  @tintRuleType = PB.[tintRuleType],
			@tintPricingMethod = PB.[tintPricingMethod],
			@tintDiscountType = PB.[tintDiscountType],
			@decDiscount = PB.[decDiscount],
			@intQntyItems = PB.[intQntyItems],
			@decMaxDedPerAmt = PB.[decMaxDedPerAmt]
	FROM    [PriceBookRules] PB
			LEFT OUTER JOIN [PriceBookRuleDTL] PD ON PB.[numPricRuleID] = PD.[numRuleID]
	WHERE   [numPricRuleID] = @numRuleID
	
	
	
    
    IF ( @tintPricingMethod = 1 ) -- Price table
        BEGIN
				SET @intQntyItems = 1;
			   SELECT TOP 1  @tintRuleType = [tintRuleType],
						@tintDiscountType = [tintDiscountType],
						@decDiscount = [decDiscount]
			   FROM     [PricingTable]
			   WHERE    [numPriceRuleID] = @numRuleID
						AND @units BETWEEN [intFromQty] AND [intToQty]

IF (@@ROWCOUNT=0)						
	RETURN @monPrice;


IF @tintRuleType = 2
	BEGIN
		SELECT @monPrice = dbo.[fn_GetVendorCost](@ItemCode)
	END
	
				-- SELECT dbo.GetPriceBasedOnPriceBook(366,500,20)
				 IF ( @tintDiscountType = 1 ) -- Percentage 
                BEGIN
					
					SET @decDiscount = @monPrice * ( @decDiscount /100);

					IF ( @tintRuleType = 1 )
							SET @PriceBookPrice =  (@monPrice - @decDiscount)  ;
					IF ( @tintRuleType = 2 )
							SET @PriceBookPrice = @monPrice + @decDiscount   ;
					
                END
               -- SELECT dbo.GetPriceBasedOnPriceBook(366,500,21)
				IF ( @tintDiscountType = 2 ) -- Flat discount 
					BEGIN
							
						IF ( @tintRuleType = 1 )
								SET @PriceBookPrice =  (@monPrice -   @decDiscount) ;
						IF ( @tintRuleType = 2 )
								SET @PriceBookPrice =  (@monPrice +  @decDiscount)  ;
					END
           
        END
        
    IF ( @tintPricingMethod = 2 ) -- Pricing Formula 
        BEGIN

		IF @tintRuleType = 2
		BEGIN
			SELECT @monPrice = dbo.[fn_GetVendorCost](@ItemCode)
		END
		  
    -- SELECT dbo.GetPriceBasedOnPriceBook(368,500,10)

            IF ( @tintDiscountType = 1 ) -- Percentage 
                BEGIN
					SET @decDiscount = @decDiscount * (@units/@intQntyItems);
					SET @decDiscount = @decDiscount * @monPrice /100;
--					RETURN @decDiscount
--					RETURN @decMaxDedPerAmt
					SET @decMaxDedPerAmt = @decMaxDedPerAmt * @monPrice /100;
					
					
					IF (@decDiscount > @decMaxDedPerAmt)
						SET @decDiscount = @decMaxDedPerAmt;
					
					
					IF ( @tintRuleType = 1 )
							SET @PriceBookPrice = @monPrice - @decDiscount  ;
					IF ( @tintRuleType = 2 )
							SET @PriceBookPrice = @monPrice + @decDiscount   ;
					
                END
            IF ( @tintDiscountType = 2 ) -- Flat discount 
                BEGIN
					SET @decDiscount = @decDiscount * (@units/@intQntyItems);
--					RETURN @decDiscount
--					RETURN @decMaxDedPerAmt

					IF (@decDiscount > @decMaxDedPerAmt)
						SET @decDiscount = @decMaxDedPerAmt;
					
					IF ( @tintRuleType = 1 )
							SET @PriceBookPrice =  (@monPrice -   @decDiscount)  ;
					IF ( @tintRuleType = 2 )
							SET @PriceBookPrice = (@monPrice +  @decDiscount)  ;
                END
			
        END


    RETURN @PriceBookPrice ;

   END
-- SELECT dbo.GetPriceBasedOnPriceBook(366,100,1)
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='TF'AND NAME ='getpricebasedonpricebookTable')
DROP FUNCTION getpricebasedonpricebookTable
GO
CREATE FUNCTION getpricebasedonpricebookTable
--Below function applies Price Rule on given Price an Qty and returns calculated value
    (
      @numRuleID NUMERIC,
      @monPrice MONEY,
      @units INT,
      @ItemCode numeric
    )
RETURNS @PriceBookRules table
(
	PriceBookPrice money,
    tintDiscountTypeOriginal TINYINT,
    decDiscountOriginal DECIMAL(9,2),
	tintRuleType TINYINT
)                               
AS 
BEGIN
	DECLARE @ListPrice MONEY
    DECLARE @PriceBookPrice MONEY
    DECLARE @tintRuleType TINYINT
    DECLARE @tintPricingMethod TINYINT
    DECLARE @tintDiscountType TINYINT
    DECLARE @decDiscount DECIMAL(9,2)
	DECLARE @decDiscountOriginal DECIMAL(9,2)
    DECLARE @intQntyItems INT
    DECLARE @decMaxDedPerAmt DECIMAL(9,2)
--    DECLARE @intOperator INT


	SET @ListPrice =  @monPrice   
    SET @PriceBookPrice = 0
    SELECT  @tintRuleType = PB.[tintRuleType],
			@tintPricingMethod = PB.[tintPricingMethod],
			@tintDiscountType = PB.[tintDiscountType],
			@decDiscount = PB.[decDiscount],
			@decDiscountOriginal=PB.[decDiscount],
			@intQntyItems = PB.[intQntyItems],
			@decMaxDedPerAmt = PB.[decMaxDedPerAmt]
	FROM    [PriceBookRules] PB
			LEFT OUTER JOIN [PriceBookRuleDTL] PD ON PB.[numPricRuleID] = PD.[numRuleID]
	WHERE   [numPricRuleID] = @numRuleID
	
    IF ( @tintPricingMethod = 1 ) -- Price table
        BEGIN
				SET @intQntyItems = 1;
				
				SELECT TOP 1 @tintRuleType = [tintRuleType],
						@tintDiscountType = [tintDiscountType],
						@decDiscount = [decDiscount],
						@decDiscountOriginal=[decDiscount]
				FROM     [PricingTable]
				WHERE    [numPriceRuleID] = @numRuleID
						AND @units BETWEEN [intFromQty] AND [intToQty]

				IF @tintRuleType = 2
				BEGIN
					SELECT @monPrice = dbo.[fn_GetVendorCost](@ItemCode)

					IF (@@ROWCOUNT=0)	
					BEGIN					
						INSERT INTO @PriceBookRules(PriceBookPrice,tintDiscountTypeOriginal,decDiscountOriginal)
						   Values (@monPrice,0,0)
						RETURN
					END
				END
	
				
				-- SELECT dbo.GetPriceBasedOnPriceBook(366,500,20)
				 IF ( @tintDiscountType = 1 ) -- Percentage 
                BEGIN
					
					SET @decDiscount = @monPrice * ( @decDiscount /100);

					IF ( @tintRuleType = 1 )
							SET @PriceBookPrice =  (@monPrice - @decDiscount)  ;
					IF ( @tintRuleType = 2 )
							SET @PriceBookPrice = @monPrice + @decDiscount   ;
					
                END
               -- SELECT dbo.GetPriceBasedOnPriceBook(366,500,21)
				IF ( @tintDiscountType = 2 ) -- Flat discount 
					BEGIN
							
						IF ( @tintRuleType = 1 )
								SET @PriceBookPrice =  (@monPrice -   @decDiscount) ;
						IF ( @tintRuleType = 2 )
								SET @PriceBookPrice =  (@monPrice +  @decDiscount)  ;
					END
           
        END
        
    IF ( @tintPricingMethod = 2 ) -- Pricing Formula 
        BEGIN
			
			IF @tintRuleType = 2
			BEGIN
				SELECT @monPrice = dbo.[fn_GetVendorCost](@ItemCode)

				IF (@@ROWCOUNT=0)	
				BEGIN					
					INSERT INTO @PriceBookRules(PriceBookPrice,tintDiscountTypeOriginal,decDiscountOriginal)
						Values (@monPrice,0,0)
					RETURN
				END
			END
		  
    -- SELECT dbo.GetPriceBasedOnPriceBook(368,500,10)

            IF ( @tintDiscountType = 1 ) -- Percentage 
                BEGIN
					SET @decDiscount = @decDiscount * (@units/@intQntyItems);
					SET @decDiscount = @decDiscount * @monPrice /100;
--					RETURN @decDiscount
--					RETURN @decMaxDedPerAmt
					SET @decMaxDedPerAmt = @decMaxDedPerAmt * @monPrice /100;
					
					
					IF (@decDiscount > @decMaxDedPerAmt)
						SET @decDiscount = @decMaxDedPerAmt;
					
					
					IF ( @tintRuleType = 1 )
							SET @PriceBookPrice = @monPrice - @decDiscount  ;
					IF ( @tintRuleType = 2 )
							SET @PriceBookPrice = @monPrice + @decDiscount   ;
					
                END
            IF ( @tintDiscountType = 2 ) -- Flat discount 
                BEGIN
					SET @decDiscount = @decDiscount * (@units/@intQntyItems);
--					RETURN @decDiscount
--					RETURN @decMaxDedPerAmt

					IF (@decDiscount > @decMaxDedPerAmt)
						SET @decDiscount = @decMaxDedPerAmt;
					
					IF ( @tintRuleType = 1 )
							SET @PriceBookPrice =  (@monPrice -   @decDiscount)  ;
					IF ( @tintRuleType = 2 )
							SET @PriceBookPrice = (@monPrice +  @decDiscount)  ;
                END
			
        END

		If @tintRuleType = 2
		BEGIN
			IF @tintDiscountType = 1 -- Percentage
			BEGIN
				If @PriceBookPrice > @ListPrice
					SET @decDiscountOriginal = 0
				ELSE
					If @ListPrice > 0
						SET @decDiscountOriginal = ((@ListPrice - @PriceBookPrice) * 100) / CAST(@ListPrice AS FLOAT)
					ELSE
						SET @decDiscountOriginal = 0
			END
			ELSE IF @tintDiscountType = 2 -- Flat Amount
			BEGIN
				If @PriceBookPrice > @ListPrice
					SET @decDiscountOriginal = 0
				ELSE
					SET @decDiscountOriginal = @ListPrice - @PriceBookPrice
			END
		END


    INSERT INTO @PriceBookRules(PriceBookPrice,tintDiscountTypeOriginal,decDiscountOriginal,tintRuleType)
					Values (@PriceBookPrice,@tintDiscountType,@decDiscountOriginal,@tintRuleType)
	Return

   END
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetUnitPriceAfterPriceLevelApplication')
DROP FUNCTION GetUnitPriceAfterPriceLevelApplication
GO
CREATE FUNCTION [dbo].[GetUnitPriceAfterPriceLevelApplication](@numDomainID numeric(18,0), @numItemCode NUMERIC(18,0), @numQuantity INT, @numWarehouseItemID NUMERIC(18,0), @isPriceRule BIT, @numPriceRuleID NUMERIC(18,0))  
RETURNS FLOAT  
AS  
BEGIN  

DECLARE @finalUnitPrice FLOAT = 0
DECLARE @tintRuleType INT
DECLARE @tintDiscountType INT
DECLARE @decDiscount FLOAT
DECLARE @ItemPrice FLOAT

SET @tintRuleType = 0
SET @tintDiscountType = 0
SET @decDiscount  = 0
SET @ItemPrice = 0

IF @isPriceRule = 1
	SELECT TOP 1
		@tintRuleType = tintRuleType,
		@tintDiscountType = tintDiscountType,
		@decDiscount = decDiscount
	FROM 
		PricingTable 
	WHERE 
		numPriceRuleID = @numPriceRuleID AND
		(@numQuantity BETWEEN intFromQty AND intToQty)
ELSE
	SELECT TOP 1
		@tintRuleType = tintRuleType,
		@tintDiscountType = tintDiscountType,
		@decDiscount = decDiscount
	FROM 
		PricingTable 
	WHERE 
		numItemCode = @numItemCode AND
		(@numQuantity BETWEEN intFromQty AND intToQty)

IF @tintRuleType > 0 AND @tintDiscountType > 0
BEGIN
	IF @tintRuleType = 1 -- Deduct from List price
	BEGIN
		IF (SELECT ISNULL(charItemType,'') FROM Item WHERE numDomainID = @numDomainID AND numItemCode = @numItemCode) = 'P'
			If (SELECT COUNT(*) FROM Item WHERE numDomainID = @numDomainID AND numItemCode = @numItemCode AND (bitKitParent = 1 OR bitAssembly = 1)) > 0 -- Kit OR Assembly
				SELECT @ItemPrice = dbo.GetUnitPriceForKitOrAssembly(@numDomainID,@numItemCode,@numWarehouseItemID,@numQuantity)
			ELSE
				SELECT @ItemPrice = ISNULL(monWListPrice,0) FROM WareHouseItems WHERE numDomainID = @numDomainID AND numItemID = @numItemCode AND numWareHouseItemID = @numWarehouseItemID
		ELSE
			SELECT @ItemPrice = ISNULL(monListPrice,0) FROM Item WHERE numDomainID = @numDomainID AND numItemCode = @numItemCode

		IF @tintDiscountType = 1 -- Percentage
		BEGIN
			IF @decDiscount > 0
				SELECT @finalUnitPrice = @ItemPrice - (@ItemPrice * (@decDiscount/100))
		END
		ELSE IF @tintDiscountType = 2 -- Flat Amount
		BEGIN
				SELECT @finalUnitPrice = @ItemPrice - @decDiscount
		END
	END
	ELSE IF @tintRuleType = 2 -- Add to Primary Vendor Cost
	BEGIN
		If (SELECT COUNT(*) FROM Item WHERE numDomainID = @numDomainID AND numItemCode = @numItemCode AND (bitKitParent = 1 OR bitAssembly = 1)) > 0 -- Kit OR Assembly
			SELECT @ItemPrice = dbo.GetUnitPriceForKitOrAssembly(@numDomainID,@numItemCode,@numWarehouseItemID,@numQuantity)
		ELSE
			SELECT @ItemPrice = ISNULL(monCost,0) FROM Vendor WHERE numItemCode = @numItemCode AND numVendorID = (SELECT numVendorID FROM Item WHERE numItemCode = @numItemCode)

		IF @tintDiscountType = 1 -- Percentage
		BEGIN
			IF @decDiscount > 0
				SELECT @finalUnitPrice = @ItemPrice + (@ItemPrice * (@decDiscount/100))
		END
		ELSE IF @tintDiscountType = 2 -- Flat Amount
		BEGIN
			SELECT @finalUnitPrice = @ItemPrice + @decDiscount
		END
	END
	ELSE IF @tintRuleType = 3 -- Named Price
	BEGIN
		SELECT @finalUnitPrice = @decDiscount
	END
END

return @finalUnitPrice
END
GO


--------------- SPs

GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_AccountDiagnosis' ) 
    DROP PROCEDURE USP_AccountDiagnosis
GO
/****** Object:  StoredProcedure [dbo].[USP_AccountDiagnosis]    Script Date: SEP/03/2014 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  <Manish Anjara>  
-- Create date: <Wednesday, 3rd Sep, 2014>  
-- Description: <This procedure will be used for complete accounting diagnosis>  
-- =============================================  
-- EXEC USP_AccountDiagnosis 89
CREATE PROCEDURE [dbo].[USP_AccountDiagnosis]
(	
	@numDomainID NUMERIC(18,0)
)
AS
BEGIN

DECLARE @vcDiff AS VARCHAR(MAX)
SET @vcDiff = ''

--DECLARE @numDomainID NUMERIC(18, 0)
--SET @numDomainID = 143
SELECT 'General' AS [Title],'General' AS [TableName],'1. General details for all tables.' AS [vcDesc]
UNION ALL
SELECT 'GL Details' AS [Title],'GLDetails' AS [TableName],'1. General Details table should have at least 2 records (1 for Credit and 1 for Debit). Given table will get records from General Header table which records have not General Detail records.' AS [vcDesc]
UNION ALL
SELECT 'Missed Transaction Details' AS [Title],'TransactionDetails' AS [TableName],'2. Check details of every transactions whether they are in respective tables or not.'
UNION ALL
--SELECT 'Missed Transactions' AS [Title],'MissedTransactions' AS [TableName],'3. Execute Select Statement For [#MissedTransactionDetails]'
--UNION ALL
SELECT 'Not Matching Credit' AS [Title],'NotMatching_Credit' AS [TableName],'4. List entries if General header amount is not matching Total Credit value of General Details table'
UNION ALL
SELECT 'Not Matching Debit' AS [Title],'NotMatching_Debit' AS [TableName],'5. List entries if General header amount is not matching Total Debit value of General Details table'
UNION ALL
SELECT 'AR Aging Summary' AS [Title],'ARAgingSummary' AS [TableName],'6. AR Aging Summary'
UNION ALL
SELECT 'AR Aging Details' AS [Title],'ARAgingDetails' AS [TableName],'7. AR Aging Details'
UNION ALL
SELECT 'Invoice To GL Mapping' AS [Title],'InvoiceToGLMapping' AS [TableName],'8. Check whether any Invoiced transaction are in general entries or not (1)'
UNION ALL
SELECT 'GL To Invoice Mapping' AS [Title],'GLToInvoiceMapping' AS [TableName],'9. Check whether any general entries are in Invoiced transactions or not (2).'
UNION ALL
SELECT 'AP Aging Summary' AS [Title],'APAgingSummary' AS [TableName],'10. AP Aging Summary'
UNION ALL
SELECT 'AP Aging Detail' AS [Title],'APAgingDetail' AS [TableName],'11. AP Aging Details'
UNION ALL
SELECT 'Differences' AS [Title],'Differences' AS [TableName],'12. Differences in result'

-- BASICS TO STICK TO :
-- 1. General Details table should have at least 2 records (1 for Credit and 1 for Debit). 
--    Given query will get records from General Header table which records have not General Detail records

PRINT '1. General Details table should have at least 2 records (1 for Credit and 1 for Debit). '
PRINT 'Given query will get records from General Header table which records have not General Detail records'

SELECT * FROM [dbo].[General_Journal_Header] AS GJH 
WHERE [GJH].[numDomainId] = @numDomainID
AND [GJH].[numJournal_Id] NOT IN (SELECT [GJD].[numJournalId] FROM [dbo].[General_Journal_Details] AS GJD)
ORDER BY [GJH].[datEntry_Date] DESC

-- 2. Check details of every transactions whether they are in respective tables or not.
PRINT '2. Check details of every transactions whether they are in respective tables or not.'

CREATE TABLE #MissedTransactionDetails
(	TransactionType  VARCHAR(50), 
	numJournalId  NUMERIC(18,0),
	numOppId  NUMERIC(18,0),
	numOppBizDocsId  NUMERIC(18,0),
	numBillID  NUMERIC(18,0),
	vcComments VARCHAR(100),
	numBillPaymentID  NUMERIC(18,0),
	numBizDocsPaymentDetId  NUMERIC(18,0),
	numCheckHeaderID  NUMERIC(18,0),
	numDepositId  NUMERIC(18,0),
	numReturnID  NUMERIC(18,0)
)

INSERT INTO [#MissedTransactionDetails] 
--SELECT 'Opportunity' AS [TransactionType],  [GJH].[numJournal_Id],[GJH].[numOppId],[GJH].[numOppBizDocsId],[GJH].[numBillID],'GL records not in Order' AS [Comments], [GJH].[numBillPaymentID],[GJH].[numBizDocsPaymentDetId],[GJH].[numCheckHeaderID], [GJH].[numDepositId],[GJH].[numReturnID] FROM [dbo].[General_Journal_Header] AS GJH 
--WHERE [GJH].[numDomainId] = @numDomainID
--AND ISNULL([GJH].[numOppId],0) > 0
--AND [GJH].[numOppId] NOT IN 
--(
--SELECT OM.numOppID FROM [dbo].[OpportunityMaster] AS OM 
--LEFT JOIN [dbo].[OpportunityBizDocs] AS OBD ON [OM].[numOppId] = [OBD].[numOppId] AND [OBD].[bitAuthoritativeBizDocs] = 1
--WHERE OM.[numDomainId] = @numDomainID
--AND [OBD].[bitAuthoritativeBizDocs] = 1
--)
--UNION ALL
--SELECT 'Opportunity' AS [TransactionType], 0 AS [numJournal_Id],[OM].[numOppId],0 AS [numOppBizDocsId],0 AS [numBillID],'Orders not in GL' AS [Comments], 0 AS [numBillPaymentID],0 AS [numBizDocsPaymentDetId],0 AS [numCheckHeaderID], 0 AS [numDepositId],0 AS [numReturnID] FROM [dbo].[OpportunityMaster] AS OM 
--LEFT JOIN [dbo].[OpportunityBizDocs] AS OBD ON [OM].[numOppId] = [OBD].[numOppId]
--WHERE OM.[numDomainId] = @numDomainID
--AND [OBD].[bitAuthoritativeBizDocs] = 1
--AND [OM].[numOppId] NOT IN ( SELECT  [GJH].[numOppId] FROM [dbo].[General_Journal_Header] AS GJH WHERE [GJH].[numDomainId] = @numDomainID AND ISNULL([GJH].[numOppId],0) > 0 )
--UNION ALL
SELECT 'Bills' AS [TransactionType],  [GJH].[numJournal_Id],[GJH].[numOppId],[GJH].[numOppBizDocsId],[GJH].[numBillID],'GL records not in Bills' AS [Comments], [GJH].[numBillPaymentID],[GJH].[numBizDocsPaymentDetId],[GJH].[numCheckHeaderID], [GJH].[numDepositId],[GJH].[numReturnID] FROM [dbo].[General_Journal_Header] AS GJH 
WHERE [GJH].[numDomainId] = @numDomainID
AND ISNULL([GJH].[numBillID],0) > 0
AND [GJH].[numBillID] NOT IN 
(
	SELECT BH.[numBillID] FROM [dbo].[BillHeader] AS BH WHERE BH.[numDomainId] = @numDomainID
)
UNION ALL
SELECT 'Bills' AS [TransactionType], 0 AS [numJournal_Id],0 AS [numOppId],0 AS [numOppBizDocsId],0 AS [numBillID],'Bills not in GL' AS [Comments], 0 AS [numBillPaymentID],0 AS [numBizDocsPaymentDetId],0 AS [numCheckHeaderID], 0 AS [numDepositId],0 AS [numReturnID] 
FROM [dbo].[BillHeader] AS BH
WHERE BH.[numDomainId] = @numDomainID
AND BH.[numBillID] NOT IN ( SELECT  [GJH].[numBillID] FROM [dbo].[General_Journal_Header] AS GJH WHERE [GJH].[numDomainId] = @numDomainID AND ISNULL([GJH].[numBillID],0) > 0 )
UNION ALL
--SELECT 'Bill Payments' AS [TransactionType],  [GJH].[numJournal_Id],[GJH].[numOppId],[GJH].[numOppBizDocsId],[GJH].[numBillID],'GL records not in Bill Payments' AS [Comments], [GJH].[numBillPaymentID],[GJH].[numBizDocsPaymentDetId],[GJH].[numCheckHeaderID], [GJH].[numDepositId],[GJH].[numReturnID] FROM [dbo].[General_Journal_Header] AS GJH 
--WHERE [GJH].[numDomainId] = @numDomainID
--AND ISNULL([GJH].[numBillPaymentID],0) > 0
--AND [GJH].[numBillPaymentID] NOT IN 
--(
--	SELECT [numBillPaymentID] FROM [dbo].[BillPaymentHeader] AS BPH WHERE BPH.[numDomainId] = @numDomainID
--)
--UNION ALL
--SELECT 'Bill Payments' AS [TransactionType], 0 AS [numJournal_Id],0 AS [numOppId],0 AS [numOppBizDocsId],0 AS [numBillID],'Bill Payments not in GL' AS [Comments], BPH.[numBillPaymentID] AS [numBillPaymentID],0 AS [numBizDocsPaymentDetId],0 AS [numCheckHeaderID], 0 AS [numDepositId],0 AS [numReturnID] 
--FROM [dbo].[BillPaymentHeader] AS BPH 
--WHERE BPH.[numDomainId] = @numDomainID
--AND BPH.[numBillPaymentID] NOT IN ( SELECT  [GJH].[numBillPaymentID] FROM [dbo].[General_Journal_Header] AS GJH WHERE [GJH].[numDomainId] = @numDomainID AND ISNULL([GJH].[numBillPaymentID],0) > 0 )
--UNION ALL
--SELECT 'Checks' AS [TransactionType],  [GJH].[numJournal_Id],[GJH].[numOppId],[GJH].[numOppBizDocsId],[GJH].[numBillID],'GL records not in Checks' AS [Comments], [GJH].[numBillPaymentID],[GJH].[numBizDocsPaymentDetId],[GJH].[numCheckHeaderID], [GJH].[numDepositId],[GJH].[numReturnID] FROM [dbo].[General_Journal_Header] AS GJH 
--WHERE [GJH].[numDomainId] = @numDomainID
--AND ISNULL([GJH].[numCheckHeaderID],0) > 0
--AND [GJH].[numCheckHeaderID] NOT IN 
--(
--	SELECT [CH].[numCheckHeaderID] FROM [dbo].[CheckHeader] AS CH WHERE [CH].[numDomainID] = @numDomainID
--)
--UNION ALL
--SELECT 'Checks' AS [TransactionType], 0 AS [numJournal_Id],0 AS [numOppId],0 AS [numOppBizDocsId],0 AS [numBillID],'Checks not in GL' AS [Comments], 0 AS [numBillPaymentID],0 AS [numBizDocsPaymentDetId], CH.[numCheckHeaderID] AS [numCheckHeaderID], 0 AS [numDepositId],0 AS [numReturnID] 
--FROM [dbo].[CheckHeader] AS CH WHERE [CH].[numDomainID] = @numDomainID
--AND CH.[numCheckHeaderID] NOT IN ( SELECT  [GJH].[numCheckHeaderID] FROM [dbo].[General_Journal_Header] AS GJH WHERE [GJH].[numDomainId] = @numDomainID AND ISNULL([GJH].[numCheckHeaderID],0) > 0 )
--UNION ALL
SELECT 'Deposits' AS [TransactionType],  [GJH].[numJournal_Id],[GJH].[numOppId],[GJH].[numOppBizDocsId],[GJH].[numBillID],'GL records not in Deposits' AS [Comments], [GJH].[numBillPaymentID],[GJH].[numBizDocsPaymentDetId],[GJH].[numCheckHeaderID], [GJH].[numDepositId],[GJH].[numReturnID] 
FROM [dbo].[General_Journal_Header] AS GJH 
JOIN [dbo].[DepositMaster] AS DM2 ON DM2.[numDepositId] = [GJH].[numDepositId] AND [DM2].[tintDepositePage] IN (2,3) AND [GJH].[numDomainId] = [DM2].[numDomainId]
WHERE [GJH].[numDomainId] = @numDomainID
AND ISNULL([GJH].[numDepositId],0) > 0
AND [DM2].[tintDepositePage] IN (2,3)
AND [GJH].[numDepositId] NOT IN 
(
	SELECT [numDepositId] FROM [dbo].[DepositMaster] AS DM WHERE DM.[numDomainID] = @numDomainID AND [DM].[tintDepositePage] IN (2,3)
)
UNION ALL
SELECT 'Deposits' AS [TransactionType], 0 AS [numJournal_Id],0 AS [numOppId],0 AS [numOppBizDocsId],0 AS [numBillID],'Deposits not in GL' AS [Comments], 0 AS [numBillPaymentID],0 AS [numBizDocsPaymentDetId], 0 AS [numCheckHeaderID], DM.numDepositID AS [numDepositId],0 AS [numReturnID] 
FROM [dbo].[DepositMaster] AS DM WHERE DM.[numDomainID] = @numDomainID AND [DM].[tintDepositePage] IN (2,3)
AND [DM].[numDepositId] NOT IN ( SELECT  [GJH].[numDepositId] FROM [dbo].[General_Journal_Header] AS GJH WHERE [GJH].[numDomainId] = @numDomainID AND ISNULL([GJH].[numDepositId],0) > 0 )
AND [DM].[tintDepositePage] IN (2,3)
UNION ALL
SELECT 'Returns' AS [TransactionType],  [GJH].[numJournal_Id],[GJH].[numOppId],[GJH].[numOppBizDocsId],[GJH].[numBillID],'GL records not in Returns' AS [Comments], [GJH].[numBillPaymentID],[GJH].[numBizDocsPaymentDetId],[GJH].[numCheckHeaderID], [GJH].[numDepositId],[GJH].[numReturnID] FROM [dbo].[General_Journal_Header] AS GJH 
WHERE [GJH].[numDomainId] = @numDomainID
AND ISNULL([GJH].[numReturnID],0) > 0
AND [GJH].[numReturnID] NOT IN 
(
	SELECT [RH].[numReturnHeaderID] FROM [dbo].[ReturnHeader] AS RH WHERE [RH].[numDomainID] = @numDomainID
)
UNION ALL
SELECT 'Returns' AS [TransactionType], 0 AS [numJournal_Id],0 AS [numOppId],0 AS [numOppBizDocsId],0 AS [numBillID],'Returns not in GL' AS [Comments], 0 AS [numBillPaymentID],0 AS [numBizDocsPaymentDetId], 0 AS [numCheckHeaderID], 0 AS [numDepositId], RH.[numReturnHeaderID] AS [numReturnID] 
FROM [dbo].[ReturnHeader] AS RH WHERE RH.[numDomainID] = @numDomainID
AND [RH].[numReturnHeaderID] NOT IN ( SELECT  [GJH].[numReturnID] FROM [dbo].[General_Journal_Header] AS GJH WHERE [GJH].[numDomainId] = @numDomainID AND ISNULL([GJH].[numReturnID],0) > 0 )

-- Execute Select Statement For [#MissedTransactionDetails]
SELECT * FROM [#MissedTransactionDetails]
DROP TABLE #MissedTransactionDetails

--- List entries if General header amount is not matching Total Credit value of General Details table
PRINT '--- List entries if General header amount is not matching Total Credit value of General Details table--'
SELECT  [GJH].[numJournal_Id],[GJH].[numAmount], SUM([GJD].[numCreditAmt]) [TotalCredit], [GJH].[numAmount] - SUM([GJD].[numCreditAmt]) AS [Diff]
FROM    [dbo].[General_Journal_Header] AS GJH
LEFT JOIN [dbo].[General_Journal_Details] AS GJD ON [GJH].[numJournal_Id] = [GJD].[numJournalId] AND [GJH].[numDomainId] = [GJD].[numDomainId]
WHERE [GJH].[numDomainId] = @numDomainID
GROUP BY [GJH].[numJournal_Id],[GJH].[numDomainId],[GJH].[numAmount]
HAVING ([GJH].[numAmount] - SUM([GJD].[numCreditAmt])) <> 0
ORDER BY ([GJH].[numAmount] - SUM([GJD].[numCreditAmt])) DESC

--- List entries if General header amount is not matching Total Debit value of General Details table
PRINT 'List entries if General header amount is not matching Total Debit value of General Details table'
SELECT  [GJH].[numJournal_Id],[GJH].[numAmount], SUM([GJD].[numDebitAmt]) [TotalDebit], [GJH].[numAmount] - SUM([GJD].[numDebitAmt]) AS [Diff]
FROM    [dbo].[General_Journal_Header] AS GJH
LEFT JOIN [dbo].[General_Journal_Details] AS GJD ON [GJH].[numJournal_Id] = [GJD].[numJournalId] AND [GJH].[numDomainId] = [GJD].[numDomainId]
WHERE [GJH].[numDomainId] = @numDomainID
GROUP BY [GJH].[numJournal_Id],[GJH].[numDomainId],[GJH].[numAmount]
HAVING ([GJH].[numAmount] - SUM([GJD].[numDebitAmt])) <> 0
ORDER BY ([GJH].[numAmount] - SUM([GJD].[numDebitAmt])) DESC


---- Accounts Receivable Diagnosis
--==========================================================================================
PRINT '---- Accounts Receivable Diagnosis'
DECLARE @ARJournalTotal AS MONEY
DECLARE @ARTransactionTotal AS MONEY

-- JOURNAL ENTRIES TOTAL
PRINT '-- JOURNAL ENTRIES TOTAL'
SELECT  @ARJournalTotal = SUM(ISNULL([GJD].[numDebitAmt],0)) - SUM(ISNULL([GJD].[numCreditAmt],0))
FROM    [dbo].[General_Journal_Header] AS GJH
        JOIN [dbo].[General_Journal_Details] AS GJD ON [GJH].[numJournal_Id] = [GJD].[numJournalId]
                                                       AND gjh.[numDomainId] = [GJD].[numDomainId]
WHERE   1 = 1
        AND [GJH].[numDomainId] = @numDomainID
        AND [GJD].[numChartAcntId] IN (
        SELECT  numaccountID
        FROM    [dbo].[Chart_Of_Accounts] AS COA
        WHERE   COA.vcAccountCode LIKE '01010105%'
                AND [COA].[numDomainId] = [GJH].[numDomainId] )

IF ISNULL(@ARJournalTotal,0) = 0
SET @ARJournalTotal = 0
PRINT '@ARJournalTotal : ' + CONVERT(VARCHAR(18), @ARJournalTotal)

-- TRANSACTIONS TOTAL
PRINT '-- TRANSACTIONS TOTAL'
DECLARE @AuthoritativeSalesBizDocId AS INTEGER
SELECT  @AuthoritativeSalesBizDocId = ISNULL(numAuthoritativeSales, 0)
FROM    AuthoritativeBizDocs
WHERE   numDomainId = @numDomainID;

IF ISNULL(@AuthoritativeSalesBizDocId,0) = 0
SET @AuthoritativeSalesBizDocId = 0
PRINT '@AuthoritativeSalesBizDocId : ' + CONVERT(VARCHAR(18), @AuthoritativeSalesBizDocId)

SELECT  @ARTransactionTotal = SUM(TABLE1.[DealAmount]) FROM 
(
SELECT  ISNULL(OB.monDealAmount * ISNULL(Opp.fltExchangeRate, 1), 0) - ISNULL(OB.monAmountPaid * ISNULL(Opp.fltExchangeRate, 1), 0) DealAmount
FROM    OpportunityMaster Opp
        JOIN OpportunityBizDocs OB ON OB.numOppId = Opp.numOppID
        LEFT OUTER JOIN [Currency] C ON Opp.[numCurrencyID] = C.[numCurrencyID]
WHERE   tintOpptype = 1
        AND tintoppStatus = 1
        AND opp.numDomainID = @numDomainID
        AND numBizDocId = @AuthoritativeSalesBizDocId
        AND OB.bitAuthoritativeBizDocs = 1
        AND ISNULL(OB.tintDeferred, 0) <> 1
		--AND Opp.[numDivisionId] = 217158

UNION ALL --Show Impact of AR journal entries in report as well 

SELECT  CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt
             ELSE -1 * GJD.numCreditAmt
        END
FROM    dbo.General_Journal_Header GJH
        INNER JOIN dbo.General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId
                                                      AND GJH.numDomainId = GJD.numDomainId
        INNER JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = GJD.numChartAcntId
WHERE   GJH.numDomainId = @numDomainID
        AND COA.vcAccountCode LIKE '01010105%'
        AND ISNULL(numOppId, 0) = 0
        AND ISNULL(numOppBizDocsId, 0) = 0
        AND ISNULL(GJH.numDepositID, 0) = 0
        AND ISNULL(GJH.numReturnID, 0) = 0

UNION ALL -- Deposits

SELECT  ( ISNULL(monDepositAmount, 0) - ISNULL(monAppliedAmount, 0) /*- ISNULL(monRefundAmount,0)*/ ) * -1
FROM    DepositMaster DM
WHERE   DM.numDomainId = @numDomainID
        AND tintDepositePage IN ( 2, 3 )
        AND ( ( ISNULL(monDepositAmount, 0) - ISNULL(monAppliedAmount, 0) ) > 0 )

UNION ALL --Standalone Refund against Account Receivable

SELECT  CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt ELSE -1 * GJD.numCreditAmt END
FROM    dbo.ReturnHeader RH
        JOIN dbo.General_Journal_Header GJH ON RH.numReturnHeaderID = GJH.numReturnID
        JOIN General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId
        INNER JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = GJD.numChartAcntId
WHERE   RH.tintReturnType = 4
        AND RH.numDomainId = @numDomainID
        AND COA.vcAccountCode LIKE '0101010501'
        AND ( monBizDocAmount > 0 )
        AND ISNULL(RH.numBillPaymentIDRef, 0) = 0 
) TABLE1

IF ISNULL(@ARTransactionTotal,0) = 0
SET @ARTransactionTotal = 0

PRINT '@ARTransactionTotal : ' + CONVERT(VARCHAR(18), @ARTransactionTotal)

-- AR Aging Summary
PRINT '-- AR Aging Summary'
SELECT @ARJournalTotal AS [ARJournalTotal], 
	   @ARTransactionTotal AS [ARTransactionTotal],
	   @ARJournalTotal - @ARTransactionTotal AS [Difference],
	   (CASE WHEN (@ARJournalTotal - @ARTransactionTotal) = 0 THEN 'Pass' ELSE 'Failed' END) AS [Result]

IF (@ARJournalTotal - @ARTransactionTotal) <> 0
BEGIN
SET @vcDiff = @vcDiff + '@ARJournalTotal - @ARTransactionTotal : ' + CONVERT(VARCHAR(100),(@ARJournalTotal - @ARTransactionTotal))
END


-- AR Aging Details
PRINT '-- AR Aging Details'
DECLARE @InvoiceTotal AS MONEY
DECLARE @OtherAREntriesTotal AS MONEY
DECLARE @InvoicedDepositTotal AS MONEY
DECLARE @DirectDepositTotal AS MONEY
DECLARE @RefundReturnTotal AS MONEY

SELECT @InvoiceTotal = 
	   (
		SELECT  SUM(ISNULL(OB.monDealAmount * ISNULL(Opp.fltExchangeRate, 1), 0) - ISNULL(OB.monAmountPaid * ISNULL(Opp.fltExchangeRate, 1), 0))
		FROM    OpportunityMaster Opp
				JOIN OpportunityBizDocs OB ON OB.numOppId = Opp.numOppID
				LEFT OUTER JOIN [Currency] C ON Opp.[numCurrencyID] = C.[numCurrencyID]
		WHERE   tintOpptype = 1
				AND tintoppStatus = 1
				AND opp.numDomainID = @numDomainID
				AND numBizDocId = @AuthoritativeSalesBizDocId
				AND OB.bitAuthoritativeBizDocs = 1
				AND ISNULL(OB.tintDeferred, 0) <> 1
	   ),
	   @InvoicedDepositTotal = 
	   (
		SELECT  SUM((ISNULL(monDepositAmount, 0) - ISNULL(monAppliedAmount, 0) /*- ISNULL(monRefundAmount,0)*/ )* -1)
		FROM    DepositMaster DM
		JOIN [dbo].[DepositeDetails] AS DD ON [DM].[numDepositId] = [DD].[numDepositID] 
		JOIN [dbo].[OpportunityMaster] AS OM ON OM.[numOppId] = DD.[numOppID] AND OM.[numDomainId] = DM.[numDomainId] 
		JOIN [dbo].[OpportunityBizDocs] AS OBD ON [OBD].[numOppId] = [OM].[numOppId] AND [OBD].[numOppBizDocsId] = DD.[numOppBizDocsID]
		WHERE   DM.numDomainId = @numDomainID
				AND tintDepositePage IN ( 2, 3 )
				AND ( ( ISNULL(monDepositAmount, 0) - ISNULL(monAppliedAmount, 0) ) > 0 )
	   ),
	   @DirectDepositTotal = 
	   (
		SELECT  SUM((ISNULL(monDepositAmount, 0) - ISNULL(monAppliedAmount, 0))* -1)
		FROM    DepositMaster DM
		JOIN [dbo].[DepositeDetails] DD ON [DM].[numDepositId] = [DD].[numDepositID]
		WHERE   DM.numDomainId = @numDomainID
				AND tintDepositePage IN (1)
				AND ISNULL(DD.[numOppID],0) = 0 AND ISNULL(DD.[numOppBizDocsID],0) = 0
				AND ( ( ISNULL(monDepositAmount, 0) - ISNULL(monAppliedAmount, 0) ) > 0 )		
	   ),
	   @OtherAREntriesTotal = 
	   (
		SELECT  SUM(CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt ELSE -1 * GJD.numCreditAmt END)
		FROM    dbo.General_Journal_Header GJH
				INNER JOIN dbo.General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId
															  AND GJH.numDomainId = GJD.numDomainId
				INNER JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = GJD.numChartAcntId
		WHERE   GJH.numDomainId = @numDomainID
				AND COA.vcAccountCode LIKE '01010105%'
				AND ISNULL(numOppId, 0) = 0
				AND ISNULL(numOppBizDocsId, 0) = 0
				AND ISNULL(GJH.numDepositID, 0) = 0
				AND ISNULL(GJH.numReturnID, 0) = 0
	   ),
	   @RefundReturnTotal =
	   (
		SELECT  SUM(CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt ELSE -1 * GJD.numCreditAmt END)
		FROM    dbo.ReturnHeader RH
				JOIN dbo.General_Journal_Header GJH ON RH.numReturnHeaderID = GJH.numReturnID
				JOIN General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId
				INNER JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = GJD.numChartAcntId
		WHERE   RH.tintReturnType = 4
				AND RH.numDomainId = @numDomainID
				AND COA.vcAccountCode LIKE '0101010501'
				AND ( monBizDocAmount > 0 )
				AND ISNULL(RH.numBillPaymentIDRef, 0) = 0 
		)

IF ISNULL(@InvoiceTotal,0) = 0
SET @InvoiceTotal = 0
IF ISNULL(@InvoicedDepositTotal,0) = 0
SET @InvoicedDepositTotal = 0

IF ISNULL(@DirectDepositTotal,0) = 0
SET @DirectDepositTotal = 0

IF ISNULL(@OtherAREntriesTotal,0) = 0
SET @OtherAREntriesTotal = 0

IF ISNULL(@RefundReturnTotal,0) = 0
SET @RefundReturnTotal = 0

SELECT * INTO #ARAginTable FROM
(
	SELECT 'AR : Invoice' AS [TransactionType],@InvoiceTotal AS Total, 0 AS AsPerTransaction, 0 AS AsPerGL, 0 AS [Diff], 1 AS [Order] 
	UNION 
	SELECT 'AR : Manual Journal Entries' AS [TransactionType],@OtherAREntriesTotal AS Total, 0 AS AsPerTransaction, 0 AS AsPerGL, 0 AS [Diff], 5 AS [Order]
	UNION 
	SELECT 'AR : Invoiced Deposit' AS [TransactionType],@InvoicedDepositTotal AS Total, 0 AS AsPerTransaction, 0 AS AsPerGL , 0 AS [Diff], 2 AS [Order]
	UNION 
	SELECT 'AR : Make Deposit' AS [TransactionType],@DirectDepositTotal AS Total, 0 AS AsPerTransaction, 0 AS AsPerGL , 0 AS [Diff], 3 AS [Order]
	UNION 
	SELECT 'AR : Refunds' AS [TransactionType],@RefundReturnTotal AS Total, 0 AS AsPerTransaction, 0 AS AsPerGL, 0 AS [Diff], 4 AS [Order]
) AS ARAginTable

DECLARE @InvoiceTotalAsPerTransaction AS MONEY
DECLARE @InvoiceTotalAsPerGL AS MONEY

DECLARE @OtherAREntriesTotalAsPerTransaction AS MONEY
DECLARE @OtherAREntriesTotalAsPerGL AS MONEY

DECLARE @InvoicedDepositTotalAsPerTransaction AS MONEY
DECLARE @InvoicedDepositTotalAsPerGL AS MONEY

DECLARE @RefundReturnTotalAsPerTransaction AS MONEY
DECLARE @RefundReturnTotalAsPerGL AS MONEY

DECLARE @DirectDepositTotalAsPerTransaction AS MONEY
DECLARE @DirectDepositTotalAsPerGL AS MONEY

SELECT @InvoicedDepositTotalAsPerTransaction = SUM(ISNULL(monDepositAmount, 0) - ISNULL(monAppliedAmount, 0) * (-1)) FROM [dbo].[DepositMaster] AS DM
JOIN [dbo].[DepositeDetails] AS DD ON [DM].[numDepositId] = [DD].[numDepositID] AND tintDepositePage IN ( 2, 3 )
JOIN [dbo].[OpportunityMaster] AS OM ON OM.[numOppId] = DD.[numOppID] AND DM.[numDomainId] = OM.[numDomainId]
JOIN [dbo].[OpportunityBizDocs] AS OBD ON [OM].[numOppId] = [OBD].[numOppId] AND DD.[numOppBizDocsID] = [OBD].[numOppBizDocsId]
LEFT JOIN [dbo].[General_Journal_Header] AS GJH ON DM.[numDepositId] = [GJH].[numDepositId] AND DM.[numDomainId] = [GJH].[numDomainId]
WHERE [DM].[numDomainId] = @numDomainID
AND tintOpptype = 1
AND tintoppStatus = 1
AND numBizDocId = @AuthoritativeSalesBizDocId
AND OBD.bitAuthoritativeBizDocs = 1
AND ISNULL(OBD.tintDeferred, 0) <> 1
AND tintDepositePage IN ( 2, 3 )
AND ISNULL([DM].[numReturnHeaderID],0) = 0
AND ( ( ISNULL(monDepositAmount, 0) - ISNULL(monAppliedAmount, 0) ) > 0 )

SELECT @InvoicedDepositTotalAsPerGL = SUM([GJD].[numCreditAmt]) - SUM(ISNULL(monAppliedAmount, 0) * (-1)) FROM [dbo].[DepositMaster] AS DM
JOIN [dbo].[DepositeDetails] AS DD ON [DM].[numDepositId] = [DD].[numDepositID] AND tintDepositePage IN ( 2, 3 ) 
JOIN [dbo].[General_Journal_Header] AS GJH ON DM.[numDepositId] = [GJH].[numDepositId] AND DM.[numDomainId] = [GJH].[numDomainId]
JOIN [dbo].[General_Journal_Details] AS GJD ON [GJH].[numJournal_Id] = [GJD].[numJournalId]
INNER JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = GJD.numChartAcntId
WHERE [DM].[numDomainId] = @numDomainID
AND tintDepositePage IN ( 2, 3 )
AND COA.vcAccountCode LIKE '01010105%'
AND ( ( ISNULL(monDepositAmount, 0) - ISNULL(monAppliedAmount, 0) ) > 0 )

SELECT @DirectDepositTotalAsPerTransaction = SUM(ISNULL(monDepositAmount, 0) - ISNULL(monAppliedAmount, 0) * (-1)) FROM [dbo].[DepositMaster] AS DM
JOIN [dbo].[DepositeDetails] AS DD ON [DM].[numDepositId] = [DD].[numDepositID]  AND tintDepositePage IN (1)
WHERE [DM].[numDomainId] = @numDomainID
AND tintDepositePage IN (1)
AND ( ( ISNULL(monDepositAmount, 0) - ISNULL(monAppliedAmount, 0) ) > 0 )

SELECT @DirectDepositTotalAsPerGL =  SUM(ISNULL(monDepositAmount, 0) - ISNULL(monAppliedAmount, 0) * (-1))  FROM [dbo].[DepositMaster] AS DM
JOIN [dbo].[DepositeDetails] AS DD ON [DM].[numDepositId] = [DD].[numDepositID]  AND tintDepositePage IN (1)
JOIN [dbo].[General_Journal_Header] AS GJH ON DM.[numDepositId] = [GJH].[numDepositId] AND DM.[numDomainId] = [GJH].[numDomainId]
WHERE [DM].[numDomainId] = @numDomainID
AND tintDepositePage IN (1)
AND ((ISNULL(monDepositAmount,0) - ISNULL(monAppliedAmount,0)) > 0)

SELECT @InvoiceTotalAsPerTransaction = SUM(ISNULL(OB.monDealAmount * ISNULL(Opp.fltExchangeRate, 1), 0) - ISNULL(OB.monAmountPaid * ISNULL(Opp.fltExchangeRate, 1), 0))
FROM    OpportunityMaster Opp
		JOIN OpportunityBizDocs OB ON OB.numOppId = Opp.numOppID
		LEFT OUTER JOIN [Currency] C ON Opp.[numCurrencyID] = C.[numCurrencyID]
		JOIN [dbo].[General_Journal_Header] AS GJH ON [Opp].[numOppId] = [GJH].[numOppId] AND OB.[numOppBizDocsId] = [GJH].[numOppBizDocsId] AND [GJH].[numDomainId] = Opp.[numDomainId] 
WHERE   tintOpptype = 1
		AND tintoppStatus = 1
		AND opp.numDomainID = @numDomainID
		AND numBizDocId = @AuthoritativeSalesBizDocId
		AND OB.bitAuthoritativeBizDocs = 1
		AND ISNULL(OB.tintDeferred, 0) <> 1
		AND ISNULL([GJH].[numReturnID],0) = 0

SELECT @InvoiceTotalAsPerGL = SUM(ISNULL(OB.monDealAmount * ISNULL(Opp.fltExchangeRate, 1), 0)) - SUM(ISNULL(OB.monAmountPaid * ISNULL(Opp.fltExchangeRate, 1), 0))
FROM    OpportunityMaster Opp
		JOIN OpportunityBizDocs OB ON OB.numOppId = Opp.numOppID
		LEFT OUTER JOIN [Currency] C ON Opp.[numCurrencyID] = C.[numCurrencyID]
		JOIN [dbo].[General_Journal_Header] AS GJH ON [Opp].[numOppId] = [GJH].[numOppId] AND [OB].[numOppBizDocsId] = [GJH].[numOppBizDocsId] AND Opp.[numDomainId] = [GJH].[numDomainId]
		JOIN [dbo].[General_Journal_Details] AS GJD ON [GJH].[numJournal_Id] = [GJD].[numJournalId]
		INNER JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = GJD.numChartAcntId
WHERE   tintOpptype = 1
		AND tintoppStatus = 1
		AND opp.numDomainID = @numDomainID
		AND numBizDocId = @AuthoritativeSalesBizDocId
		AND OB.bitAuthoritativeBizDocs = 1
		AND ISNULL(OB.tintDeferred, 0) <> 1
		AND COA.vcAccountCode LIKE '0101010501'
		--AND ISNULL([GJH].[numReturnID],0) = 0

SELECT @OtherAREntriesTotalAsPerTransaction = 0

SELECT @OtherAREntriesTotalAsPerGL = SUM(CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt ELSE -1 * GJD.numCreditAmt END)
FROM    dbo.General_Journal_Header GJH
		INNER JOIN dbo.General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId
														AND GJH.numDomainId = GJD.numDomainId
		INNER JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = GJD.numChartAcntId
WHERE   GJH.numDomainId = @numDomainID
		AND COA.vcAccountCode LIKE '01010105%'
		AND ISNULL(numOppId, 0) = 0
		AND ISNULL(numOppBizDocsId, 0) = 0
		AND ISNULL(GJH.numDepositID, 0) = 0
		AND ISNULL(GJH.numReturnID, 0) = 0

SELECT  @RefundReturnTotalAsPerTransaction = SUM(RH.[monBizDocAmount])
FROM    dbo.ReturnHeader RH
WHERE   RH.tintReturnType = 4
		AND RH.numDomainId = @numDomainID
		AND ( monBizDocAmount > 0 )
	    AND ISNULL(RH.numBillPaymentIDRef, 0) = 0

SELECT  @RefundReturnTotalAsPerGL = SUM(CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt ELSE -1 * GJD.numCreditAmt END)
FROM    dbo.ReturnHeader RH
		JOIN dbo.General_Journal_Header GJH ON RH.numReturnHeaderID = GJH.numReturnID
		JOIN General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId
		INNER JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = GJD.numChartAcntId
WHERE   RH.tintReturnType = 4
		AND RH.numDomainId = @numDomainID
		AND COA.vcAccountCode LIKE '0101010501'
		AND ( monBizDocAmount > 0 )
		AND ISNULL(RH.numBillPaymentIDRef, 0) = 0

IF ISNULL(@InvoicedDepositTotalAsPerTransaction,0) = 0
SET @InvoicedDepositTotalAsPerTransaction = 0

IF ISNULL(@InvoicedDepositTotalAsPerGL,0) = 0
SET @InvoicedDepositTotalAsPerGL = 0

IF ISNULL(@DirectDepositTotalAsPerTransaction,0) = 0
SET @DirectDepositTotalAsPerTransaction = 0

IF ISNULL(@DirectDepositTotalAsPerGL,0) = 0
SET @DirectDepositTotalAsPerGL = 0

IF ISNULL(@InvoiceTotalAsPerTransaction,0) = 0
SET @InvoiceTotalAsPerTransaction = 0

IF ISNULL(@InvoiceTotalAsPerGL,0) = 0
SET @InvoiceTotalAsPerGL = 0

IF ISNULL(@OtherAREntriesTotalAsPerTransaction,0) = 0
SET @OtherAREntriesTotalAsPerTransaction = 0

IF ISNULL(@OtherAREntriesTotalAsPerGL,0) = 0
SET @OtherAREntriesTotalAsPerGL = 0

IF ISNULL(@RefundReturnTotalAsPerTransaction,0) = 0
SET @RefundReturnTotalAsPerTransaction = 0

IF ISNULL(@RefundReturnTotalAsPerGL,0) = 0
SET @RefundReturnTotalAsPerGL = 0

PRINT '@InvoicedDepositTotalAsPerTransaction : ' + CONVERT(VARCHAR(18), @InvoicedDepositTotalAsPerTransaction)
PRINT '@InvoicedDepositTotalAsPerGL : ' + CONVERT(VARCHAR(18), @InvoicedDepositTotalAsPerGL)
PRINT '@DirectDepositTotalAsPerTransaction : ' + CONVERT(VARCHAR(18), @DirectDepositTotalAsPerTransaction)
PRINT '@DirectDepositTotalAsPerGL : ' + CONVERT(VARCHAR(18), @DirectDepositTotalAsPerGL)
PRINT '@InvoiceTotalAsPerTransaction : ' + CONVERT(VARCHAR(18), @InvoiceTotalAsPerTransaction)
PRINT '@InvoiceTotalAsPerGL : ' + CONVERT(VARCHAR(18), @InvoiceTotalAsPerGL)
PRINT '@OtherAREntriesTotalAsPerTransaction : ' + CONVERT(VARCHAR(18), @OtherAREntriesTotalAsPerTransaction)
PRINT '@OtherAREntriesTotalAsPerGL : ' + CONVERT(VARCHAR(18), @OtherAREntriesTotalAsPerGL)
PRINT '@RefundReturnTotalAsPerTransaction : ' + CONVERT(VARCHAR(18), @RefundReturnTotalAsPerTransaction)
PRINT '@RefundReturnTotalAsPerGL : ' + CONVERT(VARCHAR(18), @RefundReturnTotalAsPerGL)

UPDATE [#ARAginTable] SET [AsPerTransaction] = @InvoicedDepositTotalAsPerTransaction,[AsPerGL] = @InvoicedDepositTotalAsPerGL, [Diff] = @InvoicedDepositTotalAsPerTransaction - @InvoicedDepositTotalAsPerGL WHERE [#ARAginTable].[TransactionType] = 'AR : Invoiced Deposit'
UPDATE [#ARAginTable] SET [AsPerTransaction] = @DirectDepositTotalAsPerTransaction,[AsPerGL] = @DirectDepositTotalAsPerGL, [Diff] = @DirectDepositTotalAsPerTransaction - @DirectDepositTotalAsPerGL WHERE [#ARAginTable].[TransactionType] = 'AR : Make Deposit'
UPDATE [#ARAginTable] SET [AsPerTransaction] = @InvoiceTotalAsPerTransaction ,[AsPerGL] = @InvoiceTotalAsPerGL, [Diff] = @InvoiceTotalAsPerTransaction - @InvoiceTotalAsPerGL  WHERE [#ARAginTable].[TransactionType] = 'AR : Invoice'
UPDATE [#ARAginTable] SET [AsPerTransaction] = @OtherAREntriesTotalAsPerTransaction ,[AsPerGL] = @OtherAREntriesTotalAsPerGL, [Diff] = @OtherAREntriesTotalAsPerTransaction - @OtherAREntriesTotalAsPerGL  WHERE [#ARAginTable].[TransactionType] = 'AR : Manual Journal Entries'
UPDATE [#ARAginTable] SET [AsPerTransaction] = @RefundReturnTotalAsPerTransaction ,[AsPerGL] = @RefundReturnTotalAsPerGL, [Diff] = @RefundReturnTotalAsPerTransaction - @RefundReturnTotalAsPerGL  WHERE [#ARAginTable].[TransactionType] = 'AR : Refunds'

PRINT '--- Execute AR Aging Detail table'
SELECT AR.[TransactionType],AR.[Total], AR.[AsPerTransaction], AR.[AsPerGL], AR.[Diff], CASE WHEN AR.[Diff] = 0 THEN 'Pass' ELSE 'Failed' END AS [Result] 
FROM #ARAginTable AS AR
ORDER BY [Order]

DECLARE @ARDifference AS VARCHAR(MAX)
SELECT @ARDifference = CASE WHEN AR.[Diff] = 0 THEN '' ELSE COALESCE(@ARDifference + ', ', '') + [TransactionType] + ' : ' + CONVERT(VARCHAR(10),AR.[Diff]) END 
FROM #ARAginTable AS AR

--SET @ARDifference = RIGHT(@ARDifference,LEN(@ARDifference) - 1)
SET @vcDiff = @vcDiff + @ARDifference + ', '

DROP TABLE #ARAginTable

--- Transaction v/s GL Entries mapping details.
PRINT '--- Transaction v/s GL Entries mapping details.'

-- Check whether any Invoiced transaction are in general entries or not.
PRINT '-- Check whether any Invoiced transaction are in general entries or not.'
SELECT [OM].[vcPOppName],OBD.[vcBizDocID],DD.* FROM [dbo].[DepositMaster] AS DM
JOIN [dbo].[DepositeDetails] AS DD ON [DM].[numDepositId] = [DD].[numDepositID] AND tintDepositePage IN ( 2, 3 )
JOIN [dbo].[OpportunityMaster] AS OM ON OM.[numOppId] = DD.[numOppID] AND DM.[numDomainId] = OM.[numDomainId]
JOIN [dbo].[OpportunityBizDocs] AS OBD ON [OM].[numOppId] = [OBD].[numOppId] AND DD.[numOppBizDocsID] = [OBD].[numOppBizDocsId]
LEFT JOIN [dbo].[General_Journal_Header] AS GJH ON DM.[numDepositId] = [GJH].[numDepositId] AND DM.[numDomainId] = [GJH].[numDomainId]
WHERE [DM].[numDomainId] = @numDomainID
AND tintOpptype = 1
AND tintoppStatus = 1
AND numBizDocId = @AuthoritativeSalesBizDocId
AND OBD.bitAuthoritativeBizDocs = 1
AND ISNULL(OBD.tintDeferred, 0) <> 1
AND tintDepositePage IN ( 2, 3 )
AND ( ( ISNULL(monDepositAmount, 0) - ISNULL(monAppliedAmount, 0) ) > 0 )
AND ISNULL(DM.numReturnHeaderID,0) = 0
AND [OBD].[numOppBizDocsId] NOT IN 
(
	SELECT [DD].[numOppBizDocsID] FROM [dbo].[DepositMaster] AS DM
	JOIN [dbo].[DepositeDetails] AS DD ON [DM].[numDepositId] = [DD].[numDepositID] AND tintDepositePage IN ( 2, 3 ) 
	JOIN [dbo].[General_Journal_Header] AS GJH ON DM.[numDepositId] = [GJH].[numDepositId] AND DM.[numDomainId] = [GJH].[numDomainId]
	WHERE [DM].[numDomainId] = @numDomainID
	AND tintDepositePage IN ( 2, 3 )
	AND ( ( ISNULL(monDepositAmount, 0) - ISNULL(monAppliedAmount, 0) ) > 0 )
)

SELECT OM.[vcPOppName],OBD.[numOppBizDocsId], [OBD].[vcBizDocID] ,[GJD].[numJournalId],[OBD].[monDealAmount],
SUM(ISNULL([GJD].[numCreditAmt],0)) [Credit], [OBD].[monDealAmount] - SUM(ISNULL([GJD].[numCreditAmt],0))  AS [Diff]
FROM [dbo].[DepositMaster] AS DM
JOIN [dbo].[DepositeDetails] AS DD ON [DM].[numDepositId] = [DD].[numDepositID] AND tintDepositePage IN ( 2, 3 )
LEFT JOIN [dbo].[OpportunityMaster] AS OM ON OM.[numOppId] = DD.[numOppID] AND DM.[numDomainId] = OM.[numDomainId]
LEFT JOIN [dbo].[OpportunityBizDocs] AS OBD ON [OM].[numOppId] = [OBD].[numOppId] AND DD.[numOppBizDocsID] = [OBD].[numOppBizDocsId]
LEFT JOIN [dbo].[General_Journal_Header] AS GJH ON DD.[numOppID] = [GJH].[numOppId] AND [DD].[numOppBizDocsId] = [GJH].[numOppBizDocsId]
LEFT JOIN dbo.[General_Journal_Details] AS GJD ON [GJH].[numJournal_Id] = [GJD].[numJournalId]
WHERE [DM].[numDomainId] = @numDomainID
AND tintOpptype = 1
AND tintoppStatus = 1
AND numBizDocId = 287
AND OBD.bitAuthoritativeBizDocs = 1
AND ISNULL(OBD.tintDeferred, 0) <> 1
AND tintDepositePage IN ( 2, 3 )
AND ( ( ISNULL(monDepositAmount, 0) - ISNULL(monAppliedAmount, 0) ) > 0 )
GROUP BY OM.[vcPOppName],OBD.[numOppBizDocsId], [OBD].[vcBizDocID] ,[GJD].[numJournalId],[OBD].[monDealAmount]
ORDER BY OBD.[numOppBizDocsId],[OBD].[monDealAmount] - SUM(ISNULL([GJD].[numCreditAmt],0))

--==========================================================================================


---- Accounts Payable Diagnosis
PRINT '---- Accounts Payable Diagnosis'
--==========================================================================================
DECLARE @APJournalTotal AS MONEY
DECLARE @APTransactionTotal AS MONEY
DECLARE @AuthoritativePurchaseBizDocId AS INTEGER
DECLARE @BillPaymentTotal AS MONEY
SELECT  @AuthoritativePurchaseBizDocId = isnull(numAuthoritativePurchase,0) FROM AuthoritativeBizDocs WHERE numDomainId = @numDomainID ;

-- SELECT  isnull(numAuthoritativePurchase,0) FROM AuthoritativeBizDocs WHERE numDomainId = @numDomainID ;
-- JOURNAL ENTRIES TOTAL
PRINT '-- JOURNAL ENTRIES TOTAL'
SELECT  @APJournalTotal = SUM([GJD].[numDebitAmt]) - SUM([GJD].[numCreditAmt])
FROM    [dbo].[General_Journal_Header] AS GJH
        JOIN [dbo].[General_Journal_Details] AS GJD ON [GJH].[numJournal_Id] = [GJD].[numJournalId]
                                                       AND gjh.[numDomainId] = [GJD].[numDomainId]
WHERE   1 = 1
        AND [GJH].[numDomainId] = @numDomainID
        AND [GJD].[numChartAcntId] IN (SELECT numaccountID FROM [dbo].[Chart_Of_Accounts] AS COA 
									   WHERE COA.vcAccountCode LIKE '0102010201' 
										 AND [COA].[numDomainId] = @numDomainID
									  )

IF ISNULL(@APJournalTotal,0) = 0
SET @APJournalTotal= 0 

PRINT '@APJournalTotal : ' + CONVERT(VARCHAR(18), @APJournalTotal)

-- TRANSACTIONS TOTAL
PRINT '-- TRANSACTIONS TOTAL'
SELECT @APTransactionTotal = SUM(APTable.DealAmount)
FROM
(
SELECT  ISNULL(ISNULL(OB.monDealAmount * ISNULL(Opp.fltExchangeRate, 1), 0) - ISNULL(OB.monAmountPaid * ISNULL(Opp.fltExchangeRate,1), 0),0) DealAmount 
FROM    OpportunityMaster Opp
        JOIN OpportunityBizDocs OB ON OB.numOppId = Opp.numOppID
        LEFT OUTER JOIN [Currency] C ON Opp.[numCurrencyID] = C.[numCurrencyID]
WHERE   tintOpptype = 2
        AND tintoppStatus = 1
        AND opp.numDomainID = @numDomainID
        AND OB.bitAuthoritativeBizDocs = 1
        AND ISNULL(OB.tintDeferred, 0) <> 1
        AND numBizDocId = @AuthoritativePurchaseBizDocId
GROUP BY OB.numOppId ,
        Opp.numDivisionId ,
        OB.numOppBizDocsId ,
        Opp.fltExchangeRate ,
        OB.numBizDocId ,
        OB.dtCreatedDate ,
        Opp.bitBillingTerms ,
        Opp.intBillingDays ,
        OB.monDealAmount ,
        Opp.numCurrencyID ,
        OB.[dtFromDate] ,
        OB.monAmountPaid
HAVING  ( ISNULL(OB.monDealAmount * ISNULL(Opp.fltExchangeRate, 1), 0) - ISNULL(OB.monAmountPaid * ISNULL(Opp.fltExchangeRate, 1), 0) ) > 0

UNION ALL 

--Add Bill Amount
SELECT  ISNULL(SUM(BH.monAmountDue), 0) - ISNULL(SUM(BH.monAmtPaid), 0)  AS [DealAmount]
FROM    BillHeader BH
WHERE   BH.numDomainID = @numDomainID
        AND ISNULL(BH.monAmountDue, 0) - ISNULL(BH.monAmtPaid, 0) > 0
		AND ISNULL(BH.[numOppId],0) = 0
GROUP BY BH.numDivisionId ,
        BH.dtDueDate
HAVING  ISNULL(SUM(BH.monAmountDue), 0) - ISNULL(SUM(BH.monAmtPaid), 0) > 0

UNION ALL
--Add Write Check entry (As Amount Paid)
SELECT  0  AS [DealAmount] 
FROM    CheckHeader CH
        JOIN dbo.CheckDetails CD ON CH.numCheckHeaderID = CD.numCheckHeaderID
        INNER JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = CD.numChartAcntId
WHERE   CH.numDomainID = @numDomainID
        AND CH.tintReferenceType = 1
        AND COA.vcAccountCode LIKE '01020102%'
GROUP BY CD.numCustomerId ,
        CH.dtCheckDate

--Show Impact of AP journal entries in report as well 
UNION ALL
SELECT  CASE WHEN GJD.numDebitAmt > 0 THEN -1 * GJD.numDebitAmt ELSE GJD.numCreditAmt END  AS [DealAmount]
FROM    dbo.General_Journal_Header GJH
        INNER JOIN dbo.General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId
                                                      AND GJH.numDomainId = GJD.numDomainId
        INNER JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = GJD.numChartAcntId
WHERE   GJH.numDomainId = @numDomainID
        AND COA.vcAccountCode LIKE '01020102%'
        AND ISNULL(numOppId, 0) = 0
        AND ISNULL(numOppBizDocsId, 0) = 0
        AND ISNULL(GJH.numBillID, 0) = 0
        AND ISNULL(GJH.numBillPaymentID, 0) = 0
        AND ISNULL(GJH.numCheckHeaderID, 0) = 0
        AND ISNULL(GJH.numReturnID, 0) = 0
        AND ISNULL(GJH.numPayrollDetailID, 0) = 0
        
UNION ALL
--Add Commission Amount
SELECT  ISNULL(SUM(BDC.numComissionAmount - ISNULL(PT.monCommissionAmt, 0)), 0) AS [DealAmount]
FROM    BizDocComission BDC
        JOIN OpportunityBizDocs OBD ON BDC.numOppBizDocId = OBD.numOppBizDocsId
        JOIN OpportunityMaster Opp ON OPP.numOppId = OBD.numOppId
        JOIN Domain D ON D.numDomainID = BDC.numDomainID
        LEFT JOIN dbo.PayrollTracking PT ON BDC.numComissionID = PT.numComissionID
WHERE   OPP.numDomainID = @numDomainID
        AND BDC.numDomainID = @numDomainID
        AND OBD.bitAuthoritativeBizDocs = 1
GROUP BY D.numDivisionId ,
        OBD.numOppId ,
        BDC.numOppBizDocId ,
        OPP.numCurrencyID ,
        BDC.numComissionAmount ,
        OBD.dtCreatedDate
HAVING  ISNULL(BDC.numComissionAmount, 0) > 0


UNION ALL 
--Standalone Refund against Account Receivable
SELECT  CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt * -1 ELSE GJD.numCreditAmt END DealAmount
FROM    dbo.ReturnHeader RH
        JOIN dbo.General_Journal_Header GJH ON RH.numReturnHeaderID = GJH.numReturnID
        JOIN General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId
		INNER JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = RH.[numAccountID]
WHERE   RH.tintReturnType = 4
        AND RH.numDomainId = @numDomainID
        AND COA.vcAccountCode LIKE '01020102%'
) AS APTable

IF ISNULL(@APTransactionTotal,0) = 0
SET @APTransactionTotal = 0

PRINT '@APTransactionTotal : ' + CONVERT(VARCHAR(18), @APTransactionTotal)

-- Bill Payment Details (UnApplied Amount)
PRINT '-- Bill Payment Details (UnApplied Amount)'
SELECT @BillPaymentTotal = SUM(ISNULL([PaymentTable].[DealAmount],0)) FROM 
(
SELECT SUM(ISNULL(monPaymentAmount,0) - ISNULL(monAppliedAmount,0))  AS [DealAmount]
FROM BillPaymentHeader BPH
WHERE BPH.numDomainId=@numDomainID   
AND (ISNULL(monPaymentAmount,0) - ISNULL(monAppliedAmount,0)) > 0
GROUP BY BPH.numDivisionId
) AS [PaymentTable]

IF ISNULL(@BillPaymentTotal,0) = 0
SET @BillPaymentTotal = 0

-- AR Aging Summary
PRINT '-- AR Aging Summary'
SELECT @APJournalTotal AS [APJournalTotal], 
	   @APTransactionTotal - @BillPaymentTotal AS [APTransactionTotal],
	   @APJournalTotal + @APTransactionTotal - @BillPaymentTotal AS [Difference],
	   (CASE WHEN (@APJournalTotal + @APTransactionTotal - @BillPaymentTotal) = 0 THEN 'Pass' ELSE 'Failed' END) AS [Result]

IF (@APJournalTotal + @APTransactionTotal - @BillPaymentTotal) <> 0
BEGIN
SET @vcDiff = @vcDiff + '@APJournalTotal + @APTransactionTotal - @BillPaymentTotal : ' + CONVERT(VARCHAR(100),(@APJournalTotal + @APTransactionTotal - @BillPaymentTotal))
END

-- AP Aging Details
PRINT '-- AP Aging Details'
DECLARE @POAuthoritativeTotal AS MONEY
DECLARE @BillTotal AS MONEY
DECLARE @CheckTotal AS MONEY
DECLARE @OtherAPEntriesTotal AS MONEY
DECLARE @CommissionTotal AS MONEY
DECLARE @RefundReturnForAPTotal AS MONEY

SELECT  @POAuthoritativeTotal = SUM([POAuthTable].DealAmount) FROM 
(
SELECT ISNULL(ISNULL(OB.monDealAmount * ISNULL(Opp.fltExchangeRate, 1), 0) - ISNULL(OB.monAmountPaid * ISNULL(Opp.fltExchangeRate,1), 0),0) DealAmount 
FROM    OpportunityMaster Opp
        JOIN OpportunityBizDocs OB ON OB.numOppId = Opp.numOppID
        LEFT OUTER JOIN [Currency] C ON Opp.[numCurrencyID] = C.[numCurrencyID]
WHERE   tintOpptype = 2
        AND tintoppStatus = 1
        AND opp.numDomainID = @numDomainID
        AND OB.bitAuthoritativeBizDocs = 1
        AND ISNULL(OB.tintDeferred, 0) <> 1
        AND numBizDocId = @AuthoritativePurchaseBizDocId
GROUP BY OB.numOppId ,
        Opp.numDivisionId ,
        OB.numOppBizDocsId ,
        Opp.fltExchangeRate ,
        OB.numBizDocId ,
        OB.dtCreatedDate ,
        Opp.bitBillingTerms ,
        Opp.intBillingDays ,
        OB.monDealAmount ,
        Opp.numCurrencyID ,
        OB.[dtFromDate] ,
        OB.monAmountPaid
HAVING  ( ISNULL(OB.monDealAmount * ISNULL(Opp.fltExchangeRate, 1), 0) - ISNULL(OB.monAmountPaid * ISNULL(Opp.fltExchangeRate, 1), 0) ) > 0
) AS [POAuthTable]

--Add Bill Amount
SELECT  @BillTotal = SUM([BillTable].DealAmount) FROM
(
SELECT  ISNULL(SUM(BH.monAmountDue), 0) - ISNULL(SUM(BH.monAmtPaid), 0)  AS [DealAmount]
FROM    BillHeader BH
WHERE   BH.numDomainID = @numDomainID
        AND ISNULL(BH.monAmountDue, 0) - ISNULL(BH.monAmtPaid, 0) > 0
GROUP BY BH.numDivisionId ,
        BH.dtDueDate
HAVING  ISNULL(SUM(BH.monAmountDue), 0) - ISNULL(SUM(BH.monAmtPaid), 0) > 0
) AS [BillTable]

--Add Write Check entry (As Amount Paid)
SELECT  @CheckTotal = SUM(ISNULL([CheckTable].[DealAmount],0)) FROM 
(
SELECT  0  AS [DealAmount] 
FROM    CheckHeader CH
        JOIN dbo.CheckDetails CD ON CH.numCheckHeaderID = CD.numCheckHeaderID
        INNER JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = CD.numChartAcntId
WHERE   CH.numDomainID = @numDomainID
        AND CH.tintReferenceType = 1
        AND COA.vcAccountCode LIKE '01020102%'
GROUP BY CD.numCustomerId ,
        CH.dtCheckDate
) AS [CheckTable]

--Show Impact of AP journal entries in report as well 
SELECT  @OtherAPEntriesTotal = SUM(ISNULL([OtherTable].[DealAmount],0)) FROM
(
SELECT  CASE WHEN GJD.numDebitAmt > 0 THEN -1 * GJD.numDebitAmt ELSE GJD.numCreditAmt END  AS [DealAmount]
FROM    dbo.General_Journal_Header GJH
        INNER JOIN dbo.General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId
                                                      AND GJH.numDomainId = GJD.numDomainId
        INNER JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = GJD.numChartAcntId
WHERE   GJH.numDomainId = @numDomainID
        AND COA.vcAccountCode LIKE '01020102%'
        AND ISNULL(numOppId, 0) = 0
        AND ISNULL(numOppBizDocsId, 0) = 0
        AND ISNULL(GJH.numBillID, 0) = 0
        AND ISNULL(GJH.numBillPaymentID, 0) = 0
        AND ISNULL(GJH.numCheckHeaderID, 0) = 0
        AND ISNULL(GJH.numReturnID, 0) = 0
        AND ISNULL(GJH.numPayrollDetailID, 0) = 0
) AS [OtherTable]        

--Add Commission Amount
SELECT  @CommissionTotal = SUM(ISNULL(TabCommission.[DealAmount],0)) FROM
(
SELECT  ISNULL(SUM(BDC.numComissionAmount - ISNULL(PT.monCommissionAmt, 0)), 0) AS [DealAmount]
FROM    BizDocComission BDC
        JOIN OpportunityBizDocs OBD ON BDC.numOppBizDocId = OBD.numOppBizDocsId
        JOIN OpportunityMaster Opp ON OPP.numOppId = OBD.numOppId
        JOIN Domain D ON D.numDomainID = BDC.numDomainID
        LEFT JOIN dbo.PayrollTracking PT ON BDC.numComissionID = PT.numComissionID
WHERE   OPP.numDomainID = @numDomainID
        AND BDC.numDomainID = @numDomainID
        AND OBD.bitAuthoritativeBizDocs = 1
GROUP BY D.numDivisionId ,
        OBD.numOppId ,
        BDC.numOppBizDocId ,
        OPP.numCurrencyID ,
        BDC.numComissionAmount ,
        OBD.dtCreatedDate
HAVING  ISNULL(BDC.numComissionAmount, 0) > 0
) AS [TabCommission]

--Standalone Refund against Account Receivable
SELECT  @RefundReturnForAPTotal = SUM(ISNULL([RefundTable].[DealAmount],0)) FROM
(
SELECT  CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt * -1 ELSE GJD.numCreditAmt END DealAmount
FROM    dbo.ReturnHeader RH
        JOIN dbo.General_Journal_Header GJH ON RH.numReturnHeaderID = GJH.numReturnID
        JOIN General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId
        INNER JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = GJD.numChartAcntId
WHERE   RH.tintReturnType = 4
        AND RH.numDomainId = @numDomainID
        AND COA.vcAccountCode LIKE '01020102%'
) AS [RefundTable]

IF ISNULL(@POAuthoritativeTotal,0) = 0
SET @POAuthoritativeTotal = 0

IF ISNULL(@BillTotal,0) = 0
SET @BillTotal = 0

IF ISNULL(@CheckTotal,0) = 0
SET @CheckTotal = 0

IF ISNULL(@OtherAPEntriesTotal,0) = 0
SET @OtherAPEntriesTotal = 0

IF ISNULL(@CommissionTotal,0) = 0
SET @CommissionTotal = 0

IF ISNULL(@RefundReturnForAPTotal,0) = 0
SET @RefundReturnForAPTotal = 0


PRINT '@POAuthoritativeTotal : ' + CONVERT(VARCHAR(18), @POAuthoritativeTotal)
PRINT '@BillTotal : ' + CONVERT(VARCHAR(18), @BillTotal)
PRINT '@CheckTotal : ' + CONVERT(VARCHAR(18), @CheckTotal)
PRINT '@OtherAPEntriesTotal : ' + CONVERT(VARCHAR(18), @OtherAPEntriesTotal)
PRINT '@CommissionTotal : ' + CONVERT(VARCHAR(18), @CommissionTotal)
PRINT '@RefundReturnForAPTotal : ' + CONVERT(VARCHAR(18), @RefundReturnForAPTotal)
PRINT '@BillPaymentTotal : ' + CONVERT(VARCHAR(18), @BillPaymentTotal)

SELECT * INTO #APAginTable FROM
(
	SELECT 'AP : PO Authoritative' AS [TransactionType],@POAuthoritativeTotal AS Total, 0 AS AsPerTransaction, 0 AS AsPerGL, 0 AS [Diff], 1 AS [Order] 
	UNION 
	SELECT 'AP : Stand Alone Bills' AS [TransactionType],@BillTotal AS Total, 0 AS AsPerTransaction, 0 AS AsPerGL, 0 AS [Diff], 2 AS [Order]
	UNION 
	SELECT 'AP : Checks' AS [TransactionType],@CheckTotal AS Total, 0 AS AsPerTransaction, 0 AS AsPerGL , 0 AS [Diff], 3 AS [Order]
	UNION 
	SELECT 'AP : Other AP Entries' AS [TransactionType],@OtherAPEntriesTotal AS Total, 0 AS AsPerTransaction, 0 AS AsPerGL , 0 AS [Diff], 4 AS [Order]
	UNION 
	SELECT 'AP : Commission' AS [TransactionType],@CommissionTotal AS Total, 0 AS AsPerTransaction, 0 AS AsPerGL, 0 AS [Diff], 5 AS [Order]
	UNION 
	SELECT 'AP : Refunds' AS [TransactionType],@RefundReturnForAPTotal AS Total, 0 AS AsPerTransaction, 0 AS AsPerGL, 0 AS [Diff], 6 AS [Order]
	--UNION 
	--SELECT 'Bill Payment Total' AS [TransactionType],@BillPaymentTotal AS Total, 0 AS AsPerTransaction, 0 AS AsPerGL, 0 AS [Diff], 7 AS [Order]
) AS APAginTable


DECLARE @POAuthoritativeTotalAsPerTransaction AS MONEY
DECLARE @BillTotalAsPerTransaction AS MONEY
DECLARE @CheckTotalAsPerTransaction AS MONEY
DECLARE @OtherAPEntriesTotalAsPerTransaction AS MONEY
DECLARE @CommissionTotalAsPerTransaction AS MONEY
DECLARE @RefundReturnForAPTotalAsPerTransaction AS MONEY
DECLARE @BillPaymentTotalAsPerTransaction AS MONEY

DECLARE @POAuthoritativeTotalAsPerGL AS MONEY
DECLARE @BillTotalAsPerGL AS MONEY
DECLARE @CheckTotalAsPerGL AS MONEY
DECLARE @OtherAPEntriesTotalAsPerGL AS MONEY
DECLARE @CommissionTotalAsPerGL AS MONEY
DECLARE @RefundReturnForAPTotalAsPerGL AS MONEY
DECLARE @BillPaymentTotalAsPerGL AS MONEY

SELECT  @POAuthoritativeTotalAsPerTransaction = SUM([POAuthTable].DealAmount) FROM 
(
SELECT ISNULL(ISNULL(OB.monDealAmount * ISNULL(Opp.fltExchangeRate, 1), 0) - ISNULL(OB.monAmountPaid * ISNULL(Opp.fltExchangeRate,1), 0),0) DealAmount 
FROM    OpportunityMaster Opp
        JOIN OpportunityBizDocs OB ON OB.numOppId = Opp.numOppID
WHERE   tintOpptype = 2
        AND tintoppStatus = 1
        AND opp.numDomainID = @numDomainID
        AND OB.bitAuthoritativeBizDocs = 1
        AND ISNULL(OB.tintDeferred, 0) <> 1
        AND numBizDocId = @AuthoritativePurchaseBizDocId
GROUP BY OB.numOppId ,
        Opp.numDivisionId ,
        OB.numOppBizDocsId ,
        Opp.fltExchangeRate ,
        OB.numBizDocId ,
        OB.dtCreatedDate ,
        Opp.bitBillingTerms ,
        Opp.intBillingDays ,
        OB.monDealAmount ,
        Opp.numCurrencyID ,
        OB.[dtFromDate] ,
        OB.monAmountPaid
HAVING  ( ISNULL(OB.monDealAmount * ISNULL(Opp.fltExchangeRate, 1), 0) - ISNULL(OB.monAmountPaid * ISNULL(Opp.fltExchangeRate, 1), 0) ) > 0
) AS [POAuthTable]

SELECT  @POAuthoritativeTotalAsPerGL = SUM([GJD].[numCreditAmt]) - SUM(ISNULL(OB.monAmountPaid * ISNULL(Opp.fltExchangeRate,1), 0))
FROM    OpportunityMaster Opp
        JOIN OpportunityBizDocs OB ON OB.numOppId = Opp.numOppID
		JOIN [dbo].[General_Journal_Header] AS GJH ON [Opp].[numOppId] = [GJH].[numOppId] AND [OB].[numOppBizDocsId] = [GJH].[numOppBizDocsId] AND Opp.[numDomainId] = [GJH].[numDomainId]
		JOIN [dbo].[General_Journal_Details] AS GJD ON [GJH].[numJournal_Id] = [GJD].[numJournalId] AND GJH.[numDomainId] = GJD.[numDomainId]
		WHERE   tintOpptype = 2
        AND tintoppStatus = 1
        AND opp.numDomainID = @numDomainID
        AND OB.bitAuthoritativeBizDocs = 1
        AND ISNULL(OB.tintDeferred, 0) <> 1
        AND numBizDocId = @AuthoritativePurchaseBizDocId
		AND [GJD].[numChartAcntId] IN (SELECT numaccountID FROM [dbo].[Chart_Of_Accounts] AS COA 
									   WHERE COA.vcAccountCode LIKE '0102010201' 
										 AND [COA].[numDomainId] = @numDomainID
									  )

--Add Bill Amount
SELECT  @BillTotalAsPerTransaction = SUM([BillTable].DealAmount) FROM
(
SELECT  ISNULL(SUM(BH.monAmountDue), 0) - ISNULL(SUM(BH.monAmtPaid), 0)  AS [DealAmount]
FROM    BillHeader BH
WHERE   BH.numDomainID = @numDomainID
        AND ISNULL(BH.monAmountDue, 0) - ISNULL(BH.monAmtPaid, 0) > 0
GROUP BY BH.numDivisionId ,
        BH.dtDueDate
HAVING  ISNULL(SUM(BH.monAmountDue), 0) - ISNULL(SUM(BH.monAmtPaid), 0) > 0
) AS [BillTable]

SELECT  @BillTotalAsPerGL = SUM([GJD].[numCreditAmt]) - ISNULL(SUM(BH.monAmtPaid), 0)
FROM    BillHeader BH
JOIN [dbo].[General_Journal_Header] AS GJH ON BH.[numBillID] = [GJH].[numBillID]
JOIN [dbo].[General_Journal_Details] AS GJD ON [GJH].[numJournal_Id] = [GJD].[numJournalId]
WHERE   BH.numDomainID = @numDomainID
        AND ISNULL(BH.monAmountDue, 0) - ISNULL(BH.monAmtPaid, 0) > 0
		AND [GJD].[numChartAcntId] IN (SELECT numaccountID FROM [dbo].[Chart_Of_Accounts] AS COA 
									   WHERE COA.vcAccountCode LIKE '0102010201' 
										 AND [COA].[numDomainId] = @numDomainID
									  )

--Add Write Check entry (As Amount Paid)
SET  @CheckTotalAsPerTransaction = 0
SET  @CheckTotalAsPerGL = 0

--Show Impact of AP journal entries in report as well 
SET @OtherAPEntriesTotalAsPerTransaction = 0
SELECT  @OtherAPEntriesTotalAsPerGL = SUM(ISNULL([OtherTable].[DealAmount],0)) FROM
(
SELECT  CASE WHEN GJD.numDebitAmt > 0 THEN -1 * GJD.numDebitAmt ELSE GJD.numCreditAmt END  AS [DealAmount]
FROM    dbo.General_Journal_Header GJH
        INNER JOIN dbo.General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId
                                                      AND GJH.numDomainId = GJD.numDomainId
        INNER JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = GJD.numChartAcntId
WHERE   GJH.numDomainId = @numDomainID
        AND COA.vcAccountCode LIKE '01020102%'
        AND ISNULL(numOppId, 0) = 0
        AND ISNULL(numOppBizDocsId, 0) = 0
        AND ISNULL(GJH.numBillID, 0) = 0
        AND ISNULL(GJH.numBillPaymentID, 0) = 0
        AND ISNULL(GJH.numCheckHeaderID, 0) = 0
        AND ISNULL(GJH.numReturnID, 0) = 0
        AND ISNULL(GJH.numPayrollDetailID, 0) = 0
) AS [OtherTable]     

----Add Commission Amount
SELECT  @CommissionTotalAsPerTransaction = SUM(ISNULL(TabCommission.[DealAmount],0)) FROM
(
SELECT  ISNULL(SUM(BDC.numComissionAmount - ISNULL(PT.monCommissionAmt, 0)), 0) AS [DealAmount]
FROM    BizDocComission BDC
        JOIN OpportunityBizDocs OBD ON BDC.numOppBizDocId = OBD.numOppBizDocsId
        JOIN OpportunityMaster Opp ON OPP.numOppId = OBD.numOppId
        JOIN Domain D ON D.numDomainID = BDC.numDomainID
        LEFT JOIN dbo.PayrollTracking PT ON BDC.numComissionID = PT.numComissionID
WHERE   OPP.numDomainID = @numDomainID
        AND BDC.numDomainID = @numDomainID
        AND OBD.bitAuthoritativeBizDocs = 1
GROUP BY D.numDivisionId ,
        OBD.numOppId ,
        BDC.numOppBizDocId ,
        OPP.numCurrencyID ,
        BDC.numComissionAmount ,
        OBD.dtCreatedDate
HAVING  ISNULL(BDC.numComissionAmount, 0) > 0
) AS [TabCommission]

SELECT  @CommissionTotalAsPerGL = 0

----Standalone Refund against Account Receivable
SELECT  @RefundReturnForAPTotalAsPerTransaction = SUM(ISNULL([RefundTable].[DealAmount],0)) FROM
(
SELECT  SUM(RH.[monBizDocAmount])  AS DealAmount
FROM    dbo.ReturnHeader RH
        INNER JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = RH.[numAccountID]
WHERE   RH.tintReturnType = 4
        AND RH.numDomainId = @numDomainID
        AND COA.vcAccountCode LIKE '01020102%'
) AS [RefundTable]

SELECT  @RefundReturnForAPTotalAsPerGL = SUM(ISNULL([RefundTable].[DealAmount],0)) FROM
(
SELECT  CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt * -1 ELSE GJD.numCreditAmt END DealAmount
FROM    dbo.ReturnHeader RH
        JOIN dbo.General_Journal_Header GJH ON RH.numReturnHeaderID = GJH.numReturnID
        JOIN General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId
        INNER JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = GJD.numChartAcntId
WHERE   RH.tintReturnType = 4
        AND RH.numDomainId = @numDomainID
        AND COA.vcAccountCode LIKE '01020102%'
) AS [RefundTable]

SELECT @BillPaymentTotalAsPerTransaction = SUM(ISNULL(monPaymentAmount,0) - ISNULL(monAppliedAmount,0)) 
FROM BillPaymentHeader BPH
WHERE BPH.numDomainId=@numDomainID   
AND (ISNULL(monPaymentAmount,0) - ISNULL(monAppliedAmount,0)) > 0

--SET @BillPaymentTotalAsPerGL = 0
SELECT @BillPaymentTotalAsPerGL = SUM(ISNULL(monPaymentAmount,0)) - SUM(ISNULL(monAppliedAmount,0))  
FROM BillPaymentHeader BPH
JOIN [dbo].[General_Journal_Header] AS GJH ON GJH.[numBillPaymentID] = [BPH].[numBillPaymentID] AND [GJH].[numDomainId] = [BPH].[numDomainID]
JOIN [dbo].[General_Journal_Details] AS GJD ON [GJH].[numJournal_Id] = [GJD].[numJournalId]
JOIN [dbo].[Chart_Of_Accounts] AS COA ON [GJD].[numChartAcntId] = [COA].[numAccountId]
WHERE BPH.numDomainId=@numDomainID   
AND COA.vcAccountCode LIKE '0102010201'

IF ISNULL(@POAuthoritativeTotalAsPerTransaction,0) = 0
SET @POAuthoritativeTotalAsPerTransaction = 0

IF ISNULL(@POAuthoritativeTotalAsPerGL,0) = 0
SET @POAuthoritativeTotalAsPerGL = 0

IF ISNULL(@BillTotalAsPerTransaction,0) = 0
SET @BillTotalAsPerTransaction = 0

IF ISNULL(@BillTotalAsPerGL,0) = 0
SET @BillTotalAsPerGL = 0
   
IF ISNULL(@OtherAPEntriesTotalAsPerGL,0) = 0
SET @OtherAPEntriesTotalAsPerGL = 0

IF ISNULL(@CommissionTotalAsPerTransaction,0) = 0
SET @CommissionTotalAsPerTransaction = 0

IF ISNULL(@RefundReturnForAPTotalAsPerTransaction,0) = 0
SET @RefundReturnForAPTotalAsPerTransaction = 0

IF ISNULL(@RefundReturnForAPTotalAsPerGL,0) = 0
SET @RefundReturnForAPTotalAsPerGL = 0

IF ISNULL(@BillPaymentTotalAsPerTransaction,0) = 0
SET @BillPaymentTotalAsPerTransaction = 0

IF ISNULL(@BillPaymentTotalAsPerGL,0) = 0
SET @BillPaymentTotalAsPerGL = 0

PRINT '@POAuthoritativeTotalAsPerTransaction : ' + CONVERT(VARCHAR(18), @POAuthoritativeTotalAsPerTransaction)
PRINT '@POAuthoritativeTotalAsPerGL : ' + CONVERT(VARCHAR(18), @POAuthoritativeTotalAsPerGL)
PRINT '@BillTotalAsPerTransaction : ' + CONVERT(VARCHAR(18), @BillTotalAsPerTransaction)
PRINT '@BillTotalAsPerGL : ' + CONVERT(VARCHAR(18), @BillTotalAsPerGL)
PRINT '@CheckTotalAsPerTransaction : ' + CONVERT(VARCHAR(18), @CheckTotalAsPerTransaction)
PRINT '@CheckTotalAsPerGL : ' + CONVERT(VARCHAR(18), @CheckTotalAsPerGL)
PRINT '@OtherAPEntriesTotalAsPerTransaction : ' + CONVERT(VARCHAR(18), @OtherAPEntriesTotalAsPerTransaction)
PRINT '@OtherAPEntriesTotalAsPerGL : ' + CONVERT(VARCHAR(18), @OtherAPEntriesTotalAsPerGL)
PRINT '@CommissionTotalAsPerTransaction : ' + CONVERT(VARCHAR(18), @CommissionTotalAsPerTransaction)
PRINT '@CommissionTotalAsPerGL : ' + CONVERT(VARCHAR(18), @CommissionTotalAsPerGL)
PRINT '@RefundReturnForAPTotalAsPerTransaction : ' + CONVERT(VARCHAR(18), @RefundReturnForAPTotalAsPerTransaction)
PRINT '@RefundReturnForAPTotalAsPerGL : ' + CONVERT(VARCHAR(18), @RefundReturnForAPTotalAsPerGL)
PRINT '@BillPaymentTotalAsPerTransaction : ' + CONVERT(VARCHAR(18), @BillPaymentTotalAsPerTransaction)
PRINT '@BillPaymentTotalAsPerGL : ' + CONVERT(VARCHAR(18), @BillPaymentTotalAsPerGL)

UPDATE #APAginTable SET [AsPerTransaction] = @POAuthoritativeTotalAsPerTransaction,[AsPerGL] = @POAuthoritativeTotalAsPerGL, [Diff] = @POAuthoritativeTotalAsPerTransaction - @POAuthoritativeTotalAsPerGL 
WHERE #APAginTable.[TransactionType] = 'AP : PO Authoritative'
UPDATE #APAginTable SET [AsPerTransaction] = @BillTotalAsPerTransaction,[AsPerGL] = @BillTotalAsPerGL, [Diff] = @BillTotalAsPerTransaction - @BillTotalAsPerGL 
WHERE #APAginTable.[TransactionType] = 'AP : Stand Alone Bills'
UPDATE #APAginTable SET [AsPerTransaction] = @CheckTotalAsPerTransaction,[AsPerGL] = @CheckTotalAsPerGL, [Diff] = @CheckTotalAsPerTransaction - @CheckTotalAsPerGL 
WHERE #APAginTable.[TransactionType] = 'Check Total'
UPDATE #APAginTable SET [AsPerTransaction] = @OtherAPEntriesTotalAsPerTransaction ,[AsPerGL] = @OtherAPEntriesTotalAsPerGL, [Diff] = @OtherAPEntriesTotalAsPerTransaction - @OtherAPEntriesTotalAsPerGL  
WHERE #APAginTable.[TransactionType] = 'AP : Other AP Entries'
UPDATE #APAginTable SET [AsPerTransaction] = @CommissionTotalAsPerTransaction ,[AsPerGL] = @CommissionTotalAsPerGL, [Diff] = @CommissionTotalAsPerTransaction - @CommissionTotalAsPerGL  
WHERE #APAginTable.[TransactionType] = 'AP : Commission'
UPDATE #APAginTable SET [AsPerTransaction] = @RefundReturnForAPTotalAsPerTransaction ,[AsPerGL] = @RefundReturnForAPTotalAsPerGL, [Diff] = @RefundReturnForAPTotalAsPerTransaction - @RefundReturnForAPTotalAsPerGL  
WHERE #APAginTable.[TransactionType] = 'AP : Refunds'
--UPDATE #APAginTable SET [AsPerTransaction] = @BillPaymentTotalAsPerTransaction ,[AsPerGL] = @BillPaymentTotalAsPerGL, [Diff] = @BillPaymentTotalAsPerTransaction - @BillPaymentTotalAsPerGL  
--WHERE #APAginTable.[TransactionType] = 'Bill Payment Total'

--SELECT @POAuthoritativeTotalAsPerGL + @BillTotalAsPerGL + @CheckTotalAsPerGL + @OtherAPEntriesTotalAsPerGL + @CommissionTotalAsPerGL + @RefundReturnForAPTotalAsPerGL

PRINT 'Execute AP Aging Details'
SELECT AP.[TransactionType],AP.[Total], AP.[AsPerTransaction], AP.[AsPerGL], AP.[Diff], CASE WHEN AP.[Diff] = 0 THEN 'Pass' ELSE 'Failed' END AS [Result] 
FROM #APAginTable AS AP
ORDER BY [Order]

DECLARE @APDifference AS VARCHAR(MAX)
SELECT @APDifference = CASE WHEN AP.[Diff] = 0 THEN '' ELSE COALESCE(@APDifference + ', ', '')  + [TransactionType] + ' : ' + CONVERT(VARCHAR(10),AP.[Diff]) END 
FROM #APAginTable AS AP

--SET @APDifference = RIGHT(@APDifference,LEN(@APDifference) - 1) 
SET @vcDiff = @vcDiff + @APDifference

DROP TABLE #APAginTable

SELECT @vcDiff = LTRIM(@vcDiff)
SELECT @vcDiff = RTRIM(@vcDiff)
IF LEN(@vcDiff) <= 5
	SET @vcDiff = ''

SELECT ISNULL(@vcDiff,0) AS [Difference]
END
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ChartofAccountsDetail')
DROP PROCEDURE USP_ChartofAccountsDetail
GO
-- EXEC USP_ChartofAccountsDetail 169,'2015-10-24 11:03:12.857','6020'
CREATE PROCEDURE [dbo].[USP_ChartofAccountsDetail]
	@numDomainID NUMERIC(9),
    @dtToDate DATETIME,
    @vcAccountId AS VARCHAR(500)
    
AS 
    BEGIN
    
    	/*RollUp of Sub Accounts */
		SELECT  COA2.numAccountId INTO #Temp /*,COA1.vcAccountCode ,COA2.vcAccountCode*/
		FROM    dbo.Chart_Of_Accounts COA1
				INNER JOIN dbo.Chart_Of_Accounts COA2 
				ON COA1.numDomainId = COA2.numDomainId
				AND COA2.vcAccountCode LIKE COA1.vcAccountCode  + '%'
		WHERE   COA1.numAccountId IN(SELECT  cast(ID AS NUMERIC(9)) FROM dbo.SplitIDs(@vcAccountId, ','))
				AND COA1.numDomainId = @numDomainID
		SELECT @vcAccountId = ISNULL( STUFF((SELECT ',' + CAST(numAccountID AS VARCHAR(10)) FROM #Temp FOR XML PATH('')),1, 1, '') , '') 

		create table #Accounts(ID NUMERIC(9));
		insert into #Accounts 
		SELECT CAST(ID AS NUMERIC(9))FROM dbo.SplitIDs(@vcAccountId, ',')

		SELECT     
			h.numDomainId,  
			coa.numAccountId,  
			CompanyName = ci.vcCompanyName,  
			dm.numDivisionID,
			0 AS Opening,
			CONVERT(VARCHAR(20), SUM(ISNULL(d.numDebitAmt, 0))) TotalDebit,
			CONVERT(VARCHAR(20), SUM(ISNULL(d.numCreditAmt, 0))) TotalCredit,
			(SUM(ISNULL(d.numDebitAmt, 0)) -  SUM(ISNULL(d.numCreditAmt, 0)))  AS Closing,
			COA.numParntAcntTypeId
			
		FROM dbo.General_Journal_Header h 
		INNER JOIN dbo.General_Journal_Details d 
			ON h.numJournal_Id = d.numJournalId 
		LEFT JOIN dbo.Chart_Of_Accounts coa
			ON d.numChartAcntId = coa.numAccountId 
		LEFT JOIN dbo.DivisionMaster dm 
		LEFT JOIN dbo.CompanyInfo ci 
			ON dm.numCompanyID = ci.numCompanyId 
			ON d.numCustomerId = dm.numDivisionID 
		WHERE
			h.numDomainId = @numDomainID 
			AND ISNULL(dm.numDivisionID,0) > 0
			AND coa.numAccountId IN (SELECT ID FROM #Accounts)

		GROUP BY h.[numDomainId],  
						coa.[numAccountId],  
						ci.vcCompanyName,  
						dm.numDivisionID,  
						COA.numParntAcntTypeId  
		order by  ci.vcCompanyName

		drop table #accounts



    END

GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ChartofChildAccounts' ) 
    DROP PROCEDURE USP_ChartofChildAccounts
GO
/****** Object:  StoredProcedure [dbo].[USP_ChartofChildAccounts]    Script Date: 09/25/2009 16:21:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  <Ajit Kumar Singh>  
-- Create date: <Friday, July 25, 2008>  
-- Description: <This procedure is use for fetching the Child Account records against Parent Account Id>  
-- =============================================  
-- exec USP_ChartofChildAccounts @numDomainId=169,@numUserCntId=1,@strSortOn='DESCRIPTION',@strSortDirection='ASCENDING'
CREATE PROCEDURE [dbo].[USP_ChartofChildAccounts]
    @numDomainId AS NUMERIC(9) = 0,
    @numUserCntId AS NUMERIC(9) = 0,
    @strSortOn AS VARCHAR(11) = NULL,
    @strSortDirection AS VARCHAR(10) = NULL
AS 
    BEGIN    
		
		-- GETTING P&L VALUE
		SELECT * INTO #view_journal FROM view_journal WHERE numDomainID=@numDomainId ;

		DECLARE @dtFinYearFrom DATETIME ;
		DECLARE @dtFinYearTo DATETIME ;
		SET @dtFinYearFrom = ( SELECT   dtPeriodFrom
                        FROM     FINANCIALYEAR
                        WHERE    dtPeriodFrom <= GETDATE()
                                AND dtPeriodTo >= GETDATE()
                                AND numDomainId = @numDomainId
                        ) ;
		PRINT @dtFinYearFrom
		SELECT @dtFinYearTo = MAX(datEntry_Date) FROM #view_journal WHERE numDomainID=@numDomainId
		PRINT @dtFinYearTo

		CREATE TABLE #PLSummary (numAccountId numeric(9),vcAccountName varchar(250),
		numParntAcntTypeID numeric(9),vcAccountDescription varchar(250),
		vcAccountCode varchar(50) COLLATE Database_Default,Opening money,Debit money,Credit MONEY,bitIsSubAccount Bit);

		INSERT INTO  #PLSummary
		SELECT COA.numAccountId,vcAccountName,numParntAcntTypeID,vcAccountDescription,vcAccountCode,
		0 AS OPENING,
		ISNULL((SELECT sum(Debit) FROM #view_journal VJ
		WHERE VJ.numDomainId=@numDomainId AND
			VJ.COAvcAccountCode like COA.vcAccountCode + '%' /*VJ.numAccountId=COA.numAccountId*/ AND
			datEntry_Date BETWEEN  @dtFinYearFrom AND @dtFinYearTo ),0) as DEBIT,

		ISNULL((SELECT sum(Credit) FROM #view_journal VJ
		WHERE VJ.numDomainId=@numDomainId AND
			VJ.COAvcAccountCode like COA.vcAccountCode + '%' /*VJ.numAccountId=COA.numAccountId*/ AND
			datEntry_Date BETWEEN  @dtFinYearFrom AND @dtFinYearTo ),0) as CREDIT,
			ISNULL(COA.bitIsSubAccount,0)
		FROM Chart_of_Accounts COA
		WHERE COA.numDomainId=@numDomainId AND COA.bitActive = 1 AND 
			  (COA.vcAccountCode LIKE '0103%' OR
			   COA.vcAccountCode LIKE '0104%' OR
			   COA.vcAccountCode LIKE '0106%')  ;


		CREATE TABLE #PLOutPut (numAccountId numeric(9),vcAccountName varchar(250),
		numParntAcntTypeID numeric(9),vcAccountDescription varchar(250),
		vcAccountCode varchar(50) COLLATE Database_Default,Opening money,Debit money,Credit Money);

		INSERT INTO #PLOutPut
		SELECT ATD.numAccountTypeID,ATD.vcAccountType,ATD.numParentID, '',ATD.vcAccountCode,
		 ISNULL(SUM(Opening),0) as Opening,
		ISNUlL(Sum(Debit),0) as Debit,ISNULL(Sum(Credit),0) as Credit
		FROM 
		 AccountTypeDetail ATD RIGHT OUTER JOIN 
		#PLSummary PL ON
		PL.vcAccountCode LIKE ATD.vcAccountCode + '%'
		AND ATD.numDomainId=@numDomainId AND
		(ATD.vcAccountCode LIKE '0103%' OR
			   ATD.vcAccountCode LIKE '0104%' OR
			   ATD.vcAccountCode LIKE '0106%')
		WHERE 
		PL.bitIsSubAccount=0
		GROUP BY 
		ATD.numAccountTypeID,ATD.vcAccountCode,ATD.vcAccountType,ATD.numParentID;


		DECLARE @CURRENTPL MONEY ;
		DECLARE @PLOPENING MONEY;
		DECLARE @PLCHARTID NUMERIC(8)
		DECLARE @TotalIncome MONEY;
		DECLARE @TotalExpense MONEY;
		DECLARE @TotalCOGS MONEY;
		DECLARE  @CurrentPL_COA money
		SET @CURRENTPL =0;	
		SET @PLOPENING=0;

		SELECT @PLCHARTID=COA.numAccountId FROM Chart_of_Accounts COA WHERE numDomainID=@numDomainId and
		bitProfitLoss=1;

		SELECT @CurrentPL_COA =  ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE numDomainID=@numDomainID
		AND numAccountId=@PLCHARTID AND datEntry_Date between  @dtFinYearFrom AND @dtFinYearTo;

		SELECT  @TotalIncome= ISNULL(sum(Opening),0) + ISNULL(sum(Credit),0) -ISNULL(sum(Debit),0) FROM
		#PLOutPut P WHERE 
		vcAccountCode IN ('0103')

		SELECT  @TotalExpense=ISNULL(sum(Opening),0)+ ISNULL(sum(Credit),0) -ISNULL(sum(Debit),0) FROM
		#PLOutPut P WHERE 
		vcAccountCode IN ('0104')

		SELECT  @TotalCOGS= ISNULL(sum(Opening),0) + ISNULL(sum(Credit),0) -ISNULL(sum(Debit),0) FROM
		#PLOutPut P WHERE 
		vcAccountCode IN ('0106')

		PRINT 'CurrentPL_COA = ' + CAST(@CurrentPL_COA AS VARCHAR(20))
		PRINT 'TotalIncome = ' + CAST(@TotalIncome AS VARCHAR(20))
		PRINT 'TotalExpense = ' + CAST(@TotalExpense AS VARCHAR(20))
		PRINT 'TotalCOGS = ' + CAST(@TotalCOGS AS VARCHAR(20))

		SELECT @CURRENTPL = @CurrentPL_COA + @TotalIncome + @TotalExpense + @TotalCOGS 
		PRINT 'Current Profit/Loss = (Income - expense - cogs)= ' + CAST( @CURRENTPL AS VARCHAR(20))

		SET @CURRENTPL=@CURRENTPL * (-1)

		SELECT @PLOPENING = ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE numDomainID=@numDomainID 
		AND ( vcAccountCode  LIKE '0103%' or  vcAccountCode  LIKE '0104%' or  vcAccountCode  LIKE '0106%')
		AND datEntry_Date <=  DATEADD(MINUTE,0,@dtFinYearFrom);

		SELECT @PLOPENING = ISNULL(@PLOPENING,0) + ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE numDomainID=@numDomainID
		AND numAccountId=@PLCHARTID AND datEntry_Date <=  DATEADD(MINUTE,0,@dtFinYearFrom);

		SET @CURRENTPL=@CURRENTPL * (-1)

		PRINT '@PLOPENING = ' + CAST(@PLOPENING AS VARCHAR(20))
		PRINT '@CURRENTPL = ' + CAST(@CURRENTPL AS VARCHAR(20))
		PRINT '-(@PLOPENING + @CURRENTPL) = ' + CAST((@PLOPENING + @CURRENTPL) AS VARCHAR(20))

        IF ( @strSortOn = 'ID'
             AND @strSortDirection = 'ASCENDING'
           ) 
            BEGIN  

                SELECT  c.numAccountId,
                        [numAccountTypeID],
                        c.[numParntAcntTypeId],
                        c.[vcAccountCode],
                        ISNULL(c.[vcAccountCode], '') + ' ~ '
                        + c.vcAccountName AS vcAccountName,
                        c.vcAccountName AS vcAccountName1,
                        AT.[vcAccountType],
                        CASE WHEN (c.[vcAccountCode] LIKE '0103%' OR c.[vcAccountCode] LIKE '0104%' OR c.[vcAccountCode] LIKE '0106%') THEN ISNULL(t1.[OPENING],0)
							 WHEN c.[vcAccountCode] = '0105010102' THEN -(@PLOPENING + @CURRENTPL)
							 ELSE (SELECT  SUM(COA.numOpeningBal) FROM dbo.Chart_Of_Accounts COA 
								   WHERE COA.numDomainId=@numDomainId 
								   AND (COA.vcAccountCode LIKE c.vcAccountCode OR (COA.vcAccountCode LIKE c.vcAccountCode + '%' AND COA.numParentAccID>0)))
						END AS [numOpeningBal],	 
                        ISNULL(c.numParentAccId,0) AS [numParentAccId],
                        COA.vcAccountName AS [vcParentAccountName],
                        (SELECT MAX(intLevel) FROM dbo.Chart_Of_Accounts) AS [intMaxLevel],
                        ISNULL(c.intLevel,1) AS [intLevel]
						,ISNULL(c.bitActive,0) bitActive
                FROM    Chart_Of_Accounts c
				LEFT OUTER JOIN [AccountTypeDetail] AT ON AT.[numAccountTypeID] = c.[numParntAcntTypeId]
				LEFT OUTER JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = c.numParentAccId
				OUTER APPLY (SELECT   SUM(Debit - Credit) AS OPENING
                                   FROM     #view_journal VJ
                                   WHERE    VJ.numDomainId = @numDomainId
                                            AND VJ.numAccountID = c.numAccountID 
											AND (c.[vcAccountCode] LIKE '0103%' OR c.[vcAccountCode] LIKE '0104%' OR c.[vcAccountCode] LIKE '0106%')
                                            AND datEntry_Date BETWEEN @dtFinYearFrom AND  @dtFinYearTo) AS t1
                WHERE   c.numDomainId = @numDomainId
                --AND c.bitActive = 0
                ORDER BY numAccountID ASC  
  
                SELECT  c.numAccountId,
						[numAccountTypeID],
						c.[numParntAcntTypeId],
						c.[vcAccountCode],
						ISNULL(c.[vcAccountCode], '') + ' ~ ' + c.vcAccountName AS vcAccountName,
						c.vcAccountName AS vcAccountName1,
						AT.[vcAccountType],
						CASE WHEN c.[numParntAcntTypeId] IS NULL THEN NULL
							 ELSE CASE WHEN (c.[vcAccountCode] LIKE '0103%' OR c.[vcAccountCode] LIKE '0104%' OR c.[vcAccountCode] LIKE '0106%') THEN ISNULL(t1.[OPENING],0)
									   WHEN c.[vcAccountCode] = '0105010102' THEN -(@PLOPENING + @CURRENTPL)	
									   ELSE (SELECT  SUM(COA.numOpeningBal) FROM dbo.Chart_Of_Accounts COA 
											 WHERE COA.numDomainId=@numDomainId 
											 AND (COA.vcAccountCode LIKE c.vcAccountCode OR (COA.vcAccountCode LIKE c.vcAccountCode + '%' AND COA.numParentAccID>0))) 
								  END
						END AS numOpeningBal,
						ISNULL(c.numParentAccId,0) AS [numParentAccId],
						COA.vcAccountName AS [vcParentAccountName],
						(SELECT MAX(intLevel) FROM dbo.Chart_Of_Accounts) AS [intMaxLevel],
						ISNULL(c.intLevel,1) AS [intLevel],
						ISNULL(c.IsConnected,0) AS [IsConnected]                        
						,ISNULL(c.numBankDetailID,0) numBankDetailID
						,ISNULL(c.bitActive,0) bitActive
				FROM    Chart_Of_Accounts c
						LEFT OUTER JOIN [AccountTypeDetail] AT ON AT.[numAccountTypeID] = c.[numParntAcntTypeId]
						LEFT OUTER JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = c.numParentAccId
						OUTER APPLY (SELECT   SUM(Debit - Credit) AS OPENING
                                   FROM     #view_journal VJ
                                   WHERE    VJ.numDomainId = @numDomainId
                                            AND VJ.numAccountID = c.numAccountID 
											AND (c.[vcAccountCode] LIKE '0103%' OR c.[vcAccountCode] LIKE '0104%' OR c.[vcAccountCode] LIKE '0106%')
                                            AND datEntry_Date BETWEEN @dtFinYearFrom AND  @dtFinYearTo) AS t1
                WHERE   c.numDomainId = @numDomainID
                --AND c.bitActive = 0
                ORDER BY c.numAccountID ASC
            END  
  
        IF ( @strSortOn = 'DESCRIPTION'
             AND @strSortDirection = 'ASCENDING'
           ) 
            BEGIN  
                
                SELECT  c.numAccountId,
						[numAccountTypeID],
						c.[numParntAcntTypeId],
						c.[vcAccountCode],
						ISNULL(c.[vcAccountCode], '') + ' ~ ' + c.vcAccountName AS vcAccountName,
						c.vcAccountName AS vcAccountName1,
						AT.[vcAccountType],
						CASE WHEN c.[numParntAcntTypeId] IS NULL THEN NULL
							 ELSE CASE WHEN (c.[vcAccountCode] LIKE '0103%' OR c.[vcAccountCode] LIKE '0104%' OR c.[vcAccountCode] LIKE '0106%') THEN ISNULL(t1.[OPENING],0)
									   WHEN c.[vcAccountCode] = '0105010102' THEN -(@PLOPENING + @CURRENTPL)
								       ELSE (SELECT SUM(COA.numOpeningBal) FROM dbo.Chart_Of_Accounts COA 
											 WHERE COA.numDomainId=@numDomainId 
											 AND (COA.vcAccountCode LIKE c.vcAccountCode OR (COA.vcAccountCode LIKE c.vcAccountCode + '%' AND COA.numParentAccID>0))) 
								  END
						END AS numOpeningBal,
						ISNULL(c.numParentAccId,0) AS [numParentAccId],
						COA.vcAccountName AS [vcParentAccountName],
						(SELECT MAX(intLevel) FROM dbo.Chart_Of_Accounts) AS [intMaxLevel],
						ISNULL(c.intLevel,1) AS [intLevel],
						ISNULL(c.IsConnected,0) AS [IsConnected]                      
						,ISNULL(c.numBankDetailID,0) numBankDetailID
						,ISNULL(c.bitActive,0) bitActive
				FROM    Chart_Of_Accounts c
						LEFT OUTER JOIN [AccountTypeDetail] AT ON AT.[numAccountTypeID] = c.[numParntAcntTypeId]
						LEFT OUTER JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = c.numParentAccId
						OUTER APPLY (SELECT   SUM(Debit - Credit) AS OPENING
                                   FROM     #view_journal VJ
                                   WHERE    VJ.numDomainId = @numDomainId
                                            AND VJ.numAccountID = c.numAccountID 
											AND (c.[vcAccountCode] LIKE '0103%' OR c.[vcAccountCode] LIKE '0104%' OR c.[vcAccountCode] LIKE '0106%')
                                            AND datEntry_Date BETWEEN @dtFinYearFrom AND  @dtFinYearTo) AS t1

                WHERE   c.numDomainId = @numDomainId
                    AND c.[numParntAcntTypeId] IS NULL
                    --AND c.bitActive = 0
                ORDER BY AT.vcAccountCode, case IsNumeric(c.vcNumber) when 1 then Replicate(0, 50 - Len(c.vcNumber)) + c.vcNumber else Replicate(9,50) end  ASC
  
                SELECT  c.numAccountId,
						[numAccountTypeID],
						c.[numParntAcntTypeId],
						c.[vcAccountCode],
						ISNULL(c.[vcAccountCode], '') + ' ~ ' + c.vcAccountName AS vcAccountName,
						c.vcAccountName AS vcAccountName1,
						AT.[vcAccountType],
						CASE WHEN c.[numParntAcntTypeId] IS NULL THEN NULL
							 ELSE  CASE WHEN (c.[vcAccountCode] LIKE '0103%' OR c.[vcAccountCode] LIKE '0104%' OR c.[vcAccountCode] LIKE '0106%') THEN ISNULL(t1.[OPENING],0)
									    WHEN c.[vcAccountCode] = '0105010102' THEN -(@PLOPENING + @CURRENTPL)
										ELSE (SELECT  SUM(COA.numOpeningBal) FROM dbo.Chart_Of_Accounts COA WHERE COA.numDomainId=@numDomainId AND (COA.vcAccountCode LIKE c.vcAccountCode OR (COA.vcAccountCode LIKE c.vcAccountCode + '%' AND COA.numParentAccID>0))) END
						END AS numOpeningBal,
						ISNULL(c.numParentAccId,0) AS [numParentAccId],
						COA.vcAccountName AS [vcParentAccountName],
						(SELECT MAX(intLevel) FROM dbo.Chart_Of_Accounts) AS [intMaxLevel],
						ISNULL(c.intLevel,1) AS [intLevel],
						ISNULL(c.IsConnected,0) AS [IsConnected]
						,ISNULL(c.numBankDetailID,0) numBankDetailID
						,ISNULL(c.bitActive,0) bitActive
				FROM    Chart_Of_Accounts c
						LEFT OUTER JOIN [AccountTypeDetail] AT ON AT.[numAccountTypeID] = c.[numParntAcntTypeId]
						LEFT OUTER JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = c.numParentAccId
						OUTER APPLY (SELECT   SUM(Debit - Credit) AS OPENING
                                   FROM     #view_journal VJ
                                   WHERE    VJ.numDomainId = @numDomainId
                                            AND VJ.numAccountID = c.numAccountID 
											AND (c.[vcAccountCode] LIKE '0103%' OR c.[vcAccountCode] LIKE '0104%' OR c.[vcAccountCode] LIKE '0106%')
                                            AND datEntry_Date BETWEEN @dtFinYearFrom AND  @dtFinYearTo) AS t1
                WHERE   c.numDomainId = @numDomainID
                        AND c.[numParntAcntTypeId] IS NOT NULL
                        --AND c.bitActive = 0
                ORDER BY AT.vcAccountCode, case IsNumeric(c.vcNumber) when 1 then Replicate(0, 50 - Len(c.vcNumber)) + c.vcNumber else Replicate(9,50) end  ASC
                
            END  
    END
	
/****** Object:  StoredProcedure [dbo].[USP_CheckCanbeReOpenOppertunity]    Script Date: 07/26/2008 16:20:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CheckCanbeReOpenOppertunity')
DROP PROCEDURE USP_CheckCanbeReOpenOppertunity
GO
CREATE PROCEDURE [dbo].[USP_CheckCanbeReOpenOppertunity]              
@intOpportunityId as numeric(9),
@bitCheck AS BIT=0
as              
              
DECLARE @OppType AS VARCHAR(2)   
DECLARE @Sel AS int   
DECLARE @itemcode AS NUMERIC     
DECLARE @numWareHouseItemID AS NUMERIC                                 
DECLARE @numUnits AS NUMERIC                                            
DECLARE @numQtyShipped AS NUMERIC
DECLARE @onHand AS NUMERIC                                                                                                                          
DECLARE @numoppitemtCode AS NUMERIC(9)
DECLARE @Kit AS BIT                   
DECLARE @LotSerial AS BIT                   
DECLARE @numQtyReceived AS NUMERIC(9)

set @Sel=0          
select @OppType=tintOppType from OpportunityMaster where numOppId=@intOpportunityId
 
IF @OppType=2
BEGIN
 select top 1 @numoppitemtCode=numoppitemtCode,@itemcode=OI.numItemCode,@numUnits=ISNULL(numUnitHour,0),@numQtyShipped=ISNULL(numQtyShipped,0),@numQtyReceived=ISNULL(numUnitHourReceived,0),
 @numWareHouseItemID=numWarehouseItmsID,@Kit= (case when bitKitParent=1 and bitAssembly=1 then 0 when bitKitParent=1 then 1 else 0 end),
 @LotSerial=(CASE WHEN I.bitLotNo=1 OR I.bitSerialized=1 THEN 1 ELSE 0 END) from OpportunityItems OI                                            
 join Item I                                            
 on OI.numItemCode=I.numItemCode and numOppId=@intOpportunityId and (bitDropShip=0 or bitDropShip is null)                                        
 where charitemtype='P'  order by OI.numoppitemtCode

while @numoppitemtCode>0                               
 begin  
  select @onHand = ISNULL(numOnHand, 0) from WareHouseItems where numWareHouseItemID=@numWareHouseItemID                                              
   	 IF @onHand < @numUnits --- @numQtyReceived) 
     begin  
		if @Sel=0 set @Sel=1
     end  
     
	  IF @LotSerial=1
	  BEGIN
	  	IF(SELECT COUNT(*) FROM WareHouseItmsDTL WHID JOIN OppWarehouseSerializedItem opp 
  ON WHID.numWareHouseItmsDTLID=opp.numWareHouseItmsDTLID where opp.numOppID=@intOpportunityId AND opp.numOppItemID=@numoppitemtCode AND WHID.tintStatus=1)>0
		BEGIN
			if @Sel=0 set @Sel=1
		END
	  END
	
  select top 1 @numoppitemtCode=numoppitemtCode,@itemcode=OI.numItemCode,@numUnits=ISNULL(numUnitHour,0),@numQtyShipped=ISNULL(numQtyShipped,0),@numQtyReceived=ISNULL(numUnitHourReceived,0),
  @numWareHouseItemID=numWarehouseItmsID,@Kit= (case when bitKitParent=1 and bitAssembly=1 then 0 when bitKitParent=1 then 1 else 0 end),
  @LotSerial=(CASE WHEN I.bitLotNo=1 OR I.bitSerialized=1 THEN 1 ELSE 0 END) from OpportunityItems OI                                            
  join Item I                                            
  on OI.numItemCode=I.numItemCode and numOppId=@intOpportunityId                                          
  where charitemtype='P' and OI.numoppitemtCode>@numoppitemtCode and (bitDropShip=0 or bitDropShip is null) order by OI.numoppitemtCode                                            
    
  if @@rowcount=0 set @numoppitemtCode=0    
 end  
END      


 IF EXISTS ( SELECT  *
			FROM    dbo.ReturnHeader
			WHERE   numOppId = @intOpportunityId)         
	BEGIN
		SET @Sel = -1 * @OppType;
	END	
                                    
  
select @Sel
GO
--Created by anoop jayaraj                                                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_checkuseratlogin')
DROP PROCEDURE usp_checkuseratlogin
GO
CREATE PROCEDURE [dbo].[USP_CheckUserAtLogin]                                                              
(                                                                                    
@UserName as varchar(100)='',                                    
@vcPassword as varchar(100)=''                                                                           
)                                                              
as                                                              

DECLARE @listIds VARCHAR(MAX)
SELECT 
	@listIds = COALESCE(@listIds+',' ,'') + CAST(UPA.numUserID AS VARCHAR(100)) 
FROM 
	UserMaster U                              
Join 
	Domain D                              
on 
	D.numDomainID=U.numDomainID
Join  
	Subscribers S                            
on 
	S.numTargetDomainID=D.numDomainID
join
	UnitPriceApprover UPA
ON
	D.numDomainID = UPA.numDomainID
WHERE 
	vcEmailID=@UserName and vcPassword=@vcPassword

/*check subscription validity */
IF EXISTS( SELECT *
FROM UserMaster U                              
 Join Domain D                              
 on D.numDomainID=U.numDomainID
 Join  Subscribers S                            
 on S.numTargetDomainID=D.numDomainID
 WHERE vcEmailID=@UserName and vcPassword=@vcPassword and bitActive=1 AND getutcdate() > dtSubEndDate)
BEGIN
	RAISERROR('SUB_RENEW',16,1)
	RETURN;
END



                                                                
 SELECT top 1 U.numUserID,numUserDetailId,isnull(vcEmailID,'') vcEmailID,isnull(numGroupID,0) numGroupID,bitActivateFlag,                              
 isnull(vcMailNickName,'') vcMailNickName,isnull(txtSignature,'') txtSignature,isnull(U.numDomainID,0) numDomainID,                              
 isnull(Div.numDivisionID,0) numDivisionID,isnull(vcCompanyName,'') vcCompanyName,isnull(E.bitExchangeIntegration,0) bitExchangeIntegration,                              
 isnull(E.bitAccessExchange,0) bitAccessExchange,isnull(E.vcExchPath,'') vcExchPath,isnull(E.vcExchDomain,'') vcExchDomain,                              
 isnull(D.vcExchUserName,'') vcExchUserName ,isnull(vcFirstname,'')+' '+isnull(vcLastName,'') as ContactName,                              
 Case when E.bitAccessExchange=0 then  E.vcExchPassword when E.bitAccessExchange=1 then D.vcExchPassword end as vcExchPassword,                          
 tintCustomPagingRows,                         
 vcDateFormat,                         
 numDefCountry,                     
 tintComposeWindow,
 dateadd(day,-sintStartDate,getutcdate()) as StartDate,                        
 dateadd(day,sintNoofDaysInterval,dateadd(day,-sintStartDate,getutcdate())) as EndDate,                        
 tintAssignToCriteria,                         
 bitIntmedPage,                         
 tintFiscalStartMonth,numAdminID,isnull(A.numTeam,0) numTeam ,                
 isnull(D.vcCurrency,'') as vcCurrency, 
 isnull(D.numCurrencyID,0) as numCurrencyID, 
 isnull(D.bitMultiCurrency,0) as bitMultiCurrency,              
 bitTrial,isnull(tintChrForComSearch,0) as tintChrForComSearch,  
isnull(tintChrForItemSearch,0) as tintChrForItemSearch,              
 case when bitSMTPServer= 1 then vcSMTPServer else '' end as vcSMTPServer  ,          
 case when bitSMTPServer= 1 then  isnull(numSMTPPort,0)  else 0 end as numSMTPPort      
,isnull(bitSMTPAuth,0) bitSMTPAuth    
,isnull([vcSmtpPassword],'') vcSmtpPassword    
,isnull(bitSMTPSSL,0) bitSMTPSSL   ,isnull(bitSMTPServer,0) bitSMTPServer,       
isnull(IM.bitImap,0) bitImapIntegration, isnull(charUnitSystem,'E')   UnitSystem, datediff(day,getutcdate(),dtSubEndDate) as NoOfDaysLeft,
ISNULL(bitCreateInvoice,0) bitCreateInvoice,
ISNULL([numDefaultSalesBizDocId],0) numDefaultSalesBizDocId,
ISNULL([numDefaultPurchaseBizDocId],0) numDefaultPurchaseBizDocId,
vcDomainName,
S.numSubscriberID,
D.[tintBaseTax],
ISNULL(D.[numShipCompany],0) numShipCompany,
ISNULL(D.[bitEmbeddedCost],0) bitEmbeddedCost,
(Select isnull(numAuthoritativeSales,0) From AuthoritativeBizDocs Where numDomainId=D.numDomainID) numAuthoritativeSales,
(Select isnull(numAuthoritativePurchase,0) From AuthoritativeBizDocs Where numDomainId=D.numDomainID) numAuthoritativePurchase,
ISNULL(D.[numDefaultSalesOppBizDocId],0) numDefaultSalesOppBizDocId,
ISNULL(tintDecimalPoints,2) tintDecimalPoints,
ISNULL(D.tintBillToForPO,0) tintBillToForPO, 
ISNULL(D.tintShipToForPO,0) tintShipToForPO,
ISNULL(D.tintOrganizationSearchCriteria,0) tintOrganizationSearchCriteria,
ISNULL(D.tintLogin,0) tintLogin,
ISNULL(bitDocumentRepositary,0) bitDocumentRepositary,ISNULL(D.tintComAppliesTo,0) tintComAppliesTo,
ISNULL(D.vcPortalName,'') vcPortalName,
(SELECT COUNT(*) FROM dbo.Subscribers WHERE getutcdate() between dtSubStartDate and dtSubEndDate and bitActive=1),
ISNULL(D.bitGtoBContact,0) bitGtoBContact,
ISNULL(D.bitBtoGContact,0) bitBtoGContact,
ISNULL(D.bitGtoBCalendar,0) bitGtoBCalendar,
ISNULL(D.bitBtoGCalendar,0) bitBtoGCalendar,
ISNULL(bitExpenseNonInventoryItem,0) bitExpenseNonInventoryItem,
ISNULL(D.bitInlineEdit,0) bitInlineEdit,
ISNULL(U.numDefaultClass,0) numDefaultClass,
ISNULL(D.bitRemoveVendorPOValidation,0) bitRemoveVendorPOValidation,
ISNULL(D.bitTrakDirtyForm,1) bitTrakDirtyForm,
isnull(D.tintBaseTaxOnArea,0) tintBaseTaxOnArea,
ISNULL(D.bitAmountPastDue,0) bitAmountPastDue,
CONVERT(DECIMAL(18,2),ISNULL(D.monAmountPastDue,0)) monAmountPastDue,
ISNULL(D.bitSearchOrderCustomerHistory,0) bitSearchOrderCustomerHistory,
isnull(U.tintTabEvent,0) as tintTabEvent,
isnull(D.bitRentalItem,0) as bitRentalItem,
isnull(D.numRentalItemClass,0) as numRentalItemClass,
isnull(D.numRentalHourlyUOM,0) as numRentalHourlyUOM,
isnull(D.numRentalDailyUOM,0) as numRentalDailyUOM,
isnull(D.tintRentalPriceBasedOn,0) as tintRentalPriceBasedOn,
isnull(D.tintCalendarTimeFormat,24) as tintCalendarTimeFormat,
isnull(D.tintDefaultClassType,0) as tintDefaultClassType,
ISNULL(U.numDefaultWarehouse,0) numDefaultWarehouse,
ISNULL(D.tintPriceBookDiscount,0) tintPriceBookDiscount,
ISNULL(D.bitAutoSerialNoAssign,0) bitAutoSerialNoAssign,
ISNULL(IsEnableClassTracking,0) AS [IsEnableClassTracking],
ISNULL(IsEnableProjectTracking,0) AS [IsEnableProjectTracking],
ISNULL(IsEnableCampaignTracking,0) AS [IsEnableCampaignTracking],
ISNULL(IsEnableResourceScheduling,0) AS [IsEnableResourceScheduling],
ISNULL(D.numSOBizDocStatus,0) AS numSOBizDocStatus,
ISNULL(D.bitAutolinkUnappliedPayment,0) AS bitAutolinkUnappliedPayment,
ISNULL(numShippingServiceItemID,0) AS [numShippingServiceItemID],
ISNULL(D.numDiscountServiceItemID,0) AS numDiscountServiceItemID,
ISNULL(D.tintCommissionType,1) AS tintCommissionType,
ISNULL(D.bitDiscountOnUnitPrice,0) AS bitDiscountOnUnitPrice,
ISNULL(D.intShippingImageWidth,0) AS intShippingImageWidth ,
ISNULL(D.intShippingImageHeight,0) AS intShippingImageHeight,
ISNULL(D.numTotalInsuredValue,0) AS numTotalInsuredValue,
ISNULL(D.numTotalCustomsValue,0) AS numTotalCustomsValue,
ISNULL(D.IsEnableUserLevelClassTracking,0) AS IsEnableUserLevelClassTracking,
ISNULL(D.bitUseBizdocAmount,1) AS [bitUseBizdocAmount],
ISNULL(D.bitDefaultRateType,1) AS [bitDefaultRateType],
ISNULL(D.bitIncludeTaxAndShippingInCommission,1) AS [bitIncludeTaxAndShippingInCommission],
ISNULL(D.[bitLandedCost],0) AS bitLandedCost,
ISNULL(D.vcLanedCostDefault,'') AS vcLanedCostDefault,
ISNULL(D.bitMinUnitPriceRule,0) AS bitMinUnitPriceRule,
ISNULL(D.numAbovePercent,0) AS numAbovePercent,
ISNULL(D.numAbovePriceField,0) AS numAbovePriceField,
ISNULL(D.numBelowPercent,0) AS numBelowPercent,
ISNULL(D.numBelowPriceField,0) AS numBelowPriceField,
ISNULL(D.numOrderStatusBeforeApproval,0) AS numOrderStatusBeforeApproval,
ISNULL(D.numOrderStatusAfterApproval,0) AS numOrderStatusAfterApproval,
ISNULL(@listIds,'') AS vcUnitPriceApprover
/*,ISNULL(D.bitAllowPPVariance,0) AS bitAllowPPVariance*/
 FROM UserMaster U                              
 left join ExchangeUserDetails E                              
 on E.numUserID=U.numUserID                              
left join ImapUserDetails IM                              
 on IM.numUserCntID=U.numUserDetailID          
 Join Domain D                              
 on D.numDomainID=U.numDomainID                                           
 left join DivisionMaster Div                                            
 on D.numDivisionID=Div.numDivisionID                               
 left join CompanyInfo C                              
 on C.numCompanyID=Div.numDivisionID                               
 left join AdditionalContactsInformation A                              
 on  A.numContactID=U.numUserDetailId                            
 Join  Subscribers S                            
 on S.numTargetDomainID=D.numDomainID                             
 WHERE vcEmailID=@UserName and vcPassword=@vcPassword and bitActive IN (1,2) AND getutcdate() between dtSubStartDate and dtSubEndDate
             
select numTerritoryId from UserTerritory UT                              
join UserMaster U                              
on numUserDetailId =UT.numUserCntID                                         
where vcEmailID=@UserName and vcPassword=@vcPassword
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_CopyStagePercentageDetails')
DROP PROCEDURE usp_CopyStagePercentageDetails
GO
CREATE PROCEDURE [dbo].[usp_CopyStagePercentageDetails]
    @numDomainid NUMERIC = 0,
    @numProjectID NUMERIC = 0 ,
    @slp_id NUMERIC = 0,
    @numContactId NUMERIC = 0 ,
    @numOppID NUMERIC = 0 
AS 

if (select count(*) from StagePercentageDetails where numOppID=@numOppID and numProjectID=@numProjectID and numDomainid=@numDomainid)=0
BEGIN

declare @id as int
SEt @id=IDENT_CURRENT('[StagePercentageDetails]');

 
;WITH tNewRow AS
(SELECT [numStageDetailsId],[numParentStageID],[numStagePercentageId],[tintConfiguration],[vcStageName]
      ,[numDomainId],[slp_id],[numAssignTo],[vcMileStoneName],[tintPercentage],[bitClose],[intDueDays],
	   [vcDescription],(@id + ROW_NUMBER() OVER (ORDER BY numStageDetailsId)) AS numNewParentStageID
FROM [StagePercentageDetails] where slp_id=@slp_id and ISNULL(numProjectid,0)=0 and ISNULL(numOppid,0)=0 and numDomainid=@numDomainid) 


SELECT *
INTO #TempTable
FROM tNewRow order by numStagePercentageId,numStageDetailsId

--WITH tNewRow AS
--(SELECT [numStageDetailsId],[numParentStageID],[numStagePercentageId],[tintConfiguration],[vcStageName]
--      ,[numDomainId],[slp_id],[numAssignTo],[vcMileStoneName],[tintPercentage],[bitClose],[intDueDays],
--	   [vcDescription],(@id + ROW_NUMBER() OVER (ORDER BY numStageDetailsId)) AS numNewParentStageID
--FROM [StagePercentageDetails] where slp_id=@slp_id and ISNULL(numProjectid,0)=0 and ISNULL(numOppid,0)=0 and numDomainid=@numDomainid) 

;WITH tParent as(SELECT t1.numNewParentStageID numStageDetailsId,isnull(t2.numNewParentStageID,0) numParentStageID,t1.[numStagePercentageId],
	   t1.[tintConfiguration],t1.[vcStageName],t1.[numDomainId],t1.[slp_id],t1.[numAssignTo],t1.[vcMileStoneName],t1.[tintPercentage],t1.[bitClose],t1.[intDueDays],
	   t1.[vcDescription] FROM #TempTable t1 LEFT JOIN #TempTable t2 ON t1.numParentStageID = t2.numStageDetailsId)

,tStageOrder AS (
    SELECT 0 AS Lvl, [numStageDetailsId],[numParentStageID],[numStagePercentageId],[tintConfiguration],[vcStageName]
      ,[numDomainId],[slp_id],[numAssignTo],[vcMileStoneName],[tintPercentage],[bitClose],[intDueDays],[vcDescription],
	CAST(ROW_NUMBER() OVER (ORDER BY numStagePercentageId,numStageDetailsId) AS VARCHAR(MAX)) as stage
    FROM tParent where [numParentStageID]=0
    UNION ALL
    SELECT p.lvl + 1, s.[numStageDetailsId],s.[numParentStageID],s.[numStagePercentageId],s.[tintConfiguration],s.[vcStageName]
      ,s.[numDomainId],s.[slp_id],s.[numAssignTo],s.[vcMileStoneName],s.[tintPercentage],s.[bitClose],s.[intDueDays],s.[vcDescription],
	p.stage + '.' + CAST(ROW_NUMBER() OVER (ORDER BY s.numStagePercentageId,s.numStageDetailsId) AS VARCHAR(MAX))
    FROM tParent s INNER JOIN tStageOrder p ON p.numStageDetailsId = s.numParentStageID)

----, tRowNumber as (SELECT *,ROW_NUMBER() OVER (ORDER BY stage) ROWNUMBER FROM tStageOrder)

--, tRowNumber as (SELECT *,ROW_NUMBER() OVER (ORDER BY cast(cast(substring(stage,1,case when(charindex('.',stage)>0) then charindex('.',stage) else len(stage) end) AS varchar(10))
-- +''+ cast(replace(substring(stage,charindex('.',stage),len(stage)),'.','') as varchar(10)) as numeric(18,10))) AS ROWNUMBER FROM tStageOrder)
--
--,tFinal as (select top 1 *,getdate() as [dtStartDate],getdate() + [intDueDays] - 1 as [dtEndDate] from tRowNumber
--
--union all select f.*,CASE WHEN f.numParentStageID=0 THEN getdate() ELSE p.[dtEndDate]+1 end as [dtStartDate],
--                    CASE WHEN f.numParentStageID=0 THEN getdate() + f.[intDueDays] - 1 else p.[dtEndDate] + f.[intDueDays] end  as [dtEndDate]
--		from tRowNumber f INNER JOIN tFinal p ON p.ROWNUMBER = (f.ROWNUMBER-1))

INSERT INTO [StagePercentageDetails] (numParentStageID,[numStagePercentageId],[tintConfiguration],[vcStageName]
      ,[numDomainId],[slp_id],[numAssignTo],[vcMileStoneName],[tintPercentage],[bitClose],
		[intDueDays],[numOppID],[vcDescription],[numProjectID],[numCreatedBy],[bintCreatedDate],[numModifiedBy],
		[bintModifiedDate],[dtStartDate],[dtEndDate],bitFromTemplate)
select [numParentStageID],[numStagePercentageId],[tintConfiguration],[vcStageName]
      ,[numDomainId],[slp_id],[numAssignTo],[vcMileStoneName],[tintPercentage],[bitClose],[intDueDays],
		@numOppID,[vcDescription],@numProjectID,@numContactId,getdate(),@numContactId,getdate(),getdate() as [dtStartDate],getdate() + [intDueDays] - CASE intDueDays WHEN 0 THEN 0 ELSE 1 END  as [dtEndDate],1
 from tStageOrder order by numStageDetailsId


;WITH tDependency as(SELECT [numStageDependancyID]
      ,SD.[numStageDetailID]
      ,SD.[numDependantOnID],S.numNewParentStageID as StageDetailID,D.numNewParentStageID as DependantOnID
  FROM [StageDependency] SD join #TempTable S on SD.numStageDetailID=S.numStageDetailsId
                            join #TempTable D on SD.numDependantOnID=D.numStageDetailsId)


INSERT INTO [StageDependency]([numStageDetailID],[numDependantOnID])
select StageDetailID,DependantOnID from tDependency

drop table #TempTable


EXEC usp_AssignProjectStageDate @numDomainid,@numProjectID,@numOppID


/*Added by chintan, Bug ID*/
IF @numOppID >0 AND @slp_id>0
BEGIN
	UPDATE dbo.OpportunityMaster SET numBusinessProcessID = @slp_id WHERE numOppId=@numOppID
END

END
--declare @id as int
--SEt @id=IDENT_CURRENT('[StagePercentageDetails]');
--
--WITH t AS
--(SELECT [numStageDetailsId],[numParentStageID],[numStagePercentageId],[tintConfiguration],[vcStageName]
--      ,[numDomainId] ,[numCreatedBy],getdate() [bintCreatedDate],[numModifiedBy],getdate() [bintModifiedDate]
--      ,[slp_id],[numAssignTo],[vcMileStoneName],[tintPercentage],[bitClose],[dtStartDate]
--      ,[dtEndDate],[intDueDays],[numProjectID],[numOppID],[vcDescription],
--(@id
--+ ROW_NUMBER() OVER (ORDER BY numStageDetailsId))
--AS numNewParentStageID
--FROM [StagePercentageDetails] where slp_id=@slp_id and ISNULL(numProjectid,0)=0 and numDomainid=@numDomainid)
--
--INSERT INTO [StagePercentageDetails] (numParentStageID,[numStagePercentageId],[tintConfiguration],[vcStageName]
--      ,[numDomainId] ,[numCreatedBy],[bintCreatedDate],[numModifiedBy],[bintModifiedDate]
--      ,[slp_id],[numAssignTo],[vcMileStoneName],[tintPercentage],[bitClose],[dtStartDate]
--      ,[dtEndDate],[intDueDays],[numProjectID],[numOppID],[vcDescription])
--SELECT isnull(t2.numNewParentStageID,0),t1.numStagePercentageId,t1.tintConfiguration,t1.vcStageName
--      ,t1.numDomainId ,t1.numCreatedBy,t1.bintCreatedDate,t1.numModifiedBy,t1.bintModifiedDate
--      ,t1.slp_id,t1.numAssignTo,t1.vcMileStoneName,t1.tintPercentage,t1.bitClose,t1.dtStartDate
--      ,t1.dtEndDate,t1.intDueDays,@numProjectID,t1.numOppID,t1.vcDescription
--FROM t t1
--LEFT JOIN t t2
--ON t1.numParentStageID = t2.numStageDetailsId;

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_createbizdocs')
DROP PROCEDURE usp_createbizdocs
GO
CREATE PROCEDURE [dbo].[USP_CreateBizDocs]                        
(                                              
 @numOppId as numeric(9)=null,                        
 @numBizDocId as numeric(9)=null,                        
 @numUserCntID as numeric(9)=null,                        
 @numOppBizDocsId as numeric(9)=NULL OUTPUT,
 @vcComments as varchar(1000),
 @bitPartialFulfillment as bit,
 @strBizDocItems as text='' ,
 --@bitDiscountType as bit,
 --@fltDiscount  as float,
 --@monShipCost as money,
 @numShipVia as numeric(9),
 @vcTrackingURL as varchar(1000),
 --@bitBillingTerms as bit,
 --@intBillingDays as integer,
 @dtFromDate AS DATETIME,
 --@bitInterestType as bit,
 --@fltInterest as float,
-- @numShipDoc as numeric(9),
 @numBizDocStatus as numeric(9) = 0,
 @bitRecurringBizDoc AS BIT = NULL,
 @numSequenceId AS VARCHAR(50) = '',
 --@tintTaxOperator AS tinyint,
 @tintDeferred as tinyint=0,
 @monCreditAmount as money= 0 OUTPUT,
 @ClientTimeZoneOffset as int=0,
 @monDealAmount as money= 0 OUTPUT,
 @bitRentalBizDoc as bit=0,
 @numBizDocTempID as numeric(9)=0,
 @vcTrackingNo AS VARCHAR(500),
 --@vcShippingMethod AS VARCHAR(100),
 @vcRefOrderNo AS VARCHAR(100),
 --@dtDeliveryDate AS DATETIME,
 @OMP_SalesTaxPercent AS FLOAT = 0, --(joseph) Added to Get Online Marketplace Sales Tax Percentage
 @numFromOppBizDocsId AS NUMERIC(9)=0,
 @bitTakeSequenceId AS BIT=0,
 @bitAllItems AS BIT=0,
 @bitNotValidateBizDocFulfillment AS BIT=0,
 @fltExchangeRateBizDoc AS FLOAT=0
)                        
as 

BEGIN TRY

DECLARE @hDocItem as INTEGER
declare @numDivisionID as numeric(9)
DECLARE @numDomainID NUMERIC(9)
DECLARE @tintOppType AS TINYINT	
DECLARE @numFulfillmentOrderBizDocId NUMERIC(9);SET @numFulfillmentOrderBizDocId=296 
	    
IF ISNULL(@fltExchangeRateBizDoc,0)=0
	SELECT @fltExchangeRateBizDoc=fltExchangeRate FROM OpportunityMaster where numOppID=@numOppId
	
select @numDomainID=numDomainID,@numDivisionID=numDivisionID,@tintOppType=tintOppType from OpportunityMaster where numOppID=@numOppId

IF @numBizDocTempID=0
BEGIN
	select @numBizDocTempID=numBizDocTempID from BizDocTemplate where numBizDocId=@numBizDocId 
	and numDomainID=@numDomainID and numOpptype=@tintOppType 
	and tintTemplateType=0 and isnull(bitDefault,0)=1 and isnull(bitEnabled,0)=1
END

if @numOppBizDocsId=0
BEGIN
IF (
	(@bitPartialFulfillment = 1 AND NOT EXISTS (SELECT * FROM   OpportunityBizDocs WHERE  numOppID = @numOppId AND numBizDocId = @numBizDocId AND bitPartialFulfilment = 0))
     OR (NOT EXISTS (SELECT * FROM   OpportunityBizDocs WHERE  numOppID = @numOppId AND numBizDocId = @numBizDocId AND bitPartialFulfilment = 0)
         AND NOT EXISTS (SELECT * FROM   OpportunityBizDocs WHERE  numOppID = @numOppId AND numBizDocId = @numBizDocId AND bitPartialFulfilment = 1))
     OR (@bitRecurringBizDoc = 1) -- Added new flag to add unit qty in case of recurring bizdoc
     OR (@numFromOppBizDocsId >0)
    )
begin                      
		
		
		--DECLARE  @vcBizDocID  AS VARCHAR(100)
        --DECLARE  @numBizMax  AS NUMERIC(9)
        DECLARE @tintShipped AS TINYINT	
        DECLARE @dtShipped AS DATETIME
        DECLARE @bitAuthBizdoc AS BIT
        
        
		select @tintShipped=ISNULL(tintShipped,0),@numDomainID=numDomainID from OpportunityMaster where numOppId=@numOppId                        
		
		IF @tintOppType = 1 AND @bitNotValidateBizDocFulfillment=0
			EXEC USP_ValidateBizDocFulfillment @numDomainID,@numOppId,@numFromOppBizDocsId,@numBizDocId
		
--		Select @numBizMax=count(*) from  OpportunityBizDocs                        
--		IF @numBizMax > 0
--          BEGIN
--				SELECT @numBizMax = MAX(numOppBizDocsId) FROM   OpportunityBizDocs
--          END
--		SET @numBizMax = @numBizMax
--        SET @vcBizDocID = @vcBizDocID + '-BD-' + CONVERT(VARCHAR(10),@numBizMax)
        IF @tintShipped =1 
			SET @dtShipped = GETUTCDATE();
		ELSE
			SET @dtShipped = null;
		IF @tintOppType = 1 AND (SELECT numAuthoritativeSales FROM [AuthoritativeBizDocs] WHERE numDomainID = @numDomainID) = @numBizDocId
			SET @bitAuthBizdoc = 1
		ELSE if @tintOppType = 2 AND (SELECT numAuthoritativePurchase FROM [AuthoritativeBizDocs] WHERE numDomainID = @numDomainID) = @numBizDocId
			SET @bitAuthBizdoc = 1
		ELSE 
			SET @bitAuthBizdoc = 0
		
		Declare @dtCreatedDate as datetime;SET @dtCreatedDate=Getutcdate()
		IF @tintDeferred=1
		BEGIn
			SET @dtCreatedDate=DateAdd(minute, @ClientTimeZoneOffset,@dtFromDate)
		END
	
	
		IF @bitTakeSequenceId=1
		BEGIN
			--Sequence #
			CREATE TABLE #tempSequence (numSequenceId bigint )
			INSERT INTO #tempSequence EXEC [dbo].[USP_GetScalerValue] @numDomainID ,33,@numBizDocId,0
			SELECT @numSequenceId =ISNULL(numSequenceId,0) FROM #tempSequence
			DROP TABLE #tempSequence
			
			--BizDoc Template ID
			CREATE TABLE #tempBizDocTempID (numBizDocTempID NUMERIC(9) )
			INSERT INTO #tempBizDocTempID EXEC [dbo].[USP_GetBizDocTemplateList] @numDomainID,@numBizDocId,1,1
			SELECT @numBizDocTempID =ISNULL(numBizDocTempID,0) FROM #tempBizDocTempID
			DROP TABLE #tempBizDocTempID
		END	
			
			
		IF @numFromOppBizDocsId>0 --If Created from Sales Fulfillment Workflow
		BEGIN
			INSERT INTO OpportunityBizDocs
                   (numOppId,numBizDocId,numCreatedBy,dtCreatedDate,numModifiedBy,dtModifiedDate,vcComments,bitPartialFulfilment,
                    monShipCost,numShipVia,vcTrackingURL,dtFromDate,numBizDocStatus,[dtShippedDate],
                    [bitAuthoritativeBizDocs],tintDeferred,bitRentalBizDoc,numBizDocTempID,/*vcTrackingNo,vcShippingMethod,dtDeliveryDate,*/vcRefOrderNo,numSequenceId,numBizDocStatusOLD,bitAutoCreated,[numMasterBizdocSequenceID])
            select @numOppId,@numBizDocId,@numUserCntID,Getutcdate(),@numUserCntID,Getutcdate(),@vcComments,@bitPartialFulfillment,
                    monShipCost,numShipVia,vcTrackingURL,dtFromDate,0,[dtShippedDate],
                    @bitAuthBizdoc,tintDeferred,bitRentalBizDoc,@numBizDocTempID,/*vcTrackingNo,vcShippingMethod,dtDeliveryDate,*/vcRefOrderNo,@numSequenceId,0,1,@numSequenceId
		    from OpportunityBizDocs OBD where  numOppId=@numOppId AND OBD.numOppBizDocsId=@numFromOppBizDocsId
		             
		END
		ELSE
		BEGIN
			INSERT INTO OpportunityBizDocs
                   (numOppId,numBizDocId,
                    --vcBizDocID,
                    numCreatedBy,
                    dtCreatedDate,
                    numModifiedBy,
                    dtModifiedDate,
                    vcComments,
                    bitPartialFulfilment,
                    --bitDiscountType,
                    --fltDiscount,
                    --monShipCost,
                    numShipVia,
                    vcTrackingURL,
                    --bitBillingTerms,
                    --intBillingDays,
                    dtFromDate,
                    --bitInterestType,
                    --fltInterest,
--                    numShipDoc,
                    numBizDocStatus,
                    --vcBizDocName,
                    [dtShippedDate],
                    [bitAuthoritativeBizDocs],tintDeferred,bitRentalBizDoc,numBizDocTempID,vcTrackingNo,
                    --vcShippingMethod,dtDeliveryDate,
                    vcRefOrderNo,numSequenceId,numBizDocStatusOLD,fltExchangeRateBizDoc,bitAutoCreated,[numMasterBizdocSequenceID])
        VALUES     (@numOppId,
                    @numBizDocId,
                    --@vcBizDocID,
                    @numUserCntID,
                    @dtCreatedDate,
                    @numUserCntID,
                    Getutcdate(),
                    @vcComments,
                    @bitPartialFulfillment,
                    --@bitDiscountType,
                    --@fltDiscount,
                    --@monShipCost,
                    @numShipVia,
                    @vcTrackingURL,
                    --@bitBillingTerms,
                    --@intBillingDays,
                    @dtFromDate,
                    --@bitInterestType,
                    --@fltInterest,
--                    @numShipDoc,
                    @numBizDocStatus,--@numBizDocStatus,--@numBizDocStatus,
                    --@vcBizDocName,
                    @dtShipped,
                    ISNULL(@bitAuthBizdoc,0),@tintDeferred,@bitRentalBizDoc,@numBizDocTempID,@vcTrackingNo,
                    --@vcShippingMethod,@dtDeliveryDate,
                    @vcRefOrderNo,@numSequenceId,0,@fltExchangeRateBizDoc,0,@numSequenceId)
        END
        
		SET @numOppBizDocsId = @@IDENTITY
		
	--Added By:Sachin Sadhu||Date:27thAug2014
	--Purpose :MAke entry in WFA queue 
IF EXISTS (SELECT 1 FROM dbo.OpportunityMaster WHERE numOppId = @numOppID AND tintOppType=1 AND isnull(tintOppStatus,0)=1 )
	BEGIN
		EXEC 
		USP_ManageOpportunityAutomationQueue			
		@numOppQueueID = 0, -- numeric(18, 0)							
		@numDomainID = @numDomainID, -- numeric(18, 0)							
		@numOppId = @numOppID, -- numeric(18, 0)							
		@numOppBizDocsId = @@IDENTITY, -- numeric(18, 0)							
		@numOrderStatus = 0, -- numeric(18, 0)					
		@numUserCntID = @numUserCntID, -- numeric(18, 0)					
		@tintProcessStatus = 1, -- tinyint						
		@tintMode = 1 -- TINYINT
   END 
   --end of script
		--Deferred BizDocs : Create Recurring entry only for create Account Journal
		if @tintDeferred=1
		BEGIN
			exec USP_RecurringAddOpportunity @numOppId,@numOppBizDocsId,0,4,@dtCreatedDate,0,@numDomainID,0,0,100,''
		END

		-- Update name template set for bizdoc
		--DECLARE @tintType TINYINT
		--SELECT @tintType = CASE @tintOppType WHEN 1 THEN 3 WHEN 2 THEN 4 END;
		EXEC dbo.USP_UpdateBizDocNameTemplate @tintOppType,@numDomainID,@numOppBizDocsId

		IF @numFromOppBizDocsId>0
		BEGIN
			insert into                       
		   OpportunityBizDocItems                                                                          
  		   (numOppBizDocID,numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,vcAttributes,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,vcNotes,/*vcTrackingNo,*/bitEmbeddedCost,monEmbeddedCost,/*monShipCost,vcShippingMethod,dtDeliveryDate,*/dtRentalStartDate,dtRentalReturnDate,tintTrackingStatus)
		   select @numOppBizDocsId,OBDI.numOppItemID,OBDI.numItemCode,OBDI.numUnitHour,OBDI.monPrice,OBDI.monTotAmount,OBDI.vcItemDesc,OBDI.numWarehouseItmsID,OBDI.vcType,OBDI.vcAttributes,OBDI.bitDropShip,OBDI.bitDiscountType,OBDI.fltDiscount,OBDI.monTotAmtBefDiscount,OBDI.vcNotes,/*OBDI.vcTrackingNo,*/OBDI.bitEmbeddedCost,OBDI.monEmbeddedCost,/*OBDI.monShipCost,OBDI.vcShippingMethod,OBDI.dtDeliveryDate,*/OBDI.dtRentalStartDate,OBDI.dtRentalReturnDate,OBDI.tintTrackingStatus
		   from OpportunityBizDocs OBD JOIN OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId=OBDI.numOppBizDocID 
		   JOIN OpportunityItems OI ON OBDI.numOppItemID=OI.numoppitemtCode
		   JOIN Item I ON OBDI.numItemCode=I.numItemCode
		   where  OBD.numOppId=@numOppId AND OBD.numOppBizDocsId=@numFromOppBizDocsId
		   AND OI.numoppitemtCode NOT IN(SELECT OBDI.numOppItemID FROM OpportunityBizDocs OBD JOIN dbo.OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId=OBDI.numOppBizDocId WHERE numOppId=@numOppId AND numBizDocId=@numBizDocId)
		END
		ELSE IF /*@bitPartialFulfillment=1 or*/ (@bitPartialFulfillment=1 and DATALENGTH(@strBizDocItems)>2)
			BEGIN
				
			if DATALENGTH(@strBizDocItems)>2
			begin
				EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strBizDocItems 

					IF @bitRentalBizDoc=1
					BEGIN
						update OI set OI.monPrice=OBZ.monPrice,OI.monTotAmount=OBZ.monTotAmount,OI.monTotAmtBefDiscount=OBZ.monTotAmount
						from OpportunityItems OI
						Join (SELECT * FROM OPENXML (@hDocItem,'/NewDataSet/BizDocItems',2)                                                                          
							WITH ( OppItemID numeric(9),Quantity numeric(18,10),monPrice money,monTotAmount money)) OBZ
						on OBZ.OppItemID=OI.numoppitemtCode
						where OBZ.OppItemID=OI.numoppitemtCode
					END
	
					insert into                       
				   OpportunityBizDocItems                                                                          
				   (numOppBizDocID,numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,vcAttributes,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,vcNotes,/*vcTrackingNo,vcShippingMethod,monShipCost,dtDeliveryDate,*/dtRentalStartDate,dtRentalReturnDate)
				   
				   select @numOppBizDocsId,numoppitemtCode,numItemCode,Quantity,CASE WHEN @tintOppType=2 THEN OBZ.monPrice ELSE OI.monPrice END,CASE WHEN @tintOppType=2 THEN OBZ.monPrice * Quantity ELSE (monTotAmount/numUnitHour)*Quantity END,vcItemDesc,numWarehouseItmsID,vcType,vcAttributes,bitDropShip,bitDiscountType,
				   case when bitDiscountType=0 then fltDiscount when bitDiscountType=1 then (fltDiscount/numUnitHour)*Quantity else fltDiscount end ,CASE WHEN @tintOppType=2 THEN OBZ.monPrice * Quantity ELSE (monTotAmtBefDiscount/numUnitHour)*Quantity END,Notes,/*TrackingNo,vcShippingMethod,monShipCost,dtDeliveryDate,*/dtRentalStartDate,dtRentalReturnDate  from OpportunityItems OI
				   Join (SELECT * FROM OPENXML (@hDocItem,'/NewDataSet/BizDocItems',2)                                                                          
				   WITH                       
				   (OppItemID numeric(9),                               
				    Quantity numeric(18,10),
					Notes varchar(500),
					--TrackingNo varchar(500),
					--vcShippingMethod varchar(100),
					--monShipCost money,
					--dtDeliveryDate datetime
					monPrice MONEY,
					dtRentalStartDate datetime,dtRentalReturnDate datetime
					)) OBZ
				   on OBZ.OppItemID=OI.numoppitemtCode
				   where  numOppId=@numOppId AND ISNULL(OI.numUnitHour,0)>0 AND ISNULL(OI.numUnitHour,0)>0
				   
				EXEC sp_xml_removedocument @hDocItem 
			END

		end
		else
		BEGIN
		   	   insert into OpportunityBizDocItems                                                                          
  			   (numOppBizDocID,numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,vcAttributes,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount)
			   select @numOppBizDocsId,numoppitemtCode,numItemCode,CASE @bitRecurringBizDoc WHEN 1 THEN 1 ELSE numUnitHour END AS numUnitHour ,monPrice,CASE @bitRecurringBizDoc WHEN 1 THEN monPrice * 1 ELSE monTotAmount END AS monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,vcAttributes,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount from OpportunityItems OI
			   where  numOppId=@numOppId AND ISNULL(OI.numUnitHour,0)>0
			   AND numoppitemtCode NOT IN(SELECT OBDI.numOppItemID FROM OpportunityBizDocs OBD JOIN dbo.OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId=OBDI.numOppBizDocId WHERE numOppId=@numOppId AND numBizDocId=@numBizDocId)
		   
			IF @bitAuthBizdoc = 1 and @tintOppType = 1
				exec USP_AutoAssign_OppSerializedItem @numDomainID,@numOppID,@numOppBizDocsId
		end


               
--		insert into OpportunityBizDocTaxItems(numOppBizDocID, numTaxItemID, fltPercentage)
--		select @numOppBizDocsId,numTaxItemID,dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,numTaxItemID,@numOppId,0) from TaxItems
--        where numDomainID= @numDomainID
--        union 
--        select @numOppBizDocsId,0,dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,0,@numOppId,0)

		SET @numOppBizDocsID = @numOppBizDocsId
		--select @numOppBizDocsId
	end
	ELSE
	  BEGIN
  			SET @numOppBizDocsId=0
			SET @monDealAmount = 0
			SET @monCreditAmount = 0
			RAISERROR ('NOT_ALLOWED',16,1);
	  END
--	IF @tintTaxOperator = 3 -- Remove default Sales Tax percentage defined and Add Sales Tax percentage Used for Online Market Place
--		BEGIN
--				DELETE FROM [OpportunityBizDocTaxItems] WHERE [numOppBizDocID] = @numOppBizDocsId AND [numTaxItemID] = 0 
--				insert into OpportunityBizDocTaxItems(numOppBizDocID, numTaxItemID, fltPercentage)
--					select @numOppBizDocsId,0,@OMP_SalesTaxPercent
--		END	
end
else
BEGIN
	  UPDATE OpportunityBizDocs
	  SET    numModifiedBy = @numUserCntID,
			 dtModifiedDate = Getdate(),
			 vcComments = @vcComments,
			 --bitDiscountType = @bitDiscountType,
			 --fltDiscount = @fltDiscount,
			 --monShipCost = @monShipCost,
			 numShipVia = @numShipVia,
			 vcTrackingURL = @vcTrackingURL,
			 --bitBillingTerms = @bitBillingTerms,
			 --intBillingDays = @intBillingDays,
			 dtFromDate = @dtFromDate,
			 --bitInterestType = @bitInterestType,
			 --fltInterest = @fltInterest,
--			 numShipDoc = @numShipDoc,
			 numBizDocStatus = @numBizDocStatus,
			 numSequenceId=@numSequenceId,
			 --[tintTaxOperator]=@tintTaxOperator,
			 numBizDocTempID=@numBizDocTempID,
			 vcTrackingNo=@vcTrackingNo,
			 --vcShippingMethod=@vcShippingMethod,dtDeliveryDate=@dtDeliveryDate,
			 vcRefOrderNo=@vcRefOrderNo,
			 fltExchangeRateBizDoc=@fltExchangeRateBizDoc
			 --,numBizDocStatusOLD=ISNULL(numBizDocStatus,0)
	  WHERE  numOppBizDocsId = @numOppBizDocsId
	  --SELECT @numOppBizDocsId
	--Added By:Sachin Sadhu||Date:27thAug2014
	--Purpose :MAke entry in WFA queue 
IF EXISTS (SELECT 1 FROM dbo.OpportunityMaster WHERE numOppId = @numOppID AND tintOppType=1 AND isnull(tintOppStatus,0)=1 )
	BEGIN
		EXEC 
		USP_ManageOpportunityAutomationQueue			
		@numOppQueueID = 0, -- numeric(18, 0)							
		@numDomainID = @numDomainID, -- numeric(18, 0)							
		@numOppId = @numOppID, -- numeric(18, 0)							
		@numOppBizDocsId = @numOppBizDocsId, -- numeric(18, 0)							
		@numOrderStatus = 0, -- numeric(18, 0)					
		@numUserCntID = @numUserCntID, -- numeric(18, 0)					
		@tintProcessStatus = 1, -- tinyint						
		@tintMode = 1 -- TINYINT
   END 
   --end of code
   	IF DATALENGTH(@strBizDocItems)>2
			BEGIN

				EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strBizDocItems 

                    delete from OpportunityBizDocItems  
					where numOppItemID not in (SELECT OppItemID FROM OPENXML (@hDocItem,'/NewDataSet/BizDocItems',2)                                                                          
					WITH                       
				   ( OppItemID numeric(9)
					)) and numOppBizDocID=@numOppBizDocsId  
                  
                     
					IF @bitRentalBizDoc=1
					BEGIN
						update OI set OI.monPrice=OBZ.monPrice,OI.monTotAmount=OBZ.monTotAmount,OI.monTotAmtBefDiscount=OBZ.monTotAmount
						from OpportunityItems OI
						Join (SELECT * FROM OPENXML (@hDocItem,'/NewDataSet/BizDocItems',2)                                                                          
							WITH ( OppItemID numeric(9),Quantity numeric(18,10),monPrice money,monTotAmount money)) OBZ
						on OBZ.OppItemID=OI.numoppitemtCode
						where OBZ.OppItemID=OI.numoppitemtCode
					END

					update OBI set OBI.numUnitHour= Quantity,OBI.monPrice=CASE WHEN @tintOppType=2 THEN OBZ.monPrice ELSE OI.monPrice END,OBI.monTotAmount=CASE WHEN @tintOppType=2 THEN OBZ.monPrice * Quantity ELSE (OI.monTotAmount/OI.numUnitHour)*Quantity END,OBI.vcNotes= Notes,
					OBI.fltDiscount=case when OI.bitDiscountType=0 then OI.fltDiscount when OI.bitDiscountType=1 then (OI.fltDiscount/OI.numUnitHour)*Quantity else OI.fltDiscount end ,
					OBI.monTotAmtBefDiscount=(OI.monTotAmtBefDiscount/OI.numUnitHour)*Quantity ,                                                                    
					/*OBI.vcTrackingNo=TrackingNo,OBI.vcShippingMethod=OBZ.vcShippingMethod,OBI.monShipCost=OBZ.monShipCost,OBI.dtDeliveryDate=OBZ.dtDeliveryDate*/
				    OBI.dtRentalStartDate=OBZ.dtRentalStartDate,OBI.dtRentalReturnDate=OBZ.dtRentalReturnDate
					from OpportunityBizDocItems OBI
					join OpportunityItems OI
					on OBI.numOppItemID=OI.numoppitemtCode
					Join (SELECT * FROM OPENXML (@hDocItem,'/NewDataSet/BizDocItems',2)                                                                          
					WITH                       
				   ( OppItemID numeric(9),                                     
				   Quantity numeric(18,10),
					Notes varchar(500),
					--TrackingNo varchar(500),
					--vcShippingMethod varchar(100),
					--monShipCost money,
					--dtDeliveryDate datetime,
					monPrice MONEY,
					dtRentalStartDate datetime,dtRentalReturnDate datetime
					)) OBZ
				   on OBZ.OppItemID=OI.numoppitemtCode
				   where  OI.numOppId=@numOppId and OBI.numOppBizDocID=@numOppBizDocsId AND ISNULL(OI.numUnitHour,0)>0


                   insert into                       
				   OpportunityBizDocItems                                                                          
				   (numOppBizDocID,numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,vcAttributes,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,vcNotes/*,vcTrackingNo,vcShippingMethod,monShipCost,dtDeliveryDate*/)                      
				   select @numOppBizDocsId,numoppitemtCode,numItemCode,Quantity,CASE WHEN @tintOppType=2 THEN OBZ.monPrice ELSE OI.monPrice END,CASE WHEN @tintOppType=2 THEN OBZ.monPrice * Quantity ELSE (monTotAmount/numUnitHour)*Quantity END,vcItemDesc,numWarehouseItmsID,vcType,vcAttributes,bitDropShip,bitDiscountType,
				   case when bitDiscountType=0 then fltDiscount when bitDiscountType=1 then (fltDiscount/numUnitHour)*Quantity else fltDiscount end ,(monTotAmtBefDiscount/numUnitHour)*Quantity,Notes/*,TrackingNo,vcShippingMethod,monShipCost,dtDeliveryDate*/ from OpportunityItems OI
				   Join (SELECT * FROM OPENXML (@hDocItem,'/NewDataSet/BizDocItems',2)                                                                          
				   WITH                       
				   ( OppItemID numeric(9),                                     
				   Quantity numeric(18,10),
					Notes varchar(500),
					monPrice MONEY
					--TrackingNo varchar(500),
					--vcShippingMethod varchar(100),
					--monShipCost money,
					--dtDeliveryDate DATETIME
					)) OBZ
				   on OBZ.OppItemID=OI.numoppitemtCode and OI.numoppitemtCode not in (select numOppItemID from OpportunityBizDocItems where numOppBizDocID =@numOppBizDocsId)
				   where  numOppId=@numOppId AND ISNULL(OI.numUnitHour,0)>0
                 
				EXEC sp_xml_removedocument @hDocItem 
			END


		
		/*Note: by chintan
			numTaxItemID= 0 which stands for default sales tax
		*/
--		IF @tintTaxOperator = 1 -- Add Sales Tax Blindly which doesn't  depends on if company/item has sales tax enabled
--		BEGIN
--				DECLARE @SalesTax float
--				SELECT @SalesTax = dbo.fn_CalSalesTaxAmt(@numOppBizDocsId,@numDomainID);
--				IF @SalesTax > 0 
--				BEGIN
--					DELETE FROM [OpportunityBizDocTaxItems] WHERE [numOppBizDocID]=@numOppBizDocsId AND [numTaxItemID]=0
--					insert into OpportunityBizDocTaxItems(numOppBizDocID, numTaxItemID, fltPercentage)
--					select @numOppBizDocsId,0,@SalesTax
--				END
--				
--		END
--		ELSE IF @tintTaxOperator = 2 -- Remove Sales Tax Blindly which doesn't  depends on if company/item has sales tax enabled
--		BEGIN
--				DELETE FROM [OpportunityBizDocTaxItems] WHERE [numOppBizDocID] = @numOppBizDocsId AND [numTaxItemID] = 0 
--		END
END

IF @numOppBizDocsId>0
BEGIN
	select @monDealAmount=isnull(dbo.[GetDealAmount](@numOppId,GETUTCDATE(),@numOppBizDocsId),0)

	--Credit Balance
--	Declare @CAError as int;Set @CAError=0
--
--   	Declare @monOldCreditAmount as money;Set @monOldCreditAmount=0
--	Select @monOldCreditAmount=isnull(monCreditAmount,0) from OpportunityBizDocs WHERE  numOppBizDocsId = @numOppBizDocsId
--
--	if @monCreditAmount>0
--	BEGIN
--			IF (@monDealAmount + @monOldCreditAmount) >= @monCreditAmount
--			BEGIN
--				IF exists (select * from [CreditBalanceHistory] where  numOppBizDocsId=@numOppBizDocsId)
--						Update [CreditBalanceHistory] set [monAmount]=@monCreditAmount * -1 where numOppBizDocsId=@numOppBizDocsId
--				ELSE
--						 INSERT INTO [CreditBalanceHistory]
--							(numOppBizDocsId,[monAmount],[dtCreateDate],[dtCreatedBy],[numDomainId],numDivisionID)
--						VALUES (@numOppBizDocsId,@monCreditAmount * -1,GETDATE(),@numUserCntID,@numDomainId,@numDivisionID)
--			END	
--			ELSE
--			BEGIN	
--				SET @monCreditAmount=0
--				SET @CAError=-1
--			END
--	  END
--	  ELSE
--	  BEGIN
--			delete from [CreditBalanceHistory] where numOppBizDocsId=@numOppBizDocsId
--	  END
--	
--	IF @tintOppType=1 --Sales
--	  update DivisionMaster set monSCreditBalance=isnull(monSCreditBalance,0) + isnull(@monOldCreditAmount,0) - isnull(@monCreditAmount,0) where numDivisionID=@numDivisionID and numDomainID=@numDomainID
--    else IF @tintOppType=2 --Purchase
--	  update DivisionMaster set monPCreditBalance=isnull(monPCreditBalance,0) + isnull(@monOldCreditAmount,0) - isnull(@monCreditAmount,0) where numDivisionID=@numDivisionID and numDomainID=@numDomainID
--		

	  --Update OpportunityBizDocs set monCreditAmount=@monCreditAmount   WHERE  numOppBizDocsId = @numOppBizDocsId
	  Update OpportunityBizDocs set vcBizDocID=vcBizDocName + ISNULL(@numSequenceId,''),
			monDealAmount=@monDealAmount
			WHERE  numOppBizDocsId = @numOppBizDocsId

	 --SET @monCreditAmount =Case when @CAError=-1 then -1 else @monCreditAmount END
END
 
	 END TRY
BEGIN CATCH
    DECLARE @ErrorMessage NVARCHAR(4000);
    DECLARE @ErrorSeverity INT;
    DECLARE @ErrorState INT;

    SELECT 
        @ErrorMessage = ERROR_MESSAGE(),
        @ErrorSeverity = ERROR_SEVERITY(),
        @ErrorState = ERROR_STATE();

    -- Use RAISERROR inside the CATCH block to return error
    -- information about the original error that caused
    -- execution to jump to the CATCH block.
    RAISERROR (@ErrorMessage, -- Message text.
               @ErrorSeverity, -- Severity.
               @ErrorState -- State.
               );
END CATCH;
/****** Object:  StoredProcedure [dbo].[Usp_CreateCloneItem]    Script Date: 18/06/2013 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Manish Anjara
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='Usp_CreateCloneItem')
DROP PROCEDURE Usp_CreateCloneItem
GO
CREATE PROCEDURE [dbo].[Usp_CreateCloneItem]
(
	@numItemCode		BIGINT OUTPUT,
	@numDomainID		NUMERIC(18,0)
)
AS 

BEGIN
--Item 	
--ItemAPI-
--ItemCategory
--ItemDetails
--ItemImages
--ItemTax
--Vendor,
--WareHouseItems
--CFW_FLD_Values_Item

	DECLARE @numNewItemCode AS BIGINT
	
	INSERT INTO dbo.Item 
	(vcItemName,txtItemDesc,charItemType,monListPrice,numItemClassification,bitTaxable,vcSKU,bitKitParent,numVendorID,numDomainID,numCreatedBy,bintCreatedDate,
	bintModifiedDate,numModifiedBy,bitAllowBackOrder,bitSerialized,vcModelID,numItemGroup,numCOGsChartAcntId,numAssetChartAcntId,numIncomeChartAcntId,
	monAverageCost,monCampaignLabourCost,bitFreeShipping,fltLength,fltWidth,fltHeight,fltWeight,vcUnitofMeasure,bitCalAmtBasedonDepItems,bitShowDeptItemDesc,
	bitShowDeptItem,bitAssembly,numBarCodeId,vcManufacturer,numBaseUnit,numPurchaseUnit,numSaleUnit,bitLotNo,IsArchieve,numItemClass,vcPathForImage,
	vcPathForTImage,tintStandardProductIDType,numShipClass,vcExportToAPI,bitAllowDropShip)
	SELECT vcItemName,txtItemDesc,charItemType,monListPrice,numItemClassification,bitTaxable,vcSKU,bitKitParent,numVendorID,numDomainID,numCreatedBy,GETDATE(),
	GETDATE(),numModifiedBy,bitAllowBackOrder,bitSerialized,vcModelID,numItemGroup,numCOGsChartAcntId,numAssetChartAcntId,numIncomeChartAcntId,
	0,monCampaignLabourCost,bitFreeShipping,fltLength,fltWidth,fltHeight,fltWeight,vcUnitofMeasure,bitCalAmtBasedonDepItems,bitShowDeptItemDesc,
	bitShowDeptItem,bitAssembly,numBarCodeId,vcManufacturer,numBaseUnit,numPurchaseUnit,numSaleUnit,bitLotNo,IsArchieve,numItemClass,vcPathForImage,
	vcPathForTImage,tintStandardProductIDType,numShipClass,vcExportToAPI,bitAllowDropShip
	FROM Item WHERE numItemCode = @numItemCode AND numDomainID = @numDomainID
	 
	SELECT @numNewItemCode  = @@IDENTITY
	 
	INSERT INTO dbo.ItemAPI 
	(WebApiId,numDomainId,numItemID,vcAPIItemID,numCreatedby,dtCreated,numModifiedby,dtModified)
	SELECT WebApiId,numDomainId,@numNewItemCode,vcAPIItemID,numCreatedby,GETDATE(),numModifiedby,GETDATE()
	FROM dbo.ItemAPI WHERE numItemID = @numItemCode AND numDomainId = @numDomainId
	
	INSERT INTO dbo.ItemCategory ( numItemID,numCategoryID )
	SELECT @numNewItemCode, numCategoryID FROM ItemCategory WHERE numItemID = @numItemCode
	
	INSERT INTO dbo.ItemDetails (numItemKitID,numChildItemID,numQtyItemsReq,numWareHouseItemId,numUOMId,vcItemDesc,sintOrder)
	SELECT @numNewItemCode,numChildItemID,numQtyItemsReq,numWareHouseItemId,numUOMId,vcItemDesc,sintOrder FROM dbo.ItemDetails WHERE numItemKitID = @numItemCode
	
	INSERT INTO dbo.ItemImages (numItemCode,vcPathForImage,vcPathForTImage,bitDefault,intDisplayOrder,numDomainId,bitIsImage)
	SELECT @numNewItemCode,vcPathForImage,vcPathForTImage,bitDefault,intDisplayOrder,numDomainId,bitIsImage 
	FROM dbo.ItemImages WHERE numItemCode = @numItemCode  AND numDomainId = @numDomainID
	
	INSERT INTO dbo.ItemTax (numItemCode,numTaxItemID,bitApplicable)
	SELECT @numNewItemCode,numTaxItemID,bitApplicable FROM dbo.ItemTax WHERE numItemCode = @numItemCode
	
	INSERT INTO dbo.Vendor (numVendorID,vcPartNo,numDomainID,monCost,numItemCode,intMinQty)
	SELECT numVendorID,vcPartNo,numDomainID,monCost,@numNewItemCode,intMinQty 
	FROM dbo.Vendor WHERE numItemCode = @numItemCode AND numDomainID = @numDomainID
	
	INSERT INTO dbo.WareHouseItems (numItemID,numWareHouseID,numOnHand,numOnOrder,numReorder,numAllocation,numBackOrder,monWListPrice,vcLocation,vcWHSKU,
	vcBarCode,numDomainID,dtModified,numWLocationID) 
	SELECT @numNewItemCode,numWareHouseID,0,0,0,0,0,monWListPrice,vcLocation,vcWHSKU,vcBarCode,numDomainID,GETDATE(),numWLocationID 
	FROM dbo.WareHouseItems  WHERE numItemID = @numItemCode AND numDomainID = @numDomainID
	
	INSERT INTO CFW_FLD_Values_Item (Fld_ID,Fld_Value,RecId)  
	SELECT Fld_ID,Fld_Value,@numNewItemCode FROM CFW_FLD_Values_Item  WHERE RecId = @numItemCode 
	
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_DeleteSalesReturnDetails' ) 
    DROP PROCEDURE USP_DeleteSalesReturnDetails
GO

CREATE PROCEDURE [dbo].[USP_DeleteSalesReturnDetails]
    (
      @numReturnHeaderID NUMERIC(9),
      @numReturnStatus	NUMERIC(18,0),
      @numDomainId NUMERIC(9),
      @numUserCntID NUMERIC(9)
    )
AS 
    BEGIN
    DECLARE @tintReturnType TINYINT,@tintReceiveType TINYINT
    
    SELECT @tintReturnType=tintReturnType,@tintReceiveType=tintReceiveType FROM ReturnHeader WHERE numReturnHeaderID = @numReturnHeaderID    
   
     IF (@tintReturnType=1 AND @tintReceiveType=2) OR @tintReturnType=3
     BEGIN
		IF EXISTS(SELECT numDepositID FROM DepositMaster WHERE tintDepositePage=3 AND numReturnHeaderID=@numReturnHeaderID AND ISNULL(monAppliedAmount,0)>0)
		BEGIN
			RAISERROR ( 'CreditMemo_PAID', 16, 1 ) ;
			RETURN ;
		END	
     END
     
     IF (@tintReturnType=2 AND @tintReceiveType=2) OR @tintReturnType=3
     BEGIN
		IF EXISTS(SELECT numReturnHeaderID FROM BillPaymentHeader WHERE numReturnHeaderID=@numReturnHeaderID AND ISNULL(monAppliedAmount,0)>0)
		BEGIN
			RAISERROR ( 'CreditMemo_PAID', 16, 1 ) ;
			RETURN ;
		END	
     END
     
     IF (@tintReturnType=1 or @tintReturnType=2) 
     BEGIN
		IF EXISTS(SELECT numReturnHeaderID FROM OppWarehouseSerializedItem WHERE ISNULL(numOppID,0)>0 AND ISNULL(numOppBizDocsId,0)>0 
			AND numWareHouseItmsDTLID IN (SELECT numWareHouseItmsDTLID FROM OppWarehouseSerializedItem 
			WHERE numReturnHeaderID=@numReturnHeaderID))
		BEGIN
			RAISERROR ( 'Serial_LotNo_Used', 16, 1 ) ;
			RETURN ;
		END	
		ELSE
		BEGIN
			  UPDATE WareHouseItmsDTL SET tintStatus=(CASE WHEN @tintReturnType=1 THEN 3 ELSE tintStatus END)
				WHERE numWareHouseItmsDTLID IN (SELECT numWareHouseItmsDTLID FROM OppWarehouseSerializedItem WHERE numReturnHeaderID=@numReturnHeaderID)
		END
     END
	
	 IF @tintReturnType=1 OR @tintReturnType=2 
        BEGIN
			EXEC usp_ManageRMAInventory @numReturnHeaderID,@numDomainId,@numUserCntID,2
        END
         	              
        UPDATE  dbo.ReturnHeader
        SET     numReturnStatus = @numReturnStatus,
                numBizdocTempID = CASE WHEN @tintReturnType=1 OR @tintReturnType=2 THEN NULL ELSE numBizdocTempID END,
                vcBizDocName =CASE WHEN @tintReturnType=1 OR @tintReturnType=2 THEN '' ELSE vcBizDocName END ,
                IsCreateRefundReceipt = 0,
                tintReceiveType=0,monBizDocAmount=0, numItemCode = 0
        WHERE   numReturnHeaderID = @numReturnHeaderID
         AND	numDomainId = @numDomainId
    END
    
     IF (@tintReturnType=1 AND @tintReceiveType=2) OR @tintReturnType=3
     BEGIN
		DELETE FROM DepositMaster WHERE tintDepositePage=3 AND numReturnHeaderID=@numReturnHeaderID
	 END	
     
     IF (@tintReturnType=2 AND @tintReceiveType=2) OR @tintReturnType=3
     BEGIN
		DELETE FROM BillPaymentHeader WHERE numReturnHeaderID=@numReturnHeaderID
     END
    
	 IF @tintReceiveType = 1 AND @tintReturnType = 4
	 BEGIN
	
		DECLARE @numDepositIDRef AS NUMERIC(18,0)
		SELECT @numDepositIDRef = ISNULL(numDepositIDRef,0) FROM [dbo].[ReturnHeader] AS RH WHERE [RH].[numReturnHeaderID] = @numReturnHeaderID
		PRINT @numDepositIDRef

		DECLARE @monAppliedAmount AS MONEY
		SELECT @monAppliedAmount = ISNULL([RH].[monAmount],0)
		FROM [dbo].[ReturnHeader] AS RH 
		WHERE [RH].[numReturnHeaderID] = @numReturnHeaderID 
		AND [RH].[numDomainId] = @numDomainId

		IF @numDepositIDRef > 0			
			BEGIN
				UPDATE [dbo].[DepositMaster] SET [monAppliedAmount] = [monAppliedAmount] - @monAppliedAmount, [monRefundAmount] = [monRefundAmount] - @monAppliedAmount WHERE [numDepositId] = @numDepositIDRef AND [numDomainId] = @numDomainId
			END
	 
		DECLARE @numBillPaymentIDRef AS NUMERIC(18,0)
		SELECT @numBillPaymentIDRef = ISNULL(numBillPaymentIDRef,0) FROM [dbo].[ReturnHeader] AS RH WHERE [RH].[numReturnHeaderID] = @numReturnHeaderID
		PRINT @numBillPaymentIDRef

		IF @numBillPaymentIDRef > 0			
			BEGIN
				UPDATE [dbo].[BillPaymentHeader] SET [monAppliedAmount] = [monAppliedAmount] - @monAppliedAmount, [monRefundAmount] = [monRefundAmount] - @monAppliedAmount WHERE [BillPaymentHeader].[numBillPaymentID] = @numBillPaymentIDRef AND [numDomainId] = @numDomainId
			END

		DELETE FROM [dbo].[CheckHeader] WHERE [CheckHeader].[numReferenceID] = @numReturnHeaderID
	 END

    DELETE FROM dbo.General_Journal_Details WHERE numJournalId IN (SELECT numJournal_Id FROM General_Journal_Header WHERE numReturnID=@numReturnHeaderID)
    DELETE FROM dbo.General_Journal_Header WHERE numReturnID=@numReturnHeaderID

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DemandForecast_Delete')
DROP PROCEDURE dbo.USP_DemandForecast_Delete
GO
CREATE PROCEDURE [dbo].[USP_DemandForecast_Delete]
	@numDomainID NUMERIC (18,0),
	@vcSelectedIDs VARCHAR(500)
AS 
BEGIN
	DECLARE @strSQL VARCHAR(MAX)

	SET @strSQL = 'DELETE FROM DemandForecast WHERE numDomainID = ' + CAST(@numDomainID AS VARCHAR(10)) + ' AND numDFID IN (' + @vcSelectedIDs + ')'

	EXEC(@strSQL)
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DemandForecast_DeleteOpp')
DROP PROCEDURE dbo.USP_DemandForecast_DeleteOpp
GO
CREATE PROCEDURE [dbo].[USP_DemandForecast_DeleteOpp]
	@vcOppID AS VARCHAR(MAX),
	@numDomainID AS NUMERIC(18,0),
	@numUserCntID AS NUMERIC(18,0)
AS 
BEGIN

BEGIN TRY 
BEGIN TRAN

	DECLARE @TEMPTABLE TABLE
	(
		NUMBER INT,
		numOppID NUMERIC(18,0)
	)

	INSERT INTO @TEMPTABLE
		(
			NUMBER,
			numOppID
		)
	SELECT
		ROW_NUMBER() OVER (ORDER BY Items) AS NUMBER,
		CAST(Items AS NUMERIC(18,0))
	FROM 
		dbo.Split(@vcOppID,',')


	DECLARE @i As INT = 1
	DECLARE @Count AS INT
	DECLARE @numOppID AS NUMERIC(18,0)
	SELECT @Count = COUNT(*) FROM @TEMPTABLE

	WHILE @i <= @Count
	BEGIN

		SELECT @numOppID = numOppID FROM @TEMPTABLE WHERE NUMBER = @i

		EXEC [USP_DeleteOppurtunity]  @numOppId = @numOppID, @numDomainID = @numDomainID,@numUserCntID = @numUserCntID 

		SET @i = @i + 1
	END
COMMIT TRAN 
END TRY 
BEGIN CATCH		
    DECLARE @strMsg VARCHAR(200)
    SET @strMsg = ERROR_MESSAGE()
    IF ( @@TRANCOUNT > 0 ) 
        BEGIN
            RAISERROR ( @strMsg, 16, 1 ) ;
            ROLLBACK TRAN
        END
END CATCH	
	
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DemandForecast_Execute')
DROP PROCEDURE dbo.USP_DemandForecast_Execute
GO
CREATE PROCEDURE [dbo].[USP_DemandForecast_Execute]
	@numDFID NUMERIC (18,0),
	@numUserCntID NUMERIC(18,0),
	@numPageIndex INT,
	@numPageSize INT,
	@ClientTimeZoneOffset INT,
	@FromDate DATETIME,
	@ToDate DATETIME
AS 
BEGIN
	DECLARE @numDomainID NUMERIC(18,0)
	DECLARE @numDFAPID NUMERIC(18,0)
	DECLARE @numDFDaysID NUMERIC(18,0)
	DECLARE @bitLastYear BIT
	DECLARE @bitIncludeSalesOpp BIT
	DECLARE @dtFromDate DATETIME
	DECLARE @dtToDate DATETIME
	DECLARE @dtLastExecution DATETIME

	
	IF (SELECT COUNT(*) FROM DemandForecast WHERE numDFID = @numDFID) > 0
	BEGIN
		
		DECLARE @TotalRowCount AS INTEGER = 0
		DECLARE @numAnalysisDays INT
		DECLARE @numForecastDays INT
		DECLARE @isNextWeekLastYear BIT

		-- Get Selected Historical Analysis Pattern, Forecast Days and is Last Year
		SELECT 
			@numDomainID=numDomainID, 
			@numDFAPID=numDFAPID, 
			@numDFDaysID=numDFDaysID, 
			@bitLastYear=bitLastYear, 
			@bitIncludeSalesOpp= bitIncludeSalesOpp,
			@dtLastExecution = dtExecutionDate
		FROM 
			DemandForecast 
		WHERE 
			numDFID = @numDFID

		-- Get Days of Historical Analysis
		SELECT @numAnalysisDays=numDays,@isNextWeekLastYear=bitNextWeekLastYear FROM DemandForecastAnalysisPattern WHERE numDFAPID = @numDFAPID

		-- Get Days for forecast
		SELECT  @numForecastDays = numDays FROM DemandForecastDays WHERE numDFDaysID = @numDFDaysID

		--User has selected its own range
		IF NOT @FromDate IS NULL AND NOT @ToDate IS NULL
		BEGIN
			SET @dtFromDate = @FromDate
			SET @dtToDate = @ToDate
		END
		ELSE
		BEGIN
		-- Check if selected historical analysis pattern is from next week last year dropdown
		IF ISNULL(@isNextWeekLastYear,0) = 1 --historical analysis pattern is from next week last year dropdown
		BEGIN
			SET @dtFromDate = DATEADD(yy,-1,DATEADD(d,1,GETDATE()))
			SET @dtToDate = DATEADD(yy,-1,DATEADD(d,@numAnalysisDays,GETDATE()))
		END
		ELSE --historical analysis pattern is from lst week dropdown
		BEGIN
			-- Check if get last week from last year
			IF ISNULL(@bitLastYear,0) = 1 --last week from last year
			BEGIN
				SET @dtToDate = DATEADD(yy,-1,DATEADD(d,-1,GETDATE()))
				SET @dtFromDate = DATEADD(yy,-1,DATEADD(d,-(@numAnalysisDays),GETDATE()))
			END
			ELSE --last week current year
			BEGIN
				SET @dtToDate = DATEADD(d,-1,GETDATE())
				SET @dtFromDate = DATEADD(d,-(@numAnalysisDays),GETDATE())
			END
		END
		END
		
		DECLARE @TEMPOppMaster TABLE
		(
			numItemCode NUMERIC(18,0),
			numWarehouseItemID NUMERIC(18,0),
			numQuantitySold INT
		)

		DECLARE @TEMPSALESOPP TABLE
		(
			numItemCode INT,
			numWarehouseItemID NUMERIC(18,0),
			numQuantitySold INT
		)

		-- Get total quantity of each item sold in sales order between from and to date and insert into temp table
		INSERT INTO @TEMPOppMaster
			(
				numItemCode,
				numWarehouseItemID,
				numQuantitySold
			)
		SELECT
			numItemCode,
			numWarehouseItmsID,
			ISNULL(numQtySold,0)
		FROM
			dbo.GetDFHistoricalSalesOrderData(@numDFID,@numDomainID,@dtFromDate,@dtToDate)

		--If user has selected to include Sales Opportunity(tintOppType = 1 AND tintOppStatus = 0) items in demand forecast then include it in total quantity sold by numPercentComplete
		--For example if 10 units within a Sales Opportunity that shows 50% completion, then an addition 5 units would be added
		IF ISNULL(@bitIncludeSalesOpp,0) = 1
		BEGIN
				
			INSERT INTO @TEMPSALESOPP
			(
				numItemCode,
				numWarehouseItemID,
				numQuantitySold
			)
			SELECT
				numItemCode,
				numWarehouseItmsID,
				ISNULL(numQtySold,0)
			FROM
				dbo.GetDFHistoricalSalesOppData(@numDFID,@numDomainID,@dtFromDate,@dtToDate)


			-- Increase quantity of item is it already exist in table @TEMPOppMaster
			UPDATE
				@TEMPOppMaster
			SET
				numQuantitySold = ISNULL(t1.numQuantitySold,0) + ISNULL(t2.numQuantitySold,0)
			FROM
				@TEMPOppMaster AS t1
			LEFT JOIN
				@TEMPSALESOPP AS t2
			ON
				t1.numItemCode = t2.numItemCode AND
				t1.numWarehouseItemID = t2.numWarehouseItemID

			-- If item is not exist in @TEMPOppMaster then insert them in @TEMPOppMaster
			INSERT INTO 
				@TEMPOppMaster
			SELECT
				TEMPSALESOPP.numItemCode,
				TEMPSALESOPP.numWarehouseItemID,
				TEMPSALESOPP.numQuantitySold
			FROM
				@TEMPSALESOPP AS TEMPSALESOPP
			WHERE NOT EXISTS
				(
					SELECT 
						1
					FROM
						@TEMPOppMaster t2
					WHERE
						TEMPSALESOPP.numItemCode = t2.numItemCode and 
						TEMPSALESOPP.numWarehouseItemID = t2.numWarehouseItemID
				)
		END

				
		SELECT 
			ROW_NUMBER() OVER (ORDER BY TEMP.dtOrderDate) AS NUMBER,
			TEMP.*
		INTO 
			#TEMP
		FROM
		(
			SELECT
				Item.vcModelID,
				Item.vcItemName,
				Item.bitAssembly,
				'Inventory Item' AS vcItemType,
				T2.numOnHand, 
				T2.numOnOrder, 
				T2.numAllocation, 
				T2.numBackOrder,
				T2.numReOrder,
				Item.numItemCode,
				(SELECT ISNULL(vcData,'') FROM ListDetails WHERE numListID = 36 and numListItemID = Item.numItemClassification) AS numItemClassification,
				(SELECT ISNULL(vcItemGroup,'') FROM ItemGroups WHERE numItemGroupID = Item.numItemGroup) AS numItemGroup,
				(CASE WHEN Item.bitAssembly = 1 THEN Item.vcSKU ELSE WareHouseItems.vcWHSKU END) AS vcSKU,
				WareHouses.numWarehouseID,
				WareHouses.vcWareHouse,
				WareHouseItems.numWarehouseItemID,
				T2.intMinQty,
				T2.numDivisionID AS numVendorID,
				T2.intLeadTimeDays,
				T2.monListPrice,
				ISNULL(t1.numQuantitySold,0) AS numQuantitySold,
				(CASE WHEN T2.intMinQty > T2.numQtyToOrder THEN T2.intMinQty ELSE T2.numQtyToOrder END) AS numQtyToOrder,
				CONVERT(DATE,T2.dtOrderDate) dtOrderDate,
				(CASE WHEN CONVERT(DATE,T2.dtOrderDate) = CONVERT(DATE,GETDATE()) THEN
				'<b><font color=red>Today</font></b>'
				WHEN CONVERT(DATE,T2.dtOrderDate) = CONVERT(DATE,DATEADD(d,1,GETDATE())) THEN
				'<b><font color=purple>YesterDay</font></b>'
				ELSE
					dbo.FormatedDateFromDate(dtOrderDate,@numDomainID)
				END) AS vcOrderDate,
				@numAnalysisDays AS numAnalysisDays,
				@numForecastDays AS numForecastDays
			FROM
				Item
			INNER JOIN
				WareHouseItems
			ON
				Item.numItemCode = WareHouseItems.numItemID
			INNER JOIN
				WareHouses
			ON
				WareHouseItems.numWarehouseID = WareHouses.numWarehouseID
			INNER JOIN
				@TEMPOppMaster t1
			ON
				Item.numItemCode = t1.numItemCode AND
				WareHouseItems.numWarehouseItemID = t1.numWarehouseItemID
			CROSS APPLY
				dbo.GetItemQuantityToOrder(t1.numItemCode,WareHouseItems.numWarehouseItemID,@numDomainID,@numAnalysisDays,@numForecastDays,ISNULL(t1.numQuantitySold,0),NULL) T2
			WHERE
				Item.numDomainID = @numDomainID AND 
				(
					(SELECT COUNT(*) FROM DemandForecastItemClassification WHERE numDFID = @numDFID) = 0 OR
					numItemClassification IN (SELECT numItemClassificationID FROM DemandForecastItemClassification WHERE numDFID = @numDFID)
				) AND
				(
					(SELECT COUNT(*) FROM DemandForecastItemGroup WHERE numDFID = @numDFID) = 0 OR
					numItemGroup IN (SELECT numItemGroupID FROM DemandForecastItemGroup WHERE numDFID = @numDFID)
				) AND
				(
					(SELECT COUNT(*) FROM DemandForecastWarehouse WHERE numDFID = @numDFID) = 0 OR
					WareHouseItems.numWareHouseID IN (SELECT numWarehouseID FROM DemandForecastWarehouse WHERE numDFID = @numDFID)
				) AND
				ISNULL(T2.numQtyToOrder,0) > 0 
		) AS TEMP
			
		
		SELECT @TotalRowCount = COUNT(*) FROM #TEMP
		
		SELECT *,ISNULL(@TotalRowCount,0) AS TotalRowCount FROM #TEMP WHERE NUMBER BETWEEN ((@numPageIndex - 1) * @numPageSize + 1) AND (@numPageIndex * @numPageSize) ORDER BY dtOrderDate

		-- Grid Column Configuration Detail
		DECLARE  @Nocolumns  AS TINYINT;
		SET @Nocolumns = 0

		SELECT @Nocolumns=isnull(sum(TotalRow),0) from(            
		SELECT count(*) TotalRow from View_DynamicColumns where numFormId=125 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 
		UNION 
		SELECT count(*) TotalRow from View_DynamicCustomColumns where numFormId=125 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1) TotalRows

		IF @Nocolumns = 0
		BEGIN
			INSERT INTO DycFormConfigurationDetails 
			(
				numFormID,numFieldID,intColumnNum,intRowNum,
				numDomainID,numUserCntID,numRelCntType,
				tintPageType,bitCustom,intColumnWidth,numViewID
			)
			SELECT 
				125,numFieldId,0,Row_number() over(order by numFormID),
				@numDomainID,@numUserCntID,0,
				1,0,0,0
			FROM 
				DycFormField_Mapping
			WHERE 
				numFormId=125 and bitDefault=1 AND ISNULL(bitSettingField,0)=1 
			ORDER BY 
				[order] ASC  
		END

		SELECT 
			tintRow+1 AS tintOrder,
			vcDbColumnName,
			vcOrigDbColumnName,
			ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName
		FROM 
			View_DynamicColumns 
		WHERE 
		 numFormId=125 AND 
		 numUserCntID=@numUserCntID AND 
		 numDomainID=@numDomainID AND 
		 tintPageType=1 AND 
		 ISNULL(bitSettingField,0)=1 AND 
		 ISNULL(bitCustom,0)=0
		ORDER BY
			tintOrder
 
		--Update Last Execution Date
		UPDATE DemandForecast SET dtExecutionDate = dateadd(MINUTE,-@ClientTimeZoneOffset,GETDATE()) WHERE numDFID = @numDFID

		SELECT CONVERT(VARCHAR(20),dateadd(MINUTE,-@ClientTimeZoneOffset,@dtLastExecution)) AS dtLastExecution
	END
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DemandForecast_Get')
DROP PROCEDURE dbo.USP_DemandForecast_Get
GO
CREATE PROCEDURE [dbo].[USP_DemandForecast_Get]
	@numDomainID NUMERIC (18,0),
	@numPageIndex INTEGER,
	@numPageSize INTEGER
AS 
BEGIN

	DECLARE @TotalRowCount AS INTEGER = 0
	SELECT @TotalRowCount = COUNT(*) FROM DemandForecast WHERE numDomainID = @numDomainID

	SELECT
		*
	FROM
		(
			SELECT
				ROW_NUMBER() OVER(ORDER BY DemandForecast.numDFID) AS NUMBER,
				DemandForecast.numDomainID,
				DemandForecast.numDFID,
				DemandForecast.bitIncludeSalesOpp,
				LEFT(TableItemClassification.list,LEN(TableItemClassification.list) - 1) AS vcItemClassification,
				LEFT(TableItemGroup.list,LEN(TableItemGroup.list) - 1) AS vcItemGroups,
				LEFT(TableWarehouse.list,LEN(TableWarehouse.list) - 1) AS vcWarehouse,
				DemandForecastAnalysisPattern.vcAnalysisPattern AS vcAnalysisPattern,
				DemandForecastDays.vcDays AS vcForecastDays,
				ISNULL(@TotalRowCount,0) AS TotalRowCount
			FROM
				DemandForecast
			LEFT JOIN
				DemandForecastAnalysisPattern
			ON
				DemandForecastAnalysisPattern.numDFAPID  = DemandForecast.numDFAPID
			LEFT JOIN
				DemandForecastDays
			ON
				DemandForecastDays.numDFDaysID = DemandForecast.numDFDaysID
			CROSS APPLY 
				( 
					SELECT 
						vcData + ', ' AS [text()] 
					FROM 
						ListDetails 
					WHERE 
						numListItemID IN (
											SELECT 
												numItemClassificationID 
											FROM 
												DemandForecastItemClassification 
											WHERE 
												numDFID = DemandForecast.numDFID)
					FOR XML PATH('') 
				) TableItemClassification (list)
				CROSS APPLY 
				( 
					SELECT 
						vcItemGroup + ', ' AS [text()] 
					FROM 
						ItemGroups 
					WHERE 
						numItemGroupID IN (
											SELECT 
												numItemGroupID 
											FROM 
												DemandForecastItemGroup
											WHERE 
												numDFID = DemandForecast.numDFID)
					FOR XML PATH('') 
				) TableItemGroup (list)
				CROSS APPLY 
				( 
					SELECT 
						vcWareHouse + ', ' AS [text()] 
					FROM 
						Warehouses 
					WHERE 
						numWareHouseID IN (
											SELECT 
												numWareHouseID 
											FROM 
												DemandForecastWarehouse
											WHERE 
												numDFID = DemandForecast.numDFID)
					FOR XML PATH('') 
				) TableWarehouse (list)
			WHERE
				DemandForecast.numDomainID = @numDomainID
		) AS TEMP
	WHERE
		NUMBER BETWEEN ((@numPageIndex - 1) * @numPageSize + 1) AND (@numPageIndex * @numPageSize)
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DemandForecast_GetByID')
DROP PROCEDURE dbo.USP_DemandForecast_GetByID
GO
CREATE PROCEDURE [dbo].[USP_DemandForecast_GetByID]
	@numDFID NUMERIC(18,0)
AS 
BEGIN
	SELECT * FROM DemandForecast WHERE numDFID = @numDFID
	SELECT * FROM DemandForecastItemClassification WHERE numDFID = @numDFID
	SELECT * FROM DemandForecastItemGroup WHERE numDFID = @numDFID
	SELECT * FROM DemandForecastWarehouse WHERE numDFID = @numDFID
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DemandForecast_GetByItemVendor')
DROP PROCEDURE dbo.USP_DemandForecast_GetByItemVendor
GO
CREATE PROCEDURE [dbo].[USP_DemandForecast_GetByItemVendor]
	 @numItemCode AS NUMERIC(9),
	 @numWarehouseItemID AS NUMERIC(9),
     @numDomainID AS NUMERIC(9),
	 @numAnalysisDays AS FLOAT,
	 @numForecastDays AS FLOAT,
	 @numTotalQtySold AS FLOAT,
	 @numVendorID AS NUMERIC(18,0)
AS 
BEGIN
	SELECT
		*
	FROM
		dbo.GetItemQuantityToOrder(@numItemCode,@numWarehouseItemID,@numDomainID,@numAnalysisDays,@numForecastDays,@numTotalQtySold,@numVendorID)
END

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DemandForecast_Save')
DROP PROCEDURE dbo.USP_DemandForecast_Save
GO
CREATE PROCEDURE [dbo].[USP_DemandForecast_Save]
	@numDFID NUMERIC(18,0),
	@numDomainID NUMERIC (18,0),
	@UserContactID NUMERIC (18,0),
	@bitIncludeSalesOpp BIT,
	@vcItemClassification VARCHAR(500),
	@vcItemGroup VARCHAR(500),
	@vcWarehouse VARCHAR(500),
	@bitLastWeek BIT,
	@bitLastYear BIT,
	@numAnalysisPattern NUMERIC (18,0),
	@numForecastDays NUMERIC (18,0)
AS 
BEGIN
	DECLARE @numDemandForecastID NUMERIC(18,0) = 0


	IF (SELECT COUNT(*) FROM DemandForecast WHERE numDFID = @numDFID) = 0
	BEGIN
		-- INSERT
		INSERT INTO DemandForecast
		(
			numDomainID,
			numDFAPID,
			bitLastWeek,
			bitLastYear,
			numDFDaysID,
			bitIncludeSalesOpp,
			dtCreatedDate,
			numCreatedBy
		)
		VALUES
		(
			@numDomainID,
			@numAnalysisPattern,
			@bitLastWeek,
			@bitLastYear,
			@numForecastDays,
			@bitIncludeSalesOpp,
			GETDATE(),
			@UserContactID
		)

		SET @numDemandForecastID = SCOPE_IDENTITY()
	END
	ELSE
	BEGIN
		-- UPDATE
		UPDATE
			DemandForecast
		SET
			numDFAPID=@numAnalysisPattern,
			bitLastWeek=@bitLastWeek,
			bitLastYear=@bitLastYear,
			numDFDaysID=@numForecastDays,
			bitIncludeSalesOpp=@bitIncludeSalesOpp,
			dtModifiedDate = GETDATE(),
			numModifiedBy = @UserContactID
		WHERE
			numDFID = @numDFID

		SET @numDemandForecastID = @numDFID

		--Delete Existing Data
		DELETE FROM DemandForecastItemClassification WHERE numDFID = @numDemandForecastID
		DELETE FROM DemandForecastItemGroup WHERE numDFID = @numDemandForecastID
		DELETE FROM DemandForecastWarehouse WHERE numDFID = @numDemandForecastID
	END		
	

	IF LEN(ISNULL(@vcItemClassification,'')) > 0
	BEGIN
		INSERT INTO DemandForecastItemClassification
		(
			numDFID,
			numItemClassificationID
		)
		SELECT
			@numDemandForecastID,
			Split.a.value('.', 'VARCHAR(100)') AS String
		FROM  
		(
		SELECT 
				CAST ('<M>' + REPLACE(@vcItemClassification, ',', '</M><M>') + '</M>' AS XML) AS String  
		) AS A 
		CROSS APPLY 
			String.nodes ('/M') AS Split(a); 
	END

	IF LEN(ISNULL(@vcItemGroup,'')) > 0
	BEGIN
		INSERT INTO DemandForecastItemGroup
		(
			numDFID,
			numItemGroupID
		)
		SELECT
			@numDemandForecastID,
			Split.a.value('.', 'VARCHAR(100)') AS String
		FROM  
		(
		SELECT 
				CAST ('<M>' + REPLACE(@vcItemGroup, ',', '</M><M>') + '</M>' AS XML) AS String  
		) AS A 
		CROSS APPLY 
			String.nodes ('/M') AS Split(a); 
	END

	IF LEN(ISNULL(@vcWarehouse,'')) > 0
	BEGIN
		INSERT INTO DemandForecastWarehouse
		(
			numDFID,
			numWarehouseID
		)
		SELECT
			@numDemandForecastID,
			Split.a.value('.', 'VARCHAR(100)') AS String
		FROM  
		(
		SELECT 
				CAST ('<M>' + REPLACE(@vcWarehouse, ',', '</M><M>') + '</M>' AS XML) AS String  
		) AS A 
		CROSS APPLY 
			String.nodes ('/M') AS Split(a); 
	END

END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DemandForecast_UpdateInventory')
DROP PROCEDURE dbo.USP_DemandForecast_UpdateInventory
GO
CREATE PROCEDURE [dbo].[USP_DemandForecast_UpdateInventory]
	@numOppID NUMERIC (18,0),
	@numUserCntID NUMERIC(18,0)
AS 
BEGIN

	-- Update Inventory when order is created from demand forecast
	exec USP_UpdatingInventoryonCloseDeal @numOppID,@numUserCntID
	
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DemandForecastAnalysisPattern_GetAll')
DROP PROCEDURE dbo.USP_DemandForecastAnalysisPattern_GetAll
GO
CREATE PROCEDURE [dbo].[USP_DemandForecastAnalysisPattern_GetAll]
	@numDomainID NUMERIC (18,0)
AS 
BEGIN
    IF EXISTS (SELECT numDFAPID FROM DemandForecastAnalysisPattern WHERE ISNULL(numDomainID,0) = @numDomainID) 
		SELECT * FROM DemandForecastAnalysisPattern WHERE ISNULL(numDomainID,0) = @numDomainID
	ELSE
		SELECT * FROM DemandForecastAnalysisPattern WHERE ISNULL(numDomainID,0) = 0 
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DemandForecastDays_GetAll')
DROP PROCEDURE dbo.USP_DemandForecastDays_GetAll
GO
CREATE PROCEDURE [dbo].[USP_DemandForecastDays_GetAll]
	@numDomainID NUMERIC (18,0)
AS 
BEGIN
    IF EXISTS (SELECT numDFDaysID FROM DemandForecastDays WHERE ISNULL(numDomainID,0) = @numDomainID) 
		SELECT * FROM DemandForecastDays WHERE ISNULL(numDomainID,0) = @numDomainID
	ELSE
		SELECT * FROM DemandForecastDays WHERE ISNULL(numDomainID,0) = 0 
END
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Domain_GetDateFormat')
DROP PROCEDURE USP_Domain_GetDateFormat
GO
Create PROCEDURE [dbo].[USP_Domain_GetDateFormat]
	-- Add the parameters for the stored procedure here
	@numDomainId numeric(18,0)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	

	Select vcDateFormat,tintDecimalPoints from Domain where numDomainId=@numDomainId





END

GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Domain_UpdateShipPhoneNumber')
DROP PROCEDURE dbo.USP_Domain_UpdateShipPhoneNumber
GO
-- =============================================
-- Author:		<Author,,Sachin Sadhu>
-- Create date: <Create Date,,27thAugust2014>
-- Description:	<Description,, Update Shipping Phone Number in E-commerce API>
-- =============================================
Create PROCEDURE USP_Domain_UpdateShipPhoneNumber
	-- Add the parameters for the stored procedure here
	@numDomainID as numeric(9)=0,       
	@vcShipToPhoneNumber as VARCHAR(50)=''
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Update Domain                                       
	 SET     
		vcShipToPhoneNo = @vcShipToPhoneNumber
		WHERE   numDomainId=@numDomainID
END
Go
/****** Object:  StoredProcedure [dbo].[USP_EcommerceSettings]    Script Date: 03/25/2009 16:46:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj              
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_ecommercesettings')
DROP PROCEDURE usp_ecommercesettings
GO
CREATE PROCEDURE [dbo].[USP_EcommerceSettings]              
@numDomainID as numeric(9),              
@vcPaymentGateWay as int ,                
@vcPGWUserId as varchar(100)='' ,                
@vcPGWPassword as varchar(100),              
@bitShowInStock as bit,              
@bitShowQOnHand as bit,        
--@tintColumns as tinyint,      
--@numCategory as numeric(9),
--@bitHideNewUsers as bit,
@bitCheckCreditStatus as BIT,
@numDefaultWareHouseID as numeric(9),
@numDefaultIncomeChartAcntId as numeric(9),
@numDefaultAssetChartAcntId as numeric(9),
@numDefaultCOGSChartAcntId as numeric(9),
@numRelationshipId as numeric(9),
@numProfileId as numeric(9),
@bitEnableDefaultAccounts BIT,
--@bitEnableCreditCart	BIT,
@bitHidePriceBeforeLogin	BIT,
@bitHidePriceAfterLogin	BIT,
@numRelationshipIdHidePrice	numeric(18, 0),
@numProfileIDHidePrice	numeric(18, 0),
--@numBizDocForCreditCard numeric(18, 0),
--@numBizDocForCreditTerm numeric(18, 0),
@bitAuthOnlyCreditCard BIT=0,
@bitSendMail BIT = 1,
@vcGoogleMerchantID VARCHAR(1000)  = '',
@vcGoogleMerchantKey VARCHAR(1000) = '',
@IsSandbox BIT ,
@numAuthrizedOrderStatus NUMERIC,
@numSiteID numeric(18, 0),
@vcPaypalUserName VARCHAR(50)  = '',
@vcPaypalPassword VARCHAR(50)  = '',
@vcPaypalSignature VARCHAR(500)  = '',
@IsPaypalSandbox BIT,
@bitSkipStep2 BIT = 0,
@bitDisplayCategory BIT = 0,
@bitShowPriceUsingPriceLevel BIT = 0,
@bitEnableSecSorting BIT = 0,
@bitSortPriceMode   BIT=0,
@numPageSize    INT=15,
@numPageVariant  INT=6,
@bitAutoSelectWarehouse BIT =0 
as              
            
            
if not exists(select 'col1' from eCommerceDTL where numDomainID=@numDomainID AND [numSiteId] = @numSiteID)            
INSERT INTO eCommerceDTL
           (numDomainId,
            vcPaymentGateWay,
            vcPGWManagerUId,
            vcPGWManagerPassword,
            bitShowInStock,
            bitShowQOnHand,
            --tintItemColumns,
           -- numDefaultCategory,
            numDefaultWareHouseID,
            numDefaultIncomeChartAcntId,
            numDefaultAssetChartAcntId,
            numDefaultCOGSChartAcntId,
            --bitHideNewUsers,
            bitCheckCreditStatus,
            [numRelationshipId],
            [numProfileId],
            [bitEnableDefaultAccounts],
 --           [bitEnableCreditCart],
            [bitHidePriceBeforeLogin],
            [bitHidePriceAfterLogin],
            [numRelationshipIdHidePrice],
            [numProfileIDHidePrice],
--            numBizDocForCreditCard,
--            numBizDocForCreditTerm,
            bitAuthOnlyCreditCard,
--            numAuthrizedOrderStatus,
            bitSendEmail,
            vcGoogleMerchantID,
            vcGoogleMerchantKey,
            IsSandbox ,
            numAuthrizedOrderStatus,
			numSiteId,
			vcPaypalUserName,
			vcPaypalPassword,
			vcPaypalSignature,
			IsPaypalSandbox,
			bitSkipStep2,
			bitDisplayCategory,
			bitShowPriceUsingPriceLevel,
			bitEnableSecSorting,
			bitSortPriceMode,
			numPageSize,
			numPageVariant,
			bitAutoSelectWarehouse
			)

VALUES     (@numDomainID,
            @vcPaymentGateWay,
            @vcPGWUserId,
            @vcPGWPassword,
            @bitShowInStock,
            @bitShowQOnHand,
           -- @tintColumns,
           -- @numCategory,
            @numDefaultWareHouseID,
            @numDefaultIncomeChartAcntId,
            @numDefaultAssetChartAcntId,
            @numDefaultCOGSChartAcntId,
           -- @bitHideNewUsers,
            @bitCheckCreditStatus,
            @numRelationshipId,
            @numProfileId,
            @bitEnableDefaultAccounts,
 --           @bitEnableCreditCart,
            @bitHidePriceBeforeLogin,
            @bitHidePriceAfterLogin,
            @numRelationshipIdHidePrice,
            @numProfileIDHidePrice,
--            @numBizDocForCreditCard,
--            @numBizDocForCreditTerm,
            @bitAuthOnlyCreditCard,
--            @numAuthrizedOrderStatus,
            @bitSendMail,
            @vcGoogleMerchantID,
            @vcGoogleMerchantKey ,
            @IsSandbox,
            @numAuthrizedOrderStatus,
			@numSiteID,
			@vcPaypalUserName,
			@vcPaypalPassword,
			@vcPaypalSignature,
			@IsPaypalSandbox,
			@bitSkipStep2,
			@bitDisplayCategory,
			@bitShowPriceUsingPriceLevel,
			@bitEnableSecSorting,
			@bitSortPriceMode,
			@numPageSize,
			@numPageVariant,
			@bitAutoSelectWarehouse
            )
 else            
 update eCommerceDTL                                   
   set              
 vcPaymentGateWay=@vcPaymentGateWay,                
 vcPGWManagerUId=@vcPGWUserId ,                
 vcPGWManagerPassword= @vcPGWPassword,              
 bitShowInStock=@bitShowInStock ,               
 bitShowQOnHand=@bitShowQOnHand,        
 --tintItemColumns= @tintColumns,      
 --numDefaultCategory=@numCategory,
 numDefaultWareHouseID =@numDefaultWareHouseID,
 numDefaultIncomeChartAcntId =@numDefaultIncomeChartAcntId ,
 numDefaultAssetChartAcntId =@numDefaultAssetChartAcntId ,
 numDefaultCOGSChartAcntId =@numDefaultCOGSChartAcntId ,
 --bitHideNewUsers =@bitHideNewUsers,
 bitCheckCreditStatus=@bitCheckCreditStatus ,
 numRelationshipId = @numRelationshipId,
 numProfileId = @numProfileId,
 bitEnableDefaultAccounts = @bitEnableDefaultAccounts,
 --[bitEnableCreditCart]=@bitEnableCreditCart,
 [bitHidePriceBeforeLogin]=@bitHidePriceBeforeLogin,
 [bitHidePriceAfterLogin]=@bitHidePriceAfterLogin,
 [numRelationshipIdHidePrice]=@numRelationshipIdHidePrice,
 [numProfileIDHidePrice]=@numProfileIDHidePrice,
-- numBizDocForCreditCard=@numBizDocForCreditCard,
-- numBizDocForCreditTerm=@numBizDocForCreditTerm,
 bitAuthOnlyCreditCard=@bitAuthOnlyCreditCard,
-- numAuthrizedOrderStatus=@numAuthrizedOrderStatus,
 bitSendEmail = @bitSendMail,
 vcGoogleMerchantID = @vcGoogleMerchantID ,
 vcGoogleMerchantKey =  @vcGoogleMerchantKey ,
 IsSandbox = @IsSandbox,
 numAuthrizedOrderStatus = @numAuthrizedOrderStatus,
	numSiteId = @numSiteID,
vcPaypalUserName = @vcPaypalUserName ,
vcPaypalPassword = @vcPaypalPassword,
vcPaypalSignature = @vcPaypalSignature,
IsPaypalSandbox = @IsPaypalSandbox,
bitSkipStep2 = @bitSkipStep2,
bitDisplayCategory = @bitDisplayCategory,
bitShowPriceUsingPriceLevel = @bitShowPriceUsingPriceLevel,
bitEnableSecSorting = @bitEnableSecSorting,
bitSortPriceMode=@bitSortPriceMode,
numPageSize=@numPageSize,
numPageVariant=@numPageVariant,
bitAutoSelectWarehouse=@bitAutoSelectWarehouse
 where numDomainId=@numDomainID
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GET_IMPORT_MAPPED_DATA')
DROP PROCEDURE USP_GET_IMPORT_MAPPED_DATA
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- SELECT * FROM dbo.Import_File_Master
-- USP_GET_IMPORT_MAPPED_DATA 39,1,48
CREATE PROCEDURE [dbo].[USP_GET_IMPORT_MAPPED_DATA] 
(
	@intImportFileID	BIGINT,
	@numDomainID		NUMERIC(18,0),
	@numFormID			NUMERIC(18,0)
)
AS 
BEGIN
	DECLARE @strSQL AS NVARCHAR(MAX)
	DECLARE @strWHERE AS VARCHAR(MAX)
	
	SET @strWHERE = ' WHERE 1=1 '
	
	IF ISNULL(@intImportFileID,0) <> 0
		BEGIN
			SET @strWHERE = @strWHERE + ' AND PARENT.intImportFileID = ' + CONVERT(VARCHAR,@intImportFileID)
		END
		
		SET @strSQL = 'SELECT   PARENT.intImportFileID AS [ImportFileID],
			   					IMPORT.vcFormName AS [ImportType],
								PARENT.vcImportFileName AS [ImportFileName],
								(CASE 
									WHEN PARENT.tintStatus = 0 THEN ''Import Pending ''
									WHEN PARENT.tintStatus = 1 THEN ''Import Completed''
									WHEN PARENT.tintStatus = 2 THEN ''Rollback Pending''	
									WHEN PARENT.tintStatus = 3 THEN ''Rollback Incomplete''	
								END) AS [Status],								
								dbo.FormatedDateTimeFromDate(PARENT.dtCreateDate,numDomainId) AS [CreatedDate],
								dbo.fn_GetContactName(PARENT.numUserContactID) AS [CreatedBy] ,
								ISNULL(
										CASE WHEN ISNULL(tintStatus,0) = 1
											 THEN CASE WHEN ISNULL(numRecordAdded,0) <> 0 AND ISNULL(numRecordUpdated,0) <> 0 AND ISNULL(numErrors,0) = 0
													   THEN CAST(numRecordAdded AS VARCHAR) + '' records added. '' + CAST(numRecordUpdated AS VARCHAR)+ '' records updated.''	
													   WHEN ISNULL(numRecordAdded,0) <> 0 AND ISNULL(numRecordUpdated,0) = 0 AND ISNULL(numErrors,0) = 0
													   THEN CAST(numRecordAdded AS VARCHAR)+ '' records added.''	
													   WHEN ISNULL(numRecordAdded,0) = 0 AND ISNULL(numRecordUpdated,0) <> 0 AND ISNULL(numErrors,0) = 0
													   THEN CAST(numRecordUpdated AS VARCHAR)+ '' records updated.''	
													   WHEN ISNULL(numRecordAdded,0) = 0 AND ISNULL(numRecordUpdated,0) = 0 AND ISNULL(numErrors,0) = 0
													   THEN '' Wrong Data exists.''	
													   ELSE	CAST(numErrors AS VARCHAR) + '' errors occurred while importing data.''
												  END
											 END	  	   
								,''-'') AS [Records],
								PARENT.tintStatus AS [intStatus],
								PARENT.numMasterID AS [MasterID],
								ISNULL(HISTORY.vcHistory_Added_Value,'''') AS [ItemCodes],
								ISNULL(CASE WHEN (ISNULL(tintStatus,0) = 1 OR ISNULL(tintStatus,0) = 3)
											THEN 
											     CASE WHEN ISNULL(numRecordAdded,0) > 0 AND ISNULL(numRecordUpdated,0) = 0 THEN 1 
											          WHEN ISNULL(numErrors,0) >= 0  AND ISNULL(numRecordUpdated,0) = 0 THEN 1 
											          ELSE 0
												 END
									   END,0)	AS [IsForRollback]  	   
						FROM dbo.Import_File_Master PARENT 
						INNER JOIN dbo.DynamicFormMaster IMPORT ON PARENT.numMasterID = IMPORT.numFormId
						LEFT JOIN dbo.Import_History HISTORY ON HISTORY.intImportFileID = PARENT.intImportFileID '
						+ @strWHERE + ' AND PARENT.numDomainId = ' + CONVERT(VARCHAR,@numDomainID) +
						' ORDER BY PARENT.intImportFileID DESC'
		PRINT @strSQL
		EXEC SP_EXECUTESQL @strSQL
			
		-------------------------------------------------------------------------------------------------------------
		
		SELECT * FROM 
		(SELECT DISTINCT PARENT.numFieldID AS [FormFieldID],
						IFFM.intMapColumnNo AS [ImportFieldID],
						IFFM.intImportFileID,
						IFFM.intImportTransactionID,
						CASE WHEN ISNULL(bitCustomField,0) = 1
							 THEN CFW.Fld_label
							 ELSE PARENT.vcDbColumnName
						END AS [dbFieldName],	 
						CASE WHEN ISNULL(bitCustomField,0) = 1
							 THEN CFW.Fld_label
							 ELSE PARENT.vcFieldName 
						END AS [FormFieldName],	 				
						CASE WHEN CFW.fld_Type = 'Drop Down List Box'
							 THEN 'SelectBox'
							 ELSE ISNULL(PARENT.vcAssociatedControlType,'') 
						END	 vcAssociatedControlType,
						ISNULL(PARENT.bitDefault,0) bitDefault,
						ISNULL(PARENT.vcFieldType,'') vcFieldType,
						ISNULL(PARENT.vcListItemType,'') vcListItemType,
						ISNULL(PARENT.vcLookBackTableName,'') vcLookBackTableName,
						CASE WHEN ISNULL(bitCustomField,0) = 1
							 THEN CFW.Fld_label
							 ELSE PARENT.vcOrigDbColumnName
						END AS [vcOrigDbColumnName],
						ISNULL(PARENT.vcPropertyName,'') vcPropertyName,
						ISNULL(bitCustomField,0) AS [bitCustomField],
						PARENT.numDomainID AS [DomainID]
		FROM dbo.View_DynamicDefaultColumns PARENT  
		LEFT JOIN dbo.Import_File_Field_Mapping IFFM ON PARENT.numFieldID = IFFM.numFormFieldID    		
		LEFT JOIN dbo.CFW_Fld_Master CFW ON CFW.numDomainID = PARENT.numDomainID
		WHERE IFFM.intImportFileID = @intImportFileID
		  AND PARENT.numDomainID = @numDomainID
		  AND numFormID = @numFormID
	
		UNION ALL
		
		SELECT DISTINCT IFFM.numFormFieldID AS [FormFieldID],
						IFFM.intMapColumnNo AS [ImportFieldID],
						IFFM.intImportFileID,
						IFFM.intImportTransactionID,
						CFW.Fld_label AS [dbFieldName],	 
						CFW.Fld_label AS [FormFieldName],	 				
						CASE WHEN CFW.fld_Type = 'Drop Down List Box'
							 THEN 'SelectBox'
							 ELSE CFW.fld_Type
						END	 vcAssociatedControlType,
						0 AS bitDefault,
						'R' AS vcFieldType,
						'' AS vcListItemType,
						'' AS  vcLookBackTableName,
						CFW.Fld_label AS [vcOrigDbColumnName],
						'' AS vcPropertyName,
						1 AS [bitCustomField],
						0
						--IFM.numDomainID AS [DomainID]
		FROM dbo.Import_File_Field_Mapping IFFM 
		INNER JOIN dbo.CFW_Fld_Master CFW ON IFFM.numFormFieldID = CFW.Fld_id 
		INNER JOIN dbo.CFW_Loc_Master LOC ON CFW.Grp_id = LOC.Loc_id										 
		INNER JOIN dbo.DycFormSectionDetail DFSD ON DFSD.Loc_id = LOC.Loc_id
		WHERE 1=1
		  --AND DFSD.numDomainID = CFW.numDomainID
		  AND IFFM.intImportFileID = @intImportFileID
		  --AND IFM.numDomainID = @numDomainID 
		  AND DFSD.numFormID = @numFormID
		  AND Grp_id = DFSD.Loc_Id ) TABLE1
		ORDER BY intImportFileID DESC 
		
	/*
	SELECT * FROM Import_File_Field_Mapping		
	SELECT * FROM Import_File_Master
	SELECT * FROM View_DynamicDefaultColumns	
	SELECT * FROM CFW_Fld_Master
	*/
	/*
		SELECT DISTINCT PARENT.numFieldID AS [FormFieldID],
						IFFM.intMapColumnNo AS [ImportFieldID],
						IFFM.intImportFileID,
						IFFM.intImportTransactionID,
						CASE WHEN ISNULL(bitCustomField,0) = 1
							 THEN CFW.Fld_label
							 ELSE PARENT.vcDbColumnName
						END AS [dbFieldName],	 
						CASE WHEN ISNULL(bitCustomField,0) = 1
							 THEN CFW.Fld_label
							 ELSE PARENT.vcFieldName 
						END AS [FormFieldName],	 				
						CASE WHEN CFW.fld_Type = 'Drop Down List Box'
							 THEN 'SelectBox'
							 ELSE ISNULL(PARENT.vcAssociatedControlType,'') 
						END	 vcAssociatedControlType,
						ISNULL(PARENT.bitDefault,0) bitDefault,
						ISNULL(PARENT.vcFieldType,'') vcFieldType,
						ISNULL(PARENT.vcListItemType,'') vcListItemType,
						ISNULL(PARENT.vcLookBackTableName,'') vcLookBackTableName,
						CASE WHEN ISNULL(bitCustomField,0) = 1
							 THEN CFW.Fld_label
							 ELSE PARENT.vcOrigDbColumnName
						END AS [vcOrigDbColumnName],
						ISNULL(PARENT.vcPropertyName,'') vcPropertyName,
						ISNULL(bitCustomField,0) AS [bitCustomField],
						PARENT.numDomainID AS [DomainID] 
		FROM View_DynamicDefaultColumns PARENT
		LEFT JOIN dbo.Import_File_Field_Mapping IFFM ON PARENT.numFieldID = IFFM.numFormFieldID    		
		LEFT JOIN dbo.CFW_Fld_Master CFW ON CFW.Grp_id = PARENT.numFormId AND CFW.numDomainID = PARENT.numDomainID
		WHERE numFormID = 48 AND PARENT.numDomainID = 1 AND IFFM.intImportFileID = 2 */	
		
--		SELECT  CHILD.numFormFieldID AS [FormFieldID],
--				CHILD.intMapColumnNo AS [ImportFieldID],
--				CHILD.intImportFileID,
--				CHILD.intImportTransactionID,
--				CASE WHEN ISNULL(bitCustomField,0) = 1
--					 THEN CFW.Fld_label
--					 ELSE DYNAMIC_FIELD.vcDbColumnName
--				END AS [dbFieldName],	 
--				--DYNAMIC_FIELD.vcDbColumnName AS [dbFieldName],
--				CASE WHEN ISNULL(bitCustomField,0) = 1
--					 THEN CFW.Fld_label
--					 ELSE DYNAMIC_FIELD.vcFormFieldName
--				END AS [FormFieldName],	 
--				--DYNAMIC_FIELD.vcFormFieldName AS [FormFieldName],
--				CASE WHEN CFW.fld_Type = 'Drop Down List Box'
--					 THEN 'SelectBox'
--					 ELSE ISNULL(DYNAMIC_FIELD.vcAssociatedControlType,'') 
--				END	 vcAssociatedControlType,
--				ISNULL(DYNAMIC_FIELD.bitDefault,0) bitDefault,
--				ISNULL(DYNAMIC_FIELD.vcFieldType,'') vcFieldType,
--				ISNULL(DYNAMIC_FIELD.vcListItemType,'') vcListItemType,
--				ISNULL(DYNAMIC_FIELD.vcLookBackTableName,'') vcLookBackTableName,
--				CASE WHEN ISNULL(bitCustomField,0) = 1
--					 THEN CFW.Fld_label
--					 ELSE DYNAMIC_FIELD.vcOrigDbColumnName
--				END AS [vcOrigDbColumnName],
--				ISNULL(DYNAMIC_FIELD.vcPropertyName,'') vcPropertyName,
--				ISNULL(bitCustomField,0) AS [bitCustomField],
--			    PARENT.numDomainID AS [DomainID]
--		FROM Import_File_Field_Mapping CHILD 
--		INNER JOIN dbo.Import_File_Master PARENT ON PARENT.intImportFileID = CHILD.intImportFileID
--		INNER JOIN dbo.Import_Master IMPORT ON IMPORT.intImportMasterID = PARENT.numMasterID
--		LEFT JOIN DynamicFormFieldMaster DYNAMIC_FIELD ON CHILD.numFormFieldID = DYNAMIC_FIELD.numFormFieldId 
--		LEFT JOIN dbo.DycFormField_Mapping DYNAMIC_FIELD_CHILD ON CHILD.numFormFieldID = DYNAMIC_FIELD_CHILD.numFieldId 
--		INNER JOIN dbo.DycFieldMaster DYNAMIC_FIELD ON DYNAMIC_FIELD_CHILD.= DYNAMIC_FIELD
--		LEFT JOIN dbo.CFW_Fld_Master CFW ON CHILD.numFormFieldID = CFW.Fld_id
--		WHERE CHILD.intImportFileID = @intImportFileID

END		
/*
-- Added Support for Bills in AP Aging summary,by chintan
*/

-- [dbo].[USP_GetAccountPayableAging] 1,null 6719
/*
-- Added Support for Bills in AP Aging summary,by chintan
*/

-- [dbo].[USP_GetAccountPayableAging] 1,null 6719
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getaccountpayableaging')
DROP PROCEDURE usp_getaccountpayableaging
GO
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

CREATE PROCEDURE [dbo].[USP_GetAccountPayableAging]
    (
      @numDomainId AS NUMERIC(9) = 0,
      @numDivisionId AS NUMERIC(9) = NULL,
      @numAccountClass AS NUMERIC(9)=0,
	  @dtFromDate AS DATETIME,
	  @dtToDate AS DATETIME
    )
AS 
    BEGIN 
  
        CREATE TABLE #TempAPRecord
            (
              [ID] [int] IDENTITY(1, 1)
                         NOT NULL,
              [numUserCntID] [numeric](18, 0) NOT NULL,
              [numOppId] [numeric](18, 0) NULL,
              [numDivisionId] [numeric](18, 0) NULL,
              [numOppBizDocsId] [numeric](18, 0) NULL,
              [DealAmount] [float] NULL,
              [numBizDocId] [numeric](18, 0) NULL,
              dtDueDate DATETIME NULL,
              [numCurrencyID] [numeric](18, 0) NULL,AmountPaid FLOAT NULL,monUnAppliedAmount MONEY NULL 
            ) ;
  
  
        CREATE TABLE #TempAPRecord1
            (
              [numID] [numeric](18, 0) IDENTITY(1, 1)
                                       NOT NULL,
              [numUserCntID] [numeric](18, 0) NOT NULL,
              [numDivisionId] [numeric](18, 0) NULL,
              [tintCRMType] [tinyint] NULL,
              [vcCompanyName] [varchar](200) NULL,
              [vcCustPhone] [varchar](50) NULL,
              [numContactId] [numeric](18, 0) NULL,
              [vcEmail] [varchar](100) NULL,
              [numPhone] [varchar](50) NULL,
              [vcContactName] [varchar](100) NULL,
              [numThirtyDays] [numeric](18, 2) NULL,
              [numSixtyDays] [numeric](18, 2) NULL,
              [numNinetyDays] [numeric](18, 2) NULL,
              [numOverNinetyDays] [numeric](18, 2) NULL,
              [numThirtyDaysOverDue] [numeric](18, 2) NULL,
              [numSixtyDaysOverDue] [numeric](18, 2) NULL,
              [numNinetyDaysOverDue] [numeric](18, 2) NULL,
              [numOverNinetyDaysOverDue] [numeric](18, 2) NULL,
              [numCompanyID] [numeric](18, 2) NULL,
              [numDomainID] [numeric](18, 2) NULL,
              [intThirtyDaysCount] [int] NULL,
              [intThirtyDaysOverDueCount] [int] NULL,
              [intSixtyDaysCount] [int] NULL,
              [intSixtyDaysOverDueCount] [int] NULL,
              [intNinetyDaysCount] [int] NULL,
              [intOverNinetyDaysCount] [int] NULL,
              [intNinetyDaysOverDueCount] [int] NULL,
              [intOverNinetyDaysOverDueCount] [int] NULL,
              [numThirtyDaysPaid] [numeric](18, 2) NULL,
              [numSixtyDaysPaid] [numeric](18, 2) NULL,
              [numNinetyDaysPaid] [numeric](18, 2) NULL,
              [numOverNinetyDaysPaid] [numeric](18, 2) NULL,
              [numThirtyDaysOverDuePaid] [numeric](18, 2) NULL,
              [numSixtyDaysOverDuePaid] [numeric](18, 2) NULL,
              [numNinetyDaysOverDuePaid] [numeric](18, 2) NULL,
              [numOverNinetyDaysOverDuePaid] [numeric](18, 2) NULL
              ,monUnAppliedAmount MONEY NULL,numTotal MONEY NULL 
            ) ;
  
  
  
        DECLARE @AuthoritativePurchaseBizDocId AS INTEGER
        SELECT  @AuthoritativePurchaseBizDocId = isnull(numAuthoritativePurchase,
                                                        0)
        FROM    AuthoritativeBizDocs
        WHERE   numDomainId = @numDomainId ;
        
     
        INSERT  INTO [#TempAPRecord]
                (
                  numUserCntID,
                  [numOppId],
                  [numDivisionId],
                  [numOppBizDocsId],
                  [DealAmount],
                  [numBizDocId],
                  dtDueDate,
                  [numCurrencyID],AmountPaid
                )
                SELECT  @numDomainId,
                        OB.numOppId,
                        Opp.numDivisionId,
                        OB.numOppBizDocsId,
                        ISNULL(OB.monDealAmount --dbo.GetDealAmount(OB.numOppID,getutcdate(),OB.numOppBizDocsId)
                               * ISNULL(Opp.fltExchangeRate, 1), 0)
                        - ISNULL(OB.monAmountPaid
                                 * ISNULL(Opp.fltExchangeRate, 1), 0) DealAmount,
                        OB.numBizDocId,
                        DATEADD(DAY,
                                CASE WHEN Opp.bitBillingTerms = 1
                                     --THEN convert(int, ISNULL(dbo.fn_GetListItemName(isnull(Opp.intBillingDays, 0)), 0))
                                     THEN convert(int, ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(Opp.intBillingDays,0)), 0))
                                     ELSE 0
                                END, dtFromDate) AS dtDueDate,
                        ISNULL(Opp.numCurrencyID, 0) numCurrencyID,
                        ISNULL(OB.monAmountPaid * ISNULL(Opp.fltExchangeRate, 1), 0) AmountPaid
                FROM    OpportunityMaster Opp
                        JOIN OpportunityBizDocs OB ON OB.numOppId = Opp.numOppID
                        LEFT OUTER JOIN [Currency] C ON Opp.[numCurrencyID] = C.[numCurrencyID]
                WHERE   tintOpptype = 2
                        AND tintoppStatus = 1
                --AND dtShippedDate IS NULL
                        AND opp.numDomainID = @numDomainId
                        AND OB.bitAuthoritativeBizDocs = 1
                        AND ISNULL(OB.tintDeferred, 0) <> 1
                        AND numBizDocId = @AuthoritativePurchaseBizDocId
                        AND ( Opp.numDivisionId = @numDivisionId
                              OR @numDivisionId IS NULL
                            )
                        AND (Opp.numAccountClass=@numAccountClass OR @numAccountClass=0)
						AND OB.[dtCreatedDate] BETWEEN @dtFromDate AND @dtToDate
                GROUP BY OB.numOppId,
                        Opp.numDivisionId,
                        OB.numOppBizDocsId,
                        Opp.fltExchangeRate,
                        OB.numBizDocId,
                        OB.dtCreatedDate,
                        Opp.bitBillingTerms,
                        Opp.intBillingDays,
                        OB.monDealAmount,
                        Opp.numCurrencyID,
                        OB.[dtFromDate],OB.monAmountPaid
                HAVING  ( ISNULL(OB.monDealAmount * ISNULL(Opp.fltExchangeRate,
                                                           1), 0)
                          - ISNULL(OB.monAmountPaid
                                   * ISNULL(Opp.fltExchangeRate, 1), 0) ) > 0
                UNION ALL 
		--Add Bill Amount
                SELECT  @numDomainId,
                        0 numOppId,
                        BH.numDivisionId,
                        0 numBizDocsId,
                        ISNULL(SUM(BH.monAmountDue), 0) - ISNULL(SUM(BH.monAmtPaid), 0) DealAmount,
                        0 numBizDocId,
                        BH.dtDueDate AS dtDueDate,
                        0 numCurrencyID,ISNULL(SUM(BH.monAmtPaid), 0) AS AmountPaid
                FROM    BillHeader BH
                WHERE   BH.numDomainID = @numDomainId
                        AND ( BH.numDivisionId = @numDivisionId
                              OR @numDivisionId IS NULL
                            )
                         AND ISNULL(BH.monAmountDue, 0) - ISNULL(BH.monAmtPaid, 0) > 0   
                         AND (BH.numAccountClass=@numAccountClass OR @numAccountClass=0)
						 AND BH.[dtBillDate] BETWEEN @dtFromDate AND @dtToDate
                GROUP BY BH.numDivisionId,
                        BH.dtDueDate
                HAVING  ISNULL(SUM(BH.monAmountDue), 0) - ISNULL(SUM(BH.monAmtPaid), 0) > 0
                UNION ALL
		--Add Write Check entry (As Amount Paid)
                SELECT  @numDomainId,
                        0 numOppId,
                        CD.numCustomerId as numDivisionId,
                        0 numBizDocsId,
                        0 DealAmount,
                        0 numBizDocId,
                        CH.dtCheckDate AS dtDueDate,
                        0 numCurrencyID,ISNULL(SUM(CD.monAmount), 0) AS AmountPaid
                FROM    CheckHeader CH
						JOIN  dbo.CheckDetails CD ON CH.numCheckHeaderID=CD.numCheckHeaderID
                        INNER JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = CD.numChartAcntId
                WHERE   CH.numDomainID = @numDomainId AND CH.tintReferenceType=1
						AND COA.vcAccountCode LIKE '01020102%'
                        AND ( CD.numCustomerId = @numDivisionId
                              OR @numDivisionId IS NULL
                            )
                        AND (CH.numAccountClass=@numAccountClass OR @numAccountClass=0)
						AND CH.[dtCheckDate] BETWEEN @dtFromDate AND @dtToDate
                GROUP BY CD.numCustomerId,
                        CH.dtCheckDate
		--Show Impact of AP journal entries in report as well 
                UNION ALL
                SELECT  @numDomainId,
                        0,
                        GJD.numCustomerId,
                        0,
                        CASE WHEN GJD.numDebitAmt > 0 THEN -1 * GJD.numDebitAmt
                             ELSE GJD.numCreditAmt
                        END,
                        0,
                        GJH.datEntry_Date,
                        GJD.[numCurrencyID],0 AS AmountPaid
                FROM    dbo.General_Journal_Header GJH
                        INNER JOIN dbo.General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId
                                                                      AND GJH.numDomainId = GJD.numDomainId
                        INNER JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = GJD.numChartAcntId
                WHERE   GJH.numDomainId = @numDomainId 
                        AND COA.vcAccountCode LIKE '01020102%'
                        AND ISNULL(numOppId, 0) = 0
                        AND ISNULL(numOppBizDocsId, 0) = 0
                        AND ISNULL(GJH.numBillID,0)=0 AND ISNULL(GJH.numBillPaymentID,0)=0 AND ISNULL(GJH.numCheckHeaderID,0)=0 AND ISNULL(GJH.numReturnID,0)=0
                        AND ISNULL(GJH.numPayrollDetailID,0)=0
                         AND ( GJD.numCustomerId = @numDivisionId
                              OR @numDivisionId IS NULL
                            )
                            AND (GJH.numAccountClass=@numAccountClass OR @numAccountClass=0)
						AND [GJH].[datEntry_Date]  BETWEEN @dtFromDate AND @dtToDate
                UNION ALL
	--Add Commission Amount
                SELECT  @numDomainId,
                        OBD.numOppId numOppId,
                        D.numDivisionId,
                        BDC.numOppBizDocId numBizDocsId,
                       isnull(sum(BDC.numComissionAmount - ISNULL(PT.monCommissionAmt,0)),0)  DealAmount,
                        0 numBizDocId,
                        OBD.dtCreatedDate AS dtDueDate,
                        ISNULL(OPP.numCurrencyID, 0) numCurrencyID,0 AS AmountPaid
                FROM    BizDocComission BDC
                        JOIN OpportunityBizDocs OBD ON BDC.numOppBizDocId = OBD.numOppBizDocsId
                        join OpportunityMaster Opp on OPP.numOppId = OBD.numOppId
                        JOIN Domain D on D.numDomainID = BDC.numDomainID
                        LEFT JOIN dbo.PayrollTracking PT ON BDC.numComissionID=PT.numComissionID
                WHERE   OPP.numDomainID = @numDomainId
                        and BDC.numDomainID = @numDomainId
                        AND ( D.numDivisionId = @numDivisionId
                              OR @numDivisionId IS NULL
                            )
                            AND (Opp.numAccountClass=@numAccountClass OR @numAccountClass=0)
						AND OBD.[dtCreatedDate]  BETWEEN @dtFromDate AND @dtToDate
				--AND BDC.bitCommisionPaid=0
                        AND OBD.bitAuthoritativeBizDocs = 1
                GROUP BY D.numDivisionId,
                        OBD.numOppId,
                        BDC.numOppBizDocId,
                        OPP.numCurrencyID,
                        BDC.numComissionAmount,
                        OBD.dtCreatedDate
                HAVING  ISNULL(BDC.numComissionAmount, 0) > 0
			UNION ALL --Standalone Refund against Account Receivable
			 SELECT @numDomainId,
					0 AS numOppID,
					RH.numDivisionId,
					0,
					CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt * -1 ELSE GJD.numCreditAmt END DealAmount,
					0 numBizDocId,
					GJH.datEntry_Date,GJD.[numCurrencyID],0 AS AmountPaid
			 FROM   dbo.ReturnHeader RH JOIN dbo.General_Journal_Header GJH ON RH.numReturnHeaderID = GJH.numReturnID
			JOIN General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId 
			INNER JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = GJD.numChartAcntId
			WHERE RH.tintReturnType=4 AND RH.numDomainId=@numDomainId  AND COA.vcAccountCode LIKE '01020102%'
					AND (RH.numDivisionId = @numDivisionId OR @numDivisionId IS NULL)
					AND (monBizDocAmount > 0) AND ISNULL(RH.numBillPaymentIDRef,0)>0
					AND (RH.numAccountClass=@numAccountClass OR @numAccountClass=0)
					AND RH.[dtCreatedDate]  BETWEEN @dtFromDate AND @dtToDate
					--GROUP BY DM.numDivisionId,DM.dtDepositDate
		
		
---SELECT  SUM(monUnAppliedAmount) FROM    [#TempAPRecord]

   INSERT  INTO [#TempAPRecord]
        (
          numUserCntID,
          [numOppId],
          [numDivisionId],
          [numOppBizDocsId],
          [DealAmount],
          [numBizDocId],
          [numCurrencyID],
          [dtDueDate],AmountPaid,monUnAppliedAmount
        )
SELECT @numDomainId,0, BPH.numDivisionId,0,0,0,0,NULL,0,sum(ISNULL(monPaymentAmount,0) - ISNULL(monAppliedAmount,0))       
 FROM BillPaymentHeader BPH
		WHERE BPH.numDomainId=@numDomainId   
		AND (BPH.numDivisionId = @numDivisionId
                            OR @numDivisionId IS NULL)
		AND (ISNULL(monPaymentAmount,0) - ISNULL(monAppliedAmount,0)) > 0
		AND (BPH.numAccountClass=@numAccountClass OR @numAccountClass=0)
		AND [BPH].[dtPaymentDate]  BETWEEN @dtFromDate AND @dtToDate
		GROUP BY BPH.numDivisionId
		

--SELECT  GETDATE()
        INSERT  INTO [#TempAPRecord1]
                (
                  [numUserCntID],
                  [numDivisionId],
                  [tintCRMType],
                  [vcCompanyName],
                  [vcCustPhone],
                  [numContactId],
                  [vcEmail],
                  [numPhone],
                  [vcContactName],
                  [numThirtyDays],
                  [numSixtyDays],
                  [numNinetyDays],
                  [numOverNinetyDays],
                  [numThirtyDaysOverDue],
                  [numSixtyDaysOverDue],
                  [numNinetyDaysOverDue],
                  [numOverNinetyDaysOverDue],
                  numCompanyID,
                  numDomainID,
                  [intThirtyDaysCount],
                  [intThirtyDaysOverDueCount],
                  [intSixtyDaysCount],
                  [intSixtyDaysOverDueCount],
                  [intNinetyDaysCount],
                  [intOverNinetyDaysCount],
                  [intNinetyDaysOverDueCount],
                  [intOverNinetyDaysOverDueCount],
                  [numThirtyDaysPaid],
                  [numSixtyDaysPaid],
                  [numNinetyDaysPaid],
                  [numOverNinetyDaysPaid],
                  [numThirtyDaysOverDuePaid],
                  [numSixtyDaysOverDuePaid],
                  [numNinetyDaysOverDuePaid],
                  [numOverNinetyDaysOverDuePaid],monUnAppliedAmount
                )
                SELECT DISTINCT
                        @numDomainId,
                        Div.numDivisionID,
                        tintCRMType AS tintCRMType,
                        vcCompanyName AS vcCompanyName,
                        '',
                        NULL,
                        '',
                        '',
                        '',
                        0 numThirtyDays,
                        0 numSixtyDays,
                        0 numNinetyDays,
                        0 numOverNinetyDays,
                        0 numThirtyDaysOverDue,
                        0 numSixtyDaysOverDue,
                        0 numNinetyDaysOverDue,
                        0 numOverNinetyDaysOverDue,
                        C.numCompanyID,
                        Div.numDomainID,
                        0 [intThirtyDaysCount],
                        0 [intThirtyDaysOverDueCount],
                        0 [intSixtyDaysCount],
                        0 [intSixtyDaysOverDueCount],
                        0 [intNinetyDaysCount],
                        0 [intOverNinetyDaysCount],
                        0 [intNinetyDaysOverDueCount],
                        0 [intOverNinetyDaysOverDueCount],
                         0 numThirtyDaysPaid,
                        0 numSixtyDaysPaid,
                        0 numNinetyDaysPaid,
                        0 numOverNinetyDaysPaid,
                        0 numThirtyDaysOverDuePaid,
                        0 numSixtyDaysOverDuePaid,
                        0 numNinetyDaysOverDuePaid,
                        0 numOverNinetyDaysOverDuePaid,0 monUnAppliedAmount
                FROM    Divisionmaster Div
                        INNER JOIN CompanyInfo C ON C.numCompanyId = Div.numCompanyID
                    --JOIN [#TempAPRecord] t ON Div.[numDivisionID] = t.[numDivisionId]
                WHERE   Div.[numDomainID] = @numDomainId
                        AND Div.[numDivisionID] IN ( SELECT DISTINCT
                                                            [numDivisionID]
                                                     FROM   [#TempAPRecord] )



--Update Custome Phone 
        UPDATE  #TempAPRecord1
        SET     [vcCustPhone] = X.[vcCustPhone]
        FROM    ( SELECT    CASE WHEN numPhone IS NULL THEN ''
                                 WHEN numPhone = '' THEN ''
                                 ELSE ', ' + numPhone
                            END AS vcCustPhone,
                            numDivisionId
                  FROM      AdditionalContactsInformation
                  WHERE     bitPrimaryContact = 1
                            AND numDivisionId IN (
                            SELECT DISTINCT
                                    numDivisionID
                            FROM    [#TempAPRecord1]
                            WHERE   numUserCntID = @numDomainId )
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID

--Update Customer info 
        UPDATE  #TempAPRecord1
        SET     [numContactId] = X.[numContactID],
                [vcEmail] = X.[vcEmail],
                [vcContactName] = X.[vcContactName],
                numPhone = X.numPhone
        FROM    ( SELECT  numContactId AS numContactID,
                            ISNULL(vcEmail, '') AS vcEmail,
                            ISNULL(vcFirstname, '') + ' ' + ISNULL(vcLastName, '') AS vcContactName,
                            CAST(ISNULL(CASE WHEN numPhone IS NULL THEN ''
                                             WHEN numPhone = '' THEN ''
                                             ELSE ', ' + numPhone
                                        END, '') AS VARCHAR) AS numPhone,
                            [numDivisionId]
                  FROM      AdditionalContactsInformation
                  WHERE     (vcPosition = 843 or isnull(bitPrimaryContact,0)=1)
                            AND numDivisionId IN (
                            SELECT DISTINCT
                                    numDivisionID
                            FROM    [#TempAPRecord1]
                            WHERE   numUserCntID = @numDomainId )
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID

                    
--SELECT  GETDATE()
------------------------------------------      
        DECLARE @baseCurrency AS NUMERIC
        SELECT  @baseCurrency = numCurrencyID
        FROM    dbo.Domain
        WHERE   numDomainId = @numDomainId
/*when any Order is placed in Foreign currency (other than base currency)then show it in purple color */

------------------------------------------
        UPDATE  #TempAPRecord1
        SET     numThirtyDays = X.numThirtyDays
        FROM    ( SELECT    SUM(DealAmount) AS numThirtyDays,
                            [numDivisionId]
                  FROM      [#TempAPRecord]
                  WHERE     numUserCntID = @numDomainId
                            AND DATEDIFF(DAY, dbo.GetUTCDateWithoutTime(),
                                         [dtDueDate]) BETWEEN 0
                                                      AND     30
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
         UPDATE  #TempAPRecord1
        SET     numThirtyDaysPaid = X.numThirtyDaysPaid
        FROM    ( SELECT    SUM(AmountPaid) AS numThirtyDaysPaid,
                            [numDivisionId]
                  FROM      [#TempAPRecord]
                  WHERE     numUserCntID = @numDomainId
                            AND DATEDIFF(DAY, dbo.GetUTCDateWithoutTime(),
                                         [dtDueDate]) BETWEEN 0
                                                      AND     30
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
        UPDATE  #TempAPRecord1
        SET     intThirtyDaysCount = 2
        WHERE   numDivisionId IN (
                SELECT DISTINCT
                        [numDivisionId]
                FROM    [#TempAPRecord]
                WHERE   numUserCntID = @numDomainId
                        AND DATEDIFF(DAY, dbo.GetUTCDateWithoutTime(),
                                     [dtDueDate]) BETWEEN 0
                                                  AND     30
                        AND numCurrencyID <> @baseCurrency )
------------------------------------------
        UPDATE  #TempAPRecord1
        SET     numSixtyDays = X.numSixtyDays
        FROM    ( SELECT    SUM(DealAmount) AS numSixtyDays,
                            numDivisionId
                  FROM      [#TempAPRecord]
                  WHERE     numUserCntID = @numDomainId
                            AND DATEDIFF(DAY, dbo.GetUTCDateWithoutTime(),
                                         [dtDueDate]) BETWEEN 31
                                                      AND     60
                            AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
        UPDATE  #TempAPRecord1
        SET     numSixtyDaysPaid = X.numSixtyDaysPaid
        FROM    ( SELECT    SUM(AmountPaid) AS numSixtyDaysPaid,
                            numDivisionId
                  FROM      [#TempAPRecord]
                  WHERE     numUserCntID = @numDomainId
                            AND DATEDIFF(DAY, dbo.GetUTCDateWithoutTime(),
                                         [dtDueDate]) BETWEEN 31
                                                      AND     60
                            AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
        UPDATE  #TempAPRecord1
        SET     intSixtyDaysCount = 2
        WHERE   numDivisionId IN (
                SELECT DISTINCT
                        [numDivisionId]
                FROM    [#TempAPRecord]
                WHERE   numUserCntID = @numDomainId
                        AND DATEDIFF(DAY, dbo.GetUTCDateWithoutTime(),
                                     [dtDueDate]) BETWEEN 31
                                                  AND     60
                        AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
                        AND numCurrencyID <> @baseCurrency )

------------------------------------------
        UPDATE  #TempAPRecord1
        SET     numNinetyDays = X.numNinetyDays
        FROM    ( SELECT    SUM(DealAmount) AS numNinetyDays,
                            numDivisionId
                  FROM      #TempAPRecord
                  WHERE     numUserCntID = @numDomainId
                            AND DATEDIFF(DAY, dbo.GetUTCDateWithoutTime(),
                                         [dtDueDate]) BETWEEN 61
                                                      AND     90
                            AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
         UPDATE  #TempAPRecord1
        SET     numNinetyDaysPaid = X.numNinetyDaysPaid
        FROM    ( SELECT    SUM(AmountPaid) AS numNinetyDaysPaid,
                            numDivisionId
                  FROM      #TempAPRecord
                  WHERE     numUserCntID = @numDomainId
                            AND DATEDIFF(DAY, dbo.GetUTCDateWithoutTime(),
                                         [dtDueDate]) BETWEEN 61
                                                      AND     90
                            AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
        UPDATE  #TempAPRecord1
        SET     intNinetyDaysCount = 2
        WHERE   numDivisionId IN (
                SELECT DISTINCT
                        [numDivisionId]
                FROM    [#TempAPRecord]
                WHERE   numUserCntID = @numDomainId
                        AND DATEDIFF(DAY, dbo.GetUTCDateWithoutTime(),
                                     [dtDueDate]) BETWEEN 61
                                                  AND     90
                        AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
                        AND numCurrencyID <> @baseCurrency )

------------------------------------------
        UPDATE  #TempAPRecord1
        SET     numOverNinetyDays = X.numOverNinetyDays
        FROM    ( SELECT    SUM(DealAmount) AS numOverNinetyDays,
                            numDivisionId
                  FROM      #TempAPRecord
                  WHERE     numUserCntID = @numDomainId
                            AND [dtDueDate] >= DATEADD(DAY, 91,
                                                       dbo.GetUTCDateWithoutTime())
                            AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
        UPDATE  #TempAPRecord1
        SET     numOverNinetyDaysPaid = X.numOverNinetyDaysPaid
        FROM    ( SELECT    SUM(AmountPaid) AS numOverNinetyDaysPaid,
                            numDivisionId
                  FROM      #TempAPRecord
                  WHERE     numUserCntID = @numDomainId
                            AND [dtDueDate] >= DATEADD(DAY, 91,
                                                       dbo.GetUTCDateWithoutTime())
                            AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
        UPDATE  #TempAPRecord1
        SET     intOverNinetyDaysCount = 2
        WHERE   numDivisionId IN (
                SELECT DISTINCT
                        [numDivisionId]
                FROM    [#TempAPRecord]
                WHERE   numUserCntID = @numDomainId
                        AND [dtDueDate] >= DATEADD(DAY, 91,
                                                   dbo.GetUTCDateWithoutTime())
                        AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
                        AND numCurrencyID <> @baseCurrency )

------------------------------------------
        UPDATE  #TempAPRecord1
        SET     numThirtyDaysOverDue = X.numThirtyDaysOverDue
        FROM    ( SELECT    SUM(DealAmount) AS numThirtyDaysOverDue,
                            numDivisionId
                  FROM      #TempAPRecord
                  WHERE     numUserCntID = @numDomainId
                            AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
                            AND DATEDIFF(DAY, [dtDueDate],
                                         dbo.GetUTCDateWithoutTime()) BETWEEN 0
                                                                      AND     30
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
         UPDATE  #TempAPRecord1
        SET     numThirtyDaysOverDuePaid = X.numThirtyDaysOverDuePaid
        FROM    ( SELECT    SUM(AmountPaid) AS numThirtyDaysOverDuePaid,
                            numDivisionId
                  FROM      #TempAPRecord
                  WHERE     numUserCntID = @numDomainId
                            AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
                            AND DATEDIFF(DAY, [dtDueDate],
                                         dbo.GetUTCDateWithoutTime()) BETWEEN 0
                                                                      AND     30
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
        UPDATE  #TempAPRecord1
        SET     intThirtyDaysOverDueCount = 2
        WHERE   numDivisionId IN (
                SELECT DISTINCT
                        [numDivisionId]
                FROM    [#TempAPRecord]
                WHERE   numUserCntID = @numDomainId
                        AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
                        AND DATEDIFF(DAY, [dtDueDate],
                                     dbo.GetUTCDateWithoutTime()) BETWEEN 0
                                                                  AND     30
                        AND numCurrencyID <> @baseCurrency )

----------------------------------
        UPDATE  #TempAPRecord1
        SET     numSixtyDaysOverDue = X.numSixtyDaysOverDue
        FROM    ( SELECT    SUM(DealAmount) AS numSixtyDaysOverDue,
                            numDivisionId
                  FROM      #TempAPRecord
                  WHERE     numUserCntID = @numDomainId
                            AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
                            AND DATEDIFF(DAY, [dtDueDate],
                                         dbo.GetUTCDateWithoutTime()) BETWEEN 31
                                                                      AND     60
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
          UPDATE  #TempAPRecord1
        SET     numSixtyDaysOverDuePaid = X.numSixtyDaysOverDuePaid
        FROM    ( SELECT    SUM(AmountPaid) AS numSixtyDaysOverDuePaid,
                            numDivisionId
                  FROM      #TempAPRecord
                  WHERE     numUserCntID = @numDomainId
                            AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
                            AND DATEDIFF(DAY, [dtDueDate],
                                         dbo.GetUTCDateWithoutTime()) BETWEEN 31
                                                                      AND     60
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
        UPDATE  #TempAPRecord1
        SET     intSixtyDaysOverDueCount = 2
        WHERE   numDivisionId IN (
                SELECT DISTINCT
                        [numDivisionId]
                FROM    [#TempAPRecord]
                WHERE   numUserCntID = @numDomainId
                        AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
                        AND DATEDIFF(DAY, [dtDueDate],
                                     dbo.GetUTCDateWithoutTime()) BETWEEN 31
                                                                  AND     60
                        AND numCurrencyID <> @baseCurrency )
------------------------------------------
        UPDATE  #TempAPRecord1
        SET     numNinetyDaysOverDue = X.numNinetyDaysOverDue
        FROM    ( SELECT    SUM(DealAmount) AS numNinetyDaysOverDue,
                            numDivisionId
                  FROM      #TempAPRecord
                  WHERE     numUserCntID = @numDomainId
                            AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
                            AND DATEDIFF(DAY, [dtDueDate],
                                         dbo.GetUTCDateWithoutTime()) BETWEEN 61
                                                                      AND     90
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
         UPDATE  #TempAPRecord1
        SET     numNinetyDaysOverDuePaid = X.numNinetyDaysOverDuePaid
        FROM    ( SELECT    SUM(AmountPaid) AS numNinetyDaysOverDuePaid,
                            numDivisionId
                  FROM      #TempAPRecord
                  WHERE     numUserCntID = @numDomainId
                            AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
                            AND DATEDIFF(DAY, [dtDueDate],
                                         dbo.GetUTCDateWithoutTime()) BETWEEN 61
                                                                      AND     90
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
        UPDATE  #TempAPRecord1
        SET     intNinetyDaysOverDueCount = 2
        WHERE   numDivisionId IN (
                SELECT DISTINCT
                        [numDivisionId]
                FROM    [#TempAPRecord]
                WHERE   numUserCntID = @numDomainId
                        AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
                        AND DATEDIFF(DAY, [dtDueDate],
                                     dbo.GetUTCDateWithoutTime()) BETWEEN 61
                                                                  AND     90
                        AND numCurrencyID <> @baseCurrency )
------------------------------------------
        UPDATE  #TempAPRecord1
        SET     numOverNinetyDaysOverDue = X.numOverNinetyDaysOverDue
        FROM    ( SELECT    SUM(DealAmount) AS numOverNinetyDaysOverDue,
                            numDivisionId
                  FROM      #TempAPRecord
                  WHERE     numUserCntID = @numDomainId
                            AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
                            AND DATEDIFF(DAY, [dtDueDate],
                                         dbo.GetUTCDateWithoutTime()) > 90
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
           UPDATE  #TempAPRecord1
        SET     numOverNinetyDaysOverDuePaid = X.numOverNinetyDaysOverDuePaid
        FROM    ( SELECT    SUM(AmountPaid) AS numOverNinetyDaysOverDuePaid,
                            numDivisionId
                  FROM      #TempAPRecord
                  WHERE     numUserCntID = @numDomainId
                            AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
                            AND DATEDIFF(DAY, [dtDueDate],
                                         dbo.GetUTCDateWithoutTime()) > 90
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
        UPDATE  #TempAPRecord1
        SET     intOverNinetyDaysOverDueCount = 2
        WHERE   numDivisionId IN (
                SELECT DISTINCT
                        [numDivisionId]
                FROM    [#TempAPRecord]
                WHERE   numUserCntID = @numDomainId
                        AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
                        AND DATEDIFF(DAY, [dtDueDate],
                                     dbo.GetUTCDateWithoutTime()) > 90
                        AND numCurrencyID <> @baseCurrency )
             


UPDATE  #TempAPRecord1
SET     monUnAppliedAmount = ISNULL(X.monUnAppliedAmount,0)
FROM    (SELECT  SUM(ISNULL(monUnAppliedAmount,0)) AS monUnAppliedAmount,numDivisionId
FROM    #TempAPRecord
WHERE   numUserCntID =@numDomainId 
GROUP BY numDivisionId
) X WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID

UPDATE  #TempAPRecord1 SET numTotal=  isnull(numThirtyDays, 0) + isnull(numThirtyDaysOverDue, 0)
                + isnull(numSixtyDays, 0) + isnull(numSixtyDaysOverDue, 0)
                + isnull(numNinetyDays, 0) + isnull(numNinetyDaysOverDue, 0)
                + isnull(numOverNinetyDays, 0)
                + isnull(numOverNinetyDaysOverDue, 0) 			
             
        SELECT  [numDivisionId],
                [numContactId],
                [tintCRMType],
                [vcCompanyName],
                [vcCustPhone],
                [vcContactName] as vcApName,
                [vcEmail],
                [numPhone] as vcPhone,
                [numThirtyDays],
                [numSixtyDays],
                [numNinetyDays],
                [numOverNinetyDays],
                [numThirtyDaysOverDue],
                [numSixtyDaysOverDue],
                [numNinetyDaysOverDue],
                [numOverNinetyDaysOverDue],
             ISNULL(numTotal,0) - ISNULL(monUnAppliedAmount,0) AS numTotal,
                numCompanyID,
                numDomainID,
                [intThirtyDaysCount],
                [intThirtyDaysOverDueCount],
                [intSixtyDaysCount],
                [intSixtyDaysOverDueCount],
                [intNinetyDaysCount],
                [intOverNinetyDaysCount],
                [intNinetyDaysOverDueCount],
                [intOverNinetyDaysOverDueCount],
                [numThirtyDaysPaid],
                [numSixtyDaysPaid],
                [numNinetyDaysPaid],
                [numOverNinetyDaysPaid],
                [numThirtyDaysOverDuePaid],
                [numSixtyDaysOverDuePaid],
                [numNinetyDaysOverDuePaid],
                [numOverNinetyDaysOverDuePaid],ISNULL(monUnAppliedAmount,0) AS monUnAppliedAmount
        FROM    #TempAPRecord1
        WHERE   numUserCntID = @numDomainId AND (numDivisionId = @numDivisionId OR @numDivisionId IS NULL)
        ORDER BY [vcCompanyName]

        drop table #TempAPRecord1
        drop table #TempAPRecord 

        SET NOCOUNT OFF   
    
    END
/****** Object:  StoredProcedure [dbo].[USP_GetAccountPayableAging_Details]    Script Date: 03/06/2009 00:30:26 ******/
    SET ANSI_NULLS ON


/****** Object:  StoredProcedure [dbo].[USP_GetAccountPayableAging_Details]    Script Date: 03/06/2009 00:30:26 ******/
    SET ANSI_NULLS ON


GO
/****** Object:  StoredProcedure [dbo].[USP_GetAccountReceivableAging]    Script Date: 09/01/2009 00:40:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--SELECT * FROM domain
--Created By Siva
-- exec [dbo].[USP_GetAccountReceivableAging] 72,null,1
/*Note By-Chintan 
Some places we have used 
DATEADD(DAY, 0, DATEDIFF(DAY,0,GETUTCDATE()))
insted of GETUTCDATE()
Reson is the difference in result repectively
"2009-03-05 00:00:00.000"	"2009-03-05 09:38:02.560"
There WE are comparing UTCDate without Time.
Created function for same thing dbo.GetUTCDateWithoutTime()
*/
-- EXEC usp_getaccountreceivableaging 169,0,1,0,'2001-01-01 00:00:00.000', '2014-12-31 23:59:59:999'
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getaccountreceivableaging')
DROP PROCEDURE usp_getaccountreceivableaging
GO
CREATE PROCEDURE [dbo].[USP_GetAccountReceivableAging]
(
               @numDomainId   AS NUMERIC(9)  = 0,
               @numDivisionId AS NUMERIC(9)  = NULL,
               @numUserCntID AS NUMERIC(9),
               @numAccountClass AS NUMERIC(9)=0,
			   @dtFromDate AS DATETIME = NULL,
			   @dtToDate AS DATETIME = NULL
               )
AS
  BEGIN
	SET NOCOUNT ON

	IF @dtFromDate IS NULL
		SET @dtFromDate = '1990-01-01 00:00:00.000'
	IF @dtToDate IS NULL
		SET @dtToDate = GETUTCDATE()

  DECLARE  @AuthoritativeSalesBizDocId  AS INTEGER
    SELECT @AuthoritativeSalesBizDocId = isnull(numAuthoritativeSales,0)
    FROM   AuthoritativeBizDocs
    WHERE  numDomainId = @numDomainId;
  
--SELECT  GETDATE()
INSERT  INTO [TempARRecord]
        (
          numUserCntID,
          [numOppId],
          [numDivisionId],
          [numOppBizDocsId],
          [DealAmount],
          [numBizDocId],
          [numCurrencyID],
          [dtDueDate],AmountPaid
        )
        SELECT  @numUserCntID,
                OB.numOppId,
                Opp.numDivisionId,
                OB.numOppBizDocsId,
                ISNULL(OB.monDealAmount --dbo.GetDealAmount(OB.numOppID,getutcdate(),OB.numOppBizDocsId)
                       * ISNULL(Opp.fltExchangeRate, 1), 0)
                - ISNULL(OB.monAmountPaid * ISNULL(Opp.fltExchangeRate, 1),
                         0) DealAmount,
                OB.numBizDocId,
                ISNULL(Opp.numCurrencyID, 0) numCurrencyID,
--                DATEADD(DAY,CASE WHEN Opp.bitBillingTerms = 1 
--								 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(Opp.intBillingDays,0)),0)) 
--								 ELSE 0 
--							END, dtFromDate) AS dtDueDate,
				DATEADD(DAY,CASE WHEN Opp.bitBillingTerms = 1 
								 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(Opp.intBillingDays,0)), 0))
								 ELSE 0 
							END, dtFromDate) AS dtDueDate,
                ISNULL(OB.monAmountPaid * ISNULL(Opp.fltExchangeRate, 1),0) as AmountPaid
        FROM    OpportunityMaster Opp
                JOIN OpportunityBizDocs OB ON OB.numOppId = Opp.numOppID
                LEFT OUTER JOIN [Currency] C ON Opp.[numCurrencyID] = C.[numCurrencyID]
        WHERE   tintOpptype = 1
                AND tintoppStatus = 1
                --AND dtShippedDate IS NULL --Commented by chintan reason: Account Transaction and AR values was not matching
                AND opp.numDomainID = @numDomainId
                AND numBizDocId = @AuthoritativeSalesBizDocId
                AND OB.bitAuthoritativeBizDocs=1
                AND ISNULL(OB.tintDeferred,0) <> 1
                AND (Opp.numDivisionId = @numDivisionId
                            OR @numDivisionId IS NULL)
                            AND (Opp.numAccountClass=@numAccountClass OR @numAccountClass=0)
				AND OB.[dtCreatedDate] BETWEEN @dtFromDate AND @dtToDate
        GROUP BY OB.numOppId,
                Opp.numDivisionId,
                OB.numOppBizDocsId,
                Opp.fltExchangeRate,
                OB.numBizDocId,
                OB.dtCreatedDate,
                Opp.bitBillingTerms,
                Opp.intBillingDays,
                OB.monDealAmount,
                Opp.numCurrencyID,
                OB.dtFromDate,OB.monAmountPaid
        HAVING  ( ISNULL(OB.monDealAmount * ISNULL(Opp.fltExchangeRate, 1), 0)
                  - ISNULL(OB.monAmountPaid * ISNULL(Opp.fltExchangeRate, 1),
                           0) ) != 0
                           
	--Show Impact of AR journal entries in report as well 
UNION 
 SELECT @numUserCntID,
        0,
        GJD.numCustomerId,
        0,
        CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt ELSE -1*GJD.numCreditAmt END,
        0,
        GJD.[numCurrencyID],
        GJH.datEntry_Date ,0 AS AmountPaid
 FROM   dbo.General_Journal_Header GJH
        INNER JOIN dbo.General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId
                                                      AND GJH.numDomainId = GJD.numDomainId
        INNER JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = GJD.numChartAcntId
 WHERE  GJH.numDomainId = @numDomainId
        AND COA.vcAccountCode LIKE '0101010501'
        AND ISNULL(numOppId,0)=0 AND ISNULL(numOppBizDocsId,0)=0
        AND (GJD.numCustomerId = @numDivisionId
                            OR @numDivisionId IS NULL) AND ISNULL(GJH.numDepositID,0)=0 AND ISNULL(GJH.numReturnID,0)=0
		AND (GJH.numAccountClass=@numAccountClass OR @numAccountClass=0)                            
		AND GJH.[datEntry_Date] BETWEEN @dtFromDate AND @dtToDate
UNION ALL
 SELECT @numUserCntID,
        0,
        DM.numDivisionId,
        0,
        (ISNULL(monDepositAmount,0) - ISNULL(monAppliedAmount,0) /*- ISNULL(monRefundAmount,0)*/) * -1,
        0,
        DM.numCurrencyID,
        DM.dtDepositDate ,0 AS AmountPaid
 FROM   DepositMaster DM 
 WHERE DM.numDomainId=@numDomainId AND tintDepositePage IN(2,3)  
		AND (DM.numDivisionId = @numDivisionId OR @numDivisionId IS NULL)
		AND ((ISNULL(monDepositAmount,0) - ISNULL(monAppliedAmount,0) /*- ISNULL(monRefundAmount,0)*/) > 0)
		AND (DM.numAccountClass=@numAccountClass OR @numAccountClass=0)
		--GROUP BY DM.numDivisionId,DM.dtDepositDate
		AND DM.[dtDepositDate] BETWEEN @dtFromDate AND @dtToDate
UNION ALL --Standalone Refund against Account Receivable
 SELECT @numUserCntID,
        0,
        RH.numDivisionId,
        0,
        CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt ELSE -1*GJD.numCreditAmt END,
        0,
        GJD.[numCurrencyID],
        GJH.datEntry_Date ,0 AS AmountPaid
 FROM   dbo.ReturnHeader RH JOIN dbo.General_Journal_Header GJH ON RH.numReturnHeaderID = GJH.numReturnID
JOIN General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId 
INNER JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = GJD.numChartAcntId
WHERE RH.tintReturnType=4 AND RH.numDomainId=@numDomainId  AND COA.vcAccountCode LIKE '0101010501'
		AND (RH.numDivisionId = @numDivisionId OR @numDivisionId IS NULL)
		AND (monBizDocAmount > 0) AND ISNULL(RH.numBillPaymentIDRef,0)=0 --AND ISNULL(RH.numDepositIDRef,0)>0
		AND (RH.numAccountClass=@numAccountClass OR @numAccountClass=0)
		AND GJH.[datEntry_Date]  BETWEEN @dtFromDate AND @dtToDate
		--GROUP BY DM.numDivisionId,DM.dtDepositDate

	
INSERT  INTO [TempARRecord]
        (
          numUserCntID,
          [numOppId],
          [numDivisionId],
          [numOppBizDocsId],
          [DealAmount],
          [numBizDocId],
          [numCurrencyID],
          [dtDueDate],AmountPaid,monUnAppliedAmount
        )
SELECT @numUserCntID,0, DM.numDivisionId,0,0,0,0,NULL,0,sum(ISNULL(monDepositAmount,0) - ISNULL(monAppliedAmount,0))       
 FROM DepositMaster DM 
		WHERE DM.numDomainId=@numDomainId AND tintDepositePage IN(2,3)  
		AND (DM.numDivisionId = @numDivisionId
                            OR @numDivisionId IS NULL)
		AND ((ISNULL(monDepositAmount,0) - ISNULL(monAppliedAmount,0)) > 0)
		AND (DM.numAccountClass=@numAccountClass OR @numAccountClass=0)
		AND DM.[dtDepositDate] BETWEEN @dtFromDate AND @dtToDate
		GROUP BY DM.numDivisionId


--SELECT  * FROM    [TempARRecord]


--SELECT  GETDATE()
INSERT  INTO [TempARRecord1] (
	[numUserCntID],
	[numDivisionId],
	[tintCRMType],
	[vcCompanyName],
	[vcCustPhone],
	[numContactId],
	[vcEmail],
	[numPhone],
	[vcContactName],
	[numThirtyDays],
	[numSixtyDays],
	[numNinetyDays],
	[numOverNinetyDays],
	[numThirtyDaysOverDue],
	[numSixtyDaysOverDue],
	[numNinetyDaysOverDue],
	[numOverNinetyDaysOverDue],
	numCompanyID,
	numDomainID,
	[intThirtyDaysCount],
	[intThirtyDaysOverDueCount],
	[intSixtyDaysCount],
	[intSixtyDaysOverDueCount],
	[intNinetyDaysCount],
	[intOverNinetyDaysCount],
	[intNinetyDaysOverDueCount],
	[intOverNinetyDaysOverDueCount],
	[numThirtyDaysPaid],
	[numSixtyDaysPaid],
	[numNinetyDaysPaid],
	[numOverNinetyDaysPaid],
	[numThirtyDaysOverDuePaid],
	[numSixtyDaysOverDuePaid],
	[numNinetyDaysOverDuePaid],
	[numOverNinetyDaysOverDuePaid],monUnAppliedAmount
) 
        SELECT DISTINCT
                @numUserCntID,
                Div.numDivisionID,
                tintCRMType AS tintCRMType,
                vcCompanyName AS vcCompanyName,
                '',
                NULL,
                '',
                '',
                '',
                0 numThirtyDays,
                0 numSixtyDays,
                0 numNinetyDays,
                0 numOverNinetyDays,
                0 numThirtyDaysOverDue,
                0 numSixtyDaysOverDue,
                0 numNinetyDaysOverDue,
                0 numOverNinetyDaysOverDue,
                C.numCompanyID,
				Div.numDomainID,
                0 [intThirtyDaysCount],
				0 [intThirtyDaysOverDueCount],
				0 [intSixtyDaysCount],
				0 [intSixtyDaysOverDueCount],
				0 [intNinetyDaysCount],
				0 [intOverNinetyDaysCount],
				0 [intNinetyDaysOverDueCount],
				0 [intOverNinetyDaysOverDueCount],
				0 numThirtyDaysPaid,
                0 numSixtyDaysPaid,
                0 numNinetyDaysPaid,
                0 numOverNinetyDaysPaid,
                0 numThirtyDaysOverDuePaid,
                0 numSixtyDaysOverDuePaid,
                0 numNinetyDaysOverDuePaid,
                0 numOverNinetyDaysOverDuePaid,0 monUnAppliedAmount
        FROM    Divisionmaster Div
                INNER JOIN CompanyInfo C ON C.numCompanyId = Div.numCompanyID
                    --JOIN [TempARRecord] t ON Div.[numDivisionID] = t.[numDivisionId]
        WHERE   Div.[numDomainID] = @numDomainId
                AND Div.[numDivisionID] IN ( SELECT DISTINCT
                                                    [numDivisionID]
                                             FROM   [TempARRecord] )



--Update Custome Phone 
UPDATE  TempARRecord1
SET     [vcCustPhone] = X.[vcCustPhone]
FROM    ( SELECT 
                            CASE WHEN numPhone IS NULL THEN ''
                                 WHEN numPhone = '' THEN ''
                                 ELSE ', ' + numPhone
                            END AS vcCustPhone,
                            numDivisionId
                  FROM      AdditionalContactsInformation
                  WHERE     bitPrimaryContact = 1
                            AND numDivisionId  IN (SELECT DISTINCT numDivisionID FROM [TempARRecord1] WHERE numUserCntID =@numUserCntID)

        ) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID

--Update Customer info 
UPDATE  TempARRecord1
SET     [numContactId] = X.[numContactID],[vcEmail] = X.[vcEmail],[vcContactName] = X.[vcContactName],numPhone=X.numPhone
FROM    ( SELECT numContactId AS numContactID,
                     ISNULL(vcEmail,'') AS vcEmail,
                     ISNULL(vcFirstname,'') + ' ' + ISNULL(vcLastName,'') AS vcContactName,
                     CAST(ISNULL( CASE WHEN numPhone IS NULL THEN '' WHEN numPhone = '' THEN '' ELSE ', ' + numPhone END,'') AS VARCHAR) AS numPhone,
                     [numDivisionId]
                  FROM      AdditionalContactsInformation
                  WHERE     (vcPosition = 844 or isnull(bitPrimaryContact,0)=1)
                            AND numDivisionId  IN (SELECT DISTINCT numDivisionID FROM [TempARRecord1] WHERE numUserCntID =@numUserCntID)
    ) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID

                    
--SELECT  GETDATE()
------------------------------------------      
DECLARE @baseCurrency AS NUMERIC
SELECT @baseCurrency=numCurrencyID FROM dbo.Domain WHERE numDomainId=@numDomainId
/*when any Order is placed in Foreign currency (other than base currency)then show it in purple color */
------------------------------------------
UPDATE  TempARRecord1
SET     numThirtyDays = X.numThirtyDays
FROM    ( SELECT    SUM(DealAmount) AS numThirtyDays,
                    [numDivisionId]
          FROM      [TempARRecord]
          WHERE     numUserCntID =@numUserCntID AND
		DATEDIFF(DAY,dbo.GetUTCDateWithoutTime(),[dtDueDate]) BETWEEN 0 AND 30	
          GROUP BY  numDivisionId
        ) X WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1
SET     numThirtyDaysPaid = X.numThirtyDaysPaid
FROM    ( SELECT    SUM(AmountPaid) AS numThirtyDaysPaid,
                    [numDivisionId]
          FROM      [TempARRecord]
          WHERE     numUserCntID =@numUserCntID AND
		DATEDIFF(DAY,dbo.GetUTCDateWithoutTime(),[dtDueDate]) BETWEEN 0 AND 30	
          GROUP BY  numDivisionId
        ) X WHERE   TempARRecord1.numDivisionID = X.numDivisionID

                
UPDATE  TempARRecord1 SET intThirtyDaysCount = 2 WHERE numDivisionId IN ( SELECT DISTINCT [numDivisionId]
          FROM      [TempARRecord]
          WHERE     numUserCntID =@numUserCntID AND DATEDIFF(DAY,dbo.GetUTCDateWithoutTime(),[dtDueDate]) BETWEEN 0 AND 30	
                                                AND numCurrencyID <>@baseCurrency )
------------------------------------------
UPDATE  TempARRecord1
SET     numSixtyDays = X.numSixtyDays
FROM    ( SELECT  SUM(DealAmount) AS numSixtyDays,
        numDivisionId
FROM    [TempARRecord]
WHERE   numUserCntID =@numUserCntID AND 
	DATEDIFF(DAY,dbo.GetUTCDateWithoutTime(),[dtDueDate]) BETWEEN 31 AND 60	
        AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
GROUP BY numDivisionId
) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1
SET     numSixtyDaysPaid = X.numSixtyDaysPaid
FROM    ( SELECT  SUM(AmountPaid) AS numSixtyDaysPaid,
        numDivisionId
FROM    [TempARRecord]
WHERE   numUserCntID =@numUserCntID AND 
	DATEDIFF(DAY,dbo.GetUTCDateWithoutTime(),[dtDueDate]) BETWEEN 31 AND 60	
        AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
GROUP BY numDivisionId
) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1 SET intSixtyDaysCount = 2 WHERE numDivisionId IN ( SELECT DISTINCT [numDivisionId]
          FROM      [TempARRecord]
          WHERE     numUserCntID =@numUserCntID AND DATEDIFF(DAY,dbo.GetUTCDateWithoutTime(),[dtDueDate]) BETWEEN 31 AND 60
		AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
                                                AND numCurrencyID <>@baseCurrency )
------------------------------------------
UPDATE  TempARRecord1
SET     numNinetyDays = X.numNinetyDays
FROM    (SELECT  SUM(DealAmount) AS numNinetyDays,
        numDivisionId
FROM    TempARRecord
WHERE   numUserCntID =@numUserCntID AND DATEDIFF(DAY,dbo.GetUTCDateWithoutTime(),[dtDueDate]) BETWEEN 61 AND 90
        AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
GROUP BY numDivisionId
) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1
SET     numNinetyDaysPaid = X.numNinetyDaysPaid
FROM    (SELECT  SUM(AmountPaid) AS numNinetyDaysPaid,
        numDivisionId
FROM    TempARRecord
WHERE   numUserCntID =@numUserCntID AND DATEDIFF(DAY,dbo.GetUTCDateWithoutTime(),[dtDueDate]) BETWEEN 61 AND 90
        AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
GROUP BY numDivisionId
) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1 SET intNinetyDaysCount = 2 WHERE numDivisionId IN ( SELECT DISTINCT [numDivisionId]
          FROM      [TempARRecord]
          WHERE     numUserCntID =@numUserCntID AND DATEDIFF(DAY,dbo.GetUTCDateWithoutTime(),[dtDueDate]) BETWEEN 61 AND 90
        AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
                                                AND numCurrencyID <>@baseCurrency )
------------------------------------------
UPDATE  TempARRecord1
SET     numOverNinetyDays = X.numOverNinetyDays
FROM    (SELECT  SUM(DealAmount) AS numOverNinetyDays,
        numDivisionId
FROM    TempARRecord
WHERE   numUserCntID =@numUserCntID AND [dtDueDate] >= DATEADD(DAY, 91,
                                                    dbo.GetUTCDateWithoutTime())
        AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
GROUP BY numDivisionId
) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1
SET     numOverNinetyDaysPaid = X.numOverNinetyDaysPaid
FROM    (SELECT  SUM(AmountPaid) AS numOverNinetyDaysPaid,
        numDivisionId
FROM    TempARRecord
WHERE   numUserCntID =@numUserCntID AND [dtDueDate] >= DATEADD(DAY, 91,
                                                    dbo.GetUTCDateWithoutTime())
        AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
GROUP BY numDivisionId
) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1 SET intOverNinetyDaysCount = 2 WHERE numDivisionId IN ( SELECT DISTINCT [numDivisionId]
          FROM      [TempARRecord]
          WHERE     numUserCntID =@numUserCntID AND [dtDueDate] >= DATEADD(DAY, 91,
                                                    dbo.GetUTCDateWithoutTime())
        AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
                                                AND numCurrencyID <>@baseCurrency )

------------------------------------------
UPDATE  TempARRecord1
SET     numThirtyDaysOverDue = X.numThirtyDaysOverDue
FROM    (SELECT  SUM(DealAmount) AS numThirtyDaysOverDue,
        numDivisionId
FROM    TempARRecord
WHERE   numUserCntID =@numUserCntID AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
        AND DATEDIFF(DAY,[dtDueDate], dbo.GetUTCDateWithoutTime()) BETWEEN 0 AND 30
GROUP BY numDivisionId
) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1
SET     numThirtyDaysOverDuePaid = X.numThirtyDaysOverDuePaid
FROM    (SELECT  SUM(AmountPaid) AS numThirtyDaysOverDuePaid,
        numDivisionId
FROM    TempARRecord
WHERE   numUserCntID =@numUserCntID AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
        AND DATEDIFF(DAY,[dtDueDate], dbo.GetUTCDateWithoutTime()) BETWEEN 0 AND 30
GROUP BY numDivisionId
) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1 SET intThirtyDaysOverDueCount = 2 WHERE numDivisionId IN ( SELECT DISTINCT [numDivisionId]
          FROM      [TempARRecord]
          WHERE     numUserCntID =@numUserCntID AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
        AND DATEDIFF(DAY,[dtDueDate], dbo.GetUTCDateWithoutTime()) BETWEEN 0 AND 30
                                                AND numCurrencyID <>@baseCurrency )

----------------------------------
UPDATE  TempARRecord1
SET     numSixtyDaysOverDue = X.numSixtyDaysOverDue
FROM    ( SELECT    SUM(DealAmount) AS numSixtyDaysOverDue,
                    numDivisionId
          FROM      TempARRecord
          WHERE     numUserCntID =@numUserCntID AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
                    AND DATEDIFF(DAY,
                                 [dtDueDate],
                                 dbo.GetUTCDateWithoutTime()) BETWEEN 31
                                                              AND     60
          GROUP BY  numDivisionId
        ) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID

UPDATE  TempARRecord1
SET     numSixtyDaysOverDuePaid = X.numSixtyDaysOverDuePaid
FROM    ( SELECT    SUM(AmountPaid) AS numSixtyDaysOverDuePaid,
                    numDivisionId
          FROM      TempARRecord
          WHERE     numUserCntID =@numUserCntID AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
                    AND DATEDIFF(DAY,
                                 [dtDueDate],
                                 dbo.GetUTCDateWithoutTime()) BETWEEN 31
                                                              AND     60
          GROUP BY  numDivisionId
        ) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1 SET intSixtyDaysOverDueCount = 2 WHERE numDivisionId IN ( SELECT DISTINCT [numDivisionId]
          FROM      [TempARRecord]
          WHERE     numUserCntID =@numUserCntID AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
                    AND DATEDIFF(DAY,
                                 [dtDueDate],
                                 dbo.GetUTCDateWithoutTime()) BETWEEN 31
                                                              AND     60
                                                AND numCurrencyID <>@baseCurrency )

------------------------------------------
UPDATE  TempARRecord1
SET     numNinetyDaysOverDue = X.numNinetyDaysOverDue
FROM    (SELECT  SUM(DealAmount) AS numNinetyDaysOverDue,
        numDivisionId
FROM    TempARRecord
WHERE   numUserCntID =@numUserCntID AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
        AND DATEDIFF(DAY,[dtDueDate], dbo.GetUTCDateWithoutTime()) BETWEEN 61 AND 90
GROUP BY numDivisionId
) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1
SET     numNinetyDaysOverDuePaid = X.numNinetyDaysOverDuePaid
FROM    (SELECT  SUM(AmountPaid) AS numNinetyDaysOverDuePaid,
        numDivisionId
FROM    TempARRecord
WHERE   numUserCntID =@numUserCntID AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
        AND DATEDIFF(DAY,[dtDueDate], dbo.GetUTCDateWithoutTime()) BETWEEN 61 AND 90
GROUP BY numDivisionId
) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1 SET intNinetyDaysOverDueCount = 2 WHERE numDivisionId IN ( SELECT DISTINCT [numDivisionId]
          FROM      [TempARRecord]
          WHERE     numUserCntID =@numUserCntID AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
        AND DATEDIFF(DAY,[dtDueDate], dbo.GetUTCDateWithoutTime()) BETWEEN 61 AND 90
                                                AND numCurrencyID <>@baseCurrency )
------------------------------------------
UPDATE  TempARRecord1
SET     numOverNinetyDaysOverDue = X.numOverNinetyDaysOverDue
FROM    (SELECT  SUM(DealAmount) AS numOverNinetyDaysOverDue,
        numDivisionId
FROM    TempARRecord
WHERE   numUserCntID =@numUserCntID AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
        AND DATEDIFF(DAY, [dtDueDate], dbo.GetUTCDateWithoutTime()) > 90
GROUP BY numDivisionId
) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1
SET     numOverNinetyDaysOverDuePaid = X.numOverNinetyDaysOverDuePaid
FROM    (SELECT  SUM(AmountPaid) AS numOverNinetyDaysOverDuePaid,
        numDivisionId
FROM    TempARRecord
WHERE   numUserCntID =@numUserCntID AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
        AND DATEDIFF(DAY, [dtDueDate], dbo.GetUTCDateWithoutTime()) > 90
GROUP BY numDivisionId
) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1 SET intOverNinetyDaysOverDueCount = 2 WHERE numDivisionId IN ( SELECT DISTINCT [numDivisionId]
          FROM      [TempARRecord]
          WHERE     numUserCntID =@numUserCntID AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
        AND DATEDIFF(DAY, [dtDueDate], dbo.GetUTCDateWithoutTime()) > 90
                                                AND numCurrencyID <>@baseCurrency )


UPDATE  TempARRecord1
SET     monUnAppliedAmount = ISNULL(X.monUnAppliedAmount,0)
FROM    (SELECT  SUM(ISNULL(monUnAppliedAmount,0)) AS monUnAppliedAmount,numDivisionId
FROM    TempARRecord
WHERE   numUserCntID =@numUserCntID 
GROUP BY numDivisionId
) X WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1 SET numTotal=isnull(numThirtyDays,0)
      + isnull(numThirtyDaysOverDue,0)
      + isnull(numSixtyDays,0)
      + isnull(numSixtyDaysOverDue,0)
      + isnull(numNinetyDays,0)
      + isnull(numNinetyDaysOverDue,0)
      + isnull(numOverNinetyDays,0)
      + isnull(numOverNinetyDaysOverDue,0) /*- ISNULL(monUnAppliedAmount,0) */
           
SELECT  
		[numDivisionId],
		[numContactId],
		[tintCRMType],
		[vcCompanyName],
		[vcCustPhone],
		[vcContactName],
		[vcEmail],
		[numPhone],
		[numThirtyDays],
		[numSixtyDays],
		[numNinetyDays],
		[numOverNinetyDays],
		[numThirtyDaysOverDue],
		[numSixtyDaysOverDue],
		[numNinetyDaysOverDue],
		[numOverNinetyDaysOverDue],
--		CASE WHEN (ISNULL(numTotal,0) - ISNULL(monUnAppliedAmount,0))>=0
--		 THEN ISNULL(numTotal,0) - ISNULL(monUnAppliedAmount,0) ELSE ISNULL(numTotal,0) END AS numTotal ,
		ISNULL(numTotal,0) AS numTotal,
		numCompanyID,
		numDomainID,
		[intThirtyDaysCount],
		[intThirtyDaysOverDueCount],
		[intSixtyDaysCount],
		[intSixtyDaysOverDueCount],
		[intNinetyDaysCount],
		[intOverNinetyDaysCount],
		[intNinetyDaysOverDueCount],
		[intOverNinetyDaysOverDueCount],
		[numThirtyDaysPaid],
		[numSixtyDaysPaid],
		[numNinetyDaysPaid],
		[numOverNinetyDaysPaid],
		[numThirtyDaysOverDuePaid],
		[numSixtyDaysOverDuePaid],
		[numNinetyDaysOverDuePaid],
		[numOverNinetyDaysOverDuePaid],ISNULL(monUnAppliedAmount,0) AS monUnAppliedAmount
FROM    TempARRecord1 WHERE numUserCntID =@numUserCntID AND ISNULL(numTotal,0)!=0
ORDER BY [vcCompanyName] 
DELETE  FROM TempARRecord1 WHERE   numUserCntID = @numUserCntID ;
DELETE  FROM TempARRecord WHERE   numUserCntID = @numUserCntID ;

SET NOCOUNT OFF   
    
  END
  



/****** Object:  StoredProcedure [dbo].[usp_GetAllSelectedSystemModulesPagesAndAccesses]    Script Date: 07/26/2008 16:16:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getallselectedsystemmodulespagesandaccesses')
DROP PROCEDURE usp_getallselectedsystemmodulespagesandaccesses
GO
CREATE PROCEDURE [dbo].[usp_GetAllSelectedSystemModulesPagesAndAccesses]    
 @numModuleID NUMERIC(9) = 0,    
 @numGroupID NUMERIC(9) = 0       
--    
AS    
  BEGIN    

	
	IF @numModuleID = 32 --Relationships
	BEGIN
		SELECT
			*
		FROM
			(
				SELECT 
					PM.numModuleID, PM.numPageID, PM.vcPageDesc, GM.intViewAllowed, GM.intAddAllowed, GM.intUpdateAllowed,     
					GM.intDeleteAllowed, GM.intExportAllowed, PM.bitIsViewApplicable, PM.bitIsAddApplicable, PM.bitIsUpdateApplicable, 
					PM.bitIsDeleteApplicable, PM.bitIsExportApplicable ,isnull(PM.vcToolTip,'') as  vcToolTip
				FROM 
					PageMaster PM 
				LEFT JOIN 
					GroupAuthorization GM
				ON 
					PM.numPageID = GM.numPageID AND 
					PM.numModuleID = GM.numModuleID  AND 
					(GM.numGroupID = @numGroupID OR GM.numGroupID IS NULL) AND
					GM.bitCustomRelationship = 0
				WHERE  
					PM.numModuleID = @numModuleID AND ISNULL(bitDeleted,0) = 0 
				UNION
				SELECT
					32 AS numModuleID, 
					TableCustomRelationship.numListItemID AS numPageID, 
					TableCustomRelationship.vcData + ' List' AS vcPageDesc, 
					ISNULL(GM.intViewAllowed,3) AS intViewAllowed, 
					0 AS intAddAllowed, 
					0 AS intUpdateAllowed,     
					0 AS intDeleteAllowed, 
					0 AS intExportAllowed, 
					1 AS bitIsViewApplicable, 
					0 AS bitIsAddApplicable, 
					0 As bitIsUpdateApplicable, 
					0 AS bitIsDeleteApplicable, 
					0 AS bitIsExportApplicable ,
					'' as  vcToolTip
				FROM
					(
						SELECT 
							numListItemID,
							vcData
						FROM 
							ListDetails 
						WHERE 
							numListID = 5 AND 
							(ISNULL(constFlag,0) = 1 OR (numDomainID = ISNULL((SELECT numDomainID FROM AuthenticationGroupMaster WHERE numGroupID = @numGroupID),0) AND 
							ISNULL(bitDelete,0) = 0)) AND 
							numListItemID <> 46) AS TableCustomRelationship
				LEFT JOIN 
					GroupAuthorization GM
				ON 
					GM.numPageID = TableCustomRelationship.numListItemID AND 
					(GM.numGroupID = @numGroupID OR GM.numGroupID IS NULL) AND
					GM.bitCustomRelationship = 1
			) AS TEMP
		ORDER BY
			TEMP.vcPageDesc
	END
	ELSE
	BEGIN
	   SELECT PM.numModuleID, PM.numPageID, PM.vcPageDesc, GM.intViewAllowed, GM.intAddAllowed, GM.intUpdateAllowed,     
							 GM.intDeleteAllowed, GM.intExportAllowed, PM.bitIsViewApplicable, PM.bitIsAddApplicable, PM.bitIsUpdateApplicable, PM.bitIsDeleteApplicable, PM.bitIsExportApplicable ,isnull(PM.vcToolTip,'') as  vcToolTip
	   FROM PageMaster PM 
		left join GroupAuthorization GM
		on PM.numPageID = GM.numPageID AND PM.numModuleID = GM.numModuleID  AND (GM.numGroupID = @numGroupID OR GM.numGroupID IS NULL)  
	   WHERE  PM.numModuleID = @numModuleID AND ISNULL(bitDeleted,0) = 0 
   
	   ORDER BY PM.vcPageDesc ASC 
   END    
  END
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetAuthDetailsForDomain' ) 
    DROP PROCEDURE USP_GetAuthDetailsForDomain
GO
--USP_GetAuthDetailsForDomain 147
CREATE PROCEDURE [dbo].USP_GetAuthDetailsForDomain
    @numDomainID NUMERIC(9)
AS 
    BEGIN
  
  
  
  SELECT   AGM.numDomainID ,
                AGM.numGroupID ,
                PM.numModuleID ,
                PM.numPageID ,
                0 intExportAllowed ,
                0 intPrintAllowed ,
                0 intViewAllowed ,
                0 intAddAllowed ,
                0 intUpdateAllowed ,
                0 intDeleteAllowed,
				PM.vcPageDesc,
				(SELECT MM.vcModuleName FROM dbo.ModuleMaster MM WHERE numModuleID=PM.numModuleID) AS vcModuleName,
				0 AS bitCustomRelationship
INTO #TempAuth
FROM dbo.AuthenticationGroupMaster AGM CROSS JOIN dbo.PageMaster PM 
WHERE AGM.numDomainID=@numDomainID 
UNION
SELECT
	AGM.numDomainID ,
    AGM.numGroupID ,
    32 ,
    TEMP.numListItemID,
    0 intExportAllowed ,
    0 intPrintAllowed ,
    0 intViewAllowed ,
    0 intAddAllowed ,
    0 intUpdateAllowed ,
    0 intDeleteAllowed,
	TEMP.vcData + ' List' AS vcPageDesc,
	(SELECT MM.vcModuleName FROM dbo.ModuleMaster MM WHERE numModuleID=32) AS vcModuleName,
	1 AS bitCustomRelationship
FROM 
	dbo.AuthenticationGroupMaster AGM
CROSS JOIN 
	(
		SELECT 
			numListItemID,
			vcData
		FROM 
			ListDetails 
		WHERE 
			numListID = 5 AND 
			(ISNULL(constFlag,0) = 1 OR (numDomainID = @numDomainID AND ISNULL(bitDelete,0) = 0)) AND 
			numListItemID <> 46
	) AS TEMP
WHERE 
	AGM.numDomainID=@numDomainID


--SELECT * FROM #TempAuth

UPDATE T SET
intExportAllowed = ISNULL(GA.intExportAllowed,0),
intPrintAllowed=ISNULL(GA.intPrintAllowed,0),
intViewAllowed=ISNULL(GA.intViewAllowed,0),
intAddAllowed=ISNULL(GA.intAddAllowed,0),
intUpdateAllowed=ISNULL(GA.intUpdateAllowed,0),
intDeleteAllowed=ISNULL(GA.intDeleteAllowed,0)
from #TempAuth T LEFT JOIN dbo.GroupAuthorization GA ON GA.numPageID=T.numPageID AND GA.numDomainID=T.numDomainID AND GA.numGroupID=T.numGroupID AND GA.numModuleID=T.numModuleID WHERE T.bitCustomRelationship = 0

UPDATE T SET
intExportAllowed = ISNULL(GA.intExportAllowed,0),
intPrintAllowed=ISNULL(GA.intPrintAllowed,0),
intViewAllowed=ISNULL(GA.intViewAllowed,3),
intAddAllowed=ISNULL(GA.intAddAllowed,0),
intUpdateAllowed=ISNULL(GA.intUpdateAllowed,0),
intDeleteAllowed=ISNULL(GA.intDeleteAllowed,0)
from #TempAuth T LEFT JOIN dbo.GroupAuthorization GA ON GA.numPageID=T.numPageID AND GA.numDomainID=T.numDomainID AND GA.numGroupID=T.numGroupID AND GA.numModuleID=T.numModuleID WHERE T.bitCustomRelationship = 1

 
 SELECT * FROM #TempAuth
 --RETURN;

  --      SELECT  G.numDomainID ,
  --              G.numGroupID ,
  --              GA.numModuleID ,
  --              GA.numPageID ,
  --              GA.intExportAllowed ,
  --              GA.intPrintAllowed ,
  --              GA.intViewAllowed ,
  --              GA.intAddAllowed ,
  --              GA.intUpdateAllowed ,
  --              GA.intDeleteAllowed,
		--		PM.vcPageDesc,
		--		MM.vcModuleName
		--FROM    dbo.PageMaster PM ,
  --              dbo.GroupAuthorization GA
  --              left JOIN dbo.AuthenticationGroupMaster G ON G.numGroupID = GA.numGroupID
		--		left JOIN dbo.ModuleMaster MM ON GA.numModuleID = MM.numModuleID
  --      WHERE   GA.numModuleID = PM.numModuleID
  --              AND GA.numPageID = PM.numPageID
  --              AND G.numDomainID = @numDomainID


    --    FROM    dbo.PageMaster PM left JOIN dbo.GroupAuthorization GA
				--ON GA.numModuleID = PM.numModuleID AND GA.numPageID = PM.numPageID
    --            left JOIN dbo.AuthenticationGroupMaster G ON G.numGroupID = GA.numGroupID
				--left JOIN dbo.ModuleMaster MM ON GA.numModuleID = MM.numModuleID
        --WHERE  
        --       G.numDomainID = @numDomainID


			   
        --SELECT  G.numDomainID ,
        --        G.numGroupID ,
        --        GA.numModuleID ,
        --        GA.numPageID ,
        --        GA.intExportAllowed ,
        --        GA.intPrintAllowed ,
        --        GA.intViewAllowed ,
        --        GA.intAddAllowed ,
        --        GA.intUpdateAllowed ,
        --        GA.intDeleteAllowed
        --FROM    dbo.PageMaster PM ,
        --        dbo.GroupAuthorization GA
        --        INNER JOIN dbo.AuthenticationGroupMaster G ON G.numGroupID = GA.numGroupID
        --WHERE   GA.numModuleID = PM.numModuleID
        --        AND GA.numPageID = PM.numPageID
        --        AND G.numDomainID = @numDomainID
	
    END
/****** Object:  StoredProcedure [dbo].[USP_GetChartAcntDetails]    Script Date: 09/25/2009 15:47:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Created by Siva        
-- [USP_GetChartAcntDetails] @numDomainId=72,@dtFromDate='2007-01-01 00:00:00:000',@dtToDate='2009-09-25 00:00:00:000'                            
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_getchartacntdetails' ) 
    DROP PROCEDURE usp_getchartacntdetails
GO
CREATE PROCEDURE [dbo].[USP_GetChartAcntDetails]
    @numDomainId AS NUMERIC(9),
    @dtFromDate AS DATETIME,
    @dtToDate AS DATETIME,
    @ClientTimeZoneOffset INT, --Added by Chintan to enable calculation of date according to client machine
    @numAccountClass AS NUMERIC(9)=0
AS 
    BEGIN    
--        DECLARE @numFinYear INT ;
        DECLARE @dtFinYearFromJournal DATETIME ;
		DECLARE @dtFinYearFrom DATETIME ;

--        SET @numFinYear = ( SELECT  numFinYearId
--                            FROM    FINANCIALYEAR
--                            WHERE   dtPeriodFrom <= @dtFromDate
--                                    AND dtPeriodTo >= @dtFromDate
--                                    AND numDomainId = @numDomainId
--                          ) ;
SELECT @dtFinYearFromJournal = MIN(datEntry_Date) FROM view_journal WHERE numDomainID=@numDomainId AND (numAccountClass=@numAccountClass OR @numAccountClass=0)
SET @dtFinYearFrom = ( SELECT   dtPeriodFrom
                        FROM     FINANCIALYEAR
                        WHERE    dtPeriodFrom <= @dtFromDate
                                AND dtPeriodTo >= @dtFromDate
                                AND numDomainId = @numDomainId
                        ) ;

SELECT * INTO #view_journal FROM view_journal WHERE numDomainID=@numDomainId AND (numAccountClass=@numAccountClass OR @numAccountClass=0);

/*Timezone Logic.. Stored Date are in UTC/GMT  */
--SET @dtToDate = DATEADD(DAY, DATEDIFF(DAY, '19000101',  @dtToDate), '23:59:59');
--SELECT @dtFromDate,@ClientTimeZoneOffset,DATEADD(minute, @ClientTimeZoneOffset, @dtFromDate)
--        SET @dtFromDate = DATEADD(minute, @ClientTimeZoneOffset, @dtFromDate) ;

        /*Comment by chintan
        Reason: Standard US was facing balance not matching for undeposited func(check video for more info)
        We do not need timezone settings since all journal entry date does not store time. we only store date. 
        */
--        SET @dtToDate = DATEADD(minute, @ClientTimeZoneOffset, @dtToDate) ;


--PRINT @dtFromDate
--PRINT @dtToDate
--PRINT @numFinYear 
--PRINT @dtFinYearFrom 

--select * from view_journal where numDomainid=72

		
        CREATE TABLE #PLSummary
            (
              numAccountId NUMERIC(9),
              vcAccountName VARCHAR(250),
              numParntAcntTypeID NUMERIC(9),
              vcAccountDescription VARCHAR(250),
              vcAccountCode VARCHAR(50) COLLATE Database_Default,
              Opening MONEY,
              Debit MONEY,
              Credit MONEY
              ,bitIsSubAccount BIT,numParentAccID NUMERIC(9)
            ) ;

        INSERT  INTO #PLSummary
                SELECT  COA.numAccountId,
                        vcAccountName,
                        numParntAcntTypeID,
                        vcAccountDescription,
                        vcAccountCode,
                      /*  ISNULL(( SELECT SUM(ISNULL(monOpening, 0))
                                 FROM   CHARTACCOUNTOPENING CAO
                                 WHERE  numFinYearId = @numFinYear
                                        AND numDomainID = @numDomainId
                                        AND CAO.numAccountId IN (SELECT numAccountId FROM dbo.Chart_Of_Accounts WHERE numDomainId = @numDomainId AND vcAccountCode like COA.vcAccountCode + '%' )
                               ), 0)
                        + */ 
						CASE WHEN COA.[vcAccountCode] LIKE '0103%' OR 
							 	  COA.[vcAccountCode] LIKE '0104%' OR 
							 	  COA.[vcAccountCode] LIKE '0106%' 
							 THEN 0 
							 ELSE isnull(t1.OPENING,0) 
						END as OPENING,
                        ISNULL(t.DEBIT, 0) AS DEBIT,
                        ISNULL(t.CREDIT, 0) AS CREDIT
                               ,ISNULL(COA.bitIsSubAccount,0),ISNULL(COA.numParentAccID,0) AS numParentAccID
                FROM    Chart_of_Accounts COA
					OUTER APPLY (SELECT   SUM(Debit - Credit) AS OPENING
                                   FROM     #view_journal VJ
                                   WHERE    VJ.numDomainId = @numDomainId
                                            AND VJ.numAccountID = COA.numAccountID /*AND VJ.COAvcAccountCode like COA.vcAccountCode + '%'*/ /*VJ.numAccountId = COA.numAccountId*/
                                            AND datEntry_Date BETWEEN (CASE WHEN COA.[vcAccountCode] LIKE '0103%' OR COA.[vcAccountCode] LIKE '0104%' OR COA.[vcAccountCode] LIKE '0106%' THEN @dtFinYearFrom ELSE @dtFinYearFromJournal END )
                                                              AND  DATEADD(Minute,-1,@dtFromDate) AND (VJ.numAccountClass=@numAccountClass OR @numAccountClass=0)) AS t1
					OUTER APPLY(SELECT   SUM(Debit) as DEBIT,SUM(Credit) as CREDIT
                                   FROM   #view_journal VJ
                                 WHERE  VJ.numDomainId = @numDomainId
                                        AND VJ.numAccountID = COA.numAccountID /*AND VJ.COAvcAccountCode like COA.vcAccountCode + '%'*/ /*VJ.numAccountId = COA.numAccountId*/
                                        AND datEntry_Date BETWEEN @dtFromDate AND @dtToDate AND (VJ.numAccountClass=@numAccountClass OR @numAccountClass=0)
                                 ) as t
                    WHERE   COA.numDomainId = @numDomainId  AND COA.bitActive = 1 ;
--SELECT @dtFinYearFrom,@dtFromDate - 1,* FROM #PLSummary
        CREATE TABLE #PLOutPut
            (
              numAccountId NUMERIC(9),
              vcAccountName VARCHAR(250),
              numParntAcntTypeID NUMERIC(9),
              vcAccountDescription VARCHAR(250),
              vcAccountCode VARCHAR(50) COLLATE Database_Default,
              Opening MONEY,
              Debit MONEY,
              Credit MONEY,numParentAccID NUMERIC(9)
            ) ;

        INSERT  INTO #PLOutPut
                SELECT  ATD.numAccountTypeID,
                        ATD.vcAccountType,
                        ATD.numParentID,
                        '',
                        ATD.vcAccountCode,
                        ISNULL(SUM(Opening), 0) AS Opening,
                        ISNULL(SUM(Debit), 0) AS Debit,
                        ISNULL(SUM(Credit), 0) AS Credit,0
                FROM    /*Chart_Of_Accounts c JOIN [AccountTypeDetail] ATD ON ATD.[numAccountTypeID] = c.[numParntAcntTypeId]*/
						[AccountTypeDetail] ATD
                        RIGHT OUTER JOIN #PLSummary PL ON PL.vcAccountCode LIKE ATD.vcAccountCode + '%' AND LEN(PL.vcAccountCode)>LEN(ATD.vcAccountCode)
                        --(PL.vcAccountCode LIKE c.vcAccountCode OR (PL.vcAccountCode LIKE c.vcAccountCode + '%' AND PL.numParentAccID>0))
                                                          AND ATD.numDomainId = @numDomainId
				--WHERE  PL.bitIsSubAccount=0
                GROUP BY ATD.numAccountTypeID,
                        ATD.vcAccountCode,
                        ATD.vcAccountType,
                        ATD.numParentID ;

ALTER TABLE #PLSummary
DROP COLUMN bitIsSubAccount


-- GETTING P&L VALUE

DECLARE @CURRENTPL MONEY ;
DECLARE @PLOPENING MONEY;
DECLARE @PLCHARTID NUMERIC(8)
DECLARE @TotalIncome MONEY;
DECLARE @TotalExpense MONEY;
DECLARE @TotalCOGS MONEY;
DECLARE  @CurrentPL_COA money
SET @CURRENTPL =0;	
SET @PLOPENING=0;


SELECT @PLCHARTID=COA.numAccountId FROM Chart_of_Accounts COA WHERE numDomainID=@numDomainId and
bitProfitLoss=1;

SELECT @CurrentPL_COA =  ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE numDomainID=@numDomainID
AND numAccountId=@PLCHARTID AND datEntry_Date between  @dtFromDate AND @dtToDate;

SELECT  @TotalIncome= ISNULL(sum(Opening),0) + ISNULL(sum(Credit),0) -ISNULL(sum(Debit),0) FROM
#PLOutPut P WHERE 
vcAccountCode IN ('0103')

SELECT  @TotalExpense=ISNULL(sum(Opening),0)+ ISNULL(sum(Credit),0) -ISNULL(sum(Debit),0) FROM
#PLOutPut P WHERE 
vcAccountCode IN ('0104')

SELECT  @TotalCOGS= ISNULL(sum(Opening),0) + ISNULL(sum(Credit),0) -ISNULL(sum(Debit),0) FROM
#PLOutPut P WHERE 
vcAccountCode IN ('0106')

PRINT 'TotalIncome=									' + CAST(@TotalIncome AS VARCHAR(20))
PRINT 'TotalExpense=									'+ CAST(@TotalExpense AS VARCHAR(20))
PRINT 'TotalCOGS=										'+ CAST(@TotalCOGS AS VARCHAR(20))

SELECT @CURRENTPL = @CurrentPL_COA + @TotalIncome + @TotalExpense + @TotalCOGS 
PRINT 'Current Profit/Loss = (Income - expense - cogs)= ' + CAST( @CURRENTPL AS VARCHAR(20))

set @CURRENTPL=@CURRENTPL * (-1)

SELECT @PLOPENING = ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE numDomainID=@numDomainID 
AND ( vcAccountCode  LIKE '0103%' or  vcAccountCode  LIKE '0104%' or  vcAccountCode  LIKE '0106%')
AND datEntry_Date <=  DATEADD(Minute,-1,@dtFromDate);

SELECT @PLOPENING = ISNULL(@PLOPENING,0) + ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE numDomainID=@numDomainID
AND numAccountId=@PLCHARTID AND datEntry_Date <=  DATEADD(Minute,-1,@dtFromDate);

set @CURRENTPL=@CURRENTPL * (-1)
--------------------------------------------------------
		--UPDATE #PLSummary SET Opening = @PLOPENING WHERE #PLSummary.vcAccountCode = '0105010102'

		SELECT  P.numAccountId,
				P.vcAccountName,
				P.numParntAcntTypeID, 
				P.vcAccountDescription,
				P.vcAccountCode,
				(CASE WHEN P.[vcAccountCode] = '0105010102' THEN -@PLOPENING ELSE P.Opening END) AS Opening,
				P.Debit ,
				P.Credit,
				P.numParentAccID,
                (CASE WHEN P.[vcAccountCode] = '0105010102' THEN -(@PLOPENING + @CURRENTPL) ELSE Opening + Debit - Credit END) AS Balance,
                CASE WHEN LEN(P.[vcAccountCode])>4 THEN REPLICATE('&nbsp;', LEN(P.[vcAccountCode])-4 ) + P.[vcAccountCode] 
				ELSE P.[vcAccountCode]
                END AS AccountCode1,
                CASE WHEN LEN(P.[vcAccountCode]) > 4 THEN REPLICATE('&nbsp;', LEN(P.[vcAccountCode]) - 4 ) + P.[vcAccountName] 
                     ELSE P.[vcAccountName]
                END AS vcAccountName1,
                1 AS TYPE
        FROM    #PLSummary P
                LEFT JOIN dbo.Chart_Of_Accounts C ON C.numAccountId = P.numAccountId
        WHERE   LEN(ISNULL(P.[vcAccountCode], '')) > 2
        UNION
        SELECT  O.numAccountId,
				O.vcAccountName,
				O.numParntAcntTypeID, 
				O.vcAccountDescription,
				O.vcAccountCode,
				(CASE WHEN O.[vcAccountCode] = '0105010102' THEN -@PLOPENING ELSE O.Opening END) AS Opening,
				O.Debit ,
				O.Credit,
				O.numParentAccID,
                (CASE WHEN O.[vcAccountCode] = '0105010102' THEN -(@PLOPENING + @CURRENTPL) ELSE Opening + Debit - Credit END) AS Balance,
                CASE WHEN LEN(O.[vcAccountCode]) > 4
                     THEN REPLICATE('&nbsp;',LEN(O.[vcAccountCode]) - 4) + O.[vcAccountCode]
                     ELSE O.[vcAccountCode]
                END AS AccountCode1,
                CASE WHEN LEN(O.[vcAccountCode]) > 4
                      THEN REPLICATE('&nbsp;',LEN(O.[vcAccountCode]) - 4) + O.[vcAccountName]
                     ELSE O.[vcAccountName]
                END AS vcAccountName1,
                2 AS TYPE
                --CAST(O.[vcAccountCode] AS deci)  vcSortableAccountCode
        FROM    #PLOutPut O
--                LEFT JOIN dbo.Chart_Of_Accounts C ON C.numAccountId = O.numAccountId
        WHERE   LEN(ISNULL(O.[vcAccountCode], '')) > 2
        ORDER BY vcAccountCode,TYPE 
 
    --SELECT * FROM #PLOutPut ;
    --SELECT * FROM #PLSummary ;

        SELECT  (SUM(Opening) - @PLOPENING) AS OpeningTotal,
                SUM(Debit) AS DebitTotal,
                SUM(Credit) AS CreditTotal,
                (SUM(Opening) + SUM(Debit) - SUM(Credit) - @PLOPENING)AS BalanceTotal
        FROM    [#PLSummary]

        DROP TABLE #PLOutPut ;
        DROP TABLE #PLSummary ;


    END
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go


-- Created by Siva                                                      
-- [USP_GetChartAcntDetailsForProfitLoss] 72,'2008-09-28 14:22:53.937','2009-09-28 14:22:53.937'
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getchartacntdetailsforprofitloss')
DROP PROCEDURE usp_getchartacntdetailsforprofitloss
GO
CREATE PROCEDURE [dbo].[USP_GetChartAcntDetailsForProfitLoss]
@numDomainId as numeric(9),                                          
@dtFromDate as datetime,                                        
@dtToDate as DATETIME,
@ClientTimeZoneOffset INT, --Added by Chintan to enable calculation of date according to client machine                                                   
@numAccountClass AS NUMERIC(9)=0
As                                                        
Begin 
DECLARE @CURRENTPL MONEY ;
DECLARE @PLOPENING MONEY;
DECLARE @PLCHARTID NUMERIC(8)
--DECLARE @numFinYear INT;
DECLARE @TotalIncome MONEY;
DECLARE @TotalExpense MONEY;
DECLARE @TotalCOGS MONEY;
DECLARE @dtFinYearFrom datetime;


--set @numFinYear= (SELECT numFinYearId FROM FINANCIALYEAR WHERE dtPeriodFrom <= @dtFromDate AND  
--dtPeriodTo >=@dtFromDate AND numDomainId= @numDomainId);

--set @dtFinYearFrom = (SELECT dtPeriodFrom FROM FINANCIALYEAR WHERE dtPeriodFrom <= @dtFromDate AND  
--dtPeriodTo >=@dtFromDate AND numDomainId= @numDomainId);
SELECT * INTO #view_journal FROM view_journal WHERE numDomainID=@numDomainId AND (numAccountClass=@numAccountClass OR @numAccountClass=0);

SELECT @dtFinYearFrom=MIN(datEntry_Date) FROM #view_journal WHERE numDomainID=@numDomainId


SET @dtFromDate = DateAdd(minute, @ClientTimeZoneOffset, @dtFromDate);
SET @dtToDate = DateAdd(minute, @ClientTimeZoneOffset, @dtToDate);


--select * from view_journal where numDomainid=72

CREATE TABLE #PLSummary (numAccountId numeric(9),vcAccountName varchar(250),
numParntAcntTypeID numeric(9),vcAccountDescription varchar(250),
vcAccountCode varchar(50) COLLATE Database_Default,Opening money,Debit money,Credit MONEY,bitIsSubAccount Bit);

INSERT INTO  #PLSummary
SELECT COA.numAccountId,vcAccountName,numParntAcntTypeID,vcAccountDescription,vcAccountCode,

/*isnull((SELECT SUM(isnull(monOpening,0)) from CHARTACCOUNTOPENING CAO WHERE
numFinYearId=@numFinYear and numDomainID=@numDomainId 
AND CAO.numAccountId IN (SELECT numAccountId FROM dbo.Chart_Of_Accounts WHERE numDomainId = @numDomainId AND vcAccountCode like COA.vcAccountCode + '%' ) ),0) 

+*/

/*ISNULL((SELECT sum(Debit-Credit) FROM #view_journal VJ
WHERE VJ.numDomainId=@numDomainId AND
	VJ.COAvcAccountCode like COA.vcAccountCode + '%' AND
	datEntry_Date BETWEEN @dtFinYearFrom AND  DATEADD(Minute,-1,@dtFromDate) ),0)*/ 0 AS OPENING,

ISNULL((SELECT sum(Debit) FROM #view_journal VJ
WHERE VJ.numDomainId=@numDomainId AND
	VJ.COAvcAccountCode like COA.vcAccountCode + '%' /*VJ.numAccountId=COA.numAccountId*/ AND
	datEntry_Date BETWEEN  @dtFromDate AND @dtToDate ),0) as DEBIT,

ISNULL((SELECT sum(Credit) FROM #view_journal VJ
WHERE VJ.numDomainId=@numDomainId AND
	VJ.COAvcAccountCode like COA.vcAccountCode + '%' /*VJ.numAccountId=COA.numAccountId*/ AND
	datEntry_Date BETWEEN  @dtFromDate AND @dtToDate ),0) as CREDIT,
	ISNULL(COA.bitIsSubAccount,0)
FROM Chart_of_Accounts COA
WHERE COA.numDomainId=@numDomainId AND COA.bitActive = 1 AND 
      (COA.vcAccountCode LIKE '0103%' OR
       COA.vcAccountCode LIKE '0104%' OR
       COA.vcAccountCode LIKE '0106%')  ;

CREATE TABLE #PLOutPut (numAccountId numeric(9),vcAccountName varchar(250),
numParntAcntTypeID numeric(9),vcAccountDescription varchar(250),
vcAccountCode varchar(50) COLLATE Database_Default,Opening money,Debit money,Credit Money);

INSERT INTO #PLOutPut
SELECT ATD.numAccountTypeID,ATD.vcAccountType,ATD.numParentID, '',ATD.vcAccountCode,
 ISNULL(SUM(Opening),0) as Opening,
ISNUlL(Sum(Debit),0) as Debit,ISNULL(Sum(Credit),0) as Credit
FROM 
 AccountTypeDetail ATD RIGHT OUTER JOIN 
#PLSummary PL ON
PL.vcAccountCode LIKE ATD.vcAccountCode + '%'
AND ATD.numDomainId=@numDomainId AND
(ATD.vcAccountCode LIKE '0103%' OR
       ATD.vcAccountCode LIKE '0104%' OR
       ATD.vcAccountCode LIKE '0106%')
WHERE 
PL.bitIsSubAccount=0
GROUP BY 
ATD.numAccountTypeID,ATD.vcAccountCode,ATD.vcAccountType,ATD.numParentID;



ALTER TABLE #PLSummary
DROP COLUMN bitIsSubAccount

--SELECT * FROM #PLOutPut
--------------------------------------------------------
-- GETTING P&L VALUE
DECLARE  @CurrentPL_COA money
SET @CURRENTPL =0;	
SET @PLOPENING=0;


SELECT @PLCHARTID=COA.numAccountId FROM Chart_of_Accounts COA WHERE numDomainID=@numDomainId and
bitProfitLoss=1;

SELECT @CurrentPL_COA =  ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE numDomainID=@numDomainID
AND numAccountId=@PLCHARTID AND datEntry_Date between  @dtFromDate AND @dtToDate;

SELECT  @TotalIncome= ISNULL(sum(Opening),0) + ISNULL(sum(Credit),0) -ISNULL(sum(Debit),0) FROM
#PLOutPut P WHERE 
vcAccountCode IN ('0103')

SELECT  @TotalExpense=ISNULL(sum(Opening),0)+ ISNULL(sum(Credit),0) -ISNULL(sum(Debit),0) FROM
#PLOutPut P WHERE 
vcAccountCode IN ('0104')

SELECT  @TotalCOGS= ISNULL(sum(Opening),0) + ISNULL(sum(Credit),0) -ISNULL(sum(Debit),0) FROM
#PLOutPut P WHERE 
vcAccountCode IN ('0106')

PRINT 'TotalIncome=									' + CAST(@TotalIncome AS VARCHAR(20))
PRINT 'TotalExpense=									'+ CAST(@TotalExpense AS VARCHAR(20))
PRINT 'TotalCOGS=										'+ CAST(@TotalCOGS AS VARCHAR(20))

SELECT @CURRENTPL = @CurrentPL_COA + @TotalIncome + @TotalExpense + @TotalCOGS 
PRINT 'Current Profit/Loss = (Income - expense - cogs)= ' + CAST( @CURRENTPL AS VARCHAR(20))
--PRINT @CURRENTPL


--SELECT @CURRENTPL = @CURRENTPL - ISNULL(SUM(Opening),0)+ISNULL(sum(Debit),0)-ISNULL(sum(Credit),0) FROM
--#PLOutPut P WHERE 
--vcAccountCode IN ('0104')

set @CURRENTPL=@CURRENTPL * (-1)



SELECT @PLOPENING = ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE numDomainID=@numDomainID 
AND ( vcAccountCode  LIKE '0103%' or  vcAccountCode  LIKE '0104%' or  vcAccountCode  LIKE '0106%')
AND datEntry_Date <=  DATEADD(Minute,-1,@dtFromDate);

SELECT @PLOPENING = ISNULL(@PLOPENING,0) + ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE numDomainID=@numDomainID
AND numAccountId=@PLCHARTID AND datEntry_Date <=  DATEADD(Minute,-1,@dtFromDate);
--PRINT @PLCHARTID
--SELECT sum(Credit-Debit) FROM #view_journal VJ
--WHERE VJ.numDomainId=@numDomainId AND
--	VJ.numAccountId=@PLCHARTID
--	AND datEntry_Date <= @dtFromDate-1
--	
--	PRINT @dtFromDate
	
/*SELECT   @PLOPENING  = 
ISNULL((SELECT sum(Credit-Debit) FROM #view_journal VJ
WHERE VJ.numDomainId=@numDomainId AND
	VJ.numAccountId=COA.numAccountId 
	AND datEntry_Date <= @dtFromDate-1 ),0)
 FROM Chart_of_Accounts COA WHERE numDomainID=@numDomainId and
bitProfitLoss=1;*/
/*SELECT @PLOPENING = ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #PLOutPut P WHERE numDomainID=@numDomainID 

AND  vcAccountCode  IN( '0103' ,'0104' ,'0106')
AND datEntry_Date <=  DATEADD(Minute,-1,@dtFromDate);

SELECT @PLOPENING = ISNULL(@PLOPENING,0) + ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #PLOutPut P WHERE numDomainID=@numDomainID
AND numAccountId=@PLCHARTID
*/
--SELECT  @CURRENTPL=@CURRENTPL + 		   
--ISNULL((SELECT sum(Credit-Debit) FROM #view_journal VJ
--WHERE VJ.numDomainId=@numDomainId AND
--	VJ.numAccountId=@PLCHARTID AND
--	datEntry_Date BETWEEN @dtFromDate AND @dtToDate),0) ;

SET @CURRENTPL=@CURRENTPL * (-1)
--SELECT @PLCHARTID,@CURRENTPL,@PLOPENING

-----------------------------------------------------------------

 CREATE TABLE #PLShow (numAccountId numeric(9),vcAccountName varchar(250),
numParntAcntTypeID numeric(9),vcAccountDescription varchar(250),
vcAccountCode varchar(50),Opening money,Debit money,Credit Money,
Balance varchar(50),AccountCode1  varchar(100),vcAccountName1 varchar(250),[Type] INT);

INSERT INTO #PLShow
 
SELECT *,CAST( (/*Opening +*/ Credit - Debit ) AS VARCHAR(50)) as Balance,
CASE 
 WHEN LEN(P.[vcAccountCode])>4 THEN REPLICATE('&nbsp;', LEN(P.[vcAccountCode])-4) + P.[vcAccountCode] 
 ELSE P.[vcAccountCode]
 END AS AccountCode1,
 CASE 
 WHEN LEN(P.[vcAccountCode])>4 THEN REPLICATE('&nbsp;', LEN(P.[vcAccountCode])-4) + P.[vcAccountName]
 ELSE P.[vcAccountName]
 END AS vcAccountName1, 1 AS Type
 FROM #PLSummary P
 
 UNION
 select  0,'',0,'','0103A',0,0,0, '________________' ,'','',2 
 UNION
 Select 0,'Total Income :',0,'','0103A1',0,0,0,CAST( @TotalIncome AS VARCHAR(50)) ,'','Total Income :',2
 UNION
 select  0,'',0,'','0103A2',0,0,0, '' ,'','',2 
 UNION
SELECT *,CAST( (Opening* case substring(O.[vcAccountCode],1,4) when '0103' then (-1) else 1 end)+
(Debit* case substring(O.[vcAccountCode],1,4) when '0103' then (-1) else 1 end)-
(Credit* case substring(O.[vcAccountCode],1,4) when '0103' then (-1) else 1 end)  AS VARCHAR(50)) as Balance,

CASE 
 WHEN LEN(O.[vcAccountCode])>4 THEN REPLICATE('&nbsp;', LEN(O.[vcAccountCode])-4) + O.[vcAccountCode] 
 ELSE O.[vcAccountCode]
 END AS AccountCode1,
 CASE 
 WHEN LEN(O.[vcAccountCode])>4 THEN REPLICATE('&nbsp;', LEN(O.[vcAccountCode])-4) + O.[vcAccountName]
 ELSE O.[vcAccountName]
 END AS vcAccountName1, 2 as Type
 FROM #PLOutPut O
 
 UNION
 select  0,'',0,'','0104A',0,0,0, '________________' ,'','',2 
 UNION
 Select 0,'Total Expense :',0,'','0104A1',0,0,0,CAST( @TotalExpense AS VARCHAR(50)) ,'','Total Expense :',2
 UNION
 select  0,'',0,'','0104A2',0,0,0, '' ,'','',2 
 -----------Added by chintan for COGS change
 UNION
 select  0,'',0,'','0106A',0,0,0, '________________' ,'','',2 
 UNION
 Select 0,'Total COGS :',0,'','0106A1',0,0,0,CAST( @TotalCOGS AS VARCHAR(50)) ,'','Total COGS :',2
 UNION
 select  0,'',0,'','0106A2',0,0,0, '' ,'','',2 
 -----------End of block, Added by chintan for COGS change

--COA.numAccountId,vcAccountName,numParntAcntTypeID,vcAccountDescription,vcAccountCode
UNION
Select @PLCHARTID,'Profit / (Loss) Current :',0,'','051',0,0,0, CASE WHEN @CURRENTPL <0 THEN '(' + CAST( @CURRENTPL  AS VARCHAR(50)) + ')' ELSE CAST(@CURRENTPL AS VARCHAR(50)) END  ,'','Profit / (Loss) Current :',3
UNION
select @PLCHARTID,'Profit / (Loss) Opening :',0,'','052',0,0,0,CASE WHEN @PLOPENING <0 THEN '(' + CAST(@PLOPENING AS VARCHAR(50)) + ')' ELSE CAST(@PLOPENING AS VARCHAR(50)) END  ,'','Profit / (Loss) Opening :',4
UNION
Select  @PLCHARTID, 'Net Profit/(Loss) :',0,'','053',0,0,0, CASE WHEN (@PLOPENING + @CURRENTPL) <0 THEN '(' + CAST(@PLOPENING + @CURRENTPL AS VARCHAR(50)) + ')' ELSE CAST(@PLOPENING + @CURRENTPL AS VARCHAR(50)) END  ,'','Net Profit/(Loss) :',5


select  A.numAccountId ,
        A.vcAccountName ,
        A.numParntAcntTypeID ,
        A.vcAccountDescription ,
        A.vcAccountCode ,
        A.Opening ,
        A.Debit ,
        A.Credit ,
        A.Balance ,
        A.AccountCode1 ,
        CASE WHEN LEN(A.[vcAccountCode]) > 4
                     THEN REPLICATE('&nbsp;',LEN(A.[vcAccountCode]) - 4 )+ A.[vcAccountName]
                     ELSE A.[vcAccountName]
                END AS vcAccountName1, A.vcAccountCode ,
        A.Type 
        from #PLShow A ORDER BY A.vcAccountCode ASC ;

DROP TABLE #PLOutPut;
DROP TABLE #PLSummary;
DROP TABLE #PLShow



End
-- exec USP_GetChartAcntDetailsForProfitLoss @numDomainId=171,@dtFromDate='2013-02-14 00:00:00:000',@dtToDate='2013-02-28 23:59:59:000',@ClientTimeZoneOffset=-330
/****** Object:  StoredProcedure [dbo].[USP_GetColumnConfiguration1]    Script Date: 07/26/2008 16:16:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_getcolumnconfiguration1' )
    DROP PROCEDURE usp_getcolumnconfiguration1
GO
CREATE PROCEDURE [dbo].[USP_GetColumnConfiguration1]
    @numDomainID NUMERIC(9) ,
    @numUserCntId NUMERIC(9) ,
    @FormId TINYINT ,
    @numtype NUMERIC(9) ,
    @numViewID NUMERIC(9) = 0
AS
    DECLARE @PageId AS TINYINT         
    IF @FormId = 10
        OR @FormId = 44
        SET @PageId = 4          
    ELSE
        IF @FormId = 11
            OR @FormId = 34
            OR @FormId = 35
            OR @FormId = 36
            OR @FormId = 96
            OR @FormId = 97
            SET @PageId = 1          
        ELSE
            IF @FormId = 12
                SET @PageId = 3          
            ELSE
                IF @FormId = 13
                    SET @PageId = 11       
                ELSE
                    IF ( @FormId = 14
                         AND ( @numtype = 1
                               OR @numtype = 3
                             )
                       )
                        OR @FormId = 33
                        OR @FormId = 38
                        OR @FormId = 39
                        SET @PageId = 2          
                    ELSE
                        IF @FormId = 14
                            AND ( @numtype = 2
                                  OR @numtype = 4
                                )
                            OR @FormId = 40
                            OR @FormId = 41
                            SET @PageId = 6  
                        ELSE
                            IF @FormId = 21
                                OR @FormId = 26
                                OR @FormId = 32
                                SET @PageId = 5   
                            ELSE
                                IF @FormId = 22
                                    OR @FormId = 30
                                    SET @PageId = 5    
                                ELSE
                                    IF @FormId = 76
                                        SET @PageId = 10 
                                    ELSE
                                        IF @FormId = 84
                                            OR @FormId = 85
                                            SET @PageId = 5
                     
    IF @PageId = 1
        OR @PageId = 4
        BEGIN          
            SELECT  CONVERT(VARCHAR(9), numFieldID) + '~0' AS numFieldID ,
                    ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                    0 AS Custom
            FROM    View_DynamicDefaultColumns
            WHERE   numFormID = @FormId
                    AND ISNULL(bitSettingField, 0) = 1
                    AND ISNULL(bitDeleted, 0) = 0
                    AND numDomainID = @numDomainID
            UNION
            SELECT  CONVERT(VARCHAR(9), fld_id) + '~1' AS numFieldID ,
                    fld_label AS vcFieldName ,
                    1 AS Custom
            FROM    CFW_Fld_Master
                    LEFT JOIN CFW_Fld_Dtl ON Fld_id = numFieldId
                                             AND numRelation = CASE
                                                              WHEN @numtype IN (
                                                              1, 2, 3 )
                                                              THEN numRelation
                                                              ELSE @numtype
                                                              END
                    LEFT JOIN CFw_Grp_Master ON subgrp = CFw_Grp_Master.Grp_id
            WHERE   CFW_Fld_Master.grp_id = @PageId
                    AND CFW_Fld_Master.numDomainID = @numDomainID
                    AND Fld_type <> 'Link'
            ORDER BY Custom ,
                    vcFieldName                                       
        END  
    ELSE IF @PageId = 10
    BEGIN          
                SELECT  CONVERT(VARCHAR(9), numFieldID) + '~0' AS numFieldID ,
                        ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                        0 AS Custom
                FROM    View_DynamicDefaultColumns
                WHERE   numFormID = @FormId
                        AND ISNULL(bitSettingField, 0) = 1
                        AND ISNULL(bitDeleted, 0) = 0
                        AND numDomainID = @numDomainID
                UNION
                SELECT  CONVERT(VARCHAR(9), fld_id) + '~1' AS numFieldID ,
                        fld_label AS vcFieldName ,
                        1 AS Custom
                FROM    CFW_Fld_Master
                        LEFT JOIN CFW_Fld_Dtl ON Fld_id = numFieldId
                                                 AND numRelation = CASE
                                                              WHEN @numtype IN (
                                                              1, 2, 3 )
                                                              THEN numRelation
                                                              ELSE @numtype
                                                              END
                        LEFT JOIN CFw_Grp_Master ON subgrp = CFw_Grp_Master.Grp_id
                WHERE   CFW_Fld_Master.grp_id = 5
                        AND CFW_Fld_Master.numDomainID = @numDomainID
                        AND Fld_Type = 'SelectBox'
                ORDER BY Custom ,
                        vcFieldName       
	                                     
            END 
	ELSE IF @FormId = 125 AND @PageId=0
	BEGIN
			SELECT  CONVERT(VARCHAR(9), numFieldID) + '~0' AS numFieldID ,
                    ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                    0 AS Custom
            FROM    View_DynamicDefaultColumns
            WHERE   numFormID = @FormId
                    AND ISNULL(bitSettingField, 0) = 1
                    AND ISNULL(bitDeleted, 0) = 0
                    AND numDomainID = @numDomainID
	END

        ELSE
            IF @PageId = 5
                AND ( @FormId = 84
                      OR @FormId = 85
                    )
                BEGIN          
                    SELECT  CONVERT(VARCHAR(9), numFieldID) + '~0' AS numFieldID ,
                            ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                            0 AS Custom ,
                            vcDbColumnName
                    FROM    View_DynamicDefaultColumns
                    WHERE   numFormID = @FormId
                            AND ISNULL(bitSettingField, 0) = 1
                            AND ISNULL(bitDeleted, 0) = 0
                            AND numDomainID = @numDomainID
                    UNION
                    SELECT  CONVERT(VARCHAR(9), fld_id) + '~1' AS numFieldID ,
                            fld_label AS vcFieldName ,
                            1 AS Custom ,
                            CONVERT(VARCHAR(10), fld_id) AS vcDbColumnName
                    FROM    CFW_Fld_Master
                            LEFT JOIN CFw_Grp_Master ON subgrp = CFw_Grp_Master.Grp_id
                    WHERE   CFW_Fld_Master.grp_id = @PageId
                            AND CFW_Fld_Master.numDomainID = @numDomainID
                            AND Fld_type = 'TextBox'
                    ORDER BY Custom ,
                            vcFieldName             
                END  
             
            ELSE
                BEGIN          
      
                    SELECT  CONVERT(VARCHAR(9), numFieldID) + '~0' AS numFieldID ,
                            ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                            0 AS Custom ,
                            vcDbColumnName
                    FROM    View_DynamicDefaultColumns
                    WHERE   numFormID = @FormId
                            AND ISNULL(bitSettingField, 0) = 1
                            AND ISNULL(bitDeleted, 0) = 0
                            AND numDomainID = @numDomainID
                    UNION
                    SELECT  CONVERT(VARCHAR(9), fld_id) + '~1' AS numFieldID ,
                            fld_label AS vcFieldName ,
                            1 AS Custom ,
                            CONVERT(VARCHAR(10), fld_id) AS vcDbColumnName
                    FROM    CFW_Fld_Master
                            LEFT JOIN CFw_Grp_Master ON subgrp = CFw_Grp_Master.Grp_id
                    WHERE   CFW_Fld_Master.grp_id = @PageId
                            AND CFW_Fld_Master.numDomainID = @numDomainID
                            AND Fld_type <> 'Link'
                    ORDER BY Custom ,
                            vcFieldName             
                END                      
                      
                   
    IF @PageId = 1
        OR @PageId = 4
        BEGIN
 
            IF ( @FormId = 96
                 OR @FormId = 97
               )
                SELECT  CONVERT(VARCHAR(9), numFieldID) + '~0' numFieldID ,
                        ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                        vcDbColumnName ,
                        0 AS Custom ,
                        tintRow AS tintOrder
                FROM    View_DynamicColumns
                WHERE   numFormId = @FormId
                        AND numUserCntID = @numUserCntID
                        AND numDomainID = @numDomainID
                        AND ISNULL(bitCustom, 0) = 0
                        AND tintPageType = 1
                        AND ISNULL(bitSettingField, 0) = 1
                        AND numRelCntType = @numtype
                        AND ISNULL(bitDeleted, 0) = 0
                UNION
                SELECT  CONVERT(VARCHAR(9), numFieldID) + '~1' AS numFieldID ,
                        vcFieldName ,
                        vcFieldName AS vcDbColumnName ,
                        1 AS Custom ,
                        tintRow AS tintOrder
                FROM    View_DynamicCustomColumns
                WHERE   numFormID = @FormId
                        AND numUserCntID = @numUserCntID
                        AND numDomainID = @numDomainID
                        AND grp_id = @PageId
                        AND numDomainID = @numDomainID
                        AND vcAssociatedControlType <> 'Link'
                        AND ISNULL(bitCustom, 0) = 1
                        AND tintPageType = 1
                        AND numRelCntType = @numtype
                ORDER BY tintOrder                
            ELSE
                SELECT  CONVERT(VARCHAR(9), numFieldID) + '~0' numFieldID ,
                        ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                        vcDbColumnName ,
                        0 AS Custom ,
                        tintRow AS tintOrder
                FROM    View_DynamicColumns
                WHERE   numFormId = @FormId
                        AND numUserCntID = @numUserCntID
                        AND numDomainID = @numDomainID
                        AND ISNULL(bitCustom, 0) = 0
                        AND tintPageType = 1
                        AND ISNULL(bitSettingField, 0) = 1
                        AND numRelCntType = @numtype
                        AND ISNULL(bitDeleted, 0) = 0
                UNION
                SELECT  CONVERT(VARCHAR(9), numFieldID) + '~1' AS numFieldID ,
                        vcFieldName ,
                        '' AS vcDbColumnName ,
                        1 AS Custom ,
                        tintRow AS tintOrder
                FROM    View_DynamicCustomColumns
                WHERE   numFormID = @FormId
                        AND numUserCntID = @numUserCntID
                        AND numDomainID = @numDomainID
                        AND grp_id = @PageId
                        AND numDomainID = @numDomainID
                        AND vcAssociatedControlType <> 'Link'
                        AND ISNULL(bitCustom, 0) = 1
                        AND tintPageType = 1
                        AND numRelCntType = @numtype
                ORDER BY tintOrder          
        END     
    ELSE
        IF @PageId = 10
            BEGIN
	
                SELECT  CONVERT(VARCHAR(9), numFieldID) + '~0' numFieldID ,
                        ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                        vcDbColumnName ,
                        0 AS Custom ,
                        tintRow AS tintOrder ,
                        numListID ,
                        numFieldID AS [numFieldID1]
                FROM    View_DynamicColumns
                WHERE   numFormId = @FormId
                        AND numUserCntID = @numUserCntID
                        AND numDomainID = @numDomainID
                        AND ISNULL(bitCustom, 0) = 0
                        AND tintPageType = 1
                        AND ISNULL(bitSettingField, 0) = 1
                        AND numRelCntType = @numtype
                        AND ISNULL(bitDeleted, 0) = 0
                UNION
                SELECT  CONVERT(VARCHAR(9), numFieldID) + '~1' AS numFieldID ,
                        vcFieldName ,
                        '' AS vcDbColumnName ,
                        1 AS Custom ,
                        tintRow AS tintOrder ,
                        numListID ,
                        numFieldID AS [numFieldID1]
                FROM    View_DynamicCustomColumns
                WHERE   numFormID = @FormId
                        AND numUserCntID = @numUserCntID
                        AND numDomainID = @numDomainID   
   --and grp_id=@PageId    
                        AND numDomainID = @numDomainID
                        AND vcAssociatedControlType = 'SelectBox'
                        AND ISNULL(bitCustom, 0) = 1
                        AND tintPageType = 1
                        AND numRelCntType = @numtype
                ORDER BY tintOrder    
--   
--    SELECT DISTINCT numFieldID as numFieldID,'Archived Items' AS vcFieldName,bitCustom as Custom 
--	   FROM DycFormConfigurationDetails
--	   WHERE 1=1
--	   AND numFormID = @FormId
--	   AND ISNULL(numFieldID,0)= -1
--	   AND numDomainID = @numDomainID	
            END	   
--ELSE IF @PageId = 12
--	BEGIN
--		SELECT  CONVERT(VARCHAR(9),numFieldID) + '~0' numFieldID,
--				ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName,
--				vcDbColumnName ,
--				0 AS Custom,
--				tintRow AS tintOrder,
--				vcAssociatedControlType,
--				numlistid,
--				vcLookBackTableName,
--				numFieldID AS FieldId,
--				bitAllowEdit                    
--		FROM View_DynamicColumns
--		WHERE numFormId = @FormId 
--		AND numUserCntID = @numUserCntID 
--		AND  numDomainID = @numDomainID  
--	    AND ISNULL(bitCustom,0) = 0 
--		AND tintPageType = 1 
--		AND ISNULL(bitSettingField,0) = 1 
--		AND ISNULL(numRelCntType,0) = @numtype 
--		AND ISNULL(bitDeleted,0) = 0
--	    AND  ISNULL(numViewID,0) = @numViewID   
--	   
--	    UNION
--	   
--	    SELECT  CONVERT(VARCHAR(9),numFieldID)+'~1' AS numFieldID ,
--				vcFieldName ,'' AS vcDbColumnName,
--				1 AS Custom,
--				tintRow AS tintOrder,
--				vcAssociatedControlType,
--				numlistid,'' AS vcLookBackTableName,
--				numFieldID AS FieldId,
--				0 AS bitAllowEdit           
--	    FROM View_DynamicCustomColumns          
--	    WHERE numFormID = @FormId 
--		AND numUserCntID = @numUserCntID 
--		--AND numDomainID=@numDomainID
--		AND grp_id = @PageId  
--		AND numDomainID = @numDomainID  
--		AND vcAssociatedControlType <> 'Link' 
--		AND ISNULL(bitCustom,0) = 1 
--		--AND tintPageType = 1  
--		---AND ISNULL(numRelCntType,0)= @numtype     
--		--AND  ISNULL(numViewID,0) = @numViewID
--	    ORDER BY tintOrder     
--
--	END 
        ELSE
            IF @PageId = 5
                AND ( @FormId = 84
                      OR @FormId = 85
                    )
                BEGIN

                    IF EXISTS ( SELECT  'col1'
                                FROM    DycFormConfigurationDetails
                                WHERE   numDomainID = @numDomainID
                                        AND numFormId = @FormID
                                        AND numFieldID IN ( 507, 508, 509, 510,
                                                            511, 512 ) )
                        BEGIN
                            SELECT  CONVERT(VARCHAR(9), numFieldID) + '~0' numFieldID ,
                                    ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                                    vcDbColumnName ,
                                    0 AS Custom ,
                                    tintRow AS tintOrder ,
                                    vcAssociatedControlType ,
                                    numlistid ,
                                    vcLookBackTableName ,
                                    numFieldID AS FieldId ,
                                    bitAllowEdit ,
                                    0 AS AZordering
                            FROM    View_DynamicColumns
                            WHERE   numFormId = @FormID
                                    AND numDomainID = @numDomainID
                                    AND ISNULL(bitCustom, 0) = 0   
		 --AND tintPageType = 1   
                                    AND ISNULL(bitSettingField, 0) = 1
                                    AND ISNULL(bitDeleted, 0) = 0
                            UNION
                            SELECT  CONVERT(VARCHAR(9), numFieldID) + '~1' AS numFieldID ,
                                    vcFieldName ,
                                    '' AS vcDbColumnName ,
                                    1 AS Custom ,
                                    tintRow AS tintOrder ,
                                    vcAssociatedControlType ,
                                    numlistid ,
                                    '' AS vcLookBackTableName ,
                                    numFieldID AS FieldId ,
                                    0 AS bitAllowEdit ,
                                    0 AS AZordering
                            FROM    View_DynamicCustomColumns
                            WHERE   numFormID = @FormID
                                    AND numDomainID = @numDomainID
                                    AND grp_id = @PageID
                                    AND vcAssociatedControlType = 'TextBox'
                                    AND ISNULL(bitCustom, 0) = 1
                            ORDER BY tintOrder 
                        END
                    ELSE
                        BEGIN
                            SELECT  CONVERT(VARCHAR(9), numFieldID) + '~0' numFieldID ,
                                    ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                                    vcDbColumnName ,
                                    0 AS Custom ,
                                    tintRow AS tintOrder ,
                                    vcAssociatedControlType ,
                                    numlistid ,
                                    vcLookBackTableName ,
                                    numFieldID AS FieldId ,
                                    bitAllowEdit ,
                                    0 AS AZordering
                            FROM    View_DynamicColumns
                            WHERE   numFormId = @FormID
                                    AND numDomainID = @numDomainID
                                    AND ISNULL(bitCustom, 0) = 0   
		 --AND tintPageType = 1   
                                    AND ISNULL(bitSettingField, 0) = 1
                                    AND ISNULL(bitDeleted, 0) = 0
                            UNION
                            SELECT  CONVERT(VARCHAR(9), numFieldID) + '~1' AS numFieldID ,
                                    vcFieldName ,
                                    '' AS vcDbColumnName ,
                                    1 AS Custom ,
                                    tintRow AS tintOrder ,
                                    vcAssociatedControlType ,
                                    numlistid ,
                                    '' AS vcLookBackTableName ,
                                    numFieldID AS FieldId ,
                                    0 AS bitAllowEdit ,
                                    0 AS AZordering
                            FROM    View_DynamicCustomColumns
                            WHERE   numFormID = @FormID
                                    AND numDomainID = @numDomainID
                                    AND grp_id = @PageID
                                    AND vcAssociatedControlType = 'TextBox'
                                    AND ISNULL(bitCustom, 0) = 1
                            UNION
                            SELECT  '507~0' AS numFieldID ,
                                    'Title:A-Z' AS vcFieldName ,
                                    'Title:A-Z' AS vcDbColumnName ,
                                    1 AS Custom ,
                                    0 AS tintOrder ,
                                    ' ' AS vcAssociatedControlType ,
                                    1 AS numlistid ,
                                    '' AS vcLookBackTableName ,
                                    507 AS FieldId ,
                                    0 AS bitAllowEdit ,
                                    1 AS IsFixed
                            UNION
                            SELECT  '508~0' AS numFieldID ,
                                    'Title:Z-A' AS vcFieldName ,
                                    'Title:Z-A' AS vcDbColumnName ,
                                    1 AS Custom ,
                                    0 AS tintOrder ,
                                    ' ' AS vcAssociatedControlType ,
                                    1 AS numlistid ,
                                    '' AS vcLookBackTableName ,
                                    508 AS FieldId ,
                                    0 AS bitAllowEdit ,
                                    1 AS IsFixed
                            UNION
                            SELECT  '509~0' AS numFieldID ,
                                    'Price:Low to High' AS vcFieldName ,
                                    'Price:Low to High' AS vcDbColumnName ,
                                    1 AS Custom ,
                                    0 AS tintOrder ,
                                    ' ' AS vcAssociatedControlType ,
                                    1 AS numlistid ,
                                    '' AS vcLookBackTableName ,
                                    509 AS FieldId ,
                                    0 AS bitAllowEdit ,
                                    1 AS IsFixed
                            UNION
                            SELECT  '510~0' AS numFieldID ,
                                    'Price:High to Low' AS vcFieldName ,
                                    'Price:High to Low' AS vcDbColumnName ,
                                    1 AS Custom ,
                                    0 AS tintOrder ,
                                    ' ' AS vcAssociatedControlType ,
                                    1 AS numlistid ,
                                    '' AS vcLookBackTableName ,
                                    510 AS FieldId ,
                                    0 AS bitAllowEdit ,
                                    1 AS IsFixed
                            UNION
                            SELECT  '511~0' AS numFieldID ,
                                    'New Arrival' AS vcFieldName ,
                                    'New Arrival' AS vcDbColumnName ,
                                    1 AS Custom ,
                                    0 AS tintOrder ,
                                    ' ' AS vcAssociatedControlType ,
                                    1 AS numlistid ,
                                    '' AS vcLookBackTableName ,
                                    511 AS FieldId ,
                                    0 AS bitAllowEdit ,
                                    1 AS IsFixed
                            UNION
                            SELECT  '512~0' AS numFieldID ,
                                    'Oldest' AS vcFieldName ,
                                    'Oldest' AS vcDbColumnName ,
                                    1 AS Custom ,
                                    0 AS tintOrder ,
                                    ' ' AS vcAssociatedControlType ,
                                    1 AS numlistid ,
                                    '' AS vcLookBackTableName ,
                                    512 AS FieldId ,
                                    0 AS bitAllowEdit ,
                                    1 AS IsFixed
                            ORDER BY tintOrder 

                        END



    

                END
            ELSE
                BEGIN  
 
                    SELECT  CONVERT(VARCHAR(9), numFieldID) + '~0' numFieldID ,
                            ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                            vcDbColumnName ,
                            0 AS Custom ,
                            tintRow AS tintOrder ,
                            vcAssociatedControlType ,
                            numlistid ,
                            vcLookBackTableName ,
                            numFieldID AS FieldId ,
                            bitAllowEdit
                    FROM    View_DynamicColumns
                    WHERE   numFormId = @FormId
                            AND numUserCntID = @numUserCntID
                            AND numDomainID = @numDomainID
                            AND ISNULL(bitCustom, 0) = 0
                            AND tintPageType = 1
                            AND ISNULL(bitSettingField, 0) = 1
                            AND ISNULL(numRelCntType, 0) = @numtype
                            AND ISNULL(bitDeleted, 0) = 0
                            AND ISNULL(numViewID, 0) = @numViewID
                    UNION
                    SELECT  CONVERT(VARCHAR(9), numFieldID) + '~1' AS numFieldID ,
                            vcFieldName ,
                            '' AS vcDbColumnName ,
                            1 AS Custom ,
                            tintRow AS tintOrder ,
                            vcAssociatedControlType ,
                            numlistid ,
                            '' AS vcLookBackTableName ,
                            numFieldID AS FieldId ,
                            0 AS bitAllowEdit
                    FROM    View_DynamicCustomColumns
                    WHERE   numFormID = @FormId
                            AND numUserCntID = @numUserCntID
                            AND numDomainID = @numDomainID
                            AND grp_id = @PageId
                            AND numDomainID = @numDomainID
                            AND vcAssociatedControlType <> 'Link'
                            AND ISNULL(bitCustom, 0) = 1
                            AND tintPageType = 1
                            AND ISNULL(numRelCntType, 0) = @numtype
                            AND ISNULL(numViewID, 0) = @numViewID
                    ORDER BY tintOrder          
                END
GO
/****** Object:  StoredProcedure [USP_GetCustomerCredits]    ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetCustomerCredits')
DROP PROCEDURE USP_GetCustomerCredits
GO
CREATE PROCEDURE [USP_GetCustomerCredits]                              
(
    @numDomainId numeric(18, 0),
    @numDivisionId numeric(18, 0),
    @numReferenceId NUMERIC(18,0),
    @tintMode TINYINT,
    @numCurrencyID NUMERIC(18,0)=0,
    @numAccountClass NUMERIC(18,0)=0
)            
AS                                           
BEGIN 

    DECLARE @numBaseCurrencyID NUMERIC
	    DECLARE @BaseCurrencySymbol nVARCHAR(3);SET @BaseCurrencySymbol='$'
	    
        SELECT @numBaseCurrencyID = ISNULL(numCurrencyID,0) FROM dbo.Domain WHERE numDomainId=@numDomainId
        SELECT @BaseCurrencySymbol = ISNULL(varCurrSymbol,'$') FROM dbo.Currency WHERE numCurrencyID = @numBaseCurrencyID
    
    
IF @tintMode=1
BEGIN
	
	DECLARE @tintDepositePage AS TINYINT;SET @tintDepositePage=0
	
	IF @numReferenceId>0
	BEGIN
		SELECT @tintDepositePage=tintDepositePage FROM depositMaster WHERE numDepositID=@numReferenceId
	END
	
	PRINT @tintDepositePage
	
	IF @tintDepositePage=3
	BEGIN
		SELECT tintDepositePage AS tintRefType,
		CASE WHEN tintDepositePage=3 THEN 'Credit Memo #' + CAST((SELECT TOP 1 [ReturnHeader].[vcRMA] FROM [dbo].[ReturnHeader] WHERE [ReturnHeader].[numReturnHeaderID] = DM.[numReturnHeaderID]) AS VARCHAR(20)) ELSE 'Unapplied Payment' END AS vcReference,
		numDepositId AS numReferenceID,CAST(monDepositAmount AS DECIMAL(19,2)) AS monAmount,
		ISNULL(CAST(monDepositAmount AS DECIMAL(19,2)),0) - ISNULL(CAST(monRefundAmount AS DECIMAL(19,2)),0) AS monNonAppliedAmount,
		ISNULL(numReturnHeaderID,0) AS numReturnHeaderID,CAST (1 AS BIT) AS bitIsPaid,ISNULL(CAST(monAppliedAmount AS DECIMAL(19,2)),0) AS monAmountPaid,
		DM.numCurrencyID,ISNULL(C.varCurrSymbol,'') varCurrSymbol,ISNULL(NULLIF(DM.fltExchangeRate,0),1) fltExchangeRateReceivedPayment
		,@BaseCurrencySymbol BaseCurrencySymbol,CASE WHEN tintDepositePage=3 THEN '' ELSE ISNULL(DM.vcReference,'') END  AS vcDepositReference 
		FROM DepositMaster DM LEFT JOIN dbo.Currency C ON C.numCurrencyID = DM.numCurrencyID
		WHERE DM.numDomainId=@numDomainId AND numDivisionID=@numDivisionID AND tintDepositePage IN(3)
		AND numDepositId=@numReferenceId AND (ISNULL(DM.numCurrencyID,@numBaseCurrencyID) = @numCurrencyID OR @numCurrencyID =0)
		AND (ISNULL(DM.numAccountClass,0)=@numAccountClass OR @numAccountClass=0)
		 
		UNION
		
		SELECT tintDepositePage AS tintRefType,CASE WHEN tintDepositePage=3 THEN 'Credit Memo #' + CAST((SELECT TOP 1 [ReturnHeader].[vcRMA] FROM [dbo].[ReturnHeader] WHERE [ReturnHeader].[numReturnHeaderID] = DM.[numReturnHeaderID]) AS VARCHAR(20)) ELSE 'Unapplied Payment' END AS vcReference,
		numDepositId AS numReferenceID,CAST(monDepositAmount AS DECIMAL(19,2)) AS monAmount,
		ISNULL(CAST(monDepositAmount AS DECIMAL(19,2)),0) - ISNULL(CAST(monAppliedAmount AS DECIMAL(19,2)),0)  /*- ISNULL(CAST(monRefundAmount AS DECIMAL(19,2)),0)*/ AS monNonAppliedAmount,
		ISNULL(numReturnHeaderID,0) AS numReturnHeaderID,CAST (0 AS BIT) AS bitIsPaid,0 AS monAmountPaid,DM.numCurrencyID,ISNULL(C.varCurrSymbol,'') varCurrSymbol,
		ISNULL(NULLIF(DM.fltExchangeRate,0),1) fltExchangeRateReceivedPayment,@BaseCurrencySymbol BaseCurrencySymbol,
		CASE WHEN tintDepositePage=3 THEN '' ELSE ISNULL(DM.vcReference,'') END AS vcDepositReference  
		FROM DepositMaster DM LEFT JOIN dbo.Currency C ON C.numCurrencyID = DM.numCurrencyID
		WHERE DM.numDomainId=@numDomainId AND numDivisionID=@numDivisionID AND tintDepositePage IN(2,3)
		AND (ISNULL(monDepositAmount,0) - ISNULL(monAppliedAmount,0)) > 0 AND numDepositId!=@numReferenceId
		AND (ISNULL(DM.numCurrencyID,@numBaseCurrencyID) = @numCurrencyID OR @numCurrencyID =0)
		AND (ISNULL(DM.numAccountClass,0)=@numAccountClass OR @numAccountClass=0)
	END
	ELSE
	BEGIN
		SELECT tintDepositePage AS tintRefType,CASE WHEN tintDepositePage=3 THEN 'Credit Memo #' + CAST((SELECT TOP 1 [ReturnHeader].[vcRMA] FROM [dbo].[ReturnHeader] WHERE [ReturnHeader].[numReturnHeaderID] = DM.[numReturnHeaderID]) AS VARCHAR(20)) ELSE 'Unapplied Payment' END AS vcReference,
		numDepositId AS numReferenceID,CAST(monDepositAmount AS DECIMAL(19,2)) AS monAmount,
		ISNULL(CAST(monDepositAmount AS DECIMAL(19,2)),0) - ISNULL(CAST(monAppliedAmount AS DECIMAL(19,2)),0) /*- ISNULL(CAST(monRefundAmount AS DECIMAL(19,2)),0)*/ AS monNonAppliedAmount,
		ISNULL(numReturnHeaderID,0) AS numReturnHeaderID,CAST (0 AS BIT) AS bitIsPaid,0 AS monAmountPaid,DM.numCurrencyID,ISNULL(C.varCurrSymbol,'') varCurrSymbol
		,ISNULL(NULLIF(DM.fltExchangeRate,0),1) fltExchangeRateReceivedPayment,@BaseCurrencySymbol BaseCurrencySymbol,CASE WHEN tintDepositePage=3 THEN '' ELSE ISNULL(DM.vcReference,'') END  AS vcDepositReference 
		FROM DepositMaster DM LEFT JOIN dbo.Currency C ON C.numCurrencyID = DM.numCurrencyID
		WHERE DM.numDomainId=@numDomainId AND numDivisionID=@numDivisionID AND tintDepositePage IN(2,3)
		AND (ISNULL(CAST(monDepositAmount AS DECIMAL(19,2)),0) - ISNULL(CAST(monAppliedAmount AS DECIMAL(19,2)),0)) > 0 AND numDepositId!=@numReferenceId
		AND (ISNULL(DM.numCurrencyID,@numBaseCurrencyID) = @numCurrencyID OR @numCurrencyID =0)
		AND (ISNULL(DM.numAccountClass,0)=@numAccountClass OR @numAccountClass=0)
		ORDER BY numDepositId DESC
	END		
	
--UNION 
--
--SELECT 2 AS tintRefType,numReturnHeaderID AS numReferenceID,monAmount,monAmountUsed AS monAppliedAmount FROM ReturnHeader WHERE numDomainId=@numDomainId AND numDivisionID=@numDivisionID AND tintReturnType IN (1,4)
END

ELSE IF @tintMode=2
BEGIN

	DECLARE @numReturnHeaderID AS NUMERIC(18,0);SET @numReturnHeaderID=0
	
	IF @numReferenceId>0
	BEGIN
		SELECT @numReturnHeaderID=numReturnHeaderID FROM BillPaymentHeader WHERE numBillPaymentID=@numReferenceId
	END
	
	IF @numReturnHeaderID>0
	BEGIN
		SELECT 1 AS tintRefType,RH.vcRMA AS vcReference
		,BPH.numBillPaymentID AS numReferenceID,monPaymentAmount AS monAmount,ISNULL(monPaymentAmount,0) - ISNULL(monRefundAmount,0) AS monNonAppliedAmount,CAST (1 AS BIT) AS bitIsPaid,ISNULL(monAppliedAmount,0) AS monAmountPaid,ISNULL(BPH.numReturnHeaderID,0) AS numReturnHeaderID,
		BPH.numCurrencyID
						,ISNULL(C.varCurrSymbol,'') varCurrSymbol
						,ISNULL(NULLIF(BPH.fltExchangeRate,0),1) fltExchangeRateReceivedPayment
						,@BaseCurrencySymbol BaseCurrencySymbol
		FROM BillPaymentHeader BPH LEFT JOIN dbo.Currency C ON C.numCurrencyID = BPH.numCurrencyID 
		LEFT JOIN ReturnHeader RH ON RH.numReturnHeaderID=ISNULL(BPH.numReturnHeaderID,0)
		WHERE BPH.numDomainId=@numDomainId AND BPH.numDivisionID=@numDivisionID 
		 AND numBillPaymentID=@numReferenceId AND (ISNULL(BPH.numCurrencyID,@numBaseCurrencyID) = @numCurrencyID OR @numCurrencyID =0)
		 AND (ISNULL(BPH.numAccountClass,0)=@numAccountClass OR @numAccountClass=0)
		 
		UNION
		
		SELECT CASE WHEN ISNULL(BPH.numReturnHeaderID,0)>0 THEN 1 ELSE 0 END AS tintRefType,
			  CASE WHEN ISNULL(BPH.numReturnHeaderID,0)>0 THEN RH.vcRMA ELSE 'Unapplied Bill Payment' END AS vcReference
		,BPH.numBillPaymentID AS numReferenceID,monPaymentAmount AS monAmount,ISNULL(monPaymentAmount,0) - ISNULL(monAppliedAmount,0) - ISNULL(monRefundAmount,0) AS monNonAppliedAmount,CAST (0 AS BIT) AS bitIsPaid,0 AS monAmountPaid,ISNULL(BPH.numReturnHeaderID,0) AS numReturnHeaderID,
		BPH.numCurrencyID
						,ISNULL(C.varCurrSymbol,'') varCurrSymbol
						,ISNULL(NULLIF(BPH.fltExchangeRate,0),1) fltExchangeRateReceivedPayment
						,@BaseCurrencySymbol BaseCurrencySymbol
		FROM BillPaymentHeader BPH LEFT JOIN dbo.Currency C ON C.numCurrencyID = BPH.numCurrencyID 
		LEFT JOIN ReturnHeader RH ON RH.numReturnHeaderID=ISNULL(BPH.numReturnHeaderID,0)
		WHERE BPH.numDomainId=@numDomainId AND BPH.numDivisionID=@numDivisionID 
		AND (ISNULL(monPaymentAmount,0) - ISNULL(monAppliedAmount,0)) > 0 AND numBillPaymentID!=@numReferenceId
		AND (ISNULL(BPH.numCurrencyID,@numBaseCurrencyID) = @numCurrencyID OR @numCurrencyID =0)
		AND (ISNULL(BPH.numAccountClass,0)=@numAccountClass OR @numAccountClass=0)
		
	END
	ELSE
	BEGIN
		SELECT CASE WHEN ISNULL(BPH.numReturnHeaderID,0)>0 THEN 1 ELSE 0 END AS tintRefType,
			  CASE WHEN ISNULL(BPH.numReturnHeaderID,0)>0 THEN RH.vcRMA ELSE 'Unapplied Bill Payment' END AS vcReference
		,BPH.numBillPaymentID AS numReferenceID,monPaymentAmount AS monAmount,ISNULL(monPaymentAmount,0) - ISNULL(monAppliedAmount,0) - ISNULL(monRefundAmount,0) AS monNonAppliedAmount,CAST (0 AS BIT) AS bitIsPaid,0 AS monAmountPaid  ,ISNULL(BPH.numReturnHeaderID,0) AS numReturnHeaderID,
		BPH.numCurrencyID
						,ISNULL(C.varCurrSymbol,'') varCurrSymbol
						,ISNULL(NULLIF(BPH.fltExchangeRate,0),1) fltExchangeRateReceivedPayment
						,@BaseCurrencySymbol BaseCurrencySymbol
		FROM BillPaymentHeader BPH LEFT JOIN dbo.Currency C ON C.numCurrencyID = BPH.numCurrencyID 
		LEFT JOIN ReturnHeader RH ON RH.numReturnHeaderID=ISNULL(BPH.numReturnHeaderID,0)
		WHERE BPH.numDomainId=@numDomainId AND BPH.numDivisionID=@numDivisionID 
		AND (ISNULL(monPaymentAmount,0) - ISNULL(monAppliedAmount,0)) > 0 AND numBillPaymentID!=@numReferenceId
		AND (ISNULL(BPH.numCurrencyID,@numBaseCurrencyID) = @numCurrencyID OR @numCurrencyID =0)
		AND (ISNULL(BPH.numAccountClass,0)=@numAccountClass OR @numAccountClass=0)
	END		
	
--	SELECT tintReturnType AS tintRefType,'Purchase Return Credit' AS vcReference,numReturnHeaderID AS numReferenceID,monBizDocAmount AS monAmount, ISNULL(monBizDocAmount,0) - ISNULL(monBizDocUsedAmount,0) AS monNonAppliedAmount,CAST (0 AS BIT) AS bitIsPaid,0 AS monAmountPaid  
--		FROM ReturnHeader WHERE numDomainId=@numDomainId AND numDivisionID=@numDivisionID
--		AND numReturnHeaderID NOT IN (SELECT numReturnHeaderID FROM ReturnPaymentHistory WHERE numReferenceID=@numReferenceId AND tintReferenceType=2)
--		AND (ISNULL(monBizDocAmount,0) - ISNULL(monBizDocUsedAmount,0)) >0 AND tintReturnType=2 AND tintReceiveType=2
--		
--	UNION
--	
--	SELECT tintReturnType AS tintRefType,'Purchase Return Credit' AS vcReference,RH.numReturnHeaderID AS numReferenceID,RH.monBizDocAmount, ISNULL(RH.monBizDocAmount,0) - ISNULL(RH.monBizDocUsedAmount,0) + ISNULL(SUM(ISNULL(RPH.monAmount,0)),0) AS monNonAppliedAmount,CAST (1 AS BIT) AS bitIsPaid,ISNULL(SUM(ISNULL(RPH.monAmount,0)),0) AS monAmountPaid  
--		FROM ReturnHeader RH JOIN ReturnPaymentHistory RPH ON RH.numReturnHeaderID=RPH.numReturnHeaderID  
--		WHERE RH.numDomainId=@numDomainId AND RH.numDivisionID=@numDivisionID 
--		AND RPH.numReferenceID=@numReferenceId AND RPH.tintReferenceType=2 AND tintReturnType=2 AND tintReceiveType=2
--		GROUP BY RH.numReturnHeaderID,RH.monBizDocAmount,RH.monBizDocUsedAmount,RH.tintReturnType
--	
	
END
  
END
GO
--created by anoop jayaraj                                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getdomaindetails')
DROP PROCEDURE usp_getdomaindetails
GO
CREATE PROCEDURE [dbo].[USP_GetDomainDetails]                                          
@numDomainID as numeric(9)=0                                          
as           

DECLARE @listIds VARCHAR(MAX)
SELECT @listIds = COALESCE(@listIds+',' ,'') + CAST(numUserID AS VARCHAR(100)) FROM UnitPriceApprover WHERE numDomainID = @numDomainID
                               
select                                           
vcDomainName,                                           
isnull(vcDomainDesc,'') vcDomainDesc,                                           
isnull(bitExchangeIntegration,0) bitExchangeIntegration,                                           
isnull(bitAccessExchange,0) bitAccessExchange,                                           
isnull(vcExchUserName,'') vcExchUserName,                                           
isnull(vcExchPassword,'') vcExchPassword,                                           
isnull(vcExchPath,'') vcExchPath,                                           
isnull(vcExchDomain,'') vcExchDomain,                                         
tintCustomPagingRows,                                         
vcDateFormat,                                         
numDefCountry,                                         
tintComposeWindow,                                         
sintStartDate,                                         
sintNoofDaysInterval,                                         
tintAssignToCriteria,                                         
bitIntmedPage,                                         
tintFiscalStartMonth,                                    
--isnull(bitDeferredIncome,0) as bitDeferredIncome,                                  
isnull(tintPayPeriod,0) as tintPayPeriod,                                
isnull(numCurrencyID,0) as numCurrencyID,                            
isnull(vcCurrency,'') as vcCurrency,                      
isnull(charUnitSystem,'') as charUnitSystem ,                    
isnull(vcPortalLogo,'') as vcPortalLogo,                  
isnull(tintChrForComSearch,2) as tintChrForComSearch,                
isnull(tintChrForItemSearch,'') as tintChrForItemSearch,            
isnull(intPaymentGateWay,0) as intPaymentGateWay  ,        
 case when bitPSMTPServer= 1 then vcPSMTPServer else '' end as vcPSMTPServer  ,              
 case when bitPSMTPServer= 1 then  isnull(numPSMTPPort,0)  else 0 end as numPSMTPPort          
,isnull(bitPSMTPAuth,0) bitPSMTPAuth        
,isnull([vcPSmtpPassword],'')vcPSmtpPassword        
,isnull(bitPSMTPSSL,0) bitPSMTPSSL     
,isnull(bitPSMTPServer,0) bitPSMTPServer  ,isnull(vcPSMTPUserName,'') vcPSMTPUserName,      
--isnull(numDefaultCategory,0) numDefaultCategory,
isnull(numShipCompany,0)   numShipCompany,
isnull(bitAutoPopulateAddress,0) bitAutoPopulateAddress,
isnull(tintPoulateAddressTo,0) tintPoulateAddressTo,
isnull(bitMultiCompany,0) bitMultiCompany,
isnull(bitMultiCurrency,0) bitMultiCurrency,
ISNULL(bitCreateInvoice,0) bitCreateInvoice,
ISNULL([numDefaultSalesBizDocId],0) numDefaultSalesBizDocId,
ISNULL([bitSaveCreditCardInfo],0) bitSaveCreditCardInfo,
ISNULL([intLastViewedRecord],10) intLastViewedRecord,
ISNULL([tintCommissionType],1) tintCommissionType,
ISNULL([tintBaseTax],2) tintBaseTax,
ISNULL([numDefaultPurchaseBizDocId],0) numDefaultPurchaseBizDocId,
ISNULL(bitEmbeddedCost,0) bitEmbeddedCost,
ISNULL(numDefaultSalesOppBizDocId,0) numDefaultSalesOppBizDocId,
ISNULL(tintSessionTimeOut,1) tintSessionTimeOut,
ISNULL(tintDecimalPoints,2) tintDecimalPoints,
ISNULL(bitCustomizePortal,0) bitCustomizePortal,
ISNULL(tintBillToForPO,0) tintBillToForPO,
ISNULL(tintShipToForPO,0) tintShipToForPO,
ISNULL(tintOrganizationSearchCriteria,1) tintOrganizationSearchCriteria,
ISNULL(tintLogin,0) tintLogin,
lower(ISNULL(vcPortalName,'')) vcPortalName,
ISNULL(bitDocumentRepositary,0) bitDocumentRepositary,
D.numDomainID,ISNULL(D.tintComAppliesTo,0) tintComAppliesTo,
ISNULL(vcPSMTPDisplayName,'') vcPSMTPDisplayName,
ISNULL(bitGtoBContact,0) bitGtoBContact,
ISNULL(bitBtoGContact,0) bitBtoGContact,
ISNULL(bitGtoBCalendar,0) bitGtoBCalendar,
ISNULL(bitBtoGCalendar,0) bitBtoGCalendar,
ISNULL(bitExpenseNonInventoryItem,0) bitExpenseNonInventoryItem,
ISNULL(bitInlineEdit,0) bitInlineEdit,
ISNULL(bitRemoveVendorPOValidation,0) bitRemoveVendorPOValidation,
isnull(D.tintBaseTaxOnArea,0) tintBaseTaxOnArea,
ISNULL(D.bitAmountPastDue,0) bitAmountPastDue,
CONVERT(DECIMAL(18,2),ISNULL(D.monAmountPastDue,0)) monAmountPastDue,
ISNULL(bitSearchOrderCustomerHistory,0) bitSearchOrderCustomerHistory,
isnull(D.bitRentalItem,0) as bitRentalItem,
isnull(D.numRentalItemClass,0) as numRentalItemClass,
isnull(D.numRentalHourlyUOM,0) as numRentalHourlyUOM,
isnull(D.numRentalDailyUOM,0) as numRentalDailyUOM,
isnull(D.tintRentalPriceBasedOn,0) as tintRentalPriceBasedOn,
isnull(D.tintCalendarTimeFormat,24) as tintCalendarTimeFormat,
isnull(D.tintDefaultClassType,0) as tintDefaultClassType,
ISNULL(D.tintPriceBookDiscount,0) tintPriceBookDiscount,
ISNULL(D.bitAutoSerialNoAssign,0) as bitAutoSerialNoAssign,
ISNULL(D.bitIsShowBalance,0) AS [bitIsShowBalance],
ISNULL(numIncomeAccID,0) AS [numIncomeAccID],
ISNULL(numCOGSAccID,0) AS [numCOGSAccID],
ISNULL(numAssetAccID,0) AS [numAssetAccID],
ISNULL(IsEnableClassTracking,0) AS [IsEnableClassTracking],
ISNULL(IsEnableProjectTracking,0) AS [IsEnableProjectTracking],
ISNULL(IsEnableCampaignTracking,0) AS [IsEnableCampaignTracking],
ISNULL(IsEnableResourceScheduling,0) AS [IsEnableResourceScheduling],
ISNULL(numShippingServiceItemID,0) AS [numShippingServiceItemID],
ISNULL(D.numSOBizDocStatus,0) AS numSOBizDocStatus,
ISNULL(D.bitAutolinkUnappliedPayment,0) AS bitAutolinkUnappliedPayment,
ISNULL(D.numDiscountServiceItemID,0) AS numDiscountServiceItemID,
ISNULL(D.vcShipToPhoneNo,0) AS vcShipToPhoneNo,
ISNULL(D.vcGAUserEMail,'') AS vcGAUserEMail,
ISNULL(D.vcGAUserPassword,'') AS vcGAUserPassword,
ISNULL(D.vcGAUserProfileId,0) AS vcGAUserProfileId,
ISNULL(D.bitDiscountOnUnitPrice,0) AS bitDiscountOnUnitPrice,
ISNULL(D.intShippingImageWidth,0) AS intShippingImageWidth ,
ISNULL(D.intShippingImageHeight,0) AS intShippingImageHeight,
ISNULL(D.numTotalInsuredValue,0) AS numTotalInsuredValue,
ISNULL(D.numTotalCustomsValue,0) AS numTotalCustomsValue,
ISNULL(D.vcHideTabs,'') AS vcHideTabs,
ISNULL(D.bitUseBizdocAmount,0) AS bitUseBizdocAmount,
ISNULL(D.IsEnableUserLevelClassTracking,0) AS IsEnableUserLevelClassTracking,
ISNULL(D.bitDefaultRateType,0) AS bitDefaultRateType,
ISNULL(D.bitIncludeTaxAndShippingInCommission,0) AS bitIncludeTaxAndShippingInCommission,
/*,ISNULL(D.bitAllowPPVariance,0) AS bitAllowPPVariance*/
ISNULL(D.bitPurchaseTaxCredit,0) AS bitPurchaseTaxCredit,
ISNULL(D.bitLandedCost,0) AS bitLandedCost,
ISNULL(D.vcLanedCostDefault,'') AS vcLanedCostDefault,
ISNULL(D.bitMinUnitPriceRule,0) AS bitMinUnitPriceRule,
ISNULL(D.numAbovePercent,0) AS numAbovePercent,
ISNULL(D.numAbovePriceField,0) AS numAbovePriceField,
ISNULL(D.numBelowPercent,0) AS numBelowPercent,
ISNULL(D.numBelowPriceField,0) AS numBelowPriceField,
ISNULL(D.numOrderStatusBeforeApproval,0) AS numOrderStatusBeforeApproval,
ISNULL(D.numOrderStatusAfterApproval,0) AS numOrderStatusAfterApproval,
@listIds AS vcUnitPriceApprover,
ISNULL(D.numDefaultSalesPricing,2) AS numDefaultSalesPricing
from Domain D  
left join eCommerceDTL eComm  
on eComm.numDomainID=D.numDomainID  
where (D.numDomainID=@numDomainID OR @numDomainID=-1)



GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetDropDownValue')
DROP PROCEDURE USP_GetDropDownValue
GO
Create PROCEDURE [dbo].[USP_GetDropDownValue]     
    @numDomainID numeric(18, 0),
    @numListID numeric(18, 0),
    @vcListItemType AS VARCHAR(5),
    @vcDbColumnName VARCHAR(50)
as                 

CREATE TABLE #temp(numID VARCHAR(500),vcData VARCHAR(max))

			 IF @vcListItemType='LI' OR @vcListItemType='T'                                                    
				BEGIN  
					INSERT INTO #temp                                                   
					SELECT Ld.numListItemID,vcData from ListDetails LD          
					left join listorder LO on Ld.numListItemID= LO.numListItemID  and lo.numDomainId = @numDomainID
					where (Ld.numDomainID=@numDomainID or Ld.constFlag=1) and Ld.numListID=@numListID order by intSortOrder
				end  
 			 ELSE IF @vcListItemType='C'                                                     
				begin    
		   		    INSERT INTO #temp                                                   
	  			    SELECT  numCampaignID,vcCampaignName + CASE bitIsOnline WHEN 1 THEN ' (Online)' ELSE ' (Offline)' END AS vcCampaignName
					FROM CampaignMaster WHERE numDomainID = @numDomainID
				end  
			 ELSE IF @vcListItemType='AG'                                                     
				begin      
		   		    INSERT INTO #temp                                                   
					SELECT numGrpID, vcGrpName FROM groups       
					ORDER BY vcGrpName                                               
				end   
			else if @vcListItemType='DC'                                                     
				begin    
		   		    INSERT INTO #temp                                                   
					select numECampaignID,vcECampName from ECampaign where numDomainID= @numDomainID                                                       
				end 
			else if @vcListItemType='U' OR @vcDbColumnName='numManagerID'
			    BEGIN
		   		     INSERT INTO #temp                                                   
			    	 SELECT A.numContactID,ISNULL(A.vcFirstName,'') +' '+ ISNULL(A.vcLastName,'') as vcUserName          
					 from UserMaster UM join AdditionalContactsInformation A        
					 on UM.numUserDetailId=A.numContactID          
					 where UM.numDomainID=@numDomainID and UM.numDomainID=A.numDomainID  ORDER BY A.vcFirstName,A.vcLastName 
				END
			else if @vcListItemType='S'
			    BEGIN
		   		     INSERT INTO #temp                                                   
			    	 SELECT S.numStateID,S.vcState          
					 from State S      
					 where S.numDomainID=@numDomainID ORDER BY S.vcState
				END
			ELSE IF @vcListItemType = 'OC' 
				BEGIN
					INSERT INTO #temp   
					SELECT DISTINCT C.numCurrencyID,C.vcCurrencyDesc FROM dbo.Currency C WHERE C.numDomainID=@numDomainID
				END
			ELSE IF @vcListItemType = 'UOM' 
				BEGIN
					INSERT INTO #temp   
					SELECT  numUOMId AS numItemID,vcUnitName AS vcItemName FROM UOM WHERE numDomainID = @numDomainID
				END	
			ELSE IF @vcListItemType = 'IG' 
				BEGIN
					INSERT INTO #temp   
					SELECT numItemGroupID,vcItemGroup FROM dbo.ItemGroups WHERE numDomainID=@numDomainID   	
				END
			ELSE IF @vcListItemType = 'SYS'
				BEGIN
					INSERT INTO #temp
					SELECT 0 AS numItemID,'Lead' AS vcItemName
					UNION ALL
					SELECT 1 AS numItemID,'Prospect' AS vcItemName
					UNION ALL
					SELECT 2 AS numItemID,'Account' AS vcItemName
				END
			ELSE IF @vcListItemType = 'PP' 
				BEGIN
					INSERT INTO #temp 
					SELECT 'P','Inventory Item'
					UNION 
					SELECT 'N','Non-Inventory Item'
					UNION 
					SELECT 'S','Service'
				END

				--<asp:ListItem Value="0" Selected="True">0</asp:ListItem>
    --                                <asp:ListItem Value="25" Selected="True">25%</asp:ListItem>
    --                                <asp:ListItem Value="50">50%</asp:ListItem>
    --                                <asp:ListItem Value="75">75%</asp:ListItem>
    --                                <asp:ListItem Value="100">100%</asp:ListItem>
				ELSE IF @vcListItemType = 'SP' --Progress Percentage
				BEGIN
					INSERT INTO #temp 
					SELECT '0','0%'
					UNION 
					SELECT '25','25%'
					UNION 
					SELECT '50','50%'
					UNION 
					SELECT '75','75%'
					UNION 
					SELECT '100','100%'
				END
				ELSE IF @vcListItemType = 'ST' --Stage Details
				BEGIN
					INSERT INTO #temp 
					SELECT vcStageName ,vcStageName  from StagePercentageDetails where numDomainId=@numDomainID and slp_id=@numListID  AND ISNULL(numProjectID,0)= 0 AND ISNULL(numOppID,0)= 0
				END
				ELSE IF @vcListItemType = 'SG' --Stage Details
				BEGIN
					INSERT INTO #temp 
					SELECT vcStageName ,vcStageName  from StagePercentageDetails where numDomainId=@numDomainID   AND ISNULL(numProjectID,0)= 0 AND ISNULL(numOppID,0)= 0
				END
				ELSE IF @vcListItemType = 'SM' --Reminer,snooze time
				BEGIN
					INSERT INTO #temp 
					SELECT '5','5 minutes'
					UNION 
					SELECT '15','15 minutes'
					UNION 
					SELECT '30','30 Minutes'
					UNION 
					SELECT '60','1 Hour'
					UNION 
					SELECT '240','4 Hour'
					UNION 
					SELECT '480','8 Hour'
					UNION 
					SELECT '1440','24 Hour'

				END
			 ELSE IF @vcListItemType = 'PT' --Pin To
				BEGIN
					INSERT INTO #temp 
					SELECT '1','Sales Opportunity'
					UNION 
					SELECT '2','Purchase Opportunity'
					UNION 
					SELECT '3','Sales Order'
					UNION 
					SELECT '4','Purchase Order'
					UNION 
					SELECT '5','Project'
					UNION 
					SELECT '6','Cases'
					UNION 
					SELECT '7','Item'

				END
				ELSE IF @vcListItemType = 'DV' --Customer
				BEGIN
					--INSERT INTO #temp 
					--SELECT vcStageName ,vcStageName  from StagePercentageDetails where numDomainId=@numDomainID and slp_id=@numListID  AND ISNULL(numProjectID,0)= 0 AND ISNULL(numOppID,0)= 0
				INSERT INTO #temp 
					SELECT d.numDivisionID,a.vcCompanyname + Case when isnull(d.numCompanyDiff,0)>0 then  ' ' + dbo.fn_getlistitemname(d.numCompanyDiff) + ':' + isnull(d.vcCompanyDiff,'') else '' end as vcCompanyname from   companyinfo AS a                      
					join divisionmaster d                      
					on  a.numCompanyid=d.numCompanyid
					WHERE D.numDomainID=@numDomainID
			
				END
				ELSE IF @vcListItemType = 'BD' --Net Days
				BEGIN
					--INSERT INTO #temp 
					--SELECT vcStageName ,vcStageName  from StagePercentageDetails where numDomainId=@numDomainID and slp_id=@numListID  AND ISNULL(numProjectID,0)= 0 AND ISNULL(numOppID,0)= 0
				INSERT INTO #temp 
						SELECT numTermsID,vcTerms	
						FROM dbo.BillingTerms 
						WHERE 
						numDomainID = @numDomainID
						AND ISNULL(bitActive,0) = 1
			
				END
				ELSE IF @vcListItemType = 'BT' --Net Days
				BEGIN
					--INSERT INTO #temp 
					--SELECT vcStageName ,vcStageName  from StagePercentageDetails where numDomainId=@numDomainID and slp_id=@numListID  AND ISNULL(numProjectID,0)= 0 AND ISNULL(numOppID,0)= 0
				INSERT INTO #temp 
							SELECT numBizDocTempID,    
							vcTemplateName
							FROM BizDocTemplate  
							WHERE [numDomainID] = @numDomainID and tintTemplateType=0
			
				END
				
		    ELSE IF @vcDbColumnName='charSex'
				BEGIN
					 INSERT INTO #temp
					 SELECT 'M','Male' UNION
					 SELECT 'F','Female'
				 END
			ELSE IF @vcDbColumnName='tintOppStatus'
			  BEGIN
					 INSERT INTO #temp
					 SELECT 1,'Deal Won' UNION
					 SELECT 2,'Deal Lost'
			  END
			ELSE IF @vcDbColumnName='tintOppType'
			  BEGIN
					 INSERT INTO #temp
					 SELECT 1,'Sales Order' UNION
					 SELECT 2,'Purchase Order' UNION
					 SELECT 3,'Sales Opportunity' UNION
					 SELECT 4,'Purchase Opportunity' 
			  END
			 
--			Else                                                     
--				BEGIN                                                     
--					select @listName=vcData from ListDetails WHERE numListItemID=@numlistID
--				end 	

 INSERT INTO #temp  SELECT 0,'--None--'				
        
		SELECT * FROM #temp 
		DROP TABLE #temp	
GO

/****** Object:  StoredProcedure [dbo].[USP_GetEmailToCaseCheck]    Script Date: 07/26/2008 16:19:07 ******/

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetEmailToCaseCheck')
DROP PROCEDURE USP_GetEmailToCaseCheck
GO

-- EXEC USP_GetEmailToCaseCheck 1,'kamal@bizautomation.com',0
Create PROCEDURE [dbo].[USP_GetEmailToCaseCheck]            
@numDomainID as numeric(9),            
@vcFromEmail VARCHAR(50),
@vcCaseNumber VARCHAR(50)
as             

   Declare @numcontactid numeric(9);SET @numcontactid=0;
   DECLARE @numDivisionId NUMERIC(9);SET @numDivisionId=0;
   DECLARE @numCaseId NUMERIC(9);SET @numCaseId=0;
   
   SELECT TOP 1 @numcontactid=ADC.numcontactid,@numDivisionId=ADC.numDivisionId from AdditionalContactsInformation ADC 
   JOIN DivisionMaster DM ON ADC.numDivisionId=DM.numDivisionId 
	WHERE --ISNULL(DM.bitEmailToCase,0)=1 AND
	 DM.numDomainID=@numDomainID AND ADC.vcEmail = @vcFromEmail
		AND ADC.numDomainId = @numDomainId
		
	IF LEN(@vcCaseNumber)>0 AND @vcCaseNumber!='0' 
		AND ISNULL(@numcontactid,0) <> 0 AND ISNULL(@numDivisionId,0)<> 0
	BEGIN
		SELECT TOP 1 @numCaseId=numCaseId FROM dbo.Cases WHERE numDomainID=@numDomainID
		AND numDivisionID=@numDivisionId AND numContactId=@numcontactid
		AND vcCaseNumber LIKE '%' + @vcCaseNumber
	END
	
	SELECT ISNULL(@numcontactid,0) AS numcontactid,ISNULL(@numDivisionId,0) AS numDivisionId,ISNULL(@numCaseId,0) AS numCaseId
          
GO
    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetGeneralLedger_Virtual')
DROP PROCEDURE USP_GetGeneralLedger_Virtual
GO
CREATE PROCEDURE [dbo].[USP_GetGeneralLedger_Virtual]    
(
      @numDomainID INT,
      @vcAccountId VARCHAR(4000),
      @dtFromDate DATETIME,
      @dtToDate DATETIME,
      @vcTranType VARCHAR(50) = '',
      @varDescription VARCHAR(50) = '',
      @CompanyName VARCHAR(50) = '',
      @vcBizPayment VARCHAR(50) = '',
      @vcCheqNo VARCHAR(50) = '',
      @vcBizDocID VARCHAR(50) = '',
      @vcTranRef VARCHAR(50) = '',
      @vcTranDesc VARCHAR(50) = '',
	  @numDivisionID NUMERIC(9)=0,
      @ClientTimeZoneOffset INT,  --Added by Chintan to enable calculation of date according to client machine
      @charReconFilter CHAR(1)='',
	  @tintMode AS TINYINT=0,
      @numItemID AS NUMERIC(9)=0,
      @CurrentPage INT=0,
	  @PageSize INT=0
)
WITH Recompile -- Added by Chandan 26 Feb 2013 to avoid parameter sniffing
AS 
    BEGIN
    
--SET @dtFromDate = DateAdd(minute, @ClientTimeZoneOffset, @dtFromDate);
--SET @dtToDate = DateAdd(minute, @ClientTimeZoneOffset, @dtToDate); 
DECLARE @dtFinFromDate DATETIME;
SET @dtFinFromDate = ( SELECT   dtPeriodFrom
                       FROM     FINANCIALYEAR
                       WHERE    dtPeriodFrom <= @dtFromDate
                                AND dtPeriodTo >= @dtFromDate
                                AND numDomainId = @numDomainId
                     );

PRINT @dtFromDate
PRINT @dtToDate

DECLARE @dtFinYearFromJournal DATETIME ;
DECLARE @dtFinYearFrom DATETIME ;
SELECT @dtFinYearFromJournal = MIN(datEntry_Date) FROM view_journal WHERE numDomainID=@numDomainId 
SET @dtFinYearFrom = ( SELECT   dtPeriodFrom
                        FROM     FINANCIALYEAR
                        WHERE    dtPeriodFrom <= @dtFromDate
                                AND dtPeriodTo >= @dtFromDate
                                AND numDomainId = @numDomainId
                        ) ;

SELECT * INTO #view_journal FROM view_journal WHERE numDomainID=@numDomainId 

DECLARE @numMaxFinYearID NUMERIC
DECLARE @Month INT
DECLARE @year  INT
SELECT @numMaxFinYearID = MAX(numFinYearId) FROM dbo.FinancialYear WHERE numDomainId=@numDomainId
SELECT @Month = DATEPART(MONTH,dtPeriodFrom),@year =DATEPART(year,dtPeriodFrom) FROM dbo.FinancialYear WHERE numDomainId=@numDomainId AND numFinYearId=@numMaxFinYearID
PRINT @Month
PRINT @year
		 
DECLARE @first_id NUMERIC, @startRow int
DECLARE @CurrRecord as INT

if @tintMode = 0
BEGIN
SELECT  COA1.numAccountId INTO #Temp1 /*,COA1.vcAccountCode ,COA2.vcAccountCode*/
		FROM    dbo.Chart_Of_Accounts COA1
		WHERE   COA1.numAccountId IN(SELECT  cast(ID AS NUMERIC(9)) FROM dbo.SplitIDs(@vcAccountId, ','))
				AND COA1.numDomainId = @numDomainID
	SELECT @vcAccountId = ISNULL( STUFF((SELECT ',' + CAST(numAccountID AS VARCHAR(10)) FROM #Temp1 FOR XML PATH('')),1, 1, '') , '') 
    
    	--SELECT  @numDomainID AS numDomainID,* FROM dbo.fn_GetOpeningBalance(@vcAccountID,@numDomainID,@dtFromDate,@ClientTimeZoneOffset)
		SELECT  @numDomainID AS numDomainID, numAccountId,vcAccountName, numParntAcntTypeID,	vcAccountDescription,	vcAccountCode,	dtAsOnDate,	
		CASE WHEN ([Table1].[vcAccountCode] LIKE '0103%' OR [Table1].[vcAccountCode] LIKE '0104%' OR [Table1].[vcAccountCode] LIKE '0106%') AND (@year + @Month) = (DATEPART(YEAR,@dtFromDate) + DATEPART(MONTH,@dtFromDate)) THEN 0 
			 WHEN (@year + @Month) > (DATEPART(YEAR,@dtFromDate) + DATEPART(MONTH,@dtFromDate)) THEN isnull(t1.[OPENING],0)
			 WHEN (@year + @Month) < (DATEPART(YEAR,@dtFromDate) + DATEPART(MONTH,@dtFromDate)) THEN isnull(t2.[OPENING],0)
			 ELSE isnull(mnOpeningBalance,0)
		END  [mnOpeningBalance],
							(SELECT ISNULL(SUM(GJD.numDebitAmt),0) - ISNULL(SUM(GJD.numCreditAmt),0)
							FROM
							General_Journal_Details GJD 
							JOIN Domain D ON D.[numDomainId] = GJD.numDomainID AND D.[numDomainId] = @numDomainID
							INNER JOIN General_Journal_Header GJH ON GJD.numJournalId = GJH.numJournal_Id AND 
							GJD.numDomainId = @numDomainID AND 
							GJD.numChartAcntId = [Table1].numAccountId AND 
							GJH.numDomainID = D.[numDomainId] AND 
							GJH.datEntry_Date <= @dtFinFromDate) 
FROM dbo.fn_GetOpeningBalance(@vcAccountID,@numDomainID,@dtFromDate,@ClientTimeZoneOffset) [Table1]
OUTER APPLY (SELECT   SUM(Debit - Credit) AS OPENING
            FROM     #view_journal VJ
            WHERE    VJ.numDomainId = @numDomainId
                    AND VJ.numAccountID = [Table1].numAccountID /*AND VJ.COAvcAccountCode like COA.vcAccountCode + '%'*/ /*VJ.numAccountId = COA.numAccountId*/
                    AND datEntry_Date BETWEEN (CASE WHEN [Table1].[vcAccountCode] LIKE '0103%' OR [Table1].[vcAccountCode] LIKE '0104%' OR [Table1].[vcAccountCode] LIKE '0106%' 
													THEN @dtFinYearFrom 
													ELSE @dtFinYearFromJournal 
											   END ) AND  DATEADD(Minute,-1,@dtFromDate)) AS t1
OUTER APPLY (SELECT   SUM(Debit - Credit) AS OPENING
            FROM     #view_journal VJ
            WHERE    VJ.numDomainId = @numDomainId
                    AND VJ.numAccountID = [Table1].numAccountID /*AND VJ.COAvcAccountCode like COA.vcAccountCode + '%'*/ /*VJ.numAccountId = COA.numAccountId*/
                    AND datEntry_Date BETWEEN (CASE WHEN [Table1].[vcAccountCode] LIKE '0103%' OR [Table1].[vcAccountCode] LIKE '0104%' OR [Table1].[vcAccountCode] LIKE '0106%' 
													THEN @dtFinYearFrom 
													ELSE @dtFinYearFromJournal 
											   END ) AND  DATEADD(Minute,1,@dtFromDate)) AS t2

		--SET @dtFromDate = DateAdd(minute, @ClientTimeZoneOffset, @dtFromDate);
		--SET @dtToDate = DateAdd(minute, @ClientTimeZoneOffset, @dtToDate);

SELECT TOP 1
dbo.General_Journal_Details.numDomainId,
 dbo.Chart_Of_Accounts.numAccountId , 
dbo.General_Journal_Header.numJournal_Id,
dbo.[FormatedDateFromDate](dbo.General_Journal_Header.datEntry_Date,dbo.General_Journal_Details.numDomainID) Date,
dbo.General_Journal_Header.varDescription, 
dbo.CompanyInfo.vcCompanyName AS CompanyName,
dbo.General_Journal_Details.numTransactionId,

ISNULL(ISNULL(ISNULL(dbo.VIEW_BIZPAYMENT.Narration,dbo.General_Journal_Header.varDescription),'') + ' ' + 
(CASE WHEN isnull(dbo.General_Journal_Header.numReturnID, 0) <> 0 THEN ' Chek No: ' + isnull( CAST(CH.numCheckNo AS VARCHAR(50)),'No-Cheq') 
ELSE  dbo.VIEW_JOURNALCHEQ.Narration 
END),'') as Narration,

ISNULL(dbo.General_Journal_Details.vcReference,'') AS TranRef, 
ISNULL(dbo.General_Journal_Details.varDescription,'') AS TranDesc, 
CONVERT(VARCHAR(20),ISNULL(General_Journal_Details.numDebitAmt,0)) numDebitAmt,
CONVERT(VARCHAR(20),ISNULL(General_Journal_Details.numCreditAmt,0)) numCreditAmt,
dbo.Chart_Of_Accounts.vcAccountName,
'' as balance,
CASE 
WHEN isnull(dbo.General_Journal_Header.numCheckHeaderID, 0) <> 0 THEN + 'Checks' 
WHEN isnull(dbo.General_Journal_Header.numDepositId, 0) <> 0 AND DM.tintDepositePage = 1 THEN 'Deposit' 
WHEN isnull(dbo.General_Journal_Header.numDepositId, 0) <> 0 AND DM.tintDepositePage = 2 THEN 'Receved Pmt' 
WHEN isnull(dbo.General_Journal_Header.numOppId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numOppBizDocsId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numBizDocsPaymentDetId, 0) = 0 AND 
dbo.OpportunityMaster.tintOppType = 1 THEN 'BizDocs Invoice' 
WHEN isnull(dbo.General_Journal_Header.numOppId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numOppBizDocsId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numBizDocsPaymentDetId, 0) = 0 AND 
dbo.OpportunityMaster.tintOppType = 2 THEN 'BizDocs Purchase' 
WHEN isnull(dbo.General_Journal_Header.numCategoryHDRID, 0) <> 0 THEN 'Time And Expenses' 
WHEN ISNULL(General_Journal_Header.numBillId,0) <>0 AND isnull(BH.bitLandedCost,0)=1 then 'Landed Cost' 
WHEN isnull(dbo.General_Journal_Header.numBillID, 0) <> 0 THEN 'Bill'
WHEN isnull(dbo.General_Journal_Header.numBillPaymentID, 0) <> 0 THEN 'Pay Bill'
WHEN isnull(dbo.General_Journal_Header.numReturnID, 0) <> 0 THEN 
																CASE 
																	WHEN RH.tintReturnType=1 AND RH.tintReceiveType=1 THEN 'Sales Return Refund'
																	WHEN RH.tintReturnType=1 AND RH.tintReceiveType=2 THEN 'Sales Return Credit Memo'
																	WHEN RH.tintReturnType=2 AND RH.tintReceiveType=2 THEN 'Purchase Return Credit Memo'
																	WHEN RH.tintReturnType=3 AND RH.tintReceiveType=2 THEN 'Credit Memo'
																	WHEN RH.tintReturnType=4 AND RH.tintReceiveType=1 THEN 'Refund Against Credit Memo'
																END 
WHEN isnull(dbo.General_Journal_Header.numOppId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numOppBizDocsId, 0) = 0 THEN 'PO Fulfillment'
WHEN dbo.General_Journal_Header.numJournal_Id <> 0 THEN 'Journal' 
END AS TransactionType,

ISNULL(dbo.General_Journal_Details.bitCleared,0) AS bitCleared,
ISNULL(dbo.General_Journal_Details.bitReconcile,0) AS bitReconcile,
ISNULL(dbo.General_Journal_Details.numItemID,0) AS numItemID,
Case When isnull(BH.bitLandedCost,0)=1 then  isnull(BH.numOppId,0) else 0 end AS numLandedCostOppId
FROM dbo.General_Journal_Header INNER JOIN
dbo.General_Journal_Details ON dbo.General_Journal_Header.numJournal_Id = dbo.General_Journal_Details.numJournalId INNER JOIN
dbo.Chart_Of_Accounts ON dbo.General_Journal_Details.numChartAcntId = dbo.Chart_Of_Accounts.numAccountId LEFT OUTER JOIN
dbo.TimeAndExpense ON dbo.General_Journal_Header.numCategoryHDRID = dbo.TimeAndExpense.numCategoryHDRID 
LEFT OUTER JOIN dbo.DivisionMaster ON dbo.General_Journal_Details.numCustomerId = dbo.DivisionMaster.numDivisionID and 
dbo.General_Journal_Details.numDomainID = dbo.DivisionMaster.numDomainID
LEFT OUTER JOIN dbo.CompanyInfo ON dbo.DivisionMaster.numCompanyID = dbo.CompanyInfo.numCompanyId 
LEFT OUTER JOIN dbo.OpportunityMaster ON dbo.General_Journal_Header.numOppId = dbo.OpportunityMaster.numOppId LEFT OUTER JOIN
dbo.OpportunityBizDocs ON dbo.General_Journal_Header.numOppBizDocsId = dbo.OpportunityBizDocs.numOppBizDocsId LEFT OUTER JOIN
dbo.VIEW_JOURNALCHEQ ON (dbo.General_Journal_Header.numCheckHeaderID = dbo.VIEW_JOURNALCHEQ.numCheckHeaderID 
or (dbo.General_Journal_Header.numBillPaymentID=dbo.VIEW_JOURNALCHEQ.numReferenceID and dbo.VIEW_JOURNALCHEQ.tintReferenceType=8))LEFT OUTER JOIN
dbo.VIEW_BIZPAYMENT ON dbo.General_Journal_Header.numBizDocsPaymentDetId = dbo.VIEW_BIZPAYMENT.numBizDocsPaymentDetId LEFT OUTER JOIN
dbo.DepositMaster DM ON DM.numDepositId = dbo.General_Journal_Header.numDepositId  LEFT OUTER JOIN
dbo.ReturnHeader RH ON RH.numReturnHeaderID = dbo.General_Journal_Header.numReturnID LEFT OUTER JOIN
dbo.CheckHeader CH ON CH.numReferenceID=RH.numReturnHeaderID AND CH.tintReferenceType=10
LEFT OUTER JOIN BillHeader BH  ON ISNULL(General_Journal_Header.numBillId,0) = BH.numBillId

WHERE   dbo.General_Journal_Header.datEntry_Date BETWEEN  @dtFromDate AND @dtToDate -- Removed Tmezone bug id 1029 
AND dbo.General_Journal_Header.numDomainId = @numDomainID 
 AND (dbo.DivisionMaster.numDivisionID = @numDivisionID OR @numDivisionID = 0)
AND (bitCleared = CASE @charReconFilter WHEN 'C' THEN 1 ELSE 0 END  OR RTRIM(@charReconFilter)='A')
AND (bitReconcile = CASE @charReconFilter WHEN 'R' THEN 1 ELSE 0 END  OR RTRIM(@charReconFilter)='A') 
AND (dbo.Chart_Of_Accounts.numAccountId IN (SELECT numAccountId FROM #Temp1))
--ORDER BY dbo.General_Journal_Header.numEntryDateSortOrder asc
--ORDER BY dbo.Chart_Of_Accounts.numAccountId ASC,  dbo.General_Journal_Header.datEntry_Date ASC, dbo.General_Journal_Details.numTransactionId asc 
END
ELSE IF @tintMode = 1
BEGIN

SELECT  @numDomainID AS numDomainID, numAccountId,vcAccountName, numParntAcntTypeID,	vcAccountDescription,	vcAccountCode,	dtAsOnDate,	
		CASE WHEN ([Table1].[vcAccountCode] LIKE '0103%' OR [Table1].[vcAccountCode] LIKE '0104%' OR [Table1].[vcAccountCode] LIKE '0106%') AND (@year + @Month) = (DATEPART(YEAR,@dtFromDate) + DATEPART(MONTH,@dtFromDate)) THEN 0 
			 WHEN (@year + @Month) > (DATEPART(YEAR,@dtFromDate) + DATEPART(MONTH,@dtFromDate)) THEN isnull(t1.[OPENING],0)
			 WHEN (@year + @Month) < (DATEPART(YEAR,@dtFromDate) + DATEPART(MONTH,@dtFromDate)) THEN isnull(t2.[OPENING],0)
			 ELSE isnull(mnOpeningBalance,0)
		END  [mnOpeningBalance],
							(SELECT ISNULL(SUM(GJD.numDebitAmt),0) - ISNULL(SUM(GJD.numCreditAmt),0)
							FROM
							General_Journal_Details GJD 
							JOIN Domain D ON D.[numDomainId] = GJD.numDomainID AND D.[numDomainId] = @numDomainID
							INNER JOIN General_Journal_Header GJH ON GJD.numJournalId = GJH.numJournal_Id AND 
							GJD.numDomainId = @numDomainID AND 
							GJD.numChartAcntId = [Table1].numAccountId AND 
							GJH.numDomainID = D.[numDomainId] AND 
							GJH.datEntry_Date <= @dtFinFromDate) 
FROM dbo.fn_GetOpeningBalance(@vcAccountID,@numDomainID,@dtFromDate,@ClientTimeZoneOffset) [Table1]
OUTER APPLY (SELECT   SUM(Debit - Credit) AS OPENING
            FROM     #view_journal VJ
            WHERE    VJ.numDomainId = @numDomainId
                    AND VJ.numAccountID = [Table1].numAccountID /*AND VJ.COAvcAccountCode like COA.vcAccountCode + '%'*/ /*VJ.numAccountId = COA.numAccountId*/
                    AND datEntry_Date BETWEEN (CASE WHEN [Table1].[vcAccountCode] LIKE '0103%' OR [Table1].[vcAccountCode] LIKE '0104%' OR [Table1].[vcAccountCode] LIKE '0106%' 
													THEN @dtFinYearFrom 
													ELSE @dtFinYearFromJournal 
											   END ) AND  DATEADD(Minute,-1,@dtFromDate)) AS t1
OUTER APPLY (SELECT   SUM(Debit - Credit) AS OPENING
            FROM     #view_journal VJ
            WHERE    VJ.numDomainId = @numDomainId
                    AND VJ.numAccountID = [Table1].numAccountID /*AND VJ.COAvcAccountCode like COA.vcAccountCode + '%'*/ /*VJ.numAccountId = COA.numAccountId*/
                    AND datEntry_Date BETWEEN (CASE WHEN [Table1].[vcAccountCode] LIKE '0103%' OR [Table1].[vcAccountCode] LIKE '0104%' OR [Table1].[vcAccountCode] LIKE '0106%' 
													THEN @dtFinYearFrom 
													ELSE @dtFinYearFromJournal 
											   END ) AND  DATEADD(Minute,1,@dtFromDate)) AS t2

--SET @dtFromDate = DateAdd(minute, @ClientTimeZoneOffset, @dtFromDate);
--SET @dtToDate = DateAdd(minute, @ClientTimeZoneOffset, @dtToDate);

SET @CurrRecord = ((@CurrentPage - 1) * @PageSize) + 1
PRINT @CurrRecord
SET ROWCOUNT @CurrRecord

SELECT @first_id = dbo.General_Journal_Details.numEntryDateSortOrder1
FROM dbo.General_Journal_Header 
INNER JOIN dbo.General_Journal_Details ON dbo.General_Journal_Header.numJournal_Id = dbo.General_Journal_Details.numJournalId   
INNER JOIN dbo.Chart_Of_Accounts ON dbo.General_Journal_Details.numChartAcntId = dbo.Chart_Of_Accounts.numAccountId 
LEFT OUTER JOIN dbo.TimeAndExpense ON dbo.General_Journal_Header.numCategoryHDRID = dbo.TimeAndExpense.numCategoryHDRID -- LEFT OUTER JOIN
WHERE dbo.General_Journal_Header.datEntry_Date BETWEEN  @dtFromDate AND @dtToDate -- Removed Tmezone bug id 1029 
AND dbo.General_Journal_Header.numDomainId = @numDomainID 
AND dbo.General_Journal_Details.numDomainId = @numDomainID 
AND dbo.Chart_Of_Accounts.numDomainId = @numDomainID 
AND (bitCleared = CASE @charReconFilter WHEN 'C' THEN 1 ELSE 0 END  OR RTRIM(@charReconFilter)='A')
AND (bitReconcile = CASE @charReconFilter WHEN 'R' THEN 1 ELSE 0 END  OR RTRIM(@charReconFilter)='A') 
--AND (dbo.Chart_Of_Accounts.numAccountId =@vcAccountId)
AND (dbo.Chart_Of_Accounts.numAccountId= @vcAccountId)
AND (General_Journal_Details.numItemID = @numItemID OR @numItemID = 0)
AND dbo.General_Journal_Details.numEntryDateSortOrder1 > 0
--ORDER BY dbo.Chart_Of_Accounts.numAccountId ASC, dbo.General_Journal_Details.numTransactionId asc 
ORDER BY dbo.General_Journal_Details.numEntryDateSortOrder1 asc

PRINT @first_id
PRINT ROWCOUNT_BIG()
SET ROWCOUNT @PageSize

SELECT dbo.General_Journal_Details.numDomainId, dbo.Chart_Of_Accounts.numAccountId , dbo.General_Journal_Header.numJournal_Id,
dbo.[FormatedDateFromDate](dbo.General_Journal_Header.datEntry_Date,dbo.General_Journal_Details.numDomainID) Date,
dbo.General_Journal_Header.varDescription, 
dbo.CompanyInfo.vcCompanyName AS CompanyName,
dbo.General_Journal_Details.numTransactionId,
ISNULL(ISNULL(ISNULL(dbo.VIEW_BIZPAYMENT.Narration,dbo.General_Journal_Header.varDescription),'') + ' ' + 
(CASE WHEN isnull(dbo.General_Journal_Header.numReturnID, 0) <> 0 THEN ' Chek No: ' + isnull( CAST(CH.numCheckNo AS VARCHAR(50)),'No-Cheq') 
ELSE  dbo.VIEW_JOURNALCHEQ.Narration 
END),'') as Narration,
ISNULL(dbo.General_Journal_Details.vcReference,'') AS TranRef, 
ISNULL(dbo.General_Journal_Details.varDescription,'') AS TranDesc, 
CONVERT(VARCHAR(20),ISNULL(General_Journal_Details.numDebitAmt,0)) numDebitAmt,
CONVERT(VARCHAR(20),ISNULL(General_Journal_Details.numCreditAmt,0)) numCreditAmt,
dbo.Chart_Of_Accounts.vcAccountName,
'' as balance,
CASE 
WHEN isnull(dbo.General_Journal_Header.numCheckHeaderID, 0) <> 0 THEN + 'Checks' 
WHEN isnull(dbo.General_Journal_Header.numDepositId, 0) <> 0 AND DM.tintDepositePage = 1 THEN 'Deposit' 
WHEN isnull(dbo.General_Journal_Header.numDepositId, 0) <> 0 AND DM.tintDepositePage = 2 THEN 'Receved Pmt' 
WHEN isnull(dbo.General_Journal_Header.numOppId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numOppBizDocsId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numBizDocsPaymentDetId, 0) = 0 AND 
dbo.OpportunityMaster.tintOppType = 1 THEN 'BizDocs Invoice' 
WHEN isnull(dbo.General_Journal_Header.numOppId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numOppBizDocsId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numBizDocsPaymentDetId, 0) = 0 AND 
dbo.OpportunityMaster.tintOppType = 2 THEN 'BizDocs Purchase' 
WHEN isnull(dbo.General_Journal_Header.numCategoryHDRID, 0) <> 0 THEN 'Time And Expenses' 
WHEN ISNULL(General_Journal_Header.numBillId,0) <>0 AND isnull(BH.bitLandedCost,0)=1 then 'Landed Cost' 
WHEN isnull(dbo.General_Journal_Header.numBillID, 0) <> 0 THEN 'Bill'
WHEN isnull(dbo.General_Journal_Header.numBillPaymentID, 0) <> 0 THEN 'Pay Bill'
WHEN isnull(dbo.General_Journal_Header.numReturnID, 0) <> 0 THEN 
																CASE 
																	WHEN RH.tintReturnType=1 AND RH.tintReceiveType=1 THEN 'Sales Return Refund'
																	WHEN RH.tintReturnType=1 AND RH.tintReceiveType=2 THEN 'Sales Return Credit Memo'
																	WHEN RH.tintReturnType=2 AND RH.tintReceiveType=2 THEN 'Purchase Return Credit Memo'
																	WHEN RH.tintReturnType=3 AND RH.tintReceiveType=2 THEN 'Credit Memo'
																	WHEN RH.tintReturnType=4 AND RH.tintReceiveType=1 THEN 'Refund Against Credit Memo'
																END 
WHEN isnull(dbo.General_Journal_Header.numOppId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numOppBizDocsId, 0) = 0 THEN 'PO Fulfillment'
WHEN dbo.General_Journal_Header.numJournal_Id <> 0 THEN 'Journal' 
END AS TransactionType,
ISNULL(dbo.General_Journal_Details.bitCleared,0) AS bitCleared,
ISNULL(dbo.General_Journal_Details.bitReconcile,0) AS bitReconcile,
ISNULL(dbo.General_Journal_Details.numItemID,0) AS numItemID,
dbo.General_Journal_Details.numEntryDateSortOrder1 AS numEntryDateSortOrder,
Case When isnull(BH.bitLandedCost,0)=1 then  isnull(BH.numOppId,0) else 0 end AS numLandedCostOppId
FROM dbo.General_Journal_Header INNER JOIN
dbo.General_Journal_Details ON dbo.General_Journal_Header.numJournal_Id = dbo.General_Journal_Details.numJournalId INNER JOIN
dbo.Chart_Of_Accounts ON dbo.General_Journal_Details.numChartAcntId = dbo.Chart_Of_Accounts.numAccountId LEFT OUTER JOIN
dbo.TimeAndExpense ON dbo.General_Journal_Header.numCategoryHDRID = dbo.TimeAndExpense.numCategoryHDRID 
LEFT OUTER JOIN dbo.DivisionMaster ON dbo.General_Journal_Details.numCustomerId = dbo.DivisionMaster.numDivisionID and 
dbo.General_Journal_Details.numDomainID = dbo.DivisionMaster.numDomainID
LEFT OUTER JOIN dbo.CompanyInfo ON dbo.DivisionMaster.numCompanyID = dbo.CompanyInfo.numCompanyId 
LEFT OUTER JOIN dbo.OpportunityMaster ON dbo.General_Journal_Header.numOppId = dbo.OpportunityMaster.numOppId LEFT OUTER JOIN
dbo.OpportunityBizDocs ON dbo.General_Journal_Header.numOppBizDocsId = dbo.OpportunityBizDocs.numOppBizDocsId LEFT OUTER JOIN
dbo.VIEW_JOURNALCHEQ ON (dbo.General_Journal_Header.numCheckHeaderID = dbo.VIEW_JOURNALCHEQ.numCheckHeaderID 
or (dbo.General_Journal_Header.numBillPaymentID=dbo.VIEW_JOURNALCHEQ.numReferenceID and dbo.VIEW_JOURNALCHEQ.tintReferenceType=8))LEFT OUTER JOIN
dbo.VIEW_BIZPAYMENT ON dbo.General_Journal_Header.numBizDocsPaymentDetId = dbo.VIEW_BIZPAYMENT.numBizDocsPaymentDetId LEFT OUTER JOIN
dbo.DepositMaster DM ON DM.numDepositId = dbo.General_Journal_Header.numDepositId  LEFT OUTER JOIN
dbo.ReturnHeader RH ON RH.numReturnHeaderID = dbo.General_Journal_Header.numReturnID LEFT OUTER JOIN
dbo.CheckHeader CH ON CH.numReferenceID=RH.numReturnHeaderID AND CH.tintReferenceType=10
LEFT OUTER JOIN BillHeader BH  ON ISNULL(General_Journal_Header.numBillId,0) = BH.numBillId

WHERE   dbo.General_Journal_Header.datEntry_Date BETWEEN  @dtFromDate AND @dtToDate -- Removed Tmezone bug id 1029 
AND dbo.General_Journal_Header.numDomainId = @numDomainID 
AND (dbo.DivisionMaster.numDivisionID = @numDivisionID OR @numDivisionID = 0)
AND (bitCleared = CASE @charReconFilter WHEN 'C' THEN 1 ELSE 0 END  OR RTRIM(@charReconFilter)='A')
AND (bitReconcile = CASE @charReconFilter WHEN 'R' THEN 1 ELSE 0 END  OR RTRIM(@charReconFilter)='A') 
--AND (dbo.Chart_Of_Accounts.numAccountId =@vcAccountId)
--AND (dbo.Chart_Of_Accounts. IN ( SELECT numAccountId FROM #Temp) OR  @tintMode=1)
AND (dbo.Chart_Of_Accounts.numAccountId= @vcAccountId)
AND (General_Journal_Details.numItemID = @numItemID OR @numItemID = 0)
   AND dbo.General_Journal_Details.numEntryDateSortOrder1  >= @first_id              
ORDER BY dbo.General_Journal_Details.numEntryDateSortOrder1 asc
--AND dbo.General_Journal_Details.numTransactionId >= @first_id
--ORDER BY dbo.Chart_Of_Accounts.numAccountId ASC,   dbo.General_Journal_Details.numTransactionId asc 
SET ROWCOUNT 0

END
	
IF object_id('TEMPDB.DBO.#Temp1') IS NOT NULL drop table #Temp1	
END
/****** Object:  StoredProcedure [dbo].[usp_GetMasterListDetailsDynamicForm]    Script Date: 07/26/2008 16:17:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By: Debasish Tapan Nag                                                    
--Purpose: Returns the available form fields from the database                                                        
--Created Date: 07/09/2005                            
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getmasterlistdetailsdynamicform')
DROP PROCEDURE usp_getmasterlistdetailsdynamicform
GO
CREATE PROCEDURE [dbo].[usp_GetMasterListDetailsDynamicForm]                      
 @numListID NUMERIC(9),                        
 @vcItemType CHAR(3),                      
 @numDomainID NUMERIC(9)                      
AS     
 
 IF @vcItemType = 'SYS'
	BEGIN
		SELECT 0 AS numItemID,'Lead' AS vcItemName
		UNION ALL
		SELECT 1 AS numItemID,'Prospect' AS vcItemName
		UNION ALL
		SELECT 2 AS numItemID,'Account' AS vcItemName
	END
	
 IF @vcItemType='LI' AND @numListID=9 --Opportunity Source
	 BEGIN
		EXEC USP_GetOpportunitySource @numDomainID
	 END                 
 ELSE IF @vcItemType = 'LI'    --Master List                    
	BEGIN                        
		SELECT LD.vcData AS vcItemName, LD.numListItemID AS numItemID, 'L' As vcItemType, LD.constFlag As flagConst 
		FROM ListDetails LD left join listorder LO on Ld.numListItemID= LO.numListItemID and Lo.numDomainId = @numDomainID  
		WHERE LD.numListID = @numListID AND (LD.numDomainID = @numDomainID OR LD.constFlag = 1)
		order by ISNULL(intSortOrder,LD.sintOrder)                        
	END                        
 ELSE IF @vcItemType = 'T'    --Territories                    
	BEGIN    
		SELECT vcData AS vcItemName, numListItemID AS numItemID, 'T' As vcItemType, 0 As flagConst FROM ListDetails WHERE numListID = 78 AND numDomainID = @numDomainID                                        
	END                        
 ELSE IF @vcItemType = 'AG'   --Lead Groups                     
	BEGIN                        
		SELECT vcGrpName AS vcItemName, numGrpId AS numItemID, 'G' As vcItemType, 0 As flagConst FROM Groups                     
	END                         
 ELSE IF @vcItemType = 'S'    --States                    
	BEGIN                        
		SELECT vcState AS vcItemName, numStateID AS numItemID, 'S' As vcItemType, numCountryID As flagConst FROM State WHERE (numDomainID = @numDomainID OR constFlag = 1) AND 1 = 2                    
	END         
 ELSE IF @vcItemType = 'U'    --Users                    
	BEGIN                        
		SELECT A.numContactID AS numItemID,A.vcFirstName+' '+A.vcLastName AS vcItemName              
		from UserMaster UM             
		join AdditionalContactsInformation A            
		on UM.numUserDetailId=A.numContactID              
		where UM.numDomainID=@numDomainID        
		union        
		select  A.numContactID,vcCompanyName+' - '+A.vcFirstName+' '+A.vcLastName        
		from AdditionalContactsInformation A         
		join DivisionMaster D        
		on D.numDivisionID=A.numDivisionID        
		join ExtarnetAccounts E         
		on E.numDivisionID=D.numDivisionID        
		join ExtranetAccountsDtl DTL        
		on DTL.numExtranetID=E.numExtranetID        
		join CompanyInfo C        
		on C.numCompanyID=D.numCompanyID        
		where A.numDomainID=@numDomainID and bitPartnerAccess=1        
		and D.numDivisionID <>(select numDivisionID from domain where numDomainID=@numDomainID)                      
	END
  ELSE IF @vcItemType = 'C'    --Organization Campaign                    
	BEGIN
	   SELECT  numCampaignID AS numItemID,
                vcCampaignName + CASE bitIsOnline WHEN 1 THEN ' (Online)' ELSE ' (Offline)' END AS vcItemName
       FROM    CampaignMaster
       WHERE   numDomainID = @numDomainID
	END               
  ELSE IF @vcItemType = 'IG' 
	BEGIN
		SELECT vcItemGroup AS vcItemName, numItemGroupID AS numItemID, 'I' As vcItemType, 0 As flagConst FROM dbo.ItemGroups WHERE numDomainID=@numDomainID   	
	END
 ELSE IF @vcItemType = 'PP' 
   BEGIN
		SELECT 'Inventory Item' AS vcItemName, 'P' AS numItemID, 'P' As vcItemType, 0 As flagConst 
		UNION 
		SELECT 'Non-Inventory Item' AS vcItemName, 'N' AS numItemID, 'P' As vcItemType, 0 As flagConst 
		UNION 
		SELECT 'Service' AS vcItemName, 'S' AS numItemID, 'P' As vcItemType, 0 As flagConst 
   END
 ELSE IF @vcItemType = 'V' 
   BEGIN
		SELECT DISTINCT ISNULL(C.vcCompanyName,'') AS vcItemName,  numVendorID AS numItemID ,'V' As vcItemType, 0 As flagConst   FROM dbo.Vendor V INNER JOIN dbo.DivisionMaster DM ON DM.numDivisionID= V.numVendorID 
		INNER JOIN dbo.CompanyInfo C ON C.numCompanyId = DM.numCompanyID WHERE V.numDomainID =@numDomainID
   END
 ELSE IF @vcItemType = 'OC' 
   BEGIN
		SELECT DISTINCT C.vcCurrencyDesc AS vcItemName,C.numCurrencyID AS numItemID FROM dbo.OpportunityMaster OM INNER JOIN dbo.Currency C ON C.numCurrencyID = OM.numCurrencyID
		WHERE OM.numDomainId=@numDomainID
   END
 ELSE  IF @vcItemType = 'UOM'    --Organization Campaign                    
	BEGIN
	   
	   
	   --SELECT  numUOMId AS numItemID,
    --            vcUnitName AS vcItemName
    --   FROM    UOM
    --   WHERE   numDomainID = @numDomainID

	--Modified by Sachin Sadhu ||Date:7thAug2014
	--Purpose :Boneta facing problem in updating units 
	   
			SELECT u.numUOMId as numItemID , u.vcUnitName as vcItemName FROM 
			UOM u INNER JOIN Domain d ON u.numDomainID=d.numDomainID
			WHERE u.numDomainID=@numDomainID AND d.numDomainID=@numDomainID AND 
			u.tintUnitType=(CASE WHEN ISNULL(d.charUnitSystem,'E')='E' THEN 1 WHEN d.charUnitSystem='M' THEN 2 END)


    END   
 ELSE IF @vcItemType = 'O'    --Opp Type                   
	 BEGIN
		SELECT  1 AS numItemID,'Sales' AS vcItemName
		UNion ALL
		SELECT  2 AS numItemID,'Purchase' AS vcItemName
	 END  
 ELSE  IF @vcItemType = 'CHK'    --Opp Type                   
	BEGIN
		SELECT  1 AS numItemID,'Yes' AS vcItemName
		UNion ALL
		SELECT  0 AS numItemID,'No' AS vcItemName
	END  
 ELSE IF @vcItemType='COA'
	BEGIN
		SELECT  C.[numAccountId] AS numItemID,ISNULL(C.[vcAccountName],'') AS vcItemName
		FROM    [Chart_Of_Accounts] C
		INNER JOIN [AccountTypeDetail] ATD 
		ON C.[numParntAcntTypeId] = ATD.[numAccountTypeID]
		WHERE   C.[numDomainId] = @numDomainId
		ORDER BY C.[vcAccountCode]
	END
       
GO
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

--SELECT * FROM [Chart_Of_Accounts] WHERE [numDomainId]=72

--SELECT SUBSTRING(CONVERT(VARCHAR(20),GETUTCDATE()),0,4)
-- EXEC USP_GetMonthlySummary 72, '2008-09-26 00:00:00.000', '2009-09-26 00:00:00.000','71'
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetMonthlySummary')
DROP PROCEDURE USP_GetMonthlySummary
GO
CREATE PROCEDURE [dbo].[USP_GetMonthlySummary](
               @numDomainId   AS INT,
               @dtFromDate    AS DATETIME,
               @dtToDate      AS DATETIME,
               @vcChartAcntId AS VARCHAR(500),
               @ClientTimeZoneOffset INT --Added by Chintan to enable calculation of date according to client machine
               )
AS
  BEGIN
  
	--SET @dtFromDate = DateAdd(minute, @ClientTimeZoneOffset, @dtFromDate);
	--SET @dtToDate = DateAdd(minute, @ClientTimeZoneOffset, @dtToDate);

	PRINT @dtFromDate 
	PRINT @dtToDate

	DECLARE @mnOpeningBalance AS MONEY
	SELECT @mnOpeningBalance = ISNULL(mnOpeningBalance,0)
	FROM dbo.fn_GetOpeningBalance(@vcChartAcntId,@numDomainID,@dtFromDate,@ClientTimeZoneOffset)
	

		/*RollUp of Sub Accounts */
		SELECT  COA2.numAccountId INTO #Temp /*,COA1.vcAccountCode ,COA2.vcAccountCode*/
		FROM    dbo.Chart_Of_Accounts COA1
				INNER JOIN dbo.Chart_Of_Accounts COA2 ON COA1.numDomainId = COA2.numDomainId
		WHERE   COA1.numAccountId IN(SELECT  cast(ID AS NUMERIC(9)) FROM dbo.SplitIDs(@vcChartAcntId, ','))
				AND COA1.numDomainId = @numDomainID
				AND (COA2.vcAccountCode LIKE COA1.vcAccountCode OR (COA2.vcAccountCode LIKE COA1.vcAccountCode + '%' AND COA2.numParentAccID>0))
				
		SELECT @vcChartAcntId = ISNULL( STUFF((SELECT ',' + CAST(numAccountID AS VARCHAR(10)) FROM #Temp FOR XML PATH('')),1, 1, '') , '') 
  
  
    SELECT   numAccountId,
             COAvcAccountCode,
             vcAccountName,
             numAccountTypeID,
             vcAccountType,
             SUBSTRING(CONVERT(VARCHAR(20),datEntry_Date),0,4) +' ' +  CONVERT(VARCHAR(4), YEAR(datEntry_Date)) AS MONTH,
             MONTH(datEntry_Date) AS MonthNumber,
             YEAR(datEntry_Date) AS YEAR,
             SUM(Debit) AS Debit,
             SUM(Credit) AS Credit,
             @mnOpeningBalance AS Opening, 
             0.00 AS Closing
             
             INTO #Temp1
    FROM     VIEW_JOURNAL
    WHERE    datEntry_Date BETWEEN @dtFromDate AND @dtToDate
             AND numDomainId = @numDomainID
             AND numAccountId IN (SELECT * FROM   dbo.SplitIds(@vcChartAcntId,','))
    GROUP BY numAccountId,
             COAvcAccountCode,
             vcAccountName,
             numAccountTypeID,
             vcAccountType,
             MONTH(datEntry_Date),
             YEAR(datEntry_Date),
             SUBSTRING(CONVERT(VARCHAR(20),datEntry_Date),0,4) +' ' +  CONVERT(VARCHAR(4), YEAR(datEntry_Date))
    ORDER BY YEAR(datEntry_Date),MONTH(datEntry_Date)
    
    
--    SELECT DATEPART(MONTH,dtPeriodFrom) FY_Opening_Month,DATEPART(year,dtPeriodFrom) FY_Opening_year, * FROM dbo.FinancialYear WHERE numDomainId=@numDomainId
--Following logic applieds to only income, expense and cogs account
IF EXISTS( SELECT * FROM #Temp1 where SUBSTRING(COAvcAccountCode,0,5) IN ('0103','0104','0106') )
BEGIN
	 alter table #Temp1 alter column MONTH VARCHAR(50);
	 ALTER TABLE [#Temp1] ADD IsFinancialMonth BIT;
	 UPDATE [#Temp1] SET IsFinancialMonth = 0

    DECLARE @numMaxFinYearID NUMERIC
    DECLARE @Month INT
    DECLARE @year  INT
    
    SELECT @numMaxFinYearID = MAX(numFinYearId) FROM dbo.FinancialYear WHERE numDomainId=@numDomainId 
--    PRINT @numMaxFinYearID
    WHILE @numMaxFinYearID > 0
    BEGIN
			SELECT @Month = DATEPART(MONTH,dtPeriodFrom),@year =DATEPART(year,dtPeriodFrom) FROM dbo.FinancialYear WHERE numDomainId=@numDomainId AND numFinYearId=@numMaxFinYearID
    
--    PRINT @Month
--    PRINT @year
	/*Rule:make first month of each FY zero as opening balance and show report respectively */
			UPDATE #Temp1  SET Opening = -0.0001 ,[MONTH] = [MONTH] + ' FY Start', IsFinancialMonth = 1
			WHERE MonthNumber = @Month AND YEAR= @year
			
			IF @@ROWCOUNT=0 -- when No data avail for First month of FY
			BEGIN
				SELECT TOP 1  @Month = MonthNumber FROM #Temp1 WHERE MonthNumber>@Month AND YEAR=@year
				UPDATE #Temp1  SET Opening = -0.0001 ,[MONTH] = [MONTH] + ' FY Start', IsFinancialMonth = 1
				WHERE MonthNumber = @Month AND YEAR= @year
			END
			
    	
    	
    	 SELECT @numMaxFinYearID = MAX(numFinYearId) FROM dbo.FinancialYear WHERE numDomainId=@numDomainId AND numFinYearId < @numMaxFinYearID
    	IF @@ROWCOUNT = 0 
    	SET @numMaxFinYearID = 0
    END
END
   

    
    SELECT * FROM #Temp1
    
     DROP TABLE #Temp
     DROP TABLE #Temp1
  END
  
--  exec USP_GetMonthlySummary @numDomainId=89,@dtFromDate='2011-12-01 00:00:00:000',@dtToDate='2013-03-01 23:59:59:000',@vcChartAcntId='1444',@ClientTimeZoneOffset=-330

set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

--created by anoop jayaraj   
--EXEC [USP_GetPageNavigation] @numModuleID=9,@numDomainID=1

         
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getpagenavigation')
DROP PROCEDURE usp_getpagenavigation
GO
CREATE PROCEDURE [dbo].[USP_GetPageNavigation]            
@numModuleID as numeric(9),      
@numDomainID as numeric(9),
@numGroupID NUMERIC(18, 0)        
as            
if @numModuleID = 14      
begin
           
select  PND.numPageNavID,
        numModuleID,
        numParentID,
        vcPageNavName,
        isnull(vcNavURL, '') as vcNavURL,
        vcImageURL,
        0 intSortOrder
from    PageNavigationDTL PND
        JOIN dbo.TreeNavigationAuthorization TNA ON PND.numPageNavID = TNA.numPageNavID  --AND PND.numTabID = TNA.numTabID       
 WHERE  1 = 1
        AND ( ISNULL(TNA.bitVisible, 0) = 1
              --OR ISNULL(numParentID, 0) = 0
              --OR ISNULL(TNA.bitVisible, 0) = 0
            )
        AND ISNULL(PND.bitVisible, 0) = 1
        AND numModuleID = 14
        AND TNA.numDomainID = @numDomainID
        AND numGroupID = @numGroupID
union
select  1111 numPageNavID,
        14 numModuleID,
        133 numParentID,
        'Regular Documents' as vcPageNavName,
        '../Documents/frmRegularDocList.aspx?Category=0',
        '../images/tf_note.gif' vcImageURL,
        1
union

--select numListItemId,9 numModuleID,1111 numParentID,vcData vcPageNavName,'../Documents/frmDocList.aspx?TabID=1&Category=' + cast(numListItemId as varchar(10)) vcImageURL ,'../images/tf_note.gif',-2 vcImageURL
-- from ListDetails where numListid=29 and constFlag=1 
--UNION
--
--select numListItemId,9 numModuleID,1111 numParentID,vcData vcPageNavName,'../Documents/frmDocList.aspx?TabID=1&Category=' + cast(numListItemId as varchar(10)) vcImageURL ,'../images/tf_note.gif',-2 vcImageURL
-- from ListDetails where numListid=29 and numDomainID=@numDomainID
-- 
SELECT  Ld.numListItemId,
        14 numModuleID,
        1111 numParentID,
        vcData vcPageNavName,
        '../Documents/frmRegularDocList.aspx?Category='
        + cast(Ld.numListItemId as varchar(10)) vcImageURL,
        '../images/tf_note.gif',
        ISNULL(intSortOrder, LD.sintOrder) SortOrder
FROM    listdetails Ld
        left join listorder LO on Ld.numListItemID = LO.numListItemID
                                  and Lo.numDomainId = @numDomainID
WHERE   Ld.numListID = 29
        and ( constFlag = 1
              or Ld.numDomainID = @numDomainID
            )
--order by ISNULL(intSortOrder,LD.sintOrder)
--union
--   
-- SELECT 0 numPageNavID,9 numModuleID,1111 numParentID, vcData vcPageNavName,'../Documents/frmDocList.aspx?TabID=1&Category='+convert(varchar(10), LD.numListItemID) vcNavURL      
-- ,'../images/Text.gif' vcImageURL,0      
-- FROM listdetails Ld   
-- left join listorder LO on Ld.numListItemID= LO.numListItemID  and lo.numDomainId = @numDomainId        
-- WHERE  LD.numListID=28 and (constFlag=1 or  LD.numDomainID=@numDomainID)    
union
SELECT  ld.numListItemID AS numPageNavID,
        14 numModuleID,
        133 numParentID,
        vcData vcPageNavName,
        '../Documents/frmDocList.aspx?Category='
        + convert(varchar(10), LD.numListItemID) vcNavURL,
        '../images/tf_note.gif' vcImageURL,
        -3
FROM    listdetails Ld
        left join listorder LO on Ld.numListItemID = LO.numListItemID
                                  and lo.numDomainId = @numDomainId
        inner join AuthoritativeBizDocs AB on ( LD.numListItemID = AB.numAuthoritativeSales )
WHERE   LD.numListID = 27
        and ( constFlag = 1
              or LD.numDomainID = @numDomainID
            )
        and AB.numDomainID = @numDomainID
union
SELECT  ld.numListItemID AS numPageNavID,
        14 numModuleID,
        133 numParentID,
        vcData vcPageNavName,
        '../Documents/frmDocList.aspx?Category='
        + convert(varchar(10), LD.numListItemID) vcNavURL,
        '../images/tf_note.gif' vcImageURL,
        -3
FROM    listdetails Ld
        left join listorder LO on Ld.numListItemID = LO.numListItemID
                                  and lo.numDomainId = @numDomainId
        inner join AuthoritativeBizDocs AB on ( ld.numListItemID = AB.numAuthoritativePurchase )
WHERE   LD.numListID = 27
        and ( constFlag = 1
              or LD.numDomainID = @numDomainID
            )
        and AB.numDomainID = @numDomainID
UNION
select  0 numPageNavID,
        14 numModuleID,
        LD.numListItemID AS numParentID,
        LT.vcData as vcPageNavName,
        '../Documents/frmDocList.aspx?Category='
        + convert(varchar(10), Ld.numListItemID) + '&Status='
        + convert(varchar(10), LT.numListItemID) vcNavURL,
        '../images/Text.gif' vcImageURL,
        0
from    ListDetails LT,
        listdetails ld
where   LT.numListID = 11
        and Lt.numDomainID = @numDomainID
        AND LD.numListID = 27
        AND ( ld.constFlag = 1
              or LD.numDomainID = @numDomainID
            )
        and ( LD.numListItemID in ( select  AB.numAuthoritativeSales
                                    from    AuthoritativeBizDocs AB
                                    where   AB.numDomainId = @numDomainID )
              or ld.numListItemID in ( select   AB.numAuthoritativePurchase
                                       from     AuthoritativeBizDocs AB
                                       where    AB.numDomainId = @numDomainID )
            )


--SELECT * FROM AuthoritativeBizDocs where numDomainId=72

 
--UNION
--	SELECT 0 numPageNavID,9 numModuleID,133 numParentID, 'BizDoc Approval Rule' as  vcPageNavName,'../Documents/frmBizDocAppRule.aspx' as vcNavURL      
-- ,'../images/tf_note.gif' vcImageURL,0 
--
--UNION
--	SELECT 0 numPageNavID,9 numModuleID,133 numParentID, 'Order Auto Rule' as  vcPageNavName,'../Documents/frmOrderAutoRules.aspx' as vcNavURL      
-- ,'../images/tf_note.gif' vcImageURL,1  
union
select  2222 numPageNavID,
        14 numModuleID,
        133 numParentID,
        'Other BizDocs' as vcPageNavName,
        '',
        '../images/tf_note.gif' vcImageURL,
        -1
union
SELECT  ld.numListItemID AS numPageNavID,
        14 numModuleID,
        2222 numParentID,
        vcData vcPageNavName,
        '../Documents/frmDocList.aspx?Category='
        + convert(varchar(10), LD.numListItemID) vcNavURL,
        '../images/Text.gif' vcImageURL,
        -1
FROM    listdetails Ld
where   ld.numListID = 27
        and ( constFlag = 1
              or LD.numDomainID = @numDomainID
            )
        and LD.numListItemID not in ( select    AB.numAuthoritativeSales
                                      from      AuthoritativeBizDocs AB
                                      where     AB.numDomainID = @numDomainID )
        and LD.numListItemID not in ( select    AB.numAuthoritativePurchase
                                      from      AuthoritativeBizDocs AB
                                      where     AB.numDomainID = @numDomainID )
union
SELECT  ls.numListItemID AS numPageNavID,
        14 numModuleID,
        ld.numListItemID as numParentID,
        ls.vcData vcPageNavName,
        '../Documents/frmDocList.aspx?Category='
        + convert(varchar(10), LD.numListItemID) vcNavURL,
        null vcImageURL,
        0
FROM    listdetails Ld,
        ( select    *
          from      ListDetails LS
          where     LS.numDomainID = @numDomainID
                    and LS.numListID = 11
        ) LS
where   ld.numListID = 27
        and ( ld.constFlag = 1
              or LD.numDomainID = @numDomainID
            )
        and LD.numListItemID not in ( select    AB.numAuthoritativeSales
                                      from      AuthoritativeBizDocs AB
                                      where     AB.numDomainID = @numDomainID
                                      union
                                      select    AB.numAuthoritativePurchase
                                      from      AuthoritativeBizDocs AB
                                      where     AB.numDomainID = @numDomainID )
order by numParentID,
        intSortOrder,
        numPageNavID

END
ELSE IF @numModuleID = 35 
BEGIN
	select PND.numPageNavID,numModuleID,numParentID,vcPageNavName,isnull(vcNavURL,'') as vcNavURL,vcImageURL ,0 sintOrder      
	from PageNavigationDTL PND
	JOIN dbo.TreeNavigationAuthorization TNA ON PND.numPageNavID = TNA.numPageNavID  --AND PND.numTabID = TNA.numTabID       
	WHERE  1 = 1
        AND ( ISNULL(TNA.bitVisible, 0) = 1
              --OR ISNULL(numParentID, 0) = 0
              --OR ISNULL(TNA.bitVisible, 0) = 0
            ) 
        AND ISNULL(PND.bitVisible, 0) = 1
	AND numModuleID=@numModuleID --AND bitVisible=1  
	AND TNA.numDomainID = @numDomainID
	AND numGroupID = @numGroupID
	    
	UNION 
	 SELECT numFRID,35,CASE numFinancialViewID WHEN 26770 THEN 96 /*Profit & Loss*/
	WHEN 26769 THEN 176 /*Income & Expense*/
	WHEN 26768 THEN 98 /*Cash Flow Statement*/
	WHEN 26767 THEN 97 /*Balance Sheet*/
	WHEN 26766 THEN 81 /*A/R & A/P Aging*/
	WHEN 26766 THEN 82 /*A/R & A/P Aging*/
	ELSE 0
	END 
	  ,vcReportName,CASE numFinancialViewID 
	WHEN 26770 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =96 AND numModuleID=35)  /*Profit & Loss*/
	WHEN 26769 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =176 AND numModuleID=35) /*Income & Expense*/
	WHEN 26768 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =98 AND numModuleID=35) /*Cash Flow Statement*/
	WHEN 26767 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =97 AND numModuleID=35) /*Balance Sheet*/
	WHEN 26766 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =81 AND numModuleID=35) /*A/R & A/P Aging*/
	WHEN 26766 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =82 AND numModuleID=35) /*A/R & A/P Aging*/
	ELSE ''
	END + 
	CASE CHARINDEX('?',(CASE numFinancialViewID 
	WHEN 26770 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =96 AND numModuleID=35)  /*Profit & Loss*/
	WHEN 26769 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =176 AND numModuleID=35) /*Income & Expense*/
	WHEN 26768 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =98 AND numModuleID=35) /*Cash Flow Statement*/
	WHEN 26767 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =97 AND numModuleID=35) /*Balance Sheet*/
	WHEN 26766 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =81 AND numModuleID=35) /*A/R & A/P Aging*/
	WHEN 26766 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =82 AND numModuleID=35) /*A/R & A/P Aging*/
	ELSE ''
	END)) 
	WHEN 0 THEN '?FRID='
	ELSE '&FRID='
	END
	 + CAST(numFRID AS VARCHAR)
	 'url',NULL,0 FROM dbo.FinancialReport WHERE numDomainID=@numDomainID
	order by numParentID,numPageNavID  
END
-------------- ADDED BY MANISH ANJARA ON : 6th April,2012
ELSE IF @numModuleID = -1 AND @numDomainID = -1 
BEGIN 
	SELECT PND.numPageNavID,numModuleID,numParentID,vcPageNavName,isnull(vcNavURL,'') as vcNavURL,vcImageURL ,0 sintOrder      
	FROM PageNavigationDTL PND
	JOIN dbo.TreeNavigationAuthorization TNA ON PND.numPageNavID = TNA.numPageNavID  --AND PND.numTabID = TNA.numTabID       
	WHERE  1 = 1
        AND ( ISNULL(TNA.bitVisible, 0) = 1
              --OR ISNULL(numParentID, 0) = 0
              --OR ISNULL(TNA.bitVisible, 0) = 0
            )
        AND ISNULL(PND.bitVisible, 0) = 1
	--AND TNA.numDomainID = @numDomainID
	AND numGroupID = @numGroupID
	--WHERE bitVisible=1  
	ORDER BY numParentID,numPageNavID 
END
--------------------------------------------------------- 
-------------- ADDED BY MANISH ANJARA ON : 12th Oct,2012
ELSE IF @numModuleID = 10 
BEGIN 
	DECLARE @numParentID AS NUMERIC(18,0)
	SELECT TOP 1 @numParentID = numParentID FROM dbo.PageNavigationDTL WHERE numModuleID = 10
	PRINT @numParentID

	SELECT * FROM (
	SELECT PND.numPageNavID,numModuleID,numParentID,vcPageNavName,isnull(vcNavURL,'') as vcNavURL,vcImageURL ,0 sintOrder      
	FROM PageNavigationDTL PND
	JOIN dbo.TreeNavigationAuthorization TNA ON PND.numPageNavID = TNA.numPageNavID  --AND PND.numTabID = TNA.numTabID       
	WHERE  1 = 1
        AND ( ISNULL(TNA.bitVisible, 0) = 1
              --OR ISNULL(numParentID, 0) = 0
              --OR ISNULL(TNA.bitVisible, 0) = 0
            )
        AND ISNULL(PND.bitVisible, 0) = 1
	AND numModuleID = @numModuleID
	AND TNA.numDomainID = @numDomainID
	AND numGroupID = @numGroupID
	
	UNION ALL

	SELECT  numListItemID AS numPageNavID,
		    (SELECT numModuleID FROM ModuleMaster WHERE vcModuleName = 'Opportunities') AS [numModuleID],
			@numParentID AS numParentID,
			ISNULL(vcData,'') + 's' [vcPageNavName],
			'../Opportunity/frmOpenBizDocs.aspx?BizDocName=' + vcData + '&BizDocID=' + CONVERT(VARCHAR(10),LD.numListItemID) AS [vcNavURL],
			'../images/Text.gif' AS [vcImageURL],
			1 AS [bitVisible] 
	FROM dbo.ListDetails LD
	JOIN dbo.TreeNavigationAuthorization TNA ON LD.numListItemID = TNA.numPageNavID  AND ISNULL(bitVisible,0) = 1  
	WHERE numListID = 27 
	AND ISNULL(TNA.[numDomainID], 0) = @numDomainID
	AND ISNULL(TNA.[numGroupID], 0) = @numGroupID) AS [FINAL]
	ORDER BY numParentID,FINAL.numPageNavID 
END
--------------------------------------------------------- 
--ELSE IF @numModuleID = 13
--	BEGIN
--		select PND.numPageNavID,
--        numModuleID,
--        numParentID,
--        vcPageNavName,
--        isnull(vcNavURL, '') as vcNavURL,
--        vcImageURL,
--        0 sintOrder
-- from   PageNavigationDTL PND
-- WHERE  1 = 1
--        AND ISNULL(PND.bitVisible, 0) = 1
--        AND numModuleID = @numModuleID
-- order by numParentID,
--        numPageNavID 
--	END
--	
--	ELSE IF @numModuleID = 9
--	BEGIN
--			select PND.numPageNavID,
--        numModuleID,
--        numParentID,
--        vcPageNavName,
--        isnull(vcNavURL, '') as vcNavURL,
--        vcImageURL,
--        0 sintOrder
-- from   PageNavigationDTL PND
-- WHERE  1 = 1
--        AND ISNULL(PND.bitVisible, 0) = 1
--        AND numModuleID = @numModuleID
-- order by numParentID,
--        numPageNavID 
--	END

ELSE IF (@numModuleID = 7 OR @numModuleID = 36)
BEGIN
  select    PND.numPageNavID,
            numModuleID,
            numParentID,
            vcPageNavName,
            isnull(vcNavURL, '') as vcNavURL,
            vcImageURL,
            0 sintOrder
  from      PageNavigationDTL PND
  WHERE     1 = 1
            AND ISNULL(PND.bitVisible, 0) = 1
            AND numModuleID = @numModuleID
  order by  numParentID,
            numPageNavID 
END	
ELSE IF @numModuleID = 13
BEGIN
	select PND.numPageNavID,
        PND.numModuleID,
        numParentID,
        vcPageNavName,
        isnull(vcNavURL, '') as vcNavURL,
        vcImageURL,
        0 sintOrder
	 from   PageNavigationDTL PND
			JOIN dbo.TreeNavigationAuthorization TNA ON PND.numPageNavID = TNA.numPageNavID  --AND PND.numTabID = TNA.numTabID       
	 WHERE  1 = 1
			AND ( ISNULL(TNA.bitVisible, 0) = 1
				  --OR ISNULL(numParentID, 0) = 0
				  --OR ISNULL(TNA.bitVisible, 0) = 0
				)
			AND ISNULL(PND.bitVisible, 0) = 1
			AND PND.numModuleID = @numModuleID --AND bitVisible=1  
			AND numGroupID = @numGroupID
	 order by 
		(CASE WHEN (@numModuleID = 13 AND PND.numPageNavID=79) THEN 2000 ELSE 0 END),
		numParentID,
        numPageNavID   
END
else      
begin            
 select PND.numPageNavID,
        PND.numModuleID,
        numParentID,
        vcPageNavName,
        isnull(vcNavURL, '') as vcNavURL,
        vcImageURL,
        0 sintOrder
 from   PageNavigationDTL PND
        JOIN dbo.TreeNavigationAuthorization TNA ON PND.numPageNavID = TNA.numPageNavID  --AND PND.numTabID = TNA.numTabID       
 WHERE  1 = 1
        AND ( ISNULL(TNA.bitVisible, 0) = 1
              --OR ISNULL(numParentID, 0) = 0
              --OR ISNULL(TNA.bitVisible, 0) = 0
            )
        AND ISNULL(PND.bitVisible, 0) = 1
        AND PND.numModuleID = @numModuleID --AND bitVisible=1  
        AND numGroupID = @numGroupID
 order by numParentID,
        numPageNavID      
END

--select * from TreeNavigationAuthorization where numModuleID=13 
--select * from TreeNavigationAuthorization where numTabID=-1 AND numDomainID = 1 AND numGroupID = 1
--select * from TreeNavigationAuthorization where numListID=11 and numDomainId=72
--select * from AuthoritativeBizDocs where numDomainId=72











GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetPageNavigationAuthorizationDetails' ) 
    DROP PROCEDURE USP_GetPageNavigationAuthorizationDetails
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- EXEC USP_GetPageNavigationAuthorizationDetails 1,1
CREATE PROCEDURE USP_GetPageNavigationAuthorizationDetails
    (
      @numGroupID NUMERIC(18, 0),
      @numTabID NUMERIC(18, 0),
      @numDomainID NUMERIC(18, 0)
    )
AS 
    SET NOCOUNT ON
    SET TRANSACTION ISOLATION LEVEL READ COMMITTED
  
    BEGIN
		/*
		SELECT * FROM dbo.TabMaster
		SELECT * FROM dbo.ModuleMaster
		SELECT * FROM 
		*/
        PRINT @numTabID 
        DECLARE @numModuleID AS NUMERIC(18, 0)
        SELECT TOP 1
                @numModuleID = numModuleID
        FROM    dbo.PageNavigationDTL
        WHERE   numTabID = @numTabID 
        PRINT @numModuleID
        
        IF @numModuleID = 10 
            BEGIN
                PRINT 10
                SELECT  *
                FROM    ( SELECT    ISNULL(( SELECT TOP 1
                                                    TNA.[numPageAuthID]
                                             FROM   dbo.TreeNavigationAuthorization TNA
                                             WHERE  TNA.numPageNavID = PND.[numPageNavID]
                                                    AND TNA.numDomainID = @numDomainID
                                                    AND TNA.numGroupID = @numGroupID
                                                    AND TNA.numTabID = @numTabID
                                           ), 0) AS [numPageAuthID],
                                    ISNULL(@numGroupID, 0) AS [numGroupID],
                                    ISNULL(PND.[numTabID], 0) AS [numTabID],
                                    ISNULL(PND.[numPageNavID], 0) AS [numPageNavID],
                                    ISNULL(( SELECT TOP 1
                                                    TNA.[bitVisible]
                                             FROM   dbo.TreeNavigationAuthorization TNA
                                             WHERE  TNA.numPageNavID = PND.[numPageNavID]
                                                    AND TNA.numDomainID = @numDomainID
                                                    AND TNA.numGroupID = @numGroupID
                                                    AND TNA.numTabID = @numTabID
                                           ), 0) AS [bitVisible],
                                    @numDomainID AS [numDomainID],
                                    ( SELECT TOP 1
                                                vcPageNavName
                                      FROM      dbo.PageNavigationDTL
                                      WHERE     PageNavigationDTL.numPageNavID = PND.numPageNavID
                                    ) AS [vcNodeName],
                                    ISNULL(( SELECT TOP 1
                                                    TNA.tintType
                                             FROM   dbo.TreeNavigationAuthorization TNA
                                             WHERE  TNA.numPageNavID = PND.[numPageNavID]
                                                    AND TNA.numDomainID = @numDomainID
                                                    AND TNA.numGroupID = @numGroupID
                                                    AND TNA.numTabID = @numTabID
                                           ), 1) AS [tintType]
                          FROM      dbo.PageNavigationDTL PND
                          WHERE     ISNULL(PND.bitVisible, 0) = 1
                                    AND PND.numTabID = @numTabID
                                    AND numModuleID = @numModuleID
                          UNION ALL
                          SELECT    ISNULL(( SELECT TOP 1
                                                    TNA.[numPageAuthID]
                                             FROM   dbo.TreeNavigationAuthorization TNA
                                             WHERE  TNA.numPageNavID = LD.[numListItemID]
                                                    AND TNA.numDomainID = @numDomainID
                                                    AND TNA.numGroupID = @numGroupID
                                                    AND TNA.numTabID = @numTabID
                                           ), 0) AS [numPageAuthID],
                                    @numGroupID AS [numGroupID],
                                    1 AS [numTabID],
                                    ISNULL(LD.[numListItemID], 0) AS [numPageNavID],
                                    ISNULL(( SELECT TOP 1
                                                    TNA.[bitVisible]
                                             FROM   dbo.TreeNavigationAuthorization TNA
                                             WHERE  TNA.numPageNavID = LD.[numListItemID]
                                                    AND TNA.numDomainID = @numDomainID
                                                    AND TNA.numGroupID = @numGroupID
                                                    AND TNA.numTabID = @numTabID
                                           ), 0) AS [bitVisible],
                                    ISNULL(LD.[numDomainID], 0) AS [numDomainID],
                                    ISNULL(vcData, '') + 's' AS [vcNodeName],
                                    ISNULL(( SELECT TOP 1
                                                    TNA.tintType
                                             FROM   dbo.TreeNavigationAuthorization TNA
                                             WHERE  TNA.numPageNavID = LD.[numListItemID]
                                                    AND TNA.numDomainID = @numDomainID
                                                    AND TNA.numGroupID = @numGroupID
                                                    AND TNA.numTabID = @numTabID
                                           ), 1) AS [tintType]
                          FROM      dbo.ListDetails LD
						--LEFT JOIN dbo.TreeNavigationAuthorization TNA ON LD.numListItemID = TNA.numPageNavID 
                          WHERE     numListID = 27
                                    AND LD.numDomainID = @numDomainID
                                    AND ISNULL(constFlag, 0) = 0
						--AND ( numTabID = (CASE WHEN @numModuleID = 10 THEN 1 ELSE 0 END) OR ISNULL(numTabID,0) = 0)
                          UNION ALL
                          SELECT    ISNULL(( SELECT TNA.[numPageAuthID]
                                             FROM   dbo.TreeNavigationAuthorization TNA
                                             WHERE  TNA.numPageNavID = LD.[numListItemID]
                                                    AND TNA.numDomainID = @numDomainID
                                                    AND TNA.numGroupID = @numGroupID
                                                    AND TNA.numTabID = @numTabID
                                           ), 0) AS [numPageAuthID],
                                    @numGroupID AS [numGroupID],
                                    1 AS [numTabID],
                                    ISNULL(LD.[numListItemID], 0) AS [numPageNavID],
                                    ISNULL(( SELECT TNA.[bitVisible]
                                             FROM   dbo.TreeNavigationAuthorization TNA
                                             WHERE  TNA.numPageNavID = LD.[numListItemID]
                                                    AND TNA.numDomainID = @numDomainID
                                                    AND TNA.numGroupID = @numGroupID
                                                    AND TNA.numTabID = @numTabID
                                           ), 0) AS [bitVisible],
                                    @numDomainID AS [numDomainID],
                                    ISNULL(vcData, '') + 's' AS [vcNodeName],
                                    ISNULL(( SELECT TNA.tintType
                                             FROM   dbo.TreeNavigationAuthorization TNA
                                             WHERE  TNA.numPageNavID = LD.[numListItemID]
                                                    AND TNA.numDomainID = @numDomainID
                                                    AND TNA.numGroupID = @numGroupID
                                                    AND TNA.numTabID = @numTabID
                                           ), 1) AS [tintType]
                          FROM      dbo.ListDetails LD
						--LEFT JOIN dbo.TreeNavigationAuthorization TNA ON LD.numListItemID = TNA.numPageNavID AND ISNULL(TNA.[numDomainID], 0) = @numDomainID
                          WHERE     numListID = 27
                                    AND ISNULL(constFlag, 0) = 1
                        ) TABLE1
                ORDER BY numPageNavID
            END
		
        IF @numModuleID = 14 
            BEGIN
                IF NOT EXISTS ( SELECT  *
                                FROM    TreeNavigationAuthorization
                                WHERE   numTabID = 8
                                        AND numDomainID = 1
                                        AND numGroupID = 1 ) 
                    BEGIN
                        SELECT  TNA.numPageAuthID,
                                TNA.numGroupID,
                                TNA.numTabID,
                                TNA.numPageNavID,
                                TNA.bitVisible,
                                TNA.numDomainID,
                                ISNULL(PND.vcPageNavName, '') AS [vcNodeName],
                                tintType
                        FROM    PageNavigationDTL PND
                                JOIN dbo.TreeNavigationAuthorization TNA ON PND.numPageNavID = TNA.numPageNavID  --AND PND.numTabID = TNA.numTabID       
                        WHERE   1 = 1
                                --AND ISNULL(TNA.bitVisible, 0) = 1
                                AND ISNULL(PND.bitVisible, 0) = 1
                                AND numModuleID = @numModuleID
                                AND TNA.numDomainID = @numDomainID
                                AND numGroupID = @numGroupID
                        UNION
                        SELECT  0 numPageAuthID,
                                @numGroupID numGroupID,
                                @numTabID numTabID,
                                1111 numPageNavID,
                                1 bitVisible,
                                @numDomainID numDomainID,
                                'Regular Documents' AS [vcNodeName],
                                1 tintType
                        UNION
                        SELECT  0 numPageAuthID,
                                @numGroupID numGroupID,
                                @numTabID numTabID,
                                Ld.numListItemId,
                                1 bitVisible,
                                @numDomainID numDomainID,
                                ISNULL(vcData, '') AS [vcNodeName],
                                1 tintType
                        FROM    listdetails Ld
                                LEFT JOIN listorder LO ON Ld.numListItemID = LO.numListItemID
                                                          AND Lo.numDomainId = @numDomainID
                        WHERE   Ld.numListID = 29
                                AND ( constFlag = 1
                                      OR Ld.numDomainID = @numDomainID
                                    )
                        UNION
                        SELECT  0 numPageAuthID,
                                @numGroupID numGroupID,
                                @numTabID numTabID,
                                ld.numListItemID numPageNavID,
                                1 bitVisible,
                                @numDomainID numDomainID,
                                ISNULL(vcData, '') AS [vcNodeName],
                                1 tintType
                        FROM    listdetails Ld
                                LEFT JOIN listorder LO ON Ld.numListItemID = LO.numListItemID
                                                          AND lo.numDomainId = @numDomainID
                                INNER JOIN AuthoritativeBizDocs AB ON ( LD.numListItemID = AB.numAuthoritativeSales )
                        WHERE   LD.numListID = 27
                                AND ( constFlag = 1
                                      OR LD.numDomainID = @numDomainID
                                    )
                                AND AB.numDomainID = @numDomainID
                        UNION
                        SELECT  0 numPageAuthID,
                                @numGroupID numGroupID,
                                @numTabID numTabID,
                                ld.numListItemID numPageNavID,
                                1 bitVisible,
                                @numDomainID numDomainID,
                                ISNULL(vcData, '') AS [vcNodeName],
                                1 tintType
                        FROM    listdetails Ld
                                LEFT JOIN listorder LO ON Ld.numListItemID = LO.numListItemID
                                                          AND lo.numDomainId = @numDomainID
                                INNER JOIN AuthoritativeBizDocs AB ON ( ld.numListItemID = AB.numAuthoritativePurchase )
                        WHERE   LD.numListID = 27
                                AND ( constFlag = 1
                                      OR LD.numDomainID = @numDomainID
                                    )
                                AND AB.numDomainID = @numDomainID
                        UNION
                        SELECT  0 numPageAuthID,
                                @numGroupID numGroupID,
                                @numTabID numTabID,
                                0 numPageNavID,
                                1 bitVisible,
                                @numDomainID numDomainID,
                                ISNULL(LT.vcData, '') AS [vcNodeName],
                                1 tintType
                        FROM    ListDetails LT,
                                listdetails ld
                        WHERE   LT.numListID = 11
                                AND Lt.numDomainID = @numDomainID
                                AND LD.numListID = 27
                                AND ( ld.constFlag = 1
                                      OR LD.numDomainID = @numDomainID
                                    )
                                AND ( LD.numListItemID IN (
                                      SELECT    AB.numAuthoritativeSales
                                      FROM      AuthoritativeBizDocs AB
                                      WHERE     AB.numDomainId = @numDomainID )
                                      OR ld.numListItemID IN (
                                      SELECT    AB.numAuthoritativePurchase
                                      FROM      AuthoritativeBizDocs AB
                                      WHERE     AB.numDomainId = @numDomainID )
                                    )
                        UNION
                        SELECT  0 numPageAuthID,
                                @numGroupID numGroupID,
                                @numTabID numTabID,
                                2222 numPageNavID,
                                1 bitVisible,
                                @numDomainID numDomainID,
                                'Other BizDocs' AS [vcNodeName],
                                1 tintType
                        UNION
                        SELECT  0 numPageAuthID,
                                @numGroupID numGroupID,
                                @numTabID numTabID,
                                ld.numListItemID numPageNavID,
                                1 bitVisible,
                                @numDomainID numDomainID,
                                ISNULL(vcData, '') AS [vcNodeName],
                                1 tintType
                        FROM    listdetails Ld
                        WHERE   ld.numListID = 27
                                AND ( constFlag = 1
                                      OR LD.numDomainID = @numDomainID
                                    )
                                AND LD.numListItemID NOT IN (
                                SELECT  AB.numAuthoritativeSales
                                FROM    AuthoritativeBizDocs AB
                                WHERE   AB.numDomainID = @numDomainID )
                                AND LD.numListItemID NOT IN (
                                SELECT  AB.numAuthoritativePurchase
                                FROM    AuthoritativeBizDocs AB
                                WHERE   AB.numDomainID = @numDomainID )
                        UNION
                        SELECT  0 numPageAuthID,
                                @numGroupID numGroupID,
                                @numTabID numTabID,
                                ls.numListItemID numPageNavID,
                                1 bitVisible,
                                @numDomainID numDomainID,
                                ISNULL(ls.vcData, '') AS [vcNodeName],
                                1 tintType
                        FROM    listdetails Ld,
                                ( SELECT    *
                                  FROM      ListDetails LS
                                  WHERE     LS.numDomainID = @numDomainID
                                            AND LS.numListID = 11
                                ) LS
                        WHERE   ld.numListID = 27
                                AND ( ld.constFlag = 1
                                      OR LD.numDomainID = @numDomainID
                                    )
                                AND LD.numListItemID NOT IN (
                                SELECT  AB.numAuthoritativeSales
                                FROM    AuthoritativeBizDocs AB
                                WHERE   AB.numDomainID = @numDomainID
                                UNION
                                SELECT  AB.numAuthoritativePurchase
                                FROM    AuthoritativeBizDocs AB
                                WHERE   AB.numDomainID = @numDomainID )
                        ORDER BY tintType,
                                numPageNavID
                    END
            END
        ELSE 
            BEGIN
				
                SELECT  *
                FROM    ( SELECT    TNA.numPageAuthID,
                                    TNA.numGroupID,
                                    TNA.numTabID,
                                    TNA.numPageNavID,
                                    TNA.bitVisible,
                                    TNA.numDomainID,
                                    ISNULL(PND.vcPageNavName, '') AS [vcNodeName],
                                    tintType
                          FROM      PageNavigationDTL PND
                                    JOIN dbo.TreeNavigationAuthorization TNA ON PND.numPageNavID = TNA.numPageNavID  --AND PND.numTabID = TNA.numTabID       
                          WHERE     1 = 1
                                    --AND ISNULL(TNA.bitVisible, 0) = 1
                                    AND ISNULL(PND.bitVisible, 0) = 1
                                    AND numModuleID = @numModuleID
                                    AND TNA.numDomainID = @numDomainID
                                    AND numGroupID = @numGroupID
                          UNION
                          SELECT    0 numPageAuthID,
                                    @numGroupID numGroupID,
                                    @numTabID numTabID,
                                    1111 numPageNavID,
                                    1 bitVisible,
                                    @numDomainID numDomainID,
                                    'Regular Documents' AS [vcNodeName],
                                    1 tintType
                          UNION
                          SELECT    0 numPageAuthID,
                                    @numGroupID numGroupID,
                                    @numTabID numTabID,
                                    Ld.numListItemId,
                                    1 bitVisible,
                                    @numDomainID numDomainID,
                                    ISNULL(vcData, '') AS [vcNodeName],
                                    1 tintType
                          FROM      listdetails Ld
                                    LEFT JOIN listorder LO ON Ld.numListItemID = LO.numListItemID
                                                              AND Lo.numDomainId = @numDomainID
                          WHERE     Ld.numListID = 29
                                    AND ( constFlag = 1
                                          OR Ld.numDomainID = @numDomainID
                                        )
                          UNION
                          SELECT    0 numPageAuthID,
                                    @numGroupID numGroupID,
                                    @numTabID numTabID,
                                    ld.numListItemID numPageNavID,
                                    1 bitVisible,
                                    @numDomainID numDomainID,
                                    ISNULL(vcData, '') AS [vcNodeName],
                                    1 tintType
                          FROM      listdetails Ld
                                    LEFT JOIN listorder LO ON Ld.numListItemID = LO.numListItemID
                                                              AND lo.numDomainId = @numDomainID
                                    INNER JOIN AuthoritativeBizDocs AB ON ( LD.numListItemID = AB.numAuthoritativeSales )
                          WHERE     LD.numListID = 27
                                    AND ( constFlag = 1
                                          OR LD.numDomainID = @numDomainID
                                        )
                                    AND AB.numDomainID = @numDomainID
                          UNION
                          SELECT    0 numPageAuthID,
                                    @numGroupID numGroupID,
                                    @numTabID numTabID,
                                    ld.numListItemID numPageNavID,
                                    1 bitVisible,
                                    @numDomainID numDomainID,
                                    ISNULL(vcData, '') AS [vcNodeName],
                                    1 tintType
                          FROM      listdetails Ld
                                    LEFT JOIN listorder LO ON Ld.numListItemID = LO.numListItemID
                                                              AND lo.numDomainId = @numDomainID
                                    INNER JOIN AuthoritativeBizDocs AB ON ( ld.numListItemID = AB.numAuthoritativePurchase )
                          WHERE     LD.numListID = 27
                                    AND ( constFlag = 1
                                          OR LD.numDomainID = @numDomainID
                                        )
                                    AND AB.numDomainID = @numDomainID
                          UNION
                          SELECT    0 numPageAuthID,
                                    @numGroupID numGroupID,
                                    @numTabID numTabID,
                                    0 numPageNavID,
                                    1 bitVisible,
                                    @numDomainID numDomainID,
                                    ISNULL(LT.vcData, '') AS [vcNodeName],
                                    1 tintType
                          FROM      ListDetails LT,
                                    listdetails ld
                          WHERE     LT.numListID = 11
                                    AND Lt.numDomainID = @numDomainID
                                    AND LD.numListID = 27
                                    AND ( ld.constFlag = 1
                                          OR LD.numDomainID = @numDomainID
                                        )
                                    AND ( LD.numListItemID IN (
                                          SELECT    AB.numAuthoritativeSales
                                          FROM      AuthoritativeBizDocs AB
                                          WHERE     AB.numDomainId = @numDomainID )
                                          OR ld.numListItemID IN (
                                          SELECT    AB.numAuthoritativePurchase
                                          FROM      AuthoritativeBizDocs AB
                                          WHERE     AB.numDomainId = @numDomainID )
                                        )
                          UNION
                          SELECT    0 numPageAuthID,
                                    @numGroupID numGroupID,
                                    @numTabID numTabID,
                                    2222 numPageNavID,
                                    1 bitVisible,
                                    @numDomainID numDomainID,
                                    'Other BizDocs' AS [vcNodeName],
                                    1 tintType
                          UNION
                          SELECT    0 numPageAuthID,
                                    @numGroupID numGroupID,
                                    @numTabID numTabID,
                                    ld.numListItemID numPageNavID,
                                    1 bitVisible,
                                    @numDomainID numDomainID,
                                    ISNULL(vcData, '') AS [vcNodeName],
                                    1 tintType
                          FROM      listdetails Ld
                          WHERE     ld.numListID = 27
                                    AND ( constFlag = 1
                                          OR LD.numDomainID = @numDomainID
                                        )
                                    AND LD.numListItemID NOT IN (
                                    SELECT  AB.numAuthoritativeSales
                                    FROM    AuthoritativeBizDocs AB
                                    WHERE   AB.numDomainID = @numDomainID )
                                    AND LD.numListItemID NOT IN (
                                    SELECT  AB.numAuthoritativePurchase
                                    FROM    AuthoritativeBizDocs AB
                                    WHERE   AB.numDomainID = @numDomainID )
                          UNION
                          SELECT    0 numPageAuthID,
                                    @numGroupID numGroupID,
                                    @numTabID numTabID,
                                    ls.numListItemID numPageNavID,
                                    1 bitVisible,
                                    @numDomainID numDomainID,
                                    ISNULL(ls.vcData, '') AS [vcNodeName],
                                    1 tintType
                          FROM      listdetails Ld,
                                    ( SELECT    *
                                      FROM      ListDetails LS
                                      WHERE     LS.numDomainID = @numDomainID
                                                AND LS.numListID = 11
                                    ) LS
                          WHERE     ld.numListID = 27
                                    AND ( ld.constFlag = 1
                                          OR LD.numDomainID = @numDomainID
                                        )
                                    AND LD.numListItemID NOT IN (
                                    SELECT  AB.numAuthoritativeSales
                                    FROM    AuthoritativeBizDocs AB
                                    WHERE   AB.numDomainID = @numDomainID
                                    UNION
                                    SELECT  AB.numAuthoritativePurchase
                                    FROM    AuthoritativeBizDocs AB
                                    WHERE   AB.numDomainID = @numDomainID )
                        ) PageNavTable
                        JOIN dbo.TreeNavigationAuthorization TreeNav ON PageNavTable.numPageNavID = TreeNav.numPageNavID
                                                                        AND PageNavTable.numGroupID = TreeNav.numGroupID
                                                                        AND PageNavTable.numTabID = TreeNav.numTabID
                ORDER BY TreeNav.tintType,
                        TreeNav.numPageNavID

            END

        SELECT  *
        FROM    ( SELECT    ISNULL(( SELECT TOP 1
                                            TNA.[numPageAuthID]
                                     FROM   dbo.TreeNavigationAuthorization TNA
                                     WHERE  TNA.numPageNavID = PND.[numPageNavID]
                                            AND TNA.numDomainID = @numDomainID
                                            AND TNA.numGroupID = @numGroupID
                                            AND TNA.numTabID = @numTabID
                                   ), 0) AS [numPageAuthID],
                            ISNULL(@numGroupID, 0) AS [numGroupID],
                            ISNULL(PND.[numTabID], 0) AS [numTabID],
                            ISNULL(PND.[numPageNavID], 0) AS [numPageNavID],
                            ISNULL(( SELECT TOP 1
                                            TNA.[bitVisible]
                                     FROM   dbo.TreeNavigationAuthorization TNA
                                     WHERE  TNA.numPageNavID = PND.[numPageNavID]
                                            AND TNA.numDomainID = @numDomainID
                                            AND TNA.numGroupID = @numGroupID
                                            AND TNA.numTabID = @numTabID
                                   ), 0) AS [bitVisible],
                            @numDomainID AS [numDomainID],
                            ( SELECT TOP 1
                                        vcPageNavName
                              FROM      dbo.PageNavigationDTL
                              WHERE     PageNavigationDTL.numPageNavID = PND.numPageNavID
                            ) AS [vcNodeName],
                            ISNULL(( SELECT TOP 1
                                            TNA.tintType
                                     FROM   dbo.TreeNavigationAuthorization TNA
                                     WHERE  TNA.numPageNavID = PND.[numPageNavID]
                                            AND TNA.numDomainID = @numDomainID
                                            AND TNA.numGroupID = @numGroupID
                                            AND TNA.numTabID = @numTabID
                                   ), 1) AS [tintType]
                  FROM      dbo.PageNavigationDTL PND
                  WHERE     ISNULL(PND.bitVisible, 0) = 1
                            AND PND.numTabID = @numTabID
                            AND numModuleID = @numModuleID
                  UNION ALL
                  SELECT    ISNULL([numPageAuthID], 0) AS [numPageAuthID],
                            @numGroupID AS [numGroupID],
                            @numTabID AS [numTabID],
                            ISNULL(LD.[numListItemID], 0) AS [numPageNavID],
                            ISNULL(TNA.[bitVisible], 0) AS [bitVisible],
                            ISNULL(LD.[numDomainID], 0) AS [numDomainID],
                            vcData AS [vcNodeName],
                            1 AS [tintType]
                  FROM      dbo.ListDetails LD
                            LEFT JOIN dbo.TreeNavigationAuthorization TNA ON LD.numListItemID = TNA.numPageNavID
                  WHERE     numListID = 27
                            AND LD.numDomainID = @numDomainID
                            AND ISNULL(constFlag, 0) = 0
                            AND numTabID = ( CASE WHEN @numModuleID = 10
                                                  THEN 1
                                                  ELSE 0
                                             END )
                  UNION ALL
                  SELECT    ISNULL([numPageAuthID], 0) AS [numPageAuthID],
                            @numGroupID AS [numGroupID],
                            @numTabID AS [numTabID],
                            ISNULL(LD.[numListItemID], 0) AS [numPageNavID],
                            ISNULL(TNA.[bitVisible], 0) AS [bitVisible],
                            ISNULL(TNA.[numDomainID], 0) AS [numDomainID],
                            vcData AS [vcNodeName],
                            1 AS [tintType]
                  FROM      dbo.ListDetails LD
                            LEFT JOIN dbo.TreeNavigationAuthorization TNA ON LD.numListItemID = TNA.numPageNavID
                                                                             AND ISNULL(TNA.[numDomainID], 0) = @numDomainID
                  WHERE     numListID = 27
                            AND ISNULL(constFlag, 0) = 1
                            AND numTabID = @numTabID
                ) TABLE2
        ORDER BY numPageNavID  
    END
--Created By                                                          
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_gettableinfodefault' ) 
    DROP PROCEDURE usp_gettableinfodefault
GO
CREATE PROCEDURE [dbo].[usp_GetTableInfoDefault]                                                                        
@numUserCntID numeric=0,                        
@numRecordID numeric=0,                                    
@numDomainId numeric=0,                                    
@charCoType char(1) = 'a',                        
@pageId as numeric,                        
@numRelCntType as NUMERIC,                        
@numFormID as NUMERIC,                        
@tintPageType TINYINT             
                             
                                    
--                                                           
                                               
AS                                      
                                    
IF (
	SELECT 
		ISNULL(sum(TotalRow),0)  
	FROM
		(            
			Select count(*) TotalRow FROM View_DynamicColumns WHERE numUserCntId=@numuserCntid AND numDomainId = @numDomainId AND numFormID=@numFormID AND numRelCntType=@numRelCntType AND tintPageType=@tintPageType
			Union 
			Select count(*) TotalRow from View_DynamicCustomColumns WHERE numUserCntId=@numuserCntid AND numDomainId = @numDomainId AND numFormID=@numFormID AND numRelCntType=@numRelCntType AND tintPageType=@tintPageType
		) TotalRows
	) <> 0              
BEGIN                           
          
                
if @pageid=1 or @pageid=4 or @pageid= 12 or  @pageid= 13 or  @pageid= 14
 begin              
      
        SELECT numFieldId,ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName
        ,'' as vcURL,vcAssociatedControlType as fld_type,tintRow,tintcolumn as intcoulmn,          
         '' as vcValue,bitCustom AS bitCustomField,vcPropertyName,ISNULL(bitAllowEdit,0) AS bitCanBeUpdated,
         numListID,0 numListItemID,PopupFunctionName,
Case when vcAssociatedControlType='SelectBox' then isnull((Select numPrimaryListID from FieldRelationship where numSecondaryListID=numListID and numDomainID=@numDomainId  ),0) else 0 end as ListRelID, 
Case when vcAssociatedControlType='SelectBox' then (Select count(*) from FieldRelationship where numPrimaryListID=numListID and numDomainID=@numDomainId  ) else 0 end as DependentFields,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
bitFieldMessage,intMaxLength,intMinLength,bitFieldMessage,ISNULL(vcFieldMessage,'') vcFieldMessage
,(SELECT TOP 1 FldDTLID from CFW_FLD_Values where  RecId=@numRecordID and Fld_Id=numFieldId) as FldDTLID
,'' as Value,vcDbColumnName,vcToolTip,intFieldMaxLength
FROM View_DynamicColumns
  where numFormId=@numFormID and numUserCntID=@numUserCntID and  numDomainID=@numDomainID  
  and ISNULL(bitCustom,0)=0 and numRelCntType=@numRelCntType AND tintPageType=@tintPageType AND 
   1=(CASE WHEN @tintPageType=2 THEN CASE WHEN ISNULL(bitAddField,0)=1 THEN 1 ELSE 0 end 
		  WHEN @tintPageType=3 THEN CASE WHEN ISNULL(bitDetailField,0)=1 THEN 1 ELSE 0 END 
		  ELSE 0 END) 
 union          
    
    
    select numFieldId,vcfieldName ,vcURL,vcAssociatedControlType as fld_type,               
   tintRow,tintcolumn as intcoulmn,
 case when vcAssociatedControlType = 'SelectBox' then (select vcData from listdetails where numlistitemid=dbo.GetCustFldValue(numFieldID,@PageId,@numRecordID))                
 else dbo.GetCustFldValue(numFieldID,@PageId,@numRecordID) end as vcValue,                    
 convert(bit,1) bitCustomField,'' vcPropertyName,  
 convert(bit,1) as bitCanBeUpdated,numListID,case when vcAssociatedControlType = 'SelectBox' THEN dbo.GetCustFldValue(numFieldID,@PageId,@numRecordID) ELSE 0 END numListItemID,'' PopupFunctionName,
Case when vcAssociatedControlType='SelectBox' then isnull((Select numPrimaryListID from FieldRelationship where numSecondaryListID=numListID and numDomainID=@numDomainId  ),0) else 0 end as ListRelID ,
Case when vcAssociatedControlType='SelectBox' then (Select count(*) from FieldRelationship where numPrimaryListID=numListID and numDomainID=@numDomainId  ) else 0 end as DependentFields,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,bitFieldMessage,intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage
,isnull((SELECT TOP 1 FldDTLID from CFW_FLD_Values where  RecId=@numRecordID and Fld_Id=numFieldID),0) as FldDTLID
,dbo.GetCustFldValue(numFieldID,@PageId,@numRecordID) as Value,'' AS vcDbColumnName,vcToolTip,0 intFieldMaxLength
from View_DynamicCustomColumns_RelationShip
 where grp_id=@PageId and numRelation=@numRelCntType and numDomainID=@numDomainID and subgrp=0
 and numUserCntID=@numUserCntID 
 AND numFormId=@numFormID and numRelCntType=@numRelCntType AND tintPageType=@tintPageType
      
 order by tintRow,intcoulmn   
 end                
                
if @PageId= 2 or  @PageId= 3 or @PageId= 5 or @PageId= 6 or @PageId= 7 or @PageId= 8   or @PageId= 11                          
 begin            
		IF @numFormID = 123 -- Add/Edit Order - Item Grid Column Settings
		BEGIN
			SELECT numFieldId,ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName
				,'' as vcURL,vcAssociatedControlType as fld_type,tintRow,tintcolumn as intcoulmn,          
					'' as vcValue,ISNULL(bitCustom,0) AS bitCustomField,vcPropertyName,ISNULL(bitAllowEdit,0) AS bitCanBeUpdated,
					numListID,0 numListItemID,PopupFunctionName,
		Case when vcAssociatedControlType='SelectBox' then isnull((Select numPrimaryListID from FieldRelationship where numSecondaryListID=numListID and numDomainID=@numDomainId  ),0) else 0 end as ListRelID, 
		Case when vcAssociatedControlType='SelectBox' then (Select count(*) from FieldRelationship where numPrimaryListID=numListID and numDomainID=@numDomainId  ) else 0 end as DependentFields,
		bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
		bitFieldMessage,intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,vcDbColumnName,vcToolTip,intFieldMaxLength
		FROM View_DynamicColumns
			where numFormId=@numFormID and numUserCntID=@numUserCntID and  numDomainID=@numDomainID  
			and ISNULL(bitCustom,0)=0 and numRelCntType=@numRelCntType AND tintPageType=@tintPageType 

			union          
           
			select numFieldId,vcfieldName ,vcURL,vcAssociatedControlType as fld_type,               
			tintRow,tintcolumn as intcoulmn,
			case when vcAssociatedControlType = 'SelectBox' then (select vcData from listdetails where numlistitemid=dbo.GetCustFldValue(numFieldID,@PageId,@numRecordID))                
			else dbo.GetCustFldValue(numFieldId,@PageId,@numRecordID) end as vcValue,                    
			convert(bit,1) bitCustomField,'' vcPropertyName,  
			convert(bit,1) as bitCanBeUpdated,numListID,case when vcAssociatedControlType = 'SelectBox' THEN dbo.GetCustFldValue(numFieldId,@PageId,@numRecordID) ELSE 0 END numListItemID,'' PopupFunctionName,
		Case when vcAssociatedControlType='SelectBox' then isnull((Select numPrimaryListID from FieldRelationship where numSecondaryListID=numListID and numDomainID=@numDomainId  ),0) else 0 end as ListRelID ,
		Case when vcAssociatedControlType='SelectBox' then (Select count(*) from FieldRelationship where numPrimaryListID=numListID and numDomainID=@numDomainId  ) else 0 end as DependentFields,
		bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,bitFieldMessage,intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,'' AS vcDbColumnName
		,vcToolTip,0 intFieldMaxLength
		from View_DynamicCustomColumns
			where grp_id=@PageId and numDomainID=@numDomainID and subgrp=0
			and numUserCntID=@numUserCntID  
			AND numFormId=@numFormID and ISNULL(bitCustom,0)=1 and numRelCntType=@numRelCntType AND tintPageType=@tintPageType
    
			order by tintrow,intcoulmn      
		END
		ELSE
		BEGIN    
				SELECT numFieldId,ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName
				,'' as vcURL,vcAssociatedControlType as fld_type,tintRow,tintcolumn as intcoulmn,          
					'' as vcValue,ISNULL(bitCustom,0) AS bitCustomField,vcPropertyName,ISNULL(bitAllowEdit,0) AS bitCanBeUpdated,
					numListID,0 numListItemID,PopupFunctionName,
		Case when vcAssociatedControlType='SelectBox' then isnull((Select numPrimaryListID from FieldRelationship where numSecondaryListID=numListID and numDomainID=@numDomainId  ),0) else 0 end as ListRelID, 
		Case when vcAssociatedControlType='SelectBox' then (Select count(*) from FieldRelationship where numPrimaryListID=numListID and numDomainID=@numDomainId  ) else 0 end as DependentFields,
		bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
		bitFieldMessage,intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,vcDbColumnName,vcToolTip,intFieldMaxLength
		FROM View_DynamicColumns
			where numFormId=@numFormID and numUserCntID=@numUserCntID and  numDomainID=@numDomainID  
			and ISNULL(bitCustom,0)=0 and numRelCntType=@numRelCntType AND tintPageType=@tintPageType 
			AND  1=(CASE WHEN @tintPageType=2 THEN CASE WHEN ISNULL(bitAddField,0)=1 THEN 1 ELSE 0 end 
					WHEN @tintPageType=3 THEN CASE WHEN ISNULL(bitDetailField,0)=1 THEN 1 ELSE 0 END 
					ELSE 0 END) 

			union          
           
			select numFieldId,vcfieldName ,vcURL,vcAssociatedControlType as fld_type,               
			tintRow,tintcolumn as intcoulmn,
			case when vcAssociatedControlType = 'SelectBox' then (select vcData from listdetails where numlistitemid=dbo.GetCustFldValue(numFieldID,@PageId,@numRecordID))                
			else dbo.GetCustFldValue(numFieldId,@PageId,@numRecordID) end as vcValue,                    
			convert(bit,1) bitCustomField,'' vcPropertyName,  
			convert(bit,1) as bitCanBeUpdated,numListID,case when vcAssociatedControlType = 'SelectBox' THEN dbo.GetCustFldValue(numFieldId,@PageId,@numRecordID) ELSE 0 END numListItemID,'' PopupFunctionName,
		Case when vcAssociatedControlType='SelectBox' then isnull((Select numPrimaryListID from FieldRelationship where numSecondaryListID=numListID and numDomainID=@numDomainId  ),0) else 0 end as ListRelID ,
		Case when vcAssociatedControlType='SelectBox' then (Select count(*) from FieldRelationship where numPrimaryListID=numListID and numDomainID=@numDomainId  ) else 0 end as DependentFields,
		bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,bitFieldMessage,intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,'' AS vcDbColumnName
		,vcToolTip,0 intFieldMaxLength
		from View_DynamicCustomColumns
			where grp_id=@PageId and numDomainID=@numDomainID and subgrp=0
			and numUserCntID=@numUserCntID  
			AND numFormId=@numFormID and ISNULL(bitCustom,0)=1 and numRelCntType=@numRelCntType AND tintPageType=@tintPageType
    
			order by tintrow,intcoulmn      
	END                          
                             
 end  
               
if @PageId= 0            
  begin         
  SELECT HDR.numFieldId,HDR.vcFieldName,'' as vcURL,'' as fld_type,DTL.tintRow,DTL.intcoulmn,          
  HDR.vcDBColumnName,convert(char(1),DTL.bitCustomField)as bitCustomField,
  0 as bitIsRequired,0 bitIsEmail,0 bitIsAlphaNumeric,0 bitIsNumeric, 0 bitIsLengthValidation,0 bitFieldMessage,0 intMaxLength,0 intMinLength, 0 bitFieldMessage,'' vcFieldMessage
   from PageLayoutdtl DTL                                    
  join pagelayout HDR on DTl.numFieldId= HDR.numFieldId                                    
   where HDR.Ctype = @charCoType and numUserCntId=@numUserCntId  and bitCustomField=0 and            
  numDomainId = @numDomainId and DTL.numRelCntType=@numRelCntType   order by DTL.tintrow,intcoulmn          
  end                
end                 



ELSE IF @tintPageType=2 and @numRelCntType>3 and 
((select isnull(sum(TotalRow),0) from (Select count(*) TotalRow from View_DynamicColumns where numFormId=@numFormID and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=@tintPageType AND numFormID IN(36) AND numRelCntType=2 
Union 
Select count(*) TotalRow from View_DynamicCustomColumns where numFormId=@numFormID and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=@tintPageType AND numFormID IN(36) AND numRelCntType=2) TotalRows
) <> 0 )
BEGIN
	exec usp_GetTableInfoDefault @numUserCntID=@numUserCntID,@numRecordID=@numRecordID,@numDomainId=@numDomainId,@charCoType=@charCoType,@pageId=@pageId,
			@numRelCntType=2,@numFormID=@numFormID,@tintPageType=@tintPageType
END
                           
ELSE IF @charCoType<>'b'/*added by kamal to prevent showing of all  fields on checkout by default*/                  
BEGIN                 

	DECLARE @IsMasterConfAvailable BIT = 0
	DECLARE @numUserGroup NUMERIC(18,0)                                    

	SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID   
       
	IF @pageid=1 or @pageid=4 or @pageid= 12 or  @pageid= 13 or  @pageid= 14                
	BEGIN   
		--Check if Master Form Configuration is created by administrator if NoColumns=0

		IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = @numFormID AND numRelCntType=@numRelCntType AND bitGridConfiguration = 0 AND numGroupID = @numUserGroup) > 0
	BEGIN
		SET @IsMasterConfAvailable = 1
	END

		--If MasterConfiguration is available then load it otherwise load default columns
		IF @IsMasterConfAvailable = 1
		BEGIN
			SELECT 
				numFieldId,ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName
				,'' as vcURL,vcAssociatedControlType as fld_type,tintRow,tintcolumn as intcoulmn,          
				 '' as vcValue,convert(bit,0) AS bitCustomField,vcPropertyName,ISNULL(bitAllowEdit,0) AS bitCanBeUpdated,
				 0 Tabletype,numListID,0 numListItemID,PopupFunctionName,
				Case when vcAssociatedControlType='SelectBox' then isnull((Select numPrimaryListID from FieldRelationship where numSecondaryListID=numListID and numDomainID=@numDomainId  ),0) else 0 end as ListRelID, 
				Case when vcAssociatedControlType='SelectBox' then (Select count(*) from FieldRelationship where numPrimaryListID=numListID and numDomainID=@numDomainId  ) else 0 end as DependentFields,
				bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
				bitFieldMessage,intMaxLength,intMinLength,bitFieldMessage,ISNULL(vcFieldMessage,'') vcFieldMessage
				,vcDbColumnName,vcToolTip,intFieldMaxLength
			FROM 
				View_DynamicColumnsMasterConfig
			WHERE
				View_DynamicColumnsMasterConfig.numFormId=@numFormID AND 
				View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
				View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
				View_DynamicColumnsMasterConfig.numRelCntType=@numRelCntType AND 
				View_DynamicColumnsMasterConfig.bitGridConfiguration = 0 AND
				ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
			UNION
			SELECT 
				numFieldId as numFieldId,vcFieldName as vcfieldName ,vcURL,vcFieldType,               
				convert(tinyint,0) as tintrow,convert(int,0) as intcoulmn,  
				case when vcFieldType = 'SelectBox' then (select vcData from listdetails where numlistitemid=dbo.GetCustFldValue(numFieldId,@PageId,@numRecordID))                
				 else dbo.GetCustFldValue(numFieldId,@PageId,@numRecordID) end as vcValue,                    
				 convert(bit,1) bitCustomField,'' vcPropertyName,  
				 convert(bit,1) as bitCanBeUpdated,1 Tabletype,numListID,case when vcFieldType = 'SelectBox' THEN dbo.GetCustFldValue(numFieldId,@PageId,@numRecordID) ELSE 0 END numListItemID,'' PopupFunctionName,
				Case when vcFieldType='SelectBox' then isnull((Select numPrimaryListID from FieldRelationship where numSecondaryListID=numListID and numDomainID=@numDomainId  ),0) else 0 end as ListRelID ,
				Case when vcFieldType='SelectBox' then (Select count(*) from FieldRelationship where numPrimaryListID=numListID and numDomainID=@numDomainId  ) else 0 end as DependentFields,
				ISNULL(bitIsRequired,0) bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,bitFieldMessage,intMaxLength,intMinLength,bitFieldMessage,ISNULL(vcFieldMessage,'') vcFieldMessage
				,'' AS vcDbColumnName,vcToolTip,0 intFieldMaxLength
			FROM 
				View_DynamicCustomColumnsMasterConfig
			WHERE 
				View_DynamicCustomColumnsMasterConfig.numFormId=@numFormID AND 
				View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
				View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
				View_DynamicCustomColumnsMasterConfig.numRelCntType=@numRelCntType AND 
				View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 0 AND
				ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1
			ORDER BY 
				Tabletype,tintrow,intcoulmn  
		END
		ELSE                                        
		BEGIN
			SELECT numFieldId,ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName
        ,'' as vcURL,vcAssociatedControlType as fld_type,tintRow,tintcolumn as intcoulmn,          
         '' as vcValue,convert(bit,0) AS bitCustomField,vcPropertyName,ISNULL(bitAllowEdit,0) AS bitCanBeUpdated,
         0 Tabletype,numListID,0 numListItemID,PopupFunctionName,
Case when vcAssociatedControlType='SelectBox' then isnull((Select numPrimaryListID from FieldRelationship where numSecondaryListID=numListID and numDomainID=@numDomainId  ),0) else 0 end as ListRelID, 
Case when vcAssociatedControlType='SelectBox' then (Select count(*) from FieldRelationship where numPrimaryListID=numListID and numDomainID=@numDomainId  ) else 0 end as DependentFields,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
bitFieldMessage,intMaxLength,intMinLength,bitFieldMessage,ISNULL(vcFieldMessage,'') vcFieldMessage
,vcDbColumnName,vcToolTip,intFieldMaxLength
FROM View_DynamicDefaultColumns 
  where numFormId=@numFormID AND numDomainID=@numDomainID AND
   1=(CASE WHEN @tintPageType=2 THEN CASE WHEN ISNULL(bitAddField,0)=1 THEN 1 ELSE 0 end 
		  WHEN @tintPageType=3 THEN CASE WHEN ISNULL(bitDetailField,0)=1 THEN 1 ELSE 0 END 
		  ELSE 0 END) 
                   
   union     
  
     select fld_id as numFieldId,fld_label as vcfieldName ,vcURL,fld_type,               
   convert(tinyint,0) as tintrow,convert(int,0) as intcoulmn,  
 case when fld_type = 'SelectBox' then (select vcData from listdetails where numlistitemid=dbo.GetCustFldValue(fld_id,@PageId,@numRecordID))                
 else dbo.GetCustFldValue(fld_id,@PageId,@numRecordID) end as vcValue,                    
 convert(bit,1) bitCustomField,'' vcPropertyName,  
 convert(bit,1) as bitCanBeUpdated,1 Tabletype,CFM.numListID,case when fld_type = 'SelectBox' THEN dbo.GetCustFldValue(fld_id,@PageId,@numRecordID) ELSE 0 END numListItemID,'' PopupFunctionName,
Case when fld_type='SelectBox' then isnull((Select numPrimaryListID from FieldRelationship where numSecondaryListID=CFM.numListID and numDomainID=@numDomainId  ),0) else 0 end as ListRelID ,
Case when fld_type='SelectBox' then (Select count(*) from FieldRelationship where numPrimaryListID=CFM.numListID and numDomainID=@numDomainId  ) else 0 end as DependentFields,
ISNULL(V.bitIsRequired,0) bitIsRequired,V.bitIsEmail,V.bitIsAlphaNumeric,V.bitIsNumeric,V.bitIsLengthValidation,V.bitFieldMessage,V.intMaxLength,V.intMinLength,V.bitFieldMessage,ISNULL(V.vcFieldMessage,'') vcFieldMessage
,'' AS vcDbColumnName,CFM.vcToolTip,0 intFieldMaxLength
from CFW_Fld_Master CFM join CFW_Fld_Dtl CFD on Fld_id=numFieldId                                          
 left join CFw_Grp_Master CFG on subgrp=CFG.Grp_id                                          
 LEFT JOIN CFW_Validation V ON V.numFieldID = CFM.Fld_id
 where CFM.grp_id=@PageId and CFD.numRelation=@numRelCntType and CFM.numDomainID=@numDomainID and subgrp=0

 order by Tabletype,tintrow,intcoulmn                        
		END     
	END      
             
	IF @PageId= 2 or  @PageId= 3 or @PageId= 5 or @PageId= 6 or @PageId= 7 or @PageId= 8  or @PageId= 11                          
	BEGIN  
		--Check if Master Form Configuration is created by administrator if NoColumns=0

		IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = @numFormID AND numRelCntType=@numRelCntType AND bitGridConfiguration = 0 AND numGroupID = @numUserGroup) > 0
		BEGIN
			SET @IsMasterConfAvailable = 1
		END

		--If MasterConfiguration is available then load it otherwise load default columns
		IF @IsMasterConfAvailable = 1
		BEGIN
			SELECT 
				numFieldId,ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName
				,'' as vcURL,vcAssociatedControlType as fld_type,tintRow,tintcolumn as intcoulmn,          
				 '' as vcValue,convert(bit,0) AS bitCustomField,vcPropertyName,ISNULL(bitAllowEdit,0) AS bitCanBeUpdated,
				 0 Tabletype,numListID,0 numListItemID,PopupFunctionName,
				Case when vcAssociatedControlType='SelectBox' then isnull((Select numPrimaryListID from FieldRelationship where numSecondaryListID=numListID and numDomainID=@numDomainId  ),0) else 0 end as ListRelID, 
				Case when vcAssociatedControlType='SelectBox' then (Select count(*) from FieldRelationship where numPrimaryListID=numListID and numDomainID=@numDomainId  ) else 0 end as DependentFields,
				bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
				bitFieldMessage,intMaxLength,intMinLength,bitFieldMessage,ISNULL(vcFieldMessage,'') vcFieldMessage
				,vcDbColumnName,vcToolTip,intFieldMaxLength
			FROM 
				View_DynamicColumnsMasterConfig
			WHERE
				View_DynamicColumnsMasterConfig.numFormId=@numFormID AND 
				View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
				View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
				View_DynamicColumnsMasterConfig.numRelCntType=@numRelCntType AND 
				View_DynamicColumnsMasterConfig.bitGridConfiguration = 0 AND
				ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
			UNION
			SELECT 
				numFieldId as numFieldId,vcFieldName as vcfieldName ,vcURL,vcFieldType,               
				convert(tinyint,0) as tintrow,convert(int,0) as intcoulmn,  
				case when vcFieldType = 'SelectBox' then (select vcData from listdetails where numlistitemid=dbo.GetCustFldValue(numFieldId,@PageId,@numRecordID))                
				 else dbo.GetCustFldValue(numFieldId,@PageId,@numRecordID) end as vcValue,                    
				 convert(bit,1) bitCustomField,'' vcPropertyName,  
				 convert(bit,1) as bitCanBeUpdated,1 Tabletype,numListID,case when vcFieldType = 'SelectBox' THEN dbo.GetCustFldValue(numFieldId,@PageId,@numRecordID) ELSE 0 END numListItemID,'' PopupFunctionName,
				Case when vcFieldType='SelectBox' then isnull((Select numPrimaryListID from FieldRelationship where numSecondaryListID=numListID and numDomainID=@numDomainId  ),0) else 0 end as ListRelID ,
				Case when vcFieldType='SelectBox' then (Select count(*) from FieldRelationship where numPrimaryListID=numListID and numDomainID=@numDomainId  ) else 0 end as DependentFields,
				ISNULL(bitIsRequired,0) bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,bitFieldMessage,intMaxLength,intMinLength,bitFieldMessage,ISNULL(vcFieldMessage,'') vcFieldMessage
				,'' AS vcDbColumnName,vcToolTip,0 intFieldMaxLength
			FROM 
				View_DynamicCustomColumnsMasterConfig
			WHERE 
				View_DynamicCustomColumnsMasterConfig.numFormId=@numFormID AND 
				View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
				View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
				View_DynamicCustomColumnsMasterConfig.numRelCntType=@numRelCntType AND 
				View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 0 AND
				ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1
			ORDER BY 
				Tabletype,tintrow,intcoulmn  
		END
		ELSE                                        
		BEGIN
			SELECT numFieldId,ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName
			,'' as vcURL,vcAssociatedControlType as fld_type,tintRow,tintColumn as intcoulmn,          
			 '' as vcValue,convert(bit,0) AS bitCustomField,vcPropertyName,ISNULL(bitAllowEdit,0) AS bitCanBeUpdated,
			 0 Tabletype,numListID,0 numListItemID,PopupFunctionName,
			Case when vcAssociatedControlType='SelectBox' then isnull((Select numPrimaryListID from FieldRelationship where numSecondaryListID=numListID and numDomainID=@numDomainId  ),0) else 0 end as ListRelID, 
			Case when vcAssociatedControlType='SelectBox' then (Select count(*) from FieldRelationship where numPrimaryListID=numListID and numDomainID=@numDomainId  ) else 0 end as DependentFields,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			bitFieldMessage,intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage
			,vcDbColumnName,vcToolTip,intFieldMaxLength
			FROM View_DynamicDefaultColumns DFV
			where numFormId=@numFormID AND numDomainID=@numDomainID AND
			1=(CASE WHEN @tintPageType=2 THEN CASE WHEN ISNULL(bitAddField,0)=1 THEN 1 ELSE 0 end 
			  WHEN @tintPageType=3 THEN CASE WHEN ISNULL(bitDetailField,0)=1 THEN 1 ELSE 0 END 
			  ELSE 0 END)   
			UNION 
			select fld_id as numFieldId,fld_label as vcfieldName ,vcURL,fld_type,               
			convert(tinyint,0) as tintRow,convert(int,0) as intcoulmn,
			case when fld_type = 'SelectBox' then (select vcData from listdetails where numlistitemid=dbo.GetCustFldValue(fld_id,@PageId,@numRecordID))                
			else dbo.GetCustFldValue(fld_id,@PageId,@numRecordID) end as vcValue,                    
			convert(bit,1) bitCustomField,'' vcPropertyName,  
			convert(bit,1) as bitCanBeUpdated,1 Tabletype,CFM.numListID,case when fld_type = 'SelectBox' THEN dbo.GetCustFldValue(fld_id,@PageId,@numRecordID) ELSE 0 END numListItemID,'' PopupFunctionName,
			Case when fld_type='SelectBox' then isnull((Select numPrimaryListID from FieldRelationship where numSecondaryListID=CFM.numListID and numDomainID=@numDomainId  ),0) else 0 end as ListRelID ,
			Case when fld_type='SelectBox' then (Select count(*) from FieldRelationship where numPrimaryListID=CFM.numListID and numDomainID=@numDomainId  ) else 0 end as DependentFields,
			ISNULL(V.bitIsRequired,0) bitIsRequired,V.bitIsEmail,V.bitIsAlphaNumeric,V.bitIsNumeric,V.bitIsLengthValidation,V.bitFieldMessage,V.intMaxLength,V.intMinLength,V.bitFieldMessage,ISNULL(V.vcFieldMessage,'') vcFieldMessage,'' AS vcDbColumnName
			,CFM.vcToolTip,0 intFieldMaxLength
			FROM CFW_Fld_Master CFM left join CFW_Fld_Dtl CFD on Fld_id=numFieldId  
			left join CFw_Grp_Master CFG on subgrp=CFG.Grp_id                                    
			LEFT JOIN CFW_Validation V ON V.numFieldID = CFM.Fld_id
			WHERE CFM.grp_id=@PageId and CFM.numDomainID=@numDomainID   and subgrp=0           
			ORDER BY Tabletype,tintrow,intcoulmn                             
		END
	END   
	      
	IF @PageId= 0            
	BEGIN         
	   SELECT numFieldID,vcFieldName,'' as vcURL,'' as fld_type,vcDBColumnName,tintRow,intcolumn as intcoulmn,          
		'0' as bitCustomField,Ctype,'0' as tabletype,
		0 as bitIsRequired,0 bitIsEmail,0 bitIsAlphaNumeric,0 bitIsNumeric, 0 bitIsLengthValidation,0 bitFieldMessage,0 intMaxLength,0 intMinLength, 0 bitFieldMessage,'' vcFieldMessage
		from PageLayout where Ctype = @charCoType    order by tintrow,intcoulmn           
	END                       
END  
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetTreeNodesForRelationship' ) 
    DROP PROCEDURE USP_GetTreeNodesForRelationship
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- EXEC USP_GetPageNavigationAuthorizationDetails 1,1
CREATE PROCEDURE USP_GetTreeNodesForRelationship
    (
	  @numGroupID NUMERIC(18,0),
	  @numTabID NUMERIC(18,0),
      @numDomainID NUMERIC(18, 0),
	  @bitVisibleOnly bit
    )
AS 
    SET NOCOUNT ON
    SET TRANSACTION ISOLATION LEVEL READ COMMITTED
  
    BEGIN		
        DECLARE @numModuleID AS NUMERIC(18, 0)

        SELECT TOP 1
                @numModuleID = numModuleID
        FROM    dbo.PageNavigationDTL
        WHERE   numTabID = @numTabID 
 

SELECT
	ROW_NUMBER() OVER (ORDER BY numOrder) AS ID,
	*
INTO
	#RelationshipTree 
FROM
	(
	SELECT    
		PND.numPageNavID AS numPageNavID,
		NULL AS numListItemID,
		ISNULL(TNA.bitVisible,1) AS bitVisible,
		ISNULL(PND.vcPageNavName, '') AS [vcNodeName],
		ISNULL(vcNavURL, '') AS vcNavURL,
        vcImageURL,
		ISNULL(TNA.tintType,1) AS tintType,
		PND.numParentID AS numParentID,
		PND.numParentID AS numOriginalParentID,
		ISNULL(TreeNodeOrder.numOrder,1000) AS numOrder,
		0 AS isUpdated
	FROM      
		PageNavigationDTL PND
	LEFT JOIN 
		dbo.TreeNavigationAuthorization TNA 
	ON 
		TNA.numPageNavID = PND.numPageNavID AND
		TNA.numDomainID = @numDomainID AND 
		TNA.numGroupID = @numGroupID
	LEFT JOIN
		dbo.TreeNodeOrder 
	ON
		dbo.TreeNodeOrder.numTabID = @numTabID AND
		dbo.TreeNodeOrder.numDomainID = @numDomainID AND
		dbo.TreeNodeOrder.numGroupID = @numGroupID AND
		dbo.TreeNodeOrder.numPageNavID = PND.numPageNavID
	WHERE     
		1 = 1
		AND ISNULL(PND.bitVisible, 0) = 1
		AND numModuleID = @numModuleID
	UNION
	SELECT
		NULL AS numPageNavID,
		ListDetails.numListItemID,
		ISNULL(dbo.TreeNodeOrder.bitVisible,1) AS bitVisible,
		ListDetails.vcData AS [vcNodeName],
		'../prospects/frmCompanyList.aspx?RelId='+ convert(varchar(10),ListDetails.numListItemID) vcNavURL,
		NULL as vcImageURL,
		1 AS tintType,
		6 AS numParentID,
		6 AS numOriginalParentID,
		ISNULL(TreeNodeOrder.numOrder,2000) AS numOrder,
		0 AS isUpdated
	FROM
		ListDetails
	LEFT JOIN 
		dbo.TreeNodeOrder 
	ON
		dbo.TreeNodeOrder.numListItemID = ListDetails.numListItemID AND
		dbo.TreeNodeOrder.numParentID = 6 AND
		dbo.TreeNodeOrder.numTabID = @numTabID AND
		dbo.TreeNodeOrder.numDomainID = @numDomainID AND
		dbo.TreeNodeOrder.numGroupID = @numGroupID AND
		dbo.TreeNodeOrder.numPageNavID IS NULL
	WHERE
		numListID = 5 AND
		ListDetails.numDomainID = @numDomainID AND
		(ISNULL(bitDelete,0) = 0 OR constFlag = 1) AND
		ListDetails.numListItemID <> 46 
	UNION       
	SELECT 
		NULL AS numPageNavID,
		L2.numListItemID,   
		ISNULL(dbo.TreeNodeOrder.bitVisible,1) AS bitVisible,
		L2.vcData AS [vcNodeName],
		'../prospects/frmCompanyList.aspx?RelId=' 
		+ convert(varchar(10), FRDTL.numPrimaryListItemID) + '&profileid='
        + convert(varchar(10), numSecondaryListItemID) as vcNavURL,
        NULL as vcImageURL,
		1 AS tintType,
		FRDTL.numPrimaryListItemID AS numParentID,
		FRDTL.numPrimaryListItemID AS numOriginalParentID,
		ISNULL(TreeNodeOrder.numOrder,3000) AS numOrder,
		0 AS isUpdated
	FROM      
		FieldRelationship FR
    join 
		FieldRelationshipDTL FRDTL on FRDTL.numFieldRelID = FR.numFieldRelID
    join 
		ListDetails L1 on numPrimaryListItemID = L1.numListItemID
    join 
		ListDetails L2 on numSecondaryListItemID = L2.numListItemID
	LEFT JOIN 
		dbo.TreeNodeOrder 
	ON
		dbo.TreeNodeOrder.numListItemID = L2.numListItemID AND
		dbo.TreeNodeOrder.numParentID = FRDTL.numPrimaryListItemID AND
		dbo.TreeNodeOrder.numTabID = @numTabID AND
		dbo.TreeNodeOrder.numDomainID = @numDomainID AND
		dbo.TreeNodeOrder.numGroupID = @numGroupID AND
		dbo.TreeNodeOrder.numPageNavID IS NULL
	WHERE     
		numPrimaryListItemID <> 46
        and FR.numPrimaryListID = 5
		and FR.numSecondaryListID = 21
		and FR.numDomainID = @numDomainID
        and L2.numDomainID = @numDomainID
	UNION
	SELECT 
		NULL AS numPageNavID,
		L2.numListItemID,
		ISNULL(dbo.TreeNodeOrder.bitVisible,1) AS bitVisible,
		L2.vcData AS [vcNodeName],
		'../prospects/frmProspectList.aspx?numProfile=' + CONVERT(VARCHAR(10), L2.numListItemID) AS vcNavURL,
        NULL AS vcImageURL,
		1 AS tintType,
		11 AS numParentID,
		11 AS numOriginalParentID,
		ISNULL(TreeNodeOrder.numOrder,4000) AS numOrder,
		0 AS isUpdated
	FROM    
		FieldRelationship AS FR
	INNER JOIN 
		FieldRelationshipDTL AS FRD ON FRD.numFieldRelID = FR.numFieldRelID
	INNER JOIN 
		ListDetails AS L1 ON L1.numListItemID = FRD.numPrimaryListItemID
	INNER JOIN 
		ListDetails AS L2 ON L2.numListItemID = FRD.numSecondaryListItemID
	LEFT JOIN 
		dbo.TreeNodeOrder 
	ON
		dbo.TreeNodeOrder.numListItemID = L2.numListItemID AND
		dbo.TreeNodeOrder.numParentID = 11 AND
		dbo.TreeNodeOrder.numTabID = @numTabID AND
		dbo.TreeNodeOrder.numDomainID = @numDomainID AND
		dbo.TreeNodeOrder.numGroupID = @numGroupID AND
		dbo.TreeNodeOrder.numPageNavID IS NULL
	WHERE   ( FRD.numPrimaryListItemID = 46 )
			AND ( FR.numDomainID = @numDomainID )
	UNION
	SELECT 
		NULL AS numPageNavID,
		L2.numListItemID,
		ISNULL(dbo.TreeNodeOrder.bitVisible,1) AS bitVisible,
		L2.vcData AS [vcNodeName],
		'../account/frmAccountList.aspx?numProfile=' + CONVERT(VARCHAR(10), L2.numListItemID) AS vcNavURL,
        NULL AS vcImageURL,
		1 AS tintType,
		12 AS numParentID,
		12 AS numOriginalParentID,
		ISNULL(TreeNodeOrder.numOrder,5000) AS numOrder,
		0 AS isUpdated
	FROM    
		FieldRelationship AS FR
	INNER JOIN 
		FieldRelationshipDTL AS FRD ON FRD.numFieldRelID = FR.numFieldRelID
	INNER JOIN 
		ListDetails AS L1 ON L1.numListItemID = FRD.numPrimaryListItemID
	INNER JOIN 
		ListDetails AS L2 ON L2.numListItemID = FRD.numSecondaryListItemID
	LEFT JOIN 
		dbo.TreeNodeOrder 
	ON
		dbo.TreeNodeOrder.numListItemID = L2.numListItemID AND
		dbo.TreeNodeOrder.numParentID = 12 AND
		dbo.TreeNodeOrder.numTabID = @numTabID AND
		dbo.TreeNodeOrder.numDomainID = @numDomainID AND
		dbo.TreeNodeOrder.numGroupID = @numGroupID AND
		dbo.TreeNodeOrder.numPageNavID IS NULL
	WHERE   ( FRD.numPrimaryListItemID = 46 )
			AND ( FR.numDomainID = @numDomainID )
	UNION    
	--Select Contact
	SELECT 
		NULL AS numPageNavID,
		listdetails.numListItemID,
		ISNULL(dbo.TreeNodeOrder.bitVisible,1) AS bitVisible,
		listdetails.vcData AS [vcNodeName],
		'../contact/frmContactList.aspx?ContactType=' + CONVERT(VARCHAR(10), listdetails.numListItemID) AS vcNavURL,
        NULL AS vcImageURL,
		1 AS tintType,
		13 AS numParentID,
		13 AS numOriginalParentID,
		ISNULL(TreeNodeOrder.numOrder,6000) AS numOrder,
		0 AS isUpdated
	FROM    
		listdetails
	LEFT JOIN 
		dbo.TreeNodeOrder 
	ON
		dbo.TreeNodeOrder.numListItemID = listdetails.numListItemID AND
		dbo.TreeNodeOrder.numParentID = 13 AND
		dbo.TreeNodeOrder.numTabID = @numTabID AND
		dbo.TreeNodeOrder.numDomainID = @numDomainID AND
		dbo.TreeNodeOrder.numGroupID = @numGroupID AND
		dbo.TreeNodeOrder.numPageNavID IS NULL
	WHERE   numListID = 8
			AND ( constFlag = 1
				  OR listdetails.numDomainID = @numDomainID
				)
	UNION
	SELECT 
		NULL AS numPageNavID,
		101 AS numListItemID,
		ISNULL((
					SELECT 
						bitVisible 
					FROM 
						dbo.TreeNodeOrder 
					WHERE
						dbo.TreeNodeOrder.numListItemID = 101 AND
						dbo.TreeNodeOrder.numParentID = 13 AND
						dbo.TreeNodeOrder.numTabID = @numTabID AND
						dbo.TreeNodeOrder.numDomainID = @numDomainID AND
						dbo.TreeNodeOrder.numGroupID = @numGroupID AND
						dbo.TreeNodeOrder.numPageNavID IS NULL
				),1) AS bitVisible,
		'Primary Contact' AS [vcNodeName],
		'../Contact/frmcontactList.aspx?ContactType=101',
        NULL AS vcImageURL,
		1 AS tintType,
		13 AS numParentID,
		13 AS numOriginalParentID,
		ISNULL((
					SELECT 
						numOrder 
					FROM 
						dbo.TreeNodeOrder 
					WHERE
						dbo.TreeNodeOrder.numListItemID = 101 AND
						dbo.TreeNodeOrder.numParentID = 13 AND
						dbo.TreeNodeOrder.numTabID = @numTabID AND
						dbo.TreeNodeOrder.numDomainID = @numDomainID AND
						dbo.TreeNodeOrder.numGroupID = @numGroupID AND
						dbo.TreeNodeOrder.numPageNavID IS NULL
				),7000) AS numOrder,
		 0 AS isUpdated
	) AS TEMP
	ORDER BY
		TEMP.numOrder

	DECLARE @i AS INT = 1
	DECLARE @Count AS INT
	DECLARE @oldParentID AS INT
	SELECT @COUNT=COUNT(ID) FROM #RelationshipTree

	WHILE @i <= @COUNT
	BEGIN
		IF (SELECT ISNULL(numPageNavID,0) FROM #RelationshipTree WHERE ID = @i) <> 0
			SELECT @oldParentID = numPageNavID FROM #RelationshipTree WHERE ID = @i
		ELSE
			SELECT @oldParentID = numListItemID FROM #RelationshipTree WHERE ID = @i


		UPDATE #RelationshipTree SET numParentID = @i, isUpdated=1 WHERE numParentID = @oldParentID AND isUpdated = 0

		SET @i = @i + 1
	END


	IF @bitVisibleOnly = 1
		SELECT 
			CAST(ID AS INT) As ID,
			numPageNavID,
			numListItemID,
			bitVisible,
			vcNodeName, 
			vcNavURL, 
			vcImageURL,
			tintType,
			(CASE numParentID WHEN 0 THEN NULL ELSE CAST(numParentID AS INT) END) AS numParentID,
			numOriginalParentID, 
			numOrder 
		FROM
			#RelationshipTree 
		WHERE 
			bitVisible = 1
			AND (numParentID IN (SELECT ID FROM #RelationshipTree WHERE bitVisible = 1) OR numParentID = 0)
	ELSE
		SELECT CAST(ID AS INT) As ID,numPageNavID,numListItemID,bitVisible,vcNodeName,tintType,(CASE numParentID WHEN 0 THEN NULL ELSE CAST(numParentID AS INT) END) AS numParentID,numOriginalParentID, numOrder FROM #RelationshipTree
	
	DROP TABLE #RelationshipTree
      
    END
/****** Object:  StoredProcedure [dbo].[USP_GetVendors]    Script Date: 07/26/2008 16:18:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getvendors')
DROP PROCEDURE usp_getvendors
GO
CREATE PROCEDURE [dbo].[USP_GetVendors]         
@numDomainID as numeric(9)=0,    
@numItemCode as numeric(9)=0       
as        
select div.numCompanyID,Vendor.numItemCode,Vendor.numVendorID,vcPartNo,vcCompanyName as Vendor,        
vcFirstName+ ' '+vcLastName as ConName,        
convert(varchar(15),numPhone)+ ', '+ convert(varchar(15),numPhoneExtension) as Phone,ISNULL(monCost,0) monCost,vcItemName,ISNULL([intMinQty],0) intMinQty
,numContactId, intLeadTimeDays
from Vendor          
join divisionMaster div        
on div.numdivisionid=numVendorid        
join companyInfo com        
on com.numCompanyID=div.numCompanyID        
left join AdditionalContactsInformation ADC        
on ADC.numdivisionID=Div.numdivisionID   
join Item I  
on I.numItemCode= Vendor.numItemCode      
WHERE ISNULL(ADC.bitPrimaryContact,0)=1 and Vendor.numDomainID=@numDomainID and Vendor.numItemCode= @numItemCode
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER  ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetWFRecordData')
DROP PROCEDURE USP_GetWFRecordData
GO

Create PROCEDURE [dbo].[USP_GetWFRecordData]     
    @numDomainID numeric(18, 0),
    @numRecordID numeric(18, 0),
    @numFormID numeric(18, 0)
as                 

DECLARE @numContactId AS NUMERIC(18),@numRecOwner AS NUMERIC(18),@numAssignedTo AS NUMERIC(18),@numDivisionId AS NUMERIC(18), @txtSignature VARCHAR(8000),@vcDateFormat VARCHAR(30),@numInternalPM numeric(18,0),@numExternalPM numeric(18,0),@numNextAssignedTo numeric(18,0),@numPhone varchar(15),@numOppIDPC numeric(18,0),@numStageIDPC numeric(18,0),@numOppBizDOCIDPC numeric(18,0)

IF @numFormID=70 --Opportunities & Orders
BEGIN
	SELECT @numContactId=ISNULL(numContactId,0),@numRecOwner=ISNULL(numRecOwner,0),
		   @numAssignedTo=ISNULL(numAssignedTo,0),@numDivisionId=ISNULL(numDivisionId,0)
	FROM dbo.OpportunityMaster WHERE numDomainId=@numDomainID AND numOppId=@numRecordID

	SELECT @numStageIDPC=ISNULL(s.numStageDetailsId,0)
    FROM OpportunityMaster o left join StagePercentageDetails s on s.numOppID=o.numOppId
	WHERE o.numDomainId=@numDomainID AND o.numOppId=@numRecordID

	SELECT @numOppBizDOCIDPC=ISNULL(s.numOppBizDocsId,0)
    FROM OpportunityMaster o left join OpportunityBizDocs s on s.numOppID=o.numOppId
	WHERE o.numDomainId=@numDomainID AND o.numOppId=@numRecordID
END
IF @numFormID=49 --BizDocs
BEGIN
	SELECT @numContactId=ISNULL(om.numContactId,0),@numRecOwner=ISNULL(om.numRecOwner,0),@numOppIDPC=ISNULL(om.numOppId,0),
		   @numAssignedTo=ISNULL(om.numAssignedTo,0),@numDivisionId=ISNULL(om.numDivisionId,0),@vcDateFormat=D.vcDateFormat
	FROM  dbo.OpportunityBizDocs AS  obd 
	INNER JOIN dbo.OpportunityMaster AS OM 
	on obd.numOppId=om.numOppId 
	INNER JOIN  dbo.Domain AS d ON d.numDomainId=om.numDomainId
	WHERE om.numDomainId=@numDomainID 
		AND obd.numOppBizDocsId=@numRecordID

		
END
IF @numFormID=94 --Sales/Purchase/Project
BEGIN
declare @numProjectID numeric(18,0)
declare @numOppId numeric(18,0)
select  @numProjectID=ISNULL(numProjectID,0),@numOppId=Isnull(numOppId,0) from StagePercentageDetails where numStageDetailsId=@numRecordID

--@numInternalPM numeric(18,0),@numExternalPM numeric(18,0),@numNextAssignedTo numeric(18,0)
If(@numProjectID=0)--Order
Begin
	SELECT @numContactId=ISNULL(om.numContactId,0),@numRecOwner=ISNULL(om.numRecOwner,0),
		   @numAssignedTo=ISNULL(obd.numAssignTo,0),@numDivisionId=ISNULL(om.numDivisionId,0),@vcDateFormat=D.vcDateFormat
	FROM  dbo.StagePercentageDetails AS  obd 
	INNER JOIN dbo.OpportunityMaster AS OM 
	on obd.numOppId=om.numOppId 
	INNER JOIN  dbo.Domain AS d ON d.numDomainId=om.numDomainId
	WHERE om.numDomainId=@numDomainID 
	AND obd.numStageDetailsId=@numRecordID
	End
	Else
	Begin
	
	SELECT @numNextAssignedTo=numAssignTo  from StagePercentageDetails where numStageDetailsId=@numRecordID+1

	SELECT @numContactId=ISNULL(om.numIntPrjMgr,0),@numRecOwner=ISNULL(om.numRecOwner,0),
		   @numAssignedTo=ISNULL(obd.numAssignTo,0),@numDivisionId=ISNULL(om.numDivisionId,0),@numInternalPM=ISNULL(om.numIntPrjMgr,0),@numExternalPM=ISNULL(om.numCustPrjMgr,0)
	FROM  dbo.StagePercentageDetails AS  obd 
	INNER JOIN dbo.ProjectsMaster AS OM 
	on obd.numProjectID=om.numProId 	
	WHERE om.numDomainId=@numDomainID 
	AND obd.numStageDetailsId=@numRecordID

	END
END
IF @numFormID=68 --Organization
BEGIN
	SELECT @numContactId=ISNULL(adc.numContactId,0),@numRecOwner=ISNULL(obd.numRecOwner,0),
		   @numAssignedTo=ISNULL(obd.numAssignedTo,0),@numDivisionId=ISNULL(obd.numDivisionId,0),@vcDateFormat=d.vcDateFormat
	FROM  dbo.DivisionMaster AS  obd 	
	INNER join dbo.AdditionalContactsInformation as adc on adc.numDivisionId=obd.numDivisionID
	INNER JOIN  dbo.Domain AS d ON d.numDomainId=obd.numDomainId
	WHERE obd.numDomainId=@numDomainID 
		 AND obd.numDivisionID=@numRecordID
END
IF @numFormID=69 --Contacts
BEGIN
	SELECT @numContactId=ISNULL(adc.numContactId,0),@numRecOwner=ISNULL(obd.numRecOwner,0),
		   @numAssignedTo=ISNULL(obd.numAssignedTo,0),@numDivisionId=ISNULL(obd.numDivisionId,0),@vcDateFormat=d.vcDateFormat
	FROM  dbo.DivisionMaster AS  obd 	
	INNER join dbo.AdditionalContactsInformation as adc on adc.numDivisionId=obd.numDivisionID
	INNER JOIN  dbo.Domain AS d ON d.numDomainId=obd.numDomainId
	WHERE obd.numDomainId=@numDomainID 
		 AND adc.numContactId=@numRecordID
END
IF @numFormID=73 --Projects
BEGIN
	SELECT @numContactId=ISNULL(om.numIntPrjMgr,0),@numRecOwner=ISNULL(om.numRecOwner,0),
		   @numAssignedTo=ISNULL(om.numAssignedTo,0),@numDivisionId=ISNULL(om.numDivisionId,0),@numInternalPM=ISNULL(om.numIntPrjMgr,0),@numExternalPM=ISNULL(om.numCustPrjMgr,0)
	FROM  dbo.ProjectsMaster AS OM	
	WHERE om.numDomainId=@numDomainID 
	AND om.numProId=@numRecordID

	SELECT @numStageIDPC=ISNULL(s.numStageDetailsId,0)
    FROM ProjectsMaster o left join StagePercentageDetails s on s.numProjectID=o.numProId
	WHERE o.numDomainId=@numDomainID AND o.numProId=@numRecordID

END
IF @numFormID=72 --Cases
BEGIN
	SELECT @numContactId=ISNULL(om.numContactId,0),@numRecOwner=ISNULL(om.numRecOwner,0),
		   @numAssignedTo=ISNULL(om.numAssignedTo,0),@numDivisionId=ISNULL(om.numDivisionId,0),@vcDateFormat=d.vcDateFormat
	FROM  dbo.Cases AS OM	
	INNER JOIN  dbo.Domain AS d ON d.numDomainId=om.numDomainId
	WHERE om.numDomainId=@numDomainID 
	AND om.numCaseId=@numRecordID
	
	Select @numOppIDPC=ISNULL(o.numOppId,0)
    FROM Cases om 
	left JOIN CaseOpportunities co  on om.numCaseId=co.numCaseId  
	left join OpportunityMaster o  on o.numOppId=co.numOppId
	WHERE om.numDomainId=@numDomainID 
	AND om.numCaseId=@numRecordID
END

IF @numFormID=124 --Action items(Ticklers)
BEGIN
	SELECT @numContactId=ISNULL(om.numContactId,0),@numRecOwner=ISNULL(om.numAssignedBy,0),
		   @numAssignedTo=ISNULL(om.numAssign,0),@numDivisionId=ISNULL(om.numDivisionId,0),@vcDateFormat=d.vcDateFormat
	FROM  dbo.Communication AS OM	
	INNER JOIN  dbo.Domain AS d ON d.numDomainId=om.numDomainId
	WHERE om.numDomainId=@numDomainID 
	AND om.numCommId=@numRecordID
END



DECLARE @vcEmailID AS VARCHAR(100),@ContactName AS VARCHAR(150),@bitSMTPServer AS bit,@vcSMTPServer AS VARCHAR(100),
		@numSMTPPort AS NUMERIC,@bitSMTPAuth AS BIT,@vcSmtpPassword AS VARCHAR(100),@bitSMTPSSL BIT 

IF @numRecOwner>0
BEGIN
	SELECT @vcEmailID=vcEmailID,@ContactName=isnull(vcFirstname,'')+' '+isnull(vcLastName,''),@bitSMTPServer=isnull(bitSMTPServer,0),
		   @vcSMTPServer=case when bitSMTPServer= 1 then vcSMTPServer else '' end,
		   @numSMTPPort=case when bitSMTPServer= 1 then  isnull(numSMTPPort,0)  else 0 end,
		   @bitSMTPAuth=isnull(bitSMTPAuth,0),@vcSmtpPassword=isnull([vcSmtpPassword],''),@bitSMTPSSL=isnull(bitSMTPSSL,0),
		   @txtSignature=u.txtSignature,@numPhone=numPhone
		  FROM UserMaster U JOIN AdditionalContactsInformation A ON A.numContactID=U.numUserDetailId 
		  WHERE U.numDomainID=@numDomainID AND numUserDetailId=@numRecOwner
END

SELECT @numContactId AS numContactId,@numRecOwner AS numRecOwner,@numAssignedTo AS numAssignedTo,@numDivisionId AS numDivisionId,
	 @vcEmailID AS vcEmailID,@ContactName AS ContactName,@bitSMTPServer AS bitSMTPServer,@vcSMTPServer AS vcSMTPServer,@numSMTPPort AS numSMTPPort,
	 @bitSMTPAuth AS bitSMTPAuth,@vcSmtpPassword AS vcSmtpPassword,@bitSMTPSSL AS bitSMTPSSL,@txtSignature AS Signature,@numInternalPM as numInternalPM,@numExternalPM as numExternalPM,@numNextAssignedTo as numNextAssignedTo,@numPhone as numPhone,@numOppIDPC as numOppIDPC,@numStageIDPC as numStageIDPC ,@numOppBizDOCIDPC as numOppBizDOCIDPC
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetWorkFlowMasterDetail')
DROP PROCEDURE USP_GetWorkFlowMasterDetail
GO
Create PROCEDURE [dbo].[USP_GetWorkFlowMasterDetail]     
    @numDomainID numeric(18, 0),
	@numWFID numeric(18, 0)
as                 

SELECT * FROM WorkFlowMaster WF WHERE WF.numDomainID=@numDomainID AND WF.numWFID=@numWFID
--please

CREATE TABLE #tempField(numFieldID NUMERIC,  
vcFieldName NVARCHAR(50),vcDbColumnName NVARCHAR(50),vcOrigDbColumnName NVARCHAR(50),vcFieldDataType CHAR(1),  
vcAssociatedControlType NVARCHAR(50),vcListItemType VARCHAR(3),numListID NUMERIC,bitCustom BIT,vcLookBackTableName NVARCHAR(50),bitAllowFiltering BIT,bitAllowEdit BIT)  
DECLARE @numFormId AS NUMERIC(18,0)
SELECT @numFormId=numFormID from dbo.WorkFlowMaster WHERE numDomainID=@numDomainID
--Regular Fields  
INSERT INTO #tempField  
 SELECT numFieldID,vcFieldName,  
     vcDbColumnName,vcOrigDbColumnName,vcFieldDataType,vcAssociatedControlType,  
     vcListItemType,numListID,CAST(0 AS BIT) AS bitCustom,vcLookBackTableName,bitAllowFiltering,bitAllowEdit  
  FROM View_DynamicDefaultColumns  
  where numFormId=@numFormId and numDomainID=@numDomainID   
  AND ISNULL(bitWorkFlowField,0)=1 AND ISNULL(bitCustom,0)=0  
  
  
DECLARE @vcLocationID VARCHAR(100);SET @vcLocationID='0'  
Select @vcLocationID =isnull(vcLocationID,'0') from dynamicFormMaster where numFormID=@numFormId  
  
--Custom Fields     
INSERT INTO #tempField  
SELECT ISNULL(CFM.Fld_Id,0) as numFieldID,CFM.Fld_label as vcFieldName,  
    'Cust'+Convert(varchar(10),Fld_Id) as vcDbColumnName,'Cust'+Convert(varchar(10),Fld_Id) AS vcOrigDbColumnName,CASE CFM.Fld_type WHEN 'SelectBox' THEN 'V' WHEN 'DateField' THEN 'D' ELSE 'V' END AS vcFieldDataType,  
    CFM.Fld_type AS vcAssociatedControlType,CASE WHEN CFM.Fld_type='SelectBox' THEN 'LI' ELSE '' END AS vcListItemType,isnull(CFM.numListID,0) as numListID,  
    CAST(1 AS BIT) AS bitCustom,L.vcCustomLookBackTableName AS vcLookBackTableName,0 AS bitAllowFiltering,0 AS bitAllowEdit  
FROM CFW_Fld_Master CFM LEFT JOIN CFW_Validation V ON V.numFieldID = CFM.Fld_id  
     JOIN CFW_Loc_Master L on CFM.GRP_ID=L.Loc_Id  
            WHERE   CFM.numDomainID = @numDomainId  
     AND GRP_ID IN( SELECT ID FROM dbo.SplitIDs(@vcLocationID,','))  



--Fire Trigger Events on Fields
SELECT numFieldID,bitCustom,numWFID,numWFTriggerFieldID,CONVERT(NVARCHAR(80),CONVERT(NVARCHAR(20),numFieldId) +'_' + CASE WHEN bitCustom=0 THEN 'False' ELSE 'True' End) AS FieldID,(SELECT #tempField.vcFieldName FROM #tempField WHERE #tempField.numFieldID=
WorkFlowTriggerFieldList.numFieldID)AS FieldName FROM WorkFlowTriggerFieldList WHERE numWFID=@numWFID

SELECT * ,CONVERT(NVARCHAR(80),CONVERT(NVARCHAR(20),numFieldId) +'_' + CASE WHEN bitCustom=0 THEN 'False' ELSE 'True' End) AS FieldID,(SELECT #tempField.vcFieldName FROM #tempField WHERE #tempField.numFieldID=WorkFlowConditionList.numFieldID)AS FieldName 
,CASE WHEN  vcFilterOperator='eq' THEN 'equals' WHEN vcFilterOperator='ne' THEN 'not equal to' WHEN vcFilterOperator='gt' THEN 'greater than' WHEN vcFilterOperator='ge' THEN 'greater or equal' WHEN vcFilterOperator='lt' THEN 'less than' WHEN vcFilterOperator='le' THEN 'less or equal' ELSE 'NA' END AS FilterOperator  FROM WorkFlowConditionList WHERE numWFID=@numWFID

SELECT *,CASE WHEN tintActionType=1 THEN 
				   (SELECT  vcDocName              
					FROM    GenericDocuments  
					WHERE   numDocCategory = 369  
					AND tintDocumentType =1 --1 =generic,2=specific  
					AND ISNULL(vcDocumentSection,'') <> 'M'  
					AND numDomainId = @numDomainID  AND ISNULL(VcFileName,'') not LIKE '#SYS#%' AND numGenericDocID=numTemplateID)                
             WHEN	tintActionType=2 then (Select TemplateName From tblActionItemData  where numdomainId=@numDomainID AND RowID=numTemplateID) 
              ELSE '0'  END EmailTemplate ,
         CASE WHEN vcEmailToType='1' THEN 'Owner of trigger record' WHEN vcEmailToType='2,' THEN 'Assignee of trigger record'  WHEN vcEmailToType='1,2' THEN 'Owner of trigger record,Assignee of trigger record'  WHEN   vcEmailToType='3' THEN 'Primary contact of trigger record' WHEN   vcEmailToType='1,3,' THEN 'Owner of trigger record,Primary contact of trigger record' WHEN vcEmailToType='2,3' THEN 'Primary contact of trigger record,Assignee of trigger record'  WHEN vcEmailToType='1,2,3' THEN  'Owner of trigger record,Primary contact of trigger record,Assignee of trigger record' ELSE NULL END AS EmailToType,
         CASE WHEN tintTicklerActionAssignedTo=1 THEN 'Owner of trigger record' ELSE 'Assignee of trigger record' END AS TicklerAssignedTo,
		 (select vcTemplateName from BizDocTemplate where numBizDocTempID=numBizDocTemplateID ) as  vcBizDocTemplate, 
		 (SELECT isnull(vcRenamedListName,vcData) as vcData FROM listdetails Ld        
						left join listorder LO 
						on Ld.numListItemID= LO.numListItemID and Lo.numDomainId = @numDomainID 
						WHERE Ld.numListID=27 AND (constFlag=1 or Ld.numDomainID=@numDomainID )    AND  Ld.numListItemID=numBizDocTypeID) as vcBizDoc ,							
		 (SELECT Case When numOpptype=2 then 'Purchase' else 'Sales' end As BizDocType from BizDocTemplate WHERE numBizDocTempID=numBizDocTemplateID) as vcBizDocType ,
		 (SELECT numOpptype from BizDocTemplate where numBizDocTempID=numBizDocTemplateID) as numOppType,
		 ISNULL(vcSMSText,'') as vcSMSText    ,
		 ISNULL(vcEmailSendTo,'') as vcEmailSendTo ,
		 ISNULL(vcMailBody,'') as vcMailBody ,
		 ISNULL(vcMailSubject,'') as vcMailSubject
FROM dbo.WorkFlowActionList WHERE numWFID=@numWFID

 
SELECT *,CONVERT(NVARCHAR(80),CONVERT(NVARCHAR(20),numFieldId) +'_' + CASE WHEN bitCustom=0 THEN 'False' ELSE 'True' End) AS FieldID,(SELECT #tempField.vcFieldName FROM #tempField WHERE #tempField.numFieldID=WorkFlowActionUpdateFields.numFieldID)AS FieldName  FROM WorkFlowActionUpdateFields WHERE numWFActionID IN(SELECT numWFActionID FROM WorkFlowActionList WHERE numWFID=@numWFID)
DROP TABLE #tempField
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_InLineEditDataSave')
DROP PROCEDURE USP_InLineEditDataSave
GO
CREATE PROCEDURE [dbo].[USP_InLineEditDataSave] 
( 
 @numDomainID NUMERIC(9),
 @numUserCntID NUMERIC(9),
 @numFormFieldId NUMERIC(9),
 @bitCustom AS BIT = 0,
 @InlineEditValue VARCHAR(8000),
 @numContactID NUMERIC(9),
 @numDivisionID NUMERIC(9),
 @numWOId NUMERIC(9),
 @numProId NUMERIC(9),
 @numOppId NUMERIC(9),
 @numRecId NUMERIC(9),
 @vcPageName NVARCHAR(100),
 @numCaseId NUMERIC(9),
 @numWODetailId NUMERIC(9),
 @numCommID NUMERIC(9)
 )
AS 

declare @strSql as nvarchar(4000)
DECLARE @vcDbColumnName AS VARCHAR(100),@vcOrigDbColumnName AS VARCHAR(100),@vcLookBackTableName AS varchar(100)
DECLARE @vcListItemType as VARCHAR(3),@numListID AS numeric(9),@vcAssociatedControlType varchar(20)
declare @tempAssignedTo as numeric(9);set @tempAssignedTo=null           
DECLARE @Value1 AS VARCHAR(18),@Value2 AS VARCHAR(18)

IF  @bitCustom =0
BEGIN
	SELECT @vcDbColumnName=vcDbColumnName,@vcLookBackTableName=vcLookBackTableName,
		   @vcListItemType=vcListItemType,@numListID=numListID,@vcAssociatedControlType=vcAssociatedControlType,
		   @vcOrigDbColumnName=vcOrigDbColumnName
	 FROM DycFieldMaster WHERE numFieldId=@numFormFieldId
	 
	 
	if @vcLookBackTableName = 'DivisionMaster' 
	 BEGIN 
	 
	   IF @vcDbColumnName='numFollowUpStatus' --Add to FollowUpHistory
	    Begin
			declare @numFollow as varchar(10),@binAdded as varchar(20),@PreFollow as varchar(10),@RowCount as varchar(2)                            
			                      
			set @PreFollow=(select count(*)  numFollowUpStatus from FollowUpHistory where numDivisionID= @numDivisionID and numDomainId= @numDomainID)                          
			set @RowCount=@@rowcount  
			                        
			select   @numFollow=numFollowUpStatus, @binAdded=bintModifiedDate from divisionmaster where numDivisionID=@numDivisionID and numDomainId= @numDomainID                    
			
			if @numFollow <>'0' and @numFollow <> @InlineEditValue                          
			begin                          
				if @PreFollow<>0                          
					begin                          
			                   
						if @numFollow <> (select top 1  numFollowUpStatus from FollowUpHistory where numDivisionID= @numDivisionID and numDomainId= @numDomainID order by numFollowUpStatusID desc)                          
							begin                          
			                      	insert into FollowUpHistory(numFollowUpstatus,numDivisionID,bintAddedDate,numDomainId)                          
									values(@numFollow,@numDivisionID,@binAdded,@numDomainID)                          
							end                          
					end                          
				else                          
					begin                          
						insert into FollowUpHistory(numFollowUpstatus,numDivisionID,bintAddedDate,numDomainId)                          
						 values(@numFollow,@numDivisionID,@binAdded,@numDomainID)                          
					end                          
			   end 
			END
		
		 IF @vcDbColumnName='numAssignedTo' ---Updating if organization is assigned to someone
			BEGIN
				select @tempAssignedTo=isnull(numAssignedTo,0) from DivisionMaster where numDivisionID = @numDivisionID and numDomainId= @numDomainID           
			
				if (@tempAssignedTo<>@InlineEditValue and  @InlineEditValue<>'0')          
					begin            
						update DivisionMaster set numAssignedTo=@InlineEditValue ,numAssignedBy=@numUserCntID where numDivisionID = @numDivisionID  and numDomainId= @numDomainID           
					end           
				else if  (@InlineEditValue ='0')          
					begin          
						update DivisionMaster set numAssignedTo=0 ,numAssignedBy=0 where numDivisionID = @numDivisionID  and numDomainId= @numDomainID        
					end    
			END
	    	
		SET @strSql='update DivisionMaster set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numDivisionID=@numDivisionID and numDomainID=@numDomainID'
	 END 
	 ELSE if @vcLookBackTableName = 'AdditionalContactsInformation' 
	 BEGIN 
			IF @vcDbColumnName='bitPrimaryContact' AND @InlineEditValue='True'
			BEGIN
				DECLARE @bitPrimaryContactCheck AS BIT             
				DECLARE @numID AS NUMERIC(9) 
				
				SELECT TOP 1 @numID = numContactID,@bitPrimaryContactCheck = ISNULL(bitPrimaryContact,0)
					FROM AdditionalContactsInformation WHERE numDivisionID = @numDivisionID 
                   
					IF @@rowcount = 0 SET @numID = 0             
				
					WHILE @numID > 0            
					BEGIN            
						IF @bitPrimaryContactCheck = 1 
							UPDATE  AdditionalContactsInformation SET bitPrimaryContact = 0 WHERE numContactID = @numID  
                              
						SELECT TOP 1 @numID = numContactID,@bitPrimaryContactCheck = ISNULL(bitPrimaryContact,0)
						FROM AdditionalContactsInformation WHERE numDivisionID = @numDivisionID AND numContactID > @numID    
                                
						IF @@rowcount = 0 SET @numID = 0             
					END 
			  END
			  ELSE IF @vcDbColumnName='bitPrimaryContact' AND @InlineEditValue='False'
				BEGIN
					IF (SELECT COUNT(*) FROM AdditionalContactsInformation WHERE numDivisionID = @numDivisionID AND numContactId!=@numContactId AND ISNULL(bitPrimaryContact,0)=1) = 0
						SET @InlineEditValue='True'
				END 
			
		SET @strSql='update AdditionalContactsInformation set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numDivisionID=@numDivisionID and numDomainID=@numDomainID and numContactId=@numContactId'
	 END 
	 ELSE if @vcLookBackTableName = 'CompanyInfo' 
	 BEGIN 
		SET @strSql='update CompanyInfo set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numCompanyId=(select numCompanyId from divisionmaster where numDivisionID=@numDivisionID and numDomainID=@numDomainID) and numDomainID=@numDomainID'
	 END 
	 ELSE if @vcLookBackTableName = 'WorkOrder' 
	 BEGIN 
		DECLARE @numItemCode AS NUMERIC(9)
	 
		IF @numWODetailId>0
		BEGIN
			SET @strSql='update WorkOrderDetails set ' + @vcDbColumnName + '=@InlineEditValue where numWOId=@numWOId and numWODetailId=@numWODetailId'
			
			IF @vcDbColumnName='numQtyItemsReq'
			BEGIN 
				SELECT @numItemCode=numChildItemID FROM WorkOrderDetails WHERE numWOId=@numWOId and numWODetailId=@numWODetailId
				
				EXEC USP_UpdateWorkOrderDetailQuantity @numItemCode,@InlineEditValue,@numWOId
				--UPDATE WorkOrderDetails SET numQtyItemsReq=CONVERT(NUMERIC(9),numQtyItemsReq_Orig * CONVERT(NUMERIC(9,0),@InlineEditValue)) WHERE numWOId=@numWOId
			END 
		END 	
		ELSE
		BEGIN
			SET @strSql='update WorkOrder set ' + @vcDbColumnName + '=@InlineEditValue where numWOId=@numWOId and numDomainID=@numDomainID'
			
			IF @vcDbColumnName='numQtyItemsReq'
			BEGIN 
				SELECT @numItemCode=numItemCode FROM WorkOrder WHERE numWOId=@numWOId and numDomainID=@numDomainID
			
				EXEC USP_UpdateWorkOrderDetailQuantity @numItemCode,@InlineEditValue,@numWOId
				--UPDATE WorkOrderDetails SET numQtyItemsReq=CONVERT(NUMERIC(9),numQtyItemsReq_Orig * CONVERT(NUMERIC(9,0),@InlineEditValue)) WHERE numWOId=@numWOId
			END 
		END	
	 END
	 ELSE if @vcLookBackTableName = 'ProjectsMaster' 
	 BEGIN 
		IF @vcDbColumnName='numAssignedTo' ---Updating if Project is assigned to someone 
			BEGIN
				select @tempAssignedTo=isnull(numAssignedTo,0) from ProjectsMaster where numProId=@numProId and numDomainId= @numDomainID           
			
				if (@tempAssignedTo<>@InlineEditValue and  @InlineEditValue<>'0')          
					begin            
						update ProjectsMaster set numAssignedTo=@InlineEditValue ,numAssignedBy=@numUserCntID where numProId = @numProId  and numDomainId= @numDomainID           
					end           
				else if  (@InlineEditValue ='0')          
					begin          
						update ProjectsMaster set numAssignedTo=0 ,numAssignedBy=0 where numProId = @numProId  and numDomainId= @numDomainID        
					end    
			END
		else IF @vcDbColumnName='numProjectStatus'  ---Updating All Statge to 100% if Completed
			BEGIN
				IF @InlineEditValue='27492'
				BEGIN
					UPDATE PM SET dtCompletionDate=GETUTCDATE(),monTotalExpense=PIE.monExpense,
					monTotalIncome=PIE.monIncome,monTotalGrossProfit=PIE.monBalance,numProjectStatus=27492
					FROM ProjectsMaster PM,dbo.GetProjectIncomeExpense(@numDomainId,@numProId) PIE
					WHERE PM.numProId=@numProId
					
					EXEC USP_ManageProjectCommission @numDomainId,@numProId
						
					Update StagePercentageDetails set tinProgressPercentage=100,bitclose=1 where numProjectID=@numProId

					EXEC dbo.USP_GetProjectTotalProgress
							@numDomainID = @numDomainID, --  numeric(9, 0)
							@numProId = @numProId, --  numeric(9, 0)
							@tintMode = 1 --  tinyint
				END			
				ELSE
				BEGIN
					DELETE from BizDocComission WHERE numDomainID = @numDomainID AND numProId=@numProId AND numComissionID 
						NOT IN (SELECT BC.numComissionID FROM dbo.BizDocComission BC JOIN dbo.PayrollTracking PT 
							ON BC.numComissionID = PT.numComissionID AND BC.numDomainId = PT.numDomainId WHERE BC.numDomainID = @numDomainID AND ISNULL(BC.numProId,0) = @numProId)
				END
			END
			

		SET @strSql='update ProjectsMaster set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numProId=@numProId and numDomainID=@numDomainID'
	 END 
	 ELSE if @vcLookBackTableName = 'OpportunityMaster' 
	 BEGIN 
		IF @vcDbColumnName='numAssignedTo' ---Updating if organization is assigned to someone  
			BEGIN
				select @tempAssignedTo=isnull(numAssignedTo,0) from OpportunityMaster where numOppId=@numOppId and numDomainId= @numDomainID           
			
				if (@tempAssignedTo<>@InlineEditValue and  @InlineEditValue<>'0')          
					begin            
						update OpportunityMaster set numAssignedTo=@InlineEditValue ,numAssignedBy=@numUserCntID where numOppId = @numOppId  and numDomainId= @numDomainID           
					end           
				else if  (@InlineEditValue ='0')          
					begin          
						update OpportunityMaster set numAssignedTo=0 ,numAssignedBy=0 where numOppId = @numOppId  and numDomainId= @numDomainID        
					end    
			END
		ELSE IF @vcDbColumnName='tintActive'
		 BEGIN
		 	SET @InlineEditValue= CASE @InlineEditValue WHEN 'True' THEN 1 ELSE 0 END 
		 END
		 ELSE IF @vcDbColumnName='tintSource'
		 BEGIN
			IF CHARINDEX('~', @InlineEditValue)>0
			BEGIN
				SET @Value1=SUBSTRING(@InlineEditValue, 1,CHARINDEX('~', @InlineEditValue)-1) 
				SET @Value2=SUBSTRING(@InlineEditValue, CHARINDEX('~', @InlineEditValue)+1,LEN(@InlineEditValue)) 
			END	
			ELSE
			BEGIN
				SET @Value1=@InlineEditValue
				SET @Value2=0
			END
			
		 	update OpportunityMaster set tintSourceType=@Value2 where numOppId = @numOppId  and numDomainId= @numDomainID        
		 	
		 	SET @InlineEditValue= @Value1
		 END
		ELSE IF @vcDbColumnName='tintOppStatus' --Update Inventory  0=Open,1=Won,2=Lost
		 BEGIN
			declare @tintOppStatus AS TINYINT
			select @tintOppStatus=tintOppStatus from OpportunityMaster where numOppID=@numOppID              

			if @tintOppStatus=0 and @InlineEditValue='1' --Open to Won
				BEGIN
					select @numDivisionID=numDivisionID from  OpportunityMaster where numOppID=@numOppID   
					DECLARE @tintCRMType AS TINYINT      
					select @tintCRMType=tintCRMType from divisionmaster where numDivisionID =@numDivisionID 

					-- Promote Lead to Account when Sales/Purchase Order is created against it
					if @tintCRMType=0 --Lead & Order
					begin        
						update divisionmaster set tintCRMType=2,bintLeadProm=getutcdate(),bintLeadPromBy=@numUserCntID,bintProsProm=getutcdate(),bintProsPromBy=@numUserCntID        
						where numDivisionID=@numDivisionID        
					end
					--Promote Prospect to Account
					else if @tintCRMType=1 
					begin        
						update divisionmaster set tintCRMType=2,bintProsProm=getutcdate(),bintProsPromBy=@numUserCntID        
						where numDivisionID=@numDivisionID        
					end    

		 			EXEC USP_UpdatingInventoryonCloseDeal @numOppID,@numUserCntID

					update OpportunityMaster set bintOppToOrder=GETUTCDATE() where numOppId=@numOppId and numDomainID=@numDomainID
				END
			if (@tintOppStatus=1 and @InlineEditValue='2') or (@tintOppStatus=1 and @InlineEditValue='0') --Won to Lost or Won to Open
				EXEC USP_RevertDetailsOpp @numOppID,0,@numUserCntID 
		 END
		 ELSE IF @vcDbColumnName='vcOppRefOrderNo' --Update vcRefOrderNo OpportunityBizDocs
		 BEGIN
			 IF LEN(ISNULL(@InlineEditValue,''))>0
			 BEGIN
	   			UPDATE dbo.OpportunityBizDocs SET vcRefOrderNo=@InlineEditValue WHERE numOppId=@numOppID
			 END
		 END
		 ELSE IF @vcDbColumnName='numStatus' 
		 BEGIN
		 	 IF EXISTS (SELECT 1 FROM dbo.OpportunityMaster WHERE numOppId = @numOppID AND tintOppType=1 AND isnull(tintOppStatus,0)=1 AND ISNULL(numStatus,0)!= @InlineEditValue)
			 BEGIN
	   				EXEC USP_ManageOpportunityAutomationQueue
							@numOppQueueID = 0, -- numeric(18, 0)
							@numDomainID = @numDomainID, -- numeric(18, 0)
							@numOppId = @numOppID, -- numeric(18, 0)
							@numOppBizDocsId = 0, -- numeric(18, 0)
							@numOrderStatus = @InlineEditValue, -- numeric(18, 0)
							@numUserCntID = @numUserCntID, -- numeric(18, 0)
							@tintProcessStatus = 1, -- tinyint
							@tintMode = 1 -- TINYINT
			END 
		 END	
		
		SET @strSql='update OpportunityMaster set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numOppId=@numOppId and numDomainID=@numDomainID'
	 END
	 ELSE if @vcLookBackTableName = 'Cases' 
	 BEGIN 
	 		IF @vcDbColumnName='numAssignedTo' ---Updating if Case is assigned to someone  
			BEGIN
				select @tempAssignedTo=isnull(numAssignedTo,0) from Cases where numCaseId=@numCaseId and numDomainId= @numDomainID           
			
				if (@tempAssignedTo<>@InlineEditValue and  @InlineEditValue<>'0')          
					begin            
						update Cases set numAssignedTo=@InlineEditValue ,numAssignedBy=@numUserCntID where numCaseId = @numCaseId  and numDomainId= @numDomainID           
					end           
				else if  (@InlineEditValue ='0')          
					begin          
						update Cases set numAssignedTo=0 ,numAssignedBy=0 where numCaseId = @numCaseId  and numDomainId= @numDomainID        
					end    
			END
			
		SET @strSql='update Cases set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numCaseId=@numCaseId and numDomainID=@numDomainID'
	 END 
	 ELSE if @vcLookBackTableName = 'Tickler' 
	 BEGIN 	
		SET @strSql='update Communication set ' + @vcOrigDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,dtModifiedDate=getutcdate() where numCommId=@numCommId and numDomainID=@numDomainID'
	 END
	 
	 
	EXECUTE sp_executeSQL @strSql, N'@numDomainID NUMERIC(9),@numUserCntID NUMERIC(9),@numDivisionID NUMERIC(9),@numContactID NUMERIC(9),@InlineEditValue VARCHAR(8000),@numWOId NUMERIC(9),@numProId NUMERIC(9),@numOppId NUMERIC(9),@numCaseId NUMERIC(9),@numWODetailId NUMERIC(9),@numCommID Numeric(9)',
		     @numDomainID,@numUserCntID,@numDivisionID,@numContactID,@InlineEditValue,@numWOId,@numProId,@numOppId,@numCaseId,@numWODetailId,@numCommID;
		     
	IF @vcAssociatedControlType='SelectBox'                                                    
       BEGIN       
			IF @vcDbColumnName='tintSource' AND @vcLookBackTableName = 'OpportunityMaster'
			BEGIN
				SELECT dbo.fn_GetOpportunitySourceValue(@Value1,@Value2,@numDomainID) AS vcData                                             
			END
			else
				SELECT dbo.fn_getSelectBoxValue(@vcListItemType,@InlineEditValue,@vcDbColumnName) AS vcData                                             
		end 
		ELSE
		BEGIN
			 IF @vcDbColumnName='tintActive'
		  	 BEGIN
			 	SET @InlineEditValue= CASE @InlineEditValue WHEN 1 THEN 'True' ELSE 'False' END 
			 END

			IF @vcAssociatedControlType = 'Check box' or @vcAssociatedControlType = 'Checkbox'
			BEGIN
	 			SET @InlineEditValue= Case @InlineEditValue WHEN 'True' THEN 'Yes' ELSE 'No' END
			END
			
		    SELECT @InlineEditValue AS vcData
		END    
END 
ELSE
BEGIN	 

	SET @InlineEditValue=Cast(@InlineEditValue as varchar(1000))
	SELECT @vcAssociatedControlType=Fld_type
	 FROM CFW_Fld_Master WHERE Fld_Id=@numFormFieldId 

	IF @vcAssociatedControlType = 'CheckBox'
	BEGIN
	    SET @InlineEditValue=(case when @InlineEditValue='False' then 0 ELSE 1 END)
	END

	IF @vcPageName='organization'
	BEGIN 
		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
			BEGIN
				UPDATE CFW_FLD_Values SET Fld_Value=@InlineEditValue WHERE FldDTLID=(SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
			END	
		ELSE
			BEGIN
				insert into CFW_FLD_Values (Fld_ID,Fld_Value,RecId) VALUES (@numFormFieldId,@InlineEditValue,@numRecId)         
			END
	END 
	ELSE IF @vcPageName='contact'
	BEGIN 
		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Cont CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
			BEGIN
				UPDATE CFW_FLD_Values_Cont SET Fld_Value=@InlineEditValue WHERE FldDTLID=(SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Cont CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
			END	
		ELSE
			BEGIN
				insert into CFW_FLD_Values_Cont (Fld_ID,Fld_Value,RecId) VALUES (@numFormFieldId,@InlineEditValue,@numRecId)         
			END
	END
	ELSE IF @vcPageName='project'
	BEGIN 
		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Pro CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
			BEGIN
				UPDATE CFW_FLD_Values_Pro SET Fld_Value=@InlineEditValue WHERE FldDTLID=(SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Pro CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
			END	
		ELSE
			BEGIN
				insert into CFW_FLD_Values_Pro (Fld_ID,Fld_Value,RecId) VALUES (@numFormFieldId,@InlineEditValue,@numRecId)         
			END
	END
	ELSE IF @vcPageName='opp'
	BEGIN 
		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_Fld_Values_Opp CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
			BEGIN
				UPDATE CFW_Fld_Values_Opp SET Fld_Value=@InlineEditValue WHERE FldDTLID=(SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_Fld_Values_Opp CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
			END	
		ELSE
			BEGIN
				insert into CFW_Fld_Values_Opp (Fld_ID,Fld_Value,RecId) VALUES (@numFormFieldId,@InlineEditValue,@numRecId)         
			END
	END
	ELSE IF @vcPageName='case'
	BEGIN 
		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Case CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
			BEGIN
				UPDATE CFW_FLD_Values_Case SET Fld_Value=@InlineEditValue WHERE FldDTLID=(SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Case CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
			END	
		ELSE
			BEGIN
				insert into CFW_FLD_Values_Case (Fld_ID,Fld_Value,RecId) VALUES (@numFormFieldId,@InlineEditValue,@numRecId)         
			END
	END

	if @vcAssociatedControlType = 'SelectBox'            
	 begin            
		 select vcData from ListDetails WHERE numListItemID=@InlineEditValue            
	END
	ELSE IF @vcAssociatedControlType = 'CheckBox'
	BEGIN
	 	select CASE @InlineEditValue WHEN 1 THEN 'Yes' ELSE 'No' END AS vcData
	END
	ELSE
		BEGIN
			SELECT @InlineEditValue AS vcData
		END 
END

/****** Object:  StoredProcedure [dbo].[USP_InsertDepositHeaderDet]    Script Date: 07/26/2008 16:19:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created by Siva                              
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_insertdepositheaderdet')
DROP PROCEDURE usp_insertdepositheaderdet
GO
CREATE PROCEDURE [dbo].[USP_InsertDepositHeaderDet]                              
(@numDepositId AS NUMERIC(9)=0,        
@numChartAcntId AS NUMERIC(9)=0,                             
@datEntry_Date AS DATETIME,                                          
@numAmount AS MONEY,        
@numRecurringId AS NUMERIC(9)=0,                               
@numDomainId AS NUMERIC(9)=0,              
@numUserCntID AS NUMERIC(9)=0,
@strItems TEXT=''
,@numPaymentMethod NUMERIC
,@vcReference VARCHAR(500)
,@vcMemo VARCHAR(1000)
,@tintDepositeToType TINYINT=1 --1 = Direct Deposite to Bank Account , 2= Deposite to Default Undeposited Funds Account
,@numDivisionID NUMERIC
,@tintMode TINYINT =0 --0 = when called from receive payment page, 1 = when called from MakeDeposit Page
,@tintDepositePage TINYINT --1:Make Deposite 2:Receive Payment 3:Credit Memo(Sales Return)
,@numTransHistoryID NUMERIC(9)=0,
@numReturnHeaderID NUMERIC(9)=0,
@numCurrencyID numeric(9) =0,
@fltExchangeRate FLOAT = 1,
@numAccountClass NUMERIC(18,0)=0
)            
AS                                           
BEGIN 

IF ISNULL(@numCurrencyID,0) = 0 
BEGIN
 SET @fltExchangeRate=1
 SELECT @numCurrencyID= ISNULL(numCurrencyID,0) FROM dbo.Domain WHERE numDomainId=@numDomainId	
END
 

--Validation of closed financial year
EXEC dbo.USP_ValidateFinancialYearClosingDate @numDomainID,@datEntry_Date

DECLARE  @hDocItem INT
 
 IF @tintMode != 2
 BEGIN
	 IF @numRecurringId=0 SET @numRecurringId=NULL 
	 IF @numDivisionID=0 SET @numDivisionID=NULL
	 IF @numReturnHeaderID=0 SET @numReturnHeaderID=NULL
	 
	 IF @numDepositId=0                                  
	  BEGIN                              
		  INSERT INTO DepositMaster(numChartAcntId,dtDepositDate,monDepositAmount,numRecurringId,numDomainId,numCreatedBy,dtCreationDate,numPaymentMethod,vcReference,vcMemo,tintDepositeToType,numDivisionID,tintDepositePage,numTransHistoryID,numReturnHeaderID,numCurrencyID,fltExchangeRate,numAccountClass) VALUES            
	   (@numChartAcntId,@datEntry_Date,@numAmount,@numRecurringId,@numDomainId,@numUserCntID,GETUTCDATE(),@numPaymentMethod,@vcReference,@vcMemo,@tintDepositeToType,@numDivisionID,@tintDepositePage,@numTransHistoryID,@numReturnHeaderID,@numCurrencyID,ISNULL(@fltExchangeRate,1),@numAccountClass)                                        
	                         
	   SET @numDepositId = @@IDENTITY                                        
	   SELECT @numDepositId
	  END                            
	                        
	 IF @numDepositId<>0                        
	  BEGIN                        
		UPDATE DepositMaster SET numChartAcntId=@numChartAcntId,dtDepositDate=@datEntry_Date,monDepositAmount=@numAmount,numRecurringId=@numRecurringId,
	   numModifiedBy=@numUserCntID,dtModifiedDate=GETUTCDATE(),numPaymentMethod =@numPaymentMethod,vcReference=@vcReference,vcMemo=@vcMemo,tintDepositeToType =@tintDepositeToType,numDivisionID=@numDivisionID,tintDepositePage=@tintDepositePage,numTransHistoryID=@numTransHistoryID ,numCurrencyID=@numCurrencyID,fltExchangeRate=@fltExchangeRate
	   WHERE numDepositId=@numDepositId AND numDomainId=@numDomainId                       
	   SELECT @numDepositId
	  END            
 END 
  
  IF @tintMode = 1
  BEGIN
  
 
      IF CONVERT(VARCHAR(10),@strItems) <> ''
        BEGIN
          EXEC sp_xml_preparedocument
            @hDocItem OUTPUT ,
            @strItems
            
				-- When user unchecks deposit entry of undeposited fund, reset original entry
				UPDATE dbo.DepositMaster 
				SET bitDepositedToAcnt = 0 WHERE numDepositId IN (  
				
				 SELECT numChildDepositID FROM dbo.DepositeDetails WHERE numDepositID = @numDepositId AND numChildDepositID>0 
				 AND numDepositeDetailID NOT IN (SELECT X.numDepositeDetailID 
												FROM OPENXML(@hDocItem,'/NewDataSet/Item [numDepositeDetailID>0]',2)                                                                                                              
												WITH(numDepositeDetailID NUMERIC(9))X)
				
				)
			
			
              DELETE FROM dbo.DepositeDetails WHERE numDepositID = @numDepositId
              AND numDepositeDetailID NOT IN (SELECT X.numDepositeDetailID 
				 FROM OPENXML(@hDocItem,'/NewDataSet/Item [numDepositeDetailID>0]',2)                                                                                                              
				WITH(numDepositeDetailID NUMERIC(9))X)
			
			  INSERT INTO dbo.DepositeDetails
					  ( 
						numDepositID ,
						numOppBizDocsID ,
						numOppID ,
						monAmountPaid,
						numChildDepositID,
						numPaymentMethod,
						vcMemo,
						vcReference,
						numAccountID,
						numClassID,
						numProjectID,
						numReceivedFrom,dtCreatedDate,dtModifiedDate
					  )
			  SELECT 
					 @numDepositId,
					 NULL ,
					 NULL ,
					 X.monAmountPaid,
					 X.numChildDepositID,
					 X.numPaymentMethod,
						X.vcMemo,
						X.vcReference,
						X.numAccountID,
						X.numClassID,
						X.numProjectID,
						X.numReceivedFrom,GETUTCDATE(),GETUTCDATE()
			  FROM   (SELECT *
					  FROM   OPENXML (@hDocItem, '/NewDataSet/Item [numDepositeDetailID=0]', 2)
								WITH (
								numDepositeDetailID NUMERIC,
								numChildDepositID NUMERIC(9),
								monAmountPaid MONEY,
								numPaymentMethod NUMERIC(18, 0),
								vcMemo VARCHAR(1000),
								vcReference VARCHAR(500),
								numClassID	NUMERIC(18, 0),	
								numProjectID	NUMERIC(18, 0),
								numReceivedFrom	NUMERIC(18, 0),
								numAccountID NUMERIC(18, 0)
										)) X 
								
				UPDATE dbo.DepositeDetails
				SET
						[monAmountPaid] = X.monAmountPaid,
						[numChildDepositID] = X.numChildDepositID,
						[numPaymentMethod] = X.numPaymentMethod,
						[vcMemo] = X.vcMemo,
						[vcReference] = X.vcReference,
						[numClassID] = X.numClassID,
						[numProjectID] = X.numProjectID,
						[numReceivedFrom] = X.numReceivedFrom,
						[numAccountID] = X.numAccountID,
						dtModifiedDate=GETUTCDATE()
				FROM (SELECT *
									  FROM   OPENXML (@hDocItem, '/NewDataSet/Item [numDepositeDetailID>0]', 2)
												WITH (
												numDepositeDetailID NUMERIC,
												numChildDepositID NUMERIC(9),
												monAmountPaid MONEY,
												numPaymentMethod NUMERIC(18, 0),
												vcMemo VARCHAR(1000),
												vcReference VARCHAR(500),
												numClassID	NUMERIC(18, 0),	
												numProjectID	NUMERIC(18, 0),
												numReceivedFrom	NUMERIC(18, 0),
												numAccountID NUMERIC(18, 0)
														)) X 
				WHERE X.numDepositeDetailID =DepositeDetails.numDepositeDetailID AND dbo.DepositeDetails.numDepositID = @numDepositId




				UPDATE dbo.DepositMaster SET bitDepositedToAcnt=1 WHERE numDomainId=@numDomainID AND  numDepositId IN (
									   SELECT numChildDepositID
									  FROM   OPENXML (@hDocItem, '/NewDataSet/Item', 2)
												WITH (numChildDepositID NUMERIC(9)) WHERE numChildDepositID > 0
				)
				
								
  								
          EXEC sp_xml_removedocument
            @hDocItem
        END
  
  
  END 
  
  
  
  IF @tintMode = 0 OR @tintMode = 2
  BEGIN
      IF CONVERT(VARCHAR(10),@strItems) <> ''
        BEGIN
				  EXEC sp_xml_preparedocument
					@hDocItem OUTPUT ,
					@strItems
		            
						/*UPDATE dbo.OpportunityBizDocs 
						SET dbo.OpportunityBizDocs.monAmountPaid = ISNULL(dbo.OpportunityBizDocs.monAmountPaid,0) - ISNULL(X.monAmountPaid,0)
						FROM 				(SELECT numOppBizDocsID,monAmountPaid FROM dbo.DepositeDetails WHERE numDepositID = @numDepositId AND numOppBizDocsID>0 
												AND numDepositeDetailID NOT IN (SELECT numDepositeDetailID 
														FROM OPENXML(@hDocItem,'/NewDataSet/Item [numDepositeDetailID>0]',2)                                                                                                              
														WITH(numDepositeDetailID NUMERIC(9)) )) AS X
						WHERE  X.numOppBizDocsID = OpportunityBizDocs.numOppBizDocsId*/
						
					  SELECT 
					  numDepositeDetailID,numOppBizDocsID ,numOppID ,monAmountPaid INTO #temp
					  FROM   (SELECT *
							  FROM   OPENXML (@hDocItem, '/NewDataSet/Item', 2)
										WITH (numDepositeDetailID NUMERIC(9),numOppBizDocsID NUMERIC(9),numOppID NUMERIC(9),monAmountPaid MONEY)) X
				 	
					
					IF @tintMode = 0
					BEGIN
						UPDATE OBD
						SET monAmountPaid = ISNULL(OBD.monAmountPaid,0) - DD.monAmountPaid,OBD.numModifiedBy=@numUserCntID
						FROM OpportunityBizDocs OBD JOIN DepositeDetails DD ON OBD.numOppBizDocsId=DD.numOppBizDocsID
						WHERE DD.numDepositID = @numDepositId AND DD.numOppBizDocsID>0 
							 AND DD.numDepositeDetailID NOT IN (SELECT numDepositeDetailID 
														FROM #temp WHERE numDepositeDetailID>0)
														
													
						  DELETE FROM dbo.DepositeDetails WHERE numDepositID = @numDepositId
						  AND numDepositeDetailID NOT IN (SELECT X.numDepositeDetailID 
									FROM OPENXML(@hDocItem,'/NewDataSet/Item [numDepositeDetailID>0]',2)                                                                                                              
									WITH(numDepositeDetailID NUMERIC(9))X)
--							 AND numDepositeDetailID NOT IN (SELECT numDepositeDetailID 
--														FROM #temp WHERE numDepositeDetailID>0)
					 END
		              
		              
					  UPDATE  DD SET [monAmountPaid] = DD.monAmountPaid +  X.monAmountPaid 
								FROM DepositeDetails DD JOIN #temp X ON DD.numOppBizDocsID=X.numOppBizDocsID AND 
								DD.numOppID = X.numOppID
								WHERE X.numDepositeDetailID = 0 AND DD.numDepositID = @numDepositId
										
										
					  INSERT INTO dbo.DepositeDetails
							  ( numDepositID ,numOppBizDocsID ,numOppID ,monAmountPaid,dtCreatedDate,dtModifiedDate )
					  SELECT @numDepositId, numOppBizDocsID , numOppID , monAmountPaid,GETUTCDATE(),GETUTCDATE()
					  FROM #temp WHERE numDepositeDetailID=0 AND numOppBizDocsID NOT IN (SELECT numOppBizDocsID FROM DepositeDetails WHERE numDepositID = @numDepositId)
										
						
						IF @tintMode = 0
						BEGIN
							
		--				UPDATE OBD
		--				SET monAmountPaid = ISNULL(OBD.monAmountPaid,0) - DD.monAmountPaid
		--				FROM OpportunityBizDocs OBD JOIN DepositeDetails DD ON OBD.numOppBizDocsId=DD.numOppBizDocsID
		--				WHERE DD.numDepositID = @numDepositId AND DD.numOppBizDocsID>0 
		--					 AND DD.numDepositeDetailID IN (SELECT X.numDepositeDetailID 
		--												FROM OPENXML(@hDocItem,'/NewDataSet/Item [numDepositeDetailID>0]',2)                                                                                                              
		--												WITH(numDepositeDetailID NUMERIC(9))X)
					
						
								UPDATE  dbo.DepositeDetails
								SET     [monAmountPaid] = X.monAmountPaid ,
										numOppBizDocsID = X.numOppBizDocsID ,
										numOppID = X.numOppID,
										dtModifiedDate=GETUTCDATE()
								FROM    #temp X
								WHERE   X.numDepositeDetailID = DepositeDetails.numDepositeDetailID
										AND dbo.DepositeDetails.numDepositID = @numDepositId
						END


					UPDATE dbo.OpportunityBizDocs 
					SET dbo.OpportunityBizDocs.monAmountPaid = (SELECT ISNULL(SUM(ISNULL(monAmountPaid,0)),0) FROM DepositeDetails WHERE numOppBizDocsID  = OpportunityBizDocs.numOppBizDocsId AND numOppID  = OpportunityBizDocs.numOppID),numModifiedBy=@numUserCntID
					WHERE  OpportunityBizDocs.numOppBizDocsId IN (SELECT numOppBizDocsId FROM DepositeDetails WHERE numDepositId=@numDepositId)

					--Add to OpportunityAutomationQueue if full Amount Paid	
					INSERT INTO [dbo].[OpportunityAutomationQueue] ([numDomainID], [numOppId], [numOppBizDocsId], [numBizDocStatus], [numCreatedBy], [dtCreatedDate], [tintProcessStatus],numRuleID)
						SELECT OM.numDomainID, OM.numOppId, OBD.numOppBizDocsId, 0, @numUserCntID, GETUTCDATE(), 1,7	
						    FROM OpportunityBizDocs OBD JOIN dbo.OpportunityMaster OM ON OBD.numOppId = OM.numOppId
								WHERE OM.numDomainId=@numDomainId AND OBD.numOppBizDocsId IN (SELECT numOppBizDocsId FROM DepositeDetails WHERE numDepositId=@numDepositId)
								  AND ISNULL(OBD.monDealAmount,0)=ISNULL(OBD.monAmountPaid,0) AND ISNULL(OBD.monAmountPaid,0)>0

					--Add to OpportunityAutomationQueue if Balance due
					INSERT INTO [dbo].[OpportunityAutomationQueue] ([numDomainID], [numOppId], [numOppBizDocsId], [numBizDocStatus], [numCreatedBy], [dtCreatedDate], [tintProcessStatus],numRuleID)
						SELECT OM.numDomainID, OM.numOppId, OBD.numOppBizDocsId, 0, @numUserCntID, GETUTCDATE(), 1,14	
						    FROM OpportunityBizDocs OBD JOIN dbo.OpportunityMaster OM ON OBD.numOppId = OM.numOppId
								WHERE OM.numDomainId=@numDomainId AND OBD.numOppBizDocsId IN (SELECT numOppBizDocsId FROM DepositeDetails WHERE numDepositId=@numDepositId)
								  AND ISNULL(OBD.monDealAmount,0)>ISNULL(OBD.monAmountPaid,0) AND ISNULL(OBD.monAmountPaid,0)>0

														
  					UPDATE DepositMaster SET monAppliedAmount=(SELECT ISNULL(SUM(ISNULL(monAmountPaid,0)),0) FROM DepositeDetails WHERE numDepositId=@numDepositId)
  							WHERE numDepositId=@numDepositId
		  				
		  								
  					DROP TABLE #temp
		  								
				  EXEC sp_xml_removedocument
					@hDocItem
        END
        
        ELSE
        BEGIN
			--IF @tintMode = 0
			--BEGIN
				UPDATE OBD
				SET monAmountPaid = ISNULL(OBD.monAmountPaid,0) - DD.monAmountPaid,OBD.numModifiedBy=@numUserCntID
				FROM OpportunityBizDocs OBD JOIN DepositeDetails DD ON OBD.numOppBizDocsId=DD.numOppBizDocsID
				WHERE DD.numDepositID = @numDepositId 
											
				DELETE FROM dbo.DepositeDetails WHERE numDepositID = @numDepositId
				  
				UPDATE DepositMaster SET monAppliedAmount=(SELECT ISNULL(SUM(ISNULL(monAmountPaid,0)),0) FROM DepositeDetails WHERE numDepositId=@numDepositId)
  					WHERE numDepositId=@numDepositId
  		  
			--END
        END 
    END
               
END
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_Item_GetAll_BIZAPI' ) 
    DROP PROCEDURE USP_Item_GetAll_BIZAPI
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Sandeep Patel
-- Create date: 9 April 2014
-- Description:	Gets list of items domain wise
-- =============================================
CREATE PROCEDURE USP_Item_GetAll_BIZAPI
	@numDomainID NUMERIC(18,0) = NULL,
	@numPageIndex AS INT = 0,
    @numPageSize AS INT = 0,
    @TotalRecords INT OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT @TotalRecords = COUNT(*) FROM Item WHERE Item.numDomainID = @numDomainID

	SELECT
	*
    FROM
	(SELECT
		ROW_NUMBER() OVER (ORDER BY Item.vcItemName asc) AS RowNumber,
		Item.numItemCode,
		Item.vcItemName,
		Item.txtItemDesc,
		Item.charItemType,
		CASE 
			WHEN Item.charItemType='P' THEN 'Inventory Item' 
			WHEN Item.charItemType='N' THEN 'Non Inventory Item' 
			WHEN Item.charItemType='S' THEN 'Service' 
			WHEN Item.charItemType='A' THEN 'Accessory' 
		END AS ItemType,
		Item.vcSKU,
		Item.vcModelID,
		Item.numBarCodeId,
		Item.vcManufacturer,
		Item.vcUnitofMeasure,
		Item.monListPrice,
		Item.monAverageCost,
		Item.monCampaignLabourCost,
		Item.numItemGroup,
		Item.numItemClass,
		Item.numShipClass,
		Item.numItemClassification,
		Item.fltWidth,
		Item.fltWeight,
		Item.fltHeight,
		Item.fltLength,
		Item.numBaseUnit,
		Item.numSaleUnit,
		Item.numPurchaseUnit,
		Item.bitKitParent,
		Item.bitLotNo,
		Item.bitAssembly,
		Item.bitSerialized,
		Item.bitTaxable,
		Item.bitAllowBackOrder,
		Item.bitFreeShipping,
		Item.numCOGsChartAcntId,
		Item.numAssetChartAcntId,
		Item.numIncomeChartAcntId,
		Item.numVendorID,
		SUM(numOnHand) as numOnHand,                      
		SUM(numOnOrder) as numOnOrder,                      
		SUM(numReorder)  as numReorder,                      
		SUM(numAllocation)  as numAllocation,                      
		SUM(numBackOrder)  as numBackOrder,
		(
			SELECT
				*
			FROM
				(
					SELECT
						ItemDetails.numChildItemID,
						ItemDetails.numQtyItemsReq,
						WareHouseItems.numWareHouseItemID,
						Warehouses.numWareHouseID,
						(SELECT vcUnitName FROM UOM WHERE numUOMId = ItemDetails.numUOMId) AS vcUnitName
					FROM
						ItemDetails
					LEFT JOIN  
						WareHouseItems 
					ON
						ItemDetails.numWareHouseItemId = WareHouseItems.numWareHouseItemID
					LEFT JOIN
						Warehouses
					ON
						WareHouseItems.numWareHouseID = Warehouses.numWareHouseID
					WHERE
						ItemDetails.numItemKitID = Item.numItemCode
				)  AS SubItem
				FOR XML AUTO
		) AS SubItems
	FROM
		Item
	LEFT JOIN  
		WareHouseItems W                  
	ON 
		W.numItemID=Item.numItemCode
	WHERE 
		Item.numDomainID = @numDomainID
	GROUP BY
		Item.numItemCode,
		Item.vcItemName,
		Item.txtItemDesc,
		Item.charItemType,
		Item.vcSKU,
		Item.vcModelID,
		Item.numBarCodeId,
		Item.vcManufacturer,
		Item.vcUnitofMeasure,
		Item.monListPrice,
		Item.monAverageCost,
		Item.monCampaignLabourCost,
		Item.numItemGroup,
		Item.numItemClass,
		Item.numShipClass,
		Item.numItemClassification,
		Item.fltWidth,
		Item.fltWeight,
		Item.fltHeight,
		Item.fltLength,
		Item.numBaseUnit,
		Item.numSaleUnit,
		Item.numPurchaseUnit,
		Item.bitKitParent,
		Item.bitLotNo,
		Item.bitAssembly,
		Item.bitSerialized,
		Item.bitTaxable,
		Item.bitAllowBackOrder,
		Item.bitFreeShipping,
		Item.numCOGsChartAcntId,
		Item.numAssetChartAcntId,
		Item.numIncomeChartAcntId,
		Item.numVendorID
		) AS I
WHERE 
	(@numPageIndex = 0 OR @numPageSize = 0) OR
	(RowNumber > ((@numPageIndex - 1) * @numPageSize) and RowNumber < ((@numPageIndex * @numPageSize) + 1))
END
GO


GO
GO
/****** Object:  StoredProcedure [dbo].[USP_ItemDetails]    Script Date: 02/07/2010 15:31:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Anoop Jayaraj                                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_itemdetails')
DROP PROCEDURE usp_itemdetails
GO
CREATE PROCEDURE [dbo].[USP_ItemDetails]                                                  
@numItemCode as numeric(9) ,          
@ClientTimeZoneOffset as int                                               
as                                                 
   
   DECLARE @bitItemIsUsedInOrder AS BIT;SET @bitItemIsUsedInOrder=0
   
   DECLARE @numDomainId AS NUMERIC
   SELECT @numDomainId=numDomainId FROM Item WHERE numItemCode=@numItemCode
   
   IF GETUTCDATE()<'2013-03-15 23:59:39'
		SET @bitItemIsUsedInOrder=0
   ELSE IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityItems OI WHERE OI.numItemCode =@numItemCode) >0
		SET @bitItemIsUsedInOrder=1
   ELSE IF EXISTS(SELECT numItemID FROM dbo.General_Journal_Details WHERE numItemID= @numItemCode and numDomainId=@numDomainId AND chBizDocItems='IA1'   )
		set @bitItemIsUsedInOrder =1
                                               
select I.numItemCode, vcItemName, ISNULL(txtItemDesc,'') txtItemDesc,CAST( ISNULL(IED.txtDesc,'')  AS VARCHAR(MAX) ) vcExtendedDescToAPI, charItemType, monListPrice,                   
numItemClassification, isnull(bitTaxable,0) as bitTaxable, vcSKU AS vcSKU, isnull(bitKitParent,0) as bitKitParent,--, dtDateEntered,                  
 numVendorID, I.numDomainID, numCreatedBy, bintCreatedDate, bintModifiedDate,                   
numModifiedBy,
(SELECT TOP 1 vcPathForImage FROM ItemImages WHERE numItemCode = @numItemCode AND bitDefault = 1 AND bitIsImage = 1 ) AS vcPathForImage ,
(SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = @numItemCode AND bitDefault = 1 AND bitIsImage = 1) AS  vcPathForTImage, isnull(bitSerialized,0) as bitSerialized, vcModelID,                   
(SELECT numItemImageId,vcPathForImage,vcPathForTImage,bitDefault,case when bitdefault=1 then -1 
else isnull(intDisplayOrder,0) end as intDisplayOrder 
FROM ItemImages  
WHERE numItemCode=@numItemCode order by case when bitdefault=1 then -1 else isnull(intDisplayOrder,0) end  asc 
FOR XML AUTO,ROOT('Images')) AS xmlItemImages,
(SELECT STUFF((SELECT ',' + CONVERT(VARCHAR(50) , numCategoryID)  FROM dbo.ItemCategory WHERE numItemID = I.numItemCode FOR XML PATH('')), 1, 1, '')) AS vcItemCategories ,
numItemGroup, numCOGsChartAcntId, numAssetChartAcntId, numIncomeChartAcntId, monAverageCost,                   
monCampaignLabourCost,dbo.fn_GetContactName(numCreatedBy)+' ,'+convert(varchar(20),DateAdd(minute, -@ClientTimeZoneOffset,bintCreatedDate )) as CreatedBy ,                                      
dbo.fn_GetContactName(numModifiedBy)+' ,'+convert(varchar(20),DateAdd(minute, -@ClientTimeZoneOffset,bintModifiedDate )) as ModifiedBy,                      
sum(numOnHand) as numOnHand,                      
sum(numOnOrder) as numOnOrder,                      
sum(numReorder)  as numReorder,                      
sum(numAllocation)  as numAllocation,                      
sum(numBackOrder)  as numBackOrder,                   
isnull(fltWeight,0) as fltWeight,                
isnull(fltHeight,0) as fltHeight,                
isnull(fltWidth,0) as fltWidth,                
isnull(fltLength,0) as fltLength,                
isnull(bitFreeShipping,0) as bitFreeShipping,              
isnull(bitAllowBackOrder,0) as bitAllowBackOrder,              
isnull(vcUnitofMeasure,'Units') as vcUnitofMeasure,      
isnull(bitShowDeptItem,0) bitShowDeptItem,      
isnull(bitShowDeptItemDesc,0) bitShowDeptItemDesc,  
isnull(bitCalAmtBasedonDepItems ,0) bitCalAmtBasedonDepItems,
isnull(bitAssembly ,0) bitAssembly ,
isnull(cast(numBarCodeId as varchar(50)),'') as  numBarCodeId ,
isnull(I.vcManufacturer,'') as vcManufacturer,
ISNULL(I.numBaseUnit,0) AS numBaseUnit,ISNULL(I.numPurchaseUnit,0) AS numPurchaseUnit,ISNULL(I.numSaleUnit,0) AS numSaleUnit,
dbo.fn_GetUOMName(ISNULL(I.numBaseUnit,0)) AS vcBaseUnit,dbo.fn_GetUOMName(ISNULL(I.numPurchaseUnit,0)) AS vcPurchaseUnit,dbo.fn_GetUOMName(ISNULL(I.numSaleUnit,0)) AS vcSaleUnit,
isnull(bitLotNo,0) as bitLotNo,
ISNULL(IsArchieve,0) AS IsArchieve,
case when charItemType='P' then 'Inventory Item' when charItemType='S' then 'Service' when charItemType='A' then 'Accessory' when charItemType='N' then 'Non-Inventory Item' end as ItemType,
isnull(I.numItemClass,0) as numItemClass,
ISNULL(tintStandardProductIDType,0) tintStandardProductIDType,
ISNULL(vcExportToAPI,'') vcExportToAPI,
ISNULL(numShipClass,0) numShipClass,ISNULL(I.bitAllowDropShip,0) AS bitAllowDropShip,
ISNULL(@bitItemIsUsedInOrder,0) AS bitItemIsUsedInOrder,
ISNULL((SELECT COUNT(*) FROM ItemDetails WHERE numChildItemID = I.numItemCode),0) AS intParentKitItemCount,
ISNULL(I.bitArchiveItem,0) AS [bitArchiveItem],
ISNULL(W.numWareHouseID,0) AS [numWareHouseID],
ISNULL(W.numWareHouseItemID,0) AS [numWareHouseItemID],
ISNULL((SELECT COUNT(WI.[numWareHouseItemID]) FROM [dbo].[WareHouseItems] WI WHERE WI.[numItemID] = I.numItemCode AND WI.[numItemID] = @numItemCode),0) AS [intTransCount],
ISNULL(I.bitAsset,0) AS [bitAsset],
ISNULL(I.bitRental,0) AS [bitRental]
FROM Item I       
left join  WareHouseItems W                  
on W.numItemID=I.numItemCode                
LEFT JOIN ItemExtendedDetails IED   ON I.numItemCode = IED.numItemCode               
WHERE I.numItemCode=@numItemCode  
GROUP BY I.numItemCode, vcItemName, txtItemDesc, charItemType, monListPrice,                   
numItemClassification, bitTaxable,vcSKU, bitKitParent,numVendorID, I.numDomainID,               
numCreatedBy, bintCreatedDate, bintModifiedDate,                   
numModifiedBy,bitAllowBackOrder, bitSerialized, vcModelID,                   
numItemGroup, numCOGsChartAcntId, numAssetChartAcntId, numIncomeChartAcntId, monAverageCost,                   
monCampaignLabourCost,fltWeight,fltHeight,fltWidth,fltLength,bitFreeShipping,vcUnitofMeasure,bitShowDeptItemDesc,bitShowDeptItem,bitCalAmtBasedonDepItems,bitAssembly,numBarCodeId,I.vcManufacturer
,I.numBaseUnit,I.numPurchaseUnit,I.numSaleUnit,I.bitLotNo,I.IsArchieve,I.numItemClass,I.tintStandardProductIDType,I.vcExportToAPI,I.numShipClass,
CAST( ISNULL(IED.txtDesc,'')  AS VARCHAR(MAX) ),I.bitAllowDropShip,I.bitArchiveItem,I.bitAsset,I.bitRental,W.[numWarehouseID],W.numWareHouseItemID


--exec USP_ItemDetails @numItemCode = 197605 ,@ClientTimeZoneOffset =330


/*
declare @p7 int
set @p7=0
exec USP_ItemList1 @ItemClassification=0,@KeyWord='',@IsKit=0,@SortChar='0',@CurrentPage=1,@PageSize=10,@TotRecs=@p7 
output,@columnName='vcItemName',@columnSortOrder='Asc',@numDomainID=1,@ItemType=' ',@bitSerialized=0,@numItemGroup=0,@bitAssembly=0,@numUserCntID=1,@Where='',@IsArchive=0
select @p7
*/

GO
/****** Object:  StoredProcedure [dbo].[USP_ItemList1]    Script Date: 05/07/2009 18:14:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Created By Anoop Jayaraj                                        
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_itemlist1' ) 
    DROP PROCEDURE usp_itemlist1
GO
CREATE PROCEDURE [dbo].[USP_ItemList1]
    @ItemClassification AS NUMERIC(9) = 0,
    @KeyWord AS VARCHAR(1000) = '',
    @IsKit AS TINYINT,
    @SortChar CHAR(1) = '0',
    @CurrentPage INT,
    @PageSize INT,
    @TotRecs INT OUTPUT, 
    @columnName AS VARCHAR(50),
    @columnSortOrder AS VARCHAR(10),
    @numDomainID AS NUMERIC(9) = 0,
    @ItemType AS CHAR(1),
    @bitSerialized AS BIT,
    @numItemGroup AS NUMERIC(9),
    @bitAssembly AS BIT,
    @numUserCntID AS NUMERIC(9),
    @Where	AS VARCHAR(MAX),
    @IsArchive AS BIT = 0,
    @byteMode AS TINYINT,
	@bitAsset as BIT=0,
	@bitRental as BIT=0
AS 
	
	 DECLARE @Nocolumns AS TINYINT               
    SET @Nocolumns = 0                
 
    SELECT  @Nocolumns = ISNULL(SUM(TotalRow), 0)
    FROM    ( SELECT    COUNT(*) TotalRow
              FROM      View_DynamicColumns
              WHERE     numFormId = 21
                        AND numUserCntID = @numUserCntID
                        AND numDomainID = @numDomainID
                        AND tintPageType = 1 AND ISNULL(numRelCntType,0)=0
              UNION
              SELECT    COUNT(*) TotalRow
              FROM      View_DynamicCustomColumns
              WHERE     numFormId = 21
                        AND numUserCntID = @numUserCntID
                        AND numDomainID = @numDomainID
                        AND tintPageType = 1 AND ISNULL(numRelCntType,0)=0
            ) TotalRows
               
 
 if @Nocolumns=0
BEGIN
INSERT INTO DycFormConfigurationDetails (numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth,numViewID)
select 21,numFieldId,0,Row_number() over(order by tintRow desc),@numDomainID,@numUserCntID,0,1,0,intColumnWidth,0
 FROM    View_DynamicDefaultColumns
                    WHERE   numFormId = 21
                            AND bitDefault = 1
                            AND ISNULL(bitSettingField, 0) = 1
                            AND numDomainID = @numDomainID
                    ORDER BY tintOrder ASC 
END

    CREATE TABLE #tempForm
        (
          tintOrder TINYINT,
          vcDbColumnName NVARCHAR(50),
          vcFieldName NVARCHAR(50),
          vcAssociatedControlType NVARCHAR(50),
          vcListItemType CHAR(3),
          numListID NUMERIC(9),
          vcLookBackTableName VARCHAR(50),
          bitCustomField BIT,
          numFieldId NUMERIC,
          bitAllowSorting BIT,
          bitAllowEdit BIT,
          bitIsRequired BIT,
          bitIsEmail BIT,
          bitIsAlphaNumeric BIT,
          bitIsNumeric BIT,
          bitIsLengthValidation BIT,
          intMaxLength INT,
          intMinLength INT,
          bitFieldMessage BIT,
          vcFieldMessage VARCHAR(500),
          ListRelID NUMERIC(9)
        )


          

            INSERT  INTO #tempForm
                    SELECT  tintRow + 1 AS tintOrder,
                            vcDbColumnName,
                            ISNULL(vcCultureFieldName, vcFieldName),
                            vcAssociatedControlType,
                            vcListItemType,
                            numListID,
                            vcLookBackTableName,
                            bitCustom,
                            numFieldId,
                            bitAllowSorting,
                            bitAllowEdit,
                            bitIsRequired,
                            bitIsEmail,
                            bitIsAlphaNumeric,
                            bitIsNumeric,
                            bitIsLengthValidation,
                            intMaxLength,
                            intMinLength,
                            bitFieldMessage,
                            vcFieldMessage vcFieldMessage,
                            ListRelID
                    FROM    View_DynamicColumns
                    WHERE   numFormId = 21
                            AND numUserCntID = @numUserCntID
                            AND numDomainID = @numDomainID
                            AND tintPageType = 1
                            AND ISNULL(bitSettingField, 0) = 1
                            AND ISNULL(bitCustom, 0) = 0
                            AND ISNULL(numRelCntType,0)=0
                    UNION
                    SELECT  tintRow + 1 AS tintOrder,
                            vcDbColumnName,
                            vcFieldName,
                            vcAssociatedControlType,
                            '' AS vcListItemType,
                            numListID,
                            '',
                            bitCustom,
                            numFieldId,
                            bitAllowSorting,
                            bitAllowEdit,
                            bitIsRequired,
                            bitIsEmail,
                            bitIsAlphaNumeric,
                            bitIsNumeric,
                            bitIsLengthValidation,
                            intMaxLength,
                            intMinLength,
                            bitFieldMessage,
                            vcFieldMessage,
                            ListRelID
                    FROM    View_DynamicCustomColumns
                    WHERE   numFormId = 21
                            AND numUserCntID = @numUserCntID
                            AND numDomainID = @numDomainID
                            AND tintPageType = 1
                            AND ISNULL(bitCustom, 0) = 1
                            AND ISNULL(numRelCntType,0)=0
                    ORDER BY tintOrder ASC      
            
    
            
	IF @byteMode=0
	BEGIN
		
	IF @columnName = 'OnHand' 
        SET @columnName = 'numOnHand'
    ELSE IF @columnName = 'Backorder' 
        SET @columnName = 'numBackOrder'
    ELSE IF @columnName = 'OnOrder' 
        SET @columnName = 'numOnOrder'
    ELSE IF @columnName = 'OnAllocation' 
        SET @columnName = 'numAllocation'
    ELSE IF @columnName = 'Reorder' 
        SET @columnName = 'numReorder'
    ELSE IF @columnName = 'monStockValue' 
        SET @columnName = 'sum(numOnHand) * Isnull(monAverageCost,0)'
    ELSE IF @columnName = 'ItemType' 
        SET @columnName = 'charItemType'

  
    DECLARE @column AS VARCHAR(50)              
    SET @column = @columnName              
    DECLARE @join AS VARCHAR(400)                  
    SET @join = ''           
    IF @columnName LIKE '%Cust%' 
        BEGIN                
			DECLARE @CustomFieldType AS VARCHAR(10)
			DECLARE @fldId AS VARCHAR(10)
			
			SET @fldId = REPLACE(@columnName, 'Cust', '')
			SELECT TOP 1 @CustomFieldType = ISNULL(vcAssociatedControlType,'') FROM View_DynamicCustomColumns WHERE numFieldID = @fldId
            
            IF ISNULL(@CustomFieldType,'') = 'SelectBox' OR ISNULL(@CustomFieldType,'') = 'ListBox'
				BEGIN
					SET @join = @join + ' left Join CFW_FLD_Values_Item CFW on CFW.RecId=item.numItemCode and CFW.fld_id= ' + @fldId + ' ' 
					SET @join = @join + ' left Join ListDetails LstCF on LstCF.numListItemID = CFW.Fld_Value  '            
					SET @columnName = ' LstCF.vcData ' 	
				END
			ELSE
				BEGIN
					SET @join = ' left Join CFW_FLD_Values_Item CFW on CFW.RecId=item.numItemCode and CFW.fld_id= ' + @fldId + ' '                                                         
					SET @columnName = 'CFW.Fld_Value'                	
				END
        END                                                
           
                                      
    DECLARE @firstRec AS INTEGER                                        
    DECLARE @lastRec AS INTEGER                                        
    SET @firstRec = ( @CurrentPage - 1 ) * @PageSize                                        
    SET @lastRec = ( @CurrentPage * @PageSize + 1 )   

    DECLARE @bitLocation AS BIT  
    SELECT  @bitLocation = ( CASE WHEN COUNT(*) > 0 THEN 1
                                  ELSE 0
                             END )
    FROM    View_DynamicColumns
    WHERE   numFormId = 21
            AND numUserCntID = @numUserCntID
            AND numDomainID = @numDomainID
            AND tintPageType = 1
            AND bitCustom = 0
            AND ISNULL(bitSettingField, 0) = 1
            AND vcDbColumnName = 'vcWareHouse'
                                                                   
                                        
    DECLARE @strSql AS VARCHAR(8000)
    DECLARE @strWhere AS VARCHAR(8000)
    SET @strWhere = ' AND 1=1'
    
      SET @strSql = '
declare @numDomain numeric
set @numDomain = ' + CONVERT(VARCHAR(15), @numDomainID) + ';
    INSERT INTO #tempItemList select numItemCode'
  
--    SET @strSql = @strSql + ' from item                     
        
        
    IF @bitAssembly = '1' 
        SET @strWhere = @strWhere + ' and bitAssembly= ' + CONVERT(VARCHAR(2), @bitAssembly) + ''                                    
        
    ELSE 
        IF @IsKit = '1' 
            SET @strWhere = @strWhere + ' and (bitAssembly=0 or  bitAssembly is null) and bitKitParent= ' + CONVERT(VARCHAR(2), @IsKit) + ''               

    IF @ItemType = 'R' 
        SET @strWhere = @strWhere + ' and numItemClassification=(select numRentalItemClass from domain where numDomainId=' + CONVERT(VARCHAR(20), @numDomainID) + ')'            
    ELSE 
        IF @ItemType <> ''  
            SET @strWhere = @strWhere + ' and charItemType= ''' + @ItemType + ''''            

    IF @bitSerialized = 1 
        SET @strWhere = @strWhere + ' and (bitSerialized=1 or bitLotNo=1)'                                                   
        
    IF @SortChar <> '0' 
        SET @strWhere = @strWhere + ' and vcItemName like ''' + @SortChar + '%'''                                       
        
    IF @ItemClassification <> '0' 
        SET @strWhere = @strWhere + ' and numItemClassification=' + CONVERT(VARCHAR(15), @ItemClassification)    
    
    IF @IsArchive = 0 
        SET @strWhere = @strWhere + ' AND ISNULL(Item.IsArchieve,0) = 0'       
		
	IF @bitAsset = 0 
		BEGIN
			SET @strWhere = @strWhere + ' AND ISNULL(Item.bitAsset,0) = 0'
		END
    ELSE
		  BEGIN
			  SET @strWhere = @strWhere + ' AND (ISNULL(Item.bitAsset,0) = 1)  OR  (ISNULL(Item.bitRental,0) = 1)'
		  END

	IF @bitRental = 0 
			BEGIN
				SET @strWhere = @strWhere + ' AND ISNULL(Item.bitRental,0) = 0'
			END
	 ELSE
			  BEGIN
				  SET @strWhere = @strWhere + ' AND ISNULL(Item.bitRental,0) = 1'
			  END
		  
		

    IF @KeyWord <> '' 
        BEGIN 
            IF CHARINDEX('vcCompanyName', @KeyWord) > 0 
                BEGIN
                    SET @strWhere = @strWhere + ' and item.numItemCode in (select Vendor.numItemCode from Vendor join 
											  divisionMaster div on div.numdivisionid=Vendor.numVendorid 
											  join companyInfo com  on com.numCompanyID=div.numCompanyID  
											  WHERE Vendor.numDomainID=' + CONVERT(VARCHAR(15), @numDomainID) + '  
											  and Vendor.numItemCode= item.numItemCode and com.' + @KeyWord + ')' 
											  
					SET @join = @join + ' LEFT JOIN divisionmaster div on numVendorid=div.numDivisionId '   		
                END
            ELSE 
                SET @strWhere = @strWhere + ' and ' + @KeyWord 
        END
        
    IF @numItemGroup > 0 
        SET @strWhere = @strWhere + ' and numItemGroup = ' + CONVERT(VARCHAR(20), @numItemGroup)    
               
    IF @numItemGroup = -1 
        SET @strWhere = @strWhere + ' and numItemGroup in (select numItemGroupID from ItemGroups where numDomainID=' + CONVERT(VARCHAR(20), @numDomainID) + ')'                                      
        
    SET @strWhere = @strWhere + ISNULL(@Where,'') + ' group by numItemCode '  
  
    IF @bitLocation = 1 
        SET @strWhere = @strWhere + '  ,vcWarehouse'
        
        SET @join = @join + ' LEFT JOIN WareHouseItems ON numItemID = numItemCode '
        SET @join = @join + ' LEFT JOIN Warehouses W ON W.numWareHouseID = WareHouseItems.numWareHouseID '
        
    IF @columnName <> 'sum(numOnHand) * Isnull(monAverageCost,0)' 
        BEGIN
            SET @strWhere = @strWhere + ',' + @columnName   
            
            IF CHARINDEX('LEFT JOIN WareHouseItems',@join) = 0
            BEGIN
				SET @join = @join + ' LEFT JOIN WareHouseItems ON numItemID = numItemCode '
			END	
        END
    
  
    SET @strSql = @strSql + ' FROM Item  ' + @join + '   
	LEFT JOIN ListDetails LD ON LD.numListItemID = Item.numShipClass
	LEFT JOIN ItemCategory IC ON IC.numItemID = Item.numItemCode
	WHERE Item.numDomainID= @numDomain
	AND ( WareHouseItems.numDomainID = ' + CONVERT(VARCHAR(15), @numDomainID) + ' OR WareHouseItems.numWareHouseItemID IS NULL) ' + @strWhere
    
    SET @strSql = @strSql + ' ORDER BY ' + @columnName + ' ' + @columnSortOrder 
 
	PRINT @strSql

	CREATE TABLE #tempItemList (RowNo INT IDENTITY(1,1) NOT NULL,numItemCode NUMERIC(18,0))
	 EXEC (@strSql)
	DELETE FROM #tempItemList WHERE RowNo NOT IN (SELECT MIN(RowNo) FROM #tempItemList GROUP BY numItemCode)

	CREATE TABLE #tempItemList1 (RunningCount INT IDENTITY(1,1) NOT NULL,numItemCode NUMERIC(18,0),TotalRowCount INT)
	INSERT INTO #tempItemList1 (numItemCode) SELECT numItemCode FROM #tempItemList
	UPDATE #tempItemList1 SET TotalRowCount=(SELECT COUNT(*) FROM #tempItemList1)
	
	SET @strSql = ' select                    
 min(RunningCount) as RunningCount,
 min(TotalRowCount) as TotalRowCount,I.numItemCode,I.vcItemName,I.txtItemDesc,I.charItemType,                                     
case when charItemType=''P'' then ''Inventory Item'' when charItemType=''N'' then ''Non Inventory Item'' when charItemType=''S'' then ''Service'' when charItemType=''A'' then ''Accessory'' end as ItemType,                                      
CASE WHEN bitKitParent = 1 THEN ISNULL(dbo.fn_GetKitInventory(I.numItemCode),0) ELSE isnull(sum(numOnHand),0) END  as numOnHand,                  
isnull(sum(numOnOrder),0) as numOnOrder,                                      
isnull(sum(numReorder),0) as numReorder,                  
isnull(sum(numBackOrder),0) as numBackOrder,                  
isnull(sum(numAllocation),0) as numAllocation,                  
vcCompanyName,  
CAST(ISNULL(monAverageCost,0) AS DECIMAL(10,2)) AS monAverageCost ,
CAST(ISNULL(monAverageCost,0) * SUM(numOnHand) AS DECIMAL(10,2)) AS monStockValue,
I.vcModelID,I.monListPrice,I.vcManufacturer,I.numBarCodeId,I.fltLength,I.fltWidth,I.fltHeight,I.fltWeight,I.vcSKU,LD.vcData AS numShipClass ' 

   IF @bitLocation = 1 
        SET @strSql = @strSql + '  ,vcWarehouse'  

SET @strSql = @strSql + ' INTO #tblItem FROM #tempItemList1 as tblItem JOIN ITEM I ON tblItem.numItemCode=I.numItemCode 
left join WareHouseItems on numItemID=I.numItemCode                                  
left join divisionmaster div  on I.numVendorid=div.numDivisionId                                    
left join companyInfo com on com.numCompanyid=div.numcompanyID   
left join Warehouses W  on W.numWareHouseID=WareHouseItems.numWareHouseID 
LEFT JOIN ListDetails LD ON LD.numListItemID = i.numShipClass
    where  RunningCount >' + CONVERT(VARCHAR(15), @firstRec)
        + ' and RunningCount < ' + CONVERT(VARCHAR(15), @lastRec)
          + ' group by I.numItemCode,vcItemName,txtItemDesc,charItemType,vcCompanyName,vcModelID,monListPrice,vcManufacturer,numBarCodeId,fltLength,fltWidth,fltHeight,fltWeight,monAverageCost,I.vcSKU,LD.vcData,bitKitParent '  
          
              IF @bitLocation = 1 
        SET @strSql = @strSql + '  ,vcWarehouse'  
        
        SET @strSql = @strSql + ' order by RunningCount '   
          
    DECLARE @tintOrder AS TINYINT                                                  
    DECLARE @vcFieldName AS VARCHAR(50)                                                  
    DECLARE @vcListItemType AS VARCHAR(3)                                             
    DECLARE @vcListItemType1 AS VARCHAR(1)                                                 
    DECLARE @vcAssociatedControlType VARCHAR(10)                                                  
    DECLARE @numListID AS NUMERIC(9)                                                  
    DECLARE @vcDbColumnName VARCHAR(20)                      
    DECLARE @WhereCondition VARCHAR(2000)                       
    DECLARE @vcLookBackTableName VARCHAR(2000)                
    DECLARE @bitCustom AS BIT                  
    DECLARE @numFieldId AS NUMERIC  
    DECLARE @bitAllowSorting AS CHAR(1)   
    DECLARE @bitAllowEdit AS CHAR(1)                   
                 
    SET @tintOrder = 0                                                  
    SET @WhereCondition = ''                 
                   
   
   
    SET @strSql = @strSql
        + ' select TotalRowCount,RunningCount,temp.numItemCode,vcItemName,txtItemDesc,charItemType,ItemType,numOnHand, LD.vcData AS numShipClass,                  
				   numOnOrder,numReorder,numBackOrder,numAllocation,vcCompanyName,vcModelID,monListPrice,vcManufacturer,numBarCodeId,fltLength,fltWidth,fltHeight,
				   fltWeight,(Select SUM(WO.numQtyItemsReq) from WorkOrder WO where WO.numItemCode=temp.numItemCode and WO.numWOStatus=0) as WorkOrder,
				   monAverageCost, monStockValue,vcSKU'     
  
    IF @bitLocation = 1 
        SET @strSql = @strSql + '  ,vcWarehouse'                                               

    DECLARE @ListRelID AS NUMERIC(9) 
  
    SELECT TOP 1
            @tintOrder = tintOrder + 1,
            @vcDbColumnName = vcDbColumnName,
            @vcFieldName = vcFieldName,
            @vcAssociatedControlType = vcAssociatedControlType,
            @vcListItemType = vcListItemType,
            @numListID = numListID,
            @vcLookBackTableName = vcLookBackTableName,
            @bitCustom = bitCustomField,
            @numFieldId = numFieldId,
            @bitAllowSorting = bitAllowSorting,
            @bitAllowEdit = bitAllowEdit,
            @ListRelID = ListRelID
    FROM    #tempForm --WHERE bitCustomField=1
    ORDER BY tintOrder ASC            

    WHILE @tintOrder > 0                                                  
        BEGIN                                                  
            IF @bitCustom = 0
               BEGIN
               PRINT @vcDbColumnName
			   		IF @vcDbColumnName = 'vcPathForTImage'
		   			BEGIN
					   	SET @strSql = @strSql + ',ISNULL(II.vcPathForTImage,'''') AS [vcPathForTImage]'                   
                        SET @WhereCondition = @WhereCondition + ' LEFT JOIN ItemImages II ON II.numItemCode = temp.numItemCode AND bitDefault = 1'
					END
			   END                              
			   
            ELSE IF @bitCustom = 1 
                BEGIN      
                  
                    SELECT  @vcFieldName = FLd_label,
                            @vcAssociatedControlType = fld_type,
                            @vcDbColumnName = 'Cust'
                            + CONVERT(VARCHAR(10), Fld_Id)
                    FROM    CFW_Fld_Master
                    WHERE   CFW_Fld_Master.Fld_Id = @numFieldId                 
     
              
--    print @vcAssociatedControlType                
                    IF @vcAssociatedControlType = 'TextBox'
                        OR @vcAssociatedControlType = 'TextArea' 
                        BEGIN                
                   
                            SET @strSql = @strSql + ',CFW'  + CONVERT(VARCHAR(3), @tintOrder)  + '.Fld_Value  [' + @vcFieldName + '~' + @vcDbColumnName + ']'                   
                            SET @WhereCondition = @WhereCondition + ' left Join CFW_FLD_Values_Item CFW' + CONVERT(VARCHAR(3), @tintOrder)
                                + '                 
    on CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
                                + CONVERT(VARCHAR(10), @numFieldId)
                                + 'and CFW' + CONVERT(VARCHAR(3), @tintOrder)
                                + '.RecId=temp.numItemCode   '                                                         
                        END   
                    ELSE 
                        IF @vcAssociatedControlType = 'CheckBox' 
                            BEGIN            
               
                                SET @strSql = @strSql
                                    + ',case when isnull(CFW'
                                    + CONVERT(VARCHAR(3), @tintOrder)
                                    + '.Fld_Value,0)=0 then ''No'' when isnull(CFW'
                                    + CONVERT(VARCHAR(3), @tintOrder)
                                    + '.Fld_Value,0)=1 then ''Yes'' end   ['
                                    + @vcFieldName + '~' + @vcDbColumnName
                                    + ']'              
 
                                SET @WhereCondition = @WhereCondition
                                    + ' left Join CFW_FLD_Values_Item CFW'
                                    + CONVERT(VARCHAR(3), @tintOrder)
                                    + '             
    on CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
                                    + CONVERT(VARCHAR(10), @numFieldId)
                                    + 'and CFW'
                                    + CONVERT(VARCHAR(3), @tintOrder)
                                    + '.RecId=temp.numItemCode   '                                                     
                            END                
                        ELSE 
                            IF @vcAssociatedControlType = 'DateField' 
                                BEGIN              
                   
                                    SET @strSql = @strSql
                                        + ',dbo.FormatedDateFromDate(CFW'
                                        + CONVERT(VARCHAR(3), @tintOrder)
                                        + '.Fld_Value,'
                                        + CONVERT(VARCHAR(10), @numDomainId)
                                        + ')  [' + @vcFieldName + '~'
                                        + @vcDbColumnName + ']'                   
                                    SET @WhereCondition = @WhereCondition
                                        + ' left Join CFW_FLD_Values_Item CFW'
                                        + CONVERT(VARCHAR(3), @tintOrder)
                                        + '                 
    on CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
                                        + CONVERT(VARCHAR(10), @numFieldId)
                                        + 'and CFW'
                                        + CONVERT(VARCHAR(3), @tintOrder)
                                        + '.RecId=temp.numItemCode   '                                                         
                                END                
                            ELSE 
                                IF @vcAssociatedControlType = 'SelectBox' 
                                    BEGIN                
                                        SET @vcDbColumnName = 'Cust'
                                            + CONVERT(VARCHAR(10), @numFieldId)                
                                        SET @strSql = @strSql + ',L'
                                            + CONVERT(VARCHAR(3), @tintOrder)
                                            + '.vcData' + ' [' + @vcFieldName
                                            + '~' + @vcDbColumnName + ']'                                                          
                                        SET @WhereCondition = @WhereCondition
                                            + ' left Join CFW_FLD_Values_Item CFW'
                                            + CONVERT(VARCHAR(3), @tintOrder)
                                            + '                 
     on CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
                                            + CONVERT(VARCHAR(10), @numFieldId)
                                            + 'and CFW'
                                            + CONVERT(VARCHAR(3), @tintOrder)
                                            + '.RecId=temp.numItemCode    '                                                         
                                        SET @WhereCondition = @WhereCondition
                                            + ' left Join ListDetails L'
                                            + CONVERT(VARCHAR(3), @tintOrder)
                                            + ' on L'
                                            + CONVERT(VARCHAR(3), @tintOrder)
                                            + '.numListItemID=CFW'
                                            + CONVERT(VARCHAR(3), @tintOrder)
                                            + '.Fld_Value'                
                                    END                 
                END          
  
            SELECT TOP 1
                    @tintOrder = tintOrder + 1,
                    @vcDbColumnName = vcDbColumnName,
                    @vcFieldName = vcFieldName,
                    @vcAssociatedControlType = vcAssociatedControlType,
                    @vcListItemType = vcListItemType,
                    @numListID = numListID,
                    @vcLookBackTableName = vcLookBackTableName,
                    @bitCustom = bitCustomField,
                    @numFieldId = numFieldId,
                    @bitAllowSorting = bitAllowSorting,
                    @bitAllowEdit = bitAllowEdit,
                    @ListRelID = ListRelID
            FROM    #tempForm
            WHERE   tintOrder > @tintOrder - 1 --AND bitCustomField=1
            ORDER BY tintOrder ASC            
 
            IF @@rowcount = 0 
                SET @tintOrder = 0 
            
        END                       
      
    PRINT @bitLocation
    SET @strSql = @strSql + ' from #tblItem temp
			LEFT JOIN ListDetails LD ON LD.vcData = temp.numShipClass AND LD.numListID=461 and LD.numDomainID = '  + CONVERT(VARCHAR(20), @numDomainID) + ' '
		+ @WhereCondition
        + ' where  RunningCount >' + CONVERT(VARCHAR(15), @firstRec)
        + ' and RunningCount < ' + CONVERT(VARCHAR(15), @lastRec)
        + ' order by RunningCount'   

    SET @strSql = REPLACE(@strSql, '|', ',') 
    PRINT @strSql
    EXEC ( @strSql)  

    
                              
 
DROP TABLE #tempItemList
DROP TABLE #tempItemList1

END

UPDATE  #tempForm
    SET     vcDbColumnName = CASE WHEN bitCustomField = 1
                                  THEN vcFieldName + '~' + vcDbColumnName
                                  ELSE vcDbColumnName END
                                  
   SELECT  * FROM #tempForm

    DROP TABLE #tempForm
/****** Object:  StoredProcedure [dbo].[USP_ItemPricingRecomm]    Script Date: 02/19/2009 01:33:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- exec USP_ItemPricingRecomm @numItemCode=173028,@units=1,@numOppID=0,@numDivisionID=7052,@numDomainID=72,@tintOppType=1,@CalPrice=$50,@numWareHouseItemID=130634,@bitMode=0
--created by anoop jayaraj                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_itempricingrecomm')
DROP PROCEDURE usp_itempricingrecomm
GO
CREATE PROCEDURE [dbo].[USP_ItemPricingRecomm]                                        
@numItemCode as numeric(9),                                        
@units integer,                                  
@numOppID as numeric(9)=0,                                
@numDivisionID as numeric(9)=0,                  
@numDomainID as numeric(9)=0,            
@tintOppType as tinyint,          
@CalPrice as money,      
@numWareHouseItemID as numeric(9),
@bitMode tinyint=0
as                 
BEGIN TRY

--      SELECT 1/0 ; --Divide by zero error encountered.
      
declare @numRelationship as numeric(9)                  
declare @numProfile as numeric(9)                  
declare @monListPrice MONEY
DECLARE @bitCalAmtBasedonDepItems bit
DECLARE @numDefaultSalesPricing TINYINT

/*Profile and relationship id */
select @numRelationship=numCompanyType,@numProfile=vcProfile from DivisionMaster D                  
join CompanyInfo C on C.numCompanyId=D.numCompanyID                  
where numDivisionID =@numDivisionID       

          
            
if @tintOppType=1            
begin            

SELECT @bitCalAmtBasedonDepItems=ISNULL(bitCalAmtBasedonDepItems,0) FROM item WHERE numItemCode = @numItemCode
      


/*Get List Price for item*/      
if ((@numWareHouseItemID>0) and exists(select * from item where numItemCode=@numItemCode and bitSerialized=0 and  charItemType='P'))      
begin      
	select @monListPrice=isnull(monWListPrice,0) from WareHouseItems where numWareHouseItemID=@numWareHouseItemID      
    if @monListPrice=0 select @monListPrice=monListPrice from Item where numItemCode=@numItemCode       
end      
else      
begin      
	 select @monListPrice=monListPrice from Item where numItemCode=@numItemCode      
end      
print @monListPrice   
/*Use Calculated Price When item is Assembly/Kit,Calculate price based on sum of dependent items.*/
IF @bitCalAmtBasedonDepItems = 1 
BEGIN
	
	SELECT  
			@CalPrice = ISNULL(  SUM(ISNULL(CASE WHEN I.[charItemType]='P' 
			THEN CASE WHEN I.bitSerialized = 1 THEN I.monListPrice ELSE WI.[monWListPrice] END 
			ELSE monListPrice END,0) * ID.[numQtyItemsReq] )  ,0)
	FROM    [ItemDetails] ID
			INNER JOIN [Item] I ON ID.[numChildItemID] = I.[numItemCode]
			left join  WareHouseItems WI
			on WI.numItemID=I.numItemCode and WI.[numWareHouseItemID]=ID.[numWareHouseItemId]
	WHERE   [numItemKitID] = @numItemCode
	
END


SELECT @numDefaultSalesPricing = ISNULL(numDefaultSalesPricing,2) FROM Domain WHERE numDomainID = @numDomainID

IF @numDefaultSalesPricing = 1 -- Use Price Level
BEGIN
	DECLARE @newPrice MONEY
	DECLARE @finalUnitPrice FLOAT = 0
	DECLARE @tintRuleType INT
	DECLARE @tintDiscountType INT
	DECLARE @decDiscount FLOAT
	DECLARE @ItemPrice FLOAT

	SET @tintRuleType = 0
	SET @tintDiscountType = 0
	SET @decDiscount  = 0
	SET @ItemPrice = 0

	SELECT TOP 1
		@tintRuleType = tintRuleType,
		@tintDiscountType = tintDiscountType,
		@decDiscount = decDiscount
	FROM 
		PricingTable 
	WHERE 
		numItemCode = @numItemCode AND
		(@units BETWEEN intFromQty AND intToQty)

	IF @tintRuleType > 0 AND @tintDiscountType > 0
	BEGIN	
		IF @tintRuleType = 1 -- Deduct from List price
		BEGIN
		IF (SELECT ISNULL(charItemType,'') FROM Item WHERE numDomainID = @numDomainID AND numItemCode = @numItemCode) = 'P'
			If (SELECT COUNT(*) FROM Item WHERE numDomainID = @numDomainID AND numItemCode = @numItemCode AND (bitKitParent = 1 OR bitAssembly = 1)) > 0 
				SELECT @ItemPrice = dbo.GetUnitPriceForKitOrAssembly(@numDomainID,@numItemCode,@numWarehouseItemID,@units)
			ELSE
				SELECT @ItemPrice = ISNULL(monWListPrice,0) FROM WareHouseItems WHERE numDomainID = @numDomainID AND numItemID = @numItemCode AND numWareHouseItemID = @numWarehouseItemID
		ELSE
			SELECT @ItemPrice = ISNULL(monListPrice,0) FROM Item WHERE numDomainID = @numDomainID AND numItemCode = @numItemCode

		IF @tintDiscountType = 1 -- Percentage
		BEGIN
			IF @decDiscount > 0
				SELECT @finalUnitPrice = @ItemPrice - (@ItemPrice * (@decDiscount/100))
		END
		ELSE IF @tintDiscountType = 2 -- Flat Amount
		BEGIN
				SELECT @finalUnitPrice = @ItemPrice - @decDiscount
		END
	END
		ELSE IF @tintRuleType = 2 -- Add to Primary Vendor Cost
		BEGIN
			If (SELECT COUNT(*) FROM Item WHERE numDomainID = @numDomainID AND numItemCode = @numItemCode AND (bitKitParent = 1 OR bitAssembly = 1)) > 0 
				SELECT @ItemPrice = dbo.GetUnitPriceForKitOrAssembly(@numDomainID,@numItemCode,@numWarehouseItemID,@units)
			ELSE
				SELECT @ItemPrice = ISNULL(monCost,0) FROM Vendor WHERE numItemCode = @numItemCode AND numVendorID = (SELECT numVendorID FROM Item WHERE numItemCode = @numItemCode)

			If @ItemPrice > 0
			BEGIN
				IF @tintDiscountType = 1 -- Percentage
				BEGIN
					IF @decDiscount > 0
						SELECT @finalUnitPrice = @ItemPrice + (@ItemPrice * (@decDiscount/100))
				END
				ELSE IF @tintDiscountType = 2 -- Flat Amount
				BEGIN
					SELECT @finalUnitPrice = @ItemPrice + @decDiscount
				END
			END
		END
		ELSE IF @tintRuleType = 3 -- Named Price
		BEGIN
			SELECT @finalUnitPrice = @decDiscount
			SET @tintDiscountType = 2

			If @monListPrice > 0 AND @monListPrice >= @finalUnitPrice
			BEGIN
				SET @decDiscount = @monListPrice - @finalUnitPrice
			END
		END
	END

	Print @finalUnitPrice

	IF @finalUnitPrice = 0
		SET @newPrice = @monListPrice
	ELSE
		SET @newPrice = @finalUnitPrice

	If @tintRuleType = 2
	BEGIN
		IF @finalUnitPrice > 0
		BEGIN
			IF @tintDiscountType = 1 -- Percentage
			BEGIN
				IF @newPrice > @monListPrice
					SET @decDiscount = 0
				ELSE
					If @monListPrice > 0
						SET @decDiscount = ((@monListPrice - @newPrice) * 100) / CAST(@monListPrice AS FLOAT)
					ELSE
						SET @decDiscount = 0
			END
			ELSE IF @tintDiscountType = 2 -- Flat Amount
			BEGIN
				If @newPrice > @monListPrice
					SET @decDiscount = 0
				ELSE
					SET @decDiscount = @monListPrice - @newPrice
			END
		END
		ELSE
		BEGIN
			SET @decDiscount = 0
			SET @tintDiscountType = 0
		END
		
	END

	SELECT 
		1 as numUintHour, 
		@newPrice AS ListPrice,
		'' AS vcPOppName,
		'' AS bintCreatedDate,
		CASE 
			WHEN @tintRuleType = 0 THEN 'Price - List price'
            ELSE 
               CASE @tintRuleType
               WHEN 1 
					THEN 
						'Deduct from List price ' +  CASE 
													   WHEN @tintDiscountType = 1 THEN CONVERT(VARCHAR(20), @decDiscount) + '%'
													   ELSE 'flat ' + CONVERT(VARCHAR(20), CAST(@decDiscount AS int))
													   END 
               WHEN 2 THEN 'Add to primary vendor cost '  +  CASE 
													   WHEN @tintDiscountType = 1 THEN CONVERT(VARCHAR(20), @decDiscount) + '%'
													   ELSE 'flat ' + CONVERT(VARCHAR(20), CAST(@decDiscount AS int))
													   END 
               ELSE 'Flat ' + CONVERT(VARCHAR(20), @decDiscount)
               END
        END AS OppStatus,
		CAST(0 AS NUMERIC) AS numPricRuleID,
		CAST(1 AS TINYINT) AS tintPricingMethod,
		CAST(@tintDiscountType AS TINYINT) AS tintDiscountTypeOriginal,
		CAST(@decDiscount AS FLOAT) AS decDiscountOriginal,
		CAST(@tintRuleType AS TINYINT) AS tintRuleType
END
ELSE -- Use Price Rule
BEGIN
/* Checks Pricebook if exist any.. */      
SELECT TOP 1 1 AS numUnitHour,
		dbo.GetPriceBasedOnPriceBook(P.[numPricRuleID],@monListPrice,@units,I.numItemCode) 
		* (CASE WHEN P.tintPricingMethod =2 AND P.tintRuleType=2 THEN dbo.fn_UOMConversion(numBaseUnit,@numItemCode,@numDomainId,CASE WHEN ISNULL(numPurchaseUnit,0)>0 THEN numPurchaseUnit ELSE numBaseUnit END) ELSE 1 END)  AS ListPrice,
        vcRuleName AS vcPOppName,
        CONVERT(VARCHAR(20), NULL) AS bintCreatedDate,
        CASE WHEN numPricRuleID IS NULL THEN 'Price - List price'
             ELSE 
              CASE WHEN P.tintPricingMethod = 1
                                 THEN 'Price Book Rule'
                                 ELSE CASE PBT.tintRuleType
                                        WHEN 1 THEN 'Deduct from List price '
                                        WHEN 2 THEN 'Add to primary vendor cost '
                                        ELSE ''
                                      END
                                      + CASE WHEN tintDiscountType = 1
                                             THEN CONVERT(VARCHAR(20), decDiscount)
                                                  + '%'
                                             ELSE 'flat ' + CONVERT(VARCHAR(20), CAST(decDiscount AS int))
                                        END + ' for every '
                                      + CONVERT(VARCHAR(20), CASE WHEN [intQntyItems] = 0 THEN 1
                                                                  ELSE intQntyItems
                                                             END)
                                      + ' units, until maximum of '
                                      + CONVERT(VARCHAR(20), decMaxDedPerAmt)
                                      + CASE WHEN tintDiscountType = 1 THEN ' %' ELSE ' Flat' END END
        END AS OppStatus,P.numPricRuleID,P.tintPricingMethod,PBT.tintDiscountTypeOriginal,CAST(PBT.decDiscountOriginal AS FLOAT) AS decDiscountOriginal,PBT.tintRuleType
FROM    Item I
        LEFT JOIN PriceBookRules P ON I.numDomainID = P.numDomainID
        LEFT JOIN PriceBookRuleDTL PDTL ON P.numPricRuleID = PDTL.numRuleID
        LEFT JOIN PriceBookRuleItems PBI ON P.numPricRuleID = PBI.numRuleID
        LEFT JOIN [PriceBookPriorities] PP ON PP.[Step2Value] = P.[tintStep2] AND PP.[Step3Value] = P.[tintStep3]
		CROSS APPLY dbo.GetPriceBasedOnPriceBookTable(P.[numPricRuleID],CASE WHEN bitCalAmtBasedonDepItems=1 then @CalPrice ELSE @monListPrice END ,@units,I.numItemCode) as PBT
WHERE   numItemCode = @numItemCode AND tintRuleFor=@tintOppType
AND 
(
((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 1
OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND (tintStep3= 1 AND PDTL.numValue = @numDivisionID)) -- Priority 2
OR ((tintStep2 = 3) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 3

OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 4
OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 5
OR ((tintStep2 = 3) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 6

OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 3)) -- Priority 7
OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND (tintStep3 = 3)) -- Priority 8
OR ((tintStep2 = 3) and (tintStep3 = 3)) -- Priority 9
)
ORDER BY PP.Priority ASC
END

IF @bitMode=2  -- When this procedure is being called from USP_GetPriceOfItem for e-com
	RETURN 
-- select 1 as numUnitHour,convert(money,(case when tintRuleType is null then (case when bitCalAmtBasedonDepItems=1 then @CalPrice else @monListPrice end)*@units when tintRuleType=0 then (case when bitCalAmtBasedonDepItems=1 then @CalPrice else @monListPrice end)*@units-
--(case when bitCalAmtBasedonDepItems=1 then @CalPrice else @monListPrice end)*(case when bitVolDisc=1 then           
--(case when (@units/intQntyItems)*fltDedPercentage >decMaxDedPerAmt then decMaxDedPerAmt else (@units/intQntyItems)*fltDedPercentage end)           
-- else fltDedPercentage end )*@units/100                  
--  when tintRuleType=1 then (case when bitCalAmtBasedonDepItems=1 then @CalPrice else @monListPrice end)*@units-          
--(case when bitVolDisc=1 then (case when (@units/intQntyItems)*decflatAmount>decMaxDedPerAmt then decMaxDedPerAmt else           
--(@units/intQntyItems)*decflatAmount end) else decflatAmount end )  end)/@units)           
--as ListPrice,vcRuleName as vcPOppName,convert(varchar(20),null) as bintCreatedDate,case when numPricRuleID is null then 'Price - List price' else 'Price Book Rule' end as OppStatus  from Item I      
-- left join  PriceBookRules P      
-- on I.numDomainID=P.numDomainID                   
-- left join PriceBookRuleDTL  PDTL                  
-- on P.numPricRuleID=PDTL.numRuleID                   
--where numItemCode=@numItemCode and    (((tintRuleAppType=1 and numValue=@numItemCode) or                  
-- (tintRuleAppType=2 and numValue=numItemGroup) or                  
-- (tintRuleAppType=4 and numValue=@numDivisionID) or                  
-- (tintRuleAppType=5 and numValue=@numRelationship) or                  
-- (tintRuleAppType=6 and numValue=@numRelationship and numProfile=@numProfile) or                  
-- (tintRuleAppType=7) or (tintRuleAppType= case when numItemGroup>0 then 3 else 10 end)) and (case when bitTillDate=1 then dtTillDate  else getutcdate() end >=getutcdate()) or (tintRuleAppType is null) )                               
                                   
/*Checks Old Order for price*/
 select 1 as numUnitHour,convert(money,(monTotAmount/(case when numUnitHour< 1 then 1 else numUnitHour end))) as ListPrice,vcPOppName,convert(varchar(20),bintCreatedDate) as bintCreatedDate,case when tintOppStatus=0 then 'Opportunity' when tintOppStatus=1 then 'Deal won'   
 when tintOppStatus=2 then 'Deal Lost' end as OppStatus,CAST(0 AS NUMERIC) AS numPricRuleID,CAST(0 AS TINYINT) AS tintPricingMethod,CAST(0 AS TINYINT) as tintDiscountTypeOriginal,CAST(0 AS FLOAT) as decDiscountOriginal,CAST(0 AS TINYINT) as tintRuleType                         
 from OpportunityItems itm                                        
 join OpportunityMaster mst                                        
 on mst.numOppId=itm.numOppId              
 where  tintOppType=1 and numItemCode=@numItemCode and mst.numDivisionID=@numDivisionID order by OppStatus desc  
  
  
  /* */
 select 1 as numUnitHour,CASE WHEN @bitCalAmtBasedonDepItems=1 THEN convert(money,@CalPrice) ELSE convert(money,@monListPrice) End as ListPrice,'' as vcPOppName,convert(varchar(20),null) as bintCreatedDate,CASE WHEN @bitCalAmtBasedonDepItems=1 THEN 'Sum of dependent items' ELSE '' END as OppStatus
 ,CAST(0 AS NUMERIC) AS numPricRuleID,CAST(0 AS TINYINT) AS tintPricingMethod ,CAST(0 AS TINYINT) as tintDiscountTypeOriginal,CAST(0 AS FLOAT) as decDiscountOriginal,CAST(0 AS TINYINT) as tintRuleType                    
  
            
end             
else            
begin                                               

If @numDivisionID=0
BEGIN
 SELECT @numDivisionID=V.numVendorID FROM [Vendor] V INNER JOIN Item I 
		on V.numVendorID=I.numVendorID and V.numItemCode=I.numItemCode 
		WHERE V.[numItemCode]=@numItemCode AND V.[numDomainID]=@numDomainID 
END

IF EXISTS(SELECT ISNULL([monCost],0) ListPrice FROM [Vendor] V INNER JOIN dbo.Item I 
		ON V.numItemCode = I.numItemCode
		WHERE V.[numItemCode]=@numItemCode AND V.[numDomainID]=@numDomainID AND V.numVendorID=@numDivisionID)       
begin      
       SELECT @monListPrice=ISNULL([monCost],0) FROM [Vendor] V INNER JOIN dbo.Item I 
				ON V.numItemCode = I.numItemCode
				WHERE V.[numItemCode]=@numItemCode AND V.[numDomainID]=@numDomainID 
				AND V.numVendorID=@numDivisionID
end      
else      
begin      
	 SELECT @monListPrice=ISNULL([monCost],0) FROM [Vendor] WHERE [numItemCode]=@numItemCode AND [numDomainID]=@numDomainID
end      

print @monListPrice   

SELECT TOP 1 1 AS numUnitHour,
			dbo.GetPriceBasedOnPriceBook(P.[numPricRuleID],@monListPrice,@units,I.numItemCode) AS ListPrice,
			vcRuleName AS vcPOppName,
			CONVERT(VARCHAR(20), NULL) AS bintCreatedDate,
			CASE WHEN numPricRuleID IS NULL THEN 'Price - List price'
				 ELSE  CASE WHEN P.tintPricingMethod = 1
                                 THEN 'Price Book Rule'
                                 ELSE CASE tintRuleType
                                        WHEN 1 THEN 'Deduct from List price '
                                        WHEN 2 THEN 'Add to primary vendor cost '
                                        ELSE ''
                                      END
                                      + CASE WHEN tintDiscountType = 1
                                             THEN CONVERT(VARCHAR(20), decDiscount)
                                                  + '%'
                                             ELSE 'flat ' + CONVERT(VARCHAR(20), CAST(decDiscount AS int))
                                        END + ' for every '
                                      + CONVERT(VARCHAR(20), CASE WHEN [intQntyItems] = 0 THEN 1
                                                                  ELSE intQntyItems
                                                             END)
                                      + ' units, until maximum of '
                                      + CONVERT(VARCHAR(20), decMaxDedPerAmt)
                                      + CASE WHEN tintDiscountType = 1 THEN ' %' ELSE ' Flat' END END
			END AS OppStatus,P.numPricRuleID,P.tintPricingMethod,@numDivisionID as numDivisionID
		FROM    Item I 
        LEFT JOIN PriceBookRules P ON I.numDomainID = P.numDomainID
        LEFT JOIN PriceBookRuleDTL PDTL ON P.numPricRuleID = PDTL.numRuleID
        LEFT JOIN PriceBookRuleItems PBI ON P.numPricRuleID = PBI.numRuleID
        LEFT JOIN [PriceBookPriorities] PP ON PP.[Step2Value] = P.[tintStep2]
                                              AND PP.[Step3Value] = P.[tintStep3]
WHERE   numItemCode = @numItemCode AND tintRuleFor=@tintOppType
AND 
(
((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 1
OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND (tintStep3= 1 AND PDTL.numValue = @numDivisionID)) -- Priority 2
OR ((tintStep2 = 3) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 3

OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 4
OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 5
OR ((tintStep2 = 3) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 6

OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 3)) -- Priority 7
OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND (tintStep3 = 3)) -- Priority 8
OR ((tintStep2 = 3) and (tintStep3 = 3)) -- Priority 9
)
ORDER BY PP.Priority ASC

--	IF @bitMode = 1 /* from Popup for Grid*/
--	BEGIN
		 select 1 as numUnitHour,(monTotAmount/(case when numUnitHour< 1 then 1 else numUnitHour end)) as ListPrice,vcPOppName,convert(varchar(20),bintCreatedDate) as bintCreatedDate,case when tintOppStatus=0 then 'Opportunity' 
		 when tintOppStatus=1 then 'Deal won'  
		 when tintOppStatus=2 then 'Deal Lost' end as OppStatus
		 ,CAST(0 AS NUMERIC) AS numPricRuleID,CAST(0 AS TINYINT) AS tintPricingMethod,@numDivisionID as numDivisionID                         
		 from OpportunityItems itm                                        
		 join OpportunityMaster mst                                        
		 on mst.numOppId=itm.numOppId                                        
		 where  tintOppType=2 and numItemCode=@numItemCode and mst.numDivisionID=@numDivisionID order by OppStatus DESC
--	END
--	ELSE
--	BEGIN
		/* Checks Pricebook if exist any.. */      
		
--	END
 
--	 IF @@ROWCOUNT=0
--	 BEGIN
 		select 1 as numUnitHour,convert(money,@monListPrice) as ListPrice,'' as vcPOppName,convert(varchar(20),null) as bintCreatedDate,''  as OppStatus,@numDivisionID as numDivisionID
 		,CAST(0 AS NUMERIC) AS numPricRuleID,CAST(0 AS TINYINT) AS tintPricingMethod                         
--	 END
 
	
end

END TRY
BEGIN CATCH
    SELECT 0 AS ListPrice
    SELECT 0 AS ListPrice
    SELECT 0 AS ListPrice
END CATCH
--SELECT * FROM Category
--- Alterd By Chintan Prajapati
-- EXEC dbo.USP_ItemsForECommerce 9,60,1,1,15,0,''
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ItemsForECommerce' ) 
    DROP PROCEDURE USP_ItemsForECommerce
GO
CREATE PROCEDURE [dbo].[USP_ItemsForECommerce]
    @numSiteID AS NUMERIC(9) = 0,
    @numCategoryID AS NUMERIC(18) = 0,
    @numWareHouseID AS NUMERIC(9) = 0,
    @SortBy NVARCHAR(MAX) = 0,
    @CurrentPage INT,
    @PageSize INT,
    @TotRecs INT = 0 OUTPUT,
    @SearchText AS NVARCHAR(MAX) = '',--Comma seperated item ids
    @FilterQuery AS NVARCHAR(MAX) = '',----Comma seperated Attributes Ids
    @FilterRegularCondition AS NVARCHAR(MAX),
    --@FilterCustomCondition AS NVARCHAR(MAX),
    @FilterCustomFields AS NVARCHAR(MAX),
    @FilterCustomWhere AS NVARCHAR(MAX)
   
AS 
    BEGIN
		
		DECLARE @numDomainID AS NUMERIC(9)
		SELECT  @numDomainID = numDomainID
        FROM    [Sites]
        WHERE   numSiteID = @numSiteID
        
        DECLARE @tintDisplayCategory AS TINYINT
		SELECT @tintDisplayCategory = ISNULL(bitDisplayCategory,0) FROM dbo.eCommerceDTL WHERE numSiteID = @numSiteId
		PRINT @tintDisplayCategory 
		
		CREATE TABLE #tmpItemCat(numCategoryID NUMERIC(18,0))
		CREATE TABLE #tmpItemCode(numItemCode NUMERIC(18,0))
		IF LEN(@FilterCustomFields) > 0 AND LEN(@FilterCustomWhere) > 0
		BEGIN
--			DECLARE @strCustomSql AS NVARCHAR(MAX)
--			SET @strCustomSql = 'INSERT INTO #tmpItemCode(numItemCode)
--								 SELECT DISTINCT t2.RecId FROM CFW_Fld_Master t1 
--								 JOIN CFW_FLD_Values_Item AS t2 ON t1.Fld_ID = t2.Fld_ID 
--								 WHERE t1.Grp_id = 5 
--								 AND t1.numDomainID = (SELECT numDomainID FROM dbo.Sites WHERE numSiteID = ' + CONVERT(VARCHAR(10),@numSiteID) + ') 
--								 AND t1.fld_type <> ''Link'' AND ' + @FilterCustomCondition
			
			DECLARE @FilterCustomFieldIds AS NVARCHAR(MAX)
			SET @FilterCustomFieldIds = Replace(@FilterCustomFields,'[','')
			SET @FilterCustomFieldIds = Replace(@FilterCustomFieldIds,']','')
			
			DECLARE @strCustomSql AS NVARCHAR(MAX)
			SET @strCustomSql = 'INSERT INTO #tmpItemCode(numItemCode)
								 SELECT DISTINCT T.RecId FROM 
															(
																SELECT * FROM 
																	(
																		SELECT  t2.RecId, t1.fld_id, t2.Fld_Value FROM CFW_Fld_Master t1 
																		JOIN CFW_FLD_Values_Item AS t2 ON t1.Fld_ID = t2.Fld_ID 
																		WHERE t1.Grp_id = 5 
																		AND t1.numDomainID = ' + CONVERT(VARCHAR(10),@numDomainID) + '
																		AND t1.fld_type <> ''Link'' 
																		AND t1.fld_id IN (' + @FilterCustomFieldIds + ')
																	) p  PIVOT (MAX([Fld_Value]) FOR fld_id IN (' + @FilterCustomFields + ')) AS pvt 
															) T  WHERE 1=1 AND ' + @FilterCustomWhere
								 
			PRINT @strCustomSql
			EXEC SP_EXECUTESQL @strCustomSql								 					 
		END
		
        DECLARE @strSQL NVARCHAR(MAX)
        DECLARE @firstRec AS INTEGER
        DECLARE @lastRec AS INTEGER
        DECLARE @Where NVARCHAR(MAX)
        DECLARE @SortString NVARCHAR(MAX)
        DECLARE @searchPositionColumn NVARCHAR(MAX)
        DECLARE @row AS INTEGER
        DECLARE @data AS VARCHAR(100)
        DECLARE @dynamicFilterQuery NVARCHAR(MAX)
        DECLARE @checkFldType VARCHAR(100)
        DECLARE @count NUMERIC(9,0)
        DECLARE @whereNumItemCode NVARCHAR(MAX)
        DECLARE @filterByNumItemCode VARCHAR(100)
        
		SET @SortString = ''
        set @searchPositionColumn = ''
        SET @strSQL = ''
		SET @whereNumItemCode = ''
		SET @dynamicFilterQuery = ''
		 
        SET @firstRec = ( @CurrentPage - 1 ) * @PageSize
        SET @lastRec = ( @CurrentPage * @PageSize + 1 )
                
        
        IF @numWareHouseID = 0 
            BEGIN
                SELECT  @numWareHouseID = ISNULL(numDefaultWareHouseID,
                                                        0)
                FROM    [eCommerceDTL]
                WHERE   [numDomainID] = @numDomainID	
            END
        
		--PRIAMRY SORTING USING CART DROPDOWN
		DECLARE @Join AS NVARCHAR(MAX)
		DECLARE @SortFields AS NVARCHAR(MAX)
		DECLARE @vcSort AS VARCHAR(10)

		SET @Join = ''
		SET @SortFields = ''
		SET @vcSort = ''

		IF @SortBy = ''
			BEGIN
				SET @SortString = ' vcItemName Asc '
			END
		ELSE IF CHARINDEX('~0',@SortBy) > 0 OR CHARINDEX('~1',@SortBy) > 0
			BEGIN
				DECLARE @fldID AS NUMERIC(18,0)
				DECLARE @RowNumber AS VARCHAR(MAX)
				
				SELECT * INTO #fldValues FROM dbo.SplitIDsWithPosition(@SortBy,'~')
				SELECT @RowNumber = RowNumber, @fldID = id FROM #fldValues WHERE RowNumber = 1
				SELECT @vcSort = CASE WHEN Id = 0 THEN 'ASC' ELSE 'DESC' END FROM #fldValues WHERE RowNumber = 2
				
				SET @Join = @Join + ' LEFT JOIN CFW_FLD_Values_Item CFVI' + @RowNumber + ' ON I.numItemCode = CFVI' + @RowNumber + '.RecID AND CFVI' + @RowNumber + '.fld_Id = ' + CONVERT(VARCHAR(10),@fldID)
				---SET @SortString = 'CFVI' + @RowNumber + '.fld_Value' 
				SET @SortString = '[fld_Value' + @RowNumber + '] ' --+ @vcSort
				SET @SortFields = ', CFVI' + @RowNumber + '.fld_Value [fld_Value' + @RowNumber + ']'
			END
		ELSE
			BEGIN
				--SET @SortString = @SortBy	
				
				IF CHARINDEX('monListPrice',@SortBy) > 0
				BEGIN
					DECLARE @bitSortPriceMode AS BIT
					SELECT @bitSortPriceMode = ISNULL(bitSortPriceMode,0) FROM dbo.eCommerceDTL WHERE numSiteId = @numSiteID
					PRINT @bitSortPriceMode
					IF @bitSortPriceMode = 1
					BEGIN
						SET @SortString = ' '
					END
					ELSE
					BEGIN
						SET @SortString = @SortBy	
					END
				END
				ELSE
				BEGIN
					SET @SortString = @SortBy	
				END
				
			END	

		--SECONDARY SORTING USING DEFAULT SETTINGS FROM ECOMMERCE
		SELECT ROW_NUMBER() OVER (ORDER BY Custom,vcFieldName) AS [RowID],* INTO #tempSort FROM (
		SELECT  CONVERT(VARCHAR(9),numFieldID)+'~0' AS numFieldID,
				ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName,
				0 AS Custom,
				vcDbColumnName 
		FROM View_DynamicColumns
		WHERE numFormID = 84 
		AND ISNULL(bitSettingField,0) = 1 
		AND ISNULL(bitDeleted,0)=0 
		AND ISNULL(bitCustom,0) = 0
		AND numDomainID = @numDomainID
					       
		UNION     

		SELECT  CONVERT(VARCHAR(9),numFieldID)+'~1' AS numFieldID ,
				vcFieldName ,
				1 AS Custom,
				'' AS vcDbColumnName
		FROM View_DynamicCustomColumns          
		WHERE numFormID = 84
		AND numDomainID = @numDomainID
		AND grp_id = 5 
		AND vcAssociatedControlType = 'TextBox' 
		AND ISNULL(bitCustom,0) = 1 
		AND tintPageType=1  
		) TABLE1 ORDER BY Custom,vcFieldName
		
		--SELECT * FROM #tempSort
		DECLARE @DefaultSort AS NVARCHAR(MAX)
		DECLARE @DefaultSortFields AS NVARCHAR(MAX)
		DECLARE @DefaultSortJoin AS NVARCHAR(MAX)
		DECLARE @intCnt AS INT
		DECLARE @intTotalCnt AS INT	
		DECLARE @strColumnName AS VARCHAR(100)
		DECLARE @bitCustom AS BIT
		DECLARE @FieldID AS VARCHAR(100)				

		SET @DefaultSort = ''
		SET @DefaultSortFields = ''
		SET @DefaultSortJoin = ''		
		SET @intCnt = 0
		SET @intTotalCnt = ( SELECT COUNT(*) FROM #tempSort )
		SET @strColumnName = ''
		
		IF (SELECT ISNULL(bitEnableSecSorting,0) FROM EcommerceDtl WHERE numSiteID = @numSiteID ) = 1
		BEGIN
			CREATE TABLE #fldDefaultValues(RowNumber INT,Id VARCHAR(1000))

			WHILE(@intCnt < @intTotalCnt)
			BEGIN
				SET @intCnt = @intCnt + 1
				SELECT @FieldID = numFieldID, @strColumnName = vcDbColumnName, @bitCustom = Custom FROM #tempSort WHERE RowID = @intCnt
				
				--PRINT @FieldID
				--PRINT @strColumnName
				--PRINT @bitCustom

				IF @bitCustom = 0
					BEGIN
						SET @DefaultSort =  @strColumnName + ',' + @DefaultSort 
					END

				ELSE
					BEGIN
						DECLARE @fldID1 AS NUMERIC(18,0)
						DECLARE @RowNumber1 AS VARCHAR(MAX)
						DECLARE @vcSort1 AS VARCHAR(10)
						DECLARE @vcSortBy AS VARCHAR(1000)
						SET @vcSortBy = CONVERT(VARCHAR(100),@FieldID) + '~' + '0'
						--PRINT @vcSortBy

						INSERT INTO #fldDefaultValues(RowNumber,Id) SELECT RowNumber, id FROM dbo.SplitIDsWithPosition(@vcSortBy,'~')
						SELECT @RowNumber1 = RowNumber + 100 + @intCnt, @fldID1 = id FROM #fldDefaultValues WHERE RowNumber = 1
						SELECT @vcSort1 = 'ASC' 
						
						SET @DefaultSortJoin = @DefaultSortJoin + ' LEFT JOIN CFW_FLD_Values_Item CFVI' + @RowNumber1 + ' ON I.numItemCode = CFVI' + @RowNumber1 + '.RecID AND CFVI' + @RowNumber1 + '.fld_Id = ' + CONVERT(VARCHAR(10),@fldID1)
						SET @DefaultSort = '[fld_Value' + @RowNumber1 + '] ' + ',' + @DefaultSort 
						SET @DefaultSortFields = @DefaultSortFields + ', CFVI' + @RowNumber1 + '.fld_Value [fld_Value' + @RowNumber1 + ']'
					END

			END
			IF LEN(@DefaultSort) > 0
			BEGIN
				SET  @DefaultSort = LEFT(@DefaultSort,LEN(@DefaultSort) - 1)
				SET @SortString = @SortString + ',' 
			END
			--PRINT @SortString
			--PRINT @DefaultSort
			--PRINT @DefaultSortFields
		END
--        IF @SortBy = 1 
--            SET @SortString = ' vcItemName Asc '
--        ELSE IF @SortBy = 2 
--            SET @SortString = ' vcItemName Desc '
--        ELSE IF @SortBy = 3 
--            SET @SortString = ' monListPrice Asc '
--        ELSE IF @SortBy = 4 
--            SET @SortString = ' monListPrice Desc '
--        ELSE IF @SortBy = 5 
--            SET @SortString = ' bintCreatedDate Desc '
--        ELSE IF @SortBy = 6 
--            SET @SortString = ' bintCreatedDate ASC '	
--        ELSE IF @SortBy = 7 --sort by relevance, invokes only when search
--            BEGIN
--                 IF LEN(@SearchText) > 0 
--		            BEGIN
--						SET @SortString = ' SearchOrder ASC ';
--					    set @searchPositionColumn = ' ,SearchItems.RowNumber AS SearchOrder ' 						
--					END
--			END
--        ELSE 
--            SET @SortString = ' vcItemName Asc '
		
                                		
        IF LEN(ISNULL(@SearchText,'')) > 0
			BEGIN
				--PRINT 1
				IF CHARINDEX('SearchOrder',@SortString) > 0
				BEGIN
					SET @searchPositionColumn = ' ,SearchItems.RowNumber AS SearchOrder ' 
				END

				SET @Where = ' INNER JOIN dbo.SplitIDsWithPosition('''+ @SearchText +''','','') AS SearchItems 
                        ON SearchItems.Id = I.numItemCode  and SC.numSiteID = ' + CONVERT(VARCHAR(15), @numSiteID) + ' WHERE ISNULL(IsArchieve,0) = 0'
				END
		ELSE
			BEGIN
				--PRINT 2
				SET @Where = ' WHERE 1=1 '
							+ ' and SC.numSiteID = ' + CONVERT(VARCHAR(15), @numSiteID)
							+ ' AND ISNULL(IsArchieve,0) = 0'
							
				IF @tintDisplayCategory = 1
				BEGIN
					
					;WITH CTE AS(
					SELECT C.numCategoryID,C.tintLevel,C.numDepCategory,CASE WHEN tintLevel=1 THEN 1 ELSE 0 END AS bitChild 
						FROM Category C WHERE (numCategoryID = @numCategoryID OR ISNULL(@numCategoryID,0) = 0) AND numDomainID = @numDomainID
					UNION ALL
					SELECT  C.numCategoryID,C.tintLevel,C.numDepCategory,CTE.bitChild FROM Category C JOIN CTE ON 
					C.numDepCategory=CTE.numCategoryID AND CTE.bitChild=1 AND numDomainID = @numDomainID)
					
					INSERT INTO #tmpItemCat(numCategoryID)												 
					SELECT DISTINCT C.numCategoryID FROM CTE C JOIN dbo.ItemCategory IC ON C.numCategoryID = IC.numCategoryID 
														WHERE numItemID IN ( 
																						
													 						SELECT numItemID FROM WareHouseItems WI
																			INNER JOIN dbo.Item I ON I.numItemCode = WI.numItemID AND I.numDomainID = @numDomainID
																			GROUP BY numItemID 
																			HAVING SUM(WI.numOnHand) > 0 
																			UNION ALL
													 						SELECT numItemID FROM WareHouseItems WI
																			INNER JOIN dbo.Item I ON I.numItemCode = WI.numItemID AND I.numDomainID = @numDomainID
																			GROUP BY numItemID 
																			HAVING SUM(WI.numAllocation) > 0
																		 )		
					SELECT numItemID INTO #tmpOnHandItems FROM 
					(													 					
						SELECT numItemID FROM WareHouseItems WI 
						INNER JOIN dbo.Item I ON I.numItemCode = WI.numItemID AND I.numDomainID = @numDomainID
						GROUP BY numItemID 
						HAVING SUM(WI.numOnHand) > 0
						UNION ALL
						SELECT numItemID FROM WareHouseItems WI 
						INNER JOIN dbo.Item I ON I.numItemCode = WI.numItemID AND I.numDomainID = @numDomainID
						GROUP BY numItemID 
						HAVING SUM(WI.numAllocation) > 0				
					) TABLE1
				END
			END
		
			--SELECT * FROM  #tmpOnHandItems
			--SELECT * FROM  #tmpItemCat
			
----		IF (SELECT COUNT(*) FROM #tmpItemCat) = 0 AND ISNULL(@numCategoryID,0) = 0
----		BEGIN
----			
----			INSERT INTO #tmpItemCat(numCategoryID)
----			SELECT DISTINCT C.numCategoryID FROM Category C
----			JOIN dbo.ItemCategory IC ON C.numCategoryID = IC.numCategoryID 
----							WHERE C.numDepCategory = (SELECT TOP 1 numCategoryID FROM dbo.Category 
----													  WHERE numDomainID = @numDomainID
----													  AND tintLevel = 1)
----							AND tintLevel <> 1
----							AND numDomainID = @numDomainID
----							AND numItemID IN (
----											 	SELECT numItemID FROM WareHouseItems WI
----												INNER JOIN dbo.Item I ON I.numItemCode = WI.numItemID
----												GROUP BY numItemID 
----												HAVING SUM(WI.numOnHand) > 0
----											 )
----			--SELECT * FROM #tmpItemCat								 
----		END
----		ELSE
----		BEGIN
----			INSERT INTO #tmpItemCat(numCategoryID)VALUES(@numCategoryID)
----		END

		       SET @strSQL = @strSQL + ' WITH Items AS 
	                                     ( 
	                                     SELECT   
                                               I.numItemCode,
                                               ROW_NUMBER() OVER ( Partition by I.numItemCode Order by I.numItemCode) RowID,
                                               vcItemName,
                                               I.bintCreatedDate,
                                               vcManufacturer,
                                               C.vcCategoryName,
                                               ISNULL(txtItemDesc,'''') txtItemDesc,
                                               ISNULL(CASE  WHEN I.[charItemType] = ''P''
															THEN CASE WHEN I.bitSerialized = 1
																	  THEN ( UOM * monListPrice )
																	  ELSE ( UOM * ISNULL(W1.[monWListPrice], 0) )
																 END
															ELSE ( UOM * monListPrice )
													  END, 0) monListPrice,
                                               vcSKU,
                                               fltHeight,
                                               fltWidth,
                                               fltLength,
                                               IM.vcPathForImage,
                                               IM.vcPathForTImage,
                                               vcModelID,
                                               fltWeight,
                                               bitFreeShipping,
                                               UOM AS UOMConversionFactor,
												( CASE WHEN charItemType <> ''S'' AND charItemType <> ''N''
													   THEN ( CASE WHEN bitShowInStock = 1
																   THEN ( CASE WHEN ( ISNULL(W.numOnHand,0)<=0) THEN ''<font color=red>Out Of Stock</font>''
																			   WHEN bitAllowBackOrder = 1 THEN ''In Stock''
																			   ELSE ''In Stock'' 
																		  END )
																   ELSE ''''
															  END )
													   ELSE ''''
												  END ) AS InStock, C.vcDescription as CategoryDesc, ISNULL(W1.numWareHouseItemID,0) [numWareHouseItemID] ' 
								               +	@searchPositionColumn + @SortFields + @DefaultSortFields + '
								
                                         FROM      
                                                      Item AS I
                                         INNER JOIN   ItemCategory IC   ON   I.numItemCode = IC.numItemID
                                         LEFT JOIN    ItemImages IM     ON   I.numItemCode = IM.numItemCode AND IM.bitdefault=1 AND IM.bitIsImage = 1
                                         INNER JOIN   Category C        ON   IC.numCategoryID = C.numCategoryID
                                         INNER JOIN   SiteCategories SC ON   IC.numCategoryID = SC.numCategoryID
                                         INNER JOIN   eCommerceDTL E    ON   E.numDomainID = I.numDomainID ' + @Join + ' ' + @DefaultSortJoin + ' 
                                         CROSS APPLY (SELECT SUM(numOnHand) numOnHand FROM WareHouseItems W 
													  WHERE  W.numItemID = I.numItemCode
                                                      AND numWareHouseID = ' + CONVERT(VARCHAR(10),@numWareHouseID) + ' 
                                                      AND W.numDomainID = ' + CONVERT(VARCHAR(10),@numDomainID) + ') AS W  
										 CROSS APPLY (SELECT TOP 1 numWareHouseItemID,monWListPrice FROM WareHouseItems W 
													  WHERE  W.numItemID = I.numItemCode 
													  AND W.numDomainID = ' + CONVERT(VARCHAR(10),@numDomainID) + '
													  AND numWareHouseID = ' + CONVERT(VARCHAR(10),@numWareHouseID) + ' ) AS W1
										 CROSS APPLY (SELECT SUM(numAllocation) numAllocation FROM WareHouseItems W2 
													  WHERE  W2.numItemID = I.numItemCode
                                                      AND numWareHouseID = ' + CONVERT(VARCHAR(10),@numWareHouseID) + ' 
                                                      AND W2.numDomainID = ' + CONVERT(VARCHAR(10),@numDomainID) + ') AS W2  			  
										 CROSS APPLY (SELECT dbo.fn_UOMConversion(ISNULL(I.numSaleUnit, 0),I.numItemCode, I.numDomainId,ISNULL(I.numBaseUnit, 0)) AS UOM) AS UOM '

			IF LEN(@FilterCustomFields) > 0 AND LEN(@FilterCustomWhere) > 0
			BEGIN
				SET @strSQL = @strSQL + ' INNER JOIN #tmpItemCode TP ON TP.numItemCode = I.numItemCode ' 
			END
			
			IF (SELECT COUNT(*) FROM #tmpItemCat) > 0
			BEGIN
				SET @strSQL = @strSQL + ' INNER JOIN #tmpItemCat TC ON TC.numCategoryID = IC.numCategoryID '
				SET @strSQL = @strSQL + ' LEFT JOIN #tmpOnHandItems TOIC ON TOIC.numItemID = I.numItemCode '
			END
			ELSE IF @numCategoryID>0
			BEGIN
				SET @Where = REPLACE(@Where,'WHERE',' WHERE IC.numCategoryID = ' + CONVERT(VARCHAR(10),@numCategoryID) + ' AND ')
			END 
			
			IF @tintDisplayCategory = 1
			BEGIN
				SET @Where = @Where + ' AND (ISNULL(W.numOnHand, 0) > 0 OR ISNULL(W2.numAllocation, 0) > 0) '
			END
			
			--PRINT @Where
			SET @strSQL = @strSQL + @Where
			
			IF LEN(@FilterRegularCondition) > 0
			BEGIN
				SET @strSQL = @strSQL + @FilterRegularCondition
			END                                         
			
---PRINT @SortString + ',' + @DefaultSort + ' ' + @vcSort 
	
            IF LEN(@whereNumItemCode) > 0
                                        BEGIN
								              SET @strSQL = @strSQL + @filterByNumItemCode	
								    	END
            SET @strSQL = @strSQL +     '   )
                                        , ItemSorted AS (SELECT  ROW_NUMBER() OVER ( ORDER BY  ' + @SortString + @DefaultSort + ' ' + @vcSort + ' ) AS Rownumber,* FROM  Items WHERE RowID = 1)'
            
            DECLARE @fldList AS NVARCHAR(MAX)
			SELECT @fldList=COALESCE(@fldList + ',','') + QUOTENAME(REPLACE(Fld_label,' ','') + '_C') FROM dbo.CFW_Fld_Master WHERE numDomainID=@numDomainID AND Grp_id=5
            
            SET @strSQL = @strSQL +' SELECT  Rownumber,numItemCode,txtItemDesc,vcManufacturer,monListPrice,vcItemName,vcCategoryName,vcPathForImage,vcpathForTImage,vcSKU,fltHeight,fltWidth,fltLength,vcModelID,fltWeight,bitFreeShipping,UOMConversionFactor,InStock,CategoryDesc,numWareHouseItemID
                                     INTO #tempItem FROM ItemSorted ' 
--                                        + 'WHERE Rownumber > ' + CONVERT(VARCHAR(10), @firstRec) + '
--                                        AND   Rownumber < ' + CONVERT(VARCHAR(10), @lastRec) + '
--                                        order by Rownumber;' 
            
            SET @strSQL = @strSQL + ' SELECT * INTO #tmpPagedItems FROM #tempItem WHERE Rownumber > ' + CONVERT(VARCHAR(10), @firstRec) + '
                                      AND   Rownumber < ' + CONVERT(VARCHAR(10), @lastRec) + '
                                      order by Rownumber '
                                                                  
            IF LEN(ISNULL(@fldList,''))>0
            BEGIN
				PRINT 'flds1'
				SET @strSQL = @strSQL +'SELECT  Rownumber,numItemCode,txtItemDesc,vcManufacturer,monListPrice,vcItemName,vcCategoryName,vcPathForImage,vcpathForTImage,vcSKU,fltHeight,fltWidth,fltLength,vcModelID,fltWeight,bitFreeShipping,UOMConversionFactor,InStock,CategoryDesc,numWareHouseItemID,' + @fldList + '
                                        FROM #tmpPagedItems I left join (
			SELECT  t2.RecId, t1.Fld_label + ''_C'' as Fld_label, 
				CASE t1.fld_type WHEN ''SelectBox'' THEN isnull(dbo.fn_GetListItemName(t2.Fld_Value),'''')
								 WHEN ''CheckBox'' THEN isnull(dbo.fn_getCustomDataValue(t2.Fld_Value,9),'''')
								 WHEN ''DateField'' THEN dbo.FormatedDateFromDate(t2.Fld_Value,' + CAST(@numDomainId AS VARCHAR(18)) +')
								 ELSE Replace(ISNULL(t2.Fld_Value,''0''),''$'',''$$'') end AS Fld_Value FROM CFW_Fld_Master t1 
			JOIN CFW_FLD_Values_Item AS t2 ON t1.Fld_ID = t2.Fld_ID where t1.Grp_id = 5 and t1.numDomainID=' + CAST(@numDomainId AS VARCHAR(18)) +' AND t1.fld_type <> ''Link''
			AND t2.RecId IN (SELECT numItemCode FROM #tmpPagedItems )
			) p PIVOT (MAX([Fld_Value]) FOR Fld_label IN (' + @fldList + ')) AS pvt on I.numItemCode=pvt.RecId order by Rownumber;' 	
			END 
			ELSE
			BEGIN
				PRINT 'flds2'
				SET @strSQL = @strSQL +' SELECT  Rownumber,numItemCode,txtItemDesc,vcManufacturer,monListPrice,vcItemName,vcCategoryName,vcPathForImage,vcpathForTImage,vcSKU,fltHeight,fltWidth,fltLength,vcModelID,fltWeight,bitFreeShipping,UOMConversionFactor,InStock,CategoryDesc,numWareHouseItemID
                                         FROM  #tmpPagedItems order by Rownumber;'
			END               
                                        
            
--            PRINT(@strSQL)        
--            EXEC ( @strSQL) ; 
			                           
            SET @strSQL = @strSQL + ' SELECT @TotRecs = COUNT(*) FROM #tempItem '                            
--             SET @strSQL = 'SELECT @TotRecs = COUNT(*) 
--                           FROM Item AS I
--                           INNER JOIN   ItemCategory IC ON I.numItemCode = IC.numItemID
--                           INNER JOIN   Category C ON IC.numCategoryID = C.numCategoryID
--                           INNER JOIN   SiteCategories SC ON IC.numCategoryID = SC.numCategoryID ' --+ @Where
--             
--			 IF LEN(@FilterCustomFields) > 0 AND LEN(@FilterCustomWhere) > 0
--			 BEGIN
--				 SET @strSQL = @strSQL + ' INNER JOIN #tmpItemCode TP ON TP.numItemCode = I.numItemCode ' 
--			 END
--			
--			 IF (SELECT COUNT(*) FROM #tmpItemCat) > 0
--			 BEGIN
--				SET @strSQL = @strSQL + ' INNER JOIN #tmpItemCat TC ON TC.numCategoryID = IC.numCategoryID '
--				SET @strSQL = @strSQL + ' INNER JOIN #tmpOnHandItems TOIC ON TOIC.numItemID = I.numItemCode '
--			 END
--			 ELSE IF @numCategoryID>0
--			 BEGIN
--				SET @Where = REPLACE(@Where,'WHERE',' WHERE IC.numCategoryID = ' + CONVERT(VARCHAR(10),@numCategoryID) + ' AND ')
--			 END 
--			
--			 SET @strSQL = @strSQL + @Where  
--			 
--			 IF LEN(@FilterRegularCondition) > 0
--			 BEGIN
--				SET @strSQL = @strSQL + @FilterRegularCondition
--			 END 
			
			 PRINT  @strSQL     
             EXECUTE sp_executeSQL @strSQL, N'@TotRecs INT OUTPUT', @TotRecs OUTPUT
             
             
        DECLARE @tintOrder AS TINYINT                                                  
		DECLARE @vcFormFieldName AS VARCHAR(50)                                                  
		DECLARE @vcListItemType AS VARCHAR(1)                                             
		DECLARE @vcAssociatedControlType VARCHAR(10)                                                  
		DECLARE @numListID AS NUMERIC(9)                                                  
		DECLARE @WhereCondition VARCHAR(2000)                       
		DECLARE @numFormFieldId AS NUMERIC  
		DECLARE @vcFieldType CHAR(1)
		                  
		SET @tintOrder=0                                                  
		SET @WhereCondition =''                 
                   
              
		CREATE TABLE #tempAvailableFields(numFormFieldId  NUMERIC(9),vcFormFieldName NVARCHAR(50),
		vcFieldType CHAR(1),vcAssociatedControlType NVARCHAR(50),numListID NUMERIC(18),
		vcListItemType CHAR(1),intRowNum INT,vcItemValue VARCHAR(3000))
		   
		INSERT INTO #tempAvailableFields (numFormFieldId,vcFormFieldName,vcFieldType,vcAssociatedControlType
				,numListID,vcListItemType,intRowNum)                         
		SELECT  Fld_id,REPLACE(Fld_label,' ','') AS vcFormFieldName,
			   CASE GRP_ID WHEN 4 then 'C' WHEN 1 THEN 'D' ELSE 'C' END AS vcFieldType,
				fld_type as vcAssociatedControlType,
				ISNULL(C.numListID, 0) numListID,
				CASE WHEN C.numListID > 0 THEN 'L'
					 ELSE ''
				END vcListItemType,ROW_NUMBER() OVER (ORDER BY Fld_id ASC) AS intRowNum
		FROM    CFW_Fld_Master C LEFT JOIN CFW_Validation V ON V.numFieldID = C.Fld_id
		WHERE   C.numDomainID = @numDomainId
				AND GRP_ID IN (5)

		  
		SELECT * FROM #tempAvailableFields
		
		/*
		DROP TABLE #tempAvailableFields
		DROP TABLE #tmpItemCode
		DROP TABLE #tmpItemCat
		*/	
		
		IF OBJECT_ID('tempdb..#tempAvailableFields') IS NOT NULL
		DROP TABLE #tempAvailableFields
		
		IF OBJECT_ID('tempdb..#tmpItemCode') IS NOT NULL
		DROP TABLE #tmpItemCode
		
		IF OBJECT_ID('tempdb..#tmpItemCat') IS NOT NULL
		DROP TABLE #tmpItemCat
		
		IF OBJECT_ID('tempdb..#fldValues') IS NOT NULL
		DROP TABLE #fldValues
		
		IF OBJECT_ID('tempdb..#tempSort') IS NOT NULL
		DROP TABLE #tempSort
		
		IF OBJECT_ID('tempdb..#fldDefaultValues') IS NOT NULL
		DROP TABLE #fldDefaultValues
		
		IF OBJECT_ID('tempdb..#tmpOnHandItems') IS NOT NULL
		DROP TABLE #tmpOnHandItems

		IF OBJECT_ID('tempdb..#tmpPagedItems') IS NOT NULL
		DROP TABLE #tmpPagedItems
    END


GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_ManageInventory' ) 
    DROP PROCEDURE usp_ManageInventory
GO
CREATE PROCEDURE [dbo].[usp_ManageInventory]
    @itemcode AS NUMERIC(9) = 0,
    @numWareHouseItemID AS NUMERIC(9) = 0,
    @monAmount AS MONEY,
    @tintOpptype AS TINYINT,
    @numUnits AS NUMERIC,
    @QtyShipped AS NUMERIC,
    @QtyReceived AS NUMERIC,
    @monPrice AS MONEY,
    @tintFlag AS TINYINT,  --1: SO/PO insert/edit 2:SO/PO Close 3:SO/PO Re-Open 4:Sales Fulfillment Release 5:Sales Fulfillment Revert
    @numOppID as numeric(9)=0,
    @numoppitemtCode AS NUMERIC(9)=0,
    @numUserCntID AS NUMERIC(9)=0   
AS 
    DECLARE @onHand AS NUMERIC                                    
    DECLARE @onOrder AS NUMERIC 
    DECLARE @onBackOrder AS NUMERIC
    DECLARE @onAllocation AS NUMERIC
    DECLARE @monAvgCost AS MONEY 
    DECLARE @bitKitParent BIT
	DECLARE @numOrigUnits AS NUMERIC		
	DECLARE @description AS VARCHAR(100)
	DECLARE @bitAsset as BIT
--    DECLARE @OldOnHand AS NUMERIC    
--    DECLARE @NewAverageCost AS MONEY    
--    DECLARE @OldCost AS MONEY    
--    DECLARE @NewCost AS MONEY    
      
	SELECT @bitAsset=ISNULL(bitAsset,0) from Item where numItemCode=@itemcode--Added By Sachin
    
	SELECT  @monAvgCost = ISNULL(monAverageCost, 0),
            @bitKitParent = ( CASE WHEN ISNULL(bitKitParent,0) = 1
                               AND ISNULL(bitAssembly,0) = 1 THEN 0
                          WHEN ISNULL(bitKitParent,0) = 1 THEN 1
                          ELSE 0
                     END )
    FROM    Item
    WHERE   numitemcode = @itemcode                                   
    
    IF @numWareHouseItemID>0
    BEGIN                     
		SELECT  @onHand = ISNULL(numOnHand, 0),
				@onAllocation = ISNULL(numAllocation, 0),
				@onOrder = ISNULL(numOnOrder, 0),
				@onBackOrder = ISNULL(numBackOrder, 0)
		FROM    WareHouseItems
		WHERE   numWareHouseItemID = @numWareHouseItemID 
    END
   
   DECLARE @dtItemReceivedDate AS DATETIME;SET @dtItemReceivedDate=NULL
   
   DECLARE @TotalOnHand AS NUMERIC;SET @TotalOnHand=0  
   SELECT @TotalOnHand=SUM(ISNULL(numOnHand, 0)) FROM dbo.WareHouseItems WHERE numItemID=@itemcode
					
   SET @numOrigUnits=@numUnits
            
    IF @tintFlag = 1  --1: SO/PO insert/edit 
        BEGIN                                   
            IF @tintOpptype = 1 
                BEGIN  
			--When change below code also change to USP_ManageWorkOrderStatus  

				/* When QtyShipped has been updated form sales fulfillment then it need to be deducted */
					SET @description='SO insert/edit (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'
				
				IF @bitKitParent = 1 
                        BEGIN
                            EXEC USP_UpdatingInventoryForEmbeddedKits @numoppitemtCode,
                                @numOrigUnits, 0,@numOppID,0,@numUserCntID
                        END 
                 ELSE
				  BEGIN       
					SET @numUnits = @numUnits - @QtyShipped
                                                        
                    IF @onHand >= @numUnits 
                        BEGIN                                    
                            SET @onHand = @onHand - @numUnits                            
                            SET @onAllocation = @onAllocation + @numUnits                                    
                        END                                    
                    ELSE IF @onHand < @numUnits 
                            BEGIN                                    
                                SET @onAllocation = @onAllocation + @onHand                                    
                                SET @onBackOrder = @onBackOrder + @numUnits
                                    - @onHand                                    
                                SET @onHand = 0                                    
                            END    
			                                 
                 
				 
				    --UPDATE  WareHouseItems
        --            SET     numOnHand = @onHand,
        --                    numAllocation = @onAllocation,
        --                    numBackOrder = @onBackOrder,dtModified = GETDATE() 
        --            WHERE   numWareHouseItemID = @numWareHouseItemID  

				IF @bitAsset=0--Not Asset
						BEGIN 
							UPDATE  WareHouseItems
							SET     numOnHand = @onHand,
									numAllocation = @onAllocation,
									numBackOrder = @onBackOrder,dtModified = GETDATE() 
							WHERE   numWareHouseItemID = @numWareHouseItemID  

						END
					--ELSE --For Asset
					--	BEGIN

					--	DECLARE @onHandShake AS NUMERIC 
						  
					--						SELECT  @onHandShake = ISNULL(numOnHand, 0)											
					--						FROM    WareHouseItems
					--						WHERE   numWareHouseItemID = @numWareHouseItemID

					--		UPDATE  WareHouseItems
					--		SET   
					--				 numOnHand = @onHandShake,
					--				--numAllocation = @onAllocation,
					--				--numBackOrder = @onBackOrder,
					--				dtModified = GETDATE() 
					--		WHERE   numWareHouseItemID = @numWareHouseItemID  

					--	END


				END
			 
				       
                END                       
            ELSE IF @tintOpptype = 2 
                    BEGIN  
			        
						SET @description='PO insert/edit (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Received:' +  CAST(@QtyReceived AS VARCHAR(10)) + ')'
		            
						/* When QtyReceived has been updated form purchase fulfillment then it need to be deducted */
                        SET @numUnits = @numUnits - @QtyReceived   
                                             
                        SET @onOrder = @onOrder + @numUnits 
		    
                        UPDATE  WareHouseItems
                        SET     numOnHand = @onHand,
                                numAllocation = @onAllocation,
                                numBackOrder = @onBackOrder,
                                numOnOrder = @onOrder,dtModified = GETDATE() 
                        WHERE   numWareHouseItemID = @numWareHouseItemID 
                        
                    END                                                                                                                                         
        END
	 
    ELSE IF @tintFlag = 2 --2:SO/PO Close
            BEGIN
	 	
                PRINT '@OppType=' + CONVERT(VARCHAR(5), @tintOpptype)           
                IF @tintOpptype = 1 
                    BEGIN   
					SET @description='SO Close (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'
                    
                    IF @bitKitParent = 1 
                        BEGIN
                            EXEC USP_UpdatingInventoryForEmbeddedKits @numoppitemtCode,
                                @numOrigUnits, 2,@numOppID,0,@numUserCntID
                        END  
                     ELSE
                      BEGIN       
                    
    /* When QtyShipped has been updated form sales fulfillment then it need to be deducted */
							SET @numUnits = @numUnits - @QtyShipped
					  IF @bitAsset=0--Not Asset
							  BEGIN
									SET @onAllocation = @onAllocation - @numUnits 
							  END
					  ELSE--Asset
							  BEGIN
									 SET @onAllocation = @onAllocation  
							  END
											           
		    
							UPDATE  WareHouseItems
							SET     numOnHand = @onHand,
									numAllocation = @onAllocation,
									numBackOrder = @onBackOrder,dtModified = GETDATE() 
							WHERE   numWareHouseItemID = @numWareHouseItemID   
					   END
	
                    END                
                ELSE IF @tintOpptype = 2 
                        BEGIN
					SET @description='PO Close (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Received:' +  CAST(@QtyReceived AS VARCHAR(10)) + ')'
                        
        /* When QtyReceived has been updated form purchase fulfillment then it need to be deducted */
                            SET @numUnits = @numUnits - @QtyReceived
            
							IF EXISTS(SELECT 1 FROM dbo.OpportunityMaster WHERE numOppID=@numOppID AND ISNULL(bitStockTransfer,0)=0)
							BEGIN
								--Updating the Average Cost
								IF @TotalOnHand + @numUnits = 0
								BEGIN
									  SET @monAvgCost = @monAvgCost --( ( @TotalOnHand * @monAvgCost ) + (@numUnits * @monPrice))
								END
								ELSE
								BEGIN
									  SET @monAvgCost = ( ( @TotalOnHand * @monAvgCost )
												+ (@numUnits * @monPrice))
								/ ( @TotalOnHand + @numUnits )
								END    
		                            
								UPDATE  item
								SET     monAverageCost = @monAvgCost
								WHERE   numItemCode = @itemcode
							END
		    
--                            SELECT  @OldOnHand = SUM(numonhand)
--                            FROM    WareHouseItems WHI
--                            WHERE   WHI.numitemid = @itemcode     
--                            SET @OldCost = @OldOnHand * @monAvgCost     
      
                            IF @onOrder >= @numUnits 
                                BEGIN            
                                    PRINT 'in @onOrder >= @numUnits '
                                    SET @onOrder = @onOrder - @numUnits            
                                    IF @onBackOrder >= @numUnits 
                                        BEGIN            
                                            PRINT 'in @onBackOrder >= @numUnits '
                                            PRINT @numUnits
                                            SET @onBackOrder = @onBackOrder
                                                - @numUnits            
                                            SET @onAllocation = @onAllocation
                                                + @numUnits            
                                        END            
                                    ELSE 
                                        BEGIN            
                                            SET @onAllocation = @onAllocation
                                                + @onBackOrder            
                                            SET @numUnits = @numUnits
                                                - @onBackOrder            
                                            SET @onBackOrder = 0            
                                            SET @onHand = @onHand + @numUnits            
                                        END         
                                END            
                            ELSE IF @onOrder < @numUnits 
                                    BEGIN            
                                        PRINT 'in @onOrder < @numUnits '
                                        SET @onHand = @onHand + @onOrder
                                        SET @onOrder = @numUnits - @onOrder
                                    END         
   -- To Find New Stock Details    
--    Print '@numUnits='+convert(varchar(5),@numUnits)    
--    Print '@monPrice='+Convert(varchar(5),@monPrice)    
--                            SET @NewCost = @numUnits * @monPrice    
    ---Set @NewAverageCost=(@OldCost+@NewCost)/@onHand    
    --Print '@NewAverageCost='+Convert(varchar(5),@NewAverageCost)    
                            UPDATE  WareHouseItems
                            SET     numOnHand = @onHand,
                                    numAllocation = @onAllocation,
                                    numBackOrder = @onBackOrder,
                                    numOnOrder = @onOrder,dtModified = GETDATE() 
                            WHERE   numWareHouseItemID = @numWareHouseItemID  
			                
						SELECT @dtItemReceivedDate=dtItemReceivedDate FROM OpportunityMaster WHERE numOppId=@numOppId
			                
                --ASk Carl for more Info
--                            UPDATE  Item
--                            SET     monAverageCost = @NewAverageCost
--                            WHERE   numItemCode = @itemcode         
                        END
            END

ELSE IF @tintFlag = 3 --3:SO/PO Re-Open
            BEGIN
	 	
                PRINT '@OppType=' + CONVERT(VARCHAR(5), @tintOpptype)           
                IF @tintOpptype = 1 
                    BEGIN         
					SET @description='SO Re-Open (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'
                    
                    IF @bitKitParent = 1 
                        BEGIN
                            EXEC USP_UpdatingInventoryForEmbeddedKits @numoppitemtCode,
                                @numOrigUnits, 3,@numOppID,0,@numUserCntID
                        END 
                     ELSE
                     BEGIN
    /* When QtyShipped has been updated form sales fulfillment then it need to be deducted */
                        SET @numUnits = @numUnits - @QtyShipped
  
					 IF @bitAsset=0--Not Asset
							  BEGIN
									SET @onAllocation = @onAllocation + @numUnits
							  END
					  ELSE--Asset
							  BEGIN
									 SET @onAllocation = @onAllocation 
							  END
                                    
	    
                        UPDATE  WareHouseItems
                        SET     numOnHand = @onHand,
                                numAllocation = @onAllocation,
                                numBackOrder = @onBackOrder,dtModified = GETDATE() 
                        WHERE   numWareHouseItemID = @numWareHouseItemID
					  END
						               
                    END                
                ELSE IF @tintOpptype = 2 
                        BEGIN
					SET @description='PO Re-Open (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Received:' +  CAST(@QtyReceived AS VARCHAR(10)) + ')'
                        
        /* When QtyReceived has been updated form purchase fulfillment then it need to be deducted */
                            SET @numUnits = @numUnits - @QtyReceived
            
            
							--Updating the Average Cost
							IF EXISTS(SELECT 1 FROM dbo.OpportunityMaster WHERE numOppID=@numOppID AND ISNULL(bitStockTransfer,0)=0)
							BEGIN
								--IF @onHand + @onOrder - @numUnits <> 0 
								--BEGIN
								IF @TotalOnHand - @numUnits = 0
								BEGIN
									SET @monAvgCost = 0
	--								SET @monAvgCost = ( ( @TotalOnHand * @monAvgCost )
	--                                                     - (@numUnits * @monPrice))
								END
								ELSE
								BEGIN
									SET @monAvgCost = ( ( @TotalOnHand * @monAvgCost )
														 - (@numUnits * @monPrice))
										/ ( @TotalOnHand - @numUnits )
								END        
								--END
							--ELSE 
								--SET @monAvgCost = 0
                            
                            
								 UPDATE  item
								 SET     monAverageCost = @monAvgCost
								 WHERE   numItemCode = @itemcode
							END

                            IF @onHand >= @numUnits 
                                BEGIN     
                                     SET @onHand = @onHand - @numUnits            
                                     SET @onOrder = @onOrder + @numUnits
                                END         
                          
						   UPDATE  WareHouseItems
                            SET     numOnHand = @onHand,
                                    numOnOrder = @onOrder,dtModified = GETDATE() 
                            WHERE   numWareHouseItemID = @numWareHouseItemID  
                            
                        END
            END
            
            
            
             IF @numWareHouseItemID>0
				 BEGIN
					DECLARE @tintRefType AS TINYINT
					
					IF @tintOpptype = 1 --SO
					  SET @tintRefType=3
					ELSE IF @tintOpptype = 2 --PO
					  SET @tintRefType=4
					  
						DECLARE @numDomain AS NUMERIC(18,0)
						SET @numDomain = (SELECT TOP 1 [numDomainID] FROM WareHouseItems WHERE [WareHouseItems].[numWareHouseItemID] = @numWareHouseItemID)

						EXEC dbo.USP_ManageWareHouseItems_Tracking
						@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
						@numReferenceID = @numOppID, --  numeric(9, 0)
						@tintRefType = @tintRefType, --  tinyint
						@vcDescription = @description, --  varchar(100)
						@numModifiedBy = @numUserCntID,
						@dtRecordDate =  @dtItemReceivedDate,
						@numDomainID = @numDomain
                  END
GO
GO
/****** Object:  StoredProcedure [dbo].[USP_ManageItemsAndKits]    Script Date: 02/15/2010 21:44:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Anoop Jayaraj                                                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_manageitemsandkits')
DROP PROCEDURE usp_manageitemsandkits
GO
CREATE PROCEDURE [dbo].[USP_ManageItemsAndKits]                                                                
@numItemCode as numeric(9) output,                                                                
@vcItemName as varchar(300),                                                                
@txtItemDesc as varchar(1000),                                                                
@charItemType as char(1),                                                                
@monListPrice as money,                                                                
@numItemClassification as numeric(9),                                                                
@bitTaxable as bit,                                                                
@vcSKU as varchar(50),                                                                                                                                      
@bitKitParent as bit,                                                                                   
--@dtDateEntered as datetime,                                                                
@numDomainID as numeric(9),                                                                
@numUserCntID as numeric(9),                                                                                                  
@numVendorID as numeric(9),                                                            
@bitSerialized as bit,                                               
@strFieldList as text,                                            
@vcModelID as varchar(200)='' ,                                            
@numItemGroup as numeric(9)=0,                                      
@numCOGSChartAcntId as numeric(9)=0,                                      
@numAssetChartAcntId as numeric(9)=0,                                      
@numIncomeChartAcntId as numeric(9)=0,                            
@monAverageCost as money,                          
@monLabourCost as money,                  
@fltWeight as float,                  
@fltHeight as float,                  
@fltLength as float,                  
@fltWidth as float,                  
@bitFreeshipping as bit,                
@bitAllowBackOrder as bit,              
@UnitofMeasure as varchar(30)='',              
@strChildItems as text,            
@bitShowDeptItem as bit,              
@bitShowDeptItemDesc as bit,        
@bitCalAmtBasedonDepItems as bit,    
@bitAssembly as BIT,
@intWebApiId INT=null,
@vcApiItemId AS VARCHAR(50)=null,
@numBarCodeId as VARCHAR(25)=null,
@vcManufacturer as varchar(250)=NULL,
@numBaseUnit AS NUMERIC(18)=0,
@numPurchaseUnit AS NUMERIC(18)=0,
@numSaleUnit AS NUMERIC(18)=0,
@bitLotNo as BIT,
@IsArchieve AS BIT,
@numItemClass as numeric(9)=0,
@tintStandardProductIDType AS TINYINT=0,
@vcExportToAPI VARCHAR(50),
@numShipClass NUMERIC,
@ProcedureCallFlag SMALLINT =0,
@vcCategories AS VARCHAR(2000) = '',
@bitAllowDropShip AS BIT=0,
@bitArchiveItem AS BIT = 0,
@bitAsset AS BIT = 0,
@bitRental AS BIT = 0
as                                                                
declare @hDoc as INT

if @numCOGSChartAcntId=0 set @numCOGSChartAcntId=null                      
if  @numAssetChartAcntId=0 set @numAssetChartAcntId=null  
if  @numIncomeChartAcntId=0 set @numIncomeChartAcntId=null     

DECLARE @ParentSKU VARCHAR(50)                                        
DECLARE @ItemID AS NUMERIC(9)
DECLARE @cnt AS INT

--if @dtDateEntered = 'Jan  1 1753 12:00:00:000AM' set @dtDateEntered=null 

IF ISNULL(@charItemType,'') = '' OR ISNULL(@charItemType,'') = '0'  SET @charItemType = 'N'

IF @intWebApiId = 2 
    AND LEN(ISNULL(@vcApiItemId, '')) > 0
    AND ISNULL(@numItemGroup, 0) > 0 
    BEGIN      
    
        -- check wether this id already Mapped in ITemAPI 
        SELECT  @cnt = COUNT([numItemID])
        FROM    [ItemAPI]
        WHERE   [WebApiId] = @intWebApiId
                AND [numDomainId] = @numDomainID
                AND [vcAPIItemID] = @vcApiItemId
        IF @cnt > 0 
            BEGIN
                SELECT  @ItemID = [numItemID]
                FROM    [ItemAPI]
                WHERE   [WebApiId] = @intWebApiId
                        AND [numDomainId] = @numDomainID
                        AND [vcAPIItemID] = @vcApiItemId
        --Update Existing Product coming from API
                SET @numItemCode = @ItemID
            END
    
    END

ELSE 
    IF @intWebApiId > 1 --AND  @intWebApiId <> 2
        AND LEN(ISNULL(@vcSKU, '')) > 0 
        BEGIN
            SET @ParentSKU = @vcSKU
            PRINT 1
            --DECLARE @ItemID AS NUMERIC(9)
            --DECLARE @cnt AS INT
    
       -- check wether this id already exist in Domain
       
            SELECT  @cnt = COUNT([numItemCode])
            FROM    [Item]
            WHERE   [numDomainId] = @numDomainID
                    AND ( vcSKU = @vcSKU
                          OR @vcSKU IN ( SELECT vcWHSKU
                                         FROM   dbo.WareHouseItems
                                         WHERE  numItemID = Item.[numItemCode]
                                                AND vcWHSKU = @vcSKU )
                        )
            IF @cnt > 0 
                BEGIN
                    SELECT  @ItemID = [numItemCode],
                            @ParentSKU = vcSKU
                    FROM    [Item]
                    WHERE   [numDomainId] = @numDomainID
                            AND ( vcSKU = @vcSKU
                                  OR @vcSKU IN (
                                  SELECT    vcWHSKU
                                  FROM      dbo.WareHouseItems
                                  WHERE     numItemID = Item.[numItemCode]
                                            AND vcWHSKU = @vcSKU )
                                )
                    SET @numItemCode = @ItemID
                END
            ELSE 
                BEGIN
			
    -- check wether this id already Mapped in ITemAPI 
                    SELECT  @cnt = COUNT([numItemID])
                    FROM    [ItemAPI]
                    WHERE   [WebApiId] = @intWebApiId
                            AND [numDomainId] = @numDomainID
                            AND [vcAPIItemID] = @vcApiItemId
                    IF @cnt > 0 
                        BEGIN
                            SELECT  @ItemID = [numItemID]
                            FROM    [ItemAPI]
                            WHERE   [WebApiId] = @intWebApiId
                                    AND [numDomainId] = @numDomainID
                                    AND [vcAPIItemID] = @vcApiItemId
        --Update Existing Product coming from API
                            SET @numItemCode = @ItemID
                        END
     
                END
        END

                                                           
if @numItemCode=0 or  @numItemCode is null
begin                                                                 
 insert into Item (                                             
  vcItemName,                                             
  txtItemDesc,                                             
  charItemType,                                             
  monListPrice,                                             
  numItemClassification,                                             
  bitTaxable,                                             
  vcSKU,                                             
  bitKitParent,                                             
  --dtDateEntered,                                              
  numVendorID,                                             
  numDomainID,                                             
  numCreatedBy,                                             
  bintCreatedDate,                                             
  bintModifiedDate,                                             
 numModifiedBy,                                              
  bitSerialized,                                     
  vcModelID,                                            
  numItemGroup,                                          
  numCOGsChartAcntId,                        
  numAssetChartAcntId,                                      
  numIncomeChartAcntId,                            
  monAverageCost,                          
  monCampaignLabourCost,                  
  fltWeight,                  
  fltHeight,                  
  fltWidth,                  
  fltLength,                  
  bitFreeShipping,                
  bitAllowBackOrder,              
  vcUnitofMeasure,            
  bitShowDeptItem,            
  bitShowDeptItemDesc,        
  bitCalAmtBasedonDepItems,    
  bitAssembly,
  numBarCodeId,
  vcManufacturer,numBaseUnit,numPurchaseUnit,numSaleUnit,bitLotNo,IsArchieve,numItemClass,tintStandardProductIDType,vcExportToAPI,numShipClass,
  bitAllowDropShip,bitArchiveItem,
  bitAsset,bitRental
   )                                                              
   values                                                                
    (                                                                
  @vcItemName,                                             
  @txtItemDesc,                                  
  @charItemType,                                             
  @monListPrice,                                             
  @numItemClassification,                                             
  @bitTaxable,                                             
  @ParentSKU,                       
  @bitKitParent,                                             
  --@dtDateEntered,                                             
  @numVendorID,                                             
  @numDomainID,                                             
  @numUserCntID,                                           
  getutcdate(),                                             
  getutcdate(),                                             
  @numUserCntID,                                             
  @bitSerialized,                                       
  @vcModelID,                                            
  @numItemGroup,                                      
  @numCOGsChartAcntId,                                      
  @numAssetChartAcntId,                                      
  @numIncomeChartAcntId,                            
  @monAverageCost,                          
  @monLabourCost,                  
  @fltWeight,                  
  @fltHeight,                  
  @fltWidth,                  
  @fltLength,                  
  @bitFreeshipping,                
  @bitAllowBackOrder,              
  @UnitofMeasure,            
  @bitShowDeptItem,            
  @bitShowDeptItemDesc,        
  @bitCalAmtBasedonDepItems,    
  @bitAssembly,                                                          
   @numBarCodeId,
@vcManufacturer,@numBaseUnit,@numPurchaseUnit,@numSaleUnit,@bitLotNo,@IsArchieve,@numItemClass,@tintStandardProductIDType,@vcExportToAPI,@numShipClass,
@bitAllowDropShip,@bitArchiveItem ,@bitAsset,@bitRental)                                             
 
 set @numItemCode = SCOPE_IDENTITY()
 
 --insert warehouse for inventory item
-- EXEC sp_xml_preparedocument @hDoc OUTPUT, @strFieldList      
-- insert into WareHouseItems (numItemID, numWareHouseID,numOnHand,monWListPrice,vcLocation,vcWHSKU,vcBarCode,numDomainID,numReorder)  
--	SELECT @numItemCode,numWareHouseID,OnHand,Price,Location,SKU,BarCode,@numDomainID,Reorder
--	FROM OPENXML(@hDoc,'/NewDataSet/WareHouse[Op_Flag=1]',2)                                                    
--    with(numWareHouseItemID numeric(9),                                                                        
--    numWareHouseID numeric(9),                    
--    Reorder numeric(9),                                                                       
--    OnHand numeric(9),                                            
--    Op_Flag tinyint,      
--    Price money,
--    Location Varchar(100),
--    SKU varchar(100),
--    BarCode varchar(100))
--    
--update WareHouseItems  set                                                                         
--   numItemID=@numItemCode,                                                                       
--   numWareHouseID=X.numWareHouseID,                                                              
--   numOnHand=X.OnHand,                     
--   numReorder=X.Reorder,      
--   monWListPrice=X.Price,
--   vcLocation=X.Location,
--   vcWHSKU=X.SKU,
--	vcBarCode=X.BarCode                              
--    From (SELECT numWareHouseItemID as WareHouseItemID,numWareHouseID,Reorder,OnHand,Op_Flag,Price,Location,SKU,BarCode                                                                        
--   FROM OPENXML(@hDoc,'/NewDataSet/WareHouse[Op_Flag=2]',2)                                                                         
--    with(numWareHouseItemID numeric(9),                                                                        
--    numWareHouseID numeric(9),                    
--    Reorder numeric(9),                                                                       
--    OnHand numeric(9),                                            
--    Op_Flag tinyint,      
--    Price money,
--    Location Varchar(100),
--    SKU varchar(100),
--    BarCode varchar(100)))X                                                                         
--  where  numWareHouseItemID=X.WareHouseItemID 
--    insert into ItemUOM (numItemCode, numUOMId,numDomainId)  
--	SELECT @numItemCode,numUOMId,@numDomainID
--	FROM OPENXML(@hDoc,'/NewDataSet/TableUOM',2)                                                    
--    with(numUOMId numeric(9))
  
 IF  @intWebApiId > 1 
   BEGIN
     -- insert new product
     --insert this id into linking table
     INSERT INTO [ItemAPI] (
	 	[WebApiId],
	 	[numDomainId],
	 	[numItemID],
	 	[vcAPIItemID],
	 	[numCreatedby],
	 	[dtCreated],
	 	[numModifiedby],
	 	[dtModified],vcSKU
	 ) VALUES     (@intWebApiId,
                 @numDomainID,
                 @numItemCode,
                 @vcApiItemId,
	 			 @numUserCntID,
	 			 GETUTCDATE(),
	 			 @numUserCntID,
	 			 GETUTCDATE(),@vcSKU
	 			 ) 
   
	--update lastUpdated Date                  
	--UPDATE [WebAPIDetail] SET [vcThirteenthFldValue] = GETUTCDATE() WHERE [WebApiId] =@intWebApiId AND [numDomainId] = @numDomainID
   END
 
 
end         
else if    @numItemCode>0  AND @intWebApiId > 0 
BEGIN 
	IF @ProcedureCallFlag =1 
		BEGIN
		DECLARE @ExportToAPIList AS VARCHAR(30)
		SELECT @ExportToAPIList = vcExportToAPI FROM dbo.Item WHERE numItemCode = @numItemCode
		IF @ExportToAPIList != ''
			BEGIN
			IF NOT EXISTS(select * from (SELECT * FROM dbo.Split(@ExportToAPIList ,','))as A where items= @vcExportToAPI ) 
				BEGIN
					SET @vcExportToAPI = @ExportToAPIList + ',' + @vcExportToAPI
				End
			ELSE
				BEGIN
					SET @vcExportToAPI = @ExportToAPIList 
				End
			END
		--update ExportToAPI String value
		UPDATE dbo.Item SET vcExportToAPI=@vcExportToAPI where numItemCode=@numItemCode  AND numDomainID=@numDomainID
					
			SELECT @cnt = COUNT([numItemID]) FROM [ItemAPI] WHERE  [WebApiId] = @intWebApiId 
																AND [numDomainId] = @numDomainID
																AND [vcAPIItemID] = @vcApiItemId
																AND numItemID = @numItemCode
			IF @cnt > 0
			BEGIN
				--update lastUpdated Date 
				UPDATE [ItemAPI] SET [dtModified] =GETUTCDATE() WHERE [WebApiId] =@intWebApiId AND [numDomainId] = @numDomainID
			END
			ELSE
				--Insert ItemAPI mapping
				BEGIN
					INSERT INTO [ItemAPI] (
						[WebApiId],
						[numDomainId],
						[numItemID],
						[vcAPIItemID],
						[numCreatedby],
						[dtCreated],
						[numModifiedby],
						[dtModified],vcSKU
					) VALUES     (@intWebApiId,
						@numDomainID,
						@numItemCode,
						@vcApiItemId,
						@numUserCntID,
						GETUTCDATE(),
						@numUserCntID,
						GETUTCDATE(),@vcSKU
					)	
				END

		END   
END
                                                       
else if    @numItemCode>0  AND @intWebApiId <= 0                                                           
begin

	DECLARE @OldGroupID NUMERIC 
	SELECT @OldGroupID = ISNULL(numItemGroup,0) FROM [Item] WHERE numitemcode = @numItemCode AND [numDomainId] = @numDomainID
	
--	--validate average cost, do not update average cost if item has sales/purchase orders
--	IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityItems OI WHERE OI.numItemCode =@numItemCode) >0
--	BEGIN
--		SELECT @monAverageCost = monAverageCost FROM dbo.Item WHERE numItemCode=@numItemCode
--	END

	DECLARE @monOldAverageCost AS DECIMAL(18,4);SET @monOldAverageCost=0
	SELECT @monOldAverageCost=ISNULL(monAverageCost,0) FROM Item WHERE numItemCode =@numItemCode AND [numDomainId] = @numDomainID
	
----	-- Remove below code block while you update production server
----	PRINT 'FROM ITEM UPDATE : Supplied VendorID =' + CONVERT(VARCHAR(10), @numVendorID)
----	IF ISNULL(@numVendorID,0) = 0
----	BEGIN
----		SELECT @numVendorID = ISNULL(numVendorID,0) FROM dbo.Item WHERE numItemCode = @numItemCode
----		--PRINT @numVendorID
----	END
----	PRINT 'FROM ITEM UPDATE : Modified VendorID =' + CONVERT(VARCHAR(10), @numVendorID)
----	--------------------------------------------------------                                          
	
 update item set vcItemName=@vcItemName,                           
  txtItemDesc=@txtItemDesc,                                             
  charItemType=@charItemType,                                             
  monListPrice=@monListPrice,                                             
  numItemClassification=@numItemClassification,                                             
  bitTaxable=@bitTaxable,                                             
  vcSKU = (CASE WHEN ISNULL(@ParentSKU,'') = '' THEN @vcSKU ELSE @ParentSKU END),                                             
  bitKitParent=@bitKitParent,                                             
  --dtDateEntered=@dtDateEntered,                                             
  numVendorID=@numVendorID,                                             
  numDomainID=@numDomainID,                                             
  bintModifiedDate=getutcdate(),                                             
  numModifiedBy=@numUserCntID,                                   
  bitSerialized=@bitSerialized,                                                         
  vcModelID=@vcModelID,                                            
  numItemGroup=@numItemGroup,                                      
  numCOGsChartAcntId=@numCOGsChartAcntId,                                      
  numAssetChartAcntId=@numAssetChartAcntId,                                      
  numIncomeChartAcntId=@numIncomeChartAcntId,                            
  --monAverageCost=@monAverageCost,                          
  monCampaignLabourCost=@monLabourCost,                
  fltWeight=@fltWeight,                  
  fltHeight=@fltHeight,                  
  fltWidth=@fltWidth,                  
  fltLength=@fltLength,                  
  bitFreeShipping=@bitFreeshipping,                
  bitAllowBackOrder=@bitAllowBackOrder,              
  vcUnitofMeasure=@UnitofMeasure,            
  bitShowDeptItem=@bitShowDeptItem,            
  bitShowDeptItemDesc=@bitShowDeptItemDesc,        
  bitCalAmtBasedonDepItems=@bitCalAmtBasedonDepItems,    
  bitAssembly=@bitAssembly ,
numBarCodeId=@numBarCodeId,
vcManufacturer=@vcManufacturer,numBaseUnit=@numBaseUnit,numPurchaseUnit=@numPurchaseUnit,numSaleUnit=@numSaleUnit,bitLotNo=@bitLotNo,
IsArchieve = @IsArchieve,numItemClass=@numItemClass,tintStandardProductIDType=@tintStandardProductIDType,vcExportToAPI=@vcExportToAPI,
numShipClass=@numShipClass,bitAllowDropShip=@bitAllowDropShip, bitArchiveItem = @bitArchiveItem,bitAsset=@bitAsset,bitRental=@bitRental
where numItemCode=@numItemCode  AND numDomainID=@numDomainID

IF NOT EXISTS(SELECT numItemCode FROM dbo.OpportunityItems WHERE numItemCode = @numItemCode)
	BEGIN
		IF (((SELECT ISNULL(SUM(numOnHand),0) FROM WareHouseItems WHERE numItemID = @numItemCode AND numDomainId=@numDomainID) = 0) AND ((SELECT ISNULL(monAverageCost,0) FROM Item  WHERE numItemCode = @numItemCode AND numDomainId=@numDomainID ) = 0))
		BEGIN
			UPDATE Item SET monAverageCost=@monAverageCost  where numItemCode=@numItemCode  AND numDomainID=@numDomainID	
		END
		--UPDATE Item SET monAverageCost=@monAverageCost  where numItemCode=@numItemCode  AND numDomainID=@numDomainID	
	END
	
 --If Average Cost changed then add in Tracking
	IF ISNULL(@monAverageCost,0) != ISNULL(@monOldAverageCost,0)
	BEGIN
		DECLARE @numWareHouseItemID NUMERIC(9);SET @numWareHouseItemID=0
		DECLARE @numTotal int;SET @numTotal=0
		DECLARE @i int;SET @i=0
		DECLARE @vcDescription AS VARCHAR(100);SET @vcDescription=''
		
	    SELECT @numTotal=COUNT(*) FROM WareHouseItems where numItemID=@numItemCode AND numDomainId=@numDomainId
	     
		WHILE @i < @numTotal
		BEGIN
			SELECT @numWareHouseItemID = min(numWareHouseItemID) FROM WareHouseItems 
				WHERE numItemID=@numItemCode AND numDomainId=@numDomainId and numWareHouseItemID > @numWareHouseItemID 
				
			IF @numWareHouseItemID>0
			BEGIN
				SET @vcDescription='Average Cost Changed Manually OLD : ' + CAST(ISNULL(@monOldAverageCost,0) AS VARCHAR(20))
				
				DECLARE @numDomain AS NUMERIC(18,0)
				SET @numDomain = @numDomainID
				EXEC dbo.USP_ManageWareHouseItems_Tracking
					@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)		
					@numReferenceID = @numItemCode, --  numeric(9, 0)
					@tintRefType = 1, --  tinyint
					@vcDescription = @vcDescription, --  varchar(100)
					@tintMode = 0, --  tinyint
					@numModifiedBy = @numUserCntID, --  numeric(9, 0)
					@numDomainID = @numDomain
		    END
		    
		    SET @i = @i + 1
		END
	END
 
 DECLARE @bitCartFreeShipping as  bit 
 SELECT @bitCartFreeShipping = bitFreeShipping FROM dbo.CartItems where numItemCode = @numItemCode  AND numDomainID = @numDomainID 
 
 IF @bitCartFreeShipping <> @bitFreeshipping
 BEGIN
    UPDATE dbo.CartItems SET bitFreeShipping = @bitFreeshipping WHERE numItemCode = @numItemCode  AND numDomainID=@numDomainID
 END 
 
 
 IF  @intWebApiId > 1 
   BEGIN
      SELECT @cnt = COUNT([numItemID])
    FROM   [ItemAPI]
    WHERE  [WebApiId] = @intWebApiId
           AND [numDomainId] = @numDomainID
           AND [vcAPIItemID] = @vcApiItemId
    IF @cnt > 0
      BEGIN
      --update lastUpdated Date 
      UPDATE [ItemAPI] SET [dtModified] =GETUTCDATE() WHERE [WebApiId] =@intWebApiId AND [numDomainId] = @numDomainID
      END
      ELSE
      --Insert ItemAPI mapping
      BEGIN
        INSERT INTO [ItemAPI] (
	 	[WebApiId],
	 	[numDomainId],
	 	[numItemID],
	 	[vcAPIItemID],
	 	[numCreatedby],
	 	[dtCreated],
	 	[numModifiedby],
	 	[dtModified],vcSKU
	 ) VALUES     (@intWebApiId,
                 @numDomainID,
                 @numItemCode,
                 @vcApiItemId,
	 			 @numUserCntID,
	 			 GETUTCDATE(),
	 			 @numUserCntID,
	 			 GETUTCDATE(),@vcSKU
	 			 ) 
      END

--UPDATE [WebAPIDetail] SET [vcThirteenthFldValue] = GETUTCDATE() WHERE [WebApiId] =@intWebApiId AND [numDomainId] = @numDomainID
--UPDATE [ItemAPI] SET [dtModified] =GETUTCDATE() WHERE [WebApiId] =@intWebApiId AND [numDomainId] = @numDomainID
--  SELECT * FROM dbo.ItemCategory
--  Insert into ItemCategory(numItemID,numCategoryID)                                            
--  (SELECT numItemID,numCategoryID
--  FROM OPENXML(@hDoc,'/ItemCategories/',2)                                                                         
--    with(numWareHouseItmsDTLID numeric(9),                                                                        
--    numWareHouseItemID numeric(9),                                                  
--   ))
   
   
   END                                                                               
-- kishan It is necessary to add item into itemTax table . 
 IF	@bitTaxable = 1
	BEGIN
		IF NOT EXISTS(SELECT * FROM dbo.ItemTax WHERE numItemCode = @numItemCode AND numTaxItemID = 0)
			BEGIN
			    	INSERT INTO dbo.ItemTax (
						numItemCode,
						numTaxItemID,
						bitApplicable
					) VALUES ( 
						/* numItemCode - numeric(18, 0) */ @numItemCode,
						/* numTaxItemID - numeric(18, 0)*/ 0,
						/* bitApplicable - bit */ 1 ) 						
			END
	END


--  Insert into WareHouseItmsDTL(numWareHouseItemID,vcSerialNo,vcComments,numQty)                                            
--  (SELECT numWareHouseItemID,vcSerialNo,Comments,numQty                                                                        
--  FROM OPENXML(@hDoc,'/NewDataSet/SerializedItems[Op_Flag=1]',2)                                                                         
--    with(numWareHouseItmsDTLID numeric(9),                                                                        
--    numWareHouseItemID numeric(9),                                                  
--    vcSerialNo varchar(100),                                            
--    Op_Flag tinyint,          
--    Comments varchar(1000),numQty NUMERIC(9)))
--                                                                                
--  update WareHouseItmsDTL set                                                                         
--   numWareHouseItemID=X.numWareHouseItemID,                                                                       
--   vcSerialNo=X.vcSerialNo,          
--    vcComments=X.Comments,numQty=X.numQty                                                                                          
--    From (SELECT numWareHouseItmsDTLID as WareHouseItmsDTLID,numWareHouseItemID,vcSerialNo,Op_Flag,Comments,numQty                              
--   FROM OPENXML(@hDoc,'/NewDataSet/SerializedItems[Op_Flag=2]',2)                                                                         
--    with(numWareHouseItmsDTLID numeric(9),                                                          
--    numWareHouseItemID numeric(9),                                                                        
--    vcSerialNo varchar(100),                                            
--    Op_Flag tinyint,          
--    Comments varchar(1000),numQty NUMERIC(9)))X                                                                         
--  where  numWareHouseItmsDTLID=X.WareHouseItmsDTLID                                            
--                                             
--  delete from  WareHouseItmsDTL where numWareHouseItmsDTLID in (SELECT numWareHouseItmsDTLID                                                                        
--   FROM OPENXML(@hDoc,'/NewDataSet/SerializedItems[Op_Flag=3]',2)                                   
--    with(numWareHouseItmsDTLID numeric(9),                              
--    numWareHouseItemID numeric(9),                                                                        
--    vcSerialNo varchar(100),                                            
--  Op_Flag tinyint)) 


--  SELECT X.*,ROW_NUMBER() OVER( order by X.numQty) AS ROWNUMBER
--INTO #TempTableWareHouseItmsDTLID
--FROM ( SELECT  numWareHouseItmsDTLID,numWareHouseItemID,vcSerialNo,Op_Flag,Comments,numQty,OldQty
--                  FROM      OPENXML(@hDoc, '/NewDataSet/SerializedItems',2)
--                            WITH ( numWareHouseItmsDTLID NUMERIC(9), numWareHouseItemID NUMERIC(9), vcSerialNo VARCHAR(100), Op_Flag TINYINT, Comments VARCHAR(1000),OldQty NUMERIC(9),numQty NUMERIC(9) )
--                ) X


--DECLARE @minROWNUMBER INT
--DECLARE @maxROWNUMBER INT
--DECLARE @Diff INT,@Op_Flag AS int
--DECLARE @Diff1 INT
--DECLARE @numWareHouseItemID NUMERIC(9)
--DECLARE @numWareHouseItmsDTLID NUMERIC(9)
--
--SELECT  @minROWNUMBER = MIN(ROWNUMBER) , @maxROWNUMBER =MAX(ROWNUMBER) FROM #TempTableWareHouseItmsDTLID
--
--WHILE  @minROWNUMBER <= @maxROWNUMBER
--    BEGIN
--   	    SELECT @Diff = numQty - OldQty,@numWareHouseItemID=numWareHouseItemID,@Op_Flag=Op_Flag,@numWareHouseItmsDTLID=numWareHouseItmsDTLID FROM #TempTableWareHouseItmsDTLID WHERE ROWNUMBER=@minROWNUMBER
--   	    
--   	    IF @Op_Flag=2 
--   	    BEGIN
--		   	 UPDATE WareHouseItmsDTL SET  numWareHouseItemID = X.numWareHouseItemID,
--                vcSerialNo = X.vcSerialNo,vcComments = X.Comments,numQty=X.numQty 
--                FROM #TempTableWareHouseItmsDTLID X INNER JOIN WareHouseItmsDTL W
--                ON X.numWareHouseItmsDTLID=W.numWareHouseItmsDTLID WHERE X.ROWNUMBER=@minROWNUMBER
--		END 
--   	    
--   	    ELSE IF @Op_Flag=1 
--   	    BEGIN
--   	       	  INSERT INTO WareHouseItmsDTL(numWareHouseItemID,vcSerialNo,vcComments,numQty)  
--		   	   SELECT  X.numWareHouseItemID,X.vcSerialNo,X.Comments,X.numQty  FROM #TempTableWareHouseItmsDTLID X 
--		   	   WHERE X.ROWNUMBER=@minROWNUMBER
--		END
--   	    
--   	    ELSE IF @Op_Flag=3 
--   	    BEGIN
--   	       	  delete from  WareHouseItmsDTL where numWareHouseItmsDTLID=@numWareHouseItmsDTLID
--		END
--		
--   	    update WareHouseItems SET numOnHand=numOnHand + @Diff where numWareHouseItemID = @numWareHouseItemID AND (numOnHand + @Diff)>=0
--  
--        SELECT  @minROWNUMBER = MIN(ROWNUMBER) FROM  #TempTableWareHouseItmsDTLID WHERE  [ROWNUMBER] > @minROWNUMBER
--    END	 
      
IF @charItemType='S' or @charItemType='N'                                                                       
BEGIN
	delete from WareHouseItmsDTL where numWareHouseItemID in (select numWareHouseItemID from WareHouseItems where numItemID=@numItemCode AND [WareHouseItems].[numDomainID] = @numDomainID)
	delete from WareHouseItems where numItemID=@numItemCode AND [WareHouseItems].[numDomainID] = @numDomainID
END

--	insert into WareHouseItems (numItemID, numWareHouseID,numOnHand,monWListPrice,vcLocation,vcWHSKU,vcBarCode,numDomainID,numReorder)  
--	SELECT @numItemCode,numWareHouseID,OnHand,Price,Location,SKU,BarCode,@numDomainID,Reorder
--	FROM OPENXML(@hDoc,'/NewDataSet/WareHouse[Op_Flag=1]',2)                                                    
--    with(numWareHouseItemID numeric(9),                                                                        
--    numWareHouseID numeric(9),                    
--    Reorder numeric(9),                                                                       
--    OnHand numeric(9),                                            
--    Op_Flag tinyint,      
--    Price money,
--    Location Varchar(100),
--    SKU varchar(100),
--    BarCode varchar(100))
--SELECT @@IDENTITY	

--  update WareHouseItems  set                                                                         
--   numItemID=@numItemCode,                                                                       
--   numWareHouseID=X.numWareHouseID,                                                              
--   numOnHand=X.OnHand,                     
--   numReorder=X.Reorder,      
--   monWListPrice=X.Price,
--   vcLocation=X.Location,
--   vcWHSKU=X.SKU,
--	vcBarCode=X.BarCode                              
--    From (SELECT numWareHouseItemID as WareHouseItemID,numWareHouseID,Reorder,OnHand,Op_Flag,Price,Location,SKU,BarCode                                                                        
--   FROM OPENXML(@hDoc,'/NewDataSet/WareHouse[Op_Flag=2]',2)                                                                         
--    with(numWareHouseItemID numeric(9),                                                                        
--    numWareHouseID numeric(9),                    
--    Reorder numeric(9),                                                                       
--    OnHand numeric(9),                                            
--    Op_Flag tinyint,      
--    Price money,
--    Location Varchar(100),
--    SKU varchar(100),
--    BarCode varchar(100)))X                                                                         
--  where  numWareHouseItemID=X.WareHouseItemID                                   
                                   
                                   
--    delete from  WareHouseItmsDTL where numWareHouseItemID in (SELECT numWareHouseItemID                                                                 
--   FROM OPENXML(@hDoc,'/NewDataSet/WareHouse[Op_Flag=3]',2)                                                                         
--    with(numWareHouseItemID numeric(9),                                            
--    Op_Flag tinyint))                                             
	
	/*Enforce valiation for child records*/
--	IF exists (SELECT * FROM [OpportunityMaster] OM INNER JOIN [OpportunityItems] OI ON OM.[numOppId] = OI.[numOppId] WHERE OM.[numDomainId]=@numDomainID AND [numWarehouseItmsID] IN (SELECT numWareHouseItemID FROM OPENXML(@hDoc,'/NewDataSet/WareHouse[Op_Flag=3]',2)
--    with(numWareHouseItemID numeric(9),
--    Op_Flag TINYINT)) )
--	BEGIN
--		raiserror('CHILD_WAREHOUSE',16,1);
--		RETURN ;
--	END

--  delete from  WareHouseItems where numWareHouseItemID in (SELECT numWareHouseItemID FROM OPENXML(@hDoc,'/NewDataSet/WareHouse[Op_Flag=3]',2)
--    with(numWareHouseItemID numeric(9),                                                                 
--    numWareHouseID numeric(9),                                                                        
--    OnHand numeric(9),                                            
--    Op_Flag tinyint))                                  
    /*commented by chintan, Obsolete, Reason: While inserting new Row numWareHouseItemID will be 0,1,2 incrementally. */
--  delete from  WareHouseItems where numItemID=@numItemCode and  numWareHouseItemID not in (SELECT numWareHouseItemID                                                                    
--   FROM OPENXML(@hDoc,'/NewDataSet/WareHouse',2)                                                                         
--    with(numWareHouseItemID numeric(9),                                    
--    numWareHouseID numeric(9),                                                                        
--    OnHand numeric(9),                                            
--    Op_Flag tinyint))                                        

/*If Item is Matrix item and changed to non matrix item then remove matrix attributes associated with- by chintan*/
--IF @OldGroupID>0 AND @numItemGroup = 0 
--BEGIN
--	DELETE FROM [CFW_Fld_Values_Serialized_Items] WHERE [bitSerialized] =0 AND 
--	[RecId] IN ( SELECT numWareHouseItemID FROM  OPENXML(@hDoc,'/NewDataSet/WareHouse',2) with(numWareHouseItemID numeric(9)) )
--END
                                         
                                             
                                                       
                                             
                                             
-- delete from WareHouseItmsDTL where numWareHouseItemID not in (select numWareHouseItemID from WareHouseItems)                                            
 
-- if @bitSerialized=0 AND @bitLotNo=0 delete from WareHouseItmsDTL where  numWareHouseItemID  in (select numWareHouseItemID from WareHouseItems where numItemID=@numItemCode)                                             
    
--    DELETE FROM  ItemUOM WHERE numItemCode=@numItemCode AND numDomainId=@numDomainId
    
--    insert into ItemUOM (numItemCode, numUOMId,numDomainId)  
--	SELECT @numItemCode,numUOMId,@numDomainID
--	FROM OPENXML(@hDoc,'/NewDataSet/TableUOM',2)                                                    
--    with(numUOMId numeric(9))                                        
   
    if @bitKitParent=1  OR @bitAssembly = 1              
    begin              
  declare @hDoc1 as int                                            
  EXEC sp_xml_preparedocument @hDoc1 OUTPUT, @strChildItems                                                                        
                                                                                                 
                                                                
   update ItemDetails set                                                                         
    numQtyItemsReq=X.QtyItemsReq,numWareHouseItemId=X.numWarehouseItmsID,numUOMId=X.numUOMId,vcItemDesc=X.vcItemDesc,sintOrder=X.sintOrder                                                                                          
     From (SELECT QtyItemsReq,ItemKitID,ChildItemID,numWarehouseItmsID,numUOMId,vcItemDesc,sintOrder                             
    FROM OPENXML(@hDoc1,'/NewDataSet/Table1',2)                                                                         
     with(ItemKitID numeric(9),                                                          
     ChildItemID numeric(9),                                                                        
     QtyItemsReq numeric(9),  
     numWarehouseItmsID numeric(9),numUOMId NUMERIC(9),vcItemDesc VARCHAR(1000),sintOrder int))X                                                                         
   where  numItemKitID=X.ItemKitID and numChildItemID=ChildItemID                
              
 end   
 ELSE
 BEGIN
 	DELETE FROM ItemDetails WHERE numItemKitID=@numItemCode
 END           
                                                               
end

declare  @rows as integer                                        
 EXEC sp_xml_preparedocument @hDoc OUTPUT, @strFieldList                                                                        
                                         
 SELECT @rows=count(*) FROM OPENXML (@hDoc,'/NewDataSet/CusFlds',2)                                                  
  WITH (Fld_ID numeric(9),Fld_Value varchar(100),RecId numeric(9))                                        
                                                                           
                                          
 if @rows>0                                        
 begin                                        
  Create table #tempTable ( ID INT IDENTITY PRIMARY KEY,                    
  Fld_ID numeric(9),                                        
  Fld_Value varchar(100),                                        
  RecId numeric(9),                                    
  bitSItems bit                                                                         
  )                                         
  insert into #tempTable (Fld_ID,Fld_Value,RecId,bitSItems)                                        
  SELECT * FROM OPENXML (@hDoc,'/NewDataSet/CusFlds',2)                                                  
  WITH (Fld_ID numeric(9),Fld_Value varchar(100),RecId numeric(9),bitSItems bit)                                           
                                         
  delete CFW_Fld_Values_Serialized_Items from CFW_Fld_Values_Serialized_Items C                                        
  inner join #tempTable T                          
  on   C.Fld_ID=T.Fld_ID and C.RecId=T.RecId and bitSerialized=bitSItems                                         
                                         
  drop table #tempTable                                        
                                                            
  insert into CFW_Fld_Values_Serialized_Items(Fld_ID,Fld_Value,RecId,bitSerialized)                                                  
  select Fld_ID,isnull(Fld_Value,'') as Fld_Value,RecId,bitSItems from(SELECT * FROM OPENXML (@hDoc,'/NewDataSet/CusFlds',2)                                                  
  WITH (Fld_ID numeric(9),Fld_Value varchar(100),RecId numeric(9),bitSItems bit)) X                                         
 end  

IF @numItemCode > 0
	BEGIN
			IF EXISTS(SELECT * FROM dbo.ItemCategory WHERE numItemID = @numItemCode)
			 BEGIN
					DELETE FROM dbo.ItemCategory WHERE numItemID = @numItemCode		
			 END 
	  IF @vcCategories <> ''	
		BEGIN
        INSERT INTO ItemCategory( numItemID , numCategoryID )  
        SELECT  @numItemCode AS numItemID,ID from dbo.SplitIDs(@vcCategories,',')	
		END
	END

 EXEC sp_xml_removedocument @hDoc              
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ManagePageNavigationAuthorization' ) 
    DROP PROCEDURE USP_ManagePageNavigationAuthorization
GO

CREATE PROCEDURE USP_ManagePageNavigationAuthorization
	@numGroupID		NUMERIC(18,0),
	@numTabID		NUMERIC(18,0),
    @numDomainID	NUMERIC(18,0),
    @strItems		VARCHAR(MAX)
AS 
    SET NOCOUNT ON  
    BEGIN TRY 
        BEGIN TRAN  
           
        DELETE  FROM dbo.TreeNavigationAuthorization WHERE numGroupID = @numGroupID 
													 AND numDomainID = @numDomainID
													 AND numTabID = @numTabID	
				
        DECLARE @hDocItem INT
        IF CONVERT(VARCHAR(MAX), @strItems) <> '' 
            BEGIN
                EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems
                
				IF @numTabID = 7 --Relationships
				BEGIN

					DELETE FROM TreeNodeOrder WHERE numGroupID = @numGroupID AND numDomainID = @numDomainID AND numTabID = @numTabID	

					SELECT  
						@numGroupID AS numGroupID,
						@numTabID AS numTabID,
						X.[numPageNavID] AS numPageNavID,
						X.[numListItemID] As numListItemID,
						X.[bitVisible] AS bitVisible,
						@numDomainID AS numDomainID,
						X.[numParentID] AS numParentID,
						X.tintType AS tintType,
						X.[numOrder] AS numOrder
					INTO
						#TMEP
					FROM    
						( 
							SELECT    
								*
							FROM      
								OPENXML (@hDocItem, '/NewDataSet/Table1', 2)
								WITH ( [numPageNavID] NUMERIC(18, 0), bitVisible BIT, tintType TINYINT, numListItemID NUMERIC(18,0),numParentID NUMERIC(18,0), numOrder INT)
						) X


					INSERT  INTO [dbo].[TreeNavigationAuthorization]
							(
							  [numGroupID]
							  ,[numTabID]
							  ,[numPageNavID]
							  ,[bitVisible]
							  ,[numDomainID]
							  ,tintType
							)
					SELECT
						numGroupID,
						numTabID,
						numPageNavID,
						bitVisible,
						numDomainID,
						tintType
					FROM
						#TMEP
					WHERE
						ISNULL(numPageNavID,0) <> 0

					INSERT  INTO [dbo].[TreeNodeOrder]
					(
						numTabID,
						numDomainID,
						numGroupID,
						numPageNavID,
						numListItemID,
						numParentID,
						bitVisible,
						tintType,
						numOrder
					)
					SELECT
						numTabID,
						numDomainID,
						numGroupID,
						numPageNavID,
						numListItemID,
						numParentID,
						bitVisible,
						tintType,
						numOrder
					FROM
						#TMEP
				END
				ELSE
				BEGIN
					INSERT  INTO [dbo].[TreeNavigationAuthorization]
                        (
                          [numGroupID]
						  ,[numTabID]
						  ,[numPageNavID]
						  ,[bitVisible]
						  ,[numDomainID]
						  ,tintType
                        )
                        SELECT  @numGroupID,
							    @numTabID,
							    X.[numPageNavID],
							    X.[bitVisible],
							    @numDomainID,
							    X.tintType
                        FROM    ( SELECT    *
                                  FROM      OPENXML (@hDocItem, '/NewDataSet/Table1', 2)
                                            WITH ( [numPageNavID] NUMERIC(18, 0), bitVisible BIT, tintType TINYINT)
                                ) X
				END
                EXEC sp_xml_removedocument @hDocItem
            END


        COMMIT TRAN 
    END TRY 
    BEGIN CATCH		
        DECLARE @strMsg VARCHAR(200)
        SET @strMsg = ERROR_MESSAGE()
        IF ( @@TRANCOUNT > 0 ) 
            BEGIN
                RAISERROR ( @strMsg, 16, 1 ) ;
                ROLLBACK TRAN
                RETURN 1
            END
    END CATCH	
/****** Object:  StoredProcedure [dbo].[USP_ManageShipFields]    Script Date: 07/26/2008 16:19:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_manageshipfields')
DROP PROCEDURE usp_manageshipfields
GO
CREATE PROCEDURE [dbo].[USP_ManageShipFields]
@numDomainID as numeric(9),
@numLIstItemID as numeric(9),
@str as text
as

delete from ShippingFieldValues where numDomainID=@numDomainID and numListItemID=@numLIstItemID
DECLARE @hDocItem int  
EXEC sp_xml_preparedocument @hDocItem OUTPUT, @str                       
   insert into                       
   ShippingFieldValues                                                                          
   (intShipFieldID,vcShipFieldValue,numDomainID,numListItemID)                      
   select X.intShipFieldID,X.vcShipFieldValue,@numDomainID,@numLIstItemID from(                                                                          
   SELECT *FROM OPENXML (@hDocItem,'/NewDataSet/ShipDTL',2)                                                                          
   WITH                       
   (                                                                          
   intShipFieldID integer,                                     
   vcShipFieldValue varchar(200)            
   ))X    

  EXEC sp_xml_removedocument @hDocItem
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageWorkFlowMaster')
DROP PROCEDURE USP_ManageWorkFlowMaster
GO
Create PROCEDURE [dbo].[USP_ManageWorkFlowMaster]                                        
	@numWFID numeric(18, 0) OUTPUT,
    @vcWFName varchar(200),
    @vcWFDescription varchar(500),   
    @numDomainID numeric(18, 0),
    @numUserCntID numeric(18, 0),
    @bitActive bit,
    @numFormID NUMERIC(18,0),
    @tintWFTriggerOn NUMERIC(18),
    @vcWFAction varchar(Max),
    @vcDateField VARCHAR(MAX),
    @intDays INT,
    @intActionOn INT,
    @strText TEXT = ''
as                 

IF @numWFID=0
BEGIN
	
	INSERT INTO [dbo].[WorkFlowMaster] ([numDomainID], [vcWFName], [vcWFDescription], [numCreatedBy], [numModifiedBy], [dtCreatedDate], [dtModifiedDate], [bitActive], [numFormID], [tintWFTriggerOn],[vcWFAction],[vcDateField],[intDays],[intActionOn])
	SELECT @numDomainID, @vcWFName, @vcWFDescription, @numUserCntID, @numUserCntID, GETUTCDATE(), GETUTCDATE(), @bitActive, @numFormID, @tintWFTriggerOn,@vcWFAction,@vcDateField,@intDays,@intActionOn

	SET @numWFID=@@IDENTITY
	--start of my
	IF DATALENGTH(@strText)>2
	BEGIN
		DECLARE @hDocItems INT                                                                                                                                                                
	
		EXEC sp_xml_preparedocument @hDocItems OUTPUT, @strText                                                                                                             
        
        --Delete records into WorkFlowTriggerFieldList and Insert
        DELETE FROM WorkFlowTriggerFieldList WHERE  [numWFID] = @numWFID

		INSERT INTO dbo.WorkFlowTriggerFieldList (numWFID,numFieldID,bitCustom) 
			SELECT @numWFID,numFieldID,bitCustom
			  FROM OPENXML (@hDocItems, '/NewDataSet/WorkFlowTriggerFieldList',2) WITH ( numFieldID NUMERIC, bitCustom BIT )
	
	
		--Delete records into WorkFlowConditionList and Insert
		DELETE FROM WorkFlowConditionList WHERE  [numWFID] = @numWFID

		INSERT INTO dbo.WorkFlowConditionList (numWFID,numFieldID,bitCustom,vcFilterValue,vcFilterOperator,vcFilterANDOR,vcFilterData,intCompare) 
			SELECT @numWFID,numFieldID,bitCustom,vcFilterValue,vcFilterOperator,vcFilterANDOR,vcFilterData,intCompare
			  FROM OPENXML (@hDocItems, '/NewDataSet/WorkFlowConditionList',2) WITH ( numFieldID NUMERIC, bitCustom BIT, vcFilterValue VARCHAR(500), vcFilterOperator VARCHAR(10),vcFilterANDOR VARCHAR(10),vcFilterData VARCHAR(MAX),intCompare int)


		--Delete records into WorkFlowActionUpdateFields and Insert
		DELETE FROM WorkFlowActionUpdateFields WHERE numWFActionID IN(SELECT numWFActionID FROM WorkFlowActionList WHERE numWFID=@numWFID)

		--Delete records into WorkFlowActionList and Insert
		DELETE FROM WorkFlowActionList WHERE  [numWFID] = @numWFID

		INSERT INTO dbo.WorkFlowActionList (numWFID,tintActionType,numTemplateID,vcEmailToType,tintTicklerActionAssignedTo,vcAlertMessage,tintActionOrder,tintIsAttachment,vcApproval,tintSendMail,numBizDocTypeID,numBizDocTemplateID,vcSMSText,vcEmailSendTo,vcMailBody,vcMailSubject) 
			SELECT @numWFID,tintActionType,numTemplateID,vcEmailToType,tintTicklerActionAssignedTo,vcAlertMessage,tintActionOrder,tintIsAttachment,vcApproval ,tintSendMail,numBizDocTypeID,numBizDocTemplateID,vcSMSText,vcEmailSendTo,vcMailBody ,vcMailSubject
			  FROM OPENXML (@hDocItems, '/NewDataSet/WorkFlowActionList',2) WITH ( tintActionType TINYINT, numTemplateID NUMERIC,vcEmailToType VARCHAR(50),tintTicklerActionAssignedTo TINYINT,vcAlertMessage NTEXT,tintActionOrder TINYINT,tintIsAttachment TINYINT,vcApproval VARCHAR(100),tintSendMail TINYINT,numBizDocTypeID numeric(18,0),numBizDocTemplateID numeric(18,0),vcSMSText NTEXT,vcEmailSendTo varchar(500),vcMailBody ntext,vcMailSubject varchar(500))

		INSERT INTO dbo.WorkFlowActionUpdateFields (numWFActionID,tintModuleType,numFieldID,bitCustom,vcValue,vcData) 
			SELECT WFA.numWFActionID,tintModuleType,numFieldID,bitCustom,vcValue,vcData
			  FROM OPENXML (@hDocItems, '/NewDataSet/WorkFlowActionUpdateFields',2) WITH ( numWFActionID NUMERIC, tintModuleType NUMERIC,numFieldID NUMERIC,bitCustom BIT,vcValue VARCHAR(500),vcData VARCHAR(500) ) WFAUF
			  JOIN WorkFlowActionList WFA ON WFA.tintActionOrder=WFAUF.numWFActionID WHERE WFA.numWFID=@numWFID

		EXEC sp_xml_removedocument @hDocItems
	END	
	--end of my
END
ELSE
BEGIN
	
	UPDATE [dbo].[WorkFlowMaster]
	SET    [vcWFName] = @vcWFName, [vcWFDescription] = @vcWFDescription, [numModifiedBy] = @numUserCntID, [dtModifiedDate] = GETUTCDATE(), [bitActive] = @bitActive, [numFormID] = @numFormID, [tintWFTriggerOn] = @tintWFTriggerOn,[vcWFAction]=@vcWFAction,[vcDateField]=@vcDateField,[intDays]=@intDays,[intActionOn]=@intActionOn
	WHERE  [numDomainID] = @numDomainID AND [numWFID] = @numWFID
	
	IF DATALENGTH(@strText)>2
	BEGIN
		DECLARE @hDocItem INT                                                                                                                                                                
	
		EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strText                                                                                                             
        
        --Delete records into WorkFlowTriggerFieldList and Insert
        DELETE FROM WorkFlowTriggerFieldList WHERE  [numWFID] = @numWFID

		INSERT INTO dbo.WorkFlowTriggerFieldList (numWFID,numFieldID,bitCustom) 
			SELECT @numWFID,numFieldID,bitCustom
			  FROM OPENXML (@hDocItem, '/NewDataSet/WorkFlowTriggerFieldList',2) WITH ( numFieldID NUMERIC, bitCustom BIT )
	
	
		--Delete records into WorkFlowConditionList and Insert
		DELETE FROM WorkFlowConditionList WHERE  [numWFID] = @numWFID

		INSERT INTO dbo.WorkFlowConditionList (numWFID,numFieldID,bitCustom,vcFilterValue,vcFilterOperator,vcFilterANDOR,vcFilterData,intCompare) 
			SELECT @numWFID,numFieldID,bitCustom,vcFilterValue,vcFilterOperator,vcFilterANDOR,vcFilterData,intCompare
			  FROM OPENXML (@hDocItem, '/NewDataSet/WorkFlowConditionList',2) WITH ( numFieldID NUMERIC, bitCustom BIT, vcFilterValue VARCHAR(500), vcFilterOperator VARCHAR(10),vcFilterANDOR VARCHAR(10),vcFilterData varchar(MAX),intCompare int)


		--Delete records into WorkFlowActionUpdateFields and Insert
		DELETE FROM WorkFlowActionUpdateFields WHERE numWFActionID IN(SELECT numWFActionID FROM WorkFlowActionList WHERE numWFID=@numWFID)

		--Delete records into WorkFlowActionList and Insert
		DELETE FROM WorkFlowActionList WHERE  [numWFID] = @numWFID

		INSERT INTO dbo.WorkFlowActionList (numWFID,tintActionType,numTemplateID,vcEmailToType,tintTicklerActionAssignedTo,vcAlertMessage,tintActionOrder,tintIsAttachment,vcApproval ,tintSendMail,numBizDocTypeID ,numBizDocTemplateID,vcSMSText,vcEmailSendTo,vcMailBody,vcMailSubject ) 
			SELECT @numWFID,tintActionType,numTemplateID,vcEmailToType,tintTicklerActionAssignedTo,vcAlertMessage,tintActionOrder,tintIsAttachment,vcApproval ,tintSendMail,numBizDocTypeID ,numBizDocTemplateID,vcSMSText ,vcEmailSendTo,vcMailBody,vcMailSubject
			  FROM OPENXML (@hDocItem, '/NewDataSet/WorkFlowActionList',2) WITH ( tintActionType TINYINT, numTemplateID NUMERIC,vcEmailToType VARCHAR(50),tintTicklerActionAssignedTo TINYINT,vcAlertMessage NTEXT,tintActionOrder TINYINT,tintIsAttachment TINYINT,vcApproval VARCHAR(100),tintSendMail TINYINT,numBizDocTypeID numeric(18,0),numBizDocTemplateID numeric(18,0),vcSMSText NTEXT ,vcEmailSendTo varchar(500),vcMailBody ntext,vcMailSubject varchar(500))

		INSERT INTO dbo.WorkFlowActionUpdateFields (numWFActionID,tintModuleType,numFieldID,bitCustom,vcValue,vcData) 
			SELECT WFA.numWFActionID,tintModuleType,numFieldID,bitCustom,vcValue,vcData
			  FROM OPENXML (@hDocItem, '/NewDataSet/WorkFlowActionUpdateFields',2) WITH ( numWFActionID NUMERIC, tintModuleType NUMERIC,numFieldID NUMERIC,bitCustom BIT,vcValue VARCHAR(500),vcData VARCHAR(500)) WFAUF
			  JOIN WorkFlowActionList WFA ON WFA.tintActionOrder=WFAUF.numWFActionID WHERE WFA.numWFID=@numWFID

		EXEC sp_xml_removedocument @hDocItem
	END	
END

	


	

	


	
/****** Object:  StoredProcedure [dbo].[USP_OPPCheckCanBeShipped]    Script Date: 07/26/2008 16:20:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec USP_OPPCheckCanBeShipped 5853,1  
--created by anoop jayaraj              
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_oppcheckcanbeshipped')
DROP PROCEDURE usp_oppcheckcanbeshipped
GO
CREATE PROCEDURE [dbo].[USP_OPPCheckCanBeShipped]              
@intOpportunityId as numeric(9),
@bitCheck AS BIT=0
as              
           
Create table #tempItem(vcItemName varchar(300))   
---reverting back to previous state if deal is being edited after it is closed                                                                                
DECLARE @OppType AS VARCHAR(2),@vcItemName as varchar(300),@vcKitItemName as varchar(300)
DECLARE @Sel AS TINYINT   
DECLARE @itemcode AS NUMERIC     
DECLARE @numWareHouseItemID AS NUMERIC                                 
DECLARE @numUnits AS NUMERIC                                            
DECLARE @numQtyShipped AS NUMERIC
DECLARE @numAllocation AS NUMERIC                                                                                                                          
DECLARE @numoppitemtCode AS NUMERIC(9)
DECLARE @Kit AS BIT                   
DECLARE @numQtyReceived AS NUMERIC(9)
DECLARE @bitAsset as BIT

set @Sel=0          
select @OppType=tintOppType from OpportunityMaster where numOppId=@intOpportunityId
                                            
 select top 1 @numoppitemtCode=numoppitemtCode,@itemcode=OI.numItemCode,@numUnits=ISNULL(numUnitHour,0),@numQtyShipped=ISNULL(numQtyShipped,0),@numQtyReceived=ISNULL(numUnitHourReceived,0),
 @numWareHouseItemID=numWarehouseItmsID,@bitAsset=ISNULL(I.bitAsset,0),@Kit= (case when bitKitParent=1 and bitAssembly=1 then 0 when bitKitParent=1 then 1 else 0 end),@vcItemName=OI.vcItemName from OpportunityItems OI                                            
 join Item I                                            
 on OI.numItemCode=I.numItemCode and numOppId=@intOpportunityId and (bitDropShip=0 or bitDropShip is null)                                        
 where charitemtype='P'  order by OI.numoppitemtCode                                              
 while @numoppitemtCode>0                               
 begin  
	
	if @OppType=1 AND @Kit = 1
    begin
		declare @numOppChildItemID as numeric(9)  
		declare @Kititemcode as numeric(9)  
		declare @numKItUnits as integer  
		declare @numKitWareHouseItemID as numeric(9)  
		declare @KitonHand as numeric(9)  
		declare @KitonAllocation as numeric(9)  
		declare @KitQtyShipped as numeric(9)  

  
		select top 1 @numOppChildItemID=OKI.numOppChildItemID,@Kititemcode=OKI.numChildItemID,@numKItUnits=numQtyItemsReq,          
			 @numKitWareHouseItemID=numWareHouseItemId,@vcKitItemName=I.vcItemName,
			 @KitQtyShipped=OKI.numQtyShipped from OpportunityKitItems OKI
			 join Item I on OKI.numChildItemID=I.numItemCode    
				where charitemtype='P' and numWareHouseItemId>0 and numWareHouseItemId is not null 
					and numOppItemID=@numoppitemtCode order by OKI.numOppChildItemID                                         
			 while @numOppChildItemID>0                                          
			  begin 
			                                      
			   select @KitonAllocation=isnull(numAllocation,0) from WareHouseItems where numWareHouseItemID=@numKitWareHouseItemID                  
								  
									                 
									/*below is added to be validated from Sales fulfillment*/
						 IF @bitCheck =1
						 BEGIN
								if @numKItUnits-@KitQtyShipped>0
							 begin  
									insert into #tempItem values (@vcItemName)                              
							 end            	
						 END
						 
						SET @numKItUnits =@numKItUnits - @KitQtyShipped
						if @numKItUnits>@KitonAllocation   
						 begin  
								insert into #tempItem values (@vcItemName)                                                               
						 end  
                                                                                                                                                        
			   
			select top 1 @numOppChildItemID=OKI.numOppChildItemID,@Kititemcode=OKI.numChildItemID,@numKItUnits=numQtyItemsReq,          
			 @numKitWareHouseItemID=numWareHouseItemId,@vcKitItemName=I.vcItemName,
			 @KitQtyShipped=OKI.numQtyShipped from OpportunityKitItems OKI                                                  
			 join Item I on OKI.numChildItemID=I.numItemCode    
				where charitemtype='P' and numWareHouseItemId>0 and numWareHouseItemId is not null 
					and numOppItemID=@numoppitemtCode and OKI.numOppChildItemID>@numOppChildItemID 
             order by OKI.numOppChildItemID                                                  
			   if @@rowcount=0 set @numOppChildItemID=0  
         end          
        end
	ELSE
	BEGIN

	
				select                                          
				@numAllocation=isnull(numAllocation,0)                                           
			  from WareHouseItems where numWareHouseItemID=@numWareHouseItemID                                              
			  if @OppType=1                                              
			  begin          
			   /*below is added to be validated from Sales fulfillment*/
				 IF @bitCheck =1
				 BEGIN
						if @numUnits-@numQtyShipped>0
					 begin  
					 If (@bitAsset=0 )
					 BEGIN
							 insert into #tempItem values (@vcItemName)     
					 END
							                         
					 end            	
				 END
				 
				SET @numUnits =@numUnits - @numQtyShipped
				if @numUnits>@numAllocation   
				 begin  
				  If (@bitAsset=0 )
					 BEGIN
							insert into #tempItem values (@vcItemName)  
					END                                                             
				 end            
			    
			  end    
			  ELSE IF @OppType=2
			  BEGIN
			   IF @bitCheck =1
			   BEGIN
   				 if @numUnits-@numQtyReceived>0
				 begin  
						insert into #tempItem values (@vcItemName)                              
				 end  
			   END
			  END
	END

	select @numUnits,@numQtyShipped
                                            
  select top 1 @numoppitemtCode=numoppitemtCode,@itemcode=OI.numItemCode,@numUnits=ISNULL(numUnitHour,0),@numQtyShipped=ISNULL(numQtyShipped,0),@numQtyReceived=ISNULL(numUnitHourReceived,0),
  @numWareHouseItemID=numWarehouseItmsID,@Kit= (case when bitKitParent=1 and bitAssembly=1 then 0 when bitKitParent=1 then 1 else 0 end),@vcItemName=OI.vcItemName from OpportunityItems OI                                            
  join Item I                                            
  on OI.numItemCode=I.numItemCode and numOppId=@intOpportunityId                                          
  where charitemtype='P' and OI.numoppitemtCode>@numoppitemtCode and (bitDropShip=0 or bitDropShip is null) order by OI.numoppitemtCode                                            
    
  if @@rowcount=0 set @numoppitemtCode=0    
 end   
  
select * from #tempItem
drop table #tempItem
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_oppdetailsdtlpl')
DROP PROCEDURE usp_oppdetailsdtlpl
GO
CREATE PROCEDURE [dbo].[USP_OPPDetailsDTLPL]
(
               @OpportunityId        AS NUMERIC(9)  = NULL,
               @numDomainID          AS NUMERIC(9),
               @ClientTimeZoneOffset AS INT)
AS
  SELECT Opp.numoppid,numCampainID,
--         (SELECT vcdata
--          FROM   listdetails
--          WHERE  numlistitemid = numCampainID) AS numCampainIDName,
		 CAMP.vcCampaignName AS [numCampainIDName],
         dbo.Fn_getcontactname(Opp.numContactId) numContactIdName,
		 numContactId,
         Opp.txtComments,
         Opp.numDivisionID,
         tintOppType,
         Dateadd(MINUTE,-@ClientTimeZoneOffset,bintAccountClosingDate) AS bintAccountClosingDate,
         dbo.fn_GetOpportunitySourceValue(ISNULL(Opp.tintSource,0),ISNULL(Opp.tintSourceType,0),Opp.numDomainID) AS tintSourceName,
          tintSource,
         Opp.VcPoppName,
         intpEstimatedCloseDate,
         [dbo].[getdealamount](@OpportunityId,Getutcdate(),0) AS CalAmount,
         --monPAmount,
         CONVERT(DECIMAL(10,2),Isnull(monPAmount,0)) AS monPAmount,
		lngPConclAnalysis,
         (SELECT vcdata
          FROM   listdetails
          WHERE  numlistitemid = lngPConclAnalysis) AS lngPConclAnalysisName,
         (SELECT vcdata
          FROM   listdetails
          WHERE  numlistitemid = numSalesOrPurType) AS numSalesOrPurTypeName,
		  numSalesOrPurType,
		 Opp.vcOppRefOrderNo,
		 Opp.vcMarketplaceOrderID,
         C2.vcCompanyName,
         D2.tintCRMType,
         Opp.tintActive,
         dbo.Fn_getcontactname(Opp.numCreatedby)
           + ' '
           + CONVERT(VARCHAR(20),Dateadd(MINUTE,-@ClientTimeZoneOffset,Opp.bintCreatedDate)) AS CreatedBy,
         dbo.Fn_getcontactname(Opp.numModifiedBy)
           + ' '
           + CONVERT(VARCHAR(20),Dateadd(MINUTE,-@ClientTimeZoneOffset,Opp.bintModifiedDate)) AS ModifiedBy,
         dbo.Fn_getcontactname(Opp.numRecOwner) AS RecordOwner,
         Opp.numRecOwner,
         Isnull(tintshipped,0) AS tintshipped,
         Isnull(tintOppStatus,0) AS tintOppStatus,
         [dbo].[GetOppStatus](Opp.numOppId) AS vcDealStatus,
         dbo.Opportunitylinkeditems(@OpportunityId) +
         (SELECT COUNT(*) FROM  dbo.Communication Comm JOIN Correspondence Corr ON Comm.numCommId=Corr.numCommID
			WHERE Comm.numDomainId=Opp.numDomainId AND Corr.numDomainID=Comm.numDomainID AND Corr.tintCorrType IN (1,2,3,4) AND Corr.numOpenRecordID=Opp.numOppId) AS NoOfProjects,
         (SELECT A.vcFirstName
                   + ' '
                   + A.vcLastName
          FROM   AdditionalContactsInformation A
          WHERE  A.numcontactid = opp.numAssignedTo) AS numAssignedToName,
		  opp.numAssignedTo,
         Opp.numAssignedBy,
         (SELECT COUNT(* )
          FROM   GenericDocuments
          WHERE  numRecID = @OpportunityId
                 AND vcDocumentSection = 'O') AS DocumentCount,
         CASE 
           WHEN tintOppStatus = 1 THEN 100
           ELSE isnull((SELECT SUM(tintPercentage)
                 FROM   OpportunityStageDetails
                 WHERE  numOppId = @OpportunityId
                        AND bitStageCompleted = 1),0)
         END AS TProgress,
         (SELECT TOP 1 Isnull(varRecurringTemplateName,'')
          FROM   RecurringTemplate
          WHERE  numRecurringId = OPR.numRecurringId) AS numRecurringIdName,
		 OPR.numRecurringId,
         Link.numOppID AS numParentOppID,
         Link.vcPoppName AS ParentOppName,
         Link.vcSource AS Source,
         ISNULL(C.varCurrSymbol,'') varCurrSymbol,
          ISNULL(Opp.fltExchangeRate,0) AS [fltExchangeRate],
           ISNULL(C.chrCurrency,'') AS [chrCurrency],
         ISNULL(C.numCurrencyID,0) AS [numCurrencyID],
         (SELECT COUNT(*) FROM [OpportunityLinking] OL WHERE OL.[numParentOppID] = Opp.numOppId) AS ChildCount,
         (SELECT COUNT(*) FROM [RecurringTransactionReport] RTR WHERE RTR.[numRecTranSeedId] = Opp.numOppId)  AS RecurringChildCount,
         (SELECT COUNT(*) FROM [ShippingReport] WHERE numOppBizDocId IN (SELECT numOppBizDocsId FROM   OpportunityBizDocs BD WHERE  BD.numOppId = @OpportunityId))AS ShippingReportCount,
         ISNULL(Opp.numStatus ,0) numStatus,
         (SELECT vcdata
          FROM   listdetails
          WHERE  numlistitemid = Opp.numStatus) AS numStatusName,
          (SELECT SUBSTRING(
	(SELECT ','+  A.vcFirstName+' ' +A.vcLastName + CASE WHEN ISNULL(SR.numContactType,0)>0 THEN '(' + dbo.fn_GetListItemName(SR.numContactType) + ')' ELSE '' END
	FROM ShareRecord SR JOIN UserMaster UM ON UM.numUserDetailId=SR.numAssignedTo 
							 JOIN AdditionalContactsInformation A ON UM.numUserDetailId=A.numContactID 
		 WHERE SR.numDomainID=Opp.numDomainID AND SR.numModuleID=3 AND SR.numRecordID=Opp.numOppId
				AND UM.numDomainID=Opp.numDomainID and UM.numDomainID=A.numDomainID FOR XML PATH('')),2,200000)) AS numShareWith,
		ISNULL((SELECT SUM(ISNULL(monDealAmount,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = @OpportunityId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0) AS monDealAmount,
		ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = @OpportunityId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0) AS monAmountPaid,
		ISNULL(Opp.tintSourceType,0) AS tintSourceType,
		ISNULL(Opp.tintTaxOperator,0) AS tintTaxOperator,  
             isnull(Opp.intBillingDays,0) AS numBillingDays,isnull(Opp.bitInterestType,0) AS bitInterestType,
             isnull(opp.fltInterest,0) AS fltInterest,  isnull(Opp.fltDiscount,0) AS fltDiscount,
             isnull(Opp.bitDiscountType,0) AS bitDiscountType,ISNULL(Opp.bitBillingTerms,0) AS bitBillingTerms,
             ISNULL(OPP.numDiscountAcntType,0) AS numDiscountAcntType,
              dbo.fn_getOPPAddress(Opp.numOppId,@numDomainID,1) AS BillingAddress,
            dbo.fn_getOPPAddress(Opp.numOppId,@numDomainID,2) AS ShippingAddress,
             ISNULL((SELECT SUBSTRING((SELECT '$^$' + ISNULL( (SELECT vcShipFieldValue FROM dbo.ShippingFieldValues WHERE numDomainID=Opp.numDomainID AND intShipFieldID IN ( SELECT intShipFieldID FROM dbo.ShippingFields WHERE numListItemID=OBD.numShipVia  AND vcFieldName='Tracking URL')),'') +'#^#'+ RTRIM(LTRIM(vcTrackingNo) )  FROM dbo.OpportunityBizDocs OBD WHERE OBD.numOppId=Opp.numOppID FOR XML PATH('')),4,200000)),'') AS  vcTrackingNo,
            ISNULL(numPercentageComplete,0) numPercentageComplete,
            dbo.GetListIemName(ISNULL(numPercentageComplete,0)) AS PercentageCompleteName,
            ISNULL(numBusinessProcessID,0) AS [numBusinessProcessID],
            CASE WHEN opp.tintOppType=1 THEN dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID) ELSE '' END AS vcInventoryStatus
            ,ISNULL(bitUseShippersAccountNo,0) [bitUseShippersAccountNo], ISNULL(vcShippersAccountNo,'') [vcShippersAccountNo] ,
			(SELECT SUM((ISNULL(monTotAmtBefDiscount, ISNULL(monTotAmount,0))- ISNULL(monTotAmount,0))) FROM OpportunityBizDocItems WHERE numOppBizDocID IN (SELECT numOppBizDocsId FROM OpportunityBizDocs WHERE numOppId = @OpportunityId AND bitAuthoritativeBizDocs = 1)) AS numTotalDiscount
  FROM   OpportunityMaster Opp
         JOIN divisionMaster D2 ON Opp.numDivisionID = D2.numDivisionID
         JOIN CompanyInfo C2 ON C2.numCompanyID = D2.numCompanyID
         LEFT JOIN (SELECT TOP 1 vcPoppName,
                                 numOppID,
                                 numChildOppID,
                                 vcSource,numParentOppID,OpportunityMaster.numDivisionID AS numParentDivisionID 
                    FROM   OpportunityLinking
                          left JOIN OpportunityMaster
                             ON numOppID = numParentOppID
                    WHERE  numChildOppID = @OpportunityId) Link
           ON Link.numChildOppID = Opp.numOppID
           LEFT OUTER JOIN [Currency] C
					ON C.numCurrencyID = Opp.numCurrencyID
			LEFT OUTER JOIN [OpportunityRecurring] OPR
					ON OPR.numOppId = Opp.numOppId
					 LEFT JOIN AddressDetails AD --Billing add
				ON AD.numDomainID=D2.numDomainID AND AD.numRecordID=D2.numDivisionID AND AD.tintAddressOf = 2 AND AD.tintAddressType = 1 AND AD.bitIsPrimary=1
			 LEFT JOIN AddressDetails AD2--Shipping address
				ON AD2.numDomainID=D2.numDomainID AND AD2.numRecordID=D2.numDivisionID AND AD2.tintAddressOf = 2 AND AD2.tintAddressType = 2 AND AD2.bitIsPrimary=1	
		LEFT JOIN dbo.CampaignMaster CAMP ON CAMP.numCampaignID = Opp.numCampainID 		
  WHERE  Opp.numOppId = @OpportunityId
         AND Opp.numDomainID = @numDomainID
/****** Object:  StoredProcedure [dbo].[USP_OPPItemDetails]    Script Date: 09/01/2009 18:59:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- exec USP_OPPItemDetails @numItemCode=31455,@OpportunityId=12387,@numDomainId=110
--created by anooop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_oppitemdetails')
DROP PROCEDURE usp_oppitemdetails
GO
CREATE PROCEDURE [dbo].[USP_OPPItemDetails]
    (
      @numUserCntID NUMERIC(9) = 0,
      @OpportunityId NUMERIC(9) = 0,
      @numDomainId NUMERIC(9),
      @ClientTimeZoneOffset  INT
    )
AS 
    BEGIN
        IF @OpportunityId >0
            BEGIN
                DECLARE @DivisionID AS NUMERIC(9)
                DECLARE @TaxPercentage AS FLOAT
                DECLARE @tintOppType AS TINYINT
                DECLARE @fld_Name AS VARCHAR(500)
                DECLARE @strSQL AS VARCHAR(8000)
                DECLARE @fld_ID AS NUMERIC(9)
                DECLARE @numBillCountry AS NUMERIC(9)
                DECLARE @numBillState AS NUMERIC(9)

                
                SELECT  @DivisionID = numDivisionID,
                        @tintOppType = tintOpptype
                FROM    OpportunityMaster
                WHERE   numOppId = @OpportunityId


               -- Calculate Tax based on Country and state
             /*  SELECT  @numBillState = ISNULL(numState, 0),
						@numBillCountry = ISNULL(numCountry, 0)
			   FROM     dbo.AddressDetails
			   WHERE    numDomainID = @numDomainID
						AND numRecordID = @DivisionID
						AND tintAddressOf = 2
						AND tintAddressType = 1
						AND bitIsPrimary=1
                SET @TaxPercentage = 0
                IF @numBillCountry > 0
                    AND @numBillState > 0 
                    BEGIN
                        IF @numBillState > 0 
                            BEGIN
                                IF EXISTS ( SELECT  COUNT(*)
                                            FROM    TaxDetails
                                            WHERE   numCountryID = @numBillCountry
                                                    AND numStateID = @numBillState
                                                    AND numDomainID = @numDomainID ) 
                                    SELECT  @TaxPercentage = decTaxPercentage
                                    FROM    TaxDetails
                                    WHERE   numCountryID = @numBillCountry
                                            AND numStateID = @numBillState
                                            AND numDomainID = @numDomainID
                                ELSE 
                                    SELECT  @TaxPercentage = decTaxPercentage
                                    FROM    TaxDetails
                                    WHERE   numCountryID = @numBillCountry
                                            AND numStateID = 0
                                            AND numDomainID = @numDomainID
                            END
                    END
                ELSE 
                    IF @numBillCountry > 0 
                        SELECT  @TaxPercentage = decTaxPercentage
                        FROM    TaxDetails
                        WHERE   numCountryID = @numBillCountry
                                AND numStateID = 0
                                AND numDomainID = @numDomainID
               
               */
               

SET @strSQL = ''

--------------Make Dynamic Query--------------
SET @strSQL = @strSQL + '
Select 
Opp.numoppitemtCode,
ISNULL(Opp.bitItemPriceApprovalRequired,0) AS bitItemPriceApprovalRequired,
OM.tintOppType,
Opp.numItemCode,
I.charItemType,
ISNULL(I.bitAsset,0) as bitAsset,
ISNULL(I.bitRental,0) as bitRental,
Opp.numOppId,
Opp.bitWorkOrder,
Opp.fltDiscount,
ISNULL(( SELECT numReturnID FROM   Returns WHERE  numOppItemCode = Opp.numoppitemtCode AND numOppId = Opp.numOppID), 0) AS numReturnID,
dbo.fn_GetReturnItemQty(Opp.numoppitemtCode) AS ReturrnedQty,
[dbo].fn_GetReturnStatus(Opp.numoppitemtCode) AS ReturnStatus,
ISNULL(opp.numUOMId, 0) AS numUOM,
(SELECT vcUnitName FROM dbo.UOM WHERE numUOMId = opp.numUOMId AND ISNULL(bitEnabled,0) = 1)  AS  [vcBaseUOMName],
Opp.numUnitHour AS numOriginalUnitHour, 
isnull(M.vcPOppName,''Internal'') as Source,
numSourceID,
  CASE WHEN ISNULL(bitKitParent,0) = 1
                               AND ISNULL(bitAssembly,0) = 1 THEN 0
                          WHEN ISNULL(bitKitParent,0) = 1 THEN 1
                          ELSE 0
                     END as bitKitParent,
(CASE WHEN (I.charItemType = ''p'' AND ISNULL(bitDropShip, 0) = 0 AND bitAsset=0)THEN dbo.CheckOrderItemInventoryStatus(Opp.numItemCode,Opp.numUnitHour,Opp.numoppitemtCode,isnull(Opp.numWarehouseItmsID,0),(CASE WHEN ISNULL(bitKitParent,0) = 1
                               AND ISNULL(bitAssembly,0) = 1 THEN 0
                          WHEN ISNULL(bitKitParent,0) = 1 THEN 1
                          ELSE 0
                     END)) ELSE 0 END) AS bitBackOrder,
(SELECT vcData FROM dbo.ListDetails WHERE 1=1 AND numListItemID = numShipClass ) AS [numShipClass],ISNULL(Opp.numQtyShipped,0) numQtyShipped,ISNULL(Opp.numUnitHourReceived,0) AS numUnitHourReceived,ISNULL(OM.fltExchangeRate,0) AS fltExchangeRate,ISNULL(OM.numCurrencyID,0) AS numCurrencyID,ISNULL(OM.numDivisionID,0) AS numDivisionID,
isnull(i.numIncomeChartAcntId,0) as itemIncomeAccount,isnull(i.numAssetChartAcntId,0) as itemInventoryAsset,isnull(i.numCOGsChartAcntId,0) as itemCoGs,
ISNULL(Opp.numRentalIN,0) as numRentalIN,
ISNULL(Opp.numRentalLost,0) as numRentalLost,
ISNULL(Opp.numRentalOut,0) as numRentalOut,
(SELECT vcData FROM dbo.ListDetails WHERE 1=1 AND numListItemID = numShipClass ) AS [numShipClass],ISNULL(bitFreeShipping,0) AS [IsFreeShipping],Opp.bitDiscountType,isnull(Opp.monLandedCost,0) as monLandedCost, '

--	IF @fld_Name = 'vcPathForTImage' 
		SET @strSQL = @strSQL + 'Opp.vcPathForTImage,'
--	ELSE IF @fld_Name = 'vcItemName' 
		SET @strSQL = @strSQL + 'Opp.vcItemName,'
--	ELSE IF @fld_Name = 'vcModelID,' 
		SET @strSQL = @strSQL + 'Opp.vcModelID,'
--	ELSE IF @fld_Name = 'vcWareHouse' 
		SET @strSQL = @strSQL + 'vcWarehouse,'
		SET @strSQL = @strSQL + 'WItems.numOnHand,isnull(WItems.numAllocation,0) as numAllocation,'
		SET @strSQL = @strSQL + 'WL.vcLocation,'
--	ELSE IF @fld_Name = 'ItemType' 
		SET @strSQL = @strSQL + 'Opp.vcType ItemType,Opp.vcType,'
--	ELSE IF @fld_Name = 'vcItemDesc' 
		SET @strSQL = @strSQL + 'ISNULL(Opp.vcItemDesc, '''') vcItemDesc,'
--	ELSE IF @fld_Name = 'Attributes' 
		SET @strSQL = @strSQL + 'Opp.vcAttributes,' --dbo.fn_GetAttributes(Opp.numWarehouseItmsID, I.bitSerialized) AS Attributes,
--	ELSE IF @fld_Name = 'bitDropShip' 
		SET @strSQL = @strSQL + 'ISNULL(bitDropShip, 0) as DropShip,CASE ISNULL(bitDropShip, 0) WHEN 1 THEN ''Yes'' ELSE '''' END AS bitDropShip,'
--	ELSE IF @fld_Name = 'numUnitHour' 
		SET @strSQL = @strSQL + 'CAST(( dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), Opp.numItemCode,om.numDomainId, ISNULL(opp.numUOMId, 0))* Opp.numUnitHour ) AS NUMERIC(18, 2)) AS numUnitHour,'
--	ELSE IF @fld_Name = 'monPrice' 
		SET @strSQL = @strSQL + 'Opp.monPrice,'
--	ELSE IF @fld_Name = 'monTotAmount' 
		SET @strSQL = @strSQL + 'Opp.monTotAmount,'
--	ELSE IF @fld_Name = 'vcSKU' 
		SET @strSQL = @strSQL + 'ISNULL(WItems.vcWHSKU,ISNULL(I.vcSKU,'''')) [vcSKU],'
--	ELSE IF @fld_Name = 'vcManufacturer' 
		SET @strSQL = @strSQL + 'Opp.vcManufacturer,'
--	ELSE IF @fld_Name = 'vcItemClassification' 
		SET @strSQL = @strSQL + ' dbo.[fn_GetListItemName](I.[numItemClassification]) numItemClassification,'
--	ELSE IF @fld_Name = 'numItemGroup' 
		SET @strSQL = @strSQL + '( SELECT    [vcItemGroup] FROM      [ItemGroups] WHERE     [numItemGroupID] = I.numItemGroup ) numItemGroup,'
--	ELSE IF @fld_Name = 'vcUOMName' 
		SET @strSQL = @strSQL + 'ISNULL(u.vcUnitName, '''') numUOMId,'
--	ELSE IF @fld_Name = 'fltWeight' 
		SET @strSQL = @strSQL + 'I.fltWeight,'
--	ELSE IF @fld_Name = 'fltHeight' 
		SET @strSQL = @strSQL + 'I.fltHeight,'
--	ELSE IF @fld_Name = 'fltWidth' 
		SET @strSQL = @strSQL + 'I.fltWidth,'
--	ELSE IF @fld_Name = 'fltLength' 
		SET @strSQL = @strSQL + 'I.fltLength,'
--	ELSE IF @fld_Name = 'numBarCodeId' 
		SET @strSQL = @strSQL + 'I.numBarCodeId,'
		SET @strSQL = @strSQL + 'I.IsArchieve,'
--	ELSE IF @fld_Name = 'vcWorkOrderStatus' 
		SET @strSQL = @strSQL + 'CASE WHEN ISNULL(Opp.bitWorkOrder, 0) = 1
				 THEN ( SELECT  dbo.GetListIemName(numWOStatus)
								+ CASE WHEN ISNULL(numWOStatus, 0) = 23184
									   THEN '' - ''
											+ CAST(ISNULL(dbo.fn_GetContactName(numAssignedTo),
														  '''') + '',''
											+ CONVERT(VARCHAR(20), DATEADD(minute, '+ CONVERT(VARCHAR(15),-@ClientTimeZoneOffset)  +', bintCompletedDate)) AS VARCHAR(100))
									   ELSE ''''
								  END
						FROM    WorkOrder
						WHERE   ISNULL(numOppId, 0) = Opp.numOppId
								AND numItemCode = Opp.numItemCode
					  )
				 ELSE ''''
			END AS vcWorkOrderStatus,'
--	ELSE IF @fld_Name = 'DiscAmt' 
		SET @strSQL = @strSQL + 'CASE WHEN Opp.bitDiscountType = 0
				 THEN CAST(ISNULL(Opp.monTotAmtBefDiscount, Opp.monTotAmount)
					  - Opp.monTotAmount AS VARCHAR(10)) + '' (''
					  + CAST(Opp.fltDiscount AS VARCHAR(10)) + ''%)''
				 ELSE CAST(ISNULL(Opp.monTotAmtBefDiscount, Opp.monTotAmount)
					  - Opp.monTotAmount AS VARCHAR(10))
			END AS DiscAmt,'	
--	ELSE IF @fld_Name = 'numProjectID' 
		SET @strSQL = @strSQL + ' ISNULL(Opp.numProjectID, 0) numProjectID,
		 CASE WHEN ( SELECT TOP 1
								ISNULL(OBI.numOppBizDocItemID, 0)
						FROM    dbo.OpportunityBizDocs OBD
								INNER JOIN dbo.OpportunityBizDocItems OBI ON OBD.numOppBizDocsID = OBI.numOppBizDocID
						WHERE   OBI.numOppItemID = Opp.numoppitemtCode
								AND ISNULL(OBD.bitAuthoritativeBizDocs, 0) = 1
								and isnull(OBD.numOppID,0)=OM.numOppID
					  ) > 0 THEN 1
				 ELSE 0
			END AS bitItemAddedToAuthBizDoc,'	
--	ELSE IF @fld_Name = 'numClassID' 
		SET @strSQL = @strSQL + 'ISNULL(Opp.numClassID, 0) numClassID,'	
--	ELSE IF @fld_Name = 'vcProjectStageName' 
		SET @strSQL = @strSQL + ' ISNULL(Opp.numProjectStageID, 0) numProjectStageID,
				CASE WHEN OPP.numProjectStageID > 0
				 THEN dbo.fn_GetProjectStageName(OPP.numProjectId,
												 OPP.numProjectStageID)
				 WHEN OPP.numProjectStageID = -1 THEN ''T&E''
				 WHEN OPP.numProjectId > 0
				 THEN CASE WHEN OM.tintOppType = 1 THEN ''S.O.''
						   ELSE ''P.O.''
					  END
				 ELSE ''-''
			END AS vcProjectStageName,'	
	        
	
	

--------------Add custom fields to query----------------
SELECT TOP 1 @fld_Name= vcFieldName,@fld_ID = numFieldId FROM View_DynamicCustomColumns
WHERE 
grp_id = 5 AND numDomainID=@numDomainId
AND numFormId=26 AND bitCustom=1 AND numUserCntID = @numUserCntID ORDER BY numFieldId
WHILE @fld_ID > 0
BEGIN ------------------

	 PRINT @fld_Name
	
	       SET @strSQL = @strSQL + 'dbo.GetCustFldValueItem('
                                    + CONVERT(VARCHAR(15), @fld_ID)
                                    + ',Opp.numItemCode) as ''' + @fld_Name + ''','
       

SELECT TOP 1 @fld_Name= vcFieldName,@fld_ID = numFieldId FROM View_DynamicCustomColumns
WHERE 
grp_id = 5 AND numDomainID=@numDomainId
AND numFormId=26 AND bitCustom=1  AND numUserCntID = @numUserCntID AND numFieldId > @fld_ID ORDER BY numFieldId
 IF @@ROWCOUNT=0
 SET @fld_ID = 0
END ------------------


                                
--Purchase Opportunity ?
IF @tintOppType = 2 
    BEGIN
        SET @strSQL =  @strSQL + 'SUBSTRING(( SELECT  '','' + vcSerialNo
            + CASE WHEN ISNULL(I.bitLotNo, 0) = 1
                   THEN '' ('' + CONVERT(VARCHAR(15), oppI.numQty)
                        + '')''
                   ELSE ''''
              END
			FROM    OppWarehouseSerializedItem oppI
					JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID = whi.numWareHouseItmsDTLID
			WHERE   oppI.numOppID = Opp.numOppId
					AND oppI.numOppItemID = Opp.numoppitemtCode
			ORDER BY vcSerialNo
		  FOR
			XML PATH('''')
		  ), 2, 200000) AS SerialLotNo,
		  CAST(Opp.numUnitHour AS NUMERIC(9, 0)) numQty,
		  (SELECT vcUnitName FROM dbo.UOM WHERE numUOMId = opp.numUOMId AND ISNULL(bitEnabled,0) = 1)  AS  [vcBaseUOMName],
		  I.bitSerialized,
		  ISNULL(I.bitLotNo, 0) AS bitLotNo,
		  isnull(Opp.numWarehouseItmsID,0) as numWarehouseItmsID,'
	END
ELSE
	BEGIN	
        SET @strSQL =  @strSQL + ' '''' AS SerialLotNo,
		   CAST(Opp.numUnitHour AS NUMERIC(9, 0)) numQty,
		  I.bitSerialized,
		  ISNULL(I.bitLotNo, 0) AS bitLotNo,
		  isnull(Opp.numWarehouseItmsID,0) as numWarehouseItmsID,'
	END


        
 SET @strSQL = left(@strSQL,len(@strSQL)-1) + '   
        FROM    OpportunityItems Opp
        LEFT JOIN OpportunityMaster OM ON Opp.numOppID = oM.numOppID
        JOIN item I ON Opp.numItemCode = i.numItemcode
        LEFT JOIN WareHouseItems WItems ON WItems.numWareHouseItemID = Opp.numWarehouseItmsID
        LEFT JOIN Warehouses W ON W.numWarehouseID = WItems.numWarehouseID
        LEFT JOIN WarehouseLocation WL ON WL.numWLocationID = WItems.numWLocationID
        LEFT JOIN OpportunityMaster M ON Opp.numSourceID = M.numOppID
        LEFT JOIN UOM u ON u.numUOMId = opp.numUOMId
WHERE   om.numDomainId = '+ CONVERT(VARCHAR(10),@numDomainId) + '
        AND Opp.numOppId = '+ CONVERT(VARCHAR(15),@OpportunityId) +'
ORDER BY numoppitemtCode ASC '
  
PRINT @strSQL;
EXEC (@strSQL);
SELECT  vcSerialNo,
       vcComments AS Comments,
        numOppItemID AS numoppitemtCode,
        W.numWarehouseItmsDTLID,
        numWarehouseItmsID AS numWItmsID,
        dbo.Fn_getattributes(W.numWarehouseItmsDTLID,
                             1) Attributes
FROM    WareHouseItmsDTL W
        JOIN OppWarehouseSerializedItem O ON O.numWarehouseItmsDTLID = W.numWareHouseItmsDTLID
WHERE   numOppID = @OpportunityId


CREATE TABLE #tempForm (tintOrder TINYINT,vcDbColumnName nvarchar(50),vcOrigDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),
  vcListItemType varchar(3),numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,
  numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,bitIsAlphaNumeric BIT,bitIsNumeric BIT,
  bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),ListRelID numeric(9),intColumnWidth int,intVisible int,vcFieldDataType CHAR(1))


IF
	(
		SELECT 
			COUNT(*)
		FROM 
			View_DynamicColumns DC 
		WHERE 
			DC.numFormId=26 and DC.numDomainID=@numDomainID AND numUserCntID = @numUserCntID AND
			ISNULL(DC.bitSettingField,0)=1 AND ISNULL(DC.bitDeleted,0)=0 AND ISNULL(DC.bitCustom,0)=0) > 0
	OR
	(
		SELECT 
			COUNT(*)
		FROM 
			View_DynamicCustomColumns
		WHERE 
			numFormId=26 AND numUserCntID=@numUserCntID AND numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=0 AND ISNULL(bitCustom,0)=1
	) > 0
BEGIN	

INSERT INTO #tempForm
select isnull(DC.tintRow,0)+1 as tintOrder,DDF.vcDbColumnName,DDF.vcOrigDbColumnName,ISNULL(DDF.vcCultureFieldName,DDF.vcFieldName),                  
 DDF.vcAssociatedControlType,DDF.vcListItemType,DDF.numListID,DDF.vcLookBackTableName                                                 
,DDF.bitCustom,DDF.numFieldId,DDF.bitAllowSorting,DDF.bitInlineEdit,
DDF.bitIsRequired,DDF.bitIsEmail,DDF.bitIsAlphaNumeric,DDF.bitIsNumeric,DDF.bitIsLengthValidation,
DDF.intMaxLength,DDF.intMinLength,DDF.bitFieldMessage,DDF.vcFieldMessage vcFieldMessage,DDF.ListRelID,DDF.intColumnWidth,Case when DC.numFieldID is null then 0 else 1 end,DDF.vcFieldDataType
 FROM View_DynamicDefaultColumns DDF left join View_DynamicColumns DC on DC.numFormId=26 and DDF.numFieldId=DC.numFieldId and DC.numUserCntID=@numUserCntID and DC.numDomainID=@numDomainID and numRelCntType=0 AND tintPageType=1 
 where DDF.numFormId=26 and DDF.numDomainID=@numDomainID AND ISNULL(DDF.bitSettingField,0)=1 AND ISNULL(DDF.bitDeleted,0)=0 AND ISNULL(DDF.bitCustom,0)=0
       
 UNION
    
    select tintRow+1 as tintOrder,vcDbColumnName,vcFieldName,vcFieldName,                  
 vcAssociatedControlType,'' as vcListItemType,numListID,''                                                 
,bitCustom,numFieldId,bitAllowSorting,0 as bitAllowEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,''
 from View_DynamicCustomColumns
 where numFormId=26 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=0
 AND ISNULL(bitCustom,0)=1

END
ELSE
BEGIN
	--Check if Master Form Configuration is created by administrator if NoColumns=0
	DECLARE @IsMasterConfAvailable BIT = 0
	DECLARE @numUserGroup NUMERIC(18,0)

	SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

	IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = 26 AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
	BEGIN
		SET @IsMasterConfAvailable = 1
	END

	--If MasterConfiguration is available then load it otherwise load default columns
	IF @IsMasterConfAvailable = 1
	BEGIN
		INSERT INTO #tempForm
		SELECT 
			(intRowNum + 1) as tintOrder, vcDbColumnName, vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
			vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName,bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,
			bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth,1,vcFieldDataType
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=26 AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcFieldName,vcFieldName,                  
			 vcAssociatedControlType,'' as vcListItemType,numListID,''                                                 
			,bitCustom,numFieldId,bitAllowSorting,0 as bitAllowEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,''
		FROM 
			View_DynamicCustomColumnsMasterConfig
		WHERE 
			View_DynamicCustomColumnsMasterConfig.numFormId=26 AND 
			View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1
		ORDER BY 
			tintOrder ASC  
	END
END


select * from #tempForm order by tintOrder

drop table #tempForm

/*
                        SET @strSQL = 'select  Opp.numoppitemtCode,Opp.numOppId,
                               Opp.numItemCode,                                    
                               Opp.vcItemName,                                                                    
                               ISNULL(Opp.vcItemDesc,'''') as [Desc],                                                                    
                               Opp.vcType ItemType,                                   
                               Opp.vcPathForTImage,
                               Opp.vcModelID,
							    CAST((dbo.fn_UOMConversion(ISNULL(I.numBaseUnit,0),Opp.numItemCode,om.numDomainId,ISNULL(opp.numUOMId,0)) * Opp.numUnitHour) as numeric(18,2)) as numUnitHour,   
                               Opp.monPrice,                                                                    
                               Opp.monTotAmount,                                                     
							   ISNULL(Opp.vcItemDesc,'''') vcItemDesc,
							   Opp.vcManufacturer,
							   I.charItemType,
							   I.bitKitParent,
							   I.bitAssembly,
							   I.vcSKU,                                   
							   I.numBarCodeId,
							   dbo.[fn_GetListItemName](I.[numItemClassification]) vcClassification,
							   (SELECT [vcItemGroup] FROM [ItemGroups] WHERE [numItemGroupID] =I.numItemGroup) vcItemGroup,
							   ISNULL(I.vcUnitofMeasure,'''') vcUnitofMeasure,
							   I.fltWeight,I.fltHeight,I.fltWidth,I.fltLength,
							   WItems.numWarehouseID,
                               Opp.numWarehouseItmsID,isnull(Opp.numToWarehouseItemID,0) numToWarehouseItemID,bitDropShip DropShip, CASE ISNULL(bitDropShip,0) when 1 then ''Yes'' else '''' end as vcDropShip,
                               0 as Op_Flag,isnull(Opp.bitDiscountType,0) as bitDiscountType,isnull(Opp.fltDiscount,0) as fltDiscount,isnull(monTotAmtBefDiscount,0) as monTotAmtBefDiscount,                                    
                               vcWarehouse as Warehouse,I.bitSerialized,isnull(I.bitLotNo,0) as bitLotNo,vcAttributes, 
                               dbo.fn_GetReturnItemQty(Opp.numoppitemtCode) as ReturrnedQty,
                               [dbo].fn_GetReturnStatus(Opp.numoppitemtCode) as ReturnStatus,
							   isnull((SELECT numReturnID FROM Returns WHERE numOppItemCode=Opp.numoppitemtCode and numOppId=Opp.numOppID),0) as numReturnID,
                               isnull(v.monCost,0) as VendorCost,
                               bitTaxable,
                               case when bitTaxable =0 then 0 when bitTaxable=1 then '
                            + CONVERT(VARCHAR(15), @TaxPercentage)
                            + ' end as Tax,                                                
                                   isnull(M.vcPOppName,''Internal'') as Source,dbo.fn_GetAttributes(Opp.numWarehouseItmsID,I.bitSerialized) as Attributes  ,numSourceID '
                            + CASE WHEN @strSQL = '' THEN ''
                                   ELSE ',' + @strSQL
                              END
                            + ',isnull(opp.numUOMId,0) AS numUOM,ISNULL(u.vcUnitName,'''') vcUOMName,dbo.fn_UOMConversion(opp.numUOMId,Opp.numItemCode,om.numDomainId,null) AS UOMConversionFactor
                          ,case when Opp.bitDiscountType=0 then  cast(isnull(Opp.monTotAmtBefDiscount,Opp.monTotAmount)-Opp.monTotAmount AS VARCHAR(10)) +  '' ('' + CAST(Opp.fltDiscount AS VARCHAR(10)) +  ''%)'' 
                          ELSE cast(isnull(Opp.monTotAmtBefDiscount,Opp.monTotAmount)-Opp.monTotAmount AS VARCHAR(10)) end as DiscAmt
                           ,isnull(I.numVendorID,0) numVendorID,OM.tintOppType,cast(Opp.numUnitHour as numeric(9,0)) as numQty,SUBSTRING(
(SELECT '','' + vcSerialNo + CASE WHEN isnull(I.bitLotNo,0)=1 THEN '' ('' + CONVERT(VARCHAR(15),whi.numQty) + '')'' ELSE '''' end 
FROM OppWarehouseSerializedItem oppI JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID=whi.numWareHouseItmsDTLID where oppI.numOppID=Opp.numOppId and oppI.numOppItemID=Opp.numoppitemtCode 
ORDER BY vcSerialNo FOR XML PATH('''')),2,200000) AS SerialLotNo, ISNULL(Opp.bitWorkOrder,0) as bitWorkOrder,
CASE WHEN ISNULL(Opp.bitWorkOrder,0)=1 then (select dbo.GetListIemName(numWOStatus) + CASE WHEN isnull(numWOStatus,0)=23184 THEN
'' - '' + CAST(ISNULL(dbo.fn_GetContactName(numAssignedTo), '''')+'',''+convert(varchar(20),dateadd(minute,' + CONVERT(VARCHAR(15),-@ClientTimeZoneOffset) + ',bintCompletedDate)) AS VARCHAR(100)) ELSE '''' end
 from WorkOrder where isnull(numOppId,0)=Opp.numOppId and numItemCode=Opp.numItemCode) else '''' end as vcWorkOrderStatus,
 ISNULL(Opp.numVendorWareHouse,0) as numVendorWareHouse,ISNULL(Opp.numShipmentMethod,0) as numShipmentMethod,ISNULL(Opp.numSOVendorId,0) as numSOVendorId,Opp.numUnitHour as numOriginalUnitHour,
 isnull(Opp.numProjectID,0) numProjectID,isnull(Opp.numClassID,0) numClassID,isnull(Opp.numProjectStageID,0) numProjectStageID,Case When OPP.numProjectStageID>0 then dbo.fn_GetProjectStageName(OPP.numProjectId,OPP.numProjectStageID) when OPP.numProjectStageID=-1 then ''T&E'' When OPP.numProjectId>0 then case when OM.tintOppType=1 then ''S.O.'' else ''P.O.'' end else ''-'' end as vcProjectStageName,
 CASE WHEN (SELECT TOP 1 ISNULL(OBI.numOppBizDocItemID,0) FROM dbo.OpportunityBizDocs OBD INNER JOIN dbo.OpportunityBizDocItems OBI ON OBD.numOppBizDocsID = OBI.numOppBizDocID WHERE OBI.numOppItemID = Opp.numoppitemtCode AND ISNULL(OBD.bitAuthoritativeBizDocs,0)=1 ) > 0
      THEN 1 ELSE 0 END AS bitItemAddedToAuthBizDoc,isnull(I.numItemClass,0) as numItemClass
                              from OpportunityItems Opp                    
                               left join OpportunityMaster OM                                                            
                                  on Opp.numOppID=oM.numOppID                                                                     
                                 join item I                                                                    
                                  on Opp.numItemCode=i.numItemcode                                            
                                left join Vendor v 
									on v.numVendorID= I.numVendorID and I.numItemCode =v.numItemCode
                               left join  WareHouseItems  WItems                                        
                               on   WItems.numWareHouseItemID= Opp.numWarehouseItmsID                                            
                               left join  Warehouses  W                                            
                                  on   W.numWarehouseID= WItems.numWarehouseID                                         
                                  left join OpportunityMaster M                                                            
                                  on Opp.numSourceID=M.numOppID  
                                  LEFT JOIN  UOM u ON u.numUOMId = opp.numUOMId  
                                  where om.numDomainId ='
                            + CONVERT(VARCHAR(15), @numDomainId)
                            + ' and Opp.numOppId='+ CONVERT(VARCHAR(15), @OpportunityId) +  
                            + CASE WHEN @numItemCode > 0 THEN ' AND Opp.numOppItemTCode = ' + CONVERT(VARCHAR(15), @numItemCode)
                                   ELSE ''
                              END
                            +
                            ' order by numoppitemtCode ASC '
                        PRINT @strSQL
                        EXEC ( @strSQL
                            )
                        SELECT  vcSerialNo,
                                vcComments AS Comments,
                                numOppItemID AS numoppitemtCode,
                                W.numWarehouseItmsDTLID,
                                numWarehouseItmsID AS numWItmsID,
                                dbo.Fn_getattributes(W.numWarehouseItmsDTLID,
                                                     1) Attributes
                        FROM    WareHouseItmsDTL W
                                JOIN OppWarehouseSerializedItem O ON O.numWarehouseItmsDTLID = W.numWareHouseItmsDTLID
                        WHERE   numOppID = @OpportunityId
*/
      /*              END
                ELSE 
                    BEGIN
                        SELECT  @numDomainID = numDomainId
                        FROM    OpportunityMaster
                        WHERE   numOppID = @OpportunityId
                        SET @strSQL = ''
                        SELECT TOP 1
                                @fld_ID = Fld_ID,
                                @fld_Name = Fld_label 
                        FROM    CFW_Fld_Master
                        WHERE   grp_id = 5
                                AND numDomainID = @numDomainID
                        WHILE @fld_ID > 0
                            BEGIN
                                SET @strSQL =@strSQL + 'dbo.GetCustFldValueItem('
                                    + CONVERT(VARCHAR(15), @fld_ID)
                                    + ',Opp.numItemCode) as ''' + @fld_Name + ''''
                                SELECT TOP 1
                                        @fld_ID = Fld_ID,
                                        @fld_Name = Fld_label
                                FROM    CFW_Fld_Master
                                WHERE   grp_id = 5
                                        AND numDomainID = @numDomainID
                                        AND Fld_ID > @fld_ID
                                IF @@ROWCOUNT = 0 
                                    SET @fld_ID = 0
                                IF @fld_ID <> 0 
                                    SET @strSQL = @strSQL + ','
                            END
                        SET @strSQL = 'select  Opp.numoppitemtCode,Opp.numOppId,                                       
                                Opp.numItemCode,                                    
                               Opp.vcItemName,                                                                    
                               ISNULL(Opp.vcItemDesc,'''') as [Desc],                                                                    
                               Opp.vcType ItemType,                                   
                               Opp.vcPathForTImage,
                               Opp.vcModelID,
                              CAST((dbo.fn_UOMConversion(ISNULL(I.numBaseUnit,0),Opp.numItemCode,om.numDomainId,ISNULL(opp.numUOMId,0)) * Opp.numUnitHour) as numeric(18,2)) as numUnitHour,   
							   Opp.monPrice,                                                                    
                               Opp.monTotAmount,                                                     
							   ISNULL(Opp.vcItemDesc,'''') vcItemDesc,
							   Opp.vcManufacturer,
							   I.charItemType,
							   I.bitKitParent,
							   I.bitAssembly,
							   I.vcSKU,
							   I.numBarCodeId,
							   dbo.[fn_GetListItemName](I.[numItemClassification]) vcClassification,
							   (SELECT [vcItemGroup] FROM [ItemGroups] WHERE [numItemGroupID] =I.numItemGroup) vcItemGroup,
							   ISNULL(I.vcUnitofMeasure,'''') vcUnitofMeasure,
							   I.fltWeight,I.fltHeight,I.fltWidth,I.fltLength,
							   WItems.numWarehouseID,
                               Opp.numWarehouseItmsID,isnull(Opp.numToWarehouseItemID,0) numToWarehouseItemID,bitDropShip DropShip, CASE ISNULL(bitDropShip,0) when 1 then ''Yes'' else '''' end as vcDropShip,
                               0 as Op_Flag,isnull(Opp.bitDiscountType,0) bitDiscountType,isnull(Opp.fltDiscount,0) fltDiscount,isnull(monTotAmtBefDiscount,0) monTotAmtBefDiscount,                                           
                               vcWarehouse as Warehouse,I.bitSerialized,isnull(I.bitLotNo,0) as bitLotNo,vcAttributes,
                               dbo.fn_GetReturnItemQty(Opp.numoppitemtCode) as ReturrnedQty,
                               [dbo].fn_GetReturnStatus(Opp.numoppitemtCode) as ReturnStatus,
							   isnull((SELECT numReturnID FROM Returns WHERE numOppItemCode=Opp.numoppitemtCode and numOppId=Opp.numOppID),0) as numReturnID,
                               isnull(v.monCost,0) as VendorCost,
                               bitTaxable,
                               case when bitTaxable =0 then 0 when bitTaxable=1 then '
                            + CONVERT(VARCHAR(15), @TaxPercentage)
                            + ' end as Tax,                                                
                                   isnull(m.vcPOppName,''Internal'') as Source,dbo.fn_GetAttributes(Opp.numWarehouseItmsID,I.bitSerialized) as Attributes ,numSourceID '
                            + CASE WHEN @strSQL = '' THEN ''
                                   ELSE ',' + @strSQL
                              END
                            + ',isnull(opp.numUOMId,0) AS numUOM,ISNULL(u.vcUnitName,'''') vcUOMName,dbo.fn_UOMConversion(opp.numUOMId,Opp.numItemCode,om.numDomainId,null) AS UOMConversionFactor
                          ,case when Opp.bitDiscountType=0 then  cast(isnull(Opp.monTotAmtBefDiscount,Opp.monTotAmount)-Opp.monTotAmount AS VARCHAR(10)) +  '' ('' + CAST(Opp.fltDiscount AS VARCHAR(10)) +  ''%)'' 
                          ELSE cast(isnull(Opp.monTotAmtBefDiscount,Opp.monTotAmount)-Opp.monTotAmount AS VARCHAR(10)) end as DiscAmt
                         ,isnull(I.numVendorID,0) numVendorID,OM.tintOppType,cast(Opp.numUnitHour as numeric(9,0)) numQty,SUBSTRING(
(SELECT '','' + vcSerialNo + CASE WHEN isnull(I.bitLotNo,0)=1 THEN '' ('' + CONVERT(VARCHAR(15),whi.numQty) + '')'' ELSE '''' end 
FROM OppWarehouseSerializedItem oppI JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID=whi.numWareHouseItmsDTLID where oppI.numOppID=Opp.numOppId and oppI.numOppItemID=Opp.numoppitemtCode 
ORDER BY vcSerialNo FOR XML PATH('''')),2,200000) AS SerialLotNo, ISNULL(Opp.bitWorkOrder,0) as bitWorkOrder,
CASE WHEN ISNULL(Opp.bitWorkOrder,0)=1 then (select dbo.GetListIemName(numWOStatus) + CASE WHEN isnull(numWOStatus,0)=23184 THEN
'' - '' + CAST(ISNULL(dbo.fn_GetContactName(numAssignedTo), '''')+'',''+convert(varchar(20),dateadd(minute,'+ CONVERT(VARCHAR(15),-@ClientTimeZoneOffset) +',bintCompletedDate)) AS VARCHAR(100)) ELSE '''' end
 from WorkOrder where isnull(numOppId,0)=Opp.numOppId and numItemCode=Opp.numItemCode) else '''' end as vcWorkOrderStatus,
 ISNULL(Opp.numVendorWareHouse,0) as numVendorWareHouse,ISNULL(Opp.numShipmentMethod,0) as numShipmentMethod,ISNULL(Opp.numSOVendorId,0) as numSOVendorId,Opp.numUnitHour as numOriginalUnitHour,
 isnull(Opp.numProjectID,0) numProjectID,isnull(Opp.numClassID,0) numClassID,isnull(Opp.numProjectStageID,0) numProjectStageID,Case When OPP.numProjectStageID>0 then dbo.fn_GetProjectStageName(OPP.numProjectId,OPP.numProjectStageID) when OPP.numProjectStageID=-1 then ''T&E'' When OPP.numProjectId>0 then case when OM.tintOppType=1 then ''S.O.'' else ''P.O.'' end else ''-'' end as vcProjectStageName,
 CASE WHEN (SELECT TOP 1 ISNULL(OBI.numOppBizDocItemID,0) FROM dbo.OpportunityBizDocs OBD INNER JOIN dbo.OpportunityBizDocItems OBI ON OBD.numOppBizDocsID = OBI.numOppBizDocID WHERE OBI.numOppItemID = Opp.numoppitemtCode AND ISNULL(OBD.bitAuthoritativeBizDocs,0)=1 ) > 0
 THEN 1 ELSE 0 END AS bitItemAddedToAuthBizDoc,isnull(I.numItemClass,0) as numItemClass
              from OpportunityItems Opp                    
                               left join OpportunityMaster OM                                                            
                                  on Opp.numOppID=oM.numOppID                                    
                                 join item I                                                                    
                                  on Opp.numItemCode=i.numItemcode   
                                  left join Vendor v 
									on v.numVendorID= I.numVendorID and I.numItemCode =v.numItemCode
                                left join  WareHouseItems  WItems                                        
                                on   WItems.numWareHouseItemID= Opp.numWarehouseItmsID                                            
                                left join  Warehouses  W                                            
                                  on   W.numWarehouseID= WItems.numWarehouseID                                         
                                  left join OpportunityMaster M                                                            
                                  on Opp.numSourceID=M.numOppID                      
                                        LEFT JOIN  UOM u ON u.numUOMId = opp.numUOMId
                                  where om.numDomainId ='
                            + CONVERT(VARCHAR(15), @numDomainId)
                            + ' and Opp.numOppId='+ CONVERT(VARCHAR(15), @OpportunityId) +  
                            + CASE WHEN @numItemCode > 0 THEN ' AND Opp.numOppItemTCode = ' + CONVERT(VARCHAR(15), @numItemCode)
                                   ELSE ''
                              END
                            +
                            ' order by numoppitemtCode ASC '
                        PRINT @strSQL
                        EXEC ( @strSQL
                            )
                        SELECT  vcSerialNo,
                                vcComments AS Comments,
                                numOppItemID AS numoppitemtCode,
                                W.numWarehouseItmsDTLID,
                                numWarehouseItmsID AS numWItmsID,
                                dbo.Fn_getattributes(W.numWarehouseItmsDTLID,
                                                     1) Attributes
                        FROM    OppWarehouseSerializedItem O
                                LEFT JOIN WareHouseItmsDTL W ON O.numWarehouseItmsDTLID = W.numWareHouseItmsDTLID
                        WHERE   numOppID = @OpportunityId
                    END
            */
            END
            
    END
/****** Object:  StoredProcedure [dbo].[USP_OppShippingorReceiving]    Script Date: 07/26/2008 16:20:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- created by anoop jayaraj            
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_oppshippingorreceiving')
DROP PROCEDURE usp_oppshippingorreceiving
GO
CREATE PROCEDURE [dbo].[USP_OppShippingorReceiving]            
@OppID as numeric(9),
@numUserCntID AS NUMERIC(9)          
as
BEGIN TRY
	
	DECLARE @numDomain AS NUMERIC(18,0)
	SELECT @numDomain = OM.numDomainID FROM [dbo].[OpportunityMaster] AS OM WHERE [OM].[numOppId] = @OppID
	PRINT @numDomain
	-- Added by Manish Anjara : 17th Sep,2014
	-- Check whether order is already closed or not. If closed then show a warning message
	IF (SELECT ISNULL(tintshipped,0) FROM [dbo].[OpportunityMaster] WHERE [OpportunityMaster].[numOppId] = @OppID AND [OpportunityMaster].[numDomainId] = @numDomain) = 1
	BEGIN
		RAISERROR('ORDER_CLOSED', 16, 1)
	END		

   BEGIN TRANSACTION 
			
			update OpportunityMaster set tintshipped=1,bintAccountClosingDate=GETUTCDATE(), bintClosedDate=GETUTCDATE() where  numOppId=@OppID 
			UPDATE [OpportunityBizDocs] SET [dtShippedDate] = GETUTCDATE() WHERE [numOppId]=@OppID
			
						
			declare @status as varchar(2)            
			declare @OppType as varchar(2)   
			declare @bitStockTransfer as BIT
			DECLARE @fltExchangeRate AS FLOAT 
         
			select @status=tintOppStatus,@OppType=tintOppType,@bitStockTransfer=ISNULL(bitStockTransfer,0),@fltExchangeRate=(CASE WHEN ISNULL(fltExchangeRate,0)=0 THEN 1 ELSE fltExchangeRate END) from OpportunityMaster
			where numOppId=@OppID
			
			-- Archive item based on Item's individual setting
			IF @OppType = 1
			BEGIN
				UPDATE Item SET IsArchieve = CASE WHEN EXISTS(SELECT OpportunityItems.numItemCode FROM dbo.OpportunityItems WHERE OpportunityItems.numItemCode = I.numItemCode) 
												  THEN 1
												  ELSE 0 
											 END 
				FROM Item I JOIN dbo.OpportunityItems OI ON I.numItemCode = OI.numItemCode  
				WHERE numOppId = @OppID 
				AND I.[numDomainID] = @numDomain

				AND ISNULL(I.bitArchiveItem,0) = 1 
			END
			----------------------------------------------------------------------------            
			--Updating Item Details            
			-----------------------------------------------------------------------            
            
			---Updating the inventory Items once the deal is won
			declare @numoppitemtCode as numeric
			declare @numUnits as numeric
			declare @numWarehouseItemID as numeric       
			declare @numToWarehouseItemID as numeric(9) --to be used with stock transfer
			declare @itemcode as numeric        
			declare @QtyShipped as NUMERIC
			declare @QtyReceived as numeric
			Declare @monPrice as money
			declare @Kit as bit

    
			 select top 1 @numoppitemtCode=numoppitemtCode,@itemcode=OI.numItemCode,@numUnits=ISNULL(numUnitHour,0),@qtyShipped=ISNULL([numQtyShipped],0),@qtyReceived=ISNULL([numUnitHourReceived],0),
			 @numWareHouseItemID=ISNULL(numWarehouseItmsID,0),@numToWarehouseItemID=numToWarehouseItemID,@monPrice=isnull(monPrice,0) * @fltExchangeRate,@Kit= (case when bitKitParent=1 and bitAssembly=1 then 0 when bitKitParent=1 then 1 else 0 end) 
			 from OpportunityItems OI join Item I                                                
			 on OI.numItemCode=I.numItemCode and numOppId=@OppID and (bitDropShip=0 or bitDropShip is null)                                            
			 where (charitemtype='P' OR 1=(CASE WHEN @OppType=1 THEN 
							CASE WHEN ISNULL(I.bitKitParent,0) = 1 AND ISNULL(I.bitAssembly,0) = 1 THEN 0
								 WHEN ISNULL(I.bitKitParent,0) = 1 THEN 1
								 ELSE 0 END 
							ELSE 0 END))  
			 AND I.[numDomainID] = @numDomain	
							order by OI.numoppitemtCode      

			 while @numoppitemtCode>0                
			 begin   
 
--			  if @Kit = 1
--			  begin
--					exec USP_UpdateKitItems @numoppitemtCode,@numUnits,@OppType,2,@OppID
--			  end
             
             
				IF @bitStockTransfer = 1
				BEGIN
					--Make inventory changes as if sales order was closed , using OpportunityItems.numWarehouseItmsID
					EXEC usp_ManageInventory @itemcode,@numWareHouseItemID,0,1,@numUnits,@qtyShipped,@qtyReceived,@monPrice,2,@OppID,@numoppitemtCode,@numUserCntID
					--Make inventory changes as if purchase order was closed, using OpportunityItems.numToWarehouseItemID
					EXEC usp_ManageInventory @itemcode,@numToWarehouseItemID,0,2,@numUnits,@qtyShipped,@qtyReceived,@monPrice,2,@OppID,@numoppitemtCode,@numUserCntID
				END
				ELSE
				BEGIN
					EXEC usp_ManageInventory @itemcode,@numWareHouseItemID,0,@OppType,@numUnits,@qtyShipped,@qtyReceived,@monPrice,2,@OppID,@numoppitemtCode,@numUserCntID
				END
        
			  select top 1 @numoppitemtCode=numoppitemtCode,@itemcode=OI.numItemCode,@numUnits=ISNULL(numUnitHour,0),@qtyShipped=ISNULL([numQtyShipped],0),@qtyReceived=ISNULL([numUnitHourReceived],0),
			  @numWareHouseItemID=ISNULL(numWarehouseItmsID,0),@numToWarehouseItemID=numToWarehouseItemID,@monPrice=isnull(monPrice,0) * @fltExchangeRate,@Kit= (case when bitKitParent=1 and bitAssembly=1 then 0 when bitKitParent=1 then 1 else 0 end) 
			  from OpportunityItems OI
			  join Item I
			  on OI.numItemCode=I.numItemCode and numOppId=@OppID                                              
			  where (charitemtype='P' OR 1=(CASE WHEN @OppType=1 THEN 
							CASE WHEN ISNULL(I.bitKitParent,0) = 1 AND ISNULL(I.bitAssembly,0) = 1 THEN 0
								 WHEN ISNULL(I.bitKitParent,0) = 1 THEN 1
								 ELSE 0 END 
							ELSE 0 END)) and OI.numoppitemtCode>@numoppitemtCode and (bitDropShip=0 or bitDropShip is null) 
			  AND I.numDomainID = @numDomain
			  ORDER by OI.numoppitemtCode                                                
			  if @@rowcount=0 set @numoppitemtCode=0     
  
			 END
				--Update WareHouseItmsDTL : from tintStatus 2 to 0 (PO Serial/Lot No)

			IF @OppType=2
			BEGIn
			   update WareHouseItmsDTL SET tintStatus=0 FROM WareHouseItmsDTL owsi JOIN OppWarehouseSerializedItem opp 
			  ON owsi.numWareHouseItmsDTLID=opp.numWareHouseItmsDTLID where opp.numOppID=@OppID AND owsi.tintStatus=2
			END
			IF @OppType=1
			BEGIN
			 UPDATE  WareHouseItmsDTL SET WareHouseItmsDTL.numQty=WareHouseItmsDTL.numQty - isnull(OppWarehouseSerializedItem.numQty,0)
			 from   OppWarehouseSerializedItem join WareHouseItmsDTL
			 on WareHouseItmsDTL.numWareHouseItmsDTLID= OppWarehouseSerializedItem.numWarehouseItmsDTLID                           
			 where numOppID=@OppID      
			END

 COMMIT
END TRY
BEGIN CATCH
  -- Whoops, there was an error
  IF @@TRANCOUNT > 0
     ROLLBACK

  -- Raise an error with the details of the exception
  DECLARE @ErrMsg nvarchar(4000), @ErrSeverity int
  SELECT @ErrMsg = ERROR_MESSAGE(),
         @ErrSeverity = ERROR_SEVERITY()

  RAISERROR(@ErrMsg, @ErrSeverity, 1)
END CATCH

GO
/****** Object:  StoredProcedure [dbo].[USP_GetPageElementAttributes]    Script Date: 08/08/2009 16:15:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--EXEC [USP_GetPageElementAttributes] @numSiteID = 3,@numElementID =4
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Organization_CT')
DROP PROCEDURE USP_Organization_CT
GO
-- =============================================
-- Author:		<Author,,Sachin Sadhu>
-- Create date: <Create Date,,23April2014>
-- Description:	<Description,,Change Tracking>
-- =============================================
Create PROCEDURE [dbo].[USP_Organization_CT]
	-- Add the parameters for the stored procedure here
    @numDomainID numeric(18, 0)=0,
    @numUserCntID numeric(18, 0)=0,
    @numRecordID numeric(18, 0)=0 
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;
 -- Insert statements for procedure here
 declare @tblFields as table (

 FieldName varchar(200),
 FieldValue varchar(200)
)

Declare @Type char(1)
Declare @ChangeVersion bigint
Declare @CreationVersion bigint
Declare @last_synchronization_version bigInt=null

SELECT 			
@ChangeVersion=Ct.Sys_Change_version,@CreationVersion=Ct.Sys_Change_Creation_version
FROM 
CHANGETABLE(CHANGES dbo.DivisionMaster, 0) AS CT
WHERE numDivisionID=@numRecordID

Begin

SET @last_synchronization_version=@ChangeVersion-1     
	
		--Fields Update & for Type :Insert or Update:
			Declare @UFFields as table 
			(
			    bitActiveInActive varchar(70),
				numAssignedBy varchar(70),
				numAssignedTo varchar(70),
				numCampaignID varchar(70),
				numCompanyDiff varchar(70),
				vcCompanyDiff varchar(70), 
				numCurrencyID varchar(70),
				numFollowUpStatus1 varchar(70),
				numFollowUpStatus varchar(70),
				numGrpID varchar(70),
				bintCreatedDate varchar(70),
				vcComFax varchar(70),
				bitPublicFlag varchar(70),
				tintCRMType varchar(70),
				bitNoTax varchar(70),
				numTerID varchar(70),
				numStatusID varchar(70),
				vcComPhone varchar(70)
			)

	INSERT into @UFFields(bitActiveInActive,numAssignedBy,numAssignedTo,numCampaignID,numCompanyDiff,vcCompanyDiff,numCurrencyID,numFollowUpStatus1,numFollowUpStatus,numGrpID,bintCreatedDate,vcComFax,bitPublicFlag,tintCRMType,bitNoTax,numTerID,numStatusID,vcComPhone)
		SELECT
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.DivisionMaster'), 'bitActiveInActive', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS bitActiveInActive ,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.DivisionMaster'), 'numAssignedBy', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numAssignedBy, 
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.DivisionMaster'), 'numAssignedTo', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numAssignedTo, 
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.DivisionMaster'), 'numCampaignID', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numCampaignID, 
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.DivisionMaster'), 'numCompanyDiff', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numCompanyDiff	,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.DivisionMaster'), 'vcCompanyDiff', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS  vcCompanyDiff ,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.DivisionMaster'), 'numCurrencyID', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numCurrencyID, 
        CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.DivisionMaster'), 'numFollowUpStatus1', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numFollowUpStatus1,
        CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.DivisionMaster'), 'numFollowUpStatus', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numFollowUpStatus,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.DivisionMaster'), 'numGrpID', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numGrpID,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.DivisionMaster'), 'bintCreatedDate', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS bintCreatedDate,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.DivisionMaster'), 'vcComFax', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS vcComFax,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.DivisionMaster'), 'bitPublicFlag', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS bitPublicFlag,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.DivisionMaster'), 'tintCRMType', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS tintCRMType,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.DivisionMaster'), 'bitNoTax', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS bitNoTax,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.DivisionMaster'), 'numTerID', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numTerID,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.DivisionMaster'), 'numStatusID', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numStatusID,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.DivisionMaster'), 'vcComPhone', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS vcComPhone
		FROM 
		CHANGETABLE(CHANGES dbo.DivisionMaster, @last_synchronization_version) AS CT
		WHERE 
		ct.numDivisionID=@numRecordID

		--sp_helptext USP_GetWorkFlowFormFieldMaster
	--exec USP_GetWorkFlowFormFieldMaster 1,68
INSERT INTO @tblFields(FieldName,FieldValue)
		SELECT
		FieldName,
		FieldValue
		FROM
			(
				SELECT  bitActiveInActive,numAssignedBy,numAssignedTo,numCampaignID,numCompanyDiff,vcCompanyDiff,numCurrencyID,numFollowUpStatus1,numFollowUpStatus,numGrpID,bintCreatedDate,vcComFax,bitPublicFlag,tintCRMType,bitNoTax,numTerID,numStatusID,vcComPhone
				FROM @UFFields	
			) AS UP
		UNPIVOT
		(
		FieldValue FOR FieldName IN (bitActiveInActive,numAssignedBy,numAssignedTo,numCampaignID,numCompanyDiff,vcCompanyDiff,numCurrencyID,numFollowUpStatus1,numFollowUpStatus,numGrpID,bintCreatedDate,vcComFax,bitPublicFlag,tintCRMType,bitNoTax,numTerID,numStatusID,vcComPhone)
		) AS upv
		where FieldValue<>0
 

End

--select 'col1' from @UFFields where numCreatedBy='0' and numModifiedBy='0'
-- If Exists (select 'col1' from @UFFields where numCreatedBy='0' and numModifiedBy='0')
-- Begin
-- set @Type='I'
-- End
-- Else
-- Begin
-- set @Type='U'
-- End
-- select @Type

DECLARE @tintWFTriggerOn AS TINYINT
SET @tintWFTriggerOn=CASE @Type WHEN 'I' THEN 1 WHEN 'U' THEN 2 WHEN 'D' THEN 5 END

Declare @numAssignedTo numeric(18,0)
Declare @numAssignedBy numeric(18,0)
Declare @numWebLead numeric(18,0)

Select @numAssignedTo=numAssignedTo,@numAssignedBy=numAssignedBy,@numWebLead=numGrpId from DivisionMaster where numDivisionID=@numRecordID

--For Fields Update Exection Point
DECLARE @Columns_Updated VARCHAR(1000)
SELECT @Columns_Updated = COALESCE(@Columns_Updated + ',', '') + FieldName FROM @tblFields 
SET @Columns_Updated=ISNULL(@Columns_Updated,'')

select @Columns_Updated,@numAssignedTo,@numAssignedBy,@numWebLead
IF (@Columns_Updated='numAssignedBy,numAssignedTo')
		BEGIN	
			IF (@numWebLead=1) --web Leads

			BEGIN
					IF	(@numAssignedTo=1 AND @numAssignedBy=1)
						 BEGIN
							SET @tintWFTriggerOn=1
						 END
					 ELSE
						 BEGIN
							 SET @tintWFTriggerOn=2
						 END
			END

			ELSE    --Normal Leads
			BEGIN
						IF	(@numAssignedTo=0 AND @numAssignedBy=0)
							 BEGIN
								SET @tintWFTriggerOn=1
							 END
						ELSE
							 BEGIN
								 SET @tintWFTriggerOn=2
							 END
			END
		
	END
ELSE 
		BEGIN
			SET @tintWFTriggerOn=2
		END

--Adding data As per Opertaion
	EXEC dbo.USP_ManageWorkFlowQueue
	@numWFQueueID = 0, --  numeric(18, 0)
	@numDomainID = @numDomainID, --  numeric(18, 0)
	@numUserCntID = @numUserCntID, --  numeric(18, 0)
	@numRecordID = @numRecordID, --  numeric(18, 0)
	@numFormID = 68, --  numeric(18, 0)
	@tintProcessStatus = 1, --  tinyint
	@tintWFTriggerOn = @tintWFTriggerOn, --  tinyint
	@tintMode = 1, --  tinyint
	@vcColumnsUpdated = @Columns_Updated

	
/***************************************************Commented SP for Checking RowVersion with Change Tracking:Date:23Apr||Sachin********************************************************/



--DECLARE @synchronization_version BIGINT 



--SET @synchronization_version = CHANGE_TRACKING_CURRENT_VERSION();











--SELECT 



--    CT.numOppID, CT.SYS_CHANGE_OPERATION, 



--    CT.SYS_CHANGE_COLUMNS, CT.SYS_CHANGE_CONTEXT, 



--    CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'numOppID', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numOppID, 



--    CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'vcPOppName', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS vcPOppName 



--FROM 



--    CHANGETABLE(CHANGES dbo.OpportunityMaster, @synchronization_version) AS CT











--SET @synchronization_version = CHANGE_TRACKING_CURRENT_VERSION();



/***************************************************End of Comment||Sachin********************************************************/



END


/****** Object:  StoredProcedure [dbo].[USP_ReOpenOppertunity]    Script Date: 07/26/2008 16:20:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReOpenOppertunity')
DROP PROCEDURE USP_ReOpenOppertunity
GO
CREATE PROCEDURE [dbo].[USP_ReOpenOppertunity]            
@OppID as numeric(9),
@numUserCntID AS NUMERIC(9),
@numDomainID AS NUMERIC(9)
          
as          
BEGIN TRY
		-- Added by Manish Anjara : 17th Sep,2014
	-- Check whether order is already opened or not. If already opened then show a warning message
	IF (SELECT ISNULL(tintshipped,0) FROM [dbo].[OpportunityMaster] WHERE [OpportunityMaster].[numOppId] = @OppID AND [OpportunityMaster].[numDomainId] = @numDomainID) = 0
	BEGIN
		RAISERROR('ORDER_OPENED', 16, 1)
	END

   BEGIN TRANSACTION       
			update OpportunityMaster set tintshipped=0,bintAccountClosingDate=NULL,bintClosedDate=NULL where  numOppId=@OppID  AND [numDomainId] = @numDomainID
			UPDATE [OpportunityBizDocs] SET [dtShippedDate] = NULL WHERE [numOppId]=@OppID
        			
			--SET Received/Shipped qty to 0	
			update OpportunityItems SET numUnitHourReceived=0,numQtyShipped=0 WHERE [numOppId]=@OppID
			
			
			update OpportunityKitItems SET numQtyShipped=0 WHERE [numOppId]=@OppID
			
			declare @status as varchar(2)            
			declare @OppType as varchar(2)  
			DECLARE @fltExchangeRate AS FLOAT 
			          
			select @status=tintOppStatus,@OppType=tintOppType,@fltExchangeRate=(CASE WHEN ISNULL(fltExchangeRate,0)=0 THEN 1 ELSE fltExchangeRate END) from OpportunityMaster            
			where numOppId=@OppID AND [numDomainId] = @numDomainID
			
			-- UnArchive item based on Item's individual setting
			IF @OppType = 1
			BEGIN
				UPDATE Item SET IsArchieve = CASE WHEN EXISTS(SELECT OpportunityItems.numItemCode FROM dbo.OpportunityItems 
															  WHERE OpportunityItems.numItemCode = I.numItemCode
															  AND OpportunityItems.numoppitemtCode = OI.numoppitemtCode) 
												  THEN 0
												  ELSE 1 
											 END 
				FROM Item I JOIN dbo.OpportunityItems OI ON I.numItemCode = OI.numItemCode  
				WHERE numOppId = @OppID 
				AND I.[numDomainId] = @numDomainID
				AND ISNULL(I.bitArchiveItem,0) = 1 
			END     
			----------------------------------------------------------------------------            
			--Updating Item Details            
			-----------------------------------------------------------------------            
            
			---Updating the inventory Items once the deal is won              
			declare @numoppitemtCode as numeric            
			declare @numUnits as numeric              
			declare @numWarehouseItemID as numeric       
			declare @itemcode as numeric        
			declare @QtyShipped as NUMERIC
			declare @QtyReceived as numeric
			Declare @monPrice as money  
			declare @Kit as bit  
    
			 select top 1 @numoppitemtCode=numoppitemtCode,@itemcode=OI.numItemCode,@numUnits=ISNULL(numUnitHour,0),@qtyShipped=ISNULL([numQtyShipped],0),@qtyReceived=ISNULL([numUnitHourReceived],0),
			 @numWareHouseItemID=ISNULL(numWarehouseItmsID,0),@monPrice=isnull(monPrice,0) * @fltExchangeRate,@Kit= (case when bitKitParent=1 and bitAssembly=1 then 0 when bitKitParent=1 then 1 else 0 end) 
			 from OpportunityItems OI join Item I                                                
			 on OI.numItemCode=I.numItemCode and numOppId=@OppID and (bitDropShip=0 or bitDropShip is null)                                            
			 where (charitemtype='P' OR 1=(CASE WHEN @OppType=1 THEN 
							CASE WHEN ISNULL(I.bitKitParent,0) = 1 AND ISNULL(I.bitAssembly,0) = 1 THEN 0
								 WHEN ISNULL(I.bitKitParent,0) = 1 THEN 1
								 ELSE 0 END 
							ELSE 0 END))  
			 AND I.[numDomainId] = @numDomainID
			order by OI.numoppitemtCode      
            
			 while @numoppitemtCode>0                
			 begin   
 
				EXEC usp_ManageInventory @itemcode,@numWareHouseItemID,0,@OppType,@numUnits,@qtyShipped,@qtyReceived,@monPrice,3,@OppID,@numoppitemtCode,@numUserCntID
        
			  select top 1 @numoppitemtCode=numoppitemtCode,@itemcode=OI.numItemCode,@numUnits=ISNULL(numUnitHour,0),@qtyShipped=ISNULL([numQtyShipped],0),@qtyReceived=ISNULL([numUnitHourReceived],0),
			  @numWareHouseItemID=ISNULL(numWarehouseItmsID,0),@monPrice=isnull(monPrice,0) * @fltExchangeRate,@Kit= (case when bitKitParent=1 and bitAssembly=1 then 0 when bitKitParent=1 then 1 else 0 end) 
			  from OpportunityItems OI                                                
			  join Item I                                                
			  on OI.numItemCode=I.numItemCode and numOppId=@OppID                                              
			  where (charitemtype='P' OR 1=(CASE WHEN @OppType=1 THEN 
							CASE WHEN ISNULL(I.bitKitParent,0) = 1 AND ISNULL(I.bitAssembly,0) = 1 THEN 0
								 WHEN ISNULL(I.bitKitParent,0) = 1 THEN 1
								 ELSE 0 END 
							ELSE 0 END))
					and OI.numoppitemtCode>@numoppitemtCode and (bitDropShip=0 or bitDropShip is null) 
					AND I.[numDomainId] = @numDomainID
					order by OI.numoppitemtCode                                                
			  if @@rowcount=0 set @numoppitemtCode=0     
  
			 END
				--Update WareHouseItmsDTL : from tintStatus 0 to 2 (PO Serial/Lot No)

			IF @OppType=2
			BEGIn
			  --   update WareHouseItmsDTL SET tintStatus=2 FROM WareHouseItmsDTL owsi JOIN OppWarehouseSerializedItem opp 
			  --  ON owsi.numWareHouseItmsDTLID=opp.numWareHouseItmsDTLID where opp.numOppID=@OppID AND owsi.tintStatus=0
				DELETE FROM WareHouseItmsDTL WHERE numWareHouseItmsDTLID in(SELECT numWareHouseItmsDTLID FROM OppWarehouseSerializedItem WHERE numOppID=@OppID)
				DELETE FROM OppWarehouseSerializedItem WHERE numOppID=@OppID
			END
			IF @OppType=1
			BEGIN
			 UPDATE  WareHouseItmsDTL SET WareHouseItmsDTL.numQty=WareHouseItmsDTL.numQty + isnull(OppWarehouseSerializedItem.numQty,0)
			 from   OppWarehouseSerializedItem join WareHouseItmsDTL
			 on WareHouseItmsDTL.numWareHouseItmsDTLID= OppWarehouseSerializedItem.numWarehouseItmsDTLID                           
			 where numOppID=@OppID      
			END
			
			
			IF @OppType=2
			BEGIn
			DELETE FROM dbo.General_Journal_Details WHERE numJournalId IN (SELECT numJournal_Id FROM dbo.General_Journal_Header WHERE numDomainID=@numDomainID AND numOppId=@OppID AND ISNULL(numOppBizDocsId,0)=0 
				AND isnull(numBillID,0)=0 AND isnull(numBillPaymentID,0)=0 AND isnull(numPayrollDetailID,0)=0
				AND isnull(numCheckHeaderID,0)=0 AND isnull(numReturnID,0)=0 
				AND isnull(numDepositId,0)=0)
			
			DELETE FROM General_Journal_Header WHERE numDomainID=@numDomainID AND numOppId=@OppID AND ISNULL(numOppBizDocsId,0)=0 
				AND isnull(numBillID,0)=0 AND isnull(numBillPaymentID,0)=0 AND isnull(numPayrollDetailID,0)=0
				AND isnull(numCheckHeaderID,0)=0 AND isnull(numReturnID,0)=0 
				AND isnull(numDepositId,0)=0
			END
COMMIT
END TRY
BEGIN CATCH
  -- Whoops, there was an error
  IF @@TRANCOUNT > 0
     ROLLBACK

  -- Raise an error with the details of the exception
  DECLARE @ErrMsg nvarchar(4000), @ErrSeverity int
  SELECT @ErrMsg = ERROR_MESSAGE(),
         @ErrSeverity = ERROR_SEVERITY()

  RAISERROR(@ErrMsg, @ErrSeverity, 1)
END CATCH


GO
/****** Object:  StoredProcedure [dbo].[USP_RevertDetailsOpp]    Script Date: 07/26/2008 16:20:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj      
/* deducts Qty from allocation and Adds back to onhand */
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_RevertDetailsOpp')
DROP PROCEDURE USP_RevertDetailsOpp
GO
CREATE PROCEDURE [dbo].[USP_RevertDetailsOpp] 
@numOppId NUMERIC,
@tintMode AS TINYINT=0, -- 0:Add/Edit 1:Delete
@numUserCntID AS NUMERIC(9)
AS ---reverting back to previous state if deal is being edited 
	DECLARE @numDomain AS NUMERIC(18,0)
	SELECT @numDomain = numDomainID FROM [dbo].[OpportunityMaster] AS OM WHERE [OM].[numOppId] = @numOppID
    
	DECLARE @OppType AS VARCHAR(2)              
    DECLARE @itemcode AS NUMERIC       
    DECLARE @numWareHouseItemID AS NUMERIC                                   
    DECLARE @numToWarehouseItemID AS NUMERIC 
    DECLARE @numUnits AS NUMERIC                                              
    DECLARE @onHand AS NUMERIC                                            
    DECLARE @onOrder AS NUMERIC                                            
    DECLARE @onBackOrder AS NUMERIC                                              
    DECLARE @onAllocation AS NUMERIC
    DECLARE @numQtyShipped AS NUMERIC
    DECLARE @numUnitHourReceived AS NUMERIC
    DECLARE @numoppitemtCode AS NUMERIC(9) 
    DECLARE @monAmount AS MONEY 
    DECLARE @monAvgCost AS MONEY   
    DECLARE @Kit AS BIT                                        
    DECLARE @bitKitParent BIT
    DECLARE @bitStockTransfer BIT 
    DECLARE @numOrigUnits AS NUMERIC			
    DECLARE @description AS VARCHAR(100)
	--Added by :Sachin Sadhu||Date:18thSept2014
	--For Rental/Asset Project
	Declare @numRentalIN as Numeric
	Declare @numRentalOut as Numeric
	Declare @numRentalLost as Numeric
	DECLARE @bitAsset as BIT
	--end sachin

    						
    SELECT TOP 1
            @numoppitemtCode = numoppitemtCode,
            @itemcode = OI.numItemCode,
            @numUnits = ISNULL(numUnitHour,0),
            @numWareHouseItemID = ISNULL(numWarehouseItmsID,0),
            @Kit = ( CASE WHEN bitKitParent = 1
                               AND bitAssembly = 1 THEN 0
                          WHEN bitKitParent = 1 THEN 1
                          ELSE 0
                     END ),
            @monAmount = ISNULL(monTotAmount,0) * (CASE WHEN ISNULL(fltExchangeRate,0)=0 THEN 1 ELSE fltExchangeRate END),
            @monAvgCost = ISNULL(monAverageCost,0),
            @numQtyShipped = ISNULL(numQtyShipped,0),
            @numUnitHourReceived = ISNULL(numUnitHourReceived,0),
            @bitKitParent=ISNULL(bitKitParent,0),
            @numToWarehouseItemID =OI.numToWarehouseItemID,
            @bitStockTransfer = ISNULL(OM.bitStockTransfer,0),
            @OppType = tintOppType,
		    @numRentalIN=ISNULL(oi.numRentalIN,0),
			@numRentalOut=Isnull(oi.numRentalOut,0),
			@numRentalLost=Isnull(oi.numRentalLost,0),
			@bitAsset =ISNULL(I.bitAsset,0)
			
    FROM    OpportunityItems OI
			JOIN dbo.OpportunityMaster OM ON OI.numOppId = OM.numOppId
            JOIN Item I ON OI.numItemCode = I.numItemCode
    WHERE   (charitemtype='P' OR 1=(CASE WHEN tintOppType=1 THEN 
							CASE WHEN ISNULL(I.bitKitParent,0) = 1 AND ISNULL(I.bitAssembly,0) = 1 THEN 0
								 WHEN ISNULL(I.bitKitParent,0) = 1 THEN 1
								 ELSE 0 END 
							ELSE 0 END)) AND OI.numOppId = @numOppId
                           AND ( bitDropShip = 0
                                 OR bitDropShip IS NULL
                               )  AND ISNULL(OI.bitWorkOrder,0)=0 
    ORDER BY OI.numoppitemtCode

	PRINT '@numoppitemtCode:' + CAST(@numoppitemtCode AS VARCHAR(10))
	PRINT '@numUnits:' + CAST(@numUnits AS VARCHAR(10))
	PRINT '@numWareHouseItemID:' + CAST(@numWareHouseItemID AS VARCHAR(10))
	PRINT '@numQtyShipped:' + CAST(@numQtyShipped AS VARCHAR(10))
	PRINT '@numUnitHourReceived:' + CAST(@numUnitHourReceived AS VARCHAR(10))
	PRINT '@numToWarehouseItemID:' + CAST(@numToWarehouseItemID AS VARCHAR(10))
	PRINT '@bitStockTransfer:' + CAST(@bitStockTransfer AS VARCHAR(10))
	
    WHILE @numoppitemtCode > 0                                  
        BEGIN    
        --Kamal : Item Group take as inline Item
--            IF @Kit = 1 
--                BEGIN  
--                    EXEC USP_UpdateKitItems @numoppitemtCode, @numUnits,
--                        @OppType, 1,@numOppID  
--                END  
            
            
            SET @numOrigUnits=@numUnits
            
            IF @bitStockTransfer=1
            BEGIN
				SET @OppType = 1
			END
            
            PRINT '@OppType:' + CAST(@OppType AS VARCHAR(10))      
            IF @numWareHouseItemID>0
            BEGIN     
				PRINT 'FROM REVERT : CONDITION : @numWareHouseItemID>0'                             
				SELECT  @onHand = ISNULL(numOnHand, 0),
						@onAllocation = ISNULL(numAllocation, 0),
						@onOrder = ISNULL(numOnOrder, 0),
						@onBackOrder = ISNULL(numBackOrder, 0)
				FROM    WareHouseItems
				WHERE   numWareHouseItemID = @numWareHouseItemID       
				
				PRINT '@onHand:' + CAST(@onHand AS VARCHAR(10))
				PRINT '@onAllocation:' + CAST(@onAllocation AS VARCHAR(10))
				PRINT '@onOrder:' + CAST(@onOrder AS VARCHAR(10))
				PRINT '@onBackOrder:' + CAST(@onBackOrder AS VARCHAR(10))                                         
            END
            
            IF @OppType = 1 
                BEGIN
					SET @description='SO Deleted (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@numQtyShipped AS VARCHAR(10)) + ')'
                
                IF @Kit = 1
					BEGIN
						exec USP_UpdatingInventoryForEmbeddedKits @numoppitemtCode,@numOrigUnits,1,@numOppID,@tintMode,@numUserCntID
					END  
				ELSE
					BEGIN	
                          IF @numQtyShipped>0
								 BEGIN
									IF @tintMode=0
											SET @numUnits = @numUnits - @numQtyShipped
									ELSE IF @tintmode=1
											SET @onAllocation = @onAllocation + @numQtyShipped 
								  END 
								                    
                    IF @numUnits >= @onBackOrder 
                        BEGIN
                            SET @numUnits = @numUnits - @onBackOrder
                            SET @onBackOrder = 0
                            
                            IF (@onAllocation - @numUnits >= 0)
								SET @onAllocation = @onAllocation - @numUnits
								IF @bitAsset=1--Not Asset
										BEGIN
											  SET @onHand = @onHand +@numRentalIN+@numRentalLost+@numRentalOut     
										END
								ELSE
										BEGIN
											 SET @onHand = @onHand + @numUnits     
										END
                                                                 
                        END                                            
                    ELSE 
                        IF @numUnits < @onBackOrder 
                            BEGIN                  
								IF (@onBackOrder - @numUnits >0)
									SET @onBackOrder = @onBackOrder - @numUnits
									
--								IF @tintmode=1
--									SET @onAllocation = @onAllocation + (@numUnits - @numQtyShipped)
                            END 
                 	
						UPDATE  WareHouseItems
						SET     numOnHand = @onHand ,
								numAllocation = @onAllocation,
								numBackOrder = @onBackOrder,
								dtModified = GETDATE()
						WHERE   numWareHouseItemID = @numWareHouseItemID       

					
					                                               
                                  
				END
				
				IF @numWareHouseItemID>0
				BEGIN 
						EXEC dbo.USP_ManageWareHouseItems_Tracking
						@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
						@numReferenceID = @numOppId, --  numeric(9, 0)
						@tintRefType = 3, --  tinyint
						@vcDescription = @description, --  varchar(100)
						@numModifiedBy = @numUserCntID,
						@numDomainID = @numDomain 
				END		
          END      
          
          IF @bitStockTransfer=1
            BEGIN
				SET @numWareHouseItemID = @numToWarehouseItemID;
				SET @OppType = 2
				SELECT  @onHand = ISNULL(numOnHand, 0),
                    @onAllocation = ISNULL(numAllocation, 0),
                    @onOrder = ISNULL(numOnOrder, 0),
                    @onBackOrder = ISNULL(numBackOrder, 0)
				FROM    WareHouseItems
				WHERE   numWareHouseItemID = @numWareHouseItemID   
			END
          
          IF @OppType = 2 
                    BEGIN 
					SET @description='PO Deleted (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Received:' +  CAST(@numUnitHourReceived AS VARCHAR(10)) + ')'

						--Updating the Average Cost
--                        IF @onHand + @onOrder - @numUnits <> 0 
--                            BEGIN
--                                SET @monAvgCost = ( ( @onHand + @onOrder )
--                                                    * @monAvgCost - @monAmount )
--                                    / ( @onHand + @onOrder - @numUnits )
--                            END
--                        ELSE 
--                            SET @monAvgCost = 0
--                        UPDATE  item
--                        SET     monAverageCost = @monAvgCost
--                        WHERE   numItemCode = @itemcode
					
					    --Partial Fulfillment
						IF @tintmode=1 and  @onHand >= @numUnitHourReceived
						BEGIN
							SET @onHand= @onHand - @numUnitHourReceived
							--SET @onOrder= @onOrder + @numUnitHourReceived
						END
						
					    SET @numUnits = @numUnits - @numUnitHourReceived 

						IF (@onOrder - @numUnits)>=0
						BEGIN
							--Causing Negative Inventory Bug ID:494
							SET @onOrder = @onOrder - @numUnits	
						END
						ELSE IF (@onHand + @onOrder) - @numUnits >= 0
						BEGIN						
							SET @onHand = @onHand - (@numUnits-@onOrder)
							SET @onOrder = 0
						END
						ELSE IF  (@onHand + @onOrder + @onAllocation) - @numUnits >= 0
						BEGIN
							Declare @numDiff numeric
	
							SET @numDiff = @numUnits - @onOrder
							SET @onOrder = 0

							SET @numDiff = @numDiff - @onHand
							SET @onHand = 0

							SET @onAllocation = @onAllocation - @numDiff
							SET @onBackOrder = @onBackOrder + @numDiff
						END
					    
                        UPDATE  WareHouseItems
                        SET     numOnHand = @onHand,
                                numAllocation = @onAllocation,
                                numBackOrder = @onBackOrder,
                                numOnOrder = @onOrder,
							dtModified = GETDATE()
                        WHERE   numWareHouseItemID = @numWareHouseItemID   
                        
                        EXEC dbo.USP_ManageWareHouseItems_Tracking
						@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
						@numReferenceID = @numOppId, --  numeric(9, 0)
						@tintRefType = 4, --  tinyint
						@vcDescription = @description, --  varchar(100)
						@numModifiedBy = @numUserCntID,
						@numDomainID = @numDomain                                          
                    END   
                                                                
            SELECT TOP 1
                    @numoppitemtCode = numoppitemtCode,
                    @itemcode = OI.numItemCode,
                    @numUnits = numUnitHour,
                    @numWareHouseItemID = ISNULL(numWarehouseItmsID,0),
                    @Kit = ( CASE WHEN bitKitParent = 1
                                       AND bitAssembly = 1 THEN 0
                                  WHEN bitKitParent = 1 THEN 1
                                  ELSE 0
                             END ),
                    @monAmount = ISNULL(monTotAmount,0)  * (CASE WHEN ISNULL(fltExchangeRate,0)=0 THEN 1 ELSE fltExchangeRate END),
                    @monAvgCost = monAverageCost,
                    @numQtyShipped = ISNULL(numQtyShipped,0),
					@numUnitHourReceived = ISNULL(numUnitHourReceived,0),
					@bitKitParent=ISNULL(bitKitParent,0),
					@numToWarehouseItemID =OI.numToWarehouseItemID,
					@bitStockTransfer = ISNULL(OM.bitStockTransfer,0),
					@OppType = tintOppType
            FROM    OpportunityItems OI
					JOIN dbo.OpportunityMaster OM ON OI.numOppId = OM.numOppId
                    JOIN Item I ON OI.numItemCode = I.numItemCode
                                   
            WHERE   (charitemtype='P' OR 1=(CASE WHEN tintOppType=1 THEN 
							CASE WHEN ISNULL(I.bitKitParent,0) = 1 AND ISNULL(I.bitAssembly,0) = 1 THEN 0
								 WHEN ISNULL(I.bitKitParent,0) = 1 THEN 1
								 ELSE 0 END 
							ELSE 0 END)) AND ISNULL(OI.bitWorkOrder,0)=0 
							AND OI.numOppId = @numOppId 
                    AND OI.numoppitemtCode > @numoppitemtCode
                    AND ( bitDropShip = 0
                          OR bitDropShip IS NULL
                        )
            ORDER BY OI.numoppitemtCode                                              
            IF @@rowcount = 0 
                SET @numoppitemtCode = 0      
        END
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_SalesOrderConfiguration_Save' ) 
    DROP PROCEDURE USP_SalesOrderConfiguration_Save
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Sandeep Patel
-- Create date: 18 Feb 2014
-- Description:	Saves new sales order form configuration
-- =============================================
CREATE PROCEDURE USP_SalesOrderConfiguration_Save
	@numSOCID NUMERIC(18,0) = NULL,
	@numDomainID NUMERIC(18,0) = NULL,
	@bitAutoFocusCustomer BIT,
	@bitAutoFocusItem BIT,
	@bitDisplayRootLocation BIT,
	@bitAutoAssignOrder BIT,
	@bitDisplayLocation BIT,
	@bitDisplayFinancialStamp BIT,
	@bitDisplayItemDetails BIT,
	@bitDisplayUnitCost BIT,
	@bitDisplayProfitTotal BIT,
	@bitDisplayShippingRates BIT,
	@bitDisplayPaymentMethods BIT,
	@bitCreateOpenBizDoc BIT,
	@numListItemID NUMERIC(18,0),
	@vcPaymentMethodIDs VARCHAR(100),
	@bitDisplayCurrency BIT,
	@bitDisplayAssignTo BIT,
	@bitDisplayTemplate BIT,
	@bitDisplayCouponDiscount BIT,
	@bitDisplayShippingCharges BIT,
	@bitDisplayDiscount BIT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF (SELECT COUNT(*) FROM SalesOrderConfiguration WHERE numDomainID = @numDomainID) = 0
	BEGIN
		INSERT INTO dbo.SalesOrderConfiguration
		(
			numDomainID,
			bitAutoFocusCustomer,
			bitAutoFocusItem,
			bitDisplayRootLocation,
			bitAutoAssignOrder,
			bitDisplayLocation,
			bitDisplayFinancialStamp,
			bitDisplayItemDetails,
			bitDisplayUnitCost,
			bitDisplayProfitTotal,
			bitDisplayShippingRates,
			bitDisplayPaymentMethods,
			bitCreateOpenBizDoc,
			numListItemID,
			vcPaymentMethodIDs,
			bitDisplayCurrency,
			bitDisplayAssignTo,
			bitDisplayTemplate,
			bitDisplayCouponDiscount,
			bitDisplayShippingCharges,
			bitDisplayDiscount
		)
		VALUES
		(
			@numDomainID,
			@bitAutoFocusCustomer,
			@bitAutoFocusItem,
			@bitDisplayRootLocation,
			@bitAutoAssignOrder,
			@bitDisplayLocation,
			@bitDisplayFinancialStamp,
			@bitDisplayItemDetails,
			@bitDisplayUnitCost,
			@bitDisplayProfitTotal,
			@bitDisplayShippingRates,
			@bitDisplayPaymentMethods,
			@bitCreateOpenBizDoc,
			@numListItemID,
			@vcPaymentMethodIDs,
			@bitDisplayCurrency,
			@bitDisplayAssignTo,
			@bitDisplayTemplate,
			@bitDisplayCouponDiscount,
			@bitDisplayShippingCharges,
			@bitDisplayDiscount
		)
	END
	ELSE
	BEGIN
		UPDATE
			dbo.SalesOrderConfiguration
		SET
			bitAutoFocusCustomer = @bitAutoFocusCustomer,
			bitAutoFocusItem = @bitAutoFocusItem,
			bitDisplayRootLocation = @bitDisplayRootLocation,
			bitAutoAssignOrder = @bitAutoAssignOrder,
			bitDisplayLocation = @bitDisplayLocation,
			bitDisplayFinancialStamp = @bitDisplayFinancialStamp,
			bitDisplayItemDetails = @bitDisplayItemDetails,
			bitDisplayUnitCost = @bitDisplayUnitCost,
			bitDisplayProfitTotal = @bitDisplayProfitTotal,
			bitDisplayShippingRates = @bitDisplayShippingRates,
			bitDisplayPaymentMethods = @bitDisplayPaymentMethods,
			bitCreateOpenBizDoc = @bitCreateOpenBizDoc,
			numListItemID = @numListItemID,
			vcPaymentMethodIDs  = @vcPaymentMethodIDs,
			bitDisplayCurrency = @bitDisplayCurrency,
			bitDisplayAssignTo = @bitDisplayAssignTo,
			bitDisplayTemplate = @bitDisplayTemplate,
			bitDisplayCouponDiscount = @bitDisplayCouponDiscount,
			bitDisplayShippingCharges = @bitDisplayShippingCharges,
			bitDisplayDiscount = @bitDisplayDiscount
		WHERE
			numDomainID = @numDomainID
	END
    
END
GO


/****** Object:  StoredProcedure [dbo].[usp_SetAllSelectedSystemModulesPagesAndAccesses]    Script Date: 07/26/2008 16:21:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_setallselectedsystemmodulespagesandaccesses')
DROP PROCEDURE usp_setallselectedsystemmodulespagesandaccesses
GO
CREATE PROCEDURE [dbo].[usp_SetAllSelectedSystemModulesPagesAndAccesses]  
 @numModuleID NUMERIC(9) = 0,  
 @numGroupID NUMERIC(9) = 0,  
 @numPageID NUMERIC(9) = 0,  
 @vcColumnName VARCHAR(30) = '',  
 @vcColumnValue SMALLINT,
 @numDomainID NUMERIC(9)
--  
AS  
  BEGIN  
   DECLARE @bitCustomRelationship BIT
   DECLARE @vcQuerString VARCHAR(200)  
   IF (SELECT COUNT(*) FROM GroupAuthorization WHERE (numGroupID = @numGroupID) AND (numModuleID = @numModuleID) AND (numPageID = @numPageID) AND numDomainID=@numDomainID) > 0  
    BEGIN  
		IF (SELECT COUNT(*) FROM PageMaster WHERE numModuleID = @numModuleID AND numPageID = @numPageID) > 0
		BEGIN
			SET @bitCustomRelationship = 0
		END
		ELSE
		BEGIN
			SET @bitCustomRelationship = 1
		END

     --PRINT 'UPDATE'  
     SELECT @vcQuerString = 'UPDATE GroupAuthorization SET '  + @vcColumnName + ' = ' + CONVERT(CHAR, @vcColumnValue) + ',bitCustomRelationship =' + CONVERT(VARCHAR, @bitCustomRelationship) + '  WHERE (numGroupID = ' + CONVERT(VARCHAR, @numGroupID) + ') AND (numModuleID =' + CONVERT(VARCHAR, @numModuleID) + ') AND (numPageID =' + CONVERT(VARCHAR, @numPageID) + ') AND numDomainID=' + CONVERT(VARCHAR,@numDomainID)
    END  
   ELSE  
    BEGIN  

		IF (SELECT COUNT(*) FROM PageMaster WHERE numModuleID = @numModuleID AND numPageID = @numPageID) > 0
		BEGIN
			SET @bitCustomRelationship = 0
		END
		ELSE
		BEGIN
			SET @bitCustomRelationship = 1
		END

     --PRINT 'INSERT'  
     SELECT @vcQuerString = 'INSERT INTO GroupAuthorization (numGroupID, numModuleID, numPageID,numDomainID,bitCustomRelationship, ' + @vcColumnName +') VALUES(' + CONVERT(VARCHAR, @numGroupID) + ', ' + CONVERT(VARCHAR, @numModuleID) + ', ' + CONVERT(VARCHAR, @numPageID) + ', ' + CONVERT(VARCHAR,@numDomainID) + ', ' + CONVERT(VARCHAR, @bitCustomRelationship) + ', ' + CONVERT(
VARCHAR, @vcColumnValue) + ')'  
    END  
   PRINT @vcQuerString  
   EXEC (@vcQuerString)  
  END
GO
/****** Object:  StoredProcedure [dbo].[USP_SubScriberList]    Script Date: 07/26/2008 16:21:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
declare @p8 int
set @p8=0
exec USP_SubScriberList @numUserCntID=1,@tintSortOrder=1,@SortChar='0',@numDomainID=1,@SearchWord=NULL,@CurrentPage=1,@PageSize=20,@TotRecs=@p8 
output,@columnName='numSubscriberID',@columnSortOrder='Desc',@ClientTimeZoneOffset=-330
select @p8
*/
--- Created By Anoop Jayaraj                                                      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_subscriberlist')
DROP PROCEDURE usp_subscriberlist
GO
CREATE PROCEDURE [dbo].[USP_SubScriberList]                                                                                             
@numUserCntID numeric,                                                                                           
@tintSortOrder tinyint,                                                                            
@SortChar char(1)='0',                                  
@numDomainID as numeric(9)=0,                                                        
@SearchWord varChar(100)= '',                                                                                            
@CurrentPage int,                                                      
@PageSize int,                                                      
@TotRecs int output,                                                      
@columnName as Varchar(50),                                                      
@columnSortOrder as Varchar(10),        
@ClientTimeZoneOffset Int ,
@ActiveUsers float output
as                                                      


 declare @firstRec as integer                                                      
 declare @lastRec as integer                                                      
 set @firstRec= (@CurrentPage-1) * @PageSize                                                      
 set @lastRec= (@CurrentPage*@PageSize+1)                                                      
 
 
 
                                                     
                     
declare @strSql as varchar(8000)                    
                  
                  
set @strSql='With tblSubscriber AS (SELECT ROW_NUMBER() OVER (ORDER BY '+@columnName+' '+ @columnSortOrder+') AS RowNumber,numSubscriberID,  CASE WHEN Subscribers.numTargetDomainID IN (1,72) THEN 0 ELSE Subscribers.numTargetDomainID END AS [numDomainID]                 
 FROM Subscribers '                  
 set @strSql=@strSql +' Join Divisionmaster D on D.numDivisionID= Subscribers.numDivisionID Join Companyinfo C                  
on C.numCompanyID=D.numCompanyID '                  
                  
  set @strSql=@strSql +' Left Join AdditionalContactsInformation A on A.numContactID= Subscribers.numAdminContactID '                  
                  
                  
    set @strSql=@strSql +' where bitDeleted=0 '                  
                  
if @SortChar<>'0' set @strSql=@strSql +' and Subscribers.numDivisionID in (select D.numDivisionID from DivisionMaster D                  
 Join CompanyInfo C on C.numCompanyID=D.numCompanyID where vcCompanyName like '''+ @SortChar+'%'')'                   
                  
if @SearchWord<>''                  
begin                  
 if @tintSortOrder=1 set @strSql=@strSql +' and D.numDivisionID in (select numDivisionID from DivisionMaster D                  
 Join CompanyInfo C on C.numCompanyID=D.numCompanyID where vcCompanyName like ''%'+ @SearchWord+'%'')'                  
 else if @tintSortOrder=2 set @strSql=@strSql +' and D.numDivisionID in (select numDivisionID from AdditionalContactsInformation A                  
 where vcEmail like ''%'+ @SearchWord+'%'')'                  
end
 if @tintSortOrder=3 set @strSql=@strSql +' and bitTrial =1 '                  
 else if @tintSortOrder=4 set @strSql=@strSql +' and bitActive =1 '                  
 else if @tintSortOrder=5 set @strSql=@strSql +' and bitActive =0 '                  
 else if @tintSortOrder=6 set @strSql=@strSql +' and bitActive =0 and dtSuspendedDate <='''+ convert(varchar(30),dateadd(day,30,getutcdate()))+''''                  
 else if @tintSortOrder=7 set @strSql=@strSql +' and bitActive =0 and dtSuspendedDate >='''+ convert(varchar(30),dateadd(day,30,getutcdate()))+''''                  
                   
                  
set @strSql=@strSql +')                     
 select RowNumber,T.numSubscriberID,S.numDivisionID,numContactID,vcCompanyName,                  
 isnull(vcFirstName,'''')+ '' ''+isnull(vcLastname,'''') AdminContact,isnull(intNoofUsersSubscribed,0) + isnull(intNoofPartialSubscribed,0)/2 + isnull(intNoofMinimalSubscribed,0)/10 as intNoofUsersSubscribed,                  
 intNoOfPartners,dbo.FormatedDateTimeFromDate(dtSubStartDate,'+Convert(varchar(15),@numDomainId) +') as dtSubStartDate,    
dbo.FormatedDateTimeFromDate(dtSubEndDate,'+Convert(varchar(15),@numDomainId) +') as dtSubEndDate,case when bitTrial=0 then ''No'' when bitTrial      
=1 then ''Yes'' else ''No'' End as Trial,            
 case when bitActive=0 then ''Suspended'' when bitActive=1 then ''Active'' else ''Trial'' End as  Status,dbo.FormatedDateTimeFromDate(DateAdd(minute,'+Convert(varchar(10), -@ClientTimeZoneOffset)+',dtSuspendedDate),'+Convert(varchar(15),@numDomainId)+
  
    
') as dtSuspendedDate,              
 vcSuspendedReason,vcEmail, isnull((SELECT SUM(ISNULL(SpaceOccupied,0)) FROM dbo.View_InboxCount where numDomainID=S.numTargetDomainID),0) as TotalSize, T.numDomainID 
 from tblSubscriber T                  
 join Subscribers S                  
 on S.numSubscriberID=T.numSubscriberID           
 join DivisionMaster D                  
 on D.numDivisionID=S.numDivisionID                  
 join Companyinfo C                  
 on C.numCompanyID=D.numCompanyID                  
 Left join  AdditionalContactsInformation A                  
 on A.numContactID=S.numAdminContactID                  
 Where RowNumber > '+convert(varchar(10),@firstRec)+' and RowNumber < '+convert(varchar(10),@lastRec)+'                   
 union select 0,count(*),null,null,null,null,null,null,null,null,null,null,null,null,null,null,null from tblSubscriber order by RowNumber'                  
print @strSql                  
exec (@strSql)

SELECT @ActiveUsers =ISNULL(SUM( isnull(intNoofUsersSubscribed,0) + isnull(intNoofPartialSubscribed,0)/2 + isnull(intNoofMinimalSubscribed,0)/10) ,0)  
 from Subscribers S
 JOIN Divisionmaster D ON D.numDivisionID = S.numDivisionID
                        JOIN Companyinfo C ON C.numCompanyID = D.numCompanyID
                        JOIN AdditionalContactsInformation A ON A.numContactID = S.numAdminContactID
 WHERE S.bitActive=1 AND S.bitDeleted = 0
 
 
GO
/****** Object:  StoredProcedure [dbo].[USP_TicklerActItems]    Script Date: 07/26/2008 16:21:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Created By Anoop jayaraj                                                        
                                                    
-- Modified by Tarun Juneja                                                    
-- date 26-08-2006                                                    
-- Reason:- Enhancement In Tickler                                                    
                                                                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_tickleractitems')
DROP PROCEDURE usp_tickleractitems
GO
CREATE PROCEDURE [dbo].[USP_TicklerActItems]                                                                        
@numUserCntID as numeric(9)=null,                                                                        
@numDomainID as numeric(9)=null,                                                                        
@startDate as datetime,                                                                        
@endDate as datetime,                                                                    
@ClientTimeZoneOffset Int,  --Added by Debasish to enable calculation of date according to client machine                                                                      
@bitFilterRecord bit=0,
@columnName varchar(50),
@columnSortOrder varchar(50),
@vcBProcessValue varchar(500)
                                                  
As                                                                         
  
DECLARE @vcSelectedEmployess VARCHAR(100)
DECLARE @numCriteria INT
DECLARE @vcActionTypes VARCHAR(100)

SELECT 
	@vcSelectedEmployess = vcSelectedEmployee,
	@numCriteria = numCriteria,
	@vcActionTypes = vcActionTypes
FROM 
	TicklerListFilterConfiguration
WHERE
	numDomainID = @numDomainID AND
	numUserCntID = @numUserCntID


Create table #tempRecords (numContactID numeric(9),numDivisionID numeric(9),tintCRMType tinyint,
Id numeric(9),bitTask varchar(100),Startdate datetime,EndTime datetime,
itemDesc text,[Name] varchar(200),vcFirstName varchar(100),vcLastName varchar(100),numPhone varchar(15),numPhoneExtension varchar(7),
[Phone] varchar(30),vcCompanyName varchar(100),vcEmail varchar(50),Task varchar(100),Activity varchar(500),
Status varchar(100),numCreatedBy numeric(9),numTerId numeric(9),numAssignedTo varchar(1000),numAssignedBy varchar(50),
 caseid numeric(9),vcCasenumber varchar(50),casetimeId numeric(9),caseExpId numeric(9),type int,itemid varchar(50),
changekey varchar(50),DueDate varchar(50),bitFollowUpAnyTime bit,numNoOfEmployeesId varchar(50),vcComPhone varchar(50),vcColorScheme VARCHAR(50),
Slp_Name varchar(100),intTotalProgress INT,vcPoppName varchar(100), numOppId NUMERIC(18,0),numBusinessProcessID numeric(18,0) )                                                         
 
declare @strSql as varchar(8000)                                                            
declare @strSql1 as varchar(8000)                                                            
declare @strSql2 as varchar(8000)                                                            
 
   -------Change Row Color-------
Declare @vcCSOrigDbCOlumnName as varchar(50),@vcCSLookBackTableName as varchar(50),@vcCSAssociatedControlType as varchar(50)
SET @vcCSOrigDbCOlumnName=''
SET @vcCSLookBackTableName=''

--select top 1 @vcCSOrigDbCOlumnName=DFM.vcOrigDbCOlumnName,@vcCSLookBackTableName=DFM.vcLookBackTableName from DycFieldColorScheme DFCS join DycFieldMaster DFM on 
--DFCS.numModuleID=DFM.numModuleID and DFCS.numFieldID=DFM.numFieldID
--where DFCS.numDomainID=@numDomainID and DFM.numModuleID=1

Create table #tempColorScheme(vcOrigDbCOlumnName varchar(50),vcLookBackTableName varchar(50),vcFieldValue varchar(50),vcFieldValue1 varchar(50),vcColorScheme varchar(50),vcAssociatedControlType varchar(50))

insert into #tempColorScheme select DFM.vcOrigDbCOlumnName,DFM.vcLookBackTableName,DFCS.vcFieldValue,DFCS.vcFieldValue1,DFCS.vcColorScheme,DFFM.vcAssociatedControlType 
from DycFieldColorScheme DFCS join DycFormField_Mapping DFFM on DFCS.numFieldID=DFFM.numFieldID
join DycFieldMaster DFM on DFM.numModuleID=DFFM.numModuleID and DFM.numFieldID=DFFM.numFieldID
where DFCS.numDomainID=@numDomainID and DFFM.numFormID=43 AND DFCS.numFormID=43 and isnull(DFFM.bitAllowGridColor,0)=1

IF(SELECT COUNT(*) FROM #tempColorScheme)>0
BEGIN
   select top 1 @vcCSOrigDbCOlumnName=vcOrigDbCOlumnName,@vcCSLookBackTableName=vcLookBackTableName,@vcCSAssociatedControlType=vcAssociatedControlType from #tempColorScheme
END   
----------------------------                   


SET @strSql = 'insert into #tempRecords
Select * from (                                                    
 SELECT Addc.numContactID,Addc.numDivisionID,Div.tintCRMType,Comm.numCommId AS Id,convert(varchar(15),bitTask) as bitTask,                                               
 DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dtStartTime) as Startdate,                        
 DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dtEndTime) as EndTime ,                                                                                                                                                                  
 --DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dtStartTime) AS CloseDate,                                                                         
 Comm.textDetails AS itemDesc,                                                                                                               
 AddC.vcFirstName + '' '' + AddC.vcLastName  as [Name], 
 AddC.vcFirstName,AddC.vcLastName,Addc.numPhone,AddC.numPhoneExtension,                               
 case when Addc.numPhone<>'''' then + AddC.numPhone +case when AddC.numPhoneExtension<>'''' then '' - '' + AddC.numPhoneExtension else '''' end  else '''' end as [Phone],                                                                       
 Comp.vcCompanyName As vcCompanyName , --changed by bharat so that the complete name is displayed                                                                
 AddC.vcEmail As vcEmail ,                                                                         
-- case when Comm.bitTask=971 then  ''Communication'' when Comm.bitTask=2 then  ''Opportunity''       
--when Comm.bitTask = 972 then  ''Task'' when Comm.bitTask=973 then  ''Notes'' When Comm.bitTask = 974 then        
--''Follow-up'' else ''Unknown'' end         
dbo.GetListIemName(Comm.bitTask)AS Task,                
 listdetailsActivity.VcData As Activity ,                                                    
 listdetailsStatus.VcData As Status   ,  -- Priority column                                                                                             
 Comm.numCreatedBy,                                                                                                             
 Div.numTerId,                                           
-- case When Len( dbo.fn_GetContactName(numAssign) ) > 12                                           
--  Then Substring( dbo.fn_GetContactName(numAssign) , 0  ,  12 ) + ''..''                                           
--  Else dbo.fn_GetContactName(numAssign)                                           
-- end                                          
-- +                                          
-- '' / ''                                             
-- +                                              
-- case                                                
--  When Len( dbo.fn_GetContactName(Comm.numAssignedBy) ) > 12                                           
--  Then Substring( dbo.fn_GetContactName(Comm.numAssignedBy) , 0  ,  12 ) + ''..''                                        Else dbo.fn_GetContactName(Comm.numAssignedBy)                                          
-- end  

Substring( 
dbo.fn_GetContactName(numAssign) + 
ISNULL((SELECT SUBSTRING((SELECT '','' + dbo.fn_GetContactName(numContactId) FROM CommunicationAttendees WHERE numCommId=Comm.numCommId FOR XML PATH('''')),1,200000)),''''),0,50)
As numAssignedTo , 
dbo.fn_GetContactName(Comm.numAssignedBy) AS numAssignedBy,        
convert(varchar(50),comm.caseid) as caseid,                          
(select top 1 vcCaseNumber from cases where cases.numcaseid= comm.caseid )as vcCasenumber,                          
comm.casetimeId,                          
comm.caseExpId ,                        
0 as type   ,                    
'''' as itemid ,                    
'''' as changekey ,cast(Null as datetime) as DueDate ,bitFollowUpAnyTime,
dbo.GetListIemName(Comp.numNoOfEmployeesId) AS numNoOfEmployeesId,Div.vcComPhone'

IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
	set @strSql=@strSql+',tCS.vcColorScheme'
ELSE
	set @strSql=@strSql+','''' as vcColorScheme'

SET @strSql =@strSql + ' ,ISNULL(Slp_Name,'''') AS Slp_Name,ISNULL(intTotalProgress,0) AS [intTotalProgress],ISNULL(vcPoppName,'''') AS [vcPoppName], ISNULL(Opp.numOppId,0) AS [numOppId],ISNULL(Opp.numBusinessProcessID,0) AS numBusinessProcessID'

SET @strSql =@strSql + ' FROM  Communication Comm                              
 JOIN AdditionalContactsInformation AddC                                                  
 ON Comm.numContactId = AddC.numContactId                                  
 JOIN DivisionMaster Div                                                                         
 ON AddC.numDivisionId = Div.numDivisionID                                   
 JOIN CompanyInfo  Comp                                                                         
 ON Div.numCompanyID = Comp.numCompanyId                                                       
 Left Join listdetails listdetailsActivity                                                  
 On Comm.numActivity = listdetailsActivity.NumlistItemID                                               
 Left Join listdetails listdetailsStatus                                                 
 On Comm.numStatus = listdetailsStatus.NumlistItemID
 LEFT JOIN OpportunityMaster Opp ON Opp.numDivisionId = Div.numDivisionID AND Opp.numContactId = AddC.numContactId  AND Opp.numOppID = Comm.numOppID 
 LEFT JOIN Sales_process_List_Master splm ON splm.Slp_Id = Opp.numBusinessProcessID 
 LEFT JOIN ProjectProgress PP On PP.numOppId = Opp.numOppId '
 
 
DECLARE @Prefix AS VARCHAR(5)
 
 IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
BEGIN
	 if @vcCSLookBackTableName = 'AdditionalContactsInformation'                  
		set @Prefix = 'AddC.'                  
    else if @vcCSLookBackTableName = 'DivisionMaster'                  
		set @Prefix = 'Div.'
	else if @vcCSLookBackTableName = 'CompanyInfo'                  
		set @Prefix = 'Comp.'  
	ELSE IF @vcCSLookBackTableName='Tickler'	
		set @Prefix = 'Comm.'  
--	else if @vcCSLookBackTableName = 'ProjectProgress'                  
--		set @Prefix = 'pp.'  	
--	else if @vcCSLookBackTableName = 'OpportunityMaster'                  
--		set @Prefix = 'om.'  
--	else if @vcCSLookBackTableName = 'Sales_process_List_Master'                  
--		set @Prefix = 'splm.'  		
	else
		set @Prefix = ''   
	
	
	IF @vcCSAssociatedControlType='DateField'
	BEGIN
	IF @vcCSOrigDbCOlumnName='FormattedDate'
			SET @vcCSOrigDbCOlumnName='dtStartTime'
	
	
			set @strSql=@strSql+' left join #tempColorScheme tCS on CONVERT(VARCHAR(10),DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',' + @Prefix + @vcCSOrigDbCOlumnName + '),111) >= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue as int),GETUTCDATE()),111)
			 and CONVERT(VARCHAR(10),DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',' + @Prefix + @vcCSOrigDbCOlumnName + '),111) <= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue1 as int),GETUTCDATE()),111)'
	END
	ELSE IF @vcCSAssociatedControlType='SelectBox' OR @vcCSAssociatedControlType='ListBox' OR @vcCSAssociatedControlType='CheckBox'  OR @vcCSAssociatedControlType='TextBox'
	BEGIN
		PRINT 1
		set @strSql=@strSql+' left join #tempColorScheme tCS on tCS.vcFieldValue=' + @Prefix + @vcCSOrigDbCOlumnName
	END
END

set @strSql=@strSql+'  WHERE Comm.numDomainID = ' + Cast(@numDomainID as varchar(10)) 
 
 IF LEN(ISNULL(@vcBProcessValue,''))>0
BEGIN
	SET @strSql = @strSql + ' and ISNULL(Opp.numBusinessProcessID,0) in (SELECT Items FROM dbo.Split(''' +@vcBProcessValue + ''','',''))' 			
END

IF LEN(@vcActionTypes) > 0 
BEGIN
	SET @strSql = @strSql + ' AND Comm.bitTask IN (' + @vcActionTypes + ')'  
END
  
IF @numCriteria  = 1 --Action Items	Assigned
BEGIN
	set @strSql=@strSql+' AND 1=(Case when Comm.numAssign IN (' + CASE WHEN LEN(@vcSelectedEmployess) > 0 THEN @vcSelectedEmployess ELSE Cast(@numUserCntID as varchar(10)) END +') then 1 else 0 end)'
END
ELSE IF @numCriteria = 2 --Action Items Owned
BEGIN
	set @strSql=@strSql+' AND 1=(Case when Comm.numCreatedBy IN (' + CASE WHEN LEN(@vcSelectedEmployess) > 0 THEN @vcSelectedEmployess ELSE Cast(@numUserCntID as varchar(10)) END +' ) then 1 else 0 end)'
END
ELSE --Action Items Assigned Or Owned
BEGIN
	set @strSql=@strSql+' AND 1=(Case when (Comm.numAssign IN (' + CASE WHEN LEN(@vcSelectedEmployess) > 0 THEN @vcSelectedEmployess ELSE Cast(@numUserCntID as varchar(10)) END +')  or Comm.numCreatedBy IN (' + CASE WHEN LEN(@vcSelectedEmployess) > 0 THEN @vcSelectedEmployess ELSE Cast(@numUserCntID as varchar(10)) END +')) then 1 else 0 end)'
END
  
                                   
set @strSql=@strSql+' AND (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',dtStartTime) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',dtStartTime) <= '''+Cast(@endDate as varchar(30))+''')                                                     
 AND Comm.bitclosedflag=0                                                                         
 and Comm.bitTask <> 973  ) As X '              

IF LEN(ISNULL(@vcBProcessValue,''))=0
BEGIN
	If LEN(ISNULL(@vcActionTypes,'')) = 0 OR CHARINDEX('98989898989898',@vcActionTypes) > 0
	BEGIN
		SET @strSql1 =  ' insert into #tempRecords 
select                          
 ACI.numContactID,ACI.numDivisionID,Div.tintCRMType,ac.activityid as id ,case when alldayevent = 1 then ''5'' else ''0'' end as bitTask,                        
DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', startdatetimeutc) startdate,                        
DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dateadd(second,duration,startdatetimeutc)) as endtime,                        
activitydescription as itemdesc,                        
ACI.vcFirstName + '' '' + ACI.vcLastName name, 
 ACI.vcFirstName,ACI.vcLastName,ACI.numPhone,ACI.numPhoneExtension,                   
 case when ACI.numPhone<>'''' then + ACI.numPhone +case when ACI.numPhoneExtension<>'''' then '' - '' + ACI.numPhoneExtension else '''' end  else '''' end as [Phone],                        
 Substring ( Comp.vcCompanyName , 0 , 12 ) + ''..'' As vcCompanyName ,                      
vcEmail,                        
''Calendar'' as task,                        
[subject] as Activity,                        
case when importance =0 then ''Low'' when importance =1 then ''Normal'' when importance =2 then ''High'' end as status,                        
0 as numcreatedby,                        
0 as numterid,                        
''-/-'' as numAssignedTo, 
''-'' AS numAssignedBy,        
''0'' as caseid,                        
null as vccasenumber,                        
0 as casetimeid,                        
0 as caseexpid,                        
1 as type ,                    
isnull(itemid,'''') as itemid  ,                    
changekey  ,cast(Null as datetime) as DueDate,cast(0 as bit) as bitFollowUpAnyTime,
dbo.GetListIemName(Comp.numNoOfEmployeesId) AS numNoOfEmployeesId,Div.vcComPhone,'''' as vcColorScheme,'''',0,'''',0 AS [numOppId],0 AS numBusinessProcessID
from activity ac join             
activityresource ar on  ac.activityid=ar.activityid             
join resource res on ar.resourceid=res.resourceid                         
--join usermaster UM on um.numUserId = res.numUserCntId                
join AdditionalContactsInformation ACI on ACI.numContactId = res.numUserCntId                
 JOIN DivisionMaster Div                                                          
 ON ACI.numDivisionId = Div.numDivisionID                                   
 JOIN CompanyInfo  Comp                             
 ON Div.numCompanyID = Comp.numCompanyId                 
 where  res.numUserCntId = ' + Cast(@numUserCntID as varchar(10)) +'             
and ac.OriginalStartDateTimeUtc IS NULL            
and res.numDomainID = ' + Cast(@numDomainID as varchar(10)) +'  and 
(DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', startdatetimeutc) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', startdatetimeutc) <= '''+Cast(@endDate as varchar(30))+''')
--(startdatetimeutc between dateadd(month,-1,getutcdate()) and dateadd(month,1,getutcdate())) 
and [status] <> -1                                                     
and ac.activityid not in 
(select distinct(numActivityId) from communication Comm where Comm.numDomainID = ' + Cast(@numDomainID as varchar(10)) +'  
AND 1=(Case ' + Cast(@bitFilterRecord as varchar(10)) +' when 1 then Case when Comm.numAssign =' + Cast(@numUserCntID as varchar(10)) +'  then 1 else 0 end 
		else  Case when (Comm.numAssign =' + Cast(@numUserCntID as varchar(10)) +'  or Comm.numCreatedBy=' + Cast(@numUserCntID as varchar(10)) +' ) then 1 else 0 end end)                                                                      
-- AND (dtStartTime >= '''+Cast(@startDate as varchar(30))+''' and dtStartTime <= '''+Cast(@endDate as varchar(30))+''') 
)
 Order by endtime'
	END

-- btDocType =2 for Document approval request, =1 for bizdoc
SET @strSql2 = ' insert into #tempRecords
select 
B.numContactID,B.numDivisionID,E.tintCRMType,CASE BA.btDocType WHEN 2 THEN BA.numOppBizDocsId WHEN 3 THEN BA.numOppBizDocsId ELSE A.numBizActionID END as ID,
convert(varchar(15),A.bitTask) as bitTask,
DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dtCreatedDate) as Startdate,                        
DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dtCreatedDate) as EndTime ,  
'''' as itemDesc,
vcFirstName + '' '' + vcLastName  as [Name],
B.vcFirstName , B.vcLastName ,B.numPhone,B.numPhoneExtension,  
case when B.numPhone<>'''' then + B.numPhone +case when B.numPhoneExtension<>'''' then '' - '' + B.numPhoneExtension else '''' end  else '''' end as [Phone],
vcCompanyName,
vcEmail,
CASE BA.btDocType WHEN 2  THEN  ''Document Approval Request'' ELSE ''BizDoc Approval Request'' END as Task,
dbo.GetListIemName(bitTask) as Activity,
'''' as Status,
A.numCreatedBy,
E.numTerId, 
case When Len( dbo.fn_GetContactName(numAssign) ) > 12                                           
  Then Substring( dbo.fn_GetContactName(numAssign) , 0  ,  12 ) + ''..''                                           
  Else dbo.fn_GetContactName(numAssign)                                           
 end                                          
 numAssignedTo ,  
 ''-'' AS numAssignedBy,        
''0'' as caseid,
''0'' as vcCasenumber,
0 as CaseTimeId,
0 as CaseExpId,
CASE BA.btDocType WHEN 2 THEN 2 ELSE 3 END as [type], 
'''' AS itemid  ,      
 '''' AS changekey ,   
dtCreatedDate as DueDate  ,cast(0 as bit) as bitFollowUpAnyTime,
dbo.GetListIemName(D.numNoOfEmployeesId) AS numNoOfEmployeesId,E.vcComPhone,'''' as vcColorScheme,'''',0,'''', 0 AS [numOppId],0 AS numBusinessProcessID
from BizDocAction A LEFT JOIN dbo.BizActionDetails BA ON BA.numBizActionId = A.numBizActionId, 
AdditionalContactsInformation B, 
CompanyInfo D,
DivisionMaster E
WHERE A.numContactId=b.numContactId and a.numDomainID=b.numDomainID  and 
D.numCompanyId=E.numCompanyId and 
D.numDomainID=a.numDomainID and 
E.numDivisionId=B.numDivisionId and 
a.numDomainID= ' + Cast(@numDomainID as varchar(10)) +' and a.numContactId = ' + Cast(@numUserCntID as varchar(10)) +'  and  
A.numStatus=0 
and (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dtCreatedDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dtCreatedDate) <= '''+Cast(@endDate as varchar(30))+''')'

END


declare @Nocolumns as tinyint                
set @Nocolumns=0       
         
Select @Nocolumns=isnull(count(*),0) from View_DynamicColumns where numFormId=43 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 
  
  CREATE TABLE #tempForm (tintOrder TINYINT,vcDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),
  vcListItemType char(3),numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,
  numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,bitIsAlphaNumeric BIT,bitIsNumeric BIT,
  bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),ListRelID numeric(9))
                 
if @Nocolumns > 0            
begin      
INSERT INTO #tempForm
select tintRow+1 as tintOrder,vcDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
 vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName                                                 
,bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID 
 FROM View_DynamicColumns 
 where numFormId=43 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 
  AND ISNULL(bitSettingField,0)=1 AND ISNULL(bitCustom,0)=0
  order by tintOrder asc  
  
end            
else            
begin 

	--Check if Master Form Configuration is created by administrator if NoColumns=0
	DECLARE @IsMasterConfAvailable BIT = 0
	DECLARE @numUserGroup NUMERIC(18,0)

	SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

	IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = 43 AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
	BEGIN
		SET @IsMasterConfAvailable = 1
	END

	--If MasterConfiguration is available then load it otherwise load default columns
	IF @IsMasterConfAvailable = 1
	BEGIN
		INSERT INTO #tempForm
		SELECT 
			(intRowNum + 1) as tintOrder, vcDbColumnName, ISNULL(vcCultureFieldName,vcFieldName), vcAssociatedControlType,
			vcListItemType,numListID,vcLookBackTableName, bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID 
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=43 AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
	ORDER BY 
		tintOrder ASC  
	END
	ELSE                                        
	BEGIN
		 INSERT INTO #tempForm
		select tintOrder,vcDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
		 vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName                                                 
		,bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
		bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
		intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID 
		 FROM View_DynamicDefaultColumns
		 where numFormId=43 and bitDefault=1 AND ISNULL(bitSettingField,0)=1 AND numDomainID=@numDomainID
		order by tintOrder asc  
	END 
end                                            

DECLARE @strSql3 AS VARCHAR(8000)

SET @strSql3=   ' select * from #tempRecords order by ' + @columnName +' ' + @columnSortOrder  

print @strSql
exec (@strSql + @strSql1 + @strSql2 + @strSql3 )
drop table #tempRecords

SELECT * FROM #tempForm

DROP TABLE #tempForm
/****** Object:  StoredProcedure [dbo].[USP_TicklerCases]    Script Date: 07/26/2008 16:21:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_ticklercases')
DROP PROCEDURE usp_ticklercases
GO
CREATE PROCEDURE [dbo].[USP_TicklerCases]                          
@numUserCntID as numeric(9)=null,                          
@numDomainID as numeric(9)=null,                          
@startDate as datetime,                          
@endDate as datetime                         
as                          

DECLARE @vcSelectedEmployess VARCHAR(100)
DECLARE @numCriteria INT

SELECT 
	@vcSelectedEmployess = vcSelectedEmployeeCases,
	@numCriteria = numCriteriaCases
FROM 
	TicklerListFilterConfiguration
WHERE
	numDomainID = @numDomainID AND
	numUserCntID = @numUserCntID


declare @strSql as varchar(8000)   
                
SET @strSql = 'SELECT     
	C.numCaseId,                          
	AddC.vcFirstName + '' '' + AddC.vcLastName as Contact,
	(CASE 
		WHEN Addc.numPhone <> '''' 
		THEN AddC.numPhone + (CASE 
								WHEN AddC.numPhoneExtension<>'''' 
								THEN '' - '' + AddC.numPhoneExtension 
								ELSE '''' 
							  END)  
		ELSE '''' 
	END) AS [Phone],                      
	AddC.vcEmail,                                                                        
	C.intTargetResolveDate,                           
	C.vcCaseNumber,                           
	Com.vcCompanyName,                          
	C.textSubject ,                                                 
	Div.numTerId,    
	c.numRecOwner,                                               
	dbo.fn_GetContactName(C.numAssignedTo) as AssignedTo                         
FROM 
	Cases  C                          
INNER JOIN  
	AdditionalContactsInformation  AddC                          
ON 
	C.numContactId = AddC.numContactId                           
INNER JOIN 
	DivisionMaster Div                           
ON 
	AddC.numDivisionId = Div.numDivisionID AND  
	AddC.numDivisionId = Div.numDivisionID                           
INNER JOIN  
	CompanyInfo  Com                          
ON 
	Div.numCompanyID = Com.numCompanyId                            
WHERE  
	C.numDomainid=' + CAST(@numDomainID AS VARCHAR(10)) + ' AND 
	C.numStatus <> 136 AND 
	(intTargetResolveDate >= '''+ Cast(@startDate as varchar(30)) +''' and intTargetResolveDate <= ''' + CAST(@endDate AS VARCHAR(30)) + ''')'

IF @numCriteria  = 1 --Action Items	Assigned
BEGIN
	set @strSql=@strSql+' AND C.numAssignedTo IN (' + CASE WHEN LEN(@vcSelectedEmployess) > 0 THEN @vcSelectedEmployess ELSE Cast(@numUserCntID as varchar(10)) END +')'
END
ELSE IF @numCriteria = 2 --Action Items Owned
BEGIN
	set @strSql=@strSql+' AND C.numCreatedby IN (' + CASE WHEN LEN(@vcSelectedEmployess) > 0 THEN @vcSelectedEmployess ELSE Cast(@numUserCntID as varchar(10)) END +')'
END
ELSE --Action Items Assigned Or Owned
BEGIN
	set @strSql=@strSql+' AND (C.numAssignedTo IN (' + CASE WHEN LEN(@vcSelectedEmployess) > 0 THEN @vcSelectedEmployess ELSE Cast(@numUserCntID as varchar(10)) END +')  or C.numCreatedby IN (' + CASE WHEN LEN(@vcSelectedEmployess) > 0 THEN @vcSelectedEmployess ELSE Cast(@numUserCntID as varchar(10)) END +' ))'
END
                   
SET @strSql = @strSql + 'ORDER BY 
	intTargetResolveDate DESC'

EXEC(@strSql)
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_TicklerListFilterConfiguration_Get')
DROP PROCEDURE dbo.USP_TicklerListFilterConfiguration_Get
GO
CREATE PROCEDURE [dbo].[USP_TicklerListFilterConfiguration_Get]
    (
	  @numDomainID NUMERIC(18,0),
      @numUserCntID NUMERIC(18,0)
    )
AS 
BEGIN
   
	SELECT
		*
	FROM
		TicklerListFilterConfiguration
	WHERE
		numDomainID = @numDomainID AND
		numUserCntID = @numUserCntID
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_TicklerListFilterConfiguration_Save')
DROP PROCEDURE dbo.USP_TicklerListFilterConfiguration_Save
GO
CREATE PROCEDURE [dbo].[USP_TicklerListFilterConfiguration_Save]
    (
	  @numDomainID NUMERIC(18,0),
      @numUserCntID NUMERIC(18,0),
	  @vcSelectedEmployee VARCHAR(500),
	  @numCriteria INT,
	  @vcActionTypes VARCHAR(500),
	  @vcSelectedEmployeeOpp VARCHAR(500),
	  @vcSelectedEmployeeCases VARCHAR(500),
	  @vcAssignedStages VARCHAR(500),
	  @numCriteriaCases INT
    )
AS 
BEGIN

	IF EXISTS (SELECT ID FROM TicklerListFilterConfiguration WHERE numDomainID = @numDomainID AND numUserCntID = @numUserCntID)
	BEGIN
		UPDATE
			TicklerListFilterConfiguration
		SET
			vcSelectedEmployee=@vcSelectedEmployee,
			numCriteria=@numCriteria,
			vcActionTypes=@vcActionTypes,
			vcSelectedEmployeeOpp=@vcSelectedEmployeeOpp,
			vcSelectedEmployeeCases=@vcSelectedEmployeeCases,
			vcAssignedStages=@vcAssignedStages,
			numCriteriaCases=@numCriteriaCases
		WHERE
			numDomainID = @numDomainID AND 
			numUserCntID = @numUserCntID
	END
	ELSE
	BEGIN
		INSERT INTO TicklerListFilterConfiguration
			(
				numDomainID,
				numUserCntID,
				vcSelectedEmployee,
				numCriteria,
				vcActionTypes,
				vcSelectedEmployeeOpp,
				vcSelectedEmployeeCases,
				vcAssignedStages,
				numCriteriaCases
			)
		VALUES
			(
				@numDomainID,
				@numUserCntID,
				@vcSelectedEmployee,
				@numCriteria,
				@vcActionTypes,
				@vcSelectedEmployeeOpp,
				@vcSelectedEmployeeCases,
				@vcAssignedStages,
				@numCriteriaCases
			)
	END

END
/****** Object:  StoredProcedure [dbo].[usp_TicklerOpportuity]    Script Date: 07/26/2008 16:21:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                              
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_tickleropportuity')
DROP PROCEDURE usp_tickleropportuity
GO
CREATE PROCEDURE [dbo].[usp_TicklerOpportuity]                                      
@numUserCntID as numeric(9)=null,                                      
@numDomainID as numeric(9)=null,                                      
@startDate as datetime,                                      
@endDate as datetime                                     
as                                       
  
DECLARE @vcSelectedEmployess VARCHAR(500)
DECLARE @vcAssignedStages VARCHAR(500)
DECLARE @vcActionTypes VARCHAR(100)
DECLARE @strSql VARCHAR(8000)

SELECT 
	@vcSelectedEmployess = vcSelectedEmployeeOpp,
	@vcAssignedStages = vcAssignedStages
FROM 
	TicklerListFilterConfiguration
WHERE
	numDomainID = @numDomainID AND
	numUserCntID = @numUserCntID


SELECT   
		 Split.a.value('.', 'VARCHAR(100)') AS Data  
	INTO 
		#TEMP
	FROM  
	(
		SELECT CAST ('<M>' + REPLACE(ISNULL(@vcAssignedStages,''),',','</M><M>') + '</M>' AS XML) AS Data  
	) AS A CROSS APPLY Data.nodes ('/M') AS Split(a); 

IF LEN(@vcAssignedStages) = 0 OR (SELECT COUNT(*) FROM #TEMP WHERE Data = 1) > 0 --Sales Opportunity
BEGIN
	SET @strSql = 'select 
						SPD.numStageDetailsId,
						SPD.vcStageName,
						SPD.tinProgressPercentage as Tprogress,
						SPD.dtEndDate as CloseDate,
						opp.numOppId as ID,vcPOppName,
						''Sales Opportunity'' AS Type,
						Div.numTerId                                      
					FROM 
						OpportunityMaster opp 
					JOIN 
						StagePercentageDetails SPD 
					ON 
						opp.numOppID=SPD.numOppID 
					JOIN 
						DivisionMaster div 
					ON 
						div.numDivisionID=opp.numDivisionID  
					WHERE 
						opp.tintOppType= 1 AND 
						opp.tintOppStatus = 0 AND
						opp.numDomainID=' + CAST(@numDomainID AS VARCHAR(10))
						 + ' AND (SPD.numAssignTo IN (' + CASE WHEN LEN(@vcSelectedEmployess) > 0 THEN @vcSelectedEmployess ELSE Cast(@numUserCntID as varchar(10)) END +')  or SPD.numCreatedBy IN (' + CASE WHEN LEN(@vcSelectedEmployess) > 0 THEN @vcSelectedEmployess ELSE Cast(@numUserCntID as varchar(10)) END +' ))'
						 + ' AND (SPD.dtEndDate  >= '''+ Cast(@startDate as varchar(30)) +''' AND SPD.dtEndDate  <= ''' + CAST(@endDate AS VARCHAR(30)) + ''')'
END

IF LEN(@vcAssignedStages) = 0 OR (SELECT COUNT(*) FROM #TEMP WHERE Data = 2) > 0 --Purchase Opportunity
BEGIN
	SET @strSql =  @strSql + ' UNION
					select 
						SPD.numStageDetailsId,
						SPD.vcStageName,
						SPD.tinProgressPercentage as Tprogress,
						SPD.dtEndDate as CloseDate,
						opp.numOppId as ID,vcPOppName,
						''Purchase Opportunity'' AS Type,
						Div.numTerId                                      
					FROM 
						OpportunityMaster opp 
					JOIN 
						StagePercentageDetails SPD 
					ON 
						opp.numOppID=SPD.numOppID 
					JOIN 
						DivisionMaster div 
					ON 
						div.numDivisionID=opp.numDivisionID  
					WHERE 
						opp.tintOppType= 2 AND 
						opp.tintOppStatus = 0 AND
						opp.numDomainID=' + CAST(@numDomainID AS VARCHAR(10))
						 + ' AND (SPD.numAssignTo IN (' + CASE WHEN LEN(@vcSelectedEmployess) > 0 THEN @vcSelectedEmployess ELSE Cast(@numUserCntID as varchar(10)) END +')  or SPD.numCreatedBy IN (' + CASE WHEN LEN(@vcSelectedEmployess) > 0 THEN @vcSelectedEmployess ELSE Cast(@numUserCntID as varchar(10)) END +' ))'
						 + ' AND (SPD.dtEndDate  >= '''+ Cast(@startDate as varchar(30)) +''' AND SPD.dtEndDate  <= ''' + CAST(@endDate AS VARCHAR(30)) + ''')'
END
	
IF LEN(@vcAssignedStages) = 0 OR (SELECT COUNT(*) FROM #TEMP WHERE Data = 3) > 0 --Sales Order
BEGIN
	SET @strSql = @strSql + ' UNION
					select 
						SPD.numStageDetailsId,
						SPD.vcStageName,
						SPD.tinProgressPercentage as Tprogress,
						SPD.dtEndDate as CloseDate,
						opp.numOppId as ID,vcPOppName,
						''Sales Order'' AS Type,
						Div.numTerId                                      
					FROM 
						OpportunityMaster opp 
					JOIN 
						StagePercentageDetails SPD 
					ON 
						opp.numOppID=SPD.numOppID 
					JOIN 
						DivisionMaster div 
					ON 
						div.numDivisionID=opp.numDivisionID  
					WHERE 
						opp.tintOppType= 1 AND 
						opp.tintOppStatus = 1 AND
						opp.numDomainID=' + CAST(@numDomainID AS VARCHAR(10))
						 + ' AND (SPD.numAssignTo IN (' + CASE WHEN LEN(@vcSelectedEmployess) > 0 THEN @vcSelectedEmployess ELSE Cast(@numUserCntID as varchar(10)) END +')  or SPD.numCreatedBy IN (' + CASE WHEN LEN(@vcSelectedEmployess) > 0 THEN @vcSelectedEmployess ELSE Cast(@numUserCntID as varchar(10)) END +' ))'
						 + ' AND (SPD.dtEndDate  >= '''+ Cast(@startDate as varchar(30)) +''' AND SPD.dtEndDate  <= ''' + CAST(@endDate AS VARCHAR(30)) + ''')'
END

IF LEN(@vcAssignedStages) = 0 OR (SELECT COUNT(*) FROM #TEMP WHERE Data = 4) > 0 --Purchase Order
BEGIN
	SET @strSql = @strSql + ' UNION
					select 
						SPD.numStageDetailsId,
						SPD.vcStageName,
						SPD.tinProgressPercentage as Tprogress,
						SPD.dtEndDate as CloseDate,
						opp.numOppId as ID,vcPOppName,
						''Purchase Order'' AS Type,
						Div.numTerId                                      
					FROM 
						OpportunityMaster opp 
					JOIN 
						StagePercentageDetails SPD 
					ON 
						opp.numOppID=SPD.numOppID 
					JOIN 
						DivisionMaster div 
					ON 
						div.numDivisionID=opp.numDivisionID  
					WHERE 
						opp.tintOppType= 2 AND 
						opp.tintOppStatus = 1 AND
						opp.numDomainID=' + CAST(@numDomainID AS VARCHAR(10))
						 + ' AND (SPD.numAssignTo IN (' + CASE WHEN LEN(@vcSelectedEmployess) > 0 THEN @vcSelectedEmployess ELSE Cast(@numUserCntID as varchar(10)) END +')  or SPD.numCreatedBy IN (' + CASE WHEN LEN(@vcSelectedEmployess) > 0 THEN @vcSelectedEmployess ELSE Cast(@numUserCntID as varchar(10)) END +' ))'
						 + ' AND (SPD.dtEndDate  >= '''+ Cast(@startDate as varchar(30)) +''' AND SPD.dtEndDate  <= ''' + CAST(@endDate AS VARCHAR(30)) + ''')'
END

IF LEN(@vcAssignedStages) = 0 OR (SELECT COUNT(*) FROM #TEMP WHERE Data = 5) > 0 --Project
BEGIN
	SET @strSql = @strSql + 'UNION                     
							SELECT 
								SPD.numStageDetailsId,
								SPD.vcStageName,
								SPD.tinProgressPercentage as Tprogress,
								SPD.dtEndDate as CloseDate,
								Pro.numProId as ID,
								vcProjectName as vcPOppName,
								''Project'' as Type,Div.numTerId  
							FROM 
								ProjectsMaster Pro 
							JOIN 
								StagePercentageDetails SPD 
							ON 
								Pro.numProId=SPD.numProjectID
							JOIN 
								DivisionMaster div 
							ON 
								div.numDivisionID=Pro.numDivisionID   
							WHERE 
								Pro.numDomainID=' + CAST(@numDomainID AS VARCHAR(10))
							+ ' AND (SPD.numAssignTo IN (' + CASE WHEN LEN(@vcSelectedEmployess) > 0 THEN @vcSelectedEmployess ELSE Cast(@numUserCntID as varchar(10)) END +')  or SPD.numCreatedBy IN (' + CASE WHEN LEN(@vcSelectedEmployess) > 0 THEN @vcSelectedEmployess ELSE Cast(@numUserCntID as varchar(10)) END +' ))'
							+ ' AND (SPD.dtEndDate  >= '''+ Cast(@startDate as varchar(30)) +''' and SPD.dtEndDate  <= ''' + CAST(@endDate AS VARCHAR(30)) + ''') ORDER BY dtEndDate Desc'                   
END


Print @strSql
EXEC(@strSql)   

DROP TABLE #TEMP                           
GO
/****** Object:  StoredProcedure [dbo].[USP_UpdatedomainDetails]    Script Date: 05/07/2009 21:56:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                                      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_updatedomaindetails')
DROP PROCEDURE usp_updatedomaindetails
GO
CREATE PROCEDURE [dbo].[USP_UpdatedomainDetails]                                      
@numDomainID as numeric(9)=0,                                      
@vcDomainName as varchar(50)='',                                      
@vcDomainDesc as varchar(100)='',                                      
@bitExchangeIntegration as bit,                                      
@bitAccessExchange as bit,                                      
@vcExchUserName as varchar(100)='',                                      
@vcExchPassword as varchar(100)='',                                      
@vcExchPath as varchar(200)='',                                      
@vcExchDomain as varchar(100)='',                                    
@tintCustomPagingRows as tinyint,                                    
@vcDateFormat as varchar(30)='',                                    
@numDefCountry as numeric(9),                                    
@tintComposeWindow as tinyint,                                    
@sintStartDate as smallint,                                    
@sintNoofDaysInterval as smallint,                                    
@tintAssignToCriteria as tinyint,                                    
@bitIntmedPage as bit,                                    
@tintFiscalStartMonth as tinyint,                            
--@bitDeferredIncome as bit,                               
@tintPayPeriod as tinyint,                        
@numCurrencyID as numeric(9) ,                
@charUnitSystem as char(1)='',              
@vcPortalLogo as varchar(100)='',            
@tintChrForComSearch as tinyint  ,            
@intPaymentGateWay as int,    
@tintChrForItemSearch as tinyint,
@ShipCompany as numeric(9),
@bitAutoPopulateAddress as bit=null,
@tintPoulateAddressTo as tinyint=null,
@bitMultiCurrency as bit,
--@bitMultiCompany as BIT,
@bitCreateInvoice AS tinyint = null,
@numDefaultSalesBizDocID AS NUMERIC(9)=NULL,
@numDefaultPurchaseBizDocID AS NUMERIC(9)=NULL,
@bitSaveCreditCardInfo AS BIT = NULL,
@intLastViewedRecord AS INT=10,
@tintCommissionType AS TINYINT,
@tintBaseTax AS TINYINT,
@bitEmbeddedCost AS BIT,
@numDefaultSalesOppBizDocId NUMERIC=NULL,
@tintSessionTimeOut TINYINT=1,
@tintDecimalPoints TINYINT,
@bitCustomizePortal BIT=0,
@tintBillToForPO TINYINT,
@tintShipToForPO TINYINT,
@tintOrganizationSearchCriteria TINYINT,
@bitDocumentRepositary BIT=0,
@tintComAppliesTo TINYINT=0,
@bitGtoBContact BIT=0,
@bitBtoGContact BIT=0,
@bitGtoBCalendar BIT=0,
@bitBtoGCalendar BIT=0,
@bitExpenseNonInventoryItem BIT=0,
@bitInlineEdit BIT=0,
@bitRemoveVendorPOValidation BIT=0,
@tintBaseTaxOnArea tinyint=0,
@bitAmountPastDue BIT=0,
@monAmountPastDue money,
@bitSearchOrderCustomerHistory BIT=0,
@bitRentalItem as bit=0,
@numRentalItemClass as NUMERIC(9)=NULL,
@numRentalHourlyUOM as NUMERIC(9)=NULL,
@numRentalDailyUOM as NUMERIC(9)=NULL,
@tintRentalPriceBasedOn as tinyint=NULL,
@tintCalendarTimeFormat as tinyint=NULL,
@tintDefaultClassType as tinyint=NULL,
@tintPriceBookDiscount as tinyint=0,
@bitAutoSerialNoAssign as bit=0,
@bitIsShowBalance AS BIT = 0,
@numIncomeAccID NUMERIC(18, 0) = 0,
@numCOGSAccID NUMERIC(18, 0) = 0,
@numAssetAccID NUMERIC(18, 0) = 0,
@IsEnableClassTracking AS BIT = 0,
@IsEnableProjectTracking AS BIT = 0,
@IsEnableCampaignTracking AS BIT = 0,
@IsEnableResourceScheduling AS BIT = 0,
@ShippingServiceItem  AS NUMERIC(9)=0,
@numSOBizDocStatus AS NUMERIC(9)=0,
@bitAutolinkUnappliedPayment AS BIT=0,
@numDiscountServiceItemID AS NUMERIC(9)=0,
@vcShipToPhoneNumber as VARCHAR(50)='',
@vcGAUserEmailId as VARCHAR(50)='',
@vcGAUserPassword as VARCHAR(50)='',
@vcGAUserProfileId as VARCHAR(50)='',
@bitDiscountOnUnitPrice AS BIT=0,
@intShippingImageWidth INT = 0,
@intShippingImageHeight INT = 0,
@numTotalInsuredValue NUMERIC(10,2) = 0,
@numTotalCustomsValue NUMERIC(10,2) = 0,
@vcHideTabs VARCHAR(1000) = '',
@bitUseBizdocAmount BIT = 0,
@IsEnableUserLevelClassTracking BIT = 0,
@bitDefaultRateType BIT = 0,
@bitIncludeTaxAndShippingInCommission  BIT = 0,
/*,@bitAllowPPVariance AS BIT=0*/
@bitPurchaseTaxCredit BIT=0,
@bitLandedCost BIT=0,
@vcLanedCostDefault VARCHAR(10)='',
@bitMinUnitPriceRule BIT = 0,
@numAbovePercent AS NUMERIC(18,2),
@numAbovePriceField AS NUMERIC(18,0),
@numBelowPercent AS NUMERIC(18,2),
@numBelowPriceField AS NUMERIC(18,0),
@numOrderStatusBeforeApproval AS NUMERIC(18,0),
@numOrderStatusAfterApproval AS NUMERIC(18,0),
@vcUnitPriceApprover AS VARCHAR(100),
@numDefaultSalesPricing AS TINYINT
as                                      
                                      
 update Domain                                       
   set                                       
   vcDomainName=@vcDomainName,                                      
   vcDomainDesc=@vcDomainDesc,                                      
   bitExchangeIntegration=@bitExchangeIntegration,                                      
   bitAccessExchange=@bitAccessExchange,                                      
   vcExchUserName=@vcExchUserName,                                      
   vcExchPassword=@vcExchPassword,                                      
   vcExchPath=@vcExchPath,                                      
   vcExchDomain=@vcExchDomain,                                    
   tintCustomPagingRows =@tintCustomPagingRows,                                    
   vcDateFormat=@vcDateFormat,                                    
   numDefCountry =@numDefCountry,                                    
   tintComposeWindow=@tintComposeWindow,                                    
   sintStartDate =@sintStartDate,                                    
   sintNoofDaysInterval=@sintNoofDaysInterval,                                    
   tintAssignToCriteria=@tintAssignToCriteria,                                    
   bitIntmedPage=@bitIntmedPage,                                    
   tintFiscalStartMonth=@tintFiscalStartMonth,                            
--   bitDeferredIncome=@bitDeferredIncome,                          
   tintPayPeriod=@tintPayPeriod,
   vcCurrency=isnull((select varCurrSymbol from Currency Where numCurrencyID=@numCurrencyID  ),'')  ,                      
  numCurrencyID=@numCurrencyID,                
  charUnitSystem= @charUnitSystem,              
  vcPortalLogo=@vcPortalLogo ,            
  tintChrForComSearch=@tintChrForComSearch  ,      
  intPaymentGateWay=@intPaymentGateWay,
 tintChrForItemSearch=@tintChrForItemSearch,
 numShipCompany= @ShipCompany,
 bitAutoPopulateAddress = @bitAutoPopulateAddress,
 tintPoulateAddressTo =@tintPoulateAddressTo,
 --bitMultiCompany=@bitMultiCompany ,
 bitMultiCurrency=@bitMultiCurrency,
 bitCreateInvoice= @bitCreateInvoice,
 [numDefaultSalesBizDocId] =@numDefaultSalesBizDocId,
 bitSaveCreditCardInfo=@bitSaveCreditCardInfo,
 intLastViewedRecord=@intLastViewedRecord,
 [tintCommissionType]=@tintCommissionType,
 [tintBaseTax]=@tintBaseTax,
 [numDefaultPurchaseBizDocId]=@numDefaultPurchaseBizDocID,
 bitEmbeddedCost=@bitEmbeddedCost,
 numDefaultSalesOppBizDocId=@numDefaultSalesOppBizDocId,
 tintSessionTimeOut=@tintSessionTimeOut,
 tintDecimalPoints=@tintDecimalPoints,
 bitCustomizePortal=@bitCustomizePortal,
 tintBillToForPO = @tintBillToForPO,
 tintShipToForPO = @tintShipToForPO,
 tintOrganizationSearchCriteria=@tintOrganizationSearchCriteria,
-- vcPortalName=@vcPortalName,
 bitDocumentRepositary=@bitDocumentRepositary,tintComAppliesTo=@tintComAppliesTo,
 bitGtoBContact=@bitGtoBContact,
 bitBtoGContact=@bitBtoGContact,
 bitGtoBCalendar=@bitGtoBCalendar,
 bitBtoGCalendar=@bitBtoGCalendar,
 bitExpenseNonInventoryItem=@bitExpenseNonInventoryItem,
 bitInlineEdit=@bitInlineEdit,
 bitRemoveVendorPOValidation=@bitRemoveVendorPOValidation,
tintBaseTaxOnArea=@tintBaseTaxOnArea,
bitAmountPastDue = @bitAmountPastDue,
monAmountPastDue = @monAmountPastDue,
bitSearchOrderCustomerHistory=@bitSearchOrderCustomerHistory,
bitRentalItem=@bitRentalItem,
numRentalItemClass=@numRentalItemClass,
numRentalHourlyUOM=@numRentalHourlyUOM,
numRentalDailyUOM=@numRentalDailyUOM,
tintRentalPriceBasedOn=@tintRentalPriceBasedOn,
tintCalendarTimeFormat=@tintCalendarTimeFormat,
tintDefaultClassType=@tintDefaultClassType,
tintPriceBookDiscount=@tintPriceBookDiscount,
bitAutoSerialNoAssign=@bitAutoSerialNoAssign,
bitIsShowBalance = @bitIsShowBalance,
numIncomeAccID = @numIncomeAccID,
numCOGSAccID = @numCOGSAccID,
numAssetAccID = @numAssetAccID,
IsEnableClassTracking = @IsEnableClassTracking,
IsEnableProjectTracking = @IsEnableProjectTracking,
IsEnableCampaignTracking = @IsEnableCampaignTracking,
IsEnableResourceScheduling = @IsEnableResourceScheduling,
numShippingServiceItemID = @ShippingServiceItem,
numSOBizDocStatus=@numSOBizDocStatus,
bitAutolinkUnappliedPayment=@bitAutolinkUnappliedPayment,
numDiscountServiceItemID=@numDiscountServiceItemID,
vcShipToPhoneNo = @vcShipToPhoneNumber,
vcGAUserEMail = @vcGAUserEmailId,
vcGAUserPassword = @vcGAUserPassword,
vcGAUserProfileId = @vcGAUserProfileId,
bitDiscountOnUnitPrice=@bitDiscountOnUnitPrice,
intShippingImageWidth = @intShippingImageWidth ,
intShippingImageHeight = @intShippingImageHeight,
numTotalInsuredValue = @numTotalInsuredValue,
numTotalCustomsValue = @numTotalCustomsValue,
vcHideTabs = @vcHideTabs,
bitUseBizdocAmount = @bitUseBizdocAmount,
IsEnableUserLevelClassTracking = @IsEnableUserLevelClassTracking,
bitDefaultRateType = @bitDefaultRateType,
bitIncludeTaxAndShippingInCommission = @bitIncludeTaxAndShippingInCommission,
/*,bitAllowPPVariance=@bitAllowPPVariance*/
bitPurchaseTaxCredit=@bitPurchaseTaxCredit,
bitLandedCost=@bitLandedCost,
vcLanedCostDefault=@vcLanedCostDefault,
bitMinUnitPriceRule = @bitMinUnitPriceRule,
numAbovePercent = @numAbovePercent,
numAbovePriceField = @numAbovePriceField,
numBelowPercent = @numBelowPercent,
numBelowPriceField = @numBelowPriceField,
numOrderStatusBeforeApproval = @numOrderStatusBeforeApproval,
numOrderStatusAfterApproval = @numOrderStatusAfterApproval,
numDefaultSalesPricing = @numDefaultSalesPricing
 where numDomainId=@numDomainID

DELETE FROM UnitPriceApprover WHERE numDomainID = @numDomainID

INSERT INTO UnitPriceApprover
	(
		numDomainID,
		numUserID
	)
SELECT
     @numDomainID,
	 Split.a.value('.', 'VARCHAR(100)') AS String
FROM  
(
SELECT 
        CAST ('<M>' + REPLACE(@vcUnitPriceApprover, ',', '</M><M>') + '</M>' AS XML) AS String  
) AS A 
CROSS APPLY 
	String.nodes ('/M') AS Split(a); 

/****** Object:  StoredProcedure [dbo].[USP_UpdateVendors]    Script Date: 07/26/2008 16:21:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_updatevendors')
DROP PROCEDURE usp_updatevendors
GO
CREATE PROCEDURE [dbo].[USP_UpdateVendors]          
(          
@strFieldList as text='',    
@Itemcode as numeric(9)=0     
)          
as          
          

IF convert(varchar(10),@strFieldList) <>''
BEGIN
	 declare @hDoc  int          
	 EXEC sp_xml_preparedocument @hDoc OUTPUT, @strFieldList          
	 update Vendor          
	  set vcPartNo= X.PartNo,monCost=X.monCost,[intMinQty]=X.intMinQty,intLeadTimeDays=X.intLeadTimeDays           
	                
	 from(SELECT  * FROM OPENXML (@hDoc,'/NewDataSet/Table',2)          
	 WITH ( VendorID numeric(9),          
	  PartNo varchar(100),      
	  monCost MONEY,          
	  intMinQty int,
	  intLeadTimeDays int
	  ))X          
	 where numVendorID=X.VendorID  and  numItemCode=  @Itemcode      
	              
	 EXEC sp_xml_removedocument @hDoc
 
 END
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ValidateBizDocSequenceId' ) 
    DROP PROCEDURE USP_ValidateBizDocSequenceId
GO

CREATE PROCEDURE USP_ValidateBizDocSequenceId
@numDomainID NUMERIC(9),
@numOppId NUMERIC(9),
@numOppBizDocsId NUMERIC(9),
@numBizDocId NUMERIC(9),
@numSequenceId VARCHAR(50)
AS 
BEGIN
	SELECT OBD.numOppBizDocsId,OBD.numOppId FROM dbo.OpportunityMaster OM JOIN dbo.OpportunityBizDocs OBD ON OM.numOppId=obd.numOppId
	WHERE om.numDomainId = @numDomainID 
	AND obd.numBizDocId = @numBizDocId 
	--AND om.numOppId != @numOppId 
	AND (obd.numOppBizDocsId = ISNULL(@numOppBizDocsId,0) OR @numOppBizDocsId = 0)
	AND obd.numSequenceId=@numSequenceId
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_WorkFlowMaster_History')
DROP PROCEDURE USP_WorkFlowMaster_History
GO
  
 
-- =============================================      
-- Author:  <Author,,Sachin Sadhu>      
-- Create date: <Create Date,,3rdApril2014>      
-- Description: <Description,,To maintain Work rule execution history>      
-- =============================================      
Create PROCEDURE [dbo].[USP_WorkFlowMaster_History]       
 -- Add the parameters for the stored procedure here      
@numDomainID numeric(18,0),      
@numFormID INT  ,    
@CurrentPage int,                                                              
@PageSize int,                                                              
@TotRecs int output,           
@SortChar char(1)='0' ,                                                             
@columnName as Varchar(50),                                                              
@columnSortOrder as Varchar(50)  ,      
@SearchStr  as Varchar(50)         
AS      
BEGIN      
 -- SET NOCOUNT ON added to prevent extra result sets from      
 -- interfering with SELECT statements.      
 SET NOCOUNT ON;      
      
    -- Insert statements for procedure here      
    Create table #tempTable ( ID INT IDENTITY PRIMARY KEY,                                                               
        numWFID NUMERIC(18,0) ,    
        numFormID NUMERIC(18,0),    
        vcWFName VARCHAR(50),      
        vcPOppName VARCHAR(500),      
        vcWFDescription VARCHAR(max),      
        numRecordID NUMERIC(18,0),      
        tintProcessStatus int,      
        bitSuccess int,      
        vcDescription VARCHAR(max),      
        vcFormName VARCHAR(50),      
        dtExecutionDate DATETIME,    
        numOppId NUMERIC(18,0),      
        STATUS VARCHAR(50),    
        TriggeredOn VARCHAR(50) ,  
        vcUserName VARCHAR(200)                                                           
 )           
declare @strSql as varchar(8000)                                                        
          
set  @strSql='  SELECT       
      
   m.numWFID,      
   m.numFormID,    
   m.vcWFName,      
   ISNULL(o.vcPOppName,'''') as vcPOppName,      
   m.vcWFDescription,      
   q.numRecordID,      
   q.tintProcessStatus,      
   e.bitSuccess,      
  ISNULL( e.vcDescription,''Waiting'') as  vcDescription,      
   DFM.vcFormName,      
   e.dtExecutionDate,    
   o.numOppId,      
   CASE WHEN q.tintProcessStatus=1 THEN ''Pending Execution'' WHEN q.tintProcessStatus= 2 THEN ''In Progress''  WHEN q.tintProcessStatus=3 THEN ''Success'' WHEN q.tintProcessStatus=4 then ''Failed'' WHEN q.tintProcessStatus=6 then ''Condition Not Matched'' else ''No WF Found'' END AS STATUS,      
   CASE WHEN (m.tintWFTriggerOn=1 AND m.intDays=0)THEN ''Create'' WHEN m.tintWFTriggerOn=2 THEN ''Edit'' WHEN m.tintWFTriggerOn=3 THEN ''Create or Edit'' WHEN m.tintWFTriggerOn=4 THEN ''Fields Update'' WHEN m.tintWFTriggerOn=5 THEN ''Delete'' WHEN m.intDays>0 THEN     
''Date Field'' ELSE ''NA'' end AS TriggeredOn,  
 dbo.fn_GetContactName(ISNULL(q.numCreatedBy,0)) AS vcUserName      
  FROM dbo.WorkFlowMaster as m       
  INNER JOIN dbo.WorkFlowQueue AS q ON m.numWFID=q.numWFID       
  LEFT JOIN dbo.WorkFlowQueueExecution AS e ON e.numWFQueueID=q.numWFQueueID      
  LEFT JOIN dbo.OpportunityMaster o ON q.numRecordID =o.numOppId      
  JOIN DynamicFormMaster DFM ON m.numFormID=DFM.numFormID AND DFM.tintFlag=3    WHERE m.numDomainID='+ convert(varchar(15),@numDomainID)       
          
if @SortChar<>'0' set @strSql=@strSql + ' And m.vcWFName like '''+@SortChar+'%'''           
      
if @numFormID <> 0 set @strSql=@strSql + ' And m.numFormID = '+ convert(varchar(15),@numFormID) +''         
      
if @SearchStr<>'' set @strSql=@strSql + ' And (m.vcWFName like ''%'+@SearchStr+'%'' or       
m.vcWFDescription like ''%'+@SearchStr+'%'') '       
          
set  @strSql=@strSql +'ORDER BY ' + @columnName +' '+ @columnSortOrder          
     
insert into #tempTable( numWFID ,    
            numFormID ,    
            vcWFName ,    
            vcPOppName ,    
            vcWFDescription ,    
            numRecordID ,    
            tintProcessStatus ,    
            bitSuccess ,    
            vcDescription ,    
            vcFormName ,    
            dtExecutionDate ,    
            numOppId ,    
            STATUS ,    
            TriggeredOn,vcUserName) exec(@strSql)          
          
 declare @firstRec as integer                                                              
 declare @lastRec as integer                                                   
 set @firstRec= (@CurrentPage-1) * @PageSize                                                              
 set @lastRec= (@CurrentPage*@PageSize+1)                                                               
      
 SELECT  @TotRecs = COUNT(*)  FROM #tempTable       
       
  SELECT       
   ID AS RowNo,*    
   From #tempTable T        
   WHERE ID > @firstRec and ID < @lastRec  order by ID      
       
       
--   SELECT       
--   ROW_NUMBER() OVER(ORDER BY m.numWFID ASC) AS RowNo,    
--   m.numWFID,      
--   m.vcWFName,      
--   o.vcPOppName,      
--   m.vcWFDescription,      
--   q.numRecordID,      
--   q.tintProcessStatus,      
--   e.bitSuccess,      
--   e.vcDescription ,      
--   DFM.vcFormName,      
--   e.dtExecutionDate,    
--   o.numOppId,      
--   CASE WHEN q.tintProcessStatus=1 THEN 'Pending Execution' WHEN q.tintProcessStatus= 2 THEN 'In Progress' WHEN q.tintProcessStatus=3 THEN 'Success' WHEN q.tintProcessStatus=4 then 'Failed' else 'No WF Found' END AS STATUS,      
--   CASE WHEN (m.tintWFTriggerOn=1 AND m.intDays=0)THEN 'Create' WHEN m.tintWFTriggerOn=2 THEN 'Edit' WHEN m.tintWFTriggerOn=3 THEN 'Create or Edit' WHEN m.tintWFTriggerOn=4 THEN 'Fields Update' WHEN m.tintWFTriggerOn=5 THEN 'Delete' WHEN m.intDays>0 TH
  
--E--N     
--'Date Field' ELSE 'NA' end AS TriggeredOn      
--  FROM dbo.WorkFlowMaster as m       
--  INNER JOIN dbo.WorkFlowQueue AS q ON m.numWFID=q.numWFID       
--  LEFT JOIN dbo.WorkFlowQueueExecution AS e ON e.numWFQueueID=q.numWFQueueID      
--  INNER JOIN dbo.OpportunityMaster o ON q.numRecordID =o.numOppId      
--  JOIN DynamicFormMaster DFM ON m.numFormID=DFM.numFormID AND DFM.tintFlag=3  join #tempTable T on T.numWFID=m.numWFID      
--    
--   WHERE ID > @firstRec and ID < @lastRec AND m.numDomainID=@numDomainID order by ID      
         
   DROP TABLE #tempTable      
        
        
        
        
        
        
        
        
        
        
     
         
END 


