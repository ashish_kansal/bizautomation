/******************************************************************
Project: Release 7.2 Date: 03.APR.2017
Comments: ALTER SCRIPTS
*******************************************************************/

/************************** SANDEEP ************************/

ALTER TABLE Import_File_Master ADD tintItemLinkingID TINYINT


INSERT INTO DynamicFormMaster 
(numFormId,vcFormName,cCustomFieldsAssociated,cAOIAssociated,bitDeleted,tintFlag)
VALUES
(132,'Item Price Level','N','N',0,2)

-------------------------------------------------------------------------------

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitImport
)
VALUES
(
	4,193,132,0,0,'Model ID','TextBox',1,0,0,1
),
(
	4,203,132,0,0,'UPC','TextBox',1,0,0,1
),
(
	4,211,132,0,0,'Item ID','TextBox',1,0,0,1
),
(
	4,236,132,0,0,'UPC (M)','TextBox',1,0,0,1
),
(
	4,237,132,0,0,'SKU (M)','TextBox',1,0,0,1
),
(
	4,281,132,0,0,'SKU','TextBox',1,0,0,1
),
(
	4,470,132,0,0,'Price Rule Type','SelectBox',1,0,0,1
),
(
	4,471,132,0,0,'Price Level Discount Type','SelectBox',1,0,0,1
)



BEGIN TRY
	BEGIN TRANSACTION
		DECLARE @numFieldID NUMERIC(18,0)
		DECLARE @i AS INT = 1
		WHILE @i <= 20
		BEGIN

			INSERT INTO DycFieldMaster
			(
				numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitImport,bitAllowFiltering
			)
			VALUES
			(
				4,CONCAT('Price Level ',@i),CONCAT('monPriceLevel',@i),CONCAT('monPriceLevel',@i),CONCAT('PriceLevel',@i),'PricingTable','N','R','TextBox','',0,1,0,1,0,1,1,1,1,1,1
			)

			SET @numFieldID = SCOPE_IDENTITY();

			INSERT INTO DycFormField_Mapping
			(
				numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitImport
			)
			VALUES
			(
				4,@numFieldID,132,0,0,CONCAT('Price Level ',@i),'TextBox',1,0,0,1
			)

			SET @i = @i + 1;
		END
	COMMIT
END TRY
BEGIN CATCH
	ROLLBACK
END CATCH


BEGIN TRY
	BEGIN TRANSACTION

		DECLARE @numFieldID NUMERIC(18,0)
		DECLARE @i AS INT = 1
		WHILE @i <= 20
		BEGIN

			INSERT INTO DycFieldMaster
			(
				numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitImport,bitAllowFiltering
			)
			VALUES
			(
				4,CONCAT('Price Level ',@i,' Name'),CONCAT('vcPriceLevel',@i,'Name'),CONCAT('vcPriceLevel',@i,'Name'),CONCAT('PriceLevel',@i,'Name'),'PricingTable','V','R','TextBox','',0,1,0,1,0,1,1,1,1,1,1
			)

			SET @numFieldID = SCOPE_IDENTITY();

			INSERT INTO DycFormField_Mapping
			(
				numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitImport
			)
			VALUES
			(
				4,@numFieldID,132,0,0,CONCAT('Price Level ',@i,' Name'),'TextBox',1,0,0,1
			)

			SET @i = @i + 1;
		END
	COMMIT
END TRY
BEGIN CATCH
	ROLLBACK
END CATCH


BEGIN TRY
	BEGIN TRANSACTION

		DECLARE @numFieldID NUMERIC(18,0)
		DECLARE @i AS INT = 1
		WHILE @i <= 20
		BEGIN

			INSERT INTO DycFieldMaster
			(
				numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitImport,bitAllowFiltering
			)
			VALUES
			(
				4,CONCAT('Price Level ',@i,' Qty From'),CONCAT('intPriceLevel',@i,'FromQty'),CONCAT('intPriceLevel',@i,'FromQty'),CONCAT('PriceLevel',@i,'QtyFrom'),'PricingTable','N','R','TextBox','',0,1,0,1,0,1,1,1,1,1,1
			)

			SET @numFieldID = SCOPE_IDENTITY();

			INSERT INTO DycFormField_Mapping
			(
				numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitImport
			)
			VALUES
			(
				4,@numFieldID,132,0,0,CONCAT('PriceLevel ',@i,' Qty From'),'TextBox',1,0,0,1
			)

			INSERT INTO DycFieldMaster
			(
				numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitImport,bitAllowFiltering
			)
			VALUES
			(
				4,CONCAT('Price Level ',@i,' Qty To'),CONCAT('intPriceLevel',@i,'ToQty'),CONCAT('intPriceLevel',@i,'ToQty'),CONCAT('PriceLevel',@i,'QtyTo'),'PricingTable','N','R','TextBox','',0,1,0,1,0,1,1,1,1,1,1
			)

			SET @numFieldID = SCOPE_IDENTITY();

			INSERT INTO DycFormField_Mapping
			(
				numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitImport
			)
			VALUES
			(
				4,@numFieldID,132,0,0,CONCAT('PriceLevel ',@i,' Qty To'),'TextBox',1,0,0,1
			)

			SET @i = @i + 1;
		END

	COMMIT
END TRY
BEGIN CATCH
	ROLLBACK
END CATCH


UPDATE DycFormField_Mapping SET intSectionID=1 WHERE numFormID=132

INSERT INTO DycFormSectionDetail
(
	numFormID,intSectionID,vcSectionName,Loc_id
)
VALUES
(
	132,1,'Item Price Level Detail',0
)


ALTER TABLE Warehouses ADD numAddressID NUMERIC(18,0)


ALTER TABLE Domain ADD tintOppStautsForAutoPOBackOrder TINYINT
ALTER TABLE Domain ADD tintUnitsRecommendationForAutoPOBackOrder TINYINT
ALTER TABLE VendorShipmentMethod ADD bitPreferredMethod BIT
ALTER TABLE Vendor DROP COLUMN [intLeadTimeDays]
ALTER TABLE Vendor ADD vcNotes VARCHAR(300)


UPDATE Domain SET tintOppStautsForAutoPOBackOrder=0, tintUnitsRecommendationForAutoPOBackOrder=1


ALTER TABLE OpportunityMaster ADD numVendorAddressID NUMERIC(18,0)


/************************** PRASANT ************************/

INSERT INTO PageElementMaster
	(
		 [numElementID],
		 [vcElementName],
		 [vcUserControlPath],
		 vcTagName,
		 bitCustomization,
		 bitAdd,
		 bitDelete
	) 
	VALUES
	(
		53,
		'SerialLotReturn',
		'~/UserControls/SerialLotReturn.ascx',
		'{#SerialLotReturn#}',
		1,
		0,
		0
	)

	INSERT INTO PageElementAttributes
	(
		numElementID, 
		vcAttributeName, 
		vcControlType, 
		vcControlValues, 
		bitEditor
	)
	VALUES
	(
		53,
		'Html Customize',
		'HtmlEditor',
		NULL,
		1
	)

-----------------------------------
INSERT INTO PageElementMaster
	(
		 [numElementID],
		 [vcElementName],
		 [vcUserControlPath],
		 vcTagName,
		 bitCustomization,
		 bitAdd,
		 bitDelete
	) 
	VALUES
	(
		52,
		'ItemReturn',
		'~/UserControls/ItemReturn.ascx',
		'{#ItemReturn#}',
		1,
		0,
		0
	)

	INSERT INTO PageElementAttributes
	(
		numElementID, 
		vcAttributeName, 
		vcControlType, 
		vcControlValues, 
		bitEditor
	)
	VALUES
	(
		52,
		'Html Customize',
		'HtmlEditor',
		NULL,
		1
	)
------------------------------------------------------
INSERT INTO PageElementMaster
	(
		 [numElementID],
		 [vcElementName],
		 [vcUserControlPath],
		 vcTagName,
		 bitCustomization,
		 bitAdd,
		 bitDelete
	) 
	VALUES
	(
		51,
		'OpenRMA',
		'~/UserControls/OpenRMA.ascx',
		'{#OpenRMA#}',
		1,
		0,
		0
	)

	INSERT INTO PageElementAttributes
	(
		numElementID, 
		vcAttributeName, 
		vcControlType, 
		vcControlValues, 
		bitEditor
	)
	VALUES
	(
		51,
		'Html Customize',
		'HtmlEditor',
		NULL,
		1
	)

--------------------------------------
INSERT INTO PageElementMaster
	(
		 [numElementID],
		 [vcElementName],
		 [vcUserControlPath],
		 vcTagName,
		 bitCustomization,
		 bitAdd,
		 bitDelete
	) 
	VALUES
	(
		50,
		'OpenCases',
		'~/UserControls/OpenCases.ascx',
		'{#OpenCases#}',
		1,
		0,
		0
	)

	INSERT INTO PageElementAttributes
	(
		numElementID, 
		vcAttributeName, 
		vcControlType, 
		vcControlValues, 
		bitEditor
	)
	VALUES
	(
		50,
		'Html Customize',
		'HtmlEditor',
		NULL,
		1
	)
------------------------
INSERT INTO PageElementMaster
	(
		 [numElementID],
		 [vcElementName],
		 [vcUserControlPath],
		 vcTagName,
		 bitCustomization,
		 bitAdd,
		 bitDelete
	) 
	VALUES
	(
		49,
		'ItemPurchasedHistory',
		'~/UserControls/ItemPurchasedHistory.ascx',
		'{#ItemPurchasedHistory#}',
		1,
		0,
		0
	)

	INSERT INTO PageElementAttributes
	(
		numElementID, 
		vcAttributeName, 
		vcControlType, 
		vcControlValues, 
		bitEditor
	)
	VALUES
	(
		49,
		'Html Customize',
		'HtmlEditor',
		NULL,
		1
	)

------------------------------

INSERT INTO PageElementMaster
	(
		 [numElementID],
		 [vcElementName],
		 [vcUserControlPath],
		 vcTagName,
		 bitCustomization,
		 bitAdd,
		 bitDelete
	) 
	VALUES
	(
		48,
		'SalesOpportunity',
		'~/UserControls/SalesOpportunity.ascx',
		'{#SalesOpportunity#}',
		1,
		0,
		0
	)

	INSERT INTO PageElementAttributes
	(
		numElementID, 
		vcAttributeName, 
		vcControlType, 
		vcControlValues, 
		bitEditor
	)
	VALUES
	(
		48,
		'Html Customize',
		'HtmlEditor',
		NULL,
		1
	)

------------------------------------------------
INSERT INTO PageElementMaster
	(
		 [numElementID],
		 [vcElementName],
		 [vcUserControlPath],
		 vcTagName,
		 bitCustomization,
		 bitAdd,
		 bitDelete
	) 
	VALUES
	(
		47,
		'customerstatement',
		'~/UserControls/customerstatement.ascx',
		'{#customerstatement#}',
		1,
		0,
		0
	)

	INSERT INTO PageElementAttributes
	(
		numElementID, 
		vcAttributeName, 
		vcControlType, 
		vcControlValues, 
		bitEditor
	)
	VALUES
	(
		47,
		'Html Customize',
		'HtmlEditor',
		NULL,
		1
	)


ALTER TABLE Domain ADD vcSalesOrderTabs VARCHAR(300) DEFAULT 'Sales Orders'
ALTER TABLE Domain ADD vcSalesQuotesTabs VARCHAR(300) DEFAULT 'Sales Quotes'
ALTER TABLE Domain ADD vcItemPurchaseHistoryTabs VARCHAR(300) DEFAULT 'Item Purchase History'
ALTER TABLE Domain ADD vcItemsFrequentlyPurchasedTabs VARCHAR(300) DEFAULT 'Items Frequently Purchased'
ALTER TABLE Domain ADD vcOpenCasesTabs VARCHAR(300) DEFAULT 'Open Cases'
ALTER TABLE Domain ADD vcOpenRMATabs VARCHAR(300) DEFAULT 'Open RMAs'
ALTER TABLE Domain ADD bitSalesOrderTabs BIT DEFAULT 0
ALTER TABLE Domain ADD bitSalesQuotesTabs BIT DEFAULT 0
ALTER TABLE Domain ADD bitItemPurchaseHistoryTabs BIT DEFAULT 0
ALTER TABLE Domain ADD bitItemsFrequentlyPurchasedTabs BIT DEFAULT 0
ALTER TABLE Domain ADD bitOpenCasesTabs BIT DEFAULT 0
ALTER TABLE Domain ADD bitOpenRMATabs BIT DEFAULT 0

UPDATE 
	Domain 
SET 
	vcSalesOrderTabs='Sales Orders',
	vcSalesQuotesTabs='Sales Quotes',
	vcItemPurchaseHistoryTabs='Item Purchase History',
	vcItemsFrequentlyPurchasedTabs= 'Items Frequently Purchased',
	vcOpenCasesTabs='Open Cases',
	vcOpenRMATabs='Open RMAs',
	bitSalesOrderTabs=0,
	bitSalesQuotesTabs=0,
	bitItemPurchaseHistoryTabs=0,
	bitItemsFrequentlyPurchasedTabs=0,
	bitOpenCasesTabs=0,
	bitOpenRMATabs=0




