/******************************************************************
Project: Release 4.3 Date: 09.MARCH.2015
Comments: 
*******************************************************************/


---------------  Sandeep  ----------------------


INSERT INTO DycFormField_Mapping 
(numModuleID,numFieldID,numFormID,vcFieldName,bitAllowEdit,bitInlineEdit,vcAssociatedControlType,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering)
VALUES
(4,280,21,'Base UOM',1,1,'SelectBox',0,0,1,1,1),
(4,276,21,'Sale UOM',1,1,'SelectBox',0,0,1,1,1),
(4,275,21,'Purchase UOM',1,1,'SelectBox',0,0,1,1,1)

ALTER TABLE OpportunityMaster ADD
bitStopMerge BIT

ALTER TABLE tblActionItemData ADD
bitRemind BIT,
numRemindBeforeMinutes INT

ALTER TABLE Communication ADD
LastSnoozDateTimeUtc DATETIME

INSERT INTO PageMaster VALUES (34,10,'frmDealList.aspx','Merge Purchase Orders',1,0,0,0,0,NULL,NULL)

UPDATE ShortCutBar SET NewLink='../Admin/actionitemdetails.aspx' WHERE id=163



--------------- Manish ---------------------------


------/******************************************************************
------Project: BACRMUI   Date: 16.FEB.2015
------Comments: Adding new Bizdoc "Customer Statement"

------*******************************************************************/
BEGIN TRANSACTION

------/******************************************************************
------Project: BACRMUI   Date: 05.MAR.2015
------Comments: Adding new field for default shipping service for an account

------*******************************************************************/
BEGIN TRANSACTION

ALTER TABLE [dbo].[DivisionMaster] ADD numDefaultShippingServiceID NUMERIC(18,0)

ROLLBACK

----------------------------------DO NOT UPDATE THIS QUERY ON PRODUCTION. THIS IS ALREADY EXECUTED --------------------
DECLARE @MaxListItemID AS NUMERIC(18,0)
SELECT @MaxListItemID =  40911 --MAX([LD].[numListItemID]) + 1 FROM [dbo].[ListDetails] AS LD --WHERE [LD].[numListID] = 27
PRINT @MaxListItemID

SET IDENTITY_INSERT [ListDetails] ON

 INSERT INTO [dbo].[ListDetails]
         ( [numListItemID],
		   [numListID] ,
           [vcData] ,
           [numCreatedBY] ,
           [bintCreatedDate] ,
           [numModifiedBy] ,
           [bintModifiedDate] ,
           [bitDelete] ,
           [numDomainID] ,
           [constFlag] ,
           [sintOrder] ,
           [numListType]
         )
 VALUES  ( @MaxListItemID,
		   27 , -- numListID - numeric
           'Customer Statement' , -- vcData - varchar(50)
           1 , -- numCreatedBY - numeric
           GETDATE() , -- bintCreatedDate - datetime
           1 , -- numModifiedBy - numeric
           GETDATE() , -- bintModifiedDate - datetime
           0 , -- bitDelete - bit
           1 , -- numDomainID - numeric
           1 , -- constFlag - bit
           19 , -- sintOrder - smallint
           NULL  -- numListType - numeric
         )

SET IDENTITY_INSERT [ListDetails] OFF

----------------------------------DO NOT UPDATE THIS QUERY ON PRODUCTION. THIS IS ALREADY EXECUTED --------------------


---------------- Add Default Customer Statement Template for all domain ---------------------------------------

BEGIN TRANSACTION
DECLARE @numDomainID AS NUMERIC(18)
SELECT  ROW_NUMBER() OVER ( ORDER BY numDomainID ) AS [RowID] ,
        numDomainID
INTO    #tempDomains
FROM    dbo.Domain
WHERE   numDomainId > 0 --AND [Domain].[numDomainId] <> 1
DECLARE @intCnt AS INT
DECLARE @intCntDomain AS INT
SET @intCnt = 0
SET @intCntDomain = ( SELECT    COUNT(*)
                      FROM      #tempDomains
                    )

IF @intCntDomain > 0
    BEGIN
        WHILE ( @intCnt < @intCntDomain )
            BEGIN

                SET @intCnt = @intCnt + 1
                SELECT  @numDomainID = numDomainID
                FROM    #tempDomains
                WHERE   RowID = @intCnt
                PRINT @numDomainID

				--SELECT * FROM #tempDomains
                INSERT  INTO [dbo].[BizDocTemplate]
                        ( [numDomainID] ,
                          [numBizDocID] ,
                          [numOppType] ,
                          [txtBizDocTemplate] ,
                          [txtCSS] ,
                          [bitEnabled] ,
                          [tintTemplateType] ,
                          [vcTemplateName] ,
                          [bitDefault] ,
                          [txtNewBizDocTemplate] ,
                          [vcBizDocImagePath] ,
                          [vcBizDocFooter] ,
                          [vcPurBizDocFooter] ,
                          [bitKeepFooterBottom] ,
                          [numOrientation]
                        )
                VALUES  ( @numDomainID , -- numDomainID - numeric
                          40911 , -- numBizDocID - numeric
                          1 , -- numOppType - numeric
                          '&lt;table width=&quot;100%&quot; border=&quot;0&quot; cellpadding=&quot;0&quot;&gt;
    &lt;tbody&gt;
        &lt;tr&gt;
            &lt;td valign=&quot;top&quot; align=&quot;left&quot;&gt;#Logo#&lt;br /&gt;
            &lt;/td&gt;
            &lt;td align=&quot;right&quot; style=&quot;white-space: nowrap;&quot;&gt;&lt;/td&gt;
            &lt;td align=&quot;right&quot; class=&quot;title&quot; style=&quot;white-space: nowrap;&quot;&gt;#BizDocTemplateName#
            &lt;/td&gt;
        &lt;/tr&gt;
    &lt;/tbody&gt;
&lt;/table&gt;
&lt;br /&gt;
&lt;table width=&quot;100%&quot; border=&quot;0&quot; cellpadding=&quot;0&quot;&gt;
    &lt;tbody&gt;
        &lt;tr&gt;
            &lt;td align=&quot;right&quot; style=&quot;text-align: left;&quot;&gt;&lt;strong&gt;Bill To:&lt;/strong&gt;&lt;br /&gt;
            &lt;/td&gt;
            &lt;td align=&quot;right&quot; style=&quot;text-align: center;&quot;&gt;&lt;strong&gt;Statement Date:&lt;/strong&gt;&amp;nbsp;#StatementDate#&lt;br /&gt;
            &lt;/td&gt;
        &lt;/tr&gt;
        &lt;tr&gt;
            &lt;td align=&quot;right&quot; style=&quot;text-align: left;&quot;&gt;#Customer/VendorOrganizationName#&lt;br /&gt;
            #Customer/VendorBillToStreet#&lt;br /&gt;
            #Customer/VendorBillToCity#&lt;br /&gt;
            #Customer/VendorBillToState#&amp;nbsp;#Customer/VendorBillToPostal#&lt;br /&gt;
            #Customer/VendorBillToCountry#&lt;br /&gt;
            &lt;br /&gt;
            &lt;/td&gt;
            &lt;td align=&quot;right&quot; style=&quot;text-align: center;&quot;&gt;&lt;span style=&quot;font-size: 24px;&quot;&gt;&lt;strong&gt;Balance Due:&lt;/strong&gt;&amp;nbsp;&amp;nbsp;#BalanceDue#&lt;/span&gt;&lt;/td&gt;
        &lt;/tr&gt;
    &lt;/tbody&gt;
&lt;/table&gt;
&lt;table width=&quot;75%&quot; border=&quot;0&quot; cellpadding=&quot;0&quot;&gt;
    &lt;tbody&gt;
        &lt;tr&gt;
            &lt;td colspan=&quot;2&quot; style=&quot;text-align: center;&quot;&gt;&lt;strong&gt;&lt;span style=&quot;color: #00b050;&quot;&gt;Current Amt Due&lt;/span&gt;&lt;/strong&gt;&lt;/td&gt;
            &lt;td colspan=&quot;2&quot; style=&quot;text-align: center;&quot;&gt;&lt;strong&gt;&lt;span style=&quot;color: #ff0000;&quot;&gt;Past Due&lt;/span&gt;&lt;/strong&gt;&lt;/td&gt;
        &lt;/tr&gt;
        &lt;tr&gt;
            &lt;td style=&quot;text-align: right;&quot;&gt;Today-30:&lt;/td&gt;
            &lt;td&gt;#Today-30(CurrentAmtDue)#&lt;br /&gt;
            &lt;/td&gt;
            &lt;td style=&quot;text-align: right;&quot;&gt;Today-30:&lt;/td&gt;
            &lt;td&gt;#Today-30(PastDue)#&lt;/td&gt;
        &lt;/tr&gt;
        &lt;tr&gt;
            &lt;td style=&quot;text-align: right;&quot;&gt;31-60:&lt;/td&gt;
            &lt;td&gt;#31-60(CurrentAmtDue)#&lt;br /&gt;
            &lt;/td&gt;
            &lt;td style=&quot;text-align: right;&quot;&gt;31-60:&lt;/td&gt;
            &lt;td&gt;#61-90(PastDue)#&lt;br /&gt;
            &lt;/td&gt;
        &lt;/tr&gt;
        &lt;tr&gt;
            &lt;td style=&quot;text-align: right;&quot;&gt;61-90:&lt;/td&gt;
            &lt;td&gt;#61-90(CurrentAmtDue)#&lt;br /&gt;
            &lt;/td&gt;
            &lt;td style=&quot;text-align: right;&quot;&gt;61-90:&lt;/td&gt;
            &lt;td&gt;#61-90(PastDue)#&lt;br /&gt;
            &lt;/td&gt;
        &lt;/tr&gt;
        &lt;tr&gt;
            &lt;td style=&quot;text-align: right;&quot;&gt;Over 91:&lt;/td&gt;
            &lt;td&gt;#Over91(CurrentAmtDue)#&lt;br /&gt;
            &lt;/td&gt;
            &lt;td style=&quot;text-align: right;&quot;&gt;Over 91:&lt;/td&gt;
            &lt;td&gt;#Over91(PastDue)#&lt;br /&gt;
            &lt;/td&gt;
        &lt;/tr&gt;
    &lt;/tbody&gt;
&lt;/table&gt;
&lt;br /&gt;
&lt;table width=&quot;100%&quot; border=&quot;0&quot; cellpadding=&quot;0&quot;&gt;
    &lt;tbody&gt;
        &lt;tr&gt;
            &lt;td style=&quot;text-align: center;&quot;&gt;#Products#
            &lt;/td&gt;
        &lt;/tr&gt;
    &lt;/tbody&gt;
&lt;/table&gt;' , -- txtBizDocTemplate - text
                          '.title{color: #696969; /*padding:10px;*/font: bold 30px Arial, Helvetica, sans-serif;}
			.RowHeader{background-color: #dedede;color: #333;font-family: Arial;font-size: 8pt;}
			.RowHeader.hyperlink{color: #333;}
			.ItemHeader, .ItemStyle, .AltItemStyle{border-width: 1px;padding: 8px;font: normal 12px/17px arial;border-style: solid;border-color: #666666;background-color: #dedede;}
			.ItemHeader{border-width: 1px;padding: 8px;border-style: solid;border-color: #666666;}
			.ItemStyle td{border-width: 1px;padding: 8px;border-style: solid;border-color: #666666;background-color: #ffffff;}
			.AltItemStyle{background-color: White;border-color: black;padding: 8px;}
			.AltItemStyle td{padding: 8px;}
			.ItemHeader td{border-width: 1px;padding: 8px;font-weight: bold;border-style: solid;border-color: #666666;background-color: #dedede;}
			.ItemHeader td th{border-width: 1px;padding: 8px;border-style: solid;border-color: #666666;background-color: #dedede;}
			#tblBizDocSumm{font: normal 12px verdana;color: #333;border-width: 1px;border-color: #666666;border-collapse: collapse;background-color: #dedede;}
			#tblBizDocSumm td{border-width: 1px;padding: 8px;border-style: solid;border-color: #666666;}
			.WordWrapSerialNo{width: 30%;word-break: break-all;}' , -- txtCSS - text
                          1 , -- bitEnabled - bit
                          0 , -- tintTemplateType - tinyint
                          'Customer Statement' , -- vcTemplateName - varchar(50)
                          1 , -- bitDefault - bit
                          '&lt;table width=&quot;100%&quot; border=&quot;0&quot; cellpadding=&quot;0&quot;&gt;
    &lt;tbody&gt;
        &lt;tr&gt;
            &lt;td valign=&quot;top&quot; align=&quot;left&quot;&gt;#Logo#&lt;br /&gt;
            &lt;/td&gt;
            &lt;td align=&quot;right&quot; style=&quot;white-space: nowrap;&quot;&gt;&lt;/td&gt;
            &lt;td align=&quot;right&quot; class=&quot;title&quot; style=&quot;white-space: nowrap;&quot;&gt;#BizDocTemplateName#
            &lt;/td&gt;
        &lt;/tr&gt;
    &lt;/tbody&gt;
&lt;/table&gt;
&lt;br /&gt;
&lt;table width=&quot;100%&quot; border=&quot;0&quot; cellpadding=&quot;0&quot;&gt;
    &lt;tbody&gt;
        &lt;tr&gt;
            &lt;td align=&quot;right&quot; style=&quot;text-align: left;&quot;&gt;&lt;strong&gt;Bill To:&lt;/strong&gt;&lt;br /&gt;
            &lt;/td&gt;
            &lt;td align=&quot;right&quot; style=&quot;text-align: center;&quot;&gt;&lt;strong&gt;Statement Date:&lt;/strong&gt;&amp;nbsp;#StatementDate#&lt;br /&gt;
            &lt;/td&gt;
        &lt;/tr&gt;
        &lt;tr&gt;
            &lt;td align=&quot;right&quot; style=&quot;text-align: left;&quot;&gt;#Customer/VendorOrganizationName#&lt;br /&gt;
            #Customer/VendorBillToStreet#&lt;br /&gt;
            #Customer/VendorBillToCity#&lt;br /&gt;
            #Customer/VendorBillToState#&amp;nbsp;#Customer/VendorBillToPostal#&lt;br /&gt;
            #Customer/VendorBillToCountry#&lt;br /&gt;
            &lt;br /&gt;
            &lt;/td&gt;
            &lt;td align=&quot;right&quot; style=&quot;text-align: center;&quot;&gt;&lt;span style=&quot;font-size: 24px;&quot;&gt;&lt;strong&gt;Balance Due:&lt;/strong&gt;&amp;nbsp;&amp;nbsp;#BalanceDue#&lt;/span&gt;&lt;/td&gt;
        &lt;/tr&gt;
    &lt;/tbody&gt;
&lt;/table&gt;
&lt;table width=&quot;75%&quot; border=&quot;0&quot; cellpadding=&quot;0&quot;&gt;
    &lt;tbody&gt;
        &lt;tr&gt;
            &lt;td colspan=&quot;2&quot; style=&quot;text-align: center;&quot;&gt;&lt;strong&gt;&lt;span style=&quot;color: #00b050;&quot;&gt;Current Amt Due&lt;/span&gt;&lt;/strong&gt;&lt;/td&gt;
            &lt;td colspan=&quot;2&quot; style=&quot;text-align: center;&quot;&gt;&lt;strong&gt;&lt;span style=&quot;color: #ff0000;&quot;&gt;Past Due&lt;/span&gt;&lt;/strong&gt;&lt;/td&gt;
        &lt;/tr&gt;
        &lt;tr&gt;
            &lt;td style=&quot;text-align: right;&quot;&gt;Today-30:&lt;/td&gt;
            &lt;td&gt;#Today-30(CurrentAmtDue)#&lt;br /&gt;
            &lt;/td&gt;
            &lt;td style=&quot;text-align: right;&quot;&gt;Today-30:&lt;/td&gt;
            &lt;td&gt;#Today-30(PastDue)#&lt;/td&gt;
        &lt;/tr&gt;
        &lt;tr&gt;
            &lt;td style=&quot;text-align: right;&quot;&gt;31-60:&lt;/td&gt;
            &lt;td&gt;#31-60(CurrentAmtDue)#&lt;br /&gt;
            &lt;/td&gt;
            &lt;td style=&quot;text-align: right;&quot;&gt;31-60:&lt;/td&gt;
            &lt;td&gt;#61-90(PastDue)#&lt;br /&gt;
            &lt;/td&gt;
        &lt;/tr&gt;
        &lt;tr&gt;
            &lt;td style=&quot;text-align: right;&quot;&gt;61-90:&lt;/td&gt;
            &lt;td&gt;#61-90(CurrentAmtDue)#&lt;br /&gt;
            &lt;/td&gt;
            &lt;td style=&quot;text-align: right;&quot;&gt;61-90:&lt;/td&gt;
            &lt;td&gt;#61-90(PastDue)#&lt;br /&gt;
            &lt;/td&gt;
        &lt;/tr&gt;
        &lt;tr&gt;
            &lt;td style=&quot;text-align: right;&quot;&gt;Over 91:&lt;/td&gt;
            &lt;td&gt;#Over91(CurrentAmtDue)#&lt;br /&gt;
            &lt;/td&gt;
            &lt;td style=&quot;text-align: right;&quot;&gt;Over 91:&lt;/td&gt;
            &lt;td&gt;#Over91(PastDue)#&lt;br /&gt;
            &lt;/td&gt;
        &lt;/tr&gt;
    &lt;/tbody&gt;
&lt;/table&gt;
&lt;br /&gt;
&lt;table width=&quot;100%&quot; border=&quot;0&quot; cellpadding=&quot;0&quot;&gt;
    &lt;tbody&gt;
        &lt;tr&gt;
            &lt;td style=&quot;text-align: center;&quot;&gt;#Products#
            &lt;/td&gt;
        &lt;/tr&gt;
    &lt;/tbody&gt;
&lt;/table&gt;' , -- txtNewBizDocTemplate - text
                          NULL , -- vcBizDocImagePath - varchar(100)
                          NULL , -- vcBizDocFooter - varchar(100)
                          NULL , -- vcPurBizDocFooter - varchar(100)
                          0 , -- bitKeepFooterBottom - bit
                          1  -- numOrientation - int
                        )

            END
    END
DROP TABLE #tempDomains


--INSERT INTO [dbo].[BizDocTemplate]
--( [numDomainID] ,[numBizDocID] ,[numOppType] ,[txtBizDocTemplate] ,[txtCSS] ,[bitEnabled] ,[tintTemplateType] ,[vcTemplateName] ,[bitDefault] ,[txtNewBizDocTemplate] ,[vcBizDocImagePath] ,
-- [vcBizDocFooter] ,[vcPurBizDocFooter] ,[bitKeepFooterBottom] ,[numOrientation]
--        )
--VALUES  ( 1 , -- numDomainID - numeric
--          40911 , -- numBizDocID - numeric
--          1 , -- numOppType - numeric
--         '&lt;table width=&quot;100%&quot; border=&quot;0&quot; cellpadding=&quot;0&quot;&gt;
--    &lt;tbody&gt;
--        &lt;tr&gt;
--            &lt;td valign=&quot;top&quot; align=&quot;left&quot;&gt;#Logo#&lt;br /&gt;
--            &lt;/td&gt;
--            &lt;td align=&quot;right&quot; style=&quot;white-space: nowrap;&quot;&gt;&lt;/td&gt;
--            &lt;td align=&quot;right&quot; class=&quot;title&quot; style=&quot;white-space: nowrap;&quot;&gt;#BizDocTemplateName#
--            &lt;/td&gt;
--        &lt;/tr&gt;
--    &lt;/tbody&gt;
--&lt;/table&gt;
--&lt;br /&gt;
--&lt;table width=&quot;100%&quot; border=&quot;0&quot; cellpadding=&quot;0&quot;&gt;
--    &lt;tbody&gt;
--        &lt;tr&gt;
--            &lt;td align=&quot;right&quot; style=&quot;text-align: left;&quot;&gt;&lt;strong&gt;Bill To:&lt;/strong&gt;&lt;br /&gt;
--            &lt;/td&gt;
--            &lt;td align=&quot;right&quot; style=&quot;text-align: center;&quot;&gt;&lt;strong&gt;Statement Date:&lt;/strong&gt;&amp;nbsp;#StatementDate#&lt;br /&gt;
--            &lt;/td&gt;
--        &lt;/tr&gt;
--        &lt;tr&gt;
--            &lt;td align=&quot;right&quot; style=&quot;text-align: left;&quot;&gt;#Customer/VendorOrganizationName#&lt;br /&gt;
--            #Customer/VendorBillToStreet#&lt;br /&gt;
--            #Customer/VendorBillToCity#&lt;br /&gt;
--            #Customer/VendorBillToState#&amp;nbsp;#Customer/VendorBillToPostal#&lt;br /&gt;
--            #Customer/VendorBillToCountry#&lt;br /&gt;
--            &lt;br /&gt;
--            &lt;/td&gt;
--            &lt;td align=&quot;right&quot; style=&quot;text-align: center;&quot;&gt;&lt;span style=&quot;font-size: 24px;&quot;&gt;&lt;strong&gt;Balance Due:&lt;/strong&gt;&amp;nbsp;&amp;nbsp;#BalanceDue#&lt;/span&gt;&lt;/td&gt;
--        &lt;/tr&gt;
--    &lt;/tbody&gt;
--&lt;/table&gt;
--&lt;table width=&quot;75%&quot; border=&quot;0&quot; cellpadding=&quot;0&quot;&gt;
--    &lt;tbody&gt;
--        &lt;tr&gt;
--            &lt;td colspan=&quot;2&quot; style=&quot;text-align: center;&quot;&gt;&lt;strong&gt;&lt;span style=&quot;color: #00b050;&quot;&gt;Current Amt Due&lt;/span&gt;&lt;/strong&gt;&lt;/td&gt;
--            &lt;td colspan=&quot;2&quot; style=&quot;text-align: center;&quot;&gt;&lt;strong&gt;&lt;span style=&quot;color: #ff0000;&quot;&gt;Past Due&lt;/span&gt;&lt;/strong&gt;&lt;/td&gt;
--        &lt;/tr&gt;
--        &lt;tr&gt;
--            &lt;td style=&quot;text-align: right;&quot;&gt;Today-30:&lt;/td&gt;
--            &lt;td&gt;#Today-30(CurrentAmtDue)#&lt;br /&gt;
--            &lt;/td&gt;
--            &lt;td style=&quot;text-align: right;&quot;&gt;Today-30:&lt;/td&gt;
--            &lt;td&gt;#Today-30(PastDue)#&lt;/td&gt;
--        &lt;/tr&gt;
--        &lt;tr&gt;
--            &lt;td style=&quot;text-align: right;&quot;&gt;31-60:&lt;/td&gt;
--            &lt;td&gt;#31-60(CurrentAmtDue)#&lt;br /&gt;
--            &lt;/td&gt;
--            &lt;td style=&quot;text-align: right;&quot;&gt;31-60:&lt;/td&gt;
--            &lt;td&gt;#61-90(PastDue)#&lt;br /&gt;
--            &lt;/td&gt;
--        &lt;/tr&gt;
--        &lt;tr&gt;
--            &lt;td style=&quot;text-align: right;&quot;&gt;61-90:&lt;/td&gt;
--            &lt;td&gt;#61-90(CurrentAmtDue)#&lt;br /&gt;
--            &lt;/td&gt;
--            &lt;td style=&quot;text-align: right;&quot;&gt;61-90:&lt;/td&gt;
--            &lt;td&gt;#61-90(PastDue)#&lt;br /&gt;
--            &lt;/td&gt;
--        &lt;/tr&gt;
--        &lt;tr&gt;
--            &lt;td style=&quot;text-align: right;&quot;&gt;Over 91:&lt;/td&gt;
--            &lt;td&gt;#Over91(CurrentAmtDue)#&lt;br /&gt;
--            &lt;/td&gt;
--            &lt;td style=&quot;text-align: right;&quot;&gt;Over 91:&lt;/td&gt;
--            &lt;td&gt;#Over91(PastDue)#&lt;br /&gt;
--            &lt;/td&gt;
--        &lt;/tr&gt;
--    &lt;/tbody&gt;
--&lt;/table&gt;
--&lt;br /&gt;
--&lt;table width=&quot;100%&quot; border=&quot;0&quot; cellpadding=&quot;0&quot;&gt;
--    &lt;tbody&gt;
--        &lt;tr&gt;
--            &lt;td style=&quot;text-align: center;&quot;&gt;#Products#
--            &lt;/td&gt;
--        &lt;/tr&gt;
--    &lt;/tbody&gt;
--&lt;/table&gt;', -- txtBizDocTemplate - text
--          '.title{color: #696969; /*padding:10px;*/font: bold 30px Arial, Helvetica, sans-serif;}
--			.RowHeader{background-color: #dedede;color: #333;font-family: Arial;font-size: 8pt;}
--			.RowHeader.hyperlink{color: #333;}
--			.ItemHeader, .ItemStyle, .AltItemStyle{border-width: 1px;padding: 8px;font: normal 12px/17px arial;border-style: solid;border-color: #666666;background-color: #dedede;}
--			.ItemHeader{border-width: 1px;padding: 8px;border-style: solid;border-color: #666666;}
--			.ItemStyle td{border-width: 1px;padding: 8px;border-style: solid;border-color: #666666;background-color: #ffffff;}
--			.AltItemStyle{background-color: White;border-color: black;padding: 8px;}
--			.AltItemStyle td{padding: 8px;}
--			.ItemHeader td{border-width: 1px;padding: 8px;font-weight: bold;border-style: solid;border-color: #666666;background-color: #dedede;}
--			.ItemHeader td th{border-width: 1px;padding: 8px;border-style: solid;border-color: #666666;background-color: #dedede;}
--			#tblBizDocSumm{font: normal 12px verdana;color: #333;border-width: 1px;border-color: #666666;border-collapse: collapse;background-color: #dedede;}
--			#tblBizDocSumm td{border-width: 1px;padding: 8px;border-style: solid;border-color: #666666;}
--			.WordWrapSerialNo{width: 30%;word-break: break-all;}' , -- txtCSS - text
--          1 , -- bitEnabled - bit
--          0 , -- tintTemplateType - tinyint
--          'Customer Statement' , -- vcTemplateName - varchar(50)
--          1 , -- bitDefault - bit
--          '&lt;table width=&quot;100%&quot; border=&quot;0&quot; cellpadding=&quot;0&quot;&gt;
--    &lt;tbody&gt;
--        &lt;tr&gt;
--            &lt;td valign=&quot;top&quot; align=&quot;left&quot;&gt;#Logo#&lt;br /&gt;
--            &lt;/td&gt;
--            &lt;td align=&quot;right&quot; style=&quot;white-space: nowrap;&quot;&gt;&lt;/td&gt;
--            &lt;td align=&quot;right&quot; class=&quot;title&quot; style=&quot;white-space: nowrap;&quot;&gt;#BizDocTemplateName#
--            &lt;/td&gt;
--        &lt;/tr&gt;
--    &lt;/tbody&gt;
--&lt;/table&gt;
--&lt;br /&gt;
--&lt;table width=&quot;100%&quot; border=&quot;0&quot; cellpadding=&quot;0&quot;&gt;
--    &lt;tbody&gt;
--        &lt;tr&gt;
--            &lt;td align=&quot;right&quot; style=&quot;text-align: left;&quot;&gt;&lt;strong&gt;Bill To:&lt;/strong&gt;&lt;br /&gt;
--            &lt;/td&gt;
--            &lt;td align=&quot;right&quot; style=&quot;text-align: center;&quot;&gt;&lt;strong&gt;Statement Date:&lt;/strong&gt;&amp;nbsp;#StatementDate#&lt;br /&gt;
--            &lt;/td&gt;
--        &lt;/tr&gt;
--        &lt;tr&gt;
--            &lt;td align=&quot;right&quot; style=&quot;text-align: left;&quot;&gt;#Customer/VendorOrganizationName#&lt;br /&gt;
--            #Customer/VendorBillToStreet#&lt;br /&gt;
--            #Customer/VendorBillToCity#&lt;br /&gt;
--            #Customer/VendorBillToState#&amp;nbsp;#Customer/VendorBillToPostal#&lt;br /&gt;
--            #Customer/VendorBillToCountry#&lt;br /&gt;
--            &lt;br /&gt;
--            &lt;/td&gt;
--            &lt;td align=&quot;right&quot; style=&quot;text-align: center;&quot;&gt;&lt;span style=&quot;font-size: 24px;&quot;&gt;&lt;strong&gt;Balance Due:&lt;/strong&gt;&amp;nbsp;&amp;nbsp;#BalanceDue#&lt;/span&gt;&lt;/td&gt;
--        &lt;/tr&gt;
--    &lt;/tbody&gt;
--&lt;/table&gt;
--&lt;table width=&quot;75%&quot; border=&quot;0&quot; cellpadding=&quot;0&quot;&gt;
--    &lt;tbody&gt;
--        &lt;tr&gt;
--            &lt;td colspan=&quot;2&quot; style=&quot;text-align: center;&quot;&gt;&lt;strong&gt;&lt;span style=&quot;color: #00b050;&quot;&gt;Current Amt Due&lt;/span&gt;&lt;/strong&gt;&lt;/td&gt;
--            &lt;td colspan=&quot;2&quot; style=&quot;text-align: center;&quot;&gt;&lt;strong&gt;&lt;span style=&quot;color: #ff0000;&quot;&gt;Past Due&lt;/span&gt;&lt;/strong&gt;&lt;/td&gt;
--        &lt;/tr&gt;
--        &lt;tr&gt;
--            &lt;td style=&quot;text-align: right;&quot;&gt;Today-30:&lt;/td&gt;
--            &lt;td&gt;#Today-30(CurrentAmtDue)#&lt;br /&gt;
--            &lt;/td&gt;
--            &lt;td style=&quot;text-align: right;&quot;&gt;Today-30:&lt;/td&gt;
--            &lt;td&gt;#Today-30(PastDue)#&lt;/td&gt;
--        &lt;/tr&gt;
--        &lt;tr&gt;
--            &lt;td style=&quot;text-align: right;&quot;&gt;31-60:&lt;/td&gt;
--            &lt;td&gt;#31-60(CurrentAmtDue)#&lt;br /&gt;
--            &lt;/td&gt;
--            &lt;td style=&quot;text-align: right;&quot;&gt;31-60:&lt;/td&gt;
--            &lt;td&gt;#61-90(PastDue)#&lt;br /&gt;
--            &lt;/td&gt;
--        &lt;/tr&gt;
--        &lt;tr&gt;
--            &lt;td style=&quot;text-align: right;&quot;&gt;61-90:&lt;/td&gt;
--            &lt;td&gt;#61-90(CurrentAmtDue)#&lt;br /&gt;
--            &lt;/td&gt;
--            &lt;td style=&quot;text-align: right;&quot;&gt;61-90:&lt;/td&gt;
--            &lt;td&gt;#61-90(PastDue)#&lt;br /&gt;
--            &lt;/td&gt;
--        &lt;/tr&gt;
--        &lt;tr&gt;
--            &lt;td style=&quot;text-align: right;&quot;&gt;Over 91:&lt;/td&gt;
--            &lt;td&gt;#Over91(CurrentAmtDue)#&lt;br /&gt;
--            &lt;/td&gt;
--            &lt;td style=&quot;text-align: right;&quot;&gt;Over 91:&lt;/td&gt;
--            &lt;td&gt;#Over91(PastDue)#&lt;br /&gt;
--            &lt;/td&gt;
--        &lt;/tr&gt;
--    &lt;/tbody&gt;
--&lt;/table&gt;
--&lt;br /&gt;
--&lt;table width=&quot;100%&quot; border=&quot;0&quot; cellpadding=&quot;0&quot;&gt;
--    &lt;tbody&gt;
--        &lt;tr&gt;
--            &lt;td style=&quot;text-align: center;&quot;&gt;#Products#
--            &lt;/td&gt;
--        &lt;/tr&gt;
--    &lt;/tbody&gt;
--&lt;/table&gt;' , -- txtNewBizDocTemplate - text
--          NULL , -- vcBizDocImagePath - varchar(100)
--          NULL , -- vcBizDocFooter - varchar(100)
--          NULL , -- vcPurBizDocFooter - varchar(100)
--          0 , -- bitKeepFooterBottom - bit
--          1  -- numOrientation - int
--        )

ROLLBACK


-- Inserting Bizdoc fields

 --- GRID COLUMNS -----

--Invoice
DECLARE @numMaxFieldId AS NUMERIC(18,0)
SELECT @numMaxFieldId = MAX(numFieldID) + 1 FROM [dbo].[DycFieldMaster] AS DFM
PRINT @numMaxFieldId

SET IDENTITY_INSERT [dbo].[DycFieldMaster] ON

INSERT INTO [dbo].[DycFieldMaster]
        ( [numFieldId],
		  [numModuleID] ,
          [numDomainID] ,
          [vcFieldName] ,
          [vcDbColumnName] ,
          [vcOrigDbColumnName] ,
          [vcPropertyName] ,
          [vcLookBackTableName] ,
          [vcFieldDataType] ,
          [vcFieldType] ,
          [vcAssociatedControlType] ,
          [vcToolTip] ,
          [vcListItemType] ,
          [numListID] ,
          [PopupFunctionName] ,
          [order] ,
          [tintRow] ,
          [tintColumn] ,
          [bitInResults] ,
          [bitDeleted] ,
          [bitAllowEdit] ,
          [bitDefault] ,
          [bitSettingField] ,
          [bitAddField] ,
          [bitDetailField] ,
          [bitAllowSorting] ,
          [bitWorkFlowField] ,
          [bitImport] ,
          [bitExport] ,
          [bitAllowFiltering] ,
          [bitInlineEdit] ,
          [bitRequired] ,
          [intColumnWidth] ,
          [intFieldMaxLength] ,
          [intWFCompare] ,
          [vcGroup] ,
          [vcWFCompareField]
        )
VALUES  ( @numMaxFieldId , -- numFieldID - numeric
		  4 , -- numModuleID - numeric
          NULL , -- numDomainID - numeric
          N'Invoice' , -- vcFieldName - nvarchar(50)
          N'vcBizDocID' , -- vcDbColumnName - nvarchar(50)
          N'vcBizDocID' , -- vcOrigDbColumnName - nvarchar(50)
          'InvoiceID' , -- vcPropertyName - varchar(100)
          N'CSGrid' , -- vcLookBackTableName - nvarchar(50)
          'V' , -- vcFieldDataType - char(1)
          'R' , -- vcFieldType - char(1)
          N'TextBox' , -- vcAssociatedControlType - nvarchar(50)
          N'InvoiceID' , -- vcToolTip - nvarchar(1000)
          '' , -- vcListItemType - varchar(3)
          0 , -- numListID - numeric
          NULL , -- PopupFunctionName - varchar(100)
          63 , -- order - tinyint
          NULL , -- tintRow - tinyint
          NULL , -- tintColumn - tinyint
          1 , -- bitInResults - bit
          0 , -- bitDeleted - bit
          1 , -- bitAllowEdit - bit
          1 , -- bitDefault - bit
          1 , -- bitSettingField - bit
          NULL , -- bitAddField - bit
          NULL , -- bitDetailField - bit
          1 , -- bitAllowSorting - bit
          NULL , -- bitWorkFlowField - bit
          NULL , -- bitImport - bit
          NULL , -- bitExport - bit
          1 , -- bitAllowFiltering - bit
          NULL , -- bitInlineEdit - bit
          NULL , -- bitRequired - bit
          NULL , -- intColumnWidth - int
          NULL , -- intFieldMaxLength - int
          NULL , -- intWFCompare - int
          NULL , -- vcGroup - varchar(100)
          NULL  -- vcWFCompareField - varchar(150)
        )

SET IDENTITY_INSERT [dbo].[DycFieldMaster] OFF

INSERT INTO [dbo].[DycFormField_Mapping]
        ( [numModuleID] ,
          [numFieldID] ,
          [numDomainID] ,
          [numFormID] ,
          [bitAllowEdit] ,
          [bitInlineEdit] ,
          [vcFieldName] ,
          [vcAssociatedControlType] ,
          [vcPropertyName] ,
          [PopupFunctionName] ,
          [order] ,
          [tintRow] ,
          [tintColumn] ,
          [bitInResults] ,
          [bitDeleted] ,
          [bitDefault] ,
          [bitSettingField] ,
          [bitAddField] ,
          [bitDetailField] ,
          [bitAllowSorting] ,
          [bitWorkFlowField] ,
          [bitImport] ,
          [bitExport] ,
          [bitAllowFiltering] ,
          [bitRequired] ,
          [numFormFieldID] ,
          [intSectionID] ,
          [bitAllowGridColor]
        )
VALUES  ( 4 , -- numModuleID - numeric
          @numMaxFieldId , -- numFieldID - numeric
          NULL , -- numDomainID - numeric
          126 , -- numFormID - numeric
          1 , -- bitAllowEdit - bit
          1 , -- bitInlineEdit - bit
          N'Invoice' , -- vcFieldName - nvarchar(50)
          N'TextBox' , -- vcAssociatedControlType - nvarchar(50)
          'InvoiceID' , -- vcPropertyName - varchar(100)
          '' , -- PopupFunctionName - varchar(100)
          0 , -- order - tinyint
          1 , -- tintRow - tinyint
          0 , -- tintColumn - tinyint
          1 , -- bitInResults - bit
          0 , -- bitDeleted - bit
          1 , -- bitDefault - bit
          NULL , -- bitSettingField - bit
          NULL , -- bitAddField - bit
          NULL , -- bitDetailField - bit
          NULL , -- bitAllowSorting - bit
          NULL , -- bitWorkFlowField - bit
          NULL , -- bitImport - bit
          NULL , -- bitExport - bit
          NULL , -- bitAllowFiltering - bit
          NULL , -- bitRequired - bit
          NULL , -- numFormFieldID - numeric
          NULL, -- intSectionID - int
          NULL  -- bitAllowGridColor - bit
        )

-----------------------------

-- Order ID
DECLARE @numMaxFieldId AS NUMERIC(18,0)
SELECT @numMaxFieldId = MAX(numFieldID) + 1 FROM [dbo].[DycFieldMaster] AS DFM
PRINT @numMaxFieldId

SET IDENTITY_INSERT [dbo].[DycFieldMaster] ON

INSERT INTO [dbo].[DycFieldMaster]
        ( [numFieldId],
		  [numModuleID] ,
          [numDomainID] ,
          [vcFieldName] ,
          [vcDbColumnName] ,
          [vcOrigDbColumnName] ,
          [vcPropertyName] ,
          [vcLookBackTableName] ,
          [vcFieldDataType] ,
          [vcFieldType] ,
          [vcAssociatedControlType] ,
          [vcToolTip] ,
          [vcListItemType] ,
          [numListID] ,
          [PopupFunctionName] ,
          [order] ,
          [tintRow] ,
          [tintColumn] ,
          [bitInResults] ,
          [bitDeleted] ,
          [bitAllowEdit] ,
          [bitDefault] ,
          [bitSettingField] ,
          [bitAddField] ,
          [bitDetailField] ,
          [bitAllowSorting] ,
          [bitWorkFlowField] ,
          [bitImport] ,
          [bitExport] ,
          [bitAllowFiltering] ,
          [bitInlineEdit] ,
          [bitRequired] ,
          [intColumnWidth] ,
          [intFieldMaxLength] ,
          [intWFCompare] ,
          [vcGroup] ,
          [vcWFCompareField]
        )
VALUES  ( @numMaxFieldId , -- numFieldID - numeric
		  4 , -- numModuleID - numeric
          NULL , -- numDomainID - numeric
          N'Order ID' , -- vcFieldName - nvarchar(50)
          N'vcPOppName' , -- vcDbColumnName - nvarchar(50)
          N'vcPOppName' , -- vcOrigDbColumnName - nvarchar(50)
          'OrderID' , -- vcPropertyName - varchar(100)
          N'CSGrid' , -- vcLookBackTableName - nvarchar(50)
          'V' , -- vcFieldDataType - char(1)
          'R' , -- vcFieldType - char(1)
          N'TextBox' , -- vcAssociatedControlType - nvarchar(50)
          N'OrderID' , -- vcToolTip - nvarchar(1000)
          '' , -- vcListItemType - varchar(3)
          0 , -- numListID - numeric
          NULL , -- PopupFunctionName - varchar(100)
          63 , -- order - tinyint
          NULL , -- tintRow - tinyint
          NULL , -- tintColumn - tinyint
          1 , -- bitInResults - bit
          0 , -- bitDeleted - bit
          1 , -- bitAllowEdit - bit
          1 , -- bitDefault - bit
          1 , -- bitSettingField - bit
          NULL , -- bitAddField - bit
          NULL , -- bitDetailField - bit
          1 , -- bitAllowSorting - bit
          NULL , -- bitWorkFlowField - bit
          NULL , -- bitImport - bit
          NULL , -- bitExport - bit
          1 , -- bitAllowFiltering - bit
          NULL , -- bitInlineEdit - bit
          NULL , -- bitRequired - bit
          NULL , -- intColumnWidth - int
          NULL , -- intFieldMaxLength - int
          NULL , -- intWFCompare - int
          NULL , -- vcGroup - varchar(100)
          NULL  -- vcWFCompareField - varchar(150)
        )

SET IDENTITY_INSERT [dbo].[DycFieldMaster] OFF

INSERT INTO [dbo].[DycFormField_Mapping]
        ( [numModuleID] ,
          [numFieldID] ,
          [numDomainID] ,
          [numFormID] ,
          [bitAllowEdit] ,
          [bitInlineEdit] ,
          [vcFieldName] ,
          [vcAssociatedControlType] ,
          [vcPropertyName] ,
          [PopupFunctionName] ,
          [order] ,
          [tintRow] ,
          [tintColumn] ,
          [bitInResults] ,
          [bitDeleted] ,
          [bitDefault] ,
          [bitSettingField] ,
          [bitAddField] ,
          [bitDetailField] ,
          [bitAllowSorting] ,
          [bitWorkFlowField] ,
          [bitImport] ,
          [bitExport] ,
          [bitAllowFiltering] ,
          [bitRequired] ,
          [numFormFieldID] ,
          [intSectionID] ,
          [bitAllowGridColor]
        )
VALUES  ( 4 , -- numModuleID - numeric
          @numMaxFieldId , -- numFieldID - numeric
          NULL , -- numDomainID - numeric
          126 , -- numFormID - numeric
          1 , -- bitAllowEdit - bit
          1 , -- bitInlineEdit - bit
          N'Order ID' , -- vcFieldName - nvarchar(50)
          N'TextBox' , -- vcAssociatedControlType - nvarchar(50)
          'OrderID' , -- vcPropertyName - varchar(100)
          '' , -- PopupFunctionName - varchar(100)
          0 , -- order - tinyint
          1 , -- tintRow - tinyint
          0 , -- tintColumn - tinyint
          1 , -- bitInResults - bit
          0 , -- bitDeleted - bit
          1 , -- bitDefault - bit
          NULL , -- bitSettingField - bit
          NULL , -- bitAddField - bit
          NULL , -- bitDetailField - bit
          NULL , -- bitAllowSorting - bit
          NULL , -- bitWorkFlowField - bit
          NULL , -- bitImport - bit
          NULL , -- bitExport - bit
          NULL , -- bitAllowFiltering - bit
          NULL , -- bitRequired - bit
          NULL , -- numFormFieldID - numeric
          NULL, -- intSectionID - int
          NULL  -- bitAllowGridColor - bit
        )
-------------------------

-- Billing Date
DECLARE @numMaxFieldId AS NUMERIC(18,0)
SELECT @numMaxFieldId = MAX(numFieldID) + 1 FROM [dbo].[DycFieldMaster] AS DFM
PRINT @numMaxFieldId

SET IDENTITY_INSERT [dbo].[DycFieldMaster] ON

INSERT INTO [dbo].[DycFieldMaster]
        ( [numFieldId],
		  [numModuleID] ,
          [numDomainID] ,
          [vcFieldName] ,
          [vcDbColumnName] ,
          [vcOrigDbColumnName] ,
          [vcPropertyName] ,
          [vcLookBackTableName] ,
          [vcFieldDataType] ,
          [vcFieldType] ,
          [vcAssociatedControlType] ,
          [vcToolTip] ,
          [vcListItemType] ,
          [numListID] ,
          [PopupFunctionName] ,
          [order] ,
          [tintRow] ,
          [tintColumn] ,
          [bitInResults] ,
          [bitDeleted] ,
          [bitAllowEdit] ,
          [bitDefault] ,
          [bitSettingField] ,
          [bitAddField] ,
          [bitDetailField] ,
          [bitAllowSorting] ,
          [bitWorkFlowField] ,
          [bitImport] ,
          [bitExport] ,
          [bitAllowFiltering] ,
          [bitInlineEdit] ,
          [bitRequired] ,
          [intColumnWidth] ,
          [intFieldMaxLength] ,
          [intWFCompare] ,
          [vcGroup] ,
          [vcWFCompareField]
        )
VALUES  ( @numMaxFieldId , -- numFieldID - numeric
		  4 , -- numModuleID - numeric
          NULL , -- numDomainID - numeric
          N'Billing Date' , -- vcFieldName - nvarchar(50)
          N'dtFromDate' , -- vcDbColumnName - nvarchar(50)
          N'dtFromDate' , -- vcOrigDbColumnName - nvarchar(50)
          'BillingDate' , -- vcPropertyName - varchar(100)
          N'CSGrid' , -- vcLookBackTableName - nvarchar(50)
          'V' , -- vcFieldDataType - char(1)
          'R' , -- vcFieldType - char(1)
          N'TextBox' , -- vcAssociatedControlType - nvarchar(50)
          N'Billing Date' , -- vcToolTip - nvarchar(1000)
          '' , -- vcListItemType - varchar(3)
          0 , -- numListID - numeric
          NULL , -- PopupFunctionName - varchar(100)
          63 , -- order - tinyint
          NULL , -- tintRow - tinyint
          NULL , -- tintColumn - tinyint
          1 , -- bitInResults - bit
          0 , -- bitDeleted - bit
          1 , -- bitAllowEdit - bit
          1 , -- bitDefault - bit
          1 , -- bitSettingField - bit
          NULL , -- bitAddField - bit
          NULL , -- bitDetailField - bit
          1 , -- bitAllowSorting - bit
          NULL , -- bitWorkFlowField - bit
          NULL , -- bitImport - bit
          NULL , -- bitExport - bit
          1 , -- bitAllowFiltering - bit
          NULL , -- bitInlineEdit - bit
          NULL , -- bitRequired - bit
          NULL , -- intColumnWidth - int
          NULL , -- intFieldMaxLength - int
          NULL , -- intWFCompare - int
          NULL , -- vcGroup - varchar(100)
          NULL  -- vcWFCompareField - varchar(150)
        )

SET IDENTITY_INSERT [dbo].[DycFieldMaster] OFF

INSERT INTO [dbo].[DycFormField_Mapping]
        ( [numModuleID] ,
          [numFieldID] ,
          [numDomainID] ,
          [numFormID] ,
          [bitAllowEdit] ,
          [bitInlineEdit] ,
          [vcFieldName] ,
          [vcAssociatedControlType] ,
          [vcPropertyName] ,
          [PopupFunctionName] ,
          [order] ,
          [tintRow] ,
          [tintColumn] ,
          [bitInResults] ,
          [bitDeleted] ,
          [bitDefault] ,
          [bitSettingField] ,
          [bitAddField] ,
          [bitDetailField] ,
          [bitAllowSorting] ,
          [bitWorkFlowField] ,
          [bitImport] ,
          [bitExport] ,
          [bitAllowFiltering] ,
          [bitRequired] ,
          [numFormFieldID] ,
          [intSectionID] ,
          [bitAllowGridColor]
        )
VALUES  ( 4 , -- numModuleID - numeric
          @numMaxFieldId , -- numFieldID - numeric
          NULL , -- numDomainID - numeric
          126 , -- numFormID - numeric
          1 , -- bitAllowEdit - bit
          1 , -- bitInlineEdit - bit
          N'Billing Date' , -- vcFieldName - nvarchar(50)
          N'TextBox' , -- vcAssociatedControlType - nvarchar(50)
          'BillingDate' , -- vcPropertyName - varchar(100)
          '' , -- PopupFunctionName - varchar(100)
          0 , -- order - tinyint
          1 , -- tintRow - tinyint
          0 , -- tintColumn - tinyint
          1 , -- bitInResults - bit
          0 , -- bitDeleted - bit
          1 , -- bitDefault - bit
          NULL , -- bitSettingField - bit
          NULL , -- bitAddField - bit
          NULL , -- bitDetailField - bit
          NULL , -- bitAllowSorting - bit
          NULL , -- bitWorkFlowField - bit
          NULL , -- bitImport - bit
          NULL , -- bitExport - bit
          NULL , -- bitAllowFiltering - bit
          NULL , -- bitRequired - bit
          NULL , -- numFormFieldID - numeric
          NULL, -- intSectionID - int
          NULL  -- bitAllowGridColor - bit
        )

-----------------------------


-- Due Date
DECLARE @numMaxFieldId AS NUMERIC(18,0)
SELECT @numMaxFieldId = MAX(numFieldID) + 1 FROM [dbo].[DycFieldMaster] AS DFM
PRINT @numMaxFieldId

SET IDENTITY_INSERT [dbo].[DycFieldMaster] ON

INSERT INTO [dbo].[DycFieldMaster]
        ( [numFieldId],
		  [numModuleID] ,
          [numDomainID] ,
          [vcFieldName] ,
          [vcDbColumnName] ,
          [vcOrigDbColumnName] ,
          [vcPropertyName] ,
          [vcLookBackTableName] ,
          [vcFieldDataType] ,
          [vcFieldType] ,
          [vcAssociatedControlType] ,
          [vcToolTip] ,
          [vcListItemType] ,
          [numListID] ,
          [PopupFunctionName] ,
          [order] ,
          [tintRow] ,
          [tintColumn] ,
          [bitInResults] ,
          [bitDeleted] ,
          [bitAllowEdit] ,
          [bitDefault] ,
          [bitSettingField] ,
          [bitAddField] ,
          [bitDetailField] ,
          [bitAllowSorting] ,
          [bitWorkFlowField] ,
          [bitImport] ,
          [bitExport] ,
          [bitAllowFiltering] ,
          [bitInlineEdit] ,
          [bitRequired] ,
          [intColumnWidth] ,
          [intFieldMaxLength] ,
          [intWFCompare] ,
          [vcGroup] ,
          [vcWFCompareField]
        )
VALUES  ( @numMaxFieldId , -- numFieldID - numeric
		  4 , -- numModuleID - numeric
          NULL , -- numDomainID - numeric
          N'Due Date' , -- vcFieldName - nvarchar(50)
          N'DueDate' , -- vcDbColumnName - nvarchar(50)
          N'DueDate' , -- vcOrigDbColumnName - nvarchar(50)
          'DueDate' , -- vcPropertyName - varchar(100)
          N'CSGrid' , -- vcLookBackTableName - nvarchar(50)
          'V' , -- vcFieldDataType - char(1)
          'R' , -- vcFieldType - char(1)
          N'TextBox' , -- vcAssociatedControlType - nvarchar(50)
          N'Due Date' , -- vcToolTip - nvarchar(1000)
          '' , -- vcListItemType - varchar(3)
          0 , -- numListID - numeric
          NULL , -- PopupFunctionName - varchar(100)
          63 , -- order - tinyint
          NULL , -- tintRow - tinyint
          NULL , -- tintColumn - tinyint
          1 , -- bitInResults - bit
          0 , -- bitDeleted - bit
          1 , -- bitAllowEdit - bit
          1 , -- bitDefault - bit
          1 , -- bitSettingField - bit
          NULL , -- bitAddField - bit
          NULL , -- bitDetailField - bit
          1 , -- bitAllowSorting - bit
          NULL , -- bitWorkFlowField - bit
          NULL , -- bitImport - bit
          NULL , -- bitExport - bit
          1 , -- bitAllowFiltering - bit
          NULL , -- bitInlineEdit - bit
          NULL , -- bitRequired - bit
          NULL , -- intColumnWidth - int
          NULL , -- intFieldMaxLength - int
          NULL , -- intWFCompare - int
          NULL , -- vcGroup - varchar(100)
          NULL  -- vcWFCompareField - varchar(150)
        )

SET IDENTITY_INSERT [dbo].[DycFieldMaster] OFF

INSERT INTO [dbo].[DycFormField_Mapping]
        ( [numModuleID] ,
          [numFieldID] ,
          [numDomainID] ,
          [numFormID] ,
          [bitAllowEdit] ,
          [bitInlineEdit] ,
          [vcFieldName] ,
          [vcAssociatedControlType] ,
          [vcPropertyName] ,
          [PopupFunctionName] ,
          [order] ,
          [tintRow] ,
          [tintColumn] ,
          [bitInResults] ,
          [bitDeleted] ,
          [bitDefault] ,
          [bitSettingField] ,
          [bitAddField] ,
          [bitDetailField] ,
          [bitAllowSorting] ,
          [bitWorkFlowField] ,
          [bitImport] ,
          [bitExport] ,
          [bitAllowFiltering] ,
          [bitRequired] ,
          [numFormFieldID] ,
          [intSectionID] ,
          [bitAllowGridColor]
        )
VALUES  ( 4 , -- numModuleID - numeric
          @numMaxFieldId , -- numFieldID - numeric
          NULL , -- numDomainID - numeric
          126 , -- numFormID - numeric
          1 , -- bitAllowEdit - bit
          1 , -- bitInlineEdit - bit
          N'Due Date' , -- vcFieldName - nvarchar(50)
          N'TextBox' , -- vcAssociatedControlType - nvarchar(50)
          'DueDate' , -- vcPropertyName - varchar(100)
          '' , -- PopupFunctionName - varchar(100)
          0 , -- order - tinyint
          1 , -- tintRow - tinyint
          0 , -- tintColumn - tinyint
          1 , -- bitInResults - bit
          0 , -- bitDeleted - bit
          1 , -- bitDefault - bit
          NULL , -- bitSettingField - bit
          NULL , -- bitAddField - bit
          NULL , -- bitDetailField - bit
          NULL , -- bitAllowSorting - bit
          NULL , -- bitWorkFlowField - bit
          NULL , -- bitImport - bit
          NULL , -- bitExport - bit
          NULL , -- bitAllowFiltering - bit
          NULL , -- bitRequired - bit
          NULL , -- numFormFieldID - numeric
          NULL, -- intSectionID - int
          NULL  -- bitAllowGridColor - bit
        )

-----------------------------


-- Total Invoice
DECLARE @numMaxFieldId AS NUMERIC(18,0)
SELECT @numMaxFieldId = MAX(numFieldID) + 1 FROM [dbo].[DycFieldMaster] AS DFM
PRINT @numMaxFieldId

SET IDENTITY_INSERT [dbo].[DycFieldMaster] ON

INSERT INTO [dbo].[DycFieldMaster]
        ( [numFieldId],
		  [numModuleID] ,
          [numDomainID] ,
          [vcFieldName] ,
          [vcDbColumnName] ,
          [vcOrigDbColumnName] ,
          [vcPropertyName] ,
          [vcLookBackTableName] ,
          [vcFieldDataType] ,
          [vcFieldType] ,
          [vcAssociatedControlType] ,
          [vcToolTip] ,
          [vcListItemType] ,
          [numListID] ,
          [PopupFunctionName] ,
          [order] ,
          [tintRow] ,
          [tintColumn] ,
          [bitInResults] ,
          [bitDeleted] ,
          [bitAllowEdit] ,
          [bitDefault] ,
          [bitSettingField] ,
          [bitAddField] ,
          [bitDetailField] ,
          [bitAllowSorting] ,
          [bitWorkFlowField] ,
          [bitImport] ,
          [bitExport] ,
          [bitAllowFiltering] ,
          [bitInlineEdit] ,
          [bitRequired] ,
          [intColumnWidth] ,
          [intFieldMaxLength] ,
          [intWFCompare] ,
          [vcGroup] ,
          [vcWFCompareField]
        )
VALUES  ( @numMaxFieldId , -- numFieldID - numeric
		  4 , -- numModuleID - numeric
          NULL , -- numDomainID - numeric
          N'Total Invoice' , -- vcFieldName - nvarchar(50)
          N'TotalAmount' , -- vcDbColumnName - nvarchar(50)
          N'TotalAmount' , -- vcOrigDbColumnName - nvarchar(50)
          'TotalInvoice' , -- vcPropertyName - varchar(100)
          N'CSGrid' , -- vcLookBackTableName - nvarchar(50)
          'N' , -- vcFieldDataType - char(1)
          'R' , -- vcFieldType - char(1)
          N'TextBox' , -- vcAssociatedControlType - nvarchar(50)
          N'Total Invoice' , -- vcToolTip - nvarchar(1000)
          '' , -- vcListItemType - varchar(3)
          0 , -- numListID - numeric
          NULL , -- PopupFunctionName - varchar(100)
          63 , -- order - tinyint
          NULL , -- tintRow - tinyint
          NULL , -- tintColumn - tinyint
          1 , -- bitInResults - bit
          0 , -- bitDeleted - bit
          1 , -- bitAllowEdit - bit
          1 , -- bitDefault - bit
          1 , -- bitSettingField - bit
          NULL , -- bitAddField - bit
          NULL , -- bitDetailField - bit
          1 , -- bitAllowSorting - bit
          NULL , -- bitWorkFlowField - bit
          NULL , -- bitImport - bit
          NULL , -- bitExport - bit
          1 , -- bitAllowFiltering - bit
          NULL , -- bitInlineEdit - bit
          NULL , -- bitRequired - bit
          NULL , -- intColumnWidth - int
          NULL , -- intFieldMaxLength - int
          NULL , -- intWFCompare - int
          NULL , -- vcGroup - varchar(100)
          NULL  -- vcWFCompareField - varchar(150)
        )

SET IDENTITY_INSERT [dbo].[DycFieldMaster] OFF

INSERT INTO [dbo].[DycFormField_Mapping]
        ( [numModuleID] ,
          [numFieldID] ,
          [numDomainID] ,
          [numFormID] ,
          [bitAllowEdit] ,
          [bitInlineEdit] ,
          [vcFieldName] ,
          [vcAssociatedControlType] ,
          [vcPropertyName] ,
          [PopupFunctionName] ,
          [order] ,
          [tintRow] ,
          [tintColumn] ,
          [bitInResults] ,
          [bitDeleted] ,
          [bitDefault] ,
          [bitSettingField] ,
          [bitAddField] ,
          [bitDetailField] ,
          [bitAllowSorting] ,
          [bitWorkFlowField] ,
          [bitImport] ,
          [bitExport] ,
          [bitAllowFiltering] ,
          [bitRequired] ,
          [numFormFieldID] ,
          [intSectionID] ,
          [bitAllowGridColor]
        )
VALUES  ( 4 , -- numModuleID - numeric
          @numMaxFieldId , -- numFieldID - numeric
          NULL , -- numDomainID - numeric
          126 , -- numFormID - numeric
          1 , -- bitAllowEdit - bit
          1 , -- bitInlineEdit - bit
          N'Total Invoice' , -- vcFieldName - nvarchar(50)
          N'TextBox' , -- vcAssociatedControlType - nvarchar(50)
          'TotalInvoice' , -- vcPropertyName - varchar(100)
          '' , -- PopupFunctionName - varchar(100)
          0 , -- order - tinyint
          1 , -- tintRow - tinyint
          0 , -- tintColumn - tinyint
          1 , -- bitInResults - bit
          0 , -- bitDeleted - bit
          1 , -- bitDefault - bit
          NULL , -- bitSettingField - bit
          NULL , -- bitAddField - bit
          NULL , -- bitDetailField - bit
          NULL , -- bitAllowSorting - bit
          NULL , -- bitWorkFlowField - bit
          NULL , -- bitImport - bit
          NULL , -- bitExport - bit
          NULL , -- bitAllowFiltering - bit
          NULL , -- bitRequired - bit
          NULL , -- numFormFieldID - numeric
          NULL, -- intSectionID - int
          NULL  -- bitAllowGridColor - bit
        )

-----------------------------

-- Balance Due
DECLARE @numMaxFieldId AS NUMERIC(18,0)
SELECT @numMaxFieldId = MAX(numFieldID) + 1 FROM [dbo].[DycFieldMaster] AS DFM
PRINT @numMaxFieldId

SET IDENTITY_INSERT [dbo].[DycFieldMaster] ON

INSERT INTO [dbo].[DycFieldMaster]
        ( [numFieldId],
		  [numModuleID] ,
          [numDomainID] ,
          [vcFieldName] ,
          [vcDbColumnName] ,
          [vcOrigDbColumnName] ,
          [vcPropertyName] ,
          [vcLookBackTableName] ,
          [vcFieldDataType] ,
          [vcFieldType] ,
          [vcAssociatedControlType] ,
          [vcToolTip] ,
          [vcListItemType] ,
          [numListID] ,
          [PopupFunctionName] ,
          [order] ,
          [tintRow] ,
          [tintColumn] ,
          [bitInResults] ,
          [bitDeleted] ,
          [bitAllowEdit] ,
          [bitDefault] ,
          [bitSettingField] ,
          [bitAddField] ,
          [bitDetailField] ,
          [bitAllowSorting] ,
          [bitWorkFlowField] ,
          [bitImport] ,
          [bitExport] ,
          [bitAllowFiltering] ,
          [bitInlineEdit] ,
          [bitRequired] ,
          [intColumnWidth] ,
          [intFieldMaxLength] ,
          [intWFCompare] ,
          [vcGroup] ,
          [vcWFCompareField]
        )
VALUES  ( @numMaxFieldId , -- numFieldID - numeric
		  4 , -- numModuleID - numeric
          NULL , -- numDomainID - numeric
          N'Balance Due' , -- vcFieldName - nvarchar(50)
          N'BalanceDue' , -- vcDbColumnName - nvarchar(50)
          N'BalanceDue' , -- vcOrigDbColumnName - nvarchar(50)
          'BalanceDueGrid' , -- vcPropertyName - varchar(100)
          N'CSGrid' , -- vcLookBackTableName - nvarchar(50)
          'N' , -- vcFieldDataType - char(1)
          'R' , -- vcFieldType - char(1)
          N'TextBox' , -- vcAssociatedControlType - nvarchar(50)
          N'Balance Due' , -- vcToolTip - nvarchar(1000)
          '' , -- vcListItemType - varchar(3)
          0 , -- numListID - numeric
          NULL , -- PopupFunctionName - varchar(100)
          63 , -- order - tinyint
          NULL , -- tintRow - tinyint
          NULL , -- tintColumn - tinyint
          1 , -- bitInResults - bit
          0 , -- bitDeleted - bit
          1 , -- bitAllowEdit - bit
          1 , -- bitDefault - bit
          1 , -- bitSettingField - bit
          NULL , -- bitAddField - bit
          NULL , -- bitDetailField - bit
          1 , -- bitAllowSorting - bit
          NULL , -- bitWorkFlowField - bit
          NULL , -- bitImport - bit
          NULL , -- bitExport - bit
          1 , -- bitAllowFiltering - bit
          NULL , -- bitInlineEdit - bit
          NULL , -- bitRequired - bit
          NULL , -- intColumnWidth - int
          NULL , -- intFieldMaxLength - int
          NULL , -- intWFCompare - int
          NULL , -- vcGroup - varchar(100)
          NULL  -- vcWFCompareField - varchar(150)
        )

SET IDENTITY_INSERT [dbo].[DycFieldMaster] OFF

INSERT INTO [dbo].[DycFormField_Mapping]
        ( [numModuleID] ,
          [numFieldID] ,
          [numDomainID] ,
          [numFormID] ,
          [bitAllowEdit] ,
          [bitInlineEdit] ,
          [vcFieldName] ,
          [vcAssociatedControlType] ,
          [vcPropertyName] ,
          [PopupFunctionName] ,
          [order] ,
          [tintRow] ,
          [tintColumn] ,
          [bitInResults] ,
          [bitDeleted] ,
          [bitDefault] ,
          [bitSettingField] ,
          [bitAddField] ,
          [bitDetailField] ,
          [bitAllowSorting] ,
          [bitWorkFlowField] ,
          [bitImport] ,
          [bitExport] ,
          [bitAllowFiltering] ,
          [bitRequired] ,
          [numFormFieldID] ,
          [intSectionID] ,
          [bitAllowGridColor]
        )
VALUES  ( 4 , -- numModuleID - numeric
          @numMaxFieldId , -- numFieldID - numeric
          NULL , -- numDomainID - numeric
          126 , -- numFormID - numeric
          1 , -- bitAllowEdit - bit
          1 , -- bitInlineEdit - bit
          N'Balance Due' , -- vcFieldName - nvarchar(50)
          N'TextBox' , -- vcAssociatedControlType - nvarchar(50)
          'BalanceDueGrid' , -- vcPropertyName - varchar(100)
          '' , -- PopupFunctionName - varchar(100)
          0 , -- order - tinyint
          1 , -- tintRow - tinyint
          0 , -- tintColumn - tinyint
          1 , -- bitInResults - bit
          0 , -- bitDeleted - bit
          1 , -- bitDefault - bit
          NULL , -- bitSettingField - bit
          NULL , -- bitAddField - bit
          NULL , -- bitDetailField - bit
          NULL , -- bitAllowSorting - bit
          NULL , -- bitWorkFlowField - bit
          NULL , -- bitImport - bit
          NULL , -- bitExport - bit
          NULL , -- bitAllowFiltering - bit
          NULL , -- bitRequired - bit
          NULL , -- numFormFieldID - numeric
          NULL, -- intSectionID - int
          NULL  -- bitAllowGridColor - bit
        )

-----------------------------

-- Interest
DECLARE @numMaxFieldId AS NUMERIC(18,0)
SELECT @numMaxFieldId = MAX(numFieldID) + 1 FROM [dbo].[DycFieldMaster] AS DFM
PRINT @numMaxFieldId

SET IDENTITY_INSERT [dbo].[DycFieldMaster] ON

INSERT INTO [dbo].[DycFieldMaster]
        ( [numFieldId],
		  [numModuleID] ,
          [numDomainID] ,
          [vcFieldName] ,
          [vcDbColumnName] ,
          [vcOrigDbColumnName] ,
          [vcPropertyName] ,
          [vcLookBackTableName] ,
          [vcFieldDataType] ,
          [vcFieldType] ,
          [vcAssociatedControlType] ,
          [vcToolTip] ,
          [vcListItemType] ,
          [numListID] ,
          [PopupFunctionName] ,
          [order] ,
          [tintRow] ,
          [tintColumn] ,
          [bitInResults] ,
          [bitDeleted] ,
          [bitAllowEdit] ,
          [bitDefault] ,
          [bitSettingField] ,
          [bitAddField] ,
          [bitDetailField] ,
          [bitAllowSorting] ,
          [bitWorkFlowField] ,
          [bitImport] ,
          [bitExport] ,
          [bitAllowFiltering] ,
          [bitInlineEdit] ,
          [bitRequired] ,
          [intColumnWidth] ,
          [intFieldMaxLength] ,
          [intWFCompare] ,
          [vcGroup] ,
          [vcWFCompareField]
        )
VALUES  ( @numMaxFieldId , -- numFieldID - numeric
		  4 , -- numModuleID - numeric
          NULL , -- numDomainID - numeric
          N'Interest' , -- vcFieldName - nvarchar(50)
          N'numInterest' , -- vcDbColumnName - nvarchar(50)
          N'numInterest' , -- vcOrigDbColumnName - nvarchar(50)
          'Interest' , -- vcPropertyName - varchar(100)
          N'CSGrid' , -- vcLookBackTableName - nvarchar(50)
          'N' , -- vcFieldDataType - char(1)
          'R' , -- vcFieldType - char(1)
          N'TextBox' , -- vcAssociatedControlType - nvarchar(50)
          N'Interest' , -- vcToolTip - nvarchar(1000)
          '' , -- vcListItemType - varchar(3)
          0 , -- numListID - numeric
          NULL , -- PopupFunctionName - varchar(100)
          63 , -- order - tinyint
          NULL , -- tintRow - tinyint
          NULL , -- tintColumn - tinyint
          1 , -- bitInResults - bit
          0 , -- bitDeleted - bit
          1 , -- bitAllowEdit - bit
          1 , -- bitDefault - bit
          1 , -- bitSettingField - bit
          NULL , -- bitAddField - bit
          NULL , -- bitDetailField - bit
          1 , -- bitAllowSorting - bit
          NULL , -- bitWorkFlowField - bit
          NULL , -- bitImport - bit
          NULL , -- bitExport - bit
          1 , -- bitAllowFiltering - bit
          NULL , -- bitInlineEdit - bit
          NULL , -- bitRequired - bit
          NULL , -- intColumnWidth - int
          NULL , -- intFieldMaxLength - int
          NULL , -- intWFCompare - int
          NULL , -- vcGroup - varchar(100)
          NULL  -- vcWFCompareField - varchar(150)
        )

SET IDENTITY_INSERT [dbo].[DycFieldMaster] OFF

INSERT INTO [dbo].[DycFormField_Mapping]
        ( [numModuleID] ,
          [numFieldID] ,
          [numDomainID] ,
          [numFormID] ,
          [bitAllowEdit] ,
          [bitInlineEdit] ,
          [vcFieldName] ,
          [vcAssociatedControlType] ,
          [vcPropertyName] ,
          [PopupFunctionName] ,
          [order] ,
          [tintRow] ,
          [tintColumn] ,
          [bitInResults] ,
          [bitDeleted] ,
          [bitDefault] ,
          [bitSettingField] ,
          [bitAddField] ,
          [bitDetailField] ,
          [bitAllowSorting] ,
          [bitWorkFlowField] ,
          [bitImport] ,
          [bitExport] ,
          [bitAllowFiltering] ,
          [bitRequired] ,
          [numFormFieldID] ,
          [intSectionID] ,
          [bitAllowGridColor]
        )
VALUES  ( 4 , -- numModuleID - numeric
          @numMaxFieldId , -- numFieldID - numeric
          NULL , -- numDomainID - numeric
          126 , -- numFormID - numeric
          1 , -- bitAllowEdit - bit
          1 , -- bitInlineEdit - bit
          N'Interest' , -- vcFieldName - nvarchar(50)
          N'TextBox' , -- vcAssociatedControlType - nvarchar(50)
          'Interest' , -- vcPropertyName - varchar(100)
          '' , -- PopupFunctionName - varchar(100)
          0 , -- order - tinyint
          1 , -- tintRow - tinyint
          0 , -- tintColumn - tinyint
          1 , -- bitInResults - bit
          0 , -- bitDeleted - bit
          1 , -- bitDefault - bit
          NULL , -- bitSettingField - bit
          NULL , -- bitAddField - bit
          NULL , -- bitDetailField - bit
          NULL , -- bitAllowSorting - bit
          NULL , -- bitWorkFlowField - bit
          NULL , -- bitImport - bit
          NULL , -- bitExport - bit
          NULL , -- bitAllowFiltering - bit
          NULL , -- bitRequired - bit
          NULL , -- numFormFieldID - numeric
          NULL, -- intSectionID - int
          NULL  -- bitAllowGridColor - bit
        )

-----------------------------

-- Discount
DECLARE @numMaxFieldId AS NUMERIC(18,0)
SELECT @numMaxFieldId = MAX(numFieldID) + 1 FROM [dbo].[DycFieldMaster] AS DFM
PRINT @numMaxFieldId

SET IDENTITY_INSERT [dbo].[DycFieldMaster] ON

INSERT INTO [dbo].[DycFieldMaster]
        ( [numFieldId],
		  [numModuleID] ,
          [numDomainID] ,
          [vcFieldName] ,
          [vcDbColumnName] ,
          [vcOrigDbColumnName] ,
          [vcPropertyName] ,
          [vcLookBackTableName] ,
          [vcFieldDataType] ,
          [vcFieldType] ,
          [vcAssociatedControlType] ,
          [vcToolTip] ,
          [vcListItemType] ,
          [numListID] ,
          [PopupFunctionName] ,
          [order] ,
          [tintRow] ,
          [tintColumn] ,
          [bitInResults] ,
          [bitDeleted] ,
          [bitAllowEdit] ,
          [bitDefault] ,
          [bitSettingField] ,
          [bitAddField] ,
          [bitDetailField] ,
          [bitAllowSorting] ,
          [bitWorkFlowField] ,
          [bitImport] ,
          [bitExport] ,
          [bitAllowFiltering] ,
          [bitInlineEdit] ,
          [bitRequired] ,
          [intColumnWidth] ,
          [intFieldMaxLength] ,
          [intWFCompare] ,
          [vcGroup] ,
          [vcWFCompareField]
        )
VALUES  ( @numMaxFieldId , -- numFieldID - numeric
		  4 , -- numModuleID - numeric
          NULL , -- numDomainID - numeric
          N'Discount' , -- vcFieldName - nvarchar(50)
          N'numDiscount' , -- vcDbColumnName - nvarchar(50)
          N'numDiscount' , -- vcOrigDbColumnName - nvarchar(50)
          'Discount' , -- vcPropertyName - varchar(100)
          N'CSGrid' , -- vcLookBackTableName - nvarchar(50)
          'N' , -- vcFieldDataType - char(1)
          'R' , -- vcFieldType - char(1)
          N'TextBox' , -- vcAssociatedControlType - nvarchar(50)
          N'Discount' , -- vcToolTip - nvarchar(1000)
          '' , -- vcListItemType - varchar(3)
          0 , -- numListID - numeric
          NULL , -- PopupFunctionName - varchar(100)
          63 , -- order - tinyint
          NULL , -- tintRow - tinyint
          NULL , -- tintColumn - tinyint
          1 , -- bitInResults - bit
          0 , -- bitDeleted - bit
          1 , -- bitAllowEdit - bit
          1 , -- bitDefault - bit
          1 , -- bitSettingField - bit
          NULL , -- bitAddField - bit
          NULL , -- bitDetailField - bit
          1 , -- bitAllowSorting - bit
          NULL , -- bitWorkFlowField - bit
          NULL , -- bitImport - bit
          NULL , -- bitExport - bit
          1 , -- bitAllowFiltering - bit
          NULL , -- bitInlineEdit - bit
          NULL , -- bitRequired - bit
          NULL , -- intColumnWidth - int
          NULL , -- intFieldMaxLength - int
          NULL , -- intWFCompare - int
          NULL , -- vcGroup - varchar(100)
          NULL  -- vcWFCompareField - varchar(150)
        )

SET IDENTITY_INSERT [dbo].[DycFieldMaster] OFF

INSERT INTO [dbo].[DycFormField_Mapping]
        ( [numModuleID] ,
          [numFieldID] ,
          [numDomainID] ,
          [numFormID] ,
          [bitAllowEdit] ,
          [bitInlineEdit] ,
          [vcFieldName] ,
          [vcAssociatedControlType] ,
          [vcPropertyName] ,
          [PopupFunctionName] ,
          [order] ,
          [tintRow] ,
          [tintColumn] ,
          [bitInResults] ,
          [bitDeleted] ,
          [bitDefault] ,
          [bitSettingField] ,
          [bitAddField] ,
          [bitDetailField] ,
          [bitAllowSorting] ,
          [bitWorkFlowField] ,
          [bitImport] ,
          [bitExport] ,
          [bitAllowFiltering] ,
          [bitRequired] ,
          [numFormFieldID] ,
          [intSectionID] ,
          [bitAllowGridColor]
        )
VALUES  ( 4 , -- numModuleID - numeric
          @numMaxFieldId , -- numFieldID - numeric
          NULL , -- numDomainID - numeric
          126 , -- numFormID - numeric
          1 , -- bitAllowEdit - bit
          1 , -- bitInlineEdit - bit
          N'Discount' , -- vcFieldName - nvarchar(50)
          N'TextBox' , -- vcAssociatedControlType - nvarchar(50)
          'Discount' , -- vcPropertyName - varchar(100)
          '' , -- PopupFunctionName - varchar(100)
          0 , -- order - tinyint
          1 , -- tintRow - tinyint
          0 , -- tintColumn - tinyint
          1 , -- bitInResults - bit
          0 , -- bitDeleted - bit
          1 , -- bitDefault - bit
          NULL , -- bitSettingField - bit
          NULL , -- bitAddField - bit
          NULL , -- bitDetailField - bit
          NULL , -- bitAllowSorting - bit
          NULL , -- bitWorkFlowField - bit
          NULL , -- bitImport - bit
          NULL , -- bitExport - bit
          NULL , -- bitAllowFiltering - bit
          NULL , -- bitRequired - bit
          NULL , -- numFormFieldID - numeric
          NULL, -- intSectionID - int
          NULL  -- bitAllowGridColor - bit
        )


-----------------------------

-- Status(Days)
DECLARE @numMaxFieldId AS NUMERIC(18,0)
SELECT @numMaxFieldId = MAX(numFieldID) + 1 FROM [dbo].[DycFieldMaster] AS DFM
PRINT @numMaxFieldId

SET IDENTITY_INSERT [dbo].[DycFieldMaster] ON

INSERT INTO [dbo].[DycFieldMaster]
        ( [numFieldId],
		  [numModuleID] ,
          [numDomainID] ,
          [vcFieldName] ,
          [vcDbColumnName] ,
          [vcOrigDbColumnName] ,
          [vcPropertyName] ,
          [vcLookBackTableName] ,
          [vcFieldDataType] ,
          [vcFieldType] ,
          [vcAssociatedControlType] ,
          [vcToolTip] ,
          [vcListItemType] ,
          [numListID] ,
          [PopupFunctionName] ,
          [order] ,
          [tintRow] ,
          [tintColumn] ,
          [bitInResults] ,
          [bitDeleted] ,
          [bitAllowEdit] ,
          [bitDefault] ,
          [bitSettingField] ,
          [bitAddField] ,
          [bitDetailField] ,
          [bitAllowSorting] ,
          [bitWorkFlowField] ,
          [bitImport] ,
          [bitExport] ,
          [bitAllowFiltering] ,
          [bitInlineEdit] ,
          [bitRequired] ,
          [intColumnWidth] ,
          [intFieldMaxLength] ,
          [intWFCompare] ,
          [vcGroup] ,
          [vcWFCompareField]
        )
VALUES  ( @numMaxFieldId , -- numFieldID - numeric
		  4 , -- numModuleID - numeric
          NULL , -- numDomainID - numeric
          N'Status(Days)' , -- vcFieldName - nvarchar(50)
          N'Status(DaysLate)' , -- vcDbColumnName - nvarchar(50)
          N'Status(DaysLate)' , -- vcOrigDbColumnName - nvarchar(50)
          'Status' , -- vcPropertyName - varchar(100)
          N'CSGrid' , -- vcLookBackTableName - nvarchar(50)
          'V' , -- vcFieldDataType - char(1)
          'R' , -- vcFieldType - char(1)
          N'TextBox' , -- vcAssociatedControlType - nvarchar(50)
          N'Status(Days)' , -- vcToolTip - nvarchar(1000)
          '' , -- vcListItemType - varchar(3)
          0 , -- numListID - numeric
          NULL , -- PopupFunctionName - varchar(100)
          63 , -- order - tinyint
          NULL , -- tintRow - tinyint
          NULL , -- tintColumn - tinyint
          1 , -- bitInResults - bit
          0 , -- bitDeleted - bit
          1 , -- bitAllowEdit - bit
          1 , -- bitDefault - bit
          1 , -- bitSettingField - bit
          NULL , -- bitAddField - bit
          NULL , -- bitDetailField - bit
          1 , -- bitAllowSorting - bit
          NULL , -- bitWorkFlowField - bit
          NULL , -- bitImport - bit
          NULL , -- bitExport - bit
          1 , -- bitAllowFiltering - bit
          NULL , -- bitInlineEdit - bit
          NULL , -- bitRequired - bit
          NULL , -- intColumnWidth - int
          NULL , -- intFieldMaxLength - int
          NULL , -- intWFCompare - int
          NULL , -- vcGroup - varchar(100)
          NULL  -- vcWFCompareField - varchar(150)
        )

SET IDENTITY_INSERT [dbo].[DycFieldMaster] OFF

INSERT INTO [dbo].[DycFormField_Mapping]
        ( [numModuleID] ,
          [numFieldID] ,
          [numDomainID] ,
          [numFormID] ,
          [bitAllowEdit] ,
          [bitInlineEdit] ,
          [vcFieldName] ,
          [vcAssociatedControlType] ,
          [vcPropertyName] ,
          [PopupFunctionName] ,
          [order] ,
          [tintRow] ,
          [tintColumn] ,
          [bitInResults] ,
          [bitDeleted] ,
          [bitDefault] ,
          [bitSettingField] ,
          [bitAddField] ,
          [bitDetailField] ,
          [bitAllowSorting] ,
          [bitWorkFlowField] ,
          [bitImport] ,
          [bitExport] ,
          [bitAllowFiltering] ,
          [bitRequired] ,
          [numFormFieldID] ,
          [intSectionID] ,
          [bitAllowGridColor]
        )
VALUES  ( 4 , -- numModuleID - numeric
          @numMaxFieldId , -- numFieldID - numeric
          NULL , -- numDomainID - numeric
          126 , -- numFormID - numeric
          1 , -- bitAllowEdit - bit
          1 , -- bitInlineEdit - bit
          N'Status(Days)' , -- vcFieldName - nvarchar(50)
          N'TextBox' , -- vcAssociatedControlType - nvarchar(50)
          'Status' , -- vcPropertyName - varchar(100)
          '' , -- PopupFunctionName - varchar(100)
          0 , -- order - tinyint
          1 , -- tintRow - tinyint
          0 , -- tintColumn - tinyint
          1 , -- bitInResults - bit
          0 , -- bitDeleted - bit
          1 , -- bitDefault - bit
          NULL , -- bitSettingField - bit
          NULL , -- bitAddField - bit
          NULL , -- bitDetailField - bit
          NULL , -- bitAllowSorting - bit
          NULL , -- bitWorkFlowField - bit
          NULL , -- bitImport - bit
          NULL , -- bitExport - bit
          NULL , -- bitAllowFiltering - bit
          NULL , -- bitRequired - bit
          NULL , -- numFormFieldID - numeric
          NULL, -- intSectionID - int
          NULL  -- bitAllowGridColor - bit
        )


-----------------------------

-- Term Conditions
DECLARE @numMaxFieldId AS NUMERIC(18,0)
SELECT @numMaxFieldId = MAX(numFieldID) + 1 FROM [dbo].[DycFieldMaster] AS DFM
PRINT @numMaxFieldId

SET IDENTITY_INSERT [dbo].[DycFieldMaster] ON

INSERT INTO [dbo].[DycFieldMaster]
        ( [numFieldId],
		  [numModuleID] ,
          [numDomainID] ,
          [vcFieldName] ,
          [vcDbColumnName] ,
          [vcOrigDbColumnName] ,
          [vcPropertyName] ,
          [vcLookBackTableName] ,
          [vcFieldDataType] ,
          [vcFieldType] ,
          [vcAssociatedControlType] ,
          [vcToolTip] ,
          [vcListItemType] ,
          [numListID] ,
          [PopupFunctionName] ,
          [order] ,
          [tintRow] ,
          [tintColumn] ,
          [bitInResults] ,
          [bitDeleted] ,
          [bitAllowEdit] ,
          [bitDefault] ,
          [bitSettingField] ,
          [bitAddField] ,
          [bitDetailField] ,
          [bitAllowSorting] ,
          [bitWorkFlowField] ,
          [bitImport] ,
          [bitExport] ,
          [bitAllowFiltering] ,
          [bitInlineEdit] ,
          [bitRequired] ,
          [intColumnWidth] ,
          [intFieldMaxLength] ,
          [intWFCompare] ,
          [vcGroup] ,
          [vcWFCompareField]
        )
VALUES  ( @numMaxFieldId , -- numFieldID - numeric
		  4 , -- numModuleID - numeric
          NULL , -- numDomainID - numeric
          N'Term Conditions' , -- vcFieldName - nvarchar(50)
          N'vcTerms' , -- vcDbColumnName - nvarchar(50)
          N'vcTerms' , -- vcOrigDbColumnName - nvarchar(50)
          'Terms' , -- vcPropertyName - varchar(100)
          N'' , -- vcLookBackTableName - nvarchar(50)
          'V' , -- vcFieldDataType - char(1)
          'R' , -- vcFieldType - char(1)
          N'TextBox' , -- vcAssociatedControlType - nvarchar(50)
          N'Term Conditions' , -- vcToolTip - nvarchar(1000)
          '' , -- vcListItemType - varchar(3)
          0 , -- numListID - numeric
          NULL , -- PopupFunctionName - varchar(100)
          63 , -- order - tinyint
          NULL , -- tintRow - tinyint
          NULL , -- tintColumn - tinyint
          1 , -- bitInResults - bit
          0 , -- bitDeleted - bit
          1 , -- bitAllowEdit - bit
          1 , -- bitDefault - bit
          1 , -- bitSettingField - bit
          NULL , -- bitAddField - bit
          NULL , -- bitDetailField - bit
          1 , -- bitAllowSorting - bit
          NULL , -- bitWorkFlowField - bit
          NULL , -- bitImport - bit
          NULL , -- bitExport - bit
          1 , -- bitAllowFiltering - bit
          NULL , -- bitInlineEdit - bit
          NULL , -- bitRequired - bit
          NULL , -- intColumnWidth - int
          NULL , -- intFieldMaxLength - int
          NULL , -- intWFCompare - int
          NULL , -- vcGroup - varchar(100)
          NULL  -- vcWFCompareField - varchar(150)
        )

SET IDENTITY_INSERT [dbo].[DycFieldMaster] OFF

INSERT INTO [dbo].[DycFormField_Mapping]
        ( [numModuleID] ,
          [numFieldID] ,
          [numDomainID] ,
          [numFormID] ,
          [bitAllowEdit] ,
          [bitInlineEdit] ,
          [vcFieldName] ,
          [vcAssociatedControlType] ,
          [vcPropertyName] ,
          [PopupFunctionName] ,
          [order] ,
          [tintRow] ,
          [tintColumn] ,
          [bitInResults] ,
          [bitDeleted] ,
          [bitDefault] ,
          [bitSettingField] ,
          [bitAddField] ,
          [bitDetailField] ,
          [bitAllowSorting] ,
          [bitWorkFlowField] ,
          [bitImport] ,
          [bitExport] ,
          [bitAllowFiltering] ,
          [bitRequired] ,
          [numFormFieldID] ,
          [intSectionID] ,
          [bitAllowGridColor]
        )
VALUES  ( 4 , -- numModuleID - numeric
          @numMaxFieldId , -- numFieldID - numeric
          NULL , -- numDomainID - numeric
          126 , -- numFormID - numeric
          1 , -- bitAllowEdit - bit
          1 , -- bitInlineEdit - bit
          N'Term Conditions' , -- vcFieldName - nvarchar(50)
          N'TextBox' , -- vcAssociatedControlType - nvarchar(50)
          'Terms' , -- vcPropertyName - varchar(100)
          '' , -- PopupFunctionName - varchar(100)
          0 , -- order - tinyint
          1 , -- tintRow - tinyint
          0 , -- tintColumn - tinyint
          1 , -- bitInResults - bit
          0 , -- bitDeleted - bit
          1 , -- bitDefault - bit
          NULL , -- bitSettingField - bit
          NULL , -- bitAddField - bit
          NULL , -- bitDetailField - bit
          NULL , -- bitAllowSorting - bit
          NULL , -- bitWorkFlowField - bit
          NULL , -- bitImport - bit
          NULL , -- bitExport - bit
          NULL , -- bitAllowFiltering - bit
          NULL , -- bitRequired - bit
          NULL , -- numFormFieldID - numeric
          NULL, -- intSectionID - int
          NULL  -- bitAllowGridColor - bit
        )


---------- Added 2 fields for accounting audit facility ---------------------------

ALTER TABLE Subscribers ADD bitEnabledAccountingAudit BIT, bitEnabledNotifyAdmin BIT
UPDATE [dbo].[Subscribers] SET bitEnabledAccountingAudit = 1, bitEnabledNotifyAdmin = 1

ROLLBACK

------/******************************************************************
------Project: BACRMUI   Date: 16.FEB.2015
------Comments: Adding new Bizdoc "Customer Statement" / Adding New fields to Billing Terms

------*******************************************************************/
BEGIN TRANSACTION

ALTER TABLE [dbo].[BillingTerms] 
ADD numInterestPercentage NUMERIC(10,2), 
	numInterestPercentageIfPaidAfterDays NUMERIC(10,2),  
	numCreatedBy NUMERIC(18,0), 
	dtCreatedDate DATETIME, 
	numModifiedBy NUMERIC(18,0), 
	dtModifiedDate DATETIME

ROLLBACK


<add key="ReportPageSize" value="100"></add>
