/******************************************************************
Project: Release 10.7 Date: 19.DEC.2018
Comments: STORE PROCEDURES
*******************************************************************/


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Priya Sharma
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='Usp_CreateCloneShippingRule')
DROP PROCEDURE Usp_CreateCloneShippingRule
GO
CREATE PROCEDURE [dbo].[Usp_CreateCloneShippingRule]
(
	 @numRuleId BIGINT OUTPUT
	,@vcRuleName VARCHAR(50)
	,@numDomainID NUMERIC(18,0)
	,@numRelationship NUMERIC(18,0)
	,@numProfile NUMERIC(18,0)
)
AS 
BEGIN
BEGIN TRY
BEGIN TRANSACTION

	IF(SELECT COUNT(*) FROM dbo.ShippingRules WHERE vcRuleName = @vcRuleName AND numDomainId=@numDomainId) > 0
	BEGIN
 		RAISERROR ( 'DUPLICATE RULE',16, 1 )
 		RETURN ;
	END
	ELSE IF(SELECT 
				COUNT(*) 
			FROM 
				dbo.ShippingRules SR
			WHERE 
				numDomainId=@numDomainId 
				AND numRuleID <> ISNULL(@numRuleID,0) 
				AND ISNULL(numRelationship,0) = ISNULL(@numRelationship,0)
				AND ISNULL(numProfile,0) = ISNULL(@numProfile,0)
				AND EXISTS (SELECT 
								SRSL1.numRuleID
							FROM 
								ShippingRuleStateList SRSL1 
							INNER JOIN
								ShippingRuleStateList SRSL2
							ON
								ISNULL(SRSL1.numStateID,0) = ISNULL(SRSL2.numStateID,0)
								AND ISNULL(SRSL1.vcZipPostal,'') = ISNULL(SRSL2.vcZipPostal,'')
								AND ISNULL(SRSL1.numCountryID,0) = ISNULL(SRSL2.numCountryID,0)
							WHERE 
								SRSL1.numDomainID=@numDomainID 
								AND SRSL1.numRuleID = SR.numRuleID
								AND SRSL2.numRuleID = @numRuleID)
			) > 0
	BEGIN
 			RAISERROR ( 'DUPLICATE RELATIONSHIP, PROFILE AND STATE-ZIP',16, 1 )
 			RETURN ;
	END
	ELSE
	BEGIN
		DECLARE @numNewRuleID AS BIGINT
	
		INSERT INTO ShippingRules
		(vcRuleName,vcDescription,tintBasedOn,tinShippingMethod,tintIncludeType,numSiteID,numDomainID,tintFixedShippingQuotesMode,tintTaxMode,bitFreeShipping,FreeShippingOrderAmt,
		numRelationship,numProfile, numWareHouseID)
		SELECT @vcRuleName,vcDescription,tintBasedOn,tinShippingMethod,tintIncludeType,numSiteID,numDomainID,tintFixedShippingQuotesMode,tintTaxMode,bitFreeShipping,FreeShippingOrderAmt,
		@numRelationship,@numProfile, numWareHouseID
		FROM ShippingRules WHERE numRuleID = @numRuleId AND numDomainID = @numDomainID
	 
		SELECT @numNewRuleID = SCOPE_IDENTITY()
	 
		INSERT INTO dbo.ShippingServiceTypes 
		(numShippingCompanyID,vcServiceName,intNsoftEnum,intFrom,intTo,fltMarkup,bitMarkupType,monRate,bitEnabled,numRuleID,numDomainID)
		SELECT numShippingCompanyID,vcServiceName,intNsoftEnum,intFrom,intTo,fltMarkup,bitMarkupType,monRate,bitEnabled,@numNewRuleID,numDomainID
		FROM dbo.ShippingServiceTypes WHERE numRuleID = @numRuleId AND numDomainId = @numDomainId
	
		INSERT INTO dbo.ShippingRuleStateList( numRuleID,numCountryID,numStateID,numDomainID,tintMode,vcFromZip,vcToZip,vcZipPostal )
		SELECT @numNewRuleID, numCountryID,numStateID,numDomainID,tintMode,vcFromZip,vcToZip,vcZipPostal FROM ShippingRuleStateList WHERE numRuleID = @numRuleId

		SET @numRuleId = @numNewRuleID
	END
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH	
END
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_DeleteShippingRuleStateList' ) 
    DROP PROCEDURE USP_DeleteShippingRuleStateList
GO
CREATE PROCEDURE USP_DeleteShippingRuleStateList
    @numDomainID NUMERIC,
    @numCountryID NUMERIC,
	@numStateID NUMERIC,
	@numRuleId NUMERIC
AS 
BEGIN
 	
		DELETE FROM ShippingRuleStateList 
		WHERE numDomainID = @numDomainID AND numCountryID = @numCountryID 
		AND numStateID = @numStateID AND numRuleID = @numRuleId
 	
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
         
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetEstimateShipping')
DROP PROCEDURE USP_GetEstimateShipping
GO
CREATE PROCEDURE [dbo].[USP_GetEstimateShipping]
	@numDomainID NUMERIC(18,0)
AS 
BEGIN
	SELECT 
		numServiceTypeID
		,0 AS numRuleID
		,numShippingCompanyID
		,vcServiceName
		,monRate
		,intNsoftEnum
		,intFrom
		,intTo
		,fltMarkup
		,bitMarkUpType
		,bitEnabled
	FROM 
		ShippingServiceTypes 
	WHERE 
		numDomainID=@numDomainID 
		AND ISNULL(bitEnabled,0)=1
END      




SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
         
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetEstimateShippingEcommerce')
DROP PROCEDURE USP_GetEstimateShippingEcommerce
GO
CREATE PROCEDURE [dbo].[USP_GetEstimateShippingEcommerce]
	@numDomainID NUMERIC(18,0)
AS 
BEGIN
	IF @numDomainID IN (187,214)
	BEGIN
		SELECT 
			intNsoftEnum AS numServiceTypeID
			,0 AS numRuleID
			,numShippingCompanyID
			,vcServiceName
			,monRate
			,intNsoftEnum
			,intFrom
			,intTo
			,fltMarkup
			,bitMarkUpType
			,bitEnabled
		FROM 
			ShippingServiceTypes 
		WHERE 
			numDomainID=@numDomainID 
			AND ISNULL(bitEnabled,0)=1
		UNION
		SELECT 
			-1 numServiceTypeID
			,0 AS numRuleID
			,92 numShippingCompanyID
			,'Will call' vcServiceName
			,0 monRate
			,0 intNsoftEnum
			,0 intFrom
			,0 intTo
			,0 fltMarkup
			,0 bitMarkUpType
			,1 bitEnabled
	END
	ELSE
	BEGIN
		SELECT 
			numServiceTypeID
			,0 AS numRuleID
			,numShippingCompanyID
			,vcServiceName
			,monRate
			,intNsoftEnum
			,intFrom
			,intTo
			,fltMarkup
			,bitMarkUpType
			,bitEnabled
		FROM 
			ShippingServiceTypes 
		WHERE 
			numDomainID=@numDomainID 
			AND ISNULL(bitEnabled,0)=1
	END
END      
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetExistingAddress')
DROP PROCEDURE USP_GetExistingAddress
GO
CREATE PROCEDURE [dbo].[USP_GetExistingAddress]
@numDomainId as numeric(9),        
@numUserId as numeric(9),        
@numDivisionId as numeric(9) 
--@vcAddress varchar(500) output       
as   

declare @vcBillAddress	varchar(250)
declare @vcShipAddress	varchar(250)
declare @numBillAddress	NUMERIC(18);SET @numBillAddress=0
declare @numShipAddress	NUMERIC(18);SET @numShipAddress=0

          
Begin    
 
 DECLARE @numBillToCountry AS NUMERIC(10)
 DECLARE @numBillToState AS NUMERIC(10)
 DECLARE @vcBillToCity AS VARCHAR(100)
 DECLARE @vcBillToPostal AS VARCHAR(10)
 DECLARE @numShipToCountry AS NUMERIC(10)
 DECLARE @numShipToState AS NUMERIC(10)
 DECLARE @vcShipToCity AS VARCHAR(100)
 DECLARE @vcShipToPostal AS VARCHAR(10)
 DECLARE @numWarehouseID AS NUMERIC(18,0)
 
 SELECT @vcBillAddress = ISNULL(( vcStreet + ',' ), '') + ISNULL(( vcCity + ',' ), '')
        + isnull(dbo.fn_GetState(numState),'') + '||'
        + isnull(dbo.fn_GetListName(numCountry,0),'')  + ISNULL(( vcPostalCode ), ''),
        @numBillAddress=numAddressID,
        @numBillToCountry = ISNULL(numCountry,0) ,
        @numBillToState = ISNULL(numState,0) ,
        @vcBillToCity = ISNULL(vcCity,''),
        @vcBillToPostal = ISNULL(vcPostalCode,'')
 FROM   dbo.AddressDetails
 WHERE  numDomainID = @numDomainID
        AND numRecordID = @numDivisionID
        AND tintAddressOf = 2
        AND tintAddressType = 1
        AND bitIsPrimary = 1

	select @vcShipAddress= isnull((vcStreet + ','),'') + isnull((vcCity + ','),'') + 
			+ isnull(dbo.fn_GetState(numState),'') + '||' 
			+ isnull(dbo.fn_GetListName(numCountry,0),'') + isnull((vcPostalCode),''),
			@numShipAddress=numAddressID,
			@numShipToCountry = ISNULL(numCountry,0) ,
			@numShipToState = ISNULL(numState,0) ,
			@vcShipToCity = ISNULL(vcCity,''),
			@vcShipToPostal = ISNULL(vcPostalCode,'')
			,@numWarehouseID = ISNULL((SELECT TOP 1 numWarehouseID FROM Warehouses WHERE numDomainID=@numDomainID AND numAddressID=numAddressID),0)
			FROM   dbo.AddressDetails
 WHERE  numDomainID = @numDomainID
        AND numRecordID = @numDivisionID
        AND tintAddressOf = 2
        AND tintAddressType = 2
        AND bitIsPrimary = 1
	
	
	--set @vcAddress=(isnull(@vcBillAddress,''+ '||' + '' + '||' + '') + isnull(@vcShipAddress,''+ '||' + '' + '||' + '')); 
	SELECT ISNULL(@vcBillAddress,'||') AS vcBillAddress,
		   @numBillAddress AS numBillAddress,
		   ISNULL(@vcShipAddress,'||') AS vcShipAddress,
		   @numShipAddress AS numShipAddress,
		   @numBillToCountry AS numBillToCountry,
		   @numBillToState AS numBillToState,
		   @vcBillToCity AS vcBillToCity,
		   @vcBillToPostal AS vcBillToPostal,
		   @numShipToCountry AS numShipToCountry,
		   @numShipToState AS numShipToState,
		   @vcShipToCity AS vcShipToCity,
		   @vcShipToPostal AS vcShipToPostal,
		   dbo.fn_GetComapnyName(@numDivisionId) AS vcCompanyName,
		   @numWarehouseID AS numWarehouseID,
		  isnull(dbo.fn_GetState(@numShipToState),'') AS vcState
		   
End  


/****** Object:  StoredProcedure [dbo].[USP_GetOppWareHouseItemAttributes]    Script Date: 07/26/2008 16:18:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getoppwarehouseitemattributes')
DROP PROCEDURE usp_getoppwarehouseitemattributes
GO
CREATE PROCEDURE [dbo].[USP_GetOppWareHouseItemAttributes]
@numRecID as numeric(9)=0,
@bitSerialize as bit
AS
BEGIN
	IF ISNULL(@numRecID,0) > 0
	BEGIN
		SELECT dbo.fn_GetAttributes(@numRecID,@bitSerialize)
	END
	ELSE
	BEGIN
		SELECT ''
	END
END
GO
/****** Object:  StoredProcedure [dbo].[USP_GetOppWareHouseItemAttributes]    Script Date: 07/26/2008 16:18:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetOppWareHouseItemAttributesIds')
DROP PROCEDURE USP_GetOppWareHouseItemAttributesIds
GO
CREATE PROCEDURE [dbo].[USP_GetOppWareHouseItemAttributesIds]
@numRecID as numeric(9)=0
AS
BEGIN
	IF ISNULL(@numRecID,0) > 0
	BEGIN
		SELECT dbo.fn_GetAttributesIds(@numRecID)
	END
	BEGIN
		SELECT ''
	END
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getpromotiondrulefororder')
DROP PROCEDURE usp_getpromotiondrulefororder
GO
CREATE PROCEDURE [dbo].[USP_GetPromotiondRuleForOrder]
	 @numDomainID NUMERIC(18,0)
	,@numSubTotal FLOAT = 0
	,@numDivisionID NUMERIC(18,0)
AS
BEGIN	

	DECLARE @numProID AS NUMERIC

	SET @numProID = (
					SELECT TOP 1 PO.numProId		 
					FROM PromotionOffer PO	
					INNER JOIN PromotionOfferOrganizations PORG
					ON PO.numProId = PORG.numProId
					INNER JOIN CompanyInfo C
					ON PORG.numProfile = C.vcProfile and PORG.numRelationship = C.numCompanyType
					INNER JOIN DivisionMaster DM
					ON C.numCompanyId = DM.numCompanyID
					WHERE DM.numDivisionID = @numDivisionID
					AND PO.numDomainId = @numDomainID AND PO.IsOrderBasedPromotion = 1 AND ISNULL(bitEnabled,0) = 1
					AND  1 = (CASE WHEN ISNULL(PO.bitNeverExpires,0) = 1
								THEN 1
									WHEN ISNULL(PO.bitNeverExpires,0) = 0 
									THEN 
									CASE WHEN ((CAST(PO.dtValidFrom as DATE) <= CAST(GETDATE() as DATE) AND CAST(PO.dtValidTo as DATE) >= CAST(GETDATE() as DATE)))
									THEN 1	
									ELSE 0 END
								ELSE 0 END )
				)


	SELECT * FROM PromotionOffer WHERE numProId = @numProID 
	 	
	SELECT ROW_NUMBER() OVER(ORDER BY numProOfferOrderID ASC) AS RowNum, numOrderAmount,fltDiscountValue FROM PromotionOfferOrder WHERE numPromotionID = @numProID

END 
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetShippingRelationshipsProfiles' ) 
    DROP PROCEDURE USP_GetShippingRelationshipsProfiles
GO
CREATE PROCEDURE USP_GetShippingRelationshipsProfiles
   
    @numDomainID NUMERIC(9)
AS 
BEGIN
    SELECT DISTINCT
	(SELECT CONCAT( (SELECT vcData from ListDetails LD 
							JOIN  ShippingRules S ON LD.numListItemID = S.numRelationship
							WHERE LD.numListItemID = S.numRelationship AND S.numRuleID = SR.numRuleID ) , ' / ' , 
								(SELECT vcData from ListDetails LD 
								JOIN  ShippingRules S ON LD.numListItemID = S.numProfile
								WHERE LD.numListItemID = S.numProfile  AND S.numRuleID = SR.numRuleID )
							)) AS vcRelProfile,

	(SELECT CONCAT( (SELECT numListItemID from ListDetails LD 
							JOIN  ShippingRules S ON LD.numListItemID = S.numRelationship 
							WHERE LD.numListItemID = S.numRelationship AND S.numRuleID = SR.numRuleID) , '-' , 
								(SELECT numListItemID from ListDetails LD 
								JOIN  ShippingRules S ON LD.numListItemID = S.numProfile 
								WHERE LD.numListItemID = S.numProfile  AND S.numRuleID = SR.numRuleID)
							)) AS numRelProfileID

	FROM ShippingRules SR 
	WHERE numDomainID = @numDomainID AND SR.numRelationship > 0 AND SR.numProfile > 0
END
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetShippingRule' ) 
    DROP PROCEDURE USP_GetShippingRule
GO
CREATE PROCEDURE USP_GetShippingRule
    @numRuleID NUMERIC(9),
    @numDomainID NUMERIC(9),
    @byteMode AS TINYINT,
	@numRelProfileID AS VARCHAR(50)
AS 
BEGIN
    IF @byteMode = 0 
        BEGIN  
            SELECT  numRuleID
                    ,vcRuleName
                    ,vcDescription
                    ,tintBasedOn
                    ,tinShippingMethod
                    ,tintIncludeType
                    ,tintTaxMode
                    ,CASE WHEN tintFixedShippingQuotesMode = 0 THEN 1
                             ELSE tintFixedShippingQuotesMode END 
						AS tintFixedShippingQuotesMode
					,bitFreeShipping
					,FreeShippingOrderAmt
					,numRelationship
					,numProfile
					,numWareHouseID
                    
            FROM    dbo.ShippingRules SR
            WHERE   ( numRuleID = @numRuleID
                      OR @numRuleID = 0
                    )
                    AND numDomainID = @numDomainID
        END
    ELSE IF @byteMode = 1 
    BEGIN 
		DECLARE @numRelationship AS NUMERIC
		DECLARE @numProfile AS NUMERIC
				
		SET @numRelationship=(parsename(replace(@numRelProfileID,'-','.'),2))
		SET @numProfile=(parsename(replace(@numRelProfileID,'-','.'),1))

		SELECT minShippingCost,bitEnableStaticShippingRule FROM Domain WHERE numDomainID = @numDomainID

		IF @numRelationship > 0 AND @numProfile > 0
		BEGIN
			   SELECT  numRuleID,
                vcRuleName,
                tinShippingMethod,
                CASE WHEN tintTaxMode = 1 THEN 'Do Not Tax'
                        WHEN tintTaxMode = 2 THEN 'Tax when taxable items exists'
                        WHEN tintTaxMode = 3 THEN 'Always Tax'
                        WHEN tintTaxMode = 0 THEN 'Do Not Tax'
                        ELSE 'Tax Mode Not Selected'
                END vcTaxMode,
                CASE WHEN tintFixedShippingQuotesMode= 1 THEN 'Flat rate per item'
                        WHEN tintFixedShippingQuotesMode = 2 THEN 'Ship by weight'
                        WHEN tintFixedShippingQuotesMode = 3 THEN 'Ship by order total'
                        WHEN tintFixedShippingQuotesMode = 4 THEN 'Flat rate per order'
                        WHEN tintFixedShippingQuotesMode= 0 THEN 'Flat rate per item'
                        ELSE 'Fixed rate shipping quotes not selected'
                END  vcFixedShippingQuotesMode
				,(SELECT STUFF((SELECT ', ' + W.vcWareHouse
					FROM   Warehouses W			
					WHERE   W.numWareHouseID  in  ( SELECT  Id
							FROM    dbo.SplitIDs(SR.numWareHouseID, ','))			
					FOR XML PATH ('')),1,2,'')) AS vcShipFrom
                , (SELECT STUFF((SELECT ', ' + dbo.fn_GetState(numStateID) + ' ,' + SS.vcZipPostal
					FROM ShippingRuleStateList SS
					join ShippingRules S on ss.numRuleID = s.numRuleID
					WHERE SR.numRuleID = SS.numRuleID			
					FOR XML PATH ('')),1,2,'')) AS vcShipTo

					,(CASE WHEN ISNULL(SR.bitFreeShipping,0) = 1
						THEN  (SELECT CONCAT ( (SELECT STUFF((SELECT CONCAT (', ' , intfrom , ' - ', intTo, ' pays $',  CAST(monRate AS DECIMAL(20,2)))
								FROM ShippingServiceTypes SS
								join ShippingRules S on ss.numRuleID = s.numRuleID
								WHERE SR.numRuleID = SS.numRuleID			
								FOR XML PATH ('')),1,2,'')) , ' , Free shipping when order amount reaches $' , SR.FreeShippingOrderAmt))
					ELSE (SELECT STUFF((SELECT CONCAT (', ' , intfrom , ' - ', intTo, ' pays $',  CAST(monRate AS DECIMAL(20,2)))
								FROM ShippingServiceTypes SS
								join ShippingRules S on ss.numRuleID = s.numRuleID
								WHERE SR.numRuleID = SS.numRuleID			
								FOR XML PATH ('')),1,2,'')) 
					END ) AS vcShippingCharges
				, (SELECT CONCAT( (SELECT vcData from ListDetails LD 
					JOIN  ShippingRules S ON LD.numListItemID = S.numRelationship 
					WHERE LD.numListItemID = S.numRelationship AND S.numRuleID = SR.numRuleID) , ', ' , (SELECT vcData from ListDetails LD 
					JOIN  ShippingRules S ON LD.numListItemID = S.numProfile 
					WHERE LD.numListItemID = S.numProfile  AND S.numRuleID = SR.numRuleID))) AS vcAppliesTo	
                        
        FROM    dbo.ShippingRules SR
        WHERE  
                numDomainID = @numDomainID AND numRelationship = @numRelationship AND numprofile = @numProfile
		END

		ELSE
		BEGIN
			   SELECT  numRuleID,
                vcRuleName,
                tinShippingMethod,
                CASE WHEN tintTaxMode = 1 THEN 'Do Not Tax'
                        WHEN tintTaxMode = 2 THEN 'Tax when taxable items exists'
                        WHEN tintTaxMode = 3 THEN 'Always Tax'
                        WHEN tintTaxMode = 0 THEN 'Do Not Tax'
                        ELSE 'Tax Mode Not Selected'
                END vcTaxMode,
                CASE WHEN tintFixedShippingQuotesMode= 1 THEN 'Flat rate per item'
                        WHEN tintFixedShippingQuotesMode = 2 THEN 'Ship by weight'
                        WHEN tintFixedShippingQuotesMode = 3 THEN 'Ship by order total'
                        WHEN tintFixedShippingQuotesMode = 4 THEN 'Flat rate per order'
                        WHEN tintFixedShippingQuotesMode= 0 THEN 'Flat rate per item'
                        ELSE 'Fixed rate shipping quotes not selected'
                END  vcFixedShippingQuotesMode

				,(SELECT STUFF((SELECT ', ' + W.vcWareHouse
					FROM   Warehouses W			
					WHERE   W.numWareHouseID  in  ( SELECT  Id
							FROM    dbo.SplitIDs(SR.numWareHouseID, ','))			
					FOR XML PATH ('')),1,2,'')) AS vcShipFrom

                ,  
					STUFF((SELECT  ', ' + dbo.fn_GetListName(numCountryID,0) + ' - ' + 
						STUFF((SELECT ', ' + ISNULL(dbo.fn_GetState(numStateID),'All States') +
								(CASE WHEN LEN( STUFF((SELECT ',' + s.vcZipPostal From ShippingRuleStateList s
														WHERE SR.numRuleID = s.numRuleID AND s.numCountryID =SRSL.numCountryID and s.numStateID = SRSL.numStateID FOR XML PATH ('')),1,1,'')) > 0
						THEN CONCAT (' (',STUFF((SELECT ',' + s.vcZipPostal From ShippingRuleStateList s
															WHERE SR.numRuleID = s.numRuleID AND s.numCountryID =SRSL.numCountryID and s.numStateID = SRSL.numStateID FOR XML PATH ('')),1,1,''), ') ')
									ELSE ''
								END)
						From ShippingRuleStateList SRSL
						WHERE SR.numRuleID = SRSL.numRuleID AND SRSL.numCountryID =SS.numCountryID GROUP BY SRSL.numStateID,numCountryID FOR XML PATH ('')),1,1,'')
										
					FROM ShippingRuleStateList SS					
					WHERE SR.numRuleID = SS.numRuleID
					GROUP BY numCountryID
					FOR XML PATH ('')),1,1,'') AS vcShipTo

				,(CASE WHEN ISNULL(SR.bitFreeShipping,0) = 1
						THEN  (SELECT CONCAT ( (SELECT STUFF((SELECT CONCAT (', ' , intfrom , ' - ', intTo, ' pays $',  CAST(monRate AS DECIMAL(20,2)))
								FROM ShippingServiceTypes SS
								join ShippingRules S on ss.numRuleID = s.numRuleID
								WHERE SR.numRuleID = SS.numRuleID			
								FOR XML PATH ('')),1,2,'')) , ' , Free shipping when order amount reaches $' , SR.FreeShippingOrderAmt))
					ELSE (SELECT STUFF((SELECT CONCAT (', ' , intfrom , ' - ', intTo, ' pays $', CAST(monRate AS DECIMAL(20,2)))
								FROM ShippingServiceTypes SS
								join ShippingRules S on ss.numRuleID = s.numRuleID
								WHERE SR.numRuleID = SS.numRuleID			
								FOR XML PATH ('')),1,2,'')) 
					END ) AS vcShippingCharges
				,(SELECT CONCAT(ISNULL((SELECT vcData from ListDetails LD 
					JOIN  ShippingRules S ON LD.numListItemID = S.numRelationship 
					WHERE LD.numListItemID = S.numRelationship AND S.numRuleID = SR.numRuleID),'All Relationships') , ', ' ,ISNULL((SELECT vcData from ListDetails LD 
					JOIN  ShippingRules S ON LD.numListItemID = S.numProfile 
					WHERE LD.numListItemID = S.numProfile  AND S.numRuleID = SR.numRuleID),'All Profiles'))) AS vcAppliesTo	
                         
        FROM    dbo.ShippingRules SR
        WHERE  
                numDomainID = @numDomainID  
		END

    END 
      ELSE IF @byteMode = 2 --It is for calculating Tax on Shipping Charge (Use it later on Order Summery Page)
            BEGIN
				SELECT tintTaxMode FROM ShippingRules WHERE numRuleID = @numRuleID AND numDomainID = @numDomainID
			END
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getshippingruledetails')
DROP PROCEDURE usp_getshippingruledetails
GO
CREATE PROCEDURE [dbo].[USP_GetShippingRuleDetails]
	 @numDomainID NUMERIC(18,0)
	,@numWareHouseId VARCHAR(20)
	,@ZipCode VARCHAR(20)
	,@StateId NUMERIC(18,0)	
	,@numDivisionID NUMERIC(18,0)
	,@numSiteID NUMERIC(18,0)
	,@numCountryId NUMERIC(18,0)	
AS
BEGIN	
	DECLARE @numRuleID AS NUMERIC

	IF ISNULL(@numSiteID,0) > 0 AND ISNULL(@numDivisionID,0) = 0
	BEGIN
		DECLARE @numRelationship NUMERIC(18,0)
		DECLARE @numProfile NUMERIC(18,0)

		SELECT 
			@numRelationship=ISNULL(numRelationshipId,0)
			,@numProfile=ISNULL(numProfileId,0)
		FROM 
			eCommerceDTL 
		WHERE 
			numSiteId=@numSiteID

		SET @numRuleID = (SELECT TOP 1 
							SR.numRuleID		 
						FROM 
							ShippingRules SR						
						INNER JOIN 
							ShippingRuleStateList SRS
						ON 
							SR.numRuleID = SRS.numRuleID
						WHERE 
							(SR.numProfile = @numProfile OR ISNULL(SR.numProfile,0) = 0)
							AND (SR.numRelationship = @numRelationship OR ISNULL(SR.numRelationship,0) = 0)
							AND (CHARINDEX(@numWareHouseId, SR.numWareHouseID) > 0)
							AND SR.numDomainId = @numDomainID
							AND ISNULL(SRS.numCountryID,0)=ISNULL(@numCountryId,0)
							AND (SRS.numStateID = @StateId OR ISNULL(SRS.numStateID,0) = 0)
							AND (ISNULL(SRS.vcZipPostal,'') = '' OR (@ZipCode LIKE (SRS.vcZipPostal + '%')))
						ORDER BY
							(CASE 
								WHEN ISNULL(SRS.numCountryID,0) > 0 AND ISNULL(SRS.numStateID,0) > 0 AND LEN(ISNULL(SRS.vcZipPostal,'')) > 0 THEN 1
								WHEN ISNULL(SRS.numCountryID,0) > 0 AND ISNULL(SRS.numStateID,0) = 0 AND LEN(ISNULL(SRS.vcZipPostal,'')) > 0 THEN 2
								WHEN ISNULL(SRS.numCountryID,0) > 0 AND ISNULL(SRS.numStateID,0) > 0 AND LEN(ISNULL(SRS.vcZipPostal,'')) = 0 THEN 3
								WHEN ISNULL(SRS.numCountryID,0) > 0 AND ISNULL(SRS.numStateID,0) = 0 AND LEN(ISNULL(SRS.vcZipPostal,'')) = 0 THEN 4
								ELSE 5
							END),
							(CASE
								WHEN ISNULL(SR.numRelationship,0) > 0 AND ISNULL(SR.numProfile,0) > 0 THEN 1
								WHEN ISNULL(SR.numRelationship,0) > 0 AND ISNULL(SR.numProfile,0) = 0 THEN 2
								WHEN ISNULL(SR.numRelationship,0) = 0 AND ISNULL(SR.numProfile,0) > 0 THEN 3
								ELSE 4
							END),
							LEN(ISNULL(SRS.vcZipPostal,''))
					)
	END
	ELSE
	BEGIN
		SET @numRuleID = (SELECT TOP 1 
							SR.numRuleID		 
						FROM 
							ShippingRules SR	
						INNER JOIN 
							CompanyInfo C
						ON 
							(SR.numProfile = C.vcProfile OR ISNULL(SR.numProfile,0) = 0) 
							AND (SR.numRelationship = C.numCompanyType OR ISNULL(SR.numRelationship,0) = 0)					
						INNER JOIN 
							ShippingRuleStateList SRS
						ON 
							SR.numRuleID = SRS.numRuleID
						INNER JOIN 
							DivisionMaster DM
						ON 
							C.numCompanyId = DM.numCompanyID
						WHERE 
							DM.numDivisionID = @numDivisionID
							AND (CHARINDEX(@numWareHouseId, SR.numWareHouseID) > 0)
							AND SR.numDomainId = @numDomainID 
							AND ISNULL(SRS.numCountryID,0)=ISNULL(@numCountryId,0)
							AND (SRS.numStateID = @StateId OR ISNULL(SRS.numStateID,0) = 0)
							AND (ISNULL(SRS.vcZipPostal,'') = '' OR (@ZipCode LIKE (SRS.vcZipPostal + '%')))
						ORDER BY
							(CASE 
								WHEN ISNULL(SRS.numCountryID,0) > 0 AND ISNULL(SRS.numStateID,0) > 0 AND LEN(ISNULL(SRS.vcZipPostal,'')) > 0 THEN 1
								WHEN ISNULL(SRS.numCountryID,0) > 0 AND ISNULL(SRS.numStateID,0) = 0 AND LEN(ISNULL(SRS.vcZipPostal,'')) > 0 THEN 2
								WHEN ISNULL(SRS.numCountryID,0) > 0 AND ISNULL(SRS.numStateID,0) > 0 AND LEN(ISNULL(SRS.vcZipPostal,'')) = 0 THEN 3
								WHEN ISNULL(SRS.numCountryID,0) > 0 AND ISNULL(SRS.numStateID,0) = 0 AND LEN(ISNULL(SRS.vcZipPostal,'')) = 0 THEN 4
								ELSE 5
							END),
							(CASE
								WHEN ISNULL(SR.numRelationship,0) > 0 AND ISNULL(SR.numProfile,0) > 0 THEN 1
								WHEN ISNULL(SR.numRelationship,0) > 0 AND ISNULL(SR.numProfile,0) = 0 THEN 2
								WHEN ISNULL(SR.numRelationship,0) = 0 AND ISNULL(SR.numProfile,0) > 0 THEN 3
								ELSE 4
							END),
							LEN(ISNULL(SRS.vcZipPostal,''))
					)
	END

	SELECT * FROM ShippingRules WHERE numRuleID = @numRuleID 

	SELECT ROW_NUMBER() OVER(ORDER BY numServiceTypeID ASC) AS RowNum,
			CAST(monRate AS DECIMAL(20,2)) AS monRate,	
	 * FROM ShippingServiceTypes WHERE numRuleID = @numRuleID 
END 
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetShippingServiceTypes' ) 
    DROP PROCEDURE USP_GetShippingServiceTypes
GO
CREATE PROCEDURE USP_GetShippingServiceTypes
    @numDomainID NUMERIC,
    @numRuleID NUMERIC,
    @numServiceTypeID NUMERIC = 0,
    @tintMode TINYINT = 0
AS 
BEGIN
	
    --IF NOT EXISTS ( SELECT  *
    --                FROM    dbo.ShippingServiceTypes
    --                WHERE   numDomainID = @numDomainID
    --                        AND numRuleID = @numRuleID ) 
    --    BEGIN
    --        INSERT  INTO dbo.ShippingServiceTypes ( numShippingCompanyID,
				--									vcServiceName,
    --                                                intNsoftEnum,
    --                                                intFrom,
    --                                                intTo,
    --                                                fltMarkup,
    --                                                bitMarkupType,
    --                                                monRate,
    --                                                bitEnabled,
    --                                                numRuleID,
    --                                                numDomainID )
    --                SELECT  numShippingCompanyID,
				--			vcServiceName,
    --                        intNsoftEnum,
    --                        intFrom,
    --                        intTo,
    --                        fltMarkup,
    --                        bitMarkupType,
    --                        monRate,
    --                        bitEnabled,
    --                        @numRuleID,
    --                        @numDomainID
    --                FROM    dbo.ShippingServiceTypes
    --                WHERE   numDomainID = 0
    --    END

    IF @tintMode = 2 
    BEGIN

			IF NOT EXISTS ( SELECT  * FROM    dbo.ShippingServiceTypes
								WHERE numDomainID = @numDomainID
									AND intNsoftEnum > 0 AND ISNULL(numRuleID,0) = 0
						  ) 
				BEGIN
					INSERT INTO dbo.ShippingServiceTypes
					(	numShippingCompanyID,
						vcServiceName,
						intNsoftEnum,
						intFrom,
						intTo,
						fltMarkup,
						bitMarkupType,
						monRate,
						bitEnabled,
						numRuleID,
						numDomainID 
					)
					SELECT  numShippingCompanyID,
									vcServiceName,
									intNsoftEnum,
									intFrom,
									intTo,
									fltMarkup,
									bitMarkupType,
									monRate,
									bitEnabled,
									NULL,
									@numDomainID
					FROM    dbo.ShippingServiceTypes
					WHERE   numDomainID = 0 AND intNsoftEnum > 0 AND ISNULL(numRuleID,0) = 0
				END

            		SELECT DISTINCT * FROM (SELECT	numServiceTypeID,
										vcServiceName,
										intNsoftEnum,
										intFrom,
										intTo,
										fltMarkup,
										bitMarkupType,
										monRate,
										bitEnabled,
										numRuleID,
										numDomainID,
										numShippingCompanyID,
										CASE WHEN numShippingCompanyID = 91 THEN 'Fedex' 
												WHEN numShippingCompanyID = 88 THEN 'UPS'
												WHEN numShippingCompanyID = 90 THEN 'USPS'
												ELSE 'Other'
										END AS vcShippingCompany	 	
									FROM    dbo.ShippingServiceTypes
									WHERE   numDomainID = @numDomainID
											AND ISNULL(numRuleID,0) = 0
											AND ( numServiceTypeID = @numServiceTypeID
													OR @numServiceTypeID = 0
												)
												AND intNsoftEnum > 0
													
									UNION ALL
										
									SELECT	101,'Amazon Standard',101,0,0,0,1,0,1,0,0,0,'Amazon' vcShippingCompany	 	
										
									UNION ALL
										
									SELECT	102,'ShippingMethodStandard',102,0,0,0,1,0,1,0,0,0,'E-Bay' vcShippingCompany	 		
										
									UNION ALL
										
									SELECT	103,'Other',103,0,0,0,1,0,1,0,0,0,'E-Bay' vcShippingCompany	 	
													
									) TABLE1 WHERE (numServiceTypeID = @numServiceTypeID OR @numServiceTypeID = 0) ORDER BY vcShippingCompany
    END
      IF @tintMode=1
      BEGIN
	  	  SELECT  numServiceTypeID,
                    vcServiceName,
                    intNsoftEnum,
                    intFrom,
                    intTo,
                    fltMarkup,
                    bitMarkupType,
                   CAST(monRate AS DECIMAL(20,2)) AS monRate,
                    bitEnabled,
                    numRuleID,
                    numDomainID
            FROM    dbo.ShippingServiceTypes
            WHERE   numDomainID = @numDomainID
                    AND numRuleID = @numRuleID
                    AND ( numServiceTypeID = @numServiceTypeID
                          OR @numServiceTypeID = 0
                        )
                    AND ISNULL(intNsoftEnum,0)=0
	  END  
        

END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_insertupdatepromotionofferfororder')
DROP PROCEDURE usp_insertupdatepromotionofferfororder
GO
CREATE PROCEDURE [dbo].[USP_InsertUpdatePromotionOfferForOrder]
    @numProId AS NUMERIC(9) = 0,
    @vcProName AS VARCHAR(100),
    @numDomainId AS NUMERIC(18,0),
    @dtValidFrom AS DATE,
    @dtValidTo AS DATE,
	@bitNeverExpires BIT,
	@bitApplyToInternalOrders BIT,
	@bitAppliesToSite BIT,
	@bitRequireCouponCode BIT,
	@txtCouponCode VARCHAR(MAX),
	@tintUsageLimit TINYINT,

	--@tintOfferTriggerValueType TINYINT,
	--@fltOfferTriggerValue INT,
	--@tintOfferBasedOn TINYINT,
	--@tintDiscountType TINYINT,
	--@fltDiscountValue FLOAT,
	--@tintDiscoutBaseOn TINYINT,
	@numUserCntID NUMERIC(18,0),
	@tintCustomersBasedOn TINYINT,
	--@tintOfferTriggerValueTypeRange TINYINT,
	--@fltOfferTriggerValueRange FLOAT
	@strOrderAmount VARCHAR(MAX),
	@strfltDiscount VARCHAR(MAX),
	@tintDiscountType TINYINT
AS 
BEGIN

	IF @numProId = 0 
	BEGIN
		INSERT INTO PromotionOffer
        (
            vcProName
			,numDomainId
			,numCreatedBy
			,dtCreated
			,bitEnabled
			,IsOrderBasedPromotion 
        )
        VALUES  
		(
			@vcProName
           ,@numDomainId
           ,@numUserCntID
		   ,GETUTCDATE()
		   ,1
		   ,1
        )
            
		SELECT SCOPE_IDENTITY()
	END
	ELSE
	BEGIN	
	
	
--BEGIN TRY
--BEGIN TRANSACTION	

		--IF ISNULL(@bitAppliesToSite,0)=1 AND ISNULL(@numProId,0) > 0 AND (SELECT COUNT(*) FROM PromotionOfferSites WHERE numPromotionID=@numProId) = 0
		--BEGIN
		--	RAISERROR ( 'SELECT_SITES',16, 1 )
 	--		RETURN;
		--END

		--IF @tintOfferBasedOn <> 4
		--BEGIN
		--	IF (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numProId AND tintType=ISNULL(@tintOfferBasedOn,0) AND tintRecordType=5) = 0
		--	BEGIN
		--		RAISERROR ( 'SELECT_ITEMS_FOR_PROMOTIONS',16, 1 )
 	--			RETURN;
		--	END
		--END

		--IF ISNULL(@tintDiscoutBaseOn,0) <> 3 AND (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numProId AND tintType=ISNULL(@tintDiscoutBaseOn,0) AND tintRecordType=6) = 0
		--BEGIN
		--	RAISERROR ( 'SELECT_DISCOUNTED_ITEMS',16, 1 )
 	--		RETURN;
		--END

		/*Check For Duplicate Customer-Item relationship 
			1- The customer is specific (level 1), and the item is specific (also level 1). If the user tries to create another rule that targets the customer by name, or that same item by name, validation would say �You�ve already created a promotion rule that targets this customer and item by name�*/
		IF @tintCustomersBasedOn = 1
		BEGIN
			IF ISNULL(@tintCustomersBasedOn,0) = 1 AND (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId = @numProId AND tintType = 1) = 0
			BEGIN
				RAISERROR ( 'SELECT_INDIVIDUAL_CUSTOMER',16, 1 )
 				RETURN;
			END
		END
			
			/* 2- The customer is targeted at the group level (level 2), and the item is specific (level 1). If the user tries to create another rule that targets this same Relationship / Profile combination and that same item in level 1 (L2 customer and L1 item both match), validation would say �You�ve already created a promotion rule that targets this relationship / profile and one or more item specifically�*/
		ELSE IF @tintCustomersBasedOn = 2
		BEGIN
			IF ISNULL(@tintCustomersBasedOn,0) = 2 AND (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId = @numProId AND tintType = 2) = 0
			BEGIN
				RAISERROR ( 'SELECT_RELATIONSHIP_PROFILE',16, 1 )
 				RETURN;
			END
		END

		ELSE IF @tintCustomersBasedOn = 0
		BEGIN
			RAISERROR ( 'SELECT_CUSTOMERS',16, 1 )
 			RETURN;
		END

		IF @tintCustomersBasedOn = 3
		BEGIN
			DELETE FROM PromotionOfferorganizations 
				WHERE tintType IN (1, 2) 
					AND numProId = @numProId
		END

		IF (
			SELECT
				COUNT(*)
			FROM
				PromotionOffer
			LEFT JOIN
				PromotionOfferOrganizations
			ON
				PromotionOffer.numProId = PromotionOfferOrganizations.numProId
				AND 1 = (CASE 
							WHEN @tintCustomersBasedOn=1 THEN (CASE WHEN PromotionOfferOrganizations.tintType=1 THEN 1 ELSE 0 END) 
							WHEN @tintCustomersBasedOn=2 THEN (CASE WHEN PromotionOfferOrganizations.tintType=2 THEN 1 ELSE 0 END) 
							ELSE 1
						END)
			
			WHERE
				PromotionOffer.numDomainId=@numDomainID
				AND PromotionOffer.numProId <> @numProId
				AND tintCustomersBasedOn = @tintCustomersBasedOn AND ISNULL(bitEnabled,0) = 1			
				 
				AND 1 = (CASE 							
							WHEN @tintCustomersBasedOn=2 AND IsOrderBasedPromotion = 1 THEN
								(CASE 
									WHEN 
										(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId=@numProId AND POOInner.tintType=2 AND POOInner.numRelationship=PromotionOfferOrganizations.numRelationship AND POOInner.numProfile=PromotionOfferOrganizations.numProfile) > 0 
										
									THEN 
										1 
									ELSE 
										0 
								END)
							
						END)
			) > 0
		BEGIN
			DECLARE @ERRORMESSAGE AS VARCHAR(50)
			SET @ERRORMESSAGE = 'RELATIONSHIPPROFILE ALREADY EXISTS'							
					
			RAISERROR(@ERRORMESSAGE,16,1)
			RETURN

		END

		IF ISNULL(@bitAppliesToSite,0) = 0
		BEGIN
			DELETE FROM PromotionOfferSites WHERE numPromotionID=@numProId
		END

		UPDATE 
			PromotionOffer
		SET 
			vcProName = @vcProName
			,numDomainId = @numDomainId
			,dtValidFrom = @dtValidFrom
			,dtValidTo = @dtValidTo
			,bitNeverExpires = @bitNeverExpires
			,bitApplyToInternalOrders = @bitApplyToInternalOrders
			,bitAppliesToSite = @bitAppliesToSite
			,bitRequireCouponCode = @bitRequireCouponCode			
			,numModifiedBy = @numUserCntID
			,dtModified = GETUTCDATE()
			,tintCustomersBasedOn = @tintCustomersBasedOn
			,tintDiscountType = @tintDiscountType
		WHERE 
			numProId=@numProId

		IF (SELECT COUNT(*) FROM PromotionOfferOrder WHERE numPromotionID = @numProId AND numDomainId = @numDomainId) > 0
		BEGIN
			DELETE FROM PromotionOfferOrder WHERE numPromotionID = @numProId AND numDomainId = @numDomainId
		END

		DECLARE @Tempdata TABLE
		(
			numPromotionID NUMERIC,
			numDomainID NUMERIC,
			numOrderAmount VARCHAR(MAX),
			fltDiscountValue VARCHAR(MAX)
		)

		INSERT @Tempdata SELECT @numProId, @numDomainID, @strOrderAmount, @strfltDiscount

		;WITH tmp(numPromotionID, numDomainID, numOrderAmount, OrderAmount, numfltDiscountValue,fltDiscountValue) AS
		(
			SELECT
				numPromotionID,
				numDomainID,
				LEFT(numOrderAmount, CHARINDEX(',', numOrderAmount + ',') - 1),
				STUFF(numOrderAmount, 1, CHARINDEX(',', numOrderAmount + ','), ''),
				LEFT(fltDiscountValue, CHARINDEX(',', fltDiscountValue + ',') - 1),
				STUFF(fltDiscountValue, 1, CHARINDEX(',', fltDiscountValue + ','), '')
			FROM @Tempdata
			UNION all

			SELECT
				numPromotionID,
				numDomainID,
				LEFT(OrderAmount, CHARINDEX(',', OrderAmount + ',') - 1),
				STUFF(OrderAmount, 1, CHARINDEX(',', OrderAmount + ','), ''),
				LEFT(fltDiscountValue, CHARINDEX(',', fltDiscountValue + ',') - 1),
				STUFF(fltDiscountValue, 1, CHARINDEX(',', fltDiscountValue + ','), '')
			FROM tmp
			WHERE
				OrderAmount > '' AND fltDiscountValue > ''
		)

		INSERT INTO PromotionOfferOrder
		SELECT
			numPromotionID,				
			numOrderAmount,
			numfltDiscountValue,
			numDomainID

		FROM tmp
				
		--DROP TABLE Tempdata
---------------------------Insert Discount Codes-------------------------------------------------------------------

		DECLARE @CouponCountEntered AS NUMERIC = (SELECT COUNT(*) FROM DiscountCodes DC WHERE numPromotionID = @numProId )
		DECLARE @CouponCountINTable AS NUMERIC = (SELECT COUNT(*) FROM DiscountCodes DC 
										 WHERE numPromotionID = @numProId 
												AND vcDiscountCode IN  (
													SELECT  Id
													FROM    dbo.SplitIDs(@txtCouponCode, ',') )) 

	IF (@CouponCountEntered <> @CouponCountINTable )

	BEGIN

			DELETE DC FROM DiscountCodes DC			 
			 WHERE numPromotionID = @numProId 
					AND DC.numDiscountId NOT IN (SELECT numDiscountId FROM DiscountCodeUsage)
		
		IF @bitRequireCouponCode = 1
			BEGIN

				CREATE TABLE DiscountTempdata
				(
					numPromotionID NUMERIC,
					CodeUsageLimit TINYINT,
					vcDiscountCode VARCHAR(MAX)			
				)

				INSERT DiscountTempdata SELECT @numProId, @tintUsageLimit, @txtCouponCode

				;WITH tmp1(numPromotionID, CodeUsageLimit, vcDiscountCode, DiscountCode) AS
			(
				SELECT
					numPromotionID,
					CodeUsageLimit,
					LEFT(vcDiscountCode, CHARINDEX(',', vcDiscountCode + ',') - 1),
					STUFF(vcDiscountCode, 1, CHARINDEX(',', vcDiscountCode + ','), '')
				
				FROM DiscountTempdata
				UNION all

				SELECT
					numPromotionID,
					CodeUsageLimit,
					LEFT(DiscountCode, CHARINDEX(',', DiscountCode + ',') - 1),
					STUFF(DiscountCode, 1, CHARINDEX(',', DiscountCode + ','), '')
				FROM tmp1
				WHERE
					DiscountCode > '' 
			)	

			INSERT INTO DiscountCodes
			SELECT
				tmp1.numPromotionID,				
				tmp1.vcDiscountCode,
				tmp1.CodeUsageLimit
			FROM tmp1
			WHERE tmp1.vcDiscountCode NOT IN ( SELECT vcDiscountCode FROM DiscountCodes DC INNER JOIN DiscountCodeUsage DCU ON DC.numDiscountId = DCU.numDiscountId)
				
			DROP TABLE DiscountTempdata		

		END	
	END	
	
	ELSE
	BEGIN
		UPDATE DiscountCodes
		SET CodeUsageLimit = @tintUsageLimit
		WHERE numPromotionID = @numProId
	END
		       
        SELECT  @numProId

		---------------------------------------------------------------
--	COMMIT
--END TRY
--BEGIN CATCH
--	DECLARE @ErrorMessage1 NVARCHAR(4000)
--	DECLARE @ErrorNumber INT
--	DECLARE @ErrorSeverity INT
--	DECLARE @ErrorState INT
--	DECLARE @ErrorLine INT
--	DECLARE @ErrorProcedure NVARCHAR(200)

--	IF @@TRANCOUNT > 0
--		ROLLBACK TRANSACTION;

--	DELETE FROM PromotionOffer WHERE numProId = @numProId
--	DELETE FROM PromotionOfferOrder WHERE numPromotionID = @numProId
--	DELETE FROM DiscountCodes WHERE numPromotionID = @numProId
--	DELETE FROM PromotionOfferOrganizations WHERE numProId = @numProId


--	SELECT 
--		@ErrorMessage1 = ERROR_MESSAGE(),
--		@ErrorNumber = ERROR_NUMBER(),
--		@ErrorSeverity = ERROR_SEVERITY(),
--		@ErrorState = ERROR_STATE(),
--		@ErrorLine = ERROR_LINE(),
--		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

--	RAISERROR (@ErrorMessage1, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
--END CATCH


	END
END


GO
/****** Object:  StoredProcedure [dbo].[USP_ItemDetails]    Script Date: 02/07/2010 15:31:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Anoop Jayaraj                                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_itemdetails')
DROP PROCEDURE usp_itemdetails
GO
CREATE PROCEDURE [dbo].[USP_ItemDetails]                                                  
@numItemCode as numeric(9) ,          
@ClientTimeZoneOffset as int,
@numDivisionId as numeric(18)                                               
AS
BEGIN 
   DECLARE @bitItemIsUsedInOrder AS BIT=0
   DECLARE @bitOppOrderExists AS BIT = 0
   
   DECLARE @numCompanyID NUMERIC(18,0)=0
   IF ISNULL(@numDivisionId,0) > 0
   BEGIN
		SELECT @numCompanyID=numCompanyID FROM DivisionMaster WHERE numDivisionID=@numDivisionId
   END

   DECLARE @numDomainId AS NUMERIC
   DECLARE @numItemGroup AS NUMERIC(18,0)
   SELECT @numDomainId=numDomainId,@numItemGroup=numItemGroup FROM Item WHERE numItemCode=@numItemCode
   
	IF GETUTCDATE()<'2013-03-15 23:59:39'
		SET @bitItemIsUsedInOrder=0
	ELSE IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityItems OI WHERE OI.numItemCode =@numItemCode) > 0
	BEGIN
		SET @bitItemIsUsedInOrder=1
		SET @bitOppOrderExists = 1
	END
	ELSE IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityKitItems OI WHERE OI.numChildItemID =@numItemCode) > 0
	BEGIN
		SET @bitItemIsUsedInOrder=1
		SET @bitOppOrderExists = 1
	END
	ELSE IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityKitChildItems OI WHERE OI.numItemID =@numItemCode) > 0
	BEGIN
		SET @bitItemIsUsedInOrder=1
		SET @bitOppOrderExists = 1
	END
	ELSE IF EXISTS(SELECT numItemID FROM dbo.General_Journal_Details WHERE numItemID= @numItemCode and numDomainId=@numDomainId AND chBizDocItems='IA1'   )
		set @bitItemIsUsedInOrder =1
                                               
	SELECT 
		I.numItemCode, vcItemName, ISNULL(txtItemDesc,'') txtItemDesc,CAST(ISNULL(IED.txtDesc,'')  AS VARCHAR(MAX)) vcExtendedDescToAPI, charItemType, 
		CASE 
			WHEN charItemType='P' AND ISNULL(bitAssembly,0) = 1 then 'Assembly'
			WHEN charItemType='P' AND ISNULL(bitKitParent,0) = 1 then 'Kit'
			WHEN charItemType='P' AND ISNULL(bitAsset,0) = 1 AND ISNULL(bitRental,0)=0 then 'Asset'
			WHEN charItemType='P' AND ISNULL(bitAsset,0) = 1 AND ISNULL(bitRental,0)=1 then 'Rental Asset'
			WHEN charItemType='P' AND ISNULL(bitSerialized,0) = 1 AND ISNULL(bitAsset,0) = 0  then 'Serialized'
			WHEN charItemType='P' AND ISNULL(bitSerialized,0) = 1 AND ISNULL(bitAsset,0) = 1 AND ISNULL(bitRental,0)=0 then 'Serialized Asset'
			WHEN charItemType='P' AND ISNULL(bitSerialized,0) = 1 AND ISNULL(bitAsset,0) = 1 AND ISNULL(bitRental,0)=1 then 'Serialized Rental Asset'
			WHEN charItemType='P' AND ISNULL(bitLotNo,0)=1 THEN 'Lot #'
			ELSE CASE WHEN charItemType='P' THEN 'Inventory' ELSE '' END
		END AS InventoryItemType
		,dbo.fn_GetItemChildMembershipCount(@numDomainId,I.numItemCode) AS numChildMembershipCount
		,CASE WHEN ISNULL(bitAssembly,0) = 1 THEN dbo.fn_GetAssemblyPossibleWOQty(I.numItemCode,ISNULL(W.numWareHouseID,0)) ELSE 0 END AS numWOQty
		,CASE WHEN charItemType='P' THEN ISNULL((SELECT TOP 1 monWListPrice FROM WarehouseItems WHERE numDomainID=@numDomainID AND numItemID=@numItemCode AND ISNULL(monWListPrice,0) > 0),0) ELSE monListPrice END monListPrice,                   
		numItemClassification, isnull(bitTaxable,0) as bitTaxable, ISNULL(vcSKU,'') AS vcItemSKU, 
		(CASE WHEN I.numItemGroup > 0 THEN ISNULL(W.[vcWHSKU],'') ELSE ISNULL(vcSKU,'') END) AS vcSKU, 
		ISNULL(bitKitParent,0) as bitKitParent, V.numVendorID, V.monCost, I.numDomainID, I.numCreatedBy, I.bintCreatedDate, I.bintModifiedDate,I.numModifiedBy,
		(SELECT TOP 1 vcPathForImage FROM ItemImages WHERE numItemCode = @numItemCode AND bitDefault = 1 AND bitIsImage = 1 ) AS vcPathForImage ,
		(SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = @numItemCode AND bitDefault = 1 AND bitIsImage = 1) AS  vcPathForTImage
		,ISNULL(bitSerialized,0) as bitSerialized, vcModelID,                   
		(SELECT 
			numItemImageId
			,vcPathForImage
			,vcPathForTImage
			,bitDefault
			,CASE WHEN bitdefault=1 THEN -1 ELSE ISNULL(intDisplayOrder,0) END AS intDisplayOrder 
		FROM 
			ItemImages  
		WHERE 
			numItemCode=@numItemCode 
		ORDER BY 
			CASE WHEN bitdefault=1 THEN -1 ELSE isnull(intDisplayOrder,0) END ASC 
		FOR XML AUTO,ROOT('Images')) AS xmlItemImages,
		(SELECT STUFF((SELECT ',' + CONVERT(VARCHAR(50) , numCategoryID)  FROM dbo.ItemCategory WHERE numItemID = I.numItemCode FOR XML PATH('')), 1, 1, '')) AS vcItemCategories ,
		numItemGroup, 
		(CASE WHEN ISNULL(numItemGroup,0) > 0 AND (SELECT COUNT(*) FROM CFW_Fld_Master CFW JOIN ItemGroupsDTL ON numOppAccAttrID=Fld_id WHERE numItemGroupID=numItemGroup AND tintType=2) > 0 THEN 1 ELSE 0 END) bitItemGroupHasAttributes,
		numCOGsChartAcntId, numAssetChartAcntId, numIncomeChartAcntId, ISNULL(bitExpenseItem,0) bitExpenseItem, ISNULL(numExpenseChartAcntId,0) numExpenseChartAcntId, (CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE monAverageCost END) AS monAverageCost,                   
		monCampaignLabourCost,dbo.fn_GetContactName(I.numCreatedBy)+' ,'+convert(varchar(20),DateAdd(minute, -@ClientTimeZoneOffset,I.bintCreatedDate )) as CreatedBy ,                                      
		dbo.fn_GetContactName(I.numModifiedBy)+' ,'+convert(varchar(20),DateAdd(minute, -@ClientTimeZoneOffset,I.bintModifiedDate )) as ModifiedBy,                      
		sum(numOnHand) as numOnHand,                      
		sum(numOnOrder) as numOnOrder,                      
		MAX(numReorder)  as numReOrder,                      
		sum(numAllocation)  as numAllocation,                      
		sum(numBackOrder)  as numBackOrder,                   
		isnull(fltWeight,0) as fltWeight,                
		isnull(fltHeight,0) as fltHeight,                
		isnull(fltWidth,0) as fltWidth,                
		isnull(fltLength,0) as fltLength,                
		isnull(bitFreeShipping,0) as bitFreeShipping,              
		isnull(bitAllowBackOrder,0) as bitAllowBackOrder,              
		isnull(vcUnitofMeasure,'Units') as vcUnitofMeasure,      
		isnull(bitShowDeptItem,0) bitShowDeptItem,      
		isnull(bitShowDeptItemDesc,0) bitShowDeptItemDesc,  
		isnull(bitCalAmtBasedonDepItems ,0) bitCalAmtBasedonDepItems,
		isnull(bitAssembly ,0) bitAssembly ,
		isnull(cast(numBarCodeId as varchar(50)),'') as  numBarCodeId ,
		(CASE WHEN ISNULL(I.numManufacturer,0) > 0 THEN ISNULL((SELECT vcCompanyName FROM DivisionMaster INNER JOIN CompanyInfo ON DivisionMaster.numCompanyID=CompanyInfo.numCompanyId WHERE numDivisionID=I.numManufacturer),'')  ELSE ISNULL(I.vcManufacturer,'') END) AS vcManufacturer,
		ISNULL(I.numManufacturer,0) numManufacturer,
		ISNULL(I.numBaseUnit,0) AS numBaseUnit,ISNULL(I.numPurchaseUnit,0) AS numPurchaseUnit,ISNULL(I.numSaleUnit,0) AS numSaleUnit,
		dbo.fn_GetUOMName(ISNULL(I.numBaseUnit,0)) AS vcBaseUnit,dbo.fn_GetUOMName(ISNULL(I.numPurchaseUnit,0)) AS vcPurchaseUnit,dbo.fn_GetUOMName(ISNULL(I.numSaleUnit,0)) AS vcSaleUnit,
		isnull(bitLotNo,0) as bitLotNo,
		ISNULL(IsArchieve,0) AS IsArchieve,
		case when charItemType='P' then 'Inventory Item' when charItemType='S' then 'Service' when charItemType='A' then 'Accessory' when charItemType='N' then 'Non-Inventory Item' end as ItemType,
		isnull(I.numItemClass,0) as numItemClass,
		ISNULL(tintStandardProductIDType,0) tintStandardProductIDType,
		ISNULL(vcExportToAPI,'') vcExportToAPI,
		ISNULL(numShipClass,0) numShipClass,ISNULL(I.bitAllowDropShip,0) AS bitAllowDropShip,
		ISNULL(@bitItemIsUsedInOrder,0) AS bitItemIsUsedInOrder,
		ISNULL((SELECT COUNT(*) FROM ItemDetails WHERE numChildItemID = I.numItemCode),0) AS intParentKitItemCount,
		ISNULL(I.bitArchiveItem,0) AS [bitArchiveItem],
		ISNULL(W.numWareHouseID,0) AS [numWareHouseID],
		ISNULL(W.numWareHouseItemID,0) AS [numWareHouseItemID],
		ISNULL((SELECT COUNT(WI.[numWareHouseItemID]) FROM [dbo].[WareHouseItems] WI WHERE WI.[numItemID] = I.numItemCode AND WI.[numItemID] = @numItemCode),0) AS [intTransCount],
		ISNULL(I.bitAsset,0) AS [bitAsset],
		ISNULL(I.bitRental,0) AS [bitRental],
		ISNULL(W.vcBarCode,ISNULL(I.[numBarCodeId],'')) AS [vcBarCode],
		ISNULL((SELECT COUNT(*) FROM ItemUOMConversion WHERE numItemCode=I.numItemCode),0) AS tintUOMConvCount,
		ISNULL(I.bitVirtualInventory,0) bitVirtualInventory ,
		ISNULL(I.bitContainer,0) bitContainer
		,ISNULL(I.numContainer,0) numContainer
		,ISNULL(I.numNoItemIntoContainer,0) numNoItemIntoContainer
		,ISNULL(I.bitMatrix,0) AS bitMatrix
		,ISNULL(@bitOppOrderExists,0) AS bitOppOrderExists
		,CPN.CustomerPartNo
		,ISNULL(I.vcASIN,'') AS vcASIN
		,ISNULL(I.tintKitAssemblyPriceBasedOn,1) AS tintKitAssemblyPriceBasedOn
		,ISNULL(I.fltReorderQty,0) AS fltReorderQty
		,ISNULL(bitSOWorkOrder,0) bitSOWorkOrder
		,ISNULL(bitKitSingleSelect,0) bitKitSingleSelect
	FROM 
		Item I       
	left join  
		WareHouseItems W                  
	on 
		W.numItemID=I.numItemCode                
	LEFT JOIN 
		ItemExtendedDetails IED 
	ON 
		I.numItemCode = IED.numItemCode
	LEFT JOIN 
		Vendor V 
	ON 
		I.numVendorID = V.numVendorID AND I.numItemCode = V.numItemCode  
	LEFT JOIN 
		CustomerPartNumber CPN 
	ON 
		I.numItemCode=CPN.numItemCode 
		AND CPN.numCompanyID=@numCompanyID
	WHERE 
		I.numItemCode=@numItemCode  
	GROUP BY 
		I.numItemCode, vcItemName, txtItemDesc, charItemType, monListPrice,                   
		numItemClassification, bitTaxable,vcSKU,[W].[vcWHSKU] , bitKitParent,V.numVendorID, V.monCost, I.numDomainID,               
		I.numCreatedBy, I.bintCreatedDate, I.bintModifiedDate,numContainer,   numNoItemIntoContainer,                
		I.numModifiedBy,bitAllowBackOrder, bitSerialized, vcModelID,                   
		numItemGroup, numCOGsChartAcntId, numAssetChartAcntId, numIncomeChartAcntId, monAverageCost, I.bitVirtualInventory, bitExpenseItem, numExpenseChartAcntId,                  
		monCampaignLabourCost,fltWeight,fltHeight,fltWidth,fltLength,bitFreeShipping,vcUnitofMeasure,bitShowDeptItemDesc,bitShowDeptItem,bitCalAmtBasedonDepItems,bitAssembly,numBarCodeId,I.vcManufacturer
		,I.numBaseUnit,I.numPurchaseUnit,I.numSaleUnit,I.bitContainer,I.bitLotNo,I.IsArchieve,I.numItemClass,I.tintStandardProductIDType,I.vcExportToAPI,I.numShipClass,
		CAST( ISNULL(IED.txtDesc,'')  AS VARCHAR(MAX) ),I.bitAllowDropShip,I.bitArchiveItem,I.bitAsset,I.bitRental,W.[numWarehouseID],W.numWareHouseItemID,W.[vcBarCode],I.bitMatrix
		,I.numManufacturer,CPN.CustomerPartNo,I.vcASIN,I.tintKitAssemblyPriceBasedOn,I.fltReorderQty,bitSOWorkOrder,bitKitSingleSelect
END
--SELECT * FROM Category
--- Alterd By Chintan Prajapati
-- EXEC dbo.USP_ItemsForECommerce 9,60,1,1,15,0,''
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ItemsForECommerce' ) 
    DROP PROCEDURE USP_ItemsForECommerce
GO
CREATE PROCEDURE [dbo].[USP_ItemsForECommerce]
    @numSiteID AS NUMERIC(9) = 0,
    @numCategoryID AS NUMERIC(18) = 0,
    @numWareHouseID AS NUMERIC(9) = 0,
    @SortBy NVARCHAR(MAX) = 0,
    @CurrentPage INT,
    @PageSize INT,
    @TotRecs INT = 0 OUTPUT,
    @SearchText AS NVARCHAR(MAX) = '',--Comma seperated item ids
    @FilterQuery AS NVARCHAR(MAX) = '',----Comma seperated Attributes Ids
    @FilterRegularCondition AS NVARCHAR(MAX),
    @FilterCustomFields AS NVARCHAR(MAX),
    @FilterCustomWhere AS NVARCHAR(MAX),
	@numDivisionID AS NUMERIC(18,0) = 0,
	@numManufacturerID AS NUMERIC(18,0)=0,
	@FilterItemAttributes AS NVARCHAR(MAX)
   
AS 
BEGIN

		
		
		DECLARE @numDomainID AS NUMERIC(9)
		DECLARE @numDefaultRelationship AS NUMERIC(18,0)
		DECLARE @numDefaultProfile AS NUMERIC(18,0)
		DECLARE @tintPreLoginPriceLevel AS TINYINT
		DECLARE @tintPriceLevel AS TINYINT

		SELECT  @numDomainID = numDomainID
        FROM    [Sites]
        WHERE   numSiteID = @numSiteID
        
        DECLARE @tintDisplayCategory AS TINYINT
		
		IF ISNULL(@numDivisionID,0) > 0
		BEGIN
			SELECT @numDefaultRelationship=ISNULL(numCompanyType,0),@numDefaultProfile=ISNULL(vcProfile,0),@tintPriceLevel=ISNULL(tintPriceLevel,0) FROM DivisionMaster INNER JOIN CompanyInfo ON DivisionMaster.numCompanyID=CompanyInfo.numCompanyId WHERE numDivisionID=@numDivisionID AND DivisionMaster.numDomainID=@numDomainID
		END

		--KEEP THIS BELOW ABOVE IF CONDITION
		SELECT 
			@tintDisplayCategory = ISNULL(bitDisplayCategory,0)
			,@numDefaultRelationship=(CASE WHEN ISNULL(@numDivisionID,0) > 0 THEN @numDefaultRelationship ELSE numRelationshipId END)
			,@numDefaultProfile=(CASE WHEN ISNULL(@numDivisionID,0) > 0 THEN @numDefaultProfile ELSE numProfileId END)
			,@tintPreLoginPriceLevel=(CASE WHEN ISNULL(bitShowPriceUsingPriceLevel,0)=1 THEN (CASE WHEN @numDivisionID > 0 THEN (CASE WHEN ISNULL(bitShowPriceUsingPriceLevel,0)=1 THEN ISNULL(@tintPriceLevel,0) ELSE 0 END) ELSE ISNULL(tintPreLoginProceLevel,0) END) ELSE 0 END)
		FROM 
			dbo.eCommerceDTL 
		WHERE 
			numSiteID = @numSiteId
		
		CREATE TABLE #tmpItemCat(numCategoryID NUMERIC(18,0))
		CREATE TABLE #tmpItemCode(numItemCode NUMERIC(18,0))
		IF LEN(@FilterCustomFields) > 0 AND LEN(@FilterCustomWhere) > 0
		BEGIN			
			DECLARE @FilterCustomFieldIds AS NVARCHAR(MAX)
			SET @FilterCustomFieldIds = Replace(@FilterCustomFields,'[','')
			SET @FilterCustomFieldIds = Replace(@FilterCustomFieldIds,']','')
			
			DECLARE @strCustomSql AS NVARCHAR(MAX)
			SET @strCustomSql = 'INSERT INTO #tmpItemCode(numItemCode)
								 SELECT DISTINCT T.RecId FROM 
															(
																SELECT * FROM 
																	(
																		SELECT  t2.RecId, t1.fld_id, t2.Fld_Value FROM CFW_Fld_Master t1 
																		JOIN CFW_FLD_Values_Item AS t2 ON t1.Fld_ID = t2.Fld_ID 
																		WHERE t1.Grp_id  In (5,9)
																		AND t1.numDomainID = ' + CONVERT(VARCHAR(10),@numDomainID) + '
																		AND t1.fld_type <> ''Link'' 
																		AND t1.fld_id IN (' + @FilterCustomFieldIds + ')
																	) p  PIVOT (MAX([Fld_Value]) FOR fld_id IN (' + @FilterCustomFields + ')) AS pvt 
															) T  WHERE 1=1 AND ' + @FilterCustomWhere
								 
			EXEC SP_EXECUTESQL @strCustomSql								 					 
		END
		
        DECLARE @strSQL NVARCHAR(MAX)
        DECLARE @firstRec AS INTEGER
        DECLARE @lastRec AS INTEGER
        DECLARE @Where NVARCHAR(MAX)
        DECLARE @SortString NVARCHAR(MAX)
        DECLARE @searchPositionColumn NVARCHAR(MAX)
		DECLARE @searchPositionColumnGroupBy NVARCHAR(MAX)
        DECLARE @row AS INTEGER
        DECLARE @data AS VARCHAR(100)
        DECLARE @dynamicFilterQuery NVARCHAR(MAX)
        DECLARE @checkFldType VARCHAR(100)
        DECLARE @count NUMERIC(9,0)
        DECLARE @whereNumItemCode NVARCHAR(MAX)
        DECLARE @filterByNumItemCode VARCHAR(100)
        
		SET @SortString = ''
        set @searchPositionColumn = ''
		SET @searchPositionColumnGroupBy = ''
        SET @strSQL = ''
		SET @whereNumItemCode = ''
		SET @dynamicFilterQuery = ''
		 
        SET @firstRec = ( @CurrentPage - 1 ) * @PageSize
        SET @lastRec = ( @CurrentPage * @PageSize + 1 )
                
        DECLARE @bitAutoSelectWarehouse AS BIT
		SELECT @bitAutoSelectWarehouse = ISNULL([ECD].[bitAutoSelectWarehouse],0) FROM [dbo].[eCommerceDTL] AS ECD WHERE [ECD].[numSiteId] = @numSiteID
		AND [ECD].[numDomainId] = @numDomainID

		DECLARE @vcWarehouseIDs AS VARCHAR(1000)
        IF (@numWareHouseID = 0 OR @bitAutoSelectWarehouse = 1)
            BEGIN				
				IF @bitAutoSelectWarehouse = 1
					BEGIN
						SELECT @vcWarehouseIDs = ISNULL( STUFF((SELECT DISTINCT ',' + CAST(numWarehouseID AS VARCHAR(10)) FROM [dbo].[Warehouses] WHERE [Warehouses].[numDomainID] = @numDomainID FOR XML PATH('')),1, 1, '') , '') 
					END
				
				ELSE
					BEGIN
						SELECT  @numWareHouseID = ISNULL(numDefaultWareHouseID,0)
						FROM    [eCommerceDTL]
						WHERE   [numDomainID] = @numDomainID	

						SET @vcWarehouseIDs = @numWareHouseID
					END
            END
		ELSE
		BEGIN
			SET @vcWarehouseIDs = @numWareHouseID
		END

		--PRIAMRY SORTING USING CART DROPDOWN
		DECLARE @Join AS NVARCHAR(MAX)
		DECLARE @SortFields AS NVARCHAR(MAX)
		DECLARE @vcSort AS VARCHAR(MAX)

		SET @Join = ''
		SET @SortFields = ''
		SET @vcSort = ''

		IF @SortBy = ''
			BEGIN
				SET @SortString = ' vcItemName Asc '
			END
		ELSE IF CHARINDEX('~0',@SortBy) > 0 OR CHARINDEX('~1',@SortBy) > 0
			BEGIN
				DECLARE @fldID AS NUMERIC(18,0)
				DECLARE @RowNumber AS VARCHAR(MAX)
				
				SELECT * INTO #fldValues FROM dbo.SplitIDsWithPosition(@SortBy,'~')
				SELECT @RowNumber = RowNumber, @fldID = id FROM #fldValues WHERE RowNumber = 1
				SELECT @vcSort = CASE WHEN Id = 0 THEN 'ASC' ELSE 'DESC' END FROM #fldValues WHERE RowNumber = 2
				
				SET @Join = @Join + ' LEFT JOIN CFW_FLD_Values_Item CFVI' + @RowNumber + ' ON I.numItemCode = CFVI' + @RowNumber + '.RecID AND CFVI' + @RowNumber + '.fld_Id = ' + CONVERT(VARCHAR(10),@fldID)
				---SET @SortString = 'CFVI' + @RowNumber + '.fld_Value' 
				SET @SortString = '[fld_Value' + @RowNumber + '] ' --+ @vcSort
				SET @SortFields = ', CFVI' + @RowNumber + '.fld_Value [fld_Value' + @RowNumber + ']'
			END
		ELSE
			BEGIN
				--SET @SortString = @SortBy	
				
				IF CHARINDEX('monListPrice',@SortBy) > 0
				BEGIN
					DECLARE @bitSortPriceMode AS BIT
					SELECT @bitSortPriceMode = ISNULL(bitSortPriceMode,0) FROM dbo.eCommerceDTL WHERE numSiteId = @numSiteID
			
					IF @bitSortPriceMode = 1
					BEGIN
						SET @SortString = ' '
					END
					ELSE
					BEGIN
						SET @SortString = @SortBy	
					END
				END
				ELSE
				BEGIN
					SET @SortString = @SortBy	
				END
				
			END	

		--SECONDARY SORTING USING DEFAULT SETTINGS FROM ECOMMERCE
		SELECT ROW_NUMBER() OVER (ORDER BY Custom,vcFieldName) AS [RowID],* INTO #tempSort FROM (
		SELECT  CONVERT(VARCHAR(9),numFieldID)+'~0' AS numFieldID,
				ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName,
				0 AS Custom,
				vcDbColumnName 
		FROM View_DynamicColumns
		WHERE numFormID = 84 
		AND ISNULL(bitSettingField,0) = 1 
		AND ISNULL(bitDeleted,0)=0 
		AND ISNULL(bitCustom,0) = 0
		AND numDomainID = @numDomainID
					       
		UNION     

		SELECT  CONVERT(VARCHAR(9),numFieldID)+'~1' AS numFieldID ,
				vcFieldName ,
				1 AS Custom,
				'' AS vcDbColumnName
		FROM View_DynamicCustomColumns          
		WHERE numFormID = 84
		AND numDomainID = @numDomainID
		AND grp_id IN (5,9)
		AND vcAssociatedControlType = 'TextBox' 
		AND ISNULL(bitCustom,0) = 1 
		AND tintPageType=1  
		) TABLE1 ORDER BY Custom,vcFieldName
		
		--SELECT * FROM #tempSort
		DECLARE @DefaultSort AS NVARCHAR(MAX)
		DECLARE @DefaultSortFields AS NVARCHAR(MAX)
		DECLARE @DefaultSortJoin AS NVARCHAR(MAX)
		DECLARE @intCnt AS INT
		DECLARE @intTotalCnt AS INT	
		DECLARE @strColumnName AS VARCHAR(100)
		DECLARE @bitCustom AS BIT
		DECLARE @FieldID AS VARCHAR(100)				

		SET @DefaultSort = ''
		SET @DefaultSortFields = ''
		SET @DefaultSortJoin = ''		
		SET @intCnt = 0
		SET @intTotalCnt = ( SELECT COUNT(*) FROM #tempSort )
		SET @strColumnName = ''
		
		IF (SELECT ISNULL(bitEnableSecSorting,0) FROM EcommerceDtl WHERE numSiteID = @numSiteID ) = 1
		BEGIN
			CREATE TABLE #fldDefaultValues(RowNumber INT,Id VARCHAR(1000))

			WHILE(@intCnt < @intTotalCnt)
			BEGIN
				SET @intCnt = @intCnt + 1
				SELECT @FieldID = numFieldID, @strColumnName = vcDbColumnName, @bitCustom = Custom FROM #tempSort WHERE RowID = @intCnt

				IF @bitCustom = 0
					BEGIN
						SET @DefaultSort =  @strColumnName + ',' + @DefaultSort 
					END

				ELSE
					BEGIN
						DECLARE @fldID1 AS NUMERIC(18,0)
						DECLARE @RowNumber1 AS VARCHAR(MAX)
						DECLARE @vcSort1 AS VARCHAR(10)
						DECLARE @vcSortBy AS VARCHAR(1000)
						SET @vcSortBy = CONVERT(VARCHAR(100),@FieldID) + '~' + '0'

						INSERT INTO #fldDefaultValues(RowNumber,Id) SELECT RowNumber, id FROM dbo.SplitIDsWithPosition(@vcSortBy,'~')
						SELECT @RowNumber1 = RowNumber + 100 + @intCnt, @fldID1 = id FROM #fldDefaultValues WHERE RowNumber = 1
						SELECT @vcSort1 = 'ASC' 
						
						SET @DefaultSortJoin = @DefaultSortJoin + ' LEFT JOIN CFW_FLD_Values_Item CFVI' + @RowNumber1 + ' ON I.numItemCode = CFVI' + @RowNumber1 + '.RecID AND CFVI' + @RowNumber1 + '.fld_Id = ' + CONVERT(VARCHAR(10),@fldID1)
						SET @DefaultSort = '[fld_Value' + @RowNumber1 + '] ' + ',' + @DefaultSort 
						SET @DefaultSortFields = @DefaultSortFields + ', CFVI' + @RowNumber1 + '.fld_Value [fld_Value' + @RowNumber1 + ']'
					END

			END
			IF LEN(@DefaultSort) > 0
			BEGIN
				SET  @DefaultSort = LEFT(@DefaultSort,LEN(@DefaultSort) - 1)
				SET @SortString = @SortString + ',' 
			END
		END
--        IF @SortBy = 1 
--            SET @SortString = ' vcItemName Asc '
--        ELSE IF @SortBy = 2 
--            SET @SortString = ' vcItemName Desc '
--        ELSE IF @SortBy = 3 
--            SET @SortString = ' monListPrice Asc '
--        ELSE IF @SortBy = 4 
--            SET @SortString = ' monListPrice Desc '
--        ELSE IF @SortBy = 5 
--            SET @SortString = ' bintCreatedDate Desc '
--        ELSE IF @SortBy = 6 
--            SET @SortString = ' bintCreatedDate ASC '	
--        ELSE IF @SortBy = 7 --sort by relevance, invokes only when search
--            BEGIN
--                 IF LEN(@SearchText) > 0 
--		            BEGIN
--						SET @SortString = ' SearchOrder ASC ';
--					    set @searchPositionColumn = ' ,SearchItems.RowNumber AS SearchOrder ' 						
--					END
--			END
--        ELSE 
--            SET @SortString = ' vcItemName Asc '
		
                                		
        IF LEN(@SearchText) > 0
			BEGIN
				IF CHARINDEX('SearchOrder',@SortString) > 0
				BEGIN
					SET @searchPositionColumn = ' ,SearchItems.RowNumber AS SearchOrder ' 
					SET @searchPositionColumnGroupBy = ' ,SearchItems.RowNumber ' 
				END

				SET @Where = ' INNER JOIN dbo.SplitIDsWithPosition('''+ @SearchText +''','','') AS SearchItems 
                        ON SearchItems.Id = I.numItemCode  and SC.numSiteID = ' + CONVERT(VARCHAR(15), @numSiteID) + ' WHERE ISNULL(IsArchieve,0) = 0'
				END

		ELSE IF @SearchText = ''

			SET @Where = ' WHERE 1=2 '
			+ ' and SC.numSiteID = ' + CONVERT(VARCHAR(15), @numSiteID)
			+ ' AND ISNULL(IsArchieve,0) = 0'
		ELSE
			BEGIN
				SET @Where = ' WHERE 1=1 '
							+ ' and SC.numSiteID = ' + CONVERT(VARCHAR(15), @numSiteID)
							+ ' AND ISNULL(IsArchieve,0) = 0'

				IF @tintDisplayCategory = 1
				BEGIN
					
					;WITH CTE AS(
					SELECT C.numCategoryID,C.tintLevel,C.numDepCategory,CASE WHEN tintLevel=1 THEN 1 ELSE 0 END AS bitChild 
						FROM SiteCategories SC INNER JOIN Category C ON SC.numCategoryID=C.numCategoryID WHERE SC.numSiteID=@numSiteID AND (C.numCategoryID = @numCategoryID OR ISNULL(@numCategoryID,0) = 0) AND numDomainID = @numDomainID
					UNION ALL
					SELECT  C.numCategoryID,C.tintLevel,C.numDepCategory,CTE.bitChild FROM Category C JOIN CTE ON 
					C.numDepCategory=CTE.numCategoryID AND CTE.bitChild=1 AND numDomainID = @numDomainID)
					
					INSERT INTO #tmpItemCat(numCategoryID)												 
					SELECT DISTINCT C.numCategoryID FROM CTE C JOIN dbo.ItemCategory IC ON C.numCategoryID = IC.numCategoryID 
														WHERE numItemID IN ( 
																						
													 						SELECT numItemID FROM WareHouseItems WI
																			INNER JOIN dbo.Item I ON I.numItemCode = WI.numItemID AND I.numDomainID = @numDomainID
																			GROUP BY numItemID 
																			HAVING SUM(WI.numOnHand) > 0 
																			UNION ALL
													 						SELECT numItemID FROM WareHouseItems WI
																			INNER JOIN dbo.Item I ON I.numItemCode = WI.numItemID AND I.numDomainID = @numDomainID
																			GROUP BY numItemID 
																			HAVING SUM(WI.numAllocation) > 0
																		 )
				END

			END

		       SET @strSQL = @strSQL + CONCAT(' WITH Items AS 
												( 
													 SELECT
														I.numDomainID
														,I.numItemCode
														,ROW_NUMBER() OVER ( Partition by I.numItemCode Order by I.numItemCode) RowID
														,I.bitMatrix
														,I.bitKitParent
														,I.numItemGroup
														,vcItemName
														,I.bintCreatedDate
														,vcManufacturer
														,ISNULL(txtItemDesc,'''') txtItemDesc
														,vcSKU
														,fltHeight
														,fltWidth
														,fltLength
														,vcModelID
														,fltWeight
														,numBaseUnit
														,numSaleUnit
														,numPurchaseUnit
														,bitFreeShipping
														,C.vcCategoryName
														,C.vcDescription as CategoryDesc
														,charItemType
														,monListPrice
														,bitAllowBackOrder
														,bitShowInStock
														,numVendorID
														,numItemClassification',@searchPositionColumn,@SortFields,@DefaultSortFields,'
														,SUM(numOnHand) numOnHand
														,SUM(numAllocation) numAllocation
													 FROM      
														Item AS I
													 INNER JOIN eCommerceDTL E ON E.numDomainID = I.numDomainID AND E.numSiteId=',@numSiteID,'
													 LEFT JOIN WareHouseItems WI ON WI.numDomainID=' + CONVERT(VARCHAR(10),@numDomainID) + ' AND WI.numItemID=I.numItemCode AND numWareHouseID IN (SELECT [OutParam] FROM dbo.[SplitString](''' + @vcWarehouseIDs + ''','',''))
													 INNER JOIN ItemCategory IC ON I.numItemCode = IC.numItemID
													 INNER JOIN Category C ON IC.numCategoryID = C.numCategoryID
													 INNER JOIN SiteCategories SC ON IC.numCategoryID = SC.numCategoryID 
													  ',@Join,' ',@DefaultSortJoin)

			IF LEN(ISNULL(@FilterItemAttributes,'')) > 0
			BEGIN
				SET @strSQL = @strSQL + @FilterItemAttributes
			END

			IF LEN(@FilterCustomFields) > 0 AND LEN(@FilterCustomWhere) > 0
			BEGIN
				SET @strSQL = @strSQL + ' INNER JOIN #tmpItemCode TP ON TP.numItemCode = I.numItemCode ' 
			END
			
			IF (SELECT COUNT(*) FROM #tmpItemCat) > 0
			BEGIN
				SET @strSQL = @strSQL + ' INNER JOIN #tmpItemCat TC ON TC.numCategoryID = IC.numCategoryID '
			END
			ELSE IF @numManufacturerID>0
			BEGIN
				SET @Where = REPLACE(@Where,'WHERE',' WHERE I.numManufacturer = ' + CONVERT(VARCHAR(10),@numManufacturerID) + ' AND ')
			END 
			ELSE IF @numCategoryID>0
			BEGIN
				SET @Where = REPLACE(@Where,'WHERE',' WHERE IC.numCategoryID = ' + CONVERT(VARCHAR(10),@numCategoryID) + ' AND ')
			END 
			
			IF (SELECT COUNT(*) FROM [dbo].[EcommerceRelationshipProfile] WHERE numSiteID=@numSiteID AND numRelationship = @numDefaultRelationship AND numProfile = @numDefaultProfile) > 0
			BEGIN
				SET @Where = @Where + CONCAT(' AND I.numItemClassification NOT IN (SELECT numItemClassification FROM EcommerceRelationshipProfile ERP INNER JOIN ECommerceItemClassification EIC ON EIC.numECommRelatiionshipProfileID=ERP.ID WHERE ERP.numSiteID=',@numSiteID,' AND ERP.numRelationship=',@numDefaultRelationship,' AND ERP.numProfile=', @numDefaultProfile,')')
			END

			SET @strSQL = @strSQL + @Where
	
			IF LEN(@FilterRegularCondition) > 0
			BEGIN
				SET @strSQL = @strSQL + @FilterRegularCondition
			END                                         

            IF LEN(@whereNumItemCode) > 0
                                        BEGIN
								              SET @strSQL = @strSQL + @filterByNumItemCode	
								    	END
            SET @strSQL = CONCAT(@strSQL,' GROUP BY
												I.numDomainID
												,I.numItemCode
												,I.bitMatrix
												,I.bitKitParent
												,I.numItemGroup
												,vcItemName
												,I.bintCreatedDate
												,vcManufacturer
												,txtItemDesc
												,vcSKU
												,fltHeight
												,fltWidth
												,fltLength
												,vcModelID
												,fltWeight
												,numBaseUnit
												,numSaleUnit
												,numPurchaseUnit
												,bitFreeShipping
												,C.vcCategoryName
												,C.vcDescription
												,charItemType
												,monListPrice
												,bitAllowBackOrder
												,bitShowInStock
												,numVendorID
												,numItemClassification',@searchPositionColumnGroupBy,@SortFields,@DefaultSortFields,(CASE WHEN ISNULL(@tintDisplayCategory,0) = 1 THEN ' HAVING 1 = (CASE WHEN (I.charItemType=''P'' AND ISNULL(I.bitKitParent,0) = 0) THEN CASE WHEN (ISNULL(SUM(numOnHand), 0) > 0 OR ISNULL(SUM(numAllocation), 0) > 0) THEN 1 ELSE 0 END ELSE 1 END) ' ELSE '' END), ') SELECT * INTO #TEMPItems FROM Items;')
                                        
			IF @numDomainID IN (204,214)
			BEGIN
				SET @strSQL = @strSQL + 'DELETE FROM 
											#TEMPItems
										WHERE 
											numItemCode IN (
																SELECT 
																	F.numItemCode
																FROM 
																	#TEMPItems AS F
																WHERE 
																	ISNULL(F.bitMatrix,0) = 1 
																	AND EXISTS (
																				SELECT 
																					t1.vcItemName, t1.bitMatrix, t1.numItemGroup, Count(t1.numItemCode)
																				FROM 
																					#TEMPItems t1
																				WHERE 
																					t1.vcItemName = F.vcItemName
																					AND t1.bitMatrix = 1
																					AND t1.numItemGroup = F.numItemGroup
																				GROUP BY 
																					t1.vcItemName, t1.bitMatrix, t1.numItemGroup
																				HAVING 
																					Count(t1.numItemCode) > 1
																			)
															)
											AND numItemCode NOT IN (
																					SELECT 
																						Min(numItemCode)
																					FROM 
																						#TEMPItems AS F
																					WHERE
																						ISNULL(F.bitMatrix,0) = 1 
																						AND Exists (
																									SELECT 
																										t2.vcItemName, t2.bitMatrix, t2.numItemGroup, Count(t2.numItemCode)
																									FROM 
																										#TEMPItems t2
																									WHERE 
																										t2.vcItemName = F.vcItemName
																									   AND t2.bitMatrix = 1
																									   AND t2.numItemGroup = F.numItemGroup
																									GROUP BY 
																										t2.vcItemName, t2.bitMatrix, t2.numItemGroup
																									HAVING 
																										Count(t2.numItemCode) > 1
																								)
																					GROUP BY 
																						vcItemName, bitMatrix, numItemGroup
																				);'

			END
									  
									  
			SET @strSQL = @strSQL + ' WITH ItemSorted AS (SELECT  ROW_NUMBER() OVER ( ORDER BY  ' + @SortString + @DefaultSort + ' ' + @vcSort + ' ) AS Rownumber,* FROM  #TEMPItems WHERE RowID = 1)'



            DECLARE @fldList AS NVARCHAR(MAX)
			DECLARE @fldList1 AS NVARCHAR(MAX)
			SELECT @fldList=COALESCE(@fldList + ',','') + QUOTENAME(REPLACE(Fld_label,' ','') + '_C') FROM dbo.CFW_Fld_Master WHERE numDomainID=@numDomainID AND Grp_id=5
			SELECT @fldList1=COALESCE(@fldList1 + ',','') + QUOTENAME(REPLACE(Fld_label,' ','') + '_' + CAST(Fld_Id AS VARCHAR)) FROM dbo.CFW_Fld_Master WHERE numDomainID=@numDomainID AND Grp_id=9
            
            SET @strSQL = @strSQL + 'SELECT * INTO #tmpPagedItems FROM ItemSorted WHERE Rownumber > ' + CONVERT(VARCHAR(10), @firstRec) + '
                                      AND   Rownumber < ' + CONVERT(VARCHAR(10), @lastRec) + '
                                      order by Rownumber '
                                                                  

			SET @strSQL = CONCAT(@strSQL,'SELECT  
										Rownumber
										,I.numItemCode
										,vcCategoryName
										,CategoryDesc
										,vcItemName
										,txtItemDesc
										,vcManufacturer
										,vcSKU
										,fltHeight
										,fltWidth
										,fltLength
										,vcModelID
										,fltWeight
										,bitFreeShipping
										,', 
											(CASE 
											WHEN @tintPreLoginPriceLevel > 0 
											THEN 'ISNULL(PricingOption.monPrice,ISNULL(CASE WHEN I.[charItemType] = ''P'' THEN ( UOM * ISNULL(W1.[monWListPrice],0)) ELSE (UOM * monListPrice) END, 0))' 
											ELSE 'ISNULL(CASE WHEN I.[charItemType] = ''P'' THEN ( UOM * ISNULL(W1.[monWListPrice],0)) ELSE (UOM * monListPrice) END, 0)' 
											END)
											,' AS monListPrice,
											ISNULL(CASE WHEN I.[charItemType] = ''P'' THEN ( UOM * ISNULL(W1.[monWListPrice],0)) ELSE (UOM * monListPrice) END, 0) monMSRP					   
											',(CASE WHEN @numDomainID = 172 THEN ',ISNULL(CASE 
													WHEN tintRuleType = 1 AND tintDiscountType = 1
													THEN ISNULL((CASE WHEN I.[charItemType] = ''P'' THEN ( UOM * ISNULL(W1.[monWListPrice],0)) ELSE (UOM * monListPrice) END),0) - (ISNULL((CASE WHEN I.[charItemType] = ''P'' THEN ( UOM * ISNULL(W1.[monWListPrice],0)) ELSE (UOM * monListPrice) END),0) * ( decDiscount / 100 ) )
													WHEN tintRuleType = 1 AND tintDiscountType = 2
													THEN ISNULL((CASE WHEN I.[charItemType] = ''P'' THEN ( UOM * ISNULL(W1.[monWListPrice],0)) ELSE (UOM * monListPrice) END),0) - decDiscount
													WHEN tintRuleType = 2 AND tintDiscountType = 1
													THEN ISNULL(Vendor.monCost,0) + (ISNULL(Vendor.monCost,0) * ( decDiscount / 100 ) )
													WHEN tintRuleType = 2 AND tintDiscountType = 2
													THEN ISNULL(Vendor.monCost,0) + decDiscount
													WHEN tintRuleType = 3
													THEN decDiscount
												END, 0) AS monFirstPriceLevelPrice,ISNULL(CASE 
												WHEN tintRuleType = 1 AND tintDiscountType = 1 
												THEN decDiscount 
												WHEN tintRuleType = 1 AND tintDiscountType = 2
												THEN (decDiscount * 100) / (ISNULL((CASE WHEN I.[charItemType] = ''P'' THEN ( UOM * ISNULL(W1.[monWListPrice],0)) ELSE (UOM * monListPrice) END),0) - decDiscount)
												WHEN tintRuleType = 2 AND tintDiscountType = 1
												THEN decDiscount
												WHEN tintRuleType = 2 AND tintDiscountType = 2
												THEN (decDiscount * 100) / (ISNULL(Vendor.monCost,0) + decDiscount)
												WHEN tintRuleType = 3
												THEN 0
											END,'''') AS fltFirstPriceLevelDiscount' ELSE ',0 AS monFirstPriceLevelPrice, 0 AS fltFirstPriceLevelDiscount' END), '  
										,vcPathForImage
										,vcpathForTImage
										,UOM AS UOMConversionFactor
										,UOMPurchase AS UOMPurchaseConversionFactor
										,(CASE WHEN charItemType <> ''S'' AND charItemType <> ''N'' AND ISNULL(bitKitParent,0)=0
													THEN (CASE 
															WHEN bitAllowBackOrder = 1 THEN 1
															WHEN (ISNULL(I.numOnHand,0)<=0) THEN 0
															ELSE 1
														END)
													ELSE 1
										END) AS bitInStock,
										(
											CASE WHEN ISNULL(bitKitParent,0) = 1
											THEN
												(CASE 
													WHEN ((SELECT COUNT(*) FROM ItemDetails ID INNER JOIN Item IInner ON ID.numChildItemID = IInner.numItemCode WHERE ID.numItemKitID = I.numItemCode AND ISNULL(IInner.bitKitParent,0) = 1)) > 0 
													THEN 1 
													ELSE 0 
												END)
											ELSE
												0
											END
										) bitHasChildKits
										,(CASE WHEN charItemType <> ''S'' AND charItemType <> ''N''
													THEN ( CASE WHEN bitShowInStock = 1
																THEN (CASE 
																		WHEN bitAllowBackOrder = 1 AND I.numDomainID <> 172 AND ISNULL(I.numOnHand,0)<=0 AND ISNULL(I.numVendorID,0) > 0 AND dbo.GetVendorPreferredMethodLeadTime(I.numVendorID) > 0 THEN CONCAT(''<font class="vendorLeadTime" style="color:green">Usually ships in '',dbo.GetVendorPreferredMethodLeadTime(I.numVendorID),'' days</font>'')
																		WHEN bitAllowBackOrder = 1 AND ISNULL(I.numOnHand,0)<=0 THEN (CASE WHEN I.numDomainID = 172 THEN ''<font style="color:green">AVAILABLE</font>'' ELSE ''<font style="color:green">In Stock</font>'' END)
																		WHEN (ISNULL(I.numOnHand,0)<=0) THEN (CASE WHEN I.numDomainID = 172 THEN ''<font style="color:red">ON HOLD</font>'' ELSE ''<font style="color:red">Out Of Stock</font>'' END)
																		ELSE (CASE WHEN I.numDomainID = 172 THEN ''<font style="color:green">AVAILABLE</font>'' ELSE ''<font style="color:green">In Stock</font>'' END)  
																	END)
																ELSE ''''
															END )
													ELSE ''''
												END ) AS InStock
										, ISNULL(W1.numWareHouseItemID,0) [numWareHouseItemID]
										,(SELECT 
												COUNT(numProId) 
											FROM 
												PromotionOffer PO
											WHERE 
												numDomainId=',CAST(@numDomainID AS VARCHAR(20)),' 
												AND ISNULL(bitEnabled,0)=1
												AND ISNULL(bitAppliesToSite,0)=1 
												AND ISNULL(bitRequireCouponCode,0)=0
												AND 1 = (CASE WHEN bitRequireCouponCode = 1 THEN CASE WHEN ISNULL(intCouponCodeUsed,0) < ISNULL(tintUsageLimit,0) OR ISNULL(tintUsageLimit,0) = 0  THEN 1 ELSE 0 END ELSE 1 END)
												AND 1 = (CASE WHEN bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN CAST(GETDATE() AS DATE)>=dtValidFrom AND CAST(GETDATE() AS DATE)<=dtValidTo THEN 1 ELSE 0 END) END)
												AND (1 = (CASE 
															WHEN tintOfferBasedOn = 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 AND numValue IN (SELECT numOppAccAttrID FROM ItemGroupsDTL WHERE numItemGroupID IN (SELECT numItemGroupID FROM ItemGroups WHERE numProfileItem=I.numItemCode AND numDomainID='+CAST(@numDomainID AS VARCHAR(20))+' ) UNION SELECT I.numItemCode)) > 0 THEN 1 ELSE 0 END)
															WHEN tintOfferBasedOn = 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=2 AND numValue=I.numItemClassification) > 0 THEN 1 ELSE 0 END)
															ELSE 0
														END)
													OR
													1 = (CASE 
															WHEN tintDiscoutBaseOn = 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=6 AND tintType=1 AND numValue IN (SELECT numOppAccAttrID FROM ItemGroupsDTL WHERE numItemGroupID IN (SELECT numItemGroupID FROM ItemGroups WHERE numProfileItem=I.numItemCode AND numDomainID='+CAST(@numDomainID AS VARCHAR(20))+' ) UNION SELECT I.numItemCode)) > 0 THEN 1 ELSE 0 END)
															WHEN tintDiscoutBaseOn = 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=6 AND tintType=2 AND numValue=I.numItemClassification) > 0 THEN 1 ELSE 0 END)
															WHEN tintDiscoutBaseOn = 3 AND tintOfferBasedOn = 1 THEN (CASE WHEN (SELECT COUNT(SimilarItems.numItemCode) FROM SimilarItems WHERE numParentItemCode IN (SELECT numValue FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 AND SimilarItems.numItemCode=I.numItemCode))>0 THEN 1 ELSE 0 END)
															WHEN tintDiscoutBaseOn = 3 AND tintOfferBasedOn = 2 THEN (CASE WHEN (SELECT COUNT(SimilarItems.numItemCode) FROM SimilarItems WHERE SimilarItems.numItemCode IN(I.numItemCode) AND numParentItemCode IN (SELECT numItemCode FROM Item WHERE numItemClassification IN (SELECT numValue FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=2)))>0 THEN 1 ELSE 0 END)
															ELSE 0
														END)
													)
											)  IsOnSale',(CASE WHEN LEN(ISNULL(@fldList,'')) > 0 THEN CONCAT(',',@fldList) ELSE '' END),(CASE WHEN LEN(ISNULL(@fldList1,'')) > 0 THEN CONCAT(',',@fldList1) ELSE '' END),'
                                    FROM 
										#tmpPagedItems I
									LEFT JOIN ItemImages IM ON I.numItemCode = IM.numItemCode AND IM.bitdefault=1 AND IM.bitIsImage = 1',
									(CASE 
										WHEN @tintPreLoginPriceLevel > 0 
										THEN CONCAT(' OUTER APPLY(SELECT decDiscount AS monPrice FROM PricingTable WHERE tintRuleType=3 AND numItemCode=I.numItemCode ORDER BY numPricingID OFFSET ',CAST(@tintPreLoginPriceLevel-1 AS VARCHAR),' ROWS FETCH NEXT 1 ROWS ONLY) As PricingOption ' )
										ELSE '' 
									END),(CASE 
										WHEN @numDomainID = 172
										THEN ' OUTER APPLY (SELECT TOP 1 * FROM PricingTable WHERE numItemCode=I.numItemCode) AS PT LEFT JOIN Vendor ON Vendor.numVendorID =I.numVendorID AND Vendor.numItemCode=I.numItemCode'
										ELSE '' 
									END),' 
									OUTER APPLY (SELECT TOP 1 numWareHouseItemID,monWListPrice FROM WareHouseItems W 
													WHERE  W.numItemID = I.numItemCode 
													AND W.numDomainID = ',CONVERT(VARCHAR(10),@numDomainID),'
													AND numWareHouseID IN (SELECT [OutParam] FROM dbo.[SplitString](''' + @vcWarehouseIDs + ''','',''))) AS W1 			  
										CROSS APPLY (SELECT dbo.fn_UOMConversion(ISNULL(I.numSaleUnit, 0),I.numItemCode, I.numDomainId,ISNULL(I.numBaseUnit, 0)) AS UOM) AS UOM
										CROSS APPLY (SELECT dbo.fn_UOMConversion(ISNULL(I.numPurchaseUnit, 0),I.numItemCode, I.numDomainId,ISNULL(I.numBaseUnit, 0)) AS UOMPurchase) AS UOMPurchase')
			
			PRINT @strSQL

			IF LEN(ISNULL(@fldList,'')) > 0
			BEGIN
				SET @strSQL = @strSQL +' left join (
													SELECT  t2.RecId, REPLACE(t1.Fld_label,'' '','''') + ''_C'' as Fld_label, 
														CASE t1.fld_type WHEN ''SelectBox'' THEN isnull(dbo.fn_GetListItemName(t2.Fld_Value),'''')
																			WHEN ''CheckBox'' THEN isnull(dbo.fn_getCustomDataValue(t2.Fld_Value,9),'''')
																			WHEN ''DateField'' THEN dbo.FormatedDateFromDate(t2.Fld_Value,' + CAST(@numDomainId AS VARCHAR(18)) +')
																			ELSE Replace(ISNULL(t2.Fld_Value,''0''),''$'',''$$'') end AS Fld_Value FROM CFW_Fld_Master t1 
													JOIN CFW_FLD_Values_Item AS t2 ON t1.Fld_ID = t2.Fld_ID where t1.Grp_id = 5 and t1.numDomainID=' + CAST(@numDomainId AS VARCHAR(18)) +' AND t1.fld_type <> ''Link''
													AND t2.RecId IN (SELECT numItemCode FROM #tmpPagedItems )
													) p PIVOT (MAX([Fld_Value]) FOR Fld_label IN (' + @fldList + ')) AS pvt on I.numItemCode=pvt.RecId' 
			END

			IF LEN(ISNULL(@fldList1,'')) > 0
			BEGIN
					
				SET @strSQL = @strSQL +' left join (
													SELECT  t2.numItemCode AS RecId, REPLACE(t1.Fld_label,'' '','''') + ''_'' + CAST(t1.Fld_Id AS VARCHAR) as Fld_label, 
														CASE t1.fld_type WHEN ''SelectBox'' THEN isnull(dbo.fn_GetListItemName(t2.Fld_Value),'''')
																			WHEN ''CheckBox'' THEN isnull(dbo.fn_getCustomDataValue(t2.Fld_Value,9),'''')
																			WHEN ''DateField'' THEN dbo.FormatedDateFromDate(t2.Fld_Value,' + CAST(@numDomainId AS VARCHAR(18)) +')
																			ELSE Replace(ISNULL(t2.Fld_Value,''0''),''$'',''$$'') end AS Fld_Value FROM CFW_Fld_Master t1 
													JOIN ItemAttributes AS t2 ON t1.Fld_ID = t2.Fld_ID where t1.Grp_id = 9 and t1.numDomainID=' + CAST(@numDomainId AS VARCHAR(18)) +' AND t1.fld_type <> ''Link''
													AND t2.numItemCode IN (SELECT numItemCode FROM #tmpPagedItems )
													) p PIVOT (MAX([Fld_Value]) FOR Fld_label IN (' + @fldList1 + ')) AS pvt1 on I.numItemCode=pvt1.RecId'
			END
			ELSE
			BEGIN
				SET @strSQL = REPLACE(@strSQL, ',##FLDLIST1##', '');
			END

			SET  @strSQL = @strSQL +' order by Rownumber';
              
                                       
			                           
            SET @strSQL = @strSQL + ' SELECT @TotRecs = COUNT(*) FROM #TEMPItems WHERE RowID=1 '                            


			 DECLARE @tmpSQL AS NVARCHAR(MAX)
			 SET @tmpSQL = @strSQL
	
			 PRINT CAST(@tmpSQL AS NTEXT)
             EXEC sp_executeSQL @strSQL, N'@TotRecs INT OUTPUT', @TotRecs OUTPUT
             
             
        DECLARE @tintOrder AS TINYINT                                                  
		DECLARE @vcFormFieldName AS VARCHAR(50)                                                  
		DECLARE @vcListItemType AS VARCHAR(1)                                             
		DECLARE @vcAssociatedControlType VARCHAR(10)                                                  
		DECLARE @numListID AS NUMERIC(9)                                                  
		DECLARE @WhereCondition VARCHAR(MAX)                       
		DECLARE @numFormFieldId AS NUMERIC  
		--DECLARE @vcFieldType CHAR(1)
		DECLARE @vcFieldType VARCHAR(15)
		                  
		SET @tintOrder=0                                                  
		SET @WhereCondition =''                 
                   
              
		CREATE TABLE #tempAvailableFields(numFormFieldId  NUMERIC(9),vcFormFieldName NVARCHAR(50),
		--vcFieldType CHAR(1),
		vcFieldType VARCHAR(15),vcAssociatedControlType NVARCHAR(50),numListID NUMERIC(18),
		vcListItemType CHAR(1),intRowNum INT,vcItemValue VARCHAR(3000))
		   
		INSERT INTO #tempAvailableFields (numFormFieldId,vcFormFieldName,vcFieldType,vcAssociatedControlType
				,numListID,vcListItemType,intRowNum)                         
		SELECT  Fld_id,REPLACE(Fld_label,' ','') AS vcFormFieldName,
			   CASE GRP_ID WHEN 4 then 'C' 
					WHEN 1 THEN 'D'
					--WHEN 9 THEN 'A' 
					WHEN 9 THEN CONVERT(VARCHAR(15), Fld_id)
					ELSE 'C' END AS vcFieldType,
				fld_type as vcAssociatedControlType,
				ISNULL(C.numListID, 0) numListID,
				CASE WHEN C.numListID > 0 THEN 'L'
					 ELSE ''
				END vcListItemType,ROW_NUMBER() OVER (ORDER BY Fld_id ASC) AS intRowNum
		FROM    CFW_Fld_Master C LEFT JOIN CFW_Validation V ON V.numFieldID = C.Fld_id
		WHERE   C.numDomainID = @numDomainId
				AND GRP_ID IN (5,9)

		  
		SELECT * FROM #tempAvailableFields
		
		/*
		DROP TABLE #tempAvailableFields
		DROP TABLE #tmpItemCode
		DROP TABLE #tmpItemCat
		*/	

		IF OBJECT_ID('tempdb..#tempAvailableFields') IS NOT NULL
		DROP TABLE #tempAvailableFields
		
		IF OBJECT_ID('tempdb..#tmpItemCode') IS NOT NULL
		DROP TABLE #tmpItemCode
		
		IF OBJECT_ID('tempdb..#tmpItemCat') IS NOT NULL
		DROP TABLE #tmpItemCat
		
		IF OBJECT_ID('tempdb..#fldValues') IS NOT NULL
		DROP TABLE #fldValues
		
		IF OBJECT_ID('tempdb..#tempSort') IS NOT NULL
		DROP TABLE #tempSort
		
		IF OBJECT_ID('tempdb..#fldDefaultValues') IS NOT NULL
		DROP TABLE #fldDefaultValues

		IF OBJECT_ID('tempdb..#tmpPagedItems') IS NOT NULL
		DROP TABLE #tmpPagedItems


		IF OBJECT_ID('tempdb..#TEMPItems') IS NOT NULL
		DROP TABLE #TEMPItems
    END


GO
/****** Object:  StoredProcedure [dbo].[USP_ManageItemsAndKits]    Script Date: 02/15/2010 21:44:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Anoop Jayaraj                                                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_manageitemsandkits')
DROP PROCEDURE usp_manageitemsandkits
GO
CREATE PROCEDURE [dbo].[USP_ManageItemsAndKits]                                                                
@numItemCode as numeric(9) output,                                                                
@vcItemName as varchar(300),                                                                
@txtItemDesc as varchar(1000),                                                                
@charItemType as char(1),                                                                
@monListPrice as DECIMAL(20,5),                                                                
@numItemClassification as numeric(9),                                                                
@bitTaxable as bit,                                                                
@vcSKU as varchar(50),                                                                                                                                      
@bitKitParent as bit,                                                                                   
--@dtDateEntered as datetime,                                                                
@numDomainID as numeric(9),                                                                
@numUserCntID as numeric(9),                                                                                                  
@numVendorID as numeric(9),                                                            
@bitSerialized as bit,                                               
@strFieldList as text,                                            
@vcModelID as varchar(200)='' ,                                            
@numItemGroup as numeric(9)=0,                                      
@numCOGSChartAcntId as numeric(9)=0,                                      
@numAssetChartAcntId as numeric(9)=0,                                      
@numIncomeChartAcntId as numeric(9)=0,                            
@monAverageCost as DECIMAL(20,5),                          
@monLabourCost as DECIMAL(20,5),                  
@fltWeight as float,                  
@fltHeight as float,                  
@fltLength as float,                  
@fltWidth as float,                  
@bitFreeshipping as bit,                
@bitAllowBackOrder as bit,              
@UnitofMeasure as varchar(30)='',              
@strChildItems as text,            
@bitShowDeptItem as bit,              
@bitShowDeptItemDesc as bit,        
@bitCalAmtBasedonDepItems as bit,    
@bitAssembly as BIT,
@intWebApiId INT=null,
@vcApiItemId AS VARCHAR(50)=null,
@numBarCodeId as VARCHAR(25)=null,
@vcManufacturer as varchar(250)=NULL,
@numBaseUnit AS NUMERIC(18)=0,
@numPurchaseUnit AS NUMERIC(18)=0,
@numSaleUnit AS NUMERIC(18)=0,
@bitLotNo as BIT,
@IsArchieve AS BIT,
@numItemClass as numeric(9)=0,
@tintStandardProductIDType AS TINYINT=0,
@vcExportToAPI VARCHAR(50),
@numShipClass NUMERIC,
@ProcedureCallFlag SMALLINT =0,
@vcCategories AS VARCHAR(2000) = '',
@bitAllowDropShip AS BIT=0,
@bitArchiveItem AS BIT = 0,
@bitAsset AS BIT = 0,
@bitRental AS BIT = 0,
@numCategoryProfileID AS NUMERIC(18,0) = 0,
@bitVirtualInventory BIT,
@bitContainer BIT,
@numContainer NUMERIC(18,0)=0,
@numNoItemIntoContainer NUMERIC(18,0),
@bitMatrix BIT = 0 ,
@vcItemAttributes VARCHAR(2000) = '',
@numManufacturer NUMERIC(18,0),
@vcASIN as varchar(50),
@tintKitAssemblyPriceBasedOn TINYINT = 1,
@fltReorderQty FLOAT = 0,
@bitSOWorkOrder BIT = 0,
@bitKitSingleSelect BIT = 0,
@bitExpenseItem BIT = 0,
@numExpenseChartAcntId NUMERIC(18,0) = 0
AS
BEGIN     
BEGIN TRY
BEGIN TRANSACTION

	SET @vcManufacturer = CASE WHEN ISNULL(@numManufacturer,0) > 0 THEN ISNULL((SELECT vcCompanyName FROM DivisionMaster INNER JOIN CompanyInfo ON DivisionMaster.numCompanyID=CompanyInfo.numCompanyId WHERE numDivisionID=@numManufacturer),'')  ELSE ISNULL(@vcManufacturer,'') END

	DECLARE @bitAttributesChanged AS BIT  = 1
	DECLARE @bitOppOrderExists AS BIT = 0

	IF ISNULL(@numItemCode,0) > 0
	BEGIN
		IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityItems OI WHERE OI.numItemCode =@numItemCode) > 0
		BEGIN
			SET @bitOppOrderExists = 1
		END
		ELSE IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityKitItems OI WHERE OI.numChildItemID =@numItemCode) > 0
		BEGIN
			SET @bitOppOrderExists = 1
		END
		ELSE IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityKitChildItems OI WHERE OI.numItemID =@numItemCode) > 0
		BEGIN
			SET @bitOppOrderExists = 1
		END
	END
	ELSE 
		BEGIN
		   IF (ISNULL(@vcItemName,'') = '')
			   BEGIN
				   RAISERROR('ITEM_NAME_NOT_SELECTED',16,1)
				   RETURN
			   END
           ELSE IF (ISNULL(@charItemType,'') = '0')
			   BEGIN
				   RAISERROR('ITEM_TYPE_NOT_SELECTED',16,1)
				   RETURN
			   END
           ELSE
		   BEGIN 
		      If (@charItemType = 'P' OR (@charItemType = 'S' AND ISNULL(@bitExpenseItem,0) = 1))
			  BEGIN
				IF ((SELECT COUNT(*) FROM Chart_Of_Accounts WHERE numDomainId=@numDomainID AND numAccountId=@numAssetChartAcntId) = 0)
				BEGIN
					RAISERROR('INVALID_ASSET_ACCOUNT',16,1)
					RETURN
				END
				
			  END

				-- This is common check for CharItemType P and Other
				IF ((SELECT COUNT(*) FROM Chart_Of_Accounts WHERE numDomainId=@numDomainID AND numAccountId=@numCOGSChartAcntId) = 0)
				BEGIN
					RAISERROR('INVALID_COGS_ACCOUNT',16,1)
					RETURN
				END

				IF ((SELECT COUNT(*) FROM Chart_Of_Accounts WHERE numDomainId=@numDomainID AND numAccountId=@numIncomeChartAcntId) = 0)
				BEGIN
					RAISERROR('INVALID_INCOME_ACCOUNT',16,1)
					RETURN
				END

				IF ISNULL(@bitExpenseItem,0) = 1
				BEGIN
					IF ((SELECT COUNT(*) FROM Chart_Of_Accounts WHERE numDomainId=@numDomainID AND numAccountId=@numExpenseChartAcntId) = 0)
					BEGIN
						RAISERROR('INVALID_EXPENSE_ACCOUNT',16,1)
						RETURN
					END
				END
		   END
		END
	--INSERT CUSTOM FIELDS BASE ON WAREHOUSEITEMS
	DECLARE @hDoc AS INT     
	                                                                        	
	DECLARE @TempTable TABLE 
	(
		ID INT IDENTITY(1,1),
		Fld_ID NUMERIC(18,0),
		Fld_Value NUMERIC(18,0)
	)   

	IF ISNULL(@bitMatrix,0) = 1 AND ISNULL(@numItemGroup,0) = 0
	BEGIN
		RAISERROR('ITEM_GROUP_NOT_SELECTED',16,1)
		RETURN
	END
	ELSE IF ISNULL(@bitMatrix,0) = 1 AND LEN(ISNULL(@vcItemAttributes,'')) = 0 
	BEGIN
		RAISERROR('ITEM_ATTRIBUTES_NOT_SELECTED',16,1)
		RETURN
	END
	ELSE IF ISNULL(@bitMatrix,0) = 1 
	BEGIN
		DECLARE @bitDuplicate AS BIT = 0
	
		IF DATALENGTH(ISNULL(@vcItemAttributes,''))>2
		BEGIN
			EXEC sp_xml_preparedocument @hDoc OUTPUT, @vcItemAttributes
	                                      
			INSERT INTO @TempTable 
			(
				Fld_ID,
				Fld_Value
			)    
			SELECT
				*          
			FROM 
				OPENXML (@hDoc,'/NewDataSet/CusFlds',2)                                                  
			WITH 
				(Fld_ID NUMERIC(18,0),Fld_Value NUMERIC(18,0)) 
			ORDER BY 
				Fld_ID       

			DECLARE @strAttribute VARCHAR(500) = ''
			DECLARE @i AS INT = 1
			DECLARE @COUNT AS INT 
			DECLARE @numFldID AS NUMERIC(18,0)
			DECLARE @numFldValue AS NUMERIC(18,0)
			SELECT @COUNT = COUNT(*) FROM @TempTable

			WHILE @i <= @COUNT
			BEGIN
				SELECT @numFldID=Fld_ID,@numFldValue=Fld_Value FROM @TempTable WHERE ID=@i

				IF ISNUMERIC(@numFldValue)=1
				BEGIN
					SELECT @strAttribute=@strAttribute+Fld_label FROM CFW_Fld_Master WHERE Fld_id=@numFldID AND Grp_id = 9
                
					IF LEN(@strAttribute) > 0
					BEGIN
						SELECT @strAttribute=@strAttribute+':'+ vcData +',' FROM ListDetails WHERE numListItemID=@numFldValue
					END
				END

				SET @i = @i + 1
			END

			SET @strAttribute = SUBSTRING(@strAttribute,0,LEN(@strAttribute))

			IF
				(SELECT
					COUNT(*)
				FROM
					Item
				INNER JOIN
					ItemAttributes
				ON
					Item.numItemCode = ItemAttributes.numItemCode
					AND ItemAttributes.numDomainID = @numDomainID                     
				WHERE
					Item.numItemCode <> @numItemCode
					AND Item.vcItemName = @vcItemName
					AND Item.numItemGroup = @numItemGroup
					AND Item.numDomainID = @numDomainID
					AND dbo.fn_GetItemAttributes(@numDomainID,Item.numItemCode) = @strAttribute
				) > 0
			BEGIN
				RAISERROR('ITEM_WITH_ATTRIBUTES_ALREADY_EXISTS',16,1)
				RETURN
			END

			If ISNULL(@numItemCode,0) > 0 AND dbo.fn_GetItemAttributes(@numDomainID,@numItemCode) = @strAttribute
			BEGIN
				SET @bitAttributesChanged = 0
			END

			EXEC sp_xml_removedocument @hDoc 
		END	
	END
	ELSE IF ISNULL(@bitMatrix,0) = 0
	BEGIN
		-- REMOVE ATTRIBUTES CONFIGURATION OF ITEM AND WAREHOUSE
		DELETE FROM ItemAttributes WHERE numDomainID=@numDomainID AND numItemCode=@numItemCode
	END 
	
	IF ISNULL(@numItemGroup,0) > 0 AND ISNULL(@bitMatrix,0) = 1
	BEGIN
		IF (SELECT COUNT(*) FROM ItemGroupsDTL WHERE numItemGroupID = @numItemGroup AND tintType=2) = 0
		BEGIN
			RAISERROR('ITEM_GROUP_NOT_CONFIGURED',16,1)
			RETURN
		END
		ELSE IF EXISTS (SELECT 
							CFM.Fld_id
						FROM 
							CFW_Fld_Master CFM
						LEFT JOIN
							ListDetails LD
						ON
							CFM.numlistid = LD.numListID
						WHERE
							CFM.numDomainID = @numDomainID
							AND CFM.Fld_id IN (SELECT numOppAccAttrID FROM ItemGroupsDTL WHERE numItemGroupID=@numItemGroup AND tintType = 2)
						GROUP BY
							CFM.Fld_id HAVING COUNT(LD.numListItemID) = 0)
		BEGIN
			RAISERROR('ITEM_GROUP_NOT_CONFIGURED',16,1)
			RETURN
		END
	END

	IF @numCOGSChartAcntId=0 SET @numCOGSChartAcntId=NULL
	IF @numAssetChartAcntId=0 SET @numAssetChartAcntId=NULL  
	IF @numIncomeChartAcntId=0 SET @numIncomeChartAcntId=NULL     
	IF @numExpenseChartAcntId=0 SET @numExpenseChartAcntId=NULL

	DECLARE @ParentSKU VARCHAR(50) = @vcSKU                        
	DECLARE @ItemID AS NUMERIC(9)
	DECLARE @cnt AS INT

	IF ISNULL(@charItemType,'') = '' OR ISNULL(@charItemType,'') = '0'  
		SET @charItemType = 'N'

	IF @intWebApiId = 2 AND LEN(ISNULL(@vcApiItemId, '')) > 0 AND ISNULL(@numItemGroup, 0) > 0 
    BEGIN 
        -- check wether this id already Mapped in ITemAPI 
        SELECT 
			@cnt = COUNT([numItemID])
        FROM 
			[ItemAPI]
        WHERE 
			[WebApiId] = @intWebApiId
            AND [numDomainId] = @numDomainID
            AND [vcAPIItemID] = @vcApiItemId

        IF @cnt > 0 
        BEGIN
            SELECT 
				@ItemID = [numItemID]
            FROM 
				[ItemAPI]
            WHERE 
				[WebApiId] = @intWebApiId
                AND [numDomainId] = @numDomainID
                AND [vcAPIItemID] = @vcApiItemId
        
			--Update Existing Product coming from API
            SET @numItemCode = @ItemID
        END
    END
	ELSE IF @intWebApiId > 1 AND LEN(ISNULL(@vcSKU, '')) > 0 
    BEGIN
        SET @ParentSKU = @vcSKU
		 
        SELECT 
			@cnt = COUNT([numItemCode])
        FROM 
			[Item]
        WHERE 
			[numDomainId] = @numDomainID
            AND (vcSKU = @vcSKU OR @vcSKU IN (SELECT vcWHSKU FROM dbo.WareHouseItems WHERE numItemID = Item.[numItemCode] AND vcWHSKU = @vcSKU))
            
		IF @cnt > 0 
        BEGIN
            SELECT 
				@ItemID = [numItemCode],
                @ParentSKU = vcSKU
            FROM 
				[Item]
            WHERE 
				[numDomainId] = @numDomainID
                AND (vcSKU = @vcSKU OR @vcSKU IN (SELECT vcWHSKU FROM dbo.WareHouseItems WHERE numItemID = Item.[numItemCode] AND vcWHSKU = @vcSKU))

            SET @numItemCode = @ItemID
        END
		ELSE 
		BEGIN
			-- check wether this id already Mapped in ITemAPI 
			SELECT 
				@cnt = COUNT([numItemID])
			FROM 
				[ItemAPI]
			WHERE 
				[WebApiId] = @intWebApiId
				AND [numDomainId] = @numDomainID
				AND [vcAPIItemID] = @vcApiItemId
                    
			IF @cnt > 0 
			BEGIN
				SELECT 
					@ItemID = [numItemID]
				FROM 
					[ItemAPI]
				WHERE 
					[WebApiId] = @intWebApiId
					AND [numDomainId] = @numDomainID
					AND [vcAPIItemID] = @vcApiItemId
        
				--Update Existing Product coming from API
				SET @numItemCode = @ItemID
			END
		END
    END
                                                         
	IF @numItemCode=0 OR @numItemCode IS NULL
	BEGIN
		INSERT INTO Item 
		(                                             
			vcItemName,txtItemDesc,charItemType,monListPrice,numItemClassification,bitTaxable,vcSKU,bitKitParent,
			numVendorID,numDomainID,numCreatedBy,bintCreatedDate,bintModifiedDate,numModifiedBy,bitSerialized,
			vcModelID,numItemGroup,numCOGsChartAcntId,numAssetChartAcntId,numIncomeChartAcntId,monAverageCost,                          
			monCampaignLabourCost,fltWeight,fltHeight,fltWidth,fltLength,bitFreeShipping,bitAllowBackOrder,vcUnitofMeasure,
			bitShowDeptItem,bitShowDeptItemDesc,bitCalAmtBasedonDepItems,bitAssembly,numBarCodeId,vcManufacturer,numBaseUnit,
			numPurchaseUnit,numSaleUnit,bitLotNo,IsArchieve,numItemClass,tintStandardProductIDType,vcExportToAPI,numShipClass,
			bitAllowDropShip,bitArchiveItem,bitAsset,bitRental,bitVirtualInventory,bitContainer,numContainer,numNoItemIntoContainer
			,bitMatrix,numManufacturer,vcASIN,tintKitAssemblyPriceBasedOn,fltReorderQty,bitSOWorkOrder,bitKitSingleSelect,bitExpenseItem,numExpenseChartAcntId
		)                                                              
		VALUES                                                                
		(                                                                
			@vcItemName,@txtItemDesc,@charItemType,@monListPrice,@numItemClassification,@bitTaxable,@ParentSKU,@bitKitParent,
			@numVendorID,@numDomainID,@numUserCntID,getutcdate(),getutcdate(),@numUserCntID,@bitSerialized,@vcModelID,@numItemGroup,                                      
			@numCOGsChartAcntId,@numAssetChartAcntId,@numIncomeChartAcntId,@monAverageCost,@monLabourCost,@fltWeight,@fltHeight,@fltWidth,                  
			@fltLength,@bitFreeshipping,@bitAllowBackOrder,@UnitofMeasure,@bitShowDeptItem,@bitShowDeptItemDesc,@bitCalAmtBasedonDepItems,    
			@bitAssembly,@numBarCodeId,@vcManufacturer,@numBaseUnit,@numPurchaseUnit,@numSaleUnit,@bitLotNo,@IsArchieve,@numItemClass,
			@tintStandardProductIDType,@vcExportToAPI,@numShipClass,@bitAllowDropShip,@bitArchiveItem ,@bitAsset,@bitRental,
			@bitVirtualInventory,@bitContainer,@numContainer,@numNoItemIntoContainer
			,@bitMatrix,@numManufacturer,@vcASIN,@tintKitAssemblyPriceBasedOn,@fltReorderQty,@bitSOWorkOrder,@bitKitSingleSelect,@bitExpenseItem,@numExpenseChartAcntId
		)                                             
 
		SET @numItemCode = SCOPE_IDENTITY()
  
		IF  @intWebApiId > 1 
		BEGIN
			 -- insert new product
			 --insert this id into linking table
			 INSERT INTO [ItemAPI] (
	 			[WebApiId],
	 			[numDomainId],
	 			[numItemID],
	 			[vcAPIItemID],
	 			[numCreatedby],
	 			[dtCreated],
	 			[numModifiedby],
	 			[dtModified],vcSKU
			 ) VALUES     
			 (
				@intWebApiId,
				@numDomainID,
				@numItemCode,
				@vcApiItemId,
	 			@numUserCntID,
	 			GETUTCDATE(),
	 			@numUserCntID,
	 			GETUTCDATE(),@vcSKU
	 		) 
		END
 
		IF @charItemType = 'P' AND 1 = (CASE WHEN ISNULL(@numItemGroup,0) > 0 THEN (CASE WHEN ISNULL(@bitMatrix,0)=1 THEN 1 ELSE 0 END) ELSE 1 END)
		BEGIN
			
			DECLARE @TEMPWarehouse TABLE
			(
				ID INT IDENTITY(1,1)
				,numWarehouseID NUMERIC(18,0)
			)
		
			INSERT INTO @TEMPWarehouse (numWarehouseID) SELECT numWareHouseID FROM Warehouses WHERE numDomainID=@numDomainID

			DECLARE @j AS INT = 1
			DECLARE @jCount AS INT
			DECLARE @numTempWarehouseID NUMERIC(18,0)
			SELECT @jCount = COUNT(*) FROM @TEMPWarehouse

			WHILE @j <= @jCount
			BEGIN
				SELECT @numTempWarehouseID=numWarehouseID FROM @TEMPWarehouse WHERE ID=@j

				EXEC USP_WarehouseItems_Save @numDomainID,
											@numUserCntID,
											0,
											@numTempWarehouseID,
											0,
											@numItemCode,
											'',
											@monListPrice,
											0,
											0,
											'',
											'',
											'',
											'',
											0
 
				SET @j = @j + 1
			END
		END 
	END         
	ELSE IF @numItemCode>0 AND @intWebApiId > 0 
	BEGIN 
		IF @ProcedureCallFlag = 1 
		BEGIN
			DECLARE @ExportToAPIList AS VARCHAR(30)
			SELECT @ExportToAPIList = vcExportToAPI FROM dbo.Item WHERE numItemCode = @numItemCode

			IF @ExportToAPIList != ''
			BEGIN
				IF NOT EXISTS(select * from (SELECT * FROM dbo.Split(@ExportToAPIList ,','))as A where items= @vcExportToAPI ) 
				BEGIN
					SET @vcExportToAPI = @ExportToAPIList + ',' + @vcExportToAPI
				End
				ELSE
				BEGIN
					SET @vcExportToAPI = @ExportToAPIList 
				End
			END

			--update ExportToAPI String value
			UPDATE dbo.Item SET vcExportToAPI=@vcExportToAPI where numItemCode=@numItemCode  AND numDomainID=@numDomainID
					
			SELECT 
				@cnt = COUNT([numItemID]) 
			FROM 
				[ItemAPI] 
			WHERE  
				[WebApiId] = @intWebApiId 
				AND [numDomainId] = @numDomainID
				AND [vcAPIItemID] = @vcApiItemId
				AND numItemID = @numItemCode

			IF @cnt > 0
			BEGIN
				--update lastUpdated Date 
				UPDATE [ItemAPI] SET [dtModified] =GETUTCDATE() WHERE [WebApiId] =@intWebApiId AND [numDomainId] = @numDomainID
			END
			ELSE
			--Insert ItemAPI mapping
			BEGIN
				INSERT INTO [ItemAPI] 
				(
					[WebApiId],
					[numDomainId],
					[numItemID],
					[vcAPIItemID],
					[numCreatedby],
					[dtCreated],
					[numModifiedby],
					[dtModified],vcSKU
				) VALUES 
				(
					@intWebApiId,
					@numDomainID,
					@numItemCode,
					@vcApiItemId,
					@numUserCntID,
					GETUTCDATE(),
					@numUserCntID,
					GETUTCDATE(),@vcSKU
				)	
			END
		END   
	END                                            
	ELSE IF @numItemCode > 0 AND @intWebApiId <= 0                                                           
	BEGIN
		DECLARE @OldGroupID NUMERIC 
		SELECT @OldGroupID = ISNULL(numItemGroup,0) FROM [Item] WHERE numitemcode = @numItemCode AND [numDomainId] = @numDomainID
	
		DECLARE @monOldAverageCost AS DECIMAL(18,4);SET @monOldAverageCost=0
		SELECT @monOldAverageCost=(CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(monAverageCost,0) END) FROM Item WHERE numItemCode =@numItemCode AND [numDomainId] = @numDomainID
	   
		UPDATE 
			Item 
		SET 
			vcItemName=@vcItemName,txtItemDesc=@txtItemDesc,charItemType=@charItemType,monListPrice= @monListPrice,numItemClassification=@numItemClassification,                                             
			bitTaxable=@bitTaxable,vcSKU = (CASE WHEN ISNULL(@ParentSKU,'') = '' THEN @vcSKU ELSE @ParentSKU END),bitKitParent=@bitKitParent,                                             
			numVendorID=@numVendorID,numDomainID=@numDomainID,bintModifiedDate=getutcdate(),numModifiedBy=@numUserCntID,bitSerialized=@bitSerialized,                                                         
			vcModelID=@vcModelID,numItemGroup=@numItemGroup,numCOGsChartAcntId=@numCOGsChartAcntId,numAssetChartAcntId=@numAssetChartAcntId,                                      
			numIncomeChartAcntId=@numIncomeChartAcntId,monCampaignLabourCost=@monLabourCost,fltWeight=@fltWeight,fltHeight=@fltHeight,fltWidth=@fltWidth,                  
			fltLength=@fltLength,bitFreeShipping=@bitFreeshipping,bitAllowBackOrder=@bitAllowBackOrder,vcUnitofMeasure=@UnitofMeasure,bitShowDeptItem=@bitShowDeptItem,            
			bitShowDeptItemDesc=@bitShowDeptItemDesc,bitCalAmtBasedonDepItems=@bitCalAmtBasedonDepItems,bitAssembly=@bitAssembly,numBarCodeId=@numBarCodeId,
			vcManufacturer=@vcManufacturer,numBaseUnit=@numBaseUnit,numPurchaseUnit=@numPurchaseUnit,numSaleUnit=@numSaleUnit,bitLotNo=@bitLotNo,
			IsArchieve = @IsArchieve,numItemClass=@numItemClass,tintStandardProductIDType=@tintStandardProductIDType,vcExportToAPI=@vcExportToAPI,
			numShipClass=@numShipClass,bitAllowDropShip=@bitAllowDropShip, bitArchiveItem = @bitArchiveItem,bitAsset=@bitAsset,bitRental=@bitRental,
			bitVirtualInventory = @bitVirtualInventory,numContainer=@numContainer,numNoItemIntoContainer=@numNoItemIntoContainer,bitMatrix=@bitMatrix,numManufacturer=@numManufacturer,
			vcASIN = @vcASIN,tintKitAssemblyPriceBasedOn=@tintKitAssemblyPriceBasedOn,fltReorderQty=@fltReorderQty,bitSOWorkOrder=@bitSOWorkOrder,bitKitSingleSelect=@bitKitSingleSelect
			,bitExpenseItem=@bitExpenseItem,numExpenseChartAcntId=@numExpenseChartAcntId
		WHERE 
			numItemCode=@numItemCode 
			AND numDomainID=@numDomainID

		IF NOT EXISTS(SELECT numItemCode FROM dbo.OpportunityItems WHERE numItemCode = @numItemCode)
		BEGIN
			IF (((SELECT ISNULL(SUM(numOnHand),0) FROM WareHouseItems WHERE numItemID = @numItemCode AND numDomainId=@numDomainID) = 0) AND ((SELECT ISNULL(bitVirtualInventory,0) FROM Item  WHERE numItemCode = @numItemCode AND numDomainId=@numDomainID) = 0))
			BEGIN
				UPDATE Item SET monAverageCost=@monAverageCost  where numItemCode=@numItemCode  AND numDomainID=@numDomainID	
			END
		END
	
		--If Average Cost changed then add in Tracking
		IF ISNULL(@monAverageCost,0) != ISNULL(@monOldAverageCost,0)
		BEGIN
			DECLARE @numWareHouseItemID NUMERIC(9);SET @numWareHouseItemID=0
			DECLARE @numTotal int;SET @numTotal=0
			SET @i=0
			DECLARE @vcDescription AS VARCHAR(100);SET @vcDescription=''
		
			SELECT @numTotal=COUNT(*) FROM WareHouseItems where numItemID=@numItemCode AND numDomainId=@numDomainId
	     
			WHILE @i < @numTotal
			BEGIN
				SELECT @numWareHouseItemID = min(numWareHouseItemID) FROM WareHouseItems 
					WHERE numItemID=@numItemCode AND numDomainId=@numDomainId and numWareHouseItemID > @numWareHouseItemID 
				
				IF @numWareHouseItemID>0
				BEGIN
					SET @vcDescription='Average Cost Changed Manually OLD : ' + CAST(ISNULL(@monOldAverageCost,0) AS VARCHAR(20))
				
					DECLARE @numDomain AS NUMERIC(18,0)
					SET @numDomain = @numDomainID
					EXEC dbo.USP_ManageWareHouseItems_Tracking
						@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)		
						@numReferenceID = @numItemCode, --  numeric(9, 0)
						@tintRefType = 1, --  tinyint
						@vcDescription = @vcDescription, --  varchar(100)
						@tintMode = 0, --  tinyint
						@numModifiedBy = @numUserCntID, --  numeric(9, 0)
						@numDomainID = @numDomain
				END
		    
				SET @i = @i + 1
			END
		END
 
		DECLARE @bitCartFreeShipping as  bit 
		SELECT @bitCartFreeShipping = bitFreeShipping FROM dbo.CartItems where numItemCode = @numItemCode  AND numDomainID = @numDomainID 
 
		IF @bitCartFreeShipping <> @bitFreeshipping
		BEGIN
			UPDATE dbo.CartItems SET bitFreeShipping = @bitFreeshipping WHERE numItemCode = @numItemCode  AND numDomainID=@numDomainID
		END 
 
		IF @intWebApiId > 1 
		BEGIN
			SELECT 
				@cnt = COUNT([numItemID])
			FROM 
				[ItemAPI]
			WHERE 
				[WebApiId] = @intWebApiId
				AND [numDomainId] = @numDomainID
				AND [vcAPIItemID] = @vcApiItemId

			IF @cnt > 0
			BEGIN
				--update lastUpdated Date 
				UPDATE [ItemAPI] SET [dtModified] =GETUTCDATE() WHERE [WebApiId] =@intWebApiId AND [numDomainId] = @numDomainID
			END
			ELSE --Insert ItemAPI mapping
			BEGIN
				INSERT INTO [ItemAPI] 
				(
	 				[WebApiId],
	 				[numDomainId],
	 				[numItemID],
	 				[vcAPIItemID],
	 				[numCreatedby],
	 				[dtCreated],
	 				[numModifiedby],
	 				[dtModified],vcSKU
				 )
				 VALUES 
				 (
					@intWebApiId,
					@numDomainID,
					@numItemCode,
					@vcApiItemId,
	 				@numUserCntID,
	 				GETUTCDATE(),
	 				@numUserCntID,
	 				GETUTCDATE(),@vcSKU
	 			) 
			END
		END   
		                                                                            
		-- kishan It is necessary to add item into itemTax table . 
		IF	@bitTaxable = 1
		BEGIN
			IF NOT EXISTS(SELECT * FROM dbo.ItemTax WHERE numItemCode = @numItemCode AND numTaxItemID = 0)
			BEGIN
			    INSERT INTO dbo.ItemTax 
				(
					numItemCode,
					numTaxItemID,
					bitApplicable
				)
				VALUES 
				( 
					@numItemCode,
					0,
					1 
				) 						
			END
		END	 
      
		IF @charItemType='S' or @charItemType='N'                                                                       
		BEGIN
			DELETE FROM WareHouseItmsDTL where numWareHouseItemID in (select numWareHouseItemID from WareHouseItems where numItemID=@numItemCode AND [WareHouseItems].[numDomainID] = @numDomainID)
			DELETE FROM WareHouseItems where numItemID=@numItemCode AND [WareHouseItems].[numDomainID] = @numDomainID
		END                                      
   
		IF @bitKitParent=1 OR @bitAssembly = 1              
		BEGIN              
			DECLARE @hDoc1 AS INT                                            
			EXEC sp_xml_preparedocument @hDoc1 OUTPUT, @strChildItems                                                                        
                                             
			UPDATE 
				ItemDetails 
			SET
				numQtyItemsReq=X.QtyItemsReq
				,numUOMId=X.numUOMId
				,vcItemDesc=X.vcItemDesc
				,sintOrder=X.sintOrder                                                                                          
			From 
			(
				SELECT 
					QtyItemsReq,ItemKitID,ChildItemID,numUOMId,vcItemDesc,sintOrder, numItemDetailID As ItemDetailID                             
				FROM 
					OPENXML(@hDoc1,'/NewDataSet/Table1',2)                                                                         
				WITH
				(
					ItemKitID NUMERIC(9),                                                          
					ChildItemID NUMERIC(9),                                                                        
					QtyItemsReq DECIMAL(30, 16),
					numUOMId NUMERIC(9),
					vcItemDesc VARCHAR(1000),
					sintOrder int,
					numItemDetailID NUMERIC(18,0)
				)
			)X                                                                         
			WHERE 
				numItemKitID=X.ItemKitID 
				AND numChildItemID=ChildItemID 
				AND numItemDetailID = X.ItemDetailID

			EXEC sp_xml_removedocument @hDoc1
		 END   
		 ELSE
		 BEGIN
 			DELETE FROM ItemDetails WHERE numItemKitID=@numItemCode
		 END                                                
	END

	IF ISNULL(@numItemGroup,0) = 0 OR ISNULL(@bitMatrix,0) = 1
	BEGIN
		UPDATE WareHouseItems SET monWListPrice = @monListPrice, vcWHSKU=@vcSKU, vcBarCode=@numBarCodeId WHERE numDomainID = @numDomainID AND numItemID = @numItemCode
	END

	IF @numItemCode > 0
	BEGIN
		DELETE 
			IC
		FROM 
			dbo.ItemCategory IC
		INNER JOIN 
			Category 
		ON 
			IC.numCategoryID = Category.numCategoryID
		WHERE 
			numItemID = @numItemCode		
			AND numCategoryProfileID = @numCategoryProfileID

		IF @vcCategories <> ''	
		BEGIN
			INSERT INTO ItemCategory( numItemID , numCategoryID )  SELECT  @numItemCode AS numItemID,ID from dbo.SplitIDs(@vcCategories,',')	
		END
	END

	


	-- SAVE MATRIX ITEM ATTRIBUTES
	IF @numItemCode > 0 AND ISNULL(@bitAttributesChanged,0)=1 AND ISNULL(@bitMatrix,0) = 1 AND LEN(ISNULL(@vcItemAttributes,'')) > 0 AND ISNULL(@bitOppOrderExists,0) <> 1 AND (SELECT COUNT(*) FROM @TempTable) > 0
	BEGIN
		-- DELETE PREVIOUS ITEM ATTRIBUTES
		DELETE FROM ItemAttributes WHERE numDomainID=@numDomainID AND numItemCode=@numItemCode

		-- INSERT NEW ITEM ATTRIBUTES
		INSERT INTO ItemAttributes
		(
			numDomainID
			,numItemCode
			,FLD_ID
			,FLD_Value
		)
		SELECT
			@numDomainID
			,@numItemCode
			,Fld_ID
			,Fld_Value
		FROM
			@TempTable

		IF (SELECT COUNT(*) FROM WarehouseItems WHERE numDomainID=@numDomainID AND numItemID=@numItemCode) > 0
		BEGIN

			-- DELETE PREVIOUS WAREHOUSE ATTRIBUTES
			DELETE FROM CFW_Fld_Values_Serialized_Items WHERE RecId IN (SELECT ISNULL(numWarehouseItemID,0) FROM WarehouseItems WHERE numDomainID=@numDomainID AND numItemID=@numItemCode)

			-- INSERT NEW WAREHOUSE ATTRIBUTES
			INSERT INTO CFW_Fld_Values_Serialized_Items
			(
				Fld_ID,
				Fld_Value,
				RecId,
				bitSerialized
			)                                                  
			SELECT 
				Fld_ID,
				ISNULL(CAST(Fld_Value AS VARCHAR),'') AS Fld_Value,
				TEMPWarehouse.numWarehouseItemID,
				0 
			FROM 
				@TempTable  
			OUTER APPLY
			(
				SELECT
					numWarehouseItemID
				FROM 
					WarehouseItems 
				WHERE 
					numDomainID=@numDomainID 
					AND numItemID=@numItemCode
			) AS TEMPWarehouse
		END
	END	  
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH        
END
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageShippingRule')
DROP PROCEDURE USP_ManageShippingRule
GO
CREATE PROCEDURE USP_ManageShippingRule
	@numRuleID NUMERIC(9),
	@vcRuleName VARCHAR(50),
	@vcDescription VARCHAR(1000),
	@tintBasedOn TINYINT,
	@tinShippingMethod TINYINT,
	@tintType TINYINT,
	@numDomainID NUMERIC(9),
	@tintTaxMode TINYINT,
	@tintFixedShippingQuotesMode TINYINT,
	@bitFreeshipping BIT,
	@FreeShippingAmt INT = NULL,
	@numRelationship NUMERIC(9),
	@numProfile NUMERIC(9),
	@strWarehouseIds VARCHAR(1000)
AS 
BEGIN
	IF(SELECT COUNT(*) FROM dbo.ShippingRules WHERE vcRuleName = @vcRuleName AND numDomainId=@numDomainId AND numRuleID<>@numRuleID)>0
	BEGIN
 		RAISERROR ( 'DUPLICATE',16, 1 )
 		RETURN ;
	END

    IF ISNULL(@numRuleID,0) = 0
    BEGIN  
        INSERT INTO dbo.ShippingRules 
		(	
			vcRuleName
            ,vcDescription
            ,tintBasedOn
            ,tinShippingMethod
            ,tintIncludeType
            ,numDomainID
            ,tintTaxMode
            ,tintFixedShippingQuotesMode 
			,bitFreeShipping
			,FreeShippingOrderAmt
			,numRelationship
			,numProfile
			,numWareHouseID
		)
        VALUES 
		(
            @vcRuleName
            ,@vcDescription
            ,@tintBasedOn
            ,@tinShippingMethod
            ,@tintType
            ,@numDomainID 
            ,@tintTaxMode 
            ,@tintFixedShippingQuotesMode
			,@bitFreeshipping
			,CASE WHEN @bitFreeshipping = 0 THEN NULL ELSE @FreeShippingAmt END
			,@numRelationship
			,@numProfile
			,@strWarehouseIds
		)
		   
        SELECT numRuleID = SCOPE_IDENTITY()
    END  
    ELSE 
    BEGIN  
		IF (SELECT 
				COUNT(*) 
			FROM 
				dbo.ShippingRules SR
			WHERE 
				numDomainId=@numDomainId 
				AND numRuleID <> ISNULL(@numRuleID,0) 
				AND ISNULL(numRelationship,0) = ISNULL(@numRelationship,0)
				AND ISNULL(numProfile,0) = ISNULL(@numProfile,0)
				AND EXISTS (SELECT 
								SRSL1.numRuleID
							FROM 
								ShippingRuleStateList SRSL1 
							INNER JOIN
								ShippingRuleStateList SRSL2
							ON
								ISNULL(SRSL1.numStateID,0) = ISNULL(SRSL2.numStateID,0)
								AND ISNULL(SRSL1.vcZipPostal,'') = ISNULL(SRSL2.vcZipPostal,'')
								AND ISNULL(SRSL1.numCountryID,0) = ISNULL(SRSL2.numCountryID,0)
							WHERE 
								SRSL1.numDomainID=@numDomainID 
								AND SRSL1.numRuleID = SR.numRuleID
								AND SRSL2.numRuleID = @numRuleID)
			) > 0
		BEGIN
 			RAISERROR ( 'DUPLICATE RELATIONSHIP, PROFILE AND STATE-ZIP',16, 1 )
 			RETURN ;
		END

		UPDATE 
			dbo.ShippingRules
		SET 
			vcRuleName = @vcRuleName
			,vcDescription = @vcDescription
			,tintBasedOn = @tintBasedOn
			,tinShippingMethod = @tinShippingMethod
			,tintIncludeType = @tintType 
			,tintTaxMode = @tintTaxMode
            ,tintFixedShippingQuotesMode = @tintFixedShippingQuotesMode
			,bitFreeShipping = @bitFreeshipping
			,FreeShippingOrderAmt = (CASE WHEN @bitFreeshipping = 0 THEN NULL Else @FreeShippingAmt End)
			,numRelationship = @numRelationship
			,numProfile = @numProfile
			,numWareHouseID = @strWarehouseIds
		WHERE 
			numRuleID = @numRuleID
			AND numDomainID = @numDomainID			          

        SELECT  @numRuleID
    END
END
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ManageShippingRuleStates' ) 
    DROP PROCEDURE USP_ManageShippingRuleStates
GO
CREATE PROCEDURE USP_ManageShippingRuleStates   
    @numCountryID NUMERIC,
    @numStateID NUMERIC,
    @numRuleID NUMERIC,
    @numDomainID NUMERIC,
	@vcZipPostalRange VARCHAR(200)
AS 
BEGIN
	DECLARE @numRelationship NUMERIC(18,0)
	DECLARE @numProfile NUMERIC(18,0)

	SELECT 
		@numRelationship=numRelationship
		,@numProfile=numProfile 
	FROM 
		ShippingRules 
	WHERE 
		numDomainID=@numDomainID 
		AND numRuleID=@numRuleID

	DECLARE @TEMP TABLE
	(
		numDomainID NUMERIC(18,0)
		,numRuleID NUMERIC(18,0)
		,numCountryID NUMERIC(18,0)
		,numStateID NUMERIC(18,0)
		,vcZipPostal VARCHAR(20)
	)

	IF LEN(ISNULL(@vcZipPostalRange,'')) > 0
	BEGIN
		INSERT INTO @TEMP
		(
			numDomainID
			,numRuleID
			,numCountryID
			,numStateID
			,vcZipPostal
		)
		SELECT
			@numDomainID
			,@numRuleID
			,@numCountryID
			,@numStateID
			,OutParam
		FROM
			dbo.SplitString(@vcZipPostalRange,',')
	END
	ELSE
	BEGIN
		INSERT INTO @TEMP
		(
			numDomainID
			,numRuleID
			,numCountryID
			,numStateID
			,vcZipPostal
		)
		SELECT
			@numDomainID
			,@numRuleID
			,@numCountryID
			,@numStateID
			,''
	END

	IF (SELECT 
			COUNT(*) 
		FROM 
			dbo.ShippingRules SR
		WHERE 
			numDomainId=@numDomainId 
			AND numRuleID <> ISNULL(@numRuleID,0) 
			AND ISNULL(numRelationship,0) = ISNULL(@numRelationship,0)
			AND ISNULL(numProfile,0) = ISNULL(@numProfile,0)
			AND EXISTS (SELECT 
							SRSL1.numRuleID
						FROM 
							ShippingRuleStateList SRSL1 
						INNER JOIN
							@Temp SRSL2
						ON
							ISNULL(SRSL1.numStateID,0) = ISNULL(SRSL2.numStateID,0)
							AND ISNULL(SRSL1.vcZipPostal,'') = ISNULL(SRSL2.vcZipPostal,'')
							AND ISNULL(SRSL1.numCountryID,0) = ISNULL(SRSL2.numCountryID,0)
						WHERE 
							SRSL1.numDomainID=@numDomainID 
							AND SRSL1.numRuleID = SR.numRuleID
							AND SRSL2.numRuleID = @numRuleID)
			) > 0
	BEGIN
 		RAISERROR ( 'DUPLICATE RELATIONSHIP, PROFILE AND STATE-ZIP',16, 1 )
 		RETURN ;
	END

	INSERT INTO ShippingRuleStateList
	(
		numCountryID,				
		numStateID,
		numRuleID,
		numDomainID,
		vcZipPostal
	)
	SELECT 
		T1.numCountryID
		,T1.numStateID
		,T1.numRuleID
		,T1.numDomainID
		,T1.vcZipPostal
	FROM
		@TEMP T1
	LEFT JOIN
		ShippingRuleStateList SRSL
	ON
		SRSL.numRuleID = @numRuleID
		AND T1.numRuleID = SRSL.numRuleID
		AND T1.numCountryID = SRSL.numCountryID
		AND T1.numStateID = SRSL.numStateID
		AND ISNULL(T1.vcZipPostal,'') = ISNULL(SRSL.vcZipPostal,'')
	WHERE
		SRSL.numRuleStateID IS NULL							
					
END

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_OPPBizDocItems_GetWithKitChilds' ) 
    DROP PROCEDURE USP_OPPBizDocItems_GetWithKitChilds
GO
CREATE PROCEDURE [dbo].[USP_OPPBizDocItems_GetWithKitChilds]
(
    @numOppId AS NUMERIC(9) = NULL,
    @numOppBizDocsId AS NUMERIC(9) = NULL,
    @numDomainID AS NUMERIC(9) = 0      
)
AS 
BEGIN
    DECLARE @DivisionID AS NUMERIC(9)      
    DECLARE @tintTaxOperator AS TINYINT
	DECLARE @tintCommitAllocation TINYINT
	SELECT @tintCommitAllocation=ISNULL(tintCommitAllocation,1) FROM Domain WHERE numDomainId=@numDomainID
                                                                                                                                                                                                                                                                                                                                          
    SELECT  
		@DivisionID = numDivisionID, 
		@tintTaxOperator = [tintTaxOperator]           
    FROM    
		OpportunityMaster
    WHERE   
		numOppId = @numOppId                                                                                                                                                                          

    
                                                                                          
    DECLARE @strSQL AS VARCHAR(MAX)                                                                                          
    DECLARE @strSQL1 AS VARCHAR(8000)                                       
    DECLARE @strSQLCusFields VARCHAR(1000) = ''
    DECLARE @strSQLEmpFlds VARCHAR(500) = ''                                                                         
                                                                                              
    DECLARE @tintType AS TINYINT   
    DECLARE @tintOppType AS TINYINT                                                      
    DECLARE @intRowNum AS INT                                                  
    DECLARE @numFldID AS VARCHAR(15)                                          
    DECLARE @vcFldname AS VARCHAR(50)                                            
    DECLARE @numBizDocId AS NUMERIC(9)
    DECLARE @numBizDocTempID AS NUMERIC(9) = 0
	DECLARE @bitDisplayKitChild AS BIT = 0
        
                                           
    SELECT  
		@numBizDocTempID = ISNULL(numBizDocTempID, 0),
        @numBizDocId = numBizDocId
    FROM    
		OpportunityBizDocs
    WHERE   
		numOppBizDocsId = @numOppBizDocsId

	
	SELECT
		@bitDisplayKitChild = ISNULL(bitDisplayKitChild,0)
	FROM	
		BizDocTemplate
	WHERE
		numDomainID = @numDomainID
		AND numBizDocTempID = @numBizDocTempID


    SELECT  
		@tintType = tintOppType,
        @tintOppType = tintOppType
    FROM    
		OpportunityMaster
    WHERE   
		numOppID = @numOppId                                                                                          
                                                                                   
    IF @tintType = 1 
        SET @tintType = 7
    ELSE 
        SET @tintType = 8     
		
		
	IF (SELECT 
			COUNT(*) 
		FROM 
			View_DynamicColumns 
		WHERE   
			numFormID = @tintType 
			AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId 
			AND vcFieldType = 'R'
			AND ISNULL(numRelCntType, 0) = @numBizDocTempID
		)=0
	BEGIN            
		INSERT INTO DycFormConfigurationDetails 
		(
			numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numAuthGroupID,numRelCntType
		)
		SELECT 
			numFormId,numFieldId,1,tintRow,@numDomainID,@numBizDocId,@numBizDocTempID
		FROM 
			View_DynamicDefaultColumns
		WHERE 
			numFormId=@tintType and bitDefault=1 and numDomainID=@numDomainID order by tintRow ASC	
	 
	 
		IF (SELECT 
				COUNT(*) 
			FROM 
				DycFrmConfigBizDocsSumm Ds 
			WHERE 
				numBizDocID=@numBizDocId 
				AND Ds.numFormID=@tintType 
				AND ISNULL(Ds.numBizDocTempID,0)=@numBizDocTempID 
				AND Ds.numDomainID=@numDomainID
			)=0
		BEGIN
			INSERT INTO DycFrmConfigBizDocsSumm 
			(
				numFormID,numBizDocID,numFieldID,numDomainID,numBizDocTempID
			)
			SELECT 
				@tintType,@numBizDocId,DFM.numFieldID,@numDomainID,@numBizDocTempID 
			FROM
				DycFormField_Mapping DFFM 
			JOIN 
				DycFieldMaster DFM 
			ON 
				DFFM.numModuleID=DFFM.numModuleID and DFFM.numFieldID=DFM.numFieldID
			WHERE 
				numFormID=16 
				AND DFFM.numDomainID IS NULL
		END
	END      

	IF OBJECT_ID('tempdb..#TempBizDocProductGridColumns') IS NOT NULL DROP TABLE #TempBizDocProductGridColumns
	SELECT * INTO #TempBizDocProductGridColumns FROM (
    SELECT  
		vcFieldName AS vcFormFieldName,
        vcDbColumnName,
        vcFieldDataType,
        tintRow AS intRowNum
    FROM    
		View_DynamicColumns
    WHERE   
		numFormID = @tintType
        AND numDomainID = @numDomainID
        AND numAuthGroupID = @numBizDocId
        AND vcFieldType = 'R'
        AND ISNULL(numRelCntType, 0) = @numBizDocTempID
    UNION
    SELECT  
		vcFieldName AS vcFormFieldName,
        vcDbColumnName,
        'C' vcFieldDataType,
        tintRow AS intRowNum
    FROM    
		View_DynamicCustomColumns
    WHERE   
		numFormID = @tintType
        AND Grp_id = 5
        AND numDomainID = @numDomainID
        AND numAuthGroupID = @numBizDocId
        AND bitCustom = 1
        AND ISNULL(numRelCntType, 0) = @numBizDocTempID
    ) t1   
	ORDER BY 
		intRowNum                                                                              
                                                                                
    SELECT  
		*
    INTO    
		#Temp1
    FROM    
	( 
		SELECT   
			Opp.vcitemname AS vcItemName,
            ISNULL(OBD.numOppBizDocItemID, 0) OppBizDocItemID,
			(CASE 
				WHEN OM.tintOppType = 1 AND OM.tintOppStatus = 1 
				THEN 
					(CASE 
						WHEN (I.charItemType = 'p' AND ISNULL(Opp.bitDropShip, 0) = 0 AND ISNULL(I.bitAsset,0)=0)
						THEN 
							CAST(dbo.CheckOrderItemInventoryStatus(Opp.numItemCode,Opp.numUnitHour,Opp.numoppitemtCode,Opp.numWarehouseItmsID,(CASE WHEN ISNULL(bitKitParent,0) = 1 AND ISNULL(bitAssembly,0) = 1 THEN 0  WHEN ISNULL(bitKitParent,0) = 1 THEN 1 ELSE 0 END)) AS VARCHAR)
						ELSE ''
					END) 
				ELSE ''
			END) vcBackordered,
			ISNULL(WI.numBackOrder,0) AS numBackOrder,
            CASE WHEN charitemType = 'P' THEN 'Product'
                    WHEN charitemType = 'S' THEN 'Service'
            END AS charitemType,
            CASE WHEN charitemType = 'P' THEN 'Inventory'
	                WHEN charitemType = 'N' THEN 'Non-Inventory'
                    WHEN charitemType = 'S' THEN 'Service'
            END AS vcItemType,
			CONVERT(VARCHAR(2500), OBD.vcitemDesc) AS txtItemDesc, 
			opp.numUnitHour AS numOrgTotalUnitHour,
			dbo.fn_UOMConversion(ISNULL(i.numBaseUnit, 0),i.numItemCode, @numDomainID,ISNULL(opp.numUOMId, 0)) * opp.numUnitHour AS numTotalUnitHour,                                   
            dbo.fn_UOMConversion(ISNULL(i.numBaseUnit, 0),i.numItemCode, @numDomainID,ISNULL(opp.numUOMId, 0)) * OBD.numUnitHour AS numUnitHour,
            CONVERT(DECIMAL(20,5), (dbo.fn_UOMConversion(ISNULL(opp.numUOMId, 0), i.numItemCode, @numDomainID, ISNULL(i.numBaseUnit, 0)) * OBD.monPrice)) monPrice,
            CONVERT(VARCHAR, CONVERT(DECIMAL(20,5), OBD.monTotAmount)) Amount,
            OBD.monTotAmount/*Fo calculating sum*/,
            ISNULL(CONVERT(VARCHAR, i.monListPrice), 0) AS monListPrice,
            i.numItemCode AS ItemCode,
            opp.numoppitemtCode,
            L.vcdata AS numItemClassification,
            CASE WHEN bitTaxable = 0 THEN 'No'
                    ELSE 'Yes'
            END AS Taxable,
            numIncomeChartAcntId AS itemIncomeAccount,
            'NI' AS ItemType,
            ISNULL(GJH.numJournal_Id, 0) AS numJournalId,
            ( SELECT TOP 1
                        ISNULL(GJD.numTransactionId, 0)
                FROM      General_Journal_Details GJD
                WHERE     GJH.numJournal_Id = GJD.numJournalId
                        AND GJD.chBizDocItems = 'NI'
                        AND GJD.numoppitemtCode = opp.numoppitemtCode
            ) AS numTransactionId,
            ISNULL(Opp.vcModelID, '') vcModelID,
            (CASE WHEN charitemType = 'P' THEN dbo.USP_GetAttributes(OBD.numWarehouseItmsID, bitSerialized) ELSE ISNULL(OBD.vcAttributes,0) END) AS vcAttributes,
            ISNULL(vcPartNo, '') AS vcPartNo,
           (CASE WHEN ISNULL(opp.bitMarkupDiscount,0) = 1 
					THEN 0
					ELSE (ISNULL(OBD.monTotAmtBefDiscount, OBD.monTotAmount) - OBD.monTotAmount ) 
			END)
            AS DiscAmt,		
			(CASE WHEN OBD.numUnitHour = 0 THEN 0 ELSE CONVERT(DECIMAL(20,5),(dbo.fn_UOMConversion(ISNULL(opp.numUOMId, 0), i.numItemCode, @numDomainID, ISNULL(i.numBaseUnit, 0)) * (ISNULL(OBD.monTotAmount,0) / OBD.numUnitHour))) END) As monUnitSalePrice,
            ISNULL(OBD.vcNotes,ISNULL(opp.vcNotes,'')) vcNotes,
            OBD.vcTrackingNo,
            OBD.vcShippingMethod,
            OBD.monShipCost,
            [dbo].[FormatedDateFromDate](OBD.dtDeliveryDate,@numDomainID) dtDeliveryDate,
            Opp.numWarehouseItmsID,
            ISNULL(u.vcUnitName, '') numUOMId,
            ISNULL(Opp.vcManufacturer, '') AS vcManufacturer,
            ISNULL(V.monCost, 0) AS VendorCost,
            Opp.vcPathForTImage,
            i.[fltLength],
            i.[fltWidth],
            i.[fltHeight],
            i.[fltWeight],
            ISNULL(I.numVendorID, 0) numVendorID,
            Opp.bitDropShip AS DropShip,
            SUBSTRING(( SELECT  ' ,' + vcSerialNo
                                + CASE WHEN ISNULL(I.bitLotNo, 0) = 1
                                        THEN ' ('
                                            + CONVERT(VARCHAR(15), oppI.numQty)
                                            + ')'
                                        ELSE ''
                                    END
                        FROM    OppWarehouseSerializedItem oppI
                                JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID = whi.numWareHouseItmsDTLID
                        WHERE   oppI.numOppID = Opp.numOppId
                                AND oppI.numOppItemID = Opp.numoppitemtCode
                        ORDER BY vcSerialNo
                        FOR
                        XML PATH('')
                        ), 3, 200000) AS SerialLotNo,
            ISNULL(opp.numUOMId, 0) AS numUOM,
            ISNULL(u.vcUnitName, '') vcUOMName,
            dbo.fn_UOMConversion(opp.numUOMId, Opp.numItemCode,
                                    @numDomainID, NULL) AS UOMConversionFactor,
            ISNULL(Opp.numSOVendorId, 0) AS numSOVendorId,
            [dbo].FormatedDateTimeFromDate(OBD.dtRentalStartDate,
                                            @numDomainID) dtRentalStartDate,
            [dbo].FormatedDateTimeFromDate(OBD.dtRentalReturnDate,
                                            @numDomainID) dtRentalReturnDate,
            ISNULL(W.vcWareHouse, '') AS Warehouse,
            CASE WHEN charitemType = 'P' AND ISNULL(numItemGroup,0) > 0 AND ISNULL(bitMatrix,0)=0 THEN ISNULL(WI.vcWHSKU,I.vcSKU) ELSE I.vcSKU END vcSKU,
            CASE WHEN charitemType = 'P' AND ISNULL(numItemGroup,0) > 0 AND ISNULL(bitMatrix,0)=0 THEN ISNULL(WI.vcBarCode,I.numBarCodeId) ELSE I.numBarCodeId END vcBarcode,
			CASE WHEN charitemType = 'P' AND ISNULL(numItemGroup,0) > 0 AND ISNULL(bitMatrix,0)=0 THEN ISNULL(WI.vcBarCode,I.numBarCodeId) ELSE I.numBarCodeId END numBarCodeId,
            ISNULL(opp.numClassID, 0) AS numClassID,
            ISNULL(opp.numProjectID, 0) AS numProjectID,
            ISNULL(I.[numShipClass],0) AS numShipClass,OBD.numOppBizDocID,WL.vcLocation,ISNULL(bitSerialized,0) AS bitSerialized,ISNULL(bitLotNo,0) AS bitLotNo,
            opp.numOppId,OBD.numUnitHour AS numUnitHourOrig,ISNULL(I.numItemCode,0) AS [numItemCode],
			ISNULL(I.bitKitParent,0) AS bitKitParent,
			0 AS numOppChildItemID, 
			0 AS numOppKitChildItemID,
			0 AS bitChildItem,
			0 AS tintLevel,
			ISNULL(opp.vcPromotionDetail,'') AS vcPromotionDetail,
			(CASE WHEN ISNULL(WI.numWareHouseItemID,0) > 0 THEN W.vcWareHouse ELSE '' END) vcWarehouse,
			(CASE WHEN (SELECT COUNT(*) FROM #TempBizDocProductGridColumns WHERE vcDbColumnName='vcInclusionDetails') > 0 THEN dbo.GetOrderAssemblyKitInclusionDetails(opp.numOppID,Opp.numoppitemtCode,OBD.numUnitHour,@tintCommitAllocation,1) ELSE '' END) AS vcInclusionDetails,
			Opp.numSortOrder
			,CAST(ISNULL(Opp.numCost,0) AS DECIMAL(20,5)) AS numCost, numOnHand
			,dbo.FormatedDateFromDate(opp.ItemReleaseDate,@numDomainID) dtReleaseDate
			,(CASE WHEN I.bitKitParent = 1 THEN ISNULL(dbo.fn_GetKitAvailabilityByWarehouse(I.numItemCode,W.numWareHouseID),0) ELSE (ISNULL(numOnHand,0) + ISNULL(numAllocation,0)) END) AS numAvailable
			,CPN.CustomerPartNo 
        FROM      
			OpportunityItems opp
        JOIN 
			OpportunityBizDocItems OBD 
		ON 
			OBD.numOppItemID = opp.numoppitemtCode
		JOIN
			OpportunityBizDocs OB
		ON
			OBD.numOppBizDocID=OB.numOppBizDocsID
        LEFT JOIN 
			item i 
		ON 
			opp.numItemCode = i.numItemCode
        LEFT JOIN 
			ListDetails L 
		ON 
			i.numItemClassification = L.numListItemID
        LEFT JOIN 
			Vendor V 
		ON 
			V.numVendorID = @DivisionID
            AND V.numItemCode = opp.numItemCode
        LEFT JOIN 
			General_Journal_Header GJH 
		ON 
			opp.numoppid = GJH.numOppId
            AND GJH.numBizDocsPaymentDetId IS NULL
            AND GJH.numOppBizDocsId = @numOppBizDocsId
        LEFT JOIN 
			UOM u 
		ON 
			u.numUOMId = opp.numUOMId                                      
        LEFT JOIN 
			dbo.WareHouseItems WI 
		ON 
			opp.numWarehouseItmsID = WI.numWareHouseItemID
			AND WI.numDomainID = @numDomainID
        LEFT JOIN 
			dbo.Warehouses W 
		ON 
			WI.numDomainID = W.numDomainID
            AND WI.numWareHouseID = W.numWareHouseID
        LEFT JOIN 
			dbo.WarehouseLocation WL 
		ON 
			WL.numWLocationID = WI.numWLocationID
		LEFT JOIN
			dbo.OpportunityMaster OM
		ON 
			OM.numOppId = opp.numOppId
		LEFT JOIN	
			DivisionMaster DM
		ON
			OM.numDivisionId= DM.numDivisionID
		LEFT JOIN 
			CustomerPartNumber CPN
		ON 
			opp.numItemCode = CPN.numItemCode AND CPN.numDomainId = @numDomainID AND CPN.numCompanyId = DM.numCompanyID
        WHERE     
			opp.numOppId = @numOppId
            AND (bitShowDeptItem IS NULL OR bitShowDeptItem = 0)
            AND OBD.numOppBizDocID = @numOppBizDocsId
    ) X
	ORDER BY
		numSortOrder

	--GET KIT CHILD ITEMS (NOW WE ARE SUPPORTING KIT WITHIN KIT BUT ONLY 1 LEVEL MEANS CHILD KIT INSIDE PARENT KIT CAN NOT HAVE ANOTHER KIT AS CHILD)
	IF (@bitDisplayKitChild=1 AND (SELECT COUNT(*) FROM OpportunityKitItems WHERE numOppId = @numOppId ) > 0)
	BEGIN
		DECLARE @TEMPTABLE TABLE(
			numOppItemCode NUMERIC(18,0),
			numOppChildItemID NUMERIC(18,0), 
			numOppKitChildItemID NUMERIC(18,0),
			numItemCode NUMERIC(18,0),
			numWarehouseItemID NUMERIC(18,0),
			numTotalQty FLOAT,
			numQty FLOAT,
			numUOMID NUMERIC(18,0),
			numQtyShipped FLOAT,
			tintLevel TINYINT,
			numSortOrder INT
		);

		--FIRST LEVEL CHILD
		INSERT INTO @TEMPTABLE
		(
			numOppItemCode,numOppChildItemID,numOppKitChildItemID,numItemCode,numWarehouseItemID,numTotalQty,numQty,numUOMID,numQtyShipped,tintLevel,numSortOrder
		)
		SELECT 
			OKI.numOppItemID,
			OKI.numOppChildItemID,
			CAST(0 AS NUMERIC(18,0)),
			OKI.numChildItemID,
			ISNULL(OKI.numWareHouseItemId,0),
			CAST((t1.numOrgTotalUnitHour * OKI.numQtyItemsReq_Orig) AS FLOAT),
			CAST((t1.numUnitHour * OKI.numQtyItemsReq_Orig) AS FLOAT),
			ISNULL(OKI.numUOMID,0),
			ISNULL(numQtyShipped,0)
			,1
			,t1.numSortOrder
		FROM 
			#Temp1 t1
		INNER JOIN
			OpportunityKitItems OKI
		ON
			t1.numoppitemtCode = OKI.numOppItemID
		WHERE
			t1.bitKitParent = 1

		--SECOND LEVEL CHILD
		INSERT INTO @TEMPTABLE
		(
			numOppItemCode,numOppChildItemID,numOppKitChildItemID,numItemCode,numWarehouseItemID,numTotalQty,numQty,numUOMID,numQtyShipped,tintLevel,numSortOrder
		)
		SELECT 
			OKCI.numOppItemID,
			OKCI.numOppChildItemID,
			OKCI.numOppKitChildItemID,
			OKCI.numItemID,
			ISNULL(OKCI.numWareHouseItemId,0),
			CAST((c.numTotalQty * OKCI.numQtyItemsReq_Orig) AS NUMERIC(18,2)),
			CAST((c.numQty * OKCI.numQtyItemsReq_Orig) AS NUMERIC(18,2)),
			ISNULL(OKCI.numUOMID,0),
			ISNULL(OKCI.numQtyShipped,0),
			2
			,c.numSortOrder
		FROM 
			OpportunityKitChildItems OKCI
		INNER JOIN
			@TEMPTABLE c
		ON
			OKCI.numOppID = @numOppId
			AND OKCI.numOppItemID = c.numOppItemCode
			AND OKCI.numOppChildItemID = c.numOppChildItemID

		INSERT INTO 
			#Temp1
		SELECT   
			(CASE WHEN t2.tintLevel = 1 THEN CONCAT('&nbsp;&nbsp;',I.vcItemName) WHEN t2.tintLevel = 2 THEN CONCAT('&nbsp;&nbsp;&nbsp;&nbsp;',I.vcItemName) ELSE I.vcItemName END) AS vcItemName,
            0 OppBizDocItemID
			,(CASE WHEN (t2.numTotalQty-ISNULL(t2.numQtyShipped,0)) > ISNULL(WI.numAllocation,0) THEN ((t2.numTotalQty-ISNULL(t2.numQtyShipped,0)) - ISNULL(WI.numAllocation,0)) * dbo.fn_UOMConversion(ISNULL(i.numBaseUnit, 0),i.numItemCode, @numDomainID,ISNULL(t2.numUOMID, 0))  ELSE 0 END) AS vcBackordered
			,ISNULL(WI.numBackOrder,0) AS numBackOrder
            ,(CASE WHEN I.charitemType = 'P' THEN 'Product' WHEN charitemType = 'S' THEN 'Service' END) AS charitemType,
            (CASE WHEN charitemType = 'P' THEN 'Inventory'
	                WHEN charitemType = 'N' THEN 'Non-Inventory'
                    WHEN charitemType = 'S' THEN 'Service'
            END) AS vcItemType,
			CONVERT(VARCHAR(2500), I.txtItemDesc) AS txtItemDesc,   
			t2.numTotalQty AS numOrigTotalUnitHour,
			t2.numTotalQty AS numTotalUnitHour,
            t2.numQty AS numUnitHour,
            (ISNULL(dbo.fn_UOMConversion(ISNULL(t2.numUOMID, 0), i.numItemCode, @numDomainID, ISNULL(i.numBaseUnit, 0)),1) * (CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END)) monPrice,
            CONVERT(VARCHAR, CONVERT(DECIMAL(20,5),((t2.numQty * ISNULL(dbo.fn_UOMConversion(ISNULL(t2.numUOMID, 0), i.numItemCode, @numDomainID, ISNULL(i.numBaseUnit, 0)),1)) * (CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END)))) Amount,
            ((t2.numQty * ISNULL(dbo.fn_UOMConversion(ISNULL(t2.numUOMID, 0), i.numItemCode, @numDomainID, ISNULL(i.numBaseUnit, 0)),1)) * (CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END)) AS monTotAmount/*Fo calculating sum*/,
            ISNULL(CONVERT(VARCHAR, (CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END)), 0) AS monListPrice,
            I.numItemCode AS ItemCode,
            t2.numOppItemCode,
            L.vcdata AS numItemClassification,
            CASE WHEN bitTaxable = 0 THEN 'No' ELSE 'Yes' END AS Taxable,
            numIncomeChartAcntId AS itemIncomeAccount,
            'NI' AS ItemType,
            0 AS numJournalId,
            0 AS numTransactionId,
            ISNULL(I.vcModelID, '') vcModelID,
            dbo.USP_GetAttributes(t2.numWarehouseItemID, bitSerialized) AS vcAttributes,
            ISNULL(vcPartNo, '') AS vcPartNo,
            0 AS DiscAmt,
			(ISNULL(dbo.fn_UOMConversion(ISNULL(t2.numUOMID, 0), i.numItemCode, @numDomainID, ISNULL(i.numBaseUnit, 0)),1) * (CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END)) As monUnitSalePrice,
            '' AS vcNotes,'' AS vcTrackingNo,'' AS vcShippingMethod,0 AS monShipCost,NULL AS dtDeliveryDate,t2.numWarehouseItemID,
            ISNULL(u.vcUnitName, '') numUOMId,ISNULL(I.vcManufacturer, '') AS vcManufacturer,ISNULL(V.monCost, 0) AS VendorCost,
            ISNULL((SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = I.numItemCode AND bitDefault=1),'') AS vcPathForTImage,
            I.[fltLength],I.[fltWidth],I.[fltHeight],I.[fltWeight],ISNULL(I.numVendorID, 0) numVendorID,0 AS DropShip,
            '' AS SerialLotNo,
            ISNULL(t2.numUOMId, 0) AS numUOM,
            ISNULL(u.vcUnitName, '') vcUOMName,
            dbo.fn_UOMConversion(t2.numUOMId, t2.numItemCode,@numDomainID, NULL) AS UOMConversionFactor,
            0 AS numSOVendorId,
            NULL AS dtRentalStartDate,
            NULL dtRentalReturnDate,
            ISNULL(W.vcWareHouse, '') AS Warehouse,
            CASE WHEN charitemType = 'P' AND ISNULL(numItemGroup,0) > 0 AND ISNULL(bitMatrix,0)=0 THEN ISNULL(WI.vcWHSKU,I.vcSKU) ELSE I.vcSKU END vcSKU,
            CASE WHEN charitemType = 'P' AND ISNULL(numItemGroup,0) > 0 AND ISNULL(bitMatrix,0)=0 THEN ISNULL(WI.vcBarCode,I.numBarCodeId) ELSE I.numBarCodeId END vcBarcode,
			CASE WHEN charitemType = 'P' AND ISNULL(numItemGroup,0) > 0 AND ISNULL(bitMatrix,0)=0 THEN ISNULL(WI.vcBarCode,I.numBarCodeId) ELSE I.numBarCodeId END numBarCodeId,
            ISNULL(I.numItemClass, 0) AS numClassID,
            0 AS numProjectID,
            ISNULL(I.[numShipClass],0) AS numShipClass,
			@numOppBizDocsId AS numOppBizDocID,WL.vcLocation,ISNULL(bitSerialized,0) AS bitSerialized,ISNULL(bitLotNo,0) AS bitLotNo,
            @numOppId AS numOppId,t2.numQty AS numUnitHourOrig,
			ISNULL(I.numItemCode,0) AS [numItemCode],
			ISNULL(I.bitKitParent,0) AS bitKitParent,
			ISNULL(t2.numOppChildItemID,0) AS numOppChildItemID, 
			ISNULL(t2.numOppKitChildItemID,0) AS numOppKitChildItemID,
			1 AS bitChildItem,
			t2.tintLevel AS tintLevel,
			'' AS vcPromotionDetail,
			(CASE WHEN ISNULL(WI.numWareHouseItemID,0) > 0 THEN W.vcWareHouse ELSE '' END) vcWarehouse,
			'' vcInclusionDetails
			,0
			,0
			,numOnHand
			, ''
			,(CASE WHEN I.bitKitParent = 1 THEN ISNULL(dbo.fn_GetKitAvailabilityByWarehouse(I.numItemCode,W.numWareHouseID),0) ELSE (ISNULL(numOnHand,0) + ISNULL(numAllocation,0)) END) AS numAvailable
			,''
		FROM
			@TEMPTABLE t2
		INNER JOIN
			Item I
		ON
			t2.numItemCode = I.numItemCode
		LEFT JOIN
			WareHouseItems WI
		ON
			WI.numWareHouseItemID = t2.numWarehouseItemID
		LEFT JOIN 
			dbo.Warehouses W 
		ON 
			WI.numDomainID = W.numDomainID
            AND WI.numWareHouseID = W.numWareHouseID
        LEFT JOIN 
			dbo.WarehouseLocation WL 
		ON 
			WL.numWLocationID = WI.numWLocationID
		LEFT JOIN 
			ListDetails L 
		ON 
			I.numItemClassification = L.numListItemID
		LEFT JOIN 
			Vendor V 
		ON 
			V.numVendorID = @DivisionID
            AND V.numItemCode = t2.numItemCode
		LEFT JOIN 
			UOM u 
		ON 
			u.numUOMId = t2.numUOMId 
	END


    DECLARE @strSQLUpdate AS VARCHAR(2000);SET @strSQLUpdate=''
   
    EXEC ('ALTER TABLE #Temp1 ADD [TotalTax] DECIMAL(20,5)')

    IF @tintTaxOperator = 1 --add Sales tax
	BEGIN
		SET @strSQLUpdate = @strSQLUpdate
            + ',[TotalTax]= (CASE WHEN ISNULL(bitChildItem,0) = 0 THEN dbo.fn_CalBizDocTaxAmt('
            + CONVERT(VARCHAR(20), @numDomainID) + ',0,'
            + CONVERT(VARCHAR(20), @numOppId) +','+
            + 'numoppitemtCode,1,Amount,numUnitHour) ELSE 0 END)'
	END        
    ELSE IF @tintTaxOperator = 2 -- remove sales tax
	BEGIN
        SET @strSQLUpdate = @strSQLUpdate + ',[TotalTax]= 0'
	END
    ELSE 
	BEGIN
        SET @strSQLUpdate = @strSQLUpdate
            + ',[TotalTax]= (CASE WHEN ISNULL(bitChildItem,0) = 0 THEN dbo.fn_CalBizDocTaxAmt('
            + CONVERT(VARCHAR(20), @numDomainID) + ',0,'
            + CONVERT(VARCHAR(20), @numOppId) +','+
            + 'numoppitemtCode,1,Amount,numUnitHour) ELSE 0 END)'
    END

	-- CRV TAX TYPE
	EXEC ('ALTER TABLE #Temp1 ADD [CRV] DECIMAL(20,5)')

	IF @tintTaxOperator = 2
	BEGIN
		SET @strSQLUpdate = @strSQLUpdate
				+ ',[CRV]= (CASE WHEN ISNULL(bitChildItem,0) = 0 THEN dbo.fn_CalBizDocTaxAmt('
				+ CONVERT(VARCHAR(20), @numDomainID) + ',1,'
				+ CONVERT(VARCHAR(20), @numOppId) +','+
				+ 'numoppitemtCode,1,Amount,numUnitHour) ELSE 0 END)'
	END
	ELSE
	BEGIN
		SET @strSQLUpdate = @strSQLUpdate + ',[CRV]= 0'
	END

	DECLARE @vcTaxName AS VARCHAR(100)
    DECLARE @numTaxItemID AS NUMERIC(9)
    SET @numTaxItemID = 0
    
	SELECT TOP 1
        @vcTaxName = vcTaxName,
        @numTaxItemID = numTaxItemID
    FROM    
		TaxItems
    WHERE   
		numDomainID = @numDomainID

    WHILE @numTaxItemID > 0
    BEGIN
        EXEC ('ALTER TABLE #Temp1 ADD [' + @vcTaxName + '] DECIMAL(20,5)')

		IF @tintTaxOperator = 2 -- remove sales tax
		BEGIN
			SET @strSQLUpdate = @strSQLUpdate + ',[' + @vcTaxName + ']=0' 
		END
		ELSE
		BEGIN
			SET @strSQLUpdate = @strSQLUpdate + ',[' + @vcTaxName
				+ ']= (CASE WHEN ISNULL(bitChildItem,0) = 0 THEN dbo.fn_CalBizDocTaxAmt('
				+ CONVERT(VARCHAR(20), @numDomainID) + ','
				+ CONVERT(VARCHAR(20), @numTaxItemID) + ','
				+ CONVERT(VARCHAR(20), @numOppId) +','+
				+ 'numoppitemtCode,1,Amount,numUnitHour) ELSE 0 END)'
		END

        SELECT TOP 1
			@vcTaxName = vcTaxName,
            @numTaxItemID = numTaxItemID
        FROM    
			TaxItems
        WHERE   
			numDomainID = @numDomainID
            AND numTaxItemID > @numTaxItemID

        IF @@rowcount = 0 
            SET @numTaxItemID = 0
    END

    IF @strSQLUpdate <> '' 
    BEGIN
        SET @strSQLUpdate = 'UPDATE #Temp1 SET ItemCode=ItemCode' + @strSQLUpdate + ' WHERE ItemCode > 0 AND bitChildItem=0'
        EXEC (@strSQLUpdate)
    END

    DECLARE @vcDbColumnName NVARCHAR(50)

    SELECT TOP 1
            @numFldID = numfieldID,
            @vcFldname = vcFieldName,
            @vcDbColumnName = vcDbColumnName,
            @intRowNum = ( tintRow + 1 )
    FROM    View_DynamicCustomColumns
    WHERE   numFormID = @tintType
            AND Grp_id = 5
            AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId
            AND bitCustom = 1
            AND ISNULL(numRelCntType, 0) = @numBizDocTempID
    ORDER BY tintRow     
	                                                                                     
    WHILE @intRowNum > 0                                                                                  
    BEGIN    
        EXEC('ALTER TABLE #Temp1 ADD [' + @vcDbColumnName + '] varchar(100)')
	
        SET @strSQLUpdate = 'UPDATE #Temp1 set ItemCode=ItemCode,[' + @vcDbColumnName + ']= dbo.GetCustFldValueOppItems(' + @numFldID  + ',numoppitemtCode,ItemCode) WHERE ItemCode > 0 AND bitChildItem=0'
            
		EXEC (@strSQLUpdate)
		                   
        SELECT 
			TOP 1
            @numFldID = numfieldID,
            @vcFldname = vcFieldName,
            @vcDbColumnName = vcDbColumnName,
            @intRowNum = ( tintRow + 1 )
        FROM    
			View_DynamicCustomColumns
        WHERE   
			numFormID = @tintType
            AND Grp_id = 5
            AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId
            AND bitCustom = 1
            AND tintRow >= @intRowNum
            AND ISNULL(numRelCntType, 0) = @numBizDocTempID
        ORDER BY 
			tintRow                                                                  
		           
        IF @@rowcount = 0 
            SET @intRowNum = 0                                                                                          
    END


    SELECT  
		ROW_NUMBER() OVER ( ORDER BY numSortOrder) RowNumber,
        *
    FROM    
		#Temp1
    ORDER BY 
		numSortOrder ASC, tintLevel ASC, numoppitemtCode ASC, numOppChildItemID ASC, numOppKitChildItemID ASC

    DROP TABLE #Temp1

    SELECT  
		ISNULL(tintOppStatus, 0)
    FROM    
		OpportunityMaster
    WHERE   
		numOppID = @numOppId                                                                                                 


	SELECT * FROM #TempBizDocProductGridColumns

	IF OBJECT_ID('tempdb..#TempBizDocProductGridColumns') IS NOT NULL DROP TABLE #TempBizDocProductGridColumns
END
GO
/****** Object:  StoredProcedure [dbo].[USP_OPPGetINItemsForAuthorizativeAccounting]    Script Date: 07/26/2008 16:20:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by Siva                                                                                                            
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_oppgetinitemsforauthorizativeaccounting')
DROP PROCEDURE usp_oppgetinitemsforauthorizativeaccounting
GO
CREATE PROCEDURE  [dbo].[USP_OPPGetINItemsForAuthorizativeAccounting]                                                                                                                                                
(                                                                                                                                                
	@numOppId AS NUMERIC(18,0)=null,                                                                                                                                                
	@numOppBizDocsId AS NUMERIC(18,0)=null,                                                                
	@numDomainID AS NUMERIC(18,0)=0 ,                                                      
	@numUserCntID AS NUMERIC(18,0)=0                                                                                                                                               
)                                                                                                                                                                                                                                                                                         
AS 
BEGIN
	DECLARE @numCurrencyID AS NUMERIC(18,0)
	DECLARE @fltExchangeRate AS FLOAT

	DECLARE @fltCurrentExchangeRate AS FLOAT
	DECLARE @tintType AS TINYINT
	DECLARE @vcPOppName AS VARCHAR(100)
	DECLARE @bitPPVariance AS BIT
	DECLARE @numDivisionID AS NUMERIC(18,0)

	SELECT 
		@numCurrencyID=numCurrencyID
		,@fltExchangeRate=fltExchangeRate
		,@tintType=tintOppType
		,@vcPOppName=vcPOppName
		,@bitPPVariance=ISNULL(bitPPVariance,0)
		,@numDivisionID=numDivisionID 
	FROM 
		OpportunityMaster
	WHERE 
		numOppID=@numOppId 

	DECLARE @vcBaseCurrency AS VARCHAR(100)=''
	DECLARE @bitAutolinkUnappliedPayment AS BIT 

	SELECT 
		@vcBaseCurrency=ISNULL(C.varCurrSymbol,'')
		,@bitAutolinkUnappliedPayment=ISNULL(bitAutolinkUnappliedPayment,0) 
	FROM 
		dbo.Domain D 
	LEFT JOIN 
		Currency C 
	ON 
		D.numCurrencyID=C.numCurrencyID 
	WHERE 
		D.numDomainID=@numDomainID

	DECLARE @vcForeignCurrency AS VARCHAR(100) = ''
	SELECT 
		@vcForeignCurrency=ISNULL(C.varCurrSymbol,'') 
	FROM  
		Currency C 
	WHERE 
		numCurrencyID=@numCurrencyID

	DECLARE @fltExchangeRateBizDoc AS FLOAT 
	DECLARE @numBizDocId AS NUMERIC(18,0)                                                           
	DECLARE @vcBizDocID AS VARCHAR(100)

	SELECT 
		@numBizDocId=numBizDocId
		,@fltExchangeRateBizDoc=fltExchangeRateBizDoc
		,@vcBizDocID=vcBizDocID 
	FROM 
		OpportunityBizDocs 
	WHERE 
		numOppBizDocsId=@numOppBizDocsId                                                         

	IF @numCurrencyID > 0 
		SET @fltCurrentExchangeRate=dbo.GetExchangeRate(@numDomainID,@numCurrencyID)
	ELSE 
		SET @fltCurrentExchangeRate=@fltExchangeRate                                                                                                                                          

	IF @numBizDocId = 296 --FULFILLMENT BIZDOC
	BEGIN
		--STORE AVERAGE COST FOR SHIPPED ITEM SO THAT WE CAN CALCULATE INVENTORY IMAPCT IN ACCOUNTING AND ALSO USE IT WHEN IETM RETURN
		UPDATE
			OBDI
		SET
			OBDI.monAverageCost=ISNULL(I.monAverageCost,0)
		FROM
			OpportunityBizDocItems OBDI
		INNER JOIN
			OpportunityItems OI
		ON
			OBDI.numOppItemID=OI.numoppitemtCode
		INNER JOIN
			Item I
		ON
			OI.numItemCode = I.numItemCode
		WHERE
			OBDI.numOppBizDocID=@numOppBizDocsId
			AND OBDI.monAverageCost IS NULL
	END
                                                                                                                     
	SELECT 
		I.[vcItemName] as Item
		,charitemType as type
		,OBI.vcitemdesc as [desc]
		,OBI.numUnitHour as Unit
		,ISNULL(OBI.monPrice,0) as Price
		,ISNULL(OBI.monTotAmount,0) as Amount
		,ISNULL((opp.monTotAmount/opp.numUnitHour)*OBI.numUnitHour,0) AS  ItemTotalAmount
		,isnull(monListPrice,0) as listPrice
		,convert(varchar,i.numItemCode) as ItemCode
		,numoppitemtCode
		,L.vcdata as vcItemClassification
		,case when bitTaxable=0 then 'No' else 'Yes' end as Taxable
		,isnull(i.numIncomeChartAcntId,0) as itemIncomeAccount
		,isnull(i.numAssetChartAcntId,0) as itemInventoryAsset,'NI' as ItemType        
		,isnull(i.numCOGsChartAcntId,0) as itemCoGs
		,isnull(i.bitExpenseItem,0) as bitExpenseItem
		,isnull(i.numExpenseChartAcntId,0) as itemExpenseAccount
		,(CASE WHEN ISNULL(i.bitVirtualInventory,0) = 1 THEN 0 ELSE isnull(Opp.monAvgCost,0) END) as AverageCost
		,isnull(OBI.bitDropShip,0) as bitDropShip
		,(OBI.monTotAmtBefDiscount-OBI.monTotAmount) as DiscAmt
		,NULLIF(Opp.numProjectID,0) numProjectID
		,NULLIF(Opp.numClassID,0) numClassID
		,ISNULL(i.bitKitParent,0) AS bitKitParent
		,ISNULL(i.bitAssembly,0) AS bitAssembly
		,(CASE WHEN ISNULL(i.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(OBI.monAverageCost,0) END) AS ShippedAverageCost
	FROM 
		OpportunityItems opp
	INNER JOIN 
		OpportunityBizDocItems OBI
	ON 
		OBI.numOppItemID=Opp.numoppitemtCode       
	LEFT JOIN 
		Item I 
	ON 
		opp.numItemCode=I.numItemCode        
	LEFT JOIN 
		ListDetails L 
	ON 
		I.numItemClassification=L.numListItemID        
	WHERE 
		Opp.numOppId=@numOppId 
		AND OBI.numOppBizDocID=@numOppBizDocsId
                                                               
	SELECT 
		ISNULL(@numCurrencyID,0) as numCurrencyID
		,ISNULL(@fltExchangeRate,1) as fltExchangeRate
		,ISNULL(@fltCurrentExchangeRate,1) as CurrfltExchangeRate
		,ISNULL(@fltExchangeRateBizDoc,1) AS fltExchangeRateBizDoc
		,@vcBaseCurrency AS vcBaseCurrency
		,@vcForeignCurrency AS vcForeignCurrency
		,ISNULL(@vcPOppName,'') AS vcPOppName
		,ISNULL(@vcBizDocID,'') AS vcBizDocID
		,ISNULL(@bitPPVariance,0) AS bitPPVariance
		,@numDivisionID AS numDivisionID
		,ISNULL(@bitAutolinkUnappliedPayment,0) AS bitAutolinkUnappliedPayment                                            
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by Siva                                                                                                            
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityBizDocKitItemsForAuthorizativeAccounting')
DROP PROCEDURE USP_OpportunityBizDocKitItemsForAuthorizativeAccounting
GO
CREATE PROCEDURE  [dbo].[USP_OpportunityBizDocKitItemsForAuthorizativeAccounting]                                                                                                                                                
(                                                                                                                                                
	@numOppId NUMERIC(18,0),
	@numOppBizDocsId NUMERIC(18,0)                                                            
)                                                                                                                                        
AS                           
BEGIN
	DECLARE @numBizDocId AS NUMERIC(18,0)                                                           

	SELECT 
		@numBizDocId=numBizDocId
	FROM 
		OpportunityBizDocs 
	WHERE 
		numOppBizDocsId=@numOppBizDocsId

	IF @numBizDocId = 296 --FULFILLMENT BIZDOC
	BEGIN
		--STORE AVERAGE COST FOR SHIPPED ITEM SO THAT WE CAN CALCULATE INVENTORY IMAPCT IN ACCOUNTING AND ALSO USE IT WHEN IETM RETURN
		INSERT INTO OpportunityBizDocKitItems
		(
			numOppBizDocID,
			numOppBizDocItemID,
			numOppChildItemID,
			numChildItemID,
			monAverageCost
		)
		SELECT
			OBI.numOppBizDocID
			,OBI.numOppBizDocItemID
			,OKI.numOppChildItemID
			,OKI.numChildItemID
			,(CASE 
				WHEN ISNULL(I.charItemType,'') = 'S' AND ISNULL(I.bitExpenseItem,0) = 1 
				THEN 
					(
						CASE 
							WHEN ISNULL(IMain.tintKitAssemblyPriceBasedOn,0) = 3 
							THEN ISNULL(monCost,0) * dbo.fn_UOMConversion(I.numBaseUnit,I.numItemCode,I.numDomainID,I.numPurchaseUnit)
							ELSE ISNULL(I.monListPrice,0) 
						END
					)
				ELSE 
					ISNULL(I.monAverageCost,0) 
			END) 
		FROM
			OpportunityBizDocItems OBI
		INNER JOIN
			OpportunityItems OI
		ON
			OBI.numOppItemID=OI.numoppitemtCode
		INNER JOIN 
			Item IMain
		ON
			OI.numItemCode = IMain.numItemCode
		INNER JOIN 
			OpportunityKitItems OKI 
		ON 
			OKI.numOppItemID=OI.numoppitemtCode 
			AND OI.numOppId=OKI.numOppId
		INNER JOIN 
			Item I
		ON 
			OKI.numChildItemID=i.numItemCode
		LEFT JOIN
			Vendor V
		ON
			I.numVendorID = V.numVendorID
			AND I.numItemCode = V.numItemCode
		WHERE 
			OBI.numOppBizDocID=@numOppBizDocsId
			AND OBI.numOppBizDocItemID NOT IN (SELECT numOppBizDocItemID FROM OpportunityBizDocKitItems WHERE numOppBizDocID=@numOppBizDocsId)

		INSERT INTO OpportunityBizDocKitChildItems
		(
			numOppBizDocID,
			numOppBizDocItemID,
			numOppBizDocKitItemID,
			numOppKitChildItemID,
			numChildItemID,
			monAverageCost
		)
		SELECT
			OBI.numOppBizDocID
			,OBI.numOppBizDocItemID
			,OBKI.numOppBizDocItemID
			,OKCI.numOppKitChildItemID
			,OKCI.numItemID
			,(CASE 
				WHEN ISNULL(I.charItemType,'') = 'S' AND ISNULL(I.bitExpenseItem,0) = 1 
				THEN 
					(
						CASE 
							WHEN ISNULL(IMain.tintKitAssemblyPriceBasedOn,0) = 3 
							THEN ISNULL(monCost,0) * dbo.fn_UOMConversion(I.numBaseUnit,I.numItemCode,I.numDomainID,I.numPurchaseUnit)
							ELSE ISNULL(I.monListPrice,0) 
						END
					)
				ELSE 
					ISNULL(I.monAverageCost,0) 
			END) 
		FROM
			OpportunityBizDocItems OBI
		INNER JOIN
			OpportunityBizDocKitItems OBKI
		ON
			OBI.numOppBizDocItemID = OBKI.numOppBizDocItemID
		INNER JOIN 
			Item IMain
		ON
			OBKI.numChildItemID = IMain.numItemCode
		INNER JOIN
			OpportunityKitChildItems OKCI
		ON
			OBI.numOppItemID = OKCI.numOppItemID
			AND OBKI.numOppChildItemID = OKCI.numOppChildItemID
		INNER JOIN 
			Item I
		ON 
			OKCI.numItemID=I.numItemCode
		LEFT JOIN
			Vendor V
		ON
			I.numVendorID = V.numVendorID
			AND I.numItemCode = V.numItemCode
		WHERE 
			OBI.numOppBizDocID=@numOppBizDocsId
			AND OBI.numOppBizDocItemID NOT IN (SELECT numOppBizDocItemID FROM OpportunityBizDocKitChildItems WHERE numOppBizDocID=@numOppBizDocsId)
	END

     --OpportunityKitItems
    SELECT        
		I.charitemType as type,
		CONVERT(VARCHAR,I.numItemCode) as ItemCode,
		OBKI.numOppChildItemID AS numoppitemtCode,
		(ISNULL(OBI.numUnitHour,0) * (OKI.numQtyItemsReq_Orig * ISNULL(dbo.fn_UOMConversion(OKI.numUOMId,OKI.numChildItemID,i.numDomainID,i.numBaseUnit),1))) as Unit,       
		ISNULL(I.numIncomeChartAcntId,0) AS itemIncomeAccount,
		ISNULL(I.numAssetChartAcntId,0) as itemInventoryAsset,
		ISNULL(I.numCOGsChartAcntId,0) AS itemCoGs
		,isnull(I.bitExpenseItem,0) as bitExpenseItem
		,isnull(I.numExpenseChartAcntId,0) as itemExpenseAccount
		,(CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(OBKI.monAverageCost,0) END) AS ShippedAverageCost,
		ISNULL(OI.bitDropShip,0) AS bitDropShip,  
		NULLIF(OI.numProjectID,0) numProjectID,
		NULLIF(OI.numClassID,0) numClassID
	FROM  
		OpportunityBizDocItems OBI
	INNER JOIN 
		OpportunityBizDocKitItems OBKI 
	ON 
		OBKI.numOppBizDocItemID=OBI.numOppBizDocItemID
	INNER JOIN
		OpportunityKitItems OKI
	ON
		OKI.numOppChildItemID=OBKI.numOppChildItemID
	INNER JOIN
		OpportunityItems OI
	ON
		OKI.numOppItemID=OI.numoppitemtCode
	INNER JOIN 
		Item IMain
	ON 
		OI.numItemCode=IMain.numItemCode
	INNER JOIN 
		Item I
	ON 
		OBKI.numChildItemID=I.numItemCode     
	WHERE 
		OI.numOppId=@numOppId 
		AND OBI.numOppBizDocID=@numOppBizDocsId
		AND ISNULL(I.bitKitParent,0) = 0
		AND ISNULL(iMain.bitAssembly,0) = 0
	UNION ALL
	SELECT 
		I.charitemType as type,              
		CONVERT(VARCHAR,I.numItemCode) as ItemCode,
		OBKCI.numOppKitChildItemID AS numoppitemtCode,
		(ISNULL(OBI.numUnitHour,0) * ISNULL(OKI.numQtyItemsReq_Orig,0) * (ISNULL(OKCI.numQtyItemsReq_Orig,0) * ISNULL(dbo.fn_UOMConversion(OKCI.numUOMId,OKCI.numItemID,i.numDomainID,i.numBaseUnit),1))) as Unit,     
		ISNULL(I.numIncomeChartAcntId,0) AS itemIncomeAccount,
		ISNULL(I.numAssetChartAcntId,0) as itemInventoryAsset,
		ISNULL(I.numCOGsChartAcntId,0) AS itemCoGs
		,isnull(I.bitExpenseItem,0) as bitExpenseItem
		,isnull(I.numExpenseChartAcntId,0) as itemExpenseAccount
		,(CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(OBKCI.monAverageCost,0) END) AS ShippedAverageCost,
		ISNULL(OI.bitDropShip,0) AS bitDropShip,  
		NULLIF(OI.numProjectID,0) numProjectID,
		NULLIF(OI.numClassID,0) numClassID
	FROM  
		OpportunityBizDocItems OBI
	INNER JOIN 
		OpportunityBizDocKitChildItems OBKCI 
	ON 
		OBKCI.numOppBizDocItemID=OBI.numOppBizDocItemID
	INNER JOIN
		OpportunityKitChildItems OKCI
	ON
		OKCI.numOppKitChildItemID = OBKCI.numOppKitChildItemID
	INNER JOIN
		OpportunityKitItems OKI
	ON
		OKCI.numOppChildItemID = OKI.numOppChildItemID
	INNER JOIN
		OpportunityItems OI
	ON
		OKI.numOppItemID=OI.numoppitemtCode
	INNER JOIN 
		Item IMain
	ON 
		OI.numItemCode=IMain.numItemCode
	INNER JOIN 
		Item I
	ON 
		OBKCI.numChildItemID=I.numItemCode             
	WHERE 
		OI.numOppId=@numOppId 
		AND OBI.numOppBizDocID=@numOppBizDocsId
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityItems_ValidateKitChildsOrderImport')
DROP PROCEDURE dbo.USP_OpportunityItems_ValidateKitChildsOrderImport
GO
CREATE PROCEDURE [dbo].[USP_OpportunityItems_ValidateKitChildsOrderImport]
(
	@numDomainID NUMERIC(18,0),
    @numItemCode NUMERIC(18,0),
    @vcKitChilds VARCHAR(MAX)
)
AS 
BEGIN
	DECLARE @vcKitChildItemCodes VARCHAR(MAX) = ''

	DECLARE @TempSelectedItems TABLE
	(
		ID INT IDENTITY(1,1)
		,vcKitItemName VARCHAR(300)
		,numKitItemCode NUMERIC(18,0)
		,vcKitChildItemName VARCHAR(300)
		,numKitChildItemCode NUMERIC(18,0)
	)

	DECLARE @bitKitParent BIT
	SELECT @bitKitParent=ISNULL(bitKitParent,0) FROM Item WHERE numItemCode=@numItemCode

	IF ISNULL(@bitKitParent,0) = 1 AND (SELECT 
											COUNT(*) 
										FROM 
											ItemDetails 
										INNER JOIN 
											Item 
										ON 
											ItemDetails.numChildItemID=Item.numItemCode 
										WHERE 
											ItemDetails.numItemKitID=@numItemCode 
											AND ISNULL(Item.bitKitParent,0)=1) > 0
	BEGIN
		IF LEN(ISNULL(@vcKitChilds,'')) > 0
		BEGIN
			DECLARE @TempKits TABLE
			(
				ID INT IDENTITY(1,1)
				,vcKit VARCHAR(MAX)
			)
			DECLARE @posComma int, @strKeyVal varchar(MAX)

			SET @vcKitChilds=RTRIM(@vcKitChilds)
			SET @posComma=PATINDEX('%#,#%', @vcKitChilds)

			IF @posComma > 0
			BEGIN
				WHILE @posComma > 0
				BEGIN
					SET @strKeyVal=LTRIM(RTRIM(SUBSTRING(@vcKitChilds, 1, @posComma-1)))
	
					INSERT INTO @TempKits (vcKit) VALUES (@strKeyVal)

					SET @vcKitChilds=SUBSTRING(@vcKitChilds, @posComma +3, LEN(@vcKitChilds)-@posComma)

					SET @posComma=PATINDEX('%#,#%',@vcKitChilds)	
				END

				INSERT INTO @TempKits (vcKit) VALUES (@vcKitChilds)
			END
			ELSE
			BEGIN
				INSERT INTO @TempKits (vcKit) VALUES (@vcKitChilds)
			END
			
			DECLARE @i INT = 1
			DECLARE @iCount INT
			DECLARE @vcTempKits VARCHAR(MAX)
			DECLARE @vcTempKitChilds VARCHAR(MAX)
			DECLARE @vcTempKitItemName VARCHAR(MAX)
			DECLARE @vcTempKitChildItemName VARCHAR(MAX)
			DECLARE @iPOSComma INT

			SELECT @iCount=COUNT(*) FROM @TempKits

			WHILE @i <= @iCount
			BEGIN
				SELECT @vcTempKits=ISNULL(vcKit,0) FROM @TempKits WHERE ID=@i

				SET @posComma=PATINDEX('%#:#%', @vcTempKits)

				IF @posComma > 0
				BEGIN
					SET @vcTempKitItemName = ISNULL(LTRIM(RTRIM(SUBSTRING(@vcTempKits, 1, @posComma-1))),'')

					SET @vcTempKitChilds = ISNULL(SUBSTRING(@vcTempKits, @posComma + 3, LEN(@vcTempKits)-@posComma),'')

					SET @iPOSComma=PATINDEX('%#~#%', @vcTempKitChilds)

					IF @iPOSComma > 0
					BEGIN
						WHILE @iPOSComma > 0
						BEGIN
							SET @strKeyVal=LTRIM(RTRIM(SUBSTRING(@vcTempKitChilds, 1, @iPOSComma-1)))
	
							INSERT INTO @TempSelectedItems (vcKitItemName,vcKitChildItemName) VALUES (@vcTempKitItemName,@strKeyVal)

							SET @vcTempKitChilds=SUBSTRING(@vcTempKitChilds, @iPOSComma +3, LEN(@vcTempKitChilds)-@iPOSComma)

							SET @iPOSComma=PATINDEX('%#~#%',@vcTempKitChilds)	
						END

						INSERT INTO @TempSelectedItems (vcKitItemName,vcKitChildItemName) VALUES (@vcTempKitItemName,@vcTempKitChilds)
					END
					ELSE
					BEGIN
						INSERT INTO @TempSelectedItems (vcKitItemName,vcKitChildItemName) VALUES (@vcTempKitItemName,@vcTempKitChilds)
					END

					SET @vcTempKits = SUBSTRING(@vcTempKits, @posComma + 3, LEN(@vcTempKits)-@posComma)
				END
				ELSE
				BEGIN
					RAISERROR('INVALID_INCLISION_DETAIL',16,1)
					RETURN
				END

				SET @i = @i + 1
			END
		END
		ELSE
		BEGIN
			IF (SELECT 
					COUNT(*) 
				FROM 
					ItemDetails 
				INNER JOIN 
					Item 
				ON 
					ItemDetails.numChildItemID=Item.numItemCode 
				WHERE 
					ItemDetails.numItemKitID=@numItemCode 
					AND ISNULL(Item.bitKitParent,0)=1 
					AND ISNULL(Item.bitKitSingleSelect,0) = 1) >0
			BEGIN
				RAISERROR('INVALID_INCLISION_DETAIL',16,1)
				RETURN
			END
		END

		DECLARE @numKitItemCode NUMERIC(18,0)
		DECLARE @numKitChildItemCode NUMERIC(18,0)
		DECLARE @vcKitItemName VARCHAR(MAX)
		DECLARE @vcKitChildItemName VARCHAR(MAX)
		SET @i = 1
		SELECT @iCount=COUNT(*) FROM @TempSelectedItems

		WHILE @i <= @iCount
		BEGIN
			SELECT @vcKitItemName=ISNULL(vcKitItemName,''),@vcKitChildItemName=ISNULL(vcKitChildItemName,'') FROM @TempSelectedItems WHERE ID=@i

			IF (SELECT COUNT(*) FROM Item WHERE numDomainID=@numDomainID AND vcItemName=@vcKitItemName) = 0
			BEGIN
				RAISERROR('KIT_ITEM_NOT_FOUND',16,1)
				RETURN
			END
			ELSE IF (SELECT COUNT(*) FROM Item WHERE numDomainID=@numDomainID AND vcItemName=@vcKitItemName) > 1
			BEGIN
				RAISERROR('MULTIPLE_KIT_ITEM_FOUND',16,1)
				RETURN
			END
			ELSE IF (SELECT COUNT(*) FROM Item WHERE numDomainID=@numDomainID AND vcItemName=@vcKitChildItemName) = 0
			BEGIN
				RAISERROR('KIT_CHILD_ITEM_NOT_FOUND',16,1)
				RETURN
			END
			ELSE IF (SELECT COUNT(*) FROM Item WHERE numDomainID=@numDomainID AND vcItemName=@vcKitChildItemName) > 1
			BEGIN
				RAISERROR('MULTIPLE_KIT_CHILD_ITEM_FOUND',16,1)
				RETURN
			END
			ELSE
			BEGIN
				SET @numKitItemCode = (SELECT numItemCode FROM Item WHERE numDomainID=@numDomainID AND vcItemName=@vcKitItemName)
				SET @numKitChildItemCode = (SELECT numItemCode FROM Item WHERE numDomainID=@numDomainID AND vcItemName=@vcKitChildItemName)

				IF NOT EXISTS (SELECT 
									numItemDetailID 
								FROM 
									ItemDetails 
								INNER JOIN 
									Item 
								ON 
									ItemDetails.numChildItemID=Item.numItemCode 
								WHERE 
									numItemKitID=@numItemCode 
									AND numChildItemID=@numKitItemCode 
									AND ISNULL(bitKitParent,0)=1)
				BEGIN
					RAISERROR('INVALID_KIT_ITEM',16,1)
					RETURN
				END

				IF NOT EXISTS (SELECT 
									numItemDetailID 
								FROM 
									ItemDetails 
								WHERE 
									numItemKitID=@numKitItemCode 
									AND numChildItemID=@numKitChildItemCode)
				BEGIN
					RAISERROR('INVALID_KIT_CHILD_ITEM',16,1)
					RETURN
				END

				UPDATE 
					@TempSelectedItems 
				SET
					numKitItemCode = @numKitItemCode
					,numKitChildItemCode = @numKitChildItemCode
				WHERE
					ID=@i
			END

			SET @i = @i + 1
		END

		IF EXISTS (
			SELECT 
				numItemDetailID
			FROM 
				ItemDetails 
			INNER JOIN 
				Item 
			ON 
				ItemDetails.numChildItemID=Item.numItemCode 
			WHERE 
				ItemDetails.numItemKitID=@numItemCode 
				AND ISNULL(Item.bitKitParent,0)=1 
				AND ISNULL(Item.bitKitSingleSelect,0) = 1
				AND numChildItemID NOT IN (SELECT numKitItemCode FROM @TempSelectedItems)
		)
		BEGIN
			RAISERROR('CHILD_KIT_VALUES_NOT_PROVIDED',16,1)
			RETURN
		END

		SET @vcKitChildItemCodes = STUFF((SELECT ',' + CONCAT(numKitItemCode,'-',numKitChildItemCode) FROM @TempSelectedItems T1 FOR XML PATH('')), 1,1,SPACE(0))
	END
	ELSE
	BEGIN
		SET @vcKitChildItemCodes = ''
	END

	SELECT @vcKitChildItemCodes
END
GO
